USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_cutscene.sch"

#IF IS_DEBUG_BUILD
	USING "flow_debug_GAME.sch"
	USING "commands_debug.sch"
#ENDIF

USING "flow_private_core.sch"
USING "flow_private_game.sch"
USING "flow_processing_missions_GAME.sch"
USING "flow_mission_data_public.sch"

#if  USE_CLF_DLC
using "flow_custom_checks_CLF.sch"
#endif

#if  USE_NRM_DLC
using "flow_custom_checks_NRM.sch"
#endif

#if not USE_CLF_DLC
#if not USE_NRM_DLC
USING "flow_custom_checks_GTA5.sch"
#endif
#endif
USING "selector_public.sch"
#if not USE_CLF_DLC
#if not USE_NRM_DLC	
USING "heist_private.sch"
#ENDIF
#ENDIF
USING "friends_public.sch"
USING "mission_stat_public.sch"
USING "replay_public.sch"
USING "flow_help_public.sch"
USING "respawn_location_private.sch"
USING "shop_public.sch"
USING "blip_control_public.sch"
USING "building_control_public.sch"
USING "code_control_public.sch"
USING "randomchar_private.sch"
USING "organiser_public.sch"
USING "presence_enum.sch"
USING "locates_public.sch"
USING "vehicle_gen_private.sch"
USING "website_public.sch"
USING "cutscene_control_public.sch"
USING "commands_cutscene.sch"
#if not USE_CLF_DLC
#if not USE_NRM_DLC	
USING "shrink_narrative_control.sch"
USING "shrink_stat_tracking.sch"
#endif
#endif
USING "commands_cutscene.sch"
USING "email_public.sch"
USING "commands_stats.sch"

// ===========================================================================================================
//		PHONE commands
// ===========================================================================================================

// PURPOSE:	Adds a contact to a playable character's phonebook.
//
// INPUT PARAMS:			paramCoreVarsArrayPos			Index into the Core Variables array
// RETURN VALUE:			JUMP_LABEL_IDS					A jump label - should be STAY_ON_THIS_COMMAND until ready to move on
FUNC JUMP_LABEL_IDS PERFORM_ADD_PHONE_CONTACT(INT paramCoreVarsArrayPos)

	// Ensure the Phonecall Vars Array position is legal
	IF (paramCoreVarsArrayPos = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_ADD_PHONE_CONTACT: The Core Variables Array Position parameter is illegal. MOVING ON TO NEXT COMMAND.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	enumCharacterList eCharOwner = INT_TO_ENUM(enumCharacterList, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue1)
	enumCharacterList eCharToAdd = INT_TO_ENUM(enumCharacterList, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue2)
	
	BOOL bDisplaySignifier = FALSE
	IF g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue3 = 1
		bDisplaySignifier = TRUE
	ENDIF
	
	//Work out phonebook presence from character enum.
	enumPhonebookPresence ePhonebookPresence
	SWITCH eCharOwner
		CASE CHAR_MICHAEL
			ePhonebookPresence = MICHAEL_BOOK
		BREAK
		CASE CHAR_FRANKLIN
			ePhonebookPresence = FRANKLIN_BOOK
		BREAK
		CASE CHAR_TREVOR
			ePhonebookPresence = TREVOR_BOOK
		BREAK
		DEFAULT
			CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_ADD_PHONE_CONTACT: The phone owner parameter did not point to a playable character. MOVING ON TO NEXT COMMAND.")
		BREAK
	ENDSWITCH
	
	//Add the contact to the phonebook.
	ADD_CONTACT_TO_PHONEBOOK(	eCharToAdd,
								ePhonebookPresence,
								bDisplaySignifier)
								
	//Simply step to the next command.
	RETURN NEXT_SEQUENTIAL_COMMAND
ENDFUNC


// PURPOSE:	Removes a contact from a playable character's phonebook.
//
// INPUT PARAMS:			paramCoreSmallVarsArrayPos		Index into the Core Small Variables array
// RETURN VALUE:			JUMP_LABEL_IDS					A jump label - should be STAY_ON_THIS_COMMAND until ready to move on
FUNC JUMP_LABEL_IDS PERFORM_REMOVE_PHONE_CONTACT(INT paramCoreSmallVarsArrayPos)

	// Ensure the Phonecall Vars Array position is legal
	IF (paramCoreSmallVarsArrayPos = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_REMOVE_PHONE_CONTACT: The Core Small Variables Array Position parameter is illegal. MOVING ON TO NEXT COMMAND.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	enumCharacterList eCharOwner = INT_TO_ENUM(enumCharacterList, g_flowUnsaved.coreSmallVars[paramCoreSmallVarsArrayPos].iValue1)
	enumCharacterList eCharToAdd = INT_TO_ENUM(enumCharacterList, g_flowUnsaved.coreSmallVars[paramCoreSmallVarsArrayPos].iValue2)
	
	//Work out phonebook presence from character enum.
	enumPhonebookPresence ePhonebookPresence
	SWITCH eCharOwner
		CASE CHAR_MICHAEL
			ePhonebookPresence = MICHAEL_BOOK
		BREAK
		CASE CHAR_FRANKLIN
			ePhonebookPresence = FRANKLIN_BOOK
		BREAK
		CASE CHAR_TREVOR
			ePhonebookPresence = TREVOR_BOOK
		BREAK
		DEFAULT
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_REMOVE_PHONE_CONTACT: The phone owner parameter did not point to a playable character. MOVING ON TO NEXT COMMAND.")
		BREAK
	ENDSWITCH
	
	//Remove the contact from the phonebook.
	REMOVE_CONTACT_FROM_INDIVIDUAL_PHONEBOOK(	eCharToAdd,
												ePhonebookPresence)
												
	//Simply step to the next command.
	RETURN NEXT_SEQUENTIAL_COMMAND
ENDFUNC


// PURPOSE: Wait for the player to phone a specific NPC.
//
// INPUT PARAMS:			paramCommsVarsArrayPos		Index into the Comms Variables array
// RETURN VALUE:			JUMP_LABEL_IDS				A jump label - should be STAY_ON_THIS_COMMAND until ready to move on

FUNC JUMP_LABEL_IDS PERFORM_IS_PLAYER_PHONING_NPC(INT paramCommsVarsArrayPos)

	// Ensure the Comms Vars Array position is legal
	IF (paramCommsVarsArrayPos = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_IS_PLAYER_PHONING_NPC: The Comms Variables Array Position parameter is illegal. MOVING ON TO NEXT COMMAND.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF

	// Get the player bitset.
	INT iPlayerCharBitset = g_flowUnsaved.commsVars[paramCommsVarsArrayPos].iValue1
	
	// Is the player the correct character to make the call check?
	IF IS_PLAYER_PED_PLAYABLE(GET_CURRENT_PLAYER_PED_ENUM())
		IF IS_BIT_SET(iPlayerCharBitset, GET_CURRENT_PLAYER_PED_INT())
		
			// Get the NPC to check if we're phoning.
			enumCharacterList eNPC = INT_TO_ENUM(enumCharacterList, g_flowUnsaved.commsVars[paramCommsVarsArrayPos].iValue2)
			
			// Check the player is not the character we are checking.
			IF eNPC = GET_CURRENT_PLAYER_PED_ENUM()
				SCRIPT_ASSERT("PERFORM_IS_PLAYER_PHONING_NPC: Tried to check if the player was phoning themselves! Stepping to next command! Tell BenR.")
				RETURN NEXT_SEQUENTIAL_COMMAND
			ENDIF
			
			// Is the player making a call to this NPC?
			IF IS_CALLING_CONTACT(eNPC)
				RETURN INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.commsVars[paramCommsVarsArrayPos].iValue3)
			ENDIF
		ENDIF
	ENDIF

	RETURN INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.commsVars[paramCommsVarsArrayPos].iValue4)
ENDFUNC


// Convert FlowCommsBitflags into a CommsBitflag bitset.
FUNC INT BUILD_COMMUNICATION_SETTINGS(INT paramFlowCommsBitset)

	INT iCommSettings = 0
	IF IS_BIT_SET(paramFlowCommsBitset, FLOW_COMM_BIT_QUICK)
		SET_BIT(iCommSettings, COMM_BIT_CALL_IS_QUICK)
	ENDIF
	IF IS_BIT_SET(paramFlowCommsBitset, FLOW_COMM_BIT_CONFERENCE)
		SET_BIT(iCommSettings, COMM_BIT_CALL_IS_CONFERENCE)
	ENDIF
	IF IS_BIT_SET(paramFlowCommsBitset, FLOW_COMM_BIT_MISSED)
		SET_BIT(iCommSettings, COMM_BIT_CALL_IS_MISSED)
	ENDIF
	IF IS_BIT_SET(paramFlowCommsBitset, FLOW_COMM_BIT_TODS_SKIP_TIMER)
		SET_BIT(iCommSettings, COMM_BIT_TODS_SKIP_TIMER)
	ENDIF
	IF IS_BIT_SET(paramFlowCommsBitset, FLOW_COMM_BIT_BLOCK_HANGUP)
		SET_BIT(iCommSettings, COMM_BIT_CALL_BLOCK_HANGUP)
	ENDIF
	IF IS_BIT_SET(paramFlowCommsBitset, FLOW_COMM_BIT_UNLOCKS_MISSION)
		SET_BIT(iCommSettings, COMM_BIT_UNLOCKS_MISSION)
	ELIF IS_BIT_SET(paramFlowCommsBitset, FLOW_COMM_BIT_TRIGGERS_MISSION)
		SET_BIT(iCommSettings, COMM_BIT_TRIGGERS_MISSION)
	ENDIF
	IF IS_BIT_SET(paramFlowCommsBitset, FLOW_COMM_BIT_REQUEUE_ON_MISS)
		SET_BIT(iCommSettings, COMM_BIT_CALL_REQUEUE_ON_MISS)
	ELIF IS_BIT_SET(paramFlowCommsBitset, FLOW_COMM_BIT_TEXT_ON_MISS)
		SET_BIT(iCommSettings, COMM_BIT_CALL_TEXT_ON_MISS)
	ENDIF
	IF IS_BIT_SET(paramFlowCommsBitset, FLOW_COMM_BIT_IGNORE_GLOBAL_DELAY)
		SET_BIT(iCommSettings, COMM_BIT_INGORE_GLOBAL_DELAY)
	ENDIF
	
	RETURN iCommSettings
ENDFUNC


// PURPOSE: Force the player to make a call to an NPC character.
//
// INPUT PARAMS:			paramCommsVarsArrayPos		Index into the Comms Variables array.
// RETURN VALUE:			JUMP_LABEL_IDS				A jump label - should be STAY_ON_THIS_COMMAND until ready to move on.

FUNC JUMP_LABEL_IDS PERFORM_PLAYER_PHONE_NPC(INT paramCommsVarsArrayPos)
	// Ensure the Comms Vars Array position is legal
	IF (paramCommsVarsArrayPos = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_PLAYER_PHONE_NPC: The Comms Variables Array Position parameter is illegal. MOVING ON TO NEXT COMMAND.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	//Get the registration ID for this phonecall.
	CC_CommID eCallID = INT_TO_ENUM(CC_CommID, g_flowUnsaved.commsVars[paramCommsVarsArrayPos].iValue1)
	
	//Do we want to automatically skip this command?
	#IF IS_DEBUG_BUILD
		IF g_bDebugFlowSilencePhonecalls
			IF IS_COMMUNICATION_REGISTERED(eCallID)
				CANCEL_COMMUNICATION(eCallID)
			ENDIF
			PRIVATE_CLEAR_LAST_CALL_DATA()
			RETURN NEXT_SEQUENTIAL_COMMAND
		ENDIF
	#ENDIF
	
	//Build settings for this communication.
	INT iFlowCommsBitset = g_flowUnsaved.commsVars[paramCommsVarsArrayPos].iValue7
	
	//Check if the call has completed yet.
	IF IS_BIT_SET(iFlowCommsBitset, FLOW_COMM_BIT_WAIT_UNTIL_SENT)
		IF GET_LAST_COMPLETED_CALL() = eCallID
			PRIVATE_CLEAR_LAST_CALL_DATA()
			RETURN NEXT_SEQUENTIAL_COMMAND
		ENDIF
	ENDIF
	
	//Register the phonecall with the communication controller if it isn't already.
	IF NOT IS_COMMUNICATION_REGISTERED(eCallID)
		
		enumCharacterList eCharTo 	= INT_TO_ENUM(enumCharacterList, g_flowUnsaved.commsVars[paramCommsVarsArrayPos].iValue2)
		enumCharacterList eCharFrom = INT_TO_ENUM(enumCharacterList, g_flowUnsaved.commsVars[paramCommsVarsArrayPos].iValue3)
		CC_CommunicationType eType 	= INT_TO_ENUM(CC_CommunicationType, g_flowUnsaved.commsVars[paramCommsVarsArrayPos].iValue4)
		INT iQueueTime 				= g_flowUnsaved.commsVars[paramCommsVarsArrayPos].iValue5
		INT iRequeueTime 			= g_flowUnsaved.commsVars[paramCommsVarsArrayPos].iValue6
		INT iCommSettings			= BUILD_COMMUNICATION_SETTINGS(iFlowCommsBitset)
		
		REGISTER_CALL_FROM_PLAYER_TO_CHARACTER(	eCallID,
												eType,
												eCharFrom,
												eCharTo,
												3,
												iQueueTime,
												iRequeueTime,
												g_flowUnsaved.commsVars[paramCommsVarsArrayPos].eVectorID,
												g_flowUnsaved.commsVars[paramCommsVarsArrayPos].eCodeID,
												g_flowUnsaved.commsVars[paramCommsVarsArrayPos].eCheckID,
												iCommSettings)
	ENDIF
	
	//Do we need to wait for this call to complete?
	IF IS_BIT_SET(iFlowCommsBitset, FLOW_COMM_BIT_WAIT_UNTIL_SENT)
		RETURN STAY_ON_THIS_COMMAND
	ENDIF
	RETURN NEXT_SEQUENTIAL_COMMAND
ENDFUNC


// PURPOSE: Force the player to make a call to an NPC character. The end of the call can change based on the result of a custom check.
//
// INPUT PARAMS:			paramCommsLargeVarsArrayPos		Index into the Comms Large Variables array.
// RETURN VALUE:			JUMP_LABEL_IDS					A jump label - should be STAY_ON_THIS_COMMAND until ready to move on.

FUNC JUMP_LABEL_IDS PERFORM_PLAYER_PHONE_NPC_WITH_BRANCH(INT paramCommsLargeVarsArrayPos)
	// Ensure the Comms Vars Array position is legal
	IF (paramCommsLargeVarsArrayPos = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_PLAYER_PHONE_NPC_WITH_BRANCH: The Comms Large Variables Array Position parameter is illegal. MOVING ON TO NEXT COMMAND.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	//Get the registration ID for this phonecall.
	CC_CommID eCallID = INT_TO_ENUM(CC_CommID, g_flowUnsaved.commsLargeVars[paramCommsLargeVarsArrayPos].iValue1)
	
	//Do we want to automatically skip this command?
	#IF IS_DEBUG_BUILD
		IF g_bDebugFlowSilencePhonecalls
			IF IS_COMMUNICATION_REGISTERED(eCallID)
				CANCEL_COMMUNICATION(eCallID)
			ENDIF
			PRIVATE_CLEAR_LAST_CALL_DATA()
			RETURN NEXT_SEQUENTIAL_COMMAND
		ENDIF
	#ENDIF
	
	//Build settings for this communication.
	INT iFlowCommsBitset = g_flowUnsaved.commsLargeVars[paramCommsLargeVarsArrayPos].iValue9
	
	//Check if the call has completed yet.
	IF IS_BIT_SET(iFlowCommsBitset, FLOW_COMM_BIT_WAIT_UNTIL_SENT)
		IF GET_LAST_COMPLETED_CALL() = eCallID
			PRIVATE_CLEAR_LAST_CALL_DATA()
			RETURN NEXT_SEQUENTIAL_COMMAND
		ENDIF
	ENDIF
	
	//Register the phonecall with the communication controller if it isn't already.
	IF NOT IS_COMMUNICATION_REGISTERED(eCallID)
		
		CC_CommID eCallTrueBranchID 	= INT_TO_ENUM(CC_CommID, g_flowUnsaved.commsLargeVars[paramCommsLargeVarsArrayPos].iValue2)
		CC_CommID eCallFalseBranchID 	= INT_TO_ENUM(CC_CommID, g_flowUnsaved.commsLargeVars[paramCommsLargeVarsArrayPos].iValue3)
		FLOW_CHECK_IDS eBranchCheck 	= g_flowUnsaved.commsLargeVars[paramCommsLargeVarsArrayPos].eCheckID
		enumCharacterList eCharTo 		= INT_TO_ENUM(enumCharacterList, g_flowUnsaved.commsLargeVars[paramCommsLargeVarsArrayPos].iValue4)
		enumCharacterList eCharFrom 	= INT_TO_ENUM(enumCharacterList, g_flowUnsaved.commsLargeVars[paramCommsLargeVarsArrayPos].iValue5)
		CC_CommunicationType eType 		= INT_TO_ENUM(CC_CommunicationType, g_flowUnsaved.commsLargeVars[paramCommsLargeVarsArrayPos].iValue6)
		INT iQueueTime 					= g_flowUnsaved.commsLargeVars[paramCommsLargeVarsArrayPos].iValue7
		INT iRequeueTime 				= g_flowUnsaved.commsLargeVars[paramCommsLargeVarsArrayPos].iValue8
		INT iCommSettings				= BUILD_COMMUNICATION_SETTINGS(iFlowCommsBitset)
		
		REGISTER_BRANCHED_CALL_FROM_PLAYER_TO_CHARACTER(eCallID,
														eCallTrueBranchID,
														eCallFalseBranchID,
														eBranchCheck,
														eType,
														eCharFrom,
														eCharTo,
														3,
														iQueueTime,
														iRequeueTime,
														g_flowUnsaved.commsLargeVars[paramCommsLargeVarsArrayPos].eVectorID,
														g_flowUnsaved.commsLargeVars[paramCommsLargeVarsArrayPos].eCodeID,
														iCommSettings)
	ENDIF
	
	//Do we need to wait for this call to complete?
	IF IS_BIT_SET(iFlowCommsBitset, FLOW_COMM_BIT_WAIT_UNTIL_SENT)
		RETURN STAY_ON_THIS_COMMAND
	ENDIF
	RETURN NEXT_SEQUENTIAL_COMMAND
ENDFUNC



// PURPOSE: Setup an incoming phonecall to a set of the player characters.
//
// INPUT PARAMS:			paramCommsVarsArrayPos		Index into the Comms Variables array.
// RETURN VALUE:			JUMP_LABEL_IDS				A jump label - should be STAY_ON_THIS_COMMAND until ready to move on.

FUNC JUMP_LABEL_IDS PERFORM_PHONE_PLAYER(INT paramCommsVarsArrayPos)
	// Ensure the Comms Vars Array position is legal
	IF (paramCommsVarsArrayPos = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_PHONE_PLAYER: The Comms Variables Array Position parameter is illegal. MOVING ON TO NEXT COMMAND.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	//Get the registration ID for this phonecall.
	CC_CommID eCallID = INT_TO_ENUM(CC_CommID, g_flowUnsaved.commsVars[paramCommsVarsArrayPos].iValue1)
	//Get the call missed text message ID.
	CC_CommID eMissedCallTextID = INT_TO_ENUM(CC_CommID, g_flowUnsaved.commsVars[paramCommsVarsArrayPos].iValue2)
	
	//Do we want to automatically skip this command?
	#IF IS_DEBUG_BUILD
		IF g_bDebugFlowSilencePhonecalls
			IF IS_COMMUNICATION_REGISTERED(eCallID)
				CANCEL_COMMUNICATION(eCallID)
			ENDIF
			PRIVATE_CLEAR_LAST_CALL_DATA()
			RETURN NEXT_SEQUENTIAL_COMMAND
		ENDIF
	#ENDIF
	
	//Build settings for this communication.
	INT iFlowCommsBitset = g_flowUnsaved.commsVars[paramCommsVarsArrayPos].iValue8
	INT iCommSettings = BUILD_COMMUNICATION_SETTINGS(iFlowCommsBitset)
	
	//Check if the call has completed yet.
	IF IS_BIT_SET(iFlowCommsBitset, FLOW_COMM_BIT_WAIT_UNTIL_SENT)
		//Does this call have a missed text assigned to it?
		IF IS_BIT_SET(iCommSettings, COMM_BIT_CALL_TEXT_ON_MISS)
			//Yes, is it registered or just sent?
			IF IS_COMMUNICATION_REGISTERED(eMissedCallTextID)
				//Registered, we need to wait for it.
				RETURN STAY_ON_THIS_COMMAND
			ELIF GET_LAST_COMPLETED_TEXT_MESSAGE() = eMissedCallTextID
				//Just sent, we're done.
				PRIVATE_CLEAR_LAST_TEXT_DATA()
				IF GET_LAST_COMPLETED_CALL() = eCallID
					PRIVATE_CLEAR_LAST_CALL_DATA()
				ENDIF
				RETURN NEXT_SEQUENTIAL_COMMAND
			ENDIF
		ENDIF
	
		IF GET_LAST_COMPLETED_CALL() = eCallID
			PRIVATE_CLEAR_LAST_CALL_DATA()
			RETURN NEXT_SEQUENTIAL_COMMAND
		ENDIF
	ENDIF
	
	IF NOT IS_COMMUNICATION_REGISTERED(eCallID)
	
		INT iToPlayerBitset 		= g_flowUnsaved.commsVars[paramCommsVarsArrayPos].iValue3
		enumCharacterList eCharFrom = INT_TO_ENUM(enumCharacterList, g_flowUnsaved.commsVars[paramCommsVarsArrayPos].iValue4)
		CC_CommunicationType eType 	= INT_TO_ENUM(CC_CommunicationType, g_flowUnsaved.commsVars[paramCommsVarsArrayPos].iValue5)
		INT iQueueTime 				= g_flowUnsaved.commsVars[paramCommsVarsArrayPos].iValue6
		INT iRequeueTime 			= g_flowUnsaved.commsVars[paramCommsVarsArrayPos].iValue7
		
		REGISTER_CALL_FROM_CHARACTER_TO_PLAYER(	eCallID,
												eType,
												iToPlayerBitset,
												eCharFrom,
												3,
												iQueueTime,
												iRequeueTime,
												eMissedCallTextID,
												g_flowUnsaved.commsVars[paramCommsVarsArrayPos].eVectorID,
												g_flowUnsaved.commsVars[paramCommsVarsArrayPos].eCodeID,
												g_flowUnsaved.commsVars[paramCommsVarsArrayPos].eCheckID,
												iCommSettings)
	ENDIF
	
	//Do we need to wait for this call to complete?
	IF IS_BIT_SET(iFlowCommsBitset, FLOW_COMM_BIT_WAIT_UNTIL_SENT)
		RETURN STAY_ON_THIS_COMMAND
	ENDIF
	RETURN NEXT_SEQUENTIAL_COMMAND
ENDFUNC


// PURPOSE: Setup an incoming phonecall to a set of the player characters. The end of the call can change based on the result of a custom check.
//
// INPUT PARAMS:			paramCommsLargeVarsArrayPos		Index into the Comms Large Variables array.
// RETURN VALUE:			JUMP_LABEL_IDS					A jump label - should be STAY_ON_THIS_COMMAND until ready to move on.

FUNC JUMP_LABEL_IDS PERFORM_PHONE_PLAYER_WITH_BRANCH(INT paramCommsLargeVarsArrayPos)
	// Ensure the Comms Vars Array position is legal
	IF (paramCommsLargeVarsArrayPos = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_PHONE_PLAYER_WITH_BRANCH: The Comms Large Variables Array Position parameter is illegal. MOVING ON TO NEXT COMMAND.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	//Get the registration ID for this phonecall.
	CC_CommID eCallID = INT_TO_ENUM(CC_CommID, g_flowUnsaved.commsLargeVars[paramCommsLargeVarsArrayPos].iValue1)
	
	//Do we want to automatically skip this command?
	#IF IS_DEBUG_BUILD
		IF g_bDebugFlowSilencePhonecalls
			IF IS_COMMUNICATION_REGISTERED(eCallID)
				CANCEL_COMMUNICATION(eCallID)
			ENDIF
			PRIVATE_CLEAR_LAST_CALL_DATA()
			RETURN NEXT_SEQUENTIAL_COMMAND
		ENDIF
	#ENDIF
	
	//Build settings for this communication.
	INT iFlowCommsBitset = g_flowUnsaved.commsLargeVars[paramCommsLargeVarsArrayPos].iValue9
	INT iCommSettings = BUILD_COMMUNICATION_SETTINGS(iFlowCommsBitset)
	
	//Check if the call has completed yet.
	IF IS_BIT_SET(iFlowCommsBitset, FLOW_COMM_BIT_WAIT_UNTIL_SENT)
		IF GET_LAST_COMPLETED_CALL() = eCallID
			PRIVATE_CLEAR_LAST_CALL_DATA()
			RETURN NEXT_SEQUENTIAL_COMMAND
		ENDIF
	ENDIF
	
	IF NOT IS_COMMUNICATION_REGISTERED(eCallID)
	
		CC_CommID eCallTrueBranchID 	= INT_TO_ENUM(CC_CommID, g_flowUnsaved.commsLargeVars[paramCommsLargeVarsArrayPos].iValue2)
		CC_CommID eCallFalseBranchID 	= INT_TO_ENUM(CC_CommID, g_flowUnsaved.commsLargeVars[paramCommsLargeVarsArrayPos].iValue3)
		FLOW_CHECK_IDS eBranchCheck 	= g_flowUnsaved.commsLargeVars[paramCommsLargeVarsArrayPos].eCheckID
		INT iToPlayerBitset 			= g_flowUnsaved.commsLargeVars[paramCommsLargeVarsArrayPos].iValue4
		enumCharacterList eCharFrom 	= INT_TO_ENUM(enumCharacterList, g_flowUnsaved.commsLargeVars[paramCommsLargeVarsArrayPos].iValue5)
		CC_CommunicationType eType 		= INT_TO_ENUM(CC_CommunicationType, g_flowUnsaved.commsLargeVars[paramCommsLargeVarsArrayPos].iValue6)
		INT iQueueTime 					= g_flowUnsaved.commsLargeVars[paramCommsLargeVarsArrayPos].iValue7
		INT iRequeueTime 				= g_flowUnsaved.commsLargeVars[paramCommsLargeVarsArrayPos].iValue8
		
		REGISTER_BRANCHED_CALL_FROM_CHARACTER_TO_PLAYER(eCallID,
														eCallTrueBranchID,
														eCallFalseBranchID,
														eBranchCheck,
														eType,
														iToPlayerBitset,
														eCharFrom,
														3,
														iQueueTime,
														iRequeueTime,
														g_flowUnsaved.commsLargeVars[paramCommsLargeVarsArrayPos].eVectorID,
														g_flowUnsaved.commsLargeVars[paramCommsLargeVarsArrayPos].eCodeID,
														iCommSettings)
	ENDIF
	
	//Do we need to wait for this call to complete?
	IF IS_BIT_SET(iFlowCommsBitset, FLOW_COMM_BIT_WAIT_UNTIL_SENT)
		RETURN STAY_ON_THIS_COMMAND
	ENDIF
	RETURN NEXT_SEQUENTIAL_COMMAND
ENDFUNC


// PURPOSE: Setup an incoming phonecall to a set of the player characters and jump to a label in the strand based on if it 
// 			is answered or not.
//
// INPUT PARAMS:			paramCommsVarsArrayPos		Index into the Comms Variables array
// RETURN VALUE:			JUMP_LABEL_IDS				A jump label - should be STAY_ON_THIS_COMMAND until ready to move on

FUNC JUMP_LABEL_IDS PERFORM_PHONE_PLAYER_WITH_JUMPS(INT paramCommsLargeVarsArrayPos)
	// Ensure the Comms Large Vars Array position is legal
	IF (paramCommsLargeVarsArrayPos = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_PHONE_PLAYER_WITH_JUMPS: The Comms Large Variables Array Position parameter is illegal. MOVING ON TO NEXT COMMAND.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	//Get the Comm ID for this phonecall.
	CC_CommID eCallID = INT_TO_ENUM(CC_CommID, g_flowUnsaved.commsLargeVars[paramCommsLargeVarsArrayPos].iValue1)
	
	//Do we want to automatically skip this command?
	#IF IS_DEBUG_BUILD
		IF g_bDebugFlowSilencePhonecalls
			IF IS_COMMUNICATION_REGISTERED(eCallID)
				CANCEL_COMMUNICATION(eCallID)
			ENDIF
			PRIVATE_CLEAR_LAST_CALL_DATA()
			RETURN g_flowUnsaved.commsLargeVars[paramCommsLargeVarsArrayPos].eJump3
		ENDIF
	#ENDIF
	
	//Check if the call has completed yet.
	IF GET_LAST_COMPLETED_CALL() = eCallID
		IF GET_LAST_CALL_RESPONSE()
			PRIVATE_CLEAR_LAST_CALL_DATA()
			RETURN g_flowUnsaved.commsLargeVars[paramCommsLargeVarsArrayPos].eJump1
		ELSE
			PRIVATE_CLEAR_LAST_CALL_DATA()
			RETURN g_flowUnsaved.commsLargeVars[paramCommsLargeVarsArrayPos].eJump2
		ENDIF
	ENDIF
											
	//Register the phonecall with the communication controller if it isn't already.
	IF NOT IS_COMMUNICATION_REGISTERED(eCallID)
	
		INT iToPlayerBitset 		= g_flowUnsaved.commsLargeVars[paramCommsLargeVarsArrayPos].iValue2
		enumCharacterList eCharFrom = INT_TO_ENUM(enumCharacterList, g_flowUnsaved.commsLargeVars[paramCommsLargeVarsArrayPos].iValue3)
		CC_CommunicationType eType	= INT_TO_ENUM(CC_CommunicationType, g_flowUnsaved.commsLargeVars[paramCommsLargeVarsArrayPos].iValue4)
		INT iQueueTime 				= g_flowUnsaved.commsLargeVars[paramCommsLargeVarsArrayPos].iValue5
		INT iRequeueTime 			= g_flowUnsaved.commsLargeVars[paramCommsLargeVarsArrayPos].iValue6
		INT iFlowCommsBitset 		= g_flowUnsaved.commsLargeVars[paramCommsLargeVarsArrayPos].iValue7
		INT iCommSettings 			= BUILD_COMMUNICATION_SETTINGS(iFlowCommsBitset)
	
		REGISTER_CALL_FROM_CHARACTER_TO_PLAYER(	eCallID,
												eType,
												iToPlayerBitset,
												eCharFrom,
												3,
												iQueueTime,
												iRequeueTime,
												COMM_NONE,
												g_flowUnsaved.commsLargeVars[paramCommsLargeVarsArrayPos].eVectorID,
												g_flowUnsaved.commsLargeVars[paramCommsLargeVarsArrayPos].eCodeID,
												g_flowUnsaved.commsLargeVars[paramCommsLargeVarsArrayPos].eCheckID,
												iCommSettings)
	ENDIF
	
	//Call not completed yet. Stay on this command until it is.
	RETURN STAY_ON_THIS_COMMAND
ENDFUNC


// PURPOSE: Setup an incoming phonecall with a question to a set of the player characters. Jump to a label in the strand based
// 			on their response to the question.
//
// INPUT PARAMS:			paramCommsLargeVarsArrayPos		Index into the Comms Large Variables array.
// RETURN VALUE:			JUMP_LABEL_IDS					A jump label - should be STAY_ON_THIS_COMMAND until ready to move on.

FUNC JUMP_LABEL_IDS PERFORM_PHONE_PLAYER_YES_NO(INT paramCommsLargeVarsArrayPos)
	// Ensure the Comms Large Vars Array position is legal
	IF (paramCommsLargeVarsArrayPos = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_PHONE_PLAYER_YES_NO: The Comms Large Variables Array Position parameter is illegal. MOVING ON TO NEXT COMMAND.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	//Get the Comm ID for the base conversation.
	CC_CommID eCallID = INT_TO_ENUM(CC_CommID, g_flowUnsaved.commsLargeVars[paramCommsLargeVarsArrayPos].iValue1)
	
	//Do we want to automatically skip this command?
	#IF IS_DEBUG_BUILD
		IF g_bDebugFlowSilencePhonecalls
			IF IS_COMMUNICATION_REGISTERED(eCallID)
				CANCEL_COMMUNICATION(eCallID)
			ENDIF
			PRIVATE_CLEAR_LAST_CALL_DATA()
			RETURN g_flowUnsaved.commsLargeVars[paramCommsLargeVarsArrayPos].eJump1
		ENDIF
	#ENDIF
	
	INT iFlowCommsBitset = g_flowUnsaved.commsLargeVars[paramCommsLargeVarsArrayPos].iValue9
	INT iCommSettings = BUILD_COMMUNICATION_SETTINGS(iFlowCommsBitset)
	
	//Check if the call has completed yet.
	IF IS_BIT_SET(iFlowCommsBitset, FLOW_COMM_BIT_WAIT_UNTIL_SENT)
		IF GET_LAST_COMPLETED_CALL() = eCallID
			//Call had question. Get response if one was made.
			BOOL bMissed = FALSE
			IF WAS_LAST_CALL_ANSWERED()
				IF DID_LAST_CALL_HAVE_RESPONSE()
					IF GET_LAST_CALL_RESPONSE()
						PRIVATE_CLEAR_LAST_CALL_DATA()
						RETURN g_flowUnsaved.commsLargeVars[paramCommsLargeVarsArrayPos].eJump1 //Yes response jump.
					ELSE
						PRIVATE_CLEAR_LAST_CALL_DATA()
						RETURN g_flowUnsaved.commsLargeVars[paramCommsLargeVarsArrayPos].eJump2 //No response jump.
					ENDIF
				ELSE 
					bMissed = TRUE
				ENDIF
			ELSE
				bMissed = TRUE
			ENDIF
			
			IF bMissed 
			AND NOT IS_BIT_SET(iCommSettings, COMM_BIT_CALL_REQUEUE_ON_MISS) 
				//Call was missed and is not set to be requeued.
				//Treat this as if the "No" response was chosen.
				PRIVATE_CLEAR_LAST_CALL_DATA()
				RETURN g_flowUnsaved.commsLargeVars[paramCommsLargeVarsArrayPos].eJump2
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_COMMUNICATION_REGISTERED(eCallID)

		CC_QuestionCallResponse eYesResponse 	= INT_TO_ENUM(CC_QuestionCallResponse, g_flowUnsaved.commsLargeVars[paramCommsLargeVarsArrayPos].iValue2)
		CC_QuestionCallResponse eNoResponse 	= INT_TO_ENUM(CC_QuestionCallResponse, g_flowUnsaved.commsLargeVars[paramCommsLargeVarsArrayPos].iValue3)
		INT iToPlayerBitset 					= g_flowUnsaved.commsLargeVars[paramCommsLargeVarsArrayPos].iValue4
		enumCharacterList eCharFrom				= INT_TO_ENUM(enumCharacterList, g_flowUnsaved.commsLargeVars[paramCommsLargeVarsArrayPos].iValue5)
		CC_CommunicationType eType 				= INT_TO_ENUM(CC_CommunicationType, g_flowUnsaved.commsLargeVars[paramCommsLargeVarsArrayPos].iValue6)
		INT iQueueTime 							= g_flowUnsaved.commsLargeVars[paramCommsLargeVarsArrayPos].iValue7
		INT iRequeueTime						= g_flowUnsaved.commsLargeVars[paramCommsLargeVarsArrayPos].iValue8

		REGISTER_CALL_WITH_QUESTION_FROM_CHARACTER_TO_PLAYER(	eCallID,
																eYesResponse,
																eNoResponse,
																eType,
																iToPlayerBitset,
																eCharFrom,
																3,
																iQueueTime,
																iRequeueTime,
																COMM_NONE,
																g_flowUnsaved.commsLargeVars[paramCommsLargeVarsArrayPos].eVectorID,
																g_flowUnsaved.commsLargeVars[paramCommsLargeVarsArrayPos].eCodeID,
																g_flowUnsaved.commsLargeVars[paramCommsLargeVarsArrayPos].eCheckID,
																iCommSettings)
	ENDIF
	
	RETURN STAY_ON_THIS_COMMAND
ENDFUNC


// PURPOSE:	Sends a text message to the player.
//
// INPUT PARAMS:			paramCommsVarsArrayPos		Index into the Comms Variables array
// RETURN VALUE:			JUMP_LABEL_IDS				A jump label - should be STAY_ON_THIS_COMMAND until ready to move on

FUNC JUMP_LABEL_IDS PERFORM_TEXT_PLAYER(INT paramCommsVarsArrayPos)
	// Ensure the Comms Vars Array position is legal
	IF (paramCommsVarsArrayPos = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_TEXT_PLAYER: The Comms Variables Array Position parameter is illegal. MOVING ON TO NEXT COMMAND.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	//Get the CommID for this text message.
	CC_CommID eTextID = INT_TO_ENUM(CC_CommID, g_flowUnsaved.commsVars[paramCommsVarsArrayPos].iValue1)
	
	//Has the text message just been sent?
	IF GET_LAST_COMPLETED_TEXT_MESSAGE() = eTextID
		//Text message has been sent, we're done.
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	INT iFlowCommsSettings = g_flowUnsaved.commsVars[paramCommsVarsArrayPos].iValue7
	
	//Register the phonecall with the communication controller if it isn't already.
	IF NOT IS_COMMUNICATION_REGISTERED(eTextID)
	
		INT iToPlayerBitset 		= g_flowUnsaved.commsVars[paramCommsVarsArrayPos].iValue2
		enumCharacterList eCharFrom = INT_TO_ENUM(enumCharacterList, g_flowUnsaved.commsVars[paramCommsVarsArrayPos].iValue3)
		CC_CommunicationType eType	= INT_TO_ENUM(CC_CommunicationType, g_flowUnsaved.commsVars[paramCommsVarsArrayPos].iValue4)
		INT iQueueTime				= g_flowUnsaved.commsVars[paramCommsVarsArrayPos].iValue5
		INT iRequeueTime 			= g_flowUnsaved.commsVars[paramCommsVarsArrayPos].iValue6
		
		//Build settings for this text message.
		INT iCommSettings = 0
		IF IS_BIT_SET(iFlowCommsSettings, FLOW_COMM_BIT_TODS_SKIP_TIMER)
			SET_BIT(iCommSettings, COMM_BIT_TODS_SKIP_TIMER)
		ENDIF
		IF IS_BIT_SET(iFlowCommsSettings, FLOW_COMM_BIT_HAS_QUESTION)
			SET_BIT(iCommSettings, COMM_BIT_HAS_QUESTION)
		ENDIF
		IF IS_BIT_SET(iFlowCommsSettings, FLOW_COMM_BIT_UNLOCKS_MISSION)
			SET_BIT(iCommSettings, COMM_BIT_UNLOCKS_MISSION)
		ELIF IS_BIT_SET(iFlowCommsSettings, FLOW_COMM_BIT_TRIGGERS_MISSION)
			SET_BIT(iCommSettings, COMM_BIT_TRIGGERS_MISSION)
		ENDIF
		IF IS_BIT_SET(iFlowCommsSettings, FLOW_COMM_BIT_NONCRIT_TXTMSG)
			SET_BIT(iCommSettings, COMM_BIT_TXTMSG_NOT_CRITICAL)
		ENDIF
		IF IS_BIT_SET(iFlowCommsSettings, FLOW_COMM_BIT_LOCKED_TXTMSG)
			SET_BIT(iCommSettings, COMM_BIT_TXTMSG_LOCKED)
		ENDIF
		IF IS_BIT_SET(iFlowCommsSettings, FLOW_COMM_BIT_NO_SILENT_TXTMSG)
			SET_BIT(iCommSettings, COMM_BIT_TXTMSG_NO_SILENT)
		ENDIF
		IF IS_BIT_SET(iFlowCommsSettings, FLOW_COMM_BIT_FORCE_SILENT_TXTMSG)
			SET_BIT(iCommSettings, COMM_BIT_TXTMSG_FORCE_SILENT)
		ENDIF
		
		enumTxtMsgCanCallSender eCallBackCheck
		IF IS_BIT_SET(iFlowCommsSettings, FLOW_COMM_BIT_CANT_CALL_BACK)
			eCallBackCheck = CANNOT_CALL_SENDER
		ELSE
			eCallBackCheck = CAN_CALL_SENDER
		ENDIF
	
		REGISTER_TEXT_MESSAGE_FROM_CHARACTER_TO_PLAYER(	eTextID,
														eType,
														iToPlayerBitset,
														eCharFrom,
														iQueueTime,
														iRequeueTime,
														g_flowUnsaved.commsVars[paramCommsVarsArrayPos].eVectorID,
														g_flowUnsaved.commsVars[paramCommsVarsArrayPos].eCodeID,
														g_flowUnsaved.commsVars[paramCommsVarsArrayPos].eCheckID,
														iCommSettings,
														eCallBackCheck)
	ENDIF
	
	// Allow flow to continue if the WAIT_UNTIL_SENT bit isn't set.
	IF IS_BIT_SET(iFlowCommsSettings, FLOW_COMM_BIT_WAIT_UNTIL_SENT)
		RETURN STAY_ON_THIS_COMMAND
	ENDIF
	RETURN NEXT_SEQUENTIAL_COMMAND
ENDFUNC


// PURPOSE:	Sends an email to the player.
//
// INPUT PARAMS:			paramCommsVarsArrayPos		Index into the Comms Variables array
// RETURN VALUE:			JUMP_LABEL_IDS				A jump label - should be STAY_ON_THIS_COMMAND until ready to move on

FUNC JUMP_LABEL_IDS PERFORM_EMAIL_PLAYER(INT paramCommsVarsArrayPos)
	// Ensure the Comms Vars Array position is legal
	IF (paramCommsVarsArrayPos = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_EMAIL_PLAYER: The Comms Variables Array Position parameter is illegal. MOVING ON TO NEXT COMMAND.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	//Get the CommID for this email.
	CC_CommID eEmailID = INT_TO_ENUM(CC_CommID, g_flowUnsaved.commsVars[paramCommsVarsArrayPos].iValue1)
	
	//Has the email been sent?
	IF GET_LAST_COMPLETED_EMAIL() = eEmailID
		//Email has been sent, we're done.
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF

	INT iFlowCommsSettings = g_flowUnsaved.commsVars[paramCommsVarsArrayPos].iValue7
													
	//Register the email with the communication controller if it isn't already.
	IF NOT IS_COMMUNICATION_REGISTERED(eEmailID)
	
		INT iToPlayerBitset 		= g_flowUnsaved.commsVars[paramCommsVarsArrayPos].iValue2
		enumCharacterList eCharFrom = INT_TO_ENUM(enumCharacterList, g_flowUnsaved.commsVars[paramCommsVarsArrayPos].iValue3)
		CC_CommunicationType eType 	= INT_TO_ENUM(CC_CommunicationType, g_flowUnsaved.commsVars[paramCommsVarsArrayPos].iValue4)
		INT iQueueTime 				= g_flowUnsaved.commsVars[paramCommsVarsArrayPos].iValue5
		INT iRequeueTime 			= g_flowUnsaved.commsVars[paramCommsVarsArrayPos].iValue6
	
		//Build settings for this communication.
		INT iCommSettings = 0
		IF IS_BIT_SET(iFlowCommsSettings, FLOW_COMM_BIT_TODS_SKIP_TIMER)
			SET_BIT(iCommSettings, COMM_BIT_TODS_SKIP_TIMER)
		ENDIF
		IF IS_BIT_SET(iFlowCommsSettings, FLOW_COMM_BIT_UNLOCKS_MISSION)
			SET_BIT(iCommSettings, COMM_BIT_UNLOCKS_MISSION)
		ELIF IS_BIT_SET(iFlowCommsSettings, FLOW_COMM_BIT_TRIGGERS_MISSION)
			SET_BIT(iCommSettings, COMM_BIT_TRIGGERS_MISSION)
		ENDIF
	
		REGISTER_EMAIL_FROM_CHARACTER_TO_PLAYER(eEmailID,
												eType,
												iToPlayerBitset,
												eCharFrom,
												iQueueTime,
												iRequeueTime,
												g_flowUnsaved.commsVars[paramCommsVarsArrayPos].eVectorID,
												g_flowUnsaved.commsVars[paramCommsVarsArrayPos].eCodeID,
												g_flowUnsaved.commsVars[paramCommsVarsArrayPos].eCheckID,
												iCommSettings)
	ENDIF
	
	// Allow flow to continue if the WAIT_UNTIL_SENT bit isn't set.
	IF IS_BIT_SET(iFlowCommsSettings, FLOW_COMM_BIT_WAIT_UNTIL_SENT)
		RETURN STAY_ON_THIS_COMMAND
	ENDIF
	RETURN NEXT_SEQUENTIAL_COMMAND
ENDFUNC


// PURPOSE:	Cancels an unsent phonecall.
//
// INPUT PARAMS:			paramIndex						Index that directly stores the call ID we want to cancel.
// RETURN VALUE:			JUMP_LABEL_IDS					A jump label - this command should move on after processing

FUNC JUMP_LABEL_IDS PERFORM_CANCEL_PHONECALL(INT paramIndex)
	#IF IS_DEBUG_BUILD
		IF IS_FLOW_CONSOLE_LOG_READY_TO_PRINT()
			CDEBUG1LN(DEBUG_FLOW_COMMANDS, "Cancel Phonecall")
		ENDIF
	#ENDIF

	//NB. The communication ID is stored directly in the index.
	//Cancel the unsent phonecall.
	CANCEL_COMMUNICATION(INT_TO_ENUM(CC_CommID, paramIndex))

	// Move on to next command
	RETURN NEXT_SEQUENTIAL_COMMAND
ENDFUNC


// PURPOSE:	Cancels an unsent phonecall.
//
// INPUT PARAMS:			paramIndex						Index that directly stores the call ID we want to cancel.
// RETURN VALUE:			JUMP_LABEL_IDS					A jump label - this command should move on after processing

FUNC JUMP_LABEL_IDS PERFORM_CANCEL_TEXT(INT paramIndex)
	#IF IS_DEBUG_BUILD
		IF IS_FLOW_CONSOLE_LOG_READY_TO_PRINT()
			CDEBUG1LN(DEBUG_FLOW_COMMANDS, "Cancel Text")
		ENDIF
	#ENDIF

	//NB. The communication ID is stored directly in the index.
	//Cancel the unsent phonecall.
	CANCEL_COMMUNICATION(INT_TO_ENUM(CC_CommID, paramIndex))

	// Move on to next command
	RETURN NEXT_SEQUENTIAL_COMMAND
ENDFUNC


// PURPOSE:	Setup a chat phonecall for a specific NPC character.
//
// INPUT PARAMS:			paramCoreVarsArrayPos		Index into the Core Variables array
// RETURN VALUE:			JUMP_LABEL_IDS				A jump label - should be STAY_ON_THIS_COMMAND until ready to move on

FUNC JUMP_LABEL_IDS PERFORM_SET_CHAR_CHAT_PHONECALL(INT paramCoreVarsArrayPos)
	// Ensure the Phonecall Vars Array position is legal
	IF (paramCoreVarsArrayPos = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_SET_CHAR_CHAT_PHONECALL: The Core Variables Array Position parameter is illegal. MOVING ON TO NEXT COMMAND.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
							
	CC_CommID eChatCallID		= INT_TO_ENUM(CC_CommID, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue1)
	INT iToPlayerBitset			= g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue2
	enumCharacterList eCharTo	= INT_TO_ENUM(enumCharacterList, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue3)
	FLOW_CHECK_IDS eSendCheck 	= INT_TO_ENUM(FLOW_CHECK_IDS, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue4)
													
	// Register the chat call with the communication controller.
	REGISTER_CHAT_CALL_FROM_PLAYER_TO_CHARACTER(eChatCallID,
												iToPlayerBitset,
												eCharTo,
												3,
												3600000,
												eSendCheck)
	
	// We're done. Step to the next command.
	RETURN NEXT_SEQUENTIAL_COMMAND
	
ENDFUNC


// PURPOSE:	Send the next email in a static email thread.
//
// INPUT PARAMS:			paramIndex						Index that directly stores the email thead ID to be progressed.
// RETURN VALUE:			JUMP_LABEL_IDS					A jump label - this command should move on after processing.

FUNC JUMP_LABEL_IDS PERFORM_PROGRESS_STATIC_EMAIL_THREAD(INT paramIndex)
	#IF IS_DEBUG_BUILD
		IF IS_FLOW_CONSOLE_LOG_READY_TO_PRINT()
			CDEBUG1LN(DEBUG_FLOW_COMMANDS, "Progress Static Email Thread")
		ENDIF
	#ENDIF
	
	EMAIL_THREAD_ENUMS eEmailThread = INT_TO_ENUM(EMAIL_THREAD_ENUMS, paramIndex)

	IF IS_STATIC_EMAIL_THREAD_ACTIVE(eEmailThread)
		IF NOT IS_STATIC_EMAIL_THREAD_ENDED(eEmailThread)
			PROGRESS_EMAIL_THREAD(paramIndex)
		ELSE
			SCRIPT_ASSERT("PERFORM_PROGRESS_STATIC_EMAIL_THREAD: Tried to progress a static email thead that had ended. Bug BenR.")
		ENDIF
	ELSE
		BEGIN_EMAIL_THREAD_INT(paramIndex)
	ENDIF

	// Move on to next command
	RETURN NEXT_SEQUENTIAL_COMMAND
ENDFUNC


// ===========================================================================================================
//		Friend commands
// ===========================================================================================================

// PURPOSE:	Makes a Friend call to the player.
//
// INPUT PARAMS:			paramCoreSmallVarsArrayPos		Index into the Core Small Variables array
// RETURN VALUE:			JUMP_LABEL_IDS					A jump label - should be STAY_ON_THIS_COMMAND until ready to move on

FUNC JUMP_LABEL_IDS PERFORM_ADD_NEW_FRIEND_CONTACT(INT paramCoreSmallVarsArrayPos)

	// Ensure the Friendcall Vars Array position is legal
	IF (paramCoreSmallVarsArrayPos = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW_COMMANDS,  "Perform_Add_New_Friend_Contact: The Core Small Variables Array Position parameter is illegal. MOVING ON TO NEXT COMMAND.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	//Add the friend for the player char.
	Add_Char_As_Char_Friend(INT_TO_ENUM(enumCharacterList, g_flowUnsaved.coreSmallVars[paramCoreSmallVarsArrayPos].iValue1),
							INT_TO_ENUM(enumCharacterList, g_flowUnsaved.coreSmallVars[paramCoreSmallVarsArrayPos].iValue2))
							
	//Simply step to the next command.
	RETURN NEXT_SEQUENTIAL_COMMAND

ENDFUNC

// PURPOSE:	Makes a Friend call to the player.
//
// INPUT PARAMS:			paramCoreVarsArrayPos		Index into the Core Variables array
// RETURN VALUE:			JUMP_LABEL_IDS				A jump label - should be STAY_ON_THIS_COMMAND until ready to move on

FUNC JUMP_LABEL_IDS PERFORM_BLOCK_FRIEND_CONTACT(INT paramCoreVarsArrayPos)

	// Ensure the Core Vars Array position is legal.
	IF (paramCoreVarsArrayPos = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW_COMMANDS, "Perform_Block_Friend_Contact: The Core Variables Array Position parameter is illegal. MOVING ON TO NEXT COMMAND.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	enumCharacterList eFriendOwner = INT_TO_ENUM(enumCharacterList, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue1)
	enumCharacterList eFriend = INT_TO_ENUM(enumCharacterList, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue2)
	
	BOOL bBlock = FALSE
	IF g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue3 = 1
		bBlock = TRUE
	ENDIF
	
	IF bBlock
		//Block the friend for the player char.
		IF IS_FRIEND_BLOCK_FLAG_SET(eFriendOwner,
									eFriend,
									FRIEND_BLOCK_FLAG_HIATUS)
				
			CPRINTLN(DEBUG_FLOW, "...contact already blocked by hiatus???")
			RETURN NEXT_SEQUENTIAL_COMMAND
		ENDIF
		
		SET_FRIEND_BLOCK_FLAG(	eFriendOwner,
								eFriend,
								FRIEND_BLOCK_FLAG_HIATUS)
	ELSE
		//Unblock the friend for the player char.
		IF NOT IS_FRIEND_BLOCK_FLAG_SET(eFriendOwner,
										eFriend,
										FRIEND_BLOCK_FLAG_HIATUS)
									
			CPRINTLN(DEBUG_FLOW, "...contact not blocked by hiatus???")
			RETURN NEXT_SEQUENTIAL_COMMAND
		ENDIF

		CLEAR_FRIEND_BLOCK_FLAG(eFriendOwner,
								eFriend,
								FRIEND_BLOCK_FLAG_HIATUS)
	ENDIF
	
	//Simply step to the next command.
	RETURN NEXT_SEQUENTIAL_COMMAND

ENDFUNC


// PURPOSE:	Makes a Friend call to the player.
//
// INPUT PARAMS:			paramCoreVarsArrayPos		Index into the Core Variables array
// RETURN VALUE:			JUMP_LABEL_IDS				A jump label - should be STAY_ON_THIS_COMMAND until ready to move on

FUNC JUMP_LABEL_IDS PERFORM_BLOCK_FRIEND_CLASH(INT paramCoreVarsArrayPos)

	// Ensure the Core Vars Array position is legal
	IF (paramCoreVarsArrayPos = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW_COMMANDS, "Perform_Block_Friend_Clash: The Core Variables Array Position parameter is illegal. MOVING ON TO NEXT COMMAND.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	enumCharacterList eFriendOwner = INT_TO_ENUM(enumCharacterList, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue1)
	enumCharacterList eFriend = INT_TO_ENUM(enumCharacterList, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue2)
	
	BOOL bBlock = FALSE
	IF g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue3 = 1
		bBlock = TRUE
	ENDIF
	
	IF bBlock
		//Block the friend for the player char.
		IF IS_FRIEND_BLOCK_FLAG_SET(eFriendOwner,
									eFriend,
									FRIEND_BLOCK_FLAG_CLASH)
				
			CPRINTLN(DEBUG_FLOW, "...contact already blocked by clash???")
			RETURN NEXT_SEQUENTIAL_COMMAND
		ENDIF
		
		SET_FRIEND_BLOCK_FLAG(	eFriendOwner,
								eFriend,
								FRIEND_BLOCK_FLAG_CLASH)
	ELSE
		//Unblock the friend for the player char.
		IF NOT IS_FRIEND_BLOCK_FLAG_SET(eFriendOwner,
										eFriend,
										FRIEND_BLOCK_FLAG_CLASH)
									
			CPRINTLN(DEBUG_FLOW, "...contact not blocked by clash???")
			RETURN NEXT_SEQUENTIAL_COMMAND
		ENDIF

		CLEAR_FRIEND_BLOCK_FLAG(eFriendOwner,
								eFriend,
								FRIEND_BLOCK_FLAG_CLASH)
	ENDIF
	
	//Simply step to the next command.
	RETURN NEXT_SEQUENTIAL_COMMAND

ENDFUNC


// PURPOSE:	Makes a Friend call to the player.
//
// INPUT PARAMS:			paramCoreVarsArrayPos			Index into the Core Variables array
// RETURN VALUE:			JUMP_LABEL_IDS					A jump label - should be STAY_ON_THIS_COMMAND until ready to move on

FUNC JUMP_LABEL_IDS PERFORM_FRIEND_CONTACT_BUSY(INT paramCoreVarsArrayPos)

	// Ensure the Friendcall Vars Array position is legal
	IF (paramCoreVarsArrayPos = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW_COMMANDS, "Perform_Friend_Contact_Busy: The Core Variables Array Position parameter is illegal. MOVING ON TO NEXT COMMAND.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	//Set the friend for the player char as busy.
	SET_FRIEND_BLOCK_FLAG(	INT_TO_ENUM(enumCharacterList, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue1),
							INT_TO_ENUM(enumCharacterList, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue2),
							FRIEND_BLOCK_FLAG_MISSION,
							INT_TO_ENUM(SP_MISSIONS, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue3))

	//Simply step to the next command.
	RETURN NEXT_SEQUENTIAL_COMMAND

ENDFUNC

// PURPOSE:	Requests the script, waits for it to load, and launches it.
//
// INPUT PARAMS:			paramScriptVarsArrayPos			Index into the Script Variables array
// RETURN VALUE:			JUMP_LABEL_IDS					A jump label - should be STAY_ON_THIS_COMMAND until script successfully launched

FUNC JUMP_LABEL_IDS PERFORM_LAUNCH_SCRIPT_AND_FORGET(INT paramTextLargeVarsArrayPos)

	// Ensure the Script Vars Array position is legal
	IF (paramTextLargeVarsArrayPos = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_LAUNCH_SCRIPT_AND_FORGET: The Text Large Variables Array Position parameter is illegal. MOVING ON TO NEXT COMMAND.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	TEXT_LABEL_31 txtScriptName = g_flowUnsaved.textLargeVars[paramTextLargeVarsArrayPos].txtValue
	INT iStackSize 				= g_flowUnsaved.textLargeVars[paramTextLargeVarsArrayPos].iValue1
	
	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY(txtScriptName)) > 0
		CERRORLN(DEBUG_FLOW, "PERFORM_LAUNCH_SCRIPT_AND_FORGET: Tried to launch script ", txtScriptName, " but an instance was already running.")
		
		// RETURN CALL ADDED BY KENNETH
		// THIS IS TO FIX PT BUG 766364
		// THE GAMEFLOW REACHED THE END WHEN RUNNING A SMOKE TEST WHICH THEN TRIGGERED A FLOW RESET
		// THE CONTROLLER_TAXI SCRIPT WAS REGISTERED WITH THE RELAUNCH SYSTEM AND WAS STILL RUNNING WHEN THIS PROC TRIED TO RELAUNCH IT
		
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Request the script - always do this first so code knows that script is interested in this script
	REQUEST_SCRIPT(txtScriptName)
	
	// Has the script loaded?
	IF NOT (HAS_SCRIPT_LOADED(txtScriptName))
		// NOTE: Use PRINTSTRINGS for these so that this message always appears in the log
		CPRINTLN(DEBUG_FLOW, "......Waiting for Launch and Forget script to load: ", txtScriptName)
		
		RETURN STAY_ON_THIS_COMMAND
	ENDIF
	
	// Script has loaded, so launch it
	#IF IS_DEBUG_BUILD
		// Debug mode stores the threadID
		THREADID theThread = START_NEW_SCRIPT(txtScriptName, iStackSize)
		SET_SCRIPT_AS_NO_LONGER_NEEDED(txtScriptName)
	#ENDIF
	
	#IF IS_FINAL_BUILD
		// Release mode doesn't care about the threadID
		START_NEW_SCRIPT(txtScriptName, iStackSize)
		SET_SCRIPT_AS_NO_LONGER_NEEDED(txtScriptName)
	#ENDIF
		
	CPRINTLN(DEBUG_FLOW, "......Launch and Forget script launched: ", txtScriptName)
	
	// Store the filename in the Launch And Forget Display Globals Array
	#IF IS_DEBUG_BUILD
		STORE_DEBUG_DISPLAY_LAUNCH_AND_FORGET_FILENAME(txtScriptName, theThread)
	#ENDIF
		
	RETURN NEXT_SEQUENTIAL_COMMAND

ENDFUNC


// PURPOSE:	Blocks or unblocks motorbikes spawning in general vehicle population.
//
// INPUT PARAMS:			paramIndex			Direct index into the data.
// RETURN VALUE:			JUMP_LABEL_IDS		A jump label - should be STAY_ON_THIS_COMMAND until script successfully launched

FUNC JUMP_LABEL_IDS PERFORM_SET_BIKES_SUPRESSED_STATE(INT paramIndex)

	//Convert index back to BOOL.
	BOOL bState
	IF paramIndex = 0
		bState = FALSE
	ELIF paramIndex = 1
		bState = TRUE
	ELSE
		SCRIPT_ASSERT("PERFORM_SET_BIKES_SUPRESSED_STATE: Direct index value was invalid. Cannot be converted to a boolean.")
	ENDIF
	
	SUPPRESS_MOTORBIKES(bState)
	
	RETURN NEXT_SEQUENTIAL_COMMAND
ENDFUNC

/// PURPOSE:
///    Sets whether we're going to be tracking Franklin's outfit changes.
///    If we're activating it also saves his current outfit so we can compare against it.
/// PARAMS:
///    paramIndex - Direct index into the data.
/// RETURNS:
///    A jump label. Will be NEXT_SEQUENTIAL_COMMAND
FUNC JUMP_LABEL_IDS PERFORM_SET_FRANKLIN_OUTFIT_BITSET_STATE(INT paramIndex)

	CPRINTLN(DEBUG_FLOW, "CALLED PERFORM_SET_FRANKLIN_OUTFIT_BITSET_STATE. State = ", paramIndex)
	
	IF paramIndex = 0
		#if not USE_CLF_DLC
		#if not USE_NRM_DLC
			// de-activating
			Set_Mission_Flow_Bitset_Bit_State(FLOWBITSET_FRANKLIN_CHANGED_OUTFIT, BITS_CHARACTER_REQUEST_ACTIVE, FALSE)
		#endif
		#endif		
	ELIF paramIndex = 1
		// Activating
		
		//Record state of Franklin's clothes when Lamar asks him to change them + flag request as active.
		//Save Franklin's outfit to a global int.
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
		AND NOT IS_PED_INJURED(PLAYER_PED_ID())
			g_savedGlobals.sPlayerData.iFranklinOriginalOutfitID = GET_CORE_OUTFIT_ID_FROM_PED(PLAYER_PED_ID())
		ELSE
			PED_COMP_OUTFIT_DATA_STRUCT sDefaultOutfit = GET_PED_COMPONENT_DATA_FOR_OUTFIT(PLAYER_ONE, OUTFIT_DEFAULT)
			CPRINTLN(DEBUG_FLOW, "Player isn't Franklin, or is dead. Store Franklin's default outfit.  Torso = ", sDefaultOutfit.eItems[COMP_TYPE_TORSO], "Legs = ", sDefaultOutfit.eItems[COMP_TYPE_LEGS])
			g_savedGlobals.sPlayerData.iFranklinOriginalOutfitID = GET_CORE_OUTFIT_ID_FROM_COMPONENTS(sDefaultOutfit.eItems[COMP_TYPE_TORSO], sDefaultOutfit.eItems[COMP_TYPE_LEGS])
		ENDIF
		CPRINTLN(DEBUG_FLOW, "UPDATED SAVED OUTFIT TO: ", g_savedGlobals.sPlayerData.iFranklinOriginalOutfitID)

		//Activate “change your clothes” request.
		#if not USE_CLF_DLC
		#if not USE_NRM_DLC
			Set_Mission_Flow_Bitset_Bit_State(FLOWBITSET_FRANKLIN_CHANGED_OUTFIT, BITS_CHARACTER_REQUEST_ACTIVE, TRUE)
		#endif
		#endif	
	ELSE
		SCRIPT_ASSERT("PERFORM_SET_FRANKLIN_OUTFIT_BITSET_STATE: Direct index value was invalid. Cannot be converted to a boolean.")
	ENDIF
	
	RETURN NEXT_SEQUENTIAL_COMMAND
ENDFUNC


/// PURPOSE:
///    
/// PARAMS:
/// RETURNS:
///    
FUNC JUMP_LABEL_IDS PERFORM_WAIT_FOR_AUTOSAVE()
	// If an autosave is not rolling.
	IF IS_AUTOSAVE_REQUEST_IN_PROGRESS()
		RETURN STAY_ON_THIS_COMMAND
	ENDIF

	// Otherwise.
	RETURN NEXT_SEQUENTIAL_COMMAND
ENDFUNC



// ===========================================================================================================
//		RANDOM CHARACTER commands
// ===========================================================================================================

// PURPOSE:	Flag the activation BIT of the specified Random Character mission.
//
// INPUT PARAMS:		paramCoreVarsArrayPos		Index into the Miscellaneous Variables array
//
// NOTES:	This should generally be used to launch the first mission for a Random Character. For subsequent missions
//			for the same Random Character then generally a mission flow flag would get set to allow it to continue.
FUNC JUMP_LABEL_IDS PERFORM_ACTIVATE_RANDOMCHAR_MISSION(INT paramCoreVarsArrayPos)
	
	// Ensure the Miscellaneous Vars Array position is legal
	IF (paramCoreVarsArrayPos = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW_COMMANDS, "Perform_Activate_RandomChar_Mission: The Core Variables Array Position parameter is illegal. MOVING ON TO NEXT COMMAND.")
		
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Retrieve the Random Character Mission ID from the variables
	g_eRC_MissionIDs eRC_MissionID = INT_TO_ENUM(g_eRC_MissionIDs, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue1)
	
	// Set the mission as flow activated
	SET_BIT(g_savedGlobals.sRandomChars.savedRC[eRC_MissionID].rcFlags, ENUM_TO_INT(RC_FLAG_ACTIVATED_IN_FLOW))
	
	// Only set the activated flag if told to
	IF g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue2 = 1 // bFullActivated
		RANDOM_CHARACTER_ACTIVATE_MISSION(eRC_MissionID)
	ENDIF
	
	RETURN NEXT_SEQUENTIAL_COMMAND
	
ENDFUNC


// PURPOSE:	Unflag the activation BIT of the specified Random Character mission.
//
// INPUT PARAMS:		paramIndex		The enum data pointing to the RC mission we want to deactivate. 
//										Data stored directly in array index.
FUNC JUMP_LABEL_IDS PERFORM_DEACTIVATE_RANDOMCHAR_MISSION(INT paramIndex)
	
	// Retrieve the Random Character Mission ID from the variables
	g_eRC_MissionIDs eRC_MissionID = INT_TO_ENUM(g_eRC_MissionIDs, paramIndex) //NB. The enum data for this command was stored directly as the array index.
	
	// Unset the mission as flow activated.
	CLEAR_BIT(g_savedGlobals.sRandomChars.savedRC[eRC_MissionID].rcFlags, ENUM_TO_INT(RC_FLAG_ACTIVATED_IN_FLOW))

	RETURN NEXT_SEQUENTIAL_COMMAND
ENDFUNC


// ===========================================================================================================
//		WEAPON commands
// ===========================================================================================================

// PURPOSE:	Set the lock state of a weapon.
//
// INPUT PARAMS:		paramCoreSmallVarsArrayPos			Index into the Core Small Variables array
// RETURN VALUE:		JUMP_LABEL_IDS						The Jump Label to continue at
FUNC JUMP_LABEL_IDS PERFORM_SET_WEAPON_LOCK_STATE(INT paramCoreSmallVarsArrayPos)
	
	// Ensure the Miscellaneous Vars Array position is legal
	IF (paramCoreSmallVarsArrayPos = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW_COMMANDS, "Perform_Set_Weapon_Lock_State: The Core Small Variables Array Position parameter is illegal. MOVING ON TO NEXT COMMAND.")
		
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Get the weapon to be state changed
	WEAPON_TYPE theWeaponID = GET_WEAPONTYPE_FROM_WEAPON_BITSET(INT_TO_ENUM(WEAPON_BITSET, g_flowUnsaved.coreSmallVars[paramCoreSmallVarsArrayPos].iValue1))
	
	// Get the weapons lock state.
	BOOL newState = FALSE
	IF g_flowUnsaved.coreSmallVars[paramCoreSmallVarsArrayPos].iValue2 = 1
		newState = TRUE
	ENDIF
	
	// Set the weapons lock state.
	SET_PLAYER_PED_WEAPON_UNLOCKED(CHAR_MICHAEL, theWeaponID, newState)
	SET_PLAYER_PED_WEAPON_UNLOCKED(CHAR_TREVOR, theWeaponID, newState)
	SET_PLAYER_PED_WEAPON_UNLOCKED(CHAR_FRANKLIN, theWeaponID, newState)
	
	//If this command was called while debug launching then
	//also gives this weapon to all player characters as long as the gun shops
	//are unlocked in the flow.
	#IF IS_DEBUG_BUILD
		IF g_flowUnsaved.bUpdatingGameflow
			IF IS_SHOP_AVAILABLE(GUN_SHOP_01_DT) OR IS_SHOP_AVAILABLE(GUN_SHOP_03_HW)
				INT iCharIndex
				REPEAT 3 iCharIndex
					INT iWeaponSlot = GET_INT_FROM_PLAYER_PED_WEAPON_TYPE(theWeaponID)
					IF iWeaponSlot != -1
					#if USE_CLF_DLC
						g_savedGlobalsClifford.sPlayerData.sInfo.sWeapons[iCharIndex].sWeaponInfo[iWeaponSlot].iAmmoCount = 100
						g_savedGlobalsClifford.sPlayerData.sInfo.sWeapons[iCharIndex].sWeaponInfo[iWeaponSlot].iModsAsBitfield = 0
						g_savedGlobalsClifford.sPlayerData.sInfo.sWeapons[iCharIndex].sWeaponInfo[iWeaponSlot].iTint = 0
						g_savedGlobalsClifford.sPlayerData.sInfo.sWeapons[iCharIndex].sWeaponInfo[iWeaponSlot].iCamo = 0
						g_savedGlobalsClifford.sPlayerData.sInfo.sWeapons[iCharIndex].sWeaponInfo[iWeaponSlot].eWeaponType = theWeaponID
					#endif
					#if USE_NRM_DLC
						g_savedGlobalsnorman.sPlayerData.sInfo.sWeapons[iCharIndex].sWeaponInfo[iWeaponSlot].iAmmoCount = 100
						g_savedGlobalsnorman.sPlayerData.sInfo.sWeapons[iCharIndex].sWeaponInfo[iWeaponSlot].iModsAsBitfield = 0
						g_savedGlobalsnorman.sPlayerData.sInfo.sWeapons[iCharIndex].sWeaponInfo[iWeaponSlot].iTint = 0
						g_savedGlobalsnorman.sPlayerData.sInfo.sWeapons[iCharIndex].sWeaponInfo[iWeaponSlot].iCamo = 0
						g_savedGlobalsnorman.sPlayerData.sInfo.sWeapons[iCharIndex].sWeaponInfo[iWeaponSlot].eWeaponType = theWeaponID
					#endif
					#if not USE_CLF_DLC
					#if not USE_NRM_DLC
						g_savedGlobals.sPlayerData.sInfo.sWeapons[iCharIndex].sWeaponInfo[iWeaponSlot].iAmmoCount = 100
						g_savedGlobals.sPlayerData.sInfo.sWeapons[iCharIndex].sWeaponInfo[iWeaponSlot].iModsAsBitfield = 0
						g_savedGlobals.sPlayerData.sInfo.sWeapons[iCharIndex].sWeaponInfo[iWeaponSlot].iTint = 0
						g_savedGlobals.sPlayerData.sInfo.sWeapons[iCharIndex].sWeaponInfo[iWeaponSlot].iCamo = 0
						g_savedGlobals.sPlayerData.sInfo.sWeapons[iCharIndex].sWeaponInfo[iWeaponSlot].eWeaponType = theWeaponID
					#endif
					#endif
					ENDIF
				ENDREPEAT
			ENDIF
		ENDIF
	#ENDIF
	
	RETURN NEXT_SEQUENTIAL_COMMAND
	
ENDFUNC


// ===========================================================================================================
//		BUYABLE VEHICLE commands
// ===========================================================================================================

// PURPOSE:	Set the purchased state of a vehicle.
//
// INPUT PARAMS:		paramCoreSmallVarsArrayPos		Index into the Core Small Variables array
// RETURN VALUE:		JUMP_LABEL_IDS					The Jump Label to continue at
FUNC JUMP_LABEL_IDS PERFORM_SET_BUYABLE_VEHICLE_STATE(INT paramCoreVarsArrayPos)
	
	// Ensure the Miscellaneous Vars Array position is legal
	IF (paramCoreVarsArrayPos = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW_COMMANDS, "Perform_Set_Buyable_Vehicle_State: The Core Variables Array Position parameter is illegal. MOVING ON TO NEXT COMMAND.")
		
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Get the vehicle to be state changed
	SITE_BUYABLE_VEHICLE theVehicleID = INT_TO_ENUM(SITE_BUYABLE_VEHICLE, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue1)
	enumCharacterList theCharacterID = INT_TO_ENUM(enumCharacterList, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue2)
	
	// Perform the command relavent to the vehicle purchased state.
	IF g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue3 = 1
		VEHICLE_SETUP_STRUCT vss
		DO_SP_VEHICLE_BUY(theVehicleID, theCharacterID, vss)
	ELSE
		// TODO:We will need to setup the proc which allows us to 'un-buy' a vehicle.
	ENDIF
	
	RETURN NEXT_SEQUENTIAL_COMMAND

ENDFUNC


// ===========================================================================================================
//		STATIC BLIP commands
// ===========================================================================================================

// PURPOSE:	Set the active state of a Static Blip.
//
// INPUT PARAMS:		paramCoreSmallVarsArrayPos		Index into the Core Small Variables array
// RETURN VALUE:		JUMP_LABEL_IDS					The Jump Label to continue at

FUNC JUMP_LABEL_IDS PERFORM_SET_STATIC_BLIP_STATE(INT paramCoreSmallVarsArrayPos)
	
	// Ensure the Miscellaneous Vars Array position is legal
	IF (paramCoreSmallVarsArrayPos = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW_COMMANDS, "Perform_Set_Static_Blip_State: The Core Small Variables Array Position parameter is illegal. MOVING ON TO NEXT COMMAND.")
		
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Get the static blip to be modified
	STATIC_BLIP_NAME_ENUM theBlipID = INT_TO_ENUM(STATIC_BLIP_NAME_ENUM, g_flowUnsaved.coreSmallVars[paramCoreSmallVarsArrayPos].iValue1)
	
	// Ensure the static blip ID is legal
	IF (theBlipID = STATIC_BLIP_NAME_DUMMY_FINAL)
		CERRORLN(DEBUG_FLOW_COMMANDS, "Perform_Set_Static_Blip_State: The static blip ID to have its state set is illegal. MOVING ON TO NEXT COMMAND.")
		
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Set the static blip's state.
	BOOL newState = FALSE
	IF g_flowUnsaved.coreSmallVars[paramCoreSmallVarsArrayPos].iValue2 = 1
		newState = TRUE
	ENDIF
	SET_STATIC_BLIP_ACTIVE_STATE(theBlipID, newState)
	
	RETURN NEXT_SEQUENTIAL_COMMAND
	
ENDFUNC


// PURPOSE:	Enable the long-range status of a Static Blip.
//
// INPUT PARAMS:		paramCoreSmallVarsArrayPos			Index into the Core Small Variables array
// RETURN VALUE:		JUMP_LABEL_IDS						The Jump Label to continue at

FUNC JUMP_LABEL_IDS PERFORM_ENABLE_BLIP_LONG_RANGE(INT paramCoreSmallVarsArrayPos)

	// Ensure the Core Vars Array position is legal
	IF (paramCoreSmallVarsArrayPos = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW_COMMANDS, "Perform_Enable_Blip_Long_Range: The Core Small Variables Array Position parameter is illegal. MOVING ON TO NEXT COMMAND.")
		
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Get the static blip to be modified
	STATIC_BLIP_NAME_ENUM theBlipID = INT_TO_ENUM(STATIC_BLIP_NAME_ENUM, g_flowUnsaved.coreSmallVars[paramCoreSmallVarsArrayPos].iValue1)
	
	// Ensure the static blip ID is legal
	IF (theBlipID = STATIC_BLIP_NAME_DUMMY_FINAL)
		CERRORLN(DEBUG_FLOW_COMMANDS, "Perform_Enable_Blip_Long_Range: The static blip ID to have its state set is illegal. MOVING ON TO NEXT COMMAND.")
		
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	//Get the on mission flag (stored in the general use INT).
	BOOL notDuringMissionFlag = FALSE
	IF g_flowUnsaved.coreSmallVars[paramCoreSmallVarsArrayPos].iValue2 = 1
		notDuringMissionFlag = TRUE
	ENDIF
	
	// Set the static blip's long-range state.
	SET_STATIC_BLIP_APPEAR_EDGE_RADAR(theBlipID, TRUE, notDuringMissionFlag)
	
	RETURN NEXT_SEQUENTIAL_COMMAND
	
ENDFUNC

// PURPOSE:	Disable the long-range status of a Static Blip.
//
// INPUT PARAMS:		paramCoreSmallVarsArrayPos	Index into the Core Small Variables array
// RETURN VALUE:		JUMP_LABEL_IDS				The Jump Label to continue at

FUNC JUMP_LABEL_IDS PERFORM_DISABLE_BLIP_LONG_RANGE(INT paramCoreSmallVarsArrayPos)
	
	// Ensure the Core Vars Array position is legal
	IF (paramCoreSmallVarsArrayPos = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW_COMMANDS, "Perform_Disable_Blip_Long_Range: The Core Small Variables Array Position parameter is illegal. MOVING ON TO NEXT COMMAND.")
		
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Get the static blip to be modified
	STATIC_BLIP_NAME_ENUM theBlipID = INT_TO_ENUM(STATIC_BLIP_NAME_ENUM, g_flowUnsaved.coreSmallVars[paramCoreSmallVarsArrayPos].iValue1)
	
	// Ensure the static blip ID is legal
	IF (theBlipID = STATIC_BLIP_NAME_DUMMY_FINAL)
		CERRORLN(DEBUG_FLOW_COMMANDS, "Perform_Disable_Blip_Long_Range: The static blip ID to have its state set is illegal. MOVING ON TO NEXT COMMAND.")
		
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	
	//Get the on mission flag (stored in the general use INT).
	BOOL notDuringMissionFlag = FALSE
	IF g_flowUnsaved.coreSmallVars[paramCoreSmallVarsArrayPos].iValue2 = 1
		notDuringMissionFlag = TRUE
	ENDIF
	
	// Set the static blip's long-range state.
	SET_STATIC_BLIP_APPEAR_EDGE_RADAR(theBlipID, FALSE, notDuringMissionFlag)
	
	RETURN NEXT_SEQUENTIAL_COMMAND
	
ENDFUNC



// PURPOSE:	Set the state of a building that can have its state swapping.
//
// INPUT PARAMS:		paramCoreSmallVarsArrayPos		Index into the Core Variables array.
// RETURN VALUE:		JUMP_LABEL_IDS					The Jump Label to continue at.

FUNC JUMP_LABEL_IDS PERFORM_SET_BUILDING_STATE(INT paramCoreVarsArrayPos)
	
	// Ensure the Miscellaneous Vars Array position is legal
	IF (paramCoreVarsArrayPos = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW_COMMANDS, "Perform_Set_Building_State: The Core Variables Array Position parameter is illegal. MOVING ON TO NEXT COMMAND.")
		
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Get the building ID that is to have its state changed.
	BUILDING_NAME_ENUM theBuildingID = INT_TO_ENUM(BUILDING_NAME_ENUM, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue1)

	// Get the state of the building ID.
	BUILDING_STATE_ENUM theState = INT_TO_ENUM(BUILDING_STATE_ENUM, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue2)
	
	BOOL bLeaveArea = FALSE
	IF g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue3 = 1
		bLeaveArea = TRUE
	ENDIF
	
	// Set the new state.
#IF IS_DEBUG_BUILD
	IF g_flowUnsaved.bUpdatingGameflow
		//Don't wait for the player to leave the area if we are debug gameflow launching.
		SET_BUILDING_STATE(theBuildingID, theState, FALSE)
	ELSE
#ENDIF
		SET_BUILDING_STATE(theBuildingID, theState, bLeaveArea)
#IF IS_DEBUG_BUILD
	ENDIF
#ENDIF
	
	// Move to next command.
	RETURN NEXT_SEQUENTIAL_COMMAND
	
ENDFUNC


// ===========================================================================================================
//		SWITCH CHARACTER commands
// ===========================================================================================================

// PURPOSE:	Perform a switch to another character.
//
// INPUT PARAMS:		paramIndex							The index the chacacter switch data is stored directly into.
// RETURN VALUE:		JUMP_LABEL_IDS						The Jump Label to continue at.
FUNC JUMP_LABEL_IDS PERFORM_DO_SWITCH_TO_CHARACTER(INT paramIndex)
	
	// Wait for any mission passed screen to clear
	IF g_bResultScreenDisplaying
		RETURN STAY_ON_THIS_COMMAND
	ENDIF
	
	// Get the character we want to switch to
	enumCharacterList theCharacterID = INT_TO_ENUM(enumCharacterList, paramIndex) //NB. The enum data for this command was stored directly as the array index.
	IF IS_PED_THE_CURRENT_PLAYER_PED(theCharacterID)
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Make the switch request.
	#if USE_CLF_DLC
		g_savedGlobalsClifford.sPlayerData.eOverridePed = theCharacterID
	#endif
	#if USE_NRM_DLC
		g_savedGlobalsnorman.sPlayerData.eOverridePed = theCharacterID
	#endif
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
		g_savedGlobals.sPlayerData.eOverridePed = theCharacterID
	#endif
	#endif
	MAKE_PLAYER_PED_SWITCH_REQUEST(theCharacterID)
	
	RETURN NEXT_SEQUENTIAL_COMMAND
	
ENDFUNC



// ===========================================================================================================
//		STRAND SAVE POINTER OVERRIDE commands
// ===========================================================================================================

// PURPOSE:	Set a save command pointer override for a specific strand in the gameflow.
//
// INPUT PARAMS:		paramStrandID			The strand to set the save override for.
// RETURN VALUE:		JUMP_LABEL_IDS			The Jump Label to continue at
FUNC JUMP_LABEL_IDS PERFORM_SET_STRAND_SAVE_OVERRIDE(INT paramCoreSmallVarsArrayPos, STRANDS paramStrandID)
	// Ensure the Core Small Vars Array position is legal
	IF (paramCoreSmallVarsArrayPos = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW_COMMANDS, "Perform_Display_Help_Text: The Core Small Variables Array Position parameter is illegal. MOVING ON TO NEXT COMMAND.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	

	SP_MISSIONS eMissionToUncomplete = INT_TO_ENUM(SP_MISSIONS, g_flowUnsaved.coreSmallVars[paramCoreSmallVarsArrayPos].iValue1)
	BOOL bMPSwitchOnly = FALSE
	IF g_flowUnsaved.coreSmallVars[paramCoreSmallVarsArrayPos].iValue2 = 1
		bMPSwitchOnly = TRUE
	ENDIF

	SET_STRAND_POINTER_OVERRIDE_TO_CURRENT_STRAND_POINTER(paramStrandID, eMissionToUncomplete, bMPSwitchOnly)
	RETURN NEXT_SEQUENTIAL_COMMAND
ENDFUNC

// PURPOSE:	Clear a save command pointer override for a specific strand in the gameflow.
//
// INPUT PARAMS:		paramStrandID			The strand to set the save override for.
// RETURN VALUE:		JUMP_LABEL_IDS			The Jump Label to continue at
FUNC JUMP_LABEL_IDS PERFORM_CLEAR_STRAND_SAVE_OVERRIDE(STRANDS paramStrandID)
	CLEAR_STRAND_POINTER_OVERRIDE_FOR_STRAND(paramStrandID)
	RETURN NEXT_SEQUENTIAL_COMMAND
ENDFUNC

// ===========================================================================================================
//		HELP TEXT commands
// ===========================================================================================================

// PURPOSE: Queues a help message to display to the player at the next opportune moment.
//
// INPUT PARAMS:			paramTextVarsArrayPos			Index into the Text Variables array
// RETURN VALUE:			JUMP_LABEL_IDS					A jump label - should be STAY_ON_THIS_COMMAND until ready to move on

FUNC JUMP_LABEL_IDS PERFORM_DISPLAY_HELP_TEXT(INT paramTextLargeVarsArrayPos)
	// Ensure the Text Large Vars Array position is legal
	IF (paramTextLargeVarsArrayPos = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW_COMMANDS, "Perform_Display_Help_Text: The Text Large Variables Array Position parameter is illegal. MOVING ON TO NEXT COMMAND.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	//Queue help message.
	IF NOT ARE_STRINGS_EQUAL(g_flowUnsaved.textLargeVars[paramTextLargeVarsArrayPos].txtValue, "")
		ADD_HELP_TO_FLOW_QUEUE(	g_flowUnsaved.textLargeVars[paramTextLargeVarsArrayPos].txtValue,
								INT_TO_ENUM(FlowHelpPriority, (g_flowUnsaved.textLargeVars[paramTextLargeVarsArrayPos].iValue1%FLOW_COMMAND_HASH_MODIFIER_MAGNITUDE)),
								0,
								FLOW_HELP_NEVER_EXPIRES,
								g_flowUnsaved.textLargeVars[paramTextLargeVarsArrayPos].iValue2,
								g_flowUnsaved.textLargeVars[paramTextLargeVarsArrayPos].iValue3,
								INT_TO_ENUM(CC_CodeID, g_flowUnsaved.textLargeVars[paramTextLargeVarsArrayPos].iValue4))
					
	ELSE
		CERRORLN(DEBUG_FLOW_COMMANDS, "Perform_Display_Help_Text: Could not perform the command, a blank text label was passed. Stepping to next command.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	RETURN NEXT_SEQUENTIAL_COMMAND
ENDFUNC


// PURPOSE:	Cancels a queued help message to the player.
//
// INPUT PARAMS:			paramTextVarsArrayPos			Index into the Script Variables array
// RETURN VALUE:			JUMP_LABEL_IDS					A jump label - should be STAY_ON_THIS_COMMAND until ready to move on

FUNC JUMP_LABEL_IDS PERFORM_CANCEL_HELP_TEXT(INT paramTextLargeVarsArrayPos)
	// Ensure the Text Vars Array position is legal
	IF (paramTextLargeVarsArrayPos = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW_COMMANDS, "Perform_Display_Help_Text: The Text Large Variables Array Position parameter is illegal. MOVING ON TO NEXT COMMAND.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	REMOVE_HELP_FROM_FLOW_QUEUE(g_flowUnsaved.textLargeVars[paramTextLargeVarsArrayPos].txtValue)
	
	RETURN NEXT_SEQUENTIAL_COMMAND
ENDFUNC


// ===========================================================================================================
//		CODE CONTROLLER commands
// ===========================================================================================================

// PURPOSE:	Directly executes a custom block of script bound to a Code ID. 
//
// INPUT PARAMS:		paramCoreSmallVarsArrayPos		Index into the Core Small Variables array.
// RETURN VALUE:		JUMP_LABEL_IDS					The Jump Label to continue at.

FUNC JUMP_LABEL_IDS PERFORM_RUN_CODE_ID(INT paramCoreSmallVarsArrayPos)
	// Ensure the Phonecall Vars Array position is legal
	IF (paramCoreSmallVarsArrayPos = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW_COMMANDS, "Perform_Run_Code_ID: The Core Small Variables Array Position parameter is illegal. MOVING ON TO NEXT COMMAND.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	//Get the code ID.
	CC_CodeID theCodeID = INT_TO_ENUM(CC_CodeID, g_flowUnsaved.coreSmallVars[paramCoreSmallVarsArrayPos].iValue1)
	#if USE_CLF_DLC
		//Check the code ID is valid.
		IF theCodeID = CID_BLANK OR theCodeID = CID_CLF_MAX
			CERRORLN(DEBUG_FLOW_COMMANDS, "Perform_Run_Code_ID AGT: The code ID passed was invalid. MOVING ON TO NEXT COMMAND.")
			RETURN NEXT_SEQUENTIAL_COMMAND
		ENDIF
	#endif
	#if USE_NRM_DLC
		//Check the code ID is valid.
		IF theCodeID = CID_BLANK OR theCodeID = CID_NRM_MAX
			CERRORLN(DEBUG_FLOW_COMMANDS, "Perform_Run_Code_ID NRM: The code ID passed was invalid. MOVING ON TO NEXT COMMAND.")
			RETURN NEXT_SEQUENTIAL_COMMAND
		ENDIF
	#endif
	
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
		//Check the code ID is valid.
		IF theCodeID = CID_BLANK OR theCodeID = CID_MAX
			CERRORLN(DEBUG_FLOW_COMMANDS, "Perform_Run_Code_ID: The code ID passed was invalid. MOVING ON TO NEXT COMMAND.")
			RETURN NEXT_SEQUENTIAL_COMMAND
		ENDIF
	#endif
	#endif
	
	
	//Get the delay time.
	INT theDelay = g_flowUnsaved.coreSmallVars[paramCoreSmallVarsArrayPos].iValue2
	
	//Check the delay time is valid.
	IF theDelay < 0
		CERRORLN(DEBUG_FLOW_COMMANDS, "Perform_Run_Code_ID: The delay time passed was invlaid. delay < 0. MOVING ON TO NEXT COMMAND.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	//Exceute the code ID.
	EXECUTE_CODE_ID(theCodeID, theDelay)
	
	RETURN NEXT_SEQUENTIAL_COMMAND
ENDFUNC


// ===========================================================================================================
//		PLANNING BOARD commands
// ===========================================================================================================

// PURPOSE:	Setup a heist controller to allow a certain planning board interaction. 
// 			Wait until this interaction is completed by the player before continuing to a new label.
//
// INPUT PARAMS:		paramCommsVarsArrayPos			Index into the Comms Variables array
// RETURN VALUE:		JUMP_LABEL_IDS					The Jump Label to continue at

FUNC JUMP_LABEL_IDS PERFORM_DO_BOARD_INTERACTION(INT paramCommsVarsArrayPos)
	#if USE_CLF_DLC
		unused_parameter(paramCommsVarsArrayPos)
	#endif
	#if USE_NRM_DLC
		unused_parameter(paramCommsVarsArrayPos)
	#endif
	//hesit boards only used in 
	#if not USE_CLF_DLC
	#IF NOT USE_NRM_DLC
	// Ensure the Comms Vars Array position is legal
	IF (paramCommsVarsArrayPos = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_DO_BOARD_INTERACTION: The Comms Variables Array Position parameter is illegal. MOVING ON TO NEXT COMMAND.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	//Check if this interaction is being told to back out.
	IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_BOARD_UNDO)
		SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_BOARD_UNDO, FALSE)
		RETURN INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.commsVars[paramCommsVarsArrayPos].iValue5)
	ENDIF

	// Get the heist ID and check it's valid.
	INT theHeist = g_flowUnsaved.commsVars[paramCommsVarsArrayPos].iValue1
	IF theHeist < 0 OR theHeist >= NO_HEISTS
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_DO_BOARD_INTERACTION: Invalid heist ID passed. MOVING ON TO NEXT COMMAND.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Get the interaction ID and check it's valid.
	g_eBoardInteractions theInteraction = INT_TO_ENUM(g_eBoardInteractions, g_flowUnsaved.commsVars[paramCommsVarsArrayPos].iValue2)
	IF theInteraction = INTERACT_MAX OR theInteraction = INTERACT_NONE
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_DO_BOARD_INTERACTION: Invalid interaction ID passed. MOVING ON TO NEXT COMMAND.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Get the display group ID and check it's valid.
	INT theDisplayGroup = ENUM_TO_INT(g_flowUnsaved.commsVars[paramCommsVarsArrayPos].iValue3)
	IF INT_TO_ENUM(g_eBoardDisplayGroups, theDisplayGroup) = PBDG_MAX
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_DO_BOARD_INTERACTION: Invalid display group ID passed. MOVING ON TO NEXT COMMAND.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Get the board mode flowint flag.
	FLOW_INT_IDS theBoardModeFlag = GET_HEIST_BOARD_MODE_FLOWINT_ID(theHeist)

	// Get the current board mode flag for this heist and check it's valid.
	INT iFlaggedBoardMode = GET_MISSION_FLOW_INT_VALUE(theBoardModeFlag)
	IF iFlaggedBoardMode < 0 OR iFlaggedBoardMode >= ENUM_TO_INT(PBM_INVALID)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_DO_BOARD_INTERACTION: The current board mode flagged for this heist is invald. MOVING ON TO NEXT COMMAND.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	//It's valid. Convert to an ENUM
	g_eBoardModes theFlaggedMode = INT_TO_ENUM(g_eBoardModes, iFlaggedBoardMode)
	
	INT iSettings = g_flowUnsaved.commsVars[paramCommsVarsArrayPos].iValue6
	
	// Get the board interaction flags.
	BOOL lockExit = IS_BIT_SET(iSettings, FLOW_BOARD_BIT_LOCK_EXIT)
	BOOL viewWhenDone = IS_BIT_SET(iSettings, FLOW_BOARD_BIT_VIEW_WHEN_DONE)
	
	// Which interaction is being triggered?
	SWITCH(theInteraction)
		
		//Manage point of interest overviews.
		CASE INTERACT_POI_OVERVIEW
			IF theFlaggedMode <> PBM_POI_OVERVIEW
				//The board has not been flagged to switch to crew select mode yet. Flag it.
				SET_MISSION_FLOW_INT_VALUE(theBoardModeFlag, ENUM_TO_INT(PBM_POI_OVERVIEW))

				//Make sure the crew selected bit is cleared as we initialise the interaction.
				SET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_HEIST_POI_OVERVIEW_DONE, theHeist, FALSE)
				
				//Set board interaction modifier flags as we initialise.
				SET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_HEIST_DISABLE_BOARD_EXIT, theHeist, lockExit)
			ELSE
				//The board is already flagged to switch to crew select mode.
				//Has the crew been selected yet?
				IF GET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_HEIST_POI_OVERVIEW_DONE, theHeist)
					//Yes...
					//Set the board mode flag to viewing mode if requested.
					IF viewWhenDone
						SET_MISSION_FLOW_INT_VALUE(theBoardModeFlag, ENUM_TO_INT(PBM_VIEW))
					ENDIF
					
					//Clean up the "Heist POI Overview Done" flag.
					SET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_HEIST_POI_OVERVIEW_DONE, theHeist, FALSE)
					
					//Turn on the display group linked to this interaction.
					IF theDisplayGroup <> ENUM_TO_INT(PBDG_INVALID)					
						SET_BIT(g_savedGlobals.sHeistData.iDisplayGroupVisibleBitset[theHeist], theDisplayGroup)
						//Tell the board controller that the flow has made a change that needs the board to be refreshed.
						SET_BIT(g_iBitsetHeistBoardUpdated, theHeist) 
					ENDIF
					
					//Follow interaction complete jump.
					RETURN INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.commsVars[paramCommsVarsArrayPos].iValue4)
				ENDIF
			ENDIF
		BREAK
	
		//Manage selecting a crew.
		CASE INTERACT_CREW
			IF theFlaggedMode <> PBM_CREW_SELECT
				//The board has not been flagged to switch to crew select mode yet. Flag it.
				SET_MISSION_FLOW_INT_VALUE(theBoardModeFlag, ENUM_TO_INT(PBM_CREW_SELECT))
				
				//Ensure display group is turned off as we start the interaction.
				IF theDisplayGroup <> ENUM_TO_INT(PBDG_INVALID)
					CLEAR_BIT(g_savedGlobals.sHeistData.iDisplayGroupVisibleBitset[theHeist], theDisplayGroup)
					//Tell the board controller that the flow has made a change that needs the board to be refreshed.
					SET_BIT(g_iBitsetHeistBoardUpdated, theHeist) 
				ENDIF
				
				//Make sure the crew selected bit is cleared as we initialise the interaction.
				SET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_HEIST_CREW_SELECTED, theHeist, FALSE)
				
				//Set board interaction modifier flags as we initialise.
				SET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_HEIST_DISABLE_BOARD_EXIT, theHeist, lockExit)
			ELSE
				//The board is already flagged to switch to crew select mode.
				//Has the crew been selected yet?
				IF GET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_HEIST_CREW_SELECTED, theHeist)
					//Yes...
					//Set the board mode flag to viewing mode if requested.
					IF viewWhenDone
						SET_MISSION_FLOW_INT_VALUE(theBoardModeFlag, ENUM_TO_INT(PBM_VIEW))
					ENDIF
					
					//Clean up the "Heist Crew Selected" flag.
					SET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_HEIST_CREW_SELECTED, theHeist, FALSE)
					
					//Turn on the display group linked to this interaction.
					IF theDisplayGroup <> ENUM_TO_INT(PBDG_INVALID)
						SET_BIT(g_savedGlobals.sHeistData.iDisplayGroupVisibleBitset[theHeist], theDisplayGroup)
						//Tell the board controller that the flow has made a change that needs the board to be refreshed.
						SET_BIT(g_iBitsetHeistBoardUpdated, theHeist) 
					ENDIF
					
					//Update social club stats for the selected crew members.
					FLOW_INT_IDS theChoiceFlowInt
					SWITCH(theHeist)
						CASE HEIST_JEWEL
							theChoiceFlowInt = FLOWINT_HEIST_CHOICE_JEWEL
						BREAK
						
						CASE HEIST_DOCKS
							theChoiceFlowInt = FLOWINT_HEIST_CHOICE_DOCKS
						BREAK
						
						CASE HEIST_RURAL_BANK
							theChoiceFlowInt = FLOWINT_HEIST_CHOICE_RURAL
						BREAK
						
						CASE HEIST_AGENCY
							theChoiceFlowInt = FLOWINT_HEIST_CHOICE_AGENCY
						BREAK
						
						CASE HEIST_FINALE
							theChoiceFlowInt = FLOWINT_HEIST_CHOICE_FINALE
						BREAK
					ENDSWITCH
					INT iHeistChoice
					iHeistChoice = Get_Mission_Flow_Int_Value(theChoiceFlowInt)
					INT iCrewIndex
					REPEAT g_sHeistChoiceData[iHeistChoice].iCrewSize iCrewIndex
						STATSENUM eCrewChoiceStat
						eCrewChoiceStat = PRIVATE_Get_Heist_Crew_Choice_Stat_For_Index(theHeist, iCrewIndex)
						IF eCrewChoiceStat != CITIES_PASSED
							CDEBUG1LN(DEBUG_HEIST, 	"Flow command setting heist stat ", PRIVATE_Get_Heist_Choice_Stat_Debug_String(eCrewChoiceStat), " to ", g_savedGlobals.sHeistData.eSelectedCrew[iHeistChoice][iCrewIndex], ".")
							STAT_SET_INT(eCrewChoiceStat, ENUM_TO_INT(g_savedGlobals.sHeistData.eSelectedCrew[iHeistChoice][iCrewIndex]))
						ENDIF
					ENDREPEAT
					
					//Follow interaction complete jump.
					RETURN INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.commsVars[paramCommsVarsArrayPos].iValue4)
				ENDIF
			ENDIF
		BREAK
		
		
		//Manage selecting a gameplay choice.
		CASE INTERACT_GAMEPLAY_CHOICE
			//Look up which FLOWINT we need to check.
			FLOW_INT_IDS theChoiceFlowInt
			SWITCH(theHeist)
				CASE HEIST_JEWEL
					theChoiceFlowInt = FLOWINT_HEIST_CHOICE_JEWEL
				BREAK
				
				CASE HEIST_DOCKS
					theChoiceFlowInt = FLOWINT_HEIST_CHOICE_DOCKS
				BREAK
				
				CASE HEIST_RURAL_BANK
					theChoiceFlowInt = FLOWINT_HEIST_CHOICE_RURAL
				BREAK
				
				CASE HEIST_AGENCY
					theChoiceFlowInt = FLOWINT_HEIST_CHOICE_AGENCY
				BREAK
				
				CASE HEIST_FINALE
					theChoiceFlowInt = FLOWINT_HEIST_CHOICE_FINALE
				BREAK
			ENDSWITCH
		
			IF theFlaggedMode <> PBM_CHOICE_SELECT
				//The board has not been flagged to switch to gameplay choice select mode yet. Flag it.
				SET_MISSION_FLOW_INT_VALUE(theBoardModeFlag, ENUM_TO_INT(PBM_CHOICE_SELECT))
				
				//Make sure the choice is cleared as we initialise the interaction.
				SET_MISSION_FLOW_INT_VALUE(theChoiceFlowInt, HEIST_CHOICE_EMPTY)
				SET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_HEIST_CHOICE_SELECTED, theHeist, FALSE)
				
				//Ensure display group is turned off as we start the interaction.
				IF theDisplayGroup <> ENUM_TO_INT(PBDG_INVALID)
					CLEAR_BIT(g_savedGlobals.sHeistData.iDisplayGroupVisibleBitset[theHeist], theDisplayGroup)
					//Tell the board controller that the flow has made a change that needs the board to be refreshed.
					SET_BIT(g_iBitsetHeistBoardUpdated, theHeist) 
				ENDIF
				
				//Set board interaction modifier flags as we initialise.
				SET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_HEIST_DISABLE_BOARD_EXIT, theHeist, lockExit)
			ELSE
				//The board is already flagged to switch to gameplay choice select mode.
				//Has the choice been selected yet?
				IF GET_MISSION_FLOW_INT_VALUE(theChoiceFlowInt) <> HEIST_CHOICE_EMPTY
				AND GET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_HEIST_CHOICE_SELECTED, theHeist)
					//Yes... 
					//Set the board mode flag to viewing mode if requested.
					IF viewWhenDone
						SET_MISSION_FLOW_INT_VALUE(theBoardModeFlag, ENUM_TO_INT(PBM_VIEW))
					ENDIF
					
					//Clean up the "Heist Choice Selected" flag.
					SET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_HEIST_CHOICE_SELECTED, theHeist, FALSE)
					
					//Turn on the display group linked to this interaction.
					IF theDisplayGroup <> ENUM_TO_INT(PBDG_INVALID)
						SET_BIT(g_savedGlobals.sHeistData.iDisplayGroupVisibleBitset[theHeist], theDisplayGroup)
						//Tell the board controller that the flow has made a change that needs the board to be refreshed.
						SET_BIT(g_iBitsetHeistBoardUpdated, theHeist) 
					ENDIF
					
					//Update social club stats for the selected gameplay choice.
					INT iHeistChoice
					iHeistChoice = Get_Mission_Flow_Int_Value(theChoiceFlowInt)
					STATSENUM eGameplayChoiceStat
					eGameplayChoiceStat = PRIVATE_Get_Heist_Gameplay_Choice_Stat(theHeist)
					IF eGameplayChoiceStat != CITIES_PASSED
						CDEBUG1LN(DEBUG_HEIST, 	"Flow command setting heist stat ", 
												PRIVATE_Get_Heist_Choice_Stat_Debug_String(eGameplayChoiceStat), 
												" to ", iHeistChoice, ".")
						STAT_SET_INT(eGameplayChoiceStat, iHeistChoice)
					ENDIF

					//Follow interaction complete jump.
					RETURN INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.commsVars[paramCommsVarsArrayPos].iValue4)
				ENDIF
			ENDIF
		BREAK
		
	ENDSWITCH
	#endif
	#endif
	//Should never be reached.
	RETURN STAY_ON_THIS_COMMAND
ENDFUNC


// PURPOSE:	Tell a heist controller to turn on/off a planning board view.
//
// INPUT PARAMS:		paramCoreVarsArrayPos			Index into the Core Variables array
// RETURN VALUE:		JUMP_LABEL_IDS					The Jump Label to continue at

FUNC JUMP_LABEL_IDS PERFORM_TOGGLE_BOARD_VIEWING(INT paramCoreVarsArrayPos)
#if USE_CLF_DLC
paramCoreVarsArrayPos = paramCoreVarsArrayPos
#endif
#if USE_NRM_DLC
paramCoreVarsArrayPos = paramCoreVarsArrayPos
#endif

	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
	// Ensure the Core Vars Array position is legal
	IF (paramCoreVarsArrayPos = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_TOGGLE_BOARD_VIEWING: The Core Variables Array Position parameter is illegal. MOVING ON TO NEXT COMMAND.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	//this was approved by Steve T
	IF g_ConversationStatus = CONV_STATE_PLAYING
		IF IS_SCRIPTED_CONVERSATION_ONGOING()
			CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_TOGGLE_BOARD_VIEWING: Scripted conversation currently ongoing.")
			RETURN STAY_ON_THIS_COMMAND
		ENDIF
	ENDIF
	
	// Get the heist ID and check it's valid.
	INT theHeist = g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue1
	IF theHeist < 0 OR theHeist >= NO_HEISTS
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_TOGGLE_BOARD_VIEWING: Invalid heist ID passed. MOVING ON TO NEXT COMMAND.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Get the custom flowflag to toggle if one has been set.
	FLOW_FLAG_IDS theFlowFlag = INT_TO_ENUM(FLOW_FLAG_IDS, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue2)
	IF theFlowFlag != FLOWFLAG_NONE
		IF NOT GET_MISSION_FLOW_FLAG_STATE(theFlowFlag)
			SET_MISSION_FLOW_FLAG_STATE(theFlowFlag, TRUE)
		ENDIF
	ENDIF
	
	INT iSettings = g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue3
	
	IF IS_BIT_SET(iSettings, FLOW_BOARD_BIT_WAIT_FOR_CUTSCENE)
		IF NOT IS_CUTSCENE_PLAYING()
			CPRINTLN(DEBUG_FLOW, "Waiting for cutscene to start before toggling board view.")
			RETURN STAY_ON_THIS_COMMAND
		ENDIF
	ENDIF
	
	CPRINTLN(DEBUG_FLOW, "Cutscene started. Toggling board view.")
		
	//Tell the heist controller to turn off board exiting if requested.
	SET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_HEIST_DISABLE_BOARD_EXIT, theHeist, IS_BIT_SET(iSettings, FLOW_BOARD_BIT_LOCK_EXIT))
	
	//Tell the heist controller to toggle the board view.
	SET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_HEIST_TOGGLE_BOARD, theHeist, TRUE)
	
	//Ensure all strands are processed next frame for seamlessnes.
	g_flowUnsaved.bFlowProcessAllStrandsNextFrame = TRUE
	
	#endif
	#endif
	//Should never be reached.
	RETURN NEXT_SEQUENTIAL_COMMAND
ENDFUNC



// PURPOSE:	Modify the visible state of a group of items on a planning board.
//
// INPUT PARAMS:		paramCoreVarsArrayPos			Index into the Core Variables array
// RETURN VALUE:		JUMP_LABEL_IDS					The Jump Label to continue on to.

FUNC JUMP_LABEL_IDS PERFORM_SET_BOARD_DISPLAY_GROUP_VISIBILITY(INT paramCoreVarsArrayPos)
	// Ensure the Core Vars Array position is legal
	IF (paramCoreVarsArrayPos = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_SET_BOARD_DISPLAY_GROUP_VISIBILITY: The Core Variables Array Position parameter is illegal. MOVING ON TO NEXT COMMAND.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Get the heist ID and check it's valid.
	INT theHeist = g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue1
	IF theHeist < 0 OR theHeist >= NO_HEISTS
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_SET_BOARD_DISPLAY_GROUP_VISIBILITY: Invalid heist ID passed. MOVING ON TO NEXT COMMAND.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Get the display group index and check it's valid.
	INT theDisplayGroup = g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue2
	IF theDisplayGroup < 0 OR theDisplayGroup >= ENUM_TO_INT(PBDG_MAX)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_SET_BOARD_DISPLAY_GROUP_VISIBILITY: Invalid display group ID passed. MOVING ON TO NEXT COMMAND.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	INT iSettings = g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue3
	
	// All parameters valid. Update the state in the heist's global display group bitset.
	IF IS_BIT_SET(iSettings, FLOW_BOARD_BIT_DGROUP_VISIBLE)
		SET_BIT(g_savedGlobals.sHeistData.iDisplayGroupVisibleBitset[theHeist], theDisplayGroup)
	ELSE
		CLEAR_BIT(g_savedGlobals.sHeistData.iDisplayGroupVisibleBitset[theHeist], theDisplayGroup)
	ENDIF
	
	// Flag the planning board as being updated so the heist controller knows to make changes.
	SET_BIT(g_iBitsetHeistBoardUpdated, theHeist) 
	
	// All done. Step to the next command.
	RETURN NEXT_SEQUENTIAL_COMMAND
	
ENDFUNC



// ===========================================================================================================
//		MISSION CHECK commands
// ===========================================================================================================

FUNC JUMP_LABEL_IDS PERFORM_IS_MISSION_COMPLETED(INT paramCoreVarsArrayPos)

	// Ensure the Script Vars Array position is legal
	IF (paramCoreVarsArrayPos = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_IS_MISSION_COMPLETED: The Core Variables Array Position parameter is illegal. MOVING ON TO NEXT COMMAND.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Get the data index for the mission.
	INT iMissionDataIndex = g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue1
	
	// Ensure the mission data index is legal
	IF iMissionDataIndex = ENUM_TO_INT(SP_MISSION_NONE) OR iMissionDataIndex = ENUM_TO_INT(SP_MISSION_MAX)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_IS_MISSION_COMPLETED: The stored mission ID was invalid. SP_MISSION_NONE and SP_MISSION_MAX are not valid values.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	JUMP_LABEL_IDS ePass = INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue2)
	JUMP_LABEL_IDS eFail = INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue3)
	
	#IF IS_DEBUG_BUILD
		IF IS_FLOW_CONSOLE_LOG_READY_TO_PRINT()
			CDEBUG1LN(DEBUG_FLOW_COMMANDS, "Is Mission Completed (MISSION: ",  g_sMissionStaticData[iMissionDataIndex].scriptName, ", TRUEJUMP=", Get_Label_Display_String_From_Label_ID(ePass), ", FALSEJUMP=", Get_Label_Display_String_From_Label_ID(eFail), ", DEBUGJUMP=", Get_Label_Display_String_From_Label_ID(INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue4)), ")")
		ENDIF
	#ENDIF
	
	// Check if the mission is complete.
	#if USE_CLF_DLC
		IF g_savedGlobalsClifford.sFlow.missionSavedData[iMissionDataIndex].completed
			RETURN ePass
		ELSE
			RETURN eFail
		ENDIF
	#endif
	#if USE_NRM_DLC
		IF g_savedGlobalsnorman.sFlow.missionSavedData[iMissionDataIndex].completed
			RETURN ePass
		ELSE
			RETURN eFail
		ENDIF
	#endif
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
		IF g_savedGlobals.sFlow.missionSavedData[iMissionDataIndex].completed
			RETURN ePass
		ELSE
			RETURN eFail
		ENDIF
	#endif
	#endif
ENDFUNC



// ===========================================================================================================
//		CHARACTER CHECK commands
// ===========================================================================================================

FUNC JUMP_LABEL_IDS PERFORM_IS_PLAYING_AS_CHARACTER(INT paramCoreVarsArrayPos)
	// Ensure the Script Vars Array position is legal
	IF (paramCoreVarsArrayPos = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_IS_PLAYING_AS_CHARACTER: The Core Variables Array Position parameter is illegal. MOVING ON TO NEXT COMMAND.")
		
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Get the Character to be tested.
	enumCharacterList theCharacter = INT_TO_ENUM(enumCharacterList, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue1)
	
	// Ensure the character to be tested is legal.
	IF NOT IS_PLAYER_PED_PLAYABLE(theCharacter)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_IS_PLAYING_AS_CHARACTER: The character being checked for is not playable. MOVING ON TO NEXT COMMAND.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Check if the player is currently playing as this character.
	IF IS_PED_THE_CURRENT_PLAYER_PED(theCharacter)
		RETURN INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue2)
	ELSE
		RETURN INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue3)
	ENDIF
ENDFUNC


FUNC JUMP_LABEL_IDS PERFORM_IS_SWITCHING_TO_CHARACTER(INT paramCoreVarsArrayPos)
	// Ensure the Script Vars Array position is legal
	IF (paramCoreVarsArrayPos = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_IS_SWITCHING_TO_CHARACTER: The Core Variables Array Position parameter is illegal. MOVING ON TO NEXT COMMAND.")
		
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Get the Character to be tested.
	enumCharacterList theCharacter = INT_TO_ENUM(enumCharacterList, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue1)
	
	// Ensure the character to be tested is legal.
	IF NOT IS_PLAYER_PED_PLAYABLE(theCharacter)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_IS_SWITCHING_TO_CHARACTER: The character being checked for is not playable. MOVING ON TO NEXT COMMAND.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Check if we are in the middle of a switch.
	IF IS_PLAYER_PED_SWITCH_IN_PROGRESS()
		//Is this switch switching to the specified character?
		IF g_sPlayerPedRequest.ePed = theCharacter
			RETURN INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue2)
		ELSE
			RETURN INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue3)
		ENDIF
	ELSE
		RETURN INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue3)
	ENDIF
	
ENDFUNC


FUNC JUMP_LABEL_IDS PERFORM_IS_PLAYER_IN_AREA(INT paramCoreSmallVarsArrayPos)
	
	IF (paramCoreSmallVarsArrayPos = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_IS_PLAYER_IN_AREA: The Core Small Variables Array Position parameter is illegal. MOVING ON TO NEXT COMMAND.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	VectorID ccv = INT_TO_ENUM(VectorID,g_flowUnsaved.coreSmallVars[paramCoreSmallVarsArrayPos].iValue1)
	INT inv = g_flowUnsaved.coreSmallVars[paramCoreSmallVarsArrayPos].iValue2
	
	#IF IS_DEBUG_BUILD
		IF g_bTriggerDebugOn
			DEBUG_DRAW_VECTOR_ID_AREA(ccv, GET_DEBUG_STRING_FOR_VECTOR_ID(ccv))
		ENDIF
	#ENDIF
	
	BOOL check = IS_PED_AT_VECTOR_ID(PLAYER_PED_ID(), ccv)
	IF inv = 1
		check = !check
	ENDIF
	
	IF check
		RETURN NEXT_SEQUENTIAL_COMMAND //If check met.
	ENDIF
	
	RETURN STAY_ON_THIS_COMMAND
	
ENDFUNC



// ===========================================================================================================
//		HOTSWAP commands
// ===========================================================================================================

// PURPOSE:	Set the off-mission hotswap availability of a playable character.
//
// INPUT PARAMS:		paramCoreSmallVarsArrayPos			Index into the Core Small Variables array
// RETURN VALUE:		JUMP_LABEL_IDS						The Jump Label to continue at

FUNC JUMP_LABEL_IDS PERFORM_SET_CHAR_HOTSWAP_AVAILABILITY(INT paramCoreSmallVarsArrayPos)
	// Ensure the Core Small Vars Array position is legal
	IF (paramCoreSmallVarsArrayPos = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_SET_CHAR_HOTSWAP_AVAILABILITY: The Core Small Variables Array Position parameter is illegal. MOVING ON TO NEXT COMMAND.")
		
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Get the character who is to have their hotswap availability set.
	enumCharacterList theCharacter = INT_TO_ENUM(enumCharacterList, g_flowUnsaved.coreSmallVars[paramCoreSmallVarsArrayPos].iValue1)
	
	BOOL bSetAvailable = FALSE
	IF (g_flowUnsaved.coreSmallVars[paramCoreSmallVarsArrayPos].iValue2%FLOW_COMMAND_HASH_MODIFIER_MAGNITUDE) = 1
		bSetAvailable = TRUE
	ENDIF
	
	// Ensure the character ID is legal
	IF (theCharacter = CHAR_BLANK_ENTRY)
		CERRORLN(DEBUG_FLOW_COMMANDS, "Perform_Set_Char_Hotswap_Availability: The character who is to have their hotswap availability set is illegal. MOVING ON TO NEXT COMMAND.")
		
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Set the character's hotswap availability.
	SET_PLAYER_PED_AVAILABLE(theCharacter, bSetAvailable)
	
	RETURN NEXT_SEQUENTIAL_COMMAND
ENDFUNC


// PURPOSE:	Set the current player character.
//
// INPUT PARAMS:		paramIndex			The index in which we are storing the player character enum directly.
// RETURN VALUE:		JUMP_LABEL_IDS		The Jump Label to continue at

FUNC JUMP_LABEL_IDS PERFORM_SET_PLAYER_CHARACTER(INT paramIndex)
	
	#IF IS_DEBUG_BUILD
		IF IS_FLOW_CONSOLE_LOG_READY_TO_PRINT()
			CDEBUG1LN(DEBUG_FLOW_COMMANDS, "Set Player Character (CHAR: ", GET_CHARSHEET_DISPLAY_STRING_FROM_CHARSHEET(INT_TO_ENUM(enumCharacterList,paramIndex)), ")")
		ENDIF
	#ENDIF
	
	// Get the playable character ENUM.
	enumCharacterList newPlayerCharacter = INT_TO_ENUM(enumCharacterList,paramIndex)
	
	// Check the character ENUM is a valid playable character.
	IF NOT IS_PLAYER_PED_PLAYABLE(newPlayerCharacter)
		CERRORLN(DEBUG_FLOW_COMMANDS, "Perform_Set_Player_Character: Failed to swap character ped as the specified character was non playable. MOVING ON TO NEXT COMMAND.")
	
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Set the player ped to the new character.
	IF SET_CURRENT_SELECTOR_PED(GET_SELECTOR_SLOT_FROM_PLAYER_PED_ENUM(newPlayerCharacter))
		RETURN NEXT_SEQUENTIAL_COMMAND
	ELSE
		RETURN STAY_ON_THIS_COMMAND
	ENDIF
ENDFUNC


// ===========================================================================================================
//		SAVEHOUSE commands
// ===========================================================================================================

// PURPOSE:	Set the active state of a Savehouse.
//
// INPUT PARAMS:		paramCoreSmallVarsArrayPos		Index into the Core Small Variables array
// RETURN VALUE:		JUMP_LABEL_IDS					The Jump Label to continue at

FUNC JUMP_LABEL_IDS PERFORM_SET_SAVEHOUSE_STATE(INT paramCoreSmallVarsArrayPos)
	// Ensure the Core Small Vars Array position is legal
	IF (paramCoreSmallVarsArrayPos = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_SET_SAVEHOUSE_STATE: The Core Small Variables Array Position parameter is illegal. MOVING ON TO NEXT COMMAND.")
		
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Get the shop to be state changed
	SAVEHOUSE_NAME_ENUM theSavehouseID = INT_TO_ENUM(SAVEHOUSE_NAME_ENUM, g_flowUnsaved.coreSmallVars[paramCoreSmallVarsArrayPos].iValue1)
	
	// Set the savehouse's state.
	INT iSettings = g_flowUnsaved.coreSmallVars[paramCoreSmallVarsArrayPos].iValue2
	BOOL bAvailable = IS_BIT_SET(iSettings, REPAWN_FLAG_AVAILABLE_BIT)
	BOOL bLongRangeBlip = IS_BIT_SET(iSettings, REPAWN_FLAG_LONG_RANGE_BLIP_BIT)
	SET_SAVEHOUSE_RESPAWN_AVAILABLE(theSavehouseID, bAvailable)
	SET_SAVEHOUSE_RESPAWN_BLIP_LONG_RANGE(theSavehouseID, bLongRangeBlip)
	
	RETURN NEXT_SEQUENTIAL_COMMAND
ENDFUNC


// ===========================================================================================================
//		SHOP commands
// ===========================================================================================================

// PURPOSE:	Set the active state of a Shop.
//
// INPUT PARAMS:		paramCoreSmallVarsArrayPos		Index into the Core Small Variables array
// RETURN VALUE:		JUMP_LABEL_IDS					The Jump Label to continue at

FUNC JUMP_LABEL_IDS PERFORM_SET_SHOP_STATE(INT paramCoreSmallVarsArrayPos)
	// Ensure the Core Small Vars Array position is legal
	IF (paramCoreSmallVarsArrayPos = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_SET_SHOP_STATE: The Core Small Variables Array Position parameter is illegal. MOVING ON TO NEXT COMMAND.")
		
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Get the shop to be state changed
	SHOP_NAME_ENUM theShopID = INT_TO_ENUM(SHOP_NAME_ENUM, g_flowUnsaved.coreSmallVars[paramCoreSmallVarsArrayPos].iValue1)

	// Get the shop's new state.
	BOOL newState = FALSE
	IF g_flowUnsaved.coreSmallVars[paramCoreSmallVarsArrayPos].iValue2 = 1
		newState = TRUE
	ENDIF
	
	//Set the shop's state. 
	SET_SHOP_IS_AVAILABLE(theShopID, newState)
	SET_SHOP_BLIP_NEEDS_UPDATED(theShopID)
	
	RETURN NEXT_SEQUENTIAL_COMMAND
ENDFUNC


// PURPOSE:	Set the lock state of a weapon component.
//
// INPUT PARAMS:		paramCoreVarsArrayPos		Index into the Core Variables array
// RETURN VALUE:		JUMP_LABEL_IDS				The Jump Label to continue at

FUNC JUMP_LABEL_IDS PERFORM_SET_WEAPON_COMP_LOCK_STATE(INT paramCoreVarsArrayPos)
	// Ensure the Core Vars Array position is legal
	IF (paramCoreVarsArrayPos = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_SET_WEAPON_COMP_LOCK_STATE: The Core Variables Array Position parameter is illegal. MOVING ON TO NEXT COMMAND.")
		
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Get the weapon comp to be state changed
	WEAPON_TYPE theWeaponID = INT_TO_ENUM(WEAPON_TYPE, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue1)
	WEAPONCOMPONENT_TYPE theWeaponCompID = INT_TO_ENUM(WEAPONCOMPONENT_TYPE, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue2)
	
	// Get the weapons lock state.
	BOOL newState = FALSE
	IF g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue3 = 1
		newState = TRUE
	ENDIF
	
	// Set the weapon comps lock state.
	SET_PLAYER_PED_WEAPON_COMP_UNLOCKED(CHAR_MICHAEL, theWeaponID, theWeaponCompID, !newState)
	SET_PLAYER_PED_WEAPON_COMP_UNLOCKED(CHAR_TREVOR, theWeaponID, theWeaponCompID, !newState)
	SET_PLAYER_PED_WEAPON_COMP_UNLOCKED(CHAR_FRANKLIN, theWeaponID, theWeaponCompID, !newState)
	
	RETURN NEXT_SEQUENTIAL_COMMAND
ENDFUNC



// ===========================================================================================================
//		VEHICLE GEN commands
// ===========================================================================================================

// PURPOSE:	Set the active state of a vehicle gen.
//
// INPUT PARAMS:		paramCoreSmallVarsArrayPos		Index into the Core Small Variables array
// RETURN VALUE:		JUMP_LABEL_IDS					The Jump Label to continue at

FUNC JUMP_LABEL_IDS PERFORM_SET_VEHICLE_GEN_STATE(INT paramCoreSmallVarsArrayPos)
	// Ensure the Miscellaneous Vars Array position is legal
	IF (paramCoreSmallVarsArrayPos = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_SET_VEHICLE_GEN_STATE: The Core Small Variables Array Position parameter is illegal. MOVING ON TO NEXT COMMAND.")
		
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Get the vehicle gen to be state changed
	VEHICLE_GEN_NAME_ENUM theVehicleGenID = INT_TO_ENUM(VEHICLE_GEN_NAME_ENUM, g_flowUnsaved.coreSmallVars[paramCoreSmallVarsArrayPos].iValue1)
	
	// Set the vehicle gens state.
	BOOL newState = FALSE
	IF g_flowUnsaved.coreSmallVars[paramCoreSmallVarsArrayPos].iValue2 = 1
		newState = TRUE
	ENDIF
	
	SET_VEHICLE_GEN_AVAILABLE(theVehicleGenID, newState)
	
	RETURN NEXT_SEQUENTIAL_COMMAND
ENDFUNC


// PURPOSE:	Set the unlock state for the player characters special ability.
//
// INPUT PARAMS:		paramCoreSmallVarsArrayPos		Index into the Core Small Variables array
// RETURN VALUE:		JUMP_LABEL_IDS					The Jump Label to continue at

FUNC JUMP_LABEL_IDS PERFORM_SET_SPECIAL_ABILITY_UNLOCK_STATE(INT paramCoreSmallVarsArrayPos)
	// Ensure the Core Small Vars Array position is legal
	IF (paramCoreSmallVarsArrayPos = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_SET_SPECIAL_ABILITY_UNLOCK_STATE: The Core Small Variables Array Position parameter is illegal. MOVING ON TO NEXT COMMAND.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Get the player character
	enumCharacterList ePed = INT_TO_ENUM(enumCharacterList, g_flowUnsaved.coreSmallVars[paramCoreSmallVarsArrayPos].iValue1)
	
	// Set the special ability unlock state
	IF g_flowUnsaved.coreSmallVars[paramCoreSmallVarsArrayPos].iValue2 = 1
		SPECIAL_ABILITY_UNLOCK(GET_PLAYER_PED_MODEL(ePed))
		g_savedGlobals.sPlayerData.sInfo.bSpecialAbilityUnlocked[ePed] = TRUE
	ELSE
		SPECIAL_ABILITY_LOCK(GET_PLAYER_PED_MODEL(ePed))
		g_savedGlobals.sPlayerData.sInfo.bSpecialAbilityUnlocked[ePed] = FALSE
	ENDIF
	
	RETURN NEXT_SEQUENTIAL_COMMAND
ENDFUNC


// PURPOSE:	Set the state of a door that can have its state swapping.
//
// INPUT PARAMS:		paramCoreSmallVarsArrayPos		Index into the Core Small Variables array.
// RETURN VALUE:		JUMP_LABEL_IDS						The Jump Label to continue at.

FUNC JUMP_LABEL_IDS PERFORM_SET_DOOR_STATE(INT paramCoreSmallVarsArrayPos)
	// Ensure the Core Small Vars Array position is legal
	IF (paramCoreSmallVarsArrayPos = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_SET_DOOR_STATE: The Core Small Variables Array Position parameter is illegal. MOVING ON TO NEXT COMMAND.")
		
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Get the Door ID that is to have its state changed.
	DOOR_NAME_ENUM theDoorID = INT_TO_ENUM(DOOR_NAME_ENUM, g_flowUnsaved.coreSmallVars[paramCoreSmallVarsArrayPos].iValue1)

	// Get the state of the door ID.
	DOOR_STATE_ENUM theState
	IF (g_flowUnsaved.coreSmallVars[paramCoreSmallVarsArrayPos].iValue2%FLOW_COMMAND_HASH_MODIFIER_MAGNITUDE) = 1
		theState = DOORSTATE_UNLOCKED // 1 is unlocked
	ELSE
		theState = DOORSTATE_LOCKED
	ENDIF
	
	// Set the new state.
	SET_DOOR_STATE(theDoorID, theState)
	
	// Move to next command.
	RETURN NEXT_SEQUENTIAL_COMMAND
ENDFUNC


// PURPOSE:	Makes a Request call to the player.
//
// INPUT PARAMS:			paramIndex			The index value used to store character data in directly as an optimisation.
// RETURN VALUE:			JUMP_LABEL_IDS		A jump label - should be STAY_ON_THIS_COMMAND until ready to move on

FUNC JUMP_LABEL_IDS PERFORM_RESET_PRIORITY_SWITCH_SCENES(INT paramIndex)

	#IF IS_DEBUG_BUILD
		IF IS_FLOW_CONSOLE_LOG_READY_TO_PRINT()
			CDEBUG1LN(DEBUG_FLOW_COMMANDS, "Set Ped Request Scene")
		ENDIF
	#ENDIF
	
	//Set the Request for the player char.
	enumCharacterList ePed = INT_TO_ENUM(enumCharacterList, paramIndex) //NB.Character data is stored directly in the index field.
	INT iSample
	REPEAT 4 iSample
		#if USE_CLF_DLC
			g_savedGlobalsClifford.sPlayerSceneData.g_bPlayerTriggeredPrioritySwitch[ePed][iSample] = TRUE
		#endif
		#if USE_NRM_DLC
			g_savedGlobalsnorman.sPlayerSceneData.g_bPlayerTriggeredPrioritySwitch[ePed][iSample] = TRUE
		#endif
		#if not USE_CLF_DLC
		#if not USE_NRM_DLC	
			g_SavedGlobals.sPlayerSceneData.g_bPlayerTriggeredPrioritySwitch[ePed][iSample] = TRUE
		#endif
		#endif	
	ENDREPEAT
	
	//Simply step to the next command.
	RETURN NEXT_SEQUENTIAL_COMMAND

ENDFUNC


// PURPOSE:	Makes a Request call to the player.
//
// INPUT PARAMS:			paramCoreSmallVarsArrayPos		Index into the Core Small Variables array
// RETURN VALUE:			JUMP_LABEL_IDS					A jump label - should be STAY_ON_THIS_COMMAND until ready to move on

FUNC JUMP_LABEL_IDS PERFORM_SET_PED_REQUEST_SCENE(INT paramCoreSmallVarsArrayPos)
	// Ensure the Core Small Vars Array position is legal
	IF (paramCoreSmallVarsArrayPos = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_SET_PED_REQUEST_SCENE: The Core Small Variables Array Position parameter is illegal. MOVING ON TO NEXT COMMAND.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	//Set the Request for the player char.
	PED_REQUEST_SCENE_ENUM eReqScene	= INT_TO_ENUM(PED_REQUEST_SCENE_ENUM, g_flowUnsaved.coreSmallVars[paramCoreSmallVarsArrayPos].iValue1)
	enumCharacterList ePed				= INT_TO_ENUM(enumCharacterList, g_flowUnsaved.coreSmallVars[paramCoreSmallVarsArrayPos].iValue2)
	
	//Ensure the new set ped request scene wont overwrite a stored post-mission scene (527069)
	#if USE_CLF_DLC
		PED_REQUEST_SCENE_ENUM eOldScene	= g_savedGlobalsClifford.sPlayerSceneData.g_ePlayerLastScene[ePed]
	#endif
	#if USE_NRM_DLC
		PED_REQUEST_SCENE_ENUM eOldScene	= g_savedGlobalsnorman.sPlayerSceneData.g_ePlayerLastScene[ePed]
	#endif
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC	
		PED_REQUEST_SCENE_ENUM eOldScene	= g_SavedGlobals.sPlayerSceneData.g_ePlayerLastScene[ePed]
	#endif
	#endif	
	
	IF (eOldScene <> PR_SCENE_INVALID)
		
		IF NOT ((eOldScene = PR_SCENE_Fa_STRIPCLUB_ARM3)
		OR (eOldScene = PR_SCENE_Fa_PHONECALL_ARM3)
//		OR (eOldScene = PR_SCENE_Ma_ARM3)
//		OR (eOldScene = PR_SCENE_Fa_STRIPCLUB_FAM1)
		OR (eOldScene = PR_SCENE_Fa_PHONECALL_FAM1)
//		OR (eOldScene = PR_SCENE_Fa_STRIPCLUB_FAM3)
		OR (eOldScene = PR_SCENE_Fa_PHONECALL_FAM3))
		
			VECTOR vLastKnownCoords
			FLOAT fLastKnownHead
			IF GetLastKnownPedInfoPostMission(eOldScene, vLastKnownCoords, fLastKnownHead)
				//Simply step to the next command.
				RETURN NEXT_SEQUENTIAL_COMMAND
			ENDIF
		ENDIF
	ENDIF
	
	#if USE_CLF_DLC
		g_savedGlobalsClifford.sPlayerSceneData.g_ePlayerLastScene[ePed] = eReqScene
		g_savedGlobalsClifford.sPlayerData.sInfo.vLastKnownCoords[ePed] = <<0,0,0>>
	#endif
	#if USE_NRM_DLC
		g_savedGlobalsnorman.sPlayerSceneData.g_ePlayerLastScene[ePed] = eReqScene
		g_savedGlobalsnorman.sPlayerData.sInfo.vLastKnownCoords[ePed] = <<0,0,0>>
	#endif
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC	
		g_SavedGlobals.sPlayerSceneData.g_ePlayerLastScene[ePed] = eReqScene
		g_savedGlobals.sPlayerData.sInfo.vLastKnownCoords[ePed] = <<0,0,0>>
	#endif
	#endif	
	
								
	//Simply step to the next command.
	RETURN NEXT_SEQUENTIAL_COMMAND
ENDFUNC


FUNC JUMP_LABEL_IDS PERFORM_SET_MISSION_PASSED(INT paramIndex)
	
	#IF IS_DEBUG_BUILD
		IF IS_FLOW_CONSOLE_LOG_READY_TO_PRINT()
			CDEBUG1LN(DEBUG_FLOW_COMMANDS, "Set Mission Passed")
		ENDIF
	#ENDIF
	
	// Get the mission data index for the mission.
	INT iMissionDataIndex = paramIndex
	
	//Check the mission data index is valid.
	IF iMissionDataIndex = ENUM_TO_INT(SP_MISSION_NONE) OR iMissionDataIndex = ENUM_TO_INT(SP_MISSION_MAX)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_SET_MISSION_PASSED: The stored mission ID was invalid. SP_MISSION_NONE and SP_MISSION_MAX are not valid missions.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	SET_MISSION_COMPLETE_STATE(INT_TO_ENUM(SP_MISSIONS, iMissionDataIndex), TRUE)
	
	//Set the mission as completed in the mission save data.
	#if USE_CLF_DLC
		g_savedGlobalsClifford.sFlow.missionSavedData[iMissionDataIndex].completed = TRUE
	#endif
	#if USE_NRM_DLC
		g_savedGlobalsnorman.sFlow.missionSavedData[iMissionDataIndex].completed = TRUE
	#endif
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
		g_savedGlobals.sFlow.missionSavedData[iMissionDataIndex].completed = TRUE
	#endif
	#endif
	//Simply step to the next command.
	RETURN NEXT_SEQUENTIAL_COMMAND
ENDFUNC


FUNC JUMP_LABEL_IDS PERFORM_SET_MISSION_NOT_PASSED(INT paramIndex)
	
	#IF IS_DEBUG_BUILD
		IF IS_FLOW_CONSOLE_LOG_READY_TO_PRINT()
			CDEBUG1LN(DEBUG_FLOW_COMMANDS, "Set Mission Not Passed")
		ENDIF
	#ENDIF
	
	// Get the mission data index for the mission.
	INT iMissionDataIndex = paramIndex
	
	//Check the mission data index is valid.
	IF iMissionDataIndex = ENUM_TO_INT(SP_MISSION_NONE) OR iMissionDataIndex = ENUM_TO_INT(SP_MISSION_MAX)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_SET_MISSION_NOT_PASSED: The stored mission ID was invalid. SP_MISSION_NONE and SP_MISSION_MAX are not valid missions.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	SET_MISSION_COMPLETE_STATE(INT_TO_ENUM(SP_MISSIONS, iMissionDataIndex), FALSE)
	
	//Set the mission as completed in the mission save data.
	#if USE_CLF_DLC
	g_savedGlobalsClifford.sFlow.missionSavedData[iMissionDataIndex].completed = FALSE
	#endif
	#if USE_NRM_DLC
	g_savedGlobalsnorman.sFlow.missionSavedData[iMissionDataIndex].completed = FALSE
	#endif
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
	g_savedGlobals.sFlow.missionSavedData[iMissionDataIndex].completed = FALSE
	#endif
	#endif
	
	//Simply step to the next command.
	RETURN NEXT_SEQUENTIAL_COMMAND
ENDFUNC


// ===========================================================================================================
//		ORAGNISER commands
// ===========================================================================================================

// PURPOSE:	Add an event to a player character's organiser.
//
// INPUT PARAMS:		paramCoreVarsArrayPos		Index into the Core Variables array.
// RETURN VALUE:		JUMP_LABEL_IDS				The Jump Label to continue at.

FUNC JUMP_LABEL_IDS PERFORM_ADD_ORGANISER_EVENT(INT paramCoreVarsArrayPos)

	#IF IS_DEBUG_BUILD
		IF IS_FLOW_CONSOLE_LOG_READY_TO_PRINT()
			CDEBUG1LN(DEBUG_FLOW_COMMANDS, "Add Organiser Event")
		ENDIF
	#ENDIF
	
	// Ensure the Core Vars Array position is legal
	IF (paramCoreVarsArrayPos = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_ADD_ORGANISER_EVENT: The Core Variables Array Position parameter is illegal. MOVING ON TO NEXT COMMAND.")
		
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	//Get the event type.
	ORGANISER_EVENT_ENUM eEventType = INT_TO_ENUM(ORGANISER_EVENT_ENUM, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue1)
	
	//Get the hour.
	INT iHour = g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue2
	
	//Get the event length.
	INT iLength = g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue3
	
	//Work out the day of the week.
	DAY_OF_WEEK eDayToAdd = GET_CLOCK_DAY_OF_WEEK()
	IF g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue4 = 1
		INT iDay = ENUM_TO_INT(eDayToAdd)
		iDay++
		IWRAP(iDay, 0, 6)
		eDayToAdd = INT_TO_ENUM(DAY_OF_WEEK, iDay)
	ENDIF
	
	//Add the organiser event.
	PLACE_EVENT_IN_ABSOLUTE_ORGANISER_TIMESLOT(eEventType, iHour, eDayToAdd, iLength)
	
	//Move to next command.
	RETURN NEXT_SEQUENTIAL_COMMAND
ENDFUNC



// ===========================================================================================================
//		FINANCE commands
// ===========================================================================================================

// PURPOSE:	Debit or credit a playable character's bank account.
//
// INPUT PARAMS:		paramCoreVarsArrayPos		Index into the Core Variables array.
// RETURN VALUE:		JUMP_LABEL_IDS				The Jump Label to continue at.

FUNC JUMP_LABEL_IDS PERFORM_DO_PLAYER_BANK_ACTION(INT paramCoreVarsArrayPos)
	
	IF (paramCoreVarsArrayPos = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_DO_PLAYER_BANK_ACTION: The Core Variables Array Position parameter is illegal. MOVING ON TO NEXT COMMAND.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	//Get the playable character whose bank to apply the action to.
	enumCharacterList eCharacter = INT_TO_ENUM(enumCharacterList, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue1)
	IF NOT IS_PLAYER_PED_PLAYABLE(eCharacter)
		SCRIPT_ASSERT("PERFORM_DO_PLAYER_BANK_ACTION: The character was not playable. MOVING ON TO NEXT COMMAND.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	//Get the action type.
	BANK_ACCOUNT_ACTION_TYPE eActionType = INT_TO_ENUM(BANK_ACCOUNT_ACTION_TYPE, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue2)
	
	//Get the source of the action.
	BANK_ACCOUNT_ACTION_SOURCE_BAAC eActionSource = INT_TO_ENUM(BANK_ACCOUNT_ACTION_SOURCE_BAAC, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue3)
	
	//Get the value of the action.
	INT iActionValue = g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue4
	IF iActionValue <= 0
		SCRIPT_ASSERT("PERFORM_DO_PLAYER_BANK_ACTION: The action value was <= 0. This is an invalid value. MOVING ON TO NEXT COMMMAND.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	IF eActionType = BAA_CREDIT
		CREDIT_BANK_ACCOUNT(eCharacter,eActionSource,iActionValue,TRUE)
	ELSE//hardcore debit
		//Perform the bank action.
		#if USE_CLF_DLC
		 DO_BANK_ACCOUNT_ACTION(g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[eCharacter].bank_account,
		 						eActionType,
								eActionSource,
								iActionValue,
								TRUE)
		#endif
		#if USE_NRM_DLC
		 DO_BANK_ACCOUNT_ACTION(g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[eCharacter].bank_account,
		 						eActionType,
								eActionSource,
								iActionValue,
								TRUE)
		#endif
		#if not USE_CLF_DLC
		#if not USE_NRM_DLC
		 DO_BANK_ACCOUNT_ACTION(GLOBAL_CHARACTER_SHEET_GET_BANK_ACCOUNT(eCharacter),
		 						eActionType,
								eActionSource,
								iActionValue,
								TRUE)
		#endif
		#endif
	ENDIF
	
//FORCE DEBIT BOOL - this means that it will take as much money as it can on debit
							
	
	RETURN NEXT_SEQUENTIAL_COMMAND
	
ENDFUNC


// ===========================================================================================================
//		SHRINK commands
// ===========================================================================================================

// PURPOSE:	Check wether a given Shrink session has any valid dialogue available to it.
//
// INPUT PARAMS:
// RETURN VALUE:		JUMP_LABEL_IDS				The Jump Label to continue at.
#if not USE_CLF_DLC
#if not USE_NRM_DLC
FUNC JUMP_LABEL_IDS PERFORM_IS_SHRINK_SESSION_AVAILABLE(INT paramIndex)
	
	IF (paramIndex = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_IS_SHRINK_SESSION_AVAILABLE: The Direct Index parameter is illegal. MOVING ON TO NEXT COMMAND.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
			//Retrieve the Shrink session to query.
		SHRINK_SESSION eSession = INT_TO_ENUM(SHRINK_SESSION, (paramIndex%FLOW_COMMAND_HASH_MODIFIER_MAGNITUDE))	
		//Check if the session has valid narrative available.
		IF SHRINK_IS_NARRATIVE_VALID_FOR_SESSION(eSession)
			RETURN NEXT_SEQUENTIAL_COMMAND
		ENDIF
		IF g_savedGlobals.sFlow.missionSavedData[GET_SP_MISSION_ID_FOR_SHRINK_SESSION_ID(eSession)].completed
			RETURN NEXT_SEQUENTIAL_COMMAND
		ENDIF
			
	RETURN STAY_ON_THIS_COMMAND
ENDFUNC
#endif
#endif

FUNC JUMP_LABEL_IDS PERFORM_DO_CUSTOM_CHECK_JUMPS(INT paramCoreVarsArrayPos)

	IF (paramCoreVarsArrayPos = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_DO_CUSTOM_CHECK_JUMPS: The Core Variables Array Position parameter is illegal. MOVING ON TO NEXT COMMAND.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	//Get the custom check ID.
	FLOW_CHECK_IDS eCheckID = INT_TO_ENUM(FLOW_CHECK_IDS, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue1)
	
	//Get the TRUE jump label ID.
	JUMP_LABEL_IDS eTrueJump = INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue2)
	
	//Get the FALSE jump label ID.
	JUMP_LABEL_IDS eFalseJump = INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue3)
	
	//Retrieve the cutstom check function.
	PerformCustomFlowCheck DO_FLOW_CHECK
	GET_FLOW_CHECK_FUNCTION(eCheckID, DO_FLOW_CHECK)
	
	//Run the check.
	IF CALL DO_FLOW_CHECK()
		RETURN eTrueJump
	ENDIF
	RETURN eFalseJump
ENDFUNC

/// PURPOSE:
///    Sets an outfit as acquired so it becomes available in the player's wardrobe
/// PARAMS:
///    paramCoreVarsArrayPos - 
/// RETURNS:
///    NEXT_SEQUENTIAL_COMMAND
FUNC JUMP_LABEL_IDS PERFORM_SET_COMPONENT_ACQUIRED(INT paramIndex)
	IF (paramIndex = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_SET_COMPONENT_ACQUIRED: The Direct Index parameter is illegal. MOVING ON TO NEXT COMMAND.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	//Get the playable character who has acquired the outfit
	enumCharacterList eCharacter = INT_TO_ENUM(enumCharacterList, g_flowUnsaved.coreVars[paramIndex].iValue1)
	IF NOT IS_PLAYER_PED_PLAYABLE(eCharacter)
		SCRIPT_ASSERT("PERFORM_SET_COMPONENT_ACQUIRED: The character was not playable. MOVING ON TO NEXT COMMAND.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	//Retrieve the outfit enum.
	PED_COMP_NAME_ENUM eItem = INT_TO_ENUM(PED_COMP_NAME_ENUM, g_flowUnsaved.coreVars[paramIndex].iValue2)
	PED_COMP_TYPE_ENUM eType = INT_TO_ENUM(PED_COMP_TYPE_ENUM, g_flowUnsaved.coreVars[paramIndex].iValue3)
	
	// Make this item available and acquired
	SET_PED_COMP_ITEM_AVAILABLE_SP(GET_PLAYER_PED_MODEL(eCharacter), eType, eItem, TRUE)
	SET_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(eCharacter), eType, eItem, TRUE)
	
	RETURN NEXT_SEQUENTIAL_COMMAND
ENDFUNC

// ===========================================================================================================
//		Leave Area Mission Flag Updating
// ===========================================================================================================

INT			iFrameMissionIndex = 0 					// Index used to regulate which active mission is having checks run against it this frame.

/// PURPOSE:
///    	Periodically check for mission leave area flags that are set and unflag them if the player has left
///    the mission's trigger area.
PROC UPDATE_LEAVE_AREA_FLAGS()
	INT iMissionIndex
	INT iMissionID
	
#IF USE_CLF_DLC
    REPEAT MAX_MISSIONS_AVAILABLE_TU iMissionIndex
		IF g_availableMissionsTU[iMissionIndex].index <> -1
			iMissionID = g_flowUnsaved.coreVars[g_availableMissionsTU[iMissionIndex].index].iValue1
#ENDIF
#IF USE_NRM_DLC
    REPEAT MAX_MISSIONS_AVAILABLE_TU iMissionIndex
		IF g_availableMissionsTU[iMissionIndex].index <> -1
			iMissionID = g_flowUnsaved.coreVars[g_availableMissionsTU[iMissionIndex].index].iValue1
#ENDIF
#IF NOT USE_CLF_DLC
#IF NOT USE_NRM_DLC
    REPEAT MAX_MISSIONS_AVAILABLE iMissionIndex
		IF g_availableMissions[iMissionIndex].index <> -1
			iMissionID = g_flowUnsaved.coreVars[g_availableMissions[iMissionIndex].index].iValue1
#ENDIF
#ENDIF

			IF INT_TO_ENUM(SP_MISSIONS, iMissionID) != SP_MISSION_NONE
				IF g_sMissionActiveData[iMissionID].leaveArea

					//Yes... Does this mission have a blip set?
					IF g_sMissionStaticData[iMissionID].blip <> STATIC_BLIP_NAME_DUMMY_FINAL
						//Yes blip set, is the mission processing a replay?
						IF g_sMissionStaticData[iMissionID].coreVariablesIndex != g_replay.replayCoreVarsIndex // This mission isn't being processed by the replay controller
						OR NOT IS_REPLAY_BEING_PROCESSED()
						
							//No replay, is the player out of range of the blip?
							IF NOT IS_PED_INJURED(PLAYER_PED_ID())
								IF GET_CURRENT_PLAYER_PED_INT() <= ENUM_TO_INT(CHAR_TREVOR)
									STATIC_BLIP_NAME_ENUM eBlip = g_sMissionStaticData[iMissionID].blip
									VECTOR vBlipPos
									IF IS_BIT_SET(g_GameBlips[eBlip].iSetting,STATIC_BLIP_SETTING_MULTI_COORD_AND_SPRITE)
										vBlipPos = g_GameBlips[eBlip].vCoords[GET_CURRENT_PLAYER_PED_INT()]
									ELSE
										vBlipPos = g_GameBlips[eBlip].vCoords[0]
									ENDIF
									
									//Is this a blipped mission or a trigger scene mission?
									FLOAT fLeaveAreaDistance = g_sMissionActiveData[iMissionID].leaveAreaDistance
									FLOAT fLeaveAreaDistanceSquared = fLeaveAreaDistance * fLeaveAreaDistance
									
									//Render a leave area sphere this frame if trigger debug is turned on.
									#IF IS_DEBUG_BUILD
										IF g_bTriggerDebugOn
											DRAW_DEBUG_SPHERE(vBlipPos, fLeaveAreaDistance, 255, 0, 0, 65)
											DRAW_DEBUG_TEXT(GET_SP_MISSION_DISPLAY_STRING_FROM_ID(INT_TO_ENUM(SP_MISSIONS, iMissionID)), vBlipPos, 0, 0, 255, 255)
											DRAW_DEBUG_TEXT("Leave Area", <<vBlipPos.X-fLeaveAreaDistance, vBlipPos.Y, vBlipPos.Z>>, 255, 0, 0, 255)
										ENDIF
									#ENDIF

									FLOAT fDistanceSquaredFromTrigger = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vBlipPos)
									IF fDistanceSquaredFromTrigger > fLeaveAreaDistanceSquared
									OR IS_BIT_SET(g_iExtraMissionFlags[iMissionID], MF_EXTRA_INDEX_TEMP_ZERO_LEAVE_AREA)
										//Yes... Unflag the leave area flag.
										g_sMissionActiveData[iMissionID].leaveArea = FALSE
										g_bMichaelHasBeenDisrupted = FALSE
										
										#IF IS_DEBUG_BUILD
											CPRINTLN(DEBUG_TRIGGER, GET_THIS_SCRIPT_NAME(), " unflagged leave area flag for mission ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(INT_TO_ENUM(SP_MISSIONS, iMissionID)), ".")
										#ENDIF
										
										IF IS_BIT_SET(g_iExtraMissionFlags[iMissionID], MF_EXTRA_INDEX_TEMP_ZERO_LEAVE_AREA)
											CPRINTLN(DEBUG_TRIGGER, "Leave area unflagged due to zero distance bit being set. Clearing bit.")
											CLEAR_BIT(g_iExtraMissionFlags[iMissionID], MF_EXTRA_INDEX_TEMP_ZERO_LEAVE_AREA)
										ENDIF
										
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						//No blip set, automatically unflag the leave area flag.
						g_sMissionActiveData[iMissionID].leaveArea = FALSE
						#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_TRIGGER, GET_THIS_SCRIPT_NAME(), " unflagged leave area flag for mission ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(INT_TO_ENUM(SP_MISSIONS, iMissionID)), " as it has no blip assigned.")
						#ENDIF
					ENDIF 
				ENDIF
			ENDIF
		ENDIF
    ENDREPEAT
ENDPROC



// ===========================================================================================================
//		Mission Weather Condition Checking
// ===========================================================================================================

// PURPOSE: Periodically check if the player is near a mission blip that requires a certain weather condition
// 			to be active. WIP!
PROC UPDATE_FLOW_WEATHER_CONDITIONS()

	STRING strMissionWeather = GET_SP_MISSION_WEATHER_CONDITION(INT_TO_ENUM(SP_MISSIONS,iFrameMissionIndex))

	//Does the mission have a specific weather type stored?
	IF NOT ARE_STRINGS_EQUAL(strMissionWeather, "NONE")
	
		//Yes... Is the mission registered as avilable?
		INT iAvailableMissionIndex
#IF USE_CLF_DLC
		REPEAT MAX_MISSIONS_AVAILABLE_TU iAvailableMissionIndex
			IF g_availableMissionsTU[iAvailableMissionIndex].index = g_sMissionStaticData[iFrameMissionIndex].coreVariablesIndex
#ENDIF
#IF USE_NRM_DLC
		REPEAT MAX_MISSIONS_AVAILABLE_TU iAvailableMissionIndex
			IF g_availableMissionsTU[iAvailableMissionIndex].index = g_sMissionStaticData[iFrameMissionIndex].coreVariablesIndex
#ENDIF
#IF NOT USE_CLF_DLC
#IF NOT USE_NRM_DLC
		REPEAT MAX_MISSIONS_AVAILABLE iAvailableMissionIndex
			IF g_availableMissions[iAvailableMissionIndex].index = g_sMissionStaticData[iFrameMissionIndex].coreVariablesIndex
#ENDIF
#ENDIF
//Yes... Is the player close to the mission blip?
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					IF VDIST2(GET_STATIC_BLIP_POSITION(g_sMissionStaticData[iFrameMissionIndex].blip), GET_ENTITY_COORDS(PLAYER_PED_ID())) < 500000
						//Yes... Does the weather need changing?
						INT iCurrentWeatherHash = GET_NEXT_WEATHER_TYPE_HASH_NAME()
						INT iMissionWeatherHash = GET_HASH_KEY(strMissionWeather)
						IF iCurrentWeatherHash != iMissionWeatherHash
							//Yes... Change the weather!
							CPRINTLN(DEBUG_FLOW, "Changing weather type!")
							SET_WEATHER_TYPE_NOW_PERSIST(strMissionWeather)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT

	ENDIF

ENDPROC



// ===========================================================================================================
//		Mission intro cutscene pre-loading.
// ===========================================================================================================

// PURPOSE: Checks if any mission intro cutscenes have been flagged to be pre-loaded by the flow controller.
//			Once loaded this cutscene will be handed to the mission script when it triggers to facilitate an
//			instant and seamless launch.
PROC UPDATE_MISSION_INTRO_LOADING()

	IF g_bFlowLoadIntroCutscene
		IF NOT ARE_STRINGS_EQUAL(g_txtIntroMocapToLoad, "NONE")
			IF g_iFlowIntroCutsceneRequestID = NULL_OFFMISSION_CUTSCENE_REQUEST
				//Request permission to load an off-mission cutscene with the cutscene_controller.
				CPRINTLN(DEBUG_FLOW, "Registering a request to load a cutscene with the cutscene controller.")
				REGISTER_OFFMISSION_CUTSCENE_REQUEST(g_iFlowIntroCutsceneRequestID, OCT_STORY_MISSION)
				CPRINTLN(DEBUG_FLOW, "Returned ID was ", g_iFlowIntroCutsceneRequestID, ".")
			ELSE
				enumCharacterList ePlayer
				CUTSCENE_SECTION eSections
			
				SWITCH GET_OFFMISSION_CUTSCENE_REQURIED_ACTION(g_iFlowIntroCutsceneRequestID)
					CASE OCA_LOAD
						IF NOT IS_CUTSCENE_ACTIVE()
							ePlayer = GET_CURRENT_PLAYER_PED_ENUM()
							IF IS_PLAYER_PED_PLAYABLE(ePlayer)
								IF g_iCharIntroSectionsToLoad[ePlayer] != -1
									CPRINTLN(DEBUG_FLOW, "Started loading intro cutscene ",g_txtIntroMocapToLoad , " for mission ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(g_eMissionIDToLoadCutscene), ".")
									REQUEST_SCRIPT_WITH_NAME_HASH(g_sMissionStaticData[g_eMissionIDToLoadCutscene].scriptHash)
									IF g_iCharIntroSectionsToLoad[ePlayer] = CS_ALL
										CPRINTLN(DEBUG_FLOW, "Requesting full cutscene.")
										REQUEST_CUTSCENE(g_txtIntroMocapToLoad)
									ELSE
										CPRINTLN(DEBUG_FLOW, 	"Requesting cutscene sections [", 
																IS_BIT_SET(g_iCharIntroSectionsToLoad[ePlayer], 7),
																IS_BIT_SET(g_iCharIntroSectionsToLoad[ePlayer], 6),
																IS_BIT_SET(g_iCharIntroSectionsToLoad[ePlayer], 5),
																IS_BIT_SET(g_iCharIntroSectionsToLoad[ePlayer], 4),
																IS_BIT_SET(g_iCharIntroSectionsToLoad[ePlayer], 3),
																IS_BIT_SET(g_iCharIntroSectionsToLoad[ePlayer], 2),
																IS_BIT_SET(g_iCharIntroSectionsToLoad[ePlayer], 1),
																IS_BIT_SET(g_iCharIntroSectionsToLoad[ePlayer], 0),
																"].")					
										eSections = INT_TO_ENUM(CUTSCENE_SECTION, g_iCharIntroSectionsToLoad[ePlayer])
										REQUEST_CUTSCENE_WITH_PLAYBACK_LIST(g_txtIntroMocapToLoad, eSections)
									ENDIF
									
									//Disable SRL map pre-streaming for off-mission prestreamed cutscenes. Stops the world streaming
									//from being choked and allows us to run switch cams with the cutscenes in memory.
									IF g_eMissionIDToLoadCutscene != SP_MISSION_NONE
										IF IS_BIT_SET(g_iExtraMissionFlags[g_eMissionIDToLoadCutscene], MF_EXTRA_INDEX_PRESTREAM_ON)
											CPRINTLN(DEBUG_FLOW, "Enabling SRL map pre-streaming for cutscene. Mission flagged to enable prestreaming")
											SET_SRL_FORCE_PRESTREAM(SRL_PRESTREAM_FORCE_ON)
										ELSE
											IF IS_BIT_SET(g_sMissionStaticData[g_eMissionIDToLoadCutscene].settingsBitset, MF_INDEX_PRESTREAM_FULLY_OFF)
												CPRINTLN(DEBUG_FLOW, "Disabling SRL map pre-streaming for cutscene. Mission flagged to completely disable prestreaming.")
												SET_SRL_FORCE_PRESTREAM(SRL_PRESTREAM_FORCE_COMPLETELY_OFF)
											ELSE
												CPRINTLN(DEBUG_FLOW, "Disabling SRL map pre-streaming for cutscene.")
												SET_SRL_FORCE_PRESTREAM(SRL_PRESTREAM_FORCE_OFF)
											ENDIF
										ENDIF
									ELSE
										CPRINTLN(DEBUG_FLOW, "Disabling SRL map pre-streaming for cutscene.")
										SET_SRL_FORCE_PRESTREAM(SRL_PRESTREAM_FORCE_OFF)
									ENDIF

									g_bFlowLoadingIntroCutscene = TRUE
									g_bFlowLoadRequestStarted = TRUE
									SET_OFFMISSION_CUTSCENE_ACTIVE(g_iFlowIntroCutsceneRequestID, TRUE)
								ENDIF
							ELSE
								CERRORLN(DEBUG_FLOW, "UPDATE_MISSION_INTRO_LOADING: Tried to start preloading a mission cutscene while not a playable character. Bug BenR.")
							ENDIF
						ELSE
							CPRINTLN(DEBUG_FLOW, "Waiting for another cutscene to clean up before requesting mission intro cutscene.")
						ENDIF
					BREAK
					
					CASE OCA_UNLOAD
						CPRINTLN(DEBUG_FLOW, "Was asked to unload intro cutscene ",g_txtIntroMocapToLoad, " for mission ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(g_eMissionIDToLoadCutscene), ".")
						REMOVE_CUTSCENE()
						CPRINTLN(DEBUG_FLOW, "Re-enabling SRL map pre-streaming for cutscenes as cutscene unloads.")
						SET_SRL_FORCE_PRESTREAM(SRL_PRESTREAM_DEFAULT)
						g_bFlowLoadingIntroCutscene = FALSE
						g_bFlowLoadRequestStarted = FALSE
						SET_OFFMISSION_CUTSCENE_ACTIVE(g_iFlowIntroCutsceneRequestID, FALSE)
					BREAK
				ENDSWITCH
			ENDIF
			
			//Can we set any variations or props this frame?
			IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
				IF NOT IS_CUTSCENE_PLAYBACK_FLAG_SET(CUTSCENE_REQUESTED_FROM_WIDGET)
				
					//Process any outstanding ped based variation requests.
					WHILE g_iFlowCurrentPedVariationCount > 0
						INT iVariationIndex = g_iFlowCurrentPedVariationCount - 1
						#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_FLOW,"Processing ped variation request for ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(g_eMissionIDToLoadCutscene), 
												": Handle[", g_txtFlowCutsceneHandles[g_sFlowPedVariation[iVariationIndex].iHandleIndex], "].")
						#ENDIF
						//Make the request.
						IF NOT IS_PED_INJURED(g_sFlowPedVariation[iVariationIndex].ped)
							SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE(	g_txtFlowCutsceneHandles[g_sFlowPedVariation[iVariationIndex].iHandleIndex],
																			g_sFlowPedVariation[iVariationIndex].ped)
						ELSE
							SCRIPT_ASSERT("The flow controller tried to set cutscene variations from a ped that is dead or doesn't exist. Bug BenR.")
						ENDIF
						//Clean up array index.
						g_sFlowPedVariation[iVariationIndex].iHandleIndex = -1
						g_sFlowPedVariation[iVariationIndex].ped = NULL
						g_iFlowCurrentPedVariationCount--
					ENDWHILE
					
					//Process any outstanding outfit based variation requests.
					WHILE g_iFlowCurrentCutsceneOutfitCount > 0
						INT iOutfitIndex = g_iFlowCurrentCutsceneOutfitCount - 1
						#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_FLOW,"Processing outfit variation request for ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(g_eMissionIDToLoadCutscene), 
												": Handle[", g_txtFlowCutsceneHandles[g_sFlowCutsceneOutfit[iOutfitIndex].iHandleIndex], 
												"] Model[", ENUM_TO_INT(g_sFlowCutsceneOutfit[iOutfitIndex].eModel),
												"] Outfit[", ENUM_TO_INT(g_sFlowCutsceneOutfit[iOutfitIndex].eOutfit), "].")
						#ENDIF
						//Make the request.
						IF g_sFlowCutsceneOutfit[iOutfitIndex].eOutfit != DUMMY_PED_COMP
							SET_CUTSCENE_PED_OUTFIT(g_txtFlowCutsceneHandles[g_sFlowCutsceneOutfit[iOutfitIndex].iHandleIndex], 
													g_sFlowCutsceneOutfit[iOutfitIndex].eModel, 
													g_sFlowCutsceneOutfit[iOutfitIndex].eOutfit)
						ELSE
							SCRIPT_ASSERT("The flow controller tried to set cutscene variations for an invalid outfit enum. Bug BenR.")
						ENDIF
						//Clean up array index.
						g_sFlowCutsceneOutfit[iOutfitIndex].iHandleIndex = -1
						g_sFlowCutsceneOutfit[iOutfitIndex].eModel = DUMMY_MODEL_FOR_SCRIPT
						g_sFlowCutsceneOutfit[iOutfitIndex].eOutfit = DUMMY_PED_COMP
						g_iFlowCurrentCutsceneOutfitCount--
					ENDWHILE
			
					//Process any outstanding variation requests.
					WHILE g_iFlowCurrentCutsceneVariationCount > 0
						INT iVariationIndex = g_iFlowCurrentCutsceneVariationCount - 1
						#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_FLOW,"Processing cutscene variation request for ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(g_eMissionIDToLoadCutscene), 
												": Handle[", g_txtFlowCutsceneHandles[g_sFlowCutsceneVariations[iVariationIndex].iHandleIndex],
												"] Comp[", ENUM_TO_INT(g_sFlowCutsceneVariations[iVariationIndex].eComponent),
												"] Draw[", g_sFlowCutsceneVariations[iVariationIndex].iDrawable,
												"] Text[", g_sFlowCutsceneVariations[iVariationIndex].iTexture, "].")
						#ENDIF
						//Make the request.
						SET_CUTSCENE_PED_COMPONENT_VARIATION(	g_txtFlowCutsceneHandles[g_sFlowCutsceneVariations[iVariationIndex].iHandleIndex],
																g_sFlowCutsceneVariations[iVariationIndex].eComponent,
																g_sFlowCutsceneVariations[iVariationIndex].iDrawable,
																g_sFlowCutsceneVariations[iVariationIndex].iTexture)
						//Clean up array index.
						g_sFlowCutsceneVariations[iVariationIndex].iHandleIndex = -1
						g_sFlowCutsceneVariations[iVariationIndex].eComponent = PED_COMP_HEAD
						g_sFlowCutsceneVariations[iVariationIndex].iDrawable = 0
						g_sFlowCutsceneVariations[iVariationIndex].iTexture = 0
						g_iFlowCurrentCutsceneVariationCount--
					ENDWHILE

					//Process any outstanding prop requests.
					WHILE g_iFlowCurrentCutscenPropCount > 0
						INT iPropIndex = g_iFlowCurrentCutscenPropCount - 1
						#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_FLOW,"Processing ped variation request for ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(g_eMissionIDToLoadCutscene), 
												": Handle[", g_txtFlowCutsceneHandles[g_sFlowCutsceneProp[iPropIndex].iHandleIndex], 
												"] Position[", ENUM_TO_INT(g_sFlowCutsceneProp[iPropIndex].ePosition),
												"] PropIndex[", g_sFlowCutsceneProp[iPropIndex].iPropIndex,
												"] TextureIndex[", g_sFlowCutsceneProp[iPropIndex].iTextureIndex, "].")
						#ENDIF
						//Make the request.
						SET_CUTSCENE_PED_PROP_VARIATION(	g_txtFlowCutsceneHandles[g_sFlowCutsceneProp[iPropIndex].iHandleIndex],
															g_sFlowCutsceneProp[iPropIndex].ePosition,
															g_sFlowCutsceneProp[iPropIndex].iPropIndex,
															g_sFlowCutsceneProp[iPropIndex].iTextureIndex)
						//Clean up array index.
						g_sFlowCutsceneProp[iPropIndex].iHandleIndex = -1
						g_sFlowCutsceneProp[iPropIndex].ePosition = ANCHOR_HEAD
						g_sFlowCutsceneProp[iPropIndex].iPropIndex = 0
						g_sFlowCutsceneProp[iPropIndex].iTextureIndex = 0
						g_iFlowCurrentCutscenPropCount--
					ENDWHILE
				ENDIF
				
				CPRINTLN(DEBUG_FLOW,"Intro cutscene variation requests were made.")
				g_bFlowIntroVariationRequestsMade = TRUE
				PRIVATE_Cleanup_Cutscene_Handles()
			ENDIF
		ELSE
			CERRORLN(DEBUG_FLOW_COMMANDS, "UPDATE_MISSION_INTRO_LOADING: The flow was flagged to start loading a mission intro that has no intro cutscene defined. Bug BenR.") 
		ENDIF
	ENDIF
	
	//Check for the flow being requested to unload.
	IF g_bFlowCleanupIntroCutscene
		CPRINTLN(DEBUG_FLOW, "Flow requested to clean up any pre-loaded cutscenes.")
		
		IF g_eMissionIDToLoadCutscene != SP_MISSION_NONE
			SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(g_sMissionStaticData[g_eMissionIDToLoadCutscene].scriptHash)
		ENDIF
		
		//Reset flow cutscene pre-loading procedures.
		g_eMissionIDToLoadCutscene			= SP_MISSION_NONE
		g_iCharIntroSectionsToLoad[0]		= CS_NONE
		g_iCharIntroSectionsToLoad[1]		= CS_NONE
		g_iCharIntroSectionsToLoad[2]		= CS_NONE
		g_bFlowLoadIntroCutscene 			= FALSE
		g_bFlowIntroVariationRequestsMade 	= FALSE
		g_bFlowLoadingIntroCutscene 		= FALSE
		g_bFlowLoadRequestStarted			= FALSE
		
		IF g_iFlowIntroCutsceneRequestID != NULL_OFFMISSION_CUTSCENE_REQUEST
			REMOVE_CUTSCENE()
			CPRINTLN(DEBUG_FLOW, "Re-enabling SRL map pre-streaming for cutscenes as cutscene unloads.")
			SET_SRL_FORCE_PRESTREAM(SRL_PRESTREAM_DEFAULT)
			END_OFFMISSION_CUTSCENE_REQUEST(g_iFlowIntroCutsceneRequestID)
		ENDIF
		
		//Clean out all variation requests.
		INT index
		REPEAT g_iFlowCurrentCutsceneVariationCount index
			g_sFlowCutsceneVariations[index].iHandleIndex = -1
			g_sFlowCutsceneVariations[index].eComponent = PED_COMP_HEAD
			g_sFlowCutsceneVariations[index].iDrawable = 0
			g_sFlowCutsceneVariations[index].iTexture = 0
		ENDREPEAT
		g_iFlowCurrentCutsceneVariationCount = 0
		
		REPEAT g_iFlowCurrentCutsceneOutfitCount index
			g_sFlowCutsceneOutfit[index].iHandleIndex = -1
			g_sFlowCutsceneOutfit[index].eModel = DUMMY_MODEL_FOR_SCRIPT
			g_sFlowCutsceneOutfit[index].eOutfit = DUMMY_PED_COMP
		ENDREPEAT
		g_iFlowCurrentCutsceneOutfitCount = 0
		
		REPEAT g_iFlowCurrentPedVariationCount index
			g_sFlowPedVariation[index].iHandleIndex = -1
			g_sFlowPedVariation[index].ped = NULL
		ENDREPEAT
		g_iFlowCurrentPedVariationCount = 0
		
		REPEAT g_iFlowCurrentCutscenPropCount index
			g_sFlowCutsceneProp[index].iHandleIndex = -1
			g_sFlowCutsceneProp[index].ePosition = ANCHOR_HEAD
			g_sFlowCutsceneProp[index].iPropIndex = 0
			g_sFlowCutsceneProp[index].iTextureIndex = 0
		ENDREPEAT
		g_iFlowCurrentCutscenPropCount = 0
		
		PRIVATE_Cleanup_Cutscene_Handles()
		
		REPEAT 3 index
			g_iFlowBitsetBlockPlayerCutsceneVariations[index] = 0
		ENDREPEAT

		g_bFlowCleanupIntroCutscene = FALSE
	ENDIF
	
	//Track when the cutscene finishes loading.
	IF g_bFlowLoadingIntroCutscene
		IF g_eMissionIDToLoadCutscene != SP_MISSION_NONE
			IF IS_BIT_SET(g_iExtraMissionFlags[g_eMissionIDToLoadCutscene], MF_EXTRA_INDEX_PRESTREAM_ON)
				SET_SRL_FORCE_PRESTREAM(SRL_PRESTREAM_FORCE_ON)
			ELIF IS_BIT_SET(g_sMissionStaticData[g_eMissionIDToLoadCutscene].settingsBitset, MF_INDEX_PRESTREAM_FULLY_OFF)
				SET_SRL_FORCE_PRESTREAM(SRL_PRESTREAM_FORCE_COMPLETELY_OFF)
			ELSE
				SET_SRL_FORCE_PRESTREAM(SRL_PRESTREAM_FORCE_OFF)
			ENDIF
		ELSE
			SET_SRL_FORCE_PRESTREAM(SRL_PRESTREAM_FORCE_OFF)
		ENDIF
		IF HAS_THIS_CUTSCENE_LOADED(g_txtIntroMocapToLoad)
			g_bFlowLoadingIntroCutscene = FALSE
			g_bFlowLoadIntroCutscene = FALSE
			CPRINTLN(DEBUG_FLOW, "Finished loading intro cutscene ", g_txtIntroMocapToLoad, " for mission ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(g_eMissionIDToLoadCutscene), ".")
		ENDIF
	ENDIF
	
ENDPROC


// ===========================================================================================================
//		Monitor Michael's stats for the Shrink minigame.
// ===========================================================================================================
#if not USE_CLF_DLC
#if not USE_NRM_DLC	
CONST_INT	SHRINK_STAT_UPDATE_DELAY	2000	//Time between each stat update. Optimisation.
INT 		m_iTimeNextShrinkStatUpdate 	= 0

INT iShrinkRandomKills, iShrinkTotalKills, iShrinkStolenVehicles, iShrinkAccidents
BOOL bShrinkTrackingInitialized


PROC UPDATE_SHRINK_STAT_TRACKING()
	INT iCurInnocents, iCurCopKills, iCurSwatKills, iCurCrimKills
	INT iCurStolenCars, iCurStolenBikes, iCurStolenQuads
	INT iCurLargeAccidents
	INT iCounter
	
	// Initialize tracking values (do once).
	IF NOT bShrinkTrackingInitialized
		STAT_GET_INT(SP0_KILLS_INNOCENTS, iCurInnocents)
		STAT_GET_INT(SP0_KILLS_COP, iCurCopKills)
		STAT_GET_INT(SP0_KILLS_SWAT, iCurSwatKills)
//		STAT_GET_INT(SP0_KILLS_CRIMINALS, iCurCrimKills)
		STAT_GET_INT(SP0_NUMBER_STOLEN_CARS, iCurStolenCars)
		STAT_GET_INT(SP0_NUMBER_STOLEN_BIKES, iCurStolenBikes)
		STAT_GET_INT(SP0_NUMBER_STOLEN_QUADBIKES, iCurStolenQuads)
		STAT_GET_INT(SP0_LARGE_ACCIDENTS, iCurLargeAccidents)
		
		iShrinkRandomKills = iCurInnocents
		iShrinkTotalKills = iCurInnocents + iCurCopKills + iCurSwatKills + iCurCrimKills
		iShrinkStolenVehicles = iCurStolenCars + iCurStolenBikes + iCurStolenQuads
		iShrinkAccidents = iCurLargeAccidents
		
		bShrinkTrackingInitialized = TRUE
	ENDIF
	
	// Update Michael hitting people in cars (this must run once per frame).
//	IF GET_NEW_MICHAEL_CAR_STRIKES() > 0
//		PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "You have RUN SOMEONE OVER. Dr. Friedlander will not be pleased.")
//		SHRINK_ADD_OW_VIOLENCE_TIMESTAMP(SHRINK_VEHICLE_KILLS)
//	ENDIF
	
	// Update other behaviors (can be throttled).
	IF GET_GAME_TIMER() > m_iTimeNextShrinkStatUpdate
		SHRINK_REMOVE_OLD_SEX_ACT_TIMESTAMPS()
		SHRINK_REMOVE_OLD_LAP_DANCE_TIMESTAMPS()
		SHRINK_REMOVE_OLD_STRIP_CLUB_TIMESTAMP()
		SHRINK_REMOVE_OW_VIOLENCE_TIMESTAMPS()
		
		// Get new stat totals
		STAT_GET_INT(SP0_KILLS_INNOCENTS, iCurInnocents)
		STAT_GET_INT(SP0_KILLS_COP, iCurCopKills)
		STAT_GET_INT(SP0_KILLS_SWAT, iCurSwatKills)
//		STAT_GET_INT(SP0_KILLS_CRIMINALS, iCurCrimKills)
		STAT_GET_INT(SP0_NUMBER_STOLEN_CARS, iCurStolenCars)
		STAT_GET_INT(SP0_NUMBER_STOLEN_BIKES, iCurStolenBikes)
		STAT_GET_INT(SP0_NUMBER_STOLEN_QUADBIKES, iCurStolenQuads)
		STAT_GET_INT(SP0_LARGE_ACCIDENTS, iCurLargeAccidents)
		
		// Check for new innocent/random kills
		IF iShrinkRandomKills < iCurInnocents
			//PRINT_STRING_WITH_LITERAL_STRING("STRING", "You have KILLED A NEW INNOCENT. Dr. Friedlander will not be pleased.", DEFAULT_GOD_TEXT_TIME, 1)
			REPEAT (iCurInnocents - iShrinkRandomKills) iCounter
				SHRINK_ADD_OW_VIOLENCE_TIMESTAMP(SHRINK_RANDOM_KILLS)
			ENDREPEAT
			iShrinkRandomKills = iCurInnocents
		ENDIF
		
		// Check for new kills of any type
		IF iShrinkTotalKills < iCurInnocents + iCurCopKills + iCurSwatKills + iCurCrimKills
			//PRINT_STRING_WITH_LITERAL_STRING("STRING", "You have KILLED SOMEONE. Dr. Friedlander will not be pleased.", DEFAULT_GOD_TEXT_TIME, 1)
			REPEAT (iCurInnocents + iCurCopKills + iCurSwatKills + iCurCrimKills - iShrinkTotalKills) iCounter
				SHRINK_ADD_OW_VIOLENCE_TIMESTAMP(SHRINK_KILLS)
			ENDREPEAT
			iShrinkTotalKills = iCurInnocents + iCurCopKills + iCurSwatKills + iCurCrimKills
		ENDIF
		
		// Check for new stolen vehicles
		IF iShrinkStolenVehicles < iCurStolenCars + iCurStolenBikes + iCurStolenQuads
			//PRINT_STRING_WITH_LITERAL_STRING("STRING", "You have STOLEN A NEW VEHICLE. Dr. Friedlander will not be pleased.", DEFAULT_GOD_TEXT_TIME, 1)
			REPEAT (iCurStolenCars + iCurStolenBikes + iCurStolenQuads - iShrinkStolenVehicles) iCounter
				SHRINK_ADD_OW_VIOLENCE_TIMESTAMP(SHRINK_STOLEN_CARS)
			ENDREPEAT
			iShrinkStolenVehicles = iCurStolenCars + iCurStolenBikes + iCurStolenQuads
		ENDIF
		
		// Check for new traffic accidents
		IF iShrinkAccidents < iCurLargeAccidents
			//PRINT_STRING_WITH_LITERAL_STRING("STRING", "You have been in a MAJOR ACCIDENT. Dr. Friedlander will not be pleased.", DEFAULT_GOD_TEXT_TIME, 1)
			REPEAT (iCurLargeAccidents - iShrinkAccidents) iCounter
				SHRINK_ADD_OW_VIOLENCE_TIMESTAMP(SHRINK_DANGEROUS_DRIVING)
			ENDREPEAT
			iShrinkAccidents = iCurLargeAccidents
		ENDIF
		
		// Check for wanted levels.
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
		AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) >= 4
				SHRINK_ADD_OW_VIOLENCE_TIMESTAMP(SHRINK_4STAR_WANTED)
			ENDIF
			
			IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) >= 2
			AND (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY("ShopRobberies")) <= 0
			OR SHRINK_GET_OW_VIOLENCE_TIMESTAMP_COUNT(SHRINK_SHOP_ROBBERY) <= 0)
				SHRINK_ADD_OW_VIOLENCE_TIMESTAMP(SHRINK_2STAR_WANTED)
			ENDIF
			
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			AND GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) >= 1
				CERRORLN(DEBUG_MISSION, "Adding timestamp for being wanted in a car!")
				SHRINK_ADD_OW_VIOLENCE_TIMESTAMP(SHRINK_WANTED_IN_CAR)
			ENDIF
		ENDIF
		
		m_iTimeNextShrinkStatUpdate += SHRINK_STAT_UPDATE_DELAY
	ENDIF
ENDPROC
#endif
#endif

// ===========================================================================================================
//		Flow Custom Master Procedures
// ===========================================================================================================

// Emergency check to catch the flow controller starting up without the flow having being initialised.
// Could potentially happen if we auto switch in to MP before SP ever runs.
PROC CHECK_FOR_NEW_GAME_FLOW_START()

	#if USE_CLF_DLC
		IF NOT IS_BIT_SET(g_savedGlobalsClifford.sFlow.strandSavedVars[STRAND_CLF].savedBitflags, SAVED_BITS_STRAND_ACTIVATED)
			CPRINTLN(DEBUG_INIT_SP, "<clf>Flow controller running while the flow hasn't been activated.")
			CPRINTLN(DEBUG_INIT_SP, "<clf>Automatically starting the CLF strand.")
			SET_BIT(g_savedGlobalsClifford.sFlow.strandSavedVars[STRAND_CLF].savedBitflags, SAVED_BITS_STRAND_ACTIVATED)
			IF NOT IS_SCREEN_FADED_OUT()
				IF NOT IS_SCREEN_FADING_OUT()
					CPRINTLN(DEBUG_INIT_SP, "<clf>Fading screen out in preparation for lead-in to CLF mission.")
					DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
				ENDIF
			ENDIF
		ENDIF
	#endif
	#if USE_NRM_DLC
		IF NOT IS_BIT_SET(g_savedGlobalsnorman.sFlow.strandSavedVars[STRAND_NRM_SURVIVE].savedBitflags, SAVED_BITS_STRAND_ACTIVATED)
			CPRINTLN(DEBUG_INIT_SP, "<NRM>Flow controller running while the flow hasn't been activated.")
			CPRINTLN(DEBUG_INIT_SP, "<NRM>Automatically starting the CLF strand.")
			SET_BIT(g_savedGlobalsnorman.sFlow.strandSavedVars[STRAND_NRM_SURVIVE].savedBitflags, SAVED_BITS_STRAND_ACTIVATED)
			IF NOT IS_SCREEN_FADED_OUT()
				IF NOT IS_SCREEN_FADING_OUT()
					CPRINTLN(DEBUG_INIT_SP, "<NRM>Fading screen out in preparation for lead-in to CLF mission.")
					DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
				ENDIF
			ENDIF
		ENDIF
	#endif
	
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
		IF NOT IS_BIT_SET(g_savedGlobals.sFlow.strandSavedVars[STRAND_PROLOGUE].savedBitflags, SAVED_BITS_STRAND_ACTIVATED)
			CPRINTLN(DEBUG_INIT_SP, "Flow controller running while the flow hasn't been activated.")
			CPRINTLN(DEBUG_INIT_SP, "Automatically starting the Prologue strand.")
			SET_BIT(g_savedGlobals.sFlow.strandSavedVars[STRAND_PROLOGUE].savedBitflags, SAVED_BITS_STRAND_ACTIVATED)
			IF NOT IS_SCREEN_FADED_OUT()
				IF NOT IS_SCREEN_FADING_OUT()
					CPRINTLN(DEBUG_INIT_SP, "Fading screen out in preparation for lead-in to Prologue mission.")
					DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
				ENDIF
			ENDIF
		ENDIF
	#endif
	#endif
		
ENDPROC


PROC FLOW_RUN_CUSTOM_UPDATE()
	iFrameMissionIndex++
	#if USE_CLF_DLC
		IF iFrameMissionIndex >= ENUM_TO_INT(SP_MISSION_MAX_CLF)
			iFrameMissionIndex = 0
		ENDIF
	#endif
	#if USE_NRM_DLC
		IF iFrameMissionIndex >= ENUM_TO_INT(SP_MISSION_MAX_NRM)
			iFrameMissionIndex = 0
		ENDIF
	#endif
	
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
		IF iFrameMissionIndex >= ENUM_TO_INT(SP_MISSION_MAX)
			iFrameMissionIndex = 0
		ENDIF
	#endif
	#endif
	
	#IF IS_DEBUG_BUILD
	IF NOT GET_COMMANDLINE_PARAM_EXISTS("DisableSPStartFromMP")
	#ENDIF
		CHECK_FOR_NEW_GAME_FLOW_START()
	#IF IS_DEBUG_BUILD
	ENDIF
	#ENDIF

	UPDATE_LEAVE_AREA_FLAGS()
	UPDATE_MISSION_INTRO_LOADING()
	IF g_iFlowDisplayMissionTitle > -1
		IF g_eMissionTitleType = CP_GROUP_MISSIONS
			// only do this if we're displaying a story mission title
			
			// see if this mission needs a special case position for title
			BOOL bTimeLimit = FALSE
			BOOL bTimerBar = FALSE
			
			#if not USE_CLF_DLC
			#if not USE_NRM_DLC
				SP_MISSIONS eMission = INT_TO_ENUM(SP_MISSIONS, g_iFlowDisplayMissionTitle)
				IF eMission = SP_HEIST_FINALE_1
				OR eMission = SP_MISSION_ASSASSIN_2 
				OR eMission = SP_HEIST_FINALE_2A
				OR eMission = SP_MISSION_ME_AMANDA
					bTimeLimit = TRUE
				ENDIF
			#endif
			#endif
			UPDATE_MISSION_NAME_DISPLAYING(g_txtIntroMocapToLoad, FALSE, FALSE, bTimeLimit, bTimerBar)
		ENDIF
	ENDIF
	
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
		UPDATE_SHRINK_STAT_TRACKING()
	#endif
	#endif

ENDPROC


PROC FLOW_RUN_CUSTOM_CLEANUP()
	//Clean up any flow loaded cutscenes as the flow controller terminates.
	g_bFlowCleanupIntroCutscene = TRUE
	UPDATE_MISSION_INTRO_LOADING()
ENDPROC

PROC FLOW_RUN_CUSTOM_CLEANUP_ON_MP_TRANSITION()
	
	//This flag is used to decide behaviour as we fail out of a SP mission. Any mission exit is being 
	//skipped as we move to MP so we need to reset this flag's state. -BenR
	g_bReplayDoRejectWarp = FALSE
	
	//game timer when transitioning to MP is stored to see how long it is since we were last in SP.
	//Used to decide if its sensible to switch back to the same character you were previously (e.g. Michael>MP>Michael)
	g_iGametimeMPSPTransition = GET_GAME_TIMER()

	APPLY_STRAND_POINTER_OVERRIDES(TRUE)
ENDPROC

