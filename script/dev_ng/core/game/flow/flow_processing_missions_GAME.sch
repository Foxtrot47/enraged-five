USING "rage_builtins.sch"
USING "globals.sch"
//depot/gta5/script/dev/singleplayer/include/public/mission_control_public.sch
#IF IS_DEBUG_BUILD
	USING "flow_debug_GAME.sch"
	USING "commands_debug.sch"
#ENDIF

USING "commands_cutscene.sch"
USING "flow_private_core.sch"
USING "flow_private_game.sch"
USING "flow_mission_data_public.sch"
USING "selector_public.sch"

#IF NOT USE_SP_DLC
USING "heist_private.sch"
#ENDIF

USING "friends_public.sch"
USING "mission_stat_public.sch"
USING "replay_public.sch"
USING "flow_help_public.sch"
USING "respawn_location_private.sch"
USING "shop_public.sch"
USING "blip_control_public.sch"
USING "building_control_public.sch"
USING "code_control_public.sch"
USING "randomchar_private.sch"
USING "organiser_public.sch"
USING "presence_enum.sch"
USING "locates_public.sch"
USING "vehicle_gen_private.sch"
USING "cutscene_control_public.sch"
USING "director_mode_public.sch"

#IF NOT USE_SP_DLC
USING "shrink_narrative_control.sch"
USING "shrink_stat_tracking.sch"
#ENDIF

USING "mission_repeat_public.sch"
USING "mission_titles_private.sch"
USING "rich_presence_public.sch"
USING "trigger_scene_private.sch"
USING "net_system_activity_feed.sch"

// PURPOSE:	Gets called by all DO_MISSION flow commands to handle common requirements as we start a mission.
// 
PROC MISSION_PROCESSING_MISSION_STARTING(INT paramCoreVarsArrayPos, INT paramAvailableMissionIndex, SP_MISSIONS paramMissionID, STRANDS paramStrandID, BOOL paramHidePhone = TRUE)
	
	// Tell the flow controller to only process this strand each frame until the mission is over.
	IF paramStrandID != STRAND_NONE
		g_flowUnsaved.eLockInStrand = paramStrandID
	ENDIF
	
	IF g_bPlayerLockedInToTrigger
		SET_MAX_WANTED_LEVEL(5)
		SET_WANTED_LEVEL_MULTIPLIER(1.0)
	ENDIF
	
	sEndScreen.iHeistTimer = 0
	// Return player control and disable invincibility.
	IF IS_PLAYER_PLAYING(PLAYER_ID())
		//Clear wanted level unless this is a seamless trigger mission or a prep mission.
		IF NOT IS_BIT_SET(g_sMissionStaticData[paramMissionID].settingsBitset, MF_INDEX_SEAMLESS_TRIGGER)
		AND NOT IS_BIT_SET(g_sMissionStaticData[paramMissionID].settingsBitset, MF_INDEX_IS_PREP)
			CPRINTLN(DEBUG_FLOW, "Clearing player wanted levels as mission starts.")
			SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0) 
            SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
			START_FIRING_AMNESTY()
			IF IS_PED_USING_ACTION_MODE(PLAYER_PED_ID())
				SET_PED_USING_ACTION_MODE(player_ped_id(),false)
			ENDIF
			
			// Ensure special ability effects are cleared.
			SPECIAL_ABILITY_DEACTIVATE(PLAYER_ID())
		ENDIF
		
		//Turn off player invincibility now if the mission doesn't open with a cutscene.
	  	//If it does it will be turned off after the cutscene.
	  	IF g_eMissionIDToLoadCutscene = paramMissionID
			SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), TRUE)
			CPRINTLN(DEBUG_FLOW, "MISSION_PROCESSING_MISSION_STARTING player still invincible, needs to be turned off after intro.")
		ELSE
			SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), FALSE)
			CPRINTLN(DEBUG_FLOW, "MISSION_PROCESSING_MISSION_STARTING removed invinciblity: no cut-scene")
		ENDIF
	ENDIF
	
	//Load the death response script into memory to ensure we can launch it in one frame if the player dies.
	//We'll check it's loaded when we try and launch it.
	REQUEST_SCRIPT("buddyDeathResponse")
	
	//Stop player fly through windscreens and taking damage in crashes.
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_WillFlyThroughWindscreen, FALSE)
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_WillTakeDamageWhenVehicleCrashes, FALSE)
		CDEBUG1LN(DEBUG_FLOW, "DO_MISSION_*(", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(paramMissionID), "): Setting WillFlyThroughWindscreen to FALSE for PED_INDEX[",NATIVE_TO_INT(PLAYER_PED_ID()),"].")
		CDEBUG1LN(DEBUG_FLOW, "DO_MISSION_*(", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(paramMissionID), "): Setting WillTakeDamageWhenVehicleCrashes to FALSE for PED_INDEX[",NATIVE_TO_INT(PLAYER_PED_ID()),"].")
	
		IF NOT IS_BIT_SET(g_sMissionStaticData[paramMissionID].settingsBitset, MF_INDEX_SEAMLESS_TRIGGER)
		AND NOT IS_BIT_SET(g_sMissionStaticData[paramMissionID].settingsBitset, MF_INDEX_IS_PREP)
			// Sober the player up
			Force_All_Drunk_Peds_To_Become_Sober_And_Quit_Camera()
		ENDIF
	ENDIF
	
	UPDATE_TREVOR_BIKE_GANG_STATUS(GET_CURRENT_PLAYER_PED_ENUM())
				
	// Store pre-mission clothes so we can revert to them later if required
	IF NOT IS_REPLAY_IN_PROGRESS()
		STORE_PLAYER_PED_VARIATIONS(PLAYER_PED_ID(), TRUE)
	ENDIF
	
	// Re-apply blocking areas
	UPDATE_ALL_BUILDING_STATE_BLOCKS()
	
	// Store players current vehicle state for ambient switch
	enumCharacterList eCurrentPlayerPedEnum = GET_CURRENT_PLAYER_PED_ENUM()
	IF IS_PLAYER_PED_PLAYABLE(eCurrentPlayerPedEnum)
		STORE_VEH_DATA_FROM_PED(PLAYER_PED_ID(),
				g_sPlayerLastVeh[eCurrentPlayerPedEnum],
				g_vPlayerLastVehCoord[eCurrentPlayerPedEnum],
				g_fPlayerLastVehHead[eCurrentPlayerPedEnum],
				g_ePlayerLastVehState[eCurrentPlayerPedEnum]	#IF USE_TU_CHANGES	, g_ePlayerLastVehGen[eCurrentPlayerPedEnum]	#ENDIF	)
	ENDIF
	
	// Ensure the cellphone is taken out of SLEEP mode.
	SET_CELLPHONE_PROFILE_TO_NORMAL()

	// Clear any unseen new contact signifiers.
	BufferedContactAvailable[CHAR_MICHAEL] = FALSE
	BufferedContactAvailable[CHAR_TREVOR] = FALSE
	BufferedContactAvailable[CHAR_FRANKLIN] = FALSE

	// End any ongoing conversations.
	IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		KILL_ANY_CONVERSATION()
	ENDIF
	
	#if USE_CLF_DLC
		g_savedGlobalsClifford.sPlayerData.eOverridePed = GET_MISSION_SAVE_OVERRIDE_CHARACTER(paramMissionID)
	#endif
	#if USE_NRM_DLC
		g_savedGlobalsnorman.sPlayerData.eOverridePed = GET_MISSION_SAVE_OVERRIDE_CHARACTER(paramMissionID)
	#endif
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
		g_savedGlobals.sPlayerData.eOverridePed = GET_MISSION_SAVE_OVERRIDE_CHARACTER(paramMissionID)
	#endif
	#endif
	
	CLEAR_ADDITIONAL_TEXT(MISSION_TEXT_SLOT, TRUE)
	
	// Hang up phone unless requested not to or this is a seamless prep mission.
	IF paramHidePhone 
	AND NOT IS_BIT_SET(g_sMissionStaticData[paramMissionID].settingsBitset, MF_INDEX_IS_PREP)
		HANG_UP_AND_PUT_AWAY_PHONE()
	ENDIF
	
	// Remove any scene auto trigger requests.
	g_bSceneAutoTrigger = FALSE

	// Make all active minigame blips short range.
	SET_ALL_MINIGAME_BLIPS_SHORT_RANGE()
	
	// Reset the stored post-mission switches when starting a new mission.
	RESET_POST_MISSION_SWITCHES()
	
	// Turn off all runway minimap blips. - REMOVED DUE TO OLD CODE CAUSING BUGS - SEE BUG 1837097
	//SET_MINIMAP_COMPONENT(MINIMAP_COMPONENT_RUNWAY_1, FALSE)
	//SET_MINIMAP_COMPONENT(MINIMAP_COMPONENT_RUNWAY_2, FALSE)
	//SET_MINIMAP_COMPONENT(MINIMAP_COMPONENT_RUNWAY_3, FALSE)
	//SET_MINIMAP_COMPONENT(MINIMAP_COMPONENT_RUNWAY_4, FALSE)

	
	IF NOT IS_REPEAT_PLAY_ACTIVE()
		// Hand over any pre-streamed cutscenes to the new mission thread. (not done on repeat play)
		IF g_eMissionIDToLoadCutscene = paramMissionID
			IF NOT ARE_STRINGS_EQUAL("NONE", g_txtIntroMocapToLoad)
				CPRINTLN(DEBUG_FLOW, "Cutscene load request for ", g_txtIntroMocapToLoad, " handed over to mission ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(paramMissionID), ".")
#IF USE_CLF_DLC
				SET_SCRIPT_CAN_START_CUTSCENE(g_availableMissionsTU[paramAvailableMissionIndex].theThread)
#ENDIF
#IF USE_NRM_DLC
				SET_SCRIPT_CAN_START_CUTSCENE(g_availableMissionsTU[paramAvailableMissionIndex].theThread)
#ENDIF
#IF NOT USE_SP_DLC
				SET_SCRIPT_CAN_START_CUTSCENE(g_availableMissions[paramAvailableMissionIndex].theThread)
#ENDIF
			ENDIF
			
			//Reset flow cutscene pre-loading procedures.
			CPRINTLN(DEBUG_FLOW, "Flow releasing cutscene load request.")
			g_eMissionIDToLoadCutscene			= SP_MISSION_NONE
			g_bFlowIntroVariationRequestsMade 	= FALSE
			g_bFlowLoadRequestStarted 			= FALSE
			g_iCharIntroSectionsToLoad[0]		= CS_NONE
			g_iCharIntroSectionsToLoad[1]		= CS_NONE
			g_iCharIntroSectionsToLoad[2]		= CS_NONE
			
			//Clean out all variation requests.
			INT index
			REPEAT g_iFlowCurrentCutsceneVariationCount index
				g_sFlowCutsceneVariations[index].iHandleIndex = -1
				g_sFlowCutsceneVariations[index].eComponent = PED_COMP_HEAD
				g_sFlowCutsceneVariations[index].iDrawable = 0
				g_sFlowCutsceneVariations[index].iTexture = 0
			ENDREPEAT
			g_iFlowCurrentCutsceneVariationCount = 0
			
			REPEAT g_iFlowCurrentCutsceneOutfitCount index
				g_sFlowCutsceneOutfit[index].iHandleIndex = -1
				g_sFlowCutsceneOutfit[index].eModel = DUMMY_MODEL_FOR_SCRIPT
				g_sFlowCutsceneOutfit[index].eOutfit = DUMMY_PED_COMP
			ENDREPEAT
			g_iFlowCurrentCutsceneOutfitCount = 0
			
			REPEAT g_iFlowCurrentPedVariationCount index
				g_sFlowPedVariation[index].iHandleIndex = -1
				g_sFlowPedVariation[index].ped = NULL
			ENDREPEAT
			g_iFlowCurrentPedVariationCount = 0
			
			REPEAT g_iFlowCurrentCutscenPropCount index
				g_sFlowCutsceneProp[index].iHandleIndex = -1
				g_sFlowCutsceneProp[index].ePosition = ANCHOR_HEAD
				g_sFlowCutsceneProp[index].iPropIndex = 0
				g_sFlowCutsceneProp[index].iTextureIndex = 0
			ENDREPEAT
			g_iFlowCurrentCutscenPropCount = 0
			
			PRIVATE_Cleanup_Cutscene_Handles()
			
			REPEAT 3 index
				g_iFlowBitsetBlockPlayerCutsceneVariations[index] = 0
			ENDREPEAT
			
			g_bFlowLoadIntroCutscene = FALSE
			g_bFlowLoadingIntroCutscene = FALSE
			
			IF g_iFlowIntroCutsceneRequestID != NULL_OFFMISSION_CUTSCENE_REQUEST
				END_OFFMISSION_CUTSCENE_REQUEST(g_iFlowIntroCutsceneRequestID)
			ENDIF
		ELSE
			CPRINTLN(DEBUG_FLOW, "Another mission launched. Releasing cutscene load request.")
			// Remove cutscene this frame. This will be done by the cutscene management routines, 
			// but this maybe a frame too late. There is no harm in removing a twice.
			REMOVE_CUTSCENE() 
			g_bFlowCleanupIntroCutscene = TRUE
		ENDIF
	ENDIF
	
	// Set console rich presence values as we go on mission (friend dashboard status message).
	SET_RICH_PRESENCE_FOR_SP_STORY_MISSION(paramMissionID)
	
	// Tell the playstats system this mission is running.
	INT iVariation = GET_MISSION_VARIATION(INT_TO_ENUM(SP_MISSIONS, paramMissionID))
	TEXT_LABEL_7 txtStatID = g_sMissionStaticData[paramMissionID].statID
	
	#IF NOT USE_SP_DLC
		IF paramMissionID = SP_HEIST_JEWELRY_2
			SWITCH g_savedGlobals.sFlow.controls.intIDs[FLOWINT_HEIST_CHOICE_JEWEL]
				CASE HEIST_CHOICE_JEWEL_STEALTH			txtStatID += "A"	BREAK
				CASE HEIST_CHOICE_JEWEL_HIGH_IMPACT		txtStatID += "B"	BREAK
			ENDSWITCH
		ENDIF
	#ENDIF
	
	SCRIPT_PLAYSTATS_MISSION_STARTED(txtStatID, iVariation)
	CPRINTLN(DEBUG_FLOW, "Flagging playstat MISSION STARTED for story mission. MissionID:", txtStatID, " Variation:", iVariation)
	
	//Pass code the name of the mission we're on.
	TEXT_LABEL_7 tMissionName = GET_SP_MISSION_NAME_LABEL(paramMissionID)
	SET_MISSION_NAME(TRUE, tMissionName)
	
	//Flag the flow controller to display the title of this mission.
	IF NOT IS_BIT_SET(g_sMissionStaticData[paramMissionID].settingsBitset, MF_INDEX_NO_TITLE)
		IF NOT IS_REPLAY_IN_PROGRESS()
			MISSION_FLOW_DISPLAY_MISSION_TITLE(paramMissionID)
		ENDIF
	ENDIF
	
	// Store replay start snapshot for this mission, if it is not being replayed
	
#IF USE_CLF_DLC
	IF NOT IS_BIT_SET(g_sMissionStaticData[paramMissionID].settingsBitset, MF_INDEX_NO_FAIL)
	AND NOT (IS_BIT_SET(g_availableMissionsTU[paramAvailableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_BEING_REPLAYED))
		Store_Mission_Replay_Starting_Snapshot(paramCoreVarsArrayPos)
	ENDIF
#ENDIF

#IF USE_NRM_DLC
	IF NOT IS_BIT_SET(g_sMissionStaticData[paramMissionID].settingsBitset, MF_INDEX_NO_FAIL)
	AND NOT (IS_BIT_SET(g_availableMissionsTU[paramAvailableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_BEING_REPLAYED))
		Store_Mission_Replay_Starting_Snapshot(paramCoreVarsArrayPos)
	ENDIF
#ENDIF

#IF NOT USE_SP_DLC
	IF NOT IS_BIT_SET(g_sMissionStaticData[paramMissionID].settingsBitset, MF_INDEX_NO_FAIL)
	AND NOT (IS_BIT_SET(g_availableMissions[paramAvailableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_BEING_REPLAYED))
		Store_Mission_Replay_Starting_Snapshot(paramCoreVarsArrayPos)
	ENDIF
#ENDIF

	FLOW_PRIME_STATS_FOR_MISSION(paramMissionID)
	
	#IF NOT USE_SP_DLC 	
		PRIVATE_Prime_Heist_Crew_Stat_Tracking_On_Mission_Start(paramMissionID)
	#ENDIF
	
	STOP_GAMEPLAY_HINT()
	
	g_eRunningMission = paramMissionID
	
	CPRINTLN(DEBUG_FLOW, "DO_MISSION_*(", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(paramMissionID), "): Ran mission starting procedures.")
ENDPROC


// PURPOSE:	Gets called by all mission ending routines to handle common requirements as we come off mission.
#if USE_CLF_DLC 
PROC MISSION_PROCESSING_MISSION_OVERCLF(SP_MISSIONS paramMissionID)

	//Inform code that we are no longer on a mission.
	SET_MISSION_NAME(FALSE)
	
	//Failsafe: Clean up assets for this mission if they are still loaded. 
	IF g_eMissionSceneToCleanup = paramMissionID
		//Finale 2 Intro passes immediately but wants it's trigger scene to persist
		//for a heist controller to pick up assets from.
		g_bCleanupTriggerScene = TRUE
	ENDIF
	
	//remove trigger help text
	REMOVE_HELP_FROM_FLOW_QUEUE("TRIG_FT")
	REMOVE_HELP_FROM_FLOW_QUEUE("TRIG_F")
	REMOVE_HELP_FROM_FLOW_QUEUE("TRIG_T")
	REMOVE_HELP_FROM_FLOW_QUEUE("TRIG_MT")
	REMOVE_HELP_FROM_FLOW_QUEUE("TRIG_M")
	REMOVE_HELP_FROM_FLOW_QUEUE("TRIG_T")
	
	//Make sure no mission is flagged as just having triggered as we come off mission.
	Clear_Triggered_Flags_For_All_Trigger_Scenes()
	
	//Set leave area flags for all blipped missions on the map.
	Set_Leave_Area_Flag_For_All_Blipped_Missions()
	
	//Block all REs from launching for 30 seconds after a mission passes.
	g_iLastRandomEventLaunch = GET_GAME_TIMER() - 120000 // GET_GAME_TIMER() - (iMinTimeBetweenLaunches - 30000)
	
	UPDATE_TREVOR_BIKE_GANG_STATUS(GET_CURRENT_PLAYER_PED_ENUM())
	
	//Clear any primed heist stats.
	IF g_bHeistTempCrewStatsPrimed
		CPRINTLN(DEBUG_HEIST, "<CREWSTAT> Clearing heist crew member stat prime as we run mission over runs.")
		g_bHeistTempCrewStatsPrimed = FALSE
	ENDIF
	
	//Clean up on screen text.
	CLEAR_PRINTS()
	CLEAR_HELP(TRUE)
	CLEAR_ALL_FLOATING_HELP(TRUE)
	CLEAR_ADDITIONAL_TEXT(MISSION_TEXT_SLOT, TRUE)
	CLEAR_BRIEF()	//	For C class bug 1793220 - Graeme
	MISSION_FLOW_CLEAR_DISPLAY_MISSION_TITLE()
	SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
	
	//Block all off-mission communications for 20 seconds.
	ADD_GLOBAL_COMMUNICATION_DELAY(CC_GLOBAL_DELAY_POST_MISSION)
	
	//Restore phone's sleep mode if it was set before the mission.
	IF IS_BIT_SET(g_savedGlobalsClifford.sFlowCustom.spInitBitset, SP_INIT_RESTORE_SLEEP_MODE)
		SET_CELLPHONE_PROFILE_TO_SLEEP()
	ENDIF	
	//Clear the flag that tells missions they have been laucnched directly after loading a save.
	CLEAR_BIT(g_savedGlobalsClifford.sFlowCustom.spInitBitset, SP_INIT_LOADED_DIRECTLY_INTO_MISSION)	
	//Ensure no random events trigger for a while.
	//Update last ambient launch timer.
	g_iLastSuccessfulAmbientLaunchTime = GET_GAME_TIMER()	
	g_savedGlobalsClifford.sPlayerData.eOverridePed = NO_CHARACTER

	//Reenable player flying through windscreens.
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_WillFlyThroughWindscreen, TRUE)
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_WillTakeDamageWhenVehicleCrashes, TRUE)
		CDEBUG1LN(DEBUG_FLOW, "DO_MISSION_*(", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(paramMissionID), "): Setting WillFlyThroughWindscreen to TRUE for PED_INDEX[",NATIVE_TO_INT(PLAYER_PED_ID()),"].")
		CDEBUG1LN(DEBUG_FLOW, "DO_MISSION_*(", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(paramMissionID), "): Setting WillTakeDamageWhenVehicleCrashes to TRUE for PED_INDEX[",NATIVE_TO_INT(PLAYER_PED_ID()),"].")
	ENDIF
	
	// #2171916 Ensure player air drag is cleaned up.
	IF IS_PLAYER_PLAYING(PLAYER_ID())
		SET_AIR_DRAG_MULTIPLIER_FOR_PLAYERS_VEHICLE(PLAYER_ID(), 1.0)								
	ENDIF
	
	//Add a character delay to any character involved in the mission.
	INT iInvolvedCharBitset = g_sMissionStaticData[paramMissionID].friendCharBitset
	IF IS_BITMASK_SET(iInvolvedCharBitset, BIT_MICHAEL)
		ADD_COMMUNICATION_DELAY_FOR_CHARACTER(CHAR_MICHAEL)
	ENDIF
	IF IS_BITMASK_SET(iInvolvedCharBitset, BIT_FRANKLIN)
		ADD_COMMUNICATION_DELAY_FOR_CHARACTER(CHAR_FRANKLIN)
	ENDIF
	IF IS_BITMASK_SET(iInvolvedCharBitset, BIT_TREVOR)
		ADD_COMMUNICATION_DELAY_FOR_CHARACTER(CHAR_TREVOR)
	ENDIF
	//We're done with this until the next mission launches.
	SET_SCRIPT_AS_NO_LONGER_NEEDED("buddyDeathResponse")
	
	//reset timer for heist big score prep c
	g_FHPC_tod_NextRemindTime  = INVALID_TIMEOFDAY
	
	// Unblock shops
	SET_ALL_SHOPS_TEMPORARILY_UNAVAILABLE(FALSE)
	
	// Reset switch cam flags
	SET_SELECTOR_CAM_ACTIVE(FALSE)
	DISABLE_SELECTOR_QUICK_SWITCH_TO_DAMAGED_PED(FALSE)
	
	// Reset cutscene clothes flags
	SET_PLAYER_PED_DATA_IN_CUTSCENES(TRUE, TRUE)
	
	// Re-apply blocking areas
	UPDATE_ALL_BUILDING_STATE_BLOCKS()
	
	// Update last stored variations	
	g_sLastStoredPlayerPedClothes[CHAR_MICHAEL] = g_savedGlobalsClifford.sPlayerData.sInfo.sVariations[CHAR_MICHAEL]
	g_sLastStoredPlayerPedClothes[CHAR_FRANKLIN] = g_savedGlobalsClifford.sPlayerData.sInfo.sVariations[CHAR_FRANKLIN]
	g_sLastStoredPlayerPedClothes[CHAR_TREVOR] = g_savedGlobalsClifford.sPlayerData.sInfo.sVariations[CHAR_TREVOR]
		
	// Safeguard to ensure mission music doesn't persist after a mission has ended.
	TRIGGER_MUSIC_EVENT("GLOBAL_KILL_MUSIC")
	
	// Clear the Rich Presence value as we come off mission.
	SET_DEFAULT_RICH_PRESENCE_FOR_SP()

	//Clear the blip system leash after every mission.
	g_bBlipSystemStartupLeash = FALSE
	
	#IF IS_DEBUG_BUILD
		//Clear the running mission autoplay label.
		g_txtFlowAutoplayRunningMission = "NO MISSION"
		Stop_Metrics_For_Mission_Over(GET_SCRIPT_FROM_HASH(g_sMissionStaticData[paramMissionID].scripthash))		
	#ENDIF
	
	g_flowUnsaved.eLockInStrand = STRAND_NONE
	
	g_eRunningMission = SP_MISSION_NONE
	
	CPRINTLN(DEBUG_FLOW, "DO_MISSION_*(", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(paramMissionID), "): Ran mission over procedures.")
ENDPROC
#endif

#if USE_NRM_DLC
PROC MISSION_PROCESSING_MISSION_OVERNRM(SP_MISSIONS paramMissionID)

	//Inform code that we are no longer on a mission.
	SET_MISSION_NAME(FALSE)
	
	//Failsafe: Clean up assets for this mission if they are still loaded. 
	IF g_eMissionSceneToCleanup = paramMissionID		
		g_bCleanupTriggerScene = TRUE
	ENDIF
	
	//remove trigger help text
	REMOVE_HELP_FROM_FLOW_QUEUE("TRIG_FT")
	REMOVE_HELP_FROM_FLOW_QUEUE("TRIG_F")
	REMOVE_HELP_FROM_FLOW_QUEUE("TRIG_T")
	REMOVE_HELP_FROM_FLOW_QUEUE("TRIG_MT")
	REMOVE_HELP_FROM_FLOW_QUEUE("TRIG_M")
	REMOVE_HELP_FROM_FLOW_QUEUE("TRIG_T")
	
	//Make sure no mission is flagged as just having triggered as we come off mission.
	Clear_Triggered_Flags_For_All_Trigger_Scenes()
	
	//Set leave area flags for all blipped missions on the map.
	Set_Leave_Area_Flag_For_All_Blipped_Missions()
	
	//Block all REs from launching for 30 seconds after a mission passes.
	g_iLastRandomEventLaunch = GET_GAME_TIMER() - 120000 // GET_GAME_TIMER() - (iMinTimeBetweenLaunches - 30000)
		
	//Clear any primed heist stats.
	IF g_bHeistTempCrewStatsPrimed
		CPRINTLN(DEBUG_HEIST, "<CREWSTAT> Clearing heist crew member stat prime as we run mission over runs.")
		g_bHeistTempCrewStatsPrimed = FALSE
	ENDIF
	
	//Clean up on screen text.
	CLEAR_PRINTS()
	CLEAR_HELP(TRUE)
	CLEAR_ALL_FLOATING_HELP(TRUE)
	CLEAR_ADDITIONAL_TEXT(MISSION_TEXT_SLOT, TRUE)
	CLEAR_BRIEF()	//	For C class bug 1793220 - Graeme
	MISSION_FLOW_CLEAR_DISPLAY_MISSION_TITLE()
	SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
	
	//Block all off-mission communications for 20 seconds.
	ADD_GLOBAL_COMMUNICATION_DELAY(CC_GLOBAL_DELAY_POST_MISSION)

	//Restore phone's sleep mode if it was set before the mission.
	IF IS_BIT_SET(g_savedGlobalsnorman.sFlowCustom.spInitBitset, SP_INIT_RESTORE_SLEEP_MODE)
		SET_CELLPHONE_PROFILE_TO_SLEEP()
	ENDIF	
	//Clear the flag that tells missions they have been laucnched directly after loading a save.
	CLEAR_BIT(g_savedGlobalsnorman.sFlowCustom.spInitBitset, SP_INIT_LOADED_DIRECTLY_INTO_MISSION)	
	//Ensure no random events trigger for a while.
	//Update last ambient launch timer.
	g_iLastSuccessfulAmbientLaunchTime = GET_GAME_TIMER()	
	g_savedGlobalsnorman.sPlayerData.eOverridePed = NO_CHARACTER

	//Reenable player flying through windscreens.
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_WillFlyThroughWindscreen, TRUE)
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_WillTakeDamageWhenVehicleCrashes, TRUE)
		CDEBUG1LN(DEBUG_FLOW, "DO_MISSION_*(", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(paramMissionID), "): Setting WillFlyThroughWindscreen to TRUE for PED_INDEX[",NATIVE_TO_INT(PLAYER_PED_ID()),"].")
		CDEBUG1LN(DEBUG_FLOW, "DO_MISSION_*(", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(paramMissionID), "): Setting WillTakeDamageWhenVehicleCrashes to TRUE for PED_INDEX[",NATIVE_TO_INT(PLAYER_PED_ID()),"].")
	ENDIF
	
	// #2171916 Ensure player air drag is cleaned up.
	IF IS_PLAYER_PLAYING(PLAYER_ID())
		SET_AIR_DRAG_MULTIPLIER_FOR_PLAYERS_VEHICLE(PLAYER_ID(), 1.0)								
	ENDIF
	
	//Add a character delay to any character involved in the mission.
	INT iInvolvedCharBitset = g_sMissionStaticData[paramMissionID].friendCharBitset
	IF IS_BITMASK_SET(iInvolvedCharBitset, BIT_MICHAEL)
		ADD_COMMUNICATION_DELAY_FOR_CHARACTER(CHAR_MICHAEL)		
	ENDIF
	IF IS_BITMASK_SET(iInvolvedCharBitset, BIT_FRANKLIN)
		ADD_COMMUNICATION_DELAY_FOR_CHARACTER(CHAR_FRANKLIN)		
	ENDIF
	IF IS_BITMASK_SET(iInvolvedCharBitset, BIT_TREVOR)
		ADD_COMMUNICATION_DELAY_FOR_CHARACTER(CHAR_TREVOR)		
	ENDIF
	
	//We're done with this until the next mission launches.
	SET_SCRIPT_AS_NO_LONGER_NEEDED("buddyDeathResponse")
	
	//reset timer for heist big score prep c
	g_FHPC_tod_NextRemindTime  = INVALID_TIMEOFDAY
	
	// Unblock shops
	SET_ALL_SHOPS_TEMPORARILY_UNAVAILABLE(FALSE)
	
	// Reset switch cam flags
	SET_SELECTOR_CAM_ACTIVE(FALSE)
	DISABLE_SELECTOR_QUICK_SWITCH_TO_DAMAGED_PED(FALSE)
	
	// Reset cutscene clothes flags
	SET_PLAYER_PED_DATA_IN_CUTSCENES(TRUE, TRUE)
	
	// Re-apply blocking areas
	UPDATE_ALL_BUILDING_STATE_BLOCKS()
	
	// Update last stored variations
	g_sLastStoredPlayerPedClothes[CHAR_MICHAEL] = g_savedGlobalsnorman.sPlayerData.sInfo.sVariations[CHAR_MICHAEL]
	g_sLastStoredPlayerPedClothes[CHAR_FRANKLIN] = g_savedGlobalsnorman.sPlayerData.sInfo.sVariations[CHAR_FRANKLIN]
	g_sLastStoredPlayerPedClothes[CHAR_TREVOR] = g_savedGlobalsnorman.sPlayerData.sInfo.sVariations[CHAR_TREVOR]
		
	// Safeguard to ensure mission music doesn't persist after a mission has ended.
	TRIGGER_MUSIC_EVENT("GLOBAL_KILL_MUSIC")
	
	// Clear the Rich Presence value as we come off mission.
	SET_DEFAULT_RICH_PRESENCE_FOR_SP()

	//Clear the blip system leash after every mission.
	g_bBlipSystemStartupLeash = FALSE
	
	#IF IS_DEBUG_BUILD
		//Clear the running mission autoplay label.
		g_txtFlowAutoplayRunningMission = "NO MISSION"
		Stop_Metrics_For_Mission_Over(GET_SCRIPT_FROM_HASH_NRM(g_sMissionStaticData[paramMissionID].scripthash))		
	#ENDIF
	
	g_flowUnsaved.eLockInStrand = STRAND_NONE	
	g_eRunningMission = SP_MISSION_NONE
	
	CPRINTLN(DEBUG_FLOW, "DO_MISSION_*(", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(paramMissionID), "): Ran mission over procedures.")
ENDPROC
#endif

PROC MISSION_PROCESSING_MISSION_OVER(SP_MISSIONS paramMissionID)

#if USE_CLF_DLC
	MISSION_PROCESSING_MISSION_OVERCLF(paramMissionID)
#endif

#if USE_NRM_DLC
	MISSION_PROCESSING_MISSION_OVERNRM(paramMissionID)
#endif

#if NOT USE_SP_DLC
	//Inform code that we are no longer on a mission.
	SET_MISSION_NAME(FALSE)
	
	//Failsafe: Clean up assets for this mission if they are still loaded. 
	IF g_eMissionSceneToCleanup = paramMissionID
		//Finale 2 Intro passes immediately but wants it's trigger scene to persist
		//for a heist controller to pick up assets from.
		IF paramMissionID != SP_HEIST_FINALE_2_INTRO
			g_bCleanupTriggerScene = TRUE
		ENDIF
	ENDIF
	
	//remove trigger help text
	REMOVE_HELP_FROM_FLOW_QUEUE("TRIG_FT")
	REMOVE_HELP_FROM_FLOW_QUEUE("TRIG_F")
	REMOVE_HELP_FROM_FLOW_QUEUE("TRIG_T")
	REMOVE_HELP_FROM_FLOW_QUEUE("TRIG_MT")
	REMOVE_HELP_FROM_FLOW_QUEUE("TRIG_M")
	REMOVE_HELP_FROM_FLOW_QUEUE("TRIG_T")
	
	//Make sure no mission is flagged as just having triggered as we come off mission.
	Clear_Triggered_Flags_For_All_Trigger_Scenes()
	
	//Set leave area flags for all blipped missions on the map.
	Set_Leave_Area_Flag_For_All_Blipped_Missions()
	
	//Block all REs from launching for 30 seconds after a mission passes.
	g_iLastRandomEventLaunch = GET_GAME_TIMER() - 120000 // GET_GAME_TIMER() - (iMinTimeBetweenLaunches - 30000)

	CLEAR_ALL_FRIEND_BLOCKS_FOR_MISSION(paramMissionID)
	UPDATE_TREVOR_BIKE_GANG_STATUS(GET_CURRENT_PLAYER_PED_ENUM())
	
	//Clear any primed heist stats.
	IF g_bHeistTempCrewStatsPrimed
		CPRINTLN(DEBUG_HEIST, "<CREWSTAT> Clearing heist crew member stat prime as we run mission over runs.")
		g_bHeistTempCrewStatsPrimed = FALSE
	ENDIF
	
	//Clean up on screen text.
	CLEAR_PRINTS()
	CLEAR_HELP(TRUE)
	CLEAR_ALL_FLOATING_HELP(TRUE)
	CLEAR_ADDITIONAL_TEXT(MISSION_TEXT_SLOT, TRUE)
	CLEAR_BRIEF()	//	For C class bug 1793220 - Graeme
	MISSION_FLOW_CLEAR_DISPLAY_MISSION_TITLE()
	SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
	
	//Block all off-mission communications for 20 seconds.
	ADD_GLOBAL_COMMUNICATION_DELAY(CC_GLOBAL_DELAY_POST_MISSION)

	//Restore phone's sleep mode if it was set before the mission.
	IF IS_BIT_SET(g_savedGlobals.sFlowCustom.spInitBitset, SP_INIT_RESTORE_SLEEP_MODE)
		SET_CELLPHONE_PROFILE_TO_SLEEP()
	ENDIF	
	//Clear the flag that tells missions they have been laucnched directly after loading a save.
	CLEAR_BIT(g_savedGlobals.sFlowCustom.spInitBitset, SP_INIT_LOADED_DIRECTLY_INTO_MISSION)	
	//Ensure no random events trigger for a while.
	//Update last ambient launch timer.
	g_iLastSuccessfulAmbientLaunchTime = GET_GAME_TIMER()	
	g_savedGlobals.sPlayerData.eOverridePed = NO_CHARACTER

	//Reenable player flying through windscreens.
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_WillFlyThroughWindscreen, TRUE)
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_WillTakeDamageWhenVehicleCrashes, TRUE)
		CDEBUG1LN(DEBUG_FLOW, "DO_MISSION_*(", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(paramMissionID), "): Setting WillFlyThroughWindscreen to TRUE for PED_INDEX[",NATIVE_TO_INT(PLAYER_PED_ID()),"].")
		CDEBUG1LN(DEBUG_FLOW, "DO_MISSION_*(", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(paramMissionID), "): Setting WillTakeDamageWhenVehicleCrashes to TRUE for PED_INDEX[",NATIVE_TO_INT(PLAYER_PED_ID()),"].")
	ENDIF
	
	// #2171916 Ensure player air drag is cleaned up.
	IF IS_PLAYER_PLAYING(PLAYER_ID())
		SET_AIR_DRAG_MULTIPLIER_FOR_PLAYERS_VEHICLE(PLAYER_ID(), 1.0)								
	ENDIF
	
	//Add a character delay to any character involved in the mission.
	INT iInvolvedCharBitset = g_sMissionStaticData[paramMissionID].friendCharBitset
	IF IS_BITMASK_SET(iInvolvedCharBitset, BIT_MICHAEL)
		ADD_COMMUNICATION_DELAY_FOR_CHARACTER(CHAR_MICHAEL)		
		ADD_COMMUNICATION_DELAY_FOR_CHARACTER(CHAR_ALL_PLAYERS_CONF)
		ADD_COMMUNICATION_DELAY_FOR_CHARACTER(CHAR_MIKE_FRANK_CONF)
		ADD_COMMUNICATION_DELAY_FOR_CHARACTER(CHAR_MIKE_TREV_CONF)
		ADD_COMMUNICATION_DELAY_FOR_CHARACTER(CHAR_STEVE_MIKE_CONF)		
	ENDIF
	IF IS_BITMASK_SET(iInvolvedCharBitset, BIT_FRANKLIN)
		ADD_COMMUNICATION_DELAY_FOR_CHARACTER(CHAR_FRANKLIN)		
		ADD_COMMUNICATION_DELAY_FOR_CHARACTER(CHAR_ALL_PLAYERS_CONF)
		ADD_COMMUNICATION_DELAY_FOR_CHARACTER(CHAR_MIKE_FRANK_CONF)
		ADD_COMMUNICATION_DELAY_FOR_CHARACTER(CHAR_FRANK_TREV_CONF)
		
	ENDIF
	IF IS_BITMASK_SET(iInvolvedCharBitset, BIT_TREVOR)
		ADD_COMMUNICATION_DELAY_FOR_CHARACTER(CHAR_TREVOR)		
		ADD_COMMUNICATION_DELAY_FOR_CHARACTER(CHAR_ALL_PLAYERS_CONF)
		ADD_COMMUNICATION_DELAY_FOR_CHARACTER(CHAR_FRANK_TREV_CONF)
		ADD_COMMUNICATION_DELAY_FOR_CHARACTER(CHAR_MIKE_TREV_CONF)
		ADD_COMMUNICATION_DELAY_FOR_CHARACTER(CHAR_STEVE_TREV_CONF)		
	ENDIF
	
	IF IS_BITMASK_SET(iInvolvedCharBitset, BIT_LAMAR)
		ADD_COMMUNICATION_DELAY_FOR_CHARACTER(CHAR_LAMAR)
	ENDIF
	IF IS_BITMASK_SET(iInvolvedCharBitset, BIT_JIMMY)
		ADD_COMMUNICATION_DELAY_FOR_CHARACTER(CHAR_JIMMY)
	ENDIF
	IF IS_BITMASK_SET(iInvolvedCharBitset, BIT_AMANDA)
		ADD_COMMUNICATION_DELAY_FOR_CHARACTER(CHAR_AMANDA)
	ENDIF
	
	//We're done with this until the next mission launches.
	SET_SCRIPT_AS_NO_LONGER_NEEDED("buddyDeathResponse")
	
	//reset timer for heist big score prep c
	g_FHPC_tod_NextRemindTime  = INVALID_TIMEOFDAY
	
	// Unblock shops
	SET_ALL_SHOPS_TEMPORARILY_UNAVAILABLE(FALSE)
	
	// Reset switch cam flags
	SET_SELECTOR_CAM_ACTIVE(FALSE)
	DISABLE_SELECTOR_QUICK_SWITCH_TO_DAMAGED_PED(FALSE)
	
	// Reset cutscene clothes flags
	SET_PLAYER_PED_DATA_IN_CUTSCENES(TRUE, TRUE)
	
	// Re-apply blocking areas
	UPDATE_ALL_BUILDING_STATE_BLOCKS()
	
	// Update last stored variations	
	g_sLastStoredPlayerPedClothes[CHAR_MICHAEL] = g_savedGlobals.sPlayerData.sInfo.sVariations[CHAR_MICHAEL]
	g_sLastStoredPlayerPedClothes[CHAR_FRANKLIN] = g_savedGlobals.sPlayerData.sInfo.sVariations[CHAR_FRANKLIN]
	g_sLastStoredPlayerPedClothes[CHAR_TREVOR] = g_savedGlobals.sPlayerData.sInfo.sVariations[CHAR_TREVOR]
	
	
	// Safeguard to ensure mission music doesn't persist after a mission has ended.
	TRIGGER_MUSIC_EVENT("GLOBAL_KILL_MUSIC")
	
	// Clear the Rich Presence value as we come off mission.
	SET_DEFAULT_RICH_PRESENCE_FOR_SP()

	//Clear the blip system leash after every mission.
	g_bBlipSystemStartupLeash = FALSE
	
	#IF IS_DEBUG_BUILD
		//Clear the running mission autoplay label.
		g_txtFlowAutoplayRunningMission = "NO MISSION"		
		Stop_Metrics_For_Mission_Over(g_sMissionStaticData[paramMissionID].scriptName)		
	#ENDIF
	
	g_flowUnsaved.eLockInStrand = STRAND_NONE	
	g_eRunningMission = SP_MISSION_NONE
	
	CPRINTLN(DEBUG_FLOW, "DO_MISSION_*(", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(paramMissionID), "): Ran mission over procedures.")
#endif

ENDPROC

// PURPOSE:	Gets called by all DO_MISSION flow commands when they want to release the candidate ID for their mission.
// 
PROC MISSION_PROCESSING_RELEASE_MISSION(INT paramCoreVarsArrayPos, INT paramAvailableArrayPos, BOOL paramHadTrigger)
#IF USE_CLF_DLC
	IF paramHadTrigger
		IF IS_CURRENTLY_ON_MISSION_TO_TYPE()
			TRIGGERED_MISSION_HAS_TERMINATED(g_availableMissionsTU[paramAvailableArrayPos].missionTriggerID)
		ELSE
			Remove_Mission_Trigger(g_availableMissionsTU[paramAvailableArrayPos].missionTriggerID)
		ENDIF
	ELSE
		MISSION_OVER(g_availableMissionsTU[paramAvailableArrayPos].missionCandidateID)
	ENDIF
#ENDIF
#IF USE_NRM_DLC
	IF paramHadTrigger
		IF IS_CURRENTLY_ON_MISSION_TO_TYPE()
			TRIGGERED_MISSION_HAS_TERMINATED(g_availableMissionsTU[paramAvailableArrayPos].missionTriggerID)
		ELSE
			Remove_Mission_Trigger(g_availableMissionsTU[paramAvailableArrayPos].missionTriggerID)
		ENDIF
	ELSE
		MISSION_OVER(g_availableMissionsTU[paramAvailableArrayPos].missionCandidateID)
	ENDIF
#ENDIF
#IF NOT USE_CLF_DLC
#IF NOT USE_NRM_DLC
	IF paramHadTrigger
		IF IS_CURRENTLY_ON_MISSION_TO_TYPE()
			TRIGGERED_MISSION_HAS_TERMINATED(g_availableMissions[paramAvailableArrayPos].missionTriggerID)
		ELSE
			Remove_Mission_Trigger(g_availableMissions[paramAvailableArrayPos].missionTriggerID)
		ENDIF
	ELSE
		MISSION_OVER(g_availableMissions[paramAvailableArrayPos].missionCandidateID)
	ENDIF
#ENDIF
#ENDIF

	Reset_All_Replay_Variables()
	
	//Safeguard. Make sure scripts are unloaded from memory.
	SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(g_sMissionStaticData[g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue1].scriptHash)
	#if USE_CLF_DLC
		SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED( HASH("mission_stat_watchCLF") )
	#endif
	#if USE_NRM_DLC
		SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED( HASH("mission_stat_watchNRM") )
	#endif
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
		SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED( HASH("mission_stat_watcher") )
	#endif	
	#endif
	
	UNREGISTER_MISSION_PREVIOUSLY_AVAILABLE(paramCoreVarsArrayPos)
	
	ANIMPOSTFX_STOP_ALL()
	
	CPRINTLN(DEBUG_FLOW, "MISSION_PROCESSING_RELEASE_MISSION is unpausing the flow help queue.")
	CPRINTLN(DEBUG_FLOW, "DO_MISSION_*(", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(INT_TO_ENUM(SP_MISSIONS, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue1)), "): Released mission trigger and candidate ID.")
		
ENDPROC


// PURPOSE:	Gets called by all DO_MISSION flow commands when their mission has passed.
// 
FUNC JUMP_LABEL_IDS MISSION_PROCESSING_MISSION_PASSED(INT paramCoreVarsArrayPos, INT paramAvailableArrayPos, SP_MISSIONS paramMissionID, BOOL paramHadTrigger, BOOL paramBackgroundPass = FALSE)
	CPRINTLN(DEBUG_FLOW, "DO_MISSION_*(", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(paramMissionID), "): Running mission passed procedures.")
	
	#IF NOT USE_SP_DLC
		//Finale heist stat increases.
		PRIVATE_Finalise_Heist_Crew_Stat_Increments_On_Mission_Pass(paramMissionID)
	#ENDIF
	
	IF NOT paramBackgroundPass
		// Tell the playstats system this mission has passed.
		INT iVariation = GET_MISSION_VARIATION(paramMissionID)
		TEXT_LABEL_7 txtStatID = g_sMissionStaticData[paramMissionID].statID
		
		#IF NOT USE_SP_DLC
			IF paramMissionID = SP_HEIST_JEWELRY_2
				SWITCH g_savedGlobals.sFlow.controls.intIDs[FLOWINT_HEIST_CHOICE_JEWEL]
					CASE HEIST_CHOICE_JEWEL_STEALTH			txtStatID += "A"	BREAK
					CASE HEIST_CHOICE_JEWEL_HIGH_IMPACT		txtStatID += "B"	BREAK
				ENDSWITCH
			ENDIF
		#ENDIF
		
		PLAYSTATS_MISSION_CHECKPOINT(txtStatID, iVariation, g_replayMissionStage, 0)
		SCRIPT_PLAYSTATS_MISSION_OVER(txtStatID, iVariation, g_replayMissionStage)
		CPRINTLN(DEBUG_FLOW, "Flagging playstat MISSION PASSED for story mission. MissionID:", txtStatID, " Variation:", iVariation)

		//Set global "last mission passed" to this mission
		g_eLastMissionPassed = paramMissionID
		g_iLastMissionPassedGameTime = GET_GAME_TIMER()

		//Update the last completed mission stat with the name of this mission.
		#IF USE_CLF_DLC
			IF paramMissionID = SP_MISSION_CLF_FIN
				SET_LAST_COMPLETED_MISSION_STAT(g_sMissionStatsName, g_sMissionStaticData[paramMissionID].friendCharBitset)
			ELSE
				TEXT_LABEL_7 tMissionName = GET_SP_MISSION_NAME_LABEL(paramMissionID)
				SET_LAST_COMPLETED_MISSION_STAT(tMissionName, g_sMissionStaticData[paramMissionID].friendCharBitset)
			ENDIF
		#ENDIF
		
		#IF USE_NRM_DLC
			IF paramMissionID = SP_MISSION_NRM_SUR_CURE //temp
				SET_LAST_COMPLETED_MISSION_STAT(g_sMissionStatsName, g_sMissionStaticData[paramMissionID].friendCharBitset)
			ELSE
				TEXT_LABEL_7 tMissionName = GET_SP_MISSION_NAME_LABEL(paramMissionID)
				SET_LAST_COMPLETED_MISSION_STAT(tMissionName, g_sMissionStaticData[paramMissionID].friendCharBitset)
			ENDIF
		#ENDIF
		
		#IF NOT USE_SP_DLC
			IF paramMissionID = SP_MISSION_FINALE_CREDITS
				SET_LAST_COMPLETED_MISSION_STAT(g_sMissionStatsName, g_sMissionStaticData[paramMissionID].friendCharBitset)
			ELSE
				TEXT_LABEL_7 tMissionName = GET_SP_MISSION_NAME_LABEL(paramMissionID)
				SET_LAST_COMPLETED_MISSION_STAT(tMissionName, g_sMissionStaticData[paramMissionID].friendCharBitset)
			ENDIF
		#ENDIF

		// Call mission over routine
		MISSION_PROCESSING_MISSION_OVER(paramMissionID)
				
		// Make Director Mode characters linked to this mission available to the player.
		#IF FEATURE_SP_DLC_DIRECTOR_MODE
			UNLOCK_DIRECTOR_STORY_CHAR_FOR_STORY_MISSION(paramMissionID)
		#ENDIF
		
		//Trigger end of mission music and passed screen.
		IF NOT IS_BIT_SET(g_sMissionStaticData[paramMissionID].settingsBitset, MF_INDEX_NO_PASS)
			TRIGGER_MISSION_STATS_UI()
			g_sSelectorUI.bDisableForEndOfMission = TRUE
			g_sSelectorUI.iEndOfMissionTimer = GET_GAME_TIMER()
		ENDIF
		
		// Send a message to the PS4 activity feed.
		IF NOT IS_REPEAT_PLAY_ACTIVE()
			IF IS_PLAYSTATION_PLATFORM()
#IF NOT USE_SP_DLC
				//If we've just passed Prologue or Franklin2 post a special PS4 feed message.
				IF paramMissionID = SP_MISSION_PROLOGUE
					CPRINTLN(DEBUG_ACTIVITY_FEED, "Pushing Prologue complete activity feed message for PS4.")
					REQUEST_SYSTEM_ACTIVITY_TYPE_COMPLETED_PROLOGUE()
				ELIF paramMissionID = SP_MISSION_LESTER_1
					CPRINTLN(DEBUG_ACTIVITY_FEED, "Pushing Lester1 complete activity feed message for PS4.")
					REQUEST_SYSTEM_ACTIVITY_TYPE_COMPLETED_LESTER1()
				ELIF paramMissionID = SP_MISSION_FRANKLIN_2
					CPRINTLN(DEBUG_ACTIVITY_FEED, "Pushing Franklin2 complete activity feed message for PS4.")
					REQUEST_SYSTEM_ACTIVITY_TYPE_COMPLETED_FRANKLIN2()
				ELSE
#ENDIF
					CPRINTLN(DEBUG_ACTIVITY_FEED, "Pushing generic mission complete activity feed message for PS4.")
					REQUEST_SYSTEM_ACTIVITY_TYPE_COMPLETED_SP_MISSION(GET_CURRENT_PLAYER_PED_ENUM())
#IF NOT USE_SP_DLC				
				ENDIF
#ENDIF
			ENDIF
			
		ENDIF
		
	#IF IS_DEBUG_BUILD
	ELSE
		CPRINTLN(DEBUG_FLOW, "Mission was passed in the background. Skipping none background pass rountines.")
	#ENDIF
	ENDIF

	//Update passed failed stat.
	DO_MISSION_PASSED_OR_FAILED_STAT_UPDATE(TRUE, ENUM_TO_INT(paramMissionID))

	//Release triggers and candidate ID.
	MISSION_PROCESSING_RELEASE_MISSION(paramCoreVarsArrayPos, paramAvailableArrayPos, paramHadTrigger)

	//Flag the appropriate mission as complete in the mission data struct.
	SET_MISSION_COMPLETE_STATE(paramMissionID, TRUE)
	
	//Clean up player state.
	IF (IS_PLAYER_PLAYING(PLAYER_ID()))
        SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0) 
        SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
		SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME(PLAYER_ID())
        START_FIRING_AMNESTY()
    ENDIF
	
	//Reset shitskip skip-to-end variable
	g_bShitskipToEnd = FALSE
	
	#IF USE_CLF_DLC	
		//Reset the last known ped info on a pass, including overwriting his last known switch for certain missions
		ResetLastKnownPedInfo(g_savedGlobalsClifford.sPlayerData.sInfo, paramMissionID)
		UpdateLastKnownPedInfoPostMission(g_savedGlobalsClifford.sPlayerData.sInfo, paramMissionID)
	#ENDIF
	
	#IF USE_NRM_DLC
		//Reset the last known ped info on a pass, including overwriting his last known switch for certain missions
		ResetLastKnownPedInfo(g_savedGlobalsnorman.sPlayerData.sInfo, paramMissionID)
		UpdateLastKnownPedInfoPostMission(g_savedGlobalsnorman.sPlayerData.sInfo, paramMissionID)
	#ENDIF
	
	#IF NOT USE_SP_DLC
		//Reset the last known ped info on a pass, including overwriting his last known switch for certain missions
		ResetLastKnownPedInfo(g_savedGlobals.sPlayerData.sInfo, paramMissionID)
		UpdateLastKnownPedInfoPostMission(g_savedGlobals.sPlayerData.sInfo, paramMissionID)
	#ENDIF

	// Store players current vehicle state for ambient switch
	IF (IS_PLAYER_PLAYING(PLAYER_ID()))
	enumCharacterList eCurrentPlayerPedEnum = GET_CURRENT_PLAYER_PED_ENUM()
		IF IS_PLAYER_PED_PLAYABLE(eCurrentPlayerPedEnum)
			STORE_VEH_DATA_FROM_PED(PLAYER_PED_ID(),
					g_sPlayerLastVeh[eCurrentPlayerPedEnum],
					g_vPlayerLastVehCoord[eCurrentPlayerPedEnum],
					g_fPlayerLastVehHead[eCurrentPlayerPedEnum],
					g_ePlayerLastVehState[eCurrentPlayerPedEnum]	#IF USE_TU_CHANGES	, g_ePlayerLastVehGen[eCurrentPlayerPedEnum]	#ENDIF	)
		ENDIF
	ENDIF
	
	//Updates the family events when a mission has passed
	UpdateMissionPassedFamilyEvents(paramMissionID)
	
	// Remove headsets from player characters
	REMOVE_ALL_HEADSETS()
	
	// Unlock any radio news stories related to this mission.
	INT iNewsStoryUnlock = GET_SP_MISSION_NEWS_STORY_UNLOCK(paramMissionID)
	IF iNewsStoryUnlock != ENUM_TO_INT(NEWS_BLANK)
		if not IS_MISSION_NEWS_STORY_UNLOCKED(iNewsStoryUnlock)
			CPRINTLN(DEBUG_FLOW,"Unlocking news story #", iNewsStoryUnlock)
			UNLOCK_MISSION_NEWS_STORY(iNewsStoryUnlock)
		endif
	ENDIF
	
	//Trigger any autosaves.
	IF (NOT IS_BIT_SET(g_sMissionStaticData[paramMissionID].settingsBitset, MF_INDEX_NO_SAVE))
	AND (NOT g_bMissionStatSystemBlocker) //Mission passed screen is being blocked until later.
        MAKE_AUTOSAVE_REQUEST()
    ENDIF

	#IF NOT USE_SP_DLC
		IF NOT IS_REPEAT_PLAY_ACTIVE()
			//If we just passed the first or last heist finale trigger off
			//a facebook post.
			IF paramMissionID = SP_HEIST_JEWELRY_2
				MAKE_FACEBOOK_HEIST_COMPLETE_POST(HEIST_JEWEL)
			ELIF paramMissionID = SP_HEIST_FINALE_2A
			OR paramMissionID = SP_HEIST_FINALE_2B
				MAKE_FACEBOOK_HEIST_COMPLETE_POST(HEIST_FINALE)
			ENDIF
		ENDIF
	#ENDIF

	//Tell the flow to update all strands next frame.
	//Ensures missions which need seamless transitions trigger next frame.
	CPRINTLN(DEBUG_FLOW, "Mission ending. Flow flagged to process all strands next frame.")
	g_flowUnsaved.bFlowProcessAllStrandsNextFrame = TRUE
	
	//Fast track this strand so that we process any single frame commands
	//after this mission instantly.
	g_flowUnsaved.bFlowFastTrackCurrentStrand = TRUE
	
	Set_Leave_Area_Flag_For_Mission(paramMissionID, FALSE)
	
	RETURN INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue2) //Pass jump label.
ENDFUNC


// PURPOSE:	Gets called by all DO_MISSION flow commands when their mission has failed.
// 
FUNC JUMP_LABEL_IDS MISSION_PROCESSING_MISSION_FAILED(INT paramCoreVarsArrayPos, INT paramAvailableArrayPos, SP_MISSIONS paramMissionID, BOOL paramHadTrigger)

	// Do we want to setup a replay?
	IF NOT IS_BIT_SET(g_sMissionStaticData[paramMissionID].settingsBitset, MF_INDEX_NO_FAIL)
	AND g_bSkipFailScreen = FALSE
		
		// We dont get a chance to hit the _mission_over proc here so cleanup some states.
		
		// Unblock shops
		SET_ALL_SHOPS_TEMPORARILY_UNAVAILABLE(FALSE)
		
		// Reset switch cam flags
		SET_SELECTOR_CAM_ACTIVE(FALSE)
		DISABLE_SELECTOR_QUICK_SWITCH_TO_DAMAGED_PED(FALSE)
		
		// Reset cutscene clothes flags
		SET_PLAYER_PED_DATA_IN_CUTSCENES(TRUE, TRUE)
		
		// Re-apply blocking areas
		UPDATE_ALL_BUILDING_STATE_BLOCKS()
		
		#IF USE_CLF_DLC
			// Update last stored variations
			g_sLastStoredPlayerPedClothes[CHAR_MICHAEL] = g_savedGlobalsClifford.sPlayerData.sInfo.sVariations[CHAR_MICHAEL]
			g_sLastStoredPlayerPedClothes[CHAR_FRANKLIN] = g_savedGlobalsClifford.sPlayerData.sInfo.sVariations[CHAR_FRANKLIN]
			g_sLastStoredPlayerPedClothes[CHAR_TREVOR] = g_savedGlobalsClifford.sPlayerData.sInfo.sVariations[CHAR_TREVOR]
			//Yes, setup a replay and don't release triggers yet. We need to wait for a replay response.
			g_availableMissionsTU[paramAvailableArrayPos].bitflags = CLEAR_ALL_AVAILABLE_MISSIONS_BITFLAGS
		#ENDIF
		#IF USE_NRM_DLC
			// Update last stored variations
			g_sLastStoredPlayerPedClothes[CHAR_MICHAEL] = g_savedGlobalsnorman.sPlayerData.sInfo.sVariations[CHAR_MICHAEL]
			g_sLastStoredPlayerPedClothes[CHAR_FRANKLIN] = g_savedGlobalsnorman.sPlayerData.sInfo.sVariations[CHAR_FRANKLIN]
			g_sLastStoredPlayerPedClothes[CHAR_TREVOR] = g_savedGlobalsnorman.sPlayerData.sInfo.sVariations[CHAR_TREVOR]
			//Yes, setup a replay and don't release triggers yet. We need to wait for a replay response.
			g_availableMissionsTU[paramAvailableArrayPos].bitflags = CLEAR_ALL_AVAILABLE_MISSIONS_BITFLAGS
		#ENDIF
		#IF NOT USE_CLF_DLC
		#if not USE_NRM_DLC
			// Update last stored variations
			g_sLastStoredPlayerPedClothes[CHAR_MICHAEL] = g_savedGlobals.sPlayerData.sInfo.sVariations[CHAR_MICHAEL]
			g_sLastStoredPlayerPedClothes[CHAR_FRANKLIN] = g_savedGlobals.sPlayerData.sInfo.sVariations[CHAR_FRANKLIN]
			g_sLastStoredPlayerPedClothes[CHAR_TREVOR] = g_savedGlobals.sPlayerData.sInfo.sVariations[CHAR_TREVOR]
			//Yes, setup a replay and don't release triggers yet. We need to wait for a replay response.
			g_availableMissions[paramAvailableArrayPos].bitflags = CLEAR_ALL_AVAILABLE_MISSIONS_BITFLAGS
		#ENDIF
		#endif
		SETUP_MISSION_REPLAY(paramCoreVarsArrayPos)
		
		RETURN STAY_ON_THIS_COMMAND
	ENDIF
	
	// Not doing a fail screen, call mission over routine
	MISSION_PROCESSING_MISSION_OVER(paramMissionID)
	
	//Reset the skip fail screen flag
	g_bSkipFailScreen = FALSE
	
	//Release triggers and candidate ID.
	MISSION_PROCESSING_RELEASE_MISSION(paramCoreVarsArrayPos, paramAvailableArrayPos, paramHadTrigger)
	
	DO_MISSION_PASSED_OR_FAILED_STAT_UPDATE(FALSE, ENUM_TO_INT(paramMissionID))
	
	CPRINTLN(DEBUG_FLOW, "DO_MISSION(", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(paramMissionID), "):  Ran mission failed procedures.")

	RETURN INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue3) // Fail jump label.
ENDFUNC

#if USE_CLF_DLC
FUNC JUMP_LABEL_IDS PERFORM_DO_MISSION_AT_BLIPCLF(STRANDS paramStrandID, INT paramCoreVarsArrayPos)
	// --------INIT --------------------------------------------------------
	// Ensure the Script Vars Array position is legal
	IF (paramCoreVarsArrayPos = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_DO_MISSION_AT_BLIP: The Core Variables Array Position parameter is illegal. MOVING ON TO NEXT COMMAND.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Retrieve the details for this mission from the 'available missions' array
	INT availableMissionIndex = GET_AVAILABLE_MISSION_INDEX_FOR_STORED_MISSION(paramCoreVarsArrayPos)
	SP_MISSIONS eMissionID	= INT_TO_ENUM(SP_MISSIONS, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue1)
	INT missionScriptHash = g_sMissionStaticData[eMissionID].scriptHash
	
	#IF IS_DEBUG_BUILD
		STRING debugMissionIDString = GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMissionID)
	#ENDIF
	
	//Optimisation. Start loading required scripts as soon as the player locks in to a trigger.
	IF g_bPlayerLockedInToTrigger
		IF g_eMissionSceneToCleanup = eMissionID
		
			IF NOT IS_BIT_SET(g_sMissionStaticData[eMissionID].settingsBitset, MF_INDEX_NO_PASS)
				CDEBUG3LN(DEBUG_FLOW, "DO_MISSION_AT_BLIP(", debugMissionIDString, "): Preloading stat watcher for CLF mission.")
				REQUEST_SCRIPT_WITH_NAME_HASH( HASH("mission_stat_watchCLF") )				
			ENDIF
			REQUEST_SCRIPT_WITH_NAME_HASH(missionScriptHash)
		ENDIF
	ENDIF
	
	// --------REPLAY SETUP / REJECTION --------------------------------------
	// If a replay is in the process of being setup, wait here
	IF IS_REPLAY_BEING_PROCESSED()
		//CDEBUG3LN(DEBUG_REPLAY, "Replay is being processed, pausing perform do mission at blip.")
		RETURN STAY_ON_THIS_COMMAND
	ELSE
		// If replay has been rejected, clean up the trigger and release candidate ID and exit
		// (this will make IS_REPLAY_BEING_PROCESSED return false, so other missions can try to launch)
		IF g_replay.replayCoreVarsIndex = paramCoreVarsArrayPos
			IF g_replay.replayStageID =   RS_REJECTED
				// Replay rejected, call mission over routine
				MISSION_PROCESSING_MISSION_OVER(eMissionID)
				CPRINTLN(DEBUG_REPLAY, "Replay has been rejected, cleaning up mission.")
				MISSION_PROCESSING_RELEASE_MISSION(paramCoreVarsArrayPos, availableMissionIndex, TRUE)
				CPRINTLN(DEBUG_FLOW, "DO_MISSION_AT_BLIP(", debugMissionIDString, "):  Failed after replay rejection.")
				RETURN INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue3) // Fail jump label.
			ENDIF
		ENDIF
	ENDIF
	
	// --------REGISTER MISSION ------------------------------------------------
	// If the mission is not already 'available' then register the mission with the 'available missions' array
	// NOTE: A strand that reaches a 'do mission' command registers the mission as available and stores a
	// pointer to the mission's details together with some control variables for triggering the mission.
	IF (availableMissionIndex = ILLEGAL_ARRAY_POSITION)
		CDEBUG3LN(DEBUG_REPLAY, "availableMissionIndex = ILLEGAL_ARRAY_POSITION")
		availableMissionIndex = REGISTER_AVAILABLE_MISSION(paramCoreVarsArrayPos)
		
		// If the registration failed then exit
		IF (availableMissionIndex = ILLEGAL_ARRAY_POSITION)
			RETURN STAY_ON_THIS_COMMAND
		ENDIF
		
		#IF IS_DEBUG_BUILD
			// As an extra safety check, on successful registration ensure that the rest of the struct details are empty
			IF NOT (g_availableMissionsTU[availableMissionIndex].missionTriggerID = NO_CANDIDATE_ID)
				CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_DO_MISSION_AT_BLIP: Successfully registered in the Available Missions array but there seems to be existing data in the new array slot.")
			ENDIF
		#ENDIF
	ENDIF
	
	// Register this mission with the Mission Triggering System if it has not already been registered
	INT	missionTriggerID				= g_availableMissionsTU[availableMissionIndex].missionTriggerID
	
	// Only register the mission if it isn't being replayed
	IF NOT (IS_BIT_SET(g_availableMissionsTU[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_BEING_REPLAYED))
		IF (missionTriggerID = NO_CANDIDATE_ID)
		
			//Before registering the mission as triggerable, check if the player first needs to leave the trigger area.
			IF g_sMissionActiveData[eMissionID].leaveArea
				#IF IS_DEBUG_BUILD
					IF (GET_GAME_TIMER() % 5000) < 75 //Stop the debug message spamming too much. Print every 5 seconds.
						CPRINTLN(DEBUG_FLOW, "DO_MISSION_AT_BLIP(", debugMissionIDString, "): Waiting for player to leave area before setting up mission trigger.")
					ENDIF
				#ENDIF
				RETURN STAY_ON_THIS_COMMAND
			ENDIF
			
			CPRINTLN(DEBUG_FLOW, "DO_MISSION_AT_BLIP(", debugMissionIDString, "): Registering new trigger for mission.")
			
			// ...not registered with Mission Triggering System, so register this mission
			INT thePlayerBits					= g_sMissionStaticData[eMissionID].triggerCharBitset
			STATIC_BLIP_NAME_ENUM staticBlipID	= g_sMissionStaticData[eMissionID].blip
			
			// Make sure the blip is valid - if not, temporarily set it to any old valid blip
			IF (staticBlipID = STATIC_BLIP_NAME_DUMMY_FINAL)
				CERRORLN(DEBUG_FLOW, "PERFORM_DO_MISSION_AT_BLIP: Mission does not have a valid blip. Look at console log for details. Bug BenR.")
				g_sMissionStaticData[eMissionID].blip = STATIC_BLIP_MISSION_CLF_ARA_1 // Allow the game to run without an array overrun.
				staticBlipID = STATIC_BLIP_MISSION_CLF_ARA_1
			ENDIF
			
			IF MISSION_CANNOT_BE_REGISTERED_BECAUSE_OF_ACTIVE_FRIEND(eMissionID)
				CPRINTLN(DEBUG_FLOW, "DO_MISSION_AT_BLIP(", debugMissionIDString, "): Won't be registered as triggerable as player has activity organised with mission character.")
				RETURN STAY_ON_THIS_COMMAND
			ENDIF
			
			REGISTER_SCENE_MISSION_TRIGGER(missionTriggerID, staticBlipID, eMissionID, paramStrandID, MCTID_CONTACT_POINT, thePlayerBits)

			// ...check for registration problem
			IF (missionTriggerID = NO_CANDIDATE_ID)
				// ...mission failed to register with the Mission Triggering System
				CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_DO_MISSION_AT_BLIP: Failed to register mission with Mission Triggering System.")
				RETURN STAY_ON_THIS_COMMAND
			ENDIF
			
			// Block a friend from being contacted while on a mission with that friend.
			SET_ALL_FRIEND_BLOCKS_FOR_MISSION(eMissionID)
			
			// Store the retrieved Mission Trigger ID in the 'available missions' array
			g_availableMissionsTU[availableMissionIndex].missionTriggerID = missionTriggerID
		ENDIF
	ENDIF
	
		
	// ----------------MISSION LAUNCH -----------------------------------------------
	// Check if the mission is running
	IF NOT (IS_BIT_SET(g_availableMissionsTU[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_RUNNING))
	
		// Check if the mission is launching
		IF NOT (IS_BIT_SET(g_availableMissionsTU[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_LAUNCHING))
			// ...no, the mission is not yet launching, so check if it is being replayed and therefore should launch immediately
			IF NOT (IS_BIT_SET(g_availableMissionsTU[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_BEING_REPLAYED))
				// ...no, not being replayed, so check if the Mission Triggering System allows the mission to trigger
				IF NOT (HAS_MISSION_TRIGGER_TRIGGERED(missionTriggerID))					
						// Has this mission already been flagged as passed?
						IF g_savedGlobalsClifford.sFlow.missionSavedData[eMissionID].completed
							CPRINTLN(DEBUG_FLOW, "DO_MISSION_AT_BLIP(", debugMissionIDString, "): Mission already flagged as passed. Automatically passing.")
							SET_BIT(g_availableMissionsTU[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_RUNNING)
							SET_BIT(g_availableMissionsTU[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_PASSED)
							SET_BIT(g_availableMissionsTU[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_BACKGROUND_PASS)
							IF g_eMissionSceneToCleanup = eMissionID
								MISSION_FLOW_RELEASE_TRIGGER_SCENE_ASSETS(eMissionID)
							ENDIF
						ENDIF										
					RETURN STAY_ON_THIS_COMMAND
				ENDIF
			ENDIF
			
			CPRINTLN(DEBUG_FLOW, "DO_MISSION_AT_BLIP(", debugMissionIDString, "): Mission trigger fired.")
			
			// Request the mission script and stat watcher.
			IF NOT IS_BIT_SET(g_sMissionStaticData[eMissionID].settingsBitset, MF_INDEX_NO_PASS)
				CDEBUG3LN(DEBUG_FLOW, "DO_MISSION_AT_BLIP(", debugMissionIDString, "): Requesting stat watcher for CLF mission as mission is about to launch.")
				REQUEST_SCRIPT_WITH_NAME_HASH( HASH("mission_stat_watchCLF") )			

			ENDIF
			
			REQUEST_SCRIPT_WITH_NAME_HASH(missionScriptHash)
			SET_BIT(g_availableMissionsTU[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_LAUNCHING)
			
			//Wait for the main thread to be finished before allowing to launch.
			IF NOT g_isSPMainInitialised
				CPRINTLN(DEBUG_FLOW, "DO_MISSION_AT_BLIP(", debugMissionIDString, "): Waiting for the SP main thread to be initialised.")
				RETURN STAY_ON_THIS_COMMAND
			ENDIF

			// The mission is in the process of launching. 
			// Remove control and make the player invincible for the few frames it takes to start the mission.
			IF IS_PLAYER_PLAYING(PLAYER_ID())
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						VEHICLE_INDEX vehPlayer = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
						//Bring the vehicle out of critical state if it is in one.
						IF GET_ENTITY_HEALTH(vehPlayer) < 1
							SET_ENTITY_HEALTH(vehPlayer, 1)
						ENDIF
						IF GET_VEHICLE_ENGINE_HEALTH(vehPlayer) < 1
							SET_VEHICLE_ENGINE_HEALTH(vehPlayer, 1)
						ENDIF
						IF GET_VEHICLE_PETROL_TANK_HEALTH(vehPlayer) < 1
							SET_VEHICLE_PETROL_TANK_HEALTH(vehPlayer, 1)
						ENDIF
						
						//Bring the vehicle to a halt.
						IF NOT IS_BIT_SET(g_sMissionStaticData[eMissionID].settingsBitset, MF_INDEX_HAS_LEADIN)
						AND NOT IS_BIT_SET(g_sMissionStaticData[eMissionID].settingsBitset, MF_INDEX_NO_VEH_STOP)
							IF IS_VEHICLE_ON_ALL_WHEELS(vehPlayer)
								BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vehPlayer, 2.5, 2, 0.5, TRUE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			RETURN STAY_ON_THIS_COMMAND
		ENDIF

		//Launch the mission stat watcher
		IF NOT IS_BIT_SET(g_sMissionStaticData[eMissionID].settingsBitset, MF_INDEX_NO_PASS)				
			IF NOT IS_BIT_SET(g_availableMissionsTU[availableMissionIndex].bitflags, BITS_AVAILABLE_STAT_WATCHER_RUNNING)
				IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH( HASH("mission_stat_watchCLF") ) > 0
					CPRINTLN(DEBUG_FLOW, "DO_MISSION_AT_BLIP(", debugMissionIDString, "): Waiting for old stat watcher to terminate.")
					TERMINATE_STAT_WATCHER()
					RETURN STAY_ON_THIS_COMMAND // Wait until old watcher has terminated.
				ELIF HAS_SCRIPT_WITH_NAME_HASH_LOADED( HASH("mission_stat_watchCLF") )
					CPRINTLN(DEBUG_FLOW, "DO_MISSION_AT_BLIP(", debugMissionIDString, "): Starting new stat watcher thread for mission.")
					START_NEW_SCRIPT_WITH_NAME_HASH( HASH("mission_stat_watchCLF"), DEFAULT_STACK_SIZE)
				    SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED( HASH("mission_stat_watchCLF") )
					SET_BIT(g_availableMissionsTU[availableMissionIndex].bitflags, BITS_AVAILABLE_STAT_WATCHER_RUNNING)
				ELSE
					CPRINTLN(DEBUG_FLOW, "DO_MISSION_AT_BLIP(", debugMissionIDString, "): Waiting for stat watcher thread to stream in.")
					RETURN STAY_ON_THIS_COMMAND // Waiting for script to load.
				ENDIF			
			ENDIF		
		ENDIF
	
		// Mission launching, so always requests the script again to make sure code knows script is interested in this script
		REQUEST_SCRIPT_WITH_NAME_HASH(missionScriptHash)
		
		// Has the script loaded?
		IF NOT HAS_SCRIPT_WITH_NAME_HASH_LOADED(missionScriptHash)
			CPRINTLN(DEBUG_FLOW, "DO_MISSION_AT_BLIP(", debugMissionIDString, "): Waiting for script to load.")
			RETURN STAY_ON_THIS_COMMAND
		ENDIF
		
		//Is a switch camera in progress.
		IF IS_PLAYER_SWITCH_IN_PROGRESS()
			SWITCH_TYPE eCamSwitchType = GET_PLAYER_SWITCH_TYPE()
			IF eCamSwitchType <> SWITCH_TYPE_SHORT
				SWITCH_STATE eCamSwitchState = GET_PLAYER_SWITCH_STATE() 		
				IF eCamSwitchState < SWITCH_STATE_JUMPCUT_DESCENT
					CDEBUG1LN(DEBUG_FLOW, "DO_MISSION_AT_BLIP(", debugMissionIDString, "): Waiting for switch camera to enter descent.")
					RETURN STAY_ON_THIS_COMMAND
				ENDIF
			ENDIF
		ENDIF
	
		
		//Is an intro cutscene load request waiting to start?
		IF g_bFlowLoadIntroCutscene AND (NOT g_bFlowLoadRequestStarted)
			CDEBUG1LN(DEBUG_FLOW, "DO_MISSION_AT_BLIP(", debugMissionIDString, "): Waiting for mission intro cutscene to start loading.")
			RETURN STAY_ON_THIS_COMMAND
		ENDIF
		
		//Start getting out of a vehicle if we're in one.
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF NOT IS_BIT_SET(g_sMissionStaticData[eMissionID].settingsBitset, MF_INDEX_HAS_LEADIN)
					IF NOT IS_BIT_SET(g_sMissionStaticData[eMissionID].settingsBitset, MF_INDEX_NO_VEH_EXIT)
						TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID(), 0)
						RETURN STAY_ON_THIS_COMMAND						
					ENDIF
				ENDIF
			ENDIF
			
			//Play 400ms of the getting out of vehicle animation if we are leaving one.
			IF GET_IS_TASK_ACTIVE(PLAYER_PED_ID(), CODE_TASK_EXIT_VEHICLE)
				IF g_iTimeStartedExitingVehicle = -1
					g_iTimeStartedExitingVehicle = GET_GAME_TIMER()
					RETURN STAY_ON_THIS_COMMAND
				ELIF (GET_GAME_TIMER() - g_iTimeStartedExitingVehicle) < 400
					RETURN STAY_ON_THIS_COMMAND
				ENDIF
			ENDIF
		ENDIF

		//Is an intro cutscene load still ongoing?
		IF g_bFlowLoadingIntroCutscene
			//Fade the game out to encourage the cutscene to load.
			MANUALLY_ACTIVATE_CUTSCENE_FAILSAFE()
			
			//Have we made cutscene variation requests yet?
			IF NOT g_bFlowIntroVariationRequestsMade
				CDEBUG1LN(DEBUG_FLOW, "DO_MISSION_AT_BLIP(", debugMissionIDString, "): Waiting for cutscene requests to go through.")
				RETURN STAY_ON_THIS_COMMAND
			ENDIF
			CDEBUG1LN(DEBUG_FLOW, "DO_MISSION_AT_BLIP(", debugMissionIDString, "): Waiting for mission intro cutscene to finish loading.")
			RETURN STAY_ON_THIS_COMMAND
		ENDIF
		
		//Mission has loaded, so launch it
		g_availableMissionsTU[availableMissionIndex].theThread = START_NEW_SCRIPT_WITH_NAME_HASH(missionScriptHash, MISSION_STACK_SIZE)
		SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(missionScriptHash)
		SET_BIT(g_availableMissionsTU[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_RUNNING)

		//Run common mission starting routines.
		MISSION_PROCESSING_MISSION_STARTING(paramCoreVarsArrayPos, availableMissionIndex, eMissionID, paramStrandID)
		
		//Reset getting out of vehicle timer.
		g_iTimeStartedExitingVehicle = -1
		
		RETURN STAY_ON_THIS_COMMAND
	ENDIF

	// ---------------RUNNING MISSION ---------------------------------------------
	
	// Check if mission has failed so we can keep the mission thread running whilst we set up a replay
	BOOL hasFailed = IS_BIT_SET(g_availableMissionsTU[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_FAILED)
	
	IF (IS_THREAD_ACTIVE(g_availableMissionsTU[availableMissionIndex].theThread))
		IF hasFailed
			IF NOT IS_BIT_SET(g_sMissionStaticData[eMissionID].settingsBitset, MF_INDEX_NO_FAIL)
			AND g_bSkipFailScreen = FALSE
				// going to a replay screen, go on to termination section
				// mission script continues to run for a while during replay processing
				CPRINTLN(DEBUG_FLOW, "DO_MISSION_AT_BLIP(", debugMissionIDString, "): Failed and going to replay screen.")
			ELSE
				// not going to a replay screen, keep running
				RETURN STAY_ON_THIS_COMMAND
			ENDIF
		ELSE
			// not going to a replay screen, keep running
			RETURN STAY_ON_THIS_COMMAND
		ENDIF
	ENDIF

	// ----------MISSION TERMINATION -------------------------------------

	// ...get the Pass/Fail state for the mission
	BOOL hasPassed = IS_BIT_SET(g_availableMissionsTU[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_PASSED)

	// ...ensure the mission hasn't both passed and failed.
	IF hasPassed AND hasFailed
		CPRINTLN(DEBUG_FLOW, "DO_MISSION_AT_BLIP(", debugMissionIDString, "): Mission has both PASSED and FAILED. Treating as PASSED.")
		SCRIPT_ASSERT("PERFORM_DO_MISSION_AT_BLIP: Mission has both PASSED and FAILED. Treating as PASSED.")
		hasFailed = FALSE
	ENDIF
	
	// ...ensure the mission has either passed or failed.
	IF NOT (hasPassed OR hasFailed)
		CPRINTLN(DEBUG_FLOW, "DO_MISSION_AT_BLIP(", debugMissionIDString, "): Mission has not PASSED or FAILED. Treating as FAILED.")
		SCRIPT_ASSERT("PERFORM_DO_MISSION_AT_BLIP: : Mission has not PASSED or FAILED. Treating as FAILED.")
		#IF IS_DEBUG_BUILD
			// overwrite the fail reason, so mission scripters know the mission has failed due to an array overrun or similar
			g_txtMissionFailReason = "FLOW_FAIL"
		#ENDIF
		hasFailed = TRUE
	ENDIF
	
	//----------------MISSION PASSED--------------------------------
	IF (hasPassed)
		RETURN MISSION_PROCESSING_MISSION_PASSED(paramCoreVarsArrayPos, availableMissionIndex, eMissionID, TRUE, IS_BIT_SET(g_availableMissionsTU[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_BACKGROUND_PASS))
	ENDIF
	
	//----------------MISSION FAILED / REPLAY SETUP ------------------------
	RETURN MISSION_PROCESSING_MISSION_FAILED(paramCoreVarsArrayPos, availableMissionIndex, eMissionID, TRUE)
ENDFUNC
#endif

#if USE_NRM_DLC
FUNC JUMP_LABEL_IDS PERFORM_DO_MISSION_AT_BLIPNRM(STRANDS paramStrandID, INT paramCoreVarsArrayPos)

	// --------INIT --------------------------------------------------------
	// Ensure the Script Vars Array position is legal
	IF (paramCoreVarsArrayPos = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_DO_MISSION_AT_BLIP: The Core Variables Array Position parameter is illegal. MOVING ON TO NEXT COMMAND.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Retrieve the details for this mission from the 'available missions' array
	INT availableMissionIndex = GET_AVAILABLE_MISSION_INDEX_FOR_STORED_MISSION(paramCoreVarsArrayPos)
	SP_MISSIONS eMissionID	= INT_TO_ENUM(SP_MISSIONS, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue1)
	INT missionScriptHash = g_sMissionStaticData[eMissionID].scriptHash
	
	#IF IS_DEBUG_BUILD
		STRING debugMissionIDString = GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMissionID)
	#ENDIF
	
	//Optimisation. Start loading required scripts as soon as the player locks in to a trigger.
	IF g_bPlayerLockedInToTrigger
		IF g_eMissionSceneToCleanup = eMissionID
		
			IF NOT IS_BIT_SET(g_sMissionStaticData[eMissionID].settingsBitset, MF_INDEX_NO_PASS)
				CDEBUG3LN(DEBUG_FLOW, "DO_MISSION_AT_BLIP(", debugMissionIDString, "): Preloading stat watcher for NRM mission.")
				REQUEST_SCRIPT_WITH_NAME_HASH( HASH("mission_stat_watchNRM") )				
#IF IS_DEBUG_BUILD
			ELSE
				CDEBUG3LN(DEBUG_FLOW, "DO_MISSION_AT_BLIP(", debugMissionIDString, "): Skipped preloading stat watcher as the mission is flagged to have no pass screen.")
#ENDIF	
			ENDIF
			
			REQUEST_SCRIPT_WITH_NAME_HASH(missionScriptHash)
		ENDIF
	ENDIF
	
	// --------REPLAY SETUP / REJECTION --------------------------------------
	// If a replay is in the process of being setup, wait here
	IF IS_REPLAY_BEING_PROCESSED()
		//CDEBUG3LN(DEBUG_REPLAY, "Replay is being processed, pausing perform do mission at blip.")
		RETURN STAY_ON_THIS_COMMAND
	ELSE
		// If replay has been rejected, clean up the trigger and release candidate ID and exit
		// (this will make IS_REPLAY_BEING_PROCESSED return false, so other missions can try to launch)
		IF g_replay.replayCoreVarsIndex = paramCoreVarsArrayPos
			IF g_replay.replayStageID =   RS_REJECTED
				// Replay rejected, call mission over routine
				MISSION_PROCESSING_MISSION_OVER(eMissionID)
				CPRINTLN(DEBUG_REPLAY, "Replay has been rejected, cleaning up mission.")
				MISSION_PROCESSING_RELEASE_MISSION(paramCoreVarsArrayPos, availableMissionIndex, TRUE)
				CPRINTLN(DEBUG_FLOW, "DO_MISSION_AT_BLIP(", debugMissionIDString, "):  Failed after replay rejection.")
				RETURN INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue3) // Fail jump label.
			ENDIF
		ENDIF
	ENDIF
	
	// --------REGISTER MISSION ------------------------------------------------
	// If the mission is not already 'available' then register the mission with the 'available missions' array
	// NOTE: A strand that reaches a 'do mission' command registers the mission as available and stores a
	// pointer to the mission's details together with some control variables for triggering the mission.
	IF (availableMissionIndex = ILLEGAL_ARRAY_POSITION)
		CDEBUG3LN(DEBUG_REPLAY, "availableMissionIndex = ILLEGAL_ARRAY_POSITION")
		availableMissionIndex = REGISTER_AVAILABLE_MISSION(paramCoreVarsArrayPos)
		
		// If the registration failed then exit
		IF (availableMissionIndex = ILLEGAL_ARRAY_POSITION)
			RETURN STAY_ON_THIS_COMMAND
		ENDIF
		
		#IF IS_DEBUG_BUILD
			// As an extra safety check, on successful registration ensure that the rest of the struct details are empty
			IF NOT (g_availableMissionsTU[availableMissionIndex].missionTriggerID = NO_CANDIDATE_ID)
				CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_DO_MISSION_AT_BLIP: Successfully registered in the Available Missions array but there seems to be existing data in the new array slot.")
			ENDIF
		#ENDIF
	ENDIF
	
	// Register this mission with the Mission Triggering System if it has not already been registered
	INT	missionTriggerID				= g_availableMissionsTU[availableMissionIndex].missionTriggerID
	
	// Only register the mission if it isn't being replayed
	IF NOT (IS_BIT_SET(g_availableMissionsTU[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_BEING_REPLAYED))
		IF (missionTriggerID = NO_CANDIDATE_ID)
		
			//Before registering the mission as triggerable, check if the player first needs to leave the trigger area.
			IF g_sMissionActiveData[eMissionID].leaveArea
				#IF IS_DEBUG_BUILD
					IF (GET_GAME_TIMER() % 5000) < 75 //Stop the debug message spamming too much. Print every 5 seconds.
						CPRINTLN(DEBUG_FLOW, "DO_MISSION_AT_BLIP(", debugMissionIDString, "): Waiting for player to leave area before setting up mission trigger.")
					ENDIF
				#ENDIF
				RETURN STAY_ON_THIS_COMMAND
			ENDIF
			
			CPRINTLN(DEBUG_FLOW, "DO_MISSION_AT_BLIP(", debugMissionIDString, "): Registering new trigger for mission.")
			
			// ...not registered with Mission Triggering System, so register this mission
			INT thePlayerBits					= g_sMissionStaticData[eMissionID].triggerCharBitset
			STATIC_BLIP_NAME_ENUM staticBlipID	= g_sMissionStaticData[eMissionID].blip
			
			// Make sure the blip is valid - if not, temporarily set it to any old valid blip
			IF (staticBlipID = STATIC_BLIP_NAME_DUMMY_FINAL)
				CERRORLN(DEBUG_FLOW, "PERFORM_DO_MISSION_AT_BLIP: Mission does not have a valid blip. Look at console log for details. Bug BenR.")
				g_sMissionStaticData[eMissionID].blip = STATIC_BLIP_MISSION_NRM_RADIO_A // Allow the game to run without an array overrun.
				staticBlipID = STATIC_BLIP_MISSION_NRM_RADIO_A
			ENDIF
			
			IF MISSION_CANNOT_BE_REGISTERED_BECAUSE_OF_ACTIVE_FRIEND(eMissionID)
				CPRINTLN(DEBUG_FLOW, "DO_MISSION_AT_BLIP(", debugMissionIDString, "): Won't be registered as triggerable as player has activity organised with mission character.")
				RETURN STAY_ON_THIS_COMMAND
			ENDIF
			
			REGISTER_SCENE_MISSION_TRIGGER(missionTriggerID, staticBlipID, eMissionID, paramStrandID, MCTID_CONTACT_POINT, thePlayerBits)

			// ...check for registration problem
			IF (missionTriggerID = NO_CANDIDATE_ID)
				// ...mission failed to register with the Mission Triggering System
				CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_DO_MISSION_AT_BLIP: Failed to register mission with Mission Triggering System.")
				RETURN STAY_ON_THIS_COMMAND
			ENDIF
			
			// Block a friend from being contacted while on a mission with that friend.
			SET_ALL_FRIEND_BLOCKS_FOR_MISSION(eMissionID)
			
			// Store the retrieved Mission Trigger ID in the 'available missions' array
			g_availableMissionsTU[availableMissionIndex].missionTriggerID = missionTriggerID
		ENDIF
	ENDIF
	
		
	// ----------------MISSION LAUNCH -----------------------------------------------
	// Check if the mission is running
	IF NOT (IS_BIT_SET(g_availableMissionsTU[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_RUNNING))
	
		// Check if the mission is launching
		IF NOT (IS_BIT_SET(g_availableMissionsTU[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_LAUNCHING))
			// ...no, the mission is not yet launching, so check if it is being replayed and therefore should launch immediately
			IF NOT (IS_BIT_SET(g_availableMissionsTU[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_BEING_REPLAYED))
				// ...no, not being replayed, so check if the Mission Triggering System allows the mission to trigger
				IF NOT (HAS_MISSION_TRIGGER_TRIGGERED(missionTriggerID))
				
					// Has this mission already been flagged as passed?
					IF g_savedGlobalsnorman.sFlow.missionSavedData[eMissionID].completed
						CPRINTLN(DEBUG_FLOW, "DO_MISSION_AT_BLIP(", debugMissionIDString, "): Mission already flagged as passed. Automatically passing.")
						SET_BIT(g_availableMissionsTU[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_RUNNING)
						SET_BIT(g_availableMissionsTU[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_PASSED)
						SET_BIT(g_availableMissionsTU[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_BACKGROUND_PASS)
						IF g_eMissionSceneToCleanup = eMissionID
							MISSION_FLOW_RELEASE_TRIGGER_SCENE_ASSETS(eMissionID)
						ENDIF
					ENDIF
									
					RETURN STAY_ON_THIS_COMMAND
				ENDIF
			ENDIF
			
			CPRINTLN(DEBUG_FLOW, "DO_MISSION_AT_BLIP(", debugMissionIDString, "): Mission trigger fired.")
			
			// Request the mission script and stat watcher.
			IF NOT IS_BIT_SET(g_sMissionStaticData[eMissionID].settingsBitset, MF_INDEX_NO_PASS)
				CDEBUG3LN(DEBUG_FLOW, "DO_MISSION_AT_BLIP(", debugMissionIDString, "): Requesting stat watcher for NRM mission as mission is about to launch.")
				REQUEST_SCRIPT_WITH_NAME_HASH( HASH("mission_stat_watchNRM") )				
#IF IS_DEBUG_BUILD
			ELSE
				CDEBUG3LN(DEBUG_FLOW, "DO_MISSION_AT_BLIP(", debugMissionIDString, "): Skipped requesting stat watcher as the mission is flagged to have no pass screen.")
#ENDIF	
			ENDIF
			
			REQUEST_SCRIPT_WITH_NAME_HASH(missionScriptHash)
			SET_BIT(g_availableMissionsTU[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_LAUNCHING)
			
			//Wait for the main thread to be finished before allowing to launch.
			IF NOT g_isSPMainInitialised
				CPRINTLN(DEBUG_FLOW, "DO_MISSION_AT_BLIP(", debugMissionIDString, "): Waiting for the SP main thread to be initialised.")
				RETURN STAY_ON_THIS_COMMAND
			ENDIF

			// The mission is in the process of launching. 
			// Remove control and make the player invincible for the few frames it takes to start the mission.
			IF IS_PLAYER_PLAYING(PLAYER_ID())
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						VEHICLE_INDEX vehPlayer = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
						//Bring the vehicle out of critical state if it is in one.
						IF GET_ENTITY_HEALTH(vehPlayer) < 1
							SET_ENTITY_HEALTH(vehPlayer, 1)
						ENDIF
						IF GET_VEHICLE_ENGINE_HEALTH(vehPlayer) < 1
							SET_VEHICLE_ENGINE_HEALTH(vehPlayer, 1)
						ENDIF
						IF GET_VEHICLE_PETROL_TANK_HEALTH(vehPlayer) < 1
							SET_VEHICLE_PETROL_TANK_HEALTH(vehPlayer, 1)
						ENDIF
						
						//Bring the vehicle to a halt.
						IF NOT IS_BIT_SET(g_sMissionStaticData[eMissionID].settingsBitset, MF_INDEX_HAS_LEADIN)
						AND NOT IS_BIT_SET(g_sMissionStaticData[eMissionID].settingsBitset, MF_INDEX_NO_VEH_STOP)
							IF IS_VEHICLE_ON_ALL_WHEELS(vehPlayer)
								BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vehPlayer, 2.5, 2, 0.5, TRUE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			RETURN STAY_ON_THIS_COMMAND
		ENDIF

		//Launch the mission stat watcher
		IF NOT IS_BIT_SET(g_sMissionStaticData[eMissionID].settingsBitset, MF_INDEX_NO_PASS)
							
			IF NOT IS_BIT_SET(g_availableMissionsTU[availableMissionIndex].bitflags, BITS_AVAILABLE_STAT_WATCHER_RUNNING)
				IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH( HASH("mission_stat_watchNRM") ) > 0
					CPRINTLN(DEBUG_FLOW, "DO_MISSION_AT_BLIP(", debugMissionIDString, "): Waiting for old stat watcher to terminate.")
					TERMINATE_STAT_WATCHER()
					RETURN STAY_ON_THIS_COMMAND // Wait until old watcher has terminated.
				ELIF HAS_SCRIPT_WITH_NAME_HASH_LOADED( HASH("mission_stat_watchNRM") )
					CPRINTLN(DEBUG_FLOW, "DO_MISSION_AT_BLIP(", debugMissionIDString, "): Starting new stat watcher thread for mission.")
					START_NEW_SCRIPT_WITH_NAME_HASH( HASH("mission_stat_watchNRM"), DEFAULT_STACK_SIZE)
				    SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED( HASH("mission_stat_watchNRM") )
					SET_BIT(g_availableMissionsTU[availableMissionIndex].bitflags, BITS_AVAILABLE_STAT_WATCHER_RUNNING)
				ELSE
					CPRINTLN(DEBUG_FLOW, "DO_MISSION_AT_BLIP(", debugMissionIDString, "): Waiting for stat watcher thread to stream in.")
					RETURN STAY_ON_THIS_COMMAND // Waiting for script to load.
				ENDIF
			#IF IS_DEBUG_BUILD
			ELSE
				PRINTLN("<STAT WATCHER> <FLOW> We think the stat watcher is already running for this mission, not launching it")
			#ENDIF
			ENDIF
			
		ENDIF
	
		// Mission launching, so always requests the script again to make sure code knows script is interested in this script
		REQUEST_SCRIPT_WITH_NAME_HASH(missionScriptHash)
		
		// Has the script loaded?
		IF NOT HAS_SCRIPT_WITH_NAME_HASH_LOADED(missionScriptHash)
			CPRINTLN(DEBUG_FLOW, "DO_MISSION_AT_BLIP(", debugMissionIDString, "): Waiting for script to load.")
			RETURN STAY_ON_THIS_COMMAND
		ENDIF
		
		IF IS_PLAYER_SWITCH_IN_PROGRESS()
			SWITCH_TYPE eCamSwitchType = GET_PLAYER_SWITCH_TYPE()
			IF eCamSwitchType <> SWITCH_TYPE_SHORT
				SWITCH_STATE eCamSwitchState = GET_PLAYER_SWITCH_STATE() 		
				IF eCamSwitchState < SWITCH_STATE_JUMPCUT_DESCENT
					CDEBUG1LN(DEBUG_FLOW, "DO_MISSION_AT_BLIP(", debugMissionIDString, "): Waiting for switch camera to enter descent.")
					RETURN STAY_ON_THIS_COMMAND
				ENDIF
			ENDIF
		ENDIF
		
		//Is an intro cutscene load request waiting to start?
		IF g_bFlowLoadIntroCutscene AND (NOT g_bFlowLoadRequestStarted)
			CDEBUG1LN(DEBUG_FLOW, "DO_MISSION_AT_BLIP(", debugMissionIDString, "): Waiting for mission intro cutscene to start loading.")
			RETURN STAY_ON_THIS_COMMAND
		ENDIF
		
		//Start getting out of a vehicle if we're in one.
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF NOT IS_BIT_SET(g_sMissionStaticData[eMissionID].settingsBitset, MF_INDEX_HAS_LEADIN)
					IF NOT IS_BIT_SET(g_sMissionStaticData[eMissionID].settingsBitset, MF_INDEX_NO_VEH_EXIT)
						TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID(), 0)
						RETURN STAY_ON_THIS_COMMAND
						
					ENDIF
				ENDIF
			ENDIF
			
			//Play 400ms of the getting out of vehicle animation if we are leaving one.
			IF GET_IS_TASK_ACTIVE(PLAYER_PED_ID(), CODE_TASK_EXIT_VEHICLE)
				IF g_iTimeStartedExitingVehicle = -1
					g_iTimeStartedExitingVehicle = GET_GAME_TIMER()
					RETURN STAY_ON_THIS_COMMAND
				ELIF (GET_GAME_TIMER() - g_iTimeStartedExitingVehicle) < 400
					RETURN STAY_ON_THIS_COMMAND
				ENDIF
			ENDIF
		ENDIF

		//Is an intro cutscene load still ongoing?
		IF g_bFlowLoadingIntroCutscene
			//Fade the game out to encourage the cutscene to load.
			MANUALLY_ACTIVATE_CUTSCENE_FAILSAFE()
			
			//Have we made cutscene variation requests yet?
			IF NOT g_bFlowIntroVariationRequestsMade
				CDEBUG1LN(DEBUG_FLOW, "DO_MISSION_AT_BLIP(", debugMissionIDString, "): Waiting for cutscene requests to go through.")
				RETURN STAY_ON_THIS_COMMAND
			ENDIF
			CDEBUG1LN(DEBUG_FLOW, "DO_MISSION_AT_BLIP(", debugMissionIDString, "): Waiting for mission intro cutscene to finish loading.")
			RETURN STAY_ON_THIS_COMMAND
		ENDIF
		
		//Mission has loaded, so launch it
		g_availableMissionsTU[availableMissionIndex].theThread = START_NEW_SCRIPT_WITH_NAME_HASH(missionScriptHash, MISSION_STACK_SIZE)
		SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(missionScriptHash)
		SET_BIT(g_availableMissionsTU[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_RUNNING)

		//Run common mission starting routines.
		MISSION_PROCESSING_MISSION_STARTING(paramCoreVarsArrayPos, availableMissionIndex, eMissionID, paramStrandID)
		
		//Reset getting out of vehicle timer.
		g_iTimeStartedExitingVehicle = -1
		
		RETURN STAY_ON_THIS_COMMAND
	ENDIF

	// ---------------RUNNING MISSION ---------------------------------------------
	
	// Check if mission has failed so we can keep the mission thread running whilst we set up a replay
	BOOL hasFailed = IS_BIT_SET(g_availableMissionsTU[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_FAILED)
	
	IF (IS_THREAD_ACTIVE(g_availableMissionsTU[availableMissionIndex].theThread))
		IF hasFailed
			IF NOT IS_BIT_SET(g_sMissionStaticData[eMissionID].settingsBitset, MF_INDEX_NO_FAIL)
			AND g_bSkipFailScreen = FALSE
				// going to a replay screen, go on to termination section
				// mission script continues to run for a while during replay processing
				CPRINTLN(DEBUG_FLOW, "DO_MISSION_AT_BLIP(", debugMissionIDString, "): Failed and going to replay screen.")
			ELSE
				// not going to a replay screen, keep running
				RETURN STAY_ON_THIS_COMMAND
			ENDIF
		ELSE
			// not going to a replay screen, keep running
			RETURN STAY_ON_THIS_COMMAND
		ENDIF
	ENDIF

	// ----------MISSION TERMINATION -------------------------------------

	// ...get the Pass/Fail state for the mission
	BOOL hasPassed = IS_BIT_SET(g_availableMissionsTU[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_PASSED)

	// ...ensure the mission hasn't both passed and failed.
	IF hasPassed AND hasFailed
		CPRINTLN(DEBUG_FLOW, "DO_MISSION_AT_BLIP(", debugMissionIDString, "): Mission has both PASSED and FAILED. Treating as PASSED.")
		SCRIPT_ASSERT("PERFORM_DO_MISSION_AT_BLIP: Mission has both PASSED and FAILED. Treating as PASSED.")
		hasFailed = FALSE
	ENDIF
	
	// ...ensure the mission has either passed or failed.
	IF NOT (hasPassed OR hasFailed)
		CPRINTLN(DEBUG_FLOW, "DO_MISSION_AT_BLIP(", debugMissionIDString, "): Mission has not PASSED or FAILED. Treating as FAILED.")
		SCRIPT_ASSERT("PERFORM_DO_MISSION_AT_BLIP: : Mission has not PASSED or FAILED. Treating as FAILED.")
		#IF IS_DEBUG_BUILD
			// overwrite the fail reason, so mission scripters know the mission has failed due to an array overrun or similar
			g_txtMissionFailReason = "FLOW_FAIL"
		#ENDIF
		hasFailed = TRUE
	ENDIF
	
	//----------------MISSION PASSED--------------------------------
	IF (hasPassed)
		RETURN MISSION_PROCESSING_MISSION_PASSED(paramCoreVarsArrayPos, availableMissionIndex, eMissionID, TRUE, IS_BIT_SET(g_availableMissionsTU[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_BACKGROUND_PASS))
	ENDIF
	
	//----------------MISSION FAILED / REPLAY SETUP ------------------------
	RETURN MISSION_PROCESSING_MISSION_FAILED(paramCoreVarsArrayPos, availableMissionIndex, eMissionID, TRUE)
ENDFUNC
#endif
FUNC JUMP_LABEL_IDS PERFORM_DO_MISSION_AT_BLIP(STRANDS paramStrandID, INT paramCoreVarsArrayPos)
#if USE_CLF_DLC
	return PERFORM_DO_MISSION_AT_BLIPCLF(paramStrandID, paramCoreVarsArrayPos)
#endif
#if USE_NRM_DLC
	return PERFORM_DO_MISSION_AT_BLIPNRM(paramStrandID, paramCoreVarsArrayPos)
#endif
#if not USE_CLF_DLC
#if not USE_NRM_DLC
	
	// --------INIT --------------------------------------------------------
	// Ensure the Script Vars Array position is legal
	IF (paramCoreVarsArrayPos = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_DO_MISSION_AT_BLIP: The Core Variables Array Position parameter is illegal. MOVING ON TO NEXT COMMAND.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Retrieve the details for this mission from the 'available missions' array
	INT availableMissionIndex = GET_AVAILABLE_MISSION_INDEX_FOR_STORED_MISSION(paramCoreVarsArrayPos)
	SP_MISSIONS eMissionID	= INT_TO_ENUM(SP_MISSIONS, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue1)
	INT missionScriptHash = g_sMissionStaticData[eMissionID].scriptHash
	
	#IF IS_DEBUG_BUILD
		STRING debugMissionIDString = GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMissionID)
	#ENDIF
	
	//Optimisation. Start loading required scripts as soon as the player locks in to a trigger.
	IF g_bPlayerLockedInToTrigger
		IF g_eMissionSceneToCleanup = eMissionID
		
			IF NOT IS_BIT_SET(g_sMissionStaticData[eMissionID].settingsBitset, MF_INDEX_NO_PASS)
				CDEBUG3LN(DEBUG_FLOW, "DO_MISSION_AT_BLIP(", debugMissionIDString, "): Preloading stat watcher for CLF mission.")
				REQUEST_SCRIPT_WITH_NAME_HASH( HASH("mission_stat_watcher") )				
#IF IS_DEBUG_BUILD
			ELSE
				CDEBUG3LN(DEBUG_FLOW, "DO_MISSION_AT_BLIP(", debugMissionIDString, "): Skipped preloading stat watcher as the mission is flagged to have no pass screen.")
#ENDIF	
			ENDIF
			
			REQUEST_SCRIPT_WITH_NAME_HASH(missionScriptHash)
		ENDIF
	ENDIF
	
	// --------REPLAY SETUP / REJECTION --------------------------------------
	// If a replay is in the process of being setup, wait here
	IF IS_REPLAY_BEING_PROCESSED()
		//CDEBUG3LN(DEBUG_REPLAY, "Replay is being processed, pausing perform do mission at blip.")
		RETURN STAY_ON_THIS_COMMAND
	ELSE
		// If replay has been rejected, clean up the trigger and release candidate ID and exit
		// (this will make IS_REPLAY_BEING_PROCESSED return false, so other missions can try to launch)
		IF g_replay.replayCoreVarsIndex = paramCoreVarsArrayPos
			IF g_replay.replayStageID =   RS_REJECTED
				// Replay rejected, call mission over routine
				MISSION_PROCESSING_MISSION_OVER(eMissionID)
				CPRINTLN(DEBUG_REPLAY, "Replay has been rejected, cleaning up mission.")
				MISSION_PROCESSING_RELEASE_MISSION(paramCoreVarsArrayPos, availableMissionIndex, TRUE)
				
				CPRINTLN(DEBUG_MISSION_STATS, "Cleaning up stat trigger supression as player exits early from mission.")
				g_MissionStatSystemSuppressVisual = FALSE
				g_bSuppressNextStatTrigger = FALSE
				
				CPRINTLN(DEBUG_FLOW, "DO_MISSION_AT_BLIP(", debugMissionIDString, "):  Failed after replay rejection.")
				RETURN INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue3) // Fail jump label.
			ENDIF
		ENDIF
	ENDIF
	
	// --------REGISTER MISSION ------------------------------------------------
	// If the mission is not already 'available' then register the mission with the 'available missions' array
	// NOTE: A strand that reaches a 'do mission' command registers the mission as available and stores a
	// pointer to the mission's details together with some control variables for triggering the mission.
	IF (availableMissionIndex = ILLEGAL_ARRAY_POSITION)
		CDEBUG3LN(DEBUG_REPLAY, "availableMissionIndex = ILLEGAL_ARRAY_POSITION")
		availableMissionIndex = REGISTER_AVAILABLE_MISSION(paramCoreVarsArrayPos)
		
		// If the registration failed then exit
		IF (availableMissionIndex = ILLEGAL_ARRAY_POSITION)
			RETURN STAY_ON_THIS_COMMAND
		ENDIF
		
		#IF IS_DEBUG_BUILD
			// As an extra safety check, on successful registration ensure that the rest of the struct details are empty
			IF NOT (g_availableMissions[availableMissionIndex].missionTriggerID = NO_CANDIDATE_ID)
				CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_DO_MISSION_AT_BLIP: Successfully registered in the Available Missions array but there seems to be existing data in the new array slot.")
			ENDIF
		#ENDIF
	ENDIF
	
	// Register this mission with the Mission Triggering System if it has not already been registered
	INT	missionTriggerID				= g_availableMissions[availableMissionIndex].missionTriggerID
	
	// Only register the mission if it isn't being replayed
	IF NOT (IS_BIT_SET(g_availableMissions[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_BEING_REPLAYED))
		IF (missionTriggerID = NO_CANDIDATE_ID)
		
			//Before registering the mission as triggerable, check if the player first needs to leave the trigger area.
			IF g_sMissionActiveData[eMissionID].leaveArea
				#IF IS_DEBUG_BUILD
					IF (GET_GAME_TIMER() % 5000) < 75 //Stop the debug message spamming too much. Print every 5 seconds.
						CPRINTLN(DEBUG_FLOW, "DO_MISSION_AT_BLIP(", debugMissionIDString, "): Waiting for player to leave area before setting up mission trigger.")
					ENDIF
				#ENDIF
				RETURN STAY_ON_THIS_COMMAND
			ENDIF
			
			CPRINTLN(DEBUG_FLOW, "DO_MISSION_AT_BLIP(", debugMissionIDString, "): Registering new trigger for mission.")
			
			// ...not registered with Mission Triggering System, so register this mission
			INT thePlayerBits					= g_sMissionStaticData[eMissionID].triggerCharBitset
			STATIC_BLIP_NAME_ENUM staticBlipID	= g_sMissionStaticData[eMissionID].blip
			
			// Make sure the blip is valid - if not, temporarily set it to any old valid blip
			IF (staticBlipID = STATIC_BLIP_NAME_DUMMY_FINAL)
				CERRORLN(DEBUG_FLOW, "PERFORM_DO_MISSION_AT_BLIP: Mission does not have a valid blip. Look at console log for details. Bug BenR.")
				g_sMissionStaticData[eMissionID].blip = STATIC_BLIP_MISSION_ARMENIAN // Allow the game to run without an array overrun.
				staticBlipID = STATIC_BLIP_MISSION_ARMENIAN
			ENDIF
			
			IF MISSION_CANNOT_BE_REGISTERED_BECAUSE_OF_ACTIVE_FRIEND(eMissionID)
				CPRINTLN(DEBUG_FLOW, "DO_MISSION_AT_BLIP(", debugMissionIDString, "): Won't be registered as triggerable as player has activity organised with mission character.")
				RETURN STAY_ON_THIS_COMMAND
			ENDIF
			
			REGISTER_SCENE_MISSION_TRIGGER(missionTriggerID, staticBlipID, eMissionID, paramStrandID, MCTID_CONTACT_POINT, thePlayerBits)

			// ...check for registration problem
			IF (missionTriggerID = NO_CANDIDATE_ID)
				// ...mission failed to register with the Mission Triggering System
				CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_DO_MISSION_AT_BLIP: Failed to register mission with Mission Triggering System.")
				RETURN STAY_ON_THIS_COMMAND
			ENDIF
			
			// Block a friend from being contacted while on a mission with that friend.
			SET_ALL_FRIEND_BLOCKS_FOR_MISSION(eMissionID)
			
			// Store the retrieved Mission Trigger ID in the 'available missions' array
			g_availableMissions[availableMissionIndex].missionTriggerID = missionTriggerID
		ENDIF
	ENDIF
	
		
	// ----------------MISSION LAUNCH -----------------------------------------------
	// Check if the mission is running
	IF NOT (IS_BIT_SET(g_availableMissions[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_RUNNING))
	
		// Check if the mission is launching
		IF NOT (IS_BIT_SET(g_availableMissions[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_LAUNCHING))
			// ...no, the mission is not yet launching, so check if it is being replayed and therefore should launch immediately
			IF NOT (IS_BIT_SET(g_availableMissions[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_BEING_REPLAYED))
				// ...no, not being replayed, so check if the Mission Triggering System allows the mission to trigger
				IF NOT (HAS_MISSION_TRIGGER_TRIGGERED(missionTriggerID))
										
					// Has this mission already been flagged as passed?
					IF g_savedGlobals.sFlow.missionSavedData[eMissionID].completed
						CPRINTLN(DEBUG_FLOW, "DO_MISSION_AT_BLIP(", debugMissionIDString, "): Mission already flagged as passed. Automatically passing.")
						SET_BIT(g_availableMissions[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_RUNNING)
						SET_BIT(g_availableMissions[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_PASSED)
						SET_BIT(g_availableMissions[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_BACKGROUND_PASS)
						IF g_eMissionSceneToCleanup = eMissionID
							MISSION_FLOW_RELEASE_TRIGGER_SCENE_ASSETS(eMissionID)
						ENDIF

					// Is this a shrink mission? If so we need to run extra narrative checks?
					ELIF IS_SP_MISSION_ID_A_SHRINK_MISSION(eMissionID)
						IF NOT SHRINK_IS_NARRATIVE_VALID_FOR_SESSION(GET_SHRINK_SESSION_ID_FOR_SP_MISSION_ID(eMissionID))
							CPRINTLN(DEBUG_FLOW, "DO_MISSION_AT_BLIP(", debugMissionIDString, "): Mission is a shrink mission and there is no valid narrative at the moment. Automatically failing.")
						
							//Remove the mission from the mission triggering system.
							REMOVE_MISSION_TRIGGER(missionTriggerID)
							//Unblock any friend connections associated with this mission as it has essentially failed.
							CLEAR_ALL_FRIEND_BLOCKS_FOR_MISSION(eMissionID)
							//Unregister the mission in the available mission system.
							UNREGISTER_MISSION_PREVIOUSLY_AVAILABLE(paramCoreVarsArrayPos)
							//Jump to failed label.
							RETURN INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue3) // Fail jump label.
						ENDIF
					ENDIF
					
					RETURN STAY_ON_THIS_COMMAND
				ENDIF
			ENDIF
			
			CPRINTLN(DEBUG_FLOW, "DO_MISSION_AT_BLIP(", debugMissionIDString, "): Mission trigger fired.")
			
			// Request the mission script and stat watcher.
			IF NOT IS_BIT_SET(g_sMissionStaticData[eMissionID].settingsBitset, MF_INDEX_NO_PASS)				
				CDEBUG3LN(DEBUG_FLOW, "DO_MISSION_AT_BLIP(", debugMissionIDString, "): Requesting stat watcher for GTAV mission as mission is about to launch.")
				REQUEST_SCRIPT_WITH_NAME_HASH( HASH("mission_stat_watcher") )				
#IF IS_DEBUG_BUILD
			ELSE
				CDEBUG3LN(DEBUG_FLOW, "DO_MISSION_AT_BLIP(", debugMissionIDString, "): Skipped requesting stat watcher as the mission is flagged to have no pass screen.")
#ENDIF	
			ENDIF
			
			REQUEST_SCRIPT_WITH_NAME_HASH(missionScriptHash)
			SET_BIT(g_availableMissions[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_LAUNCHING)
			
			//Wait for the main thread to be finished before allowing to launch.
			IF NOT g_isSPMainInitialised
				CPRINTLN(DEBUG_FLOW, "DO_MISSION_AT_BLIP(", debugMissionIDString, "): Waiting for the SP main thread to be initialised.")
				RETURN STAY_ON_THIS_COMMAND
			ENDIF

			// The mission is in the process of launching. 
			// Remove control and make the player invincible for the few frames it takes to start the mission.
			IF IS_PLAYER_PLAYING(PLAYER_ID())
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						VEHICLE_INDEX vehPlayer = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
						//Bring the vehicle out of critical state if it is in one.
						IF GET_ENTITY_HEALTH(vehPlayer) < 1
							SET_ENTITY_HEALTH(vehPlayer, 1)
						ENDIF
						IF GET_VEHICLE_ENGINE_HEALTH(vehPlayer) < 1
							SET_VEHICLE_ENGINE_HEALTH(vehPlayer, 1)
						ENDIF
						IF GET_VEHICLE_PETROL_TANK_HEALTH(vehPlayer) < 1
							SET_VEHICLE_PETROL_TANK_HEALTH(vehPlayer, 1)
						ENDIF
						
						//Bring the vehicle to a halt.
						IF NOT IS_BIT_SET(g_sMissionStaticData[eMissionID].settingsBitset, MF_INDEX_HAS_LEADIN)
						AND NOT IS_BIT_SET(g_sMissionStaticData[eMissionID].settingsBitset, MF_INDEX_NO_VEH_STOP)
						OR (eMissionID = SP_MISSION_EXILE_2 AND GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN)
							IF IS_VEHICLE_ON_ALL_WHEELS(vehPlayer)
								BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vehPlayer, 2.5, 2, 0.5, TRUE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			RETURN STAY_ON_THIS_COMMAND
		ENDIF

		//Launch the mission stat watcher
		IF eMissionID != SP_MISSION_FINALE_CREDITS
		AND eMissionID != SP_MISSION_FINALE_CREDITS
			IF NOT IS_BIT_SET(g_sMissionStaticData[eMissionID].settingsBitset, MF_INDEX_NO_PASS)
				
				IF NOT IS_BIT_SET(g_availableMissions[availableMissionIndex].bitflags, BITS_AVAILABLE_STAT_WATCHER_RUNNING)
					IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH( HASH("mission_stat_watcher") ) > 0
						CPRINTLN(DEBUG_FLOW, "DO_MISSION_AT_BLIP(", debugMissionIDString, "): Waiting for old stat watcher to terminate.")
						TERMINATE_STAT_WATCHER()
						RETURN STAY_ON_THIS_COMMAND // Wait until old watcher has terminated.
					ELIF HAS_SCRIPT_WITH_NAME_HASH_LOADED( HASH("mission_stat_watcher") )
						CPRINTLN(DEBUG_FLOW, "DO_MISSION_AT_BLIP(", debugMissionIDString, "): Starting new stat watcher thread for mission.")
						START_NEW_SCRIPT_WITH_NAME_HASH( HASH("mission_stat_watcher"), SPECIAL_ABILITY_STACK_SIZE)
					    SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED( HASH("mission_stat_watcher"))
						SET_BIT(g_availableMissions[availableMissionIndex].bitflags, BITS_AVAILABLE_STAT_WATCHER_RUNNING)
					ELSE
						CPRINTLN(DEBUG_FLOW, "DO_MISSION_AT_BLIP(", debugMissionIDString, "): Waiting for stat watcher thread to stream in.")
						RETURN STAY_ON_THIS_COMMAND // Waiting for script to load.
					ENDIF
				#IF IS_DEBUG_BUILD
				ELSE
					PRINTLN("<STAT WATCHER> <FLOW> We think the stat watcher is already running for this mission, not launching it")
				#ENDIF
				ENDIF				
			ENDIF
		ENDIF
		
		// Mission launching, so always requests the script again to make sure code knows script is interested in this script
		REQUEST_SCRIPT_WITH_NAME_HASH(missionScriptHash)
		
		// Has the script loaded?
		IF NOT HAS_SCRIPT_WITH_NAME_HASH_LOADED(missionScriptHash)
			CPRINTLN(DEBUG_FLOW, "DO_MISSION_AT_BLIP(", debugMissionIDString, "): Waiting for script to load.")
			RETURN STAY_ON_THIS_COMMAND
		ENDIF
		
		//Is a switch camera in progress.
		IF eMissionID != SP_MISSION_EXILE_2
		AND eMissionID != SP_HEIST_AGENCY_2
		AND eMissionID != SP_MISSION_FAMILY_4
			IF IS_PLAYER_SWITCH_IN_PROGRESS()
				SWITCH_TYPE eCamSwitchType = GET_PLAYER_SWITCH_TYPE()
				IF eCamSwitchType <> SWITCH_TYPE_SHORT
					SWITCH_STATE eCamSwitchState = GET_PLAYER_SWITCH_STATE() 		
					IF eCamSwitchState < SWITCH_STATE_JUMPCUT_DESCENT
						CDEBUG1LN(DEBUG_FLOW, "DO_MISSION_AT_BLIP(", debugMissionIDString, "): Waiting for switch camera to enter descent.")
						RETURN STAY_ON_THIS_COMMAND
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//Is an intro cutscene load request waiting to start?
		IF g_bFlowLoadIntroCutscene AND (NOT g_bFlowLoadRequestStarted)
			CDEBUG1LN(DEBUG_FLOW, "DO_MISSION_AT_BLIP(", debugMissionIDString, "): Waiting for mission intro cutscene to start loading.")
			RETURN STAY_ON_THIS_COMMAND
		ENDIF
		
		//Start getting out of a vehicle if we're in one.
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF NOT IS_BIT_SET(g_sMissionStaticData[eMissionID].settingsBitset, MF_INDEX_HAS_LEADIN)
					IF NOT IS_BIT_SET(g_sMissionStaticData[eMissionID].settingsBitset, MF_INDEX_NO_VEH_EXIT)
						IF NOT (eMissionID = SP_MISSION_EXILE_2 AND GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN)
							IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_VEHICLE) = CAM_VIEW_MODE_FIRST_PERSON
								IF 	GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) != PERFORMING_TASK
								AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) != WAITING_TO_START_TASK
									TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID(), 500)
								ENDIF
							ELSE
								TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID(), 0)
							ENDIF
							RETURN STAY_ON_THIS_COMMAND
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			//Play 400ms of the getting out of vehicle animation if we are leaving one.
			IF GET_IS_TASK_ACTIVE(PLAYER_PED_ID(), CODE_TASK_EXIT_VEHICLE)
				IF g_iTimeStartedExitingVehicle = -1
					g_iTimeStartedExitingVehicle = GET_GAME_TIMER()
					RETURN STAY_ON_THIS_COMMAND
				ELIF (GET_GAME_TIMER() - g_iTimeStartedExitingVehicle) < 400
					RETURN STAY_ON_THIS_COMMAND
				ENDIF
			ENDIF
		ENDIF

		//Is an intro cutscene load still ongoing?
		IF g_bFlowLoadingIntroCutscene
			//Fade the game out to encourage the cutscene to load.
			MANUALLY_ACTIVATE_CUTSCENE_FAILSAFE()
			
			//Have we made cutscene variation requests yet?
			IF NOT g_bFlowIntroVariationRequestsMade
				CDEBUG1LN(DEBUG_FLOW, "DO_MISSION_AT_BLIP(", debugMissionIDString, "): Waiting for cutscene requests to go through.")
				RETURN STAY_ON_THIS_COMMAND
			ENDIF
			CDEBUG1LN(DEBUG_FLOW, "DO_MISSION_AT_BLIP(", debugMissionIDString, "): Waiting for mission intro cutscene to finish loading.")
			RETURN STAY_ON_THIS_COMMAND
		ENDIF
		
		//Mission has loaded, so launch it
		g_availableMissions[availableMissionIndex].theThread = START_NEW_SCRIPT_WITH_NAME_HASH(missionScriptHash, MISSION_STACK_SIZE)
		SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(missionScriptHash)
		SET_BIT(g_availableMissions[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_RUNNING)

		//Run common mission starting routines.
		MISSION_PROCESSING_MISSION_STARTING(paramCoreVarsArrayPos, availableMissionIndex, eMissionID, paramStrandID)
		
		//Reset getting out of vehicle timer.
		g_iTimeStartedExitingVehicle = -1
		
		RETURN STAY_ON_THIS_COMMAND
	ENDIF

	// ---------------RUNNING MISSION ---------------------------------------------
	
	// Check if mission has failed so we can keep the mission thread running whilst we set up a replay
	BOOL hasFailed = IS_BIT_SET(g_availableMissions[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_FAILED)
	
	IF (IS_THREAD_ACTIVE(g_availableMissions[availableMissionIndex].theThread))
		IF hasFailed
			IF NOT IS_BIT_SET(g_sMissionStaticData[eMissionID].settingsBitset, MF_INDEX_NO_FAIL)
			AND g_bSkipFailScreen = FALSE
				// going to a replay screen, go on to termination section
				// mission script continues to run for a while during replay processing
				CPRINTLN(DEBUG_FLOW, "DO_MISSION_AT_BLIP(", debugMissionIDString, "): Failed and going to replay screen.")
			ELSE
				// not going to a replay screen, keep running
				RETURN STAY_ON_THIS_COMMAND
			ENDIF
		ELSE
			// not going to a replay screen, keep running
			RETURN STAY_ON_THIS_COMMAND
		ENDIF
	ENDIF

	// ----------MISSION TERMINATION -------------------------------------

	// ...get the Pass/Fail state for the mission
	BOOL hasPassed = IS_BIT_SET(g_availableMissions[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_PASSED)

	// ...ensure the mission hasn't both passed and failed.
	IF hasPassed AND hasFailed
		CPRINTLN(DEBUG_FLOW, "DO_MISSION_AT_BLIP(", debugMissionIDString, "): Mission has both PASSED and FAILED. Treating as PASSED.")
		SCRIPT_ASSERT("PERFORM_DO_MISSION_AT_BLIP: Mission has both PASSED and FAILED. Treating as PASSED.")
		hasFailed = FALSE
	ENDIF
	
	// ...ensure the mission has either passed or failed.
	IF NOT (hasPassed OR hasFailed)
		CPRINTLN(DEBUG_FLOW, "DO_MISSION_AT_BLIP(", debugMissionIDString, "): Mission has not PASSED or FAILED. Treating as FAILED.")
		SCRIPT_ASSERT("PERFORM_DO_MISSION_AT_BLIP: : Mission has not PASSED or FAILED. Treating as FAILED.")
		#IF IS_DEBUG_BUILD
			// overwrite the fail reason, so mission scripters know the mission has failed due to an array overrun or similar
			g_txtMissionFailReason = "FLOW_FAIL"
		#ENDIF
		hasFailed = TRUE
	ENDIF
	
	//----------------MISSION PASSED--------------------------------
	IF (hasPassed)
		RETURN MISSION_PROCESSING_MISSION_PASSED(paramCoreVarsArrayPos, availableMissionIndex, eMissionID, TRUE, IS_BIT_SET(g_availableMissions[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_BACKGROUND_PASS))
	ENDIF
	
	//----------------MISSION FAILED / REPLAY SETUP ------------------------
	RETURN MISSION_PROCESSING_MISSION_FAILED(paramCoreVarsArrayPos, availableMissionIndex, eMissionID, TRUE)
#endif
#endif
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Sets up and maintains a mission that gets launched when switching to a player character.
//			Deals with the mission ending and handles either pass or fail.
//
// INPUT PARAMS:			paramStrandID				StrandID that launched this mission
//							paramCoreVarsArrayPos		Index into the Core Variables array
// RETURN VALUE:			JUMP_LABEL_IDS				A jump label - should be STAY_ON_THIS_COMMAND until mission termination
//
// NOTES:	Registers a SWITCH trigger with the Mission Triggering System.

FUNC JUMP_LABEL_IDS PERFORM_DO_MISSION_AT_SWITCH(STRANDS paramStrandID, INT paramCoreVarsArrayPos)
	// --------INIT --------------------------------------------------------
	// Ensure the Script Vars Array position is legal
	IF (paramCoreVarsArrayPos = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_DO_MISSION_AT_SWITCH: The Script Variables Array Position parameter is illegal. MOVING ON TO NEXT COMMAND.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF

	// Retrieve the details for this mission from the 'available missions' array
	INT availableMissionIndex = GET_AVAILABLE_MISSION_INDEX_FOR_STORED_MISSION(paramCoreVarsArrayPos)
	SP_MISSIONS eMissionID	= INT_TO_ENUM(SP_MISSIONS, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue1)
	INT missionScriptHash = g_sMissionStaticData[eMissionID].scriptHash
	
	#IF IS_DEBUG_BUILD
		STRING debugMissionIDString = GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMissionID)
	#ENDIF
	
	// --------REPLAY SETUP / REJECTION --------------------------------------
	// If a replay is in the process of being setup, wait here
	IF IS_REPLAY_BEING_PROCESSED()
		CDEBUG3LN(DEBUG_REPLAY, "Replay is being processed, pausing perform do mission at switch.")
		RETURN STAY_ON_THIS_COMMAND
	ELSE
		// If replay has been rejected, clean up the trigger and release candidate ID and exit
		// (this will make IS_REPLAY_BEING_PROCESSED return false, so other missions can try to launch)
		IF g_replay.replayCoreVarsIndex = paramCoreVarsArrayPos
			IF g_replay.replayStageID = RS_REJECTED
				// Replay rejected, call mission over routine
				MISSION_PROCESSING_MISSION_OVER(eMissionID)
				CPRINTLN(DEBUG_REPLAY, "Replay has been rejected, cleaning up mission.")
				MISSION_PROCESSING_RELEASE_MISSION(paramCoreVarsArrayPos, availableMissionIndex, TRUE)
				CPRINTLN(DEBUG_FLOW, "DO_MISSION_AT_SWITCH(", debugMissionIDString, "):  Failed after replay rejection.")
				RETURN INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue3) // Fail jump label.
			ENDIF
		ENDIF
	ENDIF
	
	// --------REGISTER MISSION ------------------------------------------------
	// If the mission is not already 'available' then register the mission with the 'available missions' array
	// NOTE: A strand that reaches a 'do mission' command registers the mission as available and stores a
	//			pointer to the mission's details together with some control variables for triggering the mission.
	IF (availableMissionIndex = ILLEGAL_ARRAY_POSITION)
		CDEBUG3LN(DEBUG_REPLAY, "availableMissionIndex = ILLEGAL_ARRAY_POSITION")
		
		// --------SWITCH CHARACTER AVAILABILITY  ---------------------------------
		IF NOT g_bSceneAutoTrigger
			INT iCharIndex
			REPEAT 3 iCharIndex
				IF IS_BIT_SET(g_sMissionStaticData[eMissionID].triggerCharBitset, iCharIndex)
					//Before registering the mission as triggerable, check that the player isn't already the switchable character.
					IF GET_CURRENT_PLAYER_PED_INT() = iCharIndex
						#IF IS_DEBUG_BUILD
							IF (GET_GAME_TIMER() % 5000) < 200 //Stop the debug message spamming too much. Print every 5 seconds.
								CPRINTLN(DEBUG_FLOW, "DO_MISSION_AT_SWITCH(", debugMissionIDString, "): Waiting until player isn't the switch character.")
							ENDIF
						#ENDIF
						RETURN STAY_ON_THIS_COMMAND
					ENDIF
					//Before registering the mission as triggerable, check if the switch character still exists in the world.
					SELECTOR_SLOTS_ENUM eSlot = GET_SELECTOR_SLOT_FROM_PLAYER_PED_ENUM(INT_TO_ENUM(enumCharacterList, iCharIndex))
					IF NOT IS_PED_INJURED(g_sPlayerPedRequest.sSelectorPeds.pedID[eSlot])
						#IF IS_DEBUG_BUILD
							IF (GET_GAME_TIMER() % 5000) < 200 //Stop the debug message spamming too much. Print every 5 seconds.
								CPRINTLN(DEBUG_FLOW, "DO_MISSION_AT_SWITCH(", debugMissionIDString, "): Waiting until switch character doesn't exists as a ped in the world.")
							ENDIF
						#ENDIF
						RETURN STAY_ON_THIS_COMMAND
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
		
		availableMissionIndex = REGISTER_AVAILABLE_MISSION(paramCoreVarsArrayPos)
		
		// If the registration failed then exit
		IF (availableMissionIndex = ILLEGAL_ARRAY_POSITION)
			RETURN STAY_ON_THIS_COMMAND
		ENDIF
		
		#IF IS_DEBUG_BUILD
			// As an extra safety check, on successful registration ensure that the rest of the struct details are empty
#IF USE_CLF_DLC
			IF NOT (g_availableMissionsTU[availableMissionIndex].missionTriggerID = NO_CANDIDATE_ID)
#ENDIF
#IF USE_NRM_DLC
			IF NOT (g_availableMissionsTU[availableMissionIndex].missionTriggerID = NO_CANDIDATE_ID)
#ENDIF
#IF NOT USE_CLF_DLC
#IF NOT USE_NRM_DLC
			IF NOT (g_availableMissions[availableMissionIndex].missionTriggerID = NO_CANDIDATE_ID)
#ENDIF
#ENDIF
CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_DO_MISSION_AT_SWITCH: Successfully registered in the Available Missions array but there seems to be existing data in the new array slot.")
			ENDIF
		#ENDIF
	ENDIF
	
	// Register this mission with the Mission Triggering System if it has not already been registered
#IF USE_CLF_DLC
	INT	missionTriggerID = g_availableMissionsTU[availableMissionIndex].missionTriggerID
	// Only register the mission if it isn't being replayed
	IF NOT (IS_BIT_SET(g_availableMissionsTU[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_BEING_REPLAYED))
#ENDIF
#IF USE_NRM_DLC
	INT	missionTriggerID = g_availableMissionsTU[availableMissionIndex].missionTriggerID
	// Only register the mission if it isn't being replayed
	IF NOT (IS_BIT_SET(g_availableMissionsTU[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_BEING_REPLAYED))
#ENDIF
#IF NOT USE_CLF_DLC
#IF NOT USE_NRM_DLC
	INT	missionTriggerID = g_availableMissions[availableMissionIndex].missionTriggerID
	// Only register the mission if it isn't being replayed
	IF NOT (IS_BIT_SET(g_availableMissions[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_BEING_REPLAYED))
#ENDIF
#ENDIF
IF (missionTriggerID = NO_CANDIDATE_ID)
		
			//Before registering the mission as triggerable, check if the player first needs to leave the trigger area.
			IF g_sMissionActiveData[eMissionID].leaveArea
				#IF IS_DEBUG_BUILD
					IF (GET_GAME_TIMER() % 5000) < 75 //Stop the debug message spamming too much. Print every 5 seconds.
						CPRINTLN(DEBUG_FLOW, "DO_MISSION_AT_SWITCH(", debugMissionIDString, "): Waiting for player to leave area before setting up mission trigger.")
					ENDIF
				#ENDIF
				RETURN STAY_ON_THIS_COMMAND
			ENDIF

			// ...not registered with Mission Triggering System, so register this mission.
			CPRINTLN(DEBUG_FLOW, "DO_MISSION_AT_SWITCH(", debugMissionIDString, "): Registering new trigger for mission.")
			
			IF MISSION_CANNOT_BE_REGISTERED_BECAUSE_OF_ACTIVE_FRIEND(eMissionID)
				CPRINTLN(DEBUG_FLOW, "DO_MISSION_AT_SWITCH(", debugMissionIDString, "): Won't be registered as triggerable as player has activity organised with mission character.")
				RETURN STAY_ON_THIS_COMMAND
			ENDIF

			REGISTER_SWITCH_MISSION_TRIGGER(missionTriggerID, eMissionID, paramStrandID, MCTID_MUST_LAUNCH, g_sMissionStaticData[eMissionID].triggerCharBitset)

			// ...check for registration problem
			IF (missionTriggerID = NO_CANDIDATE_ID)
				// ...mission failed to register with the Mission Triggering System
				CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_DO_MISSION_AT_SWITCH: Failed to register mission with Mission Triggering System.")
				RETURN STAY_ON_THIS_COMMAND
			ENDIF
			
			// Block a friend from being contacted while on a mission with that friend.
			SET_ALL_FRIEND_BLOCKS_FOR_MISSION(eMissionID)
			
			// Store the retrieved Mission Trigger ID in the 'available missions' array
#IF USE_CLF_DLC
			g_availableMissionsTU[availableMissionIndex].missionTriggerID = missionTriggerID
#ENDIF
#IF USE_NRM_DLC
			g_availableMissionsTU[availableMissionIndex].missionTriggerID = missionTriggerID
#ENDIF
#IF NOT USE_CLF_DLC
#IF NOT USE_NRM_DLC
			g_availableMissions[availableMissionIndex].missionTriggerID = missionTriggerID
#ENDIF
#ENDIF
ENDIF
	ENDIF
	
	// ----------------MISSION LAUNCH -----------------------------------------------
	// Check if the mission is running
#IF USE_CLF_DLC
	IF NOT (IS_BIT_SET(g_availableMissionsTU[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_RUNNING))
	
		// Check if the mission is launching
		IF NOT (IS_BIT_SET(g_availableMissionsTU[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_LAUNCHING))
			// ...no, the mission is not yet launching, so check if it is being replayed and therefore should launch immediately
			IF NOT (IS_BIT_SET(g_availableMissionsTU[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_BEING_REPLAYED))
#ENDIF
#IF USE_NRM_DLC
	IF NOT (IS_BIT_SET(g_availableMissionsTU[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_RUNNING))
	
		// Check if the mission is launching
		IF NOT (IS_BIT_SET(g_availableMissionsTU[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_LAUNCHING))
			// ...no, the mission is not yet launching, so check if it is being replayed and therefore should launch immediately
			IF NOT (IS_BIT_SET(g_availableMissionsTU[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_BEING_REPLAYED))
#ENDIF
#IF NOT USE_CLF_DLC
#IF NOT USE_NRM_DLC
	IF NOT (IS_BIT_SET(g_availableMissions[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_RUNNING))
	
		// Check if the mission is launching
		IF NOT (IS_BIT_SET(g_availableMissions[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_LAUNCHING))
			// ...no, the mission is not yet launching, so check if it is being replayed and therefore should launch immediately
			IF NOT (IS_BIT_SET(g_availableMissions[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_BEING_REPLAYED))
#ENDIF
#ENDIF
// ...no, not being replayed, so check if the Mission Triggering System allows the mission to trigger
				IF NOT (HAS_MISSION_TRIGGER_TRIGGERED(missionTriggerID))
					#if USE_CLF_DLC
						// Has this mission already been flagged as passed?
						IF g_savedGlobalsClifford.sFlow.missionSavedData[eMissionID].completed
							CPRINTLN(DEBUG_FLOW, "DO_MISSION_AT_SWITCH(", debugMissionIDString, "): Mission already flagged as passed. Automatically passing.")
							SET_BIT(g_availableMissionsTU[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_RUNNING)
							SET_BIT(g_availableMissionsTU[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_PASSED)
						ENDIF
					#endif
					#if USE_NRM_DLC
						// Has this mission already been flagged as passed?
						IF g_savedGlobalsnorman.sFlow.missionSavedData[eMissionID].completed
							CPRINTLN(DEBUG_FLOW, "DO_MISSION_AT_SWITCH(", debugMissionIDString, "): Mission already flagged as passed. Automatically passing.")
							SET_BIT(g_availableMissionsTU[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_RUNNING)
							SET_BIT(g_availableMissionsTU[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_PASSED)
						ENDIF
					#endif
					#if not USE_CLF_DLC
					#if not USE_NRM_DLC
						// Has this mission already been flagged as passed?
						IF g_savedGlobals.sFlow.missionSavedData[eMissionID].completed
							CPRINTLN(DEBUG_FLOW, "DO_MISSION_AT_SWITCH(", debugMissionIDString, "): Mission already flagged as passed. Automatically passing.")
							SET_BIT(g_availableMissions[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_RUNNING)
							SET_BIT(g_availableMissions[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_PASSED)
						ENDIF
					#endif
					#endif
					RETURN STAY_ON_THIS_COMMAND
				ENDIF
			ENDIF
			
			// NOTE: Use PRINTSTRINGS for these so that this message always appears in the log
			CPRINTLN(DEBUG_FLOW, "DO_MISSION_AT_SWITCH(", debugMissionIDString, "): Mission trigger fired.")
			
			// Request the mission script and stat watcher.
			IF NOT IS_BIT_SET(g_sMissionStaticData[eMissionID].settingsBitset, MF_INDEX_NO_PASS)
				#if USE_CLF_DLC
					CDEBUG3LN(DEBUG_FLOW, "DO_MISSION_AT_SWITCH(", debugMissionIDString, "): Requesting stat watcher for CLF mission as mission is about to launch.")
					REQUEST_SCRIPT_WITH_NAME_HASH( HASH("mission_stat_watchCLF") )
				#endif	
				
				#if USE_NRM_DLC
					CDEBUG3LN(DEBUG_FLOW, "DO_MISSION_AT_SWITCH(", debugMissionIDString, "): Requesting stat watcher for NRM mission as mission is about to launch.")
					REQUEST_SCRIPT_WITH_NAME_HASH( HASH("mission_stat_watchNRM") )
				#endif	
				
				#if not USE_CLF_DLC
				#if not USE_NRM_DLC
					CDEBUG3LN(DEBUG_FLOW, "DO_MISSION_AT_SWITCH(", debugMissionIDString, "): Requesting stat watcher for GTAV mission as mission is about to launch.")
					REQUEST_SCRIPT_WITH_NAME_HASH( HASH("mission_stat_watcher") )
				#endif
				#endif
#IF IS_DEBUG_BUILD
			ELSE
				CDEBUG3LN(DEBUG_FLOW, "DO_MISSION_AT_SWITCH(", debugMissionIDString, "): Skipped requesting stat watcher as the mission is flagged to have no pass screen.")
#ENDIF	
			ENDIF
			
			REQUEST_SCRIPT_WITH_NAME_HASH(missionScriptHash)
#IF USE_CLF_DLC
			SET_BIT(g_availableMissionsTU[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_LAUNCHING)
#ENDIF
#IF USE_NRM_DLC
			SET_BIT(g_availableMissionsTU[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_LAUNCHING)
#ENDIF
#IF NOT USE_CLF_DLC
#IF NOT USE_NRM_DLC
			SET_BIT(g_availableMissions[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_LAUNCHING)
#ENDIF
#ENDIF

			// The mission is in the process of launching. 
			// Remove control and stop car.
			IF IS_PLAYER_PLAYING(PLAYER_ID())
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						VEHICLE_INDEX vehPlayer = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
						
						//Bring the vehicle out of critical state if it is in one.
						IF GET_ENTITY_HEALTH(vehPlayer) < 1
							SET_ENTITY_HEALTH(vehPlayer, 1)
						ENDIF
						IF GET_VEHICLE_ENGINE_HEALTH(vehPlayer) < 1
							SET_VEHICLE_ENGINE_HEALTH(vehPlayer, 1)
						ENDIF
						IF GET_VEHICLE_PETROL_TANK_HEALTH(vehPlayer) < 1
							SET_VEHICLE_PETROL_TANK_HEALTH(vehPlayer, 1)
						ENDIF
						
						//Bring the vehicle to a halt.
						BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vehPlayer, 2.5, 2, 0.5, TRUE)
					ENDIF
				ENDIF
			ENDIF
			
			RETURN STAY_ON_THIS_COMMAND
		ENDIF
		
		//Launch the mission stat watcher
		#if not USE_CLF_DLC
		#if not USE_NRM_DLC
		IF eMissionID != SP_MISSION_FINALE_CREDITS
		#endif 
		#endif
			IF NOT IS_BIT_SET(g_sMissionStaticData[eMissionID].settingsBitset, MF_INDEX_NO_PASS)	
				IF !g_bMissionStatSystemBlocker
#IF USE_CLF_DLC
					IF NOT IS_BIT_SET(g_availableMissionsTU[availableMissionIndex].bitflags, BITS_AVAILABLE_STAT_WATCHER_RUNNING)	
#ENDIF
#IF USE_NRM_DLC
					IF NOT IS_BIT_SET(g_availableMissionsTU[availableMissionIndex].bitflags, BITS_AVAILABLE_STAT_WATCHER_RUNNING)	
#ENDIF
#IF NOT USE_CLF_DLC
#IF NOT USE_NRM_DLC
					IF NOT IS_BIT_SET(g_availableMissions[availableMissionIndex].bitflags, BITS_AVAILABLE_STAT_WATCHER_RUNNING)	
#ENDIF
#ENDIF

						#if USE_CLF_DLC				
							IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH( HASH("mission_stat_watchCLF") ) > 0
								TERMINATE_STAT_WATCHER()
								CPRINTLN(DEBUG_FLOW, "DO_MISSION_AT_SWITCH(", debugMissionIDString, "): Waiting for old stat watcher to terminate.")
								RETURN STAY_ON_THIS_COMMAND // Wait until old watcher has terminated.
							ELIF HAS_SCRIPT_WITH_NAME_HASH_LOADED( HASH("mission_stat_watchCLF") )
								CPRINTLN(DEBUG_FLOW, "DO_MISSION_AT_SWITCH(", debugMissionIDString, "): Starting new stat watcher thread for mission.")
								START_NEW_SCRIPT_WITH_NAME_HASH( HASH("mission_stat_watchCLF"), DEFAULT_STACK_SIZE)
							    SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED( HASH("mission_stat_watchCLF") )
								SET_BIT(g_availableMissionsTU[availableMissionIndex].bitflags, BITS_AVAILABLE_STAT_WATCHER_RUNNING)
							ELSE
								CPRINTLN(DEBUG_FLOW, "DO_MISSION_AT_SWITCH(", debugMissionIDString, "): Waiting for stat watcher thread to stream in.")
								RETURN STAY_ON_THIS_COMMAND // Waiting for script to load.
							ENDIF					
						#endif
						
						#if USE_NRM_DLC
							IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH( HASH("mission_stat_watchNRM") ) > 0
								TERMINATE_STAT_WATCHER()
								CPRINTLN(DEBUG_FLOW, "DO_MISSION_AT_SWITCH(", debugMissionIDString, "): Waiting for old stat watcher to terminate.")
								RETURN STAY_ON_THIS_COMMAND // Wait until old watcher has terminated.
							ELIF HAS_SCRIPT_WITH_NAME_HASH_LOADED( HASH("mission_stat_watchNRM") )
								CPRINTLN(DEBUG_FLOW, "DO_MISSION_AT_SWITCH(", debugMissionIDString, "): Starting new stat watcher thread for mission.")
								START_NEW_SCRIPT_WITH_NAME_HASH( HASH("mission_stat_watchNRM"), DEFAULT_STACK_SIZE)
							    SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED( HASH("mission_stat_watchNRM") )
								SET_BIT(g_availableMissionsTU[availableMissionIndex].bitflags, BITS_AVAILABLE_STAT_WATCHER_RUNNING)
							ELSE
								CPRINTLN(DEBUG_FLOW, "DO_MISSION_AT_SWITCH(", debugMissionIDString, "): Waiting for stat watcher thread to stream in.")
								RETURN STAY_ON_THIS_COMMAND // Waiting for script to load.
							ENDIF
						#endif
						
						#if not USE_CLF_DLC
						#if not USE_NRM_DLC
							IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH( HASH("mission_stat_watcher") ) > 0
								TERMINATE_STAT_WATCHER()
								CPRINTLN(DEBUG_FLOW, "DO_MISSION_AT_SWITCH(", debugMissionIDString, "): Waiting for old stat watcher to terminate.")
								RETURN STAY_ON_THIS_COMMAND // Wait until old watcher has terminated.
							ELIF HAS_SCRIPT_WITH_NAME_HASH_LOADED( HASH("mission_stat_watcher") )
								CPRINTLN(DEBUG_FLOW, "DO_MISSION_AT_SWITCH(", debugMissionIDString, "): Starting new stat watcher thread for mission.")
								START_NEW_SCRIPT_WITH_NAME_HASH( HASH("mission_stat_watcher"), SPECIAL_ABILITY_STACK_SIZE)
							    SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED( HASH("mission_stat_watcher") )
								SET_BIT(g_availableMissions[availableMissionIndex].bitflags, BITS_AVAILABLE_STAT_WATCHER_RUNNING)
							ELSE
								CPRINTLN(DEBUG_FLOW, "DO_MISSION_AT_SWITCH(", debugMissionIDString, "): Waiting for stat watcher thread to stream in.")
								RETURN STAY_ON_THIS_COMMAND // Waiting for script to load.
							ENDIF
						#endif
						#endif
						
					ENDIF
				ENDIF
			ENDIF
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC	
		ENDIF
	#endif
	#endif
		// Mission launching, so always requests the script again to make sure code knows script is interested in this script
		REQUEST_SCRIPT_WITH_NAME_HASH(missionScriptHash)
		
		// Has the script loaded?
		IF NOT HAS_SCRIPT_WITH_NAME_HASH_LOADED(missionScriptHash)
			CPRINTLN(DEBUG_FLOW, "DO_MISSION_AT_SWITCH(", debugMissionIDString, "): Waiting for script to load.")
			RETURN STAY_ON_THIS_COMMAND
		ENDIF
		
		// Mission has loaded, so launch it
#IF USE_CLF_DLC
		g_availableMissionsTU[availableMissionIndex].theThread = START_NEW_SCRIPT_WITH_NAME_HASH(missionScriptHash, MISSION_STACK_SIZE)
		SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(missionScriptHash)
		SET_BIT(g_availableMissionsTU[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_RUNNING)
#ENDIF
#IF USE_NRM_DLC
		g_availableMissionsTU[availableMissionIndex].theThread = START_NEW_SCRIPT_WITH_NAME_HASH(missionScriptHash, MISSION_STACK_SIZE)
		SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(missionScriptHash)
		SET_BIT(g_availableMissionsTU[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_RUNNING)
#ENDIF
#IF NOT USE_CLF_DLC
#IF NOT USE_NRM_DLC
		g_availableMissions[availableMissionIndex].theThread = START_NEW_SCRIPT_WITH_NAME_HASH(missionScriptHash, MISSION_STACK_SIZE)
		SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(missionScriptHash)
		SET_BIT(g_availableMissions[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_RUNNING)
#ENDIF
#ENDIF

		//Start getting out of a vehicle if we're in one.
		IF NOT DOES_MISSION_HAVE_TRIGGER_SCENE(eMissionID)
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID(), 0, ECF_DONT_WAIT_FOR_VEHICLE_TO_STOP)
				ENDIF
			ENDIF
		ENDIF
		
		// Run common mission starting routines.
		MISSION_PROCESSING_MISSION_STARTING(paramCoreVarsArrayPos, availableMissionIndex, eMissionID, paramStrandID)
		RETURN STAY_ON_THIS_COMMAND
	ENDIF

	// ---------------RUNNING MISSION ---------------------------------------------
	// Check if mission has failed so we can keep the mission thread running whilst we set up a replay
#IF USE_CLF_DLC
	BOOL hasFailed = IS_BIT_SET(g_availableMissionsTU[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_FAILED)
	IF (IS_THREAD_ACTIVE(g_availableMissionsTU[availableMissionIndex].theThread))
#ENDIF
#IF USE_NRM_DLC
	BOOL hasFailed = IS_BIT_SET(g_availableMissionsTU[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_FAILED)
	IF (IS_THREAD_ACTIVE(g_availableMissionsTU[availableMissionIndex].theThread))
#ENDIF
#IF NOT USE_CLF_DLC
#IF NOT USE_NRM_DLC
	BOOL hasFailed = IS_BIT_SET(g_availableMissions[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_FAILED)
	IF (IS_THREAD_ACTIVE(g_availableMissions[availableMissionIndex].theThread))
#ENDIF
#ENDIF
IF hasFailed
			IF NOT IS_BIT_SET(g_sMissionStaticData[eMissionID].settingsBitset, MF_INDEX_NO_FAIL)
			AND g_bSkipFailScreen = FALSE
				// going to a replay screen, go on to termination section
				// mission script continues to run for a while during replay processing
				CPRINTLN(DEBUG_FLOW, "DO_MISSION_AT_SWITCH(", debugMissionIDString, "): Failed and going to replay screen.")
			ELSE
				// not going to a replay screen, keep running
				RETURN STAY_ON_THIS_COMMAND
			ENDIF
		ELSE
			// not going to a replay screen, keep running
			RETURN STAY_ON_THIS_COMMAND
		ENDIF
	ENDIF

	// ----------MISSION TERMINATION -------------------------------------
	// ...get the Pass/Fail state for the mission
#IF USE_CLF_DLC
	BOOL hasPassed = IS_BIT_SET(g_availableMissionsTU[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_PASSED)
#ENDIF
#IF USE_NRM_DLC
	BOOL hasPassed = IS_BIT_SET(g_availableMissionsTU[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_PASSED)
#ENDIF
#IF NOT USE_CLF_DLC
#IF NOT USE_NRM_DLC
	BOOL hasPassed = IS_BIT_SET(g_availableMissions[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_PASSED)
#ENDIF
#ENDIF

	// ...ensure the mission hasn't both passed and failed.
	IF hasPassed AND hasFailed
		CPRINTLN(DEBUG_FLOW, "DO_MISSION_AT_SWITCH(", debugMissionIDString, "): Mission has both PASSED or FAILED. Treating as PASSED.")
		SCRIPT_ASSERT("PERFORM_DO_MISSION_AT_SWITCH: : Mission has both PASSED or FAILED. Treating as PASSED.")
		hasFailed = FALSE
	ENDIF
	
	// ...ensure the mission has either passed or failed.
	IF NOT (hasPassed OR hasFailed)
		CPRINTLN(DEBUG_FLOW, "DO_MISSION_AT_SWITCH(", debugMissionIDString, "): Mission has not PASSED or FAILED. Treating as FAILED.")
		SCRIPT_ASSERT("PERFORM_DO_MISSION_AT_SWITCH: : Mission has not PASSED or FAILED. Treating as FAILED.")
		#IF IS_DEBUG_BUILD
			// overwrite the fail reason, so mission scripters know the mission has failed due to an array overrun or similar
			g_txtMissionFailReason = "FLOW_FAIL"
		#ENDIF
		hasFailed = TRUE
	ENDIF
	
	//----------------MISSION PASSED--------------------------------
	IF (hasPassed)
		RETURN MISSION_PROCESSING_MISSION_PASSED(paramCoreVarsArrayPos, availableMissionIndex, eMissionID, TRUE)
	ENDIF
	
	//----------------MISSION FAILED / REPLAY SETUP ------------------------
	RETURN MISSION_PROCESSING_MISSION_FAILED(paramCoreVarsArrayPos, availableMissionIndex, eMissionID, TRUE)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Sets up and maintains a mission that gets launched immediately.
//			Deals with the mission ending and handles either pass or fail.
//
// INPUT PARAMS:			paramCoreVarsArrayPos		Index into the Core Variables array
//							paramStrandID				StrandID that launched this mission.			
// RETURN VALUE:			JUMP_LABEL_IDS				A jump label - should be STAY_ON_THIS_COMMAND until mission termination
//
// NOTES:	Communicates directly with the Mission Candidate System rather than using the Mission Triggering System.
#if USE_CLF_DLC
FUNC JUMP_LABEL_IDS PERFORM_DO_MISSION_NOWCLF(INT paramCoreVarsArrayPos, STRANDS paramStrandID = STRAND_NONE)

	// --------INIT --------------------------------------------------------
	// Ensure the Script Vars Array position is legal
	IF (paramCoreVarsArrayPos = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_DO_MISSION_NOW: The Script Variables Array Position parameter is illegal. MOVING ON TO NEXT COMMAND.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Retrieve the details for this mission from the 'available missions' array
	INT availableMissionIndex = GET_AVAILABLE_MISSION_INDEX_FOR_STORED_MISSION(paramCoreVarsArrayPos)
	SP_MISSIONS eMissionID	= INT_TO_ENUM(SP_MISSIONS, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue1)
	INT missionScriptHash = g_sMissionStaticData[eMissionID].scriptHash
	
	#IF IS_DEBUG_BUILD
		STRING debugMissionIDString = GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMissionID)
	#ENDIF
	
	IF NOT IS_BIT_SET(g_sMissionStaticData[eMissionID].settingsBitset, MF_INDEX_NO_PASS)
		
			CDEBUG3LN(DEBUG_FLOW, "DO_MISSION_NOW(", debugMissionIDString, "): Requesting stat watcher for CLF mission.")
			REQUEST_SCRIPT_WITH_NAME_HASH( HASH("mission_stat_watchCLF") )
		
#IF IS_DEBUG_BUILD
	ELSE
		CDEBUG3LN(DEBUG_FLOW, "DO_MISSION_NOW(", debugMissionIDString, "): Skipped requesting stat watcher as the mission is flagged to have no pass screen.")
#ENDIF	
	ENDIF
	
	REQUEST_SCRIPT_WITH_NAME_HASH(missionScriptHash)
	
	// --------REPLAY SETUP / REJECTION --------------------------------------
	// If a replay is in the process of being setup, wait here
	IF IS_REPLAY_BEING_PROCESSED()
		//CPRINTLN(DEBUG_REPLAY, "Replay is being processed, pausing PERFORM_DO_MISSION_NOW.")
		RETURN STAY_ON_THIS_COMMAND
	ELSE
		// If replay has been rejected, clean up and exit
		// (this will make IS_REPLAY_BEING_PROCESSED return false, so other missions can try to launch)
		IF g_replay.replayCoreVarsIndex = paramCoreVarsArrayPos
			IF g_replay.replayStageID =   RS_REJECTED
				// Replay rejected, call mission over routine
				MISSION_PROCESSING_MISSION_OVER(eMissionID)
				CPRINTLN(DEBUG_REPLAY, "Replay has been rejected, cleaning up mission.")
				MISSION_PROCESSING_RELEASE_MISSION(paramCoreVarsArrayPos, availableMissionIndex, FALSE)
				CPRINTLN(DEBUG_FLOW, "DO_MISSION_NOW(", debugMissionIDString, "):  Failed after replay rejection.")
				RETURN INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue3) // Fail jump label.
			ENDIF
		ENDIF
	ENDIF
	
	// Check if the player is alive before allowing the mission to trigger.
	// Do mission now doesn't use the trigger system so we need to check for this explicitly here.
	// AndyM- Added check that mission isn't failed to fix problem with dying as different character
	// to the one that triggered the mission, during a fail fade.
	IF IS_ENTITY_DEAD(PLAYER_PED_ID())
		IF availableMissionIndex = ILLEGAL_ARRAY_POSITION // mission not registered
		OR NOT IS_BIT_SET(g_availableMissionsTU[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_FAILED) // mission not failed
			//CPRINTLN(DEBUG_REPLAY, "In the entity dead check!")
			RETURN STAY_ON_THIS_COMMAND
		ENDIF
	ENDIF
	
	// Check that the player character is a valid SP character.
	IF NOT IS_PLAYER_PED_PLAYABLE(GET_CURRENT_PLAYER_PED_ENUM())
		CDEBUG1LN(DEBUG_FLOW,  "DO_MISSION_NOW(", debugMissionIDString, "): Current player character isn't a singleplayer character.")
		RETURN STAY_ON_THIS_COMMAND
	ENDIF
	
	// --------REGISTER MISSION ------------------------------------------------
	// If the mission is not already 'available' then register the mission with the 'available missions' array
	// NOTE: A strand that reaches a 'do mission' command registers the mission as available and stores a
	//			pointer to the mission's details together with some control variables for triggering the mission.
	IF (availableMissionIndex = ILLEGAL_ARRAY_POSITION)
		availableMissionIndex = REGISTER_AVAILABLE_MISSION(paramCoreVarsArrayPos)
		
		// If the registration failed then exit
		IF (availableMissionIndex = ILLEGAL_ARRAY_POSITION)
			RETURN STAY_ON_THIS_COMMAND
		ENDIF
		
		#IF IS_DEBUG_BUILD
			// As an extra safety check, on successful registration ensure that the rest of the struct details are empty
			IF NOT (g_availableMissionsTU[availableMissionIndex].missionCandidateID = NO_CANDIDATE_ID)
				CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_DO_MISSION_NOW: Successfully registered in the Available Missions array but there seems to be existing data in the new array slot.")
			ENDIF
		#ENDIF
	ENDIF
	
	INT missionCandidateID 	= g_availableMissionsTU[availableMissionIndex].missionCandidateID

	IF MISSION_CANNOT_BE_REGISTERED_BECAUSE_OF_ACTIVE_FRIEND(eMissionID)
		CPRINTLN(DEBUG_FLOW, "DO_MISSION_NOW(", debugMissionIDString, "): Won't be registered as triggerable as player has activity organised with mission character.")
		RETURN STAY_ON_THIS_COMMAND
	ENDIF
	
	// ----------------MISSION LAUNCH -----------------------------------------------
	IF NOT (IS_BIT_SET(g_availableMissionsTU[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_RUNNING))
		// ...no, the mission is not yet running, so check if it is launching
		IF NOT (IS_BIT_SET(g_availableMissionsTU[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_LAUNCHING))
			// ...no, the mission is not yet launching, so immediately launch if the mission is being replayed
			IF NOT (IS_BIT_SET(g_availableMissionsTU[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_BEING_REPLAYED))
				// Set candidate priority
				m_enumMissionCandidateTypeID eCandidatePriority
			
				IF IS_REPEAT_PLAY_ACTIVE()
				OR IS_BIT_SET(g_sMissionStaticData[eMissionID].settingsBitset, MF_INDEX_MUST_LAUNCH_PRIORITY)
					// In repeat play we force the priority so it always launches.
					// Nothing should contest this anyway but best to be safe.
					eCandidatePriority = MCTID_MUST_LAUNCH
				ELSE 
					//None repeat play or flagged to use must launch. Assume this is a phonecall priority trigger.
					eCandidatePriority = INT_TO_ENUM(m_enumMissionCandidateTypeID, MCTID_PHONECALL_TO_PLAYER)
				ENDIF
				
				// ...mission is not being replayed so still needs permission from the Mission Candidate System to launch
				//If the launch priority is phonecall the comms controller should have secured a candidate ID ahead of time.
				m_enumMissionCandidateReturnValue allowLaunch
				IF eCandidatePriority = MCTID_PHONECALL_TO_PLAYER
					IF g_iCommsCandidateID != NO_CANDIDATE_ID
						//We already have a candidate ID secured. 
						//Steal it and fake the mission getting an acceptance from the candidate controller.
						missionCandidateID = g_iCommsCandidateID
						g_iCommsCandidateID = NO_CANDIDATE_ID
						allowLaunch = MCRET_ACCEPTED
						CPRINTLN(DEBUG_FLOW, "DO_MISSION_NOW(", debugMissionIDString, "): Took the candidate ID secured by the comms controller.")
					ELSE
						//Phonecall triggered mission but the comms controller must have failed to secure a candidate ID.
						allowLaunch = REQUEST_MISSION_LAUNCH(missionCandidateID, eCandidatePriority, MISSION_TYPE_STORY)
						CPRINTLN(DEBUG_FLOW, "DO_MISSION_NOW(", debugMissionIDString, "): Launch priority was PHONECALL_TO_PLAYER but the comms controller didn't secure a candidate ID for the mission.")
					ENDIF
				ELSE
					//Not a phonecall triggered mission. Try and secure our own candidate ID.
					allowLaunch = REQUEST_MISSION_LAUNCH(missionCandidateID, eCandidatePriority, MISSION_TYPE_STORY)
				ENDIF
				
				//Store our updated candidate ID in the available missions array so we can check it next frame.
				g_availableMissionsTU[availableMissionIndex].missionCandidateID = missionCandidateID
				
				//Check if we we allowed to launch this frame.
				IF allowLaunch = MCRET_PROCESSING
					CPRINTLN(DEBUG_FLOW, "DO_MISSION_NOW(", debugMissionIDString, "): Waiting for candidate ID response.")
					RETURN STAY_ON_THIS_COMMAND
				ELIF allowLaunch = MCRET_DENIED
					CPRINTLN(DEBUG_FLOW, "DO_MISSION_NOW(", debugMissionIDString, "): Candidate ID request denied.")
					RETURN INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue3) // Fail jump label.
				ENDIF
			ENDIF
			CPRINTLN(DEBUG_FLOW, "DO_MISSION_NOW(", debugMissionIDString, "): Sucessfully secured a candidate ID to launch.")
			
			//Flag this mission as launching.
			SET_BIT(g_availableMissionsTU[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_LAUNCHING)
			
				// Has this mission already been flagged as passed?
				IF g_savedGlobalsClifford.sFlow.missionSavedData[eMissionID].completed
					CPRINTLN(DEBUG_FLOW, "DO_MISSION_NOW(", debugMissionIDString, "): Mission already flagged as passed.  Automatically passing.")
					SET_BIT(g_availableMissionsTU[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_RUNNING)
					SET_BIT(g_availableMissionsTU[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_PASSED)
				ENDIF
						
			// The mission is in the process of launching. 
			// Make the player invincible for the few frames it takes to start the mission.
			IF IS_PLAYER_PLAYING(PLAYER_ID())
				SET_PLAYER_INVINCIBLE(PLAYER_ID(), TRUE)
			ENDIF
		ENDIF
		
		//Wait for the main thread to be finished before allowing to launch.
		IF NOT g_isSPMainInitialised
			CPRINTLN(DEBUG_FLOW, "DO_MISSION_NOW(", debugMissionIDString, "): Waiting for the SP main thread to be initialised.")
			RETURN STAY_ON_THIS_COMMAND
		ENDIF
		
		//Launch the mission stat watcher

		IF NOT IS_BIT_SET(g_sMissionStaticData[eMissionID].settingsBitset, MF_INDEX_NO_PASS)			
			IF NOT IS_BIT_SET(g_availableMissionsTU[availableMissionIndex].bitflags, BITS_AVAILABLE_STAT_WATCHER_RUNNING)	
				IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH( HASH("mission_stat_watchCLF") ) > 0
					TERMINATE_STAT_WATCHER()
					CPRINTLN(DEBUG_FLOW, "DO_MISSION_NOW(", debugMissionIDString, "): Waiting for old stat watcher to terminate.")
					RETURN STAY_ON_THIS_COMMAND // Wait until old watcher has terminated.
				ELIF HAS_SCRIPT_WITH_NAME_HASH_LOADED( HASH("mission_stat_watchCLF") )
					CPRINTLN(DEBUG_FLOW, "DO_MISSION_NOW(", debugMissionIDString, "): Starting new stat watcher thread for mission.")
					START_NEW_SCRIPT_WITH_NAME_HASH( HASH("mission_stat_watchCLF"), DEFAULT_STACK_SIZE)
				    SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED( HASH("mission_stat_watchCLF") )
					SET_BIT(g_availableMissionsTU[availableMissionIndex].bitflags, BITS_AVAILABLE_STAT_WATCHER_RUNNING)
				ELSE
					CPRINTLN(DEBUG_FLOW, "DO_MISSION_NOW(", debugMissionIDString, "): Waiting for stat watcher thread to stream in.")
					RETURN STAY_ON_THIS_COMMAND // Waiting for script to load.
				ENDIF							
			ENDIF			
		ENDIF
		
		// Has the script loaded?
		IF NOT HAS_SCRIPT_WITH_NAME_HASH_LOADED(missionScriptHash)
			CPRINTLN(DEBUG_FLOW, "DO_MISSION_NOW(", debugMissionIDString, "): Waiting for script to load.")
			RETURN STAY_ON_THIS_COMMAND
		ENDIF
		
		// Mission has loaded, so launch it
		g_availableMissionsTU[availableMissionIndex].theThread = START_NEW_SCRIPT_WITH_NAME_HASH(missionScriptHash, MISSION_STACK_SIZE)
		SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(missionScriptHash)
		SET_BIT(g_availableMissionsTU[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_RUNNING)
		
		BOOL bHidePhoneOnStart = FALSE
		IF IS_BIT_SET(g_sMissionStaticData[eMissionID].settingsBitset, MF_INDEX_HIDE_PHONE_ON_START)
			bHidePhoneOnStart = TRUE
		ENDIF

		MISSION_PROCESSING_MISSION_STARTING(paramCoreVarsArrayPos, availableMissionIndex, eMissionID, paramStrandID, bHidePhoneOnStart)
		RETURN STAY_ON_THIS_COMMAND
	ENDIF
	
	// ---------------RUN MISSION ---------------------------------------------
	
	// Check if mission has failed so we can keep the mission thread running whilst we set up a replay
	BOOL hasFailed = IS_BIT_SET(g_availableMissionsTU[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_FAILED)
	
	// Mission is running, so check if it has terminated
	IF (IS_THREAD_ACTIVE(g_availableMissionsTU[availableMissionIndex].theThread))

		IF hasFailed
			IF NOT IS_BIT_SET(g_sMissionStaticData[eMissionID].settingsBitset, MF_INDEX_NO_FAIL)
			AND g_bSkipFailScreen = FALSE
				// going to a replay screen, go on to termination section
				// mission script continues to run for a while during replay processing
				CPRINTLN(DEBUG_FLOW, "DO_MISSION_NOW(", debugMissionIDString, "): Failed and going to replay screen.")
			ELSE
				// not going to a replay screen, keep running
				RETURN STAY_ON_THIS_COMMAND
			ENDIF
		ELSE
			// not going to a replay screen, keep running
			RETURN STAY_ON_THIS_COMMAND
		ENDIF
	ENDIF
	
	// ----------MISSION TERMINATION -------------------------------------
	// ...get the Pass/Fail state for the mission
	BOOL hasPassed = IS_BIT_SET(g_availableMissionsTU[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_PASSED)

	// ...ensure the mission hasn't both passed and failed
	IF (hasPassed)
	AND (hasFailed)
		CPRINTLN(DEBUG_FLOW, "DO_MISSION_NOW(", debugMissionIDString, "): Mission has both PASSED and FAILED. Treating as PASSED.")
		SCRIPT_ASSERT("PERFORM_DO_MISSION_NOW: : Mission has both PASSED and FAILED. Treating as passed.")
		hasFailed = FALSE
	ENDIF
	
	// ...ensure the mission has either passed or failed
	IF NOT (hasPassed)
	AND NOT (hasFailed)
		CPRINTLN(DEBUG_FLOW, "DO_MISSION_NOW(", debugMissionIDString, "): Mission has not PASSED or FAILED. Treating as FAILED.")
		SCRIPT_ASSERT("PERFORM_DO_MISSION_NOW: : Mission has not PASSED or FAILED. Treating as FAILED.")
		#IF IS_DEBUG_BUILD
			// overwrite the fail reason, so mission scripters know the mission has failed due to an array overrun or similar
			g_txtMissionFailReason = "FLOW_FAIL"
		#ENDIF
		hasFailed = TRUE
	ENDIF
	
	//----------------MISSION PASSED--------------------------------
	IF (hasPassed)
		RETURN MISSION_PROCESSING_MISSION_PASSED(paramCoreVarsArrayPos, availableMissionIndex, eMissionID, FALSE)
	ENDIF
	
	//----------------MISSION FAILED -------------------------------
	RETURN MISSION_PROCESSING_MISSION_FAILED(paramCoreVarsArrayPos, availableMissionIndex, eMissionID, FALSE)
ENDFUNC
#endif
#if USE_NRM_DLC
FUNC JUMP_LABEL_IDS PERFORM_DO_MISSION_NOWNRM(INT paramCoreVarsArrayPos, STRANDS paramStrandID = STRAND_NONE)

	// --------INIT --------------------------------------------------------
	// Ensure the Script Vars Array position is legal
	IF (paramCoreVarsArrayPos = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_DO_MISSION_NOW: The Script Variables Array Position parameter is illegal. MOVING ON TO NEXT COMMAND.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Retrieve the details for this mission from the 'available missions' array
	INT availableMissionIndex = GET_AVAILABLE_MISSION_INDEX_FOR_STORED_MISSION(paramCoreVarsArrayPos)
	SP_MISSIONS eMissionID	= INT_TO_ENUM(SP_MISSIONS, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue1)
	INT missionScriptHash = g_sMissionStaticData[eMissionID].scriptHash
	
	#IF IS_DEBUG_BUILD
		STRING debugMissionIDString = GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMissionID)
	#ENDIF
	
	IF NOT IS_BIT_SET(g_sMissionStaticData[eMissionID].settingsBitset, MF_INDEX_NO_PASS)		
		CDEBUG3LN(DEBUG_FLOW, "DO_MISSION_NOW(", debugMissionIDString, "): Requesting stat watcher for NRM mission.")
		REQUEST_SCRIPT_WITH_NAME_HASH( HASH("mission_stat_watchNRM") )		
#IF IS_DEBUG_BUILD
	ELSE
		CDEBUG3LN(DEBUG_FLOW, "DO_MISSION_NOW(", debugMissionIDString, "): Skipped requesting stat watcher as the mission is flagged to have no pass screen.")
#ENDIF	
	ENDIF
	
	REQUEST_SCRIPT_WITH_NAME_HASH(missionScriptHash)
	
	// --------REPLAY SETUP / REJECTION --------------------------------------
	// If a replay is in the process of being setup, wait here
	IF IS_REPLAY_BEING_PROCESSED()
		//CPRINTLN(DEBUG_REPLAY, "Replay is being processed, pausing PERFORM_DO_MISSION_NOW.")
		RETURN STAY_ON_THIS_COMMAND
	ELSE
		// If replay has been rejected, clean up and exit
		// (this will make IS_REPLAY_BEING_PROCESSED return false, so other missions can try to launch)
		IF g_replay.replayCoreVarsIndex = paramCoreVarsArrayPos
			IF g_replay.replayStageID =   RS_REJECTED
				// Replay rejected, call mission over routine
				MISSION_PROCESSING_MISSION_OVER(eMissionID)
				CPRINTLN(DEBUG_REPLAY, "Replay has been rejected, cleaning up mission.")
				MISSION_PROCESSING_RELEASE_MISSION(paramCoreVarsArrayPos, availableMissionIndex, FALSE)
				CPRINTLN(DEBUG_FLOW, "DO_MISSION_NOW(", debugMissionIDString, "):  Failed after replay rejection.")
				RETURN INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue3) // Fail jump label.
			ENDIF
		ENDIF
	ENDIF
	
	// Check if the player is alive before allowing the mission to trigger.
	// Do mission now doesn't use the trigger system so we need to check for this explicitly here.
	// AndyM- Added check that mission isn't failed to fix problem with dying as different character
	// to the one that triggered the mission, during a fail fade.
	IF IS_ENTITY_DEAD(PLAYER_PED_ID())
		IF availableMissionIndex = ILLEGAL_ARRAY_POSITION // mission not registered
		OR NOT IS_BIT_SET(g_availableMissionsTU[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_FAILED) // mission not failed
			//CPRINTLN(DEBUG_REPLAY, "In the entity dead check!")
			RETURN STAY_ON_THIS_COMMAND
		ENDIF
	ENDIF
	
	// Check that the player character is a valid SP character.
	IF NOT IS_PLAYER_PED_PLAYABLE(GET_CURRENT_PLAYER_PED_ENUM())
		CDEBUG1LN(DEBUG_FLOW,  "DO_MISSION_NOW(", debugMissionIDString, "): Current player character isn't a singleplayer character.")
		RETURN STAY_ON_THIS_COMMAND
	ENDIF
	
	// --------REGISTER MISSION ------------------------------------------------
	// If the mission is not already 'available' then register the mission with the 'available missions' array
	// NOTE: A strand that reaches a 'do mission' command registers the mission as available and stores a
	//			pointer to the mission's details together with some control variables for triggering the mission.
	IF (availableMissionIndex = ILLEGAL_ARRAY_POSITION)
		availableMissionIndex = REGISTER_AVAILABLE_MISSION(paramCoreVarsArrayPos)
		
		// If the registration failed then exit
		IF (availableMissionIndex = ILLEGAL_ARRAY_POSITION)
			RETURN STAY_ON_THIS_COMMAND
		ENDIF
		
		#IF IS_DEBUG_BUILD
			// As an extra safety check, on successful registration ensure that the rest of the struct details are empty
			IF NOT (g_availableMissionsTU[availableMissionIndex].missionCandidateID = NO_CANDIDATE_ID)
				CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_DO_MISSION_NOW: Successfully registered in the Available Missions array but there seems to be existing data in the new array slot.")
			ENDIF
		#ENDIF
	ENDIF
	
	INT missionCandidateID 	= g_availableMissionsTU[availableMissionIndex].missionCandidateID

	IF MISSION_CANNOT_BE_REGISTERED_BECAUSE_OF_ACTIVE_FRIEND(eMissionID)
		CPRINTLN(DEBUG_FLOW, "DO_MISSION_NOW(", debugMissionIDString, "): Won't be registered as triggerable as player has activity organised with mission character.")
		RETURN STAY_ON_THIS_COMMAND
	ENDIF
	
	// ----------------MISSION LAUNCH -----------------------------------------------
	IF NOT (IS_BIT_SET(g_availableMissionsTU[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_RUNNING))
		// ...no, the mission is not yet running, so check if it is launching
		IF NOT (IS_BIT_SET(g_availableMissionsTU[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_LAUNCHING))
			// ...no, the mission is not yet launching, so immediately launch if the mission is being replayed
			IF NOT (IS_BIT_SET(g_availableMissionsTU[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_BEING_REPLAYED))
				// Set candidate priority
				m_enumMissionCandidateTypeID eCandidatePriority
			
				IF IS_REPEAT_PLAY_ACTIVE()
				OR IS_BIT_SET(g_sMissionStaticData[eMissionID].settingsBitset, MF_INDEX_MUST_LAUNCH_PRIORITY)
					// In repeat play we force the priority so it always launches.
					// Nothing should contest this anyway but best to be safe.
					eCandidatePriority = MCTID_MUST_LAUNCH
				ELSE 
					//None repeat play or flagged to use must launch. Assume this is a phonecall priority trigger.
					eCandidatePriority = INT_TO_ENUM(m_enumMissionCandidateTypeID, MCTID_PHONECALL_TO_PLAYER)
				ENDIF
				
				// ...mission is not being replayed so still needs permission from the Mission Candidate System to launch
				//If the launch priority is phonecall the comms controller should have secured a candidate ID ahead of time.
				m_enumMissionCandidateReturnValue allowLaunch
				IF eCandidatePriority = MCTID_PHONECALL_TO_PLAYER
					IF g_iCommsCandidateID != NO_CANDIDATE_ID
						//We already have a candidate ID secured. 
						//Steal it and fake the mission getting an acceptance from the candidate controller.
						missionCandidateID = g_iCommsCandidateID
						g_iCommsCandidateID = NO_CANDIDATE_ID
						allowLaunch = MCRET_ACCEPTED
						CPRINTLN(DEBUG_FLOW, "DO_MISSION_NOW(", debugMissionIDString, "): Took the candidate ID secured by the comms controller.")
					ELSE
						//Phonecall triggered mission but the comms controller must have failed to secure a candidate ID.
						allowLaunch = REQUEST_MISSION_LAUNCH(missionCandidateID, eCandidatePriority, MISSION_TYPE_STORY)
						CPRINTLN(DEBUG_FLOW, "DO_MISSION_NOW(", debugMissionIDString, "): Launch priority was PHONECALL_TO_PLAYER but the comms controller didn't secure a candidate ID for the mission.")
					ENDIF
				ELSE
					//Not a phonecall triggered mission. Try and secure our own candidate ID.
					allowLaunch = REQUEST_MISSION_LAUNCH(missionCandidateID, eCandidatePriority, MISSION_TYPE_STORY)
				ENDIF
				
				//Store our updated candidate ID in the available missions array so we can check it next frame.
				g_availableMissionsTU[availableMissionIndex].missionCandidateID = missionCandidateID
				
				//Check if we we allowed to launch this frame.
				IF allowLaunch = MCRET_PROCESSING
					CPRINTLN(DEBUG_FLOW, "DO_MISSION_NOW(", debugMissionIDString, "): Waiting for candidate ID response.")
					RETURN STAY_ON_THIS_COMMAND
				ELIF allowLaunch = MCRET_DENIED
					CPRINTLN(DEBUG_FLOW, "DO_MISSION_NOW(", debugMissionIDString, "): Candidate ID request denied.")
					RETURN INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue3) // Fail jump label.
				ENDIF
			ENDIF
			CPRINTLN(DEBUG_FLOW, "DO_MISSION_NOW(", debugMissionIDString, "): Sucessfully secured a candidate ID to launch.")
			
			//Flag this mission as launching.
			SET_BIT(g_availableMissionsTU[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_LAUNCHING)
				
			// Has this mission already been flagged as passed?
			IF g_savedGlobalsnorman.sFlow.missionSavedData[eMissionID].completed
				CPRINTLN(DEBUG_FLOW, "DO_MISSION_NOW(", debugMissionIDString, "): Mission already flagged as passed.  Automatically passing.")
				SET_BIT(g_availableMissionsTU[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_RUNNING)
				SET_BIT(g_availableMissionsTU[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_PASSED)
			ENDIF
			
			// The mission is in the process of launching. 
			// Make the player invincible for the few frames it takes to start the mission.
			IF IS_PLAYER_PLAYING(PLAYER_ID())
				SET_PLAYER_INVINCIBLE(PLAYER_ID(), TRUE)
			ENDIF
		ENDIF
		
		//Wait for the main thread to be finished before allowing to launch.
		IF NOT g_isSPMainInitialised
			CPRINTLN(DEBUG_FLOW, "DO_MISSION_NOW(", debugMissionIDString, "): Waiting for the SP main thread to be initialised.")
			RETURN STAY_ON_THIS_COMMAND
		ENDIF
		
		//Launch the mission stat watcher
		
		IF NOT IS_BIT_SET(g_sMissionStaticData[eMissionID].settingsBitset, MF_INDEX_NO_PASS)
			IF NOT IS_BIT_SET(g_availableMissionsTU[availableMissionIndex].bitflags, BITS_AVAILABLE_STAT_WATCHER_RUNNING)							
				IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH( HASH("mission_stat_watchNRM") ) > 0
					TERMINATE_STAT_WATCHER()
					CPRINTLN(DEBUG_FLOW, "DO_MISSION_NOW(", debugMissionIDString, "): Waiting for old stat watcher to terminate.")
					RETURN STAY_ON_THIS_COMMAND // Wait until old watcher has terminated.
				ELIF HAS_SCRIPT_WITH_NAME_HASH_LOADED( HASH("mission_stat_watchNRM") )
					CPRINTLN(DEBUG_FLOW, "DO_MISSION_NOW(", debugMissionIDString, "): Starting new stat watcher thread for mission.")
					START_NEW_SCRIPT_WITH_NAME_HASH( HASH("mission_stat_watchNRM"), DEFAULT_STACK_SIZE)
				    SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED( HASH("mission_stat_watchNRM") )
					SET_BIT(g_availableMissionsTU[availableMissionIndex].bitflags, BITS_AVAILABLE_STAT_WATCHER_RUNNING)
				ELSE
					CPRINTLN(DEBUG_FLOW, "DO_MISSION_NOW(", debugMissionIDString, "): Waiting for stat watcher thread to stream in.")
					RETURN STAY_ON_THIS_COMMAND // Waiting for script to load.
				ENDIF
			ENDIF					
		ENDIF
		
		// Has the script loaded?
		IF NOT HAS_SCRIPT_WITH_NAME_HASH_LOADED(missionScriptHash)
			CPRINTLN(DEBUG_FLOW, "DO_MISSION_NOW(", debugMissionIDString, "): Waiting for script to load.")
			RETURN STAY_ON_THIS_COMMAND
		ENDIF
		
		// Mission has loaded, so launch it
		g_availableMissionsTU[availableMissionIndex].theThread = START_NEW_SCRIPT_WITH_NAME_HASH(missionScriptHash, MISSION_STACK_SIZE)
		SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(missionScriptHash)
		SET_BIT(g_availableMissionsTU[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_RUNNING)
		
		BOOL bHidePhoneOnStart = FALSE
		IF IS_BIT_SET(g_sMissionStaticData[eMissionID].settingsBitset, MF_INDEX_HIDE_PHONE_ON_START)
			bHidePhoneOnStart = TRUE
		ENDIF

		MISSION_PROCESSING_MISSION_STARTING(paramCoreVarsArrayPos, availableMissionIndex, eMissionID, paramStrandID, bHidePhoneOnStart)
		RETURN STAY_ON_THIS_COMMAND
	ENDIF
	
	// ---------------RUN MISSION ---------------------------------------------
	
	// Check if mission has failed so we can keep the mission thread running whilst we set up a replay
	BOOL hasFailed = IS_BIT_SET(g_availableMissionsTU[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_FAILED)
	
	// Mission is running, so check if it has terminated
	IF (IS_THREAD_ACTIVE(g_availableMissionsTU[availableMissionIndex].theThread))

		IF hasFailed
			IF NOT IS_BIT_SET(g_sMissionStaticData[eMissionID].settingsBitset, MF_INDEX_NO_FAIL)
			AND g_bSkipFailScreen = FALSE
				// going to a replay screen, go on to termination section
				// mission script continues to run for a while during replay processing
				CPRINTLN(DEBUG_FLOW, "DO_MISSION_NOW(", debugMissionIDString, "): Failed and going to replay screen.")
			ELSE
				// not going to a replay screen, keep running
				RETURN STAY_ON_THIS_COMMAND
			ENDIF
		ELSE
			// not going to a replay screen, keep running
			RETURN STAY_ON_THIS_COMMAND
		ENDIF
	ENDIF
	
	// ----------MISSION TERMINATION -------------------------------------
	// ...get the Pass/Fail state for the mission
	BOOL hasPassed = IS_BIT_SET(g_availableMissionsTU[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_PASSED)

	// ...ensure the mission hasn't both passed and failed
	IF (hasPassed)
	AND (hasFailed)
		CPRINTLN(DEBUG_FLOW, "DO_MISSION_NOW(", debugMissionIDString, "): Mission has both PASSED and FAILED. Treating as PASSED.")
		SCRIPT_ASSERT("PERFORM_DO_MISSION_NOW: : Mission has both PASSED and FAILED. Treating as passed.")
		hasFailed = FALSE
	ENDIF
	
	// ...ensure the mission has either passed or failed
	IF NOT (hasPassed)
	AND NOT (hasFailed)
		CPRINTLN(DEBUG_FLOW, "DO_MISSION_NOW(", debugMissionIDString, "): Mission has not PASSED or FAILED. Treating as FAILED.")
		SCRIPT_ASSERT("PERFORM_DO_MISSION_NOW: : Mission has not PASSED or FAILED. Treating as FAILED.")
		#IF IS_DEBUG_BUILD
			// overwrite the fail reason, so mission scripters know the mission has failed due to an array overrun or similar
			g_txtMissionFailReason = "FLOW_FAIL"
		#ENDIF
		hasFailed = TRUE
	ENDIF
	
	//----------------MISSION PASSED--------------------------------
	IF (hasPassed)
		RETURN MISSION_PROCESSING_MISSION_PASSED(paramCoreVarsArrayPos, availableMissionIndex, eMissionID, FALSE)
	ENDIF
	
	//----------------MISSION FAILED -------------------------------
	RETURN MISSION_PROCESSING_MISSION_FAILED(paramCoreVarsArrayPos, availableMissionIndex, eMissionID, FALSE)
ENDFUNC
#endif
FUNC JUMP_LABEL_IDS PERFORM_DO_MISSION_NOW(INT paramCoreVarsArrayPos, STRANDS paramStrandID = STRAND_NONE)
#if USE_CLF_DLC
 	return PERFORM_DO_MISSION_NOWCLF( paramCoreVarsArrayPos,  paramStrandID )
#endif
#if USE_NRM_DLC
 	return PERFORM_DO_MISSION_NOWNRM( paramCoreVarsArrayPos,  paramStrandID )
#endif
#if not USE_CLF_DLC
#if not USE_NRM_DLC
	// --------INIT --------------------------------------------------------
	// Ensure the Script Vars Array position is legal
	IF (paramCoreVarsArrayPos = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_DO_MISSION_NOW: The Script Variables Array Position parameter is illegal. MOVING ON TO NEXT COMMAND.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Retrieve the details for this mission from the 'available missions' array
	INT availableMissionIndex = GET_AVAILABLE_MISSION_INDEX_FOR_STORED_MISSION(paramCoreVarsArrayPos)
	SP_MISSIONS eMissionID	= INT_TO_ENUM(SP_MISSIONS, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue1)
	INT missionScriptHash = g_sMissionStaticData[eMissionID].scriptHash
	
	#IF IS_DEBUG_BUILD
		STRING debugMissionIDString = GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMissionID)
	#ENDIF
	
	IF NOT IS_BIT_SET(g_sMissionStaticData[eMissionID].settingsBitset, MF_INDEX_NO_PASS)
		CDEBUG3LN(DEBUG_FLOW, "DO_MISSION_NOW(", debugMissionIDString, "): Requesting stat watcher for GTAV mission.")
		REQUEST_SCRIPT_WITH_NAME_HASH( HASH("mission_stat_watcher") )
		
#IF IS_DEBUG_BUILD
	ELSE
		CDEBUG3LN(DEBUG_FLOW, "DO_MISSION_NOW(", debugMissionIDString, "): Skipped requesting stat watcher as the mission is flagged to have no pass screen.")
#ENDIF	
	ENDIF
	
	REQUEST_SCRIPT_WITH_NAME_HASH(missionScriptHash)
	
	// --------REPLAY SETUP / REJECTION --------------------------------------
	// If a replay is in the process of being setup, wait here
	IF IS_REPLAY_BEING_PROCESSED()
		//CPRINTLN(DEBUG_REPLAY, "Replay is being processed, pausing PERFORM_DO_MISSION_NOW.")
		RETURN STAY_ON_THIS_COMMAND
	ELSE
		// If replay has been rejected, clean up and exit
		// (this will make IS_REPLAY_BEING_PROCESSED return false, so other missions can try to launch)
		IF g_replay.replayCoreVarsIndex = paramCoreVarsArrayPos
			IF g_replay.replayStageID =   RS_REJECTED
				// Replay rejected, call mission over routine
				MISSION_PROCESSING_MISSION_OVER(eMissionID)
				CPRINTLN(DEBUG_REPLAY, "Replay has been rejected, cleaning up mission.")
				MISSION_PROCESSING_RELEASE_MISSION(paramCoreVarsArrayPos, availableMissionIndex, FALSE)
				CPRINTLN(DEBUG_FLOW, "DO_MISSION_NOW(", debugMissionIDString, "):  Failed after replay rejection.")
				RETURN INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue3) // Fail jump label.
			ENDIF
		ENDIF
	ENDIF
	
	// Check if the player is alive before allowing the mission to trigger.
	// Do mission now doesn't use the trigger system so we need to check for this explicitly here.
	// AndyM- Added check that mission isn't failed to fix problem with dying as different character
	// to the one that triggered the mission, during a fail fade.
	IF IS_ENTITY_DEAD(PLAYER_PED_ID())
		IF availableMissionIndex = ILLEGAL_ARRAY_POSITION // mission not registered
		OR NOT IS_BIT_SET(g_availableMissions[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_FAILED) // mission not failed
			//CPRINTLN(DEBUG_REPLAY, "In the entity dead check!")
			RETURN STAY_ON_THIS_COMMAND
		ENDIF
	ENDIF
	
	// Check that the player character is a valid SP character.
	IF NOT IS_PLAYER_PED_PLAYABLE(GET_CURRENT_PLAYER_PED_ENUM())
		CDEBUG1LN(DEBUG_FLOW,  "DO_MISSION_NOW(", debugMissionIDString, "): Current player character isn't a singleplayer character.")
		RETURN STAY_ON_THIS_COMMAND
	ENDIF
	
	// --------REGISTER MISSION ------------------------------------------------
	// If the mission is not already 'available' then register the mission with the 'available missions' array
	// NOTE: A strand that reaches a 'do mission' command registers the mission as available and stores a
	//			pointer to the mission's details together with some control variables for triggering the mission.
	IF (availableMissionIndex = ILLEGAL_ARRAY_POSITION)
		availableMissionIndex = REGISTER_AVAILABLE_MISSION(paramCoreVarsArrayPos)
		
		// If the registration failed then exit
		IF (availableMissionIndex = ILLEGAL_ARRAY_POSITION)
			RETURN STAY_ON_THIS_COMMAND
		ENDIF
		
		#IF IS_DEBUG_BUILD
			// As an extra safety check, on successful registration ensure that the rest of the struct details are empty
			IF NOT (g_availableMissions[availableMissionIndex].missionCandidateID = NO_CANDIDATE_ID)
				CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_DO_MISSION_NOW: Successfully registered in the Available Missions array but there seems to be existing data in the new array slot.")
			ENDIF
		#ENDIF
	ENDIF
	
	INT missionCandidateID 	= g_availableMissions[availableMissionIndex].missionCandidateID

	IF MISSION_CANNOT_BE_REGISTERED_BECAUSE_OF_ACTIVE_FRIEND(eMissionID)
		CPRINTLN(DEBUG_FLOW, "DO_MISSION_NOW(", debugMissionIDString, "): Won't be registered as triggerable as player has activity organised with mission character.")
		RETURN STAY_ON_THIS_COMMAND
	ENDIF
	
	// ----------------MISSION LAUNCH -----------------------------------------------
	IF NOT (IS_BIT_SET(g_availableMissions[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_RUNNING))
		// ...no, the mission is not yet running, so check if it is launching
		IF NOT (IS_BIT_SET(g_availableMissions[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_LAUNCHING))
			// ...no, the mission is not yet launching, so immediately launch if the mission is being replayed
			IF NOT (IS_BIT_SET(g_availableMissions[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_BEING_REPLAYED))
				// Set candidate priority
				m_enumMissionCandidateTypeID eCandidatePriority
			
				IF IS_REPEAT_PLAY_ACTIVE()
				OR IS_BIT_SET(g_sMissionStaticData[eMissionID].settingsBitset, MF_INDEX_MUST_LAUNCH_PRIORITY)
					// In repeat play we force the priority so it always launches.
					// Nothing should contest this anyway but best to be safe.
					eCandidatePriority = MCTID_MUST_LAUNCH
				ELSE 
					//None repeat play or flagged to use must launch. Assume this is a phonecall priority trigger.
					eCandidatePriority = INT_TO_ENUM(m_enumMissionCandidateTypeID, MCTID_PHONECALL_TO_PLAYER)
				ENDIF
				
				// ...mission is not being replayed so still needs permission from the Mission Candidate System to launch
				//If the launch priority is phonecall the comms controller should have secured a candidate ID ahead of time.
				m_enumMissionCandidateReturnValue allowLaunch
				IF eCandidatePriority = MCTID_PHONECALL_TO_PLAYER
					IF g_iCommsCandidateID != NO_CANDIDATE_ID
						//We already have a candidate ID secured. 
						//Steal it and fake the mission getting an acceptance from the candidate controller.
						missionCandidateID = g_iCommsCandidateID
						g_iCommsCandidateID = NO_CANDIDATE_ID
						allowLaunch = MCRET_ACCEPTED
						CPRINTLN(DEBUG_FLOW, "DO_MISSION_NOW(", debugMissionIDString, "): Took the candidate ID secured by the comms controller.")
					ELSE
						//Phonecall triggered mission but the comms controller must have failed to secure a candidate ID.
						allowLaunch = REQUEST_MISSION_LAUNCH(missionCandidateID, eCandidatePriority, MISSION_TYPE_STORY)
						CPRINTLN(DEBUG_FLOW, "DO_MISSION_NOW(", debugMissionIDString, "): Launch priority was PHONECALL_TO_PLAYER but the comms controller didn't secure a candidate ID for the mission.")
					ENDIF
				ELSE
					//Not a phonecall triggered mission. Try and secure our own candidate ID.
					allowLaunch = REQUEST_MISSION_LAUNCH(missionCandidateID, eCandidatePriority, MISSION_TYPE_STORY)
				ENDIF
				
				//Store our updated candidate ID in the available missions array so we can check it next frame.
				g_availableMissions[availableMissionIndex].missionCandidateID = missionCandidateID
				
				//Check if we we allowed to launch this frame.
				IF allowLaunch = MCRET_PROCESSING
					CPRINTLN(DEBUG_FLOW, "DO_MISSION_NOW(", debugMissionIDString, "): Waiting for candidate ID response.")
					RETURN STAY_ON_THIS_COMMAND
				ELIF allowLaunch = MCRET_DENIED
					CPRINTLN(DEBUG_FLOW, "DO_MISSION_NOW(", debugMissionIDString, "): Candidate ID request denied.")
					RETURN INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue3) // Fail jump label.
				ENDIF
			ENDIF
			CPRINTLN(DEBUG_FLOW, "DO_MISSION_NOW(", debugMissionIDString, "): Sucessfully secured a candidate ID to launch.")
			
			//Flag this mission as launching.
			SET_BIT(g_availableMissions[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_LAUNCHING)			
			// Has this mission already been flagged as passed?
			IF g_savedGlobals.sFlow.missionSavedData[eMissionID].completed
				CPRINTLN(DEBUG_FLOW, "DO_MISSION_NOW(", debugMissionIDString, "): Mission already flagged as passed.  Automatically passing.")
				SET_BIT(g_availableMissions[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_RUNNING)
				SET_BIT(g_availableMissions[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_PASSED)
			ENDIF
			
			// The mission is in the process of launching. 
			// Make the player invincible for the few frames it takes to start the mission.
			IF IS_PLAYER_PLAYING(PLAYER_ID())
				SET_PLAYER_INVINCIBLE(PLAYER_ID(), TRUE)
			ENDIF
		ENDIF
		
		//Wait for the main thread to be finished before allowing to launch.
		IF NOT g_isSPMainInitialised
			CPRINTLN(DEBUG_FLOW, "DO_MISSION_NOW(", debugMissionIDString, "): Waiting for the SP main thread to be initialised.")
			RETURN STAY_ON_THIS_COMMAND
		ENDIF
		
		//Launch the mission stat watcher
		IF eMissionID != SP_MISSION_FINALE_CREDITS				
			IF NOT IS_BIT_SET(g_sMissionStaticData[eMissionID].settingsBitset, MF_INDEX_NO_PASS)
				IF (!g_bMissionStatSystemBlocker) AND ((eMissionID != SP_MISSION_FINALE_C2) OR (!g_bMissionStatSystemUploadPending))//special case for loading autosave between C1 C2
					IF NOT IS_BIT_SET(g_availableMissions[availableMissionIndex].bitflags, BITS_AVAILABLE_STAT_WATCHER_RUNNING)						
							IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH( HASH("mission_stat_watcher") ) > 0
								TERMINATE_STAT_WATCHER()
								CPRINTLN(DEBUG_FLOW, "DO_MISSION_NOW(", debugMissionIDString, "): Waiting for old stat watcher to terminate.")
								RETURN STAY_ON_THIS_COMMAND // Wait until old watcher has terminated.
							ELIF HAS_SCRIPT_WITH_NAME_HASH_LOADED( HASH("mission_stat_watcher") )
								CPRINTLN(DEBUG_FLOW, "DO_MISSION_NOW(", debugMissionIDString, "): Starting new stat watcher thread for mission.")
								START_NEW_SCRIPT_WITH_NAME_HASH( HASH("mission_stat_watcher"), SPECIAL_ABILITY_STACK_SIZE)
							    SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED( HASH("mission_stat_watcher") )
								SET_BIT(g_availableMissions[availableMissionIndex].bitflags, BITS_AVAILABLE_STAT_WATCHER_RUNNING)
							ELSE
								CPRINTLN(DEBUG_FLOW, "DO_MISSION_NOW(", debugMissionIDString, "): Waiting for stat watcher thread to stream in.")
								RETURN STAY_ON_THIS_COMMAND // Waiting for script to load.
							ENDIF
						
					ENDIF
				ELSE
					IF (eMissionID = SP_MISSION_FINALE_C2)
						SET_BIT(g_availableMissions[availableMissionIndex].bitflags, BITS_AVAILABLE_STAT_WATCHER_RUNNING)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		// Has the script loaded?
		IF NOT HAS_SCRIPT_WITH_NAME_HASH_LOADED(missionScriptHash)
			CPRINTLN(DEBUG_FLOW, "DO_MISSION_NOW(", debugMissionIDString, "): Waiting for script to load.")
			RETURN STAY_ON_THIS_COMMAND
		ENDIF
		
		// Mission has loaded, so launch it
		g_availableMissions[availableMissionIndex].theThread = START_NEW_SCRIPT_WITH_NAME_HASH(missionScriptHash, MISSION_STACK_SIZE)
		SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(missionScriptHash)
		SET_BIT(g_availableMissions[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_RUNNING)
		
		BOOL bHidePhoneOnStart = FALSE
		IF IS_BIT_SET(g_sMissionStaticData[eMissionID].settingsBitset, MF_INDEX_HIDE_PHONE_ON_START)
			bHidePhoneOnStart = TRUE
		ENDIF

		MISSION_PROCESSING_MISSION_STARTING(paramCoreVarsArrayPos, availableMissionIndex, eMissionID, paramStrandID, bHidePhoneOnStart)
		RETURN STAY_ON_THIS_COMMAND
	ENDIF
	
	// ---------------RUN MISSION ---------------------------------------------
	
	// Check if mission has failed so we can keep the mission thread running whilst we set up a replay
	BOOL hasFailed = IS_BIT_SET(g_availableMissions[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_FAILED)
	
	// Mission is running, so check if it has terminated
	IF (IS_THREAD_ACTIVE(g_availableMissions[availableMissionIndex].theThread))

		IF hasFailed
			IF NOT IS_BIT_SET(g_sMissionStaticData[eMissionID].settingsBitset, MF_INDEX_NO_FAIL)
			AND g_bSkipFailScreen = FALSE
				// going to a replay screen, go on to termination section
				// mission script continues to run for a while during replay processing
				CPRINTLN(DEBUG_FLOW, "DO_MISSION_NOW(", debugMissionIDString, "): Failed and going to replay screen.")
			ELSE
				// not going to a replay screen, keep running
				RETURN STAY_ON_THIS_COMMAND
			ENDIF
		ELSE
			// not going to a replay screen, keep running
			RETURN STAY_ON_THIS_COMMAND
		ENDIF
	ENDIF
	
	// ----------MISSION TERMINATION -------------------------------------
	// ...get the Pass/Fail state for the mission
	BOOL hasPassed = IS_BIT_SET(g_availableMissions[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_PASSED)

	// ...ensure the mission hasn't both passed and failed
	IF (hasPassed)
	AND (hasFailed)
		CPRINTLN(DEBUG_FLOW, "DO_MISSION_NOW(", debugMissionIDString, "): Mission has both PASSED and FAILED. Treating as PASSED.")
		SCRIPT_ASSERT("PERFORM_DO_MISSION_NOW: : Mission has both PASSED and FAILED. Treating as passed.")
		hasFailed = FALSE
	ENDIF
	
	// ...ensure the mission has either passed or failed
	IF NOT (hasPassed)
	AND NOT (hasFailed)
		CPRINTLN(DEBUG_FLOW, "DO_MISSION_NOW(", debugMissionIDString, "): Mission has not PASSED or FAILED. Treating as FAILED.")
		SCRIPT_ASSERT("PERFORM_DO_MISSION_NOW: : Mission has not PASSED or FAILED. Treating as FAILED.")
		#IF IS_DEBUG_BUILD
			// overwrite the fail reason, so mission scripters know the mission has failed due to an array overrun or similar
			g_txtMissionFailReason = "FLOW_FAIL"
		#ENDIF
		hasFailed = TRUE
	ENDIF
	
	//----------------MISSION PASSED--------------------------------
	IF (hasPassed)
		RETURN MISSION_PROCESSING_MISSION_PASSED(paramCoreVarsArrayPos, availableMissionIndex, eMissionID, FALSE)
	ENDIF
	
	//----------------MISSION FAILED -------------------------------
	RETURN MISSION_PROCESSING_MISSION_FAILED(paramCoreVarsArrayPos, availableMissionIndex, eMissionID, FALSE)
#endif
#endif
ENDFUNC
#if USE_CLF_DLC
/// PURPOSE: DANGEROUS! set's a strand to a previous or future label in it's flow
///    Please do not use this unless you're sure - ask CraigV or BenR
PROC JUMP_STRAND_TO_LABELCLF(STRANDS eStrandID,JUMP_LABEL_IDS eJumpToLabel)

	IF (eStrandID = STRAND_NONE)
	OR (eStrandID = MAX_STRANDS_CLF)
		CERRORLN(DEBUG_FLOW_COMMANDS, "JUMP_STRAND_TO_LABELagt: The StrandID is illegal. MOVING ON TO NEXT COMMAND.")	
		SCRIPT_ASSERT("JUMP_STRAND_TO_LABELagt: The StrandID is illegal. MOVING ON TO NEXT COMMAND. - bug CV")
		EXIT
	ENDIF
	// Do nothing if the strand is still awaiting activation
	IF NOT (IS_BIT_SET(g_savedGlobalsClifford.sFlow.strandSavedVars[eStrandID].savedBitflags, SAVED_BITS_STRAND_ACTIVATED))
		CERRORLN(DEBUG_FLOW_COMMANDS, "JUMP_STRAND_TO_LABELagt: Ignored because strand is still awaiting activation. MOVING ON TO NEXT COMMAND.")
		SCRIPT_ASSERT("JUMP_STRAND_TO_LABELagt: Ignored because strand is still awaiting activation. MOVING ON TO NEXT COMMAND. - bug CV")
		EXIT
	ENDIF	
	// Do nothing if the strand has already terminated
	IF (IS_BIT_SET(g_savedGlobalsClifford.sFlow.strandSavedVars[eStrandID].savedBitflags, SAVED_BITS_STRAND_TERMINATED))
		CERRORLN(DEBUG_FLOW_COMMANDS, "JUMP_STRAND_TO_LABELagt: Ignored because strand has already terminated. MOVING ON TO NEXT COMMAND.")
		SCRIPT_ASSERT("JUMP_STRAND_TO_LABELagt: Ignored because strand has already terminated. MOVING ON TO NEXT COMMAND. - bug CV")
		EXIT
	ENDIF
	
	INT iCommandPos		= g_savedGlobalsClifford.sFlow.strandSavedVars[eStrandID].thisCommandPos
	INT iLowerBounds	= g_flowUnsaved.flowCommandsIndex[eStrandID].lower
	INT iUpperBounds	= g_flowUnsaved.flowCommandsIndex[eStrandID].upper
	// Ensure the command position for the strand is already within the lower and upper boundaries
	IF (iCommandPos < iLowerBounds)
		CERRORLN(DEBUG_FLOW_COMMANDS, "JUMP_STRAND_TO_LABELagt: Current command position for strand is outside lower boundary for this strand's commands", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(eStrandID))
		SCRIPT_ASSERT("JUMP_STRAND_TO_LABELagt: Current command position for strand is outside lower boundary for this strand's commands - bug CV")
		EXIT
	ENDIF
	IF (iCommandPos > iUpperBounds)
		CERRORLN(DEBUG_FLOW_COMMANDS, "JUMP_STRAND_TO_LABELagt: Current command position for strand is outside upper boundary for this strand's commands", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(eStrandID))
		SCRIPT_ASSERT("JUMP_STRAND_TO_LABELagt: Current command position for strand is outside upper boundary for this strand's commands - bug CV")
		EXIT
	ENDIF
	
	// if the current position is a varation of do mission, remove that mission from the available missions
	INT availableMissionIndex = GET_AVAILABLE_MISSION_INDEX_FOR_STORED_MISSION(g_flowUnsaved.flowCommands[iCommandPos].index)
	if g_flowUnsaved.flowCommands[iCommandPos].command = FLOW_DO_MISSION_AT_BLIP
	or g_flowUnsaved.flowCommands[iCommandPos].command = FLOW_DO_MISSION_AT_SWITCH		
		MISSION_PROCESSING_RELEASE_MISSION(g_flowUnsaved.flowCommands[iCommandPos].index, availableMissionIndex, TRUE)
	ELIF g_flowUnsaved.flowCommands[iCommandPos].command = FLOW_DO_MISSION_NOW
		MISSION_PROCESSING_RELEASE_MISSION(g_flowUnsaved.flowCommands[iCommandPos].index, availableMissionIndex, FALSE)
	ENDIF
	
	INT iTempLoop		= iLowerBounds
	INT iStorageIndex	= ILLEGAL_ARRAY_POSITION
	
	WHILE (iTempLoop <= iUpperBounds)
		IF (g_flowUnsaved.flowCommands[iTempLoop].command = FLOW_LABEL)
		
			// The label is stored in the core vars array. Retrieve the jump label.
			iStorageIndex = g_flowUnsaved.flowCommands[iTempLoop].index
			JUMP_LABEL_IDS eLabel = INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.coreVars[iStorageIndex].iValue1)
			
			//Does this match our jump request?
			IF (eLabel = eJumpToLabel)
				// ...found the jump label, so store this array position as the current position for the strand
				g_savedGlobalsClifford.sFlow.strandSavedVars[eStrandID].thisCommandPos = iTempLoop
				EXIT
			ENDIF			
		ENDIF		
		iTempLoop++
	ENDWHILE
	
	// Failed to find this label within this strand's upper and lower boundaries
	CERRORLN(DEBUG_FLOW_COMMANDS,  "JUMP_STRAND_TO_LABELagt: Failed to find label within the strand's upper and lower boundaries.", GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(eJumpToLabel), " ", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(eStrandID))
	SCRIPT_ASSERT("JUMP_STRAND_TO_LABELagt: Failed to find label within the strand's upper and lower boundaries. - bug CV")
ENDPROC
#endif

#if USE_NRM_DLC
/// PURPOSE: DANGEROUS! set's a strand to a previous or future label in it's flow
///    Please do not use this unless you're sure - ask CraigV or BenR
PROC JUMP_STRAND_TO_LABELNRM(STRANDS eStrandID,JUMP_LABEL_IDS eJumpToLabel)

	IF (eStrandID = STRAND_NONE)
	OR (eStrandID = MAX_STRANDS_NRM)
		CERRORLN(DEBUG_FLOW_COMMANDS, "JUMP_STRAND_TO_LABELNRM: The StrandID is illegal. MOVING ON TO NEXT COMMAND.")	
		SCRIPT_ASSERT("JUMP_STRAND_TO_LABELNRM: The StrandID is illegal. MOVING ON TO NEXT COMMAND. - bug CV")
		EXIT
	ENDIF
	// Do nothing if the strand is still awaiting activation
	IF NOT (IS_BIT_SET(g_savedGlobalsnorman.sFlow.strandSavedVars[eStrandID].savedBitflags, SAVED_BITS_STRAND_ACTIVATED))
		CERRORLN(DEBUG_FLOW_COMMANDS, "JUMP_STRAND_TO_LABELNRM: Ignored because strand is still awaiting activation. MOVING ON TO NEXT COMMAND.")
		SCRIPT_ASSERT("JUMP_STRAND_TO_LABELNRM: Ignored because strand is still awaiting activation. MOVING ON TO NEXT COMMAND. - bug CV")
		EXIT
	ENDIF	
	// Do nothing if the strand has already terminated
	IF (IS_BIT_SET(g_savedGlobalsnorman.sFlow.strandSavedVars[eStrandID].savedBitflags, SAVED_BITS_STRAND_TERMINATED))
		CERRORLN(DEBUG_FLOW_COMMANDS, "JUMP_STRAND_TO_LABELNRM: Ignored because strand has already terminated. MOVING ON TO NEXT COMMAND.")
		SCRIPT_ASSERT("JUMP_STRAND_TO_LABELNRM: Ignored because strand has already terminated. MOVING ON TO NEXT COMMAND. - bug CV")
		EXIT
	ENDIF
	
	INT iCommandPos		= g_savedGlobalsnorman.sFlow.strandSavedVars[eStrandID].thisCommandPos
	INT iLowerBounds	= g_flowUnsaved.flowCommandsIndex[eStrandID].lower
	INT iUpperBounds	= g_flowUnsaved.flowCommandsIndex[eStrandID].upper
	// Ensure the command position for the strand is already within the lower and upper boundaries
	IF (iCommandPos < iLowerBounds)
		CERRORLN(DEBUG_FLOW_COMMANDS, "JUMP_STRAND_TO_LABELNRM: Current command position for strand is outside lower boundary for this strand's commands", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(eStrandID))
		SCRIPT_ASSERT("JUMP_STRAND_TO_LABELNRM: Current command position for strand is outside lower boundary for this strand's commands - bug CV")
		EXIT
	ENDIF
	IF (iCommandPos > iUpperBounds)
		CERRORLN(DEBUG_FLOW_COMMANDS, "JUMP_STRAND_TO_LABELNRM: Current command position for strand is outside upper boundary for this strand's commands", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(eStrandID))
		SCRIPT_ASSERT("JUMP_STRAND_TO_LABELNRM: Current command position for strand is outside upper boundary for this strand's commands - bug CV")
		EXIT
	ENDIF
	
	// if the current position is a varation of do mission, remove that mission from the available missions
	INT availableMissionIndex = GET_AVAILABLE_MISSION_INDEX_FOR_STORED_MISSION(g_flowUnsaved.flowCommands[iCommandPos].index)
	if g_flowUnsaved.flowCommands[iCommandPos].command = FLOW_DO_MISSION_AT_BLIP
	or g_flowUnsaved.flowCommands[iCommandPos].command = FLOW_DO_MISSION_AT_SWITCH		
		MISSION_PROCESSING_RELEASE_MISSION(g_flowUnsaved.flowCommands[iCommandPos].index, availableMissionIndex, TRUE)
	ELIF g_flowUnsaved.flowCommands[iCommandPos].command = FLOW_DO_MISSION_NOW
		MISSION_PROCESSING_RELEASE_MISSION(g_flowUnsaved.flowCommands[iCommandPos].index, availableMissionIndex, FALSE)
	ENDIF
	
	INT iTempLoop		= iLowerBounds
	INT iStorageIndex	= ILLEGAL_ARRAY_POSITION
	
	WHILE (iTempLoop <= iUpperBounds)
		IF (g_flowUnsaved.flowCommands[iTempLoop].command = FLOW_LABEL)
		
			// The label is stored in the core vars array. Retrieve the jump label.
			iStorageIndex = g_flowUnsaved.flowCommands[iTempLoop].index
			JUMP_LABEL_IDS eLabel = INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.coreVars[iStorageIndex].iValue1)
			
			//Does this match our jump request?
			IF (eLabel = eJumpToLabel)
				// ...found the jump label, so store this array position as the current position for the strand
				g_savedGlobalsnorman.sFlow.strandSavedVars[eStrandID].thisCommandPos = iTempLoop
				EXIT
			ENDIF			
		ENDIF		
		iTempLoop++
	ENDWHILE
	
	// Failed to find this label within this strand's upper and lower boundaries
	CERRORLN(DEBUG_FLOW_COMMANDS,  "JUMP_STRAND_TO_LABELNRM: Failed to find label within the strand's upper and lower boundaries.", GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(eJumpToLabel), " ", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(eStrandID))
	SCRIPT_ASSERT("JUMP_STRAND_TO_LABELNRM: Failed to find label within the strand's upper and lower boundaries. - bug CV")
ENDPROC
#endif
/// PURPOSE: DANGEROUS! set's a strand to a previous or future label in it's flow
///    Please do not use this unless you're sure - ask CraigV or BenR
PROC JUMP_STRAND_TO_LABEL(STRANDS eStrandID,JUMP_LABEL_IDS eJumpToLabel)
#if USE_CLF_DLC
	JUMP_STRAND_TO_LABELCLF(eStrandID,eJumpToLabel)
	exit
#endif
#if USE_NRM_DLC
	JUMP_STRAND_TO_LABELNRM(eStrandID,eJumpToLabel)
	exit
#endif
#if not USE_CLF_DLC
#if not USE_NRM_DLC



	IF (eStrandID = STRAND_NONE)
	OR (eStrandID = MAX_STRANDS)
		CERRORLN(DEBUG_FLOW_COMMANDS, "JUMP_STRAND_TO_LABELagt: The StrandID is illegal. MOVING ON TO NEXT COMMAND.")	
		SCRIPT_ASSERT("JUMP_STRAND_TO_LABELagt: The StrandID is illegal. MOVING ON TO NEXT COMMAND. - bug CV")
		EXIT
	ENDIF
	// Do nothing if the strand is still awaiting activation
	IF NOT (IS_BIT_SET(g_savedGlobals.sFlow.strandSavedVars[eStrandID].savedBitflags, SAVED_BITS_STRAND_ACTIVATED))
		CERRORLN(DEBUG_FLOW_COMMANDS, "JUMP_STRAND_TO_LABEL: Ignored because strand is still awaiting activation. MOVING ON TO NEXT COMMAND.")
		SCRIPT_ASSERT("JUMP_STRAND_TO_LABEL: Ignored because strand is still awaiting activation. MOVING ON TO NEXT COMMAND. - bug CV")
		EXIT
	ENDIF	
	// Do nothing if the strand has already terminated
	IF (IS_BIT_SET(g_savedGlobals.sFlow.strandSavedVars[eStrandID].savedBitflags, SAVED_BITS_STRAND_TERMINATED))
		CERRORLN(DEBUG_FLOW_COMMANDS, "JUMP_STRAND_TO_LABELagt: Ignored because strand has already terminated. MOVING ON TO NEXT COMMAND.")
		SCRIPT_ASSERT("JUMP_STRAND_TO_LABELagt: Ignored because strand has already terminated. MOVING ON TO NEXT COMMAND. - bug CV")
		EXIT
	ENDIF
	
	INT iCommandPos		= g_savedGlobals.sFlow.strandSavedVars[eStrandID].thisCommandPos
	INT iLowerBounds	= g_flowUnsaved.flowCommandsIndex[eStrandID].lower
	INT iUpperBounds	= g_flowUnsaved.flowCommandsIndex[eStrandID].upper
	// Ensure the command position for the strand is already within the lower and upper boundaries
	IF (iCommandPos < iLowerBounds)
		CERRORLN(DEBUG_FLOW_COMMANDS, "JUMP_STRAND_TO_LABELagt: Current command position for strand is outside lower boundary for this strand's commands", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(eStrandID))
		SCRIPT_ASSERT("JUMP_STRAND_TO_LABELagt: Current command position for strand is outside lower boundary for this strand's commands - bug CV")
		EXIT
	ENDIF
	IF (iCommandPos > iUpperBounds)
		CERRORLN(DEBUG_FLOW_COMMANDS, "JUMP_STRAND_TO_LABELagt: Current command position for strand is outside upper boundary for this strand's commands", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(eStrandID))
		SCRIPT_ASSERT("JUMP_STRAND_TO_LABELagt: Current command position for strand is outside upper boundary for this strand's commands - bug CV")
		EXIT
	ENDIF
	
	INT iTempLoop		= iLowerBounds
	INT iStorageIndex	= ILLEGAL_ARRAY_POSITION
	
	// if the current position is a varation of do mission, remove that mission from the available missions
	INT availableMissionIndex = GET_AVAILABLE_MISSION_INDEX_FOR_STORED_MISSION(g_flowUnsaved.flowCommands[iCommandPos].index)
	if g_flowUnsaved.flowCommands[iCommandPos].command = FLOW_DO_MISSION_AT_BLIP
	or g_flowUnsaved.flowCommands[iCommandPos].command = FLOW_DO_MISSION_AT_SWITCH		
		MISSION_PROCESSING_RELEASE_MISSION(g_flowUnsaved.flowCommands[iCommandPos].index, availableMissionIndex, TRUE)
	ELIF g_flowUnsaved.flowCommands[iCommandPos].command = FLOW_DO_MISSION_NOW
		MISSION_PROCESSING_RELEASE_MISSION(g_flowUnsaved.flowCommands[iCommandPos].index, availableMissionIndex, FALSE)
	ENDIF
		
	WHILE (iTempLoop <= iUpperBounds)
		IF (g_flowUnsaved.flowCommands[iTempLoop].command = FLOW_LABEL)
		
			// The label is stored in the core vars array. Retrieve the jump label.
			iStorageIndex = g_flowUnsaved.flowCommands[iTempLoop].index
			JUMP_LABEL_IDS eLabel = INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.coreVars[iStorageIndex].iValue1)
			
			//Does this match our jump request?
			IF (eLabel = eJumpToLabel)
				// ...found the jump label, so store this array position as the current position for the strand
				g_savedGlobals.sFlow.strandSavedVars[eStrandID].thisCommandPos = iTempLoop
				EXIT
			ENDIF			
		ENDIF		
		iTempLoop++
	ENDWHILE
	
	// Failed to find this label within this strand's upper and lower boundaries
	CERRORLN(DEBUG_FLOW_COMMANDS,  "JUMP_STRAND_TO_LABELagt: Failed to find label within the strand's upper and lower boundaries.", GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(eJumpToLabel), " ", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(eStrandID))
	SCRIPT_ASSERT("JUMP_STRAND_TO_LABELagt: Failed to find label within the strand's upper and lower boundaries. - bug CV")
#endif
#endif
ENDPROC

