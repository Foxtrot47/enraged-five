USING "rage_builtins.sch"
USING "globals.sch"
//USING "Game_Specific_Flow_Private.sch"
USING "debug_channels_structs.sch"

USING "flow_commands_core.sch"
USING "flow_commands_core_override.sch"
USING "flow_commands_GAME.sch"
USING "flow_mission_data_public.sch"


// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//      MISSION NAME    :   SP_Mission_Flow_Debug.sch
//      AUTHOR          :   Keith
//      DESCRIPTION     :   Contains any standard mission flow debug functionality.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************




// Set this to 1 if initaliser debug output is required.
// This outputs a description of every Mission Flow Command as it gets stored in the commands array
CONST_INT   MISSION_FLOW_INITIALISER_DEBUG_OUTPUT	0


// Set this to 1 if profiler output is required
// This outputs a processor profile for the Mission Flow - requires the profiler switched on by widget
CONST_INT MISSION_FLOW_PROFILING	0


#IF MISSION_FLOW_PROFILING
USING "profiler.sch"	
#ENDIF



// -----------------------------------------------------------------------------------------------------------
//      Mission Flow Standard Debug Output CharSheet Strings
// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Return a CharSheet as a string for display
//
// INPUT PARAMS:    paramCharSheet          A CharSheet ID
// RETURN VALUE:    STRING                  Debug display string for the CharSheet ID
FUNC STRING GET_CHARSHEET_DISPLAY_STRING_FROM_CHARSHEETCLF(enumCharacterList paramCharSheet, BOOL paramSuppressAssert = FALSE)

    SWITCH (paramCharSheet)
        // Playable characters
		CASE CHAR_MICHAEL
            RETURN "Michael"
			BREAK
			
        CASE CHAR_FRANKLIN
            RETURN "Franklin"
			BREAK
    
        CASE CHAR_TREVOR
            RETURN "Trevor"
			BREAK

			
        // Other
        CASE CHAR_CLF_BLANK_ENTRY //CHAR_BLANK_ENTRY used when setting up a phonecall that can be received by any of the playable characters.
            RETURN "Any Playable"
			BREAK
			
		CASE NO_CHARACTER
			RETURN "No Character"
			BREAK
    ENDSWITCH
    
    IF NOT paramSuppressAssert
        SCRIPT_ASSERT("Get_CharSheet_Display_String_From_CharSheetAGT: Unknown CharSheet ID - Needs adding - Tell BenR.")
    ENDIF
    RETURN "Unknown"

ENDFUNC
FUNC STRING GET_CHARSHEET_DISPLAY_STRING_FROM_CHARSHEETNRM(enumCharacterList paramCharSheet, BOOL paramSuppressAssert = FALSE)

    SWITCH (paramCharSheet)
        // Playable characters
		CASE CHAR_MICHAEL
            RETURN "Michael"
			BREAK
			
        CASE CHAR_NRM_JIMMY //= Franklin
            RETURN "Jimmy"
			BREAK
                
        CASE CHAR_NRM_TRACEY // = Trevor
            RETURN "Tracey"
			BREAK

			
        // Other
        CASE CHAR_NRM_BLANK_ENTRY //CHAR_BLANK_ENTRY used when setting up a phonecall that can be received by any of the playable characters.
            RETURN "Any Playable"
			BREAK
			
		CASE NO_CHARACTER
			RETURN "No Character"
			BREAK
    ENDSWITCH
    
    IF NOT paramSuppressAssert
        SCRIPT_ASSERT("Get_CharSheet_Display_String_From_CharSheetNRM: Unknown CharSheet ID - Needs adding - Tell BenR.")
    ENDIF
    RETURN "Unknown"

ENDFUNC
FUNC STRING GET_CHARSHEET_DISPLAY_STRING_FROM_CHARSHEET(enumCharacterList paramCharSheet, BOOL paramSuppressAssert = FALSE)
	
	#if USE_CLF_DLC
		return GET_CHARSHEET_DISPLAY_STRING_FROM_CHARSHEETCLF(paramCharSheet,paramSuppressAssert)
	#endif
	
	#if USE_NRM_DLC
		return GET_CHARSHEET_DISPLAY_STRING_FROM_CHARSHEETNRM(paramCharSheet,paramSuppressAssert)
	#endif
	
	
    SWITCH (paramCharSheet)
        // Playable characters
        CASE CHAR_FRANKLIN
            RETURN "Franklin"
			BREAK
            
        CASE CHAR_MICHAEL
            RETURN "Michael"
			BREAK
    
        CASE CHAR_TREVOR
            RETURN "Trevor"
			BREAK

        // Story characters
        CASE CHAR_LESTER
            RETURN "Lester"
			BREAK
			
		CASE CHAR_LESTER_DEATHWISH
            RETURN "Lester Deathwish"
			BREAK
            
        CASE CHAR_JIMMY
            RETURN "Jimmy"
			BREAK
        
        CASE CHAR_TRACEY
            RETURN "Tracey"
			BREAK
        
        CASE CHAR_AMANDA
            RETURN "Amanda"
			BREAK
        
        CASE CHAR_SIMEON
            RETURN "Simeon"
			BREAK
        
        CASE CHAR_LAMAR
            RETURN "Lamar"
			BREAK
            
        CASE CHAR_RON
            RETURN "Ron"
			BREAK
            
        CASE CHAR_CHENG
            RETURN "Cheng"
			BREAK
			
		CASE CHAR_CHENGSR
            RETURN "ChengSR"
			BREAK				
            
        CASE CHAR_STEVE
            RETURN "Steve"
			BREAK
			
		CASE CHAR_STRETCH
			RETURN "Stretch"
			BREAK
            
        CASE CHAR_WADE
            RETURN "Wade"
			BREAK
            
        CASE CHAR_TENNIS_COACH
            RETURN "Tennis Coach"
			BREAK
            
        CASE CHAR_SOLOMON
            RETURN "Solomon"
			BREAK
            
        CASE CHAR_LAZLOW
            RETURN "Lazlow"
			BREAK
            
        CASE CHAR_ESTATE_AGENT
            RETURN "Estate agent"
			BREAK
            
        CASE CHAR_DEVIN
            RETURN "Devin"
			BREAK
            
        CASE CHAR_DAVE
            RETURN "Dave"
			BREAK
            
        CASE CHAR_MARTIN
            RETURN "Martin"
			BREAK
            
        CASE CHAR_FLOYD
            RETURN "Floyd"
			BREAK
			
		CASE CHAR_OSCAR
			RETURN "Oscar"
			BREAK
			
		CASE CHAR_DR_FRIEDLANDER
			RETURN "Dr. Friedlander"
			BREAK
			
		CASE CHAR_ONEIL
			RETURN "O'Neil"
			BREAK
			
		CASE CHAR_TOW_TONYA
			RETURN "Tonya"
			BREAK
			
		CASE CHAR_PATRICIA
			RETURN "Patricia"
			BREAK
			
		CASE CHAR_TANISHA
			RETURN "Tanisha"
			BREAK
			
		CASE CHAR_DENISE
			RETURN "Denise"
			BREAK
			
		CASE CHAR_MOLLY
			RETURN "Molly"
			BREAK
			
		//Unlockable Heist Crew.
		CASE CHAR_RICKIE
			RETURN "Rickie"
			BREAK
			
		CASE CHAR_CHEF
			RETURN "Chef"
			BREAK
			
		//Random Characters.
		CASE CHAR_ABIGAIL
            RETURN "Abigail"
			BREAK
			
		CASE CHAR_ANTONIA
			RETURN "Antonia"
			BREAK
		
		CASE CHAR_BARRY
            RETURN "Barry"
			BREAK
         
		CASE CHAR_BEVERLY
            RETURN "Beverly"
			BREAK
						
		CASE CHAR_CRIS
            RETURN "Cris"
			BREAK
         
		CASE CHAR_DOM
            RETURN "Dom"
			BREAK
         
		CASE CHAR_HAO
            RETURN "Hao"
			BREAK
			
		CASE CHAR_HUNTER
            RETURN "Hunter"
			BREAK
         
		CASE CHAR_JIMMY_BOSTON
            RETURN "Jimmy Boston"
			BREAK
			
		CASE CHAR_JOE
            RETURN "Joe"
			BREAK
         
		CASE CHAR_JOSEF
            RETURN "Josef"
			BREAK
			
		CASE CHAR_JOSH
            RETURN "Josh"
			BREAK
         
		CASE CHAR_MANUEL
            RETURN "Manuel"
			BREAK
         
		CASE CHAR_MARNIE
            RETURN "Marnie"
			BREAK
         
		CASE CHAR_MARY_ANN
            RETURN "Mary Ann"
			BREAK
			
		CASE CHAR_MAUDE
            RETURN "Maude"
			BREAK
         
		CASE CHAR_MRS_THORNHILL
            RETURN "Mrs Thornhill"
			BREAK
         
		CASE CHAR_NIGEL
            RETURN "Nigel"
			BREAK
			
		CASE CHAR_SASQUATCH
            RETURN "Sasquatch"
			BREAK
			
		CASE CHAR_OMEGA
			RETURN "Omega"
			BREAK
			
		//Shop Owners.
        CASE CHAR_SOCIAL_CLUB
			RETURN "Mechanic"
			BREAK
			
		CASE CHAR_AMMUNATION
            RETURN "Ammu"
			BREAK
			
		CASE CHAR_LS_CUSTOMS
            RETURN "LSC"
			BREAK
			
		CASE CHAR_LS_TOURIST_BOARD
			RETURN "Tourism"
			BREAK
			
		//Properties
		CASE CHAR_PROPERTY_TAXI_LOT
			RETURN "Taxi Lot"
			BREAK
		
		CASE CHAR_PROPERTY_CINEMA_VINEWOOD
			RETURN "Cin. Vinewood"
			BREAK
			
		CASE CHAR_PROPERTY_CINEMA_DOWNTOWN
			RETURN "Cin. Downtown"
			BREAK
			
		CASE CHAR_PROPERTY_CINEMA_MORNINGWOOD
			RETURN "Cin. Morningwood"
			BREAK
			
		CASE CHAR_PROPERTY_CAR_SCRAP_YARD
			RETURN "Car Scrapyard"
			BREAK

		CASE CHAR_PROPERTY_WEED_SHOP
			RETURN "Weed Shop"
			BREAK
			
		CASE CHAR_PROPERTY_BAR_TEQUILALA
			RETURN "Tequi-la-la"
			BREAK
			
		CASE CHAR_PROPERTY_BAR_PITCHERS
			RETURN "Pitchers"
			BREAK
			
		CASE CHAR_PROPERTY_BAR_HEN_HOUSE
			RETURN "Hen House"
			BREAK
			
		CASE CHAR_PROPERTY_BAR_HOOKIES
			RETURN "Hookies"
			BREAK

		CASE CHAR_PROPERTY_GOLF_CLUB
			RETURN "Golf Club"
			BREAK	
			
		CASE CHAR_PROPERTY_CAR_MOD_SHOP
			RETURN "Car Mod Shop"
			BREAK	
			
		CASE CHAR_PROPERTY_TOWING_IMPOUND
			RETURN "Towing Impound"
			BREAK	
			
		CASE CHAR_PROPERTY_ARMS_TRAFFICKING
			RETURN "Arms Trafficking"
			BREAK	
			
		CASE CHAR_PROPERTY_SONAR_COLLECTIONS
			RETURN "Sonar Collections"
			BREAK	

        //Confernce call mixes.
		CASE CHAR_ALL_PLAYERS_CONF
			RETURN "Main Chars"
			BREAK
		
        CASE CHAR_STEVE_MIKE_CONF
            RETURN "Steve/Mike"
			BREAK
        
        CASE CHAR_STEVE_TREV_CONF
            RETURN "Steve/Trev"
			BREAK
			
		CASE CHAR_FRANK_TREV_CONF
            RETURN "Frank/Trev"
			BREAK
		
    	CASE CHAR_MIKE_FRANK_CONF
            RETURN "Mike/Frank"
			BREAK
			
    	CASE CHAR_MIKE_TREV_CONF
            RETURN "Mike/Trev"
			BREAK
			
        // Other
        CASE CHAR_BLANK_ENTRY //CHAR_BLANK_ENTRY used when setting up a phonecall that can be received by any of the playable characters.
            RETURN "Any Playable"
			BREAK
			
		CASE NO_CHARACTER
			RETURN "No Character"
			BREAK
			
		CASE CHAR_MONKEY_MAN
			RETURN "Monkey Man"
			BREAK
			
		CASE CHAR_ISAAC
			RETURN "Isaac"
			BREAK
			
		CASE CHAR_DIRECTOR
			RETURN "Director"
			BREAK
    ENDSWITCH
    
    IF NOT paramSuppressAssert
        SCRIPT_ASSERT("Get_CharSheet_Display_String_From_CharSheet: Unknown CharSheet ID - Needs adding - Tell BenR.")
    ENDIF
    RETURN "Unknown"

ENDFUNC

// -----------------------------------------------------------------------------------------------------------
//      Mission Flow Standard Debug Output Shop Name Strings
// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Return the name of a shop as a string for display
//
// INPUT PARAMS:    paramShopID             The shop ID
// RETURN VALUE:    STRING                  Debug display string for the shop ID

FUNC STRING GET_SHOP_DISPLAY_STRING_FROM_SHOP_ID(SHOP_NAME_ENUM paramShopID)

    SWITCH (paramShopID)
        CASE HAIRDO_SHOP_01_BH
			RETURN "Salon - Rockford Hills"
			BREAK
			
		CASE HAIRDO_SHOP_02_SC			 			
			RETURN "Barbers - South Los Santos"
			BREAK
			
		CASE HAIRDO_SHOP_03_V			 			
			RETURN "Barbers - Vespucci"
			BREAK
			
		CASE HAIRDO_SHOP_04_SS			 			
			RETURN "Barbers - Sandy Shores"
			BREAK
			
		CASE HAIRDO_SHOP_05_MP			 			
			RETURN "Barbers - Mirror Park"
			BREAK
			
		CASE HAIRDO_SHOP_06_HW			 			
			RETURN "Barbers - Vinewood"
			BREAK
			
		CASE HAIRDO_SHOP_07_PB			 			
			RETURN "Barbers - Paleto Bay"
			BREAK
			

		CASE CLOTHES_SHOP_L_01_SC				 	
			RETURN "Clothes Low - South Los Santos"
			BREAK
			
		CASE CLOTHES_SHOP_L_02_GS				 	
			RETURN "Clothes Low - Grapeseed"
			BREAK
			
		CASE CLOTHES_SHOP_L_03_DT				 	
			RETURN "Clothes Low - Downtown"
			BREAK
			
		CASE CLOTHES_SHOP_L_04_CS				 	
			RETURN "Clothes Low - Countryside"
			BREAK
			
		CASE CLOTHES_SHOP_L_05_GSD				 	
			RETURN "Clothes Low - Grande Senora Desert"
			BREAK
			
		CASE CLOTHES_SHOP_L_06_VC				 	
			RETURN "Clothes Low - Vespucci Canals"
			BREAK
			
		CASE CLOTHES_SHOP_L_07_PB				 	
			RETURN "Clothes Low - Paleto Bay"
			BREAK
			

		CASE CLOTHES_SHOP_M_01_SM			 		
			RETURN "Clothes Mid - Del Perro"
			BREAK
			
		CASE CLOTHES_SHOP_M_04_HW			 		
			RETURN "Clothes Mid - Vinewood"
			BREAK
			
		CASE CLOTHES_SHOP_M_05_GOH			 		
			RETURN "Clothes Mid - Great Ocean Highway"
			BREAK
			

		CASE CLOTHES_SHOP_H_01_BH			 		
			RETURN "Clothes High - Rockford Hills"
			BREAK
			
		CASE CLOTHES_SHOP_H_02_B			 		
			RETURN "Clothes High - Burton"
			BREAK
			
		CASE CLOTHES_SHOP_H_03_MW			 		
			RETURN "Clothes High - Morningwood"
			BREAK
			
		CASE CLOTHES_SHOP_A_01_VB
			RETURN"Clothes Ambient - Vespucci Movie Masks"
			BREAK
			

		CASE TATTOO_PARLOUR_01_HW			 		
			RETURN "Tattoo - Vinewood"
			BREAK
			
		CASE TATTOO_PARLOUR_02_SS			 		
			RETURN "Tattoo - Sandy Shores"
			BREAK
			
		CASE TATTOO_PARLOUR_03_PB			 		
			RETURN "Tattoo - Paleto Bay"
			BREAK
			
		CASE TATTOO_PARLOUR_04_VC			 		
			RETURN "Tattoo - Vespucci Canals"
			BREAK
			
		CASE TATTOO_PARLOUR_05_ELS			 		
			RETURN "Tattoo - East Los Santos"
			BREAK
			
		CASE TATTOO_PARLOUR_06_GOH			 		
			RETURN "Tattoo - Great Ocean Highway"
			BREAK
			

		CASE GUN_SHOP_01_DT				 			
			RETURN "Weapons - Downtown"
			BREAK
			
		CASE GUN_SHOP_02_SS				 			
			RETURN "Weapons - Sandy Shores"
			BREAK
			
		CASE GUN_SHOP_03_HW				 			
			RETURN "Weapons - Vinewood"
			BREAK
			
		CASE GUN_SHOP_04_ELS			 			
			RETURN "Weapons - East Los Santos"
			BREAK
			
		CASE GUN_SHOP_05_PB				 			
			RETURN "Weapons - Paleto Bay"
			BREAK
			
		CASE GUN_SHOP_06_LS				 			
			RETURN "Weapons - Little Seoul"
			BREAK
			
		CASE GUN_SHOP_07_MW				 			
			RETURN "Weapons - Morningwood"
			BREAK
			
		CASE GUN_SHOP_08_CS				 			
			RETURN "Weapons - Countryside"
			BREAK
			
		CASE GUN_SHOP_09_GOH			 			
			RETURN "Weapons - Great Ocean Highway"
			BREAK
			
		CASE GUN_SHOP_10_VWH
			RETURN "Weapons - Vinewood Hills"
			BREAK
			
		CASE GUN_SHOP_11_ID1
			RETURN "Weapons - Cypress Flats"
			BREAK
			

		CASE CARMOD_SHOP_01_AP			 			
			RETURN "Car Mod - Ambient 1"
			BREAK
			
		CASE CARMOD_SHOP_05_ID2						
			RETURN "Car Mod - Ambient 2"
			BREAK
			
		CASE CARMOD_SHOP_06_BT1
			RETURN "Car Mod - Ambient 3"
			BREAK
			
		CASE CARMOD_SHOP_07_CS1					
			RETURN "Car Mod - Ambient 4"
			BREAK
			
		CASE CARMOD_SHOP_08_CS6						
			RETURN "Car Mod - Ambient 5"
			BREAK
    ENDSWITCH
    
    SCRIPT_ASSERT("Get_Shop_Display_String_From_Shop_ID: Unknown Shop ID - Needs Added - Tell KennethR")
    RETURN "Unknown"
    
ENDFUNC



// -----------------------------------------------------------------------------------------------------------
//      Mission Flow Standard Debug Output Vehicle Gen Name Strings
// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Return the name of a vehicle gen name as a string for display
//
// INPUT PARAMS:    paramVehicleGenID       The vehicle gen ID
// RETURN VALUE:    STRING                  Debug display string for the vehicle gen ID

FUNC STRING GET_VEHICLE_GEN_DISPLAY_STRING_FROM_VEHICLEGEN_ID(VEHICLE_GEN_NAME_ENUM paramVehicleGenID)
    
    SWITCH (paramVehicleGenID)
        CASE VEHGEN_MICHAEL_SAVEHOUSE		
            RETURN "Michaels savehouse"
			BREAK
			
        CASE VEHGEN_FRANKLIN_SAVEHOUSE_CAR
            RETURN "Franklins savehouse car"
			BREAK
			
		CASE VEHGEN_FRANKLIN_SAVEHOUSE_BIKE
            RETURN "Franklins savehouse bike"
			BREAK
        
        CASE VEHGEN_TREVOR_SAVEHOUSE_COUNTRY
            RETURN "Trevors country savehouse"
			BREAK
            
        CASE VEHGEN_TREVOR_SAVEHOUSE_CITY
            RETURN "Trevors city savehouse"
			BREAK
			
		CASE VEHGEN_TREVOR_SAVEHOUSE_STRIPCLUB
			RETURN "Trevors stripclub savehouse"
			BREAK
            
        CASE VEHGEN_MOUNTAIN_BIKE_CH
            RETURN "Mountain bike in hills"
			BREAK
            
        CASE VEHGEN_SEASHARK_SM
            RETURN "Seashark at beach"
			BREAK
			
		CASE VEHGEN_DUSTER
			RETURN "Duster"
			BREAK
			
		//CASE VEHGEN_IMPOUND_TOW_TRUCK
		//	RETURN "Tow truck at impound"
		//	BREAK
		
		#IF IS_DEBUG_BUILD
		// Vehicle gens with dynamic data
		CASE VEHGEN_WEB_HANGAR_MICHAEL
            RETURN "Web Hangar Michael"
			BREAK
			
		CASE VEHGEN_WEB_HANGAR_FRANKLIN
            RETURN "Web Hangar Franklin"
			BREAK
			
		CASE VEHGEN_WEB_HANGAR_TREVOR
            RETURN "Web Hangar Trevor"
			BREAK
			
		CASE VEHGEN_WEB_MARINA_MICHAEL
            RETURN "Web Marina Michael"
			BREAK
			
		CASE VEHGEN_WEB_MARINA_FRANKLIN
            RETURN "Web Marina Franklin"
			BREAK
			
		CASE VEHGEN_WEB_MARINA_TREVOR
            RETURN "Web Marina Trevor"
			BREAK
			
		CASE VEHGEN_WEB_HELIPAD_MICHAEL
            RETURN "Web Helipad Michael"
			BREAK
			
		CASE VEHGEN_WEB_HELIPAD_FRANKLIN
            RETURN "Web Helipad Franklin"
			BREAK
			
		CASE VEHGEN_WEB_HELIPAD_TREVOR_COUNTRY
            RETURN "Web Helipad Trevor Country"
			BREAK
			
		CASE VEHGEN_WEB_CAR_MICHAEL
            RETURN "Web Car Michael"
			BREAK
			
		CASE VEHGEN_WEB_CAR_FRANKLIN
            RETURN "Web Car Franklin"
			BREAK
			
		CASE VEHGEN_WEB_CAR_TREVOR
            RETURN "Web Car Trevor"
			BREAK
			
		CASE VEHGEN_MISSION_VEH
            RETURN "Mission Veh"
			BREAK
			
		CASE VEHGEN_MISSION_VEH_FBI4_PREP
            RETURN "Mission Veh FBI4 Prep"
			BREAK
			
			
		CASE VEHGEN_MICHAEL_GARAGE_1 	// Keep order
            RETURN "Michael Garage 1"
			BREAK
			
		CASE VEHGEN_FRANKLIN_GARAGE_1	//
            RETURN "Franklin Garage 1"
			BREAK
			
		CASE VEHGEN_TREVOR_GARAGE_1		//
            RETURN "Trevor Garage 1"
			BREAK
			
		CASE VEHGEN_MICHAEL_GARAGE_2	//
            RETURN "Michael Garage 2"
			BREAK
			
		CASE VEHGEN_FRANKLIN_GARAGE_2	//
            RETURN "Franklin Garage 2"
			BREAK
			
		CASE VEHGEN_TREVOR_GARAGE_2		//
            RETURN "Trevor Garage 2"
			BREAK
			
		CASE VEHGEN_MICHAEL_GARAGE_3	//
            RETURN "Michael Garage 3"
			BREAK
			
		CASE VEHGEN_FRANKLIN_GARAGE_3	//
            RETURN "Franklin Garage 3"
			BREAK
			
		CASE VEHGEN_TREVOR_GARAGE_3		//
            RETURN "Trevor Garage 3"
			BREAK
		#ENDIF
    ENDSWITCH
    
    CASSERTLN(DEBUG_FLOW, "GET_VEHICLE_GEN_DISPLAY_STRING_FROM_VEHICLEGEN_ID: Unknown Vehicle Gen ID - Needs Added -paramCommandID = ", paramVehicleGenID)
    RETURN "Unknown"
    
ENDFUNC



// -----------------------------------------------------------------------------------------------------------
//      Mission Flow Strand Command Strings
// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Return a mission flow command as a string based on the command ID
//
// INPUT PARAMS:    paramCommandID          Command ID
// RETURN VALUE:    STRING                  Debug display string for the command

FUNC STRING GET_COMMAND_DISPLAY_STRING_FROM_COMMAND_ID_CUSTOM(FLOW_COMMAND_IDS paramCommandID)

    SWITCH (paramCommandID)
        CASE FLOW_ADD_PHONE_CONTACT
            RETURN "Add Phone Contact"
			BREAK
			
		CASE FLOW_REMOVE_PHONE_CONTACT
            RETURN "Remove Phone Contact"
			BREAK
        
        CASE FLOW_ADD_NEW_FRIEND_CONTACT
            RETURN "Add New Friend Contact"
			BREAK
			
		CASE FLOW_ADD_ORGANISER_EVENT
            RETURN "Add Organiser Event"
			BREAK
        
        CASE FLOW_BLOCK_FRIEND_CONTACT
            RETURN "Block Friend Contact"
			BREAK
        
        CASE FLOW_BLOCK_FRIEND_CLASH
            RETURN "Block Friend Clash"
			BREAK
        
        CASE FLOW_FRIEND_CONTACT_BUSY
            RETURN "Friend Contact Busy"
			BREAK
        
		CASE FLOW_DO_BOARD_INTERACTION
            RETURN "Do Board Interaction"
			BREAK
        
        CASE FLOW_IS_PLAYING_AS_CHARACTER
            RETURN "Is Playing As Character"
			BREAK
			
		CASE FLOW_IS_SWITCHING_TO_CHARACTER
            RETURN "Is Switching To Character"
			BREAK
			
		CASE FLOW_IS_PLAYER_PHONING_NPC
			RETURN "Is Player Phoning NPC"
			BREAK
        
		CASE FLOW_PLAYER_PHONE_NPC
            RETURN "Player Phone NPC"
			BREAK
			
		CASE FLOW_PLAYER_PHONE_NPC_WITH_BRANCH
            RETURN "Player Phone NPC with Branch"
			BREAK
		
        CASE FLOW_PHONE_PLAYER
            RETURN "Phone Player"
			BREAK
			
		 CASE FLOW_PHONE_PLAYER_WITH_BRANCH
            RETURN "Phone Player with Branch"
			BREAK
        
        CASE FLOW_PHONE_PLAYER_YES_NO
            RETURN "Phone Player Yes/No"
			BREAK
			
		CASE FLOW_PHONE_PLAYER_WITH_JUMPS
            RETURN "Phone Player With Jumps"
			BREAK
            
        CASE FLOW_TEXT_PLAYER
            RETURN "Text Player"
			BREAK
			
		CASE FLOW_EMAIL_PLAYER
			RETURN "Email Player"
			BREAK
		
        CASE FLOW_CANCEL_PHONECALL
            RETURN "Cancel Phonecall"
			BREAK
			
		CASE FLOW_CANCEL_TEXT
            RETURN "Cancel Text"
			BREAK
			
		CASE FLOW_SET_BOARD_DISPLAY_GROUP_VISIBILITY
			RETURN "Set Board Display Group Visibility"
			BREAK
  
		CASE FLOW_SET_DOOR_STATE
            RETURN "Set Door State"
			BREAK
  
		CASE FLOW_RESET_PRIORITY_SWITCH_SCENES
            RETURN "Reset Priority Switch Scenes"
			BREAK
  
		CASE FLOW_SET_PED_REQUEST_SCENE
            RETURN "Set Ped Request Scene"
			BREAK
  
        CASE FLOW_SET_CHAR_CHAT_PHONECALL
            RETURN "Set Char Chat Phonecall"
			BREAK
            
        CASE FLOW_SET_CHAR_HOTSWAP_AVAILABILITY
            RETURN "Set Char Hotswap Availability"
			BREAK
            
        CASE FLOW_SET_HOTSWAP_STAGE_FLAG
            RETURN "Set Hotswap Stage Flag"
			BREAK
            
        CASE FLOW_SET_PLAYER_CHARACTER
            RETURN "Set Player Character"
			BREAK
        
        CASE FLOW_SET_SAVEHOUSE_STATE
            RETURN "Set Savehouse State"
			BREAK
        
        CASE FLOW_SET_SHOP_STATE
            RETURN "Set Shop State"
			BREAK
		
		CASE FLOW_SET_WEAPON_COMP_LOCK_STATE
            RETURN "Set Weapon Comp Lock State"
			BREAK
            
        CASE FLOW_SET_VEHICLE_GEN_STATE
            RETURN "Set Vehicle Gen State"
			BREAK
			
		CASE FLOW_SET_SPECIAL_ABILITY_UNLOCK_STATE
			RETURN "Set Special Ability Unlock State"
			BREAK
			
		CASE FLOW_SET_MISSION_PASSED
			RETURN	"Set Mission Passed"
			BREAK
			
		CASE FLOW_SET_MISSION_NOT_PASSED
			RETURN	"Set Mission Not Passed"
			BREAK
        
		CASE FLOW_TOGGLE_BOARD_VIEWING
            RETURN "Toggle Board Viewing"
			BREAK
            
		CASE FLOW_IS_PLAYER_IN_AREA
            RETURN "Player Area Requirement"
			BREAK
			
		CASE FLOW_DO_PLAYER_BANK_ACTION
			RETURN "Do Player Bank Action"
			BREAK
			
		CASE FLOW_IS_SHRINK_SESSION_AVAILABLE
			RETURN "Is Shrink Session Available"
			BREAK
			
		CASE FLOW_WAIT_FOR_AUTOSAVE
			RETURN "Wait For Autosave"
			BREAK
			
		CASE FLOW_DO_MISSION_AT_SWITCH
			RETURN "Do Mission From Switch"
			BREAK
			
		CASE FLOW_SET_BIKES_SUPRESSED_STATE
			RETURN "Set Bikes Supressed State"
			BREAK
			
		CASE FLOW_SET_FRANKLIN_OUTFIT_BITSET_STATE
			RETURN "Set Franklin Outfit Bitset State"
			BREAK
			
		CASE FLOW_FAST_TRACK_STRAND
			RETURN "Fast Track Strand"
			BREAK
			
		CASE FLOW_DO_CUSTOM_CHECK_JUMPS
			RETURN "Do Custom Check Jumps"
			BREAK
			
		CASE FLOW_SET_BUYABLE_VEHICLE_STATE
            RETURN "Set Buyable Vehicle State"
			
		CASE FLOW_SET_COMPONENT_ACQUIRED
			RETURN "Set component acquired"
			BREAK
			
		CASE FLOW_DO_SWITCH_TO_CHARACTER
			RETURN "Switch to character"
			BREAK
			
		CASE FLOW_SET_STRAND_SAVE_OVERRIDE
			RETURN "Set Strand Save Override"
			BREAK

		CASE FLOW_CLEAR_STRAND_SAVE_OVERRIDE
			RETURN "Clear Strand Save Override"
			BREAK

		CASE FLOW_MP_WAIT_UNTIL_RANK
			RETURN "MP_Wait_until_Rank"
			BREAK
			
		CASE FLOW_MP_CONTINUE_MAIN
			RETURN "MP_Continue_Main_After_Compleation"
			BREAK
			
		CASE FLOW_MP_SET_TUTORIAL_SESSION_STATE
			RETURN "MP_Set_Tutorial_Session_State"
			BREAK
		
		CASE FLOW_MP_SET_MGROUP_ACTIVE
			RETURN "Set_MGroup_active"
			BREAK
			
		CASE FLOW_MP_SET_MISSION_ACTIVE_IN_MGROUP
			RETURN "Set_MGroup_Mission_Active"
			BREAK
			
		CASE FLOW_MP_SETUP_NEXT_MGROUP_MISSION		
			RETURN "Set_MGroup_Nxt_Mis"
			BREAK
			
		CASE FLOW_MP_UPDATE_MGROUP_ORDER
			RETURN "Update_MGroup_Order"
			BREAK
			
		CASE FLOW_MP_PUSH_TOWARDS_WEATHER_TYPE
			RETURN "PUSH_WEATHER_TYPE"
			BREAK
		
		CASE FLOW_MP_WAIT_UNTIL_ON_MISSION
			RETURN "WaitUntilOnMiss"
			BREAK
		
		CASE FLOW_MP_DO_RACES
			RETURN "Do Races"
			BREAK
		
		CASE FLOW_MP_AWAIT_EXTERNAL_INFLUENCE
			RETURN "Ext_Influence"
			BREAK
			
		CASE FLOW_SET_ASSASSINATION_STAGE_DLC
			RETURN "Set Assassin Stage DLC"
			BREAK

		CASE FLOW_NONE
			RETURN "FLOW_NONE"
			BREAK
    ENDSWITCH
	CERRORLN(DEBUG_FLOW, "Get_Command_Display_String_From_Command_ID: Unknown Command ID - Needs Added -paramCommandID = ", paramCommandID)
    RETURN "Unknown"
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Return the planning board interaction name as a string based on an interaction ID
//
// INPUT PARAMS:    paramInteractionID      Interaction ID.
// RETURN VALUE:    STRING                  Debug display string for the planning board interaction.

FUNC STRING GET_INTERACTION_DISPLAY_STRING_FROM_INTERACTION_ID(g_eBoardInteractions paramInteractionID)

    SWITCH (paramInteractionID)
		CASE INTERACT_POI_OVERVIEW
            RETURN "POI Overview"
			BREAK
	
        CASE INTERACT_CREW
            RETURN "Select Crew"
			BREAK
            
        CASE INTERACT_GAMEPLAY_CHOICE
            RETURN "Gameplay Choice"
			BREAK
    ENDSWITCH
    
    SCRIPT_ASSERT("Get_Interaction_Display_String_From_Interaction_ID: Interaction ID not defined. May need adding. See BenR")
    RETURN "Unknown"

ENDFUNC






// -----------------------------------------------------------------------------------------------------------
//      Mission Flow Standard Debug Output Label Strings
// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Return the label as a string based on the label ID
//
// INPUT PARAMS:    paramLabelID            Label ID
// RETURN VALUE:    STRING                  Debug display string for the label
#if USE_CLF_DLC
FUNC STRING GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID_CLF(JUMP_LABEL_IDS paramLabelID)
	
	SWITCH (paramLabelID)         
		//Heist labels.
        CASE LABEL_MISS_CLF_TRAIN			RETURN "LABEL_MISS_CLF_TRAIN"			BREAK
		CASE LABEL_MISS_CLF_FIN				RETURN "LABEL_MISS_CLF_FIN"				BREAK
			
		CASE LABEL_MISS_CLF_SPA_1			RETURN "LABEL_MISS_CLF_SPA_1"			BREAK
		CASE LABEL_MISS_CLF_SPA_2			RETURN "LABEL_MISS_CLF_SPA_2"			BREAK
		CASE LABEL_MISS_CLF_SPA_FIN			RETURN "LABEL_MISS_CLF_SPA_FIN"			BREAK
			
		CASE LABEL_MISS_CLF_IAA_TRA			RETURN "LABEL_MISS_CLF_IAA_TRA"			BREAK
		CASE LABEL_MISS_CLF_IAA_LIE			RETURN "LABEL_MISS_CLF_IAA_LIE"			BREAK
		CASE LABEL_MISS_CLF_IAA_JET			RETURN "LABEL_MISS_CLF_IAA_JET"			BREAK			
		CASE LABEL_MISS_CLF_IAA_HEL			RETURN "LABEL_MISS_CLF_IAA_HEL"			BREAK			
		CASE LABEL_MISS_CLF_IAA_DRO			RETURN "LABEL_MISS_CLF_IAA_DRO"			BREAK
		CASE LABEL_MISS_CLF_IAA_RTS			RETURN "LABEL_MISS_CLF_IAA_RTS"			BREAK
			
		CASE LABEL_MISS_CLF_KOR_PRO			RETURN "LABEL_MISS_CLF_KOR_PRO"			BREAK
		CASE LABEL_MISS_CLF_KOR_RES			RETURN "LABEL_MISS_CLF_KOR_RES"			BREAK
		CASE LABEL_MISS_CLF_KOR_SUB			RETURN "LABEL_MISS_CLF_KOR_SUB"			BREAK
		CASE LABEL_MISS_CLF_KOR_SAT			RETURN "LABEL_MISS_CLF_KOR_SAT"			BREAK
		CASE LABEL_MISS_CLF_KOR_5			RETURN "LABEL_MISS_CLF_KOR_5"			BREAK
			
		CASE LABEL_MISS_CLF_RUS_PLA			RETURN "LABEL_MISS_CLF_RUS_PLA"			BREAK
		CASE LABEL_MISS_CLF_RUS_CAR			RETURN "LABEL_MISS_CLF_RUS_CAR"			BREAK
		CASE LABEL_MISS_CLF_RUS_VAS			RETURN "LABEL_MISS_CLF_RUS_VAS"			BREAK
		CASE LABEL_MISS_CLF_RUS_SAT			RETURN "LABEL_MISS_CLF_RUS_SAT"			BREAK
		CASE LABEL_MISS_CLF_RUS_JET			RETURN "LABEL_MISS_CLF_RUS_JET"			BREAK
		CASE LABEL_MISS_CLF_RUS_CLK			RETURN "LABEL_MISS_CLF_RUS_CLK"			BREAK

		CASE LABEL_MISS_CLF_ARA_DEF			RETURN "LABEL_MISS_CLF_ARA_DEF"			BREAK
		CASE LABEL_MISS_CLF_ARA_1			RETURN "LABEL_MISS_CLF_ARA_1"			BREAK
		CASE LABEL_MISS_CLF_ARA_FAKE		RETURN "LABEL_MISS_CLF_ARA_FAKE"		BREAK	
		CASE LABEL_MISS_CLF_ARA_TNK			RETURN "LABEL_MISS_CLF_ARA_TNK"			BREAK	
		
		CASE LABEL_MISS_CLF_CAS_SET			RETURN "LABEL_MISS_CLF_CAS_SET"			BREAK
		CASE LABEL_MISS_CLF_CAS_PR1			RETURN "LABEL_MISS_CLF_CAS_PR1"			BREAK
		CASE LABEL_MISS_CLF_CAS_PR2			RETURN "LABEL_MISS_CLF_CAS_PR2"			BREAK
		CASE LABEL_MISS_CLF_CAS_PR3			RETURN "LABEL_MISS_CLF_CAS_PR3"			BREAK
		CASE LABEL_MISS_CLF_CAS_HEI			RETURN "LABEL_MISS_CLF_CAS_HEI"			BREAK	
			
		CASE LABEL_MISS_CLF_MIL_DAM			RETURN "LABEL_MISS_CLF_MIL_DAM"			BREAK	
		CASE LABEL_MISS_CLF_MIL_PLA			RETURN "LABEL_MISS_CLF_MIL_PLA"			BREAK	
		CASE LABEL_MISS_CLF_MIL_RKT			RETURN "LABEL_MISS_CLF_MIL_RKT"			BREAK		
			
		CASE LABEL_MISS_CLF_ASS_POL			RETURN "LABEL_MISS_CLF_ASS_POL"			BREAK
		CASE LABEL_MISS_CLF_ASS_RET 		RETURN "LABEL_MISS_CLF_ASS_RET"			BREAK
		CASE LABEL_MISS_CLF_ASS_CAB			RETURN "LABEL_MISS_CLF_ASS_CAB"			BREAK
		CASE LABEL_MISS_CLF_ASS_GEN			RETURN "LABEL_MISS_CLF_ASS_GEN"			BREAK
		CASE LABEL_MISS_CLF_ASS_SUB			RETURN "LABEL_MISS_CLF_ASS_SUB"			BREAK
		CASE LABEL_MISS_CLF_ASS_HEL			RETURN "LABEL_MISS_CLF_ASS_HEL"			BREAK
		CASE LABEL_MISS_CLF_ASS_VIN			RETURN "LABEL_MISS_CLF_ASS_VIN"			BREAK
		CASE LABEL_MISS_CLF_ASS_HNT			RETURN "LABEL_MISS_CLF_ASS_HNT"			BREAK
		CASE LABEL_MISS_CLF_ASS_SKY			RETURN "LABEL_MISS_CLF_ASS_SKY"			BREAK
		
		CASE LABEL_MISS_CLF_JET_1			RETURN "LABEL_MISS_CLF_JET_2"			BREAK
		CASE LABEL_MISS_CLF_JET_2			RETURN "LABEL_MISS_CLF_JET_2"			BREAK
		CASE LABEL_MISS_CLF_JET_3			RETURN "LABEL_MISS_CLF_JET_3"			BREAK
		CASE LABEL_MISS_CLF_JET_REP			RETURN "LABEL_MISS_CLF_JET_REP"			BREAK
		CASE LABEL_MISS_CLF_JET_REP_END		RETURN "LABEL_MISS_CLF_JET_REP_END" 	BREAK
			
		CASE LABEL_MISS_CLF_RC_ALEX_GRND	RETURN "LABEL_MISS_CLF_RC_ALEX_GRND"	BREAK
		CASE LABEL_MISS_CLF_RC_ALEX_AIR		RETURN "LABEL_MISS_CLF_RC_ALEX_AIR"		BREAK
		CASE LABEL_MISS_CLF_RC_ALEX_UNDW	RETURN "LABEL_MISS_CLF_RC_ALEX_UNDW"	BREAK
		CASE LABEL_MISS_CLF_RC_ALEX_RWRD	RETURN "LABEL_MISS_CLF_RC_ALEX_RWRD"	BREAK
		
		CASE LABEL_MISS_CLF_RC_MEL_MONT		RETURN "LABEL_MISS_CLF_RC_MEL_MONT"		BREAK
		CASE LABEL_MISS_CLF_RC_MEL_AIRP		RETURN "LABEL_MISS_CLF_RC_MEL_AIRP"		BREAK
		CASE LABEL_MISS_CLF_RC_MEL_WING		RETURN "LABEL_MISS_CLF_RC_MEL_WING"		BREAK
		CASE LABEL_MISS_CLF_RC_MEL_DIVE		RETURN "LABEL_MISS_CLF_RC_MEL_DIVE"		BREAK
		
		CASE LABEL_MISS_CLF_RC_AGN_BODY		RETURN "LABEL_MISS_CLF_RC_AGN_BODY"		BREAK
		CASE LABEL_MISS_CLF_RC_AGN_RCPT		RETURN "LABEL_MISS_CLF_RC_AGN_RCPT"		BREAK
		CASE LABEL_MISS_CLF_RC_AGN_ESCP		RETURN "LABEL_MISS_CLF_RC_AGN_ESCP"		BREAK
		
		CASE LABEL_MISS_CLF_RC_CLA_HACK		RETURN "LABEL_MISS_CLF_RC_CLA_HACK"		BREAK
		CASE LABEL_MISS_CLF_RC_CLA_DRUG		RETURN "LABEL_MISS_CLF_RC_CLA_DRUG"		BREAK
		CASE LABEL_MISS_CLF_RC_CLA_FIN		RETURN "LABEL_MISS_CLF_RC_CLA_FIN"		BREAK

		//Game end label.
      	CASE LABEL_GAME_END_CLF
            RETURN "LABEL_GAME_END_CLF"
        	BREAK	
			
		//Game end label.
//      	CASE LABEL_GAME_END
//            RETURN "LABEL_GAME_END"
//        	BREAK	
			
        //Base labels.
        CASE NEXT_SEQUENTIAL_COMMAND
            RETURN "NEXT_SEQUENTIAL_COMMAND"
        	BREAK			
        CASE STAY_ON_THIS_COMMAND
            RETURN "STAY_ON_THIS_COMMAND"
        	BREAK
			
		CASE MAX_LABELS_CLF 
			RETURN "MAX_LABELS_CLF"
        	BREAK
	ENDSWITCH
	
	SCRIPT_ASSERT("Get_Label_Display_String_From_Label_ID_CLF: Unknown Label ID - Bug CraigV")
    RETURN "Unknown"	
ENDFUNC
#endif
#if USE_NRM_DLC
FUNC STRING GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID_NRM(JUMP_LABEL_IDS paramLabelID)
	
	SWITCH (paramLabelID)         		     
		
		//Game end label.		
		CASE LABEL_MISS_NRM_SUR_START 	 	RETURN "LABEL_MISS_NRM_SUR_START"      	BREAK
		CASE LABEL_MISS_NRM_SUR_AMANDA		RETURN "LABEL_MISS_NRM_SUR_AMANDA"  	BREAK
		CASE LABEL_MISS_NRM_SUR_TRACEY		RETURN "LABEL_MISS_NRM_SUR_TRACEY"      BREAK
		CASE LABEL_MISS_NRM_SUR_MICHAEL		RETURN "LABEL_MISS_NRM_SUR_MICHAEL"    	BREAK
		CASE LABEL_MISS_NRM_SUR_HOME		RETURN "LABEL_MISS_NRM_SUR_HOME"    	BREAK
		CASE LABEL_MISS_NRM_SUR_JIMMY		RETURN "LABEL_MISS_NRM_SUR_JIMMY"  		BREAK
		CASE LABEL_MISS_NRM_SUR_PARTY		RETURN "LABEL_MISS_NRM_SUR_PARTY"     	BREAK
		CASE LABEL_MISS_NRM_SUR_CURE		RETURN "LABEL_MISS_NRM_SUR_CURE"      	BREAK
		
		CASE LABEL_PREMISS_NRM_SUR_START    RETURN "LABEL_PREMISS_NRM_SUR_START"  	BREAK
		CASE LABEL_PREMISS_NRM_SUR_AMANDA   RETURN "LABEL_PREMISS_NRM_SUR_AMANDA"   BREAK
		CASE LABEL_PREMISS_NRM_SUR_TRACEY   RETURN "LABEL_PREMISS_NRM_SUR_TRACEY"   BREAK
		CASE LABEL_PREMISS_NRM_SUR_MICHAEL  RETURN "LABEL_PREMISS_NRM_SUR_MICHAEL"  BREAK
		CASE LABEL_PREMISS_NRM_SUR_HOME		RETURN "LABEL_PREMISS_NRM_SUR_HOME"  	BREAK
		CASE LABEL_PREMISS_NRM_SUR_JIMMY	RETURN "LABEL_PREMISS_NRM_SUR_JIMMY"    BREAK
		CASE LABEL_PREMISS_NRM_SUR_PARTY	RETURN "LABEL_PREMISS_NRM_SUR_PARTY"  	BREAK
		CASE LABEL_PREMISS_NRM_SUR_CURE		RETURN "LABEL_PREMISS_NRM_SUR_CURE"     BREAK
		
		CASE LABEL_MISS_NRM_RESCUE_ENG      RETURN "LABEL_MISS_NRM_RESCUE_ENG"    	BREAK			
		CASE LABEL_MISS_NRM_RESCUE_MED      RETURN "LABEL_MISS_NRM_RESCUE_MED"    	BREAK			
		CASE LABEL_MISS_NRM_RESCUE_GUN      RETURN "LABEL_MISS_NRM_RESCUE_GUN"    	BREAK
		CASE LABEL_MISS_NRM_SUP_FUEL        RETURN "LABEL_MISS_NRM_SUP_FUEL"       BREAK	
		CASE LABEL_MISS_NRM_SUP_AMMO        RETURN "LABEL_MISS_NRM_SUP_AMMO"       BREAK	
		CASE LABEL_MISS_NRM_SUP_MEDS        RETURN "LABEL_MISS_NRM_SUP_MEDS"       BREAK				
		CASE LABEL_MISS_NRM_SUP_FOOD        RETURN "LABEL_MISS_NRM_SUP_FOOD"       BREAK	
		
		CASE LABEL_MISS_NRM_RADIO_A         RETURN "LABEL_MISS_NRM_RADIO_A"        	BREAK				
		CASE LABEL_MISS_NRM_RADIO_B         RETURN "LABEL_MISS_NRM_RADIO_B"        	BREAK				
		CASE LABEL_MISS_NRM_RADIO_C         RETURN "LABEL_MISS_NRM_RADIO_C"        	BREAK
		
		CASE LABEL_PREMISS_NRM_RADIO_A      RETURN "LABEL_PREMISS_NRM_RADIO_A"      BREAK				
		CASE LABEL_PREMISS_NRM_RADIO_B      RETURN "LABEL_PREMISS_NRM_RADIO_B"      BREAK				
		CASE LABEL_PREMISS_NRM_RADIO_C      RETURN "LABEL_PREMISS_NRM_RADIO_C"      BREAK
			
			
      	CASE LABEL_GAME_END_NRM
            RETURN "LABEL_GAME_END_NRM"
        	BREAK	
			
		//Game end label.
//      	CASE LABEL_GAME_END
//            RETURN "LABEL_GAME_END"
//        	BREAK	
			
        //Base labels.
        CASE NEXT_SEQUENTIAL_COMMAND
            RETURN "NEXT_SEQUENTIAL_COMMAND"
        	BREAK			
        CASE STAY_ON_THIS_COMMAND
            RETURN "STAY_ON_THIS_COMMAND"
        	BREAK
			
		CASE MAX_LABELS_NRM
			RETURN "MAX_LABELS_NRM"
        	BREAK
	ENDSWITCH
	
	SCRIPT_ASSERT("Get_Label_Display_String_From_Label_ID_NRM: Unknown Label ID - Bug CraigV")
    RETURN "Unknown"	
ENDFUNC
#endif

FUNC STRING GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(JUMP_LABEL_IDS paramLabelID)

#if USE_CLF_DLC
	return GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID_CLF(paramLabelID)
#endif 
#if USE_NRM_DLC
	return GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID_NRM(paramLabelID)
#endif 
#if not USE_CLF_DLC
#if not USE_NRM_DLC
    SWITCH (paramLabelID)   
        
		//Heist labels.
        CASE LABEL_HEIST_DOCKS_RUN_S
            RETURN "LABEL_HEIST_DOCKS_RUN_S"
			BREAK
			
		CASE LABEL_HEIST_DOCKS_SELECT_CHOICE
            RETURN "LABEL_HEIST_DOCKS_SELECT_CHOICE"
			BREAK
			
		CASE LABEL_HEIST_DOCKS_PRE_P1
			RETURN "LABEL_HEIST_DOCKS_PRE_P1"
			BREAK
			
		CASE LABEL_HEIST_DOCKS_RUN_P1
            RETURN "LABEL_HEIST_DOCKS_RUN_P1"
			BREAK
		
		CASE LABEL_HEIST_DOCKS_CHOICE
			RETURN "LABEL_HEIST_DOCKS_CHOICE"
			BREAK
			
		CASE LABEL_DOCKS_T_KNOWS_F
			RETURN "LABEL_DOCKS_T_KNOWS_F"
			BREAK	
			
		CASE LABEL_HEIST_DOCKS_ACTIVATE_B_BRANCH
            RETURN "LABEL_HEIST_DOCKS_ACTIVATE_B_BRANCH"
			BREAK
			
		CASE LABEL_HEIST_DOCKS_RUN_P2B
            RETURN "LABEL_HEIST_DOCKS_RUN_P2B"
			BREAK
			
		CASE LABEL_HEIST_DOCKS_POST_PREP
			RETURN "LABEL_HEIST_DOCKS_POST_PREP"
			BREAK
			
		CASE LABEL_HEIST_DOCKS_WAIT_FOR_CALLS
			RETURN "LABEL_HEIST_DOCKS_WAIT_FOR_CALLS"
			BREAK
			
		CASE LABEL_HEIST_DOCKS_CHOICE_JUMP2
            RETURN "LABEL_HEIST_DOCKS_CHOICE_JUMP2"
			BREAK
			
	    CASE LABEL_HEIST_DOCKS_RUN_HA
            RETURN "LABEL_HEIST_DOCKS_RUN_HA"
			BREAK
			
	    CASE LABEL_HEIST_DOCKS_RUN_HB
            RETURN "LABEL_HEIST_DOCKS_RUN_HB"
			BREAK
			
		CASE LABEL_HEIST_DOCKS_END
            RETURN "LABEL_HEIST_DOCKS_END"
			BREAK
			
           
    	CASE LABEL_HEIST_AGENCY_RUN_1
            RETURN "LABEL_HEIST_AGENCY_RUN_1"
			BREAK	
			
		CASE LABEL_HEIST_AGENCY_MISSION_2_JUMPS
            RETURN "LABEL_HEIST_AGENCY_MISSION_2_JUMPS"
			BREAK
			
		CASE LABEL_HEIST_AGENCY_WAIT_2
            RETURN "LABEL_HEIST_AGENCY_WAIT_2"
			BREAK
			
    	CASE LABEL_HEIST_AGENCY_OFFER_2
            RETURN "LABEL_HEIST_AGENCY_OFFER_2"
			BREAK
			
		CASE LABEL_HEIST_AGENCY_BOARD_OVERVIEW
			RETURN "LABEL_HEIST_AGENCY_BOARD_OVERVIEW"
			BREAK
			
		CASE LABEL_HEIST_AGENCY_SELECT_CHOICE
            RETURN "LABEL_HEIST_AGENCY_SELECT_CHOICE"
			BREAK
			
        CASE LABEL_HEIST_AGENCY_SELECT_CREW
            RETURN "LABEL_HEIST_AGENCY_SELECT_CREW"
			BREAK
			
		CASE LABEL_HEIST_AGENCY_RUN_P1
			RETURN "LABEL_HEIST_AGENCY_RUN_P1"
			BREAK
			
		CASE LABEL_HEIST_AGENCY_PREP_WANTED_CALLS
			RETURN "LABEL_HEIST_AGENCY_P_WANT_CALLS"
			BREAK
			
		CASE LABEL_HEIST_AGENCY_PRE_FINALE_CALLS
			RETURN "LABEL_HEIST_AGENCY_P_WANT_CALLS"
			BREAK
			
    	CASE LABEL_HEIST_AGENCY_RUN_3A
            RETURN "LABEL_HEIST_AGENCY_RUN_3A"
			BREAK
			
		CASE LABEL_HEIST_AGENCY_PRE_3B
            RETURN "LABEL_HEIST_AGENCY_PRE_3B"
			BREAK
			
    	CASE LABEL_HEIST_AGENCY_RUN_3B
            RETURN "LABEL_HEIST_AGENCY_RUN_3B"
			BREAK
			
    	CASE LABEL_HEIST_AGENCY_END
            RETURN "LABEL_HEIST_AGENCY_END"
			BREAK
			
		CASE LABEL_HEIST_AGENCY_SKIP_RICKIE_CALL
			RETURN "LABEL_HEIST_AGENCY_SKIP_RICK_C"
			BREAK
			
			
        CASE LABEL_HEIST_FINALE_1
            RETURN "LABEL_HEIST_FINALE_1"
			BREAK
			
		CASE LABEL_HEIST_FINALE_2I
			RETURN "LABEL_HEIST_FINALE_2I"
			BREAK
			
		CASE LABEL_HEIST_FINALE_BOARD_OVERVIEW
			RETURN "LABEL_HEIST_FINALE_BOARD_OVERVIEW"
			BREAK
			
        CASE LABEL_HEIST_FINALE_SELECT_CHOICE
            RETURN "LABEL_HEIST_FINALE_SELECT_CHOICE"
			BREAK
			
        CASE LABEL_HEIST_FINALE_SELECT_CREW
            RETURN "LABEL_HEIST_FINALE_SELECT_CREW"
			BREAK
			
        CASE LABEL_HEIST_FINALE_CHOICE_JUMP
            RETURN "LABEL_HEIST_FINALE_CHOICE_JUMP"
			BREAK
			
		CASE LABEL_HEIST_FINALE_PA_TEXT
            RETURN "LABEL_HEIST_FINALE_PA_TEXT"
			BREAK
			
		CASE LABEL_HEIST_FINALE_PA
            RETURN "LABEL_HEIST_FINALE_PA"
			BREAK
			
		CASE LABEL_HEIST_FINALE_PA_WANTED_CALLS
			RETURN "LABEL_HEIST_FINALE_PA_WANT_CALLS"
			BREAK
			
		CASE LABEL_HEIST_FINALE_PB_TEXT
            RETURN "LABEL_HEIST_FINALE_PB_TEXT"
			BREAK
			
		CASE LABEL_HEIST_FINALE_PB
            RETURN "LABEL_HEIST_FINALE_PB"
			BREAK
			
		CASE LABEL_HEIST_FINALE_PB_WANTED_CALLS
			RETURN "LABEL_HEIST_FINALE_PB_WANT_CALLS"
			BREAK
			
		CASE LABEL_HEIST_FINALE_PC_EMAIL
            RETURN "LABEL_HEIST_FINALE_PC_EMAIL"
			BREAK
			
		CASE LABEL_HEIST_FINALE_PC1
            RETURN "LABEL_HEIST_FINALE_PC1"
			BREAK
			
		CASE LABEL_HEIST_FINALE_PC2
            RETURN "LABEL_HEIST_FINALE_PC2"
			BREAK
			
		CASE LABEL_HEIST_FINALE_PC3
            RETURN "LABEL_HEIST_FINALE_PC3"
			BREAK
			
		CASE LABEL_HEIST_FINALE_PD_TEXT
            RETURN "LABEL_HEIST_FINALE_PD_TEXT"
			BREAK
			
		CASE LABEL_HEIST_FINALE_PD
            RETURN "LABEL_HEIST_FINALE_PD"
			BREAK
			
		CASE LABEL_HEIST_FINALE_2_TERMINATE
            RETURN "LABEL_HEIST_FINALE_2_TERMINATE"
			BREAK
			
        CASE LABEL_HEIST_FINALE_2A
            RETURN "LABEL_HEIST_FINALE_2A"
			BREAK
			
        CASE LABEL_HEIST_FINALE_2B
            RETURN "LABEL_HEIST_FINALE_2B"
			BREAK
			
        CASE LABEL_HEIST_FINALE_END
            RETURN "LABEL_HEIST_FINALE_END"
			BREAK
			
            
        CASE LABEL_HEIST_JEWEL_INITIAL_CALL
            RETURN "LABEL_HEIST_JEWEL_INITIAL_CALL"
			BREAK
			
        CASE LABEL_HEIST_JEWEL_RUN_1
            RETURN "LABEL_HEIST_JEWEL_RUN_1"
			BREAK
			
		CASE LABEL_HEIST_JEWEL_BOARD_OVERVIEW
			RETURN "LABEL_HEIST_JEWEL_BOARD_OVERVIEW"
			BREAK
			
        CASE LABEL_HEIST_JEWEL_SELECT_CHOICE
            RETURN "LABEL_HEIST_JEWEL_SELECT_CHOICE"
			BREAK
			
        CASE LABEL_HEIST_JEWEL_SELECT_CREW
            RETURN "LABEL_HEIST_JEWEL_SELECT_CREW"
			BREAK
			
		CASE LABEL_HEIST_JEWEL_RUN_PREP_1A
            RETURN "LABEL_HEIST_JEWEL_RUN_PREP_1A"
			BREAK
			
		CASE LABEL_HEIST_JEWEL_CALL_P1A_UNLOCK_P2A
			RETURN "LABEL_HEIST_JEWEL_C_P1A_U_P2A"
			BREAK
			
		CASE LABEL_HEIST_JEWEL_CALL_P1A_PREPS_DONE
			RETURN "LABEL_HEIST_JEWEL_CALL_P1A_D"
			BREAK
			
		CASE LABEL_HEIST_JEWEL_RUN_PREP_2A
            RETURN "LABEL_HEIST_JEWEL_RUN_PREP_2A"
			BREAK
			
		CASE LABEL_HEIST_JEWEL_CALL_P2A_PREPS_DONE
			RETURN "LABEL_HEIST_JEWEL_CALL_P2A_D"
			BREAK
			
		CASE LABEL_HEIST_JEWEL_TEXT_PREP_1B
			RETURN "LABEL_HEIST_JEWEL_TEXT_PREP_1B"
			BREAK
			
		CASE LABEL_HEIST_JEWEL_RUN_PREP_1B
            RETURN "LABEL_HEIST_JEWEL_RUN_PREP_1B"
			BREAK
			
		CASE LABEL_HEIST_JEWEL_THIRD_CALL
            RETURN "LABEL_HEIST_JEWEL_THIRD_CALL"
			BREAK
			
        CASE LABEL_HEIST_JEWEL_SETUP
            RETURN "LABEL_HEIST_JEWEL_SETUP"
			BREAK
			
        CASE LABEL_HEIST_JEWEL_RUN_2
            RETURN "LABEL_HEIST_JEWEL_RUN_2"
			BREAK
			

        CASE LABEL_HEIST_RURAL_SELECT_CREW
            RETURN "LABEL_HEIST_RURAL_SELECT_CREW"
			BREAK
			
        CASE LABEL_HEIST_RURAL_RUN_S
            RETURN "LABEL_HEIST_RURAL_RUN_S"
			BREAK
			
		CASE LABEL_HEIST_RURAL_RUN_P1
			RETURN "LABEL_HEIST_RURAL_RUN_P1"
			BREAK
			
        CASE LABEL_HEIST_RURAL_RUN_H
            RETURN "LABEL_HEIST_RURAL_RUN_H"
			BREAK
			
		CASE LABEL_HEIST_RURAL_SKIP_RICKIE_CALL
			RETURN "LABEL_HEIST_RURAL_SKIP_RICK_C"
			BREAK
			
        
		//Mission labels.
        CASE LABEL_MISS_ARMENIAN_1
            RETURN "LABEL_MISS_ARMENIAN_1"
			BREAK
			
        CASE LABEL_MISS_ARMENIAN_CAR_FINE
            RETURN "LABEL_MISS_ARMENIAN_CAR_FINE"
			BREAK
			
		CASE LABEL_MISS_ARMENIAN_1_CALLS_DONE
			RETURN "LABEL_MISS_ARMENIAN_1_CALLS_DONE"
			BREAK
			
        CASE LABEL_MISS_ARMENIAN_2
            RETURN "LABEL_MISS_ARMENIAN_2"
			BREAK
			
		CASE LABEL_SKIP_ARM2_STRETCH_TEXT
            RETURN "LABEL_SKIP_ARM2_STRETCH_TEXT"
			BREAK
			
        CASE LABEL_MISS_ARMENIAN_3
            RETURN "LABEL_MISS_ARMENIAN_3"
			BREAK
			
			
		CASE LABEL_MISS_ASSASSIN_1
            RETURN "LABEL_MISS_ASSASSIN_1"
			BREAK
			
		CASE LABEL_MISS_ASSASSIN_2
            RETURN "LABEL_MISS_ASSASSIN_2"
			BREAK
			
		CASE LABEL_MISS_ASSASSIN_3
            RETURN "LABEL_MISS_ASSASSIN_3"
			BREAK
			
		CASE LABEL_MISS_ASSASSIN_4
            RETURN "LABEL_MISS_ASSASSIN_4"
			BREAK
			
		CASE LABEL_MISS_ASSASSIN_5
            RETURN "LABEL_MISS_ASSASSIN_5"
			BREAK
			
        CASE LABEL_MISS_CARSTEAL_1
            RETURN "LABEL_MISS_CARSTEAL_1"
			BREAK
			
		CASE LABEL_MISS_CARSTEAL_2
            RETURN "LABEL_MISS_CARSTEAL_2"
			BREAK
			
		CASE LABEL_MISS_CARSTEAL_3
            RETURN "LABEL_MISS_CARSTEAL_3"
			BREAK
		
		CASE LABEL_MISS_CARSTEAL_4
            RETURN "LABEL_MISS_CARSTEAL_4"
			BREAK
			
			
        CASE LABEL_MISS_CHINESE_1
            RETURN "LABEL_MISS_CHINESE_1"
			BREAK
			
        CASE LABEL_MISS_CHINESE_2
            RETURN "LABEL_MISS_CHINESE_2"
			BREAK
			
            
        CASE LABEL_MISS_EXILE_1
            RETURN "LABEL_MISS_EXILE_1"
			BREAK
			
        CASE LABEL_MISS_EXILE_2
            RETURN "LABEL_MISS_EXILE_2"
			BREAK
			
        CASE LABEL_MISS_EXILE_3
            RETURN "LABEL_MISS_EXILE_3"
			BREAK
            
			
        CASE LABEL_MISS_FAMILY_1
            RETURN "LABEL_MISS_FAMILY_1"
			BREAK

        CASE LABEL_MISS_FAMILY_2
            RETURN "LABEL_MISS_FAMILY_2"
			BREAK
		
		CASE LABEL_SHOOT_RANGE_DONE	
			RETURN "LABEL_SHOOT_RANGE_DONE"
			BREAK
			
        CASE LABEL_MISS_FAMILY_3
            RETURN "LABEL_MISS_FAMILY_3"
			BREAK
			
        CASE LABEL_MISS_FAMILY_4
            RETURN "LABEL_MISS_FAMILY_4"
			BREAK
			
        CASE LABEL_MISS_FAMILY_5
            RETURN "LABEL_MISS_FAMILY_5"
			BREAK
			
        CASE LABEL_MISS_FAMILY_6
            RETURN "LABEL_MISS_FAMILY_6"
			BREAK
			
		CASE LABEL_FAM6_AH2_HELP_JUMP
			RETURN "LABEL_FAM6_AH2_HELP_JUMP"
			BREAK
			
		CASE LABEL_FAM6_SKIP_SHRINK_4_JUMP	
			RETURN "LABEL_FAM6_SKIP_SHRINK_4_JUMP"
			BREAK
			
        CASE LABEL_MISS_FBI_OFFICERS_1
            RETURN "LABEL_MISS_FBI_OFFICERS_1"
			BREAK
			
        CASE LABEL_MISS_FBI_OFFICERS_2
            RETURN "LABEL_MISS_FBI_OFFICERS_2"
			BREAK
			
        CASE LABEL_MISS_FBI_OFFICERS_3_CALLS
            RETURN "LABEL_MISS_FBI_OFFICERS_3_CALLS"
			BREAK
			
        CASE LABEL_MISS_FBI_OFFICERS_3
            RETURN "LABEL_MISS_FBI_OFFICERS_3"
			BREAK
			
		CASE LABEL_MISS_FBI_4_INTRO
			RETURN "LABEL_MISS_FBI_4_INTRO"
			BREAK
			
		CASE LABEL_MISS_FBI_4_PREP_1
			RETURN "LABEL_MISS_FBI_4_PREP_1"
			BREAK
			
		CASE LABEL_MISS_FBI_4_PREP_2
			RETURN "LABEL_MISS_FBI_4_PREP_2"
			BREAK
			
		CASE LABEL_MISS_FBI_4_PREP_3
			RETURN "LABEL_MISS_FBI_4_PREP_3"
			BREAK
			
		CASE LABEL_MISS_FBI_4_PREP_4
			RETURN "LABEL_MISS_FBI_4_PREP_4"
			BREAK
			
		CASE LABEL_MISS_FBI_4_PREP_5
			RETURN "LABEL_MISS_FBI_4_PREP_5"
			BREAK
				
		CASE LABEL_MISS_FBI_4_SETUP
			RETURN "LABEL_MISS_FBI_4_SETUP"
			BREAK
			
		CASE LABEL_MISS_FBI_4_SKIP_CALLS
			RETURN "LABEL_MISS_FBI_4_SKIP_CALLS"
			BREAK
			
        CASE LABEL_MISS_FBI_OFFICERS_4
            RETURN "LABEL_MISS_FBI_OFFICERS_4"
			BREAK
			
        CASE LABEL_MISS_FBI_OFFICERS_5
            RETURN "LABEL_MISS_FBI_OFFICERS_5"
			BREAK
			
			
		CASE LABEL_MISS_FINALE_CUTSCENE_1
            RETURN "LABEL_MISS_FINALE_CUTSCENE_1"
			BREAK
			
		CASE LABEL_MISS_FINALE_A
            RETURN "LABEL_MISS_FINALE_A"
			BREAK
			
		CASE LABEL_MISS_FINALE_B
            RETURN "LABEL_MISS_FINALE_B"
			BREAK

		CASE LABEL_MISS_FINALE_C1
            RETURN "LABEL_MISS_FINALE_C1"
			BREAK
			
		CASE LABEL_MISS_FINALE_C2
            RETURN "LABEL_MISS_FINALE_C2"
			BREAK
			
		CASE LABEL_MISS_FINALE_CREDITS
            RETURN "LABEL_MISS_FINALE_CREDITS"
			BREAK

		CASE LABEL_MISS_FINALE_END
			RETURN "LABEL_MISS_FINALE_END"
			BREAK
			
		CASE LABEL_MISS_FINALE_TREV_KILLED_CALLS
	      	RETURN "LABEL_MISS_FINALE_TREV_KILLED_CALLS"
	        BREAK
			
		CASE LABEL_MISS_FINALE_TM_KILLED_CALLS
			RETURN "LABEL_MISS_FINALE_TM_KILLED_CALLS"
			BREAK
			
		CASE LABEL_FINALE_TREVOR_MUM_CHECK
			RETURN "LABEL_FINALE_TREVOR_MUM_CHECK"
			BREAK
			
        CASE LABEL_MISS_FRANKLIN_0
            RETURN "LABEL_MISS_FRANKLIN_0"
			BREAK
			
		CASE LABEL_SKIP_FRAN0_STRETCH_TEXT
            RETURN "LABEL_SKIP_FRAN0_STRETCH_TEXT"
			BREAK
			
        CASE LABEL_MISS_FRANKLIN_1
            RETURN "LABEL_MISS_FRANKLIN_1"
			BREAK
			
        CASE LABEL_MISS_FRANKLIN_2
            RETURN "LABEL_MISS_FRANKLIN_2"
			BREAK
					
		CASE LABEL_MISS_LAMAR_1_HOLD
            RETURN "LABEL_MISS_LAMAR_1_HOLD"
			BREAK	
		
        CASE LABEL_MISS_LAMAR_1
            RETURN "LABEL_MISS_LAMAR_1"
			BREAK
            
        CASE LABEL_MISS_LESTER_1
            RETURN "LABEL_MISS_LESTER_1"
			BREAK
			
		CASE LABEL_MISS_MARTIN_1
			RETURN "LABEL_MISS_MARTIN_1"
			BREAK
			
        CASE LABEL_MISS_MICHAEL_1
            RETURN "LABEL_MISS_MICHAEL_1"
			BREAK
			
        CASE LABEL_MISS_MICHAEL_2
            RETURN "LABEL_MISS_MICHAEL_2"
			BREAK
			
		CASE LABEL_MISS_MICHAEL_3
            RETURN "LABEL_MISS_MICHAEL_3"
			BREAK
			
		CASE LABEL_MISS_MICHAEL_4
            RETURN "LABEL_MISS_MICHAEL_4"
			BREAK
			
			
		CASE LABEL_MISS_MIKE_EVT_PRE_AMANDA_1
			RETURN "LABEL_MISS_MIKE_EVT_PRE_AMANDA_1"
			BREAK
			
		CASE LABEL_MISS_MIKE_EVT_AMANDA_1
			RETURN "LABEL_MISS_MIKE_EVT_AMANDA_1"
			BREAK
			
		CASE LABEL_MISS_MIKE_EVT_AMANDA_PASS
			RETURN "LABEL_MISS_MIKE_EVT_AMANDA_PASS"
			BREAK
			
		CASE LABEL_MISS_MIKE_EVT_AMANDA_FAIL
			RETURN "LABEL_MISS_MIKE_EVT_AMANDA_FAIL"
			BREAK
			
		CASE LABEL_MISS_MIKE_EVT_PRE_TRACEY_1
			RETURN "LABEL_MISS_MIKE_EVT_PRE_TRACEY_1"
			BREAK
			
		CASE LABEL_MISS_MIKE_EVT_TRACEY_1
			RETURN "LABEL_MISS_MIKE_EVT_TRACEY_1"
			BREAK
			
		CASE LABEL_MISS_MIKE_EVT_TRACEY_FAIL
			RETURN "LABEL_MISS_MIKE_EVT_TRACEY_FAIL"
			BREAK
			
		CASE LABEL_MISS_MIKE_EVT_TRACEY_PASS
			RETURN "LABEL_MISS_MIKE_EVT_TRACEY_PASS"
			BREAK
			
		CASE LABEL_MISS_MIKE_EVT_PRE_JIMMY_1
			RETURN "LABEL_MISS_MIKE_EVT_PRE_JIMMY_1"
			BREAK

		CASE LABEL_MISS_MIKE_EVT_JIMMY_1
			RETURN "LABEL_MISS_MIKE_EVT_JIMMY_1"
			BREAK
			
		CASE LABEL_MISS_MIKE_EVT_JIMMY_FAIL
			RETURN "LABEL_MISS_MIKE_EVT_JIMMY_FAIL"
			BREAK

		CASE LABEL_MISS_MIKE_EVT_END
			RETURN "LABEL_MISS_MIKE_EVT_END"
			BREAK
			
        
        CASE LABEL_MISS_PROLOGUE
            RETURN "LABEL_MISS_PROLOGUE"
			BREAK
			
			
		CASE LABEL_MISS_SHRINK_1
			RETURN "LABEL_MISS_SHRINK_1"
			BREAK
			
		CASE LABEL_MISS_SHRINK_2
			RETURN "LABEL_MISS_SHRINK_2"
			BREAK
			
		CASE LABEL_MISS_SHRINK_3_TEXT
			RETURN "LABEL_MISS_SHRINK_3_TEXT"
			BREAK
			
		CASE LABEL_MISS_SHRINK_3
			RETURN "LABEL_MISS_SHRINK_3"
			BREAK
			
		CASE LABEL_MISS_SHRINK_4_TEXT
			RETURN "LABEL_MISS_SHRINK_4_TEXT"
			BREAK
			
		CASE LABEL_MISS_SHRINK_4
			RETURN "LABEL_MISS_SHRINK_4"
			BREAK
			
		CASE LABEL_MISS_SHRINK_4_SKIPPED	
			RETURN "LABEL_MISS_SHRINK_4_SKIPPED"
			BREAK
			
		CASE LABEL_MISS_SHRINK_5
			RETURN "LABEL_MISS_SHRINK_5"
			BREAK
			
		CASE LABEL_MISS_SHRINK_KILLED
			RETURN "LABEL_MISS_SHRINK_KILLED"
			BREAK
            
        CASE LABEL_MISS_SOLOMON_1
            RETURN "LABEL_MISS_SOLOMON_1"
			BREAK
			
		CASE LABEL_MISS_SOLOMON_2
            RETURN "LABEL_MISS_SOLOMON_2"
			BREAK
			
		CASE LABEL_MISS_SOLOMON_3
            RETURN "LABEL_MISS_SOLOMON_3"
			BREAK
			

        CASE LABEL_MISS_TREVOR_1
            RETURN "LABEL_MISS_TREVOR_1"
			BREAK
			
        CASE LABEL_MISS_TREVOR_2
            RETURN "LABEL_MISS_TREVOR_2"
			BREAK
			
        CASE LABEL_MISS_TREVOR_3
            RETURN "LABEL_MISS_TREVOR_3"
			BREAK
			
		CASE LABEL_MISS_TREVOR_4
            RETURN "LABEL_MISS_TREVOR_4"
			BREAK
			
			
		//Minigame labels.
		CASE LABEL_MINIGAME_ASSASSINATIONS
			RETURN "LABEL_MINIGAME_ASSASSINATIONS"
			BREAK
			
		CASE LABEL_MINIGAME_COUNTRY_RACE
			RETURN "LABEL_MINIGAME_COUNTRY_RACE"
			BREAK
			
		CASE LABEL_MINIGAME_DARTS
			RETURN "LABEL_MINIGAME_DARTS"
			BREAK
			
		CASE LABEL_MINIGAME_DARTS2
			RETURN "LABEL_MINIGAME_DARTS2"
			BREAK
			
		CASE LABEL_MINIGAME_BASEJUMPING
			RETURN "LABEL_MINIGAME_BASEJUMPING"
			BREAK
			
		CASE LABEL_MINIGAME_DRUG_TRAFFICKING
            RETURN "LABEL_MINIGAME_DRUG_TRAFFICKING"
			BREAK
			
		CASE LABEL_MINIGAME_GOLF
            RETURN "LABEL_MINIGAME_GOLF"
			BREAK
			
		CASE LABEL_MINIGAME_HUNTING
            RETURN "LABEL_MINIGAME_HUNTING"
			BREAK
			
		CASE LABEL_MINIGAME_OFFROAD_RACES_TREVOR
            RETURN "LABEL_MINIGAME_OFFROAD_RACES_TREVOR"
			BREAK
			
		CASE LABEL_MINIGAME_OFFROAD_RACES_ALL
            RETURN "LABEL_MINIGAME_OFFROAD_RACES_ALL"
			BREAK
			
		CASE LABEL_MINIGAME_PILOT_SCHOOL
            RETURN "LABEL_MINIGAME_PILOT_SCHOOL"
			BREAK
			
		CASE LABEL_MINIGAME_RAMPAGES
            RETURN "LABEL_MINIGAME_RAMPAGES"
			BREAK
			
		CASE LABEL_MINIGAME_SHOOTING_RANGE
            RETURN "LABEL_MINIGAME_SHOOTING_RANGE"
			BREAK
			
		CASE LABEL_MINIGAME_SHRINK
        	RETURN "LABEL_MINIGAME_SHRINK"
			BREAK
			
		CASE LABEL_MINIGAME_SEA_RACES
			RETURN "LABEL_MINIGAME_SEA_RACES"
			BREAK
			
		CASE LABEL_MINIGAME_STREET_RACES
			RETURN "LABEL_MINIGAME_STREET_RACES"
			BREAK
			
		CASE LABEL_MINIGAME_STUNT_PLANE
            RETURN "LABEL_MINIGAME_STUNT_PLANE"
			BREAK
			
		CASE LABEL_MINIGAME_TAXI
			RETURN "LABEL_MINIGAME_TAXI"
			BREAK
			
		CASE LABEL_MINIGAME_TENNIS
            RETURN "LABEL_MINIGAME_TENNIS"
			BREAK
			
		CASE LABEL_MINIGAME_TOWING
            RETURN "LABEL_MINIGAME_TOWING"
			BREAK
			
		
			
		DEFAULT 
			SWITCH (paramLabelID) 
				CASE LABEL_MINIGAME_TRIATHLON
		            RETURN "LABEL_MINIGAME_TRIATHLON"
					BREAK
					
				CASE LABEL_MINIGAME_YOGA
					RETURN "LABEL_MINIGAME_YOGA"
					BREAK
					
				//Shop labels.
				CASE LABEL_SHOP_BARBER_HIGH
		            RETURN "LABEL_SHOP_BARBER_HIGH"
		        	BREAK
					
				CASE LABEL_SHOP_BARBER_LOW
		            RETURN "LABEL_SHOP_BARBER_LOW"
		        	BREAK
					
				CASE LABEL_SHOP_CLOTHES_LOW
		            RETURN "LABEL_SHOP_CLOTHES_LOW"
		        	BREAK
					
				CASE LABEL_SHOP_CLOTHES_MID
		            RETURN "LABEL_SHOP_CLOTHES_MID"
		        	BREAK
					
				CASE LABEL_SHOP_CLOTHES_HIGH
		            RETURN "LABEL_SHOP_CLOTHES_HIGH"
		        	BREAK
					
				CASE LABEL_SHOP_TATTOO_PARLOUR
		            RETURN "LABEL_SHOP_TATTOO_PARLOUR"
		        	BREAK
					
				CASE LABEL_SHOP_GUN_SHOP
		            RETURN "LABEL_SHOP_GUN_SHOP"
		        	BREAK
					
				CASE LABEL_SHOP_CARMOD_SHOP
		            RETURN "LABEL_SHOP_CARMOD_SHOP"
		        	BREAK
				
				//Random Character labels.
				CASE LABEL_RC_ABIGAIL_1
		            RETURN "LABEL_RC_ABIGAIL_1"
		        	BREAK	
							
				CASE LABEL_RC_BARRY_1
		            RETURN "LABEL_RC_BARRY_1"
		        	BREAK
					
				CASE LABEL_RC_BARRY_2
		            RETURN "LABEL_RC_BARRY_2"
		        	BREAK
					
				CASE LABEL_RC_BARRY_3
		            RETURN "LABEL_RC_BARRY_3"
		        	BREAK
					
				CASE LABEL_RC_DREYFUSS_1
		            RETURN "LABEL_RC_DREYFUSS_1"
		        	BREAK
				
				CASE LABEL_RC_EPSILON_1
		            RETURN "LABEL_RC_EPSILON_1"
		        	BREAK

				CASE LABEL_RC_EXTREME_1
		            RETURN "LABEL_RC_EXTREME_1"
		        	BREAK
					
				CASE LABEL_RC_FANATIC_1
		            RETURN "LABEL_RC_FANATIC_1"
		        	BREAK
					
				CASE LABEL_RC_FANATIC_2
		            RETURN "LABEL_RC_FANATIC_2"
		        	BREAK
					
				CASE LABEL_RC_FANATIC_3
		            RETURN "LABEL_RC_FANATIC_3"
		        	BREAK
				
				CASE LABEL_RC_HAO_1
		            RETURN "LABEL_RC_HAO_1"
		        	BREAK
					
				CASE LABEL_RC_HUNTING_1
		            RETURN "LABEL_RC_HUNTING_1"
		        	BREAK
					
				CASE LABEL_RC_JOSH_1
		            RETURN "LABEL_RC_JOSH_1"
		        	BREAK
				
				CASE LABEL_RC_MAUDE_1
		            RETURN "LABEL_RC_MAUDE_1"
		        	BREAK	
					
				CASE LABEL_RC_MINUTE_1
		            RETURN "LABEL_RC_MINUTE_1"
		        	BREAK

				CASE LABEL_RC_MRS_PHILIPS_1
		            RETURN "LABEL_RC_MRS_PHILIPS_1"
		        	BREAK
					
				CASE LABEL_RC_NIGEL_1
		            RETURN "LABEL_RC_NIGEL_1"
		        	BREAK
					
				CASE LABEL_RC_OMEGA_1
		            RETURN "LABEL_RC_OMEGA_1"
		        	BREAK
				
				CASE LABEL_RC_PAPARAZZO_1
		            RETURN "LABEL_RC_PAPARAZZO_1"
		        	BREAK
					
				CASE LABEL_RC_PAPARAZZO_3
		            RETURN "LABEL_RC_PAPARAZZO_3"
		        	BREAK
				
				CASE LABEL_RC_RAMPAGE_1
		            RETURN "LABEL_RC_RAMPAGE_1"
		        	BREAK
				
				CASE LABEL_RC_RAMPAGE_3
		            RETURN "LABEL_RC_RAMPAGE_3"
		        	BREAK	
			
				CASE LABEL_RC_PAPARAZZO_4
		            RETURN "LABEL_RC_PAPARAZZO_4"
		        	BREAK
					
				CASE LABEL_RC_THELASTONE
		            RETURN "LABEL_RC_THELASTONE"
		        	BREAK
					
				CASE LABEL_RC_TONYA_1
		            RETURN "LABEL_RC_TONYA_1"
		        	BREAK
					
				//Random Event labels.
				CASE LABEL_RE_BATCH_POST_TONYA1
		            RETURN "LABEL_RE_BATCH_POST_TONYA1"
		        	BREAK
					
				CASE LABEL_RE_BATCH_POST_LAMAR1
					RETURN "LABEL_RE_BATCH_POST_LAMAR1"
					BREAK
					
				CASE LABEL_RE_BATCH_POST_JEWEL_SETUP
					RETURN "LABEL_RE_BATCH_POST_JEWEL_SETUP"
					BREAK
					
				CASE LABEL_RE_BATCH_POST_TREV1
		            RETURN "LABEL_RE_BATCH_POST_TREV1"
		        	BREAK
					
				CASE LABEL_RE_BATCH_POST_TREV2
		            RETURN "LABEL_RE_BATCH_POST_TREV2"
		        	BREAK
					
		    	CASE LABEL_RE_BATCH_POST_TREV3
		            RETURN "LABEL_RE_BATCH_POST_TREV3"
		        	BREAK
					
				CASE LABEL_RE_YETARIAN
					RETURN "LABEL_RE_YETARIAN"
					BREAK
					
				CASE LABEL_RE_POST_SOLOMON3
					RETURN "LABEL_RE_POST_SOLOMON3"
					BREAK
					
					
				//Misc labels.
				CASE LABEL_SHOP_ROBBERIES
					RETURN "LABEL_SHOP_ROBBERIES"
					BREAK
					
				CASE LABEL_BAIL_BONDS
					RETURN "LABEL_BAIL_BONDS"
					BREAK

				CASE LABEL_CARWASH
					RETURN "LABEL_CARWASH"
					BREAK
					
				CASE LABEL_WILDLIFE_PHOTOGRAPHY
					RETURN "LABEL_WILDLIFE_PHOTOGRAPHY"
					BREAK
					
				CASE LABEL_MURDER_MYSTERY_P1
					RETURN "LABEL_MURDER_MYSTERY_P1"
					BREAK
					
				CASE LABEL_MURDER_MYSTERY_P2
					RETURN "LABEL_MURDER_MYSTERY_P2"
					BREAK
					
				CASE LABEL_MURDER_MYSTERY_P3
					RETURN "LABEL_MURDER_MYSTERY_P3"
					BREAK
					
				CASE LABEL_MURDER_MYSTERY_P4
					RETURN "LABEL_MURDER_MYSTERY_P4"
					BREAK

				CASE LABEL_MURDER_MYSTERY_P5
					RETURN "LABEL_MURDER_MYSTERY_P5"
					BREAK

				CASE LABEL_MURDER_MYSTERY_P6
					RETURN "LABEL_MURDER_MYSTERY_P6"
					BREAK

				CASE LABEL_MURDER_MYSTERY_P7
					RETURN "LABEL_MURDER_MYSTERY_P7"
					BREAK

				//Chop
				CASE LABEL_CHOP_UNLOCK_SC
					RETURN "LABEL_CHOP_UNLOCK_SC"
					BREAK
					
				CASE LABEL_CHOP_UNLOCK_VH
					RETURN "LABEL_CHOP_UNLOCK_VH"
					BREAK
					
				//DLC Labels.
				CASE LABEL_MISS_DLC_ASSASSIN_START
					RETURN "LABEL_MISS_DLC_ASSASSIN_START"
					BREAK
				
				CASE LABEL_MISS_DLC_ASSASSIN_QUICK_START
					RETURN "LABEL_MISS_DLC_ASSASSIN_QUICK_START"
					BREAK
				
				CASE LABEL_MISS_DLC_ASSASSIN_POL
					RETURN "LABEL_MISS_DLC_ASSASSIN_POL"
					BREAK
					
				CASE LABEL_MISS_DLC_ASSASSIN_RET
					RETURN "LABEL_MISS_DLC_ASSASSIN_RET"
					BREAK

				CASE LABEL_MISS_DLC_ASSASSIN_CAB
					RETURN "LABEL_MISS_DLC_ASSASSIN_CAB"
					BREAK

				CASE LABEL_MISS_DLC_ASSASSIN_GEN
					RETURN "LABEL_MISS_DLC_ASSASSIN_GEN"
					BREAK

				CASE LABEL_MISS_DLC_ASSASSIN_SUB
					RETURN "LABEL_MISS_DLC_ASSASSIN_SUB"
					BREAK

				CASE LABEL_MISS_DLC_ASSASSIN_DEA
					RETURN "LABEL_MISS_DLC_ASSASSIN_DEA"
					BREAK

				CASE LABEL_MISS_DLC_ASSASSIN_HEL
					RETURN "LABEL_MISS_DLC_ASSASSIN_HEL"
					BREAK

				CASE LABEL_MISS_DLC_ASSASSIN_BAR
					RETURN "LABEL_MISS_DLC_ASSASSIN_BAR"
					BREAK

				CASE LABEL_MISS_DLC_ASSASSIN_STR
					RETURN "LABEL_MISS_DLC_ASSASSIN_STR"
					BREAK

				CASE LABEL_MISS_DLC_ASSASSIN_VIN
					RETURN "LABEL_MISS_DLC_ASSASSIN_VIN"
					BREAK
					
				CASE LABEL_MISS_DLC_ASSASSIN_HNT
					RETURN "LABEL_MISS_DLC_ASSASSIN_HNT"
					BREAK

				CASE LABEL_MISS_DLC_ASSASSIN_SKY
					RETURN "LABEL_MISS_DLC_ASSASSIN_SKY"
					BREAK
					
				//Game end label.
		      	CASE LABEL_GAME_END
		            RETURN "LABEL_GAME_END"
		        	BREAK
									
		        //Base labels.
		        CASE NEXT_SEQUENTIAL_COMMAND
		            RETURN "NEXT_SEQUENTIAL_COMMAND"
		        	BREAK
					
		        CASE STAY_ON_THIS_COMMAND
		            RETURN "STAY_ON_THIS_COMMAND"
		        	BREAK
			ENDSWITCH
		break
    ENDSWITCH
    
    SCRIPT_ASSERT("Get_Label_Display_String_From_Label_ID: Unknown Label ID - Bug BenR")
    RETURN "Unknown"
#endif
#endif
ENDFUNC




// -----------------------------------------------------------------------------------------------------------
//      Mission Flow Standard Debug Output Synchronisation Strings
// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Return the sync as a string based on the synchronisationID
//
// INPUT PARAMS:    paramSyncID             Synchronisation ID
// RETURN VALUE:    STRING                  Debug display string for the syncID
#if USE_CLF_DLC
FUNC STRING GET_SYNCHRONISATION_DISPLAY_STRING_FROM_SYNC_ID_CLF(SYNCHRONIZATION_IDS paramSyncID)

    SWITCH (paramSyncID)
	
		CASE SYNC_IAA_FINALE
            RETURN "SYNC_IAA_FINALE "
			BREAK	
		
		CASE SYNC_IAA_RTS
            RETURN "SYNC_IAA_RTS"
			BREAK	
			
		CASE SYNC_KOR_FINALE
            RETURN "SYNC_KOR_FINALE"
			BREAK	
			
		CASE SYNC_ARA_FINALE
            RETURN "SYNC_ARA_FINALE"
			BREAK	
			
		CASE SYNC_A14_FINALE
            RETURN "SYNC_A14_FINALE"
			BREAK	
			
		CASE SYNC_KOR_PROTECT
            RETURN "SYNC_KOR_PROTECT"
			BREAK	
			
		CASE SYNC_KOR_SUBBASE
            RETURN "SYNC_KOR_SUBBASE"
			BREAK		
			
		CASE SYNC_RUS_JETPACK
            RETURN "SYNC_RUS_JETPACK"
			BREAK	
			
		CASE SYNC_RUS_END
            RETURN "SYNC_RUS_END"
			BREAK	
			
		CASE SYNC_PAPERMAN
			RETURN "SYNC_PAPERMAN"
			BREAK
			
		CASE SYNC_CAS_PREP2
			RETURN "SYNC_CAS_PREP2"
			BREAK
			
		CASE SYNC_CAS_PREP3
			RETURN "SYNC_CAS_PREP3"
			BREAK
		
		CASE SYNC_CAS_HEIST
			RETURN "SYNC_CAS_HEIST"
			BREAK
			
		CASE SYNC_MIL_DAM
			RETURN "SYNC_MIL_DAM"
			BREAK
			
		CASE SYNC_MIL_PLA
			RETURN "SYNC_MIL_PLA"
			BREAK
	
		CASE SYNC_MIL_RKT
			RETURN "SYNC_MIL_RKT"
			BREAK
			            
        CASE MAX_SYNCHRONISATION_IDS_CLF
            RETURN "ERROR (MAX_SYNCHRONISATION_IDS_CLF)"
			BREAK
    ENDSWITCH
    
    SCRIPT_ASSERT("Get_Sending_Strand_For_Synchronisation_CLF: Unknown Synchronisation ID - Needs Added")
    RETURN "Unknown"

ENDFUNC
#endif
#if USE_NRM_DLC
FUNC STRING GET_SYNCHRONISATION_DISPLAY_STRING_FROM_SYNC_ID_NRM(SYNCHRONIZATION_IDS paramSyncID)

    SWITCH (paramSyncID)
							
        CASE SYNC_NONE
            RETURN "ERROR (SYNC_NONE)"
			BREAK
            
        CASE MAX_SYNCHRONISATION_IDS_NRM
            RETURN "ERROR (MAX_SYNCHRONISATION_IDS_NRM)"
			BREAK
    ENDSWITCH
    
    SCRIPT_ASSERT("Get_Sending_Strand_For_Synchronisation_CLF: Unknown Synchronisation ID - Needs Added")
    RETURN "Unknown"

ENDFUNC
#endif

FUNC STRING GET_SYNCHRONISATION_DISPLAY_STRING_FROM_SYNC_ID(SYNCHRONIZATION_IDS paramSyncID)

#if USE_CLF_DLC
	return GET_SYNCHRONISATION_DISPLAY_STRING_FROM_SYNC_ID_CLF(paramSyncID)
#endif
#if USE_NRM_DLC
	return GET_SYNCHRONISATION_DISPLAY_STRING_FROM_SYNC_ID_NRM(paramSyncID)
#endif
#if not USE_CLF_DLC
#if not USE_NRM_DLC
    SWITCH (paramSyncID)
	
		CASE SYNC_ASSA3_TO_CARSTEAL1
            RETURN "Assa 3 sync to Carsteal 1"
			BREAK
			
		CASE SYNC_ASSA5_TO_EXILE1
            RETURN "Assa 5 sync to Exile 1"
			BREAK
            
        CASE SYNC_CAR_STEAL_TO_MICHAEL
            RETURN "Car Steal sync to Michael"
			BREAK
			
		CASE SYNC_CAR_STEAL3_TO_MARTIN1
			RETURN "Car Steal 3 sync to Martin 1"
    
        CASE SYNC_CHINESE_TO_TREVOR
            RETURN "Chinese sync to Trevor"
			BREAK
			
			
		CASE SYNC_DOCKS_HEIST_P_TO_FBI_OFF_3
			RETURN "Docks Hiest P sync to FBI Officers 3"
			BREAK
			
		CASE SYNC_DOCKS_HEIST_TO_DOCKS_HEIST_2
			RETURN "Docks Hiest sync to Docks Heist 2"
			BREAK	


		CASE SYNC_EXILE2_TO_RURAL_HEIST1
            RETURN "Exile2 sync to Rural Heist1"
			BREAK
            
		CASE SYNC_EXILE3_TO_RURAL_HEIST2
            RETURN "Exile3 sync to Rural Heist2"
			BREAK
			
		CASE SYNC_FAMILY3_TO_TONYA1
			RETURN "Family 3 sync to Tonya 1"
			BREAK
			
		CASE SYNC_FAMILY2_TO_FRANKLIN0
			RETURN "Family 2 sync to Franklin 0"
		
		CASE SYNC_FAMILY_TO_FBI_OFF
			RETURN "Family sync to FBI Officers"
			BREAK

		CASE SYNC_FAMILY_TO_TREVOR
            RETURN "Family sync to Trevor"
			BREAK
			
		CASE SYNC_FAMILY6_TO_AGENCY1
            RETURN "Family6 sync to Agency1"
			BREAK
			
		CASE SYNC_FBI_OFF4_TO_ASS1
			RETURN "FBI Officers 4 to Assass 1"
			BREAK
			
		CASE SYNC_FBI_OFF4_TO_DOCKSH2
			RETURN "FBI Officers 4 to DocksH 2"
			BREAK
			
		CASE SYNC_FBI_OFF4_TO_FRANKLIN1
			RETURN "FBI Officers 4 to Franklin 1"
			
		CASE SYNC_FBI_OFF_TO_FBI_OFF2
            RETURN "FBI Officers sync to FBI Officers 2"
			BREAK
			
		CASE SYNC_FBI_OFF_TO_FBI_OFF3
            RETURN "FBI Officers sync to FBI Officers 3"
			BREAK
			
		CASE SYNC_FBI_OFF_TO_FBI_OFF4
            RETURN "FBI Officers sync to FBI Officers 4"
			BREAK
			
		CASE SYNC_FBI_OFF_TO_FBI_OFF5
            RETURN "FBI Officers sync to FBI Officers 5"
			BREAK

        CASE SYNC_FBI_OFF4_TO_DOCKSH_PREP
            RETURN "FBI Off 4 sync to Docks Heist Prep"
			BREAK
            
        CASE SYNC_FBI_OFF_TO_EXILE
            RETURN "FBI Officers sync to Exile"
			BREAK
            
        CASE SYNC_FBI_OFF_TO_FAMILY
            RETURN "FBI Officers sync to Family"
			BREAK
			
		CASE SYNC_FINALE_HEIST1_TO_SOLOMON1B
            RETURN "Finale Heist 1 sync to Solomon 1B"
			BREAK
			
		CASE SYNC_FINALE_HEIST_TO_MICHAEL4
            RETURN "Finale Heist sync to Michael 4"
			BREAK
			
		CASE SYNC_FINALE_HEIST_TO_FINALE_HEIST2
			RETURN "Fainel Heist sync to Fainel Heist 2"
			BREAK
			
		CASE SYNC_FINALE_HEIST_TO_FINALE_HEIST3
			RETURN "Fainel Heist sync to Fainel Heist 3"
			BREAK
			
		CASE SYNC_FINALE_HEIST_TO_FINALE_HEIST4
			RETURN "Fainel Heist sync to Fainel Heist 4"
			BREAK
			
		CASE SYNC_FRANKLIN1_TO_LAMAR
            RETURN "Franklin 1 sync to Lamar"
			BREAK	
		
		CASE SYNC_FRANKLIN1_TO_FBI_OFF2
            RETURN "Franklin 1 sync to FBI Officers 2"
			BREAK
			
		CASE SYNC_FRANKLIN2_TO_FAMILY6
            RETURN "Franklin 2 sync to Family 6"
			BREAK
			
		CASE SYNC_FRANKLIN2_TO_MICHAEL3
            RETURN "Franklin 2 sync to Michael 3"
			BREAK
		
        CASE SYNC_JEWEL_HEIST_PREP_TO_FAMILY3
            RETURN "Jewel Heist Preps sync to Fam 3"
			BREAK
			
		CASE SYNC_JEWEL_HEIST_TO_JEWEL_HEIST_2
            RETURN "Jewel Heist sync to Jewel 2"
			BREAK
			
		CASE SYNC_LAMAR1_TO_FRANKLIN0
			RETURN "Lamar 1 sync to Franklin 0"
			BREAK
			
		CASE SYNC_MARTIN1_TO_CARSTEAL2
            RETURN "Martin 1 sync to Car Steal 2"
			BREAK
			
		CASE SYNC_MICHAEL1_TO_CARS3
            RETURN "Michael 1 sync to Carsteal 3"
			BREAK
			
		CASE SYNC_MICHAEL4_TO_FRANKLIN2
			RETURN "Michael4 sync to Franklin2"
			BREAK
			
		CASE SYNC_MICHAEL4_TO_SOLOMON3
            RETURN "Michael 4 sync to Solomon 3"
			BREAK
			
        CASE SYNC_MICHAEL2_TO_CAR_STEAL4
            RETURN "Michael 2 sync to Car Steal 4"
			BREAK
			
		CASE SYNC_MICHAEL3_TO_AGENCY_HEIST
            RETURN "Michael 3 sync to Agency Heist"
			BREAK

		CASE SYNC_MIKE_EV_T_TO_FAMILY6
            RETURN "Michael Ev T sync to Family 6"
			BREAK
			
		CASE SYNC_MIKE_EV_J_TO_MICHAEL4
            RETURN "Michael Ev J sync to Michael 4"
			BREAK
				
		CASE SYNC_RURAL_HEIST2_TO_EXILE2
            RETURN "Rural Heist2 to Exile2"
			BREAK
		
		CASE SYNC_SHRINK4_TO_EXILE1
            RETURN "Shrink4 to Exile1"
			BREAK
			
		CASE SYNC_SHRINK2_TO_FAMILY4
            RETURN "Shrink2 to Family4"
			BREAK
			
		CASE SYNC_SHRINK5_TO_FAMILY6
            RETURN "Shrink5 to Family6"
			BREAK
			
		CASE SYNC_SHRINK3_TO_SOLOMON1
            RETURN "Shrink3 to Solomon1"
			BREAK
			
		CASE SYNC_SHRINK5_TO_SOLOMON3
            RETURN "Shrink5 to Solomon3"
			BREAK
			
		CASE SYNC_SHRINK5_TO_MICHAEL3
            RETURN "Shrink5 to Michael3"
			BREAK
			
		CASE SYNC_SOLOMON2_TO_MICHAEL2
            RETURN "Solomon 2 sync to Michael 2"
			BREAK
			
		CASE SYNC_SOLOMON3_TO_AGENCYH2
			RETURN "Solomon 3 sync to AgencyH 2"
			BREAK
			
		CASE SYNC_SOLOMON3_TO_FAMILY6
			RETURN "Solomon 3 sync to Family 6"
			BREAK
			
		CASE SYNC_TREVOR2_TO_CHINESE1_CALL
			RETURN "Trevor 2 sync to Chinese 1 Call"
			BREAK
			
		CASE SYNC_TREVOR3_TO_CHINESE2
            RETURN "Trevor 3 sync to Chinese 2"
			BREAK
			
		CASE SYNC_TREVOR4_TO_FBI_OFF5
            RETURN "Trevor 4 sync to FBI Officers 5"
			BREAK
			
        CASE SYNC_NONE
            RETURN "ERROR (SYNC_NONE)"
			BREAK
            
        CASE MAX_SYNCHRONISATION_IDS
            RETURN "ERROR (MAX_SYNCHRONISATION_IDS)"
			BREAK
    ENDSWITCH
    
    SCRIPT_ASSERT("Get_Sending_Strand_For_Synchronisation: Unknown Synchronisation ID - Needs Added")
    RETURN "Unknown"
#endif
#endif
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Return the strand name as a string based on the strand ID
//
// INPUT PARAMS:    paramStrandID           Strand ID
// RETURN VALUE:    STRING                  Debug display string for the strand
#if USE_CLF_DLC
FUNC STRING GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID_CLF(STRANDS paramStrandID)

    SWITCH (paramStrandID)
		
		CASE STRAND_CLF
			RETURN "Clifford"
			BREAK
			
		CASE STRAND_CLF_IAA
			RETURN "IAA"
			BREAK
			
		CASE STRAND_CLF_RUS
			RETURN "Russian"
			BREAK
			
		CASE STRAND_CLF_KOR
			RETURN "Korean"
			BREAK
			
		CASE STRAND_CLF_ARA
			RETURN "Arab"
			BREAK
			
		CASE STRAND_CLF_CAS1
			RETURN "Casino 1"
			BREAK
			
		CASE STRAND_CLF_CAS2
			RETURN "Casino 2"
			BREAK
		
		CASE STRAND_CLF_CAS3
			RETURN "Casino 3"
			BREAK
		
		CASE STRAND_CLF_MIL
			RETURN "Miles Dam"
			BREAK
			
		CASE STRAND_CLF_MIL2
			RETURN "Miles Rocket"
			BREAK
		
		CASE STRAND_CLF_MIL3
			RETURN "Miles Planes"
			BREAK
		
		CASE STRAND_CLF_ASS_PAP
			RETURN "Paperman"
			BREAK
		
		CASE STRAND_CLF_ASS_A14
			RETURN "Agent 14"
			BREAK
		
		CASE STRAND_CLF_JET
			RETURN "Jetpack"
			BREAK
		
		CASE STRAND_CLF_RC_ALEX
			RETURN "RC Alex"
			BREAK
			
		CASE STRAND_CLF_RC_MELODY
			RETURN "RC Melody"
			BREAK
			
		CASE STRAND_CLF_RC_AGNES
			RETURN "RC Agnes"
			BREAK
			
		CASE STRAND_CLF_RC_CLAIRE
			RETURN "RC Claire"
			BREAK
						
		CASE MAX_STRANDS_CLF
            RETURN "ERROR (MAX_STRANDS_CLF)"
			BREAK
	ENDSWITCH
	
	SCRIPT_ASSERT("GET_CLF_STRAND_DISPLAY_STRING_FROM_STRAND_ID: Unknown Strand ID - Needs Added")
    RETURN "Unknown"
ENDFUNC
#endif
#if USE_NRM_DLC
FUNC STRING GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID_NRM(STRANDS paramStrandID)

    SWITCH (paramStrandID)
		
		CASE STRAND_NRM_SURVIVE
			RETURN "norman SURVIVE"
			BREAK
			
		CASE STRAND_NRM_SUPPLY_FUEL
			RETURN "SUPPLY FUEL"
			BREAK
		CASE STRAND_NRM_SUPPLY_AMMO
			RETURN "SUPPLY AMMO"
			BREAK
		CASE STRAND_NRM_SUPPLY_MED
			RETURN "SUPPLY MEDS"
			BREAK
		CASE STRAND_NRM_SUPPLY_FOOD
			RETURN "SUPPLY FOOD"
			BREAK
			
		CASE STRAND_NRM_RESCUE_ENG
			RETURN "RESCUE ENG"
			BREAK
		CASE STRAND_NRM_RESCUE_MED
			RETURN "RESCUE MED"
			BREAK
		CASE STRAND_NRM_RESCUE_GUN
			RETURN "RESCUE GUN"
			BREAK
			
		CASE STRAND_NRM_RADIO
			RETURN "RADIO TOWER"
			BREAK
			
		CASE MAX_STRANDS_NRM
            RETURN "ERROR (MAX_STRANDS_NRM)"
			BREAK
	ENDSWITCH
	
	SCRIPT_ASSERT("GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID_NRM: Unknown Strand ID - Needs Added")
    RETURN "Unknown"
ENDFUNC
#endif

FUNC STRING GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(STRANDS paramStrandID)

#if USE_CLF_DLC
	return GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID_CLF(paramStrandID)
#endif
#if USE_NRM_DLC
	return GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID_NRM(paramStrandID)
#endif
#if not USE_CLF_DLC
#if not USE_NRM_DLC
    SWITCH (paramStrandID)
		CASE STRAND_AGENCY_HEIST
            RETURN "Agency Heist"
			BREAK
	
        CASE STRAND_ARMENIAN
            RETURN "Armenian"
			BREAK
			
		CASE STRAND_ASSASSINATIONS
			RETURN "Assassinations"
			BREAK
            
        CASE STRAND_CAR_STEAL
            RETURN "Car Steal"
			BREAK
            
        CASE STRAND_CHINESE
            RETURN "Chinese"
			BREAK
            
        CASE STRAND_DOCKS_HEIST
            RETURN "Docks Heist"
			BREAK
			
		CASE STRAND_DOCKS_HEIST_2
            RETURN "Docks Heist 2"
			BREAK
            
        CASE STRAND_EXILE
            RETURN "Exile"
			BREAK
            
        CASE STRAND_FAMILY
            RETURN "Family"
			BREAK
            
        CASE STRAND_FBI_OFFICERS
            RETURN "FBI Officers"
			BREAK
			
		CASE STRAND_FBI_OFFICERS_2
            RETURN "FBI Officers 2"
			BREAK
			
		CASE STRAND_FBI_OFFICERS_3
            RETURN "FBI Officers 3"
			BREAK
			
		CASE STRAND_FBI_OFFICERS_4
            RETURN "FBI Officers 4"
			BREAK
			
		CASE STRAND_FBI_OFFICERS_5
            RETURN "FBI Officers 5"
			BREAK
			
		CASE STRAND_FINALE
            RETURN "Finale"
			BREAK

        CASE STRAND_FINALE_HEIST
            RETURN "Finale Heist"
			BREAK
			
		CASE STRAND_FINALE_HEIST_2
            RETURN "Finale Heist 2"
			BREAK
			
		CASE STRAND_FINALE_HEIST_3
            RETURN "Finale Heist 3"
			BREAK
			
		CASE STRAND_FINALE_HEIST_4
            RETURN "Finale Heist 4"
			BREAK
            
        CASE STRAND_FRANKLIN
            RETURN "Franklin"
			BREAK
            
        CASE STRAND_JEWEL_HEIST
            RETURN "Jewel Heist"
			BREAK
			
		CASE STRAND_JEWEL_HEIST_2
            RETURN "Jewel Heist 2"
			BREAK
            
        CASE STRAND_LAMAR
            RETURN "Lamar"
			BREAK
            
        CASE STRAND_LESTER
            RETURN "Lester"
			BREAK
			
		CASE STRAND_MARTIN
            RETURN "Martin"
			BREAK
			
        CASE STRAND_MICHAEL
            RETURN "Michael"
			BREAK
			
		CASE STRAND_MICHAEL_EVENTS
            RETURN "Michael Events"
			BREAK

        CASE STRAND_PROLOGUE
            RETURN "Prologue"
			BREAK
            
        CASE STRAND_RURAL_BANK_HEIST
            RETURN "Rural Bank Heist"
			BREAK
			
		CASE STRAND_SHRINK
			RETURN "Shrink"
			BREAK
            
        CASE STRAND_SOLOMON
            RETURN "Solomon"
			BREAK

        CASE STRAND_TREVOR
            RETURN "Trevor"
			BREAK	
			
        CASE STRAND_NONE
            RETURN "ERROR (STRAND_NONE)"
			BREAK
			
		CASE MAX_STRANDS
            RETURN "ERROR (MAX_STRANDS)"
			BREAK
    ENDSWITCH
    
    SCRIPT_ASSERT("GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID: Unknown Strand ID - Needs Added")
    RETURN "Unknown"
#endif
#endif

ENDFUNC

// -----------------------------------------------------------------------------------------------------------
//      Mission Flow Standard Debug Output Flow Bitset Strings
// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Return the flow bitset as a string based on the FlowBitsetID
//
// INPUT PARAMS:    paramBitsetID           Flow Bitset ID
// RETURN VALUE:    STRING                  Debug display string for the Flow Bitset ID
#if USE_CLF_DLC
FUNC STRING GET_BITSET_DISPLAY_STRING_FROM_BITSET_IDCLF(FLOW_BITSET_IDS paramBitsetID)

    SWITCH (paramBitsetID)   
		CASE FLOWBITSET_CLF_MINIGAME_ACTIVE
            RETURN "Minigame Active Bitset"
			BREAK
		CASE FLOWBITSET_NONE
            RETURN "ERROR (FLOWBITSET_NONE)"
			BREAK	
    	CASE MAX_FLOW_BITSET_IDS_CLF
            RETURN "ERROR (MAX_FLOW_BITSET_IDS_NRM)"
			BREAK   
        CASE MAX_FLOW_BITSET_IDS
            RETURN "ERROR (MAX_FLOW_BITSET_IDS)"
			BREAK
    ENDSWITCH
    
    SCRIPT_ASSERT("Get_Bitset_Display_String_From_Bitset_IDNRM: Unknown Flow Bitset ID - Needs Added")
    RETURN "Unknown"

ENDFUNC
#endif
#if USE_NRM_DLC
FUNC STRING GET_BITSET_DISPLAY_STRING_FROM_BITSET_IDNRM(FLOW_BITSET_IDS paramBitsetID)

    SWITCH (paramBitsetID)   
		CASE FLOWBITSET_NRM_MINIGAME_ACTIVE
            RETURN "Minigame Active Bitset"
			BREAK
		CASE FLOWBITSET_NONE
            RETURN "ERROR (FLOWBITSET_NONE)"
			BREAK	
    	CASE MAX_FLOW_BITSET_IDS_NRM
            RETURN "ERROR (MAX_FLOW_BITSET_IDS_NRM)"
			BREAK   
        CASE MAX_FLOW_BITSET_IDS
            RETURN "ERROR (MAX_FLOW_BITSET_IDS)"
			BREAK
    ENDSWITCH
    
    SCRIPT_ASSERT("Get_Bitset_Display_String_From_Bitset_IDNRM: Unknown Flow Bitset ID - Needs Added")
    RETURN "Unknown"

ENDFUNC
#endif

FUNC STRING GET_BITSET_DISPLAY_STRING_FROM_BITSET_ID(FLOW_BITSET_IDS paramBitsetID)
#if USE_CLF_DLC
	return GET_BITSET_DISPLAY_STRING_FROM_BITSET_IDCLF(paramBitsetID)
#endif
#if USE_NRM_DLC
	return GET_BITSET_DISPLAY_STRING_FROM_BITSET_IDNRM(paramBitsetID)
#endif

#if not USE_CLF_DLC
#if not USE_NRM_DLC
    SWITCH (paramBitsetID)   
		CASE FLOWBITSET_MINIGAME_ACTIVE
            RETURN "Minigame Active Bitset"
			BREAK
            
        CASE FLOWBITSET_HEIST_DISABLE_BOARD_EXIT
            RETURN "Heist Disable Board Exit"
			BREAK
            
        CASE FLOWBITSET_HEIST_TOGGLE_BOARD
            RETURN "Heist Toggle Board"
			BREAK
			
		CASE FLOWBITSET_HEIST_POI_OVERVIEW_DONE
            RETURN "Heist POI Overview Done Bitset"
			BREAK
                        
        CASE FLOWBITSET_HEIST_CREW_SELECTED
            RETURN "Heist Crew Selected Bitset"
			BREAK
			
		CASE FLOWBITSET_HEIST_CHOICE_SELECTED
            RETURN "Heist Choice Selected Bitset"
			BREAK

        CASE FLOWBITSET_FRANKLIN_CHANGED_HAIRCUT
            RETURN "Franklin Changed Haircut Bitset"
			BREAK
            
        CASE FLOWBITSET_FRANKLIN_CHANGED_OUTFIT
            RETURN "Franklin Changed Outfit Bitset"
			BREAK
            
        CASE FLOWBITSET_MICHAEL_VISITED_GYM
            RETURN "Michael Visited Gym Bitset"
			BREAK

        CASE FLOWBITSET_RESTART_FLOW_LAUNCHED_SCRIPTS_1
            RETURN "Restart Flow Launch Scripts Bitset 1"
			BREAK
			
		CASE FLOWBITSET_RESTART_FLOW_LAUNCHED_SCRIPTS_2
            RETURN "Restart Flow Launch Scripts Bitset 2"
			BREAK

        CASE FLOWBITSET_NONE
            RETURN "ERROR (FLOWBITSET_NONE)"
			BREAK
            
        CASE MAX_FLOW_BITSET_IDS
            RETURN "ERROR (MAX_FLOW_BITSET_IDS)"
			BREAK
    ENDSWITCH
    
    SCRIPT_ASSERT("Get_Bitset_Display_String_From_Bitset_ID: Unknown Flow Bitset ID - Needs Added")
    RETURN "Unknown"
#endif
#endif
ENDFUNC


// -----------------------------------------------------------------------------------------------------------
//      Mission Flow Standard Debug Output Flow Flag Strings
// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Return the flow flag as a string based on the FlowFlagID
//
// INPUT PARAMS:    paramFlagID             Flow Flag ID
// RETURN VALUE:    STRING                  Debug display string for the Flow Flag ID
#if USE_CLF_DLC
FUNC STRING GET_FLAG_DISPLAY_STRING_FROM_FLAG_IDCLF(FLOW_FLAG_IDS paramFlagID)

    SWITCH (paramFlagID)
		CASE FLOWFLAG_CLF01
            RETURN "ERROR (FLOWFLAG_NONE)"
			BREAK
			
		CASE FLOWFLAG_SPY_CAR_UNLOCKED
            RETURN "FLOWFLAG_SPY_CAR_UNLOCKED"
			BREAK	
			
		CASE FLOWFLAG_JP_UNLOCKED
			RETURN "FLOWFLAG_JP_UNLOCKED"
			BREAK
			
        CASE FLOWFLAG_NONE
            RETURN "ERROR (FLOWFLAG_NONE)"
			BREAK
            
        CASE MAX_FLOW_FLAG_IDS
            RETURN "ERROR (MAX_FLOW_FLAG_IDS)"
			BREAK
    ENDSWITCH
    
    SCRIPT_ASSERT("Get_Flag_Display_String_From_Flag_IDagt: Unknown Flow Flag ID - Needs Added")
    RETURN "Unknown"

ENDFUNC
#endif
#if USE_NRM_DLC
FUNC STRING GET_FLAG_DISPLAY_STRING_FROM_FLAG_IDNRM(FLOW_FLAG_IDS paramFlagID)

    SWITCH (paramFlagID)
		
	 	CASE FLOWFLAG_NRM_START
            RETURN "FLOWFLAG_NRM_START"
			BREAK
        CASE FLOWFLAG_NONE
            RETURN "ERROR (FLOWFLAG_NONE)"
			BREAK
		CASE FLOWFLAG_NRM_HAVE_ENGINEER
            RETURN "FLOWFLAG_NRM_HAVE_ENGINEER"
			BREAK
		CASE FLOWFLAG_NRM_HAVE_MEDIC
            RETURN "FLOWFLAG_NRM_HAVE_MEDIC"
			BREAK	
        CASE FLOWFLAG_NRM_HAVE_GUNMAN
            RETURN "FLOWFLAG_NRM_HAVE_GUNMAN"
			BREAK	
		CASE FLOWFLAG_NRM_COLLECTED_FUEL
            RETURN "FLOWFLAG_NRM_COLLECTED_FUEL"
			BREAK
		CASE FLOWFLAG_NRM_COLLECTED_AMMO
            RETURN "FLOWFLAG_NRM_COLLECTED_AMMO"
			BREAK
		CASE FLOWFLAG_NRM_COLLECTED_MEDS
            RETURN "FLOWFLAG_NRM_COLLECTED_MEDS"
			BREAK
		CASE FLOWFLAG_NRM_COLLECTED_FOOD
            RETURN "FLOWFLAG_NRM_COLLECTED_FOOD"
			BREAK
        CASE MAX_FLOW_FLAG_IDS
            RETURN "ERROR (MAX_FLOW_FLAG_IDS)"
			BREAK
    ENDSWITCH
    
    SCRIPT_ASSERT("Get_Flag_Display_String_From_Flag_IDNRM: Unknown Flow Flag ID - Needs Added")
    RETURN "Unknown"

ENDFUNC
#endif
FUNC STRING GET_FLAG_DISPLAY_STRING_FROM_FLAG_ID(FLOW_FLAG_IDS paramFlagID)
#if USE_CLF_DLC
	return GET_FLAG_DISPLAY_STRING_FROM_FLAG_IDCLF(paramFlagID)
#endif
#if USE_NRM_DLC
	return GET_FLAG_DISPLAY_STRING_FROM_FLAG_IDNRM(paramFlagID)
#endif
#if not USE_CLF_DLC
#if not USE_NRM_DLC
    SWITCH (paramFlagID)
		CASE FLOWFLAG_HEIST_BOARD_UNDO
			RETURN "Heist Board Undo"
			BREAK
	
        CASE FLOWFLAG_HEIST_FINISHED_DOCKS
            RETURN "Docks Heist Finished Flag"
			BREAK
            
        CASE FLOWFLAG_HEIST_FINISHED_AGENCY
            RETURN "Agency Heist Finished Flag"
			BREAK
            
        CASE FLOWFLAG_HEIST_FINISHED_FINALE
            RETURN "Finale Heist Finished Flag"
			BREAK
            
        CASE FLOWFLAG_HEIST_FINISHED_JEWEL
            RETURN "Jewellery Heist Finished Flag"
			BREAK
            
        CASE FLOWFLAG_HEIST_FINISHED_RURAL_BANK
            RETURN "Rural Bank Heist Finished Flag"
			BREAK

			
		CASE FLOWFLAG_HEIST_AGENCY_PRIME_BOARD_TRANSITION
            RETURN "Agency Heist Prime Board Transition"
			BREAK
		
		CASE FLOWFLAG_HEIST_AGENCY_LOAD_BOARD_EXIT_CUTSCENE
            RETURN "Agency Heist Load Board Exit Cutscene"
			BREAK
            
        CASE FLOWFLAG_HEIST_AGENCY_DO_BOARD_EXIT_CUTSCENE
            RETURN "Agency Heist Do Board Exit Cutscene"
			BREAK
			
		CASE FLOWFLAG_HEIST_AGENCY_CALLS_COMPLETE
			RETURN "Agency Heist Calls Complete"
			BREAK
			
		CASE FLOWFLAG_HEIST_AGENCY_2_UNLOCKED
			RETURN "Agency Heist 2 Unlocked"
			BREAK
			
		CASE FLOWFLAG_HEIST_AGENCY_2_AUTOTRIGGERED
			RETURN "Agency Heist 2 Autotriggered"
			BREAK
			
		CASE FLOWFLAG_HEIST_AGENCY_PREP_2_DONE
			RETURN "Agency Heist Prep 2 Completed"
			BREAK
			
		CASE FLOWFLAG_HEIST_AGENCY_FIRETRUCK_GOT_WANTED
			RETURN "Agency Heist Firetruck Got Wanted"
			BREAK
			
		CASE FLOWFLAG_HEIST_AGENCY_PREP_CALLS_DONE
			RETURN "Agency Heist Prep Calls Done"
			BREAK

			
		CASE FLOWFLAG_HEIST_JEWEL_PREP2A_READY
			RETURN "Jewel Heist Prep2A Ready"
			BREAK
				
		CASE FLOWFLAG_HEIST_JEWEL_PRIME_BOARD_TRANSITION
            RETURN "Jewel Heist Prime Board Transition"
			BREAK
			
		CASE FLOWFLAG_HEIST_JEWEL_LOAD_BOARD_EXIT_CUTSCENE
            RETURN "Jewel Heist Load Board Exit Cutscene"
			BREAK
            
        CASE FLOWFLAG_HEIST_JEWEL_DO_BOARD_EXIT_CUTSCENE
            RETURN "Jewel Heist Do Board Exit Cutscene"
			BREAK
			
		CASE FLOWFLAG_HEIST_JEWEL_RUN
			RETURN "Jewel Heist Run"
			BREAK
					
		CASE FLOWFLAG_HEIST_DOCKS_PRIME_BOARD_TRANSITION
            RETURN "Docks Heist Prime Board Transition"
			BREAK
			
		CASE FLOWFLAG_HEIST_DOCKS_LOAD_BOARD_EXIT_CUTSCENE
            RETURN "Docks Heist Load Board Exit Cutscene"
			BREAK
            
        CASE FLOWFLAG_HEIST_DOCKS_DO_BOARD_EXIT_CUTSCENE
            RETURN "Docks Heist Do Board Exit Cutscene"
			BREAK

			
		CASE FLOWFLAG_HEIST_DOCKS_PRE_HEIST_TEXTS_DONE
            RETURN "Docks Heist Pre-Heist Texts Done"
			BREAK
			
			
		CASE FLOWFLAG_HEIST_FINALE_PRIME_BOARD_TRANSITION
			RETURN "Finale Heist Prime Board Transition"
			BREAK
			
		CASE FLOWFLAG_HEIST_FINALE_LOAD_BOARD_EXIT_CUTSCENE
			RETURN "Finale Heist Load Board Exit Cutscene"
			BREAK
			
		CASE FLOWFLAG_HEIST_FINALE_DO_BOARD_EXIT_CUTSCENE
			RETURN "Finale Heist Do Board Exit Cutscene"
			BREAK
			
		CASE FLOWFLAG_HEIST_FINALE_PREP_GOT_WANTED
			RETURN "Finale Heist Prep Got Wanted"
			BREAK

		CASE FLOWFLAG_HEIST_FINALE_PREP_A_CALLS_DONE
			RETURN "Finale Heist Prep A Calls Done"
			BREAK
			
		CASE FLOWFLAG_HEIST_FINALE_PREP_B_CALLS_DONE
			RETURN "Finale Heist Prep B Calls Done"
			BREAK
			
		CASE FLOWFLAG_HEIST_FINALE_PREPC_EMAIL_DONE
			RETURN "Finale Heist PrepC Email Done"
			BREAK
			
		CASE FLOWFLAG_HEIST_FINALE_PREPC_CAR_1_STOLEN_MAP
			RETURN "Finale Heist PrepC Car3 Stolen Map"
			BREAK
			
		CASE FLOWFLAG_HEIST_FINALE_PREPC_CAR_2_STOLEN_MAP
			RETURN "Finale Heist PrepC Car2 Stolen Map"
			BREAK
			
		CASE FLOWFLAG_HEIST_FINALE_PREPC_CAR_3_STOLEN_MAP
			RETURN "Finale Heist PrepC Car3 Stolen Map"
			BREAK

		CASE FLOWFLAG_HEIST_FINALE_PREPE_DONE
			RETURN "Finale Heist Prep E Done"
			BREAK

		CASE FLOWFLAG_HEIST_FINALE_PREPE_PLACING_CAR
			RETURN "Finale Heist Prep E Placing car"
			BREAK
			
		CASE FLOWFLAG_HEIST_FINALE_2_READY
			RETURN "Finale Heist 2 Ready"
			BREAK
			
			
		CASE FLOWFLAG_HEIST_RURAL_LOAD_MIKE_WIN_ENDING
			RETURN "Rural Heist Load Mike Win Ending"
			BREAK
			
		CASE FLOWFLAG_HEIST_RURAL_LOAD_TREV_WIN_ENDING
			RETURN "Rural Heist Load Trev Win Ending"
			BREAK
			
		CASE FLOWFLAG_HEIST_RURAL_LOAD_BOARD_EXIT_CUTSCENE
			RETURN "Rural Heist Load Board Exit Cutscene"
			BREAK
			
		CASE FLOWFLAG_HEIST_RURAL_DO_BOARD_EXIT_CUTSCENE
			RETURN "Rural Heist Do Board Exit Cutscene"
			BREAK
			
		CASE FLOWFLAG_HEIST_RURAL_PREP_CALL_DONE
			RETURN "Rural Heist Prep Calls Done"
			BREAK
			
			
		CASE FLOWFLAG_UNLOCK_LAMAR_1
			RETURN "Unlock Lamar 1"
			BREAK
        
		CASE FLOWFLAG_MISSION_FRANK1_MC_CLIPPED
			RETURN "Frank 1 MC CLIP KILLED"
			BREAK
			
		CASE FLOWFLAG_MISSION_FBI_3_CALLS_DONE
			RETURN "Mission FBI 3 Calls Done"
			BREAK
			
		CASE FLOWFLAG_MISSION_FBI_4_PREP_3_COMPLETED
			RETURN "Mission FBI 4 Prep 3 Completed"
			BREAK
			
		CASE FLOWFLAG_MISSION_FBI_4_UNLOCKED_FROM_PREP
			RETURN "Mission FBI 4 Unlocked From Prep"
			BREAK
			
		CASE FLOWFLAG_MISSION_FBI_4_CALLS_DONE
			RETURN "Mission FBI 4 Calls Done"
			BREAK
		
		CASE FLOWFLAG_ASS1_UNLOCKED	
			RETURN "Assassination unlocked"
			BREAK
			
		CASE FLOWFLAG_ASS_COMPLETED
			RETURN "Assassinations completed"
			BREAK
			
		CASE FLOWFLAG_MISSION_MICHAEL_4_TEXTS_DONE
			RETURN "Mission Michael 4 Texts Done"
			BREAK
			
			
		CASE FLOWFLAG_MISSION_MARTIN_1_CALLS_DONE
			RETURN "Mission Martin 1 Calls Done"
			BREAK
			
			
        CASE FLOWFLAG_ALLOW_RANDOM_EVENTS
            RETURN "Allow Random Events Flag"
			BREAK
			
		CASE FLOWFLAG_ALLOW_CINEMA_ACTIVITY
			RETURN "Allow Cinema Activity"
			BREAK
			
		CASE FLOWFLAG_ALLOW_COMEDYCLUB_ACTIVITY
			RETURN "Allow Comedy Club Activity"
			BREAK
			
		CASE FLOWFLAG_ALLOW_LIVEMUSIC_ACTIVITY
			RETURN "Allow Live Music Activity"
			BREAK
			
		CASE FLOWFLAG_ALLOW_SHOP_ROBBERIES
			RETURN "Allow Shop Robberies"
			BREAK
            
            
        CASE FLOWFLAG_BLOCK_SAVEHOUSE_FOR_AUTOSAVE
            RETURN "Block safehouse for autosave"
			BREAK
			
        CASE FLOWFLAG_TREVOR_RESTRICTED_TO_COUNTRY
            RETURN "Trevor Restricted To Country"
			BREAK
			
        CASE FLOWFLAG_TREVOR_RESTRICTED_TO_CITY
            RETURN "Trevor Restricted To City"
			BREAK
            
		CASE FLOWFLAG_CHOP_THE_DOG_UNLOCKED
            RETURN "Chop the Dog Unlocked"
			BREAK
			
		CASE FLOWFLAG_CARSTEAL3_INITIALISED
			RETURN "Carsteal3 Initialised"
			BREAK
			
		CASE FLOWFLAG_MOVIE_STUDIO_OPEN
            RETURN "Movie Studio Open"
			BREAK
			
		CASE FLOWFLAG_MOVIE_STUDIO_OPEN_FRANKLIN
            RETURN "Movie Studio Open Franklin"
			BREAK
			
		CASE FLOWFLAG_AIR_VEHICLE_PARACHUTE_UNLOCKED
			RETURN "Air Vehicle Parachute Unlocked"
			BREAK
			
		CASE FLOWFLAG_WATER_VEHICLE_SCUBA_GEAR_UNLOCKED
			RETURN "Water Vehicle Scuba Gear Unlocked"
			BREAK
			
		CASE FLOWFLAG_OFFROAD_RACING_UNLOCKED_FOR_ALL
			RETURN "Offroad Racing Unlocked For All"
			BREAK
			
		CASE FLOWFLAG_RUN_BUILDINGSITE_AMBIENCE_AUDIO
			RETURN "Run Building Site Ambience Audio"
			BREAK
			
		CASE FLOWFLAG_MICHAEL_DONE_FLYING_LESSONS
			RETURN "Michael Done Flying Lessons"
			BREAK
		
		CASE FLOWFLAG_ARM1_CAR_DAMAGED
			RETURN "FLOWFLAG_ARM1_CAR_DAMAGED"
			BREAK
					
		CASE FLOWFLAG_FRANKLIN_FROZEN_POST_PROLOGUE
			RETURN "Franklin Frozen Post Prologue"
			BREAK
			
		CASE FLOWFLAG_STRETCH_TEXT_SENT
			RETURN "Stretch Text Sent"
			BREAK
			
		CASE FLOWFLAG_ARM3_STRIPCLUB_SWITCH_AVAILABLE
			RETURN "Arm3 Stripclub Switch Available"
			BREAK
			
		CASE FLOWFLAG_DARTS_YELLOW_JACK_AVAILABLE
			RETURN "Darts Yellow Jack Available"
			BREAK
			
		CASE FLOWFLAG_RES_AND_RCS_UNLOCKED
			RETURN "REs And RCs Unlocked"
			BREAK
		
		CASE FLOWFLAG_EXILE1_PICKUPS_UNLOCKED
			RETURN "Exile 1 Pickups Unlocked"
			BREAK
			
		CASE FLOWFLAG_CARSTEAL2_KILLED_MULLIGAN
			RETURN "Carsteal2 Killed Mulligan"
			BREAK
			
		CASE FLOWFLAG_FRAN_DONE_ACTIVITY_WITH_LAMAR
			RETURN "Fran Done Activtiy with Lamar"
			BREAK
			
		CASE FLOWFLAG_TREV_DONE_ACTIVITY_WITH_LAMAR
			RETURN "Trev Done Activity with Lamar"
			BREAK
			
		CASE FLOWFLAG_AMANDA_MICHAEL_EVENT_SKIPPED
			RETURN "Amanda Michael Event Skipped"
			BREAK
		
		CASE FLOWFLAG_SHRINK_KILLED
			RETURN "Shrink Killed"
			BREAK
			
		CASE FLOWFLAG_BLOCK_FRAN_MISSIONS_FOR_TREV
			RETURN "Block Fran Missions for Trevor"
			BREAK
		
		CASE FLOWFLAG_GUNRANGE_TUTORIAL_COMPLETED
			RETURN "Completed gun range tutorial"
			BREAK
			
		CASE FLOWFLAG_WILDLIFE_PHOTOGRAPHY_UNLOCKED
			RETURN "Wildlife Photography Unlocked"
			BREAK
		
		// RCM			
		CASE FLOWFLAG_BARRY3_TEXT_RECEIVED
			RETURN "Barry 3 Text Received"
			BREAK
			
		CASE FLOWFLAG_BARRY4_TEXT_RECEIVED
			RETURN "Barry 4 Text Received"
			BREAK
		
		CASE FLOWFLAG_EPSILON_QUESTIONNAIRE_DONE
			RETURN "Epsilon Questionnaire Done"
			BREAK
			
		CASE FLOWFLAG_EPSILON_DONATED_500
            RETURN "Epsilon $500 Donated"
			BREAK
			
		CASE FLOWFLAG_EPSILON_DONATED_5000
            RETURN "Epsilon $5000 Donated"
			BREAK
			
		CASE FLOWFLAG_EPSILON_CARS_DONE
			RETURN "Epsilon Cars Done"
			BREAK
		
		CASE FLOWFLAG_EPSILON_DONATED_10000
            RETURN "Epsilon $10000 Donated"
			BREAK
			
		CASE FLOWFLAG_EPSILON_ROBES_BOUGHT
			RETURN "Epsilon Robes Bought"
			BREAK
		
		CASE FLOWFLAG_EPSILON_ROBES_DONE
			RETURN "Epsilon Robes Done"
			BREAK
			
		CASE FLOWFLAG_EPSILON_6_TEXT_RECEIVED
			RETURN "Epsilon 6 Text Received"
			BREAK
	
		CASE FLOWFLAG_EPSILON_DESERT_DONE
			RETURN "Epsilon Desert Done"
			BREAK
			
		CASE FLOWFLAG_EPSILON_UNLOCKED_TRACT
			RETURN "Epsilon Unlocked Tract"
			BREAK
	
		CASE FLOWFLAG_EXTREME2_TEXT_RECEIVED
			RETURN "Extreme 2 Text Received"
			BREAK
			
		CASE FLOWFLAG_EXTREME3_TEXT_RECEIVED
			RETURN "Extreme 3 Text Received"
			BREAK
		
		CASE FLOWFLAG_EXTREME4_BJUMPS_FINISHED
			RETURN "Extreme 4 Base Jumps Finished"
			BREAK
			
		CASE FLOWFLAG_HUNTING1_TEXT_RECEIVED
			RETURN "Hunting 1 Text Received"
			BREAK
			
		CASE FLOWFLAG_NIGEL1_EMAIL_RECEIVED
			RETURN "Nigel 1 Email Received"
			BREAK
		
		CASE FLOWFLAG_NIGEL3_AL_DI_NAPOLI_KILLED
			RETURN "Nigel 3 Di Napoli Killed"
			BREAK
			
		CASE FLOWFLAG_PAPARAZZO3_TEXT_RECEIVED
			RETURN "Paparazzo 3 Text Received"
			BREAK
			
		CASE FLOWFLAG_TONYA3_TEXT_RECEIVED
			RETURN "Tonya 3 Text Received"
			BREAK
			
		CASE FLOWFLAG_TONYA4_TEXT_RECEIVED
			RETURN "Tonya 4 Text Received"
			BREAK
			
		CASE FLOWFLAG_FOR_SALE_SIGNS_DESTROYED
            RETURN "For Sale Signs Destroyed"
			BREAK
			
		CASE FLOWFLAG_LETTER_SCRAPS_DONE
			RETURN "Letter Scraps Done"
			BREAK
		
		CASE FLOWFLAG_SPACESHIP_PARTS_DONE
			RETURN "Spaceship Parts Done"
			BREAK
			
		CASE FLOWFLAG_ALL_RAMPAGES_UNLOCKED
			RETURN "All Rampages Unlocked"
			BREAK
			
		CASE FLOWFLAG_PURCHASED_MARINA_PROPERTY
			RETURN "Purchased Marina Property"
			BREAK
					
		// Player controls
        CASE FLOWFLAG_PLAYER_VEH_F_UNLOCK_BIKE
            RETURN "Unlock Bagger for Franklin"
			BREAK
			
		CASE FLOWFLAG_PLAYER_VEH_T_UNLOCK_RASP_JAM
			RETURN "Unlock Mr Raspberry Jam"
			BREAK
            
        CASE FLOWFLAG_PLAYER_PED_INTRODUCED_M
            RETURN "Michael introduced"
			BREAK
            
        CASE FLOWFLAG_PLAYER_PED_INTRODUCED_F
            RETURN "Franklin introduced"
			BREAK
            
        CASE FLOWFLAG_PLAYER_PED_INTRODUCED_T
            RETURN "Trevor introduced"
			BREAK
			
		CASE FLOWFLAG_MIC_HAS_HAGGARD_SUIT
			RETURN "Michael has haggard suit"
			BREAK
			
		CASE FLOWFLAG_MIC_SET_HAGGARD_SUIT
			RETURN "Michael set haggard suit"
			BREAK
			
		CASE FLOWFLAG_MIC_REM_HAGGARD_SUIT
			RETURN "Michael removed haggard suit"
			BREAK
			
		CASE FLOWFLAG_MIC_HIDE_BARE_CHEST
			RETURN "Michael hide bare chest"
			BREAK
			
		CASE FLOWFLAG_MIC_PRO_MASK_REMOVED
			RETURN "Michael Porlogue mask removed"
			BREAK
			
		CASE FLOWFLAG_TRV_PRO_MASK_REMOVED
			RETURN "Trevor Porlogue mask removed"
			BREAK
			
		CASE FLOWFLAG_FRANKLIN_MOVED_TO_HILLS_APARTMENT
            RETURN "Franklin moved to Hills apartment"	
			BREAK
			
		CASE FLOWFLAG_MICHAEL_FRANKLIN_ARE_FRIENDS
            RETURN "Michael/Franklin are friends"
			BREAK
		
		CASE FLOWFLAG_MICHAEL_AMANDA_HAVE_SPLIT
            RETURN "Michael/Amanda have split up"
			BREAK
		
		CASE FLOWFLAG_MICHAEL_TREVOR_HAVE_FALLEN_OUT
            RETURN "Michael/Trevor have fallen out"
			BREAK
		
		CASE FLOWFLAG_MICHAEL_TREVOR_EXILE_STARTED
            RETURN "Michael/Trevor exile started"	
			BREAK
			
		CASE FLOWFLAG_MICHAEL_TREVOR_EXILE_FINISHED
            RETURN "Michael/Trevor exile finished"
			BREAK
			
		CASE FLOWFLAG_ORTEGA_KILLED
			RETURN "Ortega Killed"
			BREAK
			
		CASE FLOWFLAG_GAME_100_PERCENT_COMPLETE
			RETURN "Game 100 Percent Complete"
			BREAK
			
		CASE FLOWFLAG_FINAL_CHOICE_MADE
			RETURN "Final choice made"
			BREAK
		
		CASE FLOWFLAG_MICHAEL_KILLED
			RETURN "Michael killed"
			BREAK
			
		CASE FLOWFLAG_TREVOR_KILLED
			RETURN "Trevor killed"
			BREAK
			
		CASE FLOWFLAG_RESPAWNED_AFTER_FINALE
			RETURN "Respawned after Finale"
			BREAK
			
		CASE FLOWFLAG_DIVING_SCRAPS_DONE
			RETURN "Diving Scraps Done"
			BREAK
			
		CASE FLOWFLAG_PLAYER_HAS_USED_FP_VIEW
			RETURN "Player has used FP view"
			BREAK
			
		CASE FLOWFLAG_MIKE_CON_BOUGHT
			RETURN "con site 'investment' for mike"
			BREAK
			
		CASE FLOWFLAG_FRANK_CON_BOUGHT
			RETURN "con site 'investment' for frank"
			BREAK
			
		CASE FLOWFLAG_TREV_CON_BOUGHT
			RETURN "con site 'investment' for trev"
			BREAK
			
		CASE FLOWFLAG_BEVERLY_SENT_WILDLIFE_TEXT
			RETURN "Beverly sent wildlife text"
			BREAK
			
		CASE FLOWFLAG_SENT_PROXIMITY_MINE_EMAIL
			RETURN "Sent proximity mine email"
			BREAK
			
		CASE FLOWFLAG_SENT_HOMING_MISSILE_EMAIL
			RETURN "Sent homing missile email"
			BREAK
			
		CASE FLOWFLAG_SENT_KNUCKLE_DUSTER_EMAIL
			RETURN "Sent knuckle duster email"
			BREAK
			
		CASE FLOWFLAG_SENT_COMBAT_PDW_EMAIL
			RETURN "Sent combat PDW email"
			BREAK
		
		CASE FLOWFLAG_SENT_MARKSMAN_PISTOL_EMAIL
			RETURN "Sent marksman pistol email"
			BREAK
		
		CASE FLOWFLAG_SENT_MACHETE_EMAIL
			RETURN "Sent machete email"
			BREAK
			
		CASE FLOWFLAG_SENT_MACHINE_PISTOL_EMAIL
			RETURN "Sent machine pistol email"
			BREAK
		
        CASE FLOWFLAG_NONE
            RETURN "ERROR (FLOWFLAG_NONE)"
			BREAK
            
        CASE MAX_FLOW_FLAG_IDS
            RETURN "ERROR (MAX_FLOW_FLAG_IDS)"
			BREAK
    ENDSWITCH
    
    SCRIPT_ASSERT("Get_Flag_Display_String_From_Flag_ID: Unknown Flow Flag ID - Needs Added")
    RETURN "Unknown"
#endif
#endif
ENDFUNC

// -----------------------------------------------------------------------------------------------------------
//      Mission Flow Standard Debug Output Flow Int Strings
// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Return the flow int as a string based on the FlowIntID
//
// INPUT PARAMS:    paramIntID              Flow Int ID
// RETURN VALUE:    STRING                  Debug display string for the Flow Int ID
#if USE_CLF_DLC
FUNC STRING GET_INT_DISPLAY_STRING_FROM_INT_IDCLF(FLOW_INT_IDS paramIntID)

    SWITCH (paramIntID)      

        CASE FLOWINT_NONE
            RETURN "ERROR (FLOWINT_NONE)"
			BREAK
			
		CASE FLOWINT_CLF_JETPACK_LEVEL
			RETURN "FLOWINT_CLF_JETPACK_LEVEL"
			BREAK
			
        CASE MAX_FLOW_INT_IDS_CLF
            RETURN "ERROR (MAX_FLOW_INT_IDS)"
			BREAK
        
        CASE MAX_FLOW_INT_IDS
            RETURN "ERROR (MAX_FLOW_INT_IDS)"
			BREAK
    ENDSWITCH
    
    SCRIPT_ASSERT("Get_Int_Display_String_From_Int_IDagt: Unknown Flow Int ID - Needs Added")
    RETURN "Unknown"

ENDFUNC
#endif
#if USE_NRM_DLC
FUNC STRING GET_INT_DISPLAY_STRING_FROM_INT_IDNRM(FLOW_INT_IDS paramIntID)

    SWITCH (paramIntID)
       
        CASE FLOWINT_NONE
            RETURN "ERROR (FLOWINT_NONE)"
			BREAK
        CASE MAX_FLOW_INT_IDS_NRM
            RETURN "ERROR (MAX_FLOW_INT_IDS)"
			BREAK   
        CASE MAX_FLOW_INT_IDS
            RETURN "ERROR (MAX_FLOW_INT_IDS)"
			BREAK
    ENDSWITCH
    
    SCRIPT_ASSERT("Get_Int_Display_String_From_Int_IDNRM: Unknown Flow Int ID - Needs Added")
    RETURN "Unknown"

ENDFUNC
#endif

FUNC STRING GET_INT_DISPLAY_STRING_FROM_INT_ID(FLOW_INT_IDS paramIntID)
#if USE_CLF_DLC
return GET_INT_DISPLAY_STRING_FROM_INT_IDCLF(paramIntID)
#endif
#if USE_NRM_DLC
return GET_INT_DISPLAY_STRING_FROM_INT_IDNRM(paramIntID)
#endif

#if not USE_CLF_DLC
#if not USE_NRM_DLC
    SWITCH (paramIntID)
        CASE FLOWINT_HEIST_MISSION_TRIGGER_HEIST
            RETURN "Heist Mission Trigger (Heist)"
			BREAK
            
        CASE FLOWINT_HEIST_MISSION_TRIGGER_MISSION
            RETURN "Heist Mission Trigger (Mission)"
			BREAK
            
        CASE FLOWINT_HEIST_BOARD_MODE_JEWEL
            RETURN "Board Mode Jewel"
			BREAK
            
        CASE FLOWINT_HEIST_BOARD_MODE_DOCKS
            RETURN "Board Mode Docks"
			BREAK
            
        CASE FLOWINT_HEIST_BOARD_MODE_RURAL
            RETURN "Board Mode Rural"
			BREAK
            
        CASE FLOWINT_HEIST_BOARD_MODE_AGENCY
            RETURN "Board Mode Agency"
			BREAK
            
        CASE FLOWINT_HEIST_BOARD_MODE_FINALE
            RETURN "Board Mode Finale"
			BREAK
            
        CASE FLOWINT_HEIST_CHOICE_JEWEL
            RETURN "Heist Choice Jewel"
			BREAK
            
        CASE FLOWINT_HEIST_CHOICE_DOCKS
            RETURN "Heist Choice Docks"
			BREAK
            
        CASE FLOWINT_HEIST_CHOICE_RURAL
            RETURN "Heist Choice Rural"
			BREAK
            
        CASE FLOWINT_HEIST_CHOICE_AGENCY
            RETURN "Heist Choice Agency"
			BREAK
            
        CASE FLOWINT_HEIST_CHOICE_FINALE
            RETURN "Heist Choice Finale"
			BREAK
			
		CASE FLOWINT_MISSION_CHOICE_FINALE
			RETURN "Mission Choice Finale"
			BREAK

        CASE FLOWINT_NONE
            RETURN "ERROR (FLOWINT_NONE)"
			BREAK
            
        CASE MAX_FLOW_INT_IDS
            RETURN "ERROR (MAX_FLOW_INT_IDS)"
			BREAK
    ENDSWITCH
    
    SCRIPT_ASSERT("Get_Int_Display_String_From_Int_ID: Unknown Flow Int ID - Needs Added")
    RETURN "Unknown"
#endif
#endif
ENDFUNC

#if USE_NRM_DLC
FUNC STRING GET_FLOWCHECK_DISPLAY_STRING(FLOW_CHECK_IDS paramCheck)

    SWITCH (paramCheck)
       
        CASE FLOW_CHECK_NONE
            RETURN "ERROR (FLOW_CHECK_NONE)"
			BREAK
		CASE FLOW_CHECK_TRUE
            RETURN "FLOW_CHECK_TRUE"
			BREAK	
		
		CASE FLOW_CHECK_NRM_NEED_ENG
            RETURN "FLOW_CHECK_NRM_NEED_ENG"
			BREAK 
		CASE FLOW_CHECK_NRM_NEED_MEDIC
            RETURN "FLOW_CHECK_NRM_NEED_MEDIC"
			BREAK	
        CASE FLOW_CHECK_NRM_NEED_GUNMAN
            RETURN "FLOW_CHECK_NRM_NEED_GUNMAN"
			BREAK 
		
		CASE FLOW_CHECK_NRM_NEED_FUEL
            RETURN "FLOW_CHECK_NRM_NEED_FUEL"
			BREAK 
		CASE FLOW_CHECK_NRM_NEED_AMMO
            RETURN "FLOW_CHECK_NRM_NEED_AMMO"
			BREAK	
        CASE FLOW_CHECK_NRM_NEED_MEDS
            RETURN "FLOW_CHECK_NRM_NEED_MEDS"
			BREAK 
		CASE FLOW_CHECK_NRM_NEED_FOOD
            RETURN "FLOW_CHECK_NRM_NEED_FOOD"
			BREAK	
			
		CASE MAX_FLOW_CHECK_IDS_NRM
            RETURN "MAX_FLOW_CHECK_IDS_NRM"
			BREAK	
		CASE MAX_FLOW_CHECK_IDS
            RETURN "MAX_FLOW_CHECK_IDS"
			BREAK 	
    ENDSWITCH
    
    SCRIPT_ASSERT("Get_Int_Display_String_From_Int_IDNRM: Unknown Flow Int ID - Needs Added")
    RETURN "Unknown"

ENDFUNC
#endif

FUNC TEXT_LABEL_7 GET_MISSION_STAT_ID_FROM_SCRIPT_HASH(INT paramScriptHash)
	SP_MISSIONS eMissionID = GET_SP_MISSION_ID_FOR_SCRIPT_HASH(paramScriptHash, TRUE)
	
	IF eMissionID = SP_MISSION_NONE
		SCRIPT_ASSERT("GET_MISSION_STAT_ID_FROM_SCRIPT_HASH: Failed to find a mission data entry for the passed script hash.")
		TEXT_LABEL_7 tFailed = "NONE"
		RETURN tFailed
	ENDIF
	RETURN g_sMissionStaticData[eMissionID].statID
ENDFUNC

// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Get the character ID associated with a given character display string.
//
// INPUT PARAMS:    paramDisplayString      A character's display string.
// RETURN VALUE:    STRANDS              The character ID enum that is associated with the display string. 
//                                          Returns CHAR_BLANK_ENTRY if the string did not match any character IDs.
FUNC enumCharacterList GET_CHARSHEET_ID_FROM_DISPLAY_STRINGCLF(STRING paramDisplayString)

    IF NOT IS_STRING_NULL(paramDisplayString)
        INT iCharsheetIndex
        REPEAT MAX_CLF_CHARACTERS iCharsheetIndex
            IF ARE_STRINGS_EQUAL(paramDisplayString, GET_CHARSHEET_DISPLAY_STRING_FROM_CHARSHEETCLF(INT_TO_ENUM(enumCharacterList,iCharsheetIndex), TRUE) )
                RETURN INT_TO_ENUM(enumCharacterList, iCharsheetIndex)
            ENDIF
        ENDREPEAT
    ENDIF
    
    RETURN CHAR_BLANK_ENTRY

ENDFUNC
FUNC enumCharacterList GET_CHARSHEET_ID_FROM_DISPLAY_STRINGNRM(STRING paramDisplayString)

    IF NOT IS_STRING_NULL(paramDisplayString)
        INT iCharsheetIndex
        REPEAT MAX_NRM_CHARACTERS iCharsheetIndex
            IF ARE_STRINGS_EQUAL(paramDisplayString, GET_CHARSHEET_DISPLAY_STRING_FROM_CHARSHEETNRM(INT_TO_ENUM(enumCharacterList,iCharsheetIndex), TRUE) )
                RETURN INT_TO_ENUM(enumCharacterList, iCharsheetIndex)
            ENDIF
        ENDREPEAT
    ENDIF
    
    RETURN CHAR_BLANK_ENTRY

ENDFUNC
FUNC enumCharacterList GET_CHARSHEET_ID_FROM_DISPLAY_STRING(STRING paramDisplayString)

	#if USE_CLF_DLC
		return GET_CHARSHEET_ID_FROM_DISPLAY_STRINGCLF(paramDisplayString)
	#endif
	
	#if USE_NRM_DLC
		return GET_CHARSHEET_ID_FROM_DISPLAY_STRINGNRM(paramDisplayString)
	#endif


    IF NOT IS_STRING_NULL(paramDisplayString)
        INT iCharsheetIndex
		INT iMaxCharacter = ENUM_TO_INT(GLOBAL_CHARACTER_SHEET_GET_MAX_CHARACTERS_FOR_GAMEMODE())
        REPEAT iMaxCharacter iCharsheetIndex
            IF ARE_STRINGS_EQUAL(paramDisplayString, GET_CHARSHEET_DISPLAY_STRING_FROM_CHARSHEET(INT_TO_ENUM(enumCharacterList,iCharsheetIndex), TRUE) )
                RETURN INT_TO_ENUM(enumCharacterList, iCharsheetIndex)
            ENDIF
        ENDREPEAT
    ENDIF
    
    RETURN CHAR_BLANK_ENTRY

ENDFUNC


// ===========================================================================================================
//		Clear Mission Flow Debug Globals
// ===========================================================================================================

// PURPOSE:	Resets the variables containing the mission flow launch and forget display on teh F9 screen
// 
PROC CLEAR_MISSION_FLOW_DEBUG_LAUNCH_AND_FORGET_DISPLAY_VARIABLES()

	INT tempLoop = 0
	
	REPEAT DEBUG_MAX_LAUNCH_AND_FORGET_SCRIPTS tempLoop
		g_debugLaunchAndForgetScripts[tempLoop].filename	= ""
		g_debugLaunchAndForgetScripts[tempLoop].theThreadID	= NULL
	ENDREPEAT
	
ENDPROC

// ===========================================================================================================
//      Launch and Forget Script Tracking Debug
// ===========================================================================================================

// PURPOSE: Looks for a free space in the Launch and Forget tracking array and stores the details for display
//          on F9 screen
//
// INPUT PARAMS:    paramFilename       Script filename
//                  paramThreadID       Thread ID for script

PROC STORE_DEBUG_DISPLAY_LAUNCH_AND_FORGET_FILENAME(TEXT_LABEL_31 paramFilename, THREADID paramThreadID)

    INT tempLoop = 0
    
    REPEAT DEBUG_MAX_LAUNCH_AND_FORGET_SCRIPTS tempLoop
        IF (g_debugLaunchAndForgetScripts[tempLoop].theThreadID = NULL)
            g_debugLaunchAndForgetScripts[tempLoop].theThreadID = paramThreadID
            g_debugLaunchAndForgetScripts[tempLoop].filename    = paramFilename
            
            EXIT
        ENDIF
    ENDREPEAT
    
    STRING theError = "Store_Debug_Display_Launch_And_Forget_Filename: No freespace for new filenames. Increase DEBUG_MAX_LAUNCH_AND_FORGET_SCRIPTS"
   // DISPLAY_MISSION_FLOW_ASSERT_MESSAGE_WITH_STRING(theError, paramFilename)
    SCRIPT_ASSERT(theError)

ENDPROC


// ===========================================================================================================
//      Automatic Flow Datafile Generation.
// ===========================================================================================================

PROC DEBUG_EXPORT_SP_RICH_PRESENCE_ORDER()
	STRING strPath = "X:/gta5/titelupdate/dev_ng/common/data/script/"
	STRING strFile = "SP_rich_presence.txt"

	CLEAR_NAMED_DEBUG_FILE	(strPath, strFile)
	OPEN_NAMED_DEBUG_FILE	(strPath, strFile)
	
	//Export story mission XLAST values.
	INT iDataIndex
	REPEAT SP_MISSION_MAX iDataIndex
		//Compose mission name label.
		TEXT_LABEL_7 tMissionNameLabel = GET_SP_MISSION_NAME_LABEL(INT_TO_ENUM(SP_MISSIONS, iDataIndex))
		
		//Compose rich presence info lines.
		TEXT_LABEL_63 tLine = "[SPM_"
		tLine += iDataIndex
		tLine += "_STR]"
		SAVE_STRING_TO_NAMED_DEBUG_FILE(tLine, strPath, strFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(strPath, strFile)
		
		STRING strMissionName
		#if USE_CLF_DLC
		strMissionName = GET_STRING_FROM_TEXT_FILE(tMissionNameLabel)
		#endif
		#if USE_NRM_DLC
		strMissionName = GET_STRING_FROM_TEXT_FILE(tMissionNameLabel)
		#endif
		
		#if not USE_CLF_DLC
		#if not USE_NRM_DLC
		SWITCH INT_TO_ENUM(SP_MISSIONS, iDataIndex)
			CASE SP_MISSION_FINALE_A
			CASE SP_MISSION_FINALE_B
			CASE SP_MISSION_FINALE_C1
			CASE SP_MISSION_FINALE_C2
			CASE SP_MISSION_FINALE_CREDITS
				strMissionName = "The Finale"
			BREAK
			DEFAULT
				strMissionName = GET_STRING_FROM_TEXT_FILE(tMissionNameLabel)
			BREAK
		ENDSWITCH
		#endif
		#endif
		
		IF GET_LENGTH_OF_LITERAL_STRING(strMissionName) > 44
			tLine = GET_STRING_FROM_STRING(strMissionName,0,41)
			tLine += "..."
		ELSE
			tLine = strMissionName
		ENDIF
		SAVE_STRING_TO_NAMED_DEBUG_FILE(tLine, strPath, strFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(strPath, strFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(strPath, strFile)
	ENDREPEAT
	
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(strPath, strFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(strPath, strFile)
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
	//Export RC mission XLAST values.
	REPEAT MAX_RC_MISSIONS iDataIndex
		//Compose mission name label.
		TEXT_LABEL_7 tNameLabel = GET_RC_MISSION_NAME_LABEL(INT_TO_ENUM(g_eRC_MissionIDs, iDataIndex))
		
		//Compose rich presence info lines.
		TEXT_LABEL_63 tLine = "[SPRC_"
		tLine += iDataIndex
		tLine += "_STR]"
		SAVE_STRING_TO_NAMED_DEBUG_FILE(tLine, strPath, strFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(strPath, strFile)
		
		STRING strRCName = GET_STRING_FROM_TEXT_FILE(tNameLabel)
		IF GET_LENGTH_OF_LITERAL_STRING(strRCName) > 44
			tLine = GET_STRING_FROM_STRING(strRCName,0,41)
			tLine += "..."
		ELSE
			tLine = strRCName
		ENDIF
		SAVE_STRING_TO_NAMED_DEBUG_FILE(tLine, strPath, strFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(strPath, strFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(strPath, strFile)
	ENDREPEAT
	#endif
	#endif
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(strPath, strFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(strPath, strFile)
	
	//Export minigame XLAST values.
	REPEAT MAX_MINIGAMES iDataIndex
		//Compose mission name label.
		TEXT_LABEL_7 tNameLabel = GET_MINIGAME_NAME_LABEL(INT_TO_ENUM(SP_MINIGAMES, iDataIndex))
		
		//Compose rich presence info lines.
		TEXT_LABEL_63 tLine = "[SPMG_"
		tLine += iDataIndex
		tLine += "_STR]"
		SAVE_STRING_TO_NAMED_DEBUG_FILE(tLine, strPath, strFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(strPath, strFile)
		
		STRING strName = GET_STRING_FROM_TEXT_FILE(tNameLabel)
		IF GET_LENGTH_OF_LITERAL_STRING(strName) > 44
			tLine = GET_STRING_FROM_STRING(strName,0,41)
			tLine += "..."
		ELSE
			tLine = strName
		ENDIF
		SAVE_STRING_TO_NAMED_DEBUG_FILE(tLine, strPath, strFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(strPath, strFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(strPath, strFile)
	ENDREPEAT
	
	CLOSE_DEBUG_FILE()
ENDPROC


PROC DEBUG_WRITE_COMPLETION_ORDER_STAT_XML_LINE(TEXT_LABEL_15 paramStatName, SP_MISSIONS paramMissionID, STRING paramPath, STRING paramFile)
	//Example line of XML.
	//<stat Name="FL_CO_AH1"				Type="int"   	 Group="11"  SaveCategory="0" Default="0"  online="false"   profile="true"  FlushPriority="0" ServerAuthoritative="false"   community="false"  UserData="STAT_FLAG_NEVER_SHOW"  Owner="script"  Comment="Flow completion order stat for mission AGENCY_1" 		/>	

	STRING strLineStart = "<stat Name=\""
	STRING strLineMidA = "\"				Type=\"int\"   	 Group=\"11\"  SaveCategory=\"0\" Default=\"0\"  online=\"false\"   profile=\"true\"  FlushPriority=\"0\" "
	STRING strLineMidB = "ServerAuthoritative=\"false\"   community=\"false\"  UserData=\"STAT_FLAG_NEVER_SHOW\"  Owner=\"script\"  Comment=\"Flow completion order stat for mission "
	STRING strLineEnd = "\" 		/>"

	//Write opening of XML line to file.
	SAVE_STRING_TO_NAMED_DEBUG_FILE(strLineStart, paramPath, paramFile)
	//Write stat name to file.
	SAVE_STRING_TO_NAMED_DEBUG_FILE(paramStatName, paramPath, paramFile)
	//Write middle of line to file.
	SAVE_STRING_TO_NAMED_DEBUG_FILE(strLineMidA, paramPath, paramFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE(strLineMidB, paramPath, paramFile)
	//Write mission name to file.
	SAVE_STRING_TO_NAMED_DEBUG_FILE(GET_SP_MISSION_DISPLAY_STRING_FROM_ID(paramMissionID) , paramPath, paramFile)
	//Write end of XML line to file.
	SAVE_STRING_TO_NAMED_DEBUG_FILE(strLineEnd, paramPath, paramFile)
	//Break to new line.
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(paramPath, paramFile)
ENDPROC


PROC DEBUG_EXPORT_SP_MISSION_COMPLETION_ORDER_STATS()

	STRING strPath = "X:/gta5/titelupdate/dev_ng/common/data/script/"
	STRING strFile = "SP_co_stats.txt"

	CLEAR_NAMED_DEBUG_FILE	(strPath, strFile)
	OPEN_NAMED_DEBUG_FILE	(strPath, strFile)

	INT iMissionDataIndex
	REPEAT SP_MISSION_MAX iMissionDataIndex
		IF NOT IS_BIT_SET(g_sMissionStaticData[iMissionDataIndex].settingsBitset, MF_INDEX_NO_COMP_ORDER)
			SP_MISSIONS eMissionID = INT_TO_ENUM(SP_MISSIONS, iMissionDataIndex)
			#if USE_CLF_DLC
				//Compose stat name label.
				TEXT_LABEL_15 txtStatName = "FL_CO_"
				txtStatName += g_sMissionStaticData[iMissionDataIndex].statID
				DEBUG_WRITE_COMPLETION_ORDER_STAT_XML_LINE(txtStatName, eMissionID, strPath, strFile)
			#endif
			#if USE_NRM_DLC
				//Compose stat name label.
				TEXT_LABEL_15 txtStatName = "FL_CO_"
				txtStatName += g_sMissionStaticData[iMissionDataIndex].statID
				DEBUG_WRITE_COMPLETION_ORDER_STAT_XML_LINE(txtStatName, eMissionID, strPath, strFile)
			#endif
			#if not USE_CLF_DLC
			#if not USE_NRM_DLC
				IF eMissionID = SP_HEIST_JEWELRY_2
					//Special case. Create two stats for the Jewel Heist finale.
					TEXT_LABEL_15 txtStatName = "FL_CO_"
					txtStatName += g_sMissionStaticData[iMissionDataIndex].statID
					txtStatName += "A"
					DEBUG_WRITE_COMPLETION_ORDER_STAT_XML_LINE(txtStatName, eMissionID, strPath, strFile)
					txtStatName = "FL_CO_"
					txtStatName += g_sMissionStaticData[iMissionDataIndex].statID
					txtStatName += "B"
					DEBUG_WRITE_COMPLETION_ORDER_STAT_XML_LINE(txtStatName, eMissionID, strPath, strFile)
				ELSE	
					IF eMissionID = SP_MISSION_FBI_4_PREP_4
						//Add an extra stat for prep 3 before generating prep 4.
						TEXT_LABEL_15 txtStatName = "FL_CO_"
						txtStatName += g_sMissionStaticData[SP_MISSION_FBI_4_PREP_3].statID
						DEBUG_WRITE_COMPLETION_ORDER_STAT_XML_LINE(txtStatName, SP_MISSION_FBI_4_PREP_3, strPath, strFile)
					ENDIF
					//Compose stat name label.
					TEXT_LABEL_15 txtStatName = "FL_CO_"
					txtStatName += g_sMissionStaticData[iMissionDataIndex].statID
					DEBUG_WRITE_COMPLETION_ORDER_STAT_XML_LINE(txtStatName, eMissionID, strPath, strFile)
				ENDIF
			#endif
			#endif
		ENDIF
	ENDREPEAT
	
	CLOSE_DEBUG_FILE()
ENDPROC
