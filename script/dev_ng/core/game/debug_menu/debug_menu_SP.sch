USING "debug_menu_shared_core.sch"

USING "commands_ped.sch"
USING "commands_camera.sch"
USING "commands_interiors.sch"
USING "commands_streaming.sch"
USING "script_player.sch"
USING "cellphone_public.sch"
USING "flow_launcher_main_core.sch"
USING "selector_public.sch"
USING "email_public.sch"
USING "player_ped_public.sch"
USING "family_private.sch"
USING "finance_control_public.sch"
USING "friends_public.sch"
USING "flow_public_core_override.sch"
USING "commands_brains.sch"
USING "randomChar_Public.sch"
USING "mission_repeat_public.sch"
USING "flow_diagram_core.sch"
USING "vehicle_gen_public.sch"
USING "blip_control_public.sch"
USING "building_control_public.sch"
USING "RC_Setup_public.sch"
USING "common_packages.sch"
USING "website_public.sch"
USING "properties_public.sch"
USING "random_event_private.sch"
USING "code_control_data_GTA5.sch"
USING "director_mode_public.sch"


/// PURPOSE: Cleans up any mission scripts that may be running
PROC DEBUG_MENU_CLEANUP_RUNNING_MISSION_SCRIPTS(DEBUG_MENU_DATA_STRUCT &sDebugMenuData)

	//Don't clean up if it is one of these item types.
	IF sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].iType = DM_HASH_ITEM_INPUT 
	OR sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].iType = DM_HASH_ITEM_CODE 
		EXIT
	ENDIF
	IF sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].iType = DM_HASH_ITEM_WARP
		IF IS_STRING_NULL_OR_EMPTY(sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].sMissionID)
		AND (sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].iLabelID = 0)
			EXIT
		ENDIF
	ENDIF

	// Trigger the cleanup of any scripts that are specifically waiting for the 
	// FORCE_CLEANUP_FLAG_DEBUG_MENU flag.
	FORCE_CLEANUP(FORCE_CLEANUP_FLAG_DEBUG_MENU)
	
	//Now clean up all mission scripts.
	Cleanup_Running_Mission_Scripts()
ENDPROC


/// PURPOSE: Sets the appropriate player character
PROC SP_SET_CHARACTER(DEBUG_MENU_DATA_STRUCT &sDebugMenuData)
	
	INT iCharacter = sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].iCharacter

	IF iCharacter =  GET_HASH_KEY("CHAR_MICHAEL")
		WHILE NOT SET_CURRENT_SELECTOR_PED(SELECTOR_PED_MICHAEL)
			WAIT(0)
		ENDWHILE
		SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_DEFAULT, FALSE)
		STORE_PLAYER_PED_VARIATIONS(PLAYER_PED_ID(), TRUE)
	ELIF iCharacter = GET_HASH_KEY("CHAR_TREVOR")
		WHILE NOT SET_CURRENT_SELECTOR_PED(SELECTOR_PED_TREVOR)
			WAIT(0)
		ENDWHILE
		SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_DEFAULT, FALSE)
		STORE_PLAYER_PED_VARIATIONS(PLAYER_PED_ID(), TRUE)
	ELIF iCharacter = GET_HASH_KEY("CHAR_FRANKLIN")
		WHILE NOT SET_CURRENT_SELECTOR_PED(SELECTOR_PED_FRANKLIN)
			WAIT(0)
		ENDWHILE
		SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_DEFAULT, FALSE)
		STORE_PLAYER_PED_VARIATIONS(PLAYER_PED_ID(), TRUE)
	ENDIF
	
	// If we have selected a mission or script to launch then we should activate the per character filters.
	IF NOT g_bUseCharacterFilters
		IF sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].iType = DM_HASH_ITEM_MISSION
		OR sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].iType = DM_HASH_ITEM_BUGSTAR
		OR sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].iType = DM_HASH_ITEM_SCRIPT
		OR sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].iType = DM_HASH_ITEM_LAUNCHER
			g_bUseCharacterFilters = TRUE
			UPDATE_PLAYER_PED_TIMECYCLE_MODIFIER()
		ENDIF
	ENDIF
	
ENDPROC


PROC DEBUG_MENU_RUN_ITEM_CUSTOM(DEBUG_MENU_DATA_STRUCT &sDebugMenuData)
	IF NOT g_savedGlobals.sFlow.isGameflowActive
		RESET_GAMEFLOW()
	ENDIF
	// ...perform any character swaps associated with menu item.
	SP_Set_Character(sDebugMenuData)
ENDPROC


PROC DO_RANDOM_EVENT_CHARACTER_SELECTION(SP_RANDOM_EVENTS eRandomEvent, INT iVariation)

	enumCharacterList eSelectedChar = NO_CHARACTER
	
	//Get mission name label.
	TEXT_LABEL_7 tMissionName = GET_RANDOM_EVENT_NAME_LABEL(eRandomEvent)
	
	//State tracking variables.
	BOOL bMenuActive = TRUE
	INT iMenuTimeLastSelectionChange = 0
	INT iStickLeftY
	INT temp
	
	//Get screen aspect ratio.
	INT iScreenX
	INT iScreenY
	GET_SCREEN_RESOLUTION(iScreenX,iScreenY)
	FLOAT fAspectRatio = TO_FLOAT(iScreenY) / TO_FLOAT(iScreenX)
	
	//Store useful dimensions.
	FLOAT fMenuWidth = 0.175
	FLOAT fMenuRowHeight = 0.035
	FLOAT fMenuHeight = fMenuRowHeight*3.0
	FLOAT fMenuRowZeroY = 0.5 - (0.5*fMenuHeight) + (0.5*fMenuRowHeight)
	
	//Find valid starting selection and check if there is more than one character available.
	INT iMenuSelected = 0
	INT iValidCharCount = 0
	INT iCharIndex
	
	FOR iCharIndex = 2 TO 0 STEP -1
		IF SHOULD_RANDOM_EVENT_START_FOR_PLAYER_PED(eRandomEvent, iVariation, INT_TO_ENUM(enumCharacterList, iCharIndex))
			iValidCharCount++
			iMenuSelected = iCharIndex
		ENDIF
	ENDFOR
	
	IF iValidCharCount = 0
		SCRIPT_ASSERT("DO_RANDOM_EVENT_CHARACTER_SELECTION: There didn't seem to be any valid triggerable characters for this random event. Tell PaulD.")
	ELIF iValidCharCount = 1
		CPRINTLN(DEBUG_RANDOM_EVENTS, "Only one character is available for this event. Auto selected ", GET_CHARSHEET_DISPLAY_STRING_FROM_CHARSHEET(INT_TO_ENUM(enumCharacterList, iMenuSelected)), ".")
		eSelectedChar = INT_TO_ENUM(enumCharacterList, iMenuSelected)
		//RETURN INT_TO_ENUM(enumCharacterList, iMenuSelected)
	ENDIF
	
	IF eSelectedChar = NO_CHARACTER
	
		//Start displaying help text.
		CLEAR_HELP()
		PRINT_HELP_FOREVER("CHAR_SEL_H")
		
		SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
	
		//Run menu.
		WHILE bMenuActive
		
			DISABLE_CELLPHONE_THIS_FRAME_ONLY()
			DISABLE_SELECTOR_THIS_FRAME()
			
			//Draw background layers.
			DRAW_RECT(0.5, 0.5 - fMenuRowHeight, fMenuWidth + (0.012*fAspectRatio), fMenuHeight + (fMenuRowHeight*2) + 0.012, 0,0,0,255)
			DRAW_RECT(0.5, 0.5, fMenuWidth, fMenuHeight , 10,10,10,255)
			
			//Draw selection highlight.
			IF iMenuSelected < 0 OR iMenuSelected > 2 OR NOT SHOULD_RANDOM_EVENT_START_FOR_PLAYER_PED(eRandomEvent, iVariation, INT_TO_ENUM(enumCharacterList, iMenuSelected))
				// Disabled
				DRAW_RECT(0.5, fMenuRowZeroY + (fMenuRowHeight*TO_FLOAT(iMenuSelected)), fMenuWidth, fMenuRowHeight , 100, 100, 100, 100)
			ELSE
				// Enabled
				DRAW_RECT(0.5, fMenuRowZeroY + (fMenuRowHeight*TO_FLOAT(iMenuSelected)), fMenuWidth, fMenuRowHeight , 100, 100, 100, 255)
			ENDIF
			
			//Draw menu text.
			CONFIGURE_CHARACTER_MENU_TEXT(255,255,255,255)
			DISPLAY_TEXT(0.5 - (GET_STRING_WIDTH(tMissionName)*0.5), fMenuRowZeroY-(fMenuRowHeight*2)-0.015, tMissionName)
			CONFIGURE_CHARACTER_MENU_TEXT(255,255,255,255)
			DISPLAY_TEXT(0.5 - (GET_STRING_WIDTH("CHAR_SEL")*0.5), fMenuRowZeroY-fMenuRowHeight-0.015, "CHAR_SEL")
			enumCharacterList eChar
			REPEAT 3 iCharIndex
				eChar = INT_TO_ENUM(enumCharacterList, iCharIndex)
				IF SHOULD_RANDOM_EVENT_START_FOR_PLAYER_PED(eRandomEvent, iVariation, eChar)
					CONFIGURE_CHARACTER_MENU_TEXT(255, 255, 255, 255)
				ELSE
					CONFIGURE_CHARACTER_MENU_TEXT(150, 150, 150, 150)
				ENDIF
				STRING strCharText
				IF iCharIndex = ENUM_TO_INT(CHAR_MICHAEL)
					strCharText = "CHAR_SEL_M"
				ELIF iCharIndex = ENUM_TO_INT(CHAR_FRANKLIN)
					strCharText = "CHAR_SEL_F"
				ELIF iCharIndex = ENUM_TO_INT(CHAR_TREVOR)
					strCharText = "CHAR_SEL_T"
				ENDIF
				DISPLAY_TEXT(0.5-(GET_STRING_WIDTH(strCharText)*0.5), fMenuRowZeroY + (fMenuRowHeight*TO_FLOAT(iCharIndex))-0.015, strCharText)
			ENDREPEAT
			
			//Manage menu input.
			//Check for menu deactivation input.
			IF IS_BUTTON_PRESSED(PAD1, CROSS)
			OR IS_BUTTON_PRESSED(PAD1, CIRCLE)
			OR IS_BUTTON_PRESSED(PAD1, TRIANGLE)
			OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_RETURN)
			OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_ESCAPE)
			OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
				bMenuActive = FALSE
			ENDIF
			
			//Check for selection change input.
			IF (GET_GAME_TIMER() - iMenuTimeLastSelectionChange) > 250
				//Get analogue stick positons.
				GET_POSITION_OF_ANALOGUE_STICKS(PAD1,temp,iStickLeftY,temp,temp)
				//Check for up input.
				IF IS_BUTTON_PRESSED(PAD1, DPADUP)
				OR IS_KEYBOARD_KEY_PRESSED(KEY_UP)
				OR iStickLeftY < -30
					iMenuSelected--
					IF iMenuSelected < 0
						iMenuSelected = 2
					ENDIF
					WHILE NOT SHOULD_RANDOM_EVENT_START_FOR_PLAYER_PED(eRandomEvent, iVariation, INT_TO_ENUM(enumCharacterList, iMenuSelected))
						iMenuSelected--
						IF iMenuSelected < 0
							iMenuSelected = 2
						ENDIF
					ENDWHILE
					iMenuTimeLastSelectionChange = GET_GAME_TIMER()
				ENDIF
				//Check for down input.
				IF IS_BUTTON_PRESSED(PAD1, DPADDOWN)
				OR IS_KEYBOARD_KEY_PRESSED(KEY_DOWN)
				OR iStickLeftY > 30
					iMenuSelected++
					IF iMenuSelected > 2
						iMenuSelected = 0
					ENDIF
					WHILE NOT SHOULD_RANDOM_EVENT_START_FOR_PLAYER_PED(eRandomEvent, iVariation, INT_TO_ENUM(enumCharacterList, iMenuSelected))
						iMenuSelected++
						IF iMenuSelected > 2
							iMenuSelected = 0
						ENDIF
					ENDWHILE
					iMenuTimeLastSelectionChange = GET_GAME_TIMER()
				ENDIF
			ENDIF
			WAIT(0)
		ENDWHILE
		
		eSelectedChar = INT_TO_ENUM(enumCharacterList, iMenuSelected)
		
	ENDIF
	
	IF eSelectedChar != NO_CHARACTER
	AND eSelectedChar != GET_CURRENT_PLAYER_PED_ENUM()
		SELECTOR_SLOTS_ENUM eSelectorTriggerChar
		SWITCH eSelectedChar
			CASE CHAR_MICHAEL
				eSelectorTriggerChar = SELECTOR_PED_MICHAEL
			BREAK
			CASE CHAR_FRANKLIN
				eSelectorTriggerChar = SELECTOR_PED_FRANKLIN
			BREAK
			CASE CHAR_TREVOR
				eSelectorTriggerChar = SELECTOR_PED_TREVOR
			BREAK
			DEFAULT
				SCRIPT_ASSERT("DO_RANDOM_EVENT_CHARACTER_SELECTION: Character selection returned an invalid player character. Tell PaulD.")
			BREAK	
		ENDSWITCH
		WHILE NOT SET_CURRENT_SELECTOR_PED(eSelectorTriggerChar)
			WAIT(0)
		ENDWHILE
		SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_DEFAULT, FALSE)
		STORE_PLAYER_PED_VARIATIONS(PLAYER_PED_ID(), TRUE)
	ENDIF
	
	CLEAR_HELP()
	
	SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	
ENDPROC

/// PURPOSE: For Random Events, warps the player away from the warp location to allow the script
///    			to reload when the real warp takes place. Clear all random event timestamps.
PROC SP_PERFORM_ANY_SPECIAL_RANDOM_EVENT_CODE(DEBUG_MENU_DATA_STRUCT &sDebugMenuData)

	//Cleanup any REs then wait a few frames to allow the old script to tidy up.
	FORCE_CLEANUP(FORCE_CLEANUP_FLAG_RANDOM_EVENTS)
	WAIT(500)
	
	//Setting chosen script name
	g_sRandomEventDebugScriptRequested = SP_Get_Latest_Selection()
	CPRINTLN(DEBUG_RANDOM_EVENTS, "Setting  g_sRandomEventDebugScriptRequested to ", g_sRandomEventDebugScriptRequested)
	
	// XML is selecting a random event variation.
	g_iRandomEventDebugVarRequested = sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].iRank
	
	DO_RANDOM_EVENT_CHARACTER_SELECTION(GET_RANDOM_EVENT_ENUM_FROM_SCRIPT_NAME(g_sRandomEventDebugScriptRequested), g_iRandomEventDebugVarRequested)
	
	//Flag all shops as visited twice so the shop robbery Random Events work first time.
	INT iShopIndex
	REPEAT NUMBER_OF_SHOPS iShopIndex
		SET_SHOP_BIT(INT_TO_ENUM(SHOP_NAME_ENUM, iShopIndex), SHOP_NS_FLAG_CAN_ROBBERY_TAKE_PLACE_IN_SHOP, TRUE)
	ENDREPEAT
	
	IF ARE_STRINGS_EQUAL("altruist_cult", g_sRandomEventDebugScriptRequested)
		PRINTLN("SP_Perform_Any_Special_Random_Event_Code: Debug launching altruist cult script. Setting number of delivered peds to 10.")
		g_savedGlobals.sRandomEventData.iNumberOfDeliveredPeds = 10
	ENDIF
	
	g_iLastRandomEventLaunch = GET_GAME_TIMER() - iMinTimeBetweenLaunches
	
	// Refresh all world points to ensure they retrigger.
	REACTIVATE_ALL_WORLD_BRAINS_THAT_ARE_WAITING_TILL_OUT_OF_RANGE()
	REACTIVATE_ALL_OBJECT_BRAINS_THAT_ARE_WAITING_TILL_OUT_OF_RANGE()
	
	PRINTLN("SP_Perform_Any_Special_Random_Event_Code - World points refreshed to allow retriggering of event.")
	
ENDPROC

/// PURPOSE: For Random Characters, warps the player away from the warp location to allow the script
///    		to reload when the real warp takes place.

PROC SP_PERFORM_ANY_SPECIAL_RANDOM_CHARACTER_CODE()

	g_bRandomCharsAvailableInDebug = TRUE
	
	IF IS_PLAYER_PLAYING(PLAYER_ID())
		CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
	ENDIF
	
	STRING filenameStr = SP_Get_Latest_Selection()
	INT filenameHash = GET_HASH_KEY(filenameStr)
	BOOL bAmbientPassScript
	g_eRC_MissionIDs eRCMission = NO_RC_MISSION
	g_structRCMissionsStatic sRCMissionDetails
	g_structRCMissionsStatic sRCTempMissionDetails
	g_eRC_MissionIDs eRCTempMission
	INT iMission
	
	// Search all the Random Character missions looking for this filenameHash
	REPEAT MAX_RC_MISSIONS iMission
		Retrieve_Random_Character_Static_Mission_Details(INT_TO_ENUM(g_eRC_MissionIDs, iMission), sRCMissionDetails)
		IF (GET_HASH_KEY(sRCMissionDetails.rcScriptName) = filenameHash)
		OR (GET_HASH_KEY(sRCMissionDetails.rcAmbientPassScript) = filenameHash)
			bAmbientPassScript = (GET_HASH_KEY(sRCMissionDetails.rcAmbientPassScript) = filenameHash)
			eRCMission = (INT_TO_ENUM(g_eRC_MissionIDs, iMission))
			iMission = ENUM_TO_INT(MAX_RC_MISSIONS)+1
		ENDIF
	ENDREPEAT
	
	// Activate the random character mission
	IF eRCMission <> NO_RC_MISSION
		
		// Keep track of completed rc missions that belong to the same group
		INT iSubMissionsComplete
		INT iSubMissionsCompleted[10]
		INT iSubMissionFlags[10]
		IF sRCMissionDetails.rcMissionGroup <> NO_RC_MISSION_GROUP
			
			REPEAT MAX_RC_MISSIONS iMission
				eRCTempMission = INT_TO_ENUM(g_eRC_MissionIDs, iMission)
				IF (eRCTempMission <> eRCMission) // Ignore this mission
					Retrieve_Random_Character_Static_Mission_Details(eRCTempMission, sRCTempMissionDetails)
					// In the same group?
					IF (sRCTempMissionDetails.rcMissionGroup = sRCMissionDetails.rcMissionGroup)
						iSubMissionsCompleted[iSubMissionsComplete] = iMission
						iSubMissionFlags[iSubMissionsComplete] = g_savedGlobals.sRandomChars.savedRC[eRCTempMission].rcFlags
						iSubMissionsComplete++
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
		
		// Only do reset if we're not in gameflow mode (flow launch will do the reset in that case)
		IF NOT g_savedGlobals.sFlow.isGameflowActive
			// Reset all the saved data so no other rc mission interferes with this one
			Reset_Random_Character_Stored_Mission_Details()
		ENDIF
		
		// Put the player in an outfit that will allow him to play this mission
		RC_UPDATE_PLAYER_OUTFIT(eRCMission)
		
		// Set mission as Activated and Ready To Play
		// If we are launching the pass script, set mission as Completed
		Activate_RC_Mission(eRCMission, bAmbientPassScript)
		
		// Reinstate tracked sub mission states
		REPEAT iSubMissionsComplete iMission
			g_savedGlobals.sRandomChars.savedRC[iSubMissionsCompleted[iMission]].rcFlags = iSubMissionFlags[iMission]
			// unlock all of the submissions
			Activate_RC_Mission(eRCMission, FALSE)
		ENDREPEAT
		
		// Set time of day if required. Note, debug menu will override this
		IF sRCMissionDetails.rcStartTime != 0000 OR sRCMissionDetails.rcEndTime != 2359
			INT iHour = (sRCMissionDetails.rcStartTime/100)
			INT iMinute = (sRCMissionDetails.rcStartTime%100) 
			ADVANCE_CLOCK_TIME_TO(iHour, iMinute, 0)
		ENDIF
		
		g_bResetRandomCharDebugRange = TRUE
		REACTIVATE_ALL_WORLD_BRAINS_THAT_ARE_WAITING_TILL_OUT_OF_RANGE()
		
		// Setup flowflags and any other requirements needed when debug launching mission
		SWITCH (eRCMission)
			
			// Abigail
			CASE RC_ABIGAIL_2
				CPRINTLN(DEBUG_RANDOM_CHAR, "ABIGAIL 2: PRE FLOW FLAG SET")
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_DIVING_SCRAPS_DONE, TRUE)
			FALLTHRU

			CASE RC_ABIGAIL_1
				CPRINTLN(DEBUG_RANDOM_CHAR, "ABIGAIL 1: PRE FLOW FLAG SET")
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_PURCHASED_MARINA_PROPERTY, TRUE)
			BREAK
	
			// Epsilon
			CASE RC_EPSILON_8
				CPRINTLN(DEBUG_RANDOM_CHAR, "EPSILON 8: PRE FLOW FLAG SET")
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_EPSILON_DESERT_DONE, TRUE)
			FALLTHRU
			
			CASE RC_EPSILON_7
			CASE RC_EPSILON_6
				CPRINTLN(DEBUG_RANDOM_CHAR, "EPSILON 6&7: PRE FLOW FLAG SET")
				SET_VEHICLE_GEN_AVAILABLE(VEHGEN_EPSILON6_PLANE, TRUE) // Fix B*1162882
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_EPSILON_ROBES_BOUGHT, TRUE)
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_EPSILON_ROBES_DONE, TRUE)
				
				// Enable robes in wardrobe
				SET_PED_COMP_ITEM_AVAILABLE_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_OUTFIT, OUTFIT_P0_EPSILON, TRUE)
				SET_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_OUTFIT, OUTFIT_P0_EPSILON, TRUE)
			FALLTHRU

			CASE RC_EPSILON_5
				CPRINTLN(DEBUG_RANDOM_CHAR, "EPSILON 5: PRE FLOW FLAG SET")
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_EPSILON_DONATED_10000, TRUE)
			FALLTHRU
			
			CASE RC_EPSILON_4
				CPRINTLN(DEBUG_RANDOM_CHAR, "EPSILON 4: PRE FLOW FLAG SET")
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_EPSILON_CARS_DONE, TRUE)
			FALLTHRU
			
			CASE RC_EPSILON_3
				CPRINTLN(DEBUG_RANDOM_CHAR, "EPSILON 3 PRE FLOW FLAG SET")
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_EPSILON_DONATED_5000, TRUE)
			FALLTHRU
			
			CASE RC_EPSILON_2
				CPRINTLN(DEBUG_RANDOM_CHAR, "EPSILON 2: PRE FLOW FLAG SET")
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_EPSILON_DONATED_500, TRUE)
			FALLTHRU
			
			CASE RC_EPSILON_1
				CPRINTLN(DEBUG_RANDOM_CHAR, "EPSILON 1: PRE FLOW FLAG SET")
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_EPSILON_QUESTIONNAIRE_DONE, TRUE)
			BREAK
			
			// Josh
			CASE RC_JOSH_2
			CASE RC_JOSH_3
			CASE RC_JOSH_4
				CPRINTLN(DEBUG_RANDOM_CHAR, "JOSH 2-4: PRE FLOW FLAG SET")
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_FOR_SALE_SIGNS_DESTROYED, TRUE)
			BREAK
			
			//Maude - reset Bail Bonds
			CASE RC_MAUDE_1
				// terminate active bail bond launcher
				TERMINATE_ALL_SCRIPTS_WITH_THIS_NAME("BailBond_Launcher")
				
				// reset saved current mission to initialised state
				g_savedGlobals.sBailBondData.iCurrentMission = 0
				
				// reset launcher saved state to initialise
				g_savedGlobals.sBailBondData.iLauncherState = -1
				
				//reset the email thread
				RESET_EMAIL_THREAD(BAIL_BONDS)
			BREAK
			
			// Mrs. Philips
			CASE RC_MRS_PHILIPS_2
			
				CPRINTLN(DEBUG_RANDOM_CHAR, "MRS PHILIPS 2: WARPING TREVOR INTO VAN")
				
				// Create and warp player inside Deludamol Van		
				REQUEST_MODEL(RUMPO2)
				WHILE NOT HAS_MODEL_LOADED(RUMPO2)
					WAIT(0)
				ENDWHILE
				
				// Take out of existing vehicle and place into van
				IF IS_PLAYER_PLAYING(PLAYER_ID())
	
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						VEHICLE_INDEX oldCar 
						oldCar = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
						IF DOES_ENTITY_EXIST(oldCar)
							IF NOT IS_ENTITY_A_MISSION_ENTITY(oldCar)
								SET_ENTITY_AS_MISSION_ENTITY(oldCar)
							ENDIF
							SET_ENTITY_COORDS(PLAYER_PED_ID(), GET_ENTITY_COORDS(PLAYER_PED_ID()))
							DELETE_VEHICLE(oldCar)
						ENDIF
					ENDIF
					
					VEHICLE_INDEX newCar 
					newCar = CREATE_VEHICLE(RUMPO2, GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_HEADING(PLAYER_PED_ID()))
					IF DOES_ENTITY_EXIST(newCar)
						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), newCar)
						SET_VEHICLE_AS_NO_LONGER_NEEDED(newCar)
					ENDIF
				ENDIF

				// Release model
				SET_MODEL_AS_NO_LONGER_NEEDED(RUMPO2)
			BREAK
			
			CASE RC_THELASTONE
				
				// Set 100% complete, so mission unlocks + activates
				CPRINTLN(DEBUG_RANDOM_CHAR, "THE LAST ONE: 100% COMPLETE")
				SetAllItemsAsDebugComplete()
				RecalculateResultantPercentageTotal()
			BREAK
			
			// Tonya 
			CASE RC_TONYA_2 
			CASE RC_TONYA_3 
			CASE RC_TONYA_4 
			CASE RC_TONYA_5 
				CPRINTLN(DEBUG_RANDOM_CHAR, "TONYA: COMPLETING FRANKLIN 0") 
				SET_MISSION_COMPLETE_STATE(SP_MISSION_FRANKLIN_0, TRUE) 
			BREAK
		ENDSWITCH
		
		IF eRCMission = RC_EPSILON_8
			// (Had to put this here as epsilon missions use fallthru in above switch)
			// Update which epsilon outfit the player has, so he has the medal version
			CPRINTLN(DEBUG_RANDOM_CHAR, "EPSILON 8: UPDATING EPSILON OUTFIT")
			SET_PED_COMP_ITEM_AVAILABLE_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_OUTFIT, OUTFIT_P0_EPSILON, FALSE)
			SET_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_OUTFIT, OUTFIT_P0_EPSILON, FALSE)
		
			SET_PED_COMP_ITEM_AVAILABLE_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_OUTFIT, OUTFIT_P0_EPSILON_WITH_MEDAL, TRUE)
			SET_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_OUTFIT, OUTFIT_P0_EPSILON_WITH_MEDAL, TRUE)
		ENDIF
		
	ELSE
		CPRINTLN(DEBUG_RANDOM_CHAR, "SP_Debug - Invalid random character mission:", filenameStr)
	ENDIF
ENDPROC


/// PURPOSE: 	For Random Characters, configures the debug launching RC mission flag before
///    			any gameflow launch is activated. Ensures the gameflow launch knows which RC
///    			mission is being triggered before it starts.
PROC SP_PERFORM_RANDOM_CHARACTER_PRE_LAUNCH_SETUP()

	STRING filenameStr = SP_Get_Latest_Selection()
	INT filenameHash = GET_HASH_KEY(filenameStr)
	g_eRC_MissionIDs eRCMission = NO_RC_MISSION
	g_structRCMissionsStatic sRCMissionDetails
	INT iMission
	
	// Search all the Random Character missions looking for this filenameHash
	REPEAT MAX_RC_MISSIONS iMission
		Retrieve_Random_Character_Static_Mission_Details(INT_TO_ENUM(g_eRC_MissionIDs, iMission), sRCMissionDetails)
		IF (GET_HASH_KEY(sRCMissionDetails.rcScriptName) = filenameHash)
		OR (GET_HASH_KEY(sRCMissionDetails.rcAmbientPassScript) = filenameHash)
			eRCMission = (INT_TO_ENUM(g_eRC_MissionIDs, iMission))
		ENDIF
	ENDREPEAT

	//Stops the leave area flag being turned on for this RC mission as we launch to it.
	g_eDebugLaunchingToRCMission = eRCMission
	
	// Allow the Tonya unlock texts to send when launching to the Tonya missions they introduce.
	// All other debug launches will skip queuing these texts.
	SWITCH eRCMission
		CASE RC_TONYA_3
			SET_BIT(g_iDebugLaunchBlockCommunication, BIT_DBG_LNCH_COMM_SEND_TONYA_3_TXT)
		BREAK
		CASE RC_TONYA_4
			SET_BIT(g_iDebugLaunchBlockCommunication, BIT_DBG_LNCH_COMM_SEND_TONYA_4_TXT)
		BREAK
	ENDSWITCH
		
ENDPROC

/// PURPOSE: 
///    Ensures player is warped away from Michael Event starting location
PROC SP_PERFORM_ANY_SPECIAL_MICHAEL_EVENT_CODE()
		
	IF IS_PLAYER_PLAYING(PLAYER_ID())
		
		// Work out Michael Event
		STRING filenameStr = SP_Get_Latest_Selection()
		INT filenameHash = GET_HASH_KEY(filenameStr)
		
		// Amanda Event
		IF GET_HASH_KEY("ME_Amanda1") = filenameHash

		// Jimmy Event
		ELIF GET_HASH_KEY("ME_Jimmy1") = filenameHash

		// Tracey Event
		ELIF GET_HASH_KEY("ME_Tracey1") = filenameHash
			
			IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), << 238.1823, -34.5401, 68.7233 >>) < 200
				CPRINTLN(DEBUG_AMBIENT, "Warping away from mission area...")
				SET_ENTITY_COORDS(PLAYER_PED_ID(), << 90.6710, -215.2205, 53.4915 >>)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), 338.3)
			ENDIF
		ENDIF
	ENDIF
ENDPROC


PROC SP_DEBUG_SPAWN_PLAYER_IN_VEHICLE(MODEL_NAMES paramVehModel)
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		VECTOR vVehPos
		VECTOR vPosition
		FLOAT fHeading
		INT iNumLanes

		REQUEST_MODEL(paramVehModel)
		WHILE NOT HAS_MODEL_LOADED(paramVehModel)
			WAIT(0)
		ENDWHILE
		
		//Create the vehicle, place it on a road node, place the player in it, and then set it as no longer needed.
		VEHICLE_INDEX vehToSpawn
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			vVehPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
			GET_NTH_CLOSEST_VEHICLE_NODE_WITH_HEADING(vVehPos, 1, vPosition, fHeading, iNumLanes)
			vehToSpawn = CREATE_VEHICLE(paramVehModel, vPosition, fHeading)
			SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehToSpawn)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(vehToSpawn)
		ENDIF
		SET_MODEL_AS_NO_LONGER_NEEDED(paramVehModel)
	ENDIF
ENDPROC


/// PURPOSE: Checks to see if an item is enabled
PROC SP_RUN_SCRIPT_FOR_ENABLEID(DEBUG_MENU_DATA_STRUCT &sDebugMenuData, INT iDepth, BOOL bForceDisabled = FALSE)

	INT iCount
	
	FOR iCount = 0 TO sDebugMenuData.iItemCount[iDepth]-1
	
		IF bForceDisabled
			CLEAR_DEBUG_MENU_ITEM_FLAG(sDebugMenuData.sItems[iDepth][iCount].iFlags, DM_BIT_ENABLED)
		ELSE
			SET_DEBUG_MENU_ITEM_FLAG(sDebugMenuData.sItems[iDepth][iCount].iFlags, DM_BIT_ENABLED)
			
			SWITCH INT_TO_ENUM(SP_MENU_ENABLE_ID_ENUM, (sDebugMenuData.sItems[iDepth][iCount].iEnableID))
				
				CASE SPEID_DEFAULT
					SET_DEBUG_MENU_ITEM_FLAG(sDebugMenuData.sItems[iDepth][iCount].iFlags, DM_BIT_ENABLED)
				BREAK
				
				CASE SPEID_DONOTENABLE
					CLEAR_DEBUG_MENU_ITEM_FLAG(sDebugMenuData.sItems[iDepth][iCount].iFlags, DM_BIT_ENABLED)
				BREAK
				
				CASE SPEID_GAMEFLOW_ONLY
					IF NOT g_savedGlobals.sFlow.isGameflowActive
						CLEAR_DEBUG_MENU_ITEM_FLAG(sDebugMenuData.sItems[iDepth][iCount].iFlags, DM_BIT_ENABLED)
					ENDIF
				BREAK
				
				CASE SPEID_DEBUG_ONLY
					IF g_savedGlobals.sFlow.isGameflowActive
						CLEAR_DEBUG_MENU_ITEM_FLAG(sDebugMenuData.sItems[iDepth][iCount].iFlags, DM_BIT_ENABLED)
					ENDIF
				BREAK
				
				DEFAULT
					SCRIPT_ASSERT("Debug Menu - Invalid iEnableID")
					PRINTLN("Debug Menu - Invalid iEnableID '", GET_STRING_FROM_HASH_KEY(sDebugMenuData.sItems[iDepth][iCount].iEnableID),"'")
				BREAK
				
			ENDSWITCH
		ENDIF
		
	ENDFOR
	
ENDPROC
/// PURPOSE: Runs a piece of script for the specified codeID
FUNC BOOL SP_RUN_SCRIPT_FOR_SWITCH_CODEID(SP_MENU_CODE_ID_ENUM eCodeID)
	SWITCH eCodeID
		
		CASE SPCID_PRM_DEFAULT			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M_DEFAULT) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRF_DEFAULT			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_F_DEFAULT) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRT_DEFAULT			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T_DEFAULT) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		
		CASE SPCID_PRFa_PHONECALL_ARM3	g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_Fa_PHONECALL_ARM3) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRFa_PHONECALL_FAM1	g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_Fa_PHONECALL_FAM1) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRFa_PHONECALL_FAM3	g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_Fa_PHONECALL_FAM3) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRFa_STRIPCLUB_ARM3	g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_Fa_STRIPCLUB_ARM3) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
//		CASE SPCID_PRMa_ARM3			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_Ma_ARM3) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
//		CASE SPCID_PRFa_STRIPCLUB_FAM1	g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_Fa_STRIPCLUB_FAM1) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
//		CASE SPCID_PRFa_STRIPCLUB_FAM3	g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_Fa_STRIPCLUB_FAM3) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
//		CASE SPCID_PRFa_FAMILY1			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_Fa_FAMILY1) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRFa_FAMILY3			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_Fa_FAMILY3) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRTa_FBI2			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_Ta_FBI2) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRTa_FINALE1			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_Ta_FINALE1) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRTa_CARSTEAL4		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_Ta_CARSTEAL4) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRFa_FINALE2intro	g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_Fa_FINALE2intro) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRTa_FINALE2intro	g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_Ta_FINALE2intro) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRMa_FAMILY1			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_Ma_FAMILY1) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRFa_FBI4intro		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_Fa_FBI4intro) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRMa_FBI4intro		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_Ma_FBI4intro) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRMa_FBI5			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_Ma_FBI5) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRMa_FBI3			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_Ma_FBI3) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRFa_FBI4			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_Fa_FBI4) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRFa_FBI5			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_Fa_FBI5) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
//		CASE SPCID_PRMa_FAMILY4_a		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_Ma_FAMILY4_a) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
//		CASE SPCID_PRMa_FAMILY4_b		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_Ma_FAMILY4_b) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRTa_FAMILY4			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_Ta_FAMILY4) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRTa_FINALEC			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_Ta_FINALEC) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRMa_FRANKLIN2		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_Ma_FRANKLIN2) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRTa_FRANKLIN2		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_Ta_FRANKLIN2) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRMa_FBI1end			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_Ma_FBI1end) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
//		CASE SPCID_PRMa_MARTIN1			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_Ma_MARTIN1) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
//		CASE SPCID_PRTa_MARTIN1			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_Ta_MARTIN1) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRFa_AGENCY1			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_Fa_AGENCY1) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRFa_AGENCYprep1		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_Fa_AGENCYprep1) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRFa_AGENCY3B		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_Fa_AGENCY3B) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
//		CASE SPCID_PRTa_CARSTEAL1		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_Ta_CARSTEAL1) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRFa_CARSTEAL1		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_Fa_CARSTEAL1) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRFa_AGENCY2			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_Fa_AGENCY2) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRTa_CARSTEAL1		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_Ta_CARSTEAL1) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRTa_CARSTEAL2		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_Ta_CARSTEAL2) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRFa_RURAL2A			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_Fa_RURAL2A) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRTa_RURAL2A			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_Ta_RURAL2A) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRTa_RC_MRSP2			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_Ta_RC_MRSP2) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRMa_RURAL1			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_Ma_RURAL1) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRFTa_FRANKLIN1a		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_FTa_FRANKLIN1a) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
//		CASE SPCID_PRFTa_FRANKLIN1b		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_FTa_FRANKLIN1b) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRFTa_FRANKLIN1c		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_FTa_FRANKLIN1c) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRFTa_FRANKLIN1d		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_FTa_FRANKLIN1d) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRFTa_FRANKLIN1e		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_FTa_FRANKLIN1e) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
//		CASE SPCID_PRMa_FRANKLIN2		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_Ma_FRANKLIN2) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
//		CASE SPCID_PRTa_FRANKLIN2		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_Ta_FRANKLIN2) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRMa_EXILE2			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_Ma_EXILE2) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRFa_EXILE2			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_Fa_EXILE2) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRFa_EXILE3			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_Fa_EXILE3) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRMa_EXILE3			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_Ma_EXILE3) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM4_WASHFACE		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M4_WASHFACE) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
//		CASE SPCID_PRFa_MICHAEL2		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_Fa_MICHAEL2) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRFa_MICHAEL3		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_Fa_MICHAEL3) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRTa_MICHAEL3		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_Ta_MICHAEL3) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRMa_DOCKS2A			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_Ma_DOCKS2A) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRMa_DOCKS2B			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_Ma_DOCKS2B) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRMa_FINALE1			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_Ma_FINALE1) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRFa_AGENCY3A		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_Fa_AGENCY3A) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRMa_FINALE2A		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_Ma_FINALE2A) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRTa_FINALE2A		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_Ta_FINALE2A) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRMa_FINALE2B		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_Ma_FINALE2B) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRTa_FINALE2B		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_Ta_FINALE2B) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRFa_FBI1			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_Fa_FBI1) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRFa_MARTIN1			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_Fa_MARTIN1) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRMa_TREVOR3			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_Ma_TREVOR3) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRFa_TREVOR3			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_Fa_TREVOR3) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRFa_DOCKS2A			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_Fa_DOCKS2A) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRFa_FINALE1			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_Fa_FINALE1) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRFa_FBI2			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_Fa_FBI2) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRTa_FBI4			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_Ta_FBI4) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRFa_DOCKS2B			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_Fa_DOCKS2B) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRTa_FAMILY6			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_Ta_FAMILY6) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRTa_FINALEprepD		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_Ta_FINALEprepD) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRMa_FAMILY6			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_Ma_FAMILY6) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRFa_FINALEA			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_Fa_FINALEA) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRFa_FINALEB			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_Fa_FINALEB) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRMa_FINALEC			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_Ma_FINALEC) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRFa_FINALEC			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_Fa_FINALEC) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK

		#IF NOT IS_JAPANESE_BUILD
		CASE SPCID_PRT_SHIT				g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T_SHIT) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRT_JERKOFF			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T_JERKOFF) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		#ENDIF 
		CASE SPCID_PRT_HEADINSINK		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T_HEADINSINK) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM_MD_FBI2			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M_MD_FBI2) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRF_MD_FRANKLIN2		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_F_MD_FRANKLIN2) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		
		CASE SPCID_PRM2_BEDROOM			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M2_BEDROOM) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM2_SAVEHOUSE0_b	g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M2_SAVEHOUSE0_b) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM2_SAVEHOUSE1_a	g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M2_SAVEHOUSE1_a) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM2_SAVEHOUSE1_b	g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M2_SAVEHOUSE1_b) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM2_SMOKINGGOLF		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M2_SMOKINGGOLF) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM4_WAKEUPSCREAM
			g_eCurrentFamilyEvent[FM_MICHAEL_WIFE] = FAMILY_MEMBER_BUSY
			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M4_WAKEUPSCREAM)
			g_bWarpDebugPlayerCharScene = TRUE
			RETURN TRUE BREAK
		CASE SPCID_PRM4_WAKESUPSCARED
			g_eCurrentFamilyEvent[FM_MICHAEL_WIFE] = FAMILY_MEMBER_BUSY
			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M4_WAKESUPSCARED)
			g_bWarpDebugPlayerCharScene = TRUE
			RETURN TRUE BREAK
//		CASE SPCID_PRM4_HOUSEBED_b		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M4_HOUSEBED_b) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM6_HOUSEBED		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M6_HOUSEBED) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM4_WATCHINGTV		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M4_WATCHINGTV) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
//		CASE SPCID_PRM6_HOUSETV_b		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M6_HOUSETV_b) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM2_KIDS_TV			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M2_KIDS_TV) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM_POOLSIDE_a		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M_POOLSIDE_a) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM_POOLSIDE_b		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M_POOLSIDE_b) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM2_CARSLEEP_a		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M2_CARSLEEP_a) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM2_CARSLEEP_b		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M2_CARSLEEP_b) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM_CANAL_a			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M_CANAL_a) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM_CANAL_b			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M_CANAL_b) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM_CANAL_c			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M_CANAL_c) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM2_LUNCH_a			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M2_LUNCH_a) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
//		CASE SPCID_PRM7_LUNCH_b			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M7_LUNCH_b) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM4_EXITRESTAURANT	g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M4_EXITRESTAURANT) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM4_LUNCH_b			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M4_LUNCH_b) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM4_CINEMA			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M4_CINEMA) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM2_WIFEEXITSCAR	g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M2_WIFEEXITSCAR) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM2_DROPOFFDAU_a	g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M2_DROPOFFDAU_a) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM2_DROPOFFDAU_b	g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M2_DROPOFFDAU_b) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM2_DROPOFFSON_a	g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M2_DROPOFFSON_a) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM2_DROPOFFSON_b	g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M2_DROPOFFSON_b) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM_PIER_a			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M_PIER_a) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM_PIER_b			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M_PIER_b) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM_TRAFFIC_a		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M_TRAFFIC_a) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM_TRAFFIC_b		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M_TRAFFIC_b) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM_TRAFFIC_c		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M_TRAFFIC_c) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM_VWOODPARK_a		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M_VWOODPARK_a) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM_VWOODPARK_b		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M_VWOODPARK_b) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM_BENCHCALL_a		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M_BENCHCALL_a) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM_BENCHCALL_b		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M_BENCHCALL_b) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM_PARKEDHILLS_a	g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M_PARKEDHILLS_a) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM_PARKEDHILLS_b	g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M_PARKEDHILLS_b) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM6_PARKEDHILLS_a	g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M6_PARKEDHILLS_a) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM6_PARKEDHILLS_b	g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M6_PARKEDHILLS_b) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM6_PARKEDHILLS_c	g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M6_PARKEDHILLS_c) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM6_PARKEDHILLS_d	g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M6_PARKEDHILLS_d) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM6_PARKEDHILLS_e	g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M6_PARKEDHILLS_e) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM2_DRIVING_a		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M2_DRIVING_a) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM2_DRIVING_b		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M2_DRIVING_b) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM6_DRIVING_a		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M6_DRIVING_a) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM6_DRIVING_b		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M6_DRIVING_b) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM6_DRIVING_c		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M6_DRIVING_c) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM6_DRIVING_d		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M6_DRIVING_d) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM6_DRIVING_e		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M6_DRIVING_e) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM6_DRIVING_f		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M6_DRIVING_f) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM6_DRIVING_g		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M6_DRIVING_g) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM6_DRIVING_h		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M6_DRIVING_h) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM6_RONBORING		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M6_RONBORING) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM2_PHARMACY		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M2_PHARMACY) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
//		CASE SPCID_PRM4_DOORSTUMBLE		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M4_DOORSTUMBLE) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM_COFFEE_a			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M_COFFEE_a) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM_COFFEE_b			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M_COFFEE_b) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM_COFFEE_c			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M_COFFEE_c) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM2_CYCLING_a		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M2_CYCLING_a) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM2_CYCLING_b		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M2_CYCLING_b) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM2_CYCLING_c		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M2_CYCLING_c) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
//		CASE SPCID_PRM_BAR_a			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M_BAR_a) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
//		CASE SPCID_PRM_BAR_b			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M_BAR_b) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM2_MARINA			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M2_MARINA) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM2_ARGUEWITHWIFE	g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M2_ARGUEWITHWIFE) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM4_PARKEDBEACH		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M4_PARKEDBEACH) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM_HOOKERMOTEL		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M_HOOKERMOTEL) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
//		CASE SPCID_PRM_HOOKERCAR		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M_HOOKERCAR) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM6_MORNING_a		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M6_MORNING_a) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
//		CASE SPCID_PRM6_MORNING_b		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M6_MORNING_b) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM6_CARSLEEP		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M6_CARSLEEP) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM6_HOUSETV_a		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M6_HOUSETV_a) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM6_SUNBATHING		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M6_SUNBATHING) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM6_DRINKINGBEER	g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M6_DRINKINGBEER) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM6_ONPHONE			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M6_ONPHONE) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM6_DEPRESSED		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M6_DEPRESSED) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM6_BOATING			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M6_BOATING) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM6_LIQUORSTORE		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M6_LIQUORSTORE) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
//		CASE SPCID_PRM6_PILOTSCHOOL
//			Execute_Code_ID(CID_ACTIVATE_MINIGAME_PILOT_SCHOOL)
//			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M6_PILOTSCHOOL)
//			g_bWarpDebugPlayerCharScene = TRUE
//			RETURN TRUE BREAK
//		CASE SPCID_PRM6_TRIATHLON
//			Execute_Code_ID(CID_ACTIVATE_MINIGAME_TRIATHLON)
//			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M6_TRIATHLON)
//			g_bWarpDebugPlayerCharScene = TRUE
//			RETURN TRUE BREAK
		CASE SPCID_PRM7_RESTAURANT		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M7_RESTAURANT) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM7_LOUNGECHAIRS	g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M7_LOUNGECHAIRS) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM7_BYESOLOMON_a	g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M7_BYESOLOMON_a) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM7_BYESOLOMON_b	g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M7_BYESOLOMON_b) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM7_WIFETENNIS		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M7_WIFETENNIS) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM7_ROUNDTABLE		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M7_ROUNDTABLE) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM7_REJECTENTRY		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M7_REJECTENTRY) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM7_HOOKERS			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M7_HOOKERS) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM7_EXITBARBER
			SET_SHOP_HAS_RUN_ENTRY_INTRO(HAIRDO_SHOP_01_BH, TRUE)
			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M7_EXITBARBER)
			g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM7_EXITFANCYSHOP	g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M7_EXITFANCYSHOP) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM7_FAKEYOGA
		
			Execute_Activate_Minigame_Yoga()
			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M7_FAKEYOGA)
			g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		
		CASE SPCID_PRM7_COFFEE			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M7_COFFEE) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM7_GETSREADY		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M7_GETSREADY) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
//		CASE SPCID_PRM7_PARKEDHILLS		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M7_PARKEDHILLS) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM7_READSCRIPT		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M7_READSCRIPT) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM7_EMPLOYEECONVO
			SUPPRESS_RESTRICTED_AREA_WANTED_LEVEL(AC_MOVIE_STUDIO, TRUE)
			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M7_EMPLOYEECONVO)
			g_bWarpDebugPlayerCharScene = TRUE
			RETURN TRUE BREAK
		CASE SPCID_PRM7_TALKTOGUARD
			SUPPRESS_RESTRICTED_AREA_WANTED_LEVEL(AC_MOVIE_STUDIO, TRUE)
			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M7_TALKTOGUARD)
			g_bWarpDebugPlayerCharScene = TRUE
			RETURN TRUE BREAK
		CASE SPCID_PRM7_LOT_JIMMY
			SUPPRESS_RESTRICTED_AREA_WANTED_LEVEL(AC_MOVIE_STUDIO, TRUE)
			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M7_LOT_JIMMY)
			g_bWarpDebugPlayerCharScene = TRUE
			RETURN TRUE BREAK
		CASE SPCID_PRM7_KIDS_TV			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M7_KIDS_TV) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM7_KIDS_GAMING		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M7_KIDS_GAMING) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM7_OPENDOORFORAMA	g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M7_OPENDOORFORAMA) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM7_DROPPINGOFFJMY	g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M7_DROPPINGOFFJMY) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM7_BIKINGJIMMY		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M7_BIKINGJIMMY) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRM7_TRACEYEXITSCAR	g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M7_TRACEYEXITSCAR) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		
		CASE SPCID_PRM_S_FAMILY4
			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_M_S_FAMILY4)
			g_bWarpDebugPlayerCharScene = TRUE
			RETURN TRUE BREAK
			
	ENDSWITCH
	SWITCH eCodeID
		CASE SPCID_PRF0_SH_ASLEEP		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_F0_SH_ASLEEP) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRF1_SH_ASLEEP		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_F1_SH_ASLEEP) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRF1_NAPPING			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_F1_NAPPING) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRF1_GETTINGREADY	g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_F1_GETTINGREADY) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRF0_SH_READING		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_F0_SH_READING) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRF1_SH_READING		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_F1_SH_READING) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRF0_SH_PUSHUP_a		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_F0_SH_PUSHUP_a) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRF0_SH_PUSHUP_b		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_F0_SH_PUSHUP_b) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRF1_SH_PUSHUP		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_F1_SH_PUSHUP) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRF_KUSH_DOC_a		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_F_KUSH_DOC_a) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRF_MD_KUSH_DOC		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_F_MD_KUSH_DOC) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRF_KUSH_DOC_b		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_F_KUSH_DOC_b) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRF_KUSH_DOC_c		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_F_KUSH_DOC_c) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRF0_GARBAGE			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_F0_GARBAGE) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRF1_GARBAGE			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_F1_GARBAGE) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRF_THROW_CUP		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_F_THROW_CUP) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRF_HIT_CUP_HAND		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_F_HIT_CUP_HAND) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRF_GYM				g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_F_GYM) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		
		CASE SPCID_PRF0_WALKCHOP
			IF IS_PED_UNINJURED(PLAYER_PED_ID()) SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-112.41016, -1505.82422, 32.74073>>) ENDIF // See B*1192528 - Ensure player out of world brain range so Chop script can launch and take ownership of Chop
			Set_Mission_Flow_Flag_State(FLOWFLAG_CHOP_THE_DOG_UNLOCKED, TRUE)			
			Set_Savehouse_Respawn_Available(SAVEHOUSE_FRANKLIN_SC, TRUE)			
			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_F0_WALKCHOP)
			g_bWarpDebugPlayerCharScene = TRUE
			RETURN TRUE BREAK
		CASE SPCID_PRF0_PLAYCHOP
			IF IS_PED_UNINJURED(PLAYER_PED_ID()) SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-112.41016, -1505.82422, 32.74073>>) ENDIF // See B*1192528 - Ensure player out of world brain range so Chop script can launch and take ownership of Chop
			Set_Mission_Flow_Flag_State(FLOWFLAG_CHOP_THE_DOG_UNLOCKED, TRUE)
			Set_Savehouse_Respawn_Available(SAVEHOUSE_FRANKLIN_SC, TRUE)
			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_F0_PLAYCHOP)
			g_bWarpDebugPlayerCharScene = TRUE
			RETURN TRUE BREAK
		CASE SPCID_PRF_WALKCHOP_a
			IF IS_PED_UNINJURED(PLAYER_PED_ID()) SET_ENTITY_COORDS(PLAYER_PED_ID(), <<171.64238, 663.96442, 205.68253>>) ENDIF // See B*1192528 - Ensure player out of world brain range so Chop script can launch and take ownership of Chop
			Set_Mission_Flow_Flag_State(FLOWFLAG_CHOP_THE_DOG_UNLOCKED, TRUE)
			Set_Savehouse_Respawn_Available(SAVEHOUSE_FRANKLIN_VH, TRUE)
			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_F_WALKCHOP_a)
			g_bWarpDebugPlayerCharScene = TRUE
			RETURN TRUE BREAK
		CASE SPCID_PRF_WALKCHOP_b
			IF IS_PED_UNINJURED(PLAYER_PED_ID()) SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-360.08066, 363.70609, 108.67895>>) ENDIF // See B*1192528 - Ensure player out of world brain range so Chop script can launch and take ownership of Chop
			Set_Mission_Flow_Flag_State(FLOWFLAG_CHOP_THE_DOG_UNLOCKED, TRUE)
			Set_Savehouse_Respawn_Available(SAVEHOUSE_FRANKLIN_VH, TRUE)
			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_F_WALKCHOP_b)
			g_bWarpDebugPlayerCharScene = TRUE
			RETURN TRUE BREAK
		CASE SPCID_PRF1_PLAYCHOP
			IF IS_PED_UNINJURED(PLAYER_PED_ID()) SET_ENTITY_COORDS(PLAYER_PED_ID(), <<126.59258, 578.77899, 182.37239>>) ENDIF // See B*1192528 - Ensure player out of world brain range so Chop script can launch and take ownership of Chop
			Set_Mission_Flow_Flag_State(FLOWFLAG_CHOP_THE_DOG_UNLOCKED, TRUE)
			Set_Savehouse_Respawn_Available(SAVEHOUSE_FRANKLIN_VH, TRUE)
			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_F1_PLAYCHOP)
			g_bWarpDebugPlayerCharScene = TRUE
			RETURN TRUE BREAK		
		CASE SPCID_PRF_TRAFFIC_a		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_F_TRAFFIC_a) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRF_TRAFFIC_b		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_F_TRAFFIC_b) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRF_TRAFFIC_c		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_F_TRAFFIC_c) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRF0_BIKE			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_F0_BIKE) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRF0_CLEANCAR		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_F0_CLEANCAR) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRF1_BIKE			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_F1_BIKE) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRF1_CLEANCAR		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_F1_CLEANCAR) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRF1_BYETAXI			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_F1_BYETAXI) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRF_BIKE_c			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_F_BIKE_c) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRF_BIKE_d			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_F_BIKE_d) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRF0_TANISHAFIGHT	g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_F0_TANISHAFIGHT) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRF1_NEWHOUSE		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_F1_NEWHOUSE) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
//		CASE SPCID_PRF_LAMARGRAFF		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_F_LAMGRAFF) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRF_CLUB				g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_F_CLUB) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRF_CS_CHECKSHOE
			SET_MISSION_COMPLETE_STATE(SP_MISSION_CARSTEAL_1, TRUE)
			SET_MISSION_COMPLETE_STATE(SP_MISSION_CARSTEAL_4, FALSE)
			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_F_CS_CHECKSHOE)
			g_bWarpDebugPlayerCharScene = TRUE
			RETURN TRUE BREAK
		CASE SPCID_PRF_CS_WIPEHANDS
			SET_MISSION_COMPLETE_STATE(SP_MISSION_CARSTEAL_1, TRUE)
			SET_MISSION_COMPLETE_STATE(SP_MISSION_CARSTEAL_4, FALSE)
			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_F_CS_WIPEHANDS)
			g_bWarpDebugPlayerCharScene = TRUE
			RETURN TRUE BREAK
		CASE SPCID_PRF_CS_WIPERIGHT
			SET_MISSION_COMPLETE_STATE(SP_MISSION_CARSTEAL_1, TRUE)
			SET_MISSION_COMPLETE_STATE(SP_MISSION_CARSTEAL_4, FALSE)
			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_F_CS_WIPERIGHT)
			g_bWarpDebugPlayerCharScene = TRUE
			RETURN TRUE BREAK
//		CASE SPCID_PRF_TAUNT			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_F_TAUNT) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRF_LAMTAUNT_P1		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_F_LAMTAUNT_P1) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
//		CASE SPCID_PRF_LAMTAUNT_P2		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_F_LAMTAUNT_P2) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRF_LAMTAUNT_P3		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_F_LAMTAUNT_P3) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRF_LAMTAUNT_P5		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_F_LAMTAUNT_P5) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRF_LAMTAUNT_NIGHT	g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_F_LAMTAUNT_NIGHT) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRF_BAR_a_01			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_F_BAR_a_01) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRF_BAR_b_01			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_F_BAR_b_01) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRF_BAR_c_02
			SET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_MINIGAME_ACTIVE, ENUM_TO_INT(MINIGAME_STRIPCLUB), TRUE) 
			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_F_BAR_c_02)
			g_bWarpDebugPlayerCharScene = TRUE
			RETURN TRUE BREAK
		CASE SPCID_PRF_BAR_d_02			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_F_BAR_d_02) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRF_BAR_e_01			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_F_BAR_e_01) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRF1_POOLSIDE_a		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_F1_POOLSIDE_a) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRF1_POOLSIDE_b		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_F1_POOLSIDE_b) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRF1_CLEANINGAPT		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_F1_CLEANINGAPT) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRF1_ONCELL			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_F1_ONCELL) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRF1_SNACKING		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_F1_SNACKING) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRF1_ONLAPTOP		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_F1_ONLAPTOP) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRF1_IRONING			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_F1_IRONING) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
//		CASE SPCID_PRF0_WATCHINGTV		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_F0_WATCHINGTV) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRF1_WATCHINGTV		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_F1_WATCHINGTV) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRF_S_EXILE2
			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_F_S_EXILE2)
			g_bWarpDebugPlayerCharScene = TRUE
			RETURN TRUE BREAK
		CASE SPCID_PRF_S_AGENCY_2A_a
			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_F_S_AGENCY_2A_a)
			g_bWarpDebugPlayerCharScene = TRUE
			RETURN TRUE BREAK
		CASE SPCID_PRF_S_AGENCY_2A_b
			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_F_S_AGENCY_2A_b)
			g_bWarpDebugPlayerCharScene = TRUE
			RETURN TRUE BREAK
		CASE SPCID_PRT_SC_MOCKLAPDANCE
			SET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_MINIGAME_ACTIVE, ENUM_TO_INT(MINIGAME_STRIPCLUB), TRUE)
			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T_SC_MOCKLAPDANCE)
			g_bWarpDebugPlayerCharScene = TRUE
			RETURN TRUE BREAK
		CASE SPCID_PRT_SC_BAR
			SET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_MINIGAME_ACTIVE, ENUM_TO_INT(MINIGAME_STRIPCLUB), TRUE)
			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T_SC_BAR)
			g_bWarpDebugPlayerCharScene = TRUE
			RETURN TRUE BREAK
		CASE SPCID_PRT_SC_CHASE
			SET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_MINIGAME_ACTIVE, ENUM_TO_INT(MINIGAME_STRIPCLUB), TRUE)
			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T_SC_CHASE)
			g_bWarpDebugPlayerCharScene = TRUE
			RETURN TRUE BREAK
		CASE SPCID_PRT_STRIPCLUB_out	g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T_STRIPCLUB_out) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRT_ESCORTED_OUT		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T_ESCORTED_OUT) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRT_CN_CHATEAU_b		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T_CN_CHATEAU_b) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRT_CN_CHATEAU_c		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T_CN_CHATEAU_c) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRT_CR_CHATEAU_d		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T_CR_CHATEAU_d) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRT_GARBAGE_FOOD		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T_GARBAGE_FOOD) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRT_THROW_FOOD		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T_THROW_FOOD) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRT_SMOKEMETH		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T_SMOKEMETH) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRT_FIGHTBBUILD		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T_FIGHTBBUILD) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRT_ANNOYSUNBATHERS
			SET_CLOCK_TIME(12, GET_CLOCK_MINUTES(), GET_CLOCK_SECONDS())
			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T_ANNOYSUNBATHERS)
			g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRT_CR_BLOCK_CAMERA	g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T_CR_BLOCK_CAMERA) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRT_GUITARBEATDOWN	g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T_GUITARBEATDOWN) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRT_DOCKS_a			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T_DOCKS_a) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRT_DOCKS_b			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T_DOCKS_b) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRT_DOCKS_c			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T_DOCKS_c) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRT_DOCKS_d			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T_DOCKS_d) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRT_CR_LINGERIE		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T_CR_LINGERIE) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
//		CASE SPCID_PRT_CR_MACHINE		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T_CR_MACHINE) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRT_CR_FUNERAL		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T_CR_FUNERAL) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRT_CR_DUMPSTER		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T_CR_DUMPSTER) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRT_CN_WAKETRASH_b	g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T_CN_WAKETRASH_b) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRT_CR_WAKEBEACH		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T_CR_WAKEBEACH) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRT_CN_WAKEBARN		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T_CN_WAKEBARN) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRT_CN_WAKETRAIN		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T_CN_WAKETRAIN) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRT_CR_WAKEROOFTOP	g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T_CR_WAKEROOFTOP) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRT_CN_WAKEMOUNTAIN	g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T_CN_WAKEMOUNTAIN) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRT_CR_ALLEYDRUNK	g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T_CR_ALLEYDRUNK) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRT_SC_ALLEYDRUNK	g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T_SC_ALLEYDRUNK) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRT_PUKEINTOFOUNT	g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T_PUKEINTOFOUNT) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRT_CN_PARK_b		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T_CN_PARK_b) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
//		CASE SPCID_PRT_CN_PARK_c		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T_CN_PARK_c) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRT_CR_POLICE_a		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T_CR_POLICE_a) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRT_CN_POLICE_b		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T_CN_POLICE_b) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRT_CN_POLICE_c		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T_CN_POLICE_c) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRT_NAKED_BRIDGE		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T_NAKED_BRIDGE) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRT_NAKED_GARDEN		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T_NAKED_GARDEN) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRT_NAKED_ISLAND		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T_NAKED_ISLAND) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRT_CR_CHASECAR_a	g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T_CR_CHASECAR_a) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRT_CN_CHASECAR_b	g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T_CN_CHASECAR_b) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRT_CR_CHASEBIKE		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T_CR_CHASEBIKE) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRT_CR_CHASESCOOTER	g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T_CR_CHASESCOOTER) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRT_CR_BRIDGEDROP	g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T_CR_BRIDGEDROP) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRT_FIGHTBAR_a		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T_FIGHTBAR_a) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRT_FIGHTBAR_b		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T_FIGHTBAR_b) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRT_FIGHTBAR_c		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T_FIGHTBAR_c) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRT_YELLATDOORMAN	g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T_YELLATDOORMAN) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRT_FIGHTYAUCLUB_b	g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T_FIGHTYAUCLUB_b) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRT_FIGHTCASINO		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T_FIGHTCASINO) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRT_CR_RUDEATCAFE	g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T_CR_RUDEATCAFE) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRT_CR_TIETEMPLE
			SET_CLOCK_TIME(16, GET_CLOCK_MINUTES(), GET_CLOCK_SECONDS())
			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T_CR_RAND_TEMPLE)
			g_bWarpDebugPlayerCharScene = TRUE
			RETURN TRUE BREAK
		CASE SPCID_PRT_UNDERPIER		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T_UNDERPIER) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRT_KONEIGHBOUR		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T_KONEIGHBOUR) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRT_SCARETRAMP		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T_SCARETRAMP) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRT_DRUNKHOWLING		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T_DRUNKHOWLING) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRT_SC_DRUNKHOWLING	
			SET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_MINIGAME_ACTIVE, ENUM_TO_INT(MINIGAME_STRIPCLUB), TRUE)
			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T_SC_DRUNKHOWLING)
			g_bWarpDebugPlayerCharScene = TRUE
			RETURN TRUE BREAK
		CASE SPCID_PRT_FLOYDSAVEHOUSE	g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T_FLOYDSAVEHOUSE) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRT_FLOYDSPOON_A		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T_FLOYDSPOON_A) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRT_FLOYDSPOON_B		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T_FLOYDSPOON_B) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRT_FLOYDSPOON_B2	g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T_FLOYDSPOON_B2) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRT_FLOYDSPOON_A2	g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T_FLOYDSPOON_A2) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRT_FLOYDCRYING_A	g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T_FLOYDCRYING_A) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRT_FLOYDCRYING_E0	g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T_FLOYDCRYING_E0) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRT_FLOYDCRYING_E1	g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T_FLOYDCRYING_E1) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRT_FLOYDCRYING_E2	g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T_FLOYDCRYING_E2) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRT_FLOYDCRYING_E3	g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T_FLOYDCRYING_E3) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRT_FLOYD_BEAR
			SET_BUILDING_STATE(BUILDINGNAME_ES_FLOYDS_APPARTMENT_RASPBERRY_JAM, BUILDINGSTATE_DESTROYED, FALSE)
			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T_FLOYD_BEAR)
			g_bWarpDebugPlayerCharScene = TRUE
			RETURN TRUE BREAK
		CASE SPCID_PRT_FLOYD_DOLL		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T_FLOYD_DOLL) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRT_FLOYDPINEAPPLE	g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T_FLOYDPINEAPPLE) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRT6_SMOKECRYSTAL	g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T6_SMOKECRYSTAL) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
//		CASE SPCID_PRT6_BLOWSHITUP		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T6_BLOWSHITUP) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
//		CASE SPCID_PRT6_EVENING			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T6_EVENING) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRT6_METHLAB			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T6_METHLAB) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRT6_HUNTING1
			
				Execute_Activate_Minigame_Hunting()
				
				enumCharacterList eT6H1_Ped
				WEAPON_TYPE eT6H1_WeaponType
				WEAPON_SLOT eT6H1_WeaponSlot
				
				eT6H1_Ped = CHAR_TREVOR
				eT6H1_WeaponType = WEAPONTYPE_SNIPERRIFLE
				eT6H1_WeaponSlot = GET_WEAPONTYPE_SLOT(eT6H1_WeaponType)
				
				g_savedGlobals.sPlayerData.sInfo.sWeapons[eT6H1_Ped].sWeaponInfo[GET_INT_FROM_PLAYER_PED_WEAPON_SLOT(eT6H1_WeaponSlot)].eWeaponType = eT6H1_WeaponType
				g_savedGlobals.sPlayerData.sInfo.sWeapons[eT6H1_Ped].sWeaponInfo[GET_INT_FROM_PLAYER_PED_WEAPON_SLOT(eT6H1_WeaponSlot)].iAmmoCount = 50
				
				g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T6_HUNTING1)
				g_bWarpDebugPlayerCharScene = TRUE
				RETURN TRUE BREAK
			
		CASE SPCID_PRT6_HUNTING2
			
				Execute_Activate_Minigame_Hunting()
				enumCharacterList eT6H2_Ped
				WEAPON_TYPE eT6H2_WeaponType
				WEAPON_SLOT eT6H2_WeaponSlot
				
				eT6H2_Ped = CHAR_TREVOR
				eT6H2_WeaponType = WEAPONTYPE_SNIPERRIFLE
				eT6H2_WeaponSlot = GET_WEAPONTYPE_SLOT(eT6H2_WeaponType)
				
				g_savedGlobals.sPlayerData.sInfo.sWeapons[eT6H2_Ped].sWeaponInfo[GET_INT_FROM_PLAYER_PED_WEAPON_SLOT(eT6H2_WeaponSlot)].eWeaponType = eT6H2_WeaponType
				g_savedGlobals.sPlayerData.sInfo.sWeapons[eT6H2_Ped].sWeaponInfo[GET_INT_FROM_PLAYER_PED_WEAPON_SLOT(eT6H2_WeaponSlot)].iAmmoCount = 50
				
				g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T6_HUNTING2)
				g_bWarpDebugPlayerCharScene = TRUE
				RETURN TRUE BREAK
			
		CASE SPCID_PRT6_HUNTING3
			
				Execute_Activate_Minigame_Hunting()
				enumCharacterList eT6H3_Ped
				WEAPON_TYPE eT6H3_WeaponType
				WEAPON_SLOT eT6H3_WeaponSlot
				
				eT6H3_Ped = CHAR_TREVOR
				eT6H3_WeaponType = WEAPONTYPE_HEAVYSNIPER
				eT6H3_WeaponSlot = GET_WEAPONTYPE_SLOT(eT6H3_WeaponType)
				
				g_savedGlobals.sPlayerData.sInfo.sWeapons[eT6H3_Ped].sWeaponInfo[GET_INT_FROM_PLAYER_PED_WEAPON_SLOT(eT6H3_WeaponSlot)].eWeaponType = eT6H3_WeaponType
				g_savedGlobals.sPlayerData.sInfo.sWeapons[eT6H3_Ped].sWeaponInfo[GET_INT_FROM_PLAYER_PED_WEAPON_SLOT(eT6H3_WeaponSlot)].iAmmoCount = 50
				
				g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T6_HUNTING3)
				g_bWarpDebugPlayerCharScene = TRUE
				RETURN TRUE BREAK
			
		CASE SPCID_PRT6_TRAF_AIR
			Execute_Code_ID(CID_ACTIVATE_MINIGAME_TRAFFICKING)
			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T6_TRAF_AIR)
			g_bWarpDebugPlayerCharScene = TRUE
			RETURN TRUE BREAK
//		CASE SPCID_PRT6_DISPOSEBODY_A	g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T6_DISPOSEBODY_A) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRT6_DIGGING			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T6_DIGGING) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRT6_FLUSHESFOOT		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T6_FLUSHESFOOT) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRT_CN_PIER
		
			enumCharacterList ePed
			WEAPON_TYPE eWeaponType
			WEAPON_SLOT eWeaponSlot
			
			ePed = CHAR_TREVOR
			eWeaponType = WEAPONTYPE_PISTOL
			eWeaponSlot = GET_WEAPONTYPE_SLOT(eWeaponType)
			
			g_savedGlobals.sPlayerData.sInfo.sWeapons[ePed].sWeaponInfo[GET_INT_FROM_PLAYER_PED_WEAPON_SLOT(eWeaponSlot)].eWeaponType = eWeaponType
			g_savedGlobals.sPlayerData.sInfo.sWeapons[ePed].sWeaponInfo[GET_INT_FROM_PLAYER_PED_WEAPON_SLOT(eWeaponSlot)].iAmmoCount = 50
			
			g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T_CN_PIER)
			g_bWarpDebugPlayerCharScene = TRUE
			RETURN TRUE BREAK
		CASE SPCID_PRT6_LAKE		g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T6_LAKE) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		CASE SPCID_PRT_FLYING_PLANE	g_iSelectedDebugPlayerCharScene = ENUM_TO_INT(PR_SCENE_T_FLYING_PLANE) g_bWarpDebugPlayerCharScene = TRUE RETURN TRUE BREAK
		
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC


FUNC BOOL SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(enumFamilyMember eFamilyMember, enumFamilyEvents eFamilyEvent)
	
	enumFamilyMember eMember
	REPEAT MAX_FAMILY_MEMBER eMember
		g_eCurrentFamilyEvent[eMember] = NO_FAMILY_EVENTS
	ENDREPEAT
	
	g_eDebugSelectedMember					= eFamilyMember
	PRIVATE_Set_Current_Family_Member_Event(eFamilyMember, eFamilyEvent)
	
	g_bUpdatedFamilyEvents					= TRUE
	g_bDebugForceCreateFamily				= TRUE
	
	WAIT(0)
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SP_RUN_SCRIPT_FOR_FAMILY_CODEID(SP_MENU_CODE_ID_ENUM eCodeID)
	SWITCH eCodeID
		CASE SPCID_FAM_M_FAMILY_ON_LAPTOPS						SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_SON, FE_M_FAMILY_on_laptops) BREAK
		CASE SPCID_FAM_M_FAMILY_MIC4_locked_in_room				SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_SON, FE_M_FAMILY_MIC4_locked_in_room) BREAK
		CASE SPCID_FAM_M7_FAMILY_finished_breakfast				SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_SON, FE_M7_FAMILY_finished_breakfast) BREAK
		CASE SPCID_FAM_M7_FAMILY_finished_pizza					SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_SON, FE_M7_FAMILY_finished_pizza) BREAK
		CASE SPCID_FAM_M7_FAMILY_watching_TV					SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_SON, FE_M7_FAMILY_watching_TV) BREAK
		
		CASE SPCID_FAM_M2_SON_gaming_loop						SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_SON, FE_M2_SON_gaming_loop) BREAK
//		CASE SPCID_FAM_M2_SON_gaming_EXIT						SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_SON, FE_M2_SON_gaming_exit) BREAK
//		CASE SPCID_FAM_M7_SON_GAMING_EXIT						SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_SON, FE_M7_SON_gaming_exit) BREAK
		CASE SPCID_FAM_M_SON_SHOWER								SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_SON, FE_M_SON_rapping_in_the_shower) BREAK
		CASE SPCID_FAM_M_SON_BORROWS_CAR						SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_SON, FE_M_SON_Borrows_sisters_car) BREAK
		CASE SPCID_FAM_M_SON_WATCHING_PORN						SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_SON, FE_M_SON_watching_porn) BREAK
		CASE SPCID_FAM_M_SON_ASKS_FOR_FOOD						SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_SON, FE_M_SON_in_room_asks_for_munchies) BREAK
		CASE SPCID_FAM_M_SON_PHONECALL							SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_SON, FE_M_SON_phone_calls_in_room) BREAK
		CASE SPCID_FAM_M_SON_ON_ECSTASY							SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_SON, FE_M_SON_on_ecstasy_AND_friendly) BREAK
		CASE SPCID_FAM_M_SON_SISTER_FIGHT_A						SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_SON, FE_M_SON_Fighting_with_sister_A) BREAK
		CASE SPCID_FAM_M_SON_SISTER_FIGHT_B						SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_SON, FE_M_SON_Fighting_with_sister_B) BREAK
		CASE SPCID_FAM_M_SON_SISTER_FIGHT_C						SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_SON, FE_M_SON_Fighting_with_sister_C) BREAK
		CASE SPCID_FAM_M_SON_SISTER_FIGHT_D						SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_SON, FE_M_SON_Fighting_with_sister_D) BREAK
		CASE SPCID_FAM_M_SON_SMOKING_BONG						SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_SON, FE_M_SON_smoking_weed_in_a_bong) BREAK
		CASE SPCID_FAM_M_SON_RAIDS_FRIDGE						SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_SON, FE_M_SON_raids_fridge_for_food) BREAK
		CASE SPCID_FAM_M_SON_SLEEPING							SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_SON, FE_M_SON_sleeping) BREAK
		CASE SPCID_FAM_M7_SON_jumping_jacks						SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_SON, FE_M7_SON_jumping_jacks) BREAK
		CASE SPCID_FAM_M7_SON_gaming							SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_SON, FE_M7_SON_gaming) BREAK
		CASE SPCID_FAM_M7_SON_going_for_a_bike_ride				SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_SON, FE_M7_SON_going_for_a_bike_ride) BREAK
		CASE SPCID_FAM_M7_SON_coming_back_from_a_bike_ride		SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_SON, FE_M7_SON_coming_back_from_a_bike_ride) BREAK
		CASE SPCID_FAM_M7_SON_on_laptop_looking_for_jobs		SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_SON, FE_M7_SON_on_laptop_looking_for_jobs) BREAK
		CASE SPCID_FAM_M2_SON_WATCHTV							SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_SON, FE_M2_SON_watching_TV) BREAK
		CASE SPCID_FAM_M7_SON_WATCHTV_TRACEY					SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_SON, FE_M7_SON_watching_TV_with_tracey) BREAK
		
		CASE SPCID_FAM_M_DAUG_SUNBATHING						SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_DAUGHTER, FE_M2_DAUGHTER_sunbathing) BREAK
		CASE SPCID_FAM_M_DAUG_GOING_TO_CAR						SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_DAUGHTER, FE_M_DAUGHTER_Going_out_in_her_car) BREAK
		CASE SPCID_FAM_M_DAUG_HOME_DRUNK						SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_DAUGHTER, FE_M_DAUGHTER_Coming_home_drunk) BREAK
		CASE SPCID_FAM_M_DAUG_PHONECALL_a						SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_DAUGHTER, FE_M_DAUGHTER_on_phone_to_friends) BREAK
		CASE SPCID_FAM_M_DAUG_PHONECALL_b						SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_DAUGHTER, FE_M_DAUGHTER_on_phone_LOCKED) BREAK
		CASE SPCID_FAM_M_DAUG_SHOWER							SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_DAUGHTER, FE_M_DAUGHTER_shower) BREAK
		CASE SPCID_FAM_M_DAUG_WORKING_OUT						SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_DAUGHTER, FE_M_DAUGHTER_workout_with_mp3) BREAK
		CASE SPCID_FAM_M_DAUG_DANCE_PRACTICE					SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_DAUGHTER, FE_M_DAUGHTER_dancing_practice) BREAK
		CASE SPCID_FAM_M_DAUG_WATCHING_TV_SOBER					SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_DAUGHTER, FE_M_DAUGHTER_watching_TV_sober) BREAK
		CASE SPCID_FAM_M_DAUG_WATCHING_TV_DRUNK					SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_DAUGHTER, FE_M_DAUGHTER_watching_TV_drunk) BREAK
		CASE SPCID_FAM_M_DAUG_SAT_CRYING						SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_DAUGHTER, FE_M_DAUGHTER_crying_over_a_guy) BREAK
		CASE SPCID_FAM_M_DAUG_SCREAMS_AT_DAD					SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_DAUGHTER, FE_M_DAUGHTER_screaming_at_dad) BREAK
		CASE SPCID_FAM_M_DAUG_TOILET_PURGE						SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_DAUGHTER, FE_M_DAUGHTER_purges_in_the_bathroom) BREAK
		CASE SPCID_FAM_M_DAUG_SNIFFING_DRUGS					SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_DAUGHTER, FE_M_DAUGHTER_sniffs_drugs_in_toilet) BREAK
		CASE SPCID_FAM_M_DAUG_SEX_SOUNDS						SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_DAUGHTER, FE_M_DAUGHTER_sex_sounds_from_room) BREAK
		CASE SPCID_FAM_M_DAUG_WALKS_TO_MUSIC					SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_DAUGHTER, FE_M_DAUGHTER_walks_to_room_music) BREAK
		CASE SPCID_FAM_M_DAUG_SLEEPING							SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_DAUGHTER, FE_M_DAUGHTER_sleeping) BREAK
		CASE SPCID_FAM_M_DAUG_COUCHSLEEP						SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_DAUGHTER, FE_M_DAUGHTER_couchsleep) BREAK
		CASE SPCID_FAM_M7_DAUG_studying_on_phone				SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_DAUGHTER, FE_M7_DAUGHTER_studying_on_phone) BREAK
		CASE SPCID_FAM_M7_DAUG_studying_does_nails				SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_DAUGHTER, FE_M7_DAUGHTER_studying_does_nails) BREAK
		CASE SPCID_FAM_M7_DAUG_sunbathing						SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_DAUGHTER, FE_M7_DAUGHTER_sunbathing) BREAK
		
		CASE SPCID_FAM_M_WIFE_screams_at_mexmaid				SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_WIFE, FE_M_WIFE_screams_at_mexmaid) BREAK
		CASE SPCID_FAM_M2_WIFE_in_face_mask						SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_WIFE, FE_M2_WIFE_in_face_mask) BREAK
		CASE SPCID_FAM_M7_WIFE_in_face_mask						SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_WIFE, FE_M7_WIFE_in_face_mask) BREAK
		CASE SPCID_FAM_M_WIFE_playing_tennis					SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_WIFE, FE_M_WIFE_playing_tennis) BREAK
		CASE SPCID_FAM_M2_WIFE_doing_yoga						SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_WIFE, FE_M2_WIFE_doing_yoga) BREAK
		CASE SPCID_FAM_M7_WIFE_doing_yoga						SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_WIFE, FE_M7_WIFE_doing_yoga) BREAK
//		CASE SPCID_FAM_M_WIFE_getting_nails_done				SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_WIFE, FE_M_WIFE_getting_nails_done) BREAK
//		CASE SPCID_FAM_M_WIFE_leaving_in_car_v2					SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_WIFE, FE_M_WIFE_leaving_in_car_v2) BREAK
//		CASE SPCID_FAM_M_WIFE_MD_leaving_in_car_v3				SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_WIFE, FE_M_WIFE_MD_leaving_in_car_v3) BREAK
		CASE SPCID_FAM_M_WIFE_leaving_in_car					SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_WIFE, FE_M_WIFE_leaving_in_car) BREAK
		CASE SPCID_FAM_M2_WIFE_with_shopping_bags_enter			SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_WIFE, FE_M2_WIFE_with_shopping_bags_enter) BREAK
		CASE SPCID_FAM_M7_WIFE_with_shopping_bags_enter			SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_WIFE, FE_M7_WIFE_with_shopping_bags_enter) BREAK
//		CASE SPCID_FAM_M2_WIFE_with_shopping_bags_idle			SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_WIFE, FE_M2_WIFE_with_shopping_bags_idle) BREAK
//		CASE SPCID_FAM_M7_WIFE_with_shopping_bags_idle			SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_WIFE, FE_M7_WIFE_with_shopping_bags_idle) BREAK
//		CASE SPCID_FAM_M2_WIFE_with_shopping_bags_exit			SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_WIFE, FE_M2_WIFE_with_shopping_bags_exit) BREAK
//		CASE SPCID_FAM_M7_WIFE_with_shopping_bags_exit			SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_WIFE, FE_M7_WIFE_with_shopping_bags_exit) BREAK
		CASE SPCID_FAM_M_WIFE_gets_drink_in_kitchen				SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_WIFE, FE_M_WIFE_gets_drink_in_kitchen) BREAK
		CASE SPCID_FAM_M2_WIFE_sunbathing						SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_WIFE, FE_M2_WIFE_sunbathing) BREAK
		CASE SPCID_FAM_M7_WIFE_sunbathing						SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_WIFE, FE_M7_WIFE_sunbathing) BREAK
//		CASE SPCID_FAM_M_WIFE_getting_botox_done				SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_WIFE, FE_M_WIFE_getting_botox_done) BREAK
		CASE SPCID_FAM_M2_WIFE_passed_out_SOFA  				SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_WIFE, FE_M2_WIFE_passed_out_SOFA  ) BREAK
		CASE SPCID_FAM_M7_WIFE_passed_out_SOFA  				SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_WIFE, FE_M7_WIFE_passed_out_SOFA  ) BREAK
//		CASE SPCID_FAM_M_WIFE_SCREAMING_AT_SON_P1				SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_WIFE, FE_M_WIFE_screaming_at_son_P1) BREAK
		CASE SPCID_FAM_M_WIFE_SCREAMING_AT_SON_P2				SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_WIFE, FE_M_WIFE_screaming_at_son_P2) BREAK
		CASE SPCID_FAM_M_WIFE_SCREAMING_AT_SON_P3				SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_WIFE, FE_M_WIFE_screaming_at_son_P3) BREAK
		CASE SPCID_FAM_M_WIFE_screaming_at_daughter				SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_WIFE, FE_M_WIFE_screaming_at_daughter) BREAK
		CASE SPCID_FAM_M2_WIFE_phones_man_OR_therapist			SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_WIFE, FE_M2_WIFE_phones_man_OR_therapist) BREAK
		CASE SPCID_FAM_M7_WIFE_phones_man_OR_therapist			SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_WIFE, FE_M7_WIFE_phones_man_OR_therapist) BREAK
		CASE SPCID_FAM_M_WIFE_hangs_up_and_wanders				SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_WIFE, FE_M_WIFE_hangs_up_and_wanders) BREAK
		#IF NOT IS_JAPANESE_BUILD
		CASE SPCID_FAM_M2_WIFE_using_vibrator					SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_WIFE, FE_M2_WIFE_using_vibrator) BREAK
		CASE SPCID_FAM_M_WIFE_using_vibrator_END				SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_WIFE, FE_M_WIFE_using_vibrator_END) BREAK
		CASE SPCID_FAM_M7_WIFE_using_vibrator					SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_WIFE, FE_M7_WIFE_using_vibrator) BREAK
		#ENDIF
		CASE SPCID_FAM_M_WIFE_passed_out_BED					SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_WIFE, FE_M_WIFE_passed_out_BED) BREAK
		CASE SPCID_FAM_M2_WIFE_sleeping							SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_WIFE, FE_M2_WIFE_sleeping) BREAK
		CASE SPCID_FAM_M7_WIFE_sleeping							SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_WIFE, FE_M7_WIFE_sleeping) BREAK
		CASE SPCID_FAM_M7_WIFE_Making_juice						SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_WIFE, FE_M7_WIFE_Making_juice) BREAK
		CASE SPCID_FAM_M7_WIFE_shopping_with_daughter			SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_WIFE, FE_M7_WIFE_shopping_with_daughter) BREAK
//		CASE SPCID_FAM_M7_WIFE_shopping_with_son				SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_WIFE, FE_M7_WIFE_shopping_with_son) BREAK
//		CASE SPCID_FAM_M7_WIFE_on_phone							SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_WIFE, FE_M7_WIFE_on_phone) BREAK
		
//		CASE SPCID_FAM_M_MAID_cooking_for_son					SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_MEXMAID, FE_M_MEXMAID_cooking_for_son) BREAK
		CASE SPCID_FAM_M2_MAID_cleans_booze_pot_other			SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_MEXMAID, FE_M2_MEXMAID_cleans_booze_pot_other) BREAK
		CASE SPCID_FAM_M7_MAID_cleans_booze_pot_other			SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_MEXMAID, FE_M7_MEXMAID_cleans_booze_pot_other) BREAK
		CASE SPCID_FAM_M2_MAID_CLEAN_SURFACE_a					SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_MEXMAID, FE_M2_MEXMAID_clean_surface_a) BREAK
		CASE SPCID_FAM_M2_MAID_CLEAN_SURFACE_b					SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_MEXMAID, FE_M2_MEXMAID_clean_surface_b) BREAK
		CASE SPCID_FAM_M2_MAID_CLEAN_SURFACE_c					SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_MEXMAID, FE_M2_MEXMAID_clean_surface_c) BREAK
		CASE SPCID_FAM_M7_MAID_CLEAN_SURFACE					SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_MEXMAID, FE_M7_MEXMAID_clean_surface) BREAK
		CASE SPCID_FAM_M2_MAID_clean_window						SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_MEXMAID, FE_M2_MEXMAID_clean_window) BREAK
		CASE SPCID_FAM_M7_MAID_clean_window						SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_MEXMAID, FE_M7_MEXMAID_clean_window) BREAK
//		CASE SPCID_FAM_M_MAID_MIC4_CLEAN_SURFACE				SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_MEXMAID, FE_M_MEXMAID_MIC4_clean_surface) BREAK
		CASE SPCID_FAM_M_MAID_MIC4_CLEAN_WINDOW					SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_MEXMAID, FE_M_MEXMAID_MIC4_clean_window) BREAK
		CASE SPCID_FAM_M_MAID_does_the_dishes					SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_MEXMAID, FE_M_MEXMAID_does_the_dishes) BREAK
//		CASE SPCID_FAM_M_MAID_watching_TV						SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_MEXMAID, FE_M_MEXMAID_watching_TV) BREAK
		CASE SPCID_FAM_M_MAID_STEALING_STUFF					SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_MEXMAID, FE_M_MEXMAID_stealing_stuff) BREAK
		CASE SPCID_FAM_M_MAID_STEALING_STUFF_caught				SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_MEXMAID, FE_M_MEXMAID_stealing_stuff_caught) BREAK
		
		CASE SPCID_FAM_M_GARD_with_leaf_blower					SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_GARDENER, FE_M_GARDENER_with_leaf_blower) BREAK
		CASE SPCID_FAM_M_GARD_planting_flowers					SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_GARDENER, FE_M_GARDENER_planting_flowers) BREAK
//		CASE SPCID_FAM_M_GARD_trimming_hedges					SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_GARDENER, FE_M_GARDENER_trimming_hedges) BREAK
		CASE SPCID_FAM_M_GARD_cleaning_pool						SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_GARDENER, FE_M_GARDENER_cleaning_pool) BREAK
		CASE SPCID_FAM_M_GARD_mowing_lawn						SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_GARDENER, FE_M_GARDENER_mowing_lawn) BREAK
		CASE SPCID_FAM_M_GARD_watering_flowers					SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_GARDENER, FE_M_GARDENER_watering_flowers) BREAK
//		CASE SPCID_FAM_M_GARD_spraying_for_weeds				SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_GARDENER, FE_M_GARDENER_spraying_for_weeds) BREAK
		CASE SPCID_FAM_M_GARD_on_phone							SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_GARDENER, FE_M_GARDENER_on_phone) BREAK
		CASE SPCID_FAM_M_GARD_smoking_weed						SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_MICHAEL_GARDENER, FE_M_GARDENER_smoking_weed) BREAK
		
//		CASE SPCID_FAM_M_MICHAEL_MIC2_washing_face				SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_TREVOR_0_MICHAEL, FE_M_MICHAEL_MIC2_washing_face) BREAK
		
		CASE SPCID_FAM_F_AUNT_pelvic_floor_exercises			SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_FRANKLIN_AUNT, FE_F_AUNT_pelvic_floor_exercises) BREAK
		CASE SPCID_FAM_F_AUNT_in_face_mask						SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_FRANKLIN_AUNT, FE_F_AUNT_in_face_mask) BREAK
		CASE SPCID_FAM_F_AUNT_watching_TV						SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_FRANKLIN_AUNT, FE_F_AUNT_watching_TV) BREAK
		CASE SPCID_FAM_F_AUNT_listens_to_selfhelp_tapes			SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_FRANKLIN_AUNT, FE_F_AUNT_listens_to_selfhelp_tapes_x) BREAK
		CASE SPCID_FAM_F_AUNT_returned_to_aunts					SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_FRANKLIN_AUNT, FE_F_AUNT_returned_to_aunts) BREAK
		
//		CASE SPCID_FAM_F_LAMAR_and_STRETCH_chill_outside		SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_FRANKLIN_LAMAR, FE_F_LAMAR_and_STRETCH_chill_outside) BREAK
//		CASE SPCID_FAM_F_LAMAR_and_STRETCH_bbq_outside			SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_FRANKLIN_LAMAR, FE_F_LAMAR_and_STRETCH_bbq_outside) BREAK
//		CASE SPCID_FAM_F_LAMAR_and_STRETCH_arguing				SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_FRANKLIN_LAMAR, FE_F_LAMAR_and_STRETCH_arguing) BREAK
//		CASE SPCID_FAM_F_LAMAR_and_STRETCH_shout_at_cops		SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_FRANKLIN_LAMAR, FE_F_LAMAR_and_STRETCH_shout_at_cops) BREAK
		CASE SPCID_FAM_F_LAMAR_and_STRETCH_wandering			SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_FRANKLIN_LAMAR, FE_F_LAMAR_and_STRETCH_wandering) BREAK
		
		CASE SPCID_FAM_T0_RON_monitoring_police_frequency		SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_TREVOR_0_RON, FE_T0_RON_monitoring_police_frequency) BREAK
		CASE SPCID_FAM_T0_RON_listens_to_radio_broadcast		SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_TREVOR_0_RON, FE_T0_RON_listens_to_radio_broadcast) BREAK
		CASE SPCID_FAM_T0_RON_ranting_about_government_LAYING	SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_TREVOR_0_RON, FE_T0_RON_ranting_about_government_LAYING) BREAK
		CASE SPCID_FAM_T0_RON_ranting_about_government_SITTING	SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_TREVOR_0_RON, FE_T0_RON_ranting_about_government_SITTING) BREAK
		CASE SPCID_FAM_T0_RON_smoking_crystal					SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_TREVOR_0_RON, FE_T0_RON_smoking_crystal) BREAK
		CASE SPCID_FAM_T0_RON_drinks_moonshine_from_a_jar		SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_TREVOR_0_RON, FE_T0_RON_drinks_moonshine_from_a_jar) BREAK
		CASE SPCID_FAM_T0_RON_stares_through_binoculars			SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_TREVOR_0_RON, FE_T0_RON_stares_through_binoculars) BREAK
		
		CASE SPCID_FAM_T0_MICHAEL_depressed_head_in_hands		SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_TREVOR_0_MICHAEL, FE_T0_MICHAEL_depressed_head_in_hands) BREAK
		CASE SPCID_FAM_T0_MICHAEL_sunbathing					SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_TREVOR_0_MICHAEL, FE_T0_MICHAEL_sunbathing) BREAK
		CASE SPCID_FAM_T0_MICHAEL_drinking_beer					SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_TREVOR_0_MICHAEL, FE_T0_MICHAEL_drinking_beer) BREAK
		CASE SPCID_FAM_T0_MICHAEL_on_phone_to_therapist			SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_TREVOR_0_MICHAEL, FE_T0_MICHAEL_on_phone_to_therapist) BREAK
		CASE SPCID_FAM_T0_MICHAEL_hangs_up_and_wanders			SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_TREVOR_0_MICHAEL, FE_T0_MICHAEL_hangs_up_and_wanders) BREAK
		
		CASE SPCID_FAM_T0_TREVOR_smoking_crystal				SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_TREVOR_0_TREVOR, FE_T0_TREVOR_smoking_crystal) BREAK
		#IF NOT IS_JAPANESE_BUILD
		CASE SPCID_FAM_T0_TREVOR_doing_a_shit					SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_TREVOR_0_TREVOR, FE_T0_TREVOR_doing_a_shit) BREAK
		#ENDIF
		CASE SPCID_FAM_T0_TREVOR_blowing_shit_up				SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_TREVOR_0_TREVOR, FE_T0_TREVOR_blowing_shit_up) BREAK
		CASE SPCID_FAM_T0_TREVOR_passed_out_naked_drunk			SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_TREVOR_0_TREVOR, FE_T0_TREVOR_passed_out_naked_drunk) BREAK
		CASE SPCID_FAM_T0_TREVOR_and_kidnapped_wife_walk		SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_TREVOR_0_TREVOR, FE_T0_TREVOR_and_kidnapped_wife_walk) BREAK
		CASE SPCID_FAM_T0_TREVOR_and_kidnapped_wife_stare		SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_TREVOR_0_TREVOR, FE_T0_TREVOR_and_kidnapped_wife_stare) BREAK
		//CASE SPCID_FAM_T0_TREVOR_and_kidnapped_wife_laugh		SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_TREVOR_0_TREVOR, FE_T0_TREVOR_and_kidnapped_wife_laugh) BREAK
		
		CASE SPCID_FAM_T0_RONEX_outside_looking_lonely			SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_TREVOR_0_RON, FE_T0_RONEX_outside_looking_lonely) BREAK
		CASE SPCID_FAM_T0_RONEX_trying_to_pick_up_signals		SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_TREVOR_0_RON, FE_T0_RONEX_trying_to_pick_up_signals) BREAK
		CASE SPCID_FAM_T0_RONEX_working_a_moonshine_sill		SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_TREVOR_0_RON, FE_T0_RONEX_working_a_moonshine_sill) BREAK
		CASE SPCID_FAM_T0_RONEX_doing_target_practice			SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_TREVOR_0_RON, FE_T0_RONEX_doing_target_practice) BREAK
//		CASE SPCID_FAM_T0_RONEX_conspiracies_boring_Michael		SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_TREVOR_0_RON, FE_T0_RONEX_conspiracies_boring_Michael) BREAK
		
		CASE SPCID_FAM_T0_KIDNAPPED_WIFE_cleaning				SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_TREVOR_0_WIFE, FE_T0_KIDNAPPED_WIFE_cleaning) BREAK
		CASE SPCID_FAM_T0_KIDNAPPED_WIFE_does_garden_work		SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_TREVOR_0_WIFE, FE_T0_KIDNAPPED_WIFE_does_garden_work) BREAK
		CASE SPCID_FAM_T0_KIDNAPPED_WIFE_talks_to_Michael		SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_TREVOR_0_WIFE, FE_T0_KIDNAPPED_WIFE_talks_to_Michael) BREAK
		
		CASE SPCID_FAM_T0_MOTHER_duringRandomChar				SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_TREVOR_0_MOTHER, FE_T0_MOTHER_duringRandomChar) BREAK
//		CASE SPCID_FAM_T0_MOTHER_something_b					SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_TREVOR_0_MOTHER, FE_T0_MOTHER_duringRandomChar) BREAK
//		CASE SPCID_FAM_T0_MOTHER_something_c					SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_TREVOR_0_MOTHER, FE_T0_MOTHER_something_c) BREAK
		
		CASE SPCID_FAM_T1_FLOYD_cleaning
			SET_BUILDING_STATE(BUILDINGNAME_ES_FLOYDS_APPARTMENT,			BUILDINGSTATE_DESTROYED)
			
			#IF NOT IS_JAPANESE_BUILD
				SET_BUILDING_STATE (BUILDINGNAME_ES_FLOYDS_APPARTMENT_SEX_TOYS, BUILDINGSTATE_DESTROYED)
			#ENDIF
			
			SET_BUILDING_STATE(BUILDINGNAME_ES_FLOYDS_APPARTMENT_MESS_1,	BUILDINGSTATE_NORMAL)
			SET_BUILDING_STATE(BUILDINGNAME_ES_FLOYDS_APPARTMENT_MESS_2,	BUILDINGSTATE_CLEANUP)
			SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_TREVOR_1_FLOYD, FE_T1_FLOYD_cleaning) BREAK
		CASE SPCID_FAM_T1_FLOYD_CRIES_IN_FOETAL_POSITION		SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_TREVOR_1_FLOYD, FE_T1_FLOYD_cries_in_foetal_position) BREAK
		CASE SPCID_FAM_T1_FLOYD_cries_on_sofa					SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_TREVOR_1_FLOYD, FE_T1_FLOYD_cries_on_sofa) BREAK
		CASE SPCID_FAM_T1_FLOYD_pineapple
			SET_DOOR_STATE(DOORNAME_T_APARTMENT_VB, DOORSTATE_UNLOCKED)
			SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_TREVOR_1_FLOYD, FE_T1_FLOYD_pineapple)
			BREAK
		CASE SPCID_FAM_T1_FLOYD_on_phone_to_girlfriend			SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_TREVOR_1_FLOYD, FE_T1_FLOYD_on_phone_to_girlfriend) BREAK
		CASE SPCID_FAM_T1_FLOYD_hangs_up_and_wanders			SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_TREVOR_1_FLOYD, FE_T1_FLOYD_hangs_up_and_wanders) BREAK
		CASE SPCID_FAM_T1_FLOYD_hiding_from_Trevor_a			SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_TREVOR_1_FLOYD, FE_T1_FLOYD_hiding_from_Trevor_a) BREAK
		CASE SPCID_FAM_T1_FLOYD_hiding_from_Trevor_b			SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_TREVOR_1_FLOYD, FE_T1_FLOYD_hiding_from_Trevor_b) BREAK
		CASE SPCID_FAM_T1_FLOYD_hiding_from_Trevor_c			SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_TREVOR_1_FLOYD, FE_T1_FLOYD_hiding_from_Trevor_c) BREAK
		CASE SPCID_FAM_T1_FLOYD_is_sleeping						SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_TREVOR_1_FLOYD, FE_T1_FLOYD_is_sleeping) BREAK
		
//		CASE SPCID_FAM_T1_FLOYD_with_wade_post_trevor3			SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_TREVOR_1_FLOYD, FE_T1_FLOYD_with_wade_post_trevor3) BREAK
		CASE SPCID_FAM_T1_FLOYD_with_wade_post_docks1			SP_RUN_SCRIPT_FOR_THIS_FAMILY_CODEID(FM_TREVOR_1_FLOYD, FE_T1_FLOYD_with_wade_post_docks1) BREAK
		
		DEFAULT
			RETURN FALSE
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC


FUNC BOOL SP_RUN_SCRIPT_FOR_CODEID_ALT(DEBUG_MENU_DATA_STRUCT &sDebugMenuData)	
	SWITCH INT_TO_ENUM(SP_MENU_CODE_ID_ENUM, (sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].iAltCodeID[sDebugMenuData.iCurrentAltItem]))
		CASE SPCID_LAUNCH_FLOW_PRIOR
			SET_DEBUG_MENU_CUSTOM_PROCESS_FLAG(sDebugMenuData.iCustomProcessFlags, DM_CUSTOM_PROCESS_LAUNCH_TO_BEFORE_MISSION)
		BREAK
		CASE SPCID_LAUNCH_FLOW_AFTER
			SET_DEBUG_MENU_CUSTOM_PROCESS_FLAG(sDebugMenuData.iCustomProcessFlags, DM_CUSTOM_PROCESS_LAUNCH_TO_AFTER_MISSION)	
		BREAK


		DEFAULT
			CERRORLN(DEBUG_DEBUG_MENU, "Unknown Alt Code ID ", sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].iAltCodeID[sDebugMenuData.iCurrentAltItem])
		BREAK
	ENDSWITCH
	RETURN TRUE
ENDFUNC

/// PURPOSE: Runs a piece of script for the specified codeID
FUNC BOOL SP_RUN_SCRIPT_FOR_CODEID(DEBUG_MENU_DATA_STRUCT &sDebugMenuData, BOOL bUsePrelaunchCodeID)	
	TEXT_LABEL_15 sMissID

	SP_MENU_CODE_ID_ENUM eCodeID
	IF bUsePrelaunchCodeID
		eCodeID = INT_TO_ENUM(SP_MENU_CODE_ID_ENUM, (sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].iCodeIDPreLaunch))
	ELSE
		eCodeID = INT_TO_ENUM(SP_MENU_CODE_ID_ENUM, (sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].iCodeID))
	ENDIF
	
	SWITCH eCodeID
		CASE SPCID_DEFAULT
			// Do nothing
		BREAK
		
		CASE SPCID_HEIST
			// Do nothing - every heist mission from XML currently includes this codeID. No longer required but coul dbe useful in future.
		BREAK
		
		CASE SPCID_H_JEWEL_STEALTH_CR
			IF g_savedGlobals.sFlow.isGameflowActive
				SET_BIT(g_iDebugHeistDoCrewSelection, HEIST_JEWEL)
			ENDIF
			FALLTHRU
		CASE SPCID_H_JEWEL_STEALTH
			//Save choice to flow INT.
			Set_Mission_Flow_Int_Value(FLOWINT_HEIST_CHOICE_JEWEL,	HEIST_CHOICE_JEWEL_STEALTH)
			//Save choice to globals for the flow launcher to inspect.
			IF g_savedGlobals.sFlow.isGameflowActive	
				g_iDebugChoiceSelection[HEIST_JEWEL] = HEIST_CHOICE_JEWEL_STEALTH
			ELSE
				//In debug mode do crew selection right now.
				DEBUG_SET_RANDOM_CREW_FOR_HEIST(HEIST_JEWEL)
				DEBUG_DO_CREW_SELECTION_FOR_HEIST(HEIST_JEWEL)
			ENDIF
		BREAK
		
		CASE SPCID_H_JEWEL_STEALTH_P_ROUTE_1
			//Force prep route 1.
			g_iForcePrepMissionRoute = 1
			//Save choice to flow INT.
			Set_Mission_Flow_Int_Value(FLOWINT_HEIST_CHOICE_JEWEL,	HEIST_CHOICE_JEWEL_STEALTH)
			//Save choice to globals for the flow launcher to inspect.
			IF g_savedGlobals.sFlow.isGameflowActive	
				g_iDebugChoiceSelection[HEIST_JEWEL] = HEIST_CHOICE_JEWEL_STEALTH
			ENDIF
		BREAK
		
		CASE SPCID_H_JEWEL_STEALTH_P_ROUTE_2
			//Force prep route 2.
			g_iForcePrepMissionRoute = 2
			//Save choice to flow INT.
			Set_Mission_Flow_Int_Value(FLOWINT_HEIST_CHOICE_JEWEL,	HEIST_CHOICE_JEWEL_STEALTH)
			//Save choice to globals for the flow launcher to inspect.
			IF g_savedGlobals.sFlow.isGameflowActive	
				g_iDebugChoiceSelection[HEIST_JEWEL] = HEIST_CHOICE_JEWEL_STEALTH
			ENDIF
		BREAK
		
		CASE SPCID_H_JEWEL_HIGH_IMPACT_CR
			IF g_savedGlobals.sFlow.isGameflowActive
				SET_BIT(g_iDebugHeistDoCrewSelection, HEIST_JEWEL)
			ENDIF
			FALLTHRU
		CASE SPCID_H_JEWEL_HIGH_IMPACT
			//Save choice to flow INT.
			Set_Mission_Flow_Int_Value(FLOWINT_HEIST_CHOICE_JEWEL,	HEIST_CHOICE_JEWEL_HIGH_IMPACT)
			IF g_savedGlobals.sFlow.isGameflowActive
				//Save choice to globals for the flow launcher to inspect.
				g_iDebugChoiceSelection[HEIST_JEWEL] = HEIST_CHOICE_JEWEL_HIGH_IMPACT
			ELSE
				//In debug mode do crew selection right now.
				DEBUG_SET_RANDOM_CREW_FOR_HEIST(HEIST_JEWEL)
				DEBUG_DO_CREW_SELECTION_FOR_HEIST(HEIST_JEWEL)
			ENDIF
		BREAK
		
		CASE SPCID_H_HIGH_IMP_P_ROUTE_1
			//Force prep route 1.
			g_iForcePrepMissionRoute = 1
			//Save choice to flow INT.
			Set_Mission_Flow_Int_Value(FLOWINT_HEIST_CHOICE_JEWEL,	HEIST_CHOICE_JEWEL_HIGH_IMPACT)
			//Save choice to globals for the flow launcher to inspect.
			IF g_savedGlobals.sFlow.isGameflowActive	
				g_iDebugChoiceSelection[HEIST_JEWEL] = HEIST_CHOICE_JEWEL_HIGH_IMPACT
			ENDIF
		BREAK
		
		CASE SPCID_H_HIGH_IMP_P_ROUTE_2
			//Force prep route 2.
			g_iForcePrepMissionRoute = 2
			//Save choice to flow INT.
			Set_Mission_Flow_Int_Value(FLOWINT_HEIST_CHOICE_JEWEL,	HEIST_CHOICE_JEWEL_HIGH_IMPACT)
			//Save choice to globals for the flow launcher to inspect.
			IF g_savedGlobals.sFlow.isGameflowActive	
				g_iDebugChoiceSelection[HEIST_JEWEL] = HEIST_CHOICE_JEWEL_HIGH_IMPACT
			ENDIF
		BREAK
		
		CASE SPCID_H_HIGH_IMP_P_ROUTE_3
			//Force prep route 3.
			g_iForcePrepMissionRoute = 3
			//Save choice to flow INT.
			Set_Mission_Flow_Int_Value(FLOWINT_HEIST_CHOICE_JEWEL,	HEIST_CHOICE_JEWEL_HIGH_IMPACT)
			//Save choice to globals for the flow launcher to inspect.
			IF g_savedGlobals.sFlow.isGameflowActive	
				g_iDebugChoiceSelection[HEIST_JEWEL] = HEIST_CHOICE_JEWEL_HIGH_IMPACT
			ENDIF
		BREAK
		
		CASE SPCID_H_JEWEL_SETUP
			// Put player in a suitable suit
			IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_DEFAULT, FALSE)
				ENDIF
			ENDIF
		BREAK
		
		CASE SPCID_H_DOCKS_DEEP_SEA_CR
			IF g_savedGlobals.sFlow.isGameflowActive
				SET_BIT(g_iDebugHeistDoCrewSelection, HEIST_DOCKS)
			ENDIF
			FALLTHRU
		CASE SPCID_H_DOCKS_DEEP_SEA
			//Save choice to flow INT.
			Set_Mission_Flow_Int_Value(FLOWINT_HEIST_CHOICE_DOCKS,	HEIST_CHOICE_DOCKS_DEEP_SEA)
			IF g_savedGlobals.sFlow.isGameflowActive
				//Save choice to globals for the flow launcher to inspect.
				g_iDebugChoiceSelection[HEIST_DOCKS] = HEIST_CHOICE_DOCKS_DEEP_SEA
			ELSE
				//In debug mode do crew selection right now.
				DEBUG_SET_RANDOM_CREW_FOR_HEIST(HEIST_DOCKS)
				DEBUG_DO_CREW_SELECTION_FOR_HEIST(HEIST_DOCKS)
			ENDIF
		BREAK
		
		
		CASE SPCID_H_DOCKS_BLOW_UP_BOAT_CR
			IF g_savedGlobals.sFlow.isGameflowActive
				SET_BIT(g_iDebugHeistDoCrewSelection, HEIST_DOCKS)
			ENDIF
			FALLTHRU
		CASE SPCID_H_DOCKS_BLOW_UP_BOAT
			//Save choice to flow INT.
			Set_Mission_Flow_Int_Value(FLOWINT_HEIST_CHOICE_DOCKS,	HEIST_CHOICE_DOCKS_BLOW_UP_BOAT)
			IF g_savedGlobals.sFlow.isGameflowActive
				//Save choice to globals for the flow launcher to inspect.
				g_iDebugChoiceSelection[HEIST_DOCKS] = HEIST_CHOICE_DOCKS_BLOW_UP_BOAT
			ELSE
				//In debug mode do crew selection right now.
				DEBUG_SET_RANDOM_CREW_FOR_HEIST(HEIST_DOCKS)
				DEBUG_DO_CREW_SELECTION_FOR_HEIST(HEIST_DOCKS)
			ENDIF
		BREAK
		
		
		CASE SPCID_H_RURAL_NO_TANK_ALL_CR
			IF g_savedGlobals.sFlow.isGameflowActive
				SET_BIT(g_iDebugHeistDoCrewSelection, HEIST_JEWEL)
			ENDIF
			FALLTHRU
		CASE SPCID_H_RURAL_NO_TANK_CR
			IF g_savedGlobals.sFlow.isGameflowActive
				SET_BIT(g_iDebugHeistDoCrewSelection, HEIST_RURAL_BANK)
			ENDIF
			FALLTHRU
		CASE SPCID_H_RURAL_NO_TANK
			//Save choice to flow INT.
			Set_Mission_Flow_Int_Value(FLOWINT_HEIST_CHOICE_RURAL,	HEIST_CHOICE_RURAL_NO_TANK)
			IF g_savedGlobals.sFlow.isGameflowActive
				//Save choice to globals for the flow launcher to inspect.
				g_iDebugChoiceSelection[HEIST_RURAL_BANK] = HEIST_CHOICE_RURAL_NO_TANK
			ELSE
				//In debug mode do crew selection right now.
				DEBUG_SET_RANDOM_CREW_FOR_HEIST(HEIST_RURAL_BANK)
				DEBUG_DO_CREW_SELECTION_FOR_HEIST(HEIST_RURAL_BANK)
			ENDIF
		BREAK
		
		
		CASE SPCID_H_RURAL_NO_TANK_P_ROUTE_1
			//Force prep route 1.
			g_iForcePrepMissionRoute = 1
			//Save choice to flow INT.
			Set_Mission_Flow_Int_Value(FLOWINT_HEIST_CHOICE_RURAL,	HEIST_CHOICE_RURAL_NO_TANK)
			IF g_savedGlobals.sFlow.isGameflowActive
				//Save choice to globals for the flow launcher to inspect.
				g_iDebugChoiceSelection[HEIST_RURAL_BANK] = HEIST_CHOICE_RURAL_NO_TANK
			ELSE
				//In debug mode do crew selection right now.
				DEBUG_SET_RANDOM_CREW_FOR_HEIST(HEIST_RURAL_BANK)
				DEBUG_DO_CREW_SELECTION_FOR_HEIST(HEIST_RURAL_BANK)
			ENDIF
		BREAK
		
		
		CASE SPCID_H_RURAL_NO_TANK_P_ROUTE_2
			//Force prep route 2.
			g_iForcePrepMissionRoute = 2
			//Save choice to flow INT.
			Set_Mission_Flow_Int_Value(FLOWINT_HEIST_CHOICE_RURAL,	HEIST_CHOICE_RURAL_NO_TANK)
			IF g_savedGlobals.sFlow.isGameflowActive
				//Save choice to globals for the flow launcher to inspect.
				g_iDebugChoiceSelection[HEIST_RURAL_BANK] = HEIST_CHOICE_RURAL_NO_TANK
			ELSE
				//In debug mode do crew selection right now.
				DEBUG_SET_RANDOM_CREW_FOR_HEIST(HEIST_RURAL_BANK)
				DEBUG_DO_CREW_SELECTION_FOR_HEIST(HEIST_RURAL_BANK)
			ENDIF
		BREAK
		
		
		CASE SPCID_H_AGENCY_FIRETRUCK_ALL_CR
			IF g_savedGlobals.sFlow.isGameflowActive
				SET_BIT(g_iDebugHeistDoCrewSelection, HEIST_JEWEL)
				SET_BIT(g_iDebugHeistDoCrewSelection, HEIST_RURAL_BANK)
			ENDIF
			FALLTHRU
		CASE SPCID_H_AGENCY_FIRETRUCK_CR
			IF g_savedGlobals.sFlow.isGameflowActive
				SET_BIT(g_iDebugHeistDoCrewSelection, HEIST_AGENCY)
			ENDIF
			FALLTHRU
		CASE SPCID_H_AGENCY_FIRETRUCK
			//Save choice to flow INT.
			Set_Mission_Flow_Int_Value(FLOWINT_HEIST_CHOICE_AGENCY,	HEIST_CHOICE_AGENCY_FIRETRUCK)
			IF g_savedGlobals.sFlow.isGameflowActive
				//Save choice to globals for the flow launcher to inspect.
				g_iDebugChoiceSelection[HEIST_AGENCY] = HEIST_CHOICE_AGENCY_FIRETRUCK
			ELSE
				//In debug mode do crew selection right now.
				DEBUG_SET_RANDOM_CREW_FOR_HEIST(HEIST_AGENCY)
				DEBUG_DO_CREW_SELECTION_FOR_HEIST(HEIST_AGENCY)
			ENDIF
		BREAK
		
		
		
		CASE SPCID_H_AGENCY_HELICOPTER_ALL_CR
			IF g_savedGlobals.sFlow.isGameflowActive
				SET_BIT(g_iDebugHeistDoCrewSelection, HEIST_JEWEL)
				SET_BIT(g_iDebugHeistDoCrewSelection, HEIST_RURAL_BANK)
			ENDIF
			FALLTHRU
		CASE SPCID_H_AGENCY_HELICOPTER_CR
			IF g_savedGlobals.sFlow.isGameflowActive
				SET_BIT(g_iDebugHeistDoCrewSelection, HEIST_AGENCY)
			ENDIF
			FALLTHRU
		CASE SPCID_H_AGENCY_HELICOPTER
			//Save choice to flow INT.
			Set_Mission_Flow_Int_Value(FLOWINT_HEIST_CHOICE_AGENCY,	HEIST_CHOICE_AGENCY_HELICOPTER)
			IF g_savedGlobals.sFlow.isGameflowActive
				//Save choice to globals for the flow launcher to inspect.
				g_iDebugChoiceSelection[HEIST_AGENCY] = HEIST_CHOICE_AGENCY_HELICOPTER
			ELSE
				//In debug mode do crew selection right now.
				DEBUG_SET_RANDOM_CREW_FOR_HEIST(HEIST_AGENCY)
				DEBUG_DO_CREW_SELECTION_FOR_HEIST(HEIST_AGENCY)
			ENDIF
		BREAK
		
		
		CASE SPCID_H_FINALE_TRAFFIC_ALL_CR
			IF g_savedGlobals.sFlow.isGameflowActive
				SET_BIT(g_iDebugHeistDoCrewSelection, HEIST_JEWEL)
				SET_BIT(g_iDebugHeistDoCrewSelection, HEIST_RURAL_BANK)
				SET_BIT(g_iDebugHeistDoCrewSelection, HEIST_AGENCY)
			ENDIF
			FALLTHRU
		CASE SPCID_H_FINALE_TRAFFIC_CR
			IF g_savedGlobals.sFlow.isGameflowActive
				SET_BIT(g_iDebugHeistDoCrewSelection, HEIST_FINALE)
			ENDIF
			FALLTHRU
		CASE SPCID_H_FINALE_TRAFFIC
			//Save choice to flow INT.
			Set_Mission_Flow_Int_Value(FLOWINT_HEIST_CHOICE_FINALE,	HEIST_CHOICE_FINALE_TRAFFCONT)
			IF g_savedGlobals.sFlow.isGameflowActive
				//Save choice to globals for the flow launcher to inspect.
				g_iDebugChoiceSelection[HEIST_FINALE] = HEIST_CHOICE_FINALE_TRAFFCONT
			ELSE
				//In debug mode do crew selection right now.
				DEBUG_SET_RANDOM_CREW_FOR_HEIST(HEIST_FINALE)
				DEBUG_DO_CREW_SELECTION_FOR_HEIST(HEIST_FINALE)
			ENDIF
		BREAK
		
		
		CASE SPCID_H_FINALE_CHOPPER_ALL_CR
			IF g_savedGlobals.sFlow.isGameflowActive
				SET_BIT(g_iDebugHeistDoCrewSelection, HEIST_JEWEL)
				SET_BIT(g_iDebugHeistDoCrewSelection, HEIST_RURAL_BANK)
				SET_BIT(g_iDebugHeistDoCrewSelection, HEIST_AGENCY)
			ENDIF
			FALLTHRU
		CASE SPCID_H_FINALE_CHOPPER_CR
			IF g_savedGlobals.sFlow.isGameflowActive
				SET_BIT(g_iDebugHeistDoCrewSelection, HEIST_FINALE)
			ENDIF
			FALLTHRU
		CASE SPCID_H_FINALE_CHOPPER
			//Save choice to flow INT.
			Set_Mission_Flow_Int_Value(FLOWINT_HEIST_CHOICE_FINALE, HEIST_CHOICE_FINALE_HELI)
			IF g_savedGlobals.sFlow.isGameflowActive
				//Save choice to globals for the flow launcher to inspect.
				g_iDebugChoiceSelection[HEIST_FINALE] = HEIST_CHOICE_FINALE_HELI
			ELSE
				//In debug mode do crew selection right now.
				DEBUG_SET_RANDOM_CREW_FOR_HEIST(HEIST_FINALE)
				DEBUG_DO_CREW_SELECTION_FOR_HEIST(HEIST_FINALE)
			ENDIF
		BREAK
		
		
		CASE SPCID_SET_ROOM_FOR_GAME_VIEWPORT
			WAIT(0)
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				INT iRoomKey 
				iRoomKey = GET_KEY_FOR_ENTITY_IN_ROOM(PLAYER_PED_ID())
				SET_ROOM_FOR_GAME_VIEWPORT_BY_KEY(iRoomKey)
			ENDIF
		BREAK

		CASE SPCID_DO_PLAYTHROUGH
			DEBUG_MENU_CLEANUP_RUNNING_MISSION_SCRIPTS(sDebugMenuData)

			#IF IS_DEBUG_BUILD
				//pause right here and show the options script
				//g_flowUnsaved.bUpdatingGameflow = FALSE
				SET_PLAYER_CONTROL(GET_PLAYER_INDEX(),FALSE)
				REQUEST_SCRIPT("debug_app_select_screen")
				WHILE NOT HAS_SCRIPT_LOADED("debug_app_select_screen")
					WAIT(0)
				ENDWHILE
				START_NEW_SCRIPT("debug_app_select_screen",0)
				SET_SCRIPT_AS_NO_LONGER_NEEDED("debug_app_select_screen")
				WHILE GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("debug_app_select_screen")) > 0
					//PRINTSTRING("Debug menu holding until debug_app_select_screen finishes.\n")
					WAIT(0)
				ENDWHILE
				SET_PLAYER_CONTROL(GET_PLAYER_INDEX(),TRUE)
				SET_PAUSE_MENU_ACTIVE(FALSE)
			#ENDIF
			
			Activate_Gameflow_At_First_Strand()
		BREAK
		
		CASE SPCID_GENERATE_FLOW
			//Setup which heist choices to use.
			g_iDebugHeistDoCrewSelection = 0
			g_iDebugChoiceSelection[HEIST_JEWEL]		= HEIST_CHOICE_JEWEL_STEALTH
			g_iDebugChoiceSelection[HEIST_DOCKS] 		= HEIST_CHOICE_DOCKS_DEEP_SEA
			g_iDebugChoiceSelection[HEIST_RURAL_BANK] 	= HEIST_CHOICE_RURAL_NO_TANK
			g_iDebugChoiceSelection[HEIST_AGENCY] 		= HEIST_CHOICE_AGENCY_FIRETRUCK
			g_iDebugChoiceSelection[HEIST_FINALE] 		= HEIST_CHOICE_FINALE_TRAFFCONT
			SET_DEBUG_MENU_CUSTOM_PROCESS_FLAG(sDebugMenuData.iCustomProcessFlags, DM_CUSTOM_PROCESS_GENERATE_FLOW)
		BREAK
		
		CASE SPCID_DISPLAY_FLOW
			SET_DEBUG_MENU_CUSTOM_PROCESS_FLAG(sDebugMenuData.iCustomProcessFlags, DM_CUSTOM_PROCESS_ACTIVATE_FLOW_DIAGRAM)
		BREAK
		
		CASE SPCID_GAMEFLOW_ON
			g_savedGlobals.sFlow.isGameflowActive	= TRUE
		BREAK
		
		CASE SPCID_GAMEFLOW_OFF
			g_savedGlobals.sFlow.isGameflowActive	= FALSE
		BREAK
		
		CASE SPCID_RANDOM_EVENTS_ON
			Set_Mission_Flow_Flag_State(FLOWFLAG_ALLOW_RANDOM_EVENTS, TRUE)
		BREAK	
		
		CASE SPCID_RANDOM_EVENTS_OFF
			Set_Mission_Flow_Flag_State(FLOWFLAG_ALLOW_RANDOM_EVENTS, FALSE)
		BREAK	
		
		CASE SPCID_LBDBUGS_ON
			g_bDisplayLBDBugSummary = TRUE
		BREAK
		
		CASE SPCID_LBDBUGS_OFF
			g_bDisplayLBDBugSummary = FALSE
		BREAK
		
#IF NOT IS_JAPANESE_BUILD
		CASE SPCID_PREORDER_ON
			g_bPreorderGame = TRUE
		BREAK
		
		CASE SPCID_PREORDER_OFF
			g_bPreorderGame = FALSE
		BREAK
		
		CASE SPCID_PREORDER_NG_ON
			g_bPreorderGameNextGen = TRUE
		BREAK
		
		CASE SPCID_PREORDER_NG_OFF
			g_bPreorderGameNextGen = FALSE
		BREAK
#ENDIF

#IF IS_JAPANESE_BUILD
		CASE SPCID_JAPANESE_SE_ON
			g_bJapaneseSpecialEditionGame = TRUE
		BREAK
		
		CASE SPCID_JAPANESE_SE_OFF
			g_bJapaneseSpecialEditionGame = FALSE
		BREAK
#ENDIF

		CASE SPCID_GENERATE_PLAYTHROUGH
			//Setup which heist choices to use.
			g_iDebugHeistDoCrewSelection = 0
			g_iDebugChoiceSelection[HEIST_JEWEL]		= HEIST_CHOICE_JEWEL_STEALTH
			g_iDebugChoiceSelection[HEIST_DOCKS] 		= HEIST_CHOICE_DOCKS_DEEP_SEA
			g_iDebugChoiceSelection[HEIST_RURAL_BANK] 	= HEIST_CHOICE_RURAL_NO_TANK
			g_iDebugChoiceSelection[HEIST_AGENCY] 		= HEIST_CHOICE_AGENCY_FIRETRUCK
			g_iDebugChoiceSelection[HEIST_FINALE] 		= HEIST_CHOICE_FINALE_TRAFFCONT
			SET_DEBUG_MENU_CUSTOM_PROCESS_FLAG(sDebugMenuData.iCustomProcessFlags, DM_CUSTOM_PROCESS_GENERATE_PLAYTHROUGH)
		BREAK
		CASE SPCID_GENERATE_PLAYTHROUGHCLF			
			SET_DEBUG_MENU_CUSTOM_PROCESS_FLAG(sDebugMenuData.iCustomProcessFlags, DM_CUSTOM_PROCESS_GENERATE_PLAYTHROUGH)
		BREAK
		CASE SPCID_GENERATE_PLAYTHROUGHNRM		
			SET_DEBUG_MENU_CUSTOM_PROCESS_FLAG(sDebugMenuData.iCustomProcessFlags, DM_CUSTOM_PROCESS_GENERATE_PLAYTHROUGH)
		BREAK
		
		CASE SPCID_PLAYTHROUGH_BUILDER
			//Setup which heist choices to use.
			g_iDebugHeistDoCrewSelection = 0
			g_iDebugChoiceSelection[HEIST_JEWEL]		= HEIST_CHOICE_JEWEL_STEALTH
			g_iDebugChoiceSelection[HEIST_DOCKS] 		= HEIST_CHOICE_DOCKS_DEEP_SEA
			g_iDebugChoiceSelection[HEIST_RURAL_BANK] 	= HEIST_CHOICE_RURAL_NO_TANK
			g_iDebugChoiceSelection[HEIST_AGENCY] 		= HEIST_CHOICE_AGENCY_FIRETRUCK
			g_iDebugChoiceSelection[HEIST_FINALE] 		= HEIST_CHOICE_FINALE_TRAFFCONT
			SET_DEBUG_MENU_CUSTOM_PROCESS_FLAG(sDebugMenuData.iCustomProcessFlags, DM_CUSTOM_PROCESS_PLAYTHROUGH_BUILDER)
		BREAK
		CASE SPCID_PLAYTHROUGH_BUILDERCLF			
			SET_DEBUG_MENU_CUSTOM_PROCESS_FLAG(sDebugMenuData.iCustomProcessFlags, DM_CUSTOM_PROCESS_PLAYTHROUGH_BUILDER)
		BREAK
		CASE SPCID_PLAYTHROUGH_BUILDERNRM			
			SET_DEBUG_MENU_CUSTOM_PROCESS_FLAG(sDebugMenuData.iCustomProcessFlags, DM_CUSTOM_PROCESS_PLAYTHROUGH_BUILDER)
		BREAK
		
		CASE SPCID_TURN_ON_ALL_INTERIORS
			SET_INTERIOR_CAPPED(INTERIOR_V_COMEDY, FALSE)
			SET_INTERIOR_CAPPED(INTERIOR_V_PSYCHEOFFICE, FALSE)
			SET_INTERIOR_CAPPED(INTERIOR_V_RANCH, FALSE)
			SET_INTERIOR_CAPPED(INTERIOR_V_RECYCLE, FALSE)
			SET_INTERIOR_CAPPED(INTERIOR_V_ROCKCLUB, FALSE)
			SET_INTERIOR_CAPPED(INTERIOR_V_JANITOR, FALSE)
			SET_INTERIOR_CAPPED(INTERIOR_V_LESTERS, FALSE)
			SET_INTERIOR_CAPPED(INTERIOR_V_TORTURE, FALSE)
			SET_INTERIOR_CAPPED(INTERIOR_V_CINEMA_VINEWOOD, FALSE)
			SET_INTERIOR_CAPPED(INTERIOR_V_CINEMA_MORNINGWOOD, FALSE)
			SET_INTERIOR_CAPPED(INTERIOR_V_CINEMA_DOWNTOWN, FALSE)
			SET_INTERIOR_CAPPED(INTERIOR_V_EPSILONISM, FALSE)
			SET_INTERIOR_CAPPED(INTERIOR_V_GARAGES, FALSE)
			SET_INTERIOR_CAPPED(INTERIOR_V_GARAGEM, FALSE)
			SET_INTERIOR_CAPPED(INTERIOR_V_GARAGEL, FALSE)
			SET_INTERIOR_CAPPED(INTERIOR_V_SHERIFF, FALSE)
			SET_INTERIOR_CAPPED(INTERIOR_V_SHERIFF2, FALSE)
			SET_INTERIOR_CAPPED(INTERIOR_V_LAB, FALSE)
			SET_INTERIOR_CAPPED(INTERIOR_V_FARMHOUSE, FALSE)
			SET_INTERIOR_CAPPED(INTERIOR_V_CHOPSHOP, FALSE)
			SET_INTERIOR_CAPPED(INTERIOR_V_FOUNDRY, FALSE)
			SET_INTERIOR_CAPPED(INTERIOR_V_ABATTOIR, FALSE)
			SET_INTERIOR_CAPPED(INTERIOR_V_58_SOL_OFFICE, FALSE)
			SET_INTERIOR_CAPPED(INTERIOR_V_APARTMENT_HIGH_1, FALSE)
			SET_INTERIOR_CAPPED(INTERIOR_V_APARTMENT_HIGH_2, FALSE)
			SET_INTERIOR_CAPPED(INTERIOR_V_APARTMENT_HIGH_3, FALSE)
			SET_INTERIOR_CAPPED(INTERIOR_V_APARTMENT_HIGH_4, FALSE)
			SET_INTERIOR_CAPPED(INTERIOR_V_APARTMENT_HIGH_5, FALSE)
			SET_INTERIOR_CAPPED(INTERIOR_V_APARTMENT_HIGH_6, FALSE)
			SET_INTERIOR_CAPPED(INTERIOR_V_APARTMENT_HIGH_7, FALSE)
			SET_INTERIOR_CAPPED(INTERIOR_V_APARTMENT_HIGH_8, FALSE)
			SET_INTERIOR_CAPPED(INTERIOR_V_APARTMENT_HIGH_9, FALSE)
			SET_INTERIOR_CAPPED(INTERIOR_V_APARTMENT_HIGH_10, FALSE)
			SET_INTERIOR_CAPPED(INTERIOR_V_APARTMENT_HIGH_11, FALSE)
			SET_INTERIOR_CAPPED(INTERIOR_V_APARTMENT_HIGH_12, FALSE)
			SET_INTERIOR_CAPPED(INTERIOR_V_APARTMENT_HIGH_13, FALSE)
			SET_INTERIOR_CAPPED(INTERIOR_V_APARTMENT_HIGH_14, FALSE)
			SET_INTERIOR_CAPPED(INTERIOR_V_APARTMENT_HIGH_15, FALSE)
			SET_INTERIOR_CAPPED(INTERIOR_V_APARTMENT_HIGH_16, FALSE)
			SET_INTERIOR_CAPPED(INTERIOR_V_APARTMENT_HIGH_17, FALSE)
			SET_INTERIOR_CAPPED(INTERIOR_V_APARTMENT_HIGH_18, FALSE)
			SET_INTERIOR_CAPPED(INTERIOR_V_APARTMENT_HIGH_19, FALSE)
			SET_INTERIOR_CAPPED(INTERIOR_V_APARTMENT_HIGH_20, FALSE)
			SET_INTERIOR_CAPPED(INTERIOR_V_APART_MIDSPAZ, FALSE)
			SET_INTERIOR_CAPPED(INTERIOR_V_GARAGEM_SP, FALSE)
			SET_INTERIOR_CAPPED(INTERIOR_V_BAHAMA, FALSE)
			
			SET_INTERIOR_DISABLED(INTERIOR_V_COMEDY, FALSE)
			SET_INTERIOR_DISABLED(INTERIOR_V_PSYCHEOFFICE, FALSE)
			SET_INTERIOR_DISABLED(INTERIOR_V_RANCH, FALSE)
			SET_INTERIOR_DISABLED(INTERIOR_V_RECYCLE, FALSE)
			SET_INTERIOR_DISABLED(INTERIOR_V_ROCKCLUB, FALSE)
			SET_INTERIOR_DISABLED(INTERIOR_V_JANITOR, FALSE)
			SET_INTERIOR_DISABLED(INTERIOR_V_LESTERS, FALSE)
			SET_INTERIOR_DISABLED(INTERIOR_V_TORTURE, FALSE)
			SET_INTERIOR_DISABLED(INTERIOR_V_CINEMA_VINEWOOD, FALSE)
			SET_INTERIOR_DISABLED(INTERIOR_V_CINEMA_MORNINGWOOD, FALSE)
			SET_INTERIOR_DISABLED(INTERIOR_V_CINEMA_DOWNTOWN, FALSE)
			SET_INTERIOR_DISABLED(INTERIOR_V_EPSILONISM, FALSE)
			SET_INTERIOR_DISABLED(INTERIOR_V_GARAGES, FALSE)
			SET_INTERIOR_DISABLED(INTERIOR_V_GARAGEM, FALSE)
			SET_INTERIOR_DISABLED(INTERIOR_V_GARAGEL, FALSE)
			SET_INTERIOR_DISABLED(INTERIOR_V_SHERIFF, FALSE)
			SET_INTERIOR_DISABLED(INTERIOR_V_SHERIFF2, FALSE)
			SET_INTERIOR_DISABLED(INTERIOR_V_LAB, FALSE)
			SET_INTERIOR_DISABLED(INTERIOR_V_FARMHOUSE, FALSE)
			SET_INTERIOR_DISABLED(INTERIOR_V_CHOPSHOP, FALSE)
			SET_INTERIOR_DISABLED(INTERIOR_V_FOUNDRY, FALSE)
			SET_INTERIOR_DISABLED(INTERIOR_V_ABATTOIR, FALSE)
			SET_INTERIOR_DISABLED(INTERIOR_V_58_SOL_OFFICE, FALSE)
			SET_INTERIOR_DISABLED(INTERIOR_V_APARTMENT_HIGH_1, FALSE)
			SET_INTERIOR_DISABLED(INTERIOR_V_APARTMENT_HIGH_2, FALSE)
			SET_INTERIOR_DISABLED(INTERIOR_V_APARTMENT_HIGH_3, FALSE)
			SET_INTERIOR_DISABLED(INTERIOR_V_APARTMENT_HIGH_4, FALSE)
			SET_INTERIOR_DISABLED(INTERIOR_V_APARTMENT_HIGH_5, FALSE)
			SET_INTERIOR_DISABLED(INTERIOR_V_APARTMENT_HIGH_6, FALSE)
			SET_INTERIOR_DISABLED(INTERIOR_V_APARTMENT_HIGH_7, FALSE)
			SET_INTERIOR_DISABLED(INTERIOR_V_APARTMENT_HIGH_8, FALSE)
			SET_INTERIOR_DISABLED(INTERIOR_V_APARTMENT_HIGH_9, FALSE)
			SET_INTERIOR_DISABLED(INTERIOR_V_APARTMENT_HIGH_10, FALSE)
			SET_INTERIOR_DISABLED(INTERIOR_V_APARTMENT_HIGH_11, FALSE)
			SET_INTERIOR_DISABLED(INTERIOR_V_APARTMENT_HIGH_12, FALSE)
			SET_INTERIOR_DISABLED(INTERIOR_V_APARTMENT_HIGH_13, FALSE)
			SET_INTERIOR_DISABLED(INTERIOR_V_APARTMENT_HIGH_14, FALSE)
			SET_INTERIOR_DISABLED(INTERIOR_V_APARTMENT_HIGH_15, FALSE)
			SET_INTERIOR_DISABLED(INTERIOR_V_APARTMENT_HIGH_16, FALSE)
			SET_INTERIOR_DISABLED(INTERIOR_V_APARTMENT_HIGH_17, FALSE)
			SET_INTERIOR_DISABLED(INTERIOR_V_APARTMENT_HIGH_18, FALSE)
			SET_INTERIOR_DISABLED(INTERIOR_V_APARTMENT_HIGH_19, FALSE)
			SET_INTERIOR_DISABLED(INTERIOR_V_APARTMENT_HIGH_20, FALSE)
			SET_INTERIOR_DISABLED(INTERIOR_V_APART_MIDSPAZ, FALSE)
			SET_INTERIOR_DISABLED(INTERIOR_V_GARAGEM_SP, FALSE)
			SET_INTERIOR_DISABLED(INTERIOR_V_BAHAMA, FALSE)
		BREAK
	
		CASE SPCID_BUILDING_TOGGLE_SWEAT1
			// Sweatshop normal
			SET_BUILDING_STATE(BUILDINGNAME_IPL_SWEATSHOP_NO_INTERIOR, BUILDINGSTATE_DESTROYED)
			SET_BUILDING_STATE(BUILDINGNAME_IPL_SWEATSHOP_WITH_INTERIOR, BUILDINGSTATE_DESTROYED) 
			SET_BUILDING_STATE(BUILDINGNAME_IPL_SWEATSHOP_LOD_DOOR, BUILDINGSTATE_DESTROYED) 
			SET_BUILDING_STATE(BUILDINGNAME_IPL_SWEATSHOP_BURNT, BUILDINGSTATE_NORMAL) 
			SET_BUILDING_STATE(BUILDINGNAME_IPL_SWEATSHOP_WINDOW_LIGHTS, BUILDINGSTATE_NORMAL)
			
			SET_DOOR_STATE(DOORNAME_SWEATSHOP_L, DOORSTATE_UNLOCKED)
			SET_DOOR_STATE(DOORNAME_SWEATSHOP_R, DOORSTATE_UNLOCKED)
			SET_DOOR_STATE(DOORNAME_SWEATSHOP_OFFICE, DOORSTATE_UNLOCKED)
			SET_DOOR_STATE(DOORNAME_SWEATSHOP_OFFICE_STORE, DOORSTATE_UNLOCKED)
			SET_DOOR_STATE(DOORNAME_SWEATSHOP_STORE, DOORSTATE_UNLOCKED)
			
			SET_INTERIOR_CAPPED(INTERIOR_V_SWEAT, FALSE)
			SET_INTERIOR_CAPPED(INTERIOR_V_SWEATEMPTY, TRUE)
			
			SET_INTERIOR_DISABLED(INTERIOR_V_SWEAT, FALSE)
			SET_INTERIOR_DISABLED(INTERIOR_V_SWEATEMPTY, TRUE)
		BREAK
		CASE SPCID_BUILDING_TOGGLE_SWEAT2
			// Sweatshop empty
			SET_BUILDING_STATE(BUILDINGNAME_IPL_SWEATSHOP_NO_INTERIOR, BUILDINGSTATE_DESTROYED)
			SET_BUILDING_STATE(BUILDINGNAME_IPL_SWEATSHOP_WITH_INTERIOR, BUILDINGSTATE_CLEANUP) 
			SET_BUILDING_STATE(BUILDINGNAME_IPL_SWEATSHOP_LOD_DOOR, BUILDINGSTATE_DESTROYED) 
			SET_BUILDING_STATE(BUILDINGNAME_IPL_SWEATSHOP_BURNT, BUILDINGSTATE_NORMAL) 
			SET_BUILDING_STATE(BUILDINGNAME_IPL_SWEATSHOP_WINDOW_LIGHTS, BUILDINGSTATE_NORMAL) 
			SET_DOOR_STATE(DOORNAME_SWEATSHOP_L, DOORSTATE_UNLOCKED)
			SET_DOOR_STATE(DOORNAME_SWEATSHOP_R, DOORSTATE_UNLOCKED)
			SET_DOOR_STATE(DOORNAME_SWEATSHOP_OFFICE, DOORSTATE_UNLOCKED)
			SET_DOOR_STATE(DOORNAME_SWEATSHOP_OFFICE_STORE, DOORSTATE_UNLOCKED)
			SET_DOOR_STATE(DOORNAME_SWEATSHOP_STORE, DOORSTATE_UNLOCKED)
			SET_INTERIOR_CAPPED(INTERIOR_V_SWEAT, TRUE)
			SET_INTERIOR_CAPPED(INTERIOR_V_SWEATEMPTY, FALSE)
			SET_INTERIOR_DISABLED(INTERIOR_V_SWEAT, TRUE)
			SET_INTERIOR_DISABLED(INTERIOR_V_SWEATEMPTY, FALSE)
		BREAK
		CASE SPCID_BUILDING_TOGGLE_SWEAT3
			// Sweatshop burnt exterior, no interior
			SET_BUILDING_STATE(BUILDINGNAME_IPL_SWEATSHOP_NO_INTERIOR, BUILDINGSTATE_NORMAL )
			SET_BUILDING_STATE(BUILDINGNAME_IPL_SWEATSHOP_WITH_INTERIOR, BUILDINGSTATE_NORMAL ) 
			SET_BUILDING_STATE(BUILDINGNAME_IPL_SWEATSHOP_LOD_DOOR, BUILDINGSTATE_NORMAL ) 
			SET_BUILDING_STATE(BUILDINGNAME_IPL_SWEATSHOP_BURNT, BUILDINGSTATE_CLEANUP ) 
			SET_BUILDING_STATE(BUILDINGNAME_IPL_SWEATSHOP_WINDOW_LIGHTS, BUILDINGSTATE_DESTROYED )
			SET_DOOR_STATE(DOORNAME_SWEATSHOP_L, DOORSTATE_UNLOCKED)
			SET_DOOR_STATE(DOORNAME_SWEATSHOP_R, DOORSTATE_UNLOCKED)
			SET_DOOR_STATE(DOORNAME_SWEATSHOP_OFFICE, DOORSTATE_UNLOCKED)
			SET_DOOR_STATE(DOORNAME_SWEATSHOP_OFFICE_STORE, DOORSTATE_UNLOCKED)
			SET_DOOR_STATE(DOORNAME_SWEATSHOP_STORE, DOORSTATE_UNLOCKED)
			SET_INTERIOR_CAPPED(INTERIOR_V_SWEAT, TRUE)
			SET_INTERIOR_CAPPED(INTERIOR_V_SWEATEMPTY, TRUE)
			SET_INTERIOR_DISABLED(INTERIOR_V_SWEAT, TRUE)
			SET_INTERIOR_DISABLED(INTERIOR_V_SWEATEMPTY, TRUE)
		BREAK
		
		CASE SPCID_BUILDING_TOGGLE_LIFE1
			// Lifeinvader normal
			SET_DOOR_STATE(DOORNAME_LIFE_INVADER_FRONT_L, DOORSTATE_UNLOCKED)
			SET_DOOR_STATE(DOORNAME_LIFE_INVADER_FRONT_R, DOORSTATE_UNLOCKED)
			SET_DOOR_STATE(DOORNAME_LIFE_INVADER_REAR_L, DOORSTATE_UNLOCKED)
			SET_DOOR_STATE(DOORNAME_LIFE_INVADER_REAR_R, DOORSTATE_UNLOCKED)
			SET_DOOR_STATE(DOORNAME_LIFE_INVADER_SIDE_01, DOORSTATE_UNLOCKED)
			SET_BUILDING_STATE(BUILDINGNAME_IPL_INVADER_OFFICE_INTERIOR, BUILDINGSTATE_DESTROYED)
		BREAK
		
		CASE SPCID_BUILDING_TOGGLE_HOSP1
			// default
			SET_BUILDING_STATE(BUILDINGNAME_IPL_PILLBOX_HILL, BUILDINGSTATE_NORMAL)
			SET_BUILDING_STATE(BUILDINGNAME_IPL_PILLBOX_HILL_INTERIOR, BUILDINGSTATE_NORMAL)
			SET_BUILDING_STATE(BUILDINGNAME_ES_PILLBOX_HILL, BUILDINGSTATE_NORMAL)
		BREAK
		CASE SPCID_BUILDING_TOGGLE_HOSP2
			// destroyed
			SET_BUILDING_STATE(BUILDINGNAME_IPL_PILLBOX_HILL, BUILDINGSTATE_DESTROYED)
			SET_BUILDING_STATE(BUILDINGNAME_IPL_PILLBOX_HILL_INTERIOR, BUILDINGSTATE_NORMAL)
			SET_BUILDING_STATE(BUILDINGNAME_ES_PILLBOX_HILL, BUILDINGSTATE_DESTROYED)
		BREAK
		CASE SPCID_BUILDING_TOGGLE_HOSP3
			// fixed
			SET_BUILDING_STATE(BUILDINGNAME_IPL_PILLBOX_HILL, BUILDINGSTATE_CLEANUP)
			SET_BUILDING_STATE(BUILDINGNAME_IPL_PILLBOX_HILL_INTERIOR, BUILDINGSTATE_DESTROYED)
			SET_BUILDING_STATE(BUILDINGNAME_ES_PILLBOX_HILL, BUILDINGSTATE_CLEANUP)
		BREAK
		
		CASE SPCID_BUILDING_TOGGLE_STADIUM
			// normal
			SET_BUILDING_STATE(BUILDINGNAME_IPL_STADIUM_INTERIOR, BUILDINGSTATE_DESTROYED)
		BREAK
		
		CASE SPCID_BUILDING_TOGGLE_JEWEL1
			SET_BUILDING_STATE(BUILDINGNAME_IPL_JEWEL_STORE, BUILDINGSTATE_NORMAL)
			SET_BUILDING_STATE(BUILDINGNAME_IPL_JEWEL_HEIST_MAX_RENDA_INTERIOR, BUILDINGSTATE_NORMAL)
			SET_DOOR_STATE(DOORNAME_HEIST_JEWELERS_L, DOORSTATE_UNLOCKED)
			SET_DOOR_STATE(DOORNAME_HEIST_JEWELERS_R, DOORSTATE_UNLOCKED)
		BREAK
		CASE SPCID_BUILDING_TOGGLE_JEWEL2
			SET_BUILDING_STATE(BUILDINGNAME_IPL_JEWEL_STORE, BUILDINGSTATE_DESTROYED)
			SET_BUILDING_STATE(BUILDINGNAME_IPL_JEWEL_HEIST_MAX_RENDA_INTERIOR, BUILDINGSTATE_DESTROYED)
			SET_DOOR_STATE(DOORNAME_HEIST_JEWELERS_L, DOORSTATE_UNLOCKED)
			SET_DOOR_STATE(DOORNAME_HEIST_JEWELERS_R, DOORSTATE_UNLOCKED)
		BREAK
		CASE SPCID_BUILDING_TOGGLE_JEWEL3
			SET_BUILDING_STATE(BUILDINGNAME_IPL_JEWEL_STORE, BUILDINGSTATE_CLEANUP)
			SET_BUILDING_STATE(BUILDINGNAME_IPL_JEWEL_HEIST_MAX_RENDA_INTERIOR, BUILDINGSTATE_NORMAL)
			SET_DOOR_STATE(DOORNAME_HEIST_JEWELERS_L, DOORSTATE_LOCKED)
			SET_DOOR_STATE(DOORNAME_HEIST_JEWELERS_R, DOORSTATE_LOCKED)
		BREAK
		
		CASE SPCID_BUILDING_TOGGLE_FINALE1
			SET_BUILDING_STATE(BUILDINGNAME_IPL_BIG_SCORE_BANK, BUILDINGSTATE_DESTROYED)
			SET_BUILDING_STATE(BUILDINGNAME_IPL_JEWEL_HEIST_BIKE_TUNNELS, BUILDINGSTATE_DESTROYED) 
			SET_INTERIOR_CAPPED(INTERIOR_DT1_03_CARPARK, FALSE)
			SET_INTERIOR_DISABLED(INTERIOR_DT1_03_CARPARK, FALSE)
		BREAK
		
		CASE SPCID_BUILDING_TOGGLE_FINALE2
			SET_BUILDING_STATE(BUILDINGNAME_IPL_BIG_SCORE_BANK, BUILDINGSTATE_DESTROYED)
			SET_BUILDING_STATE(BUILDINGNAME_IPL_JEWEL_HEIST_BIKE_TUNNELS, BUILDINGSTATE_DESTROYED) 
			SET_INTERIOR_CAPPED(INTERIOR_DT1_03_CARPARK, FALSE)
			SET_INTERIOR_DISABLED(INTERIOR_DT1_03_CARPARK, FALSE)
			SET_BUILDING_STATE(BUILDINGNAME_IPL_BIG_SCORE_HOLE_COVER, BUILDINGSTATE_DESTROYED)
		BREAK
		CASE SPCID_BUILDING_TOGGLE_FINALE3
			SET_BUILDING_STATE(BUILDINGNAME_IPL_BIG_SCORE_BANK, BUILDINGSTATE_DESTROYED)
			SET_BUILDING_STATE(BUILDINGNAME_IPL_JEWEL_HEIST_BIKE_TUNNELS, BUILDINGSTATE_DESTROYED) 
			SET_INTERIOR_CAPPED(INTERIOR_DT1_03_CARPARK, FALSE)
			SET_INTERIOR_DISABLED(INTERIOR_DT1_03_CARPARK, FALSE)
			SET_BUILDING_STATE(BUILDINGNAME_IPL_BIG_SCORE_HOLE_COVER, BUILDINGSTATE_NORMAL)
		BREAK
		
		CASE SPCID_BUILDING_TOGGLE_TREV1
			SET_BUILDING_STATE(BUILDINGNAME_IPL_TREVORS_TRAILER, BUILDINGSTATE_NORMAL)
			
			SET_BUILDING_STATE(BUILDINGNAME_ES_TREVORS_TRAILER_NORMAL_COP_UNIFORM, BUILDINGSTATE_NORMAL)
			SET_BUILDING_STATE(BUILDINGNAME_ES_TREVORS_TRAILER_TRASH_COP_UNIFORM, BUILDINGSTATE_NORMAL)
			SET_BUILDING_STATE(BUILDINGNAME_ES_TREVORS_TRAILER_TIDY_COP_UNIFORM, BUILDINGSTATE_NORMAL)
			SET_BUILDING_STATE(BUILDINGNAME_ES_TREVORS_TRAILER_NORMAL_BRIEFCASE, BUILDINGSTATE_NORMAL)
			SET_BUILDING_STATE(BUILDINGNAME_ES_TREVORS_TRAILER_TRASH_BRIEFCASE, BUILDINGSTATE_NORMAL)
			SET_BUILDING_STATE(BUILDINGNAME_ES_TREVORS_TRAILER_TIDY_BRIEFCASE, BUILDINGSTATE_NORMAL)
			SET_BUILDING_STATE(BUILDINGNAME_ES_TREVORS_TRAILER_NORMAL_MICHAEL_STAY, BUILDINGSTATE_NORMAL)
			SET_BUILDING_STATE(BUILDINGNAME_ES_TREVORS_TRAILER_TRASH_MICHAEL_STAY, BUILDINGSTATE_NORMAL)
			SET_BUILDING_STATE(BUILDINGNAME_ES_TREVORS_TRAILER_TIDY_MICHAEL_STAY, BUILDINGSTATE_NORMAL)
		BREAK
		CASE SPCID_BUILDING_TOGGLE_TREV2
			SET_BUILDING_STATE(BUILDINGNAME_IPL_TREVORS_TRAILER, BUILDINGSTATE_DESTROYED)
			
			SET_BUILDING_STATE(BUILDINGNAME_ES_TREVORS_TRAILER_NORMAL_COP_UNIFORM, BUILDINGSTATE_NORMAL)
			SET_BUILDING_STATE(BUILDINGNAME_ES_TREVORS_TRAILER_TRASH_COP_UNIFORM, BUILDINGSTATE_NORMAL)
			SET_BUILDING_STATE(BUILDINGNAME_ES_TREVORS_TRAILER_TIDY_COP_UNIFORM, BUILDINGSTATE_NORMAL)
			SET_BUILDING_STATE(BUILDINGNAME_ES_TREVORS_TRAILER_NORMAL_BRIEFCASE, BUILDINGSTATE_NORMAL)
			SET_BUILDING_STATE(BUILDINGNAME_ES_TREVORS_TRAILER_TRASH_BRIEFCASE, BUILDINGSTATE_NORMAL)
			SET_BUILDING_STATE(BUILDINGNAME_ES_TREVORS_TRAILER_TIDY_BRIEFCASE, BUILDINGSTATE_NORMAL)
			SET_BUILDING_STATE(BUILDINGNAME_ES_TREVORS_TRAILER_NORMAL_MICHAEL_STAY, BUILDINGSTATE_NORMAL)
			SET_BUILDING_STATE(BUILDINGNAME_ES_TREVORS_TRAILER_TRASH_MICHAEL_STAY, BUILDINGSTATE_NORMAL)
			SET_BUILDING_STATE(BUILDINGNAME_ES_TREVORS_TRAILER_TIDY_MICHAEL_STAY, BUILDINGSTATE_NORMAL)
		BREAK
		CASE SPCID_BUILDING_TOGGLE_TREV3
			SET_BUILDING_STATE(BUILDINGNAME_IPL_TREVORS_TRAILER, BUILDINGSTATE_CLEANUP)
			
			SET_BUILDING_STATE(BUILDINGNAME_ES_TREVORS_TRAILER_NORMAL_COP_UNIFORM, BUILDINGSTATE_NORMAL)
			SET_BUILDING_STATE(BUILDINGNAME_ES_TREVORS_TRAILER_TRASH_COP_UNIFORM, BUILDINGSTATE_NORMAL)
			SET_BUILDING_STATE(BUILDINGNAME_ES_TREVORS_TRAILER_TIDY_COP_UNIFORM, BUILDINGSTATE_NORMAL)
			SET_BUILDING_STATE(BUILDINGNAME_ES_TREVORS_TRAILER_NORMAL_BRIEFCASE, BUILDINGSTATE_NORMAL)
			SET_BUILDING_STATE(BUILDINGNAME_ES_TREVORS_TRAILER_TRASH_BRIEFCASE, BUILDINGSTATE_NORMAL)
			SET_BUILDING_STATE(BUILDINGNAME_ES_TREVORS_TRAILER_TIDY_BRIEFCASE, BUILDINGSTATE_NORMAL)
			SET_BUILDING_STATE(BUILDINGNAME_ES_TREVORS_TRAILER_NORMAL_MICHAEL_STAY, BUILDINGSTATE_NORMAL)
			SET_BUILDING_STATE(BUILDINGNAME_ES_TREVORS_TRAILER_TRASH_MICHAEL_STAY, BUILDINGSTATE_NORMAL)
			SET_BUILDING_STATE(BUILDINGNAME_ES_TREVORS_TRAILER_TIDY_MICHAEL_STAY, BUILDINGSTATE_NORMAL)
		BREAK
		CASE SPCID_BUILDING_TOGGLE_TREV4
			// Cop uniform
			IF GET_BUILDING_STATE(BUILDINGNAME_IPL_TREVORS_TRAILER) = BUILDINGSTATE_NORMAL
				SET_BUILDING_STATE(BUILDINGNAME_ES_TREVORS_TRAILER_NORMAL_COP_UNIFORM, BUILDINGSTATE_DESTROYED)
				SET_BUILDING_STATE(BUILDINGNAME_ES_TREVORS_TRAILER_TRASH_COP_UNIFORM, BUILDINGSTATE_NORMAL)
				SET_BUILDING_STATE(BUILDINGNAME_ES_TREVORS_TRAILER_TIDY_COP_UNIFORM, BUILDINGSTATE_NORMAL)
			ELIF GET_BUILDING_STATE(BUILDINGNAME_IPL_TREVORS_TRAILER) = BUILDINGSTATE_DESTROYED
				SET_BUILDING_STATE(BUILDINGNAME_ES_TREVORS_TRAILER_NORMAL_COP_UNIFORM, BUILDINGSTATE_NORMAL)
				SET_BUILDING_STATE(BUILDINGNAME_ES_TREVORS_TRAILER_TRASH_COP_UNIFORM, BUILDINGSTATE_DESTROYED)
				SET_BUILDING_STATE(BUILDINGNAME_ES_TREVORS_TRAILER_TIDY_COP_UNIFORM, BUILDINGSTATE_NORMAL)
			ELSE
				SET_BUILDING_STATE(BUILDINGNAME_ES_TREVORS_TRAILER_NORMAL_COP_UNIFORM, BUILDINGSTATE_NORMAL)
				SET_BUILDING_STATE(BUILDINGNAME_ES_TREVORS_TRAILER_TRASH_COP_UNIFORM, BUILDINGSTATE_NORMAL)
				SET_BUILDING_STATE(BUILDINGNAME_ES_TREVORS_TRAILER_TIDY_COP_UNIFORM, BUILDINGSTATE_DESTROYED)
			ENDIF
		BREAK
		CASE SPCID_BUILDING_TOGGLE_TREV5
			// Briefcase
			IF GET_BUILDING_STATE(BUILDINGNAME_IPL_TREVORS_TRAILER) = BUILDINGSTATE_NORMAL
				SET_BUILDING_STATE(BUILDINGNAME_ES_TREVORS_TRAILER_NORMAL_BRIEFCASE, BUILDINGSTATE_DESTROYED)
				SET_BUILDING_STATE(BUILDINGNAME_ES_TREVORS_TRAILER_TRASH_BRIEFCASE, BUILDINGSTATE_NORMAL)
				SET_BUILDING_STATE(BUILDINGNAME_ES_TREVORS_TRAILER_TIDY_BRIEFCASE, BUILDINGSTATE_NORMAL)
			ELIF GET_BUILDING_STATE(BUILDINGNAME_IPL_TREVORS_TRAILER) = BUILDINGSTATE_DESTROYED
				SET_BUILDING_STATE(BUILDINGNAME_ES_TREVORS_TRAILER_NORMAL_BRIEFCASE, BUILDINGSTATE_NORMAL)
				SET_BUILDING_STATE(BUILDINGNAME_ES_TREVORS_TRAILER_TRASH_BRIEFCASE, BUILDINGSTATE_DESTROYED)
				SET_BUILDING_STATE(BUILDINGNAME_ES_TREVORS_TRAILER_TIDY_BRIEFCASE, BUILDINGSTATE_NORMAL)
			ELSE
				SET_BUILDING_STATE(BUILDINGNAME_ES_TREVORS_TRAILER_NORMAL_BRIEFCASE, BUILDINGSTATE_NORMAL)
				SET_BUILDING_STATE(BUILDINGNAME_ES_TREVORS_TRAILER_TRASH_BRIEFCASE, BUILDINGSTATE_NORMAL)
				SET_BUILDING_STATE(BUILDINGNAME_ES_TREVORS_TRAILER_TIDY_BRIEFCASE, BUILDINGSTATE_DESTROYED)
			ENDIF
		BREAK
		CASE SPCID_BUILDING_TOGGLE_TREV6
			// Michael stay
			IF GET_BUILDING_STATE(BUILDINGNAME_IPL_TREVORS_TRAILER) = BUILDINGSTATE_NORMAL
				SET_BUILDING_STATE(BUILDINGNAME_ES_TREVORS_TRAILER_NORMAL_MICHAEL_STAY, BUILDINGSTATE_DESTROYED)
				SET_BUILDING_STATE(BUILDINGNAME_ES_TREVORS_TRAILER_TRASH_MICHAEL_STAY, BUILDINGSTATE_NORMAL)
				SET_BUILDING_STATE(BUILDINGNAME_ES_TREVORS_TRAILER_TIDY_MICHAEL_STAY, BUILDINGSTATE_NORMAL)
			ELIF GET_BUILDING_STATE(BUILDINGNAME_IPL_TREVORS_TRAILER) = BUILDINGSTATE_DESTROYED
				SET_BUILDING_STATE(BUILDINGNAME_ES_TREVORS_TRAILER_NORMAL_MICHAEL_STAY, BUILDINGSTATE_NORMAL)
				SET_BUILDING_STATE(BUILDINGNAME_ES_TREVORS_TRAILER_TRASH_MICHAEL_STAY, BUILDINGSTATE_DESTROYED)
				SET_BUILDING_STATE(BUILDINGNAME_ES_TREVORS_TRAILER_TIDY_MICHAEL_STAY, BUILDINGSTATE_NORMAL)
			ELSE
				SET_BUILDING_STATE(BUILDINGNAME_ES_TREVORS_TRAILER_NORMAL_MICHAEL_STAY, BUILDINGSTATE_NORMAL)
				SET_BUILDING_STATE(BUILDINGNAME_ES_TREVORS_TRAILER_TRASH_MICHAEL_STAY, BUILDINGSTATE_NORMAL)
				SET_BUILDING_STATE(BUILDINGNAME_ES_TREVORS_TRAILER_TIDY_MICHAEL_STAY, BUILDINGSTATE_DESTROYED)
			ENDIF
		BREAK
			
		CASE SPCID_BUILDING_TOGGLE_FBI1
			SET_BUILDING_STATE(BUILDINGNAME_IPL_FIB_C4, BUILDINGSTATE_DESTROYED)
			
			ACTIVATE_INTERIOR_ENTITY_SET(GET_INTERIOR_AT_COORDS_with_type(<<155.4538, -760.7770, 245.1522>>, "V_FIB02"), "V_FIB02_set_AH3a")
			ACTIVATE_INTERIOR_ENTITY_SET(GET_INTERIOR_AT_COORDS_with_type(<<153.5201, -765.0731, 257.1523>>, "V_FIB03"), "V_FIB03_set_AH3a")
			ACTIVATE_INTERIOR_ENTITY_SET(GET_INTERIOR_AT_COORDS_with_type(<<128.0967, -737.0934, 235.1497>>, "V_FIB04"), "V_FIB04_set_AH3a")
		BREAK
		CASE SPCID_BUILDING_TOGGLE_FBI2
		BREAK
		CASE SPCID_BUILDING_TOGGLE_FBI3
		BREAK

		CASE SPCID_GENERATE_RICH_PRESENCE
			DEBUG_EXPORT_SP_RICH_PRESENCE_ORDER()
		BREAK
		
		CASE SPCID_GENERATE_FLOW_CO_STAT_XML
			DEBUG_EXPORT_SP_MISSION_COMPLETION_ORDER_STATS()
		BREAK
		
		CASE SPCID_BANK_DISPLAY
			//g_bDebugFinanceDisplay = TRUE
			//TODO trigger ATM
		BREAK
		
		CASE SPCID_EMAIL_DISPLAY
			g_bDebugEmailDisplay = TRUE
		BREAK
		
		CASE SPCID_RANDOM_EVENT
			SP_Perform_Any_Special_Random_Event_Code(sDebugmenuData)
		BREAK
		
		CASE SPCID_RANDOM_CHAR
			SP_PERFORM_ANY_SPECIAL_RANDOM_CHARACTER_CODE()
		BREAK
		
		CASE SPCID_RANDOM_CHAR_PRE_LAUNCH
			SP_PERFORM_RANDOM_CHARACTER_PRE_LAUNCH_SETUP()
		BREAK
		
		CASE SPCID_EXTREME2_ACQUIRE_OUTFIT
			Execute_Code_ID(CID_EXTREME2_ACQUIRE_OUTFIT) // unlock skydiving outfit
			SP_PERFORM_ANY_SPECIAL_RANDOM_CHARACTER_CODE() // default RC mission checks
		BREAK
		
		CASE SPCID_DREYFUSS1_COMPLETE_LETTER_SCRAPS
			Execute_Code_ID(CID_DREYFUSS1_COMPLETE_LETTER_SCRAPS) // set flow flag
			SP_PERFORM_ANY_SPECIAL_RANDOM_CHARACTER_CODE() // default RC mission checks
		BREAK
		
		CASE SPCID_JOSH1_SIGNS_DESTROYED
			Execute_Code_ID(CID_JOSH1_SIGNS_DESTROYED) // set flow flag
			SP_PERFORM_ANY_SPECIAL_RANDOM_CHARACTER_CODE() // default RC mission checks
		BREAK

		CASE SPCID_OMEGA1_COMPLETE_SPACESHIP_PARTS
			Execute_Code_ID(CID_OMEGA1_COMPLETE_SPACESHIP_PARTS) // set flow flag
			SP_PERFORM_ANY_SPECIAL_RANDOM_CHARACTER_CODE() // default RC mission checks
		BREAK
		
		CASE SPCID_SUPPRESS_MILITARY_WANTED
			g_bMilitaryWantedLevelSuppressedByMenu = TRUE
		BREAK
		
		CASE SPCID_ACTIVITY_COMEDY_CLUB
			g_bActivityLaunchedFromDebugMenu = TRUE
			Set_Mission_Flow_Flag_State(FLOWFLAG_ALLOW_COMEDYCLUB_ACTIVITY, TRUE)
			SP_Perform_Any_Special_Random_Event_Code(sDebugmenuData)
		BREAK
		
		CASE SPCID_ACTIVITY_LIVE_MUSIC
			g_bActivityLaunchedFromDebugMenu = TRUE
			Set_Mission_Flow_Flag_State(FLOWFLAG_ALLOW_LIVEMUSIC_ACTIVITY, TRUE)
			SP_Perform_Any_Special_Random_Event_Code(sDebugmenuData)
		BREAK
		
		CASE SPCID_ACTIVITY_CINEMA
			g_bActivityLaunchedFromDebugMenu = TRUE
			Set_Mission_Flow_Flag_State(FLOWFLAG_ALLOW_CINEMA_ACTIVITY, TRUE)
			SP_Perform_Any_Special_Random_Event_Code(sDebugmenuData)
		BREAK
		
		CASE SPCID_ACTIVITY_SAFEHOUSE
			
			// Sober up player
			IF IS_PLAYER_PLAYING(PLAYER_ID())
			
				SET_PED_MOTION_BLUR(PLAYER_PED_ID(), FALSE)
				IF Is_Ped_Drunk(PLAYER_PED_ID())
					Make_Ped_Sober(PLAYER_PED_ID())
				ENDIF
			ENDIF
			
			// Exit drunk cam
			Quit_Drunk_Camera_Immediately()
			
			// Ensure sound fx are normal
			SET_AUDIO_SPECIAL_EFFECT_MODE(AUDIO_SPECIAL_EFFECT_MODE_NORMAL)
		BREAK
		
		CASE SPCID_ACTIVATE_CARWASHES
			REACTIVATE_ALL_WORLD_BRAINS_THAT_ARE_WAITING_TILL_OUT_OF_RANGE()
			Execute_Code_ID(CID_ACTIVATE_CARWASHES)
		BREAK
		
		CASE SPCID_ACTIVATE_FAIRGROUND
			REACTIVATE_ALL_WORLD_BRAINS_THAT_ARE_WAITING_TILL_OUT_OF_RANGE() 
			Execute_Code_ID(CID_ACTIVATE_FAIRGROUND)
		BREAK		
		
		CASE SPCID_UNLOCK_SUBMERSIBLE
			Execute_Code_ID(CID_UNLOCK_SUBMERSIBLE)
		BREAK
		
		CASE SPCID_ENDGAME_TREVOR_DEAD	
			SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_TREVOR_KILLED, TRUE)
			SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_MICHAEL_KILLED, FALSE)
			SET_MISSION_COMPLETE_STATE(SP_MISSION_FINALE_A, TRUE)
			SET_MISSION_COMPLETE_STATE(SP_MISSION_FINALE_B, FALSE)
			SET_MISSION_COMPLETE_STATE(SP_MISSION_FINALE_C1, FALSE)
			SET_MISSION_COMPLETE_STATE(SP_MISSION_FINALE_C2, FALSE)
			SET_PLAYER_PED_AVAILABLE(CHAR_TREVOR, FALSE)
			SET_PLAYER_PED_AVAILABLE(CHAR_MICHAEL, TRUE)
			SET_FRIEND_BLOCK_FLAG(CHAR_MICHAEL, 	CHAR_TREVOR, 	FRIEND_BLOCK_FLAG_HIATUS)
			SET_FRIEND_BLOCK_FLAG(CHAR_FRANKLIN, 	CHAR_TREVOR, 	FRIEND_BLOCK_FLAG_HIATUS)
			SET_FRIEND_BLOCK_FLAG(CHAR_LAMAR, 		CHAR_TREVOR, 	FRIEND_BLOCK_FLAG_HIATUS)
			SET_FRIEND_BLOCK_FLAG(CHAR_JIMMY, 		CHAR_TREVOR, 	FRIEND_BLOCK_FLAG_HIATUS)
			SET_FRIEND_BLOCK_FLAG(CHAR_MICHAEL, 	CHAR_FRANKLIN, 	FRIEND_BLOCK_FLAG_HIATUS)
		BREAK
		
		CASE SPCID_ENDGAME_MICHAEL_DEAD	
			SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_TREVOR_KILLED, FALSE)
			SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_MICHAEL_KILLED, TRUE)
			SET_MISSION_COMPLETE_STATE(SP_MISSION_FINALE_A, FALSE)
			SET_MISSION_COMPLETE_STATE(SP_MISSION_FINALE_B, FALSE)
			SET_MISSION_COMPLETE_STATE(SP_MISSION_FINALE_C1, TRUE)
			SET_MISSION_COMPLETE_STATE(SP_MISSION_FINALE_C2, TRUE)
			SET_PLAYER_PED_AVAILABLE(CHAR_TREVOR, TRUE)
			SET_PLAYER_PED_AVAILABLE(CHAR_MICHAEL, FALSE)
			SET_FRIEND_BLOCK_FLAG(CHAR_FRANKLIN,	CHAR_MICHAEL,	FRIEND_BLOCK_FLAG_HIATUS)
			SET_FRIEND_BLOCK_FLAG(CHAR_TREVOR,		CHAR_MICHAEL,	FRIEND_BLOCK_FLAG_HIATUS)
			SET_FRIEND_BLOCK_FLAG(CHAR_JIMMY,		CHAR_MICHAEL,	FRIEND_BLOCK_FLAG_HIATUS)
			SET_FRIEND_BLOCK_FLAG(CHAR_AMANDA,		CHAR_MICHAEL,	FRIEND_BLOCK_FLAG_HIATUS)
			SET_FRIEND_BLOCK_FLAG(CHAR_TREVOR,		CHAR_FRANKLIN,	FRIEND_BLOCK_FLAG_HIATUS)
			SET_FRIEND_BLOCK_FLAG(CHAR_JIMMY,		CHAR_FRANKLIN,	FRIEND_BLOCK_FLAG_HIATUS)
			SET_FRIEND_BLOCK_FLAG(CHAR_JIMMY,		CHAR_TREVOR,	FRIEND_BLOCK_FLAG_HIATUS)
			SET_BUILDING_STATE(BUILDINGNAME_IPL_KILLED_MICHAEL, BUILDINGSTATE_DESTROYED)
		BREAK
			
		CASE SPCID_ENDGAME_T_AND_M_ALIVE	
			SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_TREVOR_KILLED, TRUE)
			SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_MICHAEL_KILLED, TRUE)
			SET_PLAYER_PED_AVAILABLE(CHAR_TREVOR, TRUE)
			SET_PLAYER_PED_AVAILABLE(CHAR_MICHAEL, TRUE)
			CLEAR_FRIEND_BLOCK_FLAG(CHAR_MICHAEL, CHAR_TREVOR, FRIEND_BLOCK_FLAG_HIATUS)
		BREAK
			
		CASE SPCID_ACTIVITY_STRIPCLUB
			REACTIVATE_ALL_WORLD_BRAINS_THAT_ARE_WAITING_TILL_OUT_OF_RANGE() //incase player is in stripclub activation range when he debug unlocks club
			SET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_MINIGAME_ACTIVE, ENUM_TO_INT(MINIGAME_STRIPCLUB), TRUE)
			SET_DOOR_STATE(DOORNAME_STRIPCLUB_F, DOORSTATE_UNLOCKED)
			SET_DOOR_STATE(DOORNAME_STRIPCLUB_R, DOORSTATE_UNLOCKED)
		BREAK
		
		CASE SPCID_REVISIT_POSITION
			VECTOR vCoords
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())	
				vCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
				SET_ENTITY_COORDS(PLAYER_PED_ID(), vCoords+<<500.0, 500.0, 500.0>>)
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
				SET_ENTITY_COLLISION(PLAYER_PED_ID(), FALSE)
				SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), TRUE)
				WAIT(0)
				WAIT(0)
				WAIT(0)
				WAIT(0)
				WAIT(0)
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					SET_ENTITY_COORDS_NO_OFFSET(PLAYER_PED_ID(), vCoords)
					FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
					SET_ENTITY_COLLISION(PLAYER_PED_ID(), TRUE)
					SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), FALSE)
				ENDIF
			ENDIF
		BREAK
		
		CASE SPCID_FRIADD_MICHAEL
			PRINTSTRING("add michael as friend")PRINTNL()
			Add_Char_As_DefaultPlayer_Friend(CHAR_MICHAEL)
		BREAK
		
		CASE SPCID_FRIBLK_MICHAEL
			IF NOT IS_FRIEND_BLOCK_FLAG_SET(GET_CURRENT_PLAYER_PED_ENUM(), CHAR_MICHAEL, FRIEND_BLOCK_FLAG_HIATUS)
				PRINTSTRING("block michael as friend")PRINTNL()
				SET_FRIEND_BLOCK_FLAG(GET_CURRENT_PLAYER_PED_ENUM(), CHAR_MICHAEL, FRIEND_BLOCK_FLAG_HIATUS)
			ELSE
				PRINTSTRING("unblock michael as friend")PRINTNL()
				CLEAR_FRIEND_BLOCK_FLAG(GET_CURRENT_PLAYER_PED_ENUM(), CHAR_MICHAEL, FRIEND_BLOCK_FLAG_HIATUS)
				CLEAR_FRIEND_BLOCK_FLAG(GET_CURRENT_PLAYER_PED_ENUM(), CHAR_MICHAEL, FRIEND_BLOCK_FLAG_MISSION)
			ENDIF
		BREAK
		
		CASE SPCID_FRIADD_TREVOR
			PRINTSTRING("add trevor as friend")PRINTNL()
			Add_Char_As_DefaultPlayer_Friend(CHAR_TREVOR)
		BREAK
		
		CASE SPCID_FRIBLK_TREVOR
			IF NOT IS_FRIEND_BLOCK_FLAG_SET(GET_CURRENT_PLAYER_PED_ENUM(), CHAR_TREVOR, FRIEND_BLOCK_FLAG_HIATUS)
				PRINTSTRING("block trevor as friend")PRINTNL()
				SET_FRIEND_BLOCK_FLAG(GET_CURRENT_PLAYER_PED_ENUM(), CHAR_TREVOR, FRIEND_BLOCK_FLAG_HIATUS)
			ELSE
				PRINTSTRING("unblock trevor as friend")PRINTNL()
				CLEAR_FRIEND_BLOCK_FLAG(GET_CURRENT_PLAYER_PED_ENUM(), CHAR_TREVOR, FRIEND_BLOCK_FLAG_HIATUS)
				CLEAR_FRIEND_BLOCK_FLAG(GET_CURRENT_PLAYER_PED_ENUM(), CHAR_TREVOR, FRIEND_BLOCK_FLAG_MISSION)
			ENDIF
		BREAK
		
		CASE SPCID_FRIADD_FRANKLIN
			PRINTSTRING("add franklin as friend")PRINTNL()
			Add_Char_As_DefaultPlayer_Friend(CHAR_FRANKLIN)
		BREAK
		
		CASE SPCID_FRIBLK_FRANKLIN
			IF NOT IS_FRIEND_BLOCK_FLAG_SET(GET_CURRENT_PLAYER_PED_ENUM(), CHAR_FRANKLIN, FRIEND_BLOCK_FLAG_HIATUS)
				PRINTSTRING("block franklin as friend")PRINTNL()
				SET_FRIEND_BLOCK_FLAG(GET_CURRENT_PLAYER_PED_ENUM(), CHAR_FRANKLIN, FRIEND_BLOCK_FLAG_HIATUS)
			ELSE
				PRINTSTRING("unblock franklin as friend")PRINTNL()
				CLEAR_FRIEND_BLOCK_FLAG(GET_CURRENT_PLAYER_PED_ENUM(), CHAR_FRANKLIN, FRIEND_BLOCK_FLAG_HIATUS)
				CLEAR_FRIEND_BLOCK_FLAG(GET_CURRENT_PLAYER_PED_ENUM(), CHAR_FRANKLIN, FRIEND_BLOCK_FLAG_MISSION)
			ENDIF
		BREAK
		
		CASE SPCID_FRIADD_LAMAR
			PRINTSTRING("add Lamar as friend")PRINTNL()
			Add_Char_As_DefaultPlayer_Friend(CHAR_LAMAR)
		BREAK
		
		CASE SPCID_FRIBLK_LAMAR
			IF NOT IS_FRIEND_BLOCK_FLAG_SET(GET_CURRENT_PLAYER_PED_ENUM(), CHAR_LAMAR, FRIEND_BLOCK_FLAG_HIATUS)
				PRINTSTRING("block Lamar as friend")PRINTNL()
				SET_FRIEND_BLOCK_FLAG(GET_CURRENT_PLAYER_PED_ENUM(), CHAR_LAMAR, FRIEND_BLOCK_FLAG_HIATUS)
			ELSE
				PRINTSTRING("unblock Lamar as friend")PRINTNL()
				CLEAR_FRIEND_BLOCK_FLAG(GET_CURRENT_PLAYER_PED_ENUM(), CHAR_LAMAR, FRIEND_BLOCK_FLAG_HIATUS)
				CLEAR_FRIEND_BLOCK_FLAG(GET_CURRENT_PLAYER_PED_ENUM(), CHAR_LAMAR, FRIEND_BLOCK_FLAG_MISSION)
			ENDIF
		BREAK
		
		CASE SPCID_FRI_AMBLAUNCH
			PRINTSTRING("Ambient launching does not need enabling anymore, it's permanently on")PRINTNL()
		BREAK
		
		CASE SPCID_FRI_FORCEACT
			PRINTSTRING("Force a Friend Activity with anyone")PRINTNL()
			g_bForceFriendActivityWithAnyone = TRUE
		BREAK

		CASE SPCID_FRI_DISPL
			g_iDebugSelectedFriendConnDisplay++
		BREAK
		
		CASE SPCID_POLICE_STATION_VB	g_eDebug_ForcePoliceStationRespawn = POLICE_STATION_VB	g_bDebug_KeepRespawnControllerRunning = TRUE BREAK
		CASE SPCID_POLICE_STATION_SC	g_eDebug_ForcePoliceStationRespawn = POLICE_STATION_SC	g_bDebug_KeepRespawnControllerRunning = TRUE BREAK
		CASE SPCID_POLICE_STATION_DT	g_eDebug_ForcePoliceStationRespawn = POLICE_STATION_DT	g_bDebug_KeepRespawnControllerRunning = TRUE BREAK
		CASE SPCID_POLICE_STATION_RH	g_eDebug_ForcePoliceStationRespawn = POLICE_STATION_RH	g_bDebug_KeepRespawnControllerRunning = TRUE BREAK
		CASE SPCID_POLICE_STATION_SS	g_eDebug_ForcePoliceStationRespawn = POLICE_STATION_SS	g_bDebug_KeepRespawnControllerRunning = TRUE BREAK
		CASE SPCID_POLICE_STATION_PB	g_eDebug_ForcePoliceStationRespawn = POLICE_STATION_PB	g_bDebug_KeepRespawnControllerRunning = TRUE BREAK
		CASE SPCID_POLICE_STATION_HW 	g_eDebug_ForcePoliceStationRespawn = POLICE_STATION_HW	g_bDebug_KeepRespawnControllerRunning = TRUE BREAK
		
		CASE SPCID_HOSPITAL_RH			g_eDebug_ForceHospitalRespawn = HOSPITAL_RH	g_bDebug_KeepRespawnControllerRunning = TRUE BREAK
		CASE SPCID_HOSPITAL_SC			g_eDebug_ForceHospitalRespawn = HOSPITAL_SC	g_bDebug_KeepRespawnControllerRunning = TRUE BREAK
		CASE SPCID_HOSPITAL_DT			g_eDebug_ForceHospitalRespawn = HOSPITAL_DT	g_bDebug_KeepRespawnControllerRunning = TRUE BREAK
		CASE SPCID_HOSPITAL_SS			g_eDebug_ForceHospitalRespawn = HOSPITAL_SS	g_bDebug_KeepRespawnControllerRunning = TRUE BREAK
		CASE SPCID_HOSPITAL_PB			g_eDebug_ForceHospitalRespawn = HOSPITAL_PB	g_bDebug_KeepRespawnControllerRunning = TRUE BREAK
		
		CASE SPCID_STAT_MODIFIER
			g_bRPGStatTest = NOT g_bRPGStatTest
		BREAK
		
		CASE SPCID_CARMOD_GARAGE
		
			VEHICLE_INDEX veh
			
			REQUEST_MODEL(INFERNUS)
			WHILE NOT HAS_MODEL_LOADED(INFERNUS)
				REQUEST_MODEL(INFERNUS)
				WAIT(0)
			ENDWHILE
			veh = CREATE_VEHICLE(INFERNUS, << -1098.8207, -1987.4276, 12.1798 >>, 0.2747, FALSE)
			SET_VEHICLE_ON_GROUND_PROPERLY(veh)
			SET_MODEL_AS_NO_LONGER_NEEDED(INFERNUS)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(veh)
			
			REQUEST_MODEL(DUBSTA)
			WHILE NOT HAS_MODEL_LOADED(DUBSTA)
				REQUEST_MODEL(DUBSTA)
				WAIT(0)
			ENDWHILE
			veh = CREATE_VEHICLE(DUBSTA, << -1098.2831, -2001.2451, 12.1837 >>, 6.9082, FALSE)
			SET_VEHICLE_ON_GROUND_PROPERLY(veh)
			SET_MODEL_AS_NO_LONGER_NEEDED(DUBSTA)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(veh)
			
			#if not USE_NRM_DLC
			Execute_Activate_Shop_Carmod(0)
			#endif
			
			g_bShopsAvailableInDebug = TRUE
		BREAK
		
		CASE SPCID_SHOP_SYSTEMS_ON
			g_bShopsAvailableInDebug = TRUE
		BREAK
		
		CASE SPCID_SAVEGAME_BED_ON
			g_bSavegameBedAvailableInDebug = TRUE
		BREAK
		
		CASE SPCID_VEHICLE_GEN_ON
			g_bVehicleGenAvailableInDebug = TRUE
		BREAK
		
		CASE SPCID_ACTIVATE_SHOPS
			
			// Send shop kill command and wait for scripts to cleanup
			FORCE_SHOP_CLEANUP_ALL()
			WAIT(0)
			WAIT(0)
			WAIT(0)
			WAIT(0)
			WAIT(0)
			
			// Make sure the shops will run
			INT i
			FOR i = 0 TO NUMBER_OF_SHOPS-1
			
				// SP ONLY SHOPS
				IF IS_SHOP_AVAILABLE_IN_SINGLEPLAYER(INT_TO_ENUM(SHOP_NAME_ENUM, i))
					SET_SHOP_IS_AVAILABLE(INT_TO_ENUM(SHOP_NAME_ENUM, i), TRUE)
					SET_SHOP_IS_OPEN_FOR_BUSINESS(INT_TO_ENUM(SHOP_NAME_ENUM, i), TRUE)
					SET_PLAYER_IS_KICKING_OFF_IN_SHOP(INT_TO_ENUM(SHOP_NAME_ENUM, i), FALSE)
					SET_PLAYER_HAS_VISITED_SHOP(INT_TO_ENUM(SHOP_NAME_ENUM, i), FALSE)
					SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(INT_TO_ENUM(SHOP_NAME_ENUM, i), FALSE)
					SET_ROBBERY_CAN_TAKE_PLACE_IN_SHOP(INT_TO_ENUM(SHOP_NAME_ENUM, i), FALSE)
				ENDIF
				SET_SHOP_BIT(INT_TO_ENUM(SHOP_NAME_ENUM, i), SHOP_NS_FLAG_CLEAR_SHOP_LAUNCHED_FLAG, TRUE)
			ENDFOR
			
			// Spawn a vehicle near the car mod garage
			IF GET_DISTANCE_BETWEEN_COORDS(GET_SHOP_COORDS(CARMOD_SHOP_01_AP), GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)) < 50.0
				VEHICLE_INDEX tempVehID
				REQUEST_MODEL(DUBSTA)
				WHILE NOT HAS_MODEL_LOADED(DUBSTA)
					WAIT(0)
				ENDWHILE
				tempVehID = CREATE_VEHICLE(DUBSTA, << -1125.9045, -1976.0383, 12.1822 >>, 92.1747, FALSE, FALSE)
				SET_VEHICLE_ON_GROUND_PROPERLY(tempVehID)
				SET_VEHICLE_AS_NO_LONGER_NEEDED(tempVehID)
				SET_MODEL_AS_NO_LONGER_NEEDED(DUBSTA)
			ENDIF
			
			// Unlock all gun shop weapons
			REPEAT 3 i
				SET_PLAYER_PED_WEAPON_UNLOCKED(INT_TO_ENUM(enumCharacterList, i), WEAPONTYPE_INVALID, TRUE)
				SET_PLAYER_PED_WEAPON_UNLOCKED(INT_TO_ENUM(enumCharacterList, i), WEAPONTYPE_UNARMED, TRUE)
				SET_PLAYER_PED_WEAPON_UNLOCKED(INT_TO_ENUM(enumCharacterList, i), WEAPONTYPE_PISTOL, TRUE)
				SET_PLAYER_PED_WEAPON_UNLOCKED(INT_TO_ENUM(enumCharacterList, i), WEAPONTYPE_COMBATPISTOL, TRUE)
				SET_PLAYER_PED_WEAPON_UNLOCKED(INT_TO_ENUM(enumCharacterList, i), WEAPONTYPE_APPISTOL, TRUE)
				SET_PLAYER_PED_WEAPON_UNLOCKED(INT_TO_ENUM(enumCharacterList, i), WEAPONTYPE_SMG, TRUE)
				SET_PLAYER_PED_WEAPON_UNLOCKED(INT_TO_ENUM(enumCharacterList, i), WEAPONTYPE_MICROSMG, TRUE)
				SET_PLAYER_PED_WEAPON_UNLOCKED(INT_TO_ENUM(enumCharacterList, i), WEAPONTYPE_ASSAULTRIFLE, TRUE)
				SET_PLAYER_PED_WEAPON_UNLOCKED(INT_TO_ENUM(enumCharacterList, i), WEAPONTYPE_CARBINERIFLE, TRUE)
				SET_PLAYER_PED_WEAPON_UNLOCKED(INT_TO_ENUM(enumCharacterList, i), WEAPONTYPE_ADVANCEDRIFLE, TRUE)
				SET_PLAYER_PED_WEAPON_UNLOCKED(INT_TO_ENUM(enumCharacterList, i), WEAPONTYPE_MG, TRUE)
				SET_PLAYER_PED_WEAPON_UNLOCKED(INT_TO_ENUM(enumCharacterList, i), WEAPONTYPE_COMBATMG, TRUE)
				SET_PLAYER_PED_WEAPON_UNLOCKED(INT_TO_ENUM(enumCharacterList, i), WEAPONTYPE_PUMPSHOTGUN, TRUE)
				SET_PLAYER_PED_WEAPON_UNLOCKED(INT_TO_ENUM(enumCharacterList, i), WEAPONTYPE_SAWNOFFSHOTGUN, TRUE)
				SET_PLAYER_PED_WEAPON_UNLOCKED(INT_TO_ENUM(enumCharacterList, i), WEAPONTYPE_ASSAULTSHOTGUN, TRUE)
				SET_PLAYER_PED_WEAPON_UNLOCKED(INT_TO_ENUM(enumCharacterList, i), WEAPONTYPE_HEAVYSNIPER, TRUE)
				SET_PLAYER_PED_WEAPON_UNLOCKED(INT_TO_ENUM(enumCharacterList, i), WEAPONTYPE_REMOTESNIPER, TRUE)
				SET_PLAYER_PED_WEAPON_UNLOCKED(INT_TO_ENUM(enumCharacterList, i), WEAPONTYPE_SNIPERRIFLE, TRUE)
				SET_PLAYER_PED_WEAPON_UNLOCKED(INT_TO_ENUM(enumCharacterList, i), WEAPONTYPE_GRENADELAUNCHER, TRUE)
				SET_PLAYER_PED_WEAPON_UNLOCKED(INT_TO_ENUM(enumCharacterList, i), WEAPONTYPE_RPG, TRUE)
				SET_PLAYER_PED_WEAPON_UNLOCKED(INT_TO_ENUM(enumCharacterList, i), WEAPONTYPE_MINIGUN, TRUE)
				SET_PLAYER_PED_WEAPON_UNLOCKED(INT_TO_ENUM(enumCharacterList, i), WEAPONTYPE_GRENADE, TRUE)
				SET_PLAYER_PED_WEAPON_UNLOCKED(INT_TO_ENUM(enumCharacterList, i), WEAPONTYPE_SMOKEGRENADE, TRUE)
				SET_PLAYER_PED_WEAPON_UNLOCKED(INT_TO_ENUM(enumCharacterList, i), WEAPONTYPE_STICKYBOMB, TRUE)
				SET_PLAYER_PED_WEAPON_UNLOCKED(INT_TO_ENUM(enumCharacterList, i), WEAPONTYPE_MOLOTOV, TRUE)
				SET_PLAYER_PED_WEAPON_UNLOCKED(INT_TO_ENUM(enumCharacterList, i), WEAPONTYPE_STUNGUN, TRUE)
				SET_PLAYER_PED_WEAPON_UNLOCKED(INT_TO_ENUM(enumCharacterList, i), WEAPONTYPE_PETROLCAN, TRUE)
				SET_PLAYER_PED_WEAPON_UNLOCKED(INT_TO_ENUM(enumCharacterList, i), WEAPONTYPE_OBJECT, TRUE)
				SET_PLAYER_PED_WEAPON_UNLOCKED(INT_TO_ENUM(enumCharacterList, i), GADGETTYPE_PARACHUTE, TRUE)
				SET_PLAYER_PED_WEAPON_UNLOCKED(INT_TO_ENUM(enumCharacterList, i), WEAPONTYPE_KNIFE, TRUE)
				SET_PLAYER_PED_WEAPON_UNLOCKED(INT_TO_ENUM(enumCharacterList, i), WEAPONTYPE_NIGHTSTICK, TRUE)
				SET_PLAYER_PED_WEAPON_UNLOCKED(INT_TO_ENUM(enumCharacterList, i), WEAPONTYPE_HAMMER, TRUE)
				SET_PLAYER_PED_WEAPON_UNLOCKED(INT_TO_ENUM(enumCharacterList, i), WEAPONTYPE_BAT, TRUE)
				SET_PLAYER_PED_WEAPON_UNLOCKED(INT_TO_ENUM(enumCharacterList, i), WEAPONTYPE_CROWBAR, TRUE)
				SET_PLAYER_PED_WEAPON_UNLOCKED(INT_TO_ENUM(enumCharacterList, i), WEAPONTYPE_DLC_PISTOL50, TRUE)
				SET_PLAYER_PED_WEAPON_UNLOCKED(INT_TO_ENUM(enumCharacterList, i), WEAPONTYPE_DLC_ASSAULTSMG, TRUE)
				SET_PLAYER_PED_WEAPON_UNLOCKED(INT_TO_ENUM(enumCharacterList, i), WEAPONTYPE_DLC_BULLPUPSHOTGUN, TRUE)
				SET_PLAYER_PED_WEAPON_UNLOCKED(INT_TO_ENUM(enumCharacterList, i), WEAPONTYPE_DLC_ASSAULTMG, TRUE)
				SET_PLAYER_PED_WEAPON_UNLOCKED(INT_TO_ENUM(enumCharacterList, i), WEAPONTYPE_DLC_ASSAULTSNIPER, TRUE)
				SET_PLAYER_PED_WEAPON_UNLOCKED(INT_TO_ENUM(enumCharacterList, i), WEAPONTYPE_DLC_PROGRAMMABLEAR, TRUE)
			ENDREPEAT
			
			// Ensure some mods are unlocked
			Execute_Code_ID(CID_ACTIVATE_SHOP_CARMOD, 0)
			
			g_bShopsAvailableInDebug = TRUE
			
				IF GET_COMMANDLINE_PARAM_EXISTS("sc_SupermodDebug")
					EXECUTE_CONTENT_CHANGESET(HASH("mpLowrider"), HASH("GROUP_MAP"), HASH("MPLOWRIDER_GTA5_CITYE_SCENTRAL_01"))
				ENDIF
			
		BREAK
		
		CASE SPCID_ACQUIRE_ITEMS
			SET_ALL_PED_COMPONENT_ITEMS_ACQUIRED()
		BREAK
		
		CASE SPCID_AVAILABLE_ITEMS
			SET_ALL_PED_COMPONENT_ITEMS_AVAILABLE()
		BREAK
		
		CASE SPCID_ACTIVATE_SAVEHOUSES
			INT iSavehouse			
			REPEAT NUMBER_OF_SAVEHOUSE_LOCATIONS iSavehouse
				Set_Savehouse_Respawn_Available(INT_TO_ENUM(SAVEHOUSE_NAME_ENUM, iSavehouse), TRUE)
			ENDREPEAT								
		BREAK
		
		CASE SPCID_AMB_PRISON
			/*REQUEST_MODEL(BUZZARD)
			WHILE NOT HAS_MODEL_LOADED(BUZZARD)
				WAIT(0)
			ENDWHILE
			
			// Create the taxi and then set it as no longer needed.
			VEHICLE_INDEX vehHeli
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				vehHeli = CREATE_VEHICLE(BUZZARD, << 1863.9872, 2606.8630, 44.6724 >>, 100.0586)
				SET_VEHICLE_AS_NO_LONGER_NEEDED(vehHeli)
			ENDIF
			SET_MODEL_AS_NO_LONGER_NEEDED(BUZZARD)*/
		BREAK
		
		CASE SPCID_BASEJUMP
			TEXT_LABEL_15 sBJID
			sBJID = sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].sMissionID

			// Tell the base jump script which jump to launch.
			IF NOT ARE_STRINGS_EQUAL("OJBJa", sBJID)
				IF ARE_STRINGS_EQUAL("OJBJ1", sBJID)
					g_savedGlobals.sBasejumpData.iLaunchRank = ENUM_TO_INT(BJJUMPID_HARBOR)
				ELIF ARE_STRINGS_EQUAL("OJBJ2", sBJID)
					g_savedGlobals.sBasejumpData.iLaunchRank = ENUM_TO_INT(BJJUMPID_RACE_TRACK)
				ELIF ARE_STRINGS_EQUAL("OJBJ3", sBJID)
					g_savedGlobals.sBasejumpData.iLaunchRank = ENUM_TO_INT(BJJUMPID_WINDMILLS)
				ELIF ARE_STRINGS_EQUAL("OJBJ4", sBJID)
					g_savedGlobals.sBasejumpData.iLaunchRank = ENUM_TO_INT(BJJUMPID_NORTH_CLIFF)
				ELIF ARE_STRINGS_EQUAL("OJBJ5", sBJID)
					g_savedGlobals.sBasejumpData.iLaunchRank = ENUM_TO_INT(BJJUMPID_MAZE_BANK)
				ELIF ARE_STRINGS_EQUAL("OJBJ6", sBJID)
					g_savedGlobals.sBasejumpData.iLaunchRank = ENUM_TO_INT(BJJUMPID_CRANE)
					IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
						PRINTLN("Freezing player for 5 seconds while crane loads in for Basejump 6.")
						FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
						WAIT(3000)
						FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
						PRINTLN("Unfreezing player.")
					ENDIF
				ELIF ARE_STRINGS_EQUAL("OJBJ7", sBJID)
					g_savedGlobals.sBasejumpData.iLaunchRank = ENUM_TO_INT(BJJUMPID_RIVER_CLIFF)
				ELIF ARE_STRINGS_EQUAL("OJBJ8", sBJID)
					g_savedGlobals.sBasejumpData.iLaunchRank = ENUM_TO_INT(BJJUMPID_RUNAWAY_TRAIN)
				ELIF ARE_STRINGS_EQUAL("OJBJ9", sBJID)
					g_savedGlobals.sBasejumpData.iLaunchRank = ENUM_TO_INT(BJJUMPID_GOLF_COURSE)
				ELIF ARE_STRINGS_EQUAL("OJBJ10", sBJID)
					g_savedGlobals.sBasejumpData.iLaunchRank = ENUM_TO_INT(BJJUMPID_1K)
				ELIF ARE_STRINGS_EQUAL("OJBJ11", sBJID)
					g_savedGlobals.sBasejumpData.iLaunchRank = ENUM_TO_INT(BJJUMPID_1_5K)
				ELIF ARE_STRINGS_EQUAL("OJBJ12", sBJID)
					g_savedGlobals.sBasejumpData.iLaunchRank = ENUM_TO_INT(BJJUMPID_CANAL)
				ELIF ARE_STRINGS_EQUAL("OJBJ13", sBJID)
					g_savedGlobals.sBasejumpData.iLaunchRank = ENUM_TO_INT(BJJUMPID_ROCK_CLIFF)
				ENDIF
				
				//if we're launching directly to a jump, make sure to set the bool so we'll always spawn a heli
				#IF IS_DEBUG_BUILD
					g_bjDebugLaunchTimeStamp = GET_GAME_TIMER()
					PRINTLN("setting bj debug launch timestamp to: ", g_bjDebugLaunchTimeStamp)
				#ENDIF
				PRINTLN("About to start base jumping script. Rank Starting: ", g_savedGlobals.sBasejumpData.iLaunchRank)
			ENDIF
		BREAK
		
		CASE SPCID_HUNTING
			#IF IS_DEBUG_BUILD
				SET_CLOCK_TIME(5, 0, 0)
				
				g_bjDebugLaunchTimeStamp = GET_GAME_TIMER()
				PRINTLN("setting hunting debug launch timestamp to: ", g_bjDebugLaunchTimeStamp)
			#ENDIF
		BREAK
		
		CASE SPCID_BILLIARDS
			Execute_Code_ID(CID_ACTIVATE_MINIGAME_POOL)
		BREAK
		
		CASE SPCID_ASSASSIN_MISSION
			// Need to grab the mission ID.
			sMissID = sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].sMissionID

			IF ARE_STRINGS_EQUAL(sMissID, "OJAva")
				g_savedGlobals.sAssassinData.iCurrentAssassinRank = 0
			ELIF ARE_STRINGS_EQUAL(sMissID, "OJAml")
				g_savedGlobals.sAssassinData.iCurrentAssassinRank = 1
			ELIF ARE_STRINGS_EQUAL(sMissID, "OJAhk")
				g_savedGlobals.sAssassinData.iCurrentAssassinRank = 2
			ELIF ARE_STRINGS_EQUAL(sMissID, "OJAbs")
				g_savedGlobals.sAssassinData.iCurrentAssassinRank = 3
			ELIF ARE_STRINGS_EQUAL(sMissID, "OJAcs")
				g_savedGlobals.sAssassinData.iCurrentAssassinRank = 4
			ENDIF
			
			SET_CLOCK_TIME(12, 0, 0)
			
			// Flag us as being a debug launch.
			SET_BITMASK_AS_ENUM(g_savedGlobals.sAssassinData.iBools, ASS_BOOL_DEBUG_LAUNCH)
		BREAK
		
		CASE SPCID_DARTS
			TEXT_LABEL_15 sDBID
			sDBID = sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].sMissionID
			
			VECTOR vDBPlayerLocation
			FLOAT vDBPlayerHeading
			
			IF ARE_STRINGS_EQUAL(sDBID, "MGDT1")
				vDBPlayerLocation =  <<-572.2811, 292.0009, 78.1765>>
				vDBPlayerHeading = 345.9209
			ELIF ARE_STRINGS_EQUAL(sDBID, "MGDT2")
				vDBPlayerLocation = << 1994.2838, 3049.4629, 46.1258 >>
				vDBPlayerHeading = 49.4248
			ELSE
				SCRIPT_ASSERT("SPCID_DARTS: Mission ID not found! Maybe update strings or the debug xml?")
			ENDIF
			
			//Warp the player near the darts location.
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
				DO_PLAYER_MAP_WARP_WITH_LOAD(vDBPlayerLocation, vDBPlayerHeading)
				
				g_bForceCustomDartSpawn = TRUE
			ENDIF
		BREAK
		
		CASE SPCID_GROUND_TRAFFIC
			// Kill the old trafficking script
			TERMINATE_ALL_SCRIPTS_WITH_THIS_NAME("traffick_ground")
			
			//Set up Trevor as the owner of Oscar's airfield.
			g_savedGlobals.sPropertyData.propertyOwnershipData[PROPERTY_ARMS_TRAFFICKING].charOwner = CHAR_TREVOR
			
			// Turn trafficking on.
			EXECUTE_CODE_ID(CID_ACTIVATE_MINIGAME_TRAFFICKING)
			
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
			ENDIF
			
			// Need to parse the mission ID for a number. That number is our rank.
			INT iRank
			iRank = sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].iRank
			g_savedGlobals.sTraffickingData.iGroundRank = iRank
			
			g_savedGlobals.sTraffickingData.todNextPlayable_Gnd = INVALID_TIMEOFDAY
			
			SET_BITMASK_AS_ENUM(g_savedGlobals.sTraffickingData.iBools, TRAF_BOOL_LaunchedGndViaDebug)
		BREAK
		
		CASE SPCID_AIR_TRAFFIC
			// Kill the old trafficking script
			TERMINATE_ALL_SCRIPTS_WITH_THIS_NAME("traffick_air")
			
			//Set up Trevor as the owner of Oscar's airfield.
			g_savedGlobals.sPropertyData.propertyOwnershipData[PROPERTY_ARMS_TRAFFICKING].charOwner = CHAR_TREVOR
			
			g_savedGlobals.sTraffickingData.todNextPlayable_Air = INT_TO_ENUM(TIMEOFDAY, 0)
			
			// Turn trafficking on.
			Execute_Code_ID(CID_ACTIVATE_MINIGAME_TRAFFICKING)
			
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
			ENDIF
			
			// Need to parse the mission ID for a number. That number is our rank.
			sMissID = sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].sMissionID

			IF ARE_STRINGS_EQUAL(sMissID, "OJDA1")
				g_savedGlobals.sTraffickingData.iAirRank = 0
			ELIF ARE_STRINGS_EQUAL(sMissID, "OJDA2")
				g_savedGlobals.sTraffickingData.iAirRank = 1
			ELIF ARE_STRINGS_EQUAL(sMissID, "OJDA3")
				g_savedGlobals.sTraffickingData.iAirRank = 2
			ELIF ARE_STRINGS_EQUAL(sMissID, "OJDA4")
				g_savedGlobals.sTraffickingData.iAirRank = 3
			ELIF ARE_STRINGS_EQUAL(sMissID, "OJDA5")
				g_savedGlobals.sTraffickingData.iAirRank = 4
			ELIF ARE_STRINGS_EQUAL(sMissID, "OJDA6")
				IF g_savedGlobals.sTraffickingData.iAirRank < 5
					g_savedGlobals.sTraffickingData.iAirRank = 5
				ENDIF
			ENDIF
			g_savedGlobals.sTraffickingData.todNextPlayable_Air = INVALID_TIMEOFDAY
			
			SET_BITMASK_AS_ENUM(g_savedGlobals.sTraffickingData.iBools, TRAF_BOOL_LaunchedAirViaDebug)
		BREAK
		
		CASE SPCID_OFFROAD_RACES
			sMissID = sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].sMissionID
			g_savedGlobals.sTraffickingData.todNextPlayable_Gnd = INT_TO_ENUM(TIMEOFDAY, 0)
			
			SWITCH GET_HASH_KEY(sMissID)
				//Simulate the state of blips and globals when no races have been completed.
				CASE HASH("MGOR1gf")
					SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_OFFROAD_RACE5,	TRUE)
					SET_STATIC_BLIP_LONG_RANGE(STATIC_BLIP_MINIGAME_OFFROAD_RACE5)
					g_savedGlobals.sOffroadData.eCurrentRace = OFFROAD_RACE_CANYON_CLIFFS
				BREAK
				
				//Simulate the state of blips and globals after completing OFFROAD_RACE5.
				CASE HASH("MGOR2gf")
					
					SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_OFFROAD_RACE5,	TRUE)
					SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_OFFROAD_RACE8,	TRUE)
					SET_STATIC_BLIP_LONG_RANGE(STATIC_BLIP_MINIGAME_OFFROAD_RACE8)
					g_savedGlobals.sOffroadData.eCurrentRace = OFFROAD_RACE_RIDGE_RUN
				BREAK
				
				//Simulate the state of blips and globals after completing OFFROAD_RACE8.
				CASE HASH("MGOR6gf")
					SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_OFFROAD_RACE5,	TRUE)
					SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_OFFROAD_RACE8,	TRUE)
					SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_OFFROAD_RACE12, 	TRUE)
					SET_STATIC_BLIP_LONG_RANGE(STATIC_BLIP_MINIGAME_OFFROAD_RACE12)
					g_savedGlobals.sOffroadData.eCurrentRace = OFFROAD_RACE_MINEWARD_SPIRAL
				BREAK
				
				//Simulate the state of blips and globals after completing OFFROAD_RACE12.
				CASE HASH("MGOR3gf")
					SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_OFFROAD_RACE5,	TRUE)
					SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_OFFROAD_RACE8,	TRUE)
					SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_OFFROAD_RACE12,	TRUE)
					SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_OFFROAD_RACE9,	TRUE)
					SET_STATIC_BLIP_LONG_RANGE(STATIC_BLIP_MINIGAME_OFFROAD_RACE9)
					g_savedGlobals.sOffroadData.eCurrentRace = OFFROAD_RACE_VALLEY_TRAIL			
				BREAK
				
				//Simulate the state of blips and globals after completing OFFROAD_RACE9.
				CASE HASH("MGOR4gf")
					SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_OFFROAD_RACE5,	TRUE)
					SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_OFFROAD_RACE8,	TRUE)
					SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_OFFROAD_RACE12,	TRUE)
					SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_OFFROAD_RACE9,	TRUE)
					SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_OFFROAD_RACE10,	TRUE)
					SET_STATIC_BLIP_LONG_RANGE(STATIC_BLIP_MINIGAME_OFFROAD_RACE10)
					g_savedGlobals.sOffroadData.eCurrentRace = OFFROAD_RACE_LAKESIDE_SPLASH
				BREAK
				
				//Simulate the state of blips and globals after completing OFFROAD_RACE10.
				CASE HASH("MGOR5gf")
					SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_OFFROAD_RACE5,	TRUE)
					SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_OFFROAD_RACE8,	TRUE)
					SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_OFFROAD_RACE12,	TRUE)
					SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_OFFROAD_RACE9,	TRUE)
					SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_OFFROAD_RACE10,	TRUE)
					SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_OFFROAD_RACE11,	TRUE)
					SET_STATIC_BLIP_LONG_RANGE(STATIC_BLIP_MINIGAME_OFFROAD_RACE11)
					g_savedGlobals.sOffroadData.eCurrentRace = OFFROAD_RACE_ECO_FRIENDLY
				BREAK
				
				//Artificially turn all offroad races on for debug launch items.
				DEFAULT
					SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_OFFROAD_RACE5,	TRUE)
					SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_OFFROAD_RACE8,	TRUE)
					SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_OFFROAD_RACE9,	TRUE)
					SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_OFFROAD_RACE10,	TRUE)
					SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_OFFROAD_RACE11,	TRUE)
					SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_OFFROAD_RACE12,	TRUE)
					g_savedGlobals.sOffroadData.eCurrentRace = OFFROAD_RACE_ECO_FRIENDLY
					
					//Execute this codeIDs manually in debug mode.
					Execute_Code_ID(CID_ACTIVATE_MINIGAME_OFFROAD_RACES)
				BREAK
			ENDSWITCH
			
			// We're warping to a race. Set a flag. This will make us a car.
			g_ORR_DebugLaunchWarpTime = GET_GAME_TIMER() 
		BREAK
		
		CASE SPCID_SHOOTRANGE
			g_savedGlobals.sRangeData[0].m_bSeenTutorial = FALSE
			
			// Turn shooting range on.
			Execute_Code_ID(CID_ACTIVATE_GUNSHOP_AND_RANGE)
		BREAK
		
		CASE SPCID_SPAWN_TAXI
		
			BOOL bNeedTaxi
			VECTOR vTaxiPos
			VECTOR vPosition
			FLOAT fHeading
			INT iNumLanes
		
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())

				bNeedTaxi = TRUE
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					IF (GET_ENTITY_MODEL(GET_ENTITY_FROM_PED_OR_VEHICLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))) = TAXI)
						bNeedTaxi = FALSE
					ENDIF
				ENDIF
				
				IF bNeedTaxi
					REQUEST_MODEL(TAXI)
					WHILE NOT HAS_MODEL_LOADED(TAXI)
						WAIT(0)
					ENDWHILE
					
					// Create the taxi and then set it as no longer needed.
					VEHICLE_INDEX vehTaxi
					IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
						vTaxiPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
						GET_NTH_CLOSEST_VEHICLE_NODE_WITH_HEADING(vTaxiPos, 1, vPosition, fHeading, iNumLanes)
						vehTaxi = CREATE_VEHICLE(TAXI, vPosition, fHeading)
						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehTaxi)
						SET_VEHICLE_AS_NO_LONGER_NEEDED(vehTaxi)
					ENDIF
					SET_MODEL_AS_NO_LONGER_NEEDED(TAXI)
				ENDIF
			ENDIF
		BREAK
		
		CASE SPCID_TAXI_MISSION
			
			sMissID = sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].sMissionID
			
			IF NOT ARE_STRINGS_EQUAL(sMissID, "OJTX")
				// Okay, we selected a debug mission from the menu, not the controller.
				// Spawn a taxi.
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					bNeedTaxi = TRUE
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						IF (GET_ENTITY_MODEL(GET_ENTITY_FROM_PED_OR_VEHICLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))) = TAXI)
							bNeedTaxi = FALSE
						ENDIF
					ENDIF
					
					IF bNeedTaxi
						REQUEST_MODEL(TAXI)
						WHILE NOT HAS_MODEL_LOADED(TAXI)
							WAIT(0)
						ENDWHILE
						
						// Create the taxi and then set it as no longer needed.
						VEHICLE_INDEX vehTaxi
						IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
							vTaxiPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
							GET_NTH_CLOSEST_VEHICLE_NODE_WITH_HEADING(vTaxiPos, 1, vPosition, fHeading, iNumLanes)
							vehTaxi = CREATE_VEHICLE(TAXI, vPosition, fHeading)
							SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehTaxi)
							SET_VEHICLE_AS_NO_LONGER_NEEDED(vehTaxi)
						ENDIF
						SET_MODEL_AS_NO_LONGER_NEEDED(TAXI)
					ENDIF
					
					// Set our rank based on what we picked
					//<!-- 1. Need Excitement -->		//<!-- 2. Take It Easy-->		//<!-- 3. Deadline -->
					//<!-- 4. Got Your Back -->			//<!-- 5. Take To Best -->		//<!-- 6. I Know the Way -->
					//<!-- 7. Cut You In -->			//<!-- 8. Got You Now -->		//<!-- 9. Clown Car -->	//<!-- 10. Follow That Car -->
					IF ARE_STRINGS_EQUAL(sMissID, "OJTX1")
						SET_BITMASK_AS_ENUM(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_NeedExcitement)
					ELIF ARE_STRINGS_EQUAL(sMissID, "OJTX2")
						SET_BITMASK_AS_ENUM(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_TakeItEasy)
					ELIF ARE_STRINGS_EQUAL(sMissID, "OJTX3")
						SET_BITMASK_AS_ENUM(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_Deadline)
					ELIF ARE_STRINGS_EQUAL(sMissID, "OJTX4")
						SET_BITMASK_AS_ENUM(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_GotYourBack)
					ELIF ARE_STRINGS_EQUAL(sMissID, "OJTX5")
						SET_BITMASK_AS_ENUM(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_TakeToBest)
//					ELIF ARE_STRINGS_EQUAL(sMissID, "OJTX6")
//						SET_BITMASK_AS_ENUM(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_IKnowWay)
					ELIF ARE_STRINGS_EQUAL(sMissID, "OJTX7")
						SET_BITMASK_AS_ENUM(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_CutYouIn)
					ELIF ARE_STRINGS_EQUAL(sMissID, "OJTX8")
						SET_BITMASK_AS_ENUM(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_GotYouNow)
					ELIF ARE_STRINGS_EQUAL(sMissID, "OJTX9")
						SET_BITMASK_AS_ENUM(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_ClownCar)
					ELIF ARE_STRINGS_EQUAL(sMissID, "OJTX10")
						SET_BITMASK_AS_ENUM(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_FollowThatCar)
					ELIF ARE_STRINGS_EQUAL(sMissID, "OJTXP")
						SET_BITMASK_AS_ENUM(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_Procedural)
					ENDIF
					SET_BITMASK_AS_ENUM(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_LAUNCHED_VIA_DEBUG)
				ENDIF
			ENDIF
		BREAK
		
		CASE SPCID_TENNIS
			// Tennis has 4 things. In this order:
			// - Venice Warp
			// - Michael Warp
			// - Activate MG
			// - Make Wife play Tennis
			// Need to parse the mission ID for a number. That number is our rank.
			sMissID = sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].sMissionID
			IF ARE_STRINGS_EQUAL(sMissID, "MGTNm")
				
				// Make the wife play Tennis.
				g_eCurrentFamilyEvent[FM_MICHAEL_WIFE] = FE_M_WIFE_playing_tennis
			ENDIF
			IF ARE_STRINGS_EQUAL(sMissID, "MGTNw")
				//do nothing
			ENDIF
			
		BREAK
		
		CASE SPCID_GOLF
			TEXT_LABEL_31 sGolfID
			sGolfID = sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].sMissionID
			
			IF ARE_STRINGS_EQUAL(sGolfID, "MGGFa")
				// Activate.
				Execute_Code_ID(CID_ACTIVATE_MINIGAME_GOLF)
			ELIF ARE_STRINGS_EQUAL(sGolfID, "MGGFlb")
				// Lock all buddies.
				g_savedGlobals.sGolfData.unlockedBuddies = GB_NONE
			ELIF ARE_STRINGS_EQUAL(sGolfID, "MGGFub")
				// Unlock all buddies.
				g_savedGlobals.sGolfData.unlockedBuddies = GB_ALL_UNLOCKED
			ELSE
				// Case where we're launching golf directly.
				g_savedGlobals.sFlow.isGameflowActive = FALSE
			ENDIF
		BREAK
		
		CASE SPCID_TOWING

			TEXT_LABEL_31 sTowingID
			VEHICLE_INDEX vehTowTruck
			sTowingID = sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].sMissionID
			PRINTLN("Here's the towing ID: ", sTowingID)
			
			// Turn towing on.
			Execute_Code_ID(CID_ACTIVATE_MINIGAME_TOWING)
				
			IF ARE_STRINGS_EQUAL(sTowingID, "OJTCgfT1")
				CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_TONYA1)
				CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_TONYA2)
				CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_TONYA3)
				CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_TONYA4)
				CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_TONYA5)
				CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_PROC)
			
				// Launching the controller.
				SET_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_TONYA1)
				PRINTLN("SETTING BITMASK - TOW_GLOBAL_TONYA1")
			ELIF ARE_STRINGS_EQUAL(sTowingID, "OJTCgfT2")
				CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_TONYA1)
				CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_TONYA2)
				CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_TONYA3)
				CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_TONYA4)
				CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_TONYA5)
				CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_PROC)
			
				// Trying to launch the second Tonya mission.
				SET_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_TONYA2)
				PRINTLN("SETTING BITMASK - TOW_GLOBAL_TONYA2")
			ELIF ARE_STRINGS_EQUAL(sTowingID, "OJTCgfT3")
				CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_TONYA1)
				CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_TONYA2)
				CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_TONYA3)
				CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_TONYA4)
				CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_TONYA5)
				CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_PROC)
				
				// Trying to launch the third Tonya mission.
				SET_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_TONYA3)
				PRINTLN("SETTING BITMASK - TOW_GLOBAL_TONYA3")
			ELIF ARE_STRINGS_EQUAL(sTowingID, "OJTCgfT4")
				CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_TONYA1)
				CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_TONYA2)
				CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_TONYA3)
				CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_TONYA4)
				CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_TONYA5)
				CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_PROC)
				
				// Trying to launch the fourth Tonya mission.
				SET_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_TONYA4)
				PRINTLN("SETTING BITMASK - TOW_GLOBAL_TONYA4")
			ELIF ARE_STRINGS_EQUAL(sTowingID, "OJTCgfT5")
				CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_TONYA1)
				CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_TONYA2)
				CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_TONYA3)
				CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_TONYA4)
				CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_TONYA5)
				CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_PROC)
				
				// Trying to launch the fifth Tonya mission.
				SET_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_TONYA5)
				PRINTLN("SETTING BITMASK - TOW_GLOBAL_TONYA5")
			ELIF ARE_STRINGS_EQUAL(sTowingID, "OJTCgfP")
				CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_TONYA1)
				CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_TONYA2)
				CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_TONYA3)
				CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_TONYA4)
				CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_TONYA5)
				CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_PROC)
				
				// Trying to launch the fifth Tonya mission.
				SET_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_PROC)
				PRINTLN("SETTING BITMASK - TOW_GLOBAL_PROC")
				
				BOOL bNeedTruck
				bNeedTruck = TRUE
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						IF (GET_ENTITY_MODEL(GET_ENTITY_FROM_PED_OR_VEHICLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))) = TOWTRUCK)
							bNeedTruck = FALSE
						ENDIF
					ENDIF
				ENDIF
				
				IF bNeedTruck
					CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
				
					REQUEST_MODEL(TOWTRUCK)
					WHILE NOT HAS_MODEL_LOADED(TOWTRUCK)
						WAIT(0)
					ENDWHILE
					
					// Create the truck and then set it as no longer needed.
					VECTOR vTruckPos
					INT iNode
					FLOAT fTruckHead
					VEHICLE_INDEX vehTruck
					IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					
						vTruckPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
						//Try and find a vehicle node near to the player.
						IF NOT GET_RANDOM_VEHICLE_NODE(vTruckPos, 25.0, 1, FALSE, FALSE, vTruckPos, iNode)
							//Failed to find a vehicle node. Use towing blip location instead.
							vTruckPos = <<368.3565, -1547.1593, 28.3398>>// GET_STATIC_BLIP_POSITION(STATIC_BLIP_MINIGAME_TOWING_TONYA)
							fTruckHead = 50.8682
						ELSE
							GET_NTH_CLOSEST_VEHICLE_NODE_WITH_HEADING(vTruckPos, 1, vTruckPos, fTruckHead, iNumLanes, NF_NONE)							
						ENDIF
		
						vehTruck = CREATE_VEHICLE(TOWTRUCK, vTruckPos, fTruckHead)
						IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
							SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehTruck)
						ENDIF
						SET_VEHICLE_AS_NO_LONGER_NEEDED(vehTruck)
					ENDIF
					SET_MODEL_AS_NO_LONGER_NEEDED(TOWTRUCK)
				ENDIF
				
				
			ELIF ARE_STRINGS_EQUAL(sTowingID, "TOWT")
				// Trying to spawn the tow truck.
				REQUEST_MODEL(TOWTRUCK)
				WHILE NOT HAS_MODEL_LOADED(TOWTRUCK)
					WAIT(0)
				ENDWHILE
				
				// Create the taxi and then set it as no longer needed.
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					VECTOR vTowtruckPos
					vTowtruckPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), <<2.0, 3.0, 0.75>>)
					vehTowTruck = CREATE_VEHICLE(TOWTRUCK, vTowtruckPos, GET_ENTITY_HEADING(PLAYER_PED_ID()))
				ENDIF
				SET_VEHICLE_AS_NO_LONGER_NEEDED(vehTowTruck)
				SET_MODEL_AS_NO_LONGER_NEEDED(TOWTRUCK)
			ELSE
				IF ARE_STRINGS_EQUAL(sTowingID, "OJTWB")
					// Trying to launch the Broken Down variant.
					SET_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_Debug_BrokenDown)
				ELIF ARE_STRINGS_EQUAL(sTowingID, "OJTWT")
					// Trying to launch the Train variant.
					SET_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_Debug_Train)
				ELIF ARE_STRINGS_EQUAL(sTowingID, "OJTWH")
					// Trying to launch the Handicap variant.
					SET_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_Debug_Handicap)
				ELIF ARE_STRINGS_EQUAL(sTowingID, "OJTWA")
					// Trying to launch the Abandoned variant.
					SET_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_Debug_Abandoned)
				ELIF ARE_STRINGS_EQUAL(sTowingID, "OJTWN")
					// Trying to launch the Accident variant.
					SET_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_Debug_Accident)
				ELIF ARE_STRINGS_EQUAL(sTowingID, "OJTWS")
					// Trying to launch the "selection" variant.
					SET_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_Debug_Selection)
				ENDIF
				SET_BITMASK_AS_ENUM(g_savedGlobals.sTowingData.iBools, TOW_GLOBAL_LaunchedViaDebug)
				
				// Potentially need to spawna truck,,,
				BOOL bNeedTruck
				bNeedTruck = TRUE
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						IF (GET_ENTITY_MODEL(GET_ENTITY_FROM_PED_OR_VEHICLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))) = TOWTRUCK)
							bNeedTruck = FALSE
						ENDIF
					ENDIF
				ENDIF
				
				IF bNeedTruck
					CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
				
					REQUEST_MODEL(TOWTRUCK)
					WHILE NOT HAS_MODEL_LOADED(TOWTRUCK)
						WAIT(0)
					ENDWHILE
					
					// Create the truck and then set it as no longer needed.
					VECTOR vTruckPos
					INT iNode
					FLOAT fTruckHead
					VEHICLE_INDEX vehTruck
					IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					
						vTruckPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
						//Try and find a vehicle node near to the player.
						IF NOT GET_RANDOM_VEHICLE_NODE(vTruckPos, 25.0, 1, FALSE, FALSE, vTruckPos, iNode)
							//Failed to find a vehicle node. Use towing blip location instead.
							vTruckPos = <<368.3565, -1547.1593, 28.3398>>// GET_STATIC_BLIP_POSITION(STATIC_BLIP_MINIGAME_TOWING_TONYA)
							fTruckHead = 50.8682
						ELSE
							GET_NTH_CLOSEST_VEHICLE_NODE_WITH_HEADING(vTruckPos, 1, vTruckPos, fTruckHead, iNumLanes, NF_NONE)							
						ENDIF
		
						vehTruck = CREATE_VEHICLE(TOWTRUCK, vTruckPos, fTruckHead)
						IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
							SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehTruck)
						ENDIF
						SET_VEHICLE_AS_NO_LONGER_NEEDED(vehTruck)
					ENDIF
					SET_MODEL_AS_NO_LONGER_NEEDED(TOWTRUCK)
				ENDIF
			ENDIF
		BREAK
		
		CASE SPCID_TRIATHLON
			TEXT_LABEL_31 sMissID1
			sMissID1 = sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].sMissionID
			
			IF ARE_STRINGS_EQUAL(sMissID1, "MGTRgf")
				// We're only trying to activate it.
				g_savedGlobals.sTriathlonData.eCurrentRaceUnlocked = TRIATHLON_RACE_VESPUCCI
			ELSE
//				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
//						SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_TORSO,	13, 0)
//						SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_LEG,		12, 0)
//						SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_FEET,		8, 	0)
//						                            
//					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR                     
//						SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_TORSO,	10, 0)
//						SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_LEG, 		10, 0)
//						SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_FEET, 	4, 	0)
//						                            
//					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN                   
//						SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_TORSO,	5, 	0)
//						SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_LEG, 		5, 	0)
//						SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_FEET, 	3, 	0)
//					ENDIF
//				ENDIF
			
				// We have force-activated the other races, handle that.
				g_savedGlobals.sTriathlonData.eCurrentRaceUnlocked = TRIATHLON_RACE_IRONMAN
				
				SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_TRIATHLON2,	TRUE)
				SET_STATIC_BLIP_LONG_RANGE(STATIC_BLIP_MINIGAME_TRIATHLON2)
				SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_TRIATHLON3,	TRUE)
				SET_STATIC_BLIP_LONG_RANGE(STATIC_BLIP_MINIGAME_TRIATHLON3)
			ENDIF
			
			Execute_Code_ID(CID_ACTIVATE_MINIGAME_TRIATHLON)
		BREAK

		CASE SPCID_SEA_RACE

			// Flag flow bitset for the minigame.
			SET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_MINIGAME_ACTIVE, ENUM_TO_INT(MINIGAME_SEA_RACES), TRUE)

			// Setup blips
			SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_SEA_RACE1, TRUE)
			SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_SEA_RACE2, TRUE)
			SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_SEA_RACE3, TRUE)
			SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_SEA_RACE4, TRUE)

			// Clear leave area checks
			CLEAR_BIT(g_savedGlobals.sSeaRaceData.iRaceLeaveArea, ENUM_TO_INT(SEA_RACE_EAST))
			CLEAR_BIT(g_savedGlobals.sSeaRaceData.iRaceLeaveArea, ENUM_TO_INT(SEA_RACE_NORTH))
			CLEAR_BIT(g_savedGlobals.sSeaRaceData.iRaceLeaveArea, ENUM_TO_INT(SEA_RACE_CANYON))
			CLEAR_BIT(g_savedGlobals.sSeaRaceData.iRaceLeaveArea, ENUM_TO_INT(SEA_RACE_CITY))

			// Spawn player onto a jet-ski
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				
				BOOL bNeedBoat
				bNeedBoat = TRUE
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					IF IS_THIS_MODEL_A_BOAT(GET_ENTITY_MODEL(GET_ENTITY_FROM_PED_OR_VEHICLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))))
						bNeedBoat = FALSE
					ENDIF
				ENDIF
				
				IF bNeedBoat
					REQUEST_MODEL(SEASHARK)
					WHILE NOT HAS_MODEL_LOADED(SEASHARK)
						WAIT(0)
					ENDWHILE
					
					// Create the boat and then set it as no longer needed.
					VEHICLE_INDEX vehBoat
					IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
						vehBoat = CREATE_VEHICLE(SEASHARK, GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_HEADING(PLAYER_PED_ID()))
						SET_MODEL_AS_NO_LONGER_NEEDED(SEASHARK)
						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehBoat)
						SET_VEHICLE_AS_NO_LONGER_NEEDED(vehBoat)
					ENDIF
				ENDIF
			ENDIF
			
			// Launch controller race script
			REQUEST_SCRIPT("controller_Races")
			WHILE NOT HAS_SCRIPT_LOADED("controller_Races")
				REQUEST_SCRIPT("controller_Races")
				WAIT(0)
			ENDWHILE
			
			START_NEW_SCRIPT("controller_Races", MICRO_STACK_SIZE)
			SET_SCRIPT_AS_NO_LONGER_NEEDED("controller_Races")
		BREAK
	
		CASE SPCID_SPECIAL_PED
			
			// Restore all special peds
			g_savedGlobals.sSpecialPedData.ePedsKilled = SP_NONE
		
			// Reset world points
			REACTIVATE_ALL_WORLD_BRAINS_THAT_ARE_WAITING_TILL_OUT_OF_RANGE()
		BREAK
		
		CASE SPCID_STREET_RACE

			// Flag flow bitset for the minigame.
			SET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_MINIGAME_ACTIVE, ENUM_TO_INT(MINIGAME_STREET_RACES), TRUE)
			
			// Street race is being triggered via the debug menu
			g_RaceLaunchedFromDebug = TRUE
		
			// Set to correct time of day
			SET_CLOCK_TIME(20, 0, 0)
		
			// Always unlock the first two street races
			SET_BIT(g_savedGlobals.sStreetRaceData.iRaceUnlocked, ENUM_TO_INT(STREET_RACE_LOS_SANTOS))
			CLEAR_BIT(g_savedGlobals.sStreetRaceData.iRaceLeaveArea, ENUM_TO_INT(STREET_RACE_LOS_SANTOS))
			SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_STREET_RACE1,	TRUE)
			
			SET_BIT(g_savedGlobals.sStreetRaceData.iRaceUnlocked, ENUM_TO_INT(STREET_RACE_CITY_CIRCUIT))
			CLEAR_BIT(g_savedGlobals.sStreetRaceData.iRaceLeaveArea, ENUM_TO_INT(STREET_RACE_CITY_CIRCUIT))
			SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_STREET_RACE2,	TRUE)
			
			// Get value from debug menu to work out the selected race
			sMissID = sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].sMissionID
			
			// Setup which additional races should be unlocked
			IF ARE_STRINGS_EQUAL(sMissID, "MGSR6")
				
				SET_BIT(g_savedGlobals.sStreetRaceData.iRaceUnlocked, ENUM_TO_INT(STREET_RACE_AIRPORT))
				CLEAR_BIT(g_savedGlobals.sStreetRaceData.iRaceLeaveArea, ENUM_TO_INT(STREET_RACE_AIRPORT))
				SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_STREET_RACE4,	TRUE)
				
				SET_BIT(g_savedGlobals.sStreetRaceData.iRaceUnlocked, ENUM_TO_INT(STREET_RACE_FREEWAY))
				CLEAR_BIT(g_savedGlobals.sStreetRaceData.iRaceLeaveArea, ENUM_TO_INT(STREET_RACE_FREEWAY))
				SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_STREET_RACE5,	TRUE)
				
				SET_BIT(g_savedGlobals.sStreetRaceData.iRaceUnlocked, ENUM_TO_INT(STREET_RACE_VESPUCCI_CANALS))
				CLEAR_BIT(g_savedGlobals.sStreetRaceData.iRaceLeaveArea, ENUM_TO_INT(STREET_RACE_VESPUCCI_CANALS))
				SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_STREET_RACE6,	TRUE)
				
			ELIF ARE_STRINGS_EQUAL(sMissID, "MGSR5")
			
				SET_BIT(g_savedGlobals.sStreetRaceData.iRaceUnlocked, ENUM_TO_INT(STREET_RACE_AIRPORT))
				CLEAR_BIT(g_savedGlobals.sStreetRaceData.iRaceLeaveArea, ENUM_TO_INT(STREET_RACE_AIRPORT))
				SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_STREET_RACE4,	TRUE)
				
				SET_BIT(g_savedGlobals.sStreetRaceData.iRaceUnlocked, ENUM_TO_INT(STREET_RACE_FREEWAY))
				CLEAR_BIT(g_savedGlobals.sStreetRaceData.iRaceLeaveArea, ENUM_TO_INT(STREET_RACE_FREEWAY))
				SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_STREET_RACE5,	TRUE)
				
			ELIF ARE_STRINGS_EQUAL(sMissID, "MGSR4")
				SET_BIT(g_savedGlobals.sStreetRaceData.iRaceUnlocked, ENUM_TO_INT(STREET_RACE_AIRPORT))
				CLEAR_BIT(g_savedGlobals.sStreetRaceData.iRaceLeaveArea, ENUM_TO_INT(STREET_RACE_AIRPORT))
				SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_STREET_RACE4,	TRUE)
			ENDIF
			
			// Give enough cash for the race fee
			CREDIT_BANK_ACCOUNT(GET_CURRENT_PLAYER_PED_ENUM(), BAAC_UNLOGGED_SMALL_ACTION, 1500)

			// Warp player into bike if the race is Vespucci Canals
			IF ARE_STRINGS_EQUAL(sMissID, "MGSR6")
				
				// Request bike
				REQUEST_MODEL(BATI)
				WHILE NOT HAS_MODEL_LOADED(BATI)
					WAIT(0)
				ENDWHILE
				
				// Take out of existing vehicle and place onto bike
				IF IS_PLAYER_PLAYING(PLAYER_ID())
	
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						VEHICLE_INDEX oldCar 
						oldCar = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
						IF DOES_ENTITY_EXIST(oldCar)
							IF NOT IS_ENTITY_A_MISSION_ENTITY(oldCar)
								SET_ENTITY_AS_MISSION_ENTITY(oldCar)
							ENDIF
							SET_ENTITY_COORDS(PLAYER_PED_ID(), GET_ENTITY_COORDS(PLAYER_PED_ID()))
							DELETE_VEHICLE(oldCar)
						ENDIF
					ENDIF
					
					VEHICLE_INDEX newCar 
					newCar = CREATE_VEHICLE(BATI, GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_HEADING(PLAYER_PED_ID()))
					IF DOES_ENTITY_EXIST(newCar)
						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), newCar)
						SET_VEHICLE_AS_NO_LONGER_NEEDED(newCar)
					ENDIF
				ENDIF

				// Release model
				SET_MODEL_AS_NO_LONGER_NEEDED(BATI)
			ENDIF
			
			// Launch controller race script
			REQUEST_SCRIPT("controller_Races")
			WHILE NOT HAS_SCRIPT_LOADED("controller_Races")
				WAIT(0)
			ENDWHILE
			START_NEW_SCRIPT("controller_Races", MICRO_STACK_SIZE)
			SET_SCRIPT_AS_NO_LONGER_NEEDED("controller_Races")
		BREAK
				
		CASE SPCID_STUNT_RACE
			// Turn stunt races on.
			Execute_Code_ID(CID_ACTIVATE_MINIGAME_STUNT_PLANES)
			SET_BITMASK_AS_ENUM(g_savedGlobals.sSPTTData.iFlags, SPTT_BITFLAG_LaunchedViaDebug)
		BREAK
		
		CASE SPCID_RESET_EPSILON_TRACT
			RESET_ALL_PACKAGE_FLAGS_TO_ZERO(SCRAP_TRACT)
		BREAK

		CASE SPCID_RESET_LETTER_FLAGS
			RESET_ALL_PACKAGE_FLAGS_TO_ZERO(SCRAP_LETTER)
		BREAK
		
		CASE SPCID_RESET_DIVING_FLAGS
			
			// Reset collectables
			RESET_ALL_PACKAGE_FLAGS_TO_ZERO(SCRAP_DIVING)
			
			// Request dinghy
			REQUEST_MODEL(DINGHY)
			WHILE NOT HAS_MODEL_LOADED(DINGHY)
				WAIT(0)
			ENDWHILE
			
			// Take out of existing vehicle...
			IF IS_PLAYER_PLAYING(PLAYER_ID())

				// Remove car
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					VEHICLE_INDEX viOld 
					viOld = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					IF DOES_ENTITY_EXIST(viOld)
						IF NOT IS_ENTITY_A_MISSION_ENTITY(viOld)
							SET_ENTITY_AS_MISSION_ENTITY(viOld)
						ENDIF
						SET_ENTITY_COORDS(PLAYER_PED_ID(), GET_ENTITY_COORDS(PLAYER_PED_ID()))
						DELETE_VEHICLE(viOld)
					ENDIF
				ENDIF
			ENDIF
				
			WAIT(0)
			
			// ... and into the dinghy!
			IF IS_PLAYER_PLAYING(PLAYER_ID())	
				VEHICLE_INDEX viBoat
				viBoat = CREATE_VEHICLE(DINGHY, GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_HEADING(PLAYER_PED_ID()))
				IF DOES_ENTITY_EXIST(viBoat)
					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), viBoat)
					SET_VEHICLE_AS_NO_LONGER_NEEDED(viBoat)
				ENDIF
			ENDIF

			// Release model
			SET_MODEL_AS_NO_LONGER_NEEDED(DINGHY)
		BREAK
		
		CASE SPCID_RESET_HUNTING_FLAGS
			g_savedGlobals.sAmbient.iHighScoreFreeMode = 0
			g_savedGlobals.sAmbient.iChallengeRankFreeMode[0] = 0
			g_savedGlobals.sAmbient.iChallengeRankFreeMode[1] = 0
			g_savedGlobals.sAmbient.iChallengeRankFreeMode[2] = 0
		BREAK
		
		CASE SPCID_RESET_SIGN_FLAGS
			g_savedGlobals.sAmbient.iForSaleSignDestroyedFlags = 0
		BREAK
		
		CASE SPCID_RESET_SPACESHIP_FLAGS
			RESET_ALL_PACKAGE_FLAGS_TO_ZERO(SCRAP_SHIP)
		BREAK
		
		CASE SPCID_RESET_SUBMARINE_FLAGS
			
			// Reset collectables
			RESET_ALL_PACKAGE_FLAGS_TO_ZERO(SCRAP_SUB)
			
			// Request dinghy
			REQUEST_MODEL(SUBMERSIBLE)
			WHILE NOT HAS_MODEL_LOADED(SUBMERSIBLE)
				WAIT(0)
			ENDWHILE
				
			// Take out of existing vehicle and place into dinghy
			IF IS_PLAYER_PLAYING(PLAYER_ID())

				// Remove car
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					VEHICLE_INDEX viOld 
					viOld = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					IF DOES_ENTITY_EXIST(viOld)
						IF NOT IS_ENTITY_A_MISSION_ENTITY(viOld)
							SET_ENTITY_AS_MISSION_ENTITY(viOld)
						ENDIF
						SET_ENTITY_COORDS(PLAYER_PED_ID(), GET_ENTITY_COORDS(PLAYER_PED_ID()))
						DELETE_VEHICLE(viOld)
					ENDIF
				ENDIF
			ENDIF
					
			WAIT(0)
			
			// ... and into the submersible!
			IF IS_PLAYER_PLAYING(PLAYER_ID())
				VEHICLE_INDEX viSub
				viSub = CREATE_VEHICLE(SUBMERSIBLE, GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_HEADING(PLAYER_PED_ID()))
				IF DOES_ENTITY_EXIST(viSub)
					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), viSub)
					SET_VEHICLE_AS_NO_LONGER_NEEDED(viSub)
				ENDIF
			ENDIF

			// Release model
			SET_MODEL_AS_NO_LONGER_NEEDED(SUBMERSIBLE)
		BREAK
		
		CASE SPCID_ST_COORD_SENDER
			g_bSendCoords = TRUE
			g_bRead_coordinates = FALSE
		BREAK
		
		CASE SPCID_ST_COORD_RECEIVER	
			g_bSendCoords = FALSE
			g_bRead_coordinates = TRUE
		BREAK
		
		CASE SPCID_MRKT_FAMILY_EVENTS
			IF NOT (IS_BIT_SET(m_sharedDebugFlags, DBG_F8_SCREEN_ON))
				SET_BIT(m_sharedDebugFlags, DBG_F8_SCREEN_ON)
				
				PRINTSTRING("...F8 SCREEN ON")
				PRINTNL()
			ENDIF
		BREAK
				
		CASE SPCID_CARAPP			
			Set_Savehouse_Respawn_Available(SAVEHOUSE_MICHAEL_BH, TRUE)
			Set_Savehouse_Respawn_Available(SAVEHOUSE_FRANKLIN_SC, TRUE)
			Set_Savehouse_Respawn_Available(SAVEHOUSE_TREVOR_CS, TRUE)					
			g_bVehicleGenAvailableInDebug = TRUE
		BREAK
		
		CASE SPCID_UNLOCK_CHOP_TRICKS
			g_bDebugAppScreenOverride = TRUE
			g_bDebugChopPlayedStatusOverride = TRUE
			g_bUnlockAllChopTricks = TRUE
		BREAK
		
		CASE SPCID_LOAD_PROLOGUE_MAP
		
			IF IS_CUTSCENE_PLAYING()
				STOP_CUTSCENE()
			ENDIF
			
			//IPL Groups
			REQUEST_IPL("prologue01")
			REQUEST_IPL("prologue02")
			REQUEST_IPL("prologue03")
			REQUEST_IPL("prologue04")
			REQUEST_IPL("prologue05")
			REQUEST_IPL("prologue06")
			REQUEST_IPL("prologuerd")
			
			REQUEST_IPL("Prologue01c")
			REQUEST_IPL("Prologue01d")
			REQUEST_IPL("Prologue01e")
			REQUEST_IPL("Prologue01f")
			REQUEST_IPL("Prologue01g")
			REQUEST_IPL("prologue01h")
			REQUEST_IPL("prologue01i")
			REQUEST_IPL("prologue01j")
			REQUEST_IPL("prologue01k")
			REQUEST_IPL("prologue01z")
			REQUEST_IPL("prologue03b")
			REQUEST_IPL("prologue04b")
			REQUEST_IPL("prologue05b")
			REQUEST_IPL("prologue06b")
			REQUEST_IPL("prologuerdb")
			REQUEST_IPL("prologue_occl")
			
			//Minimap
			SET_MINIMAP_IN_PROLOGUE_AND_HANDLE_STATIC_BLIPS(TRUE)
			g_bPrologueMapLoaded = TRUE
		BREAK
		
		CASE SPCID_LOAD_MAIN_MAP
		
			IF IS_CUTSCENE_PLAYING()
				STOP_CUTSCENE()
			ENDIF
			
			//IPL Groups
			REMOVE_IPL("prologue01")
			REMOVE_IPL("prologue02")
			REMOVE_IPL("prologue03")
			REMOVE_IPL("prologue04")
			REMOVE_IPL("prologue05")
			REMOVE_IPL("prologue06")
			REMOVE_IPL("prologuerd")
			
			REMOVE_IPL("Prologue01c")
			REMOVE_IPL("Prologue01d")
			REMOVE_IPL("Prologue01e")
			REMOVE_IPL("Prologue01f")
			REMOVE_IPL("Prologue01g")
			REMOVE_IPL("prologue01h")
			REMOVE_IPL("prologue01i")
			REMOVE_IPL("prologue01j")
			REMOVE_IPL("prologue01k")
			REMOVE_IPL("prologue01z")
			REMOVE_IPL("prologue03b")
			REMOVE_IPL("prologue04b")
			REMOVE_IPL("prologue05b")
			REMOVE_IPL("prologue06b")
			REMOVE_IPL("prologuerdb")
			REMOVE_IPL("prologue_occl")
			
			REMOVE_IPL("prologue06_int")
			
			//Minimap
			SET_MINIMAP_IN_PROLOGUE_AND_HANDLE_STATIC_BLIPS(FALSE)
			g_bPrologueMapLoaded = FALSE
		BREAK
		
		CASE SPCID_GIVE_PARACHUTE
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), GADGETTYPE_PARACHUTE)
					GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), GADGETTYPE_PARACHUTE, 1)
					IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE SPCID_MILLIONAIRE
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				CREDIT_BANK_ACCOUNT(GET_CURRENT_PLAYER_PED_ENUM(), BAAC_LESTER, 10000000)
			ENDIF
		BREAK
		
		CASE SPCID_FULL_PLAYER_STATS
			INT iTempStat
			REPEAT NUMBER_OF_PLAYER_STATS iTempStat
				g_iDebugPlayerStatOffset[iTempStat] = 100
			ENDREPEAT
		BREAK
		
		CASE SPCID_GIVE_PLAYER_EVERYTHING
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				// HEALTH
				SET_ENTITY_HEALTH(PLAYER_PED_ID(), 1000)
				ADD_ARMOUR_TO_PED(PLAYER_PED_ID(), 1000)
				SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), TRUE)
				
				// SPECIAL ABILITY
				SPECIAL_ABILITY_UNLOCK(GET_ENTITY_MODEL(PLAYER_PED_ID()))
				SPECIAL_ABILITY_FILL_METER(PLAYER_ID(), TRUE)
				
				// PARACHUTE
				IF NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), GADGETTYPE_PARACHUTE)
					GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), GADGETTYPE_PARACHUTE, 1)
				ENDIF
				
				// WEAPONS
				// Pistols
				GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_PISTOL, -1, FALSE, FALSE)
				GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_COMBATPISTOL, -1, FALSE, FALSE)
				GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_APPISTOL, -1, FALSE, FALSE)

				// Sub machine guns
				GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_MICROSMG, -1, FALSE, FALSE)
				GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_SMG, -1, FALSE, FALSE)

				// Assault rifles
				GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_ASSAULTRIFLE, -1, FALSE, FALSE)
				GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_CARBINERIFLE, -1, TRUE, TRUE)
				GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_ADVANCEDRIFLE, -1, FALSE, FALSE)

				// Light machine guns
				GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_MG, -1, FALSE, FALSE)
				GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_COMBATMG, -1, FALSE, FALSE)

				// Shotguns
				GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_PUMPSHOTGUN, -1, FALSE, FALSE)
				GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_SAWNOFFSHOTGUN, -1, FALSE, FALSE)
				GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_ASSAULTSHOTGUN, -1, FALSE, FALSE)

				// Sniper rifles
				GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_SNIPERRIFLE, -1, FALSE, FALSE)
				GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_HEAVYSNIPER, -1, FALSE, FALSE)
				GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_REMOTESNIPER, -1, FALSE, FALSE)

				// Heavy weapons
				GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_GRENADELAUNCHER, -1, FALSE, FALSE)
				GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_GRENADELAUNCHER_SMOKE, -1, FALSE, FALSE)
				GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_RPG, -1, FALSE, FALSE)
				GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_MINIGUN, -1, FALSE, FALSE)

				// Thrown weapons   
				GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_GRENADE, -1, FALSE, FALSE)
				GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_SMOKEGRENADE, -1, FALSE, FALSE)
				GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_STICKYBOMB, -1, FALSE, FALSE)
				GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_MOLOTOV, -1, FALSE, FALSE)

				// Special weapons
				GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_STUNGUN, -1, FALSE, FALSE)
				GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_PETROLCAN, -1, FALSE, FALSE)
				//GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_DLC_LOUDHAILER, -1, FALSE, FALSE)
				GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_DIGISCANNER, -1, FALSE, FALSE)
//				GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_CAMERA, -1, FALSE, FALSE)

				// Melee Weapons
				GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_KNIFE, -1, FALSE, FALSE)
				GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_NIGHTSTICK, -1, FALSE, FALSE)
				GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_HAMMER, -1, FALSE, FALSE)
				GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_BAT, -1, FALSE, FALSE)
				GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_CROWBAR, -1, FALSE, FALSE)
			ENDIF
		BREAK
		
		CASE SPCID_BAILBOND_DEBUG_QUARRY
			
			IF IS_PLAYER_PLAYING(PLAYER_ID())
				CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
			ENDIF
	
			//set Maude as complete
			Activate_RC_Mission(RC_MAUDE_1, TRUE)	
			
			// terminate after simulating flow to completion of Maude 1, since that will launch BailBond_Launcher
			TERMINATE_ALL_SCRIPTS_WITH_THIS_NAME("BailBond_Launcher")	
			
			// set mission passed states of all the bail bonds to match the debug skip
			CLEAR_BIT(g_savedGlobals.sBailBondData.iLauncherBitFlags, ENUM_TO_INT(BBL_BIT_FLAG_COMPLETED_QUARRY))
			CLEAR_BIT(g_savedGlobals.sBailBondData.iLauncherBitFlags, ENUM_TO_INT(BBL_BIT_FLAG_COMPLETED_FARM))
			CLEAR_BIT(g_savedGlobals.sBailBondData.iLauncherBitFlags, ENUM_TO_INT(BBL_BIT_FLAG_COMPLETED_MOUNTAIN))
			CLEAR_BIT(g_savedGlobals.sBailBondData.iLauncherBitFlags, ENUM_TO_INT(BBL_BIT_FLAG_COMPLETED_HOBO))
			
			// ensure Bail Bond will launch for playing already being within range
			SET_BIT(g_savedGlobals.sBailBondData.iLauncherBitFlags, ENUM_TO_INT(BBL_BIT_FLAG_DONE_INITIAL_LEAVE_AREA_CHECK))
			
			// ensure Bail Bond will wait in launch state until safe to do so
			SET_BIT(g_savedGlobals.sBailBondData.iLauncherBitFlags, ENUM_TO_INT(BBL_BIT_FLAG_DEBUG_LAUNCHED))
			
			// set the current mission for the launcher
			g_savedGlobals.sBailBondData.iCurrentMission = ENUM_TO_INT(BBI_QUARRY)
			
			// override the saved launcher state to go straight to send the email stage
			g_savedGlobals.sBailBondData.iLauncherState = 0
			
			REQUEST_SCRIPT("BailBond_Launcher")
			WHILE NOT HAS_SCRIPT_LOADED("BailBond_Launcher")
					WAIT(0)
			ENDWHILE
			START_NEW_SCRIPT("BailBond_Launcher",DEFAULT_STACK_SIZE)
			SET_SCRIPT_AS_NO_LONGER_NEEDED("BailBond_Launcher")
		BREAK
		
		CASE SPCID_BAILBOND_DEBUG_FARM
			
			IF IS_PLAYER_PLAYING(PLAYER_ID())
				CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
			ENDIF
			
			//set Maude as complete
			Activate_RC_Mission(RC_MAUDE_1, TRUE)	
			
			// terminate after simulating flow to completion of Maude 1, since that will launch BailBond_Launcher
			TERMINATE_ALL_SCRIPTS_WITH_THIS_NAME("BailBond_Launcher")	
			
			// set mission passed states of all the bail bonds to match the debug skip
			SET_BIT(g_savedGlobals.sBailBondData.iLauncherBitFlags, ENUM_TO_INT(BBL_BIT_FLAG_COMPLETED_QUARRY))
			CLEAR_BIT(g_savedGlobals.sBailBondData.iLauncherBitFlags, ENUM_TO_INT(BBL_BIT_FLAG_COMPLETED_FARM))
			CLEAR_BIT(g_savedGlobals.sBailBondData.iLauncherBitFlags, ENUM_TO_INT(BBL_BIT_FLAG_COMPLETED_MOUNTAIN))
			CLEAR_BIT(g_savedGlobals.sBailBondData.iLauncherBitFlags, ENUM_TO_INT(BBL_BIT_FLAG_COMPLETED_HOBO))			
			
			// ensure Bail Bond will launch for playing already being within range
			SET_BIT(g_savedGlobals.sBailBondData.iLauncherBitFlags, ENUM_TO_INT(BBL_BIT_FLAG_DONE_INITIAL_LEAVE_AREA_CHECK))
			
			// ensure Bail Bond will wait in launch state until safe to do so
			SET_BIT(g_savedGlobals.sBailBondData.iLauncherBitFlags, ENUM_TO_INT(BBL_BIT_FLAG_DEBUG_LAUNCHED))
			
			// set the current mission for the launcher
			g_savedGlobals.sBailBondData.iCurrentMission = ENUM_TO_INT(BBI_FARM)
			
			// override the saved launcher state to go straight to send the email stage
			g_savedGlobals.sBailBondData.iLauncherState = 0

			REQUEST_SCRIPT("BailBond_Launcher")
			WHILE NOT HAS_SCRIPT_LOADED("BailBond_Launcher")
					WAIT(0)
			ENDWHILE
			START_NEW_SCRIPT("BailBond_Launcher",DEFAULT_STACK_SIZE)
			SET_SCRIPT_AS_NO_LONGER_NEEDED("BailBond_Launcher")
		BREAK
		
		CASE SPCID_BAILBOND_DEBUG_MOUNTAIN
		
			IF IS_PLAYER_PLAYING(PLAYER_ID())
				CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
			ENDIF
			
			//set Maude as complete
			Activate_RC_Mission(RC_MAUDE_1, TRUE)	
			
			// terminate after simulating flow to completion of Maude 1, since that will launch BailBond_Launcher
			TERMINATE_ALL_SCRIPTS_WITH_THIS_NAME("BailBond_Launcher")		
			
			// set mission passed states of all the bail bonds to match the debug skip
			SET_BIT(g_savedGlobals.sBailBondData.iLauncherBitFlags, ENUM_TO_INT(BBL_BIT_FLAG_COMPLETED_QUARRY))
			SET_BIT(g_savedGlobals.sBailBondData.iLauncherBitFlags, ENUM_TO_INT(BBL_BIT_FLAG_COMPLETED_FARM))
			CLEAR_BIT(g_savedGlobals.sBailBondData.iLauncherBitFlags, ENUM_TO_INT(BBL_BIT_FLAG_COMPLETED_MOUNTAIN))
			CLEAR_BIT(g_savedGlobals.sBailBondData.iLauncherBitFlags, ENUM_TO_INT(BBL_BIT_FLAG_COMPLETED_HOBO))		
			
			// ensure Bail Bond will launch for playing already being within range
			SET_BIT(g_savedGlobals.sBailBondData.iLauncherBitFlags, ENUM_TO_INT(BBL_BIT_FLAG_DONE_INITIAL_LEAVE_AREA_CHECK))
			
			// ensure Bail Bond will wait in launch state until safe to do so
			SET_BIT(g_savedGlobals.sBailBondData.iLauncherBitFlags, ENUM_TO_INT(BBL_BIT_FLAG_DEBUG_LAUNCHED))
			
			// set the current mission for the launcher
			g_savedGlobals.sBailBondData.iCurrentMission = ENUM_TO_INT(BBI_MOUNTAIN)
			
			// override the saved launcher state to go straight to send the email stage
			g_savedGlobals.sBailBondData.iLauncherState = 0

			REQUEST_SCRIPT("BailBond_Launcher")
			WHILE NOT HAS_SCRIPT_LOADED("BailBond_Launcher")
					WAIT(0)
			ENDWHILE
			START_NEW_SCRIPT("BailBond_Launcher",DEFAULT_STACK_SIZE)
			SET_SCRIPT_AS_NO_LONGER_NEEDED("BailBond_Launcher")
		BREAK
		
		CASE SPCID_BAILBOND_DEBUG_HOBO
		
			IF IS_PLAYER_PLAYING(PLAYER_ID())
				CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
			ENDIF
			
			//set Maude as complete
			Activate_RC_Mission(RC_MAUDE_1, TRUE)	
			
			// terminate after simulating flow to completion of Maude 1, since that will launch BailBond_Launcher
			TERMINATE_ALL_SCRIPTS_WITH_THIS_NAME("BailBond_Launcher")	
			
			// set mission passed states of all the bail bonds to match the debug skip
			SET_BIT(g_savedGlobals.sBailBondData.iLauncherBitFlags, ENUM_TO_INT(BBL_BIT_FLAG_COMPLETED_QUARRY))
			SET_BIT(g_savedGlobals.sBailBondData.iLauncherBitFlags, ENUM_TO_INT(BBL_BIT_FLAG_COMPLETED_FARM))
			SET_BIT(g_savedGlobals.sBailBondData.iLauncherBitFlags, ENUM_TO_INT(BBL_BIT_FLAG_COMPLETED_MOUNTAIN))
			CLEAR_BIT(g_savedGlobals.sBailBondData.iLauncherBitFlags, ENUM_TO_INT(BBL_BIT_FLAG_COMPLETED_HOBO))		
			
			// ensure Bail Bond will launch for playing already being within range
			SET_BIT(g_savedGlobals.sBailBondData.iLauncherBitFlags, ENUM_TO_INT(BBL_BIT_FLAG_DONE_INITIAL_LEAVE_AREA_CHECK))
			
			// ensure Bail Bond will wait in launch state until safe to do so
			SET_BIT(g_savedGlobals.sBailBondData.iLauncherBitFlags, ENUM_TO_INT(BBL_BIT_FLAG_DEBUG_LAUNCHED))
			
			// set the current mission for the launcher
			g_savedGlobals.sBailBondData.iCurrentMission = ENUM_TO_INT(BBI_HOBO)
			
			// override the saved launcher state to go straight to send the email stage
			g_savedGlobals.sBailBondData.iLauncherState = 0

			REQUEST_SCRIPT("BailBond_Launcher")
			WHILE NOT HAS_SCRIPT_LOADED("BailBond_Launcher")
					WAIT(0)
			ENDWHILE
			START_NEW_SCRIPT("BailBond_Launcher",DEFAULT_STACK_SIZE)
			SET_SCRIPT_AS_NO_LONGER_NEEDED("BailBond_Launcher")
		BREAK

		CASE SPCID_PROPERTY_MANAGEMENT
			sMissID = sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].sMissionID

			IF ARE_STRINGS_EQUAL(sMissID, "PCSCDEF1")
				g_PropertySystemData.currentPropertyManagementProperty = PROPERTY_CAR_SCRAP_YARD
				g_PropertySystemData.currentPropertyManagementMission = PROPERTY_MANAGEMENT_MISSION_DEFEND
				g_PropertySystemData.iCurrentPropertyManagementVariation = 0
			ELIF ARE_STRINGS_EQUAL(sMissID, "PCSCDEF2")
				g_PropertySystemData.currentPropertyManagementProperty = PROPERTY_CAR_SCRAP_YARD
				g_PropertySystemData.currentPropertyManagementMission = PROPERTY_MANAGEMENT_MISSION_DEFEND
				g_PropertySystemData.iCurrentPropertyManagementVariation = 1
			ELIF ARE_STRINGS_EQUAL(sMissID, "PWEEDEL1")
				g_PropertySystemData.currentPropertyManagementProperty = PROPERTY_WEED_SHOP
				g_PropertySystemData.currentPropertyManagementMission = PROPERTY_MANAGEMENT_MISSION_DELIVERY
				g_PropertySystemData.iCurrentPropertyManagementVariation = 0
			ELIF ARE_STRINGS_EQUAL(sMissID, "PWEEDEL2")
				g_PropertySystemData.currentPropertyManagementProperty = PROPERTY_WEED_SHOP
				g_PropertySystemData.currentPropertyManagementMission = PROPERTY_MANAGEMENT_MISSION_DELIVERY
				g_PropertySystemData.iCurrentPropertyManagementVariation = 1
			ELIF ARE_STRINGS_EQUAL(sMissID, "PWEEDEL3")
				g_PropertySystemData.currentPropertyManagementProperty = PROPERTY_WEED_SHOP
				g_PropertySystemData.currentPropertyManagementMission = PROPERTY_MANAGEMENT_MISSION_DELIVERY
				g_PropertySystemData.iCurrentPropertyManagementVariation = 2
			ELIF ARE_STRINGS_EQUAL(sMissID, "PWEEDEL4")
				g_PropertySystemData.currentPropertyManagementProperty = PROPERTY_WEED_SHOP
				g_PropertySystemData.currentPropertyManagementMission = PROPERTY_MANAGEMENT_MISSION_DELIVERY
				g_PropertySystemData.iCurrentPropertyManagementVariation = 3
			ELIF ARE_STRINGS_EQUAL(sMissID, "PWEEDEL5")
				g_PropertySystemData.currentPropertyManagementProperty = PROPERTY_WEED_SHOP
				g_PropertySystemData.currentPropertyManagementMission = PROPERTY_MANAGEMENT_MISSION_DELIVERY
				g_PropertySystemData.iCurrentPropertyManagementVariation = 4
			ELIF ARE_STRINGS_EQUAL(sMissID, "PWEEDEL6")
				g_PropertySystemData.currentPropertyManagementProperty = PROPERTY_WEED_SHOP
				g_PropertySystemData.currentPropertyManagementMission = PROPERTY_MANAGEMENT_MISSION_DELIVERY
				g_PropertySystemData.iCurrentPropertyManagementVariation = 5
			ELIF ARE_STRINGS_EQUAL(sMissID, "PWEEDEL7")
				g_PropertySystemData.currentPropertyManagementProperty = PROPERTY_WEED_SHOP
				g_PropertySystemData.currentPropertyManagementMission = PROPERTY_MANAGEMENT_MISSION_DELIVERY
				g_PropertySystemData.iCurrentPropertyManagementVariation = 6
			ELIF ARE_STRINGS_EQUAL(sMissID, "OJTX1")
				g_PropertySystemData.currentPropertyManagementProperty = PROPERTY_TAXI_LOT
				g_PropertySystemData.currentPropertyManagementMission = PROPERTY_MANAGEMENT_MISSION_TAXI
				g_PropertySystemData.iCurrentPropertyManagementVariation = 6
			ELIF ARE_STRINGS_EQUAL(sMissID, "OJTX2")
				g_PropertySystemData.currentPropertyManagementProperty = PROPERTY_TAXI_LOT
				g_PropertySystemData.currentPropertyManagementMission = PROPERTY_MANAGEMENT_MISSION_TAXI
				g_PropertySystemData.iCurrentPropertyManagementVariation = 7
			ELIF ARE_STRINGS_EQUAL(sMissID, "OJTX3")
				g_PropertySystemData.currentPropertyManagementProperty = PROPERTY_TAXI_LOT
				g_PropertySystemData.currentPropertyManagementMission = PROPERTY_MANAGEMENT_MISSION_TAXI
				g_PropertySystemData.iCurrentPropertyManagementVariation = 2
			ELIF ARE_STRINGS_EQUAL(sMissID, "OJTX4")
				g_PropertySystemData.currentPropertyManagementProperty = PROPERTY_TAXI_LOT
				g_PropertySystemData.currentPropertyManagementMission = PROPERTY_MANAGEMENT_MISSION_TAXI
				g_PropertySystemData.iCurrentPropertyManagementVariation = 5
			ELIF ARE_STRINGS_EQUAL(sMissID, "OJTX5")
				g_PropertySystemData.currentPropertyManagementProperty = PROPERTY_TAXI_LOT
				g_PropertySystemData.currentPropertyManagementMission = PROPERTY_MANAGEMENT_MISSION_TAXI
				g_PropertySystemData.iCurrentPropertyManagementVariation = 8
			ELIF ARE_STRINGS_EQUAL(sMissID, "OJTX7")
				g_PropertySystemData.currentPropertyManagementProperty = PROPERTY_TAXI_LOT
				g_PropertySystemData.currentPropertyManagementMission = PROPERTY_MANAGEMENT_MISSION_TAXI
				g_PropertySystemData.iCurrentPropertyManagementVariation = 1
			ELIF ARE_STRINGS_EQUAL(sMissID, "OJTX8")
				g_PropertySystemData.currentPropertyManagementProperty = PROPERTY_TAXI_LOT
				g_PropertySystemData.currentPropertyManagementMission = PROPERTY_MANAGEMENT_MISSION_TAXI
				g_PropertySystemData.iCurrentPropertyManagementVariation = 4
			ELIF ARE_STRINGS_EQUAL(sMissID, "OJTX9")
				g_PropertySystemData.currentPropertyManagementProperty = PROPERTY_TAXI_LOT
				g_PropertySystemData.currentPropertyManagementMission = PROPERTY_MANAGEMENT_MISSION_TAXI
				g_PropertySystemData.iCurrentPropertyManagementVariation = 0
			ELIF ARE_STRINGS_EQUAL(sMissID, "OJTX10")
				g_PropertySystemData.currentPropertyManagementProperty = PROPERTY_TAXI_LOT
				g_PropertySystemData.currentPropertyManagementMission = PROPERTY_MANAGEMENT_MISSION_TAXI
				g_PropertySystemData.iCurrentPropertyManagementVariation = 3
			ELIF ARE_STRINGS_EQUAL(sMissID, "PRECSTCIDF")	
				g_PropertySystemData.currentPropertyManagementProperty = PROPERTY_CINEMA_DOWNTOWN
				g_PropertySystemData.currentPropertyManagementMission = PROPERTY_MANAGEMENT_MISSION_RECOVER_STOLEN
				g_PropertySystemData.iCurrentPropertyManagementVariation = 0		
			ELIF ARE_STRINGS_EQUAL(sMissID, "PRECSTCIMF")	
				g_PropertySystemData.currentPropertyManagementProperty = PROPERTY_CINEMA_MORNINGWOOD
				g_PropertySystemData.currentPropertyManagementMission = PROPERTY_MANAGEMENT_MISSION_RECOVER_STOLEN
				g_PropertySystemData.iCurrentPropertyManagementVariation = 0			
			ELIF ARE_STRINGS_EQUAL(sMissID, "PRECSTCIVF")	
				g_PropertySystemData.currentPropertyManagementProperty = PROPERTY_CINEMA_VINEWOOD
				g_PropertySystemData.currentPropertyManagementMission = PROPERTY_MANAGEMENT_MISSION_RECOVER_STOLEN
				g_PropertySystemData.iCurrentPropertyManagementVariation = 0		
			ELIF ARE_STRINGS_EQUAL(sMissID, "PRECSTCIDT")	
				g_PropertySystemData.currentPropertyManagementProperty = PROPERTY_CINEMA_DOWNTOWN
				g_PropertySystemData.currentPropertyManagementMission = PROPERTY_MANAGEMENT_MISSION_RECOVER_STOLEN
				g_PropertySystemData.iCurrentPropertyManagementVariation = 1
			ELIF ARE_STRINGS_EQUAL(sMissID, "PRECSTCIMT")	
				g_PropertySystemData.currentPropertyManagementProperty = PROPERTY_CINEMA_MORNINGWOOD
				g_PropertySystemData.currentPropertyManagementMission = PROPERTY_MANAGEMENT_MISSION_RECOVER_STOLEN
				g_PropertySystemData.iCurrentPropertyManagementVariation = 1
			ELIF ARE_STRINGS_EQUAL(sMissID, "PRECSTCIVT")	
				g_PropertySystemData.currentPropertyManagementProperty = PROPERTY_CINEMA_VINEWOOD
				g_PropertySystemData.currentPropertyManagementMission = PROPERTY_MANAGEMENT_MISSION_RECOVER_STOLEN
				g_PropertySystemData.iCurrentPropertyManagementVariation = 1
			ELIF ARE_STRINGS_EQUAL(sMissID, "PBDELTEQCS")	
				g_PropertySystemData.currentPropertyManagementProperty = PROPERTY_BAR_TEQUILALA
				g_PropertySystemData.currentPropertyManagementMission = PROPERTY_MANAGEMENT_MISSION_DELIVERY
				g_PropertySystemData.iCurrentPropertyManagementVariation = 0	
			ELIF ARE_STRINGS_EQUAL(sMissID, "PMRECBARAT")	
				g_PropertySystemData.currentPropertyManagementProperty = PROPERTY_BAR_TEQUILALA
				g_PropertySystemData.currentPropertyManagementMission = PROPERTY_MANAGEMENT_MISSION_RECOVER_STOLEN
				g_PropertySystemData.iCurrentPropertyManagementVariation = 0	
			ELIF ARE_STRINGS_EQUAL(sMissID, "PMRECBARBT")	
				g_PropertySystemData.currentPropertyManagementProperty = PROPERTY_BAR_PITCHERS
				g_PropertySystemData.currentPropertyManagementMission = PROPERTY_MANAGEMENT_MISSION_RECOVER_STOLEN
				g_PropertySystemData.iCurrentPropertyManagementVariation = 0	
			ELIF ARE_STRINGS_EQUAL(sMissID, "PMRECBARFT")	
				g_PropertySystemData.currentPropertyManagementProperty = PROPERTY_BAR_HEN_HOUSE
				g_PropertySystemData.currentPropertyManagementMission = PROPERTY_MANAGEMENT_MISSION_RECOVER_STOLEN
				g_PropertySystemData.iCurrentPropertyManagementVariation = 0
			ELIF ARE_STRINGS_EQUAL(sMissID, "PMRECBARIT")	
				g_PropertySystemData.currentPropertyManagementProperty = PROPERTY_BAR_HOOKIES
				g_PropertySystemData.currentPropertyManagementMission = PROPERTY_MANAGEMENT_MISSION_RECOVER_STOLEN
				g_PropertySystemData.iCurrentPropertyManagementVariation = 0
			ELIF ARE_STRINGS_EQUAL(sMissID, "PMRECBARAP")	
				g_PropertySystemData.currentPropertyManagementProperty = PROPERTY_BAR_TEQUILALA
				g_PropertySystemData.currentPropertyManagementMission = PROPERTY_MANAGEMENT_MISSION_RECOVER_STOLEN
				g_PropertySystemData.iCurrentPropertyManagementVariation = 1		
			ELIF ARE_STRINGS_EQUAL(sMissID, "PMRECBARBP")	
				g_PropertySystemData.currentPropertyManagementProperty = PROPERTY_BAR_PITCHERS
				g_PropertySystemData.currentPropertyManagementMission = PROPERTY_MANAGEMENT_MISSION_RECOVER_STOLEN
				g_PropertySystemData.iCurrentPropertyManagementVariation = 1		
			ELIF ARE_STRINGS_EQUAL(sMissID, "PMRECBARFP")	
				g_PropertySystemData.currentPropertyManagementProperty = PROPERTY_BAR_HEN_HOUSE
				g_PropertySystemData.currentPropertyManagementMission = PROPERTY_MANAGEMENT_MISSION_RECOVER_STOLEN
				g_PropertySystemData.iCurrentPropertyManagementVariation = 1		
			ELIF ARE_STRINGS_EQUAL(sMissID, "PMRECBARIP")	
				g_PropertySystemData.currentPropertyManagementProperty = PROPERTY_BAR_HOOKIES
				g_PropertySystemData.currentPropertyManagementMission = PROPERTY_MANAGEMENT_MISSION_RECOVER_STOLEN
				g_PropertySystemData.iCurrentPropertyManagementVariation = 1		
			// GANG ATTACK: Stay and fight 
			ELIF ARE_STRINGS_EQUAL(sMissID, "PMGANGBARA")	
				g_PropertySystemData.currentPropertyManagementProperty = PROPERTY_BAR_TEQUILALA
				g_PropertySystemData.currentPropertyManagementMission = PROPERTY_MANAGEMENT_MISSION_GANG_ATTACK
				g_PropertySystemData.iCurrentPropertyManagementVariation = 0	
			ELIF ARE_STRINGS_EQUAL(sMissID, "PMGANGBARB")	
				g_PropertySystemData.currentPropertyManagementProperty = PROPERTY_BAR_PITCHERS
				g_PropertySystemData.currentPropertyManagementMission = PROPERTY_MANAGEMENT_MISSION_GANG_ATTACK
				g_PropertySystemData.iCurrentPropertyManagementVariation = 0	
			ELIF ARE_STRINGS_EQUAL(sMissID, "PMGANGBARF")	
				g_PropertySystemData.currentPropertyManagementProperty = PROPERTY_BAR_HEN_HOUSE
				g_PropertySystemData.currentPropertyManagementMission = PROPERTY_MANAGEMENT_MISSION_GANG_ATTACK
				g_PropertySystemData.iCurrentPropertyManagementVariation = 0
			ELIF ARE_STRINGS_EQUAL(sMissID, "PMGANGBARI")	
				g_PropertySystemData.currentPropertyManagementProperty = PROPERTY_BAR_HOOKIES
				g_PropertySystemData.currentPropertyManagementMission = PROPERTY_MANAGEMENT_MISSION_GANG_ATTACK
				g_PropertySystemData.iCurrentPropertyManagementVariation = 0
			// GANG ATTACK: Flee in vehicles
			ELIF ARE_STRINGS_EQUAL(sMissID, "PMGANGBARA2")	
				g_PropertySystemData.currentPropertyManagementProperty = PROPERTY_BAR_TEQUILALA
				g_PropertySystemData.currentPropertyManagementMission = PROPERTY_MANAGEMENT_MISSION_GANG_ATTACK
				g_PropertySystemData.iCurrentPropertyManagementVariation = 1	
			ELIF ARE_STRINGS_EQUAL(sMissID, "PMGANGBARB2")	
				g_PropertySystemData.currentPropertyManagementProperty = PROPERTY_BAR_PITCHERS
				g_PropertySystemData.currentPropertyManagementMission = PROPERTY_MANAGEMENT_MISSION_GANG_ATTACK
				g_PropertySystemData.iCurrentPropertyManagementVariation = 1	
			ELIF ARE_STRINGS_EQUAL(sMissID, "PMGANGBARF2")	
				g_PropertySystemData.currentPropertyManagementProperty = PROPERTY_BAR_HEN_HOUSE
				g_PropertySystemData.currentPropertyManagementMission = PROPERTY_MANAGEMENT_MISSION_GANG_ATTACK
				g_PropertySystemData.iCurrentPropertyManagementVariation = 1
			ELIF ARE_STRINGS_EQUAL(sMissID, "PMGANGBARI2")	
				g_PropertySystemData.currentPropertyManagementProperty = PROPERTY_BAR_HOOKIES
				g_PropertySystemData.currentPropertyManagementMission = PROPERTY_MANAGEMENT_MISSION_GANG_ATTACK
				g_PropertySystemData.iCurrentPropertyManagementVariation = 1
			ELIF ARE_STRINGS_EQUAL(sMissID, "PMPLACINDT")	
				g_PropertySystemData.currentPropertyManagementProperty = PROPERTY_CINEMA_DOWNTOWN
				g_PropertySystemData.currentPropertyManagementMission = PROPERTY_MANAGEMENT_MISSION_PLANE_PROMOTION
				g_PropertySystemData.iCurrentPropertyManagementVariation = 1
			ELIF ARE_STRINGS_EQUAL(sMissID, "PMPLACINVW")	
				g_PropertySystemData.currentPropertyManagementProperty = PROPERTY_CINEMA_VINEWOOD
				g_PropertySystemData.currentPropertyManagementMission = PROPERTY_MANAGEMENT_MISSION_PLANE_PROMOTION
				g_PropertySystemData.iCurrentPropertyManagementVariation = 1
			ELIF ARE_STRINGS_EQUAL(sMissID, "PMPLACINMW")	
				g_PropertySystemData.currentPropertyManagementProperty = PROPERTY_CINEMA_MORNINGWOOD
				g_PropertySystemData.currentPropertyManagementMission = PROPERTY_MANAGEMENT_MISSION_PLANE_PROMOTION
				g_PropertySystemData.iCurrentPropertyManagementVariation = 1
			ELIF ARE_STRINGS_EQUAL(sMissID, "PBDELTEQM1")	
				g_PropertySystemData.currentPropertyManagementProperty = PROPERTY_BAR_TEQUILALA
				g_PropertySystemData.currentPropertyManagementMission = PROPERTY_MANAGEMENT_MISSION_DELIVERY
				g_PropertySystemData.iCurrentPropertyManagementVariation = 0	// DELIVERY_MISSION_VARIATION_BREAKABLE_CARGO_LIQUOR_MARKET
			ELIF ARE_STRINGS_EQUAL(sMissID, "PBDELTEQM2")
				g_PropertySystemData.currentPropertyManagementProperty = PROPERTY_BAR_TEQUILALA
				g_PropertySystemData.currentPropertyManagementMission = PROPERTY_MANAGEMENT_MISSION_DELIVERY
				g_PropertySystemData.iCurrentPropertyManagementVariation = 1	// DELIVERY_MISSION_VARIATION_BREAKABLE_CARGO_LIQUOR_MARKET_2
			ELIF ARE_STRINGS_EQUAL(sMissID, "PBDELTEQM3")	
				g_PropertySystemData.currentPropertyManagementProperty = PROPERTY_BAR_TEQUILALA
				g_PropertySystemData.currentPropertyManagementMission = PROPERTY_MANAGEMENT_MISSION_DELIVERY
				g_PropertySystemData.iCurrentPropertyManagementVariation = 2	// DELIVERY_MISSION_VARIATION_BREAKABLE_CARGO_LIQUOR_MARKET_3
			ELIF ARE_STRINGS_EQUAL(sMissID, "PBDELTEQP1")
				g_PropertySystemData.currentPropertyManagementProperty = PROPERTY_BAR_TEQUILALA
				g_PropertySystemData.currentPropertyManagementMission = PROPERTY_MANAGEMENT_MISSION_DELIVERY
				g_PropertySystemData.iCurrentPropertyManagementVariation = 3	// DELIVERY_MISSION_VARIATION_BREAKABLE_CARGO_PISSWASSER_1
			ELIF ARE_STRINGS_EQUAL(sMissID, "PBDELTEQP2")	
				g_PropertySystemData.currentPropertyManagementProperty = PROPERTY_BAR_TEQUILALA
				g_PropertySystemData.currentPropertyManagementMission = PROPERTY_MANAGEMENT_MISSION_DELIVERY
				g_PropertySystemData.iCurrentPropertyManagementVariation = 4	// DELIVERY_MISSION_VARIATION_BREAKABLE_CARGO_PISSWASSER_2
				
			ELIF ARE_STRINGS_EQUAL(sMissID, "PBDELPITM1")	
				g_PropertySystemData.currentPropertyManagementProperty = PROPERTY_BAR_PITCHERS
				g_PropertySystemData.currentPropertyManagementMission = PROPERTY_MANAGEMENT_MISSION_DELIVERY
				g_PropertySystemData.iCurrentPropertyManagementVariation = 0	// DELIVERY_MISSION_VARIATION_BREAKABLE_CARGO_LIQUOR_MARKET
			ELIF ARE_STRINGS_EQUAL(sMissID, "PBDELPITM2")
				g_PropertySystemData.currentPropertyManagementProperty = PROPERTY_BAR_PITCHERS
				g_PropertySystemData.currentPropertyManagementMission = PROPERTY_MANAGEMENT_MISSION_DELIVERY
				g_PropertySystemData.iCurrentPropertyManagementVariation = 1	// DELIVERY_MISSION_VARIATION_BREAKABLE_CARGO_LIQUOR_MARKET_2
			ELIF ARE_STRINGS_EQUAL(sMissID, "PBDELPITM3")	
				g_PropertySystemData.currentPropertyManagementProperty = PROPERTY_BAR_PITCHERS
				g_PropertySystemData.currentPropertyManagementMission = PROPERTY_MANAGEMENT_MISSION_DELIVERY
				g_PropertySystemData.iCurrentPropertyManagementVariation = 2	// DELIVERY_MISSION_VARIATION_BREAKABLE_CARGO_LIQUOR_MARKET_3
			ELIF ARE_STRINGS_EQUAL(sMissID, "PBDELPITP1")
				g_PropertySystemData.currentPropertyManagementProperty = PROPERTY_BAR_PITCHERS
				g_PropertySystemData.currentPropertyManagementMission = PROPERTY_MANAGEMENT_MISSION_DELIVERY
				g_PropertySystemData.iCurrentPropertyManagementVariation = 3	// DELIVERY_MISSION_VARIATION_BREAKABLE_CARGO_PISSWASSER_1
			ELIF ARE_STRINGS_EQUAL(sMissID, "PBDELPITP2")	
				g_PropertySystemData.currentPropertyManagementProperty = PROPERTY_BAR_PITCHERS
				g_PropertySystemData.currentPropertyManagementMission = PROPERTY_MANAGEMENT_MISSION_DELIVERY
				g_PropertySystemData.iCurrentPropertyManagementVariation = 4	// DELIVERY_MISSION_VARIATION_BREAKABLE_CARGO_PISSWASSER_2
				
			ELIF ARE_STRINGS_EQUAL(sMissID, "PBDELHENV")
				g_PropertySystemData.currentPropertyManagementProperty = PROPERTY_BAR_HEN_HOUSE
				g_PropertySystemData.currentPropertyManagementMission = PROPERTY_MANAGEMENT_MISSION_DELIVERY
				g_PropertySystemData.iCurrentPropertyManagementVariation = 0	// DELIVERY_MISSION_VARIATION_BREAKABLE_CARGO_VINYARD
			ELIF ARE_STRINGS_EQUAL(sMissID, "PBDELHENC")	
				g_PropertySystemData.currentPropertyManagementProperty = PROPERTY_BAR_HEN_HOUSE
				g_PropertySystemData.currentPropertyManagementMission = PROPERTY_MANAGEMENT_MISSION_DELIVERY
				g_PropertySystemData.iCurrentPropertyManagementVariation = 1	// DELIVERY_MISSION_VARIATION_BREAKABLE_CARGO_COUNTRY
				
			ELIF ARE_STRINGS_EQUAL(sMissID, "PBDELHOOV")
				g_PropertySystemData.currentPropertyManagementProperty = PROPERTY_BAR_HOOKIES
				g_PropertySystemData.currentPropertyManagementMission = PROPERTY_MANAGEMENT_MISSION_DELIVERY
				g_PropertySystemData.iCurrentPropertyManagementVariation = 0	// DELIVERY_MISSION_VARIATION_BREAKABLE_CARGO_VINYARD
			ELIF ARE_STRINGS_EQUAL(sMissID, "PBDELHOOC")	
				g_PropertySystemData.currentPropertyManagementProperty = PROPERTY_BAR_HOOKIES
				g_PropertySystemData.currentPropertyManagementMission = PROPERTY_MANAGEMENT_MISSION_DELIVERY
				g_PropertySystemData.iCurrentPropertyManagementVariation = 1	// DELIVERY_MISSION_VARIATION_BREAKABLE_CARGO_COUNTRY
			ENDIF  
			
			g_iGlobalWaitTime = 0
			INT iResetWaitTime
		    REPEAT ENUM_TO_INT(GLOBAL_CHARACTER_SHEET_GET_MAX_CHARACTERS_FOR_GAMEMODE()) iResetWaitTime
		        g_iCharWaitTime[iResetWaitTime] = 0
		    ENDREPEAT			
			
			CLEAR_BITMASK_AS_ENUM(g_PropertySystemData.iSystemBit, PROPERTY_SYSTEM_BIT_SEND_FAIL_TEXT)
			CLEAR_BITMASK_AS_ENUM(g_PropertySystemData.iSystemBit, PROPERTY_SYSTEM_BIT_SEND_PASS_TEXT)
			CLEAR_BITMASK_AS_ENUM(g_PropertySystemData.iSystemBit, PROPERTY_SYSTEM_BIT_PLAYED_PASS_SOUND)
			CLEAR_BITMASK_AS_ENUM(g_PropertySystemData.iSystemBit, PROPERTY_SYSTEM_BIT_DISPLAY_MISSION_PASS)
			
			IF IS_COMMUNICATION_REGISTERED(g_PropertySystemData.sManagementCommsID)
				CANCEL_COMMUNICATION(g_PropertySystemData.sManagementCommsID)
			ENDIF			
				
		 	g_PropertySystemData.missionPassedScreenState = PROPERTY_MISSION_PASSED_SCREEN_STATE_NOT_STARTED
			g_PropertySystemData.failReasonOverride = PROPERTY_MISSION_FAIL_REASON_OVERRIDE_NONE
			g_PropertySystemData.passReasonOverride = PROPERTY_MISSION_PASS_REASON_OVERRIDE_NONE
			
		    IF HAS_SCALEFORM_MOVIE_LOADED(g_PropertySystemData.passedScaleform)
	           	SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(g_PropertySystemData.passedScaleform)
			   	SET_RESULT_SCREEN_DISPLAYING_STATE(FALSE)			
			ENDIF
			//g_PropertySystemData.bDebugSetAsOwner[g_PropertySystemData.currentPropertyManagementProperty] = TRUE
			enumCharacterList setChar
			SWITCH g_PropertySystemData.currentPropertyManagementProperty
				CASE PROPERTY_ARMS_TRAFFICKING
					setChar = CHAR_TREVOR
				BREAK
				CASE PROPERTY_CAR_MOD_SHOP
				CASE PROPERTY_TOWING_IMPOUND
				CASE PROPERTY_WEED_SHOP
				CASE PROPERTY_TAXI_LOT
				CASE PROPERTY_BAR_HOOKIES
					setChar = CHAR_FRANKLIN
				BREAK
				CASE PROPERTY_CINEMA_VINEWOOD
				CASE PROPERTY_CINEMA_DOWNTOWN
				CASE PROPERTY_CINEMA_MORNINGWOOD
					setChar = CHAR_MICHAEL
				BREAK 
				DEFAULT 
					setChar = GET_CURRENT_PLAYER_PED_ENUM()
				BREAK
			ENDSWITCH
			
			g_savedGlobals.sPropertyData.propertyOwnershipData[g_PropertySystemData.currentPropertyManagementProperty].charOwner = setChar
			 
			g_PropertySystemData.propertyManagementState = PROPERTY_MANAGEMENT_STATE_GIVE_NOTIFICATION
			g_PropertySystemData.bDebugRequeue = TRUE
		BREAK
		
		CASE SPCID_RESET_AMMO
			g_bResetAmmoNewGame = TRUE
		BREAK
		
		CASE SPCID_KEEP_AMMO
			g_bResetAmmoNewGame = FALSE
		BREAK
		
		CASE SPCID_STOCKMARKET
			//FIRE_AND_FORGET_BAWSAQ_BROWSER_DEBUG()
		BREAK

		CASE SPCID_MICHAEL_EVENT 
			SP_PERFORM_ANY_SPECIAL_MICHAEL_EVENT_CODE()
		BREAK
		
		CASE SPCID_AMBIENT_PHILIPS
			
			// Complete RCM
			Activate_RC_Mission(RC_MRS_PHILIPS_1, TRUE)

			// Allow vehicle gens to spawn when warping to area
			CLEAR_MUST_LEAVE_AREA_VEHICLE_GEN_FLAG(VEHGEN_MRSP_PHARMACY1)
			CLEAR_MUST_LEAVE_AREA_VEHICLE_GEN_FLAG(VEHGEN_MRSP_PHARMACY2)
			CLEAR_MUST_LEAVE_AREA_VEHICLE_GEN_FLAG(VEHGEN_MRSP_HOSPITAL1)
			CLEAR_MUST_LEAVE_AREA_VEHICLE_GEN_FLAG(VEHGEN_MRSP_HOSPITAL2)
			CLEAR_MUST_LEAVE_AREA_VEHICLE_GEN_FLAG(VEHGEN_MRSP_HOSPITAL3)
			CLEAR_MUST_LEAVE_AREA_VEHICLE_GEN_FLAG(VEHGEN_MRSP_HOSPITAL4)
		BREAK
		
		CASE SPCID_UNLOCK_BLIMP_CONTACT
			ADD_CONTACT_TO_PHONEBOOK(CHAR_BLIMP, ALL_OWNERS_BOOKS)
		BREAK
		
//		CASE SPCID_CASINO_MINIGAME_WARP
//			DO_PLAYER_MAP_WARP_WITH_LOAD(<<975.0649, 43.2549, 80.6180>>, 78.1340)
//		BREAK
		
		CASE SPCID_CASINO_POKER_WARP
			DO_PLAYER_MAP_WARP_WITH_LOAD(<<966.9172, 45.2775, 80.0588>>, 87.8897 )
		BREAK

		CASE SPCID_CASINO_BLACKJACK_WARP
			DO_PLAYER_MAP_WARP_WITH_LOAD(<<967.2065, 48.1126, 80.0588>>, 261.5536)
		BREAK

		CASE SPCID_CASINO_ROULETTE_WARP
			DO_PLAYER_MAP_WARP_WITH_LOAD(<<964.4283, 48.7141, 80.0588>>, 41.2441)
		BREAK
		
		CASE SPCID_CASINO_SLOT_WARP
			DO_PLAYER_MAP_WARP_WITH_LOAD(<<974.9168, 55.1696, 78.9681>>, 264.2273)
		BREAK

		CASE SPCID_CASINO_EXTERIOR_WARP
			DO_PLAYER_MAP_WARP_WITH_LOAD(<<877.0709, 19.7356, 77.7698>>, 278.1835)
		BREAK

		CASE SPCID_UNLOCK_TOWING_ODDJOB
			
			// Complete all Tonya RCM's
			Activate_RC_Mission(RC_TONYA_1, TRUE)
			Activate_RC_Mission(RC_TONYA_2, TRUE)
			Activate_RC_Mission(RC_TONYA_3, TRUE)
			Activate_RC_Mission(RC_TONYA_4, TRUE)
			Activate_RC_Mission(RC_TONYA_5, TRUE)
						
			// Set number of towing jobs completed for procedural controller
			g_savedGlobals.sTowingData.iTowingJobsCompleted = 5
			
			// set property controller to boot towing
			g_PropertySystemData.ambientScriptProperty = PROPERTY_TOWING_IMPOUND
			RUN_PROPERTY_PURCHASE_CODE_ID(PROPERTY_TOWING_IMPOUND, TRUE)
			SET_BITMASK_AS_ENUM(g_PropertySystemData.iSystemBit, PROPERTY_SYSTEM_BIT_LAUNCH_AMBIENT_SCRIPT)
			
			// Franklin now owns the towing impound
			g_savedGlobals.sPropertyData.propertyOwnershipData[PROPERTY_TOWING_IMPOUND].charOwner = CHAR_FRANKLIN
		BREAK

		DEFAULT
			#if FEATURE_SP_DLC_DIRECTOR_MODE
			INT index
			#ENDIF
			
			SWITCH eCodeID
			
				CASE SPCID_LOAD_OLD_H_IPLS
					ON_ENTER_SP()
					g_bTurnOnNewHIPLs = FALSE
				BREAK

				CASE SPCID_LOAD_NEW_H_IPLS
					ON_ENTER_MP()
						REQUEST_IPL("hei_carrier")
						REQUEST_IPL("hei_carrier_int1")
						REQUEST_IPL("hei_carrier_int2")
						REQUEST_IPL("hei_carrier_int3")
						REQUEST_IPL("hei_carrier_int4")
						REQUEST_IPL("hei_carrier_int5")
						REQUEST_IPL("hei_carrier_int6")
						REQUEST_IPL("hei_yacht_heist")
						REQUEST_IPL("hei_yacht_heist_enginrm")
						REQUEST_IPL("hei_yacht_heist_Lounge")
						REQUEST_IPL("hei_yacht_heist_Bridge")
						REQUEST_IPL("hei_yacht_heist_Bar")
						REQUEST_IPL("hei_yacht_heist_Bedrm")
					g_bTurnOnNewHIPLs = TRUE
				BREAK

				CASE SPCID_MAGDEMO_2a
					PRINTLN("ooosh a")
					g_iMagDemoVariation = 0
				BREAK
				CASE SPCID_MAGDEMO_2b
					PRINTLN("ooosh b")
					g_iMagDemoVariation = 1
				BREAK
			
				CASE SPCID_REFRESH_CAM_FOR_SCENARIOS
					DO_SCREEN_FADE_OUT(0)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(180)
					SET_CLOCK_TIME(13, 0, 0)
					RESET_SCENARIO_TYPES_ENABLED()
					WAIT(0)
					RESET_SCENARIO_GROUPS_ENABLED()
					WAIT(0)
					INSTANTLY_FILL_PED_POPULATION()
					WAIT(0)
					POPULATE_NOW()
					WAIT(0)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
					DO_SCREEN_FADE_IN(0)
				BREAK
				
				CASE SPCID_BLOCK_WILDLIFE_EMAIL
					SET_BIT(g_iDebugLaunchBlockCommunication, BIT_DBG_LNCH_COMM_BLOCK_WILDLIFE_PHOTO)
				BREAK
				
				CASE SPCID_LAST_GEN_PLAYER_ON
					g_bLastGenPlayer = TRUE
				BREAK
				
				CASE SPCID_LAST_GEN_PLAYER_OFF
					g_bLastGenPlayer = FALSE
				BREAK
				
				CASE SPCID_LAST_GEN_SE_PLAYER_ON
					g_bLastGenSpecialEditionPlayer = TRUE
				BREAK
				
				CASE SPCID_LAST_GEN_SE_PLAYER_OFF
					g_bLastGenSpecialEditionPlayer = FALSE
				BREAK
				
				CASE SPCID_LAST_GEN_CE_PLAYER_ON
					g_bLastGenCollectorEditionPlayer = TRUE
				BREAK
				
				CASE SPCID_LAST_GEN_CE_PLAYER_OFF
					g_bLastGenCollectorEditionPlayer = FALSE
				BREAK

				CASE SPCID_NEXT_MONKEY_MOSAIC
					g_iDebugMosaicToJumpTo++
					g_bDebugJumpToMosaic = TRUE
				BREAK
				
				CASE SPCID_LAST_MONKEY_MOSAIC
					g_iDebugMosaicToJumpTo--
					g_bDebugJumpToMosaic = TRUE
				BREAK
				
				CASE SPCID_SHRINK_VARIATION
					SHRINK_SET_LAST_NARRATIVE(g_savedGlobals.sShrinkData, 0)
				BREAK
				
				CASE SPCID_UNLOCK_MONKEY_PHOTO_RE
					SP_PERFORM_ANY_SPECIAL_RANDOM_EVENT_CODE(sDebugMenuData)
					Execute_Code_ID(CID_DEBUG_UNLOCK_MONKEY_PHOTO_RE,0)
				BREAK
				
				CASE SPCID_LAUNCH_MURDER_MYSTERY
					TERMINATE_ALL_SCRIPTS_WITH_THIS_NAME( "murderMystery" )
					WHILE( GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH( HASH( "murderMystery" ) ) <> 0 )
						CPRINTLN( DEBUG_SIMON, "Waiting for murderMystery to die" )
						WAIT( 0 )
					ENDWHILE
					REACTIVATE_ALL_WORLD_BRAINS_THAT_ARE_WAITING_TILL_OUT_OF_RANGE()
				BREAK
				
				CASE SPCID_COUNTRY_RACE_1_WARP
					IF g_bLastGenPlayer g_iDebugRaceWarp = 0	ENDIF
				BREAK
				
				CASE SPCID_COUNTRY_RACE_2_WARP
					IF g_bLastGenPlayer g_iDebugRaceWarp = 1	ENDIF
				BREAK
				
				CASE SPCID_COUNTRY_RACE_3_WARP
					IF g_bLastGenPlayer g_iDebugRaceWarp = 2	ENDIF
				BREAK
				
				CASE SPCID_COUNTRY_RACE_4_WARP
					IF g_bLastGenPlayer g_iDebugRaceWarp = 3	ENDIF
				BREAK
				
				CASE SPCID_COUNTRY_RACE_5_WARP
					IF g_bLastGenPlayer g_iDebugRaceWarp = 4	ENDIF
				BREAK

				CASE SPCID_SPAWN_CHEV_MARHSALL	
					SP_DEBUG_SPAWN_PLAYER_IN_VEHICLE(INT_TO_ENUM(MODEL_NAMES, HASH("MARSHALL")))
				BREAK
				CASE SPCID_SPAWN_PISS_DOMIN		
					SP_DEBUG_SPAWN_PLAYER_IN_VEHICLE(INT_TO_ENUM(MODEL_NAMES, HASH("DOMINATOR2")))
				BREAK
				CASE SPCID_SPAWN_RED_GAUNT	
					SP_DEBUG_SPAWN_PLAYER_IN_VEHICLE(INT_TO_ENUM(MODEL_NAMES, HASH("GAUNTLET2")))
				BREAK	
				CASE SPCID_SPAWN_SPRUNK_BUFF		
					SP_DEBUG_SPAWN_PLAYER_IN_VEHICLE(INT_TO_ENUM(MODEL_NAMES, HASH("BUFFALO3")))
				BREAK
				CASE SPCID_SPAWN_XERO_BLIMP	
					SP_DEBUG_SPAWN_PLAYER_IN_VEHICLE(INT_TO_ENUM(MODEL_NAMES, HASH("BLIMP2")))
				BREAK
				CASE SPCID_SPAWN_DUEL_CAR	
					SP_DEBUG_SPAWN_PLAYER_IN_VEHICLE(INT_TO_ENUM(MODEL_NAMES, HASH("DUKES2")))
				BREAK	
				CASE SPCID_SPAWN_BUBBLE_SUB	
					SP_DEBUG_SPAWN_PLAYER_IN_VEHICLE(INT_TO_ENUM(MODEL_NAMES, HASH("SUBMERSIBLE2")))
				BREAK	
				CASE SPCID_SPAWN_DODO	
					SP_DEBUG_SPAWN_PLAYER_IN_VEHICLE(INT_TO_ENUM(MODEL_NAMES, HASH("DODO")))
				BREAK		
				CASE SPCID_SPAWN_BLISTA_COMACT	
					SCRIPT_ASSERT("New NG vehicle isn't in the build yet. Sorry.")
					//SP_DEBUG_SPAWN_PLAYER_IN_VEHICLE(INT_TO_ENUM(MODEL_NAMES, HASH("BLISTA2")))
				BREAK
				CASE SPCID_EQUIP_NG_WEAPON_HATCHET	
					IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
						GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_DLC_HATCHET, 1, TRUE, TRUE)
					ENDIF
				BREAK
				CASE SPCID_EQUIP_NG_WEAPON_RAILGUN	
					IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
						GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_DLC_RAILGUN, 999, TRUE, TRUE)
					ENDIF
				BREAK
				
				CASE SPCID_PEYOTE_RESET_PROGRESSION		g_bDebugPeyoteResetProgression = TRUE							BREAK
				
				CASE SPCID_PEYOTE_LOWLAND_LABELS_ON		g_bDebugPeyotePickupLabels[APT_LOWLAND] = TRUE					BREAK
				CASE SPCID_PEYOTE_LOWLAND_LABELS_OFF	g_bDebugPeyotePickupLabels[APT_LOWLAND] = FALSE					BREAK
				CASE SPCID_PEYOTE_HIGHLAND_LABELS_ON	g_bDebugPeyotePickupLabels[APT_HIGHLAND] = TRUE					BREAK
				CASE SPCID_PEYOTE_HIGHLAND_LABELS_OFF	g_bDebugPeyotePickupLabels[APT_HIGHLAND] = FALSE				BREAK
				CASE SPCID_PEYOTE_WATER_LABELS_ON		g_bDebugPeyotePickupLabels[APT_UNDERWATER] = TRUE				BREAK
				CASE SPCID_PEYOTE_WATER_LABELS_OFF		g_bDebugPeyotePickupLabels[APT_UNDERWATER] = FALSE				BREAK
				CASE SPCID_PEYOTE_SECRET_LABELS_ON		g_bDebugPeyotePickupLabels[APT_SECRET] = TRUE					BREAK
				CASE SPCID_PEYOTE_SECRET_LABELS_OFF		g_bDebugPeyotePickupLabels[APT_SECRET] = FALSE					BREAK
				
				CASE SPCID_PEYOTE_LOWLAND_BLIPS_ON		g_bDebugPeyotePickupBlips[APT_LOWLAND] = TRUE					BREAK
				CASE SPCID_PEYOTE_LOWLAND_BLIPS_OFF		g_bDebugPeyotePickupBlips[APT_LOWLAND] = FALSE					BREAK
				CASE SPCID_PEYOTE_HIGHLAND_BLIPS_ON		g_bDebugPeyotePickupBlips[APT_HIGHLAND] = TRUE					BREAK
				CASE SPCID_PEYOTE_HIGHLAND_BLIPS_OFF	g_bDebugPeyotePickupBlips[APT_HIGHLAND] = FALSE					BREAK
				CASE SPCID_PEYOTE_WATER_BLIPS_ON		g_bDebugPeyotePickupBlips[APT_UNDERWATER] = TRUE				BREAK
				CASE SPCID_PEYOTE_WATER_BLIPS_OFF		g_bDebugPeyotePickupBlips[APT_UNDERWATER] = FALSE				BREAK
				CASE SPCID_PEYOTE_SECRET_BLIPS_ON		g_bDebugPeyotePickupBlips[APT_SECRET] = TRUE					BREAK
				CASE SPCID_PEYOTE_SECRET_BLIPS_OFF		g_bDebugPeyotePickupBlips[APT_SECRET] = FALSE					BREAK
				
				CASE SPCID_PEYOTE_WARP_TO_LOWLAND_0		g_eDebugPeyoteType = APT_LOWLAND	g_iDebugPeyoteIndex = 0		BREAK
				CASE SPCID_PEYOTE_WARP_TO_LOWLAND_1		g_eDebugPeyoteType = APT_LOWLAND	g_iDebugPeyoteIndex = 1		BREAK
				CASE SPCID_PEYOTE_WARP_TO_LOWLAND_2		g_eDebugPeyoteType = APT_LOWLAND	g_iDebugPeyoteIndex = 2		BREAK
				CASE SPCID_PEYOTE_WARP_TO_LOWLAND_3		g_eDebugPeyoteType = APT_LOWLAND	g_iDebugPeyoteIndex = 3		BREAK
				CASE SPCID_PEYOTE_WARP_TO_LOWLAND_4		g_eDebugPeyoteType = APT_LOWLAND	g_iDebugPeyoteIndex = 4		BREAK
				CASE SPCID_PEYOTE_WARP_TO_LOWLAND_5		g_eDebugPeyoteType = APT_LOWLAND	g_iDebugPeyoteIndex = 5		BREAK
				CASE SPCID_PEYOTE_WARP_TO_LOWLAND_6		g_eDebugPeyoteType = APT_LOWLAND	g_iDebugPeyoteIndex = 6		BREAK
				CASE SPCID_PEYOTE_WARP_TO_LOWLAND_7		g_eDebugPeyoteType = APT_LOWLAND	g_iDebugPeyoteIndex = 7		BREAK
				CASE SPCID_PEYOTE_WARP_TO_LOWLAND_8		g_eDebugPeyoteType = APT_LOWLAND	g_iDebugPeyoteIndex = 8		BREAK
				CASE SPCID_PEYOTE_WARP_TO_LOWLAND_9		g_eDebugPeyoteType = APT_LOWLAND	g_iDebugPeyoteIndex = 9		BREAK
				CASE SPCID_PEYOTE_WARP_TO_LOWLAND_10	g_eDebugPeyoteType = APT_LOWLAND	g_iDebugPeyoteIndex = 10	BREAK
				CASE SPCID_PEYOTE_WARP_TO_LOWLAND_11	g_eDebugPeyoteType = APT_LOWLAND	g_iDebugPeyoteIndex = 11	BREAK
				CASE SPCID_PEYOTE_WARP_TO_LOWLAND_12	g_eDebugPeyoteType = APT_LOWLAND	g_iDebugPeyoteIndex = 12	BREAK
				CASE SPCID_PEYOTE_WARP_TO_LOWLAND_13	g_eDebugPeyoteType = APT_LOWLAND	g_iDebugPeyoteIndex = 13	BREAK
				CASE SPCID_PEYOTE_WARP_TO_LOWLAND_14	g_eDebugPeyoteType = APT_LOWLAND	g_iDebugPeyoteIndex = 14	BREAK
				
				CASE SPCID_PEYOTE_WARP_TO_HIGHLAND_0	g_eDebugPeyoteType = APT_HIGHLAND	g_iDebugPeyoteIndex = 0		BREAK
				CASE SPCID_PEYOTE_WARP_TO_HIGHLAND_1	g_eDebugPeyoteType = APT_HIGHLAND	g_iDebugPeyoteIndex = 1		BREAK
				CASE SPCID_PEYOTE_WARP_TO_HIGHLAND_2	g_eDebugPeyoteType = APT_HIGHLAND	g_iDebugPeyoteIndex = 2		BREAK
				CASE SPCID_PEYOTE_WARP_TO_HIGHLAND_3	g_eDebugPeyoteType = APT_HIGHLAND	g_iDebugPeyoteIndex = 3		BREAK
				CASE SPCID_PEYOTE_WARP_TO_HIGHLAND_4	g_eDebugPeyoteType = APT_HIGHLAND	g_iDebugPeyoteIndex = 4		BREAK
				CASE SPCID_PEYOTE_WARP_TO_HIGHLAND_5	g_eDebugPeyoteType = APT_HIGHLAND	g_iDebugPeyoteIndex = 5		BREAK
				
				CASE SPCID_PEYOTE_WARP_TO_WATER_0		g_eDebugPeyoteType = APT_UNDERWATER	g_iDebugPeyoteIndex = 0		BREAK
				CASE SPCID_PEYOTE_WARP_TO_WATER_1		g_eDebugPeyoteType = APT_UNDERWATER	g_iDebugPeyoteIndex = 1		BREAK
				CASE SPCID_PEYOTE_WARP_TO_WATER_2		g_eDebugPeyoteType = APT_UNDERWATER	g_iDebugPeyoteIndex = 2		BREAK
				CASE SPCID_PEYOTE_WARP_TO_WATER_3		g_eDebugPeyoteType = APT_UNDERWATER	g_iDebugPeyoteIndex = 3		BREAK
				CASE SPCID_PEYOTE_WARP_TO_WATER_4		g_eDebugPeyoteType = APT_UNDERWATER	g_iDebugPeyoteIndex = 4		BREAK
				CASE SPCID_PEYOTE_WARP_TO_WATER_5		g_eDebugPeyoteType = APT_UNDERWATER	g_iDebugPeyoteIndex = 5		BREAK
				
				CASE SPCID_PEYOTE_WARP_TO_SECRET_SUN	g_eDebugPeyoteType = APT_SECRET		g_iDebugPeyoteIndex = 0		BREAK
				CASE SPCID_PEYOTE_WARP_TO_SECRET_MON	g_eDebugPeyoteType = APT_SECRET		g_iDebugPeyoteIndex = 1		BREAK
				CASE SPCID_PEYOTE_WARP_TO_SECRET_TUE	g_eDebugPeyoteType = APT_SECRET		g_iDebugPeyoteIndex = 2		BREAK
				CASE SPCID_PEYOTE_WARP_TO_SECRET_WED	g_eDebugPeyoteType = APT_SECRET		g_iDebugPeyoteIndex = 3		BREAK
				CASE SPCID_PEYOTE_WARP_TO_SECRET_THU	g_eDebugPeyoteType = APT_SECRET		g_iDebugPeyoteIndex = 4		BREAK
				CASE SPCID_PEYOTE_WARP_TO_SECRET_FRI	g_eDebugPeyoteType = APT_SECRET		g_iDebugPeyoteIndex = 5		BREAK
				CASE SPCID_PEYOTE_WARP_TO_SECRET_SAT	g_eDebugPeyoteType = APT_SECRET		g_iDebugPeyoteIndex = 6		BREAK
				
				CASE SPCID_PEYOTE_NEXT_ANIMAL_BOAR			g_eDebugPeyoteAnimalLowland = AP_LOWLAND_BOAR				BREAK	
				CASE SPCID_PEYOTE_NEXT_ANIMAL_CAT			g_eDebugPeyoteAnimalLowland = AP_LOWLAND_CAT				BREAK				
				CASE SPCID_PEYOTE_NEXT_ANIMAL_COW			g_eDebugPeyoteAnimalLowland = AP_LOWLAND_COW				BREAK				
				CASE SPCID_PEYOTE_NEXT_ANIMAL_COYOTE		g_eDebugPeyoteAnimalLowland = AP_LOWLAND_COYOTE				BREAK				
				CASE SPCID_PEYOTE_NEXT_ANIMAL_DEER			g_eDebugPeyoteAnimalLowland = AP_LOWLAND_DEER				BREAK				
				CASE SPCID_PEYOTE_NEXT_ANIMAL_HUSKY			g_eDebugPeyoteAnimalLowland = AP_LOWLAND_HUSKY				BREAK			
				CASE SPCID_PEYOTE_NEXT_ANIMAL_MTLION		g_eDebugPeyoteAnimalLowland = AP_LOWLAND_MTLION				BREAK				
				CASE SPCID_PEYOTE_NEXT_ANIMAL_PIG			g_eDebugPeyoteAnimalLowland = AP_LOWLAND_PIG				BREAK				
				CASE SPCID_PEYOTE_NEXT_ANIMAL_POODLE		g_eDebugPeyoteAnimalLowland = AP_LOWLAND_POODLE				BREAK				
				CASE SPCID_PEYOTE_NEXT_ANIMAL_PUG			g_eDebugPeyoteAnimalLowland = AP_LOWLAND_PUG				BREAK				
				CASE SPCID_PEYOTE_NEXT_ANIMAL_RABBIT		g_eDebugPeyoteAnimalLowland = AP_LOWLAND_RABBIT				BREAK							
				CASE SPCID_PEYOTE_NEXT_ANIMAL_RETRIEVER		g_eDebugPeyoteAnimalLowland = AP_LOWLAND_RETRIEVER			BREAK		
				CASE SPCID_PEYOTE_NEXT_ANIMAL_ROTTWEILER	g_eDebugPeyoteAnimalLowland = AP_LOWLAND_ROTTWEILER			BREAK			
				CASE SPCID_PEYOTE_NEXT_ANIMAL_SHEPHERD		g_eDebugPeyoteAnimalLowland = AP_LOWLAND_SHEPHERD			BREAK			
				CASE SPCID_PEYOTE_NEXT_ANIMAL_WESTY			g_eDebugPeyoteAnimalLowland = AP_LOWLAND_WESTY				BREAK			
				
				CASE SPCID_PEYOTE_NEXT_ANIMAL_CHICKENHAWK	g_eDebugPeyoteAnimalHighland = AP_HIGHLAND_CHICKENHAWK		BREAK
				CASE SPCID_PEYOTE_NEXT_ANIMAL_CORMORANT		g_eDebugPeyoteAnimalHighland = AP_HIGHLAND_CORMORANT		BREAK		
				CASE SPCID_PEYOTE_NEXT_ANIMAL_CROW			g_eDebugPeyoteAnimalHighland = AP_HIGHLAND_CROW				BREAK		
				CASE SPCID_PEYOTE_NEXT_ANIMAL_HEN			g_eDebugPeyoteAnimalHighland = AP_HIGHLAND_HEN				BREAK			
				CASE SPCID_PEYOTE_NEXT_ANIMAL_PIGEON		g_eDebugPeyoteAnimalHighland = AP_HIGHLAND_PIGEON			BREAK			
				CASE SPCID_PEYOTE_NEXT_ANIMAL_SEAGULL		g_eDebugPeyoteAnimalHighland = AP_HIGHLAND_SEAGULL			BREAK		
				
				CASE SPCID_PEYOTE_NEXT_ANIMAL_DOLPHIN		g_eDebugPeyoteAnimalUnderwater = AP_UNDERWATER_DOLPHIN		BREAK		
				CASE SPCID_PEYOTE_NEXT_ANIMAL_FISH			g_eDebugPeyoteAnimalUnderwater = AP_UNDERWATER_FISH			BREAK			
				CASE SPCID_PEYOTE_NEXT_ANIMAL_KILLERWHALE	g_eDebugPeyoteAnimalUnderwater = AP_UNDERWATER_KILLERWHALE	BREAK	
				CASE SPCID_PEYOTE_NEXT_ANIMAL_SHARKHAMMER	g_eDebugPeyoteAnimalUnderwater = AP_UNDERWATER_SHARKHAMMER	BREAK	
				CASE SPCID_PEYOTE_NEXT_ANIMAL_SHARKTIGER	g_eDebugPeyoteAnimalUnderwater = AP_UNDERWATER_SHARKTIGER	BREAK		
				CASE SPCID_PEYOTE_NEXT_ANIMAL_STINGRAY		g_eDebugPeyoteAnimalUnderwater = AP_UNDERWATER_STINGRAY		BREAK
				
				CASE SPCID_MP_DELETE_ALL_CHARACTERS
					FAKE_KEY_PRESS(KEY_LCONTROL)
					FAKE_KEY_PRESS(KEY_H)
					PRINTLN("[SPMPDEBUG] - [M MENU] - Pressed CTRL + H to delete all characters")
				BREAK
				
					
				DEFAULT
					#if FEATURE_SP_DLC_DIRECTOR_MODE
					SWITCH eCodeID
					
					
						CASE SPCID_RUN_DIRECTOR_MODE				g_bLaunchDirectorMode = TRUE								BREAK						
						
						CASE SPCID_DM_UNLOCK_ALL					
							REPEAT MAX_DU_STORY index
								SET_DIRECTOR_STORY_CHARACTER_UNLOCKED(INT_TO_ENUM(DirectorUnlockStory, index), TRUE)
							ENDREPEAT
							REPEAT MAX_DU_HEIST index
								SET_DIRECTOR_HEIST_CHARACTER_UNLOCKED(INT_TO_ENUM(DirectorUnlockHeist, index))
							ENDREPEAT
							REPEAT MAX_DU_SPECIAL index
								SET_DIRECTOR_SPECIAL_CHARACTER_UNLOCKED(INT_TO_ENUM(DirectorUnlockSpecial, index), TRUE)
							ENDREPEAT
							REPEAT MAX_DU_ANIMAL index
								SET_DIRECTOR_ANIMAL_CHARACTER_UNLOCKED(INT_TO_ENUM(DirectorUnlockAnimal, index), TRUE)
							ENDREPEAT
						BREAK
						CASE SPCID_DM_LOCK_ALL					
							g_savedGlobals.sDirectorModeData.iBitsetCharacterStoryUnlock = 0
							g_savedGlobals.sDirectorModeData.iBitsetCharacterHeistUnlock = 0
							g_savedGlobals.sDirectorModeData.iBitsetCharacterSpecialUnlock = 0
							g_savedGlobals.sDirectorModeData.iBitsetCharacterAnimalUnlock = 0
						BREAK
						
						CASE SPCID_DM_UNLOCK_STORY_ALL
							REPEAT MAX_DU_STORY index
								SET_DIRECTOR_STORY_CHARACTER_UNLOCKED(INT_TO_ENUM(DirectorUnlockStory, index), TRUE)
							ENDREPEAT
						BREAK
						CASE SPCID_DM_LOCK_STORY_ALL					
							g_savedGlobals.sDirectorModeData.iBitsetCharacterStoryUnlock = 0
						BREAK
						CASE SPCID_DM_UNLOCK_STORY_MICHAEL		SET_DIRECTOR_STORY_CHARACTER_UNLOCKED(DU_STORY_MICHAEL)			BREAK
						CASE SPCID_DM_UNLOCK_STORY_FRANKLIN		SET_DIRECTOR_STORY_CHARACTER_UNLOCKED(DU_STORY_FRANKLIN)		BREAK			
						CASE SPCID_DM_UNLOCK_STORY_TREVOR		SET_DIRECTOR_STORY_CHARACTER_UNLOCKED(DU_STORY_TREVOR)			BREAK			
						CASE SPCID_DM_UNLOCK_STORY_AMANDA_T		SET_DIRECTOR_STORY_CHARACTER_UNLOCKED(DU_STORY_AMANDA_T)		BREAK		
						CASE SPCID_DM_UNLOCK_STORY_BEVERLY		SET_DIRECTOR_STORY_CHARACTER_UNLOCKED(DU_STORY_BEVERLY)			BREAK			
						CASE SPCID_DM_UNLOCK_STORY_BRAD			SET_DIRECTOR_STORY_CHARACTER_UNLOCKED(DU_STORY_BRAD)			BREAK				
						CASE SPCID_DM_UNLOCK_STORY_CHRIS_F		SET_DIRECTOR_STORY_CHARACTER_UNLOCKED(DU_STORY_CHRIS_F)			BREAK			
						CASE SPCID_DM_UNLOCK_STORY_DAVE_N		SET_DIRECTOR_STORY_CHARACTER_UNLOCKED(DU_STORY_DAVE_N)			BREAK			
						CASE SPCID_DM_UNLOCK_STORY_DEVIN		SET_DIRECTOR_STORY_CHARACTER_UNLOCKED(DU_STORY_DEVIN)			BREAK				
						CASE SPCID_DM_UNLOCK_STORY_DR_F			SET_DIRECTOR_STORY_CHARACTER_UNLOCKED(DU_STORY_DR_F)			BREAK				
						CASE SPCID_DM_UNLOCK_STORY_FABIEN		SET_DIRECTOR_STORY_CHARACTER_UNLOCKED(DU_STORY_FABIEN)			BREAK			
						CASE SPCID_DM_UNLOCK_STORY_FLOYD		SET_DIRECTOR_STORY_CHARACTER_UNLOCKED(DU_STORY_FLOYD)			BREAK				
						CASE SPCID_DM_UNLOCK_STORY_JIMMY		SET_DIRECTOR_STORY_CHARACTER_UNLOCKED(DU_STORY_JIMMY)			BREAK				
						CASE SPCID_DM_UNLOCK_STORY_LAMAR		SET_DIRECTOR_STORY_CHARACTER_UNLOCKED(DU_STORY_LAMAR)			BREAK				
						CASE SPCID_DM_UNLOCK_STORY_LAZLOW		SET_DIRECTOR_STORY_CHARACTER_UNLOCKED(DU_STORY_LAZLOW)			BREAK			
						CASE SPCID_DM_UNLOCK_STORY_LESTER		SET_DIRECTOR_STORY_CHARACTER_UNLOCKED(DU_STORY_LESTER)			BREAK			
						CASE SPCID_DM_UNLOCK_STORY_MAUDE		SET_DIRECTOR_STORY_CHARACTER_UNLOCKED(DU_STORY_MAUDE)			BREAK				
						CASE SPCID_DM_UNLOCK_STORY_MRS_T		SET_DIRECTOR_STORY_CHARACTER_UNLOCKED(DU_STORY_MRS_T)			BREAK				
						CASE SPCID_DM_UNLOCK_STORY_RON			SET_DIRECTOR_STORY_CHARACTER_UNLOCKED(DU_STORY_RON)				BREAK			
						CASE SPCID_DM_UNLOCK_STORY_PATRICIA		SET_DIRECTOR_STORY_CHARACTER_UNLOCKED(DU_STORY_PATRICIA)		BREAK			
						CASE SPCID_DM_UNLOCK_STORY_SIMEON		SET_DIRECTOR_STORY_CHARACTER_UNLOCKED(DU_STORY_SIMEON)			BREAK			
						CASE SPCID_DM_UNLOCK_STORY_SOLOMON		SET_DIRECTOR_STORY_CHARACTER_UNLOCKED(DU_STORY_SOLOMON)			BREAK			
						CASE SPCID_DM_UNLOCK_STORY_STEVE_HAINS	SET_DIRECTOR_STORY_CHARACTER_UNLOCKED(DU_STORY_STEVE_HAINS)		BREAK		
						CASE SPCID_DM_UNLOCK_STORY_STRETCH		SET_DIRECTOR_STORY_CHARACTER_UNLOCKED(DU_STORY_STRETCH)			BREAK			
						CASE SPCID_DM_UNLOCK_STORY_TANISHA		SET_DIRECTOR_STORY_CHARACTER_UNLOCKED(DU_STORY_TANISHA)			BREAK			
						CASE SPCID_DM_UNLOCK_STORY_TAO_C		SET_DIRECTOR_STORY_CHARACTER_UNLOCKED(DU_STORY_TAO_C)			BREAK				
						CASE SPCID_DM_UNLOCK_STORY_TRACEY		SET_DIRECTOR_STORY_CHARACTER_UNLOCKED(DU_STORY_TRACEY)			BREAK			
						CASE SPCID_DM_UNLOCK_STORY_WADE			SET_DIRECTOR_STORY_CHARACTER_UNLOCKED(DU_STORY_WADE)			BREAK				
						
						CASE SPCID_DM_UNLOCK_HEIST_ALL
							REPEAT MAX_DU_HEIST index
								SET_DIRECTOR_HEIST_CHARACTER_UNLOCKED(INT_TO_ENUM(DirectorUnlockHeist, index))
							ENDREPEAT
						BREAK
						CASE SPCID_DM_LOCK_HEIST_ALL					
							g_savedGlobals.sDirectorModeData.iBitsetCharacterHeistUnlock = 0
						BREAK
						CASE SPCID_DM_UNLOCK_HEIST_CHEF			SET_DIRECTOR_HEIST_CHARACTER_UNLOCKED(DU_HEIST_CHEF, FALSE)			BREAK
						CASE SPCID_DM_UNLOCK_HEIST_CHRISTIAN	SET_DIRECTOR_HEIST_CHARACTER_UNLOCKED(DU_HEIST_CHRISTIAN, FALSE)	BREAK			
						CASE SPCID_DM_UNLOCK_HEIST_DARYL		SET_DIRECTOR_HEIST_CHARACTER_UNLOCKED(DU_HEIST_DARYL, FALSE)		BREAK				
						CASE SPCID_DM_UNLOCK_HEIST_EDDIE		SET_DIRECTOR_HEIST_CHARACTER_UNLOCKED(DU_HEIST_EDDIE, FALSE)		BREAK				
						CASE SPCID_DM_UNLOCK_HEIST_GUSTAV		SET_DIRECTOR_HEIST_CHARACTER_UNLOCKED(DU_HEIST_GUSTAV, FALSE)		BREAK			
						CASE SPCID_DM_UNLOCK_HEIST_HUGH			SET_DIRECTOR_HEIST_CHARACTER_UNLOCKED(DU_HEIST_HUGH, FALSE)			BREAK				
						CASE SPCID_DM_UNLOCK_HEIST_KARIM		SET_DIRECTOR_HEIST_CHARACTER_UNLOCKED(DU_HEIST_KARIM, FALSE)		BREAK				
						CASE SPCID_DM_UNLOCK_HEIST_KARL			SET_DIRECTOR_HEIST_CHARACTER_UNLOCKED(DU_HEIST_KARL, FALSE)			BREAK				
						CASE SPCID_DM_UNLOCK_HEIST_NORM			SET_DIRECTOR_HEIST_CHARACTER_UNLOCKED(DU_HEIST_NORM, FALSE)			BREAK			
						CASE SPCID_DM_UNLOCK_HEIST_PACKIE		SET_DIRECTOR_HEIST_CHARACTER_UNLOCKED(DU_HEIST_PACKIE, FALSE)		BREAK			
						CASE SPCID_DM_UNLOCK_HEIST_PAIGE		SET_DIRECTOR_HEIST_CHARACTER_UNLOCKED(DU_HEIST_PAIGE, FALSE)		BREAK				
						CASE SPCID_DM_UNLOCK_HEIST_RICKIE		SET_DIRECTOR_HEIST_CHARACTER_UNLOCKED(DU_HEIST_RICKIE, FALSE)		BREAK			
						CASE SPCID_DM_UNLOCK_HEIST_TALINA		SET_DIRECTOR_HEIST_CHARACTER_UNLOCKED(DU_HEIST_TALINA, FALSE)		BREAK			
						
						CASE SPCID_DM_UNLOCK_SPECIAL_ALL
							REPEAT MAX_DU_SPECIAL index
								SET_DIRECTOR_SPECIAL_CHARACTER_UNLOCKED(INT_TO_ENUM(DirectorUnlockSpecial, index), TRUE)
							ENDREPEAT
						BREAK
						CASE SPCID_DM_LOCK_SPECIAL_ALL					
							g_savedGlobals.sDirectorModeData.iBitsetCharacterSpecialUnlock = 0
						BREAK
						CASE SPCID_DM_UNLOCK_SPECIAL_ANDYMOON		SET_DIRECTOR_SPECIAL_CHARACTER_UNLOCKED(DU_SPECIAL_ANDYMOON)	BREAK
						CASE SPCID_DM_UNLOCK_SPECIAL_BAYGOR			SET_DIRECTOR_SPECIAL_CHARACTER_UNLOCKED(DU_SPECIAL_BAYGOR)		BREAK			
						CASE SPCID_DM_UNLOCK_SPECIAL_BILLBEYNOR		SET_DIRECTOR_SPECIAL_CHARACTER_UNLOCKED(DU_SPECIAL_BILLBINDER)	BREAK		
						CASE SPCID_DM_UNLOCK_SPECIAL_CLINTON		SET_DIRECTOR_SPECIAL_CHARACTER_UNLOCKED(DU_SPECIAL_CLINTON)		BREAK			
						CASE SPCID_DM_UNLOCK_SPECIAL_GRIFF			SET_DIRECTOR_SPECIAL_CHARACTER_UNLOCKED(DU_SPECIAL_GRIFF)		BREAK			
						CASE SPCID_DM_UNLOCK_SPECIAL_JANE			SET_DIRECTOR_SPECIAL_CHARACTER_UNLOCKED(DU_SPECIAL_JANE)		BREAK			
						CASE SPCID_DM_UNLOCK_SPECIAL_JEROME			SET_DIRECTOR_SPECIAL_CHARACTER_UNLOCKED(DU_SPECIAL_JEROME)		BREAK			
						CASE SPCID_DM_UNLOCK_SPECIAL_JESSE			SET_DIRECTOR_SPECIAL_CHARACTER_UNLOCKED(DU_SPECIAL_JESSE)		BREAK			
						CASE SPCID_DM_UNLOCK_SPECIAL_MANI			SET_DIRECTOR_SPECIAL_CHARACTER_UNLOCKED(DU_SPECIAL_MANI)		BREAK			
						CASE SPCID_DM_UNLOCK_SPECIAL_MIME			SET_DIRECTOR_SPECIAL_CHARACTER_UNLOCKED(DU_SPECIAL_MIME)		BREAK			
						CASE SPCID_DM_UNLOCK_SPECIAL_PAMELADRAKE	SET_DIRECTOR_SPECIAL_CHARACTER_UNLOCKED(DU_SPECIAL_PAMELADRAKE)	BREAK		
						CASE SPCID_DM_UNLOCK_SPECIAL_SUPERHERO		SET_DIRECTOR_SPECIAL_CHARACTER_UNLOCKED(DU_SPECIAL_SUPERHERO)	BREAK		
						CASE SPCID_DM_UNLOCK_SPECIAL_ZOMBIE			SET_DIRECTOR_SPECIAL_CHARACTER_UNLOCKED(DU_SPECIAL_ZOMBIE)		BREAK			
						
						CASE SPCID_DM_UNLOCK_ANIMAL_ALL
							REPEAT MAX_DU_ANIMAL index
								SET_DIRECTOR_ANIMAL_CHARACTER_UNLOCKED(INT_TO_ENUM(DirectorUnlockAnimal, index), TRUE)
							ENDREPEAT
						BREAK
						CASE SPCID_DM_LOCK_ANIMAL_ALL					
							g_savedGlobals.sDirectorModeData.iBitsetCharacterAnimalUnlock = 0
						BREAK
						CASE SPCID_DM_UNLOCK_ANIMAL_BOAR		SET_DIRECTOR_ANIMAL_CHARACTER_UNLOCKED(DU_ANIMAL_BOAR)			BREAK		
						CASE SPCID_DM_UNLOCK_ANIMAL_CAT			SET_DIRECTOR_ANIMAL_CHARACTER_UNLOCKED(DU_ANIMAL_CAT)			BREAK						
						CASE SPCID_DM_UNLOCK_ANIMAL_COW			SET_DIRECTOR_ANIMAL_CHARACTER_UNLOCKED(DU_ANIMAL_COW)			BREAK						
						CASE SPCID_DM_UNLOCK_ANIMAL_COYOTE		SET_DIRECTOR_ANIMAL_CHARACTER_UNLOCKED(DU_ANIMAL_COYOTE)		BREAK					
						CASE SPCID_DM_UNLOCK_ANIMAL_DEER		SET_DIRECTOR_ANIMAL_CHARACTER_UNLOCKED(DU_ANIMAL_DEER)			BREAK						
						CASE SPCID_DM_UNLOCK_ANIMAL_HUSKY		SET_DIRECTOR_ANIMAL_CHARACTER_UNLOCKED(DU_ANIMAL_HUSKY)			BREAK					
						CASE SPCID_DM_UNLOCK_ANIMAL_MTLION		SET_DIRECTOR_ANIMAL_CHARACTER_UNLOCKED(DU_ANIMAL_MTLION)		BREAK					
						CASE SPCID_DM_UNLOCK_ANIMAL_PIG			SET_DIRECTOR_ANIMAL_CHARACTER_UNLOCKED(DU_ANIMAL_PIG)			BREAK						
						CASE SPCID_DM_UNLOCK_ANIMAL_POODLE		SET_DIRECTOR_ANIMAL_CHARACTER_UNLOCKED(DU_ANIMAL_POODLE)		BREAK					
						CASE SPCID_DM_UNLOCK_ANIMAL_PUG			SET_DIRECTOR_ANIMAL_CHARACTER_UNLOCKED(DU_ANIMAL_PUG)			BREAK						
						CASE SPCID_DM_UNLOCK_ANIMAL_RABBIT		SET_DIRECTOR_ANIMAL_CHARACTER_UNLOCKED(DU_ANIMAL_RABBIT)		BREAK					
						CASE SPCID_DM_UNLOCK_ANIMAL_RETRIEVER	SET_DIRECTOR_ANIMAL_CHARACTER_UNLOCKED(DU_ANIMAL_RETRIEVER)		BREAK				
						CASE SPCID_DM_UNLOCK_ANIMAL_ROTTWEILER	SET_DIRECTOR_ANIMAL_CHARACTER_UNLOCKED(DU_ANIMAL_ROTTWEILER)	BREAK				
						CASE SPCID_DM_UNLOCK_ANIMAL_SHEPHERD	SET_DIRECTOR_ANIMAL_CHARACTER_UNLOCKED(DU_ANIMAL_SHEPHERD)		BREAK					
						CASE SPCID_DM_UNLOCK_ANIMAL_WESTY		SET_DIRECTOR_ANIMAL_CHARACTER_UNLOCKED(DU_ANIMAL_WESTY)			BREAK					
						CASE SPCID_DM_UNLOCK_ANIMAL_CHICKENHAWK	SET_DIRECTOR_ANIMAL_CHARACTER_UNLOCKED(DU_ANIMAL_CHICKENHAWK)	BREAK				
						CASE SPCID_DM_UNLOCK_ANIMAL_CORMORANT	SET_DIRECTOR_ANIMAL_CHARACTER_UNLOCKED(DU_ANIMAL_CORMORANT)		BREAK				
						CASE SPCID_DM_UNLOCK_ANIMAL_CROW		SET_DIRECTOR_ANIMAL_CHARACTER_UNLOCKED(DU_ANIMAL_CROW)			BREAK						
						CASE SPCID_DM_UNLOCK_ANIMAL_HEN			SET_DIRECTOR_ANIMAL_CHARACTER_UNLOCKED(DU_ANIMAL_HEN)			BREAK						
						CASE SPCID_DM_UNLOCK_ANIMAL_PIGEON		SET_DIRECTOR_ANIMAL_CHARACTER_UNLOCKED(DU_ANIMAL_PIGEON)		BREAK					
						CASE SPCID_DM_UNLOCK_ANIMAL_SEAGULL		SET_DIRECTOR_ANIMAL_CHARACTER_UNLOCKED(DU_ANIMAL_SEAGULL)		BREAK					
						CASE SPCID_DM_UNLOCK_ANIMAL_SASQUATCH	SET_DIRECTOR_ANIMAL_CHARACTER_UNLOCKED(DU_ANIMAL_SASQUATCH)		BREAK		
						
						CASE SPCID_DM_UNLOCK_LOCATION_ALL
							REPEAT MAX_DTL index
								UNLOCK_DIRECTOR_TRAVEL_LOCATION(INT_TO_ENUM(DirectorTravelLocation, index), TRUE)
							ENDREPEAT
						BREAK
						CASE SPCID_DM_LOCK_LOCATION_ALL					
							g_savedGlobals.sDirectorModeData.iBitsetTravelLocationRevealed = 0
							SET_BIT(g_savedGlobals.sDirectorModeData.iBitsetTravelLocationRevealed, ENUM_TO_INT(DTL_USER_1))
							SET_BIT(g_savedGlobals.sDirectorModeData.iBitsetTravelLocationRevealed, ENUM_TO_INT(DTL_USER_2))
						BREAK
						CASE SPCID_DM_LOCK_LOCATION_LS_INT			UNLOCK_DIRECTOR_TRAVEL_LOCATION(DTL_LS_INTERNATIONAL)		BREAK		
						CASE SPCID_DM_LOCK_LOCATION_DOCKS			UNLOCK_DIRECTOR_TRAVEL_LOCATION(DTL_DOCKS)					BREAK			
						CASE SPCID_DM_LOCK_LOCATION_LS_DOWNTOWN		UNLOCK_DIRECTOR_TRAVEL_LOCATION(DTL_LS_DOWNTOWN)			BREAK		
						CASE SPCID_DM_LOCK_LOCATION_DEL_PERRO_PIER	UNLOCK_DIRECTOR_TRAVEL_LOCATION(DTL_DEL_PERRO_PIER)			BREAK	
						CASE SPCID_DM_LOCK_LOCATION_VINEWOOD_HILLS	UNLOCK_DIRECTOR_TRAVEL_LOCATION(DTL_VINEWOOD_HILLS)			BREAK	
						CASE SPCID_DM_LOCK_LOCATION_WIND_FARM		UNLOCK_DIRECTOR_TRAVEL_LOCATION(DTL_WIND_FARM)				BREAK		
						CASE SPCID_DM_LOCK_LOCATION_SANDY_SHORES	UNLOCK_DIRECTOR_TRAVEL_LOCATION(DTL_SANDY_SHORES)			BREAK		
						CASE SPCID_DM_LOCK_LOCATION_STAB_CITY		UNLOCK_DIRECTOR_TRAVEL_LOCATION(DTL_STAB_CITY)				BREAK		
						CASE SPCID_DM_LOCK_LOCATION_FORT_ZANCUDO	UNLOCK_DIRECTOR_TRAVEL_LOCATION(DTL_FORT_ZANCUDO)			BREAK		
						CASE SPCID_DM_LOCK_LOCATION_CANYON			UNLOCK_DIRECTOR_TRAVEL_LOCATION(DTL_CANYON)					BREAK			
						CASE SPCID_DM_LOCK_LOCATION_CABLE_CAR		UNLOCK_DIRECTOR_TRAVEL_LOCATION(DTL_CABLE_CAR)				BREAK		
						CASE SPCID_DM_LOCK_LOCATION_PALETO			UNLOCK_DIRECTOR_TRAVEL_LOCATION(DTL_PALETO)					BREAK			
						CASE SPCID_DM_LOCK_LOCATION_LIGHTHOUSE		UNLOCK_DIRECTOR_TRAVEL_LOCATION(DTL_LIGHTHOUSE)				BREAK		

						DEFAULT
					#ENDIF	
							//-------------------- CLIFFORD SPECIFIC ----------------------
							IF SP_RUN_SCRIPT_FOR_SWITCH_CODEID(INT_TO_ENUM(SP_MENU_CODE_ID_ENUM, (sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].iCodeID)))
								RETURN TRUE
							ENDIF
							IF SP_RUN_SCRIPT_FOR_FAMILY_CODEID(INT_TO_ENUM(SP_MENU_CODE_ID_ENUM, (sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].iCodeID)))
								RETURN TRUE
							ENDIF
							
							PRINTNL()
							PRINTSTRING("Debug Menu - Invalid codeID '")
							PRINTSTRING(GET_STRING_FROM_HASH_KEY(sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].iCodeID))
							PRINTSTRING("'")
							PRINTNL()
							
							SCRIPT_ASSERT("Debug Menu - Invalid codeID")
					#if FEATURE_SP_DLC_DIRECTOR_MODE	
						BREAK						
					ENDSWITCH
					#ENDIF
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

//Runs any Code ID set in the default codeID param of the menu item.
FUNC BOOL SP_RUN_SCRIPT_FOR_CODEID_POST_GAMEFLOW_LAUNCH(DEBUG_MENU_DATA_STRUCT &sDebugMenuData)
	RETURN SP_RUN_SCRIPT_FOR_CODEID(sDebugMenuData, FALSE)
ENDFUNC

//Runs any Code ID set in the pre-launch codeID param of the menu item.
FUNC BOOL SP_RUN_SCRIPT_FOR_CODEID_PRE_GAMEFLOW_LAUNCH(DEBUG_MENU_DATA_STRUCT &sDebugMenuData)
	RETURN SP_RUN_SCRIPT_FOR_CODEID(sDebugMenuData, TRUE)
ENDFUNC

/// PURPOSE: Checks to see if a codeItem should be displayed
PROC SP_RUN_SCRIPT_FOR_DISPLAYID(DEBUG_MENU_DATA_STRUCT &sDebugMenuData, INT iDepth)

	INT iCount
	
	FOR iCount = 0 TO sDebugMenuData.iItemCount[iDepth]-1
	
		SET_DEBUG_MENU_ITEM_FLAG(sDebugMenuData.sItems[iDepth][iCount].iFlags, DM_BIT_DISPLAY)
		
		SWITCH INT_TO_ENUM(SP_MENU_DISPLAY_ID_ENUM, (sDebugMenuData.sItems[iDepth][iCount].iDisplayID))
			
			CASE SPDID_DEFAULT
				SET_DEBUG_MENU_ITEM_FLAG(sDebugMenuData.sItems[iDepth][iCount].iFlags, DM_BIT_DISPLAY)
			BREAK
			
			CASE SPDID_DONOTDISPLAY
				CLEAR_DEBUG_MENU_ITEM_FLAG(sDebugMenuData.sItems[iDepth][iCount].iFlags, DM_BIT_DISPLAY)
			BREAK
			
			CASE SPDID_GAMEFLOW_ON
				// If gameflow mode is ON, then turn the 'switch to gameflow mode' option off
				IF (g_savedGlobals.sFlow.isGameflowActive)
					CLEAR_DEBUG_MENU_ITEM_FLAG(sDebugMenuData.sItems[iDepth][iCount].iFlags, DM_BIT_DISPLAY)
				ENDIF
			BREAK
			
			CASE SPDID_GAMEFLOW_OFF
				// If gameflow mode is OFF, then turn the 'switch to debug mode' option off
				IF NOT (g_savedGlobals.sFlow.isGameflowActive)
					CLEAR_DEBUG_MENU_ITEM_FLAG(sDebugMenuData.sItems[iDepth][iCount].iFlags, DM_BIT_DISPLAY)
				ENDIF
			BREAK
			
			CASE SPDID_RANDOM_EVENTS_ON
				// If Random Events are ON then turn the 'switch on Random Events' option off
				IF (Get_Mission_Flow_Flag_State(FLOWFLAG_ALLOW_RANDOM_EVENTS))
					CLEAR_DEBUG_MENU_ITEM_FLAG(sDebugMenuData.sItems[iDepth][iCount].iFlags, DM_BIT_DISPLAY)
				ENDIF
			BREAK
			
			CASE SPDID_RANDOM_EVENTS_OFF
				// If Random Events are OFF then turn the 'switch off Random Events' option off
				IF NOT (Get_Mission_Flow_Flag_State(FLOWFLAG_ALLOW_RANDOM_EVENTS))
					CLEAR_DEBUG_MENU_ITEM_FLAG(sDebugMenuData.sItems[iDepth][iCount].iFlags, DM_BIT_DISPLAY)
				ENDIF
			BREAK
			
			CASE SPDID_LBDBUGS_ON
				// If g_bDisplayLBDBugSummary is TRUE, then turn the 'Turn LBD bug summary on' option off
				IF (g_bDisplayLBDBugSummary)
					CLEAR_DEBUG_MENU_ITEM_FLAG(sDebugMenuData.sItems[iDepth][iCount].iFlags, DM_BIT_DISPLAY)
				ENDIF
			BREAK
			
			CASE SPDID_LBDBUGS_OFF
				// If g_bDisplayLBDBugSummary is FALSE, then turn the 'Turn LBD bug summary off' option off
				IF NOT (g_bDisplayLBDBugSummary)
					CLEAR_DEBUG_MENU_ITEM_FLAG(sDebugMenuData.sItems[iDepth][iCount].iFlags, DM_BIT_DISPLAY)
				ENDIF
			BREAK

			CASE SPDID_LAST_GEN_PLAYER_ON
				// If g_bLastGenPlayer is TRUE, then turn the 'Switch to Last Gen Player' option off
				IF (g_bLastGenPlayer)
					CLEAR_DEBUG_MENU_ITEM_FLAG(sDebugMenuData.sItems[iDepth][iCount].iFlags, DM_BIT_DISPLAY)
				ENDIF
			BREAK
			
			CASE SPDID_LAST_GEN_PLAYER_OFF
				// If g_bLastGenPlayer is FALSE, then turn the 'Switch off Last Gen Player' option off
				IF NOT (g_bLastGenPlayer)
					CLEAR_DEBUG_MENU_ITEM_FLAG(sDebugMenuData.sItems[iDepth][iCount].iFlags, DM_BIT_DISPLAY)
				ENDIF
			BREAK
			
			CASE SPDID_LAST_GEN_SE_PLAYER_ON
				// If g_bLastGenSpecialEditionPlayer is TRUE, then turn the 'Switch to Last Gen Special Edition Player' option off
				IF (g_bLastGenSpecialEditionPlayer)
					CLEAR_DEBUG_MENU_ITEM_FLAG(sDebugMenuData.sItems[iDepth][iCount].iFlags, DM_BIT_DISPLAY)
				ENDIF
			BREAK
			
			CASE SPDID_LAST_GEN_SE_PLAYER_OFF
				// If g_bLastGenSpecialEditionPlayer is FALSE, then turn the 'Switch off Last Gen Special Edition Player' option off
				IF NOT (g_bLastGenSpecialEditionPlayer)
					CLEAR_DEBUG_MENU_ITEM_FLAG(sDebugMenuData.sItems[iDepth][iCount].iFlags, DM_BIT_DISPLAY)
				ENDIF
			BREAK
			
			CASE SPDID_LAST_GEN_CE_PLAYER_ON
				// If g_bLastGenCollectorEditionPlayer is TRUE, then turn the 'Switch to Last Gen Collector Edition Player' option off
				IF (g_bLastGenCollectorEditionPlayer)
					CLEAR_DEBUG_MENU_ITEM_FLAG(sDebugMenuData.sItems[iDepth][iCount].iFlags, DM_BIT_DISPLAY)
				ENDIF
			BREAK
			
			CASE SPDID_LAST_GEN_CE_PLAYER_OFF
				// If g_bLastGenCollectorEditionPlayer is FALSE, then turn the 'Switch off Last Gen Collector Edition Player' option off
				IF NOT (g_bLastGenCollectorEditionPlayer)
					CLEAR_DEBUG_MENU_ITEM_FLAG(sDebugMenuData.sItems[iDepth][iCount].iFlags, DM_BIT_DISPLAY)
				ENDIF
			BREAK
			
			CASE SPDID_PREORDER_ON
				// If g_bPreorderGame is TRUE, then turn the 'Switch to Special Edition game' option off
				IF (g_bPreorderGame)
					CLEAR_DEBUG_MENU_ITEM_FLAG(sDebugMenuData.sItems[iDepth][iCount].iFlags, DM_BIT_DISPLAY)
				ENDIF
			BREAK
			
			CASE SPDID_PREORDER_OFF
				// If g_bPreorderGame is FALSE, then turn the 'Switch to Standard game' option off
				IF NOT (g_bPreorderGame)
					CLEAR_DEBUG_MENU_ITEM_FLAG(sDebugMenuData.sItems[iDepth][iCount].iFlags, DM_BIT_DISPLAY)
				ENDIF
			BREAK
			
			CASE SPDID_PREORDER_NG_ON
				// If g_bPreorderGameNextGen is TRUE, then turn the 'Switch to NG Preorder Game' option off
				IF (g_bPreorderGameNextGen)
					CLEAR_DEBUG_MENU_ITEM_FLAG(sDebugMenuData.sItems[iDepth][iCount].iFlags, DM_BIT_DISPLAY)
				ENDIF
			BREAK
			
			CASE SPDID_PREORDER_NG_OFF
				// If g_bPreorderGame is FALSE, then turn the 'Switch to NG Preorder Game' option off
				IF NOT (g_bPreorderGameNextGen)
					CLEAR_DEBUG_MENU_ITEM_FLAG(sDebugMenuData.sItems[iDepth][iCount].iFlags, DM_BIT_DISPLAY)
				ENDIF
			BREAK

			CASE SPDID_JAPANESE_SE_ON
				// If g_bJapaneseSpecialEditionGame is TRUE, then turn the 'Switch to Japanese Special Edition game' option off
				IF (g_bJapaneseSpecialEditionGame)
					CLEAR_DEBUG_MENU_ITEM_FLAG(sDebugMenuData.sItems[iDepth][iCount].iFlags, DM_BIT_DISPLAY)
				ENDIF
			BREAK
			
			CASE SPDID_JAPANESE_SE_OFF
				// If g_bJapaneseSpecialEditionGame is FALSE, then turn the 'Switch to Standard game' option off
				IF NOT (g_bJapaneseSpecialEditionGame)
					CLEAR_DEBUG_MENU_ITEM_FLAG(sDebugMenuData.sItems[iDepth][iCount].iFlags, DM_BIT_DISPLAY)
				ENDIF
			BREAK
			
			CASE SPDID_PED_MICHAEL
				IF IS_PED_THE_CURRENT_PLAYER_PED(CHAR_MICHAEL)
					CLEAR_DEBUG_MENU_ITEM_FLAG(sDebugMenuData.sItems[iDepth][iCount].iFlags, DM_BIT_DISPLAY)
				ENDIF
			BREAK
			
			CASE SPDID_PED_FRANKLIN
				IF IS_PED_THE_CURRENT_PLAYER_PED(CHAR_FRANKLIN)
					CLEAR_DEBUG_MENU_ITEM_FLAG(sDebugMenuData.sItems[iDepth][iCount].iFlags, DM_BIT_DISPLAY)
				ENDIF
			BREAK
			
			CASE SPDID_PED_TREVOR
				IF IS_PED_THE_CURRENT_PLAYER_PED(CHAR_TREVOR)
					CLEAR_DEBUG_MENU_ITEM_FLAG(sDebugMenuData.sItems[iDepth][iCount].iFlags, DM_BIT_DISPLAY)
				ENDIF
			BREAK
			
			CASE SPDID_PRM_DEFAULT
				enumCharacterList paramMichaelCharID
				paramMichaelCharID = CHAR_MICHAEL
				
				IF (NOT Is_TimeOfDay_Valid(g_savedGlobals.sPlayerData.sInfo.sLastTimeActive[paramMichaelCharID]))
				OR ARE_VECTORS_EQUAL(g_savedGlobals.sPlayerData.sInfo.vLastKnownCoords[paramMichaelCharID], <<0,0,0>>)
					CLEAR_DEBUG_MENU_ITEM_FLAG(sDebugMenuData.sItems[iDepth][iCount].iFlags, DM_BIT_DISPLAY)
				ENDIF
			BREAK
			CASE SPDID_PRF_DEFAULT
				enumCharacterList paramFranklinCharID
				paramFranklinCharID = CHAR_FRANKLIN
				
				IF (NOT Is_TimeOfDay_Valid(g_savedGlobals.sPlayerData.sInfo.sLastTimeActive[paramFranklinCharID]))
				OR ARE_VECTORS_EQUAL(g_savedGlobals.sPlayerData.sInfo.vLastKnownCoords[paramFranklinCharID], <<0,0,0>>)
					CLEAR_DEBUG_MENU_ITEM_FLAG(sDebugMenuData.sItems[iDepth][iCount].iFlags, DM_BIT_DISPLAY)
				ENDIF
			BREAK
			CASE SPDID_PRT_DEFAULT
				enumCharacterList paramTrevorCharID
				paramTrevorCharID = CHAR_TREVOR
				
				IF (NOT Is_TimeOfDay_Valid(g_savedGlobals.sPlayerData.sInfo.sLastTimeActive[paramTrevorCharID]))
				OR ARE_VECTORS_EQUAL(g_savedGlobals.sPlayerData.sInfo.vLastKnownCoords[paramTrevorCharID], <<0,0,0>>)
					CLEAR_DEBUG_MENU_ITEM_FLAG(sDebugMenuData.sItems[iDepth][iCount].iFlags, DM_BIT_DISPLAY)
				ENDIF
			BREAK
			
			CASE SPDID_FRIADD_MICHAEL
				IF IS_PED_THE_CURRENT_PLAYER_PED(CHAR_MICHAEL)
					CLEAR_DEBUG_MENU_ITEM_FLAG(sDebugMenuData.sItems[iDepth][iCount].iFlags, DM_BIT_DISPLAY)
				ELSE
					IF ARE_CHARS_FRIENDS(GET_CURRENT_PLAYER_PED_ENUM(), CHAR_MICHAEL)
						CLEAR_DEBUG_MENU_ITEM_FLAG(sDebugMenuData.sItems[iDepth][iCount].iFlags, DM_BIT_DISPLAY)
					ENDIF
				ENDIF
			BREAK
			CASE SPDID_FRIBLK_MICHAEL
				IF IS_PED_THE_CURRENT_PLAYER_PED(CHAR_MICHAEL)
					CLEAR_DEBUG_MENU_ITEM_FLAG(sDebugMenuData.sItems[iDepth][iCount].iFlags, DM_BIT_DISPLAY)
				ELSE
					IF NOT ARE_CHARS_FRIENDS(GET_CURRENT_PLAYER_PED_ENUM(), CHAR_MICHAEL)
						CLEAR_DEBUG_MENU_ITEM_FLAG(sDebugMenuData.sItems[iDepth][iCount].iFlags, DM_BIT_DISPLAY)
					ENDIF
				ENDIF
			BREAK
			
			CASE SPDID_FRIADD_FRANKLIN
				IF IS_PED_THE_CURRENT_PLAYER_PED(CHAR_FRANKLIN)
					CLEAR_DEBUG_MENU_ITEM_FLAG(sDebugMenuData.sItems[iDepth][iCount].iFlags, DM_BIT_DISPLAY)
				ELSE
					IF ARE_CHARS_FRIENDS(GET_CURRENT_PLAYER_PED_ENUM(), CHAR_FRANKLIN)
						CLEAR_DEBUG_MENU_ITEM_FLAG(sDebugMenuData.sItems[iDepth][iCount].iFlags, DM_BIT_DISPLAY)
					ENDIF
				ENDIF
			BREAK
			CASE SPDID_FRIBLK_FRANKLIN
				IF IS_PED_THE_CURRENT_PLAYER_PED(CHAR_FRANKLIN)
					CLEAR_DEBUG_MENU_ITEM_FLAG(sDebugMenuData.sItems[iDepth][iCount].iFlags, DM_BIT_DISPLAY)
				ELSE
					IF NOT ARE_CHARS_FRIENDS(GET_CURRENT_PLAYER_PED_ENUM(), CHAR_FRANKLIN)
						CLEAR_DEBUG_MENU_ITEM_FLAG(sDebugMenuData.sItems[iDepth][iCount].iFlags, DM_BIT_DISPLAY)
					ENDIF
				ENDIF
			BREAK
			
			CASE SPDID_FRIADD_TREVOR
				IF IS_PED_THE_CURRENT_PLAYER_PED(CHAR_TREVOR)
					CLEAR_DEBUG_MENU_ITEM_FLAG(sDebugMenuData.sItems[iDepth][iCount].iFlags, DM_BIT_DISPLAY)
				ELSE
					IF ARE_CHARS_FRIENDS(GET_CURRENT_PLAYER_PED_ENUM(), CHAR_TREVOR)
						CLEAR_DEBUG_MENU_ITEM_FLAG(sDebugMenuData.sItems[iDepth][iCount].iFlags, DM_BIT_DISPLAY)
					ENDIF
				ENDIF
			BREAK
			CASE SPDID_FRIBLK_TREVOR
				IF IS_PED_THE_CURRENT_PLAYER_PED(CHAR_TREVOR)
					CLEAR_DEBUG_MENU_ITEM_FLAG(sDebugMenuData.sItems[iDepth][iCount].iFlags, DM_BIT_DISPLAY)
				ELSE
					IF NOT ARE_CHARS_FRIENDS(GET_CURRENT_PLAYER_PED_ENUM(), CHAR_TREVOR)
						CLEAR_DEBUG_MENU_ITEM_FLAG(sDebugMenuData.sItems[iDepth][iCount].iFlags, DM_BIT_DISPLAY)
					ENDIF
				ENDIF
			BREAK
			
			CASE SPDID_FRIADD_LAMAR
				IF NOT IS_PED_THE_CURRENT_PLAYER_PED(CHAR_FRANKLIN)
					CLEAR_DEBUG_MENU_ITEM_FLAG(sDebugMenuData.sItems[iDepth][iCount].iFlags, DM_BIT_DISPLAY)
				ELSE
					IF ARE_CHARS_FRIENDS(GET_CURRENT_PLAYER_PED_ENUM(), CHAR_LAMAR)
						CLEAR_DEBUG_MENU_ITEM_FLAG(sDebugMenuData.sItems[iDepth][iCount].iFlags, DM_BIT_DISPLAY)
					ENDIF
				ENDIF
			BREAK
			CASE SPDID_FRIBLK_LAMAR
				IF NOT IS_PED_THE_CURRENT_PLAYER_PED(CHAR_FRANKLIN)
					CLEAR_DEBUG_MENU_ITEM_FLAG(sDebugMenuData.sItems[iDepth][iCount].iFlags, DM_BIT_DISPLAY)
				ELSE
					IF NOT ARE_CHARS_FRIENDS(GET_CURRENT_PLAYER_PED_ENUM(), CHAR_LAMAR)
						CLEAR_DEBUG_MENU_ITEM_FLAG(sDebugMenuData.sItems[iDepth][iCount].iFlags, DM_BIT_DISPLAY)
					ENDIF
				ENDIF
			BREAK
			
			CASE SPDID_LOAD_PROLOGUE_MAP
				IF g_bPrologueMapLoaded
					CLEAR_DEBUG_MENU_ITEM_FLAG(sDebugMenuData.sItems[iDepth][iCount].iFlags, DM_BIT_DISPLAY)
				ENDIF
			BREAK
			
			CASE SPDID_LOAD_MAIN_MAP
				IF NOT g_bPrologueMapLoaded
					CLEAR_DEBUG_MENU_ITEM_FLAG(sDebugMenuData.sItems[iDepth][iCount].iFlags, DM_BIT_DISPLAY)
				ENDIF
			BREAK
			
			CASE SPDID_KEEP_AMMO
				IF g_bResetAmmoNewGame
					CLEAR_DEBUG_MENU_ITEM_FLAG(sDebugMenuData.sItems[iDepth][iCount].iFlags, DM_BIT_DISPLAY)
				ENDIF
			BREAK
			
			CASE SPDID_RESET_AMMO
				IF NOT g_bResetAmmoNewGame
					CLEAR_DEBUG_MENU_ITEM_FLAG(sDebugMenuData.sItems[iDepth][iCount].iFlags, DM_BIT_DISPLAY)
				ENDIF
			BREAK
			
			CASE SPDID_LOAD_OLD_H_IPLS
				IF NOT g_bTurnOnNewHIPLs
					CLEAR_DEBUG_MENU_ITEM_FLAG(sDebugMenuData.sItems[iDepth][iCount].iFlags, DM_BIT_DISPLAY)
				ENDIF
			BREAK
			CASE SPDID_LOAD_NEW_H_IPLS
				IF g_bTurnOnNewHIPLs
					CLEAR_DEBUG_MENU_ITEM_FLAG(sDebugMenuData.sItems[iDepth][iCount].iFlags, DM_BIT_DISPLAY)
				ENDIF
			BREAK
			
			
			CASE SPDID_PEYOTE_LOWLAND_LABELS_ON
				IF NOT g_bDebugPeyotePickupLabels[APT_LOWLAND]
					CLEAR_DEBUG_MENU_ITEM_FLAG(sDebugMenuData.sItems[iDepth][iCount].iFlags, DM_BIT_DISPLAY)
				ENDIF
			BREAK
			CASE SPDID_PEYOTE_LOWLAND_LABELS_OFF
				IF g_bDebugPeyotePickupLabels[APT_LOWLAND]
					CLEAR_DEBUG_MENU_ITEM_FLAG(sDebugMenuData.sItems[iDepth][iCount].iFlags, DM_BIT_DISPLAY)
				ENDIF
			BREAK
			CASE SPDID_PEYOTE_HIGHLAND_LABELS_ON
				IF NOT g_bDebugPeyotePickupLabels[APT_HIGHLAND]
					CLEAR_DEBUG_MENU_ITEM_FLAG(sDebugMenuData.sItems[iDepth][iCount].iFlags, DM_BIT_DISPLAY)
				ENDIF
			BREAK
			CASE SPDID_PEYOTE_HIGHLAND_LABELS_OFF
				IF g_bDebugPeyotePickupLabels[APT_HIGHLAND]
					CLEAR_DEBUG_MENU_ITEM_FLAG(sDebugMenuData.sItems[iDepth][iCount].iFlags, DM_BIT_DISPLAY)
				ENDIF
			BREAK
			CASE SPDID_PEYOTE_WATER_LABELS_ON
				IF NOT g_bDebugPeyotePickupLabels[APT_UNDERWATER]
					CLEAR_DEBUG_MENU_ITEM_FLAG(sDebugMenuData.sItems[iDepth][iCount].iFlags, DM_BIT_DISPLAY)
				ENDIF
			BREAK
			CASE SPDID_PEYOTE_WATER_LABELS_OFF
				IF g_bDebugPeyotePickupLabels[APT_UNDERWATER]
					CLEAR_DEBUG_MENU_ITEM_FLAG(sDebugMenuData.sItems[iDepth][iCount].iFlags, DM_BIT_DISPLAY)
				ENDIF
			BREAK
			CASE SPDID_PEYOTE_SECRET_LABELS_ON
				IF NOT g_bDebugPeyotePickupLabels[APT_SECRET]
					CLEAR_DEBUG_MENU_ITEM_FLAG(sDebugMenuData.sItems[iDepth][iCount].iFlags, DM_BIT_DISPLAY)
				ENDIF
			BREAK
			CASE SPDID_PEYOTE_SECRET_LABELS_OFF
				IF g_bDebugPeyotePickupLabels[APT_SECRET]
					CLEAR_DEBUG_MENU_ITEM_FLAG(sDebugMenuData.sItems[iDepth][iCount].iFlags, DM_BIT_DISPLAY)
				ENDIF
			BREAK
			
			
			CASE SPDID_PEYOTE_LOWLAND_BLIPS_ON
				IF NOT g_bDebugPeyotePickupBlips[APT_LOWLAND]
					CLEAR_DEBUG_MENU_ITEM_FLAG(sDebugMenuData.sItems[iDepth][iCount].iFlags, DM_BIT_DISPLAY)
				ENDIF
			BREAK
			CASE SPDID_PEYOTE_LOWLAND_BLIPS_OFF
				IF g_bDebugPeyotePickupBlips[APT_LOWLAND]
					CLEAR_DEBUG_MENU_ITEM_FLAG(sDebugMenuData.sItems[iDepth][iCount].iFlags, DM_BIT_DISPLAY)
				ENDIF
			BREAK
			CASE SPDID_PEYOTE_HIGHLAND_BLIPS_ON
				IF NOT g_bDebugPeyotePickupBlips[APT_HIGHLAND]
					CLEAR_DEBUG_MENU_ITEM_FLAG(sDebugMenuData.sItems[iDepth][iCount].iFlags, DM_BIT_DISPLAY)
				ENDIF
			BREAK
			CASE SPDID_PEYOTE_HIGHLAND_BLIPS_OFF
				IF g_bDebugPeyotePickupBlips[APT_HIGHLAND]
					CLEAR_DEBUG_MENU_ITEM_FLAG(sDebugMenuData.sItems[iDepth][iCount].iFlags, DM_BIT_DISPLAY)
				ENDIF
			BREAK
			CASE SPDID_PEYOTE_WATER_BLIPS_ON
				IF NOT g_bDebugPeyotePickupBlips[APT_UNDERWATER]
					CLEAR_DEBUG_MENU_ITEM_FLAG(sDebugMenuData.sItems[iDepth][iCount].iFlags, DM_BIT_DISPLAY)
				ENDIF
			BREAK
			CASE SPDID_PEYOTE_WATER_BLIPS_OFF
				IF g_bDebugPeyotePickupBlips[APT_UNDERWATER]
					CLEAR_DEBUG_MENU_ITEM_FLAG(sDebugMenuData.sItems[iDepth][iCount].iFlags, DM_BIT_DISPLAY)
				ENDIF
			BREAK
			CASE SPDID_PEYOTE_SECRET_BLIPS_ON
				IF NOT g_bDebugPeyotePickupBlips[APT_SECRET]
					CLEAR_DEBUG_MENU_ITEM_FLAG(sDebugMenuData.sItems[iDepth][iCount].iFlags, DM_BIT_DISPLAY)
				ENDIF
			BREAK
			CASE SPDID_PEYOTE_SECRET_BLIPS_OFF
				IF g_bDebugPeyotePickupBlips[APT_SECRET]
					CLEAR_DEBUG_MENU_ITEM_FLAG(sDebugMenuData.sItems[iDepth][iCount].iFlags, DM_BIT_DISPLAY)
				ENDIF
			BREAK
			
			DEFAULT
				SCRIPT_ASSERT("Debug Menu - Invalid displayID")
				PRINTNL()
				PRINTSTRING("Debug Menu - Invalid displayID '")
				PRINTSTRING(GET_STRING_FROM_HASH_KEY(sDebugMenuData.sItems[iDepth][iCount].iDisplayID))
				PRINTSTRING("'")
				PRINTNL()
			BREAK
			
		ENDSWITCH
		
	ENDFOR
	
ENDPROC


FUNC BOOL DEBUG_LAUNCH_TO_MISSION(DEBUG_MENU_DATA_STRUCT &sDebugMenuData, FLOW_LAUNCHER_VARS &flowLauncher)

	// Run the flow launcher to update the gameflow to a plausible state.
	TEXT_LABEL_63 scriptFile = sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].sFile
	
	CPRINTLN(DEBUG_FLOW_LAUNCHER, "Debug launching to mission ", scriptFile)
	RETURN LAUNCHER_LAUNCH_TO_MISSION_SCRIPT(flowLauncher, scriptFile)

	// MissionID (ARM_1, ARM2, JWL_S1 etc.)
	// sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].sMissionID
	
	// Mission script (armenian1.sc, armenian2.sc, jewelry_setup1.sc etc)
	// sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].sFile
		
	// Use this piece of script if you want to chop of the .sc to keep things consistent
//		TEXT_LABEL_63 scriptFile = sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].sFile
//		IF ARE_STRINGS_EQUAL(GET_STRING_FROM_STRING(scriptFile, GET_LENGTH_OF_LITERAL_STRING(scriptFile)-3, GET_LENGTH_OF_LITERAL_STRING(scriptFile)), ".sc")
//		scriptFile = GET_STRING_FROM_STRING(scriptFile, 0, GET_LENGTH_OF_LITERAL_STRING(scriptFile)-3)
ENDFUNC


FUNC BOOL DEBUG_LAUNCH_TO_BEFORE_MISSION(DEBUG_MENU_DATA_STRUCT &sDebugMenuData, FLOW_LAUNCHER_VARS &flowLauncher)
	TEXT_LABEL_63 scriptFile = sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].sFile
	
	CPRINTLN(DEBUG_FLOW_LAUNCHER, "Debug launching to before mission ", scriptFile)
	RETURN LAUNCHER_LAUNCH_TO_BEFORE_MISSION_SCRIPT(flowLauncher, scriptFile)
ENDFUNC


FUNC BOOL DEBUG_LAUNCH_TO_AFTER_MISSION(DEBUG_MENU_DATA_STRUCT &sDebugMenuData, FLOW_LAUNCHER_VARS &flowLauncher)
	TEXT_LABEL_63 scriptFile = sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].sFile
	
	CPRINTLN(DEBUG_FLOW_LAUNCHER, "Debug launching to after mission ", scriptFile)
	RETURN LAUNCHER_LAUNCH_TO_AFTER_MISSION_SCRIPT(flowLauncher, scriptFile)
ENDFUNC



/// PURPOSE: Set the game to the appropriate state if a mission has just been selected
/// RETRUNS: True if the game has launched to a mission using the flow launcher.
PROC SP_PREPARE_GAME_STATE(DEBUG_MENU_DATA_STRUCT &sDebugMenuData)
	
	// Only prepare the game state if we are in gameflow mode
	IF (!g_bLoadedClifford and !g_bLoadedNorman and g_savedGlobals.sFlow.isGameflowActive)
	OR (g_bLoadedClifford and g_savedGlobalsClifford.sFlow.isGameflowActive)
	OR (g_bLoadedNorman and g_savedGlobalsnorman.sFlow.isGameflowActive)
	
		// Check if a mission was selected.
		IF sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].iType = DM_HASH_ITEM_MISSION
		OR sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].iType = DM_HASH_ITEM_BUGSTAR
		
			#IF IS_DEBUG_BUILD
				PRINTLN("SP_Prepare_Game_State: Running flow launch for mission script.")
			#ENDIF
			
			SET_DEBUG_MENU_CUSTOM_PROCESS_FLAG(sDebugMenuData.iCustomProcessFlags, DM_CUSTOM_PROCESS_LAUNCH_TO_MISSION)
		//Check if the menu item has a label ID set.
		ELIF sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].iLabelID <> 0
			//Check if there are extra labels set, requiring a multi label launch.
			IF sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].iExtraLabelID[0] <> 0
				SET_DEBUG_MENU_CUSTOM_PROCESS_FLAG(sDebugMenuData.iCustomProcessFlags, DM_CUSTOM_PROCESS_LAUNCHER_MULTI_LABEL_LAUNCH)
			ELSE
				SET_DEBUG_MENU_CUSTOM_PROCESS_FLAG(sDebugMenuData.iCustomProcessFlags, DM_CUSTOM_PROCESS_LAUNCHER_LAUNCH_FLOW_TO_LABEL)
			ENDIF
		ELSE
			//Do none mission specific launch steps.
			SET_DEBUG_MENU_CUSTOM_PROCESS_FLAG(sDebugMenuData.iCustomProcessFlags, DM_CUSTOM_PROCESS_LAUNCH_TO_MISSION_OUTCOME)
		ENDIF
		
	ELSE
		//Do none mission specific launch steps.
		SET_DEBUG_MENU_CUSTOM_PROCESS_FLAG(sDebugMenuData.iCustomProcessFlags, DM_CUSTOM_PROCESS_LAUNCH_TO_MISSION_OUTCOME)
	ENDIF
	
ENDPROC

/// PURPOSE: Checks that a script is safe to run
FUNC BOOL SP_CHECK_SCRIPT(STRING scriptFile)
	
	IF DOES_SCRIPT_EXIST(scriptFile)
	
		IF GET_HASH_KEY(scriptFile) = SCRIPT_CHECK_01
		//OR GET_HASH_KEY(scriptFile) = SCRIPT_CHECK_02
		
			INT iKey, iTime
			TEXT_LABEL_15 sKey = ""
			TEXT_LABEL_15 sDisplayText
			BOOL bComplete = FALSE
			BOOL bPass = FALSE
			
			WHILE NOT bComplete
			AND NOT IS_KEYBOARD_KEY_JUST_PRESSED(KEY_ESCAPE)
			AND NOT IS_GAME_KEYBOARD_KEY_JUST_PRESSED(KEY_ESCAPE)
			
				WAIT(0)
				
				// Make sure we stay in game keyboard
				IF GET_KEYBOARD_MODE() <> KEYBOARD_MODE_GAME
					SET_KEYBOARD_MODE(KEYBOARD_MODE_GAME)
				ENDIF
				
				// Add key
				IF GET_LENGTH_OF_LITERAL_STRING(sKey) < 14
					FOR iKey = 0 TO 35
						IF IS_GAME_KEYBOARD_KEY_JUST_PRESSED(DM_GET_DEBUG_KEY_FOR_INT(iKey))
							sKey += DM_GET_STRING_FOR_DEBUG_KEY(DM_GET_DEBUG_KEY_FOR_INT(iKey))
							iTime = GET_GAME_TIMER()
						ENDIF
					ENDFOR
				ENDIF
				
				// Remove key
				IF IS_GAME_KEYBOARD_KEY_JUST_PRESSED(KEY_BACK)
					IF GET_LENGTH_OF_LITERAL_STRING(sKey) > 1
						sKey = GET_STRING_FROM_STRING(sKey, 0, GET_LENGTH_OF_LITERAL_STRING(sKey)-1)
					ELSE
						sKey = ""
					ENDIF
					iTime = 0
				ENDIF
				
				// Key entered
				IF IS_GAME_KEYBOARD_KEY_JUST_PRESSED(KEY_RETURN)
					bComplete = TRUE
				ENDIF
				
				// Display key
				sDisplayText = " "
				FOR iKey = 0 TO GET_LENGTH_OF_LITERAL_STRING(sKey)-1
					// Display last if in time
					IF iKey = GET_LENGTH_OF_LITERAL_STRING(sKey)-1
						IF GET_GAME_TIMER() - iTime < 600
							sDisplayText += GET_STRING_FROM_STRING(sKey, iKey, iKey+1)
						ELSE
							sDisplayText +="*"
						ENDIF
					ELSE
						sDisplayText +="*"
					ENDIF
				ENDFOR
				SET_TEXT_SCALE(0.5, 0.65)
				SET_TEXT_COLOUR(255, 255, 255, 255)
				SET_TEXT_CENTRE(TRUE)
				SET_TEXT_DROPSHADOW(0, 0, 0, 0, 0)
				SET_TEXT_EDGE(0, 0, 0, 0, 0)
				SET_TEXT_PROPORTIONAL(TRUE)
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.5, 0.6, "STRING", sDisplayText)
				
				// Display message
				
				sDisplayText = "D"
				sDisplayText += "M"
				sDisplayText += "_"
				sDisplayText += "M"
				sDisplayText += "A"
				sDisplayText += "R"
				sDisplayText += "K"
				sDisplayText += "E"
				sDisplayText += "T"
				IF DOES_TEXT_LABEL_EXIST(sDisplayText)
					SET_TEXT_SCALE(0.5, 0.5)
					SET_TEXT_COLOUR(255, 255, 255, 255)
					SET_TEXT_CENTRE(TRUE)
					SET_TEXT_DROPSHADOW(0, 0, 0, 0, 0)
					SET_TEXT_EDGE(0, 0, 0, 0, 0)
					DISPLAY_TEXT(0.5, 0.5, sDisplayText)
				ENDIF
			ENDWHILE
			
			// Check input
			IF bComplete
				bPass = (GET_HASH_KEY(sKey) = DM_HASH_SCRIPT_KEY)
				
				IF bPass
					sDisplayText = "D"
					sDisplayText += "M"
					sDisplayText += "_"
					sDisplayText += "M"
					sDisplayText += "A"
					sDisplayText += "R"
					sDisplayText += "K"
					sDisplayText += "E"
					sDisplayText += "T"
					sDisplayText += "_"
					sDisplayText += "P"
				ELSE
					sDisplayText = "D"
					sDisplayText += "M"
					sDisplayText += "_"
					sDisplayText += "M"
					sDisplayText += "A"
					sDisplayText += "R"
					sDisplayText += "K"
					sDisplayText += "E"
					sDisplayText += "T"
					sDisplayText += "_"
					sDisplayText += "F"
				ENDIF
				
				iTime = GET_GAME_TIMER()
				
				// Display message for a second
				WHILE (GET_GAME_TIMER() - iTime) < 1000
					WAIT(0)
					IF DOES_TEXT_LABEL_EXIST(sDisplayText)
						SET_TEXT_SCALE(0.5, 0.5)
						SET_TEXT_COLOUR(255, 255, 255, 255)
						SET_TEXT_CENTRE(TRUE)
						SET_TEXT_DROPSHADOW(0, 0, 0, 0, 0)
						SET_TEXT_EDGE(0, 0, 0, 0, 0)
						DISPLAY_TEXT(0.5, 0.5, sDisplayText)
					ENDIF
				ENDWHILE
				
				// If fail, retry
				IF NOT bPass
					RETURN SP_CHECK_SCRIPT(scriptFile)
				ENDIF
			ENDIF
			
			// Return to debug keyboard
			SET_KEYBOARD_MODE(KEYBOARD_MODE_DEBUG)
			
			RETURN bPass
		ENDIF
		
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE: Launches the specified script file
PROC SP_LAUNCH_SCRIPT(DEBUG_MENU_DATA_STRUCT &sDebugMenuData)

	
	
	// Check if xml has specified not to launch a script
	IF NOT (IS_DEBUG_MENU_ITEM_FLAG_SET(sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].iFlags, DM_BIT_LAUNCH_SCRIPT))
		EXIT
	ENDIF

	// Some items in the menu can store a script file for information purposes only.
	// We can use the item type to determine if a script should be launched.
	// If we ever want to have a scenario where a mission item has an associated script
	// but does not want the script to be launched then we should allow for the xml to
	// accept a bool and then we can store it in the DEBUG_MENU_ITEM_STRUCT data struct.
	IF sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].iType <> DM_HASH_ITEM_MISSION
	AND sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].iType <> DM_HASH_ITEM_BUGSTAR
	AND sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].iType <> DM_HASH_ITEM_SCRIPT
	AND sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].iType <> DM_HASH_ITEM_TOOL
	AND sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].iType <> DM_HASH_ITEM_LAUNCHER
		EXIT
	ENDIF
		
	TEXT_LABEL_63 scriptFile = sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].sFile
	
	IF (IS_STRING_NULL(scriptFile))
	OR ARE_STRINGS_EQUAL(scriptFile, "")
		EXIT
	ENDIF
		
	IF ARE_STRINGS_EQUAL(GET_STRING_FROM_STRING(scriptFile, GET_LENGTH_OF_LITERAL_STRING(scriptFile)-3, GET_LENGTH_OF_LITERAL_STRING(scriptFile)), ".sc")
		scriptFile = GET_STRING_FROM_STRING(scriptFile, 0, GET_LENGTH_OF_LITERAL_STRING(scriptFile)-3)
	ENDIF

	IF SP_CHECK_SCRIPT(scriptFile)
		REQUEST_SCRIPT(scriptFile)
		WHILE NOT (HAS_SCRIPT_LOADED(scriptFile))
			WAIT(0)
			REQUEST_SCRIPT(scriptFile)
		ENDWHILE
		
		PRINTLN(" SP_Launch_Script ", scriptFile)
		
		IF sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].iType = DM_HASH_ITEM_MISSION
		OR sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].iType = DM_HASH_ITEM_BUGSTAR
			START_NEW_SCRIPT(scriptFile, MISSION_STACK_SIZE)
		ELIF sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].iType = DM_HASH_ITEM_TOOL
			START_NEW_SCRIPT(scriptFile, SCRIPT_XML_STACK_SIZE)
		ELSE
			//Temporary nasty hack for end of project.
			//Hardcode the stripclub script to launch with the friend stack.
			IF ARE_STRINGS_EQUAL(sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].sFile, "stripclub")
			OR ARE_STRINGS_EQUAL(sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].sFile, "scriptTest1")
			OR ARE_STRINGS_EQUAL(sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].sFile, "scriptTest2")
			OR ARE_STRINGS_EQUAL(sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].sFile, "scriptTest3")
				START_NEW_SCRIPT(scriptFile, FRIEND_STACK_SIZE)
			ELSE
				START_NEW_SCRIPT(scriptFile, DEFAULT_STACK_SIZE)
			ENDIF
		ENDIF
		
		SET_SCRIPT_AS_NO_LONGER_NEEDED(scriptFile)
		// Ensure the cellphone is taken out of SLEEP mode
		SET_CELLPHONE_PROFILE_TO_NORMAL()
	ENDIF
ENDPROC

/// PURPOSE: Picks a random coord from the possible 3
PROC SP_GET_POSITION_FOR_WARP(DEBUG_MENU_DATA_STRUCT &sDebugMenuData, VECTOR &vCoords, FLOAT &fHeading)
	
	INT i, j, k
	VECTOR vTemp[3]
	FLOAT fTemp[3]
	
	vTemp[0] = << DM_NO_VECTOR, DM_NO_VECTOR, DM_NO_VECTOR >>
	vTemp[1] = << DM_NO_VECTOR, DM_NO_VECTOR, DM_NO_VECTOR >>
	vTemp[2] = << DM_NO_VECTOR, DM_NO_VECTOR, DM_NO_VECTOR >>
	
	fTemp[0] = DM_NO_HEADING
	fTemp[1] = DM_NO_HEADING
	fTemp[2] = DM_NO_HEADING
	
	REPEAT 3 i
		IF sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].vCoord[i].x <> DM_NO_VECTOR
		AND sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].vCoord[i].y <> DM_NO_VECTOR
		AND sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].vCoord[i].z <> DM_NO_VECTOR
			vTemp[j] = sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].vCoord[i]
			fTemp[j] = sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].fHeading[i]
			j++
		ENDIF
	ENDREPEAT
	
	k = GET_RANDOM_INT_IN_RANGE(0, j)
	
	vCoords = vTemp[k]
	fHeading = fTemp[k]
ENDPROC


FUNC BOOL DEBUG_MENU_CHECK_BLIP_AS_WARP_POSITION(VECTOR &theCoords, DEBUG_MENU_DATA_STRUCT &sDebugMenuData)
	IF IS_DEBUG_MENU_ITEM_FLAG_SET(sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].iFlags, DM_BIT_BLIPCOORDS)
		//Check if this mission has an associated mission blip.
		STATIC_BLIP_NAME_ENUM eMissionBlip = LAUNCHER_GET_CONTACT_POINT_FROM_GTA5_SCRIPT_NAME(sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].sFile, TRUE)
		IF eMissionBlip <> STATIC_BLIP_NAME_DUMMY_FINAL
			//If so, retrieve warp coordinates to trigger the mission with.
			theCoords = g_GameBlips[eMissionBlip].vCoords[0]
			PRINTSTRING("\nSP_Warp_Player() - Using contact point coords for script '")PRINTSTRING(sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].sFile)PRINTSTRING("'")PRINTNL()
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC	

/// PURPOSE: Sets the players coordinates, heading, and interior ID
FUNC BOOL SP_WARP_PLAYER_INTO_INTERIOR(DEBUG_MENU_DATA_STRUCT &sDebugMenuData, VECTOR theCoords, FLOAT theHeading)

	TEXT_LABEL_31 theRoom = sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].sInterior
	INTERIOR_INSTANCE_INDEX roomInterior
	
	//LOAD_SCENE(theCoords)
	
	// Only attempt to find an interior if a room type has been specified
	IF NOT ARE_STRINGS_EQUAL("", theRoom)
	
		DEBUG_MENU_CHECK_BLIP_AS_WARP_POSITION(theCoords, sDebugMenuData)

	
		PRINTLN("SP_Warp_Player_Into_Interior() - Checking for interior using room type '", theRoom, "'")
			
		roomInterior = GET_INTERIOR_AT_COORDS_WITH_TYPE(theCoords, theRoom)
		
		PRINTLN("SP_Warp_Player_Into_Interior - roomInterior = ", NATIVE_TO_INT(roomInterior))
		
		IF NATIVE_TO_INT(roomInterior) <> -1
		AND NATIVE_TO_INT(roomInterior) <> 0
			PRINTLN("SP_Warp_Player_Into_Interior - Valid interior found, progressing with load... ")
			
			//Pin interior and wait for it to load.
			INT iBailTimer = GET_GAME_TIMER()
			INT iBailSeconds
			INT iBailSecondsOut
			PIN_INTERIOR_IN_MEMORY(roomInterior)
			WHILE NOT IS_INTERIOR_READY(roomInterior)
				IF NOT IS_INTERIOR_READY(roomInterior)
				
					iBailSeconds = (10000 - (GET_GAME_TIMER() - iBailTimer))/1000
					
					IF iBailSeconds <= 0
						RETURN FALSE
					ELIF iBailSeconds <> iBailSecondsOut
						PRINTLN("SP_Warp_Player_Into_Interior : interior ", NATIVE_TO_INT(roomInterior), " is not ready, will bail out in ", iBailSeconds, " seconds")
						iBailSecondsOut = iBailSeconds
					ENDIF					
				ENDIF
				
				WAIT(0)
			ENDWHILE
			
			//Warp the player
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				SET_ENTITY_COORDS(PLAYER_PED_ID(), theCoords, TRUE, FALSE)
				
				IF NOT (theHeading < DM_MIN_HEADING)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), theHeading)
				ENDIF
				
				WAIT(0)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
				
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					IF IS_INTERIOR_SCENE()
						RETAIN_ENTITY_IN_INTERIOR(PLAYER_PED_ID(), roomInterior)
					ENDIF
				ENDIF
				
			ENDIF
			
			WAIT(0)
			
			//Unpin interior now that we're in it.
			UNPIN_INTERIOR(roomInterior)
			
			RETURN TRUE
		ELSE
			PRINTLN("SP_Warp_Player_Into_Interior() - Unable to find interior")
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC


/// PURPOSE: Sets the players coordinates and heading if interior is null
PROC SP_WARP_PLAYER(DEBUG_MENU_DATA_STRUCT &sDebugMenuData, BOOL paramKeepVehicle = FALSE)

	// If we are in gameflow mode and have selected a mission, bail out and let the mid-flow launcher take care of the warp.
	IF g_savedGlobals.sFlow.isGameflowActive
		IF sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].iType = DM_HASH_ITEM_MISSION
		OR sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].iType = DM_HASH_ITEM_BUGSTAR
			PRINTLN("SP_Warp_Player() - Letting the mid-flow launcher take care of warp.")
			EXIT
		ENDIF
	ENDIF
	
	paramKeepVehicle = (IS_DEBUG_MENU_ITEM_FLAG_SET(sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].iFlags, DM_BIT_KEEP_VEH))
	
	// NOTE: Leave the player in a vehicle if already in one
	VECTOR theCoords, waterTestCoords
	FLOAT theHeading
	SP_GET_POSITION_FOR_WARP(sDebugMenuData, theCoords, theHeading)
	
	IF paramKeepVehicle
		MODEL_NAMES vehModel = GET_DEFAULT_DEBUG_VEHICLE(TEST_PROBE_AGAINST_WATER(theCoords+<<0,0,50>>,theCoords+<<0,0,-50>>,waterTestCoords))
		VEHICLE_INDEX warpVehicle
		IF IS_DEBUG_MENU_ITEM_FLAG_SET(sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].iFlags, DM_BIT_VEHICLE)
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					REQUEST_MODEL(vehModel)
					WHILE NOT HAS_MODEL_LOADED(vehModel)
					WAIT(0)
					ENDWHILE
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						warpVehicle = CREATE_VEHICLE(vehModel, GET_ENTITY_COORDS(PLAYER_PED_ID()))
						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), warpVehicle)
						SET_VEHICLE_AS_NO_LONGER_NEEDED(warpVehicle)
					ENDIF
					SET_MODEL_AS_NO_LONGER_NEEDED(vehModel)
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	// If the useBlipCoords flag has been set then see if we can obtain the warp coords for the contact point
	IF DEBUG_MENU_CHECK_BLIP_AS_WARP_POSITION(theCoords, sDebugMenuData)
		// Remove vehicle when using blip coords.
		paramKeepVehicle = FALSE
	ENDIF

	IF (IS_PLAYER_PLAYING(PLAYER_ID()))
		IF NOT (theCoords.x < DM_MIN_VECTOR)
			IF NOT (SP_WARP_PLAYER_INTO_INTERIOR(sDebugMenuData, theCoords, theHeading))
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					IF (paramKeepVehicle)
						SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), theCoords)
					ELSE
						SET_ENTITY_COORDS(PLAYER_PED_ID(), theCoords)
					ENDIF
					
					IF NOT (theHeading < DM_MIN_HEADING)
						SET_ENTITY_HEADING(PLAYER_PED_ID(), theHeading)
					ENDIF
					
					LOAD_SCENE(theCoords)
					WAIT(0)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
				ENDIF
			ENDIF
		ELSE
			IF NOT (theHeading < DM_MIN_HEADING)
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					SET_ENTITY_HEADING(PLAYER_PED_ID(), theHeading)
				ENDIF
			ENDIF
			
			WAIT(0)
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
		ENDIF
	ENDIF
	
	PRINTLN("SP_Warp_Player() coords = ", theCoords, PICK_STRING( theHeading <> DM_NO_HEADING, ", heading = ", ""), PICK_FLOAT(theHeading <> DM_NO_HEADING, theHeading, 0.0))
ENDPROC


/// PURPOSE: Returns the time stamp from when the debug menu item was last selected.
///    		 If the item is not on the list then -1 will be returned.
//FUNC INT SP_GET_SELECTION_TIMESTAMP(STRING sLabel)
//	
//	IF NOT ARE_STRINGS_EQUAL("", sLabel)
//		IF GET_LENGTH_OF_LITERAL_STRING(sLabel) > 3
//			IF ARE_STRINGS_EQUAL(GET_STRING_FROM_STRING(sLabel, GET_LENGTH_OF_LITERAL_STRING(sLabel)-3, GET_LENGTH_OF_LITERAL_STRING(sLabel)), ".sc")
//				sLabel = GET_STRING_FROM_STRING(sLabel, 0, GET_LENGTH_OF_LITERAL_STRING(sLabel)-3)
//			ENDIF
//		ENDIF
//
//		INT i
//		FOR i = 0 TO MAX_DEBUG_MENU_SELECTIONS_TO_TRACK-1
//			IF NOT IS_STRING_NULL(g_debugMenuControl.sDMSelections[i].sLabel)
//				IF NOT ARE_STRINGS_EQUAL("", g_debugMenuControl.sDMSelections[i].sLabel)
//					IF ARE_STRINGS_EQUAL(g_debugMenuControl.sDMSelections[i].sLabel, sLabel)
//						RETURN g_debugMenuControl.sDMSelections[i].iTimeStamp
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDFOR
//	ENDIF
//	
//	RETURN -1
//ENDFUNC

/// PURPOSE: Checks to see if the player has made a selection
PROC SP_CHECK_MENU_SELECTION(DEBUG_MENU_DATA_STRUCT &sDebugMenuData)

	DISABLE_CELLPHONE_THIS_FRAME_ONLY()
	DISABLE_SELECTOR_THIS_FRAME()

	IF NOT sDebugMenuData.bDrawAltWindow
		// Make a selection
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
		OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_RETURN)
			// Check that the item is not a menu or break item
			IF sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].iType <> DM_HASH_ITEM_MENU
			AND sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].iType <> DM_HASH_ITEM_BREAK
			AND sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].iType <> DM_HASH_ITEM_INPUT
			AND IS_DEBUG_MENU_ITEM_FLAG_SET(sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].iFlags, DM_BIT_ENABLED)
				// If there are alternate codeIDs, we may want to popup a window instead
				IF sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].iAltCodeID[0] != ENUM_TO_INT(SPCID_DEFAULT)
					sDebugMenuData.iAltWindowTimer = GET_GAME_TIMER() + DM_INPUT_CHECK_DELAY_msec * 2
				ENDIF
			ENDIF
		ELIF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
		OR IS_KEYBOARD_KEY_JUST_RELEASED(KEY_RETURN)
			// Check that the item is not a menu or break item
			IF sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].iType <> DM_HASH_ITEM_MENU
			AND sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].iType <> DM_HASH_ITEM_BREAK
			AND sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].iType <> DM_HASH_ITEM_INPUT
			AND IS_DEBUG_MENU_ITEM_FLAG_SET(sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].iFlags, DM_BIT_ENABLED)
				// ...if an item was just selected in the Playthrough menu then automatically activate gameflow mode.
				IF sDebugMenuData.iCurrentDepth = 2
					IF ARE_STRINGS_EQUAL(sDebugMenuData.sItems[0][sDebugMenuData.iCurrentItem[0]].sTitle, "DM_PLAYTHRU")
						g_savedGlobals.sFlow.isGameflowActive = TRUE
					ENDIF
				ENDIF
				
				DEBUG_MENU_RUN_ITEM_CUSTOM(sDebugMenuData)
				
				// ...make a note of this item selection.
				SP_STORE_SELECTION_NAME_AND_TIME(sDebugMenuData)
				
				// ...check for necessary script resets.
				DEBUG_MENU_CLEANUP_RUNNING_MISSION_SCRIPTS(sDebugMenuData)
				
				// ...perform any time updates associated with menu item.
				SET_DEBUG_MENU_CUSTOM_PROCESS_FLAG(sDebugMenuData.iCustomProcessFlags, DM_CUSTOM_PROCESS_SET_GAME_TIME)
				
				// ...perform any special code associated with menu item.
				SET_DEBUG_MENU_CUSTOM_PROCESS_FLAG(sDebugMenuData.iCustomProcessFlags, DM_CUSTOM_PROCESS_RUN_CODE_ID)
				SET_DEBUG_MENU_CUSTOM_PROCESS_FLAG(sDebugMenuData.iCustomProcessFlags, DM_CUSTOM_PROCESS_RUN_CODE_ID_PRELAUNCH)
				
				// ...perform any game flow changes.
				SP_PREPARE_GAME_STATE(sDebugMenuData)
				
				// ...close and cleanup menu.
				SET_DEBUG_MENU_PROCESS_FLAG(sDebugMenuData.iProcessFlags, DM_PROCESS_CLEANUP)
				
			ENDIF
		ELIF sDebugMenuData.iAltWindowTimer < GET_GAME_TIMER() AND sDebugMenuData.iAltWindowTimer != 0
			sDebugMenuData.bDrawAltWindow = TRUE
			sDebugMenuData.iCurrentAltItem = 0
		ENDIF
	ELSE
		IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL,  INPUT_FRONTEND_CANCEL)
			sDebugMenuData.bDrawAltWindow = FALSE
		ENDIF
			
		IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
		OR IS_KEYBOARD_KEY_JUST_RELEASED(KEY_RETURN)
			IF sDebugMenuData.iAltWindowTimer != 0	
				sDebugMenuData.iAltWindowTimer = 0// We set this to 0 once the accept has been released from opening the window
			ELSE
				// ...make a note of this item selection.
				SP_STORE_SELECTION_NAME_AND_TIME(sDebugMenuData)
				
				// ...check for necessary script resets.
				DEBUG_MENU_CLEANUP_RUNNING_MISSION_SCRIPTS(sDebugMenuData)
				
				// ...perform any time updates associated with menu item.
				SET_DEBUG_MENU_CUSTOM_PROCESS_FLAG(sDebugMenuData.iCustomProcessFlags, DM_CUSTOM_PROCESS_SET_GAME_TIME)
				
				// ...perform any special code associated with menu item.
				SET_DEBUG_MENU_CUSTOM_PROCESS_FLAG(sDebugMenuData.iCustomProcessFlags, DM_CUSTOM_PROCESS_RUN_CODE_ID)
				SET_DEBUG_MENU_CUSTOM_PROCESS_FLAG(sDebugMenuData.iCustomProcessFlags, DM_CUSTOM_PROCESS_RUN_CODE_ID_PRELAUNCH)
				
				// ...perform game flow changes based on alternate menu selection.
				SP_RUN_SCRIPT_FOR_CODEID_ALT(sDebugMenuData)
				
				// ...close and cleanup menu.
				SET_DEBUG_MENU_PROCESS_FLAG(sDebugMenuData.iProcessFlags, DM_PROCESS_CLEANUP)
				
				sDebugMenuData.bDrawAltWindow = FALSE
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC


PROC SP_CLEANUP_LAUNCH(DEBUG_MENU_DATA_STRUCT &sDebugMenuData)
	IF IS_DEBUG_MENU_CUSTOM_PROCESS_FLAG_SET(sDebugMenuData.iCustomProcessFlags, DM_CUSTOM_PROCESS_NONE)
	ENDIF
ENDPROC

PROC SP_RESET_LAUNCH(DEBUG_MENU_DATA_STRUCT &sDebugMenuData)
		// Prevent any interference
	SET_DEBUG_MENU_CUSTOM_PROCESS_FLAG(sDebugMenuData.iCustomProcessFlags, DM_CUSTOM_PROCESS_DEACTIVATE_FLOW_DIAGRAM)
	//g_bDebugFinanceDisplay = FALSE

ENDPROC

PROC BUILD_DEBUG_MENU_LIST_EXAMPLE(DEBUG_MENU_DATA_STRUCT &sDebugMenuData, INT iDepth)
	g_eRC_MissionIDs rcIndex
	g_structRCMissionsStatic paramDetails
	REPEAT MAX_RC_MISSIONS rcIndex
		IF ENUM_TO_INT(rcIndex) >= DM_MAX_ITEMS_PER_DEPTH
			EXIT
		ENDIF
		DM_RESET_MENU_ITEM(sDebugMenuData.sItems[iDepth][sDebugMenuData.iItemCount[iDepth]])
		Retrieve_Random_Character_Static_Mission_Details(rcIndex, paramDetails)
		
		sDebugMenuData.sItems[iDepth][sDebugMenuData.iItemCount[iDepth]].sFile = paramDetails.rcScriptName
		sDebugMenuData.sItems[iDepth][sDebugMenuData.iItemCount[iDepth]].iType = DM_HASH_ITEM_MISSION
		sDebugMenuData.sItems[iDepth][sDebugMenuData.iItemCount[iDepth]].iUniqueID = sDebugMenuData.iItemCount[iDepth]
		sDebugMenuData.iItemCount[iDepth]++
	ENDREPEAT
ENDPROC

FUNC BOOL BUILD_DEBUG_MENU_LIST_DYNAMIC(DEBUG_MENU_DATA_STRUCT &sDebugMenuData, INT iDepth, SP_MENU_CODE_ID_ENUM codeID)
	sDebugMenuData.iItemCount[iDepth] = 0
	BOOL bBugstarItem = FALSE
	SWITCH codeID
		CASE SPCID_DYNAMIC_EXAMPLE
			BUILD_DEBUG_MENU_LIST_EXAMPLE(sDebugMenuData, iDepth)
		BREAK
		DEFAULT
			RETURN FALSE
		BREAK
	ENDSWITCH
	
	IF bBugstarItem
		// Allow Bugstar data to be updated
		//sDebugMenuData.sBugstarData.iGetBasicInfoStage = 0
		sDebugMenuData.sBugstarData.iDepthToLoadBasicInfo = iDepth
		SET_BUGSTAR_PROCESS_FLAG(sDebugMenuData.sBugstarData.eProcessFlags, BSTAR_PROCESS_BUGSTAR_QUERY_RESET)
		SET_DEBUG_MENU_PROCESS_FLAG(sDebugMenuData.iProcessFlags, DM_PROCESS_BUGSTAR_GET_BASIC)
		SET_DEBUG_MENU_PROCESS_FLAG(sDebugMenuData.iProcessFlags, DM_PROCESS_WAIT)
	ELSE
		CLEAR_DEBUG_MENU_PROCESS_FLAG(sDebugMenuData.iProcessFlags, DM_PROCESS_BUGSTAR_GET_BASIC)
	ENDIF
	// Reset some states
	DM_SORT_MENU_LIST_ALPHABETICAL(sDebugMenuData, iDepth)
	sDebugMenuData.iCurrentItem[iDepth] = 0
	sDebugMenuData.iTopItem[iDepth] = 0
	sDebugMenuData.iUnderItem[iDepth] = sDebugMenuData.iItemCount[iDepth]
	
	INT iCount
	FOR iCount = iDepth+1 TO DM_MAX_DEPTH-1
		sDebugMenuData.iItemCount[iCount] = 0
	ENDFOR
	
	RETURN TRUE
ENDFUNC
