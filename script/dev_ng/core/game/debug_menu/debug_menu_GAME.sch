USING "debug_menu_shared_core.sch"

USING "commands_ped.sch"
USING "script_player.sch"
USING "flow_launcher_main_core.sch"
USING "selector_public.sch"
USING "flow_diagram_core.sch"


#if USE_CLF_DLC
	USING "debug_menu_CLF.sch"
#endif
#if USE_NRM_DLC
	USING "debug_menu_NRM.sch"
#endif
#if not USE_CLF_DLC
#if not USE_NRM_DLC
	USING "debug_menu_SP.sch"
#endif
#endif

USING "debug_menu_MP.sch"


// Used by both single and multi lable launch routines.
// Procedure extracted here to avoid script duplication.
PROC RUN_PRE_GAMEFLOW_LAUNCH_ROUTINES(VECTOR &paramPlayerPosition, FLOAT &paramPlayerHeading)

	IF NOT IS_SCREEN_FADED_OUT()
		IF NOT IS_SCREEN_FADING_OUT()
			DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME_SHORT)
		ENDIF
		WHILE NOT IS_SCREEN_FADED_OUT()
			WAIT(0)
		ENDWHILE
	ENDIF
	
	#IF IS_DEBUG_BUILD
		g_bBlockMissionTrigger = TRUE
	#ENDIF
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		paramPlayerPosition = GET_ENTITY_COORDS(PLAYER_PED_ID())
		paramPlayerHeading 	= GET_ENTITY_HEADING(PLAYER_PED_ID())

		CPRINTLN(DEBUG_DEBUG_MENU, "Warping player to safe space before gameflow launch.")
		DO_PLAYER_MAP_WARP_WITH_LOAD(<<0,0,-100>>, NO_HEADING, FALSE)
		IF NOT IS_ENTITY_ATTACHED(PLAYER_PED_ID())
			FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
		ENDIF
	ENDIF

ENDPROC


// Used by both single and multi lable launch routines.
// Procedure extracted here to avoid script duplication.
PROC RUN_POST_GAMEFLOW_LAUNCH_ROUTINES(VECTOR paramPlayerPosition, FLOAT paramPlayerHeading)

	//Check if character switch is valid for current point in gameflow.
	#if USE_CLF_DLC
		IF g_savedGlobalsClifford.sFlow.isGameflowActive
			IF NOT IS_PLAYER_PED_AVAILABLE(GET_CURRENT_PLAYER_PED_ENUM())
				//IF sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].iLabelID != 0
					CPRINTLN(DEBUG_DEBUG_MENU, "Current player ped character is not available at this point in the gameflow.")
					SELECTOR_SLOTS_ENUM eCharToSwitchTo
					IF IS_PLAYER_PED_AVAILABLE(CHAR_MICHAEL)
						eCharToSwitchTo = SELECTOR_PED_MICHAEL
					ELIF IS_PLAYER_PED_AVAILABLE(CHAR_FRANKLIN)
						eCharToSwitchTo = SELECTOR_PED_FRANKLIN
					ELIF IS_PLAYER_PED_AVAILABLE(CHAR_TREVOR)
						eCharToSwitchTo = SELECTOR_PED_TREVOR
					ELSE
						SCRIPT_ASSERT("No available characters for given point in mission flow. Tell BenR.")
					ENDIF
					CPRINTLN(DEBUG_DEBUG_MENU, "Swapping player character to ", GET_CHARSHEET_DISPLAY_STRING_FROM_CHARSHEET(GET_PLAYER_PED_ENUM_FROM_SELECTOR_SLOT(eCharToSwitchTo)), ".")
					WHILE NOT SET_CURRENT_SELECTOR_PED(eCharToSwitchTo)
						WAIT(0)
					ENDWHILE
					SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_DEFAULT, FALSE)
					STORE_PLAYER_PED_VARIATIONS(PLAYER_PED_ID(), TRUE)
				//ENDIF
			ENDIF
			
			//Reset the players last known ped info
			ResetLastKnownPedInfo(g_savedGlobalsClifford.sPlayerData.sInfo, SP_MISSION_NONE)
			
			RESTORE_PLAYER_PED_WEAPONS(PLAYER_PED_ID()) // ensure player has his default weapon if unlocked
		ENDIF
	#endif
	#if USE_NRM_DLC
		IF g_savedGlobalsnorman.sFlow.isGameflowActive
			IF NOT IS_PLAYER_PED_AVAILABLE(GET_CURRENT_PLAYER_PED_ENUM())
				//IF sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].iLabelID != 0
					CPRINTLN(DEBUG_DEBUG_MENU, "Current player ped character is not available at this point in the gameflow.")
					SELECTOR_SLOTS_ENUM eCharToSwitchTo
					IF IS_PLAYER_PED_AVAILABLE(CHAR_MICHAEL)
						eCharToSwitchTo = SELECTOR_PED_MICHAEL
					ELIF IS_PLAYER_PED_AVAILABLE(CHAR_FRANKLIN)
						eCharToSwitchTo = SELECTOR_PED_FRANKLIN
					ELIF IS_PLAYER_PED_AVAILABLE(CHAR_TREVOR)
						eCharToSwitchTo = SELECTOR_PED_TREVOR
					ELSE
						SCRIPT_ASSERT("No available characters for given point in mission flow. Tell BenR.")
					ENDIF
					CPRINTLN(DEBUG_DEBUG_MENU, "Swapping player character to ", GET_CHARSHEET_DISPLAY_STRING_FROM_CHARSHEET(GET_PLAYER_PED_ENUM_FROM_SELECTOR_SLOT(eCharToSwitchTo)), ".")
					WHILE NOT SET_CURRENT_SELECTOR_PED(eCharToSwitchTo)
						WAIT(0)
					ENDWHILE
					SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_DEFAULT, FALSE)
					STORE_PLAYER_PED_VARIATIONS(PLAYER_PED_ID(), TRUE)
				//ENDIF
			ENDIF
			
			//Reset the players last known ped info
			ResetLastKnownPedInfo(g_savedGlobalsnorman.sPlayerData.sInfo, SP_MISSION_NONE)
			
			RESTORE_PLAYER_PED_WEAPONS(PLAYER_PED_ID()) // ensure player has his default weapon if unlocked
		ENDIF
	#endif
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
		IF g_savedGlobals.sFlow.isGameflowActive
			IF NOT IS_PLAYER_PED_AVAILABLE(GET_CURRENT_PLAYER_PED_ENUM())
				//IF sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].iLabelID != 0
					CPRINTLN(DEBUG_DEBUG_MENU, "Current player ped character is not available at this point in the gameflow.")
					SELECTOR_SLOTS_ENUM eCharToSwitchTo
					IF IS_PLAYER_PED_AVAILABLE(CHAR_MICHAEL)
						eCharToSwitchTo = SELECTOR_PED_MICHAEL
					ELIF IS_PLAYER_PED_AVAILABLE(CHAR_FRANKLIN)
						eCharToSwitchTo = SELECTOR_PED_FRANKLIN
					ELIF IS_PLAYER_PED_AVAILABLE(CHAR_TREVOR)
						eCharToSwitchTo = SELECTOR_PED_TREVOR
					ELSE
						SCRIPT_ASSERT("No available characters for given point in mission flow. Tell BenR.")
					ENDIF
					CPRINTLN(DEBUG_DEBUG_MENU, "Swapping player character to ", GET_CHARSHEET_DISPLAY_STRING_FROM_CHARSHEET(GET_PLAYER_PED_ENUM_FROM_SELECTOR_SLOT(eCharToSwitchTo)), ".")
					WHILE NOT SET_CURRENT_SELECTOR_PED(eCharToSwitchTo)
						WAIT(0)
					ENDWHILE
					SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_DEFAULT, FALSE)
					STORE_PLAYER_PED_VARIATIONS(PLAYER_PED_ID(), TRUE)
				//ENDIF
			ENDIF
			
			//Reset the players last known ped info
			ResetLastKnownPedInfo(g_savedGlobals.sPlayerData.sInfo, SP_MISSION_NONE)
			
			RESTORE_PLAYER_PED_WEAPONS(PLAYER_PED_ID()) // ensure player has his default weapon if unlocked
		ENDIF
	#endif
	#endif
	CLEAR_ALL_MUST_LEAVE_AREA_VEHICLE_GEN_FLAGS()
	
	//Wait to let other control scripts catch up with the flow changes.
	WAIT(500)
	
	REACTIVATE_ALL_WORLD_BRAINS_THAT_ARE_WAITING_TILL_OUT_OF_RANGE()
	REACTIVATE_ALL_OBJECT_BRAINS_THAT_ARE_WAITING_TILL_OUT_OF_RANGE()

	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		DO_PLAYER_MAP_WARP_WITH_LOAD(paramPlayerPosition, paramPlayerHeading, FALSE)
		IF NOT IS_ENTITY_ATTACHED(PLAYER_PED_ID())
			FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
		ENDIF
	ENDIF
	//CPRINTLN(DEBUG_RANDOM_CHAR, "post flow warp to: ", paramPlayerPosition)
	
	// Kenneth R.
	// Moved this to the DM_CUSTOM_PROCESS_LAUNCH_TO_MISSION_OUTCOME stage
	// so we dont see the SP_WARP_PLAYER() take place.
	
	//DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME_SHORT)
	
ENDPROC




PROC PROCESS_DEBUG_MENU_FLOW_ACTIVATION(DEBUG_MENU_DATA_STRUCT &sDebugMenuData, FLOW_DIAGRAM &sDebugFlowDiagram, FLOW_LAUNCHER_VARS &flowLauncher)
	IF IS_DEBUG_MENU_CUSTOM_PROCESS_FLAG_SET(sDebugMenuData.iCustomProcessFlags, DM_CUSTOM_PROCESS_ACTIVATE_FLOW_DIAGRAM)
		CLEAR_DEBUG_MENU_CUSTOM_PROCESS_FLAG(sDebugMenuData.iCustomProcessFlags, DM_CUSTOM_PROCESS_ACTIVATE_FLOW_DIAGRAM)
		ACTIVATE_FLOW_DIAGRAM(sDebugFlowDiagram)
	ENDIF
	IF IS_DEBUG_MENU_CUSTOM_PROCESS_FLAG_SET(sDebugMenuData.iCustomProcessFlags, DM_CUSTOM_PROCESS_DEACTIVATE_FLOW_DIAGRAM)
		CLEAR_DEBUG_MENU_CUSTOM_PROCESS_FLAG(sDebugMenuData.iCustomProcessFlags, DM_CUSTOM_PROCESS_DEACTIVATE_FLOW_DIAGRAM)
		DEACTIVATE_FLOW_DIAGRAM(sDebugFlowDiagram)
	ENDIF
	
	IF IS_DEBUG_MENU_CUSTOM_PROCESS_FLAG_SET(sDebugMenuData.iCustomProcessFlags, DM_CUSTOM_PROCESS_GENERATE_FLOW)
		CLEAR_DEBUG_MENU_CUSTOM_PROCESS_FLAG(sDebugMenuData.iCustomProcessFlags, DM_CUSTOM_PROCESS_GENERATE_FLOW)
		LAUNCHER_GENERATE_FLOW_DIAGRAM_XML(flowLauncher)
		g_flowDiagram.bFlowDiagramDataLoaded = FALSE
		g_flowDiagram.bFlowDiagramDataSorted = FALSE
		g_flowDiagram.bFlowDiagramPositioned = FALSE
	ENDIF
	
	IF IS_DEBUG_MENU_CUSTOM_PROCESS_FLAG_SET(sDebugMenuData.iCustomProcessFlags, DM_CUSTOM_PROCESS_GENERATE_PLAYTHROUGH)
		CLEAR_DEBUG_MENU_CUSTOM_PROCESS_FLAG(sDebugMenuData.iCustomProcessFlags, DM_CUSTOM_PROCESS_GENERATE_PLAYTHROUGH)
		LAUNCHER_GENERATE_PLAYTHROUGH_ORDER(flowLauncher)
	ENDIF
		
	IF IS_DEBUG_MENU_CUSTOM_PROCESS_FLAG_SET(sDebugMenuData.iCustomProcessFlags, DM_CUSTOM_PROCESS_PLAYTHROUGH_BUILDER)
		CLEAR_DEBUG_MENU_CUSTOM_PROCESS_FLAG(sDebugMenuData.iCustomProcessFlags, DM_CUSTOM_PROCESS_PLAYTHROUGH_BUILDER)
		LAUNCHER_GENERATE_PLAYTHROUGH_ORDER(flowLauncher)
		#if USE_CLF_DLC
			g_savedGlobalsClifford.sFlow.isGameflowActive = FALSE
		#endif
		#if USE_NRM_DLC
			g_savedGlobalsnorman.sFlow.isGameflowActive = FALSE
		#endif
		#if not  USE_CLF_DLC
		#if not USE_NRM_DLC
			g_savedGlobals.sFlow.isGameflowActive = FALSE
		#endif
		#endif
	ENDIF
	
	IF IS_DEBUG_MENU_CUSTOM_PROCESS_FLAG_SET(sDebugMenuData.iCustomProcessFlags, DM_CUSTOM_PROCESS_LAUNCH_TO_MISSION)
		CLEAR_DEBUG_MENU_CUSTOM_PROCESS_FLAG(sDebugMenuData.iCustomProcessFlags, DM_CUSTOM_PROCESS_LAUNCH_TO_MISSION)
		//Wait to let running scripts terminate from force cleanup.
		WAIT(250)
		IF NOT DEBUG_LAUNCH_TO_MISSION(sDebugMenuData, flowLauncher)
			//Wait to let other control scripts catch up with the flow changes.
			WAIT(1000)
			SET_DEBUG_MENU_CUSTOM_PROCESS_FLAG(sDebugMenuData.iCustomProcessFlags, DM_CUSTOM_PROCESS_LAUNCH_TO_MISSION_OUTCOME)
		ENDIF
	ENDIF
	
	IF IS_DEBUG_MENU_CUSTOM_PROCESS_FLAG_SET(sDebugMenuData.iCustomProcessFlags, DM_CUSTOM_PROCESS_LAUNCH_TO_BEFORE_MISSION)
		CLEAR_DEBUG_MENU_CUSTOM_PROCESS_FLAG(sDebugMenuData.iCustomProcessFlags, DM_CUSTOM_PROCESS_LAUNCH_TO_BEFORE_MISSION)
		//Wait to let running scripts terminate from force cleanup.
		WAIT(250)
		IF NOT DEBUG_LAUNCH_TO_BEFORE_MISSION(sDebugMenuData, flowLauncher)
			//Wait to let other control scripts catch up with the flow changes.
			WAIT(1000)
			SET_DEBUG_MENU_CUSTOM_PROCESS_FLAG(sDebugMenuData.iCustomProcessFlags, DM_CUSTOM_PROCESS_LAUNCH_TO_MISSION_OUTCOME)
		ENDIF
	ENDIF
	
	IF IS_DEBUG_MENU_CUSTOM_PROCESS_FLAG_SET(sDebugMenuData.iCustomProcessFlags, DM_CUSTOM_PROCESS_LAUNCH_TO_AFTER_MISSION)
		CLEAR_DEBUG_MENU_CUSTOM_PROCESS_FLAG(sDebugMenuData.iCustomProcessFlags, DM_CUSTOM_PROCESS_LAUNCH_TO_AFTER_MISSION)
		//Wait to let running scripts terminate from force cleanup.
		WAIT(250)
		IF NOT DEBUG_LAUNCH_TO_AFTER_MISSION(sDebugMenuData, flowLauncher)
			//Wait to let other control scripts catch up with the flow changes.
			WAIT(1000)
			SET_DEBUG_MENU_CUSTOM_PROCESS_FLAG(sDebugMenuData.iCustomProcessFlags, DM_CUSTOM_PROCESS_LAUNCH_TO_MISSION_OUTCOME)
		ENDIF
	ENDIF
	
	IF IS_DEBUG_MENU_CUSTOM_PROCESS_FLAG_SET(sDebugMenuData.iCustomProcessFlags, DM_CUSTOM_PROCESS_LAUNCH_TO_MISSION_OUTCOME)
		CLEAR_DEBUG_MENU_CUSTOM_PROCESS_FLAG(sDebugMenuData.iCustomProcessFlags, DM_CUSTOM_PROCESS_LAUNCH_TO_MISSION_OUTCOME)
		//We've not launched to a mission.
		//Do none mission specific launch steps.
		
		// ...launch any scripts associated with menu item.
		SP_LAUNCH_SCRIPT(sDebugMenuData)
		
		// ...perform any warps associatated with menu item.
		SP_WARP_PLAYER(sDebugMenuData)
		
		
		DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME_SHORT)
		
		#IF IS_DEBUG_BUILD
			g_bBlockMissionTrigger = FALSE
		#ENDIF
	ENDIF
	
	IF IS_DEBUG_MENU_CUSTOM_PROCESS_FLAG_SET(sDebugMenuData.iCustomProcessFlags, DM_CUSTOM_PROCESS_LAUNCHER_LAUNCH_FLOW_TO_LABEL)
		CPRINTLN(DEBUG_DEBUG_MENU, "SP_Prepare_Game_State: Running flow launch for XML defined LabelID.")
		
		#if USE_CLF_DLC
			//Work out which label has been linked.
			INT iLabelIndex
			REPEAT MAX_LABELS_CLF iLabelIndex
				JUMP_LABEL_IDS eCurrentLabel = INT_TO_ENUM(JUMP_LABEL_IDS, iLabelIndex)
				IF GET_HASH_KEY(GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(eCurrentLabel)) = sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].iLabelID

					VECTOR vPlayerPosition 	= <<0,0,0>>
					FLOAT fPlayerHeading	= 0.0
					
					RUN_PRE_GAMEFLOW_LAUNCH_ROUTINES(vPlayerPosition, fPlayerHeading)
					LAUNCHER_LAUNCH_FLOW_TO_LABEL(flowLauncher, eCurrentLabel, -1, FALSE, FALSE, FALSE)
					RUN_POST_GAMEFLOW_LAUNCH_ROUTINES(vPlayerPosition, fPlayerHeading)
					
					SET_DEBUG_MENU_CUSTOM_PROCESS_FLAG(sDebugMenuData.iCustomProcessFlags, DM_CUSTOM_PROCESS_LAUNCH_TO_MISSION_OUTCOME)
					CLEAR_DEBUG_MENU_CUSTOM_PROCESS_FLAG(sDebugMenuData.iCustomProcessFlags, DM_CUSTOM_PROCESS_LAUNCHER_LAUNCH_FLOW_TO_LABEL)
					EXIT
				ENDIF
			ENDREPEAT
		#endif
		#if USE_NRM_DLC
			//Work out which label has been linked.
			INT iLabelIndex
			REPEAT MAX_LABELS_NRM iLabelIndex
				JUMP_LABEL_IDS eCurrentLabel = INT_TO_ENUM(JUMP_LABEL_IDS, iLabelIndex)
				IF GET_HASH_KEY(GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(eCurrentLabel)) = sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].iLabelID

					VECTOR vPlayerPosition 	= <<0,0,0>>
					FLOAT fPlayerHeading	= 0.0
					
					RUN_PRE_GAMEFLOW_LAUNCH_ROUTINES(vPlayerPosition, fPlayerHeading)
					LAUNCHER_LAUNCH_FLOW_TO_LABEL(flowLauncher, eCurrentLabel, -1, FALSE, FALSE, FALSE)
					RUN_POST_GAMEFLOW_LAUNCH_ROUTINES(vPlayerPosition, fPlayerHeading)
					
					SET_DEBUG_MENU_CUSTOM_PROCESS_FLAG(sDebugMenuData.iCustomProcessFlags, DM_CUSTOM_PROCESS_LAUNCH_TO_MISSION_OUTCOME)
					CLEAR_DEBUG_MENU_CUSTOM_PROCESS_FLAG(sDebugMenuData.iCustomProcessFlags, DM_CUSTOM_PROCESS_LAUNCHER_LAUNCH_FLOW_TO_LABEL)
					EXIT
				ENDIF
			ENDREPEAT
		#endif
		
		#if not USE_CLF_DLC
		#if not USE_NRM_DLC
			//Work out which label has been linked.
			INT iLabelIndex
			REPEAT MAX_LABELS iLabelIndex
				JUMP_LABEL_IDS eCurrentLabel = INT_TO_ENUM(JUMP_LABEL_IDS, iLabelIndex)
				IF GET_HASH_KEY(GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(eCurrentLabel)) = sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].iLabelID

					VECTOR vPlayerPosition 	= <<0,0,0>>
					FLOAT fPlayerHeading	= 0.0
					
					RUN_PRE_GAMEFLOW_LAUNCH_ROUTINES(vPlayerPosition, fPlayerHeading)
					LAUNCHER_LAUNCH_FLOW_TO_LABEL(flowLauncher, eCurrentLabel, -1, FALSE, FALSE, FALSE)
					RUN_POST_GAMEFLOW_LAUNCH_ROUTINES(vPlayerPosition, fPlayerHeading)
					
					SET_DEBUG_MENU_CUSTOM_PROCESS_FLAG(sDebugMenuData.iCustomProcessFlags, DM_CUSTOM_PROCESS_LAUNCH_TO_MISSION_OUTCOME)
					CLEAR_DEBUG_MENU_CUSTOM_PROCESS_FLAG(sDebugMenuData.iCustomProcessFlags, DM_CUSTOM_PROCESS_LAUNCHER_LAUNCH_FLOW_TO_LABEL)
					EXIT
				ENDIF
			ENDREPEAT
		#endif
		#endif
	ENDIF
	
	IF IS_DEBUG_MENU_CUSTOM_PROCESS_FLAG_SET(sDebugMenuData.iCustomProcessFlags, DM_CUSTOM_PROCESS_LAUNCHER_MULTI_LABEL_LAUNCH)
		CPRINTLN(DEBUG_DEBUG_MENU, "SP_Prepare_Game_State: Running multi label flow launch for XML defined LabelIDs.")
		
		//Figure out which labels have been set and in which strands.
		INT iStrandIndex
		JUMP_LABEL_IDS eStrandLabels[MAX_STRANDS]
		
		#if USE_CLF_DLC
			REPEAT MAX_STRANDS_CLF iStrandIndex
				eStrandLabels[iStrandIndex] = STAY_ON_THIS_COMMAND
			ENDREPEAT
		#endif
		#if USE_NRM_DLC
			REPEAT MAX_STRANDS_NRM iStrandIndex
				eStrandLabels[iStrandIndex] = STAY_ON_THIS_COMMAND
			ENDREPEAT
		#endif
		
		#if not USE_CLF_DLC		
		#if not USE_NRM_DLC
			REPEAT MAX_STRANDS iStrandIndex
				eStrandLabels[iStrandIndex] = STAY_ON_THIS_COMMAND
			ENDREPEAT
		#endif
		#endif		
		BOOL bBaseLabelFound
		BOOL bExtraLabelFound[MAX_EXTRA_LABEL_IDS]
		
		//Check which extra label IDs have been set.
		INT iExtraLabelIndex
		REPEAT MAX_EXTRA_LABEL_IDS iExtraLabelIndex
			IF sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].iExtraLabelID[iExtraLabelIndex] = 0
				bExtraLabelFound[iExtraLabelIndex] = TRUE
			ENDIF
		ENDREPEAT

		//For each strand check if the XML has defined a label for it.
		#if USE_CLF_DLC
			REPEAT MAX_STRANDS_CLF iStrandIndex
				//Check through all commands defined for this strand.
				INT iCommandIndex
				FOR iCommandIndex = g_flowUnsaved.flowCommandsIndex[iStrandIndex].lower TO g_flowUnsaved.flowCommandsIndex[iStrandIndex].upper
					JUMP_LABEL_IDS eCommandLabel = STAY_ON_THIS_COMMAND
				
					//Check if the command at this iCommandIndex is a 'Label' command.
					IF g_flowUnsaved.flowCommands[iCommandIndex].command = FLOW_LABEL
						eCommandLabel = INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.coreVars[g_flowUnsaved.flowCommands[iCommandIndex].index].iValue1)
					ENDIF
					
					//Check if the command at this iCommandIndex is a 'Label Launcher Only' command.
					IF g_flowUnsaved.flowCommands[iCommandIndex].command = FLOW_LABEL_LAUNCHER_ONLY
						eCommandLabel = INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.flowCommands[iCommandIndex].index)
					ENDIF
					
					// We've found a label command. Do any of the XML defined labels match it?
					IF eCommandLabel != STAY_ON_THIS_COMMAND
						//Does base label match?
						IF NOT bBaseLabelFound
							IF GET_HASH_KEY(GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(eCommandLabel)) = sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].iLabelID
								//Match found. Has this strand alread got a label set?
								IF eStrandLabels[iStrandIndex] = STAY_ON_THIS_COMMAND
									CPRINTLN(DEBUG_DEBUG_MENU, "Multi label launch: Label for strand ", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(INT_TO_ENUM(STRANDS, iStrandIndex)), " set to ", GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(eCommandLabel), ".")
									eStrandLabels[iStrandIndex] = eCommandLabel
								ELSE
									SCRIPT_ASSERT("Menu item attempted to launch gameflow to multi points but two points were defined in the same strand. Tell BenR.")
								ENDIF
								bBaseLabelFound = TRUE
							ENDIF
						ENDIF
						
						//Do any of the extra labels match?
						REPEAT MAX_EXTRA_LABEL_IDS iExtraLabelIndex
							IF NOT bExtraLabelFound[iExtraLabelIndex]
								IF GET_HASH_KEY(GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(eCommandLabel)) = sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].iExtraLabelID[iExtraLabelIndex]
									//Match found. Has this strand alread got a label set?
									IF eStrandLabels[iStrandIndex] = STAY_ON_THIS_COMMAND
										CPRINTLN(DEBUG_DEBUG_MENU, "Multi label launch: Label for strand ", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(INT_TO_ENUM(STRANDS, iStrandIndex)), " set to ", GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(eCommandLabel), ".")
										eStrandLabels[iStrandIndex] = eCommandLabel
									ELSE
										SCRIPT_ASSERT("Menu item attempted to launch gameflow to multi points but two labels were defined in the same strand. Tell BenR.")
									ENDIF
									bExtraLabelFound[iExtraLabelIndex] = TRUE
								ENDIF
							ENDIF
						ENDREPEAT
					ENDIF
				ENDFOR
			ENDREPEAT
		#endif
		#if USE_NRM_DLC
			REPEAT MAX_STRANDS_NRM iStrandIndex
				//Check through all commands defined for this strand.
				INT iCommandIndex
				FOR iCommandIndex = g_flowUnsaved.flowCommandsIndex[iStrandIndex].lower TO g_flowUnsaved.flowCommandsIndex[iStrandIndex].upper
					JUMP_LABEL_IDS eCommandLabel = STAY_ON_THIS_COMMAND
				
					//Check if the command at this iCommandIndex is a 'Label' command.
					IF g_flowUnsaved.flowCommands[iCommandIndex].command = FLOW_LABEL
						eCommandLabel = INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.coreVars[g_flowUnsaved.flowCommands[iCommandIndex].index].iValue1)
					ENDIF
					
					//Check if the command at this iCommandIndex is a 'Label Launcher Only' command.
					IF g_flowUnsaved.flowCommands[iCommandIndex].command = FLOW_LABEL_LAUNCHER_ONLY
						eCommandLabel = INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.flowCommands[iCommandIndex].index)
					ENDIF
					
					// We've found a label command. Do any of the XML defined labels match it?
					IF eCommandLabel != STAY_ON_THIS_COMMAND
						//Does base label match?
						IF NOT bBaseLabelFound
							IF GET_HASH_KEY(GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(eCommandLabel)) = sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].iLabelID
								//Match found. Has this strand alread got a label set?
								IF eStrandLabels[iStrandIndex] = STAY_ON_THIS_COMMAND
									CPRINTLN(DEBUG_DEBUG_MENU, "Multi label launch: Label for strand ", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(INT_TO_ENUM(STRANDS, iStrandIndex)), " set to ", GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(eCommandLabel), ".")
									eStrandLabels[iStrandIndex] = eCommandLabel
								ELSE
									SCRIPT_ASSERT("Menu item attempted to launch gameflow to multi points but two points were defined in the same strand. Tell BenR.")
								ENDIF
								bBaseLabelFound = TRUE
							ENDIF
						ENDIF
						
						//Do any of the extra labels match?
						REPEAT MAX_EXTRA_LABEL_IDS iExtraLabelIndex
							IF NOT bExtraLabelFound[iExtraLabelIndex]
								IF GET_HASH_KEY(GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(eCommandLabel)) = sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].iExtraLabelID[iExtraLabelIndex]
									//Match found. Has this strand alread got a label set?
									IF eStrandLabels[iStrandIndex] = STAY_ON_THIS_COMMAND
										CPRINTLN(DEBUG_DEBUG_MENU, "Multi label launch: Label for strand ", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(INT_TO_ENUM(STRANDS, iStrandIndex)), " set to ", GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(eCommandLabel), ".")
										eStrandLabels[iStrandIndex] = eCommandLabel
									ELSE
										SCRIPT_ASSERT("Menu item attempted to launch gameflow to multi points but two labels were defined in the same strand. Tell BenR.")
									ENDIF
									bExtraLabelFound[iExtraLabelIndex] = TRUE
								ENDIF
							ENDIF
						ENDREPEAT
					ENDIF
				ENDFOR
			ENDREPEAT
		#endif
		#if not USE_CLF_DLC
		#if not USE_NRM_DLC
			REPEAT MAX_STRANDS iStrandIndex
				//Check through all commands defined for this strand.
				INT iCommandIndex
				FOR iCommandIndex = g_flowUnsaved.flowCommandsIndex[iStrandIndex].lower TO g_flowUnsaved.flowCommandsIndex[iStrandIndex].upper
					JUMP_LABEL_IDS eCommandLabel = STAY_ON_THIS_COMMAND
				
					//Check if the command at this iCommandIndex is a 'Label' command.
					IF g_flowUnsaved.flowCommands[iCommandIndex].command = FLOW_LABEL
						eCommandLabel = INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.coreVars[g_flowUnsaved.flowCommands[iCommandIndex].index].iValue1)
					ENDIF
					
					//Check if the command at this iCommandIndex is a 'Label Launcher Only' command.
					IF g_flowUnsaved.flowCommands[iCommandIndex].command = FLOW_LABEL_LAUNCHER_ONLY
						eCommandLabel = INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.flowCommands[iCommandIndex].index)
					ENDIF
					
					// We've found a label command. Do any of the XML defined labels match it?
					IF eCommandLabel != STAY_ON_THIS_COMMAND
						//Does base label match?
						IF NOT bBaseLabelFound
							IF GET_HASH_KEY(GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(eCommandLabel)) = sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].iLabelID
								//Match found. Has this strand alread got a label set?
								IF eStrandLabels[iStrandIndex] = STAY_ON_THIS_COMMAND
									CPRINTLN(DEBUG_DEBUG_MENU, "Multi label launch: Label for strand ", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(INT_TO_ENUM(STRANDS, iStrandIndex)), " set to ", GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(eCommandLabel), ".")
									eStrandLabels[iStrandIndex] = eCommandLabel
								ELSE
									SCRIPT_ASSERT("Menu item attempted to launch gameflow to multi points but two points were defined in the same strand. Tell BenR.")
								ENDIF
								bBaseLabelFound = TRUE
							ENDIF
						ENDIF
						
						//Do any of the extra labels match?
						REPEAT MAX_EXTRA_LABEL_IDS iExtraLabelIndex
							IF NOT bExtraLabelFound[iExtraLabelIndex]
								IF GET_HASH_KEY(GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(eCommandLabel)) = sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].iExtraLabelID[iExtraLabelIndex]
									//Match found. Has this strand alread got a label set?
									IF eStrandLabels[iStrandIndex] = STAY_ON_THIS_COMMAND
										CPRINTLN(DEBUG_DEBUG_MENU, "Multi label launch: Label for strand ", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(INT_TO_ENUM(STRANDS, iStrandIndex)), " set to ", GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(eCommandLabel), ".")
										eStrandLabels[iStrandIndex] = eCommandLabel
									ELSE
										SCRIPT_ASSERT("Menu item attempted to launch gameflow to multi points but two labels were defined in the same strand. Tell BenR.")
									ENDIF
									bExtraLabelFound[iExtraLabelIndex] = TRUE
								ENDIF
							ENDIF
						ENDREPEAT
					ENDIF
				ENDFOR
			ENDREPEAT
		#endif
		#endif
		

		VECTOR vPlayerPosition 	= <<0,0,0>>
		FLOAT fPlayerHeading	= 0.0
				
		RUN_PRE_GAMEFLOW_LAUNCH_ROUTINES(vPlayerPosition, fPlayerHeading)
		IF NOT LAUNCHER_LAUNCH_FLOW_TO_MULTIPLE_LABELS(flowLauncher, eStrandLabels)
			SCRIPT_ASSERT("Menu item attempted to launch gameflow to multi points but it was impossible to acheive. Please adjust labelIDs in XML.")
		ENDIF
		RUN_POST_GAMEFLOW_LAUNCH_ROUTINES(vPlayerPosition, fPlayerHeading)
		
		SET_DEBUG_MENU_CUSTOM_PROCESS_FLAG(sDebugMenuData.iCustomProcessFlags, DM_CUSTOM_PROCESS_LAUNCH_TO_MISSION_OUTCOME)
		CLEAR_DEBUG_MENU_CUSTOM_PROCESS_FLAG(sDebugMenuData.iCustomProcessFlags, DM_CUSTOM_PROCESS_LAUNCHER_MULTI_LABEL_LAUNCH)
		EXIT
	ENDIF
	
ENDPROC


PROC PROCESS_DEBUG_MENU_GAME_TIME_MANAGEMENT(DEBUG_MENU_DATA_STRUCT &sDebugMenuData)

	IF IS_DEBUG_MENU_CUSTOM_PROCESS_FLAG_SET(sDebugMenuData.iCustomProcessFlags, DM_CUSTOM_PROCESS_SET_GAME_TIME)
		IF sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].iHour <> -1
			SP_SET_TIME_OF_DAY(sDebugMenuData)
			CLEAR_DEBUG_MENU_CUSTOM_PROCESS_FLAG(sDebugMenuData.iCustomProcessFlags, DM_CUSTOM_PROCESS_SET_GAME_TIME)
		ENDIF
	ENDIF
	
ENDPROC


PROC PROCESS_DEBUG_MENU_CODE_ID_EXECUTION(DEBUG_MENU_DATA_STRUCT &sDebugMenuData)
	
	IF NOT IS_DEBUG_MENU_CUSTOM_PROCESS_FLAG_SET(sDebugMenuData.iCustomProcessFlags, DM_CUSTOM_PROCESS_LAUNCH_TO_MISSION_OUTCOME)
		IF IS_DEBUG_MENU_CUSTOM_PROCESS_FLAG_SET(sDebugMenuData.iCustomProcessFlags, DM_CUSTOM_PROCESS_RUN_CODE_ID)
			CALL sDebugMenuData.fpRunCodeID(sDebugMenuData)
			CLEAR_DEBUG_MENU_CUSTOM_PROCESS_FLAG(sDebugMenuData.iCustomProcessFlags, DM_CUSTOM_PROCESS_RUN_CODE_ID)
		ENDIF
	ENDIF
	
ENDPROC


PROC PROCESS_DEBUG_MENU_PRELAUNCH_CODE_ID_EXECUTION(DEBUG_MENU_DATA_STRUCT &sDebugMenuData)

	IF IS_DEBUG_MENU_CUSTOM_PROCESS_FLAG_SET(sDebugMenuData.iCustomProcessFlags, DM_CUSTOM_PROCESS_RUN_CODE_ID_PRELAUNCH)
		CALL sDebugMenuData.fpRunCodeIDPreLaunch(sDebugMenuData)
		CLEAR_DEBUG_MENU_CUSTOM_PROCESS_FLAG(sDebugMenuData.iCustomProcessFlags, DM_CUSTOM_PROCESS_RUN_CODE_ID_PRELAUNCH)
	ENDIF
	
ENDPROC


PROC DEBUG_MENU_PROCESS_CHANGE_MODES(DEBUG_MENU_DATA_STRUCT &sDebugMenuData)
	
	IF NOT (NETWORK_IS_GAME_IN_PROGRESS())
		// Fill in debug menu XML for SP
		#if USE_CLF_DLC
			sDebugMenuData.sXMLFile =	"SPDebug/SP_debugCLF_menu.xml"
		#endif
		#if USE_NRM_DLC
			sDebugMenuData.sXMLFile =	"SPDebug/SP_debugNRM_menu2.xml"
		#endif
		#if not USE_CLF_DLC
		#if not USE_NRM_DLC
			sDebugMenuData.sXMLFile = "SPDebug/SP_debug_menu.xml"
		#endif
		#endif
		// Fill in debug menu function pointers for SP
		sDebugMenuData.fpRunCodeID = &SP_RUN_SCRIPT_FOR_CODEID_POST_GAMEFLOW_LAUNCH
		sDebugMenuData.fpRunCodeIDPreLaunch = &SP_RUN_SCRIPT_FOR_CODEID_PRE_GAMEFLOW_LAUNCH
		sDebugMenuData.fpRunDisplayID = &SP_RUN_SCRIPT_FOR_DISPLAYID
		sDebugMenuData.fpRunEnableID = &SP_RUN_SCRIPT_FOR_ENABLEID
//		sDebugMenuData.fpProcessSelection = &DebugMenuProcessSelection_NULL
		sDebugMenuData.fpResetData = &SP_RESET_LAUNCH
		sDebugMenuData.fpCleanup = &SP_CLEANUP_LAUNCH
		sDebugMenuData.fpCheckSelection = &SP_CHECK_MENU_SELECTION
		sDebugMenuData.fpLaunchScript = &SP_LAUNCH_SCRIPT
		sDebugMenuData.fpWarpPlayer = &SP_WARP_PLAYER
		
		
	ELSE
		SWITCH INT_TO_ENUM(MP_GAMEMODE, NETWORK_GET_GAME_MODE())
			CASE GAMEMODE_FM
				sDebugMenuData.sXMLFile = "MPDebug/MP_FM_debug_menu.xml"
			BREAK
			#IF FEATURE_FREEMODE_ARCADE
			CASE GAMEMODE_ARCADE
			CASE GAMEMODE_FAKE_MP
				sDebugMenuData.sXMLFile = "MPDebug/MP_Arcade_debug_menu.xml"
			BREAK
			#ENDIF
		ENDSWITCH
		
		// Override for Ratings Build.
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_RatingsDebugMenu")
			sDebugMenuData.sXMLFile = "MPDebug/MP_Ratings_debug_menu.xml"
		ENDIF
		
		sDebugMenuData.fpRunCodeID = &MP_RUN_SCRIPT_FOR_CODEID
		sDebugMenuData.fpRunDisplayID = &MP_RUN_SCRIPT_FOR_DISPLAYID
		sDebugMenuData.fpRunEnableID = &MP_RUN_SCRIPT_FOR_ENABLEID
		sDebugMenuData.fpProcessSelection = &MP_PROCESS_SELECTION
		sDebugMenuData.fpResetData = &MP_RESET_LAUNCH
		sDebugMenuData.fpCleanup = &MP_CLEANUP_LAUNCH
		sDebugMenuData.fpCheckSelection = &MP_CHECK_MENU_SELECTION
		sDebugMenuData.fpLaunchScript = &MP_LAUNCH_SCRIPT
		sDebugMenuData.fpWarpPlayer = &MP_WARP_PLAYER
	ENDIF
ENDPROC
