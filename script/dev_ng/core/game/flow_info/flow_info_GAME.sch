USING "flow_info_utils_core.sch"
USING "comms_control_private.sch"

FUNC BOOL FLOW_INFO_IS_MISSION_ACTIVE(INT missionIndex)
	INT availableMissionIndex = GET_AVAILABLE_MISSION_INDEX_FOR_STORED_MISSION(missionIndex)
	
	IF availableMissionIndex = ILLEGAL_ARRAY_POSITION
		RETURN FALSE
	ENDIF
	
#IF USE_CLF_DLC
	IF (IS_BIT_SET(g_availableMissionsTU[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_LAUNCHING))
	OR (IS_BIT_SET(g_availableMissionsTU[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_RUNNING))
#ENDIF
#IF USE_NRM_DLC
	IF (IS_BIT_SET(g_availableMissionsTU[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_LAUNCHING))
	OR (IS_BIT_SET(g_availableMissionsTU[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_RUNNING))
#ENDIF
#IF NOT USE_CLF_DLC
#IF NOT USE_NRM_DLC
	IF (IS_BIT_SET(g_availableMissions[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_LAUNCHING))
	OR (IS_BIT_SET(g_availableMissions[availableMissionIndex].bitflags, BITS_AVAILABLE_MISSION_RUNNING))
#ENDIF
#ENDIF
RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC


// PURPOSE:	Display the 'do mission' message - should be suitable for all 'do mission' variants
FUNC FLOW_INFO_COLOUR DISPLAY_STRAND_DO_MISSION(INT paramCommandPos, FLOAT paramYPos, FLOW_INFO_COLOUR paramColour)
	FLOW_INFO_COLOUR highlightColour = paramColour
	
	INT coreVarsIndex = g_flowUnsaved.flowCommands[paramCommandPos].index
	IF FLOW_INFO_IS_MISSION_ACTIVE(coreVarsIndex)
		highlightColour = COL_ACTIVE
	ENDIF

	// The command
	FLOW_COMMAND_IDS thisCommand = g_flowUnsaved.flowCommands[paramCommandPos].command
	DISPLAY_FLOW_INFO_LITERAL_TEXT(highlightColour, FLOW_INFO_DESCRIPTION_XPOS, paramYPos, GET_COMMAND_DISPLAY_STRING_FROM_COMMAND_ID(thisCommand))

	// The Mission Filename
	DISPLAY_FLOW_INFO_LITERAL_TEXT(highlightColour, FLOW_INFO_PARAM1_XPOS, paramYPos, GET_SP_MISSION_DISPLAY_STRING_FROM_ID(INT_TO_ENUM(SP_MISSIONS, g_flowUnsaved.coreVars[coreVarsIndex].iValue1)))
	
	RETURN highlightColour
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Display the 'Do planning board interaction' message.
//
// INPUT PARAMS:	paramCommandPos		The position of the command within the commands array
//					paramYPos			The screen Y Pos for these details
//					paramColour			The text display colour
PROC DISPLAY_STRAND_DO_BOARD_INTERACTION(INT paramCommandPos, FLOAT paramYPos, FLOW_INFO_COLOUR paramColour)
	// The command
	Display_FLOW_INFO_Literal_Text(paramColour, FLOW_INFO_DESCRIPTION_XPOS, paramYPos, Get_Command_Display_String_From_Command_ID(FLOW_DO_BOARD_INTERACTION))
	
	// The interaction description.
	FLOW_STORAGE_TYPES flowStorageType = g_flowUnsaved.flowCommands[paramCommandPos].storageType
	IF flowStorageType != FST_COMMS
		// ...should never really hit this, but just in case
		EXIT
	ENDIF
	
	//Display interaction type.
	INT commsVarsIndex = g_flowUnsaved.flowCommands[paramCommandPos].index
	Display_FLOW_INFO_Literal_Text(paramColour, FLOW_INFO_PARAM1_XPOS, paramYPos, Get_Interaction_Display_String_From_Interaction_ID(INT_TO_ENUM(g_eBoardInteractions, g_flowUnsaved.commsVars[commsVarsIndex].iValue2)))
	
ENDPROC

// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Display the 'Comms' command stored in a commsVars array position.
//
// INPUT PARAMS:	paramCommandPos		The position of the command within the commands array
//					paramYPos			The screen Y Pos for these details
//					paramColour			The text display colour
PROC DISPLAY_STRAND_COMMS_COMMAND(INT paramCommandPos, FLOAT paramYPos, FLOW_INFO_COLOUR paramColour)
	// The command
	FLOW_COMMAND_IDS thisCommand = g_flowUnsaved.flowCommands[paramCommandPos].command
	Display_FLOW_INFO_Literal_Text(paramColour, FLOW_INFO_DESCRIPTION_XPOS, paramYPos, Get_Command_Display_String_From_Command_ID(thisCommand))

	// The Phonecall conversationID
	INT commsVarsIndex = g_flowUnsaved.flowCommands[paramCommandPos].index
	Display_FLOW_INFO_Literal_Text(	paramColour, 
									FLOW_INFO_PARAM1_XPOS, paramYPos, 
									GET_COMM_ID_DEBUG_STRING(INT_TO_ENUM(CC_CommID, g_flowUnsaved.commsVars[commsVarsIndex].iValue1)), 
									FLOW_INFO_TEXT_SCALE_SMALL_X, FLOW_INFO_TEXT_SCALE_SMALL_Y)
ENDPROC


// PURPOSE:	Display the 'Comms' command stored in a commsLargeVars array position.
//
// INPUT PARAMS:	paramCommandPos		The position of the command within the commands array
//					paramYPos			The screen Y Pos for these details
//					paramColour			The text display colour
PROC DISPLAY_STRAND_COMMS_LARGE_COMMAND(INT paramCommandPos, FLOAT paramYPos, FLOW_INFO_COLOUR paramColour)
	// The command
	FLOW_COMMAND_IDS thisCommand = g_flowUnsaved.flowCommands[paramCommandPos].command
	Display_FLOW_INFO_Literal_Text(paramColour, FLOW_INFO_DESCRIPTION_XPOS, paramYPos, Get_Command_Display_String_From_Command_ID(thisCommand))

	// The Phonecall conversationID
	INT commsLargeVarsIndex = g_flowUnsaved.flowCommands[paramCommandPos].index
	Display_FLOW_INFO_Literal_Text(	paramColour, 
									FLOW_INFO_PARAM1_XPOS, paramYPos, 
									GET_COMM_ID_DEBUG_STRING(INT_TO_ENUM(CC_CommID, g_flowUnsaved.commsLargeVars[commsLargeVarsIndex].iValue1)), 
									FLOW_INFO_TEXT_SCALE_SMALL_X, FLOW_INFO_TEXT_SCALE_SMALL_Y)
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Display the 'Add New Phone Contact' message.
//
// INPUT PARAMS:	paramCommandPos		The position of the command within the commands array
//					paramYPos			The screen Y Pos for these details
//					paramColour			The text display colour
//



// -----------------------------------------------------------------------------------------------------------
//		F9 Screen Communication Control Display
// -----------------------------------------------------------------------------------------------------------

// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Display playable character priority levels.
// 
PROC DISPLAY_PLAYABLE_CHARACTER_COMMUNICATION_PRIORITY_LEVELS(FLOAT paramYPos)
	TEXT_LABEL_63 tPriorityText
	if g_bLoadedClifford
		tPriorityText = "Mike: "
		tPriorityText += PRIVATE_Get_Debug_String_For_Communication_Priority(g_savedGlobalsClifford.sCommsControlData.eCharacterPriorityLevel[CHAR_MICHAEL])
		tPriorityText += "   Fran: "
		tPriorityText += PRIVATE_Get_Debug_String_For_Communication_Priority(g_savedGlobalsClifford.sCommsControlData.eCharacterPriorityLevel[CHAR_FRANKLIN])
		tPriorityText += "   Trev: "
		tPriorityText += PRIVATE_Get_Debug_String_For_Communication_Priority(g_savedGlobalsClifford.sCommsControlData.eCharacterPriorityLevel[CHAR_TREVOR])
	elif g_bLoadedNorman
		tPriorityText = "Mike: "
		tPriorityText += PRIVATE_Get_Debug_String_For_Communication_Priority(g_savedGlobalsnorman.sCommsControlData.eCharacterPriorityLevel[CHAR_MICHAEL])
		tPriorityText += "   Fran: "
		tPriorityText += PRIVATE_Get_Debug_String_For_Communication_Priority(g_savedGlobalsnorman.sCommsControlData.eCharacterPriorityLevel[CHAR_FRANKLIN])
		tPriorityText += "   Trev: "
		tPriorityText += PRIVATE_Get_Debug_String_For_Communication_Priority(g_savedGlobalsnorman.sCommsControlData.eCharacterPriorityLevel[CHAR_TREVOR])
	else
		tPriorityText = "Mike: "
		tPriorityText += PRIVATE_Get_Debug_String_For_Communication_Priority(g_savedGlobals.sCommsControlData.eCharacterPriorityLevel[CHAR_MICHAEL])
		tPriorityText += "   Fran: "
		tPriorityText += PRIVATE_Get_Debug_String_For_Communication_Priority(g_savedGlobals.sCommsControlData.eCharacterPriorityLevel[CHAR_FRANKLIN])
		tPriorityText += "   Trev: "
		tPriorityText += PRIVATE_Get_Debug_String_For_Communication_Priority(g_savedGlobals.sCommsControlData.eCharacterPriorityLevel[CHAR_TREVOR])
	endif
	
	Display_FLOW_INFO_Literal_Text(COL_MID, FLOW_INFO_COMM_NAME_XPOS, paramYPos, tPriorityText)
ENDPROC



// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Display the timer progress bar that should display behind each communication timer line.
// 
PROC Display_Communication_Timer_Background_Bar(INT paramTimerCount, INT paramTimerMax, FLOAT paramYPos)
	FLOAT fTopX = FLOW_INFO_COMM_TIMER_XPOS - 0.0025
	FLOAT fWidth = ((FLOW_INFO_COMM_STATUS_XPOS-fTopX)+0.05) * (TO_FLOAT(paramTimerCount)/TO_FLOAT(paramTimerMax))
	
	FLOAT fTopY = paramYPos + 0.0025
	FLOAT fHeight = FLOW_INFO_ADD_Y

	DRAW_RECT_FROM_CORNER(fTopX, fTopY, fWidth, fHeight, 0, 0, 0, 175)
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Display the information line for one of the communication controller global delay timers.
// 
PROC Display_Communication_Global_Delay_Timer(STRING paramTimerName, INT paramTimerCount, INT paramGameTime, FLOAT paramYPos)
	INT iTimeLeft = IMAX(0, paramTimerCount - paramGameTime)
	
	FLOW_INFO_COLOUR eTimerColour
	IF paramTimerCount = 0
		eTimerColour = COL_MID
	ELSE
		eTimerColour = COL_DULL
		Display_Communication_Timer_Background_Bar(iTimeLeft, FLOW_INFO_COMM_MAX_QUEUE_TIME, paramYPos)
	ENDIF
	
	INT iSingleDigit = CEIL(TO_FLOAT(iTimeLeft)/1000)
	
	Display_FLOW_INFO_TextLabel_With_One_Number(eTimerColour, FLOW_INFO_COMM_TIMER_XPOS, paramYPos, "NUMBER", iSingleDigit)
	Display_FLOW_INFO_Literal_Text(eTimerColour, FLOW_INFO_COMM_NAME_XPOS, paramYPos, paramTimerName)
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Display the information line for one of the communication controller character delay timers.
// 
PROC Display_Communication_Character_Delay_Timer(STRING paramTimerName, INT paramTimerCount, INT paramGameTime, FLOAT paramYPos)
	INT iTimeLeft = IMAX(0, paramTimerCount - paramGameTime)

	FLOW_INFO_COLOUR eTimerColour
	IF paramTimerCount = 0
		eTimerColour = COL_MID
	ELSE
		eTimerColour = COL_DULL
		Display_Communication_Timer_Background_Bar(iTimeLeft, FLOW_INFO_COMM_MAX_QUEUE_TIME, paramYPos)
	ENDIF
	
	INT iSingleDigit = CEIL(TO_FLOAT(iTimeLeft)/1000)
	
	Display_FLOW_INFO_TextLabel_With_One_Number(eTimerColour, FLOW_INFO_COMM_TIMER_XPOS, paramYPos, "NUMBER", iSingleDigit)
	Display_FLOW_INFO_Literal_Text(eTimerColour, FLOW_INFO_COMM_NAME_XPOS, paramYPos, paramTimerName)
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Display the information line for one of the communication controller phonecall or text message timers.
// 
PROC Display_Communication_CallText_Timer(STRING paramTimerName, STRING paramTimerChars, STRING paramTimerPriority, STRING paramTimerStatus, INT paramTimerCount, INT paramTimerMax, INT paramGameTime, FLOAT paramYPos, BOOL paramInProgress)
	INT iTimeLeft = IMAX(0, paramTimerCount - paramGameTime)
	
	FLOW_INFO_COLOUR eTimerColour
	IF paramInProgress
		eTimerColour = COL_BRIGHT
	ELIF paramTimerCount = 0
		eTimerColour = COL_MID
	ELSE
		eTimerColour = COL_DULL
		Display_Communication_Timer_Background_Bar(iTimeLeft, paramTimerMax, paramYPos)
	ENDIF
	
	INT iSingleDigit = CEIL(TO_FLOAT(iTimeLeft)/1000)
	
	Display_FLOW_INFO_TextLabel_With_One_Number(eTimerColour, FLOW_INFO_COMM_TIMER_XPOS, paramYPos, "NUMBER", iSingleDigit)
	Display_FLOW_INFO_Literal_Text(eTimerColour, FLOW_INFO_COMM_NAME_XPOS, paramYPos, paramTimerName, FLOW_INFO_TEXT_SCALE_SMALL_X, FLOW_INFO_TEXT_SCALE_SMALL_Y)
	Display_FLOW_INFO_Literal_Text(eTimerColour, FLOW_INFO_COMM_PRIORITY_XPOS, paramYPos, paramTimerPriority)
	Display_FLOW_INFO_Literal_Text(eTimerColour, FLOW_INFO_COMM_CHARS_XPOS, paramYPos, paramTimerChars)
	Display_FLOW_INFO_Literal_Text(eTimerColour, FLOW_INFO_COMM_STATUS_XPOS, paramYPos, paramTimerStatus)
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Display the information line for a call in the missed call queue.
// 
PROC Display_Communication_Static_CallText(STRING paramCallTextName, STRING paramCallTextChars, STRING paramCallTextPriority, FLOAT paramYPos)
	Display_FLOW_INFO_Literal_Text(COL_MID, FLOW_INFO_COMM_NAME_XPOS, paramYPos, paramCallTextName, FLOW_INFO_TEXT_SCALE_SMALL_X, FLOW_INFO_TEXT_SCALE_SMALL_Y)
	Display_FLOW_INFO_Literal_Text(COL_MID, FLOW_INFO_COMM_CHARS_XPOS, paramYPos, paramCallTextChars)
	Display_FLOW_INFO_Literal_Text(COL_MID, FLOW_INFO_COMM_PRIORITY_XPOS, paramYPos, paramCallTextPriority)
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Display the information line for a call in the missed call queue.
// 
PROC Display_Communication_Chat_Call(STRING paramStaticCallName, STRING paramStaticCallChars, STRING paramStaticCallPriority, INT paramQueueTime, INT paramGameTime, FLOAT paramYPos)
	INT iTimeLeft = IMAX(0, paramQueueTime - paramGameTime)
	INT iSingleDigit = CEIL(TO_FLOAT(iTimeLeft)/60000)
	
	Display_FLOW_INFO_TextLabel_With_One_Number(COL_MID, FLOW_INFO_COMM_TIMER_XPOS, paramYPos, "NUMBER", iSingleDigit)
	Display_FLOW_INFO_Literal_Text(COL_MID, FLOW_INFO_COMM_NAME_XPOS, paramYPos, paramStaticCallName, FLOW_INFO_TEXT_SCALE_SMALL_X, FLOW_INFO_TEXT_SCALE_SMALL_Y)
	Display_FLOW_INFO_Literal_Text(COL_MID, FLOW_INFO_COMM_PRIORITY_XPOS, paramYPos, paramStaticCallPriority)
	Display_FLOW_INFO_Literal_Text(COL_MID, FLOW_INFO_COMM_CHARS_XPOS, paramYPos, paramStaticCallChars)
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Display the communication controller section of the F9 screen.
// 
PROC Display_Communication_Controller()
	INT iGameTime = GET_GAME_TIMER()
	INT index
	FLOAT theYPos = FLOW_INFO_START_YPOS
	TEXT_LABEL_63 tTimerName
	
	//Display playable character priority levels.
	Display_FLOW_INFO_Literal_Text(COL_MID, FLOW_INFO_COMM_TIMER_XPOS, theYPos, "[Character Priority Levels]")
	theYPos += FLOW_INFO_ADD_Y
	Display_Playable_Character_Communication_Priority_Levels(theYPos)
	
	theYPos += FLOW_INFO_ADD_Y*2

	//Display all global delay timers.
	Display_FLOW_INFO_Literal_Text(COL_MID, FLOW_INFO_COMM_TIMER_XPOS, theYPos, "[Global Delay Timers]")
	IF iGameTime < g_iGlobalWaitTime
		theYPos += FLOW_INFO_ADD_Y
		Display_Communication_Global_Delay_Timer("Communication Global Delay", g_iGlobalWaitTime, iGameTime, theYPos)
	ENDIF
	
	if g_bLoadedClifford
		REPEAT MAX_CLF_CHARACTERS index
			IF iGameTime < g_iCharWaitTime[index]
				tTimerName = Get_CharSheet_Display_String_From_CharSheet(INT_TO_ENUM(enumCharacterList,index))
				tTimerName += " Character Delay"
			
				theYPos += FLOW_INFO_ADD_Y
				Display_Communication_Character_Delay_Timer(tTimerName, g_iCharWaitTime[index], iGameTime, theYPos)
			ENDIF
		ENDREPEAT
	elif g_bLoadedNorman
		REPEAT MAX_NRM_CHARACTERS index
			IF iGameTime < g_iCharWaitTime[index]
				tTimerName = Get_CharSheet_Display_String_From_CharSheet(INT_TO_ENUM(enumCharacterList,index))
				tTimerName += " Character Delay"
			
				theYPos += FLOW_INFO_ADD_Y
				Display_Communication_Character_Delay_Timer(tTimerName, g_iCharWaitTime[index], iGameTime, theYPos)
			ENDIF
		ENDREPEAT
	else
		INT iMaxCharacter = ENUM_TO_INT(GLOBAL_CHARACTER_SHEET_GET_MAX_CHARACTERS_FOR_GAMEMODE())
		REPEAT iMaxCharacter index
			IF iGameTime < g_iCharWaitTime[index]
				tTimerName = Get_CharSheet_Display_String_From_CharSheet(INT_TO_ENUM(enumCharacterList,index))
				tTimerName += " Character Delay"
			
				theYPos += FLOW_INFO_ADD_Y
				Display_Communication_Character_Delay_Timer(tTimerName, g_iCharWaitTime[index], iGameTime, theYPos)
			ENDIF
		ENDREPEAT
	endif	
	theYPos += FLOW_INFO_ADD_Y*2
	
	//Display queued calls.
	Display_FLOW_INFO_Literal_Text(COL_MID, FLOW_INFO_COMM_TIMER_XPOS, theYPos, "[Calls Queue]")
	TEXT_LABEL_63 tCharsString
	if g_bLoadedClifford
		REPEAT g_savedGlobalsClifford.sCommsControlData.iNoQueuedCalls index
			tTimerName = GET_COMM_ID_DEBUG_STRING(g_savedGlobalsClifford.sCommsControlData.sQueuedCalls[index].sCommData.eID)
			
			IF IS_BIT_SET(g_savedGlobalsClifford.sCommsControlData.sQueuedCalls[index].sCommData.iSettings, COMM_BIT_FROM_CHAR_IS_PLAYER)
				IF IS_BIT_SET(g_savedGlobalsClifford.sCommsControlData.sQueuedCalls[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(CHAR_MICHAEL))
					tCharsString += "M"
				ENDIF
				IF IS_BIT_SET(g_savedGlobalsClifford.sCommsControlData.sQueuedCalls[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(CHAR_FRANKLIN))
					tCharsString += "F"
				ENDIF
				IF IS_BIT_SET(g_savedGlobalsClifford.sCommsControlData.sQueuedCalls[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(CHAR_TREVOR))
					tCharsString += "T"
				ENDIF
				tCharsString += "->"
				tCharsString = Get_CharSheet_Display_String_From_CharSheet(g_savedGlobalsClifford.sCommsControlData.sQueuedCalls[index].sCommData.eNPCCharacter)
			ELSE
				tCharsString = Get_CharSheet_Display_String_From_CharSheet(g_savedGlobalsClifford.sCommsControlData.sQueuedCalls[index].sCommData.eNPCCharacter)
				tCharsString += "->"
				IF IS_BIT_SET(g_savedGlobalsClifford.sCommsControlData.sQueuedCalls[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(CHAR_MICHAEL))
					tCharsString += "M"
				ENDIF
				IF IS_BIT_SET(g_savedGlobalsClifford.sCommsControlData.sQueuedCalls[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(CHAR_FRANKLIN))
					tCharsString += "F"
				ENDIF
				IF IS_BIT_SET(g_savedGlobalsClifford.sCommsControlData.sQueuedCalls[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(CHAR_TREVOR))
					tCharsString += "T"
				ENDIF
			ENDIF

			BOOL bInProgress
			IF index = g_iCallInProgress
				bInProgress = TRUE
			ELSE
				bInProgress = FALSE
			ENDIF
			
			theYPos += FLOW_INFO_ADD_Y
			
			Display_Communication_CallText_Timer(	tTimerName, 
													tCharsString,
													PRIVATE_Get_Debug_String_For_Communication_Priority(g_savedGlobalsClifford.sCommsControlData.sQueuedCalls[index].sCommData.ePriority),
													PRIVATE_Get_Debug_String_For_Communication_Status(PRIVATE_Get_Phonecall_Status(g_savedGlobalsClifford.sCommsControlData.sQueuedCalls[index].sCommData.eID, iGameTime)),
													g_savedGlobalsClifford.sCommsControlData.sQueuedCalls[index].sCommData.iQueueTime, 
													FLOW_INFO_COMM_MAX_QUEUE_TIME,
													iGameTime,
													theYPos,
													bInProgress)
		ENDREPEAT
		
	elif g_bLoadedNorman
		REPEAT g_savedGlobalsnorman.sCommsControlData.iNoQueuedCalls index
			tTimerName = GET_COMM_ID_DEBUG_STRING(g_savedGlobalsnorman.sCommsControlData.sQueuedCalls[index].sCommData.eID)
			
			IF IS_BIT_SET(g_savedGlobalsnorman.sCommsControlData.sQueuedCalls[index].sCommData.iSettings, COMM_BIT_FROM_CHAR_IS_PLAYER)
				IF IS_BIT_SET(g_savedGlobalsnorman.sCommsControlData.sQueuedCalls[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(CHAR_MICHAEL))
					tCharsString += "M"
				ENDIF
				IF IS_BIT_SET(g_savedGlobalsnorman.sCommsControlData.sQueuedCalls[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(CHAR_FRANKLIN))
					tCharsString += "F"
				ENDIF
				IF IS_BIT_SET(g_savedGlobalsnorman.sCommsControlData.sQueuedCalls[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(CHAR_TREVOR))
					tCharsString += "T"
				ENDIF
				tCharsString += "->"
				tCharsString = Get_CharSheet_Display_String_From_CharSheet(g_savedGlobalsnorman.sCommsControlData.sQueuedCalls[index].sCommData.eNPCCharacter)
			ELSE
				tCharsString = Get_CharSheet_Display_String_From_CharSheet(g_savedGlobalsnorman.sCommsControlData.sQueuedCalls[index].sCommData.eNPCCharacter)
				tCharsString += "->"
				IF IS_BIT_SET(g_savedGlobalsnorman.sCommsControlData.sQueuedCalls[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(CHAR_MICHAEL))
					tCharsString += "M"
				ENDIF
				IF IS_BIT_SET(g_savedGlobalsnorman.sCommsControlData.sQueuedCalls[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(CHAR_FRANKLIN))
					tCharsString += "F"
				ENDIF
				IF IS_BIT_SET(g_savedGlobalsnorman.sCommsControlData.sQueuedCalls[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(CHAR_TREVOR))
					tCharsString += "T"
				ENDIF
			ENDIF

			BOOL bInProgress
			IF index = g_iCallInProgress
				bInProgress = TRUE
			ELSE
				bInProgress = FALSE
			ENDIF
			
			theYPos += FLOW_INFO_ADD_Y
			
			Display_Communication_CallText_Timer(	tTimerName, 
													tCharsString,
													PRIVATE_Get_Debug_String_For_Communication_Priority(g_savedGlobalsnorman.sCommsControlData.sQueuedCalls[index].sCommData.ePriority),
													PRIVATE_Get_Debug_String_For_Communication_Status(PRIVATE_Get_Phonecall_Status(g_savedGlobalsnorman.sCommsControlData.sQueuedCalls[index].sCommData.eID, iGameTime)),
													g_savedGlobalsnorman.sCommsControlData.sQueuedCalls[index].sCommData.iQueueTime, 
													FLOW_INFO_COMM_MAX_QUEUE_TIME,
													iGameTime,
													theYPos,
													bInProgress)
		ENDREPEAT
		
	else
		REPEAT g_savedGlobals.sCommsControlData.iNoQueuedCalls index
			tTimerName = GET_COMM_ID_DEBUG_STRING(g_savedGlobals.sCommsControlData.sQueuedCalls[index].sCommData.eID)
			
			IF IS_BIT_SET(g_savedGlobals.sCommsControlData.sQueuedCalls[index].sCommData.iSettings, COMM_BIT_FROM_CHAR_IS_PLAYER)
				IF IS_BIT_SET(g_savedGlobals.sCommsControlData.sQueuedCalls[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(CHAR_MICHAEL))
					tCharsString += "M"
				ENDIF
				IF IS_BIT_SET(g_savedGlobals.sCommsControlData.sQueuedCalls[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(CHAR_FRANKLIN))
					tCharsString += "F"
				ENDIF
				IF IS_BIT_SET(g_savedGlobals.sCommsControlData.sQueuedCalls[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(CHAR_TREVOR))
					tCharsString += "T"
				ENDIF
				tCharsString += "->"
				tCharsString = Get_CharSheet_Display_String_From_CharSheet(g_savedGlobals.sCommsControlData.sQueuedCalls[index].sCommData.eNPCCharacter)
			ELSE
				tCharsString = Get_CharSheet_Display_String_From_CharSheet(g_savedGlobals.sCommsControlData.sQueuedCalls[index].sCommData.eNPCCharacter)
				tCharsString += "->"
				IF IS_BIT_SET(g_savedGlobals.sCommsControlData.sQueuedCalls[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(CHAR_MICHAEL))
					tCharsString += "M"
				ENDIF
				IF IS_BIT_SET(g_savedGlobals.sCommsControlData.sQueuedCalls[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(CHAR_FRANKLIN))
					tCharsString += "F"
				ENDIF
				IF IS_BIT_SET(g_savedGlobals.sCommsControlData.sQueuedCalls[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(CHAR_TREVOR))
					tCharsString += "T"
				ENDIF
			ENDIF

			BOOL bInProgress
			IF index = g_iCallInProgress
				bInProgress = TRUE
			ELSE
				bInProgress = FALSE
			ENDIF
			
			theYPos += FLOW_INFO_ADD_Y
			
			Display_Communication_CallText_Timer(	tTimerName, 
													tCharsString,
													PRIVATE_Get_Debug_String_For_Communication_Priority(g_savedGlobals.sCommsControlData.sQueuedCalls[index].sCommData.ePriority),
													PRIVATE_Get_Debug_String_For_Communication_Status(PRIVATE_Get_Phonecall_Status(g_savedGlobals.sCommsControlData.sQueuedCalls[index].sCommData.eID, iGameTime)),
													g_savedGlobals.sCommsControlData.sQueuedCalls[index].sCommData.iQueueTime, 
													FLOW_INFO_COMM_MAX_QUEUE_TIME,
													iGameTime,
													theYPos,
													bInProgress)
		ENDREPEAT
	endif
	theYPos += FLOW_INFO_ADD_Y*2
	
	//Display queued texts.
	Display_FLOW_INFO_Literal_Text(COL_MID, FLOW_INFO_COMM_TIMER_XPOS, theYPos, "[Text Queue]")
	if g_bLoadedClifford
		REPEAT g_savedGlobalsClifford.sCommsControlData.iNoQueuedTexts index
			tTimerName = GET_COMM_ID_DEBUG_STRING(g_savedGlobalsClifford.sCommsControlData.sQueuedTexts[index].sCommData.eID)
			
			tCharsString = Get_CharSheet_Display_String_From_CharSheet(g_savedGlobalsClifford.sCommsControlData.sQueuedTexts[index].sCommData.eNPCCharacter)
			tCharsString += "->"
			IF IS_BIT_SET(g_savedGlobalsClifford.sCommsControlData.sQueuedTexts[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(CHAR_MICHAEL))
				tCharsString += "M"
			ENDIF
			IF IS_BIT_SET(g_savedGlobalsClifford.sCommsControlData.sQueuedTexts[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(CHAR_FRANKLIN))
				tCharsString += "F"
			ENDIF
			IF IS_BIT_SET(g_savedGlobalsClifford.sCommsControlData.sQueuedTexts[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(CHAR_TREVOR))
				tCharsString += "T"
			ENDIF
			
			theYPos += FLOW_INFO_ADD_Y
			Display_Communication_CallText_Timer(	tTimerName,
													tCharsString,
													PRIVATE_Get_Debug_String_For_Communication_Priority(g_savedGlobalsClifford.sCommsControlData.sQueuedTexts[index].sCommData.ePriority), 
													PRIVATE_Get_Debug_String_For_Communication_Status(PRIVATE_Get_Text_Message_Status(g_savedGlobalsClifford.sCommsControlData.sQueuedTexts[index].sCommData.eID, iGameTime)),
													g_savedGlobalsClifford.sCommsControlData.sQueuedTexts[index].sCommData.iQueueTime, 
													FLOW_INFO_COMM_MAX_QUEUE_TIME,
													iGameTime,
													theYPos,
													FALSE)
		ENDREPEAT
		
	elif g_bLoadedNorman
		REPEAT g_savedGlobalsnorman.sCommsControlData.iNoQueuedTexts index
			tTimerName = GET_COMM_ID_DEBUG_STRING(g_savedGlobalsnorman.sCommsControlData.sQueuedTexts[index].sCommData.eID)
			
			tCharsString = Get_CharSheet_Display_String_From_CharSheet(g_savedGlobalsnorman.sCommsControlData.sQueuedTexts[index].sCommData.eNPCCharacter)
			tCharsString += "->"
			IF IS_BIT_SET(g_savedGlobalsnorman.sCommsControlData.sQueuedTexts[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(CHAR_MICHAEL))
				tCharsString += "M"
			ENDIF
			IF IS_BIT_SET(g_savedGlobalsnorman.sCommsControlData.sQueuedTexts[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(CHAR_FRANKLIN))
				tCharsString += "F"
			ENDIF
			IF IS_BIT_SET(g_savedGlobalsnorman.sCommsControlData.sQueuedTexts[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(CHAR_TREVOR))
				tCharsString += "T"
			ENDIF
			
			theYPos += FLOW_INFO_ADD_Y
			Display_Communication_CallText_Timer(	tTimerName,
													tCharsString,
													PRIVATE_Get_Debug_String_For_Communication_Priority(g_savedGlobalsnorman.sCommsControlData.sQueuedTexts[index].sCommData.ePriority), 
													PRIVATE_Get_Debug_String_For_Communication_Status(PRIVATE_Get_Text_Message_Status(g_savedGlobalsnorman.sCommsControlData.sQueuedTexts[index].sCommData.eID, iGameTime)),
													g_savedGlobalsnorman.sCommsControlData.sQueuedTexts[index].sCommData.iQueueTime, 
													FLOW_INFO_COMM_MAX_QUEUE_TIME,
													iGameTime,
													theYPos,
													FALSE)
		ENDREPEAT	
		
	else
		REPEAT g_savedGlobals.sCommsControlData.iNoQueuedTexts index
			tTimerName = GET_COMM_ID_DEBUG_STRING(g_savedGlobals.sCommsControlData.sQueuedTexts[index].sCommData.eID)
			
			tCharsString = Get_CharSheet_Display_String_From_CharSheet(g_savedGlobals.sCommsControlData.sQueuedTexts[index].sCommData.eNPCCharacter)
			tCharsString += "->"
			IF IS_BIT_SET(g_savedGlobals.sCommsControlData.sQueuedTexts[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(CHAR_MICHAEL))
				tCharsString += "M"
			ENDIF
			IF IS_BIT_SET(g_savedGlobals.sCommsControlData.sQueuedTexts[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(CHAR_FRANKLIN))
				tCharsString += "F"
			ENDIF
			IF IS_BIT_SET(g_savedGlobals.sCommsControlData.sQueuedTexts[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(CHAR_TREVOR))
				tCharsString += "T"
			ENDIF
			
			theYPos += FLOW_INFO_ADD_Y
			Display_Communication_CallText_Timer(	tTimerName,
													tCharsString,
													PRIVATE_Get_Debug_String_For_Communication_Priority(g_savedGlobals.sCommsControlData.sQueuedTexts[index].sCommData.ePriority), 
													PRIVATE_Get_Debug_String_For_Communication_Status(PRIVATE_Get_Text_Message_Status(g_savedGlobals.sCommsControlData.sQueuedTexts[index].sCommData.eID, iGameTime)),
													g_savedGlobals.sCommsControlData.sQueuedTexts[index].sCommData.iQueueTime, 
													FLOW_INFO_COMM_MAX_QUEUE_TIME,
													iGameTime,
													theYPos,
													FALSE)
		ENDREPEAT
	endif
	theYPos += FLOW_INFO_ADD_Y*2
	
	//Display queued emails.
	Display_FLOW_INFO_Literal_Text(COL_MID, FLOW_INFO_COMM_TIMER_XPOS, theYPos, "[Email Queue]")
	if g_bLoadedClifford
		REPEAT g_savedGlobalsClifford.sCommsControlData.iNoQueuedEmails index
			tTimerName = GET_COMM_ID_DEBUG_STRING(g_savedGlobalsClifford.sCommsControlData.sQueuedEmails[index].sCommData.eID)
			
			tCharsString = Get_CharSheet_Display_String_From_CharSheet(g_savedGlobalsClifford.sCommsControlData.sQueuedEmails[index].sCommData.eNPCCharacter)
			tCharsString += "->"
			IF IS_BIT_SET(g_savedGlobalsClifford.sCommsControlData.sQueuedEmails[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(CHAR_MICHAEL))
				tCharsString += "M"
			ENDIF
			IF IS_BIT_SET(g_savedGlobalsClifford.sCommsControlData.sQueuedEmails[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(CHAR_FRANKLIN))
				tCharsString += "F"
			ENDIF
			IF IS_BIT_SET(g_savedGlobalsClifford.sCommsControlData.sQueuedEmails[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(CHAR_TREVOR))
				tCharsString += "T"
			ENDIF
			
			theYPos += FLOW_INFO_ADD_Y
			Display_Communication_CallText_Timer(	tTimerName,
													tCharsString,
													PRIVATE_Get_Debug_String_For_Communication_Priority(g_savedGlobalsClifford.sCommsControlData.sQueuedEmails[index].sCommData.ePriority), 
													PRIVATE_Get_Debug_String_For_Communication_Status(PRIVATE_Get_Email_Status(g_savedGlobalsClifford.sCommsControlData.sQueuedEmails[index].sCommData.eID, iGameTime)),
													g_savedGlobalsClifford.sCommsControlData.sQueuedEmails[index].sCommData.iQueueTime, 
													FLOW_INFO_COMM_MAX_QUEUE_TIME,
													iGameTime,
													theYPos,
													FALSE)
		ENDREPEAT
		
	elif g_bLoadedNorman
		REPEAT g_savedGlobalsnorman.sCommsControlData.iNoQueuedEmails index
			tTimerName = GET_COMM_ID_DEBUG_STRING(g_savedGlobalsnorman.sCommsControlData.sQueuedEmails[index].sCommData.eID)
			
			tCharsString = Get_CharSheet_Display_String_From_CharSheet(g_savedGlobalsnorman.sCommsControlData.sQueuedEmails[index].sCommData.eNPCCharacter)
			tCharsString += "->"
			IF IS_BIT_SET(g_savedGlobalsnorman.sCommsControlData.sQueuedEmails[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(CHAR_MICHAEL))
				tCharsString += "M"
			ENDIF
			IF IS_BIT_SET(g_savedGlobalsnorman.sCommsControlData.sQueuedEmails[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(CHAR_FRANKLIN))
				tCharsString += "F"
			ENDIF
			IF IS_BIT_SET(g_savedGlobalsnorman.sCommsControlData.sQueuedEmails[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(CHAR_TREVOR))
				tCharsString += "T"
			ENDIF
			
			theYPos += FLOW_INFO_ADD_Y
			Display_Communication_CallText_Timer(	tTimerName,
													tCharsString,
													PRIVATE_Get_Debug_String_For_Communication_Priority(g_savedGlobalsnorman.sCommsControlData.sQueuedEmails[index].sCommData.ePriority), 
													PRIVATE_Get_Debug_String_For_Communication_Status(PRIVATE_Get_Email_Status(g_savedGlobalsnorman.sCommsControlData.sQueuedEmails[index].sCommData.eID, iGameTime)),
													g_savedGlobalsnorman.sCommsControlData.sQueuedEmails[index].sCommData.iQueueTime, 
													FLOW_INFO_COMM_MAX_QUEUE_TIME,
													iGameTime,
													theYPos,
													FALSE)
		ENDREPEAT	
		
	else
		REPEAT g_savedGlobals.sCommsControlData.iNoQueuedEmails index
			tTimerName = GET_COMM_ID_DEBUG_STRING(g_savedGlobals.sCommsControlData.sQueuedEmails[index].sCommData.eID)
			
			tCharsString = Get_CharSheet_Display_String_From_CharSheet(g_savedGlobals.sCommsControlData.sQueuedEmails[index].sCommData.eNPCCharacter)
			tCharsString += "->"
			IF IS_BIT_SET(g_savedGlobals.sCommsControlData.sQueuedEmails[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(CHAR_MICHAEL))
				tCharsString += "M"
			ENDIF
			IF IS_BIT_SET(g_savedGlobals.sCommsControlData.sQueuedEmails[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(CHAR_FRANKLIN))
				tCharsString += "F"
			ENDIF
			IF IS_BIT_SET(g_savedGlobals.sCommsControlData.sQueuedEmails[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(CHAR_TREVOR))
				tCharsString += "T"
			ENDIF
			
			theYPos += FLOW_INFO_ADD_Y
			Display_Communication_CallText_Timer(	tTimerName,
													tCharsString,
													PRIVATE_Get_Debug_String_For_Communication_Priority(g_savedGlobals.sCommsControlData.sQueuedEmails[index].sCommData.ePriority), 
													PRIVATE_Get_Debug_String_For_Communication_Status(PRIVATE_Get_Email_Status(g_savedGlobals.sCommsControlData.sQueuedEmails[index].sCommData.eID, iGameTime)),
													g_savedGlobals.sCommsControlData.sQueuedEmails[index].sCommData.iQueueTime, 
													FLOW_INFO_COMM_MAX_QUEUE_TIME,
													iGameTime,
													theYPos,
													FALSE)
		ENDREPEAT
	endif
	theYPos += FLOW_INFO_ADD_Y*2
	
	//Display registered chat calls.
	Display_FLOW_INFO_Literal_Text(COL_MID, FLOW_INFO_COMM_TIMER_XPOS, theYPos, "[Chat Calls]")
	if g_bLoadedClifford
		REPEAT IMIN(g_savedGlobalsClifford.sCommsControlData.iNoChatCalls, FLOW_INFO_COMM_MAX_CHAT_CALL_DISPLAY) index
			tTimerName = GET_COMM_ID_DEBUG_STRING(g_savedGlobalsClifford.sCommsControlData.sChatCalls[index].sCommData.eID)
			
			tCharsString = Get_CharSheet_Display_String_From_CharSheet(g_savedGlobalsClifford.sCommsControlData.sChatCalls[index].sCommData.eNPCCharacter)
			tCharsString += "->"
			IF IS_BIT_SET(g_savedGlobalsClifford.sCommsControlData.sChatCalls[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(CHAR_MICHAEL))
				tCharsString += "M"
			ENDIF
			IF IS_BIT_SET(g_savedGlobalsClifford.sCommsControlData.sChatCalls[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(CHAR_FRANKLIN))
				tCharsString += "F"
			ENDIF
			IF IS_BIT_SET(g_savedGlobalsClifford.sCommsControlData.sChatCalls[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(CHAR_TREVOR))
				tCharsString += "T"
			ENDIF
			
			theYPos += FLOW_INFO_ADD_Y
			Display_Communication_Chat_Call(	tTimerName, 
												tCharsString,
												PRIVATE_Get_Debug_String_For_Communication_Priority(g_savedGlobalsClifford.sCommsControlData.sChatCalls[index].sCommData.ePriority),
												g_savedGlobalsClifford.sCommsControlData.sChatCalls[index].sCommData.iQueueTime,
												iGameTime,
												theYPos)
		ENDREPEAT
		//Check if there were too many chat calls to render. Make it obvious we cut the list short.
		IF g_savedGlobalsClifford.sCommsControlData.iNoChatCalls > FLOW_INFO_COMM_MAX_CHAT_CALL_DISPLAY
			theYPos += FLOW_INFO_ADD_Y
			Display_FLOW_INFO_Literal_Text(COL_MID, FLOW_INFO_COMM_TIMER_XPOS, theYPos, "...")
			Display_FLOW_INFO_Literal_Text(COL_MID, FLOW_INFO_COMM_NAME_XPOS, theYPos, "...")
			Display_FLOW_INFO_Literal_Text(COL_MID, FLOW_INFO_COMM_PRIORITY_XPOS, theYPos, "...")
			Display_FLOW_INFO_Literal_Text(COL_MID, FLOW_INFO_COMM_CHARS_XPOS, theYPos, "...")
		ENDIF
		
	elif g_bLoadedNorman
		REPEAT IMIN(g_savedGlobalsnorman.sCommsControlData.iNoChatCalls, FLOW_INFO_COMM_MAX_CHAT_CALL_DISPLAY) index
			tTimerName = GET_COMM_ID_DEBUG_STRING(g_savedGlobalsnorman.sCommsControlData.sChatCalls[index].sCommData.eID)
			
			tCharsString = Get_CharSheet_Display_String_From_CharSheet(g_savedGlobalsnorman.sCommsControlData.sChatCalls[index].sCommData.eNPCCharacter)
			tCharsString += "->"
			IF IS_BIT_SET(g_savedGlobalsnorman.sCommsControlData.sChatCalls[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(CHAR_MICHAEL))
				tCharsString += "M"
			ENDIF
			IF IS_BIT_SET(g_savedGlobalsnorman.sCommsControlData.sChatCalls[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(CHAR_FRANKLIN))
				tCharsString += "F"
			ENDIF
			IF IS_BIT_SET(g_savedGlobalsnorman.sCommsControlData.sChatCalls[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(CHAR_TREVOR))
				tCharsString += "T"
			ENDIF
			
			theYPos += FLOW_INFO_ADD_Y
			Display_Communication_Chat_Call(	tTimerName, 
												tCharsString,
												PRIVATE_Get_Debug_String_For_Communication_Priority(g_savedGlobalsnorman.sCommsControlData.sChatCalls[index].sCommData.ePriority),
												g_savedGlobalsnorman.sCommsControlData.sChatCalls[index].sCommData.iQueueTime,
												iGameTime,
												theYPos)
		ENDREPEAT
		//Check if there were too many chat calls to render. Make it obvious we cut the list short.
		IF g_savedGlobalsnorman.sCommsControlData.iNoChatCalls > FLOW_INFO_COMM_MAX_CHAT_CALL_DISPLAY
			theYPos += FLOW_INFO_ADD_Y
			Display_FLOW_INFO_Literal_Text(COL_MID, FLOW_INFO_COMM_TIMER_XPOS, theYPos, "...")
			Display_FLOW_INFO_Literal_Text(COL_MID, FLOW_INFO_COMM_NAME_XPOS, theYPos, "...")
			Display_FLOW_INFO_Literal_Text(COL_MID, FLOW_INFO_COMM_PRIORITY_XPOS, theYPos, "...")
			Display_FLOW_INFO_Literal_Text(COL_MID, FLOW_INFO_COMM_CHARS_XPOS, theYPos, "...")
		ENDIF	
	else
		REPEAT IMIN(g_savedGlobals.sCommsControlData.iNoChatCalls, FLOW_INFO_COMM_MAX_CHAT_CALL_DISPLAY) index
			tTimerName = GET_COMM_ID_DEBUG_STRING(g_savedGlobals.sCommsControlData.sChatCalls[index].sCommData.eID)
			
			tCharsString = Get_CharSheet_Display_String_From_CharSheet(g_savedGlobals.sCommsControlData.sChatCalls[index].sCommData.eNPCCharacter)
			tCharsString += "->"
			IF IS_BIT_SET(g_savedGlobals.sCommsControlData.sChatCalls[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(CHAR_MICHAEL))
				tCharsString += "M"
			ENDIF
			IF IS_BIT_SET(g_savedGlobals.sCommsControlData.sChatCalls[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(CHAR_FRANKLIN))
				tCharsString += "F"
			ENDIF
			IF IS_BIT_SET(g_savedGlobals.sCommsControlData.sChatCalls[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(CHAR_TREVOR))
				tCharsString += "T"
			ENDIF
			
			theYPos += FLOW_INFO_ADD_Y
			Display_Communication_Chat_Call(	tTimerName, 
												tCharsString,
												PRIVATE_Get_Debug_String_For_Communication_Priority(g_savedGlobals.sCommsControlData.sChatCalls[index].sCommData.ePriority),
												g_savedGlobals.sCommsControlData.sChatCalls[index].sCommData.iQueueTime,
												iGameTime,
												theYPos)
		ENDREPEAT
		//Check if there were too many chat calls to render. Make it obvious we cut the list short.
		IF g_savedGlobals.sCommsControlData.iNoChatCalls > FLOW_INFO_COMM_MAX_CHAT_CALL_DISPLAY
			theYPos += FLOW_INFO_ADD_Y
			Display_FLOW_INFO_Literal_Text(COL_MID, FLOW_INFO_COMM_TIMER_XPOS, theYPos, "...")
			Display_FLOW_INFO_Literal_Text(COL_MID, FLOW_INFO_COMM_NAME_XPOS, theYPos, "...")
			Display_FLOW_INFO_Literal_Text(COL_MID, FLOW_INFO_COMM_PRIORITY_XPOS, theYPos, "...")
			Display_FLOW_INFO_Literal_Text(COL_MID, FLOW_INFO_COMM_CHARS_XPOS, theYPos, "...")
		ENDIF
	endif
	theYPos += FLOW_INFO_ADD_Y*2
	
	//Display missed call queue.
	Display_FLOW_INFO_Literal_Text(COL_MID, FLOW_INFO_COMM_TIMER_XPOS, theYPos, "[Missed Calls]")
	if g_bLoadedClifford
		REPEAT g_savedGlobalsClifford.sCommsControlData.iNoMissedCalls index
			tTimerName = GET_COMM_ID_DEBUG_STRING(g_savedGlobalsClifford.sCommsControlData.sMissedCalls[index].sCommData.eID)
			
			tCharsString = Get_CharSheet_Display_String_From_CharSheet(g_savedGlobalsClifford.sCommsControlData.sMissedCalls[index].sCommData.eNPCCharacter)
			tCharsString += "->"
			IF IS_BIT_SET(g_savedGlobalsClifford.sCommsControlData.sMissedCalls[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(CHAR_MICHAEL))
				tCharsString += "M"
			ENDIF
			IF IS_BIT_SET(g_savedGlobalsClifford.sCommsControlData.sMissedCalls[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(CHAR_FRANKLIN))
				tCharsString += "F"
			ENDIF
			IF IS_BIT_SET(g_savedGlobalsClifford.sCommsControlData.sMissedCalls[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(CHAR_TREVOR))
				tCharsString += "T"
			ENDIF
			
			theYPos += FLOW_INFO_ADD_Y
			Display_Communication_Static_CallText(	tTimerName, 
													tCharsString,
													PRIVATE_Get_Debug_String_For_Communication_Priority(g_savedGlobalsClifford.sCommsControlData.sMissedCalls[index].sCommData.ePriority),
													theYPos)
		ENDREPEAT
		theYPos += FLOW_INFO_ADD_Y*2	
		//Display sent text queue.
		Display_FLOW_INFO_Literal_Text(COL_MID, FLOW_INFO_COMM_TIMER_XPOS, theYPos, "[Sent Texts]")
		REPEAT g_savedGlobalsClifford.sCommsControlData.iNoSentTexts index
			tTimerName = GET_COMM_ID_DEBUG_STRING(g_savedGlobalsClifford.sCommsControlData.sSentTexts[index].sCommData.eID)
			
			tCharsString = Get_CharSheet_Display_String_From_CharSheet(g_savedGlobalsClifford.sCommsControlData.sSentTexts[index].sCommData.eNPCCharacter)
			tCharsString += "->"
			IF IS_BIT_SET(g_savedGlobalsClifford.sCommsControlData.sSentTexts[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(CHAR_MICHAEL))
				tCharsString += "M"
			ENDIF
			IF IS_BIT_SET(g_savedGlobalsClifford.sCommsControlData.sSentTexts[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(CHAR_FRANKLIN))
				tCharsString += "F"
			ENDIF
			IF IS_BIT_SET(g_savedGlobalsClifford.sCommsControlData.sSentTexts[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(CHAR_TREVOR))
				tCharsString += "T"
			ENDIF
			
			theYPos += FLOW_INFO_ADD_Y
			Display_Communication_Static_CallText(	tTimerName, 
													tCharsString,
													PRIVATE_Get_Debug_String_For_Communication_Priority(g_savedGlobalsClifford.sCommsControlData.sSentTexts[index].sCommData.ePriority),
													theYPos)
		ENDREPEAT
	elif g_bLoadedNorman
		REPEAT g_savedGlobalsnorman.sCommsControlData.iNoMissedCalls index
			tTimerName = GET_COMM_ID_DEBUG_STRING(g_savedGlobalsnorman.sCommsControlData.sMissedCalls[index].sCommData.eID)
			
			tCharsString = Get_CharSheet_Display_String_From_CharSheet(g_savedGlobalsnorman.sCommsControlData.sMissedCalls[index].sCommData.eNPCCharacter)
			tCharsString += "->"
			IF IS_BIT_SET(g_savedGlobalsnorman.sCommsControlData.sMissedCalls[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(CHAR_MICHAEL))
				tCharsString += "M"
			ENDIF
			IF IS_BIT_SET(g_savedGlobalsnorman.sCommsControlData.sMissedCalls[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(CHAR_FRANKLIN))
				tCharsString += "F"
			ENDIF
			IF IS_BIT_SET(g_savedGlobalsnorman.sCommsControlData.sMissedCalls[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(CHAR_TREVOR))
				tCharsString += "T"
			ENDIF
			
			theYPos += FLOW_INFO_ADD_Y
			Display_Communication_Static_CallText(	tTimerName, 
													tCharsString,
													PRIVATE_Get_Debug_String_For_Communication_Priority(g_savedGlobalsnorman.sCommsControlData.sMissedCalls[index].sCommData.ePriority),
													theYPos)
		ENDREPEAT
		theYPos += FLOW_INFO_ADD_Y*2	
		//Display sent text queue.
		Display_FLOW_INFO_Literal_Text(COL_MID, FLOW_INFO_COMM_TIMER_XPOS, theYPos, "[Sent Texts]")
		REPEAT g_savedGlobalsnorman.sCommsControlData.iNoSentTexts index
			tTimerName = GET_COMM_ID_DEBUG_STRING(g_savedGlobalsnorman.sCommsControlData.sSentTexts[index].sCommData.eID)
			
			tCharsString = Get_CharSheet_Display_String_From_CharSheet(g_savedGlobalsnorman.sCommsControlData.sSentTexts[index].sCommData.eNPCCharacter)
			tCharsString += "->"
			IF IS_BIT_SET(g_savedGlobalsnorman.sCommsControlData.sSentTexts[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(CHAR_MICHAEL))
				tCharsString += "M"
			ENDIF
			IF IS_BIT_SET(g_savedGlobalsnorman.sCommsControlData.sSentTexts[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(CHAR_FRANKLIN))
				tCharsString += "F"
			ENDIF
			IF IS_BIT_SET(g_savedGlobalsnorman.sCommsControlData.sSentTexts[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(CHAR_TREVOR))
				tCharsString += "T"
			ENDIF
			
			theYPos += FLOW_INFO_ADD_Y
			Display_Communication_Static_CallText(	tTimerName, 
													tCharsString,
													PRIVATE_Get_Debug_String_For_Communication_Priority(g_savedGlobalsnorman.sCommsControlData.sSentTexts[index].sCommData.ePriority),
													theYPos)
		ENDREPEAT	
	else
		REPEAT g_savedGlobals.sCommsControlData.iNoMissedCalls index
			tTimerName = GET_COMM_ID_DEBUG_STRING(g_savedGlobals.sCommsControlData.sMissedCalls[index].sCommData.eID)
			
			tCharsString = Get_CharSheet_Display_String_From_CharSheet(g_savedGlobals.sCommsControlData.sMissedCalls[index].sCommData.eNPCCharacter)
			tCharsString += "->"
			IF IS_BIT_SET(g_savedGlobals.sCommsControlData.sMissedCalls[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(CHAR_MICHAEL))
				tCharsString += "M"
			ENDIF
			IF IS_BIT_SET(g_savedGlobals.sCommsControlData.sMissedCalls[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(CHAR_FRANKLIN))
				tCharsString += "F"
			ENDIF
			IF IS_BIT_SET(g_savedGlobals.sCommsControlData.sMissedCalls[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(CHAR_TREVOR))
				tCharsString += "T"
			ENDIF
			
			theYPos += FLOW_INFO_ADD_Y
			Display_Communication_Static_CallText(	tTimerName, 
													tCharsString,
													PRIVATE_Get_Debug_String_For_Communication_Priority(g_savedGlobals.sCommsControlData.sMissedCalls[index].sCommData.ePriority),
													theYPos)
		ENDREPEAT
		theYPos += FLOW_INFO_ADD_Y*2	
		//Display sent text queue.
		Display_FLOW_INFO_Literal_Text(COL_MID, FLOW_INFO_COMM_TIMER_XPOS, theYPos, "[Sent Texts]")
		REPEAT g_savedGlobals.sCommsControlData.iNoSentTexts index
			tTimerName = GET_COMM_ID_DEBUG_STRING(g_savedGlobals.sCommsControlData.sSentTexts[index].sCommData.eID)
			
			tCharsString = Get_CharSheet_Display_String_From_CharSheet(g_savedGlobals.sCommsControlData.sSentTexts[index].sCommData.eNPCCharacter)
			tCharsString += "->"
			IF IS_BIT_SET(g_savedGlobals.sCommsControlData.sSentTexts[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(CHAR_MICHAEL))
				tCharsString += "M"
			ENDIF
			IF IS_BIT_SET(g_savedGlobals.sCommsControlData.sSentTexts[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(CHAR_FRANKLIN))
				tCharsString += "F"
			ENDIF
			IF IS_BIT_SET(g_savedGlobals.sCommsControlData.sSentTexts[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(CHAR_TREVOR))
				tCharsString += "T"
			ENDIF
			
			theYPos += FLOW_INFO_ADD_Y
			Display_Communication_Static_CallText(	tTimerName, 
													tCharsString,
													PRIVATE_Get_Debug_String_For_Communication_Priority(g_savedGlobals.sCommsControlData.sSentTexts[index].sCommData.ePriority),
													theYPos)
		ENDREPEAT
	endif
	theYPos += FLOW_INFO_ADD_Y*2.5
	
	//Update the global value that dictates how far down the screen the background renders.
	IF theYPos > g_fFlowInfoBackgroundHeight
		g_fFlowInfoBackgroundHeight = theYPos
	ENDIF
	
ENDPROC


// ===========================================================================================================
//		Launch and Forget Script Tracking
// ===========================================================================================================

// PURPOSE:	Displays any Launch and Forget scripts on the Flow Info screen, displaying any if allowed.
// 
PROC DISPLAY_LAUNCH_AND_FORGET_SCRIPTS()

	IF NOT (g_showFlowLaunchAndForgetScripts)
		EXIT
	ENDIF
	
	FLOAT theYPos = FLOW_INFO_START_YPOS
	
	INT tempLoop = 0
	
	// Display the filenames of any Launch and Forget scripts
	// NOTE: This array is maintained in debug.sc
	REPEAT DEBUG_MAX_LAUNCH_AND_FORGET_SCRIPTS tempLoop
		IF NOT (g_debugLaunchAndForgetScripts[tempLoop].theThreadID = NULL)
			DISPLAY_FLOW_INFO_LITERAL_TEXT(COL_BRIGHT, FLOW_INFO_LAUNCH_AND_FORGET_XPOS, theYPos, g_debugLaunchAndForgetScripts[tempLoop].filename)
			theYPos += FLOW_INFO_ADD_Y
		ENDIF
	ENDREPEAT

ENDPROC



// ===========================================================================================================
//		Flow Help Text Queue Tracking
// ===========================================================================================================


// PURPOSE:	Displays the flow help text queue.
// 
PROC DISPLAY_HELP_TEXT_QUEUE()

	FLOAT theYPos = FLOW_INFO_HELP_QUEUE_YPOS + FLOW_INFO_ADD_Y
	
	//Diplay paused state.
	FLOW_INFO_COLOUR eItemColour = COL_BRIGHT
	IF g_bFlowHelpPaused
		DISPLAY_FLOW_INFO_LITERAL_TEXT(COL_BRIGHT, FLOW_INFO_HELP_QUEUE_ITEM_XPOS, theYPos, "*PAUSED*")
		eItemColour = COL_DULL
	ENDIF
	theYPos += FLOW_INFO_ADD_Y
	
	//Display each queued item.
	INT tempLoop = 0
	IF g_bLoadedClifford
		REPEAT g_savedGlobalsClifford.sFlowHelp.iFlowHelpCount tempLoop
			DISPLAY_FLOW_INFO_LITERAL_TEXT(eItemColour, FLOW_INFO_HELP_QUEUE_ITEM_XPOS, theYPos, g_savedGlobalsClifford.sFlowHelp.sHelpQueue[tempLoop].tHelpText)
			DISPLAY_FLOW_INFO_LITERAL_TEXT(eItemColour, FLOW_INFO_HELP_QUEUE_ITEM_XPOS, theYPos, g_savedGlobalsClifford.sFlowHelp.sHelpQueue[tempLoop].tHelpText)
			DISPLAY_FLOW_INFO_LITERAL_TEXT(eItemColour, FLOW_INFO_HELP_QUEUE_ITEM_XPOS, theYPos, g_savedGlobalsClifford.sFlowHelp.sHelpQueue[tempLoop].tHelpText)
			theYPos += FLOW_INFO_ADD_Y
		ENDREPEAT	
	ELIF g_bLoadedNorman
		REPEAT g_savedGlobalsnorman.sFlowHelp.iFlowHelpCount tempLoop
			DISPLAY_FLOW_INFO_LITERAL_TEXT(eItemColour, FLOW_INFO_HELP_QUEUE_ITEM_XPOS, theYPos, g_savedGlobalsnorman.sFlowHelp.sHelpQueue[tempLoop].tHelpText)
			DISPLAY_FLOW_INFO_LITERAL_TEXT(eItemColour, FLOW_INFO_HELP_QUEUE_ITEM_XPOS, theYPos, g_savedGlobalsnorman.sFlowHelp.sHelpQueue[tempLoop].tHelpText)
			DISPLAY_FLOW_INFO_LITERAL_TEXT(eItemColour, FLOW_INFO_HELP_QUEUE_ITEM_XPOS, theYPos, g_savedGlobalsnorman.sFlowHelp.sHelpQueue[tempLoop].tHelpText)
			theYPos += FLOW_INFO_ADD_Y
		ENDREPEAT		
	ELSE
		REPEAT g_savedGlobals.sFlowHelp.iFlowHelpCount tempLoop
			DISPLAY_FLOW_INFO_LITERAL_TEXT(eItemColour, FLOW_INFO_HELP_QUEUE_ITEM_XPOS, theYPos, g_savedGlobals.sFlowHelp.sHelpQueue[tempLoop].tHelpText)
			DISPLAY_FLOW_INFO_LITERAL_TEXT(eItemColour, FLOW_INFO_HELP_QUEUE_ITEM_XPOS, theYPos, g_savedGlobals.sFlowHelp.sHelpQueue[tempLoop].tHelpText)
			DISPLAY_FLOW_INFO_LITERAL_TEXT(eItemColour, FLOW_INFO_HELP_QUEUE_ITEM_XPOS, theYPos, g_savedGlobals.sFlowHelp.sHelpQueue[tempLoop].tHelpText)
			theYPos += FLOW_INFO_ADD_Y
		ENDREPEAT
	ENDIF
ENDPROC


FUNC BOOL DISPLAY_STRAND_CURRENT_COMMAND_DESCRIPTION_CUSTOM(FLOW_COMMAND_IDS thisCommand, INT thisCommandPos, FLOAT paramYPos, FLOW_INFO_COLOUR &lineColour)
	
	
	SWITCH (thisCommand)
	
		CASE FLOW_DO_MISSION_AT_SWITCH
			DISPLAY_STRAND_DO_MISSION(thisCommandPos, paramYPos, lineColour)
			RETURN TRUE
			BREAK
			
		CASE FLOW_DO_BOARD_INTERACTION
			DISPLAY_STRAND_DO_BOARD_INTERACTION(thisCommandPos, paramYPos, lineColour)
			RETURN TRUE
			BREAK
				
		CASE FLOW_PLAYER_PHONE_NPC
		CASE FLOW_PHONE_PLAYER
		CASE FLOW_TEXT_PLAYER
		CASE FLOW_EMAIL_PLAYER
			DISPLAY_STRAND_COMMS_COMMAND(thisCommandPos, paramYPos, lineColour)
			RETURN TRUE
			BREAK
			
		CASE FLOW_PHONE_PLAYER_YES_NO
		CASE FLOW_PHONE_PLAYER_WITH_JUMPS
		CASE FLOW_PLAYER_PHONE_NPC_WITH_BRANCH
		CASE FLOW_PHONE_PLAYER_WITH_BRANCH
			DISPLAY_STRAND_COMMS_LARGE_COMMAND(thisCommandPos, paramYPos, lineColour)
			RETURN TRUE
			BREAK
			
		// Basic display commands (these have no additional display requirements or are only on-screen for one frame)
		CASE FLOW_ADD_ORGANISER_EVENT
		CASE FLOW_CANCEL_PHONECALL
		CASE FLOW_CANCEL_TEXT
		CASE FLOW_IS_MISSION_COMPLETED
		CASE FLOW_IS_PLAYING_AS_CHARACTER
		CASE FLOW_IS_PLAYER_PHONING_NPC
		CASE FLOW_IS_SWITCHING_TO_CHARACTER
		CASE FLOW_SET_BOARD_DISPLAY_GROUP_VISIBILITY
		CASE FLOW_SET_DOOR_STATE
		CASE FLOW_RESET_PRIORITY_SWITCH_SCENES
		CASE FLOW_SET_PED_REQUEST_SCENE
		CASE FLOW_SET_CHAR_CHAT_PHONECALL
		CASE FLOW_SET_CHAR_HOTSWAP_AVAILABILITY
		CASE FLOW_SET_HOTSWAP_STAGE_FLAG
		CASE FLOW_SET_PLAYER_CHARACTER
		CASE FLOW_SET_SAVEHOUSE_STATE
		CASE FLOW_SET_SHOP_STATE
		CASE FLOW_SET_WEAPON_COMP_LOCK_STATE
		CASE FLOW_SET_VEHICLE_GEN_STATE
		CASE FLOW_SET_SPECIAL_ABILITY_UNLOCK_STATE
		CASE FLOW_SET_MISSION_PASSED
		CASE FLOW_TOGGLE_BOARD_VIEWING
		CASE FLOW_ADD_PHONE_CONTACT
		CASE FLOW_REMOVE_PHONE_CONTACT
		CASE FLOW_ADD_NEW_FRIEND_CONTACT
		CASE FLOW_BLOCK_FRIEND_CONTACT
		CASE FLOW_BLOCK_FRIEND_CLASH
		CASE FLOW_FRIEND_CONTACT_BUSY
		CASE FLOW_IS_PLAYER_IN_AREA
		CASE FLOW_DO_PLAYER_BANK_ACTION
		CASE FLOW_IS_SHRINK_SESSION_AVAILABLE
		CASE FLOW_SET_BIKES_SUPRESSED_STATE
		CASE FLOW_SET_FRANKLIN_OUTFIT_BITSET_STATE
		CASE FLOW_FAST_TRACK_STRAND
		CASE FLOW_DO_CUSTOM_CHECK_JUMPS
		CASE FLOW_SET_BUYABLE_VEHICLE_STATE
		CASE FLOW_SET_COMPONENT_ACQUIRED
		CASE FLOW_DO_SWITCH_TO_CHARACTER
		CASE FLOW_SET_STRAND_SAVE_OVERRIDE
		CASE FLOW_CLEAR_STRAND_SAVE_OVERRIDE
			DISPLAY_FLOW_INFO_LITERAL_TEXT(lineColour, FLOW_INFO_DESCRIPTION_XPOS, paramYPos, GET_COMMAND_DISPLAY_STRING_FROM_COMMAND_ID(thisCommand))
			RETURN TRUE
		BREAK
			
		DEFAULT
			RETURN FALSE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC DISPLAY_TITLES_CUSTOM()
	DISPLAY_FLOW_INFO_LITERAL_TEXT(COL_MID, FLOW_INFO_TITLE_COMM_XPOS, 0.04, "COMMUNICATION CONTROL")
	DISPLAY_FLOW_INFO_LITERAL_TEXT(COL_MID, FLOW_INFO_TITLE_LAUNCH_XPOS, 0.04, "LAUNCHED SCRIPTS")
	DISPLAY_FLOW_INFO_LITERAL_TEXT(COL_MID, FLOW_INFO_HELP_QUEUE_TITLE_XPOS, FLOW_INFO_HELP_QUEUE_YPOS, "HELP TEXT QUEUE")
ENDPROC

PROC DISPLAY_FLOW_INFO_CUSTOM()
	DISPLAY_COMMUNICATION_CONTROLLER()
	DISPLAY_LAUNCH_AND_FORGET_SCRIPTS()
	DISPLAY_HELP_TEXT_QUEUE()
ENDPROC


