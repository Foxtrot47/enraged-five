USING "rage_builtins.sch"
USING "globals.sch"
USING "flow_structs_core.sch"
USING "flow_diagram_utils_core.sch"
USING "flow_mission_data_public.sch"
#if USE_CLF_DLC
FUNC STRING Get_Strand_XML_FilenameCLF(STRANDS eStrand)

	SWITCH (eStrand)
		CASE STRAND_CLF
			RETURN ""
		BREAK
		
		CASE STRAND_CLF_ARA
			RETURN ""
		BREAK
		
		CASE STRAND_CLF_ASS_A14
			RETURN ""
		BREAK
		
		CASE STRAND_CLF_ASS_PAP
			RETURN ""
		BREAK
		
		CASE STRAND_CLF_CAS1
			RETURN ""
		BREAK
		
		CASE STRAND_CLF_CAS2
			RETURN ""
		BREAK
		
		CASE STRAND_CLF_IAA
			RETURN ""
		BREAK
		
		CASE STRAND_CLF_JET
			RETURN ""
		BREAK
		
		CASE STRAND_CLF_KOR
			RETURN ""
		BREAK
		
		CASE STRAND_CLF_MIL
			RETURN ""
		BREAK
		
		CASE STRAND_CLF_MIL2
			RETURN ""
		BREAK
		
		CASE STRAND_CLF_MIL3
			RETURN ""
		BREAK
		
		CASE STRAND_CLF_RUS
			RETURN ""
		BREAK
		
		CASE STRAND_CLF_RC_ALEX
			RETURN ""
		BREAK
			
		CASE STRAND_CLF_RC_MELODY
			RETURN ""
		BREAK
			
		CASE STRAND_CLF_RC_AGNES
			RETURN ""
		BREAK
			
		CASE STRAND_CLF_RC_CLAIRE
			RETURN ""
		BREAK
	ENDSWITCH
	
	SCRIPT_ASSERT("Get_Strand_XML_FilenameAGT: Could not find an XML filename for the specified strand.")
	RETURN "INVALID STRAND"
ENDFUNC
#endif
#if USE_NRM_DLC
FUNC STRING Get_Strand_XML_FilenameNRM(STRANDS eStrand)

	SWITCH (eStrand)
	
		CASE STRAND_NRM_SURVIVE
			RETURN ""
		BREAK
		
		CASE STRAND_NRM_RESCUE_ENG
			RETURN ""
		BREAK
		CASE STRAND_NRM_RESCUE_MED
			RETURN ""
		BREAK
		CASE STRAND_NRM_RESCUE_GUN
			RETURN ""
		BREAK
		
		CASE STRAND_NRM_SUPPLY_FUEL
			RETURN ""
		BREAK
		CASE STRAND_NRM_SUPPLY_AMMO
			RETURN ""
		BREAK
		CASE STRAND_NRM_SUPPLY_MED
			RETURN ""
		BREAK
		CASE STRAND_NRM_SUPPLY_FOOD
			RETURN ""
		BREAK
		
		CASE STRAND_NRM_RADIO
			RETURN ""
		BREAK	
	ENDSWITCH
	
	SCRIPT_ASSERT("Get_Strand_XML_FilenameNRM: Could not find an XML filename for the specified strand.")
	RETURN "INVALID STRAND"
ENDFUNC
#endif

FUNC STRING Get_Strand_XML_Filename(STRANDS eStrand)
#if USE_CLF_DLC
	return Get_Strand_XML_FilenameCLF(eStrand)
#endif
#if USE_NRM_DLC
	return Get_Strand_XML_FilenameNRM(eStrand)
#endif
#if not USE_CLF_DLC
#if not USE_NRM_DLC
	SWITCH (eStrand)
		CASE STRAND_AGENCY_HEIST
			RETURN "X:/gta5/titleupdate/dev_ng/common/data/script/xml/SPDebug/SP_agency_heist.xml"
		BREAK
		
		CASE STRAND_ARMENIAN
			RETURN "X:/gta5/titleupdate/dev_ng/common/data/script/xml/SPDebug/SP_armenian_strand.xml"
		BREAK
		
		CASE STRAND_ASSASSINATIONS
			RETURN "X:/gta5/titleupdate/dev_ng/common/data/script/xml/SPDebug/SP_assassinations_strand.xml"
		BREAK
		
		CASE STRAND_CAR_STEAL
			RETURN "X:/gta5/titleupdate/dev_ng/common/data/script/xml/SPDebug/SP_car_steal_strand.xml"
		BREAK
		
		CASE STRAND_CHINESE
			RETURN "X:/gta5/titleupdate/dev_ng/common/data/script/xml/SPDebug/SP_chinese_strand.xml"
		BREAK
		
		CASE STRAND_DOCKS_HEIST
		CASE STRAND_DOCKS_HEIST_2
			RETURN "X:/gta5/titleupdate/dev_ng/common/data/script/xml/SPDebug/SP_dock_heist.xml"
		BREAK
		
		CASE STRAND_EXILE
			RETURN "X:/gta5/titleupdate/dev_ng/common/data/script/xml/SPDebug/SP_exile_strand.xml"
		BREAK
		
		CASE STRAND_FAMILY
			RETURN "X:/gta5/titleupdate/dev_ng/common/data/script/xml/SPDebug/SP_family_strand.xml"
		BREAK
		
		CASE STRAND_FBI_OFFICERS
		CASE STRAND_FBI_OFFICERS_2
		CASE STRAND_FBI_OFFICERS_3
		CASE STRAND_FBI_OFFICERS_4
		CASE STRAND_FBI_OFFICERS_5
			RETURN "X:/gta5/titleupdate/dev_ng/common/data/script/xml/SPDebug/SP_fbi_strand.xml"
		BREAK
		
		CASE STRAND_FINALE
			RETURN "X:/gta5/titleupdate/dev_ng/common/data/script/xml/SPDebug/SP_finale_strand.xml"
		BREAK
				
		CASE STRAND_FINALE_HEIST
		CASE STRAND_FINALE_HEIST_2
		CASE STRAND_FINALE_HEIST_3
		CASE STRAND_FINALE_HEIST_4
			RETURN "X:/gta5/titleupdate/dev_ng/common/data/script/xml/SPDebug/SP_finale_heist.xml"
		BREAK
		
		CASE STRAND_FRANKLIN
			RETURN "X:/gta5/titleupdate/dev_ng/common/data/script/xml/SPDebug/SP_franklin_strand.xml"
		BREAK
		
		CASE STRAND_JEWEL_HEIST
		CASE STRAND_JEWEL_HEIST_2
			RETURN "X:/gta5/titleupdate/dev_ng/common/data/script/xml/SPDebug/SP_jewl_heist.xml"
		BREAK
		
		CASE STRAND_LAMAR
			RETURN "X:/gta5/titleupdate/dev_ng/common/data/script/xml/SPDebug/SP_lamar_strand.xml"
		BREAK
		
		CASE STRAND_LESTER
			RETURN "X:/gta5/titleupdate/dev_ng/common/data/script/xml/SPDebug/SP_lester_strand.xml"
		BREAK
		
		CASE STRAND_MARTIN
			RETURN "X:/gta5/titleupdate/dev_ng/common/data/script/xml/SPDebug/SP_martin_strand.xml"
		BREAK
		
		CASE STRAND_MICHAEL
			RETURN "X:/gta5/titleupdate/dev_ng/common/data/script/xml/SPDebug/SP_michael_strand.xml"
		BREAK
		
		CASE STRAND_MICHAEL_EVENTS
			RETURN "X:/gta5/titleupdate/dev_ng/common/data/script/xml/SPDebug/SP_michael_events.xml"
		BREAK
		
		CASE STRAND_PROLOGUE
			RETURN "X:/gta5/titleupdate/dev_ng/common/data/script/xml/SPDebug/SP_prologue_strand.xml"
		BREAK
		
		CASE STRAND_RURAL_BANK_HEIST
			RETURN "X:/gta5/titleupdate/dev_ng/common/data/script/xml/SPDebug/SP_rurl_heist.xml"
		BREAK
		
		CASE STRAND_SHRINK
			RETURN "X:/gta5/titleupdate/dev_ng/common/data/script/xml/SPDebug/SP_Shrink.xml"
		BREAK
		
		CASE STRAND_SOLOMON
			RETURN "X:/gta5/titleupdate/dev_ng/common/data/script/xml/SPDebug/SP_solomon_strand.xml"
		BREAK
		
		CASE STRAND_TREVOR
			RETURN "X:/gta5/titleupdate/dev_ng/common/data/script/xml/SPDebug/SP_trevor_strand.xml"
		BREAK
	ENDSWITCH
	
	SCRIPT_ASSERT("Get_Strand_XML_Filename: Could not find an XML filename for the specified strand.")
	RETURN "INVALID STRAND"
#endif
#endif
ENDFUNC


FUNC BOOL FLOW_DIAGRAM_IS_MISSION_COMPLETE(TEXT_LABEL_63 missionName)
	RETURN Get_Mission_Complete_State(GET_SP_MISSION_ID_FOR_SCRIPT_NAME(Remove_SC_From_Mission_Filename(missionName)))
ENDFUNC


PROC Activate_Flow_Diagram_Custom(FLOW_DIAGRAM &sFlowDiagram)
	DISABLE_CELLPHONE(TRUE)
	sFlowDiagram.iElementCount = sFlowDiagram.iElementCount
ENDPROC


PROC Deactivate_Flow_Diagram_Custom(FLOW_DIAGRAM &sFlowDiagram)
	DISABLE_CELLPHONE(FALSE)
	sFlowDiagram.iElementCount = sFlowDiagram.iElementCount
ENDPROC

#if USE_CLF_DLC
PROC FLOW_DIAGRAM_INITIALIZE_STRANDSCLF(FLOW_DIAGRAM &sFlowDiagram, BOOL &bActiveStrands[], INT iCurrentXPos)
	//The Prologue strand is always active at the start of the game so flag it as such.
	bActiveStrands[STRAND_CLF] = TRUE
	sFlowDiagram.sStrandBounds[STRAND_CLF].startPos = iCurrentXPos
	CDEBUG1LN(DEBUG_FLOW_DIAGRAM_POSITION, "Setting STRAND_CLF strand to active.")
ENDPROC
#endif
#if USE_NRM_DLC
PROC FLOW_DIAGRAM_INITIALIZE_STRANDSNRM(FLOW_DIAGRAM &sFlowDiagram, BOOL &bActiveStrands[], INT iCurrentXPos)
	//The Prologue strand is always active at the start of the game so flag it as such.
	bActiveStrands[STRAND_NRM_SURVIVE] = TRUE
	sFlowDiagram.sStrandBounds[STRAND_NRM_SURVIVE].startPos = iCurrentXPos
	CDEBUG1LN(DEBUG_FLOW_DIAGRAM_POSITION, "Setting STRAND_NRM_SURVIVE strand to active.")
ENDPROC
#endif
PROC FLOW_DIAGRAM_INITIALIZE_STRANDS(FLOW_DIAGRAM &sFlowDiagram, BOOL &bActiveStrands[], INT iCurrentXPos)
	#if USE_CLF_DLC
		FLOW_DIAGRAM_INITIALIZE_STRANDSCLF(sFlowDiagram,bActiveStrands,iCurrentXPos)
	#endif
	#if USE_NRM_DLC
		FLOW_DIAGRAM_INITIALIZE_STRANDSNRM(sFlowDiagram,bActiveStrands,iCurrentXPos)
	#endif
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
	//The Prologue strand is always active at the start of the game so flag it as such.
	bActiveStrands[STRAND_PROLOGUE] = TRUE
	sFlowDiagram.sStrandBounds[STRAND_PROLOGUE].startPos = iCurrentXPos
	CDEBUG1LN(DEBUG_FLOW_DIAGRAM_POSITION, "Setting PROLOGUE strand to active.")
	#endif
	#endif
ENDPROC


PROC Flow_Diagram_Launch_Mission(FLOW_DIAGRAM &sFlowDiagram, FLOW_LAUNCHER_VARS &flowLauncher)
			
			//Switch to the correct character for this mission.
			INT iMissionDataIndex = ENUM_TO_INT(GET_SP_MISSION_ID_FOR_SCRIPT_NAME(REMOVE_SC_FROM_MISSION_FILENAME(sFlowDiagram.sElement[sFlowDiagram.iHighlightedMission].mainData)))
			INT iTriggerableCharBitset = g_sMissionStaticData[iMissionDataIndex].triggerCharBitset
			enumCharacterList eCurrentPlayer = GET_CURRENT_PLAYER_PED_ENUM()
			
			IF NOT Is_Mission_Triggerable_By_Character(g_sMissionStaticData[iMissionDataIndex].triggerCharBitset, eCurrentPlayer)
				//We need to swap character.
				IF Is_Mission_Triggerable_By_Character(iTriggerableCharBitset, CHAR_MICHAEL)
					WHILE NOT SET_CURRENT_SELECTOR_PED(SELECTOR_PED_MICHAEL)
						WAIT(0)
					ENDWHILE
				ELIF Is_Mission_Triggerable_By_Character(iTriggerableCharBitset, CHAR_FRANKLIN)
					WHILE NOT SET_CURRENT_SELECTOR_PED(SELECTOR_PED_FRANKLIN)
						WAIT(0)
					ENDWHILE
				ELIF Is_Mission_Triggerable_By_Character(iTriggerableCharBitset, CHAR_TREVOR)
					WHILE NOT SET_CURRENT_SELECTOR_PED(SELECTOR_PED_TREVOR)
						WAIT(0)
					ENDWHILE
				ELSE
					SCRIPT_ASSERT("Manage_Flow_Diagram_Input: Couldn't find an appropriate character to swap the player to when launching mission")
				ENDIF
			ENDIF
	
			LAUNCHER_LAUNCH_TO_MISSION_SCRIPT_HASH(flowLauncher, GET_HASH_KEY(sFlowDiagram.sElement[sFlowDiagram.iHighlightedMission].mainData), TRUE, FALSE)

ENDPROC


PROC Load_Flow_Diagram_Data_From_XML_Custom(FLOW_DIAGRAM &sFlowDiagram, INT iAttributeIndex)
	STRING strChar
	TEXT_LABEL_23 tCharA = ""
	TEXT_LABEL_23 tCharB = ""
	TEXT_LABEL_23 tCharC = ""
	INT iType
	
	SWITCH INT_TO_ENUM(FLOW_DIAGRAM_ATTRIBUTE_CUSTOM, GET_HASH_KEY(GET_XML_NODE_ATTRIBUTE_NAME(iAttributeIndex)))
		CASE FD_ATTR_HEIST
			sFlowDiagram.sElement[sFlowDiagram.iElementCount].mainData = GET_STRING_FROM_XML_NODE_ATTRIBUTE(iAttributeIndex)
			
			CDEBUG1LN(DEBUG_FLOW_DIAGRAM_LOAD, "heist: ", sFlowDiagram.sElement[sFlowDiagram.iElementCount].mainData)
		BREAK
		CASE FD_ATTR_CHAR
		CASE FD_ATTR_CHARTO
			strChar = GET_STRING_FROM_XML_NODE_ATTRIBUTE(iAttributeIndex)
			
			CDEBUG1LN(DEBUG_FLOW_DIAGRAM_LOAD, "char/charTo: ",strChar)
			
			IF Does_Character_String_Contain_Multiple_Characters(strChar)
				CDEBUG1LN(DEBUG_FLOW_DIAGRAM_LOAD, "Found multiple characters...")
			
				Get_Multiple_Characters_From_Character_String(strChar, tCharA, tCharB, tCharC)
				IF NOT ARE_STRINGS_EQUAL(tCharA, "")
					sFlowDiagram.sElementCustom[sFlowDiagram.iElementCount].characterA = Get_CharSheet_ID_From_Display_String(tCharA)
				ELSE
					sFlowDiagram.sElementCustom[sFlowDiagram.iElementCount].characterA = CHAR_BLANK_ENTRY
				ENDIF
				IF NOT ARE_STRINGS_EQUAL(tCharB, "")
					sFlowDiagram.sElementCustom[sFlowDiagram.iElementCount].characterB = Get_CharSheet_ID_From_Display_String(tCharB)
				ELSE
					sFlowDiagram.sElementCustom[sFlowDiagram.iElementCount].characterB = CHAR_BLANK_ENTRY
				ENDIF
				IF NOT ARE_STRINGS_EQUAL(tCharC, "")
					sFlowDiagram.sElementCustom[sFlowDiagram.iElementCount].characterC = Get_CharSheet_ID_From_Display_String(tCharC)
				ELSE
					sFlowDiagram.sElementCustom[sFlowDiagram.iElementCount].characterC = CHAR_BLANK_ENTRY
				ENDIF
			ELSE
				sFlowDiagram.sElementCustom[sFlowDiagram.iElementCount].characterA = Get_CharSheet_ID_From_Display_String(strChar)
			ENDIF
			CDEBUG1LN(DEBUG_FLOW_DIAGRAM_LOAD, "charA: ", Get_CharSheet_Display_String_From_CharSheet(sFlowDiagram.sElementCustom[sFlowDiagram.iElementCount].characterA))
			CDEBUG1LN(DEBUG_FLOW_DIAGRAM_LOAD, "charB: ", Get_CharSheet_Display_String_From_CharSheet(sFlowDiagram.sElementCustom[sFlowDiagram.iElementCount].characterB))
			CDEBUG1LN(DEBUG_FLOW_DIAGRAM_LOAD, "charC: ", Get_CharSheet_Display_String_From_CharSheet(sFlowDiagram.sElementCustom[sFlowDiagram.iElementCount].characterC))
		BREAK	
		CASE FD_ATTR_CHARFROM
			strChar = GET_STRING_FROM_XML_NODE_ATTRIBUTE(iAttributeIndex)
			sFlowDiagram.sElementCustom[sFlowDiagram.iElementCount].characterB = Get_CharSheet_ID_From_Display_String(strChar)
			
			CDEBUG1LN(DEBUG_FLOW_DIAGRAM_LOAD, "charFrom: ", strChar)
		BREAK
		CASE FD_ATTR_TYPE
			iType = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttributeIndex)
			//Fudge. Store the type in the character enum.
			sFlowDiagram.sElementCustom[sFlowDiagram.iElementCount].characterA = INT_TO_ENUM(enumCharacterList, iType)
		
			CDEBUG1LN(DEBUG_FLOW_DIAGRAM_LOAD, "type: ", iType)
		BREAK
	ENDSWITCH
ENDPROC


PROC Display_Flow_Diagram_Mission_Element_Custom(FLOW_DIAGRAM &sFlowDiagram, FLOW_DIAGRAM_ELEMENT &sMissionElement, FLOW_DIAGRAM_ELEMENT_CUSTOM &sMissionElementCustom, FLOAT fXCoord, FLOAT fYCoord)
	Setup_Flow_Diagram_Element_Large_Text(sFlowDiagram)
	DISPLAY_TEXT_WITH_LITERAL_STRING(	fXCoord - (GET_STRING_WIDTH_WITH_STRING("STRING",sMissionElement.mainData)*0.5), 
										fYCoord - (0.009*sFlowDiagram.fZoom),
										"STRING",
										sMissionElement.mainData)
									
	Setup_Flow_Diagram_Element_Large_Text(sFlowDiagram)
	STRING strCharacter = Get_CharSheet_Display_String_From_CharSheet(sMissionElementCustom.characterA)
	DISPLAY_TEXT_WITH_LITERAL_STRING(	fXCoord - (GET_STRING_WIDTH_WITH_STRING("STRING",strCharacter)*0.5), 
										fYCoord - (0.0005*sFlowDiagram.fZoom),
										"STRING",
										strCharacter)
	IF sMissionElementCustom.characterB <> CHAR_BLANK_ENTRY
		Setup_Flow_Diagram_Element_Large_Text(sFlowDiagram)
		strCharacter = Get_CharSheet_Display_String_From_CharSheet(sMissionElementCustom.characterB)
		DISPLAY_TEXT_WITH_LITERAL_STRING(	fXCoord - (GET_STRING_WIDTH_WITH_STRING("STRING",strCharacter)*0.5), 
											fYCoord - (-0.0030*sFlowDiagram.fZoom),
											"STRING",
											strCharacter)
	ENDIF
	IF sMissionElementCustom.characterC <> CHAR_BLANK_ENTRY
		Setup_Flow_Diagram_Element_Large_Text(sFlowDiagram)
		strCharacter = Get_CharSheet_Display_String_From_CharSheet(sMissionElementCustom.characterC)
		DISPLAY_TEXT_WITH_LITERAL_STRING(	fXCoord - (GET_STRING_WIDTH_WITH_STRING("STRING",strCharacter)*0.5), 
											fYCoord - (-0.0065*sFlowDiagram.fZoom),
											"STRING",
											strCharacter)
	ENDIF

ENDPROC


PROC Display_Flow_Diagram_Planning_Element(FLOW_DIAGRAM &sFlowDiagram, FLOW_DIAGRAM_ELEMENT &sPlanningElement, FLOW_DIAGRAM_ELEMENT_CUSTOM &sPlanningElementCustom,FLOAT fXCoord)

	//Convert y grid coord to screen coord.
	FLOAT fYCoord = Get_Screen_Y_Coord(sFlowDiagram, sPlanningElement.yPosA)

	//Cull on x axis.
	FLOAT fHalfScaled = FD_PLANNING_ELEMENT_WIDTH * 0.5 * sFlowDiagram.fZoom
	IF 	(fXCoord + fHalfScaled) >= 0.0 
		IF (fXCoord - fHalfScaled) <= 1.0
		
			//Cull on y axis.
			fHalfScaled = FD_PLANNING_ELEMENT_HEIGHT * 0.5 * sFlowDiagram.fZoom
			IF (fYCoord + fHalfScaled) >= 0.0 
				IF (fYCoord - fHalfScaled) <= 1.0
		
					DRAW_RECT(	fXCoord, 
								fYCoord,
								FD_PLANNING_ELEMENT_WIDTH * sFlowDiagram.fZoom, 
								FD_PLANNING_ELEMENT_HEIGHT * sFlowDiagram.fZoom, 
								FD_COL_R_PLANNING, FD_COL_G_PLANNING, FD_COL_B_PLANNING, 255)
								
					IF sFlowDiagram.fZoom > 1.5
						Setup_Flow_Diagram_Element_Large_Text(sFlowDiagram)
						DISPLAY_TEXT_WITH_LITERAL_STRING(	fXCoord - (GET_STRING_WIDTH_WITH_STRING("STRING","Planning Board")*0.5), 
															fYCoord - (0.005*sFlowDiagram.fZoom),
															"STRING",
															"Planning Board")
																							
						Setup_Flow_Diagram_Element_Small_Text(sFlowDiagram)
						TEXT_LABEL_31 tInteraction
						SWITCH (INT_TO_ENUM(g_eBoardInteractions, ENUM_TO_INT(sPlanningElementCustom.characterA)))
							CASE INTERACT_CREW
								tInteraction = "Crew Select"
							BREAK
							CASE INTERACT_GAMEPLAY_CHOICE
								tInteraction = "Gameplay Choice"
							BREAK
						ENDSWITCH
						DISPLAY_TEXT_WITH_LITERAL_STRING(	fXCoord - (GET_STRING_WIDTH_WITH_STRING("STRING",tInteraction)*0.5), 
															fYCoord - (-0.001*sFlowDiagram.fZoom),
															"STRING",
															tInteraction)
															
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC


PROC Display_Flow_Diagram_Phone_Element(FLOW_DIAGRAM &sFlowDiagram, FLOW_DIAGRAM_ELEMENT &sPhoneElement, FLOW_DIAGRAM_ELEMENT_CUSTOM &sPhoneElementCustom,FLOAT fXCoord)

	//Convert y grid coord to screen coord.
	FLOAT fYCoord = Get_Screen_Y_Coord(sFlowDiagram, sPhoneElement.yPosA)

	//Cull on x axis.
	FLOAT fHalfScaled = FD_PHONE_ELEMENT_WIDTH * 0.5 * sFlowDiagram.fZoom
	IF 	(fXCoord + fHalfScaled) >= 0.0 
		IF (fXCoord - fHalfScaled) <= 1.0
		
			//Cull on y axis.
			fHalfScaled = FD_PHONE_ELEMENT_HEIGHT * 0.5 * sFlowDiagram.fZoom
			IF (fYCoord + fHalfScaled) >= 0.0 
				IF (fYCoord - fHalfScaled) <= 1.0
		
					DRAW_RECT(	fXCoord, 
								fYCoord,
								FD_PHONE_ELEMENT_WIDTH * sFlowDiagram.fZoom, 
								FD_PHONE_ELEMENT_HEIGHT * sFlowDiagram.fZoom, 
								FD_COL_R_PHONE, FD_COL_G_PHONE, FD_COL_B_PHONE, 255)
								
					IF sFlowDiagram.fZoom > 1.5
						Setup_Flow_Diagram_Element_Large_Text(sFlowDiagram)
						DISPLAY_TEXT_WITH_LITERAL_STRING(	fXCoord - (GET_STRING_WIDTH_WITH_STRING("STRING","Call")*0.5), 
															fYCoord - (0.009*sFlowDiagram.fZoom),
															"STRING",
															"Call")
															
						Setup_Flow_Diagram_Element_Small_Text(sFlowDiagram)
						TEXT_LABEL_31 tCharacterFrom = "From "
						tCharacterFrom += Get_CharSheet_Display_String_From_CharSheet(sPhoneElementCustom.characterB)
						DISPLAY_TEXT_WITH_LITERAL_STRING(	fXCoord - (GET_STRING_WIDTH_WITH_STRING("STRING",tCharacterFrom)*0.5), 
															fYCoord - (0.003*sFlowDiagram.fZoom),
															"STRING",
															tCharacterFrom)
															
						Setup_Flow_Diagram_Element_Small_Text(sFlowDiagram)
						TEXT_LABEL_31 tCharacterTo = "To "
						tCharacterTo += Get_CharSheet_Display_String_From_CharSheet(sPhoneElementCustom.characterA)
						DISPLAY_TEXT_WITH_LITERAL_STRING(	fXCoord - (GET_STRING_WIDTH_WITH_STRING("STRING",tCharacterTo)*0.5), 
															fYCoord - (-0.001*sFlowDiagram.fZoom),
															"STRING",
															tCharacterTo)
															
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC


PROC Display_Flow_Diagram_Phone_YesNo_Element(FLOW_DIAGRAM &sFlowDiagram, FLOW_DIAGRAM_ELEMENT &sPhoneElement, FLOW_DIAGRAM_ELEMENT_CUSTOM &sPhoneElementCustom, FLOAT fXCoord)
	
	//Convert y grid coord to screen coord.
	FLOAT fYCoord = Get_Screen_Y_Coord(sFlowDiagram, sPhoneElement.yPosA)
	
	//Cull on x axis.
	FLOAT fHalfScaled = FD_PHONE_ELEMENT_WIDTH * 0.5 * sFlowDiagram.fZoom
	IF 	(fXCoord + fHalfScaled) >= 0.0 
		IF (fXCoord - fHalfScaled) <= 1.0
		
			//Cull on y axis.
			fHalfScaled = FD_PHONE_ELEMENT_HEIGHT * 0.5 * sFlowDiagram.fZoom
			IF (fYCoord + fHalfScaled) >= 0.0 
				IF (fYCoord - fHalfScaled) <= 1.0
				
					DRAW_RECT(	fXCoord, 
								fYCoord,
								FD_PHONE_ELEMENT_WIDTH * sFlowDiagram.fZoom, 
								FD_PHONE_ELEMENT_HEIGHT * sFlowDiagram.fZoom, 
								FD_COL_R_PHONE, FD_COL_G_PHONE, FD_COL_B_PHONE, 255)
								
					IF sFlowDiagram.fZoom > 1.5
						Setup_Flow_Diagram_Element_Large_Text(sFlowDiagram)
						DISPLAY_TEXT_WITH_LITERAL_STRING(	fXCoord - (GET_STRING_WIDTH_WITH_STRING("STRING","Call Y/N")*0.5), 
															fYCoord - (0.009*sFlowDiagram.fZoom),
															"STRING",
															"Call Y/N")
															
						Setup_Flow_Diagram_Element_Small_Text(sFlowDiagram)
						TEXT_LABEL_31 tCharacterFrom = "From "
						tCharacterFrom += Get_CharSheet_Display_String_From_CharSheet(sPhoneElementCustom.characterB)
						DISPLAY_TEXT_WITH_LITERAL_STRING(	fXCoord - (GET_STRING_WIDTH_WITH_STRING("STRING",tCharacterFrom)*0.5), 
															fYCoord - (0.003*sFlowDiagram.fZoom),
															"STRING",
															tCharacterFrom)
															
						Setup_Flow_Diagram_Element_Small_Text(sFlowDiagram)
						TEXT_LABEL_31 tCharacterTo = "To "
						tCharacterTo += Get_CharSheet_Display_String_From_CharSheet(sPhoneElementCustom.characterA)
						DISPLAY_TEXT_WITH_LITERAL_STRING(	fXCoord - (GET_STRING_WIDTH_WITH_STRING("STRING",tCharacterTo)*0.5), 
															fYCoord - (-0.001*sFlowDiagram.fZoom),
															"STRING",
															tCharacterTo)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC


PROC Display_Flow_Diagram_Text_Element(FLOW_DIAGRAM &sFlowDiagram, FLOW_DIAGRAM_ELEMENT &sTextElement, FLOW_DIAGRAM_ELEMENT_CUSTOM &sTextElementCustom, FLOAT fXCoord)

	//Convert y grid coord to screen coord.
	FLOAT fYCoord = Get_Screen_Y_Coord(sFlowDiagram, sTextElement.yPosA)

	//Cull on x axis.
	FLOAT fHalfScaled = FD_PHONE_ELEMENT_WIDTH * 0.5 * sFlowDiagram.fZoom
	IF 	(fXCoord + fHalfScaled) >= 0.0 
		IF (fXCoord - fHalfScaled) <= 1.0
		
			//Cull on y axis.
			fHalfScaled = FD_PHONE_ELEMENT_HEIGHT * 0.5 * sFlowDiagram.fZoom
			IF (fYCoord + fHalfScaled) >= 0.0 
				IF (fYCoord - fHalfScaled) <= 1.0
				
					DRAW_RECT(	fXCoord, 
								fYCoord, 
								FD_PHONE_ELEMENT_WIDTH * sFlowDiagram.fZoom, 
								FD_PHONE_ELEMENT_HEIGHT * sFlowDiagram.fZoom, 
								FD_COL_R_PHONE, FD_COL_G_PHONE, FD_COL_B_PHONE, 255)
								
					IF sFlowDiagram.fZoom > 1.5
						Setup_Flow_Diagram_Element_Large_Text(sFlowDiagram)
						DISPLAY_TEXT_WITH_LITERAL_STRING(	fXCoord - (GET_STRING_WIDTH_WITH_STRING("STRING","Text")*0.5), 
															fYCoord - (0.009*sFlowDiagram.fZoom),
															"STRING",
															"Text")
															
						Setup_Flow_Diagram_Element_Small_Text(sFlowDiagram)
						TEXT_LABEL_31 tCharacterFrom = "From "
						tCharacterFrom += Get_CharSheet_Display_String_From_CharSheet(sTextElementCustom.characterB)
						DISPLAY_TEXT_WITH_LITERAL_STRING(	fXCoord - (GET_STRING_WIDTH_WITH_STRING("STRING",tCharacterFrom)*0.5), 
															fYCoord - (0.003*sFlowDiagram.fZoom),
															"STRING",
															tCharacterFrom)
															
						Setup_Flow_Diagram_Element_Small_Text(sFlowDiagram)
						TEXT_LABEL_31 tCharacterTo = "To "
						tCharacterTo += Get_CharSheet_Display_String_From_CharSheet(sTextElementCustom.characterA)
						DISPLAY_TEXT_WITH_LITERAL_STRING(	fXCoord - (GET_STRING_WIDTH_WITH_STRING("STRING",tCharacterTo)*0.5), 
															fYCoord - (-0.001*sFlowDiagram.fZoom),
															"STRING",
															tCharacterTo)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL STORE_FLOW_DIAGRAM_CUSTOM_ELEMENT(FLOW_DIAGRAM &sFlowDiagram)
	SWITCH INT_TO_ENUM(FLOW_DIAGRAM_ELEMENT_TYPE, GET_HASH_KEY(GET_XML_NODE_NAME()))
		CASE FD_ELEMENT_TYPE_PLANNING	
		CASE FD_ELEMENT_TYPE_PHONE		
		CASE FD_ELEMENT_TYPE_PHONE_YESNO
		CASE FD_ELEMENT_TYPE_TEXT		
			CDEBUG1LN(DEBUG_FLOW_DIAGRAM_LOAD, GET_XML_NODE_NAME())
			sFlowDiagram.sElement[sFlowDiagram.iElementCount].type = INT_TO_ENUM(FLOW_DIAGRAM_ELEMENT_TYPE, GET_HASH_KEY(GET_XML_NODE_NAME()))
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	
	RETURN FALSE
ENDFUNC


PROC Set_Position_Of_Second_Pass_Diagram_Element_Custom(FLOW_DIAGRAM &sFlowDiagram,
													INT iXPos, 
													FLOW_DIAGRAM_ELEMENT &sElement, 
													INT &iStrandCurrentElementIndex[MAX_STRANDS],
													INT &iReservedBounds[2])
	INT iStrandYPos = Get_Strand_Y_Position(sFlowDiagram, sElement.strand)
	
	SWITCH (sElement.type)

		CASE FD_ELEMENT_TYPE_PLANNING
		CASE FD_ELEMENT_TYPE_PHONE
		CASE FD_ELEMENT_TYPE_PHONE_YESNO
		CASE FD_ELEMENT_TYPE_TEXT

			IF iReservedBounds[0] = -1
			OR iStrandYPos < iReservedBounds[0]
			OR iStrandYPos > iReservedBounds[1]
				CDEBUG1LN(DEBUG_FLOW_DIAGRAM_POSITION, "[2nd-Pass]Positioned MISSION/COMMUNICATION element for strand ", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(sElement.strand))
				Set_Position_Of_Default_Element(sElement, iXPos, iStrandYPos)
				iStrandCurrentElementIndex[sElement.strand]++
			ENDIF
		
		BREAK
	ENDSWITCH
ENDPROC

FUNC TEXT_LABEL_7 GET_MISSION_ID_FROM_DIAGRAM_ELEMENT(FLOW_DIAGRAM_ELEMENT &sElement)
	RETURN GET_SP_MISSION_ID_LABEL_FOR_SCRIPT_NAME(REMOVE_SC_FROM_MISSION_FILENAME(sElement.mainData))
ENDFUNC


PROC DISPLAY_DIAGRAM_ELEMENT_CUSTOM(FLOW_DIAGRAM &sFlowDiagram, FLOW_DIAGRAM_ELEMENT &sElement, FLOW_DIAGRAM_ELEMENT_CUSTOM &sElementCustom, FLOAT fXCoord, INT elementIndex, BOOL &bHighlighted)
	elementIndex = elementIndex
	UNUSED_PARAMETER(bHighlighted)
	SWITCH (sElement.type)
		CASE FD_ELEMENT_TYPE_PLANNING
			Display_Flow_Diagram_Planning_Element(sFlowDiagram, sElement, sElementCustom, fXCoord)
		BREAK
		
		CASE FD_ELEMENT_TYPE_PHONE
			Display_Flow_Diagram_Phone_Element(sFlowDiagram, sElement, sElementCustom, fXCoord)
		BREAK
		
		CASE FD_ELEMENT_TYPE_PHONE_YESNO
			Display_Flow_Diagram_Phone_YesNo_Element(sFlowDiagram, sElement, sElementCustom, fXCoord)
		BREAK
		
		CASE FD_ELEMENT_TYPE_TEXT
			Display_Flow_Diagram_Text_Element(sFlowDiagram, sElement, sElementCustom, fXCoord)
		BREAK

	ENDSWITCH
ENDPROC
