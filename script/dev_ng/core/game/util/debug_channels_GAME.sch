PROC DEBUG_REGISTER_CHANNELS_CUSTOM()
	
	REGISTER_SCRIPT_CHANNEL(DEBUG_MINIGAME, 			"MINIGAME")
	REGISTER_SCRIPT_CHANNEL(DEBUG_MISSION, 				"MISSION")
	REGISTER_SCRIPT_CHANNEL(DEBUG_AMBIENT, 				"AMBIENT")
	REGISTER_SCRIPT_CHANNEL(DEBUG_METRIC_DATA,			"METRIC_DATA")
	REGISTER_SCRIPT_CHANNEL(DEBUG_TUNEABLES,			"TUNEABLES")
	REGISTER_SCRIPT_CHANNEL(DEBUG_SPECTATOR, 			"SPECTATOR")
//	REGISTER_SCRIPT_CHANNEL(DEBUG_NET_ACTIVITY_CREATOR, 			"NET_ACTIVITY_CREATOR")
	
	//System subchannels.
	REGISTER_SCRIPT_CHANNEL(DEBUG_INIT, 				"INIT",			ENUM_TO_INT(DEBUG_SYSTEM))
	REGISTER_SCRIPT_CHANNEL(DEBUG_CLEANUP_SP, 			"CLEANUP_SP",	ENUM_TO_INT(DEBUG_SYSTEM))
	REGISTER_SCRIPT_CHANNEL(DEBUG_AUTOPLAY, 			"AUTOPLAY",		ENUM_TO_INT(DEBUG_SYSTEM))
	REGISTER_SCRIPT_CHANNEL(DEBUG_CODEID, 				"CODEID",		ENUM_TO_INT(DEBUG_SYSTEM))
	REGISTER_SCRIPT_CHANNEL(DEBUG_COMMUNICATIONS, 		"COMMS",		ENUM_TO_INT(DEBUG_SYSTEM))
	REGISTER_SCRIPT_CHANNEL(DEBUG_FLOW_CUTS, 			"FLOW_CUTS",	ENUM_TO_INT(DEBUG_SYSTEM))
	REGISTER_SCRIPT_CHANNEL(DEBUG_FLOW_HELP, 			"FLOW_HELP",	ENUM_TO_INT(DEBUG_SYSTEM))
	REGISTER_SCRIPT_CHANNEL(DEBUG_FRIENDS, 				"FRIENDS",		ENUM_TO_INT(DEBUG_SYSTEM))
	REGISTER_SCRIPT_CHANNEL(DEBUG_HEIST, 				"HEIST",		ENUM_TO_INT(DEBUG_SYSTEM))
	REGISTER_SCRIPT_CHANNEL(DEBUG_PAUSE_MENU, 			"PAUSE",		ENUM_TO_INT(DEBUG_SYSTEM))
	REGISTER_SCRIPT_CHANNEL(DEBUG_RANDOM_CHAR, 			"RANDOM_CHAR",	ENUM_TO_INT(DEBUG_SYSTEM))
	REGISTER_SCRIPT_CHANNEL(DEBUG_REPEAT, 				"REPEAT",		ENUM_TO_INT(DEBUG_SYSTEM))
	REGISTER_SCRIPT_CHANNEL(DEBUG_REPLAY, 				"REPLAY",		ENUM_TO_INT(DEBUG_SYSTEM))
	REGISTER_SCRIPT_CHANNEL(DEBUG_RESPAWN, 				"RESPAWN",		ENUM_TO_INT(DEBUG_SYSTEM))
	REGISTER_SCRIPT_CHANNEL(DEBUG_TRIGGER, 				"TRIGGER",		ENUM_TO_INT(DEBUG_SYSTEM))
	REGISTER_SCRIPT_CHANNEL(DEBUG_PED_COMP, 			"PED_COMP",		ENUM_TO_INT(DEBUG_SYSTEM))
	REGISTER_SCRIPT_CHANNEL(DEBUG_BLIP, 				"BLIP",			ENUM_TO_INT(DEBUG_SYSTEM))
	REGISTER_SCRIPT_CHANNEL(DEBUG_SHOPS, 				"SHOPS",		ENUM_TO_INT(DEBUG_SYSTEM))
	REGISTER_SCRIPT_CHANNEL(DEBUG_PROPERTY,				"PROPERTY",		ENUM_TO_INT(DEBUG_SYSTEM))
	REGISTER_SCRIPT_CHANNEL(DEBUG_SCRIPT_LAUNCH,		"SCRIPT_LAUNCH",ENUM_TO_INT(DEBUG_SYSTEM))
	REGISTER_SCRIPT_CHANNEL(DEBUG_DOOR,					"DOORS",		ENUM_TO_INT(DEBUG_SYSTEM))
	REGISTER_SCRIPT_CHANNEL(DEBUG_ACHIEVEMENT,			"ACHIEVEMENT",	ENUM_TO_INT(DEBUG_SYSTEM))
	REGISTER_SCRIPT_CHANNEL(DEBUG_MISSION_STATS,		"MISS_STATS",	ENUM_TO_INT(DEBUG_SYSTEM))
	REGISTER_SCRIPT_CHANNEL(DEBUG_INTERNET,				"INTERNET",		ENUM_TO_INT(DEBUG_SYSTEM))
	REGISTER_SCRIPT_CHANNEL(DEBUG_DRUNK,				"DRUNK",		ENUM_TO_INT(DEBUG_SYSTEM))
    REGISTER_SCRIPT_CHANNEL(DEBUG_CELL_DIALOG,          "PHONE_DIALOG", ENUM_TO_INT(DEBUG_SYSTEM))
	REGISTER_SCRIPT_CHANNEL(DEBUG_BUILDING,		 		"BUILDING",		ENUM_TO_INT(DEBUG_SYSTEM))
	REGISTER_SCRIPT_CHANNEL(DEBUG_STOCKS,				"STOCKS",		ENUM_TO_INT(DEBUG_SYSTEM))
	REGISTER_SCRIPT_CHANNEL(DEBUG_DAN,		 			"DAN",			ENUM_TO_INT(DEBUG_SYSTEM))
	REGISTER_SCRIPT_CHANNEL(DEBUG_RBJ,		 			"RBJ",			ENUM_TO_INT(DEBUG_SYSTEM))	
	REGISTER_SCRIPT_CHANNEL(DEBUG_LOAD_QUEUE,		 	"LOAD_QUEUE",	ENUM_TO_INT(DEBUG_SYSTEM))
	REGISTER_SCRIPT_CHANNEL(DEBUG_FINANCE,		 		"FINANCE",		ENUM_TO_INT(DEBUG_SYSTEM))
	REGISTER_SCRIPT_CHANNEL(DEBUG_SWITCH,		 		"SWITCH",		ENUM_TO_INT(DEBUG_SYSTEM))
	REGISTER_SCRIPT_CHANNEL(DEBUG_SOCIAL,		 		"SOCIAL",		ENUM_TO_INT(DEBUG_SYSTEM))
	REGISTER_SCRIPT_CHANNEL(DEBUG_LASTGEN,		 		"LASTGEN",		ENUM_TO_INT(DEBUG_SYSTEM))
	REGISTER_SCRIPT_CHANNEL(DEBUG_DIRECTOR,		 		"DIRECTOR",		ENUM_TO_INT(DEBUG_SYSTEM))
	REGISTER_SCRIPT_CHANNEL(DEBUG_WEAPONS,		 		"WEAPONS",		ENUM_TO_INT(DEBUG_SYSTEM))
	REGISTER_SCRIPT_CHANNEL(DEBUG_YACHT, 				"PRIVATE_YACHT",ENUM_TO_INT(DEBUG_SYSTEM))
	REGISTER_SCRIPT_CHANNEL(DEBUG_BGSCRIPT_KICK,		"BGSCRIPT_KICK",ENUM_TO_INT(DEBUG_SYSTEM))
	REGISTER_SCRIPT_CHANNEL(DEBUG_FMPICKUPS,			"FMPICKUPS",	ENUM_TO_INT(DEBUG_SYSTEM))
	REGISTER_SCRIPT_CHANNEL(DEBUG_SMPL_INTERIOR,		"DEBUG_SMPL_INTERIOR",	ENUM_TO_INT(DEBUG_SYSTEM))
	REGISTER_SCRIPT_CHANNEL(DEBUG_DELIVERY,				"DEBUG_DELIVERY",	ENUM_TO_INT(DEBUG_SYSTEM))
	REGISTER_SCRIPT_CHANNEL(DEBUG_NET_MARKETING_REWARDS,"NET_MARKETING_REWARDS", ENUM_TO_INT(DEBUG_SYSTEM))
	
	//Init subchannels.
	REGISTER_SCRIPT_CHANNEL(DEBUG_INIT_SP, 				"INIT_SP",		ENUM_TO_INT(DEBUG_INIT))
	REGISTER_SCRIPT_CHANNEL(DEBUG_INIT_MP, 				"INIT_MP",		ENUM_TO_INT(DEBUG_INIT))
	
	//Communications subchannels.
	REGISTER_SCRIPT_CHANNEL(DEBUG_EMAIL, 				"EMAIL",		ENUM_TO_INT(DEBUG_COMMUNICATIONS))
	
	//Stocks subchannels.
	REGISTER_SCRIPT_CHANNEL(DEBUG_BAWSAQ, 				"BAWSAQ",		ENUM_TO_INT(DEBUG_STOCKS))
	REGISTER_SCRIPT_CHANNEL(DEBUG_LCN, 					"LCN",			ENUM_TO_INT(DEBUG_STOCKS))
	
	//Minigame subchannels.
	REGISTER_SCRIPT_CHANNEL(DEBUG_MG_LAUNCHER_APART,	"MG_LAUNCHER_APART",	ENUM_TO_INT(DEBUG_MINIGAME))
	REGISTER_SCRIPT_CHANNEL(DEBUG_MG_LAUNCHER,			"MG_LAUNCHER",	ENUM_TO_INT(DEBUG_MINIGAME))
	REGISTER_SCRIPT_CHANNEL(DEBUG_TENNIS, 				"TENNIS",		ENUM_TO_INT(DEBUG_MINIGAME))
	REGISTER_SCRIPT_CHANNEL(DEBUG_DARTS, 				"DARTS",		ENUM_TO_INT(DEBUG_MINIGAME))
	REGISTER_SCRIPT_CHANNEL(DEBUG_GOLF, 				"GOLF",			ENUM_TO_INT(DEBUG_MINIGAME))
	REGISTER_SCRIPT_CHANNEL(DEBUG_SCLUB, 				"SCLUB",		ENUM_TO_INT(DEBUG_MINIGAME))
	REGISTER_SCRIPT_CHANNEL(DEBUG_ARM_WRESTLING,		"ARM_WRESTLE",	ENUM_TO_INT(DEBUG_MINIGAME))
	REGISTER_SCRIPT_CHANNEL(DEBUG_SP_RACES,				"SP_RACES",		ENUM_TO_INT(DEBUG_MINIGAME))
	REGISTER_SCRIPT_CHANNEL(DEBUG_OR_RACES,				"OR_RACES",		ENUM_TO_INT(DEBUG_MINIGAME))
	REGISTER_SCRIPT_CHANNEL(DEBUG_TRIATHLON,			"TRIATHLON",	ENUM_TO_INT(DEBUG_MINIGAME))
	REGISTER_SCRIPT_CHANNEL(DEBUG_SHOOTRANGE,			"SHOOTRANGE",	ENUM_TO_INT(DEBUG_MINIGAME))
	REGISTER_SCRIPT_CHANNEL(DEBUG_ROULETTE,				"ROULETTE",		ENUM_TO_INT(DEBUG_MINIGAME))
	
	//Oddjob subchannels.
	REGISTER_SCRIPT_CHANNEL(DEBUG_OJ_TAXI, 				"OJ_TAXI",		ENUM_TO_INT(DEBUG_MINIGAME))
	REGISTER_SCRIPT_CHANNEL(DEBUG_BOOTY_CALL, 			"BOOTY_CALL",	ENUM_TO_INT(DEBUG_MINIGAME))
	REGISTER_SCRIPT_CHANNEL(DEBUG_RAMPAGE, 				"RAMPAGE",		ENUM_TO_INT(DEBUG_MINIGAME))
	REGISTER_SCRIPT_CHANNEL(DEBUG_NET_MINIGAME,			"NET_MINIGAME", ENUM_TO_INT(DEBUG_MINIGAME))
	REGISTER_SCRIPT_CHANNEL(DEBUG_HUNTING,				"HUNTING",		ENUM_TO_INT(DEBUG_MINIGAME))
	
	//Mission subchannels.
	//NB. Mission specific/personal channels are pointless. Only one mission runs at a time. 
	//Almost all of these could be DEBUG_MISSION. Keeps list more concise and any dev knows how to
	//tweak any mission's logging levels. Avoid next project. -BenR
	REGISTER_SCRIPT_CHANNEL(DEBUG_TREVOR3, 				"TREVOR_3",				ENUM_TO_INT(DEBUG_MISSION))
	REGISTER_SCRIPT_CHANNEL(DEBUG_CHINESE2, 			"CHINESE_2",			ENUM_TO_INT(DEBUG_MISSION))
	REGISTER_SCRIPT_CHANNEL(DEBUG_NET_MISSION,			"NET_MISSION", 			ENUM_TO_INT(DEBUG_MISSION))
	REGISTER_SCRIPT_CHANNEL(DEBUG_SHARM_STEALTH,		"SHARM_STLTH", 			ENUM_TO_INT(DEBUG_MISSION))
	REGISTER_SCRIPT_CHANNEL(DEBUG_MIKE,					"MIKE_W", 				ENUM_TO_INT(DEBUG_MISSION))
	REGISTER_SCRIPT_CHANNEL(DEBUG_MIKE_UTIL,			"MIKE_W_UTIL", 			ENUM_TO_INT(DEBUG_MISSION))
	REGISTER_SCRIPT_CHANNEL(DEBUG_SIMON,				"SIMON_B", 				ENUM_TO_INT(DEBUG_MISSION))
	REGISTER_SCRIPT_CHANNEL(DEBUG_COLIN,				"COLIN",				ENUM_TO_INT(DEBUG_MISSION))
	REGISTER_SCRIPT_CHANNEL(DEBUG_OWAIN,				"OWAIN",				ENUM_TO_INT(DEBUG_MISSION))
	REGISTER_SCRIPT_CHANNEL(DEBUG_CONTROLLER,			"MISSION_CONTROLLER",	ENUM_TO_INT(DEBUG_MISSION))
	
	//Mission controller subchannels.
	REGISTER_SCRIPT_CHANNEL(DEBUG_MC_BLIP,				"MCBLIP",				ENUM_TO_INT(DEBUG_CONTROLLER))
	SET_SCRIPT_CHANNEL_LEVEL( DEBUG_CONTROLLER, CHANNEL_FILELOG_LEVEL, CHANNEL_SEVERITY_DEBUG3 )
	SET_SCRIPT_CHANNEL_LEVEL( DEBUG_CONTROLLER, CHANNEL_TTY_LEVEL, CHANNEL_SEVERITY_DEBUG3 )
	
	//Ambient subchannels.
	REGISTER_SCRIPT_CHANNEL(DEBUG_NET_AMBIENT, 							"NET_AMBIENT", 		   			 	ENUM_TO_INT(DEBUG_AMBIENT))
	REGISTER_SCRIPT_CHANNEL(DEBUG_MP_AMBIENT_MANAGER,					"MP_AMBIENT_MANAGER",  			 	ENUM_TO_INT(DEBUG_AMBIENT))
	REGISTER_SCRIPT_CHANNEL(DEBUG_NET_HEADSHOTS,						"NET_HEADSHOTS", 	   			 	ENUM_TO_INT(DEBUG_AMBIENT))
	REGISTER_SCRIPT_CHANNEL(DEBUG_PROSTITUTE, 							"PROSTITUTE", 		   			 	ENUM_TO_INT(DEBUG_AMBIENT))
	REGISTER_SCRIPT_CHANNEL(DEBUG_SHOP_ROBBERIES,						"SHOP_ROBS",   		   			 	ENUM_TO_INT(DEBUG_AMBIENT))
	REGISTER_SCRIPT_CHANNEL(DEBUG_BAIL_BOND,							"BAIL_BOND",   		   			 	ENUM_TO_INT(DEBUG_AMBIENT))
	REGISTER_SCRIPT_CHANNEL(DEBUG_TAXI_SERVICE,							"TAXI_SERVICE",   					ENUM_TO_INT(DEBUG_AMBIENT))
	REGISTER_SCRIPT_CHANNEL(DEBUG_RANDOM_EVENTS,						"RANDOM_EVENT",        				ENUM_TO_INT(DEBUG_AMBIENT))
	REGISTER_SCRIPT_CHANNEL(DEBUG_MP_CONTACT_REQUESTS,					"NET_CONTACT_REQUESTS",			 	ENUM_TO_INT(DEBUG_AMBIENT))
	REGISTER_SCRIPT_CHANNEL(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE,	"COLLECTIBLES_PHOTOGRAPHY_WILDLIFE",ENUM_TO_INT(DEBUG_AMBIENT))
	REGISTER_SCRIPT_CHANNEL(DEBUG_FAMILY,		 						"FAMILY",							ENUM_TO_INT(DEBUG_AMBIENT))
	REGISTER_SCRIPT_CHANNEL(DEBUG_ANIMAL_MODE,		 					"ANIMAL_MODE",						ENUM_TO_INT(DEBUG_AMBIENT))
	REGISTER_SCRIPT_CHANNEL(DEBUG_NET_AMBIENT_LR, 						"NET_AMBIENT_LR", 		   			ENUM_TO_INT(DEBUG_AMBIENT))
	REGISTER_SCRIPT_CHANNEL(DEBUG_NET_MAGNATE, 							"NET_MAGNATE", 		   			 	ENUM_TO_INT(DEBUG_AMBIENT))
	REGISTER_SCRIPT_CHANNEL(DEBUG_PI_MENU_MK2,							"PI_MENU_MK2",						ENUM_TO_INT(DEBUG_AMBIENT))
	REGISTER_SCRIPT_CHANNEL(DEBUG_NODE_MENU,							"NODE_MENU",						ENUM_TO_INT(DEBUG_AMBIENT))
    
	//Spectator
    REGISTER_SCRIPT_CHANNEL(DEBUG_SPECTATOR,            				"SPECTATOR")
    
	// Activity Creator
	REGISTER_SCRIPT_CHANNEL(DEBUG_NET_ACTIVITY_CREATOR,	  				"NET_ACTIVITY_CREATOR")	
	
	// Facilty Scenario
	REGISTER_SCRIPT_CHANNEL(DEBUG_NET_ACTIVITY_SCENARIO,	  				"NET_ACTIVITY_SCENARIO")	
	
	// Gun Turret 
	REGISTER_SCRIPT_CHANNEL(DEBUG_NET_GUN_TURRET,	  				"NET_GUN_TURRET")	
	
	// Safehouse
	REGISTER_SCRIPT_CHANNEL(DEBUG_SAFEHOUSE,		 					"SAFEHOUSE")
	REGISTER_SCRIPT_CHANNEL(DEBUG_SAFEHOUSE,          					"NET_SAFEHOUSE")
	REGISTER_SCRIPT_CHANNEL(DEBUG_PI_DOORS,          					"PI_DOORS")
	
	REGISTER_SCRIPT_CHANNEL(DEBUG_ACT_CELEB,          					"NET_ACT_CELEB",          			ENUM_TO_INT(DEBUG_SAFEHOUSE))
	REGISTER_SCRIPT_CHANNEL(DEBUG_HEIST_PREP_EXIT,	  					"HEIST_PREP_EXIT",					ENUM_TO_INT(DEBUG_SAFEHOUSE))	
	
	
	
//	REGISTER_SCRIPT_CHANNEL(DEBUG_SHOWER,   		  "DEBUG_SHOWER",           ENUM_TO_INT(DEBUG_SAFEHOUSE))
//	REGISTER_SCRIPT_CHANNEL(DEBUG_MP_TELESCOPE,   	  "DEBUG_MP_TELESCOPE",     ENUM_TO_INT(DEBUG_SAFEHOUSE))
	
	REGISTER_SCRIPT_CHANNEL(DEBUG_MP_MINIGAME_ACT,    "DEBUG_MP_MINIGAME_ACT",            ENUM_TO_INT(DEBUG_SAFEHOUSE))
	REGISTER_SCRIPT_CHANNEL(DEBUG_MP_TV,   			  "DEBUG_MP_TV",            ENUM_TO_INT(DEBUG_SAFEHOUSE))
	REGISTER_SCRIPT_CHANNEL(DEBUG_SPAWN_ACTIVITIES,   "NET_SPAWN_ACTIVITIES",   ENUM_TO_INT(DEBUG_SAFEHOUSE))
	REGISTER_SCRIPT_CHANNEL(DEBUG_HEIST_PLAN_PROP,    "NET_HEIST_PLAN_PROP",    ENUM_TO_INT(DEBUG_SAFEHOUSE))
	REGISTER_SCRIPT_CHANNEL(DEBUG_RADIO,			  "NET_DEBUG_RADIO",		ENUM_TO_INT(DEBUG_SAFEHOUSE))
	
	
	//Achievement subchannels.
	REGISTER_SCRIPT_CHANNEL(DEBUG_ACTIVITY_FEED,		"ACTIVITY_FEED",		ENUM_TO_INT(DEBUG_ACHIEVEMENT))
    REGISTER_SCRIPT_CHANNEL(DEBUG_MP_CNC1,              "DEBUG_MP_CNC1",  		ENUM_TO_INT(DEBUG_SYSTEM))

	// MP Heist debugging.
	REGISTER_SCRIPT_CHANNEL(DEBUG_MP_HEISTS,            "MP_HEISTS")
	REGISTER_SCRIPT_CHANNEL(DEBUG_MP_HEISTS_AMEC,       "AMEC_HEISTS", 			ENUM_TO_INT(DEBUG_MP_HEISTS)) 
	REGISTER_SCRIPT_CHANNEL(DEBUG_MP_HEISTS_KGM,       	"KGM_HEISTS", 			ENUM_TO_INT(DEBUG_MP_HEISTS)) 
	REGISTER_SCRIPT_CHANNEL(DEBUG_MP_HEISTS_RBJ, 		"RBJ_HEISTS",			ENUM_TO_INT(DEBUG_MP_HEISTS))
	
	//Multiplayer Transitions
	REGISTER_SCRIPT_CHANNEL(DEBUG_SKYCAM, 				"DEBUG_SKYCAM",     	ENUM_TO_INT(DEBUG_SYSTEM))
	REGISTER_SCRIPT_CHANNEL(DEBUG_TRANSITION, 			"DEBUG_TRANSITION", 	ENUM_TO_INT(DEBUG_SYSTEM))
	REGISTER_SCRIPT_CHANNEL(DEBUG_OVERHEADS, 			"DEBUG_OVERHEADS",  	ENUM_TO_INT(DEBUG_SYSTEM))
	REGISTER_SCRIPT_CHANNEL(DEBUG_MP_TUTORIAL, 			"DEBUG_MP_TUTORIAL", 	ENUM_TO_INT(DEBUG_SYSTEM)) 
	REGISTER_SCRIPT_CHANNEL(DEBUG_MP_UNLOCKS, 			"DEBUG_MP_UNLOCKS",  	ENUM_TO_INT(DEBUG_SYSTEM))
	REGISTER_SCRIPT_CHANNEL(DEBUG_MP_CNC1, 				"DEBUG_MP_CNC1",  		ENUM_TO_INT(DEBUG_SYSTEM))
	REGISTER_SCRIPT_CHANNEL(DEBUG_SONYFEED, 			"DEBUG_SONYFEED",     	ENUM_TO_INT(DEBUG_SYSTEM))

	// Online Character Select/Creation
	REGISTER_SCRIPT_CHANNEL(DEBUG_NET_CHARACTER, 	"DEBUG_NET_CHARACTER", ENUM_TO_INT(DEBUG_SYSTEM))
	
	// Online Spawning.
	REGISTER_SCRIPT_CHANNEL(DEBUG_SPAWNING, "SPAWNING", ENUM_TO_INT(DEBUG_SYSTEM))
	
	// Cutscene.
	REGISTER_SCRIPT_CHANNEL(DEBUG_CUTSCENE, "CUTSCENE", ENUM_TO_INT(DEBUG_SYSTEM))
	
	// Freemode Delivery
	REGISTER_SCRIPT_CHANNEL(DEBUG_NET_FREEMODE_DELIVERY, "NET_FREEMODE_DELIVERY", ENUM_TO_INT(DEBUG_SYSTEM))
	
	REGISTER_SCRIPT_CHANNEL(DEBUG_GANG_OPS, "NET_GANG_OPS")
	REGISTER_SCRIPT_CHANNEL(DEBUG_GANG_OPS_PLAN, "NET_GANG_OPS_PLAN")
	REGISTER_SCRIPT_CHANNEL(DEBUG_GANG_OPS_FIN, "NET_GANG_OPS_FIN")
	
	// Dancing in the club (Business battles)
	REGISTER_SCRIPT_CHANNEL(DEBUG_NET_DANCING, "DEBUG_NET_DANCING")
	REGISTER_SCRIPT_CHANNEL(DEBUG_NET_PED_DANCING, "DEBUG_NET_PED_DANCING")
	REGISTER_SCRIPT_CHANNEL(DEBUG_NET_DRONE, "DEBUG_NET_DRONE")
	REGISTER_SCRIPT_CHANNEL(DEBUG_EXT_MENU, "DEBUG_EXT_MENU")
	REGISTER_SCRIPT_CHANNEL(DEBUG_NET_VEH_WEAPON, "DEBUG_NET_VEH_WEAPON")
	
	// Arena wars
	REGISTER_SCRIPT_CHANNEL(DEBUG_NET_TURRET, "DEBUG_NET_TURRET")
	
	REGISTER_SCRIPT_CHANNEL(DEBUG_CASINO_PEDS, "DEBUG_CASINO_PEDS")
	REGISTER_SCRIPT_CHANNEL(DEBUG_NET_DJ, "DEBUG_NET_DJ")
	
	REGISTER_SCRIPT_CHANNEL(DEBUG_CASINO_HEIST_PLANNING, "NET_CASINO_HEIST_PLANNING")
	
	REGISTER_SCRIPT_CHANNEL(DEBUG_NET_HEIST_PLANNING_GENERIC, "NET_HEIST_PLANNING_GENERIC")
	
	REGISTER_SCRIPT_CHANNEL(DEBUG_COPS_CROOKS, "COPS_CROOKS")
ENDPROC

