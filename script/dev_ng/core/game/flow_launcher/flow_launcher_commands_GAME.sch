FUNC JUMP_LABEL_IDS LAUNCHER_SKIP_NEXT_STRAND_COMMAND_CUSTOM(FLOW_LAUNCHER_VARS &flowLauncher, STRANDS paramStrand, BOOL paramGeneratePlaythroughOrder = FALSE, BOOL paramGenerateFlowDiagram = FALSE)
	JUMP_LABEL_IDS eNextLabel
	
	
	#if USE_CLF_DLC
		//Save current command data from the strand's current pointer position.
		FLOW_COMMANDS sCurrentCommand = g_flowUnsaved.flowCommands[g_savedGlobalsClifford.sFlow.strandSavedVars[paramStrand].thisCommandPos]
	#endif
	#if USE_NRM_DLC
		//Save current command data from the strand's current pointer position.
		FLOW_COMMANDS sCurrentCommand = g_flowUnsaved.flowCommands[g_savedGlobalsnorman.sFlow.strandSavedVars[paramStrand].thisCommandPos]
	#endif
	
	#if not USE_CLF_DLC
	#if not use_NRM_DLC
		//Save current command data from the strand's current pointer position.
		FLOW_COMMANDS sCurrentCommand = g_flowUnsaved.flowCommands[g_savedGlobals.sFlow.strandSavedVars[paramStrand].thisCommandPos]
	#endif
	#endif
	
	//Check what the next command is and deal with it accordingly.
	SWITCH (sCurrentCommand.command)
	
		CASE FLOW_ADD_PHONE_CONTACT
			LAUNCHER_DISPLAY_GENERIC_COMMAND_DEBUG_OUTPUT(paramStrand, sCurrentCommand.command, TRUE)
		
			//Call launcher specific function.
			eNextLabel = LAUNCHER_SKIP_ADD_PHONE_CONTACT(sCurrentCommand.index)
		BREAK
		
		CASE FLOW_REMOVE_PHONE_CONTACT
			LAUNCHER_DISPLAY_GENERIC_COMMAND_DEBUG_OUTPUT(paramStrand, sCurrentCommand.command, FALSE)
		
			//Call flow command.
			eNextLabel = PERFORM_REMOVE_PHONE_CONTACT(sCurrentCommand.index)
		BREAK
		
		CASE FLOW_ADD_NEW_FRIEND_CONTACT
			LAUNCHER_DISPLAY_GENERIC_COMMAND_DEBUG_OUTPUT(paramStrand, sCurrentCommand.command, FALSE)
		
			//Call flow command.
			eNextLabel = PERFORM_ADD_NEW_FRIEND_CONTACT(sCurrentCommand.index)
		BREAK
		
		CASE FLOW_BLOCK_FRIEND_CONTACT
			LAUNCHER_DISPLAY_GENERIC_COMMAND_DEBUG_OUTPUT(paramStrand, sCurrentCommand.command, FALSE)
		
			//Call flow command.
			eNextLabel = PERFORM_BLOCK_FRIEND_CONTACT(sCurrentCommand.index)
		BREAK
		CASE FLOW_BLOCK_FRIEND_CLASH
			LAUNCHER_DISPLAY_GENERIC_COMMAND_DEBUG_OUTPUT(paramStrand, sCurrentCommand.command, FALSE)
		
			//Call flow command.
			eNextLabel = PERFORM_BLOCK_FRIEND_CLASH(sCurrentCommand.index)
		BREAK
		CASE FLOW_FRIEND_CONTACT_BUSY
			LAUNCHER_DISPLAY_GENERIC_COMMAND_DEBUG_OUTPUT(paramStrand, sCurrentCommand.command, FALSE)
		
			//Call flow command.
			eNextLabel = PERFORM_FRIEND_CONTACT_BUSY(sCurrentCommand.index)
		BREAK
		CASE FLOW_SET_CHAR_CHAT_PHONECALL
			LAUNCHER_DISPLAY_GENERIC_COMMAND_DEBUG_OUTPUT(paramStrand, sCurrentCommand.command, FALSE)
		
			//Call flow command.
			eNextLabel = PERFORM_SET_CHAR_CHAT_PHONECALL(sCurrentCommand.index)
		BREAK
		
		CASE FLOW_IS_PLAYER_PHONING_NPC
			//Call the launcher specific skip function.
			eNextLabel = LAUNCHER_SKIP_IS_PHONING_NPC_COMMAND(sCurrentCommand.index)
		BREAK
		
		CASE FLOW_PLAYER_PHONE_NPC
			LAUNCHER_DISPLAY_GENERIC_COMMAND_DEBUG_OUTPUT(paramStrand, sCurrentCommand.command, TRUE)
			
			//If we're generating a flow diagram, write this phone command to xml in the specified debug file.
			IF paramGenerateFlowDiagram
				LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, "   <phone		strand=\"")
				LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(paramStrand))
				LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, "\"")
				LAUNCHER_WRITE_STRING_TABS_TO_DIAGRAM_XML_FILE	(flowLauncher, 6, GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(paramStrand))
				LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, "charFrom=\"")
				LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, GET_CHARSHEET_DISPLAY_STRING_FROM_CHARSHEET(INT_TO_ENUM(enumCharacterList, g_flowUnsaved.commsVars[sCurrentCommand.index].iValue3)))
				LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, "\"")
				LAUNCHER_WRITE_STRING_TABS_TO_DIAGRAM_XML_FILE	(flowLauncher, 8, GET_CHARSHEET_DISPLAY_STRING_FROM_CHARSHEET(INT_TO_ENUM(enumCharacterList, g_flowUnsaved.commsVars[sCurrentCommand.index].iValue3)))
				LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, "charTo=\"")
				LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, GET_CHARSHEET_DISPLAY_STRING_FROM_CHARSHEET(INT_TO_ENUM(enumCharacterList, g_flowUnsaved.commsVars[sCurrentCommand.index].iValue2)))
				LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, "\"/>")
				LAUNCHER_WRITE_NEW_LINE_TO_DIAGRAM_XML_FILE		(flowLauncher)
				flowLauncher.iLauncherCurrentDiagramElementID++
			ENDIF
			
			//Call the launcher specific skip function.
			eNextLabel = LAUNCHER_SKIP_COMMS_COMMAND(sCurrentCommand)
		BREAK
		
		CASE FLOW_PLAYER_PHONE_NPC_WITH_BRANCH
			LAUNCHER_DISPLAY_GENERIC_COMMAND_DEBUG_OUTPUT(paramStrand, sCurrentCommand.command, TRUE)
			
			//If we're generating a flow diagram, write this phone command to xml in the specified debug file.
			IF paramGenerateFlowDiagram
				LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, "   <phone		strand=\"")
				LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(paramStrand))
				LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, "\"")
				LAUNCHER_WRITE_STRING_TABS_TO_DIAGRAM_XML_FILE	(flowLauncher, 6, GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(paramStrand))
				LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, "charFrom=\"")
				LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, GET_CHARSHEET_DISPLAY_STRING_FROM_CHARSHEET(INT_TO_ENUM(enumCharacterList, g_flowUnsaved.commsLargeVars[sCurrentCommand.index].iValue5)))
				LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, "\"")
				LAUNCHER_WRITE_STRING_TABS_TO_DIAGRAM_XML_FILE	(flowLauncher, 8, GET_CHARSHEET_DISPLAY_STRING_FROM_CHARSHEET(INT_TO_ENUM(enumCharacterList, g_flowUnsaved.commsLargeVars[sCurrentCommand.index].iValue5)))
				LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, "charTo=\"")
				LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, GET_CHARSHEET_DISPLAY_STRING_FROM_CHARSHEET(INT_TO_ENUM(enumCharacterList, g_flowUnsaved.commsLargeVars[sCurrentCommand.index].iValue4)))
				LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, "\"/>")
				LAUNCHER_WRITE_NEW_LINE_TO_DIAGRAM_XML_FILE		(flowLauncher)
				flowLauncher.iLauncherCurrentDiagramElementID++
			ENDIF
			
			//Call the launcher specific skip function.
			eNextLabel = LAUNCHER_SKIP_COMMS_LARGE_COMMAND(sCurrentCommand)
		BREAK
		
		CASE FLOW_PHONE_PLAYER
			LAUNCHER_DISPLAY_GENERIC_COMMAND_DEBUG_OUTPUT(paramStrand, sCurrentCommand.command, TRUE)
			
			//If we're generating a flow diagram, write this phone command to xml in the specified debug file.
			IF paramGenerateFlowDiagram
				TEXT_LABEL_63 txtCharTo
				BOOL bSetOne
				
				txtCharTo = ""
				bSetOne= FALSE
				IF IS_BIT_SET(g_flowUnsaved.commsVars[sCurrentCommand.index].iValue3, ENUM_TO_INT(CHAR_MICHAEL))
					txtCharTo += "MICHAEL"
					bSetOne = TRUE
				ENDIF
				IF IS_BIT_SET(g_flowUnsaved.commsVars[sCurrentCommand.index].iValue3, ENUM_TO_INT(CHAR_FRANKLIN))
					IF bSetOne 
						txtCharTo += "|FRANKLIN"
					ELSE
						txtCharTo += "FRANKLIN"
						bSetOne = TRUE
					ENDIF
				ENDIF
				IF IS_BIT_SET(g_flowUnsaved.commsVars[sCurrentCommand.index].iValue3, ENUM_TO_INT(CHAR_TREVOR))
					IF bSetOne 
						txtCharTo += "|TREVOR"
					ELSE
						txtCharTo += "TREVOR"
					ENDIF
				ENDIF
				LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, "   <phone		strand=\"")
				LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(paramStrand))
				LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, "\"")
				LAUNCHER_WRITE_STRING_TABS_TO_DIAGRAM_XML_FILE	(flowLauncher, 6, GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(paramStrand))
				LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, "charFrom=\"")
				LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, GET_CHARSHEET_DISPLAY_STRING_FROM_CHARSHEET(INT_TO_ENUM(enumCharacterList, g_flowUnsaved.commsVars[sCurrentCommand.index].iValue4)))
				LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, "\"")
				LAUNCHER_WRITE_STRING_TABS_TO_DIAGRAM_XML_FILE	(flowLauncher, 8, GET_CHARSHEET_DISPLAY_STRING_FROM_CHARSHEET(INT_TO_ENUM(enumCharacterList, g_flowUnsaved.commsVars[sCurrentCommand.index].iValue4)))
				LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, "charTo=\"")
				LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, txtCharTo)
				LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, "\"/>")
				LAUNCHER_WRITE_NEW_LINE_TO_DIAGRAM_XML_FILE		(flowLauncher)
				flowLauncher.iLauncherCurrentDiagramElementID++
			ENDIF
			
			//Call the launcher specific skip function.
			eNextLabel = LAUNCHER_SKIP_COMMS_COMMAND(sCurrentCommand)
		BREAK
		
		
		CASE FLOW_PHONE_PLAYER_WITH_JUMPS
			LAUNCHER_DISPLAY_GENERIC_COMMAND_DEBUG_OUTPUT(paramStrand, sCurrentCommand.command, TRUE)
			
			//If we're generating a flow diagram, write this phone command to xml in the specified debug file.
			IF paramGenerateFlowDiagram
				TEXT_LABEL_63 txtCharTo
				BOOL bSetOne
				
				txtCharTo = ""
				bSetOne= FALSE
				IF IS_BIT_SET(g_flowUnsaved.commsLargeVars[sCurrentCommand.index].iValue2, ENUM_TO_INT(CHAR_MICHAEL))
					txtCharTo += "MICHAEL"
					bSetOne = TRUE
				ENDIF
				IF IS_BIT_SET(g_flowUnsaved.commsLargeVars[sCurrentCommand.index].iValue2, ENUM_TO_INT(CHAR_FRANKLIN))
					IF bSetOne 
						txtCharTo += "|FRANKLIN"
					ELSE
						txtCharTo += "FRANKLIN"
						bSetOne = TRUE
					ENDIF
				ENDIF
				IF IS_BIT_SET(g_flowUnsaved.commsLargeVars[sCurrentCommand.index].iValue2, ENUM_TO_INT(CHAR_TREVOR))
					IF bSetOne 
						txtCharTo += "|TREVOR"
					ELSE
						txtCharTo += "TREVOR"
					ENDIF
				ENDIF
				LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, "   <phone		strand=\"")
				LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(paramStrand))
				LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, "\"")
				LAUNCHER_WRITE_STRING_TABS_TO_DIAGRAM_XML_FILE	(flowLauncher, 6, GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(paramStrand))
				LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, "charFrom=\"")
				LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, GET_CHARSHEET_DISPLAY_STRING_FROM_CHARSHEET(INT_TO_ENUM(enumCharacterList, g_flowUnsaved.commsLargeVars[sCurrentCommand.index].iValue3)))
				LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, "\"")
				LAUNCHER_WRITE_STRING_TABS_TO_DIAGRAM_XML_FILE	(flowLauncher, 8, GET_CHARSHEET_DISPLAY_STRING_FROM_CHARSHEET(INT_TO_ENUM(enumCharacterList, g_flowUnsaved.commsLargeVars[sCurrentCommand.index].iValue3)))
				LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, "charTo=\"")
				LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, txtCharTo)
				LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, "\"/>")
				LAUNCHER_WRITE_NEW_LINE_TO_DIAGRAM_XML_FILE		(flowLauncher)
				flowLauncher.iLauncherCurrentDiagramElementID++
			ENDIF
			
			//Call the launcher specific skip function.
			eNextLabel = LAUNCHER_SKIP_COMMS_LARGE_COMMAND(sCurrentCommand)
		BREAK
		
		
		CASE FLOW_PHONE_PLAYER_WITH_BRANCH
			LAUNCHER_DISPLAY_GENERIC_COMMAND_DEBUG_OUTPUT(paramStrand, sCurrentCommand.command, TRUE)
			
			//If we're generating a flow diagram, write this phone command to xml in the specified debug file.
			IF paramGenerateFlowDiagram
				TEXT_LABEL_63 txtCharTo
				BOOL bSetOne
				
				txtCharTo = ""
				bSetOne= FALSE
				IF IS_BIT_SET(g_flowUnsaved.commsLargeVars[sCurrentCommand.index].iValue4, ENUM_TO_INT(CHAR_MICHAEL))
					txtCharTo += "MICHAEL"
					bSetOne = TRUE
				ENDIF
				IF IS_BIT_SET(g_flowUnsaved.commsLargeVars[sCurrentCommand.index].iValue4, ENUM_TO_INT(CHAR_FRANKLIN))
					IF bSetOne 
						txtCharTo += "|FRANKLIN"
					ELSE
						txtCharTo += "FRANKLIN"
						bSetOne = TRUE
					ENDIF
				ENDIF
				IF IS_BIT_SET(g_flowUnsaved.commsLargeVars[sCurrentCommand.index].iValue4, ENUM_TO_INT(CHAR_TREVOR))
					IF bSetOne 
						txtCharTo += "|TREVOR"
					ELSE
						txtCharTo += "TREVOR"
					ENDIF
				ENDIF
				LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, "   <phone		strand=\"")
				LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(paramStrand))
				LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, "\"")
				LAUNCHER_WRITE_STRING_TABS_TO_DIAGRAM_XML_FILE	(flowLauncher, 6, GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(paramStrand))
				LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, "charFrom=\"")
				LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, GET_CHARSHEET_DISPLAY_STRING_FROM_CHARSHEET(INT_TO_ENUM(enumCharacterList, g_flowUnsaved.commsLargeVars[sCurrentCommand.index].iValue5)))
				LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, "\"")
				LAUNCHER_WRITE_STRING_TABS_TO_DIAGRAM_XML_FILE	(flowLauncher, 8, GET_CHARSHEET_DISPLAY_STRING_FROM_CHARSHEET(INT_TO_ENUM(enumCharacterList, g_flowUnsaved.commsLargeVars[sCurrentCommand.index].iValue5)))
				LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, "charTo=\"")
				LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, txtCharTo)
				LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, "\"/>")
				LAUNCHER_WRITE_NEW_LINE_TO_DIAGRAM_XML_FILE		(flowLauncher)
				flowLauncher.iLauncherCurrentDiagramElementID++
			ENDIF
		
			//Call the launcher specific skip function.
			eNextLabel = LAUNCHER_SKIP_COMMS_LARGE_COMMAND(sCurrentCommand)
		BREAK
		
		CASE FLOW_PHONE_PLAYER_YES_NO
			LAUNCHER_DISPLAY_GENERIC_COMMAND_DEBUG_OUTPUT(paramStrand, sCurrentCommand.command, TRUE)
			
			//If we're generating a flow diagram, write this phone command to xml in the specified debug file.
			IF paramGenerateFlowDiagram
				TEXT_LABEL_63 txtCharTo
				BOOL bSetOne
				
				txtCharTo = ""
				bSetOne= FALSE
				IF IS_BIT_SET(g_flowUnsaved.commsLargeVars[sCurrentCommand.index].iValue4, ENUM_TO_INT(CHAR_MICHAEL))
					txtCharTo += "MICHAEL"
					bSetOne = TRUE
				ENDIF
				IF IS_BIT_SET(g_flowUnsaved.commsLargeVars[sCurrentCommand.index].iValue4, ENUM_TO_INT(CHAR_FRANKLIN))
					IF bSetOne 
						txtCharTo += "|FRANKLIN"
					ELSE
						txtCharTo += "FRANKLIN"
						bSetOne = TRUE
					ENDIF
				ENDIF
				IF IS_BIT_SET(g_flowUnsaved.commsLargeVars[sCurrentCommand.index].iValue4, ENUM_TO_INT(CHAR_TREVOR))
					IF bSetOne 
						txtCharTo += "|TREVOR"
					ELSE
						txtCharTo += "TREVOR"
					ENDIF
				ENDIF
				LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, "   <phoneYesNo	strand=\"")
				LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(paramStrand))
				LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, "\"")
				LAUNCHER_WRITE_STRING_TABS_TO_DIAGRAM_XML_FILE	(flowLauncher, 6, GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(paramStrand))
				LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, "charFrom=\"")
				LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, GET_CHARSHEET_DISPLAY_STRING_FROM_CHARSHEET(INT_TO_ENUM(enumCharacterList, g_flowUnsaved.commsLargeVars[sCurrentCommand.index].iValue5)))
				LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, "\"")
				LAUNCHER_WRITE_STRING_TABS_TO_DIAGRAM_XML_FILE	(flowLauncher, 8, GET_CHARSHEET_DISPLAY_STRING_FROM_CHARSHEET(INT_TO_ENUM(enumCharacterList, g_flowUnsaved.commsLargeVars[sCurrentCommand.index].iValue5)))
				LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, "charTo=\"")
				LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, txtCharTo)
				LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, "\"/>")
				LAUNCHER_WRITE_NEW_LINE_TO_DIAGRAM_XML_FILE		(flowLauncher)
				flowLauncher.iLauncherCurrentDiagramElementID++
			ENDIF
		
			//Call the launcher specific skip function.
			eNextLabel = LAUNCHER_SKIP_COMMS_LARGE_COMMAND(sCurrentCommand)
		BREAK
		
		CASE FLOW_TEXT_PLAYER
			LAUNCHER_DISPLAY_GENERIC_COMMAND_DEBUG_OUTPUT(paramStrand, sCurrentCommand.command, TRUE)
			
			//If we're generating a flow diagram, write this text message command to xml in the specified debug file.
			IF paramGenerateFlowDiagram
				TEXT_LABEL_63 txtCharTo
				BOOL bSetOne
				
				txtCharTo = ""
				bSetOne= FALSE
				IF IS_BIT_SET(g_flowUnsaved.commsVars[sCurrentCommand.index].iValue2, ENUM_TO_INT(CHAR_MICHAEL))
					txtCharTo += "MICHAEL"
					bSetOne = TRUE
				ENDIF
				IF IS_BIT_SET(g_flowUnsaved.commsVars[sCurrentCommand.index].iValue2, ENUM_TO_INT(CHAR_FRANKLIN))
					IF bSetOne 
						txtCharTo += "|FRANKLIN"
					ELSE
						txtCharTo += "FRANKLIN"
						bSetOne = TRUE
					ENDIF
				ENDIF
				IF IS_BIT_SET(g_flowUnsaved.commsVars[sCurrentCommand.index].iValue2, ENUM_TO_INT(CHAR_TREVOR))
					IF bSetOne 
						txtCharTo += "|TREVOR"
					ELSE
						txtCharTo += "TREVOR"
					ENDIF
				ENDIF
				LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, "   <text		strand=\"")
				LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(paramStrand))
				LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, "\"")
				LAUNCHER_WRITE_STRING_TABS_TO_DIAGRAM_XML_FILE	(flowLauncher, 6, GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(paramStrand))
				LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, "charFrom=\"")
				LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, GET_CHARSHEET_DISPLAY_STRING_FROM_CHARSHEET(INT_TO_ENUM(enumCharacterList, g_flowUnsaved.commsVars[sCurrentCommand.index].iValue3)))
				LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, "\"")
				LAUNCHER_WRITE_STRING_TABS_TO_DIAGRAM_XML_FILE	(flowLauncher, 8, GET_CHARSHEET_DISPLAY_STRING_FROM_CHARSHEET(INT_TO_ENUM(enumCharacterList, g_flowUnsaved.commsVars[sCurrentCommand.index].iValue3)))
				LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, "charTo=\"")
				LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, txtCharTo)
				LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, "\"/>")
				LAUNCHER_WRITE_NEW_LINE_TO_DIAGRAM_XML_FILE		(flowLauncher)
				flowLauncher.iLauncherCurrentDiagramElementID++
			ENDIF
		
			//Call the launcher specific skip function.
			eNextLabel = LAUNCHER_SKIP_COMMS_COMMAND(sCurrentCommand)
		BREAK
		
		CASE FLOW_EMAIL_PLAYER
			LAUNCHER_DISPLAY_GENERIC_COMMAND_DEBUG_OUTPUT(paramStrand, sCurrentCommand.command, FALSE)
		
			//Call the launcher specific skip function.
			eNextLabel = LAUNCHER_SKIP_COMMS_COMMAND(sCurrentCommand)
		BREAK
		
		CASE FLOW_CANCEL_PHONECALL
			LAUNCHER_DISPLAY_GENERIC_COMMAND_DEBUG_OUTPUT(paramStrand, sCurrentCommand.command, FALSE)
		
			//Call flow command.
			eNextLabel = PERFORM_CANCEL_PHONECALL(sCurrentCommand.index)
		BREAK
		
		CASE FLOW_CANCEL_TEXT
			LAUNCHER_DISPLAY_GENERIC_COMMAND_DEBUG_OUTPUT(paramStrand, sCurrentCommand.command, FALSE)
		
			//Call flow command.
			eNextLabel = PERFORM_CANCEL_TEXT(sCurrentCommand.index)
		BREAK
		
		CASE FLOW_ADD_ORGANISER_EVENT
			LAUNCHER_DISPLAY_GENERIC_COMMAND_DEBUG_OUTPUT(paramStrand, sCurrentCommand.command, FALSE)
		
			//Call flow command.
			eNextLabel = PERFORM_ADD_ORGANISER_EVENT(sCurrentCommand.index)
		BREAK

		CASE FLOW_DO_BOARD_INTERACTION
			LAUNCHER_DISPLAY_GENERIC_COMMAND_DEBUG_OUTPUT(paramStrand, sCurrentCommand.command, TRUE)
		
			//Call launcher specific function.
			eNextLabel = LAUNCHER_SKIP_DO_BOARD_INTERACTION_COMMAND(flowLauncher, paramStrand, sCurrentCommand.index, paramGenerateFlowDiagram)
		BREAK
		
		CASE FLOW_IS_MISSION_COMPLETED
			LAUNCHER_DISPLAY_GENERIC_COMMAND_DEBUG_OUTPUT(paramStrand, sCurrentCommand.command, FALSE)
		
			//Follow the debug jump for this command.
			eNextLabel = INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.coreVars[sCurrentCommand.index].iValue4)
		BREAK
		
		CASE FLOW_IS_PLAYING_AS_CHARACTER
			LAUNCHER_DISPLAY_GENERIC_COMMAND_DEBUG_OUTPUT(paramStrand, sCurrentCommand.command, FALSE)
		
			//Follow the debug jump for this command.
			eNextLabel = INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.coreVars[sCurrentCommand.index].iValue4)
		BREAK
		
		CASE FLOW_IS_SWITCHING_TO_CHARACTER
			LAUNCHER_DISPLAY_GENERIC_COMMAND_DEBUG_OUTPUT(paramStrand, sCurrentCommand.command, FALSE)
		
			//Follow the debug jump for this command.
			eNextLabel = INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.coreVars[sCurrentCommand.index].iValue4)
		BREAK
		
		CASE FLOW_SET_DOOR_STATE
			LAUNCHER_DISPLAY_GENERIC_COMMAND_DEBUG_OUTPUT(paramStrand, sCurrentCommand.command, FALSE)
			
			//Call flow command.
			eNextLabel = PERFORM_SET_DOOR_STATE(sCurrentCommand.index)
		BREAK
		
		CASE FLOW_RESET_PRIORITY_SWITCH_SCENES
			LAUNCHER_DISPLAY_GENERIC_COMMAND_DEBUG_OUTPUT(paramStrand, sCurrentCommand.command, FALSE)
		
			//Call flow command.
			eNextLabel = PERFORM_RESET_PRIORITY_SWITCH_SCENES(sCurrentCommand.index)
		BREAK
		
		CASE FLOW_SET_PED_REQUEST_SCENE
			LAUNCHER_DISPLAY_GENERIC_COMMAND_DEBUG_OUTPUT(paramStrand, sCurrentCommand.command, FALSE)
		
			//Call flow command.
			eNextLabel = PERFORM_SET_PED_REQUEST_SCENE(sCurrentCommand.index)
		BREAK
		
		CASE FLOW_SET_CHAR_HOTSWAP_AVAILABILITY
			LAUNCHER_DISPLAY_GENERIC_COMMAND_DEBUG_OUTPUT(paramStrand, sCurrentCommand.command, FALSE)
		
			//Call flow command.
			eNextLabel = Perform_Set_Char_Hotswap_Availability(sCurrentCommand.index)
		BREAK
		
		CASE FLOW_SET_PLAYER_CHARACTER
			LAUNCHER_DISPLAY_GENERIC_COMMAND_DEBUG_OUTPUT(paramStrand, sCurrentCommand.command, TRUE)
		
			//Simply skip to the next command.
			eNextLabel = NEXT_SEQUENTIAL_COMMAND
		BREAK
		
		CASE FLOW_SET_SAVEHOUSE_STATE
			LAUNCHER_DISPLAY_GENERIC_COMMAND_DEBUG_OUTPUT(paramStrand, sCurrentCommand.command, FALSE)
		
			//Call flow command.
			eNextLabel = Perform_Set_Savehouse_State(sCurrentCommand.index)
		BREAK
		
		CASE FLOW_SET_SHOP_STATE
			LAUNCHER_DISPLAY_GENERIC_COMMAND_DEBUG_OUTPUT(paramStrand, sCurrentCommand.command, FALSE)
		
			//Call flow command.
			eNextLabel = Perform_Set_Shop_State(sCurrentCommand.index)
		BREAK
		
		CASE FLOW_SET_WEAPON_COMP_LOCK_STATE
			LAUNCHER_DISPLAY_GENERIC_COMMAND_DEBUG_OUTPUT(paramStrand, sCurrentCommand.command, FALSE)
		
			//Call flow command.
			eNextLabel = Perform_Set_Weapon_Comp_Lock_State(sCurrentCommand.index)
		BREAK
		
		CASE FLOW_SET_BOARD_DISPLAY_GROUP_VISIBILITY
			LAUNCHER_DISPLAY_GENERIC_COMMAND_DEBUG_OUTPUT(paramStrand, sCurrentCommand.command, FALSE)
		
			//Call flow command.
			eNextLabel = Perform_Set_Board_Display_Group_Visibility(sCurrentCommand.index)
		BREAK
		
		CASE FLOW_SET_VEHICLE_GEN_STATE
			LAUNCHER_DISPLAY_GENERIC_COMMAND_DEBUG_OUTPUT(paramStrand, sCurrentCommand.command, FALSE)
		
			//Call flow command.
			eNextLabel = Perform_Set_Vehicle_Gen_State(sCurrentCommand.index)
		BREAK
		
		CASE FLOW_SET_SPECIAL_ABILITY_UNLOCK_STATE
			LAUNCHER_DISPLAY_GENERIC_COMMAND_DEBUG_OUTPUT(paramStrand, sCurrentCommand.command, FALSE)
		
			//Call flow command.
			eNextLabel = Perform_Set_Special_Ability_Unlock_State(sCurrentCommand.index)
		BREAK
		
		CASE FLOW_SET_MISSION_PASSED
			LAUNCHER_DISPLAY_GENERIC_COMMAND_DEBUG_OUTPUT(paramStrand, sCurrentCommand.command, FALSE)
			
			//Call flow command.
			eNextLabel = Perform_Set_Mission_Passed(sCurrentCommand.index)
		BREAK
		
		CASE FLOW_SET_MISSION_NOT_PASSED
			LAUNCHER_DISPLAY_GENERIC_COMMAND_DEBUG_OUTPUT(paramStrand, sCurrentCommand.command, FALSE)
			
			//Call flow command.
			eNextLabel = Perform_Set_Mission_Not_Passed(sCurrentCommand.index)
		BREAK
		
		CASE FLOW_TOGGLE_BOARD_VIEWING
			LAUNCHER_DISPLAY_GENERIC_COMMAND_DEBUG_OUTPUT(paramStrand, sCurrentCommand.command, TRUE)
		
			//Simply skip to the next command.
			eNextLabel = NEXT_SEQUENTIAL_COMMAND
		BREAK
		
		CASE FLOW_IS_PLAYER_IN_AREA
			LAUNCHER_DISPLAY_GENERIC_COMMAND_DEBUG_OUTPUT(paramStrand, sCurrentCommand.command, TRUE)
			
			//Simply skip to the next command.
			eNextLabel = NEXT_SEQUENTIAL_COMMAND
		BREAK
		
		CASE FLOW_DO_PLAYER_BANK_ACTION
			LAUNCHER_DISPLAY_GENERIC_COMMAND_DEBUG_OUTPUT(paramStrand, sCurrentCommand.command, FALSE)
			
			//Call flow command.
			eNextLabel = PERFORM_DO_PLAYER_BANK_ACTION(sCurrentCommand.index)
		BREAK
		
		CASE FLOW_IS_SHRINK_SESSION_AVAILABLE
			LAUNCHER_DISPLAY_GENERIC_COMMAND_DEBUG_OUTPUT(paramStrand, sCurrentCommand.command, TRUE)
		
			//Simply skip to the next command.
			eNextLabel = NEXT_SEQUENTIAL_COMMAND
		BREAK
		
		CASE FLOW_DO_MISSION_AT_SWITCH
			LAUNCHER_DISPLAY_GENERIC_COMMAND_DEBUG_OUTPUT(paramStrand, sCurrentCommand.command, TRUE)
		
			//Simply skip to the next command.
			eNextLabel = LAUNCHER_SKIP_DO_MISSION_COMMAND(flowLauncher, paramStrand, sCurrentCommand.index, paramGeneratePlaythroughOrder, paramGenerateFlowDiagram)
		BREAK
		
		CASE FLOW_WAIT_FOR_AUTOSAVE
			LAUNCHER_DISPLAY_GENERIC_COMMAND_DEBUG_OUTPUT(paramStrand, sCurrentCommand.command, TRUE)
			
			//Simply skip to the next command.
			eNextLabel = NEXT_SEQUENTIAL_COMMAND
		BREAK
		
		CASE FLOW_SET_BIKES_SUPRESSED_STATE
			LAUNCHER_DISPLAY_GENERIC_COMMAND_DEBUG_OUTPUT(paramStrand, sCurrentCommand.command, FALSE)
			
			//Perform flow command.
			eNextLabel = PERFORM_SET_BIKES_SUPRESSED_STATE(sCurrentCommand.index)
		BREAK
		
		CASE FLOW_SET_FRANKLIN_OUTFIT_BITSET_STATE
			LAUNCHER_DISPLAY_GENERIC_COMMAND_DEBUG_OUTPUT(paramStrand, sCurrentCommand.command, FALSE)
			
			//Perform flow command.
			eNextLabel = PERFORM_SET_FRANKLIN_OUTFIT_BITSET_STATE(sCurrentCommand.index)
		BREAK
		
		CASE FLOW_FAST_TRACK_STRAND
			LAUNCHER_DISPLAY_GENERIC_COMMAND_DEBUG_OUTPUT(paramStrand, sCurrentCommand.command, TRUE)
			
			//Simply skip to the next command.
			eNextLabel = NEXT_SEQUENTIAL_COMMAND
		BREAK
		
		CASE FLOW_DO_CUSTOM_CHECK_JUMPS
			LAUNCHER_DISPLAY_GENERIC_COMMAND_DEBUG_OUTPUT(paramStrand, sCurrentCommand.command, TRUE)
			
			//Call the launcher specific skip function.
			eNextLabel = LAUNCHER_SKIP_DO_CUSTOM_CHECK_JUMPS(sCurrentCommand.index)
		BREAK
		
		CASE FLOW_SET_BUYABLE_VEHICLE_STATE
			LAUNCHER_DISPLAY_GENERIC_COMMAND_DEBUG_OUTPUT(paramStrand, sCurrentCommand.command, FALSE)
		
			//Call flow command.
			eNextLabel = PERFORM_SET_BUYABLE_VEHICLE_STATE(sCurrentCommand.index)
		BREAK
		
		
		CASE FLOW_SET_COMPONENT_ACQUIRED
			LAUNCHER_DISPLAY_GENERIC_COMMAND_DEBUG_OUTPUT(paramStrand, sCurrentCommand.command, FALSE)
		
			//Call flow command.
			eNextLabel = PERFORM_SET_COMPONENT_ACQUIRED(sCurrentCommand.index)
		BREAK
		
		CASE FLOW_DO_SWITCH_TO_CHARACTER
			LAUNCHER_DISPLAY_GENERIC_COMMAND_DEBUG_OUTPUT(paramStrand, sCurrentCommand.command, TRUE)
		
			//Call the launcher specific skip function.
			eNextLabel = LAUNCHER_SKIP_DO_CHARACTER_SWITCH(sCurrentCommand.index)
		BREAK
		
		CASE FLOW_SET_STRAND_SAVE_OVERRIDE
			LAUNCHER_DISPLAY_GENERIC_COMMAND_DEBUG_OUTPUT(paramStrand, sCurrentCommand.command, FALSE)
		
			//Call the launcher specific skip function.
			eNextLabel = PERFORM_SET_STRAND_SAVE_OVERRIDE(sCurrentCommand.index, paramStrand)
		BREAK
		
		CASE FLOW_CLEAR_STRAND_SAVE_OVERRIDE
			LAUNCHER_DISPLAY_GENERIC_COMMAND_DEBUG_OUTPUT(paramStrand, sCurrentCommand.command, FALSE)
		
			//Call the launcher specific skip function.
			eNextLabel = PERFORM_CLEAR_STRAND_SAVE_OVERRIDE(paramStrand)
		BREAK
		
		DEFAULT
			CERRORLN(DEBUG_FLOW_LAUNCHER, "Tried to process an unrecognised command in strand", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(paramStrand))
			eNextLabel = NEXT_SEQUENTIAL_COMMAND
		BREAK
	ENDSWITCH

	RETURN eNextLabel
	
ENDFUNC


