USING "vehicle_gen_public.sch"
USING "flow_mission_data_public.sch"
USING "selector_public.sch"
USING "flow_reset_game.sch"


PROC CONFIGURE_CHARACTER_MENU_TEXT(INT r, INT g, INT b, INT a)
	SET_TEXT_SCALE (0.38, 0.34)
	SET_TEXT_WRAP(0.0, 1.0)
	SET_TEXT_DROPSHADOW (0,0,0,0,255)
	SET_TEXT_COLOUR(r,g,b,a)
	SET_TEXT_EDGE (0,0,0,0,255)     
	SET_TEXT_PROPORTIONAL (FALSE)                                                 
	SET_TEXT_FONT(FONT_STANDARD)
ENDPROC


//PURPOSE:		Renders a small debug menu to the screen and allows a developer to select
//				which of main character to trigger a mission with. Uses the mission's
//				triggerable character bitset to limit the selection to the characters
//				available to the mission.
//
//PARAM NOTES:	paramMissionDataIndex - 	The mission's data array index.
//RETURNS:		The enum of the playable character selected to start the mission.
//
FUNC enumCharacterList LAUNCHER_DO_MISSION_CHARACTER_SELECTION(INT paramMissionDataIndex)

	//Get triggerable character bitset.
	INT iTriggerableCharBitset = g_sMissionStaticData[paramMissionDataIndex].triggerCharBitset
	
	//Get mission name label.
	TEXT_LABEL_7 tMissionName = GET_SP_MISSION_NAME_LABEL(INT_TO_ENUM(SP_MISSIONS, paramMissionDataIndex))
	
	CPRINTLN(DEBUG_FLOW_LAUNCHER, "Running mission character selection for ", GET_STRING_FROM_TEXT_FILE(tMissionName), ".")
	
	//State tracking variables.
	BOOL bMenuActive = TRUE
	INT iMenuTimeLastSelectionChange = 0
	INT iStickLeftY
	INT temp
	
	//Get screen aspect ratio.
	INT iScreenX
	INT iScreenY
	GET_SCREEN_RESOLUTION(iScreenX,iScreenY)
	FLOAT fAspectRatio = TO_FLOAT(iScreenY) / TO_FLOAT(iScreenX)
	
	//Store useful dimensions.
	FLOAT fMenuWidth = 0.175
	FLOAT fMenuRowHeight = 0.035
	FLOAT fMenuHeight = fMenuRowHeight*3.0
	FLOAT fMenuRowZeroY = 0.5 - (0.5*fMenuHeight) + (0.5*fMenuRowHeight)
	
	//Find valid starting selection and check if there is more than one character available.
	INT iMenuSelected = 0
	INT iValidCharCount = 0
	INT iCharIndex
	FOR iCharIndex = 2 TO 0 STEP -1
		IF Is_Mission_Triggerable_By_Character(iTriggerableCharBitset, INT_TO_ENUM(enumCharacterList, iCharIndex))
			IF NOT IS_BIT_SET(g_sMissionDebugData[paramMissionDataIndex].charBitsBlockDebugLaunch, iCharIndex)
				iValidCharCount++
				iMenuSelected = iCharIndex
			ENDIF
		ENDIF
	ENDFOR
	IF iValidCharCount = 0
		SCRIPT_ASSERT("LAUNCHER_DO_MISSION_CHARACTER_SELECTION: There didn't seem to be any valid triggerable characters for this mission. Tell BenR.")
	ELIF iValidCharCount = 1
		CPRINTLN(DEBUG_FLOW_LAUNCHER, "Only one character is available for this mission. Auto selected ", GET_CHARSHEET_DISPLAY_STRING_FROM_CHARSHEET(INT_TO_ENUM(enumCharacterList, iMenuSelected)), ".")
		RETURN INT_TO_ENUM(enumCharacterList, iMenuSelected)
	ENDIF
	
	//Start displaying help text.
	CLEAR_HELP()
	PRINT_HELP_FOREVER("CHAR_SEL_H")
	
	//Run menu.
	WHILE bMenuActive
	
		DISABLE_CELLPHONE_THIS_FRAME_ONLY()
		DISABLE_SELECTOR_THIS_FRAME()
	
		//Draw background layers.
		DRAW_RECT(0.5, 0.5 - fMenuRowHeight, fMenuWidth + (0.012*fAspectRatio), fMenuHeight + (fMenuRowHeight*2) + 0.012, 0,0,0,255)
		DRAW_RECT(0.5, 0.5, fMenuWidth, fMenuHeight , 10,10,10,255)
		//Draw selection highlight.
		IF iMenuSelected < 0 OR iMenuSelected > 2 OR NOT Is_Mission_Triggerable_By_Character(iTriggerableCharBitset, INT_TO_ENUM(enumCharacterList, iMenuSelected))
			// Disabled
			DRAW_RECT(0.5, fMenuRowZeroY + (fMenuRowHeight*TO_FLOAT(iMenuSelected)), fMenuWidth, fMenuRowHeight , 100, 100, 100, 100)
		ELSE
			// Enabled
			DRAW_RECT(0.5, fMenuRowZeroY + (fMenuRowHeight*TO_FLOAT(iMenuSelected)), fMenuWidth, fMenuRowHeight , 100, 100, 100, 255)
		ENDIF
		//Draw menu text.
		CONFIGURE_CHARACTER_MENU_TEXT(255,255,255,255)
		DISPLAY_TEXT(0.5 - (GET_STRING_WIDTH(tMissionName)*0.5), fMenuRowZeroY-(fMenuRowHeight*2)-0.015, tMissionName)
		CONFIGURE_CHARACTER_MENU_TEXT(255,255,255,255)
		DISPLAY_TEXT(0.5 - (GET_STRING_WIDTH("CHAR_SEL")*0.5), fMenuRowZeroY-fMenuRowHeight-0.015, "CHAR_SEL")
		enumCharacterList eChar
		REPEAT 3 iCharIndex
			eChar = INT_TO_ENUM(enumCharacterList, iCharIndex)
			IF Is_Mission_Triggerable_By_Character(iTriggerableCharBitset, eChar)
			AND NOT IS_BIT_SET(g_sMissionDebugData[paramMissionDataIndex].charBitsBlockDebugLaunch, ENUM_TO_INT(eChar))
				CONFIGURE_CHARACTER_MENU_TEXT(255, 255, 255, 255)
			ELSE
				CONFIGURE_CHARACTER_MENU_TEXT(150, 150, 150, 150)
			ENDIF
			STRING strCharText
			IF iCharIndex = ENUM_TO_INT(CHAR_MICHAEL)
				strCharText = "CHAR_SEL_M"
			ELIF iCharIndex = ENUM_TO_INT(CHAR_FRANKLIN)
				strCharText = "CHAR_SEL_F"
			ELIF iCharIndex = ENUM_TO_INT(CHAR_TREVOR)
				strCharText = "CHAR_SEL_T"
			ENDIF
			DISPLAY_TEXT(0.5-(GET_STRING_WIDTH(strCharText)*0.5), fMenuRowZeroY + (fMenuRowHeight*TO_FLOAT(iCharIndex))-0.015, strCharText)
		ENDREPEAT
		
		//Manage menu input.
		//Check for menu deactivation input.
		IF IS_BUTTON_PRESSED(PAD1, CROSS)
		OR IS_BUTTON_PRESSED(PAD1, CIRCLE)
		OR IS_BUTTON_PRESSED(PAD1, TRIANGLE)
		OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_RETURN)
		OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_ESCAPE)
		OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
			bMenuActive = FALSE
		ENDIF
		
		//Check for selection change input.
		IF (GET_GAME_TIMER() - iMenuTimeLastSelectionChange) > 250
			//Get analogue stick positons.
			GET_POSITION_OF_ANALOGUE_STICKS(PAD1,temp,iStickLeftY,temp,temp)
			//Check for up input.
			IF IS_BUTTON_PRESSED(PAD1, DPADUP)
			OR IS_KEYBOARD_KEY_PRESSED(KEY_UP)
			OR iStickLeftY < -30
				iMenuSelected--
				IF iMenuSelected < 0
					iMenuSelected = 2
				ENDIF
				WHILE NOT Is_Mission_Triggerable_By_Character(iTriggerableCharBitset, INT_TO_ENUM(enumCharacterList, iMenuSelected))
				OR IS_BIT_SET(g_sMissionDebugData[paramMissionDataIndex].charBitsBlockDebugLaunch, ENUM_TO_INT(iMenuSelected))
					iMenuSelected--
					IF iMenuSelected < 0
						iMenuSelected = 2
					ENDIF
				ENDWHILE
				iMenuTimeLastSelectionChange = GET_GAME_TIMER()
			ENDIF
			//Check for down input.
			IF IS_BUTTON_PRESSED(PAD1, DPADDOWN)
			OR IS_KEYBOARD_KEY_PRESSED(KEY_DOWN)
			OR iStickLeftY > 30
				iMenuSelected++
				IF iMenuSelected > 2
					iMenuSelected = 0
				ENDIF
				WHILE NOT Is_Mission_Triggerable_By_Character(iTriggerableCharBitset, INT_TO_ENUM(enumCharacterList, iMenuSelected))
				OR IS_BIT_SET(g_sMissionDebugData[paramMissionDataIndex].charBitsBlockDebugLaunch, ENUM_TO_INT(iMenuSelected))
					iMenuSelected++
					IF iMenuSelected > 2
						iMenuSelected = 0
					ENDIF
				ENDWHILE
				iMenuTimeLastSelectionChange = GET_GAME_TIMER()
			ENDIF
		ENDIF
		WAIT(0)
	ENDWHILE
	
	//Stop displaying help text.
	CLEAR_HELP(FALSE)
	
	//Menu selection has been made.
	CPRINTLN(DEBUG_FLOW_LAUNCHER, GET_CHARSHEET_DISPLAY_STRING_FROM_CHARSHEET(INT_TO_ENUM(enumCharacterList, iMenuSelected)), " was selected.")
	RETURN INT_TO_ENUM(enumCharacterList, iMenuSelected)
ENDFUNC


//PURPOSE:		Warps the player to a specific location on the map using a safe load.
//
//PARAM NOTES:	paramWarpPos - The location to warp to.
//
PROC LAUNCHER_WARP_TO_LAUNCH_LOCATION(VECTOR paramWarpPos)
	//Warp the player to a trigger location if necessary.
	CPRINTLN(DEBUG_FLOW_LAUNCHER, "Starting player map warp to mission blip: ", paramWarpPos)
	
	//Tell any trigger scenes to auto trigger as they are created.
	//This will be unflagged as the mission starts.
	g_bSceneAutoTrigger = TRUE
	
	g_flowUnsaved.bUpdatingGameflow = TRUE
	
	DO_PLAYER_MAP_WARP_WITH_LOAD(paramWarpPos)
	
	g_flowUnsaved.bUpdatingGameflow = FALSE
	CPRINTLN(DEBUG_FLOW_LAUNCHER, "Finished player map warp to mission blip.")
ENDPROC


//PURPOSE:		Converts a mission script hash to a flow label and then activates the flow to that label using
//				the main flow launcher algorithm.
//
//PARAM NOTES:	paramScriptHash - An INT hash value generated from a unique mission script name.
//
FUNC BOOL LAUNCHER_LAUNCH_TO_MISSION_SCRIPT_HASH(FLOW_LAUNCHER_VARS &flowLauncher, INT paramScriptHash, BOOL paramAutoTrigger, BOOL paramLaunchToBefore, BOOL paramManualCharSelect = TRUE)

	BOOL bDoWarp = FALSE
	VECTOR vWarpPos
	
	//Get the mission data index from the script hash.
	INT iMissionDataIndex = ENUM_TO_INT(GET_SP_MISSION_ID_FOR_SCRIPT_HASH(paramScriptHash, TRUE))
	
	IF iMissionDataIndex <> ENUM_TO_INT(SP_MISSION_NONE)
	
		RESET_REPLAY_CONTROLLER()
	
		IF paramAutoTrigger
			//Do character selection and swapping.
			enumCharacterList eTriggerChar
			IF paramManualCharSelect
				//Display selection menu to allow dev to manually select character.
				eTriggerChar = LAUNCHER_DO_MISSION_CHARACTER_SELECTION(iMissionDataIndex)
			ELSE
				//Pick the first character available on this missions.
				INT iCharIndex = 0
				WHILE NOT IS_BIT_SET(g_sMissionStaticData[iMissionDataIndex].triggerCharBitset, iCharIndex)
					iCharIndex++
					IF iCharIndex > ENUM_TO_INT(CHAR_TREVOR)	
						SCRIPT_ASSERT("LAUNCHER_LAUNCH_TO_MISSION_SCRIPT_HASH: Tried to automatically pick a starting character, but mission didn't have any valid characters set.")
					ENDIF
				ENDWHILE
				eTriggerChar = INT_TO_ENUM(enumCharacterList, iCharIndex)
			ENDIF
			SELECTOR_SLOTS_ENUM eSelectorTriggerChar
			SWITCH eTriggerChar
				CASE CHAR_MICHAEL
					eSelectorTriggerChar = SELECTOR_PED_MICHAEL
				BREAK
				CASE CHAR_FRANKLIN
					eSelectorTriggerChar = SELECTOR_PED_FRANKLIN
				BREAK
				CASE CHAR_TREVOR
					eSelectorTriggerChar = SELECTOR_PED_TREVOR
				BREAK
				DEFAULT
					SCRIPT_ASSERT("LAUNCHER_LAUNCH_TO_MISSION_SCRIPT_HASH: Character selection returned an invalid player character. Tell BenR.")
				BREAK	
			ENDSWITCH
			WHILE NOT SET_CURRENT_SELECTOR_PED(eSelectorTriggerChar)
				WAIT(0)
			ENDWHILE
			SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_DEFAULT, FALSE)
			STORE_PLAYER_PED_VARIATIONS(PLAYER_PED_ID(), TRUE)
			
			//If we want to auto trigger and this mission needs a warp then set up a blip warp.
			IF g_sMissionDebugData[iMissionDataIndex].doLaunchWarp
				//Check if this mission has an associated mission blip.
				//Move the player to a safe location so they don't trigger another mission
				//as the flow skips forward.
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					CPRINTLN(DEBUG_FLOW_LAUNCHER, "Warping player to safe location <<-106.6363, 7483.4497, 1.9928>> to avoid accidental triggers until after launch warp.")
					SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-106.6363, 7483.4497, 1.9928>>)
					IF IS_ENTITY_ATTACHED(PLAYER_PED_ID())
						DETACH_ENTITY(PLAYER_PED_ID())
					ENDIF
					FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
				ENDIF
			
				//If so, retrieve warp coordinates to trigger the mission with.
				IF NOT ARE_VECTORS_EQUAL(g_sMissionDebugData[iMissionDataIndex].warpPosition, <<0,0,0>>)
					// Mission has a specific debug warp blip set.
					vWarpPos = g_sMissionDebugData[iMissionDataIndex].warpPosition
				ELSE
					// No debug warp blip set. Warp to the main mission blip. Is it character specific?
					IF IS_BIT_SET(g_GameBlips[g_sMissionStaticData[iMissionDataIndex].blip].iSetting,STATIC_BLIP_SETTING_MULTI_COORD_AND_SPRITE)
						vWarpPos = g_GameBlips[g_sMissionStaticData[iMissionDataIndex].blip].vCoords[ENUM_TO_INT(eTriggerChar)]
					ELSE
						vWarpPos = g_GameBlips[g_sMissionStaticData[iMissionDataIndex].blip].vCoords[0]
					ENDIF
				ENDIF
				bDoWarp = TRUE
			ENDIF
		ENDIF

		//Check if this mission has a starting hour set.
		INT iStartHour = NULL_HOUR
		IF paramAutoTrigger AND NOT paramLaunchToBefore
			iStartHour = g_sMissionStaticData[iMissionDataIndex].startHour
		ENDIF
		
		//Get the target jump label for this mission.
		JUMP_LABEL_IDS eTargetLabel
		IF NOT paramAutoTrigger AND paramLaunchToBefore AND g_sMissionDebugData[iMissionDataIndex].beforeLaunchLabel != STAY_ON_THIS_COMMAND
			eTargetLabel = g_sMissionDebugData[iMissionDataIndex].beforeLaunchLabel
		ELSE
			eTargetLabel = g_sMissionDebugData[iMissionDataIndex].launchLabel
		ENDIF
		
		//If we have a valid label update the flow to this label.
		IF eTargetLabel <> STAY_ON_THIS_COMMAND
			LAUNCHER_LAUNCH_FLOW_TO_LABEL(flowLauncher, eTargetLabel, iStartHour)
			
			IF paramAutoTrigger
				IF bDoWarp
					LAUNCHER_WARP_TO_LAUNCH_LOCATION(vWarpPos)
				ENDIF
			ELSE
				//Switch to a character available at this point in the flow.
				SELECTOR_SLOTS_ENUM eCharToSwitchTo
				IF IS_PLAYER_PED_AVAILABLE(CHAR_MICHAEL)
					eCharToSwitchTo = SELECTOR_PED_MICHAEL
				ELIF IS_PLAYER_PED_AVAILABLE(CHAR_FRANKLIN)
					eCharToSwitchTo = SELECTOR_PED_FRANKLIN
				ELIF IS_PLAYER_PED_AVAILABLE(CHAR_TREVOR)
					eCharToSwitchTo = SELECTOR_PED_TREVOR
				ELSE
					SCRIPT_ASSERT("No available characters for given point in mission flow. Tell BenR.")
				ENDIF
				
				WHILE NOT SET_CURRENT_SELECTOR_PED(eCharToSwitchTo)
					WAIT(0)
				ENDWHILE
				SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_DEFAULT, FALSE)
				STORE_PLAYER_PED_VARIATIONS(PLAYER_PED_ID(), TRUE)
			ENDIF
			RESTORE_PLAYER_PED_WEAPONS(PLAYER_PED_ID()) // ensure player has his default weapon if unlocked
			
			CPRINTLN(DEBUG_FLOW_LAUNCHER, "Waiting for mission trigger to come available.")
			IF DOES_MISSION_HAVE_TRIGGER_SCENE(INT_TO_ENUM(SP_MISSIONS, iMissionDataIndex))
				WHILE GET_TRIGGER_INDEX_FOR_MISSION(INT_TO_ENUM(SP_MISSIONS, iMissionDataIndex)) = -1
					WAIT(0)
				ENDWHILE
			ENDIF
			CPRINTLN(DEBUG_FLOW_LAUNCHER, "Mission trigger registered.")
			
			//Unflag leave area flag now that the warp has completed.
			Set_Leave_Area_Flag_For_Mission(INT_TO_ENUM(SP_MISSIONS, iMissionDataIndex), FALSE)
			
			//Fast track building state updates.
			g_bBuildingScenarioBlocksUpdateThisFrame = TRUE
			
			RETURN TRUE
		ELSE
			CPRINTLN(DEBUG_FLOW_LAUNCHER, "Selected script hash has no label in the flow. Not updating the flow.")
			RETURN FALSE
		ENDIF
	ELSE
		CPRINTLN(DEBUG_FLOW_LAUNCHER, "Selected script did not have a mission data entry. Not updating the flow.")
		RETURN FALSE
	ENDIF
	
	//Should never be reached.
	RETURN FALSE
ENDFUNC


//PURPOSE:		Converts a mission script name to a flow label and then activates the flow to that label using
//				an appropriate flow launcher algorithm.
//
//PARAM NOTES:	paramScriptName - A text label containing the name of the script we want to activate to.
//
FUNC BOOL LAUNCHER_LAUNCH_TO_MISSION_SCRIPT(FLOW_LAUNCHER_VARS &flowLauncher, TEXT_LABEL_63 paramScriptName)
	
	//Get the hash for this script name.
	INT iScriptHash = GET_HASH_KEY(Remove_SC_From_Mission_Filename(paramScriptName))
	
	//Activate to this hash.
	RETURN LAUNCHER_LAUNCH_TO_MISSION_SCRIPT_HASH(flowLauncher, iScriptHash, TRUE, FALSE)
ENDFUNC


//PURPOSE:		Converts a mission script name to a flow label and then activates the flow to just before the mission
//				is triggered without actually triggering it.
//
//PARAM NOTES:	paramScriptName - A text label containing the name of the script we want to activate to.
//
FUNC BOOL LAUNCHER_LAUNCH_TO_BEFORE_MISSION_SCRIPT(FLOW_LAUNCHER_VARS &flowLauncher, TEXT_LABEL_63 paramScriptName)
	
	//Get the hash for this script name.
	INT iScriptHash = GET_HASH_KEY(Remove_SC_From_Mission_Filename(paramScriptName))
	
	//Activate to this hash.
	RETURN LAUNCHER_LAUNCH_TO_MISSION_SCRIPT_HASH(flowLauncher, iScriptHash, FALSE, TRUE)
ENDFUNC

//PURPOSE:		Converts a mission script name to a flow label and then activates the flow to that label and automatically
//				passes the mission.
//
//PARAM NOTES:	paramScriptName - A text label containing the name of the script we want to activate to.
//
FUNC BOOL LAUNCHER_LAUNCH_TO_AFTER_MISSION_SCRIPT(FLOW_LAUNCHER_VARS &flowLauncher, TEXT_LABEL_63 paramScriptName)

	//Get the hash for this script name.
	INT iScriptHash = GET_HASH_KEY(Remove_SC_From_Mission_Filename(paramScriptName))
	
	SP_MISSIONS eMission = GET_SP_MISSION_ID_FOR_SCRIPT_HASH(iScriptHash)
	
	//Activate to this hash.
	BOOL bLaunched = LAUNCHER_LAUNCH_TO_MISSION_SCRIPT_HASH(flowLauncher, iScriptHash, FALSE, FALSE)
	SET_MISSION_COMPLETE_STATE(eMission, TRUE)
	#if USE_CLF_DLC
		UpdateLastKnownPedInfoPostMission(g_savedGlobalsClifford.sPlayerData.sInfo, eMission)
	#endif
	#if USE_NRM_DLC
		UpdateLastKnownPedInfoPostMission(g_savedGlobalsnorman.sPlayerData.sInfo, eMission)
	#endif
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
		UpdateLastKnownPedInfoPostMission(g_savedGlobals.sPlayerData.sInfo, eMission)
	#endif
	#endif
	
	RETURN bLaunched
ENDFUNC

