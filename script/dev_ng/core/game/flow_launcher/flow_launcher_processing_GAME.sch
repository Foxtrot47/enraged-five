//╒═════════════════════════════════════════════════════════════════════════════╕
//│				Author:  Ben Rollinson					Date: 27/11/11			│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│				Flow Launcher Processing GTA5 Specific Header					│
//│																				│
//│		DESCRIPTION: GTA5 specific procedures and functions for the Flow 		│
//│		Launcher system.														│
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛

#IF IS_DEBUG_BUILD

USING "rage_builtins.sch"
USING "globals.sch"

USING "flow_launcher_processing.sch"
USING "flow_reset_GAME.sch"
#if  USE_CLF_DLC
using "flow_custom_checks_CLF.sch"
#endif

#if  USE_NRM_DLC
using "flow_custom_checks_NRM.sch"
#endif

#if not USE_CLF_DLC
#if not USE_NRM_DLC
USING "script_heist.sch"
USING "flow_custom_checks_GTA5.sch"
#endif
#endif
USING "comms_control_data_GTA5.sch"
USING "email_public.sch"

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════╡ Command Specific Skipping Functions ╞═══════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

//PURPOSE:		A command used when processing any phone or text player flow commands within the flow launcher.
//
//PARAM NOTES:	iCommandIndex - The g_flowUnsaved.phonecallVars[] array index at which the phone or text player command is located.
//
FUNC JUMP_LABEL_IDS LAUNCHER_SKIP_COMMS_COMMAND(FLOW_COMMANDS paramCommand)
	//Ensure the Comms Vars Array position is legal.
	IF (paramCommand.index = ILLEGAL_ARRAY_POSITION)
		SCRIPT_ASSERT("LAUNCHER_SKIP_COMMS_COMMAND: The Phonecall Variables Array Position parameter is illegal. Skipping to next command.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	#if USE_CLF_DLC
		//Check if this command has a code ID associated with it.
		IF g_flowUnsaved.commsVars[paramCommand.index].eCodeID <> CID_BLANK
		AND g_flowUnsaved.commsVars[paramCommand.index].eCodeID <> CID_CLF_MAX
			//Set the code ID to execute with no delay.
			EXECUTE_CODE_ID(g_flowUnsaved.commsVars[paramCommand.index].eCodeID, 0)
		ENDIF
	#endif
	#if USE_NRM_DLC
		//Check if this command has a code ID associated with it.
		IF g_flowUnsaved.commsVars[paramCommand.index].eCodeID <> CID_BLANK
		AND g_flowUnsaved.commsVars[paramCommand.index].eCodeID <> CID_NRM_MAX
			//Set the code ID to execute with no delay.
			EXECUTE_CODE_ID(g_flowUnsaved.commsVars[paramCommand.index].eCodeID, 0)
		ENDIF
	#endif
	
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
		//Check if this command has a code ID associated with it.
		IF g_flowUnsaved.commsVars[paramCommand.index].eCodeID <> CID_BLANK
		AND g_flowUnsaved.commsVars[paramCommand.index].eCodeID <> CID_MAX
			//Set the code ID to execute with no delay.
			EXECUTE_CODE_ID(g_flowUnsaved.commsVars[paramCommand.index].eCodeID, 0)
		ENDIF
	#endif
	#endif
	
	
	IF paramCommand.command = FLOW_EMAIL_PLAYER
		//Rebuild the email thread enum for this communication.
		CC_CommID eEmailID = INT_TO_ENUM(CC_CommID, g_flowUnsaved.commsVars[paramCommand.index].iValue1)
		EMAIL_THREAD_ENUMS eEmailThread
		PRIVATE_Get_Email_Thread(eEmailID, eEmailThread)
	
		CDEBUG3LN(DEBUG_FLOW_LAUNCHER, "Progressing email thread ", ENUM_TO_INT(eEmailThread), ".")
	
		IF IS_STATIC_EMAIL_THREAD_ACTIVE(eEmailThread)
			IF NOT IS_STATIC_EMAIL_THREAD_ENDED(eEmailThread)
				PROGRESS_EMAIL_THREAD(ENUM_TO_INT(eEmailThread))
			ELSE
				SCRIPT_ASSERT("LAUNCHER_SKIP_COMMUNICATION_COMMAND: Tried to progress an email thread that had ended.")
			ENDIF
		ELSE
			BEGIN_EMAIL_THREAD(eEmailThread)
		ENDIF
	ENDIF
	
	//Else step to the next command.
	RETURN NEXT_SEQUENTIAL_COMMAND
ENDFUNC


//PURPOSE:		A command used when processing any phone or text player flow commands within the flow launcher.
//
//PARAM NOTES:	iCommandIndex - The g_flowUnsaved.phonecallVars[] array index at which the phone or text player command is located.
//
FUNC JUMP_LABEL_IDS LAUNCHER_SKIP_COMMS_LARGE_COMMAND(FLOW_COMMANDS paramCommand)
	//Ensure the Comms Large Vars Array position is legal.
	IF (paramCommand.index = ILLEGAL_ARRAY_POSITION)
		SCRIPT_ASSERT("LAUNCHER_SKIP_COMMS_COMMAND: The Phonecall Variables Array Position parameter is illegal. Skipping to next command.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	#if USE_CLF_DLC
		//Check if this command has a code ID associated with it.
		IF g_flowUnsaved.commsLargeVars[paramCommand.index].eCodeID <> CID_BLANK
		AND g_flowUnsaved.commsLargeVars[paramCommand.index].eCodeID <> CID_CLF_MAX
			//Set the code ID to execute with no delay.
			EXECUTE_CODE_ID(g_flowUnsaved.commsLargeVars[paramCommand.index].eCodeID, 0)
		ENDIF
	#endif
	#if USE_NRM_DLC
		//Check if this command has a code ID associated with it.
		IF g_flowUnsaved.commsLargeVars[paramCommand.index].eCodeID <> CID_BLANK
		AND g_flowUnsaved.commsLargeVars[paramCommand.index].eCodeID <> CID_NRM_MAX
			//Set the code ID to execute with no delay.
			EXECUTE_CODE_ID(g_flowUnsaved.commsLargeVars[paramCommand.index].eCodeID, 0)
		ENDIF
	#endif
	
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
		//Check if this command has a code ID associated with it.
		IF g_flowUnsaved.commsLargeVars[paramCommand.index].eCodeID <> CID_BLANK
		AND g_flowUnsaved.commsLargeVars[paramCommand.index].eCodeID <> CID_MAX
			//Set the code ID to execute with no delay.
			EXECUTE_CODE_ID(g_flowUnsaved.commsLargeVars[paramCommand.index].eCodeID, 0)
		ENDIF
	#endif
	#endif
	
	
	
	IF paramCommand.command = FLOW_PLAYER_PHONE_NPC_WITH_BRANCH
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	//Follow the debug jump label.
	RETURN g_flowUnsaved.commsLargeVars[paramCommand.index].eJump3
ENDFUNC


FUNC JUMP_LABEL_IDS LAUNCHER_SKIP_IS_PHONING_NPC_COMMAND(INT paramCommandIndex)
	//Ensure the Phonecall Vars Array position is legal.
	IF (paramCommandIndex = ILLEGAL_ARRAY_POSITION)
		SCRIPT_ASSERT("LAUNCHER_SKIP_IS_PHONING_NPC_COMMAND: The Comms Variables Array Position parameter is illegal. Skipping to next command.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	//Is a debug jump label set?
	JUMP_LABEL_IDS eDebugJump = INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.commsVars[paramCommandIndex].iValue5)
	IF eDebugJump != STAY_ON_THIS_COMMAND
		RETURN eDebugJump
	ENDIF
	
	//No then follow the yes jump.
	RETURN INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.commsVars[paramCommandIndex].iValue3)
ENDFUNC


//PURPOSE:		A command used when processing any 'add new phone contact' flow commands within the flow launcher.
//
//PARAM NOTES:	paramCommandIndex - The g_flowUnsaved.phonecallVars[] array index at which the 'add new phone contact' command is located.
//
FUNC JUMP_LABEL_IDS LAUNCHER_SKIP_ADD_PHONE_CONTACT(INT paramCommandIndex)

	//Ensure the Phonecall Vars Array position is legal.
	IF (paramCommandIndex = ILLEGAL_ARRAY_POSITION)
		SCRIPT_ASSERT("LAUNCHER_SKIP_ADD_PHONE_CONTACT: The Core Variables Array Position parameter is illegal. Skipping to next command.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	enumCharacterList eCharOwner = INT_TO_ENUM(enumCharacterList, g_flowUnsaved.coreVars[paramCommandIndex].iValue1)
	enumCharacterList eCharToAdd = INT_TO_ENUM(enumCharacterList, g_flowUnsaved.coreVars[paramCommandIndex].iValue2)
	
	//Work out phonebook presence from character enum.
	enumPhonebookPresence ePhonebookPresence
	SWITCH eCharOwner
		CASE CHAR_MICHAEL
			ePhonebookPresence = MICHAEL_BOOK
		BREAK
		CASE CHAR_FRANKLIN
			ePhonebookPresence = FRANKLIN_BOOK
		BREAK
		CASE CHAR_TREVOR
			ePhonebookPresence = TREVOR_BOOK
		BREAK
		DEFAULT
			SCRIPT_ASSERT("LAUNCHER_SKIP_ADD_PHONE_CONTACT: The phone owner parameter did not point to a playable character. MOVING ON TO NEXT COMMAND.")
		BREAK
	ENDSWITCH
	
	//Add the contact to the phonebook but don't display the onscreen notification.
	ADD_CONTACT_TO_PHONEBOOK(	eCharToAdd,
								ePhonebookPresence,
								FALSE)
	
	//We're done. Step to the next command.
	RETURN NEXT_SEQUENTIAL_COMMAND
ENDFUNC


FUNC JUMP_LABEL_IDS LAUNCHER_SKIP_DISPLAY_HELP_TEXT(INT paramCommandIndex)
	
	//Ensure the Phonecall Vars Array position is legal.
	IF (paramCommandIndex = ILLEGAL_ARRAY_POSITION)
		SCRIPT_ASSERT("LAUNCHER_SKIP_DISPLAY_HELP_TEXT: The Text Large Variables Array Position parameter is illegal. Skipping to next command.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	//Does this help text have a code ID assigned to it?
	CC_CodeID eCodeID = INT_TO_ENUM(CC_CodeID, g_flowUnsaved.textLargeVars[paramCommandIndex].iValue4)
	IF eCodeID != CID_BLANK
		//Yes, execute it.
		Execute_Code_ID(eCodeID, 0)
	ENDIF
	
	//We're done. Step to next command.
	RETURN NEXT_SEQUENTIAL_COMMAND
ENDFUNC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═════════════╡ Launcher GTA5 Specific Debug Display Functions ╞══════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛ 

//PURPOSE:		Display a debug line for a 'set static blip state' command that is being processed by the flow launcher.
//
//PARAM NOTES:	paramStrand - The strand in which the command is found.
//				paramCommand - The flow command struct of the command that is being processed.
//
PROC LAUNCHER_DISPLAY_SET_STATIC_BLIP_STATE_COMMAND_DEBUG_OUTPUT(STRANDS paramStrand, FLOW_COMMANDS paramCommand)
	#IF IS_DEBUG_BUILD
	CDEBUG1LN(DEBUG_FLOW_LAUNCHER, GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(paramStrand), ": Performing '", GET_COMMAND_DISPLAY_STRING_FROM_COMMAND_ID(paramCommand.command), "(", DEBUG_GET_STRING_NAME_OF_STATIC_BLIP(ENUM_TO_INT(g_flowUnsaved.miscellaneousVars[paramCommand.index].blipID)), ")'")
	#ENDIF
ENDPROC



//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═════════════╡ Launcher GTA5 Specific Launcher Sub-functions ╞═══════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛ 

//PURPOSE:		Gets the flow label that is associated with a GTA5 SP mission ID. This will almost always be the first label
//				preceding the DO_MISSION_ command that triggers the script.
//
FUNC JUMP_LABEL_IDS LAUNCHER_GET_LABEL_FROM_GTA5_MISSION_ID(SP_MISSIONS paramMissionID)
	#if USE_CLF_DLC
		IF paramMissionID = SP_MISSION_NONE OR paramMissionID = SP_MISSION_MAX_CLF
			SCRIPT_ASSERT("LAUNCHER_GET_LABEL_FROM_GTA5_MISSION_ID: Invalid mission ID passed. SP_MISSION_NONE and SP_MISSION_MAX_CLF are not valid.")
			RETURN STAY_ON_THIS_COMMAND
		ENDIF
	#endif
	#if USE_NRM_DLC
		IF paramMissionID = SP_MISSION_NONE OR paramMissionID = SP_MISSION_MAX_NRM
			SCRIPT_ASSERT("LAUNCHER_GET_LABEL_FROM_GTA5_MISSION_ID: Invalid mission ID passed. SP_MISSION_NONE and SP_MISSION_MAX_NRM are not valid.")
			RETURN STAY_ON_THIS_COMMAND
		ENDIF
	#endif
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
		IF paramMissionID = SP_MISSION_NONE OR paramMissionID = SP_MISSION_MAX
			SCRIPT_ASSERT("LAUNCHER_GET_LABEL_FROM_GTA5_MISSION_ID: Invalid mission ID passed. SP_MISSION_NONE and SP_MISSION_MAX are not valid.")
			RETURN STAY_ON_THIS_COMMAND
		ENDIF
	#endif
	#endif
	

	RETURN g_sMissionDebugData[paramMissionID].launchLabel
ENDFUNC


//PURPOSE:		Gets the static blip that is associated with a GTA5 SP mission ID.
//
FUNC STATIC_BLIP_NAME_ENUM LAUNCHER_GET_CONTACT_POINT_FROM_GTA5_MISSION_ID(SP_MISSIONS paramMissionID)
	
	#if USE_CLF_DLC
		IF paramMissionID = SP_MISSION_NONE OR paramMissionID = SP_MISSION_MAX_CLF
			SCRIPT_ASSERT("LAUNCHER_GET_CONTACT_POINT_FROM_GTA5_MISSION_ID: Invalid mission ID passed. SP_MISSION_NONE and SP_MISSION_MAX_CLF are not valid.")
			RETURN STAY_ON_THIS_COMMAND
		ENDIF
	#endif
	#if USE_NRM_DLC
		IF paramMissionID = SP_MISSION_NONE OR paramMissionID = SP_MISSION_MAX_NRM
			SCRIPT_ASSERT("LAUNCHER_GET_CONTACT_POINT_FROM_GTA5_MISSION_ID: Invalid mission ID passed. SP_MISSION_NONE and SP_MISSION_MAX_CLF are not valid.")
			RETURN STAY_ON_THIS_COMMAND
		ENDIF
	#endif
	
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
		IF paramMissionID = SP_MISSION_NONE OR paramMissionID = SP_MISSION_MAX
			SCRIPT_ASSERT("LAUNCHER_GET_CONTACT_POINT_FROM_GTA5_MISSION_ID: Invalid mission ID passed. SP_MISSION_NONE and SP_MISSION_MAX are not valid.")
			RETURN STAY_ON_THIS_COMMAND
		ENDIF
	#endif	
	#endif


	RETURN g_sMissionStaticData[paramMissionID].blip
ENDFUNC


//PURPOSE:		Gets the static blip that is associated with a mission script.
//
FUNC STATIC_BLIP_NAME_ENUM LAUNCHER_GET_CONTACT_POINT_FROM_GTA5_SCRIPT_NAME(TEXT_LABEL_63 paramScriptName, BOOL paramSupressAssert = FALSE)
	SP_MISSIONS eMissionID = GET_SP_MISSION_ID_FOR_SCRIPT_NAME(paramScriptName, paramSupressAssert)
	
	#if USE_CLF_DLC
		IF eMissionID = SP_MISSION_NONE OR eMissionID = SP_MISSION_MAX_CLF
			IF NOT paramSupressAssert
				CERRORLN(DEBUG_FLOW, "LAUNCHER_GET_CONTACT_POINT_FROM_GTA5_SCRIPT_NAME: Failed to find a contact point blip associated with script \"", paramScriptName, "\".")
				SCRIPT_ASSERT("LAUNCHER_GET_CONTACT_POINT_FROM_GTA5_SCRIPT_NAME: Failed to find a valid contact point for mission script as there was no data entry found.")
			ENDIF
			RETURN STATIC_BLIP_NAME_DUMMY_FINAL
		ENDIF
	#endif
	#if USE_NRM_DLC
		IF eMissionID = SP_MISSION_NONE OR eMissionID = SP_MISSION_MAX_NRM
			IF NOT paramSupressAssert
				CERRORLN(DEBUG_FLOW, "LAUNCHER_GET_CONTACT_POINT_FROM_GTA5_SCRIPT_NAME: Failed to find a contact point blip associated with script \"", paramScriptName, "\".")
				SCRIPT_ASSERT("LAUNCHER_GET_CONTACT_POINT_FROM_GTA5_SCRIPT_NAME: Failed to find a valid contact point for mission script as there was no data entry found.")
			ENDIF
			RETURN STATIC_BLIP_NAME_DUMMY_FINAL
		ENDIF
	#endif
	
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
		IF eMissionID = SP_MISSION_NONE OR eMissionID = SP_MISSION_MAX
			IF NOT paramSupressAssert
				CERRORLN(DEBUG_FLOW, "LAUNCHER_GET_CONTACT_POINT_FROM_GTA5_SCRIPT_NAME: Failed to find a contact point blip associated with script \"", paramScriptName, "\".")
				SCRIPT_ASSERT("LAUNCHER_GET_CONTACT_POINT_FROM_GTA5_SCRIPT_NAME: Failed to find a valid contact point for mission script as there was no data entry found.")
			ENDIF
			RETURN STATIC_BLIP_NAME_DUMMY_FINAL
		ENDIF
	#endif	
	#endif
	
	
	
	RETURN g_sMissionStaticData[eMissionID].blip
ENDFUNC


//PURPOSE:		A procedure that will update flow bitset states in a way that simulates the player selecting a heist
//				gameplay choice via a heist planning board. This is used by the flow launcher when skipping through a heist
//				strand.
//
//PARAM NOTES:	paramHeist -	The index of the heist for which we want to make a gameplay choice.

PROC LAUNCHER_SIMULATE_HEIST_GAMEPLAY_CHOICE_SELECTION(INT paramHeist)
#if USE_CLF_DLC
paramHeist = paramHeist
#endif
#if USE_NRM_DLC
paramHeist = paramHeist
#endif

#if not USE_CLF_DLC
#if not USE_NRM_DLC	
	CDEBUG1LN(DEBUG_FLOW_LAUNCHER, "Simulating planning board gameplay choice selection.")

	//Ensure the heist index is legal.
	IF (paramHeist < 0)
	OR (paramHeist >= NO_HEISTS)
		SCRIPT_ASSERT("LAUNCHER_SIMULATE_HEIST_GAMEPLAY_CHOICE_SELECTION: The heist index is illegal.")
	ENDIF
	
	//Set which flow INT we should save the gameplay choice into.
	FLOW_INT_IDS eHeistChoiceInt = Get_Heist_Choice_FlowInt_ID(paramHeist)

	//Check if we have a debug choice saved in globals for this choice.
	IF g_iDebugChoiceSelection[paramHeist] != HEIST_CHOICE_EMPTY
		CDEBUG1LN(DEBUG_FLOW_LAUNCHER, "Auto choosing gameplay choice ", g_iDebugChoiceSelection[paramHeist], " for heist ", paramHeist)

		//Set this choice based on the stored global value.
		Set_Mission_Flow_Int_Value(eHeistChoiceInt, g_iDebugChoiceSelection[paramHeist])
		
		g_iDebugChoiceSelection[paramHeist] = HEIST_CHOICE_EMPTY
	ELSE
		//Default the choice to the first choice in this heist's slots.
		INT iChoice = Get_Choice_In_Heist_Choice_Slot(paramHeist,0)
		Set_Mission_Flow_Int_Value(eHeistChoiceInt, iChoice)
	
		CDEBUG1LN(DEBUG_FLOW_LAUNCHER, "No choice to be made for heist ", paramHeist, ". Defaulting to first available choice for this heist which is ", iChoice)
	ENDIF
#endif
#endif
ENDPROC

//PURPOSE:		A procedure that will update flow bitset states in a way that simulates the player selecting a crew
//				via a heist planning board. This is used by the flow launcher when skipping through a heist
//				strand.
//
//PARAM NOTES:	paramHeist -  The index of the heist for which we want to select a crew.
//
PROC LAUNCHER_SIMULATE_HEIST_CREW_SELECTION(INT paramHeist)
#if USE_CLF_DLC
paramHeist = paramHeist
#endif
#if USE_NRM_DLC
paramHeist = paramHeist
#endif

#if not USE_CLF_DLC
#if not USE_NRM_DLC	
	CDEBUG1LN(DEBUG_FLOW_LAUNCHER, "Simulating planning board crew selection.")

	//Ensure the heist bit is legal.
	IF (paramHeist < 0)
	OR (paramHeist >= 32)
		SCRIPT_ASSERT("LAUNCHER_SIMULATE_HEIST_CREW_SELECTION: The heist index is illegal.")
	ENDIF
	
	//Generate a random debug crew for this heist choice.
	DEBUG_SET_RANDOM_CREW_FOR_HEIST(paramHeist)
	
	//If this heist has been flagged to manually select a crew, run the selector.
	IF IS_BIT_SET(g_iDebugHeistDoCrewSelection, paramHeist)
		DEBUG_DO_CREW_SELECTION_FOR_HEIST(paramHeist)
		CLEAR_BIT(g_iDebugHeistDoCrewSelection, paramHeist)
	ENDIF
#endif
#endif
ENDPROC


//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═══════════╡ Launcher GTA5 Specific Command Skipping Functions ╞═════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC LAUNCHER_TERMINATE_SCRIPT_WITH_NAME(STRING paramScriptName)
	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY(paramScriptName)) > 0
		SCRIPT_THREAD_ITERATOR_RESET()
		BOOL bThreadTerminated = FALSE
		THREADID threadTemp = SCRIPT_THREAD_ITERATOR_GET_NEXT_THREAD_ID()
		
		WHILE (NOT bThreadTerminated) AND (threadTemp != NULL)
			IF ARE_STRINGS_EQUAL(GET_NAME_OF_SCRIPT_WITH_THIS_ID(threadTemp),paramScriptName)
				CPRINTLN(DEBUG_FLOW_LAUNCHER, "Laucher terminating script ", paramScriptName, ".")
				TERMINATE_THREAD(threadTemp)
				bThreadTerminated = TRUE
			ELSE
				threadTemp = SCRIPT_THREAD_ITERATOR_GET_NEXT_THREAD_ID()
			ENDIF
		ENDWHILE
	ENDIF
ENDPROC

#if USE_CLF_DLC
PROC LAUNCHER_UPDATE_GAME_STATE_FOR_SKIPPED_MISSION_CLF(SP_MISSIONS paramMissionID)
	SWITCH paramMissionID
		CASE SP_MISSION_CLF_TRAIN
			//Nothing to do yet...
		BREAK					
	ENDSWITCH
ENDPROC
#ENDIF
#if USE_NRM_DLC
PROC LAUNCHER_UPDATE_GAME_STATE_FOR_SKIPPED_MISSION_NRM(SP_MISSIONS paramMissionID)
	SWITCH paramMissionID
		CASE SP_MISSION_NRM_SUR_START	
		CASE SP_MISSION_NRM_SUR_AMANDA
		CASE SP_MISSION_NRM_SUR_TRACEY	
		CASE SP_MISSION_NRM_SUR_MICHAEL
		CASE SP_MISSION_NRM_SUR_HOME
		CASE SP_MISSION_NRM_SUR_JIMMY
		CASE SP_MISSION_NRM_SUR_PARTY
		CASE SP_MISSION_NRM_SUR_CURE  	
		
		CASE SP_MISSION_NRM_RESCUE_ENG	
		CASE SP_MISSION_NRM_RESCUE_MED	
		CASE SP_MISSION_NRM_RESCUE_GUN	
		
		CASE SP_MISSION_NRM_SUP_FUEL		
		CASE SP_MISSION_NRM_SUP_AMMO		
		CASE SP_MISSION_NRM_SUP_MEDS		
		CASE SP_MISSION_NRM_SUP_FOOD	
		
		CASE SP_MISSION_NRM_RADIO_A		
		CASE SP_MISSION_NRM_RADIO_B		
		CASE SP_MISSION_NRM_RADIO_C
			//Nothing to do yet...
		BREAK					
	ENDSWITCH
ENDPROC
#ENDIF

#IF NOT USE_CLF_DLC
#IF NOT USE_NRM_DLC

PROC LAUNCHER_UPDATE_GAME_STATE_FOR_SKIPPED_MISSION(SP_MISSIONS paramMissionID)

	SWITCH paramMissionID
		CASE SP_MISSION_ARMENIAN_1
			CPRINTLN(DEBUG_FLOW_LAUNCHER, "Updating game state while skipping over SP_MISSION_ARMENIAN_1.")
			SET_ONE_TIME_HELP_MESSAGE_DISPLAYED(FHM_FIRST_MISSION_PASS)
		BREAK
		
		CASE SP_MISSION_ARMENIAN_2
			CPRINTLN(DEBUG_FLOW_LAUNCHER, "Updating game state while skipping over SP_MISSION_ARMENIAN_2.")
			SET_ONE_TIME_HELP_MESSAGE_DISPLAYED(FHM_FIRST_CALL_RINGING)
			SET_ONE_TIME_HELP_MESSAGE_DISPLAYED(FHM_FIRST_CALL_ONGOING)
			SET_ONE_TIME_HELP_MESSAGE_DISPLAYED(FHM_FIRST_RC_BLIP)
			SET_ONE_TIME_HELP_MESSAGE_DISPLAYED(FHM_MULTI_WEAPONS_IN_SLOT)
			SET_ONE_TIME_HELP_MESSAGE_DISPLAYED(FHM_CONVERTIBLE_ROOF)
			SET_ONE_TIME_HELP_MESSAGE_DISPLAYED(FHM_HEADLIGHTS)
			SET_ONE_TIME_HELP_MESSAGE_DISPLAYED(FHM_VEHICLE_RADIO_A)
			SET_ONE_TIME_HELP_MESSAGE_DISPLAYED(FHM_VEHICLE_RADIO_B)
		BREAK
		
		CASE SP_MISSION_ARMENIAN_3
			CPRINTLN(DEBUG_FLOW_LAUNCHER, "Updating game state while skipping over SP_MISSION_ARMENIAN_3.")
			SET_ONE_TIME_HELP_MESSAGE_DISPLAYED(FHM_FIRST_TEXT_RECEIVED)
			SET_ONE_TIME_HELP_MESSAGE_DISPLAYED(FHM_SOCIAL_CLUB_LINK)
			SET_HEIST_CREW_MEMBER_UNLOCKED(CM_GUNMAN_G_PACKIE_UNLOCK)
		BREAK
		
		CASE SP_MISSION_FRANKLIN_0
			CPRINTLN(DEBUG_FLOW_LAUNCHER, "Updating game state while skipping over SP_MISSION_FRANKLIN_0.")
			SET_ONE_TIME_HELP_MESSAGE_DISPLAYED(FHM_NO_STAMINA)
		BREAK
		
		CASE SP_MISSION_FAMILY_1
			CPRINTLN(DEBUG_FLOW_LAUNCHER, "Updating game state while skipping over SP_MISSION_FAMILY_1.")
			SET_ONE_TIME_HELP_MESSAGE_DISPLAYED(FHM_SELECTOR_MISSIONS_A)
			SET_ONE_TIME_HELP_MESSAGE_DISPLAYED(FHM_SELECTOR_MISSIONS_B)
			LAUNCHER_TERMINATE_SCRIPT_WITH_NAME("SH_Intro_M_Home")
			REMOVE_SCRIPT_FROM_RELAUNCH_LIST(LAUNCH_BIT_SH_INTRO_M_HOME)
			CPRINTLN(DEBUG_FLOW_LAUNCHER, "Set mission count, selector call, and no swap help text as shown.")
		BREAK
		
		CASE SP_MISSION_EXILE_1
			CPRINTLN(DEBUG_FLOW_LAUNCHER, "Updating game state while skipping over SP_MISSION_EXILE_1.")
			LAUNCHER_TERMINATE_SCRIPT_WITH_NAME("SH_Intro_F_Hills")
			REMOVE_SCRIPT_FROM_RELAUNCH_LIST(LAUNCH_BIT_SH_INTRO_F_HILLS)
		BREAK
		
		CASE SP_MISSION_TREVOR_1
			CPRINTLN(DEBUG_FLOW_LAUNCHER, "Updating game state while skipping over SP_MISSION_TREVOR_1.")
			SET_HEIST_CREW_MEMBER_UNLOCKED(CM_DRIVER_G_TALINA_UNLOCK)
		BREAK
		
		CASE SP_MISSION_CHINESE_1
			CPRINTLN(DEBUG_FLOW_LAUNCHER, "Updating game state while skipping over SP_MISSION_CHINESE_1.")
			SET_HEIST_CREW_MEMBER_UNLOCKED(CM_DRIVER_G_TALINA_UNLOCK)
		BREAK
		
		CASE SP_MISSION_TREVOR_2
			CPRINTLN(DEBUG_FLOW_LAUNCHER, "Updating game state while skipping over SP_MISSION_TREVOR_2.")
			g_savedGlobals.sTraffickingData.bAirBlipActive = TRUE
			SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_TRAF_AIR, TRUE)
			CPRINTLN(DEBUG_FLOW_LAUNCHER, "Set air trafficking blip active without help.")
			g_savedGlobals.sTraffickingData.bGndBlipActive = TRUE
			SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_TRAF_GND, TRUE)
			CPRINTLN(DEBUG_FLOW_LAUNCHER, "Set ground trafficking blip active without help.")
		BREAK
		
		CASE SP_MISSION_TREVOR_3
			CPRINTLN(DEBUG_FLOW_LAUNCHER, "Updating game state while skipping over SP_MISSION_TREVOR_3")
			SET_ONE_TIME_HELP_MESSAGE_DISPLAYED(FHM_SELECTOR_NO_SWAP_A)
			SET_ONE_TIME_HELP_MESSAGE_DISPLAYED(FHM_SELECTOR_NO_SWAP_B)
			SET_ONE_TIME_HELP_MESSAGE_DISPLAYED(FHM_SELECTOR_NO_SWAP_C)
		BREAK
		
		
	ENDSWITCH
ENDPROC

#ENDIF
#ENDIF


//PURPOSE:		A generic command used when processing any 'Do Mission' flow commands within the flow launcher.
//
//PARAM NOTES:	paramStrand - The strand in which the 'Do Mission' command is being skipped.
//				paramCoreVarsIndex - The g_flowUnsaved.coreVars[] array index at which the 'Do Mission' command is located.
//				paramGeneratePlaythroughOrder - Are we generating a playthrough order XML file containing an ordered list of mission names?mand?
//				paramGenerateFlowDiagram - Are we generating an XML diagram to be used by the flow_diagram system?
//
FUNC JUMP_LABEL_IDS LAUNCHER_SKIP_DO_MISSION_COMMAND(FLOW_LAUNCHER_VARS &flowLauncher, STRANDS paramStrand, INT paramCoreVarsIndex, BOOL paramGeneratePlaythroughOrder = FALSE, BOOL paramGenerateFlowDiagram = FALSE)
	INT index

	//Ensure the Script Vars Array position is legal.
	IF (paramCoreVarsIndex = ILLEGAL_ARRAY_POSITION)
		SCRIPT_ASSERT("LAUNCHER_SKIP_GENERIC_DO_MISSION_COMMAND: The Core Variables Array Position parameter is illegal. Skipping to next command.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	SP_MISSIONS eMissionID = INT_TO_ENUM(SP_MISSIONS, g_flowUnsaved.coreVars[paramCoreVarsIndex].iValue1)
	
	//If we're generating a playthrough order, write the necessary xml to the specified debug file.
	IF paramGeneratePlaythroughOrder
		LAUNCHER_WRITE_STRING_TO_PLAYTHROUGH_XML_FILE	(flowLauncher, "   <missionItem file=\"")
		#if USE_CLF_DLC
			LAUNCHER_WRITE_STRING_TO_PLAYTHROUGH_XML_FILE	(flowLauncher, GET_SCRIPT_FROM_HASH(g_sMissionStaticData[eMissionID].scriptHash))
		#endif
		#if USE_NRM_DLC
			LAUNCHER_WRITE_STRING_TO_PLAYTHROUGH_XML_FILE	(flowLauncher, GET_SCRIPT_FROM_HASH_NRM(g_sMissionStaticData[eMissionID].scriptHash))
		#endif
		#if not USE_CLF_DLC
		#if not USE_NRM_DLC
			LAUNCHER_WRITE_STRING_TO_PLAYTHROUGH_XML_FILE	(flowLauncher, g_sMissionStaticData[eMissionID].scriptName)
		#endif
		#endif
		LAUNCHER_WRITE_STRING_TO_PLAYTHROUGH_XML_FILE	(flowLauncher, ".sc")
		LAUNCHER_WRITE_STRING_TO_PLAYTHROUGH_XML_FILE	(flowLauncher, "\"/>")
		LAUNCHER_WRITE_NEW_LINE_TO_PLAYTHROUGH_XML_FILE	(flowLauncher)
	ENDIF
	
	//If we're generating a flow diagram, write the necessary xml to the specified debug file.
	IF paramGenerateFlowDiagram
		INT iTriggerCharBitset = Get_Mission_Triggerable_Characters_Bitset(eMissionID)
	
		LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, "   <mission		strand=\"")
		LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(paramStrand))
		LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, "\"")
		LAUNCHER_WRITE_STRING_TABS_TO_DIAGRAM_XML_FILE	(flowLauncher, 6, GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(paramStrand))
		LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, "name=\"")
		#if USE_CLF_DLC
			LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE	(flowLauncher, GET_SCRIPT_FROM_HASH(g_sMissionStaticData[eMissionID].scriptHash))
		#endif
		#if use_NRM_dlc
			LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE	(flowLauncher, GET_SCRIPT_FROM_HASH_NRM(g_sMissionStaticData[eMissionID].scriptHash))
		#endif
		#if not USE_CLF_DLC
		#if not use_NRM_dlc
			LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE	(flowLauncher, g_sMissionStaticData[eMissionID].scriptName)
		#endif
		#endif
		LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, "\"")
		#if USE_CLF_DLC
			LAUNCHER_WRITE_STRING_TABS_TO_DIAGRAM_XML_FILE	(flowLauncher, 10,GET_SCRIPT_FROM_HASH(g_sMissionStaticData[eMissionID].scriptHash))
		#endif
		#if USE_NRM_DLC
			LAUNCHER_WRITE_STRING_TABS_TO_DIAGRAM_XML_FILE	(flowLauncher, 10,GET_SCRIPT_FROM_HASH_NRM(g_sMissionStaticData[eMissionID].scriptHash))
		#endif
		#if not USE_CLF_DLC
		#if not use_NRM_DLC
			LAUNCHER_WRITE_STRING_TABS_TO_DIAGRAM_XML_FILE	(flowLauncher, 10, g_sMissionStaticData[eMissionID].scriptName)
		#endif
		#endif
		LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, "char=\"")
		
		BOOL bFirstCharWritten = FALSE
		REPEAT 3 index
			IF IS_MISSION_TRIGGERABLE_BY_CHARACTER(iTriggerCharBitset, INT_TO_ENUM(enumCharacterList, index))
				IF bFirstCharWritten
					LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE	(flowLauncher, "|")
				ENDIF
				LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, GET_CHARSHEET_DISPLAY_STRING_FROM_CHARSHEET(INT_TO_ENUM(enumCharacterList, index)))
				bFirstCharWritten = TRUE
			ENDIF
		ENDREPEAT
		LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, "\"")
		LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, "/>")
		LAUNCHER_WRITE_NEW_LINE_TO_DIAGRAM_XML_FILE		(flowLauncher)
		
		flowLauncher.iLauncherCurrentDiagramElementID++
	ENDIF
	
	#IF NOT USE_SP_DLC
	
	//If this is a heist finale.
	IF IS_BIT_SET(g_sMissionStaticData[eMissionID].settingsBitset, MF_INDEX_IS_HEIST)
	
		//Simulate leveling/killing the crew members.
		INT iHeist = GET_HEIST_FROM_MISSION_ID(eMissionID)
		
		CPRINTLN(DEBUG_HEIST, "-----------------------------------------------------")
		CPRINTLN(DEBUG_HEIST, "Debug passing ", GET_HEIST_INDEX_DEBUG_STRING(iHeist), ". Simulating crew level up.")
		CPRINTLN(DEBUG_HEIST, "-----------------------------------------------------")
	
		PRIVATE_Prime_Heist_Crew_Stat_Tracking_On_Mission_Start(eMissionID)
		INCREMENT_ALL_CREW_MEMBER_STATS_DURING_HEIST(iHeist, FALSE)
		PRIVATE_Finalise_Heist_Crew_Stat_Increments_On_Mission_Pass(eMissionID)
		
		CPRINTLN(DEBUG_HEIST, "-----------------------------------------------------")
		CPRINTLN(DEBUG_HEIST, "")
		
		//Set debug take values.
		g_savedGlobals.sHeistData.sEndScreenData[iHeist].iPlayerTake[CHAR_MICHAEL] = 10000
		g_savedGlobals.sHeistData.sEndScreenData[iHeist].iPlayerTake[CHAR_FRANKLIN] = 10000
		g_savedGlobals.sHeistData.sEndScreenData[iHeist].iPlayerTake[CHAR_TREVOR] = 10000
	ENDIF

	#ENDIF
	
	//Update the friend connection stages to unlock any connections that were locked for this mission.
	CLEAR_ALL_FRIEND_BLOCKS_FOR_MISSION(eMissionID)
	
	//Flag the appropriate mission as complete in the mission data struct.
	SET_MISSION_COMPLETE_STATE(eMissionID, TRUE)
	
	//Update various game state variables as we skip over the mission (flag early game help text as seen etc.).
	#IF USE_CLF_DLC
		LAUNCHER_UPDATE_GAME_STATE_FOR_SKIPPED_MISSION_CLF(eMissionID)
	#ENDIF
	#IF USE_NRM_DLC
		LAUNCHER_UPDATE_GAME_STATE_FOR_SKIPPED_MISSION_NRM(eMissionID)
	#ENDIF
	#IF NOT USE_SP_DLC
		LAUNCHER_UPDATE_GAME_STATE_FOR_SKIPPED_MISSION(eMissionID)
	#ENDIF
	
	// Make Director Mode characters linked to this mission available to the player.
	// Do this silently for debug skip.
	#IF FEATURE_SP_DLC_DIRECTOR_MODE
		UNLOCK_DIRECTOR_STORY_CHAR_FOR_STORY_MISSION(eMissionID, TRUE)
	#ENDIF
	
	//Set the missions stats to a plausible passed values.
	REPEAT g_sMissionStaticData[eMissionID].statCount index
		DEBUG_SET_MISSION_STAT_TO_COMPLETED_VALUE(g_sMissionStaticData[eMissionID].stats[index])
	ENDREPEAT
	
	//Follow the debug jump for this DO_MISSION command.
	RETURN INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.CoreVars[paramCoreVarsIndex].iValue4) // Debug jump label.

ENDFUNC


//PURPOSE:		Used when processing any 'do board interaction' flow command within the flow launcher.
//
//PARAM NOTES:	
//
FUNC JUMP_LABEL_IDS LAUNCHER_SKIP_DO_BOARD_INTERACTION_COMMAND(FLOW_LAUNCHER_VARS &flowLauncher, STRANDS paramStrand, INT iCommandIndex, BOOL bGenerateFlowDiagram = FALSE)

	//Ensure the Phonecall Vars Array position is legal.
	IF (iCommandIndex = ILLEGAL_ARRAY_POSITION)
		SCRIPT_ASSERT("LAUNCHER_SKIP_DO_BOARD_INTERACTION_COMMAND: The Comms Variables Array Position parameter is illegal. Skipping to next command.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	//Get the heist associated with this command.
	INT iHeist = g_flowUnsaved.commsVars[iCommandIndex].iValue1
	
	//Get the interaction type associated with this command.
	g_eBoardInteractions eInteraction = INT_TO_ENUM(g_eBoardInteractions, g_flowUnsaved.commsVars[iCommandIndex].iValue2)
	
	//If we're generating a flow diagram, write the necessary xml to the specified debug file.
	IF bGenerateFlowDiagram
		IF eInteraction != INTERACT_POI_OVERVIEW
			LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, "   <planning	strand=\"")
			LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(paramStrand))
			LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, "\"")
			LAUNCHER_WRITE_STRING_TABS_TO_DIAGRAM_XML_FILE	(flowLauncher, 6, GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(paramStrand))
			LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, "heist=\"")
			STRING strHeist
			SWITCH iHeist
				CASE HEIST_JEWEL
					strHeist = "Jewelry"
				BREAK	
				CASE HEIST_DOCKS
					strHeist = "Docks"
				BREAK		
				CASE HEIST_RURAL_BANK
					strHeist = "Rural Bank"
				BREAK
				CASE HEIST_AGENCY
					strHeist = "Agency"
				BREAK	
				CASE HEIST_FINALE
					strHeist = "Finale"
				BREAK	
			ENDSWITCH
			LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, strHeist)
			LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, "\"")
			LAUNCHER_WRITE_STRING_TABS_TO_DIAGRAM_XML_FILE	(flowLauncher, 10, strHeist)
			LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, "type=\"")
			LAUNCHER_WRITE_INT_TO_DIAGRAM_XML_FILE			(flowLauncher, iHeist)
			LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, "\"")
			LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, "/>")
			LAUNCHER_WRITE_NEW_LINE_TO_DIAGRAM_XML_FILE		(flowLauncher)
			flowLauncher.iLauncherCurrentDiagramElementID++
		ENDIF
	ENDIF
	
	//Simulate appropriate interaction.
	SWITCH eInteraction
		CASE INTERACT_POI_OVERVIEW
			//Do nothing.
		BREAK
	
		CASE INTERACT_CREW
			LAUNCHER_SIMULATE_HEIST_CREW_SELECTION(iHeist)
		BREAK
		
		CASE INTERACT_GAMEPLAY_CHOICE
			LAUNCHER_SIMULATE_HEIST_GAMEPLAY_CHOICE_SELECTION(iHeist)
		BREAK
		
		DEFAULT
			SCRIPT_ASSERT("LAUNCHER_SKIP_DO_BOARD_INTERACTION_COMMAND: The interaction type assigned to the command was invalid. Skipping to next command.")
			RETURN NEXT_SEQUENTIAL_COMMAND
		BREAK
	ENDSWITCH
	
	//Set the display group for this interaction on.
	INT iDisplayGroup = g_flowUnsaved.commsVars[iCommandIndex].iValue3
	SET_BIT(g_savedGlobals.sHeistData.iDisplayGroupVisibleBitset[iHeist], iDisplayGroup)

	//Follow the complete jump for this "Do Board Interaction" command.
	RETURN INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.commsVars[iCommandIndex].iValue4)

ENDFUNC


//PURPOSE:		A command used when processing any 'run code ID' flow commands within the flow launcher.
//
//PARAM NOTES:	paramCommandIndex - The g_flowUnsaved.phonecallVars[] array index at which the 'run code ID' command is located.
//
FUNC JUMP_LABEL_IDS LAUNCHER_SKIP_RUN_CODE_ID(INT paramCommandIndex)

	//Ensure the Phonecall Vars Array position is legal.
	IF (paramCommandIndex = ILLEGAL_ARRAY_POSITION)
		SCRIPT_ASSERT("LAUNCHER_SKIP_RUN_CODE_ID: The Core Small Variables Array Position parameter is illegal. Skipping to next command.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	//Get the code ID and check it is valid.
	CC_CodeID eCodeID = INT_TO_ENUM(CC_CodeID, g_flowUnsaved.coreSmallVars[paramCommandIndex].iValue1)
	
	#if USE_CLF_DLC
		IF eCodeID = CID_BLANK
		OR eCodeID = CID_CLF_MAX
			SCRIPT_ASSERT("LAUNCHER_SKIP_RUN_CODE_ID agt: The code ID associated with the command is invalid. Skipping to next command.")
			RETURN NEXT_SEQUENTIAL_COMMAND
		ENDIF
	#endif
	#if USE_NRM_DLC
	IF eCodeID = CID_BLANK
	OR eCodeID = CID_NRM_MAX
		SCRIPT_ASSERT("LAUNCHER_SKIP_RUN_CODE_ID NRM: The code ID associated with the command is invalid. Skipping to next command.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF	
	#endif
	
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
		IF eCodeID = CID_BLANK
		OR eCodeID = CID_MAX
			SCRIPT_ASSERT("LAUNCHER_SKIP_RUN_CODE_ID: The code ID associated with the command is invalid. Skipping to next command.")
			RETURN NEXT_SEQUENTIAL_COMMAND
		ENDIF
	#endif
	#endif
	
	
	//Execute the Code ID with the "updating flow" flag set and no delay.
	EXECUTE_CODE_ID(eCodeID, 0)
	
	//We're done. Step to the next command.
	RETURN NEXT_SEQUENTIAL_COMMAND

ENDFUNC


//PURPOSE:		A command used when processing any 'debug run code ID' flow commands within the flow launcher.
//
//PARAM NOTES:	paramCommandIndex - The g_flowUnsaved.phonecallVars[] array index at which the 'run code ID' command is located.
//
FUNC JUMP_LABEL_IDS LAUNCHER_SKIP_DEBUG_RUN_CODE_ID(INT paramCommandIndex)

	//Ensure the Debug Vars Array position is legal.
	IF (paramCommandIndex = ILLEGAL_ARRAY_POSITION)
		SCRIPT_ASSERT("LAUNCHER_SKIP_DEBUG_RUN_CODE_ID: The Debug Variables Array Position parameter is illegal. Skipping to next command.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	CC_CodeID theCodeID = INT_TO_ENUM(CC_CodeID, (g_flowUnsaved.debugVars[paramCommandIndex].anInt1%FLOW_COMMAND_HASH_MODIFIER_MAGNITUDE))
	
	#if USE_CLF_DLC
		//Get the code ID and check it is valid.
		IF theCodeID = CID_BLANK OR theCodeID = CID_CLF_MAX
			SCRIPT_ASSERT("LAUNCHER_DEBUG_SKIP_RUN_CODE_ID: The code ID associated with the command is invalid. Skipping to next command.")
			RETURN NEXT_SEQUENTIAL_COMMAND
		ENDIF
	#endif
	#if USE_NRM_DLC
		//Get the code ID and check it is valid.
		IF theCodeID = CID_BLANK OR theCodeID = CID_NRM_MAX
			SCRIPT_ASSERT("LAUNCHER_DEBUG_SKIP_RUN_CODE_ID: The code ID associated with the command is invalid. Skipping to next command.")
			RETURN NEXT_SEQUENTIAL_COMMAND
		ENDIF	
	#endif
	
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
		//Get the code ID and check it is valid.
		IF theCodeID = CID_BLANK OR theCodeID = CID_MAX
			SCRIPT_ASSERT("LAUNCHER_DEBUG_SKIP_RUN_CODE_ID: The code ID associated with the command is invalid. Skipping to next command.")
			RETURN NEXT_SEQUENTIAL_COMMAND
		ENDIF
	#endif
	#endif
	
	
	
	//Execute the Code ID with the "updating flow" flag set and no delay.
	EXECUTE_CODE_ID(theCodeID, 0)
	
	//We're done. Step to the next command.
	RETURN NEXT_SEQUENTIAL_COMMAND

ENDFUNC


FUNC JUMP_LABEL_IDS LAUNCHER_SKIP_ACTIVATE_RANDOMCHAR_MISSION(FLOW_LAUNCHER_VARS &flowLauncher, STRANDS paramStrand, INT paramCommandIndex)
//To ensure this compiles.
paramStrand = paramStrand
flowLauncher.bLauncherRunMultipleLabelLaunch = flowLauncher.bLauncherRunMultipleLabelLaunch

#if  USE_CLF_DLC
	paramCommandIndex = paramCommandIndex 
	RETURN NEXT_SEQUENTIAL_COMMAND
#endif
#if USE_NRM_DLC
	paramCommandIndex = paramCommandIndex 
	RETURN NEXT_SEQUENTIAL_COMMAND
#endif	

#if not USE_CLF_DLC
#if not use_NRM_DLC
	//Ensure the misc array position is legal.
	IF (paramCommandIndex = ILLEGAL_ARRAY_POSITION)
		SCRIPT_ASSERT("LAUNCHER_SKIP_ACTIVATE_RANDOMCHAR_MISSION: The Core Variables Array Position parameter is illegal. Skipping to next command.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	//Get the RC mission ID and check it is valid.
	g_eRC_MissionIDs eRCTempMission = INT_TO_ENUM(g_eRC_MissionIDs, g_flowUnsaved.coreVars[paramCommandIndex].iValue1)
	IF eRCTempMission = NO_RC_MISSION OR eRCTempMission = MAX_RC_MISSIONS
		SCRIPT_ASSERT("LAUNCHER_SKIP_ACTIVATE_RANDOMCHAR_MISSION: The RC Mission ID associated with the command is invalid. Skipping to next command.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Are we trying to launch to an RC mission?
	IF g_eDebugLaunchingToRCMission != NO_RC_MISSION AND g_eDebugLaunchingToRCMission != MAX_RC_MISSIONS
		g_structRCMissionsStatic sRCLaunchingMissionDetails
		g_structRCMissionsStatic sRCTempMissionDetails
		Retrieve_Random_Character_Static_Mission_Details(g_eDebugLaunchingToRCMission, sRCLaunchingMissionDetails)
		Retrieve_Random_Character_Static_Mission_Details(eRCTempMission, sRCTempMissionDetails)
		
		CPRINTLN(DEBUG_FLOW, "RC skip to = " , sRCLaunchingMissionDetails.rcScriptName, "RC skip over = " , sRCTempMissionDetails.rcScriptName)
		
		//Is the strand associated with the RC mission in this command the same stand as the mission we're launching to?
		IF sRCLaunchingMissionDetails.rcStrandID = sRCTempMissionDetails.rcStrandID
		
			CDEBUG1LN(DEBUG_FLOW_LAUNCHER, "Simulating completing missions in RC strand.")
			
			//Yes, auto pass all RC missions in this strand up to the one we're launching to.
			BOOL bKeepLooking = TRUE
			INT iMission
			REPEAT MAX_RC_MISSIONS iMission
				IF bKeepLooking
					eRCTempMission = INT_TO_ENUM(g_eRC_MissionIDs, iMission)
					
					// Stop when we hit our mission.
					IF (eRCTempMission = g_eDebugLaunchingToRCMission) 
						
						//Activate this mission but don't complete it.
						Activate_RC_Mission(eRCTempMission, FALSE)
						bKeepLooking = FALSE
					ELSE
						Retrieve_Random_Character_Static_Mission_Details(eRCTempMission, sRCTempMissionDetails)
						IF (sRCTempMissionDetails.rcStrandID = sRCLaunchingMissionDetails.rcStrandID)
						AND (sRCTempMissionDetails.rcMissionGroup != sRCLaunchingMissionDetails.rcMissionGroup OR sRCTempMissionDetails.rcMissionGroup = NO_RC_MISSION_GROUP) // Ignore items in same sub group
						AND (eRCTempMission != RC_BARRY_1 AND eRCTempMission != RC_BARRY_2)	// Don't complete Barry 1 & 2 when unlocking subsequent Barry missions as these aren't required to launch
								
							//Flag this mission as complete.
							Activate_RC_Mission(eRCTempMission, TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
	ELSE
		CPRINTLN(DEBUG_FLOW, "Not jumping to RC: g_eDebugLaunchingToRCMission = " , g_eDebugLaunchingToRCMission)
	ENDIF
	
	RETURN PERFORM_ACTIVATE_RANDOMCHAR_MISSION(paramCommandIndex)
#endif
#endif
ENDFUNC


FUNC JUMP_LABEL_IDS LAUNCHER_SKIP_DO_CUSTOM_CHECK_JUMPS(INT paramCommandIndex)
	//Get the debug jump label ID.
	JUMP_LABEL_IDS eDebugJump = INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.coreVars[paramCommandIndex].iValue4)
	
	IF eDebugJump = STAY_ON_THIS_COMMAND
		//No debug jump defined. Run the check.
		//Get the true jump label ID.
		JUMP_LABEL_IDS eTrueJump = INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.coreVars[paramCommandIndex].iValue2)
		
		//Get the false jump label ID.
		JUMP_LABEL_IDS eFalseJump = INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.coreVars[paramCommandIndex].iValue3)
		
		//Get the custom check ID.
		FLOW_CHECK_IDS eCheckID = INT_TO_ENUM(FLOW_CHECK_IDS, g_flowUnsaved.coreVars[paramCommandIndex].iValue1)
		
		//Retrieve the custom check function itself.
		PerformCustomFlowCheck DO_FLOW_CHECK
		GET_FLOW_CHECK_FUNCTION(eCheckID, DO_FLOW_CHECK)
		
		//Run the check and jump based on the result.
		IF CALL DO_FLOW_CHECK()
			RETURN eTrueJump
		ENDIF
		RETURN eFalseJump
	ELSE
		RETURN eDebugJump
	ENDIF
ENDFUNC


FUNC JUMP_LABEL_IDS LAUNCHER_SKIP_DO_CHARACTER_SWITCH(INT paramCommandIndex)

	//Ensure the misc array position is legal.
	IF (paramCommandIndex = ILLEGAL_ARRAY_POSITION)
		SCRIPT_ASSERT("LAUNCHER_SKIP_DO_CHARACTER_SWITCH: The Core Variables Array Position parameter is illegal. Skipping to next command.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	////////////////////////////////////////////////////////////////////////////////
	///     FOR NOW DO NOT SET WHEN SKIPPING - BREAKS DEBUG MENU CHARACTER SELECTION
	RETURN NEXT_SEQUENTIAL_COMMAND
	
	
	
	
	// Get the player character
	enumCharacterList ePed = INT_TO_ENUM(enumCharacterList, paramCommandIndex)
	
	IF ePed = CHAR_MICHAEL
	OR ePed = CHAR_FRANKLIN
	OR ePed = CHAR_TREVOR
		// Set the character
		IF NOT SET_CURRENT_SELECTOR_PED(GET_SELECTOR_SLOT_FROM_PLAYER_PED_ENUM(ePed), TRUE)
			RETURN STAY_ON_THIS_COMMAND
		ENDIF
	ELSE
		SCRIPT_ASSERT("LAUNCHER_SKIP_DO_CHARACTER_SWITCH: The character enum associated with the command is invalid. Skipping to next command.")
	ENDIF
	
	//We're done. Step to the next command.
	RETURN NEXT_SEQUENTIAL_COMMAND
ENDFUNC


//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════╡ Launcher GTA5 Specific Flow Control Functions ╞══════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛ 

//PURPOSE: Sets the GTA5 gameflow back to it's initial state.
//
PROC LAUNCHER_RESET_FLOW()

	CDEBUG1LN(DEBUG_FLOW_LAUNCHER, "Resetting the GTA5 flow state.")
	
	//Reset the gameflow state.
	Reset_Gameflow()
ENDPROC


//PURPOSE: Sets the GTA5 gameflow back to it's initial state and then triggers the prologue strand.
//
PROC LAUNCHER_RESTART_FLOW()

	CDEBUG1LN(DEBUG_FLOW_LAUNCHER, "Restarting the GTA5 flow.")
	
	//Reset the gameflow state and then activate the proglogue strand.
	ACTIVATE_GAMEFLOW_AT_FIRST_STRAND()
ENDPROC


//PURPOSE: Returns a flow label that sits at the very end of the GTA5 gameflow.
FUNC JUMP_LABEL_IDS LAUNCHER_GET_FLOW_FINAL_LABEL()
	#if USE_CLF_DLC
		RETURN LABEL_GAME_END_CLF
	#endif
	#if USE_NRM_DLC
		RETURN LABEL_GAME_END_NRM
	#endif
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
		RETURN LABEL_GAME_END
	#endif
	#endif
ENDFUNC


#ENDIF
