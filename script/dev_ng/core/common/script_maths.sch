//  script_MATHS.sch
//
//  Common math procedures/functions used in the game.

//USING "globals.sch"
USING "commands_debug.sch"
USING "types.sch"

USING "commands_entity.sch"
USING "commands_object.sch"
USING "commands_misc.sch"
USING "debug_channels_structs.sch"

// --- Constants ----------------------------------------------------------------
CONST_INT	 	HIGHEST_INT 		2147483647
CONST_INT 		LOWEST_INT			-2147483647	
CONST_FLOAT		CONST_PI			3.141592653589793238462643383279


// --- Bitfield Constants -------------------------------------------------------
CONST_INT BIT0 1
CONST_INT BIT1 (BIT0 * 2)
CONST_INT BIT2 (BIT1 * 2)
CONST_INT BIT3 (BIT2 * 2)
CONST_INT BIT4 (BIT3 * 2)
CONST_INT BIT5 (BIT4 * 2)
CONST_INT BIT6 (BIT5 * 2)
CONST_INT BIT7 (BIT6 * 2)
CONST_INT BIT8 (BIT7 * 2)
CONST_INT BIT9 (BIT8 * 2)
CONST_INT BIT10 (BIT9 * 2)
CONST_INT BIT11 (BIT10 * 2)
CONST_INT BIT12 (BIT11 * 2)
CONST_INT BIT13 (BIT12 * 2)
CONST_INT BIT14 (BIT13 * 2)
CONST_INT BIT15 (BIT14 * 2)
CONST_INT BIT16 (BIT15 * 2)
CONST_INT BIT17 (BIT16 * 2)
CONST_INT BIT18 (BIT17 * 2)
CONST_INT BIT19 (BIT18 * 2)
CONST_INT BIT20 (BIT19 * 2)
CONST_INT BIT21 (BIT20 * 2)
CONST_INT BIT22 (BIT21 * 2)
CONST_INT BIT23 (BIT22 * 2)
CONST_INT BIT24 (BIT23 * 2)
CONST_INT BIT25 (BIT24 * 2)
CONST_INT BIT26 (BIT25 * 2)
CONST_INT BIT27 (BIT26 * 2)
CONST_INT BIT28 (BIT27 * 2)
CONST_INT BIT29 (BIT28 * 2)
CONST_INT BIT30 (BIT29 * 2)

// AMB
// I AM REMOVING BIT31 - in script it works (usually) but has been flaky when going into and out of decorators.
// PLEASE do not put this back in
//CONST_INT BIT31 (BIT30 * 2)  // -2147483648 for 32-bit signed int, this definition works with code.
CONST_INT ALLBITS -1 // same as ~0 or 0xFFFFFFFF on code side for 32 bit integers


/////////////////////////////
///// TERNARY OP COMMANDS ///
////////////////////////////   

/// PURPOSE:
///    Acts like a ternary operator - picks between two integers depending on the evaluation of the expression passed in
/// PARAMS:
///    bEval - Expression to evaluate
///    iValueTrue - return value if expression is TRUE
///    iValueFalse - return value is expression is FALSE
/// RETURNS:
///    bEval ? iValueTrue : iValueFalse
FUNC INT PICK_INT(BOOL bEval,INT iValueTrue,INT iValueFalse)
	IF bEval
		RETURN iValueTrue
	ELSE
		RETURN iValueFalse
	ENDIF
ENDFUNC
 
/// PURPOSE:
///    Acts like a ternary operator - picks between two floats depending on the evaluation of the expression passed in
/// PARAMS:
///    bEval - Expression to evaluate
///    fValueTrue - return value if expression is TRUE
///    fValueFalse - return value is expression is FALSE
/// RETURNS:
///    bEval ? fValueTrue : fValueFalse
FUNC FLOAT PICK_FLOAT(BOOL bEval,FLOAT fValueTrue,FLOAT fValueFalse)
	IF bEval
		RETURN fValueTrue
	ELSE
		RETURN fValueFalse
	ENDIF
ENDFUNC

/// PURPOSE:
///    Acts like a ternary operator - picks between two strings depending on the evaluation of the expression passed in
/// PARAMS:
///    bEval - Expression to evaluate
///    sValueTrue - return value if expression is TRUE
///    sValueFalse - return value is expression is FALSE
/// RETURNS:
///    bEval ? sValueTrue : sValueFalse
FUNC STRING PICK_STRING(BOOL bEval,STRING sValueTrue,STRING sValueFalse)
	IF bEval
		RETURN sValueTrue
	ELSE
		RETURN sValueFalse
	ENDIF
ENDFUNC

/// PURPOSE:
///    Acts like a ternary operator - picks between two vectors depending on the evaluation of the expression passed in
/// PARAMS:
///    bEval - Expression to evaluate
///    vValueTrue - return value if expression is TRUE
///    vValueFalse - return value is expression is FALSE
/// RETURNS:
///    bEval ? vValueTrue : vValueFalse
FUNC VECTOR PICK_VECTOR(BOOL bEval,VECTOR vValueTrue,VECTOR vValueFalse)
	IF bEval
		RETURN vValueTrue
	ELSE
		RETURN vValueFalse
	ENDIF
ENDFUNC



PROC ENABLE_BIT( INT &iBitField, INT iBitIndex, BOOL bEnable )
	IF bEnable
		SET_BIT( iBitField, iBitIndex )
	ELSE
		CLEAR_BIT( iBitField, iBitIndex )
	ENDIF
ENDPROC


// These bit commands allow you to use an enum as the bit index
//--------------------------------------------------------------------------------------------

PROC SET_BIT_ENUM( INT &iBitField, ENUM_TO_INT eBitIndex )
	SET_BIT( iBitField, ENUM_TO_INT( eBitIndex ) )
ENDPROC

PROC CLEAR_BIT_ENUM( INT &iBitField, ENUM_TO_INT eBitIndex )
	CLEAR_BIT( iBitField, ENUM_TO_INT( eBitIndex ) )
ENDPROC

FUNC BOOL IS_BIT_SET_ENUM( INT &iBitField, ENUM_TO_INT eBitIndex )
	RETURN IS_BIT_SET( iBitField, ENUM_TO_INT( eBitIndex ) )
ENDFUNC

PROC ENABLE_BIT_ENUM( INT &iBitField, ENUM_TO_INT eBitIndex, BOOL bEnable )
	ENABLE_BIT( iBitField, ENUM_TO_INT( eBitIndex ), bEnable )
ENDPROC 


//////////////////////
/// BITFIELD OPERATIONS
/// PURPOSE: Sets the bit passed in the integer bitfield (the bit is set to 1 - TRUE). For the bit number, use one of the VALUE constants: BIT0 to BIT31. 
/// PARAMS:
///    iBitField - bitfield you want to set
///    iBitValue - bit to set in the bitfield. Can take multiple bits or'd together with |
PROC SET_BITMASK(INT &iBitField, INT iBitMask)
	iBitField = iBitField | iBitMask
ENDPROC

/// PURPOSE: Clears the bit passed in the integer bitfield (the bit is set to 0 - FALSE). For the bit number, use one of the VALUE constants: BIT0 to BIT31. 
/// PARAMS:
///    iBitField - bitfield you want to clear
///    iBitValue - bit to set in the bitfield. Can take multiple bits or'd together with |
PROC CLEAR_BITMASK(INT &iBitField, INT iBitMask)
	iBitField -= iBitField & iBitMask
ENDPROC


/// PURPOSE: Returns TRUE if the bit passed is set in the integer bitfield. For the bit number, use one of the VALUE constants: BIT0 to BIT31.
/// PARAMS:
///    iBitField - bitfield you want to check
///    iBitValue - bit to set in the bitfield. Can take multiple bits or'd together with |
/// RETURNS: TRUE if the given mask is set, FALSE otherwise.
FUNC BOOL IS_BITMASK_SET(INT iBitField, INT iBitMask)
	RETURN (iBitField & iBitMask) != 0
ENDFUNC

/// BITFIELD OPERATIONS
/// PURPOSE: Sets the bit passed in the integer bitfield (the bit is set to 1 - TRUE). For the bit number, use an ENUM declared as one of the VALUE constants: BIT0 to BIT31. 
///    iBitField - bitfield you want to set
///    eBitMask - bit to set in the bitfield. Can take multiple bits or'd together with |. This version takes any ENUM value, rather than an INT 
PROC SET_BITMASK_AS_ENUM(INT &iBitField, ENUM_TO_INT eBitMask)
	SET_BITMASK(iBitField, ENUM_TO_INT(eBitMask))
ENDPROC

/// BITFIELD OPERATIONS
/// PURPOSE: Sets the bit passed in the integer bitfield (the bit is set to 1 - TRUE). For the both the bitfield and bit number, use an ENUM declared as one of the VALUE constants: BIT0 to BIT31. 
///    iBitField - bitfield you want to set. This version takes any ENUM value, rather than an INT 
///    eBitMask - bit to set in the bitfield. Can take multiple bits or'd together with |. This version takes any ENUM value, rather than an INT 
PROC SET_BITMASK_ENUM_AS_ENUM(ENUM_TO_INT &iBitField, ENUM_TO_INT eBitMask)
	iBitField = iBitField | eBitMask
ENDPROC

/// PURPOSE: Clears the bit passed in the integer bitfield (the bit is set to 0 - FALSE). For the bit number, use an ENUM declared as one of the VALUE constants: BIT0 to BIT31. 
///    iBitField - bitfield you want to clear
///    eBitMask - bit to set in the bitfield. Can take multiple bits or'd together with |. This version takes any ENUM value, rather than an INT 
PROC CLEAR_BITMASK_AS_ENUM(INT &iBitField, ENUM_TO_INT eBitMask)
	CLEAR_BITMASK(iBitField, ENUM_TO_INT(eBitMask))
ENDPROC

/// PURPOSE: Clears the bit passed in the integer bitfield (the bit is set to 0 - FALSE). For the bit number, use an ENUM declared as one of the VALUE constants: BIT0 to BIT31. 
///    iBitField - bitfield you want to clear
///    eBitMask - bit to set in the bitfield. Can take multiple bits or'd together with |. This version takes any ENUM value, rather than an INT 
PROC CLEAR_BITMASK_ENUM_AS_ENUM(ENUM_TO_INT &eBitField, ENUM_TO_INT eBitMask)
	eBitField -= eBitField & eBitMask
ENDPROC

/// PURPOSE: Returns TRUE if the bit passed is set in the integer bitfield. For the bit number, use an ENUM declared as one of the VALUE constants: BIT0 to BIT31.
///    iBitField - bitfield you want to check
///    eBitMask - bit to set in the bitfield. Can take multiple bits or'd together with |. This version takes any ENUM value, rather than an INT 
/// RETURNS: TRUE if the given mask is set, FALSE otherwise.
FUNC BOOL IS_BITMASK_AS_ENUM_SET(INT iBitField, ENUM_TO_INT eBitMask)
// this shaves off 28% of the instructions (from 25 to 18)
	RETURN (iBitField & ENUM_TO_INT(eBitMask)) != 0
ENDFUNC

/// PURPOSE: Returns TRUE if the bit passed is set in the integer bitfield. For the bit number, 
///  use an ENUM declared as one of the VALUE constants: BIT0 to BIT31.
///    iBitField - bitfield you want to check
///    eBitMask - bit to set in the bitfield. Can take multiple bits or'd together with |. This version takes any ENUM value, rather than an INT 
/// RETURNS: TRUE if the given mask is set, FALSE otherwise.
FUNC BOOL IS_BITMASK_ENUM_AS_ENUM_SET(ENUM_TO_INT eBitField, ENUM_TO_INT eBitMask)
	RETURN (ENUM_TO_INT(eBitField) & ENUM_TO_INT(eBitMask)) != 0
ENDFUNC

/// PURPOSE: Returns the bit index of the lowest bit set in a bitfield
///    For example, passing 12, the bits set are 8 (BIT3) and 4 (BIT2) so 2 will be returned
/// PARAMS: Any integer where bits have been set
/// RETURNS: Lowest bit set, or -1 if a blank bitfield is passed in
FUNC INT GET_LOWEST_BIT_SET(INT iBitfield)
	INT iCount
	
	IF iBitfield = 0
		RETURN -1
	ENDIF
	
	REPEAT 32 iCount
		IF IS_BIT_SET(iBitfield, 0)
			RETURN iCount
		ENDIF
		iBitfield = SHIFT_RIGHT(iBitfield, 1)
	ENDREPEAT
	
	RETURN -1
ENDFUNC

/// PURPOSE: Returns the integer generated if the given int were set
///    For example, passing 0 (BIT0) returns 1, passing 4 (BIT4) returns 16
/// PARAMS: Bit index 0-31
/// RETURNS: Int equivalent, or -1 if an invalid bit index is given.
FUNC INT BIT_TO_INT(INT iBit)
	RETURN SHIFT_LEFT(1, iBit)
ENDFUNC

/// PURPOSE:
/// Sums the bits in the given bitfield
/// RETURNS:
/// Number of bits set in the bitfield
FUNC INT SUM_BITS(INT bitField)
	// Obtained from http://graphics.stanford.edu/~seander/bithacks.html#CountBitsSetKernighan
	// shaves off 30%-96% instructions on the average
	INT count, result = bitField
	IF result < 0
		count = 1
		CLEAR_BITMASK(result,(BIT30 * 2))
	ELSE
		count = 0 
	ENDIF
	WHILE result > 0
		result = result & (result-1)
		++count
	ENDWHILE
	RETURN count
ENDFUNC

/// PURPOSE:
///    Creates a sequential bitmask of a specified width.
/// PARAMS:
///    iMaskWidth - How many bits wide should the bit mask be.
///    iStartBit - The starting bit
/// RETURNS:
///    Returns the generated bit mask
FUNC INT GENERATE_SEQUENTIAL_BITMASK( INT iMaskWidth, INT iStartBit )

	INT iResult
	INT i
	FOR i = 0 TO iMaskWidth-1
		SET_BIT( iResult, iStartBit + i)
	ENDFOR
	RETURN iResult
ENDFUNC

FUNC INT GET_PACKED_BITFIELD_ARRAY_ARRAY_INDEX( INT iElementNum, INT iDataBitWidth )
	int result = FLOOR( TO_FLOAT(iElementNum * iDataBitWidth ) / (floor(32.0 / iDataBitWidth) * iDataBitWidth)  )
	RETURN result
ENDFUNC

FUNC INT GET_PACKED_BITFIELD_ARRAY_ELEMENT_INDEX( INT iElementNum, INT iDataBitWidth )
	int iResult = iElementNum - ( GET_PACKED_BITFIELD_ARRAY_ARRAY_INDEX( iElementNum, iDataBitWidth ) * floor( 32.0 / iDataBitWidth ) )
	RETURN iResult
	
ENDFUNC

/// PURPOSE:
///    Allows you to pack several pieces of information of equal size into a single int. More info...
///    For example: an enum which has 4 possible values needs only 2 bits of memory. An int = 32 bits, 32/2 = 16 so we can store
///    16 lots of that enum in 1 int. Kind of like using a single int as an array.
///    Use GET_PACKED_BITFILED_VALUE to retrieve information later.
/// PARAMS:
///    iBitField - The int in which to store the information
///    iElementID - Index of the element to be stored
///    iElementWidth - How many bits needed to represent 1 element (use GET_BITS_NEEDED_TO_REPRESENT_NUMBER() )
///    iValue - The value you wish to store
PROC SET_PACKED_BITFIELD_VALUE( INT &iBitField, INT iElementID, INT iElementWidth, INT iValue)

	//iElementID 	= GET_PACKED_BITFIELD_ARRAY_ELEMENT_INDEX( iElementID, iElementWidth )

	// generate a bitmask to use		
	INT iBitmask = GENERATE_SEQUENTIAL_BITMASK( iElementWidth, iElementID * iElementWidth )
	// wipe the element first
	iBitField -= iBitField & iBitmask	
	// Set value
	iBitField |= SHIFT_LEFT( iValue, iElementID * iElementWidth )  
ENDPROC

FUNC INT GET_BITS_NEEDED_TO_REPRESENT_NUMBER( INT iValue )
	RETURN CEIL( LOG10(TO_FLOAT(iValue+1)) / LOG10(2.0) ) // we do +1 because 0 is a valid value that needs to be represented
ENDFUNC

PROC SET_PACKED_BITFIELD_VALUE_FROM_ENUM( INT &iBitField, INT iElementID, INT iElementWidth, ENUM_TO_INT eValue )
	SET_PACKED_BITFIELD_VALUE( iBitField, iElementID, iElementWidth, ENUM_TO_INT( eValue ) )
ENDPROC

FUNC INT GET_PACKED_BITFILED_VALUE( INT iBitField, INT iElementID, INT iElementWidth )
	INT iBitmask = GENERATE_SEQUENTIAL_BITMASK( iElementWidth, 0 )
	RETURN SHIFT_RIGHT( iBitField, iElementID * iElementWidth ) & iBitmask
ENDFUNC


PROC SET_PACKED_BITFIELD_VALUE_IN_ARRAY( INT &iBitFieldArr[], INT iElementNum, INT iDataBitWidth, INT iValue )
	INT iArrayID 	= GET_PACKED_BITFIELD_ARRAY_ARRAY_INDEX( iElementNum, iDataBitWidth )
	INT iElementID 	= GET_PACKED_BITFIELD_ARRAY_ELEMENT_INDEX( iElementNum, iDataBitWidth )
	IF iArrayID >= COUNT_OF(iBitFieldArr)
		SCRIPT_ASSERT( "SET_PACKED_BITFIELD_VALUE_IN_ARRAY() - iBitFieldArr[], array is too small to store the requested amount of data." )
	ELSE
		SET_PACKED_BITFIELD_VALUE( iBitFieldArr[iArrayID], iElementID, iDataBitWidth, iValue )
	ENDIF
ENDPROC

PROC SET_PACKED_BITFIELD_VALUE_IN_ARRAY_FROM_ENUM( INT &iBitFieldArr[], INT iElementNum, INT iDataBitWidth, ENUM_TO_INT eEnumValue )
	SET_PACKED_BITFIELD_VALUE_IN_ARRAY( iBitFieldArr, iElementNum, iDataBitWidth, ENUM_TO_INT( eEnumValue ) )
ENDPROC

FUNC INT GET_PACKED_BITFIELD_VALUE_FROM_ARRAY( INT &iBitFieldArr[], INT iElementNum, INT iDataBitWidth )
	INT iArrayID 	= GET_PACKED_BITFIELD_ARRAY_ARRAY_INDEX( iElementNum, iDataBitWidth )
	INT iElementID 	= GET_PACKED_BITFIELD_ARRAY_ELEMENT_INDEX( iElementNum, iDataBitWidth )
	IF iArrayID >= COUNT_OF(iBitFieldArr)
		SCRIPT_ASSERT( "GET_PACKED_BITFIELD_VALUE_FROM_ARRAY() - iBitFieldArr[] is not large enough to have an element of that bit width stored in it." )
	ELSE
		RETURN GET_PACKED_BITFILED_VALUE( iBitFieldArr[iArrayID], iElementID, iDataBitWidth )
	ENDIF
	RETURN 0
ENDFUNC


#IF IS_DEBUG_BUILD

/// PURPOSE:
///    Get a human-readable representation of a bitfield as a string. Useful for printing bitfield state to debug 
///    channels and logs. Remember bitfields are read right to left so bit 0 is on the far right.
/// PARAMS:
///    iBitField - The bit field to convert to text.
/// RETURNS: A TEXT_LABEL_63 containing 32 "1" and "0" characters that represent the state of the bitfield.
FUNC TEXT_LABEL_63 DEBUG_GET_BITFIELD_TEXT_LABEL(INT iBitField)
	TEXT_LABEL_63 tl63BitField = ""
	
	INT index
	FOR index = 31 TO 0 STEP -1
		tl63BitField += PICK_STRING(IS_BIT_SET(iBitField, index), "1", "0")
	ENDFOR
	
	RETURN tl63BitField
ENDFUNC

#ENDIF


/////////////////////////
///// MIN/MAX COMMANDS //
/////////////////////////   

/// PURPOSE:
///    Returns the smaller of the two integer values given
/// PARAMS:
///    iVal1 - value 1 to compare
///    iVal2 - value 2 to compare
/// RETURNS:
///    iVal1 if iVal1 is less than iVal2, iVal2 otherwise
FUNC INT IMIN(INT iVal1, INT iVal2)
	IF iVal1 > iVal2
		RETURN iVal2
	ENDIF
	RETURN iVal1
ENDFUNC


/// PURPOSE:
///    Returns the larger of the two integer values given
/// PARAMS:
///    iVal1 - value 1 to compare
///    iVal2 - value 2 to compare
/// RETURNS:
///    iVal1 if iVal1 is greater than iVal2, iVal2 otherwise
FUNC INT IMAX(INT iVal1, INT iVal2)
	IF iVal1 > iVal2
		RETURN iVal1
	ENDIF
	RETURN iVal2
ENDFUNC

/// PURPOSE:
///    Returns the smaller of the two floating point values given
/// PARAMS:
///    fVal1 - value 1 to compare
///    fVal2 - value 2 to compare
/// RETURNS:
///    fVal1 if fVal1 is less than fVal2, fVal2 otherwise
FUNC FLOAT FMIN(FLOAT fVal1, FLOAT fVal2)
	IF fVal1 > fVal2
		RETURN fVal2
	ENDIF
	RETURN fVal1
ENDFUNC

/// PURPOSE:
///    Returns the larger of the two float values given
/// PARAMS:
///    fVal1 - value 1 to compare
///    fVal2 - value 2 to compare
/// RETURNS:
///    fVal1 if fVal1 is greater than fVal2, fVal2 otherwise
FUNC FLOAT FMAX(FLOAT fVal1, FLOAT fVal2)
	IF fVal1 > fVal2
		RETURN fVal1
	ENDIF
	RETURN fVal2
ENDFUNC


/////////////////////////
///// VECTOR COMMANDS ///
/////////////////////////   

/// PURPOSE: returns a vector of 0,0,0
FUNC VECTOR ZERO_VECTOR()
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

/// PURPOSE: returns true if the passed vector is 0,0,0
FUNC BOOL IS_VECTOR_ZERO(VECTOR vPos)
	IF vPos.x = 0.0
	AND vPos.y = 0.0
	AND vPos.z = 0.0
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE: returns the cross product of two passed vectors
/// the cross product is a binary operation on two vectors in a three-dimensional Euclidean 
/// space that results in another vector which is perpendicular to the plane containing the 
/// two input vectors.   
FUNC VECTOR CROSS_PRODUCT(VECTOR p1, VECTOR p2)

    RETURN( << (p1.y*p2.z - p1.z*p2.y), (p1.z*p2.x - p1.x*p2.z) , (p1.x*p2.y - p1.y*p2.x) >> )

ENDFUNC

 
/// PURPOSE: returns a the dot product from 2 vectors (takes two vectors over the real numbers R and returns a real-valued scalar quantity)
FUNC FLOAT DOT_PRODUCT(VECTOR v, VECTOR w)

    RETURN(v.x*w.x + v.y*w.y + v.z*w.z)

ENDFUNC

/// PURPOSE: returns the 2d dot product on the xy plane from 2 vectors
///    DOES NOT normalize the vectors on the xy plane first! Make sure you know what the return value means!
FUNC FLOAT DOT_PRODUCT_XY(VECTOR v, VECTOR w)

    RETURN(v.x*w.x + v.y*w.y)

ENDFUNC

/// PURPOSE:
///    Returns a normalised version of the vector,  a vector with a magnitude of one.///    
/// PARAMS:
///    vec - Vector to be normalised.
/// RETURNS:
///    Normalised vector. 
FUNC VECTOR NORMALISE_VECTOR(VECTOR vec)
	
	float magnitude = VMAG(vec)
	
	// check for division by 0 (as it crashes game)
	IF magnitude != 0
		FLOAT invMag = 1.0 / magnitude
		vec *= invMag
	ELSE
		CPRINTLN(DEBUG_SYSTEM, "** ERROR NORMALISE_VECTOR - magnitude == 0 ")
		vec.X = 0
		vec.y = 0
		vec.z = 0
	ENDIF
	
	RETURN vec 

ENDFUNC

///PURPOSE: 
///    Returns the UP vector given the Euler angle rotation vector
///PARAMS:
///    vec - The <<x,y,z>> rotation vector (get_entity_rotation) or the <<pitch, roll, yaw>> euler angles vector
///RETURNS:
///    The normalised UP vector IN LOCAL SPACE! Rotate around Z for world space vector direction
FUNC VECTOR GET_UP_VECTOR(VECTOR vec)
	VECTOR res = <<SIN(vec.y),-SIN(vec.x)*COS(vec.y),COS(vec.x)*COS(vec.y)>>
	RETURN res
ENDFUNC

/// PURPOSE:
///    Returns a vector scaled to the desired length, with the orientation of the original.
///    Faster than normalising, then re-scaling a vector.
///    However, passing in an already-normalised vector will waste calculations; just scale the vector.
/// PARAMS:
///    vNonNormalised - Vector to be scaled; if normalized, this function is a waste
///    fDesiredLen - Desired length
/// RETURNS:
///    Vector in direction of vNonNormalised with magnitude fDesiredLen
FUNC VECTOR GET_VECTOR_OF_LENGTH(VECTOR vNonNormalised, FLOAT fDesiredLen)
	IF fDesiredLen = 0.0
		RETURN <<0.0,0.0,0.0>>
	ENDIF
	
	FLOAT fMag = VMAG(vNonNormalised)
	
	IF fMag <> 0.0
		RETURN vNonNormalised * (fDesiredLen / fMag)
	ENDIF
	
	RETURN <<0.0,0.0,0.0>>
ENDFUNC

/// PURPOSE:
///	   Rotates a vector in 3D space about the world z-axis.
///    It is assumed here that the axis of rotation passes through the origin.
///    If you are rotating by an increment of 90 degrees, see next function.
///    If you are rotating lots of things by the same angle and want to precompute the trig values, see next next function.
///  PARAMS:
///    vToRotate - Should be orientation only (should come from origin)
///    fZRot - Rotation in degrees
///  RETURNS:
///    Vector rotated locally about the world z axis by fZRot degrees
FUNC VECTOR ROTATE_VECTOR_ABOUT_Z(VECTOR vToRotate,  FLOAT fZRot)
	VECTOR vNew
	FLOAT fSin = SIN(fZRot)
	FLOAT fCos = COS(fZRot)
	vNew.x = vToRotate.x * fCos - vToRotate.y * fSin
	vNew.y = vToRotate.x * fSin + vToRotate.y * fCos
	vNew.z = vToRotate.z
	RETURN vNew
ENDFUNC

///PURPOSE: 
///    Returns the UP vector given an entity
///PARAMS:
///    ent - the entity index whose rotation it will use
///RETURNS:
///    The normalised UP vector IN WORLD SPACE! Use GET_UP_VECTOR for local space
FUNC VECTOR GET_ENTITY_UP_VECTOR(ENTITY_INDEX ent)
	VECTOR vRot = GET_ENTITY_ROTATION(ent)
	RETURN ROTATE_VECTOR_ABOUT_Z(GET_UP_VECTOR(GET_ENTITY_ROTATION(ent,EULER_XYZ)),vRot.z)
ENDFUNC

/// PURPOSE:
///	   Rotates a vector in 3D space about the world z-axis in 90-degree increments.
///    Utilises shortcuts for commonly-used 90 degree rotations.
///    It is assumed here that the axis of rotation passes through the origin.
///  PARAMS:
///    vToRotate - Should be orientation only (should come from origin)
///    eRotation - Rotation by 90, 180, or -90 (270) degrees
///  RETURNS:
///    Vector rotated locally about the world z axis
ENUM ROT_STEP
	ROTSTEP_90,
	ROTSTEP_180,
	ROTSTEP_270,
	ROTSTEP_NEG_90 = ROTSTEP_270
ENDENUM
FUNC VECTOR ROTATE_VECTOR_ABOUT_Z_ORTHO(VECTOR vToRotate, ROT_STEP eRotation)
	VECTOR vNew
	
	SWITCH eRotation
		CASE ROTSTEP_90
			vNew.x = -vToRotate.y
			vNew.y =  vToRotate.x
		BREAK
		CASE ROTSTEP_180
			vNew.x = -vToRotate.x
			vNew.y = -vToRotate.y
		BREAK
		CASE ROTSTEP_NEG_90
			vNew.x =  vToRotate.y
			vNew.y = -vToRotate.x
		BREAK
	ENDSWITCH
	
	vNew.z = vToRotate.z
	RETURN vNew
ENDFUNC

/// PURPOSE:
///	   Rotates a vector in 3D space about the world z-axis, using precomputed trig values.
///    It is assumed here that the axis of rotation passes through the origin.
///    Useful if you need to rotate lots of vectors by the same angle; avoids recomputing values.
///  PARAMS:
///    vToRotate - Should be orientation only (should come from origin)
///    fSin - sin(rotAngle)
///    fCos - cos(rotAngle)
///  RETURNS:
///    Vector rotated locally about the world z axis
FUNC VECTOR ROTATE_VECTOR_ABOUT_Z_PRECOMPUTE(VECTOR vToRotate,  FLOAT fSin, FLOAT fCos)
	VECTOR vNew
	vNew.x = vToRotate.x * fCos - vToRotate.y * fSin
	vNew.y = vToRotate.x * fSin + vToRotate.y * fCos
	vNew.z = vToRotate.z
	RETURN vNew
ENDFUNC

/// PURPOSE:
///	   Gives the projection of one vector onto another. Make sure vProjectee is normalised.
///  PARAMS:
///    vProjector - This vector is projected onto vProjectee
///    vProjectee - vProjector is projected onto this vector
///  RETURNS:
///    Projection of vProjector onto vProjectee
FUNC VECTOR PROJECT_VECTOR_ONTO_VECTOR(VECTOR vProjector, VECTOR vProjectee)
	IF (vProjector.x = 0.0 AND vProjector.y = 0.0 AND vProjector.z = 0.0) ORELSE (vProjectee.x = 0.0 AND vProjectee.y = 0.0 AND vProjectee.z = 0.0)
		RETURN <<0.0, 0.0, 0.0>>
	ENDIF
	RETURN GET_VECTOR_OF_LENGTH(vProjectee, DOT_PRODUCT(vProjector, vProjectee))
ENDFUNC

/// PURPOSE:
///	   Gives the projection of one vector onto another along the xy plane (preserves z values).
///  PARAMS:
///    vProjector - This vector is projected onto vProjectee
///    vProjectee - vProjector is projected onto this vector
///  RETURNS:
///    Projection of vProjector onto vProjectee
FUNC VECTOR PROJECT_VECTOR_ONTO_VECTOR_XY(VECTOR vProjector, VECTOR vProjectee)
	IF (vProjector.x = 0.0 AND vProjector.y = 0.0) ORELSE (vProjectee.x = 0.0 AND vProjectee.y = 0.0)
		RETURN <<0.0, 0.0, vProjector.z>>
	ENDIF
	
	FLOAT fLen = SQRT(vProjectee.x * vProjectee.x + vProjectee.y * vProjectee.y)
	FLOAT fDot = DOT_PRODUCT_XY(vProjector, vProjectee)
	fLen = fDot / fLen
	vProjectee.x *= fLen
	vProjectee.y *= fLen
	vProjectee.z = vProjector.z
	RETURN vProjectee
ENDFUNC

/// PURPOSE:
///    Project a point onto a line defined by 2 points
/// RETURNS: Proejcted vector in 3d space.
///    
FUNC VECTOR PROJECT_POINT_ONTO_LINE(VECTOR point, VECTOR lineStart, VECTOR lineFinish)
	// A + dot(AP,AB) / dot(AB,AB) * AB
	//A - line start
	//B - line finish
	//P - point
	
	VECTOR AB
	AB = lineFinish - lineStart
	
	RETURN lineStart + DOT_PRODUCT(point - lineStart, AB) / DOT_PRODUCT(AB, AB) * AB
ENDFUNC

/// PURPOSE:
///    Tests a point against a vertically-aligned cylinder.
/// PARAMS:
///    vTestPos - The position to check
///    vCylinder - The cylinder's center or bottom (see below)
///    fRadius - Radius of the cylinder
///    fHeight - Height of the cylinder
///    bIsBottom - true if vCylinder refers to bottom of cylinder
/// RETURNS:
///    True if the point lies within the cylinder
FUNC BOOL IS_POINT_IN_CYLINDER(VECTOR vTestPos, VECTOR vCylinder, FLOAT fRadius, FLOAT fHeight, BOOL bIsBottom = TRUE)
	VECTOR vCylToPoint
	
	IF NOT bIsBottom
		vCylinder.z -= 0.5 * fHeight
	ENDIF
	
	vCylToPoint = vTestPos - vCylinder
	
	IF vCylToPoint.z < 0.0 OR vCylToPoint.z > fHeight
		RETURN FALSE
	ENDIF
	
	RETURN vCylToPoint.x * vCylToPoint.x + vCylToPoint.y * vCylToPoint.y <= fRadius * fRadius
ENDFUNC

/// PURPOSE:
///    Gets a random vector within a sphere centred at vPos with a radius of fRadius.
/// PARAMS:
///    vPos - The sphere centre
///    fRadius - Radius of the sphere
/// RETURNS:
///    A random position vector within fRadius of vPos.
FUNC VECTOR GET_RANDOM_POINT_IN_SPHERE(VECTOR vPos, FLOAT fRadius )
 
	// Pick a random direction vector.
	VECTOR vDir = << GET_RANDOM_FLOAT_IN_RANGE(-1,1) , GET_RANDOM_FLOAT_IN_RANGE(-1,1), GET_RANDOM_FLOAT_IN_RANGE(-1,1) >>
	
	// Project this vector into the sphere some random distance.
	RETURN vPos + GET_VECTOR_OF_LENGTH(vDir, GET_RANDOM_FLOAT_IN_RANGE(0, fRadius))

ENDFUNC

/// PURPOSE:
///    Gets a random vector within a sphere centred at vPos with a radius of fRadius. Distributes points evenly.
///    Non-uniform version is faster but tends to bunch points up in the center, and along the eight non-cardinal axes.
/// PARAMS:
///    vPos - The sphere centre
///    fRadius - Radius of the sphere
/// RETURNS:
///    A random position vector within fRadius of vPos.
FUNC VECTOR GET_RANDOM_POINT_IN_SPHERE_UNIFORM(VECTOR vPos, FLOAT fRadius)
	VECTOR vDir
	FLOAT fRadiusSq = fRadius * fRadius
	INT i
	
	// Generate points in a cube that encompasses the sphere.
	REPEAT 20 i
		vDir = <<GET_RANDOM_FLOAT_IN_RANGE(-fRadius, fRadius), GET_RANDOM_FLOAT_IN_RANGE(-fRadius, fRadius), GET_RANDOM_FLOAT_IN_RANGE(-fRadius, fRadius)>>
		IF VDIST2(vDir,<<0,0,0>>) <= fRadiusSq		
			RETURN vPos + vDir
		ENDIF
	ENDREPEAT
	
	// Statistically, 99% of the time six checks is enough and only one in 2.7 million calls will fail the first 20 checks.
	// If that happens, fall back to the non-uniform version.
	RETURN GET_RANDOM_POINT_IN_SPHERE(vPos, fRadius)
ENDFUNC

/// PURPOSE:
///    Gets a random vector within a disc centred at vPos with a radius of fRadius and a height of fHeight.
/// PARAMS:
///    vPos - The sphere disc
///    fRadius - Radius of the disc
///    fHeight - Height of the disc (goes both above and below vPos)
/// RETURNS:
///    A random position vector within fRadius of vPos.
FUNC VECTOR GET_RANDOM_POINT_IN_DISC(VECTOR vPos, FLOAT fRadius, FLOAT fHeight )
 
	// Pick a random unit direction vector.
	VECTOR vDir = << GET_RANDOM_FLOAT_IN_RANGE(-1,1) , GET_RANDOM_FLOAT_IN_RANGE(-1,1), 0 >>
	FLOAT fHalfHeight = fHeight / 2.0
	
	// Project this vector into the disc some random distance.
	vDir = GET_VECTOR_OF_LENGTH(vDir, GET_RANDOM_FLOAT_IN_RANGE(0, fRadius))
	
	// Grab a random height and add it on.
	vDir.z = GET_RANDOM_FLOAT_IN_RANGE(-fHalfHeight, fHalfHeight)
	
	RETURN vPos + vDir

ENDFUNC


/// PURPOSE:
///    Gets a random vector within a disc centred at vPos with a radius of fRadius and a height of fHeight.
///    Non-uniform version tends to group points in the center and along the four non-cardinal axes.
/// PARAMS:
///    vPos - The sphere disc
///    fRadius - Radius of the disc
///    fHeight - Height of the disc (goes both above and below vPos)
/// RETURNS:
///    A random position vector within fRadius of vPos.
FUNC VECTOR GET_RANDOM_POINT_IN_DISC_UNIFORM(VECTOR vPos, FLOAT fRadius, FLOAT fHeight)
	VECTOR vDir
	FLOAT fRadiusSq = fRadius * fRadius
	FLOAT fHalfHeight
	INT i
	
	// Generate points in a square that encompasses the circle.
	REPEAT 10 i
		vDir = <<GET_RANDOM_FLOAT_IN_RANGE(-fRadius, fRadius), GET_RANDOM_FLOAT_IN_RANGE(-fRadius, fRadius), 0.0>>
		IF vDir.x * vDir.x + vDir.y * vDir.y <= fRadiusSq
			fHalfHeight = fHeight / 2.0
			vDir.z = GET_RANDOM_FLOAT_IN_RANGE(-fHalfHeight, fHalfHeight)
			RETURN vPos + vDir
		ENDIF
	ENDREPEAT
	
	// Statistically, 99% of the time three checks is enough, and only one in 4.8 million calls will fail the first 10 checks.
	// If that happens, use the non-uniform version.
	RETURN GET_RANDOM_POINT_IN_DISC(vPos, fRadius, fHeight)
ENDFUNC


/// PURPOSE:
///    Gets a random bool
//RETURNS:
// TRUE or FALSE based on random int generation.
FUNC BOOL GET_RANDOM_BOOL()
	IF IS_BIT_SET(GET_RANDOM_INT_IN_RANGE(0, 65535), 0)
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC


/// PURPOSE:
///    Checks if two vectors are exactly equal.
/// PARAMS:
///    v1 - Vector 1
///    v2 - Vector 2
/// RETURNS:
///    TRUE if v1 = v2
FUNC BOOL ARE_VECTORS_EQUAL(VECTOR v1, VECTOR v2, BOOL bDisregardZ = FALSE)
	
	IF bDisregardZ
		RETURN (v1.x = v2.x) AND (v1.y = v2.y)
	ELSE
		RETURN (v1.x = v2.x) AND (v1.y = v2.y) AND (v1.z = v2.z)
	ENDIF
	
ENDFUNC

/// PURPOSE:
///    Checks if two vectors are almost equal / are within fTolerance of each other. 
/// PARAMS:
///    v1 - Vector 1
///    v2 - Vector 2
///    fTolerance - Tolerance factor.
/// RETURNS:
///    TRUE if v1 is within fTolerance of v2.
FUNC BOOL ARE_VECTORS_ALMOST_EQUAL(VECTOR v1, VECTOR v2, FLOAT fTolerance = 0.5 , BOOL bDisregardZ = FALSE )
	
	IF fTolerance < 0
		fTolerance = 0
	ENDIF
	IF NOT bDisregardZ
		IF ABSF(v1.x - v2.x) <= fTolerance
			IF ABSF(v1.y - v2.y) <= fTolerance
				IF ABSF(v1.z - v2.z) <= fTolerance
					RETURN TRUE
				ENDIF
			ENDIF		
		ENDIF
	ELSE
		IF ABSF(v1.x - v2.x) <= fTolerance
			IF ABSF(v1.y - v2.y) <= fTolerance
				RETURN TRUE
			ENDIF		
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC


/// PURPOSE: Converts radians to degrees.
FUNC FLOAT RAD_TO_DEG(FLOAT fRadians)
	RETURN fRadians * 57.2957795130 //(180.0 / PI)
ENDFUNC


/// PURPOSE: Converts degrees to radians.
FUNC FLOAT DEG_TO_RAD(FLOAT fDegrees)
	RETURN fDegrees * 0.0174532925 //(PI / 180.0)
ENDFUNC


/// PURPOSE:
///    Clamps a desired float value to the nearest specified minimum or maximum value.  
/// PARAMS:
///    X - float value to be clamped
///    Min - lower constraint
///    Max - lower constraint
/// RETURNS:
///    Clamped value
 FUNC FLOAT CLAMP (FLOAT X, FLOAT Min, FLOAT Max)
	IF X > Max 
		RETURN Max
	ELIF X < Min
		RETURN Min
	ENDIF
	RETURN X
 ENDFUNC
 
 /// PURPOSE:
 ///    Clamps the vector components between Min (inclusive) & Max (inclusive).
 /// RETURNS:
 ///    Clamped vector
 FUNC VECTOR CLAMP_VECTOR_COMPONENTS(VECTOR v, VECTOR vMin, VECTOR vMax)
 	v.x = CLAMP(v.x, vMin.x, vMax.x)
 	v.y = CLAMP(v.y, vMin.y, vMax.y)
 	v.z = CLAMP(v.z, vMin.z, vMax.z)
	RETURN v
 ENDFUNC
  
 /// PURPOSE:
///    Clamps a desired integer value to the nearest specified minimum or maximum value.  
/// PARAMS:
///    X - integer value to be clamped
///    Min - lower constraint
///    Max - lower constraint
/// RETURNS:
///    Clamped value
 FUNC INT CLAMP_INT(INT X, INT Min, INT Max)
	IF X > Max 
		RETURN Max 
	ELIF X < Min 
		RETURN Min
	ENDIF 
	RETURN X
ENDFUNC
 
 
/// PURPOSE:
///    Returns the lesser of the two values.  
/// PARAMS:
///    fValue1 - Value 1
///    fValue2 - Value 2
/// RETURNS:
///    Lesser of the two values.
FUNC FLOAT MIN_VALUE(FLOAT fValue1, FLOAT fValue2)
	IF fValue1 < fValue2
		RETURN fValue1
	ELIF fValue2 < fValue1
		RETURN fValue2
	ENDIF
	RETURN fValue1
ENDFUNC


/// PURPOSE:
///    Returns the lesser of the two values.  
/// PARAMS:
///    iValue1 - Value 1
///    iValue2 - Value 2
/// RETURNS:
///    Lesser of the two values.
FUNC INT MIN_VALUE_INT(INT iValue1, INT iValue2)
	IF iValue1 < iValue2
		RETURN iValue1
	ELIF iValue2 < iValue1
		RETURN iValue2
	ENDIF
	RETURN iValue1
ENDFUNC

/// PURPOSE:
///    Finds the minimum value in array 
/// RETURNS:
///    n - lenght 
FUNC INT MIN_ARRAY_INDEX_VALUE(INT &arr[], INT n)
	INT i
	INT min = arr[0]
	FOR i = 1 TO n - 1
		IF arr[i] < min
			min = arr[i]
		ENDIF
	ENDFOR 
	
	RETURN min
ENDFUNC

/// PURPOSE:
///    Finds the lowest array index
/// RETURNS:
///    n - lenght 
FUNC INT MIN_ARRAY_INDEX(INT &arr[], INT n)
	INT i, iIndex
	INT min = arr[0]
	iIndex = 0
	FOR i = 1 TO n - 1
		IF arr[i] < min
			min = arr[i]
			iIndex = i
		ENDIF
	ENDFOR 
	
	RETURN iIndex
ENDFUNC
 
/// PURPOSE:
///    Returns the greater of the two values.  
/// PARAMS:
///    fValue1 - Value 1
///    fValue2 - Value 2
/// RETURNS:
///    Greater of the two values.
FUNC FLOAT MAX_VALUE(FLOAT fValue1, FLOAT fValue2)
	IF fValue1 > fValue2
		RETURN fValue1
	ELIF fValue2 > fValue1
		RETURN fValue2
	ENDIF
	RETURN fValue1
ENDFUNC


/// PURPOSE:
///    Returns the greater of the two values.  
/// PARAMS:
///    iValue1 - Value 1
///    iValue2 - Value 2
/// RETURNS:
///    Greater of the two values.
FUNC INT MAX_VALUE_INT(INT iValue1, INT iValue2)
	IF iValue1 > iValue2
		RETURN iValue1
	ELIF iValue2 > iValue1
		RETURN iValue2
	ENDIF
	RETURN iValue1
ENDFUNC


/// PURPOSE:
///    Wraps a value to a limited area. 
/// PARAMS:
///    x - value to be wrapped
///    min - lower constraint
///    max - upper constraint
/// RETURNS:
///    Wrapped value
///    
FUNC FLOAT WRAP (FLOAT X, FLOAT Min, FLOAT Max)
	IF Min = Max
		RETURN Min
	ENDIF
	
	FLOAT fRange = Max - Min
	X = X - ROUND((X - Min) / fRange) * fRange
   	IF X < Min
    	X = X + fRange
	ENDIF
    RETURN X
ENDFUNC 


/// PURPOSE:
///    Wraps an integer value to a limited area.  
/// PARAMS:
///    x - integer value to be wrapped
///    min - lower constraint
///    max - upper constraint
/// RETURNS:
///    Wrapped value
/// 
FUNC INT IWRAP (INT x, INT min, INT max)
	//Check our parameters are valid.
	IF min > max 
		CERRORLN(DEBUG_SYSTEM, "IWRAP: Invalid min and max parameters. min > max.")
		RETURN -1
	ENDIF
	
	//Is x already in range.
	IF (x >= min) AND (x <= max) 
		RETURN x
	ENDIF
	
	//If we are working with negative values, translate into positive space
	//to simplify calculations.
	INT translation = 0
	IF x < min 
		IF x < 0
			translation = -x
		ENDIF
	ELSE
		IF min < 0
			translation = -min
		ENDIF
	ENDIF
	x += translation
	min += translation
	max += translation
	
	//Wrap our value.
	INT range = max - min
	INT offset
	INT wrapRemainder
	IF x > max 
		offset = x - max 
		wrapRemainder = offset % range
		x = min + wrapRemainder
	ELSE
		offset = min - x
		wrapRemainder = offset % range
		x = max - wrapRemainder
	ENDIF
	
	//Reverse the translation.
	RETURN x - translation
ENDFUNC

/// PURPOSE:
///    Wraps integer value between 0 and max.
///    Note that it will wrap by its over shoot
///    e.g. x = 13 max = 10 would return 3
/// PARAMS:
///    x - index to wrap
///    max - max index value
/// RETURNS:
///    index wrapped between 0 and max
FUNC INT IWRAP_INDEX(INT x, INT max)
	RETURN ((x % max) + max) % max
ENDFUNC

/// PURPOSE:
///    Maps a float value from one range to another
/// PARAMS:
///    fValueToMap - the value to wrap
///    fFromSource - the original range min
///    fToSource -  the original range max
///    fFromTarget - the target range min
///    fToTarget - the target range max
/// RETURNS:
///    the value mapped to the fFromTarget and fToTarget range
FUNC FLOAT MAP(FLOAT fValueToMap, FLOAT fFromSource, FLOAT fToSource, FLOAT fFromTarget, FLOAT fToTarget)
	 RETURN (fValueToMap - fFromSource) / (fToSource - fFromSource) * (fToTarget - fFromTarget) + fFromTarget
ENDFUNC

/// PURPOSE:
///    Maps an int value from one range to another
/// PARAMS:
///    iValueToMap - the value to wrap
///    iFromSource - the original range min
///    iToSource -  the original range max
///    iFromTarget - the target range min
///    iToTarget - the target range max
/// RETURNS:
///    the value mapped to the iFromTarget and iToTarget range
FUNC INT IMAP(INT iValueToMap, INT iFromSource, INT iToSource, INT iFromTarget, INT iToTarget)
	 RETURN (iValueToMap - iFromSource) / (iToSource - iFromSource) * (iToTarget - iFromTarget) + iFromTarget
ENDFUNC
 
 /// PURPOSE:
///    Interpolates between two float values based on passed in alpha (linear)
/// PARAMS:
///    fVal1 - Value at alpha = 0
///    fVal2 - Value at alpha = 1
///    fAlpha - Interp value between 0 and 1
/// RETURNS:
///    Interpolated value
///    
FUNC FLOAT LERP_FLOAT(FLOAT fVal1, FLOAT fVal2, FLOAT fAlpha)
	RETURN (1.0 - fAlpha) * fVal1 + fAlpha * fVal2
ENDFUNC
 
 /// PURPOSE:
///    Interpolates between two int values based on passed in alpha (linear)
/// PARAMS:
///    iVal1 - Value at alpha = 0
///    iVal2 - Value at alpha = 1
///    fAlpha - Interp value between 0 and 1
/// RETURNS:
///    Interpolated value
///    
FUNC INT LERP_INT(INT iVal1, INT iVal2, FLOAT fAlpha)
	RETURN ROUND((1.0 - fAlpha) * iVal1 + fAlpha * iVal2)
ENDFUNC
 
 /// PURPOSE:
///    Interpolates between two vectors based on passed in alpha (linear)
/// PARAMS:
///    v1 - Value at alpha = 0
///    v2 - Value at alpha = 1
///    fAlpha - Interp value between 0 and 1
/// RETURNS:
///    Interpolated value
///    
FUNC VECTOR LERP_VECTOR(VECTOR v1, VECTOR v2, FLOAT fAlpha)
	RETURN (1.0 - fAlpha) * v1 + fAlpha * v2
ENDFUNC

 /// PURPOSE:
///    Uses a quadratic Bezier curve to plot a point.
/// PARAMS:
///    vStart - Value at alpha = 0
///    vControl - Middle pt that forms the curve
///    vEnd - Value at alpha = 1
///    fAlpha - Interp value between 0 and 1
/// RETURNS:
///    Interpolated value
///    
FUNC VECTOR QUADRATIC_BEZIER_PLOT(VECTOR vStart, VECTOR vControl, VECTOR vEnd, FLOAT fAlpha)
	VECTOR vInterp1, vInterp2
	vInterp1 = LERP_VECTOR(vStart, vControl, fAlpha)
	vInterp2 = LERP_VECTOR(vControl, vEnd, fAlpha)
	RETURN LERP_VECTOR(vInterp1, vInterp2, fAlpha)
ENDFUNC


 /// PURPOSE:
///    Interpolates between two floats based on a cosine curve
/// PARAMS:
///    fVal1 - Value at alpha = 0
///    fVal2 - Value at alpha = 1
///    fAlpha - Interp value between 0 and 1
/// RETURNS:
///    Interpolated Float
///    
FUNC FLOAT COSINE_INTERP_FLOAT(FLOAT fVal1, FLOAT fVal2, FLOAT fAlpha)
	FLOAT alpha2 = (1.0 - COS(RAD_TO_DEG(fAlpha * CONST_PI))) * 0.5
	RETURN  fVal1*(1.0 - alpha2) + fVal2*alpha2
ENDFUNC

 /// PURPOSE:
///    Interpolates between two vectors based on a cosine curve
/// PARAMS:
///    v1 - Value at alpha = 0
///    v2 - Value at alpha = 1
///    fAlpha - Interp value between 0 and 1
/// RETURNS:
///    Interpolated value
///    
FUNC VECTOR COSINE_INTERP_VECTOR(VECTOR v1, VECTOR v2, FLOAT fAlpha)
	FLOAT alpha2 = (1.0 - COS(RAD_TO_DEG(fAlpha * CONST_PI))) * 0.5
	RETURN  v1*(1.0 - alpha2) + v2*alpha2
ENDFUNC

/// AUTHOR: Thomas french
//PURPOSE: Checks if a ped is in an angled area. This calculates the angled area from a single face. 
FUNC BOOL IS_PED_IN_ANGLED_AREA_CALCULATED_FROM_SINGLE_FACE (PED_INDEX ped, VECTOR VEC1, VECTOR VEC2, float distance, BOOL check3D = TRUE, PED_TRANSPORT_MODE PTM_MODE = TM_ANY)
    BOOL bInArea = FALSE
    
    // subtract our two worLd vectors to get our resultant vector
    vector resultant = vec1 - vec2
    
    //get a unit vector at right angles to our resultant
    vector vRightAngleVec = NORMALISE_VECTOR (CROSS_PRODUCT(<<resultant.x, resultant.y, 0.0>>, <<0.0, 0.0, 1.0>>))
    
    //apply the right angled unit vector to our world coords and multiply by the distance scalar 
    vec1.x = VEC1.x + (vRightAngleVec.x * (Distance/2))
    vec1.y = VEC1.y + (vRightAngleVec.y * (Distance/2))
    
    vec2.x = VEC2.x + (vRightAngleVec.x * (Distance/2))
    vec2.y = VEC2.y + (vRightAngleVec.y * (Distance/2))
    
    if IS_ENTITY_IN_ANGLED_AREA( PED, VEC1, VEC2, Distance, FALSE, check3D, PTM_MODE)
        bInArea = TRUE
    ENDIF
    
    return bInArea

ENDFUNC

//INFO:			Author - Paul Davies
//PARAMS NOTES:	
//				Rotation - The rotation vector.
//    			QuadW - The outputted w value for the quaternion rotation.	
//    			QuadX - The outputted x value for the quaternion rotation.
//    			QuadY - The outputted y value for the quaternion rotation.
//    			QuadZ - The outputted z value for the quaternion rotation.
//PURPOSE:		Coverts a standard rotation vector into the values required to perform the same rotation with a quaternion.
PROC GET_ROTATION_QUATERNION(VECTOR Rotation, FLOAT &QuadX, FLOAT &QuadY, FLOAT &QuadZ, FLOAT &QuadW)
	//pitch and roll are flipped here to make the quaternion the same as that returned by code
	FLOAT pitch = (Rotation.y)/2
	FLOAT yaw = (Rotation.z)/2
	FLOAT roll = (Rotation.x)/2
	
	FLOAT sinp = SIN(pitch)
	FLOAT siny = SIN(yaw)
	FLOAT sinr = SIN(roll)
	FLOAT cosp = COS(pitch)
	FLOAT cosy = COS(yaw)
	FLOAT cosr = COS(roll)
	
	QuadX = sinr * cosp * cosy - cosr * sinp * siny
	QuadY = cosr * sinp * cosy + sinr * cosp * siny
	QuadZ = cosr * cosp * siny - sinr * sinp * cosy
	QuadW = cosr * cosp * cosy + sinr * sinp * siny
ENDPROC

 
FUNC BOOL IS_ANGLE_WITHIN_RANGE_OF_ANGLE_WRAPPED(FLOAT iAngle, FLOAT iOtherAngle, FLOAT iRange)
	FLOAT upperLimit, lowerLimit
	upperLimit = iOtherAngle + (iRange/2)
	IF upperLimit > 360
	    upperLimit -= 360.0
	ENDIF

	lowerLimit = iOtherAngle - (iRange/2)
	IF lowerLimit < 0
	    lowerLimit += 360.0
	ENDIF
  
    IF upperLimit > lowerLimit
		IF iAngle < upperLimit
		AND iAngle > lowerLimit
		    RETURN TRUE
		ELSE
		    RETURN FALSE
		ENDIF
    ELSE
        IF iAngle < upperLimit
        OR iAngle > lowerLimit
            RETURN TRUE
        ELSE
            RETURN FALSE
        ENDIF
    ENDIF
      
    RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Compute remainder of division.
FUNC FLOAT FMOD_REMAINDER(FLOAT fNumerator, FLOAT fDenominator)
	RETURN fNumerator - FLOOR(fNumerator/fDenominator) * fDenominator
ENDFUNC

/// PURPOSE:
///    Gets the difference in degrees between 2 angles, will automatically wrap the angles. Returns the smallest angular difference and its sign.
FUNC FLOAT GET_ANGULAR_DIFFERENCE(FLOAT fAngleA, FLOAT fAngleB)
	FLOAT fDiff = FMOD_REMAINDER(fAngleB - fAngleA + 180, 360)
	IF fDiff < 0
		fDiff += 360
	ENDIF
	RETURN fDiff - 180
ENDFUNC

/// PURPOSE:
///    Using a coord and heading to determine whether that heading points to a coord within a certain angular range.
/// PARAMS:
///    vCoord - The origin coord
///    fHeading - heading we want to test to see if its pointing at a coord
///    vOtherCoord - the coord we want to see if we're pointing towards
///    fAngleRange - the acceptable range
/// RETURNS:
///    Returns TRUE if this heading is pointing at the coord from its coord within the angular range.
FUNC BOOL IS_COORD_AND_HEADING_FACING_COORD_WITHIN_RANGE( VECTOR vCoord, FLOAT fHeading, VECTOR vOtherCoord, FLOAT fAngleRange )

	VECTOR vWorldForwardVector = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS( <<0,0,0>>, fHeading, <<0,1,0>> )
	FLOAT fDot = DOT_PRODUCT( NORMALISE_VECTOR( vWorldForwardVector ), NORMALISE_VECTOR( vOtherCoord - vCoord ) )

	RETURN ACOS( fDot ) <= fAngleRange
ENDFUNC


/// PURPOSE:
///    Used to query if an entity is facing towards a coord withing a certain angular range.
/// PARAMS:
///    entity - the entity we are querying
///    vCoord - the coord to query against
///    fAngleRange - the acceptable angular range
/// RETURNS:
///    returns TRUE if the entity is facing the coord and is within the specified angular range.
FUNC BOOL IS_ENTITY_FACING_COORD_WITHIN_RANGE( ENTITY_INDEX entity, VECTOR vCoord, FLOAT fAngleRange )
	RETURN IS_COORD_AND_HEADING_FACING_COORD_WITHIN_RANGE( GET_ENTITY_COORDS( entity ), GET_ENTITY_HEADING( entity ), vCoord, fAngleRange )
ENDFUNC


// Interpolation types used with interpolation helpers
// for an idea of the type of movement produced by each type, see http://codeplea.com/simple-interpolation
ENUM INTERPOLATION_TYPE
	INTERPTYPE_LINEAR,			// constant change over time
	INTERPTYPE_ACCEL,			// accelerates (starts slow, gets faster over time)
	INTERPTYPE_DECEL,			// decelerates (starts fast, gets slower over time)
	INTERPTYPE_COSINE,			// accel to decel using a cosine curve
	INTERPTYPE_SMOOTHSTEP,		// similar to cosine except cheaper (no division)
	// interps along an arc instead of a straight line for consistent speed when interping rotations
	INTERPTYPE_SPHERICAL_LINEAR,
	INTERPTYPE_SPHERICAL_ACCEL,
	INTERPTYPE_SPHERICAL_DECEL,
	INTERPTYPE_SPHERICAL_COSINE,
	INTERPTYPE_SPHERICAL_SMOOTHSTEP
ENDENUM


FUNC FLOAT INTERP_FLOAT( FLOAT f1, FLOAT f2, FLOAT fAlpha, INTERPOLATION_TYPE eInterpType = INTERPTYPE_LINEAR )

	FLOAT fResult
	
	// Adjust the alpha for the incoming type of interpolation
	SWITCH eInterpType
	
		CASE INTERPTYPE_ACCEL
		CASE INTERPTYPE_SPHERICAL_ACCEL
			fAlpha = POW( fAlpha, 2 )
		BREAK
		
		CASE INTERPTYPE_DECEL
		CASE INTERPTYPE_SPHERICAL_DECEL
			fAlpha = 1 - POW( 1 - fAlpha, 2 )
		BREAK
		
		CASE INTERPTYPE_COSINE
		CASE INTERPTYPE_SPHERICAL_COSINE
			fAlpha = ( ( -COS( RAD_TO_DEG( CONST_PI * fAlpha ) ) ) / 2 ) + 0.5
		BREAK
		
		CASE INTERPTYPE_SMOOTHSTEP
		CASE INTERPTYPE_SPHERICAL_SMOOTHSTEP
			fAlpha = POW( fAlpha, 2 ) * ( 3 - ( 2 * fAlpha ) )
		BREAK

	ENDSWITCH

	// carry out the interpolation
	SWITCH eInterpType
	
		CASE INTERPTYPE_LINEAR
			fResult = f1 + ( fAlpha * ( f2 - f1 ) )
		BREAK
		
		CASE INTERPTYPE_ACCEL
		CASE INTERPTYPE_DECEL
		CASE INTERPTYPE_COSINE
		CASE INTERPTYPE_SMOOTHSTEP
			fResult = INTERP_FLOAT( f1, f2, fAlpha, INTERPTYPE_LINEAR )
		BREAK
		
		CASE INTERPTYPE_SPHERICAL_LINEAR
			fResult = (1 - fAlpha) * f1 + fAlpha * f2
		BREAK
		
		CASE INTERPTYPE_SPHERICAL_ACCEL
		CASE INTERPTYPE_SPHERICAL_DECEL
		CASE INTERPTYPE_SPHERICAL_COSINE
		CASE INTERPTYPE_SPHERICAL_SMOOTHSTEP
			fResult = INTERP_FLOAT( f1, f2, fAlpha, INTERPTYPE_SPHERICAL_LINEAR )
		BREAK
		
	ENDSWITCH

	RETURN fResult
ENDFUNC


FUNC VECTOR INTERP_VECTOR( VECTOR v1, VECTOR v2, FLOAT fAlpha, INTERPOLATION_TYPE eInterpType = INTERPTYPE_LINEAR )
	VECTOR vResult
	vResult.x = INTERP_FLOAT( v1.x, v2.x, fAlpha, eInterpType )
	vResult.y = INTERP_FLOAT( v1.y, v2.y, fAlpha, eInterpType )
	vResult.z = INTERP_FLOAT( v1.z, v2.z, fAlpha, eInterpType )
	RETURN vResult
ENDFUNC


FUNC VECTOR GET_POINT_ON_CIRCUMFERENCE( VECTOR vCentre, FLOAT fRadius, FLOAT fAngle )
	VECTOR vResult
	vResult.x = vCentre.x + fRadius * COS( fAngle )
	vResult.y = vCentre.y + fRadius * SIN( fAngle )
	vResult.z = vCentre.z
	RETURN vResult
ENDFUNC


FUNC VECTOR GET_RANDOM_POINT_ON_CIRCUMFERENCE( VECTOR vCentre, FLOAT fRadius )
	RETURN GET_POINT_ON_CIRCUMFERENCE( vCentre, fRadius, TO_FLOAT( GET_RANDOM_INT_IN_RANGE( 0, 360 ) ) )
ENDFUNC


FUNC VECTOR GET_POINT_ON_SPHERE_CIRCUMFERENCE( VECTOR vCentre, FLOAT fRadius, FLOAT fAngleAroundZ, FLOAT fDropAngle )
	VECTOR vResult
	vResult.x = fRadius * cos( fAngleAroundZ ) * sin ( fDropAngle )
	vResult.y = fRadius * sin( fAngleAroundZ ) * sin ( fDropAngle )
	vResult.z = fRadius * cos( fDropAngle )
	vResult	 += vCentre
	RETURN vResult
ENDFUNC

//INFO:			Author - Joseph Elvin
//PARAMS NOTES:	
//				f1 - Value at alpha = 0
//			    f2 - Value at alpha = 1
//    			fAlpha - Interp value between 0 and 1
//    			eInterpType - The desired interp type.
//PURPOSE:		Interpolates between two float values based on passed in alpha wrapping between 0 - 360
//				For example if f1 is 0 and f2 is 350, instead of interping 350 degrees between 0 - 350
//				it'll interp 10 degrees the opposite direction.
FUNC FLOAT INTERP_DEGREES(FLOAT f1, FLOAT f2, FLOAT fAlpha, INTERPOLATION_TYPE eInterpType = INTERPTYPE_LINEAR)
	
	FLOAT difference = ABSF(f2 - f1)
	IF difference > 180
		IF f2 > f1
			f1 += 360
		ELSE
			f2 += 360
		ENDIF
	ENDIF
	
	FLOAT value = INTERP_FLOAT(f1, f2, fAlpha, eInterpType)
	FLOAT rangeZero = 360
	
	IF value >= 0 AND value <= 360
		RETURN value
	ENDIF
	
	RETURN (value % rangeZero)
	
ENDFUNC

//INFO:			Author - Joseph Elvin
//PARAMS NOTES:	
//				v1 - Value at alpha = 0
//			    v2 - Value at alpha = 1
//    			fAlpha - Interp value between 0 and 1
//    			eInterpType - The desired interp type.
//PURPOSE:		Interpolates between two vectors based on passed in alpha wrapping between 0 - 360
//				For example if v1 is <<0,0,0>> and v2 is <<0,300,350>> instead of interping 300 degrees between 0 - 300
//				and 350 degrees between 0 - 350 it'll interp 60 degrees and 10 degrees the opposite directions.
FUNC VECTOR INTERP_ROTATION( VECTOR v1, VECTOR v2, FLOAT fAlpha, INTERPOLATION_TYPE eInterpType = INTERPTYPE_LINEAR )

	VECTOR vResult
	vResult.x = INTERP_DEGREES( v1.x, v2.x, fAlpha, eInterpType )
	vResult.y = INTERP_DEGREES( v1.y, v2.y, fAlpha, eInterpType )	
	vResult.z = INTERP_DEGREES( v1.z, v2.z, fAlpha, eInterpType )
	RETURN vResult
	
ENDFUNC






FUNC FLOAT GET_GRAPH_TYPE_SIN_ACCEL_DECEL(FLOAT t)
	FLOAT result
	
	IF (t > 0.0)
		IF (t < 1.0)
			result = 0.5 * (1.0 - COS(RAD_TO_DEG(t * CONST_PI)))
		ELSE
			result = 1.0
		ENDIF
	ELSE
		result = 0.0
	ENDIF
	
	RETURN result
ENDFUNC

FUNC FLOAT GET_GRAPH_TYPE_ACCEL(FLOAT t)
	FLOAT result
	
	IF (t > 0.0)
		IF (t < 1.0)
			result = 1.0 - COS(RAD_TO_DEG(t * CONST_PI * 0.5))
		ELSE
			result = 1.0
		ENDIF
	ELSE
		result = 0.0
	ENDIF
	
	RETURN result
ENDFUNC

FUNC FLOAT GET_GRAPH_TYPE_DECEL(FLOAT t)
	FLOAT result
	
	IF (t > 0.0)
		IF (t < 1.0)
			result = SIN(RAD_TO_DEG(t * CONST_PI * 0.5))
		ELSE
			result = 1.0
		ENDIF
	ELSE
		result = 0.0
	ENDIF
	
	RETURN result
ENDFUNC

PROC QUICK_SORT_INT_WITH_INDEXES(INT &iArray[], INT &iArrayIndex[], INT iLeft, INT iRight, BOOL bDescendingOrder = FALSE)

	INT i, j
	INT p = iArray[((iLeft + iRight) / 2)]
	INT q
	INT u
	i = iLeft
	j = iRight

	WHILE (i <= j)
		
		IF bDescendingOrder
			WHILE ((iArray[i] > p) AND (i < iRight))
				i++
			ENDWHILE
			
			WHILE ((p > iArray[j]) AND (j > iLeft))
				j--
			ENDWHILE
		ELSE
			WHILE ((iArray[i] < p) AND (i < iRight))
				i++
			ENDWHILE
			
			WHILE ((p < iArray[j]) AND (j > iLeft))
				j--
			ENDWHILE
		ENDIF
		
		IF (i <= j)
			q = iArray[i]
			u = iArrayIndex[i]
			iArray[i] = iArray[j]
			iArrayIndex[i] = iArrayIndex[j]
			iArray[j] = q
			iArrayIndex[j] = u
			i++
			j--
		ENDIF
	
	ENDWHILE
	
	IF (i < iRight)
		QUICK_SORT_INT_WITH_INDEXES(iArray, iArrayIndex, i, iRight, bDescendingOrder)
	ENDIF
	
	IF (iLeft < j)
		QUICK_SORT_INT_WITH_INDEXES(iArray, iArrayIndex, iLeft, j, bDescendingOrder)
	ENDIF

ENDPROC

PROC QUICK_SORT_FLOAT_WITH_INDEXES(FLOAT &fArray[], INT &iArray[], INT iLeft, INT iRight)

	INT i, j
	FLOAT p = fArray[((iLeft + iRight) / 2)]
	FLOAT q
	INT u
	i = iLeft
	j = iRight

	WHILE (i <= j)
	
		WHILE ((fArray[i] < p) AND (i < iRight))
			i++
		ENDWHILE
		
		WHILE ((p < fArray[j]) AND (j > iLeft))
			j--
		ENDWHILE
		
		IF (i <= j)
			q = fArray[i]
			u = iArray[i]
			fArray[i] = fArray[j]
			iArray[i] = iArray[j]
			fArray[j] = q
			iArray[j] = u
			i++
			j--
		ENDIF
	
	ENDWHILE
	
	IF (i < iRight)
		QUICK_SORT_FLOAT_WITH_INDEXES(fArray, iArray, i, iRight)
	ENDIF
	
	IF (iLeft < j)
		QUICK_SORT_FLOAT_WITH_INDEXES(fArray, iArray, iLeft, j)
	ENDIF

ENDPROC

/// PURPOSE:
///    Take an array of floats an perform divide & conquer quick sort.
///    Not stable
///	   O(log(n))
///	   O(n2) time, but typically O(n-log(n)) time
/// PARAMS:
///    fArray - Passed in array
///    iLeft - Start element
///    iRight - End element (length)
PROC QUICK_SORT_FLOAT(FLOAT &fArray[], INT iLeft, INT iRight)

	INT i, j
	FLOAT p = fArray[((iLeft + iRight) / 2)]
	FLOAT q
	i = iLeft
	j = iRight
	
	WHILE (i <= j)
	
		WHILE ((fArray[i] < p) AND (i < iRight))
			i++
		ENDWHILE
		
		WHILE ((p < fArray[j]) AND (j > iLeft))
			j--
		ENDWHILE
		
		IF (i <= j)
			q = fArray[i]
			fArray[i] = fArray[j]
			fArray[j] = q
			i++
			j--
		ENDIF
	
	ENDWHILE
	
	IF (i < iRight)
		QUICK_SORT_FLOAT(fArray, i, iRight)
	ENDIF
	
	IF (iLeft < j)
		QUICK_SORT_FLOAT(fArray, iLeft, j)
	ENDIF

ENDPROC

FUNC FLOAT NORMALIZE_ANGLE_POSITIVE(FLOAT fAngle)
	WHILE fAngle > 360.0
		fAngle -= 360.0
	ENDWHILE
	WHILE fAngle < -360.0
		fAngle += 360.0
	ENDWHILE
	
	RETURN fAngle
ENDFUNC

FUNC FLOAT NORMALIZE_ANGLE(FLOAT fAngle)
	WHILE fAngle > 180.0
		fAngle -= 360.0
	ENDWHILE
	WHILE fAngle < -180.0
		fAngle += 360.0
	ENDWHILE
	
	RETURN fAngle
ENDFUNC

FUNC VECTOR NORMALISE_ROTATION_3D(VECTOR vRot)
	vRot.x = NORMALIZE_ANGLE(vRot.x)
	vRot.y = NORMALIZE_ANGLE(vRot.y)
	vRot.z = NORMALIZE_ANGLE(vRot.z)
	
	RETURN vRot
ENDFUNC

STRUCT TRANSFORM_STRUCT
	VECTOR Position
	VECTOR Rotation
ENDSTRUCT


FUNC FLOAT GET_DISTANCE_BETWEEN_COORDS_SQUARED(VECTOR vCoord1,  VECTOR vCoord2)


	RETURN VDIST2(vCoord1, vCoord2)

ENDFUNC 

// [LM] - fInterpedVal = Pass in a value to slowly interp it to the target "fValToInterpTo"
// [LM] - fSpeed = advised range 0.5 and 4.0.
PROC INTERP_FLOAT_TO_TARGET_AT_SPEED(FLOAT &fInterpedVal, FLOAT fValToInterpTo, FLOAT fSpeed)
	FLOAT fValDiff
	IF fValToInterpTo < 0
		fValDiff = fInterpedVal - fValToInterpTo
		IF fInterpedVal > fValToInterpTo
			fInterpedVal -= fValDiff*(GET_FRAME_TIME()*fSpeed)
		ELIF fInterpedVal < fValToInterpTo
			fValDiff = fValToInterpTo - fInterpedVal
			fInterpedVal += fValDiff*(GET_FRAME_TIME()*fSpeed)
		ENDIF			
	ELIF fValToInterpTo > 0
		fValDiff = fValToInterpTo - fInterpedVal
		IF fInterpedVal < fValToInterpTo
			fInterpedVal += fValDiff*(GET_FRAME_TIME()*fSpeed)
		ELIF fInterpedVal > fValToInterpTo
			fValDiff = fInterpedVal - fValToInterpTo
			fInterpedVal -= fValDiff*(GET_FRAME_TIME()*fSpeed)	
		ENDIF
	ELSE
		IF fInterpedVal > fValToInterpTo
			fValDiff = fInterpedVal - fValToInterpTo
			fInterpedVal -= fValDiff*(GET_FRAME_TIME()*fSpeed)
		ELIF fInterpedVal < fValToInterpTo
			fValDiff = fValToInterpTo - fInterpedVal
			fInterpedVal += fValDiff*(GET_FRAME_TIME()*fSpeed)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Get sum of digits of num
FUNC INT GET_SUM_OF_INT_DIGITS(INT num)
	INT sum = 0 
	WHILE num != 0
		sum = sum + num % 10 
		num = num/10
	ENDWHILE
	
	RETURN sum
ENDFUNC

/// PURPOSE:
///    Check if number is even, odd if returns false
FUNC BOOL IS_INT_EVEN(INT iNum)
	IF iNum % 2 = 0
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC
