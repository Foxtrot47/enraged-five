
USING "script_maths.sch"

	// We can only have one bugstar query running at any one time so use this
	// flag to tell if we need to wait for one to finish before starting another.
	// Scripts should use the commands in scripts_bugstar.sch to check if it is
	// safe to start a new query and to set when a query has started/finished.
STRUCT BUGSTAR_QUERY
	BOOL 	bQueriesDisabled
	BOOL 	bQueryStarted
	INT 	iQueryID
ENDSTRUCT

ENUM BUGSTAR_PROCESS_FLAGS
	BSTAR_PROCESS_NONE						= 0,
	BSTAR_PROCESS_BUGSTAR_QUERY_RESET	= BIT12,
	BSTAR_PROCESS_BUGSTAR_QUERY_STARTED	= BIT13,
	BSTAR_PROCESS_BUGSTAR_QUERY_PENDING	= BIT14
ENDENUM
CONST_INT DM_MAX_BUGSTAR_DEPTS 15
CONST_INT DM_MAX_MISSION_BUGS 10

STRUCT BUGSTAR_DATA_STRUCT
	BUGSTAR_PROCESS_FLAGS	eProcessFlags
	INT iABugs
	INT iBBugs
	INT iCBugs
	INT iDBugs
	INT iTasks
	INT iTodos
	INT iWish
	INT iFixedBugs
	INT iOverdueBugs
	INT iAvgBugAge
	
	INT iTop5Bugs[5]
	INT iTop5BugsAvgAge[5]
	
	INT iOwnerCount
	
	INT iDeptBugs[DM_MAX_BUGSTAR_DEPTS]
	
	INT iDepthToLoadBasicInfo
	
	INT iBugstarID
	
	TEXT_LABEL_63	sBugstarFile					// Bugstar file that holds mission info
ENDSTRUCT

STRUCT MISSION_BUGS_STRUCT
	INT iBugNumber[DM_MAX_MISSION_BUGS]	// Segment of total bugs
	INT iDaysOpen[DM_MAX_MISSION_BUGS]
	INT iCurrentItem	// Current item in segment
	TEXT_LABEL_15 sMissionID // MissionID of current item
	INT iTopItem		// First item of total bugs
	INT iTotalItems		// Total bugs this mission has
	BOOL bDisplay
	BOOL bInitialised
	TEXT_LABEL_31 sCurrentDate
ENDSTRUCT



PROC SET_BUGSTAR_PROCESS_FLAG(BUGSTAR_PROCESS_FLAGS &eProcessFlags, BUGSTAR_PROCESS_FLAGS eFlagToSet)
	eProcessFlags = eProcessFlags | eFlagToSet
ENDPROC

PROC CLEAR_BUGSTAR_PROCESS_FLAG(BUGSTAR_PROCESS_FLAGS &eProcessFlags, BUGSTAR_PROCESS_FLAGS eFlagToClear)
	eProcessFlags -= eProcessFlags & eFlagToClear
ENDPROC

FUNC BOOL IS_BUGSTAR_PROCESS_FLAG_SET(BUGSTAR_PROCESS_FLAGS eProcessFlags, BUGSTAR_PROCESS_FLAGS eFlagToCheck)
	RETURN (eProcessFlags & eFlagToCheck) != BSTAR_PROCESS_NONE
ENDFUNC
