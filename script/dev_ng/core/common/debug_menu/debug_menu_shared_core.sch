//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	Shared_Debug_Menu.sch										//
//		AUTHOR			:	Kenneth Ross												//
//		DESCRIPTION		:	Containts structs, enums, and commands for use with the		//
//							debug menu.													//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////


// Include the standard headers (which also ensures the correct #DEFINEs are recognised)
USING "rage_builtins.sch"
USING "globals.sch"

// Do nothing in release mode
#IF IS_FINAL_BUILD
SCRIPT
ENDSCRIPT
#ENDIF

// Only include in debug mode
#IF IS_DEBUG_BUILD

USING "commands_xml.sch"
USING "commands_debug.sch"
USING "commands_misc.sch"
USING "commands_pad.sch"
USING "commands_hud.sch"
USING "commands_graphics.sch"
USING "commands_script.sch"
USING "commands_network.sch"
USING "commands_clock.sch"
USING "script_bugstar.sch"
USING "script_buttons.sch"
USING "debug_menu_structs_core.sch"
USING "debug_menu_shared_GAME.sch"

#IF NOT DEFINED(GET_STRING_FOR_CODEID)
FUNC STRING GET_STRING_FOR_CODEID(SP_MENU_CODE_ID_ENUM codeID)
	SWITCH codeID
		CASE SPCID_DEFAULT
			RETURN "Default"
		DEFAULT
			RETURN "Unknown Command"
	ENDSWITCH
	RETURN "Unknown Command"
ENDFUNC
#ENDIF

/// PURPOSE:
///    Converts a single character string input into a prioritized INT (not ASCII)
/// PARAMS:
///    passedSingleCharacter - STRING which contains a single character
/// RETURNS:
///    A prioritized INT, can be used for alphabetization
FUNC INT CONVERT_SINGLE_CHARACTER_TO_INT(STRING passedSingleCharacter)    	
    IF ARE_STRINGS_EQUAL(passedSingleCharacter, "A")
        RETURN 10
    ENDIF
    IF ARE_STRINGS_EQUAL(passedSingleCharacter, "B")
        RETURN 11
    ENDIF
    IF ARE_STRINGS_EQUAL(passedSingleCharacter, "C")
        RETURN 12
    ENDIF
    IF ARE_STRINGS_EQUAL(passedSingleCharacter, "D")
        RETURN 13
    ENDIF
    IF ARE_STRINGS_EQUAL(passedSingleCharacter, "E")
        RETURN 14
    ENDIF
    IF ARE_STRINGS_EQUAL(passedSingleCharacter, "F")
        RETURN 15
    ENDIF    
    IF ARE_STRINGS_EQUAL(passedSingleCharacter, "G")
        RETURN 16
    ENDIF
    IF ARE_STRINGS_EQUAL(passedSingleCharacter, "H")
        RETURN 17
    ENDIF
    IF ARE_STRINGS_EQUAL(passedSingleCharacter, "I")
        RETURN 18
    ENDIF
    IF ARE_STRINGS_EQUAL(passedSingleCharacter, "J")
        RETURN 19
    ENDIF
    IF ARE_STRINGS_EQUAL(passedSingleCharacter, "K")
        RETURN 20
    ENDIF
    IF ARE_STRINGS_EQUAL(passedSingleCharacter, "L")
        RETURN 21
    ENDIF
    IF ARE_STRINGS_EQUAL(passedSingleCharacter, "M")
        RETURN 22
    ENDIF
    IF ARE_STRINGS_EQUAL(passedSingleCharacter, "N")
        RETURN 23
    ENDIF
    IF ARE_STRINGS_EQUAL(passedSingleCharacter, "O")
        RETURN 24
    ENDIF
    IF ARE_STRINGS_EQUAL(passedSingleCharacter, "P")
        RETURN 25
    ENDIF
    IF ARE_STRINGS_EQUAL(passedSingleCharacter, "Q")
        RETURN 26
    ENDIF
    IF ARE_STRINGS_EQUAL(passedSingleCharacter, "R")
        RETURN 27
    ENDIF    
    IF ARE_STRINGS_EQUAL(passedSingleCharacter, "S")
        RETURN 28
    ENDIF
    IF ARE_STRINGS_EQUAL(passedSingleCharacter, "T")
        RETURN 29
    ENDIF
    IF ARE_STRINGS_EQUAL(passedSingleCharacter, "U")
        RETURN 30
    ENDIF
    IF ARE_STRINGS_EQUAL(passedSingleCharacter, "V")
        RETURN 31
    ENDIF
    IF ARE_STRINGS_EQUAL(passedSingleCharacter, "W")
        RETURN 32
    ENDIF
    IF ARE_STRINGS_EQUAL(passedSingleCharacter, "X")
        RETURN 33
    ENDIF
    IF ARE_STRINGS_EQUAL(passedSingleCharacter, "Y")
        RETURN 34
    ENDIF
    IF ARE_STRINGS_EQUAL(passedSingleCharacter, "Z")
        RETURN 35
    ENDIF
	IF ARE_STRINGS_EQUAL(passedSingleCharacter, "0")
        RETURN 0
    ENDIF
    IF ARE_STRINGS_EQUAL(passedSingleCharacter, "1")
        RETURN 1
    ENDIF
    IF ARE_STRINGS_EQUAL(passedSingleCharacter, "2")
        RETURN 2
    ENDIF
    IF ARE_STRINGS_EQUAL(passedSingleCharacter, "3")
        RETURN 3
    ENDIF
    IF ARE_STRINGS_EQUAL(passedSingleCharacter, "4")
        RETURN 4
    ENDIF
    IF ARE_STRINGS_EQUAL(passedSingleCharacter, "5")
        RETURN 5
    ENDIF    
    IF ARE_STRINGS_EQUAL(passedSingleCharacter, "6")
        RETURN 6
    ENDIF
    IF ARE_STRINGS_EQUAL(passedSingleCharacter, "7")
        RETURN 7
    ENDIF
    IF ARE_STRINGS_EQUAL(passedSingleCharacter, "8")
        RETURN 8
    ENDIF
    IF ARE_STRINGS_EQUAL(passedSingleCharacter, "9")
        RETURN 9
    ENDIF
	RETURN 37
ENDFUNC


//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		VALIDATION CHECKS																//
//    																					//
//////////////////////////////////////////////////////////////////////////////////////////

/// PURPOSE: Checks to see if the specified string is a valid script file
FUNC BOOL DM_CHECK_IF_SCRIPT_EXISTS(STRING sFile)
	
	TEXT_LABEL_63 sTemp = sFile
	
	IF ARE_STRINGS_EQUAL(GET_STRING_FROM_STRING(sTemp, GET_LENGTH_OF_LITERAL_STRING(sTemp)-3, GET_LENGTH_OF_LITERAL_STRING(sTemp)), ".sc")
		sTemp = GET_STRING_FROM_STRING(sTemp, 0, GET_LENGTH_OF_LITERAL_STRING(sTemp)-3)
	ENDIF
	
	IF NOT DOES_SCRIPT_EXIST(sTemp)
		CERRORLN(DEBUG_DEBUG_MENU, "Debug Menu - Script file in XML doesn't exist '", sTemp, "'")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE: Attempts to get the next menu item on the current list
FUNC BOOL DM_GET_NEXT_MENU_ITEM(DEBUG_MENU_DATA_STRUCT &sDebugMenuData, INT iDepth, BOOL bBreak = FALSE, BOOL bLoop = TRUE)
	
	// First pass: start+1 TO last
	IF sDebugMenuData.iCurrentItem[iDepth] < sDebugMenuData.iItemCount[iDepth]-1 // Check we aren't at the end of the array
		INT i = sDebugMenuData.iCurrentItem[iDepth]+1
			
		WHILE i < sDebugMenuData.iItemCount[iDepth]
			IF IS_DEBUG_MENU_ITEM_FLAG_SET(sDebugMenuData.sItems[iDepth][i].iFlags, DM_BIT_DISPLAY)
				IF (sDebugMenuData.sItems[iDepth][i].iType <> DM_HASH_ITEM_BREAK)
				OR (sDebugMenuData.sItems[iDepth][i].iType = DM_HASH_ITEM_BREAK AND bBreak)
					sDebugMenuData.iCurrentItem[iDepth] = i
					RETURN TRUE
				ENDIF
			ENDIF
			i++
		ENDWHILE
	ENDIF
	
	// Second pass: first TO start-1
	IF bLoop
		INT i = 0
		
		WHILE i < sDebugMenuData.iCurrentItem[iDepth]
			IF IS_DEBUG_MENU_ITEM_FLAG_SET(sDebugMenuData.sItems[iDepth][i].iFlags, DM_BIT_DISPLAY)
				IF (sDebugMenuData.sItems[iDepth][i].iType <> DM_HASH_ITEM_BREAK)
				OR (sDebugMenuData.sItems[iDepth][i].iType = DM_HASH_ITEM_BREAK AND bBreak)
					sDebugMenuData.iCurrentItem[iDepth] = i
					RETURN TRUE
				ENDIF
			ENDIF
			i++
		ENDWHILE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE: Attempts to get the previous menu item on the current list
FUNC BOOL DM_GET_PREVIOUS_MENU_ITEM(DEBUG_MENU_DATA_STRUCT &sDebugMenuData, INT iDepth, BOOL bBreak = FALSE, BOOL bLoop = TRUE)

	// First pass: start-1 TO first
	IF sDebugMenuData.iCurrentItem[iDepth] > 0 // Check we aren't at the start of the array
		INT i = sDebugMenuData.iCurrentItem[iDepth]-1
		
		WHILE i >= 0
			IF IS_DEBUG_MENU_ITEM_FLAG_SET(sDebugMenuData.sItems[iDepth][i].iFlags, DM_BIT_DISPLAY)
				IF (sDebugMenuData.sItems[iDepth][i].iType <> DM_HASH_ITEM_BREAK)
				OR (sDebugMenuData.sItems[iDepth][i].iType = DM_HASH_ITEM_BREAK AND bBreak)
					sDebugMenuData.iCurrentItem[iDepth] = i
					RETURN TRUE
				ENDIF
			ENDIF
			i--
		ENDWHILE
	ENDIF
	
	// Second pass: last TO start+1
	IF bLoop
		INT i = sDebugMenuData.iItemCount[iDepth]-1
		
		WHILE i > sDebugMenuData.iCurrentItem[iDepth]
			IF IS_DEBUG_MENU_ITEM_FLAG_SET(sDebugMenuData.sItems[iDepth][i].iFlags, DM_BIT_DISPLAY)
				IF (sDebugMenuData.sItems[iDepth][i].iType <> DM_HASH_ITEM_BREAK)
				OR (sDebugMenuData.sItems[iDepth][i].iType = DM_HASH_ITEM_BREAK AND bBreak)
					sDebugMenuData.iCurrentItem[iDepth]= i
					RETURN TRUE
				ENDIF
			ENDIF
			i--
		ENDWHILE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE: Attempts to get another item if the current is not valid selection
FUNC BOOL DM_GET_VALID_MENU_ITEMS(DEBUG_MENU_DATA_STRUCT &sDebugMenuData)

	BOOL bPrevListUpdated = FALSE

	INT iDepth
	FOR iDepth= 0 TO DM_MAX_DEPTH-1
		
		IF bPrevListUpdated
		ENDIF
		
		IF sDebugMenuData.sItems[iDepth][sDebugMenuData.iCurrentItem[iDepth]].iType = DM_HASH_ITEM_BREAK
		OR NOT IS_DEBUG_MENU_ITEM_FLAG_SET(sDebugMenuData.sItems[iDepth][sDebugMenuData.iCurrentItem[iDepth]].iFlags, DM_BIT_DISPLAY)
			IF DM_GET_NEXT_MENU_ITEM(sDebugMenuData, iDepth)
				bPrevListUpdated = TRUE
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN TRUE
ENDFUNC

/// PURPOSE: Attempts to get another item if the current is not valid selection
FUNC BOOL DM_GET_VALID_MENU_ITEM(DEBUG_MENU_DATA_STRUCT &sDebugMenuData, INT iDepth)
	IF sDebugMenuData.sItems[iDepth][sDebugMenuData.iCurrentItem[iDepth]].iType = DM_HASH_ITEM_BREAK
	OR NOT IS_DEBUG_MENU_ITEM_FLAG_SET(sDebugMenuData.sItems[iDepth][sDebugMenuData.iCurrentItem[iDepth]].iFlags, DM_BIT_DISPLAY)
		RETURN DM_GET_NEXT_MENU_ITEM(sDebugMenuData, iDepth)
	ENDIF
	
	RETURN TRUE
ENDFUNC

//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		PROCS TO LOAD AND SORT THE DATA													//
//    																					//
//////////////////////////////////////////////////////////////////////////////////////////
#IF NOT DEFINED(DM_CLEAR_PROCESS_FLAGS_FOR_CLEANUP_CUSTOM) 
/// PURPOSE:
///   	Project-specific func for clearing process flags on menu shutdown
/// PARAMS:
///    sDebugMenuData - Master debug menu data
PROC DM_CLEAR_PROCESS_FLAGS_FOR_CLEANUP_CUSTOM(DEBUG_MENU_DATA_STRUCT &sDebugMenuData)
	sDebugMenuData.bMouseMoveUp = sDebugMenuData.bMouseMoveUp
ENDPROC
#ENDIF

/// PURPOSE:
///    Clears all process flags on menu shutdown, so they are fresh when the menu is next opened.
///    Calls a project-specific cleanup func.
/// PARAMS:
///    sDebugMenuData - Master debug menu data
PROC DM_CLEAR_PROCESS_FLAGS_FOR_CLEANUP(DEBUG_MENU_DATA_STRUCT &sDebugMenuData)
	// Clear all process flags except the bugstar query flags
	CLEAR_DEBUG_MENU_PROCESS_FLAG(sDebugMenuData.iProcessFlags, DM_PROCESS_LAUNCH)
	CLEAR_DEBUG_MENU_PROCESS_FLAG(sDebugMenuData.iProcessFlags, DM_PROCESS_WAIT)
	CLEAR_DEBUG_MENU_PROCESS_FLAG(sDebugMenuData.iProcessFlags, DM_PROCESS_PRE_INITIALISED)
	CLEAR_DEBUG_MENU_PROCESS_FLAG(sDebugMenuData.iProcessFlags, DM_PROCESS_INITIALISED)
	CLEAR_DEBUG_MENU_PROCESS_FLAG(sDebugMenuData.iProcessFlags, DM_PROCESS_INITIALISE_NEXT_MENU)
	CLEAR_DEBUG_MENU_PROCESS_FLAG(sDebugMenuData.iProcessFlags, DM_PROCESS_RESTORE_SELECTION)
	CLEAR_DEBUG_MENU_PROCESS_FLAG(sDebugMenuData.iProcessFlags, DM_PROCESS_RESTORE_SORT)
	CLEAR_DEBUG_MENU_PROCESS_FLAG(sDebugMenuData.iProcessFlags, DM_PROCESS_UPDATE_MENU_ITEM)
	CLEAR_DEBUG_MENU_PROCESS_FLAG(sDebugMenuData.iProcessFlags, DM_PROCESS_CLEANUP)
	CLEAR_DEBUG_MENU_PROCESS_FLAG(sDebugMenuData.iProcessFlags, DM_PROCESS_BUGSTAR_GET_BASIC)
	CLEAR_DEBUG_MENU_PROCESS_FLAG(sDebugMenuData.iProcessFlags, DM_PROCESS_BUGSTAR_GET_DETAILED)
	CLEAR_DEBUG_MENU_PROCESS_FLAG(sDebugMenuData.iProcessFlags, DM_PROCESS_BUGSTAR_GET_BUGS)
	DM_CLEAR_PROCESS_FLAGS_FOR_CLEANUP_CUSTOM(sDebugMenuData)
ENDPROC

/// PURPOSE:
///    Turns debub menu on and off
/// PARAMS:
///    sDebugMenuData - Master debug menu data
PROC DM_TOGGLE_MENU_LAUNCH_STATE(DEBUG_MENU_DATA_STRUCT &sDebugMenuData)
	IF IS_DEBUG_MENU_PROCESS_FLAG_SET(sDebugMenuData.iProcessFlags, DM_PROCESS_LAUNCH)
		SET_DEBUG_MENU_PROCESS_FLAG(sDebugMenuData.iProcessFlags, DM_PROCESS_CLEANUP)
	ELSE
		SET_DEBUG_MENU_PROCESS_FLAG(sDebugMenuData.iProcessFlags, DM_PROCESS_LAUNCH)
	ENDIF
ENDPROC

/// PURPOSE: Stores the last item selection so we can restore later
PROC DM_UPDATE_LAST_MENU_SELECTION(DEBUG_MENU_DATA_STRUCT &sData)

	INT i
	REPEAT DM_MAX_DEPTH i
		g_debugMenuControl.iDMTitleHash[i] = GET_HASH_KEY(sData.sItems[i][sData.iCurrentItem[i]].sTitle)
		g_debugMenuControl.iDMUniqueID[i] = sData.sItems[i][sData.iCurrentItem[i]].iUniqueID
		g_debugMenuControl.iDMCurrentItem[i] = sData.iCurrentItem[i]
		g_debugMenuControl.iDMTopItem[i] = sData.iTopItem[i]
		g_debugMenuControl.iDMUnderItem[i] = sData.iUnderItem[i]
		g_debugMenuControl.bDMSelectionRestored[i] = FALSE
	ENDREPEAT
	
	g_debugMenuControl.iDMSortType = ENUM_TO_INT(sData.eMissionSort)
	g_debugMenuControl.iDMCurrentDepth = sData.iCurrentDepth
ENDPROC

/// PURPOSE: Restores the last item selection
FUNC BOOL DM_RESTORE_LAST_MENU_SELECTION(DEBUG_MENU_DATA_STRUCT &sData, INT iDepth)

	// Compare the item position and title
	IF sData.sItems[iDepth][g_debugMenuControl.iDMCurrentItem[iDepth]].iUniqueID = g_debugMenuControl.iDMUniqueID[iDepth]
	AND GET_HASH_KEY(sData.sItems[iDepth][g_debugMenuControl.iDMCurrentItem[iDepth]].sTitle) = g_debugMenuControl.iDMTitleHash[iDepth]
	
		// Make sure the item is on display
		IF IS_DEBUG_MENU_ITEM_FLAG_SET(sData.sItems[iDepth][g_debugMenuControl.iDMCurrentItem[iDepth]].iFlags, DM_BIT_DISPLAY)
		
			// Make sure we dont pass the last browse depth
			IF iDepth <= g_debugMenuControl.iDMCurrentDepth
			
				// Update the states
				sData.iCurrentItem[iDepth] 	= g_debugMenuControl.iDMCurrentItem[iDepth]
				sData.iTopItem[iDepth] 		= g_debugMenuControl.iDMTopItem[iDepth]
				sData.iUnderItem[iDepth] 	= g_debugMenuControl.iDMUnderItem[iDepth]
				
				g_debugMenuControl.bDMSelectionRestored[iDepth] = TRUE
				
				RETURN TRUE
				
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE: Removed any debug text that was added for the list of bugs
PROC DM_REMOVE_BUGSTAR_DEBUG_BUG_TEXT(MISSION_BUGS_STRUCT &sBugData)
	
	INT i
	TEXT_LABEL_15 tlTemp
	
	REPEAT DM_MAX_MISSION_BUGS i
		// Summary
		tlTemp = ""
		tlTemp += sBugData.iBugNumber[i]
		tlTemp += "_S"
		//PRINTLN("REMOVE_DEBUG_STRING with label: ", tlTemp)
		REMOVE_DEBUG_STRING_WITH_THIS_KEY(tlTemp)
		
		// Developer
		tlTemp = ""
		tlTemp += sBugData.iBugNumber[i]
		tlTemp += "_D"
		//PRINTLN("REMOVE_DEBUG_STRING with label: ", tlTemp)
		REMOVE_DEBUG_STRING_WITH_THIS_KEY(tlTemp)
		
		// Category
		tlTemp = ""
		tlTemp += sBugData.iBugNumber[i]
		tlTemp += "_C"
		//PRINTLN("REMOVE_DEBUG_STRING with label: ", tlTemp)
		REMOVE_DEBUG_STRING_WITH_THIS_KEY(tlTemp)
		
		// Due date
		tlTemp = ""
		tlTemp += sBugData.iBugNumber[i]
		tlTemp += "_DD"
		//PRINTLN("REMOVE_DEBUG_STRING with label: ", tlTemp)
		REMOVE_DEBUG_STRING_WITH_THIS_KEY(tlTemp)
		
		// Days open
		tlTemp = ""
		tlTemp += sBugData.iBugNumber[i]
		tlTemp += "_DO"
		//PRINTLN("REMOVE_DEBUG_STRING with label: ", tlTemp)
		REMOVE_DEBUG_STRING_WITH_THIS_KEY(tlTemp)
		
		INT iOwner
		
		// Dept owners
		REPEAT DM_MAX_BUGSTAR_DEPTS iOwner
			tlTemp = ""
			tlTemp += sBugData.sMissionID
			tlTemp += "DPTOWN"
			tlTemp += iOwner
			//PRINTLN("REMOVE_DEBUG_STRING with label: ", tlTemp)
			REMOVE_DEBUG_STRING_WITH_THIS_KEY(tlTemp)
		ENDREPEAT
		
		// Top 5 owners
		REPEAT 5 iOwner
			tlTemp = ""
			tlTemp += sBugData.sMissionID
			tlTemp += "T5OWN"
			tlTemp += iOwner
			//PRINTLN("REMOVE_DEBUG_STRING with label: ", tlTemp)
			REMOVE_DEBUG_STRING_WITH_THIS_KEY(tlTemp)
		ENDREPEAT
		
	ENDREPEAT
ENDPROC

/// PURPOSE: Removed any debug text that was added for Bugstar data
///    NOTE: Use -1 for iDepth to clear all depths
PROC DM_REMOVE_BUGSTAR_DEBUG_TEXT(DEBUG_MENU_DATA_STRUCT &sData, INT iDepth)

	INT i, j
	TEXT_LABEL_15 tlTemp
	FOR i = 0 TO DM_MAX_DEPTH-1
		IF iDepth = -1 // All depths
		OR iDepth <= i // All depths greater than specified
			// Find any bugstar items and remove mission ids
			FOR j = 0 TO DM_MAX_ITEMS_PER_DEPTH-1
				
				// Mission description
				tlTemp = ""
				tlTemp += sData.sItems[i][j].sMissionID
				tlTemp += "_MD1"
				//PRINTLN("REMOVE_DEBUG_STRING with label: ", tlTemp)
				REMOVE_DEBUG_STRING_WITH_THIS_KEY(tlTemp)
				
				// Story description
				tlTemp = ""
				tlTemp += sData.sItems[i][j].sMissionID
				tlTemp += "_SD1"
				//PRINTLN("REMOVE_DEBUG_STRING with label: ", tlTemp)
				REMOVE_DEBUG_STRING_WITH_THIS_KEY(tlTemp)
				
				// Mission comments
				tlTemp = ""
				tlTemp += sData.sItems[i][j].sMissionID
				tlTemp += "_MC1"
				//PRINTLN("REMOVE_DEBUG_STRING with label: ", tlTemp)
				REMOVE_DEBUG_STRING_WITH_THIS_KEY(tlTemp)
			ENDFOR
		ENDIF
	ENDFOR
	
	// Bug summary
	tlTemp = ""
	tlTemp += sData.sBugData.sMissionID
	tlTemp += "_S"
	//PRINTLN("REMOVE_DEBUG_STRING with label: ", tlTemp)
	REMOVE_DEBUG_STRING_WITH_THIS_KEY(tlTemp)
	
	// Bug class
	tlTemp = ""
	tlTemp += sData.sBugData.sMissionID
	tlTemp += "_C"
	//PRINTLN("REMOVE_DEBUG_STRING with label: ", tlTemp)
	REMOVE_DEBUG_STRING_WITH_THIS_KEY(tlTemp)
	
	// Bug due date
	tlTemp = ""
	tlTemp += sData.sBugData.sMissionID
	tlTemp += "_DD"
	//PRINTLN("REMOVE_DEBUG_STRING with label: ", tlTemp)
	REMOVE_DEBUG_STRING_WITH_THIS_KEY(tlTemp)
	
	// Days open
	tlTemp = ""
	tlTemp += sData.sBugData.sMissionID
	tlTemp += "_DO"
	//PRINTLN("REMOVE_DEBUG_STRING with label: ", tlTemp)
	REMOVE_DEBUG_STRING_WITH_THIS_KEY(tlTemp)
	
	// Bug developer
	tlTemp = ""
	tlTemp += sData.sBugData.sMissionID
	tlTemp += "_D"
	//PRINTLN("REMOVE_DEBUG_STRING with label: ", tlTemp)
	REMOVE_DEBUG_STRING_WITH_THIS_KEY(tlTemp)
	
	INT iOwner
	
	// Dept owners
	REPEAT DM_MAX_BUGSTAR_DEPTS iOwner
		tlTemp = ""
		tlTemp += sData.sBugData.sMissionID
		tlTemp += "DPTOWN"
		tlTemp += iOwner
		//PRINTLN("REMOVE_DEBUG_STRING with label: ", tlTemp)
		REMOVE_DEBUG_STRING_WITH_THIS_KEY(tlTemp)
	ENDREPEAT
	
	// Top 5 owners
	REPEAT 5 iOwner
		tlTemp = ""
		tlTemp += sData.sBugData.sMissionID
		tlTemp += "T5OWN"
		tlTemp += iOwner
		//PRINTLN("REMOVE_DEBUG_STRING with label: ", tlTemp)
		REMOVE_DEBUG_STRING_WITH_THIS_KEY(tlTemp)
	ENDREPEAT
ENDPROC

FUNC INT DM_GET_INT_VALUE_OF_CHARACTER(STRING s)

	INT i
	TEXT_LABEL_63 sRangeUpper = " _0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	TEXT_LABEL_63 sRangeLower = " _0123456789abcdefghijklmnopqrstuvwxyz"
	TEXT_LABEL_3  sString = s
	
	// Cycle through all the numbers/letters
	FOR i = 0 TO GET_LENGTH_OF_LITERAL_STRING(sRangeUpper)-1

		// Compare the specified string with the current string in the cycle
		IF ARE_STRINGS_EQUAL(sString, GET_STRING_FROM_STRING(sRangeUpper, i, i+1))
		OR ARE_STRINGS_EQUAL(sString, GET_STRING_FROM_STRING(sRangeLower, i, i+1))	
			RETURN i
		ENDIF
		
	ENDFOR
	
	RETURN -1

ENDFUNC

/// PURPOSE: Arranges the item list to match sort criteria
///    NOTES: Only mission items on same depth will be sorted
PROC DM_SORT_MENU_LIST(DEBUG_MENU_DATA_STRUCT &sDebugMenuData, INT iDepth)
	
	// Some vars to help us sort
	BOOL bNoChange, bFoundDiff
	INT iCurrentItem, iSeekItem, iLenCurrent, iLenSeek, iCurrent, iSeek, iChar
	TEXT_LABEL_3 sCurrent, sSeek
	DEBUG_MENU_ITEM_STRUCT sTempItem
	
	// When we sort we want to keep the same item selected
	// We will only need to do this for the current depth
	// as the previous depth items will all be menus.
	INT iUniqueID = sDebugMenuData.sItems[iDepth][sDebugMenuData.iCurrentItem[iDepth]].iUniqueID
	
	SWITCH sDebugMenuData.eMissionSort
	
		CASE DM_SORT_DEFAULT
		
			// When we built the original list, the iUniqueID matched the sItems[] array index (iItemCount)
			// So we can simply traverse through the list to make sure the iUniqueID matches the sItem[] index
			
			// Go through all the items
			FOR iCurrentItem = 0 TO sDebugMenuData.iItemCount[iDepth]-1
			
				// We only want to deal with mission item types
				IF GET_HASH_KEY(sDebugMenuData.sItems[iDepth][iCurrentItem].sMissionID) != 0
				//IF sDebugMenuData.sItems[iDepth][iCurrentItem].iType = DM_HASH_ITEM_MISSION
				//OR sDebugMenuData.sItems[iDepth][iCurrentItem].iType = DM_HASH_ITEM_BUGSTAR
			
					// If the item is not in the original spot
					IF sDebugMenuData.sItems[iDepth][iCurrentItem].iUniqueID <> iCurrentItem
						
						// Cycle through all the items to find the correct one
						bNoChange = TRUE
						iSeekItem = iCurrentItem+1
						WHILE bNoChange
						AND iSeekItem < sDebugMenuData.iItemCount[iDepth]
							
							// Swap items when we find the correct one
							IF sDebugMenuData.sItems[iDepth][iSeekItem].iUniqueID = iCurrentItem
								
								// Make copy of item in the current slot
								sTempItem = sDebugMenuData.sItems[iDepth][iCurrentItem]
								
								// Put the correct item into the slot
								sDebugMenuData.sItems[iDepth][iCurrentItem] = sDebugMenuData.sItems[iDepth][iSeekItem]
								
								// Put the copy into the old correct item slot
								sDebugMenuData.sItems[iDepth][iSeekItem] = sTempItem
								
								// Bail out of the loop							
								bNoChange = FALSE
							ENDIF
							
							iSeekItem++
						
						ENDWHILE
					
					// Original spot?
					ENDIF
				
				// Mission item?
				ENDIF
			
			// End of item cycle
			ENDFOR
					
		BREAK
		
		CASE DM_SORT_PERCENTAGE_ASC
		CASE DM_SORT_PERCENTAGE_DESC
		
			// Go through all the items
			FOR iCurrentItem = 0 TO sDebugMenuData.iItemCount[iDepth]-1
			
				// We only want to deal with mission item types
				IF GET_HASH_KEY(sDebugMenuData.sItems[iDepth][iCurrentItem].sMissionID) != 0
				//IF sDebugMenuData.sItems[iDepth][iCurrentItem].iType = DM_HASH_ITEM_MISSION
				//OR sDebugMenuData.sItems[iDepth][iCurrentItem].iType = DM_HASH_ITEM_BUGSTAR
				
					// Compare item with the rest of the items in the list
					FOR iSeekItem = iCurrentItem+1  TO sDebugMenuData.iItemCount[iDepth]-1
					
						// Check we are in range
						IF iSeekItem < sDebugMenuData.iItemCount[iDepth]
						
							// We only want to deal with mission items
							IF GET_HASH_KEY(sDebugMenuData.sItems[iDepth][iSeekItem].sMissionID) != 0
							//IF sDebugMenuData.sItems[iDepth][iSeekItem].iType = DM_HASH_ITEM_MISSION
							//OR sDebugMenuData.sItems[iDepth][iSeekItem].iType = DM_HASH_ITEM_BUGSTAR
							
								// Compare percentages and decide if we should swap items
								IF (sDebugMenuData.eMissionSort = DM_SORT_PERCENTAGE_ASC  AND sDebugMenuData.sItems[iDepth][iCurrentItem].iPerc > sDebugMenuData.sItems[iDepth][iSeekItem].iPerc)
								OR (sDebugMenuData.eMissionSort = DM_SORT_PERCENTAGE_DESC AND sDebugMenuData.sItems[iDepth][iCurrentItem].iPerc < sDebugMenuData.sItems[iDepth][iSeekItem].iPerc)
																			
									// Make copy of item in the current slot
									sTempItem = sDebugMenuData.sItems[iDepth][iCurrentItem]
						
									// Put the correct item into the slot
									sDebugMenuData.sItems[iDepth][iCurrentItem] = sDebugMenuData.sItems[iDepth][iSeekItem]
									
									// Put the copy into the old correct item slot
									sDebugMenuData.sItems[iDepth][iSeekItem] = sTempItem
								
								// Swap items?
								ENDIF
								
							// Mission item?
							ENDIF
							
						// In range?
						ENDIF
						
					// End of seek item cycle
					ENDFOR
				
				// Mission item?
				ENDIF
		
			// End of item cycle
			ENDFOR
		
		BREAK
		
		CASE DM_SORT_BUGSTAR_ASC
		CASE DM_SORT_BUGSTAR_DESC
		
			// Go through all the items
			FOR iCurrentItem = 0 TO sDebugMenuData.iItemCount[iDepth]-1
			
				// We only want to deal with mission item types
				IF GET_HASH_KEY(sDebugMenuData.sItems[iDepth][iCurrentItem].sMissionID) != 0
				//IF sDebugMenuData.sItems[iDepth][iCurrentItem].iType = DM_HASH_ITEM_MISSION
				//OR sDebugMenuData.sItems[iDepth][iCurrentItem].iType = DM_HASH_ITEM_BUGSTAR
				
					// Compare item with the rest of the items in the list
					FOR iSeekItem = iCurrentItem+1  TO sDebugMenuData.iItemCount[iDepth]-1
							
						// We only want to deal with mission items
						IF GET_HASH_KEY(sDebugMenuData.sItems[iDepth][iSeekItem].sMissionID) != 0
						//IF sDebugMenuData.sItems[iDepth][iSeekItem].iType = DM_HASH_ITEM_MISSION
						//OR sDebugMenuData.sItems[iDepth][iSeekItem].iType = DM_HASH_ITEM_BUGSTAR
							
							iChar		 = 0
							bFoundDiff	 = FALSE
							iLenCurrent  = GET_LENGTH_OF_LITERAL_STRING(sDebugMenuData.sItems[iDepth][iCurrentItem].sMissionID)
							iLenSeek	 = GET_LENGTH_OF_LITERAL_STRING(sDebugMenuData.sItems[iDepth][iSeekItem].sMissionID)

							// Cycle through the strings until we find an unmatched character or reach the end of a string
							WHILE NOT bFoundDiff
							AND iChar < iLenCurrent
							AND iChar < iLenSeek
							
								// Grab the current characters
								sCurrent = GET_STRING_FROM_STRING(sDebugMenuData.sItems[iDepth][iCurrentItem].sMissionID, iChar, iChar+1)
								sSeek	 = GET_STRING_FROM_STRING(sDebugMenuData.sItems[iDepth][iSeekItem].sMissionID, iChar, iChar+1)

								// Look for strings that do not match
								IF NOT ARE_STRINGS_EQUAL(sCurrent, sSeek)
								
									//Grab the INT value
									iCurrent = DM_GET_INT_VALUE_OF_CHARACTER(GET_STRING_FROM_STRING(sDebugMenuData.sItems[iDepth][iCurrentItem].sMissionID, iChar, iChar+1))
									iSeek = DM_GET_INT_VALUE_OF_CHARACTER(GET_STRING_FROM_STRING(sDebugMenuData.sItems[iDepth][iSeekItem].sMissionID, iChar, iChar+1))
									
									bFoundDiff = TRUE
									
								ENDIF
								
								iChar++
									
							ENDWHILE
							
							// Decide if we should perform a swap
							IF bFoundDiff
								
								// Compare the INTs
								IF (iCurrent > iSeek AND sDebugMenuData.eMissionSort = DM_SORT_BUGSTAR_ASC)
								OR (iCurrent < iSeek AND sDebugMenuData.eMissionSort = DM_SORT_BUGSTAR_DESC)
									
									// Make copy of item in the current slot
									sTempItem = sDebugMenuData.sItems[iDepth][iCurrentItem]
						
									// Put the correct item into the slot
									sDebugMenuData.sItems[iDepth][iCurrentItem] = sDebugMenuData.sItems[iDepth][iSeekItem]
									
									// Put the copy into the old correct item slot
									sDebugMenuData.sItems[iDepth][iSeekItem] = sTempItem
									
								ENDIF
								
							ELSE
								
								// Swap items if one string longer than the other and we match sort criteria
								IF (sDebugMenuData.eMissionSort = DM_SORT_BUGSTAR_ASC AND iLenCurrent > iLenSeek)
								OR (sDebugMenuData.eMissionSort = DM_SORT_BUGSTAR_DESC AND iLenCurrent < iLenSeek)
									
									// Make copy of item in the current slot
									sTempItem = sDebugMenuData.sItems[iDepth][iCurrentItem]
							
									// Put the correct item into the slot
									sDebugMenuData.sItems[iDepth][iCurrentItem] = sDebugMenuData.sItems[iDepth][iSeekItem]
									
									// Put the copy into the old correct item slot
									sDebugMenuData.sItems[iDepth][iSeekItem] = sTempItem
									
								ENDIF
								
							// Swap items?
							ENDIF

						// Mission item?
						ENDIF
						
					// End of seek item cycle
					ENDFOR
				
				// Mission item?
				ENDIF
			
			// End of item cycle
			ENDFOR
		
		BREAK
		
	ENDSWITCH
	
	// Update the current selection
	INT i
	FOR i = 0 TO sDebugMenuData.iItemCount[iDepth]-1
		IF sDebugMenuData.sItems[iDepth][i].iUniqueID = iUniqueID
			sDebugMenuData.iCurrentItem[iDepth] = i
			
			// Check if the selected item has went below iUnderItem
			IF sDebugMenuData.iCurrentItem[iDepth] >= sDebugMenuData.iUnderItem[iDepth]
				
				// Get the new top item by traversing back up the list
				INT j
				FOR j = 0 TO DM_MAX_ITEMS_IN_COLUMN-2	// has to be -2 not -1 as we want to keep the iUnderItem as the last+1 in list
					// Obtain the previous item in list
					DM_GET_PREVIOUS_MENU_ITEM(sDebugMenuData, iDepth, TRUE, FALSE)
				ENDFOR
	
				// Set the iTopItem to the previous menu item
				sDebugMenuData.iTopItem[iDepth] = sDebugMenuData.iCurrentItem[iDepth]
				
				// Reset the current item
				sDebugMenuData.iCurrentItem[iDepth] = i
				
			ENDIF
			
			// Check if the selected item has went above iTopItem
			IF sDebugMenuData.iCurrentItem[iDepth] < sDebugMenuData.iTopItem[iDepth]
				// Make the current selection the top item
				sDebugMenuData.iTopItem[iDepth] = sDebugMenuData.iCurrentItem[iDepth]
			ENDIF
		ENDIF
	ENDFOR
	
ENDPROC

/// PURPOSE:
///    Resets all of the data within a debug menu item
/// PARAMS:
///    sItem - Reference to a single menu item
PROC RESET_MENU_ITEM_DATA(DEBUG_MENU_ITEM_STRUCT &sItem)
	sItem.sTitle 			= ""
	sItem.sTitleExtra		= ""
	sItem.sFile 			= ""
	sItem.sAuth 			= ""
	sItem.sInfo 			= ""
	sItem.sExtraInfo 		= ""
	sItem.sMissionID 		= ""
	sItem.sInterior			= ""
	sItem.iUniqueID 		= 0
	sItem.iType 			= 0
	sItem.iCodeID 			= 0
	sItem.iCodeIDPreLaunch	= 0
	sItem.iLabelID 			= 0
	sItem.iDisplayID 		= 0
	sItem.iEnableID 		= 0
	sItem.iPerc 			= 0
	sItem.iImportantBugs	= 0
	sItem.iCharacter 		= 0
	sItem.iFlags  			= DM_ITEM_NONE
	sItem.vCoord[0] 		= << 0.0, 0.0, 0.0 >>
	sItem.fHeading[0]		= 0.0
	sItem.vCoord[1] 		= << 0.0, 0.0, 0.0 >>
	sItem.fHeading[1]		= 0.0
	sItem.vCoord[2] 		= << 0.0, 0.0, 0.0 >>
	sItem.fHeading[2]		= 0.0
	sItem.iRank 			= 0
	sItem.iHour				= -1
ENDPROC

/// PURPOSE: Resets all the debug menu data back to the defaults
PROC RESET_ALL_DEBUG_MENU_DATA(DEBUG_MENU_DATA_STRUCT &sDebugMenuData)
	
	INT i, j
	FOR i = 0 TO DM_MAX_DEPTH-1
	
		sDebugMenuData.iItemCount[i] 	= 0
		sDebugMenuData.iTopItem[i] 		= 0
		sDebugMenuData.iUnderItem[i] 	= 0
		sDebugMenuData.iCurrentItem[i] 	= 0
		// Item data
		FOR j = 0 TO DM_MAX_ITEMS_PER_DEPTH-1
			RESET_MENU_ITEM_DATA(sDebugMenuData.sItems[i][j])
		ENDFOR
	ENDFOR
	
	RESET_ALL_BUGSTAR_DATA(sDebugMenuData.sBugstarData)
	sDebugMenuData.eMissionSort 			= DM_SORT_DEFAULT
	sDebugMenuData.sMenuTitle 				= ""
	sDebugMenuData.iCurrentDepth 			= 0
	sDebugMenuData.iAllowInputCheckTimer 	= 0
	sDebugMenuData.iAltWindowTimer 			= 0
	sDebugMenuData.bDrawAltWindow			= FALSE
	RESET_MISSION_BUGS_DATA(sDebugMenuData.sBugData, TRUE, TRUE, TRUE)
ENDPROC

/// PURPOSE:
///    Does a shorter reset of a menu item
/// PARAMS:
///    sMenuItem - 
PROC DM_RESET_MENU_ITEM(DEBUG_MENU_ITEM_STRUCT &sMenuItem)
	// Set some default states
	sMenuItem.iUniqueID 		= -1
	sMenuItem.iPerc 			= -1
	sMenuItem.iImportantBugs	= -1
	sMenuItem.iCharacter 		= -1
	sMenuItem.vCoord[0] 		= << DM_NO_VECTOR, DM_NO_VECTOR, DM_NO_VECTOR >>
	sMenuItem.fHeading[0]		= DM_NO_HEADING
	sMenuItem.vCoord[1] 		= << DM_NO_VECTOR, DM_NO_VECTOR, DM_NO_VECTOR >>
	sMenuItem.fHeading[1]		= DM_NO_HEADING
	sMenuItem.vCoord[2] 		= << DM_NO_VECTOR, DM_NO_VECTOR, DM_NO_VECTOR >>
	sMenuItem.fHeading[2]		= DM_NO_HEADING
	SET_DEBUG_MENU_ITEM_FLAG(sMenuItem.iFlags, DM_BIT_DISPLAY | DM_BIT_ENABLED | DM_BIT_BLIPCOORDS | DM_BIT_KEEP_VEH | DM_BIT_LAUNCH_SCRIPT)
	sMenuItem.iHour 			= -1
	SET_DEBUG_MENU_ITEM_FLAG(sMenuItem.iFlags, DM_BIT_DISPLAY)
	SET_DEBUG_MENU_ITEM_FLAG(sMenuItem.iFlags, DM_BIT_ENABLED)
	SET_DEBUG_MENU_ITEM_FLAG(sMenuItem.iFlags, DM_BIT_BLIPCOORDS)
	SET_DEBUG_MENU_ITEM_FLAG(sMenuItem.iFlags, DM_BIT_KEEP_VEH)
	SET_DEBUG_MENU_ITEM_FLAG(sMenuItem.iFlags, DM_BIT_LAUNCH_SCRIPT)
ENDPROC

/// PURPOSE:
///    Shorter reset of bugstar data
/// PARAMS:
///    sBugstarData - 
PROC DM_RESET_BUGSTSAR_DATA(BUGSTAR_DATA_STRUCT &sBugstarData)
	// Bugstar data
	INT i
	FOR i = 0 TO DM_MAX_BUGSTAR_DEPTS-1
		sBugstarData.iDeptBugs[i] = 0
	ENDFOR
	FOR i = 0 TO 4
		sBugstarData.iTop5Bugs[i] = 0
		sBugstarData.iTop5BugsAvgAge[i] = 0
	ENDFOR
ENDPROC


/// PURPOSE: Retrieves attribute data from the current node in the xml file that is currently loaded
PROC DM_GET_NODE_ATTRIBUTES(DEBUG_MENU_ITEM_STRUCT &sMenuItem, BUGSTAR_DATA_STRUCT &sBugstarData, BOOL bCopyMissionID = FALSE)

	// Use local copies so we know all the data is cleared
	DEBUG_MENU_ITEM_STRUCT sMenuItemLocal
	DM_RESET_MENU_ITEM(sMenuItemLocal)
	DM_RESET_BUGSTSAR_DATA(sBugstarData)

	BUGSTAR_DATA_STRUCT sBugstarDataLocal

	
	IF bCopyMissionID
		sMenuItemLocal.sMissionID = sMenuItem.sMissionID
	ENDIF
	
	
	// Bugstar data
	INT i
	FOR i = 0 TO DM_MAX_BUGSTAR_DEPTS-1
		sBugstarData.iDeptBugs[i] = 0
	ENDFOR
	FOR i = 0 TO 4
		sBugstarData.iTop5Bugs[i] = 0
		sBugstarData.iTop5BugsAvgAge[i] = 0
	ENDFOR
	
	INT eachAttribute
	TEXT_LABEL_15 sLabel
	
	// Check the node has some attributes
	IF GET_NUMBER_OF_XML_NODE_ATTRIBUTES() <> 0
	
		// Loop through all the attributes
		FOR eachAttribute = 0 TO (GET_NUMBER_OF_XML_NODE_ATTRIBUTES()-1)
	
			// Convert the attribute name to a hash key like we did with the nodes so we can use a switch statement
			SWITCH GET_HASH_KEY(GET_XML_NODE_ATTRIBUTE_NAME(eachAttribute))

				// Grab the title
				CASE DM_HASH_TITLE
					sMenuItemLocal.sTitle = GET_STRING_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
				BREAK
				
				// Grab the title extra
				CASE DM_HASH_TITLE_EXTRA
					sMenuItemLocal.sTitleExtra = GET_STRING_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
				BREAK

				// Grab the file name
				CASE DM_HASH_FILE
					sMenuItemLocal.sFile = GET_STRING_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
				BREAK
				
				// Grab line 1 of info
				CASE DM_HASH_INFO
					sMenuItemLocal.sInfo = GET_STRING_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
				BREAK
				
				// Grab line 2 of info
				CASE DM_HASH_EXTRA_INFO
					sMenuItemLocal.sExtraInfo = GET_STRING_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
				BREAK
				
				// Grab the x coord
				CASE DM_HASH_POSX
					sMenuItemLocal.vCoord[0].x = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
				BREAK
				
				// Grab the y coord
				CASE DM_HASH_POSY
					sMenuItemLocal.vCoord[0].y = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
				BREAK
				
				// Grab the z coord
				CASE DM_HASH_POSZ
					sMenuItemLocal.vCoord[0].z = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
				BREAK
				
				// Grab the heading
				CASE DM_HASH_HEADING
					sMenuItemLocal.fHeading[0] = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
				BREAK
				
				// Grab the x1 coord
				CASE DM_HASH_POSX1
					sMenuItemLocal.vCoord[1].x = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
				BREAK
				
				// Grab the y1 coord
				CASE DM_HASH_POSY1
					sMenuItemLocal.vCoord[1].y = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
				BREAK
				
				// Grab the z1 coord
				CASE DM_HASH_POSZ1
					sMenuItemLocal.vCoord[1].z = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
				BREAK
				
				// Grab the heading1
				CASE DM_HASH_HEADING1
					sMenuItemLocal.fHeading[1] = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
				BREAK
				
				// Grab the x2 coord
				CASE DM_HASH_POSX2
					sMenuItemLocal.vCoord[2].x = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
				BREAK
				
				// Grab the y2 coord
				CASE DM_HASH_POSY2
					sMenuItemLocal.vCoord[2].y = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
				BREAK
				
				// Grab the z2 coord
				CASE DM_HASH_POSZ2
					sMenuItemLocal.vCoord[2].z = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
				BREAK
				
				// Grab the heading2
				CASE DM_HASH_HEADING2
					sMenuItemLocal.fHeading[2] = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
				BREAK
				
				// Grab the codeID
				CASE DM_HASH_CODE_ID
					sMenuItemLocal.iCodeID = GET_HASH_KEY(GET_STRING_FROM_XML_NODE_ATTRIBUTE(eachAttribute))
				BREAK
				
				// Grab the preCodeID
				CASE DM_HASH_CODE_ID_PRELAUNCH
					sMenuItemLocal.iCodeIDPreLaunch = GET_HASH_KEY(GET_STRING_FROM_XML_NODE_ATTRIBUTE(eachAttribute))
				BREAK
				
				// Grab the codeID Alt 1
				CASE DM_HASH_CODE_ID_A_1
					sMenuItemLocal.iAltCodeID[0] = GET_HASH_KEY(GET_STRING_FROM_XML_NODE_ATTRIBUTE(eachAttribute))
				BREAK

				// Grab the codeID Alt 2
				CASE DM_HASH_CODE_ID_A_2
					sMenuItemLocal.iAltCodeID[1] = GET_HASH_KEY(GET_STRING_FROM_XML_NODE_ATTRIBUTE(eachAttribute))
				BREAK

				// Grab the codeID Alt 3
				CASE DM_HASH_CODE_ID_A_3
					sMenuItemLocal.iAltCodeID[2] = GET_HASH_KEY(GET_STRING_FROM_XML_NODE_ATTRIBUTE(eachAttribute))
				BREAK

				// Grab the codeID Alt 4
				CASE DM_HASH_CODE_ID_A_4
					sMenuItemLocal.iAltCodeID[3] = GET_HASH_KEY(GET_STRING_FROM_XML_NODE_ATTRIBUTE(eachAttribute))
				BREAK

				// Grab the labelID
				CASE DM_HASH_LABEL_ID
					sMenuItemLocal.iLabelID = GET_HASH_KEY(GET_STRING_FROM_XML_NODE_ATTRIBUTE(eachAttribute))
				BREAK
				
				// Grab the labelID2
				CASE DM_HASH_LABEL_ID_2
					sMenuItemLocal.iExtraLabelID[0] = GET_HASH_KEY(GET_STRING_FROM_XML_NODE_ATTRIBUTE(eachAttribute))
				BREAK
				
				// Grab the labelID3
				CASE DM_HASH_LABEL_ID_3
					sMenuItemLocal.iExtraLabelID[1] = GET_HASH_KEY(GET_STRING_FROM_XML_NODE_ATTRIBUTE(eachAttribute))
				BREAK

				// Grab the displayID
				CASE DM_HASH_DISPLAY_ID
					sMenuItemLocal.iDisplayID = GET_HASH_KEY(GET_STRING_FROM_XML_NODE_ATTRIBUTE(eachAttribute))
				BREAK
				
				// Grab the selectID
				CASE DM_HASH_ENABLE_ID
					sMenuItemLocal.iEnableID = GET_HASH_KEY(GET_STRING_FROM_XML_NODE_ATTRIBUTE(eachAttribute))
				BREAK
				
				// Grab the roomType
				CASE DM_HASH_INTERIOR
					sMenuItemLocal.sInterior = GET_STRING_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
				BREAK
				
				// Grab the author
				CASE DM_HASH_AUTHOR
					sMenuItemLocal.sAuth = GET_STRING_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
				BREAK
				
				// Grab the missionID
				CASE DM_HASH_MISSION_ID
					sMenuItemLocal.sMissionID = GET_STRING_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
				BREAK
				
				// Grab the missionName
				CASE DM_HASH_MISSION_NAME
					sMenuItemLocal.sTitle = GET_STRING_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
				BREAK
				
				// Grab the story
				CASE DM_HASH_MISSION_STORY
					// Store the mission description returned from bugstar query
					sLabel = ""
					sLabel += sMenuItemLocal.sMissionID
					sLabel += "_MD1"
					//PRINTLN("ADDING_DEBUG_STRING with label: ", sLabel)
					ADD_DEBUG_STRING(sLabel, GET_STRING_FROM_XML_NODE_ATTRIBUTE(eachAttribute))
				BREAK
				
				// Grab the description
				CASE DM_HASH_MISSION_DESC
					// Store the story description returned from bugstar query
					sLabel = ""
					sLabel += sMenuItemLocal.sMissionID
					sLabel += "_SD1"
					//PRINTLN("ADDING_DEBUG_STRING with label: ", sLabel)
					ADD_DEBUG_STRING(sLabel, GET_STRING_FROM_XML_NODE_ATTRIBUTE(eachAttribute))
				BREAK
				
				// Grab the comments
				CASE DM_HASH_MISSION_COMMENTS
					// Store the mission comments returned from bugstar query
					sLabel = ""
					sLabel += sMenuItemLocal.sMissionID
					sLabel += "_MC1"
					//PRINTLN("ADDING_DEBUG_STRING with label: ", sLabel)
					ADD_DEBUG_STRING(sLabel, GET_STRING_FROM_XML_NODE_ATTRIBUTE(eachAttribute))
				BREAK
				
				// Grab the character
				CASE DM_HASH_CHARACTER
					sMenuItemLocal.iCharacter = GET_HASH_KEY(GET_STRING_FROM_XML_NODE_ATTRIBUTE(eachAttribute))
				BREAK
				
				// Grab the percentage
				CASE DM_HASH_PERCENTAGE
					sMenuItemLocal.iPerc = GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
				BREAK
				
				// Grab the important bugs
				CASE DM_HASH_IMPORTANT_BUGS
					sMenuItemLocal.iImportantBugs = GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
				BREAK
				
				// Grab the highlight flag
				CASE DM_HASH_HIGHLIGHT
					IF GET_BOOL_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
						SET_DEBUG_MENU_ITEM_FLAG(sMenuItemLocal.iFlags, DM_BIT_HIGHLIGHT)
					ENDIF
				BREAK
				
				// Grab the blip flag
				CASE DM_HASH_BLIPCOORDS
					IF NOT GET_BOOL_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
						CLEAR_DEBUG_MENU_ITEM_FLAG(sMenuItemLocal.iFlags, DM_BIT_BLIPCOORDS)
					ENDIF
				BREAK
				
				// Grab the vehicle flag
				CASE DM_HASH_VEHICLE
					IF GET_BOOL_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
						SET_DEBUG_MENU_ITEM_FLAG(sMenuItemLocal.iFlags, DM_BIT_VEHICLE)
					ENDIF
				BREAK
				
				// Grab the keep vehicle flag
				CASE DM_HASH_KEEP_VEHICLE
					IF GET_BOOL_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
						SET_DEBUG_MENU_ITEM_FLAG(sMenuItemLocal.iFlags, DM_BIT_KEEP_VEH)
					ELSE
						CLEAR_DEBUG_MENU_ITEM_FLAG(sMenuItemLocal.iFlags, DM_BIT_KEEP_VEH)
					ENDIF
				BREAK
				
				// Grab the is launchScript flag
				CASE DM_HASH_LAUNCH_SCRIPT
					IF GET_BOOL_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
						SET_DEBUG_MENU_ITEM_FLAG(sMenuItemLocal.iFlags, DM_BIT_LAUNCH_SCRIPT)
					ELSE
						CLEAR_DEBUG_MENU_ITEM_FLAG(sMenuItemLocal.iFlags, DM_BIT_LAUNCH_SCRIPT)
					ENDIF
				BREAK
				
				// Grab the rank
				CASE DM_HASH_MISSION_RANK
					sMenuItemLocal.iRank = GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
				BREAK
				
				// Grab the hour
				CASE DM_HASH_CLOCK_HOUR
					sMenuItemLocal.iHour = GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
				BREAK
				
				DEFAULT
					PRINTLN("Got an unknown attribute ", GET_HASH_KEY(GET_XML_NODE_ATTRIBUTE_NAME(eachAttribute)))
				BREAK
			// End of the attribute switch statement
			ENDSWITCH
		
		// End for loop going through the attributes
		ENDFOR
		
	ENDIF
	
	sMenuItem = sMenuItemLocal
	sBugstarData = sBugstarDataLocal
ENDPROC



/// PURPOSE: Attempts to get bug data for current mission using a bugstar query
FUNC BOOL DM_GET_MISSION_BUGS_DATA(DEBUG_MENU_DATA_STRUCT &sDebugMenuData)
	
	// Make sure the depth is valid before we proceed
	INT iDepth = sDebugMenuData.iCurrentDepth
	IF iDepth < 0 OR iDepth >= DM_MAX_DEPTH
		RETURN TRUE
	ENDIF
	
	// Make sure the item is valid before we proceed
	INT iItem = sDebugMenuData.iCurrentItem[iDepth]
	IF iItem < 0 OR iItem >= DM_MAX_ITEMS_PER_DEPTH
		RETURN TRUE
	ENDIF
	
	// Perform various checks before we proceed
	IF NOT IS_DEBUG_MENU_ITEM_FLAG_SET(sDebugMenuData.sItems[iDepth][iItem].iFlags, DM_BIT_BUG_INFO)
		RETURN TRUE
	ENDIF

	TEXT_LABEL_7 sCurrentMissionID = sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].sMissionID
	
	// If current mission is not the mission in the bug data, reset
	IF NOT ARE_STRINGS_EQUAL(sCurrentMissionID, sDebugMenuData.sBugData.sMissionID)
		RESET_MISSION_BUGS_DATA(sDebugMenuData.sBugData, FALSE, TRUE, TRUE)
		sDebugMenuData.sBugData.sMissionID = sCurrentMissionID
	ENDIF
	
	RETURN BUGSTAR_MISSION_BUGS(sDebugMenuData.sBugstarData, sDebugMenuData.sBugData)
ENDFUNC



/// PURPOSE: Attempts to get bugstar info for the current item
FUNC BOOL DM_GET_DETAILED_BUGSTAR_MISSION_DATA(DEBUG_MENU_DATA_STRUCT &sDebugMenuData)
	// Make sure the depth is valid before we proceed
	INT iDepth = sDebugMenuData.iCurrentDepth
	IF iDepth < 0 OR iDepth >= DM_MAX_DEPTH
		RETURN TRUE
	ENDIF
	
	// Make sure the item is valid before we proceed
	INT iItem = sDebugMenuData.iCurrentItem[iDepth]
	IF iItem < 0 OR iItem >= DM_MAX_ITEMS_PER_DEPTH
		RETURN TRUE
	ENDIF
	
	// Perform various checks before we proceed
	IF NOT IS_DEBUG_MENU_ITEM_FLAG_SET(sDebugMenuData.sItems[iDepth][iItem].iFlags, DM_BIT_BUG_INFO)
		RETURN TRUE
	ENDIF
	DEBUG_MENU_ITEM_STRUCT sItemCopy
	
	
	RETURN BUGSTAR_DETAILED_MISSION_DATA(sDebugMenuData.sBugstarData, sItemCopy, sDebugMenuData.sItems[iDepth][iItem].sMissionID)
ENDFUNC





/// PURPOSE: Loads the bugstar xml and updates all the bugstar items with the data it finds for the matching mission ID
FUNC BOOL DM_GET_BASIC_BUGSTAR_MISSION_DATA(DEBUG_MENU_DATA_STRUCT &sDebugMenuData, INT iDepth)

	// First thing is to perform a quick check on which items are bugstar items,
	// taking note of the array pos so we can:
	// 1. See if we need to load the bugstar file
	// 2. Be able to quickly link to the bugstar items in the array
	// 3. Cut down on checks/cycles.
	// We will use hash keys to make the process quicker, this way we just need an INT array the size of max items per list const.
	// Note: We are now trying to obtain bugstar data for mission items. If we find a matching ID we can upgrade to a bugstar item.
	
	IF iDepth >= DM_MAX_DEPTH
	OR ARE_STRINGS_EQUAL(sDebugMenuData.sBugstarData.sBugstarFile, "")
		CDEBUG1LN(DEBUG_DEBUG_MENU, "DM_GET_BASIC_BUGSTAR_MISSION_DATA: No bugstar file, or we are out of our depth!")
		RETURN TRUE
	ENDIF
	
	INT iCount
	INT iBugstarItemCount
	INT iBugstarItems[DM_MAX_ITEMS_PER_DEPTH]
	
	// Obtain a list of pointers to the bugstar items
	FOR iCount = 0 TO sDebugMenuData.iItemCount[iDepth]-1
		IF GET_HASH_KEY(sDebugMenuData.sItems[iDepth][iCount].sMissionID) != 0
			iBugstarItems[iBugstarItemCount] = iCount
			iBugstarItemCount++
		ENDIF
	ENDFOR
	
	IF iBugstarItemCount = 0
		CDEBUG1LN(DEBUG_DEBUG_MENU, "DM_GET_BASIC_BUGSTAR_MISSION_DATA: No bugstar items to grab")
		RETURN TRUE
	ENDIF

	RETURN BUGSTAR_BASIC_MISSION_DATA(sDebugMenuData.sItems[iDepth], sDebugMenuData.sBugstarData, iBugstarItems, iBugstarItemCount)
ENDFUNC

/// PURPOSE: Builds a menu list by filling the debug menu struct with data from the specified xml
FUNC BOOL BUILD_DEBUG_MENU_LIST(DEBUG_MENU_DATA_STRUCT &sDebugMenuData, INT iDepth, STRING sXMLFile)

	IF iDepth >= DM_MAX_DEPTH
	OR IS_STRING_NULL_OR_EMPTY(sXMLFile)
		RETURN FALSE
	ENDIF
	
	BOOL bBugstarItem = FALSE
	BUGSTAR_DATA_STRUCT sBugstarTemp
	
	// Reset item count
	sDebugMenuData.iItemCount[iDepth] = 0
	
	// Load in xml file if it exists
	IF LOAD_XML_FILE(sXMLFile)
	
		CDEBUG1LN(DEBUG_DEBUG_MENU, "Build_Debug_Menu_List - Loading xml file '", sXMLFile, "'")
		
		INT iNodeCount = GET_NUMBER_OF_XML_NODES()
 
    	// Check that the xml contains some nodes
    	IF iNodeCount <> 0
		
        	// Integers used for the FOR loops going through all the nodes and attributes
        	INT eachNode, eachAttribute
		   
        	// Loop through all the nodes
        	FOR eachNode = 0 TO (iNodeCount-1)
			
				// Check we have room for a new item
				IF sDebugMenuData.iItemCount[iDepth] < DM_MAX_ITEMS_PER_DEPTH-1
				
					// Convert the current node name to a hash key so it can be used in a switch against the const's created at the top of this file
					SWITCH GET_HASH_KEY(GET_XML_NODE_NAME())
						
						/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
						// Case statement for the "menuTitle" node, once we hit this we want to look at all the attributes
						// Attributes: menuTitle
						//
						CASE DM_HASH_DEBUG_MENU_TITLE
							
							// Check the node has some attributes
							IF GET_NUMBER_OF_XML_NODE_ATTRIBUTES() <> 0
							
								// Loop through all the attributes
								FOR eachAttribute = 0 TO (GET_NUMBER_OF_XML_NODE_ATTRIBUTES()-1)
		
									// Convert the attribute name to a hash key like we did with the nodes so we can use a switch statement
									SWITCH GET_HASH_KEY(GET_XML_NODE_ATTRIBUTE_NAME(eachAttribute))
									
										// Grab the title
										CASE DM_HASH_TITLE
											sDebugMenuData.sMenuTitle = GET_STRING_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
										BREAK
									// End of the attribute switch statement
									ENDSWITCH
								
								// End for loop going through the attributes
								ENDFOR
								
							ENDIF
						
						// End of the menu title case statement
						BREAK
						
						/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
						// Case statement for the "bugstarFile" node, once we hit this we want to look at all the attributes
						// Attributes: file
						//
						CASE DM_HASH_BUGSTAR_FILE
							
							// Check the node has some attributes
							IF GET_NUMBER_OF_XML_NODE_ATTRIBUTES() <> 0
							
								// Loop through all the attributes
								FOR eachAttribute = 0 TO (GET_NUMBER_OF_XML_NODE_ATTRIBUTES()-1)
		
									// Convert the attribute name to a hash key like we did with the nodes so we can use a switch statement
									SWITCH GET_HASH_KEY(GET_XML_NODE_ATTRIBUTE_NAME(eachAttribute))
									
										// Grab the file
										CASE DM_HASH_FILE
											sDebugMenuData.sBugstarData.sBugstarFile = GET_STRING_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
										BREAK
									// End of the attribute switch statement
									ENDSWITCH
								
								// End for loop going through the attributes
								ENDFOR
								
							ENDIF
						
						// End of the bugstar file case statement
						BREAK
						
						/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
						// Case statement for the "menuItem" node, once we hit this we want to look at all the attributes
						// Attributes: title, file, description
						//
						CASE DM_HASH_ITEM_MENU
							DM_GET_NODE_ATTRIBUTES(sDebugMenuData.sItems[iDepth][sDebugMenuData.iItemCount[iDepth]], sBugstarTemp)
							sDebugMenuData.sItems[iDepth][sDebugMenuData.iItemCount[iDepth]].iType = DM_HASH_ITEM_MENU
							sDebugMenuData.sItems[iDepth][sDebugMenuData.iItemCount[iDepth]].iUniqueID = sDebugMenuData.iItemCount[iDepth]
							sDebugMenuData.iItemCount[iDepth]++
						
						// End of the menu case statement
						BREAK
						
						/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
						// Case statement for the "inputItem" node, once we hit this we want to look at all the attributes
						// Attributes: title
						//
						CASE DM_HASH_ITEM_INPUT
							DM_GET_NODE_ATTRIBUTES(sDebugMenuData.sItems[iDepth][sDebugMenuData.iItemCount[iDepth]], sBugstarTemp)
							sDebugMenuData.sItems[iDepth][sDebugMenuData.iItemCount[iDepth]].iType = DM_HASH_ITEM_INPUT
							sDebugMenuData.sItems[iDepth][sDebugMenuData.iItemCount[iDepth]].iUniqueID = sDebugMenuData.iItemCount[iDepth]
							sDebugMenuData.iItemCount[iDepth]++
						
						// End of the input case statement
						BREAK
						
						/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
						// Case statement for the "missionItem" node, once we hit this we want to look at all the attributes
						// Attributes: title, file, description, x, y, z, heading, warp, missionID, author, percentage, highlight
						//
						CASE DM_HASH_ITEM_MISSION
							DM_GET_NODE_ATTRIBUTES(sDebugMenuData.sItems[iDepth][sDebugMenuData.iItemCount[iDepth]], sBugstarTemp)
							sDebugMenuData.sItems[iDepth][sDebugMenuData.iItemCount[iDepth]].iType = DM_HASH_ITEM_MISSION
							sDebugMenuData.sItems[iDepth][sDebugMenuData.iItemCount[iDepth]].iUniqueID = sDebugMenuData.iItemCount[iDepth]
							IF GET_HASH_KEY(sDebugMenuData.sItems[iDepth][sDebugMenuData.iItemCount[iDepth]].sMissionID) != 0
								bBugstarItem = TRUE
							ENDIF
							sDebugMenuData.iItemCount[iDepth]++
							
						// End of the mission case statement
						BREAK
						
						/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
						// Case statement for the "launcherItem" node, once we hit this we want to look at all the attributes
						// Attributes: title, file, description, x, y, z, heading, warp, missionID, author, percentage, highlight
						//
						CASE DM_HASH_ITEM_LAUNCHER
							DM_GET_NODE_ATTRIBUTES(sDebugMenuData.sItems[iDepth][sDebugMenuData.iItemCount[iDepth]], sBugstarTemp)
							sDebugMenuData.sItems[iDepth][sDebugMenuData.iItemCount[iDepth]].iType = DM_HASH_ITEM_LAUNCHER
							sDebugMenuData.sItems[iDepth][sDebugMenuData.iItemCount[iDepth]].iUniqueID = sDebugMenuData.iItemCount[iDepth]
							IF GET_HASH_KEY(sDebugMenuData.sItems[iDepth][sDebugMenuData.iItemCount[iDepth]].sMissionID) != 0
								bBugstarItem = TRUE
							ENDIF
							sDebugMenuData.iItemCount[iDepth]++
							
						// End of the mission case statement
						BREAK
						
						/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
						// Case statement for the "bugstarItem" node, once we hit this we want to look at all the attributes
						// Attributes: title, file, description, x, y, z, heading, warp, missionID, author, percentage, highlight, bugs, todos, story
						//
						CASE DM_HASH_ITEM_BUGSTAR
							DM_GET_NODE_ATTRIBUTES(sDebugMenuData.sItems[iDepth][sDebugMenuData.iItemCount[iDepth]], sBugstarTemp)
							sDebugMenuData.sItems[iDepth][sDebugMenuData.iItemCount[iDepth]].iType = DM_HASH_ITEM_BUGSTAR
							sDebugMenuData.sItems[iDepth][sDebugMenuData.iItemCount[iDepth]].iUniqueID = sDebugMenuData.iItemCount[iDepth]
							IF GET_HASH_KEY(sDebugMenuData.sItems[iDepth][sDebugMenuData.iItemCount[iDepth]].sMissionID) != 0
								bBugstarItem = TRUE
							ENDIF
							sDebugMenuData.iItemCount[iDepth]++
							
						// End of the bugstar case statement
						BREAK
						
						/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
						// Case statement for the "toolItem" node, once we hit this we want to look at all the attributes
						// Attributes: title, file, x, y, z, heading, warp
						//
						CASE DM_HASH_ITEM_TOOL
							DM_GET_NODE_ATTRIBUTES(sDebugMenuData.sItems[iDepth][sDebugMenuData.iItemCount[iDepth]], sBugstarTemp)
							sDebugMenuData.sItems[iDepth][sDebugMenuData.iItemCount[iDepth]].iType = DM_HASH_ITEM_TOOL
							sDebugMenuData.sItems[iDepth][sDebugMenuData.iItemCount[iDepth]].iUniqueID = sDebugMenuData.iItemCount[iDepth]
							IF GET_HASH_KEY(sDebugMenuData.sItems[iDepth][sDebugMenuData.iItemCount[iDepth]].sMissionID) != 0
								bBugstarItem = TRUE
							ENDIF
							sDebugMenuData.iItemCount[iDepth]++
							
						// End of the mission case statement
						BREAK
						
						/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
						// Case statement for the "scriptItem" node, once we hit this we want to look at all the attributes
						// Attributes: title, file, x, y, z, heading, warp
						//
						CASE DM_HASH_ITEM_SCRIPT
							DM_GET_NODE_ATTRIBUTES(sDebugMenuData.sItems[iDepth][sDebugMenuData.iItemCount[iDepth]], sBugstarTemp)
							sDebugMenuData.sItems[iDepth][sDebugMenuData.iItemCount[iDepth]].iType = DM_HASH_ITEM_SCRIPT
							sDebugMenuData.sItems[iDepth][sDebugMenuData.iItemCount[iDepth]].iUniqueID = sDebugMenuData.iItemCount[iDepth]
							IF GET_HASH_KEY(sDebugMenuData.sItems[iDepth][sDebugMenuData.iItemCount[iDepth]].sMissionID) != 0
								bBugstarItem = TRUE
							ENDIF
							sDebugMenuData.iItemCount[iDepth]++
							
						// End of the script case statement
						BREAK
						
						/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
						// Case statement for the "warpItem" node, once we hit this we want to look at all the attributes
						// Attributes: title, x, y, z, heading
						//
						CASE DM_HASH_ITEM_WARP
							DM_GET_NODE_ATTRIBUTES(sDebugMenuData.sItems[iDepth][sDebugMenuData.iItemCount[iDepth]], sBugstarTemp)
							sDebugMenuData.sItems[iDepth][sDebugMenuData.iItemCount[iDepth]].iType = DM_HASH_ITEM_WARP
							sDebugMenuData.sItems[iDepth][sDebugMenuData.iItemCount[iDepth]].iUniqueID = sDebugMenuData.iItemCount[iDepth]
							IF GET_HASH_KEY(sDebugMenuData.sItems[iDepth][sDebugMenuData.iItemCount[iDepth]].sMissionID) != 0
								bBugstarItem = TRUE
							ENDIF
							sDebugMenuData.iItemCount[iDepth]++
							
						// End of the warp case statement
						BREAK
						
						/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
						// Case statement for the "codeItem" node, once we hit this we want to look at all the attributes
						// Attributes: title, codeID, displayID
						//
						CASE DM_HASH_ITEM_CODE
							DM_GET_NODE_ATTRIBUTES(sDebugMenuData.sItems[iDepth][sDebugMenuData.iItemCount[iDepth]], sBugstarTemp)
							sDebugMenuData.sItems[iDepth][sDebugMenuData.iItemCount[iDepth]].iType = DM_HASH_ITEM_CODE
							sDebugMenuData.sItems[iDepth][sDebugMenuData.iItemCount[iDepth]].iUniqueID = sDebugMenuData.iItemCount[iDepth]
							sDebugMenuData.iItemCount[iDepth]++
							
						// End of the script case statement
						BREAK
						
						/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
						// Case statement for the "breakItem" node, once we hit this we just set the basics
						//
						CASE DM_HASH_ITEM_BREAK
							DM_GET_NODE_ATTRIBUTES(sDebugMenuData.sItems[iDepth][sDebugMenuData.iItemCount[iDepth]], sBugstarTemp)
							sDebugMenuData.sItems[iDepth][sDebugMenuData.iItemCount[iDepth]].iType = DM_HASH_ITEM_BREAK
							sDebugMenuData.sItems[iDepth][sDebugMenuData.iItemCount[iDepth]].iUniqueID = sDebugMenuData.iItemCount[iDepth]
							sDebugMenuData.iItemCount[iDepth]++
						
						// End of the break case statement
						BREAK
		
					// End of the node switch statement
					ENDSWITCH
						
					// Tell the script to goto the next node in the xml file
					GET_NEXT_XML_NODE()
				
				// We have reached our item limit so do not add item to list
				ELSE
					ASSERTLN("Debug Menu - Cannot add item to list [ITEM LIMIT REACHED] - MAX 50")
					CERRORLN(DEBUG_DEBUG_MENU, "Debug Menu - Cannot add item to list [ITEM LIMIT REACHED]")
				ENDIF
	
			// End of for loop going through the nodes
			ENDFOR
	
			// Now we have gone through all the nodes you need to unload the xml file
			DELETE_XML_FILE()
				
		ENDIF
	
	// Failed to load XML file
	ELSE
		CDEBUG1LN(DEBUG_DEBUG_MENU, "Debug Menu - Failed to load xml file '", sXMLFile, "'")
		
		RETURN FALSE
	ENDIF
	
	IF bBugstarItem
		// Allow Bugstar data to be updated
		//sDebugMenuData.sBugstarData.iGetBasicInfoStage = 0
		sDebugMenuData.sBugstarData.iDepthToLoadBasicInfo = iDepth
		SET_BUGSTAR_PROCESS_FLAG(sDebugMenuData.sBugstarData.eProcessFlags, BSTAR_PROCESS_BUGSTAR_QUERY_RESET)
		SET_DEBUG_MENU_PROCESS_FLAG(sDebugMenuData.iProcessFlags, DM_PROCESS_BUGSTAR_GET_BASIC)
	ELSE
		CLEAR_DEBUG_MENU_PROCESS_FLAG(sDebugMenuData.iProcessFlags, DM_PROCESS_BUGSTAR_GET_BASIC)
	ENDIF
	
	// Reset some states
	DM_SORT_MENU_LIST(sDebugMenuData, iDepth)
	sDebugMenuData.iCurrentItem[iDepth] = 0
	sDebugMenuData.iTopItem[iDepth] = 0
	sDebugMenuData.iUnderItem[iDepth] = sDebugMenuData.iItemCount[iDepth]
	
	INT iCount
	FOR iCount = iDepth+1 TO DM_MAX_DEPTH-1
		sDebugMenuData.iItemCount[iCount] = 0
	ENDFOR
	
	RETURN TRUE
	
ENDFUNC


//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		PROCS TO HANDLE MENU NAVIGATION													//
//    																					//
//////////////////////////////////////////////////////////////////////////////////////////  

/// PURPOSE: Handles the player navigation input
PROC CHECK_DEBUG_MENU_NAVIGATION(DEBUG_MENU_DATA_STRUCT &sDebugMenuData)
	
	INT tempTimer = GET_GAME_TIMER()
	IF (sDebugMenuData.iAllowInputCheckTimer > tempTimer)
		EXIT
	ENDIF
	
	// Read the analogue stick position
	INT leftStickLR = 0
	INT leftStickUD = 0
	INT rightStickLR = 0
	INT rightStickUD = 0
	GET_CONTROL_VALUE_OF_ANALOGUE_STICKS( leftStickLR, leftStickUD, rightStickLR, rightStickUD)
	
	
	// If the popup window is drawing, only do navigation relative to it. Ignore the rest of the menu
	IF sDebugMenuData.bDrawAltWindow
		// Move down (menu)
		IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
		OR IS_KEYBOARD_KEY_PRESSED(KEY_DOWN)
		OR (leftStickUD > 50)
		OR sDebugMenuData.bMouseMoveDown
			sDebugMenuData.iCurrentAltItem++
			IF sDebugMenuData.iCurrentAltItem >= MAX_ALT_CODE_IDS
				sDebugMenuData.iCurrentAltItem = 0
			ENDIF
			IF sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].iAltCodeID[sDebugMenuData.iCurrentAltItem] = ENUM_TO_INT(SPCID_DEFAULT)
				sDebugMenuData.iCurrentAltItem = 0
			ENDIF		
			sDebugMenuData.bMouseMoveDown = FALSE
			sDebugMenuData.iAllowInputCheckTimer = tempTimer + DM_INPUT_CHECK_DELAY_msec

		// Move up (menu)
		ELIF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
		OR IS_KEYBOARD_KEY_PRESSED(KEY_UP)
		OR (leftStickUD < -50)
		OR sDebugMenuData.bMouseMoveUp
			sDebugMenuData.iCurrentAltItem--
			IF sDebugMenuData.iCurrentAltItem < 0
				sDebugMenuData.iCurrentAltItem = MAX_ALT_CODE_IDS-1
				WHILE sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].iAltCodeID[sDebugMenuData.iCurrentAltItem] = ENUM_TO_INT(SPCID_DEFAULT)
					sDebugMenuData.iCurrentAltItem--
				ENDWHILE
			ENDIF
			sDebugMenuData.bMouseMoveUp = FALSE
			sDebugMenuData.iAllowInputCheckTimer = tempTimer + DM_INPUT_CHECK_DELAY_msec
			
		ENDIF		
		EXIT
	ENDIF
	
	// Mouse control
	// Note: The item selection part takes place in DM_Display_Menu_Item_Border(...) and
	// 		 the mouse move up/down part takes place in Display_Debug_Menu(...)
	
	// Move down (menu)
	IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
	OR IS_KEYBOARD_KEY_PRESSED(KEY_DOWN)
	OR (leftStickUD > 50)
	OR sDebugMenuData.bMouseMoveDown
		IF DM_GET_NEXT_MENU_ITEM(sDebugMenuData, sDebugMenuData.iCurrentDepth, FALSE, !sDebugMenuData.bMouseMoveDown)
			
			// Check if iCurrentItem has went below iUnderItem
			IF sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth] >= sDebugMenuData.iUnderItem[sDebugMenuData.iCurrentDepth]
			
				// Store the current item so we can reset it once we obtain the new iTopItem
				INT iCurrentCopy = sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]
						
				// Get the new top item by traversing back up the list
				INT i			
				FOR i = 0 TO DM_MAX_ITEMS_IN_COLUMN-2	// has to be -2 not -1 as we want to keep the iUnderItem as the last+1 in list
					// Obtain the previous item in list
					DM_GET_PREVIOUS_MENU_ITEM(sDebugMenuData, sDebugMenuData.iCurrentDepth, TRUE, FALSE)
				ENDFOR
	
				// Set the iTopItem to the previous menu item
				sDebugMenuData.iTopItem[sDebugMenuData.iCurrentDepth] = sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]
				
				// Reset the current item
				sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth] = iCurrentCopy
				
			ENDIF
			
			// Check if iCurrentItem has went above iTopItem
			IF sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth] <  sDebugMenuData.iTopItem[sDebugMenuData.iCurrentDepth]
				sDebugMenuData.iTopItem[sDebugMenuData.iCurrentDepth] = sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]
			ENDIF
			
			SET_DEBUG_MENU_PROCESS_FLAG(sDebugMenuData.iProcessFlags, DM_PROCESS_UPDATE_MENU_ITEM)
			RESET_MISSION_BUGS_DATA(sDebugMenuData.sBugData, TRUE, TRUE, TRUE)
			sDebugMenuData.bMouseMoveDown = FALSE
			sDebugMenuData.iAllowInputCheckTimer = tempTimer + DM_INPUT_CHECK_DELAY_msec
		ENDIF
		
		
	// Move up (menu)
	ELIF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
	OR IS_KEYBOARD_KEY_PRESSED(KEY_UP)
	OR (leftStickUD < -50)
	OR sDebugMenuData.bMouseMoveUp
		IF DM_GET_PREVIOUS_MENU_ITEM(sDebugMenuData, sDebugMenuData.iCurrentDepth, FALSE, !sDebugMenuData.bMouseMoveUp)
		
			// Check if iCurrentItem has went above iTopItem
			IF sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth] <  sDebugMenuData.iTopItem[sDebugMenuData.iCurrentDepth]
				sDebugMenuData.iTopItem[sDebugMenuData.iCurrentDepth] = sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]
			ENDIF
			
			// Check if iCurrentItem has went below iUnderItem - This means that we have looped
			IF sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth] >= sDebugMenuData.iUnderItem[sDebugMenuData.iCurrentDepth]
				// Set the under item to the currentItem+1 so it doesnt mess up
				sDebugMenuData.iUnderItem[sDebugMenuData.iCurrentDepth] = sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]+1
				
				// Store the current item so we can reset it once we obtain the new iTopItem
				INT iCurrentCopy = sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]
				
				// Get the new top item by traversing back up the list
				INT i			
				FOR i = 0 TO DM_MAX_ITEMS_IN_COLUMN-2	// has to be -2 not -1 as we want to keep the iUnderItem as the last+1 in list
					// Obtain the previous item in list
					DM_GET_PREVIOUS_MENU_ITEM(sDebugMenuData, sDebugMenuData.iCurrentDepth, TRUE, FALSE)
				ENDFOR
				
				// Set the iTopItem to the previous menu item
				sDebugMenuData.iTopItem[sDebugMenuData.iCurrentDepth] = sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]
				
				// Reset the current item
				sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth] = iCurrentCopy
				
			ENDIF
			
			SET_DEBUG_MENU_PROCESS_FLAG(sDebugMenuData.iProcessFlags, DM_PROCESS_UPDATE_MENU_ITEM)
			RESET_MISSION_BUGS_DATA(sDebugMenuData.sBugData, TRUE, TRUE, TRUE)
			sDebugMenuData.bMouseMoveUp = FALSE
			sDebugMenuData.iAllowInputCheckTimer = tempTimer + DM_INPUT_CHECK_DELAY_msec
		ENDIF
		
		
	// Move right
	ELIF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
	OR IS_KEYBOARD_KEY_PRESSED(KEY_RIGHT)
	OR (leftStickLR > 50)
		IF NOT IS_DEBUG_MENU_PROCESS_FLAG_SET(sDebugMenuData.iProcessFlags, DM_PROCESS_UPDATE_MENU_ITEM)
		AND NOT IS_DEBUG_MENU_PROCESS_FLAG_SET(sDebugMenuData.iProcessFlags, DM_PROCESS_BUGSTAR_GET_BASIC)
		
			IF sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].iType = DM_HASH_ITEM_MENU
			AND sDebugMenuData.iCurrentDepth < DM_MAX_DEPTH-1
				sDebugMenuData.iCurrentDepth++
				SET_DEBUG_MENU_PROCESS_FLAG(sDebugMenuData.iProcessFlags, DM_PROCESS_UPDATE_MENU_ITEM)
				RESET_MISSION_BUGS_DATA(sDebugMenuData.sBugData, TRUE, TRUE, TRUE)
			ENDIF
			
			sDebugMenuData.iAllowInputCheckTimer = tempTimer + DM_INPUT_CHECK_DELAY_msec
		ENDIF
	
	
	// Move left
	ELIF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
	OR IS_KEYBOARD_KEY_PRESSED(KEY_LEFT)
	OR (leftStickLR < -50)
		IF sDebugMenuData.iCurrentDepth > 0
			sDebugMenuData.iCurrentDepth--
			RESET_MISSION_BUGS_DATA(sDebugMenuData.sBugData, TRUE, TRUE, TRUE)
		ENDIF
		
		sDebugMenuData.iAllowInputCheckTimer = tempTimer + DM_INPUT_CHECK_DELAY_msec
		
		
	// Sort
	ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_X)
		IF sDebugMenuData.eMissionSort < INT_TO_ENUM(DEBUG_MENU_SORT_ENUM, ENUM_TO_INT(DM_MAX_NUMBER_OF_SORT_ENUMS)-1)
			sDebugMenuData.eMissionSort = INT_TO_ENUM(DEBUG_MENU_SORT_ENUM, ENUM_TO_INT(sDebugMenuData.eMissionSort)+1)
		ELSE
			sDebugMenuData.eMissionSort = INT_TO_ENUM(DEBUG_MENU_SORT_ENUM, 0)
		ENDIF
		
		INT i
		FOR i = 0 TO DM_MAX_DEPTH-1
			DM_SORT_MENU_LIST(sDebugMenuData, i)
		ENDFOR
		
		
	// Bugs
	ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LB)
		// Reset and display
		IF NOT sDebugMenuData.sBugData.bDisplay
		AND IS_DEBUG_MENU_ITEM_FLAG_SET(sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].iFlags, DM_BIT_BUG_INFO)
			sDebugMenuData.sBugData.bDisplay = TRUE
			
			// Build new list?
			IF NOT sDebugMenuData.sBugData.bInitialised
				SET_DEBUG_MENU_PROCESS_FLAG(sDebugMenuData.iProcessFlags, DM_PROCESS_BUGSTAR_GET_BUGS)
				RESET_MISSION_BUGS_DATA(sDebugMenuData.sBugData, FALSE, TRUE, TRUE)
				sDebugMenuData.sBugData.bInitialised = TRUE
			ENDIF
			
		// Hide
		ELSE
			sDebugMenuData.sBugData.bDisplay = FALSE
		ENDIF
		
	
	// Move down (bugs)
	ELIF (rightStickUD > 50)
		IF sDebugMenuData.sBugData.bDisplay
			IF NOT IS_DEBUG_MENU_PROCESS_FLAG_SET(sDebugMenuData.iProcessFlags, DM_PROCESS_BUGSTAR_GET_BUGS)
				IF sDebugMenuData.sBugData.iCurrentItem < sDebugMenuData.sBugData.iTotalItems-1
					sDebugMenuData.sBugData.iCurrentItem++
					
					// Build new list?
					IF sDebugMenuData.sBugData.iCurrentItem >= (sDebugMenuData.sBugData.iTopItem+DM_MAX_MISSION_BUGS)
						sDebugMenuData.sBugData.iTopItem = sDebugMenuData.sBugData.iCurrentItem
						DM_REMOVE_BUGSTAR_DEBUG_BUG_TEXT(sDebugMenuData.sBugData)
						SET_DEBUG_MENU_PROCESS_FLAG(sDebugMenuData.iProcessFlags, DM_PROCESS_BUGSTAR_GET_BUGS)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
	// Move up bugs (bugs)
	ELIF (rightStickUD < -50)
		IF sDebugMenuData.sBugData.bDisplay
			IF NOT IS_DEBUG_MENU_PROCESS_FLAG_SET(sDebugMenuData.iProcessFlags, DM_PROCESS_BUGSTAR_GET_BUGS)
				IF sDebugMenuData.sBugData.iCurrentItem > 0
					sDebugMenuData.sBugData.iCurrentItem--
					
					// Build new list?
					IF sDebugMenuData.sBugData.iCurrentItem < sDebugMenuData.sBugData.iTopItem
						sDebugMenuData.sBugData.iTopItem -= DM_MAX_MISSION_BUGS
						// Keep in range
						IF sDebugMenuData.sBugData.iTopItem < 0
							sDebugMenuData.sBugData.iTopItem = 0
						ENDIF
						DM_REMOVE_BUGSTAR_DEBUG_BUG_TEXT(sDebugMenuData.sBugData)
						SET_DEBUG_MENU_PROCESS_FLAG(sDebugMenuData.iProcessFlags, DM_PROCESS_BUGSTAR_GET_BUGS)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC


//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		PROCS TO DISPLAY THE DATA														//
//    																					//
//////////////////////////////////////////////////////////////////////////////////////////

/// PURPOSE: Returns the X scale of the screen
FUNC FLOAT DM_GET_SCREEN_SCALE_X()
	IF NOT GET_IS_WIDESCREEN()
		RETURN 0.021
	ENDIF
	
	RETURN 0.017
ENDFUNC

/// PURPOSE: Returns the Y scale of the screen
FUNC FLOAT DM_GET_SCREEN_SCALE_Y()
	RETURN 0.03
ENDFUNC

/// PURPOSE: Returns the X scale for item text
FUNC FLOAT DM_GET_TEXT_SCALE_X()
	RETURN 0.199
ENDFUNC

/// PURPOSE: Returns the Y scale for item text. Larger if text is selected
FUNC FLOAT DM_GET_TEXT_SCALE_Y(BOOL bSelected)
	IF bSelected
		//RETURN 0.420
		RETURN 0.345
	ENDIF
	RETURN 0.345
ENDFUNC

/// PURPOSE: Returns the X scale for item text
FUNC FLOAT DM_GET_INFO_TEXT_SCALE_X()
	RETURN 0.199
ENDFUNC

/// PURPOSE: Returns the Y scale for item text. Larger if text is selected
FUNC FLOAT DM_GET_INFO_TEXT_SCALE_Y(BOOL bSelected)
	IF bSelected
		RETURN 0.400
	ENDIF
	RETURN 0.310
ENDFUNC

/// PURPOSE: Returns the Y scale for item text. Larger if text is selected
FUNC FLOAT DM_GET_SMALL_TEXT_SCALE_Y()
	RETURN 0.305
ENDFUNC

/// PURPOSE: Returns the X spacing for item text
FUNC FLOAT DM_GET_TEXT_SPACING_X()
	RETURN 2.07
ENDFUNC

/// PURPOSE: Returns the Y spacing for item text
FUNC FLOAT DM_GET_TEXT_SPACING_Y(BOOL bSelected)
	IF bSelected
		//RETURN 3.230
		RETURN 3.800
	ENDIF
	RETURN 3.800
ENDFUNC

/// PURPOSE: Returns the X scale for title text
FUNC FLOAT DM_GET_TITLE_SCALE_X()
	RETURN 0.360
ENDFUNC

/// PURPOSE: Returns the Y scale for title text
FUNC FLOAT DM_GET_TITLE_SCALE_Y()
	RETURN 0.575
ENDFUNC

/// PURPOSE: Returns the X scale for help text
FUNC FLOAT DM_GET_HELP_SCALE_X()
	RETURN 0.360
ENDFUNC

/// PURPOSE: Returns the Y scale for help text
FUNC FLOAT DM_GET_HELP_SCALE_Y()
	RETURN 0.450
ENDFUNC

/// PURPOSE: Checks to see if the string is not null and not blank
FUNC BOOL DM_IS_TEXT_VALID(STRING s)
	IF IS_STRING_NULL(s)
		RETURN FALSE
	ENDIF
	
	IF ARE_STRINGS_EQUAL(s, "")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC STRING DM_CUTDOWN_TEXT(STRING sText, FLOAT xPos, FLOAT xMargin, BOOL bSelected)

	BOOL bFoundLen = FALSE
	FLOAT xEndPos
	INT iStrLen
	STRING sStr
	
	IF DOES_TEXT_LABEL_EXIST(sText)
		sStr = GET_STRING_FROM_TEXT_FILE(sText)
	ELSE
		sStr = sText
	ENDIF
	
	iStrLen = GET_LENGTH_OF_LITERAL_STRING(sStr)
	
	WHILE NOT bFoundLen
//	PRINTLN("\nchop ")PRINTINT(iStrLen)PRINTNL()
		SET_TEXT_SCALE(DM_GET_TEXT_SCALE_X(), DM_GET_TEXT_SCALE_Y(bSelected))
		xEndPos = xPos + GET_STRING_WIDTH_WITH_STRING("STRING", GET_STRING_FROM_STRING(sStr, 0, iStrLen)) //(DM_Get_Text_Scale_X() * DM_Get_Screen_Scale_X() * DM_Get_Text_Spacing_X() * iStrLen)
		SET_TEXT_SCALE(DM_GET_TEXT_SCALE_X(), DM_GET_TEXT_SCALE_Y(bSelected))
		xEndPos += GET_STRING_WIDTH("DM_CUTOFF_S")
		
		IF xEndPos < xMargin
			bFoundLen = TRUE
		ELSE
			iStrLen--
		ENDIF
	ENDWHILE
	
	/*IF DOES_TEXT_LABEL_EXIST(sText)
		STRING sStr = GET_STRING_FROM_TEXT_FILE(sText)
		sText = GET_STRING_FROM_STRING(sStr, 0, iStrLen)
	ENDIF*/
	
	RETURN GET_STRING_FROM_STRING(sStr, 0, iStrLen)
	
ENDFUNC

PROC DM_SET_COLOUR_FOR_DISABLED_ITEM()
	SET_TEXT_COLOUR(150, 150, 150, 150)
ENDPROC

/// PURPOSE: Displays the menu title to screen
PROC DM_DISPLAY_MENU_TITLE(FLOAT xPos, FLOAT yPos, STRING s)		
	
	IF NOT DM_IS_TEXT_VALID(s)
		EXIT
	ENDIF
	
	SET_TEXT_SCALE(DM_GET_TITLE_SCALE_X(), DM_GET_TITLE_SCALE_Y())
	SET_TEXT_COLOUR(255, 255, 255, 255)
	SET_TEXT_DROPSHADOW(0, 0, 0, 0, 0)
	SET_TEXT_EDGE(1, 0, 0, 0, 255)
	SET_TEXT_CENTRE(TRUE)
	DISPLAY_TEXT(xPos, yPos, s)
ENDPROC

/// PURPOSE: Displays the menu item to screen
PROC DM_DISPLAY_MENU_ITEM(BOOL bEnabled, BOOL bSelected, FLOAT xPos, FLOAT yPos, FLOAT xMargin, STRING s, BOOL shortText, INT r = 255, INT g = 255, INT b = 255, INT a = 255)
	
	IF NOT DM_IS_TEXT_VALID(s)
		EXIT
	ENDIF
	
	SET_TEXT_SCALE(DM_GET_TEXT_SCALE_X(), DM_GET_TEXT_SCALE_Y(bSelected))
	SET_TEXT_COLOUR(r, g, b, a)
	SET_TEXT_DROPSHADOW(0, 0, 0, 0, 4)
	SET_TEXT_EDGE(0, 0, 0, 0, 0)
	
	IF bSelected
		SET_TEXT_EDGE(1, 0, 0, 0, 255)
	ENDIF
	
	IF NOT bEnabled
		DM_SET_COLOUR_FOR_DISABLED_ITEM()
	ENDIF
	
	IF shortText
		IF xPos < xMargin

			FLOAT xEndPos = xPos + GET_STRING_WIDTH_WITH_STRING("STRING", s)	//(DM_Get_Text_Scale_X() * DM_Get_Screen_Scale_X() * DM_Get_Text_Spacing_X() * GET_LENGTH_OF_LITERAL_STRING(s))
			
			IF xEndPos > xMargin
				//DISPLAY_TEXT_WITH_LITERAL_STRING(xPos, yPos, "DM_CUTOFF_TS", DM_Cutdown_Text(s, xPos, xMargin, bSelected))
				DISPLAY_TEXT(xPos, yPos, "DM_CUTOFF_S")
			ELSE
				DISPLAY_TEXT_WITH_LITERAL_STRING(xPos, yPos, "STRING", s)
			ENDIF
		ENDIF
	ELSE
		DISPLAY_TEXT_WITH_LITERAL_STRING(xPos, yPos, "STRING", s)
	ENDIF
ENDPROC

/// PURPOSE: Displays the menu item from label to screen
///    Notes: If the label is not found then the text will just be printed
PROC DM_DISPLAY_MENU_ITEM_LABEL(BOOL bEnabled, BOOL bSelected, FLOAT xPos, FLOAT yPos, STRING sLabel, FLOAT xMargin = 1.0, BOOL shortText = FALSE)	

	IF NOT DOES_TEXT_LABEL_EXIST(sLabel)
		DM_DISPLAY_MENU_ITEM(bEnabled, bSelected, xPos, yPos, xMargin, sLabel, shortText)
		EXIT
	ENDIF

	SET_TEXT_SCALE(DM_GET_TEXT_SCALE_X(), DM_GET_TEXT_SCALE_Y(bSelected))
	SET_TEXT_COLOUR(255, 255, 255, 255)
	SET_TEXT_DROPSHADOW(0, 0, 0, 0, 4)
	SET_TEXT_EDGE(0, 0, 0, 0, 0)
	
	IF bSelected
		SET_TEXT_EDGE(1, 0, 0, 0, 255)
	ENDIF
	
	IF NOT bEnabled
		DM_SET_COLOUR_FOR_DISABLED_ITEM()
	ENDIF
	
	IF shortText
		IF xPos < xMargin

			FLOAT xEndPos = xPos + GET_STRING_WIDTH(sLabel)//(DM_Get_Text_Scale_X() * DM_Get_Screen_Scale_X() * DM_Get_Text_Spacing_X() * GET_LENGTH_OF_STRING_WITH_THIS_TEXT_LABEL(sLabel))
			
			IF xEndPos > xMargin
				//DISPLAY_TEXT_WITH_LITERAL_STRING(xPos, yPos, "DM_CUTOFF_TS", DM_Cutdown_Text(sLabel, xPos, xMargin, bSelected))
				DISPLAY_TEXT(xPos, yPos, "DM_CUTOFF_S")
			ELSE
				DISPLAY_TEXT(xPos, yPos, sLabel)
			ENDIF
		ENDIF
	ELSE
		DISPLAY_TEXT(xPos, yPos, sLabel)
	ENDIF
ENDPROC

/// PURPOSE: Displays the menu item to screen
PROC DM_DISPLAY_MENU_ITEM_BUG_COUNT(BOOL bEnabled, BOOL bSelected, FLOAT xPos, FLOAT yPos, FLOAT xMargin, INT iBugs, BOOL shortText)

	IF GET_COMMANDLINE_PARAM_EXISTS("nodebugperc")
		EXIT
	ENDIF
	
	// Force the bugs to be at least 0
	IF iBugs < 0 iBugs = 0 ENDIF
	
	// Work out which colour the bugs should be
	INT r, g, b
	
	IF (iBugs <= 5) 			// GREEN
		r=0		g=255	b=0
	ELIF (iBugs <= 10) 			// AMBER
		r=255 	g=191 	b=0
	ELSE			 			// RED
		r=255 	g=0		b=0
	ENDIF
	
	SET_TEXT_TO_USE_TEXT_FILE_COLOURS(TRUE)
	SET_TEXT_SCALE(DM_GET_TEXT_SCALE_X(), DM_GET_TEXT_SCALE_Y(bSelected))
	SET_TEXT_COLOUR(r, g, b, 255)
	SET_TEXT_DROPSHADOW(0, 0, 0, 0, 4)
	SET_TEXT_EDGE(0, 0, 0, 0, 0)
	
	IF bSelected
		SET_TEXT_EDGE(1, 0, 0, 0, 255)
	ENDIF
	
	IF NOT bEnabled
		DM_SET_COLOUR_FOR_DISABLED_ITEM()
	ENDIF
	
	IF shortText
		IF xPos < xMargin

			FLOAT xEndPos = xPos + GET_STRING_WIDTH_WITH_NUMBER("NUMBER", iBugs)	//(DM_Get_Text_Scale_X() * DM_Get_Screen_Scale_X() * DM_Get_Text_Spacing_X() * GET_LENGTH_OF_LITERAL_STRING(s))
			
			IF xEndPos > xMargin
				//DISPLAY_TEXT_WITH_LITERAL_STRING(xPos, yPos, "DM_CUTOFF_TS", DM_Cutdown_Text(s, xPos, xMargin, bSelected))
				DISPLAY_TEXT(xPos, yPos, "DM_CUTOFF_S")
			ELSE
				DISPLAY_TEXT_WITH_NUMBER(xPos, yPos, "NUMBER", iBugs)
			ENDIF
		ENDIF
	ELSE
		DISPLAY_TEXT_WITH_NUMBER(xPos, yPos, "NUMBER", iBugs)
	ENDIF
ENDPROC


/// PURPOSE: Displays highlighted menu item using text label
PROC DM_DISPLAY_MENU_ITEM_LABEL_HIGHLIGHT(BOOL bEnabled, BOOL bSelected, FLOAT xPos, FLOAT yPos, STRING sLabel, STRING sCol, FLOAT xMargin = 1.0, BOOL shortText = FALSE)
	
	IF NOT DOES_TEXT_LABEL_EXIST(sLabel)
		DM_DISPLAY_MENU_ITEM(bEnabled, bSelected, xPos, yPos, xMargin, sLabel, shortText)
		EXIT
	ENDIF
	
	SET_TEXT_SCALE(DM_GET_TEXT_SCALE_X(), DM_GET_TEXT_SCALE_Y(bSelected))
	SET_TEXT_COLOUR(255, 255, 255, 255)
	SET_TEXT_DROPSHADOW(0, 0, 0, 0, 4)
	SET_TEXT_EDGE(0, 0, 0, 0, 0)
	
	IF bSelected
		SET_TEXT_EDGE(1, 0, 0, 0, 255)
	ENDIF
	
	IF NOT bEnabled
		DM_SET_COLOUR_FOR_DISABLED_ITEM()
	ENDIF
	
	IF shortText
		IF xPos < xMargin
			
			FLOAT xEndPos = xPos + GET_STRING_WIDTH(sLabel)//(DM_Get_Text_Scale_X() * DM_Get_Screen_Scale_X() * DM_Get_Text_Spacing_X() * GET_LENGTH_OF_STRING_WITH_THIS_TEXT_LABEL(sLabel))
			
			IF xEndPos > xMargin
			
			//	TEXT_LABEL_63 sText
			//	sText = GET_STRING_FROM_TEXT_FILE(sCol)
				//sText += DM_Cutdown_Text(sLabel, xPos, xMargin, bSelected)
				//sText += GET_STRING_FROM_TEXT_FILE("DM_CUTOFF_S")
				//DISPLAY_TEXT_WITH_LITERAL_STRING(xPos, yPos, "STRING", sText)
				DISPLAY_TEXT_WITH_TWO_STRINGS(xPos, yPos, "DM_MISSTITLE", sCol, "DM_CUTOFF_S")
			ELSE
				DISPLAY_TEXT_WITH_TWO_STRINGS(xPos, yPos, "DM_MISSTITLE", sCol, sLabel)
			ENDIF
		ENDIF
	ELSE
		DISPLAY_TEXT_WITH_TWO_STRINGS(xPos, yPos, "DM_MISSTITLE", sCol, sLabel)
	ENDIF
ENDPROC

/// PURPOSE: Displays the percentage item
PROC DM_DISPLAY_MENU_ITEM_PERC(BOOL bEnabled, BOOL bSelected, FLOAT xPos, FLOAT yPos, INT iPerc, FLOAT xMargin = 1.0, BOOL shortText = FALSE)	

	IF GET_COMMANDLINE_PARAM_EXISTS("nodebugperc")
		EXIT
	ENDIF
	
	// Force the percentage to be at least 0
	IF iPerc < 0 iPerc = 0 ENDIF
	
	// Work out which colour the percentage should be
	INT r, g, b
	
	IF (iPerc = -1) 			// WHITE
		r=255	g=255	b=255
	ELIF (iPerc <= 70) 			// RED
		r=255 	g=0		b=0
	ELIF (iPerc <= 94) 			// AMBER
		r=255 	g=191 	b=0
	//ELIF (iPerc <= 99) 			// GREEN
	ELSE
		r=0		g=255	b=0
	//ELSE 						// WHITE
	//	r=255	g=255	b=255
	ENDIF
	
	SET_TEXT_TO_USE_TEXT_FILE_COLOURS(TRUE)
	SET_TEXT_SCALE(DM_GET_TEXT_SCALE_X(), DM_GET_TEXT_SCALE_Y(bSelected))
	SET_TEXT_COLOUR(r, g, b, 255)
	SET_TEXT_DROPSHADOW(0, 0, 0, 0, 4)
	SET_TEXT_EDGE(0, 0, 0, 0, 0)
	
	IF bSelected
		SET_TEXT_EDGE(1, 0, 0, 0, 255)
	ENDIF
	
	IF NOT bEnabled
		DM_SET_COLOUR_FOR_DISABLED_ITEM()
	ENDIF
	
	IF shortText
		IF xPos < xMargin

			FLOAT xEndPos = xPos + GET_STRING_WIDTH_WITH_NUMBER("DM_PERC", iPerc)//(DM_Get_Text_Scale_X() * DM_Get_Screen_Scale_X() * DM_Get_Text_Spacing_X() * 4) // 4 obtained from 3 digits+%
			
			IF xEndPos > xMargin
				DISPLAY_TEXT(xPos, yPos, "DM_CUTOFF_S")
			ELSE
				DISPLAY_TEXT_WITH_NUMBER(xPos, yPos, "DM_PERC", iPerc)
			ENDIF
		ENDIF
	ELSE
		DISPLAY_TEXT_WITH_NUMBER(xPos, yPos, "DM_PERC", iPerc)
	ENDIF
ENDPROC

/// PURPOSE: Displays the menu help from label to screen
PROC DM_DISPLAY_MENU_HELP_LABEL(FLOAT xPos, FLOAT yPos, STRING sLabel)
	SET_TEXT_SCALE(DM_GET_HELP_SCALE_X(), DM_GET_HELP_SCALE_Y())
	SET_TEXT_COLOUR(255, 255, 255, 255)
	SET_TEXT_DROPSHADOW(0, 0, 0, 0, 4)
	SET_TEXT_EDGE(0, 0, 0, 0, 0)
	
	IF DOES_TEXT_LABEL_EXIST(sLabel)
		DISPLAY_TEXT(xPos, yPos, sLabel)
	ENDIF
ENDPROC

/// PURPOSE: Displays the menu items info to screen
PROC DM_DISPLAY_INFO_TITLE(FLOAT fX, FLOAT fY, STRING sLabel, INT iAlpha = 255)

	SET_TEXT_SCALE(DM_GET_TEXT_SCALE_X(), DM_GET_TEXT_SCALE_Y(FALSE))
	//SET_TEXT_COLOUR(127, 206, 255, 115)
	SET_TEXT_COLOUR(100, 100, 100, iAlpha)
	SET_TEXT_CENTRE(FALSE)
	SET_TEXT_DROPSHADOW(0, 0, 0, 0, 0)
	SET_TEXT_EDGE(0, 0, 0, 0, 0)

	IF DOES_TEXT_LABEL_EXIST(sLabel)
		DISPLAY_TEXT(fX, fY, sLabel)
	ELSE
		DISPLAY_TEXT_WITH_LITERAL_STRING(fX, fY, "STRING", sLabel)
	ENDIF
ENDPROC

/// PURPOSE: Displays the menu items info to screen
PROC DM_DISPLAY_INFO_TEXT(FLOAT fX, FLOAT fY, FLOAT fWrapEndX, STRING sLabel, BOOL bLarge, INT iR = 255, INT iG = 255, INT iB = 255, INT iA = 255)
	
	IF NOT DM_IS_TEXT_VALID(sLabel)
		sLabel = "DM_BUG_NA"
	ENDIF
	
	SET_TEXT_SCALE(DM_GET_INFO_TEXT_SCALE_X(), DM_GET_INFO_TEXT_SCALE_Y(bLarge))
	SET_TEXT_COLOUR(iR, iG, iB, iA)
	SET_TEXT_CENTRE(FALSE)
	SET_TEXT_DROPSHADOW(0, 0, 0, 0, 0)
	SET_TEXT_EDGE(0, 0, 0, 0, 0)
	
	IF fWrapEndX <> -1
		SET_TEXT_WRAP(fX, fWrapEndX)
	ENDIF

	IF DOES_TEXT_LABEL_EXIST(sLabel)
		DISPLAY_TEXT(fX, fY, sLabel)
	ELSE
		DISPLAY_TEXT_WITH_LITERAL_STRING(fX, fY, "STRING", sLabel)
	ENDIF
ENDPROC

/// PURPOSE: Displays the menu items bug info to screen
///    NOTE: iColourStyle: -1=Grey-White, 0=White-Amber-Red, 1=Red
PROC DM_DISPLAY_BUG_TEXT(FLOAT fX, FLOAT fY, STRING sLabel, BOOL bLarge, INT iNumber = -1, INT iColourStyle = -1)
	
	IF NOT DM_IS_TEXT_VALID(sLabel)
		EXIT
	ENDIF
	
	IF NOT DOES_TEXT_LABEL_EXIST(sLabel)
		EXIT
	ENDIF
	
	IF bLarge
		SET_TEXT_SCALE(DM_GET_TEXT_SCALE_X(), DM_GET_TEXT_SCALE_Y(FALSE))
	ELSE
		SET_TEXT_SCALE(DM_GET_TEXT_SCALE_X(), DM_GET_SMALL_TEXT_SCALE_Y())
	ENDIF
	
	IF iColourStyle = -1 // Grey-White
		IF bLarge
			SET_TEXT_COLOUR(100, 100, 100, 255)
		ELSE
			SET_TEXT_COLOUR(255, 255, 255, 255)
		ENDIF
	ELIF iColourStyle = 0 // White-Amber-Red
		IF iNumber = 0
			SET_TEXT_COLOUR(255, 255, 255, 255)
		ELIF iNumber > 10
			SET_TEXT_COLOUR(255, 0, 0, 255)
		ELSE
			SET_TEXT_COLOUR(255, 191, 0, 255)
		ENDIF
	ELIF iColourStyle = 1 // Red
		IF iNumber > 0
			SET_TEXT_COLOUR(255, 0, 0, 255)
		ENDIF
	ENDIF
	
	SET_TEXT_CENTRE(FALSE)
	SET_TEXT_DROPSHADOW(0, 0, 0, 0, 0)
	SET_TEXT_EDGE(0, 0, 0, 0, 0)
	
	IF iNumber <> -1
		DISPLAY_TEXT_WITH_NUMBER(fX, fY, sLabel, iNumber)
	ELSE
		DISPLAY_TEXT(fX, fY, sLabel)
	ENDIF
ENDPROC

/// PURPOSE: Displays an integer on the info screen
///    NOTE: iColourStyle: -1=White, 0=White-Amber-Red, 1=Red
PROC DM_DISPLAY_INFO_NUMBER(FLOAT fX, FLOAT fY, INT iNumber, BOOL bLarge, INT iColourStyle = -1, INT iAlpha = 255)
	
	SET_TEXT_SCALE(DM_GET_INFO_TEXT_SCALE_X(), DM_GET_INFO_TEXT_SCALE_Y(bLarge))
	
	IF iColourStyle = -1 // White
		SET_TEXT_COLOUR(255, 255, 255, iAlpha)
	ELIF iColourStyle = 0 // White-Amber-Red
		IF iNumber = 0
			SET_TEXT_COLOUR(255, 255, 255, iAlpha)
		ELIF iNumber > 10
			SET_TEXT_COLOUR(255, 0, 0, iAlpha)
		ELSE
			SET_TEXT_COLOUR(255, 191, 0, iAlpha)
		ENDIF
	ELIF iColourStyle = 1 // Red
		IF iNumber > 0
			SET_TEXT_COLOUR(255, 0, 0, iAlpha)
		ELSE
			SET_TEXT_COLOUR(255, 255, 255, iAlpha)
		ENDIF
	ENDIF
	
	SET_TEXT_CENTRE(FALSE)
	SET_TEXT_DROPSHADOW(0, 0, 0, 0, 0)
	SET_TEXT_EDGE(0, 0, 0, 0, 0)

	DISPLAY_TEXT_WITH_NUMBER(fX, fY, "DM_NUM", iNumber)
ENDPROC

/// PURPOSE: Displays the menu items info to screen
FUNC FLOAT DM_GET_INFO_TEXT_SIZE(STRING sLabel, BOOL bLarge)

	IF NOT DM_IS_TEXT_VALID(sLabel)
		sLabel = "DM_BUG_NA"
	ENDIF

	IF DOES_TEXT_LABEL_EXIST(sLabel)
		SET_TEXT_FONT(FONT_STANDARD)
		SET_TEXT_SCALE(DM_GET_INFO_TEXT_SCALE_X(), DM_GET_INFO_TEXT_SCALE_Y(bLarge))
		RETURN GET_STRING_WIDTH(sLabel)
	ELSE
	
		SET_TEXT_FONT(FONT_STANDARD)
		SET_TEXT_SCALE(DM_GET_INFO_TEXT_SCALE_X(), DM_GET_INFO_TEXT_SCALE_Y(bLarge))
		RETURN GET_STRING_WIDTH_WITH_STRING("STRING",sLabel)
	
		// Attempt to work out the size
		//RETURN (DM_Get_Info_Text_Scale_X() * DM_Get_Screen_Scale_X() * DM_Get_Text_Spacing_X() * GET_LENGTH_OF_LITERAL_STRING(sLabel))
	ENDIF
ENDFUNC

/// PURPOSE: Return the key string for the given key number
FUNC STRING DM_GET_STRING_FOR_DEBUG_KEY(KEY_NUMBER eKey)
	SWITCH eKey
		CASE KEY_A	RETURN "a"	BREAK	CASE KEY_B	RETURN "b"	BREAK	CASE KEY_C	RETURN "c"	BREAK
		CASE KEY_D	RETURN "d"	BREAK	CASE KEY_E	RETURN "e"	BREAK	CASE KEY_F	RETURN "f"	BREAK
		CASE KEY_G	RETURN "g"	BREAK	CASE KEY_H	RETURN "h"	BREAK	CASE KEY_I	RETURN "i"	BREAK
		CASE KEY_J 	RETURN "j"	BREAK	CASE KEY_K	RETURN "k"	BREAK	CASE KEY_L	RETURN "l"	BREAK
		CASE KEY_M	RETURN "m"	BREAK	CASE KEY_N	RETURN "n"	BREAK	CASE KEY_O	RETURN "o"	BREAK
		CASE KEY_P	RETURN "p"	BREAK	CASE KEY_Q	RETURN "q"	BREAK	CASE KEY_R	RETURN "r"	BREAK
		CASE KEY_S	RETURN "s"	BREAK	CASE KEY_T	RETURN "t"	BREAK	CASE KEY_U	RETURN "u"	BREAK
		CASE KEY_V	RETURN "v"	BREAK	CASE KEY_W	RETURN "w"	BREAK	CASE KEY_X	RETURN "x"	BREAK
		CASE KEY_Y	RETURN "y"	BREAK	CASE KEY_Z	RETURN "z"	BREAK	CASE KEY_0	RETURN "0"	BREAK
		CASE KEY_1	RETURN "1"	BREAK	CASE KEY_2	RETURN "2"	BREAK	CASE KEY_3	RETURN "3"	BREAK
		CASE KEY_4	RETURN "4"	BREAK	CASE KEY_5	RETURN "5"	BREAK	CASE KEY_6	RETURN "6"	BREAK
		CASE KEY_7	RETURN "7"	BREAK	CASE KEY_8	RETURN "8"	BREAK	CASE KEY_9	RETURN "9"	BREAK
	ENDSWITCH
	RETURN "XXX"
ENDFUNC

/// PURPOSE: Return the key number for the given int
FUNC KEY_NUMBER DM_GET_DEBUG_KEY_FOR_INT(INT iKey)
	SWITCH iKey
		CASE 0	RETURN KEY_A	BREAK	CASE 1	RETURN KEY_B	BREAK	CASE 2	RETURN KEY_C	BREAK
		CASE 3	RETURN KEY_D	BREAK	CASE 4	RETURN KEY_E	BREAK	CASE 5	RETURN KEY_F	BREAK
		CASE 6	RETURN KEY_G	BREAK	CASE 7	RETURN KEY_H	BREAK	CASE 8	RETURN KEY_I	BREAK
		CASE 9 	RETURN KEY_J	BREAK	CASE 10	RETURN KEY_K	BREAK	CASE 11	RETURN KEY_L	BREAK
		CASE 12	RETURN KEY_M	BREAK	CASE 13	RETURN KEY_N	BREAK	CASE 14	RETURN KEY_O	BREAK
		CASE 15	RETURN KEY_P	BREAK	CASE 16	RETURN KEY_Q	BREAK	CASE 17	RETURN KEY_R	BREAK
		CASE 18	RETURN KEY_S	BREAK	CASE 19	RETURN KEY_T	BREAK	CASE 20	RETURN KEY_U	BREAK
		CASE 21	RETURN KEY_V	BREAK	CASE 22	RETURN KEY_W	BREAK	CASE 23	RETURN KEY_X	BREAK
		CASE 24	RETURN KEY_Y	BREAK	CASE 25	RETURN KEY_Z	BREAK	CASE 26	RETURN KEY_0	BREAK
		CASE 27	RETURN KEY_1	BREAK	CASE 28	RETURN KEY_2	BREAK	CASE 29	RETURN KEY_3	BREAK
		CASE 30	RETURN KEY_4	BREAK	CASE 31	RETURN KEY_5	BREAK	CASE 32	RETURN KEY_6	BREAK
		CASE 33	RETURN KEY_7	BREAK	CASE 34	RETURN KEY_8	BREAK	CASE 35	RETURN KEY_9	BREAK
	ENDSWITCH
	RETURN KEY_SPACE
ENDFUNC

/// PURPOSE: Displays a border around the current selected item
PROC DM_DISPLAY_MENU_ITEM_BORDER(INT &iCurrentItem[DM_MAX_DEPTH], INT &iCurrentDepth, BOOL &bUpdatingItem, BOOL bSelected, BOOL bEnabled, FLOAT fTextY, INT iItem, INT iDepth, INT iType, BOOL bShortText)

	fTextY+= 0.017
	
	FLOAT fX, fY, fW, fH
		
	SWITCH iDepth
		CASE 0
			IF bShortText
				fX = 0.1310  fY = fTextY  fW = 0.1650  fH = 0.0395
			ELSE
				fX = 0.5000  fY = fTextY  fW = 0.9030  fH = 0.0395
			ENDIF
		BREAK
		
		CASE 1
			IF bShortText
				fX = 0.3070  fY = fTextY  fW = 0.1650  fH = 0.0395
			ELSE
				fX = 0.5880  fY = fTextY  fW = 0.7270  fH = 0.0395
			ENDIF
		BREAK
		
		CASE 2
			IF bShortText
				fX = 0.4830  fY = fTextY  fW = 0.1650  fH = 0.0395
			ELSE
				fX = 0.6762  fY = fTextY  fW = 0.5520  fH = 0.0395
			ENDIF	
		BREAK
	ENDSWITCH
	
	
	IF bSelected
		IF bEnabled
			//DRAW_RECT(fX, fY, fW, fH, 127, 206, 255, 115)
			DRAW_RECT(fX, fY, fW, fH, 100, 100, 100, 255)
		ELSE
			//DRAW_RECT(fX, fY, fW, fH, 127, 206, 255, 50)
			DRAW_RECT(fX, fY, fW, fH, 100, 100, 100, 100)
		ENDIF
	ENDIF
	
	// Use this border to determine if the user has clicked the item
	IF IS_MOUSE_BUTTON_JUST_PRESSED(MB_LEFT_BTN)
		FLOAT fMouseX, fMouseY
		GET_MOUSE_POSITION(fMouseX, fMouseY)
		
		// Selectable type
		IF iType <> DM_HASH_ITEM_BREAK
			// In range
			IF ((fMouseX >= fX - (fW/2.0)) AND (fMouseX <= fX + (fW/2.0)))	
				IF ((fMouseY >= fY - (fH/2.0)) AND (fMouseY <= fY + (fH/2.0)))
					// Not current
					IF iCurrentDepth <> iDepth OR iCurrentItem[iDepth] <> iItem
						// Update
						iCurrentDepth = iDepth
						iCurrentItem[iDepth] = iItem
						bUpdatingItem = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE: Works out the size and position of the scroll bar then displays on screen
///    NOTE: This was quickly put together so scroll bar may not be entirely accurate
PROC DM_DISPLAY_SCROLL_BAR(INT iDepth, INT iItemCount, INT iFirstItem, /*INT iSelectedItem, */BOOL bShortText, /*BOOL bUp, BOOL bDown,*/ INT iAlpha)

	FLOAT fPanelY = 0.3520
	
	FLOAT fScrollBarHeight = DM_GET_TEXT_SCALE_Y(FALSE) * DM_GET_SCREEN_SCALE_Y() * DM_GET_TEXT_SPACING_Y(FALSE) * (DM_MAX_ITEMS_IN_COLUMN-1)
	fScrollBarHeight += (DM_GET_TEXT_SCALE_Y(TRUE) * DM_GET_SCREEN_SCALE_Y() * DM_GET_TEXT_SPACING_Y(TRUE) * 1)
	
	// Find out the total height that the items cover
	FLOAT fTotalHeight 
	fTotalHeight = DM_GET_TEXT_SCALE_Y(FALSE) * DM_GET_SCREEN_SCALE_Y() * DM_GET_TEXT_SPACING_Y(FALSE) * (iItemCount-1)
	fTotalHeight += (DM_GET_TEXT_SCALE_Y(TRUE) * DM_GET_SCREEN_SCALE_Y() * DM_GET_TEXT_SPACING_Y(TRUE) * 1)
	
	// Work out the scale between the scroll bar and total item height
	FLOAT fScrollBarScale
	fScrollBarScale = fScrollBarHeight / fTotalHeight
	
	// Scale the scroll block
	FLOAT fBlockHeight
	fBlockHeight = fScrollBarHeight * fScrollBarScale
	
	// Grab the position of the item
	FLOAT fPosition
	fPosition = DM_GET_TEXT_SCALE_Y(FALSE) * DM_GET_SCREEN_SCALE_Y() * DM_GET_TEXT_SPACING_Y(FALSE) * (iFirstItem)
		
	// Work out the scale of the item in percentage form
	FLOAT fPositionScale
	fPositionScale = fPosition / fTotalHeight
		
	// Work out the final position of the scroll block
	FLOAT fBlockPos
	//fBlockPos = 0.3935 - (fScrollBarHeight * 0.5) + (fBlockHeight * 0.5) + (fScrollBarHeight * fPositionScale)
	fBlockPos = fPanelY - (fScrollBarHeight * 0.5) + (fBlockHeight * 0.5) + (fScrollBarHeight * fPositionScale)
	
	// Work out the scale of 1 item
	//FLOAT fItemHeight
	//fItemHeight = fBlockHeight / DM_MAX_ITEMS_IN_COLUMN
	
	// Work out the position of the current selected item
	//FLOAT fItemPos
	//fItemPos = fBlockPos - (fBlockHeight * 0.5) + (fItemHeight * iSelectedItem) + (fItemHeight * 0.5)
	

	FLOAT fPanelX = 0.950
	
	IF bShortText
		IF iDepth = 0
			fPanelX = 0.212
		ELIF iDepth = 1
			fPanelX = 0.387
		ENDIF
	ENDIF
	
	// UI - Scroll bar
	DRAW_RECT(fPanelX, 0.3540, 0.007, 0.5050, MENU_BG_R, MENU_BG_G, MENU_BG_B, iAlpha) // Scroll background
	DRAW_RECT(fPanelX, fBlockPos, 0.007, fBlockHeight, 75, 75, 75, iAlpha) // Scroll block
	//DRAW_RECT(fPanelX, fItemPos, 0.0045, fItemHeight-0.005, 161, 158, 163, iAlpha) // Item block
	
	/*IF bUp
		SET_TEXT_CENTRE(TRUE)
		DM_Display_Menu_Item(TRUE, FALSE, fPanelX, fPanelY-(fScrollBarHeight/2.0)-0.016, 1.0, "^", FALSE, 255, 255, 255, iAlpha)
	ENDIF
	IF bDown
		SET_TEXT_CENTRE(TRUE)
		DM_Display_Menu_Item(TRUE, FALSE, fPanelX, fPanelY+(fScrollBarHeight/2.0)-0.007, 1.0, "v", FALSE, 255, 255, 255, iAlpha)			
	ENDIF*/
ENDPROC

/// PURPOSE: Displays the additional information in the details pane
PROC DM_DISPLAY_MENU_ITEM_INFO(DEBUG_MENU_ITEM_STRUCT &sItem, BUGSTAR_DATA_STRUCT &sBugstarData, MISSION_BUGS_STRUCT &sBugs, BOOL bLoading, INT iAlpha)

	// Display mission bugs?
	IF sBugs.bDisplay
		INT i, iItemsAdded
		FLOAT fTextX, fTextY, fNewLine
		TEXT_LABEL_15 sTempLabel
		
		IF bLoading
			// Loading
			fTextX = 0.060
			fTextY = 0.620
			DM_DISPLAY_INFO_TEXT(fTextX, fTextY, 1.0, "LOADING BUGS...", FALSE, 255, 255, 255, iAlpha)
		ELSE
			fTextX = 0.060
			fTextY = 0.620
			fNewLine = 0.021
			
			//g_debugMenuControl.fTempPos[0] = 0.060
			
			DM_DISPLAY_INFO_TITLE(0.060, fTextY, "DM_BUGS_NUM", iAlpha)
			DM_DISPLAY_INFO_TITLE(0.110, fTextY, "DM_BUGS_SUM", iAlpha)
			DM_DISPLAY_INFO_TITLE(0.683, fTextY, "DM_BUGS_CLASS", iAlpha)
			DM_DISPLAY_INFO_TITLE(0.725, fTextY, "DM_BUGS_DAYS", iAlpha)
			//DM_DISPLAY_INFO_TITLE(0.725, fTextY, "DM_BUGS_CREATE", iAlpha) // temp until we get days open
			DM_DISPLAY_INFO_TITLE(0.785, fTextY, "DM_BUGS_DUE", iAlpha)
			DM_DISPLAY_INFO_TITLE(0.845, fTextY, "DM_BUGS_DEV", iAlpha)
			
			REPEAT DM_MAX_MISSION_BUGS i
				IF sBugs.iBugNumber[i] <> 0
					// Number
					sTempLabel = ""
					sTempLabel += sBugs.iBugNumber[i]
					DM_DISPLAY_INFO_TEXT(0.0600, fTextY+(fNewLine*(i+1)), 1.0, sTempLabel, FALSE, 255, 255, 255, iAlpha)
					
					// Summary
					sTempLabel = ""
					sTempLabel += sBugs.iBugNumber[i]
					sTempLabel += "_S"
					IF DOES_TEXT_LABEL_EXIST(sTempLabel)
						DM_DISPLAY_INFO_TEXT(0.110, fTextY+(fNewLine*(i+1)), 1.0, sTempLabel, FALSE, 255, 255, 255, iAlpha)
					ENDIF
					
					// Class
					sTempLabel = ""
					sTempLabel += sBugs.iBugNumber[i]
					sTempLabel += "_C"
					IF DOES_TEXT_LABEL_EXIST(sTempLabel)
						DM_DISPLAY_INFO_TEXT(0.683, fTextY+(fNewLine*(i+1)), 1.0, sTempLabel, FALSE, 255, 255, 255, iAlpha)
					ENDIF
					
					// Days Open
					sTempLabel = ""
					sTempLabel += sBugs.iDaysOpen[i]
					DM_DISPLAY_INFO_TEXT(0.725, fTextY+(fNewLine*(i+1)), 1.0, sTempLabel, FALSE, 255, 255, 255, iAlpha)
					
					// Created on = temp until we get days open
					/*sTempLabel = ""
					sTempLabel += sBugs.iBugNumber[i]
					sTempLabel += "_DO"
					IF DOES_TEXT_LABEL_EXIST(sTempLabel)
						DM_DISPLAY_INFO_TEXT(0.725, fTextY+(fNewLine*(i+1)), 1.0, sTempLabel, FALSE, 255, 255, 255, iAlpha)
					ENDIF*/
					
					// Due Date
					sTempLabel = ""
					sTempLabel += sBugs.iBugNumber[i]
					sTempLabel += "_DD"
					IF DOES_TEXT_LABEL_EXIST(sTempLabel)
						DM_DISPLAY_INFO_TEXT(0.785, fTextY+(fNewLine*(i+1)), 1.0, sTempLabel, FALSE, 255, 255, 255, iAlpha)
					ENDIF
					
					// Developer
					sTempLabel = ""
					sTempLabel += sBugs.iBugNumber[i]
					sTempLabel += "_D"
					IF DOES_TEXT_LABEL_EXIST(sTempLabel)
						DM_DISPLAY_INFO_TEXT(0.845, fTextY+(fNewLine*(i+1)), 1.0, sTempLabel, FALSE, 255, 255, 255, iAlpha)
					ENDIF
					
					iItemsAdded++
				ENDIF
			ENDREPEAT
			
			IF iItemsAdded > 0
				// Item border
				DRAW_RECT(0.5000, 0.654+(fNewLine*(sBugs.iCurrentItem-sBugs.iTopItem)), 0.9030, 0.0200, 100, 100, 100, iAlpha)
			ELSE
				DM_DISPLAY_INFO_TEXT(fTextX, fTextY+fNewLine, 1.0, "No mission bugs found.", FALSE, 255, 255, 255, iAlpha)
			ENDIF
		ENDIF
	ELSE
		INT i, j, k
		INT bugs[5]
		INT age[5]
		TEXT_LABEL_15 sTempLabel, sLargestLabel
		TEXT_LABEL_63 sLargestBigLabel
		FLOAT fTextX, fTextY, fNewLine, fWrapX
		
		fNewLine = 0.021
		fWrapX   = 0.5
		
		IF IS_DEBUG_MENU_ITEM_FLAG_SET(sItem.iFlags, DM_BIT_BUG_INFO)
		
			IF bLoading
				// Loading
				fTextX = 0.060
				fTextY = 0.620
				DM_DISPLAY_INFO_TEXT(fTextX, fTextY, 1.0, "LOADING INFO...", FALSE, 255, 255, 255, iAlpha)
			ELSE
				// Mission Owner
				fTextX = 0.060
				fTextY = 0.620
				
				DM_DISPLAY_INFO_TITLE(fTextX, fTextY, "DM_BUG_MO", iAlpha)
				
				sTempLabel = sItem.sAuth
				sTempLabel += "_F"
				DM_DISPLAY_INFO_TEXT(fTextX, fTextY+fNewLine, -1, sTempLabel, TRUE, 255, 255, 255, iAlpha)
				
				
				IF NOT g_bInMultiplayer
					// Mission Description
					fTextX = 0.060
					fTextY = 0.670
					
					sTempLabel = sItem.sMissionID
					IF g_debugMenuControl.bUseBugstarQuery
						sTempLabel += "_MD1"
					ELSE
						sTempLabel += "_MD"
					ENDIF
					
					DM_DISPLAY_INFO_TITLE(fTextX, fTextY, "DM_BUG_MD", iAlpha)
					IF DOES_TEXT_LABEL_EXIST(sTempLabel)
						DM_DISPLAY_INFO_TEXT(fTextX, fTextY+fNewLine, fWrapX, sTempLabel, FALSE, 255, 255, 255, iAlpha)
					ELSE
						//PRINTLN("Mission mission description ", sTempLabel)
					ENDIF
					
					
					// Story Description
					fTextX = 0.060
					fTextY = 0.730
					
					sTempLabel = sItem.sMissionID
					IF g_debugMenuControl.bUseBugstarQuery
						sTempLabel += "_SD1"
					ELSE
						sTempLabel += "_SD"
					ENDIF
					
					DM_DISPLAY_INFO_TITLE(fTextX, fTextY, "DM_BUG_SD", iAlpha)
					IF DOES_TEXT_LABEL_EXIST(sTempLabel)
						DM_DISPLAY_INFO_TEXT(fTextX, fTextY+fNewLine, fWrapX, sTempLabel, FALSE, 255, 255, 255, iAlpha)
					ELSE
						//PRINTLN("Mission story description ", sTempLabel)
					ENDIF
				ELSE
					// Mission Description
					fTextX = 0.060
					fTextY = 0.670
					
					sTempLabel = sItem.sMissionID
					IF g_debugMenuControl.bUseBugstarQuery
						sTempLabel += "_MD1"
					ELSE
						sTempLabel += "_MD"
					ENDIF
					
					DM_DISPLAY_INFO_TITLE(fTextX, fTextY, "DM_BUG_MD", iAlpha)
					IF DOES_TEXT_LABEL_EXIST(sTempLabel)
						DM_DISPLAY_INFO_TEXT(fTextX, fTextY+fNewLine, fWrapX, sTempLabel, FALSE, 255, 255, 255, iAlpha)
					ENDIF
					
					fTextY = 0.730
					
					sTempLabel = sItem.sMissionID
					IF g_debugMenuControl.bUseBugstarQuery
						sTempLabel += "_SD1"
					ELSE
						sTempLabel += "_SD"
					ENDIF
					
					IF DOES_TEXT_LABEL_EXIST(sTempLabel)
						DM_DISPLAY_INFO_TEXT(fTextX, fTextY, fWrapX, sTempLabel, FALSE, 255, 255, 255, iAlpha)
					ENDIF
				
				ENDIF
				
				
				// Mission Comments
				fTextX = 0.060
				fTextY = 0.815
				
				sTempLabel = sItem.sMissionID
				IF g_debugMenuControl.bUseBugstarQuery
					sTempLabel += "_MC1"
				ELSE
					sTempLabel += "_MC"
				ENDIF
				
				DM_DISPLAY_INFO_TITLE(fTextX, fTextY, "DM_BUG_MC", iAlpha)
				IF DOES_TEXT_LABEL_EXIST(sTempLabel)
					DM_DISPLAY_INFO_TEXT(fTextX, fTextY+fNewLine, fWrapX, sTempLabel, FALSE, 255, 0, 0, iAlpha)
				ENDIF
				
				
				IF NOT GET_COMMANDLINE_PARAM_EXISTS("nodebugperc")
					// Bugs Overview
					fTextX = 0.510
					fTextY = 0.620
					DM_DISPLAY_INFO_TITLE(fTextX, fTextY, "DM_BUG_BUGSO", iAlpha)
					
					// TOTAL
					i = (sBugstarData.iABugs + sBugstarData.iBBugs + sBugstarData.iCBugs + sBugstarData.iDBugs + sBugstarData.iTasks + sBugstarData.iTodos + sBugstarData.iWish)
					sTempLabel = " " sTempLabel += i
					DM_DISPLAY_INFO_TEXT(fTextX, fTextY+fNewLine, -1, "DM_BUG_TOTAL", FALSE, 255, 255, 255, iAlpha) 	fTextX += DM_GET_INFO_TEXT_SIZE("DM_BUG_TOTAL", FALSE)
					DM_DISPLAY_INFO_NUMBER(fTextX, fTextY+fNewLine, i, FALSE, 0, iAlpha) 				fTextX += DM_GET_INFO_TEXT_SIZE(sTempLabel, FALSE)
					
					// A
					i = (sBugstarData.iABugs)
					sTempLabel = " " sTempLabel += i
					DM_DISPLAY_INFO_TEXT(fTextX, fTextY+fNewLine, -1, "DM_BUG_A", FALSE, 255, 255, 255, iAlpha) 		fTextX += DM_GET_INFO_TEXT_SIZE("DM_BUG_A", FALSE)
					DM_DISPLAY_INFO_NUMBER(fTextX, fTextY+fNewLine, i, FALSE, -1, iAlpha) 					fTextX += DM_GET_INFO_TEXT_SIZE(sTempLabel, FALSE)
					
					// B
					i = (sBugstarData.iBBugs)
					sTempLabel = " " sTempLabel += i
					DM_DISPLAY_INFO_TEXT(fTextX, fTextY+fNewLine, -1, "DM_BUG_B", FALSE, 255, 255, 255, iAlpha) 		fTextX += DM_GET_INFO_TEXT_SIZE("DM_BUG_B", FALSE)
					DM_DISPLAY_INFO_NUMBER(fTextX, fTextY+fNewLine, i, FALSE, -1, iAlpha) 					fTextX += DM_GET_INFO_TEXT_SIZE(sTempLabel, FALSE)
					
					// C
					i = (sBugstarData.iCBugs)
					sTempLabel = " " sTempLabel += i
					DM_DISPLAY_INFO_TEXT(fTextX, fTextY+fNewLine, -1, "DM_BUG_C", FALSE, 255, 255, 255, iAlpha) 		fTextX += DM_GET_INFO_TEXT_SIZE("DM_BUG_C", FALSE)
					DM_DISPLAY_INFO_NUMBER(fTextX, fTextY+fNewLine, i, FALSE, -1, iAlpha) 					fTextX += DM_GET_INFO_TEXT_SIZE(sTempLabel, FALSE)
					
					// D
					i = (sBugstarData.iDBugs)
					sTempLabel = " " sTempLabel += i
					DM_DISPLAY_INFO_TEXT(fTextX, fTextY+fNewLine, -1, "DM_BUG_D", FALSE, 255, 255, 255, iAlpha) 		fTextX += DM_GET_INFO_TEXT_SIZE("DM_BUG_D", FALSE)
					DM_DISPLAY_INFO_NUMBER(fTextX, fTextY+fNewLine, i, FALSE, -1, iAlpha) 					fTextX += DM_GET_INFO_TEXT_SIZE(sTempLabel, FALSE)
					
					// TODO
					i = (sBugstarData.iTodos)
					sTempLabel = " " sTempLabel += i
					DM_DISPLAY_INFO_TEXT(fTextX, fTextY+fNewLine, -1, "DM_BUG_TODO", FALSE, 255, 255, 255, iAlpha) 	fTextX += DM_GET_INFO_TEXT_SIZE("DM_BUG_TODO", FALSE)
					DM_DISPLAY_INFO_NUMBER(fTextX, fTextY+fNewLine, i, FALSE, -1, iAlpha) 					fTextX += DM_GET_INFO_TEXT_SIZE(sTempLabel, FALSE)
					
					// TASK
					i = (sBugstarData.iTasks)
					sTempLabel = " " sTempLabel += i
					DM_DISPLAY_INFO_TEXT(fTextX, fTextY+fNewLine, -1, "DM_BUG_TASK", FALSE, 255, 255, 255, iAlpha) 	fTextX += DM_GET_INFO_TEXT_SIZE("DM_BUG_TASK", FALSE)
					DM_DISPLAY_INFO_NUMBER(fTextX, fTextY+fNewLine, i, FALSE, -1, iAlpha) 					fTextX += DM_GET_INFO_TEXT_SIZE(sTempLabel, FALSE)
					
					// OVERDUE
					i = (sBugstarData.iOverdueBugs)
					sTempLabel = " " sTempLabel += i
					DM_DISPLAY_INFO_TEXT(fTextX, fTextY+fNewLine, -1, "DM_BUG_OV", FALSE, 255, 255, 255, iAlpha) 		fTextX += DM_GET_INFO_TEXT_SIZE("DM_BUG_OV", FALSE)
					DM_DISPLAY_INFO_NUMBER(fTextX, fTextY+fNewLine, i, FALSE, 1, iAlpha) 				fTextX += DM_GET_INFO_TEXT_SIZE(sTempLabel, FALSE)
					
					// FIXED IN PAST 2 WEEKS
					fTextX = 0.510
					i = (sBugstarData.iFixedBugs)
					sTempLabel = " " sTempLabel += i
					DM_DISPLAY_INFO_TEXT(fTextX, fTextY+fNewLine+fNewLine, -1, "DM_BUG_FIX", FALSE, 255, 255, 255, iAlpha) 	fTextX += DM_GET_INFO_TEXT_SIZE("DM_BUG_FIX", FALSE)
					DM_DISPLAY_INFO_NUMBER(fTextX, fTextY+fNewLine+fNewLine, i, FALSE, -1, iAlpha) 					fTextX += DM_GET_INFO_TEXT_SIZE(sTempLabel, FALSE)
					
					// AVERAGE MISSION BUG AGE
					i = (sBugstarData.iAvgBugAge)
					sTempLabel = " " sTempLabel += i
					DM_DISPLAY_INFO_TEXT(fTextX, fTextY+fNewLine+fNewLine, -1, "DM_BUG_AVG", FALSE, 255, 255, 255, iAlpha) 	fTextX += DM_GET_INFO_TEXT_SIZE("DM_BUG_AVG", FALSE)
					DM_DISPLAY_INFO_NUMBER(fTextX, fTextY+fNewLine+fNewLine, i, FALSE, -1, iAlpha) 					fTextX += DM_GET_INFO_TEXT_SIZE(sTempLabel, FALSE)
					
					// Top 5 Bug Owners
					fTextX = 0.510
					fTextY = 0.690
					DM_DISPLAY_INFO_TITLE(fTextX, fTextY, "DM_BUG_BUGS5", iAlpha)
					
					j = 0 // use this to keep track of owner
					sLargestBigLabel = "" // use this to keep track of largest string
					FOR i = 0 TO 4
					
						IF sBugstarData.iTop5Bugs[i] > 0
						
							sTempLabel = sItem.sMissionID
							sTempLabel += "T5OWN"
							sTempLabel += i
							
							IF DOES_TEXT_LABEL_EXIST(sTempLabel)
							
								DM_DISPLAY_INFO_TEXT(fTextX, fTextY+fNewLine+(j*0.02), -1, sTempLabel, FALSE, 255, 255, 255, iAlpha)
								
								bugs[j] = sBugstarData.iTop5Bugs[i] // bugs for this item
								age[j] = sBugstarData.iTop5BugsAvgAge[i] // average age of owners bugs
								
								j++ // owner count
								IF DM_GET_INFO_TEXT_SIZE(sTempLabel, FALSE) > DM_GET_INFO_TEXT_SIZE(sLargestBigLabel, FALSE)
									sLargestBigLabel = sTempLabel
								ENDIF
								
								// Once we have added 3 to the list, reset row and update column
								IF j > 2
								
									fTextX += DM_GET_INFO_TEXT_SIZE(sLargestBigLabel, FALSE)+0.045
									
									FOR k = 0 TO j-1
										// Output the bug numbers and age
										sTempLabel = ""	sTempLabel += bugs[k]	sTempLabel += " ("	sTempLabel += age[k]	sTempLabel += ")"
										DM_DISPLAY_INFO_TEXT(fTextX-0.04, fTextY+fNewLine+(k*0.02), -1, sTempLabel, FALSE, 255, 255, 255, iAlpha)
									ENDFOR
									
									j = 0
									sLargestBigLabel=""
								ENDIF
							ENDIF
						ENDIF
					ENDFOR
					
					// Output bugs if we have added less than 5 to the list
					IF j <> 0
						fTextX += DM_GET_INFO_TEXT_SIZE(sLargestBigLabel, FALSE)+0.045
						FOR k = 0 TO j-1
							// Output the bug numbers and age
							sTempLabel = ""	sTempLabel += bugs[k]	sTempLabel += " ("	sTempLabel += age[k]	sTempLabel += ")"
							DM_DISPLAY_INFO_TEXT(fTextX-0.04, fTextY+fNewLine+(k*0.02), -1, sTempLabel, FALSE, 255, 255, 255, iAlpha)
						ENDFOR
					ENDIF
					
					// Bugs Per Team
					fTextX = 0.510
					fTextY = 0.740+0.042//0.740
					
					DM_DISPLAY_INFO_TITLE(fTextX, fTextY, "DM_BUG_BUGSD", iAlpha)
					
					j = 0 // use this to keep track of dept
					sLargestLabel = "" // use this to keep track of largest string
					FOR i = 0 TO DM_MAX_BUGSTAR_DEPTS-1
						IF sBugstarData.iDeptBugs[i] <> 0
						
							sTempLabel = sItem.sMissionID
							sTempLabel += "DPTOWN"
							sTempLabel += i
							
							IF DOES_TEXT_LABEL_EXIST(sTempLabel)
							
								DM_DISPLAY_INFO_TEXT(fTextX, fTextY+fNewLine+(j*0.02), -1, sTempLabel, FALSE, 255, 255, 255, iAlpha)
								
								bugs[j] = sBugstarData.iDeptBugs[i] // bugs for this item
								j++ // department count
								IF DM_GET_INFO_TEXT_SIZE(sTempLabel, FALSE) > DM_GET_INFO_TEXT_SIZE(sLargestLabel, FALSE)
									sLargestLabel = sTempLabel
								ENDIF
								
								// Once we have added 3 to the list, reset row and update column
								IF j > 2
								
									fTextX += DM_GET_INFO_TEXT_SIZE(sLargestLabel, FALSE)+0.025
									
									FOR k = 0 TO j-1
										// Output the bug numbers
										DM_DISPLAY_INFO_NUMBER(fTextX-0.02, fTextY+fNewLine+(k*0.02), bugs[k], FALSE, -1, iAlpha)
									ENDFOR
									
									j = 0
									sLargestLabel=""
								ENDIF
							ENDIF
						ENDIF
					ENDFOR
						
					// Output bugs if we have added less than 5 to the list
					IF j <> 0
						fTextX += DM_GET_INFO_TEXT_SIZE(sLargestLabel, FALSE)+0.025
						FOR k = 0 TO j-1
							// Output the bug numbers
							DM_DISPLAY_INFO_NUMBER(fTextX-0.02, fTextY+fNewLine+(k*0.02), bugs[k], FALSE, -1, iAlpha)
						ENDFOR
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF NOT ARE_STRINGS_EQUAL(sItem.sInfo, "")
				fTextX = 0.060
				fTextY = 0.620
			
				DM_DISPLAY_INFO_TITLE(fTextX, fTextY, "DM_BUG_INFO", iAlpha)
				DM_DISPLAY_INFO_TEXT(fTextX, fTextY+fNewLine, fTextX+0.425, sItem.sInfo, FALSE, 255, 255, 255, iAlpha)
				
				IF DOES_TEXT_LABEL_EXIST(sItem.sExtraInfo)
					fTextY += 0.0645
					DM_DISPLAY_INFO_TEXT(fTextX, fTextY+fNewLine, fTextX+0.425, sItem.sExtraInfo, FALSE, 255, 255, 255, iAlpha)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Decides which items to display and calls the appropriate procs
PROC DISPLAY_DEBUG_MENU(DEBUG_MENU_DATA_STRUCT &sDebugMenuData)

	INT iItem, iDepth
	
	//INT iFirstItem[DM_MAX_DEPTH] // The first item that we can display in the overal list of items
	INT iFirstItemInColumn[DM_MAX_DEPTH] // The first item in the list, relative to how many items we can actually display
	INT iLastItemInColumn[DM_MAX_DEPTH] // The last item in the list, relative to how many items we can actually display
	INT iItemsToDisplay[DM_MAX_DEPTH] // How many items we have to display
	INT iItemsInColumn[DM_MAX_DEPTH]  // How many items we can display
	//INT iSelectedItem[DM_MAX_DEPTH]  // The items that we have selected in the overal list of items
	INT iSelectedItemInColumn[DM_MAX_DEPTH]  // The items that we have selected, relative to how many items we can actually display
	BOOL bShortText, bSelected
	BOOL bFirstItemInColumnFound[DM_MAX_DEPTH]
	BOOL bFirstItemFound[DM_MAX_DEPTH]
	BOOL bDisplayTopCut[DM_MAX_DEPTH]
	BOOL bDisplayUnderCut[DM_MAX_DEPTH]
	BOOL bDisplayScroll[DM_MAX_DEPTH]
	
	TEXT_LABEL_63 sTemp
	FLOAT fXOffset, fYOffset
	FLOAT fYStep,fXStep
	FLOAT fTextX[DM_MAX_DEPTH]
	FLOAT fTextY[DM_MAX_DEPTH]
	FLOAT fMarginX[DM_MAX_DEPTH]
	
	fTextX[0] = 0.060	fTextY[0] = 0.120	// Column 1 x/y
	fTextX[1] = 0.236	fTextY[1] = 0.120	// Column 2 x/y
	fTextX[2] = 0.405	fTextY[2] = 0.120	// Column 3 x/y
	fMarginX[0] = 0.205
	fMarginX[1] = 0.380
	fMarginX[2] = 0.555
	
	// UI - Background
	DRAW_RECT(0.5000, 0.4900, 0.9250, 0.9000, 10, 10, 10, 200)
	
	// UI - Title bar
	DM_Display_Menu_Title(0.5000, 0.0500, sDebugMenuData.sMenuTitle)
	DRAW_RECT(0.5000, 0.0700, 0.9110, 0.0500, MENU_BG_EDGE_R, MENU_BG_EDGE_G, MENU_BG_EDGE_B, 200)
	DRAW_RECT(0.5000, 0.0700, 0.9060, 0.0430, MENU_BG_R, MENU_BG_G, MENU_BG_B, 255)
	
	// UI - Info section
	DRAW_RECT(0.5000, 0.7460, 0.9110, 0.2700, MENU_BG_EDGE_R, MENU_BG_EDGE_G, MENU_BG_EDGE_B, 200)
	DRAW_RECT(0.5000, 0.7460, 0.9060, 0.2630, MENU_BG_R, MENU_BG_G, MENU_BG_B, 255)
	
	// UI - Display the menu help
	DRAW_RECT(0.5000, 0.9100, 0.9110, 0.0500, MENU_BG_EDGE_R, MENU_BG_EDGE_G, MENU_BG_EDGE_B, 200)
	DRAW_RECT(0.5000, 0.9100, 0.9060, 0.0430, MENU_BG_R, MENU_BG_G, MENU_BG_B, 255)
	
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		DM_DISPLAY_MENU_HELP_LABEL(0.058, 0.895, "DM_NAV")
		DM_DISPLAY_MENU_HELP_LABEL(0.223, 0.895, "DM_SELECT")
		DM_DISPLAY_MENU_HELP_LABEL(0.418, 0.895, "DM_EXIT")
		
		IF NOT sDebugMenuData.sBugData.bDisplay
			DM_DISPLAY_MENU_HELP_LABEL(0.545, 0.895, "DM_INFO_0")
		ELSE
			DM_DISPLAY_MENU_HELP_LABEL(0.545, 0.895, "DM_INFO_1")
		ENDIF
		
		sTemp = "DM_SORT_"
		sTemp += ENUM_TO_INT(sDebugMenuData.eMissionSort)
		DM_DISPLAY_MENU_HELP_LABEL(0.685, 0.895, sTemp)
	ELSE
		DM_DISPLAY_MENU_HELP_LABEL(0.095, 0.895, "DM_NAV")
		DM_DISPLAY_MENU_HELP_LABEL(0.291, 0.895, "DM_SELECT")
		DM_DISPLAY_MENU_HELP_LABEL(0.487, 0.895, "DM_EXIT")
	ENDIF
	
	BOOL bUpdatingItem = IS_DEBUG_MENU_PROCESS_FLAG_SET(sDebugMenuData.iProcessFlags, DM_PROCESS_UPDATE_MENU_ITEM)
	BOOL bDisplayInfo = TRUE
	BOOL bDisplayItems, bDisplayLoading
	
	
	// Go through all the item levels
	FOR iDepth = 0 TO DM_MAX_DEPTH-1
	
		bDisplayItems = TRUE
		bDisplayLoading = FALSE
		
		IF IS_DEBUG_MENU_PROCESS_FLAG_SET(sDebugMenuData.iProcessFlags, DM_PROCESS_RESTORE_SELECTION)
		OR IS_DEBUG_MENU_PROCESS_FLAG_SET(sDebugMenuData.iProcessFlags, DM_PROCESS_RESTORE_SORT)
			IF (iDepth = 0)
				bDisplayLoading = TRUE
			ENDIF
			bDisplayItems = FALSE
		ELIF (bUpdatingItem OR IS_DEBUG_MENU_PROCESS_FLAG_SET(sDebugMenuData.iProcessFlags, DM_PROCESS_WAIT)) AND (iDepth = sDebugMenuData.iCurrentDepth+1)
			IF (sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].iType = DM_HASH_ITEM_MENU)
				bDisplayLoading = TRUE
			ENDIF
		ELIF IS_DEBUG_MENU_PROCESS_FLAG_SET(sDebugMenuData.iProcessFlags, DM_PROCESS_BUGSTAR_GET_BASIC)
			IF (iDepth = sDebugMenuData.iCurrentDepth+1)
				bDisplayLoading = TRUE
			ENDIF
			IF (iDepth = sDebugMenuData.iCurrentDepth)
				IF (sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].iType != DM_HASH_ITEM_MENU)
					bDisplayLoading = TRUE
				ENDIF
			ENDIF
		ENDIF
		
		IF bDisplayLoading
			DM_DISPLAY_MENU_ITEM(TRUE, FALSE, fTextX[iDepth], fTextY[iDepth], 1.0, "LOADING...", FALSE)
			bDisplayInfo = FALSE
		ELIF bDisplayItems
			// Reset the selected items
			//iSelectedItem[iDepth] = -1
			iSelectedItemInColumn[iDepth] = -1
		
			// Check if the text should be shortened
			IF (sDebugMenuData.iCurrentDepth > iDepth AND iDepth < DM_MAX_DEPTH-1)
			OR (sDebugMenuData.iCurrentDepth = iDepth AND sDebugMenuData.sItems[iDepth][sDebugMenuData.iCurrentItem[iDepth]].iType = DM_HASH_ITEM_MENU AND iDepth < DM_MAX_DEPTH-1)
				bShortText = TRUE
			ELSE
				bShortText = FALSE
			ENDIF
			
			// UI - Content panes
			FLOAT fBorderX, fBorderW
			SWITCH iDepth
				CASE 0
					IF bShortText
						fBorderX =  0.1310	fBorderW = 0.1730
						DRAW_RECT(0.1310, 0.3540, 0.1730, 0.5120, MENU_BG_EDGE_R, MENU_BG_EDGE_G, MENU_BG_EDGE_B, 200)
						DRAW_RECT(0.1310, 0.3540, 0.1680, 0.5050, MENU_BG_R, MENU_BG_G, MENU_BG_B, 255)
					ELSE
						fBorderX =  0.5000 	fBorderW = 0.9110
						DRAW_RECT(0.5000, 0.3540, 0.9110, 0.5000, MENU_BG_EDGE_R, MENU_BG_EDGE_G, MENU_BG_EDGE_B, 200)
						DRAW_RECT(0.5000, 0.3540, 0.9060, 0.5050, MENU_BG_R, MENU_BG_G, MENU_BG_B, 255)
					ENDIF
				BREAK
				
				CASE 1
					IF bShortText
						fBorderX =  0.3070	fBorderW = 0.1730
						DRAW_RECT(0.3070, 0.3540, 0.1730, 0.5120, MENU_BG_EDGE_R, MENU_BG_EDGE_G, MENU_BG_EDGE_B, 200)
						DRAW_RECT(0.3070, 0.3540, 0.1680, 0.5050, MENU_BG_R, MENU_BG_G, MENU_BG_B, 255)
					ELSE
						IF sDebugMenuData.iCurrentDepth >= iDepth
							fBorderX =  0.5880	fBorderW = 0.7350
							DRAW_RECT(0.5880, 0.3540, 0.7350, 0.5120, MENU_BG_EDGE_R, MENU_BG_EDGE_G, MENU_BG_EDGE_B, 200)
							DRAW_RECT(0.5880, 0.3540, 0.7300, 0.5050, MENU_BG_R, MENU_BG_G, MENU_BG_B, 255)
						ENDIF
					ENDIF
				BREAK
				
				CASE 2
					IF bShortText
						fBorderX =  0.4830	fBorderW = 0.1730
						DRAW_RECT(0.4830, 0.3540, 0.1730, 0.5120, MENU_BG_EDGE_R, MENU_BG_EDGE_G, MENU_BG_EDGE_B, 200)
						DRAW_RECT(0.4830, 0.3540, 0.1680, 0.5050, MENU_BG_R, MENU_BG_G, MENU_BG_B, 255)
					ELSE
						IF sDebugMenuData.iCurrentDepth >= iDepth
							fBorderX =  0.6762	fBorderW = 0.5600
							DRAW_RECT(0.6762, 0.3540, 0.5600, 0.5120, MENU_BG_EDGE_R, MENU_BG_EDGE_G, MENU_BG_EDGE_B, 200)
							DRAW_RECT(0.6762, 0.3540, 0.5550, 0.5050, MENU_BG_R, MENU_BG_G, MENU_BG_B, 255)
						ENDIF
					ENDIF	
				BREAK
			ENDSWITCH

			
			// Reset the x/y offsets
			fXOffset = 0.0
			fYOffset = 0.0
			
			// Display items that are <= current depth
			// Display items on next depth if current item is a menu
			IF bShortText
			OR iDepth <= sDebugMenuData.iCurrentDepth
			OR (iDepth = sDebugMenuData.iCurrentDepth+1 AND sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].iType = DM_HASH_ITEM_MENU)
			
				// Go through every item in the list
				FOR iItem = 0 TO sDebugMenuData.iItemCount[iDepth]-1
					// If the popup window is drawing, we don't want to draw certain rows, since text will overlap
					IF NOT sDebugMenuData.bDrawAltWindow OR iItem < 8 OR iDepth = 0
						// Check the display flag
						IF IS_DEBUG_MENU_ITEM_FLAG_SET(sDebugMenuData.sItems[iDepth][iItem].iFlags, DM_BIT_DISPLAY)
						
							// Set the first item in the list that we can potentially display
							IF NOT bFirstItemFound[iDepth]
								//iFirstItem[iDepth] = iItemsToDisplay[iDepth]
								bFirstItemFound[iDepth] = TRUE
							ENDIF
							
							IF sDebugMenuData.iTopItem[iDepth] <= iItem
										
								// Check that we are allowed to add more to the list
								IF iItemsInColumn[iDepth] < DM_MAX_ITEMS_IN_COLUMN
									
									// Set the first item in the list that is on display
									IF NOT bFirstItemInColumnFound[iDepth]
										iFirstItemInColumn[iDepth] = iItemsToDisplay[iDepth]
										bFirstItemInColumnFound[iDepth] = TRUE
									ENDIF
									
									// Set the last item in the list that is on display
									iLastItemInColumn[iDepth] = iItemsToDisplay[iDepth]
									
									// Flag if this item is selected
									IF sDebugMenuData.iCurrentItem[iDepth] = iItem
									AND iDepth <= sDebugMenuData.iCurrentDepth
										bSelected = TRUE
										//iSelectedItem[iDepth] = iItemsToDisplay[iDepth]
										iSelectedItemInColumn[iDepth] = iItemsInColumn[iDepth]
									ELSE
										bSelected = FALSE
									ENDIF
									
									fYStep 	  = DM_GET_TEXT_SCALE_Y(bSelected) * DM_GET_SCREEN_SCALE_Y() * DM_GET_TEXT_SPACING_Y(bSelected)
									fXStep 	  = DM_GET_TEXT_SCALE_X() * DM_GET_SCREEN_SCALE_X() * DM_GET_TEXT_SPACING_X()
									
									BOOL bEnabled = IS_DEBUG_MENU_ITEM_FLAG_SET(sDebugMenuData.sItems[iDepth][iItem].iFlags, DM_BIT_ENABLED)
									
									// Display the item in the appropriate manner based on type
									SWITCH sDebugMenuData.sItems[iDepth][iItem].iType
									
										CASE DM_HASH_ITEM_MISSION
										CASE DM_HASH_ITEM_BUGSTAR
										CASE DM_HASH_ITEM_LAUNCHER
											// Mission ID
											IF ARE_STRINGS_EQUAL(sDebugMenuData.sItems[iDepth][iItem].sMissionID, "")
												DM_DISPLAY_MENU_ITEM_LABEL(bEnabled, bSelected, fTextX[iDepth]+fXOffset, fTextY[iDepth]+fYOffset, "DM_MISSING", fMarginX[iDepth], bShortText)
											ELSE
												DM_DISPLAY_MENU_ITEM(bEnabled, bSelected, fTextX[iDepth]+fXOffset, fTextY[iDepth]+fYOffset, fMarginX[iDepth], sDebugMenuData.sItems[iDepth][iItem].sMissionID, bShortText)
											ENDIF
											fXOffset += fXStep * 9
											
											// Mission title
											//IF IS_DEBUG_MENU_ITEM_FLAG_SET(sDebugMenuData.sItems[iDepth][iItem].iFlags, DM_BIT_HIGHLIGHT)
											IF sDebugMenuData.sItems[iDepth][iItem].iPerc < 70
											AND sDebugMenuData.sItems[iDepth][iItem].iPerc >= 0
											
												IF ARE_STRINGS_EQUAL(sDebugMenuData.sItems[iDepth][iItem].sTitleExtra, "")
													IF NOT DOES_TEXT_LABEL_EXIST(sDebugMenuData.sItems[iDepth][iItem].sMissionID)
														DM_DISPLAY_MENU_ITEM(bEnabled, bSelected, fTextX[iDepth]+fXOffset, fTextY[iDepth]+fYOffset, fMarginX[iDepth], sDebugMenuData.sItems[iDepth][iItem].sTitle, bShortText, 189, 39,  39,  255)
													ELSE
														DM_DISPLAY_MENU_ITEM_LABEL_HIGHLIGHT(bEnabled, bSelected, fTextX[iDepth]+fXOffset, fTextY[iDepth]+fYOffset, sDebugMenuData.sItems[iDepth][iItem].sMissionID, "DM_COL_R", fMarginX[iDepth], bShortText)
													ENDIF
												ELSE
													IF NOT DOES_TEXT_LABEL_EXIST(sDebugMenuData.sItems[iDepth][iItem].sMissionID)
														sTemp = sDebugMenuData.sItems[iDepth][iItem].sTitle
													ELSE
														sTemp = GET_STRING_FROM_TEXT_FILE(sDebugMenuData.sItems[iDepth][iItem].sMissionID)
													ENDIF
													sTemp += "("	sTemp += sDebugMenuData.sItems[iDepth][iItem].sTitleExtra sTemp += ")"
													DM_DISPLAY_MENU_ITEM(bEnabled, bSelected, fTextX[iDepth]+fXOffset, fTextY[iDepth]+fYOffset, fMarginX[iDepth], sTemp, bShortText, 189, 39,  39,  255)
												ENDIF
											ELSE
												IF ARE_STRINGS_EQUAL(sDebugMenuData.sItems[iDepth][iItem].sTitleExtra, "")
													IF NOT DOES_TEXT_LABEL_EXIST(sDebugMenuData.sItems[iDepth][iItem].sMissionID)
														DM_DISPLAY_MENU_ITEM(bEnabled, bSelected, fTextX[iDepth]+fXOffset, fTextY[iDepth]+fYOffset, fMarginX[iDepth], sDebugMenuData.sItems[iDepth][iItem].sTitle, bShortText)
													ELSE
														DM_DISPLAY_MENU_ITEM_LABEL(bEnabled, bSelected, fTextX[iDepth]+fXOffset, fTextY[iDepth]+fYOffset, sDebugMenuData.sItems[iDepth][iItem].sMissionID, fMarginX[iDepth], bShortText)
													ENDIF
												ELSE
													IF NOT DOES_TEXT_LABEL_EXIST(sDebugMenuData.sItems[iDepth][iItem].sMissionID)
														sTemp = sDebugMenuData.sItems[iDepth][iItem].sTitle
													ELSE
														sTemp = GET_STRING_FROM_TEXT_FILE(sDebugMenuData.sItems[iDepth][iItem].sMissionID)
													ENDIF
													sTemp += "("	sTemp += sDebugMenuData.sItems[iDepth][iItem].sTitleExtra	sTemp += ")"
													DM_DISPLAY_MENU_ITEM(bEnabled, bSelected, fTextX[iDepth]+fXOffset, fTextY[iDepth]+fYOffset, fMarginX[iDepth], sTemp, bShortText)
												ENDIF
											ENDIF
											fXOffset += fXStep * 36
					
											// Mission script
											DM_DISPLAY_MENU_ITEM(bEnabled, bSelected, fTextX[iDepth]+fXOffset, fTextY[iDepth]+fYOffset, fMarginX[iDepth], sDebugMenuData.sItems[iDepth][iItem].sFile, bShortText)
											fXOffset += fXStep * 18
											
											// Mission author
											DM_DISPLAY_MENU_ITEM_LABEL(bEnabled, bSelected, fTextX[iDepth]+fXOffset, fTextY[iDepth]+fYOffset, sDebugMenuData.sItems[iDepth][iItem].sAuth, fMarginX[iDepth], bShortText)
											fXOffset += fXStep * 6
											
											// Mission percentage
											DM_DISPLAY_MENU_ITEM_PERC(bEnabled, bSelected, fTextX[iDepth]+fXOffset, fTextY[iDepth]+fYOffset, sDebugMenuData.sItems[iDepth][iItem].iPerc, fMarginX[iDepth], bShortText)
											fXOffset += fXStep * 5
											
											// LB bugs
											DM_DISPLAY_MENU_ITEM_BUG_COUNT(bEnabled, bSelected, fTextX[iDepth]+fXOffset, fTextY[iDepth]+fYOffset, fMarginX[iDepth], sDebugMenuData.sItems[iDepth][iItem].iImportantBugs, bShortText)
										BREAK
										
										CASE DM_HASH_ITEM_BREAK
											DM_DISPLAY_MENU_ITEM_LABEL(bEnabled, bSelected, fTextX[iDepth]+fXOffset, fTextY[iDepth]+fYOffset, "DM_BREAK")
										BREAK
										
										CASE DM_HASH_ITEM_INPUT
											IF ARE_STRINGS_EQUAL(sDebugMenuData.sItems[iDepth][iItem].sTitleExtra, "")
												DM_DISPLAY_MENU_ITEM_LABEL(bEnabled, bSelected, fTextX[iDepth]+fXOffset, fTextY[iDepth]+fYOffset, sDebugMenuData.sItems[iDepth][iItem].sTitle)
											ELSE
												sTemp = sDebugMenuData.sItems[iDepth][iItem].sTitle
												sTemp += "("	sTemp += sDebugMenuData.sItems[iDepth][iItem].sTitleExtra	sTemp += ")"
												DM_DISPLAY_MENU_ITEM_LABEL(bEnabled, bSelected, fTextX[iDepth]+fXOffset, fTextY[iDepth]+fYOffset, sTemp)
											ENDIF
											fXOffset += fXStep * 10
											
											sTemp = sDebugMenuData.sItems[iDepth][iItem].sTitle
											sTemp +="_I"
											IF DOES_TEXT_LABEL_EXIST(sTemp)
												DM_DISPLAY_MENU_ITEM_LABEL(bEnabled, bSelected, fTextX[iDepth]+fXOffset, fTextY[iDepth]+fYOffset, sTemp)
											ENDIF
										BREAK
										
										CASE DM_HASH_ITEM_WARP
										
											IF IS_DEBUG_MENU_ITEM_FLAG_SET(sDebugMenuData.sItems[iDepth][iItem].iFlags, DM_BIT_BUG_INFO)
												// Mission ID
												IF ARE_STRINGS_EQUAL(sDebugMenuData.sItems[iDepth][iItem].sMissionID, "")
													DM_DISPLAY_MENU_ITEM_LABEL(bEnabled, bSelected, fTextX[iDepth]+fXOffset, fTextY[iDepth]+fYOffset, "DM_MISSING", fMarginX[iDepth], bShortText)
												ELSE
													DM_DISPLAY_MENU_ITEM(bEnabled, bSelected, fTextX[iDepth]+fXOffset, fTextY[iDepth]+fYOffset, fMarginX[iDepth], sDebugMenuData.sItems[iDepth][iItem].sMissionID, bShortText)
												ENDIF
												fXOffset += fXStep * 9
												
												// Mission title
												IF DOES_TEXT_LABEL_EXIST(sDebugMenuData.sItems[iDepth][iItem].sMissionID)
													sTemp = GET_STRING_FROM_TEXT_FILE(sDebugMenuData.sItems[iDepth][iItem].sMissionID)
												ELSE
													sTemp = sDebugMenuData.sItems[iDepth][iItem].sTitle
												ENDIF
												
												IF NOT ARE_STRINGS_EQUAL(sDebugMenuData.sItems[iDepth][iItem].sTitleExtra, "")
													sTemp += "("	sTemp += sDebugMenuData.sItems[iDepth][iItem].sTitleExtra	sTemp += ")"
												ENDIF
												
												IF GET_LENGTH_OF_LITERAL_STRING(sTemp) > 6
													IF NOT ARE_STRINGS_EQUAL(GET_STRING_FROM_STRING(sTemp, GET_LENGTH_OF_LITERAL_STRING(sTemp)-6, GET_LENGTH_OF_LITERAL_STRING(sTemp)), "(warp)")
														sTemp += " (warp)"
													ENDIF
												ELSE
													sTemp += " (warp)"
												ENDIF
												
												//IF IS_DEBUG_MENU_ITEM_FLAG_SET(sDebugMenuData.sItems[iDepth][iItem].iFlags, DM_BIT_HIGHLIGHT)
												IF sDebugMenuData.sItems[iDepth][iItem].iPerc < 70
												AND sDebugMenuData.sItems[iDepth][iItem].iPerc >= 0
													DM_DISPLAY_MENU_ITEM(bEnabled, bSelected, fTextX[iDepth]+fXOffset, fTextY[iDepth]+fYOffset, fMarginX[iDepth], sTemp, bShortText, 189, 39,  39,  255)
												ELSE
													DM_DISPLAY_MENU_ITEM(bEnabled, bSelected, fTextX[iDepth]+fXOffset, fTextY[iDepth]+fYOffset, fMarginX[iDepth], sTemp, bShortText)
												ENDIF
												
												fXOffset += fXStep * 36
						
											ELSE
												// Title
												IF DOES_TEXT_LABEL_EXIST(sDebugMenuData.sItems[iDepth][iItem].sTitle)
													sTemp = GET_STRING_FROM_TEXT_FILE(sDebugMenuData.sItems[iDepth][iItem].sTitle)
												ELSE
													sTemp = sDebugMenuData.sItems[iDepth][iItem].sTitle
												ENDIF
												
												IF NOT ARE_STRINGS_EQUAL(sDebugMenuData.sItems[iDepth][iItem].sTitleExtra, "")
													sTemp += "("	sTemp += sDebugMenuData.sItems[iDepth][iItem].sTitleExtra	sTemp += ")"
												ENDIF
												
												IF GET_LENGTH_OF_LITERAL_STRING(sTemp) > 6
													IF NOT ARE_STRINGS_EQUAL(GET_STRING_FROM_STRING(sTemp, GET_LENGTH_OF_LITERAL_STRING(sTemp)-6, GET_LENGTH_OF_LITERAL_STRING(sTemp)), "(warp)")
														sTemp += " (warp)"
													ENDIF
												ELSE
													sTemp += " (warp)"
												ENDIF
												
												//IF IS_DEBUG_MENU_ITEM_FLAG_SET(sDebugMenuData.sItems[iDepth][iItem].iFlags, DM_BIT_HIGHLIGHT)
												IF sDebugMenuData.sItems[iDepth][iItem].iPerc < 70
												AND sDebugMenuData.sItems[iDepth][iItem].iPerc >= 0
													DM_DISPLAY_MENU_ITEM(bEnabled, bSelected, fTextX[iDepth]+fXOffset, fTextY[iDepth]+fYOffset, fMarginX[iDepth], sTemp, bShortText, 189, 39,  39,  255)
												ELSE
													DM_DISPLAY_MENU_ITEM(bEnabled, bSelected, fTextX[iDepth]+fXOffset, fTextY[iDepth]+fYOffset, fMarginX[iDepth], sTemp, bShortText)
												ENDIF
												fXOffset += fXStep * (9+36)
											ENDIF
											
											// Script
											DM_DISPLAY_MENU_ITEM(bEnabled, bSelected, fTextX[iDepth]+fXOffset, fTextY[iDepth]+fYOffset, fMarginX[iDepth], sDebugMenuData.sItems[iDepth][iItem].sFile, bShortText)
											fXOffset += fXStep * 18
											
											// Author
											DM_DISPLAY_MENU_ITEM_LABEL(bEnabled, bSelected, fTextX[iDepth]+fXOffset, fTextY[iDepth]+fYOffset, sDebugMenuData.sItems[iDepth][iItem].sAuth, fMarginX[iDepth], bShortText)
											fXOffset += fXStep * 6 // sAuthor can have 6 characters, +1 for spacing
											
											// Percentage
											IF sDebugMenuData.sItems[iDepth][iItem].iPerc <> -1
												DM_DISPLAY_MENU_ITEM_PERC(bEnabled, bSelected, fTextX[iDepth]+fXOffset, fTextY[iDepth]+fYOffset, sDebugMenuData.sItems[iDepth][iItem].iPerc, fMarginX[iDepth], bShortText)
												fXOffset += fXStep * 5
											
												// LB bugs
												DM_DISPLAY_MENU_ITEM_BUG_COUNT(bEnabled, bSelected, fTextX[iDepth]+fXOffset, fTextY[iDepth]+fYOffset, fMarginX[iDepth], sDebugMenuData.sItems[iDepth][iItem].iImportantBugs, bShortText)
											ENDIF
										BREAK
										
										CASE DM_HASH_ITEM_SCRIPT
										CASE DM_HASH_ITEM_TOOL
											IF IS_DEBUG_MENU_ITEM_FLAG_SET(sDebugMenuData.sItems[iDepth][iItem].iFlags, DM_BIT_BUG_INFO)
												// Mission ID
												IF ARE_STRINGS_EQUAL(sDebugMenuData.sItems[iDepth][iItem].sMissionID, "")
													DM_DISPLAY_MENU_ITEM_LABEL(bEnabled, bSelected, fTextX[iDepth]+fXOffset, fTextY[iDepth]+fYOffset, "DM_MISSING", fMarginX[iDepth], bShortText)
												ELSE
													DM_DISPLAY_MENU_ITEM(bEnabled, bSelected, fTextX[iDepth]+fXOffset, fTextY[iDepth]+fYOffset, fMarginX[iDepth], sDebugMenuData.sItems[iDepth][iItem].sMissionID, bShortText)
												ENDIF
												fXOffset += fXStep * 9
												
												// Mission title
												IF DOES_TEXT_LABEL_EXIST(sDebugMenuData.sItems[iDepth][iItem].sMissionID)
													sTemp = GET_STRING_FROM_TEXT_FILE(sDebugMenuData.sItems[iDepth][iItem].sMissionID)
												ELSE
													sTemp = sDebugMenuData.sItems[iDepth][iItem].sTitle
												ENDIF
												
												IF NOT ARE_STRINGS_EQUAL(sDebugMenuData.sItems[iDepth][iItem].sTitleExtra, "")
													sTemp += "("	sTemp += sDebugMenuData.sItems[iDepth][iItem].sTitleExtra	sTemp += ")"
												ENDIF
												
												IF GET_LENGTH_OF_LITERAL_STRING(sTemp) > 8
													IF NOT ARE_STRINGS_EQUAL(GET_STRING_FROM_STRING(sTemp, GET_LENGTH_OF_LITERAL_STRING(sTemp)-8, GET_LENGTH_OF_LITERAL_STRING(sTemp)), "(script)")
														sTemp += " (script)"
													ENDIF
												ELSE
													sTemp += " (script)"
												ENDIF
												
												//IF IS_DEBUG_MENU_ITEM_FLAG_SET(sDebugMenuData.sItems[iDepth][iItem].iFlags, DM_BIT_HIGHLIGHT)
												IF sDebugMenuData.sItems[iDepth][iItem].iPerc < 70
												AND sDebugMenuData.sItems[iDepth][iItem].iPerc >= 0
													DM_DISPLAY_MENU_ITEM(bEnabled, bSelected, fTextX[iDepth]+fXOffset, fTextY[iDepth]+fYOffset, fMarginX[iDepth], sTemp, bShortText, 189, 39,  39,  255)
												ELSE
													DM_DISPLAY_MENU_ITEM(bEnabled, bSelected, fTextX[iDepth]+fXOffset, fTextY[iDepth]+fYOffset, fMarginX[iDepth], sTemp, bShortText)
												ENDIF
												
												fXOffset += fXStep * 36
												
												// Script
												DM_DISPLAY_MENU_ITEM(bEnabled, bSelected, fTextX[iDepth]+fXOffset, fTextY[iDepth]+fYOffset, fMarginX[iDepth], sDebugMenuData.sItems[iDepth][iItem].sFile, bShortText)
												fXOffset += fXStep * 18
												
												// Author
												DM_DISPLAY_MENU_ITEM_LABEL(bEnabled, bSelected, fTextX[iDepth]+fXOffset, fTextY[iDepth]+fYOffset, sDebugMenuData.sItems[iDepth][iItem].sAuth, fMarginX[iDepth], bShortText)
												fXOffset += fXStep * 6 // sAuthor can have 6 characters, +1 for spacing
												
												// Percentage
												IF sDebugMenuData.sItems[iDepth][iItem].iPerc <> -1
													DM_DISPLAY_MENU_ITEM_PERC(bEnabled, bSelected, fTextX[iDepth]+fXOffset, fTextY[iDepth]+fYOffset, sDebugMenuData.sItems[iDepth][iItem].iPerc, fMarginX[iDepth], bShortText)
													fXOffset += fXStep * 5
												
													// LB bugs
													DM_DISPLAY_MENU_ITEM_BUG_COUNT(bEnabled, bSelected, fTextX[iDepth]+fXOffset, fTextY[iDepth]+fYOffset, fMarginX[iDepth], sDebugMenuData.sItems[iDepth][iItem].iImportantBugs, bShortText)
												ENDIF
											ELSE
												// Title
												IF DOES_TEXT_LABEL_EXIST(sDebugMenuData.sItems[iDepth][iItem].sTitle)
													sTemp = GET_STRING_FROM_TEXT_FILE(sDebugMenuData.sItems[iDepth][iItem].sTitle)
												ELSE
													sTemp = sDebugMenuData.sItems[iDepth][iItem].sTitle
												ENDIF
												
												IF NOT ARE_STRINGS_EQUAL(sDebugMenuData.sItems[iDepth][iItem].sTitleExtra, "")
													sTemp += "("	sTemp += sDebugMenuData.sItems[iDepth][iItem].sTitleExtra	sTemp += ")"
												ENDIF
												
												IF NOT ARE_STRINGS_EQUAL(sDebugMenuData.sItems[iDepth][iItem].sFile, "")
												OR NOT ARE_STRINGS_EQUAL(sDebugMenuData.sItems[iDepth][iItem].sAuth, "")
													sTemp += " ("
													sTemp += sDebugMenuData.sItems[iDepth][iItem].sFile
													IF DOES_TEXT_LABEL_EXIST(sDebugMenuData.sItems[iDepth][iItem].sAuth)
														sTemp += " - "
														sTemp += GET_STRING_FROM_TEXT_FILE(sDebugMenuData.sItems[iDepth][iItem].sAuth)
													ELIF NOT ARE_STRINGS_EQUAL(sDebugMenuData.sItems[iDepth][iItem].sAuth, "")
														sTemp += " - "
														sTemp += sDebugMenuData.sItems[iDepth][iItem].sAuth
													ENDIF
													sTemp += ")"
												ENDIF
												//IF IS_DEBUG_MENU_ITEM_FLAG_SET(sDebugMenuData.sItems[iDepth][iItem].iFlags, DM_BIT_HIGHLIGHT)
												IF sDebugMenuData.sItems[iDepth][iItem].iPerc < 70
												AND sDebugMenuData.sItems[iDepth][iItem].iPerc >= 0
													DM_DISPLAY_MENU_ITEM(bEnabled, bSelected, fTextX[iDepth]+fXOffset, fTextY[iDepth]+fYOffset, fMarginX[iDepth], sTemp, bShortText, 189, 39,  39,  255)
												ELSE
													DM_DISPLAY_MENU_ITEM(bEnabled, bSelected, fTextX[iDepth]+fXOffset, fTextY[iDepth]+fYOffset, fMarginX[iDepth], sTemp, bShortText)
												ENDIF
											ENDIF
										BREAK
										
										// All other item types
										DEFAULT
											//IF IS_DEBUG_MENU_ITEM_FLAG_SET(sDebugMenuData.sItems[iDepth][iItem].iFlags, DM_BIT_HIGHLIGHT)
											IF sDebugMenuData.sItems[iDepth][iItem].iPerc < 70
											AND sDebugMenuData.sItems[iDepth][iItem].iPerc >= 0
												DM_DISPLAY_MENU_ITEM_LABEL_HIGHLIGHT(bEnabled, bSelected, fTextX[iDepth]+fXOffset, fTextY[iDepth]+fYOffset, sDebugMenuData.sItems[iDepth][iItem].sTitle, "DM_COL_R", fMarginX[iDepth], bShortText)
											ELSE
												DM_DISPLAY_MENU_ITEM_LABEL(bEnabled, bSelected, fTextX[iDepth]+fXOffset, fTextY[iDepth]+fYOffset, sDebugMenuData.sItems[iDepth][iItem].sTitle, fMarginX[iDepth], bShortText)
											ENDIF
										BREAK
									ENDSWITCH
									
									// Highlight selected item and use border to determine if the user wants to select via the mouse
									DM_DISPLAY_MENU_ITEM_BORDER(sDebugMenuData.iCurrentItem, sDebugMenuData.iCurrentDepth, bUpdatingItem, bSelected, bEnabled, fTextY[iDepth]+fYOffset, iItem, iDepth, sDebugMenuData.sItems[iDepth][iItem].iType, bShortText)
									IF bUpdatingItem
										SET_DEBUG_MENU_PROCESS_FLAG(sDebugMenuData.iProcessFlags, DM_PROCESS_UPDATE_MENU_ITEM)
									ENDIF
									
									iItemsInColumn[iDepth]++ // Increase the item count for this depth
									sDebugMenuData.iUnderItem[iDepth] = iItem+1 // Set iUnderItem to the next item
									fXOffset =  0.0 // Set up for the next line
									fYOffset += fYStep
							
								// Not allowed to add any more items
								ELSE
									IF NOT bDisplayUnderCut[iDepth]
										// Display marker to notify that there are more items at the bottom of the list
										DM_DISPLAY_MENU_ITEM_LABEL(TRUE, FALSE, fTextX[iDepth]+0.0013, fTextY[iDepth]+0.457, "DM_CUTOFF")
										
										sDebugMenuData.iUnderItem[iDepth] = iItem
										bDisplayUnderCut[iDepth] = TRUE
										bDisplayScroll[iDepth] = TRUE
									ENDIF
								ENDIF
							
							// Not in scroll range
							ELSE
								// Display marker to notify that there are more items at the top of the list
								IF NOT bDisplayTopCut[iDepth]
									DM_DISPLAY_MENU_ITEM_LABEL(TRUE, FALSE, fTextX[iDepth]+0.0013, fTextY[iDepth]-0.0305, "DM_CUTOFF")
									bDisplayTopCut[iDepth] = TRUE
									bDisplayScroll[iDepth] = TRUE
								ENDIF
								
							ENDIF
							
							// Increment the amount of items that we have to display
							iItemsToDisplay[iDepth]++
						
						// Not to be displayed
						ENDIF
					ENDIF //IF NOT sDebugMenuData.bDrawAltWindow OR iItem < 8 OR iDepth = 0
				ENDFOR
				
				// Draw the scrollbar if we have too many items to display on screen
				IF bDisplayScroll[iDepth]
					IF iDepth <= sDebugMenuData.iCurrentDepth
					
						//BOOL bUp = (iSelectedItem[iDepth] > iFirstItem[iDepth])
						//BOOL bDown = (iSelectedItem[iDepth] < iItemsToDisplay[iDepth]-1)
					
						DM_DISPLAY_SCROLL_BAR(iDepth, iItemsToDisplay[iDepth], iFirstItemInColumn[iDepth], bShortText, /*bUp, bDown,*/ 255)
						
						// Use the border x and w to determine if the player is wanting to scroll via mouse
						IF IS_MOUSE_BUTTON_JUST_PRESSED(MB_LEFT_BTN)
						
							FLOAT fMouseX = 0.0
							FLOAT fMouseY = 0.0
							FLOAT fX = fBorderX
							FLOAT fTopY = 0.105
							FLOAT fBottomY = 0.600
							FLOAT fW = fBorderW
							FLOAT fH = 0.020
							
							GET_MOUSE_POSITION(fMouseX, fMouseY)
							
							IF ((fMouseY >= fTopY - (fH/2.0)) AND (fMouseY <= fTopY + (fH/2.0)))
								IF ((fMouseX >= fX - (fW/2.0)) AND (fMouseX <= fX + (fW/2.0)))
									sDebugMenuData.iCurrentDepth = iDepth
									sDebugMenuData.bMouseMoveUp = TRUE
								ENDIF
							ELIF ((fMouseY >= fBottomY - (fH/2.0)) AND (fMouseY <= fBottomY + (fH/2.0)))
								IF ((fMouseX >= fX - (fW/2.0)) AND (fMouseX <= fX + (fW/2.0)))	
									sDebugMenuData.iCurrentDepth = iDepth
									sDebugMenuData.bMouseMoveDown = TRUE
								ENDIF
							ENDIF
						ENDIF
					ELSE
						DM_DISPLAY_SCROLL_BAR(iDepth, iItemsToDisplay[iDepth], iFirstItemInColumn[iDepth], bShortText, 0)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	// Update selected item if using mouse input
	IF sDebugMenuData.bMouseMoveDown
		sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth] = iLastItemInColumn[sDebugMenuData.iCurrentDepth]
	ELIF sDebugMenuData.bMouseMoveUp
		sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth] = iFirstItemInColumn[sDebugMenuData.iCurrentDepth]
	ENDIF
	
	// Display the additional information for the current item
	IF bDisplayInfo
		IF iSelectedItemInColumn[sDebugMenuData.iCurrentDepth] <> -1
			
			BOOL bLoadingInfo
			IF sDebugMenuData.sBugData.bDisplay
				bLoadingInfo = (IS_DEBUG_MENU_PROCESS_FLAG_SET(sDebugMenuData.iProcessFlags, DM_PROCESS_BUGSTAR_GET_BUGS))
			ELSE
				bLoadingInfo = (IS_DEBUG_MENU_PROCESS_FLAG_SET(sDebugMenuData.iProcessFlags, DM_PROCESS_UPDATE_MENU_ITEM) OR IS_DEBUG_MENU_PROCESS_FLAG_SET(sDebugMenuData.iProcessFlags, DM_PROCESS_BUGSTAR_GET_DETAILED))
			ENDIF
			
			DM_DISPLAY_MENU_ITEM_INFO(sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]], sDebugMenuData.sBugstarData, sDebugMenuData.sBugData, bLoadingInfo, 255)
		ENDIF
	ENDIF
	
	// Draw the popup window
	IF sDebugMenuData.bDrawAltWindow
		INT codeItemIndex
		INT numItems
		FLOAT fYHeight

		// The window is dynamically sized to the nubmer of options. Lets see how big the box should be.
		REPEAT MAX_ALT_CODE_IDS codeItemIndex
			IF sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].iAltCodeID[codeItemIndex] != ENUM_TO_INT(SPCID_DEFAULT)
				numItems++
			ENDIF
		ENDREPEAT

		fYStep 	  = DM_GET_TEXT_SCALE_Y(FALSE) * DM_GET_SCREEN_SCALE_Y() * DM_GET_TEXT_SPACING_Y(FALSE)
		fYHeight = fYStep * numItems
		fXOffset = 0.375
		fYOffset = 0.5 - fYHeight * 0.5
		
		DRAW_RECT(0.5, 0.5, 1.0, 1.0, 0, 0, 0, 175)
		DRAW_RECT(0.5, 0.5, 0.255, fYHeight + 0.007, MENU_BG_EDGE_R, MENU_BG_EDGE_G, MENU_BG_EDGE_B, 255)
		DRAW_RECT(0.5, 0.5, 0.25, fYHeight, MENU_BG_R, MENU_BG_G, MENU_BG_B, 255)
	
		REPEAT MAX_ALT_CODE_IDS codeItemIndex
			IF sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].iAltCodeID[codeItemIndex] != ENUM_TO_INT(SPCID_DEFAULT)
				IF codeItemIndex = sDebugMenuData.iCurrentAltItem
					DRAW_RECT(0.5, fYOffset + fYStep * 0.5, 0.25, fYStep, 255, 255, 255, 75)
				ENDIF
				DM_DISPLAY_MENU_ITEM(TRUE, FALSE, fXOffset + 0.005, fYOffset + 0.0075, 0.0, GET_STRING_FOR_CODEID(INT_TO_ENUM( SP_MENU_CODE_ID_ENUM,sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].iAltCodeID[codeItemIndex])), FALSE)
			ENDIF
			fYOffset += fYStep
		ENDREPEAT
	ELSE
	ENDIF
ENDPROC


/// PURPOSE: Returns the time stamp from when the debug menu item was last selected.
FUNC STRING SP_GET_LATEST_SELECTION()
	
	INT iLatest = 0
	
	INT i
	FOR i = 0 TO MAX_DEBUG_MENU_SELECTIONS_TO_TRACK-1
		IF g_debugMenuControl.sDMSelections[i].iTimeStamp >  g_debugMenuControl.sDMSelections[iLatest].iTimeStamp
			iLatest = i
		ENDIF
	ENDFOR
	
	RETURN GET_STRING_FROM_STRING(g_debugMenuControl.sDMSelections[iLatest].sLabel, 0, GET_LENGTH_OF_LITERAL_STRING(g_debugMenuControl.sDMSelections[iLatest].sLabel))
	
ENDFUNC

/// PURPOSE: Take a note of the current menu selection along with the current time
PROC SP_STORE_SELECTION_NAME_AND_TIME(DEBUG_MENU_DATA_STRUCT &sDebugMenuData)

	// Reset back to 0 if we have reached limit
	// This way we overwrite the older entries
	IF g_debugMenuControl.iDMSelectionSlot >= MAX_DEBUG_MENU_SELECTIONS_TO_TRACK
		g_debugMenuControl.iDMSelectionSlot = 0
	ENDIF
	
	// See if we can store a script file
	IF NOT ARE_STRINGS_EQUAL("", sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].sFile)
		
		// Note - to keep things consistent, remove the .sc part of the script
		TEXT_LABEL_63 scriptFile = sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].sFile
	
		IF GET_LENGTH_OF_LITERAL_STRING(scriptFile) > 3
			IF ARE_STRINGS_EQUAL(GET_STRING_FROM_STRING(scriptFile, GET_LENGTH_OF_LITERAL_STRING(scriptFile)-3, GET_LENGTH_OF_LITERAL_STRING(scriptFile)), ".sc")
				scriptFile = GET_STRING_FROM_STRING(scriptFile, 0, GET_LENGTH_OF_LITERAL_STRING(scriptFile)-3)
			ENDIF
		ENDIF
		
		// Overwrite existing timestamp if it exists
		INT i
		FOR i = 0 TO MAX_DEBUG_MENU_SELECTIONS_TO_TRACK-1
			IF ARE_STRINGS_EQUAL(g_debugMenuControl.sDMSelections[i].sLabel, scriptFile)
				g_debugMenuControl.sDMSelections[i].iTimeStamp = GET_GAME_TIMER()
				EXIT
			ENDIF
		ENDFOR
		
		// Doesnt exist so add new label
		g_debugMenuControl.sDMSelections[g_debugMenuControl.iDMSelectionSlot].sLabel = scriptFile
		
	// If we cant store script file then grab the title
	ELSE
	
		// Note - to keep things consistent, remove the .sc part of the title
		TEXT_LABEL_63 title = sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].sTitle
	
		IF GET_LENGTH_OF_LITERAL_STRING(title) > 3
			IF ARE_STRINGS_EQUAL(GET_STRING_FROM_STRING(title, GET_LENGTH_OF_LITERAL_STRING(title)-3, GET_LENGTH_OF_LITERAL_STRING(title)), ".sc")
				title = GET_STRING_FROM_STRING(title, 0, GET_LENGTH_OF_LITERAL_STRING(title)-3)
			ENDIF
		ENDIF
		
		// Overwrite existing timestamp if it exists
		INT i
		FOR i = 0 TO MAX_DEBUG_MENU_SELECTIONS_TO_TRACK-1
			IF ARE_STRINGS_EQUAL(g_debugMenuControl.sDMSelections[i].sLabel, title)
				g_debugMenuControl.sDMSelections[i].iTimeStamp = GET_GAME_TIMER()
				EXIT
			ENDIF
		ENDFOR
	
		// Doesnt exist so add new label
		g_debugMenuControl.sDMSelections[g_debugMenuControl.iDMSelectionSlot].sLabel = title
	ENDIF
	
	// Newly added so set time stamp and increase slot num
	g_debugMenuControl.sDMSelections[g_debugMenuControl.iDMSelectionSlot].iTimeStamp = GET_GAME_TIMER()
	g_debugMenuControl.iDMSelectionSlot++
	
ENDPROC

/// PURPOSE: Sets the current time of day
PROC SP_SET_TIME_OF_DAY(DEBUG_MENU_DATA_STRUCT &sDebugMenuData)
	IF sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].iHour <> -1
		INT h = sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].iHour
		SET_CLOCK_TIME(h, 0, 0)
	ENDIF
ENDPROC

/// PURPOSE:
///    Test the two strings, character by character, to see which order they should be alphabetically
/// RETURNS:
///		TRUE if s1 should come before s2 alphabetically, FALSE otherwise    
FUNC BOOL IS_STRING_ALPHABETICALLY_BEFORE_STRING(STRING s1, STRING s2)
	STRING s11, s21
	INT char1, char2, charIndex
	REPEAT IMIN(GET_LENGTH_OF_LITERAL_STRING(s1), GET_LENGTH_OF_LITERAL_STRING(s2)) charIndex
		s11 = GET_STRING_FROM_STRING(s1, charIndex, charIndex+1)
		char1 = CONVERT_SINGLE_CHARACTER_TO_INT(s11)
		s21 = GET_STRING_FROM_STRING(s2, charIndex, charIndex+1)
		char2 = CONVERT_SINGLE_CHARACTER_TO_INT(s21)
		IF char1 > char2 
			RETURN TRUE
		ELIF char1 < char2
			RETURN FALSE
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    For dynamic menus, we want to sort alphabetically after they are loaded
PROC DM_SORT_MENU_LIST_ALPHABETICAL(DEBUG_MENU_DATA_STRUCT &sDebugMenuData, INT iDepth)
	
	// Some vars to help us sort
	INT iCurrentItem, iSeekItem
	DEBUG_MENU_ITEM_STRUCT sTempItem
	BOOL bDoSwap
	// When we sort we want to keep the same item selected
	// We will only need to do this for the current depth
	// as the previous depth items will all be menus.
	INT iUniqueID = sDebugMenuData.sItems[iDepth][sDebugMenuData.iCurrentItem[iDepth]].iUniqueID
		
	// Go through all the items
	FOR iCurrentItem = 0 TO sDebugMenuData.iItemCount[iDepth]-1
		
			// Compare item with the rest of the items in the list
			FOR iSeekItem = iCurrentItem+1  TO sDebugMenuData.iItemCount[iDepth]-1
				// Check we are in range
				IF iSeekItem < sDebugMenuData.iItemCount[iDepth]
					
						IF NOT IS_STRING_NULL_OR_EMPTY(sDebugMenuData.sItems[iDepth][iCurrentItem].sTitle)
							bDoSwap = IS_STRING_ALPHABETICALLY_BEFORE_STRING(sDebugMenuData.sItems[iDepth][iCurrentItem].sTitle, sDebugMenuData.sItems[iDepth][iSeekItem].sTitle)
						ELIF NOT IS_STRING_NULL_OR_EMPTY(sDebugMenuData.sItems[iDepth][iCurrentItem].sFile)
							bDoSwap = IS_STRING_ALPHABETICALLY_BEFORE_STRING(sDebugMenuData.sItems[iDepth][iCurrentItem].sFile, sDebugMenuData.sItems[iDepth][iSeekItem].sFile)
						ELSE
							EXIT
						ENDIF
						
						
						// Compare percentages and decide if we should swap items
						IF bDoSwap
																	
							// Make copy of item in the current slot
							sTempItem = sDebugMenuData.sItems[iDepth][iCurrentItem]
				
							// Put the correct item into the slot
							sDebugMenuData.sItems[iDepth][iCurrentItem] = sDebugMenuData.sItems[iDepth][iSeekItem]
							
							// Put the copy into the old correct item slot
							sDebugMenuData.sItems[iDepth][iSeekItem] = sTempItem
						
						// Swap items?
						ENDIF
						
				// In range?
				ENDIF
				
			// End of seek item cycle
			ENDFOR

	// End of item cycle
	ENDFOR
		
	// Update the current selection
	INT i
	FOR i = 0 TO sDebugMenuData.iItemCount[iDepth]-1
		IF sDebugMenuData.sItems[iDepth][i].iUniqueID = iUniqueID
			sDebugMenuData.iCurrentItem[iDepth] = i
			
			// Check if the selected item has went below iUnderItem
			IF sDebugMenuData.iCurrentItem[iDepth] >= sDebugMenuData.iUnderItem[iDepth]
				
				// Get the new top item by traversing back up the list
				INT j
				FOR j = 0 TO DM_MAX_ITEMS_IN_COLUMN-2	// has to be -2 not -1 as we want to keep the iUnderItem as the last+1 in list
					// Obtain the previous item in list
					DM_GET_PREVIOUS_MENU_ITEM(sDebugMenuData, iDepth, TRUE, FALSE)
				ENDFOR
	
				// Set the iTopItem to the previous menu item
				sDebugMenuData.iTopItem[iDepth] = sDebugMenuData.iCurrentItem[iDepth]
				
				// Reset the current item
				sDebugMenuData.iCurrentItem[iDepth] = i
				
			ENDIF
			
			// Check if the selected item has went above iTopItem
			IF sDebugMenuData.iCurrentItem[iDepth] < sDebugMenuData.iTopItem[iDepth]
				// Make the current selection the top item
				sDebugMenuData.iTopItem[iDepth] = sDebugMenuData.iCurrentItem[iDepth]
			ENDIF
		ENDIF
	ENDFOR
	
ENDPROC


#ENDIF
