USING "stack_sizes.sch"
USING "script_player.sch"
USING "commands_camera.sch"

/// Null function pointers for all debug menu struct FPs. Any item can be set to these, and the function has no effect when run.

FUNC BOOL DebugMenuRunCodeID_NULL(DEBUG_MENU_DATA_STRUCT &sDebugMenuData)
	sDebugMenuData.bMouseMoveUp = sDebugMenuData.bMouseMoveUp
	RETURN TRUE
ENDFUNC

PROC DebugMenuRunDisplayID_NULL(DEBUG_MENU_DATA_STRUCT &sDebugMenuData, INT iDepth)
	sDebugMenuData.bMouseMoveUp = sDebugMenuData.bMouseMoveUp
	iDepth = iDepth
ENDPROC

PROC DebugMenuRunEnableID_NULL(DEBUG_MENU_DATA_STRUCT &sDebugMenuData, INT iDepth, BOOL bForceDisabled = FALSE)
	sDebugMenuData.bMouseMoveUp = sDebugMenuData.bMouseMoveUp
	iDepth = iDepth
	bForceDisabled = bForceDisabled
ENDPROC

FUNC BOOL DebugMenuProcessSelection_NULL(DEBUG_MENU_DATA_STRUCT &sDebugMenuData)
	sDebugMenuData.bMouseMoveUp = sDebugMenuData.bMouseMoveUp
	RETURN TRUE
ENDFUNC

PROC DebugMenuResetData_NULL(DEBUG_MENU_DATA_STRUCT &sDebugMenuData)
	sDebugMenuData.bMouseMoveUp = sDebugMenuData.bMouseMoveUp
ENDPROC

PROC DebugMenuCleanup_NULL(DEBUG_MENU_DATA_STRUCT &sDebugMenuData)
	sDebugMenuData.bMouseMoveUp = sDebugMenuData.bMouseMoveUp
ENDPROC

PROC DebugMenuCheckSelection_NULL(DEBUG_MENU_DATA_STRUCT &sDebugMenuData)
	sDebugMenuData.bMouseMoveUp = sDebugMenuData.bMouseMoveUp
ENDPROC

PROC DebugMenuLaunchScript_NULL(DEBUG_MENU_DATA_STRUCT &sDebugMenuData)
	sDebugMenuData.bMouseMoveUp = sDebugMenuData.bMouseMoveUp
ENDPROC

PROC DebugMenuWarpPlayer_NULL(DEBUG_MENU_DATA_STRUCT &sDebugMenuData, BOOL paramKeepVehicle = FALSE)
	sDebugMenuData.bMouseMoveUp = sDebugMenuData.bMouseMoveUp
	paramKeepVehicle = paramKeepVehicle
ENDPROC


/// PURPOSE: Launches the specified script file
#IF NOT DEFINED(SP_LAUNCH_SCRIPT)
PROC SP_LAUNCH_SCRIPT(DEBUG_MENU_DATA_STRUCT &sDebugMenuData)

	// Check if xml has specified not to launch a script
	IF NOT (IS_DEBUG_MENU_ITEM_FLAG_SET(sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].iFlags, DM_BIT_LAUNCH_SCRIPT))
		EXIT
	ENDIF

	TEXT_LABEL_63 scriptFile = sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].sFile
	
	IF (IS_STRING_NULL(scriptFile))
	OR ARE_STRINGS_EQUAL(scriptFile, "")
		EXIT
	ENDIF
		
	IF ARE_STRINGS_EQUAL(GET_STRING_FROM_STRING(scriptFile, GET_LENGTH_OF_LITERAL_STRING(scriptFile)-3, GET_LENGTH_OF_LITERAL_STRING(scriptFile)), ".sc")
		scriptFile = GET_STRING_FROM_STRING(scriptFile, 0, GET_LENGTH_OF_LITERAL_STRING(scriptFile)-3)
	ENDIF

	REQUEST_SCRIPT(scriptFile)
	WHILE NOT (HAS_SCRIPT_LOADED(scriptFile))
		WAIT(0)
		REQUEST_SCRIPT(scriptFile)
	ENDWHILE
	
	CPRINTLN(DEBUG_DEBUG_MENU, " SP_Launch_Script ", scriptFile)
	
	IF sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].iType = DM_HASH_ITEM_MISSION
	OR sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].iType = DM_HASH_ITEM_BUGSTAR
		START_NEW_SCRIPT(scriptFile, MISSION_STACK_SIZE)
	ELIF sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].iType = DM_HASH_ITEM_TOOL	
		START_NEW_SCRIPT(scriptFile, SCRIPT_XML_STACK_SIZE)
	ELSE
		START_NEW_SCRIPT(scriptFile, DEFAULT_STACK_SIZE)
	ENDIF
	
	SET_SCRIPT_AS_NO_LONGER_NEEDED(scriptFile)
ENDPROC
#ENDIF

#IF NOT DEFINED(SP_GET_POSITION_FOR_WARP)
/// PURPOSE: Picks a random coord from the possible 3
PROC SP_GET_POSITION_FOR_WARP(DEBUG_MENU_DATA_STRUCT &sDebugMenuData, VECTOR &vCoords, FLOAT &fHeading)
	
	INT i, j, k
	VECTOR vTemp[3]
	FLOAT fTemp[3]
	
	vTemp[0] = << DM_NO_VECTOR, DM_NO_VECTOR, DM_NO_VECTOR >>
	vTemp[1] = << DM_NO_VECTOR, DM_NO_VECTOR, DM_NO_VECTOR >>
	vTemp[2] = << DM_NO_VECTOR, DM_NO_VECTOR, DM_NO_VECTOR >>
	
	fTemp[0] = DM_NO_HEADING
	fTemp[1] = DM_NO_HEADING
	fTemp[2] = DM_NO_HEADING
	
	REPEAT 3 i
		IF sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].vCoord[i].x <> DM_NO_VECTOR
		AND sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].vCoord[i].y <> DM_NO_VECTOR
		AND sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].vCoord[i].z <> DM_NO_VECTOR
			vTemp[j] = sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].vCoord[i]
			fTemp[j] = sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].fHeading[i]
			j++
		ENDIF
	ENDREPEAT
	
	k = GET_RANDOM_INT_IN_RANGE(0, j)
	
	vCoords = vTemp[k]
	fHeading = fTemp[k]
ENDPROC
#ENDIF

#IF NOT DEFINED(SP_WARP_PLAYER)
/// PURPOSE: Sets the players coordinates and heading if interior is null
PROC SP_WARP_PLAYER(DEBUG_MENU_DATA_STRUCT &sDebugMenuData, BOOL paramKeepVehicle = FALSE)
	
	paramKeepVehicle = paramKeepVehicle

	// NOTE: Leave the player in a vehicle if already in one
	VECTOR theCoords
	FLOAT theHeading
	SP_GET_POSITION_FOR_WARP(sDebugMenuData, theCoords, theHeading)
	

	IF (IS_PLAYER_PLAYING(PLAYER_ID()))
		IF NOT (theCoords.x < DM_MIN_VECTOR)
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				SET_ENTITY_COORDS(PLAYER_PED_ID(), theCoords)
				
				IF NOT (theHeading < DM_MIN_HEADING)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), theHeading)
				ENDIF
				
				WAIT(0)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
			ENDIF
		ENDIF
	ENDIF
	
	CPRINTLN(DEBUG_DEBUG_MENU, "SP_WARP_PLAYER() coords = ", theCoords, PICK_STRING( theHeading <> DM_NO_HEADING, ", heading = ", ""), PICK_FLOAT(theHeading <> DM_NO_HEADING, theHeading, 0.0))
ENDPROC
#ENDIF


#IF NOT DEFINED(SP_CHECK_MENU_SELECTION)
/// PURPOSE: Checks to see if the player has made a selection
PROC SP_CHECK_MENU_SELECTION(DEBUG_MENU_DATA_STRUCT &sDebugMenuData)

	// Make a selection
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
	OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_RETURN)
	
		// Check that the item is not a menu or break item
		IF sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].iType <> DM_HASH_ITEM_MENU
		AND sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].iType <> DM_HASH_ITEM_BREAK
		AND sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].iType <> DM_HASH_ITEM_INPUT
		AND IS_DEBUG_MENU_ITEM_FLAG_SET(sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].iFlags, DM_BIT_ENABLED)

			// ...make a note of this item selection.
			SP_STORE_SELECTION_NAME_AND_TIME(sDebugMenuData)
			
			// ...perform any time updates associated with menu item.
			SP_SET_TIME_OF_DAY(sDebugMenuData)
			
			// ...perform any special code associated with menu item.
			CALL sDebugMenuData.fpRunCodeID(sDebugMenuData)
			
				// ...launch any scripts associated with menu item.
			SP_LAUNCH_SCRIPT(sDebugMenuData)
			
			// ...perform any warps associatated with menu item.
			SP_WARP_PLAYER(sDebugMenuData)
			
			SET_DEBUG_MENU_PROCESS_FLAG(sDebugMenuData.iProcessFlags, DM_PROCESS_CLEANUP)
		ENDIF
		
	ENDIF
	
ENDPROC
#ENDIF


#IF NOT DEFINED(DEBUG_MENU_PROCESS_CHANGE_MODES)
/// PURPOSE:
///    Project-speficic 'change modes' function, if you menu has different modes (ie SP / MP). Override this!
/// PARAMS:
///    sDebugMenuData - 
PROC DEBUG_MENU_PROCESS_CHANGE_MODES(DEBUG_MENU_DATA_STRUCT &sDebugMenuData)
	
	#if USE_CLF_DLC	
		sDebugMenuData.sXMLFile = "SPDebug/SP_debugCLF_menu.xml"
	#endif
	#if USE_NRM_DLC
		sDebugMenuData.sXMLFile = "SPDebug/SP_debugNRM_menu2.xml"
	#endif
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
		sDebugMenuData.sXMLFile = "SPDebug/SP_debug_menu.xml"
	#endif
	#endif
	
	// Fill in debug menu function pointers for SP
	sDebugMenuData.fpRunCodeID = &DebugMenuRunCodeID_NULL
	sDebugMenuData.fpRunDisplayID = &DebugMenuRunDisplayID_NULL
	sDebugMenuData.fpRunEnableID = &DebugMenuRunEnableID_NULL
	sDebugMenuData.fpProcessSelection = &DebugMenuProcessSelection_NULL
	sDebugMenuData.fpResetData = &DebugMenuResetData_NULL
	sDebugMenuData.fpCleanup = &DebugMenuCleanup_NULL
	sDebugMenuData.fpCheckSelection = &SP_CHECK_MENU_SELECTION
	sDebugMenuData.fpLaunchScript = &SP_LAUNCH_SCRIPT
	sDebugMenuData.fpWarpPlayer = &SP_WARP_PLAYER
ENDPROC
#ENDIF

#IF NOT DEFINED(BUILD_DEBUG_MENU_LIST_DYNAMIC)
/// PURPOSE:
///    Project specific function that allows building of dynamic debug menus, based on xml inputs and game data. Override this!
FUNC BOOL BUILD_DEBUG_MENU_LIST_DYNAMIC(DEBUG_MENU_DATA_STRUCT &sDebugMenuData, INT iDepth, SP_MENU_CODE_ID_ENUM codeID)
	sDebugMenuData.bMouseMoveUp = sDebugMenuData.bMouseMoveUp
	iDepth = iDepth
	codeID = codeID
	RETURN FALSE
ENDFUNC
#ENDIF


