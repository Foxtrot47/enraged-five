//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	SP_Debug_Menu.sch											//
//		AUTHOR			:	Kenneth Ross												//
//		DESCRIPTION		:	Singleplayer debug menu with all the specific functionality //
//							for SP menu items. Uses the Shared_Debug_Menu.sch for		//
//							all the generic menu functionality.							//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////

// Include the standard headers (which also ensures the correct #DEFINEs are recognised)
USING "rage_builtins.sch"
USING "globals.sch"

// Do nothing in release mode
#IF IS_FINAL_BUILD
SCRIPT
ENDSCRIPT
#ENDIF

// Only include in debug mode
#IF IS_DEBUG_BUILD
USING "debug_menu_shared_core.sch"
USING "debug_menu_GAME.sch"
USING "debug_menu_core_override.sch"


//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		PROC TO CONTROL SINGLEPLAYER MENU												//
//    																					//
//////////////////////////////////////////////////////////////////////////////////////////  

/// PURPOSE: Main proc that handles the singleplayer debug menu.
///    Should be called once a frame by the debug thread.
PROC PROCESS_DEBUG_MENU(DEBUG_MENU_DATA_STRUCT &sDebugMenuData)
	
	////////////////////////////////////////////////////////////////////////////////////////////////
	///     
	///     CHECK MODE CHANGES
	///     
	IF IS_DEBUG_MENU_PROCESS_FLAG_SET(sDebugMenuData.iProcessFlags, DM_PROCESS_CHANGE_MODES)
		DEBUG_MENU_PROCESS_CHANGE_MODES(sDebugMenuData)
		CLEAR_DEBUG_MENU_PROCESS_FLAG(sDebugMenuData.iProcessFlags, DM_PROCESS_CHANGE_MODES)
	ENDIF
	
	////////////////////////////////////////////////////////////////////////////////////////////////
	///     
	///     PROCESS SELECTION
	///     
	IF IS_DEBUG_MENU_PROCESS_FLAG_SET(sDebugMenuData.iProcessFlags, DM_PROCESS_SELECTION)
		IF CALL sDebugMenuData.fpProcessSelection(sDebugMenuData)
			SET_DEBUG_MENU_PROCESS_FLAG(sDebugMenuData.iProcessFlags, DM_PROCESS_CLEANUP)
			CLEAR_DEBUG_MENU_PROCESS_FLAG(sDebugMenuData.iProcessFlags, DM_PROCESS_SELECTION)
		ENDIF
	ENDIF
	
	
	////////////////////////////////////////////////////////////////////////////////////////////////
	///     
	///     CLEANUP MENU
	///     
	IF IS_DEBUG_MENU_PROCESS_FLAG_SET(sDebugMenuData.iProcessFlags, DM_PROCESS_CLEANUP)
		IF IS_DEBUG_MENU_PROCESS_FLAG_SET(sDebugMenuData.iProcessFlags, DM_PROCESS_LAUNCH)
			// Store the menu selection so we can restore it later
			DM_UPDATE_LAST_MENU_SELECTION(sDebugMenuData)
			
			// Remove any debug text that we added
			DM_REMOVE_BUGSTAR_DEBUG_TEXT(sDebugMenuData, -1)
			DM_REMOVE_BUGSTAR_DEBUG_BUG_TEXT(sDebugMenuData.sBugData)
			
			g_debugMenuControl.bDebugMenuOnScreen = FALSE
			g_flowUnsaved.bShowMissionFlowDebugScreen = TRUE
			
			CALL sDebugMenuData.fpCleanup(sDebugMenuData)
		ENDIF
		
		DM_CLEAR_PROCESS_FLAGS_FOR_CLEANUP(sDebugMenuData)
	
	
	////////////////////////////////////////////////////////////////////////////////////////////////
	///     
	///     LAUNCH MENU
	///     
	ELIF IS_DEBUG_MENU_PROCESS_FLAG_SET(sDebugMenuData.iProcessFlags, DM_PROCESS_LAUNCH)
	
		CLEAR_DEBUG_MENU_PROCESS_FLAG(sDebugMenuData.iProcessFlags, DM_PROCESS_WAIT)
		
		
		////////////////////////////////////////////////////////////////////////////////////////////////
		///     
		///     RESET DATA
		///     
		IF NOT IS_DEBUG_MENU_PROCESS_FLAG_SET(sDebugMenuData.iProcessFlags, DM_PROCESS_PRE_INITIALISED)
			
			RESET_ALL_DEBUG_MENU_DATA(sDebugMenuData)
			DEBUG_MENU_PROCESS_CHANGE_MODES(sDebugMenuData)
			BUILD_DEBUG_MENU_LIST(sDebugMenuData, 0, sDebugMenuData.sXMLFile)
			
		
			g_flowUnsaved.bShowMissionFlowDebugScreen = FALSE
			g_debugMenuControl.bDebugMenuOnScreen = TRUE
			
			// When the Bugstar servers go down the menu can hang for up to 1 minute as it attempts to perform a query.
			// Therefore we should only renew the query flag every 5 minutes to reduce the amount of time users have to wait.
			IF NOT g_bugstarQuery.bQueriesDisabled
				IF (GET_GAME_TIMER() - g_debugMenuControl.iBugstarQueryRenew) > 0
					g_debugMenuControl.iBugstarQueryRenew = GET_GAME_TIMER()+300000
					g_debugMenuControl.bUseBugstarQuery = TRUE
				ENDIF
			ELSE
				// Make sure we clear this so we don't get stuck.
				g_debugMenuControl.bUseBugstarQuery = FALSE
			ENDIF
			
			CALL sDebugMenuData.fpResetData(sDebugMenuData)
			
			SET_DEBUG_MENU_PROCESS_FLAG(sDebugMenuData.iProcessFlags, DM_PROCESS_PRE_INITIALISED)
			SET_DEBUG_MENU_PROCESS_FLAG(sDebugMenuData.iProcessFlags, DM_PROCESS_UPDATE_MENU_ITEM)
			SET_DEBUG_MENU_PROCESS_FLAG(sDebugMenuData.iProcessFlags, DM_PROCESS_RESTORE_SELECTION)
			SET_DEBUG_MENU_PROCESS_FLAG(sDebugMenuData.iProcessFlags, DM_PROCESS_RESTORE_SORT) // Gets processed after we update basic info
			SET_DEBUG_MENU_PROCESS_FLAG(sDebugMenuData.iProcessFlags, DM_PROCESS_WAIT)
		ENDIF
		
		
		////////////////////////////////////////////////////////////////////////////////////////////////
		///     
		///     INITIALISE ITEMS FOR CURRENT MENU
		///     
		IF NOT IS_DEBUG_MENU_PROCESS_FLAG_SET(sDebugMenuData.iProcessFlags, DM_PROCESS_WAIT)
		AND NOT IS_DEBUG_MENU_PROCESS_FLAG_SET(sDebugMenuData.iProcessFlags, DM_PROCESS_INITIALISED)
		
			INT iDepth = sDebugMenuData.iCurrentDepth
			IF IS_DEBUG_MENU_PROCESS_FLAG_SET(sDebugMenuData.iProcessFlags, DM_PROCESS_INITIALISE_NEXT_MENU)
				iDepth++
			ENDIF
		
			// Determine which ones are allowed to be displayed
			CALL sDebugMenuData.fpRunDisplayID(sDebugMenuData, iDepth)
			
			// Determine which ones are enabled
			IF IS_DEBUG_MENU_ITEM_FLAG_SET(sDebugMenuData.sItems[iDepth][sDebugMenuData.iCurrentItem[iDepth]].iFlags, DM_BIT_ENABLED)
				CALL sDebugMenuData.fpRunEnableID(sDebugMenuData, iDepth, FALSE)
			ELSE
				CALL sDebugMenuData.fpRunEnableID(sDebugMenuData, iDepth, TRUE)
			ENDIF
			
			// Make sure the item selection is valid
			DM_GET_VALID_MENU_ITEM(sDebugMenuData, iDepth)
			
			SET_BUGSTAR_PROCESS_FLAG(sDebugMenuData.sBugstarData.eProcessFlags, BSTAR_PROCESS_BUGSTAR_QUERY_RESET)
			SET_DEBUG_MENU_PROCESS_FLAG(sDebugMenuData.iProcessFlags, DM_PROCESS_INITIALISED)
			SET_DEBUG_MENU_PROCESS_FLAG(sDebugMenuData.iProcessFlags, DM_PROCESS_WAIT)
			CLEAR_DEBUG_MENU_PROCESS_FLAG(sDebugMenuData.iProcessFlags, DM_PROCESS_INITIALISE_NEXT_MENU)
		ENDIF
		
		
		////////////////////////////////////////////////////////////////////////////////////////////////
		///     
		///     RESTORE LAST SELECTION FOR CURRENT MENU
		///     
		IF NOT IS_DEBUG_MENU_PROCESS_FLAG_SET(sDebugMenuData.iProcessFlags, DM_PROCESS_WAIT)
		AND IS_DEBUG_MENU_PROCESS_FLAG_SET(sDebugMenuData.iProcessFlags, DM_PROCESS_RESTORE_SELECTION)
			
			BOOL bSelectionRestored = FALSE
			
			// If we have already restored the selection for this depth, jump to the next depth
			IF g_debugMenuControl.bDMSelectionRestored[sDebugMenuData.iCurrentDepth]
				IF sDebugMenuData.iCurrentDepth < g_debugMenuControl.iDMCurrentDepth AND sDebugMenuData.iCurrentDepth < DM_MAX_DEPTH-1
					IF sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].iType = DM_HASH_ITEM_MENU
						sDebugMenuData.iCurrentDepth++
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT g_debugMenuControl.bDMSelectionRestored[sDebugMenuData.iCurrentDepth]
				bSelectionRestored = DM_RESTORE_LAST_MENU_SELECTION(sDebugMenuData, sDebugMenuData.iCurrentDepth)
			ENDIF
			
			IF bSelectionRestored
				SET_DEBUG_MENU_PROCESS_FLAG(sDebugMenuData.iProcessFlags, DM_PROCESS_UPDATE_MENU_ITEM)
			ELSE
				CLEAR_DEBUG_MENU_PROCESS_FLAG(sDebugMenuData.iProcessFlags, DM_PROCESS_RESTORE_SELECTION)
			ENDIF
		ENDIF
		
		
		////////////////////////////////////////////////////////////////////////////////////////////////
		///     
		///     PROCESS ITEM UPDATE
		///     
		IF NOT IS_DEBUG_MENU_PROCESS_FLAG_SET(sDebugMenuData.iProcessFlags, DM_PROCESS_WAIT)
		AND NOT IS_DEBUG_MENU_PROCESS_FLAG_SET(sDebugMenuData.iProcessFlags, DM_PROCESS_BUGSTAR_GET_BUGS)
		AND IS_DEBUG_MENU_PROCESS_FLAG_SET(sDebugMenuData.iProcessFlags, DM_PROCESS_UPDATE_MENU_ITEM)
		
			BOOL bProcess = TRUE
			
			// Make sure we are not trying to navigate if not restoring the selection
			IF NOT IS_DEBUG_MENU_PROCESS_FLAG_SET(sDebugMenuData.iProcessFlags, DM_PROCESS_RESTORE_SELECTION)
				INT leftStickLR = 0
				INT leftStickUD = 0
				INT rightStickLR = 0
				INT rightStickUD = 0
				GET_CONTROL_VALUE_OF_ANALOGUE_STICKS( leftStickLR, leftStickUD, rightStickLR, rightStickUD)
				
				IF (sDebugMenuData.iAllowInputCheckTimer+400 > GET_GAME_TIMER())
				AND NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
				AND NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
				AND NOT IS_KEYBOARD_KEY_PRESSED(KEY_LEFT)
				AND NOT IS_KEYBOARD_KEY_PRESSED(KEY_RIGHT)
				AND NOT (leftStickLR < -50)
				AND NOT (leftStickLR > 50)
					bProcess = FALSE
				ENDIF
			ENDIF
			
			IF bProcess
				
				DM_REMOVE_BUGSTAR_DEBUG_TEXT(sDebugMenuData, sDebugMenuData.iCurrentDepth+1)
				DM_REMOVE_BUGSTAR_DEBUG_BUG_TEXT(sDebugMenuData.sBugData)
				
				// Update sub menu
				IF sDebugMenuData.iCurrentDepth+1 < DM_MAX_DEPTH
					IF sDebugMenuData.sItems[sDebugMenuData.iCurrentDepth][sDebugMenuData.iCurrentItem[sDebugMenuData.iCurrentDepth]].iType = DM_HASH_ITEM_MENU
						IF sDebugmenuData.sItems[sDebugmenuData.iCurrentDepth][sDebugmenuData.iCurrentItem[sDebugmenuData.iCurrentDepth]].iCodeID = 0
							// If we don't have CID, we have an XML menu
							BUILD_DEBUG_MENU_LIST(sDebugmenuData, sDebugmenuData.iCurrentDepth+1, sDebugmenuData.sItems[sDebugmenuData.iCurrentDepth][sDebugmenuData.iCurrentItem[sDebugmenuData.iCurrentDepth]].sFile)
						ELSE // We have a CID to build a dynamic menu
							BUILD_DEBUG_MENU_LIST_DYNAMIC(sDebugmenuData, sDebugmenuData.iCurrentDepth+1, INT_TO_ENUM(SP_MENU_CODE_ID_ENUM, sDebugmenuData.sItems[sDebugmenuData.iCurrentDepth][sDebugmenuData.iCurrentItem[sDebugmenuData.iCurrentDepth]].iCodeID))
						ENDIF
						CLEAR_DEBUG_MENU_PROCESS_FLAG(sDebugMenuData.iProcessFlags, DM_PROCESS_INITIALISED)
						SET_DEBUG_MENU_PROCESS_FLAG(sDebugMenuData.iProcessFlags, DM_PROCESS_INITIALISE_NEXT_MENU)
						SET_DEBUG_MENU_PROCESS_FLAG(sDebugMenuData.iProcessFlags, DM_PROCESS_WAIT)
					ENDIF
				ENDIF
				CLEAR_DEBUG_MENU_PROCESS_FLAG(sDebugMenuData.iProcessFlags, DM_PROCESS_UPDATE_MENU_ITEM)
				SET_DEBUG_MENU_PROCESS_FLAG(sDebugMenuData.iProcessFlags, DM_PROCESS_BUGSTAR_GET_DETAILED)
			ENDIF
		ENDIF
		
		
		////////////////////////////////////////////////////////////////////////////////////////////////
		///     
		///     UPDATE BASIC BUGSTAR DATA
		///     
		IF NOT IS_DEBUG_MENU_PROCESS_FLAG_SET(sDebugMenuData.iProcessFlags, DM_PROCESS_WAIT)
		AND IS_DEBUG_MENU_PROCESS_FLAG_SET(sDebugMenuData.iProcessFlags, DM_PROCESS_BUGSTAR_GET_BASIC)
			
			// Get additional information for the current item
			IF DM_GET_BASIC_BUGSTAR_MISSION_DATA(sDebugMenuData, sDebugMenuData.sBugstarData.iDepthToLoadBasicInfo)
				CLEAR_DEBUG_MENU_PROCESS_FLAG(sDebugMenuData.iProcessFlags, DM_PROCESS_BUGSTAR_GET_BASIC)
				SET_BUGSTAR_PROCESS_FLAG(sDebugMenuData.sBugstarData.eProcessFlags, BSTAR_PROCESS_BUGSTAR_QUERY_RESET)
				SET_DEBUG_MENU_PROCESS_FLAG(sDebugMenuData.iProcessFlags, DM_PROCESS_WAIT)
				SET_DEBUG_MENU_PROCESS_FLAG(sDebugMenuData.iProcessFlags, DM_PROCESS_BUGSTAR_GET_DETAILED)
			ENDIF
		ENDIF
		
		
		////////////////////////////////////////////////////////////////////////////////////////////////
		///     
		///     UPDATE SORT ORDER
		///     
		IF NOT IS_DEBUG_MENU_PROCESS_FLAG_SET(sDebugMenuData.iProcessFlags, DM_PROCESS_WAIT)
		AND NOT IS_DEBUG_MENU_PROCESS_FLAG_SET(sDebugMenuData.iProcessFlags, DM_PROCESS_BUGSTAR_GET_BASIC)
		AND IS_DEBUG_MENU_PROCESS_FLAG_SET(sDebugMenuData.iProcessFlags, DM_PROCESS_RESTORE_SORT)
		
			
			// We do this after we have grabbed the basic bugstar data for each item so that
			// the sort can operate on the correct mission percentage values.
			// Also, we need to perform the sort on each sort stage to make sure the order matches.
			
			IF ENUM_TO_INT(sDebugMenuData.eMissionSort) != g_debugMenuControl.iDMSortType
			AND ENUM_TO_INT(sDebugMenuData.eMissionSort) < ENUM_TO_INT(DM_MAX_NUMBER_OF_SORT_ENUMS)-1
				sDebugMenuData.eMissionSort = INT_TO_ENUM(DEBUG_MENU_SORT_ENUM, (ENUM_TO_INT(sDebugMenuData.eMissionSort)+1))
				DM_SORT_MENU_LIST(sDebugMenuData, sDebugMenuData.sBugstarData.iDepthToLoadBasicInfo)
			ELSE	
				CLEAR_DEBUG_MENU_PROCESS_FLAG(sDebugMenuData.iProcessFlags, DM_PROCESS_RESTORE_SORT)
				SET_DEBUG_MENU_PROCESS_FLAG(sDebugMenuData.iProcessFlags, DM_PROCESS_RESTORE_SELECTION)
			ENDIF
			SET_DEBUG_MENU_PROCESS_FLAG(sDebugMenuData.iProcessFlags, DM_PROCESS_WAIT)
		ENDIF
		
		
		////////////////////////////////////////////////////////////////////////////////////////////////
		///     
		///     UPDATE DETAILED BUGSTAR DATA
		///     
		IF NOT IS_DEBUG_MENU_PROCESS_FLAG_SET(sDebugMenuData.iProcessFlags, DM_PROCESS_WAIT)
		AND NOT IS_DEBUG_MENU_PROCESS_FLAG_SET(sDebugMenuData.iProcessFlags, DM_PROCESS_BUGSTAR_GET_BASIC)
		AND IS_DEBUG_MENU_PROCESS_FLAG_SET(sDebugMenuData.iProcessFlags, DM_PROCESS_BUGSTAR_GET_DETAILED)
			// Get additional information for the current item
			IF DM_GET_DETAILED_BUGSTAR_MISSION_DATA(sDebugMenuData)
				CLEAR_DEBUG_MENU_PROCESS_FLAG(sDebugMenuData.iProcessFlags, DM_PROCESS_BUGSTAR_GET_DETAILED)
			ENDIF
		ENDIF
		
		
		////////////////////////////////////////////////////////////////////////////////////////////////
		///     
		///     UPDATE MISSION BUGS
		///     
		IF NOT IS_DEBUG_MENU_PROCESS_FLAG_SET(sDebugMenuData.iProcessFlags, DM_PROCESS_WAIT)
		AND IS_DEBUG_MENU_PROCESS_FLAG_SET(sDebugMenuData.iProcessFlags, DM_PROCESS_BUGSTAR_GET_BUGS)
		
			// Get list of bugs for the current mission
			IF DM_GET_MISSION_BUGS_DATA(sDebugMenuData)
				CLEAR_DEBUG_MENU_PROCESS_FLAG(sDebugMenuData.iProcessFlags, DM_PROCESS_BUGSTAR_GET_BUGS)
			ENDIF
		ENDIF
		
		
		////////////////////////////////////////////////////////////////////////////////////////////////
		///     
		///     UPDATE ITEM SELECTION
		///     
		IF NOT IS_DEBUG_MENU_PROCESS_FLAG_SET(sDebugMenuData.iProcessFlags, DM_PROCESS_WAIT)
			CHECK_DEBUG_MENU_NAVIGATION(sDebugMenuData)
			CALL sDebugMenuData.fpCheckSelection(sDebugMenuData)
		ENDIF
		
		
		////////////////////////////////////////////////////////////////////////////////////////////////
		///     
		///     RENDER MENU
		///     
		IF NOT IS_DEBUG_MENU_PROCESS_FLAG_SET(sDebugMenuData.iProcessFlags, DM_PROCESS_CLEANUP)
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISPLAY_DEBUG_MENU(sDebugMenuData)
			g_debugMenuControl.bDebugMenuOnScreen = TRUE
			
			// Block keys so we do not interfere with game
			DISABLE_CONTROL_BUTTON(PLAYER_CONTROL, RIGHTSTICKX)
			DISABLE_CONTROL_BUTTON(PLAYER_CONTROL, RIGHTSTICKY)
			DISABLE_CONTROL_BUTTON(PLAYER_CONTROL, LEFTSTICKX)
			DISABLE_CONTROL_BUTTON(PLAYER_CONTROL, LEFTSTICKY)
			DISABLE_CONTROL_BUTTON(PLAYER_CONTROL, DPADUP)
			DISABLE_CONTROL_BUTTON(PLAYER_CONTROL, DPADDOWN)
			DISABLE_CONTROL_BUTTON(PLAYER_CONTROL, DPADLEFT)
			DISABLE_CONTROL_BUTTON(PLAYER_CONTROL, DPADRIGHT)
			DISABLE_CONTROL_BUTTON(PLAYER_CONTROL, TRIANGLE)
			DISABLE_CONTROL_BUTTON(PLAYER_CONTROL, SQUARE)
			DISABLE_CONTROL_BUTTON(PLAYER_CONTROL, CIRCLE)
			DISABLE_CONTROL_BUTTON(PLAYER_CONTROL, CROSS)
			DISABLE_CONTROL_BUTTON(PLAYER_CONTROL, LEFTSHOULDER1)
			DISABLE_CONTROL_BUTTON(PLAYER_CONTROL, LEFTSHOULDER2)
			DISABLE_CONTROL_BUTTON(PLAYER_CONTROL, RIGHTSHOULDER1)
			DISABLE_CONTROL_BUTTON(PLAYER_CONTROL, RIGHTSHOULDER2)
			DISABLE_CONTROL_BUTTON(PLAYER_CONTROL, LEFTSHOCK)
			DISABLE_CONTROL_BUTTON(PLAYER_CONTROL, RIGHTSHOCK)
		ENDIF
	ELSE
		g_debugMenuControl.bDebugMenuOnScreen = FALSE
	ENDIF
		
ENDPROC

#ENDIF
