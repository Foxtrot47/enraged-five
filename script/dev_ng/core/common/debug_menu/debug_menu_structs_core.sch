// Debug Menu Structs, Consts, and Enums
USING "debug_menu_structs_GAME.sch"
USING "script_maths.sch"

CONST_INT MAX_DEBUG_MENU_SELECTIONS_TO_TRACK 	4
CONST_INT MAX_ALT_CODE_IDS						4
CONST_INT MAX_EXTRA_LABEL_IDS					2


// CORE DM_PROCESS BIT CONSTS
// Additional DM_PROCESS bits can be added in debug_menu_structs_GAME.sch
ENUM DEBUG_MENU_PROCESS_FLAGS
	DM_PROCESS_NONE						= 0,
	DM_PROCESS_KEYS_PRESSED				= BIT0,
	DM_PROCESS_LAUNCH					= BIT1,
	DM_PROCESS_WAIT						= BIT2,
	DM_PROCESS_PRE_INITIALISED			= BIT3,
	DM_PROCESS_INITIALISED				= BIT4,
	DM_PROCESS_INITIALISE_NEXT_MENU		= BIT5,
	DM_PROCESS_RESTORE_SELECTION		= BIT6,
	DM_PROCESS_UPDATE_MENU_ITEM			= BIT7,
	DM_PROCESS_CLEANUP					= BIT8,
	DM_PROCESS_BUGSTAR_GET_BUGS			= BIT9,
	DM_PROCESS_BUGSTAR_GET_BASIC		= BIT10,
	DM_PROCESS_BUGSTAR_GET_DETAILED		= BIT11,
	DM_PROCESS_SELECTION				= BIT12,
	DM_PROCESS_CHANGE_MODES				= BIT13,
	DM_PROCESS_RESTORE_SORT				= BIT14
ENDENUM

#IF NOT DEFINED(SP_MENU_CODE_ID_ENUM)
/// Project-specific codeID enumeration. Override this!
HASH_ENUM SP_MENU_CODE_ID_ENUM
	SPCID_DEFAULT = 0
ENDENUM
#ENDIF

#IF NOT DEFINED(DEBUG_MENU_CUSTOM_PROCESS_FLAGS)
/// Project-specific process flags enumeration. Override this!
ENUM DEBUG_MENU_CUSTOM_PROCESS_FLAGS
	DM_CUSTOM_PROCESS_NONE					= 0
ENDENUM
#ENDIF

ENUM DM_ITEM_FLAGS
	DM_ITEM_NONE			= 0,
	DM_BIT_DISPLAY			= BIT0,
	DM_BIT_ENABLED			= BIT1,
	DM_BIT_HIGHLIGHT		= BIT2,
	DM_BIT_VEHICLE			= BIT3,
	DM_BIT_BLIPCOORDS		= BIT4,
	DM_BIT_KEEP_VEH			= BIT5,
	DM_BIT_BUG_INFO			= BIT6,
	DM_BIT_LAUNCH_SCRIPT	= BIT7
ENDENUM


CONST_FLOAT	DM_NO_HEADING	-1.0		// Set a heading variable to this if heading not required
CONST_FLOAT DM_MIN_HEADING	 0.0		// If heading is less than this then it is not required - this is because of the inaccuracies of floats
CONST_FLOAT	DM_NO_VECTOR	-10001.0	// Set vector X to this if vector not required
CONST_FLOAT	DM_MIN_VECTOR	-10000.0	// If vector X is less than this then vector is not required

CONST_INT DM_MAX_DEPTH 3
CONST_INT DM_MAX_ITEMS_PER_DEPTH 60
CONST_INT DM_MAX_ITEMS_IN_COLUMN 12
CONST_INT DM_INPUT_CHECK_DELAY_msec 130

// HASH KEYS
CONST_INT DM_HASH_DEBUG_MENU_ITEMS 	764709632 			// "DebugMenuItems"
CONST_INT DM_HASH_DEBUG_MENU_TITLE  -1072139730			// "menuTitle"
CONST_INT DM_HASH_BUGSTAR_FILE  	-1733180618 		// "bugstarFile"

CONST_INT DM_HASH_ITEM_MENU 		-802149892 			// "menuItem"
CONST_INT DM_HASH_ITEM_INPUT		1843263079 			// "inputItem"
CONST_INT DM_HASH_ITEM_MISSION 		-183891836 			// "missionItem"
CONST_INT DM_HASH_ITEM_LAUNCHER		721938929 			// "launcherItem"
CONST_INT DM_HASH_ITEM_TOOL 		-2110549717 		// "toolItem"
CONST_INT DM_HASH_ITEM_SCRIPT 		1026899739 			// "scriptItem"
CONST_INT DM_HASH_ITEM_WARP 		-1351736241			// "warpItem"
CONST_INT DM_HASH_ITEM_CODE 		-1678617402			// "codeItem"
CONST_INT DM_HASH_ITEM_BREAK		-2125444006			// "breakItem"
CONST_INT DM_HASH_ITEM_BUGSTAR		-2110356570 		// "bugstarItem"

CONST_INT DM_HASH_POSX 				-1828477467 		// "x"
CONST_INT DM_HASH_POSY 				-2137718520 		// "y"
CONST_INT DM_HASH_POSZ 				-1235194722 		// "z"
CONST_INT DM_HASH_HEADING			1140182852 			// "heading"
CONST_INT DM_HASH_POSX1				1957158439 			// "x1"
CONST_INT DM_HASH_POSY1				2133096064 			// "y1"
CONST_INT DM_HASH_POSZ1				-1595984863 		// "z1"
CONST_INT DM_HASH_HEADING1			-298694592 			// "heading1"
CONST_INT DM_HASH_POSX2				378413553 			// "x2"
CONST_INT DM_HASH_POSY2				1843647487			// "y2"
CONST_INT DM_HASH_POSZ2				1803143511			// "z2"
CONST_INT DM_HASH_HEADING2			-1661557306 		// "heading2"

CONST_INT DM_HASH_VEHICLE 			-1107692287			// "vehicleWarp"
CONST_INT DM_HASH_KEEP_VEHICLE 		-126308560  		// "keepVehicle"
CONST_INT DM_HASH_FILE 				-1301001980 		// "file"
CONST_INT DM_HASH_TITLE 			1923799069 			// "title"
CONST_INT DM_HASH_TITLE_EXTRA		-953544247 			// "titleExtra"
CONST_INT DM_HASH_INFO				1745727664			// "info"
CONST_INT DM_HASH_EXTRA_INFO		-1611837690			// "extraInfo"
CONST_INT DM_HASH_CODE_ID 			580355546			// "codeID"
CONST_INT DM_HASH_CODE_ID_PRELAUNCH	-1121022949			// "preCodeID"
CONST_INT DM_HASH_CODE_ID_A_1 		-153540071			// "codeID1"
CONST_INT DM_HASH_CODE_ID_A_2 		-1797298629			// "codeID2"
CONST_INT DM_HASH_CODE_ID_A_3 		-1079067687			// "codeID3"
CONST_INT DM_HASH_CODE_ID_A_4 		2014817452			// "codeID4"
CONST_INT DM_HASH_LABEL_ID 			HASH("labelID")		// "labelID"
CONST_INT DM_HASH_LABEL_ID_2 		HASH("labelID2")	// "labelID2"
CONST_INT DM_HASH_LABEL_ID_3 		HASH("labelID3")	// "labelID3"
CONST_INT DM_HASH_DISPLAY_ID 		1005845876			// "displayID"
CONST_INT DM_HASH_ENABLE_ID 		287639646 			// "enableID"
CONST_INT DM_HASH_INTERIOR 			560567274			// "interior"
CONST_INT DM_HASH_CLOCK_HOUR		1605056896			// "hour"
//CONST_INT DM_HASH_RE_FOLLOWUP		-1194668480 		// "isFollowup"
CONST_INT DM_HASH_LAUNCH_SCRIPT		-784851915 			// "launchScript"
CONST_INT DM_HASH_MISSION_ID 		-2008381158			// "missionID"
CONST_INT DM_HASH_MISSION_NAME 		891101480			// "missionName"
CONST_INT DM_HASH_MISSION_STORY 	-691977066			// "description"
CONST_INT DM_HASH_MISSION_DESC		-1368482782			// "story"
CONST_INT DM_HASH_MISSION_COMMENTS	816964678			// "comments"
CONST_INT DM_HASH_AUTHOR 			-319118299 			// "author"
CONST_INT DM_HASH_PERCENTAGE 		-1347409643 		// "percentage"
CONST_INT DM_HASH_IMPORTANT_BUGS	HASH("lesBugs")		// 'lesBugs'
CONST_INT DM_HASH_HIGHLIGHT 		-1528702738 		// "highlight"
CONST_INT DM_HASH_CHARACTER 		1328661203  		// "character"
CONST_INT DM_HASH_BLIPCOORDS 		-2048047736 		// "useBlipCoords"

CONST_INT DM_HASH_TEAM_INFO 		945690028			// "teamInfo"
CONST_INT DM_HASH_TEAM_ID 			459292749			// "id"
CONST_INT DM_HASH_BUG_NUM 			1587604470 			// "noBugs"
CONST_INT DM_HASH_TOP5_OWNERS 		-1477265357			// "topBugOwners"
CONST_INT DM_HASH_BUG_OWNER 		-1945763472 		// "owner"
CONST_INT DM_HASH_FIXEDBUGS 		1740851653			// "fixedBugs"
CONST_INT DM_HASH_AVG_BUG_AGE		724077757 			// "avgBugAge"
CONST_INT DM_HASH_OVERDUEBUGS		-1685906165 		// "overdueBugs"
CONST_INT DM_HASH_A_BUGS			2112632024 			// "aBugs"
CONST_INT DM_HASH_B_BUGS			-929476018 			// "bBugs"
CONST_INT DM_HASH_C_BUGS			-35449893 			// "cBugs"
CONST_INT DM_HASH_D_BUGS			171212816 			// "dBugs"
CONST_INT DM_HASH_TASK_BUGS			-1353457967 		// "tasks"
CONST_INT DM_HASH_TODO_BUGS			371507088 			// "todos"
CONST_INT DM_HASH_WISH_BUGS			-503646438 			// "wish"

CONST_INT DM_HASH_BUG_MISSION		-1950263389			// "mission"
CONST_INT DM_HASH_BUG_ID			459292749			// "id"
CONST_INT DM_HASH_BUG_BUGSTAR_ID	388889380			// "bugstarMissionId"
CONST_INT DM_HASH_BUG				-209943193			// "bug"
CONST_INT DM_HASH_BUG_SUMMARY		760118442			// "summary"
CONST_INT DM_HASH_BUG_DEV			980181675			// "developer"
CONST_INT DM_HASH_BUG_CLASS			2052871693			// "category"
CONST_INT DM_HASH_BUG_DUE			-671817761			// "dueDate"
CONST_INT DM_HASH_BUG_CREATED		5237026				// "createdOn"

CONST_INT DM_HASH_CURRENT_DATE		1332810250	// "currentDate"

CONST_INT DM_HASH_SCRIPT_KEY 		-274776638

//Used by MP debug menu
CONST_INT DM_HASH_MISSION_RANK		-518474626 // "rank"

// COLORS!
///	Menu background colors are all project-specific. By default they are black/grey. Override these in your GAME structs to change.
#IF NOT DEFINED(MENU_BG_R)
CONST_INT	MENU_BG_R	0
#ENDIF
#IF NOT DEFINED(MENU_BG_G)
CONST_INT	MENU_BG_G	0
#ENDIF
#IF NOT DEFINED(MENU_BG_B)
CONST_INT	MENU_BG_B	0
#ENDIF

#IF NOT DEFINED(MENU_BG_EDGE_R)
CONST_INT	MENU_BG_EDGE_R	50
#ENDIF
#IF NOT DEFINED(MENU_BG_EDGE_G)
CONST_INT	MENU_BG_EDGE_G	50
#ENDIF
#IF NOT DEFINED(MENU_BG_EDGE_B)
CONST_INT	MENU_BG_EDGE_B	50
#ENDIF

ENUM DEBUG_MENU_SORT_ENUM
	DM_SORT_DEFAULT = 0,
	DM_SORT_PERCENTAGE_ASC,
	DM_SORT_PERCENTAGE_DESC,
	DM_SORT_BUGSTAR_ASC,
	DM_SORT_BUGSTAR_DESC,
	
	DM_MAX_NUMBER_OF_SORT_ENUMS
ENDENUM

STRUCT DEBUG_MENU_ITEM_STRUCT
	TEXT_LABEL_63	sTitle								// Current selection will highlight this.
	TEXT_LABEL_15	sTitleExtra 						// An extra bit of text that we display at the end of the title in brackets.
	TEXT_LABEL_63	sFile								// The xml or sc file we need to load.
	TEXT_LABEL_15	sAuth								// Scripter initials.
	TEXT_LABEL_15	sInfo								// Some text to give the user some info.
	TEXT_LABEL_15	sExtraInfo							// Some text to give the user some info.
	TEXT_LABEL_15	sMissionID							// GT1, GT2, Y1, Y2 etc.
	TEXT_LABEL_31	sInterior 							// The room type we are warping to.
	INT				iUniqueID							// Used for restoring last menu selection.
	INT				iType								// menuItem, scriptItem, warpItem, codeItem .
	INT				iCodeID								// The piece of script to run post gameflow launch.
	INT				iCodeIDPreLaunch					// The piece of script to run pre gameflow launch.
	INT				iAltCodeID[MAX_ALT_CODE_IDS]		// The pieces of script we should run with the alt button is selected.
	INT				iLabelID							// A gameflow label to skip the flow to when running this menu item.
	INT				iExtraLabelID[MAX_EXTRA_LABEL_IDS]	// Extra gameflow labels to also skip the flow to with a multi label launch.
	INT 			iDisplayID							// The piece of script we should run to check if the item should be displayed.
	INT 			iEnableID							// The piece of script we should run to check if the item is enabled.
	INT				iPerc								// Percentage complete.
	INT				iImportantBugs						// Number of important bugs (Les bugs for the GTAV project)
	INT				iCharacter							// The character we should switch to.
	DM_ITEM_FLAGS 	iFlags 								// Set bits: Display[0], Enabled[1], Highlight[2], Vehicle[3], BlipCoords[4]
	VECTOR 			vCoord[3]							// Where the player should warp to.
	FLOAT 			fHeading[3]							// Where the player should face.
	INT 			iRank 								// Used by MP rank of item.
	INT 			iHour 								// Sets the clock hour.
ENDSTRUCT


FORWARD STRUCT DEBUG_MENU_DATA_STRUCT

// Prototypes for mode-specific debug menu functions
TYPEDEF FUNC BOOL DebugMenuRunCodeID(DEBUG_MENU_DATA_STRUCT &sDebugMenuData)
TYPEDEF FUNC BOOL DebugMenuRunCodeIDPreLaunch(DEBUG_MENU_DATA_STRUCT &sDebugMenuData)
TYPEDEF PROC DebugMenuRunDisplayID(DEBUG_MENU_DATA_STRUCT &sDebugMenuData, INT iDepth)
TYPEDEF PROC DebugMenuRunEnableID(DEBUG_MENU_DATA_STRUCT &sDebugMenuData, INT iDepth, BOOL bForceDisabled = FALSE)
TYPEDEF FUNC BOOL DebugMenuProcessSelection(DEBUG_MENU_DATA_STRUCT &sDebugMenuData)
TYPEDEF PROC DebugMenuResetData(DEBUG_MENU_DATA_STRUCT &sDebugMenuData)
TYPEDEF PROC DebugMenuCleanup(DEBUG_MENU_DATA_STRUCT &sDebugMenuData)
TYPEDEF PROC DebugMenuCheckSelection(DEBUG_MENU_DATA_STRUCT &sDebugMenuData)
TYPEDEF PROC DebugMenuLaunchScript(DEBUG_MENU_DATA_STRUCT &sDebugMenuData)
TYPEDEF PROC DebugMenuWarpPlayer(DEBUG_MENU_DATA_STRUCT &sDebugMenuData, BOOL paramKeepVehicle = FALSE)

STRUCT DEBUG_MENU_DATA_STRUCT
	STRING					sXMLFile

	DEBUG_MENU_ITEM_STRUCT 	sItems[DM_MAX_DEPTH][DM_MAX_ITEMS_PER_DEPTH] // All the menu items
	BUGSTAR_DATA_STRUCT 	sBugstarData				// Details bugstar information for the current item
	MISSION_BUGS_STRUCT		sBugData					// List of bugs for this mission
	DEBUG_MENU_SORT_ENUM	eMissionSort				// The order we should display the mission items
	TEXT_LABEL_15			sMenuTitle					// Menu title - SP/MP/Demo etc
	
	INT 					iItemCount[DM_MAX_DEPTH]	// Current number if items in menu
	INT						iTopItem[DM_MAX_DEPTH]		// Keeps track of the top menu item
	INT						iUnderItem[DM_MAX_DEPTH]	// Keeps track of the bottom menu item+1
	INT 					iCurrentItem[DM_MAX_DEPTH]	// Keeps track of menu item for each depth
	INT 					iCurrentDepth				// Keeps track of menu depth
	
	INT  					iAllowInputCheckTimer		// Timer for input check
	BOOL					bMouseMoveUp				// Flag to tell us if the mouse has been used to move up
	BOOL					bMouseMoveDown 				// Flag to tell us if the mouse has been used to move down
	
	INT						iAltWindowTimer
	BOOL					bDrawAltWindow
	INT						iCurrentAltItem

	
	DEBUG_MENU_PROCESS_FLAGS iProcessFlags
	DEBUG_MENU_CUSTOM_PROCESS_FLAGS	iCustomProcessFlags
	
	// These function pointers are re-assigned depending on what mode we are in, since we have different menus for SP and MP
	DebugMenuRunCodeID      	fpRunCodeID
	DebugMenuRunCodeIDPreLaunch fpRunCodeIDPreLaunch
	DebugMenuRunDisplayID   	fpRunDisplayID
	DebugMenuRunEnableID    	fpRunEnableID
	DebugMenuProcessSelection   fpProcessSelection
	DebugMenuResetData      	fpResetData
	DebugMenuCleanup        	fpCleanup
	DebugMenuCheckSelection 	fpCheckSelection
	DebugMenuLaunchScript   	fpLaunchScript
	DebugMenuWarpPlayer     	fpWarpPlayer
ENDSTRUCT

STRUCT DEBUG_MENU_SELECTION_STRUCT
	TEXT_LABEL_63 sLabel	// Filename or title
	INT iTimeStamp			// Time the menu item was selected
ENDSTRUCT


STRUCT DEBUG_MENU_CONTROL

	BOOL bDebugMenuOnScreen

	BOOL bUseBugstarQuery
	INT iBugstarQueryRenew

	DEBUG_MENU_SELECTION_STRUCT sDMSelections[MAX_DEBUG_MENU_SELECTIONS_TO_TRACK]
	INT iDMSelectionSlot
	INT iDMCurrentDepth
	INT iDMSortType
	INT iDMTopItem[3]
	INT iDMUnderItem[3]
	INT iDMCurrentItem[3]
	INT iDMTitleHash[3]
	INT iDMUniqueID[3]
	BOOL bDMSelectionRestored[3]
	
	FLOAT fTempPos[5]

ENDSTRUCT

//=== FLAG ACCESSORS AND MUTATORS

PROC SET_DEBUG_MENU_PROCESS_FLAG(DEBUG_MENU_PROCESS_FLAGS &eProcessFlags, DEBUG_MENU_PROCESS_FLAGS eFlagToSet)
	eProcessFlags = eProcessFlags | eFlagToSet
ENDPROC

PROC CLEAR_DEBUG_MENU_PROCESS_FLAG(DEBUG_MENU_PROCESS_FLAGS &eProcessFlags, DEBUG_MENU_PROCESS_FLAGS eFlagToClear)
	eProcessFlags -= eProcessFlags & eFlagToClear
ENDPROC

FUNC BOOL IS_DEBUG_MENU_PROCESS_FLAG_SET(DEBUG_MENU_PROCESS_FLAGS eProcessFlags, DEBUG_MENU_PROCESS_FLAGS eFlagToCheck)
	RETURN (eProcessFlags & eFlagToCheck) != DM_PROCESS_NONE
ENDFUNC

PROC SET_DEBUG_MENU_ITEM_FLAG(DM_ITEM_FLAGS &eItemFlags, DM_ITEM_FLAGS eFlagToSet)
	eItemFlags = eItemFlags | eFlagToSet
ENDPROC

PROC CLEAR_DEBUG_MENU_ITEM_FLAG(DM_ITEM_FLAGS &eItemFlags, DM_ITEM_FLAGS eFlagToClear)
	eItemFlags -= eItemFlags & eFlagToClear
ENDPROC

FUNC BOOL IS_DEBUG_MENU_ITEM_FLAG_SET(DM_ITEM_FLAGS eItemFlags, DM_ITEM_FLAGS eFlagToCheck)
	RETURN (eItemFlags & eFlagToCheck) != DM_ITEM_NONE
ENDFUNC

//=== FLAG ACCESSORS AND MUTATORS

PROC SET_DEBUG_MENU_CUSTOM_PROCESS_FLAG(DEBUG_MENU_CUSTOM_PROCESS_FLAGS &eCustomProcessFlags, DEBUG_MENU_CUSTOM_PROCESS_FLAGS eFlagToSet)
	eCustomProcessFlags = eCustomProcessFlags | eFlagToSet
ENDPROC

PROC CLEAR_DEBUG_MENU_CUSTOM_PROCESS_FLAG(DEBUG_MENU_CUSTOM_PROCESS_FLAGS &eCustomProcessFlags, DEBUG_MENU_CUSTOM_PROCESS_FLAGS eFlagToClear)
	eCustomProcessFlags -= eCustomProcessFlags & eFlagToClear
ENDPROC

FUNC BOOL IS_DEBUG_MENU_CUSTOM_PROCESS_FLAG_SET(DEBUG_MENU_CUSTOM_PROCESS_FLAGS eCustomProcessFlags, DEBUG_MENU_CUSTOM_PROCESS_FLAGS eFlagToCheck)
	RETURN (eCustomProcessFlags & eFlagToCheck) != DM_CUSTOM_PROCESS_NONE
ENDFUNC
