USING "flow_structs_core.sch"
USING "flow_diagram_GAME.sch"

#IF NOT DEFINED(GET_STRAND_XML_FILENAME)
FUNC STRING GET_STRAND_XML_FILENAME(STRANDS eStrand)
	eStrand = eStrand
	RETURN ""
ENDFUNC
#ENDIF

#IF NOT DEFINED(LOAD_FLOW_DIAGRAM_DATA_FROM_XML_CUSTOM)
PROC LOAD_FLOW_DIAGRAM_DATA_FROM_XML_CUSTOM(FLOW_DIAGRAM &sFlowDiagram, INT iAttributeIndex)
	sFlowDiagram.iElementCount = sFlowDiagram.iElementCount
	iAttributeIndex = iAttributeIndex
ENDPROC
#ENDIF

#IF NOT DEFINED(FLOW_DIAGRAM_INITIALIZE_STRANDS)
PROC FLOW_DIAGRAM_INITIALIZE_STRANDS(FLOW_DIAGRAM &sFlowDiagram, BOOL &bActiveStrands[], INT iCurrentXPos)
	sFlowDiagram.iElementCount = sFlowDiagram.iElementCount
	bActiveStrands[0] = bActiveStrands[0]
	iCurrentXPos = iCurrentXPos
ENDPROC
#ENDIF

#IF NOT DEFINED(FLOW_DIAGRAM_IS_MISSION_COMPLETE)
FUNC BOOL FLOW_DIAGRAM_IS_MISSION_COMPLETE(TEXT_LABEL_63 missionName)
	CERRORLN(DEBUG_FLOW_DIAGRAM,  missionName )
	RETURN FALSE
ENDFUNC
#ENDIF

#IF NOT DEFINED(ACTIVATE_FLOW_DIAGRAM_CUSTOM)
PROC ACTIVATE_FLOW_DIAGRAM_CUSTOM(FLOW_DIAGRAM &sFlowDiagram)
	sFlowDiagram.iElementCount = sFlowDiagram.iElementCount
ENDPROC
#ENDIF


#IF NOT DEFINED(DEACTIVATE_FLOW_DIAGRAM_CUSTOM)
PROC DEACTIVATE_FLOW_DIAGRAM_CUSTOM(FLOW_DIAGRAM &sFlowDiagram)
	sFlowDiagram.iElementCount = sFlowDiagram.iElementCount
ENDPROC
#ENDIF

#IF NOT DEFINED(FLOW_DIAGRAM_LAUNCH_MISSION)
PROC FLOW_DIAGRAM_LAUNCH_MISSION(FLOW_DIAGRAM &sFlowDiagram, FLOW_LAUNCHER_VARS &flowLauncher)
	sFlowDiagram.iElementCount = sFlowDiagram.iElementCount
	flowLauncher.bLauncherRunMultipleLabelLaunch = flowLauncher.bLauncherRunMultipleLabelLaunch
ENDPROC
#ENDIF

#IF NOT DEFINED(STORE_FLOW_DIAGRAM_CUSTOM_ELEMENT)
FUNC BOOL STORE_FLOW_DIAGRAM_CUSTOM_ELEMENT(FLOW_DIAGRAM &sFlowDiagram)
	sFlowDiagram.iElementCount = sFlowDiagram.iElementCount
	RETURN FALSE
ENDFUNC
#ENDIF

#IF NOT DEFINED(DISPLAY_FLOW_DIAGRAM_MISSION_ELEMENT_CUSTOM)
PROC DISPLAY_FLOW_DIAGRAM_MISSION_ELEMENT_CUSTOM(FLOW_DIAGRAM &sFlowDiagram, FLOW_DIAGRAM_ELEMENT &sMissionElement, FLOW_DIAGRAM_ELEMENT_CUSTOM &sMissionElementCustom, FLOAT fXCoord, FLOAT fYCoord)
	sFlowDiagram.iElementCount = sFlowDiagram.iElementCount
	sMissionElement.completed = sMissionElement.completed
	sMissionElementCustom.bTemp = sMissionElementCustom.bTemp
	fXCoord = fXCoord
	fYCoord = fYCoord
ENDPROC
#ENDIF

#IF NOT DEFINED(SET_POSITION_OF_SECOND_PASS_DIAGRAM_ELEMENT_CUSTOM)
PROC SET_POSITION_OF_SECOND_PASS_DIAGRAM_ELEMENT_CUSTOM(FLOW_DIAGRAM &sFlowDiagram,
													INT iXPos, 
													FLOW_DIAGRAM_ELEMENT &sElement, 
													INT &iStrandCurrentElementIndex[MAX_STRANDS],
													INT &iReservedBounds[2])
													
	sFlowDiagram.iElementCount = sFlowDiagram.iElementCount
	iXPos = iXPos
	sElement.completed = sElement.completed
	iStrandCurrentElementIndex[0] = iStrandCurrentElementIndex[0]
	iReservedBounds[0] = iReservedBounds[0]
ENDPROC
#ENDIF

#IF NOT DEFINED(GET_MISSION_ID_FROM_DIAGRAM_ELEMENT)
FUNC TEXT_LABEL_7 GET_MISSION_ID_FROM_DIAGRAM_ELEMENT(FLOW_DIAGRAM_ELEMENT &sElement)
	CERRORLN(DEBUG_FLOW_DIAGRAM, "Using default GET_MISSION_ID_FROM_DIAGRAM_ELEMENT. This must be overridden for the flow diagram to work properly")
	sElement.iPerc = sElement.iPerc
	TEXT_LABEL_7 retVal = "XXXX"
	RETURN retVal
ENDFUNC
#ENDIF

#IF NOT DEFINED(DOES_FLOW_DIAGRAM_ELEMENT_GET_BUGSTAR_QUERY_CUSTOM)
FUNC BOOL DOES_FLOW_DIAGRAM_ELEMENT_GET_BUGSTAR_QUERY_CUSTOM(FLOW_DIAGRAM_ELEMENT_TYPE elementType)
	elementType = elementType
	RETURN FALSE
ENDFUNC
#ENDIF

#IF NOT DEFINED(DOES_FLOW_DIAGRAM_ELEMENT_GET_BUGSTAR_QUERY_CUSTOM)
PROC DISPLAY_DIAGRAM_ELEMENT_CUSTOM(FLOW_DIAGRAM &sFlowDiagram, FLOW_DIAGRAM_ELEMENT &sElement, FLOW_DIAGRAM_ELEMENT_CUSTOM &sElementCustom, FLOAT fXCoord, INT elementIndex, BOOL &bHighlighted)
	sFlowDiagram.iElementCount = sFlowDiagram.iElementCount
	sElement.iPerc = sElement.iPerc
	sElementCustom.bTemp = sElementCustom.bTemp
	fXCoord = fXCoord
	elementIndex = elementIndex
	bHighlighted = bHighlighted
ENDPROC
#ENDIF

#IF NOT DEFINED(DISPLAY_DIAGRAM_ELEMENT_CUSTOM)
PROC DISPLAY_DIAGRAM_ELEMENT_CUSTOM(FLOW_DIAGRAM &sFlowDiagram, FLOW_DIAGRAM_ELEMENT &sElement, FLOW_DIAGRAM_ELEMENT_CUSTOM &sElementCustom, FLOAT fXCoord, INT elementIndex, BOOL &bHighlighted)
	sFlowDiagram.iElementCount = sFlowDiagram.iElementCount
	sElement.iPerc = sElement.iPerc
	sElementCustom.bTemp = sElementCustom.bTemp
	fXCoord = fXCoord
	elementIndex = elementIndex
	bHighlighted = bHighlighted
ENDPROC
#ENDIF

 
