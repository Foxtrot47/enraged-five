
//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ Constants  ╞═════════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛
USING "commands_pad.sch"
USING "flow_diagram_structs_GAME.sch"
USING "flow_structs_core.sch"
USING "bugstar_globals.sch"
USING "debug_menu_structs_core.sch"

#IF NOT DEFINED(FLOW_INFO_PERC_RED_THRESHOLD)
CONST_INT	FLOW_INFO_PERC_RED_THRESHOLD	50		// Missions with a lower percentage than this are colored red
#ENDIF
#IF NOT DEFINED(FLOW_INFO_PERC_YELLOW_THRESHOLD)
CONST_INT	FLOW_INFO_PERC_YELLOW_THRESHOLD	70		// Missions with a lower percentage than this are colored yellow
#ENDIF
#IF NOT DEFINED(FLOW_INFO_PERC_GREEN_THRESHOLD)
CONST_INT	FLOW_INFO_PERC_GREEN_THRESHOLD	100		// Missions with a lower percentage than this are colored green
#ENDIF
						

#IF NOT DEFINED(FLOW_INFO_BUGS_RED_THRESHOLD)
CONST_INT	FLOW_INFO_BUGS_RED_THRESHOLD	40		// Missions with more bugs than this are colored red
#ENDIF
#IF NOT DEFINED(FLOW_INFO_BUGS_YELLOW_THRESHOLD)
CONST_INT	FLOW_INFO_BUGS_YELLOW_THRESHOLD	20		// Missions with more bugs than this are colored yellow
#ENDIF
#IF NOT DEFINED(FLOW_INFO_BUGS_GREEN_THRESHOLD)
CONST_INT	FLOW_INFO_BUGS_GREEN_THRESHOLD	0		// Missions with more bugs than this are colored green
#ENDIF

#IF NOT DEFINED(MAX_FLOW_DIAGRAM_SYNC_LINKS)
CONST_INT MAX_FLOW_DIAGRAM_SYNC_LINKS 15
#ENDIF

//Analogue stick indexes.
CONST_INT	LEFT_X		0
CONST_INT	LEFT_Y		1
CONST_INT	RIGHT_X		2
CONST_INT	RIGHT_Y		3

ENUM FLOW_DIAGRAM_BUTTONS
//Button indexes.
	FD_BUTTON_SELECT 		= CROSS,
	FD_BUTTON_KEY			= SQUARE,
	FD_BUTTON_EXIT 			= CIRCLE
ENDENUM

//Scrolling rates.
CONST_FLOAT FD_X_SCROLL_RATE				0.5
CONST_FLOAT FD_Y_SCROLL_RATE				0.5
CONST_FLOAT FD_ZOOM_RATE					0.045
CONST_FLOAT FD_INPUT_INTERP_RATE			0.25
CONST_FLOAT FD_ALPHA_INTERP_RATE			0.45

//Diagram limits.
CONST_FLOAT FD_MIN_ZOOM						0.24
CONST_FLOAT FD_MAX_ZOOM						12.0
CONST_FLOAT FD_VIEW_SWITCH_ZOOM				0.7
CONST_FLOAT FD_BUMP_SCROLLING_EDGE_START	0.15

//Text config.
CONST_FLOAT	FD_TEXT_SCALE_X					0.25
CONST_FLOAT	FD_TEXT_SCALE_Y					0.3
CONST_FLOAT	FD_TEXT_WRAP_XPOS_START			0.0
CONST_FLOAT	FD_TEXT_WRAP_XPOS_END			1.0

//Element spacing.
CONST_FLOAT FD_ELEMENT_X_SPACING			0.028

//Element dimensions.
CONST_FLOAT FD_MISSION_ELEMENT_WIDTH		0.022
CONST_FLOAT FD_MISSION_ELEMENT_HEIGHT		0.028
CONST_FLOAT FD_PLANNING_ELEMENT_WIDTH		0.021
CONST_FLOAT FD_PLANNING_ELEMENT_HEIGHT		0.018
CONST_FLOAT FD_PHONE_ELEMENT_WIDTH			0.015
CONST_FLOAT FD_PHONE_ELEMENT_HEIGHT			0.018
CONST_FLOAT FD_LINK_ELEMENT_WIDTH			0.008
CONST_FLOAT FD_LINK_ELEMENT_HEIGHT			0.012

//Element colours.
CONST_INT	FD_COL_R_SYNC					230 
CONST_INT	FD_COL_G_SYNC					230
CONST_INT	FD_COL_B_SYNC					230

CONST_INT	FD_COL_R_PLANNING				10
CONST_INT	FD_COL_G_PLANNING				10
CONST_INT	FD_COL_B_PLANNING				170

CONST_INT	FD_COL_R_PHONE					170
CONST_INT	FD_COL_G_PHONE					0
CONST_INT	FD_COL_B_PHONE					170

CONST_INT	FD_COL_R_LINE_FLOW				80
CONST_INT	FD_COL_G_LINE_FLOW				80
CONST_INT	FD_COL_B_LINE_FLOW				80

CONST_INT	FD_COL_R_LINE_BG				50
CONST_INT	FD_COL_G_LINE_BG				50
CONST_INT	FD_COL_B_LINE_BG				50

CONST_INT	FD_COL_R_TEXT_MAIN				255
CONST_INT	FD_COL_G_TEXT_MAIN				255
CONST_INT	FD_COL_B_TEXT_MAIN				255

CONST_INT	FD_COL_R_TEXT_TITLE				100
CONST_INT	FD_COL_G_TEXT_TITLE				100
CONST_INT	FD_COL_B_TEXT_TITLE				100

CONST_INT	FD_COL_R_TEXT_HELP				200
CONST_INT	FD_COL_G_TEXT_HELP				200
CONST_INT	FD_COL_B_TEXT_HELP				200

CONST_INT	FD_COL_R_TEXT_ELEMENT			0
CONST_INT	FD_COL_G_TEXT_ELEMENT			0
CONST_INT	FD_COL_B_TEXT_ELEMENT			0

//The maximum number of diagram elements a diagram can hold.
//Changing this significantly alters the stack size an instantiated
//diagram will need!
#IF NOT DEFINED(FD_MAX_ELEMENTS)
CONST_INT					FD_MAX_ELEMENTS				211
#ENDIF

#IF NOT DEFINED(FD_MAX_MISSIONS_PER_QUERY)
CONST_INT					FD_MAX_MISSIONS_PER_QUERY	20
#ENDIF
//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ Variables  ╞═════════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

// Element types for the flow diagram.
ENUM FLOW_DIAGRAM_ELEMENT_TYPE
	FD_ELEMENT_TYPE_ERROR = -1,
	FD_ELEMENT_TYPE_ACTIVATE	= HASH("activate"),
	FD_ELEMENT_TYPE_AWAIT_SYNC	= HASH("awaitSync"),
	FD_ELEMENT_TYPE_MISSION		= HASH("mission"),
	FD_ELEMENT_TYPE_SEND_SYNC	= HASH("sendSync"),
	
	USING "flow_diagram_types_GAME.sch"
	
	FD_ELEMENT_MAX
ENDENUM

ENUM FLOW_DIAGRAM_ATTRIBUTE
	FD_ATTR_NAME	= HASH("name"),
	FD_ATTR_SYNCID	= HASH("syncID"),
	FD_ATTR_STRAND	= HASH("strand")
ENDENUM

// Colour modes for the flow diagram.
ENUM FLOW_DIAGRAM_COLOUR_MODE
	FD_COLOUR_MODE_COMPLETE_PERCENTAGE = 0,
	FD_COLOUR_MODE_BUG_COUNT,
	FD_COLOUR_MODE_ASSETS_REQUIRED,
	FD_COLOUR_MODE_STRAND_OFFSET,
	FD_MAX_COLOUR_MODES
ENDENUM

// Element data struct for the flow diagram.
STRUCT FLOW_DIAGRAM_ELEMENT
	STRANDS						strand
	FLOW_DIAGRAM_ELEMENT_TYPE	type
	TEXT_LABEL_63				mainData
	INT							iAltData
	
	INT							xPos
	INT							yPosA
	INT							yPosB
	
	INT							colourR
	INT							colourG
	INT							colourB
	BOOL						completed
	BOOL						highlight
	
	// Cached info!
	TEXT_LABEL_15				sMissionID
	INT							iPerc
	INT							iNumBugs
ENDSTRUCT

#IF NOT DEFINED(FLOW_DIAGRAM_ELEMENT_CUSTOM)
STRUCT FLOW_DIAGRAM_ELEMENT_CUSTOM
	BOOL bTemp
ENDSTRUCT
#ENDIF

// A small struct for storing the upper and lower array bounds for a single strand on the flow diagram.
STRUCT FLOW_DIAGRAM_STRAND_BOUNDS
	INT upper
	INT lower
	INT startPos
ENDSTRUCT

// A struct for keeping track of syncs that are waiting to be linked when generating the flow diagram.
STRUCT FLOW_DIAGRAM_SYNC_LINK
	TEXT_LABEL_63	syncName
	STRANDS		sendStrand = STRAND_NONE
	STRANDS		awaitStrand = STRAND_NONE
ENDSTRUCT

// The Standard Flow Diagram Screen Text Colours
ENUM FLOW_DIAGRAM_COLOURS
	FD_COL_DULL = 90,
	FD_COL_MID = 140,
	FD_COL_BRIGHT = 200,
	FD_COL_ACTIVE = 255
ENDENUM


STRUCT FLOW_DIAGRAM
	// Diagram pathing
	STRING strFlowDiagramXML //= "X:/gta5/build/dev_ng/common/data/script/xml/debug/SP_flow_diagram.xml"
	
	//Diagram data storage.
	INT 						iElementCount
	STRANDS						eStrandOrder[MAX_STRANDS]
	FLOW_DIAGRAM_STRAND_BOUNDS	sStrandBounds[MAX_STRANDS]
	FLOW_DIAGRAM_ELEMENT 		sElement[FD_MAX_ELEMENTS]
	FLOW_DIAGRAM_ELEMENT_CUSTOM	sElementCustom[FD_MAX_ELEMENTS]
	BUGSTAR_DATA_STRUCT			sBugstarData
	DEBUG_MENU_ITEM_STRUCT		sMenuItemData
	MISSION_BUGS_STRUCT			sMissionBugs
	
	//Diagram state tracking.
	BOOL						bDoingStartMissionCheck = FALSE
	FLOW_DIAGRAM_COLOUR_MODE	eColourMode = FD_COLOUR_MODE_COMPLETE_PERCENTAGE

	//Diagram bound tracking.
	FLOAT 	fMinXOffset
	FLOAT 	fMaxXOffset
	FLOAT 	fMinYOffset
	FLOAT 	fMaxYOffset
	FLOAT	fScreenDivisionX
	FLOAT	fScreenDivisionY

	//Diagram camera variables.
	FLOAT 	fXOffset = -0.17
	FLOAT 	fYOffset = 0.0
	FLOAT 	fXOffsetTarget = -0.17
	FLOAT 	fYOffsetTarget = 0.0
	FLOAT	fZoom = 1.0
	FLOAT	fZoomTarget = 1.0

	//Diagram alpha variables.
	INT		iFrontAlphaTarget = 255
	INT		iFrontAlpha = 255
	INT		iInfoBoxAlphaTarget = 0
	INT		iInfoBoxAlpha = 0
	INT		iMissionBoxAlphaTarget = 0
	INT		iMissionBoxAlpha = 0

	//Diagram mouse and pointer variables.
	BOOL 	bDrawCursor
	BOOL	bMouseActive = FALSE
	FLOAT	fLastMouseX = 0.0
	FLOAT	fLastMouseY = 0.0
	FLOAT	fMouseZoomY = -1.0
	FLOAT 	fCursorX = 0.5
	FLOAT	fCursorY = 0.5
	FLOAT 	fCursorXTarget = 0.5
	FLOAT 	fCursorYTarget = 0.5

	//Highlighted mission variables.
	INT		iHighlightedMission
	
	//Diagram camera.
	CAMERA_INDEX camFlowDiagram
	
	// Needed for background bugstar queries
	BOOL	bFlowDiagramMissionPercCached = FALSE
	BOOL	bBugstarPercQueryStarted  = FALSE
	INT 	iDiagramPercProgress = 0
	INT 	iBugstarPercItemCount
	INT 	iBugstarPercItems[FD_MAX_MISSIONS_PER_QUERY]
	
	
	BOOL	bFlowDiagramMissionBugsCached = FALSE
	BOOL	bBugstarBugsQueryStarted = FALSE
	INT		iDiagramBugsProgress = 0

	BOOL	bBugstarInfoQueryStarted = FALSE
	INT		iMostRecentInfoQuery
ENDSTRUCT

STRUCT FLOW_DIAGRAM_VARS
	BOOL 	bFlowDiagramXMLGenerated = FALSE
	BOOL	bFlowDiagramActive = FALSE
	BOOL	bFlowDiagramDataLoaded = FALSE
	BOOL	bFlowDiagramDataSorted = FALSE
	BOOL	bFlowDiagramPositioned = FALSE
ENDSTRUCT


