//╒═════════════════════════════════════════════════════════════════════════════╕
//│				Author:  Ben Rollinson					Date: 14/01/11			│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│					  		Mission Flow Diagram Header							│
//│																				│
//│		DESCRIPTION: All script used to render and manage the mission flow		│
//│		diagram debug screen.													│
//│																				│
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛

#IF IS_DEBUG_BUILD

USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_xml.sch"

USING "flow_diagram_structs_core.sch"
USING "flow_debug_GAME.sch"
USING "flow_launcher_main_core.sch"
USING "flow_diagram_utils_core.sch"
USING "flow_diagram_GAME.sch"
USING "flow_diagram_core_override.sch"
USING "debug_menu_structs_core.sch"
USING "debug_menu_shared_core.sch"
USING "flow_public_core.sch"
USING "flow_public_core_override.sch"


/// PURPOSE:
///    Check to see if a specific type of diagram elements should send a bugstar query to collect bug data.
///    All of the standard types are checked here. Additional, project-specific element types can be added to DOES_FLOW_DIAGRAM_ELEMENT_GET_BUGSTAR_QUERY_CUSTOM()
/// PARAMS:
///    elementType - element type to check
/// RETURNS:
///    TRUE if the element type should have valid bugs, FALSE otherwise
FUNC BOOL DOES_FLOW_DIAGRAM_ELEMENT_GET_BUGSTAR_QUERY(FLOW_DIAGRAM_ELEMENT_TYPE elementType)
	SWITCH elementType
		CASE FD_ELEMENT_TYPE_MISSION
			RETURN TRUE
		BREAK
		CASE FD_ELEMENT_TYPE_ERROR
		CASE FD_ELEMENT_TYPE_ACTIVATE
		CASE FD_ELEMENT_TYPE_AWAIT_SYNC
		CASE FD_ELEMENT_TYPE_SEND_SYNC
			RETURN FALSE
		BREAK
		DEFAULT
			RETURN DOES_FLOW_DIAGRAM_ELEMENT_GET_BUGSTAR_QUERY_CUSTOM(elementType)
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC


//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════╡ Diagram Data Loading  ╞═══════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛
/// PURPOSE:
///    Parses the created XML diagram and loads elements into memory.
///    Handles all standard element types and attributes.
///    Loading Project-specific elements should be added to STORE_FLOW_DIAGRAM_CUSTOM_ELEMENT()
///    Loading project-specific attributes should be handled in LOAD_FLOW_DIAGRAM_DATA_FROM_XML_CUSTOM()
/// PARAMS:
///    sFlowDiagram - 
/// RETURNS:
///    
FUNC BOOL LOAD_FLOW_DIAGRAM_DATA_FROM_XML(FLOW_DIAGRAM &sFlowDiagram)
	CPRINTLN(DEBUG_FLOW_DIAGRAM_LOAD, "Loading Flow Diagram XML Data.")

	//Preemprively flag data as loaded. We'll unflag it if something goes wrong during loading.
	g_flowDiagram.bFlowDiagramDataLoaded = TRUE
	

	//Load the flow diagram XML file.
	CDEBUG1LN(DEBUG_FLOW_DIAGRAM_LOAD, "Loading XML file \",", sFlowDiagram.strFlowDiagramXML,"\".")
	
	IF LOAD_XML_FILE(sFlowDiagram.strFlowDiagramXML)
	
		INT iNodeCount = GET_NUMBER_OF_XML_NODES()
		
		CDEBUG1LN(DEBUG_FLOW_DIAGRAM_LOAD, "Number of XML nodes found: ", iNodeCount)
		
		// Check that the xml contains some data.
    	IF iNodeCount <> 0
			
			IF iNodeCount <= FD_MAX_ELEMENTS
			
				INT iNodeIndex, iAttributeIndex	//	, iStrandIndex
				sFlowDiagram.iElementCount = 0

				//Loop through all xml nodes in the document.
				REPEAT iNodeCount iNodeIndex
				
					CDEBUG1LN(DEBUG_FLOW_DIAGRAM_LOAD, "Processing XML Node #", iNodeIndex)
					
					BOOL bIgnoreNode = FALSE
					
					//Store diagram element type.
					SWITCH INT_TO_ENUM(FLOW_DIAGRAM_ELEMENT_TYPE, GET_HASH_KEY(GET_XML_NODE_NAME()))
						CASE FD_ELEMENT_TYPE_ACTIVATE
						CASE FD_ELEMENT_TYPE_AWAIT_SYNC
						CASE FD_ELEMENT_TYPE_MISSION
						CASE FD_ELEMENT_TYPE_SEND_SYNC
							CDEBUG1LN(DEBUG_FLOW_DIAGRAM_LOAD, GET_XML_NODE_NAME())
							sFlowDiagram.sElement[sFlowDiagram.iElementCount].type = INT_TO_ENUM(FLOW_DIAGRAM_ELEMENT_TYPE, GET_HASH_KEY(GET_XML_NODE_NAME())) 
						BREAK
						DEFAULT
							IF NOT STORE_FLOW_DIAGRAM_CUSTOM_ELEMENT(sFlowDiagram)
								bIgnoreNode = TRUE //This node is not of a type that we care about. Ignore it.
								CDEBUG1LN(DEBUG_FLOW_DIAGRAM_LOAD, "IGNORING NODE")
							ENDIF
						BREAK
					ENDSWITCH
					
					IF NOT bIgnoreNode
						//Loop through each attribute associated with this xml node.
						REPEAT GET_NUMBER_OF_XML_NODE_ATTRIBUTES() iAttributeIndex
							STRING strStrand
							SWITCH INT_TO_ENUM(FLOW_DIAGRAM_ATTRIBUTE, GET_HASH_KEY(GET_XML_NODE_ATTRIBUTE_NAME(iAttributeIndex)))
								CASE FD_ATTR_NAME
								CASE FD_ATTR_SYNCID
									sFlowDiagram.sElement[sFlowDiagram.iElementCount].mainData = GET_STRING_FROM_XML_NODE_ATTRIBUTE(iAttributeIndex)
									CDEBUG1LN(DEBUG_FLOW_DIAGRAM_LOAD, "name/syncID: ", sFlowDiagram.sElement[sFlowDiagram.iElementCount].mainData)
								BREAK
								CASE FD_ATTR_STRAND
									strStrand = GET_STRING_FROM_XML_NODE_ATTRIBUTE(iAttributeIndex)
									sFlowDiagram.sElement[sFlowDiagram.iElementCount].strand = GET_STRAND_ID_FROM_DISPLAY_STRING(strStrand)
									CDEBUG1LN(DEBUG_FLOW_DIAGRAM_LOAD, "strand: ",strStrand)
								BREAK
								DEFAULT
									LOAD_FLOW_DIAGRAM_DATA_FROM_XML_CUSTOM(sFlowDiagram, iAttributeIndex)
								BREAK
							ENDSWITCH
						ENDREPEAT
						IF DOES_FLOW_DIAGRAM_ELEMENT_GET_BUGSTAR_QUERY(sFlowDiagram.sElement[sFlowDiagram.iElementCount].type)// = FD_ELEMENT_TYPE_MISSION
							// For missions, find and cache the mission ID
							sFlowDiagram.sElement[sFlowDiagram.iElementCount].sMissionID = GET_MISSION_ID_FROM_DIAGRAM_ELEMENT(sFlowDiagram.sElement[sFlowDiagram.iElementCount])
						ENDIF
						sFlowDiagram.iElementCount++
					ENDIF
					CDEBUG1LN(DEBUG_FLOW_DIAGRAM_LOAD, "")
					//WAIT(0) 
					//Fix for 556573. BenR. Code asserts if we wait with an XML file open. If we need to schedule load here then either
					//need to reload file each frame or get this code assert modified.
					
					GET_NEXT_XML_NODE() //Step to the next node.
					
				ENDREPEAT
			ELSE
				CERRORLN(DEBUG_FLOW_DIAGRAM_LOAD, "There are more XML elements than the current array size. Increase FD_MAX_ELEMENTS?")
				g_flowDiagram.bFlowDiagramDataLoaded = FALSE
			ENDIF
		ELSE
			CERRORLN(DEBUG_FLOW_DIAGRAM_LOAD, "The XML file was empty.")
			g_flowDiagram.bFlowDiagramDataLoaded = FALSE
		ENDIF
		
		DELETE_XML_FILE()
	ELSE
		CDEBUG1LN(DEBUG_FLOW_DIAGRAM_LOAD, "Failed to load the flow diagram XML file.")
		g_flowDiagram.bFlowDiagramDataLoaded = FALSE
	ENDIF
	
	RETURN g_flowDiagram.bFlowDiagramDataLoaded
ENDFUNC


//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════╡ Diagram Colour Modes  ╞═══════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

FUNC BOOL CACHE_DIAGRAM_MISSION_PERCENTAGE(FLOW_DIAGRAM &sFlowDiagram, DEBUG_MENU_ITEM_STRUCT &sTempItems[])
/// PURPOSE: Loads the bugstar xml and updates all the bugstar items with the data it finds for the matching mission ID
	
	INT iCount
	INT iMissionList[FD_MAX_MISSIONS_PER_QUERY]
	
	REPEAT COUNT_OF(iMissionList) iCount
		iMissionList[iCount] = iCount
	ENDREPEAT
	
	IF sFlowDiagram.iDiagramPercProgress < COUNT_OF(sFlowDiagram.sElement) OR sFlowDiagram.bBugstarPercQueryStarted
		// Obtain a list of pointers to the bugstar items
		// Since we are using temp menu items which are limited by stack size, we can only grab a few missions at a time
		IF NOT sFlowDiagram.bBugstarPercQueryStarted
			sFlowDiagram.iBugstarPercItemCount = 0
			WHILE sFlowDiagram.iBugstarPercItemCount < COUNT_OF(sFlowDiagram.iBugstarPercItems) AND sFlowDiagram.iDiagramPercProgress < COUNT_OF(sFlowDiagram.sElement)
				IF DOES_FLOW_DIAGRAM_ELEMENT_GET_BUGSTAR_QUERY(sFlowDiagram.sElement[sFlowDiagram.iDiagramPercProgress].type)// = FD_ELEMENT_TYPE_MISSION
					sFlowDiagram.iBugstarPercItems[sFlowDiagram.iBugstarPercItemCount] = sFlowDiagram.iDiagramPercProgress
					sFlowDiagram.iBugstarPercItemCount++
				ENDIF
				sFlowDiagram.iDiagramPercProgress++
			ENDWHILE
				
			IF sFlowDiagram.iBugstarPercItemCount = 0
				CDEBUG1LN(DEBUG_FLOW_DIAGRAM_BUGSTAR_LOAD, "CACHE_DIAGRAM_MISSION_COMPLETE: No bugstar items to grab")
				RETURN FALSE
			ENDIF
			sFlowDiagram.bBugstarPercQueryStarted = TRUE
		ENDIF
		// Since we are re-using the debug menu bugstar interface, we need to copy all of the relevant into into the temp debug menu items
		REPEAT sFlowDiagram.iBugstarPercItemCount iCount
			sTempItems[iCount].sMissionID = sFlowDiagram.sElement[sFlowDiagram.iBugstarPercItems[iCount]].sMissionID
			CDEBUG1LN(DEBUG_FLOW_DIAGRAM_BUGSTAR_LOAD, "CACHE_DIAGRAM_MISSION_PERCENTAGE: Copy Mission ID ", iCount, " - ", sTempItems[iCount].sMissionID)
		ENDREPEAT

		IF NOT BUGSTAR_BASIC_MISSION_DATA(sTempItems, sFlowDiagram.sBugstarData, iMissionList, sFlowDiagram.iBugstarPercItemCount)
			RETURN FALSE
		ENDIF
		
		// Now we need to copy the info BACK into our cache and set the color
		CDEBUG1LN(DEBUG_FLOW_DIAGRAM_BUGSTAR_LOAD, "CACHE_DIAGRAM_MISSION_COMPLETE: We got our mission info. Copy it over")
		REPEAT sFlowDiagram.iBugstarPercItemCount iCount
			sFlowDiagram.sElement[sFlowDiagram.iBugstarPercItems[iCount]].iPerc = sTempItems[iCount].iPerc 
			CDEBUG1LN(DEBUG_FLOW_DIAGRAM_BUGSTAR_LOAD, "CACHE_DIAGRAM_MISSION_PERCENTAGE: Copy Mission Percentage ", iCount, " - ", sTempItems[iCount].sMissionID, "/", sFlowDiagram.sElement[sFlowDiagram.iBugstarPercItems[iCount]].sMissionID," : ", sTempItems[iCount].iPerc)
			GET_MISSION_PERCENTAGE_COLOUR(	sFlowDiagram.sElement[sFlowDiagram.iBugstarPercItems[iCount]].iPerc, 
										sFlowDiagram.sElement[sFlowDiagram.iBugstarPercItems[iCount]].colourR, 
										sFlowDiagram.sElement[sFlowDiagram.iBugstarPercItems[iCount]].colourG, 
										sFlowDiagram.sElement[sFlowDiagram.iBugstarPercItems[iCount]].colourB)
		ENDREPEAT
		
		// Next loop, we are going to do another query
		sFlowDiagram.bBugstarPercQueryStarted = FALSE

		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

/// PURPOSE: Attempts to get bugstar info for the current item
FUNC BOOL CACHE_DIAGRAM_MISSION_BUGS(FLOW_DIAGRAM &sFlowDiagram)
	IF sFlowDiagram.iDiagramBugsProgress < COUNT_OF(sFlowDiagram.sElement)
		IF DOES_FLOW_DIAGRAM_ELEMENT_GET_BUGSTAR_QUERY(sFlowDiagram.sElement[sFlowDiagram.iDiagramBugsProgress].type)// = FD_ELEMENT_TYPE_MISSION
			DEBUG_MENU_ITEM_STRUCT sItemCopy
			sItemCopy.sMissionID = sFlowDiagram.sElement[sFlowDiagram.iDiagramBugsProgress].sMissionID
			CDEBUG1LN(DEBUG_FLOW_DIAGRAM_BUGSTAR_LOAD, "CACHE_DIAGRAM_MISSION_BUGS: Copy Mission ID ", sFlowDiagram.iDiagramBugsProgress, " - ", sItemCopy.sMissionID)

			IF NOT BUGSTAR_DETAILED_MISSION_DATA(sFlowDiagram.sBugstarData, sItemCopy, sItemCopy.sMissionID)
				CDEBUG1LN(DEBUG_FLOW_DIAGRAM_BUGSTAR_LOAD, "CACHE_DIAGRAM_MISSION_BUGS: Waiting for detailed query to return")
				sFlowDiagram.bBugstarBugsQueryStarted = TRUE
				RETURN FALSE
			ENDIF
			
			// Copy total bugs into data
			sFlowDiagram.sElement[sFlowDiagram.iDiagramBugsProgress].iNumBugs = sFlowDiagram.sBugstarData.iABugs + sFlowDiagram.sBugstarData.iBBugs + sFlowDiagram.sBugstarData.iCBugs + sFlowDiagram.sBugstarData.iDBugs + sFlowDiagram.sBugstarData.iTodos + sFlowDiagram.sBugstarData.iTasks + sFlowDiagram.sBugstarData.iWish
			CDEBUG1LN(DEBUG_FLOW_DIAGRAM_BUGSTAR_LOAD, "CACHE_DIAGRAM_MISSION_BUGS: Copy Bugs to Mission ID ", sFlowDiagram.iDiagramBugsProgress, " - ", sItemCopy.sMissionID, " : ", sFlowDiagram.sElement[sFlowDiagram.iDiagramBugsProgress].iNumBugs)
			GET_MISSION_TOTAL_BUGS_COLOUR(	sFlowDiagram.sElement[sFlowDiagram.iDiagramBugsProgress].iNumBugs, 
										sFlowDiagram.sElement[sFlowDiagram.iDiagramBugsProgress].colourR, 
										sFlowDiagram.sElement[sFlowDiagram.iDiagramBugsProgress].colourG, 
										sFlowDiagram.sElement[sFlowDiagram.iDiagramBugsProgress].colourB)
		ENDIF
		sFlowDiagram.iDiagramBugsProgress++
		sFlowDiagram.bBugstarBugsQueryStarted = FALSE
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC


/// PURPOSE:
///    Sets all mission elements to use standard colorization based on strand order
PROC SET_DIAGRAM_COLOUR_MODE_TO_STRAND_OFFSET(FLOW_DIAGRAM &sFlowDiagram)
	INT iterDiagramElement
	REPEAT COUNT_OF(sFlowDiagram.sElement) iterDiagramElement
		FILL_IN_STRAND_COLOUR( ENUM_TO_INT(sFlowDiagram.sElement[iterDiagramElement].strand) % 8,
				sFlowDiagram.sElement[iterDiagramElement].colourR, 
				sFlowDiagram.sElement[iterDiagramElement].colourG, 
				sFlowDiagram.sElement[iterDiagramElement].colourB)
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Sets all mission elements to use colorization based on mission complete % from Bugstar queries
/// PARAMS:
///    sFlowDiagram - 
PROC SET_DIAGRAM_COLOUR_MODE_TO_COMPLETE_PERCENTAGES(FLOW_DIAGRAM &sFlowDiagram)
	CPRINTLN(DEBUG_FLOW_DIAGRAM_LOAD, "Setting diagram colour mode to mission complete percentages.")

	INT iElementIndex
	REPEAT COUNT_OF(sFlowDiagram.sElement) iElementIndex
		IF DOES_FLOW_DIAGRAM_ELEMENT_GET_BUGSTAR_QUERY(sFlowDiagram.sElement[iElementIndex].type)// = FD_ELEMENT_TYPE_MISSION
			GET_MISSION_PERCENTAGE_COLOUR(	sFlowDiagram.sElement[iElementIndex].iPerc, 
										sFlowDiagram.sElement[iElementIndex].colourR, 
										sFlowDiagram.sElement[iElementIndex].colourG, 
										sFlowDiagram.sElement[iElementIndex].colourB,
										NOT sFlowDiagram.bFlowDiagramMissionPercCached)
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Sets all mission elements to use colorization based on bug counts from Bugstar queries
/// PARAMS:
///    sFlowDiagram - 
PROC SET_DIAGRAM_COLOUR_MODE_TO_BUG_COUNTS(FLOW_DIAGRAM &sFlowDiagram)
	CPRINTLN(DEBUG_FLOW_DIAGRAM_LOAD, "Setting diagram colour mode to bug counts.")

	INT iElementIndex
	REPEAT COUNT_OF(sFlowDiagram.sElement) iElementIndex
		IF DOES_FLOW_DIAGRAM_ELEMENT_GET_BUGSTAR_QUERY(sFlowDiagram.sElement[iElementIndex].type)// = FD_ELEMENT_TYPE_MISSION
			GET_MISSION_TOTAL_BUGS_COLOUR(	sFlowDiagram.sElement[iElementIndex].iNumBugs, 
										sFlowDiagram.sElement[iElementIndex].colourR, 
										sFlowDiagram.sElement[iElementIndex].colourG, 
										sFlowDiagram.sElement[iElementIndex].colourB)
		ENDIF
	ENDREPEAT
ENDPROC	

/// PURPOSE:
///    UNIMPLEMENTED: Sets all mission elements to use colorization based on asset requirements
PROC SET_DIAGRAM_COLOUR_MODE_TO_ASSETS_REQUIRED()
	CPRINTLN(DEBUG_FLOW_DIAGRAM_LOAD, " Setting diagram colour mode to assets required.")
	//TODO.
ENDPROC


/// PURPOSE:
///    Switches the coloization mode 
PROC SWITCH_DIAGRAM_COLOUR_MODE(FLOW_DIAGRAM &sFlowDiagram, FLOW_DIAGRAM_COLOUR_MODE eColourMode)
	SWITCH(eColourMode)
		CASE FD_COLOUR_MODE_COMPLETE_PERCENTAGE
			SET_DIAGRAM_COLOUR_MODE_TO_COMPLETE_PERCENTAGES(sFlowDiagram)
		BREAK
		
		CASE FD_COLOUR_MODE_BUG_COUNT
			SET_DIAGRAM_COLOUR_MODE_TO_BUG_COUNTS(sFlowDiagram)
		BREAK
		
		CASE FD_COLOUR_MODE_ASSETS_REQUIRED
			SET_DIAGRAM_COLOUR_MODE_TO_ASSETS_REQUIRED(/*sFlowDiagram*/)
		BREAK
		CASE FD_COLOUR_MODE_STRAND_OFFSET
			SET_DIAGRAM_COLOUR_MODE_TO_STRAND_OFFSET(sFlowDiagram)
		BREAK
	ENDSWITCH
	
	sFlowDiagram.eColourMode = eColourMode
ENDPROC

/// PURPOSE:
///    Checks IS_MISSION_COMPLETE for all mission nodes and sets .completed appropriately
PROC UPDATE_DIAGRAM_COMPLETED_MISSIONS(FLOW_DIAGRAM &sFlowDiagram)
	INT iElementIndex
	
	//Search through all diagram elements.
	REPEAT sFlowDiagram.iElementCount iElementIndex
		//Is this a mission element?
		IF sFlowDiagram.sElement[iElementIndex].type = FD_ELEMENT_TYPE_MISSION
			//Yes, set its mission complete state.
			sFlowDiagram.sElement[iElementIndex].completed = FLOW_DIAGRAM_IS_MISSION_COMPLETE(sFlowDiagram.sElement[iElementIndex].mainData)
		ENDIF
	ENDREPEAT
ENDPROC


//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════╡ Diagram Data Sorting  ╞═══════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC SORT_FLOW_DIAGRAM_DATA(FLOW_DIAGRAM &sFlowDiagram)

	CPRINTLN(DEBUG_FLOW_DIAGRAM_LOAD, "Sorting Flow Diagram XML Data.")
	
	IF g_flowDiagram.bFlowDiagramDataLoaded
		
		INT iSortedIndex = -1
		BOOL bFoundStrandToSort = FALSE
		BOOL bStrandSorted[MAX_STRANDS]
		
		INT iStrandIndex
		REPEAT MAX_STRANDS iStrandIndex
			CDEBUG1LN(DEBUG_FLOW_DIAGRAM_SORT, "Sorting Pass #", iStrandIndex)
			
			//Make the initialising screen draw during this process.
			DRAW_LOADING_SCREEN()
			
			//Find a strand to sort.
			INT iElementIndex = 0
			bFoundStrandToSort = FALSE
			WHILE iElementIndex < sFlowDiagram.iElementCount AND NOT bFoundStrandToSort
				IF NOT bStrandSorted[sFlowDiagram.sElement[iElementIndex].strand] 
					sFlowDiagram.eStrandOrder[iStrandIndex] = sFlowDiagram.sElement[iElementIndex].strand 
					bFoundStrandToSort = TRUE
					
					CDEBUG1LN(DEBUG_FLOW_DIAGRAM_SORT, "Setting next strand for sorting to ", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(sFlowDiagram.eStrandOrder[iStrandIndex]), ".")
				ENDIF
				iElementIndex++
			ENDWHILE
			
			//Check we found a strand to sort.
			IF NOT bFoundStrandToSort
				CPRINTLN(DEBUG_FLOW_DIAGRAM_LOAD, "!!WARNING!! Failed to find a new strand to sort. Diagram sorting ended without processing all strands.")
				g_flowDiagram.bFlowDiagramDataSorted = TRUE	
				EXIT
			ENDIF

			//Set this strands lower bound to the index above the current sort index.
			sFlowDiagram.sStrandBounds[sFlowDiagram.eStrandOrder[iStrandIndex]].lower = iSortedIndex + 1
		
			//Sort this strand.
			FOR iElementIndex = (iSortedIndex+1) TO (sFlowDiagram.iElementCount-1)
			
				IF sFlowDiagram.sElement[iElementIndex].strand = sFlowDiagram.eStrandOrder[iStrandIndex]
					
					//Found element to shift. Shift this element down to the sorted index.
					CDEBUG1LN(DEBUG_FLOW_DIAGRAM_SORT, "Element at index ", iElementIndex, " is in the correct strand. Sorting to index ", iSortedIndex+1,".")
					
					INT iShiftIndex
					FLOW_DIAGRAM_ELEMENT eTempElement
					FOR iShiftIndex = iElementIndex TO (iSortedIndex+2) STEP -1
						eTempElement = sFlowDiagram.sElement[iShiftIndex-1]
						sFlowDiagram.sElement[iShiftIndex-1] = sFlowDiagram.sElement[iShiftIndex]
						sFlowDiagram.sElement[iShiftIndex] = eTempElement
					ENDFOR
					iSortedIndex++

				ENDIF
			ENDFOR
			
			//Set this strands upper bound to the new current sort index.
			sFlowDiagram.sStrandBounds[sFlowDiagram.eStrandOrder[iStrandIndex]].upper = iSortedIndex
			
			//Flag strand as sorted.
			bStrandSorted[sFlowDiagram.eStrandOrder[iStrandIndex]] = TRUE
			
			CDEBUG1LN(DEBUG_FLOW_DIAGRAM_SORT, GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(sFlowDiagram.eStrandOrder[iStrandIndex]), " sorted. Lower: ", sFlowDiagram.sStrandBounds[sFlowDiagram.eStrandOrder[iStrandIndex]].lower, " Upper: ", sFlowDiagram.sStrandBounds[sFlowDiagram.eStrandOrder[iStrandIndex]].upper,".")
			
			WAIT(0)
		
		ENDREPEAT
		
		g_flowDiagram.bFlowDiagramDataSorted = TRUE
		CDEBUG1LN(DEBUG_FLOW_DIAGRAM_SORT, "All strands sorted.")
		
	ELSE
		CERRORLN(DEBUG_FLOW_DIAGRAM_SORT, "Tried to sort flow diagram data before it had been correctly loaded.")
	ENDIF
	
ENDPROC


//╒═════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════╡ Diagram Element Position Setting ╞═════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛



PROC REMOVE_SYNC_LINK_FROM_ARRAY(INT index, FLOW_DIAGRAM_SYNC_LINK &sSyncLinks[], INT &iNoSyncLinks)

	IF index < 0
		CERRORLN(DEBUG_FLOW_DIAGRAM, "Tried to remove a sync link at an index that was too low. Operation failed.")
		EXIT
	ENDIF
	
	IF index > COUNT_OF(sSyncLinks)
		CERRORLN(DEBUG_FLOW_DIAGRAM, "Tried to remove a sync link at an index that was too high. Operation failed.")
		EXIT
	ENDIF

	INT iSyncIndex
	FOR iSyncIndex = index TO (iNoSyncLinks-2)
		sSyncLinks[iSyncIndex] = sSyncLinks[iSyncIndex+1]
	ENDFOR
	sSyncLinks[iSyncIndex+1].syncName = ""
	sSyncLinks[iSyncIndex+1].sendStrand = STRAND_NONE
	sSyncLinks[iSyncIndex+1].awaitStrand = STRAND_NONE
	
	iNoSyncLinks--
ENDPROC


PROC SET_POSITION_OF_FIRST_PASS_DIAGRAM_ELEMENT(FLOW_DIAGRAM &sFlowDiagram,
												INT iXPos, 
												FLOW_DIAGRAM_ELEMENT &sElement, 
												BOOL &bActiveStrands[MAX_STRANDS], 
												BOOL &bDrawStrandElement[MAX_STRANDS], 
												INT &iNoSyncsWaitingForLink,
												FLOW_DIAGRAM_SYNC_LINK &sSyncLinks[],
												INT &iStrandCurrentElementIndex[MAX_STRANDS],
												INT &iReservedBounds[2])

	BOOL bFoundSync
	INT iSyncIndex
	STRANDS eActivatedStrand
	INT iStrandYPos = GET_STRAND_Y_POSITION(sFlowDiagram, sElement.strand)
	INT iAdditionalStrandYPos
	
	SWITCH (sElement.type)
		CASE FD_ELEMENT_TYPE_ACTIVATE
			CDEBUG1LN(DEBUG_FLOW_DIAGRAM_POSITION, "[1st-Pass]Positioning ACTIVATE(", sElement.mainData,") for strand ",GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(sElement.strand), "... ")
		
			eActivatedStrand = GET_STRAND_ID_FROM_DISPLAY_STRING(sElement.mainData)
			iAdditionalStrandYPos = GET_STRAND_Y_POSITION(sFlowDiagram, eActivatedStrand)
			
			IF iReservedBounds[0] = -1
			OR (iStrandYPos < iReservedBounds[0] AND iAdditionalStrandYPos < iReservedBounds[0])
			OR (iStrandYPos > iReservedBounds[1] AND iAdditionalStrandYPos > iReservedBounds[1])
				CDEBUG1LN(DEBUG_FLOW_DIAGRAM_POSITION, "Space found. Positioning.")
			
				SET_POSITION_OF_CONNECTING_ELEMENT(sElement, iXPos, iStrandYPos, iAdditionalStrandYPos)
				//Save the start position for the activated strand.
				sFlowDiagram.sStrandBounds[eActivatedStrand].startPos = iXPos
				
				//Flag the activated strand as active for drawing elements to the flow diagram.
				bActiveStrands[eActivatedStrand] = TRUE
				bDrawStrandElement[eActivatedStrand] = TRUE
				
				iReservedBounds[0] = IMIN(iStrandYPos, iAdditionalStrandYPos)
				iReservedBounds[1] = IMAX(iStrandYPos, iAdditionalStrandYPos)
				
				iStrandCurrentElementIndex[sElement.strand]++
			ENDIF
		
		BREAK
		
		CASE FD_ELEMENT_TYPE_AWAIT_SYNC
			CDEBUG1LN(DEBUG_FLOW_DIAGRAM_POSITION, "[1st-Pass]Positioning AWAIT(", sElement.mainData, ") for strand ", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(sElement.strand), "... ")
		
			bFoundSync = FALSE
			iSyncIndex = 0
			WHILE iSyncIndex < iNoSyncsWaitingForLink AND NOT bFoundSync
				//Is there a synclink in the list that matches this element?
				IF ARE_STRINGS_EQUAL(sElement.mainData, sSyncLinks[iSyncIndex].syncName)
					bFoundSync = TRUE
					//Is it still looking for an await value?
					IF sSyncLinks[iSyncIndex].awaitStrand = STRAND_NONE
						sSyncLinks[iSyncIndex].awaitStrand = sElement.strand
					ENDIF
				ENDIF
				IF NOT bFoundSync
					iSyncIndex++
				ENDIF
			ENDWHILE
			
			//This sync is not listed. Add a new entry to the searching for list.
			IF NOT bFoundSync
				sSyncLinks[iNoSyncsWaitingForLink].syncName = sElement.mainData
				sSyncLinks[iNoSyncsWaitingForLink].sendStrand = STRAND_NONE
				sSyncLinks[iNoSyncsWaitingForLink].awaitStrand = sElement.strand
				iNoSyncsWaitingForLink++
				
			//This sync is listed. Check it has a send value set, if so we have a pair.
			ELIF sSyncLinks[iSyncIndex].sendStrand != STRAND_NONE
				
				iAdditionalStrandYPos = GET_STRAND_Y_POSITION(sFlowDiagram, sSyncLinks[iSyncIndex].sendStrand)
			
				IF iReservedBounds[0] = -1
				OR (iStrandYPos < iReservedBounds[0] AND iAdditionalStrandYPos < iReservedBounds[0])
				OR (iStrandYPos > iReservedBounds[1] AND iAdditionalStrandYPos > iReservedBounds[1])
				
					CDEBUG1LN(DEBUG_FLOW_DIAGRAM_POSITION, "Link pair found and positioned.")
				
					SET_POSITION_OF_DEFAULT_ELEMENT(sElement, iXPos, iStrandYPos)
					SET_POSITION_OF_CONNECTING_ELEMENT(sFlowDiagram.sElement[iStrandCurrentElementIndex[sSyncLinks[iSyncIndex].sendStrand]], iXPos, iAdditionalStrandYPos, iStrandYPos)
					
					iReservedBounds[0] = IMIN(iStrandYPos, iAdditionalStrandYPos)
					iReservedBounds[1] = IMAX(iStrandYPos, iAdditionalStrandYPos)
					
					iStrandCurrentElementIndex[sSyncLinks[iSyncIndex].sendStrand]++
					iStrandCurrentElementIndex[sSyncLinks[iSyncIndex].awaitStrand]++
					bDrawStrandElement[sSyncLinks[iSyncIndex].awaitStrand] = FALSE
					
					REMOVE_SYNC_LINK_FROM_ARRAY(iSyncIndex, sSyncLinks, iNoSyncsWaitingForLink)
				ENDIF
			ENDIF
		BREAK

		CASE FD_ELEMENT_TYPE_SEND_SYNC
			CDEBUG1LN(DEBUG_FLOW_DIAGRAM_POSITION, "[1st-Pass]Positioning SEND(", sElement.mainData, ") for strand ", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(sElement.strand), "... ")
		
			bFoundSync = FALSE
			iSyncIndex = 0
			WHILE iSyncIndex < iNoSyncsWaitingForLink AND NOT bFoundSync
				//Is there a synclink in the list that matches this element?
				IF ARE_STRINGS_EQUAL(sElement.mainData, sSyncLinks[iSyncIndex].syncName)
					bFoundSync = TRUE
					//Is it still looking for a send value?
					IF sSyncLinks[iSyncIndex].sendStrand = STRAND_NONE
						sSyncLinks[iSyncIndex].sendStrand = sElement.strand
					ENDIF
				ENDIF
				IF NOT bFoundSync
					iSyncIndex++
				ENDIF
			ENDWHILE

			//This sync is not listed. Add a new entry to the searching for list.
			IF NOT bFoundSync
				sSyncLinks[iNoSyncsWaitingForLink].syncName = sElement.mainData
				sSyncLinks[iNoSyncsWaitingForLink].sendStrand = sElement.strand
				sSyncLinks[iNoSyncsWaitingForLink].awaitStrand = STRAND_NONE
				iNoSyncsWaitingForLink++
			
			//This sync is listed. Check it has an await value set, if so we have a pair.
			ELIF sSyncLinks[iSyncIndex].awaitStrand <> STRAND_NONE
			
				iAdditionalStrandYPos = GET_STRAND_Y_POSITION(sFlowDiagram, sSyncLinks[iSyncIndex].awaitStrand)
				
				IF iReservedBounds[0] = -1
				OR (iStrandYPos < iReservedBounds[0] AND iAdditionalStrandYPos < iReservedBounds[0])
				OR (iStrandYPos > iReservedBounds[1] AND iAdditionalStrandYPos > iReservedBounds[1])
					
					CDEBUG1LN(DEBUG_FLOW_DIAGRAM_POSITION, "Link pair found and positioned.")
				
					SET_POSITION_OF_CONNECTING_ELEMENT(sElement, iXPos, iStrandYPos, iAdditionalStrandYPos)
					SET_POSITION_OF_DEFAULT_ELEMENT(sFlowDiagram.sElement[iStrandCurrentElementIndex[sSyncLinks[iSyncIndex].awaitStrand]], iXPos, iAdditionalStrandYPos)
					
					iReservedBounds[0] = IMIN(iStrandYPos, iAdditionalStrandYPos)
					iReservedBounds[1] = IMAX(iStrandYPos, iAdditionalStrandYPos)
					
					iStrandCurrentElementIndex[sSyncLinks[iSyncIndex].sendStrand]++
					iStrandCurrentElementIndex[sSyncLinks[iSyncIndex].awaitStrand]++
					bDrawStrandElement[sSyncLinks[iSyncIndex].sendStrand] = FALSE
					
					REMOVE_SYNC_LINK_FROM_ARRAY(iSyncIndex, sSyncLinks, iNoSyncsWaitingForLink)
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	//This strand has just drawn an element. Unflag the relevant draw element array index.
	bDrawStrandElement[sElement.strand] = FALSE
ENDPROC


PROC SET_POSITION_OF_SECOND_PASS_DIAGRAM_ELEMENT(	FLOW_DIAGRAM &sFlowDiagram,
													INT iXPos, 
													FLOW_DIAGRAM_ELEMENT &sElement, 
													BOOL &bDrawStrandElement[MAX_STRANDS], 
													INT &iStrandCurrentElementIndex[MAX_STRANDS],
													INT &iReservedBounds[2])

	INT iStrandYPos = GET_STRAND_Y_POSITION(sFlowDiagram, sElement.strand)
	
	SWITCH (sElement.type)

		CASE FD_ELEMENT_TYPE_MISSION

			IF iReservedBounds[0] = -1
			OR iStrandYPos < iReservedBounds[0]
			OR iStrandYPos > iReservedBounds[1]
				CDEBUG1LN(DEBUG_FLOW_DIAGRAM_POSITION, "[2nd-Pass]Positioned MISSION/COMMUNICATION element for strand ", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(sElement.strand), ".")
			
				SET_POSITION_OF_DEFAULT_ELEMENT(sElement, iXPos, iStrandYPos)
				iStrandCurrentElementIndex[sElement.strand]++
			ENDIF
		BREAK
		DEFAULT
			SET_POSITION_OF_SECOND_PASS_DIAGRAM_ELEMENT_CUSTOM(sFlowDiagram, iXPos,sElement,  iStrandCurrentElementIndex, iReservedBounds)
		BREAK
		
	ENDSWITCH
	
	//This strand has just drawn an element. Unflag the relevant draw element array index.
	bDrawStrandElement[sElement.strand] = FALSE
ENDPROC


PROC CALCULATE_FLOW_DIAGRAM_ELEMENT_POSITIONS(FLOW_DIAGRAM &sFlowDiagram)

	IF g_flowDiagram.bFlowDiagramDataSorted
	
		CPRINTLN(DEBUG_FLOW_DIAGRAM_LOAD, "Calculating Positions of Flow Diagram Elements.")
		
		DRAW_LOADING_SCREEN()
	
		//Screen X position tracking.
		INT iCurrentXPos = 0
		
		//Strand render tracking flags.
		INT iStrandIndex
		INT iStrandCurrentElementIndex[MAX_STRANDS]
		INT iReservedBounds[2]
		BOOL bActiveStrands[MAX_STRANDS]
		BOOL bPositionStrandElement[MAX_STRANDS]
		BOOL bAllStrandsPositioned = TRUE
		BOOL bAllStrandsComplete = FALSE
		
		iReservedBounds[0] = -1
		
		//Sync call tracking flags.
		INT iNoSyncsWaitingForLink
		FLOW_DIAGRAM_SYNC_LINK	sSyncLinks[MAX_FLOW_DIAGRAM_SYNC_LINKS]
		
		CDEBUG1LN(DEBUG_FLOW_DIAGRAM_POSITION, "Setting strand current elements to their lower bounds.")
		//For each strand...
		REPEAT MAX_STRANDS iStrandIndex
			//...copy the strand's lower bounds to the current positon array.
			iStrandCurrentElementIndex[iStrandIndex] = sFlowDiagram.sStrandBounds[iStrandIndex].lower
		ENDREPEAT
		
		
		FLOW_DIAGRAM_INITIALIZE_STRANDS(sFlowDiagram, bActiveStrands, iCurrentXPos)

		//Keep positioning elements until we reach the end of the screen.
		WHILE NOT bAllStrandsComplete
		
			//All strands were finished being positioned on the last iteration. 
			//Reset draw flags for active strands and start again.
			IF bAllStrandsPositioned
				REPEAT MAX_STRANDS iStrandIndex
					bPositionStrandElement[iStrandIndex] = bActiveStrands[iStrandIndex]
				ENDREPEAT
			ENDIF
			
			#IF IS_DEBUG_BUILD
				//Print current list of synclinks.
				CDEBUG1LN(DEBUG_FLOW_DIAGRAM_POSITION, "---ACTIVE SYNCLINKS---")
				INT iSyncIndex
				REPEAT iNoSyncsWaitingForLink iSyncIndex
					CDEBUG1LN(DEBUG_FLOW_DIAGRAM_POSITION, "[", sSyncLinks[iSyncIndex].syncName, "] Send:", 
						PICK_STRING(sSyncLinks[iSyncIndex].sendStrand != STRAND_NONE, GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(sSyncLinks[iSyncIndex].sendStrand),"WAITING"),
						" Await:", PICK_STRING(sSyncLinks[iSyncIndex].awaitStrand != STRAND_NONE, GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(sSyncLinks[iSyncIndex].awaitStrand),"WAITING"))
				ENDREPEAT
				CDEBUG1LN(DEBUG_FLOW_DIAGRAM_POSITION, "----------------------")
			#ENDIF
		
			//Loop through all strands and find first pass diagram elements to draw.
			REPEAT MAX_STRANDS iStrandIndex
				//Strand is active.
				IF bActiveStrands[iStrandIndex]
					//Strand flagged as being allowed to draw an element this pass.
					IF bPositionStrandElement[iStrandIndex]
						CDEBUG1LN(DEBUG_FLOW_DIAGRAM_POSITION, "Strand:", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(INT_TO_ENUM(STRANDS, iStrandIndex)), " Index:", iStrandCurrentElementIndex[iStrandIndex])

						//Draw element.
						SET_POSITION_OF_FIRST_PASS_DIAGRAM_ELEMENT(	sFlowDiagram, 
																	iCurrentXPos, 
																	sFlowDiagram.sElement[iStrandCurrentElementIndex[iStrandIndex]], 
																	bActiveStrands, 
																	bPositionStrandElement, 
																	iNoSyncsWaitingForLink, 
																	sSyncLinks,
																	iStrandCurrentElementIndex,
																	iReservedBounds)
																	
						//Has this strand drawn all of its elements?
						IF iStrandCurrentElementIndex[iStrandIndex] > sFlowDiagram.sStrandBounds[iStrandIndex].upper
							CDEBUG1LN(DEBUG_FLOW_DIAGRAM_POSITION, "All elements for strand ", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(INT_TO_ENUM(STRANDS, iStrandIndex)), " have been positioned. Deactivating.")
						
							//No more elements to draw. Flag it as inactive.
							bActiveStrands[iStrandIndex] = FALSE
							bPositionStrandElement[iStrandIndex] = FALSE
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
			
			//All strands were finished being positioned on the last iteration. 
			//Reset draw flags for active strands and start again.
			IF bAllStrandsPositioned
				REPEAT MAX_STRANDS iStrandIndex
					bPositionStrandElement[iStrandIndex] = bActiveStrands[iStrandIndex]
				ENDREPEAT
			ENDIF
			
			//Loop through all strands and find second pass diagram elements to draw.
			REPEAT MAX_STRANDS iStrandIndex
				//Strand is active.
				IF bActiveStrands[iStrandIndex]
					//Strand flagged as being allowed to draw an element this pass.
					IF bPositionStrandElement[iStrandIndex]

						//Draw element.
						SET_POSITION_OF_SECOND_PASS_DIAGRAM_ELEMENT(sFlowDiagram,
																	iCurrentXPos, 
																	sFlowDiagram.sElement[iStrandCurrentElementIndex[iStrandIndex]], 
																	bPositionStrandElement, 
																	iStrandCurrentElementIndex,
																	iReservedBounds)
														
						//Has this strand positioned all of its elements?
						IF iStrandCurrentElementIndex[iStrandIndex] > sFlowDiagram.sStrandBounds[iStrandIndex].upper
							CDEBUG1LN(DEBUG_FLOW_DIAGRAM_POSITION, "All elements for strand ", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(INT_TO_ENUM(STRANDS, iStrandIndex)), " have been positioned. Deactivating.")
						
							//No more elements to draw. Flag it as inactive.
							bActiveStrands[iStrandIndex] = FALSE
							bPositionStrandElement[iStrandIndex] = FALSE
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
			
			bAllStrandsPositioned = TRUE
			bAllStrandsComplete = TRUE
			REPEAT MAX_STRANDS iStrandIndex
				//Check if there are any strand with elements still left to position.
				IF bPositionStrandElement[iStrandIndex]
					bAllStrandsPositioned = FALSE
				ENDIF
				
				//Also check that at least one strand is still active.
				IF bActiveStrands[iStrandIndex]
					bAllStrandsComplete = FALSE
				ENDIF

			ENDREPEAT
			
			//All strands elements positioned. Step to the next section of the flow diagram.
			IF bAllStrandsPositioned
				CDEBUG1LN(DEBUG_FLOW_DIAGRAM_POSITION, "All elements positioned for X=", iCurrentXPos, ".")
			
				iReservedBounds[0] = -1
				iCurrentXPos ++
			ENDIF
			
//			#IF FD_DISPLAY_FLOW_DIAGRAM_POSITIONING_DEBUG
			WAIT(0)
//			#ENDIF
		ENDWHILE
		
		CDEBUG1LN(DEBUG_FLOW_DIAGRAM_POSITION, "Diagram positioning complete.")
		
		//Calculate offset bounds for scrolling.
		sFlowDiagram.fMinXOffset = GET_SCREEN_X_COORD(sFlowDiagram, 0) - 0.5 + sFlowDiagram.fXOffset
		sFlowDiagram.fMaxXOffset = GET_SCREEN_X_COORD(sFlowDiagram, iCurrentXPos-1) - 0.5 + sFlowDiagram.fXOffset
		sFlowDiagram.fMinYOffset = GET_SCREEN_Y_COORD(sFlowDiagram, 0) - 0.5 + sFlowDiagram.fYOffset
		sFlowDiagram.fMaxYOffset = GET_SCREEN_Y_COORD(sFlowDiagram, ENUM_TO_INT(MAX_STRANDS)-1) - 0.5 + sFlowDiagram.fYOffset
		
		g_flowDiagram.bFlowDiagramPositioned = TRUE
	ELSE
		CERRORLN(DEBUG_FLOW_DIAGRAM, "Calculate_Flow_Diagram_Element_Positions: Tried to calculate diagram positions before the data was sorted.")
	ENDIF
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════╡ Diagram Display Procedures ╞═════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC DISPLAY_FLOW_DIAGRAM_BACKGROUND(FLOW_DIAGRAM &sFlowDiagram)
	INT index
	
	//Draw black background box.
	DRAW_RECT(0.5,0.5,1.0,1.0,0,0,0,255)
	
	REPEAT MAX_STRANDS index
		//Draw each of the background strand lines.
		DRAW_RECT(	0.5625, GET_SCREEN_Y_COORD(sFlowDiagram, GET_STRAND_Y_POSITION(sFlowDiagram, INT_TO_ENUM(STRANDS,index))), 
					0.875, 0.0014, 
					50, 50, 50, CLAMP_INT(sFlowDiagram.iFrontAlpha-155,0,100))
	ENDREPEAT

ENDPROC


PROC DISPLAY_FLOW_DIAGRAM_FOREGROUND(FLOW_DIAGRAM &sFlowDiagram)
	INT index
	
	//Zoomed in close: Display sidebar.
	IF sFlowDiagram.iFrontAlpha > 0
		

		//Draw transparent strand name background box.
		DRAW_RECT(	0.0625, 0.5, 
					0.125, 0.98, 
					0, 0, 0, 
					CLAMP_INT(sFlowDiagram.iFrontAlpha-25,0,230))
					
		//Draw vertical deviding line.
		DRAW_RECT(	0.125, 0.5, 
					0.0022, 1.0, 
					CLAMP_INT(FD_COL_R_LINE_BG-40,0,255), CLAMP_INT(FD_COL_R_LINE_BG-40,0,255), CLAMP_INT(FD_COL_R_LINE_BG-40,0,255), 
					sFlowDiagram.iFrontAlpha)
		
		REPEAT MAX_STRANDS index
			//Draw foreground strand lines.
			DRAW_RECT(	0.0835, GET_SCREEN_Y_COORD(sFlowDiagram, GET_STRAND_Y_POSITION(sFlowDiagram, INT_TO_ENUM(STRANDS,index))), 
						0.083, 0.0014, 
						FD_COL_R_LINE_BG, FD_COL_G_LINE_BG, FD_COL_B_LINE_BG, 
						CLAMP_INT(sFlowDiagram.iFrontAlpha-155,0,100))
						
			//Display strand names.
			SETUP_FLOW_DIAGRAM_STRAND_TEXT(sFlowDiagram.iFrontAlpha)
			DISPLAY_TEXT_WITH_LITERAL_STRING(	0.04, 
												GET_SCREEN_Y_COORD(sFlowDiagram, GET_STRAND_Y_POSITION(sFlowDiagram, INT_TO_ENUM(STRANDS,index))) - 0.013, 
												"STRING",
												GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(INT_TO_ENUM(STRANDS, index)))
		ENDREPEAT
		
	//Zoomed out a long way: Display in-line strand names.
	ELIF sFlowDiagram.iFrontAlpha < 0
		
		REPEAT MAX_STRANDS index
			//Get this strand's starting x-coord.
			FLOAT fXStart = GET_SCREEN_X_COORD(sFlowDiagram, sFlowDiagram.sStrandBounds[index].startPos)
			//Get this strand's y-coord.
			FLOAT fYStrand = GET_SCREEN_Y_COORD(sFlowDiagram, GET_STRAND_Y_POSITION(sFlowDiagram, INT_TO_ENUM(STRANDS, index)))
			
			//Draw this strand's name just before the first element.
			SETUP_FLOW_DIAGRAM_INLINE_STRAND_TEXT(sFlowDiagram, -sFlowDiagram.iFrontAlpha)
			DISPLAY_TEXT_WITH_LITERAL_STRING(	fXStart - (0.02*sFlowDiagram.fZoom) - GET_STRING_WIDTH_WITH_STRING("STRING",GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(INT_TO_ENUM(STRANDS, index))), 
												fYStrand - (0.02*sFlowDiagram.fZoom), 
												"STRING", 
												GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(INT_TO_ENUM(STRANDS, index)))
		ENDREPEAT
	ENDIF
ENDPROC


PROC DISPLAY_FLOW_DIAGRAM_CURSOR(FLOW_DIAGRAM &sFlowDiagram)
	IF sFlowDiagram.iInfoBoxAlpha < 255
		INT iAlpha
	
		IF sFlowDiagram.bMouseActive
			//Calculate alpha.
			iAlpha = FLOOR(TO_FLOAT(255 - sFlowDiagram.iInfoBoxAlpha) * 0.3)
		
			IF IS_MOUSE_BUTTON_PRESSED(MB_RIGHT_BTN)
			
				//Draw mouse zoom reticule with debug lines.
				DRAW_DEBUG_LINE_2D_WITH_TWO_COLOURS(<<sFlowDiagram.fCursorX, sFlowDiagram.fMouseZoomY, 0.0>>, 
													<<sFlowDiagram.fCursorX-0.009, sFlowDiagram.fMouseZoomY, 0.0>>,
													0,255,0,iAlpha,
													0,255,0,0)
				DRAW_DEBUG_LINE_2D_WITH_TWO_COLOURS(<<sFlowDiagram.fCursorX, sFlowDiagram.fMouseZoomY, 0.0>>, 
													<<sFlowDiagram.fCursorX+0.009, sFlowDiagram.fMouseZoomY, 0.0>>,
													0,255,0,iAlpha,
													0,255,0,0)
				IF sFlowDiagram.fMouseZoomY <= sFlowDiagram.fCursorY									
					DRAW_DEBUG_LINE_2D_WITH_TWO_COLOURS(<<sFlowDiagram.fCursorX, sFlowDiagram.fMouseZoomY, 0.0>>, 
														<<sFlowDiagram.fCursorX, sFlowDiagram.fMouseZoomY - 0.016, 0.0>>,
														0,255,0,iAlpha,
														0,255,0,0)
					DRAW_DEBUG_LINE_2D(<<sFlowDiagram.fCursorX, sFlowDiagram.fMouseZoomY, 0.0>>, 
									<<sFlowDiagram.fCursorX, sFlowDiagram.fCursorY + 0.016, 0.0>>,
									0,255,0,iAlpha)
					DRAW_DEBUG_LINE_2D_WITH_TWO_COLOURS(<<sFlowDiagram.fCursorX, sFlowDiagram.fCursorY + 0.016, 0.0>>, 
														<<sFlowDiagram.fCursorX - 0.009, sFlowDiagram.fCursorY, 0.0>>,
														0,255,0,iAlpha,
														0,255,0,0)
					DRAW_DEBUG_LINE_2D_WITH_TWO_COLOURS(<<sFlowDiagram.fCursorX, sFlowDiagram.fCursorY + 0.016, 0.0>>, 
														<<sFlowDiagram.fCursorX + 0.009, sFlowDiagram.fCursorY, 0.0>>,
														0,255,0,iAlpha,
														0,255,0,0)
				ELSE
					DRAW_DEBUG_LINE_2D_WITH_TWO_COLOURS(<<sFlowDiagram.fCursorX, sFlowDiagram.fMouseZoomY, 0.0>>, 
														<<sFlowDiagram.fCursorX, sFlowDiagram.fMouseZoomY + 0.016, 0.0>>,
														0,255,0,iAlpha,
														0,255,0,0)
					DRAW_DEBUG_LINE_2D(<<sFlowDiagram.fCursorX, sFlowDiagram.fMouseZoomY, 0.0>>, 
									<<sFlowDiagram.fCursorX, sFlowDiagram.fCursorY - 0.016, 0.0>>,
									0,255,0,iAlpha)
					DRAW_DEBUG_LINE_2D_WITH_TWO_COLOURS(<<sFlowDiagram.fCursorX, sFlowDiagram.fCursorY - 0.016, 0.0>>, 
														<<sFlowDiagram.fCursorX - 0.009, sFlowDiagram.fCursorY, 0.0>>,
														0,255,0,iAlpha,
														0,255,0,0)
					DRAW_DEBUG_LINE_2D_WITH_TWO_COLOURS(<<sFlowDiagram.fCursorX, sFlowDiagram.fCursorY - 0.016, 0.0>>, 
														<<sFlowDiagram.fCursorX + 0.009, sFlowDiagram.fCursorY, 0.0>>,
														0,255,0,iAlpha,
														0,255,0,0)
				ENDIF	
			ELSE
			
				//Draw mouse crosshair with debug lines.
				DRAW_DEBUG_LINE_2D_WITH_TWO_COLOURS(<<sFlowDiagram.fCursorX, sFlowDiagram.fCursorY - 0.1, 0.0>>, 
												<<sFlowDiagram.fCursorX, sFlowDiagram.fCursorY, 0.0>>,
												0,255,0,0,
												0,255,0,iAlpha)
				DRAW_DEBUG_LINE_2D_WITH_TWO_COLOURS(<<sFlowDiagram.fCursorX, sFlowDiagram.fCursorY, 0.0>>, 
													<<sFlowDiagram.fCursorX, sFlowDiagram.fCursorY + 0.1, 0.0>>,
													0,255,0,iAlpha,
													0,255,0,0)
				DRAW_DEBUG_LINE_2D_WITH_TWO_COLOURS(<<sFlowDiagram.fCursorX - 0.056, sFlowDiagram.fCursorY, 0.0>>, 
													<<sFlowDiagram.fCursorX, sFlowDiagram.fCursorY, 0.0>>,
													0,255,0,0,
													0,255,0,iAlpha)
				DRAW_DEBUG_LINE_2D_WITH_TWO_COLOURS(<<sFlowDiagram.fCursorX, sFlowDiagram.fCursorY, 0.0>>, 
													<<sFlowDiagram.fCursorX + 0.056, sFlowDiagram.fCursorY, 0.0>>,
													0,255,0,iAlpha,
													0,255,0,0)
			ENDIF
		ELSE
			//Calculate alpha.
			iAlpha = FLOOR(TO_FLOAT(255 - sFlowDiagram.iInfoBoxAlpha) * 0.6)
		
			//Draw pad crosshair sprite.
			IF sFlowDiagram.bDrawCursor
				IF HAS_STREAMED_TEXTURE_DICT_LOADED("VisualFlow")
					DRAW_SPRITE("VisualFlow", "crosshair",sFlowDiagram.fCursorX, sFlowDiagram.fCursorY, 0.015, 0.022, 0.0, 255, 255, 255, iAlpha)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC



/// PURPOSE:
///    Query and display the bugstar info box (standardized layout with debug menu) for the currently highlighted element
PROC DISPLAY_FLOW_DIAGRAM_BUGSTAR_INFO_BOX(FLOW_DIAGRAM &sFlowDiagram)
	BOOL bBugsReadyToDisplay = FALSE 
	BOOL bFadingDisplay = sFlowDiagram.iMissionBoxAlpha > 0 // This is so we continue displaying bug info as we fade out
	
	IF NOT sFlowDiagram.bBugstarPercQueryStarted AND NOT sFlowDiagram.bBugstarBugsQueryStarted
		//Is a mission highlighted?
		IF sFlowDiagram.iHighlightedMission != -1
			//Yes, have we loaded its bugstar information?
			IF NOT IS_DEBUG_MENU_ITEM_FLAG_SET(sFlowDiagram.sMenuItemData.iFlags, DM_BIT_BUG_INFO)
				//Load mission's bugstar information.
				sFlowDiagram.sMenuItemData.sMissionID = sFlowDiagram.sElement[sFlowDiagram.iHighlightedMission].sMissionID
				sFlowDiagram.bBugstarInfoQueryStarted = TRUE
				IF BUGSTAR_DETAILED_MISSION_DATA(sFlowDiagram.sBugstarData, sFlowDiagram.sMenuItemData, sFlowDiagram.sMenuItemData.sMissionID)
					SET_DEBUG_MENU_ITEM_FLAG(sFlowDiagram.sMenuItemData.iFlags, DM_BIT_BUG_INFO)
					sFlowDiagram.bBugstarInfoQueryStarted = FALSE
					sFlowDiagram.iMostRecentInfoQuery = sFlowDiagram.iHighlightedMission
				ENDIF
			ELSE
				//Mission highlighted and its data is loaded. Display the info box.
				bBugsReadyToDisplay = TRUE
			ENDIF
			IF NOT sFlowDiagram.bDoingStartMissionCheck
				sFlowDiagram.iMissionBoxAlphaTarget = 255
			ENDIF
		ENDIF
	ENDIF
	
	IF sFlowDiagram.iHighlightedMission != -1 OR bFadingDisplay 
		DRAW_RECT(0.5000, 0.7460, 0.9110, 0.2700, MENU_BG_EDGE_R, MENU_BG_EDGE_G, MENU_BG_EDGE_B, sFlowDiagram.iMissionBoxAlpha)
		DRAW_RECT(0.5000, 0.7460, 0.9060, 0.2630, MENU_BG_R, MENU_BG_G, MENU_BG_B, sFlowDiagram.iMissionBoxAlpha)
		DM_DISPLAY_MENU_ITEM_INFO(sFlowDiagram.sMenuItemData, sFlowDiagram.sBugstarData, sFlowDiagram.sMissionBugs, (NOT bBugsReadyToDisplay) AND (NOT bFadingDisplay), sFlowDiagram.iMissionBoxAlpha)
	ENDIF
	
	IF sFlowDiagram.iHighlightedMission = -1
		sFlowDiagram.iMissionBoxAlphaTarget = 0
	ENDIF
	
	IF NOT sFlowDiagram.bBugstarPercQueryStarted AND NOT sFlowDiagram.bBugstarBugsQueryStarted
		IF (sFlowDiagram.iHighlightedMission = -1 AND sFlowDiagram.iMissionBoxAlpha = 0) OR (sFlowDiagram.iHighlightedMission != sFlowDiagram.iMostRecentInfoQuery AND NOT sFlowDiagram.bBugstarInfoQueryStarted)
			IF IS_BUGSTAR_PROCESS_FLAG_SET(sFlowDiagram.sBugstarData.eProcessFlags, BSTAR_PROCESS_BUGSTAR_QUERY_STARTED)
				CLEANUP_BUGSTAR_QUERY(sFlowDiagram.sBugstarData)
			ENDIF
			CLEAR_DEBUG_MENU_ITEM_FLAG(sFlowDiagram.sMenuItemData.iFlags, DM_BIT_BUG_INFO)
			// We can probably figure out a way to do this less often
			RESET_MENU_ITEM_DATA(sFlowDiagram.sMenuItemData)
		ENDIF
	ENDIF
ENDPROC


PROC DISPLAY_FLOW_DIAGRAM_START_MISSION_CHECK(FLOW_DIAGRAM &sFlowDiagram)
	IF sFlowDiagram.bDoingStartMissionCheck
		IF sFlowDiagram.iInfoBoxAlpha > 0
			//Draw launch mission question box.
			DRAW_RECT(0.5, 0.5, 0.3, 0.15, 10 ,10, 10, CLAMP_INT(sFlowDiagram.iInfoBoxAlpha-20,0,235))
			DRAW_RECT(0.35, 0.5, 0.0014, 0.15, 20 ,20, 20, CLAMP_INT(sFlowDiagram.iInfoBoxAlpha-15,0,240))
			DRAW_RECT(0.65, 0.5, 0.0014, 0.15, 20 ,20, 20, CLAMP_INT(sFlowDiagram.iInfoBoxAlpha-15,0,240))
			DRAW_RECT(0.5, 0.425, 0.3, 0.0014, 20 ,20, 20, CLAMP_INT(sFlowDiagram.iInfoBoxAlpha-15,0,240))
			DRAW_RECT(0.5, 0.575, 0.3, 0.0014, 20 ,20, 20, CLAMP_INT(sFlowDiagram.iInfoBoxAlpha-15,0,240))
			
			//Draw mission name and tag.
			SETUP_FLOW_DIAGRAM_KEY_TEXT(sFlowDiagram.iInfoBoxAlpha)
			TEXT_LABEL_63 tMissionLabelText = "("
			tMissionLabelText += sFlowDiagram.sMenuItemData.sMissionID
			tMissionLabelText += ")"
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.386, 0.445, "STRING", tMissionLabelText)
			SETUP_FLOW_DIAGRAM_KEY_TEXT(sFlowDiagram.iInfoBoxAlpha)
			DISPLAY_TEXT(0.450, 0.445, sFlowDiagram.sMenuItemData.sMissionID)
			
			//Draw queston.
			SETUP_FLOW_DIAGRAM_KEY_TEXT(sFlowDiagram.iInfoBoxAlpha)
			TEXT_LABEL_63 tQuestion = "Launch mission?"
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.45, 0.49, "STRING", tQuestion)
			
			//Draw response buttons.
			SETUP_FLOW_DIAGRAM_KEY_TEXT(sFlowDiagram.iInfoBoxAlpha)
			IF sFlowDiagram.bMouseActive
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.425, 0.525, "STRING", "LMB - Launch        RMB - Cancel")
			ELSE
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.425, 0.525, "STRING", "~PAD_A~ - Launch        ~PAD_B~ - Cancel")
			ENDIF
		ENDIF
	ENDIF
ENDPROC


PROC DISPLAY_FLOW_DIAGRAM_KEY(FLOW_DIAGRAM &sFlowDiagram)
	IF NOT sFlowDiagram.bDoingStartMissionCheck
		IF sFlowDiagram.iInfoBoxAlpha > 0
			//Draw key box.
			DRAW_RECT(0.5, 0.5, 0.4, 0.76, 10 ,10, 10, CLAMP_INT(sFlowDiagram.iInfoBoxAlpha-20,0,235))
			DRAW_RECT(0.3, 0.5, 0.0014, 0.76, 20 ,20, 20, CLAMP_INT(sFlowDiagram.iInfoBoxAlpha-15,0,240))
			DRAW_RECT(0.7, 0.5, 0.0014, 0.76, 20 ,20, 20, CLAMP_INT(sFlowDiagram.iInfoBoxAlpha-15,0,240))
			DRAW_RECT(0.5, 0.12, 0.4, 0.0014, 20 ,20, 20, CLAMP_INT(sFlowDiagram.iInfoBoxAlpha-15,0,240))
			DRAW_RECT(0.5, 0.88, 0.4, 0.0014, 20 ,20, 20, CLAMP_INT(sFlowDiagram.iInfoBoxAlpha-15,0,240))
			
			//Draw title.
			SETUP_FLOW_DIAGRAM_KEY_TEXT(sFlowDiagram.iInfoBoxAlpha)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.48, 0.167, "STRING", "KEY")
			
			//Draw sample mission elements.
			DRAW_RECT(	0.42, 
						0.26, 
						0.21, 
						0.0015, 
						FD_COL_R_LINE_FLOW, FD_COL_G_LINE_FLOW, FD_COL_B_LINE_FLOW, sFlowDiagram.iInfoBoxAlpha)
						
			DRAW_RECT( 	0.345, 
						0.26, 	
						FD_MISSION_ELEMENT_WIDTH * 1.8, 
						FD_MISSION_ELEMENT_HEIGHT * 1.8, 
						255, 0, 0, sFlowDiagram.iInfoBoxAlpha)
			SETUP_FLOW_DIAGRAM_BLACK_KEY_TEXT(sFlowDiagram.iInfoBoxAlpha)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.335,0.25,"STRING","<50%")
			
				
			DRAW_RECT( 	0.395, 
						0.26, 	
						FD_MISSION_ELEMENT_WIDTH * 1.8, 
						FD_MISSION_ELEMENT_HEIGHT * 1.8, 
						255, 191, 0, sFlowDiagram.iInfoBoxAlpha)
			SETUP_FLOW_DIAGRAM_BLACK_KEY_TEXT(sFlowDiagram.iInfoBoxAlpha)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.385,0.25,"STRING","<70%")
						
			DRAW_RECT( 	0.445, 
						0.26, 	
						FD_MISSION_ELEMENT_WIDTH * 1.8, 
						FD_MISSION_ELEMENT_HEIGHT * 1.8, 
						0, 255, 0, sFlowDiagram.iInfoBoxAlpha)	
			SETUP_FLOW_DIAGRAM_BLACK_KEY_TEXT(sFlowDiagram.iInfoBoxAlpha)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.431,0.25,"STRING","<100%")
						
			DRAW_RECT( 	0.495, 
						0.26, 	
						FD_MISSION_ELEMENT_WIDTH * 1.8, 
						FD_MISSION_ELEMENT_HEIGHT * 1.8, 
						255, 255, 255, sFlowDiagram.iInfoBoxAlpha)
			SETUP_FLOW_DIAGRAM_BLACK_KEY_TEXT(sFlowDiagram.iInfoBoxAlpha)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.485,0.25,"STRING","100%")
						
			SETUP_FLOW_DIAGRAM_KEY_TEXT(sFlowDiagram.iInfoBoxAlpha)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.54, 0.247, "STRING", "Mission")
									
			//Draw sample communication element.
			DRAW_RECT(	0.42, 
						0.36, 
						0.12, 
						0.0015, 
						FD_COL_R_LINE_FLOW, FD_COL_G_LINE_FLOW, FD_COL_B_LINE_FLOW, sFlowDiagram.iInfoBoxAlpha)
						
			DRAW_RECT(	0.42, 	
						0.36,
						FD_PHONE_ELEMENT_WIDTH * 1.8, 
						FD_PHONE_ELEMENT_HEIGHT * 1.8, 
						FD_COL_R_PHONE, FD_COL_G_PHONE, FD_COL_B_PHONE, sFlowDiagram.iInfoBoxAlpha)
						
			SETUP_FLOW_DIAGRAM_KEY_TEXT(sFlowDiagram.iInfoBoxAlpha)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.54, 0.347, "STRING", "Phonecall / Text Message")
			
			//Draw sample planning board element.
			DRAW_RECT(	0.42, 
						0.46, 
						0.12, 
						0.0015, 
						FD_COL_R_LINE_FLOW, FD_COL_G_LINE_FLOW, FD_COL_B_LINE_FLOW, sFlowDiagram.iInfoBoxAlpha)
						
			DRAW_RECT(	0.42, 	
						0.46,
						FD_PLANNING_ELEMENT_WIDTH * 1.8, 
						FD_PLANNING_ELEMENT_HEIGHT * 1.8, 
						FD_COL_R_PLANNING, FD_COL_G_PLANNING, FD_COL_B_PLANNING, sFlowDiagram.iInfoBoxAlpha)
						
			SETUP_FLOW_DIAGRAM_KEY_TEXT(sFlowDiagram.iInfoBoxAlpha)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.54, 0.447, "STRING", "Board Interaction")
						
			//Draw sample sync element.
			SETUP_FLOW_DIAGRAM_KEY_TEXT(sFlowDiagram.iInfoBoxAlpha)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.345, 0.527, "STRING", "A")
			DRAW_RECT(	0.42, 
						0.54, 
						0.12, 
						0.0015, 
						FD_COL_R_LINE_FLOW, FD_COL_G_LINE_FLOW, FD_COL_B_LINE_FLOW, sFlowDiagram.iInfoBoxAlpha)
						
			
			SETUP_FLOW_DIAGRAM_KEY_TEXT(sFlowDiagram.iInfoBoxAlpha)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.345, 0.637, "STRING", "B")
			
			DRAW_RECT(	0.42, 
						0.64, 
						0.12, 
						0.0015, 
						FD_COL_R_LINE_FLOW, FD_COL_G_LINE_FLOW, FD_COL_B_LINE_FLOW, sFlowDiagram.iInfoBoxAlpha)
						
			DRAW_RECT(	0.42, 
						0.64, 
						FD_LINK_ELEMENT_WIDTH * 1.8, 
						FD_LINK_ELEMENT_HEIGHT * 1.8, 
						FD_COL_R_SYNC, FD_COL_G_SYNC, FD_COL_B_SYNC, sFlowDiagram.iInfoBoxAlpha)
						
			DRAW_RECT(	0.42, 
						0.59, 
						0.0012, 
						0.1,
						FD_COL_R_SYNC, FD_COL_G_SYNC, FD_COL_B_SYNC, sFlowDiagram.iInfoBoxAlpha)
						
			SETUP_FLOW_DIAGRAM_KEY_TEXT(sFlowDiagram.iInfoBoxAlpha)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.54, 0.577, "STRING", "Strand B Sync to Strand A")
								
			//Draw sample activate element.
			SETUP_FLOW_DIAGRAM_KEY_TEXT(sFlowDiagram.iInfoBoxAlpha)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.345, 0.697, "STRING", "A")	
			DRAW_RECT(	0.42, 
						0.71, 
						0.12, 
						0.0015, 
						FD_COL_R_LINE_FLOW, FD_COL_G_LINE_FLOW, FD_COL_B_LINE_FLOW, sFlowDiagram.iInfoBoxAlpha)
			
			SETUP_FLOW_DIAGRAM_KEY_TEXT(sFlowDiagram.iInfoBoxAlpha)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.396, 0.797, "STRING", "B")
			DRAW_RECT(	0.45, 
						0.81, 
						0.06, 
						0.0015, 
						FD_COL_R_LINE_FLOW, FD_COL_G_LINE_FLOW, FD_COL_B_LINE_FLOW, sFlowDiagram.iInfoBoxAlpha)
								
			DRAW_RECT(	0.42, 
						0.81, 
						FD_LINK_ELEMENT_WIDTH * 1.8, 
						FD_LINK_ELEMENT_HEIGHT * 1.8, 
						FD_COL_R_LINE_FLOW, FD_COL_G_LINE_FLOW, FD_COL_B_LINE_FLOW, sFlowDiagram.iInfoBoxAlpha)
						
			DRAW_RECT(	0.42, 
						0.76, 
						0.0012, 
						0.1,
						FD_COL_R_LINE_FLOW, FD_COL_G_LINE_FLOW, FD_COL_B_LINE_FLOW, sFlowDiagram.iInfoBoxAlpha)
						
			SETUP_FLOW_DIAGRAM_KEY_TEXT(sFlowDiagram.iInfoBoxAlpha)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.54, 0.747, "STRING", "Strand A Activate Strand B")
			
		ENDIF
	ENDIF
ENDPROC

PROC DISPLAY_STRAND_BASE_LINE(FLOW_DIAGRAM &sFlowDiagram, STRANDS eStrand)
	FLOAT fYCoord = GET_SCREEN_Y_COORD(sFlowDiagram, GET_STRAND_Y_POSITION(sFlowDiagram, eStrand))
	CDEBUG1LN(DEBUG_FLOW_DIAGRAM_DRAW, "DISPLAY_STRAND_BASE_LINE for ", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(eStrand), " at ", fYCoord)
	
	//Cull on the y axis.
	IF 	fYCoord >= 0.0 AND fYCoord <= 1.0
		FLOAT fMinXPos, fMaxXPos
		IF NOT IS_STRAND_SYNC_ONLY(eStrand)
			fMinXPos = GET_SCREEN_X_COORD(sFlowDiagram, sFlowDiagram.sStrandBounds[eStrand].startPos)
			fMaxXPos = GET_SCREEN_X_COORD(sFlowDiagram, sFlowDiagram.sElement[sFlowDiagram.sStrandBounds[eStrand].upper].xPos)
			
			//Cull on the x axis.
			IF (fMinXPos >= 0.0 AND fMinXPos <= 1.0)
			OR (fMaxXPos >= 0.0 AND fMaxXPos <= 1.0)
			OR (fMinXPos <= 0.0 AND fMaxXPos >= 1.0)
				FLOAT fLengthX = fMaxXPos - fMinXPos
				FLOAT fCenterX = fMinXPos + (fLengthX*0.5)
				
				DRAW_RECT(	fCenterX, 
							fYCoord, 
							fLengthX, 
							0.0015, 
							FD_COL_R_LINE_FLOW, FD_COL_G_LINE_FLOW, FD_COL_B_LINE_FLOW, 255)
			ENDIF
		ELSE
			// For RCM strands, walk the strand, and draw a line between await/activate and sync
			INT flowElementIndex
			BOOL bFoundLink
			fMinXPos = GET_SCREEN_X_COORD(sFlowDiagram, sFlowDiagram.sStrandBounds[eStrand].startPos)
			REPEAT sFlowDiagram.sStrandBounds[eStrand].upper - sFlowDiagram.sStrandBounds[eStrand].lower + 1 flowElementIndex
				IF sFlowDiagram.sElement[sFlowDiagram.sStrandBounds[eStrand].lower + flowElementIndex].strand = eStrand
					IF sFlowDiagram.sElement[sFlowDiagram.sStrandBounds[eStrand].lower + flowElementIndex].type = FD_ELEMENT_TYPE_AWAIT_SYNC
						fMinXPos = GET_SCREEN_X_COORD(sFlowDiagram, sFlowDiagram.sElement[sFlowDiagram.sStrandBounds[eStrand].lower + flowElementIndex].xPos)
					ENDIF
					IF sFlowDiagram.sElement[sFlowDiagram.sStrandBounds[eStrand].lower + flowElementIndex].type = FD_ELEMENT_TYPE_SEND_SYNC
						fMaxXPos = GET_SCREEN_X_COORD(sFlowDiagram, sFlowDiagram.sElement[sFlowDiagram.sStrandBounds[eStrand].lower + flowElementIndex].xPos)
						bFoundLink = TRUE
					ENDIF
					IF bFoundLink
						bFoundLink = FALSE
						//Cull on the x axis.
						IF (fMinXPos >= 0.0 AND fMinXPos <= 1.0)
						OR (fMaxXPos >= 0.0 AND fMaxXPos <= 1.0)
						OR (fMinXPos <= 0.0 AND fMaxXPos >= 1.0)
							FLOAT fLengthX = fMaxXPos - fMinXPos
							FLOAT fCenterX = fMinXPos + (fLengthX*0.5)
							
							DRAW_RECT(	fCenterX, 
										fYCoord, 
										fLengthX, 
										0.0015, 
										FD_COL_R_LINE_FLOW, FD_COL_G_LINE_FLOW, FD_COL_B_LINE_FLOW, 255)
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
		
		ENDIF
	ENDIF
ENDPROC


PROC DISPLAY_FLOW_DIAGRAM_ACTIVATE_ELEMENT(FLOW_DIAGRAM &sFlowDiagram, FLOW_DIAGRAM_ELEMENT &sActivateElement, FLOAT fXCoord)
	CDEBUG1LN(DEBUG_FLOW_DIAGRAM_DRAW, "DISPLAY_FLOW_DIAGRAM_ACTIVATE_ELEMENT for ", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(sActivateElement.strand), " at ", fXCoord)
	
	//Cull on x axis.
	FLOAT fHalfScaled = FD_LINK_ELEMENT_WIDTH * 0.5 * sFlowDiagram.fZoom
	IF 	(fXCoord + fHalfScaled) >= 0.0 
		IF (fXCoord - fHalfScaled) <= 1.0
		
			//Convert y grid coords to screen coords.
			FLOAT fYCoordA = GET_SCREEN_Y_COORD(sFlowDiagram, sActivateElement.yPosA)
			FLOAT fYCoordB = GET_SCREEN_Y_COORD(sFlowDiagram, sActivateElement.yPosB)
		
			//Cull on y axis.
			IF (fYCoordA >= 0.0 AND fYCoordA <= 1.0)
			OR (fYCoordB >= 0.0 AND fYCoordB <= 1.0)
			OR (fYCoordA <= 0.0 AND fYCoordB >= 1.0)
			OR (fYCoordB <= 0.0 AND fYCoordA >= 1.0)
				FLOAT fDist = fYCoordB - fYCoordA
							
				DRAW_RECT(	fXCoord, 
							fYCoordB, 
							FD_LINK_ELEMENT_WIDTH * sFlowDiagram.fZoom, 
							FD_LINK_ELEMENT_HEIGHT * sFlowDiagram.fZoom, 
							FD_COL_R_LINE_FLOW, FD_COL_G_LINE_FLOW, FD_COL_B_LINE_FLOW, 255)
									
				DRAW_RECT(	fXCoord, 
							fYCoordA + 0.5*fDist, 
							0.0012, 
							ABSF(fDist), 
							FD_COL_R_LINE_FLOW, FD_COL_G_LINE_FLOW, FD_COL_B_LINE_FLOW, 255)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Draw a mission element. If text is required, add to DISPLAY_FLOW_DIAGRAM_MISSION_ELEMENT_CUSTOM() 
PROC DISPLAY_FLOW_DIAGRAM_MISSION_ELEMENT(FLOW_DIAGRAM &sFlowDiagram, FLOW_DIAGRAM_ELEMENT &sMissionElement, FLOW_DIAGRAM_ELEMENT_CUSTOM &sMissionElementCustom, FLOAT fXCoord, INT elementIndex, BOOL &bHighlighted)
	CDEBUG1LN(DEBUG_FLOW_DIAGRAM_DRAW, "DISPLAY_FLOW_DIAGRAM_MISSION_ELEMENT for ", sMissionElement.mainData, " at ", fXCoord)
	
	//Convert y grid coord to screen coord.
	FLOAT fYCoord = GET_SCREEN_Y_COORD(sFlowDiagram, sMissionElement.yPosA)
	CDEBUG1LN(DEBUG_FLOW_DIAGRAM_DRAW, "DISPLAY_FLOW_DIAGRAM_MISSION_ELEMENT at ", fXCoord, ",", fYCoord)
	
	//Cull on x axis.
	FLOAT fHalfScaledWidth = FD_MISSION_ELEMENT_WIDTH * 0.5 * sFlowDiagram.fZoom
	IF 	(fXCoord + fHalfScaledWidth) >= 0.0 
		IF (fXCoord - fHalfScaledWidth) <= 1.0
		
			//Cull on y axis.
			FLOAT fHalfScaledHeight = FD_MISSION_ELEMENT_HEIGHT * 0.5 * sFlowDiagram.fZoom
			IF (fYCoord + fHalfScaledHeight) >= 0.0 
				IF (fYCoord - fHalfScaledHeight) <= 1.0
				
					//Check if the cursor is pointing directly at this mission.
					IF sFlowDiagram.fCursorX > (fXCoord - fHalfScaledWidth)
						IF sFlowDiagram.fCursorX < (fXCoord + fHalfScaledWidth)
							IF sFlowDiagram.fCursorY > (fYCoord - fHalfScaledWidth)
								IF sFlowDiagram.fCursorY < (fYCoord + fHalfScaledWidth)
									sFlowDiagram.iHighlightedMission = elementIndex
									bHighlighted = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					//Draw a white border around highlighted missions.
					IF sFlowDiagram.iFrontAlpha > 0
						IF sMissionElement.highlight
							DRAW_RECT(	fXCoord, 
										fYCoord, 
										FD_MISSION_ELEMENT_WIDTH * 1.25 * sFlowDiagram.fZoom, 
										FD_MISSION_ELEMENT_HEIGHT * 1.25 *sFlowDiagram.fZoom, 
										120, 
										0, 
										0, 
										sFlowDiagram.iFrontAlpha)
						ENDIF
					ENDIF

					IF sMissionElement.completed
						DRAW_RECT(	fXCoord, 
									fYCoord, 
									FD_MISSION_ELEMENT_WIDTH * sFlowDiagram.fZoom, 
									FD_MISSION_ELEMENT_HEIGHT * sFlowDiagram.fZoom, 
									FLOOR(TO_FLOAT(sMissionElement.colourR) * 0.16), 
									FLOOR(TO_FLOAT(sMissionElement.colourG) * 0.16), 
									FLOOR(TO_FLOAT(sMissionElement.colourB) * 0.16), 
									255)
					ELSE
						DRAW_RECT(	fXCoord, 
									fYCoord, 
									FD_MISSION_ELEMENT_WIDTH * sFlowDiagram.fZoom, 
									FD_MISSION_ELEMENT_HEIGHT * sFlowDiagram.fZoom, 
									sMissionElement.colourR, 
									sMissionElement.colourG, 
									sMissionElement.colourB, 
									255)
					ENDIF
										
					//Draw mission name text.
					IF sFlowDiagram.fZoom > 1.5
						DISPLAY_FLOW_DIAGRAM_MISSION_ELEMENT_CUSTOM(sFlowDiagram, sMissionElement, sMissionElementCustom, fXCoord, fYCoord)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC



PROC DISPLAY_FLOW_DIAGRAM_SEND_SYNC_ELEMENT(FLOW_DIAGRAM &sFlowDiagram, FLOW_DIAGRAM_ELEMENT &sSendSyncElement, FLOAT fXCoord)
	CDEBUG1LN(DEBUG_FLOW_DIAGRAM_DRAW, "DISPLAY_FLOW_DIAGRAM_MISSION_ELEMENT for ", sSendSyncElement.mainData, " at ", fXCoord)
	
	//Convert y grid coords to screen coords.
	FLOAT fYCoordA = GET_SCREEN_Y_COORD(sFlowDiagram, sSendSyncElement.yPosA)
	FLOAT fYCoordB = GET_SCREEN_Y_COORD(sFlowDiagram, sSendSyncElement.yPosB)
	
	//Cull on x axis.
	FLOAT fHalfScaled = FD_LINK_ELEMENT_WIDTH * 0.5 * sFlowDiagram.fZoom
	IF 	(fXCoord + fHalfScaled) >= 0.0 
		IF (fXCoord - fHalfScaled) <= 1.0
		
			//Cull on the y axis
			IF (fYCoordA >= 0.0 AND fYCoordA <= 1.0)
			OR (fYCoordB >= 0.0 AND fYCoordB <= 1.0)
			OR (fYCoordA <= 0.0 AND fYCoordB >= 1.0)
			OR (fYCoordB <= 0.0 AND fYCoordA >= 1.0)
				FLOAT fDist =  fYCoordB - fYCoordA
				
				DRAW_RECT(	fXCoord, 
							fYCoordB, 
							FD_LINK_ELEMENT_WIDTH * sFlowDiagram.fZoom, 
							FD_LINK_ELEMENT_HEIGHT * sFlowDiagram.fZoom, 
							FD_COL_R_SYNC, FD_COL_G_SYNC, FD_COL_B_SYNC, 255)
									
				DRAW_RECT(	fXCoord, 
							fYCoordA + 0.5*fDist, 
							0.0012, 
							ABSF(fDist),
							FD_COL_R_SYNC, FD_COL_G_SYNC, FD_COL_B_SYNC, 255)
			ENDIF
		ENDIF
	ENDIF
ENDPROC



/// PURPOSE:
///    Displays a diagram element. All standard types are handled - to draw project-specific elements, use DISPLAY_DIAGRAM_ELEMENT_CUSTOM
PROC DISPLAY_DIAGRAM_ELEMENT(FLOW_DIAGRAM &sFlowDiagram, FLOW_DIAGRAM_ELEMENT &sElement, FLOW_DIAGRAM_ELEMENT_CUSTOM &sElementCustom, INT elementIndex, BOOL &bHighlighted)

	FLOAT fXCoord = GET_SCREEN_X_COORD(sFlowDiagram, sElement.xPos)

	SWITCH (sElement.type)
		CASE FD_ELEMENT_TYPE_ACTIVATE
			DISPLAY_FLOW_DIAGRAM_ACTIVATE_ELEMENT(sFlowDiagram, sElement, fXCoord)
		BREAK

		CASE FD_ELEMENT_TYPE_SEND_SYNC
			DISPLAY_FLOW_DIAGRAM_SEND_SYNC_ELEMENT(sFlowDiagram, sElement, fXCoord)
		BREAK
		
		CASE FD_ELEMENT_TYPE_MISSION
			DISPLAY_FLOW_DIAGRAM_MISSION_ELEMENT(sFlowDiagram, sElement, sElementCustom, fXCoord, elementIndex, bHighlighted)
		BREAK
		
		DEFAULT
			DISPLAY_DIAGRAM_ELEMENT_CUSTOM(sFlowDiagram, sElement, sElementCustom, fXCoord, elementIndex, bHighlighted)
		BREAK
		
	ENDSWITCH
	
ENDPROC


PROC DISPLAY_FLOW_DIAGRAM_COLOUR_MODE(FLOW_DIAGRAM &sFlowDiagram)
	FLOAT fXPos = 0.16
	FLOAT fYPos = 0.057
	FLOAT fBoxWidth = 0.064
	FLOAT fBoxHeight = 0.02
	FLOAT fStringWidth
	
	//Display "Colour Modes" heading.
	SETUP_FLOW_DIAGRAM_COLOUR_MODE_TEXT(255)
	fStringWidth = GET_STRING_WIDTH_WITH_STRING("STRING","COLOUR MODE")
	DISPLAY_TEXT_WITH_LITERAL_STRING(fXpos+0.076+(fBoxWidth - fStringWidth)*0.5, fYPos - 0.021, "STRING", "COLOUR MODE")

	//Display "% Complete" box.
	DRAW_DEBUG_LINE_2D(<<fXPos, fYPos, 0.00>>, <<fXPos + fBoxWidth, fYPos, 0.00>>, 20, 20, 20, 255)
	DRAW_DEBUG_LINE_2D(<<fXPos, fYPos + fBoxHeight, 0.00>>, <<fXPos + fBoxWidth, fYPos + fBoxHeight, 0.00>>, 20, 20, 20, 255)
	DRAW_DEBUG_LINE_2D(<<fXPos, fYPos, 0.00>>, <<fXPos, fYPos + fBoxHeight, 0.00>>, 20, 20, 20, 255)
	DRAW_DEBUG_LINE_2D(<<fXPos + fBoxWidth, fYPos, 0.00>>, <<fXPos + fBoxWidth, fYPos + fBoxHeight, 0.00>>, 20, 20, 20, 255)
	IF sFlowDiagram.eColourMode = FD_COLOUR_MODE_COMPLETE_PERCENTAGE
		DRAW_RECT_FROM_CORNER(fXpos, fYPos, fBoxWidth, fBoxHeight, FD_COL_R_TEXT_MAIN, FD_COL_G_TEXT_MAIN, FD_COL_B_TEXT_MAIN, 20)
	ENDIF
	//Display "% Complete" text.
	SETUP_FLOW_DIAGRAM_COLOUR_MODE_TEXT(255)
	fStringWidth = GET_STRING_WIDTH_WITH_STRING("STRING","% Complete")
	DISPLAY_TEXT_WITH_LITERAL_STRING(fXpos+(fBoxWidth - fStringWidth)*0.5, fYPos+0.0008, "STRING", "% Complete")
	
	fXPos += 0.076
	
	//Display "Bug Count" box.
	DRAW_DEBUG_LINE_2D(<<fXPos, fYPos, 0.00>>, <<fXPos + fBoxWidth, fYPos, 0.00>>, 20, 20, 20, 255)
	DRAW_DEBUG_LINE_2D(<<fXPos, fYPos + fBoxHeight, 0.00>>, <<fXPos + fBoxWidth, fYPos + fBoxHeight, 0.00>>, 20, 20, 20, 255)
	DRAW_DEBUG_LINE_2D(<<fXPos, fYPos, 0.00>>, <<fXPos, fYPos + fBoxHeight, 0.00>>, 20, 20, 20, 255)
	DRAW_DEBUG_LINE_2D(<<fXPos + fBoxWidth, fYPos, 0.00>>, <<fXPos + fBoxWidth, fYPos + fBoxHeight, 0.00>>, 20, 20, 20, 255)
	IF sFlowDiagram.eColourMode = FD_COLOUR_MODE_BUG_COUNT
		DRAW_RECT_FROM_CORNER(fXpos, fYPos, fBoxWidth, fBoxHeight, FD_COL_R_TEXT_MAIN, FD_COL_G_TEXT_MAIN, FD_COL_B_TEXT_MAIN, 20)
	ENDIF
	
	//Display "Bug Count" text.
	SETUP_FLOW_DIAGRAM_COLOUR_MODE_TEXT(255)
	fStringWidth = GET_STRING_WIDTH_WITH_STRING("STRING","Bug Count")
	DISPLAY_TEXT_WITH_LITERAL_STRING(fXpos+(fBoxWidth - fStringWidth)*0.5, fYPos+0.0008, "STRING", "Bug Count")
	
	fXPos += 0.076
	
	//Display "Assets Required" box.
	DRAW_DEBUG_LINE_2D(<<fXPos, fYPos, 0.00>>, <<fXPos + fBoxWidth, fYPos, 0.00>>, 20, 20, 20, 255)
	DRAW_DEBUG_LINE_2D(<<fXPos, fYPos + fBoxHeight, 0.00>>, <<fXPos + fBoxWidth, fYPos + fBoxHeight, 0.00>>, 20, 20, 20, 255)
	DRAW_DEBUG_LINE_2D(<<fXPos, fYPos, 0.00>>, <<fXPos, fYPos + fBoxHeight, 0.00>>, 20, 20, 20, 255)
	DRAW_DEBUG_LINE_2D(<<fXPos + fBoxWidth, fYPos, 0.00>>, <<fXPos + fBoxWidth, fYPos + fBoxHeight, 0.00>>, 20, 20, 20, 255)
	IF sFlowDiagram.eColourMode = FD_COLOUR_MODE_ASSETS_REQUIRED
		DRAW_RECT_FROM_CORNER(fXpos, fYPos, fBoxWidth, fBoxHeight, FD_COL_R_TEXT_MAIN, FD_COL_G_TEXT_MAIN, FD_COL_B_TEXT_MAIN, 20)
	ENDIF
	//Display "Assets Required" text.
	SETUP_FLOW_DIAGRAM_COLOUR_MODE_TEXT(255)
	fStringWidth = GET_STRING_WIDTH_WITH_STRING("STRING","Assets Required")
	DISPLAY_TEXT_WITH_LITERAL_STRING(fXpos+(fBoxWidth - fStringWidth)*0.5, fYPos+0.0008, "STRING", "Assets Required")
ENDPROC


PROC DISPLAY_FLOW_DIAGRAM_HELP_TEXT(FLOW_DIAGRAM &sFlowDiagram)
	//Draw the help text background.
	DRAW_RECT(0.525, 0.045, 0.76, 0.09, 12 ,12, 12, 235)
	DRAW_RECT(0.525, 0.09, 0.76, 0.002, 20 ,20, 20, 255)
	DRAW_RECT(0.145, 0.045, 0.0014, 0.09, 20 ,20, 20, 255)
	DRAW_RECT(0.905, 0.045, 0.0014, 0.09, 20 ,20, 20, 255)

	SETUP_FLOW_DIAGRAM_HELP_TEXT(150)
	IF sFlowDiagram.bMouseActive
		DISPLAY_TEXT_WITH_LITERAL_STRING(	0.42, 
											0.043,
											"STRING",
											"Hold RMB - Zoom       LMB - Activate Mission       Hold LMB Over Blank Space - Display Key       LMB+RMB - Exit")
	ELSE
		DISPLAY_TEXT_WITH_LITERAL_STRING(	0.42, 
											0.043,
											"STRING",
											"~PAD_LSTICK_ALL~-Navigate       ~PAD_RSTICK_UPDOWN~-Zoom       ~PAD_A~-Activate Mission       ~PAD_X~-Display Key       ~PAD_LB~/~PAD_RB~-Switch Colour Mode       ~PAD_B~-Exit")
	ENDIF
ENDPROC


PROC DISPLAY_FLOW_DIAGRAM(FLOW_DIAGRAM &sFlowDiagram)
	INT index
	
	REPEAT MAX_STRANDS index
		DISPLAY_STRAND_BASE_LINE(sFlowDiagram, INT_TO_ENUM(STRANDS, index))
	ENDREPEAT
	
	BOOL bHighlighted = FALSE
	REPEAT sFlowDiagram.iElementCount index
		DISPLAY_DIAGRAM_ELEMENT(sFlowDiagram, sFlowDiagram.sElement[index], sFlowDiagram.sElementCustom[index], index, bHighlighted)
	ENDREPEAT
	IF NOT bHighlighted
		sFlowDiagram.iHighlightedMission = -1
	ENDIF
	
ENDPROC



//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════╡ Diagram Activation/Deactivation ╞═════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC ACTIVATE_FLOW_DIAGRAM(FLOW_DIAGRAM &sFlowDiagram)

	DISPLAY_RADAR(FALSE)
	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
	
	ACTIVATE_FLOW_DIAGRAM_CUSTOM(sFlowDiagram)
	
	sFlowDiagram.camFlowDiagram = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
	SET_CAM_PARAMS(	sFlowDiagram.camFlowDiagram, 
					<< -160.6632, -1072.1438, -1615.4709 >>, 
					<< -89.4999, -0.2863, 58.1189 >>,
					45.0000)
	SET_CAM_NEAR_CLIP(sFlowDiagram.camFlowDiagram,0.01)
	SET_CAM_FAR_CLIP(sFlowDiagram.camFlowDiagram,0.02)
	RENDER_SCRIPT_CAMS(TRUE,FALSE)
	
	IF NOT IS_PLAYER_DEAD(PLAYER_ID())
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
		ENDIF
	ENDIF
	
	CALCULATE_SCREEN_PIXEL_DIVISIONS(sFlowDiagram) //Precalculate expensive screen pixel divisions once. Used by roundToPixel().
	
	//Set the last mouse position to the current mouse positon to start.
	GET_MOUSE_POSITION(sFlowDiagram.fLastMouseX, sFlowDiagram.fLastMouseY)
	
	//Center the cursor.
	sFlowDiagram.fCursorX = 0.5
	sFlowDiagram.fCursorXTarget = 0.5
	sFlowDiagram.fCursorY = 0.5
	sFlowDiagram.fCursorYTarget = 0.5
	
	IF g_flowDiagram.bFlowDiagramPositioned
		//Update which missions are completed.
		UPDATE_DIAGRAM_COMPLETED_MISSIONS(sFlowDiagram)
	ENDIF
	
	sFlowDiagram.bMouseActive = FALSE
	g_flowDiagram.bFlowDiagramActive = TRUE
ENDPROC



PROC DEACTIVATE_FLOW_DIAGRAM(FLOW_DIAGRAM &sFlowDiagram)
	IF g_flowDiagram.bFlowDiagramActive
		DISPLAY_RADAR(TRUE)
		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(FALSE)
		DEACTIVATE_FLOW_DIAGRAM_CUSTOM(sFlowDiagram)
		
		RENDER_SCRIPT_CAMS(FALSE,FALSE)
		DESTROY_CAM(sFlowDiagram.camFlowDiagram)
		
		IF NOT IS_PLAYER_DEAD(PLAYER_ID())
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			ENDIF
		ENDIF
		g_flowDiagram.bFlowDiagramActive = FALSE
	ENDIF
ENDPROC


//╒═════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════╡ Diagram Input Management ╞═════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛


PROC UPDATE_FLOW_DIAGRAM_BUMP_SCROLLING(FLOW_DIAGRAM &sFlowDiagram)
	FLOAT fBumpMag
	
	//Check for X axis bump scrolling.
	IF sFlowDiagram.fCursorX < FD_BUMP_SCROLLING_EDGE_START
		fBumpMag = (FD_BUMP_SCROLLING_EDGE_START - sFlowDiagram.fCursorX) / FD_BUMP_SCROLLING_EDGE_START * FD_X_SCROLL_RATE * 1.2 / sFlowDiagram.fZoom
		sFlowDiagram.fXOffsetTarget = sFlowDiagram.fXOffsetTarget -@ fBumpMag
		sFlowDiagram.fXOffsetTarget = CLAMP(sFlowDiagram.fXOffsetTarget, sFlowDiagram.fMinXOffset, sFlowDiagram.fMaxXOffset)
	ELIF sFlowDiagram.fCursorX > (1.0 - FD_BUMP_SCROLLING_EDGE_START)
		fBumpMag = (FD_BUMP_SCROLLING_EDGE_START - (1.0 - sFlowDiagram.fCursorX)) / FD_BUMP_SCROLLING_EDGE_START * FD_X_SCROLL_RATE * 1.2 / sFlowDiagram.fZoom
		sFlowDiagram.fXOffsetTarget = sFlowDiagram.fXOffsetTarget +@ fBumpMag
		sFlowDiagram.fXOffsetTarget = CLAMP(sFlowDiagram.fXOffsetTarget, sFlowDiagram.fMinXOffset, sFlowDiagram.fMaxXOffset)
	ENDIF
	
	//Check for Y axis bump scrolling.
	IF sFlowDiagram.fCursorY < FD_BUMP_SCROLLING_EDGE_START
		fBumpMag = (FD_BUMP_SCROLLING_EDGE_START - sFlowDiagram.fCursorY) / FD_BUMP_SCROLLING_EDGE_START * FD_Y_SCROLL_RATE * 1.2 / sFlowDiagram.fZoom
		sFlowDiagram.fYOffsetTarget = sFlowDiagram.fYOffsetTarget -@ fBumpMag
		sFlowDiagram.fYOffsetTarget = CLAMP(sFlowDiagram.fYOffsetTarget, sFlowDiagram.fMinYOffset, sFlowDiagram.fMaxYOffset)
	ELIF sFlowDiagram.fCursorY > (1.0 - FD_BUMP_SCROLLING_EDGE_START)
		fBumpMag = (FD_BUMP_SCROLLING_EDGE_START - (1.0 - sFlowDiagram.fCursorY)) / FD_BUMP_SCROLLING_EDGE_START * FD_Y_SCROLL_RATE * 1.2 / sFlowDiagram.fZoom
		sFlowDiagram.fYOffsetTarget = sFlowDiagram.fYOffsetTarget +@ fBumpMag
		sFlowDiagram.fYOffsetTarget = CLAMP(sFlowDiagram.fYOffsetTarget, sFlowDiagram.fMinYOffset, sFlowDiagram.fMaxYOffset)
	ENDIF

ENDPROC


PROC MANAGE_FLOW_DIAGRAM_INPUT(FLOW_DIAGRAM &sFlowDiagram, FLOW_LAUNCHER_VARS &flowLauncher)
	INT iStickPositions[4]
	FLOAT fMouseX, fMouseY
	
	//Are we doing a launch mission check?
	IF NOT sFlowDiagram.bDoingStartMissionCheck
	
		//Check for selection of mission.
		IF IS_BUTTON_JUST_PRESSED(PAD1,INT_TO_ENUM(PAD_BUTTON_NUMBER, ENUM_TO_INT(FD_BUTTON_SELECT)))
		OR IS_MOUSE_BUTTON_JUST_PRESSED(MB_LEFT_BTN)
		OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_RETURN)
			IF sFlowDiagram.iHighlightedMission != -1
				sFlowDiagram.bDoingStartMissionCheck = TRUE
				
				//Fade in the mission check box.
				sFlowDiagram.iFrontAlphaTarget = 0
				sFlowDiagram.iInfoBoxAlphaTarget = 255
				sFlowDiagram.iMissionBoxAlphaTarget = 0
				
				//Stop the diagram from drifting.
				sFlowDiagram.fXOffsetTarget = sFlowDiagram.fXOffset
				sFlowDiagram.fYOffsetTarget = sFlowDiagram.fYOffset
				sFlowDiagram.fZoomTarget = sFlowDiagram.fZoom  
				
				EXIT
			ENDIF
		ENDIF
		
		//Check for exiting.
		IF IS_BUTTON_JUST_PRESSED(PAD1, INT_TO_ENUM(PAD_BUTTON_NUMBER, ENUM_TO_INT(FD_BUTTON_EXIT)))
		OR (IS_MOUSE_BUTTON_PRESSED(MB_LEFT_BTN) AND IS_MOUSE_BUTTON_PRESSED(MB_RIGHT_BTN))
		OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_ESCAPE)
			DEACTIVATE_FLOW_DIAGRAM(sFlowDiagram)
			EXIT
		ENDIF
		
		//Check for displaying of key.
		IF IS_BUTTON_PRESSED(PAD1, INT_TO_ENUM(PAD_BUTTON_NUMBER, ENUM_TO_INT(FD_BUTTON_KEY)))
		OR (IS_MOUSE_BUTTON_PRESSED(MB_LEFT_BTN) AND sFlowDiagram.iHighlightedMission = -1)
			sFlowDiagram.iInfoBoxAlphaTarget = 255
			sFlowDiagram.iFrontAlphaTarget = 0
			sFlowDiagram.iMissionBoxAlphaTarget = 0
		ELSE
			//Default alpha levels with key down.
			sFlowDiagram.iInfoBoxAlphaTarget = 0
			IF sFlowDiagram.iHighlightedMission = -1
				sFlowDiagram.iFrontAlphaTarget = 255
				IF sFlowDiagram.fZoomTarget < FD_VIEW_SWITCH_ZOOM
					sFlowDiagram.iFrontAlphaTarget = -255
				ENDIF
			ELSE
				sFlowDiagram.iFrontAlphaTarget = 0
			ENDIF
		ENDIF
		
		//Check for switching of colour modes.
		IF IS_BUTTON_JUST_PRESSED(PAD1, LEFTSHOULDER1)
			INT iNewColourMode = ENUM_TO_INT(sFlowDiagram.eColourMode) - 1
			IF iNewColourMode < 0
				iNewColourMode = ENUM_TO_INT(FD_MAX_COLOUR_MODES) - 1
			ENDIF
			SWITCH_DIAGRAM_COLOUR_MODE(sFlowDiagram, INT_TO_ENUM(FLOW_DIAGRAM_COLOUR_MODE, iNewColourMode))
		ENDIF
		IF IS_BUTTON_JUST_PRESSED(PAD1, RIGHTSHOULDER1)
			INT iNewColourMode = ENUM_TO_INT(sFlowDiagram.eColourMode) + 1
			IF iNewColourMode >= ENUM_TO_INT(FD_MAX_COLOUR_MODES)
				iNewColourMode = 0
			ENDIF
			SWITCH_DIAGRAM_COLOUR_MODE(sFlowDiagram, INT_TO_ENUM(FLOW_DIAGRAM_COLOUR_MODE, iNewColourMode))
		ENDIF
		
		//Read mouse position.
		GET_MOUSE_POSITION(fMouseX, fMouseY)
		
		//Read analogue stick positions.
		GET_POSITION_OF_ANALOGUE_STICKS(PAD1, 	
										iStickPositions[LEFT_X], 
										iStickPositions[LEFT_Y], 
										iStickPositions[RIGHT_X], 
										iStickPositions[RIGHT_Y])
										
		//Check for input mode switching.
		IF sFlowDiagram.bMouseActive
			IF fMouseX = sFlowDiagram.fLastMouseX
				IF fMouseY = sFlowDiagram.fLastMouseY
					//Mouse inactive. Have sticks been moved?
					IF iStickPositions[LEFT_X] <> 0
					OR iStickPositions[LEFT_Y] <> 0
					OR iStickPositions[RIGHT_X] <> 0
					OR iStickPositions[RIGHT_Y] <> 0
					OR IS_BUTTON_JUST_PRESSED(PAD1, CROSS)
						sFlowDiagram.bMouseActive = FALSE
						//Center the cursor.
						sFlowDiagram.fCursorXTarget = 0.5
						sFlowDiagram.fCursorYTarget = 0.5
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF iStickPositions[LEFT_X] = 0
				IF iStickPositions[LEFT_Y] = 0
					//Pad inactive. Has mouse been moved?
					IF fMouseX <> sFlowDiagram.fLastMouseX
					OR fMouseY <> sFlowDiagram.fLastMouseY
						sFlowDiagram.bMouseActive = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		sFlowDiagram.fLastMouseX = fMouseX
		sFlowDiagram.fLastMouseY = fMouseY
		
		IF sFlowDiagram.bMouseActive
			//Mouse is active.
			sFlowDiagram.fCursorX = fMouseX
			sFlowDiagram.fCursorXTarget = sFlowDiagram.fCursorX
			sFlowDiagram.fCursorY = fMouseY
			sFlowDiagram.fCursorYTarget = sFlowDiagram.fCursorY
			
			//Zooming in and out.
			IF sFlowDiagram.fMouseZoomY <> -1.0	
				sFlowDiagram.fZoomTarget *= (1 - (fMouseY - sFlowDiagram.fMouseZoomY) / 0.3 * FD_ZOOM_RATE)
				sFlowDiagram.fZoomTarget = CLAMP(sFlowDiagram.fZoomTarget, FD_MIN_ZOOM, FD_MAX_ZOOM)
				
				//Has the zoom button been released?
				IF NOT IS_MOUSE_BUTTON_PRESSED(MB_RIGHT_BTN)
					sFlowDiagram.fMouseZoomY = -1.0
				ENDIF
			//Not zooming in and out.
			ELSE
				//Has the zoom button just be pressed?
				IF IS_MOUSE_BUTTON_PRESSED(MB_RIGHT_BTN)
					sFlowDiagram.fMouseZoomY = fMouseY
				ENDIF
				
				//Use bump scrolling.
				Update_Flow_Diagram_Bump_Scrolling(sFlowDiagram)
			ENDIF
		ELSE						
			//Check for left/right scrolling.
			IF iStickPositions[LEFT_X] <> 0 
				sFlowDiagram.fXOffsetTarget = sFlowDiagram.fXOffsetTarget +@ TO_FLOAT(iStickPositions[LEFT_X]) / 128.0 * FD_X_SCROLL_RATE / sFlowDiagram.fZoom
				sFlowDiagram.fXOffsetTarget = CLAMP(sFlowDiagram.fXOffsetTarget, sFlowDiagram.fMinXOffset, sFlowDiagram.fMaxXOffset)
			ENDIF
			
			//Check for up/down scrolling.
			IF iStickPositions[LEFT_Y] <> 0 
				sFlowDiagram.fYOffsetTarget = sFlowDiagram.fYOffsetTarget +@ TO_FLOAT(iStickPositions[LEFT_Y]) / 128.0 * FD_Y_SCROLL_RATE / sFlowDiagram.fZoom
				sFlowDiagram.fYOffsetTarget = CLAMP(sFlowDiagram.fYOffsetTarget, sFlowDiagram.fMinYOffset, sFlowDiagram.fMaxYOffset)
			ENDIF
			
			//Check for zooming in/out.
			IF iStickPositions[RIGHT_Y] <> 0
				sFlowDiagram.fZoomTarget *= (1 - (TO_FLOAT(iStickPositions[RIGHT_Y]) / 128.0 * FD_ZOOM_RATE))
				sFlowDiagram.fZoomTarget = CLAMP(sFlowDiagram.fZoomTarget, FD_MIN_ZOOM, FD_MAX_ZOOM)
			ENDIF
		ENDIF

	//Mission check in progress. Wait for choice to be made.
	ELSE
		//Launch mission choice selected.
		IF IS_BUTTON_JUST_PRESSED(PAD1, INT_TO_ENUM(PAD_BUTTON_NUMBER, ENUM_TO_INT(FD_BUTTON_SELECT)))
		OR IS_MOUSE_BUTTON_JUST_PRESSED(MB_LEFT_BTN)
		OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_RETURN)
			sFlowDiagram.bDoingStartMissionCheck = FALSE
			
			//Clean up alpha values.
			sFlowDiagram.iFrontAlphaTarget = 255
			sFlowDiagram.iFrontAlpha = 255
			sFlowDiagram.iInfoBoxAlphaTarget = 0
			sFlowDiagram.iInfoBoxAlpha = 0
			sFlowDiagram.iMissionBoxAlphaTarget = 0
			sFlowDiagram.iMissionBoxAlpha = 0
			
			//Turn off the flow diagram, we're launching a mission.
			DEACTIVATE_FLOW_DIAGRAM(sFlowDiagram)
			
			FLOW_DIAGRAM_LAUNCH_MISSION(sFlowDiagram, flowLauncher)
			
		//Don't launch mission choice selected.
		ELIF IS_BUTTON_JUST_PRESSED(PAD1, INT_TO_ENUM(PAD_BUTTON_NUMBER, ENUM_TO_INT(FD_BUTTON_EXIT)))
		OR IS_MOUSE_BUTTON_JUST_PRESSED(MB_RIGHT_BTN)
		OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_ESCAPE)
//			sFlowDiagram.iFrontAlphaTarget = 255
			sFlowDiagram.iInfoBoxAlphaTarget = 0
			sFlowDiagram.iInfoBoxAlpha = 0
			sFlowDiagram.iMissionBoxAlphaTarget = 0
//			sFlowDiagram.iMissionBoxAlpha = 0
			sFlowDiagram.bDoingStartMissionCheck = FALSE
		ENDIF
	ENDIF
	
	
	
ENDPROC


PROC INTERPOLATE_FLOW_DIAGRAM_VALUES(FLOW_DIAGRAM &sFlowDiagram)

	//Interp foreground alpha.
	FLOAT fTargetAlpha = TO_FLOAT(sFlowDiagram.iFrontAlphaTarget)
	FLOAT fCurrentAlpha = TO_FLOAT(sFlowDiagram.iFrontAlpha)
	sFlowDiagram.iFrontAlpha = FLOOR(fCurrentAlpha + (fTargetAlpha-fCurrentAlpha)* FD_ALPHA_INTERP_RATE)
	//Interp missionbox alpha.
	fTargetAlpha = TO_FLOAT(sFlowDiagram.iMissionBoxAlphaTarget)
	fCurrentAlpha = TO_FLOAT(sFlowDiagram.iMissionBoxAlpha)
	sFlowDiagram.iMissionBoxAlpha = FLOOR(fCurrentAlpha + (fTargetAlpha-fCurrentAlpha)* FD_ALPHA_INTERP_RATE)
	//Interp infobox alpha.
	fTargetAlpha = TO_FLOAT(sFlowDiagram.iInfoBoxAlphaTarget)
	fCurrentAlpha = TO_FLOAT(sFlowDiagram.iInfoBoxAlpha)
	sFlowDiagram.iInfoBoxAlpha = FLOOR(fCurrentAlpha + (fTargetAlpha-fCurrentAlpha)* FD_ALPHA_INTERP_RATE)
	//Interp offsets.
	sFlowDiagram.fXOffset += (sFlowDiagram.fXOffsetTarget - sFlowDiagram.fXOffset) * FD_INPUT_INTERP_RATE
	sFlowDiagram.fYOffset += (sFlowDiagram.fYOffsetTarget - sFlowDiagram.fYOffset) * FD_INPUT_INTERP_RATE
	//Interp cursor position.
	sFlowDiagram.fCursorX += (sFlowDiagram.fCursorXTarget - sFlowDiagram.fCursorX) * FD_INPUT_INTERP_RATE
	sFlowDiagram.fCursorY += (sFlowDiagram.fCursorYTarget - sFlowDiagram.fCursorY) * FD_INPUT_INTERP_RATE
	//Interp zoom.
	sFlowDiagram.fZoom += (sFlowDiagram.fZoomTarget - sFlowDiagram.fZoom) * FD_INPUT_INTERP_RATE

ENDPROC

PROC MANAGE_FLOW_DIAGRAM_BUGSTAR_DATA_CACHE(FLOW_DIAGRAM &sFlowDiagram)
	IF sFlowDiagram.bBugstarInfoQueryStarted
		EXIT
	ENDIF
	DEBUG_MENU_ITEM_STRUCT sTempItems[FD_MAX_MISSIONS_PER_QUERY]
	IF (sFlowDiagram.eColourMode = FD_COLOUR_MODE_COMPLETE_PERCENTAGE AND NOT sFlowDiagram.bFlowDiagramMissionPercCached) OR sFlowDiagram.bBugstarPercQueryStarted
		IF CACHE_DIAGRAM_MISSION_PERCENTAGE(sFlowDiagram, sTempItems)
			
			sFlowDiagram.bFlowDiagramMissionPercCached = TRUE
		ENDIF
	ELIF (sFlowDiagram.eColourMode = FD_COLOUR_MODE_BUG_COUNT AND NOT sFlowDiagram.bFlowDiagramMissionBugsCached) OR sFlowDiagram.bBugstarBugsQueryStarted
		IF CACHE_DIAGRAM_MISSION_BUGS(sFlowDiagram)
			sFlowDiagram.bFlowDiagramMissionBugsCached = TRUE
		ENDIF
	ENDIF	
ENDPROC



//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════╡ Diagram Main Update ╞═══════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC UPDATE_FLOW_DIAGRAM(FLOW_DIAGRAM &sFlowDiagram, FLOW_LAUNCHER_VARS &flowLauncher)
	IF sFlowDiagram.bDrawCursor
		REQUEST_STREAMED_TEXTURE_DICT("VisualFlow")
	ENDIF
	
	IF NOT g_flowDiagram.bFlowDiagramActive
		EXIT
	ENDIF
	
	IF NOT g_flowDiagram.bFlowDiagramDataLoaded
		//Load XML data into globals.
		IF NOT LOAD_FLOW_DIAGRAM_DATA_FROM_XML(sFlowDiagram)
			CPRINTLN(DEBUG_FLOW_DIAGRAM_LOAD, "Flow diagram data needs to be generated. Run \"Generate Flow Diagram\" from the debug menu.")
			PRINT_NOW("VF_NO_XML", DEFAULT_GOD_TEXT_TIME, 0)
			DEACTIVATE_FLOW_DIAGRAM(sFlowDiagram)
			EXIT
		ELIF NOT g_flowDiagram.bFlowDiagramXMLGenerated
			CPRINTLN(DEBUG_FLOW_DIAGRAM_LOAD, "Loaded a pre-existing xml file. It is possible this is out of date.")
		ENDIF
	ELIF NOT g_flowDiagram.bFlowDiagramDataSorted
		WAIT(0)
		//Sort data in globals.
		SORT_FLOW_DIAGRAM_DATA(sFlowDiagram)
		// Load in mission complete percentages as default.
		SWITCH_DIAGRAM_COLOUR_MODE(sFlowDiagram, FD_COLOUR_MODE_COMPLETE_PERCENTAGE)
	ELIF NOT g_flowDiagram.bFlowDiagramPositioned
		WAIT(0)
		//Calculate the diagram layout.
		CALCULATE_FLOW_DIAGRAM_ELEMENT_POSITIONS(sFlowDiagram)
		//Update which missions are completed.
		UPDATE_DIAGRAM_COMPLETED_MISSIONS(sFlowDiagram)
	ELSE
		//Manage user input.
		MANAGE_FLOW_DIAGRAM_INPUT(sFlowDiagram, flowLauncher)
		INTERPOLATE_FLOW_DIAGRAM_VALUES(sFlowDiagram)
		
		// Manage any required color-related bugstar queries
		MANAGE_FLOW_DIAGRAM_BUGSTAR_DATA_CACHE(sFlowDiagram)
		
		//Display the diagram.
		DISPLAY_FLOW_DIAGRAM_BACKGROUND(sFlowDiagram)
		DISPLAY_FLOW_DIAGRAM(sFlowDiagram)
		DISPLAY_FLOW_DIAGRAM_START_MISSION_CHECK(sFlowDiagram)
		DISPLAY_FLOW_DIAGRAM_KEY(sFlowDiagram)
		DISPLAY_FLOW_DIAGRAM_CURSOR(sFlowDiagram)
		DISPLAY_FLOW_DIAGRAM_HELP_TEXT(sFlowDiagram)
		DISPLAY_FLOW_DIAGRAM_COLOUR_MODE(sFlowDiagram)
		DISPLAY_FLOW_DIAGRAM_FOREGROUND(sFlowDiagram)
		DISPLAY_FLOW_DIAGRAM_BUGSTAR_INFO_BOX(sFlowDiagram)
	ENDIF
ENDPROC

#ENDIF
