//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════╡ Utility Functions  ╞═════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛
USING "commands_graphics.sch"
USING "flow_diagram_structs_core.sch"

//Precalculates values required for roundToPixel. Separated out as an optimisation.
PROC CALCULATE_SCREEN_PIXEL_DIVISIONS(FLOW_DIAGRAM &sFlowDiagram)
	//Get screen resolution.
	INT iScreenX, iScreenY
	GET_SCREEN_RESOLUTION(iScreenX,iScreenY)
	
	//Save percentage of screen width/height per pixel to global values.
	sFlowDiagram.fScreenDivisionX = 1.0/TO_FLOAT(iScreenX)
	sFlowDiagram.fScreenDivisionY = 1.0/TO_FLOAT(iScreenY)
ENDPROC


//Rounds a screen coord to the center of the nearest pixel. 
//Useful for stopping flickering when drawing rectangles.
FUNC FLOAT ROUND_TO_PIXEL(FLOW_DIAGRAM &sFlowDiagram, FLOAT fScreenCoord, BOOL bXCoord)
	IF bXCoord
		RETURN (fScreenCoord - (fScreenCoord % sFlowDiagram.fScreenDivisionX))
	ELSE
		RETURN (fScreenCoord - (fScreenCoord % sFlowDiagram.fScreenDivisionY))
	ENDIF
ENDFUNC


/// PURPOSE:
///    Sets RGB color values based on comparing mission complete % to project-specific thresholds
PROC GET_MISSION_PERCENTAGE_COLOUR(INT iPercentage, INT &iRed, INT &iGreen, INT &iBlue, BOOL bSetZeroWhite = FALSE)
	IF bSetZeroWhite AND iPercentage = 0
		iRed = 255
		iGreen = 255
		iBlue = 255
	ELIF iPercentage < FLOW_INFO_PERC_RED_THRESHOLD
		iRed = 255
		iGreen = 0
		iBlue = 0
	ELIF iPercentage < FLOW_INFO_PERC_YELLOW_THRESHOLD
		iRed= 255
		iGreen = 191
		iBlue = 0
	ELIF iPercentage < FLOW_INFO_PERC_GREEN_THRESHOLD
		iRed = 0
		iGreen = 255
		iBlue = 0
	ELSE
		iRed = 255
		iGreen = 255
		iBlue = 255
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets RGB color values based on comparing mission bug count % to project-specific thresholds
PROC GET_MISSION_TOTAL_BUGS_COLOUR(INT iTotalBugCount, INT &iRed, INT &iGreen, INT &iBlue)
	IF iTotalBugCount > FLOW_INFO_BUGS_RED_THRESHOLD
		iRed = 255
		iGreen = 0
		iBlue = 0
	ELIF iTotalBugCount > FLOW_INFO_BUGS_YELLOW_THRESHOLD
		iRed= 255
		iGreen = 191
		iBlue = 0
	ELIF iTotalBugCount > FLOW_INFO_BUGS_GREEN_THRESHOLD
		iRed = 0
		iGreen = 255
		iBlue = 0
	ELSE
		iRed = 255
		iGreen = 255
		iBlue = 255
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets RGB color values based on mission strand order
PROC FILL_IN_STRAND_COLOUR(INT iStrand, INT &iRed, INT &iGreen, INT &iBlue)
	SWITCH iStrand
		CASE 0
			iRed = 70
			iGreen = 206
			iBlue = 83
		BREAK
		CASE 1
			iRed = 69
			iGreen = 114
			iBlue = 171
		BREAK
		CASE 2
			iRed = 150
			iGreen = 102
			iBlue = 150
		BREAK
		CASE 3
			iRed = 107
			iGreen = 53
			iBlue = 165
		BREAK
		CASE 4
			iRed = 180
			iGreen = 20
			iBlue = 20
		BREAK
		CASE 5
			iRed = 85
			iGreen = 168
			iBlue = 208
		BREAK
		CASE 6
			iRed = 158
			iGreen = 104
			iBlue = 42
		BREAK
		CASE 7
			iRed = 190
			iGreen = 37
			iBlue = 29
		BREAK
	ENDSWITCH
ENDPROC

//Converts a visual flow grid X coordinate to a screen X coordinate.
FUNC FLOAT GET_SCREEN_X_COORD(FLOW_DIAGRAM &sFlowDiagram, INT iCoordX)
	FLOAT fScreenXCoord 
	
	fScreenXCoord = TO_FLOAT(iCoordX) * FD_ELEMENT_X_SPACING
	fScreenXCoord -= 0.5
	fScreenXCoord -= sFlowDiagram.fXOffset
	fScreenXCoord *= sFlowDiagram.fZoom
	fScreenXCoord += 0.5
	
	fScreenXCoord = Round_To_Pixel(sFlowDiagram, fScreenXCoord, TRUE)
	
	RETURN fScreenXCoord
ENDFUNC


//Converts a visual flow grid Y coordinate to a screen Y coordinate.
FUNC FLOAT GET_SCREEN_Y_COORD(FLOW_DIAGRAM &sFlowDiagram, INT iCoordY)
	FLOAT fScreenYCoord
	FLOAT fYStep = 1 / TO_FLOAT(ENUM_TO_INT(MAX_STRANDS) + 5)
	
	fScreenYCoord = (3 * fYStep) + (TO_FLOAT(iCoordY) * fYStep)
	fScreenYCoord -= 0.5
	fScreenYCoord -= sFlowDiagram.fYOffset
	fScreenYCoord *= sFlowDiagram.fZoom
	fScreenYCoord += 0.5
	
	fScreenYCoord = Round_To_Pixel(sFlowDiagram, fScreenYCoord, FALSE)
	
	RETURN fScreenYCoord
ENDFUNC


FUNC BOOL DOES_CHARACTER_STRING_CONTAIN_MULTIPLE_CHARACTERS(STRING strChar)

	//First, check string is not null.
	IF (IS_STRING_NULL(strChar))
		CERRORLN(DEBUG_FLOW_DIAGRAM, "Does_Character_String_Contain_Multiple_Characterss: The character string was null.")
		RETURN FALSE
	ENDIF
	
	// Get string length.
	INT stringLength = GET_LENGTH_OF_LITERAL_STRING(strChar)
	
	//Step thruogh each char in the string and look for any "|".
	//If we find one there are multiple characters names in the string.
	INT iStringIndex
	REPEAT (stringLength-1) iStringIndex
		IF ARE_STRINGS_EQUAL(GET_STRING_FROM_STRING(strChar, iStringIndex, iStringIndex+1), "|")
			//Found a "|".
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	//Found no "|".
	RETURN FALSE
ENDFUNC


PROC GET_MULTIPLE_CHARACTERS_FROM_CHARACTER_STRING(STRING strChar, TEXT_LABEL_23 &tCharA, TEXT_LABEL_23 &tCharB, TEXT_LABEL_23 &tCharC)
	tCharA = ""
	tCharB = ""
	tCharC = ""
	
	//First, check string is not null.
	IF (IS_STRING_NULL(strChar))
		CERRORLN(DEBUG_FLOW_DIAGRAM, "Get_Multiple_Characters_From_Character_String: The character string was null.")
	ENDIF
	
	// Get string length.
	INT stringLength = GET_LENGTH_OF_LITERAL_STRING(strChar)
	
	//Use this int to store the character indexes at which the string is split.
	//There are a maxumum of 2 breaks as there is a maximum of 3 playable characters.
	INT iBreakIndex[2]
	INT iNoBreaksFound = 0
	
	//Step through the string and record break indexes.
	INT iStringIndex
	REPEAT (stringLength-1) iStringIndex
		IF ARE_STRINGS_EQUAL(GET_STRING_FROM_STRING(strChar, iStringIndex, iStringIndex+1), "|")
			IF iNoBreaksFound = 2
				CERRORLN(DEBUG_FLOW_DIAGRAM, "Get_Multiple_Characters_From_Character_String: Too many character names contained in character string. There are only be 3 playable characters.")
				EXIT
			ENDIF
		
			iBreakIndex[iNoBreaksFound] = iStringIndex
			iNoBreaksFound++
		ENDIF
	ENDREPEAT
	
	//Now split up the string around the break points and save the individual names into the provided text labels.
	IF iNoBreaksFound > 0
		tCharA = GET_STRING_FROM_STRING(strChar, 0, iBreakIndex[0])
		IF iNoBreaksFound = 1
			tCharB = GET_STRING_FROM_STRING(strChar, iBreakIndex[0]+1, stringLength)
		ELIF iNoBreaksFound = 2
			tCharB = GET_STRING_FROM_STRING(strChar, iBreakIndex[0]+1, iBreakIndex[1])
			tCharC = GET_STRING_FROM_STRING(strChar, iBreakIndex[1]+1, stringLength)
		ENDIF
	ELSE
		CERRORLN(DEBUG_FLOW_DIAGRAM, "Get_Multiple_Characters_From_Character_String: No break chars found in character string.")
		EXIT
	ENDIF
	
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════╡ Text Configuration  ╞════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC SETUP_FLOW_DIAGRAM_STRAND_TEXT(INT iAlpha = 255)
	SET_TEXT_COLOUR(FD_COL_R_TEXT_MAIN, FD_COL_G_TEXT_MAIN, FD_COL_B_TEXT_MAIN, iAlpha)
	SET_TEXT_SCALE(0.28, 0.31)
	SET_TEXT_WRAP(0.0, 2.0)
	SET_TEXT_EDGE (0,0,0,0,0)     
	SET_TEXT_PROPORTIONAL (TRUE)
	SET_TEXT_DROPSHADOW(0, 0, 0, 0, iAlpha)                                          
	SET_TEXT_FONT(FONT_STANDARD)
ENDPROC


PROC SETUP_FLOW_DIAGRAM_HELP_TEXT(INT iAlpha = 255)
	SET_TEXT_COLOUR(FD_COL_R_TEXT_HELP, FD_COL_G_TEXT_HELP, FD_COL_B_TEXT_HELP, iAlpha)
	SET_TEXT_SCALE(0.29, 0.30)
	SET_TEXT_WRAP(0.0, 2.0)
	SET_TEXT_EDGE (0,0,0,0,0)     
	SET_TEXT_PROPORTIONAL (TRUE)
	SET_TEXT_DROPSHADOW(0, 0, 0, 0, iAlpha)                                          
	SET_TEXT_FONT(FONT_STANDARD)
ENDPROC


PROC SETUP_FLOW_DIAGRAM_INLINE_STRAND_TEXT(FLOW_DIAGRAM &sFlowDiagram, INT iAlpha = 255)
	SET_TEXT_COLOUR(FD_COL_R_TEXT_MAIN, FD_COL_G_TEXT_MAIN, FD_COL_B_TEXT_MAIN, iAlpha)
	SET_TEXT_SCALE(0.12 + (0.37*sFlowDiagram.fZoom), 0.08 + (0.37*sFlowDiagram.fZoom))
	SET_TEXT_WRAP(0.0, 2.0)
	SET_TEXT_EDGE (0,0,0,0,0)     
	SET_TEXT_PROPORTIONAL (TRUE)
	SET_TEXT_DROPSHADOW(0, 0, 0, 0, iAlpha)                                          
	SET_TEXT_FONT(FONT_STANDARD)
ENDPROC


PROC SETUP_FLOW_DIAGRAM_KEY_TEXT(INT iAlpha = 255)
	SET_TEXT_COLOUR(FD_COL_R_TEXT_MAIN, FD_COL_G_TEXT_MAIN, FD_COL_B_TEXT_MAIN, iAlpha)
	SET_TEXT_SCALE(0.31, 0.34)
	SET_TEXT_WRAP(0.0, 2.0)
	SET_TEXT_EDGE (0,0,0,0,0)     
	SET_TEXT_PROPORTIONAL (TRUE)
	SET_TEXT_DROPSHADOW(0, 0, 0, 0, iAlpha)                                          
	SET_TEXT_FONT(FONT_STANDARD)
ENDPROC


PROC SETUP_FLOW_DIAGRAM_BLACK_KEY_TEXT(INT iAlpha = 255)
	SET_TEXT_COLOUR(FD_COL_R_TEXT_ELEMENT, FD_COL_G_TEXT_ELEMENT, FD_COL_B_TEXT_ELEMENT, iAlpha)
	SET_TEXT_SCALE(0.24, 0.26)
	SET_TEXT_WRAP(0.0, 2.0)
	SET_TEXT_EDGE (0,0,0,0,0)     
	SET_TEXT_PROPORTIONAL (TRUE)
	SET_TEXT_DROPSHADOW(0, 0, 0, 0, iAlpha)                                          
	SET_TEXT_FONT(FONT_STANDARD)
ENDPROC


PROC SETUP_FLOW_DIAGRAM_INFOBOX_HEADING_TEXT(INT iAlpha = 255)
	SET_TEXT_COLOUR(FD_COL_R_TEXT_TITLE, FD_COL_G_TEXT_TITLE, FD_COL_B_TEXT_TITLE, iAlpha)
	SET_TEXT_SCALE(0.32, 0.35)
	SET_TEXT_WRAP(0.0, 2.0)
	SET_TEXT_EDGE (0,0,0,0,0)     
	SET_TEXT_PROPORTIONAL (TRUE)
	SET_TEXT_DROPSHADOW(50, 255-FD_COL_R_TEXT_TITLE, 255-FD_COL_G_TEXT_TITLE, 255-FD_COL_B_TEXT_TITLE, iAlpha)                                          
	SET_TEXT_FONT(FONT_STANDARD)
ENDPROC


PROC SETUP_FLOW_DIAGRAM_INFOBOX_TEXT(FLOAT fWrapPos, INT iAlpha = 255, INT iRed = FD_COL_R_TEXT_MAIN, INT iGreen = FD_COL_G_TEXT_MAIN, INT iBlue = FD_COL_B_TEXT_MAIN)
	SET_TEXT_COLOUR(iRed, iGreen, iBlue, iAlpha)
	SET_TEXT_SCALE(0.28, 0.30)
	SET_TEXT_WRAP(0.0, fWrapPos)
	SET_TEXT_EDGE (0,0,0,0,0)     
	SET_TEXT_PROPORTIONAL (TRUE)
	SET_TEXT_DROPSHADOW(50, 255-iRed, 255-iGreen, 255-iBlue, iAlpha)                                          
	SET_TEXT_FONT(FONT_STANDARD)
ENDPROC


PROC SETUP_FLOW_DIAGRAM_COLOUR_MODE_TEXT(INT iAlpha = 255)
	SET_TEXT_COLOUR(FD_COL_R_TEXT_TITLE, FD_COL_G_TEXT_TITLE, FD_COL_B_TEXT_TITLE, iAlpha)
	SET_TEXT_SCALE(0.18, 0.22)
	SET_TEXT_WRAP(0.0, 1.0)
	SET_TEXT_EDGE (0,0,0,0,0)     
	SET_TEXT_PROPORTIONAL (TRUE)
	SET_TEXT_DROPSHADOW(50, 255-FD_COL_R_TEXT_TITLE, 255-FD_COL_G_TEXT_TITLE, 255-FD_COL_B_TEXT_TITLE, iAlpha)                                          
	SET_TEXT_FONT(FONT_STANDARD)
ENDPROC


PROC SETUP_FLOW_DIAGRAM_ELEMENT_LARGE_TEXT(FLOW_DIAGRAM &sFlowDiagram)
	INT iAlpha

	IF sFlowDiagram.iFrontAlpha < 0
		iAlpha = 0
	ELSE
		IF sFlowDiagram.fZoom > 3.0
			iAlpha = 255
		ELIF sFlowDiagram.fZoom > 2.0
			iAlpha = CEIL(255 * ((sFlowDiagram.fZoom - 1.5) * 0.5))
		ENDIF
	ENDIF
	
	SET_TEXT_COLOUR(FD_COL_R_TEXT_ELEMENT, FD_COL_G_TEXT_ELEMENT, FD_COL_B_TEXT_ELEMENT, iAlpha)
	SET_TEXT_SCALE(0.063*sFlowDiagram.fZoom, 0.067*sFlowDiagram.fZoom)
	SET_TEXT_WRAP(0.0, 2.0)
	SET_TEXT_EDGE (0,0,0,0,0)     
	SET_TEXT_PROPORTIONAL (TRUE)
	SET_TEXT_DROPSHADOW(0, 0, 0, 0, 255)                                          
	SET_TEXT_FONT(FONT_STANDARD)
ENDPROC


PROC SETUP_FLOW_DIAGRAM_ELEMENT_SMALL_TEXT(FLOW_DIAGRAM &sFlowDiagram)
	INT iAlpha

	IF sFlowDiagram.iFrontAlpha < 0
		iAlpha = 0
	ELSE
		IF sFlowDiagram.fZoom > 2.5
			iAlpha = 255
		ELIF sFlowDiagram.fZoom > 1.5
			iAlpha = CEIL(255 * (sFlowDiagram.fZoom - 1.5))
		ENDIF
	ENDIF
	

	SET_TEXT_COLOUR(FD_COL_R_TEXT_ELEMENT, FD_COL_G_TEXT_ELEMENT, FD_COL_B_TEXT_ELEMENT, iAlpha)
	SET_TEXT_SCALE(0.050*sFlowDiagram.fZoom, 0.054*sFlowDiagram.fZoom)
	SET_TEXT_WRAP(0.0, 2.0)
	SET_TEXT_DROPSHADOW(0, 0, 0, 0, 255)
	SET_TEXT_EDGE (0,0,0,0,0)     
	SET_TEXT_PROPORTIONAL (TRUE)
	SET_TEXT_DROPSHADOW(0, 0, 0, 0, 255)                                          
	SET_TEXT_FONT(FONT_STANDARD)

ENDPROC


PROC DRAW_LOADING_SCREEN()
	//Display the background and a "Loading" message to keep things looking tidy.
	DRAW_RECT(0.5,0.5,1.0,1.0,0,0,0,255)
	INT iModTime = GET_GAME_TIMER()%500
	IF iModTime < 125
		SETUP_FLOW_DIAGRAM_STRAND_TEXT()
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.46, 0.5, "STRING", "LOADING |")
	ELIF iModTime < 250
		SETUP_FLOW_DIAGRAM_STRAND_TEXT()
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.46, 0.5, "STRING", "LOADING /")
	ELIF iModTime < 375
		SETUP_FLOW_DIAGRAM_STRAND_TEXT()
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.46, 0.5, "STRING", "LOADING -")
	ELSE
		SETUP_FLOW_DIAGRAM_STRAND_TEXT()
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.46, 0.5, "STRING", "LOADING \\")
	ENDIF
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════╡ Strand Lookup Functions  ╞══════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

FUNC INT GET_STRAND_Y_POSITION(FLOW_DIAGRAM &sFlowDiagram, STRANDS eElementStrand)

	INT iStrandIndex
	REPEAT MAX_STRANDS iStrandIndex
		IF sFlowDiagram.eStrandOrder[iStrandIndex] = eElementStrand
			RETURN iStrandIndex
		ENDIF
	ENDREPEAT
	
	CERRORLN(DEBUG_FLOW_DIAGRAM, "Get_Strand_Y_Position: Failed to find strand order in order array.")
	RETURN -1
	
ENDFUNC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════╡ Diagram Element Position Setting ╞═════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC SET_POSITION_OF_CONNECTING_ELEMENT(FLOW_DIAGRAM_ELEMENT &sElement, INT paramXPos,  INT paramStrandAYPos, INT paramStrandBYPos)
	sElement.xPos = paramXPos
	sElement.yPosA = paramStrandAYPos
	sElement.yPosB = paramStrandBYPos
ENDPROC


PROC SET_POSITION_OF_DEFAULT_ELEMENT(FLOW_DIAGRAM_ELEMENT &sElement, INT paramXPos, INT paramYPos)
	sElement.xPos = paramXPos
	sElement.yPosA = paramYPos
ENDPROC
