USING "types.sch"

#IF FEATURE_GEN9_STANDALONE

//INFO:
//PARAM NOTES:
//PURPOSE: Launch the IIS
NATIVE PROC SET_SHOULD_LAUNCH_IIS() = "0x3e96f0eac6264def"

#ENDIF // FEATURE_GEN9_STANDALONE
