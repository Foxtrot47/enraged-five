//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	script_conversion.sch										//
//		AUTHOR			:	Ben Rollinson												//
//		DESCRIPTION		:	Functions to convert between various unit systems.			//
//							Conversion ratios set up to exactly match code macros.		//
//							Also contains data type conversion functions.				//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////

USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_localization.sch"


// THIS IS UNSAFE!
// Please note that passing a TEXT_LABEL out of a function as a STRING is unsafe. A TEXT_LABEL is
// an allocated block of memory where as a STRING is a pointer to a memory address. If we convert a
// TEXT_LABEL to a STRING we have a pointer to a block of memory that may be freed up if the
// TEXT_LABEL goes out of scope. At this point the memory can be overwritten and the STRING
// left containing garbage. - BenR
FUNC STRING TEXT_LABEL_TO_STRING(STRING textLabelIn)
	CERRORLN(DEBUG_SYSTEM, "Error: Unsafe TEXT_LABEL_TO_STRING being used. Corrupted memory issues may occur.")
	RETURN(textLabelIn)
ENDFUNC


CONST_INT	DEFAULT_FLOAT_TO_STRING_PRECISION	3

ENUM FORMAT_FLOAT_TO_STRING
	FORMATFLOATTOSTRING_NORMAL,
	FORMATFLOATTOSTRING_PERCENTAGE
ENDENUM

/// PURPOSE:
///   Converts a float value to a string. Helpful when printing debug values. 
/// PARAMS:
///    fValue - the float to convert
///    iPrecision - floating point precision (number of decimal places required)
///    eFormat - formatting for the result (can add a percentage symbol on the end)
/// RETURNS:
///    Returns the string format of the float value passed in
FUNC STRING FLOAT_TO_STRING(FLOAT fValue, INT iPrecision = DEFAULT_FLOAT_TO_STRING_PRECISION, FORMAT_FLOAT_TO_STRING eFormat = FORMATFLOATTOSTRING_NORMAL)

	TEXT_LABEL_63 tlResult
	
	INT iIntegerComponenet, iDecimalComponent
	IF fValue < 0
		iIntegerComponenet = CEIL( fValue )
	ELSE
		iIntegerComponenet = FLOOR( fValue )
	ENDIF
	
	iDecimalComponent = ABSI( ROUND( ( fValue - iIntegerComponenet ) * POW( 10, TO_FLOAT( iPrecision ) ) ) )
	// component has rounded up to 1.0
	IF iDecimalComponent >= POW( 10, TO_FLOAT( iPrecision ) )
		IF fValue < 0
			iIntegerComponenet--
		ELSE
			iIntegerComponenet++
		ENDIF
		iDecimalComponent = 0
	ENDIF
	
	IF fValue < 0
		tlResult	= "-"
	ELSE
		tlResult	= ""
	ENDIF
	
	tlResult	+= ABSI( iIntegerComponenet )
	tlResult	+= "."
	
	INT iDroppedPlaces = 1
	REPEAT iPrecision iDroppedPlaces
		IF iDecimalComponent < POW( 10, TO_FLOAT( iDroppedPlaces ) )
			tlResult += "0"
		ENDIF
	ENDREPEAT
	
	tlResult	+= iDecimalComponent
	
	SWITCH eFormat
		CASE FORMATFLOATTOSTRING_PERCENTAGE
			tlResult += "%"
		BREAK
	ENDSWITCH

	RETURN TEXT_LABEL_TO_STRING( tlResult )
ENDFUNC

ENUM FORMAT_VECTOR_TO_STRING
	FORMATVECTORTOSTRING_AS_SEEN_IN_SCRIPT,
	FORMATVECTORTOSTRING_PER_COMPONENT
ENDENUM

/// PURPOSE:
///    Converts a vector to a string. Helpful when printing debug values.
/// PARAMS:
///    vValue - Vector value to convert
///    iPrecision - floating point precision (number of decimal places required)
///    eFormat - formatting for the result (AS SEEN IN SCRIPT = <<0,0,0>>, PER COMPONENET = X-0.0 Y-0.0 Z-0.0)
/// RETURNS:
///    Returns the string format of the vector passed in.
FUNC STRING VECTOR_TO_STRING(VECTOR vValue, INT iPrecision = DEFAULT_FLOAT_TO_STRING_PRECISION, FORMAT_VECTOR_TO_STRING eFormat = FORMATVECTORTOSTRING_AS_SEEN_IN_SCRIPT)

	TEXT_LABEL_63 tlResult
	
	TEXT_LABEL_63 tlX, tlY, tlZ
	tlX = FLOAT_TO_STRING(vValue.x, iPrecision)
	tlY = FLOAT_TO_STRING(vValue.y, iPrecision)
	tlZ = FLOAT_TO_STRING(vValue.z, iPrecision)
	
	SWITCH eFormat
		CASE FORMATVECTORTOSTRING_AS_SEEN_IN_SCRIPT
			tlResult	+= "<< "
			tlResult	+= tlX
			tlResult	+= ", "
			tlResult	+= tlY
			tlResult	+= ", "
			tlResult	+= tlZ
			tlResult	+= " >>"
		BREAK
		CASE FORMATVECTORTOSTRING_PER_COMPONENT
			tlResult	+= "X-"
			tlResult	+= tlX
			tlResult	+= " Y-"
			tlResult	+= tlY
			tlResult	+= " Z-"
			tlResult	+= tlZ
		BREAK
	ENDSWITCH
	
	RETURN TEXT_LABEL_TO_STRING( tlResult )

ENDFUNC

// Converts a bool to an int which is either 1 or 0
FUNC INT BOOL_TO_INT( BOOL bValue )
	IF bValue
		RETURN 1
	ENDIF
	
	RETURN 0
ENDFUNC

// Converts an int to a bool
FUNC BOOL INT_TO_BOOL(INT iValue)
    RETURN (iValue = 1)
ENDFUNC

// Converts a bool to a string. Helpful when printing debug values.
DEBUGONLY FUNC STRING BOOL_TO_STRING( BOOL bValue )
	IF bValue
		RETURN "TRUE"
	ENDIF
	
	RETURN "FALSE"
ENDFUNC

CONST_FLOAT METERS_TO_MILES			1609.344
CONST_FLOAT METERS_TO_FEET			0.3048
CONST_FLOAT METERS_TO_KILOMETERS	1000.0
CONST_FLOAT KILOMETERS_TO_MILES		1.609344

FUNC BOOL DOES_CURRENT_LANGUAGE_USE_METRIC_SYSTEM()
	IF GET_CURRENT_LANGUAGE() = LANGUAGE_ENGLISH
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

FUNC FLOAT CONVERT_METERS_TO_MILES(FLOAT meters)
	RETURN meters/METERS_TO_MILES
ENDFUNC


FUNC FLOAT CONVERT_KPH_TO_MPH(FLOAT kph)
	RETURN kph/KILOMETERS_TO_MILES
ENDFUNC


FUNC FLOAT CONVERT_METERS_TO_FEET(FLOAT meters)
	RETURN meters/METERS_TO_FEET
ENDFUNC


FUNC FLOAT CONVERT_METERS_TO_KILOMETERS(FLOAT meters)
	RETURN meters/METERS_TO_KILOMETERS
ENDFUNC


FUNC FLOAT CONVERT_MILES_TO_KILOMETERS(FLOAT miles)
	RETURN miles*KILOMETERS_TO_MILES
ENDFUNC
