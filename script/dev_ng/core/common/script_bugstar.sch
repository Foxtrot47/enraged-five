//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	bugstar_query_debug.sch										//
//		AUTHOR			:	Kenneth Ross												//
//		DESCRIPTION		:	Debug procs used when performing Bugstar queries.			//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////

// Include the standard headers (which also ensures the correct #DEFINEs are recognised)
USING "rage_builtins.sch"
USING "globals.sch"



// Only include in debug mode
#IF IS_DEBUG_BUILD

USING "commands_xml.sch"
USING "commands_script.sch"
USING "bugstar_globals.sch"
USING "debug_channels_structs.sch"

/// PURPOSE: Sets the stored bugstar query flags.
///    NOTE: Must be called after the calling script calls START_BUGSTAR_QUERY(...)
PROC SET_BUGSTAR_QUERY_STARTED()
	CDEBUG1LN(DEBUG_BUGSTAR, "Query started by '", GET_THIS_SCRIPT_NAME(),"'")
	g_bugstarQuery.bQueryStarted = TRUE
	g_bugstarQuery.iQueryID = GET_HASH_KEY(GET_THIS_SCRIPT_NAME())
ENDPROC

/// PURPOSE: Clears the stored bugstar query flags.
///    NOTE: Must be called after the calling script calls END_BUGSTAR_QUERY()
PROC SET_BUGSTAR_QUERY_FINISHED()
	CDEBUG1LN(DEBUG_BUGSTAR, "Query finished by '", GET_THIS_SCRIPT_NAME(), "'")
	g_bugstarQuery.bQueryStarted = FALSE
	g_bugstarQuery.iQueryID = 0
ENDPROC

/// PURPOSE: Checks to see if the calling script has started a Bugstar query.
FUNC BOOL HAS_BUGSTAR_QUERY_STARTED()
	RETURN g_bugstarQuery.bQueryStarted AND g_bugstarQuery.iQueryID = GET_HASH_KEY(GET_THIS_SCRIPT_NAME())
ENDFUNC

/// PURPOSE: Checks if it is safe for the calling script to start a new Bugstar query.
///    NOTE: This must be return true before the calling script attempts to start a new Bugstar query.
FUNC BOOL IS_BUGSTAR_QUERY_SAFE_TO_START(BOOL bEndExistingQuery = FALSE)
	
	IF g_bugstarQuery.bQueriesDisabled
		RETURN FALSE
	ENDIF
	
	IF g_bugstarQuery.bQueryStarted
	OR IS_BUGSTAR_QUERY_PENDING()
		IF bEndExistingQuery
			IF NOT IS_BUGSTAR_QUERY_PENDING()
				CDEBUG1LN(DEBUG_BUGSTAR, "Query interrupted by '", GET_THIS_SCRIPT_NAME(), "'")
				END_BUGSTAR_QUERY()
				SET_BUGSTAR_QUERY_FINISHED()
				RETURN TRUE
			ENDIF
		ENDIF
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC SETUP_BUGSTAR_QUERY(BUGSTAR_DATA_STRUCT &sBugstarData, STRING sQuery)
	START_BUGSTAR_QUERY(sQuery)
	SET_BUGSTAR_QUERY_STARTED()
	SET_BUGSTAR_PROCESS_FLAG(sBugstarData.eProcessFlags, BSTAR_PROCESS_BUGSTAR_QUERY_STARTED | BSTAR_PROCESS_BUGSTAR_QUERY_PENDING)
ENDPROC

PROC CLEANUP_BUGSTAR_QUERY(BUGSTAR_DATA_STRUCT &sBugstarData)
	// CLEAN UP QUERY
	END_BUGSTAR_QUERY()
	SET_BUGSTAR_QUERY_FINISHED()
	// Done processing so clear some flags and return
	CLEAR_BUGSTAR_PROCESS_FLAG(sBugstarData.eProcessFlags, BSTAR_PROCESS_BUGSTAR_QUERY_STARTED | BSTAR_PROCESS_BUGSTAR_QUERY_PENDING)
ENDPROC

PROC RESET_ALL_BUGSTAR_DATA(BUGSTAR_DATA_STRUCT &sBugstarData)
	INT iter
	// Bugstar data
	sBugstarData.iBugstarID = 0
	sBugstarData.iFixedBugs = 0
	sBugstarData.iOverdueBugs = 0
	sBugstarData.iABugs = 0
	sBugstarData.iBBugs = 0
	sBugstarData.iCBugs = 0
	sBugstarData.iDBugs = 0
	sBugstarData.iTasks = 0
	sBugstarData.iTodos = 0
	sBugstarData.iWish = 0
	FOR iter = 0 TO DM_MAX_BUGSTAR_DEPTS-1
		sBugstarData.iDeptBugs[iter] = 0
	ENDFOR
	FOR iter = 0 TO 4
		sBugstarData.iTop5Bugs[iter] = 0
		sBugstarData.iTop5BugsAvgAge[iter] = 0
	ENDFOR
ENDPROC

PROC RESET_MISSION_BUGS_DATA(MISSION_BUGS_STRUCT &sBugData, BOOL bResetMain, BOOL bResetTrack, BOOL bResetBugs)
	// Reset some bug data
	IF bResetMain
		sBugData.bDisplay 		= FALSE
		sBugData.bInitialised	= FALSE
	ENDIF
	
	IF bResetTrack
		sBugData.sMissionID 	= ""
		sBugData.iTopItem		= 0
		sBugData.iCurrentItem	= 0
	ENDIF
	
	IF bResetBugs
		sBugData.sCurrentDate 	= ""
		sBugData.iTotalItems 	= 0
		INT i
		FOR i = 0 TO DM_MAX_MISSION_BUGS-1
			sBugData.iBugNumber[i] = 0
			sBugData.iDaysOpen[i] = -1
		ENDFOR
	ENDIF
ENDPROC

/// PURPOSE: Retrieves attribute data from the current node in the xml file that is currently loaded
PROC BUGSTAR_GET_NODE_ATTRIBUTES(DEBUG_MENU_ITEM_STRUCT &sMenuItem, BUGSTAR_DATA_STRUCT &sBugstarData, INT iNodeHash, BOOL bCopyMissionID = FALSE)

	// Use local copies so we know all the data is cleared
	DEBUG_MENU_ITEM_STRUCT sMenuItemLocal
	BUGSTAR_DATA_STRUCT sBugstarDataLocal

	// Set some default states
	sMenuItemLocal.iUniqueID 		= -1
	sMenuItemLocal.iPerc 			= -1
	sMenuItemLocal.iImportantBugs	= -1
	sMenuItemLocal.iCharacter 		= -1
	sMenuItemLocal.vCoord[0] 		= << DM_NO_VECTOR, DM_NO_VECTOR, DM_NO_VECTOR >>
	sMenuItemLocal.fHeading[0]		= DM_NO_HEADING
	sMenuItemLocal.vCoord[1] 		= << DM_NO_VECTOR, DM_NO_VECTOR, DM_NO_VECTOR >>
	sMenuItemLocal.fHeading[1]		= DM_NO_HEADING
	sMenuItemLocal.vCoord[2] 		= << DM_NO_VECTOR, DM_NO_VECTOR, DM_NO_VECTOR >>
	sMenuItemLocal.fHeading[2]		= DM_NO_HEADING
	SET_DEBUG_MENU_ITEM_FLAG(sMenuItemLocal.iFlags, DM_BIT_DISPLAY)
	SET_DEBUG_MENU_ITEM_FLAG(sMenuItemLocal.iFlags, DM_BIT_ENABLED)
	SET_DEBUG_MENU_ITEM_FLAG(sMenuItemLocal.iFlags, DM_BIT_BLIPCOORDS)
	SET_DEBUG_MENU_ITEM_FLAG(sMenuItemLocal.iFlags, DM_BIT_KEEP_VEH)
	SET_DEBUG_MENU_ITEM_FLAG(sMenuItemLocal.iFlags, DM_BIT_LAUNCH_SCRIPT)
	sMenuItemLocal.iHour 			= -1
	
	IF bCopyMissionID
		sMenuItemLocal.sMissionID = sMenuItem.sMissionID
	ENDIF
	
	
	// Bugstar data
	INT i
	FOR i = 0 TO DM_MAX_BUGSTAR_DEPTS-1
		sBugstarData.iDeptBugs[i] = -1
	ENDFOR
	FOR i = 0 TO 4
		sBugstarData.iTop5Bugs[i] = 0
		sBugstarData.iTop5BugsAvgAge[i] = 0
	ENDFOR
	
	INT eachAttribute
	TEXT_LABEL_15 sLabel
	
	// Check the node has some attributes
	IF GET_NUMBER_OF_XML_NODE_ATTRIBUTES() = 0
		CPRINTLN(DEBUG_BUGSTAR, "BUGSTAR_GET_NODE_ATTRIBUTES: No XML Nodes Attributes to grab")
		EXIT
	ENDIF

	// Loop through all the attributes
	FOR eachAttribute = 0 TO (GET_NUMBER_OF_XML_NODE_ATTRIBUTES()-1)

		// Convert the attribute name to a hash key like we did with the nodes so we can use a switch statement
		SWITCH GET_HASH_KEY(GET_XML_NODE_ATTRIBUTE_NAME(eachAttribute))

			// Grab the title
			CASE DM_HASH_TITLE
				sMenuItemLocal.sTitle = GET_STRING_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
			BREAK
			
			// Grab the title extra
			CASE DM_HASH_TITLE_EXTRA
				sMenuItemLocal.sTitleExtra = GET_STRING_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
			BREAK

			// Grab the file name
			CASE DM_HASH_FILE
				sMenuItemLocal.sFile = GET_STRING_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
			BREAK
			
			// Grab line 1 of info
			CASE DM_HASH_INFO
				sMenuItemLocal.sInfo = GET_STRING_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
			BREAK
			
			// Grab line 2 of info
			CASE DM_HASH_EXTRA_INFO
				sMenuItemLocal.sExtraInfo = GET_STRING_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
			BREAK
			
			// Grab the x coord
			CASE DM_HASH_POSX
				sMenuItemLocal.vCoord[0].x = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
			BREAK
			
			// Grab the y coord
			CASE DM_HASH_POSY
				sMenuItemLocal.vCoord[0].y = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
			BREAK
			
			// Grab the z coord
			CASE DM_HASH_POSZ
				sMenuItemLocal.vCoord[0].z = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
			BREAK
			
			// Grab the heading
			CASE DM_HASH_HEADING
				sMenuItemLocal.fHeading[0] = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
			BREAK
			
			// Grab the x1 coord
			CASE DM_HASH_POSX1
				sMenuItemLocal.vCoord[1].x = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
			BREAK
			
			// Grab the y1 coord
			CASE DM_HASH_POSY1
				sMenuItemLocal.vCoord[1].y = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
			BREAK
			
			// Grab the z1 coord
			CASE DM_HASH_POSZ1
				sMenuItemLocal.vCoord[1].z = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
			BREAK
			
			// Grab the heading1
			CASE DM_HASH_HEADING1
				sMenuItemLocal.fHeading[1] = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
			BREAK
			
			// Grab the x2 coord
			CASE DM_HASH_POSX2
				sMenuItemLocal.vCoord[2].x = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
			BREAK
			
			// Grab the y2 coord
			CASE DM_HASH_POSY2
				sMenuItemLocal.vCoord[2].y = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
			BREAK
			
			// Grab the z2 coord
			CASE DM_HASH_POSZ2
				sMenuItemLocal.vCoord[2].z = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
			BREAK
			
			// Grab the heading2
			CASE DM_HASH_HEADING2
				sMenuItemLocal.fHeading[2] = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
			BREAK
			
			// Grab the codeID
			CASE DM_HASH_CODE_ID
				sMenuItemLocal.iCodeID = GET_HASH_KEY(GET_STRING_FROM_XML_NODE_ATTRIBUTE(eachAttribute))
			BREAK
			
			// Grab the Pre-launch codeID
			CASE DM_HASH_CODE_ID_PRELAUNCH
				sMenuItemLocal.iCodeIDPreLaunch = GET_HASH_KEY(GET_STRING_FROM_XML_NODE_ATTRIBUTE(eachAttribute))
			BREAK
			
			// Grab the labelID
			CASE DM_HASH_LABEL_ID
				sMenuItemLocal.iLabelID = GET_HASH_KEY(GET_STRING_FROM_XML_NODE_ATTRIBUTE(eachAttribute))
			BREAK
			
			// Grab the labelID2
			CASE DM_HASH_LABEL_ID_2
				sMenuItemLocal.iExtraLabelID[0] = GET_HASH_KEY(GET_STRING_FROM_XML_NODE_ATTRIBUTE(eachAttribute))
			BREAK
			
			// Grab the labelID3
			CASE DM_HASH_LABEL_ID_3
				sMenuItemLocal.iExtraLabelID[1] = GET_HASH_KEY(GET_STRING_FROM_XML_NODE_ATTRIBUTE(eachAttribute))
			BREAK
			
			// Grab the displayID
			CASE DM_HASH_DISPLAY_ID
				sMenuItemLocal.iDisplayID = GET_HASH_KEY(GET_STRING_FROM_XML_NODE_ATTRIBUTE(eachAttribute))
			BREAK
			
			// Grab the selectID
			CASE DM_HASH_ENABLE_ID
				sMenuItemLocal.iEnableID = GET_HASH_KEY(GET_STRING_FROM_XML_NODE_ATTRIBUTE(eachAttribute))
			BREAK
			
			// Grab the roomType
			CASE DM_HASH_INTERIOR
				sMenuItemLocal.sInterior = GET_STRING_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
			BREAK
			
			// Grab the author
			CASE DM_HASH_AUTHOR
				sMenuItemLocal.sAuth = GET_STRING_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
			BREAK
			
			// Grab the missionID
			CASE DM_HASH_MISSION_ID
				sMenuItemLocal.sMissionID = GET_STRING_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
			BREAK
			
			// Grab the missionName
			CASE DM_HASH_MISSION_NAME
				sMenuItemLocal.sTitle = GET_STRING_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
			BREAK
			
			// Grab the story
			CASE DM_HASH_MISSION_STORY
				// Store the mission description returned from bugstar query
				sLabel = ""
				sLabel += sMenuItemLocal.sMissionID
				sLabel += "_MD1"
				ADD_DEBUG_STRING(sLabel, GET_STRING_FROM_XML_NODE_ATTRIBUTE(eachAttribute))
			BREAK
			
			// Grab the description
			CASE DM_HASH_MISSION_DESC
				// Store the story description returned from bugstar query
				sLabel = ""
				sLabel += sMenuItemLocal.sMissionID
				sLabel += "_SD1"
				ADD_DEBUG_STRING(sLabel, GET_STRING_FROM_XML_NODE_ATTRIBUTE(eachAttribute))
			BREAK
			
			// Grab the comments
			CASE DM_HASH_MISSION_COMMENTS
				// Store the mission comments returned from bugstar query
				sLabel = ""
				sLabel += sMenuItemLocal.sMissionID
				sLabel += "_MC1"
				ADD_DEBUG_STRING(sLabel, GET_STRING_FROM_XML_NODE_ATTRIBUTE(eachAttribute))
			BREAK
			
			// Grab the character
			CASE DM_HASH_CHARACTER
				sMenuItemLocal.iCharacter = GET_HASH_KEY(GET_STRING_FROM_XML_NODE_ATTRIBUTE(eachAttribute))
			BREAK
			
			// Grab the percentage
			CASE DM_HASH_PERCENTAGE
				sMenuItemLocal.iPerc = GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
			BREAK
			
			// Grab the important bugs
			CASE DM_HASH_IMPORTANT_BUGS
				sMenuItemLocal.iImportantBugs = GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
			BREAK
			
			// Grab the highlight flag
			CASE DM_HASH_HIGHLIGHT
				IF GET_BOOL_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
					SET_DEBUG_MENU_ITEM_FLAG(sMenuItemLocal.iFlags, DM_BIT_HIGHLIGHT)
				ENDIF
			BREAK
			
			// Grab the blip flag
			CASE DM_HASH_BLIPCOORDS
				IF NOT GET_BOOL_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
					CLEAR_DEBUG_MENU_ITEM_FLAG(sMenuItemLocal.iFlags, DM_BIT_BLIPCOORDS)
				ENDIF
			BREAK
			
			// Grab the vehicle flag
			CASE DM_HASH_VEHICLE
				IF GET_BOOL_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
					SET_DEBUG_MENU_ITEM_FLAG(sMenuItemLocal.iFlags, DM_BIT_VEHICLE)
				ENDIF
			BREAK
			
			// Grab the keep vehicle flag
			CASE DM_HASH_KEEP_VEHICLE
				IF GET_BOOL_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
					SET_DEBUG_MENU_ITEM_FLAG(sMenuItemLocal.iFlags, DM_BIT_KEEP_VEH)
				ELSE
					CLEAR_DEBUG_MENU_ITEM_FLAG(sMenuItemLocal.iFlags, DM_BIT_KEEP_VEH)
				ENDIF
			BREAK
			
			// Grab the is launchScript flag
			CASE DM_HASH_LAUNCH_SCRIPT
				IF GET_BOOL_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
					SET_DEBUG_MENU_ITEM_FLAG(sMenuItemLocal.iFlags, DM_BIT_LAUNCH_SCRIPT)
				ELSE
					CLEAR_DEBUG_MENU_ITEM_FLAG(sMenuItemLocal.iFlags, DM_BIT_LAUNCH_SCRIPT)
				ENDIF
			BREAK
			
			// Grab the rank
			CASE DM_HASH_MISSION_RANK
				sMenuItemLocal.iRank = GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
			BREAK
			
			// Grab the hour
			CASE DM_HASH_CLOCK_HOUR
				sMenuItemLocal.iHour = GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
			BREAK
			
			// Grab the iBugstarID
			CASE DM_HASH_BUG_BUGSTAR_ID
				sBugstarDataLocal.iBugstarID = GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
			BREAK
			
			// Grab the bugs flag - pack into first slot of the array
			CASE DM_HASH_BUG_NUM
				sBugstarDataLocal.iDeptBugs[0] = GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
			BREAK
			
			// Grab the owner flag - pack into first slot of the array
			CASE DM_HASH_BUG_OWNER
				// Store the owner returned from bugstar query
				sLabel = ""
				sLabel += sMenuItemLocal.sMissionID
				IF iNodeHash = DM_HASH_TOP5_OWNERS
					sLabel += "T5OWN"
					sLabel += sBugstarData.iOwnerCount
					ADD_DEBUG_STRING(sLabel, GET_STRING_FROM_XML_NODE_ATTRIBUTE(eachAttribute))
				ELIF iNodeHash = DM_HASH_TEAM_INFO
					sLabel += "DPTOWN"
					sLabel += sBugstarData.iOwnerCount
					ADD_DEBUG_STRING(sLabel, GET_STRING_FROM_XML_NODE_ATTRIBUTE(eachAttribute))
				ENDIF
			BREAK
			
			// Grab the fixedBugs flag
			CASE DM_HASH_FIXEDBUGS
				sBugstarDataLocal.iFixedBugs = GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
			BREAK
			
			// Grab the avgBugAge flag
			CASE DM_HASH_AVG_BUG_AGE
				sBugstarDataLocal.iAvgBugAge = GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
			BREAK
			
			// Grab the overdueBugs flag
			CASE DM_HASH_OVERDUEBUGS
				sBugstarDataLocal.iOverdueBugs = GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
			BREAK
			
			// Grab the aBugs flag
			CASE DM_HASH_A_BUGS
				sBugstarDataLocal.iABugs = GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
			BREAK
			
			// Grab the bBugs flag
			CASE DM_HASH_B_BUGS
				sBugstarDataLocal.iBBugs = GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
			BREAK
			
			// Grab the cBugs flag
			CASE DM_HASH_C_BUGS
				sBugstarDataLocal.iCBugs = GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
			BREAK
			
			// Grab the dBugs flag
			CASE DM_HASH_D_BUGS
				sBugstarDataLocal.iDBugs = GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
			BREAK
			
			// Grab the tasks flag
			CASE DM_HASH_TASK_BUGS
				sBugstarDataLocal.iTasks = GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
			BREAK
			
			// Grab the todos flag
			CASE DM_HASH_TODO_BUGS
				sBugstarDataLocal.iTodos = GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
			BREAK
			
			// Grab the wish flag
			CASE DM_HASH_WISH_BUGS
				sBugstarDataLocal.iWish = GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
			BREAK
			
		// End of the attribute switch statement
		ENDSWITCH
	
	// End for loop going through the attributes
	ENDFOR
		
	sMenuItem = sMenuItemLocal
	sBugstarData = sBugstarDataLocal
ENDPROC



PROC BUGSTAR_GET_DETAILED_MISSION_DATA( BUGSTAR_DATA_STRUCT &sBugstarData, DEBUG_MENU_ITEM_STRUCT &sItem, STRING sMissionID)
	INT iBugstarDept = 0
	INT iBugstarOwner = 0
	BOOL bBugstarItem = FALSE
	BOOL bCopyMissionID = FALSE
	DEBUG_MENU_ITEM_STRUCT sItemCopy
	BUGSTAR_DATA_STRUCT sBugstarCopy

	RESET_ALL_BUGSTAR_DATA(sBugstarData)

	IF g_debugMenuControl.bUseBugstarQuery
		// We need to set the mission ID here so that we can add the mission/story description to debug text
		// If we are using a bugstar query, we know we already have the correct mission
		sItemCopy.sMissionID = sMissionID
		bCopyMissionID = TRUE
	ENDIF
	INT iMissionID = GET_HASH_KEY(sMissionID)
	
	
	// Integers used for the FOR loops going through all the nodes and attributes
	INT eachNode
	INT iNodeCount = GET_NUMBER_OF_XML_NODES()
	
	// Loop through all the nodes
	FOR eachNode = 0 TO (iNodeCount-1)
			
		// Convert the current node name to a hash key so it can be used in a switch against the const's created at the top of this file
		SWITCH GET_HASH_KEY(GET_XML_NODE_NAME())
			
			/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			// Case statement for the "bugstarItem" node, once we hit this we want to look at all the attributes
			// Attributes: todos, file, author, percentage, highlight
			//
			CASE DM_HASH_ITEM_BUGSTAR
			
				// bail out if we have already processed a matching bugstar item
				IF bBugstarItem
					EXIT
				ELSE
					// Grab the data for this item
					BUGSTAR_GET_NODE_ATTRIBUTES(sItemCopy, sBugstarCopy, DM_HASH_ITEM_BUGSTAR, bCopyMissionID)
					
					// If we have found a match for the current item then update the bugstar data for this item
					IF GET_HASH_KEY(sItemCopy.sMissionID) = iMissionID
					
						// Match found so update the detailed bugstar specific data for the item that we found (iCount)
						sBugstarData.iFixedBugs = sBugstarCopy.iFixedBugs
						sBugstarData.iAvgBugAge = sBugstarCopy.iAvgBugAge
						sBugstarData.iOverdueBugs = sBugstarCopy.iOverdueBugs
						sBugstarData.iABugs = sBugstarCopy.iABugs
						sBugstarData.iBBugs = sBugstarCopy.iBBugs
						sBugstarData.iCBugs = sBugstarCopy.iCBugs
						sBugstarData.iDBugs = sBugstarCopy.iDBugs
						sBugstarData.iTasks = sBugstarCopy.iTasks
						sBugstarData.iTodos = sBugstarCopy.iTodos
						sBugstarData.iWish = sBugstarCopy.iWish
						sBugstarData.iBugstarID	= sBugstarCopy.iBugstarID
						
						sItem.sTitle = sItemCopy.sTitle
						sItem.sTitleExtra = sItemCopy.sTitleExtra
						sItem.sAuth = sItemCopy.sAuth
						sItem.sInfo = sItemCopy.sInfo
						sItem.sExtraInfo = sItemCopy.sExtraInfo

						bBugstarItem = TRUE
					ENDIF
				ENDIF
				
			// End of the bugstar case statement
			BREAK
			
			/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			// Case statement for the "teamInfo" node, once we hit this we want to look at all the attributes
			// Attributes: id, noBugs
			//
			CASE DM_HASH_TEAM_INFO
			
				// This node will only appear in a bugstar item
				// We only want to deal with this if we are using data from the last Bugstar item we come across
				IF bBugstarItem
				
					// Set the owner count for use with the debug text label.
					sBugstarCopy.iOwnerCount = iBugstarDept
				
					// Grab the data for this item
					BUGSTAR_GET_NODE_ATTRIBUTES(sItemCopy, sBugstarCopy, DM_HASH_TEAM_INFO, bCopyMissionID)
					
					//Only store the data if bugs <> 0
					IF sBugstarCopy.iDeptBugs[0] <> 0
					AND iBugstarDept < DM_MAX_BUGSTAR_DEPTS
						sBugstarData.iDeptBugs[iBugstarDept] = sBugstarCopy.iDeptBugs[0]
						iBugstarDept++
					ENDIF
				ENDIF
				
			BREAK
			
			
			/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			// Case statement for the "topBugOwners" node, once we hit this we want to look at all the attributes
			// Attributes: author, noBugs
			//
			CASE DM_HASH_TOP5_OWNERS
			
				// This node will only appear in a bugstar item
				// We only want to deal with this if we are using data from the last Bugstar item we come across
				IF bBugstarItem
				
					// Set the owner count for use with the debug text label.
					sBugstarCopy.iOwnerCount = iBugstarOwner
				
					// Grab the data for this item
					BUGSTAR_GET_NODE_ATTRIBUTES(sItemCopy, sBugstarCopy, DM_HASH_TOP5_OWNERS, bCopyMissionID)
					
					IF sBugstarCopy.iDeptBugs[0] <> 0 // Top5 bugs and Dept bugs both use same attribute name so we have to share variable (iDeptBugs, not iTop5Bugs)
					AND iBugstarOwner < 5
						sBugstarData.iTop5Bugs[iBugstarOwner] = sBugstarCopy.iDeptBugs[0]
						sBugstarData.iTop5BugsAvgAge[iBugstarOwner] = sBugstarCopy.iAvgBugAge
						iBugstarOwner++
					ENDIF
				ENDIF
				
			BREAK
			
			
		// End of the node switch statement
		ENDSWITCH
			
		// Tell the script to goto the next node in the xml file
		GET_NEXT_XML_NODE()
	
	// End of for loop going through the nodes
	ENDFOR
ENDPROC


		
FUNC BOOL BUGSTAR_DETAILED_MISSION_DATA(BUGSTAR_DATA_STRUCT &sBugstarData, DEBUG_MENU_ITEM_STRUCT &sItem, STRING sMissionID)
	BOOL bUseQuery = g_debugMenuControl.bUseBugstarQuery // Cache this!
	
	IF bUseQuery
		// Reset flags if no query is active or we want to start a new one
		IF NOT HAS_BUGSTAR_QUERY_STARTED()
		OR IS_BUGSTAR_PROCESS_FLAG_SET(sBugstarData.eProcessFlags, BSTAR_PROCESS_BUGSTAR_QUERY_RESET)
			CLEAR_BUGSTAR_PROCESS_FLAG(sBugstarData.eProcessFlags, BSTAR_PROCESS_BUGSTAR_QUERY_STARTED | BSTAR_PROCESS_BUGSTAR_QUERY_PENDING | BSTAR_PROCESS_BUGSTAR_QUERY_RESET)
		ENDIF
		
		// Start the query if we havent done so already
		IF NOT IS_BUGSTAR_PROCESS_FLAG_SET(sBugstarData.eProcessFlags, BSTAR_PROCESS_BUGSTAR_QUERY_STARTED)
			IF IS_BUGSTAR_QUERY_SAFE_TO_START(TRUE)
				TEXT_LABEL_63 sQuery = "Missions/Summary/"
				sQuery += sMissionID
				sQuery += "?filter=detailed"
				
				CDEBUG1LN(DEBUG_BUGSTAR, "...starting new UNIFIED query: ", sQuery)
				SETUP_BUGSTAR_QUERY(sBugstarData, sQuery)
			ENDIF
			RETURN FALSE
		ENDIF
		
		// Wait for the query to finish
		IF IS_BUGSTAR_PROCESS_FLAG_SET(sBugstarData.eProcessFlags, BSTAR_PROCESS_BUGSTAR_QUERY_PENDING)
			IF (IS_BUGSTAR_QUERY_PENDING())
				RETURN FALSE
			ENDIF
			CLEAR_BUGSTAR_PROCESS_FLAG(sBugstarData.eProcessFlags, BSTAR_PROCESS_BUGSTAR_QUERY_PENDING)
		ENDIF
		
		IF NOT IS_BUGSTAR_QUERY_SUCCESSFUL()
			CPRINTLN(DEBUG_BUGSTAR, "BUGSTAR_DETAILED_MISSION_DATA: Bugstar Query Failed")
			// Problem with bugstar?
			g_debugMenuControl.bUseBugstarQuery = FALSE
			CLEANUP_BUGSTAR_QUERY(sBugstarData)
			RETURN FALSE
		ENDIF
		CDEBUG1LN(DEBUG_BUGSTAR, "...UNIFIED query successful")
		IF NOT CREATE_XML_FROM_BUGSTAR_QUERY()
			CPRINTLN(DEBUG_BUGSTAR, "BUGSTAR_DETAILED_MISSION_DATA: Cannot create XML from Bugstar Query")
			// Problem with bugstar?
			g_debugMenuControl.bUseBugstarQuery = FALSE
			CLEANUP_BUGSTAR_QUERY(sBugstarData)
			RETURN FALSE
		ENDIF
	
	ELSE
		// Load up the bugstar file if it exists
		IF NOT LOAD_XML_FILE(sBugstarData.sBugstarFile)
			// Failed to load XML file
			CPRINTLN(DEBUG_BUGSTAR, "BUGSTAR_DETAILED_MISSION_DATA: Failed to load xml file '", sBugstarData.sBugstarFile,"'")
			RETURN TRUE
		ENDIF
		CDEBUG1LN(DEBUG_BUGSTAR, "BUGSTAR_DETAILED_MISSION_DATA: Loading xml file '", sBugstarData.sBugstarFile, "'")
	ENDIF

	// Check that the xml contains some nodes
	IF GET_NUMBER_OF_XML_NODES() = 0
		CPRINTLN(DEBUG_BUGSTAR, "BUGSTAR_DETAILED_MISSION_DATA: No XML Nodes for Bugstar Query")
		RETURN TRUE
	ENDIF

	// Do the real work
	CDEBUG1LN(DEBUG_BUGSTAR, "BUGSTAR_DETAILED_MISSION_DATA: XML/query Loaded properly - grabbing data")
	BUGSTAR_GET_DETAILED_MISSION_DATA(sBugstarData, sItem, sMissionID)

	// Now we have gone through all the nodes you need to unload the xml file
	DELETE_XML_FILE()

	IF bUseQuery
		CLEANUP_BUGSTAR_QUERY(sBugstarData)
	ENDIF
	RETURN TRUE
ENDFUNC

		
		/// PURPOSE: Temp proc to cycle through array of INTs that point to bugstar items and compare missionIDs
FUNC BOOL BUGSTAR_COMPARE_DATA(DEBUG_MENU_ITEM_STRUCT &sItems[], INT &iBugstarItems[], INT iBugstarItemCount, INT iMissionID, INT &iArrayPos)
	
	// Cycle through our bugstar items
	INT i
	FOR i = 0 TO iBugstarItemCount-1
		// See if we can match up the missionsID for the bugstar item and the current xml item (iMissionID)
		IF GET_HASH_KEY(sItems[iBugstarItems[i]].sMissionID) = iMissionID
		AND NOT IS_DEBUG_MENU_ITEM_FLAG_SET(sItems[iBugstarItems[i]].iFlags, DM_BIT_BUG_INFO) // Do not allow item to be processed twice.
			iArrayPos = i
			RETURN TRUE
		ENDIF
	ENDFOR
	
	RETURN FALSE
ENDFUNC

PROC BUGSTAR_GET_BASIC_MISSION_DATA(DEBUG_MENU_ITEM_STRUCT &sItems[], INT &iBugstarItems[], INT iBugstarItemCount)
	
	// Integers used for the FOR loops going through all the nodes and attributes
	INT eachNode
	INT eachAttribute
	
	// The struct we use to store the data for the current item
	DEBUG_MENU_ITEM_STRUCT sItemCopy
	BUGSTAR_DATA_STRUCT sBugstarData

	INT iMissionIDCount
	INT iCount
	INT iMissionIDHash
	INT iAttributeCount
	INT iNodeCount = GET_NUMBER_OF_XML_NODES()
	
	// Loop through all the nodes
	FOR eachNode = 0 TO (iNodeCount-1)
			
		// Convert the current node name to a hash key so it can be used in a switch against the const's created at the top of this file
		IF GET_HASH_KEY(GET_XML_NODE_NAME()) = DM_HASH_ITEM_BUGSTAR
			
			iMissionIDCount = 0
			/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			// Case statement for the "bugstarItem" node, once we hit this we want to look at all the attributes
			// Attributes: todos, file, author, percentage, highlight
			//
			
			// Grab the mission ID for this attribute and compare
			iMissionIDHash = 0
			iAttributeCount = GET_NUMBER_OF_XML_NODE_ATTRIBUTES()
			FOR eachAttribute = 0 TO (iAttributeCount-1)
				// Convert the attribute name to a hash key like we did with the nodes so we can use a switch statement
				SWITCH GET_HASH_KEY(GET_XML_NODE_ATTRIBUTE_NAME(eachAttribute))
					// Grab the missionID
					CASE DM_HASH_MISSION_ID
						iMissionIDHash = GET_HASH_KEY(GET_STRING_FROM_XML_NODE_ATTRIBUTE(eachAttribute))
						eachAttribute = iAttributeCount+1//Bail
					BREAK
				ENDSWITCH
			ENDFOR
			
			IF iMissionIDHash != 0
				// We have put the comparison into a func so that we can bail out when we find a match (potentially saves thousands of checks!)
				WHILE BUGSTAR_COMPARE_DATA(sItems, iBugstarItems, iBugstarItemCount, iMissionIDHash, iCount)
				
					// Now grab all the data for this item
					BUGSTAR_GET_NODE_ATTRIBUTES(sItemCopy, sBugstarData, DM_HASH_ITEM_BUGSTAR)
						
					// Match found so update the bugstar specific data for the item that we found (iCount)
					sItems[iBugstarItems[iCount]].sInfo 			= sItemCopy.sInfo
					sItems[iBugstarItems[iCount]].sExtraInfo 		= sItemCopy.sExtraInfo
					sItems[iBugstarItems[iCount]].sAuth 			= sItemCopy.sAuth
					sItems[iBugstarItems[iCount]].iPerc 			= sItemCopy.iPerc
					sItems[iBugstarItems[iCount]].iImportantBugs 	= sItemCopy.iImportantBugs
					// Only update script name if missing
					IF GET_HASH_KEY(sItems[iBugstarItems[iCount]].sFile) = 0
						sItems[iBugstarItems[iCount]].sFile = sItemCopy.sFile
					ENDIF
					
					// We only want to udpdate specific flags
					IF IS_DEBUG_MENU_ITEM_FLAG_SET(sItemCopy.iFlags, DM_BIT_HIGHLIGHT)
						SET_DEBUG_MENU_ITEM_FLAG(sItems[iBugstarItems[iCount]].iFlags, DM_BIT_HIGHLIGHT)
					ELSE
						CLEAR_DEBUG_MENU_ITEM_FLAG(sItems[iBugstarItems[iCount]].iFlags, DM_BIT_HIGHLIGHT)
					ENDIF
					SET_DEBUG_MENU_ITEM_FLAG(sItems[iBugstarItems[iCount]].iFlags, DM_BIT_BUG_INFO)
					iMissionIDCount++
				ENDWHILE
			ENDIF
			IF iMissionIDCount = 0
				// Mission name not required so remove if we added
				/*TEXT_LABEL_15 sMissionNameLabel
				sMissionNameLabel = ""
				sMissionNameLabel += sItemCopy.sMissionID
				sMissionNameLabel += "_MD1"
				REMOVE_DEBUG_STRING_WITH_THIS_KEY(sMissionNameLabel)*/
			ENDIF
		ENDIF	
			
		// Tell the script to goto the next node in the xml file
		GET_NEXT_XML_NODE()
			
	// End of for loop going through the nodes
	ENDFOR
ENDPROC

FUNC BOOL BUGSTAR_BASIC_MISSION_DATA(DEBUG_MENU_ITEM_STRUCT &sItems[], BUGSTAR_DATA_STRUCT &sBugstarData, INT &iBugstarItems[], INT iBugstarItemCount)
	BOOL bUseQuery = g_debugMenuControl.bUseBugstarQuery // Cache this!
	
	INT iCount
	IF bUseQuery
		IF NOT HAS_BUGSTAR_QUERY_STARTED()
		OR IS_BUGSTAR_PROCESS_FLAG_SET(sBugstarData.eProcessFlags, BSTAR_PROCESS_BUGSTAR_QUERY_RESET)
			CLEAR_BUGSTAR_PROCESS_FLAG(sBugstarData.eProcessFlags, BSTAR_PROCESS_BUGSTAR_QUERY_STARTED | BSTAR_PROCESS_BUGSTAR_QUERY_PENDING | BSTAR_PROCESS_BUGSTAR_QUERY_RESET)
		ENDIF
		
		// Start the query if we havent done so already
		IF NOT IS_BUGSTAR_PROCESS_FLAG_SET(sBugstarData.eProcessFlags, BSTAR_PROCESS_BUGSTAR_QUERY_STARTED)
			IF IS_BUGSTAR_QUERY_SAFE_TO_START(TRUE)
				INT iItem
				BOOL bQueryStarted
				TEXT_LABEL_63 sQuery = "Missions/Summary/"
				
				FOR iCount = 0 TO COUNT_OF(sItems)-1
					
					IF NOT ARE_STRINGS_EQUAL(sItems[iCount].sMissionID, "")
						sQuery += sItems[iCount].sMissionID
						sQuery += ","
						iItem++
					ENDIF
					
					// Add 6 mission id's at a time
					IF iItem >= 5
						IF NOT bQueryStarted
							START_BUGSTAR_QUERY_APPEND()
							ADD_BUGSTAR_QUERY_STRING(sQuery)
							CDEBUG1LN(DEBUG_BUGSTAR, "...starting new query: ", sQuery)
							bQueryStarted = TRUE
						ELSE
							ADD_BUGSTAR_QUERY_STRING(sQuery)
							CDEBUG1LN(DEBUG_BUGSTAR, "...appending to query: ", sQuery)
						ENDIF
						// Reset for next 6
						sQuery = ""
						iItem = 0
					ENDIF
				ENDFOR
				
				// Add any remaining id's
				IF iItem > 0
					IF NOT bQueryStarted
						START_BUGSTAR_QUERY_APPEND()
						ADD_BUGSTAR_QUERY_STRING(sQuery)
						CDEBUG1LN(DEBUG_BUGSTAR, "...starting new query: ", sQuery)
						bQueryStarted = TRUE
					ELSE
						ADD_BUGSTAR_QUERY_STRING(sQuery)
						CDEBUG1LN(DEBUG_BUGSTAR, "...appending to query: ", sQuery)
					ENDIF
				ENDIF
				
				IF bQueryStarted
					sQuery = "?filter=basic"
					ADD_BUGSTAR_QUERY_STRING(sQuery)
					CDEBUG1LN(DEBUG_BUGSTAR, "...appending to query: ", sQuery)
					STOP_BUGSTAR_QUERY_APPEND()
					SET_BUGSTAR_QUERY_STARTED()
					SET_BUGSTAR_PROCESS_FLAG(sBugstarData.eProcessFlags, BSTAR_PROCESS_BUGSTAR_QUERY_STARTED | BSTAR_PROCESS_BUGSTAR_QUERY_PENDING)
				ELSE
					// No bugstar items to process.
					RETURN TRUE
				ENDIF
			ENDIF
			RETURN FALSE
		ENDIF
		
		// Wait for the query to finish
		IF IS_BUGSTAR_PROCESS_FLAG_SET(sBugstarData.eProcessFlags, BSTAR_PROCESS_BUGSTAR_QUERY_PENDING)
			IF (IS_BUGSTAR_QUERY_PENDING())
				RETURN FALSE
			ENDIF
			CLEAR_BUGSTAR_PROCESS_FLAG(sBugstarData.eProcessFlags, BSTAR_PROCESS_BUGSTAR_QUERY_PENDING)
		ENDIF
		
		// If the query was successful, create the xml and parse
		IF NOT IS_BUGSTAR_QUERY_SUCCESSFUL()
			CPRINTLN(DEBUG_BUGSTAR, "BUGSTAR_BASIC_MISSION_DATA: Bugstar Query not successful")
			g_debugMenuControl.bUseBugstarQuery = FALSE
			CLEANUP_BUGSTAR_QUERY(sBugstarData)
			RETURN FALSE
		ENDIF
		CDEBUG1LN(DEBUG_BUGSTAR, "...query successful")
			
		IF NOT CREATE_XML_FROM_BUGSTAR_QUERY()
			CPRINTLN(DEBUG_BUGSTAR, "BUGSTAR_BASIC_MISSION_DATA: Unable to create XML from Bugstar Query")
			g_debugMenuControl.bUseBugstarQuery = FALSE
			CLEANUP_BUGSTAR_QUERY(sBugstarData)
			RETURN FALSE
		ENDIF
	ELSE
		// Load up the bugstar file if it exists
		IF NOT LOAD_XML_FILE(sBugstarData.sBugstarFile)
			// Failed to load XML file
			CPRINTLN(DEBUG_BUGSTAR, "DM_GET_BASIC_BUGSTAR_MISSION_DATA: Failed to load xml file '", sBugstarData.sBugstarFile, "'")
			RETURN TRUE
		ENDIF
		CDEBUG1LN(DEBUG_BUGSTAR, "DM_GET_BASIC_BUGSTAR_MISSION_DATA: Loading xml file '", sBugstarData.sBugstarFile, "'")
	ENDIF

	// Check that the xml contains some nodes
	IF GET_NUMBER_OF_XML_NODES() = 0
		CPRINTLN(DEBUG_BUGSTAR, "BUGSTAR_BASIC_MISSION_DATA: No nodes in XML from Bugstar Query")
		DELETE_XML_FILE()
		IF bUseQuery
			CLEANUP_BUGSTAR_QUERY(sBugstarData)
		ENDIF
		RETURN TRUE
	ENDIF
	
	// Do the real work
	CDEBUG1LN(DEBUG_BUGSTAR, "BUGSTAR_BASIC_MISSION_DATA: XML/query Loaded properly - grabbing data")
	BUGSTAR_GET_BASIC_MISSION_DATA(sItems, iBugstarItems, iBugstarItemCount)

	// Now we have gone through all the nodes you need to unload the xml file
	DELETE_XML_FILE()
	IF bUseQuery
		CLEANUP_BUGSTAR_QUERY(sBugstarData)
	ENDIF
	RETURN TRUE
ENDFUNC

PROC BUGSTAR_GET_NUMBER_OF_DAYS_OPEN(MISSION_BUGS_STRUCT &sBugData)

	// We are unable to have a days open field returned from Bugstar so we 
	// have to get the created on field and then compute the days open manually.
	// Currently waiting on a command to get real time date before we can display
	// it in the menu.
	
	IF NOT IS_STRING_NULL_OR_EMPTY(sBugData.sCurrentDate)
	
		// Dates are stored as YYYY-MM-DD
		CLOGLN(DEBUG_BUGSTAR, CHANNEL_SEVERITY_DEBUG3, "BUGSTAR_GET_NUMBER_OF_DAYS_OPEN")
		
		INT i, iTemp
		TEXT_LABEL_15 sDateLabel, sTempLabel
		BOOL bReverseDates
		INT iCurrentDay, iCurrentMonth, iCurrentYear
		INt iBugDay, iBugMonth, iBugYear
		INT iStoredDay, iStoredMonth, iStoredYear
		INT iDaysOpen
		
		INT iDaysInMonth[13]
		iDaysInMonth[1] = 31 // Jan
		iDaysInMonth[2] = 28 // Feb, ignore leap years
		iDaysInMonth[3] = 31 // Mar
		iDaysInMonth[4] = 30 // Apr
		iDaysInMonth[5] = 31 // May
		iDaysInMonth[6] = 30 // Jun
		iDaysInMonth[7] = 31 // Jul
		iDaysInMonth[8] = 31 // Aug
		iDaysInMonth[9] = 30 // Sep
		iDaysInMonth[10] = 31 // Oct
		iDaysInMonth[11] = 30 // Nov
		iDaysInMonth[12] = 31 // Dec
		
		sTempLabel = GET_STRING_FROM_STRING(sBugData.sCurrentDate, 0, 4)
		STRING_TO_INT(sTempLabel, iStoredYear)
		sTempLabel = GET_STRING_FROM_STRING(sBugData.sCurrentDate, 5, 7)
		STRING_TO_INT(sTempLabel, iStoredMonth)
		sTempLabel = GET_STRING_FROM_STRING(sBugData.sCurrentDate, 8, 10)
		STRING_TO_INT(sTempLabel, iStoredDay)
	
		CLOGLN(DEBUG_BUGSTAR, CHANNEL_SEVERITY_DEBUG3, "Current date = ", iStoredDay, "/", iStoredMonth, "/", iStoredYear)
		
		REPEAT DM_MAX_MISSION_BUGS i
			IF sBugData.iBugNumber[i] != 0
				sDateLabel = ""
				sDateLabel += sBugData.iBugNumber[i]
				sDateLabel += "_DO"
				IF DOES_TEXT_LABEL_EXIST(sDateLabel)
					
					// Get bug date
					sDateLabel = GET_STRING_FROM_TEXT_FILE(sDateLabel)
					sTempLabel = GET_STRING_FROM_STRING(sDateLabel, 0, 4)
					STRING_TO_INT(sTempLabel, iBugYear)
					sTempLabel = GET_STRING_FROM_STRING(sDateLabel, 5, 7)
					STRING_TO_INT(sTempLabel, iBugMonth)
					sTempLabel = GET_STRING_FROM_STRING(sDateLabel, 8, 10)
					STRING_TO_INT(sTempLabel, iBugDay)
					
					CLOGLN(DEBUG_BUGSTAR, CHANNEL_SEVERITY_DEBUG3, "Bug open = ", iBugDay, "/", iBugMonth, "/", iBugYear)
					
					// Work out the difference in days
					iDaysOpen = 0
					iCurrentDay = iStoredDay
					iCurrentMonth = iStoredMonth
					iCurrentYear = iStoredYear
					
					// Make sure created date is before the current date
					bReverseDates = FALSE
					IF (iCurrentYear > iBugYear)
					OR (iCurrentYear = iBugYear AND iCurrentMonth > iBugMonth)
					OR (iCurrentYear = iBugYear AND iCurrentMonth = iBugMonth AND iCurrentDay > iBugDay)
						bReverseDates = TRUE
						iTemp = iCurrentDay		iCurrentDay = iBugDay		iBugDay = iTemp
						iTemp = iCurrentMonth	iCurrentMonth = iBugMonth	iBugMonth = iTemp
						iTemp = iCurrentYear	iCurrentYear = iBugYear		iBugYear = iTemp
					ENDIF
					
					WHILE (iBugDay != iCurrentDay) OR (iBugMonth != iCurrentMonth) OR (iBugYear != iCurrentYear)
						
						WHILE (iBugDay != iCurrentDay)
							// Add the difference to days open
							IF (iCurrentDay > iBugDay)
								iDaysOpen += (iCurrentDay - iBugDay)
								iBugDay = iCurrentDay
							
							// Take days from next month
							ELSE
								iCurrentMonth--
								iCurrentDay += iDaysInMonth[FLOOR(WRAP(TO_FLOAT(iCurrentMonth), 1, 12))]
							ENDIF
						ENDWHILE
						
						WHILE (iBugMonth != iCurrentMonth)
							// Add to current day and reduce current month
							IF (iCurrentMonth > iBugMonth)
								iCurrentMonth--
								iCurrentDay += iDaysInMonth[FLOOR(WRAP(TO_FLOAT(iCurrentMonth), 1, 12))]
								IF (iCurrentMonth < 1)
									iCurrentMonth += 12
									iCurrentYear--
								ENDIF
								
							// Take months from next year
							ELSE
								iCurrentYear--
								iCurrentMonth += 12
							ENDIF
						ENDWHILE
						
						WHILE (iBugYear != iCurrentYear)
							// Add to current month and reduce current year
							IF (iCurrentYear > iBugYear)
								iCurrentMonth += 12
								iCurrentYear--
							
							// We need some more time so add to current year and reduce current day
							ELSE
								iCurrentYear++
								iDaysOpen -= 365
							ENDIF
						ENDWHILE
					ENDWHILE
					
					IF bReverseDates
						iDaysOpen *= -1
					ENDIF
					
					sBugData.iDaysOpen[i] = iDaysOpen
					
					CLOGLN(DEBUG_BUGSTAR, CHANNEL_SEVERITY_DEBUG3, "*****Bug ", sBugData.iBugNumber[i], " has been open for ", sBugData.iDaysOpen[i], " days.")
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC

PROC BUGSTAR_GET_MISSION_BUGS(MISSION_BUGS_STRUCT &sBugData)		
	// Integers used for the FOR loops going through all the nodes and attributes
	INT eachNode
	TEXT_LABEL_15 sLabel
	INT iBugCount = -1
	BOOL bSafeToAdd = FALSE

	RESET_MISSION_BUGS_DATA(sBugData, FALSE, FALSE, TRUE)
	
	INT iNodeCount = GET_NUMBER_OF_XML_NODES()
	
	// Loop through all the nodes
	FOR eachNode = 0 TO (iNodeCount-1)
		SWITCH GET_HASH_KEY(GET_XML_NODE_NAME())
		
			// 'id'
			CASE DM_HASH_BUG_ID
				// New bug discovered so increment count and see if we are safe to add to list
				IF sBugData.iTotalItems >= sBugData.iTopItem
				AND sBugData.iTotalItems < sBugData.iTopItem+DM_MAX_MISSION_BUGS
					bSafeToAdd = TRUE
				ELSE
					bSafeToAdd = FALSE
				ENDIF
				sBugData.iTotalItems++
				
				IF bSafeToAdd
					iBugCount++
					STRING_TO_INT(GET_STRING_FROM_XML_NODE(), sBugData.iBugNumber[iBugCount])
				ENDIF
			BREAK
			
			// 'summary'
			CASE DM_HASH_BUG_SUMMARY
				IF bSafeToAdd
					sLabel = ""
					sLabel += sBugData.iBugNumber[iBugCount]
					sLabel += "_S"
					CLOGLN(DEBUG_BUGSTAR, CHANNEL_SEVERITY_DEBUG3, "ADDING_DEBUG_STRING with label: ", sLabel)
					ADD_DEBUG_STRING(sLabel, GET_STRING_FROM_XML_NODE())
				ENDIF
			BREAK
			
			// 'developer'
			CASE DM_HASH_BUG_DEV
				IF bSafeToAdd
					sLabel = ""
					sLabel += sBugData.iBugNumber[iBugCount]
					sLabel += "_D"
					CLOGLN(DEBUG_BUGSTAR, CHANNEL_SEVERITY_DEBUG3, "ADDING_DEBUG_STRING with label: ", sLabel)
					ADD_DEBUG_STRING(sLabel, GET_STRING_FROM_XML_NODE())
				ENDIF
			BREAK
			
			// 'category'
			CASE DM_HASH_BUG_CLASS
				IF bSafeToAdd
					sLabel = ""
					sLabel += sBugData.iBugNumber[iBugCount]
					sLabel += "_C"
					CLOGLN(DEBUG_BUGSTAR, CHANNEL_SEVERITY_DEBUG3, "ADDING_DEBUG_STRING with label: ", sLabel)
					ADD_DEBUG_STRING(sLabel, GET_STRING_FROM_XML_NODE())
				ENDIF
			BREAK
			
			// 'dueDate'
			CASE DM_HASH_BUG_DUE
				IF bSafeToAdd
					sLabel = ""
					sLabel += sBugData.iBugNumber[iBugCount]
					sLabel += "_DD"
					CLOGLN(DEBUG_BUGSTAR, CHANNEL_SEVERITY_DEBUG3, "ADDING_DEBUG_STRING with label: ", sLabel)
					ADD_DEBUG_STRING(sLabel, GET_STRING_FROM_STRING(GET_STRING_FROM_XML_NODE(), 0, 10))
				ENDIF
			BREAK
			
			// 'createdOn'
			CASE DM_HASH_BUG_CREATED
				IF bSafeToAdd
					sLabel = ""
					sLabel += sBugData.iBugNumber[iBugCount]
					sLabel += "_DO"
					CLOGLN(DEBUG_BUGSTAR, CHANNEL_SEVERITY_DEBUG3, "ADDING_DEBUG_STRING with label: ", sLabel)
					ADD_DEBUG_STRING(sLabel, GET_STRING_FROM_STRING(GET_STRING_FROM_XML_NODE(), 0, 10))
				ENDIF
			BREAK
			
			// 'currentDate'
			CASE DM_HASH_CURRENT_DATE
				sBugData.sCurrentDate = GET_STRING_FROM_XML_NODE()
				CLOGLN(DEBUG_BUGSTAR, CHANNEL_SEVERITY_DEBUG3, "BUGSTAR GOT A CURRENT DATE OF: ", sBugData.sCurrentDate)
			BREAK
		ENDSWITCH
		
		// Tell the script to goto the next node in the xml file
		GET_NEXT_XML_NODE()
	ENDFOR
	
	BUGSTAR_GET_NUMBER_OF_DAYS_OPEN(sBugData)
ENDPROC	

FUNC BOOL BUGSTAR_MISSION_BUGS(BUGSTAR_DATA_STRUCT &sBugstarData, MISSION_BUGS_STRUCT &sBugData)

	// Reset flags if no query is active or we want to start a new one
	IF NOT HAS_BUGSTAR_QUERY_STARTED()
	OR IS_BUGSTAR_PROCESS_FLAG_SET(sBugstarData.eProcessFlags, BSTAR_PROCESS_BUGSTAR_QUERY_RESET)
		CLEAR_BUGSTAR_PROCESS_FLAG(sBugstarData.eProcessFlags, BSTAR_PROCESS_BUGSTAR_QUERY_STARTED | BSTAR_PROCESS_BUGSTAR_QUERY_PENDING | BSTAR_PROCESS_BUGSTAR_QUERY_RESET)
	ENDIF
	
	// Start the query if we havent done so already
	IF NOT IS_BUGSTAR_PROCESS_FLAG_SET(sBugstarData.eProcessFlags, BSTAR_PROCESS_BUGSTAR_QUERY_STARTED)
		IF IS_BUGSTAR_QUERY_SAFE_TO_START(TRUE)
			
			TEXT_LABEL_63 sQuery = "MissionsV2/"
			sQuery += sBugstarData.iBugstarID
			sQuery += "/"
			START_BUGSTAR_QUERY_APPEND()
			ADD_BUGSTAR_QUERY_STRING(sQuery)
			CDEBUG1LN(DEBUG_BUGSTAR, "...starting new query: ", sQuery)
			
			sQuery = "Bugs?fields=Id,Summary,Category,"
			ADD_BUGSTAR_QUERY_STRING(sQuery)
			CDEBUG1LN(DEBUG_BUGSTAR, "...appending to query: ", sQuery)
			
			sQuery = "DueDate,Developer,State,CreatedOn"
			ADD_BUGSTAR_QUERY_STRING(sQuery)
			CDEBUG1LN(DEBUG_BUGSTAR, "...appending to query: ", sQuery)
			
			STOP_BUGSTAR_QUERY_APPEND()
			SET_BUGSTAR_QUERY_STARTED()				
			SET_BUGSTAR_PROCESS_FLAG(sBugstarData.eProcessFlags, BSTAR_PROCESS_BUGSTAR_QUERY_STARTED | BSTAR_PROCESS_BUGSTAR_QUERY_PENDING)
		ENDIF
		RETURN FALSE
	ENDIF
	
	// Wait for the query to finish
	IF IS_BUGSTAR_PROCESS_FLAG_SET(sBugstarData.eProcessFlags, BSTAR_PROCESS_BUGSTAR_QUERY_PENDING)
		IF (IS_BUGSTAR_QUERY_PENDING())
			RETURN FALSE
		ENDIF
		CLEAR_BUGSTAR_PROCESS_FLAG(sBugstarData.eProcessFlags, BSTAR_PROCESS_BUGSTAR_QUERY_PENDING)
	ENDIF
	
	// If the query was successful, create the xml and parse
	IF  NOT IS_BUGSTAR_QUERY_SUCCESSFUL()
		CPRINTLN(DEBUG_BUGSTAR, "BUGSTAR_MISSION_BUGS: query not successful")
		CLEANUP_BUGSTAR_QUERY(sBugstarData)
		RETURN FALSE
	ENDIF
	CDEBUG1LN(DEBUG_BUGSTAR, "...query successful")
	IF NOT CREATE_XML_FROM_BUGSTAR_QUERY()
		CPRINTLN(DEBUG_BUGSTAR, "BUGSTAR_MISSION_BUGS: could not create XML from query")
		CLEANUP_BUGSTAR_QUERY(sBugstarData)
		RETURN FALSE
	ENDIF
	// Check that the xml contains some nodes
	IF GET_NUMBER_OF_XML_NODES() = 0
		CPRINTLN(DEBUG_BUGSTAR, "BUGSTAR_MISSION_BUGS: no nodes in XML from query")
		DELETE_XML_FILE()
		CLEANUP_BUGSTAR_QUERY(sBugstarData)
		RETURN FALSE
	ENDIF

	CDEBUG1LN(DEBUG_BUGSTAR, "BUGSTAR_MISSION_BUGS: Loaded XML/query successful - grabbing data")
	BUGSTAR_GET_MISSION_BUGS(sBugData)		
			
	DELETE_XML_FILE()
	CLEANUP_BUGSTAR_QUERY(sBugstarData)
	
	RETURN TRUE
ENDFUNC


#ENDIF // IS_DEBUG_BUILD
