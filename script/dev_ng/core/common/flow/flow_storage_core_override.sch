USING "flow_commands_core_override.sch"
USING "flow_storage_core.sch"
USING "flow_storage_GAME.sch"

// PURPOSE: Store 'Activate RandomChar Mission' command
// 
// INPUT PARAMS:	paramRandomCharID		The RandomChar ID
#IF NOT DEFINED(SETFLOW_ACTIVATE_RANDOMCHAR_MISSION)
PROC SETFLOW_ACTIVATE_RANDOMCHAR_MISSION()

ENDPROC
#ENDIF


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Store 'Display Help Text' command
// 
// INPUT PARAMS:	paramDisplayTime		The amount of time each section of the help message will remain on screen.
//					paramHelpLabel1			A text label pointing to the help text for the first section of the help message.
//					paramHelpLabel2			A text label pointing to the help text for the second section of the help message. Leave this blank to ignore this label.
//					paramHelpLabel3			A text label pointing to the help text for the third section of the help message. Leave this blank to ignore this label.
//					paramHelpLabel4			A text label pointing to the help text for the fourth section of the help message. Leave this blank to ignore this label.

#IF NOT DEFINED(SETFLOW_DISPLAY_HELP_TEXT)
PROC SETFLOW_DISPLAY_HELP_TEXT()

ENDPROC
#ENDIF



// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Store 'Display Help Text' command
// 
// INPUT PARAMS:	paramHelpLabel		The text label of the help message we want to remove from the queue.
#IF NOT DEFINED(SETFLOW_CANCEL_HELP_TEXT)
PROC SETFLOW_CANCEL_HELP_TEXT()

ENDPROC
#ENDIF


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Store 'Do Mission At Blip' command + variables
//
// INPUT PARAMS:	paramFilename		The mission filename
//					paramBlipID			The contact point blip ID associated with this mission
//					enumCharacterList	The player character that starts the mission
//					paramPassGoto		The command to move on to when the mission passes
//					paramFailGoto		The command to move on to when the mission fails

#IF NOT DEFINED(SETFLOW_DO_MISSION_AT_BLIP)
PROC SETFLOW_DO_MISSION_AT_BLIP()

ENDPROC
#ENDIF


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Store 'Do Mission Now' command + variables
//
// INPUT PARAMS:	paramFilename		The mission filename
//					paramPassGoto		The command to move on to when the mission passes
//					paramFailGoto		The command to move on to when the mission fails
//					paramDebugGoto		The command to move on to when we're stepping through the flow in debug mode with the flow launcher.

#IF NOT DEFINED(SETFLOW_DO_MISSION_NOW)
PROC SETFLOW_DO_MISSION_NOW()

ENDPROC
#ENDIF


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Store 'Is Mission Completed' command + variables
//

#IF NOT DEFINED(SETFLOW_IS_MISSION_COMPELTED)
PROC SETFLOW_IS_MISSION_COMPLETED()

ENDPROC
#ENDIF


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Store 'Launch Script and Forget' command + variables
//
// INPUT PARAMS:	paramFilename		The script filename

#IF NOT DEFINED(SETFLOW_LAUNCH_SCRIPT_AND_FORGET)
PROC SETFLOW_LAUNCH_SCRIPT_AND_FORGET()

ENDPROC
#ENDIF


// PURPOSE: Store 'Run Code ID' command + variables
//
#IF NOT DEFINED(SETFLOW_RUN_CODE_ID)
PROC SETFLOW_RUN_CODE_ID()

ENDPROC
#ENDIF


// PURPOSE: Store 'Debug Run Code ID' command + variables
//
#IF NOT DEFINED(SETFLOW_DEBUG_RUN_CODE_ID)
PROC SETFLOW_DEBUG_RUN_CODE_ID()

ENDPROC
#ENDIF

// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Store 'Enable Blip Long-Range' command
// 
#IF NOT DEFINED(SETFLOW_ENABLE_BLIP_LONG_RANGE)
PROC SETFLOW_ENABLE_BLIP_LONG_RANGE()

ENDPROC
#ENDIF


// PURPOSE: Store 'Disable Blip Long-Range' command
#IF NOT DEFINED(SETFLOW_DISABLE_BLIP_LONG_RANGE)
PROC SETFLOW_DISABLE_BLIP_LONG_RANGE()

ENDPROC
#ENDIF


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Store 'Set Building State' command
// 
// INPUT PARAMS:	paramBuildingID				An enum representing the building that will have its state changed.
//					paramStateID				An enum representing the state that the building will be changed to.
#IF NOT DEFINED(SETFLOW_SET_BUILDING_STATE)
PROC SETFLOW_SET_BUILDING_STATE()

ENDPROC
#ENDIF
// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Store 'Set Weapon Lock State' command
// 
// INPUT PARAMS:	paramWeaponCompID		The weapon enum
//					paramState				The new lock state: TRUE (1) or FALSE (0)
//
// NOTES:	The weapon enum will be converted to and stored as an INT

#IF NOT DEFINED(SETFLOW_SET_WEAPON_LOCK_STATE)
PROC SETFLOW_SET_WEAPON_LOCK_STATE()

ENDPROC
#ENDIF

// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Store 'Set Static Blip State' command
// 
// INPUT PARAMS:	paramBlipID				The static blip ID
//					paramState				The new static blip active state: TRUE (1) or FALSE (0)

#IF NOT DEFINED(SETFLOW_SET_STATIC_BLIP_STATE)
PROC SETFLOW_SET_STATIC_BLIP_STATE()

ENDPROC
#ENDIF
