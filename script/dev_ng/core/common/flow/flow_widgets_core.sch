
USING "rage_builtins.sch"
USING "globals.sch"
USING "flow_debug_core.sch"
USING "flow_private_GAME.sch"
USING "flow_reset_GAME.sch"
USING "flow_widgets_GAME.sch"

// PURPOSE:	Resets the gameflow and then activates the first strand.
// 
#IF NOT DEFINED(ACTIVATE_GAMEFLOW_AT_FIRST_STRAND)
PROC ACTIVATE_GAMEFLOW_AT_FIRST_STRAND()
ENDPROC
#ENDIF
// ===========================================================================================================
//      Mission Flow Widgets Creation
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//      Mission Flow Strand Widgets
// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Display the upper and lower boundaries within the commands array for a strand's commands
//
// INPUT PARAMS:    paramStrandAsInt        Strand ID as an INT

PROC CREATE_MISSION_FLOW_INTERNALS_STRAND_COMMAND_ARRAY_BOUNDARIES_WIDGETS(STRANDS paramStrand)

    ADD_WIDGET_INT_READ_ONLY("Lower Boundary (Commands Array)", g_flowUnsaved.flowCommandsIndex[paramStrand].lower)
    ADD_WIDGET_INT_READ_ONLY("Upper Boundary (Commands Array)", g_flowUnsaved.flowCommandsIndex[paramStrand].upper)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Set up all widget's required to view the internals of each strand
// 
PROC CREATE_MISSION_FLOW_INTERNALS_STRAND_WIDGETS(FLOW_DEBUG &paramDebugVars)

    STRANDS strandIndex
	#if USE_CLF_DLC
	    START_WIDGET_GROUP("Strands")
	        REPEAT MAX_STRANDS_CLF strandIndex
	            START_WIDGET_GROUP(GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(strandIndex))
	                ADD_WIDGET_BOOL("Strand Active", paramDebugVars.boolStrandActiveWidgetState[strandIndex])
	                CREATE_MISSION_FLOW_INTERNALS_STRAND_COMMAND_ARRAY_BOUNDARIES_WIDGETS(strandIndex)
	                ADD_WIDGET_INT_READ_ONLY("Current Command (Commands Array)", g_savedGlobalsClifford.sFlow.strandSavedVars[strandIndex].thisCommandPos)
	            STOP_WIDGET_GROUP()
	        ENDREPEAT
	    STOP_WIDGET_GROUP()
	#endif
	#if USE_NRM_DLC
	    START_WIDGET_GROUP("Strands")
	        REPEAT MAX_STRANDS_NRM strandIndex
	            START_WIDGET_GROUP(GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(strandIndex))
	                ADD_WIDGET_BOOL("Strand Active", paramDebugVars.boolStrandActiveWidgetState[strandIndex])
	                CREATE_MISSION_FLOW_INTERNALS_STRAND_COMMAND_ARRAY_BOUNDARIES_WIDGETS(strandIndex)
	                ADD_WIDGET_INT_READ_ONLY("Current Command (Commands Array)", g_savedGlobalsnorman.sFlow.strandSavedVars[strandIndex].thisCommandPos)
	            STOP_WIDGET_GROUP()
	        ENDREPEAT
	    STOP_WIDGET_GROUP()
	#endif
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
	    START_WIDGET_GROUP("Strands")
	        REPEAT MAX_STRANDS strandIndex
	            START_WIDGET_GROUP(GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(strandIndex))
	                ADD_WIDGET_BOOL("Strand Active", paramDebugVars.boolStrandActiveWidgetState[strandIndex])
	                CREATE_MISSION_FLOW_INTERNALS_STRAND_COMMAND_ARRAY_BOUNDARIES_WIDGETS(strandIndex)
	                ADD_WIDGET_INT_READ_ONLY("Current Command (Commands Array)", g_savedGlobals.sFlow.strandSavedVars[strandIndex].thisCommandPos)
	            STOP_WIDGET_GROUP()
	        ENDREPEAT
	    STOP_WIDGET_GROUP()
	#endif
	#endif 
ENDPROC




// -----------------------------------------------------------------------------------------------------------
//      Mission Flow Control Variables Widgets
// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Displays the current state of all Mission Flow synchronisation flags
// 
PROC CREATE_MISSION_FLOW_INTERNALS_SYNCHRONISATION_WIDGETS()

    SYNCHRONIZATION_IDS syncIndex
    #if USE_CLF_DLC
	    START_WIDGET_GROUP("Synchronisation Flags CLF")
	        REPEAT MAX_SYNCHRONISATION_IDS_CLF syncIndex
	            ADD_WIDGET_BOOL(GET_SYNCHRONISATION_DISPLAY_STRING_FROM_SYNC_ID(syncIndex), g_savedGlobalsClifford.sFlow.controls.syncIDs[syncIndex])
	        ENDREPEAT
	    STOP_WIDGET_GROUP()
	#endif
	#if USE_NRM_DLC
	    START_WIDGET_GROUP("Synchronisation Flags NRM")
	        REPEAT MAX_SYNCHRONISATION_IDS_NRM syncIndex
	            ADD_WIDGET_BOOL(GET_SYNCHRONISATION_DISPLAY_STRING_FROM_SYNC_ID(syncIndex), g_savedGlobalsnorman.sFlow.controls.syncIDs[syncIndex])
	        ENDREPEAT
	    STOP_WIDGET_GROUP()
	#endif
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
	    START_WIDGET_GROUP("Synchronisation Flags")
	        REPEAT MAX_SYNCHRONISATION_IDS syncIndex
	            ADD_WIDGET_BOOL(GET_SYNCHRONISATION_DISPLAY_STRING_FROM_SYNC_ID(syncIndex), g_savedGlobals.sFlow.controls.syncIDs[syncIndex])
	        ENDREPEAT
	    STOP_WIDGET_GROUP()
	#endif  
	#endif

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Displays the current state of all Mission Flow Flags
// 
PROC CREATE_MISSION_FLOW_INTERNALS_FLAGS_WIDGETS()

    FLOW_FLAG_IDS flowFlagIndex
    #if USE_CLF_DLC
	    START_WIDGET_GROUP("Flow Flags")
	        REPEAT MAX_FLOW_FLAG_IDS flowFlagIndex
	            ADD_WIDGET_BOOL(GET_FLAG_DISPLAY_STRING_FROM_FLAG_ID(flowFlagIndex), g_savedGlobalsClifford.sFlow.controls.flagIDs[flowFlagIndex])
	        ENDREPEAT
	    STOP_WIDGET_GROUP()
	#endif
	#if USE_NRM_DLC
	    START_WIDGET_GROUP("Flow Flags")
	        REPEAT MAX_FLOW_FLAG_IDS flowFlagIndex
	            ADD_WIDGET_BOOL(GET_FLAG_DISPLAY_STRING_FROM_FLAG_ID(flowFlagIndex), g_savedGlobalsnorman.sFlow.controls.flagIDs[flowFlagIndex])
	        ENDREPEAT
	    STOP_WIDGET_GROUP()
	#endif
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
	    START_WIDGET_GROUP("Flow Flags")
	        REPEAT MAX_FLOW_FLAG_IDS flowFlagIndex
	            ADD_WIDGET_BOOL(GET_FLAG_DISPLAY_STRING_FROM_FLAG_ID(flowFlagIndex), g_savedGlobals.sFlow.controls.flagIDs[flowFlagIndex])
	        ENDREPEAT
	    STOP_WIDGET_GROUP()
	#endif  
	#endif

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Displays the current state of all Mission Flow Ints
// 
PROC CREATE_MISSION_FLOW_INTERNALS_INTS_WIDGETS()

    FLOW_INT_IDS flowIntIndex
    #if USE_CLF_DLC
	    START_WIDGET_GROUP("Flow Ints")
	        REPEAT MAX_FLOW_INT_IDS flowIntIndex
	            ADD_WIDGET_INT_SLIDER(GET_INT_DISPLAY_STRING_FROM_INT_ID(flowIntIndex), g_savedGlobalsClifford.sFlow.controls.intIDs[flowIntIndex], -50, 200, 1)
	        ENDREPEAT
	    STOP_WIDGET_GROUP()
	#endif
    #if USE_NRM_DLC
	    START_WIDGET_GROUP("Flow Ints")
	        REPEAT MAX_FLOW_INT_IDS flowIntIndex
	            ADD_WIDGET_INT_SLIDER(GET_INT_DISPLAY_STRING_FROM_INT_ID(flowIntIndex), g_savedGlobalsnorman.sFlow.controls.intIDs[flowIntIndex], -50, 200, 1)
	        ENDREPEAT
	    STOP_WIDGET_GROUP()
	#endif	
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
	    START_WIDGET_GROUP("Flow Ints")
	        REPEAT MAX_FLOW_INT_IDS flowIntIndex
	            ADD_WIDGET_INT_SLIDER(GET_INT_DISPLAY_STRING_FROM_INT_ID(flowIntIndex), g_savedGlobals.sFlow.controls.intIDs[flowIntIndex], -50, 200, 1)
	        ENDREPEAT
	    STOP_WIDGET_GROUP()
	#endif  
	#endif

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Displays the current state of all Bits of a Mission Flow Bitset
// 
// INPUT PARAMS:        paramDebugVars          The struct containing all the debug variables

PROC CREATE_MISSION_FLOW_INTERNALS_BITSETS_WIDGETS()

    FLOW_BITSET_IDS flowBitsetIndex
    #if USE_CLF_DLC
		 START_WIDGET_GROUP("Flow Bitsets")
	        REPEAT MAX_FLOW_BITSET_IDS flowBitsetIndex
	            ADD_BIT_FIELD_WIDGET(GET_BITSET_DISPLAY_STRING_FROM_BITSET_ID(flowBitsetIndex), g_savedGlobalsClifford.sFlow.controls.bitsetIDs[flowBitsetIndex])
	        ENDREPEAT
    	STOP_WIDGET_GROUP()
	#endif
	 #if USE_NRM_DLC
		 START_WIDGET_GROUP("Flow Bitsets")
	        REPEAT MAX_FLOW_BITSET_IDS flowBitsetIndex
	            ADD_BIT_FIELD_WIDGET(GET_BITSET_DISPLAY_STRING_FROM_BITSET_ID(flowBitsetIndex), g_savedGlobalsnorman.sFlow.controls.bitsetIDs[flowBitsetIndex])
	        ENDREPEAT
    	STOP_WIDGET_GROUP()
	#endif
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
		START_WIDGET_GROUP("Flow Bitsets")
	        REPEAT MAX_FLOW_BITSET_IDS flowBitsetIndex
	            ADD_BIT_FIELD_WIDGET(GET_BITSET_DISPLAY_STRING_FROM_BITSET_ID(flowBitsetIndex), g_savedGlobals.sFlow.controls.bitsetIDs[flowBitsetIndex])
	        ENDREPEAT
	    STOP_WIDGET_GROUP()
	#endif  
	#endif
    

ENDPROC




// -----------------------------------------------------------------------------------------------------------
//      Mission Flow Internals Widgets
// -----------------------------------------------------------------------------------------------------------
#IF NOT DEFINED(CREATE_MISSION_FLOW_INTERNALS_WIDGETS_CUSTOM)
PROC CREATE_MISSION_FLOW_INTERNALS_WIDGETS_CUSTOM(FLOW_DEBUG &paramDebugVars)
	paramDebugVars.boolOnMission = paramDebugVars.boolOnMission
ENDPROC
#ENDIF

// PURPOSE: Set up all widget's required to view the mission flow internals
// 
// INPUT PARAMS:        paramDebugVars          The struct containing all the debug variables

PROC CREATE_MISSION_FLOW_INTERNALS_WIDGETS(FLOW_DEBUG &paramDebugVars)

    paramDebugVars.boolOnMission        = IS_CURRENTLY_ON_MISSION_TO_TYPE()
    paramDebugVars.boolInMultiplayer    = g_bInMultiplayer

    START_WIDGET_GROUP("Flow Internals")
        ADD_WIDGET_BOOL("On Mission (script flag) [READ-ONLY]? Yes (Ticked) or No (Unticked)", paramDebugVars.boolOnMission)
		#if USE_CLF_DLC
			ADD_WIDGET_BOOL("Flow Mode? Gameflow (Ticked) or Debug (Unticked)", g_savedGlobalsClifford.sFlow.isGameflowActive)
		#endif
		#if USE_NRM_DLC
			ADD_WIDGET_BOOL("Flow Mode? Gameflow (Ticked) or Debug (Unticked)", g_savedGlobalsnorman.sFlow.isGameflowActive)
		#endif
		#if not USE_CLF_DLC
		#if not USE_NRM_DLC
			ADD_WIDGET_BOOL("Flow Mode? Gameflow (Ticked) or Debug (Unticked)", g_savedGlobals.sFlow.isGameflowActive)
		#endif
		#endif
		
		ADD_WIDGET_BOOL("Force go on mission.", paramDebugVars.bGoOnMission)
		ADD_WIDGET_BOOL("Force go off mission.", paramDebugVars.bGoOffMission)
		
		CREATE_MISSION_FLOW_INTERNALS_WIDGETS_CUSTOM(paramDebugVars)
		ADD_WIDGET_BOOL("Controller Busy", g_flowUnsaved.bFlowControllerBusy)
        CREATE_MISSION_FLOW_INTERNALS_STRAND_WIDGETS(paramDebugVars)
        CREATE_MISSION_FLOW_INTERNALS_SYNCHRONISATION_WIDGETS()
        CREATE_MISSION_FLOW_INTERNALS_FLAGS_WIDGETS()
        CREATE_MISSION_FLOW_INTERNALS_INTS_WIDGETS()
        CREATE_MISSION_FLOW_INTERNALS_BITSETS_WIDGETS()
    STOP_WIDGET_GROUP()

ENDPROC




// -----------------------------------------------------------------------------------------------------------
//      Create Mission Flow Widget Groups
// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Set up all mission flow widget groupings
// 
// INPUT PARAMS:        paramDebugVars          The struct containing all the debug variables

PROC CREATE_MISSION_FLOW_WIDGETS(FLOW_DEBUG &paramDebugVars)

   	paramDebugVars.missionFlowWidgetGroup = START_WIDGET_GROUP("Mission Flow")
	
        ADD_WIDGET_BOOL("Activate Mission Flow Widgets", paramDebugVars.bShowMissionFlowWidgets)
    
        IF paramDebugVars.bShowMissionFlowWidgets
            // Mission Flow Internals - gives an insight into what is going on inside the mission flow
            CREATE_MISSION_FLOW_INTERNALS_WIDGETS(paramDebugVars)
        
            // Console Log Output
            START_WIDGET_GROUP("Console Log Output")
                ADD_WIDGET_BOOL("Commands Processing On?", g_flowUnsaved.bFlowCommandsConsoleLogOutputOn)
            STOP_WIDGET_GROUP()
        
            // F9 Screen
            START_WIDGET_GROUP("F9 Screen Output")
               	ADD_WIDGET_BOOL("Show F9 Screen? (Used by other scripts)", g_flowUnsaved.bShowMissionFlowDebugScreen)
            STOP_WIDGET_GROUP()
            
            // profiler widget
			#IF MISSION_FLOW_PROFILING
	            #IF SCRIPT_PROFILER_ACTIVE 
					CREATE_SCRIPT_PROFILER_WIDGET() 
				#ENDIF
			#ENDIF
            
            paramDebugVars.bMissionFlowWidgetsCreated = TRUE
        ELSE
            paramDebugVars.bMissionFlowWidgetsCreated = FALSE
        ENDIF
        
    STOP_WIDGET_GROUP()

ENDPROC





// ===========================================================================================================
//      Mission Flow Widgets Maintenance
// ===========================================================================================================

// PURPOSE: Maintain any Mission Flow widgets that need polled each frame
// 
// INPUT PARAMS:        paramDebugVars          The struct containing all the debug variables

PROC MAINTAIN_MISSION_FLOW_WIDGETS(FLOW_DEBUG &paramDebugVars)

    IF paramDebugVars.bShowMissionFlowWidgets
    
        IF NOT paramDebugVars.bMissionFlowWidgetsCreated
            DELETE_WIDGET_GROUP(paramDebugVars.missionFlowWidgetGroup)
            CREATE_MISSION_FLOW_WIDGETS(paramDebugVars)
        ENDIF

        // Force any Read-Only widget variables to be the correct values
        paramDebugVars.boolOnMission     = IS_CURRENTLY_ON_MISSION_TO_TYPE()
        paramDebugVars.boolInMultiplayer = g_bInMultiplayer

        BOOL bitState = FALSE
		
		IF paramDebugVars.bGoOnMission
			REQUEST_MISSION_LAUNCH(paramDebugVars.iCandidateID, MCTID_MUST_LAUNCH, MISSION_TYPE_STORY)
			paramDebugVars.bGoOnMission = FALSE
		ENDIF
		
		IF paramDebugVars.bGoOffMission
			Mission_Over(paramDebugVars.iCandidateID)
			paramDebugVars.bGoOffMission = FALSE
		ENDIF

        //Update strand activation bits.
        INT strandLoop = 0
		#if USE_CLF_DLC
			REPEAT MAX_STRANDS_CLF strandLoop
	            // Compare 'Now' with strand activation bit
	            bitState = IS_BIT_SET(g_savedGlobalsClifford.sFlow.strandSavedVars[strandLoop].savedBitflags, SAVED_BITS_STRAND_ACTIVATED)
	            IF NOT (paramDebugVars.boolStrandActiveWidgetState[strandLoop] = bitState)
	                // ...they differ, so compare NOW with KNOWN
	                IF (paramDebugVars.boolStrandActiveWidgetState[strandLoop] = paramDebugVars.boolStrandActiveKnownState[strandLoop])
	                    // ...they are the same, so the strand activation bit has changed, so update the NOW array
	                    paramDebugVars.boolStrandActiveWidgetState[strandLoop] = bitState
	                ELSE
	                    // ...they differ, so the widget has changed, so update the strand activation bit
	                    bitState = paramDebugVars.boolStrandActiveWidgetState[strandLoop]
	                    IF (bitState = TRUE)
	                        SET_BIT(g_savedGlobalsClifford.sFlow.strandSavedVars[strandLoop].savedBitflags, SAVED_BITS_STRAND_ACTIVATED)
	                    ELSE
	                        CLEAR_BIT(g_savedGlobalsClifford.sFlow.strandSavedVars[strandLoop].savedBitflags, SAVED_BITS_STRAND_ACTIVATED)
	                    ENDIF
	                ENDIF
	            ENDIF
	            
	            //  The strand active bit should now be in sync with its widget representation bool, so copy the
	            //  NOW state into the KNOWN state ready for next frame
	            paramDebugVars.boolStrandActiveKnownState[strandLoop] = paramDebugVars.boolStrandActiveWidgetState[strandLoop]
	        ENDREPEAT 
		#endif
		#if USE_NRM_DLC
			REPEAT MAX_STRANDS_NRM strandLoop
	            // Compare 'Now' with strand activation bit
	            bitState = IS_BIT_SET(g_savedGlobalsnorman.sFlow.strandSavedVars[strandLoop].savedBitflags, SAVED_BITS_STRAND_ACTIVATED)
	            IF NOT (paramDebugVars.boolStrandActiveWidgetState[strandLoop] = bitState)
	                // ...they differ, so compare NOW with KNOWN
	                IF (paramDebugVars.boolStrandActiveWidgetState[strandLoop] = paramDebugVars.boolStrandActiveKnownState[strandLoop])
	                    // ...they are the same, so the strand activation bit has changed, so update the NOW array
	                    paramDebugVars.boolStrandActiveWidgetState[strandLoop] = bitState
	                ELSE
	                    // ...they differ, so the widget has changed, so update the strand activation bit
	                    bitState = paramDebugVars.boolStrandActiveWidgetState[strandLoop]
	                    IF (bitState = TRUE)
	                        SET_BIT(g_savedGlobalsnorman.sFlow.strandSavedVars[strandLoop].savedBitflags, SAVED_BITS_STRAND_ACTIVATED)
	                    ELSE
	                        CLEAR_BIT(g_savedGlobalsnorman.sFlow.strandSavedVars[strandLoop].savedBitflags, SAVED_BITS_STRAND_ACTIVATED)
	                    ENDIF
	                ENDIF
	            ENDIF
	            
	            //  The strand active bit should now be in sync with its widget representation bool, so copy the
	            //  NOW state into the KNOWN state ready for next frame
	            paramDebugVars.boolStrandActiveKnownState[strandLoop] = paramDebugVars.boolStrandActiveWidgetState[strandLoop]
	        ENDREPEAT 
		#endif
		#if not USE_CLF_DLC
		#if not USE_NRM_DLC
			REPEAT MAX_STRANDS strandLoop
	            // Compare 'Now' with strand activation bit
	            bitState = IS_BIT_SET(g_savedGlobals.sFlow.strandSavedVars[strandLoop].savedBitflags, SAVED_BITS_STRAND_ACTIVATED)
	            IF NOT (paramDebugVars.boolStrandActiveWidgetState[strandLoop] = bitState)
	                // ...they differ, so compare NOW with KNOWN
	                IF (paramDebugVars.boolStrandActiveWidgetState[strandLoop] = paramDebugVars.boolStrandActiveKnownState[strandLoop])
	                    // ...they are the same, so the strand activation bit has changed, so update the NOW array
	                    paramDebugVars.boolStrandActiveWidgetState[strandLoop] = bitState
	                ELSE
	                    // ...they differ, so the widget has changed, so update the strand activation bit
	                    bitState = paramDebugVars.boolStrandActiveWidgetState[strandLoop]
	                    IF (bitState = TRUE)
	                        SET_BIT(g_savedGlobals.sFlow.strandSavedVars[strandLoop].savedBitflags, SAVED_BITS_STRAND_ACTIVATED)
	                    ELSE
	                        CLEAR_BIT(g_savedGlobals.sFlow.strandSavedVars[strandLoop].savedBitflags, SAVED_BITS_STRAND_ACTIVATED)
	                    ENDIF
	                ENDIF
	            ENDIF
	            
	            //  The strand active bit should now be in sync with its widget representation bool, so copy the
	            //  NOW state into the KNOWN state ready for next frame
	            paramDebugVars.boolStrandActiveKnownState[strandLoop] = paramDebugVars.boolStrandActiveWidgetState[strandLoop]
	        ENDREPEAT
		#endif  
        #endif
        
    ELSE
    
        IF paramDebugVars.bMissionFlowWidgetsCreated
            DELETE_WIDGET_GROUP(paramDebugVars.missionFlowWidgetGroup)
            CREATE_MISSION_FLOW_WIDGETS(paramDebugVars)
        ENDIF
    
    ENDIF

ENDPROC




