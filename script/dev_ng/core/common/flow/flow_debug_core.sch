USING "commands_debug.sch"
USING "commands_misc.sch"
USING "commands_hud.sch"

#IF IS_DEBUG_BUILD
USING "debug_channels_structs.sch"
USING "flow_debug_GAME.sch"





#IF NOT DEFINED(MISSION_FLOW_PROFILING)
CONST_INT MISSION_FLOW_PROFILING	0
#ENDIF

// Set this to 1 if initaliser debug output is required.
// This outputs a description of every Mission Flow Command as it gets stored in the commands array
#IF NOT DEFINED(MISSION_FLOW_INITIALISER_DEBUG_OUTPUT)
CONST_INT   MISSION_FLOW_INITIALISER_DEBUG_OUTPUT	0
#ENDIF

#IF NOT DEFINED(FLOW_COMMANDS_CONSOLE_LOG_OUTPUT_TIMER_MSEC)
CONST_INT		FLOW_COMMANDS_CONSOLE_LOG_OUTPUT_TIMER_MSEC		300000
#ENDIF


// -----------------------------------------------------------------------------------------------------------
//      Mission Flow Standard Debug Output Strand Name Strings
// -----------------------------------------------------------------------------------------------------------

// Used to calculate how many spaces to add after the strand name to keep it aligned for the console log output
CONST_INT   MAX_STRAND_NAME_DISPLAY_LENGTH      16


// -----------------------------------------------------------------------------------------------------------
//      Mission Flow Standard Debug Output BOOL Strings
// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Return a BOOL as a string for display
//
// INPUT PARAMS:    paramBool               A Bool
// RETURN VALUE:    STRING                  Debug display string for the bool

FUNC STRING GET_BOOL_DISPLAY_STRING_FROM_BOOL(BOOL paramBool)
    IF (paramBool)
        RETURN "True"
    ENDIF
    
    RETURN "False"
ENDFUNC



// -----------------------------------------------------------------------------------------------------------
//      Mission Flow Standard Debug Output Label Strings
// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Return the label as a string based on the label ID
//
// INPUT PARAMS:    paramLabelID            Label ID
// RETURN VALUE:    STRING                  Debug display string for the label
#IF NOT DEFINED(GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID)
FUNC STRING GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(JUMP_LABEL_IDS paramLabelID)
	paramLabelID = paramLabelID
	RETURN ""
ENDFUNC
#ENDIF

// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Return the strand name as a string based on the strand ID
//
// INPUT PARAMS:    paramStrandID           Strand ID
// RETURN VALUE:    STRING                  Debug display string for the strand
#IF NOT DEFINED(GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID)
FUNC STRING GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(STRANDS paramStrandID)
	paramStrandID = paramStrandID
	RETURN ""
ENDFUNC
#ENDIF

// PURPOSE: Return the strand name as a string based on the strand ID
//
// INPUT PARAMS:    paramStrandID           Strand ID
// RETURN VALUE:    STRING                  Debug display string for the strand
#if USE_NRM_DLC
#IF NOT DEFINED(GET_FLOWCHECK_DISPLAY_STRING)
FUNC STRING GET_FLOWCHECK_DISPLAY_STRING(FLOW_CHECK_IDS paramFlowCheck)
	paramFlowCheck = paramFlowCheck
	RETURN ""
ENDFUNC
#ENDIF
#endif

#IF NOT DEFINED(GET_COMMAND_DISPLAY_STRING_FROM_COMMAND_ID_CUSTOM)
FUNC STRING GET_COMMAND_DISPLAY_STRING_FROM_COMMAND_ID_CUSTOM(FLOW_COMMAND_IDS paramCommandID)
	CERRORLN(DEBUG_FLOW_COMMANDS, "Get_Command_Display_String_From_Command_ID: Unknown Command ID - Needs Added")
	paramCommandID = paramCommandID
	RETURN "Unknown"
ENDFUNC
#ENDIF

// -----------------------------------------------------------------------------------------------------------
//      Mission Flow Standard Debug Output Synchronisation Strings
// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Return the sync as a string based on the synchronisationID
//
// INPUT PARAMS:    paramSyncID             Synchronisation ID
// RETURN VALUE:    STRING                  Debug display string for the syncID

#IF NOT DEFINED(GET_SYNCHRONISATION_DISPLAY_STRING_FROM_SYNC_ID)
FUNC STRING GET_SYNCHRONISATION_DISPLAY_STRING_FROM_SYNC_ID(SYNCHRONIZATION_IDS paramSyncID)
	paramSyncID = paramSyncID
	RETURN ""
ENDFUNC
#ENDIF




// -----------------------------------------------------------------------------------------------------------
//      Mission Flow Standard Debug Output Flow Bitset Strings
// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Return the flow bitset as a string based on the FlowBitsetID
//
// INPUT PARAMS:    paramBitsetID           Flow Bitset ID
// RETURN VALUE:    STRING                  Debug display string for the Flow Bitset ID

#IF NOT DEFINED(GET_BITSET_DISPLAY_STRING_FROM_BITSET_ID)
FUNC STRING GET_BITSET_DISPLAY_STRING_FROM_BITSET_ID(FLOW_BITSET_IDS paramBitsetID)
	paramBitsetID = paramBitsetID
	RETURN ""
ENDFUNC
#ENDIF



// -----------------------------------------------------------------------------------------------------------
//      Mission Flow Standard Debug Output Flow Flag Strings
// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Return the flow flag as a string based on the FlowFlagID
//
// INPUT PARAMS:    paramFlagID             Flow Flag ID
// RETURN VALUE:    STRING                  Debug display string for the Flow Flag ID

#IF NOT DEFINED(GET_FLAG_DISPLAY_STRING_FROM_FLAG_ID)
FUNC STRING GET_FLAG_DISPLAY_STRING_FROM_FLAG_ID(FLOW_FLAG_IDS paramFlagID)
	paramFlagID = paramFlagID
	RETURN ""
ENDFUNC
#ENDIF


// -----------------------------------------------------------------------------------------------------------
//      Mission Flow Standard Debug Output Flow Int Strings
// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Return the flow int as a string based on the FlowIntID
//
// INPUT PARAMS:    paramIntID              Flow Int ID
// RETURN VALUE:    STRING                  Debug display string for the Flow Int ID

#IF NOT DEFINED(GET_INT_DISPLAY_STRING_FROM_INT_ID)
FUNC STRING GET_INT_DISPLAY_STRING_FROM_INT_ID(FLOW_INT_IDS paramIntID)
	paramIntID = paramIntID
	RETURN ""
ENDFUNC
#ENDIF


// -----------------------------------------------------------------------------------------------------------
//      Mission Flow Strand Command Strings
// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Return a mission flow command as a string based on the command ID
//
// INPUT PARAMS:    paramCommandID          Command ID
// RETURN VALUE:    STRING                  Debug display string for the command

FUNC STRING GET_COMMAND_DISPLAY_STRING_FROM_COMMAND_ID(FLOW_COMMAND_IDS paramCommandID)

    SWITCH (paramCommandID)
		CASE FLOW_DEBUG_DUMMY
			RETURN "Debug Dummy"
		
		CASE FLOW_ACTIVATE_RANDOMCHAR_MISSION
			RETURN "Activate Random Char Mission"
	
        CASE FLOW_ACTIVATE_STRAND
            RETURN "Activate Strand"
            
        CASE FLOW_AWAIT_ACTIVATION
            RETURN "Await Activation"
        
        CASE FLOW_AWAIT_SYNCHRONISATION
            RETURN "Await Sync"
            
        CASE FLOW_DISPLAY_HELP_TEXT
            RETURN "Display Help Text"
			
		CASE FLOW_CANCEL_HELP_TEXT
            RETURN "Cancel Help Text"
        
        CASE FLOW_DO_BITSET_JUMPS
            RETURN "Do Bitset Jumps"
        
        CASE FLOW_DO_FLAG_JUMPS
            RETURN "Do Flag Jumps"
        
        CASE FLOW_DO_INT_JUMPS
            RETURN "Do Int Jumps"
        
        CASE FLOW_DO_MISSION_AT_BLIP
            RETURN "Do Mission At Blip"
				
        CASE FLOW_DO_MISSION_NOW
            RETURN "Do Mission Now"
			
		 CASE FLOW_IS_MISSION_COMPLETED
            RETURN "Is Mission Completed"
            
        CASE FLOW_DO_STRAND_KILLED_JUMPS
            RETURN "Do Strand Killed Jumps"
			
		CASE FLOW_IS_GAME_TIME_AFTER_HOUR
            RETURN "Is Game Time After Hour"
			
		CASE FLOW_GOTO
            RETURN "Goto"
        
		CASE FLOW_STRAND_GOTO
			RETURN "strand goto"
		
        CASE FLOW_KILL_STRAND
            RETURN "Kill Strand"
        
        CASE FLOW_LABEL
            RETURN "Label"
			
		CASE FLOW_LABEL_LAUNCHER_ONLY
			RETURN "Label Launcher Only"

		CASE FLOW_LAUNCH_SCRIPT_AND_FORGET
            RETURN "Launch Script And Forget"
            
		CASE FLOW_RUN_CODE_ID
			RETURN "Run Code ID"
			
		CASE FLOW_DEBUG_RUN_CODE_ID
			RETURN "Debug Run Code ID"
            
        CASE FLOW_SEND_SYNCHRONISATION
            RETURN "Send Synchronisation"
        
        CASE FLOW_STORE_BITSET_BIT_STATE
            RETURN "Store Bitset Bit State"
        
        CASE FLOW_STORE_FLAG_STATE
            RETURN "Store Flag State"
        
        CASE FLOW_STORE_INT_VALUE
            RETURN "Store Int Value"
            
        CASE FLOW_ENABLE_BLIP_LONG_RANGE
            RETURN "Enable Blip Long-Range"

		CASE FLOW_DISABLE_BLIP_LONG_RANGE
            RETURN "Disable Blip Long-Range"

        CASE FLOW_SET_BUILDING_STATE
            RETURN "Set Building State"
			
        CASE FLOW_SET_GAME_CLOCK
            RETURN "Set Game Clock"
        
		CASE FLOW_SET_WEAPON_LOCK_STATE
            RETURN "Set Weapon Lock State"
			
		CASE FLOW_SET_STATIC_BLIP_STATE
            RETURN "Set Static Blip State"
        
        CASE FLOW_TERMINATE
            RETURN "Terminate"
			
        CASE FLOW_WAIT_IN_GAME_TIME
            RETURN "Wait_In_Game_Time"
        
        CASE FLOW_WAIT_IN_REAL_TIME
            RETURN "Wait_In_Real_Time"
            
        CASE FLOW_WAIT_UNTIL_GAME_HOUR
            RETURN "Wait_Until_Game_Hour"
            
        CASE FLOW_WAIT_UNTIL_GAME_TIME_AND_DATE
            RETURN "Wait_Until_Game_Time_And_Date"
           
		DEFAULT
			RETURN GET_COMMAND_DISPLAY_STRING_FROM_COMMAND_ID_CUSTOM(paramCommandID)
    ENDSWITCH
    
    CERRORLN(DEBUG_FLOW_COMMANDS, "Get_Command_Display_String_From_Command_ID: Unknown Command ID - Needs Added")
    RETURN "Unknown"
ENDFUNC





// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Return a string containing the number of spaces required
//
// INPUT PARAMS:    paramSpacesRequired     The number of spaces required
// RETURN VALUE:    STRING                  Debug display string for various number of spaces

FUNC STRING GET_REQUIRED_SPACES_AS_A_DISPLAY_STRING(INT paramSpacesRequired)

    SWITCH (paramSpacesRequired)
        CASE 0
            RETURN ""
            
        CASE 1
            RETURN " "
            
        CASE 2
            RETURN "  "
            
        CASE 3
            RETURN "   "
            
        CASE 4
            RETURN "    "
            
        CASE 5
            RETURN "     "
            
        CASE 6
            RETURN "      "
            
        CASE 7
            RETURN "       "
            
        CASE 8
            RETURN "        "
            
        CASE 9
            RETURN "         "
            
        CASE 10
            RETURN "          "
            
        CASE 11
            RETURN "           "
            
        CASE 12
            RETURN "            "
            
        CASE 13
            RETURN "             "
            
        CASE 14
            RETURN "              "
            
        CASE 15
            RETURN "               "
            
        DEFAULT
            RETURN "<need more spaces>"
    ENDSWITCH
    
    CERRORLN(DEBUG_FLOW, "GET_REQUIRED_SPACES_AS_A_DISPLAY_STRING: Spaces Required Is Less Than 0")
    RETURN ""

ENDFUNC





// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Get the strand ID associated with a given strand display string.
//
// INPUT PARAMS:    paramDisplayString      A strand's display string.
// RETURN VALUE:    STRANDS              The strand ID enum that is associated with the display string. 
//                                          Returns STRAND_NONE if the string did not match any strand IDs.

FUNC STRANDS GET_STRAND_ID_FROM_DISPLAY_STRING(STRING paramDisplayString)

    IF NOT IS_STRING_NULL(paramDisplayString)
        STRANDS strandIndex
		#if USE_CLF_DLC
			REPEAT MAX_STRANDS_CLF strandIndex
	            IF ARE_STRINGS_EQUAL(paramDisplayString,GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(strandIndex))
	                RETURN strandIndex
	            ENDIF
	        ENDREPEAT
		#endif	
		#if USE_NRM_DLC
			REPEAT MAX_STRANDS_NRM strandIndex
	            IF ARE_STRINGS_EQUAL(paramDisplayString,GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(strandIndex))
	                RETURN strandIndex
	            ENDIF
	        ENDREPEAT
		#endif	
		#if not USE_CLF_DLC
		#if not USE_NRM_DLC
			REPEAT MAX_STRANDS strandIndex
	            IF ARE_STRINGS_EQUAL(paramDisplayString,GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(strandIndex))
	                RETURN strandIndex
	            ENDIF
	        ENDREPEAT
		#endif  
		#endif
    ENDIF
    
    RETURN STRAND_NONE
    
ENDFUNC


// ===========================================================================================================
//      Mission Flow Standard Debug Output Messages
// ===========================================================================================================

// PURPOSE: Output a Mission Flow Assert Header Message
// 
//PROC DISPLAY_MISSION_FLOW_ASSERT_MESSAGE_HEADER()
//    PRINTLN()
//    PRINTLN("---MISSION FLOW ASSERT-----------------------------------------")
//ENDPROC
//
//
//// -----------------------------------------------------------------------------------------------------------
//
//// PURPOSE: Output a Mission Flow Assert Footer Message
//// 
//PROC DISPLAY_MISSION_FLOW_MESSAGE_FOOTER()
//
//    PRINTLN("---------------------------------------------------------------")
//    PRINTLN()
//
//ENDPROC
//
//
//// -----------------------------------------------------------------------------------------------------------
//
//// PURPOSE: Output a Mission Flow Error Message containing the Strand ID string
////
//// INPUT PARAMS:    paramErrorText          The body of the error message
////                  paramStrandID           The Strand ID to be converted to a string and added to the message
//
//PROC DISPLAY_MISSION_FLOW_ERROR_MESSAGE_WITH_STRAND(STRING paramErrorText, STRANDS paramStrandID)
//    
//ENDPROC
//
//
//// -----------------------------------------------------------------------------------------------------------
//
//// PURPOSE: Output a Mission Flow Error Message containing an additional string
////
//// INPUT PARAMS:    paramErrorText          The body of the error message
////                  paramString             Additional string added to the message
//
//PROC DISPLAY_MISSION_FLOW_ERROR_MESSAGE_WITH_STRING(STRING paramErrorText, STRING paramString)
//    CERRORLN(DEBUG_FLOW, "...", paramErrorText, " (", paramString, ")")
//ENDPROC
//
//
//// -----------------------------------------------------------------------------------------------------------
//
//// PURPOSE: Output a Mission Flow Error Message
////
//// INPUT PARAMS:    paramErrorText          The body of the error message
//
//PROC DISPLAY_MISSION_FLOW_ERROR_MESSAGE(STRING paramErrorText)
//    CERRORLN(DEBUG_FLOW, "...", paramErrorText)
//ENDPROC
//
//
//// -----------------------------------------------------------------------------------------------------------
//
//// PURPOSE: Output a Mission Flow Assert Message containing the Strand ID string
////
//// INPUT PARAMS:    paramAssertText         The body of the error message
////                  paramStrandID           The Strand ID to be converted to a string and added to the message
//
//PROC DISPLAY_MISSION_FLOW_ASSERT_MESSAGE_WITH_STRAND(STRING paramAssertText, STRANDS paramStrandID)
//
//    DISPLAY_MISSION_FLOW_ASSERT_MESSAGE_HEADER()
//    DISPLAY_MISSION_FLOW_ERROR_MESSAGE_WITH_STRAND(paramAssertText, paramStrandID)
//    DISPLAY_MISSION_FLOW_MESSAGE_FOOTER()
//
//ENDPROC
//
//
//// -----------------------------------------------------------------------------------------------------------
//
//// PURPOSE: Output a Mission Flow Assert Message containing an additional string
////
//// INPUT PARAMS:    paramAssertText         The body of the error message
////                  paramString             Additional string added to the message
//
//PROC DISPLAY_MISSION_FLOW_ASSERT_MESSAGE_WITH_STRING(STRING paramAssertText, STRING paramString)
//
//    DISPLAY_MISSION_FLOW_ASSERT_MESSAGE_HEADER()
//    DISPLAY_MISSION_FLOW_ERROR_MESSAGE_WITH_STRING(paramAssertText, paramString)
//    DISPLAY_MISSION_FLOW_MESSAGE_FOOTER()
//
//ENDPROC
//
//
//// -----------------------------------------------------------------------------------------------------------
//
//// PURPOSE: Output a Mission Flow Assert Message
////
//// INPUT PARAMS:    paramAssertText         The body of the error message
//
//PROC DISPLAY_MISSION_FLOW_ASSERT_MESSAGE(STRING paramAssertText)
//
//    DISPLAY_MISSION_FLOW_ASSERT_MESSAGE_HEADER()
//    DISPLAY_MISSION_FLOW_ERROR_MESSAGE(paramAssertText)
//    DISPLAY_MISSION_FLOW_MESSAGE_FOOTER()
//
//ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Output a Mission Flow Fatal Assert Message containing the Strand ID string - endless loop
//
// INPUT PARAMS:    paramAssertText         The body of the error message
//                  paramStrandID           The Strand ID to be converted to a string and added to the message

PROC DISPLAY_MISSION_FLOW_FATAL_ASSERT_MESSAGE_WITH_STRAND(STRING paramAssertText, STRANDS paramStrandID)

    WHILE (TRUE)
        CERRORLN(DEBUG_FLOW, "...", paramAssertText, " (STRAND = ", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(paramStrandID), ")")
        WAIT(0)
    ENDWHILE

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Output a Mission Flow Fatal Assert Message containing an additional string - endless loop
//
// INPUT PARAMS:    paramAssertText         The body of the error message
//                  paramString             Additional string added to the message

PROC DISPLAY_MISSION_FLOW_FATAL_ASSERT_MESSAGE_WITH_STRING(STRING paramAssertText, STRING paramString)

    WHILE (TRUE)
        CERRORLN(DEBUG_FLOW, "...", paramAssertText, " (", paramString, ")")
        WAIT(0)
    ENDWHILE

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Output a Mission Flow Fatal Assert Message - endless loop
//
// INPUT PARAMS:    paramAssertText         The body of the error message

PROC DISPLAY_MISSION_FLOW_FATAL_ASSERT_MESSAGE(STRING paramAssertText)

    WHILE (TRUE)
        CERRORLN(DEBUG_FLOW, "...", paramAssertText)
        WAIT(0)
    ENDWHILE

ENDPROC




FUNC BOOL IS_FLOW_CONSOLE_LOG_READY_TO_PRINT()

    IF NOT (g_flowUnsaved.bOutputFlowCommandsToConsoleLogNow)
        RETURN FALSE
    ENDIF
	
 #if USE_CLF_DLC
     IF NOT (g_savedGlobalsClifford.sFlow.isGameflowActive)
        RETURN FALSE
    ENDIF    
 #endif
 #if USE_NRM_DLC
     IF NOT (g_savedGlobalsnorman.sFlow.isGameflowActive)
        RETURN FALSE
    ENDIF    
 #endif
 
 #if not USE_CLF_DLC
 #if not USE_NRM_DLC
    IF NOT (g_savedGlobals.sFlow.isGameflowActive)
        RETURN FALSE
    ENDIF     
 #endif
 #endif
 
    RETURN TRUE
ENDFUNC



// ===========================================================================================================
//      Debug Console Log Output Routines
// ===========================================================================================================

// PURPOSE: Check if debug output should be output next frame. If timer is 0 or less it will be output.
//
// RETURN VALUE:        BOOL            TRUE if it should be output, FALSE if not
//
// NOTES:   The intention here is to prevent spamming the console log every frame by periodically
//              issuing the output if nothing has changed, but to issue the output in the next frame
//              if something changes in this frame.
//          To force an output next frame, set the timer to 0.

PROC  MAINTAIN_FLOW_CONSOLE_LOG_OUTPUT_REQUIRED_NEXT_FRAME()

    // Is console log output switched on?
    IF NOT (g_flowUnsaved.bFlowCommandsConsoleLogOutputOn)
        // ...no, so set the timer to 0 to ensure it gets output on the first frame after it is switched on
        g_flowUnsaved.iFlowCommandsConsoleLogOutputTimer = 0
        g_flowUnsaved.bOutputFlowCommandsToConsoleLogNow = FALSE
        
        EXIT
    ENDIF
    
    // Console Log Output is switched on
    // Has the timer expired?
    INT gameTimer = GET_GAME_TIMER()
    
    IF (g_flowUnsaved.iFlowCommandsConsoleLogOutputTimer < gameTimer)
        // ...timer has expired, output next frame and reset the game timer
        g_flowUnsaved.bOutputFlowCommandsToConsoleLogNow = TRUE
        g_flowUnsaved.iFlowCommandsConsoleLogOutputTimer = gameTimer + FLOW_COMMANDS_CONSOLE_LOG_OUTPUT_TIMER_msec
        
        EXIT
    ENDIF
    
    // Timer has not expired so output net required next frame
    g_flowUnsaved.bOutputFlowCommandsToConsoleLogNow = FALSE

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Forces the Flow Commands Processing Console Log output to be output next frame
// 
PROC FORCE_FLOW_CONSOLE_LOG_OUTPUT_NEXT_FRAME()

    g_flowUnsaved.iFlowCommandsConsoleLogOutputTimer = 0

ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════╡ Utility Commands ╞═════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

// PURPOSE: Strips off any .sc on the end of a mission file name.
//
// INPUT PARAMS:	paramFilename		The mission filename.
// RETURN VALUE:	STRING				The filename with any .sc removed.
//
FUNC STRING REMOVE_SC_FROM_MISSION_FILENAME(STRING paramFilename)

	STRING standardisedString = paramFilename
	
	IF (IS_STRING_NULL(paramFilename))
		RETURN ""
	ENDIF
	
	// Check if the string has .sc on the end, if it has then strip it out
	INT stringLength = GET_LENGTH_OF_LITERAL_STRING(paramFilename)
	
	IF NOT (stringLength > 3)
		RETURN standardisedString
	ENDIF
	
	// ...extract the last three characters of the string
	INT endPoint	= stringLength
	INT startPoint	= stringLength - 3
	
	STRING lastThreeChars = GET_STRING_FROM_STRING(paramFilename, startPoint, endPoint)

	IF NOT (ARE_STRINGS_EQUAL(lastThreeChars, ".sc"))
		RETURN standardisedString
	ENDIF
	
	// The last three characters are .sc which are not needed, so strip them off
	standardisedString = GET_FIRST_N_CHARACTERS_OF_LITERAL_STRING(paramFilename, startPoint)

	RETURN standardisedString

ENDFUNC
	

// PURPOSE:	Checks the consistency of the commands array data - basic checks
// 
PROC CHECK_MISSION_FLOW_COMMANDS_ARRAY()
	
	STRANDS theStrandID = STRAND_NONE
#if USE_CLF_DLC
	REPEAT MAX_STRANDS_CLF theStrandID
		IF (g_flowUnsaved.flowCommandsIndex[theStrandID].lower = ILLEGAL_ARRAY_POSITION)
			CERRORLN(DEBUG_FLOW, "Check_Mission_Flow_Commands_Array: Lower bounds for a strand's command array position is illegal", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID( theStrandID))
		ENDIF
		
		IF (g_flowUnsaved.flowCommandsIndex[theStrandID].upper = ILLEGAL_ARRAY_POSITION)
			CERRORLN(DEBUG_FLOW, "Check_Mission_Flow_Commands_Array: Upper bounds for a strand's command array position is illegal", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID( theStrandID))
		ENDIF
	ENDREPEAT
#endif
#if USE_NRM_DLC
	REPEAT MAX_STRANDS_NRM theStrandID
		IF (g_flowUnsaved.flowCommandsIndex[theStrandID].lower = ILLEGAL_ARRAY_POSITION)
			CERRORLN(DEBUG_FLOW, "Check_Mission_Flow_Commands_Array: Lower bounds for a strand's command array position is illegal", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID( theStrandID))
		ENDIF
		
		IF (g_flowUnsaved.flowCommandsIndex[theStrandID].upper = ILLEGAL_ARRAY_POSITION)
			CERRORLN(DEBUG_FLOW, "Check_Mission_Flow_Commands_Array: Upper bounds for a strand's command array position is illegal", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID( theStrandID))
		ENDIF
	ENDREPEAT
#endif

#if not USE_CLF_DLC
#if not USE_NRM_DLC
	REPEAT MAX_STRANDS theStrandID
		IF (g_flowUnsaved.flowCommandsIndex[theStrandID].lower = ILLEGAL_ARRAY_POSITION)
			CERRORLN(DEBUG_FLOW, "Check_Mission_Flow_Commands_Array: Lower bounds for a strand's command array position is illegal", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID( theStrandID))
		ENDIF
		
		IF (g_flowUnsaved.flowCommandsIndex[theStrandID].upper = ILLEGAL_ARRAY_POSITION)
			CERRORLN(DEBUG_FLOW, "Check_Mission_Flow_Commands_Array: Upper bounds for a strand's command array position is illegal", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID( theStrandID))
		ENDIF
	ENDREPEAT
#endif
#endif

ENDPROC

// ===========================================================================================================
//		Mission Flow Globals Debug Output
// ===========================================================================================================
// PURPOSE:	Outputs all the missionflow array usage values
// 
PROC OUTPUT_MISSION_FLOW_ARRAY_USAGE()
	CPRINTLN(DEBUG_FLOW, "---------------------------")
	
	// Display Commands Array Usage (always)
	CPRINTLN(DEBUG_FLOW, "Commands Array Slots Usage: ", g_flowUnsaved.iNumStoredFlowCommands, " (from ", MAX_MISSION_FLOW_COMMANDS, ")   [STORAGE SIZE EACH: ", SIZE_OF(FLOW_COMMANDS), "  (Totals: Used = ", SIZE_OF(FLOW_COMMANDS) * g_flowUnsaved.iNumStoredFlowCommands, ", Allocated = ",(SIZE_OF(FLOW_COMMANDS)) * MAX_MISSION_FLOW_COMMANDS, ")]")
	
	// Display Core Variable Slots usage
	CPRINTLN(DEBUG_FLOW, "Core Variable Slots Usage: ", g_flowUnsaved.iNumStoredFlowVariables[FST_CORE], " (from ", MAX_CORE_VARIABLE_SLOTS, ")   [STORAGE SIZE EACH: ", SIZE_OF(CORE_COMMAND_STORAGE), "  (Totals: Used = ", SIZE_OF(CORE_COMMAND_STORAGE) * g_flowUnsaved.iNumStoredFlowVariables[FST_CORE], ", Allocated = ", (SIZE_OF(CORE_COMMAND_STORAGE)) * MAX_CORE_VARIABLE_SLOTS, ")]")
	
	// Display Core Small Variable Slots usage
	CPRINTLN(DEBUG_FLOW, "Core Small Variable Slots Usage: ", g_flowUnsaved.iNumStoredFlowVariables[FST_CORE_SMALL], " (from ", MAX_CORE_SMALL_VARIABLE_SLOTS, ")   [STORAGE SIZE EACH: ", SIZE_OF(CORE_SMALL_COMMAND_STORAGE), "  (Totals: Used = ", SIZE_OF(CORE_SMALL_COMMAND_STORAGE) * g_flowUnsaved.iNumStoredFlowVariables[FST_CORE_SMALL], ", Allocated = ", (SIZE_OF(CORE_SMALL_COMMAND_STORAGE)) * MAX_CORE_SMALL_VARIABLE_SLOTS, ")]")
	
	// Display Text Variable Slots usage
	CPRINTLN(DEBUG_FLOW, "Text Variable Slots Usage: ", g_flowUnsaved.iNumStoredFlowVariables[FST_TEXT], " (from ", MAX_TEXT_VARIABLE_SLOTS, ")   [STORAGE SIZE EACH: ", SIZE_OF(TEXT_COMMAND_STORAGE), "  (Totals: Used = ", SIZE_OF(TEXT_COMMAND_STORAGE) * g_flowUnsaved.iNumStoredFlowVariables[FST_TEXT], ", Allocated = ", (SIZE_OF(TEXT_COMMAND_STORAGE)) * MAX_TEXT_VARIABLE_SLOTS, ")]")
	
	// Display Text Large Variable Slots usage
	CPRINTLN(DEBUG_FLOW, "Text Large Variable Slots Usage: ", g_flowUnsaved.iNumStoredFlowVariables[FST_TEXT_LARGE], " (from ", MAX_TEXT_LARGE_VARIABLE_SLOTS, ")   [STORAGE SIZE EACH: ", SIZE_OF(TEXT_LARGE_COMMAND_STORAGE), "  (Totals: Used = ", SIZE_OF(TEXT_LARGE_COMMAND_STORAGE) * g_flowUnsaved.iNumStoredFlowVariables[FST_TEXT_LARGE], ", Allocated = ", (SIZE_OF(TEXT_LARGE_COMMAND_STORAGE)) * MAX_TEXT_LARGE_VARIABLE_SLOTS, ")]")
	
	// Display Script Variable Slots usage
	CPRINTLN(DEBUG_FLOW, "Script Variable Slots Usage: ", g_flowUnsaved.iNumStoredFlowVariables[FST_SCRIPT], " (from ", MAX_SCRIPT_VARIABLE_SLOTS, ")   [STORAGE SIZE EACH: ", SIZE_OF(SCRIPT_COMMAND_STORAGE), "  (Totals: Used = ", SIZE_OF(SCRIPT_COMMAND_STORAGE) * g_flowUnsaved.iNumStoredFlowVariables[FST_SCRIPT], ", Allocated = ", SIZE_OF(SCRIPT_COMMAND_STORAGE) * MAX_SCRIPT_VARIABLE_SLOTS, ")]")
	
	// Display Miscellaneous Variable Slots usage
	CPRINTLN(DEBUG_FLOW, "Miscellaneous Variable Slots Usage: ", g_flowUnsaved.iNumStoredFlowVariables[FST_MISCELLANEOUS], " (from ", MAX_MISCELLANEOUS_VARIABLE_SLOTS, ")   [STORAGE SIZE EACH: ", SIZE_OF(MISCELLANEOUS_COMMAND_STORAGE), "  (Totals: Used = ", SIZE_OF(MISCELLANEOUS_COMMAND_STORAGE) * g_flowUnsaved.iNumStoredFlowVariables[FST_MISCELLANEOUS], ", Allocated = ", SIZE_OF(MISCELLANEOUS_COMMAND_STORAGE) * MAX_MISCELLANEOUS_VARIABLE_SLOTS, ")]")
	
	#IF DEFINED(COMMS_COMMAND_STORAGE)
	// Display Phonecall Variable Slots usage
	CPRINTLN(DEBUG_FLOW, "Comms Variable Slots Usage: ", g_flowUnsaved.iNumStoredFlowVariables[FST_COMMS], " (from ", MAX_COMMS_VARIABLE_SLOTS, ")   [STORAGE SIZE EACH: ", SIZE_OF(COMMS_COMMAND_STORAGE), "  (Totals: Used = ", SIZE_OF(COMMS_COMMAND_STORAGE) * g_flowUnsaved.iNumStoredFlowVariables[FST_COMMS], ", Allocated = ", SIZE_OF(COMMS_COMMAND_STORAGE) * MAX_COMMS_VARIABLE_SLOTS, ")]")
	#ENDIF
	#IF DEFINED(COMMS_LARGE_COMMAND_STORAGE)
	// Display Phonecall Variable Slots usage
	CPRINTLN(DEBUG_FLOW, "Comms Large Variable Slots Usage: ", g_flowUnsaved.iNumStoredFlowVariables[FST_COMMS_LARGE], " (from ", MAX_COMMS_LARGE_VARIABLE_SLOTS, ")   [STORAGE SIZE EACH: ", SIZE_OF(COMMS_LARGE_COMMAND_STORAGE), "  (Totals: Used = ", SIZE_OF(COMMS_LARGE_COMMAND_STORAGE) * g_flowUnsaved.iNumStoredFlowVariables[FST_COMMS_LARGE], ", Allocated = ", SIZE_OF(COMMS_LARGE_COMMAND_STORAGE) * MAX_COMMS_LARGE_VARIABLE_SLOTS, ")]")
	#ENDIF
	#IF DEFINED(PHONECALL_COMMAND_STORAGE)
	// Display Phonecall Variable Slots usage
	CPRINTLN(DEBUG_FLOW, "Phonecall Variable Slots Usage: ", g_flowUnsaved.iNumStoredFlowVariables[FST_PHONECALL], " (from ", MAX_PHONECALL_VARIABLE_SLOTS, ")   [STORAGE SIZE EACH: ", SIZE_OF(PHONECALL_COMMAND_STORAGE), "  (Totals: Used = ", SIZE_OF(PHONECALL_COMMAND_STORAGE) * g_flowUnsaved.iNumStoredFlowVariables[FST_PHONECALL], ", Allocated = ", SIZE_OF(PHONECALL_COMMAND_STORAGE) * MAX_PHONECALL_VARIABLE_SLOTS, ")]")
	#ENDIF
	
	// Display Debug Variable Slots usage
	CPRINTLN(DEBUG_FLOW, "Debug Variable Slots Usage: ", g_flowUnsaved.iNumStoredFlowVariables[FST_DEBUG], " (from ", MAX_DEBUG_VARIABLE_SLOTS, ")   [STORAGE SIZE EACH: ", SIZE_OF(DEBUG_COMMAND_STORAGE), "  (Totals: Used = ", SIZE_OF(DEBUG_COMMAND_STORAGE) * g_flowUnsaved.iNumStoredFlowVariables[FST_DEBUG], ", Allocated = ", SIZE_OF(DEBUG_COMMAND_STORAGE) * MAX_DEBUG_VARIABLE_SLOTS, ")]")
	
	CPRINTLN(DEBUG_FLOW, "---------------------------")
ENDPROC


FUNC INT DEBUG_GET_STRAND_INDEX_FOR_COMMAND_INDEX(INT iCommandIndex)
	INT iStrandIndex
#if USE_CLF_DLC
	REPEAT MAX_STRANDS_CLF iStrandIndex
		IF iCommandIndex >= g_flowUnsaved.flowCommandsIndex[iStrandIndex].lower
		AND iCommandIndex <= g_flowUnsaved.flowCommandsIndex[iStrandIndex].upper
			RETURN iStrandIndex
		ENDIF
	ENDREPEAT
#endif
#if USE_NRM_DLC
	REPEAT MAX_STRANDS_NRM iStrandIndex
		IF iCommandIndex >= g_flowUnsaved.flowCommandsIndex[iStrandIndex].lower
		AND iCommandIndex <= g_flowUnsaved.flowCommandsIndex[iStrandIndex].upper
			RETURN iStrandIndex
		ENDIF
	ENDREPEAT
#endif

#if not USE_CLF_DLC
#if not USE_NRM_DLC
	REPEAT MAX_STRANDS iStrandIndex
		IF iCommandIndex >= g_flowUnsaved.flowCommandsIndex[iStrandIndex].lower
		AND iCommandIndex <= g_flowUnsaved.flowCommandsIndex[iStrandIndex].upper
			RETURN iStrandIndex
		ENDIF
	ENDREPEAT
#endif
#endif

	SCRIPT_ASSERT("DEBUG_GET_STRAND_INDEX_FOR_COMMAND_INDEX: Couldn't find valid strand index for command index.")
	RETURN -1
ENDFUNC


PROC PRINT_FLOW_COMMAND_INDEX_DIRECT(INT iCommandIndex)
	CPRINTLN(DEBUG_FLOW, "--------------------------------")
	CPRINTLN(DEBUG_FLOW, "INDEX-DIRECT:", GET_COMMAND_DISPLAY_STRING_FROM_COMMAND_ID(g_flowUnsaved.flowCommands[iCommandIndex].command), ":", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(INT_TO_ENUM(STRANDS, DEBUG_GET_STRAND_INDEX_FOR_COMMAND_INDEX(iCommandIndex))))
	CPRINTLN(DEBUG_FLOW, g_flowUnsaved.flowCommands[iCommandIndex].index)
	CPRINTLN(DEBUG_FLOW, "--------------------------------")
ENDPROC


PROC PRINT_FLOW_COMMAND_CORE(INT iCommandIndex)
	CPRINTLN(DEBUG_FLOW, "--------------------------------")
	CPRINTLN(DEBUG_FLOW, "CORE:", GET_COMMAND_DISPLAY_STRING_FROM_COMMAND_ID(g_flowUnsaved.flowCommands[iCommandIndex].command), ":", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(INT_TO_ENUM(STRANDS, DEBUG_GET_STRAND_INDEX_FOR_COMMAND_INDEX(iCommandIndex))))
	CPRINTLN(DEBUG_FLOW, g_flowUnsaved.coreVars[g_flowUnsaved.flowCommands[iCommandIndex].index].iValue1)
	CPRINTLN(DEBUG_FLOW, g_flowUnsaved.coreVars[g_flowUnsaved.flowCommands[iCommandIndex].index].iValue2)
	CPRINTLN(DEBUG_FLOW, g_flowUnsaved.coreVars[g_flowUnsaved.flowCommands[iCommandIndex].index].iValue3)
	CPRINTLN(DEBUG_FLOW, g_flowUnsaved.coreVars[g_flowUnsaved.flowCommands[iCommandIndex].index].iValue4)
	CPRINTLN(DEBUG_FLOW, "--------------------------------")
ENDPROC


PROC PRINT_FLOW_COMMAND_CORE_SMALL(INT iCommandIndex)
	CPRINTLN(DEBUG_FLOW, "--------------------------------")
	CPRINTLN(DEBUG_FLOW, "CORE-SMALL:", GET_COMMAND_DISPLAY_STRING_FROM_COMMAND_ID(g_flowUnsaved.flowCommands[iCommandIndex].command), ":", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(INT_TO_ENUM(STRANDS, DEBUG_GET_STRAND_INDEX_FOR_COMMAND_INDEX(iCommandIndex))))
	CPRINTLN(DEBUG_FLOW, g_flowUnsaved.coreSmallVars[g_flowUnsaved.flowCommands[iCommandIndex].index].iValue1)
	CPRINTLN(DEBUG_FLOW, g_flowUnsaved.coreSmallVars[g_flowUnsaved.flowCommands[iCommandIndex].index].iValue2)
	CPRINTLN(DEBUG_FLOW, "--------------------------------")
ENDPROC


PROC PRINT_FLOW_COMMAND_TEXT(INT iCommandIndex)
	CPRINTLN(DEBUG_FLOW, "--------------------------------")
	CPRINTLN(DEBUG_FLOW, "TEXT:", GET_COMMAND_DISPLAY_STRING_FROM_COMMAND_ID(g_flowUnsaved.flowCommands[iCommandIndex].command), ":", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(INT_TO_ENUM(STRANDS, DEBUG_GET_STRAND_INDEX_FOR_COMMAND_INDEX(iCommandIndex))))
	CPRINTLN(DEBUG_FLOW, g_flowUnsaved.textVars[g_flowUnsaved.flowCommands[iCommandIndex].index].txtValue)
	CPRINTLN(DEBUG_FLOW, "--------------------------------")
ENDPROC


PROC PRINT_FLOW_COMMAND_TEXT_LARGE(INT iCommandIndex)
	CPRINTLN(DEBUG_FLOW, "--------------------------------")
	CPRINTLN(DEBUG_FLOW, "TEXT-LARGE:", GET_COMMAND_DISPLAY_STRING_FROM_COMMAND_ID(g_flowUnsaved.flowCommands[iCommandIndex].command), ":", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(INT_TO_ENUM(STRANDS, DEBUG_GET_STRAND_INDEX_FOR_COMMAND_INDEX(iCommandIndex))))
	CPRINTLN(DEBUG_FLOW, g_flowUnsaved.textLargeVars[g_flowUnsaved.flowCommands[iCommandIndex].index].txtValue)
	CPRINTLN(DEBUG_FLOW, g_flowUnsaved.textLargeVars[g_flowUnsaved.flowCommands[iCommandIndex].index].iValue1)
	CPRINTLN(DEBUG_FLOW, g_flowUnsaved.textLargeVars[g_flowUnsaved.flowCommands[iCommandIndex].index].iValue2)
	CPRINTLN(DEBUG_FLOW, g_flowUnsaved.textLargeVars[g_flowUnsaved.flowCommands[iCommandIndex].index].iValue3)
	CPRINTLN(DEBUG_FLOW, g_flowUnsaved.textLargeVars[g_flowUnsaved.flowCommands[iCommandIndex].index].iValue4)
	CPRINTLN(DEBUG_FLOW, "--------------------------------")
ENDPROC


PROC PRINT_FLOW_COMMAND_DEBUG(INT iCommandIndex)
	CPRINTLN(DEBUG_FLOW, "--------------------------------")
	CPRINTLN(DEBUG_FLOW, "DEBUG:", GET_COMMAND_DISPLAY_STRING_FROM_COMMAND_ID(g_flowUnsaved.flowCommands[iCommandIndex].command),":", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(INT_TO_ENUM(STRANDS, DEBUG_GET_STRAND_INDEX_FOR_COMMAND_INDEX(iCommandIndex))))
	CPRINTLN(DEBUG_FLOW, g_flowUnsaved.debugVars[g_flowUnsaved.flowCommands[iCommandIndex].index].anInt1)
	CPRINTLN(DEBUG_FLOW, g_flowUnsaved.debugVars[g_flowUnsaved.flowCommands[iCommandIndex].index].anInt2)
	CPRINTLN(DEBUG_FLOW, "--------------------------------")
ENDPROC


PROC PRINT_FLOW_COMMAND_COMMS(INT iCommandIndex)
	CPRINTLN(DEBUG_FLOW, "--------------------------------")
	CPRINTLN(DEBUG_FLOW, "COMMS:", GET_COMMAND_DISPLAY_STRING_FROM_COMMAND_ID(g_flowUnsaved.flowCommands[iCommandIndex].command), ":", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(INT_TO_ENUM(STRANDS, DEBUG_GET_STRAND_INDEX_FOR_COMMAND_INDEX(iCommandIndex))))
	CPRINTLN(DEBUG_FLOW, g_flowUnsaved.commsVars[g_flowUnsaved.flowCommands[iCommandIndex].index].iValue1)
	CPRINTLN(DEBUG_FLOW, g_flowUnsaved.commsVars[g_flowUnsaved.flowCommands[iCommandIndex].index].iValue2)
	CPRINTLN(DEBUG_FLOW, g_flowUnsaved.commsVars[g_flowUnsaved.flowCommands[iCommandIndex].index].iValue3)
	CPRINTLN(DEBUG_FLOW, g_flowUnsaved.commsVars[g_flowUnsaved.flowCommands[iCommandIndex].index].iValue4)
	CPRINTLN(DEBUG_FLOW, g_flowUnsaved.commsVars[g_flowUnsaved.flowCommands[iCommandIndex].index].iValue5)
	CPRINTLN(DEBUG_FLOW, g_flowUnsaved.commsVars[g_flowUnsaved.flowCommands[iCommandIndex].index].iValue6)
	CPRINTLN(DEBUG_FLOW, g_flowUnsaved.commsVars[g_flowUnsaved.flowCommands[iCommandIndex].index].iValue7)
	CPRINTLN(DEBUG_FLOW, g_flowUnsaved.commsVars[g_flowUnsaved.flowCommands[iCommandIndex].index].iValue8)
	CPRINTLN(DEBUG_FLOW, g_flowUnsaved.commsVars[g_flowUnsaved.flowCommands[iCommandIndex].index].eVectorID)
	CPRINTLN(DEBUG_FLOW, g_flowUnsaved.commsVars[g_flowUnsaved.flowCommands[iCommandIndex].index].eCodeID)
	CPRINTLN(DEBUG_FLOW, g_flowUnsaved.commsVars[g_flowUnsaved.flowCommands[iCommandIndex].index].eCheckID)
	CPRINTLN(DEBUG_FLOW, "--------------------------------")
ENDPROC


PROC PRINT_FLOW_COMMAND_COMMS_LARGE(INT iCommandIndex)
	CPRINTLN(DEBUG_FLOW, "--------------------------------")
	CPRINTLN(DEBUG_FLOW, "COMMS-LARGE:", GET_COMMAND_DISPLAY_STRING_FROM_COMMAND_ID(g_flowUnsaved.flowCommands[iCommandIndex].command), ":", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(INT_TO_ENUM(STRANDS, DEBUG_GET_STRAND_INDEX_FOR_COMMAND_INDEX(iCommandIndex))))
	CPRINTLN(DEBUG_FLOW, g_flowUnsaved.commsLargeVars[g_flowUnsaved.flowCommands[iCommandIndex].index].iValue1)
	CPRINTLN(DEBUG_FLOW, g_flowUnsaved.commsLargeVars[g_flowUnsaved.flowCommands[iCommandIndex].index].iValue2)
	CPRINTLN(DEBUG_FLOW, g_flowUnsaved.commsLargeVars[g_flowUnsaved.flowCommands[iCommandIndex].index].iValue3)
	CPRINTLN(DEBUG_FLOW, g_flowUnsaved.commsLargeVars[g_flowUnsaved.flowCommands[iCommandIndex].index].iValue4)
	CPRINTLN(DEBUG_FLOW, g_flowUnsaved.commsLargeVars[g_flowUnsaved.flowCommands[iCommandIndex].index].iValue5)
	CPRINTLN(DEBUG_FLOW, g_flowUnsaved.commsLargeVars[g_flowUnsaved.flowCommands[iCommandIndex].index].iValue6)
	CPRINTLN(DEBUG_FLOW, g_flowUnsaved.commsLargeVars[g_flowUnsaved.flowCommands[iCommandIndex].index].iValue7)
	CPRINTLN(DEBUG_FLOW, g_flowUnsaved.commsLargeVars[g_flowUnsaved.flowCommands[iCommandIndex].index].iValue8)
	CPRINTLN(DEBUG_FLOW, g_flowUnsaved.commsLargeVars[g_flowUnsaved.flowCommands[iCommandIndex].index].iValue9)
	CPRINTLN(DEBUG_FLOW, g_flowUnsaved.commsLargeVars[g_flowUnsaved.flowCommands[iCommandIndex].index].eJump1)
	CPRINTLN(DEBUG_FLOW, g_flowUnsaved.commsLargeVars[g_flowUnsaved.flowCommands[iCommandIndex].index].eJump2)
	CPRINTLN(DEBUG_FLOW, g_flowUnsaved.commsLargeVars[g_flowUnsaved.flowCommands[iCommandIndex].index].eJump3)
	CPRINTLN(DEBUG_FLOW, g_flowUnsaved.commsLargeVars[g_flowUnsaved.flowCommands[iCommandIndex].index].eVectorID)
	CPRINTLN(DEBUG_FLOW, g_flowUnsaved.commsLargeVars[g_flowUnsaved.flowCommands[iCommandIndex].index].eCodeID)
	CPRINTLN(DEBUG_FLOW, g_flowUnsaved.commsLargeVars[g_flowUnsaved.flowCommands[iCommandIndex].index].eCheckID)
	CPRINTLN(DEBUG_FLOW, "--------------------------------")
ENDPROC


PROC PRINT_FLOW_COMMAND(INT iCommandIndex)
	SWITCH g_flowUnsaved.flowCommands[iCommandIndex].storageType
		CASE FST_INDEX_DIRECT
			PRINT_FLOW_COMMAND_INDEX_DIRECT(iCommandIndex)
		BREAK
		CASE FST_CORE
			PRINT_FLOW_COMMAND_CORE(iCommandIndex)
		BREAK
		CASE FST_CORE_SMALL
			PRINT_FLOW_COMMAND_CORE_SMALL(iCommandIndex)
		BREAK
		CASE FST_TEXT
			PRINT_FLOW_COMMAND_TEXT(iCommandIndex)
		BREAK
		CASE FST_TEXT_LARGE
			PRINT_FLOW_COMMAND_TEXT_LARGE(iCommandIndex)
		BREAK
		CASE FST_COMMS
			PRINT_FLOW_COMMAND_COMMS(iCommandIndex)
		BREAK
		CASE FST_COMMS_LARGE
			PRINT_FLOW_COMMAND_COMMS_LARGE(iCommandIndex)
		BREAK
		#IF IS_DEBUG_BUILD
		CASE FST_DEBUG
			PRINT_FLOW_COMMAND_DEBUG(iCommandIndex)
		BREAK
		#ENDIF
		DEFAULT
			CPRINTLN(DEBUG_FLOW, "--------------------------------")
			CPRINTLN(DEBUG_FLOW, "UNDEFINED:", GET_COMMAND_DISPLAY_STRING_FROM_COMMAND_ID(g_flowUnsaved.flowCommands[iCommandIndex].command))
			CPRINTLN(DEBUG_FLOW, "--------------------------------")
		BREAK
	ENDSWITCH
ENDPROC

#ENDIF
