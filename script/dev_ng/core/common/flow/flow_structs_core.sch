USING "commands_debug.sch"
USING "clock_globals.sch"
USING "flow_structs_GAME.sch"
USING "flow_commands_enums_GAME.sch"


// Compile in flow command hash ID uniqueness checking on startup. Heavy global useage.
#IF IS_DEBUG_BUILD
CONST_INT	ENABLE_FLOW_COMMAND_HASH_ID_CHECKING	0
#ENDIF
CONST_INT	FLOW_COMMAND_HASH_MODIFIER_MAGNITUDE	1000000


// Used to limit the processing time of the flow controller. This value will control how many strands the 
// flow controller maintains per frame.
#IF NOT DEFINED(MAX_STRANDS_TO_PROCESS_PER_FRAME)
CONST_INT	MAX_STRANDS_TO_PROCESS_PER_FRAME		1
#ENDIF


// The maximum number of mission flow commands and variable storage slots available
// These hold the data that gets set up when the mission flow commands are being initialised

#IF NOT DEFINED(MAX_MISSION_FLOW_COMMANDS)
CONST_INT   MAX_MISSION_FLOW_COMMANDS           1
#ENDIF

#IF NOT DEFINED(MAX_CORE_VARIABLE_SLOTS)
CONST_INT	MAX_CORE_VARIABLE_SLOTS				1
#ENDIF

#IF NOT DEFINED(MAX_CORE__SMALL_VARIABLE_SLOTS)
CONST_INT	MAX_CORE__SMALLVARIABLE_SLOTS		1
#ENDIF

#IF NOT DEFINED(MAX_COMMS_VARIABLE_SLOTS)
CONST_INT	MAX_COMMS_VARIABLE_SLOTS			1
#ENDIF

#IF NOT DEFINED(MAX_COMMS_LARGE_VARIABLE_SLOTS)
CONST_INT	MAX_COMMS_LARGE_VARIABLE_SLOTS		1
#ENDIF

#IF NOT DEFINED(MAX_TEXT_VARIABLE_SLOTS)
CONST_INT	MAX_TEXT_VARIABLE_SLOTS				1
#ENDIF

#IF NOT DEFINED(MAX_TEXT_LARGE_VARIABLE_SLOTS)
CONST_INT	MAX_TEXT_LARGE_VARIABLE_SLOTS		1
#ENDIF

#IF NOT DEFINED(MAX_MISCELLANEOUS_VARIABLE_SLOTS)
CONST_INT   MAX_MISCELLANEOUS_VARIABLE_SLOTS    1
#ENDIF

#IF NOT DEFINED(MAX_SCRIPT_VARIABLE_SLOTS)
CONST_INT   MAX_SCRIPT_VARIABLE_SLOTS           1
#ENDIF

#IF NOT DEFINED(MAX_DEBUG_VARIABLE_SLOTS)
CONST_INT	MAX_DEBUG_VARIABLE_SLOTS			1
#ENDIF

#IF NOT DEFINED(MAX_MISSION_DATA_SLOTS)
CONST_INT	MAX_MISSION_DATA_SLOTS				1
#ENDIF

// The maximum number of missions that can be available to the player at any one time
#IF NOT DEFINED(MAX_MISSIONS_AVAILABLE)
CONST_INT   MAX_MISSIONS_AVAILABLE              1
#ENDIF

#IF NOT DEFINED(DEBUG_MAX_LAUNCH_AND_FORGET_SCRIPTS)
CONST_INT DEBUG_MAX_LAUNCH_AND_FORGET_SCRIPTS	1
#ENDIF
// Miscellaneous
CONST_INT   ILLEGAL_ARRAY_POSITION              -1


// Saved Strand BitFlag IDs - add new ones here, automatically all cleared on initialisation
// ****
// **** IMPORTANT: Variables may get saved using these bits so once setup the order MUST STAY THE SAME ***
// ****
CONST_INT       CLEAR_ALL_AVAILABLE_MISSIONS_BITFLAGS       0
CONST_INT       BITS_AVAILABLE_MISSION_LAUNCHING            1
CONST_INT       BITS_AVAILABLE_MISSION_RUNNING              2
CONST_INT       BITS_AVAILABLE_MISSION_BEING_REPLAYED       3
CONST_INT       BITS_AVAILABLE_MISSION_PASSED               4
CONST_INT       BITS_AVAILABLE_MISSION_FAILED               5
CONST_INT       BITS_AVAILABLE_MISSION_BACKGROUND_PASS      6  // This mission was passed without the player playing it.
CONST_INT       BITS_AVAILABLE_STAT_WATCHER_RUNNING     	7  // We've started a stat_watcher thread for this mission.


// ===========================================================================================================
//      Mission Flow Strand Control Variables
// ===========================================================================================================

// Saved Strand BitFlag IDs - add new ones here, automatically all cleared on initialisation
// ****
// **** IMPORTANT: Variables may get saved using these bits so once setup the order MUST STAY THE SAME ***
// ****
CONST_INT       CLEAR_ALL_SAVED_STRAND_BITFLAGS         0
CONST_INT       SAVED_BITS_STRAND_ACTIVATED             1
CONST_INT       SAVED_BITS_STRAND_TERMINATED            2

// KEEP THIS HERE
CONST_INT   CLEAR_ALL_FLOW_BITFLAGS         0


// NB. Using HASH_ENUMs allows us to insert new flow commands into the list without
// altering other command's IDs. This will be useful when doing consistency checking
// with saves rolled across builds.
HASH_ENUM FLOW_COMMAND_IDS
	USING "flow_commands_core.sch"
	USING "flow_commands_core_override.sch"
	USING "flow_commands_GAME.sch"
	
	FLOW_NONE = -1
ENDENUM



// ===========================================================================================================
//      Strand IDs
// ===========================================================================================================

//  a strand is a sequence of commands
//  multiple strands can be controlled by the same Mission Giver (ie: for Day/Night or Either/Or missions)
//  a strand should require very little setup - unlike GTA IV (aim: a strand ID and a sequence of commands)
#IF NOT DEFINED(STRANDS)
ENUM STRANDS
     STRAND_NONE = -1,
	 MAX_STRANDS = 1
ENDENUM
#ENDIF



// ===========================================================================================================
//      Jump Label IDs
// ===========================================================================================================

#IF NOT DEFINED(JUMP_LABEL_IDS)
ENUM JUMP_LABEL_IDS
    NEXT_SEQUENTIAL_COMMAND		= -2,
    // Use this as the Default (or 'NONE') Label
    STAY_ON_THIS_COMMAND 		= -1,

	//A label located at the very end of the flow.
    //Used by the flow launcher to generate a full playthrough order.
    LABEL_GAME_END,

	//This should always be last.
	MAX_LABELS
ENDENUM
#ENDIF



// ===========================================================================================================
//      Synchronisation IDs
// ===========================================================================================================

//  a synchronisation ID is used to allow one strand to wait until another strand reaches a specific command
//  NOTE: Strand doing the waiting is synchronised TO strand trying to reach a specific command
//  EG: SYNC_JEWEL_HEIST_TO_FAMILY: Jewel Heist strand waits for Family strand to allow it to continue
#IF NOT DEFINED(SYNCHRONIZATION_IDS)
ENUM SYNCHRONIZATION_IDS
    SYNC_NONE = -1,
	MAX_SYNCHRONISATION_IDS = 1 // No zero sized arrays!
ENDENUM
#ENDIF



// ===========================================================================================================
//      Flow Flag IDs
// ===========================================================================================================

//  A flow flag allows different parts of the mission flow to communicate and also allows
//      external scripts to communcate with the mission flow and vice versa
//  NOTE: Using an array of BOOLs rather than bitflags - there shouldn't be huge numbers of these
//          but there are likely to be more than 32 which introduces unneeded complexity into the
//          bitflag system
#IF NOT DEFINED(FLOW_FLAG_IDS)
ENUM FLOW_FLAG_IDS
    FLOWFLAG_NONE = -1,
	MAX_FLOW_FLAG_IDS = 1 // No zero sized arrays!
ENDENUM
#ENDIF



// ===========================================================================================================
//      Flow Int IDs
// ===========================================================================================================

//  A flow int allows different parts of the mission flow to communicate and also allows
//      external scripts to communcate with the mission flow and vice versa
#IF NOT DEFINED(FLOW_INT_IDS)
ENUM FLOW_INT_IDS
    FLOWINT_NONE = -1,
	MAX_FLOW_INT_IDS = 1 // No zero sized arrays!	
ENDENUM
#ENDIF



// ===========================================================================================================
//      Flow BitSet IDs and Bitflag identifiers
// ===========================================================================================================

//  A flow bitflag allows a connected series of bits to be accessed by different parts of the mission flow
//      or externally by other scripts.
//  In essense, each bitset provides an array of flags rather than creating many Flow Flags. It also allows
//      accessing these bits to be controlled through generic routines rather than having to create specific
//      routines to reference specific Flow Flag IDs.
//  For example, we could have an ENUM with these entries: BIOTECH_HEIST=0, AGENCY_HEIST=1, BANK_HEIST=2. We also
//      create a bitset to handle Heists Completed. When a Heist completes it sets the bit associated with
//      it's ENUM value, which can be done within a generic routine.


// Add any sets of bitflag IDs for use with a bitset here
// Heist IDs are found in heist_globals.sch
#IF NOT DEFINED(FLOW_BITSET_IDS)
ENUM FLOW_BITSET_IDS
    FLOWBITSET_NONE = -1,
	MAX_FLOW_BITSET_IDS = 1
ENDENUM
#ENDIF



// ===========================================================================================================
//      Static Blip IDs
// ===========================================================================================================

//If you want your added enum to show up on the debug widget properly then add an entry for it
//in blip_control_public.sch in the switch block in DEBUG_GET_STRING_NAME_OF_STATIC_BLIP
#IF NOT DEFINED(STATIC_BLIP_NAME_ENUM)
ENUM STATIC_BLIP_NAME_ENUM//TODO move this out to own file perhaps
	STATIC_BLIP_NONE = -1
ENDENUM
#ENDIF



// ===========================================================================================================
//      Command Storage Types
// ===========================================================================================================

// Command Storage Type ID
// There are various types of data containers for the mission flow commands to prevent the 
//  'one size fits all' used in GTA IV which wasted a huge amount of global and save game memory.

ENUM FLOW_STORAGE_TYPES
    FST_INDEX_DIRECT,		// The storage type is the index value itself - use cautiously
	FST_CORE,				// The core storage type (4 generic integers)
	FST_CORE_SMALL,			// The core storage type (2 generic integers)
	FST_TEXT,				// Identifies storage associated with text based commands.
	FST_TEXT_LARGE,			// Identifies storage associated with larger text based commands.
	FST_SCRIPT,             // Identifies storage associated with scripts (missions + standalone, etc)
	FST_MISCELLANEOUS,      // Identifies miscellaneous storage for other commands
	
	//GTAV SP specific.
	FST_COMMS,				// Identifies storage associated with a small communication command.
	FST_COMMS_LARGE,		// Identifies storage associated with a larger communication command.
	
#IF IS_DEBUG_BUILD
	FST_DEBUG,				// Storage associated with debug-only flow commands. Storage type won't exist in release.
#ENDIF
	
	//Multiplayer.
    FST_MP_TINY,       		// Identifies storage associated with tiny commands for multiplayer  
    FST_MP_MISSION,    		// Identifies storage associated with mission commands for multiplayer 
	
	// Leave these at the bottom
    MAX_FLOW_STORAGE_TYPES,
	FST_NONE
ENDENUM



// ===========================================================================================================
//      Core Storage Type Variables
// ===========================================================================================================

/// PURPOSE: Core command variables is a generic storage solution for flow commands which need space for 2-4 integers. Typically, this will be branching commands
/// 		 Each command that uses the core command struct needs to remember it's own mapping into the 4 generic integers.
STRUCT CORE_COMMAND_STORAGE
	INT  				iValue1
	INT   				iValue2
	INT   				iValue3
	INT   				iValue4                
ENDSTRUCT


/// PURPOSE: Core command variables is a generic storage solution for flow commands which need space for 2-4 integers. Typically, this will be branching commands
/// 		 Each command that uses the core command struct needs to remember it's own mapping into the 4 generic integers.
STRUCT CORE_SMALL_COMMAND_STORAGE
	INT  				iValue1
	INT   				iValue2               
ENDSTRUCT



// ===========================================================================================================
//      Text Storage Type Variables
// ===========================================================================================================

// Text Command Variables
// Contains all variables associated with text based flow command.
STRUCT TEXT_COMMAND_STORAGE
	TEXT_LABEL_31			txtValue
ENDSTRUCT


// Text Large Command Variables
// Contains all variables associated with larger text based flow command.
STRUCT TEXT_LARGE_COMMAND_STORAGE
	TEXT_LABEL_31			txtValue
	INT						iValue1
	INT						iValue2
	INT						iValue3
	INT						iValue4
ENDSTRUCT


// ===========================================================================================================
//      Script Storage Type Variables
// ===========================================================================================================

// Script Variables
// Contains all variables associated with one script
// NOTE: This may need to be split into saved and unsaved versions of the data

// REMEMBER: New additions to the struct need to be cleared out: Clear_Script_Variables_Array()
STRUCT SCRIPT_COMMAND_STORAGE
    INT						id				// An ID for this mission.
	TEXT_LABEL_31           filename        // The script filename for the mission (without the .sc)
    INT                     filenameHash    // The hash value for the filename
    JUMP_LABEL_IDS         	passGoto        // The label to jump to on mission Pass
    JUMP_LABEL_IDS         	failGoto        // The label to jump to on mission Fail
	INT						generalInt		// General storage int.
	#IF IS_DEBUG_BUILD
		JUMP_LABEL_IDS		debugGoto       // The label used by the flow launcher to bypass flow branching.
	#ENDIF
ENDSTRUCT


// ===========================================================================================================
//      Miscellaneous Storage Type Variables
// ===========================================================================================================

// Miscellaneous Command Variables
// Contains all variables associated with one miscellaneous command
// This is a general purpose variable storage struct when it doesn't make sense to create a specific variables struct

// REMEMBER: New additions to the struct need to be cleared out: Clear_Miscellaneous_Variables_Array()
STRUCT MISCELLANEOUS_COMMAND_STORAGE
    FLOW_BITSET_IDS        	bitsetID        // A general flow bitset ID for commands that need it
    FLOW_FLAG_IDS          	flagID          // A general flow flag ID for commands that need it
    FLOW_INT_IDS           	intID           // A general flow int ID for commands that need it
    JUMP_LABEL_IDS         	jumpLabelID1    // A general label for commands that need to do a jump
    JUMP_LABEL_IDS         	jumpLabelID2    // A second general label for commands that need to do a jump
    JUMP_LABEL_IDS         	jumpLabelID3    // A third general label for commands that need to do a jump
	STRANDS              	strandID        // A general Strand ID for commands that need it
    SYNCHRONIZATION_IDS   	syncID          // A general Sync ID for commands that need it
    STATIC_BLIP_NAME_ENUM   blipID          // A general blip ID for commands that need it
    TIMEOFDAY               aTimeOfDay      // A general TIMEOFDAY struct for holding an in-game date and time.
    BOOL                    aBool           // A general purpose BOOL for any commands that need it
    INT                     anInt           // A general purpose INT for any commands that need it
ENDSTRUCT


// ===========================================================================================================
//      Debug-only Storage Type Variables
// ===========================================================================================================

#IF IS_DEBUG_BUILD
STRUCT DEBUG_COMMAND_STORAGE
    INT	                    anInt1			// A general purpose INT for any commands that need it.
    INT                     anInt2			// A general purpose INT for any commands that need it.
ENDSTRUCT
#ENDIF



// ===========================================================================================================
//      Central Mission Flow Commands Variables
// ===========================================================================================================

// Mission Flow Commands struct
// Contains the command and a reference to the variables required for this command

// REMEMBER: New additions to the struct need to be cleared out: Clear_Mission_Flow_Commands_Array()
STRUCT FLOW_COMMANDS
    FLOW_COMMAND_IDS		command         // The Command ID
    FLOW_STORAGE_TYPES   	storageType     // The Storage Type used for this command
    INT                     index           // array position of the data within the storage type array
ENDSTRUCT

// Mission Flow Commands array index struct
// Allows each strand to record the array positions of it's first and last entries in the array
// NOTE: I was going to combine this into one INT using 'SET_BITS_IN_RANGE', etc but extracting the
//          max and min values would have been done for every strand every frame, so the extra processing
//          to save a few INTS seemed excessive

STRUCT FLOW_COMMAND_BOUNDS
    INT     lower           // lower boundary (inclusive)
    INT     upper           // upper boundary (inclusive)
ENDSTRUCT



// ===========================================================================================================
//      Saved variables for each strand
// ===========================================================================================================

// REMEMBER: New additions to the struct need to be cleared out: Clear_Mission_Flow_Strands_Array()
STRUCT STRAND_SAVED_VARS
    INT                     savedBitflags           // Saved Strand Bools organised as BitFlags
    INT                     thisCommandPos          // Position within the Mission Flow command array for current command
    INT						thisCommandHashID       // The unique hash ID of the command this strand was saved on. Used for error checking and save game repairing.
ENDSTRUCT



// ===========================================================================================================
//      Mission Flow Control Variables
// ===========================================================================================================

// REMEMBER: Clear these out on gameflow start.
STRUCT FLOW_CONTROL_SAVED_VARS
    BOOL        syncIDs[MAX_SYNCHRONISATION_IDS]	// Synchronisation flags - using BOOLS rather than BITFLAGS for ease.
    BOOL        flagIDs[MAX_FLOW_FLAG_IDS]			// Flow flags - using BOOLS rather than BITFLAGS for ease.
    INT         intIDs[MAX_FLOW_INT_IDS]			// Flow ints.
    INT         bitsetIDs[MAX_FLOW_BITSET_IDS] 		// Flow bitsets - each int contains up to 32 bitflags - used for generic access to grouped bitflags.
ENDSTRUCT


STRUCT FLOW_UNSAVED_VARS
// FLAG -   Tells the game that the game flow is being automatically configured and all gameflow controllers
//          should stop maintaining the flow state.
	BOOL    bUpdatingGameflow = FALSE

// FLAG -	This flag returns true while the flow controller is busy processing strand commands. While
//			the controller is in a stable state and waiting for player input to progress this will
//			return false.
	BOOL	bFlowControllerBusy = FALSE
	
// FLAG - 	Set when the flow has no active strands. This will put the flow controller into a low maintenance
//			mode that sleeps the controller every 250ms until the flow finds a strand has been activated.
	BOOL	bIdleUntilActiveStrand = TRUE
	
// Allow the commands processing debug to be output to the console log - controlled by a widget
// When 'on' this will output the command for each strand to the console log at intervals
// NOTE: When timer is less than '0' then commands will be output, so to force output on the
//			next frame force the timer to be 0 on this frame
	BOOL	bFlowCommandsConsoleLogOutputOn		= FALSE
	INT		iFlowCommandsConsoleLogOutputTimer		= 0
	BOOL	bOutputFlowCommandsToConsoleLogNow		= FALSE
	
// FLAG -	When flagged the flow contoller will keep processing the strand it is currently processing until
//			that strand hits a blocking command.
	BOOL	bFlowFastTrackCurrentStrand = FALSE

// FLAG -	When flagged the flow contoller will process all strands the next frame it updates. Usually
//			will only process X number of strands per frame, defined by constant MAX_STRANDS_TO_PROCESS_PER_FRAME. 
	BOOL 	bFlowProcessAllStrandsNextFrame = FALSE

// Allow the F9 mission flow debug screen to display or not?
// NOTE: This flag is used by other systems to hide it when other screens are on display
	BOOL	bShowMissionFlowDebugScreen			= FALSE
	
	// -	While this variable is set to a valid strand ID the flow controller will only process that strand
	//		every frame until this is set back to STRAND_NONE. Useful while on-mission.
	STRANDS	eLockInStrand = STRAND_NONE
	
// ===========================================================================================================
//      Strand Timer Variables - Unsaved
// ===========================================================================================================

	BOOL bStrandTimerSet[MAX_STRANDS]			// A flag to track whether a timer has been set for a specific strand.
	INT  iStrandTimer[MAX_STRANDS]				// A realtime clock timer for each strand.
	TIMEOFDAY sStrandInGameTimer[MAX_STRANDS]	// A TIMEOFDAY timer for each strand.

// -----------------------------------------------------------------------------------------------------------
// Array of core command varaibles for mission flow commands to be stored into.
// This array is built up as flow commands are initialised at the beginning of the game.

	CORE_COMMAND_STORAGE	coreVars[MAX_CORE_VARIABLE_SLOTS]
	
	
// -----------------------------------------------------------------------------------------------------------
// Array of core command varaibles for mission flow commands to be stored into.
// This array is built up as flow commands are initialised at the beginning of the game.

	CORE_SMALL_COMMAND_STORAGE	coreSmallVars[MAX_CORE_SMALL_VARIABLE_SLOTS]
	

// -----------------------------------------------------------------------------------------------------------
// Array of text based command varaibles for mission flow commands to be stored into.
// This array is built up as flow commands are initialised at the beginning of the game.

	TEXT_COMMAND_STORAGE  textVars[MAX_TEXT_VARIABLE_SLOTS]


// -----------------------------------------------------------------------------------------------------------
// Array of larger text based command varaibles for mission flow commands to be stored into.
// This array is built up as flow commands are initialised at the beginning of the game.

	TEXT_LARGE_COMMAND_STORAGE  textLargeVars[MAX_TEXT_LARGE_VARIABLE_SLOTS]
	

// -----------------------------------------------------------------------------------------------------------
// Array of script details for all missions and standalone scripts.
// This array is built up as the flow commands are initialised at the beginning of the game.

	SCRIPT_COMMAND_STORAGE scriptVars[MAX_SCRIPT_VARIABLE_SLOTS]


// -----------------------------------------------------------------------------------------------------------
// Array of miscellaneous details for all mission flow commands that don;t justify creation of specific data storage.
// This array is built up as flow commands are initialised at the beginning of the game.

	MISCELLANEOUS_COMMAND_STORAGE  miscellaneousVars[MAX_MISCELLANEOUS_VARIABLE_SLOTS]


// -----------------------------------------------------------------------------------------------------------
// Array of comms command varaibles for mission flow commands to be stored into.
// This array is built up as flow commands are initialised at the beginning of the game.

#IF DEFINED(COMMS_COMMAND_STORAGE)
	COMMS_COMMAND_STORAGE  commsVars[MAX_COMMS_VARIABLE_SLOTS]
#ENDIF


// -----------------------------------------------------------------------------------------------------------
// Array of comms large command varaibles for mission flow commands to be stored into.
// This array is built up as flow commands are initialised at the beginning of the game.

#IF DEFINED(COMMS_LARGE_COMMAND_STORAGE)
	COMMS_LARGE_COMMAND_STORAGE  commsLargeVars[MAX_COMMS_LARGE_VARIABLE_SLOTS]
#ENDIF


// -----------------------------------------------------------------------------------------------------------
// Array of phonecall details for all mission flow commands associated with phonecalls.
// This array is built up as flow commands are initialised at the beginning of the game.

#IF DEFINED(PHONECALL_COMMAND_STORAGE)
	PHONECALL_COMMAND_STORAGE  phonecallVars[MAX_PHONECALL_VARIABLE_SLOTS]
#ENDIF

// -----------------------------------------------------------------------------------------------------------
// Array of debug command varaibles for all debug-only mission flow commands.
// This array is built up as flow commands are initialised at the beginning of the game.
// NB. Array doesn't exist in release.

#IF IS_DEBUG_BUILD
	DEBUG_COMMAND_STORAGE  debugVars[MAX_DEBUG_VARIABLE_SLOTS]
#ENDIF



// -----------------------------------------------------------------------------------------------------------
// Central flow commands storage.

	FLOW_COMMANDS        	flowCommands[MAX_MISSION_FLOW_COMMANDS]				// The main array indexing where each flow command is stored.
	FLOW_COMMAND_BOUNDS   	flowCommandsIndex[MAX_STRANDS]						// Strand bounds array. Tracks the indexes at which each strand begins and ends within the main g_flowUnsaved.flowCommands array.
	INT     				iNumStoredFlowCommands  = 0						// Counter to track the total number of flow commands that are stored.
	INT     				iNumStoredFlowVariables[MAX_FLOW_STORAGE_TYPES]	// Counter to track the number of commands stored in each storage type array.

ENDSTRUCT




// Active mission data struct to be saved. There will be one of these per mission.
STRUCT MISSION_SAVED_DATA
    BOOL                  	completed				// TRUE if the mission has been completed.
	INT						missionFailsNoProgress	// Number of time this mission has been failed without reaching a new checkpoint. Used by shitskips.
	INT 					missionFailsTotal    	// Number of times this mission has been failed in total.
	INT						iCompletionOrder 		// How many story missions the player had completed before this one. Used in the repeat play system to order missions in the phone. 
	INT						iScore					// "The Score"
	FLOAT					fStatCompletion			// "The Percentage"
ENDSTRUCT

// ===========================================================================================================
//      All Mission Flow Saved Variables
// ===========================================================================================================

// Gathers all mission flow saved structs within one struct so only one line added to the saved globals struct

STRUCT FLOW_SAVED_VARS						
    BOOL                            isGameflowActive		    				// A flag that determines if the gameflow is actively running.
    BOOL                            flowCompleted		    					// A flag that states whether the flow has been completed and no longer needs to process strands.
	STRAND_SAVED_VARS               strandSavedVars[MAX_STRANDS]        		// All strand saved variables
    FLOW_CONTROL_SAVED_VARS     	controls                    				// All saved mission flow control variables
	MISSION_SAVED_DATA  			missionSavedData[MAX_MISSION_DATA_SLOTS]	// Active saved data for each mission in the game.
ENDSTRUCT


// ===========================================================================================================
//      Mission Flow Debug Variables
//      These get initialised in: Initialise_Mission_Flow_Debug_Variables()
// ===========================================================================================================
#IF IS_DEBUG_BUILD
// All Mission Flow Debug specific variables
STRUCT FLOW_DEBUG
    // Control Flags (to ensure read only)
    BOOL    boolOnMission
    BOOL    boolInMultiplayer
 	BOOL	bShowMissionFlowWidgets 
	BOOL	bMissionFlowWidgetsCreated
	
	BOOL 	bGoOnMission
	BOOL 	bGoOffMission
	INT		iCandidateID
	
    //Strand specific bitset.
    BOOL    boolStrandActiveKnownState[MAX_STRANDS]
    BOOL    boolStrandActiveWidgetState[MAX_STRANDS]
	
	WIDGET_GROUP_ID	missionFlowWidgetGroup
ENDSTRUCT
#ENDIF
