
#IF NOT DEFINED(MAINTAIN_CURRENT_MISSION_FLOW_COMMAND_CUSTOM)
FUNC BOOL MAINTAIN_CURRENT_MISSION_FLOW_COMMAND_CUSTOM(FLOW_COMMAND_IDS eCommand, INT iStorageIndex, STRANDS paramStrandID, JUMP_LABEL_IDS &eNextJumpLabel)
	eCommand = eCommand
	iStorageIndex = iStorageIndex
	paramStrandID = paramStrandID
	eNextJumpLabel = eNextJumpLabel
	RETURN FALSE
ENDFUNC
#ENDIF


#IF NOT DEFINED(FLOW_RUN_CUSTOM_UPDATE)
// Override this to have the flow controller run game-specific checks once an update
PROC FLOW_RUN_CUSTOM_UPDATE()
ENDPROC
#ENDIF


#IF NOT DEFINED(FLOW_RUN_CUSTOM_CLEANUP)
// Override this to have the flow controller run game-specific script when cleaning up.
PROC FLOW_RUN_CUSTOM_CLEANUP()
ENDPROC
#ENDIF

