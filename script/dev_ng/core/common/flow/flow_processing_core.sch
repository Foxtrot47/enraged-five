USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_clock.sch"
USING "flow_public_core.sch"
USING "script_clock.sch"

USING "flow_debug_core.sch"

USING "flow_processing_GAME.sch"
USING "flow_processing_core_override.sch"

// ===========================================================================================================
//		Flow Process Strand Jump
// ===========================================================================================================
#if USE_CLF_DLC
PROC FLOW_FOLLOW_JUMP_CLF(STRANDS paramStrandID, JUMP_LABEL_IDS paramJumpID)

	
	// Ensure the strand is valid.
	SWITCH (paramStrandID)
		CASE STRAND_NONE
		CASE MAX_STRANDS_CLF
			CERRORLN(DEBUG_FLOW_COMMANDS, "FLOW_FOLLOW_JUMP: Strand ID is not valid.", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(paramStrandID))
			EXIT
	ENDSWITCH

	// Do nothing if the strand is still awaiting activation
	IF NOT (IS_BIT_SET(g_savedGlobalsClifford.sFlow.strandSavedVars[paramStrandID].savedBitflags, SAVED_BITS_STRAND_ACTIVATED))
		CERRORLN(DEBUG_FLOW_COMMANDS, "FLOW_FOLLOW_JUMP: Ignored because strand is still awaiting activation.", GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(paramJumpID))
		EXIT
	ENDIF
	
	// Do nothing if the strand has already terminated
	IF (IS_BIT_SET(g_savedGlobalsClifford.sFlow.strandSavedVars[paramStrandID].savedBitflags, SAVED_BITS_STRAND_TERMINATED))
		CERRORLN(DEBUG_FLOW_COMMANDS, "FLOW_FOLLOW_JUMP: Ignored because strand has already terminated.", GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(paramJumpID))
		EXIT
	ENDIF

	// Check if the command pointer doesn't need to move
	IF (paramJumpID = STAY_ON_THIS_COMMAND)
		EXIT
	ENDIF
	
	// A new command will be processed next frame
	// From this point on, always force console log output of the current commands being processed
	#IF IS_DEBUG_BUILD
		FORCE_FLOW_CONSOLE_LOG_OUTPUT_NEXT_FRAME()
	#ENDIF
	
	// Check if the command pointer should move to the next command
	IF (paramJumpID = NEXT_SEQUENTIAL_COMMAND)
		g_savedGlobalsClifford.sFlow.strandSavedVars[paramStrandID].thisCommandPos++
		EXIT
	ENDIF
	
	// Move the command pointer to the requested labelID ensuring it stays within the commands list for this strand
	INT iCommandPos		= g_savedGlobalsClifford.sFlow.strandSavedVars[paramStrandID].thisCommandPos
	INT iLowerBounds	= g_flowUnsaved.flowCommandsIndex[paramStrandID].lower
	INT iUpperBounds	= g_flowUnsaved.flowCommandsIndex[paramStrandID].upper
	
	// Ensure the command position for the strand is already within the lower and upper boundaries
	IF (iCommandPos < iLowerBounds)
		CERRORLN(DEBUG_FLOW_COMMANDS, "FLOW_FOLLOW_JUMP: Current command position for strand is outside lower boundary for this strand's commands", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(paramStrandID))
		EXIT
	ENDIF
	IF (iCommandPos > iUpperBounds)
		CERRORLN(DEBUG_FLOW_COMMANDS, "FLOW_FOLLOW_JUMP: Current command position for strand is outside upper boundary for this strand's commands", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(paramStrandID))
		EXIT
	ENDIF
	
	// Starting at the lower boundary and stopping at the upper boundary, find the label for this goto
	INT iTempLoop		= iLowerBounds
	INT iStorageIndex	= ILLEGAL_ARRAY_POSITION
	
	WHILE (iTempLoop <= iUpperBounds)
		IF (g_flowUnsaved.flowCommands[iTempLoop].command = FLOW_LABEL)
		
			// The label is stored in the core vars array. Retrieve the jump label.
			iStorageIndex = g_flowUnsaved.flowCommands[iTempLoop].index
			JUMP_LABEL_IDS eLabel = INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.coreVars[iStorageIndex].iValue1)
			
			//Does this match our jump request?
			IF (eLabel = paramJumpID)
				// ...found the jump label, so store this array position as the current position for the strand
				g_savedGlobalsClifford.sFlow.strandSavedVars[paramStrandID].thisCommandPos = iTempLoop
				EXIT
			ENDIF
			
		ENDIF
		
		#IF IS_DEBUG_BUILD
			//In debug builds safeguard against trying to jump to a 'Launcher Only' label.
			IF (g_flowUnsaved.flowCommands[iTempLoop].command = FLOW_LABEL_LAUNCHER_ONLY)
				// The label is stored directly in the index. Retrieve the jump label.
				JUMP_LABEL_IDS eLabel = INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.flowCommands[iTempLoop].index)
				
				//Does this match our jump request?
				IF (eLabel = paramJumpID)
					//This is an illegal jump. Assert and exit.
					CERRORLN(DEBUG_FLOW_COMMANDS, "Illegal jump to debug label! S:", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(paramStrandID), " L:", GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(eLabel))
					EXIT
				ENDIF
			ENDIF
		#ENDIF
		
		iTempLoop++
	ENDWHILE
	
	// Failed to find this label within this strand's upper and lower boundaries
	CERRORLN(DEBUG_FLOW_COMMANDS,  "FLOW_FOLLOW_JUMP_CLF: Failed to find label within the strand's upper and lower boundaries.", GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(paramJumpID), " ", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(paramStrandID))
ENDPROC
#endif
#if USE_NRM_DLC
PROC FLOW_FOLLOW_JUMP_NRM(STRANDS paramStrandID, JUMP_LABEL_IDS paramJumpID)

	
	// Ensure the strand is valid.
	SWITCH (paramStrandID)
		CASE STRAND_NONE
		CASE MAX_STRANDS_NRM
			CERRORLN(DEBUG_FLOW_COMMANDS, "FLOW_FOLLOW_JUMP: Strand ID is not valid.", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(paramStrandID))
			EXIT
	ENDSWITCH

	// Do nothing if the strand is still awaiting activation
	IF NOT (IS_BIT_SET(g_savedGlobalsnorman.sFlow.strandSavedVars[paramStrandID].savedBitflags, SAVED_BITS_STRAND_ACTIVATED))
		CERRORLN(DEBUG_FLOW_COMMANDS, "FLOW_FOLLOW_JUMP: Ignored because strand is still awaiting activation.", GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(paramJumpID))
		EXIT
	ENDIF
	
	// Do nothing if the strand has already terminated
	IF (IS_BIT_SET(g_savedGlobalsnorman.sFlow.strandSavedVars[paramStrandID].savedBitflags, SAVED_BITS_STRAND_TERMINATED))
		CERRORLN(DEBUG_FLOW_COMMANDS, "FLOW_FOLLOW_JUMP: Ignored because strand has already terminated.", GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(paramJumpID))
		EXIT
	ENDIF

	// Check if the command pointer doesn't need to move
	IF (paramJumpID = STAY_ON_THIS_COMMAND)
		EXIT
	ENDIF
	
	// A new command will be processed next frame
	// From this point on, always force console log output of the current commands being processed
	#IF IS_DEBUG_BUILD
		FORCE_FLOW_CONSOLE_LOG_OUTPUT_NEXT_FRAME()
	#ENDIF
	
	// Check if the command pointer should move to the next command
	IF (paramJumpID = NEXT_SEQUENTIAL_COMMAND)
		g_savedGlobalsnorman.sFlow.strandSavedVars[paramStrandID].thisCommandPos++
		EXIT
	ENDIF
	
	// Move the command pointer to the requested labelID ensuring it stays within the commands list for this strand
	INT iCommandPos		= g_savedGlobalsnorman.sFlow.strandSavedVars[paramStrandID].thisCommandPos
	INT iLowerBounds	= g_flowUnsaved.flowCommandsIndex[paramStrandID].lower
	INT iUpperBounds	= g_flowUnsaved.flowCommandsIndex[paramStrandID].upper
	
	// Ensure the command position for the strand is already within the lower and upper boundaries
	IF (iCommandPos < iLowerBounds)
		CERRORLN(DEBUG_FLOW_COMMANDS, "FLOW_FOLLOW_JUMP: Current command position for strand is outside lower boundary for this strand's commands", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(paramStrandID))
		EXIT
	ENDIF
	IF (iCommandPos > iUpperBounds)
		CERRORLN(DEBUG_FLOW_COMMANDS, "FLOW_FOLLOW_JUMP: Current command position for strand is outside upper boundary for this strand's commands", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(paramStrandID))
		EXIT
	ENDIF
	
	// Starting at the lower boundary and stopping at the upper boundary, find the label for this goto
	INT iTempLoop		= iLowerBounds
	INT iStorageIndex	= ILLEGAL_ARRAY_POSITION
	
	WHILE (iTempLoop <= iUpperBounds)
		IF (g_flowUnsaved.flowCommands[iTempLoop].command = FLOW_LABEL)
		
			// The label is stored in the core vars array. Retrieve the jump label.
			iStorageIndex = g_flowUnsaved.flowCommands[iTempLoop].index
			JUMP_LABEL_IDS eLabel = INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.coreVars[iStorageIndex].iValue1)
			
			//Does this match our jump request?
			IF (eLabel = paramJumpID)
				// ...found the jump label, so store this array position as the current position for the strand
				g_savedGlobalsnorman.sFlow.strandSavedVars[paramStrandID].thisCommandPos = iTempLoop
				EXIT
			ENDIF
			
		ENDIF
		
		#IF IS_DEBUG_BUILD
			//In debug builds safeguard against trying to jump to a 'Launcher Only' label.
			IF (g_flowUnsaved.flowCommands[iTempLoop].command = FLOW_LABEL_LAUNCHER_ONLY)
				// The label is stored directly in the index. Retrieve the jump label.
				JUMP_LABEL_IDS eLabel = INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.flowCommands[iTempLoop].index)
				
				//Does this match our jump request?
				IF (eLabel = paramJumpID)
					//This is an illegal jump. Assert and exit.
					CERRORLN(DEBUG_FLOW_COMMANDS, "Illegal jump to debug label! S:", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(paramStrandID), " L:", GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(eLabel))
					EXIT
				ENDIF
			ENDIF
		#ENDIF
		
		iTempLoop++
	ENDWHILE
	
	// Failed to find this label within this strand's upper and lower boundaries
	CERRORLN(DEBUG_FLOW_COMMANDS,  "FLOW_FOLLOW_JUMP_CLF: Failed to find label within the strand's upper and lower boundaries.", GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(paramJumpID), " ", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(paramStrandID))
ENDPROC
#endif
PROC FLOW_FOLLOW_JUMP(STRANDS paramStrandID, JUMP_LABEL_IDS paramJumpID)

#if USE_CLF_DLC
	FLOW_FOLLOW_JUMP_CLF( paramStrandID,  paramJumpID)
	EXIT
#endif
#if USE_NRM_DLC
	FLOW_FOLLOW_JUMP_NRM( paramStrandID,  paramJumpID)
	EXIT
#endif
#if not USE_CLF_DLC
#if not USE_NRM_DLC
	// Ensure the strand is valid.
	SWITCH (paramStrandID)
		CASE STRAND_NONE
		CASE MAX_STRANDS
			CERRORLN(DEBUG_FLOW_COMMANDS, "FLOW_FOLLOW_JUMP: Strand ID is not valid.", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(paramStrandID))
			EXIT
	ENDSWITCH

	// Do nothing if the strand is still awaiting activation
	IF NOT (IS_BIT_SET(g_savedGlobals.sFlow.strandSavedVars[paramStrandID].savedBitflags, SAVED_BITS_STRAND_ACTIVATED))
		CERRORLN(DEBUG_FLOW_COMMANDS, "FLOW_FOLLOW_JUMP: Ignored because strand is still awaiting activation.", GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(paramJumpID))
		EXIT
	ENDIF
	
	// Do nothing if the strand has already terminated
	IF (IS_BIT_SET(g_savedGlobals.sFlow.strandSavedVars[paramStrandID].savedBitflags, SAVED_BITS_STRAND_TERMINATED))
		CERRORLN(DEBUG_FLOW_COMMANDS, "FLOW_FOLLOW_JUMP: Ignored because strand has already terminated.", GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(paramJumpID))
		EXIT
	ENDIF

	// Check if the command pointer doesn't need to move
	IF (paramJumpID = STAY_ON_THIS_COMMAND)
		EXIT
	ENDIF
	
	// A new command will be processed next frame
	// From this point on, always force console log output of the current commands being processed
	#IF IS_DEBUG_BUILD
		FORCE_FLOW_CONSOLE_LOG_OUTPUT_NEXT_FRAME()
	#ENDIF
	
	// Check if the command pointer should move to the next command
	IF (paramJumpID = NEXT_SEQUENTIAL_COMMAND)
		g_savedGlobals.sFlow.strandSavedVars[paramStrandID].thisCommandPos++
		EXIT
	ENDIF
	
	// Move the command pointer to the requested labelID ensuring it stays within the commands list for this strand
	INT iCommandPos		= g_savedGlobals.sFlow.strandSavedVars[paramStrandID].thisCommandPos
	INT iLowerBounds	= g_flowUnsaved.flowCommandsIndex[paramStrandID].lower
	INT iUpperBounds	= g_flowUnsaved.flowCommandsIndex[paramStrandID].upper
	
	// Ensure the command position for the strand is already within the lower and upper boundaries
	IF (iCommandPos < iLowerBounds)
		CERRORLN(DEBUG_FLOW_COMMANDS, "FLOW_FOLLOW_JUMP: Current command position for strand is outside lower boundary for this strand's commands", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(paramStrandID))
		EXIT
	ENDIF
	IF (iCommandPos > iUpperBounds)
		CERRORLN(DEBUG_FLOW_COMMANDS, "FLOW_FOLLOW_JUMP: Current command position for strand is outside upper boundary for this strand's commands", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(paramStrandID))
		EXIT
	ENDIF
	
	// Starting at the lower boundary and stopping at the upper boundary, find the label for this goto
	INT iTempLoop		= iLowerBounds
	INT iStorageIndex	= ILLEGAL_ARRAY_POSITION
	
	WHILE (iTempLoop <= iUpperBounds)
		IF (g_flowUnsaved.flowCommands[iTempLoop].command = FLOW_LABEL)
		
			// The label is stored in the core vars array. Retrieve the jump label.
			iStorageIndex = g_flowUnsaved.flowCommands[iTempLoop].index
			JUMP_LABEL_IDS eLabel = INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.coreVars[iStorageIndex].iValue1)
			
			//Does this match our jump request?
			IF (eLabel = paramJumpID)
				// ...found the jump label, so store this array position as the current position for the strand
				g_savedGlobals.sFlow.strandSavedVars[paramStrandID].thisCommandPos = iTempLoop
				EXIT
			ENDIF
			
		ENDIF
		
		#IF IS_DEBUG_BUILD
			//In debug builds safeguard against trying to jump to a 'Launcher Only' label.
			IF (g_flowUnsaved.flowCommands[iTempLoop].command = FLOW_LABEL_LAUNCHER_ONLY)
				// The label is stored directly in the index. Retrieve the jump label.
				JUMP_LABEL_IDS eLabel = INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.flowCommands[iTempLoop].index)
				
				//Does this match our jump request?
				IF (eLabel = paramJumpID)
					//This is an illegal jump. Assert and exit.
					CERRORLN(DEBUG_FLOW_COMMANDS, "Illegal jump to debug label! S:", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(paramStrandID), " L:", GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(eLabel))
					EXIT
				ENDIF
			ENDIF
		#ENDIF
		
		iTempLoop++
	ENDWHILE
	
	// Failed to find this label within this strand's upper and lower boundaries
	CERRORLN(DEBUG_FLOW_COMMANDS,  "FLOW_FOLLOW_JUMP: Failed to find label within the strand's upper and lower boundaries.", GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(paramJumpID), " ", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(paramStrandID))
#endif
#endif
ENDPROC



// ===========================================================================================================
//		Strand Management Command Processing
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
// PURPOSE:	Perform the 'Activate Strand' flow command. Sets the Activation BIT of the specified strand.
//
// INPUT PARAMS:		paramIndex			The command storage index. This command diretly stores variable data to this index.
// RETURN VALUE:		JUMP_LABEL_IDS		The Jump Label to continue at.
#if USE_CLF_DLC
FUNC JUMP_LABEL_IDS PERFORM_ACTIVATE_STRAND_CLF(INT paramIndex)

	
	// Check the index is within valid casting range.
	IF paramIndex < ENUM_TO_INT(STRAND_NONE) OR paramIndex > ENUM_TO_INT(MAX_STRANDS_CLF)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_ACTIVATE_STRAND: The passed index is not castable into a valid STRANDS enum. MOVING ON TO NEXT COMMAND.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Get the strand to be activated.
	// NB. This command uses direct index storage.
	STRANDS eStrand = INT_TO_ENUM(STRANDS, paramIndex)
	
	// Ensure the strand to be activated is legal
	IF (eStrand = STRAND_NONE)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_ACTIVATE_STRAND: The Strand to be activated is illegal. MOVING ON TO NEXT COMMAND.", eStrand)
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Ensure the strand to be activated hasn't already been activated
	// NOT AN ASSERT though, because this could be genuine
	IF (IS_BIT_SET(g_savedGlobalsClifford.sFlow.strandSavedVars[eStrand].savedBitflags, SAVED_BITS_STRAND_ACTIVATED))
		CWARNINGLN(DEBUG_FLOW_COMMANDS, "PERFORM_ACTIVATE_STRAND: The Strand to be activated is already active. MOVING ON TO NEXT COMMAND.", eStrand)
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Activate the strand
	SET_BIT(g_savedGlobalsClifford.sFlow.strandSavedVars[eStrand].savedBitflags, SAVED_BITS_STRAND_ACTIVATED)
	
	// Output flow debug.
	#IF IS_DEBUG_BUILD
		IF IS_FLOW_CONSOLE_LOG_READY_TO_PRINT()
			CDEBUG1LN(DEBUG_FLOW_COMMANDS, "Activate Strand (STRAND: ", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(eStrand), ")")
		ENDIF
	#ENDIF
	
	RETURN NEXT_SEQUENTIAL_COMMAND
	
ENDFUNC
#endif
#if USE_NRM_DLC
FUNC JUMP_LABEL_IDS PERFORM_ACTIVATE_STRAND_NRM(INT paramIndex)

	
	// Check the index is within valid casting range.
	IF paramIndex < ENUM_TO_INT(STRAND_NONE) OR paramIndex > ENUM_TO_INT(MAX_STRANDS_NRM)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_ACTIVATE_STRAND: The passed index is not castable into a valid STRANDS enum. MOVING ON TO NEXT COMMAND.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Get the strand to be activated.
	// NB. This command uses direct index storage.
	STRANDS eStrand = INT_TO_ENUM(STRANDS, paramIndex)
	
	// Ensure the strand to be activated is legal
	IF (eStrand = STRAND_NONE)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_ACTIVATE_STRAND: The Strand to be activated is illegal. MOVING ON TO NEXT COMMAND.", eStrand)
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Ensure the strand to be activated hasn't already been activated
	// NOT AN ASSERT though, because this could be genuine
	IF (IS_BIT_SET(g_savedGlobalsnorman.sFlow.strandSavedVars[eStrand].savedBitflags, SAVED_BITS_STRAND_ACTIVATED))
		CWARNINGLN(DEBUG_FLOW_COMMANDS, "PERFORM_ACTIVATE_STRAND: The Strand to be activated is already active. MOVING ON TO NEXT COMMAND.", eStrand)
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Activate the strand
	SET_BIT(g_savedGlobalsnorman.sFlow.strandSavedVars[eStrand].savedBitflags, SAVED_BITS_STRAND_ACTIVATED)
	
	// Output flow debug.
	#IF IS_DEBUG_BUILD
		IF IS_FLOW_CONSOLE_LOG_READY_TO_PRINT()
			CDEBUG1LN(DEBUG_FLOW_COMMANDS, "Activate Strand (STRAND: ", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(eStrand), ")")
		ENDIF
	#ENDIF
	
	RETURN NEXT_SEQUENTIAL_COMMAND
	
ENDFUNC
#endif
FUNC JUMP_LABEL_IDS PERFORM_ACTIVATE_STRAND(INT paramIndex)
	
	#if USE_CLF_DLC
		return PERFORM_ACTIVATE_STRAND_CLF(paramIndex)
	#endif
	#if USE_NRM_DLC
		return PERFORM_ACTIVATE_STRAND_NRM(paramIndex)
	#endif
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
	// Check the index is within valid casting range.
	IF paramIndex < ENUM_TO_INT(STRAND_NONE) OR paramIndex > ENUM_TO_INT(MAX_STRANDS)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_ACTIVATE_STRAND: The passed index is not castable into a valid STRANDS enum. MOVING ON TO NEXT COMMAND.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Get the strand to be activated.
	// NB. This command uses direct index storage.
	STRANDS eStrand = INT_TO_ENUM(STRANDS, paramIndex)
	
	// Ensure the strand to be activated is legal
	IF (eStrand = STRAND_NONE)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_ACTIVATE_STRAND: The Strand to be activated is illegal. MOVING ON TO NEXT COMMAND.", eStrand)
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Ensure the strand to be activated hasn't already been activated
	// NOT AN ASSERT though, because this could be genuine
	IF (IS_BIT_SET(g_savedGlobals.sFlow.strandSavedVars[eStrand].savedBitflags, SAVED_BITS_STRAND_ACTIVATED))
		CWARNINGLN(DEBUG_FLOW_COMMANDS, "PERFORM_ACTIVATE_STRAND: The Strand to be activated is already active. MOVING ON TO NEXT COMMAND.", eStrand)
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Activate the strand
	SET_BIT(g_savedGlobals.sFlow.strandSavedVars[eStrand].savedBitflags, SAVED_BITS_STRAND_ACTIVATED)
	
	// Output flow debug.
	#IF IS_DEBUG_BUILD
		IF IS_FLOW_CONSOLE_LOG_READY_TO_PRINT()
			CDEBUG1LN(DEBUG_FLOW_COMMANDS, "Activate Strand (STRAND: ", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(eStrand), ")")
		ENDIF
	#ENDIF
	
	RETURN NEXT_SEQUENTIAL_COMMAND
	#endif
	#endif
ENDFUNC


// -----------------------------------------------------------------------------------------------------------
// PURPOSE:	Perform the 'Kill Strand' flow command. Sets the Termination BIT of the specified strand.
//
// INPUT PARAMS:		paramIndex			The command storage index. This command diretly stores variable data to this index.
// RETURN VALUE:		JUMP_LABEL_IDS		The Jump Label to continue at.
#IF USE_CLF_DLC
FUNC JUMP_LABEL_IDS PERFORM_KILL_STRAND_CLF(INT paramIndex)

	// Check the index is within valid casting range.
	IF paramIndex < ENUM_TO_INT(STRAND_NONE) OR paramIndex > ENUM_TO_INT(MAX_STRANDS_CLF)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_KILL_STRAND_CLF: The passed index is not castable into a valid STRANDS enum. MOVING ON TO NEXT COMMAND.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF

	// Get the strand to be killed.
	// NB. This command uses direct index storage.
	STRANDS eStrand = INT_TO_ENUM(STRANDS, paramIndex)
	
	// Ensure the strand to be killed is legal
	IF (eStrand = STRAND_NONE)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_KILL_STRAND_CLF: The Strand to be killed is illegal. MOVING ON TO NEXT COMMAND.", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(eStrand))
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Ensure the strand to be killed hasn't already been terminated
	// NOT AN ASSERT though, because this could be genuine
	IF (IS_BIT_SET(g_savedGlobalsClifford.sFlow.strandSavedVars[eStrand].savedBitflags, SAVED_BITS_STRAND_TERMINATED))
		CWARNINGLN(DEBUG_FLOW_COMMANDS, "PERFORM_KILL_STRAND_CLF: The Strand to be killed is already terminated. MOVING ON TO NEXT COMMAND.", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(eStrand))
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Output flow debug.
	#IF IS_DEBUG_BUILD
		IF IS_FLOW_CONSOLE_LOG_READY_TO_PRINT()
			CDEBUG1LN(DEBUG_FLOW_COMMANDS, "Kill Strand (STRAND: ",  GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(eStrand), ")")
		ENDIF
	#ENDIF
	
	// Terminate the strand
	SET_BIT(g_savedGlobalsClifford.sFlow.strandSavedVars[eStrand].savedBitflags, SAVED_BITS_STRAND_TERMINATED)
	
	// If the strand isn't activated yet, also activate it.
	// Avoids leaving the strand in a nonsensical state.
	IF NOT IS_BIT_SET(g_savedGlobalsClifford.sFlow.strandSavedVars[eStrand].savedBitflags, SAVED_BITS_STRAND_ACTIVATED)
		SET_BIT(g_savedGlobalsClifford.sFlow.strandSavedVars[eStrand].savedBitflags, SAVED_BITS_STRAND_ACTIVATED)
	ENDIF
	
	RETURN NEXT_SEQUENTIAL_COMMAND
	
ENDFUNC
#endif
#if USE_NRM_DLC
FUNC JUMP_LABEL_IDS PERFORM_KILL_STRAND_NRM(INT paramIndex)

	// Check the index is within valid casting range.
	IF paramIndex < ENUM_TO_INT(STRAND_NONE) OR paramIndex > ENUM_TO_INT(MAX_STRANDS_NRM)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_KILL_STRAND_CLF: The passed index is not castable into a valid STRANDS enum. MOVING ON TO NEXT COMMAND.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF

	// Get the strand to be killed.
	// NB. This command uses direct index storage.
	STRANDS eStrand = INT_TO_ENUM(STRANDS, paramIndex)
	
	// Ensure the strand to be killed is legal
	IF (eStrand = STRAND_NONE)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_KILL_STRAND_CLF: The Strand to be killed is illegal. MOVING ON TO NEXT COMMAND.", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(eStrand))
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Ensure the strand to be killed hasn't already been terminated
	// NOT AN ASSERT though, because this could be genuine
	IF (IS_BIT_SET(g_savedGlobalsnorman.sFlow.strandSavedVars[eStrand].savedBitflags, SAVED_BITS_STRAND_TERMINATED))
		CWARNINGLN(DEBUG_FLOW_COMMANDS, "PERFORM_KILL_STRAND_CLF: The Strand to be killed is already terminated. MOVING ON TO NEXT COMMAND.", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(eStrand))
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Output flow debug.
	#IF IS_DEBUG_BUILD
		IF IS_FLOW_CONSOLE_LOG_READY_TO_PRINT()
			CDEBUG1LN(DEBUG_FLOW_COMMANDS, "Kill Strand (STRAND: ",  GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(eStrand), ")")
		ENDIF
	#ENDIF
	
	// Terminate the strand
	SET_BIT(g_savedGlobalsnorman.sFlow.strandSavedVars[eStrand].savedBitflags, SAVED_BITS_STRAND_TERMINATED)
	
	// If the strand isn't activated yet, also activate it.
	// Avoids leaving the strand in a nonsensical state.
	IF NOT IS_BIT_SET(g_savedGlobalsnorman.sFlow.strandSavedVars[eStrand].savedBitflags, SAVED_BITS_STRAND_ACTIVATED)
		SET_BIT(g_savedGlobalsnorman.sFlow.strandSavedVars[eStrand].savedBitflags, SAVED_BITS_STRAND_ACTIVATED)
	ENDIF
	
	RETURN NEXT_SEQUENTIAL_COMMAND
	
ENDFUNC
#ENDIF
FUNC JUMP_LABEL_IDS PERFORM_KILL_STRAND(INT paramIndex)

	#if USE_CLF_DLC
		return PERFORM_KILL_STRAND_CLF(paramIndex)
	#endif
	#if USE_NRM_DLC
		return PERFORM_KILL_STRAND_NRM(paramIndex)
	#endif
	#IF NOT USE_CLF_DLC
	#if not USE_NRM_DLC
	// Check the index is within valid casting range.
	IF paramIndex < ENUM_TO_INT(STRAND_NONE) OR paramIndex > ENUM_TO_INT(MAX_STRANDS)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_KILL_STRAND: The passed index is not castable into a valid STRANDS enum. MOVING ON TO NEXT COMMAND.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF

	// Get the strand to be killed.
	// NB. This command uses direct index storage.
	STRANDS eStrand = INT_TO_ENUM(STRANDS, paramIndex)
	
	// Ensure the strand to be killed is legal
	IF (eStrand = STRAND_NONE)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_KILL_STRAND: The Strand to be killed is illegal. MOVING ON TO NEXT COMMAND.", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(eStrand))
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Ensure the strand to be killed hasn't already been terminated
	// NOT AN ASSERT though, because this could be genuine
	IF (IS_BIT_SET(g_savedGlobals.sFlow.strandSavedVars[eStrand].savedBitflags, SAVED_BITS_STRAND_TERMINATED))
		CWARNINGLN(DEBUG_FLOW_COMMANDS, "PERFORM_KILL_STRAND: The Strand to be killed is already terminated. MOVING ON TO NEXT COMMAND.", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(eStrand))
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Output flow debug.
	#IF IS_DEBUG_BUILD
		IF IS_FLOW_CONSOLE_LOG_READY_TO_PRINT()
			CDEBUG1LN(DEBUG_FLOW_COMMANDS, "Kill Strand (STRAND: ",  GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(eStrand), ")")
		ENDIF
	#ENDIF
	
	// Terminate the strand
	SET_BIT(g_savedGlobals.sFlow.strandSavedVars[eStrand].savedBitflags, SAVED_BITS_STRAND_TERMINATED)
	
	// If the strand isn't activated yet, also activate it.
	// Avoids leaving the strand in a nonsensical state.
	IF NOT IS_BIT_SET(g_savedGlobals.sFlow.strandSavedVars[eStrand].savedBitflags, SAVED_BITS_STRAND_ACTIVATED)
		SET_BIT(g_savedGlobals.sFlow.strandSavedVars[eStrand].savedBitflags, SAVED_BITS_STRAND_ACTIVATED)
	ENDIF
	
	RETURN NEXT_SEQUENTIAL_COMMAND
	#endif
	#endif
ENDFUNC



// ===========================================================================================================
//		Jump Command Processing
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
// PURPOSE:	Perform the 'Goto' flow command. Modifies the command pointer for a strand to point to the location 
//			of the specified label.
//
// INPUT PARAMS:	paramIndex			The command storage index. This command diretly stores variable data to this index.
//					paramGotoID			The Goto ID to search for
#if USE_CLF_DLC
FUNC JUMP_LABEL_IDS PERFORM_GOTO_CLF(INT paramIndex)

	// Get the jump label.
	JUMP_LABEL_IDS eGotoJump = INT_TO_ENUM(JUMP_LABEL_IDS, (paramIndex%FLOW_COMMAND_HASH_MODIFIER_MAGNITUDE)) // NB. This command uses direct index storage.

	// Check the index is within valid casting range.
	IF eGotoJump < NEXT_SEQUENTIAL_COMMAND OR eGotoJump > MAX_LABELS_CLF
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_GOTO_CLF: The passed index is not castable into a valid JUMP_LABEL_IDS enum. MOVING ON TO NEXT COMMAND.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Output flow debug.
	#IF IS_DEBUG_BUILD
		IF IS_FLOW_CONSOLE_LOG_READY_TO_PRINT()
			CDEBUG1LN(DEBUG_FLOW_COMMANDS, "Goto (LABEL: ", GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(eGotoJump), ")")
		ENDIF
	#ENDIF
	
	// Return the jump label to be jumped to.
	RETURN eGotoJump

ENDFUNC
#endif
#if USE_NRM_DLC
FUNC JUMP_LABEL_IDS PERFORM_GOTO_NRM(INT paramIndex)

	// Get the jump label.
	JUMP_LABEL_IDS eGotoJump = INT_TO_ENUM(JUMP_LABEL_IDS, (paramIndex%FLOW_COMMAND_HASH_MODIFIER_MAGNITUDE)) // NB. This command uses direct index storage.

	// Check the index is within valid casting range.
	IF eGotoJump < NEXT_SEQUENTIAL_COMMAND OR eGotoJump > MAX_LABELS_NRM
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_GOTO_CLF: The passed index is not castable into a valid JUMP_LABEL_IDS enum. MOVING ON TO NEXT COMMAND.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Output flow debug.
	#IF IS_DEBUG_BUILD
		IF IS_FLOW_CONSOLE_LOG_READY_TO_PRINT()
			CDEBUG1LN(DEBUG_FLOW_COMMANDS, "Goto (LABEL: ", GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(eGotoJump), ")")
		ENDIF
	#ENDIF
	
	// Return the jump label to be jumped to.
	RETURN eGotoJump

ENDFUNC
#endif
FUNC JUMP_LABEL_IDS PERFORM_GOTO(INT paramIndex)

	#if USE_CLF_DLC
		return PERFORM_GOTO_CLF(paramIndex)
	#endif
	#if USE_NRM_DLC
		return PERFORM_GOTO_NRM(paramIndex)
	#endif
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
	// Get the jump label.
	JUMP_LABEL_IDS eGotoJump = INT_TO_ENUM(JUMP_LABEL_IDS, (paramIndex%FLOW_COMMAND_HASH_MODIFIER_MAGNITUDE)) // NB. This command uses direct index storage.

	// Check the index is within valid casting range.
	IF eGotoJump < NEXT_SEQUENTIAL_COMMAND OR eGotoJump > MAX_LABELS
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_GOTO: The passed index is not castable into a valid JUMP_LABEL_IDS enum. MOVING ON TO NEXT COMMAND.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Output flow debug.
	#IF IS_DEBUG_BUILD
		IF IS_FLOW_CONSOLE_LOG_READY_TO_PRINT()
			CDEBUG1LN(DEBUG_FLOW_COMMANDS, "Goto (LABEL: ", GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(eGotoJump), ")")
		ENDIF
	#ENDIF
	
	// Return the jump label to be jumped to.
	RETURN eGotoJump
	#endif
	#endif
ENDFUNC


// -----------------------------------------------------------------------------------------------------------
// PURPOSE:	Perform the 'strand Goto' flow command. Modifies the command pointer for a strand to point to the location 
//			of the specified label within that strand.
//
// INPUT PARAMS:		paramMiscellaneousVarsArrayPos		Index into the Miscellaneous Variables array
// RETURN VALUE:		JUMP_LABEL_IDS						The Jump Label to continue at
#if USE_CLF_DLC
FUNC JUMP_LABEL_IDS PERFORM_STRAND_GOTO_CLF(INT paramCoreSmallVarsArrayPos)

// Ensure the Core Small Vars Array position is legal
	IF (paramCoreSmallVarsArrayPos = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_STRAND_GOTO_CLF: The Core Small Variables Array Position parameter is illegal. MOVING ON TO NEXT COMMAND.")
		
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Get the Strand to be tested
	STRANDS eStrandID = INT_TO_ENUM(STRANDS, g_flowUnsaved.coreSmallVars[paramCoreSmallVarsArrayPos].iValue1)	
	
	// Ensure the strand to be tested is legal
	IF (eStrandID = STRAND_NONE)
	OR (eStrandID = MAX_STRANDS_CLF)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_STRAND_GOTO_CLF: The StrandID is illegal. MOVING ON TO NEXT COMMAND.")
		
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Do nothing if the strand is still awaiting activation
	IF NOT (IS_BIT_SET(g_savedGlobalsClifford.sFlow.strandSavedVars[eStrandID].savedBitflags, SAVED_BITS_STRAND_ACTIVATED))
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_STRAND_GOTO: Ignored because strand is still awaiting activation. MOVING ON TO NEXT COMMAND.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Do nothing if the strand has already terminated
	IF (IS_BIT_SET(g_savedGlobalsClifford.sFlow.strandSavedVars[eStrandID].savedBitflags, SAVED_BITS_STRAND_TERMINATED))
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_STRAND_GOTO: Ignored because strand has already terminated. MOVING ON TO NEXT COMMAND.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
		
	// Get the jump labels.
	JUMP_LABEL_IDS eJumpToLabel = INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.coresmallVars[paramCoreSmallVarsArrayPos].iValue2)
	
	INT iCommandPos		= g_savedGlobalsClifford.sFlow.strandSavedVars[eStrandID].thisCommandPos
	INT iLowerBounds	= g_flowUnsaved.flowCommandsIndex[eStrandID].lower
	INT iUpperBounds	= g_flowUnsaved.flowCommandsIndex[eStrandID].upper
	
	// Ensure the command position for the strand is already within the lower and upper boundaries
	IF (iCommandPos < iLowerBounds)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_STRAND_GOTO: Current command position for strand is outside lower boundary for this strand's commands", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(eStrandID))
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	IF (iCommandPos > iUpperBounds)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_STRAND_GOTO: Current command position for strand is outside upper boundary for this strand's commands", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(eStrandID))
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Starting at the lower boundary and stopping at the upper boundary, find the label for this goto
	INT iTempLoop		= iLowerBounds
	INT iStorageIndex	= ILLEGAL_ARRAY_POSITION
	
	WHILE (iTempLoop <= iUpperBounds)
		IF (g_flowUnsaved.flowCommands[iTempLoop].command = FLOW_LABEL)
		
			// The label is stored in the core vars array. Retrieve the jump label.
			iStorageIndex = g_flowUnsaved.flowCommands[iTempLoop].index
			JUMP_LABEL_IDS eLabel = INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.coreVars[iStorageIndex].iValue1)
			
			//Does this match our jump request?
			IF (eLabel = eJumpToLabel)
				// ...found the jump label, so store this array position as the current position for the strand
				g_savedGlobalsClifford.sFlow.strandSavedVars[eStrandID].thisCommandPos = iTempLoop
				RETURN NEXT_SEQUENTIAL_COMMAND
			ENDIF
			
		ENDIF
		
		iTempLoop++
	ENDWHILE
	
	// Failed to find this label within this strand's upper and lower boundaries
	CERRORLN(DEBUG_FLOW_COMMANDS,  "PERFORM_STRAND_GOTO: Failed to find label within the strand's upper and lower boundaries.", GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(eJumpToLabel), " ", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(eStrandID))
	RETURN NEXT_SEQUENTIAL_COMMAND
ENDFUNC
#endif
#if USE_NRM_DLC
FUNC JUMP_LABEL_IDS PERFORM_STRAND_GOTO_NRM(INT paramCoreSmallVarsArrayPos)

// Ensure the Core Small Vars Array position is legal
	IF (paramCoreSmallVarsArrayPos = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_STRAND_GOTO_CLF: The Core Small Variables Array Position parameter is illegal. MOVING ON TO NEXT COMMAND.")
		
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Get the Strand to be tested
	STRANDS eStrandID = INT_TO_ENUM(STRANDS, g_flowUnsaved.coreSmallVars[paramCoreSmallVarsArrayPos].iValue1)	
	
	// Ensure the strand to be tested is legal
	IF (eStrandID = STRAND_NONE)
	OR (eStrandID = MAX_STRANDS_NRM)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_STRAND_GOTO_CLF: The StrandID is illegal. MOVING ON TO NEXT COMMAND.")
		
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Do nothing if the strand is still awaiting activation
	IF NOT (IS_BIT_SET(g_savedGlobalsnorman.sFlow.strandSavedVars[eStrandID].savedBitflags, SAVED_BITS_STRAND_ACTIVATED))
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_STRAND_GOTO: Ignored because strand is still awaiting activation. MOVING ON TO NEXT COMMAND.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Do nothing if the strand has already terminated
	IF (IS_BIT_SET(g_savedGlobalsnorman.sFlow.strandSavedVars[eStrandID].savedBitflags, SAVED_BITS_STRAND_TERMINATED))
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_STRAND_GOTO: Ignored because strand has already terminated. MOVING ON TO NEXT COMMAND.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
		
	// Get the jump labels.
	JUMP_LABEL_IDS eJumpToLabel = INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.coresmallVars[paramCoreSmallVarsArrayPos].iValue2)
	
	INT iCommandPos		= g_savedGlobalsnorman.sFlow.strandSavedVars[eStrandID].thisCommandPos
	INT iLowerBounds	= g_flowUnsaved.flowCommandsIndex[eStrandID].lower
	INT iUpperBounds	= g_flowUnsaved.flowCommandsIndex[eStrandID].upper
	
	// Ensure the command position for the strand is already within the lower and upper boundaries
	IF (iCommandPos < iLowerBounds)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_STRAND_GOTO: Current command position for strand is outside lower boundary for this strand's commands", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(eStrandID))
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	IF (iCommandPos > iUpperBounds)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_STRAND_GOTO: Current command position for strand is outside upper boundary for this strand's commands", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(eStrandID))
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Starting at the lower boundary and stopping at the upper boundary, find the label for this goto
	INT iTempLoop		= iLowerBounds
	INT iStorageIndex	= ILLEGAL_ARRAY_POSITION
	
	WHILE (iTempLoop <= iUpperBounds)
		IF (g_flowUnsaved.flowCommands[iTempLoop].command = FLOW_LABEL)
		
			// The label is stored in the core vars array. Retrieve the jump label.
			iStorageIndex = g_flowUnsaved.flowCommands[iTempLoop].index
			JUMP_LABEL_IDS eLabel = INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.coreVars[iStorageIndex].iValue1)
			
			//Does this match our jump request?
			IF (eLabel = eJumpToLabel)
				// ...found the jump label, so store this array position as the current position for the strand
				g_savedGlobalsnorman.sFlow.strandSavedVars[eStrandID].thisCommandPos = iTempLoop
				RETURN NEXT_SEQUENTIAL_COMMAND
			ENDIF
			
		ENDIF
		
		iTempLoop++
	ENDWHILE
	
	// Failed to find this label within this strand's upper and lower boundaries
	CERRORLN(DEBUG_FLOW_COMMANDS,  "PERFORM_STRAND_GOTO: Failed to find label within the strand's upper and lower boundaries.", GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(eJumpToLabel), " ", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(eStrandID))
	RETURN NEXT_SEQUENTIAL_COMMAND
ENDFUNC
#ENDIF
FUNC JUMP_LABEL_IDS PERFORM_STRAND_GOTO(INT paramCoreSmallVarsArrayPos)

	#if USE_CLF_DLC
		return PERFORM_STRAND_GOTO_CLF(paramCoreSmallVarsArrayPos)
	#endif
	#if USE_NRM_DLC
		return PERFORM_STRAND_GOTO_NRM(paramCoreSmallVarsArrayPos)
	#endif
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
// Ensure the Core Small Vars Array position is legal
	IF (paramCoreSmallVarsArrayPos = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_STRAND_GOTO: The Core Small Variables Array Position parameter is illegal. MOVING ON TO NEXT COMMAND.")
		
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Get the Strand to be tested
	STRANDS eStrandID = INT_TO_ENUM(STRANDS, g_flowUnsaved.coreSmallVars[paramCoreSmallVarsArrayPos].iValue1)	
	
	// Ensure the strand to be tested is legal
	IF (eStrandID = STRAND_NONE)
	OR (eStrandID = MAX_STRANDS)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_STRAND_GOTO: The StrandID is illegal. MOVING ON TO NEXT COMMAND.")
		
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Do nothing if the strand is still awaiting activation
	IF NOT (IS_BIT_SET(g_savedGlobals.sFlow.strandSavedVars[eStrandID].savedBitflags, SAVED_BITS_STRAND_ACTIVATED))
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_STRAND_GOTO: Ignored because strand is still awaiting activation. MOVING ON TO NEXT COMMAND.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Do nothing if the strand has already terminated
	IF (IS_BIT_SET(g_savedGlobals.sFlow.strandSavedVars[eStrandID].savedBitflags, SAVED_BITS_STRAND_TERMINATED))
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_STRAND_GOTO: Ignored because strand has already terminated. MOVING ON TO NEXT COMMAND.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
		
	// Get the jump labels.
	JUMP_LABEL_IDS eJumpToLabel = INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.coresmallVars[paramCoreSmallVarsArrayPos].iValue2)
	
	INT iCommandPos		= g_savedGlobals.sFlow.strandSavedVars[eStrandID].thisCommandPos
	INT iLowerBounds	= g_flowUnsaved.flowCommandsIndex[eStrandID].lower
	INT iUpperBounds	= g_flowUnsaved.flowCommandsIndex[eStrandID].upper
	
	// Ensure the command position for the strand is already within the lower and upper boundaries
	IF (iCommandPos < iLowerBounds)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_STRAND_GOTO: Current command position for strand is outside lower boundary for this strand's commands", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(eStrandID))
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	IF (iCommandPos > iUpperBounds)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_STRAND_GOTO: Current command position for strand is outside upper boundary for this strand's commands", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(eStrandID))
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Starting at the lower boundary and stopping at the upper boundary, find the label for this goto
	INT iTempLoop		= iLowerBounds
	INT iStorageIndex	= ILLEGAL_ARRAY_POSITION
	
	WHILE (iTempLoop <= iUpperBounds)
		IF (g_flowUnsaved.flowCommands[iTempLoop].command = FLOW_LABEL)
		
			// The label is stored in the core vars array. Retrieve the jump label.
			iStorageIndex = g_flowUnsaved.flowCommands[iTempLoop].index
			JUMP_LABEL_IDS eLabel = INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.coreVars[iStorageIndex].iValue1)
			
			//Does this match our jump request?
			IF (eLabel = eJumpToLabel)
				// ...found the jump label, so store this array position as the current position for the strand
				g_savedGlobals.sFlow.strandSavedVars[eStrandID].thisCommandPos = iTempLoop
				RETURN NEXT_SEQUENTIAL_COMMAND
			ENDIF
			
		ENDIF
		
		iTempLoop++
	ENDWHILE
	
	// Failed to find this label within this strand's upper and lower boundaries
	CERRORLN(DEBUG_FLOW_COMMANDS,  "PERFORM_STRAND_GOTO: Failed to find label within the strand's upper and lower boundaries.", GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(eJumpToLabel), " ", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(eStrandID))
	RETURN NEXT_SEQUENTIAL_COMMAND
	#ENDIF
	#ENDIF
ENDFUNC


// ===========================================================================================================
//		Branching Jump Command Processing
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
// PURPOSE:	Perform the 'Store Flag State' flow command. Set the specified Flow Flag to its required state.
//
// INPUT PARAMS:		paramCoreVarsArrayPos		Index into the Core Variables array
// RETURN VALUE:		JUMP_LABEL_IDS				The Jump Label to continue at
#if USE_CLF_DLC
FUNC JUMP_LABEL_IDS PERFORM_STORE_FLAG_STATE_CLF(INT paramCoreVarsArrayPos)
	
	// Ensure the Core Vars Array position is legal
	IF (paramCoreVarsArrayPos = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_STORE_FLAG_STATE_CLF: The Core Variables Array Position parameter is illegal. MOVING ON TO NEXT COMMAND.")
		
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Get the Flow Flag to be modified
	FLOW_FLAG_IDS eFlagID = INT_TO_ENUM(FLOW_FLAG_IDS, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue1)
	
	// Ensure the flag to be modified is legal
	IF (eFlagID = FLOWFLAG_NONE)
	OR (eFlagID = MAX_FLOW_FLAG_IDS_CLF)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_STORE_FLAG_STATE: The FlowFlagID to be stored is illegal. MOVING ON TO NEXT COMMAND.")
		
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	
	// Retrieve the new flag state. Convert from INT back to BOOL.
	BOOL bNewState = FALSE
	IF g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue2 = 1
		bNewState = TRUE
	ENDIF
	
	// Output flow debug.
	#IF IS_DEBUG_BUILD
		IF IS_FLOW_CONSOLE_LOG_READY_TO_PRINT()
			CDEBUG1LN(DEBUG_FLOW_COMMANDS, "Store Flag State (FLAG=", GET_FLAG_DISPLAY_STRING_FROM_FLAG_ID(eFlagID), " NEW-STATE=", GET_BOOL_DISPLAY_STRING_FROM_BOOL(bNewState), ")")
		ENDIF
	#ENDIF
	
	// Set the Flow Flag to its new state.
	SET_MISSION_FLOW_FLAG_STATE(eFlagID, bNewState)
	
	RETURN NEXT_SEQUENTIAL_COMMAND
	
ENDFUNC
#endif
#if USE_NRM_DLC
FUNC JUMP_LABEL_IDS PERFORM_STORE_FLAG_STATE_NRM(INT paramCoreVarsArrayPos)
	
	// Ensure the Core Vars Array position is legal
	IF (paramCoreVarsArrayPos = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_STORE_FLAG_STATE_CLF: The Core Variables Array Position parameter is illegal. MOVING ON TO NEXT COMMAND.")
		
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Get the Flow Flag to be modified
	FLOW_FLAG_IDS eFlagID = INT_TO_ENUM(FLOW_FLAG_IDS, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue1)
	
	// Ensure the flag to be modified is legal
	IF (eFlagID = FLOWFLAG_NONE)
	OR (eFlagID = MAX_FLOW_FLAG_IDS_NRM)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_STORE_FLAG_STATE: The FlowFlagID to be stored is illegal. MOVING ON TO NEXT COMMAND.")
		
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	
	// Retrieve the new flag state. Convert from INT back to BOOL.
	BOOL bNewState = FALSE
	IF g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue2 = 1
		bNewState = TRUE
	ENDIF
	
	// Output flow debug.
	#IF IS_DEBUG_BUILD
		IF IS_FLOW_CONSOLE_LOG_READY_TO_PRINT()
			CDEBUG1LN(DEBUG_FLOW_COMMANDS, "Store Flag State (FLAG=", GET_FLAG_DISPLAY_STRING_FROM_FLAG_ID(eFlagID), " NEW-STATE=", GET_BOOL_DISPLAY_STRING_FROM_BOOL(bNewState), ")")
		ENDIF
	#ENDIF
	
	// Set the Flow Flag to its new state.
	SET_MISSION_FLOW_FLAG_STATE(eFlagID, bNewState)
	
	RETURN NEXT_SEQUENTIAL_COMMAND
	
ENDFUNC
#endif
FUNC JUMP_LABEL_IDS PERFORM_STORE_FLAG_STATE(INT paramCoreVarsArrayPos)

	#if USE_CLF_DLC
		return PERFORM_STORE_FLAG_STATE_CLF(paramCoreVarsArrayPos)
	#endif
	#if USE_NRM_DLC
		return PERFORM_STORE_FLAG_STATE_NRM(paramCoreVarsArrayPos)
	#endif
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
	// Ensure the Core Vars Array position is legal
	IF (paramCoreVarsArrayPos = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_STORE_FLAG_STATE: The Core Variables Array Position parameter is illegal. MOVING ON TO NEXT COMMAND.")
		
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Get the Flow Flag to be modified
	FLOW_FLAG_IDS eFlagID = INT_TO_ENUM(FLOW_FLAG_IDS, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue1)
	
	// Ensure the flag to be modified is legal
	IF (eFlagID = FLOWFLAG_NONE)
	OR (eFlagID = MAX_FLOW_FLAG_IDS)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_STORE_FLAG_STATE: The FlowFlagID to be stored is illegal. MOVING ON TO NEXT COMMAND.")
		
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	
	// Retrieve the new flag state. Convert from INT back to BOOL.
	BOOL bNewState = FALSE
	IF g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue2 = 1
		bNewState = TRUE
	ENDIF
	
	// Output flow debug.
	#IF IS_DEBUG_BUILD
		IF IS_FLOW_CONSOLE_LOG_READY_TO_PRINT()
			CDEBUG1LN(DEBUG_FLOW_COMMANDS, "Store Flag State (FLAG=", GET_FLAG_DISPLAY_STRING_FROM_FLAG_ID(eFlagID), " NEW-STATE=", GET_BOOL_DISPLAY_STRING_FROM_BOOL(bNewState), ")")
		ENDIF
	#ENDIF
	
	// Set the Flow Flag to its new state.
	SET_MISSION_FLOW_FLAG_STATE(eFlagID, bNewState)
	
	RETURN NEXT_SEQUENTIAL_COMMAND
	#endif
	#endif
ENDFUNC


// -----------------------------------------------------------------------------------------------------------
// PURPOSE:	Perform the 'Store Int Value' flow command. Set the specified Flow Int to the required value.
//
// INPUT PARAMS:		paramCoreVarsArrayPos		Index into the Core Variables array
// RETURN VALUE:		JUMP_LABEL_IDS				The Jump Label to continue at
#if USE_CLF_DLC
FUNC JUMP_LABEL_IDS PERFORM_STORE_INT_VALUE_CLF(INT paramCoreVarsArrayPos)

	// Ensure the Core Vars Array position is legal
	IF (paramCoreVarsArrayPos = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_STORE_INT_VALUE: The Core Variables Array Position parameter is illegal. MOVING ON TO NEXT COMMAND.")
		
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Get the Flow Int to be modified
	FLOW_INT_IDS eIntID = INT_TO_ENUM(FLOW_INT_IDS, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue1)
	
	// Ensure the flag to be modified is legal
	IF (eIntID = FLOWINT_NONE)
	OR (eIntID = MAX_FLOW_INT_IDS_CLF)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_STORE_INT_VALUE: The FlowIntID to be stored is illegal. MOVING ON TO NEXT COMMAND.")
		
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Set the Flow Int to its new state
	INT iNewValue = g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue2
	
	// Output flow debug.
	#IF IS_DEBUG_BUILD
		IF IS_FLOW_CONSOLE_LOG_READY_TO_PRINT()
			CDEBUG1LN(DEBUG_FLOW_COMMANDS, "Store Int Value (INT=", GET_INT_DISPLAY_STRING_FROM_INT_ID(eIntID)," NEW-VALUE=", iNewValue, ")")
		ENDIF
	#ENDIF
			
	SET_MISSION_FLOW_INT_VALUE(eIntID, iNewValue)
	
	RETURN NEXT_SEQUENTIAL_COMMAND
	
ENDFUNC
#endif
#if USE_NRM_DLC
FUNC JUMP_LABEL_IDS PERFORM_STORE_INT_VALUE_NRM(INT paramCoreVarsArrayPos)

	// Ensure the Core Vars Array position is legal
	IF (paramCoreVarsArrayPos = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_STORE_INT_VALUE: The Core Variables Array Position parameter is illegal. MOVING ON TO NEXT COMMAND.")
		
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Get the Flow Int to be modified
	FLOW_INT_IDS eIntID = INT_TO_ENUM(FLOW_INT_IDS, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue1)
	
	// Ensure the flag to be modified is legal
	IF (eIntID = FLOWINT_NONE)
	OR (eIntID = MAX_FLOW_INT_IDS_NRM)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_STORE_INT_VALUE: The FlowIntID to be stored is illegal. MOVING ON TO NEXT COMMAND.")
		
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Set the Flow Int to its new state
	INT iNewValue = g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue2
	
	// Output flow debug.
	#IF IS_DEBUG_BUILD
		IF IS_FLOW_CONSOLE_LOG_READY_TO_PRINT()
			CDEBUG1LN(DEBUG_FLOW_COMMANDS, "Store Int Value (INT=", GET_INT_DISPLAY_STRING_FROM_INT_ID(eIntID)," NEW-VALUE=", iNewValue, ")")
		ENDIF
	#ENDIF
			
	SET_MISSION_FLOW_INT_VALUE(eIntID, iNewValue)
	
	RETURN NEXT_SEQUENTIAL_COMMAND
	
ENDFUNC
#endif
FUNC JUMP_LABEL_IDS PERFORM_STORE_INT_VALUE(INT paramCoreVarsArrayPos)

	#if USE_CLF_DLC
		return PERFORM_STORE_INT_VALUE_CLF(paramCoreVarsArrayPos)
	#endif
	#if USE_NRM_DLC
		return PERFORM_STORE_INT_VALUE_NRM(paramCoreVarsArrayPos)
	#endif
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
	// Ensure the Core Vars Array position is legal
	IF (paramCoreVarsArrayPos = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_STORE_INT_VALUE: The Core Variables Array Position parameter is illegal. MOVING ON TO NEXT COMMAND.")
		
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Get the Flow Int to be modified
	FLOW_INT_IDS eIntID = INT_TO_ENUM(FLOW_INT_IDS, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue1)
	
	// Ensure the flag to be modified is legal
	IF (eIntID = FLOWINT_NONE)
	OR (eIntID = MAX_FLOW_INT_IDS)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_STORE_INT_VALUE: The FlowIntID to be stored is illegal. MOVING ON TO NEXT COMMAND.")
		
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Set the Flow Int to its new state
	INT iNewValue = g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue2
	
	// Output flow debug.
	#IF IS_DEBUG_BUILD
		IF IS_FLOW_CONSOLE_LOG_READY_TO_PRINT()
			CDEBUG1LN(DEBUG_FLOW_COMMANDS, "Store Int Value (INT=", GET_INT_DISPLAY_STRING_FROM_INT_ID(eIntID)," NEW-VALUE=", iNewValue, ")")
		ENDIF
	#ENDIF
			
	SET_MISSION_FLOW_INT_VALUE(eIntID, iNewValue)
	
	RETURN NEXT_SEQUENTIAL_COMMAND
	#endif
	#endif
ENDFUNC


// -----------------------------------------------------------------------------------------------------------
// PURPOSE:	Perform the 'Store Bitset Bit State' flow command. Sets the specified Flow Bitset to its required state.
//
// INPUT PARAMS:		paramCoreVarsArrayPos		Index into the Core Variables array
// RETURN VALUE:		JUMP_LABEL_IDS				The Jump Label to continue at
#if USE_CLF_DLC
FUNC JUMP_LABEL_IDS PERFORM_STORE_BITSET_BIT_STATE_CLF(INT paramCoreVarsArrayPos)
	
	// Ensure the Core Vars Array position is legal
	IF (paramCoreVarsArrayPos = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_STORE_BITSET_BIT_STATE: The Core Variables Array Position parameter is illegal. MOVING ON TO NEXT COMMAND.")
		
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Get the Flow Bitset to be tested
	FLOW_BITSET_IDS eBitsetID = INT_TO_ENUM(FLOW_BITSET_IDS, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue1)
	
	// Ensure the bitset to be tested is legal
	IF (eBitsetID = FLOWBITSET_NONE)
	OR (eBitsetID = MAX_FLOW_BITSET_IDS_CLF)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_STORE_BITSET_BIT_STATE: The BitsetID is illegal. MOVING ON TO NEXT COMMAND.")
		
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Get the Bit to be tested
	INT iBit = g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue2
	
	// Ensure the bit to be tested is legal
	IF (iBit < 0)
	OR (iBit >= 32)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_STORE_BITSET_BIT_STATE: The Bit is illegal. MOVING ON TO NEXT COMMAND.")
		
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Retrieve the new state. Convert from INT back to BOOL.
	BOOL bNewState = FALSE
	IF g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue3 = 1
		bNewState = TRUE
	ENDIF
	
	// Output flow debug.
	#IF IS_DEBUG_BUILD
		IF IS_FLOW_CONSOLE_LOG_READY_TO_PRINT()
			CDEBUG1LN(DEBUG_FLOW_COMMANDS, "Store Bitset Bit State (BITSET=", GET_BITSET_DISPLAY_STRING_FROM_BITSET_ID(eBitsetID), " BIT=", iBit, " NEW-STATE=", GET_BOOL_DISPLAY_STRING_FROM_BOOL(bNewState), ")")
		ENDIF
	#ENDIF
	
	// Set the Flow Bitset Bit to its new state
	SET_MISSION_FLOW_BITSET_BIT_STATE(eBitsetID, iBit, bNewState)
	
	RETURN NEXT_SEQUENTIAL_COMMAND
	
ENDFUNC
#endif
#if USE_NRM_DLC
FUNC JUMP_LABEL_IDS PERFORM_STORE_BITSET_BIT_STATE_NRM(INT paramCoreVarsArrayPos)
	
	// Ensure the Core Vars Array position is legal
	IF (paramCoreVarsArrayPos = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_STORE_BITSET_BIT_STATE: The Core Variables Array Position parameter is illegal. MOVING ON TO NEXT COMMAND.")
		
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Get the Flow Bitset to be tested
	FLOW_BITSET_IDS eBitsetID = INT_TO_ENUM(FLOW_BITSET_IDS, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue1)
	
	// Ensure the bitset to be tested is legal
	IF (eBitsetID = FLOWBITSET_NONE)
	OR (eBitsetID = MAX_FLOW_BITSET_IDS_NRM)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_STORE_BITSET_BIT_STATE: The BitsetID is illegal. MOVING ON TO NEXT COMMAND.")
		
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Get the Bit to be tested
	INT iBit = g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue2
	
	// Ensure the bit to be tested is legal
	IF (iBit < 0)
	OR (iBit >= 32)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_STORE_BITSET_BIT_STATE: The Bit is illegal. MOVING ON TO NEXT COMMAND.")
		
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Retrieve the new state. Convert from INT back to BOOL.
	BOOL bNewState = FALSE
	IF g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue3 = 1
		bNewState = TRUE
	ENDIF
	
	// Output flow debug.
	#IF IS_DEBUG_BUILD
		IF IS_FLOW_CONSOLE_LOG_READY_TO_PRINT()
			CDEBUG1LN(DEBUG_FLOW_COMMANDS, "Store Bitset Bit State (BITSET=", GET_BITSET_DISPLAY_STRING_FROM_BITSET_ID(eBitsetID), " BIT=", iBit, " NEW-STATE=", GET_BOOL_DISPLAY_STRING_FROM_BOOL(bNewState), ")")
		ENDIF
	#ENDIF
	
	// Set the Flow Bitset Bit to its new state
	SET_MISSION_FLOW_BITSET_BIT_STATE(eBitsetID, iBit, bNewState)
	
	RETURN NEXT_SEQUENTIAL_COMMAND
	
ENDFUNC
#endif
FUNC JUMP_LABEL_IDS PERFORM_STORE_BITSET_BIT_STATE(INT paramCoreVarsArrayPos)
	
	#if USE_CLF_DLC
		return PERFORM_STORE_BITSET_BIT_STATE_CLF(paramCoreVarsArrayPos)
	#endif
	#if USE_NRM_DLC
		return PERFORM_STORE_BITSET_BIT_STATE_NRM(paramCoreVarsArrayPos)
	#endif	
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC	
	// Ensure the Core Vars Array position is legal
	IF (paramCoreVarsArrayPos = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_STORE_BITSET_BIT_STATE: The Core Variables Array Position parameter is illegal. MOVING ON TO NEXT COMMAND.")
		
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Get the Flow Bitset to be tested
	FLOW_BITSET_IDS eBitsetID = INT_TO_ENUM(FLOW_BITSET_IDS, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue1)
	
	// Ensure the bitset to be tested is legal
	IF (eBitsetID = FLOWBITSET_NONE)
	OR (eBitsetID = MAX_FLOW_BITSET_IDS)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_STORE_BITSET_BIT_STATE: The BitsetID is illegal. MOVING ON TO NEXT COMMAND.")
		
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Get the Bit to be tested
	INT iBit = g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue2
	
	// Ensure the bit to be tested is legal
	IF (iBit < 0)
	OR (iBit >= 32)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_STORE_BITSET_BIT_STATE: The Bit is illegal. MOVING ON TO NEXT COMMAND.")
		
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Retrieve the new state. Convert from INT back to BOOL.
	BOOL bNewState = FALSE
	IF g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue3 = 1
		bNewState = TRUE
	ENDIF
	
	// Output flow debug.
	#IF IS_DEBUG_BUILD
		IF IS_FLOW_CONSOLE_LOG_READY_TO_PRINT()
			CDEBUG1LN(DEBUG_FLOW_COMMANDS, "Store Bitset Bit State (BITSET=", GET_BITSET_DISPLAY_STRING_FROM_BITSET_ID(eBitsetID), " BIT=", iBit, " NEW-STATE=", GET_BOOL_DISPLAY_STRING_FROM_BOOL(bNewState), ")")
		ENDIF
	#ENDIF
	
	// Set the Flow Bitset Bit to its new state
	SET_MISSION_FLOW_BITSET_BIT_STATE(eBitsetID, iBit, bNewState)
	
	RETURN NEXT_SEQUENTIAL_COMMAND
	#endif
	#endif
ENDFUNC


// -----------------------------------------------------------------------------------------------------------
// PURPOSE:	Perform the 'Do Flag Jumps' flow command. Jump to a specific label based on the TRUE/FALSE state 
//			of a Flow Flag.
//
// INPUT PARAMS:		paramCoreVarsArrayPos		Index into the Core Variables array
// RETURN VALUE:		JUMP_LABEL_IDS				The Jump Label to continue at
//
// NOTES:	To perform the equivalent of a GTA IV 'Flag Wall' command, set the FALSE Jump to be STAY ON THIS COMMAND
//			and the TRUE Jump to be NEXT SEQUENTIAL COMMAND
#if USE_CLF_DLC
FUNC JUMP_LABEL_IDS PERFORM_DO_FLAG_JUMPS_CLF(INT paramCoreVarsArrayPos)
	
	// Ensure the Core Vars Array position is legal
	IF (paramCoreVarsArrayPos = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_DO_FLAG_JUMPS: The Core Variables Array Position parameter is illegal. MOVING ON TO NEXT COMMAND.")
		
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Get the Flow Flag to be tested
	FLOW_FLAG_IDS eFlagID = INT_TO_ENUM(FLOW_FLAG_IDS, (g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue1%FLOW_COMMAND_HASH_MODIFIER_MAGNITUDE))
	
	// Ensure the flag to be tested is legal
	IF (eFlagID = FLOWFLAG_NONE)
	OR (eFlagID = MAX_FLOW_FLAG_IDS_CLF)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_DO_FLAG_JUMPS: The FlowFlagID is illegal. MOVING ON TO NEXT COMMAND.")
		
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Get the jump labels.
	JUMP_LABEL_IDS eTrueJump = INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue2)
	JUMP_LABEL_IDS eFalseJump = INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue3)
	
	// Output flow debug.
	#IF IS_DEBUG_BUILD
		IF IS_FLOW_CONSOLE_LOG_READY_TO_PRINT()
			CDEBUG1LN(DEBUG_FLOW_COMMANDS, "Do Flag Jumps (FLAG=", GET_FLAG_DISPLAY_STRING_FROM_FLAG_ID(eFlagID), ", TRUEJUMP=", Get_Label_Display_String_From_Label_ID(eTrueJump), ", FALSEJUMP=", Get_Label_Display_String_From_Label_ID(eFalseJump), ")")
		ENDIF
	#ENDIF
	
	// Check the state of the Flow Flag
	IF (Get_Mission_Flow_Flag_State(eFlagID))
		// Perform the 'TRUE' jump
		RETURN eTrueJump
	ENDIF
	
	// Perform the 'FALSE' jump
	RETURN eFalseJump
	
ENDFUNC
#endif
#if USE_NRM_DLC
FUNC JUMP_LABEL_IDS PERFORM_DO_FLAG_JUMPS_NRM(INT paramCoreVarsArrayPos)
	
	// Ensure the Core Vars Array position is legal
	IF (paramCoreVarsArrayPos = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_DO_FLAG_JUMPS: The Core Variables Array Position parameter is illegal. MOVING ON TO NEXT COMMAND.")
		
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Get the Flow Flag to be tested
	FLOW_FLAG_IDS eFlagID = INT_TO_ENUM(FLOW_FLAG_IDS, (g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue1%FLOW_COMMAND_HASH_MODIFIER_MAGNITUDE))
	
	// Ensure the flag to be tested is legal
	IF (eFlagID = FLOWFLAG_NONE)
	OR (eFlagID = MAX_FLOW_FLAG_IDS_NRM)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_DO_FLAG_JUMPS: The FlowFlagID is illegal. MOVING ON TO NEXT COMMAND.")
		
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Get the jump labels.
	JUMP_LABEL_IDS eTrueJump = INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue2)
	JUMP_LABEL_IDS eFalseJump = INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue3)
	
	// Output flow debug.
	#IF IS_DEBUG_BUILD
		IF IS_FLOW_CONSOLE_LOG_READY_TO_PRINT()
			CDEBUG1LN(DEBUG_FLOW_COMMANDS, "Do Flag Jumps (FLAG=", GET_FLAG_DISPLAY_STRING_FROM_FLAG_ID(eFlagID), ", TRUEJUMP=", Get_Label_Display_String_From_Label_ID(eTrueJump), ", FALSEJUMP=", Get_Label_Display_String_From_Label_ID(eFalseJump), ")")
		ENDIF
	#ENDIF
	
	// Check the state of the Flow Flag
	IF (Get_Mission_Flow_Flag_State(eFlagID))
		// Perform the 'TRUE' jump
		RETURN eTrueJump
	ENDIF
	
	// Perform the 'FALSE' jump
	RETURN eFalseJump
	
ENDFUNC
#endif
FUNC JUMP_LABEL_IDS PERFORM_DO_FLAG_JUMPS(INT paramCoreVarsArrayPos)
	
	
	#if USE_CLF_DLC
		return PERFORM_DO_FLAG_JUMPS_CLF( paramCoreVarsArrayPos)
	#endif
	#if USE_NRM_DLC
		return PERFORM_DO_FLAG_JUMPS_NRM( paramCoreVarsArrayPos)
	#endif	
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC	
	// Ensure the Core Vars Array position is legal
	IF (paramCoreVarsArrayPos = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_DO_FLAG_JUMPS: The Core Variables Array Position parameter is illegal. MOVING ON TO NEXT COMMAND.")
		
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Get the Flow Flag to be tested
	FLOW_FLAG_IDS eFlagID = INT_TO_ENUM(FLOW_FLAG_IDS, (g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue1%FLOW_COMMAND_HASH_MODIFIER_MAGNITUDE))
	
	// Ensure the flag to be tested is legal
	IF (eFlagID = FLOWFLAG_NONE)
	OR (eFlagID = MAX_FLOW_FLAG_IDS)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_DO_FLAG_JUMPS: The FlowFlagID is illegal. MOVING ON TO NEXT COMMAND.")
		
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Get the jump labels.
	JUMP_LABEL_IDS eTrueJump = INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue2)
	JUMP_LABEL_IDS eFalseJump = INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue3)
	
	// Output flow debug.
	#IF IS_DEBUG_BUILD
		IF IS_FLOW_CONSOLE_LOG_READY_TO_PRINT()
			CDEBUG1LN(DEBUG_FLOW_COMMANDS, "Do Flag Jumps (FLAG=", GET_FLAG_DISPLAY_STRING_FROM_FLAG_ID(eFlagID), ", TRUEJUMP=", Get_Label_Display_String_From_Label_ID(eTrueJump), ", FALSEJUMP=", Get_Label_Display_String_From_Label_ID(eFalseJump), ")")
		ENDIF
	#ENDIF
	
	// Check the state of the Flow Flag
	IF (Get_Mission_Flow_Flag_State(eFlagID))
		// Perform the 'TRUE' jump
		RETURN eTrueJump
	ENDIF
	
	// Perform the 'FALSE' jump
	RETURN eFalseJump
	#endif
	#endif
ENDFUNC


// -----------------------------------------------------------------------------------------------------------
// PURPOSE:	Perform the 'Do Int Jumps' flow command. Jump to a specific label based on a comparison of the 
//			Int Value with a comparison int.
//
// INPUT PARAMS:		paramCoreVarsArrayPos		Index into the Core Variables array
// RETURN VALUE:		JUMP_LABEL_IDS				The Jump Label to continue at
//
// NOTES:	To perform the equivalent of a GTA IV 'Flag Wall' command, set the NOTEQUAL Jump to be STAY ON THIS COMMAND
//			and the EQUAL Jumpt to be NEXT SEQUENTIAL COMMAND

FUNC JUMP_LABEL_IDS PERFORM_DO_INT_JUMPS(INT paramCoreVarsArrayPos)

	// Ensure the Core Vars Array position is legal
	IF (paramCoreVarsArrayPos = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_DO_INT_JUMPS: The Core Variables Array Position parameter is illegal. MOVING ON TO NEXT COMMAND.")
		
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Get the Flow Int to be tested
	FLOW_INT_IDS eIntID = INT_TO_ENUM(FLOW_INT_IDS, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue1)
	
	// Ensure the INT to be tested is legal
	IF (eIntID = FLOWINT_NONE)
	OR (eIntID = MAX_FLOW_INT_IDS)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_DO_INT_JUMPS: The FlowIntID is illegal. MOVING ON TO NEXT COMMAND.")
		
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Compare the Int ID value with the comparison int
	INT iComparisonInt = g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue2
	
	// Get the jump labels.
	JUMP_LABEL_IDS eEqualJump = INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue3)
	JUMP_LABEL_IDS eNotEqualJump = INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue4)
	
	// Output flow debug.
	#IF IS_DEBUG_BUILD
		IF IS_FLOW_CONSOLE_LOG_READY_TO_PRINT()
			CDEBUG1LN(DEBUG_FLOW_COMMANDS, "Do Int Jumps (INT=", GET_INT_DISPLAY_STRING_FROM_INT_ID(eIntID), ", COMPARE=", iComparisonInt, ", EQUALJUMP=", Get_Label_Display_String_From_Label_ID(eEqualJump), ", NOTEQUALJUMP=", Get_Label_Display_String_From_Label_ID(eNotEqualJump),")")
		ENDIF
	#ENDIF
	
	IF (Get_Mission_Flow_Int_Value(eIntID) = iComparisonInt)
		// Perform the 'EQUAL' jump
		RETURN eEqualJump
	ENDIF
	
	// Perform the 'NOTEQUAL' jump
	RETURN eNotEqualJump
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------
// PURPOSE:	Perform the 'Do Bitset Jumps' flow command. Jump to a specific label based on the TRUE/FALSE state 
//			of a Flow Bitset Bit.
//
// INPUT PARAMS:		paramCoreVarsArrayPos		Index into the Core Variables array
// RETURN VALUE:		JUMP_LABEL_IDS				The Jump Label to continue at
//
// NOTES:	To perform the equivalent of a GTA IV 'Flag Wall' command, set the FALSE Jump to be STAY ON THIS COMMAND
//			and the TRUE Jump to be NEXT SEQUENTIAL COMMAND

FUNC JUMP_LABEL_IDS PERFORM_DO_BITSET_JUMPS(INT paramCoreVarsArrayPos)
	
	// Ensure the Core Vars Array position is legal
	IF (paramCoreVarsArrayPos = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_DO_BITSET_JUMPS: The Core Variables Array Position parameter is illegal. MOVING ON TO NEXT COMMAND.")
		
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Get the Flow Bitset to be tested
	FLOW_BITSET_IDS eBitsetID = INT_TO_ENUM(FLOW_BITSET_IDS, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue1)
	
	// Ensure the bitset to be tested is legal
	IF (eBitsetID = FLOWBITSET_NONE)
	OR (eBitsetID = MAX_FLOW_BITSET_IDS)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_DO_BITSET_JUMPS: The BitsetID is illegal. MOVING ON TO NEXT COMMAND.")
		
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Get the Bit to be tested
	INT iBit = g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue2
	
	// Ensure the bit to be tested is legal
	IF (iBit < 0)
	OR (iBit >= 32)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_DO_BITSET_JUMPS: The Bit is illegal. MOVING ON TO NEXT COMMAND.")
		
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Get the jump labels.
	JUMP_LABEL_IDS eTrueJump = INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue3)
	JUMP_LABEL_IDS eFalseJump = INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue4)
	
	// Output flow debug.
	#IF IS_DEBUG_BUILD
		IF IS_FLOW_CONSOLE_LOG_READY_TO_PRINT()
			CDEBUG1LN(DEBUG_FLOW_COMMANDS, "Do Bitset Jumps (BITSET=", GET_BITSET_DISPLAY_STRING_FROM_BITSET_ID(eBitsetID), ", BIT=", iBit, ", TRUEJUMP=", Get_Label_Display_String_From_Label_ID(eTrueJump), ", FALSEJUMP=", Get_Label_Display_String_From_Label_ID(eFalseJump), ")")
		ENDIF
	#ENDIF
	
	// Check the state of the Flow Bitset Bit
	IF (Get_Mission_Flow_Bitset_Bit_State(eBitsetID, iBit))
		// Perform the 'TRUE' jump
		RETURN eTrueJump
	ENDIF
	
	// Perform the 'FALSE' jump
	RETURN eFalseJump
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------
// PURPOSE: Perform the 'Do Strand Killed Jumps' flow command. Jump to a specific label based on the whether 
//			or not a strand has terminated.
//
// INPUT PARAMS:		paramMiscellaneousVarsArrayPos		Index into the Miscellaneous Variables array
// RETURN VALUE:		JUMP_LABEL_IDS						The Jump Label to continue at
//
// NOTES:	To perform the equivalent of a GTA IV 'Wall' command, set the NOTKILLED Jump to be STAY ON THIS COMMAND
//				and the KILLED Jump to be NEXT SEQUENTIAL COMMAND
#if USE_CLF_DLC
FUNC JUMP_LABEL_IDS PERFORM_DO_STRAND_KILLED_JUMPS_CLF(INT paramCoreVarsArrayPos)
	
	// Ensure the Core Vars Array position is legal
	IF (paramCoreVarsArrayPos = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_DO_STRAND_KILLED_JUMPS: The Core Variables Array Position parameter is illegal. MOVING ON TO NEXT COMMAND.")
		
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Get the Strand to be tested
	STRANDS eStrandID = INT_TO_ENUM(STRANDS, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue1)
	
	// Ensure the strand to be tested is legal
	IF (eStrandID = STRAND_NONE)
	OR (eStrandID = MAX_STRANDS_CLF)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_DO_STRAND_KILLED_JUMPS: The StrandID is illegal. MOVING ON TO NEXT COMMAND.")
		
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Get the jump labels.
	JUMP_LABEL_IDS eKilledJump = INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue2)
	JUMP_LABEL_IDS eNotKilledJump = INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue3)
	
	// Output flow debug.
	#IF IS_DEBUG_BUILD
		IF IS_FLOW_CONSOLE_LOG_READY_TO_PRINT()
			CDEBUG1LN(DEBUG_FLOW_COMMANDS, "Do Strand Killed Jumps (STRAND=", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(eStrandID), ", KILLEDJUMP=", Get_Label_Display_String_From_Label_ID(eKilledJump), ", NOTKILLEDJUMP=", Get_Label_Display_String_From_Label_ID(eNotKilledJump),")")
		ENDIF
	#ENDIF
	
	// Check the termination state of the Strand
	IF (IS_BIT_SET(g_savedGlobalsClifford.sFlow.strandSavedVars[eStrandID].savedBitflags, SAVED_BITS_STRAND_TERMINATED))
		// Perform the 'KILLED' jump
		RETURN eKilledJump
	ENDIF
	
	// Perform the 'NOTKILLED' jump
	RETURN eNotKilledJump
	
ENDFUNC
#endif
#if USE_NRM_DLC
FUNC JUMP_LABEL_IDS PERFORM_DO_STRAND_KILLED_JUMPS_NRM(INT paramCoreVarsArrayPos)
	
	// Ensure the Core Vars Array position is legal
	IF (paramCoreVarsArrayPos = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_DO_STRAND_KILLED_JUMPS: The Core Variables Array Position parameter is illegal. MOVING ON TO NEXT COMMAND.")
		
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Get the Strand to be tested
	STRANDS eStrandID = INT_TO_ENUM(STRANDS, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue1)
	
	// Ensure the strand to be tested is legal
	IF (eStrandID = STRAND_NONE)
	OR (eStrandID = MAX_STRANDS_NRM)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_DO_STRAND_KILLED_JUMPS: The StrandID is illegal. MOVING ON TO NEXT COMMAND.")
		
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Get the jump labels.
	JUMP_LABEL_IDS eKilledJump = INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue2)
	JUMP_LABEL_IDS eNotKilledJump = INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue3)
	
	// Output flow debug.
	#IF IS_DEBUG_BUILD
		IF IS_FLOW_CONSOLE_LOG_READY_TO_PRINT()
			CDEBUG1LN(DEBUG_FLOW_COMMANDS, "Do Strand Killed Jumps (STRAND=", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(eStrandID), ", KILLEDJUMP=", Get_Label_Display_String_From_Label_ID(eKilledJump), ", NOTKILLEDJUMP=", Get_Label_Display_String_From_Label_ID(eNotKilledJump),")")
		ENDIF
	#ENDIF
	
	// Check the termination state of the Strand
	IF (IS_BIT_SET(g_savedGlobalsnorman.sFlow.strandSavedVars[eStrandID].savedBitflags, SAVED_BITS_STRAND_TERMINATED))
		// Perform the 'KILLED' jump
		RETURN eKilledJump
	ENDIF
	
	// Perform the 'NOTKILLED' jump
	RETURN eNotKilledJump
	
ENDFUNC
#endif

FUNC JUMP_LABEL_IDS PERFORM_DO_STRAND_KILLED_JUMPS(INT paramCoreVarsArrayPos)
	
	#if USE_CLF_DLC
		return PERFORM_DO_STRAND_KILLED_JUMPS_CLF(paramCoreVarsArrayPos)
	#endif	
	#if USE_NRM_DLC
		return PERFORM_DO_STRAND_KILLED_JUMPS_NRM(paramCoreVarsArrayPos)
	#endif	
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
	// Ensure the Core Vars Array position is legal
	IF (paramCoreVarsArrayPos = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_DO_STRAND_KILLED_JUMPS: The Core Variables Array Position parameter is illegal. MOVING ON TO NEXT COMMAND.")
		
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Get the Strand to be tested
	STRANDS eStrandID = INT_TO_ENUM(STRANDS, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue1)
	
	// Ensure the strand to be tested is legal
	IF (eStrandID = STRAND_NONE)
	OR (eStrandID = MAX_STRANDS)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_DO_STRAND_KILLED_JUMPS: The StrandID is illegal. MOVING ON TO NEXT COMMAND.")
		
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Get the jump labels.
	JUMP_LABEL_IDS eKilledJump = INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue2)
	JUMP_LABEL_IDS eNotKilledJump = INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue3)
	
	// Output flow debug.
	#IF IS_DEBUG_BUILD
		IF IS_FLOW_CONSOLE_LOG_READY_TO_PRINT()
			CDEBUG1LN(DEBUG_FLOW_COMMANDS, "Do Strand Killed Jumps (STRAND=", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(eStrandID), ", KILLEDJUMP=", Get_Label_Display_String_From_Label_ID(eKilledJump), ", NOTKILLEDJUMP=", Get_Label_Display_String_From_Label_ID(eNotKilledJump),")")
		ENDIF
	#ENDIF
	
	// Check the termination state of the Strand
	IF (IS_BIT_SET(g_savedGlobals.sFlow.strandSavedVars[eStrandID].savedBitflags, SAVED_BITS_STRAND_TERMINATED))
		// Perform the 'KILLED' jump
		RETURN eKilledJump
	ENDIF
	
	// Perform the 'NOTKILLED' jump
	RETURN eNotKilledJump
	#endif
	#endif
ENDFUNC



// ===========================================================================================================
// 		Strand-to-strand Synchronisation Command Processing
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
// PURPOSE:	Perform the 'Await Synchronisation' flow command. Wait for the specified synchronisation flag 
//			to be set.
//
// INPUT PARAMS:		paramIndex			The command storage index. This command diretly stores variable data to this index.
// RETURN VALUE:		JUMP_LABEL_IDS		The Jump Label to continue at
#if USE_CLF_DLC
FUNC JUMP_LABEL_IDS PERFORM_AWAIT_SYNCHRONISATION_CLF(INT paramIndex)
	
	// Get the synchronisation flag to be modified
	SYNCHRONIZATION_IDS eSyncID = INT_TO_ENUM(SYNCHRONIZATION_IDS, (paramIndex%FLOW_COMMAND_HASH_MODIFIER_MAGNITUDE))
	
	// Ensure the sync to be activated is legal
	IF (eSyncID = SYNC_NONE)
	OR (eSyncID = MAX_SYNCHRONISATION_IDS_CLF)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_AWAIT_SYNCHRONISATION: The SyncID to be sent is illegal. MOVING ON TO NEXT COMMAND.")
		
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Check if the synchronisation flag has been set
	IF NOT (g_savedGlobalsClifford.sFlow.controls.syncIDs[eSyncID])
		// ...no, so keep waiting for it
		RETURN STAY_ON_THIS_COMMAND
	ENDIF
	
	// Output flow debug.
	#IF IS_DEBUG_BUILD
		IF IS_FLOW_CONSOLE_LOG_READY_TO_PRINT()
			CDEBUG1LN(DEBUG_FLOW_COMMANDS, "Await Synchronisation (SYNC: ", GET_SYNCHRONISATION_DISPLAY_STRING_FROM_SYNC_ID(eSyncID), ")")
		ENDIF
	#ENDIF
	
	// Synchronisation has been received
	RETURN NEXT_SEQUENTIAL_COMMAND
	
ENDFUNC
#endif
#if USE_NRM_DLC
FUNC JUMP_LABEL_IDS PERFORM_AWAIT_SYNCHRONISATION_NRM(INT paramIndex)
	
	// Get the synchronisation flag to be modified
	SYNCHRONIZATION_IDS eSyncID = INT_TO_ENUM(SYNCHRONIZATION_IDS, (paramIndex%FLOW_COMMAND_HASH_MODIFIER_MAGNITUDE))
	
	// Ensure the sync to be activated is legal
	IF (eSyncID = SYNC_NONE)
	OR (eSyncID = MAX_SYNCHRONISATION_IDS_NRM)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_AWAIT_SYNCHRONISATION: The SyncID to be sent is illegal. MOVING ON TO NEXT COMMAND.")
		
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Check if the synchronisation flag has been set
	IF NOT (g_savedGlobalsnorman.sFlow.controls.syncIDs[eSyncID])
		// ...no, so keep waiting for it
		RETURN STAY_ON_THIS_COMMAND
	ENDIF
	
	// Output flow debug.
	#IF IS_DEBUG_BUILD
		IF IS_FLOW_CONSOLE_LOG_READY_TO_PRINT()
			CDEBUG1LN(DEBUG_FLOW_COMMANDS, "Await Synchronisation (SYNC: ", GET_SYNCHRONISATION_DISPLAY_STRING_FROM_SYNC_ID(eSyncID), ")")
		ENDIF
	#ENDIF
	
	// Synchronisation has been received
	RETURN NEXT_SEQUENTIAL_COMMAND
	
ENDFUNC
#endif
FUNC JUMP_LABEL_IDS PERFORM_AWAIT_SYNCHRONISATION(INT paramIndex)
	
	#if USE_CLF_DLC
		return PERFORM_AWAIT_SYNCHRONISATION_CLF(paramIndex)
	#endif	
	#if USE_NRM_DLC
		return PERFORM_AWAIT_SYNCHRONISATION_NRM(paramIndex)
	#endif	
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
	// Get the synchronisation flag to be modified
	SYNCHRONIZATION_IDS eSyncID = INT_TO_ENUM(SYNCHRONIZATION_IDS, (paramIndex%FLOW_COMMAND_HASH_MODIFIER_MAGNITUDE))
	
	// Ensure the sync to be activated is legal
	IF (eSyncID = SYNC_NONE)
	OR (eSyncID = MAX_SYNCHRONISATION_IDS)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_AWAIT_SYNCHRONISATION: The SyncID to be sent is illegal. MOVING ON TO NEXT COMMAND.")
		
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Check if the synchronisation flag has been set
	IF NOT (g_savedGlobals.sFlow.controls.syncIDs[eSyncID])
		// ...no, so keep waiting for it
		RETURN STAY_ON_THIS_COMMAND
	ENDIF
	
	// Output flow debug.
	#IF IS_DEBUG_BUILD
		IF IS_FLOW_CONSOLE_LOG_READY_TO_PRINT()
			CDEBUG1LN(DEBUG_FLOW_COMMANDS, "Await Synchronisation (SYNC: ", GET_SYNCHRONISATION_DISPLAY_STRING_FROM_SYNC_ID(eSyncID), ")")
		ENDIF
	#ENDIF
	
	// Synchronisation has been received
	RETURN NEXT_SEQUENTIAL_COMMAND
	#endif
	#endif
ENDFUNC


// -----------------------------------------------------------------------------------------------------------
// PURPOSE:	Perform the 'Send Synchronisation' flow command. Set the specified synchronisation flag.
//
// INPUT PARAMS:		paramIndex			The command storage index. This command diretly stores variable data to this index.
// RETURN VALUE:		JUMP_LABEL_IDS		The Jump Label to continue at
#if USE_CLF_DLC
FUNC JUMP_LABEL_IDS PERFORM_SEND_SYNCHRONISATION_CLF(INT paramIndex)
	
	// Get the synchronisation flag to be modified
	SYNCHRONIZATION_IDS eSyncID = INT_TO_ENUM(SYNCHRONIZATION_IDS, (paramIndex%FLOW_COMMAND_HASH_MODIFIER_MAGNITUDE))
	
	// Ensure the sync to be activated is legal
	IF (eSyncID = SYNC_NONE)
	OR (eSyncID = MAX_SYNCHRONISATION_IDS_CLF)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_SEND_SYNCHRONISATION: The SyncID to be sent is illegal. MOVING ON TO NEXT COMMAND.")
		
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Output some information to the console log if the synchronisation flag has already been set
	// NOTE: Don't assert because this may not be an error
	#IF IS_DEBUG_BUILD
		IF (g_savedGlobalsClifford.sFlow.controls.syncIDs[eSyncID])
			CDEBUG1LN(DEBUG_FLOW_COMMANDS, "Flow controller sending Synchronisation, but it has already been set. [SYNC = ",Get_Synchronisation_Display_String_From_Sync_ID(eSyncID))
		ENDIF
	#ENDIF
	
	// Output flow debug.
	#IF IS_DEBUG_BUILD
		IF IS_FLOW_CONSOLE_LOG_READY_TO_PRINT()
			CDEBUG1LN(DEBUG_FLOW_COMMANDS, "Send Synchronisation (SYNC: ", GET_SYNCHRONISATION_DISPLAY_STRING_FROM_SYNC_ID(eSyncID), ")")
		ENDIF
	#ENDIF
	
	// Set the Synchronisation flag
	g_savedGlobalsClifford.sFlow.controls.syncIDs[eSyncID] = TRUE
	
	RETURN NEXT_SEQUENTIAL_COMMAND
	
ENDFUNC
#endif
#if USE_NRM_DLC
FUNC JUMP_LABEL_IDS PERFORM_SEND_SYNCHRONISATION_NRM(INT paramIndex)
	
	// Get the synchronisation flag to be modified
	SYNCHRONIZATION_IDS eSyncID = INT_TO_ENUM(SYNCHRONIZATION_IDS, (paramIndex%FLOW_COMMAND_HASH_MODIFIER_MAGNITUDE))
	
	// Ensure the sync to be activated is legal
	IF (eSyncID = SYNC_NONE)
	OR (eSyncID = MAX_SYNCHRONISATION_IDS_NRM)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_SEND_SYNCHRONISATION: The SyncID to be sent is illegal. MOVING ON TO NEXT COMMAND.")
		
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Output some information to the console log if the synchronisation flag has already been set
	// NOTE: Don't assert because this may not be an error
	#IF IS_DEBUG_BUILD
		IF (g_savedGlobalsnorman.sFlow.controls.syncIDs[eSyncID])
			CDEBUG1LN(DEBUG_FLOW_COMMANDS, "Flow controller sending Synchronisation, but it has already been set. [SYNC = ",Get_Synchronisation_Display_String_From_Sync_ID(eSyncID))
		ENDIF
	#ENDIF
	
	// Output flow debug.
	#IF IS_DEBUG_BUILD
		IF IS_FLOW_CONSOLE_LOG_READY_TO_PRINT()
			CDEBUG1LN(DEBUG_FLOW_COMMANDS, "Send Synchronisation (SYNC: ", GET_SYNCHRONISATION_DISPLAY_STRING_FROM_SYNC_ID(eSyncID), ")")
		ENDIF
	#ENDIF
	
	// Set the Synchronisation flag
	g_savedGlobalsnorman.sFlow.controls.syncIDs[eSyncID] = TRUE
	
	RETURN NEXT_SEQUENTIAL_COMMAND
	
ENDFUNC
#endif
FUNC JUMP_LABEL_IDS PERFORM_SEND_SYNCHRONISATION(INT paramIndex)
	
	#if USE_CLF_DLC
		return PERFORM_SEND_SYNCHRONISATION_CLF(paramIndex)
	#endif	
	
	#if USE_NRM_DLC
		return PERFORM_SEND_SYNCHRONISATION_NRM(paramIndex)
	#endif
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
	// Get the synchronisation flag to be modified
	SYNCHRONIZATION_IDS eSyncID = INT_TO_ENUM(SYNCHRONIZATION_IDS, (paramIndex%FLOW_COMMAND_HASH_MODIFIER_MAGNITUDE))
	
	// Ensure the sync to be activated is legal
	IF (eSyncID = SYNC_NONE)
	OR (eSyncID = MAX_SYNCHRONISATION_IDS)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_SEND_SYNCHRONISATION: The SyncID to be sent is illegal. MOVING ON TO NEXT COMMAND.")
		
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Output some information to the console log if the synchronisation flag has already been set
	// NOTE: Don't assert because this may not be an error
	#IF IS_DEBUG_BUILD
		IF (g_savedGlobals.sFlow.controls.syncIDs[eSyncID])
			CDEBUG1LN(DEBUG_FLOW_COMMANDS, "Flow controller sending Synchronisation, but it has already been set. [SYNC = ",Get_Synchronisation_Display_String_From_Sync_ID(eSyncID))
		ENDIF
	#ENDIF
	
	// Output flow debug.
	#IF IS_DEBUG_BUILD
		IF IS_FLOW_CONSOLE_LOG_READY_TO_PRINT()
			CDEBUG1LN(DEBUG_FLOW_COMMANDS, "Send Synchronisation (SYNC: ", GET_SYNCHRONISATION_DISPLAY_STRING_FROM_SYNC_ID(eSyncID), ")")
		ENDIF
	#ENDIF
	
	// Set the Synchronisation flag
	g_savedGlobals.sFlow.controls.syncIDs[eSyncID] = TRUE
	
	RETURN NEXT_SEQUENTIAL_COMMAND
	#endif
	#endif
ENDFUNC



// ===========================================================================================================
// 		Time Management Command Processing
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
// PURPOSE:	Perform the 'Set Game Clock' flow command. Set the in-game clock to a specific time and/or date.
//
// INPUT PARAMS:		paramCoreVarsArrayPos		Index into the Core Variables array
// RETURN VALUE:		JUMP_LABEL_IDS				The Jump Label to continue at

FUNC JUMP_LABEL_IDS PERFORM_SET_GAME_CLOCK(INT paramCoreVarsArrayPos)
	
	// Ensure the Core Vars Array position is legal
	IF (paramCoreVarsArrayPos = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_SET_GAME_CLOCK: The Core Variables Array Position parameter is illegal. MOVING ON TO NEXT COMMAND.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Get the bitset containing which clock fields are to be set.
	INT iFieldsToBeSetBitset = g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue2
	
	// Reconstruct a TimeOfDay struct using the two stored int values.
	TIMEOFDAY sNewClockValues = INT_TO_ENUM( TIMEOFDAY, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue1)
	
	// Set the hour field if its bit is flagged.
	IF IS_BIT_SET(iFieldsToBeSetBitset, 0)
		SET_CLOCK_TIME(GET_TIMEOFDAY_HOUR(sNewClockValues), GET_CLOCK_MINUTES(), GET_CLOCK_SECONDS())
	ENDIF
	// Set the minute field if its bit is flagged.
	IF IS_BIT_SET(iFieldsToBeSetBitset, 1)
		SET_CLOCK_TIME(GET_CLOCK_HOURS(), GET_TIMEOFDAY_MINUTE(sNewClockValues), GET_CLOCK_SECONDS())
	ENDIF
	// Set the second field if its bit is flagged.
	IF IS_BIT_SET(iFieldsToBeSetBitset, 2)
		SET_CLOCK_TIME(GET_CLOCK_HOURS(), GET_CLOCK_MINUTES(), GET_TIMEOFDAY_SECOND(sNewClockValues))
	ENDIF
	// Set the day field if its bit is flagged.
	IF IS_BIT_SET(iFieldsToBeSetBitset, 3)
		SET_CLOCK_DATE(GET_TIMEOFDAY_DAY(sNewClockValues), GET_CLOCK_MONTH(), GET_CLOCK_YEAR())
	ENDIF
	// Set the month field if its bit is flagged.
	IF IS_BIT_SET(iFieldsToBeSetBitset, 4)
		SET_CLOCK_DATE(GET_CLOCK_DAY_OF_MONTH(), INT_TO_ENUM(MONTH_OF_YEAR, GET_TIMEOFDAY_MONTH(sNewClockValues)), GET_CLOCK_YEAR())
	ENDIF
	// Set the year field if its bit is flagged.
	IF IS_BIT_SET(iFieldsToBeSetBitset, 5)
		SET_CLOCK_DATE(GET_CLOCK_DAY_OF_MONTH(), GET_CLOCK_MONTH(), GET_TIMEOFDAY_YEAR(sNewClockValues))
	ENDIF
	
	// Output flow debug.
	#IF IS_DEBUG_BUILD
		IF IS_FLOW_CONSOLE_LOG_READY_TO_PRINT()
			CDEBUG1LN(DEBUG_FLOW_COMMANDS, "Set Game Clock (HOUR: ", GET_TIMEOFDAY_HOUR(sNewClockValues), ", MIN: ", GET_TIMEOFDAY_MINUTE(sNewClockValues), ", SEC: ", GET_TIMEOFDAY_SECOND(sNewClockValues), ", DAY: ", GET_TIMEOFDAY_DAY(sNewClockValues), ", MON: ", GET_TIMEOFDAY_MONTH(sNewClockValues), ", YEAR: ", GET_TIMEOFDAY_YEAR(sNewClockValues), ")")
		ENDIF
	#ENDIF
	
	RETURN NEXT_SEQUENTIAL_COMMAND
ENDFUNC


// -----------------------------------------------------------------------------------------------------------
// PURPOSE:	Perform the 'Wait In Game Time' flow command. Set a strand to wait for a period of time measured 
//			by the in-game clock.
//
// INPUT PARAMS:		paramStrandID				The strand that this command is operating on
//						paramCoreVarsArrayPos		Index into the Core Variables array
// RETURN VALUE:		JUMP_LABEL_IDS				The Jump Label to continue at

FUNC JUMP_LABEL_IDS PERFORM_WAIT_IN_GAME_TIME(STRANDS paramStrandID, INT paramCoreVarsArrayPos)

	// Ensure the Core Vars Array position is legal
	IF (paramCoreVarsArrayPos = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_WAIT_IN_GAME_TIME: The Core Variables Array Position parameter is illegal. MOVING ON TO NEXT COMMAND.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// If this strand doesn't have a timer set then we need to store the command's TIMEOFDAY.
	IF NOT g_flowUnsaved.bStrandTimerSet[paramStrandID]
	
		// Store current time of day.
		g_flowUnsaved.sStrandInGameTimer[paramStrandID] = GET_CURRENT_TIMEOFDAY()
		
		// Retrieve the time to wait values.
		INT iSecondsToWait 	=  g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue3
		INT iMinutesToWait 	= g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue2
		INT iHoursToWait 	= g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue1
		

		/*
		PRINTLN("\nCOMMAND: Wait In Game Time (HOURS=", iHoursToWait, " MINS=", iMinutesToWait, " SECS=", iSecondsToWait, ")\n")
		
		
		//time comparison check
		PRINTLN("\nCheck, getting current back from time system before shift\n(HOURS=", GET_TIMEOFDAY_HOUR(g_flowUnsaved.sStrandInGameTimer[paramStrandID]), 
		" MINS=", GET_TIMEOFDAY_MINUTE(g_flowUnsaved.sStrandInGameTimer[paramStrandID]), 
		" SECS=", GET_TIMEOFDAY_SECOND(g_flowUnsaved.sStrandInGameTimer[paramStrandID]), 
		" YEARS=", GET_TIMEOFDAY_YEAR(g_flowUnsaved.sStrandInGameTimer[paramStrandID]), ")\n")
		*/
		ADD_TIME_TO_TIMEOFDAY(g_flowUnsaved.sStrandInGameTimer[paramStrandID], iSecondsToWait, iMinutesToWait, iHoursToWait, 0, 0, 0)
		g_flowUnsaved.bStrandTimerSet[paramStrandID] = TRUE

		/*
		PRINTLN("\nCheck, getting current back from time system after shift\n(HOURS=", GET_TIMEOFDAY_HOUR(g_flowUnsaved.sStrandInGameTimer[paramStrandID]), 
		" MINS=", GET_TIMEOFDAY_MINUTE(g_flowUnsaved.sStrandInGameTimer[paramStrandID]), 
		" SECS=", GET_TIMEOFDAY_SECOND(g_flowUnsaved.sStrandInGameTimer[paramStrandID]), 
		" YEARS=", GET_TIMEOFDAY_YEAR(g_flowUnsaved.sStrandInGameTimer[paramStrandID]), ")\n")
		*/
		// Output flow debug.
		#IF IS_DEBUG_BUILD
			IF IS_FLOW_CONSOLE_LOG_READY_TO_PRINT()
				CDEBUG1LN(DEBUG_FLOW_COMMANDS, "Wait In Game Time (HOURS=", iHoursToWait, " MINS=", iMinutesToWait, " SECS=", iSecondsToWait, ")")
			ENDIF
		#ENDIF
		
	// Timer set. Check if the game clock has advanced passed the stored TIMEOFDAY.
	ELSE
		
		IF IS_TIMEOFDAY_AFTER_TIMEOFDAY(GET_CURRENT_TIMEOFDAY(), g_flowUnsaved.sStrandInGameTimer[paramStrandID])
			g_flowUnsaved.bStrandTimerSet[paramStrandID] = FALSE
			RETURN NEXT_SEQUENTIAL_COMMAND
		ENDIF
		
	ENDIF
	
	RETURN STAY_ON_THIS_COMMAND
ENDFUNC


// -----------------------------------------------------------------------------------------------------------
// PURPOSE:	Perform the 'Wait In Real Time' flow command. Set a strand to wait for a period of time measured 
//			in real world time.
//
// INPUT PARAMS:		paramStrandID		The strand that this command is operating on.
//						paramIndex			The command storage index. This command diretly stores variable data to this index.
// RETURN VALUE:		JUMP_LABEL_IDS		The Jump Label to continue at.

FUNC JUMP_LABEL_IDS PERFORM_WAIT_IN_REAL_TIME(STRANDS paramStrandID, INT paramIndex)
	
	// If this strand hasn't got a timer set then we need to store the clock time we are going to wait until.
	IF NOT g_flowUnsaved.bStrandTimerSet[paramStrandID]
	
		// Ensure the wait time is positive.
		IF (paramIndex < 0)
			CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_WAIT_IN_REAL_TIME: Invalid wait time passed. Time was a negative value.")
			RETURN NEXT_SEQUENTIAL_COMMAND
		ENDIF
	
		g_flowUnsaved.iStrandTimer[paramStrandID] = GET_GAME_TIMER() + paramIndex // Wait time directly stored in index.
		g_flowUnsaved.bStrandTimerSet[paramStrandID] = TRUE
		
		// Output flow debug.
		#IF IS_DEBUG_BUILD
			IF IS_FLOW_CONSOLE_LOG_READY_TO_PRINT()
				CDEBUG1LN(DEBUG_FLOW_COMMANDS, "Wait In Real Time (TIME=", paramIndex, ")")
			ENDIF
		#ENDIF
	
	// Timer set. Check if the game clock is greater than the time we're waiting for.
	ELIF GET_GAME_TIMER() > g_flowUnsaved.iStrandTimer[paramStrandID]
	
		g_flowUnsaved.bStrandTimerSet[paramStrandID] = FALSE
		
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	RETURN STAY_ON_THIS_COMMAND
ENDFUNC


// -----------------------------------------------------------------------------------------------------------
// PURPOSE:	Perform the 'Wait Until Game Time' flow command. Set a strand to wait until a specified hour is 
//			reached on the in-game clock.
//
// INPUT PARAMS:		paramStrandID		The strand that this command is operating on.
//						paramIndex			The command storage index. This command diretly stores variable data to this index.
// RETURN VALUE:		JUMP_LABEL_IDS		The Jump Label to continue at.

FUNC JUMP_LABEL_IDS PERFORM_WAIT_UNTIL_GAME_HOUR(STRANDS paramStrandID, INT paramIndex)
	
	// If this strand hasn't got a timer set then we need to store the command's TIMEOFDAY.
	IF NOT g_flowUnsaved.bStrandTimerSet[paramStrandID]
	
		// Ensure the hour value is valid.
		IF (paramIndex < 0 OR paramIndex > 23)
			CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_WAIT_UNTIL_GAME_HOUR: Invalid hour passed. The hour was not in the range [0 <= hour <= 23].")
			RETURN NEXT_SEQUENTIAL_COMMAND
		ENDIF
	
		// Get the hour.
		INT iHourToWaitTill = paramIndex // Wait hour directly stored in index.
		TIMEOFDAY sTimeToWaitTill = GET_CURRENT_TIMEOFDAY()
		
		// If the current hour is after the required hour then we need to wait until the next day.
		IF GET_TIMEOFDAY_HOUR(sTimeToWaitTill) > iHourToWaitTill
			ADD_TIME_TO_TIMEOFDAY(sTimeToWaitTill, 0, 0, 0, 1, 0, 0)
		ENDIF
		// If the current time isn't already in the correct hour set the hour to wait till.
		IF GET_TIMEOFDAY_HOUR(sTimeToWaitTill) <> iHourToWaitTill
			SET_TIMEOFDAY_SECOND(sTimeToWaitTill, 0)
			SET_TIMEOFDAY_MINUTE(sTimeToWaitTill, 0)
			SET_TIMEOFDAY_HOUR(sTimeToWaitTill, iHourToWaitTill)
		ENDIF
	
		// Setup the strand wait timer.
		g_flowUnsaved.sStrandInGameTimer[paramStrandID] = sTimeToWaitTill
		g_flowUnsaved.bStrandTimerSet[paramStrandID] = TRUE
		
		// Output flow debug.
		#IF IS_DEBUG_BUILD
			IF IS_FLOW_CONSOLE_LOG_READY_TO_PRINT()
				CDEBUG1LN(DEBUG_FLOW_COMMANDS, "Wait Until Game Hour (HOUR=", iHourToWaitTill, ")")
			ENDIF
		#ENDIF
		
	// Timer set. Check if the game clock has advanced passed the stored TIMEOFDAY.
	ELIF IS_NOW_AFTER_TIMEOFDAY(g_flowUnsaved.sStrandInGameTimer[paramStrandID])
		
		g_flowUnsaved.bStrandTimerSet[paramStrandID] = FALSE
		RETURN NEXT_SEQUENTIAL_COMMAND
		
	ENDIF
	
	RETURN STAY_ON_THIS_COMMAND
ENDFUNC


// -----------------------------------------------------------------------------------------------------------
// PURPOSE:	Perform the 'Wait Until Game Time And Date' flow command. Set a strand to wait until a specified 
//			time and date is reached on the in-game clock.
//
// INPUT PARAMS:		paramStrandID				The strand that this command is operating on
//						paramCoreVarsArrayPos		Index into the Core Variables array
// RETURN VALUE:		JUMP_LABEL_IDS				The Jump Label to continue at

FUNC JUMP_LABEL_IDS PERFORM_WAIT_UNTIL_GAME_TIME_AND_DATE(STRANDS paramStrandID, INT paramIndex)
	
	// Ensure the Core Vars Array position is legal
//	IF (paramCoreVarsArrayPos = ILLEGAL_ARRAY_POSITION)
//		SCRIPT_ASSERT("PERFORM_WAIT_UNTIL_GAME_TIME_AND_DATE: The Core Variables Array Position parameter is illegal. MOVING ON TO NEXT COMMAND.")
//		RETURN NEXT_SEQUENTIAL_COMMAND
//	ENDIF
	
	// If this strand hasn't got a timer set then we need to store the command's TIMEOFDAY.
	IF NOT g_flowUnsaved.bStrandTimerSet[paramStrandID]
	
		// Reconstruct a TimeOfDay struct using the two stored int values.
		TIMEOFDAY sTimeToWaitTo = INT_TO_ENUM(TIMEOFDAY, paramIndex)
//		sTimeToWaitTo.iStoredTime	= g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue1
//		sTimeToWaitTo.iYears 		= g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue2
	
		// Store time to wait to to the strands global in-game timer.
		g_flowUnsaved.sStrandInGameTimer[paramStrandID] = sTimeToWaitTo
		g_flowUnsaved.bStrandTimerSet[paramStrandID] = TRUE
		
		#IF IS_DEBUG_BUILD
			IF IS_FLOW_CONSOLE_LOG_READY_TO_PRINT()
				CDEBUG1LN(DEBUG_FLOW_COMMANDS, "Wait Until Game Time And Date (SEC=", GET_TIMEOFDAY_SECOND(sTimeToWaitTo), " MIN=", GET_TIMEOFDAY_MINUTE(sTimeToWaitTo), " HOUR=", GET_TIMEOFDAY_HOUR(sTimeToWaitTo), " DAY=", GET_TIMEOFDAY_DAY(sTimeToWaitTo), " MON=", GET_TIMEOFDAY_MONTH(sTimeToWaitTo), " YEAR=", GET_TIMEOFDAY_YEAR(sTimeToWaitTo), ")")
			ENDIF
		#ENDIF

	// Timer set. Check if the game clock has advanced passed the stored TIMEOFDAY.
	ELIF IS_NOW_AFTER_TIMEOFDAY(g_flowUnsaved.sStrandInGameTimer[paramStrandID])
		
		g_flowUnsaved.bStrandTimerSet[paramStrandID] = FALSE
		RETURN NEXT_SEQUENTIAL_COMMAND
		
	ENDIF
	
	RETURN STAY_ON_THIS_COMMAND
ENDFUNC


// -----------------------------------------------------------------------------------------------------------
// PURPOSE:	Perform the 'Is Game Time After Hour' flow command. Jump to a specific label based on the whether 
//			or not the in-game clock has past a specified hour. 
//
// INPUT PARAMS:		paramCoreVarsArrayPos		Index into the Core Variables array
// RETURN VALUE:		JUMP_LABEL_IDS				The Jump Label to continue at

FUNC JUMP_LABEL_IDS PERFORM_IS_GAME_TIME_AFTER_HOUR(INT paramCoreVarsArrayPos)

	// Ensure the Core Vars Array position is legal
	IF (paramCoreVarsArrayPos = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_IS_GAME_TIME_AFTER_HOUR: The Core Variables Array Position parameter is illegal. MOVING ON TO NEXT COMMAND.")
		
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Get the hour to be tested.
	INT iHour = g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue1
	
	// Ensure the hour to be tested is legal.
	IF iHour < 0 OR iHour >= 24
		CERRORLN(DEBUG_FLOW_COMMANDS, "PERFORM_IS_GAME_TIME_AFTER_HOUR: The hour being checked is not a valid value. MOVING ON TO NEXT COMMAND.")
		
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	// Get the jump labels.
	JUMP_LABEL_IDS eTrueJump = INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue2)
	JUMP_LABEL_IDS eFalseJump = INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.coreVars[paramCoreVarsArrayPos].iValue3)
	
	// Output flow debug.
	#IF IS_DEBUG_BUILD
		IF IS_FLOW_CONSOLE_LOG_READY_TO_PRINT()
			CDEBUG1LN(DEBUG_FLOW_COMMANDS, "Is Game Time After Hour (HOUR: ", iHour, ", TRUEJUMP=", Get_Label_Display_String_From_Label_ID(eTrueJump), ", FALSEJUMP=", Get_Label_Display_String_From_Label_ID(eFalseJump), ")")
		ENDIF
	#ENDIF
	
	// Check if the game time is after the specified hour.
	IF GET_CLOCK_HOURS() >= iHour
		RETURN eTrueJump
	ELSE
		RETURN eFalseJump
	ENDIF
	
ENDFUNC









