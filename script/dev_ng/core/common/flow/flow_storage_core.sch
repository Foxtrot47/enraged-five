USING "flow_commands_core.sch"
USING "script_clock.sch"
USING "debug_channels_structs.sch"
USING "flow_debug_core.sch"




// ===========================================================================================================
//		Housekeeping for a new Strand's Command Storage
// ===========================================================================================================

// PURPOSE:	Prepares for a new strand's commands and stores the lower boundary
//
// INPUT PARAMS:	paramStrandID			The strand ID

PROC START_STRAND_COMMANDS_STORAGE(STRANDS paramStrandID)

	CDEBUG1LN(DEBUG_FLOW_STORAGE, "BEGIN: Mission Flow Command Storage (STRAND = ", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(paramStrandID),")")

	// Ensure legal strand ID
	IF NOT (paramStrandID < MAX_STRANDS)
		CERRORLN(DEBUG_FLOW, "Begin_Strand_Commands: illegal Strand ID")
		EXIT
	ENDIF
	
	
	// Ensure strand cleared and ready for data
	IF NOT (g_flowUnsaved.flowCommandsIndex[paramStrandID].lower = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW, "Begin_Strand_Commands: Strand Data not cleared - attempting to overwrite already stored data", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(paramStrandID))
		EXIT
	ENDIF
	
	// Store the lower boundary for this strand
	g_flowUnsaved.flowCommandsIndex[paramStrandID].lower	= g_flowUnsaved.iNumStoredFlowCommands

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Ends a strands command storage and stores the upper boundary
//
// INPUT PARAMS:	paramStrandID		The strand ID

PROC STOP_STRAND_COMMANDS_STORAGE(STRANDS paramStrandID)

	// Ensure legal strand ID
	IF NOT (paramStrandID < MAX_STRANDS)
		CERRORLN(DEBUG_FLOW, "End_Strand_Commands: illegal Strand ID")
		EXIT
	ENDIF


	// Ensure strand has a lower boundary set (consistency checking)
	IF (g_flowUnsaved.flowCommandsIndex[paramStrandID].lower = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW, "End_Strand_Commands: No lower boundary for this strand has been set", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(paramStrandID))
		EXIT
	ENDIF
	
	// Ensure strand has no upper boundary set yet
	IF NOT (g_flowUnsaved.flowCommandsIndex[paramStrandID].upper = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW, "End_Strand_Commands: Strand Data upper boundary not cleared - possibly attempting to overwrite already stored data", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(paramStrandID))
		EXIT
	ENDIF
	
	// Ensure some data was stored
	IF (g_flowUnsaved.flowCommandsIndex[paramStrandID].lower = g_flowUnsaved.iNumStoredFlowCommands)
		g_flowUnsaved.flowCommandsIndex[paramStrandID].lower = ILLEGAL_ARRAY_POSITION
		CERRORLN(DEBUG_FLOW, "End_Strand_Commands: No commands found for strand ID", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(paramStrandID))
		EXIT
	ENDIF
	
	// Store the upper boundary for this strand
	// NOTE: need to subtract 1 to make the upper boundary inclusive
	g_flowUnsaved.flowCommandsIndex[paramStrandID].upper	= (g_flowUnsaved.iNumStoredFlowCommands - 1)
	
	#if USE_CLF_DLC
		// If the command position hasn't already been set by savegame loading, set it to the default lower bounds position.
		IF g_savedGlobalsClifford.sFlow.strandSavedVars[paramStrandID].thisCommandPos = ILLEGAL_ARRAY_POSITION
			g_savedGlobalsClifford.sFlow.strandSavedVars[paramStrandID].thisCommandPos = g_flowUnsaved.flowCommandsIndex[paramStrandID].lower
		ENDIF
	#endif
	#if USE_NRM_DLC
		// If the command position hasn't already been set by savegame loading, set it to the default lower bounds position.
		IF g_savedGlobalsnorman.sFlow.strandSavedVars[paramStrandID].thisCommandPos = ILLEGAL_ARRAY_POSITION
			g_savedGlobalsnorman.sFlow.strandSavedVars[paramStrandID].thisCommandPos = g_flowUnsaved.flowCommandsIndex[paramStrandID].lower
		ENDIF
	#endif
	
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
		// If the command position hasn't already been set by savegame loading, set it to the default lower bounds position.
		IF g_savedGlobals.sFlow.strandSavedVars[paramStrandID].thisCommandPos = ILLEGAL_ARRAY_POSITION
			g_savedGlobals.sFlow.strandSavedVars[paramStrandID].thisCommandPos = g_flowUnsaved.flowCommandsIndex[paramStrandID].lower
		ENDIF
	#endif
	#endif
	
	CDEBUG1LN(DEBUG_FLOW_STORAGE, "END: Mission Flow Command Storage (STRAND = ", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(paramStrandID), ")")

ENDPROC



// ===========================================================================================================
//		Flow Command Storage Array Access.
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//		Core Array Storage
// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Store command variables into the array of core command structs.
//
// INPUT PARAMS:	paramArrayPos		The array position within the Core Variables storage array	
//					paramInt1			The first general integer value to be stored for the core command.
//					paramInt2			The second general integer value to be stored for the core command.
//					paramInt3			The third general integer value to be stored for the core command.
//					paramInt4			The fourth general integer value to be stored for the core command.

PROC STORE_CORE_STORAGE_TYPE_VARIABLES(INT paramArrayPos, INT paramInt1, INT paramInt2, INT paramInt3, INT paramInt4)

	g_flowUnsaved.coreVars[paramArrayPos].iValue1	= paramInt1
	g_flowUnsaved.coreVars[paramArrayPos].iValue2	= paramInt2
	g_flowUnsaved.coreVars[paramArrayPos].iValue3	= paramInt3
	g_flowUnsaved.coreVars[paramArrayPos].iValue4	= paramInt4

ENDPROC


// -----------------------------------------------------------------------------------------------------------
//		Core Small Array Storage
// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Store command variables into the array of small core command structs.
//
// INPUT PARAMS:	paramArrayPos		The array position within the Core Variables storage array	
//					paramInt1			The first general integer value to be stored for the core command.
//					paramInt2			The second general integer value to be stored for the core command.

PROC STORE_CORE_SMALL_STORAGE_TYPE_VARIABLES(INT paramArrayPos, INT paramInt1, INT paramInt2)

	g_flowUnsaved.coreSmallVars[paramArrayPos].iValue1	= paramInt1
	g_flowUnsaved.coreSmallVars[paramArrayPos].iValue2	= paramInt2

ENDPROC


// -----------------------------------------------------------------------------------------------------------
//		Text Array Storage
// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Store all the variables associated with text based mission flow commands
//
PROC STORE_TEXT_TYPE_VARIABLES(INT paramArrayPos, TEXT_LABEL_31 paramTextLabel)
	
	g_flowUnsaved.textVars[paramArrayPos].txtValue 	= paramTextLabel
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------
//		Text Large Array Storage
// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Store all the variables associated with larger text based mission flow commands
//
PROC STORE_TEXT_LARGE_TYPE_VARIABLES(INT paramArrayPos, TEXT_LABEL_31 paramTextLabel, INT paramInt1, INT paramInt2, INT paramInt3, INT paramInt4)
	
	g_flowUnsaved.textLargeVars[paramArrayPos].txtValue = paramTextLabel
	g_flowUnsaved.textLargeVars[paramArrayPos].iValue1	= paramInt1
	g_flowUnsaved.textLargeVars[paramArrayPos].iValue2	= paramInt2
	g_flowUnsaved.textLargeVars[paramArrayPos].iValue3	= paramInt3
	g_flowUnsaved.textLargeVars[paramArrayPos].iValue4	= paramInt4

ENDPROC


// -----------------------------------------------------------------------------------------------------------
//		Miscellaneous Array Storage
// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Store all the variables associated with commands that don;t justify their own specific data storage struct
//
// INPUT PARAMS:	paramArrayPos		The array position within the Miscellaneous Variables storage array
//					paramFlagID			A flagID
//					paramLabelID1		A labelID for a GOTO or a jump
//					paramLabelID2		A second LabelID for a GOTO or a jump
//					paramLabelID3		A third LabelID for a GOTO or a jump
//					paramStrandID		A strandID
//					paramSyncID			A syncID
//					paramBool			TRUE or FALSE

PROC STORE_MISCELLANEOUS_STORAGE_TYPE_VARIABLES(INT paramArrayPos, FLOW_BITSET_IDS paramBitsetID, FLOW_FLAG_IDS paramFlagID, FLOW_INT_IDS paramIntID, JUMP_LABEL_IDS paramLabelID1, JUMP_LABEL_IDS paramLabelID2, JUMP_LABEL_IDS paramLabelID3, STRANDS paramStrandID, SYNCHRONIZATION_IDS paramSyncID, STATIC_BLIP_NAME_ENUM paramBlipID, TIMEOFDAY paramTimeOfDay, BOOL paramBool, INT paramInt)

	g_flowUnsaved.miscellaneousVars[paramArrayPos].bitsetID		= paramBitsetID
	g_flowUnsaved.miscellaneousVars[paramArrayPos].flagID		= paramFlagID
	g_flowUnsaved.miscellaneousVars[paramArrayPos].intID		= paramIntID
	g_flowUnsaved.miscellaneousVars[paramArrayPos].jumpLabelID1	= paramLabelID1
	g_flowUnsaved.miscellaneousVars[paramArrayPos].jumpLabelID2	= paramLabelID2
	g_flowUnsaved.miscellaneousVars[paramArrayPos].jumpLabelID3	= paramLabelID3
	g_flowUnsaved.miscellaneousVars[paramArrayPos].strandID		= paramStrandID
	g_flowUnsaved.miscellaneousVars[paramArrayPos].syncID		= paramSyncID
	g_flowUnsaved.miscellaneousVars[paramArrayPos].blipID		= paramBlipID
	g_flowUnsaved.miscellaneousVars[paramArrayPos].aTimeOfDay	= paramTimeOfDay
	g_flowUnsaved.miscellaneousVars[paramArrayPos].aBool		= paramBool
	g_flowUnsaved.miscellaneousVars[paramArrayPos].anInt		= paramInt

ENDPROC



// -----------------------------------------------------------------------------------------------------------
//		Script Array Storage
// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Store all the variables associated with launching a script (mission or standalone)
//
// INPUT PARAMS:	paramArrayPos		The array position within the Script Variables storage array
//					paramID 			The array position within the Script Variables storage array
//					paramFilenameNoSC	The filename for the script without its .sc extension.
//					paramPassGoto		The jump label to goto on mission Pass
//					paramFailGoto		The jump label to goto on mission Fail
//					paramDebugGoto		The jump label to goto when stepping through the flow in debug mode with the flow launcher.

PROC STORE_SCRIPT_STORAGE_TYPE_VARIABLES(INT paramArrayPos, INT paramID, STRING paramFilenameNoSC, JUMP_LABEL_IDS paramPassGoto, JUMP_LABEL_IDS paramFailGoto, JUMP_LABEL_IDS paramDebugGoto)
	
	g_flowUnsaved.scriptVars[paramArrayPos].id				= paramID
	g_flowUnsaved.scriptVars[paramArrayPos].filename		= paramFilenameNoSC
	g_flowUnsaved.scriptVars[paramArrayPos].filenameHash	= GET_HASH_KEY(paramFilenameNoSC)
	g_flowUnsaved.scriptVars[paramArrayPos].passGoto		= paramPassGoto
	g_flowUnsaved.scriptVars[paramArrayPos].failGoto		= paramFailGoto
	
	#IF IS_DEBUG_BUILD
		g_flowUnsaved.scriptVars[paramArrayPos].debugGoto	= paramDebugGoto
	#ENDIF
	#IF IS_FINAL_BUILD
		UNUSED_PARAMETER(paramDebugGoto)
	#ENDIF
ENDPROC


#IF IS_DEBUG_BUILD

// -----------------------------------------------------------------------------------------------------------
//		Debug Array Storage
// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Store command variables into the array of debug command structs.
//
// INPUT PARAMS:	paramArrayPos		The array position within the Debug Variables storage array	
//					paramInt1			The first general integer value to be stored for the debug command.
//					paramInt2			The second general integer value to be stored for the debug command.

PROC STORE_DEBUG_STORAGE_TYPE_VARIABLES(INT paramArrayPos, INT paramInt1, INT paramInt2)

	g_flowUnsaved.debugVars[paramArrayPos].anInt1 = paramInt1
	g_flowUnsaved.debugVars[paramArrayPos].anInt2 = paramInt2
	
ENDPROC

#ENDIF




// ===========================================================================================================
//		Housekeeping for storing a new Command
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
// PURPOSE: Prepares to store a new command
//
// INPUT PARAMS:	paramStorageType		The Storage Type ID to prepare the storage for new variables

PROC START_FLOW_COMMAND(FLOW_STORAGE_TYPES paramStorageType)

#IF IS_DEBUG_BUILD
	STRING theError = ""
#ENDIF	//	IS_DEBUG_BUILD

	// Ensure there is space in the commands array.
	IF (g_flowUnsaved.iNumStoredFlowCommands >= MAX_MISSION_FLOW_COMMANDS)
#IF IS_DEBUG_BUILD
			theError = "Start_Flow_Command: Out of Flow Commands array space. Increase MAX_MISSION_FLOW_COMMANDS."
			DISPLAY_MISSION_FLOW_FATAL_ASSERT_MESSAGE(theError)
			SCRIPT_ASSERT(theError)
#ENDIF
		EXIT
	ENDIF
	
	// Ensure there is space in the relevant storage type variables array.
	SWITCH (paramStorageType)
		CASE FST_NONE
		CASE FST_INDEX_DIRECT
			BREAK
			
		CASE FST_CORE
			IF (g_flowUnsaved.iNumStoredFlowVariables[paramStorageType] >= MAX_CORE_VARIABLE_SLOTS)
#IF IS_DEBUG_BUILD
					theError = "Start_Flow_Command: Out of Core Variables storage space. Increase MAX_CORE_VARIABLE_SLOTS"
					DISPLAY_MISSION_FLOW_FATAL_ASSERT_MESSAGE(theError)
					SCRIPT_ASSERT(theError)
#ENDIF
				EXIT
			ENDIF
			BREAK
			
		CASE FST_CORE_SMALL
			IF (g_flowUnsaved.iNumStoredFlowVariables[paramStorageType] >= MAX_CORE_SMALL_VARIABLE_SLOTS)
#IF IS_DEBUG_BUILD
					theError = "Start_Flow_Command: Out of Core Variables storage space. Increase MAX_CORE_SMALL_VARIABLE_SLOTS"
					DISPLAY_MISSION_FLOW_FATAL_ASSERT_MESSAGE(theError)
					SCRIPT_ASSERT(theError)
#ENDIF
				EXIT
			ENDIF
			BREAK
			
		CASE FST_COMMS
			IF (g_flowUnsaved.iNumStoredFlowVariables[paramStorageType] >= MAX_COMMS_VARIABLE_SLOTS)
#IF IS_DEBUG_BUILD
					theError = "Start_Flow_Command: Out of Comms Variables storage space. Increase MAX_COMMS_VARIABLE_SLOTS"
					DISPLAY_MISSION_FLOW_FATAL_ASSERT_MESSAGE(theError)
					SCRIPT_ASSERT(theError)
#ENDIF
				EXIT
			ENDIF
			BREAK
			
		CASE FST_COMMS_LARGE
			IF (g_flowUnsaved.iNumStoredFlowVariables[paramStorageType] >= MAX_COMMS_LARGE_VARIABLE_SLOTS)
#IF IS_DEBUG_BUILD
					theError = "Start_Flow_Command: Out of Comms Large Variables storage space. Increase MAX_COMMS_LARGE_VARIABLE_SLOTS"
					DISPLAY_MISSION_FLOW_FATAL_ASSERT_MESSAGE(theError)
					SCRIPT_ASSERT(theError)
#ENDIF
				EXIT
			ENDIF
			BREAK
			
		CASE FST_TEXT
			IF (g_flowUnsaved.iNumStoredFlowVariables[paramStorageType] >= MAX_TEXT_VARIABLE_SLOTS)
#IF IS_DEBUG_BUILD
					theError = "Start_Flow_Command: Out of Text Variables storage space. Increase MAX_TEXT_VARIABLE_SLOTS"
					DISPLAY_MISSION_FLOW_FATAL_ASSERT_MESSAGE(theError)
					SCRIPT_ASSERT(theError)
#ENDIF
				EXIT
			ENDIF
			BREAK
			
		CASE FST_TEXT_LARGE
			IF (g_flowUnsaved.iNumStoredFlowVariables[paramStorageType] >= MAX_TEXT_LARGE_VARIABLE_SLOTS)
#IF IS_DEBUG_BUILD
					theError = "Start_Flow_Command: Out of Text Large Variables storage space. Increase MAX_TEXT_LARGE_VARIABLE_SLOTS"
					DISPLAY_MISSION_FLOW_FATAL_ASSERT_MESSAGE(theError)
					SCRIPT_ASSERT(theError)
#ENDIF
				EXIT
			ENDIF
			BREAK
			
		CASE FST_SCRIPT
			IF (g_flowUnsaved.iNumStoredFlowVariables[paramStorageType] >= MAX_SCRIPT_VARIABLE_SLOTS)
#IF IS_DEBUG_BUILD
					theError = "Start_Flow_Command: Out of Script Variables storage space. Increase MAX_SCRIPT_VARIABLE_SLOTS"
					DISPLAY_MISSION_FLOW_FATAL_ASSERT_MESSAGE(theError)
					SCRIPT_ASSERT(theError)
#ENDIF
				EXIT
			ENDIF
			BREAK
			
		CASE FST_MISCELLANEOUS
			IF (g_flowUnsaved.iNumStoredFlowVariables[paramStorageType] >= MAX_MISCELLANEOUS_VARIABLE_SLOTS)
#IF IS_DEBUG_BUILD
					theError = "Start_Flow_Command: Out of Miscellaneous Variables storage space. Increase MAX_MISCELLANEOUS_VARIABLE_SLOTS"
					DISPLAY_MISSION_FLOW_FATAL_ASSERT_MESSAGE(theError)
					SCRIPT_ASSERT(theError)
#ENDIF
				EXIT
			ENDIF
			BREAK
			
#IF IS_DEBUG_BUILD
		CASE FST_DEBUG
			IF (g_flowUnsaved.iNumStoredFlowVariables[paramStorageType] >= MAX_DEBUG_VARIABLE_SLOTS)
				theError = "Start_Flow_Command: Out of Debug Variables storage space. Increase MAX_DEBUG_VARIABLE_SLOTS"
				DISPLAY_MISSION_FLOW_FATAL_ASSERT_MESSAGE(theError)
				SCRIPT_ASSERT(theError)
				EXIT
			ENDIF
			BREAK
#ENDIF
			
		DEFAULT
			CERRORLN(DEBUG_FLOW_COMMANDS, "Start_Flow_Command: Unrecognised storage type")
			BREAK
	ENDSWITCH

ENDPROC


// -----------------------------------------------------------------------------------------------------------
// PURPOSE: Tidies up after storing a new command
//
// INPUT PARAMS:	paramStorageType		The Storage Type ID to update the storage type control variables

PROC STOP_FLOW_COMMAND(FLOW_STORAGE_TYPES paramStorageType)

	// Increase the number of stored commands
	g_flowUnsaved.iNumStoredFlowCommands++
	
	// Increase the number of storage type spaces items used.
	IF NOT (paramStorageType = FST_NONE)
		g_flowUnsaved.iNumStoredFlowVariables[paramStorageType]++
	ENDIF

ENDPROC



// ===========================================================================================================
//		Strand Management Command Storage
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
// PURPOSE: Store 'Activate Strand' command
// 
// INPUT PARAMS:	paramStrandID		The StrandID

PROC SETFLOW_ACTIVATE_STRAND(STRANDS paramStrandID)

	FLOW_STORAGE_TYPES	thisStorageType = FST_INDEX_DIRECT
	
	START_FLOW_COMMAND(thisStorageType)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_ACTIVATE_STRAND
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index		= ENUM_TO_INT(paramStrandID) //Direct storage to index.
										
	CDEBUG1LN(DEBUG_FLOW_STORAGE, g_flowUnsaved.iNumStoredFlowCommands, ": ACTIVATE STRAND [STRAND=", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(paramStrandID),"]")
	
	STOP_FLOW_COMMAND(thisStorageType)

ENDPROC


// -----------------------------------------------------------------------------------------------------------
// PURPOSE: Store 'Await Activation' command

PROC SETFLOW_AWAIT_ACTIVATION()

	FLOW_STORAGE_TYPES	thisStorageType = FST_NONE
	
	START_FLOW_COMMAND(thisStorageType)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_AWAIT_ACTIVATION
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index			= ILLEGAL_ARRAY_POSITION
											
	CDEBUG1LN(DEBUG_FLOW_STORAGE, g_flowUnsaved.iNumStoredFlowCommands, ": AWAIT ACTIVATION")
	
	STOP_FLOW_COMMAND(thisStorageType)

ENDPROC


// -----------------------------------------------------------------------------------------------------------
// PURPOSE: Store 'Kill Strand' command
// 
// INPUT PARAMS:	paramStrandID		The StrandID

PROC SETFLOW_KILL_STRAND(STRANDS paramStrandID)

	FLOW_STORAGE_TYPES	thisStorageType = FST_INDEX_DIRECT
	
	START_FLOW_COMMAND(thisStorageType)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_KILL_STRAND
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index		= ENUM_TO_INT(paramStrandID) //Direct storage to index.
											
	CDEBUG1LN(DEBUG_FLOW_STORAGE, g_flowUnsaved.iNumStoredFlowCommands, ": KILL STRAND [STRAND=", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(paramStrandID), "]")
	
	STOP_FLOW_COMMAND(thisStorageType)

ENDPROC


// -----------------------------------------------------------------------------------------------------------
// PURPOSE: Store 'Terminate' command

PROC SETFLOW_TERMINATE()

	FLOW_STORAGE_TYPES	thisStorageType = FST_NONE
	
	START_FLOW_COMMAND(thisStorageType)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_TERMINATE
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index		= ILLEGAL_ARRAY_POSITION
											
	CDEBUG1LN(DEBUG_FLOW_STORAGE, g_flowUnsaved.iNumStoredFlowCommands, ": TERMINATE")
	STOP_FLOW_COMMAND(thisStorageType)

ENDPROC



// ===========================================================================================================
//		Jump Command Storage
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
// PURPOSE: Store 'Goto' command
// 
// INPUT PARAMS:	paramLabelID		The LabelID

PROC SETFLOW_GOTO(JUMP_LABEL_IDS paramLabelID, INT paramHashModifier = 0)

	FLOW_STORAGE_TYPES	thisStorageType = FST_INDEX_DIRECT
	
	START_FLOW_COMMAND(thisStorageType)
	
	INT iData = ENUM_TO_INT(paramLabelID)
	iData += (paramHashModifier*FLOW_COMMAND_HASH_MODIFIER_MAGNITUDE)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_GOTO
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index			= iData //Direct storage to index.
							
	CDEBUG1LN(DEBUG_FLOW_STORAGE, g_flowUnsaved.iNumStoredFlowCommands, ": GOTO [LABEL=", GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(paramLabelID), "]")
	
	STOP_FLOW_COMMAND(thisStorageType)

ENDPROC

PROC SETFLOW_STRAND_GOTO(STRANDS paramStrandID, JUMP_LABEL_IDS paramLabelID)

	FLOW_STORAGE_TYPES	thisStorageType = FST_CORE_SMALL
	
	START_FLOW_COMMAND(thisStorageType)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_STRAND_GOTO
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index			= g_flowUnsaved.iNumStoredFlowVariables[thisStorageType]
	
	STORE_CORE_SMALL_STORAGE_TYPE_VARIABLES( g_flowUnsaved.iNumStoredFlowVariables[thisStorageType],
										ENUM_TO_INT(paramStrandID),
										ENUM_TO_INT(paramLabelID))
											
	CDEBUG1LN(DEBUG_FLOW_STORAGE, g_flowUnsaved.iNumStoredFlowCommands, ": DO STRAND GOTO [STRAND=", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(paramStrandID), " jump lable [LABEL=", GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(paramLabelID), "]")
	STOP_FLOW_COMMAND(thisStorageType)
	
ENDPROC
// -----------------------------------------------------------------------------------------------------------
// PURPOSE: Store 'Label' command
// 
// INPUT PARAMS:	paramLabelID				The LabelID
//					paramSafeBreakpoint			A flag that states whether or not this label defines a safe breakpoint.
//												Used by both the flow launcher and the corrupted save restore system.

PROC SETFLOW_LABEL(JUMP_LABEL_IDS paramLabelID, BOOL paramSafeBreakpoint = TRUE)

	FLOW_STORAGE_TYPES	thisStorageType = FST_CORE
	
	// ERROR CHECKING: Ensure this label hasn't already been setup.
	INT 	tempLoop		= 0
	
	WHILE (tempLoop < g_flowUnsaved.iNumStoredFlowCommands)
		IF (g_flowUnsaved.flowCommands[tempLoop].command = FLOW_LABEL)
			// The labels are stored in the core Vars array.
			INT iCoreVarsArrayIndex = g_flowUnsaved.flowCommands[tempLoop].index
			JUMP_LABEL_IDS eLabel = INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.coreVars[iCoreVarsArrayIndex].iValue1)
			IF (eLabel = paramLabelID)
				// ...found the jump label, so already stored
				CERRORLN(DEBUG_FLOW, "STORING MISSION FLOW COMMANDS: SETFLOW_LABEL: Label is already stored.", GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(paramLabelID))
				EXIT
			ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD
			IF (g_flowUnsaved.flowCommands[tempLoop].command = FLOW_LABEL_LAUNCHER_ONLY)
				// The labels are stored directly in the command index.
				JUMP_LABEL_IDS eLabel = INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.flowCommands[tempLoop].index)
				IF (eLabel = paramLabelID)
					// ...found the jump label, so already stored
					CERRORLN(DEBUG_FLOW, "STORING MISSION FLOW COMMANDS: SETFLOW_LABEL: Label is already stored.", GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(paramLabelID))
					EXIT
				ENDIF
			ENDIF
		#ENDIF
		
		tempLoop++
	ENDWHILE
	
	START_FLOW_COMMAND(thisStorageType)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_LABEL
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index		= g_flowUnsaved.iNumStoredFlowVariables[thisStorageType]
	
	//Convert paramState to an INT.
	INT iSafeBreakpoint = 0
	IF paramSafeBreakpoint
		iSafeBreakpoint = 1
	ENDIF
	
	STORE_CORE_STORAGE_TYPE_VARIABLES(	g_flowUnsaved.iNumStoredFlowVariables[thisStorageType],
										ENUM_TO_INT(paramLabelID),
										iSafeBreakpoint,
										-1,			//Not needed.
										-1)			//Not needed.
										
	CDEBUG1LN(DEBUG_FLOW_STORAGE, g_flowUnsaved.iNumStoredFlowCommands, ": LABEL [LABEL=", GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(paramLabelID), " SAFE-BREAKPOINT=", GET_BOOL_DISPLAY_STRING_FROM_BOOL(paramSafeBreakpoint), "]")
	
	STOP_FLOW_COMMAND(thisStorageType)

ENDPROC


// -----------------------------------------------------------------------------------------------------------
// PURPOSE: Store 'Label Launcher Only' command. Debug only.
// 
// INPUT PARAMS:	paramLabelID				The LabelID

PROC SETFLOW_LABEL_LAUNCHER_ONLY(JUMP_LABEL_IDS paramLabelID)

//Only store this command in debug builds.
#IF IS_DEBUG_BUILD
		
	FLOW_STORAGE_TYPES	thisStorageType = FST_INDEX_DIRECT
	
	// ERROR CHECKING: Ensure this label hasn't already been setup.
	INT 	tempLoop		= 0
	
	WHILE (tempLoop < g_flowUnsaved.iNumStoredFlowCommands)
		IF (g_flowUnsaved.flowCommands[tempLoop].command = FLOW_LABEL)
			// The labels are stored in the core Vars array.
			INT iCoreVarsArrayIndex = g_flowUnsaved.flowCommands[tempLoop].index
			JUMP_LABEL_IDS eLabel = INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.coreVars[iCoreVarsArrayIndex].iValue1)
			IF (eLabel = paramLabelID)
				// ...found the jump label, so already stored
				CERRORLN(DEBUG_FLOW, "STORING MISSION FLOW COMMANDS: SETFLOW_LABEL_LAUNCHER_ONLY: Label is already stored.", GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(paramLabelID))
				EXIT
			ENDIF
		ENDIF
		
		IF (g_flowUnsaved.flowCommands[tempLoop].command = FLOW_LABEL_LAUNCHER_ONLY)
			// The labels are stored directly in the command index.
			JUMP_LABEL_IDS eLabel = INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.flowCommands[tempLoop].index)
			IF (eLabel = paramLabelID)
				// ...found the jump label, so already stored
				CERRORLN(DEBUG_FLOW, "STORING MISSION FLOW COMMANDS: SETFLOW_LABEL_LAUNCHER_ONLY: Label is already stored.", GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(paramLabelID))
				EXIT
			ENDIF
		ENDIF
		
		tempLoop++
	ENDWHILE
		
	START_FLOW_COMMAND(thisStorageType)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_LABEL_LAUNCHER_ONLY
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index			= ENUM_TO_INT(paramLabelID) //Direct storage to index.
										
	CDEBUG1LN(DEBUG_FLOW_STORAGE, g_flowUnsaved.iNumStoredFlowCommands, ": SETFLOW_LABEL_LAUNCHER_ONLY [LABEL=", GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(paramLabelID),"]")

#ENDIF
	
#IF IS_FINAL_BUILD
		
	FLOW_STORAGE_TYPES	thisStorageType = FST_NONE
	
	START_FLOW_COMMAND(thisStorageType)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_DEBUG_DUMMY
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index			= -1
	
	UNUSED_PARAMETER(paramLabelID)
	
#ENDIF
	
	STOP_FLOW_COMMAND(thisStorageType)
		
ENDPROC



// ===========================================================================================================
//		Branching Jump Command Storage
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
// PURPOSE: Store 'Store Flag State' command
// 
// INPUT PARAMS:	paramFlagID				The FlagID
//					paramState				The new flag state: TRUE or FALSE

PROC SETFLOW_STORE_FLAG_STATE(FLOW_FLAG_IDS paramFlagID, BOOL paramState, INT paramHashModifier = 0)

	FLOW_STORAGE_TYPES	thisStorageType = FST_CORE
	
	START_FLOW_COMMAND(thisStorageType)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_STORE_FLAG_STATE
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index		= g_flowUnsaved.iNumStoredFlowVariables[thisStorageType]
	
	//Convert paramState to an INT.
	INT iState = 0
	IF paramState
		iState = 1
	ENDIF
	
	STORE_CORE_STORAGE_TYPE_VARIABLES(	g_flowUnsaved.iNumStoredFlowVariables[thisStorageType],
										ENUM_TO_INT(paramFlagID),
										iState,
										paramHashModifier,	//Not needed.
										-1)					//Not needed.
										
	CDEBUG1LN(DEBUG_FLOW_STORAGE, g_flowUnsaved.iNumStoredFlowCommands, ": STORE FLAG STATE [FLAG=", GET_FLAG_DISPLAY_STRING_FROM_FLAG_ID(paramFlagID), " STATE=", GET_BOOL_DISPLAY_STRING_FROM_BOOL(paramState), "]")
	
	STOP_FLOW_COMMAND(thisStorageType)

ENDPROC


// -----------------------------------------------------------------------------------------------------------
// PURPOSE: Store 'Store Int Value' command
// 
// INPUT PARAMS:	paramIntID				The IntID
//					INT						The new int value

PROC SETFLOW_STORE_INT_VALUE(FLOW_INT_IDS paramIntID, INT paramValue)

	FLOW_STORAGE_TYPES	thisStorageType = FST_CORE
	
	START_FLOW_COMMAND(thisStorageType)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_STORE_INT_VALUE
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index		= g_flowUnsaved.iNumStoredFlowVariables[thisStorageType]
	
	STORE_CORE_STORAGE_TYPE_VARIABLES(	g_flowUnsaved.iNumStoredFlowVariables[thisStorageType],
										ENUM_TO_INT(paramIntID),
										paramValue,
										-1,			//Not needed.
										-1)			//Not needed.
											
	CDEBUG1LN(DEBUG_FLOW_STORAGE, g_flowUnsaved.iNumStoredFlowCommands, ": STORE INT VALUE [INT=", GET_INT_DISPLAY_STRING_FROM_INT_ID(paramIntID), " VALUE=", paramValue, "]")
	
	STOP_FLOW_COMMAND(thisStorageType)

ENDPROC


// -----------------------------------------------------------------------------------------------------------
// PURPOSE: Store 'Store Bitset Bit State' command
// 
// INPUT PARAMS:	paramBitsetID			The BitsetID
//					paramBit				The Bit being stored as an INT
//					paramState				The new bit state: TRUE (1) or FALSE (0)

PROC SETFLOW_STORE_BITSET_BIT_STATE(FLOW_BITSET_IDS paramBitsetID, INT paramBit, BOOL paramState)

	FLOW_STORAGE_TYPES	thisStorageType = FST_CORE
	
	START_FLOW_COMMAND(thisStorageType)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_STORE_BITSET_BIT_STATE
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index		= g_flowUnsaved.iNumStoredFlowVariables[thisStorageType]
	
	//Convert paramState to an INT.
	INT iState = 0
	IF paramState
		iState = 1
	ENDIF
	
	STORE_CORE_STORAGE_TYPE_VARIABLES(	g_flowUnsaved.iNumStoredFlowVariables[thisStorageType],
										ENUM_TO_INT(paramBitsetID),
										paramBit,
										iState,
										-1)			//Not needed.
											
	CDEBUG1LN(DEBUG_FLOW_STORAGE, g_flowUnsaved.iNumStoredFlowCommands, ": STORE BITSET BIT STATE [BITSET=", GET_BITSET_DISPLAY_STRING_FROM_BITSET_ID(paramBitsetID), " BIT=", paramBit, " STATE=", GET_BOOL_DISPLAY_STRING_FROM_BOOL(paramState), "]")
	
	STOP_FLOW_COMMAND(thisStorageType)

ENDPROC


// -----------------------------------------------------------------------------------------------------------
// PURPOSE: Store 'Do Flow Flag Jumps' command
// 
// INPUT PARAMS:	paramFlagID				The FlagID
//					paramTrueJump			The JumpLabel if the flag is TRUE
//					paramFalseJump			The JumpLabel if the flag is FALSE
//					paramDebugJump			The JumpLabel if we're stepping through the flow in debug mode with the flow launcher.

PROC SETFLOW_DO_FLAG_JUMPS(FLOW_FLAG_IDS paramFlagID, JUMP_LABEL_IDS paramTrueJump, JUMP_LABEL_IDS paramFalseJump, JUMP_LABEL_IDS paramDebugJump = STAY_ON_THIS_COMMAND, INT paramHashModifier = 0)

	FLOW_STORAGE_TYPES	thisStorageType = FST_CORE
	
	START_FLOW_COMMAND(thisStorageType)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_DO_FLAG_JUMPS
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index		= g_flowUnsaved.iNumStoredFlowVariables[thisStorageType]
	
	INT iData = ENUM_TO_INT(paramFlagID)
	iData += (paramHashModifier*FLOW_COMMAND_HASH_MODIFIER_MAGNITUDE)
	
	STORE_CORE_STORAGE_TYPE_VARIABLES(	g_flowUnsaved.iNumStoredFlowVariables[thisStorageType],
										iData,
										ENUM_TO_INT(paramTrueJump),
										ENUM_TO_INT(paramFalseJump),
										ENUM_TO_INT(paramDebugJump))
											
	CDEBUG1LN(DEBUG_FLOW_STORAGE, g_flowUnsaved.iNumStoredFlowCommands, ": DO FLAG JUMPS [FLAG=", GET_FLAG_DISPLAY_STRING_FROM_FLAG_ID(paramFlagID), " TRUEGOTO=", GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(paramTrueJump), " FALSEGOTO=", GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(paramFalseJump), " DEBUGGOTO=", GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(paramDebugJump), "]")
	STOP_FLOW_COMMAND(thisStorageType)

ENDPROC


// -----------------------------------------------------------------------------------------------------------
// PURPOSE: Store 'Do Flow Int Jumps' command
// 
// INPUT PARAMS:	paramIntID				The IntID
//					paramIntCompare			The Int Comparison value
//					paramEqualJump			The JumpLabel if the int comparison value matches the value in the IntID
//					paramNotEqualJump		The JumpLabel if the int comparison value doesn't match the value in the IntID

PROC SETFLOW_DO_INT_JUMPS(FLOW_INT_IDS paramIntID, INT paramIntCompare, JUMP_LABEL_IDS paramEqualJump, JUMP_LABEL_IDS paramNotEqualJump)

	FLOW_STORAGE_TYPES	thisStorageType = FST_CORE
	
	START_FLOW_COMMAND(thisStorageType)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_DO_INT_JUMPS
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index		= g_flowUnsaved.iNumStoredFlowVariables[thisStorageType]
	
	STORE_CORE_STORAGE_TYPE_VARIABLES(	g_flowUnsaved.iNumStoredFlowVariables[thisStorageType],
										ENUM_TO_INT(paramIntID),
										paramIntCompare,
										ENUM_TO_INT(paramEqualJump),
										ENUM_TO_INT(paramNotEqualJump))
											
	CDEBUG1LN(DEBUG_FLOW_STORAGE, g_flowUnsaved.iNumStoredFlowCommands, ": DO INT JUMPS [INT=", GET_INT_DISPLAY_STRING_FROM_INT_ID(paramIntID), " COMPARE=", paramIntCompare, " EQUALGOTO=", GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(paramEqualJump), " NOTEQUALGOTO=", GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(paramNotEqualJump), "]")
	STOP_FLOW_COMMAND(thisStorageType)

ENDPROC


// -----------------------------------------------------------------------------------------------------------
// PURPOSE: Store 'Do Flow Bitset Jumps' command
// 
// INPUT PARAMS:	paramBitsetID			The BitsetID
//					paramBit				The required bit as an INT between 1 and 32
//					paramTrueJump			The JumpLabel if the bitflag for that bitset is TRUE
//					paramFalseJump			The JumpLabel if the bitflag for that bitset is FALSE

PROC SETFLOW_DO_BITSET_JUMPS(FLOW_BITSET_IDS paramBitsetID, INT paramBit, JUMP_LABEL_IDS paramTrueJump, JUMP_LABEL_IDS paramFalseJump)

	FLOW_STORAGE_TYPES	thisStorageType = FST_CORE
	
	START_FLOW_COMMAND(thisStorageType)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_DO_BITSET_JUMPS
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index			= g_flowUnsaved.iNumStoredFlowVariables[thisStorageType]
	
	STORE_CORE_STORAGE_TYPE_VARIABLES(	g_flowUnsaved.iNumStoredFlowVariables[thisStorageType],
										ENUM_TO_INT(paramBitsetID),
										paramBit,
										ENUM_TO_INT(paramTrueJump),
										ENUM_TO_INT(paramFalseJump))
							
	CDEBUG1LN(DEBUG_FLOW_STORAGE, g_flowUnsaved.iNumStoredFlowCommands, ": DO BITSET JUMPS [BITSET=", GET_BITSET_DISPLAY_STRING_FROM_BITSET_ID(paramBitsetID), " BIT=", paramBit, " TRUEGOTO=", GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(paramTrueJump), " FALSEGOTO=", GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(paramFalseJump), "]")
	STOP_FLOW_COMMAND(thisStorageType)

ENDPROC


// -----------------------------------------------------------------------------------------------------------
// PURPOSE: Store 'Do Strand Killed Jumps' command
// 
// INPUT PARAMS:	paramStrandID			The StrandID to check for terminated
//					paramKilledJump			The JumpLabel if the comparison strand has terminated
//					paramNotKilledJump		The JumpLabel if the comparison strand has not terminated

PROC SETFLOW_DO_STRAND_KILLED_JUMPS(STRANDS paramStrandID, JUMP_LABEL_IDS paramKilledJump, JUMP_LABEL_IDS paramNotKilledJump)

	FLOW_STORAGE_TYPES	thisStorageType = FST_CORE
	
	START_FLOW_COMMAND(thisStorageType)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_DO_STRAND_KILLED_JUMPS
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index		= g_flowUnsaved.iNumStoredFlowVariables[thisStorageType]
	
	STORE_CORE_STORAGE_TYPE_VARIABLES(	g_flowUnsaved.iNumStoredFlowVariables[thisStorageType],
										ENUM_TO_INT(paramStrandID),
										ENUM_TO_INT(paramKilledJump),
										ENUM_TO_INT(paramNotKilledJump),
										-1)								//Not needed.
											
	CDEBUG1LN(DEBUG_FLOW_STORAGE, g_flowUnsaved.iNumStoredFlowCommands, ": DO STRAND KILLED JUMPS [STRAND=", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(paramStrandID), " KILLEDGOTO=", GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(paramKilledJump), " NOTKILLEDGOTO=", GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(paramNotKilledJump), "]")
	STOP_FLOW_COMMAND(thisStorageType)

ENDPROC



// ===========================================================================================================
// 		Strand-to-strand Synchronisation Command Storage.
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
// PURPOSE: Store 'Await Synchronisation' command
// 
// INPUT PARAMS:	paramSyncID			The SynchronisationID

PROC SETFLOW_AWAIT_SYNCHRONISATION(SYNCHRONIZATION_IDS paramSyncID, INT paramHashModifier = 0)

	FLOW_STORAGE_TYPES	thisStorageType = FST_INDEX_DIRECT
	
	START_FLOW_COMMAND(thisStorageType)
	
	INT iData = ENUM_TO_INT(paramSyncID)
	iData += (paramHashModifier*FLOW_COMMAND_HASH_MODIFIER_MAGNITUDE)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_AWAIT_SYNCHRONISATION
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index			= iData //Direct storage to index.
								
	CDEBUG1LN(DEBUG_FLOW_STORAGE, g_flowUnsaved.iNumStoredFlowCommands, ": AWAIT SYNCHRONISATION [SYNC=", GET_SYNCHRONISATION_DISPLAY_STRING_FROM_SYNC_ID(paramSyncID), "]")
	
	STOP_FLOW_COMMAND(thisStorageType)

ENDPROC


// -----------------------------------------------------------------------------------------------------------
// PURPOSE: Store 'Send Synchronisation' command
// 
// INPUT PARAMS:	paramSyncID			The SynchronisationID

PROC SETFLOW_SEND_SYNCHRONISATION(SYNCHRONIZATION_IDS paramSyncID, INT paramHashModifier = 0)

	FLOW_STORAGE_TYPES	thisStorageType = FST_INDEX_DIRECT
	
	START_FLOW_COMMAND(thisStorageType)
	
	INT iData = ENUM_TO_INT(paramSyncID)
	iData += (paramHashModifier*FLOW_COMMAND_HASH_MODIFIER_MAGNITUDE)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_SEND_SYNCHRONISATION
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index			= iData	//Direct storage to index.
											
	CDEBUG1LN(DEBUG_FLOW_STORAGE, g_flowUnsaved.iNumStoredFlowCommands, ": SEND SYNCHRONISATION [SYNC=", GET_SYNCHRONISATION_DISPLAY_STRING_FROM_SYNC_ID(paramSyncID), "]")
	
	STOP_FLOW_COMMAND(thisStorageType)

ENDPROC



// ===========================================================================================================
// 		Time Management Command Storage.
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
// PURPOSE: Store 'Set Game Clock' command
// 
// INPUT PARAMS:	paramHour		The new hour to set the game clock to. If set to -1 the game clock's hour will not be altered.
//					paramMinute		The new minute to set the game clock to. If set to -1 the game clock's minute will not be altered.
//					paramSecond		The new second to set the game clock to. If set to -1 the game clock's second will not be altered.
//					paramDay		The new day to set the game clock to. If set to -1 the game clock's day will not be altered.
//					paramMonth		The new month to set the game clock to. If set to -1 the game clock's month will not be altered.
//					paramYear		The new year to set the game clock to. If set to -1 the game clock's year will not be altered.

PROC SETFLOW_SET_GAME_CLOCK(INT paramHour, INT paramMinute = -1, INT paramSecond = -1, INT paramDay = -1, INT paramMonth = -1, INT paramYear = -1)

	FLOW_STORAGE_TYPES	thisStorageType = FST_CORE
	
	START_FLOW_COMMAND(thisStorageType)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_SET_GAME_CLOCK
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index		= g_flowUnsaved.iNumStoredFlowVariables[thisStorageType]
	

	//Store which clock fields are being set and set up valid values to store into a time of day struct.
	INT iFieldsToBeSetBitset = 0
	INT iStoredHour, iStoredMinute, iStoredSecond, iStoredDay, iStoredMonth, iStoredYear
	
	//Is the hour field being set?
	IF paramHour = -1
		iStoredHour = 0
	ELSE
		SET_BIT(iFieldsToBeSetBitset, 0)
		iStoredHour = paramHour
	ENDIF
	//Is the minute field being set?
	IF paramMinute = -1
		iStoredMinute = 0
	ELSE
		SET_BIT(iFieldsToBeSetBitset, 1)
		iStoredMinute = paramMinute
	ENDIF
	//Is the second field being set?
	IF paramSecond = -1
		iStoredSecond = 0
	ELSE
		SET_BIT(iFieldsToBeSetBitset, 2)
		iStoredSecond = paramSecond
	ENDIF
	//Is the day field being set?
	IF paramDay = -1
		iStoredDay = 1
	ELSE
		SET_BIT(iFieldsToBeSetBitset, 3)
		iStoredDay = paramDay
	ENDIF
	//Is the month field being set?
	IF paramMonth = -1
		iStoredMonth = 1
	ELSE
		SET_BIT(iFieldsToBeSetBitset, 4)
		iStoredMonth = paramMonth
	ENDIF
	//Is the year field being set?
	IF paramYear = -1
		iStoredYear = TIMEOFDAY_YEAR_OFFSET
	ELSE
		SET_BIT(iFieldsToBeSetBitset, 5)
		iStoredYear = paramYear
	ENDIF
	
	//Set up the time of day struct to be stored.
	TIMEOFDAY sTimeOfDay
	SET_TIMEOFDAY(sTimeOfDay, iStoredSecond, iStoredMinute, iStoredHour, iStoredDay, INT_TO_ENUM( MONTH_OF_YEAR, iStoredMonth), iStoredYear)
	
	//Copy sTimeOfDay INT fields out and store them in the core storage array.
	STORE_CORE_STORAGE_TYPE_VARIABLES(	g_flowUnsaved.iNumStoredFlowVariables[thisStorageType],
										ENUM_TO_INT(sTimeOfDay),
										iFieldsToBeSetBitset,
										-1,
										-1)						//Not needed.
											
	CDEBUG1LN(DEBUG_FLOW_STORAGE, g_flowUnsaved.iNumStoredFlowCommands, ": SET GAME CLOCK [HOUR=", paramHour, " MIN=", paramMinute, " MIN=", paramSecond, " DAY=", paramDay, " MONTH=", ENUM_TO_INT(paramMonth), " YEAR=", paramYear, "]")
	
	STOP_FLOW_COMMAND(thisStorageType)

ENDPROC


// -----------------------------------------------------------------------------------------------------------
// PURPOSE: Store 'Wait_In_Game_Time' command
// 
// INPUT PARAMS:	paramHours		The number of in-game hours to wait the strand.
//					paramMinutes	The number of in-game minutes to wait the strand.
//					paramSeconds	The number of in-game seconds to wait the strand.

PROC SETFLOW_WAIT_IN_GAME_TIME(INT paramHours, INT paramMinutes, INT paramSeconds)

	FLOW_STORAGE_TYPES	thisStorageType = FST_CORE
	
	START_FLOW_COMMAND(thisStorageType)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_WAIT_IN_GAME_TIME
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index		= g_flowUnsaved.iNumStoredFlowVariables[thisStorageType]
	
	//Check passed parameters are valid.
	IF paramHours < 0
		CERRORLN(DEBUG_FLOW, "SETFLOW_WAIT_IN_GAME_TIME: Invalid hours parameter passed.  paramHours < 0")
		EXIT
	ENDIF
	IF paramMinutes < 0
		CERRORLN(DEBUG_FLOW, "SETFLOW_WAIT_IN_GAME_TIME: Invalid minutes parameter passed.  paramMinutes < 0")
		EXIT
	ENDIF
	IF paramSeconds < 0
		CERRORLN(DEBUG_FLOW, "SETFLOW_WAIT_IN_GAME_TIME: Invalid seconds parameter passed.  paramSeconds < 0")
		EXIT
	ENDIF
	
	STORE_CORE_STORAGE_TYPE_VARIABLES(	g_flowUnsaved.iNumStoredFlowVariables[thisStorageType],
										paramHours,
										paramMinutes,
										paramSeconds,
										-1)						//Not needed.					
											
	CDEBUG1LN(DEBUG_FLOW_STORAGE, g_flowUnsaved.iNumStoredFlowCommands, ": SET WAIT IN GAME TIME [HOURS=", paramHours, " MINS=", paramMinutes, " SECS=", paramSeconds, "]")
	
	STOP_FLOW_COMMAND(thisStorageType)

ENDPROC


// -----------------------------------------------------------------------------------------------------------
// PURPOSE: Store 'Wait_In_Real_Time' command
// 
// INPUT PARAMS:	paramTime		The amount of time the strand should wait for in milliseconds of real world time.

PROC SETFLOW_WAIT_IN_REAL_TIME(INT paramTime)

	FLOW_STORAGE_TYPES	thisStorageType = FST_INDEX_DIRECT
	
	START_FLOW_COMMAND(thisStorageType)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_WAIT_IN_REAL_TIME
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index		= paramTime //Direct storage to index.
	
	//Check passed parameters are valid.
	IF paramTime < 0
		CERRORLN(DEBUG_FLOW, "SETFLOW_WAIT_IN_REAL_TIME: Invalid wait time parameter passed.  paramTime < 0")
		EXIT
	ENDIF
														
	CDEBUG1LN(DEBUG_FLOW_STORAGE, g_flowUnsaved.iNumStoredFlowCommands, ": SET WAIT IN REAL TIME [TIME=", paramTime, "]")
	
	STOP_FLOW_COMMAND(thisStorageType)

ENDPROC


// -----------------------------------------------------------------------------------------------------------
// PURPOSE: Store 'Wait_Until_Game_Hour' command
// 
// INPUT PARAMS:	paramHour		The hour that the in-game clock must reach before the strand will continue. Must be between 0 and 23.

PROC SETFLOW_WAIT_UNTIL_GAME_HOUR(INT paramHour)

	FLOW_STORAGE_TYPES	thisStorageType = FST_INDEX_DIRECT
	
	START_FLOW_COMMAND(thisStorageType)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_WAIT_UNTIL_GAME_HOUR
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index		= paramHour //Direct storage to index.
	
	//Check passed hour is valid.
	IF paramHour < 0 OR paramHour > 23
		CERRORLN(DEBUG_FLOW, "SETFLOW_WAIT_UNTIL_GAME_HOUR: Ivalid hour parameter passed. It is not in the range  0 <= paramHour <= 23.")
		EXIT
	ENDIF
										
	CDEBUG1LN(DEBUG_FLOW_STORAGE, g_flowUnsaved.iNumStoredFlowCommands, ": SET WAIT UNTIL GAME HOUR [HOUR=", paramHour, "]")
	
	STOP_FLOW_COMMAND(thisStorageType)

ENDPROC


// -----------------------------------------------------------------------------------------------------------
// PURPOSE: Store 'Wait_Until_Game_Time_And_Date' command
// 
// INPUT PARAMS:	paramSecond		The second that the in-game clock must reach before the strand will continue. Must be between 0 and 59.
//					paramMinute		The minute that the in-game clock must reach before the strand will continue. Must be between 0 and 59.
//					paramHour		The hour that the in-game clock must reach before the strand will continue. Must be between 0 and 23.
//					paramDay		The day that the in-game clock must reach before the strand will continue. Must be between 1 and 31.
//					paramMonth		The month that the in-game clock must reach before the strand will continue. Must be between 1 and 12.
//					paramYear		The year that the in-game clock must reach before the strand will continue. Must be greater than or equal to 2000.

PROC SETFLOW_WAIT_UNTIL_GAME_TIME_AND_DATE(INT paramSecond, INT paramMinute, INT paramHour, INT paramDay, INT paramMonth, INT paramYear)

	FLOW_STORAGE_TYPES	thisStorageType = FST_INDEX_DIRECT
	
	START_FLOW_COMMAND(thisStorageType)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_WAIT_UNTIL_GAME_TIME_AND_DATE
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	
	//Check passed parameters are valid.
	IF paramSecond < 0 OR paramSecond > 59
		CERRORLN(DEBUG_FLOW, "SETFLOW_WAIT_UNTIL_GAME_TIME_AND_DATE: Ivalid second parameter passed. It is not in the range  0 <= paramSecond <= 59.")
		EXIT
	ENDIF
	
	IF paramMinute < 0 OR paramMinute > 59
		CERRORLN(DEBUG_FLOW, "SETFLOW_WAIT_UNTIL_GAME_TIME_AND_DATE: Ivalid minute parameter passed. It is not in the range  0 <= paramMinute <= 59.")
		EXIT
	ENDIF

	IF paramHour < 0 OR paramHour > 23
		CERRORLN(DEBUG_FLOW, "SETFLOW_WAIT_UNTIL_GAME_TIME_AND_DATE: Ivalid hour parameter passed. It is not in the range  0 <= paramHour <= 23.")
		EXIT
	ENDIF

	IF paramDay < 1 OR paramDay > 31
		CERRORLN(DEBUG_FLOW, "SETFLOW_WAIT_UNTIL_GAME_TIME_AND_DATE: Ivalid day parameter passed. It is not in the range 1 <= paramDay <= 31.")
		EXIT
	ENDIF

	IF paramMonth < 0 OR paramMonth > 11
		CERRORLN(DEBUG_FLOW, "SETFLOW_WAIT_UNTIL_GAME_TIME_AND_DATE: Ivalid month parameter passed. It is not in the range  0 <= paramMonth <= 11.")
		EXIT
	ENDIF

	IF paramYear < 2000
		CERRORLN(DEBUG_FLOW, "SETFLOW_WAIT_UNTIL_GAME_TIME_AND_DATE: Ivalid year parameter passed.  paramYear was < 2000.")
		EXIT
	ENDIF
	
	//Configure the TIMEOFDAY struct.
	TIMEOFDAY sWaitTillTimeOfDay
	SET_TIMEOFDAY(sWaitTillTimeOfDay, paramSecond, paramMinute, paramHour, paramDay, INT_TO_ENUM(MONTH_OF_YEAR, paramMonth), paramYear)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index		= ENUM_TO_INT(sWaitTillTimeOfDay)

	
//	//Copy sWaitTillTimeOfDay INT fields out and store them in the core storage array.
//	STORE_CORE_STORAGE_TYPE_VARIABLES(	g_flowUnsaved.iNumStoredFlowVariables[thisStorageType],
//										sWaitTillTimeOfDay.iStoredTime,
//										sWaitTillTimeOfDay.iYears,
//										-1,						//Not needed.
//										-1)						//Not needed.				
											
	CDEBUG1LN(DEBUG_FLOW_STORAGE, g_flowUnsaved.iNumStoredFlowCommands, ": SET WAIT UNTIL GAME TIME AND DATE [SEC=", paramSecond, " MIN=", paramMinute, " HOUR=", paramHour, " DAY=", paramDay, " MNTH=", paramMonth, " YEAR=", paramYear, "]")
	
	STOP_FLOW_COMMAND(thisStorageType)

ENDPROC


// -----------------------------------------------------------------------------------------------------------
// PURPOSE: Store 'Is Game Time After Hour' command
// 
// INPUT PARAMS:	paramHour			Is the in-game clock hour higher than this hour?
//					paramTrueJump		The label to jump to if the in-game clock is on a higher hour.
//					paramFalseJump		The label to jump to if the in-game clock is not on a higher hour.
//					paramDebugJump		The label to jump to when debug skipping through the flow.

PROC SETFLOW_IS_GAME_TIME_AFTER_HOUR(INT paramHour, JUMP_LABEL_IDS paramTrueJump, JUMP_LABEL_IDS paramFalseJump, JUMP_LABEL_IDS paramDebugJump = STAY_ON_THIS_COMMAND)

	//If no specific debug jump is set, use the true jump by default.
	IF paramDebugJump = STAY_ON_THIS_COMMAND
		paramDebugJump = paramTrueJump
	ENDIF
	
	//Check if the hour value is in the valid range.
	IF 	paramHour < 0 OR paramHour >= 24
		CERRORLN(DEBUG_FLOW, "SETFLOW_IS_GAME_TIME_AFTER_HOUR: An invalid hour param was passed. Should be 0 <= hour < 24.")
	ENDIF
	
	FLOW_STORAGE_TYPES	thisStorageType = FST_CORE
	
	START_FLOW_COMMAND(thisStorageType)
	
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].command		= FLOW_IS_GAME_TIME_AFTER_HOUR
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].storageType	= thisStorageType
	g_flowUnsaved.flowCommands[g_flowUnsaved.iNumStoredFlowCommands].index		= g_flowUnsaved.iNumStoredFlowVariables[thisStorageType]
		
	STORE_CORE_STORAGE_TYPE_VARIABLES(	g_flowUnsaved.iNumStoredFlowVariables[thisStorageType],
										paramHour,
										ENUM_TO_INT(paramTrueJump),
										ENUM_TO_INT(paramFalseJump),
										ENUM_TO_INT(paramDebugJump))
		
	CDEBUG1LN(DEBUG_FLOW_STORAGE, g_flowUnsaved.iNumStoredFlowCommands, ": IS GAME TIME AFTER HOUR [HOUR=", paramHour, " TRUEGOTO=", GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(paramTrueJump), " FALSEGOTO=", GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(paramFalseJump), " DEBUGGOTO=", GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(paramDebugJump), "]")
	
	STOP_FLOW_COMMAND(thisStorageType)

ENDPROC





