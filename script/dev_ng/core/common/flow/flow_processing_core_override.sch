USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_script.sch"
USING "flow_private_core.sch"
USING "flow_public_core.sch"
USING "flow_debug_core.sch"




// ===========================================================================================================
//		DO MISSION commands
// ===========================================================================================================

// PURPOSE:	Sets up and maintains a mission that gets launched by entering a contact point.
//				Deals with the mission ending and handles either pass or fail.
//
// INPUT PARAMS:			paramStrandID					Strand ID that launched this mission
//							paramScriptVarsArrayPos			Index into the Script Variables array
// RETURN VALUE:			JUMP_LABEL_IDS					A jump label - should be STAY_ON_THIS_COMMAND until mission termination
//
// NOTES:	Makes use of the Mission Triggering System when checking for the mission being triggered.
#IF NOT DEFINED(PERFORM_DO_MISSION_AT_BLIP)
FUNC JUMP_LABEL_IDS PERFORM_DO_MISSION_AT_BLIP(STRANDS paramStrandID, INT paramScriptVarsArrayPos)
	paramStrandID = paramStrandID
	paramScriptVarsArrayPos = paramScriptVarsArrayPos
	CERRORLN(DEBUG_FLOW_COMMANDS, "This is the base PERFORM_DO_MISSION_AT_BLIP command processing. Please override this")
	RETURN STAY_ON_THIS_COMMAND
ENDFUNC
#ENDIF

// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Sets up and maintains a mission that gets launched immediately.
//				Deals with the mission ending and handles either pass or fail.
//
// INPUT PARAMS:			paramStrandID				StrandID that launched this mission
//							paramScriptVarsArrayPos		Index into the Script Variables array
// RETURN VALUE:			JUMP_LABEL_IDS				A jump label - should be STAY_ON_THIS_COMMAND until mission termination
//
// NOTES:	Communicates directly with the Mission Candidate System rather than using the Mission Triggering System.

#IF NOT DEFINED(PERFORM_DO_MISSION_NOW)
FUNC JUMP_LABEL_IDS PERFORM_DO_MISSION_NOW(INT paramScriptVarsArrayPos)
	paramScriptVarsArrayPos = paramScriptVarsArrayPos
	CERRORLN(DEBUG_FLOW_COMMANDS, "This is the base PERFORM_DO_MISSION_NOW command processing. Please override this")
	RETURN STAY_ON_THIS_COMMAND
ENDFUNC
#ENDIF

// PURPOSE:	Requests the script, waits for it to load, and launches it.
//
// INPUT PARAMS:			paramScriptVarsArrayPos			Index into the Script Variables array
// RETURN VALUE:			JUMP_LABEL_IDS					A jump label - should be STAY_ON_THIS_COMMAND until script successfully launched

#IF NOT DEFINED(PERFORM_LAUNCH_SCRIPT_AND_FORGET)
FUNC JUMP_LABEL_IDS PERFORM_LAUNCH_SCRIPT_AND_FORGET(INT paramScriptVarsArrayPos)
	paramScriptVarsArrayPos = paramScriptVarsArrayPos
	CERRORLN(DEBUG_FLOW_COMMANDS, "This is the base PERFORM_LAUNCH_SCRIPT_AND_FORGET command processing. Please override this")
	RETURN STAY_ON_THIS_COMMAND

ENDFUNC
#ENDIF


// ===========================================================================================================
//		MISSION STATE JUMP commands
// ===========================================================================================================

// PURPOSE:	Performs a jump based on whether or not a specific mission has been completed yet. 
//
// INPUT PARAMS:		paramScriptVarsArrayPos			Index into the Script Variables array.
// RETURN VALUE:		JUMP_LABEL_IDS					The Jump Label to continue at.

#IF NOT DEFINED(PERFORM_IS_MISSION_COMPLETED)
FUNC JUMP_LABEL_IDS PERFORM_IS_MISSION_COMPLETED(INT paramScriptVarsArrayPos)
	paramScriptVarsArrayPos = paramScriptVarsArrayPos
	CERRORLN(DEBUG_FLOW_COMMANDS, "This is the base IS_MISSION_COMPLETED command processing. Please override this")
	RETURN STAY_ON_THIS_COMMAND

ENDFUNC
#ENDIF



// ===========================================================================================================
//		RANDOM CHARACTER commands
// ===========================================================================================================

// PURPOSE:	Flag the activation BIT of the specified Random Character mission.
//
// INPUT PARAMS:		paramCoreVarsArrayPos		Index into the Miscellaneous Variables array
//
// NOTES:	This should generally be used to launch the first mission for a Random Character. For subsequent missions
//			for the same Random Character then generally a mission flow flag would get set to allow it to continue.
#IF NOT DEFINED(PERFORM_ACTIVATE_RANDOMCHAR_MISSION)
FUNC JUMP_LABEL_IDS PERFORM_ACTIVATE_RANDOMCHAR_MISSION(INT paramCoreVarsArrayPos)
	paramCoreVarsArrayPos = paramCoreVarsArrayPos
	CERRORLN(DEBUG_FLOW_COMMANDS, "This is the base PERFORM_ACTIVATE_RANDOMCHAR_MISSION command processing. Please override this")
	RETURN STAY_ON_THIS_COMMAND
	
ENDFUNC
#ENDIF


// PURPOSE:	Unflag the activation BIT of the specified Random Character mission.
//
// INPUT PARAMS:		paramIndex		The enum data pointing to the RC mission we want to deactivate. 
//										Data stored directly in array index.
#IF NOT DEFINED(PERFORM_DEACTIVATE_RANDOMCHAR_MISSION)
FUNC JUMP_LABEL_IDS PERFORM_DEACTIVATE_RANDOMCHAR_MISSION(INT paramIndex)
	paramIndex = paramIndex
	CERRORLN(DEBUG_FLOW_COMMANDS, "This is the base PERFORM_DEACTIVATE_RANDOMCHAR_MISSION command processing. Please override this")
	RETURN STAY_ON_THIS_COMMAND
	
ENDFUNC
#ENDIF


// ===========================================================================================================
//		WEAPON commands
// ===========================================================================================================

// PURPOSE:	Set the lock state of a weapon.
//
// INPUT PARAMS:		paramMiscellaneousVarsArrayPos		Index into the Miscellaneous Variables array
// RETURN VALUE:		JUMP_LABEL_IDS						The Jump Label to continue at
#IF NOT DEFINED(PERFORM_SET_WEAPON_LOCK_STATE)
FUNC JUMP_LABEL_IDS PERFORM_SET_WEAPON_LOCK_STATE(INT paramMiscellaneousVarsArrayPos)
	paramMiscellaneousVarsArrayPos = paramMiscellaneousVarsArrayPos
	CERRORLN(DEBUG_FLOW_COMMANDS, "This is the base PERFORM_SET_WEAPON_LOCK_STATE command processing. Please override this")
	RETURN STAY_ON_THIS_COMMAND

	
ENDFUNC
#ENDIF


// ===========================================================================================================
//		STATIC BLIP commands
// ===========================================================================================================

// PURPOSE:	Set the active state of a Static Blip.
//
// INPUT PARAMS:		paramMiscellaneousVarsArrayPos		Index into the Miscellaneous Variables array
// RETURN VALUE:		JUMP_LABEL_IDS						The Jump Label to continue at

#IF NOT DEFINED(PERFORM_SET_STATIC_BLIP_STATE)
FUNC JUMP_LABEL_IDS PERFORM_SET_STATIC_BLIP_STATE(INT paramMiscellaneousVarsArrayPos)
	paramMiscellaneousVarsArrayPos = paramMiscellaneousVarsArrayPos
	CERRORLN(DEBUG_FLOW_COMMANDS, "This is the base PERFORM_SET_STATIC_BLIP_STATE command processing. Please override this")
	RETURN STAY_ON_THIS_COMMAND

	
ENDFUNC
#ENDIF


// PURPOSE:	Enables the long-range status of a Static Blip.
//
// INPUT PARAMS:		paramMiscellaneousVarsArrayPos		Index into the Miscellaneous Variables array
// RETURN VALUE:		JUMP_LABEL_IDS						The Jump Label to continue at

#IF NOT DEFINED(PERFORM_ENABLE_BLIP_LONG_RANGE)
FUNC JUMP_LABEL_IDS PERFORM_ENABLE_BLIP_LONG_RANGE(INT paramMiscellaneousVarsArrayPos)
	paramMiscellaneousVarsArrayPos = paramMiscellaneousVarsArrayPos
	CERRORLN(DEBUG_FLOW_COMMANDS, "This is the base PERFORM_ENABLE_BLIP_LONG_RANGE command processing. Please override this")
	RETURN STAY_ON_THIS_COMMAND

	
ENDFUNC
#ENDIF

// PURPOSE:	Disables the long-range status of a Static Blip.
//
// INPUT PARAMS:		paramMiscellaneousVarsArrayPos		Index into the Miscellaneous Variables array
// RETURN VALUE:		JUMP_LABEL_IDS						The Jump Label to continue at

#IF NOT DEFINED(PERFORM_DISABLE_BLIP_LONG_RANGE)
FUNC JUMP_LABEL_IDS PERFORM_DISABLE_BLIP_LONG_RANGE(INT paramMiscellaneousVarsArrayPos)
	paramMiscellaneousVarsArrayPos = paramMiscellaneousVarsArrayPos
	CERRORLN(DEBUG_FLOW_COMMANDS, "This is the base PERFORM_DISABLE_BLIP_LONG_RANGE command processing. Please override this")
	RETURN STAY_ON_THIS_COMMAND

	
ENDFUNC
#ENDIF

// PURPOSE:	Set the state of a building that can have its state swapping.
//
// INPUT PARAMS:		paramMiscellaneousVarsArrayPos		Index into the Miscellaneous Variables array.
// RETURN VALUE:		JUMP_LABEL_IDS						The Jump Label to continue at.

#IF NOT DEFINED(PERFORM_SET_BUILDING_STATE)
FUNC JUMP_LABEL_IDS PERFORM_SET_BUILDING_STATE(INT paramMiscellaneousVarsArrayPos)
	paramMiscellaneousVarsArrayPos = paramMiscellaneousVarsArrayPos
	CERRORLN(DEBUG_FLOW_COMMANDS, "This is the base PERFORM_SET_BUILDING_STATE command processing. Please override this")
	RETURN STAY_ON_THIS_COMMAND

	
ENDFUNC
#ENDIF


// ===========================================================================================================
//		HELP TEXT commands
// ===========================================================================================================

// PURPOSE:	Makes a phone call to the player.
//
// INPUT PARAMS:			paramPhonecallVarsArrayPos		Index into the Phonecall Variables array
// RETURN VALUE:			JUMP_LABEL_IDS					A jump label - should be STAY_ON_THIS_COMMAND until ready to move on

#IF NOT DEFINED(PERFORM_DISPLAY_HELP_TEXT)
FUNC JUMP_LABEL_IDS PERFORM_DISPLAY_HELP_TEXT(INT paramPhonecallVarsArrayPos)
	paramPhonecallVarsArrayPos = paramPhonecallVarsArrayPos
	CERRORLN(DEBUG_FLOW_COMMANDS, "This is the base PERFORM_DISPLAY_HELP_TEXT command processing. Please override this")
	RETURN STAY_ON_THIS_COMMAND

ENDFUNC
#ENDIF


// ===========================================================================================================
//		CODE CONTROLLER commands
// ===========================================================================================================

// PURPOSE:	Directly executes a custom block of script bound to a Code ID. 
//
// INPUT PARAMS:		paramPhonecallVarsArrayPos		Index into the Phonecall Variables array.
// RETURN VALUE:		JUMP_LABEL_IDS					The Jump Label to continue at.

#IF NOT DEFINED(PERFORM_RUN_CODE_ID)
FUNC JUMP_LABEL_IDS PERFORM_RUN_CODE_ID(INT paramPhonecallVarsArrayPos)
	paramPhonecallVarsArrayPos = paramPhonecallVarsArrayPos
	CERRORLN(DEBUG_FLOW_COMMANDS, "This is the base PERFORM_RUN_CODE_ID command processing. Please override this")
	RETURN STAY_ON_THIS_COMMAND

ENDFUNC
#ENDIF


// PURPOSE:	Directly executes a custom block of script bound to a Code ID, but only in debug mode.
//
// INPUT PARAMS:		paramDebugVarsArrayPos		Index into the Debug Variables array.
// RETURN VALUE:		JUMP_LABEL_IDS				The Jump Label to continue at.

#IF IS_DEBUG_BUILD
#IF NOT DEFINED(PERFORM_DEBUG_RUN_CODE_ID)
FUNC JUMP_LABEL_IDS PERFORM_DEBUG_RUN_CODE_ID(INT paramDebugVarsArrayPos)
	paramDebugVarsArrayPos = paramDebugVarsArrayPos
	CERRORLN(DEBUG_FLOW_COMMANDS, "This is the base PERFORM_DEBUG_RUN_CODE_ID command processing. Please override this")
	RETURN STAY_ON_THIS_COMMAND
ENDFUNC
#ENDIF
#ENDIF
