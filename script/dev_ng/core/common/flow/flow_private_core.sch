USING "rage_builtins.sch"
USING "globals.sch"

USING "flow_debug_core.sch"

USING "flow_private_GAME.sch"




// ===========================================================================================================
//		Clear Mission Flow Globals
// ===========================================================================================================

// PURPOSE:	Resets the variables containing the current number of each mission flow variables storage type in use
// 
PROC CLEAR_CURRENT_FLOW_STORAGE_MAXINARRAY_VARIABLES()

	INT tempLoop = 0
	
	REPEAT MAX_FLOW_STORAGE_TYPES tempLoop
		g_flowUnsaved.iNumStoredFlowVariables[tempLoop]	= 0
	ENDREPEAT
ENDPROC


// -----------------------------------------------------------------------------------------------------------
// PURPOSE:	Clears the contents of the core variables array
// 
PROC CLEAR_CORE_VARIABLES_ARRAY()
	INT tempLoop = 0
	
	REPEAT MAX_CORE_VARIABLE_SLOTS tempLoop
		g_flowUnsaved.coreVars[tempLoop].iValue1		= 0
		g_flowUnsaved.coreVars[tempLoop].iValue2		= 1
		g_flowUnsaved.coreVars[tempLoop].iValue3		= 2
		g_flowUnsaved.coreVars[tempLoop].iValue4		= 3
	ENDREPEAT
ENDPROC


// -----------------------------------------------------------------------------------------------------------
// PURPOSE:	Clears the contents of the core small variables array
// 
PROC CLEAR_CORE_SMALL_VARIABLES_ARRAY()
	INT tempLoop = 0
	
	REPEAT MAX_CORE_SMALL_VARIABLE_SLOTS tempLoop
		g_flowUnsaved.coreSmallVars[tempLoop].iValue1		= 0
		g_flowUnsaved.coreSmallVars[tempLoop].iValue2		= 1
	ENDREPEAT
ENDPROC


// -----------------------------------------------------------------------------------------------------------
// PURPOSE:	Clears the contents of the text variables array
// 
PROC CLEAR_TEXT_VARIABLES_ARRAY()
	INT tempLoop = 0
	
	REPEAT MAX_TEXT_VARIABLE_SLOTS tempLoop
		g_flowUnsaved.textVars[tempLoop].txtValue		= "NOT_SETUP"
	ENDREPEAT
ENDPROC


// -----------------------------------------------------------------------------------------------------------
// PURPOSE:	Clears the contents of the text large variables array
// 
PROC CLEAR_TEXT_LARGE_VARIABLES_ARRAY()
	INT tempLoop = 0
	
	REPEAT MAX_TEXT_LARGE_VARIABLE_SLOTS tempLoop
		g_flowUnsaved.textLargeVars[tempLoop].txtValue		= "NOT_SETUP"
		g_flowUnsaved.textLargeVars[tempLoop].iValue1		= 0
		g_flowUnsaved.textLargeVars[tempLoop].iValue2		= 0
		g_flowUnsaved.textLargeVars[tempLoop].iValue3		= 0
		g_flowUnsaved.textLargeVars[tempLoop].iValue4		= 0
	ENDREPEAT
ENDPROC


// -----------------------------------------------------------------------------------------------------------
// PURPOSE:	Clears the contents of the miscellaneous variables array
// 
PROC CLEAR_MISCELLANEOUS_VARIABLES_ARRAY()
	INT tempLoop = 0
	
	REPEAT MAX_MISCELLANEOUS_VARIABLE_SLOTS tempLoop
		g_flowUnsaved.miscellaneousVars[tempLoop].bitsetID		= FLOWBITSET_NONE
		g_flowUnsaved.miscellaneousVars[tempLoop].flagID		= FLOWFLAG_NONE
		g_flowUnsaved.miscellaneousVars[tempLoop].intID			= FLOWINT_NONE
		g_flowUnsaved.miscellaneousVars[tempLoop].jumpLabelID1	= STAY_ON_THIS_COMMAND
		g_flowUnsaved.miscellaneousVars[tempLoop].jumpLabelID2	= STAY_ON_THIS_COMMAND
		g_flowUnsaved.miscellaneousVars[tempLoop].strandID		= STRAND_NONE
		g_flowUnsaved.miscellaneousVars[tempLoop].syncID		= SYNC_NONE
		g_flowUnsaved.miscellaneousVars[tempLoop].aBool			= FALSE
		g_flowUnsaved.miscellaneousVars[tempLoop].anInt			= 0
	ENDREPEAT
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clears the contents of the script variables array
// 
PROC CLEAR_SCRIPT_VARIABLES_ARRAY()
	INT tempLoop = 0
	
	REPEAT MAX_SCRIPT_VARIABLE_SLOTS tempLoop
		g_flowUnsaved.scriptVars[tempLoop].id				= 0
		g_flowUnsaved.scriptVars[tempLoop].filename			= ""
		g_flowUnsaved.scriptVars[tempLoop].filenameHash		= 0
		g_flowUnsaved.scriptVars[tempLoop].passGoto			= STAY_ON_THIS_COMMAND
		g_flowUnsaved.scriptVars[tempLoop].failGoto			= STAY_ON_THIS_COMMAND
		g_flowUnsaved.scriptVars[tempLoop].generalInt		= 0

		#IF IS_DEBUG_BUILD
			g_flowUnsaved.scriptVars[tempLoop].debugGoto		= STAY_ON_THIS_COMMAND
		#ENDIF
	ENDREPEAT
ENDPROC




// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clears the contents of the mission flow commands array
// 
PROC CLEAR_MISSION_FLOW_COMMANDS_ARRAY()

	INT tempLoop = 0
	
	REPEAT MAX_MISSION_FLOW_COMMANDS tempLoop
		g_flowUnsaved.flowCommands[tempLoop].command		= FLOW_NONE
		g_flowUnsaved.flowCommands[tempLoop].storageType	= FST_NONE
		g_flowUnsaved.flowCommands[tempLoop].index			= ILLEGAL_ARRAY_POSITION
	ENDREPEAT
	
	g_flowUnsaved.iNumStoredFlowCommands 	= 0

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clears the mission flow commands index array
// 
PROC CLEAR_MISSION_FLOW_COMMANDS_INDEX()

	INT tempLoop = 0
	
	REPEAT MAX_STRANDS tempLoop
		g_flowUnsaved.flowCommandsIndex[tempLoop].lower		= ILLEGAL_ARRAY_POSITION
		g_flowUnsaved.flowCommandsIndex[tempLoop].upper		= ILLEGAL_ARRAY_POSITION
	ENDREPEAT

ENDPROC


// -----------------------------------------------------------------------------------------------------------
#if USE_CLF_DLC
// PURPOSE:	Clears the mission flow strands arrays
PROC CLEAR_MISSION_FLOW_SAVED_STRANDS_ARRAY_CLF()

	INT tempLoop = 0
	
		// Saved array
	REPEAT MAX_STRANDS_CLF tempLoop
		g_savedGlobalsClifford.sFlow.strandSavedVars[tempLoop].savedBitflags			= CLEAR_ALL_SAVED_STRAND_BITFLAGS
		g_savedGlobalsClifford.sFlow.strandSavedVars[tempLoop].thisCommandPos			= ILLEGAL_ARRAY_POSITION
		g_savedGlobalsClifford.sFlow.strandSavedVars[tempLoop].thisCommandHashID 		= 0
	ENDREPEAT
	
ENDPROC
#endif
#if USE_NRM_DLC
PROC CLEAR_MISSION_FLOW_SAVED_STRANDS_ARRAY_NRM()

	INT tempLoop = 0
	
		// Saved array
	REPEAT MAX_STRANDS_NRM tempLoop
		g_savedGlobalsnorman.sFlow.strandSavedVars[tempLoop].savedBitflags			= CLEAR_ALL_SAVED_STRAND_BITFLAGS
		g_savedGlobalsnorman.sFlow.strandSavedVars[tempLoop].thisCommandPos			= ILLEGAL_ARRAY_POSITION
		g_savedGlobalsnorman.sFlow.strandSavedVars[tempLoop].thisCommandHashID 		= 0
	ENDREPEAT
	
ENDPROC
#endif
PROC CLEAR_MISSION_FLOW_SAVED_STRANDS_ARRAY()

#if USE_CLF_DLC
	CLEAR_MISSION_FLOW_SAVED_STRANDS_ARRAY_CLF()
	exit
#endif
#if USE_NRM_DLC
	CLEAR_MISSION_FLOW_SAVED_STRANDS_ARRAY_NRM()
	exit
#endif
#if  not USE_CLF_DLC
#if not USE_NRM_DLC
	INT tempLoop = 0
	
		// Saved array
	REPEAT MAX_STRANDS tempLoop
		g_savedGlobals.sFlow.strandSavedVars[tempLoop].savedBitflags			= CLEAR_ALL_SAVED_STRAND_BITFLAGS
		g_savedGlobals.sFlow.strandSavedVars[tempLoop].thisCommandPos			= ILLEGAL_ARRAY_POSITION
		g_savedGlobals.sFlow.strandSavedVars[tempLoop].thisCommandHashID 		= 0
	ENDREPEAT
#endif
#endif
ENDPROC

// -----------------------------------------------------------------------------------------------------------
// PURPOSE:	Clears the mission flow synchronisation flags
#if USE_CLF_DLC
PROC CLEAR_MISSION_FLOW_SAVED_SYNCHRONISATION_ARRAY_CLF()

	INT tempLoop = 0
	REPEAT MAX_SYNCHRONISATION_IDS_CLF tempLoop
		g_savedGlobalsClifford.sFlow.controls.syncIDs[tempLoop]		= FALSE
	ENDREPEAT
	
ENDPROC
#endif
#if USE_NRM_DLC
PROC CLEAR_MISSION_FLOW_SAVED_SYNCHRONISATION_ARRAY_NRM()

	INT tempLoop = 0
	REPEAT MAX_SYNCHRONISATION_IDS_NRM tempLoop
		g_savedGlobalsnorman.sFlow.controls.syncIDs[tempLoop]		= FALSE
	ENDREPEAT
	
ENDPROC
#endif
// PURPOSE:	Clears the mission flow synchronisation flags

PROC CLEAR_MISSION_FLOW_SAVED_SYNCHRONISATION_ARRAY()

#if USE_CLF_DLC
	CLEAR_MISSION_FLOW_SAVED_SYNCHRONISATION_ARRAY_CLF()
	exit
#endif
#if USE_NRM_DLC
	CLEAR_MISSION_FLOW_SAVED_SYNCHRONISATION_ARRAY_NRM()
	exit
#endif
#if not USE_CLF_DLC
#if not USE_NRM_DLC
	INT tempLoop = 0
	REPEAT MAX_SYNCHRONISATION_IDS tempLoop
		g_savedGlobals.sFlow.controls.syncIDs[tempLoop]		= FALSE
	ENDREPEAT
#endif
#endif
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clears the mission flow flags
#if USE_CLF_DLC
PROC CLEAR_MISSION_FLOW_SAVED_FLAGS_ARRAY_CLF()

	INT tempLoop = 0	
	REPEAT MAX_FLOW_FLAG_IDS_CLF tempLoop	
		g_savedGlobalsClifford.sFlow.controls.flagIDs[tempLoop]		= FALSE
	ENDREPEAT
	
ENDPROC
#endif
#if USE_NRM_DLC
PROC CLEAR_MISSION_FLOW_SAVED_FLAGS_ARRAY_NRM()

	INT tempLoop = 0	
	REPEAT MAX_FLOW_FLAG_IDS_NRM tempLoop	
		g_savedGlobalsnorman.sFlow.controls.flagIDs[tempLoop]		= FALSE
	ENDREPEAT
	
ENDPROC
#endif
PROC CLEAR_MISSION_FLOW_SAVED_FLAGS_ARRAY()

#if USE_CLF_DLC
	CLEAR_MISSION_FLOW_SAVED_FLAGS_ARRAY_CLF()
	exit
#endif
#if USE_NRM_DLC
	CLEAR_MISSION_FLOW_SAVED_FLAGS_ARRAY_NRM()
	exit
#endif
#if not USE_CLF_DLC
#if not USE_NRM_DLC
	INT tempLoop = 0
	REPEAT MAX_FLOW_FLAG_IDS tempLoop	
		g_savedGlobals.sFlow.controls.flagIDs[tempLoop]		= FALSE
	ENDREPEAT
#endif
#endif
ENDPROC
// -----------------------------------------------------------------------------------------------------------



// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clears the mission flow ints
#if USE_CLF_DLC
PROC CLEAR_MISSION_FLOW_SAVED_INTS_ARRAY_CLF()

	INT tempLoop = 0
	
	REPEAT MAX_FLOW_INT_IDS_CLF tempLoop
		g_savedGlobalsClifford.sFlow.controls.intIDs[tempLoop]		= 0
	ENDREPEAT

	
ENDPROC
#endif
#if USE_NRM_DLC
PROC CLEAR_MISSION_FLOW_SAVED_INTS_ARRAY_NRM()

	INT tempLoop = 0
	
	REPEAT MAX_FLOW_INT_IDS_NRM tempLoop
		g_savedGlobalsnorman.sFlow.controls.intIDs[tempLoop]		= 0
	ENDREPEAT

	
ENDPROC
#endif
PROC CLEAR_MISSION_FLOW_SAVED_INTS_ARRAY()

#if USE_CLF_DLC
	CLEAR_MISSION_FLOW_SAVED_INTS_ARRAY_CLF()
	exit
#endif
#if USE_NRM_DLC
	CLEAR_MISSION_FLOW_SAVED_INTS_ARRAY_NRM()
	exit
#endif
#if not USE_CLF_DLC
#if not USE_NRM_DLC
	INT tempLoop = 0

	REPEAT MAX_FLOW_INT_IDS tempLoop
		g_savedGlobals.sFlow.controls.intIDs[tempLoop]				= 0
	ENDREPEAT
#endif
#endif
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clears the mission flow bitsets
#if  USE_CLF_DLC
PROC CLEAR_MISSION_FLOW_SAVED_BITSETS_ARRAY_CLF()

	INT tempLoop = 0

	REPEAT MAX_FLOW_BITSET_IDS_CLF tempLoop
		g_savedGlobalsClifford.sFlow.controls.bitsetIDs[tempLoop]		= CLEAR_ALL_FLOW_BITFLAGS
	ENDREPEAT


ENDPROC
#endif
#if USE_NRM_DLC
PROC CLEAR_MISSION_FLOW_SAVED_BITSETS_ARRAY_NRM()

	INT tempLoop = 0

	REPEAT MAX_FLOW_BITSET_IDS_NRM tempLoop
		g_savedGlobalsnorman.sFlow.controls.bitsetIDs[tempLoop]		= CLEAR_ALL_FLOW_BITFLAGS
	ENDREPEAT


ENDPROC
#endif
PROC CLEAR_MISSION_FLOW_SAVED_BITSETS_ARRAY()

#if USE_CLF_DLC
	CLEAR_MISSION_FLOW_SAVED_BITSETS_ARRAY_CLF()
	exit
#endif
#if USE_NRM_DLC
	CLEAR_MISSION_FLOW_SAVED_BITSETS_ARRAY_NRM()
	exit
#endif
#if not USE_CLF_DLC
#if not USE_NRM_DLC
	INT tempLoop = 0
	
	REPEAT MAX_FLOW_BITSET_IDS tempLoop
		g_savedGlobals.sFlow.controls.bitsetIDs[tempLoop]		= CLEAR_ALL_FLOW_BITFLAGS
	ENDREPEAT
#endif
#endif
ENDPROC
// -----------------------------------------------------------------------------------------------------------


// -----------------------------------------------------------------------------------------------------------
//		Clear On Gameflow Start
// -----------------------------------------------------------------------------------------------------------

#IF NOT DEFINED(CLEAR_MISSION_FLOW_NON_SAVED_GLOBALS_CUSTOM)
PROC CLEAR_MISSION_FLOW_NON_SAVED_GLOBALS_CUSTOM()
ENDPROC
#ENDIF

// PURPOSE:	Clears any mission flow globals that are not being saved.
// 
PROC CLEAR_MISSION_FLOW_NON_SAVED_GLOBALS()

	CDEBUG1LN(DEBUG_FLOW, "")
	CDEBUG1LN(DEBUG_FLOW, "============================================================================")
	CDEBUG1LN(DEBUG_FLOW, "------------------ CLEAR MISSION FLOW NON SAVED GLOBALS --------------------")
	CDEBUG1LN(DEBUG_FLOW, "============================================================================")
	CDEBUG1LN(DEBUG_FLOW, "")
	
	CLEAR_CURRENT_FLOW_STORAGE_MAXINARRAY_VARIABLES()
	CLEAR_CORE_VARIABLES_ARRAY()
	CLEAR_CORE_SMALL_VARIABLES_ARRAY()
	CLEAR_TEXT_VARIABLES_ARRAY()
	CLEAR_TEXT_LARGE_VARIABLES_ARRAY()
	
	//Not needed?
	CLEAR_MISCELLANEOUS_VARIABLES_ARRAY()
	CLEAR_SCRIPT_VARIABLES_ARRAY()
	
	CLEAR_MISSION_FLOW_COMMANDS_ARRAY()
	CLEAR_MISSION_FLOW_COMMANDS_INDEX()
	CLEAR_MISSION_FLOW_NON_SAVED_GLOBALS_CUSTOM()
ENDPROC


#if not USE_CLF_DLC
#if not USE_NRM_DLC

#IF NOT DEFINED(CLEAR_MISSION_FLOW_SAVED_GLOBALS_CUSTOM)
PROC CLEAR_MISSION_FLOW_SAVED_GLOBALS_CUSTOM()
ENDPROC
#ENDIF

// PURPOSE:	Clears any mission flow globals that are being saved.
// 
PROC CLEAR_MISSION_FLOW_SAVED_GLOBALS()

	CDEBUG1LN(DEBUG_FLOW, "")
	CDEBUG1LN(DEBUG_FLOW, "============================================================================")
	CDEBUG1LN(DEBUG_FLOW, "-------------------- CLEAR MISSION FLOW SAVED GLOBALS  ---------------------")
	CDEBUG1LN(DEBUG_FLOW, "============================================================================")
	CDEBUG1LN(DEBUG_FLOW, "")

	// Strand Control variables
	CLEAR_MISSION_FLOW_SAVED_STRANDS_ARRAY()
	
	// Synchronisation variables
	CLEAR_MISSION_FLOW_SAVED_SYNCHRONISATION_ARRAY()
	
	// Flow Flag variables
	CLEAR_MISSION_FLOW_SAVED_FLAGS_ARRAY()
	
	// Flow Int Variables
	CLEAR_MISSION_FLOW_SAVED_INTS_ARRAY()
	
	// Flow Bitset Variables
	CLEAR_MISSION_FLOW_SAVED_BITSETS_ARRAY()
	
	CLEAR_MISSION_FLOW_SAVED_GLOBALS_CUSTOM()
ENDPROC
#endif
#endif

#if USE_CLF_DLC
#IF NOT DEFINED(CLEAR_MISSION_FLOW_SAVED_GLOBALS_CUSTOM_CLF)
PROC CLEAR_MISSION_FLOW_SAVED_GLOBALS_CUSTOM_CLF()
ENDPROC
#endif
// PURPOSE:	Clears any mission flow globals that are being saved.
// 
PROC CLEAR_MISSION_FLOW_SAVED_GLOBALS_CLF()

	CDEBUG1LN(DEBUG_FLOW, "")
	CDEBUG1LN(DEBUG_FLOW, "============================================================================")
	CDEBUG1LN(DEBUG_FLOW, "------------------- CLEAR MISSION FLOW SAVED GLOBALS AGT--------------------")
	CDEBUG1LN(DEBUG_FLOW, "============================================================================")
	CDEBUG1LN(DEBUG_FLOW, "")

	// Strand Control variables
	CLEAR_MISSION_FLOW_SAVED_STRANDS_ARRAY_CLF()
	
	// Synchronisation variables
	CLEAR_MISSION_FLOW_SAVED_SYNCHRONISATION_ARRAY_CLF()
	
	// Flow Flag variables
	CLEAR_MISSION_FLOW_SAVED_FLAGS_ARRAY_CLF()
	
	// Flow Int Variables
	CLEAR_MISSION_FLOW_SAVED_INTS_ARRAY_CLF()
	
	// Flow Bitset Variables
	CLEAR_MISSION_FLOW_SAVED_BITSETS_ARRAY_CLF()
	
	CLEAR_MISSION_FLOW_SAVED_GLOBALS_CUSTOM_CLF()
ENDPROC
#endif

#if USE_NRM_DLC
#IF NOT DEFINED(CLEAR_MISSION_FLOW_SAVED_GLOBALS_CUSTOM_NRM)
PROC CLEAR_MISSION_FLOW_SAVED_GLOBALS_CUSTOM_NRM()
ENDPROC
#endif
// PURPOSE:	Clears any mission flow globals that are being saved.
// 
PROC CLEAR_MISSION_FLOW_SAVED_GLOBALS_NRM()

	CDEBUG1LN(DEBUG_FLOW, "")
	CDEBUG1LN(DEBUG_FLOW, "============================================================================")
	CDEBUG1LN(DEBUG_FLOW, "------------------- CLEAR MISSION FLOW SAVED GLOBALS NRM--------------------")
	CDEBUG1LN(DEBUG_FLOW, "============================================================================")
	CDEBUG1LN(DEBUG_FLOW, "")

	// Strand Control variables
	CLEAR_MISSION_FLOW_SAVED_STRANDS_ARRAY_NRM()
	
	// Synchronisation variables
	CLEAR_MISSION_FLOW_SAVED_SYNCHRONISATION_ARRAY_NRM()
	
	// Flow Flag variables
	CLEAR_MISSION_FLOW_SAVED_FLAGS_ARRAY_NRM()
	
	// Flow Int Variables
	CLEAR_MISSION_FLOW_SAVED_INTS_ARRAY_NRM()
	
	// Flow Bitset Variables
	CLEAR_MISSION_FLOW_SAVED_BITSETS_ARRAY_NRM()
	
	CLEAR_MISSION_FLOW_SAVED_GLOBALS_CUSTOM_NRM()
ENDPROC
#endif
// ===========================================================================================================
//		Initialise Global Variables on Gameflow Start
// ===========================================================================================================

// PURPOSE:	Sets up the Current Command again to be the lower boundary command for each strand
// 
#if USE_CLF_DLC
PROC SETUP_STRAND_COMMANDS_TO_BE_LOWER_BOUNDARY_COMMANDS_CLF()
	INT tempLoop = 0		
	REPEAT MAX_STRANDS_CLF tempLoop
		g_savedGlobalsClifford.sFlow.strandSavedVars[tempLoop].thisCommandPos	= g_flowUnsaved.flowCommandsIndex[tempLoop].lower
	ENDREPEAT	
ENDPROC
#endif

#if USE_NRM_DLC
PROC SETUP_STRAND_COMMANDS_TO_BE_LOWER_BOUNDARY_COMMANDS_NRM()
	INT tempLoop = 0		
	REPEAT MAX_STRANDS_NRM tempLoop
		g_savedGlobalsnorman.sFlow.strandSavedVars[tempLoop].thisCommandPos	= g_flowUnsaved.flowCommandsIndex[tempLoop].lower
	ENDREPEAT	
ENDPROC
#endif

PROC SETUP_STRAND_COMMANDS_TO_BE_LOWER_BOUNDARY_COMMANDS()
	
	#if USE_CLF_DLC
		SETUP_STRAND_COMMANDS_TO_BE_LOWER_BOUNDARY_COMMANDS_CLF()
		exit
	#endif
	#if USE_NRM_DLC
		SETUP_STRAND_COMMANDS_TO_BE_LOWER_BOUNDARY_COMMANDS_NRM()
		exit
	#endif
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
	INT tempLoop = 0
		REPEAT MAX_STRANDS tempLoop
			g_savedGlobals.sFlow.strandSavedVars[tempLoop].thisCommandPos	= g_flowUnsaved.flowCommandsIndex[tempLoop].lower
		ENDREPEAT		
	#endif
	#endif
ENDPROC




