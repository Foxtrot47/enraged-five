USING "commands_debug.sch"
USING "flow_debug_core.sch"
USING "flow_private_game.sch"

// ===========================================================================================================
//      Flow Flag Access Functions
// ===========================================================================================================

// PURPOSE: Sets the state of a mission flow flag.
//
// INPUT PARAMS:    paramFlagID         Flag ID having its state changed
//                  paramState          TRUE or FALSE

PROC SET_MISSION_FLOW_FLAG_STATE(FLOW_FLAG_IDS paramFlagID, BOOL paramState)

    // Ensure the flagID is valid
    IF (paramFlagID = MAX_FLOW_FLAG_IDS)
    OR (paramFlagID = FLOWFLAG_NONE)
        CERRORLN(DEBUG_FLOW, "Set_Mission_Flow_Flag_State: Illegal Flag ID - Tell Keith", Get_Flag_Display_String_From_Flag_ID(paramFlagID))
        EXIT
    ENDIF
	
#if USE_CLF_DLC
	// Has the state changed?
    IF (g_savedGlobalsClifford.sFlow.controls.flagIDs[paramFlagID] = paramState)
		CDEBUG1LN(DEBUG_FLOW, "...Mission Flow Flag has same state: ", GET_FLAG_DISPLAY_STRING_FROM_FLAG_ID(paramFlagID), " = ", GET_BOOL_DISPLAY_STRING_FROM_BOOL(paramState))
		EXIT
    ENDIF    
    // State has changed
    g_savedGlobalsClifford.sFlow.controls.flagIDs[paramFlagID] = paramState
#endif
#if USE_NRM_DLC
	// Has the state changed?
    IF (g_savedGlobalsnorman.sFlow.controls.flagIDs[paramFlagID] = paramState)
		CDEBUG1LN(DEBUG_FLOW, "...Mission Flow Flag has same state: ", GET_FLAG_DISPLAY_STRING_FROM_FLAG_ID(paramFlagID), " = ", GET_BOOL_DISPLAY_STRING_FROM_BOOL(paramState))
		EXIT
    ENDIF    
    // State has changed
    g_savedGlobalsnorman.sFlow.controls.flagIDs[paramFlagID] = paramState
#endif

#if not USE_CLF_DLC
#if not USE_NRM_DLC
	// Has the state changed?
    IF (g_savedGlobals.sFlow.controls.flagIDs[paramFlagID] = paramState)
        EXIT
    ENDIF    
    // State has changed
    g_savedGlobals.sFlow.controls.flagIDs[paramFlagID] = paramState
#endif
#endif   
    
    CDEBUG1LN(DEBUG_FLOW, "...Mission Flow Flag has changed state: ", GET_FLAG_DISPLAY_STRING_FROM_FLAG_ID(paramFlagID), " = ", GET_BOOL_DISPLAY_STRING_FROM_BOOL(paramState))
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Returns Mission Flow Flag State.
//
// INPUT PARAMS:    paramFlagID         Flag ID
// RETURN VALUE:    BOOL                Current state (TRUE or FALSE)

FUNC BOOL GET_MISSION_FLOW_FLAG_STATE(FLOW_FLAG_IDS paramFlagID)

    // Ensure the flagID is valid
    IF (paramFlagID = MAX_FLOW_FLAG_IDS)
    OR (paramFlagID = FLOWFLAG_NONE)
        CERRORLN(DEBUG_FLOW, "Set_Mission_Flow_Flag_State: Illegal Flag ID - Tell Keith - RETURNING FALSE", GET_FLAG_DISPLAY_STRING_FROM_FLAG_ID(paramFlagID))
        RETURN FALSE
    ENDIF
	
#if USE_CLF_DLC
	// Return the current state
    RETURN (g_savedGlobalsClifford.sFlow.controls.flagIDs[paramFlagID])
#endif
#if USE_NRM_DLC
	// Return the current state
    RETURN (g_savedGlobalsnorman.sFlow.controls.flagIDs[paramFlagID])
#endif

#if not USE_CLF_DLC
#if not USE_NRM_DLC
    // Return the current state
    RETURN (g_savedGlobals.sFlow.controls.flagIDs[paramFlagID])
#endif  
#endif

ENDFUNC




// ===========================================================================================================
//      Flow Int Access Functions
// ===========================================================================================================

// PURPOSE: Sets the value of a mission flow int.
//
// INPUT PARAMS:    paramIntID          Int ID having its value changed
//                  paramValue          The int value

PROC SET_MISSION_FLOW_INT_VALUE(FLOW_INT_IDS paramIntID, INT paramValue)

    // Ensure the intID is valid
    IF (paramIntID = MAX_FLOW_INT_IDS)
    OR (paramIntID = FLOWINT_NONE)
		CERRORLN(DEBUG_FLOW, "Set_Mission_Flow_Int_Value: Illegal Int ID - Tell Keith", GET_INT_DISPLAY_STRING_FROM_INT_ID(paramIntID))
       
        EXIT
    ENDIF

#if USE_CLF_DLC
	// Has the value changed?
    IF (g_savedGlobalsClifford.sFlow.controls.intIDs[paramIntID] = paramValue)
        EXIT
    ENDIF    
    // Value has changed
    g_savedGlobalsClifford.sFlow.controls.intIDs[paramIntID] = paramValue
#endif
#if USE_NRM_DLC
	// Has the value changed?
    IF (g_savedGlobalsnorman.sFlow.controls.intIDs[paramIntID] = paramValue)
        EXIT
    ENDIF    
    // Value has changed
    g_savedGlobalsnorman.sFlow.controls.intIDs[paramIntID] = paramValue
#endif

#if not USE_CLF_DLC
#if not USE_NRM_DLC
    // Has the value changed?
    IF (g_savedGlobals.sFlow.controls.intIDs[paramIntID] = paramValue)
        EXIT
    ENDIF    
    // Value has changed
    g_savedGlobals.sFlow.controls.intIDs[paramIntID] = paramValue
#endif 	
#endif

    CDEBUG1LN(DEBUG_FLOW, "...Mission Flow Int has changed value: ", GET_INT_DISPLAY_STRING_FROM_INT_ID(paramIntID), " = ", paramValue)
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Returns Mission Flow Int Value.
//
// INPUT PARAMS:    paramIntID          Int ID
// RETURN VALUE:    INT                 Current value

FUNC INT GET_MISSION_FLOW_INT_VALUE(FLOW_INT_IDS paramIntID)

    
    // Ensure the intID is valid
    IF (paramIntID = MAX_FLOW_INT_IDS)
    OR (paramIntID = FLOWINT_NONE)
        CERRORLN(DEBUG_FLOW, "Get_Mission_Flow_Int_Value: Illegal Int ID - Tell Keith - RETURNING 0", GET_INT_DISPLAY_STRING_FROM_INT_ID(paramIntID))
        RETURN 0
    ENDIF
 
#if USE_CLF_DLC
	// Return the current value
    RETURN (g_savedGlobalsClifford.sFlow.controls.intIDs[paramIntID])
#endif
#if USE_NRM_DLC
	// Return the current value
    RETURN (g_savedGlobalsnorman.sFlow.controls.intIDs[paramIntID])
#endif

#if not USE_CLF_DLC
#if not USE_NRM_DLC
    // Return the current value
    RETURN (g_savedGlobals.sFlow.controls.intIDs[paramIntID])
#endif  	
#endif

ENDFUNC




// ===========================================================================================================
//      Flow Bitset Access Functions
// ===========================================================================================================

// PURPOSE: Sets the state of a mission flow bitset bit.
//
// INPUT PARAMS:    paramBitsetID       Bitset ID having a state changed
//                  paramBit            Bitflag as an Int of the bit being modified
//                  paramState          TRUE or FALSE

PROC SET_MISSION_FLOW_BITSET_BIT_STATE(FLOW_BITSET_IDS paramBitsetID, INT paramBit, BOOL paramState)

     // Ensure the BitsetID is valid
    IF (paramBitsetID = MAX_FLOW_BITSET_IDS)
    OR (paramBitsetID = FLOWBITSET_NONE)
        CERRORLN(DEBUG_FLOW, "Set_Mission_Flow_Bitset_Bit_State: Illegal Bitset ID - Tell Keith", GET_BITSET_DISPLAY_STRING_FROM_BITSET_ID(paramBitsetID))
        EXIT
    ENDIF
    
    // Ensure the Bit is valid
    IF (paramBit < 0)
    OR (paramBit >= 32)
        CERRORLN(DEBUG_FLOW, "Set_Mission_Flow_Bitset_Bit_State: Illegal Bit (should be between 0 and 31) - Tell Keith")
        EXIT
    ENDIF

#if USE_CLF_DLC
	// Has the state changed?
    BOOL bitState = IS_BIT_SET(g_savedGlobalsClifford.sFlow.controls.bitsetIDs[paramBitsetID], paramBit)
    IF (bitState = paramState)
        EXIT
    ENDIF    
    // State has changed
    IF (paramState)
        SET_BIT(g_savedGlobalsClifford.sFlow.controls.bitsetIDs[paramBitsetID], paramBit)
    ELSE
        CLEAR_BIT(g_savedGlobalsClifford.sFlow.controls.bitsetIDs[paramBitsetID], paramBit)
    ENDIF
#endif
#if USE_NRM_DLC
	// Has the state changed?
    BOOL bitState = IS_BIT_SET(g_savedGlobalsnorman.sFlow.controls.bitsetIDs[paramBitsetID], paramBit)
    IF (bitState = paramState)
        EXIT
    ENDIF    
    // State has changed
    IF (paramState)
        SET_BIT(g_savedGlobalsnorman.sFlow.controls.bitsetIDs[paramBitsetID], paramBit)
    ELSE
        CLEAR_BIT(g_savedGlobalsnorman.sFlow.controls.bitsetIDs[paramBitsetID], paramBit)
    ENDIF
#endif

#if not USE_CLF_DLC
#if not USE_NRM_DLC
    // Has the state changed?
    BOOL bitState = IS_BIT_SET(g_savedGlobals.sFlow.controls.bitsetIDs[paramBitsetID], paramBit)
    IF (bitState = paramState)
        EXIT
    ENDIF    
    // State has changed
    IF (paramState)
        SET_BIT(g_savedGlobals.sFlow.controls.bitsetIDs[paramBitsetID], paramBit)
    ELSE
        CLEAR_BIT(g_savedGlobals.sFlow.controls.bitsetIDs[paramBitsetID], paramBit)
    ENDIF
#endif  	
#endif

    CDEBUG1LN(DEBUG_FLOW, "...Mission Flow Bitset bit has changed state: ", GET_BITSET_DISPLAY_STRING_FROM_BITSET_ID(paramBitsetID), " (BIT ", paramBit, ") = ", GET_BOOL_DISPLAY_STRING_FROM_BOOL(paramState))
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Returns Mission Flow Bitset Bit State.
//
// INPUT PARAMS:    paramBitsetID       Bitset ID
//                  paramBit            Bitflag as an Int of the bit being modified
// RETURN VALUE:    BOOL                Current state (TRUE (1) or FALSE (0))

FUNC BOOL GET_MISSION_FLOW_BITSET_BIT_STATE(FLOW_BITSET_IDS paramBitsetID, INT paramBit)

    // Ensure the flagID is valid
    IF (paramBitsetID = MAX_FLOW_BITSET_IDS)
    OR (paramBitsetID = FLOWBITSET_NONE)
        CERRORLN(DEBUG_FLOW, "Get_Mission_Flow_Bitset_Bit_State: Illegal Bitset ID - Tell Keith - RETURNING FALSE", GET_BITSET_DISPLAY_STRING_FROM_BITSET_ID(paramBitsetID))
        RETURN FALSE
    ENDIF
    
    // Ensure the Bit is valid
    IF (paramBit < 0)
    OR (paramBit >= 32)
        CERRORLN(DEBUG_FLOW, "Get_Mission_Flow_Bitset_Bit_State: Illegal Bit (should be between 0 and 31) - Tell Keith - RETURNING FALSE")
        RETURN FALSE
    ENDIF
	
#if USE_CLF_DLC
	// Return the current state of the bitset bit
    BOOL bitState = IS_BIT_SET(g_savedGlobalsClifford.sFlow.controls.bitsetIDs[paramBitsetID], paramBit)
    RETURN bitState
#endif
#if USE_NRM_DLC
	// Return the current state of the bitset bit
    BOOL bitState = IS_BIT_SET(g_savedGlobalsnorman.sFlow.controls.bitsetIDs[paramBitsetID], paramBit)
    RETURN bitState
#endif

#if not USE_CLF_DLC
#if not USE_NRM_DLC
    // Return the current state of the bitset bit
    BOOL bitState = IS_BIT_SET(g_savedGlobals.sFlow.controls.bitsetIDs[paramBitsetID], paramBit)
    RETURN bitState
#endif     
#endif 

ENDFUNC
