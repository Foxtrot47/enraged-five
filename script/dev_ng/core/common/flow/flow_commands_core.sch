#IF NOT DEFINED(FLOW_COMMAND_IDS)
CONST_INT   ONLY_CORE_COMMANDS      1

HASH_ENUM FLOW_COMMAND_IDS
#ENDIF
	// ------------------------------
	// Core flow management commands.
	// ------------------------------

	// Strand management commands.
	FLOW_ACTIVATE_STRAND,					// Tells a strand to activate.
	FLOW_AWAIT_ACTIVATION,					// Waits until a strand has been told to activate.
	FLOW_KILL_STRAND,						// Kills a strand.
	FLOW_TERMINATE,							// Terminates a strand.

	// Jump commands.
	FLOW_GOTO,								// Goto a jump label.
	FLOW_STRAND_GOTO,						// Set strand at jump label.
	FLOW_LABEL,								// Stores a jump label - used by GOTO and other jump instructions.
#IF IS_DEBUG_BUILD
	FLOW_LABEL_LAUNCHER_ONLY,				// Debug only command, should NOT be used in Final builds.
#ENDIF
	
	// Branching jump commands.
	FLOW_STORE_FLAG_STATE,					// Sets the state of the specified flag to TRUE or FALSE.
	FLOW_STORE_INT_VALUE,					// Sets the value of the specified int.
	FLOW_STORE_BITSET_BIT_STATE,			// Sets the state of the specified bit of the bitset to TRUE (1) or FALSE (0).
	FLOW_DO_FLAG_JUMPS,						// Performs either a TRUE or a FALSE jump depending on the state of the flag.
	FLOW_DO_INT_JUMPS,						// Performs an EQUAL or a NOTEQUAL jump based on comparison of Int with a Compare Value.
	FLOW_DO_BITSET_JUMPS,					// Performs either a TRUE or a FALSE jump depending on the state of the bitflag in a bitset.
	FLOW_DO_STRAND_KILLED_JUMPS,			// Performs a KILLED or NOTKILLED jump based on whether the specified strand has terminated or not.
	
	// Strand-to-strand synchronisation commands.
	FLOW_AWAIT_SYNCHRONISATION,				// Waits until a strand receives a 'reached synchronisation point' message from another strand.
	FLOW_SEND_SYNCHRONISATION,				// Sends a 'reached synchronisation point' message to another strand.

	// Time management commands.
	FLOW_SET_GAME_CLOCK,					// Sets the in-game clock to a specific time and/or date.
	FLOW_WAIT_IN_GAME_TIME,					// Tells a strand to hang for a period of time measured by the in-game clock.
	FLOW_WAIT_IN_REAL_TIME,					// Tells a strand to hang for a period of time measured in real world time.
	FLOW_WAIT_UNTIL_GAME_HOUR,				// Tells a strand to hang until a specified hour is reached on the in-game clock.
	FLOW_WAIT_UNTIL_GAME_TIME_AND_DATE,		// Tells a strand to hang until a specified time and date is reached on the in-game clock.
	FLOW_IS_GAME_TIME_AFTER_HOUR,			// Performs either a TRUE or a FALSE jump based on whether the game clock is after the specified hour.

	FLOW_DEBUG_DUMMY,						// Debug command dummy replacement for Final. Insert these instead of debug commands to keep flow 
											// command indexes consistent between release and debug saves.
#IF DEFINED(ONLY_CORE_COMMANDS)
    // Leave this at the bottom.
    FLOW_NONE = -1
ENDENUM
#ENDIF
