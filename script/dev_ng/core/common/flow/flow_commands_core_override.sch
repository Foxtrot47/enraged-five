#IF NOT DEFINED(FLOW_COMMAND_IDS)
CONST_INT   ONLY_CORE_OVERRIDE_COMMANDS      1
ENUM FLOW_COMMAND_IDS
	USING "flow_commands_core.sch"
#ENDIF	

	// Game-neutral common commands
 	FLOW_DO_MISSION_AT_BLIP,                        // Sets up a contact point mission and controls it until it passes or fails
 	FLOW_DO_MISSION_NOW,                            // Launches a mission immediately and controls it until it passes or fails
 	FLOW_IS_MISSION_COMPLETED,						// Does a jump based on whether or not a mission has been completed yet.
	FLOW_LAUNCH_SCRIPT_AND_FORGET,                  // Launches a standalone script - the mission flow takes no more interest in this script
	FLOW_RUN_CODE_ID,                               // Triggers the execution of a custom block of script bound to a code ID.
	FLOW_ENABLE_BLIP_LONG_RANGE,                    // Sets a static blip as long-range (appears on radar edge)
	FLOW_DISABLE_BLIP_LONG_RANGE,                   // Stops a static blip from being long-range (no longer appears on radar edge)
	FLOW_SET_BUILDING_STATE,                        // Sets the state of any building that has swappable building states.
	FLOW_SET_WEAPON_LOCK_STATE,                     // Sets whether a weapon should be unlocked or not.
	FLOW_ACTIVATE_RANDOMCHAR_MISSION,               // Sets a RandomChar mission to be available to play in the gameflow.
	FLOW_DEACTIVATE_RANDOMCHAR_MISSION,				// Sets a previously activated RandomChar mission to be unavailable to play in the gameflow.
	FLOW_SET_STATIC_BLIP_STATE,                     // Sets whether a static blip is active or not
	FLOW_DISPLAY_HELP_TEXT,                         // Triggers a help text pop-up on screen.
	FLOW_CANCEL_HELP_TEXT,                        	// Unqueues a certain help message if it is currently queued.
	FLOW_WAIT_FOR_AUTOSAVE,							// Stays on command while an autosave is in progress
#IF IS_DEBUG_BUILD
	FLOW_DEBUG_RUN_CODE_ID,							// Triggers the execution of a custom block of script bound to a code ID that only runs in debug.
#ENDIF

#IF DEFINED(ONLY_CORE_OVERRIDE_COMMANDS)
    // Leave this at the bottom.
    FLOW_NONE = -1
ENDENUM
#ENDIF
