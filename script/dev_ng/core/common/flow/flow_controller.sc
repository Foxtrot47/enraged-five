

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_misc.sch"
USING "commands_hud.sch"
USING "debug_channels_structs.sch"

USING "flow_processing_core.sch"

USING "flow_controller_GAME.sch"	
USING "flow_controller_core_override.sch"	

	USING "flow_debug_core.sch"
#IF IS_DEBUG_BUILD
	USING "flow_widgets_core.sch"
#ENDIF

// Set up the current profiler level
#IF IS_DEBUG_BUILD
	#IF MISSION_FLOW_PROFILING
		USING "profiler.sch"
	#ENDIF
#ENDIF

// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	flow_controller.sc
//		AUTHOR			:	Keith
//		DESCRIPTION		:	Controls the Mission Flow.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************


INT 	m_iStrandIndex = 0						// Index used to track which strand we are currently processing. Persists across frames to allow scheduling.
INT 	m_iTerminatedStrandCount = 0			// Tracks how many strands were found terminated. Used to determine when the flow has been completed.
BOOL	m_bStrandChangedCommandThisPass = FALSE	// Denotes whether any strand has stepped to a new command on the current pass of all strands. Used 
												// to dictate whether the controller is busy or not.

// Set up the current profiler level
#IF IS_DEBUG_BUILD
	#IF MISSION_FLOW_PROFILING
		INT m_profilerDisplayLevel = 0
	#ENDIF
#ENDIF


// ===========================================================================================================
//		Cleanup
// ===========================================================================================================

// PURPOSE: Ensures that the script gets a chance to cleanup under specific circumstances (ie: moving from SP to MP)
// 
PROC SCRIPT_CLEANUP()
	CPRINTLN(DEBUG_FLOW, "Flow controller cleaning up and terminating.")

	FLOW_RUN_CUSTOM_CLEANUP()
	MISSION_FLOW_CLEAR_DISPLAY_MISSION_TITLE()//1612717
	TERMINATE_THIS_THREAD()
ENDPROC

// ===========================================================================================================
//		Commands Distribution
// ===========================================================================================================

// PURPOSE:	Check if the current command for the strand has completed
//
// INPUT PARAMS:		paramStrandID			Strand ID for strand command belongs to
//						paramCommandArrayPos	Array position within the commands array for this command

PROC MAINTAIN_CURRENT_MISSION_FLOW_COMMAND(STRANDS paramStrandID, INT paramCommandArrayPos)

	JUMP_LABEL_IDS		eNextJumpLabel		= STAY_ON_THIS_COMMAND
	FLOW_COMMAND_IDS	eCommand			= g_flowUnsaved.flowCommands[paramCommandArrayPos].command
	INT					iStorageIndex		= g_flowUnsaved.flowCommands[paramCommandArrayPos].index
	
	SWITCH (eCommand)
			
		// Dummy replacement for a debug command. Do nothing and step to next command.
		CASE FLOW_DEBUG_DUMMY
			eNextJumpLabel = NEXT_SEQUENTIAL_COMMAND
			BREAK

		// Activate the specified strand by setting the activation BIT to TRUE
		CASE FLOW_ACTIVATE_STRAND
			eNextJumpLabel = PERFORM_ACTIVATE_STRAND(iStorageIndex)
			BREAK
			
			
		// Increment strand's mission flow pointer once the 'activation' bitflag is TRUE.
		// Strands won't be processed until the bitflag is set so if this command is being
		// processed we can assume the bitflag is set. Always step to next command.
		CASE FLOW_AWAIT_ACTIVATION
			#IF IS_DEBUG_BUILD
				IF IS_FLOW_CONSOLE_LOG_READY_TO_PRINT()
					CDEBUG1LN(DEBUG_FLOW_COMMANDS, "COMMAND: Await Activation()")
				ENDIF
			#ENDIF
			eNextJumpLabel = NEXT_SEQUENTIAL_COMMAND
			BREAK
			
			
		// Kill the specified strand by setting the termination BIT to TRUE
		CASE FLOW_KILL_STRAND
			eNextJumpLabel = PERFORM_KILL_STRAND(iStorageIndex)
			BREAK
			
			
		// Set the 'terminated' bitflag for the strand we're operating on.
		CASE FLOW_TERMINATE
			#IF IS_DEBUG_BUILD
				IF IS_FLOW_CONSOLE_LOG_READY_TO_PRINT()
					CDEBUG1LN(DEBUG_FLOW_COMMANDS, "COMMAND: Terminate strand()")
				ENDIF
			#ENDIF
			SET_BIT(g_savedGlobals.sFlow.strandSavedVars[paramStrandID].savedBitflags, SAVED_BITS_STRAND_TERMINATED)
			eNextJumpLabel = STAY_ON_THIS_COMMAND
			BREAK


		// Goto a label
		CASE FLOW_GOTO
			eNextJumpLabel = PERFORM_GOTO(iStorageIndex)
			BREAK	
			
		case FLOW_STRAND_GOTO
			eNextJumpLabel = PERFORM_STRAND_GOTO(iStorageIndex)
			BREAK
			
		// Store a Label. This command is ignored during processing, but a jump instruction will look for this label 
		// and set this as the next command.
		CASE FLOW_LABEL
			#IF IS_DEBUG_BUILD
				IF IS_FLOW_CONSOLE_LOG_READY_TO_PRINT()
					CDEBUG1LN(DEBUG_FLOW_COMMANDS, "COMMAND: Label()")
				ENDIF
			#ENDIF

			eNextJumpLabel = NEXT_SEQUENTIAL_COMMAND
			BREAK
			
			
#IF IS_DEBUG_BUILD
		// Store a Label. This command is ignored during processing, but a jump instruction will look for this label
		// and set this as the next command.
		CASE FLOW_LABEL_LAUNCHER_ONLY
			IF IS_FLOW_CONSOLE_LOG_READY_TO_PRINT()
				CDEBUG1LN(DEBUG_FLOW_COMMANDS, "COMMAND: Label Launcher Only()")
			ENDIF
		
			eNextJumpLabel = NEXT_SEQUENTIAL_COMMAND
			BREAK
		
		//Debug only version of run code ID. Won't exist in release scripts but do nothing when running it
		//in debug scripts.
		CASE FLOW_DEBUG_RUN_CODE_ID
			eNextJumpLabel = NEXT_SEQUENTIAL_COMMAND
			BREAK
#ENDIF
	
	
		// Store the state of a Flow Flag
		CASE FLOW_STORE_FLAG_STATE
			eNextJumpLabel = PERFORM_STORE_FLAG_STATE(iStorageIndex)
			BREAK
			
			
		// Store the value of an Int Flag
		CASE FLOW_STORE_INT_VALUE
			eNextJumpLabel = PERFORM_STORE_INT_VALUE(iStorageIndex)
			BREAK
			
			
		// Store the state of a Bitset Bit
		CASE FLOW_STORE_BITSET_BIT_STATE
			eNextJumpLabel = PERFORM_STORE_BITSET_BIT_STATE(iStorageIndex)
			BREAK
			
			
		// Perform a Jump based on a Flow Flag being TRUE or FALSE
		CASE FLOW_DO_FLAG_JUMPS
			eNextJumpLabel = PERFORM_DO_FLAG_JUMPS(iStorageIndex)
			BREAK
			
			
		// Perform a Jump based on a Flow Int being compared with a specified int value
		CASE FLOW_DO_INT_JUMPS
			eNextJumpLabel = PERFORM_DO_INT_JUMPS(iStorageIndex)
			BREAK
			
			
		// Perform a Jump based on a Flow Bitset Bit being TRUE or FALSE
		CASE FLOW_DO_BITSET_JUMPS
			eNextJumpLabel = PERFORM_DO_BITSET_JUMPS(iStorageIndex)
			BREAK
			
			
		// Perform a Jump based on whether a specified strand has terminated or not
		CASE FLOW_DO_STRAND_KILLED_JUMPS
			eNextJumpLabel = PERFORM_DO_STRAND_KILLED_JUMPS(iStorageIndex)
			BREAK
			
			
		// Await Synchronisation from another strand before continuing processing this strand
		CASE FLOW_AWAIT_SYNCHRONISATION
			eNextJumpLabel = PERFORM_AWAIT_SYNCHRONISATION(iStorageIndex)
			BREAK
			
			
		// Send Synchronisation to allow another strand to proceed.
		CASE FLOW_SEND_SYNCHRONISATION
			eNextJumpLabel = PERFORM_SEND_SYNCHRONISATION(iStorageIndex)
			BREAK
			
			
		// Update the game clock to be set at a specified time and date.
		CASE FLOW_SET_GAME_CLOCK
			eNextJumpLabel = PERFORM_SET_GAME_CLOCK(iStorageIndex)
			BREAK
			
			
		// Hang the strand for a set period of time measured by the in-game clock.
		CASE FLOW_WAIT_IN_GAME_TIME
			eNextJumpLabel = PERFORM_WAIT_IN_GAME_TIME(paramStrandID, iStorageIndex)
			BREAK
		
		
		// Hang the strand for a set period of time measured in real world time.
		CASE FLOW_WAIT_IN_REAL_TIME
			eNextJumpLabel = PERFORM_WAIT_IN_REAL_TIME(paramStrandID, iStorageIndex)
			BREAK
			
			
		// Hang the strand until a specified hour is reached on the in-game clock.
		CASE FLOW_WAIT_UNTIL_GAME_HOUR
			eNextJumpLabel = PERFORM_WAIT_UNTIL_GAME_HOUR(paramStrandID, iStorageIndex)
			BREAK
			
		// Hang the strand until a specified time is reached on the in-game clock.
		CASE FLOW_WAIT_UNTIL_GAME_TIME_AND_DATE
			eNextJumpLabel = PERFORM_WAIT_UNTIL_GAME_TIME_AND_DATE(paramStrandID, iStorageIndex)
			BREAK
		
		// Perform a Jump based on whether the game clock hour is higher than a specified hour.
		CASE FLOW_IS_GAME_TIME_AFTER_HOUR
			eNextJumpLabel = PERFORM_IS_GAME_TIME_AFTER_HOUR(iStorageIndex)
			BREAK
			
		// Activate the specified strand by setting the activation BIT to TRUE
		CASE FLOW_ACTIVATE_RANDOMCHAR_MISSION
			eNextJumpLabel = PERFORM_ACTIVATE_RANDOMCHAR_MISSION(iStorageIndex)
			BREAK
			
		// Deactivate the specified strand by setting the activation BIT to FALSE
		CASE FLOW_DEACTIVATE_RANDOMCHAR_MISSION
			eNextJumpLabel = PERFORM_DEACTIVATE_RANDOMCHAR_MISSION(iStorageIndex)
			BREAK	
			
		// Displays a sequence of help text messages to the player.
		CASE FLOW_DISPLAY_HELP_TEXT
			eNextJumpLabel = PERFORM_DISPLAY_HELP_TEXT(iStorageIndex)
			BREAK
			
		// Displays a sequence of help text messages to the player.
		CASE FLOW_CANCEL_HELP_TEXT
			eNextJumpLabel = PERFORM_CANCEL_HELP_TEXT(iStorageIndex)
			BREAK
			
		// Setup and perform a mission launched at a blip, waiting for pass or fail
		CASE FLOW_DO_MISSION_AT_BLIP
			eNextJumpLabel = PERFORM_DO_MISSION_AT_BLIP(paramStrandID, iStorageIndex)
			BREAK
	
		// Setup and perform a mission launched immediately, waiting for pass or fail
		CASE FLOW_DO_MISSION_NOW
			eNextJumpLabel = PERFORM_DO_MISSION_NOW(iStorageIndex, paramStrandID)
			BREAK

		// Launch a standalone script - mission flow then ignores it
		CASE FLOW_LAUNCH_SCRIPT_AND_FORGET
			eNextJumpLabel = PERFORM_LAUNCH_SCRIPT_AND_FORGET(iStorageIndex)
			BREAK

		// Run a custom block of script bound to a code ID.
		CASE FLOW_RUN_CODE_ID
			eNextJumpLabel = PERFORM_RUN_CODE_ID(iStorageIndex)
			BREAK
			
		
			
		CASE FLOW_ENABLE_BLIP_LONG_RANGE
			eNextJumpLabel = PERFORM_ENABLE_BLIP_LONG_RANGE(iStorageIndex)
			BREAK
			
		CASE FLOW_DISABLE_BLIP_LONG_RANGE
			eNextJumpLabel = PERFORM_DISABLE_BLIP_LONG_RANGE(iStorageIndex)
			BREAK

		CASE FLOW_SET_BUILDING_STATE
			eNextJumpLabel = PERFORM_SET_BUILDING_STATE(iStorageIndex)
			BREAK
			
		CASE FLOW_SET_WEAPON_LOCK_STATE
			eNextJumpLabel = PERFORM_SET_WEAPON_LOCK_STATE(iStorageIndex)
			BREAK
			
		CASE FLOW_SET_STATIC_BLIP_STATE
			eNextJumpLabel = PERFORM_SET_STATIC_BLIP_STATE(iStorageIndex)
			BREAK
			
		DEFAULT
			IF NOT MAINTAIN_CURRENT_MISSION_FLOW_COMMAND_CUSTOM(eCommand, iStorageIndex, paramStrandID, eNextJumpLabel)
				CERRORLN(DEBUG_FLOW, "Update_Strand: Unknown Command ID. Command:", GET_COMMAND_DISPLAY_STRING_FROM_COMMAND_ID(eCommand), " Strand:", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(paramStrandID), ".")
				SCRIPT_ASSERT("Update_Strand: Unknown Command ID.")

				// Just move on to the next command
				eNextJumpLabel = NEXT_SEQUENTIAL_COMMAND
			ENDIF
		BREAK
	ENDSWITCH
	
	// Move the command pointer.
	IF NOT (IS_BIT_SET(g_savedGlobals.sFlow.strandSavedVars[paramStrandID].savedBitflags, SAVED_BITS_STRAND_TERMINATED))
		IF eNextJumpLabel <> STAY_ON_THIS_COMMAND
			g_flowUnsaved.bFlowControllerBusy = TRUE
			m_bStrandChangedCommandThisPass = TRUE
		ENDIF
		FLOW_FOLLOW_JUMP(paramStrandID, eNextJumpLabel)
	ENDIF
	
ENDPROC




// ===========================================================================================================
//		Processing For One Strand
// ===========================================================================================================

// PURPOSE:	Update one command for one mission flow strand
//
// INPUT PARAMS:	paramStrand					STRAND ID
//					paramUpdateUntilBlocked		If this flag is TRUE, continue to update the strand this frame until we hit a blocking command.

PROC UPDATE_STRAND(STRANDS paramStrandID, BOOL paramUpdateUntilBlocked = FALSE)
	
	// Perform some consistency checking within the commands array
	INT iCommandPos			= g_savedGlobals.sFlow.strandSavedVars[paramStrandID].thisCommandPos
	INT iLastCommandPos 	= iCommandPos
	INT iLowerBounds		= g_flowUnsaved.flowCommandsIndex[paramStrandID].lower
	INT iUpperBounds		= g_flowUnsaved.flowCommandsIndex[paramStrandID].upper
	
	IF (iCommandPos < iLowerBounds)
		CERRORLN(DEBUG_FLOW, "Update_Strand: Lower Command Array Boundary is illegal for this strand", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID( paramStrandID))
		
		EXIT
	ENDIF
	
	IF (iCommandPos > iUpperBounds)
		CERRORLN(DEBUG_FLOW, "Update_Strand: Upper Command Array Boundary is illegal for this strand", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID( paramStrandID))
		
		EXIT
	ENDIF
	
	// Debug output - prefix for strand
	#IF IS_DEBUG_BUILD
		IF IS_FLOW_CONSOLE_LOG_READY_TO_PRINT()
			STRING strandString = GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(paramStrandID)
			INT stringLength = GET_LENGTH_OF_LITERAL_STRING(strandString)
			CDEBUG1LN(DEBUG_FLOW, strandString, GET_REQUIRED_SPACES_AS_A_DISPLAY_STRING(MAX_STRAND_NAME_DISPLAY_LENGTH-stringLength), " :   ", PICK_STRING( NOT (IS_BIT_SET(g_savedGlobals.sFlow.strandSavedVars[paramStrandID].savedBitflags, SAVED_BITS_STRAND_ACTIVATED)), PICK_STRING(IS_BIT_SET(g_savedGlobals.sFlow.strandSavedVars[paramStrandID].savedBitflags, SAVED_BITS_STRAND_TERMINATED), "terminated", "active"), "awaiting activation"))
		ENDIF
	#ENDIF
	
	// Do nothing if the strand is still awaiting activation
	IF NOT (IS_BIT_SET(g_savedGlobals.sFlow.strandSavedVars[paramStrandID].savedBitflags, SAVED_BITS_STRAND_ACTIVATED))
		EXIT
	ENDIF
	
	// Do nothing if the strand has already terminated
	IF (IS_BIT_SET(g_savedGlobals.sFlow.strandSavedVars[paramStrandID].savedBitflags, SAVED_BITS_STRAND_TERMINATED))
		//Incremented terminated strand count.
		m_iTerminatedStrandCount++
		EXIT
	ENDIF
	
	// Process the strand's current flow command.
	MAINTAIN_CURRENT_MISSION_FLOW_COMMAND(paramStrandID, iCommandPos)
	
	// Optimisation: If the next command is a label, process it immediately.
	iCommandPos = g_savedGlobals.sFlow.strandSavedVars[paramStrandID].thisCommandPos
	IF g_flowUnsaved.flowCommands[iCommandPos].command = FLOW_LABEL
#IF IS_DEBUG_BUILD
	OR g_flowUnsaved.flowCommands[iCommandPos].command = FLOW_LABEL_LAUNCHER_ONLY
#ENDIF
		MAINTAIN_CURRENT_MISSION_FLOW_COMMAND(paramStrandID, iCommandPos)
	ENDIF
	
	// Optimisation: UpdateUntilBlocked forces a strand to keep updating this frame until it hits a blocking command.
	IF paramUpdateUntilBlocked
		CDEBUG1LN(DEBUG_FLOW, "Fast-tracking strand ", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(paramStrandID), ".")
		WHILE iLastCommandPos != g_savedGlobals.sFlow.strandSavedVars[paramStrandID].thisCommandPos
			iLastCommandPos = g_savedGlobals.sFlow.strandSavedVars[paramStrandID].thisCommandPos
			MAINTAIN_CURRENT_MISSION_FLOW_COMMAND(paramStrandID, g_savedGlobals.sFlow.strandSavedVars[paramStrandID].thisCommandPos)
			CDEBUG1LN(DEBUG_FLOW, "Fast-tracked command ", GET_COMMAND_DISPLAY_STRING_FROM_COMMAND_ID(g_flowUnsaved.flowCommands[iLastCommandPos].command), ".") 
		ENDWHILE
		CDEBUG1LN(DEBUG_FLOW, "Fast-tracking ended on blocking command ", GET_COMMAND_DISPLAY_STRING_FROM_COMMAND_ID(g_flowUnsaved.flowCommands[g_savedGlobals.sFlow.strandSavedVars[paramStrandID].thisCommandPos].command), ".") 
	ENDIF

ENDPROC


// ===========================================================================================================
//		Processing For All Strands
// ===========================================================================================================

// PURPOSE: Update each mission flow strand each frame
// 
PROC MAINTAIN_MISSION_FLOW()

	STRANDS	loopAsStrandID = STRAND_NONE
	
	// Debug output
	#IF IS_DEBUG_BUILD
		IF IS_FLOW_CONSOLE_LOG_READY_TO_PRINT()
			CDEBUG1LN(DEBUG_FLOW, "")
			CDEBUG1LN(DEBUG_FLOW, "----------------------------------------------------------------------------")
			CDEBUG1LN(DEBUG_FLOW, "---- MISSION FLOW COMMANDS PROCESSING UPDATE -------------------------------")
			CDEBUG1LN(DEBUG_FLOW, "----------------------------------------------------------------------------")
		ENDIF
	#ENDIF
	
	// Maintain the state of active missions.
	FLOW_RUN_CUSTOM_UPDATE()

	//Work out how many strands to process this frame
	INT iStrandsProcessedThisFrame = 0
	INT iStrandsToProcessThisFrame
	
	//Has the flow been requested to only process a specific strand?
	IF g_flowUnsaved.eLockInStrand != STRAND_NONE
		iStrandsToProcessThisFrame = 1
		m_iTerminatedStrandCount = 0
		m_iStrandIndex = ENUM_TO_INT(g_flowUnsaved.eLockInStrand)
	//Has the flow been requested to process every strand this frame?
	ELIF g_flowUnsaved.bFlowProcessAllStrandsNextFrame
		CDEBUG1LN(DEBUG_FLOW, "The flow was requested to process all strands this frame.")
		m_iStrandIndex = 0
		m_iTerminatedStrandCount = 0
		iStrandsToProcessThisFrame = ENUM_TO_INT(MAX_STRANDS)
		g_flowUnsaved.bFlowProcessAllStrandsNextFrame = FALSE
	//The flow hasn't been given any specfic requests. Process the normal quota this frame.
	ELSE
		iStrandsToProcessThisFrame = MAX_STRANDS_TO_PROCESS_PER_FRAME
	ENDIF
	
	//Keep looping through strands until we have processed our quota amount.
	WHILE iStrandsProcessedThisFrame < iStrandsToProcessThisFrame

		//Terminated strands don't count towards quota.
		IF NOT IS_BIT_SET(g_savedGlobals.sFlow.strandSavedVars[m_iStrandIndex].savedBitflags, SAVED_BITS_STRAND_TERMINATED)
			//Unactivate strands don't count towards quota.
			IF IS_BIT_SET(g_savedGlobals.sFlow.strandSavedVars[m_iStrandIndex].savedBitflags, SAVED_BITS_STRAND_ACTIVATED)
				//Update a strand.
				loopAsStrandID = INT_TO_ENUM(STRANDS, m_iStrandIndex)
				UPDATE_STRAND(loopAsStrandID)
				
				//Optimisation: Did the last command request this strand is fast-tracked
				//to the next blocking command this frame?
				IF g_flowUnsaved.bFlowFastTrackCurrentStrand
					CDEBUG1LN(DEBUG_FLOW, "The flow was requested to fast track strand ", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(loopAsStrandID), " this frame.")
					UPDATE_STRAND(loopAsStrandID, TRUE)
					g_flowUnsaved.bFlowFastTrackCurrentStrand = FALSE
				ENDIF
				
				iStrandsProcessedThisFrame++
			ENDIF
		ELSE
			m_iTerminatedStrandCount++
			IF m_iTerminatedStrandCount = ENUM_TO_INT(MAX_STRANDS)
				CPRINTLN(DEBUG_FLOW, "All strands terminated this frame. Flagging the flow as completed.")
				g_savedGlobals.sFlow.flowCompleted = TRUE
				g_flowUnsaved.bFlowControllerBusy = FALSE
				EXIT
			ENDIF
		ENDIF
		
		m_iStrandIndex++
		
		IF m_iStrandIndex = ENUM_TO_INT(MAX_STRANDS)
			IF NOT m_bStrandChangedCommandThisPass
				//No commands have changed for an entire pass of all strands.
				//It is safe to flag the controller as not busy.
				g_flowUnsaved.bFlowControllerBusy = FALSE
			ENDIF
			m_iTerminatedStrandCount++
			m_bStrandChangedCommandThisPass = FALSE
			m_iTerminatedStrandCount = 0
			m_iStrandIndex = 0
		ENDIF
	ENDWHILE
	
	// Debug output
	#IF IS_DEBUG_BUILD
		IF IS_FLOW_CONSOLE_LOG_READY_TO_PRINT()
			CDEBUG1LN(DEBUG_FLOW, "----------------------------------------------------------------------------")
			CDEBUG1LN(DEBUG_FLOW, "")
		ENDIF
	// Is console log output required next frame?
		MAINTAIN_FLOW_CONSOLE_LOG_OUTPUT_REQUIRED_NEXT_FRAME()
	#ENDIF
	
ENDPROC




// ===========================================================================================================

SCRIPT
	// This script needs to cleanup only when the game moves from SP to MP
	IF (HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_SP_TO_MP|FORCE_CLEANUP_FLAG_MAGDEMO))
		
		IF GET_CAUSE_OF_MOST_RECENT_FORCE_CLEANUP() = FORCE_CLEANUP_FLAG_SP_TO_MP
			CDEBUG1LN(DEBUG_FLOW, "...flow_controller.sc has been forced to cleanup (SP to MP)")
			IF g_eRunningMission != SP_MISSION_NONE
				CPRINTLN(DEBUG_FLOW, "<SP-MP-MISSION> Flagging mission ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(g_eRunningMission), " as running as we start switch to MP.")
				g_eMissionRunningOnMPSwitchStart = g_eRunningMission
				g_MissionLeftSPIn = g_eRunningMission
				MISSION_PROCESSING_MISSION_OVER(g_eRunningMission)
			ENDIF
			
			FLOW_RUN_CUSTOM_CLEANUP_ON_MP_TRANSITION()
		ENDIF
		
		SCRIPT_CLEANUP()
	ENDIF

	#IF IS_DEBUG_BUILD
		// Create an instance of the mission flow debug variables struct
		FLOW_DEBUG flowDebugVars
		
		// Create the Mission Flow widgets
		CREATE_MISSION_FLOW_WIDGETS(flowDebugVars)
	#ENDIF

	m_bStrandChangedCommandThisPass = FALSE
	g_flowUnsaved.bFlowControllerBusy = FALSE
	g_flowUnsaved.bIdleUntilActiveStrand = TRUE //Initialise in the idle state.
	
	WHILE (TRUE)
	
		//Is the controller in an idle mode waiting for a strand to be activated?
		IF g_flowUnsaved.bIdleUntilActiveStrand
			INT iStrandIndex = 0
			WHILE g_flowUnsaved.bIdleUntilActiveStrand
			AND iStrandIndex < ENUM_TO_INT(MAX_STRANDS)
				IF IS_BIT_SET(g_savedGlobals.sFlow.strandSavedVars[iStrandIndex].savedBitflags, SAVED_BITS_STRAND_ACTIVATED)
					g_flowUnsaved.bIdleUntilActiveStrand = FALSE
				ELSE
					iStrandIndex++
				ENDIF
				WAIT(0) //Check a strand every 0ms.
			ENDWHILE
			
		//Controller not idling. Process active strands.
		ELSE
			#IF IS_DEBUG_BUILD
				#IF MISSION_FLOW_PROFILING
					// The whole of the mission flow profiling is level 0
					#IF SCRIPT_PROFILER_ACTIVE
					SCRIPT_PROFILER_START_OF_FRAME()
					#ENDIF
					m_profilerDisplayLevel = 0
					
					// Individual totals of major mission flow parts is profile level 1
					m_profilerDisplayLevel++

					// The mission flow widgets should be profile level 2
					m_profilerDisplayLevel++
				#ENDIF
				
				MAINTAIN_MISSION_FLOW_WIDGETS(flowDebugVars)
				
				#IF MISSION_FLOW_PROFILING
					// The total for all mission flow widgets processing should be profile level 1
					m_profilerDisplayLevel--
					#IF SCRIPT_PROFILER_ACTIVE
					ADD_SCRIPT_PROFILE_MARKER("Maintain Widgets", m_profilerDisplayLevel)
					#ENDIF

					// The mission flow update should be profile level 2
					m_profilerDisplayLevel++
				#ENDIF
			#ENDIF
				
			IF NOT g_savedGlobals.sFlow.flowCompleted
				IF g_savedGlobals.sFlow.isGameflowActive						//Only maintain if gameflow is flagged as active.
					IF NOT g_flowUnsaved.bUpdatingGameflow						//Do not maintain if the flow launcher is modifying the game state.
						IF NOT IS_AUTO_SAVE_IN_PROGRESS()						//Do not maintain if an autosave is in progress.
							MAINTAIN_MISSION_FLOW()
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			//#1628975
			IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
				IF g_eRCMRunningOnMPSwitchStart = NO_RC_MISSION
					g_eRC_MissionIDs eRCM
					REPEAT MAX_RC_MISSIONS eRCM
						IF g_RandomChars[eRCM].rcIsRunning
							CPRINTLN(DEBUG_RANDOM_CHAR, "<SP-MP-RCM> Flagging random char ", ENUM_TO_INT(eRCM), " as running as we will be starting switch to MP.")
							
							g_eRCMRunningOnMPSwitchStart = eRCM
						ENDIF
					ENDREPEAT
				ELSE
					IF NOT g_RandomChars[g_eRCMRunningOnMPSwitchStart].rcIsRunning
						CPRINTLN(DEBUG_RANDOM_CHAR, "<SP-MP-RCM> Unflagging random char ", ENUM_TO_INT(g_eRCMRunningOnMPSwitchStart), ".")
						
						g_eRCMRunningOnMPSwitchStart = NO_RC_MISSION
					ENDIF
				ENDIF
			ENDIF
			
			#IF IS_DEBUG_BUILD
				#IF MISSION_FLOW_PROFILING
					// The total for the whole mission flow update processing should be profile level 1
					m_profilerDisplayLevel--
					#IF SCRIPT_PROFILER_ACTIVE
					ADD_SCRIPT_PROFILE_MARKER("Maintain Mission Flow", m_profilerDisplayLevel)
					#ENDIF

					// The total for the whole mission flow processing should be profile level 0
					m_profilerDisplayLevel--
					#IF SCRIPT_PROFILER_ACTIVE
					ADD_SCRIPT_PROFILE_MARKER("Flow_Controller", m_profilerDisplayLevel)
					#ENDIF
					
					#IF SCRIPT_PROFILER_ACTIVE
					SCRIPT_PROFILER_END_OF_FRAME()
					#ENDIF
					
				#ENDIF
			#ENDIF
			WAIT(0)
			
		ENDIF
	ENDWHILE

ENDSCRIPT


