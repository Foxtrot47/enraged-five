USING "commands_entity.sch"
USING "commands_network.sch"
USING "commands_decorator.sch"
USING "MP_globals.sch"

/// PURPOSE:
///    Checks to see if a player int id is a valid value, is not -ve.
/// PARAMS:
///    iPlayer - The player int.
/// RETURNS:
///    True if the player int is not -ve and less than GET_MAX_NUM_OF_NET_PARTICIPANTS
FUNC BOOL IS_NET_PARTICIPANT_INT_ID_VALID(INT iParticipant)
	RETURN (iParticipant >= 0) AND (iParticipant < NETWORK_GET_MAX_NUM_PARTICIPANTS() )
ENDFUNC

/// PURPOSE: Returns an Invalid Player Index (Currently player index is from 0-31, this sets it to -1.  If we were to use NULL it would return 0)
/// RETURNS: An Invalid Player Index
/// NOTES: Used in Multiplayer
FUNC PLAYER_INDEX INVALID_PLAYER_INDEX()
	RETURN INT_TO_NATIVE(PLAYER_INDEX, -1)	
ENDFUNC

/// PURPOSE: Returns an Invalid participant Index (Currently participant index is from 0-18, this sets it to -1.  If we were to use NULL it would return 0)
/// RETURNS: An Invalid participant Index
/// NOTES: Used in Multiplayer
FUNC PARTICIPANT_INDEX INVALID_PARTICIPANT_INDEX()
	RETURN INT_TO_NATIVE(PARTICIPANT_INDEX, -1)	
ENDFUNC

//FUNC BOOL IS_GAMESPY_ONLINE() 
//	IF IS_XBOX360_VERSION ()
//		RETURN(TRUE)
//	ENDIF
//	#IF IS_DEBUG_BUILD
//		IF (g_bGameSpyOnline)
//			RETURN(TRUE)
//		ENDIF
//	#ENDIF	
//	RETURN(NETWORK_IS_GAMESPY_ONLINE())
//	#IF IS_FINAL_BUILD
//		RETURN(NETWORK_IS_GAMESPY_ONLINE())
//	#ENDIF
//ENDFUNC

/// PURPOSE: Check a player is in an OK playing state depending on the passed in variables
FUNC BOOL IS_NET_PLAYER_OK(PLAYER_INDEX playerID, BOOL bCheckPlayerAlive = TRUE, BOOL bCheckPlayerInGameModeMainScriptRunningCase = TRUE)
	INT iPlayer = NATIVE_TO_INT(playerID)
	
	IF iPlayer != -1
		IF NETWORK_IS_PLAYER_ACTIVE(playerID)
		
			//Check the player is alive
			IF (bCheckPlayerAlive)
				IF NOT IS_PLAYER_PLAYING(playerID)
					RETURN FALSE
				ENDIF
			ENDIF
			
			//Check the player is in the main running state
			IF (bCheckPlayerInGameModeMainScriptRunningCase)
				//If it's the local player, the use their cached state, as it can change mid frame
				IF iPlayer = MPGlobals.g_iLocalPlayerID
					RETURN MPGlobals.g_bLocalPlayerActive
				ELIF GlobalplayerBD[iPlayer].iGameState != MAIN_GAME_STATE_RUNNING
					RETURN FALSE
				ENDIF
			ENDIF
			
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//FUNC INT GET_THE_NETWORK_TIMER()
//
//	SCRIPT_ASSERT("GET_THE_NETWORK_TIMER - this should no longer be used. Use new GET_NETWORK_TIME() function instead.")
//
//	INT iTimer
//	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
//		iTimer = GET_GAME_TIMER()
//	ELSE
//		iTimer = NATIVE_TO_INT(GET_NETWORK_TIME())
//	ENDIF
//	RETURN(iTimer)
//ENDFUNC


FUNC BOOL IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(PLAYER_INDEX playerID)
	IF (playerID = INT_TO_NATIVE(PLAYER_INDEX, -1))
		RETURN(FALSE)
	ELSE
		RETURN IS_BIT_SET(MPGlobals.iBS_ActivePlayersInMainScript, NATIVE_TO_INT(playerID))
	ENDIF
	RETURN(TRUE)
ENDFUNC

/// PURPOSE: Returns TRUE if entity is free to edit. 
/// NOTES: It will set the entity as a mission entity (if it is not already) so you will need to mark as no longer needed after using this. 
FUNC BOOL CAN_EDIT_THIS_ENTITY(ENTITY_INDEX EntityID, BOOL &bSetAsMissionEntity)
	IF DOES_ENTITY_EXIST(EntityID)
		IF NOT IS_ENTITY_A_MISSION_ENTITY(EntityID)
			IF NETWORK_GET_ENTITY_IS_LOCAL(EntityID)
				IF NOT IS_THIS_MODEL_A_TRAIN(GET_ENTITY_MODEL(EntityID))
					SET_ENTITY_AS_MISSION_ENTITY(EntityID, FALSE, TRUE )
					PRINTLN("CAN_EDIT_THIS_ENTITY - setting vehicle as mission entity") 
					bSetAsMissionEntity = TRUE
				ELSE
					PRINTLN("CAN_EDIT_THIS_ENTITY - model is a train, cannot set as mission entity")
				ENDIF
			ENDIF
		ENDIF
		IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(EntityID, FALSE)
			IF NETWORK_HAS_CONTROL_OF_ENTITY(EntityID)							
				PRINTLN("CAN_EDIT_THIS_ENTITY - returning TRUE") 
				RETURN(TRUE)
			ELSE
				PRINTLN("CAN_EDIT_THIS_ENTITY - returning FALSE, does not have control of entity") 
			ENDIF
		ELSE
			PRINTLN("CAN_EDIT_THIS_ENTITY - returning FALSE, does not belong to this script") 	
		ENDIF
	ENDIF
	RETURN(FALSE)
ENDFUNC


//EOF







