//╒═════════════════════════════════════════════════════════════════════════════╕
//│				Author:  Ben Rollinson					Date: 27/11/11			│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│						Flow Launcher Processing Header							│
//│																				│
//│		DESCRIPTION: 	Procedures and functions that are used throughout		│
//│						the Flow Launcher system.								│
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛

#IF IS_DEBUG_BUILD

USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_debug.sch"
USING "commands_hud.sch"
USING "debug_channels_structs.sch"

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════╡ Launcher XML Writing Procedures ╞═════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

//PURPOSE: 	Write a string to the game's Flow Diagram XML file.
//
PROC LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE(FLOW_LAUNCHER_VARS &flowLauncher, STRING paramString)
	SAVE_STRING_TO_NAMED_DEBUG_FILE(paramString, flowLauncher.txtLauncherXMLPath, flowLauncher.txtLauncherDiagramXMLFile)
ENDPROC


//PURPOSE: 	Write a string to the game's Playthrough Order XML file.
//
PROC LAUNCHER_WRITE_STRING_TO_PLAYTHROUGH_XML_FILE(FLOW_LAUNCHER_VARS &flowLauncher, STRING paramString)
	SAVE_STRING_TO_NAMED_DEBUG_FILE(paramString, flowLauncher.txtLauncherXMLPath, flowLauncher.txtLauncherPlaythroughXMLFile)
ENDPROC


//PURPOSE: 	Write an int to the game's Flow Diagram XML file.
//
PROC LAUNCHER_WRITE_INT_TO_DIAGRAM_XML_FILE(FLOW_LAUNCHER_VARS &flowLauncher, INT paramInt)
	SAVE_INT_TO_NAMED_DEBUG_FILE(paramInt, flowLauncher.txtLauncherXMLPath, flowLauncher.txtLauncherDiagramXMLFile)
ENDPROC


//PURPOSE: 	Write an int to the game's Playthrough Order XML file.
//
PROC LAUNCHER_WRITE_INT_TO_PLAYTHROUGH_XML_FILE(FLOW_LAUNCHER_VARS &flowLauncher, INT paramInt)
	SAVE_INT_TO_NAMED_DEBUG_FILE(paramInt, flowLauncher.txtLauncherXMLPath, flowLauncher.txtLauncherPlaythroughXMLFile)
ENDPROC


//PURPOSE: 	Write a new line to the game's Flow Diagram XML file.
//
PROC LAUNCHER_WRITE_NEW_LINE_TO_DIAGRAM_XML_FILE(FLOW_LAUNCHER_VARS &flowLauncher)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(flowLauncher.txtLauncherXMLPath, flowLauncher.txtLauncherDiagramXMLFile)
ENDPROC


//PURPOSE: 	Write a new line to the game's Playthrough Order XML file.
//
PROC LAUNCHER_WRITE_NEW_LINE_TO_PLAYTHROUGH_XML_FILE(FLOW_LAUNCHER_VARS &flowLauncher)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(flowLauncher.txtLauncherXMLPath, flowLauncher.txtLauncherPlaythroughXMLFile)
ENDPROC


//PURPOSE: 	An attempt to work out how many tabs are required to align a given string with 
// 			other strings of varying lengths in an XML file. Saves to Flow Diagram XML.
//
PROC LAUNCHER_WRITE_STRING_TABS_TO_DIAGRAM_XML_FILE(FLOW_LAUNCHER_VARS &flowLauncher, INT paramTabCount, STRING paramAttribute)
	IF paramTabCount < 0
		CERRORLN(DEBUG_FLOW_LAUNCHER, "LAUNCHER_WRITE_STRING_TABS_TO_DIAGRAM_XML_FILE: Invalid tab count passed to procedure.")
		EXIT
	ENDIF

	INT iSpacesToCover = (paramTabCount * 4) - 2 - GET_LENGTH_OF_LITERAL_STRING(paramAttribute)
	INT iNoTabs = FLOOR(TO_FLOAT(iSpacesToCover) * 0.25)

	WHILE iNoTabs > 0
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	", flowLauncher.txtLauncherXMLPath, flowLauncher.txtLauncherDiagramXMLFile)
		iNoTabs--
	ENDWHILE
ENDPROC


//PURPOSE: 	An attempt to work out how many tabs are required to align a given string with 
// 			other strings of varying lengths in an XML file. Saves to Playthrough Order XML.
//
PROC LAUNCHER_WRITE_STRING_TABS_TO_PLAYTHROUGH_XML_FILE(FLOW_LAUNCHER_VARS &flowLauncher, INT paramTabCount, STRING paramAttribute)
	IF paramTabCount < 0
		CERRORLN(DEBUG_FLOW_LAUNCHER, "LAUNCHER_WRITE_STRING_TABS_TO_PLAYTHROUGH_XML_FILE: Invalid tab count passed to procedure.")
		EXIT
	ENDIF

	INT iSpacesToCover = (paramTabCount * 4) - 2 - GET_LENGTH_OF_LITERAL_STRING(paramAttribute)
	INT iNoTabs = FLOOR(TO_FLOAT(iSpacesToCover) * 0.25)

	WHILE iNoTabs > 0
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	", flowLauncher.txtLauncherXMLPath, flowLauncher.txtLauncherPlaythroughXMLFile)
		iNoTabs--
	ENDWHILE
ENDPROC



#ENDIF
