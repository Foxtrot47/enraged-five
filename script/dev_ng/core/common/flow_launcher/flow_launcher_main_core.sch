//╒═════════════════════════════════════════════════════════════════════════════╕
//│				Author:  Ben Rollinson					Date: 23/09/10			│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│					  			Flow Launcher									│
//│																				│
//│		DESCRIPTION: 	A system used to jump the game flow to a				│									
//│						specific point midway through the flow. The 			│
//│						launcher will simulate all actions that would			│
//│						have occurred before that point, leaving the game 		│
//│						in a state that accurately represents the game			│
//│						having been played up to that point.					│
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛

#IF IS_DEBUG_BUILD

USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_camera.sch"
USING "commands_interiors.sch"
USING "commands_streaming.sch"

USING "script_player.sch"

USING "flow_launcher_processing.sch"
USING "flow_processing_core.sch"
USING "flow_launcher_processing_core.sch"
USING "flow_launcher_commands_core.sch"



//PURPOSE:		Main flow launcher algorithm. Activate the flow to a specified label using a depth first search. 
//				Step to the next safe label in a strand in each active strand until the target label is reached. 
//				This should always leave the flow in a stable state.
//
//PARAM NOTES:	paramLabel - The flow label that we are looking to activate to.
//				paramDoWarp - Should we warp the player to a location once the activation is complete?
//				paramWarpPos - The warp position to warp the player to after activation. Irrelevant if paramDoWarp is not set.
//				paramSetHour - The hour to set the game clock to. Used when auto triggering missions with time restrictions.
//				paramGeneratePlaythroughOrder - Should we generate a playthrough order xml file while activating the flow?
//				paramGenerateFlowDiagram - Should we generate a visual flow diagram xml file while activating the flow?
//
PROC LAUNCHER_LAUNCH_FLOW_TO_LABEL(FLOW_LAUNCHER_VARS &flowLauncher, JUMP_LABEL_IDS paramLabel, INT paramSetHour = -1, BOOL paramGeneratePlaythroughOrder = FALSE, BOOL paramGenerateFlowDiagram = FALSE, BOOL paramFadeBackIn = TRUE)
	
	//Check we're not trying to generate a playthrough order and a flow diagram at the same time.
	IF paramGeneratePlaythroughOrder AND paramGenerateFlowDiagram
		CERRORLN(DEBUG_FLOW_LAUNCHER, "LAUNCHER_LAUNCH_FLOW_TO_LABEL: Tried to generate a playthrough order at the same time as a flow diagram. Operation not permitted.")
		EXIT
	ENDIF
	
	CDEBUG1LN(DEBUG_FLOW_LAUNCHER, "=================================")
	CDEBUG1LN(DEBUG_FLOW_LAUNCHER, "     FLOW LAUNCHER - START")
	CDEBUG1LN(DEBUG_FLOW_LAUNCHER, "=================================")
	CDEBUG1LN(DEBUG_FLOW_LAUNCHER, "Activating to label [", GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(paramLabel),"].")
	
	IF NOT IS_SCREEN_FADED_OUT()
		DO_SCREEN_FADE_OUT(0)
	ENDIF
	
	//Restart the flow at the first strand.
	LAUNCHER_RESTART_FLOW()
	
	//Stop all flow control scripts from maintaining the flow state.
	g_flowUnsaved.bUpdatingGameflow = TRUE
	
	//If we're about to generate a playthrough order or a flow diagram, prepare the xml file we will be outputting to.
	IF paramGeneratePlaythroughOrder
		// Add the initial xml data to the file
		CLEAR_NAMED_DEBUG_FILE	(flowLauncher.txtLauncherXMLPath, flowLauncher.txtLauncherPlaythroughXMLFile)
		OPEN_NAMED_DEBUG_FILE	(flowLauncher.txtLauncherXMLPath, flowLauncher.txtLauncherPlaythroughXMLFile)
      	LAUNCHER_WRITE_STRING_TO_PLAYTHROUGH_XML_FILE	(flowLauncher, "<?xml version='1.0'?>")
      	LAUNCHER_WRITE_NEW_LINE_TO_PLAYTHROUGH_XML_FILE	(flowLauncher)
      	LAUNCHER_WRITE_STRING_TO_PLAYTHROUGH_XML_FILE	(flowLauncher, "<DebugMenuItems>")
     	LAUNCHER_WRITE_NEW_LINE_TO_PLAYTHROUGH_XML_FILE	(flowLauncher)
	ENDIF
	
	IF paramGenerateFlowDiagram
		// Add the initial xml data to the file
		CLEAR_NAMED_DEBUG_FILE	(flowLauncher.txtLauncherXMLPath, flowLauncher.txtLauncherDiagramXMLFile)
		OPEN_NAMED_DEBUG_FILE	(flowLauncher.txtLauncherXMLPath, flowLauncher.txtLauncherDiagramXMLFile)
      	LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE	(flowLauncher, "<?xml version='1.0'?>")
		LAUNCHER_WRITE_NEW_LINE_TO_DIAGRAM_XML_FILE	(flowLauncher)
		LAUNCHER_WRITE_NEW_LINE_TO_DIAGRAM_XML_FILE	(flowLauncher)
		LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE	(flowLauncher, "<FlowDiagramElements>")
      	LAUNCHER_WRITE_NEW_LINE_TO_DIAGRAM_XML_FILE	(flowLauncher)
		// Reset flow diagram element counter.
		flowLauncher.iLauncherCurrentDiagramElementID = 0
	ENDIF
	
	//Skip through strand commands until we find the target label.
	BOOL bFoundLabel = FALSE
	WHILE NOT bFoundLabel
	
		WAIT(0)
		#if USE_CLF_DLC
			//Depth first search, but always process to the next label or holding command.
			INT iStrandIndex
			REPEAT MAX_STRANDS_CLF iStrandIndex	
				//Check strand is active.
				IF IS_BIT_SET(g_savedGlobalsClifford.sFlow.strandSavedVars[iStrandIndex].savedBitflags,SAVED_BITS_STRAND_ACTIVATED)
					//Check strand hasn't been terminated.
					IF NOT IS_BIT_SET(g_savedGlobalsClifford.sFlow.strandSavedVars[iStrandIndex].savedBitflags,SAVED_BITS_STRAND_TERMINATED)
						
						//Save command position.
						INT iLastCommandPosition = g_savedGlobalsClifford.sFlow.strandSavedVars[iStrandIndex].thisCommandPos
						//Skip next command in strand.
						LAUNCHER_SKIP_NEXT_STRAND_COMMAND(flowLauncher, INT_TO_ENUM(STRANDS,iStrandIndex), paramGeneratePlaythroughOrder, paramGenerateFlowDiagram)
						//Get the new command we've stepped to.
						FLOW_COMMANDS sCurrentStrandCommand = g_flowUnsaved.flowCommands[g_savedGlobalsClifford.sFlow.strandSavedVars[iStrandIndex].thisCommandPos]
						
						WHILE IS_BIT_SET(g_savedGlobalsClifford.sFlow.strandSavedVars[iStrandIndex].savedBitflags,SAVED_BITS_STRAND_ACTIVATED)		// Strand has started.
						AND NOT IS_BIT_SET(g_savedGlobalsClifford.sFlow.strandSavedVars[iStrandIndex].savedBitflags,SAVED_BITS_STRAND_TERMINATED)	// Strand hasn't ended.
						AND NOT (sCurrentStrandCommand.command = FLOW_LABEL)																// We've not hit a 'Label' command.
						AND NOT (sCurrentStrandCommand.command = FLOW_LABEL_LAUNCHER_ONLY)													// We've not hit a 'Label Launcher Only' command.
						AND (iLastCommandPosition <> g_savedGlobalsClifford.sFlow.strandSavedVars[iStrandIndex].thisCommandPos)						// We're not stuck on the same command as last loop.
						
							//Save command position.
							iLastCommandPosition = g_savedGlobalsClifford.sFlow.strandSavedVars[iStrandIndex].thisCommandPos
							//Skip next command in strand.
							LAUNCHER_SKIP_NEXT_STRAND_COMMAND(flowLauncher, INT_TO_ENUM(STRANDS,iStrandIndex), paramGeneratePlaythroughOrder, paramGenerateFlowDiagram)
							//Get the new command we've stepped to.
							sCurrentStrandCommand = g_flowUnsaved.flowCommands[g_savedGlobalsClifford.sFlow.strandSavedVars[iStrandIndex].thisCommandPos]
						
						ENDWHILE
						
						//Line break debug output after a strand has been processed.
						CDEBUG1LN(DEBUG_FLOW_LAUNCHER, "------------------------ <clf> --------------------------------")
						
						BOOL bUnsafeBreak = FALSE
						
						//Have we found a matching 'Label' command?
						IF sCurrentStrandCommand.command = FLOW_LABEL
							// If we have, check if it's the label we're searching for.
							IF INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.coreVars[sCurrentStrandCommand.index].iValue1) = paramLabel
								bFoundLabel = TRUE
							
							// Not a matching label. Is it a safe break point?
							ELIF g_flowUnsaved.coreVars[sCurrentStrandCommand.index].iValue2 != 1
								bUnsafeBreak = TRUE
								CDEBUG1LN(DEBUG_FLOW_LAUNCHER, "Stepping over label that is an unsafe breakpoint. ", GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.coreVars[sCurrentStrandCommand.index].iValue1)), ".")
							ENDIF
						ENDIF
						
						
						//Launch script commands take multiple frames and should be looped over until they are
						//done.
						IF sCurrentStrandCommand.command = FLOW_LAUNCH_SCRIPT_AND_FORGET
							bUnsafeBreak = TRUE
						ENDIF
						
						//Have we found a matching 'Label Launcher Only' command?
						IF sCurrentStrandCommand.command = FLOW_LABEL_LAUNCHER_ONLY
							IF INT_TO_ENUM(JUMP_LABEL_IDS, sCurrentStrandCommand.index) = paramLabel
								bFoundLabel = TRUE
								bUnsafeBreak = TRUE
								CDEBUG1LN(DEBUG_FLOW_LAUNCHER, "Stepping over label that is an unsafe breakpoint. ", GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(INT_TO_ENUM(JUMP_LABEL_IDS, sCurrentStrandCommand.index)), ".")
							ENDIF
						ENDIF

						// We've launched to the required label. Do any required post-launch processing.
						IF bFoundLabel
							CDEBUG1LN(DEBUG_FLOW_LAUNCHER, "Found label [", GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(paramLabel), "]. Flow sucessfully updated.")
							
							// Update the game time if requested.
							IF paramSetHour <> -1
								IF paramSetHour < 0 AND paramSetHour > 11
									CERRORLN(DEBUG_FLOW_LAUNCHER, "LAUNCHER_LAUNCH_FLOW_TO_LABEL: Tried to set the game clock to an invalid hour.")
								ELSE
									CDEBUG1LN(DEBUG_FLOW_LAUNCHER, " Setting the game clock hour to ", paramSetHour, ".")
									SET_CLOCK_TIME(paramSetHour, 0, 0)
								ENDIF
							ENDIF
							
							//If we've just generated a playthrough order, close out the xml file.
							IF paramGeneratePlaythroughOrder
								LAUNCHER_WRITE_STRING_TO_PLAYTHROUGH_XML_FILE	(flowLauncher, "</DebugMenuItems>")
	 							LAUNCHER_WRITE_NEW_LINE_TO_PLAYTHROUGH_XML_FILE	(flowLauncher)
	  							CLOSE_DEBUG_FILE()
							ENDIF
							
							IF paramGenerateFlowDiagram
								LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE	(flowLauncher, "</FlowDiagramElements>")
								LAUNCHER_WRITE_NEW_LINE_TO_DIAGRAM_XML_FILE	(flowLauncher)
	  							CLOSE_DEBUG_FILE()
							ENDIF
							
							CDEBUG1LN(DEBUG_FLOW_LAUNCHER, "=================================")
							CDEBUG1LN(DEBUG_FLOW_LAUNCHER, "      FLOW LAUNCHER - END")
							CDEBUG1LN(DEBUG_FLOW_LAUNCHER, "=================================")
							
							IF paramFadeBackIn
								IF IS_SCREEN_FADED_OUT()
									DO_SCREEN_FADE_IN(500)
								ENDIF
							ENDIF
							
							//We need to wait 1 frame to let systems process the modified flow state
							//before resuming normal flow operation.
							WAIT(0)
							g_flowUnsaved.bUpdatingGameflow = FALSE
		
							EXIT
						ENDIF
						
						IF bUnsafeBreak
							// Stopped at a label that was not a safe breakpoint.
							// Step back strand index so we do another pass on the current strand.
							iStrandIndex--
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
		#endif
		#if USE_NRM_DLC
			//Depth first search, but always process to the next label or holding command.
			INT iStrandIndex
			REPEAT MAX_STRANDS_NRM iStrandIndex	
				//Check strand is active.
				IF IS_BIT_SET(g_savedGlobalsnorman.sFlow.strandSavedVars[iStrandIndex].savedBitflags,SAVED_BITS_STRAND_ACTIVATED)
					//Check strand hasn't been terminated.
					IF NOT IS_BIT_SET(g_savedGlobalsnorman.sFlow.strandSavedVars[iStrandIndex].savedBitflags,SAVED_BITS_STRAND_TERMINATED)
						
						//Save command position.
						INT iLastCommandPosition = g_savedGlobalsnorman.sFlow.strandSavedVars[iStrandIndex].thisCommandPos
						//Skip next command in strand.
						LAUNCHER_SKIP_NEXT_STRAND_COMMAND(flowLauncher, INT_TO_ENUM(STRANDS,iStrandIndex), paramGeneratePlaythroughOrder, paramGenerateFlowDiagram)
						//Get the new command we've stepped to.
						FLOW_COMMANDS sCurrentStrandCommand = g_flowUnsaved.flowCommands[g_savedGlobalsnorman.sFlow.strandSavedVars[iStrandIndex].thisCommandPos]
						
						WHILE IS_BIT_SET(g_savedGlobalsnorman.sFlow.strandSavedVars[iStrandIndex].savedBitflags,SAVED_BITS_STRAND_ACTIVATED)		// Strand has started.
						AND NOT IS_BIT_SET(g_savedGlobalsnorman.sFlow.strandSavedVars[iStrandIndex].savedBitflags,SAVED_BITS_STRAND_TERMINATED)	// Strand hasn't ended.
						AND NOT (sCurrentStrandCommand.command = FLOW_LABEL)																// We've not hit a 'Label' command.
						AND NOT (sCurrentStrandCommand.command = FLOW_LABEL_LAUNCHER_ONLY)													// We've not hit a 'Label Launcher Only' command.
						AND (iLastCommandPosition <> g_savedGlobalsnorman.sFlow.strandSavedVars[iStrandIndex].thisCommandPos)						// We're not stuck on the same command as last loop.
						
							//Save command position.
							iLastCommandPosition = g_savedGlobalsnorman.sFlow.strandSavedVars[iStrandIndex].thisCommandPos
							//Skip next command in strand.
							LAUNCHER_SKIP_NEXT_STRAND_COMMAND(flowLauncher, INT_TO_ENUM(STRANDS,iStrandIndex), paramGeneratePlaythroughOrder, paramGenerateFlowDiagram)
							//Get the new command we've stepped to.
							sCurrentStrandCommand = g_flowUnsaved.flowCommands[g_savedGlobalsnorman.sFlow.strandSavedVars[iStrandIndex].thisCommandPos]
						
						ENDWHILE
						
						//Line break debug output after a strand has been processed.
						CDEBUG1LN(DEBUG_FLOW_LAUNCHER, "------------------------ <NRM> --------------------------------")
						
						BOOL bUnsafeBreak = FALSE
						
						//Have we found a matching 'Label' command?
						IF sCurrentStrandCommand.command = FLOW_LABEL
							// If we have, check if it's the label we're searching for.
							IF INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.coreVars[sCurrentStrandCommand.index].iValue1) = paramLabel
								bFoundLabel = TRUE
							
							// Not a matching label. Is it a safe break point?
							ELIF g_flowUnsaved.coreVars[sCurrentStrandCommand.index].iValue2 != 1
								bUnsafeBreak = TRUE
								CDEBUG1LN(DEBUG_FLOW_LAUNCHER, "Stepping over label that is an unsafe breakpoint. ", GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.coreVars[sCurrentStrandCommand.index].iValue1)), ".")
							ENDIF
						ENDIF
						
						
						//Launch script commands take multiple frames and should be looped over until they are
						//done.
						IF sCurrentStrandCommand.command = FLOW_LAUNCH_SCRIPT_AND_FORGET
							bUnsafeBreak = TRUE
						ENDIF
						
						//Have we found a matching 'Label Launcher Only' command?
						IF sCurrentStrandCommand.command = FLOW_LABEL_LAUNCHER_ONLY
							IF INT_TO_ENUM(JUMP_LABEL_IDS, sCurrentStrandCommand.index) = paramLabel
								bFoundLabel = TRUE
								bUnsafeBreak = TRUE
								CDEBUG1LN(DEBUG_FLOW_LAUNCHER, "Stepping over label that is an unsafe breakpoint. ", GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(INT_TO_ENUM(JUMP_LABEL_IDS, sCurrentStrandCommand.index)), ".")
							ENDIF
						ENDIF

						// We've launched to the required label. Do any required post-launch processing.
						IF bFoundLabel
							CDEBUG1LN(DEBUG_FLOW_LAUNCHER, "Found label [", GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(paramLabel), "]. Flow sucessfully updated.")
							
							// Update the game time if requested.
							IF paramSetHour <> -1
								IF paramSetHour < 0 AND paramSetHour > 11
									CERRORLN(DEBUG_FLOW_LAUNCHER, "LAUNCHER_LAUNCH_FLOW_TO_LABEL: Tried to set the game clock to an invalid hour.")
								ELSE
									CDEBUG1LN(DEBUG_FLOW_LAUNCHER, " Setting the game clock hour to ", paramSetHour, ".")
									SET_CLOCK_TIME(paramSetHour, 0, 0)
								ENDIF
							ENDIF
							
							//If we've just generated a playthrough order, close out the xml file.
							IF paramGeneratePlaythroughOrder
								LAUNCHER_WRITE_STRING_TO_PLAYTHROUGH_XML_FILE	(flowLauncher, "</DebugMenuItems>")
	 							LAUNCHER_WRITE_NEW_LINE_TO_PLAYTHROUGH_XML_FILE	(flowLauncher)
	  							CLOSE_DEBUG_FILE()
							ENDIF
							
							IF paramGenerateFlowDiagram
								LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE	(flowLauncher, "</FlowDiagramElements>")
								LAUNCHER_WRITE_NEW_LINE_TO_DIAGRAM_XML_FILE	(flowLauncher)
	  							CLOSE_DEBUG_FILE()
							ENDIF
							
							CDEBUG1LN(DEBUG_FLOW_LAUNCHER, "=================================")
							CDEBUG1LN(DEBUG_FLOW_LAUNCHER, "      FLOW LAUNCHER - END")
							CDEBUG1LN(DEBUG_FLOW_LAUNCHER, "=================================")
							
							IF paramFadeBackIn
								IF IS_SCREEN_FADED_OUT()
									DO_SCREEN_FADE_IN(500)
								ENDIF
							ENDIF
							
							//We need to wait 1 frame to let systems process the modified flow state
							//before resuming normal flow operation.
							WAIT(0)
							g_flowUnsaved.bUpdatingGameflow = FALSE
		
							EXIT
						ENDIF
						
						IF bUnsafeBreak
							// Stopped at a label that was not a safe breakpoint.
							// Step back strand index so we do another pass on the current strand.
							iStrandIndex--
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
		#endif
		
		#if not USE_CLF_DLC
		#if not USE_NRM_DLC
			//Depth first search, but always process to the next label or holding command.
			INT iStrandIndex
			REPEAT MAX_STRANDS iStrandIndex	
				//Check strand is active.
				IF IS_BIT_SET(g_savedGlobals.sFlow.strandSavedVars[iStrandIndex].savedBitflags,SAVED_BITS_STRAND_ACTIVATED)
					//Check strand hasn't been terminated.
					IF NOT IS_BIT_SET(g_savedGlobals.sFlow.strandSavedVars[iStrandIndex].savedBitflags,SAVED_BITS_STRAND_TERMINATED)
						
						//Save command position.
						INT iLastCommandPosition = g_savedGlobals.sFlow.strandSavedVars[iStrandIndex].thisCommandPos
						//Skip next command in strand.
						LAUNCHER_SKIP_NEXT_STRAND_COMMAND(flowLauncher, INT_TO_ENUM(STRANDS,iStrandIndex), paramGeneratePlaythroughOrder, paramGenerateFlowDiagram)
						//Get the new command we've stepped to.
						FLOW_COMMANDS sCurrentStrandCommand = g_flowUnsaved.flowCommands[g_savedGlobals.sFlow.strandSavedVars[iStrandIndex].thisCommandPos]
						
						WHILE IS_BIT_SET(g_savedGlobals.sFlow.strandSavedVars[iStrandIndex].savedBitflags,SAVED_BITS_STRAND_ACTIVATED)		// Strand has started.
						AND NOT IS_BIT_SET(g_savedGlobals.sFlow.strandSavedVars[iStrandIndex].savedBitflags,SAVED_BITS_STRAND_TERMINATED)	// Strand hasn't ended.
						AND NOT (sCurrentStrandCommand.command = FLOW_LABEL)																// We've not hit a 'Label' command.
						AND NOT (sCurrentStrandCommand.command = FLOW_LABEL_LAUNCHER_ONLY)													// We've not hit a 'Label Launcher Only' command.
						AND (iLastCommandPosition <> g_savedGlobals.sFlow.strandSavedVars[iStrandIndex].thisCommandPos)						// We're not stuck on the same command as last loop.
						
							//Save command position.
							iLastCommandPosition = g_savedGlobals.sFlow.strandSavedVars[iStrandIndex].thisCommandPos
							//Skip next command in strand.
							LAUNCHER_SKIP_NEXT_STRAND_COMMAND(flowLauncher, INT_TO_ENUM(STRANDS,iStrandIndex), paramGeneratePlaythroughOrder, paramGenerateFlowDiagram)
							//Get the new command we've stepped to.
							sCurrentStrandCommand = g_flowUnsaved.flowCommands[g_savedGlobals.sFlow.strandSavedVars[iStrandIndex].thisCommandPos]
						
						ENDWHILE
						
						//Line break debug output after a strand has been processed.
						CDEBUG1LN(DEBUG_FLOW_LAUNCHER, "------------------------------------------------------------")
						
						BOOL bUnsafeBreak = FALSE
						
						//Have we found a matching 'Label' command?
						IF sCurrentStrandCommand.command = FLOW_LABEL
							// If we have, check if it's the label we're searching for.
							IF INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.coreVars[sCurrentStrandCommand.index].iValue1) = paramLabel
								bFoundLabel = TRUE
							
							// Not a matching label. Is it a safe break point?
							ELIF g_flowUnsaved.coreVars[sCurrentStrandCommand.index].iValue2 != 1
								bUnsafeBreak = TRUE
								CDEBUG1LN(DEBUG_FLOW_LAUNCHER, "Stepping over label that is an unsafe breakpoint. ", GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.coreVars[sCurrentStrandCommand.index].iValue1)), ".")
							ENDIF
						ENDIF
						
						
						//Launch script commands take multiple frames and should be looped over until they are
						//done.
						IF sCurrentStrandCommand.command = FLOW_LAUNCH_SCRIPT_AND_FORGET
							bUnsafeBreak = TRUE
						ENDIF
						
						//Have we found a matching 'Label Launcher Only' command?
						IF sCurrentStrandCommand.command = FLOW_LABEL_LAUNCHER_ONLY
							IF INT_TO_ENUM(JUMP_LABEL_IDS, sCurrentStrandCommand.index) = paramLabel
								bFoundLabel = TRUE
								bUnsafeBreak = TRUE
								CDEBUG1LN(DEBUG_FLOW_LAUNCHER, "Stepping over label that is an unsafe breakpoint. ", GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(INT_TO_ENUM(JUMP_LABEL_IDS, sCurrentStrandCommand.index)), ".")
							ENDIF
						ENDIF

						// We've launched to the required label. Do any required post-launch processing.
						IF bFoundLabel
							CDEBUG1LN(DEBUG_FLOW_LAUNCHER, "Found label [", GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(paramLabel), "]. Flow sucessfully updated.")
							
							// Update the game time if requested.
							IF paramSetHour <> -1
								IF paramSetHour < 0 AND paramSetHour > 11
									CERRORLN(DEBUG_FLOW_LAUNCHER, "LAUNCHER_LAUNCH_FLOW_TO_LABEL: Tried to set the game clock to an invalid hour.")
								ELSE
									CDEBUG1LN(DEBUG_FLOW_LAUNCHER, " Setting the game clock hour to ", paramSetHour, ".")
									SET_CLOCK_TIME(paramSetHour, 0, 0)
								ENDIF
							ENDIF
							
							//If we've just generated a playthrough order, close out the xml file.
							IF paramGeneratePlaythroughOrder
								LAUNCHER_WRITE_STRING_TO_PLAYTHROUGH_XML_FILE	(flowLauncher, "</DebugMenuItems>")
	 							LAUNCHER_WRITE_NEW_LINE_TO_PLAYTHROUGH_XML_FILE	(flowLauncher)
	  							CLOSE_DEBUG_FILE()
							ENDIF
							
							IF paramGenerateFlowDiagram
								LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE	(flowLauncher, "</FlowDiagramElements>")
								LAUNCHER_WRITE_NEW_LINE_TO_DIAGRAM_XML_FILE	(flowLauncher)
	  							CLOSE_DEBUG_FILE()
							ENDIF
							
							CDEBUG1LN(DEBUG_FLOW_LAUNCHER, "=================================")
							CDEBUG1LN(DEBUG_FLOW_LAUNCHER, "      FLOW LAUNCHER - END")
							CDEBUG1LN(DEBUG_FLOW_LAUNCHER, "=================================")
							
							IF paramFadeBackIn
								IF IS_SCREEN_FADED_OUT()
									DO_SCREEN_FADE_IN(500)
								ENDIF
							ENDIF
							
							//We need to wait 1 frame to let systems process the modified flow state
							//before resuming normal flow operation.
							WAIT(0)
							g_flowUnsaved.bUpdatingGameflow = FALSE
		
							EXIT
						ENDIF
						
						IF bUnsafeBreak
							// Stopped at a label that was not a safe breakpoint.
							// Step back strand index so we do another pass on the current strand.
							iStrandIndex--
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
		#endif
		#endif
	ENDWHILE
	
ENDPROC


//PURPOSE:		Main flow launcher algorithm. Attempt to activate the flow to a number of labels (max. one per strand)
//				at the same time. To be used with the 'Flow Launcher' widget.
//
//PARAM NOTES:	paramStrandLabels[] - 	An array of labels to be searched for. Labels set to STAY_ON_THIS_COMMAND or NEXT_SEQUENTIAL_COMMAND  
//										will be ignroed by the algorithm.
//
//RETURN:		Returns whether or not the activation was successful or not. A false result means that it was impossible to reach all the
//				chosen labels at the same time.
//
FUNC BOOL LAUNCHER_LAUNCH_FLOW_TO_MULTIPLE_LABELS(FLOW_LAUNCHER_VARS &flowLauncher, JUMP_LABEL_IDS &paramStrandLabels[MAX_STRANDS])
	INT index
	INT index2
	BOOL bFoundAllLabels = FALSE
	INT iSearchHaltCount //Counts up consecutive iterations of the algorithm in which no progress was made.
	BOOL bStrandNoLabelRequirement[MAX_STRANDS] //Flags wether or not we're searching for a label in this strand.
	BOOL bStrandLabelFound[MAX_STRANDS] //Flags wether or not we've met the label requirement for this strand.
	INT iStrandLastCommandPos[MAX_STRANDS]
	
	CDEBUG1LN(DEBUG_FLOW_LAUNCHER, "=================================")
	CDEBUG1LN(DEBUG_FLOW_LAUNCHER, "     FLOW LAUNCHER - START")
	CDEBUG1LN(DEBUG_FLOW_LAUNCHER, "=================================")
	CDEBUG1LN(DEBUG_FLOW_LAUNCHER, "Attempting to activate to multiple labels:")
	
	#if USE_CLF_DLC	
		REPEAT MAX_STRANDS_CLF index
			IF paramStrandLabels[index] <> STAY_ON_THIS_COMMAND
			AND paramStrandLabels[index] <> NEXT_SEQUENTIAL_COMMAND
				CDEBUG1LN(DEBUG_FLOW_LAUNCHER, GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(INT_TO_ENUM(STRANDS, index)), " - [", GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(paramStrandLabels[index]), "]")
			ENDIF
		ENDREPEAT
	#endif
	#if USE_NRM_DLC
		REPEAT MAX_STRANDS_NRM index
			IF paramStrandLabels[index] <> STAY_ON_THIS_COMMAND
			AND paramStrandLabels[index] <> NEXT_SEQUENTIAL_COMMAND
				CDEBUG1LN(DEBUG_FLOW_LAUNCHER, GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(INT_TO_ENUM(STRANDS, index)), " - [", GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(paramStrandLabels[index]), "]")
			ENDIF
		ENDREPEAT
	#endif
	
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
		REPEAT MAX_STRANDS index
			IF paramStrandLabels[index] <> STAY_ON_THIS_COMMAND
			AND paramStrandLabels[index] <> NEXT_SEQUENTIAL_COMMAND
				CDEBUG1LN(DEBUG_FLOW_LAUNCHER, GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(INT_TO_ENUM(STRANDS, index)), " - [", GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(paramStrandLabels[index]), "]")
			ENDIF
		ENDREPEAT		
	#endif
	#endif
	
	CDEBUG1LN(DEBUG_FLOW_LAUNCHER, "")	
	IF NOT IS_SCREEN_FADED_OUT()
		DO_SCREEN_FADE_OUT(0)
	ENDIF	
	//First check that all labels we are searching for are located in the correct strands.
	//Also check that at least one label has been chosen.
	
	#if USE_CLF_DLC	
		REPEAT MAX_STRANDS_CLF index
			BOOL bLabelInStrandRange = FALSE
			
			//Check if the label is set to something specific or not.
			IF paramStrandLabels[index] = STAY_ON_THIS_COMMAND
			OR paramStrandLabels[index] = NEXT_SEQUENTIAL_COMMAND
				bLabelInStrandRange = TRUE //Not set to anything specific, don't need to check if it's in range.
			ELSE
				//Iterate through all commands in this strand's range.
				FOR index2 = g_flowUnsaved.flowCommandsIndex[index].lower TO g_flowUnsaved.flowCommandsIndex[index].upper
					
					//Check if the command at this index is a 'Label' command.
					IF g_flowUnsaved.flowCommands[index2].command = FLOW_LABEL
						//Check if the label is the one we want to search for.
						IF INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.coreVars[g_flowUnsaved.flowCommands[index2].index].iValue1) = paramStrandLabels[index]
							bLabelInStrandRange = TRUE //Found the label. All is well.
						ENDIF
					ENDIF
					
					//Check if the command at this index is a 'Label Launcher Only' command.
					IF g_flowUnsaved.flowCommands[index2].command = FLOW_LABEL_LAUNCHER_ONLY
						//Check if the label is the one we want to search for.
						IF INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.flowCommands[index2].index) = paramStrandLabels[index]
							bLabelInStrandRange = TRUE //Found the label. All is well.
						ENDIF
					ENDIF
				ENDFOR
			ENDIF
			
			//Did we fail to find the label in the strand?
			IF NOT bLabelInStrandRange
				CERRORLN(DEBUG_FLOW_LAUNCHER, "Label [", GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(paramStrandLabels[index]), "] is not in strand ", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(INT_TO_ENUM(STRANDS, index)))
				RETURN FALSE
			ENDIF
		ENDREPEAT	
		//Restart the flow at the first strand.
		LAUNCHER_RESTART_FLOW()	
		//Stop all flow control scripts from maintaining the flow state.
		g_flowUnsaved.bUpdatingGameflow = TRUE	
		//Work out which strands have had labels set and which strands we don't care about.
		REPEAT MAX_STRANDS_CLF index
			IF paramStrandLabels[index] = STAY_ON_THIS_COMMAND
			OR paramStrandLabels[index] = NEXT_SEQUENTIAL_COMMAND
				bStrandNoLabelRequirement[index] = TRUE
				bStrandLabelFound[index] = TRUE
			ENDIF
		ENDREPEAT
	#endif
	#if USE_NRM_DLC
		REPEAT MAX_STRANDS_NRM index
			BOOL bLabelInStrandRange = FALSE
			
			//Check if the label is set to something specific or not.
			IF paramStrandLabels[index] = STAY_ON_THIS_COMMAND
			OR paramStrandLabels[index] = NEXT_SEQUENTIAL_COMMAND
				bLabelInStrandRange = TRUE //Not set to anything specific, don't need to check if it's in range.
			ELSE
				//Iterate through all commands in this strand's range.
				FOR index2 = g_flowUnsaved.flowCommandsIndex[index].lower TO g_flowUnsaved.flowCommandsIndex[index].upper
					
					//Check if the command at this index is a 'Label' command.
					IF g_flowUnsaved.flowCommands[index2].command = FLOW_LABEL
						//Check if the label is the one we want to search for.
						IF INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.coreVars[g_flowUnsaved.flowCommands[index2].index].iValue1) = paramStrandLabels[index]
							bLabelInStrandRange = TRUE //Found the label. All is well.
						ENDIF
					ENDIF
					
					//Check if the command at this index is a 'Label Launcher Only' command.
					IF g_flowUnsaved.flowCommands[index2].command = FLOW_LABEL_LAUNCHER_ONLY
						//Check if the label is the one we want to search for.
						IF INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.flowCommands[index2].index) = paramStrandLabels[index]
							bLabelInStrandRange = TRUE //Found the label. All is well.
						ENDIF
					ENDIF
				ENDFOR
			ENDIF
			
			//Did we fail to find the label in the strand?
			IF NOT bLabelInStrandRange
				CERRORLN(DEBUG_FLOW_LAUNCHER, "Label [", GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(paramStrandLabels[index]), "] is not in strand ", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(INT_TO_ENUM(STRANDS, index)))
				RETURN FALSE
			ENDIF
		ENDREPEAT	
		//Restart the flow at the first strand.
		LAUNCHER_RESTART_FLOW()	
		//Stop all flow control scripts from maintaining the flow state.
		g_flowUnsaved.bUpdatingGameflow = TRUE	
		//Work out which strands have had labels set and which strands we don't care about.
		REPEAT MAX_STRANDS_NRM index
			IF paramStrandLabels[index] = STAY_ON_THIS_COMMAND
			OR paramStrandLabels[index] = NEXT_SEQUENTIAL_COMMAND
				bStrandNoLabelRequirement[index] = TRUE
				bStrandLabelFound[index] = TRUE
			ENDIF
		ENDREPEAT
	#endif
	
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
		REPEAT MAX_STRANDS index
			BOOL bLabelInStrandRange = FALSE
			
			//Check if the label is set to something specific or not.
			IF paramStrandLabels[index] = STAY_ON_THIS_COMMAND
			OR paramStrandLabels[index] = NEXT_SEQUENTIAL_COMMAND
				bLabelInStrandRange = TRUE //Not set to anything specific, don't need to check if it's in range.
			ELSE
				//Iterate through all commands in this strand's range.
				FOR index2 = g_flowUnsaved.flowCommandsIndex[index].lower TO g_flowUnsaved.flowCommandsIndex[index].upper
					
					//Check if the command at this index is a 'Label' command.
					IF g_flowUnsaved.flowCommands[index2].command = FLOW_LABEL
						//Check if the label is the one we want to search for.
						IF INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.coreVars[g_flowUnsaved.flowCommands[index2].index].iValue1) = paramStrandLabels[index]
							bLabelInStrandRange = TRUE //Found the label. All is well.
						ENDIF
					ENDIF
					
					//Check if the command at this index is a 'Label Launcher Only' command.
					IF g_flowUnsaved.flowCommands[index2].command = FLOW_LABEL_LAUNCHER_ONLY
						//Check if the label is the one we want to search for.
						IF INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.flowCommands[index2].index) = paramStrandLabels[index]
							bLabelInStrandRange = TRUE //Found the label. All is well.
						ENDIF
					ENDIF
				ENDFOR
			ENDIF
			
			//Did we fail to find the label in the strand?
			IF NOT bLabelInStrandRange
				CERRORLN(DEBUG_FLOW_LAUNCHER, "Label [", GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(paramStrandLabels[index]), "] is not in strand ", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(INT_TO_ENUM(STRANDS, index)))
				RETURN FALSE
			ENDIF
		ENDREPEAT	
		//Restart the flow at the first strand.
		LAUNCHER_RESTART_FLOW()	
		//Stop all flow control scripts from maintaining the flow state.
		g_flowUnsaved.bUpdatingGameflow = TRUE	
		//Work out which strands have had labels set and which strands we don't care about.
		REPEAT MAX_STRANDS index
			IF paramStrandLabels[index] = STAY_ON_THIS_COMMAND
			OR paramStrandLabels[index] = NEXT_SEQUENTIAL_COMMAND
				bStrandNoLabelRequirement[index] = TRUE
				bStrandLabelFound[index] = TRUE
			ENDIF
		ENDREPEAT		
	#endif
	#endif
	
	
	//Keep stepping through strands until we have hit every label we're looking for.
	WHILE NOT bFoundAllLabels
	
		WAIT(0)
	
		iSearchHaltCount++
		#if USE_CLF_DLC
			//Depth first search, but always process to the next label or holding command.
			REPEAT MAX_STRANDS_CLF index		
				//Check strand is active.
				IF IS_BIT_SET(g_savedGlobalsClifford.sFlow.strandSavedVars[index].savedBitflags,SAVED_BITS_STRAND_ACTIVATED)
					//Check strand hasn't been terminated.
					IF NOT IS_BIT_SET(g_savedGlobalsClifford.sFlow.strandSavedVars[index].savedBitflags,SAVED_BITS_STRAND_TERMINATED)
						//Skip next command in strand.
						IF NOT bStrandLabelFound[index]
						OR bStrandNoLabelRequirement[index]
						
							//Save command position.
							iStrandLastCommandPos[index] = g_savedGlobalsClifford.sFlow.strandSavedVars[index].thisCommandPos
							//Skip next command in strand.
							LAUNCHER_SKIP_NEXT_STRAND_COMMAND(flowLauncher, INT_TO_ENUM(STRANDS,index))
							//Get the new command we've stepped to.
							FLOW_COMMANDS sCurrentStrandCommand = g_flowUnsaved.flowCommands[g_savedGlobalsClifford.sFlow.strandSavedVars[index].thisCommandPos]
							
							WHILE IS_BIT_SET(g_savedGlobalsClifford.sFlow.strandSavedVars[index].savedBitflags,SAVED_BITS_STRAND_ACTIVATED)
							AND NOT IS_BIT_SET(g_savedGlobalsClifford.sFlow.strandSavedVars[index].savedBitflags,SAVED_BITS_STRAND_TERMINATED)
							AND NOT (sCurrentStrandCommand.command = FLOW_LABEL)
							AND NOT (sCurrentStrandCommand.command = FLOW_LABEL_LAUNCHER_ONLY)
							AND (iStrandLastCommandPos[index] <> g_savedGlobalsClifford.sFlow.strandSavedVars[index].thisCommandPos)
							
								//Save command position.
								iStrandLastCommandPos[index] = g_savedGlobalsClifford.sFlow.strandSavedVars[index].thisCommandPos
								//Skip next command in strand.
								LAUNCHER_SKIP_NEXT_STRAND_COMMAND(flowLauncher, INT_TO_ENUM(STRANDS,index))
								//Get the new command we've stepped to.
								sCurrentStrandCommand = g_flowUnsaved.flowCommands[g_savedGlobalsClifford.sFlow.strandSavedVars[index].thisCommandPos]
							
							ENDWHILE
							
							//Line break debug output after a strand has been processed.
							CDEBUG1LN(DEBUG_FLOW_LAUNCHER, "------------------------------------------------------------")
										
							//Check if we've stepped to a new command or we're waiting for a script to load.
							IF g_savedGlobalsClifford.sFlow.strandSavedVars[index].thisCommandPos <> iStrandLastCommandPos[index]
							OR sCurrentStrandCommand.command = FLOW_LAUNCH_SCRIPT_AND_FORGET
								iSearchHaltCount = 0 //As long as at least one strand has stepped to a new command we're still progressing.
							ENDIF
							
							//We are looking for a label in this strand. Have we found it?
							IF NOT bStrandNoLabelRequirement[index]
							
								IF sCurrentStrandCommand.command = FLOW_LABEL
									IF INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.coreVars[sCurrentStrandCommand.index].iValue1) = paramStrandLabels[index]
										//We've found the label for this strand.
										CDEBUG1LN(DEBUG_FLOW_LAUNCHER, "Reached label [", GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(paramStrandLabels[index]), "] for strand ", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(INT_TO_ENUM(STRANDS, index)), ".")
										bStrandLabelFound[index] = TRUE
									ENDIF
								ENDIF
								
								IF sCurrentStrandCommand.command = FLOW_LABEL_LAUNCHER_ONLY
									IF INT_TO_ENUM(JUMP_LABEL_IDS, sCurrentStrandCommand.index) = paramStrandLabels[index]
										//We've found the label for this strand.
										CDEBUG1LN(DEBUG_FLOW_LAUNCHER, "Reached launcher only label [", GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(paramStrandLabels[index]), "] for strand ", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(INT_TO_ENUM(STRANDS, index)), ".")
										bStrandLabelFound[index] = TRUE
									ENDIF
								ENDIF
								
								//Check if we've found all labels.
								index2 = 0
								bFoundAllLabels = TRUE
								WHILE (index2 < ENUM_TO_INT(MAX_STRANDS_CLF)) AND bFoundAllLabels
									IF NOT bStrandLabelFound[index2]
										bFoundAllLabels = FALSE
									ENDIF
									index2++
								ENDWHILE
							
							//We're not looking for a label in this strand. Are we sitting on a safe breakpoint?
							ELSE
								IF sCurrentStrandCommand.command = FLOW_LABEL
									IF g_flowUnsaved.coreVars[sCurrentStrandCommand.index].iValue2 != 1
										CDEBUG1LN(DEBUG_FLOW_LAUNCHER, "Stepping over label that is an unsafe breakpoint. LABEL:", GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.coreVars[sCurrentStrandCommand.index].iValue1)), ".")
										// Stopped at a label that was not a safe breakpoint.
										// Step back strand index so we do another pass on the current strand.
										index--
									ENDIF
								ELIF sCurrentStrandCommand.command = FLOW_LABEL_LAUNCHER_ONLY
									CDEBUG1LN(DEBUG_FLOW_LAUNCHER, "Stepping over label that is an unsafe breakpoint. LABEL:", GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(INT_TO_ENUM(JUMP_LABEL_IDS, sCurrentStrandCommand.index)), ".")
									// Stopped at a label that was not a safe breakpoint.
									// Step back strand index so we do another pass on the current strand.
									index--
								ENDIF
							ENDIF
						
						ENDIF
					ENDIF
				ENDIF			
			ENDREPEAT
		#endif
		#if USE_NRM_DLC
			//Depth first search, but always process to the next label or holding command.
			REPEAT MAX_STRANDS_NRM index		
				//Check strand is active.
				IF IS_BIT_SET(g_savedGlobalsnorman.sFlow.strandSavedVars[index].savedBitflags,SAVED_BITS_STRAND_ACTIVATED)
					//Check strand hasn't been terminated.
					IF NOT IS_BIT_SET(g_savedGlobalsnorman.sFlow.strandSavedVars[index].savedBitflags,SAVED_BITS_STRAND_TERMINATED)
						//Skip next command in strand.
						IF NOT bStrandLabelFound[index]
						OR bStrandNoLabelRequirement[index]
						
							//Save command position.
							iStrandLastCommandPos[index] = g_savedGlobalsnorman.sFlow.strandSavedVars[index].thisCommandPos
							//Skip next command in strand.
							LAUNCHER_SKIP_NEXT_STRAND_COMMAND(flowLauncher, INT_TO_ENUM(STRANDS,index))
							//Get the new command we've stepped to.
							FLOW_COMMANDS sCurrentStrandCommand = g_flowUnsaved.flowCommands[g_savedGlobalsnorman.sFlow.strandSavedVars[index].thisCommandPos]
							
							WHILE IS_BIT_SET(g_savedGlobalsnorman.sFlow.strandSavedVars[index].savedBitflags,SAVED_BITS_STRAND_ACTIVATED)
							AND NOT IS_BIT_SET(g_savedGlobalsnorman.sFlow.strandSavedVars[index].savedBitflags,SAVED_BITS_STRAND_TERMINATED)
							AND NOT (sCurrentStrandCommand.command = FLOW_LABEL)
							AND NOT (sCurrentStrandCommand.command = FLOW_LABEL_LAUNCHER_ONLY)
							AND (iStrandLastCommandPos[index] <> g_savedGlobalsnorman.sFlow.strandSavedVars[index].thisCommandPos)
							
								//Save command position.
								iStrandLastCommandPos[index] = g_savedGlobalsnorman.sFlow.strandSavedVars[index].thisCommandPos
								//Skip next command in strand.
								LAUNCHER_SKIP_NEXT_STRAND_COMMAND(flowLauncher, INT_TO_ENUM(STRANDS,index))
								//Get the new command we've stepped to.
								sCurrentStrandCommand = g_flowUnsaved.flowCommands[g_savedGlobalsnorman.sFlow.strandSavedVars[index].thisCommandPos]
							
							ENDWHILE
							
							//Line break debug output after a strand has been processed.
							CDEBUG1LN(DEBUG_FLOW_LAUNCHER, "------------------------------------------------------------")
										
							//Check if we've stepped to a new command or we're waiting for a script to load.
							IF g_savedGlobalsnorman.sFlow.strandSavedVars[index].thisCommandPos <> iStrandLastCommandPos[index]
							OR sCurrentStrandCommand.command = FLOW_LAUNCH_SCRIPT_AND_FORGET
								iSearchHaltCount = 0 //As long as at least one strand has stepped to a new command we're still progressing.
							ENDIF
							
							//We are looking for a label in this strand. Have we found it?
							IF NOT bStrandNoLabelRequirement[index]
							
								IF sCurrentStrandCommand.command = FLOW_LABEL
									IF INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.coreVars[sCurrentStrandCommand.index].iValue1) = paramStrandLabels[index]
										//We've found the label for this strand.
										CDEBUG1LN(DEBUG_FLOW_LAUNCHER, "Reached label [", GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(paramStrandLabels[index]), "] for strand ", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(INT_TO_ENUM(STRANDS, index)), ".")
										bStrandLabelFound[index] = TRUE
									ENDIF
								ENDIF
								
								IF sCurrentStrandCommand.command = FLOW_LABEL_LAUNCHER_ONLY
									IF INT_TO_ENUM(JUMP_LABEL_IDS, sCurrentStrandCommand.index) = paramStrandLabels[index]
										//We've found the label for this strand.
										CDEBUG1LN(DEBUG_FLOW_LAUNCHER, "Reached launcher only label [", GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(paramStrandLabels[index]), "] for strand ", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(INT_TO_ENUM(STRANDS, index)), ".")
										bStrandLabelFound[index] = TRUE
									ENDIF
								ENDIF
								
								//Check if we've found all labels.
								index2 = 0
								bFoundAllLabels = TRUE
								WHILE (index2 < ENUM_TO_INT(MAX_STRANDS_NRM)) AND bFoundAllLabels
									IF NOT bStrandLabelFound[index2]
										bFoundAllLabels = FALSE
									ENDIF
									index2++
								ENDWHILE
							
							//We're not looking for a label in this strand. Are we sitting on a safe breakpoint?
							ELSE
								IF sCurrentStrandCommand.command = FLOW_LABEL
									IF g_flowUnsaved.coreVars[sCurrentStrandCommand.index].iValue2 != 1
										CDEBUG1LN(DEBUG_FLOW_LAUNCHER, "Stepping over label that is an unsafe breakpoint. LABEL:", GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.coreVars[sCurrentStrandCommand.index].iValue1)), ".")
										// Stopped at a label that was not a safe breakpoint.
										// Step back strand index so we do another pass on the current strand.
										index--
									ENDIF
								ELIF sCurrentStrandCommand.command = FLOW_LABEL_LAUNCHER_ONLY
									CDEBUG1LN(DEBUG_FLOW_LAUNCHER, "Stepping over label that is an unsafe breakpoint. LABEL:", GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(INT_TO_ENUM(JUMP_LABEL_IDS, sCurrentStrandCommand.index)), ".")
									// Stopped at a label that was not a safe breakpoint.
									// Step back strand index so we do another pass on the current strand.
									index--
								ENDIF
							ENDIF
						
						ENDIF
					ENDIF
				ENDIF			
			ENDREPEAT
		#endif
		#if not USE_CLF_DLC
		#if not USE_NRM_DLC
			//Depth first search, but always process to the next label or holding command.
			REPEAT MAX_STRANDS index		
				//Check strand is active.
				IF IS_BIT_SET(g_savedGlobals.sFlow.strandSavedVars[index].savedBitflags,SAVED_BITS_STRAND_ACTIVATED)
					//Check strand hasn't been terminated.
					IF NOT IS_BIT_SET(g_savedGlobals.sFlow.strandSavedVars[index].savedBitflags,SAVED_BITS_STRAND_TERMINATED)
						//Skip next command in strand.
						IF NOT bStrandLabelFound[index]
						OR bStrandNoLabelRequirement[index]
						
							//Save command position.
							iStrandLastCommandPos[index] = g_savedGlobals.sFlow.strandSavedVars[index].thisCommandPos
							//Skip next command in strand.
							LAUNCHER_SKIP_NEXT_STRAND_COMMAND(flowLauncher, INT_TO_ENUM(STRANDS,index))
							//Get the new command we've stepped to.
							FLOW_COMMANDS sCurrentStrandCommand = g_flowUnsaved.flowCommands[g_savedGlobals.sFlow.strandSavedVars[index].thisCommandPos]
							
							WHILE IS_BIT_SET(g_savedGlobals.sFlow.strandSavedVars[index].savedBitflags,SAVED_BITS_STRAND_ACTIVATED)
							AND NOT IS_BIT_SET(g_savedGlobals.sFlow.strandSavedVars[index].savedBitflags,SAVED_BITS_STRAND_TERMINATED)
							AND NOT (sCurrentStrandCommand.command = FLOW_LABEL)
							AND NOT (sCurrentStrandCommand.command = FLOW_LABEL_LAUNCHER_ONLY)
							AND (iStrandLastCommandPos[index] <> g_savedGlobals.sFlow.strandSavedVars[index].thisCommandPos)
							
								//Save command position.
								iStrandLastCommandPos[index] = g_savedGlobals.sFlow.strandSavedVars[index].thisCommandPos
								//Skip next command in strand.
								LAUNCHER_SKIP_NEXT_STRAND_COMMAND(flowLauncher, INT_TO_ENUM(STRANDS,index))
								//Get the new command we've stepped to.
								sCurrentStrandCommand = g_flowUnsaved.flowCommands[g_savedGlobals.sFlow.strandSavedVars[index].thisCommandPos]
							
							ENDWHILE
							
							//Line break debug output after a strand has been processed.
							CDEBUG1LN(DEBUG_FLOW_LAUNCHER, "------------------------------------------------------------")
										
							//Check if we've stepped to a new command or we're waiting for a script to load.
							IF g_savedGlobals.sFlow.strandSavedVars[index].thisCommandPos <> iStrandLastCommandPos[index]
							OR sCurrentStrandCommand.command = FLOW_LAUNCH_SCRIPT_AND_FORGET
								iSearchHaltCount = 0 //As long as at least one strand has stepped to a new command we're still progressing.
							ENDIF
							
							//We are looking for a label in this strand. Have we found it?
							IF NOT bStrandNoLabelRequirement[index]
							
								IF sCurrentStrandCommand.command = FLOW_LABEL
									IF INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.coreVars[sCurrentStrandCommand.index].iValue1) = paramStrandLabels[index]
										//We've found the label for this strand.
										CDEBUG1LN(DEBUG_FLOW_LAUNCHER, "Reached label [", GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(paramStrandLabels[index]), "] for strand ", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(INT_TO_ENUM(STRANDS, index)), ".")
										bStrandLabelFound[index] = TRUE
									ENDIF
								ENDIF
								
								IF sCurrentStrandCommand.command = FLOW_LABEL_LAUNCHER_ONLY
									IF INT_TO_ENUM(JUMP_LABEL_IDS, sCurrentStrandCommand.index) = paramStrandLabels[index]
										//We've found the label for this strand.
										CDEBUG1LN(DEBUG_FLOW_LAUNCHER, "Reached launcher only label [", GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(paramStrandLabels[index]), "] for strand ", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(INT_TO_ENUM(STRANDS, index)), ".")
										bStrandLabelFound[index] = TRUE
									ENDIF
								ENDIF
								
								//Check if we've found all labels.
								index2 = 0
								bFoundAllLabels = TRUE
								WHILE (index2 < ENUM_TO_INT(MAX_STRANDS)) AND bFoundAllLabels
									IF NOT bStrandLabelFound[index2]
										bFoundAllLabels = FALSE
									ENDIF
									index2++
								ENDWHILE
							
							//We're not looking for a label in this strand. Are we sitting on a safe breakpoint?
							ELSE
								IF sCurrentStrandCommand.command = FLOW_LABEL
									IF g_flowUnsaved.coreVars[sCurrentStrandCommand.index].iValue2 != 1
										CDEBUG1LN(DEBUG_FLOW_LAUNCHER, "Stepping over label that is an unsafe breakpoint. LABEL:", GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.coreVars[sCurrentStrandCommand.index].iValue1)), ".")
										// Stopped at a label that was not a safe breakpoint.
										// Step back strand index so we do another pass on the current strand.
										index--
									ENDIF
								ELIF sCurrentStrandCommand.command = FLOW_LABEL_LAUNCHER_ONLY
									CDEBUG1LN(DEBUG_FLOW_LAUNCHER, "Stepping over label that is an unsafe breakpoint. LABEL:", GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(INT_TO_ENUM(JUMP_LABEL_IDS, sCurrentStrandCommand.index)), ".")
									// Stopped at a label that was not a safe breakpoint.
									// Step back strand index so we do another pass on the current strand.
									index--
								ENDIF
							ENDIF
						
						ENDIF
					ENDIF
				ENDIF
				
			ENDREPEAT	
		#endif		
		#endif
		//Check if the search has failed to progress on this iteration.
		IF NOT bFoundAllLabels
			CDEBUG1LN(DEBUG_FLOW_LAUNCHER, "Flow hasn't progressed for the last ", iSearchHaltCount, " iterations.")  
			IF iSearchHaltCount > 2
				//All strands have been halted for 2 iterations and we haven't found all labels.
				//It is impossible to hit all labels at once.
				//Return a negative result.
				CDEBUG1LN(DEBUG_FLOW_LAUNCHER, "Impossible to reach all labels. Failed to update flow.")
				CDEBUG1LN(DEBUG_FLOW_LAUNCHER, "=================================")
				CDEBUG1LN(DEBUG_FLOW_LAUNCHER, "      FLOW LAUNCHER - END")
				CDEBUG1LN(DEBUG_FLOW_LAUNCHER, "=================================")
				
				LAUNCHER_RESET_FLOW() //Reset the flow to a stable state.
				g_flowUnsaved.bUpdatingGameflow = FALSE				
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(0)
				ENDIF				
				RETURN FALSE
			ENDIF
		ENDIF
		
	ENDWHILE
	
	//All labels found! 
	//Return a positive result.
	CDEBUG1LN(DEBUG_FLOW_LAUNCHER, "Activated to all labels! Flow sucessfully updated.")
	CDEBUG1LN(DEBUG_FLOW_LAUNCHER, "=================================")
	CDEBUG1LN(DEBUG_FLOW_LAUNCHER, "      FLOW LAUNCHER - END")
	CDEBUG1LN(DEBUG_FLOW_LAUNCHER, "=================================")
	
	g_flowUnsaved.bUpdatingGameflow = FALSE
	RETURN TRUE

ENDFUNC

//PURPOSE:		Activates the game to a special label placed at the very end of the flow and generates
//				a playthrough order xml file as it goes. This will create a complete playthrough order.
//
PROC LAUNCHER_GENERATE_PLAYTHROUGH_ORDER(FLOW_LAUNCHER_VARS &flowLauncher)
	//Skip to the final label in the flow while writing out missions names to file.
	LAUNCHER_LAUNCH_FLOW_TO_LABEL(flowLauncher,  LAUNCHER_GET_FLOW_FINAL_LABEL(), -1, TRUE, FALSE)
	
	//Reset the flow once done.
	LAUNCHER_RESET_FLOW()
ENDPROC


//PURPOSE:		Launches the game flow to the very end and writes out an XML file that describes
//				the flow structure as it goes. This XML can be used to render a dynamic flow
//				diagram with the 'Flow Diagram' script system.
//
PROC LAUNCHER_GENERATE_FLOW_DIAGRAM_XML( FLOW_LAUNCHER_VARS &flowLauncher)
	//Skip to the final label in the flow while writing out missions names to file.
	LAUNCHER_LAUNCH_FLOW_TO_LABEL(flowLauncher,  LAUNCHER_GET_FLOW_FINAL_LABEL(), -1, FALSE, TRUE)
	
	//Reset the flow once done.
	LAUNCHER_RESET_FLOW()
	
	g_flowDiagram.bFlowDiagramXMLGenerated = TRUE
ENDPROC


//PURPOSE:		Creates the 'Flow Launcher' debug widget. This widget allows a dev to
//				select a number of flow labels (max. one per strand) and attempt to launch
//				the flow to all of these labels at once. Useful for recreating exact flow
//				states.
//
PROC LAUNCHER_CREATE_LAUNCH_TO_MULTIPLE_LABELS_WIDGET(FLOW_LAUNCHER_VARS &flowLauncher)
	INT iIndex
	INT iIndex2
	INT iLabelCount

	START_WIDGET_GROUP("Flow Launcher")
	
		ADD_WIDGET_STRING("Label Selections")
	
		int iRepeatNum=0
		iRepeatNum=ENUM_TO_INT( MAX_STRANDS)
		
		#if USE_CLF_DLC		
			iRepeatNum= ENUM_TO_INT(MAX_STRANDS_CLF)
		#endif	
		#if USE_NRM_DLC
			iRepeatNum= ENUM_TO_INT(MAX_STRANDS_NRM)
		#endif
		
		REPEAT iRepeatNum iIndex
			START_NEW_WIDGET_COMBO()
				//Add default "unspecified" option.
				ADD_TO_WIDGET_COMBO("No_Label_Specified")
				flowLauncher.eLauncherStrandLabels[iIndex][0] = STAY_ON_THIS_COMMAND
				
				iLabelCount = 1
			
				//Iterate through all commands in this strand's range.
				FOR iIndex2 = g_flowUnsaved.flowCommandsIndex[iIndex].lower TO g_flowUnsaved.flowCommandsIndex[iIndex].upper
					//Check if the command at this iIndex is a 'Label' command.
					IF g_flowUnsaved.flowCommands[iIndex2].command = FLOW_LABEL
						//Add a combo entry for this label.
						JUMP_LABEL_IDS eLabel = INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.coreVars[g_flowUnsaved.flowCommands[iIndex2].index].iValue1)
						ADD_TO_WIDGET_COMBO(Get_Label_Display_String_From_Label_ID(eLabel))
						
						//Save the label at this combo position.
						flowLauncher.eLauncherStrandLabels[iIndex][iLabelCount] = eLabel
						iLabelCount++
					ENDIF
					
					//Check if the command at this iIndex is a 'Label Launcher Only' command.
					IF g_flowUnsaved.flowCommands[iIndex2].command = FLOW_LABEL_LAUNCHER_ONLY
						//Add a combo entry for this label.
						JUMP_LABEL_IDS eLabel = INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.flowCommands[iIndex2].index)
						ADD_TO_WIDGET_COMBO(Get_Label_Display_String_From_Label_ID(eLabel))
						
						//Save the label at this combo position.
						flowLauncher.eLauncherStrandLabels[iIndex][iLabelCount] = eLabel
						iLabelCount++
					ENDIF
				ENDFOR
				
			STOP_WIDGET_COMBO(GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(INT_TO_ENUM(STRANDS, iIndex)), flowLauncher.iLauncherTargetStrandLabels[iIndex])
		ENDREPEAT
		
		ADD_WIDGET_STRING("")
		
		//Add a bool to trigger the flow activation.
		ADD_WIDGET_BOOL("Launch Flow", flowLauncher.bLauncherRunMultipleLabelLaunch)
		
		//Add a text widget displays the last activation result.
		flowLauncher.widgetLauncherActivationResult = ADD_TEXT_WIDGET("Last Launch Result")
		SET_CONTENTS_OF_TEXT_WIDGET(flowLauncher.widgetLauncherActivationResult, "No Launch Attempted")
		
	STOP_WIDGET_GROUP()
ENDPROC


//PURPOSE:		The update procedure for the 'Flow Launcher' widget. Should be called
//				once per frame to ensure the widget is maintained correctly.
//
PROC LAUNCHER_UPDATE_ACTIVATE_TO_MULTIPLE_LABELS_WIDGET(FLOW_LAUNCHER_VARS &flowLauncher)
	INT index
	BOOL bNoLabelsSelected = TRUE

	IF flowLauncher.bLauncherRunMultipleLabelLaunch
		flowLauncher.bLauncherRunMultipleLabelLaunch = FALSE
	
		JUMP_LABEL_IDS eTargetLabels[MAX_STRANDS]
		#if USE_CLF_DLC
			REPEAT MAX_STRANDS_CLF index
				eTargetLabels[index] = flowLauncher.eLauncherStrandLabels[index][flowLauncher.iLauncherTargetStrandLabels[index]]
				IF  eTargetLabels[index] <> STAY_ON_THIS_COMMAND
				AND eTargetLabels[index] <> NEXT_SEQUENTIAL_COMMAND
					bNoLabelsSelected = FALSE
				ENDIF
			ENDREPEAT
		#endif
		#if USE_NRM_DLC
			REPEAT MAX_STRANDS_NRM index
				eTargetLabels[index] = flowLauncher.eLauncherStrandLabels[index][flowLauncher.iLauncherTargetStrandLabels[index]]
				IF  eTargetLabels[index] <> STAY_ON_THIS_COMMAND
				AND eTargetLabels[index] <> NEXT_SEQUENTIAL_COMMAND
					bNoLabelsSelected = FALSE
				ENDIF
			ENDREPEAT
		#endif
		
		#if not USE_CLF_DLC
		#if not USE_NRM_DLC
			REPEAT MAX_STRANDS index
				eTargetLabels[index] = flowLauncher.eLauncherStrandLabels[index][flowLauncher.iLauncherTargetStrandLabels[index]]
				IF  eTargetLabels[index] <> STAY_ON_THIS_COMMAND
				AND eTargetLabels[index] <> NEXT_SEQUENTIAL_COMMAND
					bNoLabelsSelected = FALSE
				ENDIF
			ENDREPEAT
		#endif
		#endif
		
		IF bNoLabelsSelected
			SET_CONTENTS_OF_TEXT_WIDGET(flowLauncher.widgetLauncherActivationResult, "No Labels Selected")
		ELIF LAUNCHER_LAUNCH_FLOW_TO_MULTIPLE_LABELS(flowLauncher, eTargetLabels)
			SET_CONTENTS_OF_TEXT_WIDGET(flowLauncher.widgetLauncherActivationResult, "Launch Successful")
		ELSE
			SET_CONTENTS_OF_TEXT_WIDGET(flowLauncher.widgetLauncherActivationResult, "Launch Failed")
		ENDIF
		
		IF IS_SCREEN_FADED_OUT()
			DO_SCREEN_FADE_IN(250)
		ENDIF
	ENDIF
ENDPROC



USING "flow_launcher_main_GAME.sch"


#ENDIF
