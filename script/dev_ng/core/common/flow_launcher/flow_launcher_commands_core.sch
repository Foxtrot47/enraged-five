
USING "flow_launcher_commands_GAME.sch"

#if USE_CLF_DLC
	Using "static_mission_data_helpCLF.sch"
#endif
#if use_NRM_DLC
	Using "static_mission_data_helpNRM.sch"
#endif

#IF NOT DEFINED(LAUNCHER_SKIP_NEXT_STRAND_COMMAND_CUSTOM)
FUNC JUMP_LABEL_IDS LAUNCHER_SKIP_NEXT_STRAND_COMMAND_CUSTOM(FLOW_LAUNCHER_VARS &flowLauncher, STRANDS paramStrand, BOOL paramGeneratePlaythroughOrder = FALSE, BOOL paramGenerateFlowDiagram = FALSE)
	flowLauncher.bLauncherRunMultipleLabelLaunch = flowLauncher.bLauncherRunMultipleLabelLaunch
	paramStrand = paramStrand
	paramGenerateFlowDiagram = paramGenerateFlowDiagram
	paramGeneratePlaythroughOrder = paramGeneratePlaythroughOrder
	RETURN NEXT_SEQUENTIAL_COMMAND
ENDFUNC
#ENDIF

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═════════════════╡ Flow Launcher - Core Engine Procedures ╞══════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

//PURPOSE:		Tells the flow launcher to skip/process the next command in a strand.
//
//PARAM NOTES:	paramStrand - The strand in which a command is to be processed/skipped.
//				paramGeneratePlaythroughOrder - Are we generating a playthrough order XML file containing an ordered list of mission names?
//				paramGenerateFlowDiagram - Are we generating an XML diagram to be used by the flow_diagram system?
//
PROC LAUNCHER_SKIP_NEXT_STRAND_COMMAND(FLOW_LAUNCHER_VARS &flowLauncher, STRANDS paramStrand, BOOL paramGeneratePlaythroughOrder = FALSE, BOOL paramGenerateFlowDiagram = FALSE)
	JUMP_LABEL_IDS eNextLabel
	
	#if USE_CLF_DLC
		//Save current command data from the strand's current pointer position.
		FLOW_COMMANDS sCurrentCommand = g_flowUnsaved.flowCommands[g_savedGlobalsClifford.sFlow.strandSavedVars[paramStrand].thisCommandPos]
		CDEBUG1LN(DEBUG_FLOW_LAUNCHER,"AGT: LAUNCHER_SKIP_NEXT_STRAND_COMMAND command: ", GET_COMMAND_DISPLAY_STRING_FROM_COMMAND_ID(sCurrentCommand.command)," from strand: ",GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(paramStrand))
	#endif
	#if USE_NRM_DLC
		//Save current command data from the strand's current pointer position.
		FLOW_COMMANDS sCurrentCommand = g_flowUnsaved.flowCommands[g_savedGlobalsnorman.sFlow.strandSavedVars[paramStrand].thisCommandPos]
		CDEBUG1LN(DEBUG_FLOW_LAUNCHER,"NRM: LAUNCHER_SKIP_NEXT_STRAND_COMMAND command: ", GET_COMMAND_DISPLAY_STRING_FROM_COMMAND_ID(sCurrentCommand.command)," from strand: ",GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(paramStrand))
	#endif
	
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
		//Save current command data from the strand's current pointer position.
		FLOW_COMMANDS sCurrentCommand = g_flowUnsaved.flowCommands[g_savedGlobals.sFlow.strandSavedVars[paramStrand].thisCommandPos]
	#endif
	#endif
	
	//Check what the next command is and deal with it accordingly.
	SWITCH (sCurrentCommand.command)
	
		CASE FLOW_DEBUG_DUMMY
			SCRIPT_ASSERT("LAUNCHER_SKIP_NEXT_STRAND_COMMAND: Skipping a FLOW_DEBUG_DUMMY command in debug. These commands should only exist in release.")
			LAUNCHER_DISPLAY_GENERIC_COMMAND_DEBUG_OUTPUT(paramStrand, sCurrentCommand.command, TRUE)
			eNextLabel = NEXT_SEQUENTIAL_COMMAND
			BREAK
	
		CASE FLOW_ACTIVATE_STRAND
			CDEBUG1LN(DEBUG_FLOW_LAUNCHER, GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(paramStrand), ": Performing '", GET_COMMAND_DISPLAY_STRING_FROM_COMMAND_ID(sCurrentCommand.command), "(", GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(INT_TO_ENUM(STRANDS, sCurrentCommand.index)),")'")
			
			//If we're generating a flow diagram, write this activate strand command to xml in the specified debug file.
			IF paramGenerateFlowDiagram
				LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, "   <activate	strand=\"")
				LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(paramStrand))
				LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, "\"")
				LAUNCHER_WRITE_STRING_TABS_TO_DIAGRAM_XML_FILE	(flowLauncher, 6, GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(paramStrand))
				LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, "name=\"")
				LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(INT_TO_ENUM(STRANDS, sCurrentCommand.index)))
				LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, "\"/>")
				LAUNCHER_WRITE_NEW_LINE_TO_DIAGRAM_XML_FILE		(flowLauncher)
				flowLauncher.iLauncherCurrentDiagramElementID++
			ENDIF
		
			//Call Keith's flow command.
			eNextLabel = PERFORM_ACTIVATE_STRAND(sCurrentCommand.index)
		BREAK
		
		CASE FLOW_AWAIT_ACTIVATION
			LAUNCHER_DISPLAY_GENERIC_COMMAND_DEBUG_OUTPUT(paramStrand, sCurrentCommand.command, FALSE)
		
			//Simply skip to the next command.
			//If this strand is being processed we already know it's active.
			eNextLabel = NEXT_SEQUENTIAL_COMMAND
		BREAK
		
		CASE FLOW_AWAIT_SYNCHRONISATION
			CDEBUG1LN(DEBUG_FLOW_LAUNCHER, GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(paramStrand), ": Performing '", GET_COMMAND_DISPLAY_STRING_FROM_COMMAND_ID(sCurrentCommand.command), "(", GET_SYNCHRONISATION_DISPLAY_STRING_FROM_SYNC_ID(INT_TO_ENUM(SYNCHRONIZATION_IDS, (sCurrentCommand.index%FLOW_COMMAND_HASH_MODIFIER_MAGNITUDE))), ")'")
			
			//Call flow command.
			eNextLabel = PERFORM_AWAIT_SYNCHRONISATION(sCurrentCommand.index)
			
			//If we're generating a flow diagram, write this await sync strand command to xml in the specified debug file.
			IF paramGenerateFlowDiagram
				IF eNextLabel = NEXT_SEQUENTIAL_COMMAND
					LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, "   <awaitSync	strand=\"")
					LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(paramStrand))
					LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, "\"")
					LAUNCHER_WRITE_STRING_TABS_TO_DIAGRAM_XML_FILE	(flowLauncher, 6, GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(paramStrand))
					LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, "syncID=\"")
					LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, GET_SYNCHRONISATION_DISPLAY_STRING_FROM_SYNC_ID(INT_TO_ENUM(SYNCHRONIZATION_IDS, (sCurrentCommand.index%FLOW_COMMAND_HASH_MODIFIER_MAGNITUDE))))
					LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, "\"/>")
					LAUNCHER_WRITE_NEW_LINE_TO_DIAGRAM_XML_FILE		(flowLauncher)
					flowLauncher.iLauncherCurrentDiagramElementID++
				ENDIF
			ENDIF
		BREAK
		
		CASE FLOW_DO_BITSET_JUMPS
			LAUNCHER_DISPLAY_GENERIC_COMMAND_DEBUG_OUTPUT(paramStrand, sCurrentCommand.command, FALSE)
		
			//Call flow command.
			eNextLabel = PERFORM_DO_BITSET_JUMPS(sCurrentCommand.index)
		BREAK
		
		CASE FLOW_DO_FLAG_JUMPS
			LAUNCHER_DISPLAY_GENERIC_COMMAND_DEBUG_OUTPUT(paramStrand, sCurrentCommand.command, TRUE)
		
			//Run the launcher specific skip command.
			eNextLabel = LAUNCHER_SKIP_DO_FLAG_JUMPS_COMMAND(sCurrentCommand.index)
		BREAK
		
		CASE FLOW_DO_INT_JUMPS
			LAUNCHER_DISPLAY_GENERIC_COMMAND_DEBUG_OUTPUT(paramStrand, sCurrentCommand.command, FALSE)
			
			//Call flow command.
			eNextLabel = PERFORM_DO_INT_JUMPS(sCurrentCommand.index)
		BREAK
		
		CASE FLOW_DO_STRAND_KILLED_JUMPS
			LAUNCHER_DISPLAY_GENERIC_COMMAND_DEBUG_OUTPUT(paramStrand, sCurrentCommand.command, FALSE)
		
			//Call flow command.
			eNextLabel = PERFORM_DO_STRAND_KILLED_JUMPS(sCurrentCommand.index)
		BREAK
		
		CASE FLOW_IS_GAME_TIME_AFTER_HOUR
			LAUNCHER_DISPLAY_GENERIC_COMMAND_DEBUG_OUTPUT(paramStrand, sCurrentCommand.command, FALSE)
		
			//Follow the debug jump for this command.
			eNextLabel = INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.coreVars[sCurrentCommand.index].iValue4)
		BREAK
		CASE FLOW_GOTO
			LAUNCHER_DISPLAY_GENERIC_COMMAND_DEBUG_OUTPUT(paramStrand, sCurrentCommand.command, FALSE)
		
			//Set next jump as this GOTO command's jump label.
			eNextLabel = INT_TO_ENUM(JUMP_LABEL_IDS, sCurrentCommand.index)
		BREAK
		CASE FLOW_STRAND_GOTO
			LAUNCHER_DISPLAY_GENERIC_COMMAND_DEBUG_OUTPUT(paramStrand, sCurrentCommand.command, FALSE)
						
			eNextLabel = PERFORM_STRAND_GOTO(sCurrentCommand.index)			
		BREAK
		CASE FLOW_KILL_STRAND
			LAUNCHER_DISPLAY_GENERIC_COMMAND_DEBUG_OUTPUT(paramStrand, sCurrentCommand.command, FALSE)
		
			//Call flow command.
			eNextLabel = PERFORM_KILL_STRAND(sCurrentCommand.index)
		BREAK
		
		CASE FLOW_LABEL
			CDEBUG1LN(DEBUG_FLOW_LAUNCHER, GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(paramStrand), ": Skipping '", GET_COMMAND_DISPLAY_STRING_FROM_COMMAND_ID(sCurrentCommand.command), "(", GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(INT_TO_ENUM(JUMP_LABEL_IDS, PICK_INT(sCurrentCommand.command = FLOW_LABEL, g_flowUnsaved.coreVars[sCurrentCommand.index].iValue1, sCurrentCommand.index))), ")'")
			
			//Now skip to the next command.
			eNextLabel = NEXT_SEQUENTIAL_COMMAND
		BREAK
		
		#IF IS_DEBUG_BUILD
			CASE FLOW_LABEL_LAUNCHER_ONLY
				CDEBUG1LN(DEBUG_FLOW_LAUNCHER, GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(paramStrand), ": Skipping '", GET_COMMAND_DISPLAY_STRING_FROM_COMMAND_ID(sCurrentCommand.command), "(", GET_LABEL_DISPLAY_STRING_FROM_LABEL_ID(INT_TO_ENUM(JUMP_LABEL_IDS, PICK_INT(sCurrentCommand.command = FLOW_LABEL, g_flowUnsaved.coreVars[sCurrentCommand.index].iValue1, sCurrentCommand.index))), ")'")
			
				//Simply skip to the next command.
				eNextLabel = NEXT_SEQUENTIAL_COMMAND
			BREAK
		#ENDIF

		CASE FLOW_SEND_SYNCHRONISATION
			CDEBUG1LN(DEBUG_FLOW_LAUNCHER, GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(paramStrand), ": Performing '", GET_COMMAND_DISPLAY_STRING_FROM_COMMAND_ID(sCurrentCommand.command), "(", GET_SYNCHRONISATION_DISPLAY_STRING_FROM_SYNC_ID(INT_TO_ENUM(SYNCHRONIZATION_IDS, (sCurrentCommand.index%FLOW_COMMAND_HASH_MODIFIER_MAGNITUDE))), ")'")
			
			//If we're generating a flow diagram, write this send sync strand command to xml in the specified debug file.
			IF paramGenerateFlowDiagram
				LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, "   <sendSync	strand=\"")
				LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(paramStrand))
				LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, "\"")
				LAUNCHER_WRITE_STRING_TABS_TO_DIAGRAM_XML_FILE	(flowLauncher, 6, GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(paramStrand))
				LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, "syncID=\"")
				LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, GET_SYNCHRONISATION_DISPLAY_STRING_FROM_SYNC_ID(INT_TO_ENUM(SYNCHRONIZATION_IDS, (sCurrentCommand.index%FLOW_COMMAND_HASH_MODIFIER_MAGNITUDE))))
				LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, "\"/>")
				LAUNCHER_WRITE_NEW_LINE_TO_DIAGRAM_XML_FILE		(flowLauncher)
				flowLauncher.iLauncherCurrentDiagramElementID++
			ENDIF	
				
			//Call flow command.
			eNextLabel = PERFORM_SEND_SYNCHRONISATION(sCurrentCommand.index)
		BREAK
		
		CASE FLOW_SET_GAME_CLOCK
			LAUNCHER_DISPLAY_GENERIC_COMMAND_DEBUG_OUTPUT(paramStrand, sCurrentCommand.command, FALSE)
		
			//Call flow command.
			eNextLabel = PERFORM_SET_GAME_CLOCK(sCurrentCommand.index)
		BREAK
		
		CASE FLOW_STORE_BITSET_BIT_STATE
			LAUNCHER_DISPLAY_GENERIC_COMMAND_DEBUG_OUTPUT(paramStrand, sCurrentCommand.command, FALSE)
		
			//Call flow command.
			eNextLabel = PERFORM_STORE_BITSET_BIT_STATE(sCurrentCommand.index)
		BREAK
		
		CASE FLOW_STORE_FLAG_STATE
			LAUNCHER_DISPLAY_GENERIC_COMMAND_DEBUG_OUTPUT(paramStrand, sCurrentCommand.command, FALSE)
		
			//Call flow command.
			eNextLabel = PERFORM_STORE_FLAG_STATE(sCurrentCommand.index)
		BREAK
		
		CASE FLOW_STORE_INT_VALUE
			LAUNCHER_DISPLAY_GENERIC_COMMAND_DEBUG_OUTPUT(paramStrand, sCurrentCommand.command, FALSE)
		
			//Call flow command.
			eNextLabel = PERFORM_STORE_INT_VALUE(sCurrentCommand.index)
		BREAK
		
		CASE FLOW_TERMINATE
			LAUNCHER_DISPLAY_GENERIC_COMMAND_DEBUG_OUTPUT(paramStrand, sCurrentCommand.command, FALSE)
		
			//Set this strand's terminated bit.
			#if USE_CLF_DLC
				SET_BIT(g_savedGlobalsClifford.sFlow.strandSavedVars[paramStrand].savedBitflags, SAVED_BITS_STRAND_TERMINATED)
			#endif	
			#if USE_NRM_DLC
				SET_BIT(g_savedGlobalsnorman.sFlow.strandSavedVars[paramStrand].savedBitflags, SAVED_BITS_STRAND_TERMINATED)
			#endif	
			#if not USE_CLF_DLC
			#if not USE_NRM_DLC
				SET_BIT(g_savedGlobals.sFlow.strandSavedVars[paramStrand].savedBitflags, SAVED_BITS_STRAND_TERMINATED)
			#endif
			#endif
			eNextLabel = STAY_ON_THIS_COMMAND
		BREAK
		
		CASE FLOW_WAIT_IN_GAME_TIME
			LAUNCHER_DISPLAY_GENERIC_COMMAND_DEBUG_OUTPUT(paramStrand, sCurrentCommand.command, TRUE)
		
			//Simply skip to the next command.
			eNextLabel = NEXT_SEQUENTIAL_COMMAND
		BREAK
		
		CASE FLOW_WAIT_IN_REAL_TIME
			LAUNCHER_DISPLAY_GENERIC_COMMAND_DEBUG_OUTPUT(paramStrand, sCurrentCommand.command, TRUE)
		
			//Simply skip to the next command.
			eNextLabel = NEXT_SEQUENTIAL_COMMAND
		BREAK
		
		CASE FLOW_WAIT_UNTIL_GAME_HOUR
			LAUNCHER_DISPLAY_GENERIC_COMMAND_DEBUG_OUTPUT(paramStrand, sCurrentCommand.command, TRUE)
		
			//Run the launcher specific skip command.
			eNextLabel = LAUNCHER_SKIP_WAIT_UNTIL_GAME_HOUR_COMMAND(sCurrentCommand.index)
		BREAK
		
		CASE FLOW_WAIT_UNTIL_GAME_TIME_AND_DATE
			LAUNCHER_DISPLAY_GENERIC_COMMAND_DEBUG_OUTPUT(paramStrand, sCurrentCommand.command, TRUE)
		
			//Run the launcher specific skip command.
			eNextLabel = LAUNCHER_SKIP_WAIT_UNTIL_GAME_TIME_AND_DATE_COMMAND(sCurrentCommand.index)
		BREAK
		
		CASE FLOW_ACTIVATE_RANDOMCHAR_MISSION
			LAUNCHER_DISPLAY_GENERIC_COMMAND_DEBUG_OUTPUT(paramStrand, sCurrentCommand.command, TRUE)

			//Run the launcher specific skip command.
			eNextLabel = LAUNCHER_SKIP_ACTIVATE_RANDOMCHAR_MISSION(flowLauncher, paramStrand, sCurrentCommand.index)
		BREAK
		
		CASE FLOW_DEACTIVATE_RANDOMCHAR_MISSION
			LAUNCHER_DISPLAY_GENERIC_COMMAND_DEBUG_OUTPUT(paramStrand, sCurrentCommand.command, FALSE)

			// Call flow command.
			eNextLabel = PERFORM_DEACTIVATE_RANDOMCHAR_MISSION(sCurrentCommand.index)
		BREAK
			
		CASE FLOW_DISPLAY_HELP_TEXT
			LAUNCHER_DISPLAY_GENERIC_COMMAND_DEBUG_OUTPUT(paramStrand, sCurrentCommand.command, TRUE)
		
			//Run the launcher specific skip command.
			eNextLabel = LAUNCHER_SKIP_DISPLAY_HELP_TEXT(sCurrentCommand.index)
		BREAK
		
		CASE FLOW_CANCEL_HELP_TEXT
			LAUNCHER_DISPLAY_GENERIC_COMMAND_DEBUG_OUTPUT(paramStrand, sCurrentCommand.command, FALSE)
		
			/// Call flow command.
			eNextLabel = PERFORM_CANCEL_HELP_TEXT(sCurrentCommand.index)
		BREAK
		
		
		CASE FLOW_DO_MISSION_AT_BLIP
			#if USE_CLF_DLC
				CDEBUG1LN(DEBUG_FLOW_LAUNCHER, GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(paramStrand), ": Skipping '", GET_COMMAND_DISPLAY_STRING_FROM_COMMAND_ID(sCurrentCommand.command), "(", GET_SCRIPT_FROM_HASH(g_sMissionStaticData[g_flowUnsaved.coreVars[sCurrentCommand.index].iValue1].scriptHash), ")'")
			#endif
			#if USE_NRM_DLC
				CDEBUG1LN(DEBUG_FLOW_LAUNCHER, GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(paramStrand), ": Skipping '", GET_COMMAND_DISPLAY_STRING_FROM_COMMAND_ID(sCurrentCommand.command), "(", GET_SCRIPT_FROM_HASH_NRM(g_sMissionStaticData[g_flowUnsaved.coreVars[sCurrentCommand.index].iValue1].scriptHash), ")'")
			#endif	
			#if not USE_CLF_DLC
			#if not USE_NRM_DLC
				CDEBUG1LN(DEBUG_FLOW_LAUNCHER, GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(paramStrand), ": Skipping '", GET_COMMAND_DISPLAY_STRING_FROM_COMMAND_ID(sCurrentCommand.command), "(", g_sMissionStaticData[g_flowUnsaved.coreVars[sCurrentCommand.index].iValue1].scriptName, ")'")
			#endif	
			#endif
			//Call launcher specific function.
			eNextLabel = LAUNCHER_SKIP_DO_MISSION_COMMAND(flowLauncher, paramStrand, sCurrentCommand.index, paramGeneratePlaythroughOrder, paramGenerateFlowDiagram)
		BREAK
		
		CASE FLOW_DO_MISSION_NOW
			#if USE_CLF_DLC
				CDEBUG1LN(DEBUG_FLOW_LAUNCHER, GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(paramStrand), ": Skipping '", GET_COMMAND_DISPLAY_STRING_FROM_COMMAND_ID(sCurrentCommand.command), "(", GET_SCRIPT_FROM_HASH(g_sMissionStaticData[g_flowUnsaved.coreVars[sCurrentCommand.index].iValue1].scriptHash), ")'")
			#endif	
			#if USE_NRM_DLC
				CDEBUG1LN(DEBUG_FLOW_LAUNCHER, GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(paramStrand), ": Skipping '", GET_COMMAND_DISPLAY_STRING_FROM_COMMAND_ID(sCurrentCommand.command), "(", GET_SCRIPT_FROM_HASH_NRM(g_sMissionStaticData[g_flowUnsaved.coreVars[sCurrentCommand.index].iValue1].scriptHash), ")'")
			#endif
			#if not USE_CLF_DLC
			#if not USE_NRM_DLC
				CDEBUG1LN(DEBUG_FLOW_LAUNCHER, GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(paramStrand), ": Skipping '", GET_COMMAND_DISPLAY_STRING_FROM_COMMAND_ID(sCurrentCommand.command), "(", g_sMissionStaticData[g_flowUnsaved.coreVars[sCurrentCommand.index].iValue1].scriptName, ")'")
			#endif	
			#endif
			//Call launcher specific function.
			eNextLabel = LAUNCHER_SKIP_DO_MISSION_COMMAND(flowLauncher, paramStrand, sCurrentCommand.index, paramGeneratePlaythroughOrder, paramGenerateFlowDiagram)
		BREAK
		
		CASE FLOW_LAUNCH_SCRIPT_AND_FORGET
			LAUNCHER_DISPLAY_GENERIC_COMMAND_DEBUG_OUTPUT(paramStrand, sCurrentCommand.command, FALSE)
		
			//Call flow command.
			eNextLabel = PERFORM_LAUNCH_SCRIPT_AND_FORGET(sCurrentCommand.index)
			
			//A script needs to stream in. Wait a frame.
			IF eNextLabel = STAY_ON_THIS_COMMAND
				WAIT(0)
			ENDIF
		BREAK
		
		CASE FLOW_RUN_CODE_ID
			LAUNCHER_DISPLAY_GENERIC_COMMAND_DEBUG_OUTPUT(paramStrand, sCurrentCommand.command, TRUE)
		
			//Call flow command.
			eNextLabel = LAUNCHER_SKIP_RUN_CODE_ID(sCurrentCommand.index)
		BREAK
		
		#IF IS_DEBUG_BUILD
		CASE FLOW_DEBUG_RUN_CODE_ID
			LAUNCHER_DISPLAY_GENERIC_COMMAND_DEBUG_OUTPUT(paramStrand, sCurrentCommand.command, TRUE)
			
			//Call flow command.
			eNextLabel = LAUNCHER_SKIP_DEBUG_RUN_CODE_ID(sCurrentCommand.index)
		BREAK
		#ENDIF
		
		CASE FLOW_ENABLE_BLIP_LONG_RANGE
			LAUNCHER_DISPLAY_GENERIC_COMMAND_DEBUG_OUTPUT(paramStrand, sCurrentCommand.command, FALSE)
		
			//Call flow command.
			eNextLabel = PERFORM_ENABLE_BLIP_LONG_RANGE(sCurrentCommand.index)
		BREAK

		CASE FLOW_DISABLE_BLIP_LONG_RANGE
			LAUNCHER_DISPLAY_GENERIC_COMMAND_DEBUG_OUTPUT(paramStrand, sCurrentCommand.command, FALSE)
		
			//Call flow command.
			eNextLabel = PERFORM_DISABLE_BLIP_LONG_RANGE(sCurrentCommand.index)
		BREAK
		
		CASE FLOW_SET_BUILDING_STATE
			LAUNCHER_DISPLAY_GENERIC_COMMAND_DEBUG_OUTPUT(paramStrand, sCurrentCommand.command, FALSE)
			
			//Call flow command.
			eNextLabel = PERFORM_SET_BUILDING_STATE(sCurrentCommand.index)
		BREAK
		
		CASE FLOW_SET_WEAPON_LOCK_STATE
			LAUNCHER_DISPLAY_GENERIC_COMMAND_DEBUG_OUTPUT(paramStrand, sCurrentCommand.command, FALSE)
		
			//Call flow command.
			eNextLabel = PERFORM_SET_WEAPON_LOCK_STATE(sCurrentCommand.index)
		BREAK
		
		CASE FLOW_SET_STATIC_BLIP_STATE
			LAUNCHER_DISPLAY_GENERIC_COMMAND_DEBUG_OUTPUT(paramStrand, sCurrentCommand.command, FALSE)
		
			//Call flow command.
			eNextLabel = PERFORM_SET_STATIC_BLIP_STATE(sCurrentCommand.index)
		BREAK
		
		DEFAULT
			eNextLabel = LAUNCHER_SKIP_NEXT_STRAND_COMMAND_CUSTOM(flowLauncher, paramStrand, paramGeneratePlaythroughOrder, paramGenerateFlowDiagram)
		BREAK
	ENDSWITCH
	
	//If this strand hasn't been terminated, jump command pointer to the next command.
	#if USE_CLF_DLC
		IF NOT (IS_BIT_SET(g_savedGlobalsClifford.sFlow.strandSavedVars[paramStrand].savedBitflags, SAVED_BITS_STRAND_TERMINATED))
			FLOW_FOLLOW_JUMP(paramStrand, eNextLabel)
		ENDIF	
	#endif
	#if USE_NRM_DLC
		IF NOT (IS_BIT_SET(g_savedGlobalsnorman.sFlow.strandSavedVars[paramStrand].savedBitflags, SAVED_BITS_STRAND_TERMINATED))
			FLOW_FOLLOW_JUMP(paramStrand, eNextLabel)
		ENDIF	
	#endif
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
		IF NOT (IS_BIT_SET(g_savedGlobals.sFlow.strandSavedVars[paramStrand].savedBitflags, SAVED_BITS_STRAND_TERMINATED))
			FLOW_FOLLOW_JUMP(paramStrand, eNextLabel)
		ENDIF	
	#endif
	#endif

ENDPROC


