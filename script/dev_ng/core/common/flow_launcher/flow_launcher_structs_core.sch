USING "flow_commands_enums_GAME.sch"
USING "flow_structs_core.sch"
USING "flow_launcher_structs_GAME.sch"

#IF NOT DEFINED(MAX_LABELS_PER_STRAND)
CONST_INT		MAX_LABELS_PER_STRAND	25
#ENDIF

CONST_FLOAT		COLLISION_LOADING_DISTANCE		150.0

STRUCT FLOW_LAUNCHER_VARS
	BOOL			bLauncherWidgetSetup = FALSE
	BOOL 			bLauncherRunMultipleLabelLaunch = FALSE
	INT 			iLauncherCurrentDiagramElementID = 0
	INT 			iLauncherTargetStrandLabels[MAX_STRANDS]
	JUMP_LABEL_IDS 	eLauncherStrandLabels[MAX_STRANDS][MAX_LABELS_PER_STRAND]
	TEXT_WIDGET_ID 	widgetLauncherActivationResult

	TEXT_LABEL_63	txtLauncherXMLPath				//	= "X:/gta5/build/dev_ng/common/data/script/xml/"
	TEXT_LABEL_63	txtLauncherDiagramXMLFile		//	= "debug/SP_flow_diagram.xml"
	TEXT_LABEL_63	txtLauncherPlaythroughXMLFile	//	= "SPPlaythrough/SP_playthrough_order.xml"
ENDSTRUCT
