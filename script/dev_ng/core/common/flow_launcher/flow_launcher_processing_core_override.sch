//╒═════════════════════════════════════════════════════════════════════════════╕
//│				Author:  Ben Rollinson					Date: 27/11/11			│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│				Flow Launcher Processing Core Overrideable Header				│
//│																				│
//│		DESCRIPTION: 	Procedures and functions for the Flow Launcher system	│
//│						that should be overriden by each game in				│
//│						flow_launcher_processing_GAME.sch.						│
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛

#IF IS_DEBUG_BUILD

USING "flow_launcher_processing.sch"



#IF NOT DEFINED(LAUNCHER_DISPLAY_SET_STATIC_BLIP_STATE_COMMAND_DEBUG_OUTPUT)
PROC LAUNCHER_DISPLAY_SET_STATIC_BLIP_STATE_COMMAND_DEBUG_OUTPUT(STRANDS paramStrand, FLOW_COMMANDS paramCommand)
	CDEBUG1LN(DEBUG_FLOW_LAUNCHER, GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(paramStrand), ": Performing '", GET_COMMAND_DISPLAY_STRING_FROM_COMMAND_ID(paramCommand.command), "(", ENUM_TO_INT(g_flowUnsaved.miscellaneousVars[paramCommand.index].blipID), ")'")
ENDPROC
#ENDIF

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞════════════╡ Overrideable Launcher Command Skipping Functions ╞═════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛ 

//PURPOSE:		A generic command used when processing any 'Do Mission' flow commands within the flow launcher.
//
//PARAM NOTES:	paramStrand - The strand in which the 'Do Mission' command is being skipped.
//				paramCommandIndex - The g_flowUnsaved.scriptVars[] array index at which the 'Do Mission' command is located.
//				paramGeneratePlaythroughOrder - Are we generating a playthrough order XML file containing an ordered list of mission names?mand?
//				paramGenerateFlowDiagram - Are we generating an XML diagram to be used by the flow_diagram system?
//
#IF NOT DEFINED(LAUNCHER_SKIP_DO_MISSION_COMMAND)
FUNC JUMP_LABEL_IDS LAUNCHER_SKIP_DO_MISSION_COMMAND(FLOW_LAUNCHER_VARS &flowLauncher, STRANDS paramStrand, INT paramCommandIndex, BOOL paramGeneratePlaythroughOrder = FALSE, BOOL paramGenerateFlowDiagram = FALSE)
	
	//Ensure the Script Vars Array position is legal.
	IF (paramCommandIndex = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW_LAUNCHER, "LAUNCHER_SKIP_DO_MISSION_COMMAND: The Script Variables Array Position parameter is illegal. Skipping to next command.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	//If we're generating a playthrough order, write the necessary xml to the specified debug file.
	IF paramGeneratePlaythroughOrder
		LAUNCHER_WRITE_STRING_TO_PLAYTHROUGH_XML_FILE	(flowLauncher, "   <missionItem file=\"")
		LAUNCHER_WRITE_STRING_TO_PLAYTHROUGH_XML_FILE	(flowLauncher, GET_SCRIPT_NAME_FROM_MISSION_ID(paramCommandIndex].iValue1))
		LAUNCHER_WRITE_STRING_TO_PLAYTHROUGH_XML_FILE	(flowLauncher, ".sc")
		LAUNCHER_WRITE_STRING_TO_PLAYTHROUGH_XML_FILE	(flowLauncher, "\"/>")
		LAUNCHER_WRITE_NEW_LINE_TO_PLAYTHROUGH_XML_FILE	(flowLauncher)
	ENDIF
	
	//If we're generating a flow diagram, write the necessary xml to the specified debug file.
	IF paramGenerateFlowDiagram
	
		LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, "   <mission		strand=\"")
		LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(paramStrand))
		LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, "\"")
		LAUNCHER_WRITE_STRING_TABS_TO_DIAGRAM_XML_FILE	(flowLauncher, 6, GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(paramStrand))
		LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, "name=\"")
		LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, GET_SCRIPT_NAME_FROM_MISSION_ID(g_flowUnsaved.coreVars[paramCommandIndex].iValue1))
		LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, "\"")
		LAUNCHER_WRITE_STRING_TO_DIAGRAM_XML_FILE		(flowLauncher, "/>")
		LAUNCHER_WRITE_NEW_LINE_TO_DIAGRAM_XML_FILE		(flowLauncher)
		
		flowLauncher.iLauncherCurrentDiagramElementID++
	ENDIF
	
	CERRORLN(DEBUG_FLOW_LAUNCHER, "This is the base LAUNCHER_SKIP_DO_MISSION_COMMAND flow launcher processing command. Please override this.")
	
	//Follow the debug jump for this DO_MISSION command.
	RETURN INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.coreVars[paramCommandIndex].iValue4) // Debug jump label.

ENDFUNC
#ENDIF


//PURPOSE:		A command used when processing any 'run code ID' flow commands within the flow launcher.
//
//PARAM NOTES:	paramCommandIndex - The g_flowUnsaved.phonecallVars[] array index at which the 'run code ID' command is located.
//
#IF NOT DEFINED(LAUNCHER_SKIP_RUN_CODE_ID)
FUNC JUMP_LABEL_IDS LAUNCHER_SKIP_RUN_CODE_ID(INT paramCommandIndex)

	//Ensure the Phonecall Vars Array position is legal.
	IF (paramCommandIndex = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW_LAUNCHER, "LAUNCHER_SKIP_RUN_CODE_ID: The Phonecall Variables Array Position parameter is illegal. Skipping to next command.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	//We're done. Step to the next command.
	RETURN NEXT_SEQUENTIAL_COMMAND

ENDFUNC
#ENDIF


//PURPOSE:		A command used when processing any 'debug run code ID' flow commands within the flow launcher.
//
//PARAM NOTES:	paramCommandIndex - The g_flowUnsaved.debugVars[] array index at which the 'debug run code ID' command is located.
//
#IF NOT DEFINED(LAUNCHER_SKIP_DEBUG_RUN_CODE_ID)
FUNC JUMP_LABEL_IDS LAUNCHER_SKIP_DEBUG_RUN_CODE_ID(INT paramCommandIndex)

	//Ensure the Debug Vars Array position is legal.
	IF (paramCommandIndex = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW_LAUNCHER, "LAUNCHER_SKIP_DEBUG_RUN_CODE_ID: The Debug Variables Array Position parameter is illegal. Skipping to next command.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	//We're done. Step to the next command.
	RETURN NEXT_SEQUENTIAL_COMMAND

ENDFUNC
#ENDIF


#IF NOT DEFINED(LAUNCHER_SKIP_ACTIVATE_RANDOMCHAR_MISSION)
FUNC JUMP_LABEL_IDS LAUNCHER_SKIP_ACTIVATE_RANDOMCHAR_MISSION(FLOW_LAUNCHER_VARS &flowLauncher, STRANDS paramStrand, INT paramCommandIndex)
	paramStrand = paramStrand
	flowLauncher.bLauncherRunMultipleLabelLaunch = flowLauncher.bLauncherRunMultipleLabelLaunch
	RETURN PERFORM_ACTIVATE_RANDOMCHAR_MISSION(paramCommandIndex)
ENDFUNC
#ENDIF


#IF NOT DEFINED(LAUNCHER_SKIP_DISPLAY_HELP_TEXT)
FUNC JUMP_LABEL_IDS LAUNCHER_SKIP_DISPLAY_HELP_TEXT(INT paramCommandIndex)
	paramCommandIndex = paramCommandIndex
	RETURN NEXT_SEQUENTIAL_COMMAND
ENDFUNC
#ENDIF




//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════╡ Overrideable Launcher Flow Control Functions ╞═══════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛ 

//PURPOSE: Sets the gameflow back to it's initial state.
//
#IF NOT DEFINED(LAUNCHER_RESET_FLOW)
PROC LAUNCHER_RESET_FLOW()

	CDEBUG1LN(DEBUG_FLOW_LAUNCHER, " Resetting the flow state.")
	
	CERRORLN(DEBUG_FLOW_LAUNCHER, "This is the base LAUNCHER_RESET_FLOW flow launcher processing command. Please override this.")

ENDPROC
#ENDIF


//PURPOSE: Sets the gameflow back to it's initial state and then triggers the opening strand.
//
#IF NOT DEFINED(LAUNCHER_RESTART_FLOW)
PROC LAUNCHER_RESTART_FLOW()

	CDEBUG1LN(DEBUG_FLOW_LAUNCHER, "Restarting the flow.")
	
	CERRORLN(DEBUG_FLOW_LAUNCHER, "This is the base LAUNCHER_RESTART_FLOW flow launcher processing command. Please override this.")

ENDPROC
#ENDIF


//PURPOSE: Returns a flow label that sits at the very end of the gameflow.
#IF NOT DEFINED(LAUNCHER_GET_FLOW_FINAL_LABEL)
FUNC JUMP_LABEL_IDS LAUNCHER_GET_FLOW_FINAL_LABEL()

	CERRORLN(DEBUG_FLOW_LAUNCHER, "This is the base LAUNCHER_GET_FLOW_FINAL_LABEL flow launcher processing command. Please override this.")

	RETURN STAY_ON_THIS_COMMAND
ENDFUNC
#ENDIF





#ENDIF
