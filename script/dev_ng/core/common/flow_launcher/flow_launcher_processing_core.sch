//╒═════════════════════════════════════════════════════════════════════════════╕
//│				Author:  Ben Rollinson					Date: 27/11/11			│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│						Flow Launcher Processing Core Header					│
//│																				│
//│		DESCRIPTION: 	Procedures and functions for the Flow Launcher			│
//│						core command set.										│
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛

#IF IS_DEBUG_BUILD

USING "flow_launcher_processing_GAME.sch"
USING "flow_launcher_processing_core_override.sch"
//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════╡ Launcher Debug Display Functions ╞════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

//PURPOSE:		Display a debug line for a generic command that is being processed by the flow launcher.
//
//PARAM NOTES:	paramStrand - The strand in which the command is found.
//				paramCommand - The flow command ID of the command being processed.
//				paramSkipping - A flag to saw whether or not the command is being skipped over, or processed fully.
//
PROC LAUNCHER_DISPLAY_GENERIC_COMMAND_DEBUG_OUTPUT(STRANDS paramStrand, FLOW_COMMAND_IDS paramCommand, BOOL paramSkipping)
	CDEBUG1LN(DEBUG_FLOW_LAUNCHER, GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(paramStrand), PICK_STRING( paramSkipping, ": Skipping '",": Performing '"), GET_COMMAND_DISPLAY_STRING_FROM_COMMAND_ID(paramCommand), "'")
ENDPROC


//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════╡ Command Specific Skipping Functions ╞═══════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

//PURPOSE:		A command used when processing any 'Do Flag Jumps' flow commands within the flow launcher.
//
//PARAM NOTES:	paramCommandIndex - The g_flowUnsaved.coreVars[] array index at which the 'Do Flag Jumps' command is located.
//
FUNC JUMP_LABEL_IDS LAUNCHER_SKIP_DO_FLAG_JUMPS_COMMAND(INT paramCommandIndex)

	//Ensure the Miscellaneous Vars Array position is legal.
	IF (paramCommandIndex = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW_LAUNCHER, "LAUNCHER_SKIP_DO_FLAG_JUMPS_COMMAND: The Core Variables Array Position parameter is illegal. Skipping to next command.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	JUMP_LABEL_IDS eDebugJump = INT_TO_ENUM(JUMP_LABEL_IDS, g_flowUnsaved.coreVars[paramCommandIndex].iValue4)
	
	IF eDebugJump = STAY_ON_THIS_COMMAND
		//No debug jump defined. Process the main flow command.
		RETURN PERFORM_DO_FLAG_JUMPS(paramCommandIndex)
	ELSE
		//Debug jump defined. Return the debug jump label.
		RETURN eDebugJump
	ENDIF

ENDFUNC


//PURPOSE:		A command used when processing any 'wait until game hour' flow commands within the flow launcher.
//
//PARAM NOTES:	paramCommandIndex - The index at which the 'Wait Until Game Hour' command is stored. The required value is stored directly in this index.
//
FUNC JUMP_LABEL_IDS LAUNCHER_SKIP_WAIT_UNTIL_GAME_HOUR_COMMAND(INT paramCommandIndex)

	//Get the hour we want to skip to. 
	INT iHour = paramCommandIndex //NB. Direct index storage.

	//If the current hour on the clock is after the specified hour then jump forward one day.
	IF GET_CLOCK_HOURS() > iHour
		SET_CLOCK_DATE(GET_CLOCK_DAY_OF_MONTH()+1, GET_CLOCK_MONTH(), GET_CLOCK_YEAR())
	ENDIF
	//If we're not already in the right hour set the clock to the required hour.
	IF GET_CLOCK_HOURS() <> iHour
		SET_CLOCK_TIME(iHour, 0, 0)
	ENDIF
	
	//Now step to the next command.
	RETURN NEXT_SEQUENTIAL_COMMAND
ENDFUNC


//PURPOSE:		A command used when processing any 'Wait Until Game Time and Date' flow commands within the flow launcher.
//
//PARAM NOTES:	paramCommandIndex - The g_flowUnsaved.coreVars[] array index at which the 'Wait Until Game Time and Date' command is located.
//
FUNC JUMP_LABEL_IDS LAUNCHER_SKIP_WAIT_UNTIL_GAME_TIME_AND_DATE_COMMAND(INT paramCommandIndex)

	//Ensure the Core Vars Array position is legal.
	IF (paramCommandIndex = ILLEGAL_ARRAY_POSITION)
		CERRORLN(DEBUG_FLOW_LAUNCHER, "LAUNCHER_SKIP_WAIT_UNTIL_GAME_TIME_AND_DATE_COMMAND: The Core Variables Array Position parameter is illegal. Skipping to next command.")
		RETURN NEXT_SEQUENTIAL_COMMAND
	ENDIF
	
	//Reconstruct the TimeOfDay we want to skip to using the two stored int values.
	TIMEOFDAY sSkipToTimeOfDay		= INT_TO_ENUM(TIMEOFDAY, paramCommandIndex)
//	sSkipToTimeOfDay.iStoredTime	= g_flowUnsaved.coreVars[paramCommandIndex].iValue1
//	sSkipToTimeOfDay.iYears 		= g_flowUnsaved.coreVars[paramCommandIndex].iValue2

	//Set the game clock date to the requested date.
	SET_CLOCK_DATE(	GET_TIMEOFDAY_DAY(sSkipToTimeOfDay), 
					INT_TO_ENUM(MONTH_OF_YEAR, GET_TIMEOFDAY_MONTH(sSkipToTimeOfDay)), 
					GET_TIMEOFDAY_YEAR(sSkipToTimeOfDay))
	//Set the game clock time to the requested time.
	SET_CLOCK_TIME(	GET_TIMEOFDAY_HOUR(sSkipToTimeOfDay), 
					GET_TIMEOFDAY_MINUTE(sSkipToTimeOfDay), 
					GET_TIMEOFDAY_SECOND(sSkipToTimeOfDay))
	
	//Now step to the next command.
	RETURN NEXT_SEQUENTIAL_COMMAND

ENDFUNC

#ENDIF

