//╒═════════════════════════════════════════════════════════════════════════════╕
//│					  			Clock Public Header								│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│		AUTHORS:		Ben Rollinson											│
//│		PREVIOUS:		Ryan Paradis											│
//│		DATE:			05/11/10												│
//│		DESCRIPTION: 	Public script functions related to times, dates and 	│
//│						the in game clock.										│									
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛

USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_clock.sch"
USING "commands_script.sch"
USING "commands_stats.sch"
USING "script_maths.sch"
USING "debug_channels_structs.sch"

CONST_INT TOD_MONTH_BITMASK		BIT0  | BIT1  | BIT2  | BIT3 					//15			// First 4 bits, gives us 16 values for month
CONST_INT TOD_DAY_BITMASK		BIT4  | BIT5  | BIT6  | BIT7  | BIT8 			//496			// Bits 5-9, gives us 32 values for day.
CONST_INT TOD_HOUR_BITMASK		BIT9  | BIT10 | BIT11 | BIT12 | BIT13 			// 15872		// Bits 10-14, gives us 32 values for hour.
CONST_INT TOD_MINUTE_BITMASK	BIT14 | BIT15 | BIT16 | BIT17 | BIT18 | BIT19 	// 1032193		// Bits 15-20, gives us 64 values for minute.
CONST_INT TOD_SECOND_BITMASK	BIT20 | BIT21 | BIT22 | BIT23 | BIT24 | BIT25	// 66060288		// Bits 21-26, gives us 64 values for second.
CONST_INT TOD_YEAR_BITMASK		BIT26 | BIT27 | BIT28 | BIT29 | BIT30							// Bits 27-31, gives us 32 values for year. We would need a project specific offset, g_iYearOffset
CONST_INT TOD_YEAR_SIGN_BITMASK (BIT30*2)	// If this bit is set, the year is negative (below the g_iYearOffset)

CONST_INT TOD_DAY_SHIFT		4
CONST_INT TOD_HOUR_SHIFT	9
CONST_INT TOD_MINUTE_SHIFT	14
CONST_INT TOD_SECOND_SHIFT	20
CONST_INT TOD_YEAR_SHIFT	26
CONST_INT TOD_YEAR_SIGN_SHIFT	31

ENUM TIMECOMP
	TC_LATER_THAN = 1,
	TC_SAME,
	TC_EARLIER_THAN
ENDENUM


// PURPOSE: Clears a timeofday struct.
//
// RETURN PARAM:	paramTimeOfDay		The TIMEOFDAY struct being cleared
//
// NOTES:			Setting both INT parts of the struct to 0. This is fine because for a date the 'day' part
//						needs to be between 1 and 31 so a value of 0 for the storedTime creates an illegal
//						date/time combo.
PROC CLEAR_TIMEOFDAY(TIMEOFDAY &paramTimeOfDay)
	paramTimeOfDay = INVALID_TIMEOFDAY
ENDPROC

///    Takes an int and returns a MONTH_OF_YEAR
/// PARAMS:
///    Month - 1- 12 params
/// RETURNS:
///    
FUNC STRING GET_MONTH_STRING_FROM_MONTH_OF_YEAR(MONTH_OF_YEAR Month)

	SWITCH Month
		CASE JANUARY	RETURN "JANUARY"					BREAK	
		CASE FEBRUARY	RETURN "FEBRUARY"					BREAK	
		CASE MARCH		RETURN "MARCH"				BREAK	
		CASE APRIL		RETURN "APRIL"				BREAK	
		CASE MAY		RETURN "MAY"					BREAK	
		CASE JUNE		RETURN "JUNE"					BREAK	
		CASE JULY		RETURN "JULY"					BREAK	
		CASE AUGUST		RETURN "AUGUST"				BREAK	
		CASE SEPTEMBER	RETURN "SEPTEMBER"				BREAK	
		CASE OCTOBER	RETURN "OCTOBER"					BREAK	
		CASE NOVEMBER	RETURN "NOVEMBER"					BREAK	
		CASE DECEMBER	RETURN "DECEMBER"					BREAK	
	
	ENDSWITCH
	RETURN "NONE"

ENDFUNC


/// PURPOSE:	Calculates the number of days in a given month. Takes into account leap years.
///    
/// INPUT PARAMS:	eMonth		The month as a MONTH_OF_YEAR enum.
///					iYear		The year the month is in.
///    
/// RETURNS:	 The number of days in the specified month.
FUNC INT GET_NUMBER_OF_DAYS_IN_MONTH(MONTH_OF_YEAR eMonth, INT iYear)

	IF iYear < 0
		CASSERTLN(DEBUG_SYSTEM, "GET_NUMBER_OF_DAYS_IN_MONTH: Invalid year [", iYear, "] passed. iYear < 0. Using default year 0.")
		iYear = 0
	ENDIF

	SWITCH eMonth
		// 31 day months
		CASE JANUARY
		CASE MARCH
		CASE MAY
		CASE JULY
		CASE AUGUST
		CASE OCTOBER
		CASE DECEMBER
			RETURN 31
		BREAK
			
		// 30 day months
		CASE APRIL
		CASE JUNE
		CASE SEPTEMBER
		CASE NOVEMBER
			RETURN 30
		BREAK	
		
		// 28/29 day months
		CASE FEBRUARY
			// Is this a leap year?
			// Year exactly divides by 4...
			IF iYear % 4 = 0  // This can be made more efficient by checking iYear & (BIT0 | BIT1) = 0
				// ...and doesn't exactly divide by 100. Leap year.
				IF iYear % 100 != 0
					RETURN 29
				// ...and divides exactly by 100 and 400. Leap year.
				ELIF iYear % 400 = 0
					RETURN 29
				ENDIF
			ENDIF
			
			// Not a leap year.
			RETURN 28
		BREAK
	ENDSWITCH
	
	// Should never reach here.
	CASSERTLN(DEBUG_SYSTEM, "GET_NUMBER_OF_DAYS_IN_MONTH: Invalid month passed. Returning default 30 days.")
	RETURN 30
ENDFUNC


/// RETURNS:
///    The year stored in that TIMEOFDAY
FUNC INT GET_TIMEOFDAY_YEAR(TIMEOFDAY sTimeOfDay)
	RETURN ((SHIFT_RIGHT(ENUM_TO_INT(sTimeOfDay), TOD_YEAR_SHIFT) & (BIT0|BIT1|BIT2|BIT3|BIT4)) * PICK_INT(IS_BIT_SET(ENUM_TO_INT(sTimeOfDay), TOD_YEAR_SIGN_SHIFT), -1, 1)) + TIMEOFDAY_YEAR_OFFSET
ENDFUNC


/// RETURNS:
///    The month that the passed in TOD has. 
///    NOTE: TIMEOFDAY IS 0-11 (Jen-Dec), JUST LIKE MONTH_OF_YEAR!
FUNC MONTH_OF_YEAR GET_TIMEOFDAY_MONTH(TIMEOFDAY sTimeOfDay)
	// Don't need to shift. Month is stored in the first 4 bits.
	RETURN INT_TO_ENUM(MONTH_OF_YEAR, (ENUM_TO_INT(sTimeOfDay) & TOD_MONTH_BITMASK))
ENDFUNC


/// RETURNS:
///    The day stored in that TIMEOFDAY
FUNC INT GET_TIMEOFDAY_DAY(TIMEOFDAY sTimeOfDay)
	// Shift it back right, and get the first 5 bits.
	RETURN SHIFT_RIGHT(ENUM_TO_INT(sTimeOfDay), TOD_DAY_SHIFT) & (BIT0|BIT1|BIT2|BIT3|BIT4)
ENDFUNC


/// RETURNS:
///    The hour stored in that TIMEOFDAY
FUNC INT GET_TIMEOFDAY_HOUR(TIMEOFDAY sTimeOfDay)
	// Shift it back right, and get the first 5 bits.
	RETURN SHIFT_RIGHT(ENUM_TO_INT(sTimeOfDay), TOD_HOUR_SHIFT) & (BIT0|BIT1|BIT2|BIT3|BIT4)
ENDFUNC


/// RETURNS:
///    The minute stored in that TIMEOFDAY
FUNC INT GET_TIMEOFDAY_MINUTE(TIMEOFDAY sTimeOfDay)
	// Shift it back right, and get the first 6 bits.
	RETURN SHIFT_RIGHT(ENUM_TO_INT(sTimeOfDay), TOD_MINUTE_SHIFT) & (BIT0|BIT1|BIT2|BIT3|BIT4|BIT5)
ENDFUNC


/// RETURNS:
///    The second stored in that TIMEOFDAY
FUNC INT GET_TIMEOFDAY_SECOND(TIMEOFDAY sTimeOfDay)
	// Shift it back right, and get the first 6 bits.
	RETURN SHIFT_RIGHT(ENUM_TO_INT(sTimeOfDay), TOD_SECOND_SHIFT) & (BIT0|BIT1|BIT2|BIT3|BIT4|BIT5)
ENDFUNC





/// PURPOSE:
///    Sets the second value of the passed in time of day.
PROC SET_TIMEOFDAY_SECOND(TIMEOFDAY &sTimeOfDay, INT iSecond)

	// Check the second is valid.
	IF (iSecond < 0) OR (iSecond >= 60)
		CASSERTLN(DEBUG_SYSTEM, "SET_TIMEOFDAY_SECOND: Passed \"", iSecond, "\", a < 0 or > 60 sec which is invalid. Failed to set TIMEOFDAY.")
		EXIT
	ENDIF
	sTimeOfDay -= sTimeOfDay & INT_TO_ENUM(TIMEOFDAY, TOD_SECOND_BITMASK)
	sTimeOfDay |= INT_TO_ENUM(TIMEOFDAY, SHIFT_LEFT(iSecond, TOD_SECOND_SHIFT))
ENDPROC


/// PURPOSE:
///    Sets the minute value of the passed in time of day.
PROC SET_TIMEOFDAY_MINUTE(TIMEOFDAY &sTimeOfDay, INT iMinute)

	// Check the minute is valid. 
	IF (iMinute < 0) OR (iMinute >= 60)
		CASSERTLN(DEBUG_SYSTEM, "SET_TIMEOFDAY_MINUTE: Passed \"", iMinute, "\", a < 0 or > 60 min which is invalid. Failed to set TIMEOFDAY.")
		EXIT
	ENDIF
	
	sTimeOfDay -= sTimeOfDay & INT_TO_ENUM(TIMEOFDAY,TOD_MINUTE_BITMASK )
	sTimeOfDay |= INT_TO_ENUM(TIMEOFDAY, SHIFT_LEFT(iMinute, TOD_MINUTE_SHIFT))
ENDPROC


/// PURPOSE:
///    Sets the hour value of the passed in time of day.
PROC SET_TIMEOFDAY_HOUR(TIMEOFDAY &sTimeOfDay, INT iHour)

	// Check the hour is valid.
	IF (iHour < 0) OR (iHour > 24)
		PRINTLN("PRINT: SET_TIMEOFDAY_HOUR: Passed \"", iHour, "\", a < 0 or > 24 hour which is invalid. Failed to set TIMEOFDAY.")
		CASSERTLN(DEBUG_SYSTEM, "SET_TIMEOFDAY_HOUR: Passed \"", iHour, "\", a < 0 or > 24 hour which is invalid. Failed to set TIMEOFDAY.")
		EXIT
	ENDIF
	
	sTimeOfDay -= sTimeOfDay & INT_TO_ENUM(TIMEOFDAY,TOD_HOUR_BITMASK) 
	sTimeOfDay |= INT_TO_ENUM(TIMEOFDAY, SHIFT_LEFT(iHour, TOD_HOUR_SHIFT))
ENDPROC


/// PURPOSE:
///    Sets the day value of the passed in time of day.
PROC SET_TIMEOFDAY_DAY(TIMEOFDAY &sTimeOfDay, INT iDay)

	//Get the current month stored in this TIMEOFDAY struct.
	MONTH_OF_YEAR eMonth = GET_TIMEOFDAY_MONTH(sTimeOfDay)
	INT iYear = GET_TIMEOFDAY_YEAR(sTimeOfDay)

	// Check the day is valid.
	IF (iDay < 1) OR (iDay > GET_NUMBER_OF_DAYS_IN_MONTH(eMonth, iYear))
		
		CASSERTLN(DEBUG_SYSTEM, "SET_TIMEOFDAY_DAY: Passed an invalid day [", iDay, "] for the TIMEOFDAY's current month [", eMonth, "]. Failed to set TIMEOFDAY.")
		EXIT
	ENDIF
	
	sTimeOfDay -= sTimeOfDay & INT_TO_ENUM(TIMEOFDAY, TOD_DAY_BITMASK )
	sTimeOfDay |= INT_TO_ENUM(TIMEOFDAY, SHIFT_LEFT(iDay, TOD_DAY_SHIFT))
ENDPROC


/// PURPOSE:
///    Sets the month value of the passed in time of day.
PROC SET_TIMEOFDAY_MONTH(TIMEOFDAY &sTimeOfDay, MONTH_OF_YEAR eMonth)

	// Check the month is valid.
	IF (eMonth < JANUARY) OR (eMonth > DECEMBER)
		CASSERTLN(DEBUG_SYSTEM, "SET_TIMEOFDAY_MONTH: Given \"", eMonth, "\" val < 0 or > 11 which is invalid. Failed to set TIMEOFDAY.")
		EXIT
	ENDIF
	
	//Retrieve current TIMEOFDAY values.
	sTimeOfDay -= sTimeOfDay & INT_TO_ENUM(TIMEOFDAY, TOD_MONTH_BITMASK )
	sTimeOfDay |= INT_TO_ENUM(TIMEOFDAY, ENUM_TO_INT(eMonth))	// In the enum, January is 0, Dec is 11.
ENDPROC


/// PURPOSE:
///    Sets the year value of the passed in time of day.
PROC SET_TIMEOFDAY_YEAR(TIMEOFDAY &sTimeOfDay, INT iYear)

	// Check the year is valid.
	IF (iYear <= 0)
		DEBUG_PRINTCALLSTACK()
		CASSERTLN(DEBUG_SYSTEM, "SET_TIMEOFDAY_YEAR: Passed \"", iYear, "\", a <= 0 year which is invalid. Failed to set TIMEOFDAY.")
		EXIT
	ENDIF
	IF iYear > TIMEOFDAY_YEAR_OFFSET + 32
	OR iYear < TIMEOFDAY_YEAR_OFFSET - 32
		// Too big to embed
		CASSERTLN(DEBUG_SYSTEM, "SET_TIMEOFDAY_YEAR: Passed a year (",iYear,") which is bigger than g_iYearOffset +- 32. Failed to set TIMEOFDAY.")
		EXIT
	ENDIF
	sTimeOfDay -= sTimeOfDay & INT_TO_ENUM(TIMEOFDAY, TOD_YEAR_BITMASK) 
	
	IF iYear < TIMEOFDAY_YEAR_OFFSET
	// The year embedded is negative
		// Set the TIMEOFDAY year value.
		sTimeOfDay |= INT_TO_ENUM(TIMEOFDAY, SHIFT_LEFT(TIMEOFDAY_YEAR_OFFSET - iYear, TOD_YEAR_SHIFT))
		sTimeOfDay |= INT_TO_ENUM(TIMEOFDAY, TOD_YEAR_SIGN_BITMASK)
	ELSE
		// Set the TIMEOFDAY year value.
		sTimeOfDay |= INT_TO_ENUM(TIMEOFDAY, SHIFT_LEFT(iYear - TIMEOFDAY_YEAR_OFFSET, TOD_YEAR_SHIFT)) 
		sTimeOfDay -= sTimeOfDay & INT_TO_ENUM(TIMEOFDAY, TOD_YEAR_SIGN_BITMASK) 
	ENDIF
	
ENDPROC


/// RETURNS:
///    Returns the current Time of Day.
FUNC TIMEOFDAY GET_CURRENT_TIMEOFDAY()
	TIMEOFDAY sRetVal
	SET_TIMEOFDAY_SECOND(sRetVal, GET_CLOCK_SECONDS())
	SET_TIMEOFDAY_MINUTE(sRetVal, GET_CLOCK_MINUTES())
	SET_TIMEOFDAY_HOUR(sRetVal, GET_CLOCK_HOURS())
	SET_TIMEOFDAY_DAY(sRetVal, GET_CLOCK_DAY_OF_MONTH())
	SET_TIMEOFDAY_MONTH(sRetVal, GET_CLOCK_MONTH())
	SET_TIMEOFDAY_YEAR(sRetVal, GET_CLOCK_YEAR())
	
	RETURN sRetVal
ENDFUNC



#IF IS_DEBUG_BUILD

/// PURPOSE:
///    Creates a TEXT_LABEL_63 representation of a TIMEOFDAY data type.
FUNC TEXT_LABEL_63 TIMEOFDAY_TO_TEXT_LABEL(TIMEOFDAY sTimeOfDay)

	TEXT_LABEL_63 tDateTime = ""
	INT iTempInt = GET_TIMEOFDAY_HOUR(sTimeOfDay)
	IF iTempInt < 10
		tDateTime += 0
	ENDIF
	tDateTime += iTempInt
	tDateTime += ":"
	
	iTempInt = GET_TIMEOFDAY_MINUTE(sTimeOfDay)
	IF iTempInt < 10
		tDateTime += 0
	ENDIF
	tDateTime += iTempInt
	tDateTime += ":"
	
	iTempInt = GET_TIMEOFDAY_SECOND(sTimeOfDay)
	IF iTempInt < 10
		tDateTime += 0
	ENDIF
	tDateTime += iTempInt
	tDateTime += "  "
	
	iTempInt = GET_TIMEOFDAY_DAY(sTimeOfDay)
	IF iTempInt < 10
		tDateTime += 0
	ENDIF
	tDateTime += iTempInt
	tDateTime += "/"
	
	iTempInt = ENUM_TO_INT(GET_TIMEOFDAY_MONTH(sTimeOfDay))
	IF iTempInt < 9
		tDateTime += 0
	ENDIF
	tDateTime += iTempInt + 1
	tDateTime += "/"
	tDateTime += GET_TIMEOFDAY_YEAR(sTimeOfDay)
	
	RETURN tDateTime
ENDFUNC


/// PURPOSE:
///    Prints the time and date stored in the TIMEOFDAY.
PROC PRINT_TIMEOFDAY(TIMEOFDAY sTimeOfDay, DEBUG_CHANNELS eChannel = DEBUG_SYSTEM)
	TEXT_LABEL_63 txtTimeOfDay = TIMEOFDAY_TO_TEXT_LABEL(sTimeOfDay)
	CPRINTLN(eChannel, txtTimeOfDay)
ENDPROC


/// PURPOSE:
///    Writes the time and date stored in the TIMEOFDAY to temp_debug.txt
PROC SAVE_TIMEOFDAY_TO_DEBUG_FILE(TIMEOFDAY sTimeOfDay)
	TEXT_LABEL_63 txtTimeOfDay = TIMEOFDAY_TO_TEXT_LABEL(sTimeOfDay)
	SAVE_STRING_TO_DEBUG_FILE(txtTimeOfDay)
ENDPROC


/// PURPOSE:
///    Saves the time and date stored in the TIMEOFDAY to the given file at the given path.
PROC SAVE_TIMEOFDAY_TO_NAMED_DEBUG_FILE(TIMEOFDAY sTimeOfDay, string FilePath, string FileName)
	TEXT_LABEL_63 txtTimeOfDay = TIMEOFDAY_TO_TEXT_LABEL(sTimeOfDay)
	SAVE_STRING_TO_NAMED_DEBUG_FILE(txtTimeOfDay, FilePath, FileName)
ENDPROC


/// PURPOSE:
///    Prints the time and date stored in the TIMEOFDAY.
PROC GET_TIMEOFDAY_STRING(TIMEOFDAY sTimeOfDay, TEXT_LABEL_63 &tl63Passed, BOOL bShortString = FALSE, BOOL bIncludeMonthAndYear = FALSE)
	INT iYear	= GET_TIMEOFDAY_YEAR(sTimeOfDay)
	MONTH_OF_YEAR iMonth	= GET_TIMEOFDAY_MONTH(sTimeOfDay)
	INT iDay	= GET_TIMEOFDAY_DAY(sTimeOfDay)
	INT iHour	= GET_TIMEOFDAY_HOUR(sTimeOfDay)
	INT iMinute = GET_TIMEOFDAY_MINUTE(sTimeOfDay)
	INT iSecond = GET_TIMEOFDAY_SECOND(sTimeOfDay)
	
	
	// Print the time. Put in zeros where needed.
	TEXT_LABEL_63 timeString = ""
	
	IF bIncludeMonthAndYear
	
		timeString += iYear
		timeString += ":"
		timeString += GET_MONTH_STRING_FROM_MONTH_OF_YEAR(iMonth)
		timeString += ":"
		timeString += iDay
		timeString += ":"
	
	ENDIF
	
	IF (iHour < 10)
		timeString += "0"
	ENDIF
	timeString += iHour
	timeString += ":"
	
	IF (iMinute < 10)
		timeString += "0"
	ENDIF
	timeString += iMinute
	
	IF NOT (bShortString)
		timeString += ":"
		
		IF (iSecond < 10)
			timeString += "0"
		ENDIF
		timeString += iSecond
	ENDIF
	
	
	// Print the date:
	TIMEOFDAY CurrentTimeOfDay = GET_CURRENT_TIMEOFDAY()
	INT iDelayInDays = GET_TIMEOFDAY_DAY(CurrentTimeOfDay)
	TEXT_LABEL_31 dateString
	iDay -= iDelayInDays
	IF iDay = 0
		dateString += " Today"
	ELIF iDay = 1
		dateString += " tomorrow"
	ELSE
		dateString = " in "
		dateString += iDay
		dateString = " days"
	ENDIF
	
	// KGM 27/4/12: Shortened String Output
	IF (bShortString)
		tl63Passed = "("
		tl63Passed += iDay
		tl63Passed += ") "
		tl63Passed += timeString
		
		EXIT
	ENDIF
	
	timeString += dateString 
	tl63Passed = "Wait Until "
	tl63Passed += timeString // This could be better!
ENDPROC

#ENDIF


/// RETURNS:
///    Returns the current Time of Day.
FUNC TIMEOFDAY GET_CURRENT_TIMEOFDAY_POSIX()
	TIMEOFDAY sRetVal
	STRUCT_STAT_DATE aDate
	GET_POSIX_TIME(aDate.Year, aDate.Month, aDate.Day, aDate.Hour, aDate.Minute, aDate.Seconds)
	SET_TIMEOFDAY_SECOND(sRetVal,  aDate.Seconds)
	SET_TIMEOFDAY_MINUTE(sRetVal, aDate.Minute)
	SET_TIMEOFDAY_HOUR(sRetVal, aDate.Hour)
	SET_TIMEOFDAY_DAY(sRetVal, aDate.Day)
	SET_TIMEOFDAY_MONTH(sRetVal, INT_TO_ENUM(MONTH_OF_YEAR, (aDate.Month-1)))
	SET_TIMEOFDAY_YEAR(sRetVal, aDate.Year)
	
	RETURN sRetVal
ENDFUNC


/// PURPOSE:
///    Completely configures all values of a time of day struct.
PROC SET_TIMEOFDAY(TIMEOFDAY &sTimeOfDay, INT iSecond, INT iMinute, INT iHour, INT iDay, MONTH_OF_YEAR eMonth, INT iYear)
	SET_TIMEOFDAY_SECOND(sTimeOfDay, iSecond)
	SET_TIMEOFDAY_MINUTE(sTimeOfDay, iMinute)
	SET_TIMEOFDAY_HOUR(sTimeOfDay, iHour)
	SET_TIMEOFDAY_MONTH(sTimeOfDay, eMonth)
	SET_TIMEOFDAY_DAY(sTimeOfDay, iDay)
	SET_TIMEOFDAY_YEAR(sTimeOfDay, iYear)
ENDPROC


/// PURPOSE:
///    Pass in a TIMEOFDAY, and the function will add the (also passed in) seconds, minutes, hours, days, months and years to it that have been specified.
///    Returned via the referenced TIMEOFDAY passed in.
PROC ADD_TIME_TO_TIMEOFDAY(TIMEOFDAY & sTimeOfDay, INT iAddSeconds = 0,INT iAddMinutes = 0, INT iAddHours = 0, INT iAddDays = 0, INT iAddMonths = 0, INT iAddYears = 0)
	// Get what the current is, and hold that, we'll add to that.
	INT iYear	= GET_TIMEOFDAY_YEAR(sTimeOfDay)
	MONTH_OF_YEAR eMonth 	= GET_TIMEOFDAY_MONTH(sTimeOfDay)
	INT iDay	= GET_TIMEOFDAY_DAY(sTimeOfDay)
	INT iHour	= GET_TIMEOFDAY_HOUR(sTimeOfDay)
	INT iMinute = GET_TIMEOFDAY_MINUTE(sTimeOfDay)
	INT iSecond = GET_TIMEOFDAY_SECOND(sTimeOfDay)
	
	// If everything passed in is empty, exit early (no need to assert).
	IF (iAddYears = 0) AND (iAddMonths = 0) AND (iAddDays = 0) AND (iAddHours = 0) AND (iAddMinutes = 0) AND (iAddSeconds = 0)
		EXIT
	ENDIF
	
	// Don't allow negative arguments for now. They add a number of complications.
	IF iAddSeconds < 0
		SCRIPT_ASSERT("ADD_TIME_TO_TIMEOFDAY: Invalid arguement. Negative iAddSeconds passed.")
		EXIT
	ENDIF
	IF iAddMinutes < 0
		SCRIPT_ASSERT("ADD_TIME_TO_TIMEOFDAY: Invalid arguement. Negative iAddMinutes passed.")
		EXIT
	ENDIF
	IF iAddHours < 0
		SCRIPT_ASSERT("ADD_TIME_TO_TIMEOFDAY: Invalid arguement. Negative iAddHours passed.")
		EXIT
	ENDIF
	IF iAddDays < 0
		SCRIPT_ASSERT("ADD_TIME_TO_TIMEOFDAY: Invalid arguement. Negative iAddDays passed.")
		EXIT
	ENDIF
	IF iAddMonths < 0
		SCRIPT_ASSERT("ADD_TIME_TO_TIMEOFDAY: Invalid arguement. Negative iAddMonths passed.")
		EXIT
	ENDIF
	IF iAddYears < 0
		SCRIPT_ASSERT("ADD_TIME_TO_TIMEOFDAY: Invalid arguement. Negative iAddYears passed.")
		EXIT
	ENDIF
	
	// Add seconds.
	iSecond += iAddSeconds
	
	// If we're being set to over 60 seconds, add a minute.
	WHILE iSecond >= 60
		iAddMinutes += 1
		iSecond -= 60
	ENDWHILE
	
	
	// Add minutes.
	iMinute += iAddMinutes
		
	// If we're being set to over 60 minutes, add an hour.
	WHILE iMinute >= 60
		iAddHours += 1
		iMinute -= 60
	ENDWHILE
	
	
	// Add hours.
	iHour += iAddHours
	
	// If we're over 24 hours, add a day.
	WHILE iHour >= 24
		iAddDays += 1
		iHour -= 24
	ENDWHILE
	
	
	// Add days.
	iDay += iAddDays
		
	// Get number of days in current month.
	INT iDaysInMonth = GET_NUMBER_OF_DAYS_IN_MONTH(eMonth, iYear)
	// If we've run out of days in this month, add a month.
	WHILE iDay > iDaysInMonth
		eMonth += INT_TO_ENUM(MONTH_OF_YEAR,1)
		iDay -= iDaysInMonth
		
		//Check if we've stepped forward a year before recalulating days in new month.
		IF eMonth > DECEMBER
			iYear += 1
			eMonth -= INT_TO_ENUM( MONTH_OF_YEAR, 12)
		ENDIF
		
		//Get number of days in new month.
		iDaysInMonth = GET_NUMBER_OF_DAYS_IN_MONTH(eMonth, iYear)
	ENDWHILE
	

	// Add months.
	eMonth += INT_TO_ENUM( MONTH_OF_YEAR, iAddMonths) 
	
	// If we're over 12 months, add a year.
	WHILE (eMonth > DECEMBER)
		iAddYears += 1
		eMonth -= INT_TO_ENUM( MONTH_OF_YEAR, 12)
	ENDWHILE
	
	
	// Add years.
	iYear += iAddYears
	
	SET_TIMEOFDAY(sTimeOfDay, iSecond, iMinute, iHour, iDay, eMonth, iYear)
ENDPROC


/// PURPOSE:
///    Pass in a TIMEOFDAY, and the function will add the (also passed in) seconds, minutes, hours, days, months and years to it that have been specified.
///    Returned via the referenced TIMEOFDAY passed in.
PROC SUBTRACT_TIME_FROM_TIMEOFDAY(TIMEOFDAY & sTimeOfDay, INT iRemoveSeconds = 0,INT iRemoveMinutes = 0, INT iRemoveHours = 0, INT iRemoveDays = 0, INT iRemoveMonths = 0, INT iRemoveYears = 0)
	// Get what the current is, and hold that, we'll add to that.
	INT iYear	= GET_TIMEOFDAY_YEAR(sTimeOfDay)
	INT iMonth 	= ENUM_TO_INT(GET_TIMEOFDAY_MONTH(sTimeOfDay))
	INT iDay	= GET_TIMEOFDAY_DAY(sTimeOfDay)
	INT iHour	= GET_TIMEOFDAY_HOUR(sTimeOfDay)
	INT iMinute = GET_TIMEOFDAY_MINUTE(sTimeOfDay)
	INT iSecond = GET_TIMEOFDAY_SECOND(sTimeOfDay)
	
	// If everything passed in is empty, exit early (no need to assert).
	IF (iRemoveDays = 0) AND (iRemoveHours = 0) AND (iRemoveMinutes = 0) AND (iRemoveSeconds = 0)
		EXIT
	ENDIF
	
	// Don't allow negative arguments for now. They add a number of complications.
	IF iRemoveSeconds < 0
		SCRIPT_ASSERT("SUBTRACT_TIME_FROM_TIMEOFDAY: Invalid arguement. Negative iRemoveSeconds passed.")
		EXIT
	ENDIF
	IF iRemoveMinutes < 0
		SCRIPT_ASSERT("SUBTRACT_TIME_FROM_TIMEOFDAY: Invalid arguement. Negative iRemoveMinutes passed.")
		EXIT
	ENDIF
	IF iRemoveHours < 0
		SCRIPT_ASSERT("SUBTRACT_TIME_FROM_TIMEOFDAY: Invalid arguement. Negative iRemoveHours passed.")
		EXIT
	ENDIF
	IF iRemoveDays < 0
		SCRIPT_ASSERT("SUBTRACT_TIME_FROM_TIMEOFDAY: Invalid arguement. Negative iRemoveDays passed.")
		EXIT
	ENDIF
	
	// Remove seconds.
	iSecond -= iRemoveSeconds
	
	// If we're being set to under 0 seconds, remove a minute.
	WHILE iSecond < 0
		iRemoveMinutes += 1
		iSecond += 60
	ENDWHILE
	
	
	// Remove minutes.
	iMinute -= iRemoveMinutes
		
	// If we're being set to under 0 minutes, remove an hour.
	WHILE iMinute < 0
		iRemoveHours += 1
		iMinute += 60
	ENDWHILE
	
	
	// Remove hours.
	iHour -= iRemoveHours
	
	// If we're under 0 hours, remove a day.
	WHILE iHour < 0
		iRemoveDays += 1
		iHour += 24
	ENDWHILE
	
	
	// Remove days.
	iDay -= iRemoveDays
	
	// If we've being set to under 0 days, remove a month.
	WHILE iDay <= 0
		iRemoveMonths += 1
		
		//Check how many days were in the previous month.
		INT iPreviousMonth = iMonth - 1
		INT iPreviousYear = iYear
		IF iPreviousMonth < 0
			iPreviousMonth = ENUM_TO_INT(DECEMBER)
			iPreviousYear--
		ENDIF
		MONTH_OF_YEAR ePreviousMonth = INT_TO_ENUM(MONTH_OF_YEAR, iPreviousMonth)
		INT iDaysInPreviousMonth = GET_NUMBER_OF_DAYS_IN_MONTH(ePreviousMonth, iPreviousYear)
		
		iDay += iDaysInPreviousMonth
	ENDWHILE
	
	// Remove months.
	iMonth -= iRemoveMonths
	
	// If we're under 0 months, remove a year.
	WHILE iMonth < 0
		iRemoveYears += 1
		iMonth += 12
	ENDWHILE
	
	
	// Remove years.
	iYear -= iRemoveYears
	
	SET_TIMEOFDAY(sTimeOfDay, iSecond, iMinute, iHour, iDay, INT_TO_ENUM(MONTH_OF_YEAR, iMonth), iYear)
ENDPROC


// PURPOSE: Checks to see if the timeofday param is valid
//
// RETURN PARAM:	paramTimeOfDay		The TIMEOFDAY enum being checked
FUNC BOOL IS_TIMEOFDAY_VALID(TIMEOFDAY paramTimeOfDay)

	// check to see if the passed TIMEOFDAY has been cleared (all values are zero)
	IF paramTimeOfDay = INVALID_TIMEOFDAY
		RETURN FALSE
	ENDIF
	
	// Check the second is valid.
	INT iSecond = GET_TIMEOFDAY_SECOND(paramTimeOfDay)
	IF (iSecond < 0) OR (iSecond >= 60)
		RETURN FALSE
	ENDIF
	
	// Check the minute is valid. 
	INT iMinute = GET_TIMEOFDAY_MINUTE(paramTimeOfDay)
	IF (iMinute < 0) OR (iMinute >= 60)
		RETURN FALSE
	ENDIF
	
	// Check the hour is valid.
	INT iHour = GET_TIMEOFDAY_HOUR(paramTimeOfDay)
	IF (iHour < 0) OR (iHour > 23)
		RETURN FALSE
	ENDIF
	
	// Check the year is valid.
	INT iYear = GET_TIMEOFDAY_YEAR(paramTimeOfDay)
	IF (iYear <= 0)	
	OR iYear > TIMEOFDAY_YEAR_OFFSET + 32
	OR iYear < TIMEOFDAY_YEAR_OFFSET - 32
		RETURN FALSE
	ENDIF
	
	// Check the month is valid.
	MONTH_OF_YEAR eMonth = GET_TIMEOFDAY_MONTH(paramTimeOfDay)
	IF (eMonth < JANUARY) OR (eMonth > DECEMBER)
		RETURN FALSE
	ENDIF
	
	// Check the day is valid.
	INT iDay = GET_TIMEOFDAY_DAY(paramTimeOfDay)
	IF (iDay < 1) OR (iDay > GET_NUMBER_OF_DAYS_IN_MONTH(eMonth, iYear))
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC


/// PURPOSE:
///    Checks to see if Timeofday 1 is later than time of day 2.
/// RETURNS:
///    TRUE if TOD1 is later than TOD2, FALSE if not.
FUNC BOOL IS_TIMEOFDAY_AFTER_TIMEOFDAY(TIMEOFDAY sTOD1, TIMEOFDAY sTOD2)
	IF NOT IS_TIMEOFDAY_VALID(sTOD2) OR NOT IS_TIMEOFDAY_VALID(sTOD1)
		RETURN TRUE
	ENDIF
	
	// Easy check first. Years.
	INT iTOD1Temp = GET_TIMEOFDAY_YEAR(sTOD1)
	INT iTOD2Temp = GET_TIMEOFDAY_YEAR(sTOD2)
	IF iTOD1Temp > iTOD2Temp
		RETURN TRUE
	ELIF iTOD1Temp < iTOD2Temp
		RETURN FALSE
	ENDIF
	
	// Check months.	
	iTOD1Temp = ENUM_TO_INT(GET_TIMEOFDAY_MONTH(sTOD1))
	iTOD2Temp = ENUM_TO_INT(GET_TIMEOFDAY_MONTH(sTOD2))
	IF iTOD1Temp > iTOD2Temp
		RETURN TRUE
	ELIF iTOD1Temp < iTOD2Temp
		RETURN FALSE
	ENDIF
			
	// Months are same, check days.
	iTOD1Temp = GET_TIMEOFDAY_DAY(sTOD1)
	iTOD2Temp = GET_TIMEOFDAY_DAY(sTOD2)
	IF iTOD1Temp > iTOD2Temp
		RETURN TRUE
	ELIF iTOD1Temp < iTOD2Temp
		RETURN FALSE
	ENDIF
				
	// Days are same, check hours.
	iTOD1Temp = GET_TIMEOFDAY_HOUR(sTOD1)
	iTOD2Temp = GET_TIMEOFDAY_HOUR(sTOD2)
	IF iTOD1Temp > iTOD2Temp
		RETURN TRUE
	ELIF iTOD1Temp < iTOD2Temp
		RETURN FALSE
	ENDIF
				
	// Hours are same, do minute check.
	iTOD1Temp = GET_TIMEOFDAY_MINUTE(sTOD1)
	iTOD2Temp = GET_TIMEOFDAY_MINUTE(sTOD2)
	IF iTOD1Temp > iTOD2Temp
		RETURN TRUE
	ELIF iTOD1Temp < iTOD2Temp
		RETURN FALSE
	ENDIF
						
	// Minutes are the same... last check, do the seconds.
	iTOD1Temp = GET_TIMEOFDAY_SECOND(sTOD1)
	iTOD2Temp = GET_TIMEOFDAY_SECOND(sTOD2)
	IF iTOD1Temp > iTOD2Temp
		RETURN TRUE
	ENDIF
	
	// Current time is not later than passed in time, returning FALSE
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Tried to be descriptive with the function name, but it checks to see if our current time right now is AFTER the passed in TIMEOFDAY
/// RETURNS:
///    TRUE if the current time is later than the passed in time.
FUNC BOOL IS_NOW_AFTER_TIMEOFDAY(TIMEOFDAY sTimeOfDay)
	RETURN IS_TIMEOFDAY_AFTER_TIMEOFDAY(GET_CURRENT_TIMEOFDAY(), sTimeOfDay)
ENDFUNC

//BOOL bSwapDiff = TRUE

/// PURPOSE:
///    
/// PARAMS:
///    sTimeOfDayOne - 
///    sTimeOfDayTwo - 
/// RETURNS:
///    
PROC GET_DIFFERENCE_BETWEEN_TIMEOFDAYS(TIMEOFDAY sTimeOfDayOne, TIMEOFDAY sTimeOfDayTwo, INT &iSeconds, INT &iMinutes, INT &iHours, INT &iDays, INT &iMonths, INT &iYears /*, BOOL bDisplayDebug = FALSE*/)
//	IF bDisplayDebug
//		CDEBUG1LN(DEBUG_SYSTEM, "<CLOCK> GET_DIFFERENCE_BETWEEN_TIMEOFDAYS")
//	
//		#IF IS_DEBUG_BUILD
//			TEXT_LABEL_63 txtTOD
//			txtTOD = TIMEOFDAY_TO_TEXT_LABEL(sTimeOfDayOne)
//			CDEBUG1LN(DEBUG_SYSTEM, "<CLOCK> TimeOfDayOne: ", txtTOD)
//			txtTOD = TIMEOFDAY_TO_TEXT_LABEL(sTimeOfDayTwo)
//			CDEBUG1LN(DEBUG_SYSTEM, "<CLOCK> TimeOfDayTwo: ", txtTOD)
//		#ENDIF
//	ENDIF
	
	INT iCurrentMonth
	INT iStartYear
	INT iTestYear
	INT iTestMonth 	
	INT iTestDay	
	INT iTestHour	
	INT iTestMinute 
	INT iTestSecond 

	IF (IS_TIMEOFDAY_AFTER_TIMEOFDAY(sTimeOfDayOne, sTimeOfDayTwo))
		//IF bDisplayDebug CDEBUG1LN(DEBUG_SYSTEM, "<CLOCK> TimeOfDayOne is after TimeOfDayTwo") ENDIF
		
		iCurrentMonth 	= ENUM_TO_INT(GET_TIMEOFDAY_MONTH(sTimeOfDayTwo))
		iStartYear  	= GET_TIMEOFDAY_YEAR(sTimeOfDayOne)
		iTestYear 		= GET_TIMEOFDAY_YEAR(sTimeOfDayOne) - GET_TIMEOFDAY_YEAR(sTimeOfDayTwo)
		iTestMonth 		= ENUM_TO_INT(GET_TIMEOFDAY_MONTH(sTimeOfDayOne) - GET_TIMEOFDAY_MONTH(sTimeOfDayTwo))
		iTestDay		= GET_TIMEOFDAY_DAY(sTimeOfDayOne) - GET_TIMEOFDAY_DAY(sTimeOfDayTwo)
		iTestHour		= GET_TIMEOFDAY_HOUR(sTimeOfDayOne) - GET_TIMEOFDAY_HOUR(sTimeOfDayTwo)
		iTestMinute 	= GET_TIMEOFDAY_MINUTE(sTimeOfDayOne) - GET_TIMEOFDAY_MINUTE(sTimeOfDayTwo)
		iTestSecond 	= GET_TIMEOFDAY_SECOND(sTimeOfDayOne) - GET_TIMEOFDAY_SECOND(sTimeOfDayTwo)
	ELSE
		//IF bDisplayDebug CDEBUG1LN(DEBUG_SYSTEM, "<CLOCK> TimeOfDayTwo is after TimeOfDayOne") ENDIF
	
		iCurrentMonth 	= ENUM_TO_INT(GET_TIMEOFDAY_MONTH(sTimeOfDayOne))
		iStartYear  	= GET_TIMEOFDAY_YEAR(sTimeOfDayTwo)
		iTestYear 		= GET_TIMEOFDAY_YEAR(sTimeOfDayTwo) - GET_TIMEOFDAY_YEAR(sTimeOfDayOne)
		iTestMonth 		= ENUM_TO_INT(GET_TIMEOFDAY_MONTH(sTimeOfDayTwo) - GET_TIMEOFDAY_MONTH(sTimeOfDayOne))
		iTestDay		= GET_TIMEOFDAY_DAY(sTimeOfDayTwo) - GET_TIMEOFDAY_DAY(sTimeOfDayOne)
		iTestHour		= GET_TIMEOFDAY_HOUR(sTimeOfDayTwo) - GET_TIMEOFDAY_HOUR(sTimeOfDayOne)
		iTestMinute 	= GET_TIMEOFDAY_MINUTE(sTimeOfDayTwo) - GET_TIMEOFDAY_MINUTE(sTimeOfDayOne)
		iTestSecond 	= GET_TIMEOFDAY_SECOND(sTimeOfDayTwo) - GET_TIMEOFDAY_SECOND(sTimeOfDayOne)
	ENDIF
	
//	IF bDisplayDebug
//		CDEBUG1LN(DEBUG_SYSTEM, "<CLOCK> iCurrentMonth: ", iCurrentMonth)
//		CDEBUG1LN(DEBUG_SYSTEM, "<CLOCK> iStartYear: ", iStartYear)
//		CDEBUG1LN(DEBUG_SYSTEM, "<CLOCK> iTestYear: ", iTestYear)
//		CDEBUG1LN(DEBUG_SYSTEM, "<CLOCK> iTestMonth: ", iTestMonth)
//		CDEBUG1LN(DEBUG_SYSTEM, "<CLOCK> iTestDay: ", iTestDay)
//		CDEBUG1LN(DEBUG_SYSTEM, "<CLOCK> iTestHour: ", iTestHour)
//		CDEBUG1LN(DEBUG_SYSTEM, "<CLOCK> iTestMinute: ", iTestMinute)
//		CDEBUG1LN(DEBUG_SYSTEM, "<CLOCK> iTestSecond: ", iTestSecond)
//	ENDIF
	
	// Seconds
	//IF bDisplayDebug CDEBUG1LN(DEBUG_SYSTEM, "<CLOCK> Before second calculation. Second:", iTestSecond, " Minute:",iTestMinute) ENDIF
	WHILE (iTestSecond < 0)
		//IF bDisplayDebug CDEBUG1LN(DEBUG_SYSTEM, "<CLOCK> Increasing seconds, reducing minutes.") ENDIF
		iTestSecond	+= 60
		iTestMinute--
	ENDWHILE
	WHILE (iTestSecond > 59)
		//IF bDisplayDebug CDEBUG1LN(DEBUG_SYSTEM, "<CLOCK> Decreasing seconds, increasing minutes.") ENDIF
		iTestSecond -= 60
		iTestMinute++
	ENDWHILE
	//IF bDisplayDebug CDEBUG1LN(DEBUG_SYSTEM, "<CLOCK> After second calculation. Second:", iTestSecond, " Minute:",iTestMinute) ENDIF
		
	// Minutes
	//IF bDisplayDebug CDEBUG1LN(DEBUG_SYSTEM, "<CLOCK> Before minute calculation. Minute:", iTestMinute, " Hour:",iTestHour) ENDIF
	WHILE (iTestMinute < 0)
		//IF bDisplayDebug CDEBUG1LN(DEBUG_SYSTEM, "<CLOCK> Increasing minutes, reducing hours.") ENDIF
		iTestMinute	+= 60
		iTestHour--
	ENDWHILE
	WHILE (iTestMinute > 59)
		//IF bDisplayDebug CDEBUG1LN(DEBUG_SYSTEM, "<CLOCK> Reducing minutes, increasing hours.") ENDIF
		iTestMinute	-= 60
		iTestHour++
	ENDWHILE
	//IF bDisplayDebug CDEBUG1LN(DEBUG_SYSTEM, "<CLOCK> After minute calculation. Minute:", iTestMinute, " Hour:",iTestHour) ENDIF
	
	// Hours
	//IF bDisplayDebug CDEBUG1LN(DEBUG_SYSTEM, "<CLOCK> Before hour calculation. Hour:", iTestHour, " Day:",iTestDay) ENDIF
	WHILE (iTestHour < 0)
		//IF bDisplayDebug CDEBUG1LN(DEBUG_SYSTEM, "<CLOCK> Increasing hours, reducing days.") ENDIF
		iTestHour += 24
		iTestDay--
	ENDWHILE
	WHILE (iTestHour > 23)
		//IF bDisplayDebug CDEBUG1LN(DEBUG_SYSTEM, "<CLOCK> Reducing hours, increasing days.") ENDIF
		iTestHour -= 24
		iTestDay++
	ENDWHILE
	//IF bDisplayDebug CDEBUG1LN(DEBUG_SYSTEM, "<CLOCK> After hour calculation. Hour:", iTestHour, " Day:",iTestDay) ENDIF
	
	// Days
	//IF bDisplayDebug CDEBUG1LN(DEBUG_SYSTEM, "<CLOCK> Before days calculation. Day:", iTestDay, " Month:",iTestMonth) ENDIF
	WHILE (iTestDay	< 0)
	
		// Month
		WHILE (iTestMonth < 0)
			//IF bDisplayDebug CDEBUG1LN(DEBUG_SYSTEM, "<CLOCK> Increasing month, reducing years.") ENDIF
			iTestMonth += 12
			iTestYear--
		ENDWHILE
		
		//IF bDisplayDebug CDEBUG1LN(DEBUG_SYSTEM, "<CLOCK> Increasing days, reducing months.") ENDIF
		iTestDay += GET_NUMBER_OF_DAYS_IN_MONTH(INT_TO_ENUM(MONTH_OF_YEAR, iCurrentMonth), iStartYear )
		iTestMonth--
		
		iCurrentMonth = ROUND(WRAP(TO_FLOAT(iCurrentMonth+1), 0, 12))
		//IF bDisplayDebug CDEBUG1LN(DEBUG_SYSTEM, "<CLOCK> Current month set to ", iCurrentMonth) ENDIF
	ENDWHILE
	//IF bDisplayDebug CDEBUG1LN(DEBUG_SYSTEM, "<CLOCK> After days calculation. Day:", iTestDay, " Month:",iTestMonth) ENDIF
	
	// Month
	//IF bDisplayDebug CDEBUG1LN(DEBUG_SYSTEM, "<CLOCK> Before months calculation. Month:", iTestMonth, " Year:",iTestYear) ENDIF
	WHILE (iTestMonth < 0)
		//IF bDisplayDebug CDEBUG1LN(DEBUG_SYSTEM, "<CLOCK> Increasing months, reducing years.") ENDIF
		iTestMonth += 12
		iTestYear--
	ENDWHILE
	WHILE (iTestMonth > 12)
		//IF bDisplayDebug CDEBUG1LN(DEBUG_SYSTEM, "<CLOCK> Decreasing months, increasing years.") ENDIF
		iTestMonth -= 12
		iTestYear++
	ENDWHILE
	//IF bDisplayDebug CDEBUG1LN(DEBUG_SYSTEM, "<CLOCK> After months calculation. Month:", iTestMonth, " Year:",iTestYear) ENDIF
	
	// Set the values
	iSeconds = iTestSecond
	iMinutes = iTestMinute
	iHours = iTestHour
	iDays = iTestDay
	iMonths = iTestMonth
	iYears = iTestYear
	
//	IF bDisplayDebug
//		CDEBUG1LN(DEBUG_SYSTEM, "<CLOCK> RESULTS")
//		CDEBUG1LN(DEBUG_SYSTEM, "<CLOCK> Years difference: ", iYears)
//		CDEBUG1LN(DEBUG_SYSTEM, "<CLOCK> Months difference: ", iMonths)
//		CDEBUG1LN(DEBUG_SYSTEM, "<CLOCK> Days difference: ", iDays)
//		CDEBUG1LN(DEBUG_SYSTEM, "<CLOCK> Hours difference: ", iHours)
//		CDEBUG1LN(DEBUG_SYSTEM, "<CLOCK> Minutes difference: ", iMinutes)
//		CDEBUG1LN(DEBUG_SYSTEM, "<CLOCK> Seconds difference: ", iSeconds)
//	ENDIF
ENDPROC


/// PURPOSE:
///    
/// PARAMS:
///    sLastTimeActive - 
/// RETURNS:
///    
PROC GET_DIFFERENCE_BETWEEN_NOW_AND_TIMEOFDAY(TIMEOFDAY sLastTimeActive, INT &iSeconds, INT &iMinutes, INT &iHours, INT &iDays, INT &iMonths, INT &iYears)
	GET_DIFFERENCE_BETWEEN_TIMEOFDAYS(GET_CURRENT_TIMEOFDAY(), sLastTimeActive, iSeconds, iMinutes, iHours, iDays, iMonths, iYears)
ENDPROC


// PURPOSE:	Check if a time is between two hours
//
// INPUT PARAMS:		paramStartHour			The start hour (inclusive)
//						paramEndHour			The end hour (exclusive)
FUNC BOOL IS_HOUR_BETWEEN_THESE_HOURS(INT paramTestHour, INT paramStartHour, INT paramEndHour)

	// Ensure valid hours
	// ...ensure start hour is between 0 and 24 (where 24 is converted to 0)
	IF (paramStartHour = 24)
		paramStartHour = 0
	ENDIF
	
	IF (paramStartHour < 0)
	OR (paramStartHour >= 24)
		#IF IS_DEBUG_BUILD
			CASSERTLN(DEBUG_SYSTEM, "IS_HOUR_BETWEEN_THESE_HOURS: Illegal Start Hour: ", paramStartHour, " called by script: ", GET_THIS_SCRIPT_NAME())
		#ENDIF
		
		RETURN FALSE
	ENDIF

	// ...ensure end hour is between 0 and 24 (where 24 is converted to 0)
	IF (paramEndHour = 24)
		paramEndHour = 0
	ENDIF
	
	IF (paramEndHour < 0)
	OR (paramEndHour >= 24)
		#IF IS_DEBUG_BUILD
			CASSERTLN(DEBUG_SYSTEM, "IS_HOUR_BETWEEN_THESE_HOURS: Illegal End Hour: ", paramEndHour, " called by script: ", GET_THIS_SCRIPT_NAME())
		#ENDIF
		
		RETURN FALSE
	ENDIF

	// If start and end hours are the same, then check is for the full day, so return TRUE
	IF (paramStartHour = paramEndHour)
		RETURN TRUE
	ENDIF
	
	// Check if the valid time is over midnight
	BOOL timeWrap = (paramEndHour < paramStartHour)
	
	INT theStartHour = paramStartHour
	INT theEndHour = paramEndHour
	IF (timeWrap)
		theEndHour = 24
	ENDIF
	
	IF (paramTestHour >= theStartHour)
	AND (paramTestHour < theEndHour)
		// Valid time
		RETURN TRUE
	ENDIF
	
	// If the time doesn't span Midnight, then this is an illegal time
	IF NOT (timeWrap)
		RETURN FALSE
	ENDIF
	
	// Additional check for times that span midnight
	theStartHour = 0
	theEndHour = paramEndHour

	IF (paramTestHour >= theStartHour)
	AND (paramTestHour < theEndHour)
		// Valid time
		RETURN TRUE
	ENDIF
	
	// Illegal time
	RETURN FALSE

ENDFUNC

// PURPOSE:	Check if a time is between two hours
//
// INPUT PARAMS:		paramStartHour			The start hour (inclusive)
//						paramEndHour			The end hour (exclusive)
FUNC BOOL IS_TIME_BETWEEN_THESE_HOURS(INT paramStartHour, INT paramEndHour)
	RETURN IS_HOUR_BETWEEN_THESE_HOURS(GET_CLOCK_HOURS(), paramStartHour, paramEndHour)
ENDFUNC



FUNC BOOL HasNumOfMinutesPassedSincePedTimeStruct(TIMEOFDAY sLastTimeActive, INT iNumOfMinutes)
	IF NOT Is_TIMEOFDAY_Valid(sLastTimeActive)
		RETURN FALSE
	ENDIF
	
	INT iSeconds, iMinutes, iHours, iDays, iMonths, iYears
	GET_DIFFERENCE_BETWEEN_NOW_AND_TIMEOFDAY(sLastTimeActive, iSeconds, iMinutes, iHours, iDays, iMonths, iYears)
	
	// 		only need to check if year, month, day or hour is 
	// greater than zero to see if more than an hour has passed
	IF (iYears		> 0)
	OR (iMonths		> 0)
	OR (iDays		> 0)
	OR (iHours		> 0)
	OR (iMinutes	>= iNumOfMinutes)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HasNumOfHoursPassedSincePedTimeStruct(TIMEOFDAY sLastTimeActive, INT iNumOfHours)
	IF NOT Is_TIMEOFDAY_Valid(sLastTimeActive)
		RETURN FALSE
	ENDIF
	
	INT iSeconds, iMinutes, iHours, iDays, iMonths, iYears
	GET_DIFFERENCE_BETWEEN_NOW_AND_TIMEOFDAY(sLastTimeActive, iSeconds, iMinutes, iHours, iDays, iMonths, iYears)
	
	// 		only need to check if year, month, or day is 
	// greater than zero to see if more than an hour has passed
	IF (iYears	> 0)
	OR (iMonths	> 0)
	OR (iDays	> 0)
	OR (iHours	>= iNumOfHours)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

// ===========================================================================================================
#IF IS_DEBUG_BUILD

PROC DO_TIMEOFDAY_TEST()
	INT timeIter
	TIMEOFDAY testTime 
	CPRINTLN(DEBUG_SYSTEM, "Second test")
	REPEAT 60 timeIter
		testTime = INT_TO_ENUM(TIMEOFDAY, 0)
		SET_TIMEOFDAY(testTime, timeIter, 0, 0, 1, JANUARY, 2011)
		PRINT_TIMEOFDAY(testTime)
		ADD_TIME_TO_TIMEOFDAY(testTime, 10)
		PRINT_TIMEOFDAY(testTime)
		CPRINTLN(DEBUG_SYSTEM, "")
	ENDREPEAT
	CPRINTLN(DEBUG_SYSTEM, "Minute test")
	REPEAT 60 timeIter
		testTime = INT_TO_ENUM(TIMEOFDAY, 0)
		SET_TIMEOFDAY(testTime, 0, timeIter, 0, 1, JANUARY, 2011)
		PRINT_TIMEOFDAY(testTime)
		ADD_TIME_TO_TIMEOFDAY(testTime, 0, 15)
		PRINT_TIMEOFDAY(testTime)
		CPRINTLN(DEBUG_SYSTEM, "")
	ENDREPEAT
	CPRINTLN(DEBUG_SYSTEM, "Hour test")
	REPEAT 24 timeIter
		testTime = INT_TO_ENUM(TIMEOFDAY, 0)
		SET_TIMEOFDAY(testTime, 0, 0, timeIter, 1, JANUARY, 2011)
		PRINT_TIMEOFDAY(testTime)
		ADD_TIME_TO_TIMEOFDAY(testTime, 0, 0, 5)
		PRINT_TIMEOFDAY(testTime)
		CPRINTLN(DEBUG_SYSTEM, "")
	ENDREPEAT
	CPRINTLN(DEBUG_SYSTEM, "Day test")
	REPEAT 31 timeIter
		testTime = INT_TO_ENUM(TIMEOFDAY, 0)
		SET_TIMEOFDAY(testTime, 0, 0, 0, 1 + timeIter, JANUARY, 2011)
		PRINT_TIMEOFDAY(testTime)
		ADD_TIME_TO_TIMEOFDAY(testTime, 0, 0, 0, 4)
		PRINT_TIMEOFDAY(testTime)
		CPRINTLN(DEBUG_SYSTEM, "")
	ENDREPEAT
	CPRINTLN(DEBUG_SYSTEM, "Month test")
	REPEAT 12 timeIter
		testTime = INT_TO_ENUM(TIMEOFDAY, 0)
		SET_TIMEOFDAY(testTime, 0, 0, 0, 1, INT_TO_ENUM(MONTH_OF_YEAR, timeIter), 2011)
		PRINT_TIMEOFDAY(testTime)
		ADD_TIME_TO_TIMEOFDAY(testTime, 0, 0, 0, 0, 3)
		PRINT_TIMEOFDAY(testTime)
		CPRINTLN(DEBUG_SYSTEM, "")
	ENDREPEAT

ENDPROC

#ENDIF


