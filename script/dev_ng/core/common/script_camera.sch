USING "commands_camera.sch"

/// PURPOSE:
///    Grabs pertinent values from the game camera, sets up a new scripted camera with this (for manipulation)
/// RETURNS:
///    The new camera index.
FUNC CAMERA_INDEX GET_GAME_CAMERA_COPY()
	RETURN CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", GET_GAMEPLAY_CAM_COORD(), GET_GAMEPLAY_CAM_ROT(), GET_GAMEPLAY_CAM_FOV())
ENDFUNC

