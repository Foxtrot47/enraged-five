#IF NOT DEFINED(FLOW_INFO_IS_MISSION_ACTIVE)
/// PURPOSE:
///    Project-specific function to determine if a mission is active or not. Override this!
FUNC BOOL FLOW_INFO_IS_MISSION_ACTIVE(INT missionIndex)
	missionIndex = missionIndex
	RETURN FALSE
ENDFUNC
#ENDIF

#IF NOT DEFINED(DISPLAY_STRAND_CURRENT_COMMAND_DESCRIPTION_CUSTOM)
/// PURPOSE:
///    Project-specific function to draw flow info descriptions for project-specific flow commands. Override this!
FUNC BOOL DISPLAY_STRAND_CURRENT_COMMAND_DESCRIPTION_CUSTOM(FLOW_COMMAND_IDS thisCommand, INT thisCommandPos, FLOAT paramYPos, FLOW_INFO_COLOUR &lineColour)
	thisCommand = thisCommand
	thisCommandPos = thisCommandPos
	paramYPos = paramYPos
	lineColour = lineColour
	RETURN FALSE
ENDFUNC
#ENDIF

#IF NOT DEFINED(DISPLAY_TITLES_CUSTOM)
/// PURPOSE:
///    Display project specific titles for the center and right hand panels. Override this!
PROC DISPLAY_TITLES_CUSTOM()
ENDPROC
#ENDIF

#IF NOT DEFINED(DISPLAY_FLOW_INFO_CUSTOM)
/// PURPOSE:
///    Display project-specific info for center and righthand panels. Override this!
PROC DISPLAY_FLOW_INFO_CUSTOM()
ENDPROC
#ENDIF

#IF NOT DEFINED(DISPLAY_STRAND_DO_MISSION)
// PURPOSE:	Display the 'do mission' message - should be suitable for all 'do mission' variants
FUNC FLOW_INFO_COLOUR DISPLAY_STRAND_DO_MISSION(INT paramCommandPos, FLOAT paramYPos, FLOW_INFO_COLOUR paramColour)

	FLOW_INFO_COLOUR highlightColour = paramColour
	
	// Extract the details for the mission display
	FLOW_STORAGE_TYPES flowStorageType = g_flowUnsaved.flowCommands[paramCommandPos].storageType
	IF NOT (flowStorageType = FST_SCRIPT)
		// ...should never really hit this, but just in case
		RETURN highlightColour
	ENDIF
	
	INT scriptVarsIndex = g_flowUnsaved.flowCommands[paramCommandPos].index
	IF FLOW_INFO_IS_MISSION_ACTIVE(scriptVarsIndex)
		highlightColour = COL_ACTIVE
	ENDIF

	// The command
	FLOW_COMMAND_IDS thisCommand = g_flowUnsaved.flowCommands[paramCommandPos].command
	DISPLAY_FLOW_INFO_LITERAL_TEXT(highlightColour, FLOW_INFO_DESCRIPTION_XPOS, paramYPos, GET_COMMAND_DISPLAY_STRING_FROM_COMMAND_ID(thisCommand))

	// The Mission Filename
	DISPLAY_FLOW_INFO_LITERAL_TEXT(highlightColour, FLOW_INFO_PARAM1_XPOS, paramYPos, g_flowUnsaved.scriptVars[scriptVarsIndex].filename)
	
	RETURN highlightColour
	
ENDFUNC
#ENDIF

