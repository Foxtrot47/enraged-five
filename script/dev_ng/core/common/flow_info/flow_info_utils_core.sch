
USING "flow_info_structs_core.sch"
USING "commands_graphics.sch"
USING "commands_hud.sch"

// ===========================================================================================================
//		Standard F9 Screen Display Controls
// ===========================================================================================================


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Display the re-sizeable background for the general F9 screen
// 
PROC DISPLAY_BACKGROUND()

	// Calculate the background rectangle height
	FLOAT	textBoxYSize	= g_fFlowInfoBackgroundHeight //FLOW_INFO_ADD_Y * ENUM_TO_INT(MAX_STRANDS)
	FLOAT	boxYBorder		= FLOW_INFO_START_YPOS + (FLOW_INFO_ADD_Y * 0.5)
	FLOAT	totalBoxYSize	= textBoxYSize + boxYBorder
	
	FLOAT	boxYCentre		= totalBoxYSize * 0.5
	
	DRAW_RECT(0.5, boxYCentre, 1.0, totalBoxYSize, 0, 0, 0, 175)

ENDPROC


// -----------------------------------------------------------------------------------------------------------
//		Interface With Native Commands
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set up the text colour to be used for this text
//
// INPUT PARAMS:	paramColour			The Colour value used for the R,G,B

PROC SETUP_FLOW_INFO_TEXT_COLOUR(FLOW_INFO_COLOUR paramColour)

	INT theRGB = ENUM_TO_INT(paramColour)
	
	SWITCH (paramColour)
		CASE COL_ACTIVE
			SET_TEXT_COLOUR(200, 200, 0, 255)
			BREAK
			
		DEFAULT
			SET_TEXT_COLOUR(theRGB, theRGB, theRGB, 255)
			BREAK
	ENDSWITCH

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set up the standard text display used for the F9 screen
//
// INPUT PARAMS:	paramColour			The Colour value used for the R,G,B

PROC SETUP_FLOW_INFO_TEXT_DISPLAY(FLOW_INFO_COLOUR paramColour, FLOAT paramScaleX = FLOW_INFO_TEXT_SCALE_X, FLOAT paramScaleY = FLOW_INFO_TEXT_SCALE_Y)

	SETUP_FLOW_INFO_TEXT_COLOUR(paramColour)
	SET_TEXT_SCALE(paramScaleX, paramScaleY)
	SET_TEXT_WRAP(FLOW_INFO_TEXT_WRAP_XPOS_START, FLOW_INFO_TEXT_WRAP_XPOS_END)
	SET_TEXT_DROPSHADOW(0, 0, 0, 0, 255)

ENDPROC




// -----------------------------------------------------------------------------------------------------------
//		Generic F9 Screen Display Commands
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set up the standard literal text display used for the F9 screen and display text with it
//
// INPUT PARAMS:	paramColour			The Colour value used for the R,G,B
//					paramXPos			Text X pos
//					paramYPos			Text Y pos
//					paramText			The literal string to be output

PROC DISPLAY_FLOW_INFO_LITERAL_TEXT(FLOW_INFO_COLOUR paramColour, FLOAT paramXPos, FLOAT paramYPos, STRING paramText, FLOAT paramScaleX = FLOW_INFO_TEXT_SCALE_X, FLOAT paramScaleY = FLOW_INFO_TEXT_SCALE_Y)

	SETUP_FLOW_INFO_TEXT_DISPLAY(paramColour, paramScaleX, paramScaleY)
	DISPLAY_TEXT_WITH_LITERAL_STRING(paramXPos, paramYPos, "STRING", paramText)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set up the standard textLabel text display
//
// INPUT PARAMS:	paramColour			The Colour value used for the R,G,B
//					paramXPos			Text X pos
//					paramYPos			Text Y pos
//					paramTextLabel		The textLabel to be output

PROC DISPLAY_FLOW_INFO_TEXTLABEL(FLOW_INFO_COLOUR paramColour, FLOAT paramXPos, FLOAT paramYPos, STRING paramTextLabel, FLOAT paramScaleX = FLOW_INFO_TEXT_SCALE_X, FLOAT paramScaleY = FLOW_INFO_TEXT_SCALE_Y)

	SETUP_FLOW_INFO_TEXT_DISPLAY(paramColour, paramScaleX, paramScaleY)
	DISPLAY_TEXT(paramXPos, paramYPos, paramTextLabel)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set up the standard textLabel text display with 1 number used for the F9 screen
//
// INPUT PARAMS:	paramColour			The Colour value used for the R,G,B
//					paramXPos			Text X pos
//					paramYPos			Text Y pos
//					paramTextLabel		The textLabel to be output
//					paramInt			The first number

PROC DISPLAY_FLOW_INFO_TEXTLABEL_WITH_ONE_NUMBER(FLOW_INFO_COLOUR paramColour, FLOAT paramXPos, FLOAT paramYPos, STRING paramTextLabel, INT paramInt, FLOAT paramScaleX = FLOW_INFO_TEXT_SCALE_X, FLOAT paramScaleY = FLOW_INFO_TEXT_SCALE_Y)

	SETUP_FLOW_INFO_TEXT_DISPLAY(paramColour, paramScaleX, paramScaleY)
	DISPLAY_TEXT_WITH_NUMBER(paramXPos, paramYPos, paramTextLabel, paramInt)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set up the standard textLabel text display with 2 numbers used for the F9 screen
//
// INPUT PARAMS:	paramColour			The Colour value used for the R,G,B
//					paramXPos			Text X pos
//					paramYPos			Text Y pos
//					paramTextLabel		The textLabel to be output
//					paramInt1			The first number
//					paramInt2			The second number

PROC DISPLAY_FLOW_INFO_TEXTLABEL_WITH_TWO_NUMBERS(FLOW_INFO_COLOUR paramColour, FLOAT paramXPos, FLOAT paramYPos, STRING paramTextLabel, INT paramInt1, INT paramInt2, FLOAT paramScaleX = FLOW_INFO_TEXT_SCALE_X, FLOAT paramScaleY = FLOW_INFO_TEXT_SCALE_Y)

	SETUP_FLOW_INFO_TEXT_DISPLAY(paramColour, paramScaleX, paramScaleY)
	DISPLAY_TEXT_WITH_2_NUMBERS(paramXPos, paramYPos, paramTextLabel, paramInt1, paramInt2)

ENDPROC
