USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_graphics.sch"
USING "commands_misc.sch"
USING "script_clock.sch"
USING "flow_structs_core.sch"
USING "flow_debug_core.sch"




// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	SP_Mission_Flow_FLOW_INFO_Screen.sch
//		AUTHOR			:	Keith
//		DESCRIPTION		:	Updates the Flow Info display.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************


USING "flow_info_structs_core.sch"
USING "flow_info_utils_core.sch"
USING "flow_info_GAME.sch"
USING "flow_info_core_override.sch"





// ===========================================================================================================
//		Specific Flow Info Screen Command Display Messages
// ===========================================================================================================

// PURPOSE:	Display the 'unknown command' message
//
// INPUT PARAMS:	paramYPos			The screen Y Pos for these details
//					paramColour			The text display colour

PROC DISPLAY_STRAND_UNKNOWN_COMMAND(FLOAT paramYPos, FLOW_INFO_COLOUR paramColour)

	DISPLAY_FLOW_INFO_LITERAL_TEXT(paramColour, FLOW_INFO_DESCRIPTION_XPOS, paramYPos, "UNKNOWN COMMAND")

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Display the 'strand awaiting activation' message
//
// INPUT PARAMS:	paramYPos			The screen Y Pos for these details
// RETURN VALUE:	FLOW_INFO_COLOUR			Colour with which to display the 'strand name'

FUNC FLOW_INFO_COLOUR DISPLAY_STRAND_NOT_ACTIVATED(FLOAT paramYPos)

	FLOW_INFO_COLOUR lineColour = COL_MID
	DISPLAY_FLOW_INFO_LITERAL_TEXT(lineColour, FLOW_INFO_DESCRIPTION_XPOS, paramYPos, "awaiting activation")
	
	RETURN lineColour

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Display the 'strand terminated' message
//
// INPUT PARAMS:	paramYPos			The screen Y Pos for these details
// RETURN VALUE:	FLOW_INFO_COLOUR			Colour with which to display the 'strand name'

FUNC FLOW_INFO_COLOUR DISPLAY_STRAND_TERMINATED(FLOAT paramYPos)

	FLOW_INFO_COLOUR lineColour = COL_DULL
	DISPLAY_FLOW_INFO_LITERAL_TEXT(lineColour, FLOW_INFO_DESCRIPTION_XPOS, paramYPos, "terminated")
	
	RETURN lineColour

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Display the strand name
//
// INPUT PARAMS:	paramStrandID		The strand ID
//					paramYPos			The screen Y Pos for these details
//					paramColour			The colour to display the text

PROC DISPLAY_STRAND_NAME(STRANDS paramStrandID, FLOAT paramYPos, FLOW_INFO_COLOUR paramColour)
	#if USE_CLF_DLC
		STRING theName = GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID_CLF(paramStrandID)
		DISPLAY_FLOW_INFO_LITERAL_TEXT(paramColour, FLOW_INFO_NAME_XPOS, paramYPos, theName)
	#endif	
	#if USE_NRM_DLC
		STRING theName = GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID_NRM(paramStrandID)
		DISPLAY_FLOW_INFO_LITERAL_TEXT(paramColour, FLOW_INFO_NAME_XPOS, paramYPos, theName)	
	#endif	
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
		STRING theName = GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(paramStrandID)
		DISPLAY_FLOW_INFO_LITERAL_TEXT(paramColour, FLOW_INFO_NAME_XPOS, paramYPos, theName)
	#endif
	#endif
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Display the strand boundaries
//
// INPUT PARAMS:	paramStrandID		The strand ID
//					paramYPos			The screen Y Pos for these details
//					paramColour			The colour to display the text

//PROC DISPLAY_STRAND_BOUNDARIES(STRANDS paramStrandID, FLOAT paramYPos, FLOW_INFO_COLOUR paramColour)
//	
//	INT lowerBoundary = g_flowUnsaved.flowCommandsIndex[paramStrandID].lower
//	INT upperBoundary = g_flowUnsaved.flowCommandsIndex[paramStrandID].upper
//	
//	DISPLAY_FLOW_INFO_TEXTLABEL_WITH_TWO_NUMBERS(paramColour, FLOW_INFO_BOUNDARIES_XPOS, paramYPos, "F9_Bounds", lowerBoundary, upperBoundary)
//
//ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Display the strand command array position
//
// INPUT PARAMS:	paramStrandID		The strand ID
//					paramYPos			The screen Y Pos for these details
//					paramColour			The colour to display the text

//PROC DISPLAY_STRAND_COMMAND_ARRAY_POS(STRANDS paramStrandID, FLOAT paramYPos, FLOW_INFO_COLOUR paramColour)
//	
//	INT thisCommandPos	= g_savedGlobals.sFlow.strandSavedVars[paramStrandID].thisCommandPos
//	
//	DISPLAY_FLOW_INFO_TEXTLABEL_WITH_ONE_NUMBER(paramColour, FLOW_INFO_COMMAND_ARRAY_POS_XPOS, paramYPos, "number", thisCommandPos)
//
//ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Display the 'await synchronisation' message
//
// INPUT PARAMS:	paramCommandPos		The position of the command within the commands array
//					paramYPos			The screen Y Pos for these details
//					paramColour			The text display colour

PROC DISPLAY_STRAND_AWAIT_SYNCHRONISATION(INT paramCommandPos, FLOAT paramYPos, FLOW_INFO_COLOUR paramColour)

	// The command.
	DISPLAY_FLOW_INFO_LITERAL_TEXT(paramColour, FLOW_INFO_DESCRIPTION_XPOS, paramYPos, GET_COMMAND_DISPLAY_STRING_FROM_COMMAND_ID(FLOW_AWAIT_SYNCHRONISATION))
	
	// The sync flag.
	SYNCHRONIZATION_IDS eSyncID = INT_TO_ENUM(SYNCHRONIZATION_IDS, g_flowUnsaved.flowCommands[paramCommandPos].index) // Flag ID stored in index.
	DISPLAY_FLOW_INFO_LITERAL_TEXT(paramColour, FLOW_INFO_PARAM1_XPOS, paramYPos, GET_SYNCHRONISATION_DISPLAY_STRING_FROM_SYNC_ID(eSyncID))
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Display the 'do bitset jumps' message
//
// INPUT PARAMS:	paramCommandPos		The position of the command within the commands array
//					paramYPos			The screen Y Pos for these details
//					paramColour			The text display colour

PROC DISPLAY_STRAND_DO_BITSET_JUMPS(INT paramCommandPos, FLOAT paramYPos, FLOW_INFO_COLOUR paramColour)

	// The command.
	DISPLAY_FLOW_INFO_LITERAL_TEXT(paramColour, FLOW_INFO_DESCRIPTION_XPOS, paramYPos, GET_COMMAND_DISPLAY_STRING_FROM_COMMAND_ID(FLOW_DO_BITSET_JUMPS))
	
	// The bitset description.
	INT iCoreVarsIndex 			= g_flowUnsaved.flowCommands[paramCommandPos].index
	FLOW_BITSET_IDS eBitsetID	= INT_TO_ENUM(FLOW_BITSET_IDS, g_flowUnsaved.coreVars[iCoreVarsIndex].iValue1)
	DISPLAY_FLOW_INFO_LITERAL_TEXT(paramColour, FLOW_INFO_PARAM1_XPOS, paramYPos, GET_BITSET_DISPLAY_STRING_FROM_BITSET_ID(eBitsetID))
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Display the 'do flag jumps' message
//
// INPUT PARAMS:	paramCommandPos		The position of the command within the commands array
//					paramYPos			The screen Y Pos for these details
//					paramColour			The text display colour

PROC DISPLAY_STRAND_DO_FLAG_JUMPS(INT paramCommandPos, FLOAT paramYPos, FLOW_INFO_COLOUR paramColour)

	// The command.
	DISPLAY_FLOW_INFO_LITERAL_TEXT(paramColour, FLOW_INFO_DESCRIPTION_XPOS, paramYPos, GET_COMMAND_DISPLAY_STRING_FROM_COMMAND_ID(FLOW_DO_FLAG_JUMPS))
	
	// The flag description.
	INT iCoreVarsIndex 		= g_flowUnsaved.flowCommands[paramCommandPos].index
	FLOW_FLAG_IDS eFlagID	= INT_TO_ENUM(FLOW_FLAG_IDS, g_flowUnsaved.coreVars[iCoreVarsIndex].iValue1)
	DISPLAY_FLOW_INFO_LITERAL_TEXT(paramColour, FLOW_INFO_PARAM1_XPOS, paramYPos, GET_FLAG_DISPLAY_STRING_FROM_FLAG_ID(eFlagID))
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Display the 'do int jumps' message
//
// INPUT PARAMS:	paramCommandPos		The position of the command within the commands array
//					paramYPos			The screen Y Pos for these details
//					paramColour			The text display colour

PROC DISPLAY_STRAND_DO_INT_JUMPS(INT paramCommandPos, FLOAT paramYPos, FLOW_INFO_COLOUR paramColour)

	// The command.
	DISPLAY_FLOW_INFO_LITERAL_TEXT(paramColour, FLOW_INFO_DESCRIPTION_XPOS, paramYPos, GET_COMMAND_DISPLAY_STRING_FROM_COMMAND_ID(FLOW_DO_INT_JUMPS))
	
	// The int description.
	INT iCoreVarsIndex 		= g_flowUnsaved.flowCommands[paramCommandPos].index
	FLOW_INT_IDS eIntID	= INT_TO_ENUM(FLOW_INT_IDS, g_flowUnsaved.coreVars[iCoreVarsIndex].iValue1)
	DISPLAY_FLOW_INFO_LITERAL_TEXT(paramColour, FLOW_INFO_PARAM1_XPOS, paramYPos, GET_INT_DISPLAY_STRING_FROM_INT_ID(eIntID))
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------





// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Display the 'do strand killed jumps' message
//
// INPUT PARAMS:	paramCommandPos		The position of the command within the commands array
//					paramYPos			The screen Y Pos for these details
//					paramColour			The text display colour

PROC DISPLAY_STRAND_DO_STRAND_KILLED_JUMPS(INT paramCommandPos, FLOAT paramYPos, FLOW_INFO_COLOUR paramColour)

	// The command
	DISPLAY_FLOW_INFO_LITERAL_TEXT(paramColour, FLOW_INFO_DESCRIPTION_XPOS, paramYPos, GET_COMMAND_DISPLAY_STRING_FROM_COMMAND_ID(FLOW_DO_STRAND_KILLED_JUMPS))
	
	// The strand ID.
	INT iCoreVarsIndex 		= g_flowUnsaved.flowCommands[paramCommandPos].index
	STRANDS eStrandID	= INT_TO_ENUM(STRANDS, g_flowUnsaved.coreVars[iCoreVarsIndex].iValue1)
	DISPLAY_FLOW_INFO_LITERAL_TEXT(paramColour, FLOW_INFO_PARAM1_XPOS, paramYPos, GET_STRAND_DISPLAY_STRING_FROM_STRAND_ID(eStrandID))
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Display the 'launch script' message - hopefully suitable for all launch script command variants
//
// INPUT PARAMS:	paramCommandPos		The position of the command within the commands array
//					paramYPos			The screen Y Pos for these details
//					paramColour			The text display colour

PROC DISPLAY_STRAND_LAUNCH_SCRIPT(INT paramCommandPos, FLOAT paramYPos, FLOW_INFO_COLOUR paramColour)

	// The command
	FLOW_COMMAND_IDS thisCommand = g_flowUnsaved.flowCommands[paramCommandPos].command
	DISPLAY_FLOW_INFO_LITERAL_TEXT(paramColour, FLOW_INFO_DESCRIPTION_XPOS, paramYPos, GET_COMMAND_DISPLAY_STRING_FROM_COMMAND_ID(thisCommand))

	// The Mission Filename
	INT textLargeVarsIndex = g_flowUnsaved.flowCommands[paramCommandPos].index
	DISPLAY_FLOW_INFO_LITERAL_TEXT(paramColour, FLOW_INFO_PARAM1_XPOS, paramYPos, g_flowUnsaved.textLargeVars[textLargeVarsIndex].txtValue)
	
ENDPROC







// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Display the 'Wait_For_Game_Time' message - suitable for all Wait_*_Game_Time commands.
//
// INPUT PARAMS:	paramCommandPos		The position of the command within the commands array
//					paramStrand			The strand that is waiting
//					paramYPos			The screen Y Pos for these details
//					paramColour			The text display colour

PROC DISPLAY_WAIT_FOR_GAME_TIME(INT paramCommandPos, STRANDS paramStrand, FLOAT paramYPos, FLOW_INFO_COLOUR paramColour)

	// The command
	FLOW_COMMAND_IDS thisCommand = g_flowUnsaved.flowCommands[paramCommandPos].command
	DISPLAY_FLOW_INFO_LITERAL_TEXT(paramColour, FLOW_INFO_DESCRIPTION_XPOS, paramYPos, GET_COMMAND_DISPLAY_STRING_FROM_COMMAND_ID(thisCommand))
	
	// Construct a texl label containing the time game datetime we are waiting for.
	TEXT_LABEL_63 tDateTime = ""
	INT iTempInt = GET_TIMEOFDAY_HOUR(g_flowUnsaved.sStrandInGameTimer[paramStrand])
	IF iTempInt < 10
		tDateTime += 0
	ENDIF
	tDateTime += iTempInt
	tDateTime += ":"
	
	iTempInt = GET_TIMEOFDAY_MINUTE(g_flowUnsaved.sStrandInGameTimer[paramStrand])
	IF iTempInt < 10
		tDateTime += 0
	ENDIF
	tDateTime += iTempInt
	tDateTime += ":"
	
	iTempInt = GET_TIMEOFDAY_SECOND(g_flowUnsaved.sStrandInGameTimer[paramStrand])
	IF iTempInt < 10
		tDateTime += 0
	ENDIF
	tDateTime += iTempInt
	tDateTime += "  "
	
	iTempInt = GET_TIMEOFDAY_DAY(g_flowUnsaved.sStrandInGameTimer[paramStrand])
	IF iTempInt < 10
		tDateTime += 0
	ENDIF
	tDateTime += iTempInt
	tDateTime += "/"
	
	iTempInt = ENUM_TO_INT(GET_TIMEOFDAY_MONTH(g_flowUnsaved.sStrandInGameTimer[paramStrand]))
	IF iTempInt < 9
		tDateTime += 0
	ENDIF
	tDateTime += iTempInt + 1
	tDateTime += "/"
	tDateTime += GET_TIMEOFDAY_YEAR(g_flowUnsaved.sStrandInGameTimer[paramStrand])
	
	// Display the game time we are waiting for.
	DISPLAY_FLOW_INFO_LITERAL_TEXT(paramColour, FLOW_INFO_PARAM1_XPOS, paramYPos, tDateTime)
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Display the 'Wait_In_Real_Time' message
//
// INPUT PARAMS:	paramCommandPos		The position of the command within the commands array
//					paramStrand			The strand that is waiting
//					paramYPos			The screen Y Pos for these details
//					paramColour			The text display colour

PROC DISPLAY_WAIT_IN_REAL_TIME(INT paramCommandPos, STRANDS paramStrand, FLOAT paramYPos, FLOW_INFO_COLOUR paramColour)

	// The command
	FLOW_COMMAND_IDS thisCommand = g_flowUnsaved.flowCommands[paramCommandPos].command
	DISPLAY_FLOW_INFO_LITERAL_TEXT(paramColour, FLOW_INFO_DESCRIPTION_XPOS, paramYPos, GET_COMMAND_DISPLAY_STRING_FROM_COMMAND_ID(thisCommand))
	
	// Display the time still left to wait (in milliseconds).
	DISPLAY_FLOW_INFO_TEXTLABEL_WITH_ONE_NUMBER(paramColour, FLOW_INFO_PARAM1_XPOS, paramYPos, "NUMBER", g_flowUnsaved.iStrandTimer[paramStrand] - GET_GAME_TIMER())
	
ENDPROC



// ===========================================================================================================

// PURPOSE:	Display the current command description
//
// INPUT PARAMS:	paramStrandID		The strand ID
//					paramYPos			The screen Y Pos for these details
// RETURN VALUE:	paramColour			The text display colour

FUNC FLOW_INFO_COLOUR DISPLAY_STRAND_CURRENT_COMMAND_DESCRIPTION(STRANDS paramStrandID, FLOAT paramYPos)
	INT thisCommandPos	
	
	if g_bLoadedClifford
		thisCommandPos	= g_savedGlobalsClifford.sFlow.strandSavedVars[paramStrandID].thisCommandPos
	
	elif g_bLoadedNorman
		thisCommandPos	= g_savedGlobalsnorman.sFlow.strandSavedVars[paramStrandID].thisCommandPos
	
	else
		thisCommandPos	= g_savedGlobals.sFlow.strandSavedVars[paramStrandID].thisCommandPos
	endif
	
	BOOL foundTheCommand = FALSE
	FLOW_INFO_COLOUR lineColour = COL_BRIGHT
	
	IF thisCommandPos != -1
	
		FLOW_COMMAND_IDS thisCommand = g_flowUnsaved.flowCommands[thisCommandPos].command
		foundTheCommand = TRUE
		
		SWITCH (thisCommand)
			CASE FLOW_AWAIT_SYNCHRONISATION
				DISPLAY_STRAND_AWAIT_SYNCHRONISATION(thisCommandPos, paramYPos, lineColour)
				BREAK
				
			CASE FLOW_DO_BITSET_JUMPS
				DISPLAY_STRAND_DO_BITSET_JUMPS(thisCommandPos, paramYPos, lineColour)
				BREAK
				
			CASE FLOW_DO_FLAG_JUMPS
				DISPLAY_STRAND_DO_FLAG_JUMPS(thisCommandPos, paramYPos, lineColour)
				BREAK
				
			CASE FLOW_DO_INT_JUMPS
				DISPLAY_STRAND_DO_INT_JUMPS(thisCommandPos, paramYPos, lineColour)
				BREAK
			
			CASE FLOW_DO_MISSION_AT_BLIP
			CASE FLOW_DO_MISSION_NOW
				lineColour = DISPLAY_STRAND_DO_MISSION(thisCommandPos, paramYPos, lineColour)
				BREAK
				
				
			CASE FLOW_DO_STRAND_KILLED_JUMPS
				DISPLAY_STRAND_DO_STRAND_KILLED_JUMPS(thisCommandPos, paramYPos, lineColour)
				BREAK
			
			CASE FLOW_LAUNCH_SCRIPT_AND_FORGET
				DISPLAY_STRAND_LAUNCH_SCRIPT(thisCommandPos, paramYPos, lineColour)
				BREAK
				
			CASE FLOW_WAIT_IN_GAME_TIME
			CASE FLOW_WAIT_UNTIL_GAME_HOUR
			CASE FLOW_WAIT_UNTIL_GAME_TIME_AND_DATE
				DISPLAY_WAIT_FOR_GAME_TIME(thisCommandPos, paramStrandID, paramYPos, lineColour)
				BREAK
				
			CASE FLOW_WAIT_IN_REAL_TIME
				DISPLAY_WAIT_IN_REAL_TIME(thisCommandPos, paramStrandID, paramYPos, lineColour)
				BREAK
		
			// Basic display commands (these have no additional display requirements or are only on-screen for one frame)
			CASE FLOW_ACTIVATE_RANDOMCHAR_MISSION
			CASE FLOW_ACTIVATE_STRAND
			CASE FLOW_AWAIT_ACTIVATION
			CASE FLOW_GOTO
			CASE FLOW_STRAND_GOTO
			CASE FLOW_IS_GAME_TIME_AFTER_HOUR
			CASE FLOW_KILL_STRAND
			CASE FLOW_LABEL
			CASE FLOW_ENABLE_BLIP_LONG_RANGE
			CASE FLOW_DISABLE_BLIP_LONG_RANGE
			CASE FLOW_DISPLAY_HELP_TEXT
			CASE FLOW_CANCEL_HELP_TEXT
			CASE FLOW_SET_BUILDING_STATE
			CASE FLOW_SET_GAME_CLOCK
			CASE FLOW_RUN_CODE_ID
			CASE FLOW_DEBUG_RUN_CODE_ID
			CASE FLOW_SET_WEAPON_LOCK_STATE
			CASE FLOW_SET_STATIC_BLIP_STATE
			CASE FLOW_SEND_SYNCHRONISATION
			CASE FLOW_STORE_BITSET_BIT_STATE
			CASE FLOW_STORE_FLAG_STATE
			CASE FLOW_STORE_INT_VALUE
			CASE FLOW_TERMINATE
			CASE FLOW_DEBUG_DUMMY
			
				DISPLAY_FLOW_INFO_LITERAL_TEXT(lineColour, FLOW_INFO_DESCRIPTION_XPOS, paramYPos, GET_COMMAND_DISPLAY_STRING_FROM_COMMAND_ID(thisCommand))
				BREAK
				
			DEFAULT
				foundTheCommand = DISPLAY_STRAND_CURRENT_COMMAND_DESCRIPTION_CUSTOM(thisCommand, thisCommandPos, paramYPos, lineColour)
				BREAK
		ENDSWITCH
	ENDIF
	
	IF NOT (foundTheCommand)
		DISPLAY_STRAND_UNKNOWN_COMMAND(paramYPos, lineColour)
	ENDIF
	
	RETURN lineColour

ENDFUNC








// -----------------------------------------------------------------------------------------------------------
//		Flow Info Screen Strand Display
// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Display the details for one strand
//
// INPUT PARAMS:	paramStrandID		The Strand ID
//					paramYPos			The screen Y Pos for these details

PROC DISPLAY_STRAND(STRANDS paramStrandID, FLOAT paramYPos)

	// Display strand information
//	DISPLAY_STRAND_BOUNDARIES(paramStrandID, paramYPos, COL_DULL)
	
	FLOW_INFO_COLOUR lineColour = COL_BRIGHT
	IF g_bLoadedClifford
		// Check if the strand has been activated
		IF NOT (IS_BIT_SET(g_savedGlobalsClifford.sFlow.strandSavedVars[paramStrandID].savedBitflags, SAVED_BITS_STRAND_ACTIVATED))
			lineColour = DISPLAY_STRAND_NOT_ACTIVATED(paramYPos)
			DISPLAY_STRAND_NAME(paramStrandID, paramYPos, lineColour)
			EXIT
		ENDIF
		// Check if the strand has been terminated
		IF (IS_BIT_SET(g_savedGlobalsClifford.sFlow.strandSavedVars[paramStrandID].savedBitflags, SAVED_BITS_STRAND_TERMINATED))
			lineColour = DISPLAY_STRAND_TERMINATED(paramYPos)
			DISPLAY_STRAND_NAME(paramStrandID, paramYPos, lineColour)
			EXIT
		ENDIF
		
	ELIF g_bLoadedNorman
		// Check if the strand has been activated
		IF NOT (IS_BIT_SET(g_savedGlobalsnorman.sFlow.strandSavedVars[paramStrandID].savedBitflags, SAVED_BITS_STRAND_ACTIVATED))
			lineColour = DISPLAY_STRAND_NOT_ACTIVATED(paramYPos)
			DISPLAY_STRAND_NAME(paramStrandID, paramYPos, lineColour)
			EXIT
		ENDIF
		// Check if the strand has been terminated
		IF (IS_BIT_SET(g_savedGlobalsnorman.sFlow.strandSavedVars[paramStrandID].savedBitflags, SAVED_BITS_STRAND_TERMINATED))
			lineColour = DISPLAY_STRAND_TERMINATED(paramYPos)
			DISPLAY_STRAND_NAME(paramStrandID, paramYPos, lineColour)
			EXIT
		ENDIF	
		
	ELSE
		// Check if the strand has been activated
		IF NOT (IS_BIT_SET(g_savedGlobals.sFlow.strandSavedVars[paramStrandID].savedBitflags, SAVED_BITS_STRAND_ACTIVATED))
			lineColour = DISPLAY_STRAND_NOT_ACTIVATED(paramYPos)
			DISPLAY_STRAND_NAME(paramStrandID, paramYPos, lineColour)
			EXIT
		ENDIF
		// Check if the strand has been terminated
		IF (IS_BIT_SET(g_savedGlobals.sFlow.strandSavedVars[paramStrandID].savedBitflags, SAVED_BITS_STRAND_TERMINATED))
			lineColour = DISPLAY_STRAND_TERMINATED(paramYPos)
			DISPLAY_STRAND_NAME(paramStrandID, paramYPos, lineColour)
			EXIT
		ENDIF
	ENDIF
	
	// Display command information
	lineColour = DISPLAY_STRAND_CURRENT_COMMAND_DESCRIPTION(paramStrandID, paramYPos)
//	DISPLAY_STRAND_COMMAND_ARRAY_POS(paramStrandID, paramYPos, lineColour)
	
	// Display Strand Name in appropriate colour
	DISPLAY_STRAND_NAME(paramStrandID, paramYPos, lineColour)

ENDPROC




// -----------------------------------------------------------------------------------------------------------
//		Flow Info Screen Display
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Display the titles for the different sections of the Flow Info screen
// 
PROC DISPLAY_TITLES()
	DISPLAY_FLOW_INFO_LITERAL_TEXT(COL_MID, FLOW_INFO_TITLE_FLOW_XPOS, 0.04, "GAMEFLOW STATE")
	DISPLAY_TITLES_CUSTOM()
ENDPROC


// PURPOSE:	Display the details for all strands
// 
PROC DISPLAY_DETAILS()

	INT tempLoop = 0
	STRANDS	loopAsStrandID = STRAND_NONE
	
	FLOAT theY = FLOW_INFO_START_YPOS
	#if USE_CLF_DLC
		REPEAT MAX_STRANDS_CLF tempLoop
			loopAsStrandID = INT_TO_ENUM(STRANDS, tempLoop)			
			DISPLAY_STRAND(loopAsStrandID, theY)			
			theY += FLOW_INFO_ADD_Y
		ENDREPEAT
	#endif	
	#if USE_NRM_DLC
		REPEAT MAX_STRANDS_NRM tempLoop
			loopAsStrandID = INT_TO_ENUM(STRANDS, tempLoop)			
			DISPLAY_STRAND(loopAsStrandID, theY)			
			theY += FLOW_INFO_ADD_Y
		ENDREPEAT	
	#endif
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
		REPEAT MAX_STRANDS tempLoop
			loopAsStrandID = INT_TO_ENUM(STRANDS, tempLoop)			
			DISPLAY_STRAND(loopAsStrandID, theY)			
			theY += FLOW_INFO_ADD_Y
		ENDREPEAT	
	#endif
	#endif
	g_fFlowInfoBackgroundHeight = (theY + FLOW_INFO_ADD_Y*2.0)
ENDPROC


// ===========================================================================================================

// PURPOSE:	Displays the Mission Flow Info screen when allowed by other systems
// 
PROC DISPLAY_FLOW_INFO_SCREEN()
	
	// Check if some other system has hidden this screen
	IF NOT g_flowUnsaved.bShowMissionFlowDebugScreen
		EXIT
	ENDIF

	DISPLAY_TITLES()
	DISPLAY_DETAILS()
	DISPLAY_FLOW_INFO_CUSTOM()
	DISPLAY_BACKGROUND()
ENDPROC
