//
//	script_ped.sch
//
//  Common ped procedures/functions used in the game.

USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_entity.sch"
USING "commands_ped.sch"

// *******************************************************************************************
//  PED TASK FUNCTIONS AND PROCEDURES
// *******************************************************************************************
/// PURPOSE:    
///    
/// PARAMS:     
///    model_name - 
/// AUTHOR:     alwyn.roberts@rockstarnorth.com
/// COMMENTS:   
FUNC BOOL IS_PED_AT_ANGLED_COORD(PED_INDEX pedIndex,
        VECTOR VecCentre, VECTOR VecBounds, FLOAT fAngle,
        BOOL HighlightArea = FALSE, BOOL check3D = TRUE, PED_TRANSPORT_MODE ptm_mode = TM_ANY)
    VECTOR vTemp_one, vTemp_two
    
    vTemp_one.x = VecCentre.x-(COS(fAngle)*VecBounds.x)
    vTemp_one.y = VecCentre.y-(SIN(fAngle)*VecBounds.x)
    vTemp_one.z = VecCentre.z-VecBounds.z
    
    vTemp_two.x = VecCentre.x+(COS(fAngle)*VecBounds.x)
    vTemp_two.y = VecCentre.y+(SIN(fAngle)*VecBounds.x)
    vTemp_two.z = VecCentre.z+VecBounds.z
    
    IF IS_ENTITY_IN_ANGLED_AREA(pedIndex, vTemp_one, vTemp_two,
            VecBounds.y*2.0,        //DistanceFrom1To4,
            HighlightArea, check3D, ptm_mode)
        RETURN TRUE
    ENDIF
    
    RETURN FALSE
ENDFUNC

FUNC BOOL IS_PED_CLOSE_TO_IDEAL_HEADING(PED_INDEX ped, FLOAT idealHeading, FLOAT acceptableRange = 30.0)
	FLOAT upperLimit, lowerLimit
	upperLimit = idealHeading + (acceptableRange/2)
	IF upperLimit > 360
		upperLimit -= 360.0
	ENDIF
	
	lowerLimit = idealHeading - (acceptableRange/2)
	IF lowerLimit < 0
		lowerLimit += 360.0
	ENDIF
	
	IF NOT IS_PED_INJURED(ped)
		IF upperLimit > lowerLimit
			IF GET_ENTITY_HEADING(ped) < upperLimit
			AND GET_ENTITY_HEADING(ped) > lowerLimit
				RETURN TRUE
			ELSE
				RETURN FALSE
			ENDIF
		ELSE
			IF GET_ENTITY_HEADING(ped) < upperLimit
			OR GET_ENTITY_HEADING(ped) > lowerLimit
				RETURN TRUE
			ELSE
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC FLOAT GET_DISTANCE_BETWEEN_ENTITIES(ENTITY_INDEX entityA, ENTITY_INDEX entityB, BOOL bCheck3D = TRUE)

	VECTOR vEntityAPos
	VECTOR vEntityBPos

	IF NOT IS_ENTITY_DEAD(entityA)
		vEntityAPos = GET_ENTITY_COORDS(entityA)
	ELSE
		vEntityAPos = GET_ENTITY_COORDS(entityA, FALSE)
	ENDIF
	IF NOT IS_ENTITY_DEAD(entityB)
		vEntityBPos = GET_ENTITY_COORDS(entityB)
	ELSE
		vEntityBPos = GET_ENTITY_COORDS(entityB, FALSE)
	ENDIF
	
	RETURN GET_DISTANCE_BETWEEN_COORDS(vEntityAPos, vEntityBPos, bCheck3D)

ENDFUNC

FUNC FLOAT GET_DIST2_BETWEEN_ENTITIES(ENTITY_INDEX entityA, ENTITY_INDEX entityB)

	VECTOR vEntityAPos
	VECTOR vEntityBPos

	IF NOT IS_ENTITY_DEAD(entityA)
		vEntityAPos = GET_ENTITY_COORDS(entityA)
	ELSE
		vEntityAPos = GET_ENTITY_COORDS(entityA, FALSE)
	ENDIF
	IF NOT IS_ENTITY_DEAD(entityB)
		vEntityBPos = GET_ENTITY_COORDS(entityB)
	ELSE
		vEntityBPos = GET_ENTITY_COORDS(entityB, FALSE)
	ENDIF
	
	RETURN VDIST2(vEntityAPos, vEntityBPos)

ENDFUNC

FUNC FLOAT GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(ENTITY_INDEX entity1, vector coord, BOOL bCheck3D = TRUE)
	VECTOR vEntity1Pos
	IF NOT IS_ENTITY_DEAD(entity1)
		vEntity1Pos = GET_ENTITY_COORDS(entity1)
	ELSE
		vEntity1Pos = GET_ENTITY_COORDS(entity1, FALSE)
	ENDIF
	
	RETURN GET_DISTANCE_BETWEEN_COORDS(vEntity1Pos, coord, bCheck3D)
ENDFUNC

FUNC FLOAT GET_DIST2_BETWEEN_ENTITY_AND_COORD(ENTITY_INDEX eiEntity, VECTOR vCoord, BOOL bCheck3D = TRUE)
	VECTOR vEntity1Pos
	IF NOT IS_ENTITY_DEAD(eiEntity)
		vEntity1Pos = GET_ENTITY_COORDS(eiEntity)
	ELSE
		vEntity1Pos = GET_ENTITY_COORDS(eiEntity, FALSE)
	ENDIF
	
	IF !bCheck3D
		vEntity1Pos.z = 0.0
		vCoord.z = 0.0
	ENDIF
	
	RETURN VDIST2(vEntity1Pos, vCoord)
ENDFUNC

FUNC FLOAT GET_DISTANCE_BETWEEN_PEDS(PED_INDEX ped1, PED_INDEX ped2)
	RETURN(GET_DISTANCE_BETWEEN_ENTITIES(ped1,ped2))
ENDFUNC

/// PURPOSE: Sets a peds max health and scales their current health to match
PROC SET_PED_MAX_HEALTH_WITH_SCALE(PED_INDEX ped, INT iMaxHealth)
	IF NOT IS_PED_INJURED(ped)
		// Store current health as a percentage
		FLOAT fHealthPerc = (((TO_FLOAT(GET_ENTITY_HEALTH(ped))-100.0) / (TO_FLOAT(GET_PED_MAX_HEALTH(ped))-100.0)) * 100.0) // Health under 100 is considered dead so remove margin
		
		// Set new max health
		SET_PED_MAX_HEALTH(ped, iMaxHealth)
		
		// Set new health percentage
		SET_ENTITY_HEALTH(ped, ROUND(((fHealthPerc / 100.0) * (TO_FLOAT(GET_PED_MAX_HEALTH(ped)) - 100.0)) + 100.0))
	
	ENDIF
ENDPROC

/// PURPOSE:
///    Returns VEHICLE_SEAT enum for the specified ped in specified vehicle. Make sure the specified ped is already sitting in the specified vehicle.
/// PARAMS:
///    PedIndex - Ped to check.
///    VehicleIndex - Vehicle to check.
/// RETURNS:
///    VEHICLE_SEAT enum that is taken by specified ped in specified vehicle. Returns VS_ANY_PASSENGER as default if no seat is found.
FUNC VEHICLE_SEAT GET_PED_VEHICLE_SEAT(PED_INDEX PedIndex, VEHICLE_INDEX VehicleIndex)
    
	IF 	NOT IS_ENTITY_DEAD(PedIndex)
	AND	NOT IS_ENTITY_DEAD(VehicleIndex)

		IF IS_PED_SITTING_IN_VEHICLE(PedIndex, VehicleIndex)

			IF GET_PED_IN_VEHICLE_SEAT(VehicleIndex, VS_DRIVER) = PedIndex
			  RETURN VS_DRIVER
			ENDIF

			IF GET_PED_IN_VEHICLE_SEAT(VehicleIndex, VS_FRONT_RIGHT) = PedIndex
			  RETURN VS_FRONT_RIGHT
			ENDIF

			IF GET_PED_IN_VEHICLE_SEAT(VehicleIndex, VS_BACK_LEFT) = PedIndex
			  RETURN VS_BACK_LEFT
			ENDIF

			IF GET_PED_IN_VEHICLE_SEAT(VehicleIndex, VS_BACK_RIGHT) = PedIndex
			  RETURN VS_BACK_RIGHT
			ENDIF
			
		ELSE
			
			#IF IS_DEBUG_BUILD
				SCRIPT_ASSERT("Ped is not sitting in vehicle. Check if ped is sitting in vehicle first before calling GET_PED_VEHICLE_SEAT().")
			#ENDIF
		
		ENDIF
		 
	ENDIF
     
    RETURN VS_ANY_PASSENGER
     
ENDFUNC

/// PURPOSE:
///    Checks if specified ped is sitting in specified vehicle seat in specified vehicle.
/// PARAMS:
///    PedIndex - Ped to check.
///    VehicleIndex - Vehicle to check.
///    eVehicleSeat - VEHICLE_SEAT enum to check.
/// RETURNS:
///    Returns TRUE if the ped is sitting in specified seat in specified vehicle. Returns FALSE otherwise.
FUNC BOOL IS_PED_SITTING_IN_VEHICLE_SEAT(PED_INDEX PedIndex, VEHICLE_INDEX VehicleIndex, VEHICLE_SEAT eVehicleSeat)

	IF 	NOT IS_ENTITY_DEAD(PedIndex)
	AND	NOT IS_ENTITY_DEAD(VehicleIndex)
	
		IF IS_PED_SITTING_IN_VEHICLE(PedIndex, VehicleIndex)
	
			IF GET_PED_IN_VEHICLE_SEAT(VehicleIndex, eVehicleSeat) = PedIndex
				RETURN TRUE
			ENDIF
			
		ENDIF
	
	ENDIF

	RETURN FALSE

ENDFUNC

/// PURPOSE:
///    Checks if specified ped is in specified vehicle with entity exist and death checks.
/// PARAMS:
///    PedIndex - Ped to check.
///    VehicleIndex - Vehicle to check.
///    bConsiderEnteringAsInVehicle - Should entering the vehicle be considered as in vehicle.
/// RETURNS:
///    TRUE if ped and vehicle exist, they are not dead and ped is in the vehicle. FALSE if otherwise.
FUNC BOOL IS_PED_IN_THIS_VEHICLE(PED_INDEX PedIndex, VEHICLE_INDEX VehicleIndex, BOOL bConsiderEnteringAsInVehicle = FALSE)
	
	IF 	DOES_ENTITY_EXIST(PedIndex)
	AND NOT IS_ENTITY_DEAD(PedIndex)
		IF 	DOES_ENTITY_EXIST(VehicleIndex)
		AND	NOT IS_ENTITY_DEAD(VehicleIndex)
		AND IS_ENTITY_A_VEHICLE(VehicleIndex)
			IF IS_PED_IN_VEHICLE(PedIndex, VehicleIndex, bConsiderEnteringAsInVehicle)
				RETURN TRUE
			ENDIF
	
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

/// PURPOSE:
///    Checks if specified ped is sitting in specified vehicle with entity exist and death checks. 
/// PARAMS:
///    PedIndex - Ped to check.
///    VehicleIndex - Vehicle to check.
/// RETURNS:
///    TRUE if ped and vehicle exist, they are not dead and ped is sitting in the vehicle. FALSE if otherwise. 
FUNC BOOL IS_PED_SITTING_IN_THIS_VEHICLE(PED_INDEX PedIndex, VEHICLE_INDEX VehicleIndex)
	
	IF 	DOES_ENTITY_EXIST(PedIndex)
	AND NOT IS_ENTITY_DEAD(PedIndex)
		IF 	DOES_ENTITY_EXIST(VehicleIndex)
		AND	NOT IS_ENTITY_DEAD(VehicleIndex)
	
			IF IS_PED_SITTING_IN_VEHICLE(PedIndex, VehicleIndex)
				RETURN TRUE
			ENDIF
	
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

/// PURPOSE:
///    Checks if specified peds exist, are not dead and are in the same vehicle.
/// PARAMS:
///    PedIndex1 - First ped to check.
///    PedIndex2 - Second ped to check.
/// RETURNS:
///    TRUE if both peds exist, they are not dead and they are both in the same vehicle.
FUNC BOOL ARE_PEDS_IN_THE_SAME_VEHICLE(PED_INDEX PedIndex1, PED_INDEX PedIndex2)

	IF 	DOES_ENTITY_EXIST(PedIndex1)
	AND	DOES_ENTITY_EXIST(PedIndex2)
		IF 	NOT IS_ENTITY_DEAD(PedIndex1)
		AND	NOT IS_ENTITY_DEAD(PedIndex2)
		
			IF 	IS_PED_IN_ANY_VEHICLE(PedIndex1)
			AND IS_PED_IN_ANY_VEHICLE(PedIndex2)
		
				VEHICLE_INDEX VehicleIndex1, VehicleIndex2
			
				VehicleIndex1 = GET_VEHICLE_PED_IS_IN(PedIndex1)
				VehicleIndex2 = GET_VEHICLE_PED_IS_IN(PedIndex2)
			
				IF 	DOES_ENTITY_EXIST(VehicleIndex1)
				AND	DOES_ENTITY_EXIST(VehicleIndex2)
							
					IF ( VehicleIndex1 = VehicleIndex2 )
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	RETURN FALSE

ENDFUNC

/// PURPOSE: 
///    Checks if a script task is running or is waiting to start
///  PARAMS:
///    pedIndex - Ped to check 
///    task - Script task to check
///  RETURNS:
///    	Returns TRUE if the task is running or waiting to start on the given ped
FUNC BOOL IS_SCRIPT_TASK_RUNNING_OR_STARTING(PED_INDEX pedIndex, SCRIPT_TASK_NAME task, BOOL bUseStrictChecks = TRUE)
	SCRIPTTASKSTATUS sts = GET_SCRIPT_TASK_STATUS(pedIndex, task)
	IF sts = PERFORMING_TASK 
	OR sts = WAITING_TO_START_TASK
		RETURN TRUE
	ELIF NOT bUseStrictChecks
		IF sts = DORMANT_TASK
		OR sts = VACANT_STAGE
		//OR sts = FINISHED_TASK
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Sets visual field angles and ranges for specified ped.
/// PARAMS:
///    PedIndex - Ped index to change.
///    fSeeingRange - How far the vision extends.
///    fPeripheralSeeingRange - How far the peripheral vision extends.
///    fCenterAngle - The angle that defines field of view.
///    fMinAngle - The minimum angle that defines peripheral field of view.
///    fMaxAngle - The maximum angle that defines peripheral field of view.
/// NOTES:
///    Remember to restore ped's default senses when ped goes into AI combat for optimal AI combat behaviour.
PROC SET_PED_VISUAL_FIELD_PROPERTIES(PED_INDEX PedIndex, FLOAT fSeeingRange = 60.0, FLOAT fPeripheralSeeingRange = 5.0,
                                                      FLOAT fCenterAngle = 120.0, FLOAT fMinAngle = -90.0, FLOAT fMaxAngle = 90.0)
      IF NOT IS_PED_INJURED(PedIndex)
            SET_PED_SEEING_RANGE(PedIndex, fSeeingRange)
            SET_PED_VISUAL_FIELD_PERIPHERAL_RANGE(PedIndex, fPeripheralSeeingRange)
            SET_PED_VISUAL_FIELD_CENTER_ANGLE(PedIndex, fCenterAngle / 2.0)
            SET_PED_VISUAL_FIELD_MIN_ANGLE(PedIndex, fMinAngle)
            SET_PED_VISUAL_FIELD_MAX_ANGLE(PedIndex, fMaxAngle)
      ENDIF
ENDPROC

/// PURPOSE:
///    Returns the the closest ped from a specified relationship group to the specified ped
/// PARAMS:
///    PedIndex - Ped index to check for closest peds.
///    RelGroupHash - Relationship group to check for.
///    iProximity - Which ped from the closest peds should be returned.
///    bOnScreen - Checks if peds are on screen when searching for closest peds.
///    ExclusionPedType - Which PED_TYPE to exclude from the search.
/// RETURNS:
///    Returns PED_INDEX of the closest ped if found, NULL if otherwise.
FUNC PED_INDEX GET_PED_CLOSEST_PED_FROM_RELATIONSHIP_GROUP(PED_INDEX PedIndex, REL_GROUP_HASH RelGroupHash, INT iProximity = 0,
														   BOOL bOnScreen = FALSE, PED_TYPE ExclusionPedType = PEDTYPE_ANIMAL
														   #IF IS_DEBUG_BUILD, BOOL bPrintOutput = FALSE #ENDIF)
	
	PED_INDEX pedArray[16]
	
	INT i = 0
		
	IF NOT IS_PED_INJURED(PedIndex)
	
		GET_PED_NEARBY_PEDS(PedIndex, pedArray, ExclusionPedType)						//peds are already sorted by closest distance
		
		FOR i = 0 TO ( COUNT_OF(pedArray) - 1 )
		
			IF DOES_ENTITY_EXIST(pedArray[i])
			
				IF NOT IS_PED_INJURED(pedArray[i])
				
					IF ( GET_PED_RELATIONSHIP_GROUP_HASH(pedArray[i]) = RelGroupHash )
				
						IF ( iProximity <= 0)

							IF ( bOnScreen = TRUE )
						
								IF IS_ENTITY_ON_SCREEN(pedArray[i])

									#IF IS_DEBUG_BUILD
										DRAW_DEBUG_LINE(GET_ENTITY_COORDS(PedIndex), GET_ENTITY_COORDS(pedArray[i]), 255, 255, 0)
										IF ( bPrintOutput = TRUE )
											PRINTLN(GET_THIS_SCRIPT_NAME(), ": Closest nearby ped on screen has index ", i, ".")
										ENDIF
									#ENDIF

									RETURN pedArray[i]

								ENDIF
					
							ELSE

								#IF IS_DEBUG_BUILD
									DRAW_DEBUG_LINE(GET_ENTITY_COORDS(PedIndex), GET_ENTITY_COORDS(pedArray[i]), 255, 255, 0)
									IF ( bPrintOutput = TRUE )
										PRINTLN(GET_THIS_SCRIPT_NAME(), ": Closest nearby ped has index ", i, ".")
									ENDIF
								#ENDIF

								RETURN pedArray[i]
								
							ENDIF
							
						ELSE
						
							IF ( i + iProximity <= COUNT_OF(pedArray) - 1 )
							
								IF DOES_ENTITY_EXIST(pedArray[i + iProximity])
			
									IF NOT IS_PED_INJURED(pedArray[i + iProximity])
									
										IF ( GET_PED_RELATIONSHIP_GROUP_HASH(pedArray[i + iProximity]) = RelGroupHash )
									
											IF ( bOnScreen = TRUE )
										
												IF IS_ENTITY_ON_SCREEN(pedArray[i + iProximity])

													#IF IS_DEBUG_BUILD
														DRAW_DEBUG_LINE(GET_ENTITY_COORDS(PedIndex), GET_ENTITY_COORDS(pedArray[i + iProximity]), 255, 255, 0)
														IF ( bPrintOutput = TRUE )
															PRINTLN(GET_THIS_SCRIPT_NAME(), ": Nearby ped on screen offset by ", iProximity, " from closest nearby ped has index ", i + iProximity, ".")
														ENDIF
													#ENDIF

													RETURN pedArray[i + iProximity]

												ENDIF
									
											ELSE

												#IF IS_DEBUG_BUILD
													DRAW_DEBUG_LINE(GET_ENTITY_COORDS(PedIndex), GET_ENTITY_COORDS(pedArray[i + iProximity]), 255, 255, 0)
													IF ( bPrintOutput = TRUE )
														PRINTLN(GET_THIS_SCRIPT_NAME(), ": Nearby ped offset by ", iProximity, " from closest nearby ped has index ", i + iProximity, ".")
													ENDIF
												#ENDIF

												RETURN pedArray[i + iProximity]
												
											ENDIF
																		
										ENDIF
									
									ENDIF
								
								ENDIF
							
							ENDIF
							
						ENDIF
													
					ENDIF
				
				ENDIF
			
			ENDIF
	
		ENDFOR
		
	ENDIF
	
	RETURN NULL

ENDFUNC

