USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_audio.sch"
USING "commands_entity.sch"
USING "commands_player.sch"

// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//      MISSION NAME    :   script_player.sch
//      AUTHOR          :   Keith
//      DESCRIPTION     :   Contains useful script player functions required by
//                              muliple scripts.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

/// PURPOSE:
///    Gets the current player's Group Index
/// RETURNS:
///    GROUP_INDEX:     GROUP_INDEX for the current player
/// NOTES:
///    Used in Singleplayer
FUNC GROUP_INDEX PLAYER_GROUP_ID()
    //RETURN GET_PLAYER_GROUP(INT_TO_PLAYERINDEX(GET_PLAYER_ID()))
    RETURN GET_PLAYER_GROUP(GET_PLAYER_INDEX())

ENDFUNC

// ----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Performs various checks to make sure it is safe for the player to start a scripted cutscene.
/// PARAMS:
///    DoDeathChecks - Do the death checks as part of this command?
/// RETURNS:
///    BOOL         TRUE if it is safe to start the cutscene, otherwise FALSE
/// NOTES:
///    Also calls the code commands: IS_PLAYER_READY_FOR_CUTSCENE() and CAN_PLAYER_START_MISSION()
FUNC BOOL CAN_PLAYER_START_CUTSCENE(BOOL bDoDeathChecks = true, BOOL bRequiresVehicle = FALSE, bool bDoPassengerCheck = true)

    IF (IS_MINIGAME_IN_PROGRESS())
        RETURN FALSE
    ENDIF
    
    /*REMOVED 16.09.11 This command seems to be getting used in peculiar places for things that aren't cutscenes.
    SET_SCRIPTS_SAFE_FOR_CUTSCENE also cleaned up cellphone and conversations, so we will rely on that only.

    //Steve T cellphone and dialogue checks.
    //Make sure any cellphone call is terminated and the on_screen cellphone is put away before starting the cutscene.

    //This is essentially a hang_up_and_put_away_phone routine.
    
    IF (ENUM_TO_INT(g_Cellphone.PhoneDS)) > (ENUM_TO_INT(PDS_AWAY)) //If phone is onscreen...

        IF IS_MOBILE_PHONE_CALL_ONGOING()
            STOP_SCRIPTED_CONVERSATION (FALSE) //don't finish last line of conversation as this is a call....
        ENDIF
        g_ConversationStatus = CONV_STATE_HANGUPAWAY //tell dialogue handler to clean up a call and phone using HungupAwayCleanup specifically.
        RETURN FALSE

    ENDIF
    */
    
    IF bDoDeathChecks
        IF IS_ENTITY_DEAD(PLAYER_PED_ID())
            RETURN FALSE
        ENDIF
    ENDIF

  
    // vehicle checks
    VEHICLE_INDEX theCar = NULL


    IF (IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()))
        // If the player is not sitting in the vehicle, don't start the cutscene
        IF NOT (IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID()))
            RETURN FALSE
        ENDIF
        
        // If the player is not the driver then he must be a passenger, don't start the cutscene
        theCar = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
        IF bDoDeathChecks
            IF IS_ENTITY_DEAD(theCar)
                RETURN FALSE
            ENDIF
        ENDIF
        
		// if the player is in a car, make sure he is the driver
        // (to avoid triggering cutscene when player arrives as taxi passenger)
		if bDoPassengerCheck
			IF NOT (IS_ENTITY_DEAD(theCar))
	            IF (GET_PED_IN_VEHICLE_SEAT(theCar, VS_DRIVER) != PLAYER_PED_ID())
	                RETURN FALSE
	            ENDIF
	        ENDIF
		endif 

		// AMB This is an awkward place for CONSTs, these should be moved out, either to the top of this file, or some other common location
        // Unless the vehicle is pretty much upright, don't start the cutscene
        CONST_FLOAT MINIMUM_UPRIGHT_VALUE   0.950
        CONST_FLOAT MAXIMUM_UPRIGHT_VALUE   1.011
        
        IF NOT (IS_ENTITY_DEAD(theCar))
            IF (GET_ENTITY_UPRIGHT_VALUE(theCar) < MINIMUM_UPRIGHT_VALUE)
            OR (GET_ENTITY_UPRIGHT_VALUE(theCar) > MAXIMUM_UPRIGHT_VALUE)
                RETURN FALSE
            ENDIF
        ENDIF
		
	ELIF bRequiresVehicle
		RETURN FALSE
    ENDIF


    IF NOT (IS_PLAYER_READY_FOR_CUTSCENE(PLAYER_ID()))
        RETURN FALSE 
    ENDIF


    IF NOT (CAN_PLAYER_START_MISSION(PLAYER_ID()))
        RETURN FALSE 
    ENDIF
    
    
    // Everything ok
    RETURN TRUE

ENDFUNC





