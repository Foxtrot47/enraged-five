//╒═════════════════════════════════════════════════════════════════════════════╕
//│				Author:		Ben Rollinson				Date: 04/11/10			│
//│				Previous:	Ryan Paradis										│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│						     Clock Global Variables								│
//│																				│
//│			Contains all data structures required to define dates and			│
//│			times within scripts.												│
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛

ENUM TIMEOFDAY
	// INVALID_TIMEOFDAY = 31:63:63 on Februray 31, -31 years
	INVALID_TIMEOFDAY = -15
ENDENUM
