//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	script_buttons.sch											//
//		AUTHOR			:	Kenneth Ross												//
//		DESCRIPTION		:	Contains functions to query the state of special buttons.	//
//							This includes input checks for menu accept, menu decline 	//
//							and cutscene skip.											//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"
USING "commands_pad.sch"
USING "commands_camera.sch"

CONST_INT CUTSCENE_SKIP_DELAY	1000

//////////////////////////////////////////////////////////////////////////////
///    
///    MENU ACCEPT
///    

/// PURPOSE: Checks to see if player is currently pressing the menu accept
/// RETURNS: TRUE if the player is pressing the button, FALSE if they are not
FUNC BOOL IS_MENU_ACCEPT_BUTTON_PRESSED()
	
	IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_FRONTEND_ACCEPT)
	OR IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

/// PURPOSE: Checks to see if player has just pressed the menu accept button
/// RETURNS: TRUE if the player has just pressed the button, FALSE if they have not
FUNC BOOL IS_MENU_ACCEPT_BUTTON_JUST_PRESSED()
	
	IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_FRONTEND_ACCEPT)
	OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC



//////////////////////////////////////////////////////////////////////////////
///    
///    MENU DECLINE
///    

/// PURPOSE: Checks to see if player is currently pressing the menu decline
/// RETURNS: TRUE if the player is pressing the button, FALSE if they are not
FUNC BOOL IS_MENU_DECLINE_BUTTON_PRESSED()

	IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_FRONTEND_CANCEL)
	OR IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

/// PURPOSE: Checks to see if player has just pressed the menu decline button
/// RETURNS: TRUE if the player has just pressed the button, FALSE if they have not
FUNC BOOL IS_MENU_DECLINE_BUTTON_JUST_PRESSED()
	
	IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_FRONTEND_CANCEL)
	OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC



//////////////////////////////////////////////////////////////////////////////
///    
///    CUTSCENE SKIP
///    

/// PURPOSE: Checks to see if player is pressing the cutscene skip button
/// RETURNS: TRUE if the player is pressing the button, FALSE if they are not
FUNC BOOL IS_CUTSCENE_SKIP_BUTTON_PRESSED()
	IF IS_PAUSE_MENU_ACTIVE()
		RETURN FALSE
	ENDIF
	IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SKIP_CUTSCENE)
	OR IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_SKIP_CUTSCENE)
	#IF IS_DEBUG_BUILD
	OR IS_KEYBOARD_KEY_PRESSED(KEY_J)
	#ENDIF
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

/// PURPOSE: Checks to see if player has just pressed the cutscene skip button
/// RETURNS: TRUE if the player has just pressed the button, FALSE if they have not
FUNC BOOL IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED()
	IF IS_PAUSE_MENU_ACTIVE()
		RETURN FALSE
	ENDIF
	IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SKIP_CUTSCENE)
	OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SKIP_CUTSCENE)
	#IF IS_DEBUG_BUILD
	OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
	#ENDIF
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

/// PURPOSE: Checks to see if player has just pressed the cutscene skip button
/// RETURNS: TRUE if the player has just pressed the button, FALSE if they have not
FUNC BOOL IS_CUTSCENE_SKIP_BUTTON_JUST_RELEASED()
	IF IS_PAUSE_MENU_ACTIVE()
		RETURN FALSE
	ENDIF
	IF IS_CONTROL_JUST_RELEASED(PLAYER_CONTROL, INPUT_SKIP_CUTSCENE)
	OR IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_SKIP_CUTSCENE)
	#IF IS_DEBUG_BUILD
	OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
	#ENDIF
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC


/// PURPOSE: Checks to see if player has just pressed the cutscene skip button after a short delay. Avoiding accidental cutscene skips.
/// RETURNS: TRUE if the player has just pressed the button, FALSE if they have not
FUNC BOOL IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY(INT skipDelay = CUTSCENE_SKIP_DELAY)

	IF IS_SCREEN_FADED_IN()
		//Reset time if time has passed
		IF (GET_GAME_TIMER() - gCutsceneSkipLastCalledTime) > skipDelay
			gCutsceneSkipStartTime = GET_GAME_TIMER()
		ENDIF

		gCutsceneSkipLastCalledTime = GET_GAME_TIMER()

		IF (GET_GAME_TIMER() - gCutsceneSkipStartTime) > skipDelay
			IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED()
				gCutsceneSkipStartTime = GET_GAME_TIMER()	//Force reset, for Ryan's problem.
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF 
	
	RETURN FALSE
	
ENDFUNC

/// PURPOSE:Converts GET_POSITION_OF_ANALOGUE_STICKS to GET_CONTROL_VALUE
/// PARAMS:
///    ctPassed - They control type to check for
///    iLx - Return LEFT  X
///    iLy - Return LEFT  Y 
///    iRx - Return RIGHT X
///    iRy - Return RIGHT Y
PROC GET_CONTROL_VALUE_OF_ANALOGUE_STICKS(INT &iLx, INT &iLy, INT &iRx, INT &iRy, BOOL bCheckDisabledControls = FALSE)
	
	iLx = FLOOR(GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_X) * 127)
	iLy = FLOOR(GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y) * 127)
	iRx = FLOOR(GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_X) * 127)
	iRy = FLOOR(GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_Y) * 127)
	
	IF bCheckDisabledControls
		IF iLx = 0.0 AND iLy = 0.0
			iLx = FLOOR(GET_DISABLED_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_X) * 127)
			iLy = FLOOR(GET_DISABLED_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y) * 127)
		ENDIF
		IF iRx = 0.0 AND iRy = 0.0
			iRx = FLOOR(GET_DISABLED_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_X) * 127)
			iRy = FLOOR(GET_DISABLED_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_Y) * 127)
		ENDIF
	ENDIF
		
ENDPROC

/// PURPOSE:Converts GET_POSITION_OF_ANALOGUE_STICKS to GET_CONTROL_VALUE, USING UNBOUND VALUES SO PC MOUSE SUPPORT WORKS PROPERLY.
/// PARAMS:
///    ctPassed - They control type to check for
///    iLx - Return LEFT  X
///    iLy - Return LEFT  Y 
///    iRx - Return RIGHT X
///    iRy - Return RIGHT Y
PROC GET_CONTROL_VALUE_OF_ANALOGUE_STICKS_UNBOUND(INT &iLx, INT &iLy, INT &iRx, INT &iRy, BOOL bCheckDisabledControls = FALSE, BOOL bDisableMouseInvert = FALSE )
	
	iLx = FLOOR(GET_CONTROL_UNBOUND_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_X) * 127)
	iLy = FLOOR(GET_CONTROL_UNBOUND_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y) * 127)
	iRx = FLOOR(GET_CONTROL_UNBOUND_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_X) * 127)
	iRy = FLOOR(GET_CONTROL_UNBOUND_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_Y) * 127)
	
	IF bCheckDisabledControls
		IF NOT IS_CONTROL_ENABLED(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_X)
			iLx = FLOOR(GET_DISABLED_CONTROL_UNBOUND_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_X) * 127)
		ENDIF
		IF NOT IS_CONTROL_ENABLED(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y)
			iLy = FLOOR(GET_DISABLED_CONTROL_UNBOUND_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y) * 127)
		ENDIF

		IF NOT IS_CONTROL_ENABLED(FRONTEND_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_X)
			iRx = FLOOR(GET_DISABLED_CONTROL_UNBOUND_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_X) * 127)
		ENDIF
		IF NOT IS_CONTROL_ENABLED(FRONTEND_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_Y)
			iRy = FLOOR(GET_DISABLED_CONTROL_UNBOUND_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_Y) * 127)
		ENDIF
	ENDIF
		
	// In some circumstances we don't want the script look up/down equivalent input to invert on PC - B* 1842661
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
	
		IF bDisableMouseInvert
	
			IF IS_LOOK_INVERTED()
				iRY *= -1
			ENDIF
		
			IF IS_MOUSE_LOOK_INVERTED()
				iRY *= -1
			ENDIF
		
		ENDIF
	
	ENDIF
		
ENDPROC

