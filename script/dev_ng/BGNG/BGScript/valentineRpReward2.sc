//Commands
USING "rage_builtins.sch"
USING "commands_script.sch"
USING "commands_network.sch"
USING "commands_dlc.sch"

//Globals
USING "globals.sch"

//Common Helpers
USING "Bg_Common_Helpers.sch"

STRING sVersionNumber = "1.48.00"

BOOL bActivatedPrint
INT iActivatePrintProgress

PROC DISPLAY_TEXT_WITH_LITERAL_STRING_FOR_RELEASE_F9(FLOAT DisplayAtX, FLOAT DisplayAtY, STRING pTextLabel, STRING pLiteralString)
	BEGIN_TEXT_COMMAND_DISPLAY_TEXT(pTextLabel)
		ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY(pLiteralString)
	END_TEXT_COMMAND_DISPLAY_TEXT(DisplayAtX, DisplayAtY)
ENDPROC

PROC MAINTAIN_CHECK_TO_ACTIVATE_PRINT()
	IF (bActivatedPrint)
		SET_TEXT_SCALE(0.0000, 0.23)
    	SET_TEXT_COLOUR(255,255,255, 255)   
    	DISPLAY_TEXT_WITH_LITERAL_STRING_FOR_RELEASE_F9(0.69, 0.06, "STRING", sVersionNumber)
	ENDIF			

	IF NOT (bActivatedPrint)
		
		IF IS_PAUSE_MENU_ACTIVE()
			SWITCH iActivatePrintProgress
				CASE 0
					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
						iActivatePrintProgress++
						SETTIMERA(0)
					ENDIF
				BREAK
				CASE 1
					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
						iActivatePrintProgress++
						SETTIMERA(0)
					ELIF TIMERA() > 2000
						iActivatePrintProgress = 0
					ENDIF
				BREAK
				CASE 2
					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
						iActivatePrintProgress++
						SETTIMERA(0)
					ELIF TIMERA() > 2000
						iActivatePrintProgress = 0
					ENDIF
				BREAK
				CASE 3
					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
						iActivatePrintProgress++
						SETTIMERA(0)
					ELIF TIMERA() > 2000
						iActivatePrintProgress = 0
					ENDIF
				BREAK
				CASE 4
					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
						bActivatedPrint = TRUE
						SETTIMERA(0)
					ELIF TIMERA() > 2000
						iActivatePrintProgress = 0
					ENDIF
				BREAK
			ENDSWITCH
		ENDIF
	ELSE
		IF IS_PAUSE_MENU_ACTIVE()
			SWITCH iActivatePrintProgress
				CASE 0
					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
						iActivatePrintProgress++
						SETTIMERA(0)
					ENDIF
				BREAK
				CASE 1
					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
						iActivatePrintProgress++
						SETTIMERA(0)
					ELIF TIMERA() > 2000
						iActivatePrintProgress = 0
					ENDIF
				BREAK
				CASE 2
					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
						iActivatePrintProgress++
						SETTIMERA(0)
					ELIF TIMERA() > 2000
						iActivatePrintProgress = 0
					ENDIF
				BREAK
				CASE 3
					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
						iActivatePrintProgress++
						SETTIMERA(0)
					ELIF TIMERA() > 2000
						iActivatePrintProgress = 0
					ENDIF
				BREAK
				CASE 4
					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
						bActivatedPrint = FALSE
						SETTIMERA(0)
					ELIF TIMERA() > 2000
						iActivatePrintProgress = 0
					ENDIF
				BREAK
			ENDSWITCH
		ENDIF
		
	ENDIF
	
ENDPROC

//url:bugstar:3290469 - BG Script that amends the tunable values for vehicle sales limiter
PROC FIX_FOR_3290469()
	IF g_sMPTunables.iThreshold_no_of_minutes != 120
		g_sMPTunables.iThreshold_no_of_minutes = 120
		PRINTLN("FIX_FOR_3290469 - THRESHOLD_NO_OF_MINUTES set to 120 minutes")
	ENDIF
ENDPROC
//url:bugstar:5092622 - [BG SCRIPT][PUBLIC] [EXPLOIT] Frozen money - Director mode, join friend, suspend app.
PROC FIX_FOR_5092622()
	IF g_bDirectorModeRunning
	AND	(GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("director_mode")) <= 0)
	AND IS_SKYSWOOP_AT_GROUND()
		PRINTLN("[FIX_FOR_5092622] - Detected MP launch with Director Mode not cleaned up properly, quitting to SP")
		SHUTDOWN_AND_LAUNCH_SINGLE_PLAYER_GAME()	
	ENDIF
ENDPROC
//url:bugstar:3280561 - [BG SCRIPT][PUBLIC][REPORTED] "Insurance Fraud" Mod is causing players to be put into Bad Sport lobbies and lose GTA$
PROC FIX_FOR_3280561()
	IF IS_PC_VERSION()
		INT iPlayer
		REPEAT COUNT_OF(GlobalplayerBD_FM) iPlayer
			SET_BIT(GlobalplayerBD_FM[iPlayer].iPersonalVehicleFlagsBS, PERSONAL_VEHICLE_BD_FLAG_IN_VEHICLE_WITH_BOUNTY)
		ENDREPEAT
	ENDIF
ENDPROC

BOOL bDoneEnableDecreasingDupeValueCheck
//url:bugstar:3533670 - [BG SCRIPT ONLY] Decreasing dupe sell values for repeat offenders.
FUNC BOOL IS_REPEAT_OFFENDER_1()
	INT iCurrentPeak = GET_MP_INT_PLAYER_STAT(MPPLY_CHAR_PEAK_EXPLOIT_LEVEL)
	IF (iCurrentPeak <  g_sMPTunables.iRepeatOffenderMinExploitLevel1)
		RETURN(FALSE)
	ENDIF
	INT iPrevious[4]
	INT i	
	iPrevious[0] = GET_MP_INT_PLAYER_STAT(MPPLY_PREVSEASONEXPLOITLEVEL)
	iPrevious[1] = GET_MP_INT_PLAYER_STAT(MPPLY_PREVSEASON2EXPLOITLEVEL)
	iPrevious[2] = GET_MP_INT_PLAYER_STAT(MPPLY_PREVSEASON3EXPLOITLEVEL)
	iPrevious[3] = GET_MP_INT_PLAYER_STAT(MPPLY_PREVSEASON4EXPLOITLEVEL)
	REPEAT (g_sMPTunables.iRepeatOffenderNumSeasons1-1) i
		IF (iPrevious[i] < g_sMPTunables.iRepeatOffenderMinExploitLevel1)
			RETURN(FALSE)
		ENDIF
	ENDREPEAT
	RETURN(TRUE)
ENDFUNC

FUNC BOOL IS_REPEAT_OFFENDER_2()
	
	INT iCurrentPeak = GET_MP_INT_PLAYER_STAT(MPPLY_CHAR_PEAK_EXPLOIT_LEVEL)
	
	IF (iCurrentPeak <  g_sMPTunables.iRepeatOffenderMinExploitLevel2)
		RETURN(FALSE)
	ENDIF
	
	INT iPrevious[4]
	INT i
	iPrevious[0] = GET_MP_INT_PLAYER_STAT(MPPLY_PREVSEASONEXPLOITLEVEL)
	iPrevious[1] = GET_MP_INT_PLAYER_STAT(MPPLY_PREVSEASON2EXPLOITLEVEL)
	iPrevious[2] = GET_MP_INT_PLAYER_STAT(MPPLY_PREVSEASON3EXPLOITLEVEL)
	iPrevious[3] = GET_MP_INT_PLAYER_STAT(MPPLY_PREVSEASON4EXPLOITLEVEL)
	REPEAT (g_sMPTunables.iRepeatOffenderNumSeasons2-1) i
		IF (iPrevious[i] < g_sMPTunables.iRepeatOffenderMinExploitLevel2)
			RETURN(FALSE)
		ENDIF
	ENDREPEAT	
	RETURN(TRUE)
ENDFUNC
PROC FIX_FOR_3533670()	
	IF NOT (bDoneEnableDecreasingDupeValueCheck)	
		IF (g_FinishedFreemodeInit)		
			IF IS_REPEAT_OFFENDER_1()
			OR IS_REPEAT_OFFENDER_2()
				g_sMPTunables.bEnabled_GET_CARMOD_SELL_VALUE_apply_dupe_multiplier = TRUE
				PRINTLN("ENABLE_DECREASING_DUPE_VALUES - repeat offender, activating.")
			ELSE
				g_sMPTunables.bEnabled_GET_CARMOD_SELL_VALUE_apply_dupe_multiplier = FALSE
				PRINTLN("ENABLE_DECREASING_DUPE_VALUES - repeat offender, NOT activating.")
			ENDIF		
			bDoneEnableDecreasingDupeValueCheck = TRUE
		ENDIF		
	ELSE
		IF NOT (g_FinishedFreemodeInit)
			PRINTLN("ENABLE_DECREASING_DUPE_VALUES - resetting bDoneEnableDecreasingDupeValueCheck")
			bDoneEnableDecreasingDupeValueCheck = FALSE
		ENDIF		
	ENDIF	
ENDPROC

//url:bugstar:3496271 - [BG SCRIPT] Custom plate option disabled for players with exploit level 4 or above.
BOOL bRunDisablePlatesCheck
PROC FIX_FOR_3496271()

	IF NOT (bRunDisablePlatesCheck)
		IF (g_FinishedFreemodeInit)
	
			INT iCurrentExploitLevel = GET_MP_INT_PLAYER_STAT(MPPLY_CHAR_EXPLOIT_LEVEL)
						
			IF (iCurrentExploitLevel >= 4)						
				g_sMPTunables.bDisable_SC_Number_Plate_Apply = TRUE
				PRINTLN("DISABLE_PLATE_BASED_ON_EXPLOIT - disabling plates, exploit level ", iCurrentExploitLevel, " iTotalModValueThresholdLow = ", 4)
			ELSE
				// don't set tunable to false incase we have actually set the tunable world wide to stop a duping method.
				//g_sMPTunables.bDisable_SC_Number_Plate_Apply = FALSE
			ENDIF
				
			bRunDisablePlatesCheck = TRUE
			
		ENDIF
	ELSE
		IF NOT (g_FinishedFreemodeInit)	
			bRunDisablePlatesCheck = FALSE
			PRINTLN("DISABLE_PLATE_BASED_ON_EXPLOIT - freemode not initialised, clearing bRunDisablePlatesCheck")
		ENDIF
	ENDIF

ENDPROC
/// START - CHILIADWAKEUP
/// PURPOSE:
///    Very efficient test for if two entities are with fRange metres of each other
///    Will be more efficient than doing IS_ENTITY_AT_ENTITY()
///    NOTE: This function doesn't do dead checks to keep it fast, make sure you do them yourself!
/// PARAMS:
///    e1 - First entity
///    e2 - Second entity
///    fRange - Test if the entities are this close to each other
FUNC BOOL IS_ENTITY_IN_RANGE_COORDS(ENTITY_INDEX e1, VECTOR vCoord, FLOAT fRange)
	RETURN ( VDIST2(GET_ENTITY_COORDS(e1), vCoord) <= fRange*fRange )
ENDFUNC

OBJECT_INDEX oiChiliadWakeUp_ObjectId
BOOL bChiliadWakeUp_StartedWakeUp, bChiliadWakeUp_FinishedStationaryStage
SCRIPT_TIMER stChiliadWakeUp_ObjectTimer, stChiliadWakeUp_SpeedTimer, stChiliadWakeUp_SoundTimer, stChiliadWakeUp_CallTimer
VECTOR vChiliadWakeUp_PreviousEndCoord
INT iChiliadWakeUp_LerpStage
INT iChiliadWakeUp_ObjectSoundId = -1
INT iChiliadWakeUp_PlayerSoundId = -1

CONST_INT CHILIADWAKEUP_STATIONARY_TIME 	4500	

CONST_INT CHILIADWAKEUP_SECOND_LERP_TIME 	2000
CONST_INT CHILIADWAKEUP_THIRD_LERP_TIME 	1000
CONST_INT CHILIADWAKEUP_FOURTH_LERP_TIME 	100
CONST_INT CHILIADWAKEUP_LAST_LERP_TIME 		50

CONST_INT CHILIADWAKEUP_PLAYER_SOUND_DELAY	1000
CONST_INT CHILIADWAKEUP_PLAYER_SOUND_TIME	3000
CONST_INT CHILIADWAKEUP_PLAYER_CALL_DELAY	60000

CONST_INT CHILIADWAKEUP_RESUPPLY_COUNT		577
CONST_INT CHILIADWAKEUP_TATTOO_CHECK		5000

PROC CHILIADWAKEUP_ROTATE_OBJECT()
	VECTOR vCurrentRot = GET_ENTITY_ROTATION(oiChiliadWakeUp_ObjectId)
	vCurrentRot.x = 0.0
	vCurrentRot.y = 0.0
	vCurrentRot.z += 1.0
	SET_ENTITY_ROTATION(oiChiliadWakeUp_ObjectId, vCurrentRot)
ENDPROC

FUNC VECTOR CHILIADWAKEUP_GET_LERP_END_COORD()
	SWITCH iChiliadWakeUp_LerpStage
		CASE 0 		RETURN <<306.567,5652.283,785.594>>
		CASE 1		RETURN <<-1082.8652, 6390.5195, 924.2493>>
		CASE 2 		RETURN <<-2542.8740, 7217.5137, 1051.3335>>
		CASE 3		RETURN <<-4139.1064, 8143.3022, 1412.6940>>
	ENDSWITCH
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

FUNC INT CHILIADWAKEUP_GET_TOTAL_LERP()
	RETURN CHILIADWAKEUP_SECOND_LERP_TIME+CHILIADWAKEUP_THIRD_LERP_TIME+CHILIADWAKEUP_FOURTH_LERP_TIME+CHILIADWAKEUP_LAST_LERP_TIME
ENDFUNC

FUNC INT CHILIADWAKEUP_GET_LERP_TIME()
	SWITCH iChiliadWakeUp_LerpStage
		CASE 0		RETURN CHILIADWAKEUP_SECOND_LERP_TIME
		CASE 1 		RETURN CHILIADWAKEUP_THIRD_LERP_TIME
		CASE 2		RETURN CHILIADWAKEUP_FOURTH_LERP_TIME
		CASE 3		RETURN CHILIADWAKEUP_LAST_LERP_TIME
	ENDSWITCH
	RETURN -1
ENDFUNC

FUNC BOOL CHILIADWAKEUP_IS_WEATHER_SUITABLE()
	INT iPrevWeather, iNextWeather
	FLOAT fInterp
	GET_CURR_WEATHER_STATE(iPrevWeather, iNextWeather, fInterp)
	IF fInterp < 0.5
		IF iPrevWeather = HASH("RAIN")
		OR iPrevWeather = HASH("THUNDER")
		OR iNextWeather = HASH("XMAS")
			RETURN TRUE
		ENDIF
	ELSE
		IF iNextWeather = HASH("RAIN")
		OR iNextWeather = HASH("THUNDER")
		OR iNextWeather = HASH("XMAS")
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL CHILIADWAKEUP_MET_REQUIREMENTS()
	IF GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SEEN_CS)
	AND GET_MP_INT_CHARACTER_STAT(MP_STAT_LFETIME_BIKER_BUY_COMPLET5 ,GET_ACTIVE_CHARACTER_SLOT()) > CHILIADWAKEUP_RESUPPLY_COUNT
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL CHILIADWAKEUP_CAN_LAUNCH()
	IF GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_NIGHTCLUB_DRUNK_SPAWN_CHILIAD_UNLOCK)
	AND g_TransitionSpawnData.iSpawnActivity = ENUM_TO_INT(SPAWN_ACTIVITY_DRUNK_AWAKEN)
	AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
	AND IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), <<426.433,5614.172,766.414>>, 1.0)
	AND g_TransitionSessionNonResetVars.sMagnateGangBossData.iClockHours >= 1
	AND g_TransitionSessionNonResetVars.sMagnateGangBossData.iClockHours < 4
	AND CHILIADWAKEUP_IS_WEATHER_SUITABLE()
	AND CHILIADWAKEUP_MET_REQUIREMENTS()
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC CHILIADWAKEUP_MAINTAIN()
	// Prevent phone calls during egg
	IF HAS_NET_TIMER_STARTED(stChiliadWakeUp_CallTimer)
		SET_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_REQUESTED_DISABLE_CELLPHONE_THIS_FRAME)
		IF HAS_NET_TIMER_EXPIRED(stChiliadWakeUp_CallTimer, CHILIADWAKEUP_PLAYER_CALL_DELAY)
			REINIT_NET_TIMER(stChiliadWakeUp_CallTimer)
			RESET_NET_TIMER(stChiliadWakeUp_CallTimer)
		ENDIF
	ENDIF
	
	// Finish sounds afterwards
	IF HAS_NET_TIMER_STARTED(stChiliadWakeUp_SoundTimer)
	AND HAS_NET_TIMER_EXPIRED(stChiliadWakeUp_SoundTimer, CHILIADWAKEUP_PLAYER_SOUND_DELAY)
		IF iChiliadWakeUp_PlayerSoundId = (-1)
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				iChiliadWakeUp_PlayerSoundId = GET_SOUND_ID()
				PLAY_SOUND_FROM_ENTITY(iChiliadWakeUp_PlayerSoundId, "05", PLAYER_PED_ID(), "DLC_GR_CS2_Sounds")						// Geiger
			ENDIF
		ELIF HAS_NET_TIMER_EXPIRED(stChiliadWakeUp_SoundTimer, (CHILIADWAKEUP_PLAYER_SOUND_DELAY+CHILIADWAKEUP_PLAYER_SOUND_TIME))
			IF NOT HAS_SOUND_FINISHED(iChiliadWakeUp_PlayerSoundId)
				STOP_SOUND(iChiliadWakeUp_PlayerSoundId)
			ENDIF
			RELEASE_SOUND_ID(iChiliadWakeUp_PlayerSoundId)
			iChiliadWakeUp_PlayerSoundId = -1
			STOP_AUDIO_SCENE("DLC_GR_CS2_General_Scene")
			
			REINIT_NET_TIMER(stChiliadWakeUp_SoundTimer)
			RESET_NET_TIMER(stChiliadWakeUp_SoundTimer)
			
			PRINTLN("[CHILIADWAKEUP] CHILIADWAKEUP_MAINTAIN - Finished player sound after ", (CHILIADWAKEUP_PLAYER_SOUND_DELAY+CHILIADWAKEUP_PLAYER_SOUND_TIME), "ms.")
		ENDIF
	ENDIF
	
	// Check if event should trigger
	IF NOT bChiliadWakeUp_StartedWakeUp
	AND NOT CHILIADWAKEUP_CAN_LAUNCH()
		EXIT
	ENDIF
	bChiliadWakeUp_StartedWakeUp = TRUE

	// Prevent phone calls during egg
	SET_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_REQUESTED_DISABLE_CELLPHONE_THIS_FRAME)
	
	// Create object and apply reward
	IF NOT DOES_ENTITY_EXIST(oiChiliadWakeUp_ObjectId)
		MODEL_NAMES eModel = INT_TO_ENUM(MODEL_NAMES, HASH("imp_prop_ship_01a"))
		REQUEST_MODEL(eModel)
		IF HAS_MODEL_LOADED(eModel)
			oiChiliadWakeUp_ObjectId = CREATE_OBJECT_NO_OFFSET(eModel, <<360.8759, 5623.4272, 780.1747>>, FALSE, FALSE, TRUE)
			SET_MODEL_AS_NO_LONGER_NEEDED(eModel)
			
			FREEZE_ENTITY_POSITION(oiChiliadWakeUp_ObjectId, TRUE)
			SET_ENTITY_LOD_DIST(oiChiliadWakeUp_ObjectId, 8000)

			INT iStatBackMark = GET_MP_INT_CHARACTER_STAT(MP_STAT_BACK_MARK)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_BACK_MARK, iStatBackMark+1)
			
			STRUCT_GUNRUNNING_MISSION_END sGUN_Telemetry_dataTemp
			sGUN_Telemetry_dataTemp.m_missionTypeId = ENUM_TO_INT(eGUNRUN_TYPE_STEAL)
			sGUN_Telemetry_dataTemp.m_missionId = 24
			PLAYSTATS_GUNRUNNING_MISSION_ENDED(sGUN_Telemetry_dataTemp)  
			
			vChiliadWakeUp_PreviousEndCoord = GET_ENTITY_COORDS(oiChiliadWakeUp_ObjectId, FALSE)
			iChiliadWakeUp_LerpStage = 0
			PRINTLN("[CHILIADWAKEUP] CHILIADWAKEUP_MAINTAIN - Created object and applied reward. Stat: ", iStatBackMark+1)
		ELSE
			PRINTLN("[CHILIADWAKEUP] CHILIADWAKEUP_MAINTAIN - Model is loading.")
		ENDIF
	// Handle object movement, acceleration and audio
	ELSE
		CHILIADWAKEUP_ROTATE_OBJECT()
		
		IF IS_SCREEN_FADED_IN()		
		OR IS_SCREEN_FADING_IN()
			
			IF iChiliadWakeUp_ObjectSoundId = (-1)
				START_AUDIO_SCENE("DLC_GR_CS2_General_Scene")
				iChiliadWakeUp_ObjectSoundId = GET_SOUND_ID()
				PLAY_SOUND_FROM_ENTITY(iChiliadWakeUp_ObjectSoundId, "07", oiChiliadWakeUp_ObjectId, "DLC_GR_CS2_Sounds")			// Loud collect sound
			ENDIF
		
			IF NOT bChiliadWakeUp_FinishedStationaryStage
				IF HAS_NET_TIMER_EXPIRED(stChiliadWakeUp_ObjectTimer, CHILIADWAKEUP_STATIONARY_TIME)		// 2 seconds past, ufo starts to speed off
					FREEZE_ENTITY_POSITION(oiChiliadWakeUp_ObjectId, FALSE)
					
					REINIT_NET_TIMER(stChiliadWakeUp_ObjectTimer)
					RESET_NET_TIMER(stChiliadWakeUp_ObjectTimer)
					
					START_NET_TIMER(stChiliadWakeUp_SpeedTimer)
					bChiliadWakeUp_FinishedStationaryStage = TRUE
					PRINTLN("[CHILIADWAKEUP] CHILIADWAKEUP_MAINTAIN - Finished stationary stage after ", CHILIADWAKEUP_STATIONARY_TIME, "ms.")
				ENDIF
			ELSE
				FLOAT fSpeedValue = TO_FLOAT(GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stChiliadWakeUp_SpeedTimer)) / TO_FLOAT(CHILIADWAKEUP_GET_TOTAL_LERP())
				FLOAT fLerpValue = TO_FLOAT(GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stChiliadWakeUp_ObjectTimer)) / TO_FLOAT(CHILIADWAKEUP_GET_LERP_TIME())
				fLerpValue = fLerpValue * fSpeedValue
				VECTOR vLerpVector = LERP_VECTOR(vChiliadWakeUp_PreviousEndCoord, CHILIADWAKEUP_GET_LERP_END_COORD(), fLerpValue)
				SET_ENTITY_COORDS(oiChiliadWakeUp_ObjectId, vLerpVector)

				IF fLerpValue >= 1.0
					iChiliadWakeUp_LerpStage++
					vChiliadWakeUp_PreviousEndCoord = GET_ENTITY_COORDS(oiChiliadWakeUp_ObjectId, FALSE) 
					REINIT_NET_TIMER(stChiliadWakeUp_ObjectTimer)
					RESET_NET_TIMER(stChiliadWakeUp_ObjectTimer)
					
					IF CHILIADWAKEUP_GET_LERP_TIME() = -1
						DELETE_OBJECT(oiChiliadWakeUp_ObjectId)
						FORCE_LIGHTNING_FLASH()
						
						IF NOT HAS_SOUND_FINISHED(iChiliadWakeUp_ObjectSoundId)
							STOP_SOUND(iChiliadWakeUp_ObjectSoundId)
						ENDIF
						RELEASE_SOUND_ID(iChiliadWakeUp_ObjectSoundId)
						iChiliadWakeUp_ObjectSoundId = -1

						START_NET_TIMER(stChiliadWakeUp_SoundTimer)
						START_NET_TIMER(stChiliadWakeUp_CallTimer)
						
						REINIT_NET_TIMER(stChiliadWakeUp_SpeedTimer)
						RESET_NET_TIMER(stChiliadWakeUp_SpeedTimer)
						
						vChiliadWakeUp_PreviousEndCoord = <<0.0, 0.0, 0.0>>
						iChiliadWakeUp_LerpStage = 0
						
						bChiliadWakeUp_StartedWakeUp = FALSE
						bChiliadWakeUp_FinishedStationaryStage = FALSE

						PRINTLN("[CHILIADWAKEUP] CHILIADWAKEUP_MAINTAIN - Finished all lerp stages. Deleted object and starting player sound delay.")
					ELSE
						PRINTLN("[CHILIADWAKEUP] CHILIADWAKEUP_MAINTAIN - Finished lerp stage. Moving onto stage ", iChiliadWakeUp_LerpStage)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC
/// END - CHILIADWAKEUP
   
//Main loop
SCRIPT(launch_Data launchData)

	SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
	NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME() 
	
	IF launchData.iD = launchData.iD
	ENDIF
	PRINTLN("[BGSCRIPT] - Launching - ", sVersionNumber)			

	g_iBGScriptVersion = GET_HASH_KEY(sVersionNumber) // this is required to be set for the bg script kick system.
	PRINTLN("[BGSCRIPT] g_iBGScriptVersion = ", g_iBGScriptVersion)
	
	
	//Wait 10s afterbooting
	INT iTime = GET_GAME_TIMER() + 10000
	WHILE GET_GAME_TIMER() < iTime
		WAIT(0)
	ENDWHILE
	
	//Main Loops
	WHILE TRUE

		
		// cheat for rockstar dev's to display which bg script is running.
		IF IS_ROCKSTAR_DEV()
		#IF IS_DEBUG_BUILD
		OR TRUE
		#ENDIF		
			MAINTAIN_CHECK_TO_ACTIVATE_PRINT()			
		ENDIF

		IF NETWORK_IS_GAME_IN_PROGRESS()
		AND (g_sCURRENT_UGC_STATUS.g_bUgcGlobalsBlockLoaded)
			//url:bugstar:3290469 - BG Script that amends the tunable values for vehicle sales limiter
			FIX_FOR_3290469()
		
			//url:bugstar:3280561 - [BG SCRIPT][PUBLIC][REPORTED] "Insurance Fraud" Mod is causing players to be put into Bad Sport lobbies and lose GTA$
			FIX_FOR_3280561()
		
			//url:bugstar:3533670 - [BG SCRIPT ONLY] Decreasing dupe sell values for repeat offenders.
			FIX_FOR_3533670()
		
			//url:bugstar:3496271 - [BG SCRIPT] Custom plate option disabled for players with exploit level 4 or above.
			FIX_FOR_3496271()
			
			//url:bugstar:5092622 - [BG SCRIPT][PUBLIC] [EXPLOIT] Frozen money - Director mode, join friend, suspend app.
			FIX_FOR_5092622()
			
			//CHILIADWAKEUP
			CHILIADWAKEUP_MAINTAIN()
		ENDIF
		
		IF NETWORK_IS_ACTIVITY_SESSION()

		ENDIF
	
		//The one wait		
		WAIT(0)
	ENDWHILE	
	PRINTLN("[BGSCRIPT] BG_IS_EXITFLAG_SET() = TRUE, cleaning up")
	//BG_SET_EXITFLAG_RESPONSE()
	TERMINATE_THIS_THREAD()
ENDSCRIPT



