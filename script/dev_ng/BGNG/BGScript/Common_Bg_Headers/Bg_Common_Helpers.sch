//Commands
USING "rage_builtins.sch"
USING "commands_cutscene.sch"

//Globals
/////////////// - DO NOT CHANGE THE ORDER OF THES OR YOU WILL CAUSE GLOBAL SIGNATURE MISSMATCH
USING "globals.sch"
USING "mp_globals.sch"
USING "mp_globals_fm.sch"
USING "mp_globals_ugc.sch"
USING "mp_globals_tunables.sch"
USING "mp_globals_block_mp_sg.sch"
USING "mp_property_global_common.sch"
USING "mp_globals_block_mp_prop.sch"
USING "mp_globals_common.sch"
USING "MP_globals_COMMON_TU.sch"
USING "stats_helper_packed_enums.sch"
USING "mp_property_global_common.sch"
USING "mp_globals_block_mp_prop_special.sch"
USING "profiler_globals.sch"
USING "mp_globals_block_sc_lb.sch"
/////////////// - DO NOT CHANGE THE ORDER OF THES OR YOU WILL CAUSE GLOBAL SIGNATURE MISSMATCH


FUNC PLAYER_INDEX INVALID_PLAYER_INDEX()
	RETURN INT_TO_NATIVE(PLAYER_INDEX, -1)	
ENDFUNC

//Number of booleans a stat can have.
CONST_INT NUM_PACKED_BOOL_PER_STAT 64

/// PURPOSE: Check a player is in an OK playing state depending on the passed in variables
FUNC BOOL IS_NET_PLAYER_OK(PLAYER_INDEX playerID, BOOL bCheckPlayerAlive = TRUE, BOOL bCheckPlayerInGameModeMainScriptRunningCase = TRUE)
	INT iPlayer = NATIVE_TO_INT(playerID)
	
	IF iPlayer != -1
		IF NETWORK_IS_PLAYER_ACTIVE(playerID)
		
			//Check the player is alive
			IF (bCheckPlayerAlive)
				IF NOT IS_PLAYER_PLAYING(playerID)
					RETURN FALSE
				ENDIF
			ENDIF
			
			//Check the player is in the main running state
			IF (bCheckPlayerInGameModeMainScriptRunningCase)
				//If it's the local player, the use their cached state, as it can change mid frame
				IF iPlayer = MPGlobals.g_iLocalPlayerID
					RETURN MPGlobals.g_bLocalPlayerActive
				ELIF GlobalplayerBD[iPlayer].iGameState != MAIN_GAME_STATE_RUNNING
					RETURN FALSE
				ENDIF
			ENDIF
			
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_ACTIVE_CHARACTER_SLOT()
	RETURN g_Private_ACTIVE_CHARACTER
ENDFUNC

/// PURPOSE: Returns the value of a boolean packed stat.
FUNC BOOL GET_PACKED_STAT_BOOL(STATS_PACKED stat, INT slot = -1)
	
	IF slot = -1
		slot = GET_ACTIVE_CHARACTER_SLOT()
	ENDIF

	RETURN GET_PACKED_STAT_BOOL_CODE(stat, slot)
ENDFUNC
FUNC INT GET_SLOT_NUMBER(INT iCharacter)

	INT Result =  iCharacter
	IF Result = -1	
		INT CurrentActive = GET_ACTIVE_CHARACTER_SLOT()
		IF CurrentActive > -1
			MPGlobalsStats.HasSlotErrorHitOnce = FALSE
			Result = CurrentActive
		ELSE
			Result = 0 
				MPGlobalsStats.HasSlotErrorHitOnce = TRUE
		ENDIF
	ENDIF
	RETURN Result

ENDFUNC

PROC SET_MP_INT_CHARACTER_STAT(MP_INT_STATS anIntStat,  INT Value, INT iSlot = -1, BOOL CoderReset = TRUE, BOOL inTransition = FALSE)
	
	IF inTransition
	ENDIF
	
	STATSENUM MainstatBody = MPIntStatNamesArray[anIntStat][GET_SLOT_NUMBER(iSlot)]
		
	IF ENUM_TO_INT(MainstatBody) <> 0
		STAT_SET_INT(MainstatBody,Value,CoderReset)
	ENDIF
ENDPROC
FUNC INT GET_MP_INT_CHARACTER_STAT(MP_INT_STATS anIntStat, INT iSlot = -1 , BOOL bIgnoreAssert = FALSE)
	
	IF anIntStat != MAX_NUM_MP_INT_STATS 
	
		IF bIgnoreAssert = FALSE

		ENDIF
		STATSENUM MainstatBody = MPIntStatNamesArray[anIntStat][GET_SLOT_NUMBER(iSlot)]
		
		INT Result
		IF STAT_GET_INT(MainstatBody, Result)
			RETURN Result
		ENDIF
	ENDIF
	RETURN 0
ENDFUNC
FUNC SKYSWOOP GET_SKYSWOOP_STAGE()
	RETURN g_TransitionData.SwoopStage
ENDFUNC
FUNC BOOL IS_SKYSWOOP_AT_GROUND()
	IF GET_SKYSWOOP_STAGE() = SKYSWOOP_NONE
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC INT GET_MP_INT_PLAYER_STAT(MPPLY_INT_STATS aStat)

	STATSENUM MainstatBody = INT_TO_ENUM(STATSENUM, aStat)
	INT Result
	IF STAT_GET_INT(MainstatBody, Result)
		RETURN Result
	ENDIF
	RETURN 0
ENDFUNC
