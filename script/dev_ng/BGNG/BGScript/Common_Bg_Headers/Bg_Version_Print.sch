USING "commands_hud.sch"
USING "Bg_Script_Vars.sch"
USING "net_script_timers.sch"
USING "commands_script.sch"
USING "commands_network.sch"

PROC DISPLAY_TEXT_WITH_LITERAL_STRING_FOR_RELEASE_F9(FLOAT DisplayAtX, FLOAT DisplayAtY, STRING pTextLabel, STRING pLiteralString)
	BEGIN_TEXT_COMMAND_DISPLAY_TEXT(pTextLabel)
		ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY(pLiteralString)
	END_TEXT_COMMAND_DISPLAY_TEXT(DisplayAtX, DisplayAtY)
ENDPROC

PROC MAINTAIN_CHECK_TO_ACTIVATE_PRINT(BG_SCRIPT_VARS &sBg_Script_Vars)
	IF NOT (sBg_Script_Vars.bActivatedPrint)
		
		IF IS_PAUSE_MENU_ACTIVE()
			SWITCH sBg_Script_Vars.iActivatePrintProgress
				CASE 0
					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
						sBg_Script_Vars.iActivatePrintProgress++
						SETTIMERA(0)
					ENDIF
				BREAK
				CASE 1
					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
						sBg_Script_Vars.iActivatePrintProgress++
						SETTIMERA(0)
					ELIF TIMERA() > 2000
						sBg_Script_Vars.iActivatePrintProgress = 0
					ENDIF
				BREAK
				CASE 2
					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
						sBg_Script_Vars.iActivatePrintProgress++
						SETTIMERA(0)
					ELIF TIMERA() > 2000
						sBg_Script_Vars.iActivatePrintProgress = 0
					ENDIF
				BREAK
				CASE 3
					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
						sBg_Script_Vars.iActivatePrintProgress++
						SETTIMERA(0)
					ELIF TIMERA() > 2000
						sBg_Script_Vars.iActivatePrintProgress = 0
					ENDIF
				BREAK
				CASE 4
					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
						sBg_Script_Vars.bActivatedPrint = TRUE
						SETTIMERA(0)
					ELIF TIMERA() > 2000
						sBg_Script_Vars.iActivatePrintProgress = 0
					ENDIF
				BREAK
			ENDSWITCH
		ENDIF
	ELSE
		IF IS_PAUSE_MENU_ACTIVE()
			SWITCH sBg_Script_Vars.iActivatePrintProgress
				CASE 0
					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
						sBg_Script_Vars.iActivatePrintProgress++
						SETTIMERA(0)
					ENDIF
				BREAK
				CASE 1
					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
						sBg_Script_Vars.iActivatePrintProgress++
						SETTIMERA(0)
					ELIF TIMERA() > 2000
						sBg_Script_Vars.iActivatePrintProgress = 0
					ENDIF
				BREAK
				CASE 2
					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
						sBg_Script_Vars.iActivatePrintProgress++
						SETTIMERA(0)
					ELIF TIMERA() > 2000
						sBg_Script_Vars.iActivatePrintProgress = 0
					ENDIF
				BREAK
				CASE 3
					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
						sBg_Script_Vars.iActivatePrintProgress++
						SETTIMERA(0)
					ELIF TIMERA() > 2000
						sBg_Script_Vars.iActivatePrintProgress = 0
					ENDIF
				BREAK
				CASE 4
					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
						sBg_Script_Vars.bActivatedPrint = FALSE
						SETTIMERA(0)
					ELIF TIMERA() > 2000
						sBg_Script_Vars.iActivatePrintProgress = 0
					ENDIF
				BREAK
			ENDSWITCH
		ENDIF
	ENDIF
	
	IF (sBg_Script_Vars.bActivatedPrint)
		SET_TEXT_SCALE(0.0000, 0.23)
    	SET_TEXT_COLOUR(255,255,255, 255)   
    	DISPLAY_TEXT_WITH_LITERAL_STRING_FOR_RELEASE_F9(0.69, 0.06, "STRING", sVersionNumber)
	ENDIF
	
ENDPROC


