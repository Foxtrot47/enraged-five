USING "commands_hud.sch"
USING "commands_script.sch"
USING "commands_network.sch"
USING "commands_dlc.sch"
USING "commands_decorator.sch"
USING "commands_lobby.sch"

//Common BG script headers
USING "Bg_Script_Vars.sch"
USING "Bg_Version_Print.sch"

//Always called at the start of every frame in every BG script
PROC MAINTAIN_BG_START_OF_FRAME(BG_SCRIPT_VARS &sBg_Script_Vars)
	
	
	// cheat for rockstar dev's to display which bg script is running.
	IF IS_ROCKSTAR_DEV()
	#IF IS_DEBUG_BUILD
	OR TRUE
	#ENDIF
	
		MAINTAIN_CHECK_TO_ACTIVATE_PRINT(sBg_Script_Vars)
			
	ENDIF
ENDPROC

