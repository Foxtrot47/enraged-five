USING "buildtype.sch"

//Vars needed for the BG script
STRUCT BG_SCRIPT_VARS
	BOOL bActivatedPrint = FALSE
	INT iActivatePrintProgress = 0
ENDSTRUCT
