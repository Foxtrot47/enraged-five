require 'rexml/document'
require 'rexml/xpath'


def printScriptFilesFromManifest(projectName)
  begin
  file = File.new( projectName )
  rescue
    puts 'EOF'
    return
  end
  include REXML
  doc = Document.new file
  XPath.each(doc,"//Utility") {|element| puts element.attributes["name"]}
end

def main()
  if(ARGV[0]==nil)
    puts "No arguments provided, exiting"
    return
  end
  printScriptFilesFromManifest(ARGV[0])
end

main