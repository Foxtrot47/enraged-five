USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_script.sch"

USING "net_mission_joblist.sch"
USING "net_mission_control.sch"
USING "fmmc_mp_setup_mission.sch"
USING "net_app_public.sch"
USING "net_activity_selector.sch"       // To allow an Active Contact Mission to be deleted
USING "net_contact_requests.sch"
USING "net_cash_transactions.sch"
USING "net_corona_V2.sch"
USING "net_simple_interior.sch"

USING "net_gang_boss.sch"


// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************
//
//      MISSION NAME    :   appMpJobListNEW.sc
//      AUTHOR          :   Keith
//      DESCRIPTION     :   Controls the display and selection of the Multiplayer Joblist app.
//      NOTES           :   Built on the Blank App template and using the old MP JobList for inspiration.
//                          Main difference is that this will use a header that directly accesses multiplayer globals for generating
//                              the JobList contents - also likely to change to use new JobList-specific scaleform screens rather
//                              than the text message screen.
//                          While the player is in a deeper stage of the app than the JobList screen, the joblist details shouldn't
//                              get updated. This is to preserve the data. If a mission is joined or cancelled this app will call
//                              a mission triggering public function which will handle out of date mission data (ie: if the joinable
//                              job has become full since the joblist was generated, etc.). There may be exceptions to this rule though.
//                              If the current job ends then the player should probably be taken back to the joblist screen if the
//                              player was on the Cancel Job or Confirm Cancel Job screens.
//      MODIFICATIONS   :   28/3/12: Removed the 'confirm' screen when joining a job.
//
// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************




// ===========================================================================================================
//      Variables and Constants
// ===========================================================================================================

// JobList Screen Stages
ENUM enumJobListStages
    JLS_JOBLIST,                                // The JobList
    JLS_CURRENT_JOB,                            // The current job - offer to cancel
    JLS_JOINABLE_JOB,                           // A joinable job - offer to join
    JLS_INVITATION,                             // Invitation - offer to warp to a corona where at least one player is waiting to start the mission
    JLS_CSINVITE,                               // Cross-Session Invite - join player in a different session
    JLS_RVINVITE,                               // Basic Invite - on acceptance just needs to set a flag and kill the app
    JLS_FAKE_INVITATION,                        // Fake Invitation for tutorial mission - on acceptance just needs to set a flag and kill the app
    JLS_CONFIRM_CANCEL_JOB,                     // The Cancel Job confirmation screen
	JLS_CONFIRM_INVITE_LOSE_WANTED_LEVEL,		// Confirmation screen after accepting an Invitation with a Wanted Level
	JLS_CONFIRM_CSINVITE_LOSE_WANTED_LEVEL,		// Confirmation screen after accepting a Cross-Session Invite with a Wanted Level
	JLS_CONFIRM_RVINVITE_LOSE_WANTED_LEVEL,		// Confirmation screen after accepting a Basic Invite with a Wanted Level
    JLS_WAITING_FOR_RESERVATION_CONFIRMATION,   // After trying to reserve the player's place on an activity, wait for confirmation of being reserved
    JLS_FM_WAITING_FOR_FADEOUT_BEFORE_JOIN,     // For the FM joblist, need to tidy it up by fading out before joining the mission
    JLS_WAITING_FOR_PLAYER_TO_QUIT_MISSION,     // The player chose to cancel the current mission so wait until the player is no longer on mission
	JLS_ALTERNATIVE_INVITE_SCREEN,				// Displays alternative invite screen
	JLS_ALTERNATIVE_PASSIVE_WARNING				// Passive warning screen
ENDENUM

enumJobListStages   m_currentStage = JLS_JOBLIST


// -----------------------------------------------------------------------------------------------------------

// NOTE: The JobList screen, which is view state 20
CONST_INT       JOBLIST_VIEW_STATE              20

// NOTE: The Job screen currently uses the Text Message View screen, which is view state 7
CONST_INT       JOB_VIEW_STATE                  7

// NOTE: The Confirm screen currently uses the Simple List screen, which is view state 13
CONST_INT       CONFIRM_VIEW_STATE              13


// -----------------------------------------------------------------------------------------------------------

// The Scaleform Text Labels used for button text
TEXT_LABEL_15   m_jobListIconTextBack           = "CELL_206"
TEXT_LABEL_15   m_joblistIconTextYes            = "CELL_212"
TEXT_LABEL_15   m_joblistIconTextNo             = "CELL_213"
TEXT_LABEL_15   m_jobListIconTextOptions        = "CELL_214"
TEXT_LABEL_15   m_jobListIconTextAcceptJob      = "CELL_243"
TEXT_LABEL_15   m_jobListIconTextCancelJob      = "CELL_244"


// -----------------------------------------------------------------------------------------------------------

// Text used within JobList app
TEXT_LABEL_15   m_joblistTextCrook              = "CELL_29"//"CELL_17"  Steve T change to make this say Job List also.
TEXT_LABEL_15   m_joblistTextFM                 = "CELL_29"
TEXT_LABEL_15	m_acceptJobText					= "CELL_MP_500"
TEXT_LABEL_15   m_cancelJobText                 = "CELL_246"
TEXT_LABEL_15   m_areYouSureText                = "CELL_249"
TEXT_LABEL_15	m_loseWantedLevelText			= "CELL_MP_501"
TEXT_LABEL_15   m_currentJobText                = "CELL_260"
TEXT_LABEL_15   m_joinableJobText               = "CELL_263"
TEXT_LABEL_15   m_invitationText                = "CELL_265"


// -----------------------------------------------------------------------------------------------------------

// Contact Pictures
TEXT_LABEL_15   m_defaultContactPicture         = "CELL_300"


// -----------------------------------------------------------------------------------------------------------

// The JobList array position for the Job selected by the player
INT             m_selectedJobSlot               = 0
INT 			m_totalAlternativeInvites 		= 0

/// PURPOSE:
///    Adjusts the scaleform slot returned based on the number of alternatives (those outwith joblist array) being displayed
/// RETURNS:
///    
FUNC INT Get_Invite_Array_Index()

	// Should never attempt to grab this when the selected slot is an alternative invite
	IF m_totalAlternativeInvites > m_selectedJobSlot
		PRINTLN("...KGM [JobList][App]: Get_Invite_Array_Index(): m_totalAlternativeInvites < m_selectedJobSlot")
		PRINTLN("...KGM [JobList][App]: 		m_totalAlternativeInvites = ", m_totalAlternativeInvites)
		PRINTLN("...KGM [JobList][App]: 		m_selectedJobSlot = ", m_selectedJobSlot)
		SCRIPT_ASSERT("...KGM [JobList][App]: Get_Invite_Array_Index(): m_totalAlternativeInvites < m_selectedJobSlot")
		RETURN 0
	ENDIF
	
	RETURN m_selectedJobSlot - m_totalAlternativeInvites
ENDFUNC

BOOL bAcceptPressed = FALSE

// Offset for the style in the joblist.
CONST_INT STYLE_OFFSET 4

// -----------------------------------------------------------------------------------------------------------

// For displaying the 'wanted level' help text only when the player first brings up the phone
// KGM 31/8/13: ToDo 1605311 (Pay to lose Wanted Level when accepting invites)
// Using this flag to display a different Help message advising the player there is a charge for losing the wanted level
BOOL            m_wantedLevelHelpDisplayed      = FALSE

// Using this flag to display a different Help message advising the player there is a charge for losing the wanted level
BOOL            m_cantAcceptJobHelpDisplayed     = FALSE

//// For displaying the 'bounty' help text only when the player first brings up the phone
//BOOL          m_bountyHelpDisplayed       = FALSE


// -----------------------------------------------------------------------------------------------------------

// An instance of the Mp App data struct
structMPAppData m_sAppData


// -----------------------------------------------------------------------------------------------------------

//For loading data from the cloud.
GET_UGC_CONTENT_STRUCT sGetUGC_content


// -----------------------------------------------------------------------------------------------------------

// Variables used to switch to a new screen if the invite appears to be getting delayed because of a Host Migration
BOOL	m_allowSwitchToPleaseWait	= FALSE
INT		m_switchToPleaseWaitTimeout	= 0

CONST_INT	SWITCH_TO_PLEASE_WAIT_TIMEOUT_msec		5000


// -----------------------------------------------------------------------------------------------------------

TEXT_LABEL_23 m_currentWarningHelpText = ""


// ===========================================================================================================
//      Cleanup Routines
// ===========================================================================================================

// PURPOSE: Cleanly terminates the script
PROC Cleanup_and_Terminate()

	
	IF NOT g_bRunQuitHeistWarningScreen
	AND NOT g_sBossAgencyApp.bBossAgencyAppRunningWarningScreen
	
		g_cancelJobButtonOnScreen = FALSE
    	TERMINATE_THIS_THREAD()
		
	
	ENDIF
	
	
ENDPROC




// ===========================================================================================================
//      Warning Help Text Routines
// ===========================================================================================================

// PURPOSE:	KGM 22/7/15 [BUG 2437060]: Display a warning help text when on the 'invite accept' screen, if required (ie: some FM Events)
PROC Display_Accept_Invite_Warning_Help_Text_If_Required()

	// NOTE: Stores the help so that it can be check if still on display and removed when no longer needed (ie: screen change or app termination)

	BOOL displayHelp = FALSE

	IF GB_IS_PLAYER_CRITICAL_TO_JOB(PLAYER_ID())
		IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_BIKER_GANG()
			m_currentWarningHelpText = "PHINVQUITBBB"
		ELSE
			m_currentWarningHelpText = "PHINVQUITBB"
		ENDIF
		displayHelp = TRUE
	ELIF IS_PLAYER_CRITICAL_TO_ANY_EVENT(PLAYER_ID())
		m_currentWarningHelpText = "PHINVQUITB"
		displayHelp = TRUE
	ENDIF
	
	IF (FM_EVENT_SHOULD_GIVE_PHONE_INVITE_WARNING())
		m_currentWarningHelpText = "PHINVQUIT"
		displayHelp = TRUE
	ENDIF
	
	
	IF (displayHelp)
		PRINTLN(".KGM [JobList][App]: Display_Accept_Invite_Warning_Help_Text_If_Required(). Display: ", m_currentWarningHelpText)
	ENDIF
	
	PRINT_HELP(m_currentWarningHelpText)
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	KGM 22/7/15 [BUG 2437060]: Clear a warning help text displayed by the 'invite accept' screen when no longer required
PROC Clear_Accept_Invite_Warning_Help_Text_If_On_Display()

	IF (IS_STRING_NULL_OR_EMPTY(m_currentWarningHelpText))
		EXIT
	ENDIF
	
	// Is the message being displayed?
	IF NOT (IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(m_currentWarningHelpText))
		// ...no, so clear the variable
		m_currentWarningHelpText = ""
		EXIT
	ENDIF
	
	// Message is being displayed, so clear it and the variable
	CLEAR_HELP()
	
	PRINTLN(".KGM [JobList][App]: Clear_Accept_Invite_Warning_Help_Text_If_On_Display(). Clear: ", m_currentWarningHelpText)
	
	m_currentWarningHelpText = ""

ENDPROC



// ===========================================================================================================
//      Cost Of Mission Routines
// ===========================================================================================================

/// PURPOSE: Get the amount player must pay for job 
FUNC INT Get_Cost_Of_Accepting_Invite(INT paramFMMissionType, INT paramMissionSubtype)
	
	INT theCharge = 0
	
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		RETURN(0)
	ENDIF
	
	// Invites to simple interior dont cost anything
	IF paramFMMissionType = FMMC_TYPE_SIMPLE_INTERIOR
	#IF FEATURE_HEIST_ISLAND
	OR  paramFMMissionType = FMMC_TYPE_HEIST_ISLAND_INVITE
	#ENDIF
		RETURN(0)
	ENDIF

	// KGM 28/8/13: A poor player is allowed to Play Again or play a Random mission after completing a mission without having to pay - this will include losing wanted level
	IF (g_allowPoorPlayerToPlayAgainForFree)
		RETURN (theCharge)
	ENDIF
	
	// If the player has a wanted level, a charge will be applied to lose it
	IF (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0)
		theCharge += GET_CONTACT_REQUEST_COST(REQUEST_REMOVE_WANTED)
	ENDIF
	
	// If the player doesn't need to pay to play this activity, then Lose Wanted Level is the only potential charge
	IF NOT (DO_I_NEED_TO_PAY_TO_PLAY_THIS_FM_ACTIVITY(paramFMMissionType, paramMissionSubtype))
		RETURN (theCharge)
	ENDIF
	
	// Add on the cost of playing the mission
	theCharge += GET_FM_ACTIVITY_COST(paramFMMissionType, paramMissionSubtype)
	
	RETURN (theCharge)

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	KGM 16/4/13: Check if the player has enough cash to accept this Invite
FUNC BOOL Can_I_Afford_To_Accept_Invite(INT paramFMMissionType, INT paramMissionSubtype)

	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		RETURN(TRUE)
	ENDIF

	INT theCharge = Get_Cost_Of_Accepting_Invite(paramFMMissionType, paramMissionSubtype)
	
	IF NETWORK_CAN_SPEND_MONEY(theCharge, FALSE, TRUE, FALSE)
		RETURN TRUE
	ENDIF
	
	// Can't afford it
	RETURN FALSE
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the player has enough cash to clear the Wanted Level (added for Apartment Invite checks)
FUNC BOOL Can_I_Afford_To_Clear_Wanted_Level()
	RETURN NETWORK_CAN_SPEND_MONEY(GET_CONTACT_REQUEST_COST(REQUEST_REMOVE_WANTED), FALSE, TRUE, FALSE)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	KGM 9/12/13: Check if the player has enough cash to pay the H2H wager
//
// INPUT PARAMS:		paramWagerH2H		The H2H wager amount, or -1 if not head-to-head
FUNC BOOL Can_I_Afford_Wager_If_Head_To_Head_Playlist(INT paramWagerH2H)

	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		RETURN(TRUE)
	ENDIF
	
	IF (paramWagerH2H < 0)
		RETURN TRUE
	ENDIF
	
	// Added NETWORK_GET_VC_BANK_BALANCE for 1871222 as we now take cash from wallet and bank
	IF NETWORK_CAN_SPEND_MONEY(paramWagerH2H, FALSE, TRUE, FALSE)
		RETURN TRUE
	ENDIF
	
	// Can't afford it
	RETURN FALSE
	
ENDFUNC




// ===========================================================================================================
//      FM-Specific Routines
// ===========================================================================================================

// PURPOSE: Check if the game should fadeout before launching the mission type
//
// INPUT PARAMS:            paramMissionType            FM Mission Type
// RETURN VALUE:            BOOL                        TRUE if the game should fadeout, otherwise FALSE
FUNC BOOL Should_Game_FadeOut_When_Launching_This_FM_Mission_Type(INT paramMissionType)

    SWITCH (paramMissionType)
        // Don't fade for these mission types
        CASE FMMC_TYPE_GANGHIDEOUT
            #IF IS_DEBUG_BUILD
                NET_PRINT("...KGM MP [JobList][App]: Mission Type doesn't require a fade prior to joining the mission: ")
                NET_PRINT_INT(paramMissionType)
                NET_NL()
            #ENDIF
            
            RETURN FALSE
    ENDSWITCH
    
    // In general, most mission types should fade out
    RETURN TRUE

ENDFUNC




// ===========================================================================================================
//      Display Routines
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//      Job screen
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Send the IsHeistMessage param to the Joblist list display
//
// INPUT PARAMS:			paramFMMCType			FMMC Mission Type
//							paramSubtype			Subtype
PROC Send_IsHeist_Params(INT paramFMMCType, INT paramSubtype, INT iHash, INT iAdversaryModeType)
	
	INT iHeistMessage = 0
	IF (paramFMMCType = FMMC_TYPE_MISSION)
	OR (paramFMMCType = FMMC_TYPE_HEIST_PHONE_INVITE)
	OR (paramFMMCType = FMMC_TYPE_HEIST_PHONE_INVITE_2)
	OR (paramFMMCType = FMMC_TYPE_IH_QUICK_INVITE)
	
		IF GANGOPS_FLOW_IS_A_GANGOPS_MISSION_FROM_ROOT_ID(iHash)
			iHeistMessage = 19
		ELIF (paramSubtype = FMMC_MISSION_TYPE_HEIST)
		OR (paramSubtype = FMMC_MISSION_TYPE_PLANNING)
		OR (paramFMMCType = FMMC_TYPE_HEIST_PHONE_INVITE)
		OR (paramFMMCType = FMMC_TYPE_HEIST_PHONE_INVITE_2)
		OR (paramFMMCType = FMMC_TYPE_IH_QUICK_INVITE)
		
			iHeistMessage = 1
		ELIF (paramSubtype = FMMC_MISSION_TYPE_FLOW_MISSION)
			iHeistMessage = 3
		ELIF g_sMPTunables.bRED_INVITE
			IF SHOULD_THIS_BG_BE_RED(iHash, iAdversaryModeType)
				iHeistMessage = 2
			ENDIF
		ENDIF
	ENDIF
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iHeistMessage)
	
    #IF IS_DEBUG_BUILD
        NET_PRINT("         - iHeistMessage? ")
		IF (iHeistMessage = 1)
			NET_PRINT("1")
		ELIF (iHeistMessage = 2)
			NET_PRINT("2")
		ELIF (iHeistMessage = 3)
			NET_PRINT("3")
		ELSE
			NET_PRINT("0")
		ENDIF
        NET_NL()
    #ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Send the BlipSprite, IsHeistMessage, and IsMultiplayer params to the Joblist list display
//
// INPUT PARAMS:			paramFMMCType			FMMC Mission Type
//							paramSubtype			Subtype
PROC Send_BlipSprite_IsHeist_IsMP_Scaleform_Params(INT paramFMMCType, INT paramSubtype, INT iHash, INT iAdversaryModeType)
	#IF IS_DEBUG_BUILD
	PRINTLN("Send_BlipSprite_IsHeist_IsMP_Scaleform_Params - paramFMMCType = ", paramFMMCType, " paramSubtype = ", paramSubtype, " iHash = ", iHash, " iAdversaryModeType = ", iAdversaryModeType)
	DEBUG_PRINTCALLSTACK()
	#ENDIF
	BLIP_SPRITE blipSprite = Get_Joblist_BlipID(paramFMMCType, paramSubtype, iHash)
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(blipSprite))
	
    #IF IS_DEBUG_BUILD
        NET_PRINT("         - BlipSpriteAsInt: ")
		NET_PRINT_INT(ENUM_TO_INT(blipSprite))
        NET_NL()
    #ENDIF
	
	Send_IsHeist_Params(paramFMMCType, paramSubtype, iHash, iAdversaryModeType)
	
	BOOL isMultiplayer = NETWORK_IS_GAME_IN_PROGRESS()
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(isMultiplayer)
	
    #IF IS_DEBUG_BUILD
        NET_PRINT("         - isMultiplayer? ")
		IF (isMultiplayer)
			NET_PRINT("TRUE")
		ELSE
			NET_PRINT("FALSE")
		ENDIF
        NET_NL()
    #ENDIF
		
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Display the FM Activity details screen
// NOTES:   KGM 31/7/12: TEMP until FM uses Mission Controller
PROC Display_FM_Activities_Screen()
    
    // Delete all data from the JobList Screen
    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieIndex, "SET_DATA_SLOT_EMPTY", (TO_FLOAT(JOB_VIEW_STATE)))
	
	#IF IS_DEBUG_BUILD
    	NET_PRINT("...KGM MP [JobList][App]: Display_FM_Activities_Screen() - clearing job details [view state: ") NET_PRINT_INT(JOB_VIEW_STATE) NET_PRINT("]") NET_NL()
	#ENDIF
	
	// Field Details
	TEXT_LABEL_63	playerName			= ""
	TEXT_LABEL_15	bodyText			= ""
	TEXT_LABEL_23	bodyConfiguration	= ""
	INT				theCreator			= 0
	INT				thisSlot			= 0
	PLAYER_INDEX	thisPlayer
	INT				joblistPlayerGBD	= g_sJobListMP[Get_Invite_Array_Index()].jlUniqueID
	VECTOR			missionCoords		= GlobalplayerBD_FM[joblistPlayerGBD].currentMissionData.mdPrimaryCoords
	BOOL			useMissionName		= FALSE
	BOOL			isPlaylist			= FALSE

    // Store the details of the text message - there is only one slot
    BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieIndex, "SET_DATA_SLOT")
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(JOB_VIEW_STATE)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(thisSlot)
		
		// Get Body text (ie: deathMatch, Mission, etc)
		bodyText 			= Get_FM_Joblist_Activity_TextLabel(g_sJobListMP[Get_Invite_Array_Index()].jlMissionAsInt, g_sJobListMP[Get_Invite_Array_Index()].jlSubtype,g_sJobListMP[Get_Invite_Array_Index()].jlPropertyType, g_sJobListMP[Get_Invite_Array_Index()].jlRootContentIdHash)
		
		// Header Line of Body (use The Creator Name (literal string composed using the Text Commands))
		// ...is a playlist?
		isPlaylist = FALSE
		IF (g_sJobListMP[Get_Invite_Array_Index()].jlState = MP_JOBLIST_JOINABLE_PLAYLIST)
			isPlaylist = TRUE
		ENDIF
		
		// ...the creator?
		IF (isPlaylist)
			theCreator = FMMC_ROCKSTAR_CREATOR_ID
		ELSE
			theCreator = g_sJobListMP[Get_Invite_Array_Index()].jlInstance
		ENDIF
		
		IF (theCreator = FMMC_ROCKSTAR_CREATOR_ID)
        OR (theCreator = FMMC_ROCKSTAR_HIDEOUT_CREATOR_ID)
        OR (theCreator = FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID)
        OR (theCreator = FMMC_MINI_GAME_CREATOR_ID)
            // Display Rockstar as creator
            BEGIN_TEXT_COMMAND_SCALEFORM_STRING("CELL_266")     // "Rockstar"
            END_TEXT_COMMAND_SCALEFORM_STRING()
        ELSE
            thisPlayer = INT_TO_PLAYERINDEX(theCreator)
            IF (IS_NET_PLAYER_OK(thisPlayer, FALSE))
                // Display Creator Name
                playerName  = GET_PLAYER_NAME(thisPlayer)
                BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
                    ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(playerName)
                END_TEXT_COMMAND_SCALEFORM_STRING()
            ELSE
                // Creator no longer OK, so display activity type
                BEGIN_TEXT_COMMAND_SCALEFORM_STRING(bodyText)
                END_TEXT_COMMAND_SCALEFORM_STRING()
            ENDIF
        ENDIF
		
		// Display Body text (ie: deathMatch, Mission, etc)
        useMissionName = FALSE
        IF NOT (IS_STRING_NULL_OR_EMPTY(g_sJobListMP[Get_Invite_Array_Index()].jlMissionName))
            useMissionName = TRUE
        ENDIF
        
        IF (isPlaylist)
            bodyConfiguration = "THREESTRINGSNL"
        ELSE
            bodyConfiguration = "TWOSTRINGSNL"
        ENDIF
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING(bodyConfiguration)
            IF (useMissionName)
                ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY(g_sJobListMP[Get_Invite_Array_Index()].jlMissionName)
            ELSE
                ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(GET_NAME_OF_ZONE(missionCoords))
            ENDIF
            IF (isPlayList)
                ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL("CELL_261")
            ENDIF
            ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(bodyText)
        END_TEXT_COMMAND_SCALEFORM_STRING()
        
        // Contact Picture
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(m_defaultContactPicture)
    END_SCALEFORM_MOVIE_METHOD()
    
    // Display the screen
    // NOTE: Must be after the slots have been filled
    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", (TO_FLOAT(JOB_VIEW_STATE)))
    
    // NOTE: The header and the Display Buttons will be set by the calling function as they are screen-specific

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Display the FM Current Activity details screen
PROC Display_FM_Current_Activity_Screen()
    
    // Delete all data from the JobList Screen
    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieIndex, "SET_DATA_SLOT_EMPTY", (TO_FLOAT(JOB_VIEW_STATE)))
	
	#IF IS_DEBUG_BUILD
    	NET_PRINT("...KGM MP [JobList][App]: Display_FM_Current_Activity_Screen() - clearing job details [view state: ") NET_PRINT_INT(JOB_VIEW_STATE) NET_PRINT("]") NET_NL()
	#ENDIF
	
	// Field Details
	TEXT_LABEL_63		playerName				= ""
	TEXT_LABEL_15       typeText                = ""
    TEXT_LABEL_23       textLayoutToUse         = ""
    TEXT_LABEL_63       missionName             = ""
    INT                 thisSlot                = 0
    BOOL                displayAdditionalInfo   = FALSE
    BOOL                displayFullDescription  = FALSE
    BOOL                useHeadshotTXD          = FALSE
    PEDHEADSHOT_ID      playerHeadshot          = NULL
    TEXT_LABEL_63       headshotTXD             = ""
	UGC_DESCRIPTION		splitDescriptionString

    // Store the details of the Current Mission - there is only one slot
    BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieIndex, "SET_DATA_SLOT")
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(JOB_VIEW_STATE)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(thisSlot)
		
		// Get Type text (ie: deathMatch, Mission, etc)
		typeText 	= Get_FM_Joblist_Activity_TextLabel(g_sJobListMP[Get_Invite_Array_Index()].jlMissionAsInt, g_sJobListMP[Get_Invite_Array_Index()].jlSubtype,g_sJobListMP[Get_Invite_Array_Index()].jlPropertyType, g_sJobListMP[Get_Invite_Array_Index()].jlRootContentIdHash)
		
		IF (IS_NET_PLAYER_OK(PLAYER_ID(), FALSE))
            playerName  = GET_PLAYER_NAME(PLAYER_ID())
            		
            BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
                ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(playerName)
            END_TEXT_COMMAND_SCALEFORM_STRING()
            
            useHeadshotTXD = TRUE
        ELSE
            // Invitor doesn't exist so use the mission type
            BEGIN_TEXT_COMMAND_SCALEFORM_STRING(typeText)
            END_TEXT_COMMAND_SCALEFORM_STRING()
            
            useHeadshotTXD = FALSE
        ENDIF
		
		// Display Body text
        // KGM 16/11/12: This section is getting a bit messy and assumes there is either both a mission name and description, or neither - it doesn't deal with there only being one of them but that is possibly not required
        // ...are there additional details to display?
        displayAdditionalInfo = TRUE
        IF NOT (Is_There_A_MissionsAtCoords_Focus_Mission())
            displayAdditionalInfo = FALSE
        ENDIF
        
        MP_MISSION_ID_DATA  thisMissionData
        BOOL bCasinoHeist = SHOULD_SET_CASINO_HEIST_MISSION_NAME()
		BOOL bIslandHeist = SHOULD_SET_ISLAND_HEIST_MISSION_NAME()
        IF (displayAdditionalInfo)
            thisMissionData = Get_MissionsAtCoords_Slot_MissionID_Data(Get_MissionsAtCoords_Focus_Mission_Slot())
			IF bCasinoHeist
				missionName = CASINO_HEIST_GET_HEIST_MISSION_NAME()
			ELIF bIslandHeist
				missionName = GET_ISLAND_HEIST_MISSION_NAME()
            ELSE
				missionName = Get_Mission_Name_For_FM_Cloud_Loaded_Activity(thisMissionData)
        	ENDIF
        ENDIF
	
		// Retrieve the full 500-character description?
        // NOTE: This method of retrieving the description may be temporary
        IF (displayAdditionalInfo)
		AND (g_sJobListMP[Get_Invite_Array_Index()].jlDescHash != 0)
			// ...there is a cached description ID, so retrieve the description
			Split_Cached_Description_Into_TL63s(g_sJobListMP[Get_Invite_Array_Index()].jlDescHash, splitDescriptionString)
			displayFullDescription	= TRUE
        ENDIF
		
		// If there is a mission name (and description) then display those, otherwise display the mission type
        IF (displayAdditionalInfo)
		AND NOT bIslandHeist
            // ...display mission name and description
            IF (displayFullDescription)
			AND NOT bCasinoHeist
                textLayoutToUse = "STRINGNLDESC8"
            ELSE
                textLayoutToUse = "TWOSTRINGSNL"
            ENDIF
        ELSE
            // ...display mission type
            textLayoutToUse = "STRING"
        ENDIF
            
        BEGIN_TEXT_COMMAND_SCALEFORM_STRING(textLayoutToUse)
            IF (displayAdditionalInfo)
                // ...display mission name and description
                ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY(missionName)                                          // Literal String: Mission Name from cloud
                
                IF bCasinoHeist
                    ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(CASINO_HEIST_GET_HEIST_MISSION_DESCRIPTION())
				ELIF bIslandHeist
                    ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(GET_ISLAND_HEIST_MISSION_DESCRIPTION())
				ELIF (displayFullDescription)
					INT tempLoop = 0
					REPEAT NUM_TEXTLABELS_IN_UGC_DESCRIPTION tempLoop
						ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY(splitDescriptionString.TextLabel[tempLoop])
					ENDREPEAT
                ELSE
                    ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL("NDESCRIPT")//g_sJobListMP[Get_Invite_Array_Index()].jlDescription)    // Literal String: Mission Description from cloud
                ENDIF
            ELSE
                // ...display mission type
                ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(typeText)
            ENDIF
        END_TEXT_COMMAND_SCALEFORM_STRING()
		
		// Contact Picture
        IF (useHeadshotTXD)
            // ...get the contact picture
            playerHeadshot = Get_HeadshotID_For_Player(PLAYER_ID())
                
            IF (playerHeadshot = NULL)
                useHeadshotTXD = FALSE
            ELSE
                headshotTXD = GET_PEDHEADSHOT_TXD_STRING(playerHeadshot)
            ENDIF
        ENDIF
		
		// Display the player headshot if available, or the default headshot if not
        IF (useHeadshotTXD)
            // ...display the headshot
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING(headshotTXD)
            
            #IF IS_DEBUG_BUILD
                NET_PRINT("...KGM MP [JobList][App]: Display_FM_Current_Activity_Screen() - passing player headshot TXD: ") NET_PRINT(headshotTXD) NET_NL()
            #ENDIF
        ELSE
            // ...use the default headshot
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(m_defaultContactPicture)
        ENDIF
		
		// KGM 7/2/15 [BUG 2222982]: Use Heist Background colour?
		Send_IsHeist_Params(g_sJobListMP[Get_Invite_Array_Index()].jlMissionAsInt, g_sJobListMP[Get_Invite_Array_Index()].jlSubtype, g_sJobListMP[Get_Invite_Array_Index()].jlRootContentIdHash, g_sJobListMP[Get_Invite_Array_Index()].jlAdversaryModeType)
    END_SCALEFORM_MOVIE_METHOD()
    
    // Display the screen
    // NOTE: Must be after the slots have been filled
    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", (TO_FLOAT(JOB_VIEW_STATE)))
    
    // NOTE: The header and the Display Buttons will be set by the calling function as they are screen-specific

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Display the common details of the Job screen
PROC Display_Job_Screen()

    // KGM 31/7/12: HOPEFULLY TEMP UNTIL FM USES MISSION CONTROLLER, THEN IT CAN BE REMOVED?
    IF (GET_CURRENT_GAMEMODE() = GAMEMODE_FM)
    OR (GET_CURRENT_GAMEMODE() = GAMEMODE_SP)
    OR NOT (NETWORK_IS_GAME_IN_PROGRESS())
        Display_FM_Activities_Screen()
        EXIT
    ENDIF
    
    // Delete all data from the JobList Screen
    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieIndex, "SET_DATA_SLOT_EMPTY", (TO_FLOAT(JOB_VIEW_STATE)))
    
    #IF IS_DEBUG_BUILD
        NET_PRINT("...KGM MP [JobList][App]: Display_Job_Screen() - clearing job details [view state: ") NET_PRINT_INT(JOB_VIEW_STATE) NET_PRINT("]") NET_NL()
    #ENDIF
    
    // KGM 24/7/11: Temporary data probably only needed while we use the text message view screen
    enumCharacterList   tempCharSheetID = CHAR_MP_MEX_BOSS

    // Get the Mission ID ENUM
    MP_MISSION thisMission = INT_TO_ENUM(MP_MISSION, g_sJobListMP[Get_Invite_Array_Index()].jlMissionAsInt)
    
    // Store the details of the text message - there is only one slot
    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(JOB_VIEW_STATE)),
                        (TO_FLOAT(0)), INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, 
                        g_sCharacterSheetAll[tempCharSheetID].label,
                        GET_MP_MISSION_NAME_TEXT_LABEL(thisMission, GET_PLAYER_TEAM(PLAYER_ID())),
                        g_sCharacterSheetAll[tempCharSheetID].picture)
    
    // Display the screen
    // NOTE: Must be after the slots have been filled
    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", (TO_FLOAT(JOB_VIEW_STATE)))
    
    // NOTE: The header and the Display Buttons will be set by the calling function as they are screen-specific

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Display the Current Job screen
PROC Display_Current_Job_Screen()

    // The Current Job screen is the Apps first screen now if there is a current job, so the phone should be in RunningApp mode so that it knows to return to the HomeScreen on 'BACK'.
    IF NOT (g_Cellphone.PhoneDS = PDS_RUNNINGAPP)
        g_Cellphone.PhoneDS = PDS_RUNNINGAPP
        
        #IF IS_DEBUG_BUILD
            // ...some additional debugging help - a unique identifier for each time g_Cellphone.phoneDS gets updated
            cdPrintnl()
            cdPrintstring("STATE ASSIGNMENT 197. appMPJobListNEW::Display_Current_Job_Screen() assigns PDS_RUNNINGAPP")
            cdPrintnl()
        #ENDIF
    ENDIF

    // Display the Job Screen
    IF (GET_CURRENT_GAMEMODE() = GAMEMODE_FM)

        Display_FM_Current_Activity_Screen()
    ELSE
        Display_Job_Screen()
    ENDIF
    
    // Display the Header
    Display_MP_App_Header(m_currentJobText)

    // For the Current Mission, display 'Cancel Job' and 'Back' buttons
    Display_MP_App_Buttons(MP_APP_ICON_BLANK, "", MP_APP_ICON_DELETE, m_jobListIconTextCancelJob, MP_APP_ICON_BACK, m_jobListIconTextBack, m_sAppData)
    
    // Now on the 'Current Job' screen
    m_currentStage = JLS_CURRENT_JOB
                
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Display the Joinable Job screen
PROC Display_Joinable_Job_Screen()

    // Display the Job Screen
    Display_Job_Screen()
    
    // Display the Header
    Display_MP_App_Header(m_joinableJobText)
    
    // For a Joinable Mission, display 'Accept Job' and 'Back' buttons
    Display_MP_App_Buttons(MP_APP_ICON_YES, m_jobListIconTextAcceptJob, MP_APP_ICON_BLANK, "", MP_APP_ICON_BACK, m_jobListIconTextBack, m_sAppData)
    
    // Now on the 'Joinable Job' screen
    m_currentStage = JLS_JOINABLE_JOB
                
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Display the Invitation screen
PROC Display_Invitation_Screen()
    
    // Delete all data from the JobList Screen
    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieIndex, "SET_DATA_SLOT_EMPTY", (TO_FLOAT(JOB_VIEW_STATE)))
	
	#IF IS_DEBUG_BUILD
    	NET_PRINT("...KGM MP [JobList][App]: Display_Invitation_Screen() - clearing job details [view state: ") NET_PRINT_INT(JOB_VIEW_STATE) NET_PRINT("]") NET_NL()
	#ENDIF
	
	// Field Details
	TEXT_LABEL_15       typeText                = ""
    INT                 thisSlot                = 0
    TEXT_LABEL_63       invitorTXD              = ""
    TEXT_LABEL_63       invitorName             = ""
    PEDHEADSHOT_ID      invitorHeadshot         = NULL
    PLAYER_INDEX        theInvitingPlayer       = INVALID_PLAYER_INDEX()
    enumCharacterList   theInvitingNPC          = NO_CHARACTER
    BOOL                invitorIsPlayer         = IS_BIT_SET(g_sJobListMP[Get_Invite_Array_Index()].jlBitfield, JOBLIST_BITFLAG_INVITOR_IS_PLAYER)
    BOOL                invitorIsValid          = TRUE
    BOOL                useInvitorHeadshotTXD   = FALSE
    BOOL                displayAdditionalInfo   = FALSE
    BOOL                displayFullDescription  = FALSE
    TEXT_LABEL_15       textLayoutToUse         = ""
	UGC_DESCRIPTION		splitDescriptionString
	INT					minPlayers				= g_sJobListMP[Get_Invite_Array_Index()].jlMinPlayers
	INT					maxPlayers				= g_sJobListMP[Get_Invite_Array_Index()].jlMaxPlayers
	BOOL				sameMinMaxPlayers		= FALSE
	INT					theDistance				= 0
	BOOL				rockstarMission			= TRUE
	BOOL 				isPlaylist				= FALSE 
	INT currentPlaylistJob  = 0
    INT totalPlaylistJobs   = 0
	
	
	IF (minPlayers = maxPlayers)
		sameMinMaxPlayers = TRUE
	ENDIF
	
	IF (IS_NET_PLAYER_OK(PLAYER_ID()))
	    IF NOT (IS_PED_INJURED(PLAYER_PED_ID()))
	        theDistance = ROUND(GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(PLAYER_ID()), g_sJobListMP[Get_Invite_Array_Index()].jlCoords))
	    ENDIF
	ENDIF
	
	// The 'jlInstance' field is being used to hold the creatorID
	// KGM 19/10/14: Don't display 'non R*' for a heist-related mission
	IF (g_sJobListMP[Get_Invite_Array_Index()].jlInstance < NUM_NETWORK_PLAYERS)
		rockstarMission = FALSE
		
		IF (g_sJobListMP[Get_Invite_Array_Index()].jlMissionAsInt = FMMC_TYPE_MISSION)
			IF (g_sJobListMP[Get_Invite_Array_Index()].jlSubtype = FMMC_MISSION_TYPE_HEIST)
			OR (g_sJobListMP[Get_Invite_Array_Index()].jlSubtype = FMMC_MISSION_TYPE_PLANNING)
				rockstarMission = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	// Store the details of the Invitation - there is only one slot
    BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieIndex, "SET_DATA_SLOT")
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(JOB_VIEW_STATE)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(thisSlot)
		
		// Get Type text (ie: deathMatch, Mission, etc)
		typeText 			= Get_FM_Joblist_Activity_TextLabel(g_sJobListMP[Get_Invite_Array_Index()].jlMissionAsInt, g_sJobListMP[Get_Invite_Array_Index()].jlSubtype,g_sJobListMP[Get_Invite_Array_Index()].jlPropertyType, g_sJobListMP[Get_Invite_Array_Index()].jlRootContentIdHash)
		
		// Header Line of Body (use The Invitor Name (literal string composed using the Text Commands))
		// TEMP: Invitor as In stored in 'uniqueID' field
		IF (invitorIsPlayer)
			theInvitingPlayer	= INT_TO_PLAYERINDEX(g_sJobListMP[Get_Invite_Array_Index()].jlUniqueID)
			invitorIsValid		= IS_NET_PLAYER_OK(theInvitingPlayer, FALSE)
			isPlaylist = IS_PLAYER_ON_A_PLAYLIST(theInvitingPlayer)
			
			currentPlaylistJob  = GlobalplayerBD_FM_2[NATIVE_TO_INT(theInvitingPlayer)].iPlaylistStartPos
        	totalPlaylistJobs   = GlobalplayerBD_FM[NATIVE_TO_INT(theInvitingPlayer)].sPlaylistVars.iLength
		ELSE
			theInvitingNPC		= INT_TO_ENUM(enumCharacterList, g_sJobListMP[Get_Invite_Array_Index()].jlUniqueID)
			invitorIsValid		= TRUE
		ENDIF
		
		IF (invitorIsValid)
			IF (invitorIsPlayer)
				invitorName	= GET_PLAYER_NAME(theInvitingPlayer)
			ELSE
				TEXT_LABEL	npcNameTL		= Get_NPC_Name(theInvitingNPC)
				STRING		npcNameAsString	= CONVERT_TEXT_LABEL_TO_STRING(npcNameTL)
				
				invitorName = GET_FILENAME_FOR_AUDIO_CONVERSATION(npcNameAsString)
			ENDIF
			
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
				ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(invitorName)
			END_TEXT_COMMAND_SCALEFORM_STRING()
			
			useInvitorHeadshotTXD = TRUE
		ELSE
			// Invitor doesn't exist so use the mission type
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING(typeText)
			END_TEXT_COMMAND_SCALEFORM_STRING()
			
			useInvitorHeadshotTXD = FALSE
		ENDIF
		
		// Display Body text
		// KGM 16/11/12: This section is getting a bit messy and assumes there is either both a mission name and description, or neither - it doesn't deal with there only being one of them but that is possibly not required
		// ...are there additional details to display?
		displayAdditionalInfo = TRUE
		IF (IS_STRING_NULL_OR_EMPTY(g_sJobListMP[Get_Invite_Array_Index()].jlMissionName))
			displayAdditionalInfo = FALSE
		ENDIF
		
	
		// Retrieve the full 500-character description?
		IF (displayAdditionalInfo)
			IF (g_sJobListMP[Get_Invite_Array_Index()].jlDescHash != 0)
				// ...there is a cached description ID, so retrieve the description
				Split_Cached_Description_Into_TL63s(g_sJobListMP[Get_Invite_Array_Index()].jlDescHash, splitDescriptionString)
				displayFullDescription	= TRUE
			ELSE
				// KGM 16/3/15 [BUG 2275132]: Heist Prep details from the cache will have to also retrieve the cached description
				IF (g_sJobListMP[Get_Invite_Array_Index()].jlMissionAsInt = FMMC_TYPE_MISSION)
				AND (g_sJobListMP[Get_Invite_Array_Index()].jlSubtype = FMMC_MISSION_TYPE_PLANNING)
					// This is a Heist Prep, so the description may be cached along with the heist prep header details
					// NOTE: This is the new script cache setup by Bobby, not the Code Cache of the descriptions
					Copy_Description_From_Heist_Prep_Cache_Into_TL63s(g_sJobListMP[Get_Invite_Array_Index()].jlContentID, splitDescriptionString)
					displayFullDescription = TRUE
				ENDIF
			ENDIF
			
			// Is this an NPC Invite with a specific description to display?
			IF NOT (invitorIsPlayer)
			AND NOT (IS_STRING_NULL_OR_EMPTY(g_sJobListMP[Get_Invite_Array_Index()].jlDescription))
				// ...this NPC requires a specific piece of description text to be displayed so 'split' it as though it's a 500-char description
				STRING theDescription = GET_FILENAME_FOR_AUDIO_CONVERSATION(g_sJobListMP[Get_Invite_Array_Index()].jlDescription)
				PRINTLN(".KGM [JobList][App]: NPC requires a Specific Description (split into segments for display). TL: ", g_sJobListMP[Get_Invite_Array_Index()].jlDescription)
				
				// Also 'split' it as though it is a 500-length description (clearing out the remainder)
				Split_Full_Description_String_Into_TL63s(theDescription, splitDescriptionString)
				
				displayFullDescription	= TRUE
			ENDIF
			
			// KGM 14/11/14 [BUG 2157535]: A Player invite into a Heist Cutscene should use the 'umbrella' description
			IF (invitorIsPlayer)
				IF NOT (IS_STRING_NULL_OR_EMPTY(g_sJobListMP[Get_Invite_Array_Index()].jlDescription))
					IF (ARE_STRINGS_EQUAL(g_sJobListMP[Get_Invite_Array_Index()].jlDescription, "[HEIST PREP CUTSCENE]"))
						// ...this is a player invite into a Heist Cutscene, so use the umbrella description
						INT thisHeistRCID = Joblist_Get_Heist_Finale_RCID_From_ContentID(g_sJobListMP[Get_Invite_Array_Index()].jlContentID)
						IF (thisHeistRCID != 0)
							TEXT_LABEL_15 theUmbrellaTL = GET_HEIST_STRAND_GENERIC_DESCRIPTION(thisHeistRCID)
							
							IF NOT (IS_STRING_NULL_OR_EMPTY(theUmbrellaTL))
								STRING theDescription = GET_FILENAME_FOR_AUDIO_CONVERSATION(theUmbrellaTL)
								PRINTLN(".KGM [JobList][App]:Player Invite to a Heist Cutscene uses umbrella description (split into segments for display). TL: ", theUmbrellaTL)
								
								// Also 'split' it as though it is a 500-length description (clearing out the remainder)
								Split_Full_Description_String_Into_TL63s(theDescription, splitDescriptionString)
								
								displayFullDescription	= TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		BOOL bDistance = TRUE
		
		// If there is a mission name (and description) then display those, otherwise display the mission type
		IF (displayAdditionalInfo)
			// ...choose layout that displays name (and description) with Players and Distance
			IF isPlaylist
				textLayoutToUse = "JLI_PP_PLR"
			ELIF (sameMinMaxPlayers)
				// min/max players are the same
				IF (rockstarMission)
					// rockstar mission
					textLayoutToUse = "JLI_P_NDD"
				ELSE
					// non-rockstar missions
					textLayoutToUse = "JLI_P_NDDNR"
				ENDIF
			ELSE
				// min/max players are differnt
				IF (rockstarMission)
					// If this is a rockstar SVM
					IF SVM_FLOW_IS_MISSION_SVM_FLOW_FROM_ROOT_ID(g_sJobListMP[Get_Invite_Array_Index()].jlRootContentIdHash)
					AND GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()	
						textLayoutToUse = "JLI_PP_NSVM"
						bDistance = FALSE
					ELIF WVM_FLOW_IS_MISSION_WVM_FLOW_FROM_ROOT_ID(g_sJobListMP[Get_Invite_Array_Index()].jlRootContentIdHash)
					AND GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()	
						textLayoutToUse = "JLI_PP_NWVM"
						bDistance = FALSE
					// rockstar mission
					ELSE
						textLayoutToUse = "JLI_PP_NDD"
					ENDIF
				ELSE
					// non-rockstar missions
					textLayoutToUse = "JLI_PP_NDDNR"
				ENDIF
			ENDIF
		ELSE
			// ...display mission type
			textLayoutToUse = "STRING"
		ENDIF
			
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING(textLayoutToUse)
			IF (displayAdditionalInfo)
				// ...display as many details as possible
				// ...minimum players, and max players if required
				IF isPlaylist
					ADD_TEXT_COMPONENT_INTEGER(currentPlaylistJob)
					ADD_TEXT_COMPONENT_INTEGER(totalPlaylistJobs)
				ELSE
					ADD_TEXT_COMPONENT_INTEGER(minPlayers)
					IF NOT (sameMinMaxPlayers)
						ADD_TEXT_COMPONENT_INTEGER(maxPlayers)
					ENDIF
				ENDIF
				// ...mission name
				ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY(g_sJobListMP[Get_Invite_Array_Index()].jlMissionName)		// Literal String: Mission Name from cloud
				
				IF bDistance
					// ...distance
					ADD_TEXT_COMPONENT_INTEGER(theDistance)
				ENDIF
				
				// ...description
				IF (displayFullDescription)
					// ...full description as split from 500-char field
					INT tempLoop = 0
					REPEAT NUM_TEXTLABELS_IN_UGC_DESCRIPTION tempLoop
						ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY(splitDescriptionString.TextLabel[tempLoop])
					ENDREPEAT
				ELSE
					// ...the mission description from the cloud, and clear the remaining 7 expected strings
					INT tempLoop = 0
					REPEAT NUM_TEXTLABELS_IN_UGC_DESCRIPTION tempLoop
						IF (tempLoop = 0)
							// Literal String: Mission Description from cloud
							ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY(g_sJobListMP[Get_Invite_Array_Index()].jlDescription)
						ELSE
							// Clear out the remaining expected string components
							ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY("")
						ENDIF
					ENDREPEAT
				ENDIF
			ELSE
				// ...display mission type
				ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(typeText)
			ENDIF
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		// Contact Picture
		IF (useInvitorHeadshotTXD)
			// ...the invitorID is valid, so try to get the contact picture
			IF (invitorIsPlayer)
				invitorHeadshot = Get_HeadshotID_For_Player(theInvitingPlayer)
				
				IF (invitorHeadshot = NULL)
					useInvitorHeadshotTXD = FALSE
				ELSE
					invitorTXD = GET_PEDHEADSHOT_TXD_STRING(invitorHeadshot)
				ENDIF
			ELSE
				invitorTXD = GET_FILENAME_FOR_AUDIO_CONVERSATION(Get_NPC_Headshot(theInvitingNPC))
			ENDIF
			
		ENDIF
		
		// Display the player headshot if available, or the default headshot if not
		IF (useInvitorHeadshotTXD)
			// ...display the headshot
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING(invitorTXD)
			
			#IF IS_DEBUG_BUILD
		    	NET_PRINT("...KGM MP [JobList][App]: Display_Invitation_Screen() - passing player headshot TXD: ") NET_PRINT(invitorTXD) NET_NL()
			#ENDIF
		ELSE
			// ...use the default headshot
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(m_defaultContactPicture)
		ENDIF
		
		// KGM 7/2/15 [BUG 2222982]: Use Heist Background colour?
		Send_IsHeist_Params(g_sJobListMP[Get_Invite_Array_Index()].jlMissionAsInt, g_sJobListMP[Get_Invite_Array_Index()].jlSubtype, g_sJobListMP[Get_Invite_Array_Index()].jlRootContentIdHash, g_sJobListMP[Get_Invite_Array_Index()].jlAdversaryModeType)
    END_SCALEFORM_MOVIE_METHOD()
    
    // Display the screen
    // NOTE: Must be after the slots have been filled
    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", (TO_FLOAT(JOB_VIEW_STATE)))

    // Display the Header - the Mission Type
    Display_MP_App_Header(m_invitationText)
    
    // For an Invitation, display 'Accept Job' and 'Back' buttons
    // KGM 23/6/13: Also display 'delete' button for Invites from other players
    // KGM 10/7/13: Also display 'delete' button for Invites from NPCs
    Display_MP_App_Buttons(MP_APP_ICON_YES, m_jobListIconTextAcceptJob, MP_APP_ICON_DELETE, m_jobListIconTextCancelJob, MP_APP_ICON_BACK, m_jobListIconTextBack, m_sAppData)
	
	// KGM 22/7/15 [BUG 2437060]: Display a help message warning when on certain FM Events
	Display_Accept_Invite_Warning_Help_Text_If_Required()
    
    // Now on the 'Invitation' screen
    m_currentStage = JLS_INVITATION
                
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Display the Cross-Session Invite screen
PROC Display_CSInvite_Screen()
    
    // Delete all data from the JobList Screen
    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieIndex, "SET_DATA_SLOT_EMPTY", (TO_FLOAT(JOB_VIEW_STATE)))
	
	#IF IS_DEBUG_BUILD
    	NET_PRINT("...KGM MP [JobList][App]: Display_CSInvite_Screen() - clearing job details [view state: ") NET_PRINT_INT(JOB_VIEW_STATE) NET_PRINT("]") NET_NL()
	#ENDIF
	
	// Field Details
	TEXT_LABEL_15		typeText				= ""	
	INT					thisSlot				= 0
	TEXT_LABEL_63		invitorName				= ""
	BOOL				displayAdditionalInfo	= FALSE
	BOOL				displayFullDescription	= FALSE
	TEXT_LABEL_15		textLayoutToUse			= ""
	UGC_DESCRIPTION		splitDescriptionString
	
	// Store the details of the Invitation - there is only one slot
    BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieIndex, "SET_DATA_SLOT")
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(JOB_VIEW_STATE)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(thisSlot)
		
		// Get Type text (ie: deathMatch, Mission, etc)
		typeText 			= Get_FM_Joblist_Activity_TextLabel(g_sJobListMP[Get_Invite_Array_Index()].jlMissionAsInt, g_sJobListMP[Get_Invite_Array_Index()].jlSubtype,g_sJobListMP[Get_Invite_Array_Index()].jlPropertyType, g_sJobListMP[Get_Invite_Array_Index()].jlRootContentIdHash)
		
		#IF FEATURE_COPS_N_CROOKS
		IF IS_THIS_CLOUD_FILE_NAME_ARCADE_MODE(g_sJobListMP[Get_Invite_Array_Index()].jlContentID)
			typeText = g_sJobListMP[Get_Invite_Array_Index()].jlContentID
		ENDIF
		#ENDIF
		
		// Header Line of Body (use The Creator Name (literal string composed using the Text Commands))
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			invitorName = "<C>"
			invitorName += g_sJobListMP[Get_Invite_Array_Index()].jlInvitorName
			invitorName += "</C>"
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(invitorName)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		// Display Body text
		// KGM 16/11/12: This section is getting a bit messy and assumes there is either both a mission name and description, or neither - it doesn't deal with there only being one of them but that is possibly not required
		// ...are there additional details to display?
		displayAdditionalInfo = TRUE
		IF (IS_STRING_NULL_OR_EMPTY(g_sJobListMP[Get_Invite_Array_Index()].jlMissionName))
			displayAdditionalInfo = FALSE
		ENDIF
		
	
		// Retrieve the full 500-character description?
		// NOTE: This method of retrieving the description may be temporary
		IF (displayAdditionalInfo)
			IF (g_sJobListMP[Get_Invite_Array_Index()].jlDescHash != 0)
				// ...there is a cached description ID, so retrieve the description
				Split_Cached_Description_Into_TL63s(g_sJobListMP[Get_Invite_Array_Index()].jlDescHash, splitDescriptionString)
				displayFullDescription	= TRUE
			ELSE
				// KGM 16/3/15 [BUG 2275132]: Heist Prep details from the cache will have to also retrieve the cached description
				IF (g_sJobListMP[Get_Invite_Array_Index()].jlMissionAsInt = FMMC_TYPE_MISSION)
				AND (g_sJobListMP[Get_Invite_Array_Index()].jlSubtype = FMMC_MISSION_TYPE_PLANNING)
					// This is a Heist Prep, so the description may be cached along with the heist prep header details
					// NOTE: This is the new script cache setup by Bobby, not the Code Cache of the descriptions
					Copy_Description_From_Heist_Prep_Cache_Into_TL63s(g_sJobListMP[Get_Invite_Array_Index()].jlContentID, splitDescriptionString)
					displayFullDescription = TRUE
				ENDIF
			ENDIF
		ENDIF
		
		// If there is a mission name (and description) then display those, otherwise display the mission type
		IF (displayAdditionalInfo)
			// ...display mission name and description
			IF (displayFullDescription)
				textLayoutToUse = "STRINGNLDESC8"
			ELSE
				textLayoutToUse = "TWOSTRINGSNL"
			ENDIF
		ELSE
			// ...display mission type
			textLayoutToUse = "STRING"
		ENDIF
			
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING(textLayoutToUse)
			IF (displayAdditionalInfo)
				// ...display mission name and description
				ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY(g_sJobListMP[Get_Invite_Array_Index()].jlMissionName)		// Literal String: Mission Name from cloud
				
				IF (displayFullDescription)
					INT tempLoop = 0
					REPEAT NUM_TEXTLABELS_IN_UGC_DESCRIPTION tempLoop
						ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY(splitDescriptionString.TextLabel[tempLoop])
					ENDREPEAT
				ELSE
					ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY(g_sJobListMP[Get_Invite_Array_Index()].jlDescription)	// Literal String: Mission Description from cloud
				ENDIF
			ELSE
				// ...display mission type
				ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(typeText)
			ENDIF
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		// ...use the default headshot
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(m_defaultContactPicture)
		
		// KGM 7/2/15 [BUG 2222982]: Use Heist Background colour?
		Send_IsHeist_Params(g_sJobListMP[Get_Invite_Array_Index()].jlMissionAsInt, g_sJobListMP[Get_Invite_Array_Index()].jlSubtype, g_sJobListMP[Get_Invite_Array_Index()].jlRootContentIdHash, g_sJobListMP[Get_Invite_Array_Index()].jlAdversaryModeType)
    END_SCALEFORM_MOVIE_METHOD()
    
    // Display the screen
    // NOTE: Must be after the slots have been filled
    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", (TO_FLOAT(JOB_VIEW_STATE)))

    // Display the Header - the Mission Type
    Display_MP_App_Header(m_invitationText)
    
    // For an Invitation, display 'Accept Job' and 'Back' buttons
    // NOTE: All Cross-Session Invites are from players so all should display the 'delete' icon
    Display_MP_App_Buttons(MP_APP_ICON_YES, m_jobListIconTextAcceptJob, MP_APP_ICON_DELETE, m_jobListIconTextCancelJob, MP_APP_ICON_BACK, m_jobListIconTextBack, m_sAppData)
	
	// KGM 22/7/15 [BUG 2437060]: Display a help message warning when on certain FM Events
	Display_Accept_Invite_Warning_Help_Text_If_Required()
    
    // Now on the 'Invitation' screen
    m_currentStage = JLS_CSINVITE
                
ENDPROC


FUNC INT GET_STYLE_FROM_GANG_ID(INT iGangID)

	PRINTLN("...KGM MP [JobList][App][Biker]: GET_STYLE_FROM_GANG_ID() - iGangID: ", iGangID)

	IF iGangID != -1
		RETURN (GET_HUD_COLOUR_SLOT_ASSIGNED_TO_GANG(iGangID) + STYLE_OFFSET)
	ENDIF

	RETURN 0

ENDFUNC

FUNC INT GET_LOCAL_PLAYER_STYLE(INT iInviteSlot)

	IF iInviteSlot != -1
		RETURN GET_STYLE_FROM_GANG_ID(GET_GANG_ID_FOR_PLAYER(g_sBossAgencyApp.bossAgencyInvites[iInviteSlot].playerInvitor))
	ENDIF
	
	RETURN 0

ENDFUNC

// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Display the Basic Invite screen
// NOTES:   20/6/13:    FMMC_TYPE_RACE_TO_POINT needs to display additional details on the Invite screen - this will be a hardcoded structure
PROC Display_RVInvite_Screen()
    
    // Delete all data from the JobList Screen
    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieIndex, "SET_DATA_SLOT_EMPTY", (TO_FLOAT(JOB_VIEW_STATE)))
	
	#IF IS_DEBUG_BUILD
    	NET_PRINT("...KGM MP [JobList][App]: Display_RVInvite_Screen() - clearing job details [view state: ") NET_PRINT_INT(JOB_VIEW_STATE) NET_PRINT("]") NET_NL()
	#ENDIF
	
	// Field Details
	TEXT_LABEL_15		typeText				= ""
	INT					thisSlot				= 0
	TEXT_LABEL_63		invitorTXD				= ""
	TEXT_LABEL_63		invitorName				= ""
	PEDHEADSHOT_ID		invitorHeadshot			= NULL
	PLAYER_INDEX		theInvitingPlayer		= INVALID_PLAYER_INDEX()
	enumCharacterList	theInvitingNPC			= NO_CHARACTER
	BOOL				invitorIsPlayer			= IS_BIT_SET(g_sJobListMP[Get_Invite_Array_Index()].jlBitfield, JOBLIST_BITFLAG_INVITOR_IS_PLAYER)
	BOOL				invitorIsValid			= TRUE
	BOOL				useInvitorHeadshotTXD	= FALSE
	BOOL				displayMissionName		= FALSE
	TEXT_LABEL_23		textLayoutToUse			= ""
	
	// Store the details of the Invitation - there is only one slot
    BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieIndex, "SET_DATA_SLOT")
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(JOB_VIEW_STATE)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(thisSlot)
		
		// Get Type text (ie: deathMatch, Mission, etc)
		typeText 	= Get_FM_Joblist_Activity_TextLabel(g_sJobListMP[Get_Invite_Array_Index()].jlMissionAsInt, g_sJobListMP[Get_Invite_Array_Index()].jlSubtype,g_sJobListMP[Get_Invite_Array_Index()].jlPropertyType, g_sJobListMP[Get_Invite_Array_Index()].jlRootContentIdHash)
		
		// Header Line of Body (use The Invitor Name (literal string composed using the Text Commands))
		// TEMP: Invitor as In stored in 'uniqueID' field
		IF (invitorIsPlayer)
			theInvitingPlayer	= INT_TO_PLAYERINDEX(g_sJobListMP[Get_Invite_Array_Index()].jlUniqueID)
			invitorIsValid		= IS_NET_PLAYER_OK(theInvitingPlayer, FALSE)
		ELSE
			theInvitingNPC		= INT_TO_ENUM(enumCharacterList, g_sJobListMP[Get_Invite_Array_Index()].jlUniqueID)
			invitorIsValid		= TRUE
		ENDIF
		
		IF (invitorIsValid)
			IF (invitorIsPlayer)
				invitorName	= GET_PLAYER_NAME(theInvitingPlayer)
			ELSE
				TEXT_LABEL	npcNameTL		= Get_NPC_Name(theInvitingNPC)
				STRING		npcNameAsString	= CONVERT_TEXT_LABEL_TO_STRING(npcNameTL)
				
				invitorName = GET_FILENAME_FOR_AUDIO_CONVERSATION(npcNameAsString)
			ENDIF
			
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
				ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(invitorName)
			END_TEXT_COMMAND_SCALEFORM_STRING()
			
			useInvitorHeadshotTXD = TRUE
		ELSE
			// Invitor doesn't exist so use the mission type
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING(typeText)
			END_TEXT_COMMAND_SCALEFORM_STRING()
			
			useInvitorHeadshotTXD = FALSE
		ENDIF
		
		// Display Body text
		// ...are there additional details to display?
		displayMissionName = TRUE
		IF (IS_STRING_NULL_OR_EMPTY(g_sJobListMP[Get_Invite_Array_Index()].jlMissionName))
			displayMissionName = FALSE
		ENDIF
		
		// If there is a mission name then display it, otherwise display the mission type
		textLayoutToUse = "STRING"
		
		// KGM 20/6/13: IMPROMPTU RACE NEEDS ADDITIONAL DETAILS: Destination, Distance, Cost
		//				Only add this if all the data is valid
		BOOL useImpromptuRaceLayout = FALSE
		IF (g_sJobListMP[Get_Invite_Array_Index()].jlMissionAsInt = FMMC_TYPE_RACE_TO_POINT)
			IF NOT (IS_STRING_NULL_OR_EMPTY(g_sJobListMP[Get_Invite_Array_Index()].jlDescription))
			AND (g_sJobListMP[Get_Invite_Array_Index()].jlDescHash != -1)
			AND (g_sJobListMP[Get_Invite_Array_Index()].jlInstance != -1)
				textLayoutToUse			= "JL_IMP_RACE"
				useImpromptuRaceLayout	= TRUE
			ELSE
				#IF IS_DEBUG_BUILD
			    	NET_PRINT("...KGM MP [JobList][App]: Display_RVInvite_Screen() - Impromptu Race contains additional details that are not valid, so not displaying them") NET_NL()
					NET_PRINT("      DESTINATION: ")
					IF NOT (IS_STRING_NULL_OR_EMPTY(g_sJobListMP[Get_Invite_Array_Index()].jlDescription))
						NET_PRINT(g_sJobListMP[Get_Invite_Array_Index()].jlDescription)
					ELSE
						NET_PRINT(" <NULL>   [INVALID DATA]")
					ENDIF
					NET_NL()
					NET_PRINT("      DISTANCE   : ")
					IF (g_sJobListMP[Get_Invite_Array_Index()].jlDescHash != -1)
						NET_PRINT_INT(g_sJobListMP[Get_Invite_Array_Index()].jlDescHash)
					ELSE
						NET_PRINT(" -1   [INVALID DATA]")
					ENDIF
					NET_NL()
					NET_PRINT("      COST       : ")
					IF (g_sJobListMP[Get_Invite_Array_Index()].jlInstance != -1)
						NET_PRINT_INT(g_sJobListMP[Get_Invite_Array_Index()].jlInstance)
					ELSE
						NET_PRINT(" -1   [INVALID DATA]")
					ENDIF
					NET_NL()
				#ENDIF
			ENDIF
		ENDIF
		
		// KGM 12/7/13: ONE ON ONE DEATHMATCH NEEDS ADDITIONAL DETAILS: Cost
		//				Only add this if all the data is valid
		BOOL useOneOnOneDMLayout = FALSE
		IF (g_sJobListMP[Get_Invite_Array_Index()].jlMissionAsInt = FMMC_TYPE_IMPROMPTU_DM)
		OR (g_sJobListMP[Get_Invite_Array_Index()].jlMissionAsInt = FMMC_TYPE_GB_BOSS_DEATHMATCH)
			IF (g_sJobListMP[Get_Invite_Array_Index()].jlDescHash != -1)
				textLayoutToUse			= "JL_1ON1_DM"
				useOneOnOneDMLayout		= TRUE
			ELSE
				#IF IS_DEBUG_BUILD
			    	NET_PRINT("...KGM MP [JobList][App]: Display_RVInvite_Screen() - One On One DM contains additional details that are not valid, so not displaying them") NET_NL()
					NET_PRINT("      COST       : ")
					IF (g_sJobListMP[Get_Invite_Array_Index()].jlDescHash != -1)
						NET_PRINT_INT(g_sJobListMP[Get_Invite_Array_Index()].jlDescHash)
					ELSE
						NET_PRINT(" -1   [INVALID DATA]")
					ENDIF
					NET_NL()
				#ENDIF
			ENDIF
		ENDIF
		
		// CV 19/8/13: APARTMENT INVITES NEEDS ADDITIONAL DETAILS: Cost
		//				Only add this if all the data is valid
		BOOL useApartmentLayout = FALSE
		IF (g_sJobListMP[Get_Invite_Array_Index()].jlMissionAsInt = FMMC_TYPE_APARTMENT)
			IF NOT (IS_STRING_NULL_OR_EMPTY(g_sJobListMP[Get_Invite_Array_Index()].jlDescription))
				textLayoutToUse			= "TWOSTRINGSNL"
				useApartmentLayout		= TRUE
			ELSE
				#IF IS_DEBUG_BUILD
			    	NET_PRINT("...KGM MP [JobList][App]: Display_RVInvite_Screen() - Apartment invite contains additional details that are not valid, so not displaying them") NET_NL()
					NET_PRINT("      Description       : ")
					IF NOT (IS_STRING_NULL_OR_EMPTY(g_sJobListMP[Get_Invite_Array_Index()].jlDescription))
						NET_PRINT(g_sJobListMP[Get_Invite_Array_Index()].jlDescription)
					ELSE
						NET_PRINT(" <NULL>   [INVALID DATA]")
					ENDIF
					NET_NL()					
				#ENDIF
			ENDIF
		ENDIF
		
		IF (g_sJobListMP[Get_Invite_Array_Index()].jlMissionAsInt = FMMC_TYPE_SIMPLE_INTERIOR)
		#IF FEATURE_HEIST_ISLAND
		OR g_sJobListMP[Get_Invite_Array_Index()].jlMissionAsInt = FMMC_TYPE_HEIST_ISLAND_INVITE
		#ENDIF
		#IF FEATURE_TUNER
		OR g_sJobListMP[Get_Invite_Array_Index()].jlMissionAsInt = FMMC_TYPE_FMC_INVITE
		#ENDIF
			IF NOT (IS_STRING_NULL_OR_EMPTY(g_sJobListMP[Get_Invite_Array_Index()].jlDescription))
				textLayoutToUse			= "TWOSTRINGSNL"
				useApartmentLayout		= TRUE
			ELSE
				PRINTLN("[SIMPLE_INTERIOR][INVITES] jlDescription is empty.")
			ENDIF
		ENDIF
		
		// KGM 22/1/15: Quick Heist Invites need different details based on whether the mission will be a Prep or Finale
		BOOL addAdditionalText = TRUE
		IF g_sJobListMP[Get_Invite_Array_Index()].jlMissionAsInt = FMMC_TYPE_HEIST_PHONE_INVITE
		OR g_sJobListMP[Get_Invite_Array_Index()].jlMissionAsInt = FMMC_TYPE_HEIST_PHONE_INVITE_2
		OR g_sJobListMP[Get_Invite_Array_Index()].jlMissionAsInt = FMMC_TYPE_VCM_QUICK_INVITE
			IF GANGOPS_FLOW_IS_A_GANGOPS_MISSION_FROM_ROOT_ID(g_sJobListMP[Get_Invite_Array_Index()].jlRootContentIdHash)
				textLayoutToUse = "CELL_268_GO_DETAIL"	// Continue Heist: setup
			ELIF VCM_FLOW_IS_THIS_MISSION_VCM_FLOW(g_sJobListMP[Get_Invite_Array_Index()].jlRootContentIdHash)
				textLayoutToUse = "CELL_268_RETRY"	// Retry : setup
			ELIF (g_sJobListMP[Get_Invite_Array_Index()].jlSubtype = FMMC_MISSION_TYPE_PLANNING)
				textLayoutToUse = "CELL_268_PREP_DETAIL"	// Continue Heist: setup
				addAdditionalText = FALSE
				displayMissionName = FALSE
			ELSE
				textLayoutToUse = "CELL_268_HEIST_DETAIL"	// Continue Heist: <Finale Name>
			ENDIF
		ELIF g_sJobListMP[Get_Invite_Array_Index()].jlMissionAsInt = FMMC_TYPE_IH_QUICK_INVITE
			textLayoutToUse = "IHQUICK_BODY" // Continue: Prep work
		ENDIF
		
        BEGIN_TEXT_COMMAND_SCALEFORM_STRING(textLayoutToUse)
            IF (displayMissionName)
                // ...display mission name
                ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY(g_sJobListMP[Get_Invite_Array_Index()].jlMissionName)        // Literal String: Mission Name
			ELSE
                // ...display mission type
				IF (addAdditionalText)
                	ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(typeText)
				ENDIF
            ENDIF
            
            IF (useImpromptuRaceLayout)
                // All the additional parameters are squeezed into whatever Joblist entries happened to be free
                // ...destination
                ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_sJobListMP[Get_Invite_Array_Index()].jlDescription)
                // ...distance
                ADD_TEXT_COMPONENT_INTEGER(g_sJobListMP[Get_Invite_Array_Index()].jlDescHash)
                // ...cost
                ADD_TEXT_COMPONENT_INTEGER(g_sJobListMP[Get_Invite_Array_Index()].jlInstance)
            ENDIF
            
            IF (useOneOnOneDMLayout)
                // All the additional parameters are squeezed into whatever Joblist entries happened to be free
                // ...cost
                ADD_TEXT_COMPONENT_INTEGER(g_sJobListMP[Get_Invite_Array_Index()].jlDescHash)
            ENDIF
			IF (useApartmentLayout)
				// ...destination
                ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_sJobListMP[Get_Invite_Array_Index()].jlDescription)
				
			ENDIF
        END_TEXT_COMMAND_SCALEFORM_STRING()
        
        // Contact Picture
        IF (useInvitorHeadshotTXD)
            // ...the invitorID is valid, so try to get the contact picture
            IF (invitorIsPlayer)
                invitorHeadshot = Get_HeadshotID_For_Player(theInvitingPlayer)
                
                IF (invitorHeadshot = NULL)
                    useInvitorHeadshotTXD = FALSE
                ELSE
                    invitorTXD = GET_PEDHEADSHOT_TXD_STRING(invitorHeadshot)
                ENDIF
            ELSE
                invitorTXD = GET_FILENAME_FOR_AUDIO_CONVERSATION(Get_NPC_Headshot(theInvitingNPC))
            ENDIF
            
        ENDIF
        
        // Display the player headshot if available, or the default headshot if not
        IF (useInvitorHeadshotTXD)
            // ...display the headshot
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING(invitorTXD)
            
            #IF IS_DEBUG_BUILD
                NET_PRINT("...KGM MP [JobList][App]: Display_RVInvite_Screen() - passing player headshot TXD: ") NET_PRINT(invitorTXD) NET_NL()
            #ENDIF
        ELSE
            // ...use the default headshot
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(m_defaultContactPicture)
        ENDIF
		
		IF g_sJobListMP[Get_Invite_Array_Index()].jlMissionAsInt = FMMC_TYPE_SIMPLE_INTERIOR
		AND GB_IS_LOCAL_PLAYER_MEMBER_OF_A_BIKER_GANG()
			INT iStyle = GET_LOCAL_PLAYER_STYLE(thisSlot)
			PRINTLN("[SIMPLE_INTERIOR][INVITES] - Display_RVInvite_Screen - iStyle: ", iStyle)
			// Colour of invite box
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStyle)
		ELSE
		// KGM 7/2/15 [BUG 2222982]: Use Heist Background colour?
		Send_IsHeist_Params(g_sJobListMP[Get_Invite_Array_Index()].jlMissionAsInt, g_sJobListMP[Get_Invite_Array_Index()].jlSubtype, g_sJobListMP[Get_Invite_Array_Index()].jlRootContentIdHash, g_sJobListMP[Get_Invite_Array_Index()].jlAdversaryModeType)
		ENDIF
	
	END_SCALEFORM_MOVIE_METHOD()
    
    // Display the screen
    // NOTE: Must be after the slots have been filled
    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", (TO_FLOAT(JOB_VIEW_STATE)))

    // Display the Header - the Mission Type
    Display_MP_App_Header(m_invitationText)
    
    // For an Invitation, display 'Accept Job' and 'Back' buttons
    // KGM 23/6/13: Basic Invites won't get the 'delete' option.
    Display_MP_App_Buttons(MP_APP_ICON_YES, m_jobListIconTextAcceptJob, MP_APP_ICON_BLANK, "", MP_APP_ICON_BACK, m_jobListIconTextBack, m_sAppData)
    
    // Now on the 'Invitation' screen
    m_currentStage = JLS_RVINVITE
                
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Display the Fake Invitation screen
PROC Display_Fake_Invitation_Screen()
    
    // Delete all data from the JobList Screen
    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieIndex, "SET_DATA_SLOT_EMPTY", (TO_FLOAT(JOB_VIEW_STATE)))
    
    #IF IS_DEBUG_BUILD
        NET_PRINT("...KGM MP [JobList][App]: Display_Fake_Invitation_Screen() - clearing job details [view state: ") NET_PRINT_INT(JOB_VIEW_STATE) NET_PRINT("]") NET_NL()
    #ENDIF
    
    // Field Details
    TEXT_LABEL_15   typeText                = ""
    INT             thisSlot                = 0
    TEXT_LABEL_63   invitingChar            = GLOBAL_CHARACTER_SHEET_GET_LABEL(g_fakeJoblistInvite.fjiSender)
    STRING          invitorTXD              = GET_FILENAME_FOR_AUDIO_CONVERSATION(g_sCharacterSheetAll[g_fakeJoblistInvite.fjiSender].picture)
    
    // Store the details of the Fake Invitation - there is only one slot
    BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieIndex, "SET_DATA_SLOT")
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(JOB_VIEW_STATE)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(thisSlot)
		
		// Get Type text (ie: deathMatch, Mission, etc)
		typeText 	= Get_FM_Joblist_Activity_TextLabel(g_fakeJoblistInvite.fjiType)
		
		// Header Line of Body (display the invitor name)
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING(invitingChar)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		// Display Body text (assume the activity creator is Rockstar and that there is only a mission name, no decription)
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("THREESTRINGSNL")
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL("CELL_266")		// "RockStar"
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(typeText)
			ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY(g_fakeJoblistInvite.fjiMissionName)		// Literal String
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		// Display the character headshot
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING(invitorTXD)
			
		#IF IS_DEBUG_BUILD
	    	NET_PRINT("...KGM MP [JobList][App]: Display_Invitation_Screen() - passing character headshot TXD: ") NET_PRINT(invitorTXD) NET_NL()
		#ENDIF
    END_SCALEFORM_MOVIE_METHOD()
    
    // Display the screen
    // NOTE: Must be after the slots have been filled
    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", (TO_FLOAT(JOB_VIEW_STATE)))

    // Display the Header - the Mission Type
    Display_MP_App_Header(m_invitationText)
    
    // For an Invitation, display 'Accept Job' and 'Back' buttons
    Display_MP_App_Buttons(MP_APP_ICON_YES, m_jobListIconTextAcceptJob, MP_APP_ICON_BLANK, "", MP_APP_ICON_BACK, m_jobListIconTextBack, m_sAppData)
    
    // Now on the 'Fake Invitation' screen
    m_currentStage = JLS_FAKE_INVITATION
                
ENDPROC




// -----------------------------------------------------------------------------------------------------------
//      JobList screen
// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Fill the scaleform slot to indicate 'No Jobs Available To Join'
PROC Fill_FM_Joblist_Screen_No_Jobs()

    BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieIndex, "SET_DATA_SLOT")
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(JOBLIST_VIEW_STATE)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
	
		// KGM 28/1/15: Extract the 'no jobs available to join' display into it's own fucntion so it can be coloured the same as the heists
		#IF IS_DEBUG_BUILD
			PRINTLN("...KGM MP [JobList][App]: FM JOBLIST SCALEFORM SLOT DATA (Invitation) [View State: ", JOBLIST_VIEW_STATE, "] [Slot 0]")
		#ENDIF
		
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("CELL_240")		// No Jobs
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("CELL_241")		// Available to Join

		// No Blip, Not Heist, Is Multiplayer?
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(-1)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
	
		BOOL isMultiplayer = NETWORK_IS_GAME_IN_PROGRESS()
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(isMultiplayer)
    END_SCALEFORM_MOVIE_METHOD()
		
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Fill the scaleform slot details for a current or joinable mission
//
// INPUT PARAMS:            paramSlot           The scaleform slot being filled
//                          paramArrayPos       The Joblist Array Position the data is being taken from
PROC Fill_FM_Joblist_Screen_Slot_Current_or_Joinable_Mission(INT paramSlot, INT paramArrayPos)

    TEXT_LABEL_63   playerName          = ""
    TEXT_LABEL_15   typeText            = ""
    TEXT_LABEL_23   playlistConfig      = ""
    INT             theCreator          = 0
    INT             joblistPlayerGBD    = -1
    VECTOR          missionCoords       = << 0.0, 0.0, 0.0 >>
    BOOL            useMissionName      = FALSE
    BOOL            creatorIsAPlayer    = FALSE
    BOOL            creatorIsRockstar   = FALSE
    BOOL            isPlaylist          = FALSE
    BOOL            isPlaylistLobby     = FALSE
    BOOL            isAffordable        = FALSE
    PLAYER_INDEX    thisPlayer
	
    // Display regular details
    BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieIndex, "SET_DATA_SLOT")
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(JOBLIST_VIEW_STATE)
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(paramSlot)
    
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList][App]: FM JOBLIST SCALEFORM SLOT DATA")
            SWITCH (g_sJobListMP[paramArrayPos].jlState)
                CASE MP_JOBLIST_CURRENT_MISSION
                    NET_PRINT(" (Current)")
                    BREAK
                CASE MP_JOBLIST_JOINABLE_MISSION
                    NET_PRINT(" (Joinable)")
                    BREAK
                CASE MP_JOBLIST_JOINABLE_PLAYLIST
                    NET_PRINT(" (Joinable Playlist)")
                    BREAK
                DEFAULT
                    NET_PRINT(" (Unknown Joblist State)")
                    BREAK
            ENDSWITCH
            NET_PRINT(" [View State: ")
            NET_PRINT_INT(JOBLIST_VIEW_STATE)
            NET_PRINT("] [Slot ")
            NET_PRINT_INT(paramSlot)
            NET_PRINT("]")
            NET_NL()
        #ENDIF
        
        // 1st Line displays:   (BOUNTY) <Mission Name> (or 'Rockstar' if the player is in a playlist lobby)
        joblistPlayerGBD    = g_sJobListMP[paramArrayPos].jlUniqueID
        
        // ...use the mission name if available, otherwise use the missionCoords to get the map area
        missionCoords       = GlobalplayerBD_FM[joblistPlayerGBD].currentMissionData.mdPrimaryCoords
        useMissionName      = FALSE
        IF NOT (IS_STRING_NULL_OR_EMPTY(g_sJobListMP[paramArrayPos].jlMissionName))
            useMissionName = TRUE
        ENDIF
        
        // Gather playlist details
        isPlaylist = FALSE
        IF (g_sJobListMP[paramArrayPos].jlState = MP_JOBLIST_JOINABLE_PLAYLIST)
            isPlayList = TRUE
            
            // ...is still in playlist lobby?
            IF (g_sJobListMP[paramArrayPos].jlMissionAsInt = FMMC_TYPE_PLAYLIST)
                isPlaylistLobby = TRUE
            ENDIF
        ENDIF

		// If the player is in a playlist lobby (in-between missions) then display 'PLAYLIST' as the top line
		IF (isPlaylistLobby)
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("CELL_261")
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ELSE
			IF (IS_BIT_SET(g_sJobListMP[paramArrayPos].jlBitfield, JOBLIST_BITFLAG_CONTAINS_BOUNTY_TARGET))
			AND (g_sJobListMP[paramArrayPos].jlState = MP_JOBLIST_JOINABLE_MISSION)
				// This Joinable mission does contain a Bounty Target, so display an indicator
				BEGIN_TEXT_COMMAND_SCALEFORM_STRING("TWOSTRINGS")
					ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL("BB_JOBLIST")
					IF (useMissionName)
						ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY(g_sJobListMP[paramArrayPos].jlMissionName)
					ELSE
						ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(GET_NAME_OF_ZONE(missionCoords))
					ENDIF
				END_TEXT_COMMAND_SCALEFORM_STRING()
			ELSE
				// This mission does not contain the player's Bounty Target or is the CURRENT mission, so just display as-is
				BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
					IF (useMissionName)
						ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY(g_sJobListMP[paramArrayPos].jlMissionName)
					ELSE
						ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(GET_NAME_OF_ZONE(missionCoords))
					ENDIF
				END_TEXT_COMMAND_SCALEFORM_STRING()
			ENDIF
				
			#IF IS_DEBUG_BUILD
				NET_PRINT("         Mission Name: ")
				NET_PRINT(g_sJobListMP[paramArrayPos].jlMissionName)
				NET_PRINT("   ")
				NET_PRINT("(Coords: ")
				NET_PRINT_VECTOR(missionCoords)
				NET_PRINT("  [")
				NET_PRINT(GET_STRING_FROM_TEXT_FILE(GET_NAME_OF_ZONE(missionCoords)))
				NET_PRINT("])")
				NET_NL()
			#ENDIF
		ENDIF
		
		// 2nd Line displays:	<CreatorID or 'PLAYLIST'> <Mission Type>
		typeText	= Get_FM_Joblist_Activity_TextLabel(g_sJobListMP[paramArrayPos].jlMissionAsInt, g_sJobListMP[paramArrayPos].jlSubtype,g_sJobListMP[paramArrayPos].jlPropertyType, g_sJobListMP[paramArrayPos].jlRootContentIdHash)
		
		// ...gather details about affordability
		isAffordable		= TRUE
		
		IF (g_sJobListMP[paramArrayPos].jlState = MP_JOBLIST_JOINABLE_MISSION)
			isAffordable = Can_I_Afford_To_Accept_Invite(g_sJobListMP[paramArrayPos].jlMissionAsInt, g_sJobListMP[paramArrayPos].jlSubtype)
		ENDIF
		
		// ...gather details about the creator if this isn't a playlist
		creatorIsAPlayer	= FALSE
		creatorIsRockstar	= FALSE
		
		IF NOT (isAffordable)
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("CELL_259")				// ~a~ costs $~1~
				ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(typeText)
				ADD_TEXT_COMPONENT_INTEGER(Get_Cost_Of_Accepting_Invite(g_sJobListMP[paramArrayPos].jlMissionAsInt, g_sJobListMP[paramArrayPos].jlSubtype))
			END_TEXT_COMMAND_SCALEFORM_STRING()

            #IF IS_DEBUG_BUILD
                NET_PRINT("         NOT ENOUGH CASH. COSTS $")
                NET_PRINT_INT(Get_Cost_Of_Accepting_Invite(g_sJobListMP[paramArrayPos].jlMissionAsInt, g_sJobListMP[paramArrayPos].jlSubtype))
				IF (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0)
					NET_PRINT(" - PLAYER HAS WANTED LEVEL")
				ENDIF
				NET_NL()
            #ENDIF
        ELSE
            IF NOT (isPlaylist)
                theCreator  = g_sJobListMP[paramArrayPos].jlInstance
                SWITCH (theCreator)
                    // Rockstar
                    CASE FMMC_ROCKSTAR_CREATOR_ID
                    CASE FMMC_ROCKSTAR_HIDEOUT_CREATOR_ID
                    CASE FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID
                        creatorIsRockstar = TRUE
                        BREAK
                        
                    // Not a player and not Rockstar, but it is a known 'creator'
                    CASE FMMC_MINI_GAME_CREATOR_ID
                        BREAK
                        
                    // Should be a player
                    DEFAULT
                        // Ensure it is in the valid player range
                        IF (theCreator >= 0)
                        AND (theCreator < NUM_NETWORK_PLAYERS)
                            creatorIsAPlayer = TRUE
                        ELSE
                            // There must be a new creatorID addded that I'm not referring to, so flag this as an error, but let things continue.
                            NET_PRINT("...KGM MP [JobList][App]: Fill_FM_Joblist_Screen_Slot_Current_or_Joinable_Mission() - ERROR: The Creator ID is unknown. There must be a new CONST added in script with INT value: ")
                            NET_PRINT_INT(theCreator)
                            NET_NL()
                            SCRIPT_ASSERT("Fill_FM_Joblist_Screen_Slot_Current_or_Joinable_Mission() - The CreatorID is unknown. Allowing script to continue. See console log. Tell Keith.")
                        ENDIF
                        BREAK
                ENDSWITCH
            ENDIF
            
            IF (creatorIsRockstar)
                BEGIN_TEXT_COMMAND_SCALEFORM_STRING("TWOSTRINGS")
                    ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL("CELL_266")     // "RockStar"
                    ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(typeText)
                END_TEXT_COMMAND_SCALEFORM_STRING()

                #IF IS_DEBUG_BUILD
                    NET_PRINT("         ")
                    NET_PRINT(GET_STRING_FROM_TEXT_FILE("CELL_266"))
                    NET_PRINT(" ")
                    NET_PRINT(GET_STRING_FROM_TEXT_FILE(typeText))
                    NET_NL()
                #ENDIF
            ELSE
                IF (creatorIsAPlayer)
                    thisPlayer = INT_TO_PLAYERINDEX(theCreator)
                    IF (IS_NET_PLAYER_OK(thisPlayer, FALSE))
                        // Display Creator Name
                        playerName  = GET_PLAYER_NAME(thisPlayer)
						
                        BEGIN_TEXT_COMMAND_SCALEFORM_STRING("TWOSTRINGS")
                            ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(playerName)
                            ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(typeText)
                        
                            #IF IS_DEBUG_BUILD
                                NET_PRINT("         ")
                                NET_PRINT(playerName)
                                NET_PRINT(" ")
                                NET_PRINT(GET_STRING_FROM_TEXT_FILE(typeText))
                                NET_NL()
                            #ENDIF
                        END_TEXT_COMMAND_SCALEFORM_STRING()
                    ELSE
                        creatorIsAPlayer = FALSE
                    ENDIF
                ENDIF
                
                IF NOT (creatorIsAPlayer)
                    // Creator is not a player, or is no longer OK
                    IF (isPlaylist)
                        // ...this is a playlist
                        IF (isPlaylistLobby)
                            // ...not on a playlist mission, still in lobby, so only need to display one string to avoid displaying: PLAYLIST PLAYLIST LOBBY
                            playlistConfig = "STRING"
                        ELSE
                            // ...on a playlist mission
                            playlistConfig = "TWOSTRINGS"
                        ENDIF
                        
                        // KGM 29/11/12: Leaving the playlist lobby stuff for now, but I've actually extracted this out into a separate section above, so it should never hit here if in a lobby
                        BEGIN_TEXT_COMMAND_SCALEFORM_STRING(playlistConfig)
                            IF NOT (isPlaylistLobby)
                                ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL("CELL_261")     // PLAYLIST
                            ENDIF
                            ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(typeText)
                        
                            #IF IS_DEBUG_BUILD
                                NET_PRINT("         ")
                                NET_PRINT(GET_STRING_FROM_TEXT_FILE("CELL_261"))
                                NET_PRINT(" ")
                                NET_PRINT(GET_STRING_FROM_TEXT_FILE(typeText))
                                NET_NL()
                            #ENDIF
                        END_TEXT_COMMAND_SCALEFORM_STRING()
                    ELSE
                        // ...this is not a playlist
                        BEGIN_TEXT_COMMAND_SCALEFORM_STRING(typeText)
                        END_TEXT_COMMAND_SCALEFORM_STRING()
                    
                        #IF IS_DEBUG_BUILD
                            NET_PRINT("         ")
                            NET_PRINT(GET_STRING_FROM_TEXT_FILE(typeText))
                            NET_NL()
                        #ENDIF
                    ENDIF
                ENDIF
            ENDIF
        ENDIF
		
		// KGM 27/1/15 [BUG 2212656]: Three New Params: BlipID, isHeistMessage, isMultiplayer (Must be TRUE for current job)
		Send_BlipSprite_IsHeist_IsMP_Scaleform_Params(g_sJobListMP[paramArrayPos].jlMissionAsInt, g_sJobListMP[paramArrayPos].jlSubtype, g_sJobListMP[paramArrayPos].jlRootContentIdHash, g_sJobListMP[paramArrayPos].jlAdversaryModeType)
    END_SCALEFORM_MOVIE_METHOD()
            
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Fill the scaleform slot details for an invite to a mission
//
// INPUT PARAMS:            paramSlot           The scaleform slot being filled
//                          paramArrayPos       The Joblist Array Position the data is being taken from
PROC Fill_FM_Joblist_Screen_Slot_Invitation(INT paramSlot, INT paramArrayPos)

    // 1st Line displays:   <Inviting Player>
    // 2nd Line displays:   Invite: <Mission Type>

    PLAYER_INDEX        theInvitingPlayer   = INVALID_PLAYER_INDEX()
    enumCharacterList   theInvitingNPC      = NO_CHARACTER
    BOOL                invitorIsPlayer     = IS_BIT_SET(g_sJobListMP[paramArraypos].jlBitfield, JOBLIST_BITFLAG_INVITOR_IS_PLAYER)
    TEXT_LABEL_63       invitorName         = ""
    BOOL                invitorValid        = TRUE
    INT                 invitorRank         = -1
    TEXT_LABEL_15       typeText            = ""
    BOOL                inviteIsClosed      = IS_BIT_SET(g_sJobListMP[paramArrayPos].jlBitfield, JOBLIST_BITFLAG_INVITE_CLOSED)
    BOOL                isAffordable        = TRUE
	BOOL				canAffordWager		= TRUE
    BOOL                isPlaylist          = FALSE
	TEXT_LABEL_31		PlaylistTitle		= ""	
	BOOL				isChallange			= FALSE
    BOOL                useMissionName      = FALSE
    INT                 currentPlaylistJob  = 0
    INT                 totalPlaylistJobs   = 0
	BOOL				isHeadToHead		= FALSE
	INT					headToHeadWager		= g_sJobListMP[paramArrayPos].jlWagerH2H
		
    BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieIndex, "SET_DATA_SLOT")
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(JOBLIST_VIEW_STATE)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(paramSlot)
	
		// KGM 23/9/14 [BUG: 2085578] Adding more specific output as details are passed to scaleform to see if anything obvious crops up in a repro
		#IF IS_DEBUG_BUILD
			PRINTLN("...KGM MP [JobList][App]: FM JOBLIST SCALEFORM SLOT DATA (Invitation) [View State: ", JOBLIST_VIEW_STATE, "] [Slot ", paramSlot, "]")
		#ENDIF
		
		// 1st Line
		// NOTE: TEMP: The UniqueID variable is being used for the Inviting Player ID as an Int
		IF (invitorIsPlayer)
			theInvitingPlayer	= INT_TO_PLAYERINDEX(g_sJobListMP[paramArraypos].jlUniqueID)
			invitorValid		= IS_NET_PLAYER_OK(theInvitingPlayer, FALSE)
			
			IF (invitorValid)
				invitorRank			= GET_PLAYER_RANK(theInvitingPlayer)
				isPlaylist			= IS_PLAYER_ON_A_PLAYLIST(theInvitingPlayer)
				
				IF (isPlaylist)
//					currentPlaylistJob	= GlobalplayerBD_FM[NATIVE_TO_INT(theInvitingPlayer)].sPlaylistVars.iCurrentPlayListPosition + 1
					currentPlaylistJob	= GlobalplayerBD_FM_2[NATIVE_TO_INT(theInvitingPlayer)].iPlaylistStartPos
					totalPlaylistJobs	= GlobalplayerBD_FM[NATIVE_TO_INT(theInvitingPlayer)].sPlaylistVars.iLength
					PlaylistTitle		= GlobalplayerBD_FM[NATIVE_TO_INT(theInvitingPlayer)].tl31PlaylistName
					IF (headToHeadWager >= 0)
						isHeadToHead = TRUE
					ELSE
						if IS_PLAYER_ON_A_CHALLENGE(theInvitingPlayer)
						or IS_PLAYER_SETTING_CHALLENGE(theInvitingPlayer)
						OR IS_PLAYER_ON_A_H2H_PLAYLIST(theInvitingPlayer)	// Keeping this check here just in case the h2h wager is not set, so at least it displays 'challenge'
							isChallange = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			theInvitingNPC		= INT_TO_ENUM(enumCharacterList, g_sJobListMP[paramArraypos].jlUniqueID)
			invitorValid		= TRUE
		ENDIF
		
		IF (invitorValid)
			// Invitor is valid, so display Inviting Player Name
			TEXT_LABEL_23 invitorConfig = "STRING"
			IF (invitorIsPlayer)
        		invitorName = "<C>"
				invitorName	+= GET_PLAYER_NAME(theInvitingPlayer)
        		invitorName += "</C>"
				
				IF (invitorRank > 0)
					invitorConfig = "CELL_MP_279"
				ENDIF
			ELSE
				TEXT_LABEL	npcNameTL		= Get_NPC_Name(theInvitingNPC)
				STRING		npcNameAsString	= CONVERT_TEXT_LABEL_TO_STRING(npcNameTL)
				
				invitorName = GET_FILENAME_FOR_AUDIO_CONVERSATION(npcNameAsString)
			ENDIF
			
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING(invitorConfig)
				ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(invitorName)
				
				#IF IS_DEBUG_BUILD
					PRINTLN("...KGM MP [JobList][App]: ...invitorName: ", invitorName)
				#ENDIF
				
				IF (invitorIsPlayer)
				AND (invitorRank > 0)
					ADD_TEXT_COMPONENT_INTEGER(invitorRank)
					
					#IF IS_DEBUG_BUILD
						PRINTLN("...KGM MP [JobList][App]: ...InvitorRank: ", invitorRank)
					#ENDIF
				ENDIF
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ELSE
			// Invitor isn't valid, so just display 'Invitation' for now
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("CELL_265")		// Invitation
				#IF IS_DEBUG_BUILD
					PRINTLN("...KGM MP [JobList][App]: ...Invitor Not Valid. Pass CELL_265: ", GET_FILENAME_FOR_AUDIO_CONVERSATION("CELL_265"))
				#ENDIF
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ENDIF
		// 2nd Line
		// NOTE: TEMP: The MissionAsInt variable is being used for the Mission Type ID
		typeText	= Get_FM_Joblist_Activity_TextLabel(g_sJobListMP[paramArrayPos].jlMissionAsInt, g_sJobListMP[paramArrayPos].jlSubtype,g_sJobListMP[paramArrayPos].jlPropertyType, g_sJobListMP[paramArrayPos].jlRootContentIdHash)

		useMissionName = TRUE
		IF (IS_STRING_NULL_OR_EMPTY(g_sJobListMP[paramArrayPos].jlMissionName))
			useMissionname = FALSE
		ENDIF
		
		// ...gather details about affordability
		isAffordable = Can_I_Afford_To_Accept_Invite(g_sJobListMP[paramArrayPos].jlMissionAsInt, g_sJobListMP[paramArrayPos].jlSubtype)
		
		IF (isHeadToHead)
			canAffordWager = Can_I_Afford_Wager_If_Head_To_Head_Playlist(headToHeadWager)
		ENDIF
		
		// If the Invitor was valid, then display 'Invite: ' on the second line, otherwise miss it out
		// KGM 3/3/13: Always display: CLOSED if the invite is closed
		// KGM 16/4/13: Or display 'COSTS $nnn' if the player cannot afford it
		IF (inviteIsClosed)
		AND NOT SVM_FLOW_IS_MISSION_SVM_FLOW_FROM_ROOT_ID(g_sJobListMP[paramArrayPos].jlRootContentIdHash)
		AND NOT WVM_FLOW_IS_MISSION_WVM_FLOW_FROM_ROOT_ID(g_sJobListMP[paramArrayPos].jlRootContentIdHash)
			// Invite is closed
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("CELL_262")			// CLOSED: 
				ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(typeText)
					
				#IF IS_DEBUG_BUILD
					PRINTLN("...KGM MP [JobList][App]: ...Invite Is Closed. Pass CELL_262: ", GET_FILENAME_FOR_AUDIO_CONVERSATION("CELL_262"))
				#ENDIF
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ELIF NOT (canAffordWager)
			// Player can't afford wager to play this head to head playlist
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("CELL_259_BET")				// Wager $~1~
				ADD_TEXT_COMPONENT_INTEGER(headToHeadWager)
					
				#IF IS_DEBUG_BUILD
					PRINTLN("...KGM MP [JobList][App]: ...Can't afford wager. Pass CELL_259_BET: ", GET_FILENAME_FOR_AUDIO_CONVERSATION("CELL_259_BET"))
				#ENDIF
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ELIF NOT (isAffordable)
			// Player can't afford to play this activity
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("CELL_259")				// ~a~ costs $~n~
				ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(typeText)
				ADD_TEXT_COMPONENT_INTEGER(Get_Cost_Of_Accepting_Invite(g_sJobListMP[paramArrayPos].jlMissionAsInt, g_sJobListMP[paramArrayPos].jlSubtype))
					
				#IF IS_DEBUG_BUILD
					PRINTLN("...KGM MP [JobList][App]: ...Can't afford. typetext: ", typeText, ". Cost $", Get_Cost_Of_Accepting_Invite(g_sJobListMP[paramArrayPos].jlMissionAsInt, g_sJobListMP[paramArrayPos].jlSubtype))
				#ENDIF
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ELSE
			IF (invitorValid)
				// Invitor is valid
				//get amount of players required for this mission
				INT minPlayers = g_sJobListMP[paramArrayPos].jlMinPlayers
				INT maxPlayers = g_sJobListMP[paramArrayPos].jlMaxPlayers
				
				BOOL sameMinMaxPlayers = FALSE
				IF (minPlayers = maxPlayers)
					sameMinMaxPlayers = TRUE
				ENDIF
				
				TEXT_LABEL_23 typeTextConfig = "CELL_MP_304_PP"					// Invite to ~a~, ~1~m (~1~ to ~1~ Players)
				IF (isPlaylist)
					if isChallange
						typeTextConfig = "CELL_MP_305"							// Invites you to a ~1~ part Challenge - ~a~
					ELIF (isHeadToHead)
						typeTextConfig = "CELL_MP_H2H_M"						// Invites to ~1~ part Head To Head, Wager ~1~ - ~a~
					else
						typeTextConfig = "CELL_MP_303_TU"						// Invite to ~a~, ~1~m (Playlist ~1~ of ~1~)						
					endif
				ELSE
					IF (sameMinMaxPlayers)
						// same min and max players
						typeTextConfig = "CELL_MP_304_P"
					ELSE
						// different min and max players
						typeTextConfig = "CELL_MP_304_PP"
					ENDIF
				ENDIF
				
				Int iDistance = 0
				IF NOT (IS_PED_INJURED(PLAYER_PED_ID()))
			        vector playerCoords    	= GET_PLAYER_COORDS(PLAYER_ID())
					iDistance				= round(GET_DISTANCE_BETWEEN_COORDS(playerCoords,g_sJobListMP[paramArrayPos].jlCoords)) 
				endif
				
				//PlaylistTitle += Get_Text_Append_Badge( GET_HASH_KEY(g_sJobListMP[paramArraypos].jlContentID),
				//										g_sJobListMP[paramArraypos].jlMissionAsInt,
				//										g_sJobListMP[paramArraypos].jlSubtype)
				
				IF isChallange				
					// Invites you to a ~1~ part Challenge - ~a~
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING(typeTextConfig)
						ADD_TEXT_COMPONENT_INTEGER(totalPlaylistJobs)
						ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY(PlaylistTitle)
						
						#IF IS_DEBUG_BUILD
							PRINTLN("...KGM MP [JobList][App]: ...Challenge. totalPlaylistJobs: ", totalPlaylistJobs, ". PlaylistTitle: ", PlaylistTitle)
						#ENDIF
					END_TEXT_COMMAND_SCALEFORM_STRING()
				ELIF (isHeadToHead)
					// Invites to ~1~ part Head To Head, Wager ~1~ - ~a~
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING(typeTextConfig)
						ADD_TEXT_COMPONENT_INTEGER(totalPlaylistJobs)
						ADD_TEXT_COMPONENT_INTEGER(headToHeadWager)
						ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY(PlaylistTitle)
						
						#IF IS_DEBUG_BUILD
							PRINTLN("...KGM MP [JobList][App]: ...HeadToHead. totalPlaylistJobs: ", totalPlaylistJobs, ".headToHeadWager: ", headToHeadWager, ". PlaylistTitle: ", PlaylistTitle)
						#ENDIF
					END_TEXT_COMMAND_SCALEFORM_STRING()
				ELIF isPlaylist
					// Invite to ~a~ (~1~m)~n~(Playlist ~1~ of ~1~)	
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING(typeTextConfig)
						ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY(PlaylistTitle)
						ADD_TEXT_COMPONENT_INTEGER(iDistance)
						ADD_TEXT_COMPONENT_INTEGER(currentPlaylistJob)
						ADD_TEXT_COMPONENT_INTEGER(totalPlaylistJobs)
						
						#IF IS_DEBUG_BUILD
							PRINTLN("...KGM MP [JobList][App]: ...Playlist. PlaylistTitle: ", PlaylistTitle, ". iDistance: ", iDistance, ". currentPlaylistJob, ", currentPlaylistJob, ". totalPlaylistJobs: ", totalPlaylistJobs)
						#ENDIF
					END_TEXT_COMMAND_SCALEFORM_STRING()		
				ELSE
					// Invite to ~a~ (~1~m) (~1~ to ~1~ Players)
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING(typeTextConfig)						
						IF (useMissionName)// ...display the mission name
							ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY(g_sJobListMP[paramArrayPos].jlMissionName)
						
							#IF IS_DEBUG_BUILD
								PRINTLN("...KGM MP [JobList][App]: ...MissionName: ", g_sJobListMP[paramArrayPos].jlMissionName)
							#ENDIF
						ELSE		
							ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(typeText)// ...display the activity type
						
							#IF IS_DEBUG_BUILD
								PRINTLN("...KGM MP [JobList][App]: ...typeText: ", typeText)
							#ENDIF
						ENDIF	
						ADD_TEXT_COMPONENT_INTEGER(iDistance)
						
						#IF IS_DEBUG_BUILD
							PRINTLN("...KGM MP [JobList][App]: ...iDistance: ", iDistance)
						#ENDIF
						
						ADD_TEXT_COMPONENT_INTEGER(minPlayers)
						
						#IF IS_DEBUG_BUILD
							PRINTLN("...KGM MP [JobList][App]: ...minPlayers: ", minPlayers)
						#ENDIF
						
						IF NOT (sameMinMaxPlayers)
							ADD_TEXT_COMPONENT_INTEGER(maxPlayers)						
						
							#IF IS_DEBUG_BUILD
								PRINTLN("...KGM MP [JobList][App]: ...maxPlayers: ", maxPlayers)
							#ENDIF
						ENDIF
						
						ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY(Get_Text_Append_Badge( GET_HASH_KEY(g_sJobListMP[paramArraypos].jlContentID),
																							 g_sJobListMP[paramArraypos].jlMissionAsInt,
																							 g_sJobListMP[paramArraypos].jlSubtype,
																							 g_sJobListMP[paramArraypos].jlIsPushBikeOnly, 
																							 g_sJobListMP[paramArraypos].jlRootContentIdHash,
																							 g_sJobListMP[paramArraypos].jlAdversaryModeType))
						
						#IF IS_DEBUG_BUILD
							PRINTLN("...KGM MP [JobList][App]: ...[potential badge text (see MMM above)]")
						#ENDIF
					END_TEXT_COMMAND_UNPARSED_SCALEFORM_STRING()
				ENDIF
			ELSE
				// Invitor is not valid
				BEGIN_TEXT_COMMAND_SCALEFORM_STRING(typeText)
					#IF IS_DEBUG_BUILD
						PRINTLN("...KGM MP [JobList][App]: ...Invitor Not Valid. Pass: typeText: ", typeText)
					#ENDIF
				END_TEXT_COMMAND_SCALEFORM_STRING()
			ENDIF
		ENDIF
		
		// KGM 27/1/15 [BUG 2212656]: Three New Params: BlipID, isHeistMessage, isMultiplayer (Must be TRUE for current job)
		Send_BlipSprite_IsHeist_IsMP_Scaleform_Params(g_sJobListMP[paramArrayPos].jlMissionAsInt, g_sJobListMP[paramArrayPos].jlSubtype, g_sJobListMP[paramArrayPos].jlRootContentIdHash, g_sJobListMP[paramArrayPos].jlAdversaryModeType)

		// Debug Summary
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [JobList][App]: ......[SUMMARY] Invited to: ")
			NET_PRINT(GET_STRING_FROM_TEXT_FILE(typeText))
			IF (isPlaylist)
				NET_PRINT(" [Playlist Job ")
				NET_PRINT_INT(currentPlaylistJob)
				NET_PRINT(" of ")
				NET_PRINT_INT(totalPlaylistJobs)
				NET_PRINT("]")
			ENDIF
			IF (inviteIsClosed)
				NET_PRINT(" [INVITE CLOSED]")
			ENDIF
			IF (isHeadToHead)
			AND NOT (canAffordWager)
				NET_PRINT(" [CANNOT AFFORD WAGER. GOT $")
				NET_PRINT_INT(NETWORK_GET_VC_WALLET_BALANCE() + NETWORK_GET_VC_BANK_BALANCE())
				NET_PRINT("  NEED $")
				NET_PRINT_INT(headToHeadWager)
				NET_PRINT(")]")
			ENDIF
			IF NOT (isAffordable)
				NET_PRINT(" [CANNOT AFFORD. GOT $")
				NET_PRINT_INT(NETWORK_GET_VC_WALLET_BALANCE() + NETWORK_GET_VC_BANK_BALANCE())
				NET_PRINT("  NEED $")
				NET_PRINT_INT(Get_Cost_Of_Accepting_Invite(g_sJobListMP[paramArrayPos].jlMissionAsInt, g_sJobListMP[paramArrayPos].jlSubtype))
				NET_PRINT(")]")
				IF (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0)
					NET_PRINT(" - PLAYER HAS WANTED LEVEL")
				ENDIF
			ENDIF
			NET_NL()
		#ENDIF
    END_SCALEFORM_MOVIE_METHOD()
            
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Fill the scaleform slot details for a Cross-Session Invite to a job
//
// INPUT PARAMS:            paramSlot           The scaleform slot being filled
//                          paramArrayPos       The Joblist Array Position the data is being taken from
PROC Fill_FM_Joblist_Screen_Slot_CSInvite(INT paramSlot, INT paramArrayPos)

    // NOTE: TEMP: The MissionName field is being used for the Inviting PlayerName as an Int
    TEXT_LABEL_63       invitorName         = ""
    TEXT_LABEL_15       typeText            = ""
    BOOL                inviteIsClosed      = IS_BIT_SET(g_sJobListMP[paramArrayPos].jlBitfield, JOBLIST_BITFLAG_INVITE_CLOSED)
    BOOL                isAffordable        = TRUE
	BOOL				canAffordWager		= TRUE
	BOOL				isHeadToHead		= FALSE
	INT					headToHeadWager		= g_sJobListMP[paramArrayPos].jlWagerH2H
	
	IF (headToHeadWager >= 0)
		isHeadToHead = TRUE
	ENDIF

    BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieIndex, "SET_DATA_SLOT")
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(JOBLIST_VIEW_STATE)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(paramSlot)
	
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [JobList][App]: FM JOBLIST SCALEFORM SLOT DATA (Cross-Session Invite)")
			NET_PRINT(" [View State: ")
			NET_PRINT_INT(JOBLIST_VIEW_STATE)
			NET_PRINT("] [Slot ")
			NET_PRINT_INT(paramSlot)
			NET_PRINT("]")
			NET_NL()
		#ENDIF
		
		// 1st Line
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			invitorName = "<C>"
			invitorName += g_sJobListMP[paramArrayPos].jlInvitorName
			invitorName += "</C>"
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(invitorName)
		END_TEXT_COMMAND_SCALEFORM_STRING()
			
		#IF IS_DEBUG_BUILD
			NET_PRINT("         Invitation from: ")
			NET_PRINT(invitorName)
			NET_NL()
		#ENDIF
		
		// 2nd Line
		// NOTE: TEMP: The MissionAsInt variable is being used for the Mission Type ID
		typeText	= Get_FM_Joblist_Activity_TextLabel(g_sJobListMP[paramArrayPos].jlMissionAsInt, g_sJobListMP[paramArrayPos].jlSubtype,g_sJobListMP[paramArrayPos].jlPropertyType, g_sJobListMP[paramArrayPos].jlRootContentIdHash)
		
		// ...gather details about affordability
		isAffordable = Can_I_Afford_To_Accept_Invite(g_sJobListMP[paramArrayPos].jlMissionAsInt, g_sJobListMP[paramArrayPos].jlSubtype)
		
		IF (isHeadToHead)
			canAffordWager = Can_I_Afford_Wager_If_Head_To_Head_Playlist(headToHeadWager)
		ENDIF
		
		// Display 'Invite: ' on the second line
		// KGM 3/3/13: Always display: CLOSED if the invite is closed
		// KGM 16/4/13: Or display 'COSTS $nnn' if the player cannot afford it
		IF (inviteIsClosed)
		AND NOT SVM_FLOW_IS_MISSION_SVM_FLOW_FROM_ROOT_ID(g_sJobListMP[paramArrayPos].jlRootContentIdHash)
		AND NOT WVM_FLOW_IS_MISSION_WVM_FLOW_FROM_ROOT_ID(g_sJobListMP[paramArrayPos].jlRootContentIdHash)
			// Invite is closed
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("CELL_262")			// CLOSED: 
				ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(typeText)
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ELIF NOT (canAffordWager)
			// Player can't afford the wager for this head to head
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("CELL_259_BET")				// Wager $~n~
				ADD_TEXT_COMPONENT_INTEGER(headToHeadWager)
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ELIF NOT (isAffordable)
			// Player can't afford to play this activity
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("CELL_259")				// ~a~ costs $~n~
				ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(typeText)
				ADD_TEXT_COMPONENT_INTEGER(Get_Cost_Of_Accepting_Invite(g_sJobListMP[paramArrayPos].jlMissionAsInt, g_sJobListMP[paramArrayPos].jlSubtype))
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ELSE
			IF (isHeadToHead)
				TEXT_LABEL_23 typeTextConfig = "CELL_MP_H2H"				// Invites to Head To Head, Wager ~1~
				
				BEGIN_TEXT_COMMAND_SCALEFORM_STRING(typeTextConfig)
					ADD_TEXT_COMPONENT_INTEGER(headToHeadWager)
				END_TEXT_COMMAND_SCALEFORM_STRING()
			ELSE
				TEXT_LABEL_23 typeTextConfig = "CELL_268"					// Invite to <type>
				
				// KGM 22/1/15: 99.99% sure this PHONE_INVITE check isn't needed for a cross-session invite
				IF g_sJobListMP[paramArrayPos].jlMissionAsInt = FMMC_TYPE_HEIST_PHONE_INVITE
				OR g_sJobListMP[paramArrayPos].jlMissionAsInt = FMMC_TYPE_HEIST_PHONE_INVITE_2
					typeTextConfig = "CELL_268_HEIST"
				ENDIF

				BEGIN_TEXT_COMMAND_SCALEFORM_STRING(typeTextConfig)
					TEXT_LABEL_63 t
				
					#IF FEATURE_COPS_N_CROOKS
					IF IS_THIS_CLOUD_FILE_NAME_ARCADE_MODE(g_sJobListMP[paramArraypos].jlContentID)
						t = GET_FILENAME_FOR_AUDIO_CONVERSATION(g_sJobListMP[paramArraypos].jlContentID)
					ELSE
					#ENDIF
				
						t = GET_FILENAME_FOR_AUDIO_CONVERSATION(typeText)  
						t += Get_Text_Append_Badge( GET_HASH_KEY(g_sJobListMP[paramArraypos].jlContentID),
													g_sJobListMP[paramArraypos].jlMissionAsInt,
													g_sJobListMP[paramArraypos].jlSubtype,
													g_sJobListMP[paramArraypos].jlIsPushBikeOnly, 
													g_sJobListMP[paramArraypos].jlRootContentIdHash, 
													g_sJobListMP[paramArraypos].jlAdversaryModeType)
					
					#IF FEATURE_COPS_N_CROOKS
					ENDIF
					#ENDIF
					
					ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY(t)
					
					//ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(t)
				END_TEXT_COMMAND_UNPARSED_SCALEFORM_STRING()
			ENDIF
		ENDIF
		
		// KGM 27/1/15 [BUG 2212656]: Three New Params: BlipID, isHeistMessage, isMultiplayer (Must be TRUE for current job)
		Send_BlipSprite_IsHeist_IsMP_Scaleform_Params(g_sJobListMP[paramArrayPos].jlMissionAsInt, g_sJobListMP[paramArrayPos].jlSubtype, g_sJobListMP[paramArrayPos].jlRootContentIdHash, g_sJobListMP[paramArrayPos].jlAdversaryModeType)

		// Debug Summary
		#IF IS_DEBUG_BUILD
			NET_PRINT("         Invited to: ")
			NET_PRINT(GET_STRING_FROM_TEXT_FILE(typeText))
			IF (inviteIsClosed)
				NET_PRINT(" [INVITE CLOSED]")
			ENDIF
			IF NOT (canAffordWager)
				NET_PRINT(" [CANNOT AFFORD WAGER. GOT $")
				NET_PRINT_INT(NETWORK_GET_VC_WALLET_BALANCE() + NETWORK_GET_VC_BANK_BALANCE())
				NET_PRINT("  NEED $")
				NET_PRINT_INT(headToHeadWager)
				NET_PRINT(")]")
			ENDIF
			IF NOT (isAffordable)
				NET_PRINT(" [CANNOT AFFORD. GOT $")
				NET_PRINT_INT(NETWORK_GET_VC_WALLET_BALANCE() + NETWORK_GET_VC_BANK_BALANCE())
				NET_PRINT("  NEED $")
				NET_PRINT_INT(Get_Cost_Of_Accepting_Invite(g_sJobListMP[paramArrayPos].jlMissionAsInt, g_sJobListMP[paramArrayPos].jlSubtype))
				NET_PRINT(")]")
				IF (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0)
					NET_PRINT(" - PLAYER HAS WANTED LEVEL")
				ENDIF
			ENDIF
			NET_NL()
		#ENDIF
    END_SCALEFORM_MOVIE_METHOD()
            
ENDPROC

// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Fill the scaleform slot details for a Basic Invite to a job
//
// INPUT PARAMS:            paramSlot           The scaleform slot being filled
//                          paramArrayPos       The Joblist Array Position the data is being taken from
PROC Fill_FM_Joblist_Screen_Slot_RVInvite(INT paramSlot, INT paramArrayPos)

    PLAYER_INDEX        theInvitingPlayer   = INVALID_PLAYER_INDEX()
    enumCharacterList   theInvitingNPC      = NO_CHARACTER
    BOOL                invitorIsPlayer     = IS_BIT_SET(g_sJobListMP[paramArraypos].jlBitfield, JOBLIST_BITFLAG_INVITOR_IS_PLAYER)
    TEXT_LABEL_63       invitorName         = ""
    BOOL                invitorValid        = TRUE
    INT                 invitorRank         = -1
    TEXT_LABEL_15       typeText            = ""
    BOOL                inviteIsClosed      = IS_BIT_SET(g_sJobListMP[paramArrayPos].jlBitfield, JOBLIST_BITFLAG_INVITE_CLOSED)
    BOOL                useMissionName      = FALSE

    BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieIndex, "SET_DATA_SLOT")
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(JOBLIST_VIEW_STATE)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(paramSlot)
	
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [JobList][App]: FM JOBLIST SCALEFORM SLOT DATA (Basic Invite)")
			NET_PRINT(" [View State: ")
			NET_PRINT_INT(JOBLIST_VIEW_STATE)
			NET_PRINT("] [Slot ")
			NET_PRINT_INT(paramSlot)
			NET_PRINT("]")
			NET_NL()
		#ENDIF
		
		// 1st Line
		// NOTE: TEMP: The UniqueID variable is being used for the Inviting Player ID as an Int
		IF (invitorIsPlayer)
			theInvitingPlayer	= INT_TO_PLAYERINDEX(g_sJobListMP[paramArraypos].jlUniqueID)
			invitorValid		= IS_NET_PLAYER_OK(theInvitingPlayer, FALSE)
			
			IF (invitorValid)
				invitorRank			= GET_PLAYER_RANK(theInvitingPlayer)
			ENDIF
		ELSE
			theInvitingNPC		= INT_TO_ENUM(enumCharacterList, g_sJobListMP[paramArraypos].jlUniqueID)
			invitorValid		= TRUE
		ENDIF
		
		IF (invitorValid)
			// Invitor is valid, so display Inviting Player Name
			TEXT_LABEL_23 invitorConfig = "STRING"
			IF (invitorIsPlayer)
        		invitorName = "<C>"
				invitorName	+= GET_PLAYER_NAME(theInvitingPlayer)
        		invitorName += "</C>"
				
				IF (invitorRank > 0)
					invitorConfig = "CELL_MP_279"
				ENDIF
			ELSE
				TEXT_LABEL	npcNameTL		= Get_NPC_Name(theInvitingNPC)
				STRING		npcNameAsString	= CONVERT_TEXT_LABEL_TO_STRING(npcNameTL)
				
				invitorName = GET_FILENAME_FOR_AUDIO_CONVERSATION(npcNameAsString)
			ENDIF
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING(invitorConfig)
				ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(invitorName)
				
				IF (invitorIsPlayer)
				AND (invitorRank > 0)
					ADD_TEXT_COMPONENT_INTEGER(invitorRank)
				ENDIF
			END_TEXT_COMMAND_SCALEFORM_STRING()
			
			#IF IS_DEBUG_BUILD
				NET_PRINT("         Invitation from: ")
				NET_PRINT(invitorName)
				NET_NL()
			#ENDIF
		ELSE
			// Invitor isn't valid, so just display 'Invitation' for now
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("CELL_265")		// Invitation
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ENDIF
		
		// 2nd Line
		// NOTE: TEMP: The MissionAsInt variable is being used for the Mission Type ID
		typeText = Get_FM_Joblist_Activity_TextLabel(g_sJobListMP[paramArrayPos].jlMissionAsInt, g_sJobListMP[paramArrayPos].jlSubtype,g_sJobListMP[paramArrayPos].jlPropertyType, g_sJobListMP[paramArrayPos].jlRootContentIdHash)
		
		useMissionName = TRUE
		IF (IS_STRING_NULL_OR_EMPTY(g_sJobListMP[paramArrayPos].jlMissionName))
			useMissionname = FALSE
		ENDIF
		
		// Display 'Invite: ' on the second line
		// KGM 3/3/13: Always display: CLOSED if the invite is closed
		IF (inviteIsClosed)
		AND NOT SVM_FLOW_IS_MISSION_SVM_FLOW_FROM_ROOT_ID(g_sJobListMP[paramArrayPos].jlRootContentIdHash)
		AND NOT WVM_FLOW_IS_MISSION_WVM_FLOW_FROM_ROOT_ID(g_sJobListMP[paramArrayPos].jlRootContentIdHash)
			// Invite is closed
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("CELL_262")			// CLOSED: 
				ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(typeText)
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ELSE
			BOOL addAdditionalText = TRUE
			String textLayout = "CELL_268"// Invite to <type>

			IF g_sJobListMP[paramArrayPos].jlPropertyType = ENUM_TO_INT(SIMPLE_INTERIOR_TYPE_DEFUNCT_BASE)
				textLayout = "CELL_268_FACILITY"
			ENDIF

			IF g_sJobListMP[paramArrayPos].jlPropertyType = ENUM_TO_INT(SIMPLE_INTERIOR_TYPE_ARMORY_AIRCRAFT)
				textLayout = "CELL_268_AVENGER"
			ENDIF			
			
			IF g_sJobListMP[paramArrayPos].jlMissionAsInt = FMMC_TYPE_HEIST_PHONE_INVITE
			OR g_sJobListMP[paramArrayPos].jlMissionAsInt = FMMC_TYPE_HEIST_PHONE_INVITE_2
			OR g_sJobListMP[paramArrayPos].jlMissionAsInt = FMMC_TYPE_VCM_QUICK_INVITE
				IF GANGOPS_FLOW_IS_A_GANGOPS_MISSION_FROM_ROOT_ID(g_sJobListMP[paramArrayPos].jlRootContentIdHash)
					textLayout = "CELL_268_GO_DETAIL"	// Continue Heist: setup
				ELIF VCM_FLOW_IS_THIS_MISSION_VCM_FLOW(g_sJobListMP[paramArrayPos].jlRootContentIdHash)
					textLayout = "CELL_268_RETRY"	// Retry : setup
				ELIF (g_sJobListMP[paramArrayPos].jlSubtype = FMMC_MISSION_TYPE_PLANNING)
					textLayout = "CELL_268_PREP_DETAIL"
					addAdditionalText = FALSE
					useMissionName = FALSE
				ELSE
					textLayout = "CELL_268_HEIST_DETAIL"
				ENDIF
			ELIF g_sJobListMP[paramArrayPos].jlMissionAsInt = FMMC_TYPE_IH_QUICK_INVITE
				textLayout = "IHQUICK_BODY" // Continue: Prep work
			ENDIF
						
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING(textLayout)			
			IF (useMissionName)
				// ...display the mission name
				ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY(g_sJobListMP[paramArrayPos].jlMissionName)
			ELSE
				// ...display the activity type
				IF (addAdditionalText)
					ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(typeText)
				ENDIF
			ENDIF
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ENDIF
		
		IF g_sJobListMP[paramArrayPos].jlMissionAsInt = FMMC_TYPE_SIMPLE_INTERIOR
		AND GB_IS_LOCAL_PLAYER_MEMBER_OF_A_BIKER_GANG()
		
			// Blip sprite (-1 if not needed)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(-1)
				
			INT iStyle = GET_LOCAL_PLAYER_STYLE(paramSlot)
			PRINTLN("[SIMPLE_INTERIOR][INVITE] - Fill_FM_Joblist_Screen_Slot_RVInvite - iStyle: ", iStyle)
			// Colour of invite box
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStyle)
			
			// TRUE as this is multiplayer
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
			
		ELSE
		
			// KGM 27/1/15 [BUG 2212656]: Three New Params: BlipID, isHeistMessage, isMultiplayer (Must be TRUE for current job)
			Send_BlipSprite_IsHeist_IsMP_Scaleform_Params(g_sJobListMP[paramArrayPos].jlMissionAsInt, g_sJobListMP[paramArrayPos].jlSubtype, g_sJobListMP[paramArrayPos].jlRootContentIdHash, g_sJobListMP[paramArrayPos].jlAdversaryModeType)

		ENDIF


		// Debug Summary
		#IF IS_DEBUG_BUILD
			NET_PRINT("         Invited to: ")
			IF (useMissionName)
				NET_PRINT(g_sJobListMP[paramArrayPos].jlMissionName)
			ELSE
				NET_PRINT(GET_STRING_FROM_TEXT_FILE(typeText))
			ENDIF
			IF (inviteIsClosed)
				NET_PRINT(" [INVITE CLOSED]")
			ENDIF
			NET_NL()
		#ENDIF
    END_SCALEFORM_MOVIE_METHOD()
            
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Fill the scaleform slot details for a fake invite sent by the tutorial
PROC Fill_FM_Joblist_Screen_Slot_Fake_Invitation()

    // If it has already been accepted, then display an empty slot
    IF (g_fakeJoblistInvite.fjiAccepted)
        // There are no entries - this is copied from teh bottom of the Fill_FM_Joblist_Screen_Slots() function below
        Fill_FM_Joblist_Screen_No_Jobs()
            
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList][App]: FM JOBLIST SCALEFORM SLOT DATA: Displaying no data - fake joblist invite has already been accepted but the invite is still active") NET_NL()
        #ENDIF
        
        EXIT
    ENDIF
    
    // Fill the slot details (this is a modified copy of the Fill_FM_Joblist_Screen_Slot_Invitation() function above)
    // Display slot will always be 0 - always first line

    // 1st Line displays:   <Inviting In-Game character>
    // 2nd Line displays:   Invite: <Mission Type>

    TEXT_LABEL_15   typeText            = ""
    INT             displaySlot         = 0
    TEXT_LABEL_63   senderChar          = GLOBAL_CHARACTER_SHEET_GET_LABEL(g_fakeJoblistInvite.fjiSender)

    BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieIndex, "SET_DATA_SLOT")
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(JOBLIST_VIEW_STATE)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(displaySlot)
	
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [JobList][App]: FM JOBLIST SCALEFORM SLOT DATA (Fake Invitation)")
			NET_PRINT(" [View State: ")
			NET_PRINT_INT(JOBLIST_VIEW_STATE)
			NET_PRINT("] [Slot ")
			NET_PRINT_INT(displaySlot)
			NET_PRINT("]")
			NET_NL()
		#ENDIF
		
		// 1st Line: display Inviting In-Game Character
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING(senderChar)
		END_TEXT_COMMAND_SCALEFORM_STRING()
			
		#IF IS_DEBUG_BUILD
			NET_PRINT("         Invitation from: ")
			NET_PRINT(GET_STRING_FROM_TEXT_FILE(senderChar))
			NET_NL()
		#ENDIF
		
		// 2nd Line
		String 			textLayout 			= "CELL_268" // Display 'Invite to ' on the second line

		INT paramArrayPos
		
		REPEAT MAX_MP_JOBLIST_ENTRIES paramArrayPos
			IF g_sJobListMP[paramArrayPos].jlPropertyType = ENUM_TO_INT(SIMPLE_INTERIOR_TYPE_DEFUNCT_BASE)
				textLayout = "CELL_268_FACILITY"
			ENDIF

			IF g_sJobListMP[paramArrayPos].jlPropertyType = ENUM_TO_INT(SIMPLE_INTERIOR_TYPE_ARMORY_AIRCRAFT)
				textLayout = "CELL_268_AVENGER"
			ENDIF	
		ENDREPEAT
				
		typeText	= Get_FM_Joblist_Activity_TextLabel(g_fakeJoblistInvite.fjiType)						
				
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING(textLayout)
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(typeText)
		END_TEXT_COMMAND_SCALEFORM_STRING()
			
		#IF IS_DEBUG_BUILD
			NET_PRINT("         Invited to: ")
			NET_PRINT(GET_STRING_FROM_TEXT_FILE(typeText))
			NET_NL()
		#ENDIF
    END_SCALEFORM_MOVIE_METHOD()
            
ENDPROC


/// PURPOSE:
///    Returns a player name with the condensed gamertag font used
FUNC TEXT_LABEL_63 Get_Name_String(PLAYER_INDEX playerID)

	TEXT_LABEL_63 tlPlayerName = "<C>"
	tlPlayerName += GET_PLAYER_NAME(playerID)
	tlPlayerName += "</C>"
	
	RETURN tlPlayerName
ENDFUNC

FUNC TEXT_LABEL_31 GET_BIKER_COOP_INVITE_BODY_TEXT_LABEL()
	TEXT_LABEL_31 tl31Body
	tl31Body = "BA_APP_BODY_RC2"	// ~HUD_COLOUR_WHITE~The MC President of ~a~~HUD_COLOUR_WHITE~ has challenged you to ~a~.~s
	
	RETURN tl31Body
ENDFUNC

FUNC TEXT_LABEL_31 GET_BIKER_COOP_INVITE_BODY_TEXT_SUBTITLE()	
	TEXT_LABEL_31 tl31Body
	tl31Body = "BA_APP_BODY_RC1"	// ~HUD_COLOUR_WHITE~The MC President of ~a~~HUD_COLOUR_WHITE~ has challenged you to ~a~.~s
	
	RETURN tl31Body
ENDFUNC

FUNC BOOL Populate_Gang_Standard_Invite(INT iInviteSlot, INT cellphonePos)
	
	// Check the player is valid before we proceed
	IF g_sBossAgencyApp.bossAgencyInvites[iInviteSlot].playerInvitor = INVALID_PLAYER_INDEX()
	OR NOT IS_NET_PLAYER_OK(g_sBossAgencyApp.bossAgencyInvites[iInviteSlot].playerInvitor, FALSE)
		
		PRINTLN("...KGM MP [JobList][App][Biker]: Populate_Gang_Standard_Invite() - Player is invalid. Do not add to app. Slot [", iInviteSlot, "]")
		RETURN FALSE
	ENDIF
	
	// Populate the scaleform
	BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieIndex, "SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(JOBLIST_VIEW_STATE)			// View state
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(cellphonePos)						// Slot index (0, 1, 2, etc)
		
		// Top line
		TEXT_LABEL_63 tlPlayerName = Get_Name_String(g_sBossAgencyApp.bossAgencyInvites[iInviteSlot].playerInvitor)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(tlPlayerName)		// Display the player name at the top
		
		// Second line
//		INT iTotalGoons = GB_GET_NUM_GOONS_IN_PLAYER_GANG(g_sBossAgencyApp.bossAgencyInvites[iInviteSlot].playerInvitor)
		//IF iTotalGoons = 0
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MC_APP_BODY_T")
//		ELSE
//			IF iTotalGoons = 1
//				BEGIN_TEXT_COMMAND_SCALEFORM_STRING("MC_APP_BODY_T3")
//					ADD_TEXT_COMPONENT_INTEGER(iTotalGoons)
//				END_TEXT_COMMAND_SCALEFORM_STRING()
//			ELSE
//				BEGIN_TEXT_COMMAND_SCALEFORM_STRING("MC_APP_BODY_T2")				
//					ADD_TEXT_COMPONENT_INTEGER(iTotalGoons)
//				END_TEXT_COMMAND_SCALEFORM_STRING()
//			ENDIF
//		ENDIF
				
		// Blip sprite (-1 if not needed)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(-1)
		
		INT iStyle = GET_LOCAL_PLAYER_STYLE(iInviteSlot)
				
		PRINTLN("...KGM MP [JobList][App][Biker]: Populate_Gang_Standard_Invite() - iStyle: ", iStyle)

		// Colour of invite box
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStyle)
				
		// TRUE as this is multiplayer
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
		
	END_SCALEFORM_MOVIE_METHOD()
		
	PRINTLN("...KGM MP [JobList][App][Biker]: Populate_Gang_Standard_Invite() - Set up invite for slot [", iInviteSlot, "]. Invitor: ", GET_PLAYER_NAME(g_sBossAgencyApp.bossAgencyInvites[iInviteSlot].playerInvitor))
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Displays a Boss V Boss Deathmatch style invite
FUNC BOOL Populate_Gang_BossVBoss_Invite(INT iInviteSlot, INT cellphonePos)
	
		// Check the player is valid before we proceed
	IF g_sBossAgencyApp.bossAgencyInvites[iInviteSlot].playerInvitor = INVALID_PLAYER_INDEX()
	OR NOT IS_NET_PLAYER_OK(g_sBossAgencyApp.bossAgencyInvites[iInviteSlot].playerInvitor, FALSE)
		
		PRINTLN("...KGM MP [JobList][App][Biker]: Populate_Gang_BossVBoss_Invite() - Player is invalid. Do not add to app. Slot [", iInviteSlot, "]")
		RETURN FALSE
	ENDIF
	
	// Populate the scaleform
	BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieIndex, "SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(JOBLIST_VIEW_STATE)			// View state
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(cellphonePos)						// Slot index (0, 1, 2, etc)
		
		// Top line
		TEXT_LABEL_63 tlPlayerName = Get_Name_String(g_sBossAgencyApp.bossAgencyInvites[iInviteSlot].playerInvitor)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(tlPlayerName)		// Display the player name at the top
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("BA_APP_BODY_BKVBK1")
	
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		// Blip sprite (-1 if not needed)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(-1)
				
		INT iStyle = GET_LOCAL_PLAYER_STYLE(iInviteSlot)

		PRINTLN("...KGM MP [JobList][App][Biker]: Populate_Gang_BossVBoss_Invite() - iStyle: ", iStyle)

		// Colour of invite box
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStyle)
		
		// TRUE as this is multiplayer
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
		
	END_SCALEFORM_MOVIE_METHOD()
		
	PRINTLN("...KGM MP [JobList][App][Biker]: Populate_Gang_BossVBoss_Invite() - Set up invite for slot [", iInviteSlot, "]. Invitor: ", GET_PLAYER_NAME(g_sBossAgencyApp.bossAgencyInvites[iInviteSlot].playerInvitor))
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Displays a Biker V Biker Joust style invite
FUNC BOOL Populate_BikerVBiker_Joust_Invite(INT iInviteSlot, INT cellphonePos)
	
		// Check the player is valid before we proceed
	IF g_sBossAgencyApp.bossAgencyInvites[iInviteSlot].playerInvitor = INVALID_PLAYER_INDEX()
	OR NOT IS_NET_PLAYER_OK(g_sBossAgencyApp.bossAgencyInvites[iInviteSlot].playerInvitor, FALSE)
		
		PRINTLN("...KGM MP [JobList][App][Biker]: Populate_BikerVBiker_Joust_Invite() - Player is invalid. Do not add to app. Slot [", iInviteSlot, "]")
		RETURN FALSE
	ENDIF
	
	// Populate the scaleform
	BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieIndex, "SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(JOBLIST_VIEW_STATE)			// View state
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(cellphonePos)						// Slot index (0, 1, 2, etc)
		
		// Top line
		TEXT_LABEL_63 tlPlayerName = Get_Name_String(g_sBossAgencyApp.bossAgencyInvites[iInviteSlot].playerInvitor)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(tlPlayerName)		// Display the player name at the top
		
		// Second line
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("BA_APP_BODY_JS1")			// Has challenged you to a Boss Vs. Boss Deathmatch.
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		// Blip sprite (-1 if not needed)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(-1)
		
		INT iStyle = GET_LOCAL_PLAYER_STYLE(iInviteSlot)

		PRINTLN("...KGM MP [JobList][App][Biker]: Populate_BikerVBiker_Joust_Invite() - iStyle: ", iStyle)

		// Colour of invite box
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStyle)
		
		// TRUE as this is multiplayer
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
		
	END_SCALEFORM_MOVIE_METHOD()
		
	PRINTLN("...KGM MP [JobList][App][Biker]: Populate_BikerVBiker_Joust_Invite() - Set up invite for slot [", iInviteSlot, "]. Invitor: ", GET_PLAYER_NAME(g_sBossAgencyApp.bossAgencyInvites[iInviteSlot].playerInvitor))
	
	RETURN TRUE

ENDFUNC

/// PURPOSE:
///    Displays a Biker Coop invite
FUNC BOOL Populate_Biker_Coop_Invite(INT iInviteSlot, INT cellphonePos)
	
		// Check the player is valid before we proceed
	IF g_sBossAgencyApp.bossAgencyInvites[iInviteSlot].playerInvitor = INVALID_PLAYER_INDEX()
	OR NOT IS_NET_PLAYER_OK(g_sBossAgencyApp.bossAgencyInvites[iInviteSlot].playerInvitor, FALSE)
		
		PRINTLN("...KGM MP [JobList][App][Biker]: Populate_Biker_Coop_Invite() - Player is invalid. Do not add to app. Slot [", iInviteSlot, "]")
		RETURN FALSE
	ENDIF
	
	// Populate the scaleform
	BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieIndex, "SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(JOBLIST_VIEW_STATE)			// View state
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(cellphonePos)						// Slot index (0, 1, 2, etc)
		
		// Top line
		TEXT_LABEL_63 tlPlayerName = Get_Name_String(g_sBossAgencyApp.bossAgencyInvites[iInviteSlot].playerInvitor)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(tlPlayerName)		// Display the player name at the top
		
		TEXT_LABEL_31 tl31Subtitle = GET_BIKER_COOP_INVITE_BODY_TEXT_SUBTITLE()
		TEXT_LABEL_15 subText = GET_BIKER_INVITE_FEED_TITLE()
		// Second line
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING(tl31Subtitle)			// Has challenged you to a Boss Vs. Boss Deathmatch.
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(subText)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		// Blip sprite (-1 if not needed)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(-1)
		
		INT iStyle = GET_LOCAL_PLAYER_STYLE(iInviteSlot)

		PRINTLN("...KGM MP [JobList][App][Biker]: Populate_Biker_Coop_Invite() - iStyle: ", iStyle)

		// Colour of invite box
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStyle)
		
		// TRUE as this is multiplayer
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
		
	END_SCALEFORM_MOVIE_METHOD()
		
	PRINTLN("...KGM MP [JobList][App][Biker]: Populate_Biker_Coop_Invite() - Set up invite for slot [", iInviteSlot, "]. Invitor: ", GET_PLAYER_NAME(g_sBossAgencyApp.bossAgencyInvites[iInviteSlot].playerInvitor))
	
	RETURN TRUE

ENDFUNC

/// PURPOSE:
///    This displays the full phone screen view of an invitation (after pressing accept on an invite)
PROC Display_Biker_Invitation_Screen()
	
	IF g_Cellphone.PhoneDS != PDS_COMPLEXAPP
		g_Cellphone.PhoneDS = PDS_COMPLEXAPP
		PRINTLN("...KGM MP [JobList][App][Biker]: Display_Biker_Invitation_Screen() g_Cellphone.PhoneDS = PDS_COMPLEXAPP")
	ENDIF

	// Delete all data from the JobList Screen
    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieIndex, "SET_DATA_SLOT_EMPTY", (TO_FLOAT(JOB_VIEW_STATE)))
	
	BOOL bUsePlayerHeadshot = FALSE
	STRING invitorTXD
	PEDHEADSHOT_ID invitorHeadshot
	INT iArrayIndex = GB_GET_GANG_INVITE_INDEX(m_selectedJobSlot, GANG_INVITE_BIKER)
	PLAYER_INDEX bossID = g_sBossAgencyApp.bossAgencyInvites[iArrayIndex].playerInvitor

	PRINTLN("...KGM MP [JobList][App][Biker]: Display_Biker_Invitation_Screen() for invite: ", iArrayIndex, ", adjusted from: ", m_selectedJobSlot)

    BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieIndex, "SET_DATA_SLOT")
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(JOB_VIEW_STATE)
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		
		// Add the player name first
		IF IS_NET_PLAYER_OK(bossID, FALSE)
			TEXT_LABEL_63 tlPlayerName = Get_Name_String(bossID)
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
				ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(tlPlayerName)
			END_TEXT_COMMAND_SCALEFORM_STRING()
			bUsePlayerHeadshot = TRUE
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")
		ENDIF
		
		IF g_sBossAgencyApp.bossAgencyInvites[iArrayIndex].eInviteType = BOSS_APP_INVITE_TYPE_JOIN_ORG
					
			IF g_sMPTunables.bexec_vip2_disable_invite_info 
			
				// Grab a clean list of goons for this Gang. (Note we only support 3 goons at the moment. Need new text labels if this increases)
				PLAYER_INDEX goons[MAX_GOONS_FOR_INVITE_SCREEN]
				INT i
				INT iCount
				
				REPEAT MAX_GOONS_FOR_INVITE_SCREEN i
					IF iCount < MAX_GOONS_FOR_INVITE_SCREEN
						IF GlobalplayerBD_FM_3[NATIVE_TO_INT(bossID)].sMagnateGangBossData.GangMembers[i] != INVALID_PLAYER_INDEX()
							IF IS_NET_PLAYER_OK(GlobalplayerBD_FM_3[NATIVE_TO_INT(bossID)].sMagnateGangBossData.GangMembers[i], FALSE)	
								goons[iCount] = GlobalplayerBD_FM_3[NATIVE_TO_INT(bossID)].sMagnateGangBossData.GangMembers[i]
								iCount++
							ENDIF
						ENDIF
					ENDIF
				ENDREPEAT
				
				
				// Main body text
				IF iCount = 0
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MC_APP_BODY_TA")
				ELSE			
					TEXT_LABEL_15 tlBodyText = "BA_APP_BODY_T4"					
					TEXT_LABEL_63 tlPlayerName
					
					IF iCount = 2
						tlBodyText = "MC_APP_BODY_T5"						
					ELIF iCount > 2
						tlBodyText = "MC_APP_BODY_T6"
					ENDIF
					
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING(tlBodyText)
						REPEAT iCount i
							tlPlayerName = Get_Name_String(goons[i])
							ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(tlPlayerName)
						ENDREPEAT
					END_TEXT_COMMAND_SCALEFORM_STRING()
				ENDIF
				
			ELSE
			
				TEXT_LABEL_15 tlBodyText
				tlBodyText = "MC_APP_BODY_T7"
				BEGIN_TEXT_COMMAND_SCALEFORM_STRING(tlBodyText)
					ADD_TEXT_COMPONENT_INTEGER(GB_GET_NUM_GOONS_IN_PLAYER_GANG(bossID))
					ADD_TEXT_COMPONENT_INTEGER(g_sBossAgencyApp.bossInviteInfo[iArrayIndex].iWarehouses)
				END_TEXT_COMMAND_SCALEFORM_STRING()
					
				CPRINTLN(DEBUG_AMBIENT, "[Script_PIM] BOSS_APP_INVITE_TYPE_JOIN_ORG Invite Data. iWarehouses = ", g_sBossAgencyApp.bossInviteInfo[iArrayIndex].iWarehouses)
			ENDIF
			
		ELIF g_sBossAgencyApp.bossAgencyInvites[iArrayIndex].eInviteType = BOSS_APP_INVITE_TYPE_BOSS_V_BOSS
			
			PLAYER_INDEX playerRival
			playerRival = g_sImpromptuVars.playerThatSentImpromptuInvite
			IF NOT GB_IS_PLAYER_BOSS_OF_A_BIKER_GANG(PLAYER_ID())
			AND((playerRival != INVALID_PLAYER_INDEX()) AND (NOT GB_IS_PLAYER_BOSS_OF_A_BIKER_GANG(playerRival)))
				BEGIN_TEXT_COMMAND_SCALEFORM_STRING("BA_APP_BODY_DM2")
					//SET_COLOUR_OF_NEXT_TEXT_COMPONENT(GB_GET_PLAYER_GANG_HUD_COLOUR(g_sBossAgencyApp.bossAgencyInvites[iArrayIndex].playerInvitor))
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(g_sBossAgencyApp.bossAgencyInvites[iArrayIndex].playerInvitor)) // The Boss of ~a~~s~ has challenged you to a Boss Vs. Boss Deathmatch.
				END_TEXT_COMMAND_SCALEFORM_STRING()
			ELSE
				BEGIN_TEXT_COMMAND_SCALEFORM_STRING("BA_APP_BODY_BKVBK2")
					//SET_COLOUR_OF_NEXT_TEXT_COMPONENT(GB_GET_PLAYER_GANG_HUD_COLOUR(g_sBossAgencyApp.bossAgencyInvites[iArrayIndex].playerInvitor))
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(g_sBossAgencyApp.bossAgencyInvites[iArrayIndex].playerInvitor)) // The Boss of ~a~~s~ has challenged you to a Biker v Biker Deathmatch.
				END_TEXT_COMMAND_SCALEFORM_STRING()
			ENDIF

		ELIF g_sBossAgencyApp.bossAgencyInvites[iArrayIndex].eInviteType = BOSS_APP_INVITE_TYPE_BIKER_V_BIKER_JOUST
			
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("BA_APP_BODY_JS2")
				//SET_COLOUR_OF_NEXT_TEXT_COMPONENT(GB_GET_PLAYER_GANG_HUD_COLOUR(g_sBossAgencyApp.bossAgencyInvites[iArrayIndex].playerInvitor))
				ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(g_sBossAgencyApp.bossAgencyInvites[iArrayIndex].playerInvitor)) // The Boss of ~a~~s~ has challenged you to a Boss Vs. Boss Deathmatch.
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ELIF g_sBossAgencyApp.bossAgencyInvites[iArrayIndex].eInviteType = BOSS_APP_INVITE_TYPE_BIKER_COOP
			TEXT_LABEL_31 tl31InviteBody
			tl31InviteBody = GET_BIKER_COOP_INVITE_BODY_TEXT_LABEL()
			TEXT_LABEL_15 subText = GET_BIKER_INVITE_FEED_TITLE()
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING(tl31InviteBody)
				//SET_COLOUR_OF_NEXT_TEXT_COMPONENT(GB_GET_PLAYER_GANG_HUD_COLOUR(g_sBossAgencyApp.bossAgencyInvites[iArrayIndex].playerInvitor))
				ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(g_sBossAgencyApp.bossAgencyInvites[iArrayIndex].playerInvitor)) // The Boss of ~a~~s~ has challenged you to a Boss Vs. Boss Deathmatch.
				ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(subText)
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ENDIF
		
		// Contact Picture
        IF (bUsePlayerHeadshot)
            invitorHeadshot = Get_HeadshotID_For_Player(g_sBossAgencyApp.bossAgencyInvites[iArrayIndex].playerInvitor)
            IF (invitorHeadshot = NULL)
            	bUsePlayerHeadshot = FALSE
            ELSE
            	invitorTXD = GET_PEDHEADSHOT_TXD_STRING(invitorHeadshot)
           	ENDIF            
        ENDIF
        
        // Display the player headshot if available, or the default headshot if not
        IF (bUsePlayerHeadshot)
            // ...display the headshot
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING(invitorTXD)
            PRINTLN("...KGM MP [JobList][App][Biker]: Display_Boss_Agency_Invitation_Screen() - Passing player headshot TXD: ", invitorTXD)
        ELSE
            // ...use the default headshot
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(m_defaultContactPicture)
        ENDIF
		
		INT iStyle = GET_LOCAL_PLAYER_STYLE(iArrayIndex)
				
		PRINTLN("...KGM MP [JobList][App][Biker]: Display_Biker_Invitation_Screen() - iStyle: ", iStyle)

		// Colour of invite box
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStyle)
		
	END_SCALEFORM_MOVIE_METHOD()
	
	 // Display the screen
    // NOTE: Must be after the slots have been filled
    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", (TO_FLOAT(JOB_VIEW_STATE)))

    // Display the Header - the Mission Type
    Display_MP_App_Header(m_invitationText)
	
	// For an Invitation, display 'Accept Job' and 'Back' buttons
    // GB 23/6/13: Also display 'delete' button for Invites from other players
    // GB 10/7/13: Also display 'delete' button for Invites from NPCs
    Display_MP_App_Buttons(MP_APP_ICON_YES, m_jobListIconTextAcceptJob, MP_APP_ICON_DELETE, m_jobListIconTextCancelJob, MP_APP_ICON_BACK, m_jobListIconTextBack, m_sAppData)
    
    // Now on the 'Confirm Invitation' screen
    m_currentStage = JLS_ALTERNATIVE_INVITE_SCREEN	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Fill the FM Joblist screen slots with data
// NOTES:   TEMP until FM uses Mission Controller - data layout needs to be different
PROC Fill_FM_Joblist_Screen_Slots()

	m_totalAlternativeInvites = 0
    INT             temploop            = 0
    INT             slotsUsed           = 0

    // Delete all data from the JobList View before filling it with new data
    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieIndex, "SET_DATA_SLOT_EMPTY", (TO_FLOAT(JOBLIST_VIEW_STATE)))
    
    #IF IS_DEBUG_BUILD
        NET_PRINT("...KGM MP [JobList][App]: Fill_FM_Joblist_Screen_Slots() - clearing joblist [view state: ") NET_PRINT_INT(JOBLIST_VIEW_STATE) NET_PRINT("]") NET_NL()
    #ENDIF
    
    // KGM 4/12/12: If there is a fake joblist invite, then only display it
    IF (Has_Fake_Joblist_Invite_Been_Received())
        Fill_FM_Joblist_Screen_Slot_Fake_Invitation()
        EXIT
    ENDIF
	
		 
    #IF IS_DEBUG_BUILD
        NET_PRINT("...KGM MP [JobList][App]: Fill_FM_Joblist_Screen_Slots() - No of Biker invites: ") NET_PRINT_INT(GB_GET_TOTAL_INVITES()) NET_NL()
    #ENDIF
	
	INT iLoop
	REPEAT GB_GET_TOTAL_INVITES() iLoop
	
		// Populate our invites here
		IF g_sBossAgencyApp.bossAgencyInvites[iLoop].eInviteState = BOSS_APP_INVITE_STATE_AVAILABLE
		AND g_sBossAgencyApp.bossAgencyInvites[iLoop].eGangInviteType = GANG_INVITE_BIKER
			
			SWITCH g_sBossAgencyApp.bossAgencyInvites[iLoop].eInviteType
			
				CASE BOSS_APP_INVITE_TYPE_JOIN_ORG
					IF Populate_Gang_Standard_Invite(iLoop, slotsUsed)
						slotsUsed++
						m_totalAlternativeInvites++
					ENDIF
				BREAK
				
				CASE BOSS_APP_INVITE_TYPE_BOSS_V_BOSS
					IF Populate_Gang_BossVBoss_Invite(iLoop, slotsUsed)
						slotsUsed++
						m_totalAlternativeInvites++
					ENDIF
				BREAK	
				
				CASE BOSS_APP_INVITE_TYPE_BIKER_V_BIKER_JOUST
					IF Populate_BikerVBiker_Joust_Invite(iLoop, slotsUsed)
						slotsUsed++
						m_totalAlternativeInvites++
					ENDIF
				BREAK
				
				CASE BOSS_APP_INVITE_TYPE_BIKER_COOP
					IF Populate_Biker_Coop_Invite(iLoop, slotsUsed)
						slotsUsed++
						m_totalAlternativeInvites++
					ENDIF
				BREAK
			ENDSWITCH
		ENDIF
	
	ENDREPEAT
	
    
    REPEAT MAX_MP_JOBLIST_ENTRIES tempLoop
        SWITCH (g_sJobListMP[tempLoop].jlState)
            // There is no entry at this array position, so ignore it
            CASE NO_MP_JOBLIST_ENTRY
                BREAK
                
            CASE MP_JOBLIST_CURRENT_MISSION
            CASE MP_JOBLIST_JOINABLE_MISSION
            CASE MP_JOBLIST_JOINABLE_PLAYLIST
                Fill_FM_Joblist_Screen_Slot_Current_or_Joinable_Mission(slotsUsed, tempLoop)
                slotsUsed++
                BREAK
                
            CASE MP_JOBLIST_INVITATION
                Fill_FM_Joblist_Screen_Slot_Invitation(slotsUsed, tempLoop)
                slotsUsed++
                BREAK
                
            CASE MP_JOBLIST_CS_INVITE
                Fill_FM_Joblist_Screen_Slot_CSInvite(slotsUsed, tempLoop)
                slotsUsed++
                BREAK
                
            CASE MP_JOBLIST_RV_INVITE
                Fill_FM_Joblist_Screen_Slot_RVInvite(slotsUsed, tempLoop)
                slotsUsed++
                BREAK
            
            DEFAULT
                #IF IS_DEBUG_BUILD
                    NET_PRINT("...KGM MP [JobList][App]: Fill_FM_Joblist_Screen_Slots(): ERROR - Unknown Joblist State for slot.") NET_NL()
                    SCRIPT_ASSERT("Fill_FM_Joblist_Screen_Slots(): ERROR - Unknown Joblist State for slot. Add it to SWITCH statement. Tell Keith.")
                #ENDIF
                BREAK
        ENDSWITCH
    ENDREPEAT
    
//    #IF IS_DEBUG_BUILD
//        // Make sure the number of slots used equals the number of entries in the JobList
//        IF NOT (slotsUsed = g_numMPJobListEntries)
//            NET_PRINT("...KGM MP [JobList][App]: ERROR: FM JobList entries = ") NET_PRINT_INT(g_numMPJobListEntries)
//            NET_PRINT("  scaleform slots used = ") NET_PRINT_INT(slotsUsed) NET_NL()
//            SCRIPT_ASSERT("Fill_FM_Joblist_Screen_Slots() different number of slots used than there are entries in the JobList. See COnsole Log. Tell Keith")
//        ENDIF
//    #ENDIF
    
    IF (slotsUsed > 0)
        // ...the display has been redrawn, so return TRUE
        EXIT
    ENDIF
    
    // There are no entries
    // KGM 2/3/13: If the player has a wanted level and no entries are displayed, then display a message
// KGM 31/8/13: ToDo 1605311 (Pay to lose Wanted Level when accepting invites)
//    IF (g_sJobListMP[0].jlState != MP_JOBLIST_CURRENT_MISSION)
//        IF (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0)
//            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(JOBLIST_VIEW_STATE)),
//                                (TO_FLOAT(0)), INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM,
//                                "CELL_247", "CELL_248") //No Jobs Available With Wanted Level
//                                
//            // Display help text
//            IF NOT (m_wantedLevelHelpDisplayed)
//                m_wantedLevelHelpDisplayed = TRUE
//                
//                PRINT_HELP("FMMC_WANTED_LEVEL")
//            ENDIF
//            
//            EXIT
//        ENDIF
//    ENDIF
	

    // Player doesn't have a Wanted Level
    Fill_FM_Joblist_Screen_No_Jobs()
        
    #IF IS_DEBUG_BUILD
        NET_PRINT("...KGM MP [JobList][App]: FM JOBLIST SCALEFORM SLOT DATA: No Missions In Progress") NET_NL()
    #ENDIF

ENDPROC

// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Fill the JobList screen slots with data
PROC Fill_JobList_Screen_Slots()

    // KGM 31/7/12: HOPEFULLY TEMP UNTIL FM USES MISSION CONTROLLER, THEN IT CAN BE REMOVED?
    IF (GET_CURRENT_GAMEMODE() = GAMEMODE_FM)
    OR (GET_CURRENT_GAMEMODE() = GAMEMODE_SP)
    OR NOT (NETWORK_IS_GAME_IN_PROGRESS())

        Fill_FM_Joblist_Screen_Slots()
        EXIT
    ENDIF
    
    // Fill the text labels for the two lines of text
    TEXT_LABEL_15   topText         = ""
    TEXT_LABEL_15   bottomText      = ""
    MP_MISSION      thisMissionID   = eNULL_MISSION
    INT             playersTeam     = GET_PLAYER_TEAM(PLAYER_ID())
    INT             slotsUsed       = 0
    BOOL            storeDetails    = FALSE
    INT             tempLoop        = 0

    // Delete all data from the JobList View before filling it with new data
    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieIndex, "SET_DATA_SLOT_EMPTY", (TO_FLOAT(JOBLIST_VIEW_STATE)))
    
    #IF IS_DEBUG_BUILD
        NET_PRINT("...KGM MP [JobList][App]: Fill_JobList_Screen_Slots() - clearing joblist [view state: ") NET_PRINT_INT(JOBLIST_VIEW_STATE) NET_PRINT("]") NET_NL()
    #ENDIF	
    
    REPEAT MAX_MP_JOBLIST_ENTRIES tempLoop
        storeDetails = FALSE
        
        IF NOT (g_sJobListMP[tempLoop].jlState = NO_MP_JOBLIST_ENTRY)
            thisMissionID = INT_TO_ENUM(MP_MISSION, g_sJobListMP[tempLoop].jlMissionAsInt)
        
            SWITCH (g_sJobListMP[tempLoop].jlState)
                CASE MP_JOBLIST_CURRENT_MISSION
                    topText         = "CELL_260"                                                    // "Current"
                    bottomText      = GET_MP_MISSION_NAME_TEXT_LABEL(thisMissionID, playersTeam)    // Mission Name
                    storeDetails    = TRUE
                    BREAK
                    
                CASE MP_JOBLIST_JOINABLE_MISSION
                    topText         = "CELL_263"                                                    // "Joinable"
                    bottomText      = GET_MP_MISSION_NAME_TEXT_LABEL(thisMissionID, playersTeam)    // Mission Name
                    storeDetails    = TRUE
                    BREAK
                    
                CASE MP_JOBLIST_INVITATION
                    topText         = "CELL_265"                                                    // "Invitation"
                    bottomText      = GET_MP_MISSION_NAME_TEXT_LABEL(thisMissionID, playersTeam)    // Mission Name
                    storeDetails    = TRUE
                    BREAK
                    
                DEFAULT
                    #IF IS_DEBUG_BUILD
                        SCRIPT_ASSERT("Fill_JobList_Screen_Slots(): Unknown JobList State ENUM")
                        topText     = "CELL_250"    // "ERROR"
                        bottomText  = "CELL_250"    // "ERROR"
                        storeDetails    = TRUE
                    #ENDIF
                    BREAK
            ENDSWITCH
        ENDIF
        
        IF (storeDetails)
            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(JOBLIST_VIEW_STATE)),
                                (TO_FLOAT(slotsUsed)), INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM,
                                topText, bottomText)
            
            #IF IS_DEBUG_BUILD
                NET_PRINT("...KGM MP [JobList][App]: JOBLIST SCALEFORM SLOT DATA")
                NET_PRINT(" [Slot ") NET_PRINT_INT(slotsUsed) NET_PRINT("] [") NET_PRINT(GET_STRING_FROM_TEXT_FILE(topText))
                NET_PRINT("] [") NET_PRINT(GET_STRING_FROM_TEXT_FILE(bottomText)) NET_PRINT("]") NET_NL()
            #ENDIF
            
            // Increment slots used
            slotsUsed++
        ENDIF
    ENDREPEAT
    
    #IF IS_DEBUG_BUILD
        // Make sure the number of slots used equals the number of entries in the JobList
        IF NOT (slotsUsed = g_numMPJobListEntries)
            NET_PRINT("...KGM MP [JobList][App]: ERROR: JobList entries = ") NET_PRINT_INT(g_numMPJobListEntries)
            NET_PRINT("  scaleform slots used = ") NET_PRINT_INT(slotsUsed) NET_NL()
            SCRIPT_ASSERT("Fill_JobList_Screen_Slots() different number of slots used than there are entries in the JobList. See Console Log. Tell Keith")
        ENDIF
    #ENDIF
    
    IF (slotsUsed > 0)
        // ...the display has been redrawn, so return TRUE
        EXIT
    ENDIF
    
    // There are no entries
    Fill_FM_Joblist_Screen_No_Jobs()
    
    #IF IS_DEBUG_BUILD
        NET_PRINT("...KGM MP [JobList][App]: JOBLIST SCALEFORM SLOT DATA: No Jobs In Progress") NET_NL()
    #ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: On launch the game may want to quick display a specific job invitation for quick acceptance
//
// INPUT PARAMS:        paramEntry          // Entry on from teh joblist array where the screen should be displayed
// RETURN VALUE:        BOOL                // TRUE if a Quick Launch screen was displayed, otherwise FALSE
FUNC BOOL Quick_Display_Single_Job(INT paramEntry)

    IF NOT (g_ShouldForceSelectionOfLatestAppItem)
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList][App]: g_ShouldForceSelectionOfLatestAppItem is FALSE so a DPADUP Quick Display of a single screen is NOT REQUIRED")
            NET_NL()
        #ENDIF
                
        RETURN FALSE
    ENDIF
    
    // A quick display single screen is required
    // NOTE: This will likely need more work to be 100% accurate
    g_ShouldForceSelectionOfLatestAppItem = FALSE
    
    #IF IS_DEBUG_BUILD
        NET_PRINT("...KGM MP [JobList][App]: g_ShouldForceSelectionOfLatestAppItem is TRUE so attempting a Quick Single Job Display")
        NET_NL()
    #ENDIF
	
	// BUG 1903099: Just in case the data has updated between bringing up the phone and the joblist launching
	IF (g_numMPJobListEntries = 0)
	AND (GB_GET_TOTAL_BIKER_INVITES() = 0)
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList][App]: Ignoring Quick Display. There are now no jobs to display.")
            NET_NL()
        #ENDIF
        
        RETURN FALSE
	ENDIF
	
	// Handle biker invites first
	IF GB_GET_TOTAL_BIKER_INVITES() > m_selectedJobSlot
	
		PRINTLN("...KGM MP [JobList][App][Biker] Dpad up functionality. Showing Biker invite.")
	
		Display_Biker_Invitation_Screen()
	
		g_Cellphone.PhoneDS = PDS_COMPLEXAPP
	
		RETURN TRUE
	ENDIF	
	
    
    SWITCH (g_sJobListMP[paramEntry].jlState)
		CASE NO_MP_JOBLIST_ENTRY
            #IF IS_DEBUG_BUILD
                NET_PRINT("...KGM MP [JobList][App]: Ignoring Quick Display. First Joblist entry is now NO_MP_JOBLIST_ENTRY so data must have changed.")
                NET_NL()
            #ENDIF
            
			RETURN FALSE
	
        CASE MP_JOBLIST_CURRENT_MISSION
            #IF IS_DEBUG_BUILD
                NET_PRINT("...KGM MP [JobList][App]: Ignoring Quick Display for Current Mission screen")
                NET_NL()
            #ENDIF
            
            RETURN FALSE
            
        CASE MP_JOBLIST_JOINABLE_MISSION
        CASE MP_JOBLIST_JOINABLE_PLAYLIST
            #IF IS_DEBUG_BUILD
                NET_PRINT("...KGM MP [JobList][App]: Ignoring Quick Display for Joinable Mission or Playlist screen")
                NET_NL()
            #ENDIF
            
            RETURN FALSE
            
        CASE MP_JOBLIST_INVITATION
            // Display the Invitation screen
            #IF IS_DEBUG_BUILD
                NET_PRINT("...KGM MP [JobList][App]: Doing Quick Display for Invitation in Joblist ArrayPos: ")
                NET_PRINT_INT(paramEntry)
                NET_NL()
            #ENDIF
			
			// Don't allow if player can't afford it
			IF NOT (Can_I_Afford_To_Accept_Invite(g_sJobListMP[paramEntry].jlMissionAsInt, g_sJobListMP[paramEntry].jlSubtype))
	            #IF IS_DEBUG_BUILD
	                NET_PRINT("      IGNORING QUICK DISPLAY: Player can't afford it") NET_NL()
	            #ENDIF
				
				RETURN FALSE
			ENDIF
			
			// Don't allow if the invite is closed
			IF (IS_BIT_SET(g_sJobListMP[paramEntry].jlBitfield, JOBLIST_BITFLAG_INVITE_CLOSED))
	            #IF IS_DEBUG_BUILD
	                NET_PRINT("      IGNORING QUICK DISPLAY: Closed") NET_NL()
	            #ENDIF
				
				RETURN FALSE
			ENDIF
            
			// Allowed
            Display_Invitation_Screen()
            BREAK
            
        CASE MP_JOBLIST_CS_INVITE
            // Display the Cross-Session Invite screen
            #IF IS_DEBUG_BUILD
                NET_PRINT("...KGM MP [JobList][App]: Doing Quick Display for Cross-Session Invite in Joblist ArrayPos: ")
                NET_PRINT_INT(paramEntry)
                NET_NL()
            #ENDIF
			
			// Don't allow if player can't afford it
			IF (NETWORK_IS_GAME_IN_PROGRESS())
				IF NOT (Can_I_Afford_To_Accept_Invite(g_sJobListMP[paramEntry].jlMissionAsInt, g_sJobListMP[paramEntry].jlSubtype))
		            #IF IS_DEBUG_BUILD
		                NET_PRINT("      IGNORING QUICK DISPLAY: Player can't afford it") NET_NL()
		            #ENDIF
					
					RETURN FALSE
				ENDIF
			ENDIF
			
			// Don't allow if the invite is closed
			IF (IS_BIT_SET(g_sJobListMP[paramEntry].jlBitfield, JOBLIST_BITFLAG_INVITE_CLOSED))
	            #IF IS_DEBUG_BUILD
	                NET_PRINT("      IGNORING QUICK DISPLAY: Closed") NET_NL()
	            #ENDIF
				
				RETURN FALSE
			ENDIF
            
			// Allowed
            Display_CSInvite_Screen()
            BREAK
            
        CASE MP_JOBLIST_RV_INVITE
            // Display the Basic Invite screen
            #IF IS_DEBUG_BUILD
                NET_PRINT("...KGM MP [JobList][App]: Doing Quick Display for Basic Invite in Joblist ArrayPos: ")
                NET_PRINT_INT(paramEntry)
                NET_NL()
            #ENDIF
			
			// Don't allow for Apartment if the player has a wanted level and can't afford to clear it
			IF (g_sJobListMP[paramEntry].jlMissionAsInt = FMMC_TYPE_APARTMENT)
        		IF (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0)
					IF NOT (Can_I_Afford_To_Clear_Wanted_Level())
			            #IF IS_DEBUG_BUILD
			                NET_PRINT("      IGNORING QUICK DISPLAY: Player can't afford to clear wanted level") NET_NL()
			            #ENDIF
						
						RETURN FALSE
					ENDIF
				ENDIF
			ENDIF
			
			// Don't allow if the invite is closed
			IF (IS_BIT_SET(g_sJobListMP[paramEntry].jlBitfield, JOBLIST_BITFLAG_INVITE_CLOSED))
	            #IF IS_DEBUG_BUILD
	                NET_PRINT("      IGNORING QUICK DISPLAY: Closed") NET_NL()
	            #ENDIF
				
				RETURN FALSE
			ENDIF
            
			// Allowed
            Display_RVInvite_Screen()
            BREAK
			
		DEFAULT
            #IF IS_DEBUG_BUILD
                NET_PRINT("...KGM MP [JobList][App]: Ignoring Quick Display. Ended up in the default case.")
				SCRIPT_ASSERT("Quick_Display_Single_Job(): ERROR - UNKNOWN SWITCH CASE - Add to switch statement. Tell Keith.")
                NET_NL()
            #ENDIF
            
            RETURN FALSE
    ENDSWITCH
    
    // The app is now displaying a deeper screen than the main screen, so the phone needs to know it is in ComplexApp state so it doesn't return to Home Screen on BACK
    g_Cellphone.PhoneDS = PDS_COMPLEXAPP
    #IF IS_DEBUG_BUILD
        // ...some additional debugging help - a unique identifier for each time g_Cellphone.phoneDS gets updated
        cdPrintnl()
        cdPrintstring("STATE ASSIGNMENT 277. appMPJobListNEW::Quick_Display_Single_Job() assigns PDS_COMPLEXAPP")
        cdPrintnl()
    #ENDIF
    
    RETURN TRUE

ENDFUNC

// PURPOSE: Displays the Help message when selecting a joblist invite. This is called when a specific invite
//			is selected, alerting the player that they will be charged for accepting an invite. Added to  
//			Process_Stage_Invitation() and  Process_Stage_CSInvite()
PROC Print_Pay_Wanted_Level_Help()
	
	// KGM 31/8/13: ToDo 1605311 (Pay to lose Wanted Level when accepting invites)
    IF (g_sJobListMP[0].jlState != MP_JOBLIST_CURRENT_MISSION)
	AND (g_numMPJobListEntries > 0)
        IF (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0)
            // Display help text, but not for Basic Invites or SP
			IF (NETWORK_IS_GAME_IN_PROGRESS())
				BOOL displayHelp = FALSE
				INT jobsLoop = 0
				
				REPEAT g_numMPJobListEntries jobsLoop
					IF NOT (displayHelp)
						IF (g_sJobListMP[jobsLoop].jlState = MP_JOBLIST_INVITATION)
						OR (g_sJobListMP[jobsLoop].jlState = MP_JOBLIST_CS_INVITE)
							displayHelp = TRUE
						ENDIF
						
						IF (g_sJobListMP[jobsLoop].jlState = MP_JOBLIST_RV_INVITE)
							IF (g_sJobListMP[jobsLoop].jlMissionAsInt = FMMC_TYPE_APARTMENT)
								displayHelp = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDREPEAT
				
				IF (displayHelp)
		            IF NOT (m_wantedLevelHelpDisplayed)
		                m_wantedLevelHelpDisplayed = TRUE
		                PRINT_HELP_WITH_NUMBER("FMMC_PAY_TO_LOSE_WL_THIS", GET_CONTACT_REQUEST_COST(REQUEST_REMOVE_WANTED))
		            ENDIF
				ENDIF
			ENDIF
		ELSE
			IF m_wantedLevelHelpDisplayed = TRUE
				CLEAR_HELP()
				m_wantedLevelHelpDisplayed = FALSE
			ENDIF
        ENDIF
		
    ENDIF

ENDPROC

PROC Print_Join_Heist_Help(INT currentInviteSlot)

    IF (g_sJobListMP[0].jlState != MP_JOBLIST_CURRENT_MISSION)
	AND (g_numMPJobListEntries > 0)
            // Display help text, but not for Basic Invites or SP
			IF (NETWORK_IS_GAME_IN_PROGRESS())
				BOOL displayHelp = FALSE
				
				IF g_sJobListMP[currentInviteSlot].jlMissionAsInt = FMMC_TYPE_HEIST_PHONE_INVITE
				OR g_sJobListMP[currentInviteSlot].jlMissionAsInt = FMMC_TYPE_HEIST_PHONE_INVITE_2
					displayHelp = TRUE
				ENDIF
								
				IF (displayHelp)
		            IF NOT (m_wantedLevelHelpDisplayed)
		                m_wantedLevelHelpDisplayed = TRUE
						IF GANGOPS_FLOW_IS_A_GANGOPS_MISSION_FROM_ROOT_ID(g_sJobListMP[currentInviteSlot].jlRootContentIdHash)
			                PRINT_HELP_WITH_PLAYER_NAME("GO_JOIN_HEIST_HELP", g_sJobListMP[currentInviteSlot].jlMissionName, HUD_COLOUR_WHITE)
						ELSE
							IF (g_sJobListMP[currentInviteSlot].jlSubtype = FMMC_MISSION_TYPE_PLANNING)
			                	PRINT_HELP("FMMC_JOIN_PREP_HELP")
							ELSE
			                	PRINT_HELP_WITH_PLAYER_NAME("FMMC_JOIN_HEIST_HELP", g_sJobListMP[currentInviteSlot].jlMissionName, HUD_COLOUR_WHITE)
							ENDIF
		            	ENDIF
					ENDIF
				ENDIF
		ELSE
			IF m_wantedLevelHelpDisplayed = TRUE
				CLEAR_HELP()
				m_wantedLevelHelpDisplayed = FALSE
			ENDIF
        ENDIF
		
    ENDIF

ENDPROC

PROC Print_Cant_Join_From_Here()

    IF (g_sJobListMP[0].jlState != MP_JOBLIST_CURRENT_MISSION)
	AND (g_numMPJobListEntries > 0)
        // Display help text
		IF (NETWORK_IS_GAME_IN_PROGRESS())
			BOOL displayHelp = FALSE
			
			IF NOT (displayHelp)
				IF (g_sJobListMP[Get_Invite_Array_Index()].jlState = MP_JOBLIST_INVITATION)
				AND g_sJobListMP[Get_Invite_Array_Index()].jlMissionAsInt = FMMC_TYPE_MISSION
				AND g_sJobListMP[Get_Invite_Array_Index()].jlSubtype = FMMC_MISSION_TYPE_CONTACT
				AND IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(),MPAM_TYPE_CINEMA) 
					displayHelp = TRUE
				ENDIF
			ENDIF
			
			IF (displayHelp)
	            IF NOT (m_cantAcceptJobHelpDisplayed)
	                m_cantAcceptJobHelpDisplayed = TRUE
	                PRINT_HELP("FMMC_CANT_JOIN_FROM_HERE")
	            ENDIF
			ENDIF
		ENDIF		
    ENDIF
ENDPROC
// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Display the JobList screen
//
// INPUT PARAMS:            paramDoUpdate       [DEFAULT = TRUE] TRUE means update the data, FALSE means only display the already generated data
//                          paramKeepHighlight  [DEFAULT = TRUE] TRUE means keep the highlighter position if the data hasn't changed, FALSE means always reset it to first position
//                          paramJustLaunched   [DEFAULT = FALSE] TRUE if the Joblist app has just launched, FALSE if not (allows checking for an immediate display of a single job)
PROC Display_JobList_Screen(BOOL paramDoUpdate = TRUE, BOOL paramKeepHighlighter = TRUE, BOOL paramJustLaunched = FALSE)
	
	// KGM 22/7/15 [BUG 2437060]: If any Invite Accept warning message is on display, clear it
	Clear_Accept_Invite_Warning_Help_Text_If_On_Display()
	
    // The JobList page is the Apps first screen, so the phone should be in RunningApp mode so that it knows to return to the HomeScreen on 'BACK'.
    IF NOT (g_Cellphone.PhoneDS = PDS_RUNNINGAPP)
        g_Cellphone.PhoneDS = PDS_RUNNINGAPP
        
        #IF IS_DEBUG_BUILD
            // ...some additional debugging help - a unique identifier for each time g_Cellphone.phoneDS gets updated
            cdPrintnl()
            cdPrintstring("STATE ASSIGNMENT 201. appMPJobListNEW::Display_JobList_Screen() assigns PDS_RUNNINGAPP")
            cdPrintnl()
        #ENDIF
    ENDIF
    
    // Setup the highlighter initial position
    INT highlighterPosition = 0
    IF (paramKeepHighlighter)
        highlighterPosition = m_selectedJobSlot
    ENDIF
    
    IF (paramDoUpdate)
        // If an update is definitely required then ignore the return value from this function
		PRINTLN("[MMM] Called from Display_JobList_Screen")
        IF (Update_JobList_Details())
            // ...the joblist has changed, so reset the highlighter position
            highlighterPosition = 0
        ENDIF
    ENDIF
    
	INT iTotalInvites = g_numMPJobListEntries
	iTotalInvites += GB_GET_TOTAL_BIKER_INVITES()
	
    // Ensure the highlighter position is safe
    IF (highlighterPosition >= iTotalInvites)
        highlighterPosition = 0
    ENDIF
    
    // KGM 22/6/13: If player is on a current Job, display the 'cancel job' screen instead
    IF (g_numMPJobListEntries = 1)	//0)		//RLB 14/08/13: This is so that Impromptu Race invites are displayed and selectable when on Missions.
    AND (g_sJobListMP[0].jlState = MP_JOBLIST_CURRENT_MISSION)
        // Display the 'Cancel Job' screen instead
		Display_Current_Job_Screen()
        EXIT
    ENDIF
	
	IF m_wantedLevelHelpDisplayed = TRUE
		CLEAR_HELP()
		m_wantedLevelHelpDisplayed = FALSE
	ENDIF
	
    // KGM 6/8/13: If just launched and this was a DPADUP quick launch, then jump immediately to the first screen
    IF (paramJustLaunched)
        m_selectedJobSlot = 0
        IF (Quick_Display_Single_Job(m_selectedJobSlot))
            EXIT
        ENDIF
    ENDIF
    
    // Store the details from the JobList array in the JobList Screen slots
    Fill_JobList_Screen_Slots()
    
    // Display the screen
    // NOTE: Must be after the slots have been filled
    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", (TO_FLOAT(JOBLIST_VIEW_STATE)), (TO_FLOAT(highlighterPosition)))
    
    #IF IS_DEBUG_BUILD
        NET_PRINT("...KGM MP [JobList][App]: Display_JobList_Screen() - displaying joblist [view state: ") NET_PRINT_INT(JOBLIST_VIEW_STATE) NET_PRINT("]") NET_NL()
    #ENDIF
    
    // Display the Header
    IF (GET_CURRENT_GAMEMODE() = GAMEMODE_FM)
	OR NOT (NETWORK_IS_GAME_IN_PROGRESS())
        Display_MP_App_Header(m_joblistTextFM)
    ELSE
        Display_MP_App_Header(m_joblistTextCrook)
    ENDIF
    
    // Display the Buttons?
    BOOL displayAllButtons = FALSE
    IF (iTotalInvites > 0)
        // ...yes, there are valid entries
        displayAllButtons = TRUE
    ENDIF
    
    IF NOT (displayAllButtons)
        IF (Has_Fake_Joblist_Invite_Been_Received())
        AND NOT (g_fakeJoblistInvite.fjiAccepted)
            // ...yes, there is a fake joblist invite
            displayAllButtons = TRUE
        ENDIF
    ENDIF
    
    IF (displayAllButtons)
        // Display 'Yes' and 'Back' buttons
		// KGM 5/9/13: Also 'delete'
        Display_MP_App_Buttons(MP_APP_ICON_YES, m_jobListIconTextOptions, MP_APP_ICON_DELETE, m_jobListIconTextCancelJob, MP_APP_ICON_BACK, m_jobListIconTextBack, m_sAppData)
    ELSE
        // Only display 'Back' button
        Display_MP_App_Buttons(MP_APP_ICON_BLANK, "", MP_APP_ICON_BLANK, "", MP_APP_ICON_BACK, m_jobListIconTextBack, m_sAppData)
    ENDIF
    
    // Now on the JobList screen
    m_currentStage = JLS_JOBLIST

ENDPROC


/// PURPOSE:
///    Confirms the invite and puts away the phone
PROC Confirm_Alternative_App_Invite(INT iIndex)
	PLAYER_INDEX invitor = g_sBossAgencyApp.bossAgencyInvites[iIndex].playerInvitor

	PRINTLN("...KGM MP [JobList][App][Biker]: Confirm_Alternative_App_Invite(): iIndex = ", iIndex, " - for invite from: ", GET_PLAYER_NAME(invitor))

	IF g_sBossAgencyApp.bossAgencyInvites[iIndex].eInviteState = BOSS_APP_INVITE_STATE_AVAILABLE
	
		// Accept the invite and attempt to join the gang
		GB_REQUEST_JOIN_GANG(invitor,g_sBossAgencyApp.bossAgencyInvites[iIndex].inviteSubtype)
	
		// We delete this single invite. If accepted all invites will be deleted
		GB_Delete_Invite_In_Slot(iIndex)
	ENDIF
	
	HANG_UP_AND_PUT_AWAY_PHONE()
ENDPROC

PROC Confirm_Boss_Vs_Boss_Deathmatch_Invite(INT iIndex)

	PRINTLN("...KGM MP [JobList][App][Biker]: Confirm_Boss_Vs_Boss_Deathmatch_Invite(): iIndex = ", iIndex)

	SET_BIT(g_sImpromptuVars.iBitSet, ciBOSSVBOSS_ACCEPT_BVB_INVITE_HELP)

	// We delete this single invite. If accepted all invites will be deleted
	GB_Delete_Invite_In_Slot(iIndex)
	
	HANG_UP_AND_PUT_AWAY_PHONE()
ENDPROC

PROC Confirm_Boss_Vs_Boss_Biker_Joust_Invite(INT iIndex)

	PRINTLN("...KGM MP [JobList][App][Biker]: Confirm_Boss_Vs_Boss_Biker_Joust_Invite(): iIndex = ", iIndex)

	SET_BIT(g_sImpromptuVars.iBitSet, ciBOSSVBOSS_ACCEPT_BVB_INVITE_HELP)

	// We delete this single invite. If accepted all invites will be deleted
	GB_Delete_Invite_In_Slot(iIndex)
	
	HANG_UP_AND_PUT_AWAY_PHONE()
ENDPROC

PROC Confirm_Boss_Biker_Coop_Invite(INT iIndex)

	PRINTLN("...KGM MP [JobList][App][Biker]: Confirm_Boss_Biker_Coop_Invite(): iIndex = ", iIndex)

	SET_BIT(g_sImpromptuVars.iBitSet, ciBOSSVBOSS_ACCEPT_BVB_INVITE_HELP)

	// We delete this single invite. If accepted all invites will be deleted
	GB_Delete_Invite_In_Slot(iIndex)
	
	HANG_UP_AND_PUT_AWAY_PHONE()
ENDPROC

PROC Process_Stage_Alternative_Invite_Confirm()

	INT iBikerArrayPosition =  GB_GET_GANG_INVITE_INDEX(m_selectedJobSlot, GANG_INVITE_BIKER)
	
	// Validate the invitor
	PLAYER_INDEX invitor = g_sBossAgencyApp.bossAgencyInvites[iBikerArrayPosition].playerInvitor
	IF NOT IS_NET_PLAYER_OK(invitor, FALSE)		
		// Invitor is no longer valid, revert back to the agency screen
		Display_JobList_Screen()
		EXIT
	ENDIF
	
	// Check for scrolling (possible?)
    Check_For_MP_App_Navigation_Inputs(m_sAppData)
	
	// Check if the player wants to go BACK to Boss Agency Invite list
    IF (Did_Player_Press_MP_App_Negative_Response_Button(m_sAppData))
        Display_JobList_Screen()
        EXIT
    ENDIF
	
	// Check if the player wants to DELETE the current invite
    IF (Did_Player_Press_MP_App_Special_Response_Button(m_sAppData))
      	
		PRINTLN("...KGM MP [JobList][App][Biker]: Player has chosen to DELETE boss agency invite: ", iBikerArrayPosition)
		
		GB_Delete_Invite_In_Slot(iBikerArrayPosition)
        Display_JobList_Screen()
        EXIT
    ENDIF
	
	// If the player accepts, request to join and delete this invite (if we are confirmed all invites will be deleted)
	IF (Did_Player_Press_MP_App_Positive_Response_Button(m_sAppData))
	
		IF IS_MP_PASSIVE_MODE_ENABLED()
			
			bAcceptPressed = TRUE
		
			PRINTLN("...KGM MP [JobList][App][Biker]: Player has passive mode enabled. Warn them they will lose this.")
			m_currentStage = JLS_ALTERNATIVE_PASSIVE_WARNING			
		ELSE
			
			IF g_sBossAgencyApp.bossAgencyInvites[iBikerArrayPosition].eInviteType = BOSS_APP_INVITE_TYPE_JOIN_ORG
		       	Confirm_Alternative_App_Invite(iBikerArrayPosition)
			ELIF g_sBossAgencyApp.bossAgencyInvites[iBikerArrayPosition].eInviteType = BOSS_APP_INVITE_TYPE_BOSS_V_BOSS
				Confirm_Boss_Vs_Boss_Deathmatch_Invite(iBikerArrayPosition)
			ELIF g_sBossAgencyApp.bossAgencyInvites[iBikerArrayPosition].eInviteType = BOSS_APP_INVITE_TYPE_BIKER_V_BIKER_JOUST
				Confirm_Boss_Vs_Boss_Biker_Joust_Invite(iBikerArrayPosition)
			ELIF g_sBossAgencyApp.bossAgencyInvites[iBikerArrayPosition].eInviteType = BOSS_APP_INVITE_TYPE_BIKER_COOP
				Confirm_Boss_Biker_Coop_Invite(iBikerArrayPosition)
			ELSE
				SCRIPT_ASSERT("...KGM MP [JobList][App][Biker]: Accepting an unknown Boss Agency Invite type, HANG UP PHONE")
				HANG_UP_AND_PUT_AWAY_PHONE()
			ENDIF
		ENDIF
    ENDIF
ENDPROC

/// PURPOSE:
///    Display a warning message to the player asking them to confirm invite as a Goon to disable Passive mode
PROC Process_Stage_Alternative_Passive_Warning()

	g_sBossAgencyApp.bBossAgencyAppRunningWarningScreen = TRUE

	// Cellphone check for JUST_PRESSED rather than released. Need to wait until this is no longer true and then we can listen for our accept
	IF bAcceptPressed
		IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_POSITIVE_INPUT))
			bAcceptPressed = FALSE
		ENDIF
		EXIT
	ENDIF

	// Listen for confirmation
	IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
		
		PRINTLN("...KGM MP [JobList][App][Biker]: Process_Stage_Alternative_Passive_Warning. Player selected Accept")
		
		// Update state and accept invite
		m_currentStage = JLS_ALTERNATIVE_INVITE_SCREEN
		
		g_sBossAgencyApp.bBossAgencyAppRunningWarningScreen = FALSE
		Confirm_Alternative_App_Invite( GB_GET_GANG_INVITE_INDEX(m_selectedJobSlot, GANG_INVITE_BIKER))
		
		EXIT
	ENDIF
	
	// Listen for player backing out
	IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)

		PRINTLN("...KGM MP [JobList][App][Biker]: Process_Stage_Alternative_Passive_Warning. Player selected Cancel")
		m_currentStage = JLS_ALTERNATIVE_INVITE_SCREEN
	
		g_sBossAgencyApp.bBossAgencyAppRunningWarningScreen = FALSE
	
		EXIT
	ENDIF
	
	// Display warning message every frame.
	SET_WARNING_MESSAGE_WITH_HEADER("HUD_INPUT23", "BA_APP_PMC_W", FE_WARNING_YESNO)
	
ENDPROC





// -----------------------------------------------------------------------------------------------------------
//      Confirm screen
// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Display the common details of the Confirm screen
PROC Display_Confirm_Screen()

    // Use the Side Task 'tick' icon
    CONST_INT   CONFIRM_SLOT_ICON       12

    // Delete all data from the Confirm View before filling it with new data
    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieIndex, "SET_DATA_SLOT_EMPTY", (TO_FLOAT(CONFIRM_VIEW_STATE)))
    
    #IF IS_DEBUG_BUILD
        NET_PRINT("...KGM MP [JobList][App]: Display_Confirm_Screen() - clearing joblist [view state: ") NET_PRINT_INT(CONFIRM_VIEW_STATE) NET_PRINT("]") NET_NL()
    #ENDIF

    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(CONFIRM_VIEW_STATE)),
                    (TO_FLOAT(0)), (TO_FLOAT(CONFIRM_SLOT_ICON)), INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, m_areYouSureText)
    
    // Display the screen
    // NOTE: Must be after the slots have been filled
    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", (TO_FLOAT(CONFIRM_VIEW_STATE)))
    
    // For the confirmation screen, display 'Yes' and 'No' buttons
    Display_MP_App_Buttons(MP_APP_ICON_YES, m_joblistIconTextYes, MP_APP_ICON_BLANK, "", MP_APP_ICON_NO, m_joblistIconTextNo, m_sAppData)
                        
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Display the Cancel Job confirmation screen
PROC Display_Confirm_Cancel_Job_Screen()
    
    // Display the Confirm Screen
    Display_Confirm_Screen()
    
    // Display the Header
    Display_MP_App_Header(m_cancelJobText)
    
    // Now on the 'Confirm Cancel Job' screen
    m_currentStage = JLS_CONFIRM_CANCEL_JOB

ENDPROC




// -----------------------------------------------------------------------------------------------------------
//      Display a 'Please Wait' screen - used if ther eappears to be a problem getting reserved for an invite
// -----------------------------------------------------------------------------------------------------------

PROC Display_Please_Wait_For_Confirmation_Screen()

    // Change to a 'Please Wait' screen
    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieIndex, "SET_DATA_SLOT_EMPTY", (TO_FLOAT(JOB_VIEW_STATE)))
	
	#IF IS_DEBUG_BUILD
    	NET_PRINT("...KGM MP [JobList][App]: Display_Please_Wait_For_Confirmation_Screen() - clearing job details [view state: ") NET_PRINT_INT(JOB_VIEW_STATE) NET_PRINT("]") NET_NL()
	#ENDIF
	
	// Field Details
	TEXT_LABEL_15       typeText                = ""
    INT                 thisSlot                = 0
    TEXT_LABEL_63       invitorTXD              = ""
    TEXT_LABEL_63       invitorName             = ""
    PEDHEADSHOT_ID      invitorHeadshot         = NULL
    PLAYER_INDEX        theInvitingPlayer       = INVALID_PLAYER_INDEX()
    enumCharacterList   theInvitingNPC          = NO_CHARACTER
    BOOL                invitorIsPlayer         = IS_BIT_SET(g_sJobListMP[Get_Invite_Array_Index()].jlBitfield, JOBLIST_BITFLAG_INVITOR_IS_PLAYER)
    BOOL                invitorIsValid          = TRUE
    BOOL                useInvitorHeadshotTXD   = FALSE
	
	// Store the details of the Invitation - there is only one slot
    BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieIndex, "SET_DATA_SLOT")
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(JOB_VIEW_STATE)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(thisSlot)
		
		// Get Type text (ie: deathMatch, Mission, etc)
		typeText 			= Get_FM_Joblist_Activity_TextLabel(g_sJobListMP[Get_Invite_Array_Index()].jlMissionAsInt, g_sJobListMP[Get_Invite_Array_Index()].jlSubtype,g_sJobListMP[Get_Invite_Array_Index()].jlPropertyType, g_sJobListMP[Get_Invite_Array_Index()].jlRootContentIdHash)
		
		// Header Line of Body (use The Invitor Name (literal string composed using the Text Commands))
		// TEMP: Invitor as In stored in 'uniqueID' field
		IF (invitorIsPlayer)
			theInvitingPlayer	= INT_TO_PLAYERINDEX(g_sJobListMP[Get_Invite_Array_Index()].jlUniqueID)
			invitorIsValid		= IS_NET_PLAYER_OK(theInvitingPlayer, FALSE)
		ELSE
			theInvitingNPC		= INT_TO_ENUM(enumCharacterList, g_sJobListMP[Get_Invite_Array_Index()].jlUniqueID)
			invitorIsValid		= TRUE
		ENDIF
		
		IF (invitorIsValid)
			IF (invitorIsPlayer)
				invitorName	= GET_PLAYER_NAME(theInvitingPlayer)  				
			ELSE
				TEXT_LABEL	npcNameTL		= Get_NPC_Name(theInvitingNPC)
				STRING		npcNameAsString	= CONVERT_TEXT_LABEL_TO_STRING(npcNameTL)
				
				invitorName = GET_FILENAME_FOR_AUDIO_CONVERSATION(npcNameAsString)
			ENDIF
			
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
				ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(invitorName)
			END_TEXT_COMMAND_SCALEFORM_STRING()
			
			useInvitorHeadshotTXD = TRUE
		ELSE
			// Invitor doesn't exist so use the mission type
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING(typeText)
			END_TEXT_COMMAND_SCALEFORM_STRING()
			
			useInvitorHeadshotTXD = FALSE
		ENDIF
		
		// Display Body text - Please Wait
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("WAIT_RESERVE")
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		// Contact Picture
		IF (useInvitorHeadshotTXD)
			// ...the invitorID is valid, so try to get the contact picture
			IF (invitorIsPlayer)
				invitorHeadshot = Get_HeadshotID_For_Player(theInvitingPlayer)
				
				IF (invitorHeadshot = NULL)
					useInvitorHeadshotTXD = FALSE
				ELSE
					invitorTXD = GET_PEDHEADSHOT_TXD_STRING(invitorHeadshot)
				ENDIF
			ELSE
				invitorTXD = GET_FILENAME_FOR_AUDIO_CONVERSATION(Get_NPC_Headshot(theInvitingNPC))
			ENDIF
			
		ENDIF
		
		// Display the player headshot if available, or the default headshot if not
		IF (useInvitorHeadshotTXD)
			// ...display the headshot
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING(invitorTXD)
			
			#IF IS_DEBUG_BUILD
		    	NET_PRINT("...KGM MP [JobList][App]: Display_Please_Wait_For_Confirmation_Screen() - passing player headshot TXD: ") NET_PRINT(invitorTXD) NET_NL()
			#ENDIF
		ELSE
			// ...use the default headshot
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(m_defaultContactPicture)
		ENDIF
    END_SCALEFORM_MOVIE_METHOD()
    
    // Display the screen
    // NOTE: Must be after the slots have been filled
    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", (TO_FLOAT(JOB_VIEW_STATE)))

    // Display the Header - the Mission Type
    Display_MP_App_Header(m_invitationText)
    
    // Remove all buttons
    Display_MP_App_Buttons(MP_APP_ICON_BLANK, "", MP_APP_ICON_BLANK, "", MP_APP_ICON_BLANK, "", m_sAppData)
    
    // Don't change the stage
                
ENDPROC




// -----------------------------------------------------------------------------------------------------------
//      Confirm 'Pay To Lose Wanted Level' screen
// -----------------------------------------------------------------------------------------------------------

PROC Display_Generic_Lose_Wanted_Level_Screen()
    
    // Delete all data from the JobList Screen
    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieIndex, "SET_DATA_SLOT_EMPTY", (TO_FLOAT(JOB_VIEW_STATE)))
	
	#IF IS_DEBUG_BUILD
    	NET_PRINT("...KGM MP [JobList][App]: Display_Generic_Lose_Wanted_Level_Screen() - clearing job details [view state: ") NET_PRINT_INT(JOB_VIEW_STATE) NET_PRINT("]") NET_NL()
	#ENDIF
	
    // Display 'lose wanted level?"
    BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieIndex, "SET_DATA_SLOT")
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(JOB_VIEW_STATE)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		
		// Header Line of Body (use The Invitor Name (literal string composed using the Text Commands))
		// Display Lester name
		enumCharacterList	npcLester		= CHAR_LESTER
		TEXT_LABEL			npcNameTL		= Get_NPC_Name(npcLester)
			
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(npcNameTL)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		// Display Body text
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING(m_loseWantedLevelText)
			// ...display cost for losing wanted level
			ADD_TEXT_COMPONENT_INTEGER(GET_CONTACT_REQUEST_COST(REQUEST_REMOVE_WANTED))
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		// Lester headshot
		TEXT_LABEL_63 lesterTXD = GET_FILENAME_FOR_AUDIO_CONVERSATION(Get_NPC_Headshot(npcLester))
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING(lesterTXD)
    END_SCALEFORM_MOVIE_METHOD()
    
    // Display the screen
    // NOTE: Must be after the slots have been filled
    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", (TO_FLOAT(JOB_VIEW_STATE)))
    
    // For the confirmation screen, display 'Yes' and 'No' buttons
    Display_MP_App_Buttons(MP_APP_ICON_YES, m_joblistIconTextYes, MP_APP_ICON_BLANK, "", MP_APP_ICON_NO, m_joblistIconTextNo, m_sAppData)
    
    // Display the Header
    Display_MP_App_Header(m_acceptJobText)
                        
ENDPROC


// -----------------------------------------------------------------------------------------------------------

PROC Display_Invitation_Lose_Wanted_Level_Screen()

    Display_Generic_Lose_Wanted_Level_Screen()
	
    // Now on the 'CSInvite Confirm Lose Wanted Level' screen
    m_currentStage = JLS_CONFIRM_INVITE_LOSE_WANTED_LEVEL
                        
ENDPROC


// -----------------------------------------------------------------------------------------------------------

PROC Display_CSInvite_Lose_Wanted_Level_Screen()

    Display_Generic_Lose_Wanted_Level_Screen()
	
    // Now on the 'CSInvite Confirm Lose Wanted Level' screen
    m_currentStage = JLS_CONFIRM_CSINVITE_LOSE_WANTED_LEVEL
                        
ENDPROC


// -----------------------------------------------------------------------------------------------------------

PROC Display_RVInvite_Lose_Wanted_Level_Screen()

    Display_Generic_Lose_Wanted_Level_Screen()
	
    // Now on the 'RVInvite Confirm Lose Wanted Level' screen
    m_currentStage = JLS_CONFIRM_RVINVITE_LOSE_WANTED_LEVEL
                        
ENDPROC




// -----------------------------------------------------------------------------------------------------------
//      Wait For Current Mission To Quit screen
// -----------------------------------------------------------------------------------------------------------

// PURPOSE: A holding screen while waiting to be told the player has quit the mission
// NOTES:   Retain the data on the 'Confirm Cancel' screen, but remove the buttons
PROC Display_Wait_For_Current_Mission_To_Quit_Screen()

    // Remove all the buttons
    Display_MP_App_Buttons(MP_APP_ICON_BLANK, "", MP_APP_ICON_BLANK, "", MP_APP_ICON_BLANK, "", m_sAppData)
    
    // Now on the 'Wait For Player To Quit Mission' screen
    m_currentStage = JLS_WAITING_FOR_PLAYER_TO_QUIT_MISSION

ENDPROC




// ===========================================================================================================
//      App Stages - Processing
// ===========================================================================================================

// PURPOSE:	Delete the invitation in the specified slot
//
// INPUT PARAMS:		paramSlot			The slot to be deleted
PROC Delete_Selected_Invitation(INT paramSlot)

    // Ensure the invite is still available
    INT     		invitationSlot  	= NO_JOBLIST_ARRAY_ENTRY
    BOOL    		invitorIsPlayer 	= IS_BIT_SET(g_sJobListMP[paramSlot].jlBitfield, JOBLIST_BITFLAG_INVITOR_IS_PLAYER)
	PLAYER_INDEX	theInvitingPlayer	= INVALID_PLAYER_INDEX()
    
    IF (invitorIsPlayer)
		theInvitingPlayer = INT_TO_PLAYERINDEX(g_sJobListMP[paramSlot].jlUniqueID)
        invitationSlot  = Get_Joblist_Invitation_Slot_For_Player(theInvitingPlayer)
    ELSE
        enumCharacterList   theInvitingNPC  = INT_TO_ENUM(enumCharacterList, g_sJobListMP[paramSlot].jlUniqueID)
        TEXT_LABEL_63       theMissionName  = g_sJobListMP[paramSlot].jlMissionName
        BOOL                matchMission    = TRUE
        invitationSlot  = Get_Joblist_Invitation_Slot_For_NPC(theInvitingNPC, theMissionName, matchMission)
    ENDIF
    
    // ...is the invitation still available?
    IF (invitationSlot = NO_JOBLIST_ARRAY_ENTRY)
        // ...there is no invite from this invitor any more, so return to the Joblist screen
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList][App]: The invitation no longer exists so cannot be deleted - returning to JobList screen") NET_NL()
        #ENDIF
        
        Display_JobList_Screen()
        EXIT
    ENDIF
    
    // ...is the invitation still allowed to be displayed?
	// MMM 1881951 This is the check that is causing the problem. 
	//		 possibly use this (NOT IS_BIT_SET(g_sJobListMP[paramSlot].jlBitfield, JOBLIST_BITFLAG_INVITE_CLOSED)) to avoid having to remove this function if problems arise
	
	/*
	IF NOT (Is_Invitation_In_Slot_Allowed_To_Be_Displayed(invitationSlot))
        // ...the invitation in the slot is no longer allowed to be displayed
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList][App]: The invitation is no longer allowed to be displayed so cannot be deleted - returning to JobList screen") NET_NL()
        #ENDIF
        
        Display_JobList_Screen()
        EXIT
    ENDIF
	*/
	
	// Delete the Invite, if it's an NPC Invite then it may be a Contact Mission that needs deleted
    IF NOT (invitorIsPlayer)
        // Deleting NPC Invite, so check if it is a Contact Mission that needs cleaned up
        // KGM 10/7/13: This is quite specific and there must be a better more generic way to do this, but time is running out
        #IF IS_DEBUG_BUILD
            NET_PRINT("      Invite is from an NPC, so check if it is a Contact Mission that needs cleaned up") NET_NL()
        #ENDIF
    
        enumCharacterList   theInvitingNPC  = INT_TO_ENUM(enumCharacterList, g_sJobListMP[paramSlot].jlUniqueID)
        TEXT_LABEL_63       theMissionName  = g_sJobListMP[paramSlot].jlMissionName
        Contact_Mission_Invite_Deleted(theInvitingNPC, theMissionName)
	ELSE
		// For Invites from Players, let the invitor know
        #IF IS_DEBUG_BUILD
            NET_PRINT("      Invite is from a player, so not attempting to delete the Contact Mission on the current mission array") NET_NL()
        #ENDIF
		
		BROADCAST_JOBLIST_INVITE_REPLY(theInvitingPlayer, FALSE, TRUE)
	ENDIF
		        
	Delete_Joblist_Invitation_In_Invitation_Slot(invitationSlot)
	Display_JobList_Screen()

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Delete the Cross-Session Invite in the specified slot
//
// INPUT PARAMS:		paramSlot			The slot to be deleted
PROC Delete_Selected_CSInvite(INT paramSlot)
    
    // Ensure the invite is still available
    TEXT_LABEL_63   theInvitor      = g_sJobListMP[paramSlot].jlInvitorName
    INT             invitationSlot  = Get_Joblist_CSInvite_Slot_For_InvitorDisplayName(theInvitor)
    
    // ...is the invitation still available?
    IF (invitationSlot = NO_JOBLIST_ARRAY_ENTRY)
        // ...there is no invite from this invitor any more, so return to the Joblist screen
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList][App]: The Cross-Session Invite no longer exists so cannot be deleted - returning to JobList screen") NET_NL()
        #ENDIF
        
        Display_JobList_Screen()
        EXIT
    ENDIF
    
    // ...is the invitation still allowed to be displayed?
    IF NOT (Is_CSInvite_In_Slot_Allowed_To_Be_Displayed(invitationSlot))
        // ...the invitation in the slot is no longer allowed to be displayed
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList][App]: The Cross-Session Invite is no longer allowed to be displayed - returning to JobList screen") NET_NL()
        #ENDIF
        
        Display_JobList_Screen()
        EXIT
    ENDIF
	
	// Delete
	Delete_Joblist_CSInvite_In_Invitation_Slot(invitationSlot)
	Display_JobList_Screen()
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Delete the Basic Invite in the specified slot (if allowed)
//
// INPUT PARAMS:		paramSlot			The slot to be deleted
PROC Delete_Selected_RVInvite(INT paramSlot)

	// Can this invite be deleted?
	IF NOT (IS_BIT_SET(g_sJobListMP[paramSlot].jlBitfield, JOBLIST_BITFLAG_ALLOW_DELETE))
		IF NOT (IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("NO_DEL_RVINVITE"))
			PRINT_HELP("NO_DEL_RVINVITE")
		ENDIF
		
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList][App]: This Basic Invite has been marked as not deletable - returning to JobList screen") NET_NL()
        #ENDIF
        
        Display_JobList_Screen()
        EXIT
	ENDIF
	
	// Clear the 'cannot delete' help if still on display
	IF (IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("NO_DEL_RVINVITE"))
		CLEAR_HELP()
	ENDIF

    // Ensure the invite is still available
    INT     invitationSlot  = NO_JOBLIST_ARRAY_ENTRY
    BOOL    invitorIsPlayer = IS_BIT_SET(g_sJobListMP[paramSlot].jlBitfield, JOBLIST_BITFLAG_INVITOR_IS_PLAYER)
    
    IF (invitorIsPlayer)
        PLAYER_INDEX theInvitingPlayer = INT_TO_PLAYERINDEX(g_sJobListMP[paramSlot].jlUniqueID)
        invitationSlot  = Get_Joblist_RVInvite_Slot_For_Player(theInvitingPlayer)
    ELSE
        enumCharacterList   theInvitingNPC  = INT_TO_ENUM(enumCharacterList, g_sJobListMP[paramSlot].jlUniqueID)
        invitationSlot  = Get_Joblist_RVInvite_Slot_For_NPC(theInvitingNPC)
    ENDIF
    
    // ...is the invitation still available?
    IF (invitationSlot = NO_JOBLIST_ARRAY_ENTRY)
        // ...there is no RVInvite from this invitor any more, so return to the Joblist screen
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList][App]: The Basic Invite no longer exists so cannot be deleted - returning to JobList screen") NET_NL()
        #ENDIF
        
        Display_JobList_Screen()
        EXIT
    ENDIF
    
    // ...is the invitation still allowed to be displayed?
    IF NOT (Is_RVInvite_In_Slot_Allowed_To_Be_Displayed(invitationSlot))
        // ...the invitation in the slot is no longer allowed to be displayed
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList][App]: The Basic Invite is no longer allowed to be displayed so cannot be deleted - returning to JobList screen") NET_NL()
        #ENDIF
        
        Display_JobList_Screen()
        EXIT
    ENDIF
	
	// Delete the Basic Invite
	Delete_Joblist_RVInvite_In_Invitation_Slot(invitationSlot)
	Display_JobList_Screen()

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Display a Help message on the appropriate console
//
// INPUT PARAMS:		paramCost			The cost of playing the mission and losing the wanted level
PROC Display_Not_Enough_Cash_Help(INT paramCost)
	BOOL isHeistMission = FALSE
	
	PRINTLN("[MMM] Displaying NOT ENOUGH CASH")
	PRINTLN("[MMM] Mission as INT : ", g_sJobListMP[Get_Invite_Array_Index()].jlMissionAsInt)
	PRINTLN("[MMM] Subtype: ", g_sJobListMP[Get_Invite_Array_Index()].jlSubtype)
	
	IF g_sJobListMP[Get_Invite_Array_Index()].jlMissionAsInt = FMMC_TYPE_MISSION
		IF g_sJobListMP[Get_Invite_Array_Index()].jlSubtype = FMMC_MISSION_TYPE_HEIST 
		OR g_sJobListMP[Get_Invite_Array_Index()].jlSubtype = FMMC_MISSION_TYPE_PLANNING
			isHeistMission = TRUE
		ENDIF
	ENDIF
	
	PRINTLN("[MMM] Is Mission Heist? : ", isHeistMission)
	
	IF NOT (IS_HELP_MESSAGE_BEING_DISPLAYED())
		IF (IS_XBOX360_VERSION() OR IS_XBOX_PLATFORM())
			IF isHeistMission
				PRINT_HELP_WITH_NUMBER("FMMC_NAECASH_HEIST_X", paramCost)
			ELSE
           		PRINT_HELP_WITH_NUMBER("FMMC_NAECASH_X", paramCost)
			ENDIF
		ELIF (IS_PS3_VERSION() OR IS_PLAYSTATION_PLATFORM())
			IF isHeistMission
				PRINT_HELP_WITH_NUMBER("FMMC_NAECASH_HEIST_P", paramCost)
			ELSE
            	PRINT_HELP_WITH_NUMBER("FMMC_NAECASH_P", paramCost)
			ENDIF
		ELSE
			IF isHeistMission
				PRINT_HELP_WITH_NUMBER("FMMC_NAECASH_HEIST", paramCost)
			ELSE
				PRINT_HELP_WITH_NUMBER("FMMC_NAE_CASH", paramCost)
			ENDIF
		ENDIF
	ENDIF
	
    #IF IS_DEBUG_BUILD
        NET_PRINT("...KGM MP [JobList][App]: Player cannot afford the chosen job: ")
        NET_PRINT_INT(Get_Invite_Array_Index())
        NET_PRINT("   (GOT: $")
        NET_PRINT_INT(NETWORK_GET_VC_WALLET_BALANCE() + NETWORK_GET_VC_BANK_BALANCE())
        NET_PRINT("  NEED: $")
        NET_PRINT_INT(Get_Cost_Of_Accepting_Invite(g_sJobListMP[Get_Invite_Array_Index()].jlMissionAsInt, g_sJobListMP[Get_Invite_Array_Index()].jlSubtype))
        NET_PRINT(")")
		IF (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0)
			NET_PRINT(" - PLAYER HAS WANTED LEVEL")
		ENDIF
        NET_NL()
    #ENDIF
				
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Display a Help message on the appropriate console if the player doesn't have enough cash for the wager
//
// INPUT PARAMS:		paramCost			The cost of the Wager (Head-To-Head Playlist only?)
PROC Display_Not_Enough_Cash_For_Wager_Help(INT paramCost)

	// KGM 9/13/12: Just use the same help message as 'not enough cash' for now - may be able to change if there is time
	IF NOT (IS_HELP_MESSAGE_BEING_DISPLAYED())
		IF (IS_XBOX360_VERSION())
            PRINT_HELP_WITH_NUMBER("FMMC_NOWAGER_X", paramCost)
		ELIF (IS_PS3_VERSION())
            PRINT_HELP_WITH_NUMBER("FMMC_NOWAGER_P", paramCost)
		ELSE
			PRINT_HELP_WITH_NUMBER("FMMC_NO_WAGER", paramCost)
		ENDIF
	ENDIF
                
    #IF IS_DEBUG_BUILD
        NET_PRINT("...KGM MP [JobList][App]: Player cannot afford the wager for chosen job: ")
        NET_PRINT_INT(Get_Invite_Array_Index())
        NET_PRINT("   (GOT: $")
        NET_PRINT_INT(NETWORK_GET_VC_WALLET_BALANCE() + NETWORK_GET_VC_BANK_BALANCE())
        NET_PRINT("  NEED: $")
        NET_PRINT_INT(paramCost)
        NET_PRINT(")")
        NET_NL()
    #ENDIF
				
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Display a Help message on the appropriate console if the player doesn't have enough cash to clear their wanted level
PROC Display_Help_Not_Enough_Cash_To_Clear_Wanted_Level()

	INT theCost = GET_CONTACT_REQUEST_COST(REQUEST_REMOVE_WANTED)

	IF NOT (IS_HELP_MESSAGE_BEING_DISPLAYED())
		IF (IS_XBOX360_VERSION() OR IS_XBOX_PLATFORM())
            PRINT_HELP_WITH_NUMBER("FMMC_WL_COST_X", theCost)
		ELIF (IS_PS3_VERSION() OR IS_PLAYSTATION_PLATFORM())
            PRINT_HELP_WITH_NUMBER("FMMC_WL_COST_P", theCost)
		ELSE
			PRINT_HELP_WITH_NUMBER("FMMC_WL_COST", theCost)
		ENDIF
	ENDIF
                
    #IF IS_DEBUG_BUILD
        NET_PRINT("...KGM MP [JobList][App]: Player cannot afford to clear Wanted Level: ")
        NET_PRINT_INT(Get_Invite_Array_Index())
        NET_PRINT("   (GOT: $")
        NET_PRINT_INT(NETWORK_GET_VC_WALLET_BALANCE() + NETWORK_GET_VC_BANK_BALANCE())
        NET_PRINT("  NEED: $")
        NET_PRINT_INT(GET_CONTACT_REQUEST_COST(REQUEST_REMOVE_WANTED))
        NET_PRINT(")")
        NET_NL()
    #ENDIF
				
ENDPROC



// ===========================================================================================================
//      App Stages - Processing
// ===========================================================================================================

// PURPOSE: Performs navigation checks while the JobList screen is on display.
// NOTES:   This is the main screen shown when the app first triggers from teh cellphone home screen and is classed as the RUNNINGAPP screen,
//              so don't check for BACK button here, it is checked in the main loop and kills the app.
PROC Process_Stage_JobList()
    
    IF (g_numMPJobListEntries + m_totalAlternativeInvites > 1)
        Check_For_MP_App_Navigation_Inputs(m_sAppData)
    ENDIF
   
    IF (Did_Player_Press_MP_App_Positive_Response_Button(m_sAppData))
        
		// Only allow acceptance if a warp to another invitation isn't already in progress
        IF (Is_Invite_Being_Accepted())
            #IF IS_DEBUG_BUILD
                NET_PRINT("...KGM MP [JobList][App]: Process_Stage_JobList() - ERROR: Invite Acceptance is already in progress. Ignoring Accept button.") NET_NL()
            #ENDIF
            
            EXIT
        ENDIF
    
        // Player pressed 'SELECT', so get the array position of the currently highlighted entry
        BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieIndex, "GET_CURRENT_SELECTION")
        
        // KGM 19/6/12 - the return value is now delayed, so I'm going to use this quick method to wait for the return value - but it may cause subtle timing issues and need changed.
        //              If I do have to change it then it's going to be a bit of a pain-in-the-arse re-structuring. 
        //              I'll have two options:
        //                  1) Each frame, check for the return value being ready from the main WHILE loop, then act on it - I'll probably need lots of additional consistency checks
        //                  2) If the highlighter is moved by the player, immediately grab the new return value and store that to be used when necessary
        SCALEFORM_RETURN_INDEX thisReturnIndex = END_SCALEFORM_MOVIE_METHOD_RETURN_VALUE()
        
        WHILE NOT (IS_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_READY(thisReturnIndex))
            WAIT(0)
            
            NET_PRINT("...KGM MP [JobList][App]: Process_Stage_JobList() - waiting for the cursor position to be returned by scaleform") NET_NL()
        ENDWHILE
        
        m_selectedJobSlot = GET_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_INT(thisReturnIndex)
        
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList][App]: Process_Stage_JobList() - scaleform returned this jobslot value: ") NET_PRINT_INT(m_selectedJobSlot) NET_NL()
        #ENDIF
    
        // KGM 4/12/12: If there is a fake joblist invite, then only display it
        IF (Has_Fake_Joblist_Invite_Been_Received())
            Display_Fake_Invitation_Screen()
            
            g_Cellphone.PhoneDS = PDS_COMPLEXAPP
            #IF IS_DEBUG_BUILD
                // ...some additional debugging help - a unique identifier for each time g_Cellphone.phoneDS gets updated
                cdPrintnl()
                cdPrintstring("STATE ASSIGNMENT 202f. appMPJobListNEW::Process_Stage_JobList() assigns PDS_COMPLEXAPP for fake invitation")
                cdPrintnl()
            #ENDIF
            
            EXIT
        ENDIF
        
			IF m_selectedJobSlot < m_totalAlternativeInvites
				
				INT iInviteIndex =  GB_GET_GANG_INVITE_INDEX(m_selectedJobSlot, GANG_INVITE_BIKER)
				
				#IF IS_DEBUG_BUILD
	            NET_PRINT("...KGM MP [JobList][App][Biker]: Process_Stage_JobList() - scaleform returned this alternative invite value: ") NET_PRINT_INT(iInviteIndex) NET_PRINT(", adjusted from slot: ") NET_PRINT_INT(m_selectedJobSlot) NET_NL()
	       		#ENDIF			
			
		        // Move to the next screen
		        SWITCH (g_sBossAgencyApp.bossAgencyInvites[iInviteIndex].eInviteState)
		            CASE BOSS_APP_INVITE_STATE_EMPTY
		                #IF IS_DEBUG_BUILD
		                    // Our invite in the app has become empty (something has refreshed our data).
		                    GB_Debug_Display_Boss_Agency_App_Details()
		                    NET_PRINT("...m_selectedBossAgencyInvite returned from scaleform: ") NET_PRINT_INT(iInviteIndex) NET_PRINT(" - This was now on an EMPTY Boss Agency Invite") NET_NL()
		                #ENDIF
		                EXIT
		                
		            // Valid Entries
		            CASE BOSS_APP_INVITE_STATE_AVAILABLE
						Display_Biker_Invitation_Screen()
		                BREAK
		                
		            // Unknown Entry
		            DEFAULT
		                #IF IS_DEBUG_BUILD
		                    GB_Debug_Display_Boss_Agency_App_Details()
		                    NET_PRINT("...m_selectedBossAgencyInvite returned from scaleform: ") NET_PRINT_INT(iInviteIndex) NET_NL()
		                    SCRIPT_ASSERT("Process_Stage_JobList(): SELECT was chosen on an unknown type of Boss Agency Invite array entry. See Console log. Tell James.")
		                #ENDIF
		                EXIT
		        ENDSWITCH		       
	        
		        // The app is now displaying a deeper screen than the main screen, so the phone needs to know it is in ComplexApp state so it doesn't return to Home Screen on BACK
		        g_Cellphone.PhoneDS = PDS_COMPLEXAPP
		        #IF IS_DEBUG_BUILD
		            // ...some additional debugging help - a unique identifier for each time g_Cellphone.phoneDS gets updated
		            cdPrintnl()
		            cdPrintstring("STATE ASSIGNMENT 202. appMPBossAgency::Process_Stage_JobList() assigns PDS_COMPLEXAPP")
		            cdPrintnl()
		        #ENDIF
			
				EXIT
			ENDIF
		
		
        // KGM 3/3/13: If this is a 'closed' invitation, then ignore the player's input
        IF (IS_BIT_SET(g_sJobListMP[Get_Invite_Array_Index()].jlBitfield, JOBLIST_BITFLAG_INVITE_CLOSED))
			IF NOT (IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FMMC_JOB_FULL"))
            	PRINT_HELP("FMMC_JOB_FULL")
			ENDIF
            
            #IF IS_DEBUG_BUILD
                NET_PRINT("...KGM MP [JobList][App]: Process_Stage_JobList() - players chosen job is closed: ") NET_PRINT_INT(m_selectedJobSlot) NET_NL()
            #ENDIF
            
            EXIT
        ENDIF
		
		// KGM 9/12/13: If this is a Head-To-Head wager, check if the player can afford it
		IF (g_sJobListMP[Get_Invite_Array_Index()].jlWagerH2H > 0)
			IF NOT (Can_I_Afford_Wager_If_Head_To_Head_Playlist(g_sJobListMP[Get_Invite_Array_Index()].jlWagerH2H))
				Display_Not_Enough_Cash_For_Wager_Help(g_sJobListMP[Get_Invite_Array_Index()].jlWagerH2H)
                
                EXIT
			ENDIF
		ENDIF
        
        // KGM 16/4/13: If the player can't afford this mission, then ignore the player's input
		//		CSInvites can be accepted in SP
        IF (g_sJobListMP[Get_Invite_Array_Index()].jlState = MP_JOBLIST_JOINABLE_MISSION)
        OR (g_sJobListMP[Get_Invite_Array_Index()].jlState = MP_JOBLIST_INVITATION)
            IF NOT (Can_I_Afford_To_Accept_Invite(g_sJobListMP[Get_Invite_Array_Index()].jlMissionAsInt, g_sJobListMP[Get_Invite_Array_Index()].jlSubtype))
				Display_Not_Enough_Cash_Help(Get_Cost_Of_Accepting_Invite(g_sJobListMP[Get_Invite_Array_Index()].jlMissionAsInt, g_sJobListMP[Get_Invite_Array_Index()].jlSubtype))
                
                EXIT
            ENDIF
        ENDIF
		
		// KGM 25/9/13: Apartment Invites cost money to accept with a Wanted Level
		IF (g_sJobListMP[Get_Invite_Array_Index()].jlMissionAsInt = FMMC_TYPE_APARTMENT)
    		IF (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0)
				IF NOT (Can_I_Afford_To_Clear_Wanted_Level())
					Display_Help_Not_Enough_Cash_To_Clear_Wanted_Level()
					
					EXIT
				ENDIF
			ENDIF
		ENDIF
		
        
        // Normal Joblist entries
        // Move to the next screen
        SWITCH (g_sJobListMP[Get_Invite_Array_Index()].jlState)
            CASE NO_MP_JOBLIST_ENTRY
                #IF IS_DEBUG_BUILD
                    // KGM 19/6/12 - with the new 'delayed return' from scaleform, this assert may actually trigger if the joblist changes while we're waiting for return value
                    // I may need to handle the delayed return in a better way, but for now I'll just wait(0) until it returns.
                    Debug_Display_JobList()
                    NET_PRINT("...Get_Invite_Array_Index() returned from scaleform: ") NET_PRINT_INT(Get_Invite_Array_Index()) NET_PRINT(" - This was now on an EMPTY Joblist Array Entry") NET_NL()
                #ENDIF
                EXIT
                
            // Valid Entries
            CASE MP_JOBLIST_CURRENT_MISSION
            CASE MP_JOBLIST_JOINABLE_MISSION
            CASE MP_JOBLIST_JOINABLE_PLAYLIST
            CASE MP_JOBLIST_INVITATION
            CASE MP_JOBLIST_CS_INVITE
            CASE MP_JOBLIST_RV_INVITE
                BREAK
                
            // Unknown Entry
            DEFAULT
                #IF IS_DEBUG_BUILD
                    Debug_Display_JobList()
                    NET_PRINT("...Get_Invite_Array_Index() returned from scaleform: ") NET_PRINT_INT(Get_Invite_Array_Index()) NET_NL()
                    SCRIPT_ASSERT("Process_Stage_JobList(): SELECT was chosen on an unknown type of JobList array entry. See Console log. Tell Keith.")
                #ENDIF
                EXIT
        ENDSWITCH
        
        // Display the appropriate buttons and move on to the correct next stage
        // NOTE: Only valid entries remain at this stage in the function
        SWITCH (g_sJobListMP[Get_Invite_Array_Index()].jlState)
            CASE MP_JOBLIST_CURRENT_MISSION
                // Display the Current Job screen
                Display_Current_Job_Screen()
                BREAK
                
            CASE MP_JOBLIST_JOINABLE_MISSION
            CASE MP_JOBLIST_JOINABLE_PLAYLIST
                // Display the Joinable Job screen
                Display_Joinable_Job_Screen()
                BREAK
                
            CASE MP_JOBLIST_INVITATION
                // Display the Invitation screen
                Display_Invitation_Screen()
                BREAK
                
            CASE MP_JOBLIST_CS_INVITE
                // Display the Cross-Session Invite screen
                Display_CSInvite_Screen()
                BREAK
                
            CASE MP_JOBLIST_RV_INVITE
                // Display the Basic Invite screen
                Display_RVInvite_Screen()
                BREAK
        ENDSWITCH
        
        // The app is now displaying a deeper screen than the main screen, so the phone needs to know it is in ComplexApp state so it doesn't return to Home Screen on BACK
        g_Cellphone.PhoneDS = PDS_COMPLEXAPP
        #IF IS_DEBUG_BUILD
            // ...some additional debugging help - a unique identifier for each time g_Cellphone.phoneDS gets updated
            cdPrintnl()
            cdPrintstring("STATE ASSIGNMENT 202. appMPJobListNEW::Process_Stage_JobList() assigns PDS_COMPLEXAPP")
            cdPrintnl()
        #ENDIF
        
        EXIT
    ENDIF
     
    IF (Did_Player_Press_MP_App_Special_Response_Button(m_sAppData))
        // Only allow deletion if a warp to another invitation isn't already in progress
        IF (Is_Invite_Being_Accepted())
            #IF IS_DEBUG_BUILD
                NET_PRINT("...KGM MP [JobList][App]: Process_Stage_JobList() - ERROR: Invite Acceptance is already in progress. Ignoring Delete button.") NET_NL()
            #ENDIF
            
            EXIT
        ENDIF
    
        // Player pressed 'DELETE', so get the array position of the currently highlighted entry
        BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieIndex, "GET_CURRENT_SELECTION")
        
        // KGM 19/6/12 - the return value is now delayed, so I'm going to use this quick method to wait for the return value - but it may cause subtle timing issues and need changed.
        //              If I do have to change it then it's going to be a bit of a pain-in-the-arse re-structuring. 
        //              I'll have two options:
        //                  1) Each frame, check for the return value being ready from the main WHILE loop, then act on it - I'll probably need lots of additional consistency checks
        //                  2) If the highlighter is moved by the player, immediately grab the new return value and store that to be used when necessary
        SCALEFORM_RETURN_INDEX thisReturnIndex = END_SCALEFORM_MOVIE_METHOD_RETURN_VALUE()
        
        WHILE NOT (IS_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_READY(thisReturnIndex))
            WAIT(0)
            
            NET_PRINT("...KGM MP [JobList][App]: Process_Stage_JobList() - waiting for the cursor position to be returned by scaleform for 'delete'") NET_NL()
        ENDWHILE
        
        m_selectedJobSlot = GET_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_INT(thisReturnIndex)
        
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList][App]: Process_Stage_JobList() - scaleform returned this jobslot value for 'delete': ") NET_PRINT_INT(m_selectedJobSlot) NET_NL()
        #ENDIF
//        
//        // KGM 5/9/13: 'Closed' invitations can still be re-opened, so allowing these to be deleted
//        IF (IS_BIT_SET(g_sJobListMP[Get_Invite_Array_Index()].jlBitfield, JOBLIST_BITFLAG_INVITE_CLOSED))
//            PRINT_HELP("FMMC_JOB_CLOSED")
//            
//            #IF IS_DEBUG_BUILD
//                NET_PRINT("...KGM MP [JobList][App]: Process_Stage_JobList() - players chosen job is closed: ") NET_PRINT_INT(Get_Invite_Array_Index()) NET_NL()
//            #ENDIF
//            
//            EXIT
//        ENDIF

		// Switch our logic to biker invites if there are some displayed
		IF m_selectedJobSlot < m_totalAlternativeInvites
			
			PRINTLN("...KGM MP [JobList][App][Biker] Deleting a biker invite at index: ",  GB_GET_GANG_INVITE_INDEX(m_selectedJobSlot, GANG_INVITE_BIKER), ", adjusted from: ", m_selectedJobSlot)
			GB_Delete_Invite_In_Slot(GB_GET_GANG_INVITE_INDEX(m_selectedJobSlot, GANG_INVITE_BIKER))
			Display_JobList_Screen()
						
			EXIT
		ENDIF
		
        
        // Normal Joblist entries
        // Move to the next screen
        SWITCH (g_sJobListMP[Get_Invite_Array_Index()].jlState)
            CASE NO_MP_JOBLIST_ENTRY
                #IF IS_DEBUG_BUILD
                    // KGM 19/6/12 - with the new 'delayed return' from scaleform, this assert may actually trigger if the joblist changes while we're waiting for return value
                    // I may need to handle the delayed return in a better way, but for now I'll just wait(0) until it returns.
                    Debug_Display_JobList()
                    NET_PRINT("...Get_Invite_Array_Index() returned from scaleform for 'delete': ") NET_PRINT_INT(Get_Invite_Array_Index()) NET_PRINT(" - This was now on an EMPTY Joblist Array Entry") NET_NL()
                #ENDIF
                EXIT
                
            // Ignorable Entries
            CASE MP_JOBLIST_CURRENT_MISSION
            CASE MP_JOBLIST_JOINABLE_MISSION
            CASE MP_JOBLIST_JOINABLE_PLAYLIST
				EXIT
				
			// Deletable Entries
            CASE MP_JOBLIST_INVITATION
				Delete_Selected_Invitation(Get_Invite_Array_Index())
				EXIT
				
            CASE MP_JOBLIST_CS_INVITE
				Delete_Selected_CSInvite(Get_Invite_Array_Index())
				EXIT
				
			// Ignore for now - some are deletable, others aren't
            CASE MP_JOBLIST_RV_INVITE
 				Delete_Selected_RVInvite(Get_Invite_Array_Index())
               EXIT
                
            // Unknown Entry
            DEFAULT
                #IF IS_DEBUG_BUILD
                    Debug_Display_JobList()
                    NET_PRINT("...Get_Invite_Array_Index() returned from scaleform for 'delete': ") NET_PRINT_INT(Get_Invite_Array_Index()) NET_NL()
                    SCRIPT_ASSERT("Process_Stage_JobList(): SELECT was chosen on an unknown type of JobList array entry. See Console log. Tell Keith.")
                #ENDIF
                EXIT
        ENDSWITCH
        
        EXIT
    ENDIF
   
    // Set the flag to allow the Joblist to be updated while the JobList App is on display
    g_sJoblistUpdate.joblistActiveUpdate = TRUE
    
    // While on this screen, allow live updating of the joblist
    IF (g_sJoblistUpdate.isUpdated)
	OR GB_Is_Boss_Agency_App_Bit_Set(GB_BOSS_AGENCY_BIT_INVITES_UPDATED)
        // ...the data has just been updated, so no need to update it again
        BOOL updateData = FALSE
        Display_JobList_Screen(updateData, FALSE)
		
		GB_Clear_Boss_Agency_App_Bit(GB_BOSS_AGENCY_BIT_INVITES_UPDATED)
    ENDIF
    
    // BACK button checked in main loop and kills the app since this is the first screen of the app triggered from the cellphone home page
    
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Performs navigation checks while the Current Job screen is on display
PROC Process_Stage_Current_Job()
    
    // Check for scrolling
    Check_For_MP_App_Navigation_Inputs(m_sAppData)
    
// KGM 22/6/13: This is now the 'first' screen when there is a current job, so it isn't classed as a 'complex' app here anymore,
//      so 'Back' will allow Steve's routines will take this back to the home screen
//  // Check if the player wants to go BACK to JobList
  	IF g_Cellphone.PhoneDS = PDS_COMPLEXAPP				//RLB 14/08/13: Impromptu Race invites are displayed and selectable when on Missions. This allows the phone to go back a menu when the Mission option is selected.
		IF (Did_Player_Press_MP_App_Negative_Response_Button(m_sAppData))
	      	Display_JobList_Screen()
	      	EXIT
	  	ENDIF
    ENDIF
	
	// Check if quitting mid-mission has been disabled.
	IF GB_IS_GLOBAL_NON_BD_BIT1_SET(eGB_GLOBAL_NON_BD_BITSET_1_DISABLE_QUIT_MID_MISSION)
		Display_JobList_Screen()
		EXIT
	ENDIF
	
    // Check if the player wants to CANCEL the current mission
    IF (Did_Player_Press_MP_App_Special_Response_Button(m_sAppData))
        Display_Confirm_Cancel_Job_Screen()
        
        // The app is now displaying a deeper screen than the main screen, so the phone needs to know it is in ComplexApp state so it doesn't return to Home Screen on BACK
        g_Cellphone.PhoneDS = PDS_COMPLEXAPP
        #IF IS_DEBUG_BUILD
            // ...some additional debugging help - a unique identifier for each time g_Cellphone.phoneDS gets updated
            cdPrintnl()
            cdPrintstring("STATE ASSIGNMENT 194. appMPJobListNEW::Process_Stage_Current_Job() assigns PDS_COMPLEXAPP")
            cdPrintnl()
        #ENDIF
        EXIT
    ENDIF
    
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Performs navigation checks while the Joinable Job screen is on display
PROC Process_Stage_Joinable_Job()
    
    // Check if the player wants to go BACK to JobList
    IF (Did_Player_Press_MP_App_Negative_Response_Button(m_sAppData))
        Display_JobList_Screen()
        EXIT
    ENDIF
    
    // Check if the player wants to JOIN the joinable mission
    IF (Did_Player_Press_MP_App_Positive_Response_Button(m_sAppData))
        // The joblist shouldn't update while the player is deeper into the app than the JobList screen to ensure joblist details are preserved.
        // In theory, this check shouldn't fail - if it does then go back to the jobList screen
        // ...ensure it is a joinable mission
        IF NOT (g_sJobListMP[Get_Invite_Array_Index()].jlState     = MP_JOBLIST_JOINABLE_MISSION)
        AND NOT (g_sJobListMP[Get_Invite_Array_Index()].jlState    = MP_JOBLIST_JOINABLE_PLAYLIST)
            #IF IS_DEBUG_BUILD
                NET_PRINT("...KGM MP [JobList][App]: Attempting to join joinable job but Joblist array position does not contain joinable mission or playlist data") NET_NL()
                Debug_Display_JobList_Array_Position(Get_Invite_Array_Index())
                SCRIPT_ASSERT("Process_Stage_Confirm_Cancel_Job(): Choosing Confirm Join failed to find valid job data. See console log. Tell Keith.")
            #ENDIF
            
            Display_JobList_Screen()
            EXIT
        ENDIF

        // Join the mission
        // KGM 31/7/12: TEMP UNTIL FM MODE USES MISSION CONTROLLER
        IF (GET_CURRENT_GAMEMODE() = GAMEMODE_FM)
            // Fade out before joining
            m_currentStage = JLS_FM_WAITING_FOR_FADEOUT_BEFORE_JOIN
            
            EXIT
        ENDIF
        
        //player is forcing their own instance, clean this up.      
        IF IS_PLAYER_FORCING_OWN_INSTANCE_OF_CORONA(PLAYER_ID())
            NET_PRINT("...KGM MP [JobList][App]: IS_PLAYER_FORCING_OWN_INSTANCE_OF_CORONA = True, clearing to accept an invite") NET_NL()
            CLEAR_PLAYER_FORCING_OWN_INSTANCE_OF_CORONA()
        ENDIF
        
        // Join Mission
        Broadcast_Request_Join_Existing_Mission(g_sJobListMP[Get_Invite_Array_Index()].jlUniqueID, MP_MISSION_SOURCE_JOBLIST)
        
        // Player is attempting to join the mission.
        // Hangup and put away the phone and quit the app
        HANG_UP_AND_PUT_AWAY_PHONE()
        
        EXIT
    ENDIF
    
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Try to reserve player's spot on Joblist Invite Mission
//
// INPUT PARAMS:        paramFeedID                     The FeedID associated with invite being accepted
// RETURN VALUE:        BOOL                            TRUE if the request was accepted (this only means there was no immediate data problem, the player still isn't reserved), FALSE if there was a problem
FUNC BOOL Try_To_Reserve_Player_On_Joblist_Invite_Mission(INT paramFeedID)

    // Clear the additional variables required for the reservation request
    INT             reservationRequestUniqueID  = NO_UNIQUE_ID
    VECTOR          coronaCoords                = g_sJobListMP[Get_Invite_Array_Index()].jlCoords
	INT 			iRootContentIdHash			= g_sJobListMP[Get_Invite_Array_Index()].jlRootContentIdHash
	
	PRINTLN("[TS][GangOps] - Try_To_Reserve_Player_On_Joblist_Invite_Mission - Get_Invite_Array_Index: ", Get_Invite_Array_Index(),
				" , jlRootContentIdHash: ", iRootContentIdHash)
		
	IF GANGOPS_FLOW_IS_A_GANGOPS_MISSION_FROM_ROOT_ID(iRootContentIdHash)
		coronaCoords = GANGOPS_GET_GANG_OPS_BOARD_START_POS()
		PRINTLN("[TS][GangOps] - Try_To_Reserve_Player_On_Joblist_Invite_Mission - GangOps mission, clobbering location - coronaCoords = ", coronaCoords)
	ENDIF
	
	#IF FEATURE_CASINO_HEIST
	IF IS_THIS_MISSION_A_CASINO_HEIST_MISSION(iRootContentIdHash)
		coronaCoords = GET_CASINO_HEIST_BOARD_START_POS()
		PRINTLN("[TS][CASINO_HEIST] - Try_To_Reserve_Player_On_Joblist_Invite_Mission - Casino Heist mission, clobbering location - coronaCoords = ", coronaCoords)
	ENDIF
	#ENDIF
	
	#IF FEATURE_HEIST_ISLAND
	IF IS_THIS_MISSION_A_HEIST_ISLAND_MISSION(iRootContentIdHash)
		coronaCoords = GET_HEIST_ISLAND_BOARD_START_POS()
		PRINTLN("[TS][CASINO_HEIST] - Try_To_Reserve_Player_On_Joblist_Invite_Mission - Island Heist mission, clobbering location - coronaCoords = ", coronaCoords)
	ENDIF
	#ENDIF
	
	IF IS_THIS_MISSION_A_TUNER_ROBBERY_FINALE(iRootContentIdHash)
		coronaCoords = GET_TUNER_BOARD_START_POS()
		PRINTLN("[TS][TUNER] - Try_To_Reserve_Player_On_Joblist_Invite_Mission - Tuner Robbery mission, clobbering location - coronaCoords = ", coronaCoords)
	ENDIF
	
	IF IS_THIS_ROOT_CONTENT_ID_A_ULP_MISSION(iRootContentIdHash)
		coronaCoords = GET_ULP_MISSION_LOCATION()
		PRINTLN("[TS][SUM22] - Try_To_Reserve_Player_On_Joblist_Invite_Mission - ULP mission, clobbering location - coronaCoords = ", coronaCoords)
	ENDIF
	
	#IF FEATURE_DLC_2_2022
	IF IS_THIS_ROOT_CONTENT_ID_A_XMAS22_STORY_MISSION(iRootContentIdHash)
		coronaCoords = GET_XMAS22_STORY_START_POS()
		PRINTLN("[TS][SUM22] - Try_To_Reserve_Player_On_Joblist_Invite_Mission - Xmas22 Story mission, clobbering location - coronaCoords = ", coronaCoords)
	ENDIF
	#ENDIF
	
    BOOL            invitorIsPlayer             = IS_BIT_SET(g_sJobListMP[Get_Invite_Array_Index()].jlBitfield, JOBLIST_BITFLAG_INVITOR_IS_PLAYER)
	PLAYER_INDEX	theInvitingPlayer			= INVALID_PLAYER_INDEX()
	INT				theInvitingPlayerAsInt		= -1
	BOOL			hasCutscene					= FALSE

    // Set up the details for the mission
    MP_MISSION_DATA theMissionData
    // KGM 27/1/13: MissionID INT is the FM Mission TypeID - to be sorted properly when Joblist better interacts with Mission Controller data
    theMissionData.mdID.idMission           = Convert_FM_Mission_Type_To_MissionID(g_sJobListMP[Get_Invite_Array_Index()].jlMissionAsInt)
    // KGM 27/1/13: CreatorID is still being forced into the Instance field - to be sorted out properly
    theMissionData.mdID.idCreator           = g_sJobListMP[Get_Invite_Array_Index()].jlInstance
	
	// KGM 22/10/13 - Removed the clause that dealt with NULL contentIDs - everything should now have a contentID including Minigames (fake contentIDs)
    #IF IS_DEBUG_BUILD
        NET_PRINT("...KGM MP [JobList][App]: Setting up Reserving Player on Mission. ContentID stored as: ") NET_PRINT(g_sJobListMP[Get_Invite_Array_Index()].jlContentID) NET_NL()
	#ENDIF
	
	theMissionData.mdID.idCloudFilename		= g_sJobListMP[Get_Invite_Array_Index()].jlContentID
    theMissionData.mdID.idVariation         = Get_MissionsAtCoords_Variation_By_Filename(theMissionData.mdID.idCloudFilename)
    hasCutscene         					= Get_MissionsAtCoords_HasCutscene_By_Filename(theMissionData.mdID.idCloudFilename)
    
    // WORKAROUND (which I may keep) - if the invite is from a player then make the source an 'invite' so that a new instance of the mission cannot be started if the 
    //      previous instance is full or no longer exists. If the invite is from an in-game character then make the source a Flow BLIP so that a new instance can be started
    g_eMPMissionSource  theRequestSource    = MP_MISSION_SOURCE_INVITE
    IF NOT (invitorIsPlayer)
        theRequestSource = MP_MISSION_SOURCE_MISSION_FLOW_BLIP
    ENDIF
	
	// If the invitor is a player, then pass this to the reservation routine so that this player joins only the correct mission instance if there are multiple reserved instances
	IF (invitorIsPlayer)
		theInvitingPlayer = INT_TO_PLAYERINDEX(g_sJobListMP[Get_Invite_Array_Index()].jlUniqueID)
		
		IF (IS_NET_PLAYER_OK(theInvitingPlayer, FALSE))
			theInvitingPlayerAsInt	= NATIVE_TO_INT(theInvitingPlayer)
		ELSE
			theInvitingPlayer		= INVALID_PLAYER_INDEX()
			theInvitingPlayerAsInt	= -1
		ENDIF
	ENDIF
    
    // Broadcast the mission request to the Mission Controller, store the return value which is a unique ID for the request
    reservationRequestUniqueID = Broadcast_Reserve_Mission_By_Player(theRequestSource, theMissionData, hasCutscene, DEFAULT, DEFAULT, DEFAULT, theInvitingPlayerAsInt)
    
    // Although unlikely, it is possible that the request can return 'NO_UNIQUE_ID' so this needs to be checked
    IF (reservationRequestUniqueID = NO_UNIQUE_ID)
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList][App]: NO_UNIQUE_ID returned when Reserving Invite Mission. Don't allow player to accept invite") NET_NL()
            NET_NL()
            
            SCRIPT_ASSERT("Try_To_Reserve_Player_On_Joblist_Invite_Mission(): ERROR: Attempting to Reserve Mission, but NO_UNIQUE_ID returned. Invite Not Accepted. Tell Keith.")
        #ENDIF
		
		// Restore and Refund the Wanted level if the player paid to clear it
		Restore_And_Refund_MissionsAtCoords_Cleared_Wanted_Level()
        
        // Don't allow the player to accept the invite
        RETURN FALSE
    ENDIF
    
    // Store the Reservation Request UniqueID so it can be monitored by the header script
    IF (invitorIsPlayer)
        IF (IS_NET_PLAYER_OK(theInvitingPlayer, FALSE))
            // The invitor is a player, so store that player's current Apartment ID
            // This will be used to let a player that warps into an apartment know which apartment they are in
            invitePlayerAptData.ipInviteApartment   = GlobalplayerBD_FM[NATIVE_TO_INT(theInvitingPlayer)].propertyDetails.iCurrentlyInsideProperty
            NETWORK_HANDLE_FROM_PLAYER(theInvitingPlayer,invitePlayerAptData.ipInvitingPlayer,SIZE_OF(invitePlayerAptData.ipInvitingPlayer))
            
            #IF IS_DEBUG_BUILD
                NET_PRINT("...KGM MP [JobList][App]: Storing Apartment Player is being invited into as: ")
                NET_PRINT_INT(invitePlayerAptData.ipInviteApartment)
                NET_NL()
                NET_PRINT("...KGM MP [JobList][App]: Storing Gamer Hanlde of Player whose invite is being accepted    : ")
                NET_PRINT(GET_PLAYER_NAME(theInvitingPlayer))
                NET_NL()
            #ENDIF
        ENDIF
    ENDIF
    
    // If the invitor is a character, store details so that the Contact Mission routines can check for invite acceptance
    IF NOT (invitorIsPlayer)
        g_sCMInviteAccept.ciInvitingContact     = INT_TO_ENUM(enumCharacterList, g_sJobListMP[Get_Invite_Array_Index()].jlUniqueID)
        g_sCMInviteAccept.ciCloudFilename       = theMissionData.mdID.idCloudFilename
        g_sCMInviteAccept.ciReservedForJob      = FALSE
            
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList][App]: Storing NPC Details for Accepted Invite (for CMs): ")
            TEXT_LABEL  npcNameTL = Get_NPC_Name(g_sCMInviteAccept.ciInvitingContact)
            NET_PRINT(GET_STRING_FROM_TEXT_FILE(npcNameTL))
            NET_PRINT(" (Filename: ")
            NET_PRINT(g_sCMInviteAccept.ciCloudFilename)
            NET_PRINT(") [AWAITING PLAYER RESERVATION]")
            NET_NL()
        #ENDIF
    ELSE
        g_sCMInviteAccept.ciInvitingContact     = NO_CHARACTER
        g_sCMInviteAccept.ciCloudFilename       = ""
        g_sCMInviteAccept.ciReservedForJob      = FALSE
            
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList][App]: Clearing NPC Accepted Invite Details (for CMs). Invite is not from an NPC.")
            NET_NL()
        #ENDIF
    ENDIF
    
    INT             fmType      = g_sJobListMP[Get_Invite_Array_Index()].jlMissionAsInt
    INT             theSubtype	= g_sJobListMP[Get_Invite_Array_Index()].jlSubtype
    
    TEXT_LABEL_63   missionName = ""
    IF NOT (IS_STRING_NULL_OR_EMPTY(g_sJobListMP[Get_Invite_Array_Index()].jlMissionName))
        missionName = g_sJobListMP[Get_Invite_Array_Index()].jlMissionName
    ENDIF
    
    Setup_Invite_Being_Accepted(reservationRequestUniqueID, coronaCoords, fmType, theSubtype, missionName, paramFeedID, theMissionData.mdID.idCloudFilename, invitePlayerAptData.ipInviteApartment, theInvitingPlayer, iRootContentIdHash)
	
	// KGM 15/6/14: Same-Session invites into a Heist Corona needs to store some details used by AM_MP_PROPERTY_INT script to place the player in the correct apartment
	Clear_Same_Session_Invite_Heist_Apartment_Details()
	IF (invitorIsPlayer)
		IF (fmType = FMMC_TYPE_MISSION)
			IF (g_sJobListMP[Get_Invite_Array_Index()].jlSubtype = FMMC_MISSION_TYPE_HEIST)
			OR (g_sJobListMP[Get_Invite_Array_Index()].jlSubtype = FMMC_MISSION_TYPE_PLANNING)
        		IF (IS_NET_PLAYER_OK(theInvitingPlayer, FALSE))
					// FEATURE_HEIST_PLANNING: |TEMP: Fill the owner handle with the Gamer_Handle of the inviting player to ensure it contains valid data when not running Heists
					g_sJoblistHeistApartmentDetails.ownerHandle = GET_GAMER_HANDLE_PLAYER(theInvitingPlayer)
					
					GET_GAMER_HANDLE_OF_PROPERTY_OWNER(theInvitingPlayer, g_sJoblistHeistApartmentDetails.ownerHandle)
					g_sJoblistHeistApartmentDetails.iPackedInt = g_sJobListMP[Get_Invite_Array_Index()].jlHeistPackedInt
						// FEATURE_HEIST_PLANNING
					
					#IF IS_DEBUG_BUILD
						PRINTLN(".KGM [Joblist][App][Heist]: Accepting invite into Heist-Related corona. Storing Joblist Heist Apartment Details for AM_MP_PROPERTY_INT init:")
						PRINTLN(".KGM [Joblist][App][Heist]: ...PackedInt: ", g_sJoblistHeistApartmentDetails.iPackedInt, "   Inviting PlayerID: ", GET_PLAYER_NAME(theInvitingPlayer))
						
						GAMER_HANDLE invitingPlayerGH = GET_GAMER_HANDLE_PLAYER(theInvitingPlayer)
						PRINTLN(".KGM [Joblist][App][Heist]: ...Gamer Handle of Inviting Player (see logs):")
						DEBUG_PRINT_GAMER_HANDLE(invitingPlayerGH)
						PRINTLN(".KGM [Joblist][App][Heist]: ...Gamer Handle of Owner of the Apartment the Inviting Player is currently in (see logs):")
						DEBUG_PRINT_GAMER_HANDLE(g_sJoblistHeistApartmentDetails.ownerHandle)
						IF (NETWORK_ARE_HANDLES_THE_SAME(invitingPlayerGH, g_sJoblistHeistApartmentDetails.ownerHandle))
							PRINTLN(".KGM [Joblist][App][Heist]: ...Inviting Player is the Apartment Owner.")
						ELSE
							PRINTLN(".KGM [Joblist][App][Heist]: ...Inviting Player is NOT the Apartment Owner.")
							IF (NETWORK_IS_GAMER_IN_MY_SESSION(g_sJoblistHeistApartmentDetails.ownerHandle))
								PRINTLN(".KGM [Joblist][App][Heist]: ......Apartment Owner is in this session: ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_FROM_GAMER_HANDLE(g_sJoblistHeistApartmentDetails.ownerHandle)))
							ELSE
								PRINTLN(".KGM [Joblist][App][Heist]: ......and Apartment Owner is NOT in this session.")
							ENDIF
						ENDIF
					#ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//Check if the broadcast data sync has occured such that we can identify the owner of the property to enter
	IF invitorIsPlayer
		IF GANGOPS_FLOW_IS_A_GANGOPS_MISSION_FROM_ROOT_ID(iRootContentIdHash)
		OR IS_THIS_MISSION_A_CASINO_HEIST_MISSION(iRootContentIdHash)
		OR IS_THIS_MISSION_A_HEIST_ISLAND_MISSION(iRootContentIdHash)
		#IF FEATURE_TUNER
		OR IS_THIS_MISSION_A_TUNER_ROBBERY_FINALE(iRootContentIdHash)
		#ENDIF
			IF NOT CHECK_MP_SMPL_INT_HEIST_INTERIOR_CAN_LAUNCH(theInvitingPlayer)
				PRINTLN("[TS] [Joblist][App][Heist] - Try_To_Reserve_Player_On_Joblist_Invite_Mission - Not able to identify the property owner")
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	IF invitorIsPlayer
	AND GANGOPS_FLOW_IS_A_GANGOPS_MISSION_FROM_ROOT_ID(iRootContentIdHash)
		SETUP_MP_SMPL_INT_HEIST_INTERIOR_TO_LAUNCH(theInvitingPlayer, SIMPLE_INTERIOR_TYPE_DEFUNCT_BASE, iRootContentIdHash)
	ELSE
		PRINTLN("[TS][GangOps] [Joblist][App][Heist] - Try_To_Reserve_Player_On_Joblist_Invite_Mission - Not a valid gang ops heist invite")
	ENDIF
	
	#IF FEATURE_CASINO_HEIST
	IF invitorIsPlayer
	AND IS_THIS_MISSION_A_CASINO_HEIST_MISSION(iRootContentIdHash)
		SETUP_MP_SMPL_INT_HEIST_INTERIOR_TO_LAUNCH(theInvitingPlayer, SIMPLE_INTERIOR_TYPE_ARCADE, iRootContentIdHash)
	ELSE
		PRINTLN("[TS][CASINO_HEIST] [Joblist][App][Heist] - Try_To_Reserve_Player_On_Joblist_Invite_Mission - Not a valid casino heist invite")
	ENDIF
	#ENDIF
	
	#IF FEATURE_HEIST_ISLAND
	IF invitorIsPlayer
	AND IS_THIS_MISSION_A_HEIST_ISLAND_MISSION(iRootContentIdHash)
		SETUP_MP_SMPL_INT_HEIST_INTERIOR_TO_LAUNCH(theInvitingPlayer, SIMPLE_INTERIOR_TYPE_SUBMARINE, iRootContentIdHash)
	ELSE
		PRINTLN("[TS][HEIST_ISLAND] [Joblist][App][Heist] - Try_To_Reserve_Player_On_Joblist_Invite_Mission - Not a valid island heist invite")
	ENDIF
	#ENDIF
	
	IF invitorIsPlayer
	AND IS_THIS_MISSION_A_TUNER_ROBBERY_FINALE(iRootContentIdHash)
		SETUP_MP_SMPL_INT_HEIST_INTERIOR_TO_LAUNCH(theInvitingPlayer, SIMPLE_INTERIOR_TYPE_AUTO_SHOP, iRootContentIdHash)
	ELSE
		PRINTLN("[TS][TUNER] [Joblist][App][Heist] - Try_To_Reserve_Player_On_Joblist_Invite_Mission - Not a valid tuner invite")
	ENDIF
	
	IF invitorIsPlayer
	AND GET_FIXER_SHORT_TRIP_FROM_ROOT_CONTENT_ID(iRootContentIdHash) = FST_SHORT_TRIP_1
		IF IS_PLAYER_LAUNCHING_FIXER_SHORT_TRIP_WITH_NO_LOBBY(theInvitingPlayer)
		
			g_bHideFrontendForLobby = TRUE
			PRINTLN("[TS][FIXER] [Joblist] - Try_To_Reserve_Player_On_Joblist_Invite_Mission - Player is launching short trip with no lobby, g_bHideFrontendForLobby = TRUE")
			
			IF NOT IS_PLAYER_LAUNCHING_FIXER_SHORT_TRIP_IN_SMOKING_ROOM(theInvitingPlayer)
				g_bForceCustomUIForLobby = TRUE
				PRINTLN("[TS][FIXER] [Joblist] - Try_To_Reserve_Player_On_Joblist_Invite_Mission - Invite is for fixer short trip 1, g_bForceCustomUIForLobby = TRUE")
			ENDIF
			
		ELSE
			PRINTLN("[TS][FIXER] [Joblist] - Try_To_Reserve_Player_On_Joblist_Invite_Mission - Invite for fixer short trip but player is not launching with no lobby")
		ENDIF
	ELSE
		PRINTLN("[TS][FIXER] [Joblist] - Try_To_Reserve_Player_On_Joblist_Invite_Mission - Not an invite for fixer short trip 1")
		PRINTLN("[TS][FIXER] [Joblist] - Try_To_Reserve_Player_On_Joblist_Invite_Mission - iRootContentIdHash = ", iRootContentIdHash)
		PRINTLN("[TS][FIXER] [Joblist] - Try_To_Reserve_Player_On_Joblist_Invite_Mission - GET_FIXER_SHORT_TRIP_ROOT_CONTENT_ID_HASH(FST_SHORT_TRIP_1) = ", GET_FIXER_SHORT_TRIP_ROOT_CONTENT_ID_HASH(FST_SHORT_TRIP_1))
	ENDIF
    
    RETURN TRUE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Tries to warp the player after Invite Acceptance (including after the Wanted Level Clear screen)
PROC Accepted_Invitation_So_Attempt_To_Warp(INT paramInvitationSlot)

	// Lose the Wanted Level (the player will already have paid for this and received all necessary prompts)
	IF NOT (IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, FALSE))
        // ...player isn't OK
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList][App]: Accepted_Invitation_So_Attempt_To_Warp() - FAILED: Player is not OK. Ignoring. Returning to JobList screen") NET_NL()
        #ENDIF
        
        Display_JobList_Screen()
        EXIT
	ENDIF
	
	IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
		Store_MissionsAtCoords_Cleared_Wanted_Level(GET_CONTACT_REQUEST_COST(REQUEST_REMOVE_WANTED))
		
		// KGM 13/8/15 [BUG 2402812]: James will clear the wanted level after FadeOut and Task Clear for Same Session Contact Mission acceptance while in a vehicle
		IF (g_sJLInvitesMP[paramInvitationSlot].jliType = FMMC_TYPE_MISSION)
		AND (g_sJLInvitesMP[paramInvitationSlot].jliSubtype = FMMC_MISSION_TYPE_CONTACT)
		AND (IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()))
			#IF IS_DEBUG_BUILD
				PRINTLN(".KGM [JobList][App]: Accepted_Invitation_So_Attempt_To_Warp() - DELAY WANTED LEVEL CLEAR (Same Session Invite to Contact Mission accepted while in a vehicle)")
			#ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
				PRINTLN(".KGM [JobList][App]: Accepted_Invitation_So_Attempt_To_Warp() - WANTED LEVEL CLEARED")
			#ENDIF
			
			CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
		ENDIF
		
		MPGlobalsAmbience.bBlockWantedLevelRP = TRUE
		
		// KGM 21/2/15 [BUG 2238981]: Let MatC know the player lost a Wanted Level
		MissionsAtCoords_Player_Paid_To_Remove_Wanted_Level()
	ELSE
		#IF IS_DEBUG_BUILD
			PRINTLN(".KGM [JobList][App]: Accepted_Invitation_So_Attempt_To_Warp() - WANTED LEVEL WAS ALREADY CLEAR")
		#ENDIF
	ENDIF
	
	// Do a final check for the player being alive before accepting the invite
	IF NOT (IS_PLAYER_PLAYING(PLAYER_ID()))
        // ...player is dead, so don;t allow invite acceptance, return to the Joblist screen
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList][App]: Accepted_Invitation_So_Attempt_To_Warp() - Player is dead. Ignoring. Returning to JobList screen") NET_NL()
        #ENDIF
        
        Display_JobList_Screen()
        EXIT
	ENDIF

    // Get the FeedID for the invite being accepted
    INT theFeedID = g_sJLInvitesMP[paramInvitationSlot].jliFeedID
    
    // Try to reserve the players place on the activity before committing to the fade out and warp
    IF NOT (Try_To_Reserve_Player_On_Joblist_Invite_Mission(theFeedID))
        // ...immediately failed to reserve place on invite mission, so return to the Joblist screen
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList][App]: FAILED trying to reserve place on Invite Mission. Ignoring. Returning to JobList screen") NET_NL()
        #ENDIF
        
        Display_JobList_Screen()
        EXIT
    ENDIF
	
	// Set up timeout to a 'please wait' screen if the confirmation appears to be delayed due to a Host Migration
	m_allowSwitchToPleaseWait	= TRUE
	m_switchToPleaseWaitTimeout	= GET_GAME_TIMER() + SWITCH_TO_PLEASE_WAIT_TIMEOUT_msec
	
	// Need to store the method that this mission is being started - I can do this here and it'll be used when the mission is launched or overwritten if it fails, as appropriate
    BOOL    invitorIsPlayer = IS_BIT_SET(g_sJobListMP[Get_Invite_Array_Index()].jlBitfield, JOBLIST_BITFLAG_INVITOR_IS_PLAYER)
    
    IF (invitorIsPlayer)
        // Invitation came from a same-session player
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList][App]: Setting Job Entry Type as same-session invite from player (ciMISSION_ENTERY_TYPE_INVITE_FROM_MP)") NET_NL()
        #ENDIF
		
		SET_FM_JOB_ENTERY_TYPE(ciMISSION_ENTERY_TYPE_INVITE_FROM_MP)
    ELSE
		// Invitation came from an NPC
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList][App]: Setting Job Entry Type as NPC invite (ciMISSION_ENTERY_TYPE_NPC_INVITE)") NET_NL()
        #ENDIF
		
		SET_FM_JOB_ENTERY_TYPE(ciMISSION_ENTERY_TYPE_NPC_INVITE)
    ENDIF
	
	IF IS_THIS_MISSION_CONTENT_ID_HASH_A_GANG_ATTACK(g_FMMC_STRUCT.iRootContentIDHash)
		PRINTLN("[TS] * Accepted a phone invite -  g_FMMC_STRUCT.iRootContentIDHash = 0")
		g_FMMC_STRUCT.iRootContentIDHash = 0
    ENDIF
	
	PRINTLN("[TS] * Accepted a phone invite - ", g_sJLInvitesMP[paramInvitationSlot].jliMissionName, ", ", g_sJLInvitesMP[paramInvitationSlot].jliContentID, " - T - ", GET_CLOUD_TIME_AS_INT(), ", F - ", GET_FRAME_COUNT(), " *")
	//Set to clean up on call if needed
	SET_TRANSITION_SESSIONS_CLEAN_UP_ON_CALL_NOW_IF_NEEDED()
    // The reservation request has been sent, so wait for confirmation
    m_currentStage = JLS_WAITING_FOR_RESERVATION_CONFIRMATION   
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Performs navigation checks while the Invitation screen is on display
PROC Process_Stage_Invitation()
    
    // Ensure the invite is still available
    INT     		invitationSlot  	= NO_JOBLIST_ARRAY_ENTRY
    BOOL    		invitorIsPlayer 	= IS_BIT_SET(g_sJobListMP[Get_Invite_Array_Index()].jlBitfield, JOBLIST_BITFLAG_INVITOR_IS_PLAYER)
	PLAYER_INDEX	theInvitingPlayer	= INVALID_PLAYER_INDEX()
    
    IF (invitorIsPlayer)
		theInvitingPlayer = INT_TO_PLAYERINDEX(g_sJobListMP[Get_Invite_Array_Index()].jlUniqueID)
        invitationSlot  = Get_Joblist_Invitation_Slot_For_Player(theInvitingPlayer)
    ELSE
        enumCharacterList   theInvitingNPC  = INT_TO_ENUM(enumCharacterList, g_sJobListMP[Get_Invite_Array_Index()].jlUniqueID)
        TEXT_LABEL_63       theMissionName  = g_sJobListMP[Get_Invite_Array_Index()].jlMissionName
        BOOL                matchMission    = TRUE
        invitationSlot  = Get_Joblist_Invitation_Slot_For_NPC(theInvitingNPC, theMissionName, matchMission)
    ENDIF
    	
    // ...is the invitation still available?
    IF (invitationSlot = NO_JOBLIST_ARRAY_ENTRY)
        // ...there is no invite from this invitor any more, so return to the Joblist screen
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList][App]: The invitation no longer exists - returning to JobList screen") NET_NL()
        #ENDIF
        
        Display_JobList_Screen()
        EXIT
    ENDIF
    
    // ...is the invitation still allowed to be displayed?
    IF NOT (Is_Invitation_In_Slot_Allowed_To_Be_Displayed(invitationSlot))
        // ...the invitation in the slot is no longer allowed to be displayed
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList][App]: The invitation is no longer allowed to be displayed - returning to JobList screen") NET_NL()
        #ENDIF
        
        Display_JobList_Screen()
        EXIT
    ENDIF
    
	// TODO:
	IF IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(),MPAM_TYPE_CINEMA) 
		Print_Cant_Join_From_Here()
	ELSE
		Print_Pay_Wanted_Level_Help()
	ENDIF
	
	
    // Check for scrolling
    Check_For_MP_App_Navigation_Inputs(m_sAppData)
    
    // Check if the player wants to go BACK to JobList
    IF (Did_Player_Press_MP_App_Negative_Response_Button(m_sAppData))
        Display_JobList_Screen()
		IF m_cantAcceptJobHelpDisplayed = TRUE
			CLEAR_HELP()
			m_cantAcceptJobHelpDisplayed = FALSE
		ENDIF
        EXIT
    ENDIF
    
    // Check if the player wants to DELETE the current invite
    IF (Did_Player_Press_MP_App_Special_Response_Button(m_sAppData))
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList][App]: Player has chosen to delete Invitation in slot: ") NET_PRINT_INT(invitationSlot) NET_NL()
        #ENDIF
        
        IF NOT (invitorIsPlayer)
            // Deleting NPC Invite, so check if it is a Contact Mission that needs cleaned up
            // KGM 10/7/13: This is quite specific and there must be a better more generic way to do this, but time is running out
            #IF IS_DEBUG_BUILD
                NET_PRINT("      Invite is from an NPC, so check if it is a Contact Mission that needs cleaned up") NET_NL()
            #ENDIF
        
            enumCharacterList   theInvitingNPC  = INT_TO_ENUM(enumCharacterList, g_sJobListMP[Get_Invite_Array_Index()].jlUniqueID)
            TEXT_LABEL_63       theMissionName  = g_sJobListMP[Get_Invite_Array_Index()].jlMissionName
            Contact_Mission_Invite_Deleted(theInvitingNPC, theMissionName)
		ELSE
			// For Invites from Players, let the invitor know
            #IF IS_DEBUG_BUILD
                NET_PRINT("      Invite is from a player, so not attempting to delete the Contact Mission on the current mission array") NET_NL()
            #ENDIF
			
			BROADCAST_JOBLIST_INVITE_REPLY(theInvitingPlayer, FALSE, TRUE)
        ENDIF
        
        Delete_Joblist_Invitation_In_Invitation_Slot(invitationSlot)
        Display_JobList_Screen()
        EXIT
    ENDIF
    
    // Check if the player wants to WARP to the invitation
    IF NOT (Did_Player_Press_MP_App_Positive_Response_Button(m_sAppData))
        EXIT
    ENDIF
	
	IF IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_CINEMA) 
	AND g_sJobListMP[Get_Invite_Array_Index()].jlMissionAsInt = FMMC_TYPE_MISSION
	AND g_sJobListMP[Get_Invite_Array_Index()].jlSubtype = FMMC_MISSION_TYPE_CONTACT
		Display_JobList_Screen()
        EXIT
	ENDIF
	
	IF theInvitingPlayer != INVALID_PLAYER_INDEX()
		// An excluded invite should never appear in the invite list any more but just incase, this prevent the player from attempting to accept.
		// this is to avoid exploit in url:bugstar:1878909
		IF Is_Player_Excluded_From_MP_Mission( PLAYER_ID(), Get_UniqueID_For_This_Players_Mission(theInvitingPlayer))
			PRINTLN("[MMM] You are excluded from this job and cannot accept the invite ")
			
			PRINT_HELP("FMMC_EXCLUDED_INVITE")
			
			// TODO: Display Help Text saying "You are excluded from this job and cannot join."
			Display_JobList_Screen()		
			BROADCAST_JOBLIST_INVITE_REPLY(theInvitingPlayer, TRUE, FALSE)
			Delete_Joblist_Invitation_In_Invitation_Slot(invitationSlot)	// Delete the invite as the player will not be able to join
	        EXIT
		ENDIF
	ENDIF
	
    // The player does want to warp
    // ...are the details the same? (as long as the coords are the same we can assume everything else is the same too)
    CONST_FLOAT     COORDS_LEEWAY_m         0.3
    
    IF (VDIST(g_sJobListMP[Get_Invite_Array_Index()].jlCoords, g_sJLInvitesMP[invitationSlot].jliCoords) > COORDS_LEEWAY_m)
        // ...the coords are different so the invitation must be for a different mission, so return to the Joblist screen
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList][App]: The location has changed, so this must be a different invitation from the same player. Ignoring. Returning to JobList screen") NET_NL()
        #ENDIF
        
        Display_JobList_Screen()
        EXIT
    ENDIF
	
	// If there is a Head-To-Head Challenge wager involved then ensure the player has enough cash
	IF NOT (Can_I_Afford_Wager_If_Head_To_Head_Playlist(g_sJobListMP[Get_Invite_Array_Index()].jlWagerH2H))
		Display_Not_Enough_Cash_For_Wager_Help(g_sJobListMP[Get_Invite_Array_Index()].jlWagerH2H)
    
    	Display_JobList_Screen()
        
        EXIT
	ENDIF
	
	// If the player has a Wanted Level then re-direct to the 'are you sure' screen
	IF (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0)
        IF NOT (Can_I_Afford_To_Accept_Invite(g_sJobListMP[Get_Invite_Array_Index()].jlMissionAsInt, g_sJobListMP[Get_Invite_Array_Index()].jlSubtype))
			Display_Not_Enough_Cash_Help(Get_Cost_Of_Accepting_Invite(g_sJobListMP[Get_Invite_Array_Index()].jlMissionAsInt, g_sJobListMP[Get_Invite_Array_Index()].jlSubtype))
        
        	Display_JobList_Screen()
            
            EXIT
        ENDIF
	
		// KGM 22/7/15 [BUG 2437060]: If any Invite Accept warning message is on display, clear it
		Clear_Accept_Invite_Warning_Help_Text_If_On_Display()
			
		Display_Invitation_Lose_Wanted_Level_Screen()
		
		EXIT
	ENDIF
	
	//Don't allow any phone invites to process while processing leaving a simple interior.
	IF IS_LOCAL_PLAYER_WALKING_OUT_OF_SIMPLE_INTERIOR()
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList][App]: The player is being removed from a simple interior, don't process this invite yet.  Returning to JobList screen") NET_NL()
        #ENDIF
		Display_JobList_Screen()
		EXIT
	ENDIF
	
	IF GET_PAUSE_MENU_STATE() != PM_INACTIVE
		 #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList][App]: The pause menu is being actioned, don't process this invite yet.  Returning to JobList screen") NET_NL()
        #ENDIF
		Display_JobList_Screen()
		EXIT
	ENDIF
	
	// KGM 22/7/15 [BUG 2437060]: If any Invite Accept warning message is on display, clear it
	Clear_Accept_Invite_Warning_Help_Text_If_On_Display()
	
	INT iCurrentMissionArrayPos = -1
	INT iSeries = -1
	
	IF g_sJobListMP[Get_Invite_Array_Index()].jlRootContentIdHash != 0
		iCurrentMissionArrayPos = GET_ROCKSTAR_MISSION_FROM_ROOT_CONTENT_ID_HASH(g_sJobListMP[Get_Invite_Array_Index()].jlRootContentIdHash)
		iSeries = CV2_GET_ADVERSARY_SERIES_PLAYLIST_FROM_ROOT_CONTENT_ID(g_sJobListMP[Get_Invite_Array_Index()].jlRootContentIdHash)
	ENDIF
	
	IF NOT IS_BIT_SET(g_sJobListMP[Get_Invite_Array_Index()].jlBitfield, JOBLIST_BITFLAG_INVITOR_IS_PLAYER)
	AND g_sJobListMP[Get_Invite_Array_Index()].jlQuickMatch
	AND iCurrentMissionArrayPos != -1
		SET_VARS_WHEN_LAUNCHING_QUICKMATCH_JOB_FROM_PHONE(iCurrentMissionArrayPos, iSeries)
		HANG_UP_AND_PUT_AWAY_PHONE()
	ELSE
		Accepted_Invitation_So_Attempt_To_Warp(invitationSlot)
	ENDIF
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the 'Lose Wanted Level' choice has been accepted or denied
PROC Process_Stage_Confirm_Invite_Lose_Wanted_Level()
    
    // Check if the player responds NO and return to Current Job screen
    IF (Did_Player_Press_MP_App_Negative_Response_Button(m_sAppData))
        Display_Invitation_Screen()
        EXIT
    ENDIF
	
	// Alerts the player that their wanted level has been lost so they have been
	// sent back to the previous screen
	IF (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0)
		PRINT_HELP("FMMC_LOST_WANTED_LEVEL")
		Display_Invitation_Screen()
		EXIT
	ENDIF	
    
	g_inviteControlTimeout = GET_GAME_TIMER() + INVITE_ACCEPT_CONTROLS_BLOCK_TIMEOUT_msec
	
    // Check if the player responds YES, charge the player, clear wanted level, try to reserve player on the mission
    IF NOT (Did_Player_Press_MP_App_Positive_Response_Button(m_sAppData))
		EXIT
	ENDIF
		
	// Player has accepted the Invite
    // Ensure the invite is still available
    INT     invitationSlot  = NO_JOBLIST_ARRAY_ENTRY
    BOOL    invitorIsPlayer = IS_BIT_SET(g_sJobListMP[Get_Invite_Array_Index()].jlBitfield, JOBLIST_BITFLAG_INVITOR_IS_PLAYER)
    
    IF (invitorIsPlayer)
        PLAYER_INDEX theInvitingPlayer = INT_TO_PLAYERINDEX(g_sJobListMP[Get_Invite_Array_Index()].jlUniqueID)
        invitationSlot  = Get_Joblist_Invitation_Slot_For_Player(theInvitingPlayer)
    ELSE
        enumCharacterList   theInvitingNPC  = INT_TO_ENUM(enumCharacterList, g_sJobListMP[Get_Invite_Array_Index()].jlUniqueID)
        TEXT_LABEL_63       theMissionName  = g_sJobListMP[Get_Invite_Array_Index()].jlMissionName
        BOOL                matchMission    = TRUE
        invitationSlot  = Get_Joblist_Invitation_Slot_For_NPC(theInvitingNPC, theMissionName, matchMission)
    ENDIF
    
    // ...is the invitation still available?
    IF (invitationSlot = NO_JOBLIST_ARRAY_ENTRY)
        // ...there is no invite from this invitor any more, so return to the Joblist screen
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList][App]: The invitation no longer exists after choosing to Lose Wanted Level - returning to JobList screen") NET_NL()
        #ENDIF
        
        Display_JobList_Screen()
        EXIT
    ENDIF
	
	// Player has accepted the Invite
	// KGM 31/8/13: ToDo 1605311 (Pay to lose Wanted Level when accepting invites)
	// Charge the player to remove the wanted level (the joblist app will have verified cash values, etc)
	GAMER_HANDLE gamerHandle = GET_GAMER_HANDLE_PLAYER(PLAYER_ID())
	INT iRemoveWantedCost = GET_CONTACT_REQUEST_COST(REQUEST_REMOVE_WANTED)
	INT iScriptTransactionIndex
	
	// Trigger a cash transaction for PC Build (2151163)
	IF USE_SERVER_TRANSACTIONS()
		TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_SPEND_LOSE_WANTED_LEVEL, iRemoveWantedCost, iScriptTransactionIndex, FALSE, TRUE)
		g_cashTransactionData[iScriptTransactionIndex].eExtraData.gamerHandle = gamerHandle
	ELSE
		NETWORK_SPENT_BUY_WANTEDLEVEL(iRemoveWantedCost, gamerHandle, FALSE, TRUE)
	ENDIF
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [JobList][App]: Player paid $")
		NET_PRINT_INT(GET_CONTACT_REQUEST_COST(REQUEST_REMOVE_WANTED))
		NET_PRINT(" to remove wanted level (Invitation).")
		NET_NL()
	#ENDIF
	
	// Warp the player
	INT iCurrentMissionArrayPos = -1
	INT iSeries = -1
	
	IF g_sJobListMP[Get_Invite_Array_Index()].jlRootContentIdHash != 0
		iCurrentMissionArrayPos = GET_ROCKSTAR_MISSION_FROM_ROOT_CONTENT_ID_HASH(g_sJobListMP[Get_Invite_Array_Index()].jlRootContentIdHash)
		iSeries = CV2_GET_ADVERSARY_SERIES_PLAYLIST_FROM_ROOT_CONTENT_ID(g_sJobListMP[Get_Invite_Array_Index()].jlRootContentIdHash)
	ENDIF
	
	IF NOT IS_BIT_SET(g_sJobListMP[Get_Invite_Array_Index()].jlBitfield, JOBLIST_BITFLAG_INVITOR_IS_PLAYER)
	AND g_sJobListMP[Get_Invite_Array_Index()].jlQuickMatch
	AND iCurrentMissionArrayPos != -1
		SET_VARS_WHEN_LAUNCHING_QUICKMATCH_JOB_FROM_PHONE(iCurrentMissionArrayPos, iSeries)
		HANG_UP_AND_PUT_AWAY_PHONE()
	ELSE
		Accepted_Invitation_So_Attempt_To_Warp(invitationSlot)
	ENDIF
    
ENDPROC

// -----------------------------------------------------------------------------------------------------------

// Accepted a Cross-Session Invite so aceepte the code command
PROC Accepted_CSInvite_So_Change_Session(INT paramInvitationSlot)

    // The player does want to accept, so start a cross-session invite acceptance (this will display an alert so no confirmation needed here)
    INT inviteIndex = NETWORK_GET_PRESENCE_INVITE_INDEX_BY_ID(g_sJobListMP[Get_Invite_Array_Index()].jlUniqueID)
    
    
    #IF IS_DEBUG_BUILD
        NET_PRINT("...KGM MP [JobList][App]: IS_INVITED_TO_SCTV = ")NET_PRINT_BOOL(IS_INVITED_TO_SCTV()) NET_NL()
    #ENDIF
    
    IF IS_INVITED_TO_SCTV()
        
        
        g_PresenceInviteUniqueId = NETWORK_GET_PRESENCE_INVITE_INDEX_BY_ID(g_sJobListMP[Get_Invite_Array_Index()].jlUniqueID)
        #IF IS_DEBUG_BUILD
            NET_PRINT("         - skipping NETWORK_ACCEPT_PRESENCE_INVITE, go straight to maintransition ") NET_NL()
            NET_PRINT("         - g_PresenceInviteUniqueId =  ")NET_PRINT_INT(g_PresenceInviteUniqueId) NET_NL()
        #ENDIF
	
		// KGM 31/8/13: BUG 1605311 (Pay to lose Wanted Level when accepting invites)
		IF (NETWORK_IS_GAME_IN_PROGRESS())
			IF (IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, FALSE))
				IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)		
					CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
					MPGlobalsAmbience.bBlockWantedLevelRP = TRUE
					// KGM 21/2/15 [BUG 2238981]: Let MatC know the player lost a Wanted Level
					// KGM 12/3/15 [BUG 2271928]: TRUE because Cross-Session invite
					MissionsAtCoords_Player_Paid_To_Remove_Wanted_Level(TRUE)
				ENDIF
			
				#IF IS_DEBUG_BUILD
					NET_PRINT("         - WANTED LEVEL CLEARED") NET_NL()
				#ENDIF
			ENDIF
		ENDIF
        
        SET_CONFIRM_PRESENCE_INVITE_INTO_GAME_STATE(TRUE)
        SET_CONFIRM_INVITE_INTO_GAME_STATE(TRUE)
        SET_JOINING_GAMEMODE(GAMEMODE_FM)       

    ELSE
    
        IF (inviteIndex >= 0)
            NETWORK_ACCEPT_PRESENCE_INVITE(inviteIndex)
            
            #IF IS_DEBUG_BUILD
                NET_PRINT("...KGM MP [JobList][App]: The Cross-Session Invite has been accepted. Invite Index: ")
                NET_PRINT_INT(inviteIndex)
                NET_NL()
            #ENDIF						
			
			#IF FEATURE_COPS_N_CROOKS
			IF IS_THIS_CLOUD_FILE_NAME_ARCADE_MODE(g_sJobListMP[Get_Invite_Array_Index()].jlContentID)
				PRINTLN("...KGM MP [JobList][App]: Accepted_CSInvite_So_Change_Session - Accepted an invite to arcade mode, calling TRANSITION_TO_ARCADE_MODE(FALSE)")
				TRANSITION_TO_ARCADE_MODE(FALSE, TRUE, ARCADE_ENTRY_POINT_INVITE)
			ENDIF
			#ENDIF
			
			// KGM 31/8/13: BUG 1605311 (Pay to lose Wanted Level when accepting invites)
			IF (NETWORK_IS_GAME_IN_PROGRESS())
				IF (IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, FALSE))
					IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)		
						CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
						MPGlobalsAmbience.bBlockWantedLevelRP = TRUE
						
						// KGM 21/2/15 [BUG 2238981]: Let MatC know the player lost a Wanted Level
						// KGM 12/3/15 [BUG 2271928]: TRUE because Cross-Session invite
						MissionsAtCoords_Player_Paid_To_Remove_Wanted_Level(TRUE)
					ENDIF
				
					#IF IS_DEBUG_BUILD
						NET_PRINT("         - WANTED LEVEL CLEARED") NET_NL()
					#ENDIF
				ENDIF
            
	            // Broadcast to any other players in the car that the player has left
	            #IF IS_DEBUG_BUILD
	                NET_PRINT("...KGM MP [JobList]: About to broadcast Player Warped Because Of CSInvite (If in car): ")
	                NET_PRINT(" [MissionName: ") NET_PRINT(g_sJLCSInvitesMP[paramInvitationSlot].jcsiMissionName) NET_PRINT("]")
	                NET_PRINT(" [FM Type: ") NET_PRINT_INT(g_sJLCSInvitesMP[paramInvitationSlot].jcsiType) NET_PRINT("]")
	                NET_NL()
	            #ENDIF
	            
	            BROADCAST_EVENT_PLAYER_WARPED_BECAUSE_OF_INVITE(g_sJLCSInvitesMP[paramInvitationSlot].jcsiType, g_sJLCSInvitesMP[paramInvitationSlot].jcsiMissionName, g_sJLCSInvitesMP[paramInvitationSlot].jcsiSubtype)
				
				// KGM 17/10/14 [BUG 2086895]: There is a delay after accepting a Cross-session invite before IS_ANY_TRANSITION_SESSION_INVITE_BEING_ACTIONED() becomes TRUE
				// Added a timer to cover this delay so that scripts can check against it if necessary
				Set_Cross_Session_Invite_Just_Accepted_Timeout()
  			ENDIF
      ELSE
            #IF IS_DEBUG_BUILD
                NET_PRINT("...KGM MP [JobList][App]: IGNORING CROSS-SESSION INVITE ACCEPTANCE: The Invite Index is now illegal. Invite must have been removed.")
                NET_PRINT_INT(inviteIndex)
                NET_NL()
            #ENDIF
        ENDIF
    ENDIF
    
    // Delete the Invite if still on-screen
    IF (g_sJLCSInvitesMP[paramInvitationSlot].jcsiFeedID != NO_JOBLIST_INVITE_FEED_ID)
        THEFEED_REMOVE_ITEM(g_sJLCSInvitesMP[paramInvitationSlot].jcsiFeedID)
		Joblist_Remove_All_Invite_Feed_Messages()
    ENDIF
    
    Delete_Joblist_CSInvite_In_Invitation_Slot(paramInvitationSlot)

//	MPGlobalsAmbience.bJustAcceptedCSInvite_ForGarageScript = TRUE // fix for 1860268 - let the garage know that we've just accepted an invite.
//	PRINTLN("MPGlobalsAmbience.bJustAcceptedCSInvite_ForGarageScript set to TRUE")
// using Set_Cross_Session_Invite_Just_Accepted_Timeout instead

    // Hangup and put away the phone and quit the app
    HANG_UP_AND_PUT_AWAY_PHONE()
    
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Performs navigation checks while the Cross-Session Invite screen is on display
PROC Process_Stage_CSInvite()
    
    // Ensure the invite is still available
    TEXT_LABEL_63   theInvitor      = g_sJobListMP[Get_Invite_Array_Index()].jlInvitorName
    INT             invitationSlot  = Get_Joblist_CSInvite_Slot_For_InvitorDisplayName(theInvitor)
    
    // ...is the invitation still available?
    IF (invitationSlot = NO_JOBLIST_ARRAY_ENTRY)
        // ...there is no invite from this invitor any more, so return to the Joblist screen
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList][App]: The Cross-Session Invite no longer exists - returning to JobList screen") NET_NL()
        #ENDIF
        
        Display_JobList_Screen()
        EXIT
    ENDIF
	
    // ...is the invitation still allowed to be displayed?
    IF NOT (Is_CSInvite_In_Slot_Allowed_To_Be_Displayed(invitationSlot))
        // ...the invitation in the slot is no longer allowed to be displayed
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList][App]: The Cross-Session Invite is no longer allowed to be displayed - returning to JobList screen") NET_NL()
        #ENDIF
        
        Display_JobList_Screen()
        EXIT
    ENDIF
    
	Print_Pay_Wanted_Level_Help()
	
    // Check for scrolling
    Check_For_MP_App_Navigation_Inputs(m_sAppData)
    
    // Check if the player wants to go BACK to JobList
    IF (Did_Player_Press_MP_App_Negative_Response_Button(m_sAppData))
        Display_JobList_Screen()
        EXIT
    ENDIF
    
    // Check if the player wants to DELETE the current invite
    IF (Did_Player_Press_MP_App_Special_Response_Button(m_sAppData))
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList][App]: Player has chosen to delete Cross-Session Invite in slot: ") NET_PRINT_INT(invitationSlot) NET_NL()
        #ENDIF
        
        Delete_Joblist_CSInvite_In_Invitation_Slot(invitationSlot)
        Display_JobList_Screen()
        EXIT
    ENDIF
    
	g_inviteControlTimeout = GET_GAME_TIMER() + INVITE_ACCEPT_CONTROLS_BLOCK_TIMEOUT_msec
	
    // Check if the player wants to accept the invitation
    IF NOT (Did_Player_Press_MP_App_Positive_Response_Button(m_sAppData))
        EXIT
    ENDIF
	
	// If there is a Head-To-Head Challenge wager involved then ensure the player has enough cash
	IF NOT (Can_I_Afford_Wager_If_Head_To_Head_Playlist(g_sJobListMP[Get_Invite_Array_Index()].jlWagerH2H))
		Display_Not_Enough_Cash_For_Wager_Help(g_sJobListMP[Get_Invite_Array_Index()].jlWagerH2H)
    
    	Display_JobList_Screen()
        
        EXIT
	ENDIF
	
	// If the player has a Wanted Level then re-direct to the 'are you sure' screen, or back to the Joblist if they can't affprd to lose the wanted level
	IF (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0)
		IF (NETWORK_IS_GAME_IN_PROGRESS())
	        IF NOT (Can_I_Afford_To_Accept_Invite(g_sJobListMP[Get_Invite_Array_Index()].jlMissionAsInt, g_sJobListMP[Get_Invite_Array_Index()].jlSubtype))
				Display_Not_Enough_Cash_Help(Get_Cost_Of_Accepting_Invite(g_sJobListMP[Get_Invite_Array_Index()].jlMissionAsInt, g_sJobListMP[Get_Invite_Array_Index()].jlSubtype))
	        
	        	Display_JobList_Screen()
	            
	            EXIT
	        ENDIF
				
			// KGM 22/7/15 [BUG 2437060]: If any Invite Accept warning message is on display, clear it
			Clear_Accept_Invite_Warning_Help_Text_If_On_Display()
	
			Display_CSInvite_Lose_Wanted_Level_Screen()
			
			EXIT
		ENDIF
	ENDIF
	
	// KGM 22/7/15 [BUG 2437060]: If any Invite Accept warning message is on display, clear it
	Clear_Accept_Invite_Warning_Help_Text_If_On_Display()

	Accepted_CSInvite_So_Change_Session(invitationSlot)
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the CSInvite 'Lose Wanted Level' choice has been accepted or denied
PROC Process_Stage_Confirm_CSInvite_Lose_Wanted_Level()
    
    // Check if the player responds NO and return to Current Job screen
    IF (Did_Player_Press_MP_App_Negative_Response_Button(m_sAppData))
        Display_CSInvite_Screen()
        EXIT
    ENDIF
	
	// Alerts the player that their wanted level has been lost so they have been
	// sent back to the previous screen
	IF (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0)
		PRINT_HELP("FMMC_LOST_WANTED_LEVEL")
		Display_CSInvite_Screen()
		EXIT
	ENDIF
    	
    // Check if the player responds YES, charge the player, clear wanted level, try to reserve player on the mission
    IF NOT (Did_Player_Press_MP_App_Positive_Response_Button(m_sAppData))
		EXIT
	ENDIF
	
	// Player has accepted the Invite
	// Charge the player to remove the wanted level
	GAMER_HANDLE gamerHandle = GET_GAMER_HANDLE_PLAYER(PLAYER_ID())
	INT iRemoveWantedCost = GET_CONTACT_REQUEST_COST(REQUEST_REMOVE_WANTED)
	INT iScriptTransactionIndex
	
	IF USE_SERVER_TRANSACTIONS()
		TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_SPEND_LOSE_WANTED_LEVEL, iRemoveWantedCost, iScriptTransactionIndex, FALSE, TRUE)
		g_cashTransactionData[iScriptTransactionIndex].eExtraData.gamerHandle = gamerHandle
	ELSE
		NETWORK_SPENT_BUY_WANTEDLEVEL(iRemoveWantedCost, gamerHandle, FALSE, TRUE)
	ENDIF
	
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [JobList][App]: Player paid $")
		NET_PRINT_INT(GET_CONTACT_REQUEST_COST(REQUEST_REMOVE_WANTED))
		NET_PRINT(" to remove wanted level (Cross-Session Invite).")
		NET_NL()
	#ENDIF

	// Accept the Cross-Session Invite
    TEXT_LABEL_63   theInvitor      = g_sJobListMP[Get_Invite_Array_Index()].jlInvitorName
    INT             invitationSlot  = Get_Joblist_CSInvite_Slot_For_InvitorDisplayName(theInvitor)
	
	Accepted_CSInvite_So_Change_Session(invitationSlot)
    
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Performs navigation checks while the Basic Invite screen is on display
PROC Process_Stage_RVInvite()
    
    // Ensure the invite is still available
    INT     invitationSlot  = NO_JOBLIST_ARRAY_ENTRY
    BOOL    invitorIsPlayer = IS_BIT_SET(g_sJobListMP[Get_Invite_Array_Index()].jlBitfield, JOBLIST_BITFLAG_INVITOR_IS_PLAYER)
    
    IF (invitorIsPlayer)
        PLAYER_INDEX theInvitingPlayer = INT_TO_PLAYERINDEX(g_sJobListMP[Get_Invite_Array_Index()].jlUniqueID)
        invitationSlot  = Get_Joblist_RVInvite_Slot_For_Player(theInvitingPlayer)
    ELSE
        enumCharacterList theInvitingNPC = INT_TO_ENUM(enumCharacterList, g_sJobListMP[Get_Invite_Array_Index()].jlUniqueID)
        invitationSlot  = Get_Joblist_RVInvite_Slot_For_NPC_With_Type(theInvitingNPC, g_sJobListMP[Get_Invite_Array_Index()].jlMissionAsInt)
    ENDIF
    
    // ...is the invitation still available?
    IF (invitationSlot = NO_JOBLIST_ARRAY_ENTRY)
        // ...there is no invite from this invitor any more, so return to the Joblist screen
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList][App]: The Basic Invite no longer exists - returning to JobList screen") NET_NL()
        #ENDIF
        
        Display_JobList_Screen()
        EXIT
    ENDIF
    	
    // ...is the invitation still allowed to be displayed?
    IF NOT (Is_RVInvite_In_Slot_Allowed_To_Be_Displayed(invitationSlot))
        // ...the invitation in the slot is no longer allowed to be displayed
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList][App]: The Basic Invite is no longer allowed to be displayed - returning to JobList screen") NET_NL()
        #ENDIF
        
        Display_JobList_Screen()
        EXIT
    ENDIF
    
	IF g_sJobListMP[invitationSlot].jlMissionAsInt = FMMC_TYPE_HEIST_PHONE_INVITE
	OR g_sJobListMP[invitationSlot].jlMissionAsInt = FMMC_TYPE_HEIST_PHONE_INVITE_2
		Print_Join_Heist_Help(invitationSlot)
	ENDIF
	
    // Check if the player wants to go BACK to JobList
    IF (Did_Player_Press_MP_App_Negative_Response_Button(m_sAppData))
        Display_JobList_Screen()
        EXIT
    ENDIF
    	
    // Check if the player wants to ACCEPT the invitation
    IF NOT (Did_Player_Press_MP_App_Positive_Response_Button(m_sAppData))
        EXIT
    ENDIF
		
    // The player does want to accept the invitation
	// If the player has a Wanted Level then re-direct to the 'are you sure' screen for Apartments
	IF (g_sJobListMP[Get_Invite_Array_Index()].jlMissionAsInt = FMMC_TYPE_APARTMENT)
	OR (g_sJobListMP[Get_Invite_Array_Index()].jlMissionAsInt = FMMC_TYPE_HEIST_PHONE_INVITE)
	OR (g_sJobListMP[Get_Invite_Array_Index()].jlMissionAsInt = FMMC_TYPE_HEIST_PHONE_INVITE_2)
		IF (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0)
	        IF NOT (Can_I_Afford_To_Clear_Wanted_Level())
				Display_Help_Not_Enough_Cash_To_Clear_Wanted_Level()
	        
	        	Display_JobList_Screen()
	            
	            EXIT
	        ENDIF
				
			Display_RVInvite_Lose_Wanted_Level_Screen()
			
			EXIT
		ENDIF
	ENDIF
	
	
	
	// The calling script will handle fades and warps, so just set a flag to indicate acceptance
    Set_Joblist_RVInvite_As_Accepted_By_Player(invitationSlot)

    // Hangup and put away the phone and quit the app
    HANG_UP_AND_PUT_AWAY_PHONE()
    
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the RVInvite 'Lose Wanted Level' choice has been accepted or denied
PROC Process_Stage_Confirm_RVInvite_Lose_Wanted_Level()
    // Check if the player responds NO and return to Current Job screen
    IF (Did_Player_Press_MP_App_Negative_Response_Button(m_sAppData))
        Display_RVInvite_Screen()
        EXIT
    ENDIF
	
	// Alerts the player that their wanted level has been lost so they have been
	// sent back to the previous screen
	IF (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0)
		PRINT_HELP("FMMC_LOST_WANTED_LEVEL")
		Display_RVInvite_Screen()
		EXIT
	ENDIF
    	
    // Check if the player responds YES, charge the player, clear wanted level, try to reserve player on the mission
    IF NOT (Did_Player_Press_MP_App_Positive_Response_Button(m_sAppData))
		EXIT
	ENDIF
	
	// Player has accepted the Invite
	// Charge the player to remove the wanted level
	GAMER_HANDLE gamerHandle = GET_GAMER_HANDLE_PLAYER(PLAYER_ID())
	INT iRemoveWantedCost = GET_CONTACT_REQUEST_COST(REQUEST_REMOVE_WANTED)
	INT iScriptTransactionIndex
	IF USE_SERVER_TRANSACTIONS()
		TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_SPEND_LOSE_WANTED_LEVEL, iRemoveWantedCost, iScriptTransactionIndex, FALSE, TRUE)
		g_cashTransactionData[iScriptTransactionIndex].eExtraData.gamerHandle = gamerHandle
	ELSE
		NETWORK_SPENT_BUY_WANTEDLEVEL(iRemoveWantedCost, gamerHandle, FALSE, TRUE)
	ENDIF
		
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [JobList][App]: Player paid $")
		NET_PRINT_INT(GET_CONTACT_REQUEST_COST(REQUEST_REMOVE_WANTED))
		NET_PRINT(" to remove wanted level (Basic Invite).")
		NET_NL()
	#ENDIF
	
	IF (IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, FALSE))
		IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)		
			CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
			MPGlobalsAmbience.bBlockWantedLevelRP = TRUE
			
			// KGM 21/2/15 [BUG 2238981]: Let MatC know the player lost a Wanted Level
			MissionsAtCoords_Player_Paid_To_Remove_Wanted_Level()
		ENDIF
	
		#IF IS_DEBUG_BUILD
			NET_PRINT("         - WANTED LEVEL CLEARED") NET_NL()
		#ENDIF
	ENDIF

	// Accept the Basic Invite
    INT     invitationSlot  = NO_JOBLIST_ARRAY_ENTRY
    BOOL    invitorIsPlayer = IS_BIT_SET(g_sJobListMP[Get_Invite_Array_Index()].jlBitfield, JOBLIST_BITFLAG_INVITOR_IS_PLAYER)
    
    IF (invitorIsPlayer)
        PLAYER_INDEX theInvitingPlayer = INT_TO_PLAYERINDEX(g_sJobListMP[Get_Invite_Array_Index()].jlUniqueID)
        invitationSlot  = Get_Joblist_RVInvite_Slot_For_Player(theInvitingPlayer)
    ELSE
        enumCharacterList theInvitingNPC = INT_TO_ENUM(enumCharacterList, g_sJobListMP[Get_Invite_Array_Index()].jlUniqueID)
        invitationSlot  = Get_Joblist_RVInvite_Slot_For_NPC(theInvitingNPC)
    ENDIF
	
	// The calling script will handle fades and warps, so just set a flag to indicate acceptance
    Set_Joblist_RVInvite_As_Accepted_By_Player(invitationSlot)

    // Hangup and put away the phone and quit the app
    HANG_UP_AND_PUT_AWAY_PHONE()
    
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Performs navigation checks while the Fake Invitation screen is on display
PROC Process_Stage_Fake_Invitation()
    
    // Check if the player wants to go BACK to JobList
    IF (Did_Player_Press_MP_App_Negative_Response_Button(m_sAppData))
        Display_JobList_Screen()
        EXIT
    ENDIF
    
    // Check if the player wants to ACCEPT the invitation
    IF NOT (Did_Player_Press_MP_App_Positive_Response_Button(m_sAppData))
        EXIT
    ENDIF

    // The player does want to accept the invitation
    #IF IS_DEBUG_BUILD
        NET_PRINT("...KGM MP [JobList][App]: Process_Stage_Fake_Invitation() - Invitation has been accepted so hangup the phone.") NET_NL()
    #ENDIF

    // The calling script will handle fades and warps, so just set a flag to indicate acceptance
    Set_Fake_Joblist_Invite_As_Accepted_By_Player()

    // Hangup and put away the phone and quit the app
    HANG_UP_AND_PUT_AWAY_PHONE()
    
ENDPROC

PROC Cancel_Job()
	
	INT     playerGBD       = NATIVE_TO_INT(PLAYER_ID())
	
    // Abandon the mission
    FORCE_ABANDON_CURRENT_MP_MISSION()
    
    #IF IS_DEBUG_BUILD
        NET_PRINT("CHEAT: g_HasCancelledJobOnPhone = TRUE ") NET_NL()
    #ENDIF
    g_HasCancelledJobOnPhone = TRUE
    //GlobalplayerBD_FM[playerGBD].bQuitJob = TRUE
	SET_BIT(GlobalplayerBD_FM[playerGBD].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bQuitJob)
    // Move into a wait state
    Display_Wait_For_Current_Mission_To_Quit_Screen()
    
    #IF IS_DEBUG_BUILD
        NET_PRINT("...KGM MP [JobList][App]: Player is quitting current FM activity - FORCE QUIT flag set ready to be read by activity. Waiting for activity to end...") NET_NL()
    #ENDIF
    
	
ENDPROC

// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Performs navigation checks while the Cancel Job confirmation screen is on display.
// NOTES:   Don't worry about the data becoming out of date. The mission abandon routine will handle that gracefully.
PROC Process_Stage_Confirm_Cancel_Job()
    
	// Check if quitting mid-mission has been disabled.
	IF GB_IS_GLOBAL_NON_BD_BIT1_SET(eGB_GLOBAL_NON_BD_BITSET_1_DISABLE_QUIT_MID_MISSION)
		Display_JobList_Screen()
		EXIT
	ENDIF
	
    // Check if the player responds NO and return to Current Job screen
    IF (Did_Player_Press_MP_App_Negative_Response_Button(m_sAppData))
        Display_Current_Job_Screen()
        EXIT
    ENDIF
    
    // Check if the player responds YES send a Cancel Job request
    IF (Did_Player_Press_MP_App_Positive_Response_Button(m_sAppData))
        // The joblist shouldn't update while the player is deeper into the app than the JobList screen to ensure joblist details are preserved.
        // In theory, this check shouldn't fail - if it does then go back to the jobList screen
        // ...ensure it is the current mission
        IF NOT (g_sJobListMP[Get_Invite_Array_Index()].jlState = MP_JOBLIST_CURRENT_MISSION)
            #IF IS_DEBUG_BUILD
                NET_PRINT("...KGM MP [JobList][App]: Attempting to cancel current job but Joblist array position does not contain current mission data") NET_NL()
                Debug_Display_JobList_Array_Position(Get_Invite_Array_Index())
                SCRIPT_ASSERT("Process_Stage_Confirm_Cancel_Job(): Choosing Confirm Cancel failed to find valid job data. See console log. Tell Keith.")
            #ENDIF
            
            Display_JobList_Screen()
            EXIT
        ENDIF

        // Abandon the mission
        // KGM 30/9/12: While FM missions are handled differently from CnC missions I'll be using a bit of temporary workaround code
        IF (GET_CURRENT_GAMEMODE() = GAMEMODE_FM)
			// KGM 28/4/15 [BUG 2325536]: Handle ambient Time Trial as a special case (set flag directly rather than through 'Cancel_Job()' function)
			IF (g_sJobListMP[Get_Invite_Array_Index()].jlMissionAsInt = FMMC_TYPE_TIME_TRIAL)
			OR (g_sJobListMP[Get_Invite_Array_Index()].jlMissionAsInt = FMMC_TYPE_SANDBOX_ACTIVITY)
			OR (g_sJobListMP[Get_Invite_Array_Index()].jlMissionAsInt = FMMC_TYPE_HSW_TIME_TRIAL)
			OR (g_sJobListMP[Get_Invite_Array_Index()].jlMissionAsInt = FMMC_TYPE_HSW_SETUP)
				IF NOT (IS_FORCE_ABANDON_CURRENT_MP_MISSION_SET())
					MPGlobals.g_bForceKillActiveMission = TRUE
    				Display_Wait_For_Current_Mission_To_Quit_Screen()
    
				    #IF IS_DEBUG_BUILD
				        NET_PRINT("...KGM MP [JobList][App]: Process_Stage_Confirm_Cancel_Job() - FORCE QUIT flag set to cancel activity. Waiting for activity to end...") NET_NL()
				    #ENDIF
				ENDIF
				EXIT
			ENDIF
            
            // Ensure player is still on an FM activity
            IF NOT (IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID()))
                #IF IS_DEBUG_BUILD
                    NET_PRINT("...KGM MP [JobList][App]: Player is no longer on FM activity") NET_NL()
                #ENDIF
            
                Display_JobList_Screen()
                EXIT
            ENDIF
            
            // Ensure player is still on the same FM activity
            INT     playerGBD       = NATIVE_TO_INT(PLAYER_ID())
            INT     fmmcType        = GlobalplayerBD_FM[playerGBD].iCurrentMissionType
            INT     theCreatorID    = GlobalplayerBD_FM[playerGBD].currentMissionData.mdID.idCreator
			
            IF NOT (g_sJobListMP[Get_Invite_Array_Index()].jlMissionAsInt = fmmcType)
            OR NOT (g_sJobListMP[Get_Invite_Array_Index()].jlInstance = theCreatorID)
                #IF IS_DEBUG_BUILD
                    NET_PRINT("...KGM MP [JobList][App]: Player is now on a different FM activity") NET_NL()
                #ENDIF
            
                Display_JobList_Screen()
                EXIT
            ENDIF
            
			IF Is_Player_Currently_On_MP_Heist(PLAYER_ID())
			OR Is_Player_Currently_On_MP_Heist_Planning(PLAYER_ID())
//				IF g_iNumPlayersOnMyHeistTeam > -1
//					IF (g_iNumPlayersOnMyHeistTeam <= 1)
						g_bRunQuitHeistWarningScreen = TRUE
						PRINTLN("...KGM MP [JobList][App]: set g_bRunQuitHeistWarningScreen = TRUE.")
//						PRINTLN("...KGM MP [JobList][App]: g_iNumPlayersOnMyHeistTeam = ", g_iNumPlayersOnMyHeistTeam)
//					ENDIF
//				ENDIF
//				IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bIAmHeistLeader
//					g_bRunQuitHeistWarningScreen = TRUE
//					PRINTLN("...KGM MP [JobList][App]: set g_bRunQuitHeistWarningScreen = TRUE.")
//					PRINTLN("...KGM MP [JobList][App]: g_TransitionSessionNonResetVars.sGlobalCelebrationData.bIAmHeistLeader = TRUE")
//				ENDIF
			ENDIF
			
			IF NOT g_bRunQuitHeistWarningScreen
				Cancel_Job()
			ENDIF
			
            EXIT
        ENDIF
        
        // The original CnC routine
        MP_MISSION thisMission = INT_TO_ENUM(MP_MISSION, g_sJobListMP[Get_Invite_Array_Index()].jlMissionAsInt)
        Abandon_This_MP_Mission(thisMission, g_sJobListMP[Get_Invite_Array_Index()].jlInstance)
        
        // Move into a wait state
        Display_Wait_For_Current_Mission_To_Quit_Screen()
        
        EXIT
    ENDIF
    
ENDPROC

PROC Process_Confirm_Cancel_Job_Warning()
	
	IF g_bRunQuitHeistWarningScreen
		
		BOOL bCancelConfirmed
		BOOL bCancelWithdrawn
		
		SET_WARNING_MESSAGE_WITH_HEADER("HUD_CONNPROB", "HEIST_QUITWARNING", FE_WARNING_YESNO, "CONT_SURE")
		
		IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
			PRINTLN("...KGM MP [JobList][App]: IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT) = TRUE, setting bCancelConfirmed = TRUE.")
			bCancelConfirmed = TRUE
		ELIF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
			PRINTLN("...KGM MP [JobList][App]: IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL) = TRUE, setting bCancelWithdrawn = TRUE.")
			bCancelWithdrawn = TRUE
		ENDIF

		// Act on player decision.
		IF bCancelConfirmed
			
			PRINTLN("...KGM MP [JobList][App]: bCancelConfirmed = TRUE, calling Cancel_Job().")
			
			g_bRunQuitHeistWarningScreen = FALSE
			Cancel_Job()
			EXIT
			
		ELIF bCancelWithdrawn
			
			PRINTLN("...KGM MP [JobList][App]: bCancelWithdrawn = TRUE, exiting job list app script so we stop drawing warning message.")
			
			g_bRunQuitHeistWarningScreen = FALSE
	        EXIT
			
		ENDIF
		
	ENDIF
	
ENDPROC

// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Waits for confirmation of the player being reserved for the activity
// NOTES:   If reservation is confirmed there will be no additional monitoring for the mission request failing, etc. The MissionsAtCoords system will re-register the player and deal with any issues.
PROC Process_Stage_Waiting_For_Reservation_Confirmation()

    // Has the reservation request failed?
    IF (Has_Invite_Reservation_Request_Failed())
        // ...the reservation request 'FAILED'
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList][App]: Reservation Request for Invite FAILED. Returning to JobList screen.") NET_NL()
        #ENDIF
        
        Display_JobList_Screen()
        
		CLEAR_FM_JOB_ENTERY_TYPE()
		
        EXIT
    ENDIF
    
    // Has confirmation been received?
    IF NOT (Has_Invite_Reservation_Request_Been_Accepted())
        // ...confirmation still not received, so keep on waiting
		// KGM 14/9/13: Check for switching to a 'please wait' screen if there appears to be a problem
		IF (m_allowSwitchToPleaseWait)
			IF (GET_GAME_TIMER() > m_switchToPleaseWaitTimeout)
				m_switchToPleaseWaitTimeout	= 0
				m_allowSwitchToPleaseWait	= FALSE
				
				Display_Please_Wait_For_Confirmation_Screen()
			ENDIF
		ENDIF
		
		EXIT
    ENDIF

    #IF IS_DEBUG_BUILD
        NET_PRINT("...KGM MP [JobList][App]: Reservation Request for Invite was CONFIRMED. Hanging up the phone.") NET_NL()
    #ENDIF
    
    // Set the NPC Invite Accept Details as Reserved if applicable
    IF (g_sCMInviteAccept.ciInvitingContact != NO_CHARACTER)
        g_sCMInviteAccept.ciReservedForJob      = TRUE
        
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList][App]: Updating NPC Details for Accepted Invite (for CMs): ")
            TEXT_LABEL  npcNameTL = Get_NPC_Name(g_sCMInviteAccept.ciInvitingContact)
            NET_PRINT(GET_STRING_FROM_TEXT_FILE(npcNameTL))
            NET_PRINT(" (Filename: ")
            NET_PRINT(g_sCMInviteAccept.ciCloudFilename)
            NET_PRINT(") - PLAYER NOW RESERVED")
            NET_NL()
        #ENDIF
    ENDIF

    // Hangup and put away the phone and quit the app
    HANG_UP_AND_PUT_AWAY_PHONE()
    
//  IF IS_PLAYER_ON_A_PLAYLIST_INT(g_sJobListMP[Get_Invite_Array_Index()].jlUniqueID)
//      SET_PLAYER_ON_A_PLAYLIST(TRUE)
//      GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlaylistVars.iPlayListInstance = GlobalplayerBD_FM[g_sJobListMP[Get_Invite_Array_Index()].jlUniqueID].sPlaylistVars.iPlayListInstance
//  ENDIF
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Waits for the screen to be faded out before joining the mission
PROC Process_Stage_FM_Waiting_For_FadeOut_Before_Join(GET_UGC_CONTENT_STRUCT &sGetUGC_contentPassed)

    // Need to make sure the mission is still available
    // NOTE: Remember all the FM data is being squeezed into CnC variables that are badly named for the FM purpose - this should be temp
    INT playerGBD = g_sJobListMP[Get_Invite_Array_Index()].jlUniqueID
    IF (playerGBD = -1)
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList][App]: Attempting to join FM joinable activity but stored GBD slot is no longer valid - returning to JobList screen") NET_NL()
            Debug_Display_JobList_Array_Position(Get_Invite_Array_Index())
        #ENDIF
        
        Display_JobList_Screen()
        EXIT
    ENDIF
    
    // GBD slot is still active, so ensure all the details are still as expected
    BOOL isPlaylist = FALSE
    IF (g_sJobListMP[Get_Invite_Array_Index()].jlState = MP_JOBLIST_JOINABLE_PLAYLIST)
        isPlaylist = TRUE
    ENDIF
    
    IF (isPlaylist)
        // ...confirm still on same playlist
        IF NOT (g_sJobListMP[Get_Invite_Array_Index()].jlInstance = GlobalplayerBD_FM[playerGBD].sPlaylistVars.iPlayListInstance)
            #IF IS_DEBUG_BUILD
                NET_PRINT("...KGM MP [JobList][App]: Attempting to join FM joinable playlist but details have changed - returning to JobList screen") NET_NL()
                Debug_Display_JobList_Array_Position(Get_Invite_Array_Index())
            #ENDIF
            
            Display_JobList_Screen()
            EXIT
        ENDIF
    ELSE
        // ...confirm still on same mission
        IF NOT (g_sJobListMP[Get_Invite_Array_Index()].jlMissionAsInt = GlobalplayerBD_FM[playerGBD].iCurrentMissionType)
        OR NOT (g_sJobListMP[Get_Invite_Array_Index()].jlInstance = GlobalplayerBD_FM[playerGBD].currentMissionData.mdID.idCreator)
            #IF IS_DEBUG_BUILD
                NET_PRINT("...KGM MP [JobList][App]: Attempting to join FM joinable activity but details have changed - returning to JobList screen") NET_NL()
                Debug_Display_JobList_Array_Position(Get_Invite_Array_Index())
            #ENDIF
            
            Display_JobList_Screen()
            EXIT
        ENDIF
    ENDIF

    // If the screen isn't faded out, then need to stay in here until it is faded out taking whatever action is necessary to achieve fade out
    // KGM 16/11/12: To cater for some mission types that won't warp the player under some circumstances (Gang Hideout initially), the fadeOut is optional and may instead be handled by the script
    IF (Should_Game_FadeOut_When_Launching_This_FM_Mission_Type(g_sJobListMP[Get_Invite_Array_Index()].jlMissionAsInt))
        IF NOT (IS_SCREEN_FADED_OUT())
            // ...screen is not faded out
            IF (IS_SCREEN_FADED_IN())
                #IF IS_DEBUG_BUILD
                    NET_PRINT("...KGM MP [JobList][App]: Screen is faded in, so start a fadeout before joining a mission in progress") NET_NL()
                #ENDIF
                
                DO_SCREEN_FADE_OUT(500)
            ENDIF
            
            // Screen is not faded in, so it must beeither fading in or fading out - either way we need to wait until the fade ends before taking next action
            #IF IS_DEBUG_BUILD
                IF (IS_SCREEN_FADING_IN())
                    NET_PRINT("...KGM MP [JobList][App]: Screen is fading in, so need to wait for that to end before starting a fade out") NET_NL()
                ENDIF

                IF (IS_SCREEN_FADING_OUT())
                    NET_PRINT("...KGM MP [JobList][App]: Screen is fading out, so need to wait for that to end before joining a mission in progress") NET_NL()
                ENDIF
            #ENDIF
            
            EXIT
        ENDIF
    ENDIF
    
    // Player is joining an MP mission in progress.
    IF IS_NET_PLAYER_OK(PLAYER_ID())
        IF NOT JOIN_FM_MISSION_IN_PROGRESS_VIA_PHONE(   sGetUGC_contentPassed, 
                                                        GlobalplayerBD_FM[playerGBD].tl31CurrentMissionFileName,                //The file to load
                                                        GlobalplayerBD_FM[playerGBD].currentMissionData.mdID.idCreator, 
                                                        GlobalplayerBD_FM[playerGBD].currentMissionData.iInstanceId,            //The instance to join!
                                                        GlobalplayerBD_FM[playerGBD].currentMissionData.mdID.idVariation,       //The mission Variation
                                                        GlobalplayerBD_FM[playerGBD].iCurrentMissionType,                       //The mission Type
                                                        playerGBD,                                                              //The player that I'm joining (for PLAYLISTS!!!)
                                                        GlobalplayerBD_FM[playerGBD].tl23CurrentMissionOwner)                   //The user to load from
            #IF IS_DEBUG_BUILD
                NET_PRINT("...KGM MP [JobList][App]: Screen faded out or fade not required, but JOIN_FM_MISSION_IN_PROGRESS_VIA_PHONE() returned FALSE - continuing to wait") NET_NL()
            #ENDIF
            
            EXIT
        ENDIF
        
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList][App]: Screen is faded out and JOIN_FM_MISSION_IN_PROGRESS_VIA_PHONE() has returned TRUE - good to go") NET_NL()
            NET_PRINT("          PlayerGBD: ")
            NET_PRINT_INT(playerGBD)
            NET_PRINT("  CreatorID: ")
            NET_PRINT_INT(GlobalplayerBD_FM[playerGBD].currentMissionData.mdID.idCreator)
            NET_PRINT("  InstanceID: ")
            NET_PRINT_INT(GlobalplayerBD_FM[playerGBD].currentMissionData.iInstanceId)
            NET_PRINT("  Variation = ")
            NET_PRINT_INT(GlobalplayerBD_FM[playerGBD].currentMissionData.mdID.idVariation)
            NET_PRINT("  Type = ")
            NET_PRINT_INT(GlobalplayerBD_FM[playerGBD].iCurrentMissionType)
            NET_NL()
            NET_PRINT("          Coords = ")
            NET_PRINT_VECTOR(GlobalplayerBD_FM[playerGBD].currentMissionData.mdPrimaryCoords)
            NET_PRINT("  MissionOwner = ")
            NET_PRINT(GlobalplayerBD_FM[playerGBD].tl23CurrentMissionOwner)
            NET_PRINT("  MissionFilename = ")
            NET_PRINT(GlobalplayerBD_FM[playerGBD].tl31CurrentMissionFileName)
            NET_NL()
        #ENDIF
    ELSE
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList][App]: Screen is faded out but net player is not ok") NET_NL()
        #ENDIF
    ENDIF

    // Hangup and put away the phone and quit the app
    #IF IS_DEBUG_BUILD
        NET_PRINT("...KGM MP [JobList][App]: Process_Stage_FM_Waiting_For_FadeOut_Before_Join() hanging up the phone") NET_NL()
    #ENDIF
    
    HANG_UP_AND_PUT_AWAY_PHONE()
    
    PRINTLN("ciFMMC_MISSION_DATA_BitSet_MissionDataLoaded CLEARING")
    CLEAR_BIT(GlobalplayerBD_FM[ NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_MissionDataLoaded)
    CLEAR_BIT(GlobalplayerBD_FM[ NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_ReadyToVote)
    
    
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: The player has requested to quit the mission, so wait for the player to be off it before re-displaying updated joblist.
PROC Process_Stage_Waiting_For_Player_To_Quit_Mission()

    // KGM 30/9/12: While FM missions are handled differently from CnC missions I'll be using a bit of temporary workaround code
    IF (GET_CURRENT_GAMEMODE() = GAMEMODE_FM)
        // Clean up if the player is no longer on the FM activity
        IF NOT (IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID()))
            #IF IS_DEBUG_BUILD
                NET_PRINT("...KGM MP [JobList][App]: Player is no longer on an FM activity, so clearing 'abandon' flag and putting away phone") NET_NL()
            #ENDIF
            
            // Clear the 'quit' flag
            CLEAR_ABANDON_MP_MISSION_FLAG()
            
            // Put away the phone
            HANG_UP_AND_PUT_AWAY_PHONE()
        ELSE
            #IF IS_DEBUG_BUILD
                NET_PRINT("...KGM MP [JobList][App]: Mission has been abandoned through joblist, but still waiting for player to be off-mission") NET_NL()
            #ENDIF
        ENDIF
        
        EXIT
    ENDIF
    
    // The joblist shouldn't update while in this stage to preserve the data, but return to the JobList screen if a problem does occur
    IF NOT (g_sJobListMP[Get_Invite_Array_Index()].jlState = MP_JOBLIST_CURRENT_MISSION)
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList][App]: Waiting for player to quit current mission but Joblist array position does not contain current mission data") NET_NL()
            Debug_Display_JobList_Array_Position(Get_Invite_Array_Index())
            SCRIPT_ASSERT("Process_Stage_Waiting_For_Player_To_Quit_Mission(): Failed to find valid job data. See console log. Tell Keith.")
        #ENDIF
        
        Display_JobList_Screen()
        EXIT
    ENDIF
    
    // The original CnC method to check if the player is on this mission
    MP_MISSION thisMission = INT_TO_ENUM(MP_MISSION, g_sJobListMP[Get_Invite_Array_Index()].jlMissionAsInt)
    IF (IS_PLAYER_RUNNING_MP_MISSION_INSTANCE(PLAYER_ID(), thisMission, g_sJobListMP[Get_Invite_Array_Index()].jlInstance))
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList][App]: Joblist waiting for mission to quit before returning to main Joblist screen") NET_NL()
        #ENDIF
        
        EXIT
    ENDIF
    
    // Player is not on the current mission, so return to JobList
    Display_JobList_Screen()
        
ENDPROC




// ===========================================================================================================
//      Main Loop
// ===========================================================================================================

SCRIPT

    // Ensure script can run in network game
    NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()
    
    // DO THIS FIRST: Initialise the MP Cellphone App public header
    Initialise_MP_App_Variables(m_sAppData)
    
    // Allow help text when app first opened only (must be prior to displaying the joblist screen)
// KGM 31/8/13: ToDo 1605311 (Pay to lose Wanted Level when accepting invites)
    m_wantedLevelHelpDisplayed = FALSE
    //m_bountyHelpDisplayed = FALSE
    
    // List navigation initial values
    m_selectedJobSlot = 0
	    
    // Display the JobList Screen when the app starts
    // NOTE: Display JobList Screen should set the cellphone state as RunningApp
    // NOTE: If the player is on a mission then this will call teh 'cancel current job' screen instead and it will still be classed as RunningApp, not ComplexApp.
    Display_JobList_Screen(TRUE, FALSE, TRUE)

    // Main Loop - never terminates while the app can legitimately still run
    WHILE TRUE
        WAIT(0)
        
	    // Clear the New Invites indicator
	    Reset_New_Invites_Indicator("[MMM] Calling Reset_New_Invites_Indicator from AppMpJoblistNew")

		IF g_sBossAgencyApp.iTotalUnreadBikerInvites != 0
			PRINTLN("...KGM MP [JobList][App][Biker] Resetting new invite indicator for Biker invites")
			g_sBossAgencyApp.iTotalUnreadBikerInvites = 0
		ENDIF

        //Steve T. Removed this to get the Job List running in SP
        // Make sure still in the network game
        /*
        IF NOT (NETWORK_IS_IN_SESSION())
            #IF IS_DEBUG_BUILD
                NET_PRINT("...KGM MP [JobList][App]: appMpJobListNEW - NETWORK_IS_IN_SESSION() returned FALSE - Disabling cellphone and terminating App") NET_NL()
            #ENDIF
            
            // Disable the cellphone
            DISABLE_CELLPHONE(TRUE)
            Cleanup_and_Terminate()
        ENDIF
        */

		// Because this is just setting a flag, we need to disable to controls here as well
	
		Ensure_Exclusive_Joblist_Controls()
		
		g_inviteControlTimeout = GET_GAME_TIMER() + INVITE_ACCEPT_CONTROLS_BLOCK_TIMEOUT_msec

        
        // Get the timer
        //m_sAppData.mpadNetworkTimer = GET_NETWORK_TIME()
        
        // Set the 'allow update' global flag to FALSE - it'll be set within any function that allows the update to happen
        g_sJoblistUpdate.joblistActiveUpdate = FALSE
        
        // Used to tell the help text system that the 'delete' icon is on-screen
        // This will be set TRUE in appropriate places
        g_cancelJobButtonOnScreen   = FALSE
     	
		// Process post phone warning screen.
		
		Process_Confirm_Cancel_Job_Warning()
		
		
        // Need to handle interrupting the app if a cellphone call comes through
        // KGM 7/5/12: Also need to ensure a request for AWAY or DISABLED prevents any processing within the App
        IF NOT (g_Cellphone.PhoneDS = PDS_ONGOING_CALL)
        AND (g_CellPhone.PhoneDS > PDS_AWAY)
        
            // Deal with the various stages of the app
            SWITCH (m_currentStage)
                // JobList screen
                CASE JLS_JOBLIST
                    Process_Stage_JobList()
                    BREAK
                    
                // Cancel Current Job screen - this is now the only screen displayed if the player is on a mission and the mission can be cancelled directly freom here
                CASE JLS_CURRENT_JOB
                    Process_Stage_Current_Job()
                    g_cancelJobButtonOnScreen   = TRUE
                    BREAK
                    
                // Accept Joinable Job screen
                // NOTE: There is no longer a confirm screen after this screen
                CASE JLS_JOINABLE_JOB
                    Process_Stage_Joinable_Job()
                    BREAK
                    
                // Accept Invitation screen
                CASE JLS_INVITATION
                    Process_Stage_Invitation()
                    BREAK
                    
                // Accept Cross-Session Invite screen
                CASE JLS_CSINVITE
                    Process_Stage_CSInvite()
                    BREAK
                    
                // Accept Basic Invite screen
                CASE JLS_RVINVITE
                    Process_Stage_RVInvite()
                    BREAK
                    
                // Accept Fake Invitation screen
                CASE JLS_FAKE_INVITATION
                    Process_Stage_Fake_Invitation()
                    BREAK
                    
                // Cancel Job confirmation screen
                CASE JLS_CONFIRM_CANCEL_JOB
                    Process_Stage_Confirm_Cancel_Job()
                    BREAK
				
				// Confirm pay to lose wanted level when accepting Invitation
				CASE JLS_CONFIRM_INVITE_LOSE_WANTED_LEVEL
                    Process_Stage_Confirm_Invite_Lose_Wanted_Level()
					BREAK
					
				// Confirm pay to lose wanted level when accepting CSInivte
				CASE JLS_CONFIRM_CSINVITE_LOSE_WANTED_LEVEL
                    Process_Stage_Confirm_CSInvite_Lose_Wanted_Level()
					BREAK
					
				// Confirm pay to lose wanted level when accepting RVInvite
				CASE JLS_CONFIRM_RVINVITE_LOSE_WANTED_LEVEL
                     Process_Stage_Confirm_RVInvite_Lose_Wanted_Level()
					BREAK
                   
                // Wait for confirmation of reservation on activity
                CASE JLS_WAITING_FOR_RESERVATION_CONFIRMATION
                    Process_Stage_Waiting_For_Reservation_Confirmation()
                    BREAK
                    
                // For Freemode, wait for the game to fade out before joining the mission
                CASE JLS_FM_WAITING_FOR_FADEOUT_BEFORE_JOIN
                    Process_Stage_FM_Waiting_For_FadeOut_Before_Join(sGetUGC_content)
                    BREAK
                    
                // Wait for player to no longer be on the current mission
                CASE JLS_WAITING_FOR_PLAYER_TO_QUIT_MISSION
                    Process_Stage_Waiting_For_Player_To_Quit_Mission()
                    BREAK
					
				CASE JLS_ALTERNATIVE_INVITE_SCREEN
					Process_Stage_Alternative_Invite_Confirm()
					BREAK
					
				CASE JLS_ALTERNATIVE_PASSIVE_WARNING
					Process_Stage_Alternative_Passive_Warning()
					BREAK
                    
                DEFAULT
                    #IF IS_DEBUG_BUILD
                        SCRIPT_ASSERT("appMpJobListNEW: Unknown MP JobList App Stage. Tell Keith.")
                        PRINTNL()
                    #ENDIF
                    BREAK
            ENDSWITCH
            
            // This will automatically exit the cellphone if the back button is pressed.
            // If you move two screens deep into your app, then you must set your app state, g_Cellphone.PhoneDS to PDS_COMPLEXAPP so this doesn't return true.
            IF (g_Cellphone.PhoneDS <> PDS_COMPLEXAPP)
                IF (CHECK_FOR_APPLICATION_EXIT())
                    #IF IS_DEBUG_BUILD
                        NET_PRINT("...KGM MP [JobList][App]: appMpJobListNEW - CHECK_FOR_APPLICATION_EXIT() returned TRUE") NET_NL()
                    #ENDIF
	
					// KGM 22/7/15 [BUG 2437060]: If any Invite Accept warning message is on display, clear it
					Clear_Accept_Invite_Warning_Help_Text_If_On_Display()
                    
                    Cleanup_and_Terminate()
                ENDIF
            ENDIF
        ELSE
            //Update: Incoming calls can't currently go through to the phone whilst an app is running. If this changes, we'll need to reinstate it.

            //Reinstate the block below to make the phone terminate this script when a phonecall comes through so that the phone will
            //return to the main menu post-phonecall.

            //This is an important section! It tells this script what to do when a call, be it answerphone or other phonecall, comes through.

            //BeforeCallPhoneDS = PDS_MAXIMUM
            //Cleanup_and_Terminate()
        ENDIF
        
        //Critical! Do not remove. This must run every frame as it handles bad things such as the player dying when the phone is active.
        IF CHECK_FOR_ABNORMAL_EXIT()
            #IF IS_DEBUG_BUILD
                NET_PRINT("...KGM MP [JobList][App]: appMpJobListNEW - CHECK_FOR_ABNORMAL_EXIT() returned TRUE") NET_NL()
            #ENDIF
	
			// KGM 22/7/15 [BUG 2437060]: If any Invite Accept warning message is on display, clear it
			Clear_Accept_Invite_Warning_Help_Text_If_On_Display()
            
            Cleanup_and_Terminate()
        ENDIF
    ENDWHILE

ENDSCRIPT


