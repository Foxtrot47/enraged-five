// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	 :	appJIPMP.sc
//		AUTHOR			 :	RowanC based on Keiths's MP template
//		DESCRIPTION		:	Alows players to Join a quick Job from their phone (DM, RACE, MISSION ETC).
//									
//
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_script.sch"
USING "net_app_public.sch"
USING "cellphone_public.sch"
USING "transition_common.sch"
USING "net_transition_sessions.sch"
USING "FM_Quickmatch_Header.sch"	 
USING "drunk_public.sch"
USING "net_cash_transactions.sch"
USING "net_corona_V2.sch"

// ===========================================================================================================
//		Variables and Constants
// ===========================================================================================================

// JIP_MP Screen Stages
ENUM enumJIPMPStages
	 QUICKMATCH_MODE_LIST,									 // The List of joinable modes
	 QUICKMATCH_FRIENDS_OR_SELF_OR_ON_CALL,				// A comfirm with friends
	 QUICKMATCH_SUBLIST,								// Sublist screen
	 QUICKMATCH_OFFER_JOIN									 // A comfirm join mode screen
ENDENUM 
enumJIPMPStages m_currentStage = QUICKMATCH_MODE_LIST

ENUM enumQuickMatchModes
	QUICKMATCH_MODE_SERIES = 0,
	QUICKMATCH_MODE_HEISTS,
	QUICKMATCH_MODE_CONTACT_MISSIONS,
	QUICKMATCH_MODE_JOBS,
	QUICKMATCH_MODE_ACTIVITIES,
	QUICKMATCH_MODE_PLAYLIST,
	QUICKMATCH_MODE_RANDOM,
	QUICKMATCH_MODE_MAX
ENDENUM
enumQuickMatchModes m_currentMode = QUICKMATCH_MODE_SERIES

ENUM enumQuickMatchSeries
	QUICKMATCH_SERIES_FEATURED = 0,
	QUICKMATCH_SERIES_COMMUNITY,
	QUICKMATCH_SERIES_CAYO_PERICO,
	#IF FEATURE_GEN9_EXCLUSIVE
	QUICKMATCH_SERIES_HSW_RACE,
	#ENDIF
	QUICKMATCH_SERIES_STREET_RACE,
	QUICKMATCH_SERIES_PURSUIT,
	QUICKMATCH_SERIES_OPEN_WHEEL,
	QUICKMATCH_SERIES_RACE,
	QUICKMATCH_SERIES_SURVIVAL,
	QUICKMATCH_SERIES_ARENA_WAR,
	QUICKMATCH_SERIES_SPECIAL_RACE,
	QUICKMATCH_SERIES_SUPER_SPORT,
	QUICKMATCH_SERIES_TRANSFORM,
	QUICKMATCH_SERIES_BUNKER,
	QUICKMATCH_SERIES_STUNT,
	QUICKMATCH_SERIES_ADVERSARY,
	QUICKMATCH_SERIES_ACTIVE_PREMIUM_RACE,
	QUICKMATCH_SERIES_MAX
ENDENUM

ENUM enumQuickMatchHeists
	QUICKMATCH_HEISTS_HEIST = 0,
	QUICKMATCH_HEISTS_DOOMSDAY,
	QUICKMATCH_HEISTS_DIAMOND_CASINO,
	#IF FEATURE_HEIST_ISLAND
	QUICKMATCH_HEISTS_CAYO_PERICO,
	#ENDIF
	QUICKMATCH_HEISTS_MAX
ENDENUM

ENUM enumQuickMatchContactMissions
	QUICKMATCH_CONTACT_MISSIONS_CONTACT = 0,
	QUICKMATCH_CONTACT_MISSIONS_SHORT_TRIP,
	QUICKMATCH_CONTACT_MISSIONS_AUTO_SHOP,
	QUICKMATCH_CONTACT_MISSIONS_MOBILE_OPERATION,
	QUICKMATCH_CONTACT_MISSIONS_SPECIAL_VEHICLE,
	QUICKMATCH_CONTACT_MISSIONS_LAMAR,
	QUICKMATCH_CONTACT_MISSIONS_MAX
ENDENUM

ENUM enumQuickMatchJobs
	QUICKMATCH_JOBS_RACE = 0,
	QUICKMATCH_JOBS_DEATHMATCH,
	QUICKMATCH_JOBS_VERSUS,
	QUICKMATCH_JOBS_LTS,
	QUICKMATCH_JOBS_CAPTURE,
	QUICKMATCH_JOBS_SURVIVAL,
	QUICKMATCH_JOBS_PARACHUTING,
	QUICKMATCH_JOBS_MAX
ENDENUM

ENUM enumQuickMatchActivities
	QUICKMATCH_ACTIVITIES_ARM = 0 ,
	QUICKMATCH_ACTIVITIES_DARTS,
	QUICKMATCH_ACTIVITIES_GOLF,
	QUICKMATCH_ACTIVITIES_RANGE ,
	QUICKMATCH_ACTIVITIES_TENNIS,
	QUICKMATCH_ACTIVITIES_MAX
ENDENUM

TEXT_LABEL_15	m_QuickMatchTextFM				= "CELL_37"
TEXT_LABEL_15	m_areYouSureText			 	= "CELL_249"
TEXT_LABEL_15	m_QuickMatchIconTextBack		= "CELL_206"
TEXT_LABEL_15	m_QuickMatchIconTextYes			= "CELL_212"
TEXT_LABEL_15	m_QuickMatchIconTextNo			= "CELL_213"
// -----------------------------------------------------------------------------------------------------------

// NOTE: A List screen, which is view state 18
CONST_INT		 QUICKMATCH_LIST_VIEW_STATE					18
CONST_INT		 CONFIRM_VIEW_STATE			 				13


// -----------------------------------------------------------------------------------------------------------

TEXT_LABEL_23 m_currentWarningHelpText = ""

// The JIPMP List array position for the mode selected by the player
INT	m_selectedQuickMatchSlot	= 0
INT	g_numQuickMatchListEntries	= 6
INT	iCanDoQuickmatch
BOOL bBlockAccept
CONST_INT ciFRIENDS_OPTION_WITH		0
CONST_INT ciFRIENDS_OPTION_ON_CALL	1
CONST_INT ciFRIENDS_OPTION_DIRECT	2

BOOL dpad_scroll_pause_cued = FALSE
BOOL bDisplayWithFriends
BOOl bPlayerIsDrunk

//[RANKED_RACES]
STRUCT GET_ELO_DATA
	BOOL bGotElo
	INT iReadStage
	INT iLoadStage
	INT iFailCount
	BOOL bSuccessful
ENDSTRUCT
GET_ELO_DATA sGetEloData

INT iselectedModeSlot = 0
INT iSelectedContent = 0
INT iselectedFriendsOnCallSlot = -1
INT m_ModeList[QUICKMATCH_MODE_MAX]
INT m_SeriesList[QUICKMATCH_SERIES_MAX]
INT m_HeistsList[QUICKMATCH_HEISTS_MAX]
INT m_ContactMissionsList[QUICKMATCH_CONTACT_MISSIONS_MAX]
INT m_JobsList[QUICKMATCH_JOBS_MAX]
INT m_ActivitiesList[QUICKMATCH_ACTIVITIES_MAX]
INT m_iStageState

SCRIPT_TIMER stQuickMatchTimer
BOOL bDebounceOfferJobAccept = FALSE

SCALEFORM_RETURN_INDEX Choice_ReturnedSFIndex


INT slotsUsed = 0	//Slot counter, now also interrogated to decide which soft keys should be displayed.
INT slotsUsedContent = 0	//Slot counter, now also interrogated to decide which soft keys should be displayed.



// -----------------------------------------------------------------------------------------------------------

// An instance of the Mp App data struct (just include this - it allows some general functionality from net_app_public.sch to work - ie: screen navigation, etc
structMPAppData m_sAppData

PROC Display_Accept_Invite_Warning_Help_Text_If_Required()

	// NOTE: Stores the help so that it can be check if still on display and removed when no longer needed (ie: screen change or app termination)

	BOOL displayHelp = FALSE
	IF GB_IS_PLAYER_CRITICAL_TO_JOB(PLAYER_ID())
		IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_BIKER_GANG()
			m_currentWarningHelpText = "PHINVQUITBBB"
		ELSE
			m_currentWarningHelpText = "PHINVQUITBB"
		ENDIF
		displayHelp = TRUE
	ELIF IS_PLAYER_CRITICAL_TO_ANY_EVENT(PLAYER_ID())
		m_currentWarningHelpText = "PHINVQUITB"
		displayHelp = TRUE
	ENDIF
	
	IF (FM_EVENT_SHOULD_GIVE_PHONE_INVITE_WARNING())
		m_currentWarningHelpText = "PHINVQUIT"
		displayHelp = TRUE
	ENDIF
	
	
	IF (displayHelp)
		PRINTLN("[TS]  Display_Accept_Invite_Warning_Help_Text_If_Required(). Display: ", m_currentWarningHelpText)
	ENDIF
	
	PRINT_HELP(m_currentWarningHelpText)
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

PROC Clear_Accept_Invite_Warning_Help_Text_If_On_Display()

	IF (IS_STRING_NULL_OR_EMPTY(m_currentWarningHelpText))
		EXIT
	ENDIF
	
	// Is the message being displayed?
	IF NOT (IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(m_currentWarningHelpText))
		// ...no, so clear the variable
		m_currentWarningHelpText = ""
		EXIT
	ENDIF
	
	// Message is being displayed, so clear it and the variable
	CLEAR_HELP()
	
	PRINTLN("[TS] : Clear_Accept_Invite_Warning_Help_Text_If_On_Display(). Clear: ", m_currentWarningHelpText)
	
	m_currentWarningHelpText = ""

ENDPROC
//[RANKED_RACES]
FUNC BOOL GET_LOCAL_PLAYER_ELO()
	IF GET_PLAYER_ELO_FOR_MODE(LEADERBOARD_FREEMODE_ELO_RACES_OVERALL, sGetEloData.iReadStage, sGetEloData.iLoadStage, sGetEloData.bSuccessful, g_TransitionSessionNonResetVars.sTransVars.iMyElo)
		PRINTLN("[TS] GET_PLAYER_ELO_FOR_MODE - g_TransitionSessionNonResetVars.sTransVars.iMyElo = ", g_TransitionSessionNonResetVars.sTransVars.iMyElo)
		//If we got it then move on
		IF sGetEloData.bSuccessful
			RETURN TRUE
		ELSE
			//If we failed 3 times then set it to default and give up!S
			IF sGetEloData.iFailCount > 2
				PRINTLN("[TS] GET_PLAYER_ELO_FOR_MODE - g_TransitionSessionNonResetVars.sTransVars.iMyElo = ", g_TransitionSessionNonResetVars.sTransVars.iMyElo)
				SET_MY_TRANSITION_SESSION_ELO(1200)
				RETURN TRUE
			ENDIF
			sGetEloData.iFailCount++
			sGetEloData.iReadStage = 0
			sGetEloData.iLoadStage = 0					
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

// ===========================================================================================================
//		Cleanup Routines
// ===========================================================================================================

// PURPOSE: Cleanly terminates the script
PROC Cleanup_and_Terminate()
	//[RANKED_RACES]
	//If we've not got that ELO then wait to get it
//	IF AM_I_TRANSITION_SESSIONS_STARTING_RANKED_QUICK_MATCH()
//		IF sGetEloData.bGotElo = FALSE
//			WHILE NOT GET_LOCAL_PLAYER_ELO()
//				WAIT(0)
//			ENDWHILE
//		ENDIF
//	ENDIF
	Clear_Accept_Invite_Warning_Help_Text_If_On_Display()
	 TERMINATE_THIS_THREAD()
ENDPROC


FUNC BOOL SHOULD_SHOW_QUICKMATCH_MODE_MISSION_NEW_VS(INT iPlayList)
	IF iPlayList != -1
	AND g_sMPTunables.iProfesionalCoronaType[iPlayList] = FMMC_TYPE_MISSION_NEW_VS
	AND g_sV2CoronaVars.iCurrentArrayPosProfessional[iPlayList] != -1
		INT iStatBitSetTemp = GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_PIM_HIDE_MISC)
		IF NOT IS_BIT_SET(iStatBitSetTemp, ciPI_HIDE_MENU_ITEM_JOBS_SERIES_FEATURED) AND SHOULD_HIDE_ALL_SERIES_MODE_BLIPS()
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC STRING GET_MODE_TEXT(enumQuickMatchModes eMode)
	 SWITCH eMode
	 	CASE QUICKMATCH_MODE_SERIES				RETURN "TUN_CELL_SERIES"
		CASE QUICKMATCH_MODE_HEISTS				RETURN "TUN_CELL_HEISTS"
		CASE QUICKMATCH_MODE_CONTACT_MISSIONS	RETURN "TUN_CELL_CONTACT_M"
		CASE QUICKMATCH_MODE_JOBS				RETURN "TUN_CELL_JOBS"
		CASE QUICKMATCH_MODE_ACTIVITIES			RETURN "TUN_CELL_ACTIVITIES"
		CASE QUICKMATCH_MODE_PLAYLIST			RETURN "JIPMP_PL"
		CASE QUICKMATCH_MODE_RANDOM				RETURN "JIPMP_ANY"
	ENDSWITCH
	RETURN ""
ENDFUNC

FUNC STRING GET_CONTENT_TEXT(enumQuickMatchModes eMode,INT index)
	SWITCH eMode
		CASE QUICKMATCH_MODE_SERIES
			SWITCH INT_TO_ENUM(enumQuickMatchSeries,index)
				CASE QUICKMATCH_SERIES_FEATURED 			
					IF SHOULD_SHOW_QUICKMATCH_MODE_MISSION_NEW_VS(CV2_GET_NEW_VS_ARRAY_POS())
						RETURN "JIPMP_STNTAM"	
					ELSE
						RETURN "JIPMP_MISS_NV"		
					ENDIF
				BREAK
				CASE QUICKMATCH_SERIES_COMMUNITY			RETURN "COM_SERIES"
				CASE QUICKMATCH_SERIES_CAYO_PERICO			RETURN "CAY_SERIES"
				#IF FEATURE_GEN9_EXCLUSIVE
				CASE QUICKMATCH_SERIES_HSW_RACE				RETURN "HSW_SERIES"
				#ENDIF
				CASE QUICKMATCH_SERIES_STREET_RACE			RETURN "SR_SERIES"
				CASE QUICKMATCH_SERIES_PURSUIT				RETURN "PU_SERIES"
				CASE QUICKMATCH_SERIES_OPEN_WHEEL			RETURN "OW_SERIES"
				CASE QUICKMATCH_SERIES_RACE					RETURN "RACE_SERIES"
				CASE QUICKMATCH_SERIES_SURVIVAL				RETURN "SURV_SERIES"
				CASE QUICKMATCH_SERIES_ARENA_WAR			RETURN "ARENA_SERIES"
				CASE QUICKMATCH_SERIES_SPECIAL_RACE			RETURN "JIPMP_SVR"
				CASE QUICKMATCH_SERIES_SUPER_SPORT			RETURN "HOT_SERIES"
				CASE QUICKMATCH_SERIES_TRANSFORM			RETURN "TRAN_SERIES"
				CASE QUICKMATCH_SERIES_BUNKER				RETURN "BNKR_SERIES"
				CASE QUICKMATCH_SERIES_STUNT				
				IF g_sMPTunables.bDisableStuntSeriesBucket
						RETURN "JIPMP_STNTRD"		
					ELSE
						RETURN "JIPMP_STNTR"		
					ENDIF
				BREAK
				CASE QUICKMATCH_SERIES_ADVERSARY
				IF g_sMPTunables.bDisableStuntSeriesBucket
						RETURN "JIPMP_ADVSD"		
					ELSE
						RETURN "JIPMP_ADVSR"		
					ENDIF
				BREAK
				CASE QUICKMATCH_SERIES_ACTIVE_PREMIUM_RACE		RETURN "JIPMP_STNTPR"
			ENDSWITCH
		BREAK
		
		CASE QUICKMATCH_MODE_HEISTS
			SWITCH INT_TO_ENUM(enumQuickMatchHeists,index)
				CASE QUICKMATCH_HEISTS_HEIST				RETURN "JIPMP_HEIST"
				CASE QUICKMATCH_HEISTS_DOOMSDAY				RETURN "HUD_AWD_GANGOP"
				CASE QUICKMATCH_HEISTS_DIAMOND_CASINO		RETURN "CH_QM_PHONE"
				#IF FEATURE_HEIST_ISLAND
				CASE QUICKMATCH_HEISTS_CAYO_PERICO			RETURN "IH_END_NAME"
				#ENDIF
			ENDSWITCH
		BREAK
		
		CASE QUICKMATCH_MODE_CONTACT_MISSIONS
			SWITCH INT_TO_ENUM(enumQuickMatchContactMissions,index)
				CASE QUICKMATCH_CONTACT_MISSIONS_CONTACT				RETURN "JIPMP_MISS"
				CASE QUICKMATCH_CONTACT_MISSIONS_SHORT_TRIP				RETURN "JIPMP_STRIP"
				CASE QUICKMATCH_CONTACT_MISSIONS_AUTO_SHOP				RETURN "TUN_ROB_CONTR"
				CASE QUICKMATCH_CONTACT_MISSIONS_MOBILE_OPERATION		RETURN "JIPMP_WVM"
				CASE QUICKMATCH_CONTACT_MISSIONS_SPECIAL_VEHICLE		RETURN "JIPMP_SVM"
				CASE QUICKMATCH_CONTACT_MISSIONS_LAMAR					RETURN "JIPMP_MISS_FL"
			ENDSWITCH
		BREAK
		
		CASE QUICKMATCH_MODE_JOBS
			SWITCH INT_TO_ENUM(enumQuickMatchJobs,index)
				CASE QUICKMATCH_JOBS_RACE 					RETURN "JIPMP_RACE"
				CASE QUICKMATCH_JOBS_DEATHMATCH 			RETURN "JIPMP_DM"
				CASE QUICKMATCH_JOBS_VERSUS 				RETURN "JIPMP_MISS_V"
				CASE QUICKMATCH_JOBS_LTS 					RETURN "JIPMP_MISS_L"
				CASE QUICKMATCH_JOBS_CAPTURE				RETURN "JIPMP_MISS_C"
				CASE QUICKMATCH_JOBS_SURVIVAL				RETURN "JIPMP_SURV"
				CASE QUICKMATCH_JOBS_PARACHUTING 			RETURN "JIPMP_PARA"
			ENDSWITCH
		BREAK
		
		CASE QUICKMATCH_MODE_ACTIVITIES
			SWITCH INT_TO_ENUM(enumQuickMatchActivities,index)
				CASE QUICKMATCH_ACTIVITIES_ARM		RETURN "JIPMP_ARM"		//Join Arm Wrestling
				CASE QUICKMATCH_ACTIVITIES_DARTS 	RETURN "JIPMP_DARTS"	//Join Darts	
				CASE QUICKMATCH_ACTIVITIES_GOLF		RETURN "JIPMP_GOLF"		//Join Golf
				CASE QUICKMATCH_ACTIVITIES_RANGE 	RETURN "JIPMP_RANG"		//Join Range
				CASE QUICKMATCH_ACTIVITIES_TENNIS	RETURN "JIPMP_TEN"		//Join Tennis
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC BOOL IS_ANY_MODE_AVAILABLE()		
	IF IS_FM_TYPE_UNLOCKED(FMMC_TYPE_RACE)
	OR IS_FM_TYPE_UNLOCKED(FMMC_TYPE_DEATHMATCH)
	OR IS_FM_TYPE_UNLOCKED(FMMC_TYPE_MISSION)
	OR IS_FM_TYPE_UNLOCKED(FMMC_TYPE_SURVIVAL)
	OR IS_FM_TYPE_UNLOCKED(FMMC_TYPE_BASE_JUMP)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC


//Find out if any of my pals are in the session
FUNC BOOL ARE_ANY_FRIENDS_INT_THIS_SESSION()
	//If it's a solo session or we are off line then no!
	IF IS_THIS_A_SOLO_SESSION()
	OR HAS_ENTERED_OFFLINE_SAVE_FM()
	OR g_sMPTunables.bDisableQuickJobWithFriends
	OR g_sMPTunables.bOnCallFriendsDisabled
	OR (m_currentMode = QUICKMATCH_MODE_SERIES AND m_SeriesList[iSelectedContent] = ENUM_TO_INT(QUICKMATCH_SERIES_ACTIVE_PREMIUM_RACE))
	OR (m_currentMode = QUICKMATCH_MODE_CONTACT_MISSIONS AND m_ContactMissionsList[iSelectedContent]  = ENUM_TO_INT(QUICKMATCH_CONTACT_MISSIONS_SPECIAL_VEHICLE))
	OR (m_currentMode = QUICKMATCH_MODE_CONTACT_MISSIONS AND m_ContactMissionsList[iSelectedContent]  = ENUM_TO_INT(QUICKMATCH_CONTACT_MISSIONS_MOBILE_OPERATION))
	OR (m_currentMode = QUICKMATCH_MODE_CONTACT_MISSIONS AND m_ContactMissionsList[iSelectedContent]  = ENUM_TO_INT(QUICKMATCH_CONTACT_MISSIONS_AUTO_SHOP))
	OR (m_currentMode = QUICKMATCH_MODE_CONTACT_MISSIONS AND m_ContactMissionsList[iSelectedContent]  = ENUM_TO_INT(QUICKMATCH_CONTACT_MISSIONS_SHORT_TRIP))
	OR (m_currentMode = QUICKMATCH_MODE_HEISTS AND m_HeistsList[iSelectedContent]  = ENUM_TO_INT(QUICKMATCH_HEISTS_DIAMOND_CASINO))
	#IF FEATURE_HEIST_ISLAND
	OR (m_currentMode = QUICKMATCH_MODE_HEISTS AND m_HeistsList[iSelectedContent]  = ENUM_TO_INT(QUICKMATCH_HEISTS_CAYO_PERICO))
	#ENDIF
		#IF IS_DEBUG_BUILD
		IF IS_THIS_A_SOLO_SESSION()
			PRINTLN("appJIPMP - ARE_ANY_FRIENDS_INT_THIS_SESSION - IS_THIS_A_SOLO_SESSION!" )
		ELIF g_sMPTunables.bDisableQuickJobWithFriends
			PRINTLN("appJIPMP - ARE_ANY_FRIENDS_INT_THIS_SESSION - g_sMPTunables.bDisableQuickJobWithFriends = TRUE" )
		ELIF g_sMPTunables.bOnCallFriendsDisabled
			PRINTLN("appJIPMP - ARE_ANY_FRIENDS_INT_THIS_SESSION - g_sMPTunables.bOnCallFriendsDisabled = TRUE" )
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	INT iLoop
	GAMER_HANDLE gamerHandle
	PLAYER_INDEX tempPlayer
	//Loop all and check if there is at leats one pal that is free!
	REPEAT NUM_NETWORK_PLAYERS iLoop 
		tempPlayer = INT_TO_PLAYERINDEX(iLoop)
		IF NETWORK_IS_PLAYER_ACTIVE(tempPlayer)
		AND tempPlayer != PLAYER_ID()
			gamerHandle = GET_GAMER_HANDLE_PLAYER(tempPlayer)
			//Are the a friend
			IF NETWORK_IS_FRIEND(gamerHandle)
				//Are they free!
				IF IS_THIS_PLAYER_FREE_TO_PLAY(tempPlayer)
					PRINTLN("appJIPMP - ARE_ANY_FRIENDS_INT_THIS_SESSION - ", GET_PLAYER_NAME(tempPlayer), " is a friend" )
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT	
	//No friends!
	PRINTLN("appJIPMP - ARE_ANY_FRIENDS_INT_THIS_SESSION - NO!" )
	RETURN FALSE
ENDFUNC

//Returns TRUE if all the heists have been compleated
FUNC BOOL HAVE_COMPLEATED_ALL_HEIST_MISSIONS_ON_CALL()
	//If this feature is disabled then no
	IF g_sMPTunables.bOnCallHeistAllUnlockedDisabled
		RETURN FALSE
	ENDIF
	
	IF HAVE_COMPLEATED_ALL_HEIST_MISSIONS()
		RETURN TRUE
	ENDIF
	
	//ir check the debug widgets
	#IF IS_DEBUG_BUILD 
	IF g_sOnCallHesitDebug.bPassedFleeca
	AND g_sOnCallHesitDebug.bPassedPrison
	AND g_sOnCallHesitDebug.bPassedHumane
	AND g_sOnCallHesitDebug.bPassedSeriesA
	AND g_sOnCallHesitDebug.bPassedPacific 
		RETURN TRUE
	ENDIF
	#ENDIF
	//No
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_MODE_AVAILABLE(enumQuickMatchModes eMode)
	SWITCH eMode
		CASE QUICKMATCH_MODE_SERIES
			RETURN TRUE
		BREAK
		
		CASE QUICKMATCH_MODE_HEISTS
			RETURN TRUE
		BREAK
		
		CASE QUICKMATCH_MODE_CONTACT_MISSIONS
			RETURN TRUE
		BREAK
		
		CASE QUICKMATCH_MODE_JOBS
			RETURN TRUE
		BREAK
		
		CASE QUICKMATCH_MODE_ACTIVITIES
			IF g_sMPTunables.bOnCallMiniGamesDisabled
				RETURN FALSE
			ELSE
				RETURN IS_FM_TYPE_UNLOCKED(FMMC_TYPE_MG_ARM_WRESTLING)
			ENDIF
		BREAK
		
		CASE QUICKMATCH_MODE_PLAYLIST
			IF IS_FM_TYPE_UNLOCKED(FMMC_TYPE_RACE)
			AND IS_FM_TYPE_UNLOCKED(FMMC_TYPE_DEATHMATCH)
			AND IS_FM_TYPE_UNLOCKED(FMMC_TYPE_MISSION)
			AND IS_FM_TYPE_UNLOCKED(FMMC_TYPE_SURVIVAL)
			AND IS_FM_TYPE_UNLOCKED(FMMC_TYPE_BASE_JUMP)
			AND NOT IS_THIS_A_SOLO_SESSION()
			AND NOT HAS_ENTERED_OFFLINE_SAVE_FM()
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE QUICKMATCH_MODE_RANDOM
			IF IS_ANY_MODE_AVAILABLE()
				IF IS_THIS_A_SOLO_SESSION()
				OR HAS_ENTERED_OFFLINE_SAVE_FM()
					RETURN FALSE //Block this option by passing it back as unavailable.
				ELSE
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK	
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_CONTENT_AVAILABLE(enumQuickMatchModes eMode,INT index)   
	INT iPlayList
	SWITCH eMode
		CASE QUICKMATCH_MODE_SERIES
			SWITCH INT_TO_ENUM(enumQuickMatchSeries,index)
				CASE QUICKMATCH_SERIES_FEATURED
					IF IS_THIS_A_SOLO_SESSION()
					OR g_FMMC_ROCKSTAR_CREATED.iTypeCount[FMMC_TYPE_MISSION_VS_NEW_TYPE_COUNT] = 0
					OR HAS_ENTERED_OFFLINE_SAVE_FM()
						PRINTLN(" [NEWVS] - QUICKMATCH_MODE_MISSION_NEW_VS - RETURN FALSE")
						RETURN FALSE
					ELSE
						RETURN IS_FM_TYPE_UNLOCKED(FMMC_TYPE_MISSION_VS) OR IS_FM_TYPE_UNLOCKED(FMMC_TYPE_MISSION)
					ENDIF
				BREAK
				
				CASE QUICKMATCH_SERIES_COMMUNITY
					iPlayList =  CV2_GET_SERIES_ARRAY_POS_BY_TYPE(FMMC_TYPE_COMMUNITY_SERIES)
					PRINTLN("[TS][CV2] iPlayList = ", iPlayList)
					#IF IS_DEBUG_BUILD
					IF iPlayList != -1
						PRINTLN("[TS][CV2] g_sV2CoronaVars.iCurrentArrayPosProfessional[", iPlayList, "] = ", g_sV2CoronaVars.iCurrentArrayPosProfessional[iPlayList])
					ENDIF
					#ENDIF
					IF IS_THIS_A_SOLO_SESSION() //#1618182 - block this option is solo session returns true
					OR HAS_ENTERED_OFFLINE_SAVE_FM()
					OR iPlayList = -1 
					OR g_sV2CoronaVars.iCurrentArrayPosProfessional[iPlayList] = -1
						RETURN FALSE //Block this option by passing it back as unavailable.
					ELSE
						RETURN IS_FM_TYPE_UNLOCKED(FMMC_TYPE_RACE) OR IS_FM_TYPE_UNLOCKED(FMMC_TYPE_MISSION)
					ENDIF
				BREAK
				
				CASE QUICKMATCH_SERIES_CAYO_PERICO
					iPlayList =  CV2_GET_SERIES_ARRAY_POS_BY_TYPE(FMMC_TYPE_CAYO_PERICO_SERIES)
					PRINTLN("[TS][CV2] iPlayList = ", iPlayList)
					#IF IS_DEBUG_BUILD
					IF iPlayList != -1
						PRINTLN("[TS][CV2] g_sV2CoronaVars.iCurrentArrayPosProfessional[", iPlayList, "] = ", g_sV2CoronaVars.iCurrentArrayPosProfessional[iPlayList])
					ENDIF
					#ENDIF
					IF IS_THIS_A_SOLO_SESSION() //#1618182 - block this option is solo session returns true
					OR HAS_ENTERED_OFFLINE_SAVE_FM()
					OR iPlayList = -1 
					OR g_sV2CoronaVars.iCurrentArrayPosProfessional[iPlayList] = -1
						RETURN FALSE //Block this option by passing it back as unavailable.
					ELSE
						RETURN IS_FM_TYPE_UNLOCKED(FMMC_TYPE_RACE) OR IS_FM_TYPE_UNLOCKED(FMMC_TYPE_MISSION)
					ENDIF
				BREAK
				
				#IF FEATURE_GEN9_EXCLUSIVE
				CASE QUICKMATCH_SERIES_HSW_RACE
					iPlayList =  CV2_GET_SERIES_ARRAY_POS_BY_TYPE(FMMC_TYPE_HSW_RACE_SERIES)
					PRINTLN("[TS][CV2] iPlayList = ", iPlayList)
					#IF IS_DEBUG_BUILD
					IF iPlayList != -1
						PRINTLN("[TS][CV2] g_sV2CoronaVars.iCurrentArrayPosProfessional[", iPlayList, "] = ", g_sV2CoronaVars.iCurrentArrayPosProfessional[iPlayList])
					ENDIF
					#ENDIF
					IF IS_THIS_A_SOLO_SESSION() //#1618182 - block this option is solo session returns true
					OR HAS_ENTERED_OFFLINE_SAVE_FM()
					OR iPlayList = -1 
					OR g_sV2CoronaVars.iCurrentArrayPosProfessional[iPlayList] = -1
					OR NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_HSW_RACE_DONE)
						RETURN FALSE //Block this option by passing it back as unavailable.
					ELSE
						RETURN IS_FM_TYPE_UNLOCKED(FMMC_TYPE_RACE)
					ENDIF
				BREAK
				#ENDIF
				
				CASE QUICKMATCH_SERIES_STREET_RACE		
					iPlayList =  CV2_GET_SERIES_ARRAY_POS_BY_TYPE(FMMC_TYPE_STREET_RACE_SERIES)
					PRINTLN("[TS][CV2] iPlayList = ", iPlayList)
					#IF IS_DEBUG_BUILD
					IF iPlayList != -1
						PRINTLN("[TS][CV2] g_sV2CoronaVars.iCurrentArrayPosProfessional[", iPlayList, "] = ", g_sV2CoronaVars.iCurrentArrayPosProfessional[iPlayList])
					ENDIF
					#ENDIF
					IF IS_THIS_A_SOLO_SESSION() //#1618182 - block this option is solo session returns true
					OR HAS_ENTERED_OFFLINE_SAVE_FM()
					OR iPlayList = -1 
					OR g_sV2CoronaVars.iCurrentArrayPosProfessional[iPlayList] = -1
					OR NOT HAS_PLAYER_PURCHASED_CAR_CLUB_MEMBERSHIP(PLAYER_ID())
						RETURN FALSE //Block this option by passing it back as unavailable.
					ELSE
						RETURN IS_FM_TYPE_UNLOCKED(FMMC_TYPE_RACE)
					ENDIF
				BREAK
				
				CASE QUICKMATCH_SERIES_PURSUIT
					iPlayList =  CV2_GET_SERIES_ARRAY_POS_BY_TYPE(FMMC_TYPE_PURSUIT_SERIES)
					PRINTLN("[TS][CV2] iPlayList = ", iPlayList)
					#IF IS_DEBUG_BUILD
					IF iPlayList != -1
						PRINTLN("[TS][CV2] g_sV2CoronaVars.iCurrentArrayPosProfessional[", iPlayList, "] = ", g_sV2CoronaVars.iCurrentArrayPosProfessional[iPlayList])
					ENDIF
					#ENDIF
					IF IS_THIS_A_SOLO_SESSION() //#1618182 - block this option is solo session returns true
					OR HAS_ENTERED_OFFLINE_SAVE_FM()
					OR iPlayList = -1 
					OR g_sV2CoronaVars.iCurrentArrayPosProfessional[iPlayList] = -1
					OR NOT HAS_PLAYER_PURCHASED_CAR_CLUB_MEMBERSHIP(PLAYER_ID())
					OR NOT HAS_LOCAL_PLAYER_UNLOCKED_PURSUIT_SERIES()
						RETURN FALSE //Block this option by passing it back as unavailable.
					ELSE
						RETURN IS_FM_TYPE_UNLOCKED(FMMC_TYPE_RACE)
					ENDIF
				BREAK
				
				CASE QUICKMATCH_SERIES_OPEN_WHEEL
					iPlayList =  CV2_GET_SERIES_ARRAY_POS_BY_TYPE(FMMC_TYPE_OPEN_WHEEL_SERIES)
					PRINTLN("[TS][CV2] iPlayList = ", iPlayList)
					#IF IS_DEBUG_BUILD
					IF iPlayList != -1
						PRINTLN("[TS][CV2] g_sV2CoronaVars.iCurrentArrayPosProfessional[", iPlayList, "] = ", g_sV2CoronaVars.iCurrentArrayPosProfessional[iPlayList])
					ENDIF
					#ENDIF
					IF IS_THIS_A_SOLO_SESSION() //#1618182 - block this option is solo session returns true
					OR HAS_ENTERED_OFFLINE_SAVE_FM()
					OR iPlayList = -1 
					OR g_sV2CoronaVars.iCurrentArrayPosProfessional[iPlayList] = -1
						RETURN FALSE //Block this option by passing it back as unavailable.
					ELSE
						RETURN IS_FM_TYPE_UNLOCKED(FMMC_TYPE_RACE)
					ENDIF
				BREAK
				
				CASE QUICKMATCH_SERIES_RACE
					iPlayList =  CV2_GET_SERIES_ARRAY_POS_BY_TYPE(FMMC_TYPE_RACE_SERIES)
					PRINTLN("[TS][CV2] iPlayList = ", iPlayList)
					#IF IS_DEBUG_BUILD
					IF iPlayList != -1
						PRINTLN("[TS][CV2] g_sV2CoronaVars.iCurrentArrayPosProfessional[", iPlayList, "] = ", g_sV2CoronaVars.iCurrentArrayPosProfessional[iPlayList])
					ENDIF
					#ENDIF
					IF IS_THIS_A_SOLO_SESSION() //#1618182 - block this option is solo session returns true
					OR HAS_ENTERED_OFFLINE_SAVE_FM()
					OR iPlayList = -1 
					OR g_sV2CoronaVars.iCurrentArrayPosProfessional[iPlayList] = -1
						RETURN FALSE //Block this option by passing it back as unavailable.
					ELSE
						RETURN IS_FM_TYPE_UNLOCKED(FMMC_TYPE_RACE)
					ENDIF
				BREAK
				
				CASE QUICKMATCH_SERIES_SURVIVAL	
					iPlayList =  CV2_GET_SERIES_ARRAY_POS_BY_TYPE(FMMC_TYPE_SURVIVAL_SERIES)
					PRINTLN("[TS][CV2] iPlayList = ", iPlayList)
					#IF IS_DEBUG_BUILD
					IF iPlayList != -1
						PRINTLN("[TS][CV2] g_sV2CoronaVars.iCurrentArrayPosProfessional[", iPlayList, "] = ", g_sV2CoronaVars.iCurrentArrayPosProfessional[iPlayList])
					ENDIF
					#ENDIF
					IF IS_THIS_A_SOLO_SESSION() //#1618182 - block this option is solo session returns true
					OR HAS_ENTERED_OFFLINE_SAVE_FM()
					OR iPlayList = -1 
					OR g_sV2CoronaVars.iCurrentArrayPosProfessional[iPlayList] = -1
						RETURN FALSE //Block this option by passing it back as unavailable.
					ELSE
						RETURN IS_FM_TYPE_UNLOCKED(FMMC_TYPE_RACE)
					ENDIF
				BREAK
				
				CASE QUICKMATCH_SERIES_ARENA_WAR
					iPlayList =  CV2_GET_SERIES_ARRAY_POS_BY_TYPE(FMMC_TYPE_ARENA_SERIES)
					PRINTLN("[TS][CV2] iPlayList = ", iPlayList)
					#IF IS_DEBUG_BUILD
					IF iPlayList != -1
						PRINTLN("[TS][CV2] g_sV2CoronaVars.iCurrentArrayPosProfessional[", iPlayList, "] = ", g_sV2CoronaVars.iCurrentArrayPosProfessional[iPlayList])
					ENDIF
					#ENDIF
					IF IS_THIS_A_SOLO_SESSION() //#1618182 - block this option is solo session returns true
					OR HAS_ENTERED_OFFLINE_SAVE_FM()
					OR iPlayList = -1 
					OR g_sV2CoronaVars.iCurrentArrayPosProfessional[iPlayList] = -1
						RETURN FALSE //Block this option by passing it back as unavailable.
					ELSE
						RETURN IS_FM_TYPE_UNLOCKED(FMMC_TYPE_RACE)
					ENDIF
				BREAK
				
				CASE QUICKMATCH_SERIES_SPECIAL_RACE
					iPlayList = CV2_GET_SERIES_ARRAY_POS_BY_TYPE(FMMC_TYPE_SPECIAL_VEHICLE_RACE_SERIES)
					PRINTLN("[TS][CV2] iPlayList = ", iPlayList)
					#IF IS_DEBUG_BUILD
					IF iPlayList != -1
						PRINTLN("[TS][CV2] g_sV2CoronaVars.iCurrentArrayPosProfessional[", iPlayList, "] = ", g_sV2CoronaVars.iCurrentArrayPosProfessional[iPlayList])
					ENDIF
					#ENDIF
					IF IS_THIS_A_SOLO_SESSION() //#1618182 - block this option is solo session returns true
					OR HAS_ENTERED_OFFLINE_SAVE_FM()
					OR iPlayList = -1 
					OR g_sV2CoronaVars.iCurrentArrayPosProfessional[iPlayList] = -1
						RETURN FALSE //Block this option by passing it back as unavailable.
					ELSE
						RETURN IS_FM_TYPE_UNLOCKED(FMMC_TYPE_RACE)
					ENDIF
				BREAK
				
				CASE QUICKMATCH_SERIES_SUPER_SPORT	
					iPlayList =  CV2_GET_SERIES_ARRAY_POS_BY_TYPE(FMMC_TYPE_HOTRING_SERIES)
					PRINTLN("[TS][CV2] iPlayList = ", iPlayList)
					#IF IS_DEBUG_BUILD
					IF iPlayList != -1
						PRINTLN("[TS][CV2] g_sV2CoronaVars.iCurrentArrayPosProfessional[", iPlayList, "] = ", g_sV2CoronaVars.iCurrentArrayPosProfessional[iPlayList])
					ENDIF
					#ENDIF
					IF IS_THIS_A_SOLO_SESSION() //#1618182 - block this option is solo session returns true
					OR HAS_ENTERED_OFFLINE_SAVE_FM()
					OR iPlayList = -1 
					OR g_sV2CoronaVars.iCurrentArrayPosProfessional[iPlayList] = -1
						RETURN FALSE //Block this option by passing it back as unavailable.
					ELSE
						RETURN IS_FM_TYPE_UNLOCKED(FMMC_TYPE_RACE)
					ENDIF
				BREAK
				
				CASE QUICKMATCH_SERIES_TRANSFORM
					iPlayList =  CV2_GET_SERIES_ARRAY_POS_BY_TYPE(FMMC_TYPE_TRANSFORM_SERIES)
					PRINTLN("[TS][CV2] iPlayList = ", iPlayList)
					#IF IS_DEBUG_BUILD
					IF iPlayList != -1
						PRINTLN("[TS][CV2] g_sV2CoronaVars.iCurrentArrayPosProfessional[", iPlayList, "] = ", g_sV2CoronaVars.iCurrentArrayPosProfessional[iPlayList])
					ENDIF
					#ENDIF
					IF IS_THIS_A_SOLO_SESSION() //#1618182 - block this option is solo session returns true
					OR HAS_ENTERED_OFFLINE_SAVE_FM()
					OR iPlayList = -1 
					OR g_sV2CoronaVars.iCurrentArrayPosProfessional[iPlayList] = -1
						RETURN FALSE //Block this option by passing it back as unavailable.
					ELSE
						RETURN IS_FM_TYPE_UNLOCKED(FMMC_TYPE_RACE)
					ENDIF
				BREAK
				
				CASE QUICKMATCH_SERIES_BUNKER
					iPlayList =  CV2_GET_SERIES_ARRAY_POS_BY_TYPE(FMMC_TYPE_BUNKER_SERIES)
					PRINTLN("[TS][CV2] iPlayList = ", iPlayList)
					#IF IS_DEBUG_BUILD
					IF iPlayList != -1
						PRINTLN("[TS][CV2] g_sV2CoronaVars.iCurrentArrayPosProfessional[", iPlayList, "] = ", g_sV2CoronaVars.iCurrentArrayPosProfessional[iPlayList])
					ENDIF
					#ENDIF
					IF IS_THIS_A_SOLO_SESSION() //#1618182 - block this option is solo session returns true
					OR HAS_ENTERED_OFFLINE_SAVE_FM()
					OR iPlayList = -1 
					OR g_sV2CoronaVars.iCurrentArrayPosProfessional[iPlayList] = -1
						RETURN FALSE //Block this option by passing it back as unavailable.
					ELSE
						RETURN IS_FM_TYPE_UNLOCKED(FMMC_TYPE_RACE)
					ENDIF
				BREAK
				
				CASE QUICKMATCH_SERIES_STUNT
					IF IS_THIS_A_SOLO_SESSION() //#1618182 - block this option is solo session returns true
					OR HAS_ENTERED_OFFLINE_SAVE_FM()
					OR NOT g_sMPTunables.bFmCoronaPlaylistActive
						RETURN FALSE //Block this option by passing it back as unavailable.
					ELSE
						RETURN IS_FM_TYPE_UNLOCKED(FMMC_TYPE_RACE)
					ENDIF
				BREAK
				
				CASE QUICKMATCH_SERIES_ADVERSARY
					IF IS_THIS_A_SOLO_SESSION() //#1618182 - block this option is solo session returns true
					OR HAS_ENTERED_OFFLINE_SAVE_FM()
					OR CV2_GET_ADVERSARY_SERIES_ARRAY_POS() = -1
					OR g_sV2CoronaVars.iCurrentArrayPosProfessional[iPlayList] = -1
						RETURN FALSE //Block this option by passing it back as unavailable.
					ELSE
						RETURN IS_FM_TYPE_UNLOCKED(FMMC_TYPE_RACE)
					ENDIF
				BREAK
				
				CASE QUICKMATCH_SERIES_ACTIVE_PREMIUM_RACE
					IF IS_THIS_A_SOLO_SESSION() //#1618182 - block this option is solo session returns true
					OR HAS_ENTERED_OFFLINE_SAVE_FM()
					OR NOT g_sMPTunables.bFmCoronaProfessionalPlaylistActive
					OR g_sMPTunables.iProfesionalCoronaType[CV2_GET_RACE_ARRAY_POS()] != FMMC_TYPE_RACE
					//Not set up at all
					OR g_sV2CoronaVars.iVehicleClassCount = ciCV2_NOT_SET_UP_PROFESSIONAL_RACE
						RETURN FALSE //Block this option by passing it back as unavailable.
					ELSE
						RETURN IS_FM_TYPE_UNLOCKED(FMMC_TYPE_RACE)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		
		CASE QUICKMATCH_MODE_HEISTS
			SWITCH INT_TO_ENUM(enumQuickMatchHeists,index)
				#IF FEATURE_HEIST_ISLAND
					CASE QUICKMATCH_HEISTS_CAYO_PERICO
					
						IF IS_THIS_A_SOLO_SESSION() //#1618182 - block this option is solo session returns true
						OR HAS_ENTERED_OFFLINE_SAVE_FM()
						OR g_sMPTunables.bDisableCasinoHeistPhone
							RETURN FALSE 
						ELSE
							RETURN IS_FM_TYPE_UNLOCKED(FMMC_TYPE_MISSION)
						ENDIF
					BREAK
				#ENDIF
				
				
				CASE QUICKMATCH_HEISTS_DIAMOND_CASINO
					IF IS_THIS_A_SOLO_SESSION() //#1618182 - block this option is solo session returns true
					OR HAS_ENTERED_OFFLINE_SAVE_FM()
					OR g_sMPTunables.bDisableCasinoHeistPhone
						RETURN FALSE 
					ELSE
						RETURN IS_FM_TYPE_UNLOCKED(FMMC_TYPE_MISSION)
					ENDIF
				BREAK
				
				CASE QUICKMATCH_HEISTS_DOOMSDAY
					IF IS_THIS_A_SOLO_SESSION() //#1618182 - block this option is solo session returns true
					OR HAS_ENTERED_OFFLINE_SAVE_FM()
					//Or it's disabled
					OR g_sMPTunables.bOnCallHeistDisabled = TRUE
						RETURN FALSE //Block this option by passing it back as unavailable.
					ELSE
						RETURN IS_FM_TYPE_UNLOCKED(FMMC_TYPE_MISSION)
					ENDIF
				BREAK
				
				CASE QUICKMATCH_HEISTS_HEIST
					IF IS_THIS_A_SOLO_SESSION() //#1618182 - block this option is solo session returns true
					OR HAS_ENTERED_OFFLINE_SAVE_FM()
					//Or it's disabled
					OR g_sMPTunables.bOnCallHeistDisabled = TRUE
						RETURN FALSE //Block this option by passing it back as unavailable.
					ELSE
						RETURN IS_FM_TYPE_UNLOCKED(FMMC_TYPE_MISSION)
//						RETURN IS_MP_HEIST_STRAND_ACTIVE() OR HAVE_COMPLEATED_ALL_HEIST_MISSIONS_ON_CALL() OR  HAS_LESTER_INTRO_TO_HEISTS_CUTSCENE_BEEN_DONE()
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		
		CASE QUICKMATCH_MODE_CONTACT_MISSIONS
			SWITCH INT_TO_ENUM(enumQuickMatchContactMissions,index)
				CASE QUICKMATCH_CONTACT_MISSIONS_SHORT_TRIP
					IF IS_THIS_A_SOLO_SESSION()
					OR HAS_ENTERED_OFFLINE_SAVE_FM()
						RETURN FALSE 
					ELSE
						RETURN IS_FM_TYPE_UNLOCKED(FMMC_TYPE_MISSION)
					ENDIF
				BREAK
				
				CASE QUICKMATCH_CONTACT_MISSIONS_AUTO_SHOP
					IF IS_THIS_A_SOLO_SESSION()
					OR HAS_ENTERED_OFFLINE_SAVE_FM()
						RETURN FALSE 
					ELSE
						RETURN IS_FM_TYPE_UNLOCKED(FMMC_TYPE_MISSION)
					ENDIF
				BREAK
				
				CASE QUICKMATCH_CONTACT_MISSIONS_CONTACT
					IF IS_THIS_A_SOLO_SESSION()
					OR SHOULD_USE_OFFLINE_UGC_CONTENT()
					OR g_FMMC_ROCKSTAR_CREATED.iTypeCount[FMMC_TYPE_MISSION_CONTACT] = 0
					OR HAS_ENTERED_OFFLINE_SAVE_FM()
						RETURN FALSE
					ELSE
						RETURN IS_FM_TYPE_UNLOCKED(FMMC_TYPE_MISSION)
					ENDIF
				BREAK
				
				CASE QUICKMATCH_CONTACT_MISSIONS_LAMAR
					IF IS_THIS_A_SOLO_SESSION()
					OR NOT HAVE_LOW_RIDER_FLOW_MISSIONS_DOWNLOADED()
					OR HAS_ENTERED_OFFLINE_SAVE_FM()
					OR g_sMPTunables.bLOWRIDER_DISABLE_ON_CALL_LAMAR_STRAND
						PRINTLN(" [NEWVS] - QUICKMATCH_MODE_MISSION_FLOW - RETURN FALSE")
						RETURN FALSE
					ELSE
						RETURN (GET_FM_STRAND_PROGRESS(ciFLOW_STRAND_LOW_RIDER) > 0) OR FM_LOW_FLOW_HAS_BEEN_COMPLETED()
					ENDIF
				BREAK
				
				CASE QUICKMATCH_CONTACT_MISSIONS_MOBILE_OPERATION
					IF IS_THIS_A_SOLO_SESSION() //#1618182 - block this option is solo session returns true
					OR HAS_ENTERED_OFFLINE_SAVE_FM()
					OR g_sMPTunables.bDisableSvmPhone
						RETURN FALSE 
					ELSE
						RETURN TRUE //SVM_FLOW_GET_NEXT_AVAILABLE_MISSION() = ciSVM_FLOW_MISSION_MAX 
					ENDIF
				BREAK
				
				CASE QUICKMATCH_CONTACT_MISSIONS_SPECIAL_VEHICLE
					IF IS_THIS_A_SOLO_SESSION() //#1618182 - block this option is solo session returns true
					OR HAS_ENTERED_OFFLINE_SAVE_FM()
					OR g_sMPTunables.bDisableSvmPhone
						RETURN FALSE 
					ELSE
						RETURN TRUE //SVM_FLOW_GET_NEXT_AVAILABLE_MISSION() = ciSVM_FLOW_MISSION_MAX 
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		
		CASE QUICKMATCH_MODE_ACTIVITIES
			SWITCH INT_TO_ENUM(enumQuickMatchActivities,index)
				CASE QUICKMATCH_ACTIVITIES_ARM
					IF IS_THIS_A_SOLO_SESSION()
					OR HAS_ENTERED_OFFLINE_SAVE_FM()
					#IF IS_DEBUG_BUILD
					OR GET_COMMANDLINE_PARAM_EXISTS("sc_ForceSoloMG")
					#ENDIF
						RETURN FALSE
					ELSE
						RETURN TRUE
					ENDIF
				BREAK
				
				CASE QUICKMATCH_ACTIVITIES_TENNIS
					IF IS_THIS_A_SOLO_SESSION()
					OR HAS_ENTERED_OFFLINE_SAVE_FM()
					#IF IS_DEBUG_BUILD
					OR GET_COMMANDLINE_PARAM_EXISTS("sc_ForceSoloMG")
					#ENDIF
						RETURN FALSE
					ELSE
						RETURN TRUE
					ENDIF
				BREAK
				
				CASE QUICKMATCH_ACTIVITIES_DARTS
					RETURN TRUE
				BREAK
				CASE QUICKMATCH_ACTIVITIES_GOLF	
					RETURN TRUE
				BREAK
				CASE QUICKMATCH_ACTIVITIES_RANGE
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		
		CASE QUICKMATCH_MODE_JOBS
			SWITCH INT_TO_ENUM(enumQuickMatchJobs,index)
				CASE QUICKMATCH_JOBS_CAPTURE
					IF IS_THIS_A_SOLO_SESSION()
					OR g_FMMC_ROCKSTAR_CREATED.iTypeCount[FMMC_TYPE_MISSION_CTF] = 0
					OR HAS_ENTERED_OFFLINE_SAVE_FM()
						RETURN FALSE
					ELSE
						RETURN IS_FM_TYPE_UNLOCKED(FMMC_TYPE_MISSION_CTF)
					ENDIF
				BREAK
				
				CASE QUICKMATCH_JOBS_DEATHMATCH
					IF IS_THIS_A_SOLO_SESSION() //#1618182 - block this option is solo session returns true
					OR HAS_ENTERED_OFFLINE_SAVE_FM()
						RETURN FALSE //Block this option by passing it back as unavailable.
					ELSE
						RETURN IS_FM_TYPE_UNLOCKED(FMMC_TYPE_DEATHMATCH)
					ENDIF
				BREAK
				
				CASE QUICKMATCH_JOBS_LTS
					IF IS_THIS_A_SOLO_SESSION()
					OR g_FMMC_ROCKSTAR_CREATED.iTypeCount[FMMC_TYPE_MISSION_LTS] = 0
					OR HAS_ENTERED_OFFLINE_SAVE_FM()
						RETURN FALSE
					ELSE
						RETURN IS_FM_TYPE_UNLOCKED(FMMC_TYPE_MISSION)
					ENDIF
				BREAK
				
				CASE QUICKMATCH_JOBS_PARACHUTING
					IF g_FMMC_ROCKSTAR_CREATED.iTypeCount[FMMC_TYPE_BASE_JUMP] = 0
						RETURN FALSE
					ELSE
						RETURN IS_FM_TYPE_UNLOCKED(FMMC_TYPE_BASE_JUMP)
					ENDIF
				BREAK
				
				CASE QUICKMATCH_JOBS_RACE	
					RETURN IS_FM_TYPE_UNLOCKED(FMMC_TYPE_RACE)
				BREAK
				
				CASE QUICKMATCH_JOBS_SURVIVAL
					RETURN IS_FM_TYPE_UNLOCKED(FMMC_TYPE_SURVIVAL)
				BREAK
				
				CASE QUICKMATCH_JOBS_VERSUS
					IF IS_THIS_A_SOLO_SESSION()
					OR g_FMMC_ROCKSTAR_CREATED.iTypeCount[FMMC_TYPE_MISSION_VS] = 0
					OR HAS_ENTERED_OFFLINE_SAVE_FM()
						RETURN FALSE
					ELSE
						RETURN IS_FM_TYPE_UNLOCKED(FMMC_TYPE_MISSION)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		
	ENDSWITCH
	
	RETURN FALSE

ENDFUNC

PROC Fill_QuickMatch_Screen_Slots()

	INT imode

	slotsUsed = 0
	//iCanDoQuickMatch = 99 //comment in to quickly test no modes available.

	IF iCanDoQuickmatch = 0
	AND IS_ANY_MODE_AVAILABLE()
		REPEAT ENUM_TO_INT(QUICKMATCH_MODE_MAX) imode 
			IF IS_MODE_AVAILABLE(INT_TO_ENUM( enumQuickMatchModes,imode))
				// Store the details in the Scaleform slot
				BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieIndex, "SET_DATA_SLOT")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(18)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(slotsUsed)
				IF imode = ENUM_TO_INT(QUICKMATCH_MODE_SERIES)
					PRINTLN("appJIPMP - imode = QUICKMATCH_MODE_SERIES - slotsUsed = ", slotsUsed)
				ENDIF
				IF imode = ENUM_TO_INT(QUICKMATCH_MODE_ACTIVITIES)
					PRINTLN("appJIPMP - imode = QUICKMATCH_MODE_ACTIVITIES - slotsUsed = ", slotsUsed)
				ENDIF
				IF imode = ENUM_TO_INT(QUICKMATCH_MODE_HEISTS)
					PRINTLN("appJIPMP - imode = QUICKMATCH_MODE_HEISTS - slotsUsed = ", slotsUsed)
				ENDIF
				IF imode = ENUM_TO_INT(QUICKMATCH_MODE_CONTACT_MISSIONS)
					PRINTLN("appJIPMP - imode = QUICKMATCH_MODE_CONTACT_MISSIONS - slotsUsed = ", slotsUsed)
				ENDIF
				IF imode = ENUM_TO_INT(QUICKMATCH_MODE_JOBS)
					PRINTLN("appJIPMP - imode = QUICKMATCH_MODE_JOBS - slotsUsed = ", slotsUsed)
				ENDIF
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
				BEGIN_TEXT_COMMAND_SCALEFORM_STRING(GET_MODE_TEXT(INT_TO_ENUM(enumQuickMatchModes,imode)))
				END_TEXT_COMMAND_SCALEFORM_STRING()
				END_SCALEFORM_MOVIE_METHOD()
				m_ModeList[slotsUsed] = imode
				// Increment slots used
				slotsUsed++
				PRINTLN("appJIPMP - slots used to fill QuickJoin  ", slotsUsed, " name = ", GET_FILENAME_FOR_AUDIO_CONVERSATION(GET_MODE_TEXT(INT_TO_ENUM(enumQuickMatchModes,imode))))
			ELSE
				PRINTLN("appJIPMP - ", GET_FILENAME_FOR_AUDIO_CONVERSATION(GET_MODE_TEXT(INT_TO_ENUM(enumQuickMatchModes,imode)))," NOT AVAILABLE")
			ENDIF
			
		ENDREPEAT
	ELSE
		BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieIndex, "SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(18)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(slotsUsed)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("JIPMP_NA")
		END_TEXT_COMMAND_SCALEFORM_STRING()
		END_SCALEFORM_MOVIE_METHOD()
		PRINTLN("appJIPMP - No quick jobs available, so slots used to fill QuickJoin app is at ", slotsUsed)
	ENDIF
ENDPROC

PROC STORE_SUB_LIST_DETAILS_IN_SCALEFORM_SLOT(enumQuickMatchModes eMode,INT &iList[],INT index)
// Store the details in the Scaleform slot
	BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieIndex, "SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(18)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(slotsUsedContent)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING(GET_CONTENT_TEXT(eMode,index))
		END_TEXT_COMMAND_SCALEFORM_STRING()
	END_SCALEFORM_MOVIE_METHOD()
	iList[slotsUsedContent] = index
	// Increment slots used
	slotsUsedContent++
	PRINTLN("appJIPMP - slotsUsedContent                 = ", slotsUsedContent, " Name = ", GET_FILENAME_FOR_AUDIO_CONVERSATION(GET_CONTENT_TEXT(eMode,index)))
	PRINTLN("appJIPMP - iList[slotsUsedContent] = ", iList[index], " Name = ", GET_FILENAME_FOR_AUDIO_CONVERSATION(GET_CONTENT_TEXT(eMode,index)))
ENDPROC

PROC Fill_QuickMatch_Sub_List_Screen_Slots(enumQuickMatchModes eMode)
	INT index
	slotsUsedContent = 0
	IF iCanDoQuickmatch = 0
	AND IS_ANY_MODE_AVAILABLE()
		SWITCH eMode
			CASE QUICKMATCH_MODE_SERIES
				REPEAT ENUM_TO_INT(QUICKMATCH_SERIES_MAX) index
					IF IS_CONTENT_AVAILABLE(QUICKMATCH_MODE_SERIES,index)
					STORE_SUB_LIST_DETAILS_IN_SCALEFORM_SLOT(eMode,m_SeriesList,index)
					ENDIF
				ENDREPEAT
			BREAK
			
			CASE QUICKMATCH_MODE_ACTIVITIES
				REPEAT ENUM_TO_INT(QUICKMATCH_ACTIVITIES_MAX) index
					IF IS_CONTENT_AVAILABLE(QUICKMATCH_MODE_ACTIVITIES,index)
						STORE_SUB_LIST_DETAILS_IN_SCALEFORM_SLOT(eMode,m_ActivitiesList,index)
					ENDIF
				ENDREPEAT
			BREAK
			
			CASE QUICKMATCH_MODE_HEISTS
				REPEAT ENUM_TO_INT(QUICKMATCH_HEISTS_MAX) index
					IF IS_CONTENT_AVAILABLE(QUICKMATCH_MODE_HEISTS,index)
						STORE_SUB_LIST_DETAILS_IN_SCALEFORM_SLOT(eMode,m_HeistsList,index)
					ENDIF
				ENDREPEAT
			BREAK
			
			CASE QUICKMATCH_MODE_CONTACT_MISSIONS
				REPEAT ENUM_TO_INT(QUICKMATCH_CONTACT_MISSIONS_MAX) index
					IF IS_CONTENT_AVAILABLE(QUICKMATCH_MODE_CONTACT_MISSIONS,index)
						STORE_SUB_LIST_DETAILS_IN_SCALEFORM_SLOT(eMode,m_ContactMissionsList,index)
					ENDIF
				ENDREPEAT
			BREAK
			
			CASE QUICKMATCH_MODE_JOBS
				REPEAT ENUM_TO_INT(QUICKMATCH_JOBS_MAX) index
					IF IS_CONTENT_AVAILABLE(QUICKMATCH_MODE_JOBS,index)
						STORE_SUB_LIST_DETAILS_IN_SCALEFORM_SLOT(eMode,m_JobsList,index)
					ENDIF
				ENDREPEAT
			BREAK
		
		ENDSWITCH
	ELSE
		BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieIndex, "SET_DATA_SLOT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(18)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("JIPMP_NA")
			END_TEXT_COMMAND_SCALEFORM_STRING()
		END_SCALEFORM_MOVIE_METHOD()
		PRINTLN("appJIPMP - No quick jobs available, so slots used to fill QuickJoin app is at ", 0)	 
	ENDIF
ENDPROC

// PURPOSE: Display the JobList screen
//
// INPUT PARAMS:				paramDoUpdate		 [DEFAULT = TRUE] TRUE means update the data, FALSE means only display the already generated data
//									paramKeepHighlight	[DEFAULT = TRUE] TRUE means keep the highlighter position if the data hasn't changed, FALSE means always reset it to first position
PROC Display_First_QuickMatch_Screen()
	// The JobList page is the Apps first screen, so the phone should be in RunningApp mode so that it knows to return to the HomeScreen on 'BACK'.
	IF NOT (g_Cellphone.PhoneDS = PDS_RUNNINGAPP)
		IF g_CellPhone.PhoneDS > PDS_AWAY //Added as a precaution... 
		g_Cellphone.PhoneDS = PDS_RUNNINGAPP
		PRINTLN("STATE ASSIGNMENT 203. appJIPMP::Display_JobList_Screen() assigns PDS_RUNNINGAPP")
		ENDIF
	ENDIF
	 
	// Setup the highlighter initial position
	// Store the details from the JobList array in the JobList Screen slots

	LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieIndex,"SET_DATA_SLOT_EMPTY", QUICKMATCH_LIST_VIEW_STATE)
	LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", QUICKMATCH_LIST_VIEW_STATE,0,0,INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_208")	//Call...
	Fill_QuickMatch_Screen_Slots()
	LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", QUICKMATCH_LIST_VIEW_STATE)


	// Display the screen
	// NOTE: Must be after the slots have been filled

	PRINTLN("appJIPMP - Display_First_QuickMatch_Screen() - displaying MPJIPlist [view state: ", QUICKMATCH_LIST_VIEW_STATE, "]")

	// Display the Header
	Display_MP_App_Header(m_QuickMatchTextFM)

	IF slotsUsed = 0		//#1508749
		Display_MP_App_Buttons(MP_APP_ICON_BLANK, "", MP_APP_ICON_BLANK, "", MP_APP_ICON_BACK, m_QuickMatchIconTextBack, m_sAppData)	
	ELSE
		Display_MP_App_Buttons(MP_APP_ICON_YES, m_QuickMatchIconTextYes, MP_APP_ICON_BLANK, "", MP_APP_ICON_BACK, m_QuickMatchIconTextBack, m_sAppData)
	ENDIF

	// Now on the JobList screen
	m_currentStage = QUICKMATCH_MODE_LIST

ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Confirm screen
// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Display the common details of the Confirm screen
PROC Display_Confirm_Screen()
	// Use the Side Task 'tick' icon
	CONST_INT	CONFIRM_SLOT_ICON		 12
	
	//do the menu title
	IF m_currentMode = QUICKMATCH_MODE_HEISTS
	AND m_HeistsList[iSelectedContent] = ENUM_TO_INT(QUICKMATCH_HEISTS_HEIST) 
		Display_MP_App_Header("JIPMP_HEISTT")
		PRINTLN("Display_MP_App_Header(\"JIPMP_HEISTT\")")
	ELSE
		Display_MP_App_Header(m_QuickMatchTextFM)
		PRINTLN("Display_MP_App_Header(m_QuickMatchTextFM)")
	ENDIF

	// Delete all data from the Confirm View before filling it with new data
	LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieIndex, "SET_DATA_SLOT_EMPTY", (TO_FLOAT(CONFIRM_VIEW_STATE)))
	PRINTLN("appJIPMP - Display_Confirm_Screen() - clearing JIP [view state: ",CONFIRM_VIEW_STATE,"]")
	// Display 'Are you sure?" - there is only one slot
	LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(CONFIRM_VIEW_STATE)),(TO_FLOAT(0)), (TO_FLOAT(CONFIRM_SLOT_ICON)), INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, m_areYouSureText)
	// Display the screen
	// NOTE: Must be after the slots have been filled
	LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", (TO_FLOAT(CONFIRM_VIEW_STATE)))
	// For the confirmation screen, display 'Yes' and 'No' buttons
	Display_MP_App_Buttons(MP_APP_ICON_YES, m_QuickMatchIconTextYes, MP_APP_ICON_BLANK, "", MP_APP_ICON_NO, m_QuickMatchIconTextNO, m_sAppData)
								
ENDPROC

// PURPOSE: Display the common details of the Confirm screen
PROC Display_Friends_Or_On_Call_Screen()
	PRINTLN("appJIPMP - Display_Friends_Or_On_Call_Screen")
	bBlockAccept = FALSE
	LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieIndex,"SET_DATA_SLOT_EMPTY", QUICKMATCH_LIST_VIEW_STATE)
	LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", QUICKMATCH_LIST_VIEW_STATE,0,0,INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_208")	//Call...

	INT iSlot
	BOOL bTick = TRUE
	//do the menu title
	IF m_currentMode = QUICKMATCH_MODE_HEISTS
	AND m_HeistsList[iSelectedContent] = ENUM_TO_INT(QUICKMATCH_HEISTS_HEIST)
	AND NOT g_sMPTunables.bAutoJoinHeistsDisabled	
		IF bDisplayWithFriends
		// Store the details in the Scaleform slot
		BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieIndex, "SET_DATA_SLOT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(18)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSlot)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
				BEGIN_TEXT_COMMAND_SCALEFORM_STRING("CELL_JIP_F")//On Call with Friends in session
			END_TEXT_COMMAND_SCALEFORM_STRING()
		END_SCALEFORM_MOVIE_METHOD()
		iSlot++
		ENDIF
		BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieIndex, "SET_DATA_SLOT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(18)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSlot)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("JIPMP_HEISTQ")//Join On Call
			END_TEXT_COMMAND_SCALEFORM_STRING()
		END_SCALEFORM_MOVIE_METHOD()
		iSlot++
		// Store the details in the Scaleform slot
		BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieIndex, "SET_DATA_SLOT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(18)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSlot)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("JIPMP_HEISTD")//Join lobby direct
			END_TEXT_COMMAND_SCALEFORM_STRING()
		END_SCALEFORM_MOVIE_METHOD()
		iSlot++
	ELSE
		// Display 'Are you sure?" - there is only one slot
		//Only if we are to show with friends
		IF bDisplayWithFriends
			// Store the details in the Scaleform slot
			BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieIndex, "SET_DATA_SLOT")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(18)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSlot)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
				BEGIN_TEXT_COMMAND_SCALEFORM_STRING("CELL_JIP_F")
				END_TEXT_COMMAND_SCALEFORM_STRING()
			END_SCALEFORM_MOVIE_METHOD()
			iSlot++
		ENDIF

		// Store the details in the Scaleform slot
		IF g_sMPTunables.bOnCallDisabled = FALSE
			BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieIndex, "SET_DATA_SLOT")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(18)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSlot)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
				IF m_currentMode = QUICKMATCH_MODE_SERIES
				AND m_SeriesList[iSelectedContent]  = ENUM_TO_INT(QUICKMATCH_SERIES_ACTIVE_PREMIUM_RACE)
					IF g_sV2CoronaVars.iVehicleClassCount = ciCV2_OWN_NO_PERSONAL_VEHCICLE
						BEGIN_TEXT_COMMAND_SCALEFORM_STRING("CELL_JIPPRP")
						bTick = FALSE
					ELIF g_sV2CoronaVars.iVehicleClassCount = ciCV2_DONT_OWN_THIS_VEHCICLE
						BEGIN_TEXT_COMMAND_SCALEFORM_STRING("CELL_JIPPRV")
						ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(g_sV2CoronaVars.mnVehicleModel))
						bTick = FALSE
					ELIF g_sV2CoronaVars.iVehicleClassCount = ciCV2_DONT_OWN_VEHCICLE_IN_THIS_CLASS
						BEGIN_TEXT_COMMAND_SCALEFORM_STRING("CELL_JIPPRC")
						ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(CV2_GET_ACTIVE_RACE_CLASS_STRING(CV2_GET_CURRENT_PLAYLIST_CLASS()))
						bTick = FALSE
					ELIF g_sV2CoronaVars.iVehicleClassCount = ciCV2_PERSONAL_VEHCICLE_DESTROYED
						BEGIN_TEXT_COMMAND_SCALEFORM_STRING("FMSPR_HLP9P")
						bTick = FALSE
					ELIF NOT CV2_CAN_AFFORD_PROFESSIONAL_RACE_FEE()
						BEGIN_TEXT_COMMAND_SCALEFORM_STRING("CELL_JIPPRN")
						ADD_TEXT_COMPONENT_INTEGER(CV2_PROFESSIONAL_RACE_FEE())
						bTick = FALSE
					ELIF CV2_IS_PROCSSSIONAL_RACE_IN_COOL_DOWN()
						BEGIN_TEXT_COMMAND_SCALEFORM_STRING("PIM_DMAGUT")
						PRINTLN("[TS][CV2] (g_sV2CoronaVars.iRaceCoolDownPosix - GET_CLOUD_TIME_AS_INT()) * 1000")
						PRINTLN("[TS][CV2] (", g_sV2CoronaVars.iRaceCoolDownPosix, " - ", GET_CLOUD_TIME_AS_INT(), ") * 1000")
						ADD_TEXT_COMPONENT_SUBSTRING_TIME((g_sV2CoronaVars.iRaceCoolDownPosix - GET_CLOUD_TIME_AS_INT()) * 1000, TIME_FORMAT_HOURS|TIME_FORMAT_MINUTES|TIME_FORMAT_SECONDS)
						bTick = FALSE
					ELSE
						BEGIN_TEXT_COMMAND_SCALEFORM_STRING("CELL_JIP_PR")
						ADD_TEXT_COMPONENT_INTEGER(CV2_PROFESSIONAL_RACE_FEE())
						bTick = TRUE
					ENDIF
				ELSE
					bTick = TRUE
					IF m_currentMode = QUICKMATCH_MODE_HEISTS
					AND m_HeistsList[iSelectedContent]  = ENUM_TO_INT(QUICKMATCH_HEISTS_DIAMOND_CASINO)
					AND NOT HAS_H3_QUICKMATCH_COOLDOWN_EXPIRED()
						BEGIN_TEXT_COMMAND_SCALEFORM_STRING("PIM_DMAGUT")
						PRINTLN("[TS][CASINO_HEIST] (GET_MP_INT_PLAYER_STAT(MPPLY_H3_COOLDOWN) - GET_CLOUD_TIME_AS_INT()) * 1000")
						PRINTLN("[TS][CASINO_HEIST] (", GET_MP_INT_PLAYER_STAT(MPPLY_H3_COOLDOWN), " - ", GET_CLOUD_TIME_AS_INT(), ") * 1000")
						ADD_TEXT_COMPONENT_SUBSTRING_TIME((GET_MP_INT_PLAYER_STAT(MPPLY_H3_COOLDOWN) - GET_CLOUD_TIME_AS_INT()) * 1000, TIME_FORMAT_HOURS|TIME_FORMAT_MINUTES|TIME_FORMAT_SECONDS)
						bTick = FALSE
					#IF FEATURE_HEIST_ISLAND
					ELIF m_currentMode = QUICKMATCH_MODE_HEISTS
					AND m_HeistsList[iSelectedContent]  = ENUM_TO_INT(QUICKMATCH_HEISTS_CAYO_PERICO)
					AND NOT HAS_H4_QUICKMATCH_COOLDOWN_EXPIRED()
						BEGIN_TEXT_COMMAND_SCALEFORM_STRING("PIM_DMAGUT")
						PRINTLN("[TS][CASINO_HEIST] (GET_MP_INT_PLAYER_STAT(MPPLY_H3_COOLDOWN) - GET_CLOUD_TIME_AS_INT()) * 1000")
						PRINTLN("[TS][CASINO_HEIST] (", GET_MP_INT_PLAYER_STAT(MPPLY_H4_COOLDOWN), " - ", GET_CLOUD_TIME_AS_INT(), ") * 1000")
						ADD_TEXT_COMPONENT_SUBSTRING_TIME((GET_MP_INT_PLAYER_STAT(MPPLY_H4_COOLDOWN) - GET_CLOUD_TIME_AS_INT()) * 1000, TIME_FORMAT_HOURS|TIME_FORMAT_MINUTES|TIME_FORMAT_SECONDS)
						bTick = FALSE
					#ENDIF
					ELIF m_currentMode = QUICKMATCH_MODE_CONTACT_MISSIONS
					AND (m_ContactMissionsList[iSelectedContent]  = ENUM_TO_INT(QUICKMATCH_CONTACT_MISSIONS_SPECIAL_VEHICLE)
					OR m_ContactMissionsList[iSelectedContent]  = ENUM_TO_INT(QUICKMATCH_CONTACT_MISSIONS_MOBILE_OPERATION))
						BEGIN_TEXT_COMMAND_SCALEFORM_STRING("JIPMP_SVMH")
					ELSE
						BEGIN_TEXT_COMMAND_SCALEFORM_STRING("CELL_JIP_A")
					ENDIF
				ENDIF
				bBlockAccept = !bTick
				END_TEXT_COMMAND_SCALEFORM_STRING()
			END_SCALEFORM_MOVIE_METHOD()
			iSlot++
		ENDIF
//
//		// Store the details in the Scaleform slot
//		BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieIndex, "SET_DATA_SLOT")
//			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(18)
//			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSlot)
//			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
//			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("CELL_JIP_A")
//			END_TEXT_COMMAND_SCALEFORM_STRING()
//		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
	
	LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", QUICKMATCH_LIST_VIEW_STATE)
	
	IF m_currentMode = QUICKMATCH_MODE_HEISTS
		IF m_HeistsList[iSelectedContent]  = ENUM_TO_INT(QUICKMATCH_HEISTS_DIAMOND_CASINO)
		AND NOT HAS_H3_QUICKMATCH_COOLDOWN_EXPIRED()
			IF NOT IS_BIT_SET(MPGlobalsAmbience.iFmGbHelpBitSet9, BI_FM_GANG_BOSS_HELP_9_CASINO_HEIST_QUICK_MATCH_DELAY_HELP_DONE)
				PRINT_HELP("CH_QM_PHONEN")
				PRINTLN("[TS][CASINO_HEIST] SET_BIT(MPGlobalsAmbience.iFmGbHelpBitSet9, BI_FM_GANG_BOSS_HELP_9_CASINO_HEIST_QUICK_MATCH_DELAY_HELP_DONE)")
				SET_BIT(MPGlobalsAmbience.iFmGbHelpBitSet9, BI_FM_GANG_BOSS_HELP_9_CASINO_HEIST_QUICK_MATCH_DELAY_HELP_DONE)
			ENDIF
		#IF FEATURE_HEIST_ISLAND
		ELIF m_HeistsList[iSelectedContent]  = ENUM_TO_INT(QUICKMATCH_HEISTS_CAYO_PERICO)
		AND NOT HAS_H4_QUICKMATCH_COOLDOWN_EXPIRED()
			IF NOT IS_BIT_SET(MPGlobalsAmbience.iFmGbHelpBitSet9, BI_FM_GANG_BOSS_HELP_9_ISLAND_HEIST_QUICK_MATCH_DELAY_HELP_DONE)
				PRINT_HELP("IH_QM_PHONEN")
				PRINTLN("[TS][CASINO_HEIST] SET_BIT(MPGlobalsAmbience.iFmGbHelpBitSet9, BI_FM_GANG_BOSS_HELP_9_ISLAND_HEIST_QUICK_MATCH_DELAY_HELP_DONE)")
				SET_BIT(MPGlobalsAmbience.iFmGbHelpBitSet9, BI_FM_GANG_BOSS_HELP_9_ISLAND_HEIST_QUICK_MATCH_DELAY_HELP_DONE)
			ENDIF
		#ENDIF
		ELIF m_HeistsList[iSelectedContent] = ENUM_TO_INT(QUICKMATCH_HEISTS_HEIST)
		AND NOT g_sMPTunables.bAutoJoinHeistsDisabled
			Display_MP_App_Header(m_QuickMatchTextFM)
		ENDIF
	ENDIF

	IF bTick
		Display_MP_App_Buttons(MP_APP_ICON_YES, m_QuickMatchIconTextYes, MP_APP_ICON_BLANK, "", MP_APP_ICON_BACK, m_QuickMatchIconTextBack, m_sAppData)
	ELSE
		Display_MP_App_Buttons(MP_APP_ICON_BLANK, "", MP_APP_ICON_BLANK, "", MP_APP_ICON_BACK, m_QuickMatchIconTextBack, m_sAppData)
	ENDIF
				 
ENDPROC

//Temp - would be done in flash.
PROC Check_for_List_Navigation()
	IF dpad_scroll_pause_cued
		IF TIMERA() > 150
			PRINTLN("appJIPMP - Check_for_List_Navigation SETTING dpad_scroll_pause_cued = FALSE")
			dpad_scroll_pause_cued = FALSE
		ENDIF
	ENDIF

	// PC Scrollwheel support
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
	
        IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INPUT_CELLPHONE_SCROLL_BACKWARD)
            IF m_selectedQuickMatchSlot > 0
                m_selectedQuickMatchSlot --
            ENDIF   
            Call_Scaleform_Input_Keypress_Up()
 
        ENDIF       

        IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INPUT_CELLPHONE_SCROLL_FORWARD)
            m_selectedQuickMatchSlot ++   
            IF iCanDoQuickmatch = 0
                IF m_selectedQuickMatchSlot >= g_numQuickMatchListEntries 
                    m_selectedQuickMatchSlot = 0
                ENDIF
            ELSE
                m_selectedQuickMatchSlot = 0
            ENDIF
            Call_Scaleform_Input_Keypress_Down()
        ENDIF
		
    ENDIF
	
	

	IF dpad_scroll_pause_cued = FALSE
		IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_NAV_UP_INPUT))
			IF m_selectedQuickMatchSlot > 0
				m_selectedQuickMatchSlot --
			ENDIF	
			Call_Scaleform_Input_Keypress_Up()
			dpad_scroll_pause_cued = TRUE
			SETTIMERA (0)
		ENDIF		 

		IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_NAV_DOWN_INPUT))
			m_selectedQuickMatchSlot ++	
			IF iCanDoQuickmatch = 0
				IF m_selectedQuickMatchSlot >= g_numQuickMatchListEntries 
					m_selectedQuickMatchSlot = 0
				ENDIF
			ELSE
				m_selectedQuickMatchSlot = 0
			ENDIF
			Call_Scaleform_Input_Keypress_Down()
			dpad_scroll_pause_cued = TRUE
			SETTIMERA (0)
		ENDIF
	ENDIF
	 
ENDPROC


FUNC BOOL SHOULD_GO_TO_QUICKMATCH_FRIENDS_OR_SELF_OR_ON_CALL_SCREEN(BOOL bDisplayWithFriendsPassed)
	//If we are alone in this session then skip the friends part
	IF IS_THIS_A_SOLO_SESSION()
	OR	HAS_ENTERED_OFFLINE_SAVE_FM()
		RETURN FALSE
	ENDIF
	
	//Or there's no friends and the on call is disabled
	IF bDisplayWithFriendsPassed = FALSE
	AND (g_sMPTunables.bOnCallDisabled = TRUE
	OR bPlayerIsDrunk = TRUE)
		RETURN FALSE
	ENDIF

	RETURN TRUE
ENDFUNC

PROC UPDATE_QUICKMATCH_JOIN_SCREENS()
	IF SHOULD_GO_TO_QUICKMATCH_FRIENDS_OR_SELF_OR_ON_CALL_SCREEN(bDisplayWithFriends)
		PRINTLN("appJIPMP - Process_Stage_Mode_List - SHOULD_GO_TO_QUICKMATCH_FRIENDS_OR_SELF_OR_ON_CALL_SCREEN(bDisplayWithFriends)")
		//[RANKED_RACES] If we select ranked races then not with friends
		//AND ModeList[iselectedModeSlot] != QUICKMATCH_MODE_RACE_RANKED
		m_currentStage = QUICKMATCH_FRIENDS_OR_SELF_OR_ON_CALL
		Display_Friends_Or_On_Call_Screen()
	ELSE
		PRINTLN("appJIPMP - Process_Stage_Mode_List - SHOULD_GO_TO_QUICKMATCH_FRIENDS_OR_SELF_OR_ON_CALL_SCREEN ELSE")
		m_currentStage = QUICKMATCH_OFFER_JOIN
		g_Cellphone.PhoneDS = PDS_COMPLEXAPP
		Display_Confirm_Screen()
	ENDIF
ENDPROC

PROC GO_BACK_TO_MODES_MENU()
	PRINTLN("appJIPMP - GO_BACK_TO_MODES_MENU - BACK TO THE MODE LIST.")
	Clear_Accept_Invite_Warning_Help_Text_If_On_Display()
	g_Cellphone.PhoneDS = PDS_COMPLEXAPP
	Display_First_QuickMatch_Screen()
	m_selectedQuickMatchSlot = 0
	iSelectedContent = 0
	m_currentStage= QUICKMATCH_MODE_LIST
	Play_Back_Beep()
	m_iStageState = 0
	
ENDPROC

PROC Display_QUICKMATCH_SUBLIST_Screen(enumQuickMatchModes eMode)
	// The JobList page is the Apps first screen, so the phone should be in RunningApp mode so that it knows to return to the HomeScreen on 'BACK'.
	IF NOT (g_Cellphone.PhoneDS = PDS_RUNNINGAPP)
		IF g_CellPhone.PhoneDS > PDS_AWAY //Added as a precaution... 
			g_Cellphone.PhoneDS = PDS_RUNNINGAPP
			PRINTLN("STATE ASSIGNMENT 203. appJIPMP::Display_JobList_Screen() assigns PDS_RUNNINGAPP")
		ENDIF
	ENDIF
	
	PRINTLN("appJIPMP - Display_QUICKMATCH_SUBLIST_Screen")

	// Setup the highlighter initial position
	// Store the details from the JobList array in the JobList Screen slots
	LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieIndex,"SET_DATA_SLOT_EMPTY", QUICKMATCH_LIST_VIEW_STATE)
	LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", QUICKMATCH_LIST_VIEW_STATE,0,0,INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_208")	//Call...
	Fill_QuickMatch_Sub_List_Screen_Slots(eMode)
	LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", QUICKMATCH_LIST_VIEW_STATE)

	
	
	// Display the screen
	// NOTE: Must be after the slots have been filled
	PRINTLN("appJIPMP - : Display_QUICKMATCH_SUBLIST_Screen() - displaying MPJIPlist [view state: ", QUICKMATCH_LIST_VIEW_STATE, "]") 

	// Display the Header
	Display_MP_App_Header(GET_MODE_TEXT(eMode))

	IF slotsUsed = 0		//#1508749
		Display_MP_App_Buttons(MP_APP_ICON_BLANK, "", MP_APP_ICON_BLANK, "", MP_APP_ICON_BACK, m_QuickMatchIconTextBack, m_sAppData)	
	ELSE
		Display_MP_App_Buttons(MP_APP_ICON_YES, m_QuickMatchIconTextYes, MP_APP_ICON_BLANK, "", MP_APP_ICON_BACK, m_QuickMatchIconTextBack, m_sAppData)
	ENDIF
	// Now on the JobList screen
	m_currentStage = QUICKMATCH_SUBLIST
ENDPROC


PROC Process_Stage_Mode_List()
	Check_for_List_Navigation()
	IF iCanDoQuickmatch = 0
	AND SlotsUsed > 0 //Need this to prevent "Not available" from being selected.
		IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_POSITIVE_INPUT))	//Player selects a job type from the list.
			g_InputButtonJustPressed = TRUE
			Play_Select_Beep()
			BEGIN_SCALEFORM_MOVIE_METHOD (SF_MovieIndex, "GET_CURRENT_SELECTION")
			Choice_ReturnedSFIndex = END_SCALEFORM_MOVIE_METHOD_RETURN_VALUE()
			WHILE NOT IS_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_READY (Choice_ReturnedSFIndex)
				WAIT (0)
				PRINTLN("appJIPMP - Process_Stage_Mode_List - Waiting on scaleform return during selecting mode.")
			ENDWHILE

			iselectedModeSlot = GET_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_INT (Choice_ReturnedSFIndex)
			PRINTLN("appJIPMP - Process_Stage_Mode_List - iselectedModeSlot            = ", iselectedModeSlot)
			IF iselectedModeSlot > -1 AND iselectedModeSlot < ENUM_TO_INT(QUICKMATCH_MODE_MAX)
				PRINTLN("appJIPMP - Process_Stage_Mode_List - m_ModeList[iselectedModeSlot]) = ", m_ModeList[iselectedModeSlot])
				IF g_CellPhone.PhoneDS > PDS_AWAY
					g_Cellphone.PhoneDS = PDS_COMPLEXAPP
					// ...some additional debugging help - a unique identifier for each time g_Cellphone.phoneDS gets updated
					PRINTLN("STATE ASSIGNMENT 202. appJIPMP::Process_Stage_Mode_List() assigns PDS_COMPLEXAPP")
				ENDIF
				m_currentMode = INT_TO_ENUM(enumQuickMatchModes,m_ModeList[iselectedModeSlot])
				bDisplayWithFriends = ARE_ANY_FRIENDS_INT_THIS_SESSION()
				SWITCH INT_TO_ENUM(enumQuickMatchModes,m_currentMode)
					CASE QUICKMATCH_MODE_SERIES
						PRINTLN("appJIPMP - Process_Stage_Mode_List - Selected Mode = QUICKMATCH_MODE_SERIES")
						m_currentStage = QUICKMATCH_SUBLIST
						Display_QUICKMATCH_SUBLIST_Screen(QUICKMATCH_MODE_SERIES)
					BREAK
					CASE QUICKMATCH_MODE_HEISTS
						PRINTLN("appJIPMP - Process_Stage_Mode_List - Selected Mode = QUICKMATCH_MODE_HEISTS")
						m_currentStage = QUICKMATCH_SUBLIST
						Display_QUICKMATCH_SUBLIST_Screen(QUICKMATCH_MODE_HEISTS)
					BREAK
					CASE QUICKMATCH_MODE_CONTACT_MISSIONS
					PRINTLN("appJIPMP - Process_Stage_Mode_List - Selected Mode = QUICKMATCH_MODE_CONTACT_MISSIONS")
						m_currentStage = QUICKMATCH_SUBLIST
						Display_QUICKMATCH_SUBLIST_Screen(QUICKMATCH_MODE_CONTACT_MISSIONS)
					BREAK
					CASE QUICKMATCH_MODE_JOBS
					PRINTLN("appJIPMP - Process_Stage_Mode_List - Selected Mode = QUICKMATCH_MODE_JOBS")
						m_currentStage = QUICKMATCH_SUBLIST
						Display_QUICKMATCH_SUBLIST_Screen(QUICKMATCH_MODE_JOBS)
					BREAK
					CASE QUICKMATCH_MODE_ACTIVITIES
						PRINTLN("appJIPMP - Process_Stage_Mode_List - Selected Mode = QUICKMATCH_MODE_ACTIVITIES")
						m_currentStage = QUICKMATCH_SUBLIST
						Display_QUICKMATCH_SUBLIST_Screen(QUICKMATCH_MODE_ACTIVITIES)
					BREAK
					CASE QUICKMATCH_MODE_PLAYLIST
						PRINTLN("appJIPMP - Process_Stage_Mode_List - Selected Mode = QUICKMATCH_MODE_PLAYLIST")
						UPDATE_QUICKMATCH_JOIN_SCREENS()
					BREAK
					CASE QUICKMATCH_MODE_RANDOM
						PRINTLN("appJIPMP - Process_Stage_Mode_List - Selected Mode = QUICKMATCH_MODE_RANDOM")
						UPDATE_QUICKMATCH_JOIN_SCREENS()
					BREAK
				ENDSWITCH
			ELSE
				PRINTLN("appJIPMP - Process_Stage_Mode_List - iselectedModeSlot is out of range of m_ModeList[iselectedModeSlot]) ")
			ENDIF
		ENDIF
	ELSE
		//#1492475
		IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_POSITIVE_INPUT))
			g_InputButtonJustPressed = TRUE
			Play_Back_Beep()
		ENDIF
	ENDIF
ENDPROC

PROC Process_Stage_Sublist()
	Check_for_List_Navigation()
	INT iMaxSelections
	IF iCanDoQuickmatch = 0
	AND SlotsUsed > 0 //Need this to prevent "Not available" from being selected.
		IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_POSITIVE_INPUT))	//Player selects a job type from the list.
			g_InputButtonJustPressed = TRUE
			Play_Select_Beep()
			BEGIN_SCALEFORM_MOVIE_METHOD (SF_MovieIndex, "GET_CURRENT_SELECTION")
			Choice_ReturnedSFIndex = END_SCALEFORM_MOVIE_METHOD_RETURN_VALUE()
			WHILE NOT IS_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_READY (Choice_ReturnedSFIndex)
				WAIT (0)
				PRINTLN("appJIPMP - Process_Stage_Sublist - - Waiting on scaleform return during selecting mode.")
			ENDWHILE
			iSelectedContent = GET_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_INT (Choice_ReturnedSFIndex)
		
			PRINTLN("appJIPMP - Process_Stage_Sublist - SELECTED MODE = ", GET_MODE_TEXT(INT_TO_ENUM(enumQuickMatchModes,m_ModeList[iselectedModeSlot])))
			PRINTLN("appJIPMP - Process_Stage_Sublist - iSelectedContent = ", iSelectedContent)
			
			SWITCH m_currentMode
				CASE QUICKMATCH_MODE_SERIES
					PRINTLN("appJIPMP - Process_Stage_Sublist - - iMaxSelections = QUICKMATCH_SERIES_MAX.")
					PRINTLN("appJIPMP - Process_Stage_Sublist - SeriesList[iSelectedContent]) = ",GET_CONTENT_TEXT(QUICKMATCH_MODE_SERIES,m_SeriesList[iSelectedContent]))
					iMaxSelections = ENUM_TO_INT(QUICKMATCH_SERIES_MAX)
				BREAK
				CASE QUICKMATCH_MODE_HEISTS
					PRINTLN("appJIPMP - Process_Stage_Sublist - - iMaxSelections = QUICKMATCH_HEISTS_MAX.")
					PRINTLN("appJIPMP - Process_Stage_Sublist - SeriesList[iSelectedContent]) = ",GET_CONTENT_TEXT(QUICKMATCH_MODE_HEISTS,m_HeistsList[iSelectedContent]))
					iMaxSelections = ENUM_TO_INT(QUICKMATCH_HEISTS_MAX)
				BREAK
				CASE QUICKMATCH_MODE_JOBS
					PRINTLN("appJIPMP - Process_Stage_Sublist - - iMaxSelections = QUICKMATCH_JOBS_MAX.")
					iMaxSelections = ENUM_TO_INT(QUICKMATCH_JOBS_MAX)
				BREAK	
				CASE QUICKMATCH_MODE_CONTACT_MISSIONS
					PRINTLN("appJIPMP - Process_Stage_Sublist - - iMaxSelections = QUICKMATCH_CONTACT_MISSIONS_MAX .")
					iMaxSelections = ENUM_TO_INT(QUICKMATCH_CONTACT_MISSIONS_MAX)
				BREAK	
				CASE QUICKMATCH_MODE_ACTIVITIES
					PRINTLN("appJIPMP - Process_Stage_Sublist - - iMaxSelections = QUICKMATCH_ACTIVITIES_MAX .")
					iMaxSelections = ENUM_TO_INT(QUICKMATCH_ACTIVITIES_MAX)
				BREAK
			ENDSWITCH
		
			
			IF iSelectedContent > -1 AND iSelectedContent < iMaxSelections
				IF g_CellPhone.PhoneDS > PDS_AWAY
					g_Cellphone.PhoneDS = PDS_COMPLEXAPP
					PRINTLN("STATE ASSIGNMENT 202. appJIPMP::Process_Stage_Sublist() assigns PDS_COMPLEXAPP")
				ENDIF
				
				bDisplayWithFriends = ARE_ANY_FRIENDS_INT_THIS_SESSION()
				
				UPDATE_QUICKMATCH_JOIN_SCREENS()
			ENDIF
		ENDIF
		IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_NEGATIVE_INPUT))
			GO_BACK_TO_MODES_MENU()
		ENDIF
	ELSE
		PRINTLN("appJIPMP - Process_Stage_Sublist - SlotsUsed = 0.")
		//#1492475
		IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_POSITIVE_INPUT))
			g_InputButtonJustPressed = TRUE
			Play_Back_Beep()
		ENDIF
	ENDIF

ENDPROC

PROC PROCESS_LAUNCH_QUICKMATCH()
	INT itype
	Clear_Accept_Invite_Warning_Help_Text_If_On_Display()
	IF IS_MODE_AVAILABLE(m_currentMode)
		PRINTLN("[TS] ***************************************")
		CLEAR_MY_TRANSITION_SESSION_CONTENT_ID()			
		INT iCurrentPlaylistPos
		INT iCurrentMissionArrayPos
		INT iPlayList
		SWITCH m_currentMode
			CASE QUICKMATCH_MODE_RANDOM
				iType = ciTRANSITION_SESSIONS_ACTIVITY_TYPE_ANY
				PRINTLN("[TS] * QUICKMATCH PHONE - SELECTED RANDOM")
			BREAK
			
			CASE QUICKMATCH_MODE_PLAYLIST
				iType = FM_GAME_MODE_PLAYLIST
				PRINTLN("[TS] * QUICKMATCH PHONE - SELECTED PLAYLIST")
			BREAK
			
			CASE QUICKMATCH_MODE_SERIES
				PRINTLN("[TS] * QUICKMATCH PHONE - SELECTED QUICKMATCH_MODE_SERIES")
				SWITCH INT_TO_ENUM(enumQuickMatchSeries,m_SeriesList[iSelectedContent])
					CASE QUICKMATCH_SERIES_FEATURED
						iPlayList = CV2_GET_NEW_VS_ARRAY_POS()
						IF iPlayList != -1
						AND SHOULD_SHOW_QUICKMATCH_MODE_MISSION_NEW_VS(iPlayList)
							iType = FMMC_TYPE_MISSION_NEW_VS
							iCurrentPlaylistPos = CV2_PROFESSIONAL_GET_CURRENTLY_ACTIVE_V2_CORONA_POS(iPlayList)
							iCurrentMissionArrayPos = GET_ROCKSTAR_MISSION_FROM_ROOT_CONTENT_ID_HASH(CV2_PROFESSIONAL_GET_CURRENTLY_ACTIVE_V2_CORONA_ROOT_CONTENT_ID_HASH(iCurrentPlaylistPos, iPlayList))
							IF iCurrentMissionArrayPos != -1
								SET_MY_TRANSITION_SESSION_CONTENT_ID(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iCurrentMissionArrayPos].tlName)
								SET_TRANSITION_SESSIONS_QUICK_MATCH_MAX_PLAYERS(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iCurrentMissionArrayPos].iMaxPlayers)
								SET_TRANSITION_SESSIONS_SKIP_JOB_WARNING()
							#IF IS_DEBUG_BUILD
							ELSE
								PRINTLN("[TS] * QUICKMATCH PHONE - QUICKMATCH_SERIES_FEATURED - iCurrentMissionArrayPos = -1")
								#ENDIF
							ENDIF
						ELSE
							iType = FMMC_TYPE_MISSION_NEW_VS
							PRINTLN("[TS] * QUICKMATCH PHONE - SELECTED QUICKMATCH_SERIES_FEATURED")
						ENDIF
					BREAK
					
					CASE QUICKMATCH_SERIES_COMMUNITY
						iType = FMMC_TYPE_COMMUNITY_SERIES
						iPlayList = CV2_GET_ADVERSARY_SERIES_ARRAY_POS(FMMC_TYPE_COMMUNITY_SERIES)
						IF iPlayList != -1
							iCurrentPlaylistPos = CV2_PROFESSIONAL_GET_CURRENTLY_ACTIVE_V2_CORONA_POS(iPlayList)
							iCurrentMissionArrayPos = GET_ROCKSTAR_MISSION_FROM_ROOT_CONTENT_ID_HASH(g_sMPTunables.iFmCoronaPlaylistProffesionalRootContentId[iPlayList][iCurrentPlaylistPos], TRUE)
							IF iCurrentMissionArrayPos != -1
								SET_MY_TRANSITION_SESSION_CONTENT_ID(g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iCurrentMissionArrayPos].tlName)
								SET_TRANSITION_SESSIONS_QUICK_MATCH_MAX_PLAYERS(g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iCurrentMissionArrayPos].iMaxPlayers)
								SET_TRANSITION_SESSIONS_SKIP_JOB_WARNING()
							#IF IS_DEBUG_BUILD
							ELSE
								PRINTLN("[TS] * QUICKMATCH PHONE - QUICKMATCH_SERIES_COMMUNITY - iCurrentMissionArrayPos = -1")
								#ENDIF
							ENDIF
						ENDIF
						PRINTLN("[TS] * QUICKMATCH PHONE - SELECTED QUICKMATCH_SERIES_COMMUNITY")
					BREAK
					
					CASE QUICKMATCH_SERIES_CAYO_PERICO
						iType = FMMC_TYPE_CAYO_PERICO_SERIES
						iPlayList = CV2_GET_ADVERSARY_SERIES_ARRAY_POS(FMMC_TYPE_CAYO_PERICO_SERIES)
						IF iPlayList != -1
							iCurrentPlaylistPos = CV2_PROFESSIONAL_GET_CURRENTLY_ACTIVE_V2_CORONA_POS(iPlayList)
							iCurrentMissionArrayPos = GET_ROCKSTAR_MISSION_FROM_ROOT_CONTENT_ID_HASH(g_sMPTunables.iFmCoronaPlaylistProffesionalRootContentId[iPlayList][iCurrentPlaylistPos], TRUE)
							IF iCurrentMissionArrayPos != -1
								SET_MY_TRANSITION_SESSION_CONTENT_ID(g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iCurrentMissionArrayPos].tlName)
								SET_TRANSITION_SESSIONS_QUICK_MATCH_MAX_PLAYERS(g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[iCurrentMissionArrayPos].iMaxPlayers)
								SET_TRANSITION_SESSIONS_SKIP_JOB_WARNING()
							#IF IS_DEBUG_BUILD
							ELSE
								PRINTLN("[TS] * QUICKMATCH PHONE - QUICKMATCH_SERIES_CAYO_PERICO - iCurrentMissionArrayPos = -1")
								#ENDIF
							ENDIF
						ENDIF
						PRINTLN("[TS] * QUICKMATCH PHONE - SELECTED QUICKMATCH_SERIES_CAYO_PERICO")
					BREAK
					
					#IF FEATURE_GEN9_EXCLUSIVE
					CASE QUICKMATCH_SERIES_HSW_RACE
						IF g_sMPTunables.bDisableStuntSeriesBucket
							iType = FMMC_TYPE_RACE
						ELSE
							iType = FMMC_TYPE_HSW_RACE_SERIES
						ENDIF
						iPlayList = CV2_GET_ADVERSARY_SERIES_ARRAY_POS(FMMC_TYPE_HSW_RACE_SERIES)
						IF iPlayList != -1
							iCurrentPlaylistPos = CV2_PROFESSIONAL_GET_CURRENTLY_ACTIVE_V2_CORONA_POS(iPlayList)
							iCurrentMissionArrayPos = GET_ROCKSTAR_MISSION_FROM_ROOT_CONTENT_ID_HASH(g_sMPTunables.iFmCoronaPlaylistProffesionalRootContentId[iPlayList][iCurrentPlaylistPos])
							IF iCurrentMissionArrayPos != -1
								SET_MY_TRANSITION_SESSION_CONTENT_ID(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iCurrentMissionArrayPos].tlName)
								SET_TRANSITION_SESSIONS_QUICK_MATCH_MAX_PLAYERS(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iCurrentMissionArrayPos].iMaxPlayers)
								SET_TRANSITION_SESSIONS_SKIP_JOB_WARNING()
							#IF IS_DEBUG_BUILD
							ELSE
								PRINTLN("[TS] * QUICKMATCH PHONE - QUICKMATCH_SERIES_HSW_RACE - iCurrentMissionArrayPos = -1")
								#ENDIF
							ENDIF
						ENDIF
						PRINTLN("[TS] * QUICKMATCH PHONE - SELECTED QUICKMATCH_SERIES_HSW_RACE")
					BREAK
					#ENDIF
					
					CASE QUICKMATCH_SERIES_STREET_RACE		
						IF g_sMPTunables.bDisableStuntSeriesBucket
							iType = FMMC_TYPE_RACE
						ELSE
							iType = FMMC_TYPE_STREET_RACE_SERIES
						ENDIF
						iPlayList = CV2_GET_ADVERSARY_SERIES_ARRAY_POS(FMMC_TYPE_STREET_RACE_SERIES)
						IF iPlayList != -1
							iCurrentPlaylistPos = CV2_PROFESSIONAL_GET_CURRENTLY_ACTIVE_V2_CORONA_POS(iPlayList)
							iCurrentMissionArrayPos = GET_ROCKSTAR_MISSION_FROM_ROOT_CONTENT_ID_HASH(g_sMPTunables.iFmCoronaPlaylistProffesionalRootContentId[iPlayList][iCurrentPlaylistPos])
							IF iCurrentMissionArrayPos != -1
								SET_MY_TRANSITION_SESSION_CONTENT_ID(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iCurrentMissionArrayPos].tlName)
								SET_TRANSITION_SESSIONS_QUICK_MATCH_MAX_PLAYERS(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iCurrentMissionArrayPos].iMaxPlayers)
								SET_TRANSITION_SESSIONS_SKIP_JOB_WARNING()
							#IF IS_DEBUG_BUILD
							ELSE
								PRINTLN("[TS] * QUICKMATCH PHONE - QUICKMATCH_SERIES_STREET_RACE - iCurrentMissionArrayPos = -1")
								#ENDIF
							ENDIF
						ENDIF
						PRINTLN("[TS] * QUICKMATCH PHONE - SELECTED QUICKMATCH_SERIES_STREET_RACE")
					BREAK
					
					CASE QUICKMATCH_SERIES_PURSUIT
						IF g_sMPTunables.bDisableStuntSeriesBucket
							iType = FMMC_TYPE_RACE
						ELSE
							iType = FMMC_TYPE_PURSUIT_SERIES
						ENDIF
						iPlayList = CV2_GET_ADVERSARY_SERIES_ARRAY_POS(FMMC_TYPE_PURSUIT_SERIES)
						IF iPlayList != -1
							iCurrentPlaylistPos = CV2_PROFESSIONAL_GET_CURRENTLY_ACTIVE_V2_CORONA_POS(iPlayList)
							iCurrentMissionArrayPos = GET_ROCKSTAR_MISSION_FROM_ROOT_CONTENT_ID_HASH(g_sMPTunables.iFmCoronaPlaylistProffesionalRootContentId[iPlayList][iCurrentPlaylistPos])
							IF iCurrentMissionArrayPos != -1
								SET_MY_TRANSITION_SESSION_CONTENT_ID(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iCurrentMissionArrayPos].tlName)
								SET_TRANSITION_SESSIONS_QUICK_MATCH_MAX_PLAYERS(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iCurrentMissionArrayPos].iMaxPlayers)
								SET_TRANSITION_SESSIONS_SKIP_JOB_WARNING()
							#IF IS_DEBUG_BUILD
							ELSE
								PRINTLN("[TS] * QUICKMATCH PHONE - QUICKMATCH_SERIES_PURSUIT - iCurrentMissionArrayPos = -1")
							#ENDIF
							ENDIF
						ENDIF
						PRINTLN("[TS] * QUICKMATCH PHONE - SELECTED QUICKMATCH_SERIES_PURSUIT")
					BREAK
					
					CASE QUICKMATCH_SERIES_OPEN_WHEEL
						IF g_sMPTunables.bDisableStuntSeriesBucket
							iType = FMMC_TYPE_RACE
						ELSE
							iType = FMMC_TYPE_OPEN_WHEEL_SERIES
						ENDIF
						iPlayList = CV2_GET_ADVERSARY_SERIES_ARRAY_POS(FMMC_TYPE_OPEN_WHEEL_SERIES)
						IF iPlayList != -1
							iCurrentPlaylistPos = CV2_PROFESSIONAL_GET_CURRENTLY_ACTIVE_V2_CORONA_POS(iPlayList)
							iCurrentMissionArrayPos = GET_ROCKSTAR_MISSION_FROM_ROOT_CONTENT_ID_HASH(g_sMPTunables.iFmCoronaPlaylistProffesionalRootContentId[iPlayList][iCurrentPlaylistPos])
							IF iCurrentMissionArrayPos != -1
								SET_MY_TRANSITION_SESSION_CONTENT_ID(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iCurrentMissionArrayPos].tlName)
								SET_TRANSITION_SESSIONS_QUICK_MATCH_MAX_PLAYERS(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iCurrentMissionArrayPos].iMaxPlayers)
								SET_TRANSITION_SESSIONS_SKIP_JOB_WARNING()
							#IF IS_DEBUG_BUILD
							ELSE
							PRINTLN("[TS] * QUICKMATCH PHONE - QUICKMATCH_SERIES_OPEN_WHEEL - iCurrentMissionArrayPos = -1")
							#ENDIF
							ENDIF
						ENDIF
						PRINTLN("[TS] * QUICKMATCH PHONE - SELECTED QUICKMATCH_SERIES_OPEN_WHEEL")
					BREAK
					
					CASE QUICKMATCH_SERIES_RACE
						IF g_sMPTunables.bDisableStuntSeriesBucket
							iType = FMMC_TYPE_RACE
						ELSE
							iType = FMMC_TYPE_RACE_SERIES
						ENDIF
						iPlayList = CV2_GET_ADVERSARY_SERIES_ARRAY_POS(FMMC_TYPE_RACE_SERIES)
						IF iPlayList != -1
							iCurrentPlaylistPos = CV2_PROFESSIONAL_GET_CURRENTLY_ACTIVE_V2_CORONA_POS(iPlayList)
							iCurrentMissionArrayPos = GET_ROCKSTAR_MISSION_FROM_ROOT_CONTENT_ID_HASH(g_sMPTunables.iFmCoronaPlaylistProffesionalRootContentId[iPlayList][iCurrentPlaylistPos])
							IF iCurrentMissionArrayPos != -1
								SET_MY_TRANSITION_SESSION_CONTENT_ID(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iCurrentMissionArrayPos].tlName)
								SET_TRANSITION_SESSIONS_QUICK_MATCH_MAX_PLAYERS(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iCurrentMissionArrayPos].iMaxPlayers)
								SET_TRANSITION_SESSIONS_SKIP_JOB_WARNING()
							#IF IS_DEBUG_BUILD
							ELSE
								PRINTLN("[TS] * QUICKMATCH PHONE - QUICKMATCH_SERIES_RACE - iCurrentMissionArrayPos = -1")
								#ENDIF
							ENDIF
						ENDIF
						PRINTLN("[TS] * QUICKMATCH PHONE - SELECTED QUICKMATCH_SERIES_RACE")
					BREAK
					
					CASE QUICKMATCH_SERIES_SURVIVAL	
						IF g_sMPTunables.bDisableStuntSeriesBucket
							iType = FMMC_TYPE_RACE
						ELSE
							iType = FMMC_TYPE_SURVIVAL_SERIES
						ENDIF
						iPlayList = CV2_GET_ADVERSARY_SERIES_ARRAY_POS(FMMC_TYPE_SURVIVAL_SERIES)
						IF iPlayList != -1
							iCurrentPlaylistPos = CV2_PROFESSIONAL_GET_CURRENTLY_ACTIVE_V2_CORONA_POS(iPlayList)
							iCurrentMissionArrayPos = GET_ROCKSTAR_MISSION_FROM_ROOT_CONTENT_ID_HASH(g_sMPTunables.iFmCoronaPlaylistProffesionalRootContentId[iPlayList][iCurrentPlaylistPos])
							IF iCurrentMissionArrayPos != -1
								SET_MY_TRANSITION_SESSION_CONTENT_ID(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iCurrentMissionArrayPos].tlName)
								SET_TRANSITION_SESSIONS_QUICK_MATCH_MAX_PLAYERS(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iCurrentMissionArrayPos].iMaxPlayers)
								SET_TRANSITION_SESSIONS_SKIP_JOB_WARNING()
							#IF IS_DEBUG_BUILD
							ELSE
								PRINTLN("[TS] * QUICKMATCH PHONE - QUICKMATCH_SERIES_SURVIVAL - iCurrentMissionArrayPos = -1")
								#ENDIF
							ENDIF
						ENDIF
						PRINTLN("[TS] * QUICKMATCH PHONE - SELECTED QUICKMATCH_SERIES_SURVIVAL")
					BREAK
					
					CASE QUICKMATCH_SERIES_ARENA_WAR
						IF g_sMPTunables.bDisableStuntSeriesBucket
							iType = FMMC_TYPE_RACE
						ELSE
							iType = FMMC_TYPE_ARENA_SERIES
						ENDIF
						iPlayList = CV2_GET_ADVERSARY_SERIES_ARRAY_POS(FMMC_TYPE_ARENA_SERIES)
						IF iPlayList != -1
							iCurrentPlaylistPos = CV2_PROFESSIONAL_GET_CURRENTLY_ACTIVE_V2_CORONA_POS(iPlayList)
							iCurrentMissionArrayPos = GET_ROCKSTAR_MISSION_FROM_ROOT_CONTENT_ID_HASH(g_sMPTunables.iFmCoronaPlaylistProffesionalRootContentId[iPlayList][iCurrentPlaylistPos])
							IF iCurrentMissionArrayPos != -1
								SET_MY_TRANSITION_SESSION_CONTENT_ID(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iCurrentMissionArrayPos].tlName)
								SET_TRANSITION_SESSIONS_QUICK_MATCH_MAX_PLAYERS(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iCurrentMissionArrayPos].iMaxPlayers)
								SET_TRANSITION_SESSIONS_SKIP_JOB_WARNING()
							#IF IS_DEBUG_BUILD
							ELSE
								PRINTLN("[TS] * QUICKMATCH PHONE - QUICKMATCH_SERIES_ARENA_WAR - iCurrentMissionArrayPos = -1")
								#ENDIF
							ENDIF
						ENDIF
						PRINTLN("[TS] * QUICKMATCH PHONE - SELECTED QUICKMATCH_SERIES_ARENA_WAR")
					BREAK
					
					CASE QUICKMATCH_SERIES_SPECIAL_RACE
						IF g_sMPTunables.bDisableStuntSeriesBucket
							iType = FMMC_TYPE_MISSION_NEW_VS
						ELSE
							iType = FMMC_TYPE_SPECIAL_VEHICLE_RACE_SERIES
						ENDIF
						iPlayList = CV2_GET_ADVERSARY_SERIES_ARRAY_POS(FMMC_TYPE_SPECIAL_VEHICLE_RACE_SERIES)
						IF iPlayList != -1
							iCurrentPlaylistPos = CV2_PROFESSIONAL_GET_CURRENTLY_ACTIVE_V2_CORONA_POS(iPlayList)
							iCurrentMissionArrayPos = GET_ROCKSTAR_MISSION_FROM_ROOT_CONTENT_ID_HASH(g_sMPTunables.iFmCoronaPlaylistProffesionalRootContentId[iPlayList][iCurrentPlaylistPos])
							IF iCurrentMissionArrayPos != -1
								SET_MY_TRANSITION_SESSION_CONTENT_ID(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iCurrentMissionArrayPos].tlName)
								SET_TRANSITION_SESSIONS_QUICK_MATCH_MAX_PLAYERS(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iCurrentMissionArrayPos].iMaxPlayers)
								SET_TRANSITION_SESSIONS_SKIP_JOB_WARNING()
							#IF IS_DEBUG_BUILD
							ELSE
								PRINTLN("[TS] * QUICKMATCH PHONE - QUICKMATCH_SERIES_SPECIAL_RACE - iCurrentMissionArrayPos = -1")
							#ENDIF
							ENDIF
						ENDIF
						PRINTLN("[TS] * QUICKMATCH PHONE - SELECTED QUICKMATCH_SERIES_SPECIAL_RACE")
					BREAK
					
					CASE QUICKMATCH_SERIES_SUPER_SPORT	
						IF g_sMPTunables.bDisableStuntSeriesBucket
							iType = FMMC_TYPE_RACE
						ELSE
							iType = FMMC_TYPE_HOTRING_SERIES
						ENDIF
						iPlayList = CV2_GET_ADVERSARY_SERIES_ARRAY_POS(FMMC_TYPE_HOTRING_SERIES)
						IF iPlayList != -1
							iCurrentPlaylistPos = CV2_PROFESSIONAL_GET_CURRENTLY_ACTIVE_V2_CORONA_POS(iPlayList)
							iCurrentMissionArrayPos = GET_ROCKSTAR_MISSION_FROM_ROOT_CONTENT_ID_HASH(g_sMPTunables.iFmCoronaPlaylistProffesionalRootContentId[iPlayList][iCurrentPlaylistPos])
							IF iCurrentMissionArrayPos != -1
								SET_MY_TRANSITION_SESSION_CONTENT_ID(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iCurrentMissionArrayPos].tlName)
								SET_TRANSITION_SESSIONS_QUICK_MATCH_MAX_PLAYERS(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iCurrentMissionArrayPos].iMaxPlayers)
								SET_TRANSITION_SESSIONS_SKIP_JOB_WARNING()
							#IF IS_DEBUG_BUILD
							ELSE
								PRINTLN("[TS] * QUICKMATCH PHONE - QUICKMATCH_SERIES_SUPER_SPORT - iCurrentMissionArrayPos = -1")
								#ENDIF
							ENDIF
						ENDIF
						PRINTLN("[TS] * QUICKMATCH PHONE - SELECTED QUICKMATCH_SERIES_SUPER_SPORT")
					BREAK
					
					CASE QUICKMATCH_SERIES_TRANSFORM
						IF g_sMPTunables.bDisableStuntSeriesBucket
							iType = FMMC_TYPE_RACE
						ELSE
							iType = FMMC_TYPE_TRANSFORM_SERIES
						ENDIF
						iPlayList = CV2_GET_ADVERSARY_SERIES_ARRAY_POS(FMMC_TYPE_TRANSFORM_SERIES)
						IF iPlayList != -1
							iCurrentPlaylistPos = CV2_PROFESSIONAL_GET_CURRENTLY_ACTIVE_V2_CORONA_POS(iPlayList)
							iCurrentMissionArrayPos = GET_ROCKSTAR_MISSION_FROM_ROOT_CONTENT_ID_HASH(g_sMPTunables.iFmCoronaPlaylistProffesionalRootContentId[iPlayList][iCurrentPlaylistPos])
							IF iCurrentMissionArrayPos != -1
								SET_MY_TRANSITION_SESSION_CONTENT_ID(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iCurrentMissionArrayPos].tlName)
								SET_TRANSITION_SESSIONS_QUICK_MATCH_MAX_PLAYERS(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iCurrentMissionArrayPos].iMaxPlayers)
								SET_TRANSITION_SESSIONS_SKIP_JOB_WARNING()
							#IF IS_DEBUG_BUILD
							ELSE
								PRINTLN("[TS] * QUICKMATCH PHONE - QUICKMATCH_SERIES_TRANSFORM - iCurrentMissionArrayPos = -1")
								#ENDIF
							ENDIF
						ENDIF
						PRINTLN("[TS] * QUICKMATCH PHONE - SELECTED QUICKMATCH_SERIES_TRANSFORM")
					BREAK
					
					CASE QUICKMATCH_SERIES_BUNKER
						IF g_sMPTunables.bDisableStuntSeriesBucket
							iType = FMMC_TYPE_MISSION_NEW_VS
						ELSE
							iType = FMMC_TYPE_BUNKER_SERIES
						ENDIF
						iPlayList = CV2_GET_ADVERSARY_SERIES_ARRAY_POS(FMMC_TYPE_BUNKER_SERIES)
						IF iPlayList != -1
							iCurrentPlaylistPos = CV2_PROFESSIONAL_GET_CURRENTLY_ACTIVE_V2_CORONA_POS(iPlayList)
							iCurrentMissionArrayPos = GET_ROCKSTAR_MISSION_FROM_ROOT_CONTENT_ID_HASH(g_sMPTunables.iFmCoronaPlaylistProffesionalRootContentId[iPlayList][iCurrentPlaylistPos])
							IF iCurrentMissionArrayPos != -1
								SET_MY_TRANSITION_SESSION_CONTENT_ID(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iCurrentMissionArrayPos].tlName)
								SET_TRANSITION_SESSIONS_QUICK_MATCH_MAX_PLAYERS(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iCurrentMissionArrayPos].iMaxPlayers)
								SET_TRANSITION_SESSIONS_SKIP_JOB_WARNING()
							#IF IS_DEBUG_BUILD
							ELSE
								PRINTLN("[TS] * QUICKMATCH PHONE - QUICKMATCH_SERIES_BUNKER - iCurrentMissionArrayPos = -1")
								#ENDIF
							ENDIF
						ENDIF
						PRINTLN("[TS] * QUICKMATCH PHONE - SELECTED QUICKMATCH_SERIES_BUNKER")
					BREAK
					
					CASE QUICKMATCH_SERIES_STUNT
						IF g_sMPTunables.bDisableStuntSeriesBucket
							iType = FMMC_TYPE_RACE
						ELSE
							iType = FMMC_TYPE_RACE_STUNT_FOR_QM
						ENDIF
						iCurrentPlaylistPos = CV2_GET_CURRENTLY_ACTIVE_V2_CORONA_POS()
						iCurrentMissionArrayPos = GET_ROCKSTAR_MISSION_FROM_ROOT_CONTENT_ID_HASH(CV2_GET_CURRENTLY_ACTIVE_V2_CORONA_ROOT_CONTENT_ID_HASH(iCurrentPlaylistPos))
						IF iCurrentMissionArrayPos != -1
							SET_MY_TRANSITION_SESSION_CONTENT_ID(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iCurrentMissionArrayPos].tlName)
							SET_TRANSITION_SESSIONS_QUICK_MATCH_MAX_PLAYERS(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iCurrentMissionArrayPos].iMaxPlayers)
							SET_TRANSITION_SESSIONS_SKIP_JOB_WARNING()
						#IF IS_DEBUG_BUILD
						ELSE
							PRINTLN("[TS] * QUICKMATCH PHONE - QUICKMATCH_SERIES_STUNT - iCurrentMissionArrayPos = -1")
							#ENDIF
						ENDIF
						PRINTLN("[TS] * QUICKMATCH PHONE - SELECTED QUICKMATCH_SERIES_STUNT")
					BREAK
					
					CASE QUICKMATCH_SERIES_ADVERSARY
						IF g_sMPTunables.bDisableStuntSeriesBucket
							iType = FMMC_TYPE_MISSION_NEW_VS
						ELSE
							iType = FMMC_TYPE_ADVERSARY_SERIES
						ENDIF
						iPlayList = CV2_GET_ADVERSARY_SERIES_ARRAY_POS()
						IF iPlayList != -1
							iCurrentPlaylistPos = CV2_PROFESSIONAL_GET_CURRENTLY_ACTIVE_V2_CORONA_POS(iPlayList)
							iCurrentMissionArrayPos = GET_ROCKSTAR_MISSION_FROM_ROOT_CONTENT_ID_HASH(g_sMPTunables.iFmCoronaPlaylistProffesionalRootContentId[iPlayList][iCurrentPlaylistPos])
							IF iCurrentMissionArrayPos != -1
								SET_MY_TRANSITION_SESSION_CONTENT_ID(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iCurrentMissionArrayPos].tlName)
								SET_TRANSITION_SESSIONS_QUICK_MATCH_MAX_PLAYERS(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iCurrentMissionArrayPos].iMaxPlayers)
								SET_TRANSITION_SESSIONS_SKIP_JOB_WARNING()
							#IF IS_DEBUG_BUILD
							ELSE
								PRINTLN("[TS] * QUICKMATCH PHONE - QUICKMATCH_SERIES_ADVERSARY - iCurrentMissionArrayPos = -1")
								#ENDIF
							ENDIF
						ENDIF
						PRINTLN("[TS] * QUICKMATCH PHONE - SELECTED QUICKMATCH_SERIES_ADVERSARY")
					BREAK
					
					CASE QUICKMATCH_SERIES_ACTIVE_PREMIUM_RACE
						iType = FMMC_TYPE_RACE
						iPlayList = CV2_GET_RACE_ARRAY_POS()
						iCurrentPlaylistPos = CV2_PROFESSIONAL_GET_CURRENTLY_ACTIVE_V2_CORONA_POS(iPlayList)
						iCurrentMissionArrayPos = GET_ROCKSTAR_MISSION_FROM_ROOT_CONTENT_ID_HASH(CV2_PROFESSIONAL_GET_CURRENTLY_ACTIVE_V2_CORONA_ROOT_CONTENT_ID_HASH(iCurrentPlaylistPos, iPlayList))
						IF iCurrentMissionArrayPos != -1
						AND g_sMPTunables.iProfesionalCoronaType[iPlayList] = FMMC_TYPE_RACE
							SET_MY_TRANSITION_SESSION_CONTENT_ID(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iCurrentMissionArrayPos].tlName)
							SET_TRANSITION_SESSIONS_QUICK_MATCH_MAX_PLAYERS(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iCurrentMissionArrayPos].iMaxPlayers)
							SET_TRANSITION_SESSIONS_SKIP_JOB_WARNING()
							SET_TRANSITION_SESSIONS_LAUNCHING_PROFESSIONAL_RACE()
						#IF IS_DEBUG_BUILD
						ELSE
							PRINTLN("[TS] * QUICKMATCH PHONE - QUICKMATCH_SERIES_ACTIVE_PREMIUM_RACE - iCurrentMissionArrayPos = -1")
						#ENDIF
						ENDIF
						PRINTLN("[TS] * QUICKMATCH PHONE - SELECTED QUICKMATCH_SERIES_ACTIVE_PREMIUM_RACE")
					BREAK
				ENDSWITCH
			BREAK
			
			CASE QUICKMATCH_MODE_HEISTS
				PRINTLN("[TS] * QUICKMATCH PHONE - SELECTED QUICKMATCH_MODE_HEISTS")
				SWITCH INT_TO_ENUM(enumQuickMatchHeists,m_HeistsList[iSelectedContent])
					#IF FEATURE_HEIST_ISLAND
					CASE QUICKMATCH_HEISTS_CAYO_PERICO
						iType = FMMC_TYPE_HEIST_ISLAND_FINALE
						SET_TRANSITION_SESSIONS_STARTING_HEIST_ISLAND_QUICK_MATCH()
						SET_TRANSITION_SESSIONS_STARTING_ON_CALL()
						SET_ON_CALL_QUICKMATCH_ATTEMPTS(0)
						PRINTLN("[TS] * QUICKMATCH PHONE - SELECTED QUICKMATCH_HEISTS_CAYO_PERICO")
					BREAK
					#ENDIF
					
					CASE QUICKMATCH_HEISTS_DIAMOND_CASINO
						iType = FMMC_TYPE_GB_CASINO_HEIST
						SET_TRANSITION_SESSIONS_STARTING_CASINO_HEIST_QUICK_MATCH()
						SET_TRANSITION_SESSIONS_STARTING_ON_CALL()
						SET_ON_CALL_QUICKMATCH_ATTEMPTS(0)
						PRINTLN("[TS] * QUICKMATCH PHONE - SELECTED QUICKMATCH_HEISTS_DIAMOND_CASINO")
					BREAK
					
					CASE QUICKMATCH_HEISTS_DOOMSDAY
						iType = FMMC_TYPE_FM_GANGOPS
						SET_TRANSITION_SESSIONS_STARTING_GANG_OPS_QUICK_MATCH()
						SET_TRANSITION_SESSIONS_STARTING_ON_CALL()
						SET_ON_CALL_QUICKMATCH_ATTEMPTS(0)
						INT iGangOpsType
						iGangOpsType = GET_RANDOM_HEIST_QUICK_MATCH_TYPE(TRUE)
						IF iGangOpsType = FM_TRAN_ACTIVITY_ID_GO_FIN
							iType = FMMC_TYPE_FM_GANGOPS_FIN
						ENDIF
						SET_TRANSITION_SESSION_HEIST_QUICK_MATCH_TYPE(iGangOpsType)
						PRINTLN("[TS] * QUICKMATCH PHONE - SELECTED QUICKMATCH_HEISTS_DOOMSDAY")
					BREAK
					
					CASE QUICKMATCH_HEISTS_HEIST
						iType = GET_CURRENT_HEIST_ROO_CONTENT_ID_HASH()
						SET_TRANSITION_SESSIONS_STARTING_HEIST_QUICK_MATCH()
						SET_TRANSITION_SESSIONS_STARTING_ON_CALL()
						SET_ON_CALL_QUICKMATCH_ATTEMPTS(0)
						SET_TRANSITION_SESSION_HEIST_QUICK_MATCH_TYPE(GET_RANDOM_HEIST_QUICK_MATCH_TYPE())
						PRINTLN("[TS] * QUICKMATCH PHONE - SELECTED QUICKMATCH_HEISTS_HEIST")
					BREAK
				ENDSWITCH
			BREAK
			
			CASE QUICKMATCH_MODE_ACTIVITIES
				PRINTLN("[TS] * QUICKMATCH PHONE - SELECTED QUICKMATCH_MODE_ACTIVITIES")
				SWITCH INT_TO_ENUM(enumQuickMatchActivities,m_ActivitiesList[iSelectedContent])
					CASE QUICKMATCH_ACTIVITIES_ARM
						iType = FMMC_TYPE_MG_ARM_WRESTLING
						PRINTLN("[TS] * QUICKMATCH PHONE - SELECTED ARM")
					BREAK
				
					CASE QUICKMATCH_ACTIVITIES_DARTS
						iType = FMMC_TYPE_MG_DARTS
						PRINTLN("[TS] * QUICKMATCH PHONE - SELECTED DARTS")
					BREAK
					
					CASE QUICKMATCH_ACTIVITIES_GOLF	
						iType = FMMC_TYPE_MG_GOLF
						PRINTLN("[TS] * QUICKMATCH PHONE - SELECTED GOLF")
					BREAK
					
					CASE QUICKMATCH_ACTIVITIES_RANGE
						iType = FMMC_TYPE_MG_SHOOTING_RANGE
					PRINTLN("[TS] * QUICKMATCH PHONE - SELECTED RANGE")
					BREAK
					
					CASE QUICKMATCH_ACTIVITIES_TENNIS
						iType = FMMC_TYPE_MG_TENNIS
						PRINTLN("[TS] * QUICKMATCH PHONE - SELECTED TENNIS")
					BREAK
				ENDSWITCH
			BREAK
			
			CASE QUICKMATCH_MODE_CONTACT_MISSIONS
				PRINTLN("[TS] * QUICKMATCH PHONE - SELECTED QUICKMATCH_MODE_CONTACT_MISSIONS")
				SWITCH INT_TO_ENUM(enumQuickMatchContactMissions,m_ContactMissionsList[iSelectedContent])
				
					CASE QUICKMATCH_CONTACT_MISSIONS_SHORT_TRIP
						iType = FMMC_TYPE_FIXER_SHORT_TRIP
						SET_TRANSITION_SESSIONS_STARTING_SHORT_TRIP_QUICK_MATCH()
						SET_TRANSITION_SESSIONS_STARTING_ON_CALL()
						SET_ON_CALL_QUICKMATCH_ATTEMPTS(0)
						PRINTLN("[TS] * QUICKMATCH PHONE - SELECTED QUICKMATCH_CONTACT_MISSIONS_SHORT_TRIP")
					BREAK
					
					CASE QUICKMATCH_CONTACT_MISSIONS_AUTO_SHOP
						iType = FMMC_TYPE_TUNER_ROBBERY_FINALE
						SET_TRANSITION_SESSIONS_STARTING_TUNER_ROBBERY_QUICK_MATCH()
						SET_TRANSITION_SESSIONS_STARTING_ON_CALL()
						SET_ON_CALL_QUICKMATCH_ATTEMPTS(0)
						PRINTLN("[TS] * QUICKMATCH PHONE - SELECTED QUICKMATCH_CONTACT_MISSIONS_AUTO_SHOP")
					BREAK
					
					CASE QUICKMATCH_CONTACT_MISSIONS_CONTACT
						iType = FMMC_TYPE_MISSION
						PRINTLN("[TS] * QUICKMATCH PHONE - SELECTED QUICKMATCH_CONTACT_MISSIONS_CONTACT")
					BREAK
					
					CASE QUICKMATCH_CONTACT_MISSIONS_LAMAR
						iType = FMMC_TYPE_LOWRIDER_INTRO
						PRINTLN("[TS] * QUICKMATCH PHONE - SELECTED QUICKMATCH_CONTACT_MISSIONS_LAMAR")
					BREAK
					
					CASE QUICKMATCH_CONTACT_MISSIONS_MOBILE_OPERATION
						SET_TRANSITION_SESSIONS_STARTING_SVM_QUICK_MATCH()
						SET_TRANSITION_SESSIONS_STARTING_ON_CALL()
						SET_ON_CALL_QUICKMATCH_ATTEMPTS(0)
						iType = FMMC_TYPE_GUNRUNNING_WEAPONIZED_VEHICLE
						PRINTLN("[TS] * QUICKMATCH PHONE - QUICKMATCH_CONTACT_MISSIONS_MOBILE_OPERATION")
					BREAK
					
					CASE QUICKMATCH_CONTACT_MISSIONS_SPECIAL_VEHICLE
						SET_TRANSITION_SESSIONS_STARTING_SVM_QUICK_MATCH()
						SET_TRANSITION_SESSIONS_STARTING_ON_CALL()
						SET_ON_CALL_QUICKMATCH_ATTEMPTS(0)
						iType = FMMC_TYPE_SPECIAL_VEHICLE_MISSION
						PRINTLN("[TS] * QUICKMATCH PHONE - QUICKMATCH_CONTACT_MISSIONS_SPECIAL_VEHICLE")
					BREAK
				ENDSWITCH
			BREAK
			
			CASE QUICKMATCH_MODE_JOBS
				PRINTLN("[TS] * QUICKMATCH PHONE - SELECTED QUICKMATCH_MODE_JOBS")
				SWITCH INT_TO_ENUM(enumQuickMatchJobs,m_JobsList[iSelectedContent])
					CASE QUICKMATCH_JOBS_CAPTURE
						iType = FMMC_TYPE_MISSION_CTF
						PRINTLN("[TS] * QUICKMATCH PHONE - SELECTED QUICKMATCH_JOBS_CAPTURE")
					BREAK
				
					CASE QUICKMATCH_JOBS_DEATHMATCH
						iType = FMMC_TYPE_DEATHMATCH
						PRINTLN("[TS] * QUICKMATCH PHONE - SELECTED QUICKMATCH_JOBS_DEATHMATCH")
					BREAK
					
					CASE QUICKMATCH_JOBS_LTS
						iType = FMMC_TYPE_MISSION_LTS
						PRINTLN("[TS] * QUICKMATCH PHONE - SELECTED QUICKMATCH_JOBS_LTS")
					BREAK
					
					CASE QUICKMATCH_JOBS_PARACHUTING
						iType = FMMC_TYPE_BASE_JUMP 
						PRINTLN("[TS] * QUICHMATCH PHONE - SELECTED QUICKMATCH_JOBS_PARACHUTING")
					BREAK
					
					CASE QUICKMATCH_JOBS_RACE
						iType = FMMC_TYPE_RACE
						PRINTLN("[TS] * QUICKMATCH PHONE - SELECTED QUICKMATCH_JOBS_RACE")
					BREAK
					
					CASE QUICKMATCH_JOBS_SURVIVAL
						iType = FMMC_TYPE_SURVIVAL	
						PRINTLN("[TS] * QUICKMATCH PHONE - SELECTED QUICKMATCH_JOBS_SURVIVAL")
					BREAK
					
					CASE QUICKMATCH_JOBS_VERSUS
						iType = FMMC_TYPE_MISSION_VS
						PRINTLN("[TS] * QUICKMATCH PHONE - SELECTED QUICKMATCH_JOBS_VERSUS")
					BREAK
				ENDSWITCH
			BREAK
			
		ENDSWITCH
		PRINTLN("[TS] ***************************************")
		Play_Select_Beep()
		IF (m_currentMode = QUICKMATCH_MODE_HEISTS AND m_HeistsList[iSelectedContent] = ENUM_TO_INT(QUICKMATCH_HEISTS_HEIST))
		OR (m_currentMode = QUICKMATCH_MODE_HEISTS AND m_HeistsList[iSelectedContent] = ENUM_TO_INT(QUICKMATCH_HEISTS_DOOMSDAY))
		AND NOT g_sMPTunables.bAutoJoinHeistsDisabled
			//If we selected to join on call direct then set that we did so
			IF iselectedFriendsOnCallSlot = ciFRIENDS_OPTION_WITH
			AND bDisplayWithFriends
				SET_TRANSITION_SESSIONS_STARTING_QUICK_MATCH_WITH_LOCAL_FRIENDS()	
				FILL_FRIENDS_QUICK_MATCH_ARRAY()
			ELIF iselectedFriendsOnCallSlot = ciFRIENDS_OPTION_DIRECT
			OR (iselectedFriendsOnCallSlot = ciFRIENDS_OPTION_ON_CALL
			AND bDisplayWithFriends = FALSE)
				SET_TRANSITION_SESSIONS_STARTING_HEIST_QUICK_MATCH_DIRECT()
			ENDIF
		ELSE
			//If we selected the friends slot then set that we are starting a quick match with friends
			IF iselectedFriendsOnCallSlot = ciFRIENDS_OPTION_WITH
			//But only if we should be displaying with friends
			AND bDisplayWithFriends
				SET_TRANSITION_SESSIONS_STARTING_QUICK_MATCH_WITH_LOCAL_FRIENDS()	
				FILL_FRIENDS_QUICK_MATCH_ARRAY()
			ENDIF
		ENDIF
		//If we should always go on call
		IF g_sMPTunables.bQuickJobAlwaysOnCall
		AND NOT IS_THIS_A_SOLO_SESSION()
			SET_TRANSITION_SESSIONS_STARTING_ON_CALL()
			SET_ON_CALL_QUICKMATCH_ATTEMPTS(0)
		//If on call is not disabled
		ELIF g_sMPTunables.bOnCallDisabled = FALSE
		AND bPlayerIsDrunk = FALSE
			//If we selected to start an on call then set the flag
			IF (iselectedFriendsOnCallSlot = ciFRIENDS_OPTION_ON_CALL
			AND bDisplayWithFriends)
			//Or it was one less that on call and we're not showing the with friends option
			OR (iselectedFriendsOnCallSlot = (ciFRIENDS_OPTION_ON_CALL - 1)
			AND NOT bDisplayWithFriends)
				SET_TRANSITION_SESSIONS_STARTING_ON_CALL()
				SET_ON_CALL_QUICKMATCH_ATTEMPTS(0)
			ENDIF
		ENDIF
		//If we selected any then set that flag
		IF iType = ciTRANSITION_SESSIONS_ACTIVITY_TYPE_ANY
			SET_FM_JOB_ENTERY_TYPE(ciMISSION_ENTERY_TYPE_PHONE_RANDOM)
		ELIF AM_I_TRANSITION_SESSIONS_STARTING_ON_CALL()
			SET_FM_JOB_ENTERY_TYPE(ciMISSION_ENTERY_TYPE_ON_CALL)
		ELSE
			SET_FM_JOB_ENTERY_TYPE(ciMISSION_ENTERY_TYPE_PHONE)
		ENDIF
		
		//If we are not starting a ranked quick match then 
		IF NOT AM_I_TRANSITION_SESSIONS_STARTING_ON_CALL()
			INT iCost = GET_CONTACT_REQUEST_COST(REQUEST_REMOVE_WANTED)
			//If we can afford to pay to lose the wanted then pay
			IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
				INT iDifference
				IF NOT  NETWORK_CAN_SPEND_MONEY2(iCost, FALSE, TRUE, FALSE, iDifference)
					iCost-=iDifference
				ENDIF
				IF iCost > 0
					INT iScriptTransactionIndex
					GAMER_HANDLE gamerHandle
					gamerHandle = GET_GAMER_HANDLE_PLAYER(PLAYER_ID())
					GIVE_LOCAL_PLAYER_FM_CASH(-iCost, 1, TRUE, 0)
					
					IF USE_SERVER_TRANSACTIONS()
						// Trigger a cash transaction for PC Build (2151163)
						TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_SPEND_LOSE_WANTED_LEVEL, iCost, iScriptTransactionIndex, FALSE, TRUE)
						g_cashTransactionData[iScriptTransactionIndex].eExtraData.gamerHandle = gamerHandle
					ELSE
						NETWORK_SPENT_BUY_WANTEDLEVEL(iCost, gamerHandle, FALSE, TRUE)
					ENDIF
				ENDIF
			ENDIF
			//Set the flag to say that I'm doing a quick match
			SET_TRANSITION_SESSIONS_STARTING_QUICK_MATCH()			
			//Set that I need to warp to the start of the mission
			SET_TRANSITION_SESSIONS_NEED_TO_WARP_TO_START_SKYCAM()
			SET_PLAYER_LEAVING_CORONA_VECTOR(GET_PLAYER_PERCEIVED_COORDS(PLAYER_ID()))
			NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_NO_COLLISION | NSPC_CAN_BE_TARGETTED | NSPC_FREEZE_POSITION | NSPC_USE_PLAYER_FADE_OUT) 
			SET_NEWS_FEED_SPLASH_SCREEN_ACTIVE(TRUE, TRUE)
			//Set freemode controller state
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmLauncherGameState = FMMC_LAUNCHER_STATE_LOAD_MISSION_FOR_TRANSITION_SESSION
			PRINTLN("[TS]... SET_FM_LAUNCHER_PLAYER_GAME_STATE - FMMC_LAUNCHER_STATE_LOAD_MISSION_FOR_TRANSITION_SESSION")
		ENDIF
		//Set the type of quick match
		IF iType = FM_GAME_MODE_PLAYLIST
			SET_TRANSITION_SESSIONS_DOING_QUICK_MATCH_PLAYLIST()
		ELIF iType = ciTRANSITION_SESSIONS_ACTIVITY_TYPE_ANY
			SET_TRANSITION_SESSIONS_QUICK_MATCH_TYPE_RANDOM()
		ELSE
			SET_TRANSITION_SESSIONS_QUICK_MATCH_TYPE(iType)
		ENDIF
		CLEAR_PAUSE_MENU_IS_USING_UGC()
		//[RANKED_RACES]
		IF AM_I_TRANSITION_SESSIONS_STARTING_ON_CALL()
			HANG_UP_AND_PUT_AWAY_PHONE(FALSE)
			Cleanup_and_Terminate()
		ENDIF
	ENDIF
		
	// B*2280733 - Reset the debouncing
	bDebounceOfferJobAccept = FALSE

ENDPROC


PROC Process_Stage_FRIENDS_OR_SELF_OR_ON_CALL()
	Check_for_List_Navigation()

	// KGM 6/7/13: If there is a focus mission then put the phone away
	// (this means the player has probably just walked into a corona, so avoid trying to launch another mission)
	IF (Is_There_A_MissionsAtCoords_Focus_Mission())
		HANG_UP_AND_PUT_AWAY_PHONE()
		bBlockAccept = FALSE
		EXIT
	ENDIF

	IF iselectedModeSlot < 0
	OR iselectedModeSlot >= ENUM_TO_INT(QUICKMATCH_MODE_MAX)
		IF g_CellPhone.PhoneDS > PDS_AWAY
			g_Cellphone.PhoneDS = PDS_RUNNINGAPP
			PRINTLN("STATE ASSIGNMENT 219. appJIPMP::Process_Stage_Offer_join() assigns PDS_RUNNINGAPP")
		ENDIF
		Display_First_QuickMatch_Screen()
		m_currentStage= QUICKMATCH_MODE_LIST
		bBlockAccept = FALSE
		EXIT
	ENDIF

	// Check if the player wants to go BACK to JobList
	IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_NEGATIVE_INPUT))
		g_Cellphone.PhoneDS = PDS_COMPLEXAPP
		Display_First_QuickMatch_Screen()
		m_selectedQuickMatchSlot = 0		
		m_currentStage= QUICKMATCH_MODE_LIST
		Play_Back_Beep()
		bBlockAccept = FALSE
		EXIT
	ENDIF

	IF iCanDoQuickmatch = 0
	AND SlotsUsed > 0 //Need this to prevent "Not available" from being selected.
	AND NOT bBlockAccept
		IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_POSITIVE_INPUT))	//Player selects a job type from the list.
			PRINTLN("appJIPMP - Process_Stage_FRIENDS_OR_SELF_OR_ON_CALL - PHONE_POSITIVE_INPUT")
			g_InputButtonJustPressed = TRUE
			Play_Select_Beep()
			BEGIN_SCALEFORM_MOVIE_METHOD (SF_MovieIndex, "GET_CURRENT_SELECTION")
			Choice_ReturnedSFIndex = END_SCALEFORM_MOVIE_METHOD_RETURN_VALUE()
			WHILE NOT IS_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_READY (Choice_ReturnedSFIndex)
				WAIT (0)
				PRINTLN("appJIPMP - Process_Stage_FRIENDS_OR_SELF_OR_ON_CALL - appJIPMP - Waiting on scaleform return during selecting mode.")
			ENDWHILE
			iselectedFriendsOnCallSlot	= GET_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_INT (Choice_ReturnedSFIndex)
			m_currentStage= QUICKMATCH_OFFER_JOIN
			g_Cellphone.PhoneDS = PDS_COMPLEXAPP
			IF (m_currentMode= QUICKMATCH_MODE_CONTACT_MISSIONS AND m_ContactMissionsList[iSelectedContent]  = ENUM_TO_INT(QUICKMATCH_CONTACT_MISSIONS_SPECIAL_VEHICLE))
			OR (m_currentMode= QUICKMATCH_MODE_CONTACT_MISSIONS AND m_ContactMissionsList[iSelectedContent]  = ENUM_TO_INT(QUICKMATCH_CONTACT_MISSIONS_MOBILE_OPERATION))
				PROCESS_LAUNCH_QUICKMATCH()
			ELSE
				Display_Confirm_Screen()
			ENDIF
			bBlockAccept = FALSE
		ENDIF
	ELSE
		//#1492475
		IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_POSITIVE_INPUT))
		AND NOT	bBlockAccept
			g_InputButtonJustPressed = TRUE
			Play_Back_Beep()
		ENDIF
	ENDIF
ENDPROC

PROC Process_Stage_Offer_join()

	// KGM 6/7/13: If there is a focus mission then put the phone away
	// (this means the player has probably just walked into a corona, so avoid trying to launch another mission)
	IF (Is_There_A_MissionsAtCoords_Focus_Mission())
		bBlockAccept = FALSE		
		HANG_UP_AND_PUT_AWAY_PHONE()
		EXIT
	ENDIF

	IF iselectedModeSlot < 0
	OR iselectedModeSlot >= ENUM_TO_INT(QUICKMATCH_MODE_MAX)
		IF g_CellPhone.PhoneDS > PDS_AWAY
			g_Cellphone.PhoneDS = PDS_RUNNINGAPP
			PRINTLN("STATE ASSIGNMENT 219. appJIPMP::Process_Stage_Offer_join() assigns PDS_RUNNINGAPP")
		ENDIF
		Clear_Accept_Invite_Warning_Help_Text_If_On_Display()
		Display_First_QuickMatch_Screen()
		m_currentStage= QUICKMATCH_MODE_LIST
		bBlockAccept = FALSE		
		EXIT
	ENDIF

	// Check if the player wants to go BACK to JobList
	IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_NEGATIVE_INPUT))
		bBlockAccept = FALSE		
		Clear_Accept_Invite_Warning_Help_Text_If_On_Display()
		g_Cellphone.PhoneDS = PDS_COMPLEXAPP
		IF ARE_ANY_FRIENDS_INT_THIS_SESSION()
		AND NOT g_sMPTunables.bQuickJobAlwaysOnCall
			Display_Friends_Or_On_Call_Screen()
			m_currentStage= QUICKMATCH_FRIENDS_OR_SELF_OR_ON_CALL
		ELSE
			Display_First_QuickMatch_Screen()
			m_selectedQuickMatchSlot = 0		
			m_currentStage= QUICKMATCH_MODE_LIST
		ENDIF
		iselectedFriendsOnCallSlot = -1
		Play_Back_Beep()
		EXIT
	ENDIF
	
	Display_Accept_Invite_Warning_Help_Text_If_Required()
	
	// B*2280733 - I'm adding debouncing to this, so the script will not end until the button has been released. This avoids weapons firing while button is still held down in PC version.
	IF bDebounceOfferJobAccept = FALSE
		IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_POSITIVE_INPUT)) //Player confirms they want to start the job...
			bDebounceOfferJobAccept = TRUE
		ENDIF
	ELSE
		IF Is_Phone_Control_Just_Released (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_POSITIVE_INPUT)) //Don't exit script until button release (avoids firing weapon)...
		AND NOT	bBlockAccept
			PROCESS_LAUNCH_QUICKMATCH()
		ENDIF
	ENDIF

ENDPROC

PROC MAINTAIN_NETWORK_SCRIPT_BAIL(BOOL bInitialBail = FALSE)
	// Make sure still in the network game
	IF NOT (NETWORK_IS_IN_SESSION())
		PRINTLN("NETWORK_IS_IN_SESSION() returned FALSE - Disabling cellphone and terminating App")

		// Disable the cellphone
		DISABLE_CELLPHONE(TRUE)	

		IF bInitialBail
			//Reset the transition session clean up flag
			g_TransitionSessionNonResetVars.sTransVars.bCleanedUpAndWait = FALSE
		ENDIF

		//[RANKED_RACES]
		//CLEAR_TRANSITION_SESSIONS_STARTING_RANKED_QUICK_MATCH()
		CLEAR_TRANSITION_SESSIONS_STARTING_ON_CALL()

		Cleanup_and_Terminate()
	ENDIF
ENDPROC

// ===========================================================================================================
//		Main Loop
// ===========================================================================================================

SCRIPT

	PRINTLN("appJIPMP ************* ************* *************")
	PRINTLN("appJIPMP * STARTINGT - ", GET_CLOUD_TIME_AS_INT(), ", F - ", GET_FRAME_COUNT(), " *")
	PRINTLN("appJIPMP ************* ************* *************")
	// Ensure script can run in network game
	NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()
	iCanDoQuickmatch = CAN_DO_QUICK_MATCH(stQuickMatchTimer)
	IF iCanDoQuickmatch > 0
		PRINTLN("appJIPMP - CAN_DO_QUICK_MATCH = ", iCanDoQuickmatch, ", Quick Job functionality is blocked.")
	ENDIF
	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_OnCallDisabled")
		g_sMPTunables.bOnCallDisabled = TRUE
	ENDIF
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_OnCallFriendsDisabled")
		g_sMPTunables.bOnCallFriendsDisabled = TRUE
	ENDIF
	#ENDIF
	CLEAR_MY_TRANSITION_SESSION_ELO()

	// DO THIS FIRST: Initialise the MP Cellphone App public header
	Initialise_MP_App_Variables(m_sAppData)
	//[RANKED_RACES]
	//Check to see if we are cleaning up after a quickmatch
	IF AM_I_TRANSITION_SESSIONS_STARTING_ON_CALL()	
		IF IS_TRANSITION_SESSION_LAUNCHING()
			PRINTLN("[TS] appJIPMP - IS_TRANSITION_SESSION_LAUNCHING - Cleanup_and_Terminate")
			Cleanup_and_Terminate()
		ELSE
			//Clean up the old transition session
			CLEANUP_AFTER_RANKED_QUICKMATCH_CANCEL(TRUE)	
			IF(GET_FM_JOB_ENTERY_TYPE() = ciMISSION_ENTERY_TYPE_V2_PROFESSIONAL_CORONA_ON_CALL
			OR GET_FM_JOB_ENTERY_TYPE() = ciMISSION_ENTERY_TYPE_V2_CORONA_ON_CALL
			OR GET_FM_JOB_ENTERY_TYPE() = ciMISSION_ENTERY_TYPE_V2_ADVERSARY_CORONA_ON_CALL)
			OR GET_FM_JOB_ENTERY_TYPE() = ciMISSION_ENTERY_TYPE_ON_CALL
			OR GET_FM_JOB_ENTERY_TYPE() = ciMISSION_ENTERY_TYPE_ADVERSARY_SERIES_CORONA_ON_CALL
			OR GET_FM_JOB_ENTERY_TYPE() = ciMISSION_ENTERY_TYPE_ADVERSARY_SERIES_CORONA_ON_CALL_1
			OR GET_FM_JOB_ENTERY_TYPE() = ciMISSION_ENTERY_TYPE_ADVERSARY_SERIES_CORONA_ON_CALL_2
			OR GET_FM_JOB_ENTERY_TYPE() = ciMISSION_ENTERY_TYPE_SPECIAL_VEHICLE_SERIES_CORONA_ON_CALL
			OR GET_FM_JOB_ENTERY_TYPE() = ciMISSION_ENTERY_TYPE_BUNKER_SERIES_CORONA_ON_CALL
			OR GET_FM_JOB_ENTERY_TYPE() = ciMISSION_ENTERY_TYPE_TRANSFORM_SERIES_CORONA_ON_CALL
			OR GET_FM_JOB_ENTERY_TYPE() = ciMISSION_ENTERY_TYPE_TARGET_ASSAULT_SERIES_CORONA_ON_CALL
			OR GET_FM_JOB_ENTERY_TYPE() = ciMISSION_ENTERY_TYPE_HOTRING_SERIES_CORONA_ON_CALL
			OR GET_FM_JOB_ENTERY_TYPE() = ciMISSION_ENTERY_TYPE_ARENA_SERIES_CORONA_ON_CALL
			OR GET_FM_JOB_ENTERY_TYPE() = ciMISSION_ENTERY_TYPE_ARENA_SERIES_CORONA_WALL_ON_CALL
			OR GET_FM_JOB_ENTERY_TYPE() = ciMISSION_ENTERY_TYPE_RACE_SERIES_CORONA_ON_CALL
			OR GET_FM_JOB_ENTERY_TYPE() = ciMISSION_ENTERY_TYPE_SURVIVAL_SERIES_CORONA_ON_CALL
			OR GET_FM_JOB_ENTERY_TYPE() = ciMISSION_ENTERY_TYPE_OPEN_WHEEL_SERIES_CORONA_ON_CALL
			OR GET_FM_JOB_ENTERY_TYPE() = ciMISSION_ENTERY_TYPE_STREET_RACE_SERIES_CORONA_ON_CALL
			OR GET_FM_JOB_ENTERY_TYPE() = ciMISSION_ENTERY_TYPE_PURSUIT_SERIES_CORONA_ON_CALL
				CLEAR_FM_JOB_ENTERY_TYPE()
			ENDIF
			//Set a flag to clean the vars in fmmc_launcher
			SET_TRANSITION_SESSIONS_CLEANUP_AFTER_ON_CALL()
			//If we are in an activity session thenKill it
			BOOL bInTransition = NETWORK_IS_IN_TRANSITION() 
			IF bInTransition
			OR NETWORK_IS_TRANSITION_STARTED()
			OR NETWORK_IS_TRANSITION_BUSY()
			OR NETWORK_IS_TRANSITION_MATCHMAKING()
			OR NETWORK_IS_TRANSITION_LEAVE_POSTPONED()
				IF g_sMPTunables.bAPPJIPMP_CANCEL_MATCHMAKING_ON_NETWORK_BAIL
					PRINTLN("[TS] appJIPMP - NETWORK_CANCEL_TRANSITION_MATCHMAKING")
					NETWORK_CANCEL_TRANSITION_MATCHMAKING()
				ENDIF
				IF bInTransition
					PRINTLN("[TS] appJIPMP - NETWORK_BAIL_TRANSITION")
					NETWORK_BAIL_TRANSITION(ENUM_TO_INT(NETWORK_BAIL_TS_APPJIPMP))
				ENDIF
			ENDIF
			CLEAR_ROOT_CONTENT_ID_IF_SAFE()
			RESET_ALL_TRANSITION_SESSION_VARS()
			CLEAR_PLAYLIST_BITSET()
			//Set a flag to clean the vars in fmmc_launcher
			SET_TRANSITION_SESSIONS_CLEANUP_AFTER_ON_CALL()
		ENDIF
	ENDIF

	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
	AND Is_Ped_Drunk(PLAYER_PED_ID())
		bPlayerIsDrunk = TRUE
	ENDIF

	//Reset the transition session clean up flag
	g_TransitionSessionNonResetVars.sTransVars.bCleanedUpAndWait = FALSE
	g_sDLCContentContollerStruct.iOnCallBitset = 0
	
	// Main Loop - never terminates while the app can legitimately still run
	WHILE TRUE
		WAIT(0)

		//[RANKED_RACES]
		//We need to get that elo data!
		IF sGetEloData.bGotElo = FALSE
			IF GET_LOCAL_PLAYER_ELO()
				sGetEloData.bGotElo = TRUE
			ENDIF
		ENDIF

		MAINTAIN_NETWORK_SCRIPT_BAIL()

		iCanDoQuickmatch = CAN_DO_QUICK_MATCH(stQuickMatchTimer) //comment out to quickly test no quick jobs available. 

		// Need to handle interrupting the app if a cellphone call comes through
		// KGM 7/5/12: Also need to ensure a request for AWAY or DISABLED prevents any processing within the App


		IF NOT (g_Cellphone.PhoneDS = PDS_ONGOING_CALL)
		AND (g_CellPhone.PhoneDS > PDS_AWAY)

			// Deal with the various stages of the app
			SWITCH (m_currentStage)
				// Initial App screen
				CASE QUICKMATCH_MODE_LIST
					SWITCH m_iStageState
						CASE 0
							// Display the First App Screen when the app starts
							// NOTE: Display First App Screen should set the cellphone state as RunningApp
							Display_First_QuickMatch_Screen()
							
							// List navigation initial values
							m_selectedQuickMatchSlot = 0
							
							m_iStageState++
						BREAK
						
						CASE 1
							Process_Stage_Mode_List()
							
							// This will automatically exit the cellphone if the back button is pressed.
							// If you move two screens deep into your app, then you must set your app state, g_Cellphone.PhoneDS to PDS_COMPLEXAPP so this doesn't return true.
							IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_NEGATIVE_INPUT))
								//g_Cellphone.PhoneDS = PDS_RUNNINGAPP
								IF (CHECK_FOR_APPLICATION_EXIT())
									PRINTLN("CHECK_FOR_APPLICATION_EXIT() returned TRUE")
									Cleanup_and_Terminate()
								ENDIF
							ENDIF
						BREAK
					ENDSWITCH
					BREAK

				//Sublists.	 
				CASE QUICKMATCH_SUBLIST
					Process_Stage_Sublist()
					BREAK

				//With friends or alone.	 
				CASE QUICKMATCH_FRIENDS_OR_SELF_OR_ON_CALL
					Process_Stage_FRIENDS_OR_SELF_OR_ON_CALL()
					BREAK

				// Second App Screen (if needed)
				CASE QUICKMATCH_OFFER_JOIN
					Process_Stage_Offer_join()
					BREAK

				DEFAULT
					#IF IS_DEBUG_BUILD
					SCRIPT_ASSERT("Unknown MP JIPMP Stage.")
					PRINTNL()
					#ENDIF
				BREAK
			ENDSWITCH
	
		ELSE
			//Update: Incoming calls can't currently go through to the phone whilst an app is running. If this changes, we'll need to reinstate it.
			//Reinstate the block below to make the phone terminate this script when a phonecall comes through so that the phone will
			//return to the main menu post-phonecall.
			//This is an important section! It tells this script what to do when a call, be it answerphone or other phonecall, comes through.
			//BeforeCallPhoneDS = PDS_MAXIMUM
			//Cleanup_and_Terminate()
		ENDIF

		//Critical! Do not remove. This must run every frame as it handles bad things such as the player dying when the phone is active.
		IF CHECK_FOR_ABNORMAL_EXIT()
			PRINTLN("CHECK_FOR_ABNORMAL_EXIT() returned TRUE")
			Cleanup_and_Terminate()
		ENDIF
	ENDWHILE
ENDSCRIPT

