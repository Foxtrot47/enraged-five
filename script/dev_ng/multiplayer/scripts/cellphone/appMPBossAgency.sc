USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_script.sch"
USING "net_app_public.sch"
USING "net_mission_joblist.sch"

USING "net_gang_boss.sch"

// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************
//
//      MISSION NAME    :   appMPBossAgency.sc
//
//      Steve Taylor note: This is a clone of appMPJobListNew.sc. The phone launches this script for the Boss Agency invite app.
//      This was requested in bug 2554214, but it's an MP thing, nothing to do with the phone itself. You'll probably need to use this as
//      a basis as it uses the same scaleform view states.
//
//
//      AUTHOR          :   James A
//      DESCRIPTION     :   Controls the display and selection of the Multiplayer Boss Agency app.
//
// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************


// ===========================================================================================================
//      Variables and Constants
// ===========================================================================================================

// Boss Agency Screen Stages
ENUM enumBossAgencyStages
    BOSS_APP_INVITES,                               // The Boss Agency Invite Page
	BOSS_APP_CONFIRM_INVITE,						// Confirmation screen when accepting a Boss Invite
	BOSS_APP_GOON,									// Goon screen when you are already part of an Organization
	BOSS_APP_PASSIVE_MODE_WARNING					// Warning Screen for Passive Mode		
ENDENUM

enumBossAgencyStages   m_currentStage = BOSS_APP_INVITES

// -----------------------------------------------------------------------------------------------------------

// NOTE: The JobList screen, which is view state 25
CONST_INT       BOSS_AGENCY_VIEW_STATE          25

// NOTE: The Job screen currently uses the Text Message View screen, which is view state 26
CONST_INT       BOSS_AGENCY_INVITE_VIEW_STATE	26

CONST_INT		BOSS_AGENCY_INVALID_SELECTION 	-1


// -----------------------------------------------------------------------------------------------------------

// The Scaleform Text Labels used for button text
TEXT_LABEL_15   m_jobListIconTextBack           = "CELL_206"
TEXT_LABEL_15   m_jobListIconTextOptions        = "CELL_214"
TEXT_LABEL_15   m_jobListIconTextAcceptJob      = "CELL_243"
TEXT_LABEL_15   m_jobListIconTextCancelJob      = "CELL_244"


// -----------------------------------------------------------------------------------------------------------

// Text used within JobList app
TEXT_LABEL_15   m_invitationText                = "CELL_265"

// -----------------------------------------------------------------------------------------------------------
// Contact Pictures
TEXT_LABEL_15   m_defaultContactPicture         = "CELL_300"

// -----------------------------------------------------------------------------------------------------------
// The Array positionon the boss agency invite app
INT             m_selectedBossAgencyInvite              = 0
PLAYER_INDEX playerForInvite

BOOL bAcceptPressed = FALSE

// -----------------------------------------------------------------------------------------------------------

// An instance of the Mp App data struct
structMPAppData m_sAppData

CONST_INT   SWITCH_TO_PLEASE_WAIT_TIMEOUT_msec      5000

#IF IS_DEBUG_BUILD
// Debug only variables
INT m_debugColour = -1
BOOL bDummyInvite
#ENDIF

// ===========================================================================================================
//      Cleanup Routines
// ===========================================================================================================

// PURPOSE: Cleanly terminates the script
PROC Cleanup_and_Terminate()

	g_sBossAgencyApp.bBossAgencyAppRunningWarningScreen = FALSE

    TERMINATE_THIS_THREAD()
ENDPROC

/// PURPOSE:
///    Returns a player name with the condensed gamertag font used
FUNC TEXT_LABEL_63 Get_Name_String_For_Boss_Agency_App(PLAYER_INDEX playerID)

	TEXT_LABEL_63 tlPlayerName = "<C>"
	tlPlayerName += GET_PLAYER_NAME(playerID)
	tlPlayerName += "</C>"
	
	RETURN tlPlayerName
ENDFUNC


#IF IS_DEBUG_BUILD

FUNC HUD_COLOURS DEBUG_GET_OVERRIDDEN_HUD_COLOUR()
	
	SWITCH m_debugColour
		CASE 0		RETURN HUD_COLOUR_G1
		CASE 1		RETURN HUD_COLOUR_G2
		CASE 2		RETURN HUD_COLOUR_G3
		CASE 3		RETURN HUD_COLOUR_G4
		CASE 4		RETURN HUD_COLOUR_G5
		CASE 5		RETURN HUD_COLOUR_G6
		CASE 6		RETURN HUD_COLOUR_G7
		CASE 7		RETURN HUD_COLOUR_G8
		CASE 8		RETURN HUD_COLOUR_G9
		CASE 9		RETURN HUD_COLOUR_G10
		CASE 10		RETURN HUD_COLOUR_G11
		CASE 11		RETURN HUD_COLOUR_G12
		CASE 12		RETURN HUD_COLOUR_G13
		CASE 13		RETURN HUD_COLOUR_G14
		CASE 14		RETURN HUD_COLOUR_G15
	ENDSWITCH
	
	RETURN HUD_COLOUR_WHITE
ENDFUNC
#ENDIF

// ===========================================================================================================
//      Display Routines
// ===========================================================================================================

///    This displays the full phone screen view of an invitation (after pressing accept on an invite)
PROC Display_Boss_Agency_Invitation_Screen()
	
	IF g_Cellphone.PhoneDS != PDS_COMPLEXAPP
		g_Cellphone.PhoneDS = PDS_COMPLEXAPP
		PRINTLN("...GB MP [BossAgency][App]: Display_Boss_Agency_Invitation_Screen() g_Cellphone.PhoneDS = PDS_COMPLEXAPP")
	ENDIF

	// Delete all data from the JobList Screen
    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieIndex, "SET_DATA_SLOT_EMPTY", (TO_FLOAT(BOSS_AGENCY_INVITE_VIEW_STATE)))
	
	PRINTLN("...GB MP [BossAgency][App]: Display_Boss_Agency_Invitation_Screen() for invite: ", m_selectedBossAgencyInvite)

	BOOL bUsePlayerHeadshot = FALSE
	STRING invitorTXD
	PEDHEADSHOT_ID invitorHeadshot
	
	INT iInviteSlot = GB_GET_GANG_INVITE_INDEX(m_selectedBossAgencyInvite, GANG_INVITE_ORG)
	
	playerForInvite = g_sBossAgencyApp.bossAgencyInvites[iInviteSlot].playerInvitor
	PLAYER_INDEX bossID = g_sBossAgencyApp.bossAgencyInvites[iInviteSlot].playerInvitor

    BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieIndex, "SET_DATA_SLOT")
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(BOSS_AGENCY_INVITE_VIEW_STATE)
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		
		// Add the player name first
		IF IS_NET_PLAYER_OK(bossID, FALSE)
			TEXT_LABEL_63 tlPlayerName = Get_Name_String_For_Boss_Agency_App(bossID)
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
				SET_COLOUR_OF_NEXT_TEXT_COMPONENT(GB_GET_PLAYER_GANG_HUD_COLOUR(bossID))
				ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(tlPlayerName)
			END_TEXT_COMMAND_SCALEFORM_STRING()
			bUsePlayerHeadshot = TRUE
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")
		ENDIF
		
		IF g_sBossAgencyApp.bossAgencyInvites[iInviteSlot].eInviteType = BOSS_APP_INVITE_TYPE_JOIN_ORG
			
			IF g_sMPTunables.bexec_vip2_disable_invite_info 
			
				// Grab a clean list of goons for this Gang. (Note we only support 3 goons at the moment. Need new text labels if this increases)
				
				PLAYER_INDEX goons[MAX_GOONS_FOR_INVITE_SCREEN]
				BOOL bOffice = DOES_PLAYER_OWN_OFFICE(bossID)

				INT i
				INT iCount
				
				REPEAT MAX_GOONS_FOR_INVITE_SCREEN i
					IF iCount < MAX_GOONS_FOR_INVITE_SCREEN
						IF GlobalplayerBD_FM_3[NATIVE_TO_INT(bossID)].sMagnateGangBossData.GangMembers[i] != INVALID_PLAYER_INDEX()
							IF IS_NET_PLAYER_OK(GlobalplayerBD_FM_3[NATIVE_TO_INT(bossID)].sMagnateGangBossData.GangMembers[i], FALSE)	
								goons[iCount] = GlobalplayerBD_FM_3[NATIVE_TO_INT(bossID)].sMagnateGangBossData.GangMembers[i]
								iCount++
							ENDIF
						ENDIF
					ENDIF
				ENDREPEAT
				
				
				// Main body text
				IF iCount = 0
					
					IF bOffice
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BA_APP_BODYCTA")				// Wants you to become a Goon. 
					ELSE
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BA_APP_BODY_TA")				// Wants you to become a Goon. 
					ENDIF
					
				ELSE			
					TEXT_LABEL_15 tlBodyText = "BA_APP_BODY_T4"
					
					IF bOffice
						tlBodyText = "BA_APP_BODYCT4"
					ENDIF
					
					TEXT_LABEL_63 tlPlayerName
					IF iCount = 2
						
						IF bOffice
							tlBodyText = "BA_APP_BODYCT5"
						ELSE
							tlBodyText = "BA_APP_BODY_T5"
						ENDIF
						
					ELIF iCount > 2
						
						IF bOffice
							tlBodyText = "BA_APP_BODYCT6"
						ELSE
							tlBodyText = "BA_APP_BODY_T6"
						ENDIF
						
					ENDIF
					
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING(tlBodyText)
						REPEAT iCount i
							tlPlayerName = Get_Name_String_For_Boss_Agency_App(goons[i])
							ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(tlPlayerName)
						ENDREPEAT
					END_TEXT_COMMAND_SCALEFORM_STRING()
				ENDIF
				
			ELSE
			
				TEXT_LABEL_15 tlBodyText
				
				IF g_sBossAgencyApp.bossInviteInfo[iInviteSlot].bYachtOwned
					IF g_sBossAgencyApp.bossInviteInfo[iInviteSlot].bOfficeOwned
						tlBodyText = "BA_APP_BODY_T9"
					ELSE
						tlBodyText = "BA_APP_BODY_T8"
					ENDIF
				ELSE
					IF g_sBossAgencyApp.bossInviteInfo[iInviteSlot].bOfficeOwned
						tlBodyText = "BA_APP_BODY_T7"
					ELSE
						tlBodyText = "BA_APP_BODY_T10"
					ENDIF
				ENDIF
				
				BEGIN_TEXT_COMMAND_SCALEFORM_STRING(tlBodyText)
					ADD_TEXT_COMPONENT_INTEGER(g_sBossAgencyApp.bossInviteInfo[iInviteSlot].iWageBonus)
					ADD_TEXT_COMPONENT_INTEGER(GB_GET_NUM_GOONS_IN_PLAYER_GANG(bossID))
					ADD_TEXT_COMPONENT_INTEGER(g_sBossAgencyApp.bossInviteInfo[iInviteSlot].iWarehouses)
					ADD_TEXT_COMPONENT_INTEGER(g_sBossAgencyApp.bossInviteInfo[iInviteSlot].iVehicles)
				END_TEXT_COMMAND_SCALEFORM_STRING()
				
				CPRINTLN(DEBUG_AMBIENT, "[Script_PIM] BOSS_APP_INVITE_TYPE_JOIN_ORG Invite Data. iWageBonus = ", g_sBossAgencyApp.bossInviteInfo[iInviteSlot].iWageBonus)
				CPRINTLN(DEBUG_AMBIENT, "[Script_PIM] BOSS_APP_INVITE_TYPE_JOIN_ORG Invite Data. bYachtOwned = ", g_sBossAgencyApp.bossInviteInfo[iInviteSlot].bYachtOwned)
				CPRINTLN(DEBUG_AMBIENT, "[Script_PIM] BOSS_APP_INVITE_TYPE_JOIN_ORG Invite Data. bOfficeOwned = ", g_sBossAgencyApp.bossInviteInfo[iInviteSlot].bOfficeOwned)
				CPRINTLN(DEBUG_AMBIENT, "[Script_PIM] BOSS_APP_INVITE_TYPE_JOIN_ORG Invite Data. iVehicles = ", g_sBossAgencyApp.bossInviteInfo[iInviteSlot].iVehicles)
				CPRINTLN(DEBUG_AMBIENT, "[Script_PIM] BOSS_APP_INVITE_TYPE_JOIN_ORG Invite Data. iWarehouses = ", g_sBossAgencyApp.bossInviteInfo[iInviteSlot].iWarehouses)

			ENDIF
			
		ELIF g_sBossAgencyApp.bossAgencyInvites[iInviteSlot].eInviteType = BOSS_APP_INVITE_TYPE_BOSS_V_BOSS
			
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("BA_APP_BODY_DM2")
				SET_COLOUR_OF_NEXT_TEXT_COMPONENT(GB_GET_PLAYER_GANG_HUD_COLOUR(g_sBossAgencyApp.bossAgencyInvites[iInviteSlot].playerInvitor))
				ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(g_sBossAgencyApp.bossAgencyInvites[iInviteSlot].playerInvitor)) // The Boss of ~a~~s~ has challenged you to a Boss Vs. Boss Deathmatch.
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ENDIF
		
		// Contact Picture
        IF (bUsePlayerHeadshot)
            invitorHeadshot = Get_HeadshotID_For_Player(g_sBossAgencyApp.bossAgencyInvites[iInviteSlot].playerInvitor)
            IF (invitorHeadshot = NULL)
            	bUsePlayerHeadshot = FALSE
            ELSE
            	invitorTXD = GET_PEDHEADSHOT_TXD_STRING(invitorHeadshot)
           	ENDIF            
        ENDIF
        
        // Display the player headshot if available, or the default headshot if not
        IF (bUsePlayerHeadshot)
            // ...display the headshot
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING(invitorTXD)
            PRINTLN("...GB MP [BossAgency][App]: Display_Boss_Agency_Invitation_Screen() - Passing player headshot TXD: ", invitorTXD)
        ELSE
            // ...use the default headshot
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(m_defaultContactPicture)
        ENDIF
		
	END_SCALEFORM_MOVIE_METHOD()
	
	 // Display the screen
    // NOTE: Must be after the slots have been filled
    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", (TO_FLOAT(BOSS_AGENCY_INVITE_VIEW_STATE)))

    // Display the Header - the Mission Type
    Display_MP_App_Header(m_invitationText)
	
	// For an Invitation, display 'Accept Job' and 'Back' buttons
    // GB 23/6/13: Also display 'delete' button for Invites from other players
    // GB 10/7/13: Also display 'delete' button for Invites from NPCs
    Display_MP_App_Buttons(MP_APP_ICON_YES, m_jobListIconTextAcceptJob, MP_APP_ICON_DELETE, m_jobListIconTextCancelJob, MP_APP_ICON_BACK, m_jobListIconTextBack, m_sAppData)
    
    // Now on the 'Confirm Invitation' screen
    m_currentStage = BOSS_APP_CONFIRM_INVITE	
ENDPROC

/// PURPOSE:
///    Displays the full screen view with the name of the Organization the Goon is already a part of.
PROC Display_Boss_Agency_Goon_Screen()

		// Delete all data from the JobList Screen
    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieIndex, "SET_DATA_SLOT_EMPTY", (TO_FLOAT(BOSS_AGENCY_INVITE_VIEW_STATE)))
	
	PRINTLN("...GB MP [BossAgency][App]: Display_Boss_Agency_Goon_Screen()")

    BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieIndex, "SET_DATA_SLOT")
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(BOSS_AGENCY_INVITE_VIEW_STATE)
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		
		// Add the player name first
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			SET_COLOUR_OF_NEXT_TEXT_COMPONENT(GB_GET_PLAYER_GANG_HUD_COLOUR(PLAYER_ID()))
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(GB_GET_ORGANIZATION_NAME_AS_A_STRING())
		END_TEXT_COMMAND_SCALEFORM_STRING()			

		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BA_APP_NONEG")				// You are already a member of this Organization.
		
        // ...use the default headshot
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("CELL_AGBOSSPIC")
		
	END_SCALEFORM_MOVIE_METHOD()
	
	 // Display the screen
    // NOTE: Must be after the slots have been filled
    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", (TO_FLOAT(BOSS_AGENCY_INVITE_VIEW_STATE)))

    // Display the Header - the Mission Type
    Display_MP_App_Header("CELL_BOSSAGE")
	
	// For an Invitation, display 'Accept Job' and 'Back' buttons
    // GB 23/6/13: Also display 'delete' button for Invites from other players
    // GB 10/7/13: Also display 'delete' button for Invites from NPCs
    Display_MP_App_Buttons(MP_APP_ICON_BLANK, "", MP_APP_ICON_BLANK, "", MP_APP_ICON_BACK, m_jobListIconTextBack, m_sAppData)
    
    // Now on the 'Confirm Invitation' screen
    m_currentStage = BOSS_APP_GOON
ENDPROC


// -----------------------------------------------------------------------------------------------------------
//      Boss Agency screen
// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Fill out the phone screen with a single 'No Boss Invites' entry.
PROC Fill_Boss_Agency_App_No_Invites()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieIndex, "SET_DATA_SLOT")
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(BOSS_AGENCY_VIEW_STATE)
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
    
        // GB 28/1/15: Extract the 'no jobs available to join' display into it's own fucntion so it can be coloured the same as the heists
        #IF IS_DEBUG_BUILD
            PRINTLN("...GB MP [BossAgency][App]: Fill_Boss_Agency_App_No_Invites - No Invites [View State: ", BOSS_AGENCY_VIEW_STATE, "] [Slot 0]")
        #ENDIF
        
		IF GB_IS_PLAYER_BOSS_OF_A_GANG(PLAYER_ID())
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BA_APP_NONEB")		// No Invites
		ELSE
        	SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BA_APP_NONE")     	// No Boss Invites
		ENDIF
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")     			// Available to Join

        // No Blip, Not Heist, Is Multiplayer?
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(-1)
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(HUD_COLOUR_WHITE))
    
        BOOL isMultiplayer = NETWORK_IS_GAME_IN_PROGRESS()
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(isMultiplayer)
    END_SCALEFORM_MOVIE_METHOD()
ENDPROC


// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Displays an invite entry on the first screen of the App. Player can scroll through and select the invite
FUNC BOOL Populate_Boss_Agency_Standard_Invite(INT iInviteSlot, INT iCellphoneSlot)
	
	// Check the player is valid before we proceed
	IF g_sBossAgencyApp.bossAgencyInvites[iInviteSlot].playerInvitor = INVALID_PLAYER_INDEX()
	OR NOT IS_NET_PLAYER_OK(g_sBossAgencyApp.bossAgencyInvites[iInviteSlot].playerInvitor, FALSE)
		
		PRINTLN("...GB MP [BossAgency][App]: Populate_Boss_Agency_Standard_Invite() - Player is invalid. Do not add to app. Slot [", iInviteSlot, "]")
		RETURN FALSE
	ENDIF
	
	// Populate the scaleform
	BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieIndex, "SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(BOSS_AGENCY_VIEW_STATE)			// View state
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iCellphoneSlot)						// Slot index (0, 1, 2, etc)
		
		// Top line
		TEXT_LABEL_63 tlPlayerName = Get_Name_String_For_Boss_Agency_App(g_sBossAgencyApp.bossAgencyInvites[iInviteSlot].playerInvitor)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(tlPlayerName)		// Display the player name at the top
		BOOL bOffice = DOES_PLAYER_OWN_OFFICE(g_sBossAgencyApp.bossAgencyInvites[iInviteSlot].playerInvitor)

		// Second line
		INT iTotalGoons = GB_GET_NUM_GOONS_IN_PLAYER_GANG(g_sBossAgencyApp.bossAgencyInvites[iInviteSlot].playerInvitor)
		IF iTotalGoons = 0
			IF bOffice
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BA_APP_BODYCT")				// Wants you to become a goon.
			ELSE
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("BA_APP_BODY_T")				// Wants you to become a goon.
			ENDIF
		ELSE
			IF iTotalGoons = 1

				IF bOffice
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING("BA_APP_BODYCT3")					// Wants you to become a goon. # other in this gang.
				ELSE
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING("BA_APP_BODY_T3")					// Wants you to become a goon. # other in this gang.
				ENDIF
			
				ADD_TEXT_COMPONENT_INTEGER(iTotalGoons)
				END_TEXT_COMMAND_SCALEFORM_STRING()
			ELSE
				IF bOffice
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING("BA_APP_BODYCT2")					// Wants you to become a goon. # others in this gang.
				ELSE
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING("BA_APP_BODY_T2")					// Wants you to become a goon. # others in this gang.
				ENDIF
				
				ADD_TEXT_COMPONENT_INTEGER(iTotalGoons)
				END_TEXT_COMMAND_SCALEFORM_STRING()
			ENDIF
		ENDIF
				
		// Blip sprite (-1 if not needed)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(-1)
		
		HUD_COLOURS hcBossColour = HUD_COLOUR_WHITE
		INT gangId = GET_GANG_ID_FOR_PLAYER(g_sBossAgencyApp.bossAgencyInvites[iInviteSlot].playerInvitor)
		IF gangId != -1
			hcBossColour = GET_HUD_COLOUR_FOR_GANG_ID(gangId)
		ENDIF
		
		#IF IS_DEBUG_BUILD
		// Override out colour if we have used the widget
		IF m_debugColour != -1
			hcBossColour = DEBUG_GET_OVERRIDDEN_HUD_COLOUR()
		ENDIF
		#ENDIF
		
		PRINTLN("...GB MP [BossAgency][App]: Populate_Boss_Agency_Standard_Invite() - hcBossColour: ", ENUM_TO_INT(hcBossColour))

		// Colour of invite box
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(hcBossColour))
				
		// TRUE as this is multiplayer
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
		
	END_SCALEFORM_MOVIE_METHOD()
		
	PRINTLN("...GB MP [BossAgency][App]: Populate_Boss_Agency_Standard_Invite() - Set up invite for slot [", iInviteSlot, "]. Invitor: ", GET_PLAYER_NAME(g_sBossAgencyApp.bossAgencyInvites[iInviteSlot].playerInvitor), " with ", iTotalGoons, " goons.")
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Displays a Boss V Boss Deathmatch style invite
FUNC BOOL Populate_Boss_Agency_BossVBoss_Invite(INT iInviteSlot, INT iCellphoneSlot)
	
		// Check the player is valid before we proceed
	IF g_sBossAgencyApp.bossAgencyInvites[iInviteSlot].playerInvitor = INVALID_PLAYER_INDEX()
	OR NOT IS_NET_PLAYER_OK(g_sBossAgencyApp.bossAgencyInvites[iInviteSlot].playerInvitor, FALSE)
		
		PRINTLN("...GB MP [BossAgency][App]: Populate_Boss_Agency_BossVBoss_Invite() - Player is invalid. Do not add to app. Slot [", iInviteSlot, "]")
		RETURN FALSE
	ENDIF
	
	// Populate the scaleform
	BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieIndex, "SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(BOSS_AGENCY_VIEW_STATE)			// View state
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iCellphoneSlot)						// Slot index (0, 1, 2, etc)
		
		// Top line
		TEXT_LABEL_63 tlPlayerName = Get_Name_String_For_Boss_Agency_App(g_sBossAgencyApp.bossAgencyInvites[iInviteSlot].playerInvitor)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(tlPlayerName)		// Display the player name at the top
		
		// Second line
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("BA_APP_BODY_DM1")			// Has challenged you to a Boss Vs. Boss Deathmatch.		
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		// Blip sprite (-1 if not needed)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(-1)
		
		HUD_COLOURS hcBossColour = HUD_COLOUR_WHITE
		INT gangId = GET_GANG_ID_FOR_PLAYER(g_sBossAgencyApp.bossAgencyInvites[iInviteSlot].playerInvitor)
		IF gangId != -1
			hcBossColour = GET_HUD_COLOUR_FOR_GANG_ID(gangId)
		ENDIF
		PRINTLN("...GB MP [BossAgency][App]: Populate_Boss_Agency_BossVBoss_Invite() - hcBossColour: ", ENUM_TO_INT(hcBossColour))

		// Colour of invite box
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(hcBossColour))
		
		// TRUE as this is multiplayer
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
		
	END_SCALEFORM_MOVIE_METHOD()
		
	PRINTLN("...GB MP [BossAgency][App]: Populate_Boss_Agency_BossVBoss_Invite() - Set up invite for slot [", iInviteSlot, "]. Invitor: ", GET_PLAYER_NAME(g_sBossAgencyApp.bossAgencyInvites[iInviteSlot].playerInvitor))
	
	RETURN TRUE

ENDFUNC

// -----------------------------------------------------------------------------------------------------------

PROC Fill_Boss_Agency_Invites()

	// Delete all data from the JobList View before filling it with new data
    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieIndex, "SET_DATA_SLOT_EMPTY", (TO_FLOAT(BOSS_AGENCY_VIEW_STATE)))

	PRINTLN("...GB MP [BossAgency][App]: Fill_Boss_Agency_Invites() - clear boss agency list [view state:", BOSS_AGENCY_VIEW_STATE, "]")
	PRINTLN("...GB MP [BossAgency][App]: Fill_Boss_Agency_Invites() - Populate with ", GB_GET_TOTAL_INVITES(), " invites")
	
	INT iLoop
	INT iInviteCount
	REPEAT GB_GET_TOTAL_INVITES() iLoop
		
		// Populate our invites here
		IF g_sBossAgencyApp.bossAgencyInvites[iLoop].eInviteState = BOSS_APP_INVITE_STATE_AVAILABLE
		AND g_sBossAgencyApp.bossAgencyInvites[iLoop].eGangInviteType = GANG_INVITE_ORG
			
			SWITCH g_sBossAgencyApp.bossAgencyInvites[iLoop].eInviteType
			
				CASE BOSS_APP_INVITE_TYPE_JOIN_ORG
					IF Populate_Boss_Agency_Standard_Invite(iLoop, iInviteCount)
						iInviteCount++
					ENDIF
				BREAK
				
				CASE BOSS_APP_INVITE_TYPE_BOSS_V_BOSS
					IF Populate_Boss_Agency_BossVBoss_Invite(iLoop, iInviteCount)
						iInviteCount++
					ENDIF
				BREAK	

			ENDSWITCH
		ENDIF
	ENDREPEAT
	
	// If we have displayed invites, exit
	IF (iInviteCount > 0)
		EXIT
	ENDIF
	
	// No invites were drawn, show NO invites
	Fill_Boss_Agency_App_No_Invites()
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Display the JobList screen
//
// INPUT PARAMS:            paramDoUpdate       [DEFAULT = TRUE] TRUE means update the data, FALSE means only display the already generated data
//                          paramKeepHighlight  [DEFAULT = TRUE] TRUE means keep the highlighter position if the data hasn't changed, FALSE means always reset it to first position
//                          paramJustLaunched   [DEFAULT = FALSE] TRUE if the Joblist app has just launched, FALSE if not (allows checking for an immediate display of a single job)
PROC Display_Boss_Agency_Screen(BOOL paramKeepHighlighter = TRUE) //, BOOL paramJustLaunched = FALSE)
    
	// This is a local cache of the player we have received invite from
	playerForInvite = INVALID_PLAYER_INDEX()
	
    // The JobList page is the Apps first screen, so the phone should be in RunningApp mode so that it knows to return to the HomeScreen on 'BACK'.
    IF NOT (g_Cellphone.PhoneDS = PDS_RUNNINGAPP)
        g_Cellphone.PhoneDS = PDS_RUNNINGAPP
        
        #IF IS_DEBUG_BUILD
            // ...some additional debugging help - a unique identifier for each time g_Cellphone.phoneDS gets updated
            cdPrintnl()
            cdPrintstring("STATE ASSIGNMENT 201. appMPJobListNEW::Display_Boss_Agency_Screen() assigns PDS_RUNNINGAPP")
            cdPrintnl()
        #ENDIF
    ENDIF
    
    // Setup the highlighter initial position
    INT highlighterPosition = 0
    IF (paramKeepHighlighter)
        highlighterPosition = m_selectedBossAgencyInvite
    ENDIF
        
    // Ensure the highlighter position is safe
    IF (highlighterPosition >= g_numMPJobListEntries)
        highlighterPosition = 0
    ENDIF
    
	// If we have quicklaunched the app, show the invitation and reset our global flag
	IF g_ShouldForceSelectionOfLatestAppItem
		PRINTLN("...GB MP [BossAgency][App] Display_Boss_Agency_Screen() but we have quicklaunched. Display the invitation instead")
		
		Display_Boss_Agency_Invitation_Screen()
		g_ShouldForceSelectionOfLatestAppItem = FALSE
		EXIT
	ENDIF
	
	INT iTotalValidInvites = GB_GET_TOTAL_INVITES()
	iTotalValidInvites -= GB_GET_TOTAL_BIKER_INVITES()
	
	// If we are a goon, push us to a blank page to say you are already a member
	IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), FALSE)
	AND NOT GB_IS_LOCAL_PLAYER_MEMBER_OF_A_BIKER_GANG()
	AND iTotalValidInvites <= 0
		Display_Boss_Agency_Goon_Screen()
		PRINTLN("...GB MP [BossAgency][App] Display_Boss_Agency_Screen() part of a gang, display the gang screen")
		EXIT
	ENDIF
		  
    // Store the details from the JobList array in the JobList Screen slots
	Fill_Boss_Agency_Invites()
    
    // Display the screen
    // NOTE: Must be after the slots have been filled
    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", (TO_FLOAT(BOSS_AGENCY_VIEW_STATE)), (TO_FLOAT(highlighterPosition)))
    
    #IF IS_DEBUG_BUILD
        NET_PRINT("...GB MP [BossAgency][App]: Display_Boss_Agency_Screen() - displaying boss agency invites [view state: ") NET_PRINT_INT(BOSS_AGENCY_VIEW_STATE) NET_PRINT("]") NET_NL()
    #ENDIF
    
    // Display the Header - Boss Agency
	Display_MP_App_Header("CELL_BOSSAGE")	
	
    // Display the Buttons?
    BOOL displayAllButtons = FALSE
    IF (iTotalValidInvites > 0)
        // ...yes, there are valid entries
        displayAllButtons = TRUE
    ENDIF
        
    IF (displayAllButtons)
        // Display 'Yes' and 'Back' buttons
        // GB 5/9/13: Also 'delete'
        Display_MP_App_Buttons(MP_APP_ICON_YES, m_jobListIconTextOptions, MP_APP_ICON_DELETE, m_jobListIconTextCancelJob, MP_APP_ICON_BACK, m_jobListIconTextBack, m_sAppData)
    ELSE
        // Only display 'Back' button
        Display_MP_App_Buttons(MP_APP_ICON_BLANK, "", MP_APP_ICON_BLANK, "", MP_APP_ICON_BACK, m_jobListIconTextBack, m_sAppData)
    ENDIF
    
    // Now on the Standard Invites screen
    m_currentStage = BOSS_APP_INVITES
ENDPROC

// ===========================================================================================================
//      App Stages - Processing
// ===========================================================================================================

// PURPOSE: Performs navigation checks while the Boss Agency screen is on display.
// NOTES:   This is the main screen shown when the app first triggers from teh cellphone home screen and is classed as the RUNNINGAPP screen,
//              so don't check for BACK button here, it is checked in the main loop and kills the app.
PROC Process_Stage_Boss_Agency_Invites()
    
    IF (GB_GET_TOTAL_INVITES() > 1)
        Check_For_MP_App_Navigation_Inputs(m_sAppData)
    ENDIF
    
    IF (Did_Player_Press_MP_App_Positive_Response_Button(m_sAppData))
    
        // Player pressed 'SELECT', so get the array position of the currently highlighted entry
        BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieIndex, "GET_CURRENT_SELECTION")
        
        // GB 19/6/12 - the return value is now delayed, so I'm going to use this quick method to wait for the return value - but it may cause subtle timing issues and need changed.
        //              If I do have to change it then it's going to be a bit of a pain-in-the-arse re-structuring. 
        //              I'll have two options:
        //                  1) Each frame, check for the return value being ready from the main WHILE loop, then act on it - I'll probably need lots of additional consistency checks
        //                  2) If the highlighter is moved by the player, immediately grab the new return value and store that to be used when necessary
        SCALEFORM_RETURN_INDEX thisReturnIndex = END_SCALEFORM_MOVIE_METHOD_RETURN_VALUE()
        
        WHILE NOT (IS_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_READY(thisReturnIndex))
            WAIT(0)
            
            NET_PRINT("...GB MP [BossAgency][App]: Process_Stage_Boss_Agency_Invites() - waiting for the cursor position to be returned by scaleform") NET_NL()
        ENDWHILE
        
        m_selectedBossAgencyInvite = GET_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_INT(thisReturnIndex)
        
        #IF IS_DEBUG_BUILD
            NET_PRINT("...GB MP [BossAgency][App]: Process_Stage_Boss_Agency_Invites() - scaleform returned this boss agency invite value: ") NET_PRINT_INT(m_selectedBossAgencyInvite) NET_NL()
        #ENDIF
		
		INT iInviteSlot =  GB_GET_GANG_INVITE_INDEX(m_selectedBossAgencyInvite, GANG_INVITE_ORG)
        // Move to the next screen
        SWITCH (g_sBossAgencyApp.bossAgencyInvites[iInviteSlot].eInviteState)
            CASE BOSS_APP_INVITE_STATE_EMPTY
                #IF IS_DEBUG_BUILD
                    // Our invite in the app has become empty (something has refreshed our data).
                    GB_Debug_Display_Boss_Agency_App_Details()
                    NET_PRINT("...m_selectedBossAgencyInvite returned from scaleform: ") NET_PRINT_INT(iInviteSlot) NET_PRINT(" - This was now on an EMPTY Boss Agency Invite") NET_NL()
                #ENDIF
                EXIT
                
            // Valid Entries
            CASE BOSS_APP_INVITE_STATE_AVAILABLE
				Display_Boss_Agency_Invitation_Screen()
                BREAK
                
            // Unknown Entry
            DEFAULT
                #IF IS_DEBUG_BUILD
                    GB_Debug_Display_Boss_Agency_App_Details()
                    NET_PRINT("...m_selectedBossAgencyInvite returned from scaleform: ") NET_PRINT_INT(iInviteSlot) NET_NL()
                    SCRIPT_ASSERT("Process_Stage_Boss_Agency_Invites(): SELECT was chosen on an unknown type of Boss Agency Invite array entry. See Console log. Tell James.")
                #ENDIF
                EXIT
        ENDSWITCH
       
        
        // The app is now displaying a deeper screen than the main screen, so the phone needs to know it is in ComplexApp state so it doesn't return to Home Screen on BACK
        g_Cellphone.PhoneDS = PDS_COMPLEXAPP
        #IF IS_DEBUG_BUILD
            // ...some additional debugging help - a unique identifier for each time g_Cellphone.phoneDS gets updated
            cdPrintnl()
            cdPrintstring("STATE ASSIGNMENT 202. appMPBossAgency::Process_Stage_Boss_Agency_Invites() assigns PDS_COMPLEXAPP")
            cdPrintnl()
        #ENDIF
        
        EXIT
    ENDIF
     
    IF (Did_Player_Press_MP_App_Special_Response_Button(m_sAppData))
           
        // Player pressed 'DELETE', so get the array position of the currently highlighted entry
        BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieIndex, "GET_CURRENT_SELECTION")
        
        // GB 19/6/12 - the return value is now delayed, so I'm going to use this quick method to wait for the return value - but it may cause subtle timing issues and need changed.
        //              If I do have to change it then it's going to be a bit of a pain-in-the-arse re-structuring. 
        //              I'll have two options:
        //                  1) Each frame, check for the return value being ready from the main WHILE loop, then act on it - I'll probably need lots of additional consistency checks
        //                  2) If the highlighter is moved by the player, immediately grab the new return value and store that to be used when necessary
        SCALEFORM_RETURN_INDEX thisReturnIndex = END_SCALEFORM_MOVIE_METHOD_RETURN_VALUE()
        
        WHILE NOT (IS_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_READY(thisReturnIndex))
            WAIT(0)
            
            NET_PRINT("...GB MP [BossAgency][App]: Process_Stage_Boss_Agency_Invites() - waiting for the cursor position to be returned by scaleform for 'delete'") NET_NL()
        ENDWHILE
        
        m_selectedBossAgencyInvite = GET_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_INT(thisReturnIndex)
        
        #IF IS_DEBUG_BUILD
            NET_PRINT("...GB MP [BossAgency][App]: Process_Stage_Boss_Agency_Invites() - scaleform returned this Boss Agency invite value for 'delete': ") NET_PRINT_INT(m_selectedBossAgencyInvite) NET_NL()
        #ENDIF
        
		INT iInviteSlot = GB_GET_GANG_INVITE_INDEX(m_selectedBossAgencyInvite, GANG_INVITE_ORG)
        // Handle deletion of selection
        SWITCH (g_sBossAgencyApp.bossAgencyInvites[iInviteSlot].eInviteState)
            CASE BOSS_APP_INVITE_STATE_EMPTY
				#IF IS_DEBUG_BUILD
               	GB_Debug_Display_Boss_Agency_App_Details()
				NET_PRINT("...m_selectedBossAgencyInvite returned from scaleform: ") NET_PRINT_INT(iInviteSlot) NET_PRINT(" - This was now on an EMPTY Boss Agency Invite can not DELETE") NET_NL()
				#ENDIF
                EXIT
            
            // Deletable Entries
             CASE BOSS_APP_INVITE_STATE_AVAILABLE
                GB_Delete_Invite_In_Slot(iInviteSlot)
				Display_Boss_Agency_Screen(FALSE)
                EXIT
                             
            // Unknown Entry
            DEFAULT
				#IF IS_DEBUG_BUILD
                    GB_Debug_Display_Boss_Agency_App_Details()
                    PRINTLN("...m_selectedBossAgencyInvite returned from scaleform for 'delete': ", iInviteSlot)
                    SCRIPT_ASSERT("Process_Stage_Boss_Agency_Invites(): SELECT was chosen on an unknown type of Boss Agency invite array entry. See Console log. Tell James.")
                #ENDIF
                EXIT
        ENDSWITCH
        
        EXIT
    ENDIF
   
    // While on this screen, allow live updating of the joblist
    IF (GB_Is_Boss_Agency_App_Bit_Set(GB_BOSS_AGENCY_BIT_INVITES_UPDATED))
        // ...the data has just been updated, so no need to update it again
        GB_Clear_Boss_Agency_App_Bit(GB_BOSS_AGENCY_BIT_INVITES_UPDATED)
        Display_Boss_Agency_Screen(FALSE)
    ENDIF
    
    // BACK button checked in main loop and kills the app since this is the first screen of the app triggered from the cellphone home page
ENDPROC

/// PURPOSE:
///    Confirms the invite and puts away the phone
PROC Confirm_Boss_Agency_App_Invite(INT iInviteSlot)
	PLAYER_INDEX invitor = g_sBossAgencyApp.bossAgencyInvites[iInviteSlot].playerInvitor

	PRINTLN("...GB MP [BossAgency][App]: Confirm_Boss_Agency_App_Invite() - for invite from: ", GET_PLAYER_NAME(invitor))

	IF g_sBossAgencyApp.bossAgencyInvites[iInviteSlot].eInviteState = BOSS_APP_INVITE_STATE_AVAILABLE
	AND playerForInvite = invitor

		// Accept the invite and attempt to join the gang
		GB_REQUEST_JOIN_GANG(invitor,g_sBossAgencyApp.bossAgencyInvites[iInviteSlot].inviteSubtype)
	
		// We delete this single invite. If accepted all invites will be deleted
		GB_Delete_Invite_In_Slot(iInviteSlot)

	#IF IS_DEBUG_BUILD
	ELSE
		IF playerForInvite != invitor
			PRINTLN("...GB MP [BossAgency][App]: Confirm_Boss_Agency_App_Invite() - Invitor does not match playerForInvite. [playerForInvite = ", NATIVE_TO_INT(playerForInvite), "], [Invitor = ", NATIVE_TO_INT(Invitor), "]")
		ENDIF
	#ENDIF

	ENDIF
	
	// Clean this global up
	g_sBossAgencyApp.bBossAgencyAppRunningWarningScreen = FALSE
	
	HANG_UP_AND_PUT_AWAY_PHONE()
ENDPROC

PROC Confirm_Boss_Vs_Boss_Deathmatch_Invite(INT iInviteSlot)

	PRINTLN("...GB MP [BossAgency][App]: Confirm_Boss_Vs_Boss_Deathmatch_Invite()")

	SET_BIT(g_sImpromptuVars.iBitSet, ciBOSSVBOSS_ACCEPT_BVB_INVITE_HELP)

	// We delete this single invite. If accepted all invites will be deleted
	GB_Delete_Invite_In_Slot(iInviteSlot)
	
	// Clean this global up
	g_sBossAgencyApp.bBossAgencyAppRunningWarningScreen = FALSE
	
	HANG_UP_AND_PUT_AWAY_PHONE()
ENDPROC

/// PURPOSE:
///    Handle the input for the Invitation screen
PROC Process_Stage_Boss_Agency_Invitation_Screen()

	INT iInviteSlot = GB_GET_GANG_INVITE_INDEX(m_selectedBossAgencyInvite, GANG_INVITE_ORG)

	// Exit if this current selection is invalid
	IF m_selectedBossAgencyInvite = BOSS_AGENCY_INVALID_SELECTION
		SCRIPT_ASSERT("...GB MP [BossAgency][App]: Process_Stage_Boss_Agency_Invitation_Screen() - m_selectedBossAgencyInvite = BOSS_AGENCY_INVALID_SELECTION")
		Display_Boss_Agency_Screen()
		EXIT
	ENDIF

	// Validate the invitor
	PLAYER_INDEX invitor = g_sBossAgencyApp.bossAgencyInvites[iInviteSlot].playerInvitor
	IF NOT IS_NET_PLAYER_OK(invitor, FALSE)
	OR playerForInvite != invitor
		
		// Invitor is no longer valid, revert back to the agency screen
		Display_Boss_Agency_Screen()		
		EXIT
	ENDIF
	
	// Check for scrolling (possible?)
    Check_For_MP_App_Navigation_Inputs(m_sAppData)
	
	// Check if the player wants to go BACK to Boss Agency Invite list
    IF (Did_Player_Press_MP_App_Negative_Response_Button(m_sAppData))
        Display_Boss_Agency_Screen()
        EXIT
    ENDIF
	
	// Check if the player wants to DELETE the current invite
    IF (Did_Player_Press_MP_App_Special_Response_Button(m_sAppData))
      	
		PRINTLN("...GB MP [BossAgency][App]: Player has chosen to DELETE boss agency invite: ", iInviteSlot)
		
		GB_Delete_Invite_In_Slot(iInviteSlot)
        Display_Boss_Agency_Screen()
        EXIT
    ENDIF
	
	// If the player accepts, request to join and delete this invite (if we are confirmed all invites will be deleted)
	IF (Did_Player_Press_MP_App_Positive_Response_Button(m_sAppData))
	
		IF IS_MP_PASSIVE_MODE_ENABLED()
			
			bAcceptPressed = TRUE
		
			PRINTLN("...GB MP [BossAgency][App]: Player has passive mode enabled. Warn them they will lose this.")
			m_currentStage = BOSS_APP_PASSIVE_MODE_WARNING			
		ELSE
			IF g_sBossAgencyApp.bossAgencyInvites[iInviteSlot].eInviteType = BOSS_APP_INVITE_TYPE_JOIN_ORG
		        Confirm_Boss_Agency_App_Invite(iInviteSlot)
			ELIF g_sBossAgencyApp.bossAgencyInvites[iInviteSlot].eInviteType = BOSS_APP_INVITE_TYPE_BOSS_V_BOSS
				Confirm_Boss_Vs_Boss_Deathmatch_Invite(iInviteSlot)
			ELSE
				SCRIPT_ASSERT("...GB MP [BossAgency][App]: Accepting an unknown Boss Agency Invite type, HANG UP PHONE")
				HANG_UP_AND_PUT_AWAY_PHONE()
			ENDIF
		ENDIF
    ENDIF
	
ENDPROC

PROC Process_Stage_Boss_Agency_Goon_Screen()
		
	// Anything to do?
	
ENDPROC

/// PURPOSE:
///    Display a warning message to the player asking them to confirm invite as a Goon to disable Passive mode
PROC Process_Stage_Boss_Agency_Passive_Warning()

	// Boss agency is running warning screen (external MAINTAIN_MP_COMMUNICATIONS)
	g_sBossAgencyApp.bBossAgencyAppRunningWarningScreen = TRUE

	// Cellphone check for JUST_PRESSED rather than released. Need to wait until this is no longer true and then we can listen for our accept
	IF bAcceptPressed
		IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_POSITIVE_INPUT))
			bAcceptPressed = FALSE
		ENDIF
		EXIT
	ENDIF

	// Listen for confirmation
	IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
		
		PRINTLN("...GB MP [BossAgency][App]: Process_Stage_Boss_Agency_Passive_Warning. Player selected Accept")
		
		// Update state and accept invite
		m_currentStage = BOSS_APP_CONFIRM_INVITE
		
		INT iInviteSlot = GB_GET_GANG_INVITE_INDEX(m_selectedBossAgencyInvite, GANG_INVITE_ORG)
		Confirm_Boss_Agency_App_Invite(iInviteSlot)
		
		EXIT
	ENDIF
	
	// Listen for player backing out
	IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)

		PRINTLN("...GB MP [BossAgency][App]: Process_Stage_Boss_Agency_Passive_Warning. Player selected Cancel")
		m_currentStage = BOSS_APP_CONFIRM_INVITE
	
		EXIT
	ENDIF
	
	// Display warning message every frame.
	SET_WARNING_MESSAGE_WITH_HEADER("HUD_INPUT23", "BA_APP_PASS_W", FE_WARNING_YESNO)
	
ENDPROC


/// PURPOSE:
///    Waits until a warning screen is active to clear our global
PROC Maintain_Boss_Agency_Warning_Screen()

	IF g_sBossAgencyApp.bBossAgencyAppRunningWarningScreen
		IF m_currentStage != BOSS_APP_PASSIVE_MODE_WARNING
			IF NOT IS_WARNING_MESSAGE_ACTIVE()
				PRINTLN("...GB MP [BossAgency][App]: Maintain_Boss_Agency_Warning_Screen - Warning screen no loner active. bBossAgencyAppRunningWarningScreen = FALSE")
				g_sBossAgencyApp.bBossAgencyAppRunningWarningScreen = FALSE
			ENDIF
		ENDIF
	ENDIF
ENDPROC

#IF IS_DEBUG_BUILD

// Simple widget to put a fake invite in the app for testing purposes only (used by UI quite often)
PROC Update_Widgets()
	IF bDummyInvite
	
		PRINTLN("...GB MP [BossAgency][App]: Update_Widgets - Dummy invite generated")
	
		GB_CLEAR_TOTAL_INVITES()
		g_sBossAgencyApp.bossAgencyInvites[0].eInviteType = BOSS_APP_INVITE_TYPE_JOIN_ORG
		g_sBossAgencyApp.bossAgencyInvites[0].eInviteState = BOSS_APP_INVITE_STATE_AVAILABLE
		g_sBossAgencyApp.bossAgencyInvites[0].playerInvitor = PLAYER_ID()
		g_sBossAgencyApp.bossAgencyInvites[0].timeReceived = GET_NETWORK_TIME()
		
		Populate_Boss_Agency_Standard_Invite(0, 0)
		LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", (TO_FLOAT(BOSS_AGENCY_VIEW_STATE)), (TO_FLOAT(0)))

		bDummyInvite = FALSE
	ENDIF
ENDPROC

// Any widgets required for the app.
PROC Setup_Widgets()

	START_WIDGET_GROUP("Boss Agency App")
		
		ADD_WIDGET_INT_SLIDER("Debug Invite Colour", m_debugColour, -1, 16, 1)
		ADD_WIDGET_BOOL("Dummy Invite", bDummyInvite)
		
	STOP_WIDGET_GROUP()
ENDPROC

#ENDIF

	// #IF FEATURE_GANG_BOSS
// ===========================================================================================================
//      Main Loop
// ===========================================================================================================

SCRIPT

    // Ensure script can run in network game
    NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()
    
	
	// DO THIS FIRST: Initialise the MP Cellphone App public header
    Initialise_MP_App_Variables(m_sAppData)
        
    // List navigation initial values
    m_selectedBossAgencyInvite = 0
	
	
	#IF IS_DEBUG_BUILD
	Setup_Widgets()
	#ENDIF
	
	// Reset back to 0 (accessed list so all 'read')
	GB_RESET_TOTAL_UNREAD_INVITES()
	
	// Set the top header to black
	LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "SET_THEME", TO_FLOAT (5))
	
    // Display the Boss Agency Screen when the app starts
    // NOTE: Display Boss Agency Screen should set the cellphone state as RunningApp
    Display_Boss_Agency_Screen(FALSE)

    // Main Loop - never terminates while the app can legitimately still run
    WHILE TRUE
        WAIT(0)
        
		#IF IS_DEBUG_BUILD
		Update_Widgets()
		#ENDIF
		
        // Because this is just setting a flag, we need to disable to controls here as well
        Ensure_Exclusive_Joblist_Controls()
		
		Maintain_Boss_Agency_Warning_Screen()
               
        // Need to handle interrupting the app if a cellphone call comes through
        // GB 7/5/12: Also need to ensure a request for AWAY or DISABLED prevents any processing within the App
        IF NOT (g_Cellphone.PhoneDS = PDS_ONGOING_CALL)
        AND (g_CellPhone.PhoneDS > PDS_AWAY)
        
            // Deal with the various stages of the app
            SWITCH (m_currentStage)
                // Boss Agency screen
                CASE BOSS_APP_INVITES
                    Process_Stage_Boss_Agency_Invites()
                    BREAK
                    
                // Boss Agency accept invite to a gang screen
                CASE BOSS_APP_CONFIRM_INVITE
					Process_Stage_Boss_Agency_Invitation_Screen()
                    BREAK
                  
				// Boss Agency warn players will lose passive mode screen
				CASE BOSS_APP_PASSIVE_MODE_WARNING
					Process_Stage_Boss_Agency_Passive_Warning()
					BREAK
					
				// Tells the goon they are already in an Organization
				CASE BOSS_APP_GOON
					Process_Stage_Boss_Agency_Goon_Screen()
					BREAK
				   
                DEFAULT
                    SCRIPT_ASSERT("appMpBossAgency: Unknown MP Boss Agency App Stage. Tell James.")
                    BREAK
            ENDSWITCH
            
            // This will automatically exit the cellphone if the back button is pressed.
            // If you move two screens deep into your app, then you must set your app state, g_Cellphone.PhoneDS to PDS_COMPLEXAPP so this doesn't return true.
            IF (g_Cellphone.PhoneDS <> PDS_COMPLEXAPP)
                IF (CHECK_FOR_APPLICATION_EXIT())
                    #IF IS_DEBUG_BUILD
                        NET_PRINT("...GB MP [BossAgency][App]: appMpBossAgency - CHECK_FOR_APPLICATION_EXIT() returned TRUE") NET_NL()
                    #ENDIF
    
                    Cleanup_and_Terminate()
                ENDIF
            ENDIF
        ENDIF
        
        //Critical! Do not remove. This must run every frame as it handles bad things such as the player dying when the phone is active.
        IF CHECK_FOR_ABNORMAL_EXIT()
            PRINTLN("...GB MP [BossAgency][App]: appMpBossAgency - CHECK_FOR_ABNORMAL_EXIT() returned TRUE")
            Cleanup_and_Terminate()
        ENDIF
    ENDWHILE
	

ENDSCRIPT


