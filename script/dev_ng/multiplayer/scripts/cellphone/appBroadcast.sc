USING "rage_builtins.sch"
USING "globals.sch"
USING "net_common_functions.sch"
USING "net_app_public.sch"

// An instance of the Mp App data struct (just include this - it allows some general functionality from net_app_public.sch to work - ie: screen navigation, etc
structMPAppData	m_sAppData

CONST_INT	CONFIRM_VIEW_STATE      13

CONST_INT	CONFIRM_SLOT_ICON		12


TEXT_LABEL_15	m_BCMPIconTextYes			= "CELL_212"
TEXT_LABEL_15	m_BCMPIconTextBack			= "CELL_213"
//TEXT_LABEL_15	m_BCMPIconTextNo			= "CELL_213"
TEXT_LABEL_15	m_BCMPTextFM				= "CELL_39"
TEXT_LABEL_15	m_Activate					= "CELL_MP_300"
TEXT_LABEL_15	m_Deactivate				= "CELL_MP_301"
TEXT_LABEL_15   m_Unavailable				= "CELL_MP_302"


PROC Display_First_Broadcast_Screen()

	// The JobList page is the Apps first screen, so the phone should be in RunningApp mode so that it knows to return to the HomeScreen on 'BACK'.
	IF NOT (g_Cellphone.PhoneDS = PDS_RUNNINGAPP)
		g_Cellphone.PhoneDS = PDS_RUNNINGAPP
		
		#IF IS_DEBUG_BUILD
			// ...some additional debugging help - a unique identifier for each time g_Cellphone.phoneDS gets updated
			cdPrintnl()
			cdPrintstring("[APP-BC] STATE ASSIGNMENT 201. appBCMP::Display_JobList_Screen() assigns PDS_RUNNINGAPP")
			cdPrintnl()
		#ENDIF
	ENDIF
	
	// Delete all data from the Confirm View before filling it with new data
    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieIndex, "SET_DATA_SLOT_EMPTY", (TO_FLOAT(CONFIRM_VIEW_STATE)))
	
	// Display 'Activate or deactivate?" - there is only one slot	
	IF IS_BROADCAST_APP_AVAILABLE()
		IF (g_bBroadcastAppOn)
			LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(CONFIRM_VIEW_STATE)),
				(TO_FLOAT(0)), (TO_FLOAT(CONFIRM_SLOT_ICON)), INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, m_Deactivate)	
		ELSE
			LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(CONFIRM_VIEW_STATE)),
				(TO_FLOAT(0)), (TO_FLOAT(CONFIRM_SLOT_ICON)), INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, m_Activate)	
		ENDIF
	ELSE
		LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(CONFIRM_VIEW_STATE)),
				(TO_FLOAT(0)), (TO_FLOAT(CONFIRM_SLOT_ICON)), INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, m_Unavailable)	
	ENDIF

	
	// Display the screen
	// NOTE: Must be after the slots have been filled
    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", (TO_FLOAT(CONFIRM_VIEW_STATE)))
	
	Display_MP_App_Header(m_BCMPTextFM)
	
	// For the confirmation screen, display 'Yes' and 'No' buttons
	IF IS_BROADCAST_APP_AVAILABLE()
		Display_MP_App_Buttons(MP_APP_ICON_YES, m_BCMPIconTextYes, MP_APP_ICON_BLANK, "", MP_APP_ICON_BACK, m_BCMPIconTextBack, m_sAppData)
	ELSE
		Display_MP_App_Buttons(MP_APP_ICON_BLANK, "", MP_APP_ICON_BLANK, "", MP_APP_ICON_BACK, m_BCMPIconTextBack, m_sAppData)
	ENDIF

ENDPROC

//PROC SET_CHAT_SEND_RESTRICTIONS(BOOL bSet)
//	INT i
//	REPEAT NUM_NETWORK_PLAYERS i
//		NETWORK_OVERRIDE_SEND_RESTRICTIONS(INT_TO_NATIVE(PLAYER_INDEX, i), bSet)	
//	ENDREPEAT
//ENDPROC

PROC Process_Broadcast_App_Input()

	IF (Did_Player_Press_MP_App_Positive_Response_Button(m_sAppData))
		IF IS_BROADCAST_APP_AVAILABLE()
			IF (g_bBroadcastAppOn)
				//CLIENT_SET_UP_PROXIMITY_CHAT(CHAT_PROXIMITY_FM)
				NETWORK_OVERRIDE_SEND_RESTRICTIONS_ALL(FALSE)
				//SET_CHAT_SEND_RESTRICTIONS(FALSE)
				BROADCAST_GENERAL_EVENT(GENERAL_EVENT_TYPE_DEACTIVATE_BROADCAST_APP, ALL_PLAYERS())				
				g_bBroadcastAppOn = FALSE								
			ELSE
				//CLIENT_SET_UP_EVERYONE_CHAT()
				NETWORK_OVERRIDE_SEND_RESTRICTIONS_ALL(TRUE)
				//SET_CHAT_SEND_RESTRICTIONS(TRUE)
				BROADCAST_GENERAL_EVENT(GENERAL_EVENT_TYPE_ACTIVATE_BROADCAST_APP, ALL_PLAYERS())
				g_bBroadcastAppOn = TRUE
			ENDIF
			//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bHasBroadcastAppOn = g_bBroadcastAppOn
			HANG_UP_AND_PUT_AWAY_PHONE()
			EXIT
		ENDIF
	ENDIF
	
//	IF (Did_Player_Press_MP_App_Negative_Response_Button(m_sAppData))
//		g_Cellphone.PhoneDS = PDS_RUNNINGAPP
//		EXIT
//	ENDIF
	

ENDPROC

// PURPOSE:	Cleanly terminates the script
PROC Cleanup_and_Terminate()
	NET_PRINT("[APP-BC] Cleanup_and_Terminate ") NET_NL() 
    TERMINATE_THIS_THREAD()
ENDPROC

SCRIPT

	NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()


	// DO THIS FIRST: Initialise the MP Cellphone App public header
	Initialise_MP_App_Variables(m_sAppData)

	Display_First_Broadcast_Screen()

	WHILE TRUE
	
		WAIT(0)
	
		// Make sure still in the network game
		IF NOT (NETWORK_IS_IN_SESSION())
			#IF IS_DEBUG_BUILD
				NET_PRINT("[APP-BC] NETWORK_IS_IN_SESSION() returned FALSE - Disabling cellphone and terminating App") NET_NL()
			#ENDIF
			
			// Disable the cellphone
			DISABLE_CELLPHONE(TRUE)
			Cleanup_and_Terminate()
		ENDIF	
		
		IF NOT (g_Cellphone.PhoneDS = PDS_ONGOING_CALL)
		AND (g_CellPhone.PhoneDS > PDS_AWAY)
		
			// process
			Process_Broadcast_App_Input()
			
			// This will automatically exit the cellphone if the back button is pressed.
			// If you move two screens deep into your app, then you must set your app state, g_Cellphone.PhoneDS to PDS_COMPLEXAPP so this doesn't return true.
            IF (g_Cellphone.PhoneDS <> PDS_COMPLEXAPP)
	            IF (CHECK_FOR_APPLICATION_EXIT())
					#IF IS_DEBUG_BUILD
						NET_PRINT("[APP-BC] CHECK_FOR_APPLICATION_EXIT() returned TRUE") NET_NL()
					#ENDIF
					
					Cleanup_and_Terminate()
				ENDIF
			ENDIF
			
		ENDIF
		
		//Critical! Do not remove. This must run every frame as it handles bad things such as the player dying when the phone is active.
		IF CHECK_FOR_ABNORMAL_EXIT()
			#IF IS_DEBUG_BUILD
				NET_PRINT("[APP-BC] CHECK_FOR_ABNORMAL_EXIT() returned TRUE") NET_NL()
			#ENDIF
					
			Cleanup_and_Terminate()
		ENDIF		
		
	
	ENDWHILE

ENDSCRIPT