

// PURPOSE: Usefull network bot methods

#IF IS_DEBUG_BUILD 

// Global Headers
USING "globals.sch"

// Game Headers
USING "commands_player.sch"
USING "commands_network.sch"

USING "net_include.sch"

// ---------------------------------------------------------
// ----------------------- Main Procs -----------------------
// ---------------------------------------------------------

/// PURPOSE:
///    Convert a Network Bot Participant ID to an INT.
/// PARAMS:
///    plr - the PLAYER_INDEX to be converted.
/// RETURNS:
///    The value of the Participant ID.
FUNC INT NETWORK_BOT_PARTICIPANT_ID_TO_INT(PLAYER_INDEX plr)
	RETURN NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(plr))
ENDFUNC

/// PURPOSE:
///    Get the PED_INDEX of a player.
/// PARAMS:
///    plr - index value.
/// RETURNS:
///    The PED_INDEX
FUNC PED_INDEX NETWORK_BOT_PLAYER_PED_ID(PLAYER_INDEX plr)
    RETURN GET_PLAYER_PED(plr)
ENDFUNC

//// PURPOSE:
 ///    Check to see if the script should be terminated
 /// PARAMS:
 ///    botIndex       - network bot index.
 ///    botPlayerIndex - network bot player index.
 /// RETURNS:
 ///    TRUE if the script should be terminated.
FUNC BOOL CHECK_FOR_TERMINATE(INT botIndex, PLAYER_INDEX botPlayerIndex)

	// If we have a match end event, bail.
	IF NOT NETWORK_IS_IN_SESSION()
		NET_PRINT_STRING_INT("MATCH ENDED - Terminate script followplayer for Network Bot - ", botIndex)
		RETURN TRUE
	ENDIF

	// If we have a match end event, bail.
	IF NOT NETWORK_BOT_IS_LOCAL(botIndex)
		NET_PRINT_STRING_INT("BOT IS NOT LOCAL - Terminate script followplayer for Network Bot - ", botIndex)
		RETURN TRUE
	ENDIF

	// If we have a match end event, bail.
	IF NOT NETWORK_BOT_EXISTS(botPlayerIndex) 
		NET_PRINT_STRING_INT("NETWORK BOT DOESNT EXIST - Terminate script followplayer for Network Bot - ", botIndex)
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

//// PURPOSE:
 ///    Creates all relationship groups used for the network bots.
 /// RETURNS:
 ///    TRUE if it managed to create the relationship groups.
FUNC BOOL CREATE_NETWORK_BOTS_RELATIONSHIP_GROUPS()

	NET_PRINT("Add Network bots relationship groups.")

	IF ADD_RELATIONSHIP_GROUP("netbot_group", netbotGroup)
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Do commom pre game start initializations plus wait in a loop
///    until the script can start processing.
/// PARAMS:
///    botIndex       - Returns value of the network bot assigned to the current script.
///    botPlayerIndex - Returns value of the network bot player index.
///    botPedIndex    - Returns value of the network bot ped index.
/// NOTES:
///    During the lifetime of the current script these return parameters should never change.
PROC WAIT_FOR_START(INT& botIndex, PLAYER_INDEX& botPlayerIndex, PED_INDEX& botPedIndex)

	botPlayerIndex = NULL
	botPedIndex    = NULL

	// This script will not be paused if another script calls PAUSE_GAME
	SET_THIS_SCRIPT_CAN_BE_PAUSED(TRUE)

	WHILE NOT NETWORK_IS_IN_SESSION()
		WAIT(0)
	ENDWHILE

	botIndex = -1
	WHILE botIndex = -1
		botIndex = NETWORK_BOT_GET_SCRIPT_BOT_INDEX()
		WAIT(0)
	ENDWHILE

	botPlayerIndex = NETWORK_BOT_GET_PLAYER_ID(botIndex)
	botPedIndex    = NETWORK_BOT_PLAYER_PED_ID(botPlayerIndex)

	//SET_PED_RELATIONSHIP_GROUP_HASH(botPedIndex, netbotGroup)

ENDPROC


//// PURPOSE:
 ///    Checks a vehicle for empty seats.
 /// PARAMS:
 ///    vehicle      - vehicle index to check.
 ///    seat         - Return value of the empty seat.
 ///    numFreeSeats - Return value of the number of free seats.
 ///    driverAlso   - If the value is TRUE also check the driver seat.
 /// RETURNS:
 ///    TRUE i fthe vehicle has empty seats, FALSE otherwise.
FUNC BOOL GET_VEHICLE_HAS_FREE_SEATS(VEHICLE_INDEX vehicle, VEHICLE_SEAT& seat, INT& numFreeSeats, BOOL driverAlso = TRUE)

	BOOL ret = FALSE

	numFreeSeats = GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(vehicle) - GET_VEHICLE_NUMBER_OF_PASSENGERS(vehicle)

	IF NOT IS_ENTITY_DEAD(vehicle)
		IF IS_VEHICLE_STOPPED(vehicle)
			IF driverAlso AND IS_VEHICLE_SEAT_FREE(vehicle, VS_DRIVER)
				seat = VS_DRIVER
				ret = TRUE
			ELSE
				INT i
				REPEAT GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(vehicle) i

					IF IS_VEHICLE_SEAT_FREE(vehicle, INT_TO_ENUM(VEHICLE_SEAT, i))
						seat = INT_TO_ENUM(VEHICLE_SEAT, i)
						RETURN TRUE
					ENDIF

				ENDREPEAT
			ENDIF
		ENDIF
	ENDIF

	RETURN ret

ENDFUNC


//// PURPOSE:
 ///    Creates a Network vehicle to be used by the local network bot.
 /// PARAMS:
 ///    botVehicle - the VEHICLE_INDEX of our network bot.
 ///    botPedIndex     - the PED_INDEX of our network bot.
 ///    vPos            - postion were to create the vehicle.
 ///    fHeading        - heading of the created vehicle.
FUNC BOOL CREATE_NETWORK_BOT_VEHICLE(VEHICLE_INDEX& botVehicle, NETWORK_INDEX& botVehicleNetId, VECTOR vPos, FLOAT fHeading = 0.0)

	IF CAN_REGISTER_MISSION_VEHICLES(1)
		RESERVE_NETWORK_MISSION_VEHICLES(1)
		//	MODEL_NAMES vehModel = BUFFALO
		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(botVehicleNetId)
			IF REQUEST_LOAD_MODEL(BUFFALO)
				IF CREATE_NET_VEHICLE(botVehicleNetId, BUFFALO, vPos, fHeading, TRUE)
					botVehicle = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(NETWORK_GET_ENTITY_FROM_NETWORK_ID(botVehicleNetId))
					RETURN TRUE
					// ---------------------------------------------------------
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

//// PURPOSE:
 ///    Creates a non networked vehicle to be used by the local network bot.
 /// PARAMS:
 ///    botVehicle - the VEHICLE_INDEX of our network bot.
 ///    botPedIndex     - the PED_INDEX of our network bot.
 ///    vPos            - postion were to create the vehicle.
 ///    fHeading        - heading of the created vehicle.
PROC CREATE_LOCAL_BOT_VEHICLE(VEHICLE_INDEX& botVehicle, PED_INDEX botPedIndex, INT botIndex, VECTOR vPos, FLOAT fHeading = 0.0)

	IF IS_ENTITY_DEAD(botVehicle)
		DELETE_VEHICLE(botVehicle)
	ENDIF

	IF DOES_ENTITY_EXIST(botVehicle) AND NOT IS_ENTITY_DEAD(botVehicle)
		IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(botVehicle), GET_ENTITY_COORDS(botPedIndex)) > 30.0
			DELETE_VEHICLE(botVehicle)
		ENDIF
	ENDIF

	IF NOT DOES_ENTITY_EXIST(botVehicle)
		IF NOT HAS_MODEL_LOADED(BUFFALO)
			REQUEST_MODEL(BUFFALO)
		ENDIF

		WHILE NOT HAS_MODEL_LOADED(BUFFALO)
			WAIT(0)
		ENDWHILE


		//IS_AREA_OCCUPIED(vPos, VECTOR VecMaxCoors, TRUE, TRUE, TRUE, TRUE, TRUE)

		botVehicle = CREATE_VEHICLE(BUFFALO, vPos, fHeading, FALSE)
		NET_DEBUG_STRING(NETWORK_BOT_GET_NAME(botIndex), "Create Vehicle")

	ENDIF

ENDPROC


/// PURPOSE:
///    Processes logic for following local host player on foot.
/// PARAMS:
///    botPedIndex - the PED_INDEX of our network bot.
PROC FOLLOW_PLAYER_ON_FOOT(PED_INDEX botPedIndex)

	IF 10.0 < VDIST(GET_ENTITY_COORDS(botPedIndex, FALSE), GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE))
		IF GET_SCRIPT_TASK_STATUS(botPedIndex, SCRIPT_TASK_GOTO_ENTITY_OFFSET) != PERFORMING_TASK
			TASK_GOTO_ENTITY_OFFSET(botPedIndex, PLAYER_PED_ID())
		ENDIF
	ENDIF

ENDPROC

/// PURPOSE:
///     Processes logic for following local host player using a vehicle.
/// PARAMS:
///    botPedIndex   - the PED_INDEX of our network bot.
///    botVehicle    - the VEHICLE_INDEX of our network bot.
///    playerVehicle - the VEHICLE_INDEX of the local host player.
PROC FOLLOW_PLAYER_ON_VEHICLE(PED_INDEX botPedIndex, VEHICLE_INDEX botVehicle, VEHICLE_INDEX playerVehicle)

	IF 10.0 < VDIST(GET_ENTITY_COORDS(botPedIndex, FALSE), GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE))
	AND GET_PED_IN_VEHICLE_SEAT(botVehicle, VS_DRIVER) = botPedIndex
	AND NOT IS_ENTITY_DEAD(botVehicle)
	AND GET_SCRIPT_TASK_STATUS(botPedIndex, SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD) != PERFORMING_TASK

		TASK_VEHICLE_DRIVE_TO_COORD(botPedIndex
									,botVehicle
									,GET_ENTITY_COORDS(playerVehicle)
									,20.0
									,DRIVINGSTYLE_NORMAL
									,BUFFALO
									,DRIVINGMODE_PLOUGHTHROUGH
									,30
									,40)

	ENDIF

ENDPROC

//// PURPOSE:
 ///    If needed create a vehicle so that the Network Bot can use to follow the local host player.
 /// PARAMS:
 ///    playerVehicle - the VEHICLE_INDEX of the local host player.
 ///    botVehicle    - the VEHICLE_INDEX of our network bot.
 ///    botPedIndex   - the PED_INDEX of our network bot.
PROC START_TO_FOLLOW_IN_VEHICLE(VEHICLE_INDEX playerVehicle, VEHICLE_INDEX& botVehicle, PED_INDEX botPedIndex, INT botIndex)

	VEHICLE_SEAT useSeat = VS_DRIVER
	INT numFreeSeats = 0

	IF NOT IS_ENTITY_DEAD(playerVehicle)
		AND GET_VEHICLE_HAS_FREE_SEATS(playerVehicle, useSeat, numFreeSeats, FALSE)
		AND 100.0 > GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(playerVehicle), GET_ENTITY_COORDS(botPedIndex))

			TASK_ENTER_VEHICLE(botPedIndex, playerVehicle, DEFAULT_TIME_BEFORE_WARP, useSeat, PEDMOVE_RUN, ECF_BLOCK_SEAT_SHUFFLING)

	ELIF numFreeSeats = 0

		VECTOR vPos = GET_ENTITY_COORDS(botPedIndex)
		FLOAT xPos = GET_RANDOM_FLOAT_IN_RANGE(5.0, 20.0)
		FLOAT yPos = GET_RANDOM_FLOAT_IN_RANGE(5.0, 20.0)
		vPos.x += xPos
		vPos.y += yPos

		CREATE_LOCAL_BOT_VEHICLE(botVehicle, botPedIndex, botIndex, vPos)

		IF DOES_ENTITY_EXIST(botVehicle)
		AND GET_VEHICLE_HAS_FREE_SEATS(botVehicle, useSeat, numFreeSeats)
		AND NOT IS_ENTITY_DEAD(botPedIndex)
			TASK_ENTER_VEHICLE(botPedIndex, botVehicle, DEFAULT_TIME_BEFORE_WARP, useSeat, PEDMOVE_RUN, ECF_BLOCK_SEAT_SHUFFLING)
		ENDIF

	ENDIF

ENDPROC

#ENDIF
