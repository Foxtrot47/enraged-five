//--------------------------------------------------------------------------------------------
// A Brain that is assigned to each bot to allow them to function
//
//
//
//--------------------------------------------------------------------------------------------
USING "globals.sch"

// Game Headers
USING "commands_network.sch"
USING "script_player.sch"

// Network Headers																		   
USING "net_include.sch"






//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////// VARIABLES /////////////////////////////////////////////////////////////////////////////////////
#IF IS_DEBUG_BUILD

//Main States 

//Const Ints
CONST_INT BOT_STATE_INI			0
CONST_INT BOT_STATE_DECISION	1


//Ints
INT iBotIndex

//Bools

//Vectors

//Strings
STRING iBotName

//Player_index
PLAYER_INDEX BotPlayerId 

//Ped_index
PED_INDEX BotPedIndex






#ENDIF
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                  MAIN PROCS                            ////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#IF IS_DEBUG_BUILD
//PURPOSE: Initialise the bot
PROC INITIALISE_BOT() 
	// This script will not be paused if another script calls PAUSE_GAME
	SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)

	iBotIndex = NETWORK_BOT_GET_SCRIPT_BOT_INDEX()
	iBotName = NETWORK_BOT_GET_NAME(iBotIndex)
	BotPlayerId = NETWORK_BOT_GET_PLAYER_ID(iBotIndex)
	BotPedIndex = GET_PLAYER_PED(BotPlayerId)

	// Starting Mission						 
	NET_PRINT_TIME() 
	NET_PRINT_STRING_INT("This bot started using his brain: ", iBotIndex) NET_NL()	
	NET_PRINT_STRINGS("His name is: ", iBotName) NET_NL()

	IF NOT IS_PED_INJURED(BotPedIndex)
		REMOVE_PED_FROM_GROUP(BotPedIndex)
		GIVE_WEAPON_TO_PED(BotPedIndex, WEAPONTYPE_PISTOL, 25000, TRUE)
	ENDIF
ENDPROC


//PURPOSE: Check whether the brain should terminate
FUNC BOOL SHOULD_THIS_BRAIN_TERMINATE()
	IF NOT NETWORK_BOT_EXISTS(BotPlayerId)
		NET_PRINT_TIME() NET_PRINT_THREAD_ID() NET_PRINT(GET_THIS_SCRIPT_NAME())
		NET_PRINT(" SHOULD_THIS_MULTIPLAYER_SCRIPT_TERMINATE bot no longer exists.") NET_NL()
		RETURN(TRUE)
	ENDIF

	IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
		RETURN(TRUE)
	ENDIF

	RETURN(FALSE)
ENDFUNC






#ENDIF
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                      Main Loop                             ////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
SCRIPT
	#IF IS_DEBUG_BUILD

	//Initialise the bot
	INITIALISE_BOT()

	// Main loop.
	WHILE TRUE

		//Only have ONE wait in your scripts
		WAIT(0) 
		
		// If we have a match end event, bail.
		IF SHOULD_THIS_BRAIN_TERMINATE()
			
			//***Cleanup the bot****
			
			TERMINATE_THIS_THREAD()
		ENDIF
	
		//Respawn the bot if he is killed 
		IF IS_ENTITY_DEAD(botPedIndex)
			WAIT(5000)
			NETWORK_BOT_RESURRECT(iBotIndex)
		ENDIF

		// -----------------------------------
		// Process your Brain logic.....

	ENDWHILE
	#ENDIF
ENDSCRIPT

//EOF//////








