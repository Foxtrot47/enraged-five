
// PURPOSE: Network Bot script BRAIN that makes the bot follow the player


#IF IS_DEBUG_BUILD 

// Global Headers
USING "globals.sch"

// Network Headers
USING "net_bot_utils.sch"

// Game Headers
USING "commands_task.sch"


// Index of the Network bot that this script is controlling.
INT  botIndex

// Player Index of the Network bot that this script is controlling.
PLAYER_INDEX  botPlayerIndex

// Ped Index of the Network bot that this script is controlling.
PED_INDEX  botPedIndex

// Keep track of the vehicle created for the network bot.
VEHICLE_INDEX botVehicle

// Networ Bot possible states
INT  botState
CONST_INT NET_BOT_STATE_INIT           0
CONST_INT NET_BOT_STATE_DECIDE         1
CONST_INT NET_BOT_STATE_ON_FOOT        2
CONST_INT NET_BOT_STATE_ENTER_VEHICLE  3
CONST_INT NET_BOT_STATE_ON_VEHICLE     4
CONST_INT NET_BOT_STATE_IN_COMBAT      5


/// PURPOSE:
///    
PROC RESET_BOT_VEHICLE()

	NET_DEBUG_STRING(NETWORK_BOT_GET_NAME(botIndex), " Cleanup Created Vehicle.")

	IF botVehicle <> NULL
		IF IS_ENTITY_DEAD(botVehicle)
			DELETE_VEHICLE(botVehicle)
		ELSE
			IF DOES_ENTITY_EXIST(botVehicle) 
				IF IS_VEHICLE_EMPTY(botVehicle)
					DELETE_VEHICLE(botVehicle)
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	botVehicle = NULL

ENDPROC

/// PURPOSE:
///    Changes the network bot state and prints a message
/// PARAMS:
///    state - New Value for the network bot state.
PROC SET_BOT_STATE(INT state)

	NET_DEBUG_STRING(NETWORK_BOT_GET_NAME(botIndex), " State Update:")

	SWITCH botState
		CASE NET_BOT_STATE_INIT           NET_DEBUG(".... From NET_BOT_STATE_INIT") BREAK
		CASE NET_BOT_STATE_DECIDE         NET_DEBUG(".... From NET_BOT_STATE_DECIDE") BREAK
		CASE NET_BOT_STATE_ON_FOOT        NET_DEBUG(".... From NET_BOT_STATE_ON_FOOT") BREAK
		CASE NET_BOT_STATE_ENTER_VEHICLE  NET_DEBUG(".... From NET_BOT_STATE_ENTER_VEHICLE") BREAK
		CASE NET_BOT_STATE_ON_VEHICLE     NET_DEBUG(".... From NET_BOT_STATE_ON_VEHICLE") BREAK
		CASE NET_BOT_STATE_IN_COMBAT      NET_DEBUG(".... From NET_BOT_STATE_IN_COMBAT") BREAK
	ENDSWITCH

	SWITCH state
		CASE NET_BOT_STATE_INIT           NET_DEBUG(".... To NET_BOT_STATE_INIT") BREAK
		CASE NET_BOT_STATE_DECIDE         NET_DEBUG(".... To NET_BOT_STATE_DECIDE") BREAK
		CASE NET_BOT_STATE_ON_FOOT        NET_DEBUG(".... To NET_BOT_STATE_ON_FOOT") BREAK
		CASE NET_BOT_STATE_ENTER_VEHICLE  NET_DEBUG(".... To NET_BOT_STATE_ENTER_VEHICLE") BREAK
		CASE NET_BOT_STATE_ON_VEHICLE     NET_DEBUG(".... To NET_BOT_STATE_ON_VEHICLE") BREAK
		CASE NET_BOT_STATE_IN_COMBAT      NET_DEBUG(".... To NET_BOT_STATE_IN_COMBAT") BREAK
	ENDSWITCH

	botState = state

ENDPROC

//// PURPOSE:
 ///    Process logic for state NET_BOT_STATE_INIT
PROC PROCESS_BOT_STATE_INIT()
	CLEAR_PED_TASKS(botPedIndex)
	CLEAR_PED_SECONDARY_TASK(botPedIndex)
	SET_BOT_STATE(NET_BOT_STATE_DECIDE)
	RESET_BOT_VEHICLE()
ENDPROC

//// PURPOSE:
 ///    Process logic for state NET_BOT_STATE_DECIDE
PROC PROCESS_BOT_STATE_DECIDE()

	VEHICLE_INDEX playerVehicle = NULL
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		playerVehicle = GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())
	ENDIF

	IF playerVehicle = NULL
		SET_BOT_STATE(NET_BOT_STATE_ON_FOOT)
	ELSE

		SET_BOT_STATE(NET_BOT_STATE_ENTER_VEHICLE)

		IF botVehicle <> NULL
			IF DOES_ENTITY_EXIST(botVehicle) 
				IF IS_PED_IN_VEHICLE(botPedIndex, botVehicle)
					SET_BOT_STATE(NET_BOT_STATE_ON_VEHICLE)
				ENDIF
			ENDIF
		ENDIF

	ENDIF

ENDPROC

//// PURPOSE:
 ///    Process logic for state NET_BOT_STATE_ON_FOOT
PROC PROCESS_BOT_STATE_ON_FOOT()

	VEHICLE_INDEX playerVehicle = NULL
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		playerVehicle = GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())
	ENDIF

	IF playerVehicle = NULL
		FOLLOW_PLAYER_ON_FOOT(botPedIndex)
	ELSE
		SET_BOT_STATE(NET_BOT_STATE_INIT)
	ENDIF

ENDPROC

//// PURPOSE:
 ///    Process logic for state NET_BOT_STATE_ENTER_VEHICLE
PROC PROCESS_BOT_STATE_ENTER_VEHICLE()

	VEHICLE_INDEX tmpBotVehicle = NULL
	IF NOT IS_ENTITY_DEAD(botPedIndex)
		tmpBotVehicle = GET_VEHICLE_PED_IS_USING(botPedIndex)
	ENDIF

	VEHICLE_INDEX playerVehicle = NULL
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		playerVehicle = GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())
	ENDIF
	
	IF playerVehicle <> NULL

		// We need to create a vehicle
		IF botVehicle = NULL AND tmpBotVehicle = NULL

			START_TO_FOLLOW_IN_VEHICLE(playerVehicle, botVehicle, botPedIndex, botIndex)

		// We already have a vehicle but arent inside it.
		ELIF tmpBotVehicle = NULL AND botVehicle <> NULL AND DOES_ENTITY_EXIST(botVehicle)

			VEHICLE_SEAT useSeat = VS_DRIVER
			INT numFreeSeats = 0

			IF 100.0 > GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(botVehicle), GET_ENTITY_COORDS(botPedIndex))
			AND GET_VEHICLE_HAS_FREE_SEATS(botVehicle, useSeat, numFreeSeats)
			AND IS_VEHICLE_STOPPED(botVehicle)
			AND NOT IS_ENTITY_DEAD(botPedIndex)

				TASK_ENTER_VEHICLE(botPedIndex, botVehicle, DEFAULT_TIME_BEFORE_WARP, useSeat, PEDMOVE_RUN, ECF_BLOCK_SEAT_SHUFFLING)

			//ELSE
				//RESET_BOT_VEHICLE()

			ENDIF

		//ELIF tmpBotVehicle = NULL AND botVehicle <> NULL
		//		RESET_BOT_VEHICLE()

		ELIF tmpBotVehicle = botVehicle
			SET_BOT_STATE(NET_BOT_STATE_ON_VEHICLE)
		ENDIF

	ELSE

		SET_BOT_STATE(NET_BOT_STATE_INIT)

	ENDIF

ENDPROC

//// PURPOSE:
 ///    Process logic for state NET_BOT_STATE_ON_VEHICLE
PROC PROCESS_BOT_STATE_ON_VEHICLE()

	VEHICLE_INDEX tmpBotVehicle = NULL
	IF NOT IS_ENTITY_DEAD(botPedIndex)
		tmpBotVehicle = GET_VEHICLE_PED_IS_USING(botPedIndex)
	ENDIF

	VEHICLE_INDEX playerVehicle = NULL
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		playerVehicle = GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())
	ENDIF

	IF playerVehicle <> NULL AND tmpBotVehicle <> NULL
		IF playerVehicle <> tmpBotVehicle
			FOLLOW_PLAYER_ON_VEHICLE(botPedIndex, tmpBotVehicle, playerVehicle)
		ENDIF
	ELSE

		IF NOT IS_VEHICLE_DRIVEABLE(botVehicle)
			RESET_BOT_VEHICLE()
		ENDIF

		SET_BOT_STATE(NET_BOT_STATE_INIT)
	ENDIF

ENDPROC

//// PURPOSE:
 ///    Process logic for state NET_BOT_STATE_IN_COMBAT
PROC PROCESS_BOT_STATE_IN_COMBAT()
ENDPROC

#ENDIF

// ---------------------------------------------------------
// ----------------------- Main Loop -----------------------
// ---------------------------------------------------------
SCRIPT

#IF IS_DEBUG_BUILD 

	SET_BOT_STATE(NET_BOT_STATE_INIT)

	botIndex        = -1
	botPlayerIndex  = NULL
	botPedIndex     = NULL
	botVehicle      = NULL
	netbotCheckForSeatCounter = 500

	WAIT_FOR_START(botIndex, botPlayerIndex, botPedIndex)

	NET_PRINT("MAIN GAME LOOP STARTING")
	NET_PRINT_STRINGS("Using Bot with Name - ", NETWORK_BOT_GET_NAME(botIndex))

	// -----------------------
	// Main loop.
	// -----------------------
	WHILE TRUE
		WAIT(0)

		IF CHECK_FOR_TERMINATE(botIndex, botPlayerIndex)
			TERMINATE_THIS_THREAD()
		ENDIF

		IF IS_ENTITY_DEAD(botPedIndex)
			NETWORK_BOT_RESURRECT(botIndex)
		ENDIF

		SWITCH botState
			CASE NET_BOT_STATE_INIT
				PROCESS_BOT_STATE_INIT()
			BREAK
			CASE NET_BOT_STATE_DECIDE
				PROCESS_BOT_STATE_DECIDE()
			BREAK
			CASE NET_BOT_STATE_ON_FOOT
				PROCESS_BOT_STATE_ON_FOOT()
			BREAK
			CASE NET_BOT_STATE_ENTER_VEHICLE
				PROCESS_BOT_STATE_ENTER_VEHICLE()
			BREAK
			CASE NET_BOT_STATE_ON_VEHICLE
				PROCESS_BOT_STATE_ON_VEHICLE()
			BREAK
			CASE NET_BOT_STATE_IN_COMBAT
				PROCESS_BOT_STATE_IN_COMBAT()
			BREAK
		ENDSWITCH

		netbotCheckForSeatCounter++

	ENDWHILE

#ENDIF

// End of Mission
ENDSCRIPT

