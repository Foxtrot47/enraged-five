//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        CASINO_LUCKY_WHEEL.sc																					//
// Description: Maintains the Casino Lucky Wheel Minigame.																//
// Written by:  Online Technical Team: Scott Ranken																		//
// Date:  		13/03/19																								//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "net_include.sch"
USING "net_blips.sch"
USING "rc_helper_functions.sch"
USING "freemode_header.sch"
USING "net_gun_locker.sch"

#IF FEATURE_CASINO
USING "net_lucky_reward.sch"
USING "net_simple_interior_private.sch"

//╒══════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ CONSTANTS ╞═══════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

CONST_INT ciLUCKY_WHEEL_SEGMENTS							20								// The total number of segments that are shown on the wheel
CONST_INT ciLUCKY_WHEEL_SEGMENT_ANGLE						360/ciLUCKY_WHEEL_SEGMENTS		// The angle of each wheel segment
CONST_INT ciLUCKY_WHEEL_IDLE_KICK_TIME_MS					10000							// The time available to activate the minigame from idle state
CONST_INT ciLUCKY_WHEEL_ACTIVATION_TIME_MS					5000							// The time available to spin the lucky wheel once activated
CONST_INT ciLUCKY_WHEEL_ENTRY_ANIM_SAFETY_TIMER_MS			5000							// Safety timer for the entry anim
CONST_INT ciLUCKY_WHEEL_UPDATE_SERVER_BD_TIME_MS			1500							// Time given for the server BD to update
CONST_INT ciLUCKY_WHEEL_SPIN_TIME_THRESHOLD_FAST_MS			3000							// Threshold time for fast spin setting
CONST_INT ciLUCKY_WHEEL_SPIN_TIME_THRESHOLD_MEDIUM_MS		1500							// Threshold time for fast medium setting
CONST_INT ciLUCKY_WHEEL_SEGMENT_SPIN_TIME_FAST_MS			110								// Fast base value that is multipled by number of steps to get total time
CONST_INT ciLUCKY_WHEEL_SEGMENT_SPIN_TIME_MEDIUM_MS			353								// Medium base value that is multipled by number of steps to get total time
CONST_INT ciLUCKY_WHEEL_SEGMENT_SPIN_TIME_SLOW_MS			535								// Slow base value that is multipled by number of steps to get total time
CONST_INT ciLUCKY_WHEEL_NUMBER_OF_SPINS_FAST				4								// Number of full rotations for the fast setting
CONST_INT ciLUCKY_WHEEL_NUMBER_OF_SPINS_MEDIUM				1								// Number of full rotations for the medium setting
CONST_INT ciLUCKY_WHEEL_NUMBER_OF_SPINS_SLOW				0								// Number of full rotations for the slow setting
CONST_INT ciLUCKY_WHEEL_TRIGGER_WIN_ANIM_FAST_TIME_MS		2000							// Trigger the win animation how many milliseconds before the fast spin ends
CONST_INT ciLUCKY_WHEEL_TRIGGER_WIN_ANIM_MEDIUM_TIME_MS		2300							// Trigger the win animation how many milliseconds before the medium spin ends
CONST_INT ciLUCKY_WHEEL_TRIGGER_WIN_ANIM_SLOW_TIME_MS		2400							// Trigger the win animation how many milliseconds before the slow spin ends

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ BIT SETS ╞═══════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

// luckyWheelData.iBS
CONST_INT BS_LUCKY_WHEEL_ACTIVATED							0
CONST_INT BS_LUCKY_WHEEL_SET_PRIZE							1
CONST_INT BS_LUCKY_WHEEL_START_SPIN							2
CONST_INT BS_LUCKY_WHEEL_SET_SEGMENT_LERP_TIMER				3
CONST_INT BS_LUCKY_WHEEL_SET_ACTIVATION_TIMER				4
CONST_INT BS_LUCKY_WHEEL_SET_WHEEL_SPIN_PROPERTIES			5
CONST_INT BS_LUCKY_WHEEL_DISABLE_FREEMODE_FEATURES			6
CONST_INT BS_LUCKY_WHEEL_ENTRY_ANIM_PLAYING					7
CONST_INT BS_LUCKY_WHEEL_GO_TO_COORDS_TASK_SET				8
CONST_INT BS_LUCKY_WHEEL_READY_TO_SPIN_WHEEL				9
CONST_INT BS_LUCKY_WHEEL_PERFORM_EXIT_ANIM					10
CONST_INT BS_LUCKY_WHEEL_PLAYER_INSIDE_LOCATE				11
CONST_INT BS_LUCKY_WHEEL_TRIGGERED							12
CONST_INT BS_LUCKY_WHEEL_IDLE_TIMED_OUT						13
CONST_INT BS_LUCKY_WHEEL_SPIN_ANIM_COMPLETE					14
CONST_INT BS_LUCKY_WHEEL_PRESSED_DOWN_ON_RAISED_ARM			15
CONST_INT BS_LUCKY_WHEEL_START_FROM_ARM_RAISED_ANIM			16
CONST_INT BS_LUCKY_WHEEL_PERFORM_WIN_ANIM					17
CONST_INT BS_LUCKY_WHEEL_PERFORM_IDLE_TRANS_TO_SPIN_IDLE	18
CONST_INT BS_LUCKY_WHEEL_PICK_RANDOM_MYSTERY_PRIZE			19			// (Set this when random mystrey prize is choosen) If this bit changes make sure to update GIVE_PLAYER_MYSTERY_PRIZE - iPickRandomMystreyReward
CONST_INT BS_LUCKY_WHEEL_USAGE_HELP							20
CONST_INT BS_LUCKY_WHEEL_TRIGGER_USAGE_STAT					21
CONST_INT BS_LUCKY_WHEEL_STICK_UP_LIMIT_REACHED				22
CONST_INT BS_LUCKY_WHEEL_TURN_OFF_WHEEL_LIGHTS_ON_INIT		23
CONST_INT BS_LUCKY_WHEEL_RECEIVED_EVENT_FROM_WHEEL_USER		24
CONST_INT BS_LUCKY_WHEEL_BANNED_HELP						25
CONST_INT BS_LUCKY_WHEEL_GET_SPEED_ON_FIRST_FRAME			26
CONST_INT BS_LUCKY_WHEEL_PLAY_TICK							27
CONST_INT BS_LUCKY_WHEEL_REGION_BLOCK_HELP					28

// DISCLAIMER_MENU iLocalBs
CONST_INT BS_DISCLAIMER_MENU_REBUILD_DISCLAIMER_MENU		0

// iPermanentBS
CONST_INT BS_PERMANENT_WHEEL_SYNC_SEGMENT_ON_ENTRY			0
CONST_INT BS_PERMANENT_WHEEL_MOVE_PLAYER_OUT_OF_AREA		1
CONST_INT BS_PERMANENT_LUCKY_WHEEL_PRIZE_HELP_ON_SCREEN		2
CONST_INT BS_PERMANENT_LUCKY_WHEEL_DISCLAIMER_MENU_SHOWN	3
CONST_INT BS_PERMANENT_LUCKY_WHEEL_VOUCHER_DETAIL_HELP		4
CONST_INT BS_PERMANENT_LUCKY_WHEEL_REMAINING_TIME_TICKER	5


// m_PlayerBD.iBS
CONST_INT PLAYER_BROADCAST_DATA_START_SPIN					0


//╒══════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════════╡ ENUMS ╞═════════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

ENUM LUCKY_WHEEL_SPIN_INTENSITY
	LUCKY_WHEEL_SPIN_INTENSITY_INVALID = -1,
	LUCKY_WHEEL_SPIN_INTENSITY_LOW = 0,
	LUCKY_WHEEL_SPIN_INTENSITY_MEDIUM,
	LUCKY_WHEEL_SPIN_INTENSITY_HIGH
ENDENUM

ENUM LUCKY_WHEEL_ANIM_CLIPS
	LUCKY_WHEEL_ANIM_CLIP_INVALID = -1,
	LUCKY_WHEEL_ANIM_CLIP_ENTRY = 0,
	LUCKY_WHEEL_ANIM_CLIP_IDLE_1,
	LUCKY_WHEEL_ANIM_CLIP_RAISE_ARM,
	LUCKY_WHEEL_ANIM_CLIP_RAISE_ARM_IDLE,
	LUCKY_WHEEL_ANIM_CLIP_TIMED_OUT,
	LUCKY_WHEEL_ANIM_CLIP_SPIN_LOW,
	LUCKY_WHEEL_ANIM_CLIP_SPIN_MEDIUM,
	LUCKY_WHEEL_ANIM_CLIP_SPIN_HIGH,
	LUCKY_WHEEL_ANIM_CLIP_SPINNING_IDLE_LOW,
	LUCKY_WHEEL_ANIM_CLIP_SPINNING_IDLE_MEDIUM,
	LUCKY_WHEEL_ANIM_CLIP_SPINNING_IDLE_HIGH,
	LUCKY_WHEEL_ANIM_CLIP_WIN,
	LUCKY_WHEEL_ANIM_CLIP_WIN_BIG,
	LUCKY_WHEEL_ANIM_CLIP_WIN_HUGE,
	LUCKY_WHEEL_ANIM_CLIP_EXIT,
	LUCKY_WHEEL_ANIM_CLIP_ARM_RAISED_TO_IDLE
ENDENUM

ENUM LUCKY_WHEEL_PRIZES
	LUCKY_WHEEL_PRIZE_CLOTHING_1 = 0,
	LUCKY_WHEEL_PRIZE_RP_2500,
	LUCKY_WHEEL_PRIZE_CASH_20000,
	LUCKY_WHEEL_PRIZE_HOUSE_CHIPS_10000,
	LUCKY_WHEEL_PRIZE_DISCOUNT_VOUCHER,
	LUCKY_WHEEL_PRIZE_RP_5000,
	LUCKY_WHEEL_PRIZE_CASH_30000,
	LUCKY_WHEEL_PRIZE_HOUSE_CHIPS_15000,
	LUCKY_WHEEL_PRIZE_CLOTHING_2,
	LUCKY_WHEEL_PRIZE_RP_7500,
	LUCKY_WHEEL_PRIZE_HOUSE_CHIPS_20000,
	LUCKY_WHEEL_PRIZE_MYSTERY_STAR,
	LUCKY_WHEEL_PRIZE_CLOTHING_3,
	LUCKY_WHEEL_PRIZE_RP_10000,
	LUCKY_WHEEL_PRIZE_CASH_40000,
	LUCKY_WHEEL_PRIZE_HOUSE_CHIPS_25000,
	LUCKY_WHEEL_PRIZE_CLOTHING_4,
	LUCKY_WHEEL_PRIZE_RP_15000,
	LUCKY_WHEEL_PRIZE_PODIUM_VEHICLE,
	LUCKY_WHEEL_PRIZE_CASH_50000
ENDENUM

ENUM LUCKY_WHEEL_PRIZE_TYPE
	LUCKY_WHEEL_PRIZE_TYPE_INVALID = -1,
	LUCKY_WHEEL_PRIZE_TYPE_CLOTHING = 0,
	LUCKY_WHEEL_PRIZE_TYPE_RP,
	LUCKY_WHEEL_PRIZE_TYPE_CASH,
	LUCKY_WHEEL_PRIZE_TYPE_CHIPS,
	LUCKY_WHEEL_PRIZE_TYPE_DISCOUNT_VOUCHER,
	LUCKY_WHEEL_PRIZE_TYPE_MYSTERY_STAR,
	LUCKY_WHEEL_PRIZE_TYPE_PODIUM_VEHICLE
ENDENUM

ENUM LUCKY_WHEEL_FLOW_STATES
	LUCKY_WHEEL_FLOW_STATE_INVALID = -1,
	LUCKY_WHEEL_FLOW_STATE_INITAILISE = 0,
	LUCKY_WHEEL_FLOW_STATE_DETERMINE_PLAYER,
	LUCKY_WHEEL_FLOW_STATE_DISCLAIMER_MENU,
	LUCKY_WHEEL_FLOW_STATE_GO_TO_ENTRY_COORDS,
	LUCKY_WHEEL_FLOW_STATE_ENTRY_ANIM,
	LUCKY_WHEEL_FLOW_STATE_IDLE,
	LUCKY_WHEEL_FLOW_STATE_RAISED_IDLE,
	LUCKY_WHEEL_FLOW_STATE_RAISED_IDLE_TO_IDLE,
	LUCKY_WHEEL_FLOW_STATE_USER_INPUT,
	LUCKY_WHEEL_FLOW_STATE_SPINNING,
	LUCKY_WHEEL_FLOW_STATE_WINNER,
	LUCKY_WHEEL_FLOW_STATE_REWARDS,
	LUCKY_WHEEL_FLOW_STATE_TIMED_OUT,
	LUCKY_WHEEL_FLOW_STATE_NET_DELAY,
	LUCKY_WHEEL_FLOW_STATE_REMOTE_START_SPIN_ANIM,
	LUCKY_WHEEL_FLOW_STATE_REMOTE_START_MANUAL_SPIN,
	LUCKY_WHEEL_FLOW_STATE_USAGE_STAT,
	LUCKY_WHEEL_FLOW_STATE_CLEANUP
ENDENUM

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ STRUCTS ╞════════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

STRUCT LUCKY_WHEEL_MINIGAME_SERVER_DATA
	PLAYER_INDEX minigamePlayer										// Player index of the minigame player
	INT iServerSpinIndex = 0										// The start index of animation
ENDSTRUCT
LUCKY_WHEEL_MINIGAME_SERVER_DATA luckyWheelServerData

STRUCT LUCKY_WHEEL_MINIGAME_PLAYER_DATA
	INT iPrizeSegment = -1											// Store the prize segment
	INT iBS															// player broadcast data Bit set
	INT iSceneID = -1												// The current animation scene ID
	INT iActivationTimeMS = ciLUCKY_WHEEL_ACTIVATION_TIME_MS		// Activation time player has to spin the wheel after triggered the minigame
	
	LUCKY_WHEEL_SPIN_INTENSITY eSpinIntensity						// The intensity of the wheel spin based on user input
ENDSTRUCT
LUCKY_WHEEL_MINIGAME_PLAYER_DATA m_PlayerBD[NUM_NETWORK_PLAYERS]

STRUCT DISCLAIMER_MENU
	INT iLocalBS
	INT iCurrentMenuItem, iMenuTopItem, iNumMenuItems
	INT iLeftX, iRightX, iLeftY, iRightY, iMoveUp
	BOOL bAccept, bBack, bPrevious, bNext		
	TIME_DATATYPE scrollDelay
ENDSTRUCT

STRUCT LUCKY_WHEEL_MINIGAME_DATA
	INT iBS															// Main bitset variable
	INT iPermanentBS												// This bitset will not get reset on lucky wheel clean up 
	
	INT iActivationTimerDifference									// Difference between current network time and activation timer offset
	
	INT iLeftAnalogueStickX											// X axis left analogue input
	INT iLeftAnalogueStickY											// Y axis left analogue input
	
	INT iRightAnalogueStickX										// X axis Right analogue input
	INT iRightAnalogueStickY										// Y axis Right analogue input
	
	INT iStartingSegment											// What is out current starting segment (wheel does not restart to original starting point)
	INT iFullSpins													// Total number of spins the wheel will perform
	INT iSpinsCompleted												// Current number of spins completed in a single play of the minigame
	
	INT iSpinStartIndex	= 0 										// The start index of animation
	INT iWheelSceneID = -1											// The current wheel animation scene ID
	
	INT iNumSpinSteps												// How many segments we're going to step through to get to our final segment
	INT iCurrSpinStep												// Step counter. How many segments we've stepped through
	INT iCurrentSegmentIndex										// The current wheel segment
	
	INT iSegmentTimeMS												// Segment time in milliseconds the wheel will spin to reach the segment
	INT iDuration													// The duration it takes to lerp from starting segment to prize segment
	INT iNumTimesLightFlash	= -1									// Number of times prop lights flashed
	
	FLOAT fCurrentAngle												// Current angle of the lucky wheel
	FLOAT fStartingAngle											// Starting angle after each lerp from segment to segment
	FLOAT fEndingAngle												// Prize segment angle we want to land on
	FLOAT fUncappedCurrentAngle										// Used to track the current wheel segment
	
	STRING sAnimClip												// Stores the current animation clip to play
	STRING sAnimDict												// Stores the current animation dictionary to play from
	
	TIME_DATATYPE tdActivationTime									// Activation timer player has once they have triggered the minigame
	TIME_DATATYPE tdTotalLerpTime									// Total lerp time of a wheel spin
	TIME_DATATYPE tdTriggerWinAnimTime								// Time until we trigger the win anim just before a spin ends
	
	SCRIPT_TIMER stIdleSafetyTimer									// Kick the player when in idle state if no user input after 10 seconds
	SCRIPT_TIMER stUpdateServerBDTimer								// Timer to give the server BD time to update
	SCRIPT_TIMER stGoToEntryCoordsTaskSafetyTimer					// Safety timer for if the GO_TO_COORDS task takes too long for the entry animation
	SCRIPT_TIMER sNetDelay											// Delay active player to start the anim so remote player can sync the anim localy 
	SCRIPT_TIMER sLightSwitchOnTimer								// Timer to turn on lights around the wheel prop
	SCRIPT_TIMER sLightSwitchOffTimer								// Timer to turn off lights around the wheel prop
	SCRIPT_TIMER stControlStickUpTimer								// Block press up after 10 seconds 
	SCRIPT_TIMER sDisclaimerMenuTimeOut								// Time out if player is taking too long in disclaimer menu
	SCRIPT_TIMER sPrizeHelpTimer									// Timer to check if help text is on screen
	
	LUCKY_WHEEL_FLOW_STATES eState									// Flow states for the minigame logic
	LUCKY_WHEEL_ANIM_CLIPS eIdleClip = LUCKY_WHEEL_ANIM_CLIP_IDLE_1	// Which idle clip varation to play
	
	LUCKY_REWARD_MYSTERY_TYPE	eMystreyRewardType	= LRM_INVALID	// Mystrey reward type
	
	STRUCT_LUCKYSEVENMETRIC sMetricData								// Metric data for the lucky wheel
	
	OBJECT_INDEX objLuckyWheel										// The local wheel object
	VEHICLE_INDEX rewardVehicle										// Reward vehicle
	
	LUCKY_WHEEL_REWARD_STRUCT sLuckyReward							// Lucky reward struc which has all the variables for reward transcations 
	
	TEXT_LABEL_63 sPrizeName										// Store the name of given prize
	INT iPrizeNumber												// Store the number of given prize
	
	FLOAT fStartSoundSpeed
	FLOAT fPreviousRot
	INT iStartSpinSoundID = -1
	INT iSpinTickSoundID = -1
	INT iWinSoundID = -1
	INT iWinTypeSoundID = -1
	INT iGivenRewardPosixTime										// posix time when player given the reward 
	DISCLAIMER_MENU sDisclaimerMenu
	
	GENERIC_TRANSACTION_STATE	eTransactionResult					// Transaction result for lucky wheel stat changes
	
ENDSTRUCT
LUCKY_WHEEL_MINIGAME_DATA luckyWheelData

VAULT_WEAPON_LOADOUT_CUSTOMIZATION sGunLocker

#IF IS_DEBUG_BUILD
STRUCT LUCKY_WHEEL_MINIGAME_DEBUG_DATA
	INT iFastSpinTimeMS = 110										// Debug Data: Fastest spin time in seconds
	INT iMediumSpinTimeMS = 353										// Debug Data: Medium spin time in seconds
	INT iSlowSpinTimeMS = 535										// Debug Data: Slowest spin time in seconds
	
	BOOL bEnableDebugPrize = FALSE									// Debug Data: Enable setting the prize via rag
	INT iDebugPrize = 0												// Debug Data: Set the prize via rag
	TEXT_WIDGET_ID widgetPrize										// Debug Data: The prize name in rag
	INT iDebugMystreyPrize = 0										// Debug Data: Set the mystrey prize via rag
	TEXT_WIDGET_ID widgetMystreyPrize								// Debug Data: The mystrey prize name in rag
	TEXT_WIDGET_ID widgetMystreyVehiclePrize						// Debug Data: The mystrey vehicle prize name in rag
	TEXT_WIDGET_ID widgetMystreyClothesPrize						// Debug Data: The mystrey Clothes prize name in rag
	TEXT_WIDGET_ID widgetMystreyInventoryPrize						// Debug Data: The mystrey Inventory prize name in rag
	TEXT_WIDGET_ID widgetMystreyBusinessPrize						// Debug Data: The mystrey Business prize name in rag
	
	INT iFastNumSpins = 4											// Debug Data: Number of spins for fastest setting
	INT iMediumNumSpins = 1											// Debug Data: Number of spins for medium setting
	INT iSlowNumSpins = 0											// Debug Data: Number of spins for slowest setting
	
	INT iIdleTimerDifference = 0									// Debug Data: To display the idle timer counting down to kick
	
	BOOL bDisplaySpinDebug = FALSE									// Debug Data: Displays the wheel spin settings on-screen debug
	BOOL bDisplayServerBDDebug = FALSE								// Debug Data: Displays the server BD on-screen debug
	BOOL bResetWheelRotation = FALSE								// Debug Data: Resets the wheels rotation for testing purposes
	
	FLOAT fRotationAngle											// Debug Data: manually change wheel rotation
	BOOL bEnableManualRotation = FALSE
	
	BOOL bByPass24HLimit
	BOOL bClearPodiumVehicleWonTime
	BOOL bClear24HLimit
	
	BOOL bItemWeight
	BOOL bGenerateRandomSegment
	INT iNumOfRadomRewards
ENDSTRUCT
LUCKY_WHEEL_MINIGAME_DEBUG_DATA luckyWheelDebugData

FUNC STRING GET_BIT_SET_NAME(INT iBit)
	SWITCH iBit
		CASE BS_LUCKY_WHEEL_ACTIVATED								RETURN "BS_LUCKY_WHEEL_ACTIVATED"
		CASE BS_LUCKY_WHEEL_SET_PRIZE								RETURN "BS_LUCKY_WHEEL_SET_PRIZE"						
		CASE BS_LUCKY_WHEEL_START_SPIN								RETURN "BS_LUCKY_WHEEL_START_SPIN"				
		CASE BS_LUCKY_WHEEL_SET_SEGMENT_LERP_TIMER					RETURN "BS_LUCKY_WHEEL_SET_SEGMENT_LERP_TIMER"		
		CASE BS_LUCKY_WHEEL_SET_ACTIVATION_TIMER					RETURN "BS_LUCKY_WHEEL_SET_ACTIVATION_TIMER"			
		CASE BS_LUCKY_WHEEL_SET_WHEEL_SPIN_PROPERTIES				RETURN "BS_LUCKY_WHEEL_SET_WHEEL_SPIN_PROPERTIES"	
		CASE BS_LUCKY_WHEEL_DISABLE_FREEMODE_FEATURES				RETURN "BS_LUCKY_WHEEL_DISABLE_FREEMODE_FEATURES"	
		CASE BS_LUCKY_WHEEL_ENTRY_ANIM_PLAYING						RETURN "BS_LUCKY_WHEEL_ENTRY_ANIM_PLAYING"			
		CASE BS_LUCKY_WHEEL_GO_TO_COORDS_TASK_SET					RETURN "BS_LUCKY_WHEEL_GO_TO_COORDS_TASK_SET"		
		CASE BS_LUCKY_WHEEL_READY_TO_SPIN_WHEEL						RETURN "BS_LUCKY_WHEEL_READY_TO_SPIN_WHEEL"		
		CASE BS_LUCKY_WHEEL_PERFORM_EXIT_ANIM						RETURN "BS_LUCKY_WHEEL_PERFORM_EXIT_ANIM"			
		CASE BS_LUCKY_WHEEL_PLAYER_INSIDE_LOCATE					RETURN "BS_LUCKY_WHEEL_PLAYER_INSIDE_LOCATE"
		CASE BS_LUCKY_WHEEL_TRIGGERED								RETURN "BS_LUCKY_WHEEL_TRIGGERED"					
		CASE BS_LUCKY_WHEEL_IDLE_TIMED_OUT							RETURN "BS_LUCKY_WHEEL_IDLE_TIMED_OUT"	
		CASE BS_LUCKY_WHEEL_SPIN_ANIM_COMPLETE						RETURN "BS_LUCKY_WHEEL_SPIN_ANIM_COMPLETE"		
		CASE BS_LUCKY_WHEEL_PRESSED_DOWN_ON_RAISED_ARM				RETURN "BS_LUCKY_WHEEL_PRESSED_DOWN_ON_RAISED_ARM"	
		CASE BS_LUCKY_WHEEL_START_FROM_ARM_RAISED_ANIM				RETURN "BS_LUCKY_WHEEL_START_FROM_ARM_RAISED_ANIM"
		CASE BS_LUCKY_WHEEL_PERFORM_WIN_ANIM						RETURN "BS_LUCKY_WHEEL_PERFORM_WIN_ANIM"
		CASE BS_LUCKY_WHEEL_PERFORM_IDLE_TRANS_TO_SPIN_IDLE			RETURN "BS_LUCKY_WHEEL_PERFORM_IDLE_TRANS_TO_SPIN_IDLE"
		CASE BS_LUCKY_WHEEL_PICK_RANDOM_MYSTERY_PRIZE				RETURN "BS_LUCKY_WHEEL_PICK_RANDOM_MYSTERY_PRIZE"
		CASE BS_LUCKY_WHEEL_USAGE_HELP								RETURN "BS_LUCKY_WHEEL_USAGE_HELP"
		CASE BS_LUCKY_WHEEL_TRIGGER_USAGE_STAT						RETURN "BS_LUCKY_WHEEL_TRIGGER_USAGE_STAT"
		CASE BS_LUCKY_WHEEL_STICK_UP_LIMIT_REACHED					RETURN "BS_LUCKY_WHEEL_STICK_UP_LIMIT_REACHED"
		CASE BS_LUCKY_WHEEL_TURN_OFF_WHEEL_LIGHTS_ON_INIT			RETURN "BS_LUCKY_WHEEL_TURN_OFF_WHEEL_LIGHTS_ON_INIT"
		CASE BS_LUCKY_WHEEL_RECEIVED_EVENT_FROM_WHEEL_USER			RETURN "BS_LUCKY_WHEEL_RECEIVED_EVENT_FROM_WHEEL_USER"
		CASE BS_LUCKY_WHEEL_BANNED_HELP								RETURN "BS_LUCKY_WHEEL_BANNED_HELP"
		CASE BS_LUCKY_WHEEL_GET_SPEED_ON_FIRST_FRAME				RETURN "BS_LUCKY_WHEEL_GET_SPEED_ON_FIRST_FRAME"
		CASE BS_LUCKY_WHEEL_PLAY_TICK								RETURN "BS_LUCKY_WHEEL_PLAY_TICK"
	ENDSWITCH
	
	RETURN ""
ENDFUNC 

#ENDIF

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ FUNCTIONS ╞══════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

FUNC STRING GET_LUCKY_WHEEL_FLOW_STATE_STRING(LUCKY_WHEEL_FLOW_STATES eState)
	SWITCH eState
		CASE LUCKY_WHEEL_FLOW_STATE_INVALID						RETURN "LUCKY_WHEEL_FLOW_STATE_INVALID"
		CASE LUCKY_WHEEL_FLOW_STATE_INITAILISE					RETURN "LUCKY_WHEEL_FLOW_STATE_INITAILISE"
		CASE LUCKY_WHEEL_FLOW_STATE_DETERMINE_PLAYER			RETURN "LUCKY_WHEEL_FLOW_STATE_DETERMINE_PLAYER"
		CASE LUCKY_WHEEL_FLOW_STATE_GO_TO_ENTRY_COORDS			RETURN "LUCKY_WHEEL_FLOW_STATE_GO_TO_ENTRY_COORDS"
		CASE LUCKY_WHEEL_FLOW_STATE_ENTRY_ANIM					RETURN "LUCKY_WHEEL_FLOW_STATE_ENTRY_ANIM"
		CASE LUCKY_WHEEL_FLOW_STATE_IDLE						RETURN "LUCKY_WHEEL_FLOW_STATE_IDLE"
		CASE LUCKY_WHEEL_FLOW_STATE_USER_INPUT					RETURN "LUCKY_WHEEL_FLOW_STATE_USER_INPUT"
		CASE LUCKY_WHEEL_FLOW_STATE_SPINNING					RETURN "LUCKY_WHEEL_FLOW_STATE_SPINNING"
		CASE LUCKY_WHEEL_FLOW_STATE_WINNER						RETURN "LUCKY_WHEEL_FLOW_STATE_WINNER"
		CASE LUCKY_WHEEL_FLOW_STATE_TIMED_OUT					RETURN "LUCKY_WHEEL_FLOW_STATE_TIMED_OUT"
		CASE LUCKY_WHEEL_FLOW_STATE_REMOTE_START_SPIN_ANIM		RETURN "LUCKY_WHEEL_FLOW_STATE_REMOTE_START_SPIN_ANIM"
		CASE LUCKY_WHEEL_FLOW_STATE_REMOTE_START_MANUAL_SPIN	RETURN "LUCKY_WHEEL_FLOW_STATE_REMOTE_START_MANUAL_SPIN"	
		CASE LUCKY_WHEEL_FLOW_STATE_CLEANUP						RETURN "LUCKY_WHEEL_FLOW_STATE_CLEANUP"
		CASE LUCKY_WHEEL_FLOW_STATE_REWARDS						RETURN "LUCKY_WHEEL_FLOW_STATE_REWARDS"
		CASE LUCKY_WHEEL_FLOW_STATE_NET_DELAY					RETURN "LUCKY_WHEEL_FLOW_STATE_NET_DELAY"
		CASE LUCKY_WHEEL_FLOW_STATE_RAISED_IDLE					RETURN "LUCKY_WHEEL_FLOW_STATE_RAISED_IDLE"
		CASE LUCKY_WHEEL_FLOW_STATE_RAISED_IDLE_TO_IDLE			RETURN "LUCKY_WHEEL_FLOW_STATE_RAISED_IDLE_TO_IDLE"
		CASE LUCKY_WHEEL_FLOW_STATE_DISCLAIMER_MENU				RETURN "LUCKY_WHEEL_FLOW_STATE_DISCLAIMER_MENU"
		CASE LUCKY_WHEEL_FLOW_STATE_USAGE_STAT					RETURN "LUCKY_WHEEL_FLOW_STATE_USAGE_STAT"
	ENDSWITCH
	RETURN "null"
ENDFUNC

FUNC STRING GET_LUCKY_WHEEL_SPIN_INTENSITY_STRING(LUCKY_WHEEL_SPIN_INTENSITY eSpinIntensity)
	SWITCH eSpinIntensity
		CASE LUCKY_WHEEL_SPIN_INTENSITY_INVALID				RETURN "LUCKY_WHEEL_SPIN_INTENSITY_INVALID"
		CASE LUCKY_WHEEL_SPIN_INTENSITY_LOW 				RETURN "LUCKY_WHEEL_SPIN_INTENSITY_LOW"
		CASE LUCKY_WHEEL_SPIN_INTENSITY_MEDIUM				RETURN "LUCKY_WHEEL_SPIN_INTENSITY_MEDIUM"
		CASE LUCKY_WHEEL_SPIN_INTENSITY_HIGH				RETURN "LUCKY_WHEEL_SPIN_INTENSITY_HIGH"
	ENDSWITCH
	RETURN "null"
ENDFUNC

FUNC STRING GET_LUCKY_WHEEL_SPIN_INTENSITY_NAME(LUCKY_WHEEL_SPIN_INTENSITY eSpinIntensity)
	SWITCH eSpinIntensity
		CASE LUCKY_WHEEL_SPIN_INTENSITY_INVALID				RETURN "Invalid"
		CASE LUCKY_WHEEL_SPIN_INTENSITY_LOW 				RETURN "Low"
		CASE LUCKY_WHEEL_SPIN_INTENSITY_MEDIUM				RETURN "Medium"
		CASE LUCKY_WHEEL_SPIN_INTENSITY_HIGH				RETURN "High"
	ENDSWITCH
	RETURN "null"
ENDFUNC

FUNC LUCKY_WHEEL_FLOW_STATES GET_LUCKY_WHEEL_FLOW_STATE()
	RETURN luckyWheelData.eState
ENDFUNC

PROC SET_LUCKY_WHEEL_FLOW_STATE(LUCKY_WHEEL_FLOW_STATES eState)
	DEBUG_PRINTCALLSTACK()
	luckyWheelData.eState = eState
	#IF IS_DEBUG_BUILD
	PRINTLN("[LUCKY_WHEEL] SET_LUCKY_WHEEL_FLOW_STATE - Setting flow state: ", GET_LUCKY_WHEEL_FLOW_STATE_STRING(eState))
	#ENDIF
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ QUERY BITS ╞══════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

PROC SET_LUCKY_WHEEL_MINIGAME_BIT(INT iBit)
	IF NOT IS_BIT_SET(luckyWheelData.iBS, iBit)
		SET_BIT(luckyWheelData.iBS, iBit)
		PRINTLN("[LUCKY_WHEEL] SET_LUCKY_WHEEL_MINIGAME_BIT ", GET_BIT_SET_NAME(iBit) , " TRUE")
	ENDIF
ENDPROC

FUNC BOOL IS_LUCKY_WHEEL_MINIGAME_BIT_SET(INT iBit)
	RETURN IS_BIT_SET(luckyWheelData.iBS, iBit)
ENDFUNC

PROC CLEAR_LUCKY_WHEEL_MINIGAME_BIT(INT iBit)
	IF IS_BIT_SET(luckyWheelData.iBS, iBit)
		CLEAR_BIT(luckyWheelData.iBS, iBit)
		PRINTLN("[LUCKY_WHEEL] CLEAR_LUCKY_WHEEL_MINIGAME_BIT ", GET_BIT_SET_NAME(iBit) , " FALSE")
	ENDIF
ENDPROC

PROC SET_PRIZE_SEGMENT(INT iIndex)
	IF PLAYER_ID() != INVALID_PLAYER_INDEX()
		IF m_PlayerBD[NATIVE_TO_INT(PLAYER_ID())].iPrizeSegment != iIndex
			m_PlayerBD[NATIVE_TO_INT(PLAYER_ID())].iPrizeSegment = iIndex
			PRINTLN("[LUCKY_WHEEL] SET_PRIZE_SEGMENT: ", iIndex)
		ENDIF	
	ENDIF
ENDPROC

FUNC INT GET_PLAYER_PRIZE_SEGMENT(PLAYER_INDEX playerToCheck)
	IF playerToCheck = INVALID_PLAYER_INDEX()
		PRINTLN("[LUCKY_WHEEL] GET_PLAYER_PRIZE_SEGMENT player invalid")
		RETURN 0
	ENDIF
	RETURN m_PlayerBD[NATIVE_TO_INT(playerToCheck)].iPrizeSegment
ENDFUNC

PROC SET_WHEEL_ANIM_ACTIVATION_TIME(INT iIndex)
	IF PLAYER_ID() != INVALID_PLAYER_INDEX()
		IF m_PlayerBD[NATIVE_TO_INT(PLAYER_ID())].iActivationTimeMS != iIndex
			m_PlayerBD[NATIVE_TO_INT(PLAYER_ID())].iActivationTimeMS = iIndex
			PRINTLN("[LUCKY_WHEEL] SET_WHEEL_ANIM_ACTIVATION_TIME: ", iIndex)
		ENDIF	
	ENDIF
ENDPROC

FUNC INT GET_WHEEL_ANIM_ACTIVATION_TIME(PLAYER_INDEX playerToCheck)
	IF playerToCheck = INVALID_PLAYER_INDEX()
		PRINTLN("[LUCKY_WHEEL] GET_WHEEL_ANIM_ACTIVATION_TIME player invalid")
		RETURN 0
	ENDIF
	RETURN m_PlayerBD[NATIVE_TO_INT(playerToCheck)].iActivationTimeMS
ENDFUNC

PROC SET_WHEEL_ANIM_SCENE_ID(INT iIndex)
	IF PLAYER_ID() != INVALID_PLAYER_INDEX()
		IF m_PlayerBD[NATIVE_TO_INT(PLAYER_ID())].iSceneID != iIndex
			m_PlayerBD[NATIVE_TO_INT(PLAYER_ID())].iSceneID = iIndex
			PRINTLN("[LUCKY_WHEEL] SET_WHEEL_ANIM_SCENE_ID: ", iIndex)
		ENDIF	
	ENDIF
ENDPROC

FUNC INT GET_WHEEL_ANIM_SCENE_ID(PLAYER_INDEX playerToCheck)
	IF playerToCheck = INVALID_PLAYER_INDEX()
		PRINTLN("[LUCKY_WHEEL] GET_WHEEL_ANIM_SCENE_ID player invalid")
		RETURN 0
	ENDIF
	RETURN m_PlayerBD[NATIVE_TO_INT(playerToCheck)].iSceneID
ENDFUNC

PROC SET_PLAYER_STARTED_SPIN(BOOL bStart)
	IF PLAYER_ID() != INVALID_PLAYER_INDEX()
		IF bStart
			IF NOT IS_BIT_SET(m_PlayerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, PLAYER_BROADCAST_DATA_START_SPIN)
				SET_BIT(m_PlayerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, PLAYER_BROADCAST_DATA_START_SPIN)
				PRINTLN("[LUCKY_WHEEL] - SET_PLAYER_STARTED_SPIN TRUE")
			ENDIF
		ELSE
			IF IS_BIT_SET(m_PlayerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, PLAYER_BROADCAST_DATA_START_SPIN)
				CLEAR_BIT(m_PlayerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, PLAYER_BROADCAST_DATA_START_SPIN)
				PRINTLN("[LUCKY_WHEEL] - SET_PLAYER_STARTED_SPIN FALSE")
			ENDIF
		ENDIF
	ENDIF
ENDPROC	

FUNC BOOL IS_PLAYER_STARTED_SPIN(PLAYER_INDEX playerToCheck)
	IF playerToCheck = INVALID_PLAYER_INDEX()
		RETURN FALSE
	ENDIF	
	RETURN IS_BIT_SET(m_PlayerBD[NATIVE_TO_INT(playerToCheck)].iBS, PLAYER_BROADCAST_DATA_START_SPIN)
ENDFUNC

PROC SET_SPIN_INTENSITY(LUCKY_WHEEL_SPIN_INTENSITY eSpinIntensity)
	IF PLAYER_ID() != INVALID_PLAYER_INDEX()
		m_PlayerBD[NATIVE_TO_INT(PLAYER_ID())].eSpinIntensity = eSpinIntensity
	ENDIF
ENDPROC	

FUNC LUCKY_WHEEL_SPIN_INTENSITY GET_SPIN_INTENSITY(PLAYER_INDEX playerToCheck)
	IF playerToCheck = INVALID_PLAYER_INDEX()
		RETURN LUCKY_WHEEL_SPIN_INTENSITY_INVALID
	ENDIF
	
	RETURN m_PlayerBD[NATIVE_TO_INT(playerToCheck)].eSpinIntensity
ENDFUNC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════╡ HOST FUNCTIONS ╞════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

/// PURPOSE: Stagger the update checks to the server BD  
FUNC BOOL UPDATE_LUCKY_WHEEL_MINIGAME_SERVER_BD()
	RETURN (GET_FRAME_COUNT() % 3) = 0
ENDFUNC

FUNC BOOL IS_LUCKY_WHEEL_MINIGAME_ACTIVE()
	RETURN luckyWheelServerData.minigamePlayer != INVALID_PLAYER_INDEX()
ENDFUNC

FUNC BOOL IS_PLAYER_REQUESTING_TO_PLAY_LUCKY_WHEEL_MINIGAME(PLAYER_INDEX playerID)
	IF playerID = INVALID_PLAYER_INDEX()
		RETURN FALSE
	ENDIF
	RETURN IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.sCasinoPropertyData.iBS2Casino, PROPERTY_BROADCAST_BS2_CASINO_LUCKY_WHEEL_REQUEST_TO_PLAY)
ENDFUNC

PROC SET_PLAYER_REQUESTING_TO_PLAY_LUCKY_WHEEL_MINIGAME(PLAYER_INDEX playerID, BOOL bRequestToPlay)
	IF playerID = INVALID_PLAYER_INDEX()
		EXIT
	ENDIF
	
	IF bRequestToPlay
		IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.sCasinoPropertyData.iBS2Casino, PROPERTY_BROADCAST_BS2_CASINO_LUCKY_WHEEL_REQUEST_TO_PLAY)
			SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.sCasinoPropertyData.iBS2Casino, PROPERTY_BROADCAST_BS2_CASINO_LUCKY_WHEEL_REQUEST_TO_PLAY)
			PRINTLN("[LUCKY_WHEEL] SET_PLAYER_REQUESTING_TO_PLAY_LUCKY_WHEEL_MINIGAME - TRUE")
		ENDIF
	ELSE
		IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.sCasinoPropertyData.iBS2Casino, PROPERTY_BROADCAST_BS2_CASINO_LUCKY_WHEEL_REQUEST_TO_PLAY)
			CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.sCasinoPropertyData.iBS2Casino, PROPERTY_BROADCAST_BS2_CASINO_LUCKY_WHEEL_REQUEST_TO_PLAY)
			PRINTLN("[LUCKY_WHEEL] SET_PLAYER_REQUESTING_TO_PLAY_LUCKY_WHEEL_MINIGAME - FALSE")
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL CLEAR_LUCKY_WHEEL_MINIGAME_PLAYER()
	
	IF luckyWheelServerData.minigamePlayer = INVALID_PLAYER_INDEX()
		PRINTLN("[LUCKY_WHEEL] CLEAR_LUCKY_WHEEL_MINIGAME_PLAYER - Minigame player has an invalid player ID")
		RETURN TRUE
	ENDIF
	
	IF NOT IS_PLAYER_REQUESTING_TO_PLAY_LUCKY_WHEEL_MINIGAME(luckyWheelServerData.minigamePlayer)
		PRINTLN("[LUCKY_WHEEL] CLEAR_LUCKY_WHEEL_MINIGAME_PLAYER - Minigame player is no longer playing")
		RETURN TRUE
	ENDIF
	
	IF NOT NETWORK_IS_PLAYER_A_PARTICIPANT(luckyWheelServerData.minigamePlayer)
		PRINTLN("[LUCKY_WHEEL]  - Minigame player is no longer a participant")
		RETURN TRUE
	ENDIF
	
	IF NOT IS_NET_PLAYER_OK(luckyWheelServerData.minigamePlayer)
		PRINTLN("[LUCKY_WHEEL] CLEAR_LUCKY_WHEEL_MINIGAME_PLAYER - Minigame player is not okay")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SET_LUCKY_WHEEL_MINIGAME_PLAYER_SERVER_BD()
	
	IF luckyWheelServerData.minigamePlayer = INVALID_PLAYER_INDEX()
		
		INT iPlayerID
		PLAYER_INDEX playerID
		
		REPEAT NUM_NETWORK_PLAYERS iPlayerID
			playerID = INT_TO_PLAYERINDEX(iPlayerID)
			IF IS_PLAYER_A_PARTICIPANT_OF_THIS_SCRIPT(playerID)
				IF IS_NET_PLAYER_OK(playerID)
					IF IS_PLAYER_REQUESTING_TO_PLAY_LUCKY_WHEEL_MINIGAME(playerID)
						luckyWheelServerData.minigamePlayer = playerID
						PRINTLN("[LUCKY_WHEEL] SET_LUCKY_WHEEL_MINIGAME_PLAYER_SERVER_BD - Setting minigame player - Player ID: ", iPlayerID)
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	
	ELSE
		IF CLEAR_LUCKY_WHEEL_MINIGAME_PLAYER()
			luckyWheelServerData.minigamePlayer = INVALID_PLAYER_INDEX()
			PRINTLN("[LUCKY_WHEEL] SET_LUCKY_WHEEL_MINIGAME_PLAYER_SERVER_BD - Clearing minigame player")
		ENDIF
	ENDIF
	
ENDPROC

PROC SET_LUCKY_WHEEL_MINIGAME_SERVER_BD()
	IF UPDATE_LUCKY_WHEEL_MINIGAME_SERVER_BD()
		SET_LUCKY_WHEEL_MINIGAME_PLAYER_SERVER_BD()
	ENDIF
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════════╡ PRIZES ╞════════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛
FUNC INT GET_WHEEL_PRIZE_INDEX_FROM_LUCKY_WHEEL_REWARD_TYPE(LUCK_REWARD_TYPE eLuckyRewardType)
	SWITCH eLuckyRewardType
		CASE LR_CLOTHES_1		RETURN 	0
		CASE LR_CLOTHES_2		RETURN 	8
		CASE LR_CLOTHES_3		RETURN 	12
		CASE LR_CLOTHES_4		RETURN 	16
		CASE LR_RP2500			RETURN 	1
		CASE LR_RP5000			RETURN  5
		CASE LR_RP7500			RETURN  9
		CASE LR_RP10K			RETURN  13
		CASE LR_RP15K			RETURN  17
		CASE LR_CASH20K			RETURN 	2
		CASE LR_CASH30K			RETURN 	6
		CASE LR_CASH40K			RETURN 	14
		CASE LR_CASH50K			RETURN 	19
		CASE LR_CHIPS10K		RETURN 	3
		CASE LR_CHIPS15K		RETURN 	7
		CASE LR_CHIPS20K		RETURN 	10
		CASE LR_CHIPS25K		RETURN 	15
		CASE LR_MYSTERY			RETURN 	11
		CASE LR_VEHICLE			RETURN 	18
		CASE LR_DISCOUNT		RETURN  4
	ENDSWITCH
	
	SCRIPT_ASSERT("[LUCKY_WHEEL] GET_WHEEL_PRIZE_INDEX_FROM_LUCKY_WHEEL_REWARD_TYPE RETURN INVALID PRIZE")
	RETURN 0
ENDFUNC 

FUNC LUCK_REWARD_TYPE GET_LUCKY_REWARD_TYPE_FROM_SEGMENT_INDEX(INT iSegment)
	SWITCH iSegment
		CASE 0 					RETURN LR_CLOTHES_1		
		CASE 8 					RETURN LR_CLOTHES_2		
		CASE 12					RETURN LR_CLOTHES_3		
		CASE 16					RETURN LR_CLOTHES_4		
		CASE 1					RETURN LR_RP2500			
		CASE 5					RETURN LR_RP5000			
		CASE 9					RETURN LR_RP7500			
		CASE 13 				RETURN LR_RP10K			
		CASE 17 				RETURN LR_RP15K			
		CASE 2					RETURN LR_CASH20K		
		CASE 6					RETURN LR_CASH30K		
		CASE 14					RETURN LR_CASH40K		
		CASE 19					RETURN LR_CASH50K		
		CASE 3					RETURN LR_CHIPS10K		
		CASE 7					RETURN LR_CHIPS15K		
		CASE 10					RETURN LR_CHIPS20K		
		CASE 15					RETURN LR_CHIPS25K		
		CASE 11					RETURN LR_MYSTERY		
		CASE 18					RETURN LR_VEHICLE		
		CASE 4 					RETURN LR_DISCOUNT		
	ENDSWITCH
	
	SCRIPT_ASSERT("[LUCKY_WHEEL] GET_LUCKY_REWARD_TYPE_FROM_SEGMENT_INDEX RETURN LR_INVALID")
	RETURN LR_INVALID
ENDFUNC 

PROC SET_LUCKY_WHEEL_MINIGAME_PRIZE()
	IF NOT IS_LUCKY_WHEEL_MINIGAME_BIT_SET(BS_LUCKY_WHEEL_SET_PRIZE)
		SET_PRIZE_SEGMENT(GET_WHEEL_PRIZE_INDEX_FROM_LUCKY_WHEEL_REWARD_TYPE(PICK_RANDOM_LUCK_REWARD_TYPE()))
		#IF IS_DEBUG_BUILD
		IF luckyWheelDebugData.bEnableDebugPrize
			SET_PRIZE_SEGMENT(luckyWheelDebugData.iDebugPrize)
		ENDIF
		#ENDIF
		
		SET_LUCKY_WHEEL_MINIGAME_BIT(BS_LUCKY_WHEEL_SET_PRIZE)
	ENDIF
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ INITIALISE ╞══════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

FUNC VECTOR GET_LUCKY_WHEEL_MINIGAME_WHEEL_COORDS()
	RETURN <<1111.0519, 229.8579, -49.1330>>
ENDFUNC

FUNC FLOAT GET_LUCKY_WHEEL_MINIGAME_WHEEL_HEADING()
	RETURN 0.0
ENDFUNC

FUNC MODEL_NAMES GET_LUCKY_WHEEL_MINIGAME_MODEL()
	RETURN INT_TO_ENUM(MODEL_NAMES, HASH("vw_Prop_VW_LuckyWheel_02a"))
ENDFUNC

FUNC STRING GET_LUCKY_WHEEL_MINIGAME_ANIMATION_DICTIONARY()
	STRING sAnimDict
	IF IS_PED_FEMALE(PLAYER_PED_ID())
		sAnimDict = "ANIM_CASINO_A@AMB@CASINO@GAMES@LUCKY7WHEEL@FEMALE"
	ELSE
		sAnimDict = "ANIM_CASINO_A@AMB@CASINO@GAMES@LUCKY7WHEEL@MALE"
	ENDIF
	RETURN sAnimDict
ENDFUNC

FUNC STRING GET_LUCKY_WHEEL_MINIGAME_ANIMATION_CLIP(LUCKY_WHEEL_ANIM_CLIPS eClip)
	STRING sAnimClip 
	SWITCH eClip
		CASE LUCKY_WHEEL_ANIM_CLIP_ENTRY					sAnimClip = "Enter_to_ArmRaisedIDLE"				BREAK
		CASE LUCKY_WHEEL_ANIM_CLIP_IDLE_1					sAnimClip = "ArmRaisedIDLE"							BREAK
		CASE LUCKY_WHEEL_ANIM_CLIP_RAISE_ARM				sAnimClip = "ArmRaisedIDLE_to_SpinReadyIDLE"		BREAK
		CASE LUCKY_WHEEL_ANIM_CLIP_RAISE_ARM_IDLE			sAnimClip = "SpinReadyIDLE"							BREAK
		CASE LUCKY_WHEEL_ANIM_CLIP_TIMED_OUT				sAnimClip = "SpinStart_ArmRaisedIDLE_to_BaseIDLE"	BREAK
		CASE LUCKY_WHEEL_ANIM_CLIP_SPIN_LOW					
			IF IS_LUCKY_WHEEL_MINIGAME_BIT_SET(BS_LUCKY_WHEEL_START_FROM_ARM_RAISED_ANIM)
				sAnimClip = "spinreadyidle_to_spinningidle_low"		
			ELSE
				sAnimClip = "ArmRaisedIDLE_to_SpinningIDLE_Low"	
			ENDIF	
		BREAK
		CASE LUCKY_WHEEL_ANIM_CLIP_SPIN_MEDIUM				
			IF IS_LUCKY_WHEEL_MINIGAME_BIT_SET(BS_LUCKY_WHEEL_START_FROM_ARM_RAISED_ANIM)
				sAnimClip = "spinreadyidle_to_spinningidle_med"		
			ELSE
				sAnimClip = "ArmRaisedIDLE_to_SpinningIDLE_Med"	
			ENDIF
		BREAK
		CASE LUCKY_WHEEL_ANIM_CLIP_SPIN_HIGH				
			IF IS_LUCKY_WHEEL_MINIGAME_BIT_SET(BS_LUCKY_WHEEL_START_FROM_ARM_RAISED_ANIM)
				sAnimClip = "spinreadyidle_to_spinningidle_high"	
			ELSE
				sAnimClip = "ArmRaisedIDLE_to_SpinningIDLE_High"	
			ENDIF
		BREAK
		CASE LUCKY_WHEEL_ANIM_CLIP_SPINNING_IDLE_LOW		sAnimClip = "SpinningIDLE_Low"						BREAK
		CASE LUCKY_WHEEL_ANIM_CLIP_SPINNING_IDLE_MEDIUM		sAnimClip = "SpinningIDLE_Medium"					BREAK
		CASE LUCKY_WHEEL_ANIM_CLIP_SPINNING_IDLE_HIGH		sAnimClip = "SpinningIDLE_High"						BREAK
		CASE LUCKY_WHEEL_ANIM_CLIP_WIN						sAnimClip = "Win"									BREAK
		CASE LUCKY_WHEEL_ANIM_CLIP_WIN_BIG					sAnimClip = "Win_Big"								BREAK
		CASE LUCKY_WHEEL_ANIM_CLIP_WIN_HUGE					sAnimClip = "Win_Huge"								BREAK
		CASE LUCKY_WHEEL_ANIM_CLIP_EXIT						sAnimClip = "Exit_to_Standing"						BREAK
		CASE LUCKY_WHEEL_ANIM_CLIP_ARM_RAISED_TO_IDLE		sAnimClip = "SpinReadyIDLE_to_ArmRaisedIDLE"		BREAK
	ENDSWITCH

	RETURN sAnimClip
ENDFUNC

FUNC LUCKY_WHEEL_ANIM_CLIPS GET_LUCKY_WHEEL_MINIGAME_ANIMATION_CLIP_ENUM(STRING sAnimClip)
	LUCKY_WHEEL_ANIM_CLIPS eClip = LUCKY_WHEEL_ANIM_CLIP_INVALID
	IF ARE_STRINGS_EQUAL(sAnimClip, "Enter_to_ArmRaisedIDLE")	
		eClip = LUCKY_WHEEL_ANIM_CLIP_ENTRY
	ELIF ARE_STRINGS_EQUAL(sAnimClip, "ArmRaisedIDLE")
		eClip = LUCKY_WHEEL_ANIM_CLIP_IDLE_1
	ELIF ARE_STRINGS_EQUAL(sAnimClip, "ArmRaisedIDLE_to_SpinReadyIDLE")
		eClip = LUCKY_WHEEL_ANIM_CLIP_RAISE_ARM
	ELIF ARE_STRINGS_EQUAL(sAnimClip, "SpinReadyIDLE")
		eClip = LUCKY_WHEEL_ANIM_CLIP_RAISE_ARM_IDLE
	ELIF ARE_STRINGS_EQUAL(sAnimClip, "SpinStart_ArmRaisedIDLE_to_BaseIDLE")
		eClip = LUCKY_WHEEL_ANIM_CLIP_TIMED_OUT
	ELIF ARE_STRINGS_EQUAL(sAnimClip, "spinreadyidle_to_spinningidle_low")
	OR ARE_STRINGS_EQUAL(sAnimClip, "ArmRaisedIDLE_to_SpinningIDLE_Low")
		eClip = LUCKY_WHEEL_ANIM_CLIP_SPIN_LOW
	ELIF ARE_STRINGS_EQUAL(sAnimClip, "spinreadyidle_to_spinningidle_med")
	OR ARE_STRINGS_EQUAL(sAnimClip, "ArmRaisedIDLE_to_SpinningIDLE_Med")
		eClip = LUCKY_WHEEL_ANIM_CLIP_SPIN_MEDIUM
	ELIF ARE_STRINGS_EQUAL(sAnimClip, "spinreadyidle_to_spinningidle_high")
	OR ARE_STRINGS_EQUAL(sAnimClip, "ArmRaisedIDLE_to_SpinningIDLE_High")
		eClip = LUCKY_WHEEL_ANIM_CLIP_SPIN_HIGH
	ELIF ARE_STRINGS_EQUAL(sAnimClip, "SpinningIDLE_Low")
		eClip = LUCKY_WHEEL_ANIM_CLIP_SPINNING_IDLE_LOW
	ELIF ARE_STRINGS_EQUAL(sAnimClip, "SpinningIDLE_Med")
		eClip = LUCKY_WHEEL_ANIM_CLIP_SPINNING_IDLE_MEDIUM
	ELIF ARE_STRINGS_EQUAL(sAnimClip, "SpinningIDLE_High")
		eClip = LUCKY_WHEEL_ANIM_CLIP_SPINNING_IDLE_HIGH
	ELIF ARE_STRINGS_EQUAL(sAnimClip, "Win")
		eClip = LUCKY_WHEEL_ANIM_CLIP_WIN
	ELIF ARE_STRINGS_EQUAL(sAnimClip, "Win_Big")
		eClip = LUCKY_WHEEL_ANIM_CLIP_WIN_BIG
	ELIF ARE_STRINGS_EQUAL(sAnimClip, "Win_Huge")
		eClip = LUCKY_WHEEL_ANIM_CLIP_WIN_HUGE	
	ELIF ARE_STRINGS_EQUAL(sAnimClip, "Exit_to_Standing")
		eClip = LUCKY_WHEEL_ANIM_CLIP_EXIT
	ELIF ARE_STRINGS_EQUAL(sAnimClip, "SpinReadyIDLE_to_ArmRaisedIDLE")
		eClip = LUCKY_WHEEL_ANIM_CLIP_ARM_RAISED_TO_IDLE
	ENDIF
	RETURN eClip
ENDFUNC

FUNC LUCKY_WHEEL_ANIM_CLIPS GET_LUCKY_WHEEL_MINIGAME_SPIN_ANIMATION_ENUM(LUCKY_WHEEL_SPIN_INTENSITY eSpinIntensity)
	SWITCH eSpinIntensity
		CASE LUCKY_WHEEL_SPIN_INTENSITY_LOW			RETURN LUCKY_WHEEL_ANIM_CLIP_SPIN_LOW
		CASE LUCKY_WHEEL_SPIN_INTENSITY_MEDIUM		RETURN LUCKY_WHEEL_ANIM_CLIP_SPIN_MEDIUM
		CASE LUCKY_WHEEL_SPIN_INTENSITY_HIGH		RETURN LUCKY_WHEEL_ANIM_CLIP_SPIN_HIGH
	ENDSWITCH
	RETURN LUCKY_WHEEL_ANIM_CLIP_INVALID
ENDFUNC

FUNC LUCKY_WHEEL_ANIM_CLIPS GET_LUCKY_WHEEL_MINIGAME_SPIN_IDLE_ANIMATION_ENUM(LUCKY_WHEEL_SPIN_INTENSITY eSpinIntensity)
	SWITCH eSpinIntensity
		CASE LUCKY_WHEEL_SPIN_INTENSITY_LOW			RETURN LUCKY_WHEEL_ANIM_CLIP_SPINNING_IDLE_LOW
		CASE LUCKY_WHEEL_SPIN_INTENSITY_MEDIUM		RETURN LUCKY_WHEEL_ANIM_CLIP_SPINNING_IDLE_MEDIUM
		CASE LUCKY_WHEEL_SPIN_INTENSITY_HIGH		RETURN LUCKY_WHEEL_ANIM_CLIP_SPINNING_IDLE_HIGH
	ENDSWITCH
	RETURN LUCKY_WHEEL_ANIM_CLIP_INVALID
ENDFUNC

FUNC LUCKY_WHEEL_ANIM_CLIPS GET_LUCKY_WHEEL_MINIGAME_WIN_ANIMATION_ENUM()
	LUCKY_WHEEL_ANIM_CLIPS eClip = LUCKY_WHEEL_ANIM_CLIP_WIN_BIG
	LUCKY_WHEEL_PRIZES ePrize = INT_TO_ENUM(LUCKY_WHEEL_PRIZES, GET_PLAYER_PRIZE_SEGMENT(PLAYER_ID()))
	SWITCH ePrize
		CASE LUCKY_WHEEL_PRIZE_HOUSE_CHIPS_20000
		CASE LUCKY_WHEEL_PRIZE_HOUSE_CHIPS_25000
		CASE LUCKY_WHEEL_PRIZE_CASH_40000
		CASE LUCKY_WHEEL_PRIZE_CASH_50000
			eClip = LUCKY_WHEEL_ANIM_CLIP_WIN_BIG
		BREAK
		CASE LUCKY_WHEEL_PRIZE_PODIUM_VEHICLE
			eClip = LUCKY_WHEEL_ANIM_CLIP_WIN_HUGE
		BREAK
		DEFAULT 
			eClip = LUCKY_WHEEL_ANIM_CLIP_WIN
		BREAK
	ENDSWITCH
	RETURN eClip
ENDFUNC

FUNC LUCKY_WHEEL_ANIM_CLIPS GET_LUCKY_WHEEL_MINIGAME_IDLE_ANIM_CLIP()
	RETURN LUCKY_WHEEL_ANIM_CLIP_IDLE_1
ENDFUNC

PROC SET_LUCKY_WHEEL_MINIGAME_PLAYER_CONTROL(PLAYER_INDEX PlayerID, bool bHasControl, NET_SET_PLAYER_CONTROL_FLAGS eFlags=0)
	IF IS_ENTITY_ALIVE(GET_PLAYER_PED(PlayerID))
		NET_SET_PLAYER_CONTROL(PlayerID, bHasControl, eFlags)
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_UseKinematicModeWhenStationary, !bHasControl)
	ENDIF
ENDPROC

PROC SET_LUCKY_WHEEL_MINIGAME_PROPERTIES()
	DISABLE_CELLPHONE(TRUE)
ENDPROC

PROC CLEAR_LUCKY_WHEEL_MINIGAME_PROPERTIES()
	DISABLE_CELLPHONE(FALSE)
ENDPROC

PROC RESET_LUCKY_WHEEL_MINIGAME_DATA()
	luckyWheelData.iBS 							= 0
	SET_WHEEL_ANIM_ACTIVATION_TIME(ciLUCKY_WHEEL_ACTIVATION_TIME_MS)
	luckyWheelData.iActivationTimerDifference	= 0
	luckyWheelData.iLeftAnalogueStickX 			= 0
	luckyWheelData.iLeftAnalogueStickY 			= 0
	luckyWheelData.iRightAnalogueStickX 			= 0
	luckyWheelData.iRightAnalogueStickY 			= 0
	//luckyWheelData.iStartingSegment				= 0
	luckyWheelData.iFullSpins					= 0							
	luckyWheelData.iSpinsCompleted				= 0
	SET_WHEEL_ANIM_SCENE_ID(-1)
	luckyWheelData.iWheelSceneID				= -1
	luckyWheelData.iNumSpinSteps				= 0
	luckyWheelData.iCurrSpinStep				= 0
	luckyWheelData.iSegmentTimeMS				= 0
	luckyWheelData.fCurrentAngle				= 0.0
	luckyWheelData.fStartingAngle				= 0.0
	luckyWheelData.iDuration					= 0
	luckyWheelData.fEndingAngle 				= 0.0
	luckyWheelData.fUncappedCurrentAngle 		= 0.0
	SET_SPIN_INTENSITY(LUCKY_WHEEL_SPIN_INTENSITY_INVALID)	
	
	luckyWheelData.eIdleClip 					= LUCKY_WHEEL_ANIM_CLIP_IDLE_1
	
	SET_PRIZE_SEGMENT(-1)
	
	#IF IS_DEBUG_BUILD
	luckyWheelDebugData.iIdleTimerDifference	= 0
	#ENDIF
	
	RESET_NET_TIMER(luckyWheelData.stIdleSafetyTimer)
	RESET_NET_TIMER(luckyWheelData.stUpdateServerBDTimer)
	RESET_NET_TIMER(luckyWheelData.stGoToEntryCoordsTaskSafetyTimer)
	RESET_NET_TIMER(luckyWheelData.stControlStickUpTimer)
ENDPROC

PROC INITIALISE_LUCKY_WHEEL_MINIGAME()
	RESET_LUCKY_WHEEL_MINIGAME_DATA()
	SET_LUCKY_WHEEL_MINIGAME_PRIZE()
	SET_LUCKY_WHEEL_MINIGAME_PROPERTIES()
	SET_TRANSITION_SESSIONS_CLEAN_UP_ON_CALL_NOW_IF_NEEDED() 
	SET_LUCKY_WHEEL_MINIGAME_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_LEAVE_CAMERA_CONTROL_ON)
	luckyWheelData.sAnimDict = GET_LUCKY_WHEEL_MINIGAME_ANIMATION_DICTIONARY()
	luckyWheelData.sAnimClip = GET_LUCKY_WHEEL_MINIGAME_ANIMATION_CLIP(LUCKY_WHEEL_ANIM_CLIP_ENTRY)
	luckyWheelData.eIdleClip = GET_LUCKY_WHEEL_MINIGAME_IDLE_ANIM_CLIP()
ENDPROC

FUNC FLOAT GET_LUCKY_WHEEL_STARTING_ROTATION_Y_AXIS()
	FLOAT fYAxis = TO_FLOAT(ciLUCKY_WHEEL_SEGMENT_ANGLE*luckyWheelData.iCurrentSegmentIndex)
	RETURN -fYAxis
ENDFUNC

PROC MAINTAIN_LUCKY_WHEEL_PROP()
	IF !DOES_ENTITY_EXIST(luckyWheelData.objLuckyWheel)
		REQUEST_MODEL(GET_LUCKY_WHEEL_MINIGAME_MODEL())
		IF NOT HAS_MODEL_LOADED(GET_LUCKY_WHEEL_MINIGAME_MODEL())
			EXIT
		ENDIF

		luckyWheelData.objLuckyWheel = CREATE_OBJECT_NO_OFFSET(GET_LUCKY_WHEEL_MINIGAME_MODEL(), GET_LUCKY_WHEEL_MINIGAME_WHEEL_COORDS(), FALSE, FALSE, TRUE)
		SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(luckyWheelData.objLuckyWheel, TRUE)
		SET_ENTITY_CAN_BE_DAMAGED(luckyWheelData.objLuckyWheel, FALSE)
		PRINTLN("[LUCKY_WHEEL] MAINTAIN_LUCKY_WHEEL_PROP - created wheel")
	ENDIF	
ENDPROC

PROC SET_PLAYER_IS_USING_THE_LUCKY_WHEEL(PLAYER_INDEX playerID, BOOL bUsingWheel)
	IF playerID = INVALID_PLAYER_INDEX()
		EXIT
	ENDIF
	
	IF bUsingWheel
		IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(playerID)].SimpleInteriorBD.iBSFive, BS_SIMPLE_INTERIOR_GLOBAL_BS_FIVE_PLAYING_LUCKY_WHEEL)
			SET_BIT(GlobalplayerBD[NATIVE_TO_INT(playerID)].SimpleInteriorBD.iBSFive, BS_SIMPLE_INTERIOR_GLOBAL_BS_FIVE_PLAYING_LUCKY_WHEEL)
			PRINTLN("[LUCKY_WHEEL] SET_PLAYER_IS_USING_THE_LUCKY_WHEEL - TRUE - Setting global bit: BS_SIMPLE_INTERIOR_GLOBAL_BS_FIVE_PLAYING_LUCKY_WHEEL")
		ENDIF
	ELSE
		IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(playerID)].SimpleInteriorBD.iBSFive, BS_SIMPLE_INTERIOR_GLOBAL_BS_FIVE_PLAYING_LUCKY_WHEEL)
			CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(playerID)].SimpleInteriorBD.iBSFive, BS_SIMPLE_INTERIOR_GLOBAL_BS_FIVE_PLAYING_LUCKY_WHEEL)
			PRINTLN("[LUCKY_WHEEL] SET_PLAYER_IS_USING_THE_LUCKY_WHEEL - FALSE - Clearing global bit: BS_SIMPLE_INTERIOR_GLOBAL_BS_FIVE_PLAYING_LUCKY_WHEEL")
		ENDIF
	ENDIF
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ METRIC DATA ╞═════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

FUNC LUCKY_WHEEL_PRIZE_TYPE GET_LUCKY_WHEEL_PRIZE_TYPE()
	LUCKY_WHEEL_PRIZES ePrize = INT_TO_ENUM(LUCKY_WHEEL_PRIZES, GET_PLAYER_PRIZE_SEGMENT(PLAYER_ID()))
	SWITCH ePrize
		CASE LUCKY_WHEEL_PRIZE_RP_2500				RETURN LUCKY_WHEEL_PRIZE_TYPE_RP
		CASE LUCKY_WHEEL_PRIZE_CASH_20000			RETURN LUCKY_WHEEL_PRIZE_TYPE_CASH
		CASE LUCKY_WHEEL_PRIZE_HOUSE_CHIPS_10000	RETURN LUCKY_WHEEL_PRIZE_TYPE_CHIPS
		CASE LUCKY_WHEEL_PRIZE_CLOTHING_1			RETURN LUCKY_WHEEL_PRIZE_TYPE_CLOTHING
		CASE LUCKY_WHEEL_PRIZE_RP_5000				RETURN LUCKY_WHEEL_PRIZE_TYPE_RP
		CASE LUCKY_WHEEL_PRIZE_HOUSE_CHIPS_15000	RETURN LUCKY_WHEEL_PRIZE_TYPE_CHIPS
		CASE LUCKY_WHEEL_PRIZE_MYSTERY_STAR			RETURN LUCKY_WHEEL_PRIZE_TYPE_MYSTERY_STAR
		CASE LUCKY_WHEEL_PRIZE_CLOTHING_2			RETURN LUCKY_WHEEL_PRIZE_TYPE_CLOTHING
		CASE LUCKY_WHEEL_PRIZE_RP_7500				RETURN LUCKY_WHEEL_PRIZE_TYPE_RP
		CASE LUCKY_WHEEL_PRIZE_CASH_30000			RETURN LUCKY_WHEEL_PRIZE_TYPE_CASH
		CASE LUCKY_WHEEL_PRIZE_HOUSE_CHIPS_20000	RETURN LUCKY_WHEEL_PRIZE_TYPE_CHIPS
		CASE LUCKY_WHEEL_PRIZE_CLOTHING_3			RETURN LUCKY_WHEEL_PRIZE_TYPE_CLOTHING
		CASE LUCKY_WHEEL_PRIZE_RP_10000				RETURN LUCKY_WHEEL_PRIZE_TYPE_RP
		CASE LUCKY_WHEEL_PRIZE_PODIUM_VEHICLE		RETURN LUCKY_WHEEL_PRIZE_TYPE_PODIUM_VEHICLE
		CASE LUCKY_WHEEL_PRIZE_CASH_40000			RETURN LUCKY_WHEEL_PRIZE_TYPE_CASH
		CASE LUCKY_WHEEL_PRIZE_CLOTHING_4			RETURN LUCKY_WHEEL_PRIZE_TYPE_CLOTHING
		CASE LUCKY_WHEEL_PRIZE_RP_15000				RETURN LUCKY_WHEEL_PRIZE_TYPE_RP
		CASE LUCKY_WHEEL_PRIZE_CASH_50000			RETURN LUCKY_WHEEL_PRIZE_TYPE_CASH
		CASE LUCKY_WHEEL_PRIZE_HOUSE_CHIPS_25000	RETURN LUCKY_WHEEL_PRIZE_TYPE_CHIPS
		CASE LUCKY_WHEEL_PRIZE_DISCOUNT_VOUCHER		RETURN LUCKY_WHEEL_PRIZE_TYPE_DISCOUNT_VOUCHER
	ENDSWITCH
	RETURN LUCKY_WHEEL_PRIZE_TYPE_INVALID
ENDFUNC

FUNC STRING GET_LUCKY_WHEEL_PRIZE_TYPE_STRING(LUCKY_WHEEL_PRIZE_TYPE ePrizeType)
	IF ePrizeType = LUCKY_WHEEL_PRIZE_TYPE_MYSTERY_STAR
		SWITCH luckyWheelData.eMystreyRewardType
			CASE LRM_CLOTHES 								RETURN "MYSTREYCLOTHING"
			CASE LRM_VEHICLE								RETURN "MYSTREYVEHICLE"
			CASE LRM_CHIPS									RETURN "MYSTREYCHIPS"
			CASE LRM_CASH									RETURN "MYSTREYCASH"
			CASE LRM_RP										RETURN "MYSTREYRP"
			CASE LRM_INVENTORY								RETURN "MYSTREYINVENTORY"
			CASE LRM_BUSINESS								RETURN "MYSTREYBUSINESS"
		ENDSWITCH			
	ELSE
		SWITCH ePrizeType
			CASE LUCKY_WHEEL_PRIZE_TYPE_CLOTHING			RETURN "CLOTHING"
			CASE LUCKY_WHEEL_PRIZE_TYPE_RP					RETURN "RP"
			CASE LUCKY_WHEEL_PRIZE_TYPE_CASH				RETURN "CASH"
			CASE LUCKY_WHEEL_PRIZE_TYPE_CHIPS				RETURN "HOUSECHIPS"
			CASE LUCKY_WHEEL_PRIZE_TYPE_DISCOUNT_VOUCHER	RETURN "DISCOUNTVOUCHER"
			CASE LUCKY_WHEEL_PRIZE_TYPE_PODIUM_VEHICLE		RETURN "PODIUMVEHICLE"
		ENDSWITCH
	ENDIF	
	RETURN ""
ENDFUNC

FUNC INT GET_LUCKY_WHEEL_PRIZE_AMOUNT()
	LUCKY_WHEEL_PRIZES ePrize = INT_TO_ENUM(LUCKY_WHEEL_PRIZES, GET_PLAYER_PRIZE_SEGMENT(PLAYER_ID()))
	SWITCH ePrize
		CASE LUCKY_WHEEL_PRIZE_RP_2500				RETURN 2500
		CASE LUCKY_WHEEL_PRIZE_CASH_20000			RETURN 20000
		CASE LUCKY_WHEEL_PRIZE_HOUSE_CHIPS_10000	RETURN 10000
		CASE LUCKY_WHEEL_PRIZE_RP_5000				RETURN 5000
		CASE LUCKY_WHEEL_PRIZE_HOUSE_CHIPS_15000	RETURN 15000
		CASE LUCKY_WHEEL_PRIZE_RP_7500				RETURN 7500
		CASE LUCKY_WHEEL_PRIZE_CASH_30000			RETURN 30000
		CASE LUCKY_WHEEL_PRIZE_HOUSE_CHIPS_20000	RETURN 20000
		CASE LUCKY_WHEEL_PRIZE_RP_10000				RETURN 10000
		CASE LUCKY_WHEEL_PRIZE_CASH_40000			RETURN 40000
		CASE LUCKY_WHEEL_PRIZE_RP_15000				RETURN 15000
		CASE LUCKY_WHEEL_PRIZE_CASH_50000			RETURN 50000
		CASE LUCKY_WHEEL_PRIZE_HOUSE_CHIPS_25000	RETURN 25000
		BREAK
		CASE LUCKY_WHEEL_PRIZE_MYSTERY_STAR
			SWITCH luckyWheelData.eMystreyRewardType
				CASE LRM_CHIPS					
				CASE LRM_RP				
				CASE LRM_CASH				
					RETURN luckyWheelData.iPrizeNumber
				BREAK
			ENDSWITCH
		BREAK	
	ENDSWITCH
	RETURN 0
ENDFUNC

FUNC STRING GET_LUCKY_WHEEL_PRIZE_METRIC_REWARD_ITEM()
	LUCKY_WHEEL_PRIZES ePrize = INT_TO_ENUM(LUCKY_WHEEL_PRIZES, GET_PLAYER_PRIZE_SEGMENT(PLAYER_ID()))
	SWITCH ePrize
		CASE LUCKY_WHEEL_PRIZE_CLOTHING_1			
		CASE LUCKY_WHEEL_PRIZE_CLOTHING_2			
		CASE LUCKY_WHEEL_PRIZE_CLOTHING_3			
		CASE LUCKY_WHEEL_PRIZE_CLOTHING_4			
		CASE LUCKY_WHEEL_PRIZE_PODIUM_VEHICLE
			RETURN Get_String_From_TextLabel(luckyWheelData.sPrizeName)
		BREAK
		CASE LUCKY_WHEEL_PRIZE_MYSTERY_STAR
			SWITCH luckyWheelData.eMystreyRewardType
				CASE LRM_CLOTHES					
				CASE LRM_BUSINESS				
				CASE LRM_VEHICLE
				CASE LRM_INVENTORY
					RETURN Get_String_From_TextLabel(luckyWheelData.sPrizeName)
				BREAK
			ENDSWITCH
		BREAK	
	ENDSWITCH
	RETURN ""
ENDFUNC

FUNC BOOL SHOULD_IGNORE_SENDING_METRIC_DATA()
	IF ARE_STRINGS_EQUAL(luckyWheelData.sPrizeName, "INVALID")
		IF INT_TO_ENUM(LUCKY_WHEEL_PRIZES, GET_PLAYER_PRIZE_SEGMENT(PLAYER_ID())) = LUCKY_WHEEL_PRIZE_MYSTERY_STAR 
		AND luckyWheelData.eMystreyRewardType = LRM_VEHICLE
			PRINTLN("[LUCKY_WHEEL] SEND_LUCKY_WHEEL_METRIC_DATA player declined mystrey vehicle prize")
			RETURN TRUE
		ENDIF
		
		IF INT_TO_ENUM(LUCKY_WHEEL_PRIZES, GET_PLAYER_PRIZE_SEGMENT(PLAYER_ID())) = LUCKY_WHEEL_PRIZE_PODIUM_VEHICLE
			PRINTLN("[LUCKY_WHEEL] SEND_LUCKY_WHEEL_METRIC_DATA player declined podium vehicle prize")
			RETURN TRUE
		ENDIF	
	ENDIF
	
	RETURN FALSE
ENDFUNC 

PROC SEND_LUCKY_WHEEL_METRIC_DATA()
	
	IF NOT g_sMPTunables.bENABLE_LUCKY_SEVEN_HEAVY
		EXIT
	ENDIF
	
	IF SHOULD_IGNORE_SENDING_METRIC_DATA()
		EXIT
	ENDIF
	
	// Generic casino data
	luckyWheelData.sMetricData.m_casinoMetric.m_gameType = GET_HASH_KEY("LUCKYWHEEL")
	luckyWheelData.sMetricData.m_casinoMetric.m_matchType = GET_HASH_KEY("STANDARD")
	luckyWheelData.sMetricData.m_casinoMetric.m_endReason = GET_HASH_KEY("WIN")
	luckyWheelData.sMetricData.m_casinoMetric.m_ownPenthouse = DOES_PLAYER_OWN_A_CASINO_APARTMENT(PLAYER_ID())
	luckyWheelData.sMetricData.m_casinoMetric.m_isHost = NETWORK_IS_HOST_OF_THIS_SCRIPT()
	IF NETWORK_GET_HOST_OF_THIS_SCRIPT() != INVALID_PARTICIPANT_INDEX()
		luckyWheelData.sMetricData.m_casinoMetric.m_hostID = NETWORK_GET_PLAYER_ACCOUNT_ID(NETWORK_GET_PLAYER_INDEX(NETWORK_GET_HOST_OF_THIS_SCRIPT()))
	ENDIF
	
	// Lucky wheel prize type
	LUCKY_WHEEL_PRIZE_TYPE ePrizeType = GET_LUCKY_WHEEL_PRIZE_TYPE()
	STRING sPrizeType = GET_LUCKY_WHEEL_PRIZE_TYPE_STRING(ePrizeType)
	luckyWheelData.sMetricData.m_rewardType = GET_HASH_KEY(sPrizeType)
	
	IF IS_STRING_NULL_OR_EMPTY(GET_LUCKY_WHEEL_PRIZE_METRIC_REWARD_ITEM())
		luckyWheelData.sMetricData.m_rewardItem = 0
	ELSE
		luckyWheelData.sMetricData.m_rewardItem = GET_HASH_KEY(GET_LUCKY_WHEEL_PRIZE_METRIC_REWARD_ITEM())
	ENDIF
	
	SWITCH INT_TO_ENUM(LUCKY_WHEEL_PRIZES, GET_PLAYER_PRIZE_SEGMENT(PLAYER_ID()))
		CASE LUCKY_WHEEL_PRIZE_HOUSE_CHIPS_10000	luckyWheelData.sMetricData.m_casinoMetric.m_chipsDelta = 10000		BREAK
		CASE LUCKY_WHEEL_PRIZE_HOUSE_CHIPS_15000	luckyWheelData.sMetricData.m_casinoMetric.m_chipsDelta = 15000		BREAK
		CASE LUCKY_WHEEL_PRIZE_HOUSE_CHIPS_20000	luckyWheelData.sMetricData.m_casinoMetric.m_chipsDelta = 20000		BREAK
		CASE LUCKY_WHEEL_PRIZE_HOUSE_CHIPS_25000	luckyWheelData.sMetricData.m_casinoMetric.m_chipsDelta = 25000		BREAK
		CASE LUCKY_WHEEL_PRIZE_MYSTERY_STAR
			SWITCH luckyWheelData.eMystreyRewardType
				CASE LRM_CHIPS								
					luckyWheelData.sMetricData.m_casinoMetric.m_chipsDelta = luckyWheelData.iPrizeNumber
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	luckyWheelData.sMetricData.m_casinoMetric.m_finalChipBalance =  GET_LOCAL_PLAYERS_CASINO_CHIPS_TOTAL()
	
	// Lucky wheel prize amount
	luckyWheelData.sMetricData.m_rewardAmount = GET_LUCKY_WHEEL_PRIZE_AMOUNT()
	luckyWheelData.sMetricData.m_casinoMetric.m_win = TRUE
	
	// Casino membership
	IF DOES_PLAYER_OWN_A_CASINO_APARTMENT(PLAYER_ID())
		luckyWheelData.sMetricData.m_casinoMetric.m_membership = GET_HASH_KEY("VIP membership")
	ELSE
		luckyWheelData.sMetricData.m_casinoMetric.m_membership = GET_HASH_KEY("paid membership")
	ENDIF	
	
	luckyWheelData.sMetricData.m_casinoMetric.m_viewedLegalScreen = TRUE
	
	PRINTLN("[LUCKY_WHEEL] SEND_LUCKY_WHEEL_METRIC_DATA m_rewardType: ", luckyWheelData.sMetricData.m_rewardType, " m_rewardItem: ", luckyWheelData.sMetricData.m_rewardItem, " m_rewardAmount: " , luckyWheelData.sMetricData.m_rewardAmount )
	
	// Send metric data
	PLAYSTATS_CASINO_LUCKY_SEVEN(luckyWheelData.sMetricData)
	
ENDPROC

PROC GET_LUCKY_WHEEL_WIN_SOUND_TYPE_AND_SIZE(TEXT_LABEL &sType, FLOAT &iSize)
	LUCKY_WHEEL_PRIZES ePrize = INT_TO_ENUM(LUCKY_WHEEL_PRIZES, GET_PLAYER_PRIZE_SEGMENT(PLAYER_ID()))
	SWITCH ePrize
		CASE LUCKY_WHEEL_PRIZE_RP_2500	
		CASE LUCKY_WHEEL_PRIZE_RP_15000	
		CASE LUCKY_WHEEL_PRIZE_RP_5000	
		CASE LUCKY_WHEEL_PRIZE_RP_7500	
		CASE LUCKY_WHEEL_PRIZE_RP_10000	
			sType = "Win_RP"
			iSize = 0
		BREAK	
		CASE LUCKY_WHEEL_PRIZE_CASH_20000	
		CASE LUCKY_WHEEL_PRIZE_CASH_30000	
		CASE LUCKY_WHEEL_PRIZE_CASH_40000
		CASE LUCKY_WHEEL_PRIZE_DISCOUNT_VOUCHER
			sType = "Win_Cash"
			iSize = 0
		BREAK
		CASE LUCKY_WHEEL_PRIZE_CASH_50000	
			sType = "Win_Cash"
			iSize = 1
		BREAK
		CASE LUCKY_WHEEL_PRIZE_HOUSE_CHIPS_10000					
		CASE LUCKY_WHEEL_PRIZE_HOUSE_CHIPS_15000		
		CASE LUCKY_WHEEL_PRIZE_HOUSE_CHIPS_20000		
			sType = "Win_Chips"
			iSize = 0
		BREAK
		CASE LUCKY_WHEEL_PRIZE_HOUSE_CHIPS_25000	
			sType = "Win_Chips"
			iSize = 1
		BREAK
		CASE LUCKY_WHEEL_PRIZE_MYSTERY_STAR
			sType = "Win_Mystery"
			iSize = 0
		BREAK	
		CASE LUCKY_WHEEL_PRIZE_PODIUM_VEHICLE
			sType = "Win_Car"
			iSize = 1
		BREAK
		DEFAULT 
			sType = "Win_Clothes"
			iSize = 0
		BREAK
	ENDSWITCH
ENDPROC

PROC PLAY_WIN_SOUND(FLOAT fWinSize)
	IF HAS_SOUND_FINISHED(luckyWheelData.iWinSoundID)
		luckyWheelData.iWinSoundID = GET_SOUND_ID()
		PLAY_SOUND_FROM_COORD(luckyWheelData.iWinSoundID, "Win", GET_LUCKY_WHEEL_MINIGAME_WHEEL_COORDS(), "dlc_vw_casino_lucky_wheel_sounds", TRUE)
		SET_VARIABLE_ON_SOUND(luckyWheelData.iWinSoundID, "winSize", fWinSize)
	ELSE
		SET_VARIABLE_ON_SOUND(luckyWheelData.iWinSoundID, "winSize", fWinSize)
	ENDIF
ENDPROC

PROC PLAY_WIN_TYPE_SOUND(STRING sWinType)
	IF HAS_SOUND_FINISHED(luckyWheelData.iWinTypeSoundID)
		luckyWheelData.iWinTypeSoundID = GET_SOUND_ID()
		PLAY_SOUND_FROM_COORD(luckyWheelData.iWinTypeSoundID, sWinType, GET_LUCKY_WHEEL_MINIGAME_WHEEL_COORDS(), "dlc_vw_casino_lucky_wheel_sounds", TRUE)
	ENDIF
ENDPROC

PROC MAINTAIN_WIN_SOUNDS()
	TEXT_LABEL sType
	FLOAT fSize
	GET_LUCKY_WHEEL_WIN_SOUND_TYPE_AND_SIZE(sType, fSize)
	PLAY_WIN_SOUND(fSize)
	PLAY_WIN_TYPE_SOUND(Get_String_From_TextLabel(sType))
ENDPROC

FUNC BOOL IS_LUCKY_WHEEL_REWARDS_DONE()
	PRINTLN("[LUCKY_WHEEL] IS_LUCKY_WHEEL_REWARDS_DONE reward type: ", GET_LUCKY_REWARD_DEBUG_NAME(GET_LUCKY_REWARD_TYPE_FROM_SEGMENT_INDEX(luckyWheelData.iCurrentSegmentIndex)))
	SWITCH GET_LUCKY_REWARD_TYPE_FROM_SEGMENT_INDEX(luckyWheelData.iCurrentSegmentIndex)
		CASE LR_CLOTHES_1
		CASE LR_CLOTHES_2	
		CASE LR_CLOTHES_3	
		CASE LR_CLOTHES_4	
			RETURN GIVE_PLAYER_CLOTHES_FROM_LUCKY_WHEEL(luckyWheelData.sPrizeName, luckyWheelData.iBS, GET_LUCKY_REWARD_TYPE_FROM_SEGMENT_INDEX(luckyWheelData.iCurrentSegmentIndex), FALSE)
		BREAK
		CASE LR_RP2500		
		CASE LR_RP5000		
		CASE LR_RP7500		
		CASE LR_RP10K		
		CASE LR_RP15K		
			RETURN GIVE_PLAYER_RP_FROM_LUCKY_WHEEL(luckyWheelData.iPrizeNumber, luckyWheelData.sPrizeName, GET_LUCKY_REWARD_TYPE_FROM_SEGMENT_INDEX(luckyWheelData.iCurrentSegmentIndex))
		BREAK
		CASE LR_CASH20K		
		CASE LR_CASH30K		
		CASE LR_CASH40K		
		CASE LR_CASH50K	
			RETURN GIVE_PLAYER_CASH_FROM_LUCKY_WHEEL(luckyWheelData.iPrizeNumber, luckyWheelData.sPrizeName, GET_LUCKY_REWARD_TYPE_FROM_SEGMENT_INDEX(luckyWheelData.iCurrentSegmentIndex))
		BREAK
		CASE LR_CHIPS10K	
		CASE LR_CHIPS15K	
		CASE LR_CHIPS20K	
		CASE LR_CHIPS25K	
			RETURN GIVE_PLAYER_CHIPS_FROM_LUCKY_WHEEL(luckyWheelData.iPrizeNumber, luckyWheelData.sPrizeName, luckyWheelData.eTransactionResult, GET_LUCKY_REWARD_TYPE_FROM_SEGMENT_INDEX(luckyWheelData.iCurrentSegmentIndex))
		BREAK
		CASE LR_MYSTERY	
			IF NOT IS_BIT_SET(luckyWheelData.iBS, BS_LUCKY_WHEEL_PICK_RANDOM_MYSTERY_PRIZE)
				luckyWheelData.eMystreyRewardType = PICK_RANDOM_LUCK_MYSTERY_REWARD_TYPE()
				#IF IS_DEBUG_BUILD
				IF luckyWheelDebugData.bEnableDebugPrize
					luckyWheelData.eMystreyRewardType = INT_TO_ENUM(LUCKY_REWARD_MYSTERY_TYPE, luckyWheelDebugData.iDebugMystreyPrize)
					PRINTLN("[LUCKY_WHEEL] IS_LUCKY_WHEEL_REWARDS_DONE Mystrey DEBUG option: ", GET_LUCKY_REWARD_MYSTERY_TYPE_DEBUG_NAME(luckyWheelData.eMystreyRewardType))
				ENDIF
				#ENDIF
				SET_BIT(luckyWheelData.iBS, BS_LUCKY_WHEEL_PICK_RANDOM_MYSTERY_PRIZE)
			ELSE
				RETURN GIVE_PLAYER_MYSTERY_PRIZE(luckyWheelData.iPrizeNumber, luckyWheelData.sPrizeName, luckyWheelData.rewardVehicle, luckyWheelData.sLuckyReward, luckyWheelData.eMystreyRewardType
												, luckyWheelData.iBS, sGunLocker, luckyWheelData.eTransactionResult  #IF IS_DEBUG_BUILD , luckyWheelDebugData.bEnableDebugPrize #ENDIF )
			ENDIF	
		BREAK
		CASE LR_VEHICLE		
			RETURN GIVE_PLAYER_VEHICLE_FROM_LUCKY_WHEEL(luckyWheelData.sPrizeName, luckyWheelData.rewardVehicle, luckyWheelData.sLuckyReward, GET_LUCKY_REWARD_TYPE_FROM_SEGMENT_INDEX(luckyWheelData.iCurrentSegmentIndex))
		BREAK
		CASE LR_DISCOUNT	
			RETURN GIVE_PLAYER_VOUCHER_FROM_LUCKY_WHEEL(luckyWheelData.sPrizeName, GET_LUCKY_REWARD_TYPE_FROM_SEGMENT_INDEX(luckyWheelData.iCurrentSegmentIndex))	
		BREAK
	ENDSWITCH 
	
	RETURN FALSE
ENDFUNC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════════╡ CLEANUP ╞═══════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

PROC CLEAR_LUCKY_WHEEL_MINIGAME_PLAYER_BD()
	SET_PLAYER_REQUESTING_TO_PLAY_LUCKY_WHEEL_MINIGAME(PLAYER_ID(), FALSE)
ENDPROC

PROC SET_LUCKY_WHEEL_USAGE_TIME(BOOL bPlayerStatOnly = FALSE)
	SET_MP_INT_PLAYER_STAT(MPPLY_LUCKY_WHEEL_USAGE, luckyWheelData.iGivenRewardPosixTime + LUCKY_WHEEL_USAGE_POSIX)
	
	IF !bPlayerStatOnly
		SET_MP_INT_CHARACTER_STAT(MP_STAT_LUCKY_WHEEL_USAGE, luckyWheelData.iGivenRewardPosixTime + LUCKY_WHEEL_USAGE_POSIX)
	ENDIF	
	
	INT iNumSpin = GET_MP_INT_CHARACTER_STAT(MP_STAT_LUCKY_WHEEL_NUM_SPIN)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_LUCKY_WHEEL_NUM_SPIN, iNumSpin + 1)
	
	g_b_CasinoPrioritySave = TRUE
	
	PRINTLN("[LUCKY_WHEEL] SET_LUCKY_WHEEL_USAGE_TIME ENABLE_INTERACTION_MENU - iCurrentPosixTime: ", luckyWheelData.iGivenRewardPosixTime, " iCurrentPosixTime + LUCKY_WHEEL_USAGE_POSIX: "
			, luckyWheelData.iGivenRewardPosixTime + LUCKY_WHEEL_USAGE_POSIX, " num spin: ", iNumSpin + 1)
ENDPROC

PROC STOP_ALL_WHEEL_SOUNDS(BOOL bOnlySpingSound = FALSE)
	IF !HAS_SOUND_FINISHED(luckyWheelData.iStartSpinSoundID)
		STOP_SOUND(luckyWheelData.iStartSpinSoundID)
		RELEASE_SOUND_ID(luckyWheelData.iStartSpinSoundID)
		luckyWheelData.iStartSpinSoundID = -1
	ENDIF
	
	IF !bOnlySpingSound
		IF !HAS_SOUND_FINISHED(luckyWheelData.iWinSoundID)
			STOP_SOUND(luckyWheelData.iWinSoundID)
			RELEASE_SOUND_ID(luckyWheelData.iWinSoundID)
			luckyWheelData.iWinSoundID = -1
		ENDIF
		
		IF !HAS_SOUND_FINISHED(luckyWheelData.iWinTypeSoundID)
			STOP_SOUND(luckyWheelData.iWinTypeSoundID)
			RELEASE_SOUND_ID(luckyWheelData.iWinTypeSoundID)
			luckyWheelData.iWinTypeSoundID = -1
		ENDIF
	ENDIF
ENDPROC


PROC RELEASE_ALL_WHEEL_SOUNDS_WHEN_FINISHED()
	IF HAS_SOUND_FINISHED(luckyWheelData.iStartSpinSoundID)
		STOP_SOUND(luckyWheelData.iStartSpinSoundID)
		RELEASE_SOUND_ID(luckyWheelData.iStartSpinSoundID)
		luckyWheelData.iStartSpinSoundID = -1
	ENDIF
	
	IF HAS_SOUND_FINISHED(luckyWheelData.iWinSoundID)
		STOP_SOUND(luckyWheelData.iWinSoundID)
		RELEASE_SOUND_ID(luckyWheelData.iWinSoundID)
		luckyWheelData.iWinSoundID = -1
	ENDIF
	
	IF HAS_SOUND_FINISHED(luckyWheelData.iWinTypeSoundID)
		STOP_SOUND(luckyWheelData.iWinTypeSoundID)
		RELEASE_SOUND_ID(luckyWheelData.iWinTypeSoundID)
		luckyWheelData.iWinTypeSoundID = -1
	ENDIF
ENDPROC

FUNC STRING GET_CASINO_LUCKY_WHEEL_DISCLAIMER_MENU_NAME(INT iBitToCheck)
	SWITCH iBitToCheck
		CASE BS_DISCLAIMER_MENU_REBUILD_DISCLAIMER_MENU					RETURN "BS_DISCLAIMER_MENU_REBUILD_DISCLAIMER_MENU"
	ENDSWITCH
	RETURN ""
ENDFUNC

FUNC BOOL IS_CASINO_LUCKY_WHEEL_DISCLAIMER_MENU_BS_SET(INT iBitToCheck)
	RETURN IS_BIT_SET(luckyWheelData.sDisclaimerMenu.iLocalBS, iBitToCheck)
ENDFUNC

PROC SET_CASINO_LUCKY_WHEEL_DISCLAIMER_MENU_BS_SET(INT iBitToCheck)
	IF NOT IS_BIT_SET(luckyWheelData.sDisclaimerMenu.iLocalBS, iBitToCheck)
		SET_BIT(luckyWheelData.sDisclaimerMenu.iLocalBS, iBitToCheck)
		PRINTLN("SET_CASINO_LUCKY_WHEEL_DISCLAIMER_MENU_BS_SET - ", GET_CASINO_LUCKY_WHEEL_DISCLAIMER_MENU_NAME(iBitToCheck))
	ENDIF
ENDPROC

PROC CLEAR_CASINO_LUCKY_WHEEL_DISCLAIMER_MENU_BS_SET(INT iBitToCheck)
	IF IS_BIT_SET(luckyWheelData.sDisclaimerMenu.iLocalBS, iBitToCheck)
		CLEAR_BIT(luckyWheelData.sDisclaimerMenu.iLocalBS, iBitToCheck)
		PRINTLN("CLEAR_CASINO_LUCKY_WHEEL_DISCLAIMER_MENU_BS_SET - ", GET_CASINO_LUCKY_WHEEL_DISCLAIMER_MENU_NAME(iBitToCheck))
	ENDIF
ENDPROC

PROC CLEANUP_LUCKY_WHEEL_MINIGAME()
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
	
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("LUCK_WHEEL_SPIN")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("LUCK_W_SPIN_PC")
		CLEAR_HELP()
	ENDIF
	
	IF PLAYER_ID() = luckyWheelServerData.minigamePlayer
		CLEAR_LUCKY_WHEEL_MINIGAME_PROPERTIES()
		SET_LUCKY_WHEEL_MINIGAME_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		THEFEED_SHOW()
		
		//CLEAR_PED_TASKS(PLAYER_PED_ID())
		
		IF luckyWheelData.sLuckyReward.iTransactionResult != 0
			IF luckyWheelData.sLuckyReward.iTransactionResult = GARAGE_VEHICLE_TRANSACTION_STATE_PENDING
				DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
			ENDIF
			
			luckyWheelData.sLuckyReward.iTransactionResult = 0
		ENDIF
		
		IF DOES_ENTITY_EXIST(luckyWheelData.rewardVehicle)
		AND luckyWheelData.rewardVehicle != g_CasinoRewardVehicle	
			DELETE_VEHICLE(luckyWheelData.rewardVehicle)
		ELSE
			luckyWheelData.rewardVehicle = INT_TO_NATIVE(VEHICLE_INDEX, -1)
		ENDIF	
		
		IF IS_INTERACTION_MENU_DISABLED()
			ENABLE_INTERACTION_MENU()
			PRINTLN("[LUCKY_WHEEL] CLEANUP_LUCKY_WHEEL_MINIGAME ENABLE_INTERACTION_MENU - enabling interaction menu")
		ENDIF

		DISPLAY_RADAR(TRUE)
	ENDIF
	
	STOP_ALL_WHEEL_SOUNDS(TRUE)
	CLEANUP_MENU_ASSETS()
	
	SET_PLAYER_IS_USING_THE_LUCKY_WHEEL(PLAYER_ID(), FALSE)
	
	IF IS_LUCKY_WHEEL_MINIGAME_BIT_SET(BS_LUCKY_WHEEL_ACTIVATED)
		SET_LUCKY_WHEEL_MINIGAME_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	ENDIF
	
	luckyWheelData.sPrizeName = ""
	luckyWheelData.iPrizeNumber = 0
	luckyWheelData.fStartSoundSpeed = 0
	
	luckyWheelData.eMystreyRewardType = LRM_INVALID
	
	LUCKY_WHEEL_REWARD_STRUCT sLuckyReward
	luckyWheelData.sLuckyReward = sLuckyReward
	
	SET_LUCKY_WHEEL_FLOW_STATE(LUCKY_WHEEL_FLOW_STATE_INITAILISE)
	RESET_LUCKY_WHEEL_MINIGAME_DATA()
	CLEAR_LUCKY_WHEEL_MINIGAME_PLAYER_BD()
	SET_PLAYER_STARTED_SPIN(FALSE)
	RESET_NET_TIMER(luckyWheelData.sNetDelay)
	RESET_NET_TIMER(luckyWheelData.stUpdateServerBDTimer)
	RESET_NET_TIMER(luckyWheelData.sDisclaimerMenuTimeOut)
	SET_SPIN_INTENSITY(LUCKY_WHEEL_SPIN_INTENSITY_INVALID)
	CLEAR_CASINO_LUCKY_WHEEL_DISCLAIMER_MENU_BS_SET(BS_DISCLAIMER_MENU_REBUILD_DISCLAIMER_MENU)
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ USER INPUT ╞══════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

PROC MAINTAIN_LUCKY_WHEEL_MINIGAME_ANALOGUE_STICK_INPUT()
	luckyWheelData.iLeftAnalogueStickX = FLOOR(GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_X) * 128.0)
	luckyWheelData.iLeftAnalogueStickY = FLOOR(GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y) * 128.0)
ENDPROC

FUNC BOOL IS_LUCKY_WHEEL_MINIGAME_LEFT_ANALOGUE_STICK_UP()
	RETURN (luckyWheelData.iLeftAnalogueStickX <= 20 AND luckyWheelData.iLeftAnalogueStickX >= -20 AND luckyWheelData.iLeftAnalogueStickY <= -120)
ENDFUNC

FUNC BOOL IS_LUCKY_WHEEL_MINIGAME_LEFT_ANALOGUE_STICK_DOWN()
	IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL) 
		RETURN IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_MOVE_DOWN_ONLY)
	ENDIF
	RETURN (luckyWheelData.iLeftAnalogueStickX <= 20 AND luckyWheelData.iLeftAnalogueStickX >= -20 AND luckyWheelData.iLeftAnalogueStickY >= 120)
ENDFUNC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════╡ MINIGAME ACTIVATION ╞══════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

FUNC BOOL IS_PLAYER_TRIGGERING_LUCKY_WHEEL_MINIGAME()
	IF (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT)
	AND NOT IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE)
	AND NOT IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
	AND NOT IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE)
	AND NOT IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
	AND NOT IS_PLAYER_ON_PAUSE_MENU(PLAYER_ID()))
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_IN_LUCKY_WHEEL_MINIGAME_LOCATE(BOOL bStaggerLocate = TRUE)
	IF bStaggerLocate
		IF (GET_FRAME_COUNT() % 3) = 0
			RETURN IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1110.995483,228.903366,-50.640800>>, <<1109.727417,228.935181,-48.390800>>, 1.500000)
		ENDIF
	ELSE
		RETURN IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1110.995483,228.903366,-50.640800>>, <<1109.727417,228.935181,-48.390800>>, 1.500000)
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_PLAYER_ACTIVATED_LUCKY_WHEEL_MINIGAME()
	IF IS_LUCKY_WHEEL_MINIGAME_BIT_SET(BS_LUCKY_WHEEL_PLAYER_INSIDE_LOCATE)
		IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
			RETURN (IS_PLAYER_TRIGGERING_LUCKY_WHEEL_MINIGAME())
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ HELP TEXT ╞══════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

PROC CLEAR_LUCKY_WHEEL_MINIGAME_HELP_TEXT()
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("LW_PLAY")
		CLEAR_HELP()
	ENDIF
ENDPROC

PROC MAINTAIN_LUCKY_WHEEL_MINIGAME_HELP_TEXT()
	IF IS_PLAYER_IN_LUCKY_WHEEL_MINIGAME_LOCATE(FALSE)
		SET_LUCKY_WHEEL_MINIGAME_BIT(BS_LUCKY_WHEEL_PLAYER_INSIDE_LOCATE)
	ENDIF
	
	IF IS_LUCKY_WHEEL_MINIGAME_BIT_SET(BS_LUCKY_WHEEL_PLAYER_INSIDE_LOCATE)
		IF NOT IS_BIT_SET(luckyWheelData.iPermanentBS, BS_PERMANENT_LUCKY_WHEEL_PRIZE_HELP_ON_SCREEN)
			PRINT_HELP("LW_PLAY")
		ENDIF	
		IF NOT IS_PLAYER_IN_LUCKY_WHEEL_MINIGAME_LOCATE(FALSE)
			CLEAR_LUCKY_WHEEL_MINIGAME_BIT(BS_LUCKY_WHEEL_PLAYER_INSIDE_LOCATE)
		ENDIF
	ENDIF
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ ANIMATIONS ╞══════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

FUNC VECTOR GET_LUCKY_WHEEL_MINIGAME_WHEEL_BASE_COORDS()
	RETURN <<1111.0519, 229.8492, -50.6409>>
ENDFUNC

FUNC VECTOR GET_LUCKY_WHEEL_MINIGAME_GO_TO_COORDS(STRING sAnimClip)
	VECTOR vCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
	LUCKY_WHEEL_ANIM_CLIPS eClip = GET_LUCKY_WHEEL_MINIGAME_ANIMATION_CLIP_ENUM(sAnimClip)
	IF (eClip = LUCKY_WHEEL_ANIM_CLIP_ENTRY)
		vCoords = GET_LUCKY_WHEEL_MINIGAME_WHEEL_COORDS()
	ENDIF
	RETURN vCoords
ENDFUNC

FUNC FLOAT GET_LUCKY_WHEEL_MINIGAME_GO_TO_HEADING(STRING sAnimClip)
	FLOAT fHeading = GET_ENTITY_HEADING(PLAYER_PED_ID())
	LUCKY_WHEEL_ANIM_CLIPS eClip = GET_LUCKY_WHEEL_MINIGAME_ANIMATION_CLIP_ENUM(sAnimClip)
	IF (eClip = LUCKY_WHEEL_ANIM_CLIP_ENTRY)
		fHeading = 10.8133
	ENDIF
	RETURN fHeading
ENDFUNC

FUNC VECTOR GET_LUCKY_WHEEL_MINIGAME_ANIM_OFFSET_COORDS()
	RETURN GET_ANIM_INITIAL_OFFSET_POSITION(luckyWheelData.sAnimDict, luckyWheelData.sAnimClip, GET_LUCKY_WHEEL_MINIGAME_WHEEL_BASE_COORDS(), <<0.0, 0.0, 0.0>>)
ENDFUNC

FUNC FLOAT GET_LUCKY_WHEEL_MINIGAME_ANIM_OFFSET_HEADING()
	VECTOR vAnimRotation = GET_ANIM_INITIAL_OFFSET_ROTATION(luckyWheelData.sAnimDict, luckyWheelData.sAnimClip, GET_LUCKY_WHEEL_MINIGAME_GO_TO_COORDS(luckyWheelData.sAnimClip), <<0.0, 0.0, 0.0>>)
	RETURN vAnimRotation.z
ENDFUNC

FUNC VECTOR GET_LUCKY_WHEEL_MINIGAME_ANIM_COORDS(BOOL bGoToTask)
	IF bGoToTask
		RETURN GET_LUCKY_WHEEL_MINIGAME_ANIM_OFFSET_COORDS()
	ELSE
		RETURN GET_LUCKY_WHEEL_MINIGAME_WHEEL_BASE_COORDS()
	ENDIF
	RETURN <<0,0,0>>
ENDFUNC

FUNC FLOAT GET_LUCKY_WHEEL_MINIGAME_ANIM_HEADING(BOOL bGoToTask)
	IF bGoToTask
		RETURN GET_LUCKY_WHEEL_MINIGAME_ANIM_OFFSET_HEADING()
	ELSE
		RETURN GET_LUCKY_WHEEL_MINIGAME_WHEEL_HEADING() //GET_LUCKY_WHEEL_MINIGAME_ANIM_OFFSET_HEADING()
	ENDIF
	RETURN 0.0
ENDFUNC

PROC DISABLE_FEATURES_FOR_LUCKY_WHEEL_MINIGAME()
	IF NOT IS_LUCKY_WHEEL_MINIGAME_BIT_SET(BS_LUCKY_WHEEL_DISABLE_FREEMODE_FEATURES)
		IF NOT IS_KILL_YOURSELF_OPTION_DISABLED()
			DISABLE_KILL_YOURSELF_OPTION()
			PRINTLN("[LUCKY_WHEEL] DISABLE_FEATURES_FOR_LUCKY_WHEEL_MINIGAME - Disabling kill yourself option")
		ENDIF
		
		IF NOT IS_INTERACTION_MENU_DISABLED()
			DISABLE_INTERACTION_MENU()
			PRINTLN("[LUCKY_WHEEL] DISABLE_FEATURES_FOR_LUCKY_WHEEL_MINIGAME - Disabling interaction menu")
		ENDIF
		
		THEFEED_HIDE()
		PRINTLN("[LUCKY_WHEEL] DISABLE_FEATURES_FOR_LUCKY_WHEEL_MINIGAME - Disabling the feed")
		
		DISPLAY_RADAR(FALSE)
		PRINTLN("[LUCKY_WHEEL] DISABLE_FEATURES_FOR_LUCKY_WHEEL_MINIGAME - Disabling radar")
		SET_LUCKY_WHEEL_MINIGAME_BIT(BS_LUCKY_WHEEL_DISABLE_FREEMODE_FEATURES)
	ENDIF
ENDPROC

FUNC BOOL IS_PLAYER_LINED_UP_FOR_ENTRY_LUCKY_WHEEL_ANIMATION()
	IF (ARE_VECTORS_ALMOST_EQUAL(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_LUCKY_WHEEL_MINIGAME_ANIM_COORDS(TRUE), 0.01)
	AND IS_HEADING_ACCEPTABLE_CORRECTED(GET_ENTITY_HEADING(PLAYER_PED_ID()), GET_LUCKY_WHEEL_MINIGAME_ANIM_HEADING(TRUE), 5.0))
	OR HAS_NET_TIMER_EXPIRED(luckyWheelData.stGoToEntryCoordsTaskSafetyTimer, ciLUCKY_WHEEL_ENTRY_ANIM_SAFETY_TIMER_MS)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC STOP_CURRENT_LUCKY_WHEEL_SYNCHED_SCENE()
	IF IS_SYNCHRONIZED_SCENE_RUNNING(GET_WHEEL_ANIM_SCENE_ID(PLAYER_ID()))
		DETACH_SYNCHRONIZED_SCENE(GET_WHEEL_ANIM_SCENE_ID(PLAYER_ID()))
		NETWORK_STOP_SYNCHRONISED_SCENE(GET_WHEEL_ANIM_SCENE_ID(PLAYER_ID()))
		SET_WHEEL_ANIM_SCENE_ID(-1)
	ENDIF
ENDPROC

FUNC STRING GET_LUCKY_WHEEL_MINIGAME_WHEEL_SPIN_ANIMATION_CLIP(LUCKY_WHEEL_SPIN_INTENSITY eSpinIntensity)
	STRING sClipName = "null"
	PRINTLN("[LUCKY_WHEEL] luckyWheelData.iSpinStartIndex ", luckyWheelData.iSpinStartIndex, " eSpinIntensity: ", GET_LUCKY_WHEEL_SPIN_INTENSITY_STRING(eSpinIntensity))
	SWITCH eSpinIntensity
		CASE LUCKY_WHEEL_SPIN_INTENSITY_LOW
			IF IS_PED_FEMALE(PLAYER_PED_ID())
				SWITCH luckyWheelData.iSpinStartIndex
					CASE 0		RETURN 	"spinningwheel_low_effort_01_wheel"		BREAK
					CASE 1		RETURN 	"spinningwheel_low_effort_02_wheel"		BREAK		
					CAsE 2		RETURN 	"spinningwheel_low_effort_03_wheel"		BREAK
					CAsE 3		RETURN 	"spinningwheel_low_effort_04_wheel"		BREAK
					CAsE 4		RETURN 	"spinningwheel_low_effort_05_wheel"		BREAK
					CAsE 5		RETURN 	"spinningwheel_low_effort_06_wheel"		BREAK
					CAsE 6		RETURN 	"spinningwheel_low_effort_07_wheel"		BREAK
					CAsE 7		RETURN 	"spinningwheel_low_effort_08_wheel"		BREAK
					CAsE 8		RETURN 	"spinningwheel_low_effort_09_wheel"		BREAK
					CAsE 9		RETURN 	"spinningwheel_low_effort_10_wheel"		BREAK
					CAsE 10		RETURN 	"spinningwheel_low_effort_11_wheel"		BREAK
					CAsE 11		RETURN 	"spinningwheel_low_effort_12_wheel"		BREAK
					CAsE 12		RETURN 	"spinningwheel_low_effort_13_wheel"		BREAK
					CAsE 13		RETURN 	"spinningwheel_low_effort_14_wheel"		BREAK
					CAsE 14		RETURN 	"spinningwheel_low_effort_15_wheel"		BREAK
					CAsE 15		RETURN 	"spinningwheel_low_effort_16_wheel"		BREAK
					CAsE 16		RETURN 	"spinningwheel_low_effort_17_wheel"		BREAK
					CAsE 17		RETURN 	"spinningwheel_low_effort_18_wheel"		BREAK
					CAsE 18		RETURN 	"spinningwheel_low_effort_19_wheel"		BREAK
					CAsE 19		RETURN 	"spinningwheel_low_effort_20_wheel"		BREAK
				ENDsWITCH
			ELSE
				SWITCH luckyWheelData.iSpinStartIndex
					CASE 0		RETURN 	"spinningwheel_low_effort_01"		BREAK
					CASE 1		RETURN 	"spinningwheel_low_effort_02"		BREAK		
					CAsE 2		RETURN 	"spinningwheel_low_effort_03"		BREAK
					CAsE 3		RETURN 	"spinningwheel_low_effort_04"		BREAK
					CAsE 4		RETURN 	"spinningwheel_low_effort_05"		BREAK
					CAsE 5		RETURN 	"spinningwheel_low_effort_06"		BREAK
					CAsE 6		RETURN 	"spinningwheel_low_effort_07"		BREAK
					CAsE 7		RETURN 	"spinningwheel_low_effort_08"		BREAK
					CAsE 8		RETURN 	"spinningwheel_low_effort_09"		BREAK
					CAsE 9		RETURN 	"spinningwheel_low_effort_10"		BREAK
					CAsE 10		RETURN 	"spinningwheel_low_effort_11"		BREAK
					CAsE 11		RETURN 	"spinningwheel_low_effort_12"		BREAK
					CAsE 12		RETURN 	"spinningwheel_low_effort_13"		BREAK
					CAsE 13		RETURN 	"spinningwheel_low_effort_14"		BREAK
					CAsE 14		RETURN 	"spinningwheel_low_effort_15"		BREAK
					CAsE 15		RETURN 	"spinningwheel_low_effort_16"		BREAK
					CAsE 16		RETURN 	"spinningwheel_low_effort_17"		BREAK
					CAsE 17		RETURN 	"spinningwheel_low_effort_18"		BREAK
					CAsE 18		RETURN 	"spinningwheel_low_effort_19"		BREAK
					CAsE 19		RETURN 	"spinningwheel_low_effort_20"		BREAK
				ENDsWITCH
			ENDIF
		BREAK
		CASE LUCKY_WHEEL_SPIN_INTENSITY_MEDIUM
			IF IS_PED_FEMALE(PLAYER_PED_ID())
				// All of the medium female wheel anims are off by one from animation.
				SWITCH luckyWheelData.iSpinStartIndex
					CASE 0		RETURN 	"spinningwheel_med_effort_20_wheel"		BREAK
					CAsE 1		RETURN 	"spinningwheel_med_effort_01_wheel"		BREAK		
					CAsE 2		RETURN 	"spinningwheel_med_effort_02_wheel"		BREAK
					CAsE 3		RETURN 	"spinningwheel_med_effort_03_wheel"		BREAK
					CAsE 4		RETURN 	"spinningwheel_med_effort_04_wheel"		BREAK
					CAsE 5		RETURN 	"spinningwheel_med_effort_05_wheel"		BREAK
					CAsE 6		RETURN 	"spinningwheel_med_effort_06_wheel"		BREAK
					CAsE 7		RETURN 	"spinningwheel_med_effort_07_wheel"		BREAK
					CAsE 8		RETURN 	"spinningwheel_med_effort_08_wheel"		BREAK
					CAsE 9		RETURN 	"spinningwheel_med_effort_09_wheel"		BREAK
					CAsE 10		RETURN 	"spinningwheel_med_effort_10_wheel"		BREAK
					CAsE 11		RETURN 	"spinningwheel_med_effort_11_wheel"		BREAK
					CAsE 12		RETURN 	"spinningwheel_med_effort_12_wheel"		BREAK
					CAsE 13		RETURN 	"spinningwheel_med_effort_13_wheel"		BREAK
					CAsE 14		RETURN 	"spinningwheel_med_effort_14_wheel"		BREAK
					CAsE 15		RETURN 	"spinningwheel_med_effort_15_wheel"		BREAK
					CAsE 16		RETURN 	"spinningwheel_med_effort_16_wheel"		BREAK
					CAsE 17		RETURN 	"spinningwheel_med_effort_17_wheel"		BREAK
					CAsE 18		RETURN 	"spinningwheel_med_effort_18_wheel"		BREAK
					CAsE 19		RETURN 	"spinningwheel_med_effort_19_wheel"		BREAK
				ENDSWITCH
			ELSE
				SWITCH luckyWheelData.iSpinStartIndex
					CASE 0		RETURN 	"spinningwheel_med_effort_01"		BREAK
					CAsE 1		RETURN 	"spinningwheel_med_effort_02"		BREAK		
					CAsE 2		RETURN 	"spinningwheel_med_effort_03"		BREAK
					CAsE 3		RETURN 	"spinningwheel_med_effort_04"		BREAK
					CAsE 4		RETURN 	"spinningwheel_med_effort_05"		BREAK
					CAsE 5		RETURN 	"spinningwheel_med_effort_06"		BREAK
					CAsE 6		RETURN 	"spinningwheel_med_effort_07"		BREAK
					CAsE 7		RETURN 	"spinningwheel_med_effort_08"		BREAK
					CAsE 8		RETURN 	"spinningwheel_med_effort_09"		BREAK
					CAsE 9		RETURN 	"spinningwheel_med_effort_10"		BREAK
					CAsE 10		RETURN 	"spinningwheel_med_effort_11"		BREAK
					CAsE 11		RETURN 	"spinningwheel_med_effort_12"		BREAK
					CAsE 12		RETURN 	"spinningwheel_med_effort_13"		BREAK
					CAsE 13		RETURN 	"spinningwheel_med_effort_14"		BREAK
					CAsE 14		RETURN 	"spinningwheel_med_effort_15"		BREAK
					CAsE 15		RETURN 	"spinningwheel_med_effort_16"		BREAK
					CAsE 16		RETURN 	"spinningwheel_med_effort_17"		BREAK
					CAsE 17		RETURN 	"spinningwheel_med_effort_18"		BREAK
					CAsE 18		RETURN 	"spinningwheel_med_effort_19"		BREAK
					CAsE 19		RETURN 	"spinningwheel_med_effort_20"		BREAK
				ENDSWITCH
			ENDIF
		BREAK	
		CASE LUCKY_WHEEL_SPIN_INTENSITY_HIGH
			IF IS_PED_FEMALE(PLAYER_PED_ID())
				SWITCH luckyWheelData.iSpinStartIndex
					CASE 0		RETURN 	"spinningwheel_high_effort_01_wheel"		BREAK
					CAsE 1		RETURN 	"spinningwheel_high_effort_02_wheel"		BREAK		
					CAsE 2		RETURN 	"spinningwheel_high_effort_03_wheel"		BREAK
					CAsE 3		RETURN 	"spinningwheel_high_effort_04_wheel"		BREAK
					CAsE 4		RETURN 	"spinningwheel_high_effort_05_wheel"		BREAK
					CAsE 5		RETURN 	"spinningwheel_high_effort_06_wheel"		BREAK
					CAsE 6		RETURN 	"spinningwheel_high_effort_07_wheel"		BREAK
					CAsE 7		RETURN 	"spinningwheel_high_effort_08_wheel"		BREAK
					CAsE 8		RETURN 	"spinningwheel_high_effort_09_wheel"		BREAK
					CAsE 9		RETURN 	"spinningwheel_high_effort_10_wheel"		BREAK
					CAsE 10		RETURN 	"spinningwheel_high_effort_11_wheel"		BREAK
					CAsE 11		RETURN 	"spinningwheel_high_effort_12_wheel"		BREAK
					CAsE 12		RETURN 	"spinningwheel_high_effort_13_wheel"		BREAK
					CAsE 13		RETURN 	"spinningwheel_high_effort_14_wheel"		BREAK
					CAsE 14		RETURN 	"spinningwheel_high_effort_15_wheel"		BREAK
					CAsE 15		RETURN 	"spinningwheel_high_effort_16_wheel"		BREAK
					CAsE 16		RETURN 	"spinningwheel_high_effort_17_wheel"		BREAK
					CAsE 17		RETURN 	"spinningwheel_high_effort_18_wheel"		BREAK
					CAsE 18		RETURN 	"spinningwheel_high_effort_19_wheel"		BREAK
					CAsE 19		RETURN 	"spinningwheel_high_effort_20_wheel"		BREAK
				ENDSWITCH
			ELSE
				SWITCH luckyWheelData.iSpinStartIndex
					CASE 0		RETURN 	"spinningwheel_high_effort_01"		BREAK
					CAsE 1		RETURN 	"spinningwheel_high_effort_02"		BREAK		
					CAsE 2		RETURN 	"spinningwheel_high_effort_03"		BREAK
					CAsE 3		RETURN 	"spinningwheel_high_effort_04"		BREAK
					CAsE 4		RETURN 	"spinningwheel_high_effort_05"		BREAK
					CAsE 5		RETURN 	"spinningwheel_high_effort_06"		BREAK
					CAsE 6		RETURN 	"spinningwheel_high_effort_07"		BREAK
					CAsE 7		RETURN 	"spinningwheel_high_effort_08"		BREAK
					CAsE 8		RETURN 	"spinningwheel_high_effort_09"		BREAK
					CAsE 9		RETURN 	"spinningwheel_high_effort_10"		BREAK
					CAsE 10		RETURN 	"spinningwheel_high_effort_11"		BREAK
					CAsE 11		RETURN 	"spinningwheel_high_effort_12"		BREAK
					CAsE 12		RETURN 	"spinningwheel_high_effort_13"		BREAK
					CAsE 13		RETURN 	"spinningwheel_high_effort_14"		BREAK
					CAsE 14		RETURN 	"spinningwheel_high_effort_15"		BREAK
					CAsE 15		RETURN 	"spinningwheel_high_effort_16"		BREAK
					CAsE 16		RETURN 	"spinningwheel_high_effort_17"		BREAK
					CAsE 17		RETURN 	"spinningwheel_high_effort_18"		BREAK
					CAsE 18		RETURN 	"spinningwheel_high_effort_19"		BREAK
					CAsE 19		RETURN 	"spinningwheel_high_effort_20"		BREAK
				ENDSWITCH
			ENDIF
		BREAK
	ENDSWITCH
	RETURN sClipName
ENDFUNC

PROC STOP_CURRENT_LUCKY_WHEEL_WHEEL_SYNCHED_SCENE()
	STRING sAnimClip = GET_LUCKY_WHEEL_MINIGAME_WHEEL_SPIN_ANIMATION_CLIP(GET_SPIN_INTENSITY(PLAYER_ID()))
	STRING sAnimDict = GET_LUCKY_WHEEL_MINIGAME_ANIMATION_DICTIONARY()
	IF IS_ENTITY_PLAYING_ANIM(luckyWheelData.objLuckyWheel, sAnimDict, sAnimClip)
		STOP_ENTITY_ANIM(luckyWheelData.objLuckyWheel, sAnimClip, sAnimDict, NORMAL_BLEND_OUT)
	ENDIF
ENDPROC

FUNC BOOL IS_LUCKY_WHEEL_ANIMATION_CLIP_PLAYING(LUCKY_WHEEL_ANIM_CLIPS eClip)
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
		STRING sAnimClip = GET_LUCKY_WHEEL_MINIGAME_ANIMATION_CLIP(eClip)
		STRING sAnimDict = GET_LUCKY_WHEEL_MINIGAME_ANIMATION_DICTIONARY()
		IF NOT IS_STRING_NULL_OR_EMPTY(sAnimClip)
		AND NOT IS_STRING_NULL_OR_EMPTY(sAnimDict)
			RETURN IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), sAnimDict, sAnimClip)
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_LUCKY_WHEEL_MINIGAME_SPINNING_ANIMATION(STRING sAnimClip)
	IF ARE_STRINGS_EQUAL(sAnimClip, GET_LUCKY_WHEEL_MINIGAME_ANIMATION_CLIP(LUCKY_WHEEL_ANIM_CLIP_SPIN_HIGH))
	OR ARE_STRINGS_EQUAL(sAnimClip, GET_LUCKY_WHEEL_MINIGAME_ANIMATION_CLIP(LUCKY_WHEEL_ANIM_CLIP_SPIN_MEDIUM))
	OR ARE_STRINGS_EQUAL(sAnimClip, GET_LUCKY_WHEEL_MINIGAME_ANIMATION_CLIP(LUCKY_WHEEL_ANIM_CLIP_SPIN_LOW))
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC PERFORM_LUCKY_WHEEL_MINIGAME_SYNC_SCENE(LUCKY_WHEEL_ANIM_CLIPS eClip, BOOL bGoToTask, BOOL bLoopAnim = FALSE, BOOL bStopPreviousSyncedScene = TRUE, BOOL bHoldLastFrame = FALSE)
	IF bStopPreviousSyncedScene
		//STOP_CURRENT_LUCKY_WHEEL_SYNCHED_SCENE()
	ENDIF	
	luckyWheelData.sAnimClip = GET_LUCKY_WHEEL_MINIGAME_ANIMATION_CLIP(eClip)

	VECTOR vScenePos 	= GET_LUCKY_WHEEL_MINIGAME_ANIM_COORDS(bGoToTask)
	VECTOR vSceneRot 	= <<0.0, 0.0, GET_LUCKY_WHEEL_MINIGAME_ANIM_HEADING(bGoToTask)>>
	FLOAT fBlendIn 		= REALLY_SLOW_BLEND_IN
	FLOAT fBlendOut 	= REALLY_SLOW_BLEND_OUT
	
	IF eClip = LUCKY_WHEEL_ANIM_CLIP_SPIN_LOW
	OR eClip = LUCKY_WHEEL_ANIM_CLIP_SPIN_MEDIUM
	OR eClip = LUCKY_WHEEL_ANIM_CLIP_SPIN_HIGH
		fBlendIn 	= NORMAL_BLEND_IN
		fBlendOut 	= WALK_BLEND_OUT
	ENDIF
	
	IF eClip = LUCKY_WHEEL_ANIM_CLIP_SPINNING_IDLE_LOW	
	OR eClip = LUCKY_WHEEL_ANIM_CLIP_SPINNING_IDLE_MEDIUM	
	OR eClip = LUCKY_WHEEL_ANIM_CLIP_SPINNING_IDLE_HIGH	
		fBlendIn 	= WALK_BLEND_IN
	ENDIF	
	
	IF IS_PED_FEMALE(PLAYER_PED_ID())	
		IF eClip = LUCKY_WHEEL_ANIM_CLIP_RAISE_ARM_IDLE
			fBlendIn 	= SLOW_BLEND_IN
			fBlendOut 	= SLOW_BLEND_OUT
		ENDIF
	ENDIF
	
	IF eClip = LUCKY_WHEEL_ANIM_CLIP_RAISE_ARM
	OR eClip = LUCKY_WHEEL_ANIM_CLIP_IDLE_1
		fBlendIn 	= SLOW_BLEND_IN
		fBlendOut 	= SLOW_BLEND_OUT
	ENDIF
	
	IF eClip = LUCKY_WHEEL_ANIM_CLIP_EXIT 
		fBlendIn = SLOW_BLEND_IN
		fBlendOut = SLOW_BLEND_OUT
	ENDIF
	
	SYNCED_SCENE_PLAYBACK_FLAGS eFlag = SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT 
	IF bLoopAnim
		eFlag = SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_LOOP_WITHIN_SCENE
	ENDIF
	
	SET_WHEEL_ANIM_SCENE_ID(NETWORK_CREATE_SYNCHRONISED_SCENE(vScenePos, vSceneRot, DEFAULT, bHoldLastFrame, bLoopAnim))
	NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), GET_WHEEL_ANIM_SCENE_ID(PLAYER_ID()), luckyWheelData.sAnimDict, luckyWheelData.sAnimClip, fBlendIn, fBlendOut, eFlag)
	NETWORK_START_SYNCHRONISED_SCENE(GET_WHEEL_ANIM_SCENE_ID(PLAYER_ID()))
	
	TEXT_LABEL_63 sFacialClip = luckyWheelData.sAnimClip 
	sFacialClip += "_Facial"
	SET_FACIAL_IDLE_ANIM_OVERRIDE(PLAYER_PED_ID(), sFacialClip, luckyWheelData.sAnimDict)
	
	IF IS_LUCKY_WHEEL_MINIGAME_SPINNING_ANIMATION(luckyWheelData.sAnimClip)
		STRING sAnimClip = GET_LUCKY_WHEEL_MINIGAME_WHEEL_SPIN_ANIMATION_CLIP(GET_SPIN_INTENSITY(PLAYER_ID()))
		
		// Offsetting this will cause the starting anim to snap by x degress. Needs to be zero.
		SET_ENTITY_ROTATION(luckyWheelData.objLuckyWheel, <<0,0,0>>)
		PLAY_ENTITY_ANIM(luckyWheelData.objLuckyWheel, sAnimClip,  luckyWheelData.sAnimDict, INSTANT_BLEND_IN, FALSE, TRUE, FALSE, 0, ENUM_TO_INT(AF_HOLD_LAST_FRAME))
		FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(luckyWheelData.objLuckyWheel)
	ENDIF
ENDPROC

PROC PERFORM_LUCKY_WHEEL_MINIGAME_SYNC_SCENE_FOR_REMOTE_PLAYER()

	STRING sAnimClip

	sAnimClip = GET_LUCKY_WHEEL_MINIGAME_WHEEL_SPIN_ANIMATION_CLIP(GET_SPIN_INTENSITY(luckyWheelServerData.minigamePlayer))

	// Offsetting this will cause the starting anim to snap by x degress. Needs to be zero.
	SET_ENTITY_ROTATION(luckyWheelData.objLuckyWheel, <<0,0,0>>)
	PLAY_ENTITY_ANIM(luckyWheelData.objLuckyWheel, sAnimClip,  luckyWheelData.sAnimDict, INSTANT_BLEND_IN, FALSE, TRUE, FALSE, 0, ENUM_TO_INT(AF_HOLD_LAST_FRAME))
	FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(luckyWheelData.objLuckyWheel)
	
ENDPROC


PROC SET_LOCAL_PLAYER_PED_INVISIBLE_FOR_FIRST_PERSON_LUCKY_WHEEL_CAMERA_TRANSITION()
	IF GET_CAM_VIEW_MODE_FOR_CONTEXT(GET_CAM_ACTIVE_VIEW_MODE_CONTEXT()) = CAM_VIEW_MODE_FIRST_PERSON
		IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
			SET_ENTITY_ALPHA(PLAYER_PED_ID(), 0, FALSE)
		ENDIF
	ENDIF
ENDPROC

FUNC FLOAT GET_LUCKY_WHEEL_ANIMATION_EXIT_PHASE(LUCKY_WHEEL_ANIM_CLIPS eClip)
	FLOAT fExitAnimPhase = 0.96
	SWITCH eClip
		CASE LUCKY_WHEEL_ANIM_CLIP_RAISE_ARM
			IF IS_PED_FEMALE(PLAYER_PED_ID())
				fExitAnimPhase = 0.7
			ENDIF
		BREAK	
	ENDSWITCH
	RETURN fExitAnimPhase
ENDFUNC

FUNC BOOL HAS_LUCKY_WHEEL_ANIMATION_CLIP_FINISHED(LUCKY_WHEEL_ANIM_CLIPS eClip, BOOL bRemote = FALSE, BOOL bStopAnim = TRUE)
	PED_INDEX pedToCheck = PLAYER_PED_ID()
	PLAYER_INDEX playerToCheck = PLAYER_ID()
	
	IF bRemote
		pedToCheck = GET_PLAYER_PED(luckyWheelServerData.minigamePlayer)
		playerToCheck = luckyWheelServerData.minigamePlayer
	ENDIF
	
	IF IS_ENTITY_ALIVE(pedToCheck)
		STRING sAnimClip = GET_LUCKY_WHEEL_MINIGAME_ANIMATION_CLIP(eClip)
		STRING sAnimDict = GET_LUCKY_WHEEL_MINIGAME_ANIMATION_DICTIONARY()
		IF NOT IS_STRING_NULL_OR_EMPTY(sAnimClip)
		AND NOT IS_STRING_NULL_OR_EMPTY(sAnimDict)
			IF IS_ENTITY_PLAYING_ANIM(pedToCheck, sAnimDict, sAnimClip)
				
				FLOAT fExitAnimPhase = GET_LUCKY_WHEEL_ANIMATION_EXIT_PHASE(eClip)
				INT iLocalSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(GET_WHEEL_ANIM_SCENE_ID(playerToCheck))
				
				IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iLocalSceneID)
				OR GET_SYNCHRONIZED_SCENE_PHASE(iLocalSceneID) >= fExitAnimPhase
				OR HAS_ANIM_EVENT_FIRED(pedToCheck, GET_HASH_KEY("BLEND_OUT"))
				OR HAS_ANIM_EVENT_FIRED(pedToCheck, GET_HASH_KEY("EARLY_OUT"))
					IF bStopAnim
						STOP_CURRENT_LUCKY_WHEEL_SYNCHED_SCENE()
					ENDIF	
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_LUCKY_WHEEL_PROP_ANIMATION_CLIP_FINISHED(BOOL bRemotePlayer = FALSE)
	IF DOES_ENTITY_EXIST(luckyWheelData.objLuckyWheel)
		STRING sAnimClip
		IF !bRemotePlayer 
		 	sAnimClip = GET_LUCKY_WHEEL_MINIGAME_WHEEL_SPIN_ANIMATION_CLIP(GET_SPIN_INTENSITY(PLAYER_ID()))
		ELSE
			sAnimClip = GET_LUCKY_WHEEL_MINIGAME_WHEEL_SPIN_ANIMATION_CLIP(GET_SPIN_INTENSITY(luckyWheelServerData.minigamePlayer))
		ENDIF
		STRING sAnimDict = GET_LUCKY_WHEEL_MINIGAME_ANIMATION_DICTIONARY()
		IF NOT IS_STRING_NULL_OR_EMPTY(sAnimClip)
		AND NOT IS_STRING_NULL_OR_EMPTY(sAnimDict)
			IF IS_ENTITY_PLAYING_ANIM(luckyWheelData.objLuckyWheel, sAnimDict, sAnimClip)
				FLOAT fExitAnimPhase = 1.0
				FLOAT iAnimTime = GET_ENTITY_ANIM_CURRENT_TIME(luckyWheelData.objLuckyWheel, sAnimDict, sAnimClip)
				IF iAnimTime = fExitAnimPhase
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_LUCKY_WHEEL_MINIGAME_ENTRY_ANIMATION()
	DISABLE_FEATURES_FOR_LUCKY_WHEEL_MINIGAME()
	
	IF NOT IS_LUCKY_WHEEL_MINIGAME_BIT_SET(BS_LUCKY_WHEEL_ENTRY_ANIM_PLAYING)
		IF NOT IS_LUCKY_WHEEL_MINIGAME_BIT_SET(BS_LUCKY_WHEEL_GO_TO_COORDS_TASK_SET)
			REQUEST_ANIM_DICT(luckyWheelData.sAnimDict)
			IF HAS_ANIM_DICT_LOADED(luckyWheelData.sAnimDict)
				IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) <> PERFORMING_TASK
				AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) <> WAITING_TO_START_TASK
					TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), GET_LUCKY_WHEEL_MINIGAME_ANIM_COORDS(TRUE), PEDMOVEBLENDRATIO_WALK, 5000, GET_LUCKY_WHEEL_MINIGAME_ANIM_HEADING(TRUE), 0.001)
					START_NET_TIMER(luckyWheelData.stGoToEntryCoordsTaskSafetyTimer)
					SET_LUCKY_WHEEL_MINIGAME_BIT(BS_LUCKY_WHEEL_GO_TO_COORDS_TASK_SET)
				ENDIF
			ENDIF
		ELSE
			IF HAS_ANIM_DICT_LOADED(luckyWheelData.sAnimDict)
				IF IS_PLAYER_LINED_UP_FOR_ENTRY_LUCKY_WHEEL_ANIMATION()
					//CLEAR_PED_TASKS(PLAYER_PED_ID())
					PERFORM_LUCKY_WHEEL_MINIGAME_SYNC_SCENE(LUCKY_WHEEL_ANIM_CLIP_ENTRY, FALSE, FALSE)
					RESET_NET_TIMER(luckyWheelData.stGoToEntryCoordsTaskSafetyTimer)
					SET_LUCKY_WHEEL_MINIGAME_BIT(BS_LUCKY_WHEEL_ENTRY_ANIM_PLAYING)
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF HAS_LUCKY_WHEEL_ANIMATION_CLIP_FINISHED(LUCKY_WHEEL_ANIM_CLIP_ENTRY, FALSE, FALSE)
			PERFORM_LUCKY_WHEEL_MINIGAME_SYNC_SCENE(LUCKY_WHEEL_ANIM_CLIP_IDLE_1, FALSE, TRUE, FALSE)
			CLEAR_LUCKY_WHEEL_MINIGAME_BIT(BS_LUCKY_WHEEL_ENTRY_ANIM_PLAYING)
			SET_LUCKY_WHEEL_MINIGAME_BIT(BS_LUCKY_WHEEL_READY_TO_SPIN_WHEEL)
		ENDIF
	ENDIF
	
ENDPROC

PROC MAINTAIN_LUCKY_WHEEL_MINIGAME_IDLE_ANIMATION()
	
	IF GET_LUCKY_WHEEL_FLOW_STATE() = LUCKY_WHEEL_FLOW_STATE_RAISED_IDLE
		IF HAS_NET_TIMER_STARTED(luckyWheelData.stControlStickUpTimer)
			IF HAS_NET_TIMER_EXPIRED(luckyWheelData.stControlStickUpTimer, 10000)
				SET_LUCKY_WHEEL_MINIGAME_BIT(BS_LUCKY_WHEEL_STICK_UP_LIMIT_REACHED)
			ENDIF
		ENDIF	
	ENDIF
	
	IF IS_LUCKY_WHEEL_MINIGAME_BIT_SET(BS_LUCKY_WHEEL_TRIGGERED)
	OR IS_LUCKY_WHEEL_MINIGAME_BIT_SET(BS_LUCKY_WHEEL_IDLE_TIMED_OUT)
		EXIT
	ENDIF

	IF NOT HAS_NET_TIMER_STARTED(luckyWheelData.stIdleSafetyTimer)
		START_NET_TIMER(luckyWheelData.stIdleSafetyTimer)
	ELIF NOT HAS_NET_TIMER_EXPIRED(luckyWheelData.stIdleSafetyTimer, ciLUCKY_WHEEL_ACTIVATION_TIME_MS)
		IF luckyWheelData.eState != LUCKY_WHEEL_FLOW_STATE_IDLE
			IF IS_LUCKY_WHEEL_MINIGAME_LEFT_ANALOGUE_STICK_DOWN()
				luckyWheelData.tdActivationTime = GET_TIME_OFFSET(GET_NETWORK_TIME(), ciLUCKY_WHEEL_ACTIVATION_TIME_MS)
				SET_LUCKY_WHEEL_MINIGAME_BIT(BS_LUCKY_WHEEL_TRIGGERED)
				SET_LUCKY_WHEEL_MINIGAME_BIT(BS_LUCKY_WHEEL_PRESSED_DOWN_ON_RAISED_ARM)
				RESET_NET_TIMER(luckyWheelData.stIdleSafetyTimer)
			ENDIF
		ELSE
			IF IS_LUCKY_WHEEL_MINIGAME_LEFT_ANALOGUE_STICK_DOWN()
				SET_LUCKY_WHEEL_MINIGAME_BIT(BS_LUCKY_WHEEL_PRESSED_DOWN_ON_RAISED_ARM)
				SET_LUCKY_WHEEL_FLOW_STATE(LUCKY_WHEEL_FLOW_STATE_USER_INPUT)
			ELIF IS_LUCKY_WHEEL_MINIGAME_LEFT_ANALOGUE_STICK_UP()
			AND NOT IS_LUCKY_WHEEL_MINIGAME_BIT_SET(BS_LUCKY_WHEEL_STICK_UP_LIMIT_REACHED)
				START_NET_TIMER(luckyWheelData.stControlStickUpTimer)			
				SET_LUCKY_WHEEL_MINIGAME_BIT(BS_LUCKY_WHEEL_TRIGGERED)
				RESET_NET_TIMER(luckyWheelData.stIdleSafetyTimer)
			ENDIF
		ENDIF
	ELSE
		IF GET_LUCKY_WHEEL_FLOW_STATE() = LUCKY_WHEEL_FLOW_STATE_RAISED_IDLE
			IF !IS_LUCKY_WHEEL_MINIGAME_LEFT_ANALOGUE_STICK_UP()
			OR IS_LUCKY_WHEEL_MINIGAME_BIT_SET(BS_LUCKY_WHEEL_STICK_UP_LIMIT_REACHED)
				PERFORM_LUCKY_WHEEL_MINIGAME_SYNC_SCENE(LUCKY_WHEEL_ANIM_CLIP_ARM_RAISED_TO_IDLE, FALSE, FALSE, FALSE, TRUE)
				SET_LUCKY_WHEEL_FLOW_STATE(LUCKY_WHEEL_FLOW_STATE_RAISED_IDLE_TO_IDLE)
				
				PRINTLN("MAINTAIN_LUCKY_WHEEL_MINIGAME_IDLE_ANIMATION move from LUCKY_WHEEL_FLOW_STATE_RAISED_IDLE to idle because of time out")
			ENDIF	
		ELSE
			SET_LUCKY_WHEEL_MINIGAME_BIT(BS_LUCKY_WHEEL_IDLE_TIMED_OUT)
			RESET_NET_TIMER(luckyWheelData.stIdleSafetyTimer)
		ENDIF	
	ENDIF
	
	#IF IS_DEBUG_BUILD
	luckyWheelDebugData.iIdleTimerDifference = (ciLUCKY_WHEEL_ACTIVATION_TIME_MS-ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), luckyWheelData.stIdleSafetyTimer.Timer)))
	IF luckyWheelDebugData.iIdleTimerDifference < 0
		luckyWheelDebugData.iIdleTimerDifference = 0
	ENDIF
	#ENDIF
	
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ SPIN WHEEL ╞══════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

/// PURPOSE: Get total time of spin based on user input
FUNC INT GET_SEGMENT_LUCKY_WHEEL_SPIN_TIME()
	
	INT iSegmentTime = ciLUCKY_WHEEL_SEGMENT_SPIN_TIME_SLOW_MS
	IF GET_WHEEL_ANIM_ACTIVATION_TIME(PLAYER_ID()) > ciLUCKY_WHEEL_SPIN_TIME_THRESHOLD_FAST_MS
		iSegmentTime = ciLUCKY_WHEEL_SEGMENT_SPIN_TIME_FAST_MS
	ELIF GET_WHEEL_ANIM_ACTIVATION_TIME(PLAYER_ID()) > ciLUCKY_WHEEL_SPIN_TIME_THRESHOLD_MEDIUM_MS
		iSegmentTime = ciLUCKY_WHEEL_SEGMENT_SPIN_TIME_MEDIUM_MS
	ENDIF
	
	#IF IS_DEBUG_BUILD
	iSegmentTime = luckyWheelDebugData.iSlowSpinTimeMS
	IF GET_WHEEL_ANIM_ACTIVATION_TIME(PLAYER_ID()) > ciLUCKY_WHEEL_SPIN_TIME_THRESHOLD_FAST_MS
		iSegmentTime = luckyWheelDebugData.iFastSpinTimeMS
	ELIF GET_WHEEL_ANIM_ACTIVATION_TIME(PLAYER_ID()) > ciLUCKY_WHEEL_SPIN_TIME_THRESHOLD_MEDIUM_MS
		iSegmentTime = luckyWheelDebugData.iMediumSpinTimeMS
	ENDIF
	#ENDIF
	
	RETURN iSegmentTime
ENDFUNC

/// PURPOSE: Get total number of spins based on user input
FUNC INT GET_TOTAL_LUCKY_WHEEL_SPINS()
	
	INT iTotalSpins = ciLUCKY_WHEEL_NUMBER_OF_SPINS_SLOW
	IF GET_WHEEL_ANIM_ACTIVATION_TIME(PLAYER_ID()) > ciLUCKY_WHEEL_SPIN_TIME_THRESHOLD_FAST_MS
		iTotalSpins = ciLUCKY_WHEEL_NUMBER_OF_SPINS_FAST
	ELIF GET_WHEEL_ANIM_ACTIVATION_TIME(PLAYER_ID()) > ciLUCKY_WHEEL_SPIN_TIME_THRESHOLD_MEDIUM_MS
		iTotalSpins = ciLUCKY_WHEEL_NUMBER_OF_SPINS_MEDIUM
	ENDIF
	
	#IF IS_DEBUG_BUILD
	iTotalSpins = luckyWheelDebugData.iSlowNumSpins
	IF GET_WHEEL_ANIM_ACTIVATION_TIME(PLAYER_ID()) > ciLUCKY_WHEEL_SPIN_TIME_THRESHOLD_FAST_MS
		iTotalSpins = luckyWheelDebugData.iFastNumSpins
	ELIF GET_WHEEL_ANIM_ACTIVATION_TIME(PLAYER_ID()) > ciLUCKY_WHEEL_SPIN_TIME_THRESHOLD_MEDIUM_MS
		iTotalSpins = luckyWheelDebugData.iMediumNumSpins
	ENDIF
	#ENDIF
	
	RETURN iTotalSpins
ENDFUNC

FUNC LUCKY_WHEEL_SPIN_INTENSITY GET_LUCKY_WHEEL_SPIN_INTENSITY()
	IF GET_WHEEL_ANIM_ACTIVATION_TIME(PLAYER_ID()) > ciLUCKY_WHEEL_SPIN_TIME_THRESHOLD_FAST_MS
		RETURN LUCKY_WHEEL_SPIN_INTENSITY_HIGH
	ELIF GET_WHEEL_ANIM_ACTIVATION_TIME(PLAYER_ID()) > ciLUCKY_WHEEL_SPIN_TIME_THRESHOLD_MEDIUM_MS
		RETURN LUCKY_WHEEL_SPIN_INTENSITY_MEDIUM
	ENDIF
	RETURN LUCKY_WHEEL_SPIN_INTENSITY_LOW
ENDFUNC

FUNC FLOAT OE_TO_TS(FLOAT fAngle)
	fAngle = NORMALIZE_ANGLE(fAngle)
	
	IF fAngle < 0
		RETURN 360 + fAngle
	ENDIF

	RETURN fAngle
ENDFUNC

PROC FINALISE_LUCKY_WHEEL_SPIN()

	// Update current angle after spin
	luckyWheelData.fCurrentAngle = luckyWheelData.fEndingAngle
	luckyWheelData.fStartingAngle = luckyWheelData.fCurrentAngle
	luckyWheelData.iSpinStartIndex = GET_PLAYER_PRIZE_SEGMENT(PLAYER_ID())
	
	BROADCAST_CASINO_LUCKY_WHEEL_SPIN(luckyWheelData.iSpinStartIndex, FALSE)
	
	// Update current segment so we know which segment is now under the arrow
	luckyWheelData.iCurrentSegmentIndex = GET_PLAYER_PRIZE_SEGMENT(PLAYER_ID())
	luckyWheelData.iStartingSegment = luckyWheelData.iCurrentSegmentIndex
	
	// Update bitsets
	CLEAR_LUCKY_WHEEL_MINIGAME_BIT(BS_LUCKY_WHEEL_SET_SEGMENT_LERP_TIMER)
	
	// Update debug
	luckyWheelData.iCurrSpinStep = luckyWheelData.iNumSpinSteps
	
ENDPROC

FUNC INT GET_LUCKY_WHEEL_WIN_ANIM_TRIGGER_TIME_MS(INT &iDuration, BOOL bRemoteSpin =  FALSE)
	INT iTime
	PLAYER_INDEX playerToCheck = PLAYER_ID()
	
	IF bRemoteSpin
		playerToCheck = luckyWheelServerData.minigamePlayer
	ENDIF
	
	SWITCH GET_SPIN_INTENSITY(playerToCheck)
		CASE LUCKY_WHEEL_SPIN_INTENSITY_LOW		iTime = (iDuration-ciLUCKY_WHEEL_TRIGGER_WIN_ANIM_SLOW_TIME_MS)		BREAK
		CASE LUCKY_WHEEL_SPIN_INTENSITY_MEDIUM	iTime = (iDuration-ciLUCKY_WHEEL_TRIGGER_WIN_ANIM_MEDIUM_TIME_MS)	BREAK
		CASE LUCKY_WHEEL_SPIN_INTENSITY_HIGH	iTime = (iDuration-ciLUCKY_WHEEL_TRIGGER_WIN_ANIM_FAST_TIME_MS)		BREAK
	ENDSWITCH
	
	IF iTime < 0
		iTime = iDuration
	ENDIF
	RETURN iTime
ENDFUNC

// Turn off wheel lights prop
PROC TURN_OFF_WHEEL_LIGHTS()
	CREATE_MODEL_HIDE(<<1111.0520, 229.8490, -50.6410>>, 2.0, INT_TO_ENUM(MODEL_NAMES, HASH("VW_PROP_VW_LUCKYLIGHT_ON")), FALSE)
ENDPROC

// Turn on wheel lights prop
PROC TURN_ON_WHEEL_LIGHTS()
	REMOVE_MODEL_HIDE(<<1111.0520, 229.8490, -50.6410>>, 2.0, INT_TO_ENUM(MODEL_NAMES, HASH("VW_PROP_VW_LUCKYLIGHT_ON")), FALSE)
ENDPROC

// Turn off wheel arrow lights prop
PROC TURN_OFF_WHEEL_ARROW_LIGHTS()
	CREATE_MODEL_HIDE(<<1111.0520, 229.8490, -50.6410>>, 5.0, INT_TO_ENUM(MODEL_NAMES, HASH("VW_PROP_VW_JACKPOT_ON")), FALSE)
ENDPROC

// Turn on wheel arrow lights prop
PROC TURN_ON_WHEEL_ARROW_LIGHTS()
	REMOVE_MODEL_HIDE(<<1111.0520, 229.8490, -50.6410>>, 5.0, INT_TO_ENUM(MODEL_NAMES, HASH("VW_PROP_VW_JACKPOT_ON")), FALSE)
ENDPROC


FUNC BOOL DOES_PRIZE_HAVE_NUMBER()
	SWITCH INT_TO_ENUM(LUCKY_WHEEL_PRIZES, luckyWheelData.iCurrentSegmentIndex)
		CASE LUCKY_WHEEL_PRIZE_RP_2500
		CASE LUCKY_WHEEL_PRIZE_RP_5000
		CASE LUCKY_WHEEL_PRIZE_RP_7500
		CASE LUCKY_WHEEL_PRIZE_RP_10000
		CASE LUCKY_WHEEL_PRIZE_RP_15000
		CASE LUCKY_WHEEL_PRIZE_CASH_20000
		CASE LUCKY_WHEEL_PRIZE_CASH_30000
		CASE LUCKY_WHEEL_PRIZE_CASH_40000
		CASE LUCKY_WHEEL_PRIZE_CASH_50000
		CASE LUCKY_WHEEL_PRIZE_HOUSE_CHIPS_10000
		CASE LUCKY_WHEEL_PRIZE_HOUSE_CHIPS_15000
		CASE LUCKY_WHEEL_PRIZE_HOUSE_CHIPS_20000
		CASE LUCKY_WHEEL_PRIZE_HOUSE_CHIPS_25000
			RETURN TRUE
		BREAK	
		CASE LUCKY_WHEEL_PRIZE_MYSTERY_STAR
			SWITCH luckyWheelData.eMystreyRewardType	
				CASE LRM_CHIPS		
				CASE LRM_CASH		
				CASE LRM_RP			
					RETURN TRUE
				BREAK
			ENDSWITCH 
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC STRING GET_PRIZE_HELP_TEXT()
	SWITCH INT_TO_ENUM(LUCKY_WHEEL_PRIZES, luckyWheelData.iCurrentSegmentIndex)
		CASE LUCKY_WHEEL_PRIZE_CLOTHING_1
		CASE LUCKY_WHEEL_PRIZE_CLOTHING_2
		CASE LUCKY_WHEEL_PRIZE_CLOTHING_3
		CASE LUCKY_WHEEL_PRIZE_CLOTHING_4
			RETURN "CAS_LW_CLO"
		BREAK	
		CASE LUCKY_WHEEL_PRIZE_RP_2500				
		CASE LUCKY_WHEEL_PRIZE_RP_5000				
		CASE LUCKY_WHEEL_PRIZE_RP_7500				
		CASE LUCKY_WHEEL_PRIZE_RP_10000				
		CASE LUCKY_WHEEL_PRIZE_RP_15000				RETURN "CAS_LW_RP"			BREAK
		CASE LUCKY_WHEEL_PRIZE_CASH_20000			
		CASE LUCKY_WHEEL_PRIZE_CASH_30000						
		CASE LUCKY_WHEEL_PRIZE_CASH_40000			
		CASE LUCKY_WHEEL_PRIZE_CASH_50000			RETURN "CAS_LW_CASH"		BREAK
		CASE LUCKY_WHEEL_PRIZE_DISCOUNT_VOUCHER
			RETURN "CAS_LW_DISC"
		BREAK	
		CASE LUCKY_WHEEL_PRIZE_HOUSE_CHIPS_10000	
		CASE LUCKY_WHEEL_PRIZE_HOUSE_CHIPS_15000	
		CASE LUCKY_WHEEL_PRIZE_HOUSE_CHIPS_20000	
		CASE LUCKY_WHEEL_PRIZE_HOUSE_CHIPS_25000	RETURN "CAS_LW_CHIP"		BREAK
		CASE LUCKY_WHEEL_PRIZE_MYSTERY_STAR
			RETURN "CAS_LW_MYST"
		BREAK	
		CASE LUCKY_WHEEL_PRIZE_PODIUM_VEHICLE
			RETURN "CAS_LW_VEHI"
		BREAK	
	ENDSWITCH
	RETURN ""
ENDFUNC

FUNC INT GET_PRIZE_HELP_NUM()
	SWITCH INT_TO_ENUM(LUCKY_WHEEL_PRIZES, luckyWheelData.iCurrentSegmentIndex)
		CASE LUCKY_WHEEL_PRIZE_RP_2500				RETURN 		2500		BREAK
		CASE LUCKY_WHEEL_PRIZE_RP_5000				RETURN 		5000		BREAK
		CASE LUCKY_WHEEL_PRIZE_RP_7500				RETURN 		7500		BREAK
		CASE LUCKY_WHEEL_PRIZE_RP_10000				RETURN 		10000		BREAK
		CASE LUCKY_WHEEL_PRIZE_RP_15000				RETURN 		15000		BREAK
		CASE LUCKY_WHEEL_PRIZE_CASH_20000			RETURN 		20000		BREAK
		CASE LUCKY_WHEEL_PRIZE_CASH_30000			RETURN 		30000		BREAK
		CASE LUCKY_WHEEL_PRIZE_CASH_40000			RETURN 		40000		BREAK
		CASE LUCKY_WHEEL_PRIZE_CASH_50000			RETURN 		50000		BREAK
		CASE LUCKY_WHEEL_PRIZE_HOUSE_CHIPS_10000	RETURN 		10000		BREAK
		CASE LUCKY_WHEEL_PRIZE_HOUSE_CHIPS_15000	RETURN 		15000		BREAK
		CASE LUCKY_WHEEL_PRIZE_HOUSE_CHIPS_20000	RETURN 		20000		BREAK
		CASE LUCKY_WHEEL_PRIZE_HOUSE_CHIPS_25000	RETURN 		25000		BREAK
		CASE LUCKY_WHEEL_PRIZE_MYSTERY_STAR
			SWITCH luckyWheelData.eMystreyRewardType	
				CASE LRM_CHIPS		
				CASE LRM_CASH		
				CASE LRM_RP			
					RETURN luckyWheelData.iPrizeNumber
				BREAK
			ENDSWITCH 
		BREAK
	ENDSWITCH
	
	RETURN 0
ENDFUNC

FUNC BOOL DOES_PRIZE_NEED_TICKER()
	SWITCH INT_TO_ENUM(LUCKY_WHEEL_PRIZES, luckyWheelData.iCurrentSegmentIndex)
		CASE LUCKY_WHEEL_PRIZE_CLOTHING_1
		CASE LUCKY_WHEEL_PRIZE_CLOTHING_2
		CASE LUCKY_WHEEL_PRIZE_CLOTHING_3
		CASE LUCKY_WHEEL_PRIZE_CLOTHING_4
		CASE LUCKY_WHEEL_PRIZE_DISCOUNT_VOUCHER		
		CASE LUCKY_WHEEL_PRIZE_MYSTERY_STAR
		CASE LUCKY_WHEEL_PRIZE_PODIUM_VEHICLE
			RETURN TRUE
		BREAK	
	ENDSWITCH
	RETURN FALSE
ENDFUNC 

FUNC STRING GET_PIZE_TICKER_TITLE()
	SWITCH INT_TO_ENUM(LUCKY_WHEEL_PRIZES, luckyWheelData.iCurrentSegmentIndex)
		CASE LUCKY_WHEEL_PRIZE_CLOTHING_1
		CASE LUCKY_WHEEL_PRIZE_CLOTHING_2
		CASE LUCKY_WHEEL_PRIZE_CLOTHING_3
		CASE LUCKY_WHEEL_PRIZE_CLOTHING_4
			RETURN "CAS_LW_RCLO"
		BREAK	
		CASE LUCKY_WHEEL_PRIZE_DISCOUNT_VOUCHER
			RETURN "CAS_LW_RDISC"
		BREAK		
		CASE LUCKY_WHEEL_PRIZE_MYSTERY_STAR
			RETURN "CAS_LW_RMYST"
		BREAK	
		CASE LUCKY_WHEEL_PRIZE_PODIUM_VEHICLE
			RETURN "CAS_LW_RVEHI"
		BREAK	
	ENDSWITCH
	RETURN ""
ENDFUNC

FUNC eICON_TYPE GET_PRIZE_TICKER_ICON()
	SWITCH INT_TO_ENUM(LUCKY_WHEEL_PRIZES, luckyWheelData.iCurrentSegmentIndex)
		CASE LUCKY_WHEEL_PRIZE_CLOTHING_1
		CASE LUCKY_WHEEL_PRIZE_CLOTHING_2
		CASE LUCKY_WHEEL_PRIZE_CLOTHING_3
		CASE LUCKY_WHEEL_PRIZE_CLOTHING_4
			RETURN ICON_TYPE_CLOTHES
		BREAK	
		CASE LUCKY_WHEEL_PRIZE_DISCOUNT_VOUCHER
			RETURN ICON_TYPE_DISCOUNT
		BREAK		
		CASE LUCKY_WHEEL_PRIZE_MYSTERY_STAR
			RETURN ICON_TYPE_MYSTERY
		BREAK	
		CASE LUCKY_WHEEL_PRIZE_PODIUM_VEHICLE
			RETURN ICON_TYPE_VEHICLE
		BREAK	
	ENDSWITCH
	RETURN ICON_TYPE_INVALID
ENDFUNC

FUNC BOOL SHOULD_ADD_VEHICLE_MODEL_TO_TICKER()
	SWITCH INT_TO_ENUM(LUCKY_WHEEL_PRIZES, luckyWheelData.iCurrentSegmentIndex)	
		CASE LUCKY_WHEEL_PRIZE_MYSTERY_STAR
			SWITCH luckyWheelData.eMystreyRewardType	
				CASE LRM_VEHICLE RETURN TRUE 
			ENDSWITCH
		BREAK	
		CASE LUCKY_WHEEL_PRIZE_PODIUM_VEHICLE
			RETURN TRUE
		BREAK	
	ENDSWITCH
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_PRIZE_HELP_AND_TICKER()
	IF DOES_PRIZE_HAVE_NUMBER()
	OR (!IS_STRING_NULL_OR_EMPTY(luckyWheelData.sPrizeName) AND ARE_STRINGS_EQUAL("GIVE_RP_FOR_VEH", luckyWheelData.sPrizeName))
		IF (!IS_STRING_NULL_OR_EMPTY(luckyWheelData.sPrizeName) AND ARE_STRINGS_EQUAL("GIVE_RP_FOR_VEH", luckyWheelData.sPrizeName))
			PRINT_HELP_WITH_NUMBER("CAS_LW_RP", 20000)
			SET_BIT(luckyWheelData.iPermanentBS, BS_PERMANENT_LUCKY_WHEEL_PRIZE_HELP_ON_SCREEN)
		ELSE
			PRINT_HELP_WITH_NUMBER(GET_PRIZE_HELP_TEXT(), GET_PRIZE_HELP_NUM())
			SET_BIT(luckyWheelData.iPermanentBS, BS_PERMANENT_LUCKY_WHEEL_PRIZE_HELP_ON_SCREEN)
		ENDIF
	ELSE
		IF !IS_STRING_NULL_OR_EMPTY(luckyWheelData.sPrizeName)
			IF !ARE_STRINGS_EQUAL("INVALID", luckyWheelData.sPrizeName)
				PRINT_HELP(GET_PRIZE_HELP_TEXT())
				SET_BIT(luckyWheelData.iPermanentBS, BS_PERMANENT_LUCKY_WHEEL_PRIZE_HELP_ON_SCREEN)
			ENDIF	
		ELSE
			PRINT_HELP(GET_PRIZE_HELP_TEXT())
			SET_BIT(luckyWheelData.iPermanentBS, BS_PERMANENT_LUCKY_WHEEL_PRIZE_HELP_ON_SCREEN)
		ENDIF	
	ENDIF
	
	IF INT_TO_ENUM(LUCKY_WHEEL_PRIZES, luckyWheelData.iCurrentSegmentIndex)	 = LUCKY_WHEEL_PRIZE_DISCOUNT_VOUCHER
		IF NOT IS_BIT_SET(luckyWheelData.iPermanentBS, BS_PERMANENT_LUCKY_WHEEL_VOUCHER_DETAIL_HELP)
			SET_BIT(luckyWheelData.iPermanentBS, BS_PERMANENT_LUCKY_WHEEL_VOUCHER_DETAIL_HELP)
			PRINTLN("[LUCKY_WHEEL] MAINTAIN_PRIZE_HELP_AND_TICKER BS_PERMANENT_LUCKY_WHEEL_VOUCHER_DETAIL_HELP TRUE")
		ENDIF	
	ENDIF
	
	IF DOES_PRIZE_NEED_TICKER()
		IF !IS_STRING_NULL_OR_EMPTY(luckyWheelData.sPrizeName)
		AND !ARE_STRINGS_EQUAL("INVALID", luckyWheelData.sPrizeName)
		AND !ARE_STRINGS_EQUAL("GIVE_RP_FOR_VEH", luckyWheelData.sPrizeName)
			THEFEED_SHOW()
			IF SHOULD_ADD_VEHICLE_MODEL_TO_TICKER()
				BEGIN_TEXT_COMMAND_THEFEED_POST("TWOSTRINGS")
			ELSE
				BEGIN_TEXT_COMMAND_THEFEED_POST(luckyWheelData.sPrizeName)
			ENDIF	
			
			IF SHOULD_ADD_VEHICLE_MODEL_TO_TICKER()
				ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(GET_MAKE_FROM_MODEL(luckyWheelData.sLuckyReward.rewardVehModel, TRUE))
				ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(luckyWheelData.sPrizeName)
			ENDIF
			
			IF luckyWheelData.iCurrentSegmentIndex = ENUM_TO_INT(LUCKY_WHEEL_PRIZE_MYSTERY_STAR)
			AND DOES_PRIZE_HAVE_NUMBER()
				ADD_TEXT_COMPONENT_INTEGER(luckyWheelData.iPrizeNumber)
			ENDIF	
				
			END_TEXT_COMMAND_THEFEED_POST_UNLOCK_TU(GET_PIZE_TICKER_TITLE(), ENUM_TO_INT(GET_PRIZE_TICKER_ICON()), luckyWheelData.sPrizeName, TRUE)
		ELSE
			PRINTLN("[LUCKY_WHEEL] MAINTAIN_PRIZE_HELP_AND_TICKER luckyWheelData.sPrizeName is empty!")
		ENDIF	
	ENDIF				
ENDPROC		

PROC PLAY_SPIN_START_SOUND(FLOAT fSpeed)
	IF HAS_SOUND_FINISHED(luckyWheelData.iStartSpinSoundID)
	AND luckyWheelData.iStartSpinSoundID = -1
		luckyWheelData.iStartSpinSoundID = GET_SOUND_ID()
		PLAY_SOUND_FROM_COORD(luckyWheelData.iStartSpinSoundID, "Spin_Start", GET_LUCKY_WHEEL_MINIGAME_WHEEL_COORDS(), "dlc_vw_casino_lucky_wheel_sounds", TRUE)
		SET_VARIABLE_ON_SOUND(luckyWheelData.iStartSpinSoundID, "spinSpeed", fSpeed)
	ELSE
		SET_VARIABLE_ON_SOUND(luckyWheelData.iStartSpinSoundID, "spinSpeed", fSpeed)
	ENDIF	
ENDPROC

PROC PLAY_SPIN_SINGLE_TICK_SOUND(FLOAT fSpeed)
	IF IS_LUCKY_WHEEL_MINIGAME_BIT_SET(BS_LUCKY_WHEEL_PLAY_TICK)
		IF luckyWheelData.iSpinTickSoundID = -1 
		AND HAS_SOUND_FINISHED(luckyWheelData.iSpinTickSoundID)
			luckyWheelData.iSpinTickSoundID = GET_SOUND_ID()
			BOOL bSync = TRUE
			IF fSpeed > 0.5
				bSync = FALSE
			ENDIF	
			PLAY_SOUND_FROM_COORD(luckyWheelData.iSpinTickSoundID, "Spin_Single_Ticks", GET_LUCKY_WHEEL_MINIGAME_WHEEL_COORDS(), "dlc_vw_casino_lucky_wheel_sounds", bSync)
			SET_VARIABLE_ON_SOUND(luckyWheelData.iSpinTickSoundID, "spinSpeed", fSpeed)	
		ELSE
			STOP_SOUND(luckyWheelData.iSpinTickSoundID)
			RELEASE_SOUND_ID(luckyWheelData.iSpinTickSoundID)
			luckyWheelData.iSpinTickSoundID = -1 
			PLAY_SPIN_SINGLE_TICK_SOUND(fSpeed)
		ENDIF
	ENDIF	
ENDPROC

/// PURPOSE: Lerps between wheel segments
PROC UPDATE_LUCKY_WHEEL_SPIN(BOOL bRemoteSpin =  FALSE)
	
	IF NOT IS_LUCKY_WHEEL_MINIGAME_BIT_SET(BS_LUCKY_WHEEL_SET_SEGMENT_LERP_TIMER)
		luckyWheelData.tdTotalLerpTime = GET_TIME_OFFSET(GET_NETWORK_TIME(), luckyWheelData.iDuration)
		luckyWheelData.tdTriggerWinAnimTime = GET_TIME_OFFSET(GET_NETWORK_TIME(), GET_LUCKY_WHEEL_WIN_ANIM_TRIGGER_TIME_MS(luckyWheelData.iDuration, bRemoteSpin))
		SET_LUCKY_WHEEL_MINIGAME_BIT(BS_LUCKY_WHEEL_SET_SEGMENT_LERP_TIMER)
	ENDIF
	
	INT iAlpha = ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), luckyWheelData.tdTotalLerpTime))
	
	// Value 0.0-1.0
	FLOAT fNormalisedAlpha = (1.0 - ((TO_FLOAT(iAlpha) / luckyWheelData.iDuration) * 1.0))
	
	IF fNormalisedAlpha < 0.0
		fNormalisedAlpha = 0.0
	ELIF fNormalisedAlpha > 1.0
		fNormalisedAlpha = 1.0
	ENDIF
	
	// Bezier curve
	FLOAT fCurveAlphaReversed = POW(1.0-fNormalisedAlpha, 3.0)
	FLOAT fCurveAlpha = (1.0-fCurveAlphaReversed)
	luckyWheelData.fCurrentAngle = LERP_FLOAT(luckyWheelData.fStartingAngle, luckyWheelData.fEndingAngle, fCurveAlpha)
	luckyWheelData.fUncappedCurrentAngle = LERP_FLOAT(luckyWheelData.fStartingAngle, luckyWheelData.fEndingAngle, fCurveAlpha)

	// Increment wheel spins completed so far
	IF luckyWheelData.fCurrentAngle > 360*luckyWheelData.iSpinsCompleted
		luckyWheelData.iSpinsCompleted++
	ENDIF

	IF luckyWheelData.fUncappedCurrentAngle > (luckyWheelData.iCurrSpinStep+1)*TO_FLOAT(ciLUCKY_WHEEL_SEGMENT_ANGLE)
		luckyWheelData.iCurrSpinStep++
	ENDIF
	
	luckyWheelData.iCurrentSegmentIndex = luckyWheelData.iCurrSpinStep % ciLUCKY_WHEEL_SEGMENTS
	
	IF NOT IS_LUCKY_WHEEL_MINIGAME_BIT_SET(BS_LUCKY_WHEEL_PERFORM_WIN_ANIM)
	AND PLAYER_ID() = luckyWheelServerData.minigamePlayer
		IF NOT IS_TIME_LESS_THAN(GET_NETWORK_TIME(), luckyWheelData.tdTriggerWinAnimTime)
			PERFORM_LUCKY_WHEEL_MINIGAME_SYNC_SCENE(GET_LUCKY_WHEEL_MINIGAME_WIN_ANIMATION_ENUM(), FALSE, FALSE)
			SET_LUCKY_WHEEL_MINIGAME_BIT(BS_LUCKY_WHEEL_PERFORM_WIN_ANIM)
		ENDIF
	ENDIF
	
	// Spin has completed
	IF NOT IS_TIME_LESS_THAN(GET_NETWORK_TIME(), luckyWheelData.tdTotalLerpTime)
		FINALISE_LUCKY_WHEEL_SPIN()
		luckyWheelData.iNumTimesLightFlash = 0
		TURN_OFF_WHEEL_ARROW_LIGHTS()
		IF PLAYER_ID() = luckyWheelServerData.minigamePlayer
			MAINTAIN_WIN_SOUNDS()
			SET_LUCKY_WHEEL_FLOW_STATE(LUCKY_WHEEL_FLOW_STATE_WINNER)
		ELSE
			SET_LUCKY_WHEEL_FLOW_STATE(LUCKY_WHEEL_FLOW_STATE_CLEANUP)
		ENDIF	
	ENDIF
	
ENDPROC

/// PURPOSE: Calculates offset from current starting segment to prize segment
FUNC INT GET_LUCKY_WHEEL_MINIGAME_STARTING_OFFSET()
	INT iOffset
	IF luckyWheelData.iCurrentSegmentIndex > GET_PLAYER_PRIZE_SEGMENT(PLAYER_ID())
		INT iDiff = (ciLUCKY_WHEEL_SEGMENTS - luckyWheelData.iCurrentSegmentIndex)
		iOffset = iDiff + GET_PLAYER_PRIZE_SEGMENT(PLAYER_ID())
	ELSE
		iOffset = (GET_PLAYER_PRIZE_SEGMENT(PLAYER_ID()) - luckyWheelData.iCurrentSegmentIndex)
	ENDIF
	
	RETURN iOffset
ENDFUNC

/// PURPOSE: Offset from end of spinning anim so we know when to begin script lerping
FUNC FLOAT GET_LUCKY_WHEEL_SPIN_STARTING_OFFSET(LUCKY_WHEEL_SPIN_INTENSITY eSpinIntensity)
	SWITCH eSpinIntensity
		CASE LUCKY_WHEEL_SPIN_INTENSITY_LOW		RETURN 72.0
		CASE LUCKY_WHEEL_SPIN_INTENSITY_MEDIUM	RETURN 90.0
		CASE LUCKY_WHEEL_SPIN_INTENSITY_HIGH	RETURN 162.0
	ENDSWITCH
	RETURN 0.0
ENDFUNC

/// PURPOSE: How far away from centre of segment are we due to starting offset
FUNC FLOAT GET_LUCKY_WHEEL_SPIN_ENDING_OFFSET(LUCKY_WHEEL_SPIN_INTENSITY eSpinIntensity)
	IF IS_PED_FEMALE(PLAYER_PED_ID())
		SWITCH eSpinIntensity
			CASE LUCKY_WHEEL_SPIN_INTENSITY_LOW		RETURN 0.4746
			CASE LUCKY_WHEEL_SPIN_INTENSITY_MEDIUM	RETURN (TO_FLOAT(ciLUCKY_WHEEL_SEGMENT_ANGLE)*2.0) - 3.4757
			CASE LUCKY_WHEEL_SPIN_INTENSITY_HIGH	RETURN TO_FLOAT(ciLUCKY_WHEEL_SEGMENT_ANGLE/4) + 1.9002
		ENDSWITCH
	ELSE
		SWITCH eSpinIntensity
			CASE LUCKY_WHEEL_SPIN_INTENSITY_LOW		RETURN TO_FLOAT(ciLUCKY_WHEEL_SEGMENT_ANGLE/4) + 1.3709
			CASE LUCKY_WHEEL_SPIN_INTENSITY_MEDIUM	RETURN TO_FLOAT(ciLUCKY_WHEEL_SEGMENT_ANGLE/4) + 3.5184
			CASE LUCKY_WHEEL_SPIN_INTENSITY_HIGH	RETURN TO_FLOAT(ciLUCKY_WHEEL_SEGMENT_ANGLE) + 14.2192
		ENDSWITCH
	ENDIF
	RETURN 0.0
ENDFUNC

/// PURPOSE: Alters the total spin steps needed to reach end segment based on starting anim
FUNC INT GET_LUCKY_WHEEL_SPIN_STEP_OFFSET(BOOL bRemoteSpin =  FALSE)
	PLAYER_INDEX playerToCheck = PLAYER_ID()
	
	IF bRemoteSpin
		playerToCheck = luckyWheelServerData.minigamePlayer
	ENDIF	
	
	SWITCH GET_SPIN_INTENSITY(playerToCheck)
		CASE LUCKY_WHEEL_SPIN_INTENSITY_MEDIUM
			IF luckyWheelData.iNumSpinSteps <= 30
				RETURN ciLUCKY_WHEEL_SEGMENTS
			ENDIF
		BREAK
	ENDSWITCH
	RETURN 0
ENDFUNC

/// PURPOSE: Calculate the ending angle of a spin based on spin intensity
///    		 Need to take into account the angle covered by starting anim
FUNC FLOAT GET_LUCKY_WHEEL_SPIN_ENDING_ANGLE(BOOL bRemoteSpin =  FALSE)
	
	// Prize segment angle we want to land on
	FLOAT fEndingAngle = (luckyWheelData.iNumSpinSteps*TO_FLOAT(ciLUCKY_WHEEL_SEGMENT_ANGLE))
	PRINTLN("[LUCKY_WHEEL] fEndingAngle 1: ", fEndingAngle)
	PRINTLN("[LUCKY_WHEEL] luckyWheelData.iNumSpinSteps: ", luckyWheelData.iNumSpinSteps)
	// Add an extra full spin if ending segment is too close to starting anim
	IF fEndingAngle < 180.0
		fEndingAngle += 360.0
		luckyWheelData.iNumSpinSteps += ciLUCKY_WHEEL_SEGMENTS
	ENDIF
	
	INT iBone = GET_ENTITY_BONE_INDEX_BY_NAME(luckyWheelData.objLuckyWheel, "Base_pivot")
	VECTOR vBoneRot = GET_ENTITY_BONE_OBJECT_ROTATION(luckyWheelData.objLuckyWheel, iBone)

	// 0=180 / -180=0 convert to 0-360.
	FLOAT fBoneTsAngle = OE_TO_TS(vBoneRot.y)
	PRINTLN("[LUCKY_WHEEL] fBoneTsAngle 2: ", fBoneTsAngle)
	
	
	// Segment that the bone is on.
	INT iBoneWheelSegmentIndex = FLOOR(fBoneTsAngle / ciLUCKY_WHEEL_SEGMENT_ANGLE)
	
	FLOAT fStartToBoneAngleDiff = TO_FLOAT(iBoneWheelSegmentIndex * ciLUCKY_WHEEL_SEGMENT_ANGLE)
	
	// Spinning to the left.
	FLOAT fStartToBoneAngleDiffLeftSpin = (360 - fStartToBoneAngleDiff)
	
	// Angle diff between bone point and segment center point.
	// Offset due to anim stopping too early, noot on point.
	FLOAT fBoneToSegmentCenter = ABSF(fBoneTsAngle - fStartToBoneAngleDiff)
	
	fEndingAngle = (fEndingAngle - fStartToBoneAngleDiffLeftSpin) - fBoneToSegmentCenter
	
	LUCKY_WHEEL_SPIN_INTENSITY wheelSpinIntensity = GET_SPIN_INTENSITY(PLAYER_ID())
	IF bRemoteSpin
		wheelSpinIntensity = GET_SPIN_INTENSITY(luckyWheelServerData.minigamePlayer)
	ENDIF
	
	// Get the ending segment as a whole number
	INT iEndingSegment = ROUND(fEndingAngle / ciLUCKY_WHEEL_SEGMENT_ANGLE)
	fEndingAngle = TO_FLOAT(iEndingSegment * ciLUCKY_WHEEL_SEGMENT_ANGLE) + GET_LUCKY_WHEEL_SPIN_ENDING_OFFSET(wheelSpinIntensity) + (luckyWheelData.iSpinStartIndex * ciLUCKY_WHEEL_SEGMENT_ANGLE)
	
	FLOAT fReturnValue = (fEndingAngle * -1)
	
	PRINTLN("[LUCKY_WHEEL] GET_LUCKY_WHEEL_SPIN_ENDING_OFFSET(wheelSpinIntensity): ", GET_LUCKY_WHEEL_SPIN_ENDING_OFFSET(wheelSpinIntensity))
	
	PRINTLN("[LUCKY_WHEEL] ")
	PRINTLN("[LUCKY_WHEEL] vBoneRot: ", vBoneRot.y)
	PRINTLN("[LUCKY_WHEEL] fBoneTsAngle: ", fBoneTsAngle)
	PRINTLN("[LUCKY_WHEEL] iBoneWheelSegmentIndex: ", iBoneWheelSegmentIndex) 
	PRINTLN("[LUCKY_WHEEL] fStartToBoneAngleDiff: ", fStartToBoneAngleDiff)
	PRINTLN("[LUCKY_WHEEL] fStartToBoneAngleDiffLeftSpin: ", fStartToBoneAngleDiffLeftSpin)
	PRINTLN("[LUCKY_WHEEL] fBoneToSegmentCenter: ", fBoneToSegmentCenter)
	PRINTLN("[LUCKY_WHEEL] iEndingSegment: ", iEndingSegment)
	PRINTLN("[LUCKY_WHEEL] fEndingAngle: ", fEndingAngle)
	PRINTLN("[LUCKY_WHEEL] fReturnValue: ", fReturnValue)
	
	RETURN fReturnValue
ENDFUNC

/// PURPOSE: Starts the lucky wheel spin
PROC START_LUCKY_WHEEL_MINIGAME_SPIN(BOOL bRemoteSpin = FALSE)
	
	IF IS_LUCKY_WHEEL_MINIGAME_BIT_SET(BS_LUCKY_WHEEL_START_SPIN)
		EXIT
	ENDIF
	
	// Calculate offset from current starting segment to prize segment
	INT iOffset = GET_LUCKY_WHEEL_MINIGAME_STARTING_OFFSET()
	PRINTLN("[LUCKY_WHEEL] iOffset: ", iOffset)
	
	// Calculate how many segments we're going to step through to get to our final segment.
    luckyWheelData.iNumSpinSteps = iOffset + (luckyWheelData.iFullSpins * ciLUCKY_WHEEL_SEGMENTS)
	PRINTLN("[LUCKY_WHEEL] luckyWheelData.iNumSpinSteps1: ", luckyWheelData.iNumSpinSteps)
	
	luckyWheelData.iNumSpinSteps += GET_LUCKY_WHEEL_SPIN_STEP_OFFSET(bRemoteSpin)
	PRINTLN("[LUCKY_WHEEL] luckyWheelData.iNumSpinSteps12 ", luckyWheelData.iNumSpinSteps)
	
	// Initialise our step counter. This is incremented every time we go through a segment.
	luckyWheelData.iCurrSpinStep = luckyWheelData.iCurrentSegmentIndex
	
	// Prize segment angle we want to land on
	luckyWheelData.fEndingAngle = GET_LUCKY_WHEEL_SPIN_ENDING_ANGLE(bRemoteSpin)
	
	// Total spin duration in milliseconds
	luckyWheelData.iDuration = (luckyWheelData.iSegmentTimeMS*luckyWheelData.iNumSpinSteps)
	
	// Set bit to trigger the update function to kick off the rotation.
	SET_LUCKY_WHEEL_MINIGAME_BIT(BS_LUCKY_WHEEL_START_SPIN)

ENDPROC

/// PURPOSE: Update the rotation of the scripted wheel
PROC UPDATE_LUCKY_WHEEL_PROP_ROTATION()
	IF luckyWheelData.eState != LUCKY_WHEEL_FLOW_STATE_WINNER
		IF DOES_ENTITY_EXIST(luckyWheelData.objLuckyWheel)
			SET_ENTITY_ROTATION(luckyWheelData.objLuckyWheel, <<0.0, luckyWheelData.fCurrentAngle, 0.0>>)
		ENDIF
	ENDIF	
ENDPROC

/// PURPOSE: Perform a spin of the lucky wheel
PROC PERFORM_LUCKY_WHEEL_MINIGAME_SPIN(BOOL bRemoteSpin = FALSE)
	START_LUCKY_WHEEL_MINIGAME_SPIN(bRemoteSpin)
	UPDATE_LUCKY_WHEEL_SPIN(bRemoteSpin)
	UPDATE_LUCKY_WHEEL_PROP_ROTATION()
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════╡ MAINTAIN MINIGAME ╞═══════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

/// PURPOSE: Sets wheel properties based off the user input
PROC SET_LUCKY_WHEEL_PROPERTIES_FROM_USER_INPUT()
	SET_WHEEL_ANIM_ACTIVATION_TIME(luckyWheelData.iActivationTimerDifference)
	luckyWheelData.iSegmentTimeMS = GET_SEGMENT_LUCKY_WHEEL_SPIN_TIME()
	luckyWheelData.iFullSpins = GET_TOTAL_LUCKY_WHEEL_SPINS()
	SET_SPIN_INTENSITY(GET_LUCKY_WHEEL_SPIN_INTENSITY())
ENDPROC

FUNC BOOL SHOULD_BAIL_LUCKY_WHEEL()
	IF SHOULD_PLAYER_BE_REMOVED_FROM_CASINO_GAME_FOR_BOSS_LAUNCHING_MISSION()
		RETURN TRUE
	ENDIF	
	RETURN FALSE
ENDFUNC 

/// PURPOSE: Maintains user input of the left analogue stick to trigger the minigame
PROC MAINTAIN_LUCKY_WHEEL_USER_INPUT()
	IF NOT IS_LUCKY_WHEEL_MINIGAME_BIT_SET(BS_LUCKY_WHEEL_SET_WHEEL_SPIN_PROPERTIES)
		luckyWheelData.iActivationTimerDifference = ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), luckyWheelData.tdActivationTime))
		
		IF IS_LUCKY_WHEEL_MINIGAME_BIT_SET(BS_LUCKY_WHEEL_PRESSED_DOWN_ON_RAISED_ARM)
			CLEAR_LUCKY_WHEEL_MINIGAME_BIT(BS_LUCKY_WHEEL_PRESSED_DOWN_ON_RAISED_ARM)
			SET_LUCKY_WHEEL_PROPERTIES_FROM_USER_INPUT()
			SET_LUCKY_WHEEL_MINIGAME_BIT(BS_LUCKY_WHEEL_SET_WHEEL_SPIN_PROPERTIES)
		ENDIF
		
		// Timed out
		IF NOT IS_TIME_LESS_THAN(GET_NETWORK_TIME(), luckyWheelData.tdActivationTime)
		OR SHOULD_BAIL_LUCKY_WHEEL()
			luckyWheelData.iActivationTimerDifference = 0
			
			IF IS_LUCKY_WHEEL_ANIMATION_CLIP_PLAYING(LUCKY_WHEEL_ANIM_CLIP_RAISE_ARM_IDLE)
				PERFORM_LUCKY_WHEEL_MINIGAME_SYNC_SCENE(LUCKY_WHEEL_ANIM_CLIP_EXIT, FALSE, FALSE)
				CLEAR_LUCKY_WHEEL_MINIGAME_BIT(BS_LUCKY_WHEEL_SET_ACTIVATION_TIMER)
				SET_LUCKY_WHEEL_MINIGAME_BIT(BS_LUCKY_WHEEL_PERFORM_EXIT_ANIM)
				SET_LUCKY_WHEEL_FLOW_STATE(LUCKY_WHEEL_FLOW_STATE_TIMED_OUT)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_PLAYER_NEAR_LUCKY_WHEEL_MINIGAME()
	RETURN (VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_LUCKY_WHEEL_MINIGAME_WHEEL_BASE_COORDS()) < 5.0)
ENDFUNC

FUNC BOOL IS_INSTANCED_CASINO_MISSION_RUNNING()
	IF NETWORK_IS_ACTIVITY_SESSION()
		RETURN IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_EnableCasinoGame_Lucky7Wheel)
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_ALONE_IN_LUCKY_WHEEL_AREA()
	INT i
	REPEAT NUM_NETWORK_PLAYERS i
		PLAYER_INDEX playerID = INT_TO_PLAYERINDEX(i)
		
		IF IS_NET_PLAYER_OK(playerID)
		AND NOT NETWORK_IS_PLAYER_CONCEALED(playerID)
			IF playerID <> PLAYER_ID()
				PED_INDEX pPed = GET_PLAYER_PED(playerID)
				
				IF DOES_ENTITY_EXIST(pPed)
				AND IS_ENTITY_IN_ANGLED_AREA(pPed, <<1109.469116,228.943192,-50.640408>>, <<1111.214600,228.877731,-48.390408>>, 1.82000)
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

FUNC BOOL BLOCK_LUCKY_WHEEL_MINIGAME(BOOL bCheckDailyLimit = TRUE, BOOL bCheckInLocate = TRUE)
	BOOL bBlock = FALSE
	INT iReason
	
	IF NETWORK_IS_ACTIVITY_SESSION()
		RETURN TRUE
	ENDIF
	
	IF bCheckInLocate
		IF !IS_PLAYER_FACING_THIS_HEADING(8.7587)
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF Is_Ped_Drunk(PLAYER_PED_ID())
		RETURN TRUE
	ENDIF
	
	IF g_SpawnData.bPassedOutDrunk 
		RETURN TRUE
	ENDIF
	
	IF IS_INSTANCED_CASINO_MISSION_RUNNING()
		RETURN TRUE
	ENDIF
	
	IF bCheckInLocate
		IF NOT IS_PLAYER_NEAR_LUCKY_WHEEL_MINIGAME()
			RETURN TRUE
		ENDIF
	ENDIF	
	
	IF SHOULD_ALLOW_CASINO_ENTRY_DURING_MISSION()
		RETURN TRUE
	ENDIF
	
	IF IS_PHONE_ONSCREEN()
		PRINTLN("[LUCKY_WHEEL] BLOCK_LUCKY_WHEEL_MINIGAME phone on screen")
		RETURN TRUE
	ENDIF
	
	IF IS_PAUSE_MENU_ACTIVE()
		PRINTLN("[LUCKY_WHEEL] BLOCK_LUCKY_WHEEL_MINIGAME pause menu active")
		RETURN TRUE
	ENDIF
	
	IF IS_BROWSER_OPEN()
		PRINTLN("[LUCKY_WHEEL] BLOCK_LUCKY_WHEEL_MINIGAME browser open")
		RETURN TRUE
	ENDIF
	
	IF IS_INTERACTION_MENU_OPEN()
		PRINTLN("[LUCKY_WHEEL] BLOCK_LUCKY_WHEEL_MINIGAME interaction menu is open")
		RETURN TRUE
	ENDIF
	
	IF IS_SELECTOR_ONSCREEN()
		PRINTLN("[LUCKY_WHEEL] BLOCK_LUCKY_WHEEL_MINIGAME selector is open")
		RETURN TRUE
	ENDIF
	
	BOOL bIsPlayerInLocate = IS_PLAYER_IN_LUCKY_WHEEL_MINIGAME_LOCATE(FALSE)
	
	IF g_sMPTunables.bVC_CASINO_DISABLE_WHEEL
		IF bIsPlayerInLocate
			IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
			AND NOT IS_HELP_MESSAGE_ON_SCREEN()
			AND NOT IS_BROWSER_OPEN()
				PRINT_HELP("CASINO_LUCK_WD")
			ENDIF	
			#IF IS_DEBUG_BUILD
			IF GET_FRAME_COUNT() % 60 = 0
				PRINTLN("[LUCKY_WHEEL] BLOCK_LUCKY_WHEEL_MINIGAME bVC_CASINO_DISABLE_WHEEL is true")
			ENDIF
			#ENDIF
		ENDIF	
		bBlock = TRUE
	ENDIF	
	
	IF !HAS_PLAYER_PURCHASED_CASINO_MEMBERSHIP(PLAYER_ID())
		IF bIsPlayerInLocate
		AND NOT IS_BIT_SET(g_SimpleInteriorData.iSeventhBS, BS7_SIMPLE_INTERIOR_CASINO_LUCKY_WHEEL_MEMBERSHIP_HELP)
		AND NOT IS_PLAYER_BLOCKED_FROM_CASINO_GAME(CG_BIGWHEEL, iReason)
			IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
			AND NOT IS_HELP_MESSAGE_ON_SCREEN()
			AND NOT IS_BROWSER_OPEN()
				SET_BIT(g_SimpleInteriorData.iSeventhBS, BS7_SIMPLE_INTERIOR_CASINO_LUCKY_WHEEL_MEMBERSHIP_HELP)
				PRINT_HELP_WITH_NUMBER("CAS_MG_MEMB2", GET_CASINO_MEMBERSHIP_COST())
			ENDIF	
			#IF IS_DEBUG_BUILD
			IF GET_FRAME_COUNT() % 60 = 0
				PRINTLN("[LUCKY_WHEEL] BLOCK_LUCKY_WHEEL_MINIGAME player has not casino membership")
			ENDIF
			#ENDIF
		ENDIF	
		bBlock = TRUE
	ENDIF
	
	IF bCheckDailyLimit
		IF HAS_PLAYER_USED_LUCKY_WHEEL_IN_PAST_24_H()
		#IF IS_DEBUG_BUILD
		AND !luckyWheelDebugData.bByPass24HLimit
		#ENDIF
			IF bIsPlayerInLocate
			AND NOT IS_LUCKY_WHEEL_MINIGAME_BIT_SET(BS_LUCKY_WHEEL_USAGE_HELP)
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				AND NOT IS_HELP_MESSAGE_ON_SCREEN()
				AND NOT IS_BROWSER_OPEN()
					IF g_sMPtunables.bVC_LUCKY_WHEEL_ADDITIONAL_SPINS_ENABLE
						SWITCH g_sMPtunables.iVC_LUCKY_WHEEL_NUM_SPINS_PER_DAY
							CASE 2	PRINT_HELP("LUCKY_WHEEL_US1")	BREAK
							CASE 3	PRINT_HELP("LUCKY_WHEEL_US2")	BREAK
							CASE 4	PRINT_HELP("LUCKY_WHEEL_US3")	BREAK
							DEFAULT PRINT_HELP("LUCKY_WHEEL_US")	BREAK
						ENDSWITCH
					ELSE	
						PRINT_HELP("LUCKY_WHEEL_US")
					ENDIF	
					SET_LUCKY_WHEEL_MINIGAME_BIT(BS_LUCKY_WHEEL_USAGE_HELP)
				ENDIF	
				#IF IS_DEBUG_BUILD
				IF GET_FRAME_COUNT() % 60 = 0
					PRINTLN("[LUCKY_WHEEL] BLOCK_LUCKY_WHEEL_MINIGAME player used daily limit")
				ENDIF
				#ENDIF
			ENDIF
			bBlock = TRUE
		ENDIF
	ENDIF
	
	IF bCheckInLocate
		IF bIsPlayerInLocate
			IF !IS_PLAYER_ALONE_IN_LUCKY_WHEEL_AREA()
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("LW_PLAY")
					CLEAR_HELP()
				ENDIF
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				AND NOT IS_HELP_MESSAGE_ON_SCREEN()
				AND NOT IS_BROWSER_OPEN()
					PRINT_HELP("POD_TOO_MANY")
				ENDIF
				
				bBlock = TRUE
			ELSE
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("POD_TOO_MANY")
					CLEAR_HELP()
				ENDIF	
			ENDIF
		ELSE
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("POD_TOO_MANY")
				CLEAR_HELP()
			ENDIF	
		ENDIF
	ENDIF	
	
	IF IS_BIT_SET(luckyWheelData.iPermanentBS, BS_PERMANENT_WHEEL_MOVE_PLAYER_OUT_OF_AREA)
		bBlock = TRUE
		PRINTLN("[LUCKY_WHEEL] BLOCK_LUCKY_WHEEL_MINIGAME player moving out of area")
	ENDIF	
	
	IF IS_BIT_SET(g_simpleInteriorData.iFifthBS, BS5_SIMPLE_INTERIOR_INSTANT_PASS_OUT)
		bBlock = TRUE
		PRINTLN("[LUCKY_WHEEL] BLOCK_LUCKY_WHEEL_MINIGAME player is passing out")
	ENDIF
	
	IF SHOULD_PLAYER_BE_REMOVED_FROM_CASINO_GAME_FOR_BOSS_LAUNCHING_MISSION()
		bBlock = TRUE
		PRINTLN("[LUCKY_WHEEL] BLOCK_LUCKY_WHEEL_MINIGAME player should be removed for boss lunching mission")
	ENDIF
	
	IF IS_PLAYER_BLOCKED_FROM_CASINO_GAME(CG_BIGWHEEL, iReason)
		IF bIsPlayerInLocate
			IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
			AND NOT IS_HELP_MESSAGE_ON_SCREEN()
				PRINT_HELP("CAS_LW_REGL")
				SET_LUCKY_WHEEL_MINIGAME_BIT(BS_LUCKY_WHEEL_REGION_BLOCK_HELP)
			ENDIF	
		ENDIF	
		bBlock = TRUE
		PRINTLN("[LUCKY_WHEEL] BLOCK_LUCKY_WHEEL_MINIGAME IS_PLAYER_BLOCKED_FROM_CASINO_GAME true")
	ENDIF
	
	IF g_iCasinoKickStrikes != 0
		bBlock = TRUE
		PRINTLN("[LUCKY_WHEEL] BLOCK_LUCKY_WHEEL_MINIGAME g_iCasinoKickStrikes: ", g_iCasinoKickStrikes)
	ENDIF
	
	IF NETWORK_IS_IN_MP_CUTSCENE()
		bBlock = TRUE
		PRINTLN("[LUCKY_WHEEL] BLOCK_LUCKY_WHEEL_MINIGAME NETWORK_IS_IN_MP_CUTSCENE TRUE")
	ENDIF
	
	IF IS_FM_JOB_ENTRY_OF_TYPE_THAT_SHOULD_BLOCK_ACCESS()
		PRINTLN("[LUCKY_WHEEL] BLOCK_LUCKY_WHEEL_MINIGAME - Blocked GET_FM_JOB_ENTERY_TYPE() = ", GET_FM_JOB_ENTERY_TYPE())
		RETURN TRUE
	ENDIF
	
	IF SHOULD_ALLOW_CASINO_ENTRY_DURING_MISSION()
		PRINTLN("[LUCKY_WHEEL] BLOCK_LUCKY_WHEEL_MINIGAME - Blocked SHOULD_ALLOW_CASINO_ENTRY_DURING_MISSION TRUE")
		RETURN TRUE
	ENDIF
	
	RETURN bBlock
ENDFUNC

PROC MAINTAIN_REMOTE_PLAYER_START_SPIN_ANIM()
	IF IS_PLAYER_STARTED_SPIN(luckyWheelServerData.minigamePlayer)
		SET_PRIZE_SEGMENT(GET_PLAYER_PRIZE_SEGMENT(luckyWheelServerData.minigamePlayer))
		SET_WHEEL_ANIM_ACTIVATION_TIME(GET_WHEEL_ANIM_ACTIVATION_TIME(luckyWheelServerData.minigamePlayer))
		luckyWheelData.iSegmentTimeMS = GET_SEGMENT_LUCKY_WHEEL_SPIN_TIME()
		luckyWheelData.iFullSpins = GET_TOTAL_LUCKY_WHEEL_SPINS()
		PERFORM_LUCKY_WHEEL_MINIGAME_SYNC_SCENE_FOR_REMOTE_PLAYER()
		SET_LUCKY_WHEEL_FLOW_STATE(LUCKY_WHEEL_FLOW_STATE_REMOTE_START_MANUAL_SPIN)
		
		// url:bugstar:6339917 - Players disconnect immediately after spinning the wheel which makes the landing segment and reward not match.
		// Description: By setting the server spin index here, we don't need to wait until the end to broadcast the event with new starting segment. 
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			luckyWheelServerData.iServerSpinIndex = GET_PLAYER_PRIZE_SEGMENT(luckyWheelServerData.minigamePlayer)
			PRINTLN("[LUCKY_WHEEL] [url:bugstar:6339917] MAINTAIN_REMOTE_PLAYER_START_SPIN_ANIM - luckyWheelServerData.iServerSpinIndex: ", luckyWheelServerData.iServerSpinIndex)
		ENDIF
	ENDIF	
ENDPROC

PROC MAINTAIN_REMOTE_PALYER_MANUAL_SPIN()
	IF HAS_LUCKY_WHEEL_PROP_ANIMATION_CLIP_FINISHED(TRUE)
		SET_LUCKY_WHEEL_MINIGAME_BIT(BS_LUCKY_WHEEL_SPIN_ANIM_COMPLETE)		
	ENDIF

	// Moves onto winner state from inside here
	IF IS_LUCKY_WHEEL_MINIGAME_BIT_SET(BS_LUCKY_WHEEL_SPIN_ANIM_COMPLETE)
	AND DOES_ENTITY_EXIST(luckyWheelData.objLuckyWheel)
	AND DOES_ENTITY_HAVE_DRAWABLE(luckyWheelData.objLuckyWheel)
		PERFORM_LUCKY_WHEEL_MINIGAME_SPIN(TRUE)
	ENDIF
ENDPROC

PROC PROCESS_EVENT_CASINO_LUCKY_WHEEL_SPIN(INT iCount)
	SCRIPT_EVENT_DATA_CASINO_LUCKY_WHEEL_SPIN sEventData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, sEventData, SIZE_OF(sEventData))
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			luckyWheelServerData.iServerSpinIndex = sEventData.iCurrentSpinIndex
			//luckyWheelServerData.minigamePlayer = INT_TO_NATIVE(PLAYER_INDEX, -1)
			PRINTLN("PROCESS_EVENT_CASINO_LUCKY_WHEEL_SPIN luckyWheelServerData.iServerSpinIndex: ", luckyWheelServerData.iServerSpinIndex)
		ENDIF
		IF PLAYER_ID() != sEventData.Details.FromPlayerIndex 
		AND sEventData.bStartSpin
			SET_LUCKY_WHEEL_MINIGAME_BIT(BS_LUCKY_WHEEL_RECEIVED_EVENT_FROM_WHEEL_USER)
			PRINTLN("PROCESS_EVENT_CASINO_LUCKY_WHEEL_SPIN luckyWheelServerData.iServerSpinIndex: ", luckyWheelServerData.iServerSpinIndex, " BS_LUCKY_WHEEL_RECEIVED_EVENT_FROM_WHEEL_USER TRUE")
		ENDIF
	ELSE
		SCRIPT_ASSERT("[LUCKY_WHEEL] - PROCESS_EVENT_CASINO_LUCKY_WHEEL_SPIN - could not retrieve data.")
	ENDIF
ENDPROC

PROC MAINTAIN_LUCKY_WHEEL_EVENTS()
	INT iCount
	EVENT_NAMES ThisScriptEvent
	STRUCT_EVENT_COMMON_DETAILS Details
	
	REPEAT GET_NUMBER_OF_EVENTS(SCRIPT_EVENT_QUEUE_NETWORK) iCount

	    ThisScriptEvent = GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_NETWORK, iCount)
	   	
	    SWITCH ThisScriptEvent
	        
			CASE EVENT_NETWORK_SCRIPT_EVENT
				
				GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, Details, SIZE_OF(Details))
				
				IF Details.Type	= SCRIPT_EVENT_CASINO_LUCKY_WHEEL_SPIN
					IF g_sBlockedEvents.bSCRIPT_EVENT_CASINO_LUCKY_WHEEL_SPIN
						EXIT
					ENDIF
					PROCESS_EVENT_CASINO_LUCKY_WHEEL_SPIN(iCount)
				ENDIF
			BREAK
	    ENDSWITCH
    ENDREPEAT
ENDPROC

PROC MAINTAIN_FLASH_PROP_LIGHTS()
	IF luckyWheelData.iNumTimesLightFlash < 6
	AND luckyWheelData.iNumTimesLightFlash != -1
		IF NOT HAS_NET_TIMER_STARTED(luckyWheelData.sLightSwitchOnTimer)
			TURN_ON_WHEEL_LIGHTS()
			TURN_ON_WHEEL_ARROW_LIGHTS()
			START_NET_TIMER(luckyWheelData.sLightSwitchOnTimer)
		ELSE
			IF HAS_NET_TIMER_EXPIRED(luckyWheelData.sLightSwitchOnTimer, 250)
			AND NOT HAS_NET_TIMER_STARTED(luckyWheelData.sLightSwitchOffTimer)
				TURN_OFF_WHEEL_LIGHTS()
				IF luckyWheelData.iNumTimesLightFlash != 5
					TURN_OFF_WHEEL_ARROW_LIGHTS()
				ENDIF
				START_NET_TIMER(luckyWheelData.sLightSwitchOffTimer)
				luckyWheelData.iNumTimesLightFlash++
			ELIF HAS_NET_TIMER_STARTED(luckyWheelData.sLightSwitchOffTimer)
				IF HAS_NET_TIMER_EXPIRED(luckyWheelData.sLightSwitchOffTimer, 250)
					RESET_NET_TIMER(luckyWheelData.sLightSwitchOffTimer)
					RESET_NET_TIMER(luckyWheelData.sLightSwitchOnTimer)
				ENDIF
			ENDIF
		ENDIF	
	ELIF luckyWheelData.iNumTimesLightFlash = 6
		luckyWheelData.iNumTimesLightFlash = -1
	ENDIF 	
ENDPROC

PROC MAINTAIN_LUCKY_WHEEL_ARM_RAISED_IDLE_STATE()
	MAINTAIN_LUCKY_WHEEL_MINIGAME_IDLE_ANIMATION()
	MAINTAIN_LUCKY_WHEEL_MINIGAME_ANALOGUE_STICK_INPUT()
	
	IF IS_LUCKY_WHEEL_MINIGAME_BIT_SET(BS_LUCKY_WHEEL_PERFORM_IDLE_TRANS_TO_SPIN_IDLE)
		IF HAS_LUCKY_WHEEL_ANIMATION_CLIP_FINISHED(LUCKY_WHEEL_ANIM_CLIP_RAISE_ARM, FALSE, FALSE)
			PERFORM_LUCKY_WHEEL_MINIGAME_SYNC_SCENE(LUCKY_WHEEL_ANIM_CLIP_RAISE_ARM_IDLE, FALSE, TRUE, FALSE, FALSE)
			RESET_NET_TIMER(luckyWheelData.stIdleSafetyTimer)
			CLEAR_LUCKY_WHEEL_MINIGAME_BIT(BS_LUCKY_WHEEL_PERFORM_IDLE_TRANS_TO_SPIN_IDLE)
		ELSE
			IF IS_LUCKY_WHEEL_MINIGAME_BIT_SET(BS_LUCKY_WHEEL_TRIGGERED)
				SET_LUCKY_WHEEL_MINIGAME_BIT(BS_LUCKY_WHEEL_START_FROM_ARM_RAISED_ANIM)
				SET_LUCKY_WHEEL_FLOW_STATE(LUCKY_WHEEL_FLOW_STATE_USER_INPUT)
			ENDIF
		ENDIF
	ELSE
		IF !HAS_LUCKY_WHEEL_ANIMATION_CLIP_FINISHED(LUCKY_WHEEL_ANIM_CLIP_RAISE_ARM_IDLE, FALSE, FALSE)
			IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL) 
				PRINT_HELP("LUCK_W_SPIN_PC")
			ELSE
				PRINT_HELP("LUCK_WHEEL_SPIN")
			ENDIF
			IF IS_LUCKY_WHEEL_MINIGAME_BIT_SET(BS_LUCKY_WHEEL_TRIGGERED)
				RESET_NET_TIMER(luckyWheelData.stIdleSafetyTimer)
				SET_LUCKY_WHEEL_MINIGAME_BIT(BS_LUCKY_WHEEL_START_FROM_ARM_RAISED_ANIM)
				SET_LUCKY_WHEEL_FLOW_STATE(LUCKY_WHEEL_FLOW_STATE_USER_INPUT)
			ENDIF
		ELSE	
			IF GET_LUCKY_WHEEL_FLOW_STATE() != LUCKY_WHEEL_FLOW_STATE_RAISED_IDLE_TO_IDLE
				IF !IS_LUCKY_WHEEL_MINIGAME_LEFT_ANALOGUE_STICK_UP()
				OR IS_LUCKY_WHEEL_MINIGAME_BIT_SET(BS_LUCKY_WHEEL_STICK_UP_LIMIT_REACHED)
					PERFORM_LUCKY_WHEEL_MINIGAME_SYNC_SCENE(LUCKY_WHEEL_ANIM_CLIP_ARM_RAISED_TO_IDLE, FALSE, FALSE, FALSE, TRUE)
					SET_LUCKY_WHEEL_FLOW_STATE(LUCKY_WHEEL_FLOW_STATE_RAISED_IDLE_TO_IDLE)
				ENDIF	
			ENDIF	
		ENDIF
	
	ENDIF
ENDPROC

PROC MAINTAIN_LUCKY_WHEEL_IDLE_STATE()
	IF NOT IS_LUCKY_WHEEL_MINIGAME_BIT_SET(BS_LUCKY_WHEEL_SET_ACTIVATION_TIMER)
		luckyWheelData.tdActivationTime = GET_TIME_OFFSET(GET_NETWORK_TIME(), ciLUCKY_WHEEL_ACTIVATION_TIME_MS)
		SET_LUCKY_WHEEL_MINIGAME_BIT(BS_LUCKY_WHEEL_SET_ACTIVATION_TIMER)
	ENDIF
	
	MAINTAIN_LUCKY_WHEEL_MINIGAME_IDLE_ANIMATION()
	MAINTAIN_LUCKY_WHEEL_MINIGAME_ANALOGUE_STICK_INPUT()
	IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL) 
		PRINT_HELP("LUCK_W_SPIN_PC")
	ELSE
		PRINT_HELP("LUCK_WHEEL_SPIN")
	ENDIF
	
	IF IS_LUCKY_WHEEL_MINIGAME_BIT_SET(BS_LUCKY_WHEEL_TRIGGERED)
	AND !SHOULD_BAIL_LUCKY_WHEEL()
		PERFORM_LUCKY_WHEEL_MINIGAME_SYNC_SCENE(LUCKY_WHEEL_ANIM_CLIP_RAISE_ARM, FALSE, FALSE, FALSE)
		SET_LUCKY_WHEEL_MINIGAME_BIT(BS_LUCKY_WHEEL_PERFORM_IDLE_TRANS_TO_SPIN_IDLE)
		RESET_NET_TIMER(luckyWheelData.stIdleSafetyTimer)
		SET_LUCKY_WHEEL_FLOW_STATE(LUCKY_WHEEL_FLOW_STATE_RAISED_IDLE)
		CLEAR_LUCKY_WHEEL_MINIGAME_BIT(BS_LUCKY_WHEEL_TRIGGERED)
	ELIF IS_LUCKY_WHEEL_MINIGAME_BIT_SET(BS_LUCKY_WHEEL_IDLE_TIMED_OUT)
	OR SHOULD_BAIL_LUCKY_WHEEL()
		//IF IS_LUCKY_WHEEL_ANIMATION_CLIP_PLAYING(luckyWheelData.eIdleClip)
			PERFORM_LUCKY_WHEEL_MINIGAME_SYNC_SCENE(LUCKY_WHEEL_ANIM_CLIP_EXIT, FALSE, FALSE)
			CLEAR_LUCKY_WHEEL_MINIGAME_BIT(BS_LUCKY_WHEEL_SET_ACTIVATION_TIMER)
			SET_LUCKY_WHEEL_MINIGAME_BIT(BS_LUCKY_WHEEL_PERFORM_EXIT_ANIM)
			SET_LUCKY_WHEEL_FLOW_STATE(LUCKY_WHEEL_FLOW_STATE_TIMED_OUT)
		//ENDIF
	ENDIF
ENDPROC 

FUNC FLOAT GET_WHEEL_START_ROTATION(INT iIndex)
	SWITCH iIndex
		CASE 0			RETURN    0.0 BREAK
		CASE 1			RETURN    18.0 BREAK
		CASE 2			RETURN    36.0 BREAK
		CASE 3			RETURN    54.0 BREAK
		CASE 4			RETURN    72.0 BREAK
		CASE 5			RETURN    90.0 BREAK
		CASE 6			RETURN    108.0 BREAK
		CASE 7			RETURN    126.0 BREAK
		CASE 8			RETURN    144.0 BREAK
		CASE 9			RETURN    162.0 BREAK
		CASE 10			RETURN    180.0 BREAK
		CASE 11			RETURN    198.0 BREAK
		CASE 12			RETURN    216.0 BREAK
		CASE 13			RETURN    234.0 BREAK
		CASE 14			RETURN    252.0 BREAK
		CASE 15			RETURN    270.0 BREAK
		CASE 16			RETURN    288.0 BREAK
		CASE 17			RETURN    306.0 BREAK
		CASE 18			RETURN    324.0 BREAK
		CASE 19			RETURN    342.0 BREAK
	ENDSWITCH
	RETURN 0.0
ENDFUNC

FUNC BOOL SHOULD_START_CAMERA_UPDATE()
	
	IF (luckyWheelData.eState >= LUCKY_WHEEL_FLOW_STATE_IDLE
	AND luckyWheelData.eState <= LUCKY_WHEEL_FLOW_STATE_TIMED_OUT)
		RETURN TRUE
	ENDIF	
	
	IF luckyWheelData.eState = LUCKY_WHEEL_FLOW_STATE_NET_DELAY
		RETURN TRUE
	ENDIF	
	
	IF luckyWheelData.eState = LUCKY_WHEEL_FLOW_STATE_ENTRY_ANIM
		IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) <> PERFORMING_TASK
		AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) <> WAITING_TO_START_TASK 
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_LUCKY_WHEEL_CAMERA()
	IF SHOULD_START_CAMERA_UPDATE()
		SET_TABLE_GAMES_CAMERA_THIS_UPDATE(TABLE_GAMES_CAMERA_TYPE_LUCKY_WHEEL)
	ENDIF
ENDPROC	

PROC CLEAR_LUCKY_WHEEL_HELP()
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("LW_PLAY")
		CLEAR_HELP()
	ENDIF
	IF NOT IS_PLAYER_IN_LUCKY_WHEEL_MINIGAME_LOCATE(FALSE)
		CLEAR_LUCKY_WHEEL_MINIGAME_BIT(BS_LUCKY_WHEEL_PLAYER_INSIDE_LOCATE)
		IF IS_BIT_SET(g_SimpleInteriorData.iSeventhBS, BS7_SIMPLE_INTERIOR_CASINO_LUCKY_WHEEL_MEMBERSHIP_HELP)
			IF IS_THIS_HELP_MESSAGE_WITH_NUMBER_BEING_DISPLAYED("CAS_MG_MEMB2", GET_CASINO_MEMBERSHIP_COST())
				CLEAR_HELP()
			ENDIF	
			CLEAR_BIT(g_SimpleInteriorData.iSeventhBS, BS7_SIMPLE_INTERIOR_CASINO_LUCKY_WHEEL_MEMBERSHIP_HELP)
		ENDIF	
		IF IS_LUCKY_WHEEL_MINIGAME_BIT_SET(BS_LUCKY_WHEEL_USAGE_HELP)
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("LUCKY_WHEEL_US")
			OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("LUCKY_WHEEL_US1")
			OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("LUCKY_WHEEL_US2")
			OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("LUCKY_WHEEL_US3")
				CLEAR_HELP()
			ENDIF	
			CLEAR_LUCKY_WHEEL_MINIGAME_BIT(BS_LUCKY_WHEEL_USAGE_HELP)
		ENDIF
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CASINO_LUCK_WD")
			CLEAR_HELP()
		ENDIF
		IF IS_LUCKY_WHEEL_MINIGAME_BIT_SET(BS_LUCKY_WHEEL_BANNED_HELP)
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CAS_MG_CBAN")
				CLEAR_HELP()
				CLEAR_LUCKY_WHEEL_MINIGAME_BIT(BS_LUCKY_WHEEL_BANNED_HELP)
			ENDIF
		ENDIF	
		IF IS_LUCKY_WHEEL_MINIGAME_BIT_SET(BS_LUCKY_WHEEL_REGION_BLOCK_HELP)
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CAS_LW_REGL")
				CLEAR_HELP()
				CLEAR_LUCKY_WHEEL_MINIGAME_BIT(BS_LUCKY_WHEEL_REGION_BLOCK_HELP)
			ENDIF
		ENDIF	
	ENDIF
ENDPROC

PROC MAINTAIN_LUCKY_WHEEL_RAISED_IDLE_TO_IDLE_STATE()
	IF HAS_LUCKY_WHEEL_ANIMATION_CLIP_FINISHED(LUCKY_WHEEL_ANIM_CLIP_ARM_RAISED_TO_IDLE, FALSE, FALSE)
		CLEAR_LUCKY_WHEEL_MINIGAME_BIT(BS_LUCKY_WHEEL_TRIGGERED)
		CLEAR_LUCKY_WHEEL_MINIGAME_BIT(BS_LUCKY_WHEEL_SET_ACTIVATION_TIMER)
		RESET_NET_TIMER(luckyWheelData.stIdleSafetyTimer)
		PERFORM_LUCKY_WHEEL_MINIGAME_SYNC_SCENE(LUCKY_WHEEL_ANIM_CLIP_IDLE_1, FALSE, TRUE, FALSE)
		SET_LUCKY_WHEEL_FLOW_STATE(LUCKY_WHEEL_FLOW_STATE_IDLE)
	ELSE
		PRINTLN("[LUCKY_WHEEL] MAINTAIN_LUCKY_WHEEL_RAISED_IDLE_TO_IDLE_STATE - waiting for transition anim to finish")
	ENDIF
ENDPROC

PROC MAINTAIN_WHEEL_SPEED()
	CONST_FLOAT fHighMaxSpeed 7.0
	INT iBone = GET_ENTITY_BONE_INDEX_BY_NAME(luckyWheelData.objLuckyWheel, "Base_pivot")
	VECTOR vBoneRot = GET_ENTITY_BONE_OBJECT_ROTATION(luckyWheelData.objLuckyWheel, iBone)

	// 0=180 / -180=0 convert to 0-360.
	FLOAT fBoneTsAngle = OE_TO_TS(vBoneRot.y)
	
	VECTOR vWheelRot = GET_ENTITY_ROTATION(luckyWheelData.objLuckyWheel)
	FLOAT fWheelTsAngle = OE_TO_TS(vWheelRot.y)

	FLOAT fCurrentRotation// = fBoneTsAngle + fWheelTsAngle
	
	IF !HAS_LUCKY_WHEEL_PROP_ANIMATION_CLIP_FINISHED()
		fCurrentRotation = fBoneTsAngle
	ELSE
		IF fWheelTsAngle  = 0.0 AND fBoneTsAngle != 0.0
			fCurrentRotation = fBoneTsAngle
		ELSE	
			fCurrentRotation = fWheelTsAngle
		ENDIF	
	ENDIF
	
	IF !IS_LUCKY_WHEEL_MINIGAME_BIT_SET(BS_LUCKY_WHEEL_GET_SPEED_ON_FIRST_FRAME)
		luckyWheelData.fPreviousRot = fCurrentRotation
		SET_LUCKY_WHEEL_MINIGAME_BIT(BS_LUCKY_WHEEL_GET_SPEED_ON_FIRST_FRAME)
	ENDIF	
	
	FLOAT fAnglerPerFrame = ABSF(fCurrentRotation - luckyWheelData.fPreviousRot)
	
	IF fAnglerPerFrame > 100
		fAnglerPerFrame = ABSF(fCurrentRotation - (360 + luckyWheelData.fPreviousRot))
	ENDIF
	
	INT iAlpha = ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), luckyWheelData.tdTotalLerpTime))
	
	// Value 0.0-1.0
	FLOAT fNormalisedStartAlpha = TO_FLOAT(iAlpha) / luckyWheelData.iDuration
	
	IF fNormalisedStartAlpha < 0.0
		fNormalisedStartAlpha = 0.0
	ELIF fNormalisedStartAlpha > 1.0
		fNormalisedStartAlpha = 1.0
	ENDIF	
	
	IF GET_LUCKY_WHEEL_SPIN_INTENSITY() = LUCKY_WHEEL_SPIN_INTENSITY_LOW
		fNormalisedStartAlpha /= 2
	ENDIF
		
	luckyWheelData.fStartSoundSpeed = fNormalisedStartAlpha
	
	// If we moved a segment play the tick sound 
	IF fAnglerPerFrame >= 18
		SET_LUCKY_WHEEL_MINIGAME_BIT(BS_LUCKY_WHEEL_PLAY_TICK)
		luckyWheelData.fPreviousRot = fCurrentRotation
	ELSE
		CLEAR_LUCKY_WHEEL_MINIGAME_BIT(BS_LUCKY_WHEEL_PLAY_TICK)
	ENDIF
	
	luckyWheelData.fStartSoundSpeed = fNormalisedStartAlpha

ENDPROC

PROC GET_RANDOM_COORDS_AROUND_LUCKY_WHEEL(INT iIndex, VECTOR &vCoords)
	SWITCH iIndex
		CASE 0		vCoords = <<1114.8124, 226.8861, -50.8408>>		BREAK		//	fHeading = 54.0163			
		CASE 1		vCoords = <<1115.3910, 222.8374, -50.4301>>		BREAK		//	fHeading = 38.2561			
		CASE 2		vCoords = <<1111.7622, 223.4791, -50.8408>>		BREAK		//	fHeading = 13.5835			
		CASE 3		vCoords = <<1110.2871, 222.9306, -50.8408>>		BREAK		//	fHeading = 332.3859			
		CASE 4		vCoords = <<1108.9568, 223.4897, -50.8408>>		BREAK		//	fHeading = 327.9482			
		CASE 5		vCoords = <<1106.0417, 226.8098, -50.8408>>		BREAK		//	fHeading = 295.2900			
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Moves player out of lucky wheel area
/// PARAMS:
///    bForceCurrentPlayer - force local player to move
PROC MAINTAIN_MOVE_PLAYER_OUT_OF_WHEEL_AREA(BOOL bForceCurrentPlayer = FALSE)
	IF luckyWheelServerData.minigamePlayer != INVALID_PLAYER_INDEX()
	OR bForceCurrentPlayer = TRUE
		IF luckyWheelServerData.minigamePlayer != PLAYER_ID()
		OR bForceCurrentPlayer = TRUE
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1109.159668,228.448364,-50.630825>>, <<1112.186401,228.984085,-48.130825>>, 2.750000)
				IF NOT IS_BIT_SET(luckyWheelData.iPermanentBS, BS_PERMANENT_WHEEL_MOVE_PLAYER_OUT_OF_AREA)
					VECTOR vCoords
					INT iIndex = GET_RANDOM_INT_IN_RANGE(0, 6)
					GET_RANDOM_COORDS_AROUND_LUCKY_WHEEL(iIndex, vCoords)
					TASK_FOLLOW_NAV_MESH_TO_COORD(PLAYER_PED_ID(), vCoords, PEDMOVE_WALK, -1)
					SET_BIT(luckyWheelData.iPermanentBS, BS_PERMANENT_WHEEL_MOVE_PLAYER_OUT_OF_AREA)
				ENDIF	
			ELSE
				IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) <> PERFORMING_TASK
				AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) <> WAITING_TO_START_TASK
					IF IS_BIT_SET(luckyWheelData.iPermanentBS, BS_PERMANENT_WHEEL_MOVE_PLAYER_OUT_OF_AREA)
						CLEAR_BIT(luckyWheelData.iPermanentBS, BS_PERMANENT_WHEEL_MOVE_PLAYER_OUT_OF_AREA)
					ENDIF
				ENDIF	
			ENDIF
		ELSE
			IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) <> PERFORMING_TASK
			AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) <> WAITING_TO_START_TASK
				IF IS_BIT_SET(luckyWheelData.iPermanentBS, BS_PERMANENT_WHEEL_MOVE_PLAYER_OUT_OF_AREA)
					CLEAR_BIT(luckyWheelData.iPermanentBS, BS_PERMANENT_WHEEL_MOVE_PLAYER_OUT_OF_AREA)
				ENDIF
			ENDIF	
		ENDIF
	ELSE
		IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) <> PERFORMING_TASK
		AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) <> WAITING_TO_START_TASK
			IF IS_BIT_SET(luckyWheelData.iPermanentBS, BS_PERMANENT_WHEEL_MOVE_PLAYER_OUT_OF_AREA)
				CLEAR_BIT(luckyWheelData.iPermanentBS, BS_PERMANENT_WHEEL_MOVE_PLAYER_OUT_OF_AREA)
			ENDIF
		ENDIF	
	ENDIF		
ENDPROC

PROC MAINTAIN_MOVE_PLAYER_IF_IDLE_NEAR_WHEEL()
	IF  MPGlobalsHud.iHowLongIdling >= 30000
		IF luckyWheelServerData.minigamePlayer != PLAYER_ID()
			MAINTAIN_MOVE_PLAYER_OUT_OF_WHEEL_AREA(TRUE)
		ENDIF
	ENDIF
ENDPROC

PROC SETUP_CASINO_LUCKY_WHEEL_DISCLAIMER_MENU_CONTROLS(DISCLAIMER_MENU &sDisclaimerMenu)

	GET_CONTROL_VALUE_OF_ANALOGUE_STICKS(sDisclaimerMenu.iLeftX, sDisclaimerMenu.iLeftY, sDisclaimerMenu.iRightX, sDisclaimerMenu.iRightY)
	
	// Set the input flag states
	sDisclaimerMenu.bAccept		= (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT) OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT))
	sDisclaimerMenu.bBack		 	= (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL) OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL))
	sDisclaimerMenu.bPrevious		= ((sDisclaimerMenu.iLeftY < -64) OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)) 
	sDisclaimerMenu.bNext			= ((sDisclaimerMenu.iLeftY > 64) OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN))	
	
	HIDE_HUD_AND_RADAR_THIS_FRAME()
	
	IF LOAD_MENU_ASSETS()
	AND GET_PAUSE_MENU_STATE() = PM_INACTIVE
	AND NOT IS_SYSTEM_UI_BEING_DISPLAYED()
	AND NOT IS_WARNING_MESSAGE_ACTIVE()
	AND NOT g_sShopSettings.bProcessStoreAlert

		// Mouse control support  
		IF IS_PC_VERSION()	
		AND NOT NETWORK_TEXT_CHAT_IS_TYPING()
			IF IS_USING_CURSOR(FRONTEND_CONTROL)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_LR)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_UD)
				DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)	
				ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CURSOR_ACCEPT)
				ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CURSOR_CANCEL)
				ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CURSOR_SCROLL_UP)
				ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CURSOR_SCROLL_DOWN)
				IF ((g_iMenuCursorItem = MENU_CURSOR_NO_ITEM) OR (g_iMenuCursorItem = MENU_CURSOR_NO_CAMERA_MOVE) OR (g_iMenuCursorItem = MENU_CURSOR_DRAG_CAM))
					ENABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_LOOK_LR)
					ENABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_LOOK_UD)
				ELSE
					DISABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_LOOK_LR)
					DISABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_LOOK_UD)
				ENDIF	
				HANDLE_CURSOR_HIGHLIGHTED_MENU_ITEMS()
				HANDLE_MENU_CURSOR(FALSE)
			ENDIF
			
			// Mouse select
			IF IS_MENU_CURSOR_ACCEPT_PRESSED()
				IF g_iMenuCursorItem = sDisclaimerMenu.iCurrentMenuItem
					sDisclaimerMenu.bAccept = TRUE
				ELSE
					sDisclaimerMenu.iCurrentMenuItem = g_iMenuCursorItem
					SET_CURRENT_MENU_ITEM(sDisclaimerMenu.iCurrentMenuItem)		
				ENDIF		
			ENDIF
		
			// Menu cursor back
			IF IS_MENU_CURSOR_CANCEL_PRESSED()
				sDisclaimerMenu.bBack = TRUE
			ENDIF
			
			// Mouse scroll up
			IF IS_MENU_CURSOR_SCROLL_UP_PRESSED()
				sDisclaimerMenu.bPrevious = TRUE
			ENDIF
			
			// Mouse scroll down
			IF IS_MENU_CURSOR_SCROLL_DOWN_PRESSED()
				sDisclaimerMenu.bNext = TRUE
			ENDIF
			
		ENDIF
	ENDIF
	
ENDPROC

PROC MAINTAIN_PLAYER_CONTROLS(DISCLAIMER_MENU &sDisclaimerMenu)
	IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) <> PERFORMING_TASK
		IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
			CLEAR_PED_TASKS(PLAYER_PED_ID())
			NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_LEAVE_CAMERA_CONTROL_ON|NSPC_FREEZE_POSITION)
		ENDIF
	ENDIF		

	SETUP_CASINO_LUCKY_WHEEL_DISCLAIMER_MENU_CONTROLS(sDisclaimerMenu)

ENDPROC

PROC MAINTAIN_BUILD_MENU()
	CLEAR_MENU_DATA()
	SET_MENU_TITLE("CAS_BLP_DT")
	SET_MENU_ITEM_JUSTIFICATION(FONT_LEFT, FONT_RIGHT)
	SET_MENU_ITEM_LAYOUT(MENU_ITEM_TEXT, MENU_ITEM_TEXT)
	
	PLAY_SOUND_FRONTEND(-1, "DLC_VW_RULES", "dlc_vw_table_games_frontend_sounds")
	SET_MENU_USES_HEADER_GRAPHIC(TRUE, "CasinoUI_Lucky_Wheel", "CasinoUI_Lucky_Wheel")
	
	ADD_MENU_HELP_KEY_INPUT(INPUT_FRONTEND_CANCEL, "TCP_EXIT")
	ADD_MENU_HELP_KEY_INPUT(INPUT_FRONTEND_ACCEPT, "TCP_CONT")
ENDPROC

PROC UPDATE_CASINO_DECORATION_MENU()
	
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
	IF !IS_WARNING_MESSAGE_ACTIVE()
		SET_INPUT_EXCLUSIVE(PLAYER_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
		SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
	ENDIF
	
	IF luckyWheelData.sDisclaimerMenu.bBack
	OR HAS_NET_TIMER_EXPIRED(luckyWheelData.sDisclaimerMenuTimeOut, 20000)
	OR BLOCK_LUCKY_WHEEL_MINIGAME()
		PLAY_SOUND_FRONTEND(-1, "DLC_VW_CONTINUE", "dlc_vw_table_games_frontend_sounds")
		CLEAR_BIT(luckyWheelData.iPermanentBS, BS_PERMANENT_LUCKY_WHEEL_DISCLAIMER_MENU_SHOWN)
		PRINTLN("[LUCKY_WHEEL] UPDATE_CASINO_DECORATION_MENU - BS_PERMANENT_LUCKY_WHEEL_DISCLAIMER_MENU_SHOWN FALSE")
		SET_LUCKY_WHEEL_FLOW_STATE(LUCKY_WHEEL_FLOW_STATE_CLEANUP)
		MAINTAIN_MOVE_PLAYER_OUT_OF_WHEEL_AREA(TRUE)
	ELIF luckyWheelData.sDisclaimerMenu.bAccept	
		PLAY_SOUND_FRONTEND(-1, "DLC_VW_CONTINUE", "dlc_vw_table_games_frontend_sounds")
		SET_LUCKY_WHEEL_FLOW_STATE(LUCKY_WHEEL_FLOW_STATE_GO_TO_ENTRY_COORDS)
		SET_BIT(luckyWheelData.iPermanentBS, BS_PERMANENT_LUCKY_WHEEL_DISCLAIMER_MENU_SHOWN)
		PRINTLN("[LUCKY_WHEEL] UPDATE_CASINO_DECORATION_MENU - BS_PERMANENT_LUCKY_WHEEL_DISCLAIMER_MENU_SHOWN TRUE")
	ENDIF
ENDPROC

PROC MAINTAIN_DISCLAIMER_MENU()
	MAINTAIN_PLAYER_CONTROLS(luckyWheelData.sDisclaimerMenu)
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
	IF LOAD_MENU_ASSETS()
		IF IS_CASINO_LUCKY_WHEEL_DISCLAIMER_MENU_BS_SET(BS_DISCLAIMER_MENU_REBUILD_DISCLAIMER_MENU)
			MAINTAIN_BUILD_MENU()
			CLEAR_CASINO_LUCKY_WHEEL_DISCLAIMER_MENU_BS_SET(BS_DISCLAIMER_MENU_REBUILD_DISCLAIMER_MENU)
		ELSE
			SET_CURRENT_MENU_ITEM(-1)
		ENDIF
		
		IF !HAS_PLAYER_WON_PODIUM_VEHICLE_IN_PAST_WEEK()
			SET_CURRENT_MENU_ITEM_DESCRIPTION("CAS_DIS_MAIN")
		ELSE
			SET_CURRENT_MENU_ITEM_DESCRIPTION("CAS_DIS_MAIN2")
		ENDIF
		
		UPDATE_CASINO_DECORATION_MENU()
		
		DRAW_MENU()
	ENDIF		
ENDPROC

PROC MAINTAIN_LUCKY_WHEEL_USAGE_STAT()
	IF USE_SERVER_TRANSACTIONS()
		INT iInventoryItem[1], iInventoryValue[1]
		iInventoryItem[0] =  HASH("VW_LUCKY_WHEEL_USAGE")
		iInventoryValue[0] = luckyWheelData.iGivenRewardPosixTime + LUCKY_WHEEL_USAGE_POSIX
		IF PROCESS_UPDATE_STORAGE_DATA_TRANSACTION(luckyWheelData.eTransactionResult, iInventoryItem, iInventoryValue)
			IF luckyWheelData.eTransactionResult = TRANSACTION_STATE_SUCCESS
				SET_LUCKY_WHEEL_USAGE_TIME()
				SET_LUCKY_WHEEL_FLOW_STATE(LUCKY_WHEEL_FLOW_STATE_CLEANUP)
			ELSE
				PRINTLN("[LUCKY_WHEEL] MAINTAIN_LUCKY_WHEEL_USAGE_STAT - waiting for transaction results: ", luckyWheelData.eTransactionResult)
			ENDIF
			
			luckyWheelData.eTransactionResult = TRANSACTION_STATE_DEFAULT
		ELSE
			PRINTLN("[LUCKY_WHEEL] MAINTAIN_LUCKY_WHEEL_USAGE_STAT - PROCESS_UPDATE_STORAGE_DATA_TRANSACTION false!")
		ENDIF
	ELSE
		SET_LUCKY_WHEEL_USAGE_TIME()
		SET_LUCKY_WHEEL_FLOW_STATE(LUCKY_WHEEL_FLOW_STATE_CLEANUP)
	ENDIF	
ENDPROC

/// PURPOSE: Maintain function for the lucky wheel minigame
PROC MAINTAIN_LUCKY_WHEEL_MINIGAME()
	
	SWITCH luckyWheelData.eState
		CASE LUCKY_WHEEL_FLOW_STATE_INITAILISE
			RELEASE_ALL_WHEEL_SOUNDS_WHEN_FINISHED()
		
			IF luckyWheelData.iNumTimesLightFlash = -1
				IF NOT IS_LUCKY_WHEEL_MINIGAME_BIT_SET(BS_LUCKY_WHEEL_TURN_OFF_WHEEL_LIGHTS_ON_INIT)
					TURN_OFF_WHEEL_LIGHTS()
					SET_LUCKY_WHEEL_MINIGAME_BIT(BS_LUCKY_WHEEL_TURN_OFF_WHEEL_LIGHTS_ON_INIT)
				ENDIF	
			ENDIF	

			IF NOT IS_LUCKY_WHEEL_MINIGAME_ACTIVE()
				IF NOT IS_LUCKY_WHEEL_MINIGAME_BIT_SET(BS_LUCKY_WHEEL_ACTIVATED)
				AND NOT BLOCK_LUCKY_WHEEL_MINIGAME()
					MAINTAIN_LUCKY_WHEEL_MINIGAME_HELP_TEXT()
					IF HAS_PLAYER_ACTIVATED_LUCKY_WHEEL_MINIGAME()
						SET_PLAYER_REQUESTING_TO_PLAY_LUCKY_WHEEL_MINIGAME(PLAYER_ID(), TRUE)
						CLEAR_LUCKY_WHEEL_MINIGAME_HELP_TEXT()
						INITIALISE_LUCKY_WHEEL_MINIGAME()
						SET_LUCKY_WHEEL_MINIGAME_BIT(BS_LUCKY_WHEEL_ACTIVATED)
						SET_LUCKY_WHEEL_FLOW_STATE(LUCKY_WHEEL_FLOW_STATE_DETERMINE_PLAYER)
					ENDIF
				ELSE
					CLEAR_LUCKY_WHEEL_HELP()
				ENDIF
			ENDIF
			
			IF NOT IS_BIT_SET(luckyWheelData.iPermanentBS, BS_PERMANENT_WHEEL_SYNC_SEGMENT_ON_ENTRY)
			AND DOES_ENTITY_EXIST(luckyWheelData.objLuckyWheel)
				IF luckyWheelServerData.iServerSpinIndex != luckyWheelData.iCurrentSegmentIndex
				AND luckyWheelServerData.minigamePlayer = INVALID_PLAYER_INDEX()
					SET_ENTITY_ROTATION(luckyWheelData.objLuckyWheel, <<0.0, -GET_WHEEL_START_ROTATION(luckyWheelServerData.iServerSpinIndex), 0.0>>)
					luckyWheelData.iCurrentSegmentIndex = luckyWheelServerData.iServerSpinIndex
					SET_BIT(luckyWheelData.iPermanentBS, BS_PERMANENT_WHEEL_SYNC_SEGMENT_ON_ENTRY)
					PRINTLN("[LUCKY_WHEEL] MAINTAIN_LUCKY_WHEEL_MINIGAME - luckyWheelServerData.iServerSpinIndex: ", luckyWheelServerData.iServerSpinIndex)
				ELSE
					IF luckyWheelServerData.minigamePlayer  = PLAYER_ID()
						SET_BIT(luckyWheelData.iPermanentBS, BS_PERMANENT_WHEEL_SYNC_SEGMENT_ON_ENTRY)
					ENDIF	
				ENDIF
			ENDIF	
		BREAK
		CASE LUCKY_WHEEL_FLOW_STATE_DETERMINE_PLAYER
			IF luckyWheelServerData.minigamePlayer = PLAYER_ID()
				SET_PLAYER_IS_USING_THE_LUCKY_WHEEL(PLAYER_ID(), TRUE)
				SET_CASINO_LUCKY_WHEEL_DISCLAIMER_MENU_BS_SET(BS_DISCLAIMER_MENU_REBUILD_DISCLAIMER_MENU)
				IF NOT IS_BIT_SET(luckyWheelData.iPermanentBS, BS_PERMANENT_LUCKY_WHEEL_DISCLAIMER_MENU_SHOWN)
					START_NET_TIMER(luckyWheelData.sDisclaimerMenuTimeOut)
					SET_LUCKY_WHEEL_FLOW_STATE(LUCKY_WHEEL_FLOW_STATE_DISCLAIMER_MENU)
				ELSE
					SET_LUCKY_WHEEL_FLOW_STATE(LUCKY_WHEEL_FLOW_STATE_GO_TO_ENTRY_COORDS)
				ENDIF
				PRINTLN("[LUCKY_WHEEL] MAINTAIN_LUCKY_WHEEL_MINIGAME - I am the minigame player, continue.")
			ELSE
				IF HAS_NET_TIMER_EXPIRED(luckyWheelData.stUpdateServerBDTimer, ciLUCKY_WHEEL_UPDATE_SERVER_BD_TIME_MS)
					SET_LUCKY_WHEEL_FLOW_STATE(LUCKY_WHEEL_FLOW_STATE_CLEANUP)
					PRINTLN("[LUCKY_WHEEL] MAINTAIN_LUCKY_WHEEL_MINIGAME - I am not the minigame player, cleanup.")
				ENDIF
			ENDIF
		BREAK
		CASE LUCKY_WHEEL_FLOW_STATE_DISCLAIMER_MENU
			MAINTAIN_DISCLAIMER_MENU()
		BREAK
		CASE LUCKY_WHEEL_FLOW_STATE_GO_TO_ENTRY_COORDS
			MAINTAIN_LUCKY_WHEEL_MINIGAME_ENTRY_ANIMATION()
			IF IS_LUCKY_WHEEL_MINIGAME_BIT_SET(BS_LUCKY_WHEEL_GO_TO_COORDS_TASK_SET)
				SET_LUCKY_WHEEL_FLOW_STATE(LUCKY_WHEEL_FLOW_STATE_ENTRY_ANIM)
			ENDIF
		BREAK
		CASE LUCKY_WHEEL_FLOW_STATE_ENTRY_ANIM
			MAINTAIN_LUCKY_WHEEL_MINIGAME_ENTRY_ANIMATION()
			IF IS_LUCKY_WHEEL_MINIGAME_BIT_SET(BS_LUCKY_WHEEL_READY_TO_SPIN_WHEEL)				
				SET_LUCKY_WHEEL_FLOW_STATE(LUCKY_WHEEL_FLOW_STATE_IDLE)
			ENDIF
		BREAK
		CASE LUCKY_WHEEL_FLOW_STATE_IDLE
			MAINTAIN_LUCKY_WHEEL_IDLE_STATE()
		BREAK
		CASE LUCKY_WHEEL_FLOW_STATE_RAISED_IDLE_TO_IDLE
			MAINTAIN_LUCKY_WHEEL_RAISED_IDLE_TO_IDLE_STATE()
		BREAK
		CASE LUCKY_WHEEL_FLOW_STATE_RAISED_IDLE
			MAINTAIN_LUCKY_WHEEL_ARM_RAISED_IDLE_STATE()
		BREAK
		CASE LUCKY_WHEEL_FLOW_STATE_USER_INPUT
			MAINTAIN_LUCKY_WHEEL_MINIGAME_ANALOGUE_STICK_INPUT()
			MAINTAIN_LUCKY_WHEEL_USER_INPUT()
			IF IS_LUCKY_WHEEL_MINIGAME_BIT_SET(BS_LUCKY_WHEEL_SET_WHEEL_SPIN_PROPERTIES)
				luckyWheelData.iSpinStartIndex = luckyWheelServerData.iServerSpinIndex
				PRINTLN("[LUCKY_WHEEL] MAINTAIN_LUCKY_WHEEL_MINIGAME luckyWheelData.iSpinStartIndex: ", luckyWheelData.iSpinStartIndex)
				IF !IS_PLAYER_STARTED_SPIN(PLAYER_ID())
					luckyWheelData.iGivenRewardPosixTime = GET_CLOUD_TIME_AS_INT()
					BROADCAST_CASINO_LUCKY_WHEEL_SPIN(luckyWheelData.iSpinStartIndex, TRUE)
					SET_LUCKY_WHEEL_MINIGAME_BIT(BS_LUCKY_WHEEL_TRIGGER_USAGE_STAT)
					SET_PLAYER_STARTED_SPIN(TRUE)
				ENDIF
				START_NET_TIMER(luckyWheelData.sNetDelay)
				SET_LUCKY_WHEEL_FLOW_STATE(LUCKY_WHEEL_FLOW_STATE_NET_DELAY)					
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("LUCK_WHEEL_SPIN")
				OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("LUCK_W_SPIN_PC")
					CLEAR_HELP()
				ENDIF	
			ENDIF
		BREAK
		CASE LUCKY_WHEEL_FLOW_STATE_NET_DELAY
			IF HAS_NET_TIMER_STARTED(luckyWheelData.sNetDelay)
				IF HAS_NET_TIMER_EXPIRED(luckyWheelData.sNetDelay, 190)
					PERFORM_LUCKY_WHEEL_MINIGAME_SYNC_SCENE(GET_LUCKY_WHEEL_MINIGAME_SPIN_ANIMATION_ENUM(GET_SPIN_INTENSITY(PLAYER_ID())), FALSE, FALSE)
					RESET_NET_TIMER(luckyWheelData.sNetDelay)
					SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_AM_SPIN_THE_WHEEL)
					luckyWheelData.fStartSoundSpeed = 1.0
					SET_LUCKY_WHEEL_FLOW_STATE(LUCKY_WHEEL_FLOW_STATE_SPINNING)
					
					// url:bugstar:6339917 - Players disconnect immediately after spinning the wheel which makes the landing segment and reward not match.
					// Description: By setting the server spin index here, we don't need to wait until the end to broadcast the event with new starting segment. 
					IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
						luckyWheelServerData.iServerSpinIndex = GET_PLAYER_PRIZE_SEGMENT(luckyWheelServerData.minigamePlayer)
						PRINTLN("[LUCKY_WHEEL] [url:bugstar:6339917] MAINTAIN_LUCKY_WHEEL_MINIGAME - luckyWheelServerData.iServerSpinIndex: ", luckyWheelServerData.iServerSpinIndex)
					ENDIF
				ENDIF
			ENDIF	
		BREAK
		CASE LUCKY_WHEEL_FLOW_STATE_SPINNING
			PRINTLN("[LUCKY_WHEEL] MAINTAIN_LUCKY_WHEEL_MINIGAME - LUCKY_WHEEL_FLOW_STATE_SPINNING")
			
			IF DOES_ENTITY_EXIST(luckyWheelData.objLuckyWheel)
			AND DOES_ENTITY_HAVE_DRAWABLE(luckyWheelData.objLuckyWheel)
				MAINTAIN_WHEEL_SPEED()
			ENDIF
			
			PLAY_SPIN_START_SOUND(luckyWheelData.fStartSoundSpeed)
			PLAY_SPIN_SINGLE_TICK_SOUND(luckyWheelData.fStartSoundSpeed)
			
			IF HAS_LUCKY_WHEEL_ANIMATION_CLIP_FINISHED(GET_LUCKY_WHEEL_MINIGAME_SPIN_ANIMATION_ENUM(GET_SPIN_INTENSITY(PLAYER_ID())), FALSE, FALSE)	
				PERFORM_LUCKY_WHEEL_MINIGAME_SYNC_SCENE(GET_LUCKY_WHEEL_MINIGAME_SPIN_IDLE_ANIMATION_ENUM(GET_SPIN_INTENSITY(PLAYER_ID())), FALSE, TRUE, TRUE, FALSE)
			ENDIF
			
			IF HAS_LUCKY_WHEEL_PROP_ANIMATION_CLIP_FINISHED()
				SET_LUCKY_WHEEL_MINIGAME_BIT(BS_LUCKY_WHEEL_SPIN_ANIM_COMPLETE)		
			ENDIF

			// Moves onto winner state from inside here
			IF IS_LUCKY_WHEEL_MINIGAME_BIT_SET(BS_LUCKY_WHEEL_SPIN_ANIM_COMPLETE)
			AND DOES_ENTITY_EXIST(luckyWheelData.objLuckyWheel)
			AND DOES_ENTITY_HAVE_DRAWABLE(luckyWheelData.objLuckyWheel)
				PERFORM_LUCKY_WHEEL_MINIGAME_SPIN()
			ENDIF
		BREAK
		CASE LUCKY_WHEEL_FLOW_STATE_WINNER
			PRINTLN("[LUCKY_WHEEL] MAINTAIN_LUCKY_WHEEL_MINIGAME - LUCKY_WHEEL_FLOW_STATE_WINNER")
			IF luckyWheelData.iSpinTickSoundID != -1
				RELEASE_SOUND_ID(luckyWheelData.iSpinTickSoundID)
				luckyWheelData.iSpinTickSoundID = -1
			ENDIF	
			
			CLEAR_LUCKY_WHEEL_MINIGAME_BIT(BS_LUCKY_WHEEL_SPIN_ANIM_COMPLETE)
			IF HAS_LUCKY_WHEEL_ANIMATION_CLIP_FINISHED(GET_LUCKY_WHEEL_MINIGAME_WIN_ANIMATION_ENUM())
				IF IS_ANY_DISABLED_ANALOGUE_PRESSED()
					STOP_CURRENT_LUCKY_WHEEL_SYNCHED_SCENE()
				ENDIF	
				CLEAR_LUCKY_WHEEL_MINIGAME_BIT(BS_LUCKY_WHEEL_PERFORM_WIN_ANIM)
				RESET_NET_TIMER(luckyWheelData.sLuckyReward.menuTimeOut)
				SET_LUCKY_WHEEL_FLOW_STATE(LUCKY_WHEEL_FLOW_STATE_REWARDS)
			ENDIF
		BREAK
		CASE LUCKY_WHEEL_FLOW_STATE_REWARDS
			PRINTLN("[LUCKY_WHEEL] MAINTAIN_LUCKY_WHEEL_MINIGAME - LUCKY_WHEEL_FLOW_STATE_REWARDS")
			IF IS_LUCKY_WHEEL_REWARDS_DONE()
				SET_LUCKY_WHEEL_MINIGAME_BIT(BS_LUCKY_WHEEL_TRIGGER_USAGE_STAT)
				MAINTAIN_PRIZE_HELP_AND_TICKER()
				SEND_LUCKY_WHEEL_METRIC_DATA()
				luckyWheelData.iGivenRewardPosixTime = GET_CLOUD_TIME_AS_INT()
				SET_LUCKY_WHEEL_FLOW_STATE(LUCKY_WHEEL_FLOW_STATE_USAGE_STAT)
			ENDIF	
		BREAK
		CASE LUCKY_WHEEL_FLOW_STATE_TIMED_OUT
			PRINTLN("[LUCKY_WHEEL] MAINTAIN_LUCKY_WHEEL_MINIGAME - LUCKY_WHEEL_FLOW_STATE_TIMED_OUT")
			IF IS_LUCKY_WHEEL_MINIGAME_BIT_SET(BS_LUCKY_WHEEL_PERFORM_EXIT_ANIM)
				IF HAS_LUCKY_WHEEL_ANIMATION_CLIP_FINISHED(LUCKY_WHEEL_ANIM_CLIP_EXIT)
					IF IS_ANY_DISABLED_ANALOGUE_PRESSED()
						STOP_CURRENT_LUCKY_WHEEL_SYNCHED_SCENE()
					ENDIF	
					CLEAR_LUCKY_WHEEL_MINIGAME_BIT(BS_LUCKY_WHEEL_PERFORM_EXIT_ANIM)
					SET_LUCKY_WHEEL_FLOW_STATE(LUCKY_WHEEL_FLOW_STATE_CLEANUP)
				ENDIF	
			ELSE
				SET_LUCKY_WHEEL_FLOW_STATE(LUCKY_WHEEL_FLOW_STATE_CLEANUP)
			ENDIF
		BREAK
		CASE LUCKY_WHEEL_FLOW_STATE_REMOTE_START_SPIN_ANIM
			MAINTAIN_REMOTE_PLAYER_START_SPIN_ANIM()
		BREAK
		CASE LUCKY_WHEEL_FLOW_STATE_REMOTE_START_MANUAL_SPIN
			MAINTAIN_REMOTE_PALYER_MANUAL_SPIN()
		BREAK
		CASE LUCKY_WHEEL_FLOW_STATE_USAGE_STAT
			MAINTAIN_LUCKY_WHEEL_USAGE_STAT()
		BREAK
		CASE LUCKY_WHEEL_FLOW_STATE_CLEANUP
			CLEANUP_LUCKY_WHEEL_MINIGAME()
		BREAK
	ENDSWITCH
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════╡ DEBUG WIDGETS ╞═════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

#IF IS_DEBUG_BUILD
FUNC STRING DEBUG_GET_LUCKY_WHEEL_MINIGAME_PRIZE_NAME(INT iPrizeSegment)
	LUCKY_WHEEL_PRIZES ePrize = INT_TO_ENUM(LUCKY_WHEEL_PRIZES, iPrizeSegment)
	SWITCH ePrize
		CASE LUCKY_WHEEL_PRIZE_RP_2500				RETURN "2500 RP"
		CASE LUCKY_WHEEL_PRIZE_CASH_20000			RETURN "$20000"
		CASE LUCKY_WHEEL_PRIZE_HOUSE_CHIPS_10000	RETURN "10000 House Chips"
		CASE LUCKY_WHEEL_PRIZE_CLOTHING_1			RETURN "Clothing 1"
		CASE LUCKY_WHEEL_PRIZE_RP_5000				RETURN "5000 RP"
		CASE LUCKY_WHEEL_PRIZE_HOUSE_CHIPS_15000	RETURN "15000 House Chips"
		CASE LUCKY_WHEEL_PRIZE_MYSTERY_STAR			RETURN "Mystery Star"
		CASE LUCKY_WHEEL_PRIZE_CLOTHING_2			RETURN "Clothing 2"
		CASE LUCKY_WHEEL_PRIZE_RP_7500				RETURN "7500 RP"
		CASE LUCKY_WHEEL_PRIZE_CASH_30000			RETURN "$30000"
		CASE LUCKY_WHEEL_PRIZE_HOUSE_CHIPS_20000	RETURN "20000 House Chips"
		CASE LUCKY_WHEEL_PRIZE_CLOTHING_3			RETURN "Clothing 3"
		CASE LUCKY_WHEEL_PRIZE_RP_10000				RETURN "10000 RP"
		CASE LUCKY_WHEEL_PRIZE_PODIUM_VEHICLE		RETURN "Podium Vehicle"
		CASE LUCKY_WHEEL_PRIZE_CASH_40000			RETURN "$40000"
		CASE LUCKY_WHEEL_PRIZE_CLOTHING_4			RETURN "Clothing 4"
		CASE LUCKY_WHEEL_PRIZE_RP_15000				RETURN "15000 RP"
		CASE LUCKY_WHEEL_PRIZE_CASH_50000			RETURN "$50000"
		CASE LUCKY_WHEEL_PRIZE_HOUSE_CHIPS_25000	RETURN "25000 House Chips"
		CASE LUCKY_WHEEL_PRIZE_DISCOUNT_VOUCHER		RETURN "Discount Voucher"
	ENDSWITCH
	RETURN ""
ENDFUNC

PROC DEBUG_DISPLAY_LUCKY_WHEEL_MINIGAME_DATA()
	
	IF NOT luckyWheelDebugData.bDisplaySpinDebug
		EXIT
	ENDIF
	
	// Background
	DRAW_RECT(0.478, 0.124, 0.299, 0.250, 0, 0, 0, 255)
	
	// Title
	SET_TEXT_SCALE(0.6, 0.6)
	SET_TEXT_COLOUR(178, 34, 34, 255)
	DISPLAY_TEXT_WITH_LITERAL_STRING(0.333, 0.035, "STRING", "Lucky 7 Wheel Debug")
	
	// Prize
	SET_TEXT_SCALE(0.5, 0.5)
	SET_TEXT_COLOUR(178, 34, 34, 255)
	DISPLAY_TEXT_WITH_LITERAL_STRING(0.333, 0.093, "STRING", "Prize: ")
	
	IF GET_PLAYER_PRIZE_SEGMENT(PLAYER_ID()) != -1
		TEXT_LABEL_63 tlPrize = "("
		tlPrize += GET_PLAYER_PRIZE_SEGMENT(PLAYER_ID())
		tlPrize += ") "
		tlPrize += DEBUG_GET_LUCKY_WHEEL_MINIGAME_PRIZE_NAME(GET_PLAYER_PRIZE_SEGMENT(PLAYER_ID()))
		SET_TEXT_SCALE(0.5, 0.5)
		SET_TEXT_COLOUR(0, 255, 20, 255)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.380, 0.093, "STRING", tlPrize)
	ENDIF
	
	// Current Angle
	SET_TEXT_SCALE(0.5, 0.5)
	SET_TEXT_COLOUR(178, 34, 34, 255)
	DISPLAY_TEXT_WITH_LITERAL_STRING(0.333, 0.129, "STRING", "Current Angle: ")
	SET_TEXT_SCALE(0.5, 0.5)
	SET_TEXT_COLOUR(0, 255, 20, 255)
	DISPLAY_TEXT_WITH_FLOAT(0.448, 0.129, "NUMBER", luckyWheelData.fCurrentAngle, 1)
	
	// Current Segment
	SET_TEXT_SCALE(0.5, 0.5)
	SET_TEXT_COLOUR(178, 34, 34, 255)
	DISPLAY_TEXT_WITH_LITERAL_STRING(0.333, 0.168, "STRING", "Current Segment: ")
	SET_TEXT_SCALE(0.5, 0.5)
	SET_TEXT_COLOUR(0, 255, 20, 255)
	DISPLAY_TEXT_WITH_NUMBER(0.467, 0.170, "NUMBER", luckyWheelData.iCurrentSegmentIndex)
	
	// Starting Segment
	SET_TEXT_SCALE(0.5, 0.5)
	SET_TEXT_COLOUR(178, 34, 34, 255)
	DISPLAY_TEXT_WITH_LITERAL_STRING(0.333, 0.207, "STRING", "Starting Segment: ")
	SET_TEXT_SCALE(0.5, 0.5)
	SET_TEXT_COLOUR(0, 255, 20, 255)
	DISPLAY_TEXT_WITH_NUMBER(0.467, 0.207, "NUMBER", luckyWheelData.iStartingSegment)
	
	// Activation timer
	SET_TEXT_SCALE(0.5, 0.5)
	SET_TEXT_COLOUR(178, 34, 34, 255)
	DISPLAY_TEXT_WITH_LITERAL_STRING(0.530, 0.039, "STRING", "Timer: ")
	SET_TEXT_SCALE(0.5, 0.5)
	SET_TEXT_COLOUR(0, 255, 20, 255)
	DISPLAY_TEXT_WITH_NUMBER(0.580, 0.039, "NUMBER", luckyWheelData.iActivationTimerDifference)
	
	// Idle timer
	SET_TEXT_SCALE(0.5, 0.5)
	SET_TEXT_COLOUR(178, 34, 34, 255)
	DISPLAY_TEXT_WITH_LITERAL_STRING(0.500, 0.168, "STRING", "Idle Timer: ")
	SET_TEXT_SCALE(0.5, 0.5)
	SET_TEXT_COLOUR(0, 255, 20, 255)
	DISPLAY_TEXT_WITH_NUMBER(0.580, 0.168, "NUMBER", luckyWheelDebugData.iIdleTimerDifference)
ENDPROC

PROC DEBUG_DISPLAY_LUCKY_WHEEL_MINIGAME_SERVER_BD_DATA()
	
	IF NOT luckyWheelDebugData.bDisplayServerBDDebug
		EXIT
	ENDIF
	
	// Background
	DRAW_RECT(0.175, 0.124, 0.299, 0.179, 0, 0, 0, 255)
	
	// Title
	SET_TEXT_SCALE(0.6, 0.6)
	SET_TEXT_COLOUR(178, 34, 34, 255)
	DISPLAY_TEXT_WITH_LITERAL_STRING(0.03, 0.035, "STRING", "Server BD")
	
	// Minigame player
	SET_TEXT_SCALE(0.5, 0.5)
	SET_TEXT_COLOUR(178, 34, 34, 255)
	DISPLAY_TEXT_WITH_LITERAL_STRING(0.03, 0.093, "STRING", "Minigame Player: ")
	
	IF luckyWheelServerData.minigamePlayer != INVALID_PLAYER_INDEX()
		TEXT_LABEL_63 tlPlayer = GET_PLAYER_NAME(luckyWheelServerData.minigamePlayer)
		tlPlayer += " ("
		tlPlayer += NATIVE_TO_INT(luckyWheelServerData.minigamePlayer)
		tlPlayer += ")"
		SET_TEXT_SCALE(0.5, 0.5)
		SET_TEXT_COLOUR(0, 255, 20, 255)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.16, 0.095, "STRING", tlPlayer)
	ENDIF
	
	// Anim
	SET_TEXT_SCALE(0.4, 0.4)
	SET_TEXT_COLOUR(178, 34, 34, 255)
	DISPLAY_TEXT_WITH_LITERAL_STRING(0.208, 0.04, "STRING", "Anim: ")
	
	// Script
	SET_TEXT_SCALE(0.4, 0.4)
	SET_TEXT_COLOUR(178, 34, 34, 255)
	DISPLAY_TEXT_WITH_LITERAL_STRING(0.26, 0.04, "STRING", "Script: ")
	
	// Bars
	IF IS_LUCKY_WHEEL_MINIGAME_BIT_SET(BS_LUCKY_WHEEL_SPIN_ANIM_COMPLETE)
		DRAW_RECT(0.25, 0.057, 0.01, 0.02, 178, 34, 34, 255) // Anim
		DRAW_RECT(0.308, 0.057, 0.01, 0.02, 0, 255, 20, 255) // Script
	ELSE
		DRAW_RECT(0.25, 0.057, 0.01, 0.02, 0, 255, 20, 255)		// Anim
		DRAW_RECT(0.308, 0.057, 0.01, 0.02, 178, 34, 34, 255)	// Script
	ENDIF
ENDPROC

PROC DEBUG_MAINTAIN_RESTART_LUCKY_WHEEL_MINIGAME()
	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_1, KEYBOARD_MODIFIER_SHIFT, "")
		PRINTLN("[LUCKY_WHEEL] MAINTAIN_LUCKY_WHEEL_MINIGAME - Debug Used: SHIFT+1")
		
		// Reset the wheel anim sync scene
		NETWORK_STOP_SYNCHRONISED_SCENE(luckyWheelData.iWheelSceneID)
		DETACH_SYNCHRONIZED_SCENE(luckyWheelData.iWheelSceneID)
		
		// Reset the wheel prop rotation
		IF DOES_ENTITY_EXIST(luckyWheelData.objLuckyWheel)
			SET_ENTITY_ROTATION(luckyWheelData.objLuckyWheel, <<0.0, 0.0, 0.0>>)
		ENDIF
		
		SET_LUCKY_WHEEL_FLOW_STATE(LUCKY_WHEEL_FLOW_STATE_CLEANUP)
	ENDIF
ENDPROC

PROC DEBUG_MAINTAIN_LUCKY_WHEEL_WIDGETS()
	IF luckyWheelDebugData.bResetWheelRotation
		IF DOES_ENTITY_EXIST(luckyWheelData.objLuckyWheel)
			SET_ENTITY_ROTATION(luckyWheelData.objLuckyWheel, <<0.0, 0.0, 0.0>>)
			luckyWheelData.fStartingAngle = 0.0
		ENDIF
		luckyWheelDebugData.bResetWheelRotation = FALSE
	ENDIF
	
	IF luckyWheelDebugData.bEnableDebugPrize
		IF DOES_TEXT_WIDGET_EXIST(luckyWheelDebugData.widgetPrize)
			SET_CONTENTS_OF_TEXT_WIDGET(luckyWheelDebugData.widgetPrize, DEBUG_GET_LUCKY_WHEEL_MINIGAME_PRIZE_NAME(luckyWheelDebugData.iDebugPrize))
		ENDIF
		
		IF DOES_TEXT_WIDGET_EXIST(luckyWheelDebugData.widgetMystreyPrize)
			SET_CONTENTS_OF_TEXT_WIDGET(luckyWheelDebugData.widgetMystreyPrize, GET_LUCKY_REWARD_MYSTERY_TYPE_DEBUG_NAME(INT_TO_ENUM(LUCKY_REWARD_MYSTERY_TYPE, luckyWheelDebugData.iDebugMystreyPrize)))
		ENDIF
		
		IF DOES_TEXT_WIDGET_EXIST(luckyWheelDebugData.widgetMystreyVehiclePrize)
			SET_CONTENTS_OF_TEXT_WIDGET(luckyWheelDebugData.widgetMystreyVehiclePrize, GET_LUCKY_REWARD_MYSTERY_VEHICLE_TYPE_DEBUG_NAME(INT_TO_ENUM(LUCKY_REWARD_MYSTERY_VEHICLE_TYPE, g_iDebugLuckyWheelMystreyVehicle)))
		ENDIF
		
		IF DOES_TEXT_WIDGET_EXIST(luckyWheelDebugData.widgetMystreyClothesPrize)
			SET_CONTENTS_OF_TEXT_WIDGET(luckyWheelDebugData.widgetMystreyClothesPrize, GET_LUCKY_REWARD_CLOTHES_TYPE_TEXT_LABEL(INT_TO_ENUM(LUCKY_REWARD_CLOTHES_TYPE, g_iDebugLuckyWheelMystreyClothes)))
		ENDIF
		
		IF DOES_TEXT_WIDGET_EXIST(luckyWheelDebugData.widgetMystreyInventoryPrize)
			SET_CONTENTS_OF_TEXT_WIDGET(luckyWheelDebugData.widgetMystreyInventoryPrize, GET_LUCKY_REWARD_MYSTERY_INVENTORY_TYPE_DEBUG_NAME(INT_TO_ENUM(LUCKY_REWARD_MYSTERY_INVENTORY_TYPE, g_iDebugLuckyWheelMystreyInventory)))
		ENDIF
		IF DOES_TEXT_WIDGET_EXIST(luckyWheelDebugData.widgetMystreyBusinessPrize)
			SET_CONTENTS_OF_TEXT_WIDGET(luckyWheelDebugData.widgetMystreyBusinessPrize, GET_LUCKY_REWARD_MYSTERY_BUSINESS_TYPE_DEBUG_NAME(INT_TO_ENUM(LUCKY_REWARD_MYSTERY_BUSINESS_TYPE, g_iDebugLuckyWheelMystreyBusiness)))
		ENDIF
	ELSE
		g_iDebugLuckyWheelMystreyVehicle = -1
	ENDIF
	
	IF luckyWheelDebugData.bEnableManualRotation
		IF DOES_ENTITY_EXIST(luckyWheelData.objLuckyWheel)
			SET_ENTITY_ROTATION(luckyWheelData.objLuckyWheel, <<0.0, -luckyWheelDebugData.fRotationAngle, 0.0>>)
		ENDIF
	ELSE
		VECTOR vRot
		IF DOES_ENTITY_EXIST(luckyWheelData.objLuckyWheel)
			IF DOES_ENTITY_EXIST(luckyWheelData.objLuckyWheel)
				vRot = GET_ENTITY_ROTATION(luckyWheelData.objLuckyWheel)
				luckyWheelDebugData.fRotationAngle = vRot.y
			ENDIF	
		ENDIF	
	ENDIF	
ENDPROC

PROC CREATE_WIDGETS()
	START_WIDGET_GROUP("CASINO_LUCKY_WHEEL")
		START_WIDGET_GROUP("Set Prize")
			ADD_WIDGET_BOOL("Enable", luckyWheelDebugData.bEnableDebugPrize)
			ADD_WIDGET_INT_SLIDER("Prize ID", luckyWheelDebugData.iDebugPrize, 0, 19, 1)
			luckyWheelDebugData.widgetPrize = ADD_TEXT_WIDGET("Prize Name: ")
			ADD_WIDGET_INT_SLIDER("Mystrey Prize ID", luckyWheelDebugData.iDebugMystreyPrize, 0, ENUM_TO_INT(LRM_SUM) - 1, 1)
			luckyWheelDebugData.widgetMystreyPrize = ADD_TEXT_WIDGET("Mystrey Prize Name: ")
			ADD_WIDGET_INT_SLIDER("Mystrey Vehicle Prize ID", g_iDebugLuckyWheelMystreyVehicle, -1, ENUM_TO_INT(LRMVEH_SUM) - 1, 1)
			luckyWheelDebugData.widgetMystreyVehiclePrize  = ADD_TEXT_WIDGET("Mystrey Vehicle Prize Name: ")
			ADD_WIDGET_INT_SLIDER("Clothes Prize ID", g_iDebugLuckyWheelMystreyClothes, -1, (MAX_NUM_REWARD_CLOTHES * 2) - 1, 1)
			luckyWheelDebugData.widgetMystreyClothesPrize  = ADD_TEXT_WIDGET("Clothes Prize Name: ")
			ADD_WIDGET_INT_SLIDER("Mystrey Inventory Prize ID", g_iDebugLuckyWheelMystreyInventory, -1, ENUM_TO_INT(LRMINV_SUM) - 1, 1)
			luckyWheelDebugData.widgetMystreyInventoryPrize  = ADD_TEXT_WIDGET("Mystrey Inventory Prize Name: ")
			ADD_WIDGET_INT_SLIDER("Mystrey Business Prize ID", g_iDebugLuckyWheelMystreyBusiness, -1, ENUM_TO_INT(LRMBUSINESS_SUM) - 1, 1)
			luckyWheelDebugData.widgetMystreyBusinessPrize  = ADD_TEXT_WIDGET("Mystrey Business Prize Name: ")
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Server BD")
			ADD_WIDGET_BOOL("Display On-Screen Debug", luckyWheelDebugData.bDisplayServerBDDebug)
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Wheel Spin Settings")
			ADD_WIDGET_BOOL("Display On-Screen Debug", luckyWheelDebugData.bDisplaySpinDebug)
			ADD_WIDGET_BOOL("Reset Wheel Rotation", luckyWheelDebugData.bResetWheelRotation)
			START_WIDGET_GROUP("Spin Time Secs")
				ADD_WIDGET_INT_SLIDER("Fast", luckyWheelDebugData.iFastSpinTimeMS, 0, 100000, 5)
				ADD_WIDGET_INT_SLIDER("Medium", luckyWheelDebugData.iMediumSpinTimeMS, 0, 100000, 5)
				ADD_WIDGET_INT_SLIDER("Slow", luckyWheelDebugData.iSlowSpinTimeMS, 0, 100000, 5)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Number of Spins")
				ADD_WIDGET_INT_SLIDER("Fast", luckyWheelDebugData.iFastNumSpins, 0, 10, 1)
				ADD_WIDGET_INT_SLIDER("Medium", luckyWheelDebugData.iMediumNumSpins, 0, 10, 1)
				ADD_WIDGET_INT_SLIDER("Slow", luckyWheelDebugData.iSlowNumSpins, 0, 10, 1)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Manual Wheel rotation")
				ADD_WIDGET_BOOL("Enable", luckyWheelDebugData.bEnableManualRotation)
				ADD_WIDGET_FLOAT_SLIDER("Rotation", luckyWheelDebugData.fRotationAngle, -1000.0, 1000.0, 0.1)
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("By Pass 24H Limit")
			ADD_WIDGET_BOOL("By Pass", luckyWheelDebugData.bByPass24HLimit)
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Clear Stats")
			ADD_WIDGET_BOOL("Clear Podium vehicle Limit timer", luckyWheelDebugData.bClearPodiumVehicleWonTime)
			ADD_WIDGET_BOOL("Clear 24H Limit timer", luckyWheelDebugData.bClear24HLimit)
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Item Weight Info")
			ADD_WIDGET_BOOL("Print all reward's total weight", luckyWheelDebugData.bItemWeight)
		STOP_WIDGET_GROUP()	
		START_WIDGET_GROUP("Generate Random Reward")
			ADD_WIDGET_BOOL("Generate Random Reward", luckyWheelDebugData.bGenerateRandomSegment)
			ADD_WIDGET_INT_SLIDER("Number of reward", luckyWheelDebugData.iNumOfRadomRewards, 0, 400, 1) 
		STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP()
ENDPROC

PROC DEBUG_CLEAR_PODIUM_VEHICLE_LIMIT_TIMER()
	IF luckyWheelDebugData.bClearPodiumVehicleWonTime
		SET_MP_INT_CHARACTER_STAT(MP_STAT_LW_PODIUM_VEH_WON, 0)
		luckyWheelDebugData.bClearPodiumVehicleWonTime = FALSE
	ENDIF
	
	IF luckyWheelDebugData.bClear24HLimit
		SET_MP_INT_CHARACTER_STAT(MP_STAT_LUCKY_WHEEL_USAGE, 0)
		SET_MP_INT_PLAYER_STAT(MPPLY_LUCKY_WHEEL_USAGE, 0)
		luckyWheelDebugData.bClear24HLimit = FALSE
	ENDIF
ENDPROC

PROC DEBUG_UPDATE_ITEM_WEIGHT_INFO()
	IF luckyWheelDebugData.bItemWeight
		SAVE_STRING_TO_DEBUG_FILE("Wheel segments total weight: ")SAVE_INT_TO_DEBUG_FILE(GET_TOTAL_WEIGHT_FOR_WHEEL_SEGMENTS())SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("Clothing segments total weight: ")SAVE_INT_TO_DEBUG_FILE(GET_TOTAL_WEIGHT_FOR_CLOTHING_REWARD(FALSE))SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("Voucher segments total weight: ")SAVE_INT_TO_DEBUG_FILE(GET_TOTAL_WEIGHT_FOR_VOUCHER_REWARD())SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("Mystery segments total weight: ")SAVE_INT_TO_DEBUG_FILE(GET_TOTAL_WEIGHT_FOR_MYSTERY_SEGMENTS())SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("Mystery Clothing reward total weight: ")SAVE_INT_TO_DEBUG_FILE(GET_TOTAL_WEIGHT_FOR_CLOTHING_REWARD(TRUE))SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("Mystery Business reward total weight: ")SAVE_INT_TO_DEBUG_FILE(GET_TOTAL_WEIGHT_FOR_MYSTERY_BUSINESS_REWARD(TRUE))SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("Mystery Vehicle reward total weight: ")SAVE_INT_TO_DEBUG_FILE(GET_TOTAL_WEIGHT_FOR_MYSTERY_VEHICLE_REWARD())SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("Mystery Cash reward total weight: ")SAVE_INT_TO_DEBUG_FILE(GET_TOTAL_WEIGHT_FOR_MYSTERY_CASH_REWARD())SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("Mystery Chips reward total weight: ")SAVE_INT_TO_DEBUG_FILE(GET_TOTAL_WEIGHT_FOR_MYSTERY_CHIPS_REWARD())SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("Mystery RP reward total weight: ")SAVE_INT_TO_DEBUG_FILE(GET_TOTAL_WEIGHT_FOR_MYSTERY_RP_REWARD())SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("Mystery Inventory reward total weight: ")SAVE_INT_TO_DEBUG_FILE(GET_TOTAL_WEIGHT_FOR_MYSTERY_INVENTORY_REWARD())SAVE_NEWLINE_TO_DEBUG_FILE()
		luckyWheelDebugData.bItemWeight = FALSE
	ENDIF
	IF luckyWheelDebugData.bGenerateRandomSegment
		INT i, iSegment
		LUCKY_REWARD_MYSTERY_TYPE	eMystreyReward
		SAVE_STRING_TO_DEBUG_FILE("*******GENERATE RANDOM SEGMENT ")SAVE_INT_TO_DEBUG_FILE(luckyWheelDebugData.iNumOfRadomRewards)SAVE_STRING_TO_DEBUG_FILE(" TIMES*******")SAVE_NEWLINE_TO_DEBUG_FILE()
		FOR i = 0 TO luckyWheelDebugData.iNumOfRadomRewards - 1
			SAVE_STRING_TO_DEBUG_FILE("*********************************** ")SAVE_INT_TO_DEBUG_FILE(i)SAVE_STRING_TO_DEBUG_FILE(" ***********************************")SAVE_NEWLINE_TO_DEBUG_FILE()
			iSegment = GET_WHEEL_PRIZE_INDEX_FROM_LUCKY_WHEEL_REWARD_TYPE(PICK_RANDOM_LUCK_REWARD_TYPE())
			SAVE_STRING_TO_DEBUG_FILE("Reward: ")SAVE_STRING_TO_DEBUG_FILE(DEBUG_GET_LUCKY_WHEEL_MINIGAME_PRIZE_NAME(iSegment))SAVE_NEWLINE_TO_DEBUG_FILE()
			
			SWITCH INT_TO_ENUM(LUCKY_WHEEL_PRIZES, iSegment)
				CASE LUCKY_WHEEL_PRIZE_CLOTHING_1		
				CASE LUCKY_WHEEL_PRIZE_CLOTHING_2		
				CASE LUCKY_WHEEL_PRIZE_CLOTHING_3		
				CASE LUCKY_WHEEL_PRIZE_CLOTHING_4
					SAVE_STRING_TO_DEBUG_FILE("Clothes: ")SAVE_STRING_TO_DEBUG_FILE(GET_STRING_FROM_TEXT_FILE(GET_LUCKY_REWARD_CLOTHES_TYPE_TEXT_LABEL(PICK_RANDOM_LUCK_CLOTHES_REWARD_TYPE(FALSE))))SAVE_NEWLINE_TO_DEBUG_FILE()
				BREAK
				CASE LUCKY_WHEEL_PRIZE_MYSTERY_STAR		
					eMystreyReward = PICK_RANDOM_LUCK_MYSTERY_REWARD_TYPE()
					SAVE_STRING_TO_DEBUG_FILE("Mystrey: ")SAVE_STRING_TO_DEBUG_FILE(GET_LUCKY_REWARD_MYSTERY_TYPE_DEBUG_NAME(eMystreyReward))SAVE_NEWLINE_TO_DEBUG_FILE()
					SWITCH eMystreyReward
						CASE LRM_CLOTHES 		
							SAVE_STRING_TO_DEBUG_FILE("Mystrey Clothes: ")SAVE_STRING_TO_DEBUG_FILE(GET_STRING_FROM_TEXT_FILE(GET_LUCKY_REWARD_CLOTHES_TYPE_TEXT_LABEL(PICK_RANDOM_LUCK_CLOTHES_REWARD_TYPE(TRUE))))SAVE_NEWLINE_TO_DEBUG_FILE()
						BREAK
						CASE LRM_VEHICLE
							SAVE_STRING_TO_DEBUG_FILE("Mystrey Vehicle: ")SAVE_STRING_TO_DEBUG_FILE(GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(GET_LUCK_MYSTERY_VEHICLE_MODEL(PICK_RANDOM_LUCK_MYSTERY_VEHICLE_REWARD_TYPE())))SAVE_NEWLINE_TO_DEBUG_FILE()
						BREAK
						CASE LRM_CHIPS			
							SAVE_STRING_TO_DEBUG_FILE("Mystrey Chips: ")SAVE_STRING_TO_DEBUG_FILE(GET_LUCKY_REWARD_MYSTERY_CHIPS_TYPE_DEBUG_NAME(PICK_RANDOM_LUCK_MYSTERY_CHIPS_REWARD_TYPE()))SAVE_NEWLINE_TO_DEBUG_FILE()
						BREAK
						CASE LRM_CASH				
							SAVE_STRING_TO_DEBUG_FILE("Mystrey Cash: ")SAVE_STRING_TO_DEBUG_FILE(GET_LUCKY_REWARD_MYSTERY_CASH_TYPE_DEBUG_NAME(PICK_RANDOM_LUCK_MYSTERY_CASH_REWARD_TYPE()))SAVE_NEWLINE_TO_DEBUG_FILE()
						BREAK
						CASE LRM_RP				
							SAVE_STRING_TO_DEBUG_FILE("Mystrey RP: ")SAVE_STRING_TO_DEBUG_FILE(GET_LUCKY_REWARD_MYSTERY_RP_TYPE_DEBUG_NAME(PICK_RANDOM_LUCK_MYSTERY_RP_REWARD_TYPE()))SAVE_NEWLINE_TO_DEBUG_FILE()
						BREAK
						CASE LRM_INVENTORY		
							SAVE_STRING_TO_DEBUG_FILE("Mystrey Inventory: ")SAVE_STRING_TO_DEBUG_FILE(GET_LUCKY_REWARD_MYSTERY_INVENTORY_TYPE_DEBUG_NAME(PICK_RANDOM_LUCK_MYSTERY_INVENTORY_REWARD_TYPE()))SAVE_NEWLINE_TO_DEBUG_FILE()
						BREAK
						CASE LRM_BUSINESS		
							SAVE_STRING_TO_DEBUG_FILE("Mystrey Business : ")SAVE_STRING_TO_DEBUG_FILE(GET_LUCKY_REWARD_MYSTERY_BUSINESS_TYPE_DEBUG_NAME(PICK_RANDOM_LUCK_MYSTERY_BUSINESS_REWARD_TYPE(TRUE)))SAVE_NEWLINE_TO_DEBUG_FILE()
						BREAK	
					ENDSWITCH
				BREAK
				CASE LUCKY_WHEEL_PRIZE_DISCOUNT_VOUCHER	
					SAVE_STRING_TO_DEBUG_FILE("Voucher: ")SAVE_STRING_TO_DEBUG_FILE(GET_STRING_FROM_TEXT_FILE(GET_LUCKY_REWARD_MYSTERY_VOUCHER_TYPE_NAME(PICK_RANDOM_LUCK_MYSTERY_VOUCHER_REWARD_TYPE())))SAVE_NEWLINE_TO_DEBUG_FILE()
				BREAK
			ENDSWITCH
		ENDFOR
		SAVE_STRING_TO_DEBUG_FILE("******* GENERATE RANDOM SEGMENT DONE! *******")SAVE_NEWLINE_TO_DEBUG_FILE()
		luckyWheelDebugData.bGenerateRandomSegment = FALSE
	ENDIF
ENDPROC

PROC UPDATE_WIDGETS()
	DEBUG_MAINTAIN_LUCKY_WHEEL_WIDGETS()
	DEBUG_MAINTAIN_RESTART_LUCKY_WHEEL_MINIGAME()
	DEBUG_DISPLAY_LUCKY_WHEEL_MINIGAME_DATA()
	DEBUG_DISPLAY_LUCKY_WHEEL_MINIGAME_SERVER_BD_DATA()
	DEBUG_CLEAR_PODIUM_VEHICLE_LIMIT_TIMER()
	DEBUG_UPDATE_ITEM_WEIGHT_INFO()
ENDPROC
#ENDIF

//╒══════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════╡ SCRIPT CLEANUP ╞════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛
PROC SCRIPT_CLEANUP()
	PRINTLN("[LUCKY_WHEEL] - SCRIPT_CLEANUP")
	
	// In case player leave session as soon as got the rewardm, check if we need to set the usage stat
	IF IS_LUCKY_WHEEL_MINIGAME_BIT_SET(BS_LUCKY_WHEEL_TRIGGER_USAGE_STAT)
		SET_LUCKY_WHEEL_USAGE_TIME(TRUE)
		PRINTLN("[LUCKY_WHEEL] - SCRIPT_CLEANUP - player leaving before setting usage stat")
	ENDIF	
	
	SET_MODEL_AS_NO_LONGER_NEEDED(GET_LUCKY_WHEEL_MINIGAME_MODEL())
	SET_PLAYER_STARTED_SPIN(FALSE)
	STOP_CURRENT_LUCKY_WHEEL_WHEEL_SYNCHED_SCENE()
	STOP_CURRENT_LUCKY_WHEEL_SYNCHED_SCENE()
	SET_PLAYER_IS_USING_THE_LUCKY_WHEEL(PLAYER_ID(), FALSE)
	
	IF NOT IS_STRING_NULL_OR_EMPTY(luckyWheelData.sAnimDict)
		REMOVE_ANIM_DICT(luckyWheelData.sAnimDict)
	ENDIF
	
	IF DOES_ENTITY_EXIST(luckyWheelData.objLuckyWheel)
		DELETE_OBJECT(luckyWheelData.objLuckyWheel)
	ENDIF
	
	IF IS_BIT_SET(g_SimpleInteriorData.iSeventhBS, BS7_SIMPLE_INTERIOR_CASINO_LUCKY_WHEEL_MEMBERSHIP_HELP)
		CLEAR_BIT(g_SimpleInteriorData.iSeventhBS, BS7_SIMPLE_INTERIOR_CASINO_LUCKY_WHEEL_MEMBERSHIP_HELP)
	ENDIF
	
	STOP_ALL_WHEEL_SOUNDS()
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("CasinoUI_Lucky_Wheel")
	
	#IF IS_DEBUG_BUILD
	g_iDebugLuckyWheelMystreyVehicle = -1
	g_iDebugLuckyWheelMystreyClothes = -1
	g_iDebugLuckyWheelMystreyInventory = -1
	g_iDebugLuckyWheelMystreyBusiness = -1
	#ENDIF
	
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC

PROC INITIALISE_CASINO_LUCKY_WHEEL()
	GET_NUM_AVAILABLE_WEAPON_GROUP(sGunLocker)
	REQUEST_STREAMED_TEXTURE_DICT("CasinoUI_Lucky_Wheel")
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════╡ SCRIPT INITIALISE ╞══════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛
PROC SCRIPT_INITIALISE()
	PRINTLN("[LUCKY_WHEEL] - SCRIPT_INITIALISE - Initialising")
	
	NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE)
	
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(m_PlayerBD, SIZE_OF(m_PlayerBD))
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(luckyWheelServerData, SIZE_OF(luckyWheelServerData))
	
	IF NOT Wait_For_First_Network_Broadcast()
		PRINTLN("[LUCKY_WHEEL] - SCRIPT_INITIALISE - Failed to receive initial network broadcast. Terminating script.")
		SCRIPT_CLEANUP()
	ENDIF
	
	SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
	
	#IF IS_DEBUG_BUILD
	CREATE_WIDGETS()
	#ENDIF
	
	INITIALISE_CASINO_LUCKY_WHEEL()

	PRINTLN("[LUCKY_WHEEL] - SCRIPT_INITIALISE - Complete")
ENDPROC

PROC MAINTAIN_GAME_UI_ON_WHEEL_ACTIVATION()
	IF IS_LUCKY_WHEEL_MINIGAME_BIT_SET(BS_LUCKY_WHEEL_ACTIVATED)
		DISABLE_FRONTEND_THIS_FRAME()
		SET_FRONTEND_ACTIVE(FALSE)
		DISABLE_SELECTOR_THIS_FRAME()
	ENDIF
ENDPROC	

/// PURPOSE:
///    Checks if we recived the event from lucky wheel user, so we can request the anim and start the spin locally 
PROC MAINTAIN_REMOTE_PLAYER_WHEEL_SPIN()
	IF IS_LUCKY_WHEEL_MINIGAME_BIT_SET(BS_LUCKY_WHEEL_RECEIVED_EVENT_FROM_WHEEL_USER)
		luckyWheelData.sAnimDict = GET_LUCKY_WHEEL_MINIGAME_ANIMATION_DICTIONARY()
		REQUEST_ANIM_DICT(luckyWheelData.sAnimDict)
		IF HAS_ANIM_DICT_LOADED(luckyWheelData.sAnimDict)
			luckyWheelData.iSpinStartIndex = luckyWheelServerData.iServerSpinIndex
			PRINTLN("[LUCKY_WHEEL] MAINTAIN_REMOTE_PLAYER_WHEEL_SPIN setting remote player luckyWheelData.iSpinStartIndex: ", luckyWheelData.iSpinStartIndex)
			SET_LUCKY_WHEEL_FLOW_STATE(LUCKY_WHEEL_FLOW_STATE_REMOTE_START_SPIN_ANIM)
			CLEAR_LUCKY_WHEEL_MINIGAME_BIT(BS_LUCKY_WHEEL_RECEIVED_EVENT_FROM_WHEEL_USER)
		ELSE
			PRINTLN("[LUCKY_WHEEL] MAINTAIN_REMOTE_PLAYER_WHEEL_SPIN loading anim HAS_ANIM_DICT_LOADED false")
		ENDIF	
	ENDIF
ENDPROC

PROC MAINTAIN_LUCKY_WHEEL_PIRZE_HELP_TIMER()
	IF IS_BIT_SET(luckyWheelData.iPermanentBS, BS_PERMANENT_LUCKY_WHEEL_PRIZE_HELP_ON_SCREEN)
		IF NOT HAS_NET_TIMER_STARTED(luckyWheelData.sPrizeHelpTimer)
			START_NET_TIMER(luckyWheelData.sPrizeHelpTimer)
		ELSE
			IF HAS_NET_TIMER_EXPIRED(luckyWheelData.sPrizeHelpTimer, 40000)
			OR (NOT IS_HELP_MESSAGE_ON_SCREEN() AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED())
				CLEAR_BIT(luckyWheelData.iPermanentBS, BS_PERMANENT_LUCKY_WHEEL_PRIZE_HELP_ON_SCREEN)
				RESET_NET_TIMER(luckyWheelData.sPrizeHelpTimer)
			ENDIF	
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_DISCOUNT_VOUCHER_HELP()
	IF luckyWheelData.eState = LUCKY_WHEEL_FLOW_STATE_INITAILISE
		IF IS_BIT_SET(luckyWheelData.iPermanentBS, BS_PERMANENT_LUCKY_WHEEL_VOUCHER_DETAIL_HELP)
			INT iNumHelpDisplayed = GET_PACKED_STAT_INT(PACKED_STAT_PLUCKY_WHEEL_VOUCHER_HELP)
			IF iNumHelpDisplayed < 4
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				AND NOT IS_HELP_MESSAGE_ON_SCREEN()
					PRINT_HELP("CAS_LW_VOUCH")
					iNumHelpDisplayed++
					SET_PACKED_STAT_INT(PACKED_STAT_PLUCKY_WHEEL_VOUCHER_HELP, iNumHelpDisplayed)
					CLEAR_BIT(luckyWheelData.iPermanentBS, BS_PERMANENT_LUCKY_WHEEL_VOUCHER_DETAIL_HELP)
					PRINTLN("[LUCKY_WHEEL] MAINTAIN_PRIZE_HELP_AND_TICKER BS_PERMANENT_LUCKY_WHEEL_VOUCHER_DETAIL_HELP TRUE")
				ENDIF
			ELSE
				CLEAR_BIT(luckyWheelData.iPermanentBS, BS_PERMANENT_LUCKY_WHEEL_VOUCHER_DETAIL_HELP)
				PRINTLN("[LUCKY_WHEEL] MAINTAIN_PRIZE_HELP_AND_TICKER BS_PERMANENT_LUCKY_WHEEL_VOUCHER_DETAIL_HELP FALSE iNumHelpDisplayed: ", iNumHelpDisplayed)
			ENDIF	
		ENDIF	
	ENDIF	
ENDPROC

PROC MAINTAIN_NEW_PODIUM_VEHICLE_ANNOUNCE()
	IF GET_FRAME_COUNT() % 30 = 0
		IF IS_MODEL_VALID(INT_TO_ENUM(MODEL_NAMES, g_sMPTunables.iCASINO_PRIZE_VEHICLE_MODEL_HASH))
			IF GET_MP_INT_CHARACTER_STAT(MP_STAT_LW_NEW_POD_VEH_ANNOUNCE) != g_sMPTunables.iCASINO_PRIZE_VEHICLE_MODEL_HASH
				SET_BIT(g_iCasinoAnnouncerBS, ciCASINO_ANNOUNCER_TRIGGER_LUCKY_WHEEL_NEW_VEHICLE)
				SET_MP_INT_CHARACTER_STAT(MP_STAT_LW_NEW_POD_VEH_ANNOUNCE, g_sMPTunables.iCASINO_PRIZE_VEHICLE_MODEL_HASH)	
				PRINTLN("[LUCKY_WHEEL] MAINTAIN_NEW_PODIUM_VEHICLE_ANNOUNCE SET ciCASINO_ANNOUNCER_TRIGGER_LUCKY_WHEEL_NEW_VEHICLE")
			ENDIF
		ENDIF	
	ENDIF	
ENDPROC

PROC MAINTAIN_WHEEL_TIMER_TICKER_ON_ENTERANCE()
	IF IS_LOCAL_PLAYER_WALKING_OUT_OF_SIMPLE_INTERIOR()
		EXIT
	ENDIF
	
	IF !IS_SCREEN_FADED_IN()
		EXIT
	ENDIF	
	
	IF NETWORK_IS_ACTIVITY_SESSION()
		EXIT
	ENDIF
	
	IF NETWORK_IS_IN_MP_CUTSCENE()
		EXIT
	ENDIF	
	
	IF !IS_PLAYER_CONTROL_ON(PLAYER_ID())
		EXIT
	ENDIF
	
	IF BLOCK_LUCKY_WHEEL_MINIGAME(FALSE, FALSE)
		EXIT
	ENDIF	
	
	IF !IS_BIT_SET(luckyWheelData.iPermanentBS, BS_PERMANENT_LUCKY_WHEEL_REMAINING_TIME_TICKER)
		IF HAS_PLAYER_USED_LUCKY_WHEEL_IN_PAST_24_H()
			UGC_DATE sDate
			INT iCharacterUsedTime = GET_MP_INT_CHARACTER_STAT(MP_STAT_LUCKY_WHEEL_USAGE)
			INT iCurrentPosixTime = GET_CLOUD_TIME_AS_INT()
			INT iTimeDiff = iCharacterUsedTime - iCurrentPosixTime
			
			IF iTimeDiff > 0 
				CONVERT_POSIX_TIME(iTimeDiff, sDate)
				
				INT iTimeMS 
				iTimeMS = (sDate.nHour * 3600000)
				iTimeMS += (sDate.nMinute * 60000)
				iTimeMs += (sDate.nSecond * 1000)
				
				PRINTLN("[MAINTAIN_WHEEL_TIMER_TICKER_ON_ENTERANCE] iTimeDiff: ", iTimeDiff, " hour: ", sDate.nHour, " min: ", sDate.nMinute, " sec: ", sDate.nSecond , " iTimeMS: ", iTimeMS)
				
				TEXT_LABEL_15 sTime = GET_TEXT_OF_TIME(iTimeMs)
				BEGIN_TEXT_COMMAND_THEFEED_POST("CAS_WHEEL_TR")
				ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(sTime)
				END_TEXT_COMMAND_THEFEED_POST_TICKER(TRUE)
			ENDIF	
		ELSE
			PRINT_TICKER("CAS_WHEEL_RE")
		ENDIF
		
		SET_BIT(luckyWheelData.iPermanentBS, BS_PERMANENT_LUCKY_WHEEL_REMAINING_TIME_TICKER)
	ENDIF	
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════╡ SCRIPT MAINTAIN ╞════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

PROC RUN_MAIN_SERVER_LOGIC()
	SET_LUCKY_WHEEL_MINIGAME_SERVER_BD()
ENDPROC

PROC RUN_MAIN_CLIENT_LOGIC()
	MAINTAIN_LUCKY_WHEEL_PROP()
	MAINTAIN_LUCKY_WHEEL_MINIGAME()
	MAINTAIN_LUCKY_WHEEL_EVENTS()
	MAINTAIN_FLASH_PROP_LIGHTS()
	MAINTAIN_LUCKY_WHEEL_CAMERA()
	MAINTAIN_GAME_UI_ON_WHEEL_ACTIVATION()
	MAINTAIN_REMOTE_PLAYER_WHEEL_SPIN()
	MAINTAIN_MOVE_PLAYER_OUT_OF_WHEEL_AREA()
	MAINTAIN_MOVE_PLAYER_IF_IDLE_NEAR_WHEEL()
	MAINTAIN_LUCKY_WHEEL_PIRZE_HELP_TIMER()
	MAINTAIN_DISCOUNT_VOUCHER_HELP()
	MAINTAIN_NEW_PODIUM_VEHICLE_ANNOUNCE()
	MAINTAIN_WHEEL_TIMER_TICKER_ON_ENTERANCE()
ENDPROC

FUNC BOOL SHOULD_CASINO_LUCKY_WHEEL_SCRIPT_TERMINATE()
	BOOL bTerminate = FALSE
	
	IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
		PRINTLN("[LUCKY_WHEEL] - SHOULD_CASINO_LUCKY_WHEEL_SCRIPT_TERMINATE - SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE is returning TRUE")
		bTerminate = TRUE
	ENDIF
	
	IF NOT IS_PLAYER_IN_CASINO(PLAYER_ID())
	AND GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY("AM_MP_CASINO")) < 1
		PRINTLN("[LUCKY_WHEEL] - SHOULD_CASINO_LUCKY_WHEEL_SCRIPT_TERMINATE - Player is no longer inside the Casino")
		bTerminate = TRUE
	ENDIF
	
	IF g_bCleanupLuckywheel
		PRINTLN("[LUCKY_WHEEL] - SHOULD_CASINO_LUCKY_WHEEL_SCRIPT_TERMINATE - g_bCleanupLuckywheel")
		g_bCleanupLuckywheel = FALSE
		bTerminate = TRUE
	ENDIF
	
	RETURN bTerminate
ENDFUNC

SCRIPT
	IF NETWORK_IS_GAME_IN_PROGRESS()
		SCRIPT_INITIALISE()	
	ELSE
		SCRIPT_CLEANUP()
	ENDIF

	WHILE TRUE
		WAIT(0)

		IF SHOULD_CASINO_LUCKY_WHEEL_SCRIPT_TERMINATE()
			PRINTLN("[LUCKY_WHEEL] - Terminating script")
			SCRIPT_CLEANUP()
		ENDIF

		RUN_MAIN_CLIENT_LOGIC()
		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			RUN_MAIN_SERVER_LOGIC()
		ENDIF
		
		#IF IS_DEBUG_BUILD
		UPDATE_WIDGETS()
		#ENDIF
	ENDWHILE
ENDSCRIPT
#ENDIF //FEATURE_CASINO
