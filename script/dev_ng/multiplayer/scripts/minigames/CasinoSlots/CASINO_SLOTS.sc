USING "globals.sch"

USING "net_include.sch"
USING "menu_public.sch"
USING "context_control_public.sch"

USING "net_wait_zero.sch"

USING "net_casino.sch"
USING "net_realty_casino.sch"
USING "website_public.sch"
USING "casino_slots_rng.sch"
USING "net_simple_interior_private.sch"
USING "net_casino_peds_anim_data.sch"

CONST_INT SLOTS_REEL_SPIN_TIME 	5500
CONST_INT SLOTS_WHEEL_SPIN_TIME	5500

CONST_INT SLOTS_REEL_SPIN_TIME_FAILSAFE 	10000

CONST_INT SLOTS_WHEEL_SEGMENT_DEG_SIZE		30

CONST_INT SLOTS_WHEEL_PRIZE_AMMO_PISTOL			1
CONST_INT SLOTS_WHEEL_PRIZE_AMMO_PISTOLX2		2
CONST_INT SLOTS_WHEEL_PRIZE_AMMO_SMG			3
CONST_INT SLOTS_WHEEL_PRIZE_AMMO_SMGX2			4
CONST_INT SLOTS_WHEEL_PRIZE_AMMO_SHOTGUN		5
CONST_INT SLOTS_WHEEL_PRIZE_AMMO_SHOTGUNX2		6
CONST_INT SLOTS_WHEEL_PRIZE_AMMO_RIFLE			7
CONST_INT SLOTS_WHEEL_PRIZE_AMMO_RIFLEX2		8
CONST_INT SLOTS_WHEEL_PRIZE_AMMO_MACHINEGUN		9
CONST_INT SLOTS_WHEEL_PRIZE_AMMO_SNIPER			10
CONST_INT SLOTS_WHEEL_PRIZE_AMMO_MINIGUN		11
CONST_INT SLOTS_WHEEL_PRIZE_AMMO_GRENADE		12

CONST_INT FAME_OR_SHAME_PRIZE_CAT_CLOTHING	1
CONST_INT FAME_OR_SHAME_PRIZE_CAT_ACC		2
CONST_INT FAME_OR_SHAME_PRIZE_CAT_JUDGE		3

CONST_INT FAME_OR_SHAME_PRIZE_MAX		36

STRUCT FAME_OR_SHAME_WHEEL_PRIZE
	STATS_PACKED packedStat
	TEXT_LABEL_23 prizeLabel
	INT iPrizeCategory
	INT iID
ENDSTRUCT

INT iFameOrShamePrizeWheelAwarded = -1

STRUCT SLOT_MACHINE_SPINNER
	//INT iInitPosition
	INT iPrevPos
	INT iCurrentPos
	INT iNextPos
	
	INT iSpinCounter
ENDSTRUCT

//CONST_INT LSM_BS_SPINNING	0

LOCAL_SLOT_MACHINE  localSlots[MAX_SLOT_MACHINES]

LOCAL_SLOT_MACHINE_SETUP  slotSetup

CONST_INT LOCAL_STATE_CHECK_FOR_LOCATE		0
CONST_INT LOCAL_STATE_REQUEST_MACHINE		1
CONST_INT LOCAL_STATE_MEMBERSHIP_PURCHASE	2
CONST_INT LOCAL_STATE_DISCLOSURE			3
CONST_INT LOCAL_STATE_ENTER_MACHINE			4
CONST_INT LOCAL_STATE_USING_MACHINE			5
CONST_INT LOCAL_STATE_LEAVE_MACHINE			6
INT iClientState

CONST_INT LOCAL_BS_SETUP_CONTROLS			0
CONST_INT LOCAL_BS_MACHINE_IN_USE_HELP		1
CONST_INT LOCAL_BS_TOOK_AWAY_CONTROL		2
CONST_INT LOCAL_BS_BET_TRANS_STARTED		3
CONST_INT LOCAL_BS_BET_TRANS_COMPLETE		4
CONST_INT LOCAL_BS_BET_PRESSED_SPIN			5
CONST_INT LOCAL_BS_BET_CANT_PLAY_HELP		6
CONST_INT LOCAL_BS_FPS_CAMERA_SET			7
CONST_INT LOCAL_BS_SWAPPED_BLURRY_REELS1	8
CONST_INT LOCAL_BS_SWAPPED_BLURRY_REELS2	9
CONST_INT LOCAL_BS_SWAPPED_BLURRY_REELS3	10
CONST_INT LOCAL_BS_SWAPPED_BLURRY_WHEEL		11
CONST_INT LOCAL_BS_MOVED_VIEW_TO_WHEEL		12
CONST_INT LOCAL_BS_PAUSED_CHIPS_DISPLAY		13
CONST_INT LOCAL_BS_LARGE_PAYOUT				14
CONST_INT LOCAL_BS_CRUEL_LOSE				15
CONST_INT LOCAL_BS_WHEEL_WIN				16
CONST_INT LOCAL_BS_FINISHED_SPINNING_REEL	17
CONST_INT LOCAL_BS_REQUESTED_ANIMS			18
CONST_INT LOCAL_BS_LOADED_DISCLOSURE		19
CONST_INT LOCAL_BS_ACCEPTED_DISCLOSURE		20
CONST_INT LOCAL_BS_VIEWING_HELP_PAGE1		21
CONST_INT LOCAL_BS_VIEWING_HELP_PAGE2		22
CONST_INT LOCAL_BS_LOADED_MENU_HEADER		23
CONST_INT LOCAL_BS_DETERMINED_RESULT		24
CONST_INT LOCAL_BS_WAS_SPINNING				25
CONST_INT LOCAL_BS_SKIP_WALK_TO_POS			26
CONST_INT LOCAL_BS_PRESSED_BET				27
CONST_INT LOCAL_BS_PRESSED_BET_MAX			28
CONST_INT LOCAL_BS_JACKPOT_PAYOUT			29
CONST_INT LOCAL_BS_BYPASS_NORMAL_ENTRY		30
CONST_INT LOCAL_BS_CAN_BYPASS_NORMAL_ENTRY	31
//CONST_INT LOCAL_BS_USING_PULL_HANDLE		18
//CONST_INT LOCAL_BS_NET_MACH_FOR_PULL		19

INT iLocalBS

CONST_INT LOCAL_BS2_MEDIUM_WIN						0
CONST_INT LOCAL_BS2_ACCEPTED_DISCLOSURE2			1
CONST_INT LOCAL_BS2_FORCE_CONTROL_UPDATE_AFTER_SPIN	2

INT iLocalBS2

CONST_INT ENTER_SEAT_STAGE_WALK_TO			0
CONST_INT ENTER_SEAT_STAGE_SIT_ANIM			1
CONST_INT ENTER_SEAT_STAGE_ANIM_PLAYING		2
CONST_INT ENTER_SEAT_STAGE_FINISHED			3
INT iEnterSeatStage

CONST_INT ANIM_SLOTS_ENTER_LEFT				0
CONST_INT ANIM_SLOTS_ENTER_LEFT_SHORT		1
CONST_INT ANIM_SLOTS_ENTER_RIGHT			2
CONST_INT ANIM_SLOTS_ENTER_RIGHT_SHORT		3
CONST_INT ANIM_SLOTS_BASE					4
CONST_INT ANIM_SLOTS_BASE_TO_BET_ONE		5
CONST_INT ANIM_SLOTS_BASE_TO_BET_MAX		6
CONST_INT ANIM_SLOTS_BASE_TO_AUTOSPIN		7
CONST_INT ANIM_SLOTS_BASE_TO_SPIN			8
CONST_INT ANIM_SLOTS_BASE_TO_PULL			9
CONST_INT ANIM_SLOTS_BETIDLE				10
CONST_INT ANIM_SLOTS_BETIDLE_TO_BET_ONE		11
CONST_INT ANIM_SLOTS_BETIDLE_TO_BET_MAX		12
CONST_INT ANIM_SLOTS_BETIDLE_TO_AUTOSPIN	13
CONST_INT ANIM_SLOTS_BETIDLE_TO_SPIN		14
CONST_INT ANIM_SLOTS_BETIDLE_TO_PULL		15
CONST_INT ANIM_SLOTS_BETIDLE_TO_BASE		16
CONST_INT ANIM_SLOTS_SPINNING_REELS			17
CONST_INT ANIM_SLOTS_WIN					18
CONST_INT ANIM_SLOTS_WIN_BIG				19
CONST_INT ANIM_SLOTS_WIN_TO_WHEEL			20
CONST_INT ANIM_SLOTS_WHEEL					21
CONST_INT ANIM_SLOTS_WHEEL_TO_WIN			22
CONST_INT ANIM_SLOTS_LOSE					23
CONST_INT ANIM_SLOTS_LOSE_CRUEL				24
CONST_INT ANIM_SLOTS_EXIT_LEFT				25
CONST_INT ANIM_SLOTS_EXIT_LEFT_BETIDLE		26
CONST_INT ANIM_SLOTS_EXIT_RIGHT				27
CONST_INT ANIM_SLOTS_EXIT_RIGHT_BETIDLE		28

INT iAnimSyncedScene
INT iCurrentAnim
VECTOR vCurrentAnimLoc
VECTOR vCurrentAnimRot

INT iLastBaseVar = -1
INT iLastBetIdleVar = -1

INT iEntryAnim

CONST_INT SLOT_SOUND_NO_WIN					1
CONST_INT SLOT_SOUND_SMALL_WIN				2
CONST_INT SLOT_SOUND_BIG_WIN				3
CONST_INT SLOT_SOUND_JACKPOT				4
CONST_INT SLOT_SOUND_PLACE_BET				5
CONST_INT SLOT_SOUND_PLACE_BET_MAX			6
CONST_INT SLOT_SOUND_SPINNING				7
CONST_INT SLOT_SOUND_START_SPIN				8
CONST_INT SLOT_SOUND_REEL_STOP				9
CONST_INT SLOT_SOUND_REEL_STOP_ON_PRIZE		10
CONST_INT SLOT_SOUND_WELCOME				11
CONST_INT SLOT_SOUND_SPIN_WHEEL				12
CONST_INT SLOT_SOUND_WHEEL_WIN				13

INT iBS_MachineSpinAudio[2]

#IF NOT IS_DEBUG_BUILD
CONST_INT MAX_SLOT_ATTRACT_SOUNDS	4
#ENDIF
#IF IS_DEBUG_BUILD
INT MAX_SLOT_ATTRACT_SOUNDS = 4
#ENDIF

STRUCT SLOTS_ATTRACT_AUDIO
	INT iMachine
	INT iSoundID
ENDSTRUCT 
SLOTS_ATTRACT_AUDIO slotsAttract[20]  //TEMP NEED FINAL VALUES FROM AUDIO!!!

//CONST_INT CAM_SLOTS_ENTER				-2
//CONST_INT CAM_SLOTS_FACE_MACHINE_WHEEL	-1
//CONST_INT CAM_SLOTS_FACE_MACHINE		0
CONST_INT CAM_SLOTS_FIRST_PERS			0
CONST_INT CAM_SLOTS_THIRD_PERS			1

INT iCurrentCamera = -1

BOOL bUsingPCControls

//SCRIPT_TIMER stFPSCamTimeout

SCRIPT_TIMER stBetIdleTimeout
//SCRIPT_TIMER stIdleTimeout

SCRIPT_TIMER stExitCapsuleOverride

SCRIPT_TIMER stReelStopDelay[MAX_SLOT_MACHINES][2]

SCRIPT_TIMER stWheelStartDelay[MAX_SLOT_MACHINES]

CONST_INT GAME_HELP_BETTING 		1
CONST_INT GAME_HELP_WIN				2
CONST_INT GAME_HELP_WIN_PRE_WHEEL	3
INT iLastHelpType 

SCRIPT_TIMER stTimerWinHelp

STRUCT METRIC_STRUCT
	SCRIPT_TIMER stLightTimer
	INT iChipDelta
	INT iWins
	INT iLoses
	INT iBetAmount
ENDSTRUCT
METRIC_STRUCT slotMetrics

RESULT_STRUCT localResultOfSpin


BOOL bSetMetricTimerHeavy
BOOL bSetMetricTimerLight
TIME_DATATYPE MetricTimerHeavy
TIME_DATATYPE MetricTimerLight

STRUCT MACHINE_SCALEFORM
	SCALEFORM_INDEX si_SlotsLocalMovie
	INT iRenderTargetID = -1
ENDSTRUCT
MACHINE_SCALEFORM machineUI


STRUCT ANIM_DETAILS
	TEXT_LABEL_63 name
	BOOL bLooping
	BOOL bHoldLastFrame
ENDSTRUCT
ANIM_DETAILS tempAnimDetails

INT iSlotMachineContext = NEW_CONTEXT_INTENTION

INT iCurrentlyUsedMachine = -1
INT iScaleformMachine = -1
INT iTriggeringMachine = -1

INT iPayoutMultipler = 1

INT iNearbyCounter 
INT iNearbyMachine = -1

//OBJECT_INDEX slotMachine[MAX_SLOT_MACHINES]
OBJECT_INDEX slotReel[MAX_SLOT_MACHINES][NUM_REELS]
OBJECT_INDEX slotPrizeWheel[MAX_SLOT_MACHINES]

//CAMERA_INDEX playingCamera

OBJECT_INDEX slotBlurryReel[NUM_REELS]
OBJECT_INDEX slotBlurryWheel

STRUCT NPC_SLOT_DATA
	INT iUsedBS[2]
	INT iSpinningBS[2]
	SCRIPT_TIMER spinTimer[MAX_SLOT_MACHINES]
	FLOAT fReelPropSlotResults[MAX_SLOT_MACHINES][NUM_REELS]
ENDSTRUCT
NPC_SLOT_DATA NPCData

//OBJECT_INDEX usedSlotMachineObject
GENERIC_TRANSACTION_STATE eChipsTransactionResult = TRANSACTION_STATE_DEFAULT

#IF IS_DEBUG_BUILD
//OBJECT_INDEX slotBigTestReel[150]
//FLOAT slotBigReelTestRots[150]
INT db_iOverrideMessageID
BOOL bPrintDebugOuput = FALSE
//OBJECT_INDEX slotLineObject[MAX_SLOT_MACHINES]

//FLOAT db_CentreX = 0.435
//FLOAT db_CentreY = 0.090
//FLOAT db_Width = 0.810
//FLOAT db_Height = 0.151

BOOL bOverrideSpinSpeed
INT iReelSpinSpeed	= 600
INT iWheelSpinSpeed	= 300
#ENDIF

//SERVER DATA
//SERVER bitset
CONST_INT SERVER_BD_BS_INIT_SLOT_MACHINES	0

// The server broadcast data.
// Everyone can read this data, only the server can update it
STRUCT ServerBroadcastData
	INT iReelVirtSlotResults[MAX_SLOT_MACHINES][NUM_REELS]
	FLOAT fReelPropSlotResults[MAX_SLOT_MACHINES][NUM_REELS]
	
	//FLOAT fReelPropIdlePos[MAX_SLOT_MACHINES][NUM_REELS]
	
	INT iPrizeWheelResult[MAX_SLOT_MACHINES]

	SCRIPT_TIMER spinTimer[MAX_SLOT_MACHINES]
	INT iMachineUsedBy[MAX_SLOT_MACHINES]
	INT iBS_MachineSpinning[2]
	INT iBS_HasResult[2]
	INT iBS
ENDSTRUCT
ServerBroadcastData serverBD

//PLAYER DATA
// PLAYER BITSET
CONST_INT PLAYER_BD_BS_SPINNING				0
CONST_INT PLAYER_BD_BS_PROCESSED_RESULT		1
CONST_INT PLAYER_BD_BS_FINISHED_SPINNING	2
CONST_INT PLAYER_BD_BS_ABORTED_SPINNING		3

STRUCT PlayerBroadcastData
	INT iCurrentMachineID = -1
	INT iReelVirtSlotResults[NUM_REELS]
	FLOAT fReelPropSlotResult[NUM_REELS]
	INT iPrizeWheelResult

	INT iBS
ENDSTRUCT
PlayerBroadcastData playerBD[NUM_NETWORK_PLAYERS]

#IF IS_DEBUG_BUILD
//INT db_iPayouts[PAYOUT_MAX_PAYOUTS]
//INT db_iMachine = 0
INT iOverrideReelResult[NUM_REELS]
INT iOverrideWheelResult = -1

INT db_iMachine = 0
INT db_iPrevMachine = -1

//BOOL bRunBigReelTest

//INT db_iServerCurrentPlayerIndexDebug

//INT iBalance = 1000

FLOAT db_fExpectedPayoutPercentage
//BOOL db_bDrawDebugSpin
BOOL bCreateSlotMachineWidgets
BOOL bClearSlotMachineWidgets
BOOL db_bBlankSlotMachineWidgets
BOOL bOutputMachine
BOOL bKillScript
BOOL bd_bTestMachine
BOOL db_bDisplaySlotIDs

BOOL db_bTestRotation
BOOL db_bAllowPosOverride = TRUE
FLOAT db_fSlot1Rot, db_fSlot2Rot, db_fSlot3Rot

BOOL db_bRunningDebugMachine
WIDGET_GROUP_ID ScriptWidgetGroup
WIDGET_GROUP_ID SlotMachineWidget
BOOL bOutputOdds

STRUCT DEBUG_2D_TEXT_DATA
	VECTOR vTextItemPosition
	INT TextCol[3]
ENDSTRUCT

CONST_INT DB_SLOT_TITLE			0
CONST_INT DB_SLOT_REEL1			1
CONST_INT DB_SLOT_REEL2			2
CONST_INT DB_SLOT_REEL3			3
CONST_INT DB_SLOT_LAST_WIN		4
CONST_INT DB_SLOT_LAST_WIN_VAL	5
CONST_INT DB_SLOT_BET			6
CONST_INT DB_SLOT_BET_VAL		7
CONST_INT DB_SLOT_PAYOUT		8
CONST_INT DB_SLOT_PAYOUT_VAL	9
CONST_INT DB_SLOT_BALANCE		10
CONST_INT DB_SLOT_BALANCE_VAL	11
CONST_INT DB_SLOT_MAX			12

STRUCT DEBUG_TEXT_SLOT_MACHINE
	DEBUG_2D_TEXT_DATA items[DB_SLOT_MAX]
	FLOAT fReelSymbolOffset
ENDSTRUCT
DEBUG_TEXT_SLOT_MACHINE debugMachine
#ENDIF

FUNC INT GET_BITSET_ARRAY_INDEX(INT iIndex)
	RETURN FLOOR(TO_FLOAT(iIndex)/32)
ENDFUNC

FUNC INT GET_BITSET_ELEMENT_ID_WITH_ARRAY(INT iIndex)
	RETURN (iIndex- (GET_BITSET_ARRAY_INDEX(iIndex)*32))
ENDFUNC

PROC GET_SLOT_MACHINE_POSITION(INT iMachine, VECTOR &vReturnCoords, FLOAT &fHeading)
	SWITCH iMachine
		//main left
		CASE 0 
			vReturnCoords = <<1100.4828,230.4082,-50.8409>>	 //01
			fHeading = 45
		BREAK
		CASE 1 
			vReturnCoords = <<1100.9390, 231.0017, -50.8409>> //02
			fHeading = 60
		BREAK
		CASE 2 
			vReturnCoords = <<1101.2209,231.6943, -50.8409>> //03
			fHeading = 75
		BREAK
		CASE 3 
			vReturnCoords = <<1101.3228,232.4321, -50.8409>> //04
			fHeading = 90
		BREAK
		CASE 4 
			vReturnCoords = <<1101.2295,233.1719, -50.8409>> //05
			fHeading = 105
		BREAK
		//main top
		CASE 5 
			vReturnCoords = <<1108.9385,239.4797,-50.8409>>	 //01
			fHeading = -45
		BREAK
		CASE 6 
			vReturnCoords = <<1109.5358,239.0278, -50.8409>> //02
			fHeading = -30
		BREAK
		CASE 7 
			vReturnCoords = <<1110.2292,238.7428, -50.8409>> //03
			fHeading = -15
		BREAK
		CASE 8 
			vReturnCoords = <<1110.9741,238.6420, -50.8409>> //04
			fHeading = 0
		BREAK
		CASE 9 
			vReturnCoords = <<1111.7162,238.7384, -50.8409>> //05
			fHeading = 15
		BREAK
		CASE 10 
			vReturnCoords = <<1112.4067,239.0216, -50.8409>> //06
			fHeading = 30
		BREAK
		CASE 11 
			vReturnCoords = <<1112.9991,239.4742, -50.8409>> //01
			fHeading = 45
		BREAK
		//main right
		CASE 12
			vReturnCoords = <<1120.8530,233.1621, -50.8409>> //01
			fHeading = -105
		BREAK
		CASE 13
			vReturnCoords = <<1120.7528,232.4272, -50.8409>> //02
			fHeading = -90
		BREAK
		CASE 14 
			vReturnCoords = <<1120.8533,231.6886, -50.8409>> //03
			fHeading = -75
		BREAK
		CASE 15 
			vReturnCoords = <<1121.1350,230.9999, -50.8409>> //04
			fHeading = -60
		BREAK
		CASE 16 
			vReturnCoords = <<1121.5918,230.4106, -50.8409>> //05
			fHeading = -45
		BREAK
		
		//left front pole
		CASE 17
			vReturnCoords = <<1104.5724,229.4451, -50.8409>> //04
			fHeading = -36
		BREAK
		CASE 18
			vReturnCoords = <<1104.3022,230.3183, -50.8409>> //05
			fHeading = -108
		BREAK
		CASE 19
			vReturnCoords = <<1105.0492,230.8450, -50.8409>> //01
			fHeading = 180
		BREAK
		CASE 20
			vReturnCoords = <<1105.7809,230.2973, -50.8409>> //02
			fHeading = 108
		BREAK
		CASE 21
			vReturnCoords = <<1105.4862,229.4322, -50.8409>> //03
			fHeading = 36
		BREAK
		
		//left back pole
		CASE 22
			vReturnCoords = <<1108.0054,233.9177, -50.8409>> //05
			fHeading = -36
		BREAK
		CASE 23
			vReturnCoords = <<1107.7354,234.7909, -50.8409>> //06
			fHeading = -108
		BREAK
		CASE 24
			vReturnCoords = <<1108.4823,235.3176, -50.8409>> //02
			fHeading = 180
		BREAK
		CASE 25
			vReturnCoords = <<1109.2140,234.7699, -50.8409>> //03
			fHeading = 108
		BREAK
		CASE 26
			vReturnCoords = <<1108.9193,233.9048, -50.8409>> //04
			fHeading = 36
		BREAK
		
		//right back pole
		CASE 27
			vReturnCoords = <<1113.6398,233.6755, -50.8409>> //04
			fHeading = -36
		BREAK
		CASE 28
			vReturnCoords = <<1113.3696,234.5486, -50.8409>> //05
			fHeading = -108
		BREAK
		CASE 29
			vReturnCoords = <<1114.1166,235.0753, -50.8409>> //01
			fHeading = 180
		BREAK
		CASE 30
			vReturnCoords = <<1114.8484,234.5277, -50.8409>> //02
			fHeading = 108
		BREAK
		CASE 31
			vReturnCoords = <<1114.5536,233.6625, -50.8409>> //03
			fHeading = 36
		BREAK
		
		//right front pole
		CASE 32
			vReturnCoords = <<1116.6624,228.8896, -50.8409>> //05
			fHeading = -36
		BREAK
		CASE 33
			vReturnCoords = <<1116.3922,229.7628, -50.8409>> //06
			fHeading = -108
		BREAK
		CASE 34
			vReturnCoords = <<1117.1392,230.2895, -50.8409>> //02
			fHeading = 180
		BREAK
		CASE 35
			vReturnCoords = <<1117.8710,229.7419, -50.8409>> //03
			fHeading = 108
		BREAK
		CASE 36
			vReturnCoords = <<1117.5762,228.8767, -50.8409>> //04
			fHeading = 36
		BREAK
		
		//back room bottom wall
		CASE 37
			vReturnCoords = <<1129.6400,250.4510, -52.0409>> //01
			fHeading = 180
		BREAK
		CASE 38
			vReturnCoords = <<1130.3765,250.3577, -52.0409>> //06
			fHeading = 165
		BREAK
		CASE 39
			vReturnCoords = <<1131.0624,250.0776, -52.0409>> //05
			fHeading = 150
		BREAK
		CASE 40
			vReturnCoords = <<1131.6548,249.6264, -52.0409>> //04
			fHeading = 135
		BREAK
		CASE 41
			vReturnCoords = <<1132.1093,249.0355, -52.0409>> //03
			fHeading = 120
		BREAK
		CASE 42
			vReturnCoords = <<1132.3961,248.3382, -52.0409>> //02
			fHeading = 105
		BREAK
		CASE 43
			vReturnCoords = <<1132.4922,247.5984, -52.0409>> //01
			fHeading = 90
		BREAK
		
		//back room left pole
		CASE 44
			vReturnCoords = <<1133.9524,256.1037, -52.0409>> //03
			fHeading = -45
		BREAK
		CASE 45
			vReturnCoords = <<1133.8274,256.9098, -52.0409>> //04
			fHeading = -117
		BREAK
		CASE 46
			vReturnCoords = <<1134.5560,257.2778, -52.0409>> //05
			fHeading = 171
		BREAK
		CASE 47
			vReturnCoords = <<1135.1316,256.6990, -52.0409>> //01
			fHeading = 99
		BREAK
		CASE 48
			vReturnCoords = <<1134.7590,255.9734, -52.0409>> //02
			fHeading = 27
		BREAK
		
		//back room right pole
		CASE 49
			vReturnCoords = <<1138.1951,251.8611, -52.0409>> //04
			fHeading = -45
		BREAK
		CASE 50
			vReturnCoords = <<1138.0703,252.6677, -52.0409>> //05
			fHeading = -117
		BREAK
		CASE 51
			vReturnCoords = <<1138.7986,253.0363, -52.0409>> //06
			fHeading = 171
		BREAK
		CASE 52
			vReturnCoords = <<1139.3722,252.4563, -52.0409>> //02
			fHeading = 99
		BREAK
		CASE 53
			vReturnCoords = <<1138.9995,251.7306, -52.0409>> //03
			fHeading = 27
		BREAK
		
	ENDSWITCH
ENDPROC

FUNC INT GET_SLOT_MACHINE_WHEEL_PRIZE(INT iMachineType, INT iWheelPos)
	
	SWITCH iMachineType
		CASE MACHINE_8_EVACUATOR
			SWITCH iWheelPos
				CASE 0
					RETURN SLOTS_WHEEL_PRIZE_AMMO_SMG
				BREAK
				CASE 1
					RETURN SLOTS_WHEEL_PRIZE_AMMO_SHOTGUN 
				BREAK
				CASE 2
					RETURN SLOTS_WHEEL_PRIZE_AMMO_GRENADE
				BREAK
				CASE 3
					RETURN SLOTS_WHEEL_PRIZE_AMMO_RIFLEX2
				BREAK
				CASE 4
					RETURN SLOTS_WHEEL_PRIZE_AMMO_MINIGUN
				BREAK
				CASE 5
					RETURN SLOTS_WHEEL_PRIZE_AMMO_PISTOL
				BREAK
				CASE 6
					RETURN SLOTS_WHEEL_PRIZE_AMMO_SMGX2
				BREAK
				CASE 7
					RETURN SLOTS_WHEEL_PRIZE_AMMO_SHOTGUNX2
				BREAK
				CASE 8
					RETURN SLOTS_WHEEL_PRIZE_AMMO_MACHINEGUN
				BREAK
				CASE 9
					RETURN SLOTS_WHEEL_PRIZE_AMMO_RIFLE
				BREAK
				CASE 10
					RETURN SLOTS_WHEEL_PRIZE_AMMO_SNIPER
				BREAK
				CASE 11
					RETURN SLOTS_WHEEL_PRIZE_AMMO_PISTOLX2
				BREAK
			ENDSWITCH
		BREAK
		CASE MACHINE_4_FAME_OR_SHAME
			SWITCH iWheelPos
				CASE 0
				CASE 3
				CASE 6
				CASE 9
					RETURN FAME_OR_SHAME_PRIZE_CAT_CLOTHING
				BREAK
				CASE 1
				CASE 4
				CASE 7
				CASE 10
					RETURN FAME_OR_SHAME_PRIZE_CAT_ACC
				BREAK
				CASE 2
				CASE 5
				CASE 8
				CASE 11
					RETURN FAME_OR_SHAME_PRIZE_CAT_JUDGE
				BREAK
			ENDSWITCH
		BREAK
		DEFAULT 
			PRINTLN("CASINO_SLOTS: GET_SLOT_MACHINE_WHEEL_PRIZE: INVALID MACHINE TYPE: ",iMachineType )
			SCRIPT_ASSERT("CASINO_SLOTS: GET_SLOT_MACHINE_WHEEL_PRIZE: INVALID MACHINE TYPE: see Conor ")
		BREAK
	ENDSWITCH
	
	RETURN 0
ENDFUNC

PROC GET_SLOTS_AMMO_REWARDS(INT iPrize, AMMO_TYPE &ammoType, INT &iAmount)
	SWITCH iPrize
		CASE SLOTS_WHEEL_PRIZE_AMMO_PISTOL
			ammoType = AMMOTYPE_PISTOL
			iAmount = 1500
		BREAK
		CASE SLOTS_WHEEL_PRIZE_AMMO_PISTOLX2
			ammoType = AMMOTYPE_PISTOL
			iAmount = 3000
		BREAK
		CASE SLOTS_WHEEL_PRIZE_AMMO_SMG
			ammoType = AMMOTYPE_SMG
			iAmount = 2000
		BREAK
		CASE SLOTS_WHEEL_PRIZE_AMMO_SMGX2
			ammoType = AMMOTYPE_SMG
			iAmount = 4000	
		BREAK
		CASE SLOTS_WHEEL_PRIZE_AMMO_SHOTGUN
			ammoType = AMMOTYPE_SHOTGUN
			iAmount = 2000
		BREAK
		CASE SLOTS_WHEEL_PRIZE_AMMO_SHOTGUNX2
			ammoType = AMMOTYPE_SHOTGUN
			iAmount = 4000
		BREAK
		CASE SLOTS_WHEEL_PRIZE_AMMO_RIFLE
			ammoType = AMMOTYPE_RIFLE
			iAmount = 2500
		BREAK
		CASE SLOTS_WHEEL_PRIZE_AMMO_RIFLEX2
			ammoType = AMMOTYPE_RIFLE
			iAmount = 5000
		BREAK
		CASE SLOTS_WHEEL_PRIZE_AMMO_MACHINEGUN
			ammoType = AMMOTYPE_MG
			iAmount = 5500
		BREAK
		CASE SLOTS_WHEEL_PRIZE_AMMO_SNIPER
			ammoType = AMMOTYPE_SNIPER
			iAmount = 1000
		BREAK
		CASE SLOTS_WHEEL_PRIZE_AMMO_MINIGUN
			ammoType = AMMOTYPE_MINIGUN
			iAmount = 7000
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_FAME_OR_SHAME_WHEEL_PRIZES_DATA(INT iIndex,FAME_OR_SHAME_WHEEL_PRIZE &data)
	data.iID = iIndex
	SWITCH iIndex
		//Clothing
		CASE 0
			data.packedStat = PACKED_MP_IVE_BEEN_SHAMED_TEE
			IF IS_PLAYER_FEMALE()
				data.prizeLabel = "CLO_VWM_DECL_27"
			ELSE
				data.prizeLabel = "CLO_VWF_DECL_27"
			ENDIF
			data.iPrizeCategory = FAME_OR_SHAME_PRIZE_CAT_CLOTHING
		BREAK
		CASE 1
			data.packedStat = PACKED_MP_BLUE_IVE_BEEN_SHAMED_TEE   
			IF IS_PLAYER_FEMALE()
				data.prizeLabel = "CLO_VWM_DECL_28"
			ELSE
				data.prizeLabel = "CLO_VWF_DECL_28"
			ENDIF
			data.iPrizeCategory = FAME_OR_SHAME_PRIZE_CAT_CLOTHING
		BREAK
		CASE 2
			data.packedStat = PACKED_MP_FAME_OR_SHAME_STARS_TEE
			IF IS_PLAYER_FEMALE()
				data.prizeLabel = "CLO_VWM_DECL_33"
			ELSE
				data.prizeLabel = "CLO_VWF_DECL_33"
			ENDIF
			data.iPrizeCategory = FAME_OR_SHAME_PRIZE_CAT_CLOTHING
		BREAK
		CASE 3
			data.packedStat = PACKED_MP_RED_FAME_OR_SHAME_STARS_TEE
			IF IS_PLAYER_FEMALE()
				data.prizeLabel = "CLO_VWM_DECL_34"
			ELSE
				data.prizeLabel = "CLO_VWF_DECL_34"
			ENDIF
			data.iPrizeCategory = FAME_OR_SHAME_PRIZE_CAT_CLOTHING
		BREAK
		CASE 4
			data.packedStat = PACKED_MP_NO_TALENT_REQUIRED_TEE 
			IF IS_PLAYER_FEMALE()
				data.prizeLabel = "CLO_VWM_DECL_35"
			ELSE
				data.prizeLabel = "CLO_VWF_DECL_35"
			ENDIF
			data.iPrizeCategory = FAME_OR_SHAME_PRIZE_CAT_CLOTHING
		BREAK
		CASE 5
			data.packedStat = PACKED_MP_RED_NO_TALENT_REQUIRED_TEE
			IF IS_PLAYER_FEMALE()
				data.prizeLabel = "CLO_VWM_DECL_36"
			ELSE
				data.prizeLabel = "CLO_VWF_DECL_36"
			ENDIF
			data.iPrizeCategory = FAME_OR_SHAME_PRIZE_CAT_CLOTHING
		BREAK
		CASE 6
			data.packedStat = PACKED_MP_TEAM_TRACEY_TEE 
			IF IS_PLAYER_FEMALE()
				data.prizeLabel = "CLO_VWM_DECL_37"
			ELSE
				data.prizeLabel = "CLO_VWF_DECL_37"
			ENDIF
			data.iPrizeCategory = FAME_OR_SHAME_PRIZE_CAT_CLOTHING
		BREAK
		CASE 7
			data.packedStat = PACKED_MP_BLUE_TEAM_TRACEY_TEE
			IF IS_PLAYER_FEMALE()
				data.prizeLabel = "CLO_VWM_DECL_38"
			ELSE
				data.prizeLabel = "CLO_VWF_DECL_38"
			ENDIF
			data.iPrizeCategory = FAME_OR_SHAME_PRIZE_CAT_CLOTHING
		BREAK
		CASE 8
			data.packedStat = PACKED_MP_MONKEY_BUSINESS_TEE
			IF IS_PLAYER_FEMALE()
				data.prizeLabel = "CLO_VWM_DECL_39"
			ELSE
				data.prizeLabel = "CLO_VWF_DECL_39"
			ENDIF
			data.iPrizeCategory = FAME_OR_SHAME_PRIZE_CAT_CLOTHING
		BREAK
		CASE 9
			data.packedStat = PACKED_MP_RED_MONKEY_BUSINESS_TEE
			IF IS_PLAYER_FEMALE()
				data.prizeLabel = "CLO_VWM_DECL_40"
			ELSE
				data.prizeLabel = "CLO_VWF_DECL_40"
			ENDIF
			data.iPrizeCategory = FAME_OR_SHAME_PRIZE_CAT_CLOTHING
		BREAK
		CASE 10
			data.packedStat = PACKED_MP_FAME_OR_SHAME_LOGO_TEE 
			IF IS_PLAYER_FEMALE()
				data.prizeLabel = "CLO_VWM_DECL_41"
			ELSE
				data.prizeLabel = "CLO_VWF_DECL_41"
			ENDIF
			data.iPrizeCategory = FAME_OR_SHAME_PRIZE_CAT_CLOTHING
		BREAK
		CASE 11
			data.packedStat = PACKED_MP_BLUE_FAME_OR_SHAME_LOGO_TEE
			IF IS_PLAYER_FEMALE()
				data.prizeLabel = "CLO_VWM_DECL_42"
			ELSE
				data.prizeLabel = "CLO_VWF_DECL_42"
			ENDIF
			data.iPrizeCategory = FAME_OR_SHAME_PRIZE_CAT_CLOTHING
		BREAK
		CASE 12
			data.packedStat = PACKED_MP_STARS_FAME_OR_SHAME_SILK_ROBE
			IF IS_PLAYER_FEMALE()
				data.prizeLabel = "CLO_VWM_U_20_6"
			ELSE
				data.prizeLabel = "CLO_VWF_U_18_6"
			ENDIF
			data.iPrizeCategory = FAME_OR_SHAME_PRIZE_CAT_CLOTHING
		BREAK
		CASE 13
			data.packedStat = PACKED_MP_BLACK_FAME_OR_SHAME_SILK_ROBE
			IF IS_PLAYER_FEMALE()
				data.prizeLabel = "CLO_VWM_U_20_7"
			ELSE
				data.prizeLabel = "CLO_VWF_U_18_7"
			ENDIF
			data.iPrizeCategory = FAME_OR_SHAME_PRIZE_CAT_CLOTHING
		BREAK
		CASE 14
			data.packedStat = PACKED_MP_RED_STARS_FAME_OR_SHAME_SILK_ROBE
			IF IS_PLAYER_FEMALE()
				data.prizeLabel = "CLO_VWM_U_20_8"
			ELSE
				data.prizeLabel = "CLO_VWF_U_18_8"
			ENDIF
			data.iPrizeCategory = FAME_OR_SHAME_PRIZE_CAT_CLOTHING
		BREAK
		CASE 15
			data.packedStat = PACKED_MP_RED_FAME_OR_SHAME_SILK_ROBE
			IF IS_PLAYER_FEMALE()
				data.prizeLabel = "CLO_VWM_U_20_9"
			ELSE
				data.prizeLabel = "CLO_VWF_U_18_9"
			ENDIF
			data.iPrizeCategory = FAME_OR_SHAME_PRIZE_CAT_CLOTHING
		BREAK
		CASE 16
			data.packedStat = PACKED_MP_WHITE_FAME_OR_SHAME_SILK_ROBE
			IF IS_PLAYER_FEMALE()
				data.prizeLabel = "CLO_VWM_U_20_10"
			ELSE
				data.prizeLabel = "CLO_VWF_U_18_10"
			ENDIF
			data.iPrizeCategory = FAME_OR_SHAME_PRIZE_CAT_CLOTHING
		BREAK
		//Accesories
		CASE 17
			data.packedStat = PACKED_MP_BLACK_FAME_OR_SHAME_DEEP_SHADES
			IF IS_PLAYER_FEMALE()
				data.prizeLabel = "CLO_VWM_PEY_116"
			ELSE
				data.prizeLabel = "CLO_VWF_PEY_116"
			ENDIF
			data.iPrizeCategory = FAME_OR_SHAME_PRIZE_CAT_ACC
		BREAK
		CASE 18
			data.packedStat = PACKED_MP_RED_FAME_OR_SHAME_DEEP_SHADES
			IF IS_PLAYER_FEMALE()
				data.prizeLabel = "CLO_VWM_PEY_117"
			ELSE
				data.prizeLabel = "CLO_VWF_PEY_117"
			ENDIF
			data.iPrizeCategory = FAME_OR_SHAME_PRIZE_CAT_ACC
		BREAK
		CASE 19
			data.packedStat = PACKED_MP_BLUE_FAME_OR_SHAME_DEEP_SHADES
			IF IS_PLAYER_FEMALE()
				data.prizeLabel = "CLO_VWM_PEY_118"
			ELSE
				data.prizeLabel = "CLO_VWF_PEY_118"
			ENDIF
			data.iPrizeCategory = FAME_OR_SHAME_PRIZE_CAT_ACC
		BREAK
		CASE 20
			data.packedStat = PACKED_MP_WHITE_FAME_OR_SHAME_DEEP_SHADES
			IF IS_PLAYER_FEMALE()
				data.prizeLabel = "CLO_VWM_PEY_119"
			ELSE
				data.prizeLabel = "CLO_VWF_PEY_119"
			ENDIF
			data.iPrizeCategory = FAME_OR_SHAME_PRIZE_CAT_ACC
		BREAK
		CASE 21
			data.packedStat = PACKED_MP_GOLD_FAME_OR_SHAME_MICS
			IF IS_PLAYER_FEMALE()
				data.prizeLabel = "CLO_VWM_PE_0_0"
			ELSE
				data.prizeLabel = "CLO_VWF_PE_0_0"
			ENDIF
			data.iPrizeCategory = FAME_OR_SHAME_PRIZE_CAT_ACC
		BREAK
		CASE 22
			data.packedStat = PACKED_MP_SILVER_FAME_OR_SHAME_MICS
			IF IS_PLAYER_FEMALE()
				data.prizeLabel = "CLO_VWM_PE_0_1"
			ELSE
				data.prizeLabel = "CLO_VWF_PE_0_1"
			ENDIF
			data.iPrizeCategory = FAME_OR_SHAME_PRIZE_CAT_ACC
		BREAK
		CASE 23
			data.packedStat = PACKED_MP_RED_FAME_OR_SHAME_KRONOS
			IF IS_PLAYER_FEMALE()
				data.prizeLabel = "CLO_VWM_PW_2_6"
			ELSE
				data.prizeLabel = "CLO_VWF_PW_2_6"
			ENDIF
			data.iPrizeCategory = FAME_OR_SHAME_PRIZE_CAT_ACC
		BREAK
		CASE 24
			data.packedStat = PACKED_MP_GREEN_FAME_OR_SHAME_KRONOS 
			IF IS_PLAYER_FEMALE()
				data.prizeLabel = "CLO_VWM_PW_2_7"
			ELSE
				data.prizeLabel = "CLO_VWF_PW_2_7"
			ENDIF
			data.iPrizeCategory = FAME_OR_SHAME_PRIZE_CAT_ACC
		BREAK
		CASE 25
			data.packedStat = PACKED_MP_BLUE_FAME_OR_SHAME_KRONOS
			IF IS_PLAYER_FEMALE()
				data.prizeLabel = "CLO_VWM_PW_2_8"
			ELSE
				data.prizeLabel = "CLO_VWF_PW_2_8"
			ENDIF
			data.iPrizeCategory = FAME_OR_SHAME_PRIZE_CAT_ACC
		BREAK
		CASE 26
			data.packedStat = PACKED_MP_BLACK_FAME_OR_SHAME_KRONOS
			IF IS_PLAYER_FEMALE()
				data.prizeLabel = "CLO_VWM_PW_2_9"
			ELSE
				data.prizeLabel = "CLO_VWF_PW_2_9"
			ENDIF
			data.iPrizeCategory = FAME_OR_SHAME_PRIZE_CAT_ACC
		BREAK
		//Judges stuff
		CASE 27
			data.packedStat = PACKED_MP_AMERICA_LOVES_YOU_TEE
			IF IS_PLAYER_FEMALE()
				data.prizeLabel = "CLO_VWM_DECL_24"
			ELSE
				data.prizeLabel = "CLO_VWF_DECL_24"
			ENDIF
			data.iPrizeCategory = FAME_OR_SHAME_PRIZE_CAT_JUDGE
		BREAK
		CASE 28
			data.packedStat = PACKED_MP_BLUE_AMERICA_LOVES_YOU_TEE
			IF IS_PLAYER_FEMALE()
				data.prizeLabel = "CLO_VWM_DECL_25"
			ELSE
				data.prizeLabel = "CLO_VWF_DECL_25"
			ENDIF
			data.iPrizeCategory = FAME_OR_SHAME_PRIZE_CAT_JUDGE
		BREAK
		CASE 29
			data.packedStat = PACKED_MP_FAME_OR_SHAME_NO_EVIL_TEE
			IF IS_PLAYER_FEMALE()
				data.prizeLabel = "CLO_VWM_DECL_26"
			ELSE
				data.prizeLabel = "CLO_VWF_DECL_26"
			ENDIF
			data.iPrizeCategory = FAME_OR_SHAME_PRIZE_CAT_JUDGE
		BREAK
		CASE 30
			data.packedStat = PACKED_MP_YOURE_SO_ORIGINAL_TEE
			IF IS_PLAYER_FEMALE()
				data.prizeLabel = "CLO_VWM_DECL_29"
			ELSE
				data.prizeLabel = "CLO_VWF_DECL_29"
			ENDIF
			data.iPrizeCategory = FAME_OR_SHAME_PRIZE_CAT_JUDGE
		BREAK
		CASE 31
			data.packedStat = PACKED_MP_RED_YOURE_SO_ORIGINAL_TEE
			IF IS_PLAYER_FEMALE()
				data.prizeLabel = "CLO_VWM_DECL_30"
			ELSE
				data.prizeLabel = "CLO_VWF_DECL_30"
			ENDIF
			data.iPrizeCategory = FAME_OR_SHAME_PRIZE_CAT_JUDGE
		BREAK
		CASE 32
			data.packedStat = PACKED_MP_OH_NO_HE_DIDNT_TEE
			IF IS_PLAYER_FEMALE()
				data.prizeLabel = "CLO_VWM_DECL_31"
			ELSE
				data.prizeLabel = "CLO_VWF_DECL_31"
			ENDIF
			data.iPrizeCategory = FAME_OR_SHAME_PRIZE_CAT_JUDGE
		BREAK
		CASE 33
			data.packedStat = PACKED_MP_BLUE_OH_NO_HE_DIDNT_TEE
			IF IS_PLAYER_FEMALE()
				data.prizeLabel = "CLO_VWM_DECL_32"
			ELSE
				data.prizeLabel = "CLO_VWF_DECL_32"
			ENDIF
			data.iPrizeCategory = FAME_OR_SHAME_PRIZE_CAT_JUDGE
		BREAK
		CASE 34
			data.packedStat = PACKED_MP_YOURE_AWFUL_TEE
			IF IS_PLAYER_FEMALE()
				data.prizeLabel = "CLO_VWM_DECL_43"
			ELSE
				data.prizeLabel = "CLO_VWF_DECL_43"
			ENDIF
			data.iPrizeCategory = FAME_OR_SHAME_PRIZE_CAT_JUDGE
		BREAK
		CASE 35
			data.packedStat = PACKED_MP_RED_YOURE_AWFUL_TEE
			IF IS_PLAYER_FEMALE()
				data.prizeLabel = "CLO_VWM_DECL_44"
			ELSE
				data.prizeLabel = "CLO_VWF_DECL_44"
			ENDIF
			data.iPrizeCategory = FAME_OR_SHAME_PRIZE_CAT_JUDGE
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL GIVE_SLOT_MACHINE_WHEEL_PRIZE(INT iPrize)
	PRINTLN("CASINO_SLOTS: GIVE_SLOT_MACHINE_WHEEL_PRIZE called this frame with machine type: ",localSlots[iCurrentlyUsedMachine].iMachineType)
	IF localSlots[iCurrentlyUsedMachine].iMachineType = MACHINE_8_EVACUATOR
		AMMO_TYPE ammo
		INT iAmount
		IF iPrize = SLOTS_WHEEL_PRIZE_AMMO_GRENADE
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_GRENADE, 25,FALSE,FALSE)
			PRINTLN("CASINO_SLOTS: GIVE_SLOT_MACHINE_WHEEL_PRIZE giving player 25 grenades")
		ELSE
			GET_SLOTS_AMMO_REWARDS(iPrize,ammo,iAmount)
			ADD_PED_AMMO_BY_TYPE(PLAYER_PED_ID(),ammo, iAmount)
			PRINTLN("CASINO_SLOTS: GIVE_SLOT_MACHINE_WHEEL_PRIZE giving player ",iAmount," rounds of ammo type ",ammo)
		ENDIF
	ELIF localSlots[iCurrentlyUsedMachine].iMachineType = MACHINE_4_FAME_OR_SHAME
		iFameOrShamePrizeWheelAwarded = -1
		FAME_OR_SHAME_WHEEL_PRIZE data[20]
		INT iPrizeCount
		INT i
		INT iRand
		
		REPEAT FAME_OR_SHAME_PRIZE_MAX i
			GET_FAME_OR_SHAME_WHEEL_PRIZES_DATA(i,data[iPrizeCount])
			IF data[iPrizeCount].iPrizeCategory = iPrize
				IF NOT GET_PACKED_STAT_BOOL(data[iPrizeCount].packedStat)
					iPrizeCount++
				ENDIF
			ENDIF
		ENDREPEAT
		
		IF iPrizeCount > 0
			iRand = GET_RANDOM_INT_IN_RANGE(0,iPrizeCount)
			SET_PACKED_STAT_BOOL(data[iRand].packedStat,TRUE)
			iFameOrShamePrizeWheelAwarded = data[iRand].iID
			PRINTLN("CASINO_SLOTS: GIVE_SLOT_MACHINE_WHEEL_PRIZE awarding ",data[iRand].prizeLabel," packed stat: ",data[iRand].packedStat)
		ELSE
			GIVE_LOCAL_PLAYER_FM_XP(eXPTYPE_STANDARD, "SLOT MACHINE RP", XPTYPE_AWARDS, XPCATEGORY_RP_LUCKY_WHEEL, g_sMPTunables.iFAME_OR_SHAME_SLOT_MACHINE_RP_REWARD) 
			PRINTLN("CASINO_SLOTS: GIVE_SLOT_MACHINE_WHEEL_PRIZE NO PRIZES TO AWARD... GIVING RP = ",data[iRand].prizeLabel)
		ENDIF
	ELSE
		PRINTLN("CASINO_SLOTS: GIVE_SLOT_MACHINE_WHEEL_PRIZE:  NO A VALID MACHINE TO CALL ON!!")
	ENDIF
	SET_BIT(iLocalBS,LOCAL_BS_WHEEL_WIN)
	RETURN TRUE
ENDFUNC

PROC CLEANUP_SLOTS_SCALEFORM()
	TEXT_LABEL_23 temp
	IF HAS_SCALEFORM_MOVIE_LOADED(machineUI.si_SlotsLocalMovie)
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(machineUI.si_SlotsLocalMovie)
	ENDIF
	machineUI.iRenderTargetID = -1
	IF iScaleformMachine >= 0
		temp = "machine_0"
		temp += localSlots[iScaleformMachine].iMachineType
		temp += "a"
		IF IS_NAMED_RENDERTARGET_REGISTERED(temp)
			RELEASE_NAMED_RENDERTARGET(temp)
		ENDIF
	ENDIF
	iScaleformMachine = -1
	PRINTLN("CASINO_SLOTS: CLEANUP_SLOTS_SCALEFORM: called this frame")
ENDPROC

FUNC STRING GET_PLAYER_ANIM_DICT()
	IF IS_PLAYER_FEMALE()
		RETURN "anim_casino_a@amb@casino@games@slots@female"
	ENDIF
	RETURN "anim_casino_a@amb@casino@games@slots@male"
ENDFUNC

FUNC BOOL DOES_BOSS_HAVE_CASINO_MEMBERSHIP()
	PLAYER_INDEX bossID
	IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(),FALSE) 
		bossID = GB_GET_LOCAL_PLAYER_GANG_BOSS()
		IF bossID = INVALID_PLAYER_INDEX()
			PRINTLN("DOES_BOSS_HAVE_CASINO_MEMBERSHIP: false boss is invalid")
			RETURN FALSE
		ELSE
			#IF IS_DEBUG_BUILD
			IF IS_NET_PLAYER_OK(bossID,FALSE)
				PRINTLN("DOES_BOSS_HAVE_CASINO_MEMBERSHIP: in gang boss player ",GET_PLAYER_NAME(bossID)," membership status = ",HAS_PLAYER_PURCHASED_CASINO_MEMBERSHIP(bossID))
			ELSE
				PRINTLN("DOES_BOSS_HAVE_CASINO_MEMBERSHIP: in gang boss player #",NATIVE_TO_INT(bossID)," membership status = ",HAS_PLAYER_PURCHASED_CASINO_MEMBERSHIP(bossID))
			ENDIF
			#ENDIF
			RETURN HAS_PLAYER_PURCHASED_CASINO_MEMBERSHIP(bossID)
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC WRITE_LIGHT_METRIC_DATA(BOOL bNormalUpdate)
	IF g_sMPTunables.bENABLE_SLOT_MACHINE_LIGHT
		STRUCT_SLOTMACHINEMETRICLIGHT metricStructLight
		metricStructLight.m_casinoMetric.m_matchType = HASH("STANDARD")
		metricStructLight.m_casinoMetric.m_tableID = iCurrentlyUsedMachine
		IF IS_LOCAL_PLAYER_BANNED_FROM_BETTING()
			SWITCH GET_CASINO_GAME_BAN_REASON()
				CASE CBR_WON_TOO_MUCH		metricStructLight.m_casinoMetric.m_endReason = GET_HASH_KEY("win cutoff")		BREAK
				CASE CBR_LOST_TOO_MUCH		metricStructLight.m_casinoMetric.m_endReason = GET_HASH_KEY("loss cutoff")		BREAK
				CASE CBR_TIME_SPENT_PLAYING	metricStructLight.m_casinoMetric.m_endReason = GET_HASH_KEY("time cutoff")		BREAK
			ENDSWITCH
		ELSE
			IF bNormalUpdate
				metricStructLight.m_casinoMetric.m_endReason = HASH("NORMAL")
			ELSE
				metricStructLight.m_casinoMetric.m_endReason = HASH("QUIT")
			ENDIF
		ENDIF
		metricStructLight.m_casinoMetric.m_chipsDelta = slotMetrics.iChipDelta
		metricStructLight.m_casinoMetric.m_finalChipBalance = GET_LOCAL_PLAYERS_CASINO_CHIPS_TOTAL() 
		IF bNormalUpdate
			metricStructLight.m_casinoMetric.m_duration = 120000
		ELSE
			IF bSetMetricTimerLight
				metricStructLight.m_casinoMetric.m_duration = ABSI(GET_TIME_DIFFERENCE(MetricTimerLight, GET_NETWORK_TIME()))
			ELSE
				PRINTLN("CASINO_SLOTS: not writing duration timer not started")
			ENDIF
		ENDIF
		
		
		metricStructLight.m_casinoMetric.m_viewedLegalScreen = (IS_BIT_SET(iLocalBS,LOCAL_BS_ACCEPTED_DISCLOSURE) OR IS_BIT_SET(iLocalBS2,LOCAL_BS2_ACCEPTED_DISCLOSURE2))
		metricStructLight.m_casinoMetric.m_betAmount1 = slotMetrics.iBetAmount
		metricStructLight.m_casinoMetric.m_isHost = NETWORK_IS_HOST_OF_THIS_SCRIPT()
		IF NETWORK_GET_HOST_OF_THIS_SCRIPT() != INVALID_PARTICIPANT_INDEX()
			metricStructLight.m_casinoMetric.m_hostID =  NETWORK_GET_PLAYER_ACCOUNT_ID(NETWORK_GET_PLAYER_INDEX(NETWORK_GET_HOST_OF_THIS_SCRIPT()))
		ENDIF
		metricStructLight.m_casinoMetric.m_handsPlayed = slotMetrics.iWins+slotMetrics.iLoses 
		metricStructLight.m_casinoMetric.m_winCount = slotMetrics.iWins
		metricStructLight.m_casinoMetric.m_loseCount = slotMetrics.iLoses
		metricStructLight.m_machineStyle = localSlots[iCurrentlyUsedMachine].iMachineType
		IF DOES_PLAYER_OWN_A_CASINO_APARTMENT(PLAYER_ID())
			metricStructLight.m_casinoMetric.m_membership = HASH("VIP membership")
		ELIF HAS_LOCAL_PLAYER_PURCHASED_CASINO_MEMBERSHIP()
			metricStructLight.m_casinoMetric.m_membership = HASH("paid membership")
		ELIF DOES_BOSS_HAVE_CASINO_MEMBERSHIP()
			metricStructLight.m_casinoMetric.m_membership = HASH("gang membership")
		ELSE
			metricStructLight.m_casinoMetric.m_membership = HASH("no membership")
		ENDIF
		PLAYSTATS_CASINO_SLOT_MACHINE_LIGHT(metricStructLight)
		RESET_NET_TIMER(slotMetrics.stLightTimer)
		slotMetrics.iChipDelta = 0
		slotMetrics.iWins = 0
		slotMetrics.iLoses = 0
		slotMetrics.iBetAmount = 0
		bSetMetricTimerLight = FALSE
		
		PRINTLN("CASINO_SLOTS: WRITE_LIGHT_METRIC_DATA called this frame")
		#IF IS_DEBUG_BUILD
		IF NOT bNormalUpdate
			DEBUG_PRINTCALLSTACK()
		ENDIF
		#ENDIF
	ENDIF
ENDPROC

PROC WRITE_HEAVY_METRIC()
	IF g_sMPTunables.bENABLE_SLOT_MACHINE_LIGHT
		IF localSlots[iCurrentlyUsedMachine].iLastWin > 0
			slotMetrics.iWins++
		ELSE
			slotMetrics.iLoses++
		ENDIF
		slotMetrics.iChipDelta += localSlots[iCurrentlyUsedMachine].iLastWin - iPayoutMultipler*localSlots[iCurrentlyUsedMachine].iMachineCost
	ENDIF
	IF g_sMPTunables.bENABLE_SLOT_MACHINE_HEAVY 
		STRUCT_SLOTMACHINEMETRIC metricStruct
		metricStruct.m_casinoMetric.m_gameType = HASH("SLOTMACHINE")
		metricStruct.m_casinoMetric.m_matchType = HASH("STANDARD")
		metricStruct.m_casinoMetric.m_tableID = iCurrentlyUsedMachine
		IF IS_LOCAL_PLAYER_BANNED_FROM_BETTING()
			SWITCH GET_CASINO_GAME_BAN_REASON()
				CASE CBR_WON_TOO_MUCH		metricStruct.m_casinoMetric.m_endReason = GET_HASH_KEY("win cutoff")		BREAK
				CASE CBR_LOST_TOO_MUCH		metricStruct.m_casinoMetric.m_endReason = GET_HASH_KEY("loss cutoff")		BREAK
				CASE CBR_TIME_SPENT_PLAYING	metricStruct.m_casinoMetric.m_endReason = GET_HASH_KEY("time cutoff")		BREAK
			ENDSWITCH
		ELSE
			IF localSlots[iCurrentlyUsedMachine].iLastWin > 0
				metricStruct.m_casinoMetric.m_endReason =  HASH("win")
			ELSE
				metricStruct.m_casinoMetric.m_endReason =  HASH("lose")
			ENDIF
		ENDIF
		
		metricStruct.m_casinoMetric.m_chipsDelta = localSlots[iCurrentlyUsedMachine].iLastWin - iPayoutMultipler*localSlots[iCurrentlyUsedMachine].iMachineCost
		metricStruct.m_casinoMetric.m_finalChipBalance = GET_LOCAL_PLAYERS_CASINO_CHIPS_TOTAL() 
		metricStruct.m_casinoMetric.m_viewedLegalScreen = (IS_BIT_SET(iLocalBS,LOCAL_BS_ACCEPTED_DISCLOSURE) OR IS_BIT_SET(iLocalBS2,LOCAL_BS2_ACCEPTED_DISCLOSURE2))
		metricStruct.m_casinoMetric.m_ownPenthouse = DOES_PLAYER_OWN_A_CASINO_APARTMENT(PLAYER_ID())
		metricStruct.m_casinoMetric.m_betAmount1 = iPayoutMultipler*localSlots[iCurrentlyUsedMachine].iMachineCost
		metricStruct.m_casinoMetric.m_winAmount = localSlots[iCurrentlyUsedMachine].iLastWin
		IF iPayoutMultipler = 5
			metricStruct.m_casinoMetric.m_allIn = TRUE
		ELSE
			metricStruct.m_casinoMetric.m_allIn = FALSE
		ENDIF
		metricStruct.m_casinoMetric.m_win = localSlots[iCurrentlyUsedMachine].iLastWin > 0
		metricStruct.m_casinoMetric.m_timePlayed = ABSI(GET_TIME_DIFFERENCE(MetricTimerHeavy, GET_NETWORK_TIME()))
		metricStruct.m_casinoMetric.m_isHost = NETWORK_IS_HOST_OF_THIS_SCRIPT()
		IF NETWORK_GET_HOST_OF_THIS_SCRIPT() != INVALID_PARTICIPANT_INDEX()
			metricStruct.m_casinoMetric.m_hostID = NETWORK_GET_PLAYER_ACCOUNT_ID(NETWORK_GET_PLAYER_INDEX(NETWORK_GET_HOST_OF_THIS_SCRIPT()))
		ENDIF
		metricStruct.m_machineStyle = localSlots[iCurrentlyUsedMachine].iMachineType
		IF DOES_PLAYER_OWN_A_CASINO_APARTMENT(PLAYER_ID())
			metricStruct.m_casinoMetric.m_membership = HASH("VIP membership")
		ELIF HAS_LOCAL_PLAYER_PURCHASED_CASINO_MEMBERSHIP()
			metricStruct.m_casinoMetric.m_membership = HASH("paid membership")
		ELIF DOES_BOSS_HAVE_CASINO_MEMBERSHIP()
			metricStruct.m_casinoMetric.m_membership = HASH("gang membership")
		ELSE
			metricStruct.m_casinoMetric.m_membership = HASH("no membership")
		ENDIF
		bSetMetricTimerHeavy = FALSE
		PLAYSTATS_CASINO_SLOT_MACHINE(metricStruct)
		PRINTLN("CASINO_SLOTS: WRITE_HEAVY_METRIC: triggering heavy metric this frame")
	ENDIF
ENDPROC

PROC DRAW_GAME_HELP(INT iHelpType, INT iWinType = -1, INT iWinAmount = -1, INT iWheelPrize = -1)
	BOOL bFameOrShamePrinted
	FAME_OR_SHAME_WHEEL_PRIZE data
	IF iLastHelpType != iHelpType
		IF IS_HELP_MESSAGE_BEING_DISPLAYED()
			CLEAR_HELP()
			PRINTLN("DRAW_GAME_HELP: triggering clear help")
		ENDIF
		TEXT_LABEL_23 tempText
		SWITCH iHelpType
			CASE GAME_HELP_BETTING
				PRINT_HELP_FOREVER_WITH_2_NUMBERS_NO_SOUND("SLOTS_HELPB",localSlots[iCurrentlyUsedMachine].iMachineCost,localSlots[iCurrentlyUsedMachine].iMachineCost*5)
				PRINTLN("CASINO_SLOTS: DRAW_GAME_HELP printing GAME_HELP_BETTING")
			BREAK
			CASE GAME_HELP_WIN
			CASE GAME_HELP_WIN_PRE_WHEEL
				IF iwinType = 1
				AND localSlots[iCurrentlyUsedMachine].iMachineType = MACHINE_8_EVACUATOR
					tempText = "SLOTS_HELPW1b"
				ELSE
					tempText = "SLOTS_HELPW"
					IF iWinType >= 0
						tempText += iWinType
					ENDIF
					IF iWinType = PAYOUT_1_WILD
					OR iWinType = PAYOUT_2_WILD
					OR iWinType = PAYOUT_LINE_WILD
						tempText += localSlots[iCurrentlyUsedMachine].iMachineType
					ENDIF
				ENDIF
				IF DOES_SLOT_MACHINE_HAVE_WHEEL(iCurrentlyUsedMachine,localSlots)
					IF iWinType = PAYOUT_LINE_WILD
					AND iWheelPrize > 0
						IF localSlots[iCurrentlyUsedMachine].iMachineType = MACHINE_8_EVACUATOR
							IF iWheelPrize != SLOTS_WHEEL_PRIZE_AMMO_GRENADE
								AMMO_TYPE ammo
								INT iAmount, iCurrentMax
								GET_SLOTS_AMMO_REWARDS(iWheelPrize,ammo,iAmount)
								GET_MAX_AMMO_BY_TYPE(PLAYER_PED_ID(),ammo,iCurrentMax)
								IF iCurrentMax < iAmount
									IF ammo = AMMOTYPE_PISTOL
										tempText += 1
									ELIF ammo = AMMOTYPE_SMG
										tempText += 3
									ELIF ammo = AMMOTYPE_SHOTGUN
										tempText += 5
									ELIF ammo = AMMOTYPE_RIFLE
										tempText += 7
									ELSE
										tempText += iWheelPrize
									ENDIF
									tempText += "a"
								ELSE
									tempText += iWheelPrize
									tempText += "b"
								ENDIF
							ELSE
								tempText += iWheelPrize
							ENDIF
						ELIF localSlots[iCurrentlyUsedMachine].iMachineType = MACHINE_4_FAME_OR_SHAME
							bFameOrShamePrinted = TRUE
							IF iFameOrShamePrizeWheelAwarded = -1
								tempText = "SLOTS_HELPW74b"
								PRINT_HELP_FOREVER_WITH_NUMBER_NO_SOUND(tempText,g_sMPTunables.iFAME_OR_SHAME_SLOT_MACHINE_RP_REWARD)
							ELSE
								GET_FAME_OR_SHAME_WHEEL_PRIZES_DATA(iFameOrShamePrizeWheelAwarded,data)
								tempText = "SLOTS_HELPW74a"
								PRINT_TICKER_WITH_STRING_AND_INT(tempText,data.prizeLabel,iWinAmount)
								//PRINT_HELP_FOREVER_WITH_STRING_NO_SOUND(tempText,data.prizeLabel)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				IF NOT bFameOrShamePrinted
					PRINT_HELP_FOREVER_WITH_NUMBER_NO_SOUND(tempText,iWinAmount)
				ENDIF
				REINIT_NET_TIMER(stTimerWinHelp,TRUE)
				PRINTLN("CASINO_SLOTS: DRAW_GAME_HELP printing GAME_HELP_WIN: ",tempText)
			BREAK
		ENDSWITCH
		iLastHelpType = iHelpType
	ENDIF
ENDPROC

PROC CLEAR_GAME_HELP()
	IF iLastHelpType != 0
		IF IS_HELP_MESSAGE_BEING_DISPLAYED()
			CLEAR_HELP()
			PRINTLN("CLEAR_GAME_HELP: triggering clear help")
		ENDIF
		iLastHelpType = 0
	ENDIF
ENDPROC


FUNC TEXT_LABEL_31 GET_MACHINE_UI_HEADER(INT iMachineType)
	TEXT_LABEL_31 theText
	SWITCH iMachineType
		CASE MACHINE_1_ANGEL_AND_KNIGHT
			theText = "CasinoUI_Slots_Angel"
		BREAK
		CASE MACHINE_2_IMPOTENT_RAGE
			theText = "CasinoUI_Slots_Impotent"
		BREAK
		CASE MACHINE_3_REPUBLICAN_SPACE_RANGERS
			theText = "CasinoUI_Slots_Ranger"
		BREAK
		CASE MACHINE_4_FAME_OR_SHAME
			theText = "CasinoUI_Slots_Fame"
		BREAK
		CASE MACHINE_5_DEITY_OF_THE_SUN
			theText = "CasinoUI_Slots_Deity"
		BREAK
		CASE MACHINE_6_KNIFE_AFTER_DARK
			theText = "CasinoUI_Slots_Knife"
		BREAK
		CASE MACHINE_7_THE_DIAMOND	
			theText = "CasinoUI_Slots_Diamond"
		BREAK
		CASE MACHINE_8_EVACUATOR		
			theText = "CasinoUI_Slots_Evacuator"
		BREAK
	ENDSWITCH
	RETURN theText
ENDFUNC


/// PURPOSE:
///    Cleans up anything required when script terminates 
PROC SCRIPT_CLEANUP()
	IF iSlotMachineContext != NEW_CONTEXT_INTENTION
		RELEASE_CONTEXT_INTENTION(iSlotMachineContext)
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("dlc_vw_casino_slot_machines_playing")
		STOP_AUDIO_SCENE("dlc_vw_casino_slot_machines_playing")
	ENDIF
	
	IF iCurrentlyUsedMachine != -1
		IF IS_BIT_SET(iLocalBS,LOCAL_BS_LOADED_MENU_HEADER)
			TEXT_LABEL_31 theText = GET_MACHINE_UI_HEADER(localSlots[iCurrentlyUsedMachine].iMachineType)
			SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(theText)
			CLEAR_BIT(iLocalBS,LOCAL_BS_LOADED_MENU_HEADER)
		ENDIF
		IF IS_BIT_SET(iLocalBS, LOCAL_BS_LOADED_DISCLOSURE)
			CLEAR_MENU_DATA()
			CLEANUP_MENU_ASSETS()
     		CLEAR_BIT(iLocalBS, LOCAL_BS_LOADED_DISCLOSURE)
		ENDIF
		WRITE_LIGHT_METRIC_DATA(FALSE)
	ENDIF
	
	CLEAR_GAME_HELP()
	CLEANUP_SLOTS_SCALEFORM()
	IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSFive, BS_SIMPLE_INTERIOR_GLOBAL_BS_FIVE_PLAYING_SLOTS)
		CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSFive, BS_SIMPLE_INTERIOR_GLOBAL_BS_FIVE_PLAYING_SLOTS)
		SET_PLAYER_FINISHED_PLAYING_CASINO_GAME()
	ENDIF
	IF IS_BIT_SET(iLocalBS,LOCAL_BS_PAUSED_CHIPS_DISPLAY)
		IF IS_CASINO_CHIPS_HUD_UPDATE_PAUSED()
			SET_PAUSE_CASINO_CHIPS_HUD_UPDATE(FALSE)
		ENDIF
		CLEAR_BIT(iLocalBS,LOCAL_BS_PAUSED_CHIPS_DISPLAY)
	ENDIF
	INT i,x
	
	IF IS_BIT_SET(iLocalBS,LOCAL_BS_REQUESTED_ANIMS)
		REMOVE_ANIM_DICT(GET_PLAYER_ANIM_DICT())
	 	CLEAR_BIT(iLocalBS,LOCAL_BS_REQUESTED_ANIMS)
	ENDIF
	REPEAT MAX_SLOT_MACHINES x
		REPEAT 3 i
			IF DOES_ENTITY_EXIST(slotReel[x][i])
				DELETE_OBJECT(slotReel[x][i])
			ENDIF
		ENDREPEAT
		IF DOES_ENTITY_EXIST(slotPrizeWheel[x])
			DELETE_OBJECT(slotPrizeWheel[x])
		ENDIF
	ENDREPEAT
	REPEAT 3 i
		IF DOES_ENTITY_EXIST(slotBlurryReel[i])
			DELETE_OBJECT(slotBlurryReel[i])
		ENDIF
	ENDREPEAT
	IF DOES_ENTITY_EXIST(slotBlurryWheel)
		DELETE_OBJECT(slotBlurryWheel)
	ENDIF
//	#IF IS_DEBUG_BUILD
//	REPEAT 150 i 
//		IF DOES_ENTITY_EXIST(slotBigTestReel[i])
//			DELETE_OBJECT(slotBigTestReel[i])
//		ENDIF
//	ENDREPEAT
//	#ENDIF
//	IF DOES_CAM_EXIST(playingCamera)
//		RENDER_SCRIPT_CAMS(FALSE,FALSE)
//		DESTROY_CAM(playingCamera)
//	ENDIF
	
	IF IS_BIT_SET(iLocalBS,LOCAL_BS_TOOK_AWAY_CONTROL)
		IF IS_NET_PLAYER_OK(PLAYER_ID())
		AND NOT IS_PLAYER_SWITCH_IN_PROGRESS()
			NET_SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
			CLEAR_PED_TASKS(PLAYER_PED_ID())
		ENDIF
		IF IS_RADAR_HIDDEN()
			DISPLAY_RADAR(TRUE)
		ENDIF
		SET_DISABLE_RANK_UP_MESSAGE(FALSE)
		CLEAR_BIT(iLocalBS,LOCAL_BS_TOOK_AWAY_CONTROL)
	ENDIF
	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_EnableCasinoGameDebugText")
		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(FALSE)
	ENDIF
	#ENDIF
	CLEAR_MENU_DATA()
	CLEANUP_MENU_ASSETS()
	PRINTLN("CASINO_SLOTS: SCRIPT_CLEANUP called this frame")
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC

PROC PROCESS_PRE_GAME()

	IF g_bCleanupSlots
		PRINTLN("[CASINO SLOTS] - setting g_bCleanupSlots to false on start")
		g_bCleanupSlots = FALSE
	ENDIF
	NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE)
	
	// This makes sure the net script is active, waits untill it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD, SIZE_OF(playerBD))
	
	// KGM: Wait for the first network broadcast before moving on
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		PRINTLN("[CASINO SLOTS] - FAILED TO RECEIVE INITIAL NETWORK BROADCAST - SCRIPT CLEANUP B ")
		SCRIPT_CLEANUP()
	ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		// This script will not be paused if another script calls PAUSE_GAME
		SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
	ELSE
		PRINTLN("[CASINO SLOTS]- NETWORK_IS_GAME_IN_PROGRESS = FALSE - SCRIPT CLEANUP C")
		SCRIPT_CLEANUP()
	ENDIF
ENDPROC

FUNC INT GET_PLAYOUT_CASH_FROM_SYMBOL(INT iSymbol, INT iNumSymbol = 0)
	SWITCH iSymbol
		CASE SRS_SYMBOL1 RETURN slotSetup.iPayouts[PAYOUT_LINE_SYMBOL1]
		CASE SRS_SYMBOL2 RETURN slotSetup.iPayouts[PAYOUT_LINE_SYMBOL2]
		CASE SRS_SYMBOL3 RETURN slotSetup.iPayouts[PAYOUT_LINE_SYMBOL3]
		CASE SRS_SYMBOL4 RETURN slotSetup.iPayouts[PAYOUT_LINE_SYMBOL4]
		CASE SRS_SYMBOL5 RETURN slotSetup.iPayouts[PAYOUT_LINE_SYMBOL5]
		CASE SRS_SYMBOL6 RETURN slotSetup.iPayouts[PAYOUT_LINE_SYMBOL6]
//		CASE SRS_SYMBOL7 RETURN slotSetup.iPayouts[PAYOUT_LINE_SYMBOL7]
//		CASE SRS_SYMBOL8 RETURN slotSetup.iPayouts[PAYOUT_LINE_SYMBOL8]
//		CASE SRS_SYMBOL9 RETURN slotSetup.iPayouts[PAYOUT_LINE_SYMBOL9]
		CASE SRS_WILD
			IF iNumSymbol = 3
				RETURN slotSetup.iPayouts[PAYOUT_LINE_WILD] 
			ELIF iNumSymbol = 2
				RETURN slotSetup.iPayouts[PAYOUT_2_WILD] 
			ELIF iNumSymbol = 1
				RETURN slotSetup.iPayouts[PAYOUT_1_WILD] 
			ENDIF
		BREAK
	ENDSWITCH
	RETURN 0
ENDFUNC

FUNC INT DETERMINE_REEL_PAYOUT(INT iMachine, INT iReel1Symbol, INT iReel2Symbol, INT iReel3Symbol, BOOL bPrint = FALSE)
	INT iNumWild
	INT iPayout//, iWheelPayout
	IF bPrint
		PRINTLN("CASINO_SLOTS: DETERMINE_PAYOUT: iMachine = ",iMachine )
		PRINTLN("CASINO_SLOTS: DETERMINE_PAYOUT: iReel1Symbol = ",iReel1Symbol)
		PRINTLN("CASINO_SLOTS: DETERMINE_PAYOUT: iReel2Symbol = ",iReel2Symbol) //slotSetup[iMachineSetup].iVirtualReelContent[1][serverBD.iReelVirtSlotResults[iMachineSetup][1]])
		PRINTLN("CASINO_SLOTS: DETERMINE_PAYOUT: iReel3Symbol = ",iReel3Symbol)
	ENDIF
	CLEAR_BIT(iLocalBS,LOCAL_BS_LARGE_PAYOUT)
	CLEAR_BIT(iLocalBS,LOCAL_BS_CRUEL_LOSE)
	CLEAR_BIT(iLocalBS,LOCAL_BS_WHEEL_WIN)
	CLEAR_BIT(iLocalBS,LOCAL_BS_JACKPOT_PAYOUT)
	CLEAR_BIT(iLocalBS2,LOCAL_BS2_MEDIUM_WIN)
	IF (iReel1Symbol = iReel2Symbol)  AND (iReel2Symbol = iReel3Symbol)
		iPayout = GET_PLAYOUT_CASH_FROM_SYMBOL(iReel1Symbol,3)
		iPayout = iPayOut*(iPayoutMultipler*localSlots[iMachine].iMachineCost)
		IF iReel1Symbol >= SRS_SYMBOL5
			SET_BIT(iLocalBS,LOCAL_BS_LARGE_PAYOUT)
			IF iReel1Symbol = SRS_SYMBOL6
				SET_BIT(iLocalBS,LOCAL_BS_JACKPOT_PAYOUT)
			ENDIF
		ELIF iReel1Symbol >= SRS_SYMBOL2
			SET_BIT(iLocalBS2,LOCAL_BS2_MEDIUM_WIN)
		ENDIF
		RETURN iPayout
	ELSE
		IF iReel1Symbol = SRS_WILD
			iNumWild++
		ENDIF
		IF iReel2Symbol = SRS_WILD
			iNumWild++
		ENDIF
		IF iReel3Symbol = SRS_WILD
			iNumWild++
		ENDIF
		IF iNumWild >= 1
			IF bPrint
				PRINTLN("CASINO_SLOTS: DETERMINE_PAYOUT: num wild = ",iNumWild )
			ENDIF
			iPayout = GET_PLAYOUT_CASH_FROM_SYMBOL(SRS_WILD,iNumWild)
			iPayout = iPayOut*(iPayoutMultipler*localSlots[iMachine].iMachineCost)
			RETURN iPayout
		ENDIF
	ENDIF
	IF(iReel1Symbol = iReel2Symbol)
	AND iReel1Symbol >= SRS_SYMBOL5
		SET_BIT(iLocalBS,LOCAL_BS_CRUEL_LOSE)
	ENDIF
	RETURN 0
ENDFUNC

FUNC INT DETERMINE_WHEEL_PRIZE(INT iMachine, INT iReel1Symbol, INT iReel2Symbol, INT iReel3Symbol,INT iWheelResult = -1)
	IF iWheelResult >= 0
	AND DOES_SLOT_MACHINE_HAVE_WHEEL(iMachine,localSlots)
		IF iReel1Symbol = SRS_WILD
		AND iReel2Symbol = SRS_WILD
		AND iReel3Symbol = SRS_WILD
			RETURN GET_SLOT_MACHINE_WHEEL_PRIZE(localSlots[iMachine].iMachineType,iWheelResult)
			//iWheelPayout = iWheelPayout*(iPayoutMultipler*localSlots[iCurrentlyUsedMachine].iMachineCost)
		ENDIF
	ENDIF
	RETURN 0
ENDFUNC

FUNC INT GET_PLAYOUT_ID_FROM_SYMBOL(INT iSymbol, INT iNumSymbol = 0)
	SWITCH iSymbol
		CASE SRS_SYMBOL1 RETURN PAYOUT_LINE_SYMBOL1
		CASE SRS_SYMBOL2 RETURN PAYOUT_LINE_SYMBOL2
		CASE SRS_SYMBOL3 RETURN PAYOUT_LINE_SYMBOL3
		CASE SRS_SYMBOL4 RETURN PAYOUT_LINE_SYMBOL4
		CASE SRS_SYMBOL5 RETURN PAYOUT_LINE_SYMBOL5
		CASE SRS_SYMBOL6 RETURN PAYOUT_LINE_SYMBOL6
//		CASE SRS_SYMBOL7 RETURN PAYOUT_LINE_SYMBOL7
//		CASE SRS_SYMBOL8 RETURN PAYOUT_LINE_SYMBOL8
//		CASE SRS_SYMBOL9 RETURN PAYOUT_LINE_SYMBOL9
		CASE SRS_WILD
			IF iNumSymbol = 3
				RETURN PAYOUT_LINE_WILD
			ELIF iNumSymbol = 2
				RETURN PAYOUT_2_WILD
			ELIF iNumSymbol = 1
				RETURN PAYOUT_1_WILD
			ENDIF
		BREAK
	ENDSWITCH
	RETURN 0
ENDFUNC

FUNC INT DETERMINE_PAYOUT_ID(INT iReel1Symbol, INT iReel2Symbol, INT iReel3Symbol)
	INT iNumWild
//	PRINTLN("CASINO_SLOTS: DETERMINE_PAYOUT: iMachineSetup = ",iMachineSetup )
//	PRINTLN("CASINO_SLOTS: DETERMINE_PAYOUT: iReel1Symbol = ",iReel1Symbol)
//	PRINTLN("CASINO_SLOTS: DETERMINE_PAYOUT: iReel2Symbol = ",iReel2Symbol) //slotSetup[iMachineSetup].iVirtualReelContent[1][serverBD.iReelVirtSlotResults[iMachineSetup][1]])
//	PRINTLN("CASINO_SLOTS: DETERMINE_PAYOUT: iReel3Symbol = ",iReel3Symbol)
	
	IF (iReel1Symbol = iReel2Symbol)  AND (iReel2Symbol = iReel3Symbol)
		RETURN GET_PLAYOUT_ID_FROM_SYMBOL(iReel1Symbol,3)
	ELSE
		IF iReel1Symbol = SRS_WILD
			iNumWild++
		ENDIF
		IF iReel2Symbol = SRS_WILD
			iNumWild++
		ENDIF
		IF iReel3Symbol = SRS_WILD
			iNumWild++
		ENDIF
		IF iNumWild >= 1
			//PRINTLN("CASINO_SLOTS: DETERMINE_PAYOUT: num wild = ",iNumWild )
			RETURN GET_PLAYOUT_ID_FROM_SYMBOL(SRS_WILD,iNumWild)
		ENDIF
	ENDIF
	RETURN 0
ENDFUNC

FUNC INT WILD_HIT(INT iReel)
	RETURN slotSetup.iNumOfSymbolOnVirtReel[iReel][SRS_WILD]
ENDFUNC

FUNC INT WILD_MISS(INT iReel)
	RETURN (slotSetup.iSizeOfVirtualReel - slotSetup.iNumOfSymbolOnVirtReel[iReel][SRS_WILD])
ENDFUNC

#IF IS_DEBUG_BUILD
PROC INIT_DEBUG()
	INT i
	debugMachine.items[DB_SLOT_TITLE].vTextItemPosition = <<0.426,0.289,0>>
	REPEAT 3 i
		debugMachine.items[DB_SLOT_TITLE].TextCol[i] = 255
	ENDREPEAT
	debugMachine.items[DB_SLOT_REEL1].vTextItemPosition = <<0.415,0.350,0>>
	REPEAT 3 i
		debugMachine.items[DB_SLOT_REEL1].TextCol[i] = 255
	ENDREPEAT
	debugMachine.items[DB_SLOT_REEL2].vTextItemPosition = <<0.43,0.35,0>>
	REPEAT 3 i
		debugMachine.items[DB_SLOT_REEL2].TextCol[i] = 255
	ENDREPEAT
	debugMachine.items[DB_SLOT_REEL3].vTextItemPosition = <<0.445,0.35,0>>
	REPEAT 3 i
		debugMachine.items[DB_SLOT_REEL3].TextCol[i] = 255
	ENDREPEAT
	debugMachine.items[DB_SLOT_LAST_WIN].vTextItemPosition = <<0.379,0.587,0>>
	REPEAT 3 i
		debugMachine.items[DB_SLOT_LAST_WIN].TextCol[i] = 255
	ENDREPEAT
	debugMachine.items[DB_SLOT_LAST_WIN_VAL].vTextItemPosition = <<0.490,0.587,0>>
	REPEAT 3 i
		debugMachine.items[DB_SLOT_LAST_WIN_VAL].TextCol[i] = 255
	ENDREPEAT
	debugMachine.items[DB_SLOT_BET].vTextItemPosition = <<0.570,0.587,0>>
	debugMachine.items[DB_SLOT_BET_VAL].vTextItemPosition = <<0.620,0.587,0>>
	
	debugMachine.items[DB_SLOT_PAYOUT].vTextItemPosition = <<0.05,0.1,0>>
	debugMachine.items[DB_SLOT_PAYOUT_VAL].vTextItemPosition = <<0.2,0.1,0>>
	
	
	debugMachine.items[DB_SLOT_BALANCE].vTextItemPosition = <<0.379,0.61,0>>
	debugMachine.items[DB_SLOT_BALANCE_VAL].vTextItemPosition = <<0.490,0.61,0>>
	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_EnableCasinoGameDebugText")
		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
	ENDIF
	#ENDIF
ENDPROC


FUNC TEXT_LABEL_23 GET_DEBUG_ELEMENT_NAME(INT iElement)
	TEXT_LABEL_23 tlTemp
	SWITCH iElement
		CASE DB_SLOT_TITLE
			tlTemp = "Title"
		BREAK
		CASE DB_SLOT_REEL1
			tlTemp = "Reel1"
		BREAK
		CASE DB_SLOT_REEL2
			tlTemp = "Reel2"
		BREAK
		CASE DB_SLOT_REEL3
			tlTemp = "Reel3"
		BREAK
		CASE DB_SLOT_LAST_WIN
			tlTemp = "Last Win"
		BREAK
		CASE DB_SLOT_LAST_WIN_VAL
			tlTemp = "Win Val"
		BREAK
		CASE DB_SLOT_BET
			tlTemp = "Bet"
		BREAK
		CASE DB_SLOT_BET_VAL
			tlTemp = "Bet Val"
		BREAK
		CASE DB_SLOT_PAYOUT
			tlTemp = "Payout"
		BREAK
		CASE DB_SLOT_PAYOUT_VAL
			tlTemp = "Payout Val"
		BREAK
		CASE DB_SLOT_BALANCE
			tlTemp = "Balance"
		BREAK
		CASE DB_SLOT_BALANCE_VAL
			tlTemp = "Balance Val"
		BREAK
	ENDSWITCH
	RETURN tlTemp
ENDFUNC



PROC CREATE_DEBUG_WIDGETS()
	///TEXT_LABEL_15 tLabel
	INT i,j
	TEXT_LABEL_23 txtLabelTemp
	ScriptWidgetGroup = START_WIDGET_GROUP("CASINO_SLOTS")
		ADD_WIDGET_BOOL("Output object spin data (spammy)",bPrintDebugOuput)
		START_WIDGET_GROUP("OVERRIDE RESULT")
			REPEAT NUM_REELS i
				START_NEW_WIDGET_COMBO()
					REPEAT SRS_MAX_SYMBOLS j
						txtLabelTemp = GET_SLOT_MACHINE_SYMBOL_NAME(j)
						ADD_TO_WIDGET_COMBO(txtLabelTemp)
					ENDREPEAT
				txtLabelTemp = "REEL #"
				txtLabelTemp += i
				STOP_WIDGET_COMBO(txtLabelTemp, iOverrideReelResult[i])
			ENDREPEAT
			ADD_WIDGET_INT_SLIDER("Wheel result",iOverrideWheelResult,-1,11,1)
			
			ADD_WIDGET_INT_SLIDER("Message (1-16 for override)",db_iOverrideMessageID,0,16,1)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("DEV Only")
//			ADD_WIDGET_FLOAT_SLIDER("CentreX", db_CentreX, -2, 10, 0.01)
//			ADD_WIDGET_FLOAT_SLIDER("CentreY", db_CentreY, -2, 10, 0.01)
//			ADD_WIDGET_FLOAT_SLIDER("Width", db_Width, -1, 10, 0.01)
//			ADD_WIDGET_FLOAT_SLIDER("Height", db_Height, -1, 10, 0.01)
			ADD_WIDGET_INT_SLIDER("MAX_SLOT_ATTRACT_SOUNDS",MAX_SLOT_ATTRACT_SOUNDS,0,20,1)
			ADD_WIDGET_INT_SLIDER("iSizeOfVirtualReel",slotSetup.iSizeOfVirtualReel,0,100,1)
			ADD_WIDGET_INT_SLIDER("db_iMachine",db_iMachine,0,MAX_SLOT_MACHINES,1 )
			
			ADD_WIDGET_BOOL("Override Speeds", bOverrideSpinSpeed)
			ADD_WIDGET_INT_SLIDER("Reel Speed",iReelSpinSpeed,0,800,1 )
			ADD_WIDGET_INT_SLIDER("Wheel Speed",iWheelSpinSpeed,0,600,1 )
			
			START_WIDGET_GROUP("Text")
				REPEAT DB_SLOT_MAX i
					TEXT_LABEL_23 tlTemp = GET_DEBUG_ELEMENT_NAME(i)
					ADD_WIDGET_VECTOR_SLIDER(tlTemp,debugMachine.items[i].vTextItemPosition,0,1,0.001)
					REPEAT 3 j
						tlTemp = "Color "
						tlTemp += j
						ADD_WIDGET_INT_SLIDER(tlTemp,debugMachine.items[i].TextCol[j],0,255,1)
					ENDREPEAT
				ENDREPEAT
			STOP_WIDGET_GROUP()
			ADD_WIDGET_FLOAT_SLIDER("Symbol offset",debugMachine.fReelSymbolOffset,-1,1,0.001)
			
//			ADD_WIDGET_BOOL("bRunBigReelTest-DONT TOUCH!", bRunBigReelTest)
//			ADD_WIDGET_BOOL("Show debug spin", db_bDrawDebugSpin)
			ADD_WIDGET_BOOL("Create Slots", bCreateSlotMachineWidgets)
			ADD_WIDGET_BOOL("Kill script",bKillScript)
			ADD_WIDGET_BOOL("Test Machine",bd_bTestMachine)
			ADD_WIDGET_BOOL("db_bTestRotation",db_bTestRotation)
			ADD_WIDGET_BOOL("db_bAllowPosOverride",db_bAllowPosOverride)
			ADD_WIDGET_BOOL("Display Slot IDs",db_bDisplaySlotIDs)
			ADD_WIDGET_FLOAT_SLIDER("db_fSlot1Rot",db_fSlot1Rot,0,360,1)
			ADD_WIDGET_FLOAT_SLIDER("db_fSlot2Rot",db_fSlot2Rot,0,360,1)
			ADD_WIDGET_FLOAT_SLIDER("db_fSlot3Rot",db_fSlot3Rot,0,360,1)
		STOP_WIDGET_GROUP()
			
	STOP_WIDGET_GROUP()
ENDPROC

FUNC TEXT_LABEL_23 GET_SLOT_MACHINE_PAYOUT_NAME(INT Payout)
	TEXT_LABEL_23 payoutName
	SWITCH Payout
		CASE PAYOUT_LINE_SYMBOL1 payoutName = "PAYOUT_LINE_SYMBOL1" BREAK
		CASE PAYOUT_LINE_SYMBOL2 payoutName = "PAYOUT_LINE_SYMBOL2" BREAK
		CASE PAYOUT_LINE_SYMBOL3 payoutName = "PAYOUT_LINE_SYMBOL3" BREAK
		CASE PAYOUT_LINE_SYMBOL4 payoutName = "PAYOUT_LINE_SYMBOL4" BREAK
		CASE PAYOUT_LINE_SYMBOL5 payoutName = "PAYOUT_LINE_SYMBOL5" BREAK
		CASE PAYOUT_LINE_SYMBOL6 payoutName = "PAYOUT_LINE_SYMBOL6" BREAK
//		CASE PAYOUT_LINE_SYMBOL7 payoutName = "PAYOUT_LINE_SYMBOL7" BREAK
//		CASE PAYOUT_LINE_SYMBOL8 payoutName = "PAYOUT_LINE_SYMBOL8" BREAK
//		CASE PAYOUT_LINE_SYMBOL9 payoutName = "PAYOUT_LINE_SYMBOL9" BREAK
		CASE PAYOUT_LINE_WILD payoutName = "PAYOUT_LINE_WILD" BREAK
		CASE PAYOUT_2_WILD payoutName = "PAYOUT_2_WILD" BREAK
		CASE PAYOUT_1_WILD payoutName = "PAYOUT_1_WILD" BREAK
	ENDSWITCH
	RETURN payoutName
ENDFUNC

FUNC TEXT_LABEL_23 GET_SLOT_MACHINE_PAYOUT_FRIENDLY_NAME(INT Payout,INT iMachineType = 0)
	TEXT_LABEL_23 payoutName

	SWITCH Payout
		CASE PAYOUT_LINE_SYMBOL1 payoutName = "Match Cherries" BREAK
		CASE PAYOUT_LINE_SYMBOL2 payoutName = "Match Plums" BREAK
		CASE PAYOUT_LINE_SYMBOL3 payoutName = "Match Melons" BREAK
		CASE PAYOUT_LINE_SYMBOL4 payoutName = "Match Bells" BREAK
		CASE PAYOUT_LINE_SYMBOL5 payoutName = "Match 7s" BREAK
		CASE PAYOUT_LINE_SYMBOL6 
			SWITCH iMachineType
				CASE 0
				CASE 1 
					payoutName = "Match Keyboards" 
				BREAK
				CASE 2 payoutName = "Match Rage Face" BREAK
				CASE 3 payoutName = "Match Wings" BREAK
				CASE 4 payoutName = "Match Jackpot" BREAK
				CASE 5 payoutName = "Match Pharoh" BREAK
				CASE 6 payoutName = "Match Chainsaw" BREAK
				CASE 7 payoutName = "Match 3Diamonds" BREAK
				CASE 8 payoutName = "Match Sock Man" BREAK
			ENDSWITCH
		BREAK
//		CASE PAYOUT_LINE_SYMBOL7 payoutName = "PAYOUT_LINE_SYMBOL7" BREAK
//		CASE PAYOUT_LINE_SYMBOL8 payoutName = "PAYOUT_LINE_SYMBOL8" BREAK
//		CASE PAYOUT_LINE_SYMBOL9 payoutName = "PAYOUT_LINE_SYMBOL9" BREAK
		CASE PAYOUT_LINE_WILD
			SWITCH iMachineType
				CASE 0
				CASE 1 
					payoutName = "Match 3 Ninja Star" 
				BREAK
				CASE 2 payoutName = "Match 3 Lightning Bolt" BREAK
				CASE 3 payoutName = "Match 3 Pisswieser" BREAK
				CASE 4 payoutName = "Match 3 Microphone" BREAK
				CASE 5 payoutName = "Match 3 Ankh" BREAK
				CASE 6 payoutName = "Match 3 Knife" BREAK
				CASE 7 payoutName = "Match 3 Diamond" BREAK
				CASE 8 payoutName = "Match 3 RPG" BREAK
			ENDSWITCH
		BREAK
		CASE PAYOUT_2_WILD
			SWITCH iMachineType
				CASE 0
				CASE 1 
					payoutName = "Match 2 Ninja Star" 
				BREAK
				CASE 2 payoutName = "Match 2 Lightning Bolt" BREAK
				CASE 3 payoutName = "Match 2 Pisswieser" BREAK
				CASE 4 payoutName = "Match 2 Microphone" BREAK
				CASE 5 payoutName = "Match 2 Ankh" BREAK
				CASE 6 payoutName = "Match 2 Knife" BREAK
				CASE 7 payoutName = "Match 2 Diamond" BREAK
				CASE 8 payoutName = "Match 2 RPG" BREAK
			ENDSWITCH
		BREAK
		CASE PAYOUT_1_WILD
			SWITCH iMachineType
				CASE 0
				CASE 1 
					payoutName = "Match 1 Ninja Star" 
				BREAK
				CASE 2 payoutName = "Match 1 Lightning Bolt" BREAK
				CASE 3 payoutName = "Match 1 Pisswieser" BREAK
				CASE 4 payoutName = "Match 1 Microphone" BREAK
				CASE 5 payoutName = "Match 1 Ankh" BREAK
				CASE 6 payoutName = "Match 1 Knife" BREAK
				CASE 7 payoutName = "Match 1 Diamond" BREAK
				CASE 8 payoutName = "Match 1 RPG" BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH

	RETURN payoutName
ENDFUNC

PROC MAINTAIN_SLOT_MACHINE_WIDGETS()
	INT i,j, x 
	TEXT_LABEL_23 txtLabelTemp
	IF bCreateSlotMachineWidgets
		SET_CURRENT_WIDGET_GROUP(ScriptWidgetGroup)
		txtLabelTemp = "Slot Machine Setup "
		SlotMachineWidget = START_WIDGET_GROUP(txtLabelTemp)
			REPEAT PAYOUT_MAX_PAYOUTS x
				txtLabelTemp = GET_SLOT_MACHINE_PAYOUT_NAME(x)
				ADD_WIDGET_INT_SLIDER(txtLabelTemp,slotSetup.iPayouts[x],0,100000,1)
			ENDREPEAT
			REPEAT NUM_REELS i
				txtLabelTemp = "REEL #"
				txtLabelTemp += i
				START_WIDGET_GROUP(txtLabelTemp)
					REPEAT slotSetup.iSizeOfVirtualReel j
						START_NEW_WIDGET_COMBO()
							REPEAT SRS_MAX_SYMBOLS x
								txtLabelTemp = GET_SLOT_MACHINE_SYMBOL_NAME(x)
								ADD_TO_WIDGET_COMBO(txtLabelTemp)
							ENDREPEAT
						txtLabelTemp = "Reel Symbol #"
						txtLabelTemp += j
						STOP_WIDGET_COMBO(txtLabelTemp, slotSetup.iVirtualReelContent[i][j])
					ENDREPEAT
				STOP_WIDGET_GROUP()
			ENDREPEAT
			ADD_WIDGET_FLOAT_READ_ONLY("Payout % ",db_fExpectedPayoutPercentage)
			ADD_WIDGET_BOOL("Output Odds",bOutputOdds)
			ADD_WIDGET_BOOL("Output Machine",bOutputMachine)	
			ADD_WIDGET_BOOL("Blank Machine", db_bBlankSlotMachineWidgets)	
			ADD_WIDGET_BOOL("Remove Slots", bClearSlotMachineWidgets)	
		STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
		db_bRunningDebugMachine = TRUE
		bCreateSlotMachineWidgets = FALSE
	ENDIF
	IF bOutputMachine
		OPEN_DEBUG_FILE()
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("SLOT MACHINE OUTPUT START") 
		SAVE_NEWLINE_TO_DEBUG_FILE()
		//INT slotSetup[iMachine].iVirtualReelContent[NUM_REELS][MAX_VIRTUAL_REEL_SIZE]
		REPEAT NUM_REELS i
			SAVE_STRING_TO_DEBUG_FILE("//REEL #")  SAVE_INT_TO_DEBUG_FILE(i)
			SAVE_NEWLINE_TO_DEBUG_FILE()
			REPEAT slotSetup.iSizeOfVirtualReel j
				SAVE_STRING_TO_DEBUG_FILE("slotSetup.iVirtualReelContent[") SAVE_INT_TO_DEBUG_FILE(i)
				SAVE_STRING_TO_DEBUG_FILE("][")  SAVE_INT_TO_DEBUG_FILE(j)
				txtLabelTemp = GET_SLOT_MACHINE_SYMBOL_NAME(slotSetup.iVirtualReelContent[i][j])
				SAVE_STRING_TO_DEBUG_FILE("] = SRS_") SAVE_STRING_TO_DEBUG_FILE(txtLabelTemp)
				SAVE_NEWLINE_TO_DEBUG_FILE()
			ENDREPEAT
		ENDREPEAT
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("//PAYOUTS") 
		SAVE_NEWLINE_TO_DEBUG_FILE()
		REPEAT PAYOUT_MAX_PAYOUTS i
			SAVE_STRING_TO_DEBUG_FILE("slotSetup.iPayouts[") SAVE_INT_TO_DEBUG_FILE(i)
			SAVE_STRING_TO_DEBUG_FILE("] = ")  SAVE_INT_TO_DEBUG_FILE(slotSetup.iPayouts[i])
			SAVE_NEWLINE_TO_DEBUG_FILE()
		ENDREPEAT
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("SLOT MACHINE OUTPUT END") 
		SAVE_NEWLINE_TO_DEBUG_FILE()
		CLOSE_DEBUG_FILE()
		bOutputMachine = FALSE
	ENDIF

	IF bClearSlotMachineWidgets
		IF SlotMachineWidget != NULL
			DELETE_WIDGET_GROUP(SlotMachineWidget)
		ENDIF
		bClearSlotMachineWidgets = FALSE
		db_bRunningDebugMachine= FALSE
	ENDIF
	
	IF db_bBlankSlotMachineWidgets
		REPEAT NUM_REELS i
			REPEAT slotSetup.iSizeOfVirtualReel j
				slotSetup.iVirtualReelContent[i][j] = SRS_SYMBOL_BLANK
			ENDREPEAT
		ENDREPEAT
		db_bBlankSlotMachineWidgets = FALSE
	ENDIF
	
//	VECTOR vCoords
//	FLOAT fHeading
//	MODEL_NAMES eModel
//	IF bRunBigReelTest
//		REPEAT 150 i 
//			IF NOT DOES_ENTITY_EXIST(slotBigTestReel[i])
//				IF CAN_REGISTER_MISSION_ENTITIES(0,0,1,0)
//					eModel = INT_TO_ENUM(MODEL_NAMES, HASH("vw_Prop_Casino_Slot_01a_reels"))
//					REQUEST_MODEL(eModel)
//					IF HAS_MODEL_LOADED(eModel)
//						GET_SLOT_MACHINE_POSITION(0,vCoords,fHeading)
//						vCoords = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCoords,fHeading,<<-0.115+(-0.125*i),0.051,2>>)
//						slotBigTestReel[i] = CREATE_OBJECT(eModel,vCoords ,FALSE,FALSE,TRUE)
//						FREEZE_ENTITY_POSITION(slotBigTestReel[i],true)
//						SET_ENTITY_COLLISION(slotBigTestReel[i], FALSE)
//						SET_ENTITY_ROTATION(slotBigTestReel[i], <<-0.0000, -0.0000, fHeading>>)
//						PRINTLN("MAINTAIN_SLOT_MACHINE_WIDGETS: bRunBigReelTest creating reel at ",vCoords)
//					ENDIF
//				ENDIF
//			ELSE
//				slotBigReelTestRots[i] = slotBigReelTestRots[i] +@ 600
//				SET_ENTITY_ROTATION(slotBigTestReel[i], <<slotBigReelTestRots[i], -0.0000, fHeading>>)
//			ENDIF
//		ENDREPEAT
//	ENDIF

ENDPROC



CONST_INT DB_NUM_TESTS_OF_MACHINE 10000000
CONST_INT DB_NUM_TESTS_OF_MACHINE_PER_FRAME 10000
INT bd_iTestTotalPayIn
INT bd_iTestTotalPayOut
INT bd_iTestWinStreak,bd_iTestWinStreakCounter
INT bd_iTestLoseStreak,bd_iTestLoseStreakCounter
INT bd_iTestPayIn = 1
INT bd_iTestLoopCounter
INT bd_iTestNumPayout[PAYOUT_MAX_PAYOUTS]

PROC RUN_TEST_OF_MACHINE()
	//INT iResults[NUM_REELS]
	INT iWins, iPayOut, iPayoutID
	//INT iWheelPos = -1
	INT i//, j
	BOOL bOutputRNGresults = FALSE
	
	RESULT_STRUCT results
	IF bOutputRNGresults
		OPEN_DEBUG_FILE()
		SAVE_NEWLINE_TO_DEBUG_FILE()
	ENDIF
//	SAVE_STRING_TO_DEBUG_FILE("SLOT MACHINE: tests ",DB_NUM_TESTS_OF_MACHINE_PER_FRAME*bd_iTestLoopCounter " to ",DB_NUM_TESTS_OF_MACHINE_PER_FRAME*(bd_iTestLoopCounter+1)) 
//	SAVE_NEWLINE_TO_DEBUG_FILE()
	REPEAT DB_NUM_TESTS_OF_MACHINE_PER_FRAME i
//		REPEAT NUM_REELS j
//			iRand = GET_RANDOM_MWC_INT_IN_RANGE(0,slotSetup.iSizeOfVirtualReel)
//			iResults[j] = iRand
//		ENDREPEAT
//		IF DOES_SLOT_MACHINE_HAVE_WHEEL(db_iMachine,localSlots)
//			iRand = GET_RANDOM_MWC_INT_IN_RANGE(0,MAX_PROP_WHEEL_SIZE)
//			iWheelPos = iRand
//		ENDIF
		
		DETERMINE_SLOT_MACHINE_RESULT(db_iMachine,results,slotSetup,localSlots #IF IS_DEBUG_BUILD,  iOverrideReelResult, iOverrideWheelResult #ENDIF)
		
		IF bOutputRNGresults
			SAVE_INT_TO_DEBUG_FILE(results.iVirtualReelPos[0])
			SAVE_STRING_TO_DEBUG_FILE(",")
			SAVE_INT_TO_DEBUG_FILE(results.iVirtualReelPos[1])
			SAVE_STRING_TO_DEBUG_FILE(",")
			SAVE_INT_TO_DEBUG_FILE(results.iVirtualReelPos[2])
			SAVE_STRING_TO_DEBUG_FILE(":")
		ENDIF
		bd_iTestTotalPayIn += bd_iTestPayIn*iPayoutMultipler
		iPayOut = DETERMINE_REEL_PAYOUT(db_iMachine
						,slotSetup.iVirtualReelContent[0][results.iVirtualReelPos[0]]
						,slotSetup.iVirtualReelContent[1][results.iVirtualReelPos[1]]
						,slotSetup.iVirtualReelContent[2][results.iVirtualReelPos[2]])
						//,iWheelPos)
		
		IF results.iWheelPos >= 0
		AND DOES_SLOT_MACHINE_HAVE_WHEEL(db_iMachine,localSlots)
			IF slotSetup.iVirtualReelContent[0][results.iVirtualReelPos[0]] = SRS_WILD
			AND slotSetup.iVirtualReelContent[1][results.iVirtualReelPos[1]] = SRS_WILD
			AND slotSetup.iVirtualReelContent[2][results.iVirtualReelPos[2]] = SRS_WILD						
				PRINTLN("RUN_TEST_OF_MACHINE: WHEEL PRIZES")
				PRINTLN("PRIZE # ",GET_SLOT_MACHINE_WHEEL_PRIZE(localSlots[db_iMachine].iMachineType,results.iWheelPos))
			ENDIF
		ENDIF
		
		iPayOut = iPayout
		bd_iTestTotalPayOut += iPayOut
			//PRINTLN("RUN_TEST_OF_MACHINE: 1 =",db_SymbolsOnReels[0][results.iVirtualReelPos[0]])
		IF iPayOut > 0	
			iPayOutID = DETERMINE_PAYOUT_ID(slotSetup.iVirtualReelContent[0][results.iVirtualReelPos[0]]
						,slotSetup.iVirtualReelContent[1][results.iVirtualReelPos[1]]
						,slotSetup.iVirtualReelContent[2][results.iVirtualReelPos[2]])	
			bd_iTestNumPayout[iPayOutID]++		
			bd_iTestWinStreakCounter++
			bd_iTestLoseStreakCounter = 0
		ELSE
			bd_iTestLoseStreakCounter++
			bd_iTestWinStreakCounter = 0
		ENDIF
		IF bd_iTestWinStreakCounter > bd_iTestWinStreak
			bd_iTestWinStreak = bd_iTestWinStreakCounter
		ENDIF
		IF bd_iTestLoseStreakCounter > bd_iTestLoseStreak
			bd_iTestLoseStreak = bd_iTestLoseStreakCounter
		ENDIF
	ENDREPEAT
	IF bOutputRNGresults
		SAVE_NEWLINE_TO_DEBUG_FILE()
		CLOSE_DEBUG_FILE()
	ENDIF
	bd_iTestLoopCounter++
	IF bd_iTestLoopCounter >= DB_NUM_TESTS_OF_MACHINE/DB_NUM_TESTS_OF_MACHINE_PER_FRAME
		PRINTLN("RUN_TEST_OF_MACHINE: RESULTS")
		PRINTLN("COST PER SPIN: ",(iPayoutMultipler*localSlots[db_iMachine].iMachineCost))
		PRINTLN("PAYOUT MULTIPLIER: ",(iPayoutMultipler*localSlots[db_iMachine].iMachineCost))
		PRINTLN("TOTAL PAY IN: ",bd_iTestTotalPayIn)
		PRINTLN("TOTAL PAY OUT: ",bd_iTestTotalPayOut)
		FLOAT fPercentPayOut = TO_FLOAT(bd_iTestTotalPayOut)/TO_FLOAT(bd_iTestTotalPayIn)
		PRINTLN("% PAY OUT: ",fPercentPayOut)
		TEXT_LABEL_23 tempText
		FOR i = 1 TO PAYOUT_MAX_PAYOUTS-1
			tempText = GET_SLOT_MACHINE_PAYOUT_NAME(i)
			PRINTLN("NUM PAY OUT FOR:  ",tempText," = ",bd_iTestNumPayout[i])
			iWins += bd_iTestNumPayout[i]
		ENDFOR
		PRINTLN("WINS: ",iWins)
		fPercentPayOut = TO_FLOAT(iWins)/TO_FLOAT(DB_NUM_TESTS_OF_MACHINE)
		PRINTLN("% WIN: ",fPercentPayOut)
		PRINTLN("WIN STREAK: ",bd_iTestWinStreak)
		PRINTLN("LOSE STREAK: ",bd_iTestLoseStreak)
		bd_iTestTotalPayIn = 0
	  	bd_iTestTotalPayOut = 0
		REPEAT PAYOUT_MAX_PAYOUTS i
			bd_iTestNumPayout[i] = 0
		ENDREPEAT
		bd_iTestLoopCounter = 0
		bd_iTestLoseStreakCounter = 0
		bd_iTestWinStreakCounter = 0
		bd_iTestWinStreak = 0
		bd_iTestLoseStreak = 0
		bd_bTestMachine = FALSE
	ENDIF
ENDPROC


PROC MAINTAIN_DEBUG()
	//odds of match
	//	num symbol reel 1 x num symbols reel 2 x num symbols reel 3
	//	over
	//	max reel 1 x max reel 2 x max reel 3
	//	
	//	oddsOfMatch = (x*y*z)/POW(a,b)

	INT i,j

	MAINTAIN_SLOT_MACHINE_WIDGETS()
	IF db_bRunningDebugMachine	
		INT iMatchCalcTotal[PAYOUT_MAX_PAYOUTS]
		INT iCounter
		REPEAT SRS_MAX_SYMBOLS i
			iCounter = 0
			REPEAT NUM_REELS j
				IF slotSetup.iNumOfSymbolOnVirtReel[j][i] > 0
					iCounter++
					IF iMatchCalcTotal[i] = 0
						iMatchCalcTotal[i] = slotSetup.iNumOfSymbolOnVirtReel[j][i]
					ELSE
						iMatchCalcTotal[i] = iMatchCalcTotal[i]*slotSetup.iNumOfSymbolOnVirtReel[j][i]
					ENDIF
				ENDIF
			ENDREPEAT
			IF iCounter < 3
				iMatchCalcTotal[i] = 0
			ENDIF
		ENDREPEAT
		
		//wild 1
		//X00
		iMatchCalcTotal[PAYOUT_1_WILD] = WILD_HIT(0)*WILD_MISS(1)*WILD_MISS(2)	
		//0X0
		iMatchCalcTotal[PAYOUT_1_WILD] += WILD_MISS(0)*WILD_HIT(1)*WILD_MISS(2)	
		//00X
		iMatchCalcTotal[PAYOUT_1_WILD] += WILD_MISS(0)*WILD_MISS(1)*WILD_HIT(2)	
		
		//wild2 
		//XX0
		iMatchCalcTotal[PAYOUT_2_WILD] = WILD_HIT(0)*WILD_HIT(1)*WILD_MISS(2)	
		//X0X
		iMatchCalcTotal[PAYOUT_2_WILD] += WILD_HIT(0)*WILD_MISS(1)*WILD_HIT(2)	
		//0XX
		iMatchCalcTotal[PAYOUT_2_WILD] += WILD_MISS(0)*WILD_HIT(1)*WILD_HIT(2)		
		
		TEXT_LABEL_23 payoutName
		FLOAT fOdds
		FLOAT fTotalCombinations = POW(TO_FLOAT(slotSetup.iSizeOfVirtualReel),TO_FLOAT(NUM_REELS))
		
		
		REPEAT NUM_REELS i
			REPEAT  SRS_MAX_SYMBOLS j
				slotSetup.iNumOfSymbolOnVirtReel[i][j] = 0
			ENDREPEAT
		ENDREPEAT

		REPEAT NUM_REELS i
			REPEAT  slotSetup.iSizeOfVirtualReel j
				slotSetup.iNumOfSymbolOnVirtReel[i][slotSetup.iVirtualReelContent[i][j]]++
			ENDREPEAT
		ENDREPEAT
		
		IF bOutputOdds
			FLOAT iPayoutTotal
			REPEAT PAYOUT_MAX_PAYOUTS i
				payoutName = GET_SLOT_MACHINE_PAYOUT_NAME(i)
				fOdds = iMatchCalcTotal[i]/fTotalCombinations
				PRINTLN("CDM: CASINO_SLOTS PRIZE = ", payoutName, " Value = ",slotSetup.iPayouts[i]," ODDS (x/y) = ",iMatchCalcTotal[i],"/",fTotalCombinations," ODDS (0.0) = ",(fOdds))
				PRINTLN("CDM: CASINO_SLOTS PRIZE = ", payoutName, " Payout: ", slotSetup.iPayouts[i]*fOdds)
				iPayoutTotal += slotSetup.iPayouts[i]*fOdds
			ENDREPEAT
			PRINTLN("CDM: CASINO_SLOTS PRIZE: total payout = ", iPayoutTotal)
			REPEAT NUM_REELS i
				REPEAT SRS_MAX_SYMBOLS j
					payoutName =GET_SLOT_MACHINE_SYMBOL_NAME(j)
					PRINTLN("CDM: CASINO_SLOTS PRIZE on reel #",i," there are ", slotSetup.iNumOfSymbolOnVirtReel[i][j], " ",payoutName)
				ENDREPEAT
			ENDREPEAT
			 
			PRINTLN()
			bOutputOdds = FALSE
		ENDIF
		
		db_fExpectedPayoutPercentage = 0
		
		REPEAT PAYOUT_MAX_PAYOUTS i
			fOdds = iMatchCalcTotal[i]/fTotalCombinations
			db_fExpectedPayoutPercentage += slotSetup.iPayouts[i]*fOdds
		ENDREPEAT
		IF bd_bTestMachine
			IF db_iPrevMachine = -1
				db_iPrevMachine = iCurrentlyUsedMachine
			ENDIF
			iCurrentlyUsedMachine = db_iMachine
			RUN_TEST_OF_MACHINE()
		ELSE
			IF db_iPrevMachine >= 0
				iCurrentlyUsedMachine = db_iPrevMachine
				db_iPrevMachine = -1
			ENDIF
		ENDIF
	ENDIF
	
	IF db_bTestRotation
		VECTOR vTemp
		FLOAT fHeading
		IF iCurrentlyUsedMachine >= 0
			GET_SLOT_MACHINE_POSITION(iCurrentlyUsedMachine,vTemp,fHeading)
			SET_ENTITY_ROTATION(slotReel[iCurrentlyUsedMachine][0],<<db_fSlot1Rot,0,fHeading>>)
			SET_ENTITY_ROTATION(slotReel[iCurrentlyUsedMachine][1],<<db_fSlot2Rot,0,fHeading>>)
			SET_ENTITY_ROTATION(slotReel[iCurrentlyUsedMachine][2],<<db_fSlot3Rot,0,fHeading>>)
		ELSE
			GET_SLOT_MACHINE_POSITION(0,vTemp,fHeading)
			SET_ENTITY_ROTATION(slotReel[0][0],<<db_fSlot1Rot,0,fHeading>>)
			SET_ENTITY_ROTATION(slotReel[0][1],<<db_fSlot2Rot,0,fHeading>>)
			SET_ENTITY_ROTATION(slotReel[0][2],<<db_fSlot3Rot,0,fHeading>>)
		ENDIF
	ENDIF
	
	IF bKillScript
		PRINTLN("CASINO_SLOTS: cleaning up script for debug")
		SCRIPT_CLEANUP()
	ENDIF
	
	IF db_bDisplaySlotIDs
		FLOAT fHeading
		VECTOR vCoords
		REPEAT MAX_SLOT_MACHINES i
			GET_SLOT_MACHINE_POSITION(i, vCoords, fHeading)
			DRAW_DEBUG_TEXT_ABOVE_COORDS(vCoords, GET_STRING_FROM_INT(i), 2.5, 255, 255, 255)
		ENDREPEAT
	ENDIF
	
ENDPROC
#ENDIF

PROC INIT_DATA()
	INT i
	REPEAT MAX_SLOT_MACHINES i
		serverBD.iMachineUsedBy[i] = -1
	ENDREPEAT
	REPEAT MAX_SLOT_ATTRACT_SOUNDS i
		slotsAttract[i].iSoundID = -1
		slotsAttract[i].iMachine = -1
	ENDREPEAT
ENDPROC

PROC OVERRIDE_REEL_ROTATION_IF_NEEDED(INT iMachine, INT iReel)
	//FLOAT fRotation = localSlots[iMachine].fPropReelRots[iReel]
	#IF IS_DEBUG_BUILD
	IF NOT db_bAllowPosOverride
		EXIT
	ENDIF
	#ENDIF
	IF localSlots[iMachine].fPropReelPrevRots[iReel] != 0.0
		FLOAT fDif = localSlots[iMachine].fPropReelRots[iReel] - localSlots[iMachine].fPropReelPrevRots[iReel]
		FLOAT fMod = fDif%22.5
		IF ABSF(fMod) <= 2.5
			PRINTLN("OVERRIDE_REEL_ROTATION_IF_NEEDED: overriding rotation from ",localSlots[iMachine].fPropReelRots[iReel] )
			IF fMod < 0
				localSlots[iMachine].fPropReelRots[iReel] = localSlots[iMachine].fPropReelRots[iReel]-(GET_RANDOM_INT_IN_RANGE(3,8))
			ELSE
				localSlots[iMachine].fPropReelRots[iReel] = localSlots[iMachine].fPropReelRots[iReel]+(GET_RANDOM_INT_IN_RANGE(3,8))
			ENDIF
			PRINTLN("OVERRIDE_REEL_ROTATION_IF_NEEDED: overriding rotation to ",localSlots[iMachine].fPropReelRots[iReel] )
		ENDIF
	ENDIF
ENDPROC

FUNC INT GET_ACTIVE_MACHINE_SPIN_TIME(INT iMachine)
	IF DOES_SLOT_MACHINE_HAVE_WHEEL(iMachine,localSlots)
		IF slotSetup.iVirtualReelContent[0][serverBD.iReelVirtSlotResults[iMachine][0]] = SRS_WILD
		AND slotSetup.iVirtualReelContent[1][serverBD.iReelVirtSlotResults[iMachine][1]]= SRS_WILD
		AND slotSetup.iVirtualReelContent[2][serverBD.iReelVirtSlotResults[iMachine][2]] = SRS_WILD
			RETURN SLOTS_REEL_SPIN_TIME + SLOTS_WHEEL_SPIN_TIME
		ENDIF
	ENDIF
	RETURN SLOTS_REEL_SPIN_TIME
ENDFUNC

FUNC BOOL SHOULD_WHEEL_SPIN_ON_MACHINE(INT iMachine, BOOL bReelFinishedSpinning)
	IF DOES_SLOT_MACHINE_HAVE_WHEEL(iMachine,localSlots)
	AND slotSetup.iVirtualReelContent[0][serverBD.iReelVirtSlotResults[iMachine][0]] = SRS_WILD
	AND slotSetup.iVirtualReelContent[1][serverBD.iReelVirtSlotResults[iMachine][1]]= SRS_WILD
	AND slotSetup.iVirtualReelContent[2][serverBD.iReelVirtSlotResults[iMachine][2]] = SRS_WILD
		IF HAS_NET_TIMER_STARTED(serverBD.spinTimer[iMachine])
			IF bReelFinishedSpinning
			OR HAS_NET_TIMER_EXPIRED(serverBD.spinTimer[iMachine],SLOTS_REEL_SPIN_TIME+1000)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

//LOCAL FUNCTIONS
#IF IS_DEBUG_BUILD
PROC DEBUG_DRAW_SLOT_MACHINE()
	INT i,x
	VECTOR vTemp
	TEXT_LABEL_23 tlTemp

	IF iCurrentlyUsedMachine >= 0
	AND iClientState = LOCAL_STATE_USING_MACHINE

		IF GET_COMMANDLINE_PARAM_EXISTS("sc_EnableCasinoGameDebugText")
			INT iPayout
			DRAW_DEBUG_TEXT_2D("BET", debugMachine.items[DB_SLOT_BET].vTextItemPosition,debugMachine.items[DB_SLOT_BET].TextCol[0],debugMachine.items[DB_SLOT_BET].TextCol[1],debugMachine.items[DB_SLOT_BET].TextCol[2],255)
			tlTemp = ""
			tlTemp += localSlots[iCurrentlyUsedMachine].iMachineCost*iPayoutMultipler
			DRAW_DEBUG_TEXT_2D(tlTemp, debugMachine.items[DB_SLOT_BET_VAL].vTextItemPosition,debugMachine.items[DB_SLOT_BET_VAL].TextCol[0],debugMachine.items[DB_SLOT_BET_VAL].TextCol[1],debugMachine.items[DB_SLOT_BET_VAL].TextCol[2],255)
			
			REPEAT PAYOUT_MAX_PAYOUTS i
				iPayout = slotSetup.iPayouts[i]
				IF iPayout > 0
					iPayout = iPayout * (localSlots[iCurrentlyUsedMachine].iMachineCost*iPayoutMultipler)
					tlTemp = GET_SLOT_MACHINE_PAYOUT_FRIENDLY_NAME(i,localSlots[iCurrentlyUsedMachine].iMachineType)
					vTemp = <<0,0,0>>
					vTemp = debugMachine.items[DB_SLOT_PAYOUT].vTextItemPosition
					vTemp.y = vTemp.y + (0.02*i)
					DRAW_DEBUG_TEXT_2D(tlTemp,vTemp,debugMachine.items[DB_SLOT_PAYOUT].TextCol[0],debugMachine.items[DB_SLOT_PAYOUT].TextCol[1],debugMachine.items[DB_SLOT_PAYOUT].TextCol[2],255)
					tlTemp = ""
					tlTemp += iPayout
					vTemp = <<0,0,0>>
					vTemp = debugMachine.items[DB_SLOT_PAYOUT_VAL].vTextItemPosition
					vTemp.y = vTemp.y + (0.02*i)
					DRAW_DEBUG_TEXT_2D(tlTemp,vTemp,debugMachine.items[DB_SLOT_PAYOUT_VAL].TextCol[0],debugMachine.items[DB_SLOT_PAYOUT_VAL].TextCol[1],debugMachine.items[DB_SLOT_PAYOUT_VAL].TextCol[2],255)
				ENDIF
			ENDREPEAT
		ENDIF
		
		
	ENDIF

	REPEAT MAX_SLOT_MACHINES x
		IF IS_BIT_SET(serverBD.iBS_MachineSpinning[GET_BITSET_ARRAY_INDEX(x)],GET_BITSET_ELEMENT_ID_WITH_ARRAY(x))
			IF iCurrentlyUsedMachine = x
			AND iClientState = LOCAL_STATE_USING_MACHINE
				IF GET_COMMANDLINE_PARAM_EXISTS("sc_EnableCasinoGameDebugText")
					DRAW_DEBUG_TEXT_2D("Last Win = ",debugMachine.items[DB_SLOT_LAST_WIN].vTextItemPosition,debugMachine.items[DB_SLOT_LAST_WIN].TextCol[0],debugMachine.items[DB_SLOT_LAST_WIN].TextCol[1],debugMachine.items[DB_SLOT_LAST_WIN].TextCol[2],255)
					tlTemp = ""
					tlTemp += localSlots[iCurrentlyUsedMachine].iLastWin
					DRAW_DEBUG_TEXT_2D(tlTemp,debugMachine.items[DB_SLOT_LAST_WIN_VAL].vTextItemPosition,debugMachine.items[DB_SLOT_LAST_WIN_VAL].TextCol[0],debugMachine.items[DB_SLOT_LAST_WIN_VAL].TextCol[1],debugMachine.items[DB_SLOT_LAST_WIN_VAL].TextCol[2],255)
					DRAW_DEBUG_TEXT_2D("Balance", debugMachine.items[DB_SLOT_BALANCE].vTextItemPosition,debugMachine.items[DB_SLOT_BALANCE].TextCol[0],debugMachine.items[DB_SLOT_BALANCE].TextCol[1],debugMachine.items[DB_SLOT_BALANCE].TextCol[2],255)
					tlTemp = ""
					tlTemp += GET_LOCAL_PLAYERS_CASINO_CHIPS_TOTAL()
					DRAW_DEBUG_TEXT_2D(tlTemp, debugMachine.items[DB_SLOT_BALANCE_VAL].vTextItemPosition,debugMachine.items[DB_SLOT_BALANCE_VAL].TextCol[0],debugMachine.items[DB_SLOT_BALANCE_VAL].TextCol[1],debugMachine.items[DB_SLOT_BALANCE_VAL].TextCol[2],255)
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC
#ENDIF

FUNC TEXT_LABEL_23 GET_SLOT_MESSAGE_FROM_ID(INT iMachineType,INT iMessageID, BOOL bPositive)
	TEXT_LABEL_23 textLabel = "SLOTS_MES"
	IF bPositive
		textLabel += "P"
	ELSE
		textLabel += "N"
	ENDIF
	textLabel += iMachineType
	IF iMessageID < 10
		textLabel += "0"
		textLabel += iMessageID
	ELSE
		textLabel += iMessageID
	ENDIF
	RETURN textLabel
ENDFUNC

FUNC TEXT_LABEL_23 GET_SLOT_MESSAGE_RANDOM(INT iMachineType,BOOL bPositive)
	TEXT_LABEL_23 textLabel = "SLOTS_MES"
	INT iMessageID = GET_RANDOM_INT_IN_RANGE(1,17)
	#IF IS_DEBUG_BUILD
	IF db_iOverrideMessageID != 0
		iMessageID = db_iOverrideMessageID
	ENDIF
	#ENDIF
	
	IF bPositive
		textLabel += "P"
	ELSE
		textLabel += "N"
	ENDIF
	textLabel += iMachineType
	IF iMessageID < 10
		textLabel += "0"
		textLabel += iMessageID
	ELSE
		textLabel += iMessageID
	ENDIF
	RETURN textLabel
ENDFUNC

PROC SET_SLOTS_SCALEFORM_MESSAGE(TEXT_LABEL_23 &label)
	BEGIN_SCALEFORM_MOVIE_METHOD(machineUI.si_SlotsLocalMovie, "SET_MESSAGE")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(label)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC SET_SLOTS_SCALEFORM_BET_AMOUNT(INT iBet)
	BEGIN_SCALEFORM_MOVIE_METHOD(machineUI.si_SlotsLocalMovie, "SET_BET")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iBet)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC SET_SLOTS_SCALEFORM_LAST_WIN(INT iLastWin)
	BEGIN_SCALEFORM_MOVIE_METHOD(machineUI.si_SlotsLocalMovie, "SET_LAST_WIN")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iLastWin)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC SET_SLOTS_SCALEFORM_THEME(INT iMachineType)
	INT iTheme
	SWITCH iMachineType
		CASE MACHINE_1_ANGEL_AND_KNIGHT
			iTheme = 6
		BREAK
		CASE MACHINE_2_IMPOTENT_RAGE
			iTheme = 2
		BREAK
		CASE MACHINE_3_REPUBLICAN_SPACE_RANGERS
			iTheme = 3
		BREAK
		CASE MACHINE_4_FAME_OR_SHAME
			iTheme = 7
		BREAK
		CASE MACHINE_5_DEITY_OF_THE_SUN
			iTheme = 4
		BREAK
		CASE MACHINE_6_KNIFE_AFTER_DARK
			iTheme = 5
		BREAK
		CASE MACHINE_7_THE_DIAMOND
			iTheme = 1
		BREAK
		CASE MACHINE_8_EVACUATOR
			iTheme = 8
		BREAK
	ENDSWITCH
	BEGIN_SCALEFORM_MOVIE_METHOD(machineUI.si_SlotsLocalMovie, "SET_THEME")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTheme)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

FUNC BOOL LOAD_SLOT_MACHINE_UI()
	TEXT_LABEL_23 temp
	MODEL_NAMES theModel
	temp = "vw_Prop_Casino_Slot_0"
	temp += localSlots[iCurrentlyUsedMachine].iMachineType
	temp += "A"	
	theModel = INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY(temp))
	
	temp = "machine_0"
	temp += localSlots[iCurrentlyUsedMachine].iMachineType
	temp += "a"
	//machineUI.si_SlotsLocalMovie = REQUEST_SCALEFORM_MOVIE("temp")
	IF HAS_SCALEFORM_MOVIE_LOADED(machineUI.si_SlotsLocalMovie)
		IF NOT IS_NAMED_RENDERTARGET_REGISTERED(temp)
			REGISTER_NAMED_RENDERTARGET(temp)
			IF NOT IS_NAMED_RENDERTARGET_LINKED(theModel)
				LINK_NAMED_RENDERTARGET(theModel)
				IF machineUI.iRenderTargetID = -1
					machineUI.iRenderTargetID = GET_NAMED_RENDERTARGET_RENDER_ID(temp)
					PRINTLN("CASINO_SLOTS: LOAD_SLOT_MACHINE_UI:1 Setting RT:", machineUI.iRenderTargetID)
				ENDIF
				PRINTLN("CASINO_SLOTS: LOAD_SLOT_MACHINE_UI: 1 linked and registered ",temp)
				RETURN TRUE
			ELSE
				IF machineUI.iRenderTargetID = -1
					machineUI.iRenderTargetID = GET_NAMED_RENDERTARGET_RENDER_ID(temp)
					PRINTLN("CASINO_SLOTS: LOAD_SLOT_MACHINE_UI:2 Setting RT:", machineUI.iRenderTargetID)
				ENDIF
				PRINTLN("CASINO_SLOTS: LOAD_SLOT_MACHINE_UI:2 linked and registered ",temp)
				RETURN TRUE
			ENDIF
		ELSE
			IF NOT IS_NAMED_RENDERTARGET_LINKED(theModel)
				LINK_NAMED_RENDERTARGET(theModel)
				IF machineUI.iRenderTargetID = -1
					machineUI.iRenderTargetID= GET_NAMED_RENDERTARGET_RENDER_ID(temp)
					PRINTLN("CASINO_SLOTS: LOAD_SLOT_MACHINE_UI: 3 Setting RT:", machineUI.iRenderTargetID)
				ENDIF
				PRINTLN("CASINO_SLOTS: LOAD_SLOT_MACHINE_UI:3 linked and registered ",temp)
				RETURN TRUE
			ELSE
				IF machineUI.iRenderTargetID = -1
					machineUI.iRenderTargetID = GET_NAMED_RENDERTARGET_RENDER_ID(temp)
					PRINTLN("CASINO_SLOTS: LOAD_SLOT_MACHINE_UI:4 Setting RT:", machineUI.iRenderTargetID)
				ENDIF
				PRINTLN("CASINO_SLOTS: LOAD_SLOT_MACHINE_UI:4 linked and registered ",temp)
				RETURN TRUE
			ENDIF
		ENDIF	
	ENDIF
	RETURN FALSE
ENDFUNC

PROC DRAW_SLOT_MACHINE_SCALEFORM()
	IF HAS_SCALEFORM_MOVIE_LOADED(machineUI.si_SlotsLocalMovie)
		SET_SCALEFORM_MOVIE_TO_USE_LARGE_RT(machineUI.si_SlotsLocalMovie, TRUE) 
		SET_TEXT_RENDER_ID(machineUI.iRenderTargetID)
		SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
		SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
		//DRAW_SCALEFORM_MOVIE_FULLSCREEN(machineUI.si_SlotsLocalMovie,255,255,255,255)
		//DRAW_SCALEFORM_MOVIE(machineUI.si_SlotsLocalMovie,0.4,0.095,0.790,0.170,255,255,255,255)
//		#IF IS_DEBUG_BUILD
//		DRAW_SCALEFORM_MOVIE(machineUI.si_SlotsLocalMovie,db_CentreX,db_CentreY,db_Width, db_Height,255,255,255,255)
//		#ENDIF
//		#IF NOT IS_DEBUG_BUILD
		DRAW_SCALEFORM_MOVIE(machineUI.si_SlotsLocalMovie,0.401,0.09,0.805,0.195,255,255,255,255)
//		#ENDIF
		//DRAW_SCALEFORM_MOVIE(machineUI.si_SlotsLocalMovie,0.5,0.5,1,1,255,255,255,255)
		SET_TEXT_RENDER_ID(GET_DEFAULT_SCRIPT_RENDERTARGET_RENDER_ID())
	ENDIF
ENDPROC

PROC SET_CURRENTLY_USED_MACHINE(INT iMachine)
	IF iMachine >= 0
		iScaleformMachine = iMachine
	ENDIF
	iCurrentlyUsedMachine = iMachine
	playerBD[NATIVE_TO_INT(PLAYER_ID())].iCurrentMachineID = iMachine
	iTriggeringMachine = -1
	PRINTLN("CASINO_SLOTS: SET_CURRENTLY_USED_MACHINE = ",iMachine)
ENDPROC

PROC GET_SLOTS_ANIM_POSITION_AND_ROTATION(INT iMachine,VECTOR &vPos, VECTOR &vRot)
	FLOAT fHeading
	GET_SLOT_MACHINE_POSITION(iMachine,vPos,fHeading)
	vRot = <<0,0,fHeading>>
ENDPROC

PROC GET_RANDOM_SUFFIX_FOR_ANIMATION(INT iAnimID,TEXT_LABEL_3 &suffix,INT iMaxVariations,INT iOverrideMinRange = 0)
	INT iRand
	INT iPreventVar = -1
	IF iMaxVariations > 1
		iRand = GET_RANDOM_INT_IN_RANGE(iOverrideMinRange,iMaxVariations)
		IF iAnimID = ANIM_SLOTS_BASE
			iPreventVar = iLastBaseVar
		ELIF iAnimID = ANIM_SLOTS_BETIDLE
			iPreventVar = iLastBetIdleVar
		ENDIF
		IF iRand = iPreventVar
			iRand = iRand+1
			IF iRand >= iMaxVariations
				iRand = 0
			ENDIF
		ENDIF
	ENDIF
	SWITCH iRand
		CASE 0 suffix = "_a" BREAK
		CASE 1 suffix = "_b" BREAK
		CASE 2 suffix = "_c" BREAK
		CASE 3 suffix = "_d" BREAK
		CASE 4 suffix = "_e" BREAK
		CASE 5 suffix = "_f" BREAK
		CASE 6 suffix = "_g" BREAK
	ENDSWITCH
	
	IF iAnimID = ANIM_SLOTS_BASE
		iLastBaseVar = iRand
	ELIF iAnimID = ANIM_SLOTS_BETIDLE
		iLastBetIdleVar = iRand
	ENDIF
ENDPROC

PROC GET_ANIMATION_DETAILS(INT iAnimID,ANIM_DETAILS &details)
	details.bHoldLastFrame = TRUE
	details.bLooping = FALSE
	TEXT_LABEL_3 suffix
	INT iRand
	SWITCH iAnimID
		CASE ANIM_SLOTS_ENTER_LEFT
			details.name = "enter_left"
		BREAK
		CASE ANIM_SLOTS_ENTER_RIGHT
			details.name = "enter_right"
		BREAK
		CASE ANIM_SLOTS_ENTER_LEFT_SHORT
			details.name = "enter_left_short"
		BREAK
		CASE ANIM_SLOTS_ENTER_RIGHT_SHORT
			details.name = "enter_right_short"
		BREAK
		CASE ANIM_SLOTS_BASE
			details.name = "base_idle"
			IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
				GET_RANDOM_SUFFIX_FOR_ANIMATION(ANIM_SLOTS_BASE,suffix,1)
			ELSE
				GET_RANDOM_SUFFIX_FOR_ANIMATION(ANIM_SLOTS_BASE,suffix,5)
			ENDIF
			details.name += suffix
		BREAK
		CASE ANIM_SLOTS_BASE_TO_BET_ONE
			details.name = "press_betone_a"
		BREAK
		CASE ANIM_SLOTS_BASE_TO_BET_MAX
			details.name = "press_betmax_a"
		BREAK
		CASE ANIM_SLOTS_BASE_TO_AUTOSPIN
			details.name = "press_autospin"
		BREAK
		CASE ANIM_SLOTS_BASE_TO_SPIN
			details.name = "press_spin"
			GET_RANDOM_SUFFIX_FOR_ANIMATION(ANIM_SLOTS_BASE_TO_SPIN,suffix,2)
			details.name += suffix
		BREAK
		CASE ANIM_SLOTS_BASE_TO_PULL
			details.name = "pull_spin"
			GET_RANDOM_SUFFIX_FOR_ANIMATION(ANIM_SLOTS_BASE_TO_SPIN,suffix,2)
			details.name += suffix
		BREAK
		CASE ANIM_SLOTS_BETIDLE
			details.name = "betidle_idle"
			IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
				GET_RANDOM_SUFFIX_FOR_ANIMATION(ANIM_SLOTS_BETIDLE,suffix,1)
			ELSE
				GET_RANDOM_SUFFIX_FOR_ANIMATION(ANIM_SLOTS_BETIDLE,suffix,3)
			ENDIF
			details.name += suffix
		BREAK
		CASE ANIM_SLOTS_BETIDLE_TO_BET_ONE
			details.name = "betidle_press_betone"
			GET_RANDOM_SUFFIX_FOR_ANIMATION(ANIM_SLOTS_BETIDLE_TO_BET_ONE,suffix,3)
			details.name += suffix
		BREAK
		CASE ANIM_SLOTS_BETIDLE_TO_BET_MAX
			details.name = "betidle_press_betmax"
			GET_RANDOM_SUFFIX_FOR_ANIMATION(ANIM_SLOTS_BETIDLE_TO_BET_MAX,suffix,3)
			details.name += suffix
		BREAK
		CASE ANIM_SLOTS_BETIDLE_TO_AUTOSPIN
			details.name = "betidle_press_autospin"
		BREAK
		CASE ANIM_SLOTS_BETIDLE_TO_SPIN
			details.name = "betidle_press_spin"
			GET_RANDOM_SUFFIX_FOR_ANIMATION(ANIM_SLOTS_BETIDLE_TO_SPIN,suffix,3)
			details.name += suffix
		BREAK
		CASE ANIM_SLOTS_BETIDLE_TO_PULL
			details.name = "betidle_pull_spin"
			GET_RANDOM_SUFFIX_FOR_ANIMATION(ANIM_SLOTS_BETIDLE_TO_PULL,suffix,2)
			details.name += suffix
		BREAK
		CASE ANIM_SLOTS_BETIDLE_TO_BASE
			details.name = "betidle_to_base_transition"
		BREAK
		CASE ANIM_SLOTS_SPINNING_REELS
			details.name = "spinning"
			GET_RANDOM_SUFFIX_FOR_ANIMATION(ANIM_SLOTS_SPINNING_REELS,suffix,3)
			details.name += suffix
			details.bLooping = TRUE
			details.bHoldLastFrame = FALSE
		BREAK
		CASE ANIM_SLOTS_WIN
			details.name = "win"
			IF IS_BIT_SET(iLocalBS2,LOCAL_BS2_MEDIUM_WIN)
				GET_RANDOM_SUFFIX_FOR_ANIMATION(ANIM_SLOTS_WIN,suffix,4)
			ELSE
				GET_RANDOM_SUFFIX_FOR_ANIMATION(ANIM_SLOTS_WIN,suffix,7,4)
			ENDIF
			details.name += suffix
		BREAK
		CASE ANIM_SLOTS_WIN_BIG
			details.name = "win_big"
			GET_RANDOM_SUFFIX_FOR_ANIMATION(ANIM_SLOTS_WIN_BIG,suffix,3)
			details.name += suffix
		BREAK
		CASE ANIM_SLOTS_WIN_TO_WHEEL
			details.name = "win_to_spinning_wheel"
		BREAK
		CASE ANIM_SLOTS_WHEEL
			details.name = "spinning_wheel"
			details.bLooping = TRUE
			details.bHoldLastFrame = FALSE
		BREAK
		CASE ANIM_SLOTS_WHEEL_TO_WIN
			details.name = "win_spinning_wheel"
		BREAK
		CASE ANIM_SLOTS_LOSE
			details.name = "lose"
			iRand = GET_RANDOM_INT_IN_RANGE(0,10)
			IF iRand >= 9
				GET_RANDOM_SUFFIX_FOR_ANIMATION(ANIM_SLOTS_LOSE,suffix,6,3)
			ELSE
				GET_RANDOM_SUFFIX_FOR_ANIMATION(ANIM_SLOTS_LOSE,suffix,3)
			ENDIF
			details.name += suffix
		BREAK
		CASE ANIM_SLOTS_LOSE_CRUEL
			details.name = "lose_cruel"
			GET_RANDOM_SUFFIX_FOR_ANIMATION(ANIM_SLOTS_LOSE_CRUEL,suffix,2)
			details.name += suffix
		BREAK
		CASE ANIM_SLOTS_EXIT_LEFT
			details.name = "exit_left"
			details.bLooping = FALSE
			details.bHoldLastFrame = FALSE
		BREAK
		CASE ANIM_SLOTS_EXIT_LEFT_BETIDLE
			details.name = "betidle_left"
			details.bLooping = FALSE
			details.bHoldLastFrame = FALSE
		BREAK
		CASE ANIM_SLOTS_EXIT_RIGHT
			details.name = "exit_right"
			details.bLooping = FALSE
			details.bHoldLastFrame = FALSE
		BREAK
		CASE ANIM_SLOTS_EXIT_RIGHT_BETIDLE
			details.name = "betidle_right"
			details.bLooping = FALSE
			details.bHoldLastFrame = FALSE
		BREAK
	ENDSWITCH	
ENDPROC

FUNC FLOAT GET_PLAYER_ENTER_HEADING(INT iAnim)
	GET_ANIMATION_DETAILS(iAnim,tempAnimDetails)
	VECTOR vSceneRotation = GET_ANIM_INITIAL_OFFSET_ROTATION(GET_PLAYER_ANIM_DICT(), tempAnimDetails.name, vCurrentAnimLoc, vCurrentAnimRot, 0.01)
	RETURN vSceneRotation.z
ENDFUNC

FUNC VECTOR GET_PLAYER_ENTER_POSITION(INT iAnim)
	GET_ANIMATION_DETAILS(iAnim,tempAnimDetails)
	RETURN GET_ANIM_INITIAL_OFFSET_POSITION(GET_PLAYER_ANIM_DICT(), tempAnimDetails.name, vCurrentAnimLoc, vCurrentAnimRot, 0.01)
ENDFUNC


FUNC INT GET_PLAYER_ENTRY_ANIMATION()
	FLOAT fDistance = 100
	IF tfCASINO_PED_DEFAULT_GREET_AREA	> 0
	
	ENDIF
	INT iClosestEntryAnim, i
	REPEAT 4 i
		IF GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(PLAYER_ID()),GET_PLAYER_ENTER_POSITION(ANIM_SLOTS_ENTER_LEFT+i)) < fDistance
		AND GET_DISTANCE_BETWEEN_COORDS(GET_SLOT_MACHINE_SPECTATOR_COORDS(),GET_PLAYER_ENTER_POSITION(ANIM_SLOTS_ENTER_LEFT+i)) > 0.2
			fDistance = GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(PLAYER_ID()),GET_PLAYER_ENTER_POSITION(ANIM_SLOTS_ENTER_LEFT+i))
			iClosestEntryAnim = ANIM_SLOTS_ENTER_LEFT+i
		ENDIF
	ENDREPEAT
	RETURN iClosestEntryAnim
ENDFUNC

FUNC STRING GET_MACHINE_TYPE_SOUNDSET(INT iMachineType)
	SWITCH iMachineType
		CASE MACHINE_1_ANGEL_AND_KNIGHT 		RETURN "dlc_vw_casino_slot_machine_ak_player_sounds"
		CASE MACHINE_2_IMPOTENT_RAGE			RETURN "dlc_vw_casino_slot_machine_ir_player_sounds"
		CASE MACHINE_3_REPUBLICAN_SPACE_RANGERS	RETURN "dlc_vw_casino_slot_machine_rsr_player_sounds"
		CASE MACHINE_4_FAME_OR_SHAME			RETURN "dlc_vw_casino_slot_machine_fs_player_sounds"
		CASE MACHINE_5_DEITY_OF_THE_SUN			RETURN "dlc_vw_casino_slot_machine_ds_player_sounds"
		CASE MACHINE_6_KNIFE_AFTER_DARK			RETURN "dlc_vw_casino_slot_machine_kd_player_sounds"
		CASE MACHINE_7_THE_DIAMOND				RETURN "dlc_vw_casino_slot_machine_td_player_sounds"
		CASE MACHINE_8_EVACUATOR				RETURN "dlc_vw_casino_slot_machine_hz_player_sounds"
	ENDSWITCH
	RETURN ""
ENDFUNC

FUNC STRING GET_MACHINE_TYPE_SOUNDSET_NPC(INT iMachineType)
	SWITCH iMachineType
		CASE MACHINE_1_ANGEL_AND_KNIGHT 		RETURN "dlc_vw_casino_slot_machine_ak_npc_sounds"
		CASE MACHINE_2_IMPOTENT_RAGE			RETURN "dlc_vw_casino_slot_machine_ir_npc_sounds"
		CASE MACHINE_3_REPUBLICAN_SPACE_RANGERS	RETURN "dlc_vw_casino_slot_machine_rsr_npc_sounds"
		CASE MACHINE_4_FAME_OR_SHAME			RETURN "dlc_vw_casino_slot_machine_fs_npc_sounds"
		CASE MACHINE_5_DEITY_OF_THE_SUN			RETURN "dlc_vw_casino_slot_machine_ds_npc_sounds"
		CASE MACHINE_6_KNIFE_AFTER_DARK			RETURN "dlc_vw_casino_slot_machine_kd_npc_sounds"
		CASE MACHINE_7_THE_DIAMOND				RETURN "dlc_vw_casino_slot_machine_td_npc_sounds"
		CASE MACHINE_8_EVACUATOR				RETURN "dlc_vw_casino_slot_machine_hz_npc_sounds"
	ENDSWITCH
	RETURN ""
ENDFUNC

PROC PLAY_SLOT_SOUND_FROM_PLAYER(PLAYER_INDEX playerID,INT iMachineType,INT iSoundID)
	STRING soundSet = GET_MACHINE_TYPE_SOUNDSET(iMachineType)
	#IF IS_DEBUG_BUILD
		IF IS_NET_PLAYER_OK(playerID,FALSE)
			PRINTLN("PLAY_SLOT_SOUND_FROM_PLAYER: playing sound from player: ",GET_PLAYER_NAME(playerID)," soundset = ",soundSet," iSoundID = ",iSoundID)
		ELSE
			PRINTLN("PLAY_SLOT_SOUND_FROM_PLAYER: playing sound from player#: ",NATIVE_TO_INT(playerID)," soundset = ",soundSet," iSoundID = ",iSoundID)
		ENDIF
	#ENDIF
	SWITCH iSoundID
		CASE SLOT_SOUND_NO_WIN
			PLAY_SOUND_FROM_ENTITY(-1,"no_win",GET_PLAYER_PED(playerID),soundSet,TRUE,20)
		BREAK
		CASE SLOT_SOUND_SMALL_WIN
			PLAY_SOUND_FROM_ENTITY(-1,"small_win",GET_PLAYER_PED(playerID),soundSet,TRUE,20)
		BREAK
		CASE SLOT_SOUND_BIG_WIN
			PLAY_SOUND_FROM_ENTITY(-1,"big_win",GET_PLAYER_PED(playerID),soundSet,TRUE,20)
		BREAK
		CASE SLOT_SOUND_JACKPOT
			PLAY_SOUND_FROM_ENTITY(-1,"jackpot",GET_PLAYER_PED(playerID),soundSet,TRUE,20)
		BREAK
		CASE SLOT_SOUND_PLACE_BET
			PLAY_SOUND_FROM_ENTITY(-1,"place_bet",GET_PLAYER_PED(playerID),soundSet,TRUE,20)
		BREAK
		CASE SLOT_SOUND_PLACE_BET_MAX
			PLAY_SOUND_FROM_ENTITY(-1,"place_max_bet",GET_PLAYER_PED(playerID),soundSet,TRUE,20)
		BREAK
		CASE SLOT_SOUND_SPINNING
			PLAY_SOUND_FROM_ENTITY(-1,"spinning",GET_PLAYER_PED(playerID),soundSet,TRUE,20)
		BREAK
		CASE SLOT_SOUND_START_SPIN
			PLAY_SOUND_FROM_ENTITY(-1,"start_spin",GET_PLAYER_PED(playerID),soundSet,TRUE,20)
		BREAK
		CASE SLOT_SOUND_REEL_STOP
			PLAY_SOUND_FROM_ENTITY(-1,"wheel_stop_clunk",GET_PLAYER_PED(playerID),soundSet,TRUE,20)
		BREAK
		CASE SLOT_SOUND_REEL_STOP_ON_PRIZE
			PLAY_SOUND_FROM_ENTITY(-1,"wheel_stop_on_prize",GET_PLAYER_PED(playerID),soundSet,TRUE,20)
		BREAK
		CASE SLOT_SOUND_WELCOME
			PLAY_SOUND_FROM_ENTITY(-1,"welcome_stinger",GET_PLAYER_PED(playerID),soundSet,TRUE,20)
		BREAK
		CASE SLOT_SOUND_SPIN_WHEEL
			PLAY_SOUND_FROM_ENTITY(-1,"spin_wheel",GET_PLAYER_PED(playerID),soundSet,TRUE,20)
		BREAK
		CASE SLOT_SOUND_WHEEL_WIN
			PLAY_SOUND_FROM_ENTITY(-1,"spin_wheel_win",GET_PLAYER_PED(playerID),soundSet,TRUE,20)
		BREAK
	ENDSWITCH
	
ENDPROC

PROC PLAY_SLOT_SOUND_FROM_MACHINE_FOR_NPC(INT iMachine,INT iSoundID)
	STRING soundSet = GET_MACHINE_TYPE_SOUNDSET_NPC(localSlots[iMachine].iMachineType)
	#IF IS_DEBUG_BUILD
		PRINTLN("PLAY_SLOT_SOUND_FROM_MACHINE_FOR_NPC: playing sound from machine #: ",iMachine," soundset = ",soundSet," iSoundID = ",iSoundID)
	#ENDIF
	VECTOR vCoords
	FLOAT fHeading
	GET_SLOT_MACHINE_POSITION(iMachine,vCoords,fHeading)
	vCoords = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCoords,fHeading, << 0, -0.4,1 >>)
	SWITCH iSoundID
		CASE SLOT_SOUND_NO_WIN
			PLAY_SOUND_FROM_COORD(-1,"no_win",vCoords,soundSet,FALSE,20)
		BREAK
		CASE SLOT_SOUND_SMALL_WIN
			PLAY_SOUND_FROM_COORD(-1,"small_win",vCoords,soundSet,FALSE,20)
		BREAK
		CASE SLOT_SOUND_BIG_WIN
			PLAY_SOUND_FROM_COORD(-1,"big_win",vCoords,soundSet,FALSE,20)
		BREAK
		CASE SLOT_SOUND_JACKPOT
			PLAY_SOUND_FROM_COORD(-1,"jackpot",vCoords,soundSet,FALSE,20)
		BREAK
		CASE SLOT_SOUND_PLACE_BET
			PLAY_SOUND_FROM_COORD(-1,"place_bet",vCoords,soundSet,FALSE,20)
		BREAK
		CASE SLOT_SOUND_PLACE_BET_MAX
			PLAY_SOUND_FROM_COORD(-1,"place_max_bet",vCoords,soundSet,FALSE,20)
		BREAK
		CASE SLOT_SOUND_SPINNING
			PLAY_SOUND_FROM_COORD(-1,"spinning",vCoords,soundSet,FALSE,20)
		BREAK
		CASE SLOT_SOUND_START_SPIN
			PLAY_SOUND_FROM_COORD(-1,"start_spin",vCoords,soundSet,FALSE,20)
		BREAK
		CASE SLOT_SOUND_REEL_STOP
			PLAY_SOUND_FROM_COORD(-1,"wheel_stop_clunk",vCoords,soundSet,FALSE,20)
		BREAK
		CASE SLOT_SOUND_REEL_STOP_ON_PRIZE
			PLAY_SOUND_FROM_COORD(-1,"wheel_stop_on_prize",vCoords,soundSet,FALSE,20)
		BREAK
		CASE SLOT_SOUND_WELCOME
			PLAY_SOUND_FROM_COORD(-1,"welcome_stinger",vCoords,soundSet,FALSE,20)
		BREAK
		CASE SLOT_SOUND_SPIN_WHEEL
			PLAY_SOUND_FROM_COORD(-1,"spin_wheel",vCoords,soundSet,FALSE,20)
		BREAK
		CASE SLOT_SOUND_WHEEL_WIN
			PLAY_SOUND_FROM_COORD(-1,"spin_wheel_win",vCoords,soundSet,FALSE,20)
		BREAK
	ENDSWITCH
	
ENDPROC

FUNC BOOL LOAD_ANIM_DICTS()
	SET_BIT(iLocalBS,LOCAL_BS_REQUESTED_ANIMS)
	REQUEST_ANIM_DICT(GET_PLAYER_ANIM_DICT())
	
	IF HAS_ANIM_DICT_LOADED(GET_PLAYER_ANIM_DICT())
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC START_NETWORK_SYNCED_SCENE_FOR_SLOTS(INT iAnimationID)
	GET_ANIMATION_DETAILS(iAnimationID,tempAnimDetails)
	iAnimSyncedScene = NETWORK_CREATE_SYNCHRONISED_SCENE(vCurrentAnimLoc, vCurrentAnimRot,DEFAULT,tempAnimDetails.bHoldLastFrame,tempAnimDetails.bLooping)
	IF iAnimationID = ANIM_SLOTS_BASE_TO_BET_ONE
	OR iAnimationID = ANIM_SLOTS_BASE_TO_BET_MAX
		NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), iAnimSyncedScene, GET_PLAYER_ANIM_DICT(), tempAnimDetails.name, SLOW_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT  | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT)
	ELIF iAnimationID = ANIM_SLOTS_BETIDLE_TO_BET_ONE
	OR iAnimationID = ANIM_SLOTS_BETIDLE_TO_BET_MAX
		NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), iAnimSyncedScene, GET_PLAYER_ANIM_DICT(), tempAnimDetails.name, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT  | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT)
	ELIF iAnimationID = ANIM_SLOTS_BASE_TO_SPIN
	OR iAnimationID = ANIM_SLOTS_BETIDLE_TO_SPIN
		NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), iAnimSyncedScene, GET_PLAYER_ANIM_DICT(), tempAnimDetails.name, NORMAL_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT  | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT)
	ELIF iAnimationID = ANIM_SLOTS_ENTER_LEFT
	OR iAnimationID = ANIM_SLOTS_ENTER_LEFT_SHORT
	OR iAnimationID = ANIM_SLOTS_ENTER_RIGHT
	OR iAnimationID = ANIM_SLOTS_ENTER_RIGHT_SHORT
		NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), iAnimSyncedScene, GET_PLAYER_ANIM_DICT(), tempAnimDetails.name, REALLY_SLOW_BLEND_IN, WALK_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT  | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT,REALLY_SLOW_BLEND_IN)	
	ELSE
		NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), iAnimSyncedScene, GET_PLAYER_ANIM_DICT(), tempAnimDetails.name, REALLY_SLOW_BLEND_IN, WALK_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT  | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT)
		IF (iAnimationID = ANIM_SLOTS_BETIDLE_TO_PULL
		OR iAnimationID = ANIM_SLOTS_BASE_TO_PULL)
		//		AND DOES_ENTITY_EXIST(usedSlotMachineObject)
			TEXT_LABEL_23 modelText = "vw_prop_casino_slot_0"
			modelText += localSlots[iCurrentlyUsedMachine].iMachineType
			modelText += "a"	
			MODEL_NAMES eModel = INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY(modelText))
			//usedSlotMachineObject = GET_CLOSEST_OBJECT_OF_TYPE( vCurrentAnimLoc,0.2,eModel ,FALSE,FALSE,FALSE)
			tempAnimDetails.name += "_SLOTMACHINE"
			PRINTLN("CASINO_SLOTS: START_NETWORK_SYNCED_SCENE_FOR_SLOTS adding slot machine: ",modelText, " at: ", vCurrentAnimLoc)
			NETWORK_ADD_MAP_ENTITY_TO_SYNCHRONISED_SCENE(iAnimSyncedScene, eModel, vCurrentAnimLoc, GET_PLAYER_ANIM_DICT(), tempAnimDetails.name, REALLY_SLOW_BLEND_IN, WALK_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT  | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS)
		ENDIF
	ENDIF
	NETWORK_START_SYNCHRONISED_SCENE(iAnimSyncedScene)
	PRINTLN("CASINO_SLOTS: START_NETWORK_SYNCED_SCENE_FOR_SLOTS starting anim: ", tempAnimDetails.name, " Looping: ",tempAnimDetails.bLooping, " holdLastFrame: ",tempAnimDetails.bHoldLastFrame)
	iCurrentAnim = iAnimationID
ENDPROC	

FUNC FLOAT GET_CURRENT_ANIM_PHASE()
	INT iLocalScene
	iLocalScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(iAnimSyncedScene)
	IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalScene)
		RETURN GET_SYNCHRONIZED_SCENE_PHASE(iLocalScene)
	ENDIF
	RETURN -1.0
ENDFUNC

PROC PLAY_BET_ANIM(INT iBetType)
	IF iBetType = 0
		IF iCurrentAnim = ANIM_SLOTS_BASE
		OR (iCurrentAnim = ANIM_SLOTS_BETIDLE_TO_BASE AND HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(),HASH("CAN_BREAKOUT_FOR_BETTING")))
			START_NETWORK_SYNCED_SCENE_FOR_SLOTS(ANIM_SLOTS_BASE_TO_BET_ONE)
		ELIF iCurrentAnim = ANIM_SLOTS_BETIDLE
		OR HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(),HASH("CAN_BREAKOUT_FOR_BETTING"))
			START_NETWORK_SYNCED_SCENE_FOR_SLOTS(ANIM_SLOTS_BETIDLE_TO_BET_ONE)
		ENDIF
	ELIF iBetType = 1
		IF iCurrentAnim = ANIM_SLOTS_BASE
		OR (iCurrentAnim = ANIM_SLOTS_BETIDLE_TO_BASE AND HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(),HASH("CAN_BREAKOUT_FOR_BETTING")))
			START_NETWORK_SYNCED_SCENE_FOR_SLOTS(ANIM_SLOTS_BASE_TO_BET_MAX)	
		ELIF iCurrentAnim = ANIM_SLOTS_BETIDLE
		OR HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(),HASH("CAN_BREAKOUT_FOR_BETTING"))
			START_NETWORK_SYNCED_SCENE_FOR_SLOTS(ANIM_SLOTS_BETIDLE_TO_BET_MAX)
		ENDIF
	ENDIF
ENDPROC

PROC PLAY_SPIN_ANIM()
	IF iCurrentAnim = ANIM_SLOTS_BASE
	OR (iCurrentAnim = ANIM_SLOTS_BETIDLE_TO_BASE AND HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(),HASH("CAN_BREAKOUT_FOR_BETTING")))
		START_NETWORK_SYNCED_SCENE_FOR_SLOTS(ANIM_SLOTS_BASE_TO_SPIN)
	ELIF iCurrentAnim = ANIM_SLOTS_BETIDLE
	OR iCurrentAnim =ANIM_SLOTS_BETIDLE_TO_BET_ONE
	OR iCurrentAnim =ANIM_SLOTS_BETIDLE_TO_BET_MAX
	OR HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(),HASH("CAN_BREAKOUT_FOR_BETTING"))
		START_NETWORK_SYNCED_SCENE_FOR_SLOTS(ANIM_SLOTS_BETIDLE_TO_SPIN)
	ENDIF
ENDPROC

PROC PLAY_PULL_ANIM()
	IF iCurrentAnim = ANIM_SLOTS_BASE
	OR (iCurrentAnim = ANIM_SLOTS_BETIDLE_TO_BASE AND HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(),HASH("CAN_BREAKOUT_FOR_BETTING")))
		START_NETWORK_SYNCED_SCENE_FOR_SLOTS(ANIM_SLOTS_BASE_TO_PULL)
	ELIF iCurrentAnim = ANIM_SLOTS_BETIDLE
	OR iCurrentAnim =ANIM_SLOTS_BETIDLE_TO_BET_ONE
	OR iCurrentAnim =ANIM_SLOTS_BETIDLE_TO_BET_MAX
	OR HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(),HASH("CAN_BREAKOUT_FOR_BETTING"))
		START_NETWORK_SYNCED_SCENE_FOR_SLOTS(ANIM_SLOTS_BETIDLE_TO_PULL)
	ENDIF
	//SET_BIT(iLocalBS,LOCAL_BS_NET_MACH_FOR_PULL)
ENDPROC

PROC HANDLE_SLOTS_ANIMS()
	INT iLocalScene
	BOOL bNextAnim
	//FLOAT fEndPhase = 0.99
	IF iCurrentAnim = ANIM_SLOTS_BETIDLE
		IF NOT HAS_NET_TIMER_STARTED(stBetIdleTimeout)
			START_NET_TIMER(stBetIdleTimeout,TRUE)
		ELSE
			IF HAS_NET_TIMER_EXPIRED(stBetIdleTimeout,10000,TRUE)
				CLEAR_BIT(iLocalBS,LOCAL_BS_SETUP_CONTROLS)
				START_NETWORK_SYNCED_SCENE_FOR_SLOTS(ANIM_SLOTS_BETIDLE_TO_BASE)
				EXIT
			ENDIF
		ENDIF
	ELSE
		RESET_NET_TIMER(stBetIdleTimeout)
	ENDIF
		iLocalScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(iAnimSyncedScene)
		IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalScene)
//				IF iCurrentAnim = ANIM_SLOTS_WIN
//				OR iCurrentAnim = ANIM_SLOTS_LOSE
//					fEndPhase = 0.98
//				ENDIF
			IF GET_SYNCHRONIZED_SCENE_PHASE(iLocalScene) >= 0.99//fEndPhase
			OR HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(),HASH("BLEND_OUT"))
			//OR HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(),HASH("BLEND_OUT"))
				bNextAnim = TRUE
			ENDIF
		ELSE
			PRINTLN("CASINO_SLOTS: HANDLE_SLOTS_ANIMS synced scene not running when it should")
			bNextAnim = TRUE
		ENDIF
		IF bNextAnim
			IF iCurrentAnim = ANIM_SLOTS_BASE
			OR iCurrentAnim = ANIM_SLOTS_BETIDLE_TO_BASE
				START_NETWORK_SYNCED_SCENE_FOR_SLOTS(ANIM_SLOTS_BASE)
			ELIF iCurrentAnim = ANIM_SLOTS_BETIDLE
				START_NETWORK_SYNCED_SCENE_FOR_SLOTS(ANIM_SLOTS_BETIDLE)
			ELIF iCurrentAnim = ANIM_SLOTS_BASE_TO_BET_ONE
			OR iCurrentAnim = ANIM_SLOTS_BASE_TO_BET_MAX
			OR iCurrentAnim = ANIM_SLOTS_BASE_TO_AUTOSPIN
			OR iCurrentAnim = ANIM_SLOTS_BETIDLE_TO_BET_ONE
			OR iCurrentAnim = ANIM_SLOTS_BETIDLE_TO_BET_MAX
			OR iCurrentAnim = ANIM_SLOTS_BETIDLE_TO_AUTOSPIN
				CLEAR_BIT(iLocalBS,LOCAL_BS_SETUP_CONTROLS)
				START_NETWORK_SYNCED_SCENE_FOR_SLOTS(ANIM_SLOTS_BETIDLE)
			ELIF iCurrentAnim = ANIM_SLOTS_WIN
			OR iCurrentAnim = ANIM_SLOTS_WIN_BIG
			OR iCurrentAnim = ANIM_SLOTS_LOSE
			OR iCurrentAnim = ANIM_SLOTS_LOSE_CRUEL
			OR iCurrentAnim = ANIM_SLOTS_WHEEL_TO_WIN
				START_NETWORK_SYNCED_SCENE_FOR_SLOTS(ANIM_SLOTS_BETIDLE)
			ELIF iCurrentAnim = ANIM_SLOTS_WIN_TO_WHEEL
				START_NETWORK_SYNCED_SCENE_FOR_SLOTS(ANIM_SLOTS_WHEEL)
			ELIF iCurrentAnim = ANIM_SLOTS_BASE_TO_SPIN
			OR iCurrentAnim = ANIM_SLOTS_BASE_TO_PULL
			OR iCurrentAnim = ANIM_SLOTS_BETIDLE_TO_SPIN
			OR iCurrentAnim = ANIM_SLOTS_BETIDLE_TO_PULL
				START_NETWORK_SYNCED_SCENE_FOR_SLOTS(ANIM_SLOTS_SPINNING_REELS)
			ENDIF
		ENDIF
	//ENDIF
ENDPROC

PROC SET_SLOT_CAMERA_ACTIVE(INT iCamera)
	//VECTOR vPos//, vRot
//	IF iCamera != CAM_SLOTS_FIRST_PERS
//	AND iCamera != CAM_SLOTS_THIRD_PERS
//		IF NOT DOES_CAM_EXIST(playingCamera)
//			playingCamera = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", FALSE) 
//			SET_CAM_ACTIVE(playingCamera, TRUE)
//			RENDER_SCRIPT_CAMS(TRUE,FALSE)
//		ENDIF
//	ENDIF
	VECTOR vCoords
	FLOAT fHeading
	GET_SLOT_MACHINE_POSITION(iCurrentlyUsedMachine,vCoords,fHeading)
	SWITCH iCamera
//		CASE CAM_SLOTS_ENTER
//			vPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCoords,fHeading,<< -0.84598, -1.90365, 2.1362 >>)
//			SET_CAM_PARAMS(playingCamera,  vPos, <<0,0,0>>, 57.6097)		
//			vPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCoords,fHeading, << 0.0628427, -0.239899, 1.137 >>)
//			POINT_CAM_AT_COORD(playingCamera,vPos)
//			SHAKE_CAM(playingCamera, "HAND_SHAKE", 0.05)
//		BREAK
//		CASE CAM_SLOTS_FACE_MACHINE
//			vPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCoords,fHeading, << -0.000729531, -0.68, 1.1701 >>)
//			SET_CAM_PARAMS(playingCamera,  vPos, <<0,0,0>>, 57.6097)
//			vPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCoords,fHeading, << 0.00191624, -0.103235, 1.1031 >>)
//			POINT_CAM_AT_COORD(playingCamera,vPos)
//			SHAKE_CAM(playingCamera, "HAND_SHAKE", 0.05)
//			RENDER_SCRIPT_CAMS(TRUE,FALSE)
//		BREAK
//		CASE CAM_SLOTS_FACE_MACHINE_WHEEL
//			vPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCoords,fHeading, << -0.000729531, -0.68, 1.8701 >>)
//			SET_CAM_PARAMS(playingCamera,  vPos, <<0,0,0>>, 57.6097)
//			vPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCoords,fHeading, << 0.00191624, -0.103235, 1.8031 >>)
//			POINT_CAM_AT_COORD(playingCamera,vPos)
//		BREAK
		
		CASE CAM_SLOTS_FIRST_PERS
			IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) != CAM_VIEW_MODE_FIRST_PERSON
				SET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT, CAM_VIEW_MODE_FIRST_PERSON)
//				IF iCurrentAnim = ANIM_SLOTS_BASE 
//					IF iLastBaseVar != 0
//						START_NETWORK_SYNCED_SCENE_FOR_SLOTS(ANIM_SLOTS_BASE)
//					ENDIF
//				ELIF iCurrentAnim = ANIM_SLOTS_BETIDLE
//					IF iLastBetIdleVar != 0
//						START_NETWORK_SYNCED_SCENE_FOR_SLOTS(ANIM_SLOTS_BETIDLE)
//					ENDIF
//				ENDIF
				//SET_FIRST_PERSON_SHOOTER_CAMERA_HEADING(0)
				//SET_FIRST_PERSON_SHOOTER_CAMERA_PITCH(-33.3452)
			ENDIF
		BREAK
		CASE CAM_SLOTS_THIRD_PERS
			IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) != CAM_VIEW_MODE_THIRD_PERSON_NEAR
				SET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT, CAM_VIEW_MODE_THIRD_PERSON_NEAR)
			ENDIF
			RENDER_SCRIPT_CAMS(FALSE,FALSE)
		BREAK
	ENDSWITCH
	PRINTLN("CASINO_SLOT: SET_SLOT_CAMERA_ACTIVE setting camera = ",iCamera)
	iCurrentCamera = iCamera
ENDPROC

PROC HANDLE_FIRST_PERSON_CAMERA()
//	//Gameplay Cam Relative Heading = -0.0554
//	//Gameplay Cam Relative Pitch = -33.3452
//
	IF iCurrentCamera = CAM_SLOTS_FIRST_PERS
	AND GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
		
//		IF IS_CAM_RENDERING(playingCamera)
//			RENDER_SCRIPT_CAMS(FALSE,FALSE)
//			//SET_FIRST_PERSON_SHOOTER_CAMERA_HEADING(0)
//			//SET_FIRST_PERSON_SHOOTER_CAMERA_PITCH(-33.3452)
//		ENDIF
	ENDIF
//		FLOAT fHeadingOffset
//		FLOAT fPitchOffset
//		INT iStickX, iStickY
//		
//		iStickX = GET_CONTROL_VALUE(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT_AXIS_X)
//		iStickY = GET_CONTROL_VALUE(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT_AXIS_Y)
//		//PRINTLN("CDM: iStickX = ",iStickX)
//		//PRINTLN("CDM: iStickY = ",iStickY)
//		IF NOT IS_BIT_SET(iLocalBS, LOCAL_BS_FPS_CAMERA_SET)
//			SET_BIT(iLocalBS, LOCAL_BS_FPS_CAMERA_SET)
//			//PRINTLN("CDM: setting LOCAL_BS_FPS_CAMERA_SET ")
//			IF GET_GAMEPLAY_CAM_RELATIVE_HEADING() > 0.1
//			OR GET_GAMEPLAY_CAM_RELATIVE_HEADING() < -0.1
//				SET_FIRST_PERSON_SHOOTER_CAMERA_HEADING(0)
//				CLEAR_BIT(iLocalBS, LOCAL_BS_FPS_CAMERA_SET)
//				//PRINTLN("CDM: clearing LOCAL_BS_FPS_CAMERA_SET ",GET_GAMEPLAY_CAM_RELATIVE_HEADING())
//			ENDIF
//			IF GET_GAMEPLAY_CAM_RELATIVE_PITCH() < -35
//			OR GET_GAMEPLAY_CAM_RELATIVE_PITCH() > -31
//				SET_FIRST_PERSON_SHOOTER_CAMERA_PITCH(-33.3452)
//				CLEAR_BIT(iLocalBS, LOCAL_BS_FPS_CAMERA_SET)
//				//PRINTLN("CDM: clearing LOCAL_BS_FPS_CAMERA_SET ",GET_GAMEPLAY_CAM_RELATIVE_PITCH())
//			ENDIF
//		ELSE
//			IF (iStickX > 140 or iStickX < 116)
//			OR (iStickY > 140 or iSticky < 116)
//				REINIT_NET_TIMER(stFPSCamTimeout,TRUE)
//			ENDIF
//			IF NOT HAS_NET_TIMER_STARTED(stFPSCamTimeout)
//			OR HAS_NET_TIMER_EXPIRED(stFPSCamTimeout,10000,TRUE)
//				IF GET_GAMEPLAY_CAM_RELATIVE_HEADING() > 0.0
//					fHeadingOffset = GET_GAMEPLAY_CAM_RELATIVE_HEADING() -@ 35.0
//					
//					IF fHeadingOffset < 0.0
//						fHeadingOffset = 0.0
//					ENDIF
//				ELSE
//					fHeadingOffset = GET_GAMEPLAY_CAM_RELATIVE_HEADING() +@ 35.0
//					
//					IF fHeadingOffset > 0.0
//						fHeadingOffset = 0.0
//					ENDIF
//				ENDIF
//				
//				IF GET_GAMEPLAY_CAM_RELATIVE_PITCH() > -33.3452
//					fPitchOffset = GET_GAMEPLAY_CAM_RELATIVE_PITCH() -@ 35.0
//					
//					IF fPitchOffset < -33.3452
//						fPitchOffset = -33.3452
//					ENDIF
//				ELSE
//					fPitchOffset = GET_GAMEPLAY_CAM_RELATIVE_PITCH() +@ 35.0
//					
//					IF fPitchOffset > -33.3452
//						fPitchOffset = -33.3452
//					ENDIF
//				ENDIF
//				PRINTLN("CDM:SET_FIRST_PERSON_SHOOTER_CAMERA_HEADING ",fHeadingOffset)
//				PRINTLN("CDM:SET_FIRST_PERSON_SHOOTER_CAMERA_PITCH ",fPitchOffset)
//				SET_FIRST_PERSON_SHOOTER_CAMERA_HEADING(fHeadingOffset)
//				SET_FIRST_PERSON_SHOOTER_CAMERA_PITCH(fPitchOffset)
//			ENDIF
//		ENDIF
//	ENDIF
ENDPROC

PROC SET_CLIENT_STATE(INT iStage)
	PRINTLN("CASINO_SLOTS: SET_CLIENT_STATE from ",iClientState, " to ",iStage)
	iClientState = iStage
ENDPROC

PROC EXIT_SLOT_MACHINE_SEAT(INT iExitType)
	WRITE_LIGHT_METRIC_DATA(FALSE)
	IF iCurrentlyUsedMachine != -1
		IF IS_BIT_SET(iLocalBS,LOCAL_BS_LOADED_MENU_HEADER)
			TEXT_LABEL_31 theText = GET_MACHINE_UI_HEADER(localSlots[iCurrentlyUsedMachine].iMachineType)
			SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(theText)
			CLEAR_BIT(iLocalBS,LOCAL_BS_LOADED_MENU_HEADER)
		ENDIF
	ENDIF
	SET_CURRENTLY_USED_MACHINE(-1)
	CLEAR_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS,PLAYER_BD_BS_SPINNING)
	CLEAR_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS,PLAYER_BD_BS_PROCESSED_RESULT)
	CLEAR_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS,PLAYER_BD_BS_FINISHED_SPINNING)
	CLEAR_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS,PLAYER_BD_BS_ABORTED_SPINNING)
//	IF DOES_CAM_EXIST(playingCamera)
//		RENDER_SCRIPT_CAMS(FALSE,FALSE)
//		DESTROY_CAM(playingCamera)
//	ENDIF
	//CLEAR_BIT(iLocalBS,LOCAL_BS_RESET_BET_IF_NEEDED)
	CLEAR_BIT(iLocalBS,LOCAL_BS_BET_TRANS_STARTED)
	CLEAR_BIT(iLocalBS,LOCAL_BS_BET_TRANS_COMPLETE)
	CLEAR_BIT(iLocalBS,LOCAL_BS_BET_PRESSED_SPIN)
	CLEAR_BIT(ilocalBS,LOCAL_BS_DETERMINED_RESULT)
	CLEAR_BIT(iLocalBS,LOCAL_BS_VIEWING_HELP_PAGE1)
	CLEAR_BIT(iLocalBS,LOCAL_BS_VIEWING_HELP_PAGE2)
	iPayoutMultipler = 1
	SET_CLIENT_STATE(LOCAL_STATE_LEAVE_MACHINE)
//	RESET_NET_TIMER(stIdleTimeout)
	START_NETWORK_SYNCED_SCENE_FOR_SLOTS(iExitType)//ANIM_SLOTS_EXIT_RIGHT)
	CLEAR_GAME_HELP()
	PRINTLN("CASINO_SLOTS: EXIT_SLOT_MACHINE_SEAT animation: ",iExitType)
ENDPROC


FUNC BOOL CAN_PLAYER_PLAY_SLOTS(INT iMachine)
	TEXT_LABEL_23 theReason
	BOOL bCantPlay			
	INT iReason
	IF SHOULD_ALLOW_CASINO_ENTRY_DURING_MISSION()
		RETURN FALSE
	ENDIF
	
	
	IF IS_PLAYER_BLOCKED_FROM_CASINO_GAME(CG_SLOTS,iReason)
		theReason = "SLOTS_REGBAN"
		bCantPlay = TRUE
	ELIF IS_LOCAL_PLAYER_BANNED_FROM_BETTING()
		IF GET_CASINO_GAME_BAN_REASON() = CBR_TIME_SPENT_PLAYING
			theReason = "CAS_MG_CTIME"
			bCantPlay = TRUE
		ELSE
			theReason = "CAS_MG_CBAN"
			bCantPlay = TRUE
		ENDIF
	ELIF NOT HAS_LOCAL_PLAYER_PURCHASED_CASINO_MEMBERSHIP()
	AND NOT DOES_BOSS_HAVE_CASINO_MEMBERSHIP()
		theReason = "CAS_MG_MEMB2"
		bCantPlay = TRUE	
	ELIF GET_LOCAL_PLAYERS_CASINO_CHIPS_TOTAL() <= 0
		theReason = "CAS_MG_NOCHIPS1"
		bCantPlay = TRUE
	ELIF GET_LOCAL_PLAYERS_CASINO_CHIPS_TOTAL() < localSlots[iMachine].iMachineCost
		theReason = "CAS_MG_LOWCHIPS1"
		bCantPlay = TRUE
	ELIF g_sMPTunables.bVC_CASINO_DISABLE_SLOTS
	OR (g_sMPTunables.bVC_CASINO_DISABLE_SLOTS_ANGEL AND localSlots[iMachine].iMachineType = MACHINE_1_ANGEL_AND_KNIGHT)
	OR (g_sMPTunables.bVC_CASINO_DISABLE_SLOTS_RAGE AND localSlots[iMachine].iMachineType = MACHINE_2_IMPOTENT_RAGE)
	OR (g_sMPTunables.bVC_CASINO_DISABLE_SLOTS_RSR AND localSlots[iMachine].iMachineType = MACHINE_3_REPUBLICAN_SPACE_RANGERS)
	OR (g_sMPTunables.bVC_CASINO_DISABLE_SLOTS_FAME AND localSlots[iMachine].iMachineType = MACHINE_4_FAME_OR_SHAME)
	OR (g_sMPTunables.bVC_CASINO_DISABLE_SLOTS_DEITY AND localSlots[iMachine].iMachineType = MACHINE_5_DEITY_OF_THE_SUN)
	OR (g_sMPTunables.bVC_CASINO_DISABLE_SLOTS_TWILIGHT AND localSlots[iMachine].iMachineType = MACHINE_6_KNIFE_AFTER_DARK)
	OR (g_sMPTunables.bVC_CASINO_DISABLE_SLOTS_DIAMOND AND localSlots[iMachine].iMachineType = MACHINE_7_THE_DIAMOND)
	OR (g_sMPTunables.bVC_CASINO_DISABLE_SLOTS_EVACUATOR AND localSlots[iMachine].iMachineType = MACHINE_8_EVACUATOR)
		theReason = "SLOTS_ENTERB"
		bCantPlay = TRUE
	ELIF IS_FM_JOB_ENTRY_OF_TYPE_THAT_SHOULD_BLOCK_ACCESS()
		PRINTLN("CASINO_SLOTS: CAN_PLAYER_PLAY_SLOTS false GET_FM_JOB_ENTERY_TYPE() = ",GET_FM_JOB_ENTERY_TYPE())
		theReason = "SLOTS_ENTERB"
		bCantPlay = TRUE
	ENDIF
	IF IS_TRANSITION_SESSION_LAUNCHING()
	OR AM_I_ACCEPTING_TRANSITION_SESSIONS_INVITE()
	OR AM_I_TRANSITION_SESSIONS_STARTING_ON_CALL()
	OR IS_SCREEN_FADED_OUT()
	OR IS_SCREEN_FADING_OUT()
		bCantPlay = TRUE
		PRINTLN("CAN_PLAYER_PLAY_SLOTS: false transition session launching")
		SET_BIT(iLocalBS,LOCAL_BS_BET_CANT_PLAY_HELP)
	ENDIF
	
	IF SHOULD_PLAYER_BE_REMOVED_FROM_CASINO_GAME_FOR_BOSS_LAUNCHING_MISSION()
		bCantPlay = TRUE
		PRINTLN("CAN_PLAYER_PLAY_SLOTS: SHOULD_PLAYER_BE_REMOVED_FROM_CASINO_GAME_FOR_BOSS_LAUNCHING_MISSION")
		SET_BIT(iLocalBS,LOCAL_BS_BET_CANT_PLAY_HELP)
	ENDIF
	
	IF bCantPlay
		IF ARE_STRINGS_EQUAL(theReason, "CAS_MG_MEMB2")
			IF NOT IS_THIS_HELP_MESSAGE_WITH_NUMBER_BEING_DISPLAYED("CAS_MG_MEMB2", GET_CASINO_MEMBERSHIP_COST(TRUE))
				PRINT_HELP_FOREVER_WITH_NUMBER("CAS_MG_MEMB2", GET_CASINO_MEMBERSHIP_COST(TRUE))
			ENDIF
			SET_BIT(iLocalBS,LOCAL_BS_BET_CANT_PLAY_HELP)
			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT_SECONDARY)
				CLEAR_HELP()
				
				REQUEST_CASINO_MEMBERSHIP_PURCHASE()
				PRINTLN("CAN_PLAYER_PLAY_SLOTS: triggering membership purchase")
				SET_CLIENT_STATE(LOCAL_STATE_MEMBERSHIP_PURCHASE)
			ENDIF
		ELSE
			IF NOT IS_BIT_SET(iLocalBS,LOCAL_BS_BET_CANT_PLAY_HELP)
				CLEAR_GAME_HELP()
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					PRINT_HELP(theReason)
					SET_BIT(iLocalBS,LOCAL_BS_BET_CANT_PLAY_HELP)
				ENDIF
			ENDIF
		ENDIF
		IF iSlotMachineContext != NEW_CONTEXT_INTENTION
			RELEASE_CONTEXT_INTENTION(iSlotMachineContext)
		ENDIF
		RETURN FALSE
	ENDIF

	RETURN TRUE
ENDFUNC

PROC SET_JACKPOT_ANNOUNCER_FLAG(INT iMachineType,INT iWinAmount)
	SWITCH iMachineType
		CASE MACHINE_1_ANGEL_AND_KNIGHT 		SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCasinoAnnouncerBS, ciCASINO_ANNOUNCER_TRIGGER_ANGEL_AND_THE_NIGHT_JACKPOT)	BREAK
		CASE MACHINE_2_IMPOTENT_RAGE 			SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCasinoAnnouncerBS, ciCASINO_ANNOUNCER_TRIGGER_IMPOTENT_RAGE_JACKPOT)	BREAK
		CASE MACHINE_3_REPUBLICAN_SPACE_RANGERS SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCasinoAnnouncerBS, ciCASINO_ANNOUNCER_TRIGGER_REPUBLICAN_SPACE_RANGERS_JACKPOT)	BREAK
		CASE MACHINE_4_FAME_OR_SHAME 			SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCasinoAnnouncerBS, ciCASINO_ANNOUNCER_TRIGGER_FAME_OR_SHAME_JACKPOT)	BREAK
		CASE MACHINE_5_DEITY_OF_THE_SUN 	
			IF iWinAmount >= 1000000
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCasinoAnnouncerBS, ciCASINO_ANNOUNCER_TRIGGER_DIETY_OF_THE_SUN_JACKPOT)	
			ELSE
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCasinoAnnouncerBS, ciCASINO_ANNOUNCER_TRIGGER_GENERIC_JACKPOT)
			ENDIF
		BREAK
		CASE MACHINE_6_KNIFE_AFTER_DARK 		SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCasinoAnnouncerBS, ciCASINO_ANNOUNCER_TRIGGER_TWILIGHT_KNIFE_JACKPOT)	BREAK
		CASE MACHINE_7_THE_DIAMOND 				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCasinoAnnouncerBS, ciCASINO_ANNOUNCER_TRIGGER_DIAMOND_MINER_JACKPOT)	BREAK
		CASE MACHINE_8_EVACUATOR 				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCasinoAnnouncerBS, ciCASINO_ANNOUNCER_TRIGGER_EVACUATOR_JACKPOT)	BREAK
	ENDSWITCH
ENDPROC


PROC HANDLE_PLAYING_SLOT_MACHINE()
	BOOL inputAllowed = TRUE
	TEXT_LABEL_23 tempText
	INT i
	//INT iRand
	HANDLE_SLOTS_ANIMS()
	HANDLE_FIRST_PERSON_CAMERA()
	IF IS_PAUSE_MENU_ACTIVE_EX()
	OR IS_BROWSER_OPEN()
	OR IS_COMMERCE_STORE_OPEN()
	OR IS_STORE_PENDING_NETWORK_SHUTDOWN_TO_OPEN()
	OR IS_PLAYER_SWITCH_IN_PROGRESS()
	OR IS_SELECTOR_ONSCREEN()
	OR IS_WARNING_MESSAGE_ACTIVE()
		inputAllowed = FALSE
	ENDIF
	
	IF IS_BIT_SET(serverBD.iBS_MachineSpinning[GET_BITSET_ARRAY_INDEX(iCurrentlyUsedMachine)],GET_BITSET_ELEMENT_ID_WITH_ARRAY(iCurrentlyUsedMachine))
	AND NOT IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS,PLAYER_BD_BS_FINISHED_SPINNING)
		IF NOT IS_CASINO_CHIPS_HUD_UPDATE_PAUSED()
			SET_PAUSE_CASINO_CHIPS_HUD_UPDATE(TRUE)
			SET_BIT(iLocalBS,LOCAL_BS_PAUSED_CHIPS_DISPLAY)
		ENDIF
	ELSE
		IF IS_BIT_SET(iLocalBS,LOCAL_BS_PAUSED_CHIPS_DISPLAY)
			IF IS_CASINO_CHIPS_HUD_UPDATE_PAUSED()
				SET_PAUSE_CASINO_CHIPS_HUD_UPDATE(FALSE)
			ENDIF
			CLEAR_BIT(iLocalBS,LOCAL_BS_PAUSED_CHIPS_DISPLAY)
		ENDIF
	ENDIF
	
	INT iNumChips
	
	INT iPayoutID
	
	IF inputAllowed	
		IF IS_CONTROL_JUST_RELEASED(PLAYER_CONTROL,INPUT_NEXT_CAMERA)
		OR IS_DISABLED_CONTROL_JUST_RELEASED(PLAYER_CONTROL,INPUT_NEXT_CAMERA)
			iCurrentCamera++
			IF iCurrentCamera > CAM_SLOTS_THIRD_PERS
				iCurrentCamera = 0
			ENDIF
			SET_SLOT_CAMERA_ACTIVE(iCurrentCamera)
			CLEAR_BIT(iLocalBS,LOCAL_BS_SETUP_CONTROLS)
		ENDIF
		IF IS_BIT_SET(iLocalBS,LOCAL_BS_VIEWING_HELP_PAGE1)
		OR IS_BIT_SET(iLocalBS,LOCAL_BS_VIEWING_HELP_PAGE2)
			IF IS_CONTROL_JUST_RELEASED(PLAYER_CONTROL,INPUT_FRONTEND_LEFT)
			OR IS_DISABLED_CONTROL_JUST_RELEASED(PLAYER_CONTROL,INPUT_FRONTEND_LEFT)
			OR IS_CONTROL_JUST_RELEASED(PLAYER_CONTROL,INPUT_FRONTEND_RIGHT)
			OR IS_DISABLED_CONTROL_JUST_RELEASED(PLAYER_CONTROL,INPUT_FRONTEND_RIGHT)
				IF IS_BIT_SET(iLocalBS,LOCAL_BS_VIEWING_HELP_PAGE1)
					SET_BIT(iLocalBS,LOCAL_BS_VIEWING_HELP_PAGE2)
					CLEAR_BIT(iLocalBS,LOCAL_BS_VIEWING_HELP_PAGE1)
				ELIF IS_BIT_SET(iLocalBS,LOCAL_BS_VIEWING_HELP_PAGE2)
					SET_BIT(iLocalBS,LOCAL_BS_VIEWING_HELP_PAGE1)
					CLEAR_BIT(iLocalBS,LOCAL_BS_VIEWING_HELP_PAGE2)
				ENDIF
				CLEAR_BIT(iLocalBS,LOCAL_BS_SETUP_CONTROLS)
			ENDIF
			IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL,INPUT_FRONTEND_CANCEL)
			OR IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL,INPUT_FRONTEND_CANCEL)
				CLEAR_BIT(iLocalBS,LOCAL_BS_VIEWING_HELP_PAGE1)
				CLEAR_BIT(iLocalBS,LOCAL_BS_VIEWING_HELP_PAGE2)
				CLEAR_BIT(iLocalBS,LOCAL_BS_SETUP_CONTROLS)
			ENDIF
		ELSE
			IF IS_CONTROL_JUST_RELEASED(PLAYER_CONTROL,INPUT_FRONTEND_RS)
			OR IS_DISABLED_CONTROL_JUST_RELEASED(PLAYER_CONTROL,INPUT_FRONTEND_RS)
				SET_BIT(iLocalBS,LOCAL_BS_VIEWING_HELP_PAGE1)
				CLEAR_BIT(iLocalBS,LOCAL_BS_SETUP_CONTROLS)
				EXIT
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iLocalBS2,LOCAL_BS2_FORCE_CONTROL_UPDATE_AFTER_SPIN)
		IF iCurrentAnim = ANIM_SLOTS_BASE 
		OR iCurrentAnim = ANIM_SLOTS_BETIDLE
		OR iCurrentAnim = ANIM_SLOTS_BETIDLE_TO_BET_ONE
		OR iCurrentAnim = ANIM_SLOTS_BETIDLE_TO_BET_MAX
		OR HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(),HASH("CAN_BREAKOUT_FOR_BETTING"))
			CLEAR_BIT(iLocalBS,LOCAL_BS_SETUP_CONTROLS)
			CLEAR_BIT(iLocalBS2,LOCAL_BS2_FORCE_CONTROL_UPDATE_AFTER_SPIN)
		ENDIF
	ENDIF
	

	//DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
	//DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE)
//	SET_INPUT_EXCLUSIVE(PLAYER_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
//	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
//	SET_INPUT_EXCLUSIVE(PLAYER_CONTROL, INPUT_FRONTEND_PAUSE)
//	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE)
	IF g_sMPTunables.bENABLE_SLOT_MACHINE_LIGHT
		IF NOT HAS_NET_TIMER_STARTED(slotMetrics.stLightTimer)
			START_NET_TIMER(slotMetrics.stLightTimer,TRUE)
		ELIF HAS_NET_TIMER_EXPIRED(slotMetrics.stLightTimer,120000,TRUE)
			WRITE_LIGHT_METRIC_DATA(TRUE)
		ENDIF
	ENDIF
	IF NOT bSetMetricTimerLight
		MetricTimerLight = GET_NETWORK_TIME()
		bSetMetricTimerLight = TRUE
	ENDIF
	IF NOT bSetMetricTimerHeavy
		MetricTimerHeavy = GET_NETWORK_TIME()
		bSetMetricTimerHeavy = TRUE
	ENDIF
	IF NOT IS_BIT_SET(iLocalBS,LOCAL_BS_BET_TRANS_STARTED)
		IF (iCurrentAnim = ANIM_SLOTS_BASE OR iCurrentAnim = ANIM_SLOTS_BETIDLE)
			IF NOT CAN_PLAYER_PLAY_SLOTS(iCurrentlyUsedMachine)
				EXIT_SLOT_MACHINE_SEAT(ANIM_SLOTS_EXIT_RIGHT)
				EXIT
			ENDIF
		ENDIF
		IF inputAllowed
		AND (NOT IS_BIT_SET(serverBD.iBS_MachineSpinning[GET_BITSET_ARRAY_INDEX(iCurrentlyUsedMachine)],GET_BITSET_ELEMENT_ID_WITH_ARRAY(iCurrentlyUsedMachine))
		OR IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS,PLAYER_BD_BS_FINISHED_SPINNING))
		AND NOT IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS,PLAYER_BD_BS_SPINNING)
		AND IS_BIT_SET(iLocalBS,LOCAL_BS_SETUP_CONTROLS)
		AND NOT (IS_BIT_SET(iLocalBS,LOCAL_BS_VIEWING_HELP_PAGE1) OR IS_BIT_SET(iLocalBS,LOCAL_BS_VIEWING_HELP_PAGE2))
			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_ACCEPT)
			OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_RT)
				IF IS_LOCAL_PLAYER_BANNED_FROM_BETTING()
					IF (iCurrentAnim = ANIM_SLOTS_BASE OR iCurrentAnim = ANIM_SLOTS_BETIDLE)
						IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
							IF GET_CASINO_GAME_BAN_REASON() = CBR_TIME_SPENT_PLAYING
								PRINT_HELP("CAS_MG_CTIME")
							ELSE
								PRINT_HELP("CAS_MG_CBAN")
							ENDIF
							SET_BIT(iLocalBS,LOCAL_BS_BET_CANT_PLAY_HELP)
						ENDIF
						EXIT_SLOT_MACHINE_SEAT(ANIM_SLOTS_EXIT_RIGHT)
						PRINTLN("CASINO_SLOTS: HANDLE_PLAYING_SLOT_MACHINE player banned from betting exiting")
					ENDIF	
				ELIF GET_LOCAL_PLAYERS_CASINO_CHIPS_TOTAL() >= localSlots[iCurrentlyUsedMachine].iMachineCost*iPayoutMultipler
					IF iCurrentAnim = ANIM_SLOTS_BASE 
					OR iCurrentAnim = ANIM_SLOTS_BETIDLE
					OR iCurrentAnim = ANIM_SLOTS_BETIDLE_TO_BET_ONE
					OR iCurrentAnim = ANIM_SLOTS_BETIDLE_TO_BET_MAX
					OR HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(),HASH("CAN_BREAKOUT_FOR_BETTING"))
						SET_BIT(iLocalBS,LOCAL_BS_BYPASS_NORMAL_ENTRY)
						CLEAR_BIT(iLocalBS,LOCAL_BS_BET_PRESSED_SPIN)
						
						IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_RT)
							PRINTLN("CASINO_SLOTS: HANDLE_PLAYING_SLOT_MACHINE player using pull spin" )
							PLAY_PULL_ANIM()
						ELSE
							PRINTLN("CASINO_SLOTS: HANDLE_PLAYING_SLOT_MACHINE player using button press spin")
							PLAY_SPIN_ANIM()
						ENDIF
						CLEAR_GAME_HELP()
						CLEAR_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS,PLAYER_BD_BS_PROCESSED_RESULT)
						CLEAR_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS,PLAYER_BD_BS_FINISHED_SPINNING)
						CLEAR_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS,PLAYER_BD_BS_SPINNING)
						CLEAR_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS,PLAYER_BD_BS_ABORTED_SPINNING)
						CLEAR_BIT(ilocalBS,LOCAL_BS_DETERMINED_RESULT)
						SET_BIT(iLocalBS,LOCAL_BS_BET_TRANS_STARTED)
						CLEAR_BIT(iLocalBS,LOCAL_BS_SETUP_CONTROLS)
						PRINTLN("CASINO_SLOTS: HANDLE_PLAYING_SLOT_MACHINE starting spin on machine = ",iCurrentlyUsedMachine)
					ENDIF
				ELSE
					IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
						PRINT_HELP("SLOTS_NOMON")
					ENDIF
				ENDIF
			ENDIF
			IF NOT IS_BIT_SET(iLocalBS,LOCAL_BS_BET_TRANS_STARTED)
				IF iCurrentAnim = ANIM_SLOTS_BASE 
				OR iCurrentAnim = ANIM_SLOTS_BETIDLE
				OR iCurrentAnim = ANIM_SLOTS_BETIDLE_TO_BET_ONE
				OR iCurrentAnim = ANIM_SLOTS_BETIDLE_TO_BET_MAX
				OR HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(),HASH("CAN_BREAKOUT_FOR_BETTING"))
					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_X)
					AND NOT IS_BIT_SET(iLocalBS,LOCAL_BS_PRESSED_BET)
						SET_BIT(iLocalBS,LOCAL_BS_BYPASS_NORMAL_ENTRY)
						DRAW_GAME_HELP(GAME_HELP_BETTING)
						iPayoutMultipler++
						IF iPayoutMultipler > 5
							iPayoutMultipler = 1
						ENDIF
						SET_SLOTS_SCALEFORM_BET_AMOUNT(localSlots[iCurrentlyUsedMachine].iMachineCost*iPayoutMultipler)
						IF (iCurrentAnim = ANIM_SLOTS_BASE OR iCurrentAnim = ANIM_SLOTS_BETIDLE)
						OR HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(),HASH("CAN_BREAKOUT_FOR_BETTING"))
							PLAY_BET_ANIM(0)
						ENDIF
						PLAY_SLOT_SOUND_FROM_PLAYER(PLAYER_ID(),localSlots[iCurrentlyUsedMachine].iMachineType,SLOT_SOUND_PLACE_BET)
						DRAW_GAME_HELP(GAME_HELP_BETTING)
						PRINTLN("CASINO_SLOTS: HANDLE_PLAYING_SLOT_MACHINE Local player has set iPayoutMultipler = ",iPayoutMultipler)
						CLEAR_BIT(iLocalBS,LOCAL_BS_SETUP_CONTROLS)
					ELSE
						IF NOT IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_X)
							CLEAR_BIT(iLocalBS,LOCAL_BS_PRESSED_BET)
						ENDIF
					ENDIF
				
					IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL,INPUT_FRONTEND_Y)
					AND NOT IS_BIT_SET(iLocalBS,LOCAL_BS_PRESSED_BET_MAX)
						SET_BIT(iLocalBS,LOCAL_BS_BYPASS_NORMAL_ENTRY)
						DRAW_GAME_HELP(GAME_HELP_BETTING)
						iPayoutMultipler = 5
						iNumChips = GET_LOCAL_PLAYERS_CASINO_CHIPS_TOTAL() 
						IF iNumChips < localSlots[iCurrentlyUsedMachine].iMachineCost*iPayoutMultipler
							iPayoutMultipler = FLOOR(TO_FLOAT(iNumChips/localSlots[iCurrentlyUsedMachine].iMachineCost))
						ENDIF
						IF (iCurrentAnim = ANIM_SLOTS_BASE OR iCurrentAnim = ANIM_SLOTS_BETIDLE)
						OR HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(),HASH("CAN_BREAKOUT_FOR_BETTING"))
							PLAY_BET_ANIM(1)
						ENDIF
						PLAY_SLOT_SOUND_FROM_PLAYER(PLAYER_ID(),localSlots[iCurrentlyUsedMachine].iMachineType,SLOT_SOUND_PLACE_BET_MAX)
						SET_SLOTS_SCALEFORM_BET_AMOUNT(localSlots[iCurrentlyUsedMachine].iMachineCost*iPayoutMultipler)
						PRINTLN("CASINO_SLOTS: HANDLE_PLAYING_SLOT_MACHINE Local player has maxed iPayoutMultipler = ",iPayoutMultipler)
						CLEAR_BIT(iLocalBS,LOCAL_BS_SETUP_CONTROLS)
					ELSE
						IF NOT IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_Y)
							CLEAR_BIT(iLocalBS,LOCAL_BS_PRESSED_BET_MAX)
						ENDIF
					ENDIF
				ENDIF
				IF (iCurrentAnim = ANIM_SLOTS_BASE OR iCurrentAnim = ANIM_SLOTS_BETIDLE)
					IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL,INPUT_FRONTEND_CANCEL)
					OR IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL,INPUT_FRONTEND_CANCEL)
						EXIT_SLOT_MACHINE_SEAT(ANIM_SLOTS_EXIT_RIGHT)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			PRINTLN("CASINO_SLOTS: Not allowing input because: inputAllowed = ",inputAllowed)
			PRINTLN("CASINO_SLOTS: Not allowing input because: server spinning = ",IS_BIT_SET(serverBD.iBS_MachineSpinning[GET_BITSET_ARRAY_INDEX(iCurrentlyUsedMachine)],GET_BITSET_ELEMENT_ID_WITH_ARRAY(iCurrentlyUsedMachine)))
			PRINTLN("CASINO_SLOTS: Not allowing input because: controls = ",IS_BIT_SET(iLocalBS,LOCAL_BS_SETUP_CONTROLS))
			PRINTLN("CASINO_SLOTS: Not allowing input because: PLAYER_BD_BS_FINISHED_SPINNING = ",IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS,PLAYER_BD_BS_FINISHED_SPINNING))
			PRINTLN("CASINO_SLOTS: Not allowing input because: PLAYER_BD_BS_SPINNING = ",IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS,PLAYER_BD_BS_SPINNING))
		ENDIF
	ELSE
		IF NOT IS_BIT_SET(iLocalBS,LOCAL_BS_BET_TRANS_COMPLETE)
			IF LOCAL_PLAYER_PLACE_CASINO_BET((localSlots[iCurrentlyUsedMachine].iMachineCost*iPayoutMultipler), eChipsTransactionResult, CG_SLOTS)
				IF eChipsTransactionResult = TRANSACTION_STATE_SUCCESS 
					//SET_BIT(iLocalBS,LOCAL_BS_RESET_BET_IF_NEEDED)
					SET_BIT(iLocalBS,LOCAL_BS_BET_TRANS_COMPLETE)
					slotMetrics.iBetAmount += localSlots[iCurrentlyUsedMachine].iMachineCost*iPayoutMultipler
				ELSE
					PRINTLN("CASINO_SLOTS: Local player spin has failed transaction")
					CLEAR_BIT(iLocalBS,LOCAL_BS_BET_TRANS_STARTED)
					CLEAR_BIT(ilocalBS,LOCAL_BS_DETERMINED_RESULT)
					//CLEAR_BIT(iLocalBS,LOCAL_BS_RESET_BET_IF_NEEDED)
					CLEAR_BIT(iLocalBS,LOCAL_BS_BET_TRANS_COMPLETE)
					CLEAR_BIT(iLocalBS,LOCAL_BS_BET_PRESSED_SPIN)
					CLEAR_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS,PLAYER_BD_BS_PROCESSED_RESULT)
					CLEAR_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS,PLAYER_BD_BS_SPINNING)
					CLEAR_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS,PLAYER_BD_BS_FINISHED_SPINNING)
					SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS,PLAYER_BD_BS_ABORTED_SPINNING)
					CLEAR_BIT(iLocalBS,LOCAL_BS_SETUP_CONTROLS)
					SET_BIT(iLocalBS2,LOCAL_BS2_FORCE_CONTROL_UPDATE_AFTER_SPIN)
					IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
						PRINT_HELP("SLOTS_FAILTR")
					ENDIF
					START_NETWORK_SYNCED_SCENE_FOR_SLOTS(ANIM_SLOTS_BETIDLE)
				ENDIF
				
				eChipsTransactionResult = TRANSACTION_STATE_DEFAULT
			ELSE
				PRINTLN("HANDLE_PLAYING_SLOT_MACHINE: waiting for chip transaction")
			ENDIF
		ENDIF
		IF NOT IS_BIT_SET(iLocalBS,LOCAL_BS_BET_PRESSED_SPIN)
			IF NOT IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS,PLAYER_BD_BS_ABORTED_SPINNING)
				IF iCurrentAnim = ANIM_SLOTS_BASE_TO_SPIN
				OR iCurrentAnim = ANIM_SLOTS_BETIDLE_TO_SPIN
				OR iCurrentAnim = ANIM_SLOTS_BASE_TO_PULL
				OR iCurrentAnim = ANIM_SLOTS_BETIDLE_TO_PULL
					IF GET_CURRENT_ANIM_PHASE() > 0.99
					OR HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(),HASH("START_SPIN"))
						PRINTLN("CASINO_SLOTS: Local player has successfully requested spin after anim: start spin = ",HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(),HASH("START_SPIN")))
						
						IF IS_BIT_SET(iLocalBS,LOCAL_BS_SETUP_CONTROLS)
							///CLEANUP_MENU_ASSETS()
							CLEAR_BIT(iLocalBS,LOCAL_BS_SETUP_CONTROLS)
						ENDIF
						SET_BIT(iLocalBS,LOCAL_BS_BET_PRESSED_SPIN)
						SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS,PLAYER_BD_BS_SPINNING)
					ENDIF
				ELSE
					PRINTLN("CASINO_SLOTS: Local player has successfully requested spin")
					IF IS_BIT_SET(iLocalBS,LOCAL_BS_SETUP_CONTROLS)
						CLEAR_MENU_DATA()
						CLEANUP_MENU_ASSETS()
						CLEAR_BIT(iLocalBS,LOCAL_BS_SETUP_CONTROLS)
					ENDIF
					SET_BIT(iLocalBS,LOCAL_BS_BET_PRESSED_SPIN)
					SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS,PLAYER_BD_BS_SPINNING)
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_BIT_SET(ilocalBS,LOCAL_BS_DETERMINED_RESULT)
				IF IS_BIT_SET(serverBD.iBS_MachineSpinning[GET_BITSET_ARRAY_INDEX(iCurrentlyUsedMachine)],GET_BITSET_ELEMENT_ID_WITH_ARRAY(iCurrentlyUsedMachine))
					DETERMINE_SLOT_MACHINE_RESULT(iCurrentlyUsedMachine,localResultOfSpin,slotSetup,localSlots #IF IS_DEBUG_BUILD,  iOverrideReelResult, iOverrideWheelResult #ENDIF)
					SET_BIT(ilocalBS,LOCAL_BS_DETERMINED_RESULT)
					PRINTLN("CASINO_SLOTS: Local player processing result - has result")
				ELSE
					#IF IS_DEBUG_BUILD
					PRINTLN("CASINO_SLOTS: Waiting for server to be spinning machine #",iCurrentlyUsedMachine," before generating results. Server is: ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(NETWORK_GET_HOST_OF_THIS_SCRIPT())))
					PRINTLN("PLAYER_BD_BS_SPINNING = ",IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS,PLAYER_BD_BS_SPINNING))
					PRINTLN("PLAYER_BD_BS_FINISHED_SPINNING = ",IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS,PLAYER_BD_BS_FINISHED_SPINNING))
					#ENDIF
					
					IF serverBD.iMachineUsedBy[iCurrentlyUsedMachine] != NATIVE_TO_INT(PLAYER_ID())

						PRINTLN("CASINO_SLOTS: Local player spin has failed transaction")
						CLEAR_BIT(iLocalBS,LOCAL_BS_BET_TRANS_STARTED)
						CLEAR_BIT(ilocalBS,LOCAL_BS_DETERMINED_RESULT)
						//CLEAR_BIT(iLocalBS,LOCAL_BS_RESET_BET_IF_NEEDED)
						CLEAR_BIT(iLocalBS,LOCAL_BS_BET_TRANS_COMPLETE)
						CLEAR_BIT(iLocalBS,LOCAL_BS_BET_PRESSED_SPIN)
						CLEAR_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS,PLAYER_BD_BS_PROCESSED_RESULT)
						CLEAR_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS,PLAYER_BD_BS_SPINNING)
						CLEAR_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS,PLAYER_BD_BS_FINISHED_SPINNING)
						CLEAR_BIT(iLocalBS,LOCAL_BS_SETUP_CONTROLS)
						SET_BIT(iLocalBS2,LOCAL_BS2_FORCE_CONTROL_UPDATE_AFTER_SPIN)
						IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
							PRINT_HELP("SLOTS_FAILTR")
						ENDIF
						START_NETWORK_SYNCED_SCENE_FOR_SLOTS(ANIM_SLOTS_BETIDLE)
					ENDIF
				ENDIF
			ELSE 
				IF NOT IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS,PLAYER_BD_BS_PROCESSED_RESULT)
					localSlots[iCurrentlyUsedMachine].iLastWin = DETERMINE_REEL_PAYOUT(iCurrentlyUsedMachine
																,slotSetup.iVirtualReelContent[0][localResultOfSpin.iVirtualReelPos[0]]
																,slotSetup.iVirtualReelContent[1][localResultOfSpin.iVirtualReelPos[1]]
																,slotSetup.iVirtualReelContent[2][localResultOfSpin.iVirtualReelPos[2]])
					//											,localResultOfSpin.iWheelPos)
					localSlots[iCurrentlyUsedMachine].iLastWheelWin = DETERMINE_WHEEL_PRIZE(iCurrentlyUsedMachine
											,slotSetup.iVirtualReelContent[0][localResultOfSpin.iVirtualReelPos[0]]
											,slotSetup.iVirtualReelContent[1][localResultOfSpin.iVirtualReelPos[1]]
											,slotSetup.iVirtualReelContent[2][localResultOfSpin.iVirtualReelPos[2]]
											,localResultOfSpin.iWheelPos)

					IF localSlots[iCurrentlyUsedMachine].iLastWin > 0
						IF CASINO_PAYOUT_REWARD_TO_LOCAL_PLAYER(localSlots[iCurrentlyUsedMachine].iLastWin, eChipsTransactionResult, CG_SLOTS)
							#IF IS_DEBUG_BUILD
							IF eChipsTransactionResult = TRANSACTION_STATE_SUCCESS
								PRINTLN("CASINO_SLOTS: CASINO_PAYOUT_REWARD_TO_LOCAL_PLAYER TRANSACTION_STATE_SUCCESS")
							ELSE
								PRINTLN("CASINO_SLOTS: CASINO_PAYOUT_REWARD_TO_LOCAL_PLAYER TRANSACTION_STATE_FAILED")
								SCRIPT_ASSERT("CASINO_SLOTS: CASINO_PAYOUT_REWARD_TO_LOCAL_PLAYER TRANSACTION_STATE_FAILED")
							ENDIF
							#ENDIF
							
							IF localSlots[iCurrentlyUsedMachine].iLastWheelWin > 0
								GIVE_SLOT_MACHINE_WHEEL_PRIZE(localSlots[iCurrentlyUsedMachine].iLastWheelWin)
							ENDIF
							
							PRINTLN("CASINO_SLOTS: player recieved payout in chips of : ",localSlots[iCurrentlyUsedMachine].iLastWin)
							REPEAT NUM_REELS i
								playerBD[NATIVE_TO_INT(PLAYER_ID())].fReelPropSlotResult[i] = localResultOfSpin.fPropReelPos[i]
								playerBD[NATIVE_TO_INT(PLAYER_ID())].iReelVirtSlotResults[i] = localResultOfSpin.iVirtualReelPos[i]
								PRINTLN("CASINO_SLOTS: player fReelPropSlotResult[",i,"] = ",localResultOfSpin.fPropReelPos[i])
								PRINTLN("CASINO_SLOTS: player iReelVirtSlotResults[",i,"] = ",localResultOfSpin.iVirtualReelPos[i])
							ENDREPEAT
							playerBD[NATIVE_TO_INT(PLAYER_ID())].iPrizeWheelResult = localResultOfSpin.iWheelPos
							WRITE_HEAVY_METRIC()
							SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS,PLAYER_BD_BS_PROCESSED_RESULT)
							
							eChipsTransactionResult = TRANSACTION_STATE_DEFAULT
						ELSE
							PRINTLN("CASINO_SLOTS: waiting for CASINO_PAYOUT_REWARD_TO_LOCAL_PLAYER")
						ENDIF
					ELSE
						REPEAT NUM_REELS i
							playerBD[NATIVE_TO_INT(PLAYER_ID())].fReelPropSlotResult[i] = localResultOfSpin.fPropReelPos[i]
							playerBD[NATIVE_TO_INT(PLAYER_ID())].iReelVirtSlotResults[i] = localResultOfSpin.iVirtualReelPos[i]
						ENDREPEAT
						playerBD[NATIVE_TO_INT(PLAYER_ID())].iPrizeWheelResult = localResultOfSpin.iWheelPos
						SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS,PLAYER_BD_BS_PROCESSED_RESULT)
						WRITE_HEAVY_METRIC()
						ADD_TO_CASINO_CHIPS_LOST_ON_BETTING_THIS_GAMEDAY(localSlots[iCurrentlyUsedMachine].iMachineCost*iPayoutMultipler)
					ENDIF
				ENDIF

				IF NOT IS_BIT_SET(serverBD.iBS_MachineSpinning[GET_BITSET_ARRAY_INDEX(iCurrentlyUsedMachine)],GET_BITSET_ELEMENT_ID_WITH_ARRAY(iCurrentlyUsedMachine))
				OR IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS,PLAYER_BD_BS_FINISHED_SPINNING)
					IF localSlots[iCurrentlyUsedMachine].iLastWin > 0
						IF IS_BIT_SET(iLocalBS,LOCAL_BS_WHEEL_WIN)
							START_NETWORK_SYNCED_SCENE_FOR_SLOTS(ANIM_SLOTS_WHEEL_TO_WIN)
						ELSE
							IF IS_BIT_SET(iLocalBS,LOCAL_BS_LARGE_PAYOUT)
								IF IS_BIT_SET(iLocalBS,LOCAL_BS_JACKPOT_PAYOUT)
									PLAY_SLOT_SOUND_FROM_PLAYER(PLAYER_ID(),localSlots[iCurrentlyUsedMachine].iMachineType,SLOT_SOUND_JACKPOT)
								ELSE
									PLAY_SLOT_SOUND_FROM_PLAYER(PLAYER_ID(),localSlots[iCurrentlyUsedMachine].iMachineType,SLOT_SOUND_BIG_WIN)
								ENDIF
								START_NETWORK_SYNCED_SCENE_FOR_SLOTS(ANIM_SLOTS_WIN_BIG)
							ELSE
								PLAY_SLOT_SOUND_FROM_PLAYER(PLAYER_ID(),localSlots[iCurrentlyUsedMachine].iMachineType,SLOT_SOUND_SMALL_WIN)
								START_NETWORK_SYNCED_SCENE_FOR_SLOTS(ANIM_SLOTS_WIN)
							ENDIF
						ENDIF
						tempText = GET_SLOT_MESSAGE_RANDOM(localSlots[iCurrentlyUsedMachine].iMachineType,TRUE)
						SET_SLOTS_SCALEFORM_MESSAGE(tempText)
						iPayoutID = DETERMINE_PAYOUT_ID(slotSetup.iVirtualReelContent[0][localResultOfSpin.iVirtualReelPos[0]]
													,slotSetup.iVirtualReelContent[1][localResultOfSpin.iVirtualReelPos[1]]
													,slotSetup.iVirtualReelContent[2][localResultOfSpin.iVirtualReelPos[2]])
						IF iPayoutID = SRS_SYMBOL6
							SET_JACKPOT_ANNOUNCER_FLAG(localSlots[iCurrentlyUsedMachine].iMachineType,localSlots[iCurrentlyUsedMachine].iLastWin)
						ENDIF
						DRAW_GAME_HELP(GAME_HELP_WIN,iPayoutID,localSlots[iCurrentlyUsedMachine].iLastWin,localSlots[iCurrentlyUsedMachine].iLastWheelWin)
					ELSE
						IF IS_BIT_SET(iLocalBS,LOCAL_BS_CRUEL_LOSE)
							START_NETWORK_SYNCED_SCENE_FOR_SLOTS(ANIM_SLOTS_LOSE_CRUEL)
						ELSE
							START_NETWORK_SYNCED_SCENE_FOR_SLOTS(ANIM_SLOTS_LOSE)
						ENDIF
						PLAY_SLOT_SOUND_FROM_PLAYER(PLAYER_ID(),localSlots[iCurrentlyUsedMachine].iMachineType,SLOT_SOUND_NO_WIN)
						tempText = GET_SLOT_MESSAGE_RANDOM(localSlots[iCurrentlyUsedMachine].iMachineType,FALSE)
						SET_SLOTS_SCALEFORM_MESSAGE(tempText)
					ENDIF
					SET_SLOTS_SCALEFORM_LAST_WIN(localSlots[iCurrentlyUsedMachine].iLastWin)
					CLEAR_BIT(iLocalBS,LOCAL_BS_BET_TRANS_STARTED)
					CLEAR_BIT(iLocalBS,LOCAL_BS_BET_TRANS_COMPLETE)
					CLEAR_BIT(iLocalBS,LOCAL_BS_BET_PRESSED_SPIN)
					CLEAR_BIT(ilocalBS,LOCAL_BS_DETERMINED_RESULT)
					PRINTLN("CASINO_SLOTS: finished spin, printed messages returning to start.")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_SLOT_MACHINE_HIGH_STAKES(INT iMachine)
	RETURN localSlots[iMachine].iMachineCost >= 500 
ENDFUNC

FUNC BOOL IS_AI_PED_SAT_AT_SEAT(INT iSeat)
	IF iSeat < 32
		RETURN IS_BIT_SET(g_iPedReservedSlotMachineBS,iSeat)
	ELSE
		iSeat = iSeat-32
		RETURN IS_BIT_SET(g_iPedReservedSlotMachineBS2,iSeat)
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL SLOT_HELP_TEXT_DISPLAYED()
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SLOTS_NOMON")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SLOTS_FAILTR")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SLOTS_USED")
		RETURN TRUE
	ENDIF
	IF IS_BIT_SET(iLocalBS,LOCAL_BS_BET_CANT_PLAY_HELP)
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CAS_MG_CBAN")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CAS_MG_CTIME")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CAS_MG_LOWCHIPS1")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CAS_MG_NOCHIPS1")
		OR IS_THIS_HELP_MESSAGE_WITH_NUMBER_BEING_DISPLAYED("CAS_MG_MEMB2", GET_CASINO_MEMBERSHIP_COST(TRUE))
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SLOTS_REGBAN")
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CHECK_FOR_TRIGGER_ENTERING_MACHINE()
	VECTOR vLoc[2]
	VECTOR vOffsetInFront, vOffsetInFront2
	INT x
	TEXT_LABEL_23 tempText
	//INT iReasonCantPlay
	IF IS_NET_PLAYER_OK(PLAYER_ID())
	AND Get_Peds_Drunk_Alcohol_Hit_Count(PLAYER_PED_ID()) <= 9
	AND NOT g_SpawnData.bPassedOutDrunk
	AND NOT IS_PED_RAGDOLL(PLAYER_PED_ID())
	AND NOT IS_BIT_SET(g_simpleInteriorData.iFifthBS, BS5_SIMPLE_INTERIOR_INSTANT_PASS_OUT)
	AND NOT IS_BROWSER_OPEN()
	AND NOT IS_COMMERCE_STORE_OPEN()
	AND NOT IS_STORE_PENDING_NETWORK_SHUTDOWN_TO_OPEN()
	AND NOT IS_PLAYER_SWITCH_IN_PROGRESS()
	AND NOT IS_SELECTOR_ONSCREEN()
	AND NOT IS_PAUSE_MENU_ACTIVE_EX()
	AND NOT IS_TRANSITION_SESSION_LAUNCHING()
	AND NOT AM_I_TRANSITION_SESSIONS_STARTING_ON_CALL()
	AND NOT IS_SCREEN_FADED_OUT()
	AND NOT IS_SCREEN_FADING_OUT()
	AND NOT g_OfficeHeliDockData.bBossWantsToTakeoff
	AND g_iSimpleInteriorState != SIMPLE_INT_STATE_HELI_TAKEOFF_EXIT

//		vLoc[0] = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(slotMachine[0], <<1103.645874,230.744156,-50.637714>>)
//		PRINTLN("vLoc[0] = ",vLoc[0])

		VECTOR vCoords
		FLOAT fHeading
		IF iNearbyMachine < 0
			REPEAT 8 x
				GET_SLOT_MACHINE_POSITION(iNearbyCounter,vCoords,fHeading)
				vLoc[0] = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCoords,fHeading, << 0.0579063, -0.256112, -0.197113 >>)
				vLoc[1] = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCoords,fHeading, << 0.0289372, -1.85613, 2.75002 >>)
				
				vOffsetInFront = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCoords,fHeading, << 0, -0.4,1 >>)
				vOffsetInFront2 = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCoords,fHeading, << 0, -0.6,1 >>)
				//DRAW_DEBUG_SPHERE(vOffsetInFront,0.1,255,0,0)
				fHeading = GET_HEADING_FROM_COORDS_LA(GET_PLAYER_COORDS(PLAYER_ID()),vCoords)
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(),vLoc[0],vLoc[1], 1.250000)
					IF IS_HEADING_ACCEPTABLE_CORRECTED(GET_ENTITY_HEADING(PLAYER_PED_ID()),fHeading,45)
					OR IS_ENTITY_FACING_COORD_WITHIN_RANGE(PLAYER_PED_ID(),vOffsetInFront,45)
					OR IS_ENTITY_FACING_COORD_WITHIN_RANGE(PLAYER_PED_ID(),vOffsetInFront2,45)
						iNearbyMachine = iNearbyCounter
					ENDIF
				ENDIF
				iNearbyCounter++
				IF iNearbyCounter >= MAX_SLOT_MACHINES
					iNearbyCounter = 0
				ENDIF
			ENDREPEAT
		ELSE
			GET_SLOT_MACHINE_POSITION(iNearbyMachine,vCoords,fHeading)
			vLoc[0] = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCoords,fHeading, << 0.0579063, -0.256112, -0.197113 >>)
			vLoc[1] = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCoords,fHeading, << 0.0289372, -1.85613, 2.75002 >>)
			vOffsetInFront = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCoords,fHeading, << 0, -0.4,1 >>)
			vOffsetInFront2 = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCoords,fHeading, << 0, -0.6,1 >>)
			fHeading = GET_HEADING_FROM_COORDS_LA(GET_PLAYER_COORDS(PLAYER_ID()),vCoords)
			IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(),vLoc[0],vLoc[1], 1.250000)
			OR (NOT IS_HEADING_ACCEPTABLE_CORRECTED(GET_ENTITY_HEADING(PLAYER_PED_ID()),fHeading,45) 
			AND NOT IS_ENTITY_FACING_COORD_WITHIN_RANGE(PLAYER_PED_ID(),vOffsetInFront,45)
			AND NOT  IS_ENTITY_FACING_COORD_WITHIN_RANGE(PLAYER_PED_ID(),vOffsetInFront2,45))
				iNearbyMachine = -1
			ENDIF
		ENDIF
		IF iNearbyMachine >= 0
			GET_SLOT_MACHINE_POSITION(iNearbyMachine,vCoords,fHeading)
			//<<1101.0417, 229.9508, -50.0747>>
			vLoc[0] = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCoords,fHeading, << 0.0, -0.5, 0.77 >>)
			IF serverBD.iMachineUsedBy[iNearbyMachine] = -1
			AND NOT IS_AI_PED_SAT_AT_SEAT(iNearbyMachine)
			//IS_POSITION_OCCUPIED(vLoc[0],0.1,FALSE,FALSE,TRUE,FALSE,FALSE,PLAYER_PED_ID())
				IF IS_BIT_SET(iLocalBS,LOCAL_BS_MACHINE_IN_USE_HELP)
					CLEAR_BIT(iLocalBS,LOCAL_BS_MACHINE_IN_USE_HELP)
				ENDIF
				IF CAN_PLAYER_PLAY_SLOTS(iNearbyMachine)
						
					IF iSlotMachineContext = NEW_CONTEXT_INTENTION
						tempText = "SLOTS_ENTER"
						tempText += localSlots[iNearbyMachine].iMachineType
						IF NOT HAS_LOCAL_PLAYER_PURCHASED_CASINO_MEMBERSHIP()
							IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(),FALSE)
								PLAYER_INDEX bossID
								bossID = GB_GET_LOCAL_PLAYER_GANG_BOSS()
								IF HAS_PLAYER_PURCHASED_CASINO_MEMBERSHIP(bossID)
									IF GB_GET_PLAYER_GANG_TYPE(bossID) = GT_BIKER
										tempText += "c"
									ELIF DOES_PLAYER_OWN_OFFICE(bossID)
										tempText += "a"
									ELSE
										tempText += "b"
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						REGISTER_CONTEXT_INTENTION(iSlotMachineContext,CP_MEDIUM_PRIORITY,tempText)
					ENDIF
					IF HAS_CONTEXT_BUTTON_TRIGGERED(iSlotMachineContext)
						IF iSlotMachineContext != NEW_CONTEXT_INTENTION
							RELEASE_CONTEXT_INTENTION(iSlotMachineContext)
						ENDIF
						iTriggeringMachine = iNearbyMachine
						PRINTLN("CASINO_SLOTS: Local player has triggered entry to slot machine #",iNearbyMachine)
						NET_SET_PLAYER_CONTROL(PLAYER_ID(),FALSE,NSPC_LEAVE_CAMERA_CONTROL_ON)
						DISPLAY_RADAR(FALSE)
						SET_DISABLE_RANK_UP_MESSAGE(TRUE) 
						DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
						DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE)
						SET_INPUT_EXCLUSIVE(PLAYER_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
						SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
						SET_INPUT_EXCLUSIVE(PLAYER_CONTROL, INPUT_FRONTEND_PAUSE)
						SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
						IF NOT IS_BIT_SET(iLocalBS,LOCAL_BS_TOOK_AWAY_CONTROL)
							SET_BIT(iLocalBS,LOCAL_BS_TOOK_AWAY_CONTROL)
						ENDIF
						RETURN TRUE
					ENDIF
				ENDIF
			ELSE
				PRINTLN("CASINO_SLOTS: position occupied = ",vLoc[0])
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					IF serverBD.iMachineUsedBy[iNearbyMachine] != NATIVE_TO_INT(PLAYER_ID())
						IF NOT IS_BIT_SET(iLocalBS,LOCAL_BS_MACHINE_IN_USE_HELP)
						AND NOT SHOULD_ALLOW_CASINO_ENTRY_DURING_MISSION()
							PRINT_HELP("SLOTS_USED")
							SET_BIT(iLocalBS,LOCAL_BS_MACHINE_IN_USE_HELP)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF SLOT_HELP_TEXT_DISPLAYED()
				CLEAR_HELP()
			ENDIF
			IF IS_BIT_SET(iLocalBS,LOCAL_BS_MACHINE_IN_USE_HELP)
				CLEAR_BIT(iLocalBS,LOCAL_BS_MACHINE_IN_USE_HELP)
			ENDIF
			IF IS_BIT_SET(iLocalBS,LOCAL_BS_BET_CANT_PLAY_HELP)
				CLEAR_BIT(iLocalBS,LOCAL_BS_BET_CANT_PLAY_HELP)
			ENDIF
			IF iSlotMachineContext != NEW_CONTEXT_INTENTION
				RELEASE_CONTEXT_INTENTION(iSlotMachineContext)
			ENDIF
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		IF Get_Peds_Drunk_Alcohol_Hit_Count(PLAYER_PED_ID()) >= 9
		OR g_SpawnData.bPassedOutDrunk
		OR IS_PED_RAGDOLL(PLAYER_PED_ID())
		OR IS_BIT_SET(g_simpleInteriorData.iFifthBS, BS5_SIMPLE_INTERIOR_INSTANT_PASS_OUT)
			PRINTLN("CHECK_FOR_TRIGGER_ENTERING_MACHINE: not allowing player is too drunk")
		ENDIF
		#ENDIF
		IF SLOT_HELP_TEXT_DISPLAYED()
			CLEAR_HELP()
		ENDIF
		IF IS_BIT_SET(iLocalBS,LOCAL_BS_MACHINE_IN_USE_HELP)
			CLEAR_BIT(iLocalBS,LOCAL_BS_MACHINE_IN_USE_HELP)
		ENDIF
		IF IS_BIT_SET(iLocalBS,LOCAL_BS_BET_CANT_PLAY_HELP)
			CLEAR_BIT(iLocalBS,LOCAL_BS_BET_CANT_PLAY_HELP)
		ENDIF
		IF iSlotMachineContext != NEW_CONTEXT_INTENTION
			RELEASE_CONTEXT_INTENTION(iSlotMachineContext)
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_MACHINE_OBJECTS_BE_VISIBLE(INT iMachine)
	VECTOR vMachinePos
	FLOAT fHeading
	GET_SLOT_MACHINE_POSITION(iMachine,vMachinePos,fHeading)
	
	IF GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(PLAYER_ID()),vMachinePos) <= 10
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC INT GET_RANDOM_ATTRACT_SLOT_MACHINE(INT iLastMachine,INT iPosition)
	
	INT iMachinesToPick[36]
	INT iCount
	INT i,j
	INT iStart
	INT iEnd
	IF iPosition < MAX_SLOT_ATTRACT_SOUNDS/2
		iStart = 0
		iEnd = 35
	ELSE
		iStart = 36
		iEnd = MAX_SLOT_MACHINES-1
	ENDIF
	BOOL bFoundAlready
	FOR i = iStart TO iEnd
		IF serverBD.iMachineUsedBy[i] = -1
		AND NOT IS_AI_PED_SAT_AT_SEAT(i)
			bFoundAlready = FALSE
			REPEAT MAX_SLOT_ATTRACT_SOUNDS j
				IF slotsAttract[j].iMachine = i
					bFoundAlready = TRUE
					j = MAX_SLOT_ATTRACT_SOUNDS
				ENDIF
			ENDREPEAT
			IF NOT bFoundAlready
				iMachinesToPick[iCount] = i
				iCount++
			ENDIF
		ENDIF
	ENDFOR
	PRINTLN("GET_RANDOM_ATTRACT_SLOT_MACHINE: count = ",iCount)
	INT iRandInt = GET_RANDOM_INT_IN_RANGE(0,iCount)
	REPEAT iCount i
		IF iLastMachine != -1
		AND (iMachinesToPick[iRandInt] = iLastMachine
		OR localSlots[iMachinesToPick[iRandInt]].iMachineType = localSlots[iLastMachine].iMachineType)
			iRandInt++
			IF iRandInt >= iCount
				iRandInt=0
			ENDIF
		ELSE
			RETURN iMachinesToPick[iRandInt]
		ENDIF
	ENDREPEAT
	RETURN -1
ENDFUNC

PROC HANDLE_SLOT_MACHINE_ATTRACT_SOUNDS()
	INT i
	INT iMachine
	vector vCoords
	float fHeading
	STRING soundSet
	
	REPEAT MAX_SLOT_ATTRACT_SOUNDS i
		IF slotsAttract[i].iSoundID = -1
			iMachine = GET_RANDOM_ATTRACT_SLOT_MACHINE(slotsAttract[i].iMachine,i)
			IF iMachine != -1
				slotsAttract[i].iMachine = iMachine
				slotsAttract[i].iSoundID = GET_SOUND_ID()
				GET_SLOT_MACHINE_POSITION(iMachine,vCoords,fHeading)
				vCoords = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCoords,fHeading,<<0.0,-0.2,1>>)
				soundSet = GET_MACHINE_TYPE_SOUNDSET(localSlots[iMachine].iMachineType)
				PLAY_SOUND_FROM_COORD(slotsAttract[i].iSoundID,"attract_loop",vCoords,soundSet)
				PRINTLN("HANDLE_SLOT_MACHINE_ATTRACT_SOUNDS: playing attract sound for machine #",slotsAttract[i].iMachine," at coords: ",vCoords)
			ELSE
				PRINTLN("HANDLE_SLOT_MACHINE_ATTRACT_SOUNDS: found no free machine??")
			ENDIF
		ELSE
			IF IS_AI_PED_SAT_AT_SEAT(slotsAttract[i].iMachine)
			OR serverBD.iMachineUsedBy[slotsAttract[i].iMachine] != -1
				IF serverBD.iMachineUsedBy[slotsAttract[i].iMachine] != NATIVE_TO_INT(PLAYER_ID())
					STOP_SOUND(slotsAttract[i].iSoundID)
					RELEASE_SOUND_ID(slotsAttract[i].iSoundID)
					slotsAttract[i].iSoundID = -1
					PRINTLN("HANDLE_SLOT_MACHINE_ATTRACT_SOUNDS: clearing attract sound for occupied machine #",slotsAttract[i].iMachine)
				ENDIF
			ELIF HAS_SOUND_FINISHED(slotsAttract[i].iSoundID)
				RELEASE_SOUND_ID(slotsAttract[i].iSoundID)
				slotsAttract[i].iSoundID = -1
				PRINTLN("HANDLE_SLOT_MACHINE_ATTRACT_SOUNDS: clearing attract sound for finished sound machine #",slotsAttract[i].iMachine)
			ENDIF 
		ENDIF
	ENDREPEAT
ENDPROC

PROC HANDLE_SLOT_MACHINE_OBJECT_CREATION()
	MODEL_NAMES eModel 
	INT i,x 
	vector vCoords
	float fHeading
	TEXT_LABEL_63 modelText
	

	REPEAT MAX_SLOT_MACHINES x
		IF SHOULD_MACHINE_OBJECTS_BE_VISIBLE(x)

			IF DOES_SLOT_MACHINE_HAVE_WHEEL(x,localSlots)
				IF NOT DOES_ENTITY_EXIST(slotPrizeWheel[x])
					IF CAN_REGISTER_MISSION_ENTITIES(0,0,1,0)
						modelText = "vw_Prop_vw_slot_wheel_0"
						modelText += localSlots[x].iMachineType
						modelText += "a"
						eModel = INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY(modelText))
						PRINTLN("CASINO_SLOTS:1 HANDLE_SLOT_MACHINE_OBJECT_CREATION trying to create model ",modelText)
						REQUEST_MODEL(eModel)
						IF HAS_MODEL_LOADED(eModel)
							GET_SLOT_MACHINE_POSITION(x,vCoords,fHeading)
							vCoords = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCoords,fHeading,<<0.0,-0.120,1.600>>)
							
							slotPrizeWheel[x] = CREATE_OBJECT(eModel, vCoords,FALSE,FALSE,TRUE)
							FREEZE_ENTITY_POSITION(slotPrizeWheel[x],true)
							SET_ENTITY_ROTATION(slotPrizeWheel[x],<<-0.0000, -0.0000, fHeading>>)
							SET_MODEL_AS_NO_LONGER_NEEDED(eModel)
							PRINTLN("CASINO_SLOTS: HANDLE_SLOT_MACHINE_OBJECT_CREATION created wheel for machine #",x," at: ",vCoords)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			REPEAT 3 i
				IF NOT DOES_ENTITY_EXIST(slotReel[x][i])
					IF CAN_REGISTER_MISSION_ENTITIES(0,0,1,0)
						modelText = "vw_Prop_Casino_Slot_0"
						modelText += localSlots[x].iMachineType
						modelText += "a_reels"
						eModel = INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY(modelText))
						//eModel = INT_TO_ENUM(MODEL_NAMES, HASH("vw_Prop_Casino_Slot_01aa_reels"))
						PRINTLN("CASINO_SLOTS:2 HANDLE_SLOT_MACHINE_OBJECT_CREATION trying to create model ",modelText)
						REQUEST_MODEL(eModel)
						IF HAS_MODEL_LOADED(eModel)
							GET_SLOT_MACHINE_POSITION(x,vCoords,fHeading)
							PRINTLN("CDM TEMP:1 vCoords = ",vCoords, " fHeading = ",fHeading)
							IF i = 0
								vCoords = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCoords,fHeading, <<-0.115,0.047,0.906>>)
							ELIF i = 1
								vCoords = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCoords,fHeading, <<0.005,0.047,0.906>>)
							ELIF i = 2
								vCoords = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCoords,fHeading, <<0.125,0.047,0.906>>)
							ENDIF
							PRINTLN("CDM TEMP:2 vCoords = ",vCoords, " fHeading = ",fHeading)
							
//								IF i = 0
//									vCoords = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(slotMachine[x], <<-0.115,0.047,0.906>>)
//								ELIF i = 1
//									vCoords = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(slotMachine[x], <<0.005,0.047,0.906>>)
//								ELIF i = 2
//									vCoords = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(slotMachine[x], <<0.125,0.047,0.906>>)
//								ENDIF
//								PRINTLN("CDM TEMP:3 vCoords = ",vCoords, " fHeading = ",fHeading)

							
							slotReel[x][i] = CREATE_OBJECT(eModel,vCoords ,FALSE,FALSE,TRUE)
							PRINTLN("HANDLE_SLOT_MACHINE_OBJECT_CREATION: Creating reel #",i," for machine #",x," at: ",vCoords)
							FREEZE_ENTITY_POSITION(slotReel[x][i],true)
							SET_ENTITY_COLLISION(slotReel[x][i], FALSE)
							SET_ENTITY_ROTATION(slotReel[x][i],<<-0.0000, -0.0000, fHeading>>)
							SET_MODEL_AS_NO_LONGER_NEEDED(eModel)
						ENDIF	
					ENDIF
				ENDIF
			ENDREPEAT
		ELSE
			REPEAT 3 i
				IF DOES_ENTITY_EXIST(slotReel[x][i])
					DELETE_OBJECT(slotReel[x][i])
					PRINTLN("HANDLE_SLOT_MACHINE_OBJECT_CREATION: deleting reel #",i, " for machine #",x)
				ENDIF
			ENDREPEAT
			IF DOES_ENTITY_EXIST(slotPrizeWheel[x])
				DELETE_OBJECT(slotPrizeWheel[x])
				PRINTLN("HANDLE_SLOT_MACHINE_OBJECT_CREATION: prize wheel for machine #",x)
			ENDIF
		ENDIF
	ENDREPEAT

	IF iCurrentlyUsedMachine >= 0
		REPEAT 3 i
			IF NOT DOES_ENTITY_EXIST(slotBlurryReel[i])
				IF CAN_REGISTER_MISSION_ENTITIES(0,0,1,0)
					modelText = "Vw_prop_casino_slot_0"
					modelText += localSlots[iCurrentlyUsedMachine].iMachineType
					modelText += "b_reels"
					eModel = INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY(modelText))
					PRINTLN("CASINO_SLOTS:3 HANDLE_SLOT_MACHINE_OBJECT_CREATION trying to create model ",modelText)
					//eModel = INT_TO_ENUM(MODEL_NAMES, HASH("Vw_prop_casino_slot_01bb_reels"))
					REQUEST_MODEL(eModel)
					IF HAS_MODEL_LOADED(eModel)
						GET_SLOT_MACHINE_POSITION(iCurrentlyUsedMachine,vCoords,fHeading)
						IF i = 0
							vCoords = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCoords,fHeading, <<-0.115,0.047,0.906>>)
						ELIF i = 1
							vCoords = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCoords,fHeading, <<0.005,0.047,0.906>>)
						ELIF i = 2
							vCoords = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCoords,fHeading, <<0.125,0.047,0.906>>)
						ENDIF
						vCoords.z = vCoords.z -10
						slotBlurryReel[i] = CREATE_OBJECT(eModel, vCoords,FALSE,FALSE,TRUE)
						FREEZE_ENTITY_POSITION(slotBlurryReel[i],true)
						SET_ENTITY_ROTATION(slotBlurryReel[i],<<-0.0000, -0.0000, fHeading>>)
						SET_MODEL_AS_NO_LONGER_NEEDED(eModel)
						PRINTLN("CASINO_SLOTS: HANDLE_SLOT_MACHINE_OBJECT_CREATION created blurry reel for slot machine #",iCurrentlyUsedMachine)
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		IF DOES_SLOT_MACHINE_HAVE_WHEEL(iCurrentlyUsedMachine,localSlots)
			IF NOT DOES_ENTITY_EXIST(slotBlurryWheel)
				IF CAN_REGISTER_MISSION_ENTITIES(0,0,1,0)
					modelText = "vw_prop_vw_slot_wheel_0"
					modelText += localSlots[iCurrentlyUsedMachine].iMachineType
					modelText += "b"
					eModel = INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY(modelText))
					PRINTLN("CASINO_SLOTS:4 HANDLE_SLOT_MACHINE_OBJECT_CREATION trying to create model ",modelText)
					//eModel = INT_TO_ENUM(MODEL_NAMES, HASH("vw_prop_vw_slot_wheel_01b"))
					REQUEST_MODEL(eModel)
					IF HAS_MODEL_LOADED(eModel)
						GET_SLOT_MACHINE_POSITION(iCurrentlyUsedMachine,vCoords,fHeading)
						vCoords = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCoords,fHeading,<<0.0,-0.120,1.600>>)
						vCoords.z = vCoords.z -10
						slotBlurryWheel = CREATE_OBJECT(eModel, vCoords,FALSE,FALSE,TRUE)
						FREEZE_ENTITY_POSITION(slotBlurryWheel,true)
						SET_ENTITY_ROTATION(slotBlurryWheel,<<-0.0000, -0.0000, fHeading>>)
						SET_MODEL_AS_NO_LONGER_NEEDED(eModel)
						PRINTLN("CASINO_SLOTS: HANDLE_SLOT_MACHINE_OBJECT_CREATION created blurry wheel for slot machine #",x)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		REPEAT 3 i
			IF DOES_ENTITY_EXIST(slotBlurryReel[i])
				DELETE_OBJECT(slotBlurryReel[i])
			ENDIF
		ENDREPEAT
		IF DOES_ENTITY_EXIST(slotBlurryWheel)
			DELETE_OBJECT(slotBlurryWheel)
		ENDIF
	ENDIF
ENDPROC


PROC HANDLE_SLOT_MACHINE_UI()
	#IF IS_DEBUG_BUILD
	DEBUG_DRAW_SLOT_MACHINE()
	#ENDIF
	IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
		IF NOT bUsingPCControls
			CLEAR_BIT(iLocalBS,LOCAL_BS_SETUP_CONTROLS)
		ENDIF
		bUsingPCControls = TRUE
	ELSE
		IF bUsingPCControls
			CLEAR_BIT(iLocalBS,LOCAL_BS_SETUP_CONTROLS)
		ENDIF
		bUsingPCControls = FALSE
	ENDIF
	IF NOT IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS,PLAYER_BD_BS_SPINNING)
		IF (iCurrentAnim = ANIM_SLOTS_BASE OR iCurrentAnim = ANIM_SLOTS_BETIDLE OR iCurrentAnim = ANIM_SLOTS_BETIDLE_TO_BET_ONE OR iCurrentAnim = ANIM_SLOTS_BETIDLE_TO_BET_MAX)
			IF IS_BIT_SET(iLocalBS,LOCAL_BS_WAS_SPINNING)
				CLEAR_BIT(iLocalBS,LOCAL_BS_SETUP_CONTROLS)
			ENDIF
		ENDIF
	ELSE
		SET_BIT(iLocalBS,LOCAL_BS_WAS_SPINNING)
	ENDIF
	
	
	IF iCurrentlyUsedMachine >= 0
	AND (iClientState = LOCAL_STATE_USING_MACHINE OR IS_BIT_SET(iLocalBS,LOCAL_BS_CAN_BYPASS_NORMAL_ENTRY))
		DRAW_SLOT_MACHINE_SCALEFORM()
		IF NOT IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS,PLAYER_BD_BS_SPINNING)
		AND NOT IS_BIT_SET(iLocalBS,LOCAL_BS_BET_TRANS_STARTED)
		AND (iCurrentAnim = ANIM_SLOTS_BASE OR iCurrentAnim = ANIM_SLOTS_BETIDLE OR iCurrentAnim = ANIM_SLOTS_BETIDLE_TO_BET_ONE OR iCurrentAnim = ANIM_SLOTS_BETIDLE_TO_BET_MAX)
			IF NOT HAS_NET_TIMER_STARTED(stTimerWinHelp)
			OR HAS_NET_TIMER_EXPIRED(stTimerWinHelp,5000,TRUE)
				DRAW_GAME_HELP(GAME_HELP_BETTING)
			ENDIF
		ENDIF
		
		IF NOT IS_BIT_SET(iLocalBS,LOCAL_BS_SETUP_CONTROLS)
			
			IF IS_BIT_SET(iLocalBS,LOCAL_BS_VIEWING_HELP_PAGE1)
			OR IS_BIT_SET(iLocalBS,LOCAL_BS_VIEWING_HELP_PAGE2)
				TEXT_LABEL_23 theText = GET_MACHINE_UI_HEADER(localSlots[iCurrentlyUsedMachine].iMachineType)
				PRINTLN("HANDLE_SLOT_MACHINE_UI: loading texture dictionary: ",theText)
				//IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED(theText)
				REQUEST_STREAMED_TEXTURE_DICT(theText)
				//ENDIF
				SET_BIT(iLocalBS,LOCAL_BS_LOADED_MENU_HEADER)
				IF LOAD_MENU_ASSETS()
				AND HAS_STREAMED_TEXTURE_DICT_LOADED(theText)
					CLEAR_MENU_DATA()
					SET_MENU_USES_HEADER_GRAPHIC(TRUE, theText, theText)
					SET_CURRENT_MENU_ITEM(-1)
					IF IS_BIT_SET(iLocalBS,LOCAL_BS_VIEWING_HELP_PAGE1)
						SET_MENU_TITLE("SLOTS_RULEaT")
						theText = "SLOTS_RULEa"
						theText += localSlots[iCurrentlyUsedMachine].iMachineType
						SET_CURRENT_MENU_ITEM_DESCRIPTION(theText)
					ELIF IS_BIT_SET(iLocalBS,LOCAL_BS_VIEWING_HELP_PAGE2)
						SET_MENU_TITLE("SLOTS_RULEbT")
						SET_CURRENT_MENU_ITEM_DESCRIPTION("SLOTS_RULEb")
						ADD_CURRENT_MENU_ITEM_DESCRIPTION_INT(localSlots[iCurrentlyUsedMachine].iMachineCost)
						ADD_CURRENT_MENU_ITEM_DESCRIPTION_INT(localSlots[iCurrentlyUsedMachine].iMachineCost*5)
					ENDIF
					IF IS_BIT_SET(iLocalBS,LOCAL_BS_VIEWING_HELP_PAGE1)
						SET_MENU_TITLE_ROW_COUNT_OVERRIDE(TRUE, 1, 2)
					ELSE
						SET_MENU_TITLE_ROW_COUNT_OVERRIDE(TRUE, 2, 2)
					ENDIF
					
					ADD_MENU_HELP_KEY_INPUT(INPUT_FRONTEND_CANCEL,"SLOTS_RULEBK")
					ADD_MENU_HELP_KEY_GROUP(INPUTGROUP_FRONTEND_DPAD_LR,"SLOTS_RULENEXT")
					SET_BIT(iLocalBS,LOCAL_BS_SETUP_CONTROLS)
					DRAW_MENU()
					SET_INSTRUCTIONAL_BUTTONS_UNDER_HUD_THIS_FRAME()
				ENDIF
			ELSE
				IF LOAD_MENU_ASSETS()
					CLEAR_MENU_DATA()
					IF NOT IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS,PLAYER_BD_BS_SPINNING)
					AND (iCurrentAnim = ANIM_SLOTS_BASE 
					OR iCurrentAnim = ANIM_SLOTS_BETIDLE 
					OR iCurrentAnim = ANIM_SLOTS_BETIDLE_TO_BET_ONE 
					OR iCurrentAnim = ANIM_SLOTS_BETIDLE_TO_BET_MAX 
					OR IS_BIT_SET(iLocalBS,LOCAL_BS_CAN_BYPASS_NORMAL_ENTRY)
					OR HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(),HASH("CAN_BREAKOUT_FOR_BETTING")))
						ADD_MENU_HELP_KEY_INPUT(INPUT_FRONTEND_CANCEL,"SLOTS_EXIT") //"IB_CIN_EXIT"
						ADD_MENU_HELP_KEY_WITH_MULTIPLE_BUTTONS("SLOTS_SPIN",GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL,INPUT_FRONTEND_RT),GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL,INPUT_FRONTEND_ACCEPT))
						//ADD_MENU_HELP_KEY_INPUT(INPUT_NEXT_CAMERA,"SLOTS_CAM")
						
						ADD_MENU_HELP_KEY_INPUT(INPUT_FRONTEND_X,"SLOTS_BET")
						IF iPayoutMultipler < 5
							ADD_MENU_HELP_KEY_INPUT(INPUT_FRONTEND_Y,"SLOTS_BETM")
						ENDIF
					ENDIF
					ADD_MENU_HELP_KEY_INPUT(INPUT_FRONTEND_RS,"SLOTS_RULEIN")
					SET_BIT(iLocalBS,LOCAL_BS_SETUP_CONTROLS)
					DRAW_MENU_HELP_SCALEFORM(0,DEFAULT,-1)
					SET_INSTRUCTIONAL_BUTTONS_UNDER_HUD_THIS_FRAME()
				ENDIF
			ENDIF
		ELSE
			IF IS_BIT_SET(iLocalBS,LOCAL_BS_VIEWING_HELP_PAGE1)
			OR IS_BIT_SET(iLocalBS,LOCAL_BS_VIEWING_HELP_PAGE2)
				DRAW_MENU()
			ELSE
				DRAW_MENU_HELP_SCALEFORM(0,DEFAULT,-1)
			ENDIF
			SET_INSTRUCTIONAL_BUTTONS_UNDER_HUD_THIS_FRAME()
		ENDIF	
	ELSE
		IF IS_BIT_SET(iLocalBS,LOCAL_BS_SETUP_CONTROLS)
			CLEAR_MENU_DATA()
			CLEANUP_MENU_ASSETS()
			
			CLEAR_BIT(iLocalBS,LOCAL_BS_SETUP_CONTROLS)
		ENDIF
	ENDIF
ENDPROC

PROC DETERMINE_NPC_SLOT_MACHINE_RESULT(RESULT_STRUCT &results,CASINO_PED_SLOT_MACHINE_STATES state)
	INT i,j,iRand, iSymbolCounter
//	#IF IS_DEBUG_BUILD
//	TEXT_LABEL_23 tlTemp
//	#ENDIF

	results.fPropReelPos[0] = 0
	results.fPropReelPos[1] = 0
	results.fPropReelPos[2] = 0
	INT iOptionsArray[SRS_MAX_SYMBOLS]
	INT iCounter
	INT iSRSResults[NUM_REELS]
	SWITCH state
		CASE CASINO_PED_SLOT_MACHINE_STATE_BAD_1
		CASE CASINO_PED_SLOT_MACHINE_STATE_BAD_2
			iRand = GET_RANDOM_INT_IN_RANGE(SRS_SYMBOL1,SRS_SYMBOL4)
			iSRSResults[0] = iRand
			iSRSResults[1] = iRand
			iCounter = 0
			REPEAT SRS_MAX_SYMBOLS i
				IF i != iSRSResults[0]
				AND i != SRS_WILD
					iOptionsArray[iCounter] = i
					iCounter++
				ENDIF
			ENDREPEAT
			iRand = GET_RANDOM_INT_IN_RANGE(0,iCounter)
			iSRSResults[2] = iOptionsArray[iRand]
			PRINTLN("DETERMINE_NPC_SLOT_MACHINE_RESULT: BAD result reel 1 = ",iSRSResults[0]," reel 2 = ",iSRSResults[1]," reel 3 = ",iSRSResults[2])
		BREAK
		CASE CASINO_PED_SLOT_MACHINE_STATE_IMPARTIAL_1
		CASE CASINO_PED_SLOT_MACHINE_STATE_IMPARTIAL_2
		CASE CASINO_PED_SLOT_MACHINE_STATE_IMPARTIAL_3
			iRand = GET_RANDOM_INT_IN_RANGE(SRS_SYMBOL_BLANK,SRS_SYMBOL6)
			iSRSResults[0] = iRand
			iCounter = 0
			REPEAT SRS_SYMBOL6 i
				IF i != iSRSResults[0]
				OR iSRSResults[0] = SRS_SYMBOL_BLANK
					iOptionsArray[iCounter] = i
					iCounter++
				ENDIF
			ENDREPEAT
			iRand = GET_RANDOM_INT_IN_RANGE(0,iCounter)
			iSRSResults[1] = iOptionsArray[iRand]
			iCounter = 0
			REPEAT SRS_SYMBOL6 i
				IF i != iSRSResults[1]
				OR iSRSResults[1] = SRS_SYMBOL_BLANK
					iOptionsArray[iCounter] = i
					iCounter++
				ENDIF
			ENDREPEAT
			iRand = GET_RANDOM_INT_IN_RANGE(0,iCounter)
			iSRSResults[2] = iOptionsArray[iRand]
			PRINTLN("DETERMINE_NPC_SLOT_MACHINE_RESULT: IMPARTIAL result reel 1 = ",iSRSResults[0]," reel 2 = ",iSRSResults[1]," reel 3 = ",iSRSResults[2])
		BREAK
		CASE CASINO_PED_SLOT_MACHINE_STATE_GOOD_1
		CASE CASINO_PED_SLOT_MACHINE_STATE_GOOD_2
			iRand = GET_RANDOM_INT_IN_RANGE(SRS_SYMBOL1,SRS_SYMBOL3)
			iSRSResults[0] = iRand
			iSRSResults[1] = iRand
			iSRSResults[2] = iRand
			PRINTLN("DETERMINE_NPC_SLOT_MACHINE_RESULT: GOOD result reel 1 = ",iSRSResults[0]," reel 2 = ",iSRSResults[1]," reel 3 = ",iSRSResults[2])
		BREAK
		CASE CASINO_PED_SLOT_MACHINE_STATE_TERRIBLE
			iRand = GET_RANDOM_INT_IN_RANGE(SRS_SYMBOL5,SRS_SYMBOL6)
			iSRSResults[0] = iRand
			iSRSResults[1] = iRand
			iSRSResults[2] = SRS_SYMBOL_BLANK
			PRINTLN("DETERMINE_NPC_SLOT_MACHINE_RESULT: TERRIBLE result reel 1 = ",iSRSResults[0]," reel 2 = ",iSRSResults[1]," reel 3 = ",iSRSResults[2])
		BREAK
		CASE CASINO_PED_SLOT_MACHINE_STATE_GREAT
			iRand = GET_RANDOM_INT_IN_RANGE(SRS_SYMBOL4,SRS_SYMBOL5)
			iSRSResults[0] = iRand
			iSRSResults[1] = iRand
			iSRSResults[2] = iRand
			PRINTLN("DETERMINE_NPC_SLOT_MACHINE_RESULT: GREAT result reel 1 = ",iSRSResults[0]," reel 2 = ",iSRSResults[1]," reel 3 = ",iSRSResults[2])
		BREAK
	ENDSWITCH
	
	
	REPEAT NUM_REELS i
		iRand = 0
		IF iSRSResults[i] = SRS_SYMBOL_BLANK
			iRand = GET_RANDOM_INT_IN_RANGE(0,slotSetup.iSizeOfPropReel)
			results.fPropReelPos[i] = iRand + 0.5
		ELSE
			// check how many of that symbol
			REPEAT slotSetup.iSizeOfPropReel j
				IF slotSetup.iPropReelContent[i][j] = iSRSResults[i]
					iSymbolCounter++
				ENDIF
			ENDREPEAT
			// get random in for those
			iRand = GET_RANDOM_INT_IN_RANGE(0,iSymbolCounter)
			iSymbolCounter = 0
			j = 0
			REPEAT slotSetup.iSizeOfPropReel j
				IF slotSetup.iPropReelContent[i][j] = iSRSResults[i]
					// set position on prop wheel
					IF iSymbolCounter = iRand
						results.fPropReelPos[i] = TO_FLOAT(j)
						j = slotSetup.iSizeOfPropReel
					ENDIF
					iSymbolCounter++
				ENDIF
			ENDREPEAT
		ENDIF
	ENDREPEAT
ENDPROC


PROC HANDLE_NPC_SLOT_MACHINE_OBJECT_INTERACTION()
	INT i,x, j
	VECTOR vTemp
	FLOAT fHeading
	BOOL bStopReel[NUM_REELS]
	BOOL bReelStopped[NUM_REELS]
	#IF IS_DEBUG_BUILD
	TEXT_LABEL_23 tlTemp
	#ENDIF

	#IF IS_DEBUG_BUILD
	IF bPrintDebugOuput 
		PRINTLN("HANDLE_NPC_SLOT_MACHINE_OBJECT_INTERACTION:bPrintDebugOuput is set ")
	ENDIF
	#ENDIF
	RESULT_STRUCT results
	REPEAT MAX_CASINO_SLOT_MACHINE_PEDS i
		IF g_SlotMachineSpinningState[i].iSlotMachineID >= 0
			IF NOT IS_BIT_SET(NPCData.iUsedBS[GET_BITSET_ARRAY_INDEX(g_SlotMachineSpinningState[i].iSlotMachineID)],GET_BITSET_ELEMENT_ID_WITH_ARRAY(g_SlotMachineSpinningState[i].iSlotMachineID))
			AND IS_AI_PED_SAT_AT_SEAT(g_SlotMachineSpinningState[i].iSlotMachineID)
				SET_BIT(NPCData.iUsedBS[GET_BITSET_ARRAY_INDEX(g_SlotMachineSpinningState[i].iSlotMachineID)],GET_BITSET_ELEMENT_ID_WITH_ARRAY(g_SlotMachineSpinningState[i].iSlotMachineID))
				PRINTLN("CASINO_SLOTS: HANDLE_NPC_SLOT_MACHINE_OBJECT_INTERACTION- setting machine #",g_SlotMachineSpinningState[i].iSlotMachineID," used by NPC #",i)
			ENDIF
			IF g_SlotMachineSpinningState[i].bSpinning
				IF NOT IS_BIT_SET(NPCData.iSpinningBS[GET_BITSET_ARRAY_INDEX(g_SlotMachineSpinningState[i].iSlotMachineID)],GET_BITSET_ELEMENT_ID_WITH_ARRAY(g_SlotMachineSpinningState[i].iSlotMachineID))
					
					DETERMINE_NPC_SLOT_MACHINE_RESULT(results,g_SlotMachineSpinningState[i].eOutcome)
					REINIT_NET_TIMER(NPCData.spinTimer[g_SlotMachineSpinningState[i].iSlotMachineID])
					REPEAT NUM_REELS x
						NPCData.fReelPropSlotResults[g_SlotMachineSpinningState[i].iSlotMachineID][x] = results.fPropReelPos[x]
						#IF IS_DEBUG_BUILD
						IF bPrintDebugOuput 
							PRINTLN("CASINO_SLOTS: HANDLE_NPC_SLOT_MACHINE_OBJECT_INTERACTION- PCData.fReelPropSlotResults[",g_SlotMachineSpinningState[i].iSlotMachineID,"][",x,"] = ",NPCData.fReelPropSlotResults[g_SlotMachineSpinningState[i].iSlotMachineID][x])
						ENDIF
						#ENDIF
					ENDREPEAT
					SET_BIT(NPCData.iSpinningBS[GET_BITSET_ARRAY_INDEX(g_SlotMachineSpinningState[i].iSlotMachineID)],GET_BITSET_ELEMENT_ID_WITH_ARRAY(g_SlotMachineSpinningState[i].iSlotMachineID))
					#IF IS_DEBUG_BUILD
					IF bPrintDebugOuput 
						PRINTLN("CASINO_SLOTS: HANDLE_NPC_SLOT_MACHINE_OBJECT_INTERACTION- setting SPINNING machine #",g_SlotMachineSpinningState[i].iSlotMachineID," used by NPC #",i)
					ENDIF
					#ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	REPEAT MAX_SLOT_MACHINES x
		IF IS_BIT_SET(NPCData.iSpinningBS[GET_BITSET_ARRAY_INDEX(x)],GET_BITSET_ELEMENT_ID_WITH_ARRAY(x))
			GET_SLOT_MACHINE_POSITION(x,vTemp,fHeading)
			REPEAT NUM_REELS i
				bStopReel[i] = FALSE
				bReelStopped[i] = FALSE
				IF HAS_NET_TIMER_STARTED(NPCData.spinTimer[x])
					IF HAS_NET_TIMER_EXPIRED(NPCData.spinTimer[x],(SLOTS_REEL_SPIN_TIME-3500)+(750*i))
						IF i > 0
							IF bReelStopped[i-1]
								IF HAS_NET_TIMER_STARTED(stReelStopDelay[x][i-1])
									IF HAS_NET_TIMER_EXPIRED(stReelStopDelay[x][i-1],150)
										bStopReel[i] = TRUE
										#IF IS_DEBUG_BUILD
										IF bPrintDebugOuput 
											PRINTLN("HANDLE_NPC_SLOT_MACHINE_OBJECT_INTERACTION: 1 stopping reel #",i," on machine #",x)
										ENDIF
										#ENDIF
										IF localSlots[x].fPropReelTarget[i] = -1
											localSlots[x].fPropReelTarget[i] = NPCData.fReelPropSlotResults[x][i]*22.5 + FLOOR(localSlots[x].fPropReelRots[i]/360)*360
											#IF IS_DEBUG_BUILD
											IF bPrintDebugOuput 
												PRINTLN("HANDLE_NPC_SLOT_MACHINE_OBJECT_INTERACTION: setting target rotation  #",i," on machine #",x, " to: ",localSlots[x].fPropReelTarget[i])
											ENDIF
											#ENDIF
										ENDIF
									ENDIF
								ELSE
									START_NET_TIMER(stReelStopDelay[x][i-1])
								ENDIF
							ENDIF
						ELSE
							bStopReel[i] = TRUE	
							IF localSlots[x].fPropReelTarget[i] = -1
								localSlots[x].fPropReelTarget[i] = NPCData.fReelPropSlotResults[x][i]*22.5 + FLOOR(localSlots[x].fPropReelRots[i]/360)*360
								#IF IS_DEBUG_BUILD
								IF bPrintDebugOuput 
									PRINTLN("HANDLE_NPC_SLOT_MACHINE_OBJECT_INTERACTION: 2 setting target rotation  #",i," on machine #",x, " to: ",localSlots[x].fPropReelTarget[i])
								ENDIF
								#ENDIF
							ENDIF
							#IF IS_DEBUG_BUILD
							IF bPrintDebugOuput 
								PRINTLN("HANDLE_NPC_SLOT_MACHINE_OBJECT_INTERACTION: 2 stopping reel #",i," on machine #",x)
							ENDIF
							#ENDIF
						ENDIF
					ENDIF
				ENDIF
				IF DOES_ENTITY_EXIST(slotReel[x][i])
					IF localSlots[x].fPropReelPrevRots[i] != localSlots[x].fPropReelRots[i]
						IF NOT bStopReel[i]
							OVERRIDE_REEL_ROTATION_IF_NEEDED(x,i)
						ENDIF
						
						SET_ENTITY_ROTATION(slotReel[x][i],<<localSlots[x].fPropReelRots[i],0,fHeading>>)
						localSlots[x].fPropReelPrevRots[i] = localSlots[x].fPropReelRots[i]
					ENDIF
				ENDIF

				IF bStopReel[i]
					FLOAT fDif = (NPCData.fReelPropSlotResults[x][i]*22.5)- localSlots[x].fPropReelRots[i]%360
					IF ABSF(fDif) <= 20
					OR (localSlots[x].fPropReelTarget[i] != -1
					AND localSlots[x].fPropReelRots[i] >= localSlots[x].fPropReelTarget[i])
						//STOP
						IF localSlots[x].fPropReelRots[i] != NPCData.fReelPropSlotResults[x][i]*22.5
							PLAY_SLOT_SOUND_FROM_MACHINE_FOR_NPC(x,SLOT_SOUND_REEL_STOP)
							IF NPCData.fReelPropSlotResults[x][i] = SRS_SYMBOL6
								PLAY_SLOT_SOUND_FROM_MACHINE_FOR_NPC(x,SLOT_SOUND_REEL_STOP_ON_PRIZE)
							ENDIF
						
							#IF IS_DEBUG_BUILD
							IF bPrintDebugOuput 
								PRINTLN("HANDLE_NPC_SLOT_MACHINE_OBJECT_INTERACTION: 1 setting rotation of reel #",i," = ",localSlots[x].fPropReelRots[i])
								PRINTLN("HANDLE_NPC_SLOT_MACHINE_OBJECT_INTERACTION: reel # ",i," result pos = ",NPCData.fReelPropSlotResults[x][i])
								tlTemp = GET_SLOT_MACHINE_SYMBOL_NAME(slotSetup.iPropReelContent[i][FLOOR(NPCData.fReelPropSlotResults[x][i])])
								PRINTLN("HANDLE_NPC_SLOT_MACHINE_OBJECT_INTERACTION: reel # ",i," result symbol = ",tlTemp)
								PRINTLN("HANDLE_NPC_SLOT_MACHINE_OBJECT_INTERACTION: stop reel # ",i," on machine # ",x," = ",localSlots[x].fPropReelRots[i])
							ENDIF
							#ENDIF
						ENDIF
						localSlots[x].fPropReelRots[i] = NPCData.fReelPropSlotResults[x][i]*22.5
						bReelStopped[i] = TRUE
						#IF IS_DEBUG_BUILD
						IF bPrintDebugOuput 
							PRINTLN("HANDLE_NPC_SLOT_MACHINE_OBJECT_INTERACTION: reel # ",i," is stopped on machine # ",x," = ",localSlots[x].fPropReelRots[i])
						ENDIF
						#ENDIF
						IF DOES_ENTITY_EXIST(slotReel[x][i])
							SET_ENTITY_ROTATION(slotReel[x][i],<<localSlots[x].fPropReelRots[i],0,fHeading>>)
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
						IF bOverrideSpinSpeed
							localSlots[x].fPropReelRots[i] = localSlots[x].fPropReelRots[i] +@ iReelSpinSpeed
						ELSE
						#ENDIF
						localSlots[x].fPropReelRots[i] = localSlots[x].fPropReelRots[i] +@ 600
						
						#IF IS_DEBUG_BUILD
						ENDIF
						#ENDIF
						#IF IS_DEBUG_BUILD
						IF bPrintDebugOuput 
							PRINTLN("HANDLE_NPC_SLOT_MACHINE_OBJECT_INTERACTION: SPINNING!! but want to stop reel # ",i," on machine # ",x," = ",localSlots[x].fPropReelRots[i])
						ENDIF
						#ENDIF
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
					IF bOverrideSpinSpeed
						localSlots[x].fPropReelRots[i] = localSlots[x].fPropReelRots[i] +@ iReelSpinSpeed
					ELSE
					#ENDIF
					localSlots[x].fPropReelRots[i] = localSlots[x].fPropReelRots[i] +@ 600
					
					#IF IS_DEBUG_BUILD
					ENDIF
					#ENDIF
					#IF IS_DEBUG_BUILD
					IF bPrintDebugOuput 
						PRINTLN("HANDLE_NPC_SLOT_MACHINE_OBJECT_INTERACTION: SPINNING!! reel # ",i," on machine # ",x," = ",localSlots[x].fPropReelRots[i])
					ENDIF
					#ENDIF
				ENDIF
			ENDREPEAT
			
			IF NOT IS_BIT_SET(iBS_MachineSpinAudio[GET_BITSET_ARRAY_INDEX(x)],GET_BITSET_ELEMENT_ID_WITH_ARRAY(x))
				#IF IS_DEBUG_BUILD
				IF bPrintDebugOuput 
					PRINTLN("HANDLE_NPC_SLOT_MACHINE_OBJECT_INTERACTION: SPINNING!! playing sound on machine # ",x)
				ENDIF
				#ENDIF
				PLAY_SLOT_SOUND_FROM_MACHINE_FOR_NPC(x,SLOT_SOUND_SPINNING)
				PLAY_SLOT_SOUND_FROM_MACHINE_FOR_NPC(x,SLOT_SOUND_START_SPIN)
				SET_BIT(iBS_MachineSpinAudio[GET_BITSET_ARRAY_INDEX(x)],GET_BITSET_ELEMENT_ID_WITH_ARRAY(x))
			ENDIF
			
			IF bReelStopped[2]
				REPEAT MAX_CASINO_SLOT_MACHINE_PEDS j
					IF g_SlotMachineSpinningState[j].iSlotMachineID = x
						CLEAR_BIT(iBS_MachineSpinAudio[GET_BITSET_ARRAY_INDEX(x)],GET_BITSET_ELEMENT_ID_WITH_ARRAY(x))
						localSlots[x].fPropPrizeWheelTarget = -1
						REPEAT NUM_REELS i
							localSlots[x].fPropReelTarget[i] = -1
							IF DOES_ENTITY_EXIST(slotReel[x][i])
								IF localSlots[x].fPropReelRots[i] != NPCData.fReelPropSlotResults[x][i]*22.5
									
									GET_SLOT_MACHINE_POSITION(x,vTemp,fHeading)
									localSlots[x].fPropReelRots[i] = NPCData.fReelPropSlotResults[x][i]*22.5
									SET_ENTITY_ROTATION(slotReel[x][i],<<localSlots[x].fPropReelRots[i],0,fHeading>>)
								ENDIF
							ELSE
								localSlots[x].fPropReelRots[i] = 0
							ENDIF
						ENDREPEAT
						IF DOES_ENTITY_EXIST(slotPrizeWheel[x])
							localSlots[x].fPropPrizeWheelRot = 0
							SET_ENTITY_ROTATION(slotPrizeWheel[x],<<0,0,fHeading>>)
						ENDIF
						#IF IS_DEBUG_BUILD
						IF bPrintDebugOuput 
							PRINTLN("CASINO_SLOTS: HANDLE_NPC_SLOT_MACHINE_OBJECT_INTERACTION- setting NOT SPINNING machine #",g_SlotMachineSpinningState[j].iSlotMachineID," used by NPC #",j)
						ENDIF
						#ENDIF
						RESET_NET_TIMER(stReelStopDelay[x][0])
						RESET_NET_TIMER(stReelStopDelay[x][1])
						RESET_NET_TIMER(stWheelStartDelay[x])
						g_SlotMachineSpinningState[j].bSpinning = FALSE
						RESET_NET_TIMER(NPCData.spinTimer[g_SlotMachineSpinningState[j].iSlotMachineID])
						CLEAR_BIT(NPCData.iSpinningBS[GET_BITSET_ARRAY_INDEX(g_SlotMachineSpinningState[j].iSlotMachineID)],GET_BITSET_ELEMENT_ID_WITH_ARRAY(g_SlotMachineSpinningState[j].iSlotMachineID))
						
					ENDIF
				ENDREPEAT
			ENDIF
		ELSE
			REPEAT MAX_CASINO_SLOT_MACHINE_PEDS j
				IF g_SlotMachineSpinningState[j].iSlotMachineID = x
					CLEAR_BIT(iBS_MachineSpinAudio[GET_BITSET_ARRAY_INDEX(x)],GET_BITSET_ELEMENT_ID_WITH_ARRAY(x))
					localSlots[x].fPropPrizeWheelTarget = -1
					REPEAT NUM_REELS i
						localSlots[x].fPropReelTarget[i] = -1
						IF DOES_ENTITY_EXIST(slotReel[x][i])
							IF localSlots[x].fPropReelRots[i] != NPCData.fReelPropSlotResults[x][i]*22.5
								
								GET_SLOT_MACHINE_POSITION(x,vTemp,fHeading)
								localSlots[x].fPropReelRots[i] = NPCData.fReelPropSlotResults[x][i]*22.5
								SET_ENTITY_ROTATION(slotReel[x][i],<<localSlots[x].fPropReelRots[i],0,fHeading>>)
							ENDIF
						ELSE
							localSlots[x].fPropReelRots[i] = 0
						ENDIF
					ENDREPEAT
					IF DOES_ENTITY_EXIST(slotPrizeWheel[x])
						localSlots[x].fPropPrizeWheelRot = 0
						SET_ENTITY_ROTATION(slotPrizeWheel[x],<<0,0,fHeading>>)
					ENDIF
					#IF IS_DEBUG_BUILD
					IF bPrintDebugOuput 
						PRINTLN("CASINO_SLOTS: HANDLE_NPC_SLOT_MACHINE_OBJECT_INTERACTION- setting NOT SPINNING machine #",g_SlotMachineSpinningState[j].iSlotMachineID," used by NPC #",i)
					ENDIF
					#ENDIF
					RESET_NET_TIMER(stReelStopDelay[x][0])
					RESET_NET_TIMER(stReelStopDelay[x][1])
					RESET_NET_TIMER(stWheelStartDelay[x])
					RESET_NET_TIMER(NPCData.spinTimer[g_SlotMachineSpinningState[j].iSlotMachineID])
					CLEAR_BIT(NPCData.iSpinningBS[GET_BITSET_ARRAY_INDEX(g_SlotMachineSpinningState[j].iSlotMachineID)],GET_BITSET_ELEMENT_ID_WITH_ARRAY(g_SlotMachineSpinningState[j].iSlotMachineID))
				ENDIF
			ENDREPEAT
		ENDIF
	ENDREPEAT
ENDPROC

PROC HANDLE_SLOT_MACHINE_OBJECT_INTERACTION()
	INT i,x, iPayoutID
	VECTOR vTemp
	FLOAT fHeading
	BOOL bStopReel[NUM_REELS]
	BOOL bReelStopped[NUM_REELS]
	BOOL bStopWheel
	#IF IS_DEBUG_BUILD
	TEXT_LABEL_23 tlTemp
	#ENDIF
//	FLOAT fStartRot
//	FLOAT fEndRot
//	FLOAT fResultWithSpin
	#IF IS_DEBUG_BUILD
	IF bPrintDebugOuput 
		PRINTLN("HANDLE_SLOT_MACHINE_OBJECT_INTERACTION:bPrintDebugOuput is set ")
	ENDIF
	#ENDIF
	
	REPEAT MAX_SLOT_MACHINES x
		IF IS_BIT_SET(NPCData.iUsedBS[GET_BITSET_ARRAY_INDEX(x)],GET_BITSET_ELEMENT_ID_WITH_ARRAY(x))
			//NPC using machine
		ELSE
			IF IS_BIT_SET(serverBD.iBS_MachineSpinning[GET_BITSET_ARRAY_INDEX(x)],GET_BITSET_ELEMENT_ID_WITH_ARRAY(x))
			OR 	(x = iCurrentlyUsedMachine AND IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS,PLAYER_BD_BS_SPINNING) AND NOT IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS,PLAYER_BD_BS_FINISHED_SPINNING))
				IF x = iCurrentlyUsedMachine
				AND IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS,PLAYER_BD_BS_FINISHED_SPINNING)
					PRINTLN("HANDLE_SLOT_MACHINE_OBJECT_INTERACTION: waiting for server to stop spinning reels, finished locally.")
				ELSE
					REPEAT NUM_REELS i
						bStopReel[i] = FALSE
						bReelStopped[i] = FALSE
						//fStartRot = 0
						//fEndRot = 0
						IF HAS_NET_TIMER_STARTED(serverBD.spinTimer[x])
						//AND NOT HAS_NET_TIMER_EXPIRED(serverBD.spinTimer[x],SLOTS_REEL_SPIN_TIME)
						
							IF HAS_NET_TIMER_EXPIRED(serverBD.spinTimer[x],(SLOTS_REEL_SPIN_TIME-3500)+(750*i))
								IF IS_BIT_SET(serverBD.iBS_HasResult[GET_BITSET_ARRAY_INDEX(x)],GET_BITSET_ELEMENT_ID_WITH_ARRAY(x))
								//AND localSlots[iCurrentlyUsedMachine].spinner[i].iCurrentPos = serverBD.iReelVirtSlotResults[x][i]
									IF i > 0
										IF bReelStopped[i-1]
											IF HAS_NET_TIMER_STARTED(stReelStopDelay[x][i-1])
												IF HAS_NET_TIMER_EXPIRED(stReelStopDelay[x][i-1],150)
													bStopReel[i] = TRUE
													#IF IS_DEBUG_BUILD
													IF bPrintDebugOuput 
														PRINTLN("HANDLE_SLOT_MACHINE_OBJECT_INTERACTION: 1 stopping reel #",i," on machine #",x)
													ENDIF
													#ENDIF
													IF localSlots[x].fPropReelTarget[i] = -1
														localSlots[x].fPropReelTarget[i] = serverBD.fReelPropSlotResults[x][i]*22.5 + FLOOR(localSlots[x].fPropReelRots[i]/360)*360
														#IF IS_DEBUG_BUILD
														IF bPrintDebugOuput 
															PRINTLN("HANDLE_SLOT_MACHINE_OBJECT_INTERACTION: setting target rotation  #",i," on machine #",x, " to: ",localSlots[x].fPropReelTarget[i])
														ENDIF
														#ENDIF
													ENDIF
												ENDIF
											ELSE
												START_NET_TIMER(stReelStopDelay[x][i-1])
											ENDIF
										ENDIF
									ELSE
										bStopReel[i] = TRUE	
										IF localSlots[x].fPropReelTarget[i] = -1
											localSlots[x].fPropReelTarget[i] = serverBD.fReelPropSlotResults[x][i]*22.5 + FLOOR(localSlots[x].fPropReelRots[i]/360)*360
											#IF IS_DEBUG_BUILD
											IF bPrintDebugOuput 
												PRINTLN("HANDLE_SLOT_MACHINE_OBJECT_INTERACTION: 2 setting target rotation  #",i," on machine #",x, " to: ",localSlots[x].fPropReelTarget[i])
											ENDIF
											#ENDIF
										ENDIF
										#IF IS_DEBUG_BUILD
										IF bPrintDebugOuput 
											PRINTLN("HANDLE_SLOT_MACHINE_OBJECT_INTERACTION: 2 stopping reel #",i," on machine #",x)
										ENDIF
										#ENDIF
									ENDIF
								ELSE
									#IF IS_DEBUG_BUILD
									IF bPrintDebugOuput 
										PRINTLN("HANDLE_SLOT_MACHINE_OBJECT_INTERACTION: waiting for server to have results set on machine #",x)
									ENDIF
									#ENDIF
								ENDIF
							ELSE
								#IF IS_DEBUG_BUILD
								IF bPrintDebugOuput 
									PRINTLN("HANDLE_SLOT_MACHINE_OBJECT_INTERACTION: waiting for timer to expire for slot machine #",x)
								ENDIF
								#ENDIF
							ENDIF
						ELSE
							#IF IS_DEBUG_BUILD
							IF bPrintDebugOuput 
								PRINTLN("HANDLE_SLOT_MACHINE_OBJECT_INTERACTION: waiting for server to start spin timer machine #",x)
							ENDIF
							#ENDIF	
						ENDIF
						IF DOES_ENTITY_EXIST(slotReel[x][i])
							IF NOT bStopReel[i]
								OVERRIDE_REEL_ROTATION_IF_NEEDED(x,i)
							ENDIF
							IF iCurrentlyUsedMachine = x
								IF NOT bStopReel[i]
									IF NOT IS_BIT_SET(iLocalBS, LOCAL_BS_SWAPPED_BLURRY_REELS1+i)
										vTemp = GET_ENTITY_COORDS(slotReel[x][i])
										vTemp.z = vTemp.z-10
										SET_ENTITY_COORDS(slotReel[x][i],vTemp)
										PRINTLN("CASINO_SLOTS: HANDLE_SLOT_MACHINE_OBJECT_INTERACTION:1 moving normal reel#",i," to coords: ",vTemp, " on machine #",x)
										
										IF DOES_ENTITY_EXIST(slotBlurryReel[i])
											vTemp = GET_ENTITY_COORDS(slotBlurryReel[i])
											vTemp.z = vTemp.z+10
											SET_ENTITY_COORDS(slotBlurryReel[i],vTemp)
											FORCE_ROOM_FOR_ENTITY(slotBlurryReel[i], GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()), GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID()))
											SET_BIT(iLocalBS, LOCAL_BS_SWAPPED_BLURRY_REELS1+i)
											PRINTLN("CASINO_SLOTS: HANDLE_SLOT_MACHINE_OBJECT_INTERACTION:1 moving blurry reel#",i," to coords: ",vTemp, " on machine #",x)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							GET_SLOT_MACHINE_POSITION(x,vTemp,fHeading)
							SET_ENTITY_ROTATION(slotReel[x][i],<<localSlots[x].fPropReelRots[i],0,fHeading>>)
							IF iCurrentlyUsedMachine = x
							AND DOES_ENTITY_EXIST(slotBlurryReel[i])
								SET_ENTITY_ROTATION(slotBlurryReel[i],<<localSlots[x].fPropReelRots[i],0,fHeading>>)
							ENDIF
							localSlots[x].fPropReelPrevRots[i] = localSlots[x].fPropReelRots[i]
						ENDIF
						//PRINTLN("DRAW_SLOT_MACHINE: 3 setting rotation of reel #",i," = ",localSlots[iCurrentlyUsedMachine].fPropReelRots[i])
						IF bStopReel[i]
							FLOAT fDif = (serverBD.fReelPropSlotResults[x][i]*22.5)- localSlots[x].fPropReelRots[i]%360
							IF ABSF(fDif) <= 20
							OR (localSlots[x].fPropReelTarget[i] != -1
							AND localSlots[x].fPropReelRots[i] >= localSlots[x].fPropReelTarget[i])
							//IF localSlots[x].fPropReelRots[i]%360 = serverBD.fReelPropSlotResults[x][i]*22.5
								//STOP
								localSlots[x].fPropReelRots[i] = serverBD.fReelPropSlotResults[x][i]*22.5
								IF iCurrentlyUsedMachine = x
									
									IF IS_BIT_SET(iLocalBS, LOCAL_BS_SWAPPED_BLURRY_REELS1+i)
										IF DOES_ENTITY_EXIST(slotReel[x][i])
										AND DOES_ENTITY_EXIST(slotBlurryReel[i])
											vTemp = GET_ENTITY_COORDS(slotReel[x][i])
											vTemp.z = vTemp.z+10
											SET_ENTITY_COORDS(slotReel[x][i],vTemp)
											FORCE_ROOM_FOR_ENTITY(slotReel[x][i], GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()), GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID()))
											PRINTLN("CASINO_SLOTS: HANDLE_SLOT_MACHINE_OBJECT_INTERACTION:2 moving normal reel#",i," to coords: ",vTemp, " on machine #",x)
											
											vTemp = GET_ENTITY_COORDS(slotBlurryReel[i])
											vTemp.z = vTemp.z-10
											SET_ENTITY_COORDS(slotBlurryReel[i],vTemp)
											CLEAR_BIT(iLocalBS, LOCAL_BS_SWAPPED_BLURRY_REELS1+i)
											PRINTLN("CASINO_SLOTS: HANDLE_SLOT_MACHINE_OBJECT_INTERACTION:2 moving blurry reel#",i," to coords: ",vTemp, " on machine #",x)
										ENDIF
										PLAY_SLOT_SOUND_FROM_PLAYER(PLAYER_ID(),localSlots[iCurrentlyUsedMachine].iMachineType,SLOT_SOUND_REEL_STOP)
										IF slotSetup.iVirtualReelContent[0][localResultOfSpin.iVirtualReelPos[0]] = SRS_SYMBOL6
											PLAY_SLOT_SOUND_FROM_PLAYER(PLAYER_ID(),localSlots[iCurrentlyUsedMachine].iMachineType,SLOT_SOUND_REEL_STOP_ON_PRIZE)
										ENDIF
									ENDIF
								ENDIF
								IF DOES_ENTITY_EXIST(slotReel[x][i])
									bReelStopped[i] = TRUE
									SET_ENTITY_ROTATION(slotReel[x][i],<<localSlots[x].fPropReelRots[i],0,fHeading>>)
									IF iCurrentlyUsedMachine = x
									AND DOES_ENTITY_EXIST(slotBlurryReel[i])
										SET_ENTITY_ROTATION(slotBlurryReel[i],<<localSlots[x].fPropReelRots[i],0,fHeading>>)
									ENDIF
								ENDIF
								#IF IS_DEBUG_BUILD
								IF bPrintDebugOuput 
									PRINTLN("HANDLE_SLOT_MACHINE_OBJECT_INTERACTION: 1 setting rotation of reel #",i," = ",localSlots[x].fPropReelRots[i])
									PRINTLN("HANDLE_SLOT_MACHINE_OBJECT_INTERACTION: reel # ",i," result pos = ",serverBD.fReelPropSlotResults[x][i])
									tlTemp = GET_SLOT_MACHINE_SYMBOL_NAME(slotSetup.iPropReelContent[i][FLOOR(serverBD.fReelPropSlotResults[x][i])])
									PRINTLN("HANDLE_SLOT_MACHINE_OBJECT_INTERACTION: reel # ",i," result symbol = ",tlTemp)
								ENDIF
								#ENDIF
							ELSE
								#IF IS_DEBUG_BUILD
								IF bOverrideSpinSpeed
									localSlots[x].fPropReelRots[i] = localSlots[x].fPropReelRots[i] +@ iReelSpinSpeed
								ELSE
								#ENDIF
								localSlots[x].fPropReelRots[i] = localSlots[x].fPropReelRots[i] +@ 600
								
								#IF IS_DEBUG_BUILD
								ENDIF
								#ENDIF
								#IF IS_DEBUG_BUILD
								IF bPrintDebugOuput 
									PRINTLN("HANDLE_SLOT_MACHINE_OBJECT_INTERACTION: SPINNING!! but want to stop reel # ",i," on machine # ",x," = ",localSlots[x].fPropReelRots[i])
								ENDIF
								#ENDIF
							ENDIF
						ELSE
							#IF IS_DEBUG_BUILD
							IF bOverrideSpinSpeed
								localSlots[x].fPropReelRots[i] = localSlots[x].fPropReelRots[i] +@ iReelSpinSpeed
							ELSE
							#ENDIF
							localSlots[x].fPropReelRots[i] = localSlots[x].fPropReelRots[i] +@ 600
							
							#IF IS_DEBUG_BUILD
							ENDIF
							#ENDIF
							#IF IS_DEBUG_BUILD
							IF bPrintDebugOuput 
								PRINTLN("HANDLE_SLOT_MACHINE_OBJECT_INTERACTION: SPINNING!! reel # ",i," on machine # ",x," = ",localSlots[x].fPropReelRots[i])
							ENDIF
							#ENDIF
						ENDIF
					ENDREPEAT
					
					IF  x = iCurrentlyUsedMachine
						IF NOT IS_BIT_SET(iBS_MachineSpinAudio[GET_BITSET_ARRAY_INDEX(x)],GET_BITSET_ELEMENT_ID_WITH_ARRAY(x))
							#IF IS_DEBUG_BUILD
							IF bPrintDebugOuput 
								PRINTLN("HANDLE_SLOT_MACHINE_OBJECT_INTERACTION: SPINNING!! playing sound on machine # ",x)
							ENDIF
							#ENDIF
							PLAY_SLOT_SOUND_FROM_PLAYER(PLAYER_ID(),localSlots[iCurrentlyUsedMachine].iMachineType,SLOT_SOUND_SPINNING)
							PLAY_SLOT_SOUND_FROM_PLAYER(PLAYER_ID(),localSlots[iCurrentlyUsedMachine].iMachineType,SLOT_SOUND_START_SPIN)
							SET_BIT(iBS_MachineSpinAudio[GET_BITSET_ARRAY_INDEX(x)],GET_BITSET_ELEMENT_ID_WITH_ARRAY(x))
						ENDIF
					ENDIF
					
					IF SHOULD_WHEEL_SPIN_ON_MACHINE(x,bReelStopped[2])
						IF NOT HAS_NET_TIMER_STARTED(stWheelStartDelay[x])
							START_NET_TIMER(stWheelStartDelay[x],TRUE)
						ELSE 
							IF HAS_NET_TIMER_EXPIRED(stWheelStartDelay[x],1000,TRUE) 
							AND NOT IS_BIT_SET(iLocalBS,LOCAL_BS_MOVED_VIEW_TO_WHEEL)
								IF iCurrentlyUsedMachine = x
									IF iCurrentAnim = ANIM_SLOTS_SPINNING_REELS
										iPayoutID = DETERMINE_PAYOUT_ID(slotSetup.iVirtualReelContent[0][localResultOfSpin.iVirtualReelPos[0]]
													,slotSetup.iVirtualReelContent[1][localResultOfSpin.iVirtualReelPos[1]]
													,slotSetup.iVirtualReelContent[2][localResultOfSpin.iVirtualReelPos[2]])
										DRAW_GAME_HELP(GAME_HELP_WIN_PRE_WHEEL,iPayoutID,localSlots[iCurrentlyUsedMachine].iLastWin,-1)
										START_NETWORK_SYNCED_SCENE_FOR_SLOTS(ANIM_SLOTS_WIN_TO_WHEEL)
									ENDIF
									IF NOT IS_BIT_SET(iLocalBS,LOCAL_BS_MOVED_VIEW_TO_WHEEL)
		//								SET_SLOT_CAMERA_ACTIVE(CAM_SLOTS_FACE_MACHINE_WHEEL)
										SET_BIT(iLocalBS,LOCAL_BS_MOVED_VIEW_TO_WHEEL)
									ENDIF
								ENDIF
							ELIF HAS_NET_TIMER_EXPIRED(stWheelStartDelay[x],1250,TRUE) 
								IF NOT HAS_NET_TIMER_EXPIRED(serverBD.spinTimer[x],GET_ACTIVE_MACHINE_SPIN_TIME(x)) 
									IF HAS_NET_TIMER_EXPIRED(serverBD.spinTimer[x],GET_ACTIVE_MACHINE_SPIN_TIME(x)-2000)
										bStopWheel = TRUE
										IF localSlots[x].fPropPrizeWheelTarget = -1
											localSlots[x].fPropPrizeWheelTarget = TO_FLOAT(serverBD.iPrizeWheelResult[x]*SLOTS_WHEEL_SEGMENT_DEG_SIZE) + FLOOR(localSlots[x].fPropPrizeWheelRot/360)*360
											#IF IS_DEBUG_BUILD
											IF bPrintDebugOuput 
												PRINTLN("HANDLE_SLOT_MACHINE_OBJECT_INTERACTION: setting target rotation on wheel on machine #",x, " to: ",localSlots[x].fPropPrizeWheelTarget)
											ENDIF
											#ENDIF
										ENDIF
									ENDIF
									IF DOES_ENTITY_EXIST(slotPrizeWheel[x])
									
										IF iCurrentlyUsedMachine = x
											IF NOT bStopWheel
												IF NOT IS_BIT_SET(iLocalBS, LOCAL_BS_SWAPPED_BLURRY_WHEEL)
													vTemp = GET_ENTITY_COORDS(slotPrizeWheel[x])
													vTemp.z = vTemp.z-10
													SET_ENTITY_COORDS(slotPrizeWheel[x],vTemp)
													PRINTLN("CASINO_SLOTS: HANDLE_SLOT_MACHINE_OBJECT_INTERACTION:1 moving normal prize wheel to coords: ",vTemp)
													IF DOES_ENTITY_EXIST(slotBlurryWheel)
														vTemp = GET_ENTITY_COORDS(slotBlurryWheel)
														vTemp.z = vTemp.z+10
														SET_ENTITY_COORDS(slotBlurryWheel,vTemp)
														FORCE_ROOM_FOR_ENTITY(slotBlurryWheel, GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()), GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID()))
														SET_BIT(iLocalBS, LOCAL_BS_SWAPPED_BLURRY_WHEEL)
														PRINTLN("CASINO_SLOTS: HANDLE_SLOT_MACHINE_OBJECT_INTERACTION:1 moving blurry prize wheel to coords: ",vTemp)
													ENDIF
													PLAY_SLOT_SOUND_FROM_PLAYER(PLAYER_ID(),localSlots[iCurrentlyUsedMachine].iMachineType,SLOT_SOUND_SPIN_WHEEL)
												ENDIF
											ENDIF
											
										ENDIF
										GET_SLOT_MACHINE_POSITION(x,vTemp,fHeading)
										SET_ENTITY_ROTATION(slotPrizeWheel[x],<<0,localSlots[x].fPropPrizeWheelRot,fHeading>>)
										IF iCurrentlyUsedMachine = x
										AND DOES_ENTITY_EXIST(slotBlurryWheel)
											SET_ENTITY_ROTATION(slotBlurryWheel,<<0,localSlots[x].fPropPrizeWheelRot,fHeading>>)
										ENDIF
									ENDIF	
									IF bStopWheel
										FLOAT fDif = (TO_FLOAT(serverBD.iPrizeWheelResult[x]*SLOTS_WHEEL_SEGMENT_DEG_SIZE))- localSlots[x].fPropPrizeWheelRot%360
										IF ABSF(fDif) <= 20
										OR (localSlots[x].fPropPrizeWheelTarget  != -1
										AND localSlots[x].fPropPrizeWheelRot >= localSlots[x].fPropPrizeWheelTarget)
											//STOP
											localSlots[x].fPropPrizeWheelRot = TO_FLOAT(serverBD.iPrizeWheelResult[x]*SLOTS_WHEEL_SEGMENT_DEG_SIZE)
											//PRINTLN("DRAW_SLOT_MACHINE: stopping the wheel at ",localSlots[x].fPropPrizeWheelRot)
											IF iCurrentlyUsedMachine = x
												IF IS_BIT_SET(iLocalBS, LOCAL_BS_SWAPPED_BLURRY_WHEEL)
													IF DOES_ENTITY_EXIST(slotPrizeWheel[x])
													AND DOES_ENTITY_EXIST(slotBlurryWheel)
														vTemp = GET_ENTITY_COORDS(slotPrizeWheel[x])
														vTemp.z = vTemp.z+10
														SET_ENTITY_COORDS(slotPrizeWheel[x],vTemp)
														FORCE_ROOM_FOR_ENTITY(slotPrizeWheel[x], GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()), GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID()))
														PRINTLN("CASINO_SLOTS: HANDLE_SLOT_MACHINE_OBJECT_INTERACTION:2 moving normal prize wheel to coords: ",vTemp)
														
														vTemp = GET_ENTITY_COORDS(slotBlurryWheel)
														vTemp.z = vTemp.z-10
														SET_ENTITY_COORDS(slotBlurryWheel,vTemp)
														CLEAR_BIT(iLocalBS, LOCAL_BS_SWAPPED_BLURRY_WHEEL)
														PRINTLN("CASINO_SLOTS: HANDLE_SLOT_MACHINE_OBJECT_INTERACTION:2 moving blurry prize wheel to coords: ",vTemp)
													ENDIF
												ENDIF
												IF DOES_ENTITY_EXIST(slotPrizeWheel[x])
													SET_ENTITY_ROTATION(slotPrizeWheel[x],<<0,localSlots[x].fPropPrizeWheelRot,fHeading>>)
												ENDIF
												IF iCurrentlyUsedMachine = x
												AND DOES_ENTITY_EXIST(slotBlurryWheel)
													SET_ENTITY_ROTATION(slotBlurryWheel,<<0,localSlots[x].fPropPrizeWheelRot,fHeading>>)
												ENDIF
												PLAY_SLOT_SOUND_FROM_PLAYER(PLAYER_ID(),localSlots[iCurrentlyUsedMachine].iMachineType,SLOT_SOUND_WHEEL_WIN)
												SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS,PLAYER_BD_BS_FINISHED_SPINNING)
												PRINTLN("CASINO_SLOTS: HANDLE_SLOT_MACHINE_OBJECT_INTERACTION: finished spinning reels and wheel locally -4 fdif = ",fDif )
											ENDIF
										ELSE
											#IF IS_DEBUG_BUILD
											IF bOverrideSpinSpeed
												localSlots[x].fPropPrizeWheelRot = localSlots[x].fPropPrizeWheelRot +@ iWheelSpinSpeed
											ELSE
											#ENDIF
											
											localSlots[x].fPropPrizeWheelRot = localSlots[x].fPropPrizeWheelRot +@ 300
											
											#IF IS_DEBUG_BUILD
											ENDIF
											#ENDIF
											
											#IF IS_DEBUG_BUILD
											IF bPrintDebugOuput 
												PRINTLN("HANDLE_SLOT_MACHINE_OBJECT_INTERACTION: Stopping wheel on machine # ",x," = ",localSlots[x].fPropPrizeWheelRot)
											ENDIF
											#ENDIF
										ENDIF
									ELSE
										#IF IS_DEBUG_BUILD
										IF bOverrideSpinSpeed
											localSlots[x].fPropPrizeWheelRot = localSlots[x].fPropPrizeWheelRot +@ iWheelSpinSpeed
										ELSE
										#ENDIF
										
										localSlots[x].fPropPrizeWheelRot = localSlots[x].fPropPrizeWheelRot +@ 300
										
										#IF IS_DEBUG_BUILD
										ENDIF
										#ENDIF
										
										#IF IS_DEBUG_BUILD
										IF bPrintDebugOuput 
											PRINTLN("HANDLE_SLOT_MACHINE_OBJECT_INTERACTION: SPINNING!! wheel on machine # ",x," = ",localSlots[x].fPropPrizeWheelRot)
										ENDIF
										#ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						
						IF iCurrentlyUsedMachine = x
						AND bReelStopped[2]
							IF DOES_SLOT_MACHINE_HAVE_WHEEL(x,localSlots)
								IF slotSetup.iVirtualReelContent[0][serverBD.iReelVirtSlotResults[x][0]] = SRS_WILD
								AND slotSetup.iVirtualReelContent[1][serverBD.iReelVirtSlotResults[x][1]]= SRS_WILD
								AND slotSetup.iVirtualReelContent[2][serverBD.iReelVirtSlotResults[x][2]] = SRS_WILD
								
								ELSE
									SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS,PLAYER_BD_BS_FINISHED_SPINNING)
									PRINTLN("CASINO_SLOTS: HANDLE_SLOT_MACHINE_OBJECT_INTERACTION: finished spinning reels locally -2")
								ENDIF
							ELSE
								SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS,PLAYER_BD_BS_FINISHED_SPINNING)
								PRINTLN("CASINO_SLOTS: HANDLE_SLOT_MACHINE_OBJECT_INTERACTION: finished spinning reels locally -1")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				CLEAR_BIT(iBS_MachineSpinAudio[GET_BITSET_ARRAY_INDEX(x)],GET_BITSET_ELEMENT_ID_WITH_ARRAY(x))
				localSlots[x].fPropPrizeWheelTarget = -1
				REPEAT NUM_REELS i
					localSlots[x].fPropReelTarget[i] = -1
					IF DOES_ENTITY_EXIST(slotReel[x][i])
						IF localSlots[x].fPropReelRots[i] != serverBD.fReelPropSlotResults[x][i]*22.5
							#IF IS_DEBUG_BUILD
							IF bPrintDebugOuput 
								PRINTLN("CASINO_SLOTS: HANDLE_SLOT_MACHINE_OBJECT_INTERACTION: not spinning on host force stopping Machine #",x)
							ENDIF
							#ENDIF
							GET_SLOT_MACHINE_POSITION(x,vTemp,fHeading)
							localSlots[x].fPropReelRots[i] = serverBD.fReelPropSlotResults[x][i]*22.5
							SET_ENTITY_ROTATION(slotReel[x][i],<<localSlots[x].fPropReelRots[i],0,fHeading>>)
						ENDIF
					ELSE
						localSlots[x].fPropReelRots[i] = 0
					ENDIF
				ENDREPEAT
				IF DOES_ENTITY_EXIST(slotPrizeWheel[x])
					IF localSlots[x].fPropPrizeWheelRot != TO_FLOAT(serverBD.iPrizeWheelResult[x]*SLOTS_WHEEL_SEGMENT_DEG_SIZE)
						GET_SLOT_MACHINE_POSITION(x,vTemp,fHeading)
						localSlots[x].fPropPrizeWheelRot = TO_FLOAT(serverBD.iPrizeWheelResult[x]*SLOTS_WHEEL_SEGMENT_DEG_SIZE)
						SET_ENTITY_ROTATION(slotPrizeWheel[x],<<0,localSlots[x].fPropPrizeWheelRot,fHeading>>)
					ENDIF
				ELSE
					localSlots[x].fPropPrizeWheelRot = 0
				ENDIF

				IF iCurrentlyUsedMachine = x
					IF IS_BIT_SET(iLocalBS,LOCAL_BS_MOVED_VIEW_TO_WHEEL)
	//					SET_SLOT_CAMERA_ACTIVE(CAM_SLOTS_FACE_MACHINE)
						CLEAR_BIT(iLocalBS,LOCAL_BS_MOVED_VIEW_TO_WHEEL)
					ENDIF
					#IF IS_DEBUG_BUILD
					IF bPrintDebugOuput 
						IF IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS,PLAYER_BD_BS_SPINNING)
							PRINTLN("CASINO_SLOTS: HANDLE_SLOT_MACHINE_OBJECT_INTERACTION: player spinning but machine is stopped??")
							PRINTLN("CASINO_SLOTS: HANDLE_SLOT_MACHINE_OBJECT_INTERACTION: server spinning BS ",IS_BIT_SET(serverBD.iBS_MachineSpinning[GET_BITSET_ARRAY_INDEX(x)],GET_BITSET_ELEMENT_ID_WITH_ARRAY(x)))
							PRINTLN("CASINO_SLOTS: HANDLE_SLOT_MACHINE_OBJECT_INTERACTION: PLAYER_BD_BS_FINISHED_SPINNING ",IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS,PLAYER_BD_BS_FINISHED_SPINNING))
						ENDIF
					ENDIF
					#ENDIF

					IF IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS,PLAYER_BD_BS_FINISHED_SPINNING)
					AND NOT IS_BIT_SET(iLocalBS,LOCAL_BS_BET_TRANS_STARTED)
						RESET_NET_TIMER(stReelStopDelay[x][0])
						RESET_NET_TIMER(stReelStopDelay[x][1])
						RESET_NET_TIMER(stWheelStartDelay[x])
						CLEAR_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS,PLAYER_BD_BS_FINISHED_SPINNING)
						CLEAR_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS,PLAYER_BD_BS_SPINNING)
						CLEAR_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS,PLAYER_BD_BS_PROCESSED_RESULT)
						CLEAR_BIT(iLocalBS,LOCAL_BS_SETUP_CONTROLS)
						SET_BIT(iLocalBS2,LOCAL_BS2_FORCE_CONTROL_UPDATE_AFTER_SPIN)
						PRINTLN("CASINO_SLOTS: HANDLE_SLOT_MACHINE_OBJECT_INTERACTION: resetting server finished spin and done win anim")
					ENDIF
					
					
					
					IF IS_BIT_SET(iLocalBS, LOCAL_BS_SWAPPED_BLURRY_WHEEL)
						IF DOES_ENTITY_EXIST(slotPrizeWheel[x])
							vTemp = GET_ENTITY_COORDS(slotPrizeWheel[x])
							vTemp.z = vTemp.z+10
							SET_ENTITY_COORDS(slotPrizeWheel[x],vTemp)
							FORCE_ROOM_FOR_ENTITY(slotPrizeWheel[x], GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()), GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID()))
							PRINTLN("CASINO_SLOTS: HANDLE_SLOT_MACHINE_OBJECT_INTERACTION:3 moving normal prize wheel to coords: ",vTemp)
							
							vTemp = GET_ENTITY_COORDS(slotBlurryWheel)
							vTemp.z = vTemp.z-10
							SET_ENTITY_COORDS(slotBlurryWheel,vTemp)
							PRINTLN("CASINO_SLOTS: HANDLE_SLOT_MACHINE_OBJECT_INTERACTION:3 moving blurry prize wheel to coords: ",vTemp)
						ENDIF
						CLEAR_BIT(iLocalBS, LOCAL_BS_SWAPPED_BLURRY_WHEEL)
					ENDIF
					REPEAT NUM_REELS i
						IF IS_BIT_SET(iLocalBS, LOCAL_BS_SWAPPED_BLURRY_REELS1+i)
						
							IF DOES_ENTITY_EXIST(slotReel[x][i])
								vTemp = GET_ENTITY_COORDS(slotReel[x][i])
								vTemp.z = vTemp.z+10
								SET_ENTITY_COORDS(slotReel[x][i],vTemp)
								FORCE_ROOM_FOR_ENTITY(slotReel[x][i], GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()), GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID()))
								PRINTLN("CASINO_SLOTS: HANDLE_SLOT_MACHINE_OBJECT_INTERACTION:3 moving normal reel#",i," to coords: ",vTemp)
								
								vTemp = GET_ENTITY_COORDS(slotBlurryReel[i])
								vTemp.z = vTemp.z-10
								SET_ENTITY_COORDS(slotBlurryReel[i],vTemp)
								PRINTLN("CASINO_SLOTS: HANDLE_SLOT_MACHINE_OBJECT_INTERACTION:3 moving blurry reel#",i," to coords: ",vTemp)
							ENDIF
							CLEAR_BIT(iLocalBS, LOCAL_BS_SWAPPED_BLURRY_REELS1+i)
						ENDIF
					ENDREPEAT
				ELSE
					RESET_NET_TIMER(stReelStopDelay[x][0])
					RESET_NET_TIMER(stReelStopDelay[x][1])
					RESET_NET_TIMER(stWheelStartDelay[x])
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC HANDLE_SLOT_MACHINE_SOUNDS()
	//to be added
ENDPROC

PROC HANDLE_LOCAL_SLOT_MACHINES()
	HANDLE_SLOT_MACHINE_ATTRACT_SOUNDS()
	HANDLE_SLOT_MACHINE_UI()
	HANDLE_SLOT_MACHINE_OBJECT_CREATION()
	HANDLE_SLOT_MACHINE_OBJECT_INTERACTION()
	HANDLE_NPC_SLOT_MACHINE_OBJECT_INTERACTION()
	HANDLE_SLOT_MACHINE_SOUNDS()
ENDPROC



PROC REQUEST_MACHINE_FROM_SERVER(INT iMachine)
	playerBD[NATIVE_TO_INT(PLAYER_ID())].iCurrentMachineID = iMachine
	IF serverBD.iMachineUsedBy[iMachine] != -1
		IF serverBD.iMachineUsedBy[iMachine] = NATIVE_TO_INT(PLAYER_ID())
			SET_CURRENTLY_USED_MACHINE(iMachine)
			IF DOES_SLOT_MACHINE_HAVE_WHEEL(iMachine,localSlots)
			AND IS_BIT_SET(iLocalBS2,LOCAL_BS2_ACCEPTED_DISCLOSURE2)
				SET_CLIENT_STATE(LOCAL_STATE_ENTER_MACHINE)
			ELIF NOT DOES_SLOT_MACHINE_HAVE_WHEEL(iMachine,localSlots)
			AND IS_BIT_SET(iLocalBS,LOCAL_BS_ACCEPTED_DISCLOSURE)
				SET_CLIENT_STATE(LOCAL_STATE_ENTER_MACHINE)
			ELSE
				SET_CLIENT_STATE(LOCAL_STATE_DISCLOSURE)
			ENDIF
		ELSE
			IF NOT IS_BIT_SET(iLocalBS,LOCAL_BS_MACHINE_IN_USE_HELP)
			AND NOT SHOULD_ALLOW_CASINO_ENTRY_DURING_MISSION()
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					PRINT_HELP("SLOTS_USED")
					SET_BIT(iLocalBS,LOCAL_BS_MACHINE_IN_USE_HELP)
				ENDIF
			ENDIF
			iTriggeringMachine = -1	
			SET_CURRENTLY_USED_MACHINE(-1)
			IF IS_BIT_SET(iLocalBS,LOCAL_BS_TOOK_AWAY_CONTROL)
				IF IS_NET_PLAYER_OK(PLAYER_ID())
				AND NOT IS_PLAYER_SWITCH_IN_PROGRESS()
					NET_SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
					
				ENDIF
				IF IS_RADAR_HIDDEN()
					DISPLAY_RADAR(TRUE)
				ENDIF
				SET_DISABLE_RANK_UP_MESSAGE(FALSE) 
				CLEAR_BIT(iLocalBS,LOCAL_BS_TOOK_AWAY_CONTROL)
			ENDIF
			SET_CLIENT_STATE(LOCAL_STATE_CHECK_FOR_LOCATE)
		ENDIF
	ENDIF	
ENDPROC

FUNC BOOL ENTER_SLOT_MACHINE()
	//VECTOR vCoords
	INT iLocalScene
	TEXT_LABEL_23 tempText
	INT i
	// bRightEnter
	//VECTOR vMachinePos, vMachineRot
	//FLOAT fDistance
//	BOOL bPlayerFinishedEntry
	SWITCH iEnterSeatStage
		CASE ENTER_SEAT_STAGE_WALK_TO
			IF LOAD_ANIM_DICTS()
				IF LOAD_SLOT_MACHINE_UI()
					SET_SLOTS_SCALEFORM_BET_AMOUNT(localSlots[iCurrentlyUsedMachine].iMachineCost*iPayoutMultipler)
					SET_SLOTS_SCALEFORM_LAST_WIN(0)
					tempText = GET_SLOT_MESSAGE_RANDOM(localSlots[iCurrentlyUsedMachine].iMachineType,TRUE)
					SET_SLOTS_SCALEFORM_MESSAGE(tempText)
					SET_SLOTS_SCALEFORM_THEME(localSlots[iCurrentlyUsedMachine].iMachineType)
					GET_SLOTS_ANIM_POSITION_AND_ROTATION(iCurrentlyUsedMachine, vCurrentAnimLoc, vCurrentAnimRot)
					
					iEntryAnim = GET_PLAYER_ENTRY_ANIMATION()
					//vPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(slotMachine[iCurrentlyUsedMachine], << -0.026723, -0.846029, 0.989902 >>)
					
					
					
					CLEAR_BIT(iLocalBS,LOCAL_BS_SKIP_WALK_TO_POS)
					//fDistance = GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(PLAYER_ID()),GET_PLAYER_ENTER_POSITION(iEntryAnim),FALSE)
//					IF GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(PLAYER_ID()),GET_PLAYER_ENTER_POSITION(iEntryAnim),FALSE) <= 0.25
//						SET_BIT(iLocalBS,LOCAL_BS_SKIP_WALK_TO_POS)
//						PRINTLN("ENTER_SLOT_MACHINE: skipping straight to animation - 1")
//					ELSE
						//PRINTLN("ENTER_SLOT_MACHINE: distance on entry is: ",fDistance)
						SET_BIT(iLocalBS,LOCAL_BS_SKIP_WALK_TO_POS)
						TASK_FOLLOW_NAV_MESH_TO_COORD(PLAYER_PED_ID(),GET_PLAYER_ENTER_POSITION(iEntryAnim)
													,PEDMOVEBLENDRATIO_WALK,7000,0.05,DEFAULT,GET_PLAYER_ENTER_HEADING(iEntryAnim))
//					ENDIF
//					TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(),GET_PLAYER_ENTER_POSITION(IS_BIT_SET(iLocalBS,LOCAL_BS_ENTRY_ON_RIGHT)) , PEDMOVEBLENDRATIO_WALK, DEFAULT, GET_PLAYER_ENTER_HEADING(IS_BIT_SET(iLocalBS,LOCAL_BS_ENTRY_ON_RIGHT)), 0.05)
//					SET_SLOT_CAMERA_ACTIVE(CAM_SLOTS_ENTER)
					CLEAR_BIT(iLocalBS,LOCAL_BS_BYPASS_NORMAL_ENTRY)
					CLEAR_BIT(iLocalBS,LOCAL_BS_CAN_BYPASS_NORMAL_ENTRY)
					iEnterSeatStage++
				ENDIF
			ENDIF
		BREAK
		CASE ENTER_SEAT_STAGE_SIT_ANIM
			//fDistance = GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(PLAYER_ID()),GET_PLAYER_ENTER_POSITION(iEntryAnim),FALSE)
			IF GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(PLAYER_ID()),GET_PLAYER_ENTER_POSITION(iEntryAnim),FALSE) <= 0.25
				SET_BIT(iLocalBS,LOCAL_BS_SKIP_WALK_TO_POS)
				PRINTLN("ENTER_SLOT_MACHINE: skipping straight to animation - 2")
			ENDIF
			//PRINTLN("ENTER_SLOT_MACHINE: distance entering is: ",fDistance)
			IF (NOT IS_PED_INJURED(PLAYER_PED_ID())
			AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) <> PERFORMING_TASK
			AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) <> WAITING_TO_START_TASK
			AND NOT IS_PED_WALKING(PLAYER_PED_ID()))
			OR (IS_BIT_SET(iLocalBS,LOCAL_BS_SKIP_WALK_TO_POS) AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) <> WAITING_TO_START_TASK)
				IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_THIRD_PERSON_MEDIUM
				OR GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_THIRD_PERSON_FAR
					SET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT, CAM_VIEW_MODE_THIRD_PERSON_NEAR)
				ENDIF
				IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
					SET_TABLE_GAMES_CAMERA_THIS_UPDATE(TABLE_GAMES_CAMERA_TYPE_SLOT_MACHINE)
				ENDIF
				START_NETWORK_SYNCED_SCENE_FOR_SLOTS(iEntryAnim)
				iEnterSeatStage++
			ENDIF
		BREAK
		CASE ENTER_SEAT_STAGE_ANIM_PLAYING
			iLocalScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(iAnimSyncedScene)
			//IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
				SET_TABLE_GAMES_CAMERA_THIS_UPDATE(TABLE_GAMES_CAMERA_TYPE_SLOT_MACHINE)
			//ENDIF
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalScene)
				IF HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(),HASH("CAN_BREAKOUT_FOR_BETTING"))
					PRINTLN("ENTER_SLOT_MACHINE: player can breakout for betting")
					SET_BIT(iLocalBS,LOCAL_BS_CAN_BYPASS_NORMAL_ENTRY)
					HANDLE_PLAYING_SLOT_MACHINE()
					IF IS_BIT_SET(iLocalBS,LOCAL_BS_BYPASS_NORMAL_ENTRY)
						PRINTLN("ENTER_SLOT_MACHINE: skipping entry for input")
						CLEAR_BIT(iLocalBS,LOCAL_BS_BYPASS_NORMAL_ENTRY)
						CLEAR_BIT(iLocalBS,LOCAL_BS_CAN_BYPASS_NORMAL_ENTRY)
						RETURN TRUE
					ENDIF
				ENDIF
				IF GET_SYNCHRONIZED_SCENE_PHASE(iLocalScene) >= 0.99
					REPEAT MAX_SLOT_ATTRACT_SOUNDS i
						IF slotsAttract[i].iSoundID != -1
							IF serverBD.iMachineUsedBy[slotsAttract[i].iMachine] = NATIVE_TO_INT(PLAYER_ID())
								STOP_SOUND(slotsAttract[i].iSoundID)
								RELEASE_SOUND_ID(slotsAttract[i].iSoundID)
								slotsAttract[i].iSoundID = -1
								PRINTLN("HANDLE_SLOT_MACHINE_ATTRACT_SOUNDS: clearing attract sound for PLAYER USED machine #",slotsAttract[i].iMachine)
							ENDIF
						ENDIF
					ENDREPEAT
					PLAY_SLOT_SOUND_FROM_PLAYER(PLAYER_ID(),localSlots[iCurrentlyUsedMachine].iMachineType,SLOT_SOUND_WELCOME)
				//OR HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(),HASH("CAN_BREAKOUT_FOR_BETTING"))
					START_NETWORK_SYNCED_SCENE_FOR_SLOTS(ANIM_SLOTS_BASE)
					
					//SET_SLOT_CAMERA_ACTIVE(CAM_SLOTS_FACE_MACHINE)
					iEnterSeatStage++
				ENDIF
			ENDIF
			
		BREAK
		CASE ENTER_SEAT_STAGE_FINISHED
			//IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
				SET_TABLE_GAMES_CAMERA_THIS_UPDATE(TABLE_GAMES_CAMERA_TYPE_SLOT_MACHINE)
			//ENDIF
			CLEAR_BIT(iLocalBS,LOCAL_BS_BYPASS_NORMAL_ENTRY)
			CLEAR_BIT(iLocalBS,LOCAL_BS_CAN_BYPASS_NORMAL_ENTRY)
			RETURN TRUE
		BREAK
	ENDSWITCH

	RETURN FALSE
ENDFUNC

PROC HANDLE_DISCLOSURE_PROMPT()
	TEXT_LABEL_23 theText = GET_MACHINE_UI_HEADER(localSlots[iCurrentlyUsedMachine].iMachineType)
	PRINTLN("HANDLE_DISCLOSURE_PROMPT: loading texture dictionary: ",theText)
	REQUEST_STREAMED_TEXTURE_DICT(theText)
	SET_BIT(iLocalBS,LOCAL_BS_LOADED_MENU_HEADER)
	IF LOAD_MENU_ASSETS()
	AND HAS_STREAMED_TEXTURE_DICT_LOADED(theText)
		IF NOT IS_BIT_SET(iLocalBS, LOCAL_BS_LOADED_DISCLOSURE)
			
			CLEAR_MENU_DATA()
			SET_MENU_USES_HEADER_GRAPHIC(TRUE, theText, theText)
			TEXT_LABEL_23 theTitle = "SLOTS_TITLE"
			theTitle += localSlots[iCurrentlyUsedMachine].iMachineType
			SET_MENU_TITLE(theTitle)

			SET_CURRENT_MENU_ITEM(-1)

			SET_CURRENT_MENU_ITEM_DESCRIPTION("SLOTS_DIS")
			theTitle = "SLOTS_DIS1"
			theTitle += localSlots[iCurrentlyUsedMachine].iMachineType
			ADD_CURRENT_MENU_ITEM_DESCRIPTION_STRING(theTitle)
			ADD_CURRENT_MENU_ITEM_DESCRIPTION_STRING("SLOTS_DIS2")
			ADD_CURRENT_MENU_ITEM_DESCRIPTION_STRING("SLOTS_DIS3")
			ADD_MENU_HELP_KEY_INPUT(INPUT_FRONTEND_CANCEL,"SLOTS_EXIT")
			ADD_MENU_HELP_KEY_INPUT(INPUT_FRONTEND_ACCEPT,"SLOTS_ACC") //"IB_CIN_EXIT"
			SET_BIT(iLocalBS, LOCAL_BS_LOADED_DISCLOSURE)
			PLAY_SOUND_FRONTEND(-1, "DLC_VW_RULES", "dlc_vw_table_games_frontend_sounds")
		ENDIF
		SET_INSTRUCTIONAL_BUTTONS_UNDER_HUD_THIS_FRAME()
		DRAW_MENU()
		IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL,INPUT_FRONTEND_CANCEL)
		OR IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL,INPUT_FRONTEND_CANCEL)
			iTriggeringMachine = -1	
			SET_CURRENTLY_USED_MACHINE(-1)
			IF IS_BIT_SET(iLocalBS,LOCAL_BS_TOOK_AWAY_CONTROL)
				IF IS_NET_PLAYER_OK(PLAYER_ID())
				AND NOT IS_PLAYER_SWITCH_IN_PROGRESS()
				AND IS_SKYSWOOP_AT_GROUND()
					NET_SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
				ENDIF
				IF IS_RADAR_HIDDEN()
					DISPLAY_RADAR(TRUE)
				ENDIF
				SET_DISABLE_RANK_UP_MESSAGE(FALSE)
				CLEAR_BIT(iLocalBS,LOCAL_BS_TOOK_AWAY_CONTROL)
			ENDIF
			CLEAR_MENU_DATA()
			CLEANUP_MENU_ASSETS()
     		SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(theText)
			CLEAR_BIT(iLocalBS,LOCAL_BS_LOADED_MENU_HEADER)
			CLEAR_BIT(iLocalBS,LOCAL_BS_LOADED_DISCLOSURE)
			PLAY_SOUND_FRONTEND(-1, "DLC_VW_CONTINUE", "dlc_vw_table_games_frontend_sounds")
			SET_CLIENT_STATE(LOCAL_STATE_CHECK_FOR_LOCATE)
		ENDIF
		IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL,INPUT_FRONTEND_ACCEPT)
		OR IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL,INPUT_FRONTEND_ACCEPT)
			CLEAR_MENU_DATA()
			CLEANUP_MENU_ASSETS()
     		SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(theText)
			CLEAR_BIT(iLocalBS, LOCAL_BS_LOADED_DISCLOSURE)
			CLEAR_BIT(iLocalBS,LOCAL_BS_LOADED_MENU_HEADER)
			IF DOES_SLOT_MACHINE_HAVE_WHEEL(iCurrentlyUsedMachine,localSlots)
				SET_BIT(iLocalBS2,LOCAL_BS2_ACCEPTED_DISCLOSURE2)
			ELSE
				SET_BIT(iLocalBS,LOCAL_BS_ACCEPTED_DISCLOSURE)
			ENDIF
			PLAY_SOUND_FRONTEND(-1, "DLC_VW_CONTINUE", "dlc_vw_table_games_frontend_sounds")
			SET_CLIENT_STATE(LOCAL_STATE_ENTER_MACHINE)
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_CLIENT()
	HANDLE_LOCAL_SLOT_MACHINES()
	
	IF iClientState > LOCAL_STATE_CHECK_FOR_LOCATE
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE)
		SET_INPUT_EXCLUSIVE(PLAYER_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
		SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
		SET_INPUT_EXCLUSIVE(PLAYER_CONTROL, INPUT_FRONTEND_PAUSE)
		SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE)
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_RS)
		SET_INPUT_EXCLUSIVE(PLAYER_CONTROL, INPUT_FRONTEND_RS)
		SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_RS)
	ENDIF
	IF HAS_NET_TIMER_STARTED(stExitCapsuleOverride)
		IF NOT HAS_NET_TIMER_EXPIRED(stExitCapsuleOverride,5000,TRUE)
			SET_PED_CAPSULE(PLAYER_PED_ID(), 0.2)
			PRINTLN("CDM: Overriding capsule size")
		ENDIF
	ENDIF
	SWITCH iClientState
		CASE LOCAL_STATE_CHECK_FOR_LOCATE
			IF IS_BIT_SET(iLocalBS,LOCAL_BS_REQUESTED_ANIMS)
				REMOVE_ANIM_DICT(GET_PLAYER_ANIM_DICT())
				CLEAR_BIT(iLocalBS,LOCAL_BS_REQUESTED_ANIMS)
			ENDIF
			IF CHECK_FOR_TRIGGER_ENTERING_MACHINE()
				iEnterSeatStage = 0
				CLEAR_BIT(iLocalBS,LOCAL_BS_VIEWING_HELP_PAGE1)
				CLEAR_BIT(iLocalBS,LOCAL_BS_VIEWING_HELP_PAGE2)
				CLEAR_BIT(iLocalBS,LOCAL_BS_BYPASS_NORMAL_ENTRY)
				CLEAR_BIT(iLocalBS,LOCAL_BS_CAN_BYPASS_NORMAL_ENTRY)
				CLEAR_BIT(iLocalBS,LOCAL_BS_VIEWING_HELP_PAGE1)
				CLEAR_BIT(iLocalBS,LOCAL_BS_VIEWING_HELP_PAGE2)
				SET_CLIENT_STATE(LOCAL_STATE_REQUEST_MACHINE)
			ENDIF
		BREAK
		CASE LOCAL_STATE_REQUEST_MACHINE
//			RESET_NET_TIMER(stIdleTimeout)
			REQUEST_MACHINE_FROM_SERVER(iTriggeringMachine)
			machineUI.si_SlotsLocalMovie = REQUEST_SCALEFORM_MOVIE("SLOT_MACHINE")
		BREAK
		CASE LOCAL_STATE_DISCLOSURE
			HANDLE_DISCLOSURE_PROMPT()
		BREAK
		CASE LOCAL_STATE_MEMBERSHIP_PURCHASE
			IF NOT IS_CASINO_MEMBERSHIP_TRANSACTION_ONGOING()
				SET_CLIENT_STATE(LOCAL_STATE_CHECK_FOR_LOCATE)
			ENDIF
		BREAK
		CASE LOCAL_STATE_ENTER_MACHINE
			RESET_NET_TIMER(slotMetrics.stLightTimer)
			bSetMetricTimerHeavy = FALSE
			bSetMetricTimerLight = FALSE
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_NEXT_CAMERA)
			DISABLE_CELLPHONE_THIS_FRAME_ONLY()
			HIDE_SCRIPTED_HUD_COMPONENT_THIS_FRAME(HUD_MP_RANK_BAR) 
			SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSFive, BS_SIMPLE_INTERIOR_GLOBAL_BS_FIVE_PLAYING_SLOTS)
			IF ENTER_SLOT_MACHINE()
				SET_PLAYER_IS_PLAYING_CASINO_GAME(CG_SLOTS)
				SET_CLIENT_STATE(LOCAL_STATE_USING_MACHINE)
			ENDIF
		BREAK
		CASE LOCAL_STATE_USING_MACHINE
			IF NOT IS_AUDIO_SCENE_ACTIVE("dlc_vw_casino_slot_machines_playing")
				START_AUDIO_SCENE("dlc_vw_casino_slot_machines_playing")
			ENDIF
			SET_TABLE_GAMES_CAMERA_THIS_UPDATE(TABLE_GAMES_CAMERA_TYPE_SLOT_MACHINE)
			HANDLE_SLOT_MACHINE_UI()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_NEXT_CAMERA)
			DISABLE_CELLPHONE_THIS_FRAME_ONLY()
			HIDE_SCRIPTED_HUD_COMPONENT_THIS_FRAME(HUD_MP_RANK_BAR) 
			
//			IF iCurrentCamera = CAM_SLOTS_FACE_MACHINE
//				IF IS_NET_PLAYER_OK(PLAYER_ID())
//					SET_ENTITY_LOCALLY_INVISIBLE(PLAYER_PED_ID())
//				ENDIF
//			ENDIF
			
			HANDLE_PLAYING_SLOT_MACHINE()
		BREAK
		CASE LOCAL_STATE_LEAVE_MACHINE
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_NEXT_CAMERA)
			DISABLE_CELLPHONE_THIS_FRAME_ONLY()
			HIDE_SCRIPTED_HUD_COMPONENT_THIS_FRAME(HUD_MP_RANK_BAR) 

			IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSFive, BS_SIMPLE_INTERIOR_GLOBAL_BS_FIVE_PLAYING_SLOTS)
				CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSFive, BS_SIMPLE_INTERIOR_GLOBAL_BS_FIVE_PLAYING_SLOTS)
				SET_PLAYER_FINISHED_PLAYING_CASINO_GAME()
			ENDIF
			IF IS_AUDIO_SCENE_ACTIVE("dlc_vw_casino_slot_machines_playing")
				STOP_AUDIO_SCENE("dlc_vw_casino_slot_machines_playing")
			ENDIF
			IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
				IF GET_CURRENT_ANIM_PHASE() < 0.6
					SET_TABLE_GAMES_CAMERA_THIS_UPDATE(TABLE_GAMES_CAMERA_TYPE_SLOT_MACHINE)
				ENDIF
			ENDIF
			VECTOR vLeftInput,vRightInput
			
			vLeftInput = <<GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_X), GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y), 0.0>>
			vRightInput = <<GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_X), GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_Y), 0.0>>
			
//			IF HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(), HASH("CAN_EARLY_OUT_FOR_MOVEMENT"))
//				PRINTLN("LOCAL_STATE_LEAVE_MACHINE: can early exit")
//			ENDIF
			
			IF GET_CURRENT_ANIM_PHASE() > 0.99
			OR GET_CURRENT_ANIM_PHASE() < 0 
			OR ((VMAG(vLeftInput) >= 0.24 OR VMAG(vRightInput) >= 0.24) AND HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(), HASH("CAN_EARLY_OUT_FOR_MOVEMENT")))
//				IF ((VMAG(vLeftInput) >= 0.24) AND HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(), HASH("CAN_EARLY_OUT_FOR_MOVEMENT")))
//					PRINTLN("LOCAL_STATE_LEAVE_MACHINE: early exit triggered")
//				ENDIF
				CLEAR_PED_TASKS(PLAYER_PED_ID())
				CLEANUP_SLOTS_SCALEFORM()
				SET_CLIENT_STATE(LOCAL_STATE_CHECK_FOR_LOCATE)
				IF IS_NET_PLAYER_OK(PLAYER_ID())
				AND NOT IS_PLAYER_SWITCH_IN_PROGRESS()
					NET_SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
				ENDIF
				IF IS_BIT_SET(iLocalBS,LOCAL_BS_REQUESTED_ANIMS)
					REMOVE_ANIM_DICT(GET_PLAYER_ANIM_DICT())
					CLEAR_BIT(iLocalBS,LOCAL_BS_REQUESTED_ANIMS)
				ENDIF
				IF IS_RADAR_HIDDEN()
					DISPLAY_RADAR(TRUE)
				ENDIF
				REINIT_NET_TIMER(stExitCapsuleOverride,TRUE)
				IF IS_BIT_SET(iLocalBS,LOCAL_BS_TOOK_AWAY_CONTROL)
					SET_DISABLE_RANK_UP_MESSAGE(FALSE)
					CLEAR_BIT(iLocalBS,LOCAL_BS_TOOK_AWAY_CONTROL)
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC SERVER_INIT_SLOT_MACHINES()
	INT i, j
	RESULT_STRUCT results
	REPEAT MAX_SLOT_MACHINES i
		DETERMINE_SLOT_MACHINE_RESULT(i,results,slotSetup,localSlots #IF IS_DEBUG_BUILD,  iOverrideReelResult, iOverrideWheelResult #ENDIF)
		REPEAT NUM_REELS j
			serverBD.fReelPropSlotResults[i][j] = results.fPropReelPos[j]
			serverBD.iReelVirtSlotResults[i][j] = results.iVirtualReelPos[j]
			serverBD.iPrizeWheelResult[i] = results.iWheelPos
		ENDREPEAT
	ENDREPEAT
ENDPROC

//PLAYER_INDEX thePlayer
//	REPEAT NUM_NETWORK_PLAYERS i
//		thePlayer = INT_TO_PLAYERINDEX(i)
//		IF IS_NET_PLAYER_OK(thePlayer,FALSE)
//			IF playerBD[i].iCurrentMachineID >= 0
//				IF IS_BIT_SET(playerBD[i].iBS,PLAYER_BD_BS_SPINNING)
//					IF NOT IS_BIT_SET(LocalSlots[iCurrentlyUsedMachine].iBS,LSM_BS_SPINNING)
//						SET_BIT(LocalSlots[iCurrentlyUsedMachine].iBS,LSM_BS_SPINNING)
//						LocalSlots[iCurrentlyUsedMachine].spinTimer = playerBD[i].spinTimer
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDREPEAT

PROC SERVER_PROCESS_PLAYER_USING_MACHINE(INT iPlayerIndex)
	INT iCurMachine = playerBD[iPlayerIndex].iCurrentMachineID
	INT i
	IF serverBD.iMachineUsedBy[iCurMachine] = -1
		#IF IS_DEBUG_BUILD
		IF IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(iPlayerIndex),FALSE)
			PRINTLN("CASINO_SLOTS: SERVER_PROCESS_PLAYER_USING_MACHINE giving machine #",iCurMachine," to player ",GET_PLAYER_NAME(INT_TO_PLAYERINDEX(iPlayerIndex)))
		ELSE
			PRINTLN("CASINO_SLOTS: SERVER_PROCESS_PLAYER_USING_MACHINE giving machine #",iCurMachine," to player #",iPlayerIndex)
		ENDIF
		#ENDIF
		serverBD.iMachineUsedBy[iCurMachine] = iPlayerIndex
	ELIF serverBD.iMachineUsedBy[iCurMachine] = iPlayerIndex
		IF NOT IS_BIT_SET(serverBD.iBS_MachineSpinning[GET_BITSET_ARRAY_INDEX(iCurMachine)],GET_BITSET_ELEMENT_ID_WITH_ARRAY(iCurMachine))
			IF IS_BIT_SET(playerBD[iPlayerIndex].iBS,PLAYER_BD_BS_SPINNING)
			AND NOT IS_BIT_SET(playerBD[iPlayerIndex].iBS,PLAYER_BD_BS_FINISHED_SPINNING)
				SET_BIT(serverBD.iBS_MachineSpinning[GET_BITSET_ARRAY_INDEX(iCurMachine)],GET_BITSET_ELEMENT_ID_WITH_ARRAY(iCurMachine))
				REINIT_NET_TIMER(serverBD.spinTimer[iCurMachine])
				#IF IS_DEBUG_BUILD
				IF IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(iPlayerIndex),FALSE)
					PRINTLN("CASINO_SLOTS: SERVER_PROCESS_PLAYER_USING_MACHINE setting machine spinning ",iCurMachine,"  player ",GET_PLAYER_NAME(INT_TO_PLAYERINDEX(iPlayerIndex)))
				ELSE
					PRINTLN("CASINO_SLOTS: SERVER_PROCESS_PLAYER_USING_MACHINE setting machine spinning ",iCurMachine,"  player #",iPlayerIndex)
				ENDIF
				#ENDIF
			ENDIF
		ELSE
			IF IS_BIT_SET(playerBD[iPlayerIndex].iBS,PLAYER_BD_BS_PROCESSED_RESULT)
			OR NOT IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(iPlayerIndex),FALSE)
				REPEAT NUM_REELS i
					serverBD.fReelPropSlotResults[iCurMachine][i] = playerBD[iPlayerIndex].fReelPropSlotResult[i]
					serverBD.iReelVirtSlotResults[iCurMachine][i] = playerBD[iPlayerIndex].iReelVirtSlotResults[i]
					serverBD.iPrizeWheelResult[iCurMachine] = playerBD[iPlayerIndex].iPrizeWheelResult
					SET_BIT(serverBD.iBS_HasResult[GET_BITSET_ARRAY_INDEX(iCurMachine)],GET_BITSET_ELEMENT_ID_WITH_ARRAY(iCurMachine))
				ENDREPEAT
			ENDIF
			
			IF (IS_BIT_SET(playerBD[iPlayerIndex].iBS,PLAYER_BD_BS_PROCESSED_RESULT) 
				OR NOT IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(iPlayerIndex),FALSE)
				OR IS_BIT_SET(playerBD[iPlayerIndex].iBS,PLAYER_BD_BS_ABORTED_SPINNING))
			AND (IS_BIT_SET(playerBD[iPlayerIndex].iBS,PLAYER_BD_BS_FINISHED_SPINNING) 
				OR IS_BIT_SET(playerBD[iPlayerIndex].iBS,PLAYER_BD_BS_ABORTED_SPINNING)
				OR HAS_NET_TIMER_EXPIRED(serverBD.spinTimer[iCurMachine],GET_ACTIVE_MACHINE_SPIN_TIME(iCurMachine)+SLOTS_REEL_SPIN_TIME_FAILSAFE))
			
				#IF IS_DEBUG_BUILD
				IF IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(iPlayerIndex),FALSE)
					IF IS_BIT_SET(playerBD[iPlayerIndex].iBS,PLAYER_BD_BS_FINISHED_SPINNING)
						PRINTLN("CASINO_SLOTS: SERVER_PROCESS_PLAYER_USING_MACHINE machine finished spinning player done on machine #",iCurMachine,"  player ",GET_PLAYER_NAME(INT_TO_PLAYERINDEX(iPlayerIndex)))
					ELIF IS_BIT_SET(playerBD[iPlayerIndex].iBS,PLAYER_BD_BS_ABORTED_SPINNING)
						PRINTLN("CASINO_SLOTS: SERVER_PROCESS_PLAYER_USING_MACHINE machine ABORTED spinning player done on machine #",iCurMachine,"  player ",GET_PLAYER_NAME(INT_TO_PLAYERINDEX(iPlayerIndex)))
					ELSE
						PRINTLN("CASINO_SLOTS: SERVER_PROCESS_PLAYER_USING_MACHINE machine finished spinning timer expired on machine #",iCurMachine,"  player ",GET_PLAYER_NAME(INT_TO_PLAYERINDEX(iPlayerIndex)))
					ENDIF
				ELSE
					IF IS_BIT_SET(playerBD[iPlayerIndex].iBS,PLAYER_BD_BS_FINISHED_SPINNING)
						PRINTLN("CASINO_SLOTS: SERVER_PROCESS_PLAYER_USING_MACHINE machine finished spinning player done on machine #",iCurMachine,"  player #",iPlayerIndex)
					ELIF IS_BIT_SET(playerBD[iPlayerIndex].iBS,PLAYER_BD_BS_ABORTED_SPINNING)
						PRINTLN("CASINO_SLOTS: SERVER_PROCESS_PLAYER_USING_MACHINE machine ABORTED spinning player done on machine #",iCurMachine,"  player #",iPlayerIndex)
					ELSE
						PRINTLN("CASINO_SLOTS: SERVER_PROCESS_PLAYER_USING_MACHINE machine finished spinning timer expired on machine #",iCurMachine,"  player #",iPlayerIndex)
					ENDIF
				ENDIF
				#ENDIF
				CLEAR_BIT(serverBD.iBS_MachineSpinning[GET_BITSET_ARRAY_INDEX(iCurMachine)],GET_BITSET_ELEMENT_ID_WITH_ARRAY(iCurMachine))
				CLEAR_BIT(serverBD.iBS_HasResult[GET_BITSET_ARRAY_INDEX(iCurMachine)],GET_BITSET_ELEMENT_ID_WITH_ARRAY(iCurMachine))
				RESET_NET_TIMER(serverBD.spinTimer[iCurMachine])
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SERVER_MAINTAIN_SLOT_MACHINES()
	INT i
	INT iRequestedMachinesBS[2]
	PLAYER_INDEX playerID
//	REPEAT MAX_SLOT_MACHINES i
//		CLEAR_BIT(iRequestedMachinesBS[GET_BITSET_ARRAY_INDEX(i)],GET_BITSET_ELEMENT_ID_WITH_ARRAY(i))
//	ENDREPEAT
	REPEAT NUM_NETWORK_PLAYERS i
		playerID = INT_TO_PLAYERINDEX(i)
		IF NETWORK_IS_PLAYER_A_PARTICIPANT(playerID)
			IF IS_NET_PLAYER_OK(playerID,FALSE)
				IF playerBD[i].iCurrentMachineID >= 0
					SET_BIT(iRequestedMachinesBS[GET_BITSET_ARRAY_INDEX(playerBD[i].iCurrentMachineID)],GET_BITSET_ELEMENT_ID_WITH_ARRAY(playerBD[i].iCurrentMachineID))
					SERVER_PROCESS_PLAYER_USING_MACHINE(i)
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	REPEAT MAX_SLOT_MACHINES i
		IF NOT IS_BIT_SET(iRequestedMachinesBS[GET_BITSET_ARRAY_INDEX(i)],GET_BITSET_ELEMENT_ID_WITH_ARRAY(i))
			IF serverBD.iMachineUsedBy[i] >= 0
				#IF IS_DEBUG_BUILD
				IF IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(serverBD.iMachineUsedBy[i]),FALSE)
					PRINTLN("CASINO_SLOTS: SERVER_MAINTAIN_SLOT_MACHINES clearing request of machine #",i," of player ",GET_PLAYER_NAME(INT_TO_PLAYERINDEX(serverBD.iMachineUsedBy[i])))
				ELSE
					PRINTLN("CASINO_SLOTS: SERVER_MAINTAIN_SLOT_MACHINES clearing request of machine #",i," to player #",serverBD.iMachineUsedBy[i])
				ENDIF
				#ENDIF
				serverBD.iMachineUsedBy[i] = -1
			ENDIF
		ELSE
			IF serverBD.iMachineUsedBy[i] >= 0
				playerID = INT_TO_PLAYERINDEX(serverBD.iMachineUsedBy[i])
				IF NOT IS_NET_PLAYER_OK(playerID,FALSE)
				AND IS_BIT_SET(serverBD.iBS_MachineSpinning[GET_BITSET_ARRAY_INDEX(i)],GET_BITSET_ELEMENT_ID_WITH_ARRAY(i))
					CLEAR_BIT(serverBD.iBS_MachineSpinning[GET_BITSET_ARRAY_INDEX(i)],GET_BITSET_ELEMENT_ID_WITH_ARRAY(i))
					CLEAR_BIT(serverBD.iBS_HasResult[GET_BITSET_ARRAY_INDEX(i)],GET_BITSET_ELEMENT_ID_WITH_ARRAY(i))
					RESET_NET_TIMER(serverBD.spinTimer[i])
					PRINTLN("CASINO_SLOTS: SERVER_MAINTAIN_SLOT_MACHINES PLAYER NOT OK!! clearing request of machine #",i," to player #",serverBD.iMachineUsedBy[i])
					serverBD.iMachineUsedBy[i] = -1
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC MAINTAIN_SERVER()
	IF NOT IS_BIT_SET(serverBD.iBS,SERVER_BD_BS_INIT_SLOT_MACHINES)
		SERVER_INIT_SLOT_MACHINES()
		SET_BIT(serverBD.iBS,SERVER_BD_BS_INIT_SLOT_MACHINES)
	ENDIF
	
	SERVER_MAINTAIN_SLOT_MACHINES()
ENDPROC

PROC MAINTAIN_SCRIPT_CLEANUP()
	IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
		PRINTLN("[CASINO SLOTS] - SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE - SCRIPT CLEANUP D")
		SCRIPT_CLEANUP()
	ENDIF	
	
	IF g_bCleanupSlots
		PRINTLN("[CASINO SLOTS] - cleaning up script for g_bCleanupSlots")
		g_bCleanupSlots = FALSE
		SCRIPT_CLEANUP()
	ENDIF
	
	IF g_SpawnData.bPassedOutDrunk
	AND INT_TO_ENUM(PLAYER_SPAWN_ACTIVITY, g_TransitionSpawnData.iSpawnActivity) != SPAWN_ACTIVITY_NOTHING
		IF INT_TO_ENUM(PLAYER_SPAWN_ACTIVITY, g_TransitionSpawnData.iSpawnActivity) != SPAWN_ACTIVITY_TOILET_SPEW
			PRINTLN("[CASINO SLOTS] - cleaning up script for g_SpawnData.bPassedOutDrunk")
			SCRIPT_CLEANUP()
		ENDIF
	ENDIF
	IF NOT IS_PLAYER_IN_CASINO(PLAYER_ID())
		PRINTLN("[CASINO SLOTS] - cleaning up script for IS_PLAYER_IN_CASINO")
		SCRIPT_CLEANUP()
	ENDIF
	
	IF IS_PLAYER_AUTOWARP_BETWEEN_TWO_INTERIORS()
	AND g_SimpleInteriorData.eEventAutowarpSimpleInterior != SIMPLE_INTERIOR_CASINO
	AND IS_SCREEN_FADED_OUT()
		PRINTLN("[CASINO SLOTS] - cleaning up script for IS_PLAYER_AUTOWARP_BETWEEN_TWO_INTERIORS")
		SCRIPT_CLEANUP()
	ENDIF
	
	IF NETWORK_IS_ACTIVITY_SESSION()
	AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_EnableCasinoGame_Slots)
		PRINTLN("[CASINO SLOTS] - ciOptionsBS24_EnableCasinoGame_3CardPoker is FALSE")
		SCRIPT_CLEANUP()
	ENDIF
ENDPROC

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                      Main Loop                      //////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
SCRIPT
	
	INIT_DATA()
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		PRINTLN("[CASINO SLOTS] started ")
		PROCESS_PRE_GAME()	
	ELSE
		PRINTLN("[CASINO SLOTS] -NETWORK_IS_GAME_IN_PROGRESS = FALSE - SCRIPT CLEANUP A ")
		SCRIPT_CLEANUP()
	ENDIF
	
	#IF IS_DEBUG_BUILD
	INIT_DEBUG()
	CREATE_DEBUG_WIDGETS()
	#ENDIF
	
	SETUP_SLOT_MACHINES(slotSetup,localSlots)
	
	WHILE TRUE
		// One wait to rule them all. This can be the ONLY wait from here on in.....
		MP_LOOP_WAIT_ZERO()
			
		// If we have a match end event, bail
		MAINTAIN_SCRIPT_CLEANUP()
		
		#IF IS_DEBUG_BUILD
		MAINTAIN_DEBUG()
		#ENDIF
		
		MAINTAIN_CLIENT()
		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			MAINTAIN_SERVER()
		ENDIF
	ENDWHILE

ENDSCRIPT
