///// casino_slots_rng.sch
///// Online Technical: Conor McGuire
///// Casino slots RNG function header

USING "net_include.sch"
USING "MP_globals_casino_consts.sch"

CONST_INT MAX_SLOT_MACHINES		54

CONST_INT NUM_REELS 3
CONST_INT MAX_PROP_REEL_SIZE 16

CONST_INT MAX_PROP_WHEEL_SIZE	12

CONST_INT MAX_VIRTUAL_REEL_SIZE 64

CONST_INT SRS_SYMBOL_BLANK	0
CONST_INT SRS_SYMBOL1 		1
CONST_INT SRS_SYMBOL2 		2
CONST_INT SRS_SYMBOL3		3
CONST_INT SRS_SYMBOL4 		4
CONST_INT SRS_SYMBOL5 		5
CONST_INT SRS_SYMBOL6 		6
//CONST_INT SRS_SYMBOL7 		7
//CONST_INT SRS_SYMBOL8 		8
//CONST_INT SRS_SYMBOL9 		9
CONST_INT SRS_WILD 			7
CONST_INT SRS_MAX_SYMBOLS 	8

CONST_INT PAYOUT_NONE			0
CONST_INT PAYOUT_LINE_SYMBOL1	1
CONST_INT PAYOUT_LINE_SYMBOL2	2
CONST_INT PAYOUT_LINE_SYMBOL3	3
CONST_INT PAYOUT_LINE_SYMBOL4	4
CONST_INT PAYOUT_LINE_SYMBOL5	5
CONST_INT PAYOUT_LINE_SYMBOL6	6
//CONST_INT PAYOUT_LINE_SYMBOL7	7
//CONST_INT PAYOUT_LINE_SYMBOL8	8
//CONST_INT PAYOUT_LINE_SYMBOL9	9
CONST_INT PAYOUT_LINE_WILD		7
CONST_INT PAYOUT_2_WILD			8
CONST_INT PAYOUT_1_WILD			9
CONST_INT PAYOUT_MAX_PAYOUTS	10

CONST_INT MACHINE_1_ANGEL_AND_KNIGHT 			1
CONST_INT MACHINE_2_IMPOTENT_RAGE 				2
CONST_INT MACHINE_3_REPUBLICAN_SPACE_RANGERS 	3
CONST_INT MACHINE_4_FAME_OR_SHAME 				4
CONST_INT MACHINE_5_DEITY_OF_THE_SUN			5
CONST_INT MACHINE_6_KNIFE_AFTER_DARK			6
CONST_INT MACHINE_7_THE_DIAMOND					7
CONST_INT MACHINE_8_EVACUATOR				8

STRUCT LOCAL_SLOT_MACHINE_SETUP
	INT iSizeOfVirtualReel = 64 
	INT iVirtualReelContent[NUM_REELS][MAX_VIRTUAL_REEL_SIZE]
	INT iNumOfSymbolOnVirtReel[NUM_REELS][SRS_MAX_SYMBOLS]
	
	INT iSizeOfPropReel = 16
	INT iPropReelContent[NUM_REELS][MAX_PROP_REEL_SIZE]
	
	INT iPayouts[PAYOUT_MAX_PAYOUTS]
ENDSTRUCT

STRUCT LOCAL_SLOT_MACHINE
//	#IF IS_DEBUG_BUILD
//	SLOT_MACHINE_SPINNER spinner[NUM_REELS]
//	#ENDIF
	INT iLastWin
	INT iLastWheelWin
	FLOAT fPropReelRots[NUM_REELS]
	FLOAT fPropReelPrevRots[NUM_REELS]
	FLOAT fPropReelTarget[NUM_REELS]
	
	FLOAT fPropPrizeWheelRot
	FLOAT fPropPrizeWheelTarget = -1.0
	
	INT iMachineCost = 1
	INT iMachineType = 0
	
	INT iBS
	SCRIPT_TIMER spinTimer
ENDSTRUCT

STRUCT RESULT_STRUCT
	INT iVirtualReelPos[NUM_REELS]
	FLOAT fPropReelPos[NUM_REELS]
	INT iWheelPos
ENDSTRUCT

PROC SETUP_SLOT_MACHINES(LOCAL_SLOT_MACHINE_SETUP &slotSetup,LOCAL_SLOT_MACHINE &localSlots[])
	//REEL #0
	slotSetup.iVirtualReelContent[0][0] = SRS_SYMBOL_BLANK
	slotSetup.iVirtualReelContent[0][1] = SRS_SYMBOL6
	slotSetup.iVirtualReelContent[0][2] = SRS_SYMBOL1
	slotSetup.iVirtualReelContent[0][3] = SRS_SYMBOL2
	slotSetup.iVirtualReelContent[0][4] = SRS_SYMBOL3
	slotSetup.iVirtualReelContent[0][5] = SRS_SYMBOL4
	slotSetup.iVirtualReelContent[0][6] = SRS_WILD
	slotSetup.iVirtualReelContent[0][7] = SRS_SYMBOL2
	slotSetup.iVirtualReelContent[0][8] = SRS_SYMBOL1
	slotSetup.iVirtualReelContent[0][9] = SRS_SYMBOL3
	slotSetup.iVirtualReelContent[0][10] = SRS_SYMBOL4
	slotSetup.iVirtualReelContent[0][11] = SRS_SYMBOL1
	slotSetup.iVirtualReelContent[0][12] = SRS_SYMBOL_BLANK
	slotSetup.iVirtualReelContent[0][13] = SRS_SYMBOL5
	slotSetup.iVirtualReelContent[0][14] = SRS_SYMBOL1
	slotSetup.iVirtualReelContent[0][15] = SRS_SYMBOL_BLANK
	slotSetup.iVirtualReelContent[0][16] = SRS_SYMBOL3
	slotSetup.iVirtualReelContent[0][17] = SRS_SYMBOL_BLANK
	slotSetup.iVirtualReelContent[0][18] = SRS_SYMBOL4
	slotSetup.iVirtualReelContent[0][19] = SRS_SYMBOL2
	slotSetup.iVirtualReelContent[0][20] = SRS_SYMBOL1
	slotSetup.iVirtualReelContent[0][21] = SRS_SYMBOL2
	slotSetup.iVirtualReelContent[0][22] = SRS_SYMBOL1
	slotSetup.iVirtualReelContent[0][23] = SRS_SYMBOL1
	slotSetup.iVirtualReelContent[0][24] = SRS_WILD
	slotSetup.iVirtualReelContent[0][25] = SRS_SYMBOL3
	slotSetup.iVirtualReelContent[0][26] = SRS_SYMBOL1
	slotSetup.iVirtualReelContent[0][27] = SRS_SYMBOL2
	slotSetup.iVirtualReelContent[0][28] = SRS_SYMBOL4
	slotSetup.iVirtualReelContent[0][29] = SRS_SYMBOL5
	slotSetup.iVirtualReelContent[0][30] = SRS_WILD
	slotSetup.iVirtualReelContent[0][31] = SRS_SYMBOL3
	slotSetup.iVirtualReelContent[0][32] = SRS_SYMBOL_BLANK
	slotSetup.iVirtualReelContent[0][33] = SRS_SYMBOL4
	slotSetup.iVirtualReelContent[0][34] = SRS_SYMBOL6
	slotSetup.iVirtualReelContent[0][35] = SRS_SYMBOL1
	slotSetup.iVirtualReelContent[0][36] = SRS_SYMBOL2
	slotSetup.iVirtualReelContent[0][37] = SRS_SYMBOL_BLANK
	slotSetup.iVirtualReelContent[0][38] = SRS_SYMBOL2
	slotSetup.iVirtualReelContent[0][39] = SRS_SYMBOL_BLANK
	slotSetup.iVirtualReelContent[0][40] = SRS_SYMBOL3
	slotSetup.iVirtualReelContent[0][41] = SRS_SYMBOL5
	slotSetup.iVirtualReelContent[0][42] = SRS_SYMBOL3
	slotSetup.iVirtualReelContent[0][43] = SRS_SYMBOL4
	slotSetup.iVirtualReelContent[0][44] = SRS_SYMBOL2
	slotSetup.iVirtualReelContent[0][45] = SRS_SYMBOL1
	slotSetup.iVirtualReelContent[0][46] = SRS_SYMBOL2
	slotSetup.iVirtualReelContent[0][47] = SRS_SYMBOL1
	slotSetup.iVirtualReelContent[0][48] = SRS_SYMBOL6
	slotSetup.iVirtualReelContent[0][49] = SRS_SYMBOL1
	slotSetup.iVirtualReelContent[0][50] = SRS_WILD
	slotSetup.iVirtualReelContent[0][51] = SRS_SYMBOL5
	slotSetup.iVirtualReelContent[0][52] = SRS_WILD
	slotSetup.iVirtualReelContent[0][53] = SRS_SYMBOL1
	slotSetup.iVirtualReelContent[0][54] = SRS_SYMBOL2
	slotSetup.iVirtualReelContent[0][55] = SRS_SYMBOL4
	slotSetup.iVirtualReelContent[0][56] = SRS_SYMBOL1
	slotSetup.iVirtualReelContent[0][57] = SRS_SYMBOL2
	slotSetup.iVirtualReelContent[0][58] = SRS_SYMBOL3
	slotSetup.iVirtualReelContent[0][59] = SRS_SYMBOL5
	slotSetup.iVirtualReelContent[0][60] = SRS_WILD
	slotSetup.iVirtualReelContent[0][61] = SRS_SYMBOL4
	slotSetup.iVirtualReelContent[0][62] = SRS_SYMBOL_BLANK
	slotSetup.iVirtualReelContent[0][63] = SRS_SYMBOL6
	//REEL #1
	slotSetup.iVirtualReelContent[1][0] = SRS_SYMBOL3
	slotSetup.iVirtualReelContent[1][1] = SRS_SYMBOL_BLANK
	slotSetup.iVirtualReelContent[1][2] = SRS_SYMBOL1
	slotSetup.iVirtualReelContent[1][3] = SRS_SYMBOL6
	slotSetup.iVirtualReelContent[1][4] = SRS_SYMBOL3
	slotSetup.iVirtualReelContent[1][5] = SRS_SYMBOL1
	slotSetup.iVirtualReelContent[1][6] = SRS_SYMBOL_BLANK
	slotSetup.iVirtualReelContent[1][7] = SRS_SYMBOL3
	slotSetup.iVirtualReelContent[1][8] = SRS_SYMBOL2
	slotSetup.iVirtualReelContent[1][9] = SRS_WILD
	slotSetup.iVirtualReelContent[1][10] = SRS_SYMBOL5
	slotSetup.iVirtualReelContent[1][11] = SRS_SYMBOL_BLANK
	slotSetup.iVirtualReelContent[1][12] = SRS_SYMBOL1
	slotSetup.iVirtualReelContent[1][13] = SRS_WILD
	slotSetup.iVirtualReelContent[1][14] = SRS_SYMBOL4
	slotSetup.iVirtualReelContent[1][15] = SRS_SYMBOL_BLANK
	slotSetup.iVirtualReelContent[1][16] = SRS_SYMBOL1
	slotSetup.iVirtualReelContent[1][17] = SRS_SYMBOL1
	slotSetup.iVirtualReelContent[1][18] = SRS_SYMBOL_BLANK
	slotSetup.iVirtualReelContent[1][19] = SRS_SYMBOL2
	slotSetup.iVirtualReelContent[1][20] = SRS_SYMBOL5
	slotSetup.iVirtualReelContent[1][21] = SRS_SYMBOL1
	slotSetup.iVirtualReelContent[1][22] = SRS_SYMBOL4
	slotSetup.iVirtualReelContent[1][23] = SRS_SYMBOL_BLANK
	slotSetup.iVirtualReelContent[1][24] = SRS_SYMBOL3
	slotSetup.iVirtualReelContent[1][25] = SRS_SYMBOL_BLANK
	slotSetup.iVirtualReelContent[1][26] = SRS_SYMBOL3
	slotSetup.iVirtualReelContent[1][27] = SRS_SYMBOL_BLANK
	slotSetup.iVirtualReelContent[1][28] = SRS_SYMBOL1
	slotSetup.iVirtualReelContent[1][29] = SRS_SYMBOL_BLANK
	slotSetup.iVirtualReelContent[1][30] = SRS_SYMBOL6
	slotSetup.iVirtualReelContent[1][31] = SRS_SYMBOL_BLANK
	slotSetup.iVirtualReelContent[1][32] = SRS_SYMBOL_BLANK
	slotSetup.iVirtualReelContent[1][33] = SRS_SYMBOL1
	slotSetup.iVirtualReelContent[1][34] = SRS_SYMBOL_BLANK
	slotSetup.iVirtualReelContent[1][35] = SRS_SYMBOL3
	slotSetup.iVirtualReelContent[1][36] = SRS_SYMBOL_BLANK
	slotSetup.iVirtualReelContent[1][37] = SRS_SYMBOL1
	slotSetup.iVirtualReelContent[1][38] = SRS_SYMBOL_BLANK
	slotSetup.iVirtualReelContent[1][39] = SRS_SYMBOL2
	slotSetup.iVirtualReelContent[1][40] = SRS_SYMBOL2
	slotSetup.iVirtualReelContent[1][41] = SRS_SYMBOL4
	slotSetup.iVirtualReelContent[1][42] = SRS_SYMBOL_BLANK
	slotSetup.iVirtualReelContent[1][43] = SRS_SYMBOL_BLANK
	slotSetup.iVirtualReelContent[1][44] = SRS_SYMBOL_BLANK
	slotSetup.iVirtualReelContent[1][45] = SRS_WILD
	slotSetup.iVirtualReelContent[1][46] = SRS_SYMBOL_BLANK
	slotSetup.iVirtualReelContent[1][47] = SRS_SYMBOL2
	slotSetup.iVirtualReelContent[1][48] = SRS_WILD
	slotSetup.iVirtualReelContent[1][49] = SRS_SYMBOL5
	slotSetup.iVirtualReelContent[1][50] = SRS_SYMBOL_BLANK
	slotSetup.iVirtualReelContent[1][51] = SRS_SYMBOL1
	slotSetup.iVirtualReelContent[1][52] = SRS_SYMBOL_BLANK
	slotSetup.iVirtualReelContent[1][53] = SRS_SYMBOL_BLANK
	slotSetup.iVirtualReelContent[1][54] = SRS_SYMBOL_BLANK
	slotSetup.iVirtualReelContent[1][55] = SRS_SYMBOL1
	slotSetup.iVirtualReelContent[1][56] = SRS_SYMBOL5
	slotSetup.iVirtualReelContent[1][57] = SRS_SYMBOL5
	slotSetup.iVirtualReelContent[1][58] = SRS_SYMBOL_BLANK
	slotSetup.iVirtualReelContent[1][59] = SRS_WILD
	slotSetup.iVirtualReelContent[1][60] = SRS_SYMBOL_BLANK
	slotSetup.iVirtualReelContent[1][61] = SRS_SYMBOL_BLANK
	slotSetup.iVirtualReelContent[1][62] = SRS_SYMBOL1
	slotSetup.iVirtualReelContent[1][63] = SRS_SYMBOL6
	//REEL #2
	slotSetup.iVirtualReelContent[2][0] = SRS_SYMBOL_BLANK
	slotSetup.iVirtualReelContent[2][1] = SRS_SYMBOL1
	slotSetup.iVirtualReelContent[2][2] = SRS_WILD
	slotSetup.iVirtualReelContent[2][3] = SRS_SYMBOL6
	slotSetup.iVirtualReelContent[2][4] = SRS_SYMBOL_BLANK
	slotSetup.iVirtualReelContent[2][5] = SRS_SYMBOL2
	slotSetup.iVirtualReelContent[2][6] = SRS_SYMBOL2
	slotSetup.iVirtualReelContent[2][7] = SRS_SYMBOL4
	slotSetup.iVirtualReelContent[2][8] = SRS_SYMBOL_BLANK
	slotSetup.iVirtualReelContent[2][9] = SRS_SYMBOL1
	slotSetup.iVirtualReelContent[2][10] = SRS_SYMBOL_BLANK
	slotSetup.iVirtualReelContent[2][11] = SRS_SYMBOL5
	slotSetup.iVirtualReelContent[2][12] = SRS_SYMBOL_BLANK
	slotSetup.iVirtualReelContent[2][13] = SRS_SYMBOL2
	slotSetup.iVirtualReelContent[2][14] = SRS_SYMBOL5
	slotSetup.iVirtualReelContent[2][15] = SRS_SYMBOL_BLANK
	slotSetup.iVirtualReelContent[2][16] = SRS_SYMBOL_BLANK
	slotSetup.iVirtualReelContent[2][17] = SRS_SYMBOL3
	slotSetup.iVirtualReelContent[2][18] = SRS_SYMBOL_BLANK
	slotSetup.iVirtualReelContent[2][19] = SRS_SYMBOL1
	slotSetup.iVirtualReelContent[2][20] = SRS_SYMBOL_BLANK
	slotSetup.iVirtualReelContent[2][21] = SRS_SYMBOL_BLANK
	slotSetup.iVirtualReelContent[2][22] = SRS_SYMBOL_BLANK
	slotSetup.iVirtualReelContent[2][23] = SRS_SYMBOL2
	slotSetup.iVirtualReelContent[2][24] = SRS_SYMBOL1
	slotSetup.iVirtualReelContent[2][25] = SRS_SYMBOL_BLANK
	slotSetup.iVirtualReelContent[2][26] = SRS_SYMBOL1
	slotSetup.iVirtualReelContent[2][27] = SRS_SYMBOL_BLANK
	slotSetup.iVirtualReelContent[2][28] = SRS_SYMBOL_BLANK
	slotSetup.iVirtualReelContent[2][29] = SRS_SYMBOL2
	slotSetup.iVirtualReelContent[2][30] = SRS_SYMBOL_BLANK
	slotSetup.iVirtualReelContent[2][31] = SRS_SYMBOL1
	slotSetup.iVirtualReelContent[2][32] = SRS_SYMBOL_BLANK
	slotSetup.iVirtualReelContent[2][33] = SRS_SYMBOL4
	slotSetup.iVirtualReelContent[2][34] = SRS_SYMBOL_BLANK
	slotSetup.iVirtualReelContent[2][35] = SRS_SYMBOL4
	slotSetup.iVirtualReelContent[2][36] = SRS_SYMBOL_BLANK
	slotSetup.iVirtualReelContent[2][37] = SRS_SYMBOL_BLANK
	slotSetup.iVirtualReelContent[2][38] = SRS_SYMBOL_BLANK
	slotSetup.iVirtualReelContent[2][39] = SRS_SYMBOL3
	slotSetup.iVirtualReelContent[2][40] = SRS_SYMBOL_BLANK
	slotSetup.iVirtualReelContent[2][41] = SRS_SYMBOL4
	slotSetup.iVirtualReelContent[2][42] = SRS_SYMBOL_BLANK
	slotSetup.iVirtualReelContent[2][43] = SRS_SYMBOL_BLANK
	slotSetup.iVirtualReelContent[2][44] = SRS_WILD
	slotSetup.iVirtualReelContent[2][45] = SRS_SYMBOL_BLANK
	slotSetup.iVirtualReelContent[2][46] = SRS_SYMBOL2
	slotSetup.iVirtualReelContent[2][47] = SRS_SYMBOL_BLANK
	slotSetup.iVirtualReelContent[2][48] = SRS_SYMBOL5
	slotSetup.iVirtualReelContent[2][49] = SRS_SYMBOL4
	slotSetup.iVirtualReelContent[2][50] = SRS_SYMBOL_BLANK
	slotSetup.iVirtualReelContent[2][51] = SRS_SYMBOL3
	slotSetup.iVirtualReelContent[2][52] = SRS_SYMBOL_BLANK
	slotSetup.iVirtualReelContent[2][53] = SRS_SYMBOL_BLANK
	slotSetup.iVirtualReelContent[2][54] = SRS_SYMBOL_BLANK
	slotSetup.iVirtualReelContent[2][55] = SRS_SYMBOL3
	slotSetup.iVirtualReelContent[2][56] = SRS_SYMBOL_BLANK
	slotSetup.iVirtualReelContent[2][57] = SRS_SYMBOL3
	slotSetup.iVirtualReelContent[2][58] = SRS_SYMBOL_BLANK
	slotSetup.iVirtualReelContent[2][59] = SRS_SYMBOL2
	slotSetup.iVirtualReelContent[2][60] = SRS_SYMBOL4
	slotSetup.iVirtualReelContent[2][61] = SRS_SYMBOL_BLANK
	slotSetup.iVirtualReelContent[2][62] = SRS_SYMBOL_BLANK
	slotSetup.iVirtualReelContent[2][63] = SRS_SYMBOL6

	//PAYOUTS
	slotSetup.iPayouts[0] = 0
	slotSetup.iPayouts[1] = 25	//cherry
	slotSetup.iPayouts[2] = 50	//plum
	slotSetup.iPayouts[3] = 75	//melon
	slotSetup.iPayouts[4] = 100	//bells
	slotSetup.iPayouts[5] = 250	//sevens
	slotSetup.iPayouts[6] = 1000 //jackpot
//	slotSetup.iPayouts[7] = 0
//	slotSetup.iPayouts[8] = 0
//	slotSetup.iPayouts[9] = 0
	slotSetup.iPayouts[7] = 500 //3 wild
	slotSetup.iPayouts[8] = 5  //2 wild
	slotSetup.iPayouts[9] = 2  //1 wild

	slotSetup.iPropReelContent[0][0] = SRS_SYMBOL5 //7
	slotSetup.iPropReelContent[0][1] = SRS_SYMBOL2 //plum
	slotSetup.iPropReelContent[0][2] = SRS_SYMBOL1 //cherry
	slotSetup.iPropReelContent[0][3] = SRS_SYMBOL3 //melon
	slotSetup.iPropReelContent[0][4] = SRS_WILD //wild
	slotSetup.iPropReelContent[0][5] = SRS_SYMBOL6 //jackpot
	slotSetup.iPropReelContent[0][6] = SRS_SYMBOL1 //cherry
	slotSetup.iPropReelContent[0][7] = SRS_SYMBOL4 //bell
	slotSetup.iPropReelContent[0][8] = SRS_SYMBOL5 //7
	slotSetup.iPropReelContent[0][9] = SRS_SYMBOL2 //plum
	slotSetup.iPropReelContent[0][10] = SRS_SYMBOL3 //melon
	slotSetup.iPropReelContent[0][11] = SRS_WILD //wild
	slotSetup.iPropReelContent[0][12] = SRS_SYMBOL2 //plum
	slotSetup.iPropReelContent[0][13] = SRS_SYMBOL4 //bell
	slotSetup.iPropReelContent[0][14] = SRS_SYMBOL1 //cherry
	slotSetup.iPropReelContent[0][15] = SRS_WILD //wild
	
	slotSetup.iPropReelContent[1][0] = SRS_SYMBOL5 //7
	slotSetup.iPropReelContent[1][1] = SRS_SYMBOL2 //plum
	slotSetup.iPropReelContent[1][2] = SRS_SYMBOL1 //cherry
	slotSetup.iPropReelContent[1][3] = SRS_SYMBOL3 //melon
	slotSetup.iPropReelContent[1][4] = SRS_WILD //wild
	slotSetup.iPropReelContent[1][5] = SRS_SYMBOL6 //jackpot
	slotSetup.iPropReelContent[1][6] = SRS_SYMBOL1 //cherry
	slotSetup.iPropReelContent[1][7] = SRS_SYMBOL4 //bell
	slotSetup.iPropReelContent[1][8] = SRS_SYMBOL5 //7
	slotSetup.iPropReelContent[1][9] = SRS_SYMBOL2 //plum
	slotSetup.iPropReelContent[1][10] = SRS_SYMBOL3 //melon
	slotSetup.iPropReelContent[1][11] = SRS_WILD //wild
	slotSetup.iPropReelContent[1][12] = SRS_SYMBOL2 //plum
	slotSetup.iPropReelContent[1][13] = SRS_SYMBOL4 //bell
	slotSetup.iPropReelContent[1][14] = SRS_SYMBOL1 //cherry
	slotSetup.iPropReelContent[1][15] = SRS_WILD //wild
	
	slotSetup.iPropReelContent[2][0] = SRS_SYMBOL5 //7
	slotSetup.iPropReelContent[2][1] = SRS_SYMBOL2 //plum
	slotSetup.iPropReelContent[2][2] = SRS_SYMBOL1 //cherry
	slotSetup.iPropReelContent[2][3] = SRS_SYMBOL3 //melon
	slotSetup.iPropReelContent[2][4] = SRS_WILD //wild
	slotSetup.iPropReelContent[2][5] = SRS_SYMBOL6 //jackpot
	slotSetup.iPropReelContent[2][6] = SRS_SYMBOL1 //cherry
	slotSetup.iPropReelContent[2][7] = SRS_SYMBOL4 //bell
	slotSetup.iPropReelContent[2][8] = SRS_SYMBOL5 //7
	slotSetup.iPropReelContent[2][9] = SRS_SYMBOL2 //plum
	slotSetup.iPropReelContent[2][10] = SRS_SYMBOL3 //melon
	slotSetup.iPropReelContent[2][11] = SRS_WILD //wild
	slotSetup.iPropReelContent[2][12] = SRS_SYMBOL2 //plum
	slotSetup.iPropReelContent[2][13] = SRS_SYMBOL4 //bell
	slotSetup.iPropReelContent[2][14] = SRS_SYMBOL1 //cherry
	slotSetup.iPropReelContent[2][15] = SRS_WILD //wild
	
	INT i,j
	REPEAT NUM_REELS i
		REPEAT  slotSetup.iSizeOfVirtualReel j
			slotSetup.iNumOfSymbolOnVirtReel[i][slotSetup.iVirtualReelContent[i][j]]++
		ENDREPEAT
	ENDREPEAT

	localSlots[0].iMachineType = MACHINE_4_FAME_OR_SHAME
	localSlots[1].iMachineType = MACHINE_5_DEITY_OF_THE_SUN
	localSlots[2].iMachineType = MACHINE_6_KNIFE_AFTER_DARK
	localSlots[3].iMachineType = MACHINE_7_THE_DIAMOND
	localSlots[4].iMachineType = MACHINE_8_EVACUATOR
	localSlots[5].iMachineType = MACHINE_1_ANGEL_AND_KNIGHT
	localSlots[6].iMachineType = MACHINE_2_IMPOTENT_RAGE
	localSlots[7].iMachineType = MACHINE_3_REPUBLICAN_SPACE_RANGERS
	localSlots[8].iMachineType = MACHINE_4_FAME_OR_SHAME
	localSlots[9].iMachineType = MACHINE_5_DEITY_OF_THE_SUN
	localSlots[10].iMachineType = MACHINE_6_KNIFE_AFTER_DARK
	localSlots[11].iMachineType = MACHINE_7_THE_DIAMOND
	localSlots[12].iMachineType = MACHINE_1_ANGEL_AND_KNIGHT
	localSlots[13].iMachineType = MACHINE_2_IMPOTENT_RAGE
	localSlots[14].iMachineType = MACHINE_3_REPUBLICAN_SPACE_RANGERS
	localSlots[15].iMachineType = MACHINE_4_FAME_OR_SHAME
	localSlots[16].iMachineType = MACHINE_5_DEITY_OF_THE_SUN
	localSlots[17].iMachineType = MACHINE_4_FAME_OR_SHAME
	localSlots[18].iMachineType = MACHINE_5_DEITY_OF_THE_SUN
	localSlots[19].iMachineType = MACHINE_1_ANGEL_AND_KNIGHT
	localSlots[20].iMachineType = MACHINE_2_IMPOTENT_RAGE
	localSlots[21].iMachineType = MACHINE_3_REPUBLICAN_SPACE_RANGERS
	localSlots[22].iMachineType = MACHINE_7_THE_DIAMOND
	localSlots[23].iMachineType = MACHINE_8_EVACUATOR
	localSlots[24].iMachineType = MACHINE_4_FAME_OR_SHAME
	localSlots[25].iMachineType = MACHINE_5_DEITY_OF_THE_SUN
	localSlots[26].iMachineType = MACHINE_6_KNIFE_AFTER_DARK
	localSlots[27].iMachineType = MACHINE_4_FAME_OR_SHAME
	localSlots[28].iMachineType = MACHINE_5_DEITY_OF_THE_SUN
	localSlots[29].iMachineType = MACHINE_1_ANGEL_AND_KNIGHT
	localSlots[30].iMachineType = MACHINE_2_IMPOTENT_RAGE
	localSlots[31].iMachineType = MACHINE_3_REPUBLICAN_SPACE_RANGERS
	localSlots[32].iMachineType = MACHINE_7_THE_DIAMOND
	localSlots[33].iMachineType = MACHINE_8_EVACUATOR
	localSlots[34].iMachineType = MACHINE_4_FAME_OR_SHAME
	localSlots[35].iMachineType = MACHINE_5_DEITY_OF_THE_SUN
	localSlots[36].iMachineType = MACHINE_6_KNIFE_AFTER_DARK
	localSlots[37].iMachineType = MACHINE_8_EVACUATOR
	localSlots[38].iMachineType = MACHINE_7_THE_DIAMOND
	localSlots[39].iMachineType = MACHINE_6_KNIFE_AFTER_DARK
	localSlots[40].iMachineType = MACHINE_5_DEITY_OF_THE_SUN
	localSlots[41].iMachineType = MACHINE_4_FAME_OR_SHAME
	localSlots[42].iMachineType = MACHINE_3_REPUBLICAN_SPACE_RANGERS
	localSlots[43].iMachineType = MACHINE_2_IMPOTENT_RAGE
	localSlots[44].iMachineType = MACHINE_3_REPUBLICAN_SPACE_RANGERS
	localSlots[45].iMachineType = MACHINE_4_FAME_OR_SHAME
	localSlots[46].iMachineType = MACHINE_5_DEITY_OF_THE_SUN
	localSlots[47].iMachineType = MACHINE_1_ANGEL_AND_KNIGHT
	localSlots[48].iMachineType = MACHINE_2_IMPOTENT_RAGE
	localSlots[49].iMachineType = MACHINE_6_KNIFE_AFTER_DARK
	localSlots[50].iMachineType = MACHINE_7_THE_DIAMOND
	localSlots[51].iMachineType = MACHINE_8_EVACUATOR
	localSlots[52].iMachineType = MACHINE_4_FAME_OR_SHAME
	localSlots[53].iMachineType = MACHINE_5_DEITY_OF_THE_SUN

	REPEAT MAX_SLOT_MACHINES i
		SWITCH localSlots[i].iMachineType
			CASE MACHINE_1_ANGEL_AND_KNIGHT
			CASE MACHINE_6_KNIFE_AFTER_DARK
				localSlots[i].iMachineCost = 100
			BREAK
			CASE MACHINE_2_IMPOTENT_RAGE
			CASE MACHINE_3_REPUBLICAN_SPACE_RANGERS
				localSlots[i].iMachineCost = 25
			BREAK
			CASE MACHINE_4_FAME_OR_SHAME
			CASE MACHINE_8_EVACUATOR
				localSlots[i].iMachineCost = 5
			BREAK
			CASE MACHINE_5_DEITY_OF_THE_SUN
			CASE MACHINE_7_THE_DIAMOND
				localSlots[i].iMachineCost = 500
			BREAK
		ENDSWITCH
	ENDREPEAT 
ENDPROC

FUNC BOOL DOES_SLOT_MACHINE_HAVE_WHEEL(INT iMachine,LOCAL_SLOT_MACHINE &localSlots[])
	SWITCH localSlots[iMachine].iMachineType
		CASE MACHINE_4_FAME_OR_SHAME
			IF g_sMPtunables.bVC_CASINO_DISABLE_SLOTS_FAME_EVACUATOR
				RETURN FALSE
			ENDIF
			RETURN TRUE
		BREAK
		CASE MACHINE_8_EVACUATOR
			IF g_sMPtunables.bVC_CASINO_DISABLE_SLOTS_FAME_WHEEL
				RETURN FALSE
			ENDIF
			RETURN TRUE
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

#IF IS_DEBUG_BUILD
FUNC TEXT_LABEL_23 GET_SLOT_MACHINE_SYMBOL_NAME(INT symbol)
	TEXT_LABEL_23 symbolName
	SWITCH symbol
		CASE SRS_SYMBOL_BLANK symbolName = "SYMBOL_BLANK" BREAK
		CASE SRS_SYMBOL1 symbolName = "SYMBOL1" BREAK
		CASE SRS_SYMBOL2 symbolName = "SYMBOL2" BREAK
		CASE SRS_SYMBOL3 symbolName = "SYMBOL3" BREAK
		CASE SRS_SYMBOL4 symbolName = "SYMBOL4" BREAK
		CASE SRS_SYMBOL5 symbolName = "SYMBOL5" BREAK
		CASE SRS_SYMBOL6 symbolName = "SYMBOL6" BREAK
//		CASE SRS_SYMBOL7 symbolName = "SYMBOL7" BREAK
//		CASE SRS_SYMBOL8 symbolName = "SYMBOL8" BREAK
//		CASE SRS_SYMBOL9 symbolName = "SYMBOL9" BREAK
		CASE SRS_WILD symbolName = "WILD" BREAK
	ENDSWITCH
	RETURN symbolName
ENDFUNC
#ENDIF

PROC DETERMINE_SLOT_MACHINE_RESULT(INT iMachine,RESULT_STRUCT &results,LOCAL_SLOT_MACHINE_SETUP &slotSetup,LOCAL_SLOT_MACHINE &localSlots[] #IF IS_DEBUG_BUILD,  INT &iOverrideReelResult[], INT iOverrideWheelResult #ENDIF)
	INT i,j,iRand,iRand2, iSymbolCounter
	#IF IS_DEBUG_BUILD
	TEXT_LABEL_23 tlTemp
	#ENDIF
	REPEAT NUM_REELS i
		iSymbolCounter = 0
		iRand = 0
		iRand2 = 0
		iRand = GET_RANDOM_MWC_INT_IN_RANGE(0,slotSetup.iSizeOfVirtualReel)
		
		#IF IS_DEBUG_BUILD
		IF iOverrideReelResult[i] != SRS_SYMBOL_BLANK
			PRINTLN("DETERMINE_SLOT_MACHINE_RESULT: override result machine #",iMachine," = ",iOverrideReelResult[i])
			REPEAT slotSetup.iSizeOfVirtualReel j
				IF slotSetup.iVirtualReelContent[i][j] = iOverrideReelResult[i]
					iRand = j
					PRINTLN("DETERMINE_SLOT_MACHINE_RESULT: overriding machine #",iMachine," reel #",i," pos #",iRand)
					j = slotSetup.iSizeOfVirtualReel
				ENDIF
			ENDREPEAT
		ENDIF
		#ENDIF
		results.iVirtualReelPos[i] = iRand

		#IF IS_DEBUG_BUILD
		tlTemp = GET_SLOT_MACHINE_SYMBOL_NAME(slotSetup.iVirtualReelContent[i][iRand])
		PRINTLN("DETERMINE_SLOT_MACHINE_RESULT: Virt Machine #",iMachine," reel #",i," pos #",iRand," symbol = ", tlTemp )
		#ENDIF
		IF slotSetup.iVirtualReelContent[i][iRand] = SRS_SYMBOL_BLANK
			iRand2 = GET_RANDOM_INT_IN_RANGE(0,slotSetup.iSizeOfPropReel)
			results.fPropReelPos[i] = iRand2 + 0.5
			//PRINTLN("DETERMINE_SLOT_MACHINE_RESULT: 1 Prop Machine #",iMachine," reel #",i," pos #",serverBD.fReelPropSlotResults[iMachine][i] ," symbol = ", SRS_SYMBOL_BLANK)
		ELSE
			// check how many of that symbol
			REPEAT slotSetup.iSizeOfPropReel j
				IF slotSetup.iPropReelContent[i][j] = slotSetup.iVirtualReelContent[i][iRand]
					iSymbolCounter++
				ENDIF
			ENDREPEAT
			// get random in for those
			iRand2 = GET_RANDOM_INT_IN_RANGE(0,iSymbolCounter)
			//PRINTLN("DETERMINE_SLOT_MACHINE_RESULT: iRand2 = ",iRand2)
			iSymbolCounter = 0
			j = 0
			REPEAT slotSetup.iSizeOfPropReel j
				//PRINTLN("DETERMINE_SLOT_MACHINE_RESULT: prop contents ", slotSetup[i][j], " looking for #",localSlots[iMachine].iVirtualReelContent[i][iRand])
				IF slotSetup.iPropReelContent[i][j] = slotSetup.iVirtualReelContent[i][iRand]
					// set position on prop wheel
					IF iSymbolCounter = iRand2
						results.fPropReelPos[i] = TO_FLOAT(j)
						j = slotSetup.iSizeOfPropReel
					ENDIF
					iSymbolCounter++
				ENDIF
			ENDREPEAT
		ENDIF
	ENDREPEAT
	IF DOES_SLOT_MACHINE_HAVE_WHEEL(iMachine,localSlots)
		IF slotSetup.iVirtualReelContent[0][results.iVirtualReelPos[0]] = SRS_WILD
		AND slotSetup.iVirtualReelContent[1][results.iVirtualReelPos[1]]= SRS_WILD
		AND slotSetup.iVirtualReelContent[2][results.iVirtualReelPos[2]] = SRS_WILD
			
			iRand = GET_RANDOM_MWC_INT_IN_RANGE(0,MAX_PROP_WHEEL_SIZE)
			results.iWheelPos = iRand
			#IF IS_DEBUG_BUILD
			IF iOverrideWheelResult >= 0
				results.iWheelPos = iOverrideWheelResult
				PRINTLN("DETERMINE_SLOT_MACHINE_RESULT: override Machine ",iMachine," wheel result ",results.iWheelPos)
			ENDIF
			#ENDIF
			PRINTLN("DETERMINE_SLOT_MACHINE_RESULT: Machine ",iMachine," wheel result ",iRand)
		ENDIF
	ENDIF

ENDPROC
