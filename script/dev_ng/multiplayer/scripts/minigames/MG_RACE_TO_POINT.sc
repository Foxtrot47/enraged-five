//////////////////////////////////////////////////////////////////////////////////////////
// Name:        MG_RACE_TO_POINT														//
// Description: Criminal enters a store and holds it up to get some cash			 	//
// Written by:  Ryan Baker																//
// Date: 23/08/2012																		//
//////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"

// Game Headers
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_path.sch"
USING "commands_interiors.sch"
USING "commands_event.sch"
USING "commands_money.sch"

// Network Headers
USING "net_include.sch"
USING "net_events.sch"
USING "net_spawn.sch"

// CnC Headers
USING "net_mission.sch"

USING "net_scoring_common.sch"
USING "net_ambience.sch"

USING "help_at_location.sch"
USING "commands_object.sch"
USING "net_xp_animation.sch"

USING "net_race_to_point.sch"
USING "minigames_helpers.sch"
USING "net_friend_request.sch"

USING "net_wait_zero.sch"

USING "net_cash_transactions.sch"

#IF IS_DEBUG_BUILD
USING "net_debug_log.sch"
#ENDIF

USING "net_gang_boss.sch"

// Game States. 
CONST_INT GAME_STATE_INI 				0
CONST_INT GAME_STATE_RUNNING			1
CONST_INT GAME_STATE_LEAVE				2
CONST_INT GAME_STATE_TERMINATE_DELAY	3
CONST_INT GAME_STATE_END				4

/////////////////////////////////////////////////////
///    RACE TO POINT MENU ITEMS
CONST_INT ciRP_TYPE_SEND_INVITE						0
CONST_INT ciRP_TYPE_SEND_INVITE_2					1
CONST_INT ciRP_TYPE_DESTINATION						2
//CONST_INT ciRP_TYPE_DISTANCE						3
CONST_INT ciRP_TYPE_STANDARD_CASH					3
CONST_INT ciRP_TYPE_START							4
CONST_INT ciRP_TYPE_MAX								5
/////////////////////////////////////////////////////
///    RACE TO POINT MAX OPTIONS
CONST_INT ciRP_MAX_TYPE_SEND_INVITE					0
CONST_INT ciRP_MAX_TYPE_SEND_INVITE_2				1
INT 	  ciRP_MAX_TYPE_DESTINATION					= 10
CONST_INT ciRP_MAX_TYPE_STANDARD_CASH				0
CONST_INT ciRP_MAX_TYPE_START						0

CONST_INT SUB_MENU_NONE					0
CONST_INT SUB_MENU_INVITE_PLAYER_LIST	1

INT INVITE_PLAYER_LIST_MAX				= 1

CONST_FLOAT WAYPOINT_AREA_SEARCH		25.0
//CONST_INT 	WAGER_INCREMENT				25
CONST_FLOAT DESTINATION_MIN_DISTANCE	1000.0
//CONST_INT RACE_WAGER					100
CONST_INT RACE_XP_REWARD				150
CONST_FLOAT R2P_DESTINATION_RADIUS			7.0
CONST_FLOAT R2P_DESTINATION_RADIUS_HEIGHT	20.0

CONST_INT SAFE_NODE_SEARCH_RADIUS		25	//Range to check for Road Nodes
CONST_INT SAFE_COORDS_SEARCH_RADIUS		100	//Range to do GET_SAFE_COORDS_IN_AREA_FOR_CREATING_ENTITY 

ENUM RACE_STAGE_ENUM
	eR2P_SETUP,
	eR2P_COUNTDOWN,
	eR2P_RACE,
	eR2P_REWARD,
	eR2P_CLEANUP
ENDENUM

STRUCT R2P_SELECTED_ITEMS
	INT iSelection[ciRP_TYPE_MAX]
	INT iSelectionChangeID
ENDSTRUCT

//Server Bitset Data
CONST_INT biS_AnyCrewArrested			0
CONST_INT biS_AllCrewArrested			1
CONST_INT biS_AllCrewDead				2
CONST_INT biS_AllCrewLeftStartArea		3
CONST_INT biS_AnyCrewHasFinished 		4
CONST_INT biS_AllCrewHaveFinished 		5
CONST_INT biS_AllCrewHaveLeft 			6
CONST_INT biS_GetWaypointVector			7
CONST_INT biS_WaypointSearchStarted		8
CONST_INT biS_WaypointVectorFound		9
CONST_INT biS_FirstWaypointVectorFound	10
CONST_INT biS_LockDestination			11

// The server broadcast data.
// Everyone can read this data, only the server can update it
STRUCT ServerBroadcastData
	INT iServerGameState
		
	INT iServerBitSet
	INT iWinnerPart 		= -1
	//INT iStandardCashReward = 250
	//INT iTotalCashBet		= 0
	
	INT iTotalWager
	
	VECTOR vStart
	VECTOR vDestination
	
	VECTOR vStoredWaypoint
	
	TIME_DATATYPE StartTime
	
	INT iNumParticpants = 0
	
	R2P_SELECTED_ITEMS sSelection
	
	RACE_STAGE_ENUM eRaceStage = eR2P_SETUP
	
	SCRIPT_TIMER MissionTerminateDelayTimer
ENDSTRUCT
ServerBroadcastData serverBD
INT iWagerLastFrame
INT iStoredWager

CONST_INT biP_FinishedRace			0
CONST_INT biXPandCashGiven			1

// The client broadcast data.
// Everyone can read this data, only the local player only the server can update it
STRUCT PlayerBroadcastData
	INT iGameState
	
	INT iPlayerBitSet
	//INT iCashBet
	INT iWager
	
	RACE_STAGE_ENUM eRaceStage = eR2P_SETUP
	
	#IF IS_DEBUG_BUILD
		INT iDebugPassFail = 0
	#ENDIF
ENDSTRUCT
PlayerBroadcastData playerBD[NUM_NETWORK_PLAYERS]

///// MISSION VARIABLES /////
INT iBoolsBitSet
CONST_INT biInitialSetup			0
CONST_INT biHelpDisplayed			1
CONST_INT biWayPointActive			2
CONST_INT biStartSetup				3
CONST_INT biWagerSet				4
CONST_INT biReachedEndDestination	5
CONST_INT biJoinTickerSent			6
CONST_INT biDisqualified			7

BLIP_INDEX DestinationBlip
CHECKPOINT_INDEX DestinationCheckpoint
BLIP_INDEX RadiusBlip

BOOL bResetMenuNow
BOOL bChangedDestination = TRUE

INT iSubMenu = SUB_MENU_NONE
PLAYER_INDEX storePlayer
BOOL bSelectAvailable

BOOL bMouseAccept = FALSE
INT iMouseDestCycle = 0

SCRIPT_TIMER iMenuMoveTimer 
INT iMenuMoveTimerDelay = -1
CONST_INT MENU_MOVE_DELAY 250

SCRIPT_TIMER iMenuMoveTimerLR
INT iMenuMoveTimerDelayLR = -1

SCRIPT_TIMER AnotherCustomMenuTimer

TEXT_LABEL_15 tlDestination

CONST_FLOAT DISQUALIFIED_RANGE	7.5	//5
VECTOR vLocalStart

PIM_VHEADER_DATA VHeaderData
PIM_GLARE_DATA GlareData

///////////////////////////////////////////////
///    STRUCT For the launcher options
STRUCT R2P_LAUNCHER_MENU_STRUCT
	INT							iSelection[ciRP_TYPE_MAX]
	TEXT_LABEL_15 				tl15
	TEXT_LABEL_15 				tl15B
	INT 						iCurrentSelection
	INT 						iMenuBitSet
	INT 						iInviteTimer 			= 0
	INT 						iHelpTimer	 			= 0	
	INT 						iBitSet		 			= 0
	INT 						iSelectionOld 			= 0
	INT 						iServerIdOld			= 0
ENDSTRUCT
R2P_LAUNCHER_MENU_STRUCT StartMenuData

CONST_INT biM_InChargeOfMenu	0
CONST_INT biM_MenuSetup			1
CONST_INT biM_InviteSent		2

SCRIPT_TIMER iInviteTimer
INT iInviteTimerDelay = -1

COUNTDOWN_UI Race_CountDownUI

INT iNumParticipantsStored

BOOL bInTutorialSession

BOOL bDoWaypointHelp

SCRIPT_TIMER EndScreenTimer

SCRIPT_TIMER iWarningHelpTimer
//INT iWarningHelpTimerDelay

INT iReachedDestinationTime
SCRIPT_TIMER iResendEventTimer
CONST_INT RESEND_EVENT_DELAY	250
SCRIPT_TIMER iServerProcessWinnerTimer
CONST_INT SERVER_PROCESS_WINNER_DELAY	750

// Widgets & Debug.
#IF IS_DEBUG_BUILD
	BOOL bHostEndMissionNow
	BOOL bRunWithOnePlayer
	BOOL bWarpToEnd
#ENDIF

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                  MAIN PROCS                            ////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//Do necessary pre game start ini.
FUNC BOOL PROCESS_PRE_GAME( MP_MISSION_DATA missionScriptArgs )
	
	// This marks the script as a net script, and handles any instancing setup. 
	SETUP_MP_MISSION_FOR_NETWORK(NUM_NETWORK_PLAYERS, missionScriptArgs)	//GET_MAX_NUM_PARTICIPANTS_FOR_MP_MISSION(missionScriptArgs.mission) ,  missionScriptArgs)
	//NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE, PARTICIPANT_ID_TO_INT())  
	
	//Reserve Entities
	//RESERVE_NETWORK_MISSION_PEDS(0)

	// This makes sure the net script is active, waits untill it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD, SIZE_OF(playerBD))
	
	LOAD_MENU_ASSETS()
	
	// KGM: Wait for the first network broadcast before moving on
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		RETURN FALSE
	ENDIF
			
	// This script will not be paused if another script calls PAUSE_GAME
	SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		serverBD.vStart = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
		serverBD.vStoredWaypoint = <<0, 0, -2000>>
		SET_BIT(StartMenuData.iMenuBitSet, biM_InChargeOfMenu)
	ENDIF
	
	bInTutorialSession = NETWORK_IS_IN_TUTORIAL_SESSION()
		
	MPGlobalsAmbience.R2Pdata.bOnRaceToPoint = TRUE
	
	//SET_PLAYER_AS_AVAILABLE_FOR_FM_BETTING(missionScriptArgs.iInstanceId, 0, FMMC_TYPE_RACE_TO_POINT)
	
	iMenuMoveTimerDelay = -1
	START_NET_TIMER(iMenuMoveTimer)
	
	SET_PLAYER_NEXT_RESPAWN_LOCATION(SPAWN_LOCATION_NEAR_DEATH, TRUE)
	
	//Reset Globals
	MPGlobalsAmbience.R2Pdata.bReachedDestinationEventReceived = FALSE
	INT i
	REPEAT NUM_NETWORK_REAL_PLAYERS() i
		MPGlobalsAmbience.R2Pdata.iReachedDestinationTime[i] = 0
	ENDREPEAT
	
	//Set the Max Destinations if on a Mission
	IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_MISSION
		ciRP_MAX_TYPE_DESTINATION = 2
		NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - ON MISSION - ciRP_MAX_TYPE_DESTINATION = 3    <----------     ") NET_NL()
	ENDIF
	
	LOAD_MENU_ASSETS()
	INIT_PIM_MENU_HEADER(VHeaderData)
	
	playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_INI
	NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - PRE_GAME DONE - tt     <----------     ") NET_NL()
	
	RETURN TRUE
ENDFUNC

//Player leaves vehicle, dies, runs out of drugs, or the vehicle becomes undriveable
FUNC BOOL MISSION_END_CHECK()
	
	//All players on the Race are Arrested
	//IF IS_BIT_SET(serverBD.iServerBitSet, biS_AllCrewDead)
	//IF IS_BIT_SET(serverBD.iServerBitSet, biS_AllCrewArrested)
	//	NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - MISSION END - ALL DEAD OR ARRESTED     <----------     ") NET_NL()
	//	RETURN TRUE
	//ENDIF
	
	//Leave Area
	IF serverBD.eRaceStage = eR2P_SETUP	//<= eR2P_COUNTDOWN
		IF IS_BIT_SET(serverBD.iServerBitSet, biS_AllCrewLeftStartArea)
			NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - MISSION END - CREW NOT IN AREA     <----------     ") NET_NL()
			RETURN TRUE
		ENDIF
	ENDIF
	
	//SKIP IF WE WANT TO RUN WITH 1 PLAYER
	#IF IS_DEBUG_BUILD IF bRunWithOnePlayer RETURN FALSE ENDIF #ENDIF
	
	//Left Race
	/*IF serverBD.eRaceStage = eR2P_RACE
		IF serverBD.iNumParticpants <= 1 //IS_BIT_SET(serverBD.iServerBitSet, biS_AllCrewHaveLeft)
			NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - MISSION END - NOT ENOUGH PARTICIPANTS     <----------     ") NET_NL()
			RETURN TRUE
		ENDIF
	ENDIF*/
	
	RETURN FALSE
ENDFUNC

//Server only function that checks to see if the game mode should now end. For this test script, it if time has passed.
FUNC BOOL HAVE_MISSION_END_CONDITIONS_BEEN_MET()
	
	#IF IS_DEBUG_BUILD
		IF bHostEndMissionNow
			RETURN TRUE
		ENDIF
	#ENDIF	
	
	IF MISSION_END_CHECK()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Send outs a ticker to the other players to say you've joined
PROC SEND_OUT_JOINED_TICKER()
	IF NOT IS_BIT_SET(iBoolsBitSet, biJoinTickerSent)
		SCRIPT_EVENT_DATA_TICKER_MESSAGE TickerEventData
		TickerEventData.TickerEvent = TICKER_EVENT_RACE_TO_POINT_JOINED
		TickerEventData.playerID = PLAYER_ID()
		BROADCAST_TICKER_EVENT(TickerEventData, ALL_PLAYERS_ON_SCRIPT(FALSE))
		SET_BIT(iBoolsBitSet, biJoinTickerSent)
	ENDIF
ENDPROC

//PURPOSE: Send outs a ticker to the other players to say you've left
PROC SEND_OUT_LEFT_TICKER()
	//Send Ticker Out
	SCRIPT_EVENT_DATA_TICKER_MESSAGE TickerEventData
	IF IS_BIT_SET(iBoolsBitSet, biDisqualified)
		TickerEventData.TickerEvent = TICKER_EVENT_RACE_TO_POINT_DSQ
	ELSE
		TickerEventData.TickerEvent = TICKER_EVENT_RACE_TO_POINT_LEFT
	ENDIF
	TickerEventData.playerID = PLAYER_ID()
	BROADCAST_TICKER_EVENT(TickerEventData, ALL_PLAYERS_ON_SCRIPT(FALSE))
ENDPROC

//PURPOSE: Clears the variables being used to get Waypoint vector
PROC CLEAR_WAYPOINT_VECTOR_LOADING() 
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_GetWaypointVector)
		IF NETWORK_IS_GAME_IN_PROGRESS()
			REMOVE_NAVMESH_REQUIRED_REGIONS()
		ENDIF
		CLEAR_BIT(serverBD.iServerBitSet, biS_GetWaypointVector)
		SPAWNPOINTS_CANCEL_SEARCH()
		SET_MINIMAP_BLOCK_WAYPOINT(FALSE)
		NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - SET_MINIMAP_BLOCK_WAYPOINT - FALSE - D") NET_NL()
		NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - CLEAR_WAYPOINT_VECTOR_LOADING - biS_GetWaypointVector      <----------     ") NET_NL()
	ENDIF
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_WaypointSearchStarted)
		SPAWNPOINTS_CANCEL_SEARCH()
		CLEAR_BIT(serverBD.iServerBitSet, biS_WaypointSearchStarted)
		NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - CLEAR_WAYPOINT_VECTOR_LOADING - biS_WaypointSearchStarted      <----------     ") NET_NL()
	ENDIF
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_WaypointVectorFound)
		CLEAR_BIT(serverBD.iServerBitSet, biS_WaypointVectorFound)
		NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - CLEAR_WAYPOINT_VECTOR_LOADING - biS_WaypointVectorFound      <----------     ") NET_NL()
	ENDIF
ENDPROC

//PURPOSE: Gets the destination based on the Menu selection
FUNC VECTOR GET_DESTINATION_FROM_MENU()
	
	SWITCH serverBD.sSelection.iSelection[ciRP_TYPE_DESTINATION]
		
		CASE 0
			IF IS_WAYPOINT_ACTIVE()
				VECTOR vWaypoint
				vWaypoint = GET_BLIP_INFO_ID_COORD(GET_FIRST_BLIP_INFO_ID(GET_WAYPOINT_BLIP_ENUM_ID()))
				vWaypoint.z = GET_APPROX_HEIGHT_FOR_AREA(vWaypoint.x-5, vWaypoint.y-5, vWaypoint.x+5, vWaypoint.y+5)
						
				IF GET_DISTANCE_BETWEEN_COORDS(serverBD.vStart, vWaypoint) > DESTINATION_MIN_DISTANCE
					IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_GetWaypointVector)
						CLEAR_WAYPOINT_VECTOR_LOADING()
						
//////////						ADD_NAVMESH_REQUIRED_REGION(vWaypoint.x, vWaypoint.y, WAYPOINT_AREA_SEARCH)
						
						SET_MINIMAP_BLOCK_WAYPOINT(TRUE)
						NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - SET_MINIMAP_BLOCK_WAYPOINT - TRUE - A") NET_NL()
						
						SET_BIT(serverBD.iServerBitSet, biS_GetWaypointVector)
						NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - GET_DESTINATION_FROM_MENU - START LOOKING FOR WAYPOINT VECTOR - vWaypoint = ") NET_PRINT_VECTOR(vWaypoint) NET_NL()
						
						//GET_GROUND_Z_FOR_3D_COORD(vWaypoint+<<0, 0, 500>>, vWaypoint.z)
					ELSE
						NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - GET_DESTINATION_FROM_MENU - biS_GetWaypointVector - ALREADY SET     <----------     ") NET_NL()
					ENDIF
				
					RETURN vWaypoint
				ENDIF
			ENDIF
			
			//NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - GET_DESTINATION_FROM_MENU - USING serverBD.vStoredWaypoint ") NET_PRINT_VECTOR(serverBD.vStoredWaypoint) NET_NL()
			RETURN serverBD.vStoredWaypoint
		BREAK
		
		//City Gun Shop
		CASE 1
			CLEAR_WAYPOINT_VECTOR_LOADING()
			RETURN <<15.0720, -1121.5132, 27.8034>>
		BREAK
		
		//Golf or Mission Objective
		CASE 2
			IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType != FMMC_TYPE_MISSION
			AND NOT IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet6, BI_FM_NMH6_GOTO_GERALD_LTS)
				CLEAR_WAYPOINT_VECTOR_LOADING()
				RETURN GET_FMMC_MINI_GAME_START_LOCATION(FMMC_TYPE_MG_GOLF)
			ELSE
				CLEAR_WAYPOINT_VECTOR_LOADING()
				PRINTLN("     ---------->     RACE TO POINT - MPGlobalsAmbience.vQuickGPS = ", MPGlobalsAmbience.vQuickGPS)
				RETURN MPGlobalsAmbience.vQuickGPS
			ENDIF
		BREAK
		
		//Tennis
		CASE 3
			CLEAR_WAYPOINT_VECTOR_LOADING()
			RETURN <<-1151.0372, -1620.8402, 3.2754+3>>
		BREAK
		
		//Strip Club
		CASE 4
			CLEAR_WAYPOINT_VECTOR_LOADING()
			RETURN <<132.9310, -1305.7456, 28.1664>>
		BREAK
		
		//Lake
		CASE 5
			CLEAR_WAYPOINT_VECTOR_LOADING()
			RETURN <<2054.0930, 3935.8401, 32.1771>>
		BREAK
		
		//Mountain
		CASE 6
			CLEAR_WAYPOINT_VECTOR_LOADING()
			RETURN <<514.7371, 5532.3853, 774.9705>>
		BREAK
		
		//Airport
		CASE 7
			CLEAR_WAYPOINT_VECTOR_LOADING()
			RETURN <<-1340.3629, -3041.9187, 12.9444>>
		BREAK
		
		//Military Base
		CASE 8
			CLEAR_WAYPOINT_VECTOR_LOADING()
			RETURN <<-2352.0706, 3423.6538, 27.8667>>	//-2186.3748, 3172.1597, 31.8103>>
		BREAK
		
		//North Cost
		CASE 9
			CLEAR_WAYPOINT_VECTOR_LOADING()
			RETURN <<54.7835, 7243.3877, 1.5166>>
		BREAK
		
		//Vinewood Sign
		CASE 10
			CLEAR_WAYPOINT_VECTOR_LOADING()
			RETURN <<675.3948, 1203.2501, 322.2605>>
		BREAK
		
	ENDSWITCH
	
	CLEAR_WAYPOINT_VECTOR_LOADING()
	RETURN		<<0, 0, -2000>>		//BELOW ORIGIN
	//RETURN 	<<-346.4007, 1151.5067, 324.7254>>		//Observatory Lookout point				//<<-1363, -1620, 2.6>>	//RANDOM BEACH
ENDFUNC

//PURPOSE: Returns TRUE if player should quit whilst on the Setup menu
FUNC BOOL SHOULD_CLEANUP_FROM_SETUP_MENU()
	
	// Make the player end the mission if they leave the Start area
	IF serverBD.eRaceStage = eR2P_SETUP	//<= eR2P_COUNTDOWN	//= eR2P_SETUP	//
		IF NOT IS_NET_PLAYER_OK(PLAYER_ID())
			NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - SHOULD_CLEANUP_FROM_SETUP_MENU - PLAYER NOT OK     <----------     ") NET_NL()
			RETURN TRUE
		ENDIF
		
		IF GET_CORONA_STATUS() != CORONA_STATUS_IDLE
			NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - SHOULD_CLEANUP_FROM_SETUP_MENU - PLAYER IN CORONA     <----------     ") NET_NL()
			RETURN TRUE
		ENDIF
		
		IF IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP()
			NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - SHOULD_CLEANUP_FROM_SETUP_MENU - PLAYER IN SHOP     <----------     ") NET_NL()
			RETURN TRUE
		ENDIF
		
		IF g_bMissionEnding = TRUE
			NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - SHOULD_CLEANUP_FROM_SETUP_MENU - MISSION ENDING SET     <----------     ") NET_NL()
			RETURN TRUE
		ENDIF
		
		IF g_b_OnLeaderboard = TRUE
			NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - SHOULD_CLEANUP_FROM_SETUP_MENU - LEADERBOARD ON SCREEN     <----------     ") NET_NL()
			RETURN TRUE
		ENDIF
		
		IF g_bInAtm
			NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - SHOULD_CLEANUP_FROM_SETUP_MENU - g_bInAtm     <----------     ") NET_NL()
			RETURN TRUE
		ENDIF
		
		IF g_bBrowserVisible
			NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - SHOULD_CLEANUP_FROM_SETUP_MENU - g_bBrowserVisible    <----------     ") NET_NL()
			RETURN TRUE
		ENDIF
		
		IF IS_WARNING_MESSAGE_ACTIVE()
			NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - SHOULD_CLEANUP_FROM_SETUP_MENU - IS_WARNING_MESSAGE_ACTIVE     <----------     ") NET_NL()
			RETURN TRUE
		ENDIF
		
		IF IS_BIT_SET(MPGlobalsAmbience.iAmbBitSet, iABI_STOP_IMPROMPTU_RACE_SETUP)
			CLEAR_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_STOP_IMPROMPTU_RACE_SETUP)
			NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - SHOULD_CLEANUP_FROM_SETUP_MENU - iABI_STOP_IMPROMPTU_RACE_SETUP     <----------     ") NET_NL()
			RETURN TRUE
		ENDIF
			
		IF NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), serverBD.vStart, <<R2P_START_RADIUS, R2P_START_RADIUS, R2P_START_RADIUS>>)
			IF NOT ARE_VECTORS_EQUAL(GET_DESTINATION_FROM_MENU(), <<0, 0, 0>>)
			AND NOT ARE_VECTORS_EQUAL(GET_DESTINATION_FROM_MENU(), <<0, 0, -2000>>)
				NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - SHOULD_CLEANUP_FROM_SETUP_MENU - PLAYER NOT IN AREA     <----------     ") NET_NL()
				RETURN TRUE
			ENDIF
		ELIF NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), serverBD.vStart, <<R2P_INVITE_RADIUS, R2P_INVITE_RADIUS, R2P_INVITE_RADIUS>>)
			/*IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				IF HAS_NET_TIMER_EXPIRED(iWarningHelpTimer, iWarningHelpTimerDelay)
					//PRINT_HELP("R2P_WARNH")	//~s~You are leaving the Race to Point start location.
					//HELP_AT_SCREEN_LOCATION("R2P_WARNH", eARROW_DIRECTION
					iWarningHelpTimerDelay = 30000
					RESET_NET_TIMER(iWarningHelpTimer)
					NET_PRINT("     ---------->     RACE TO POINT - LEAVING AREA - WARNING HELP - PRINTED") NET_NL()
				ENDIF
			ENDIF*/
			IF NOT HAS_NET_TIMER_STARTED(iWarningHelpTimer)
				START_NET_TIMER(iWarningHelpTimer)
				NET_PRINT("     ---------->     RACE TO POINT - LEAVING AREA - WARNING HELP - PRINTED") NET_NL()
			ENDIF
		ELSE
			//IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("R2P_WARNH")
			IF HAS_NET_TIMER_STARTED(iWarningHelpTimer)
				//iWarningHelpTimerDelay = 3000
				RESET_NET_TIMER(iWarningHelpTimer)
				CLEAR_HELP()
				NET_PRINT("     ---------->     RACE TO POINT - LEAVING AREA - WARNING HELP - CLEARED") NET_NL()
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//Helper function to get a clients game/mission state
FUNC INT GET_CLIENT_MISSION_STATE(INT iPlayer)
	RETURN playerBD[iPlayer].iGameState
ENDFUNC

//Helper function to get the servers game/mission state
FUNC INT GET_SERVER_MISSION_STATE()
	RETURN serverBD.iServerGameState
ENDFUNC

#IF IS_DEBUG_BUILD
PROC CREATE_WIDGETS()
	INT iPlayer
	TEXT_LABEL_15 tl63

	START_WIDGET_GROUP("RACE TO POINT")  
		
		ADD_WIDGET_BOOL("Warp to End", bWarpToEnd)
		//ADD_WIDGET_INT_READ_ONLY("iCashBet", playerBD[PARTICIPANT_ID_TO_INT()].iCashBet)
		//ADD_WIDGET_INT_READ_ONLY("iStandardCashReward", serverBD.iStandardCashReward)
		//ADD_WIDGET_INT_READ_ONLY("iTotalCashBet", serverBD.iTotalCashBet)
		ADD_WIDGET_INT_READ_ONLY("iTotalWager", serverBD.iTotalWager)
		ADD_WIDGET_INT_READ_ONLY("iWinnerPart", serverBD.iWinnerPart)
		ADD_WIDGET_BOOL("bRunWithOnePlayer", bRunWithOnePlayer)
		
		ADD_WIDGET_INT_READ_ONLY("Column - iCurrentSelection", StartMenuData.iCurrentSelection)
		ADD_WIDGET_INT_READ_ONLY("Row - Selection", serverBD.sSelection.iSelection[ciRP_TYPE_DESTINATION])
		//ADD_WIDGET_INT_READ_ONLY("Wager Selection", StartMenuData.iSelection[ciRP_TYPE_WAGER_CASH])
		
		// Gameplay debug, sever only.
		START_WIDGET_GROUP("Server Only Gameplay") 
			ADD_WIDGET_BOOL("End mission", bHostEndMissionNow)			
		STOP_WIDGET_GROUP()		
		
		// Data about the server
		START_WIDGET_GROUP("Server BD") 
				ADD_WIDGET_INT_SLIDER("S. Game state", serverBD.iServerGameState,-1, HIGHEST_INT,1)
		STOP_WIDGET_GROUP()	

		// Data about the clients. * = You.
		START_WIDGET_GROUP("Client BD")  				
			REPEAT NUM_NETWORK_PLAYERS iPlayer
				tl63 = "Player "
				tl63 += iPlayer
				IF iPlayer = PARTICIPANT_ID_TO_INT()
					tl63 += "*"
				ENDIF
				START_WIDGET_GROUP(tl63)
					ADD_WIDGET_INT_SLIDER("Game state", playerBD[iPlayer].iGameState,-1, HIGHEST_INT,1)
				STOP_WIDGET_GROUP()
			ENDREPEAT				
		STOP_WIDGET_GROUP()	
		
	STOP_WIDGET_GROUP()
ENDPROC		

PROC UPDATE_WIDGETS()
	
	//Warp player to end location
	IF bWarpToEnd = TRUE
		IF playerBD[PARTICIPANT_ID_TO_INT()].eRaceStage = eR2P_RACE
			IF NET_WARP_TO_COORD(serverBD.vDestination+<<0, 0, 1>>, 0, TRUE, FALSE)
				NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - UPDATE_WIDGETS - bWarpToEnd DONE    <----------     ") NET_NL()
				bWarpToEnd = FALSE
			ELSE
				NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - UPDATE_WIDGETS - bWarpToEnd IN PROGRESS    <----------     ") NET_NL()
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

#ENDIF

#IF IS_DEBUG_BUILD
PROC DEBUG_SKIPS()
	//Force Pass
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
		playerBD[PARTICIPANT_ID_TO_INT()].iDebugPassFail = 1
		NET_PRINT_TIME() NET_PRINT("  ---->  RACE TO POINT - S-SKIP - FORCE PASS ") NET_NL()
	ENDIF
	
	//Warp to End
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
		bWarpToEnd = TRUE
		NET_PRINT_TIME() NET_PRINT("  ---->  RACE TO POINT - J-SKIP - WARP TO END ") NET_NL()
	ENDIF
ENDPROC
#ENDIF

//PURPOSE: Gets the amount of cash the player bet
//FUNC INT GET_WAGER_AMOUNT()
//	RETURN (StartMenuData.iSelection[ciRP_TYPE_WAGER_CASH]*WAGER_INCREMENT)
//ENDFUNC

//PURPOSE: Gives the player XP/Cash Reward
PROC GIVE_END_RACE_REWARD()
	
	IF NOT IS_BIT_SET(iBoolsBitSet, biXPandCashGiven)
		RaceToPointInfo sRaceToPointInfo
		sRaceToPointInfo.m_LocationHash = -1
		sRaceToPointInfo.m_MatchId = -1
		sRaceToPointInfo.m_NumPlayers = -1
		sRaceToPointInfo.m_RaceStartPos = serverBD.vStart
		sRaceToPointInfo.m_RaceEndPos = serverBD.vDestination
		
		IF serverBD.iWinnerPart = PARTICIPANT_ID_TO_INT()
						
			//INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_TOTAL_NO_SHOPS_HELD_UP, 1)
			
			PLAY_SOUND_FRONTEND(-1, "CHECKPOINT_NORMAL", "HUD_MINI_GAME_SOUNDSET", FALSE)
			
			IF iWagerLastFrame > g_sMPTunables.iImpromptuRaceCashRewardCap
				iWagerLastFrame = g_sMPTunables.iImpromptuRaceCashRewardCap
				PRINTLN("     ---------->     RACE TO POINT - iWagerLastFrame ABOVE LIMIT - CAP TO $", iWagerLastFrame)
			ENDIF
			IF serverBD.iTotalWager < ROUND(R2P_MIN_WAGER*g_sMPTunables.fImpromptuRaceExpensesMultiplier)
				serverBD.iTotalWager = ROUND(R2P_MIN_WAGER*g_sMPTunables.fImpromptuRaceExpensesMultiplier)
				PRINTLN("     ---------->     RACE TO POINT - NO WAGER - MAKE MINIMUM - B - serverBD.iTotalWager = ", serverBD.iTotalWager)
			ENDIF
			
			INT iCash = serverBD.iTotalWager
			
			PRINTLN(" LESTER KILL TARGET - [MAGNATE_GANG_BOSS] - Cash before boss cut: $", iCash)
			GB_HANDLE_GANG_BOSS_CUT(iCash)
			PRINTLN(" LESTER KILL TARGET - [MAGNATE_GANG_BOSS] - Cash after boss cut: $", iCash)
			
			IF iCash > 0
				IF USE_SERVER_TRANSACTIONS()
					INT iScriptTransactionIndex
					TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_JOBS, iCash, iScriptTransactionIndex, DEFAULT, DEFAULT)
					g_cashTransactionData[iScriptTransactionIndex].eExtraData.tl31ContenID = "mg_race_to_point"
				ELSE
					GIVE_LOCAL_PLAYER_CASH(iCash)
				//	NETWORK_EARN_FROM_RACE_WIN(serverBD.iTotalWager)
					NETWORK_EARN_FROM_JOB(iCash, "mg_race_to_point")
				ENDIF
			ENDIF
			NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - STANDARD CASH GIVEN = $") NET_PRINT_INT(serverBD.iTotalWager) NET_NL()
			
			//GIVE_LOCAL_PLAYER_XP("XPT_MEDIUMT", 150)
			GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD, PLAYER_PED_ID(), "XPT_MEDIUMT", XPTYPE_COMPLETE, XPCATEGORY_COMPLETE_RACE_TO_POINT_WON, ROUND(RACE_XP_REWARD*g_sMPTunables.fxp_tunable_Race_To_Point))	//BUG 1086434
			
			/*IF playerBD[PARTICIPANT_ID_TO_INT()].iCashBet > 0
				GIVE_LOCAL_PLAYER_CASH(playerBD[PARTICIPANT_ID_TO_INT()].iCashBet*2)	//ROUND(TO_FLOAT(serverBD.iTotalCashBet)/2))
				NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - WAGER CASH GIVEN = $") NET_PRINT_INT(playerBD[PARTICIPANT_ID_TO_INT()].iCashBet*2) NET_NL()
			ELSE
				NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - NO WAGER MADE") NET_NL()
			ENDIF*/
			
			//RACE WON TEXT
			//SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_CUSTOM, "R2P_IWBIG", )
			SETUP_NEW_BIG_MESSAGE_WITH_INT_AND_STRING(BIG_MESSAGE_GENERIC_WINNER, serverBD.iTotalWager, "R2P_IWSTR")
			
			//PRINT_TICKER("R2P_IWTIC")	//~s~You won the race to point.
			
			sRaceToPointInfo.m_RaceWon = TRUE
			PLAYSTATS_RACE_TO_POINT_MISSION_DONE(FMMC_TYPE_RACE_TO_POINT, RACE_XP_REWARD, serverBD.iTotalWager, sRaceToPointInfo)
			
			INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_RACE_2_POINT_WINS, 1)
			INCREMENT_BY_MP_INT_CHARACTER_AWARD(MP_AWARD_FMWINRACETOPOINTS, 1)
			
			//BROADCAST_BETTING_MISSION_FINISHED(PLAYER_ID())
			//UPDATE_FMMC_END_OF_MISSION_STATUS(ciFMMC_END_OF_MISSION_STATUS_PASSED, FMMC_TYPE_RACE_TO_POINT)
			
			#IF IS_DEBUG_BUILD NET_LOG("REWARD GIVEN")	#ENDIF
		ELSE
			//RACE LOST TEXT
			//SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_CUSTOM, "R2P_ILBIG", "R2P_ILSTR")
			SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_IN_STRAPLINE(BIG_MESSAGE_GENERIC_LOSER, "R2P_ILSTR", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(serverBD.iWinnerPart))))
			
			sRaceToPointInfo.m_RaceWon = FALSE
			PLAYSTATS_RACE_TO_POINT_MISSION_DONE(FMMC_TYPE_RACE_TO_POINT, 0, 0, sRaceToPointInfo)
			
			IF serverBD.iWinnerPart != -1
				IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(serverBD.iWinnerPart))
					PRINT_TICKER_WITH_PLAYER_NAME("R2P_PWTIC", NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(serverBD.iWinnerPart)))	//~a~ ~s~won the race to point.
					NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - DO PLAYER WHO WON TICKER    <----------     ") NET_NL()
					//BROADCAST_BETTING_MISSION_FINISHED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(serverBD.iWinnerPart)))
				ENDIF
			ENDIF
			INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_RACE_2_POINT_LOST, 1)
			//UPDATE_FMMC_END_OF_MISSION_STATUS(ciFMMC_END_OF_MISSION_STATUS_FAILED, FMMC_TYPE_RACE_TO_POINT)
		ENDIF
		
		
		IF DOES_BLIP_EXIST(DestinationBlip)
			REMOVE_BLIP(DestinationBlip)
		ENDIF
		DELETE_CHECKPOINT(DestinationCheckpoint)
		
		SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_AM_COMPLETE_IMPROMPTU_RACE)
		
		REQUEST_SYSTEM_ACTIVITY_TYPE_DONE_IMPROMPTU_RACE()
		
		NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - biXPandCashGiven - DONE    <----------     ") NET_NL()
		SET_BIT(iBoolsBitSet, biXPandCashGiven)
	
	//Big Message Timer
	ELSE
		IF HAS_NET_TIMER_EXPIRED(EndScreenTimer, 7500)
			playerBD[PARTICIPANT_ID_TO_INT()].eRaceStage = eR2P_CLEANUP
			NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - PLAYER STAGE = eR2P_CLEANUP    <----------     ") NET_NL()
		ENDIF
	ENDIF
ENDPROC


/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                  MISSION PROCS                  //////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

//Controls the help text for the Race
PROC CONTROL_HELP_TEXT()
	//IF IS_NET_PLAYER_OK(PLAYER_ID())
	//	
	//ENDIF
ENDPROC


//PURPOSE: Sets all players on the Race to Point to be able to chat to eahcother at any range
PROC SET_R2P_VOICE_CHAT_OVERRIDE(BOOL bEnable)
	INT iParticipant
	PLAYER_INDEX playerId
	//GAMER_HANDLE eGHandle
	
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
			playerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))
			
			IF IS_NET_PLAYER_OK(PlayerId, FALSE, TRUE)
			AND playerId != PLAYER_ID()
				//eGHandle = GET_GAMER_HANDLE_PLAYER(playerId)
				//IF IS_GAMER_HANDLE_VALID(eGHandle)
					//IF NOT NETWORK_IS_FRIEND(eGHandle)
						NETWORK_OVERRIDE_CHAT_RESTRICTIONS(playerId, bEnable)
						NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - SET_R2P_VOICE_CHAT_OVERRIDE - SET TO ") NET_PRINT_BOOL(bEnable)  NET_PRINT(" FOR ") NET_PRINT(GET_PLAYER_NAME(playerId)) NET_NL()
					/*#IF IS_DEBUG_BUILD
					ELSE
						NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - SET_R2P_VOICE_CHAT_OVERRIDE - NO NEED AS ALREADY FRIENDS WITH ") NET_PRINT(GET_PLAYER_NAME(playerId)) NET_NL()
					#ENDIF
					ENDIF*/
				//ENDIF
			ENDIF
			
		ENDIF
	ENDREPEAT
ENDPROC

// PURPOSE: returns an int with bits set to all the active players we should send to.
FUNC INT GET_PLAYERS_TO_INVITE_BITSET(INT iTeam, INT iRange)
	INT iPlayerFlags
	INT iPlayer 
	REPEAT NUM_NETWORK_PLAYERS iPlayer
		PLAYER_INDEX playerId = INT_TO_PLAYERINDEX(iPlayer)
		IF IS_NET_PLAYER_OK(playerId)
		AND IS_NET_PLAYER_OK(PLAYER_ID())
			IF (GET_PLAYER_TEAM(PlayerId) = iTeam)
			OR (GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_MISSION AND DOES_TEAM_LIKE_TEAM(iTeam, GET_PLAYER_TEAM(playerID)))
				IF (PlayerId <> PLAYER_ID())
					IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(GET_PLAYER_PED(playerId)),GET_ENTITY_COORDS(GET_PLAYER_PED(PLAYER_ID()))) <= iRange
						SET_BIT( iPlayerFlags , iPlayer)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("           GET_PLAYERS_TO_INVITE_BITSET - iPlayerFlags = ")
		NET_PRINT_INT(iPlayerFlags)
		NET_NL()
	#ENDIF
	
	RETURN(iPlayerFlags)
ENDFUNC

//Cleanup Mission Data
PROC SCRIPT_CLEANUP()
	
//	MPGlobalsAmbience.bInInterMenuInpromptuRaceSubMenu = FALSE
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			BROADCAST_RACE_TO_POINT_CANCEL_INVITE(serverBD.vStart, NETWORK_GET_INSTANCE_ID_OF_THIS_SCRIPT(), GET_PLAYERS_TO_INVITE_BITSET(GET_PLAYER_TEAM(PLAYER_ID()), R2P_INVITE_RADIUS))
		ENDIF
		MPGlobalsAmbience.R2Pdata.bStartingRacePointMenuFromPI = FALSE
	ENDIF
	
    //CLEAR_RACE_HELP()
	IF DOES_BLIP_EXIST(DestinationBlip)
		REMOVE_BLIP(DestinationBlip)
	ENDIF
	DELETE_CHECKPOINT(DestinationCheckpoint)
	
	SET_MISSION_NAME(FALSE)
		
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF playerBD[PARTICIPANT_ID_TO_INT()].eRaceStage = eR2P_SETUP
			SET_USER_RADIO_CONTROL_ENABLED(TRUE)
			CLEANUP_MENU_ASSETS()
		ENDIF
	ELSE
		SET_USER_RADIO_CONTROL_ENABLED(TRUE)
	ENDIF
	
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("R2P_WARNH")
		CLEAR_HELP()
	ENDIF
	
	CLEAR_WAYPOINT_VECTOR_LOADING()
	//SET_IGNORE_NO_GPS_FLAG(FALSE)
	SET_IGNORE_NO_GPS_FLAG_UNTIL_FIRST_NORMAL_NODE(FALSE)
	SPAWNPOINTS_CANCEL_SEARCH()
	SET_MINIMAP_BLOCK_WAYPOINT(FALSE)
	NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - SET_MINIMAP_BLOCK_WAYPOINT - FALSE - E") NET_NL()
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF NOT IS_BIT_SET(iBoolsBitSet, biXPandCashGiven)
			SEND_OUT_LEFT_TICKER()
		ENDIF
	
		IF IS_BIT_SET(iBoolsBitSet, biStartSetup)
			IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType != FMMC_TYPE_MISSION
				GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = -1
				GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.mdID.idCreator = -1
			ENDIF
			SET_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_RACETOPOINT, FALSE)
		ENDIF
		
		IF serverBD.eRaceStage >= eR2P_REWARD
			CLEANUP_BIG_MESSAGE()
		ENDIF
		
		SHOW_ONLY_BLIPS_OF_PLAYERS_ON_THIS_SCRIPT(FALSE)
	
		SET_R2P_VOICE_CHAT_OVERRIDE(FALSE)
	ENDIF
	
	//BROADCAST_PLAYER_LEFT_BETTING()
	
	/*IF NOT IS_BIT_SET(iBoolsBitSet, biXPandCashGiven)
		UPDATE_FMMC_END_OF_MISSION_STATUS(ciFMMC_END_OF_MISSION_STATUS_CANCELLED, FMMC_TYPE_RACE_TO_POINT)
	ENDIF*/
	
	CLEANUP_PIM_GLARE(GlareData)
	
	MPGlobalsAmbience.R2Pdata.bOnCountdown = FALSE
	MPGlobalsAmbience.R2Pdata.bOnRaceToPoint = FALSE
	MPGlobalsAmbience.R2Pdata.bForceEnd = FALSE
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF g_PassiveModeStored.bEnabled = TRUE
			IF NOT NETWORK_IS_ACTIVITY_SESSION()
			AND NOT HAS_TRANSITION_SESSION_SELECTED_RESTART_RANDOM()
			//AND NOT IS_PLAYER_ON_ANY_FM_MISSION_THAT_IS_NOT_GANG_ATTACK(PLAYER_ID())
				PRINTLN("     ---------->     RACE TO POINT - CLEANUP -  g_PassiveModeStored.bEnabled = TRUE - PUT PLAYER INTO PASSIVE MODE")
				ENABLE_MP_PASSIVE_MODE(PMER_END_OF_MATCH)
			ENDIF
		ENDIF
	ENDIF
	
	CLEAR_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_STOP_IMPROMPTU_RACE_SETUP)
	
	NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - CLEANUP MISSION      <----------     ") NET_NL()
	#IF IS_DEBUG_BUILD NET_LOG("SCRIPT_CLEANUP")	#ENDIF
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC

//PURPOSE: Checks if we Triggered the Race to Point with someone and will then invite them
/*PROC CONTROL_INVITING_PLAYER_TRIGGERED_WITH()
	IF MPGlobalsAmbience.R2Pdata.PlayerTriggeredWith != INVALID_PLAYER_INDEX()
		INT iPlayer
		REPEAT NUM_NETWORK_PLAYERS iPlayer
			PLAYER_INDEX PlayerId = INT_TO_PLAYERINDEX(iPlayer)
			IF MPGlobalsAmbience.R2Pdata.PlayerTriggeredWith = PlayerId
			AND MPGlobalsAmbience.R2Pdata.PlayerTriggeredWith != PLAYER_ID()
				IF IS_NET_PLAYER_OK(PlayerId)
					IF IS_ENTITY_AT_COORD(GET_PLAYER_PED(PlayerId), serverBD.vStart, <<R2P_INVITE_RADIUS, R2P_INVITE_RADIUS, R2P_INVITE_RADIUS>>)
						BROADCAST_RACE_TO_POINT_INVITE(serverBD.vStart, serverBD.vDestination, tlDestination, NETWORK_GET_INSTANCE_ID_OF_THIS_SCRIPT(), SPECIFIC_PLAYER(PlayerId))
						NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - Destination = ") NET_PRINT(tlDestination) NET_PRINT(" vStart = ") NET_PRINT_VECTOR(serverBD.vStart) NET_PRINT(" vDestination = ") NET_PRINT_VECTOR(serverBD.vDestination) NET_NL()
						NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - CONTROL_INVITING_PLAYER_TRIGGERED_WITH - INVITE SENT    <----------     ") NET_NL()
					ELSE
						NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - CONTROL_INVITING_PLAYER_TRIGGERED_WITH - PLAYER OUT OF INVITE RADIUS    <----------     ") NET_NL()
					ENDIF
				ELSE
					NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - CONTROL_INVITING_PLAYER_TRIGGERED_WITH - PLAYER NOT OKAY    <----------     ") NET_NL()
				ENDIF
				R2P_CLEAR_PLAYER_TRIGGERED_WITH()
				EXIT
			ENDIF
		ENDREPEAT
		
		R2P_CLEAR_PLAYER_TRIGGERED_WITH()
		NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - CONTROL_INVITING_PLAYER_TRIGGERED_WITH - PLAYER NOT FOUND    <----------     ") NET_NL()
	ENDIF
ENDPROC*/

//PURPOSE: Checks if we Triggered the Race to Point with someone and will then invite them
//PROC CONTROL_INVITING_PLAYER_FROM_PAUSE_MENU()
//	INT iPlayer
//	REPEAT NUM_NETWORK_PLAYERS iPlayer
//		IF IS_BIT_SET(MPGlobalsAmbience.R2Pdata.iInvitedFromPlayerListBS, iPlayer)
//			PLAYER_INDEX PlayerId = INT_TO_PLAYERINDEX(iPlayer)
//			IF IS_NET_PLAYER_OK(PlayerId)
//				IF IS_ENTITY_AT_COORD(GET_PLAYER_PED(PlayerId), serverBD.vStart, <<R2P_INVITE_RADIUS, R2P_INVITE_RADIUS, R2P_INVITE_RADIUS>>)
//					BROADCAST_RACE_TO_POINT_INVITE(serverBD.vStart, serverBD.vDestination, tlDestination, NETWORK_GET_INSTANCE_ID_OF_THIS_SCRIPT(), SPECIFIC_PLAYER(PlayerId))
//					NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - Destination = ") NET_PRINT(tlDestination) NET_PRINT(" vStart = ") NET_PRINT_VECTOR(serverBD.vStart) NET_PRINT(" vDestination = ") NET_PRINT_VECTOR(serverBD.vDestination) NET_NL()
//					NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - CONTROL_INVITING_PLAYER_FROM_PAUSE_MENU - INVITE SENT    <----------     ") NET_NL()
//				ELSE
//					NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - CONTROL_INVITING_PLAYER_FROM_PAUSE_MENU - PLAYER OUT OF INVITE RADIUS    <----------     ") NET_NL()
//				ENDIF
//			ELSE
//				NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - CONTROL_INVITING_PLAYER_FROM_PAUSE_MENU - PLAYER NOT OKAY    <----------     ") NET_NL()
//			ENDIF
//			R2P_CLEAR_PLAYER_INVITED_FROM_PAUSE_MENU()
//			EXIT
//		ENDIF
//	ENDREPEAT
//	
//	R2P_CLEAR_PLAYER_INVITED_FROM_PAUSE_MENU()
//	//NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - CONTROL_INVITING_PLAYER_FROM_PAUSE_MENU - PLAYER NOT FOUND    <----------     ") NET_NL()
//ENDPROC

//PURPOSE: Checks the player isn't the passenger of somebody else in the Race
FUNC BOOL OKAY_TO_SEND_REACHED_DESTINATION_EVENT(PED_INDEX passedInPed)
	IF IS_PED_IN_ANY_VEHICLE(passedInPed)
		VEHICLE_INDEX thisVeh  = GET_VEHICLE_PED_IS_IN(passedInPed)
		IF IS_VEHICLE_DRIVEABLE(thisVeh)
			PED_INDEX thisPed
			
			INT i
			REPEAT (GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(thisVeh)+1) i
				thisPed = GET_PED_IN_VEHICLE_SEAT(thisVeh, INT_TO_ENUM(VEHICLE_SEAT, i-1))
				IF DOES_ENTITY_EXIST(thisPed)
					IF NOT IS_PED_INJURED(thisPed)
						IF IS_PED_A_PLAYER(thisPed)
							
							PLAYER_INDEX l_player = NETWORK_GET_PLAYER_INDEX_FROM_PED(thisPed)
							IF l_player != NETWORK_GET_PLAYER_INDEX_FROM_PED(passedInPed)
								IF NETWORK_IS_PLAYER_A_PARTICIPANT(l_player)
									IF NETWORK_IS_PARTICIPANT_ACTIVE(NETWORK_GET_PARTICIPANT_INDEX(l_player))
										NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - OKAY_TO_SEND_REACHED_DESTINATION_EVENT - FALSE - NOT FIRST PARTICIPANT IN VEHICLE") NET_NL()
										RETURN FALSE
									ENDIF
								ENDIF
							ELSE
								NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - OKAY_TO_SEND_REACHED_DESTINATION_EVENT - TRUE - FIRST PARTICIPANT IN VEHICLE") NET_NL()
								RETURN TRUE
							ENDIF
							
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
			
		ENDIF
	ENDIF
	
	NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - OKAY_TO_SEND_REACHED_DESTINATION_EVENT - TRUE") NET_NL()
	RETURN TRUE
ENDFUNC

//PURPOSE: Runs through the max num participents and does relevant checks
PROC MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER()
	INT iParticipant
	INT iShortestTime
	FLOAT fShortestDistance = 999999
	FLOAT fCurrentDistance
	//Set Bits that can then be cleared in the repeat if necessary
	SET_BIT(serverBD.iServerBitSet, biS_AllCrewDead)
	SET_BIT(serverBD.iServerBitSet, biS_AllCrewArrested)
	CLEAR_BIT(serverBD.iServerBitSet, biS_AnyCrewArrested)
	SET_BIT(serverBD.iServerBitSet, biS_AllCrewLeftStartArea)
	SET_BIT(serverBD.iServerBitSet, biS_AllCrewHaveLeft)
	serverBD.iNumParticpants = 0
	IF serverBD.eRaceStage = eR2P_SETUP
		//serverBD.iTotalCashBet = 0
		iWagerLastFrame = 0
	ENDIF
	INT iFinishTime, iPlayer
	
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
			PLAYER_INDEX PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))
			PED_INDEX PlayerPedId = GET_PLAYER_PED(PlayerId)
			
			//Criminal Player Checks
			//IF IS_PLAYER_CRIMINAL(PlayerId)
				
				//** CHECKS THAT DON'T NEED THE PLAYER TO BE ALIVE **//
				
				serverBD.iNumParticpants++
				
				//Calculate Total Cash Bet
				/*IF serverBD.eRaceStage = eR2P_SETUP
					serverBD.iTotalCashBet += playerBD[PARTICIPANT_ID_TO_INT()].iCashBet
				ENDIF*/
				
				//Check if all Crooks on the mission have left the start area
				IF IS_BIT_SET(serverBD.iServerBitSet, biS_AllCrewLeftStartArea)
					IF IS_ENTITY_AT_COORD(PlayerPedId, serverBD.vStart, <<R2P_START_RADIUS, R2P_START_RADIUS, R2P_START_RADIUS>>)
						CLEAR_BIT(serverBD.iServerBitSet, biS_AllCrewLeftStartArea)
					ENDIF
				ENDIF
					
				//** CHECKS THAT REQUIRE THE PLAYER TO BE ALIVE **//
				IF IS_NET_PLAYER_OK(PlayerId)
					
					CLEAR_BIT(serverBD.iServerBitSet, biS_AllCrewDead)
					
					//Check for Arrested Players
					SET_BIT(serverBD.iServerBitSet, biS_AnyCrewArrested)
					
					//Force End
					IF MPGlobalsAmbience.R2Pdata.bForceEnd = TRUE
					AND MPGlobalsAmbience.R2Pdata.bReachedDestinationEventReceived = FALSE
						fCurrentDistance = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PlayerPedId), serverBD.vDestination)
						IF fCurrentDistance < fShortestDistance
							IF OKAY_TO_SEND_REACHED_DESTINATION_EVENT(PlayerPedId)
								fShortestDistance = fCurrentDistance
								serverBD.iWinnerPart = iParticipant
								NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - fShortestDistance = ") NET_PRINT_FLOAT(fShortestDistance) NET_NL()
								NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - serverBD.iWinnerPart = ") NET_PRINT_INT(iParticipant) NET_NL()
								NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - WINNER PLAYER  = ") NET_PRINT(GET_PLAYER_NAME(PlayerId)) NET_NL()
							ENDIF
						ENDIF
					ENDIF
					
					//Check if all Crooks on the mission have left the start area
					IF serverBD.eRaceStage = eR2P_SETUP
						iWagerLastFrame += playerBD[iParticipant].iWager
						
					ELIF serverBD.eRaceStage = eR2P_RACE
//						IF serverBD.iWinnerPart = -1 // Speirs removed to fix (1861335)
							
							#IF IS_DEBUG_BUILD
							IF playerBD[iParticipant].iDebugPassFail = 1
								serverBD.iWinnerPart = iParticipant
								NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - PARTICIPANT S-SKIP - serverBD.iWinnerPart = ") NET_PRINT_INT(iParticipant) NET_NL()
							ENDIF
							#ENDIF
							
							iPlayer = NATIVE_TO_INT(PlayerId)
							iFinishTime = MPGlobalsAmbience.R2Pdata.iReachedDestinationTime[iPlayer]
							IF MPGlobalsAmbience.R2Pdata.bReachedDestinationEventReceived = TRUE
								IF HAS_NET_TIMER_EXPIRED(iServerProcessWinnerTimer, SERVER_PROCESS_WINNER_DELAY)
									IF iFinishTime <> 0
										IF (iFinishTime < iShortestTime)
										OR iShortestTime = 0
											iShortestTime = iFinishTime
											serverBD.iWinnerPart = iParticipant
											NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - iShortestTime = ") NET_PRINT_INT(iShortestTime) NET_NL()
											NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - serverBD.iWinnerPart = ") NET_PRINT_INT(iParticipant) NET_NL()
											NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - WINNER PLAYER  = ") NET_PRINT(GET_PLAYER_NAME(PlayerId)) NET_NL()
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
							/*IF IS_ENTITY_AT_COORD(PlayerPedId, serverBD.vDestination, <<R2P_DESTINATION_RADIUS, R2P_DESTINATION_RADIUS, 25>>)	//1000>>)
								IF NOT IS_PED_IN_ANY_VEHICLE(PlayerPedId)
									serverBD.iWinnerPart = iParticipant
									NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - NOT IN VEH - serverBD.iWinnerPart = ") NET_PRINT_INT(iParticipant) NET_NL()
								ELSE
									BOOL bIsWinner = TRUE
									VEHICLE_INDEX thisVeh  = GET_VEHICLE_PED_IS_IN(PlayerPedId)
									IF IS_VEHICLE_DRIVEABLE(thisVeh)
										PED_INDEX Driver = GET_PED_IN_VEHICLE_SEAT(thisVeh, VS_DRIVER)
										IF DOES_ENTITY_EXIST(Driver)
											IF NOT IS_PED_INJURED(Driver)
												IF IS_PED_A_PLAYER(Driver)
													PLAYER_INDEX l_player = NETWORK_GET_PLAYER_INDEX_FROM_PED(Driver)
													IF l_player != PlayerId
														IF NETWORK_IS_PLAYER_A_PARTICIPANT(l_player)
															IF NETWORK_IS_PARTICIPANT_ACTIVE(NETWORK_GET_PARTICIPANT_INDEX(l_player))
																bIsWinner = FALSE
																NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - IN VEH - NOT WINNER AS DRIVER IS PARTICPANT")  NET_NL()
															ENDIF
														ENDIF
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
									
									IF bIsWinner
										serverBD.iWinnerPart = iParticipant
										NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - IN VEH - serverBD.iWinnerPart = ") NET_PRINT_INT(iParticipant) NET_NL()
									ENDIF
								ENDIF
							ENDIF*/
							
							
//						ENDIF
					ENDIF
				ELSE
					CLEAR_BIT(serverBD.iServerBitSet, biS_AllCrewArrested)
				ENDIF
			//ENDIF
			
		ENDIF
	ENDREPEAT
	//**TWH - CMcM - 1425706 - Added new function to calculate player pot contribution for Impromptu Races.
	//serverBD.iTotalWager = ROUND(serverBD.iTotalWager*0.9)	//ONLY GIVE 90% as Reward
	IF serverBD.eRaceStage = eR2P_SETUP
		IF iWagerLastFrame > g_sMPTunables.iImpromptuRaceCashRewardCap
			//PRINTLN("     ---------->     RACE TO POINT - iWagerLastFrame = ", serverBD.iTotalWager)
			//PRINTLN("     ---------->     RACE TO POINT - g_sMPTunables.iImpromptuRaceCashRewardCap = ", g_sMPTunables.iImpromptuRaceCashRewardCap)
			iWagerLastFrame = g_sMPTunables.iImpromptuRaceCashRewardCap
		ENDIF
		IF serverBD.iTotalWager != iWagerLastFrame
			bResetMenuNow = TRUE
			serverBD.iTotalWager = iWagerLastFrame
			PRINTLN("     ---------->     RACE TO POINT - g_sMPTunables.iImpromptuRaceEntryFeeCap = ", g_sMPTunables.iImpromptuRaceEntryFeeCap)
			PRINTLN("     ---------->     RACE TO POINT - g_sMPTunables.fImpromptuRaceExpensesMultiplier = ", g_sMPTunables.fImpromptuRaceExpensesMultiplier)
			PRINTLN("     ---------->     RACE TO POINT - g_sMPTunables.iImpromptuRaceCashRewardCap = ", g_sMPTunables.iImpromptuRaceCashRewardCap)
			PRINTLN("     ---------->     RACE TO POINT - UPDATED serverBD.iTotalWager = ", serverBD.iTotalWager)
		ENDIF
		IF serverBD.iTotalWager < ROUND(R2P_MIN_WAGER*g_sMPTunables.fImpromptuRaceExpensesMultiplier)
			serverBD.iTotalWager = ROUND(R2P_MIN_WAGER*g_sMPTunables.fImpromptuRaceExpensesMultiplier)
			PRINTLN("     ---------->     RACE TO POINT - NO WAGER - MAKE MINIMUM - A - serverBD.iTotalWager = ", serverBD.iTotalWager)
		ENDIF
	ENDIF
ENDPROC


//PURPOSE: Returns the max options for the current selection
FUNC INT GET_R2P_SELECTION_MAX(INT iSelection)
	SWITCH iSelection
		CASE ciRP_TYPE_SEND_INVITE		RETURN ciRP_MAX_TYPE_SEND_INVITE
		CASE ciRP_TYPE_SEND_INVITE_2	RETURN ciRP_MAX_TYPE_SEND_INVITE_2
		CASE ciRP_TYPE_DESTINATION		RETURN ciRP_MAX_TYPE_DESTINATION
		CASE ciRP_TYPE_STANDARD_CASH	RETURN ciRP_MAX_TYPE_STANDARD_CASH
		//CASE ciRP_TYPE_WAGER_CASH		RETURN ciRP_MAX_TYPE_WAGER_CASH
		CASE ciRP_TYPE_START			RETURN ciRP_MAX_TYPE_START
	ENDSWITCH
	
	RETURN 0
ENDFUNC	

////PURPOSE: Calculates the standard reward for winning the race
//FUNC INT CALCULATE_STANDARD_CASH_REWARD()
//	INT iCash
//	FLOAT fCalculation
//	FLOAT fDistance = GET_DISTANCE_BETWEEN_COORDS(serverBD.vStart, serverBD.vDestination)
//	
//	fCalculation = (fDistance/4)
//	fCalculation = (fCalculation/50)
//	
//	iCash = FLOOR(fCalculation)
//	iCash = (iCash*50)
//	
//	RETURN iCash
//ENDFUNC

//PURPOSE: Stops the Water check returning TRUE for certain areas such as the Fun Fair Pier
FUNC BOOL IS_BLOCKED_WATER_AREA(VECTOR vLocation)
	
	//Check there is no little island in the way
	/*VECTOR vStartProbe = (vLocation+<<0, 0, 25>>)
	VECTOR vEndProbe = vLocation
	vEndProbe.z = -1
	VECTOR vWaterIntersection
	IF TEST_PROBE_AGAINST_ALL_WATER(vStartProbe, vEndProbe, SCRIPT_INCLUDE_MOVER, vWaterIntersection) = SCRIPT_WATER_TEST_RESULT_WATER
		FLOAT fGroundZ
		GET_GROUND_Z_FOR_3D_COORD(vStartProbe, fGroundZ)
		PRINTLN("     ---------->     RACE TO POINT - IS_BLOCKED_WATER_AREA - vWaterIntersection = ", vWaterIntersection)
		PRINTLN("     ---------->     RACE TO POINT - IS_BLOCKED_WATER_AREA - fGroundZ = ", fGroundZ)
		IF vWaterIntersection.z < fGroundZ
			RETURN TRUE
		ENDIF
	ENDIF*/
	
	//FUN FAIR PIER
	IF IS_POINT_IN_ANGLED_AREA(vLocation, <<-1836.619019,-1262.521606,-42.180431>>, <<-1664.868164,-1060.218994,119.500671>>, 150)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Checks if there is a suitable road node in the area and uses that.
FUNC BOOL GET_SUITABLE_ROAD_NODE()
	REQUEST_PATH_NODES_IN_AREA_THIS_FRAME(serverBD.vDestination.x-SAFE_NODE_SEARCH_RADIUS, serverBD.vDestination.y-SAFE_NODE_SEARCH_RADIUS, serverBD.vDestination.x+SAFE_NODE_SEARCH_RADIUS, serverBD.vDestination.y+SAFE_NODE_SEARCH_RADIUS)
	IF ARE_NODES_LOADED_FOR_AREA(serverBD.vDestination.x-SAFE_NODE_SEARCH_RADIUS, serverBD.vDestination.y-SAFE_NODE_SEARCH_RADIUS, serverBD.vDestination.x+SAFE_NODE_SEARCH_RADIUS, serverBD.vDestination.y+SAFE_NODE_SEARCH_RADIUS)
		VECTOR vTemp
		GET_CLOSEST_VEHICLE_NODE(serverBD.vDestination, vTemp)
		IF GET_DISTANCE_BETWEEN_COORDS(serverBD.vDestination, vTemp, FALSE) <= SAFE_NODE_SEARCH_RADIUS
			NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - GET_SUITABLE_ROAD_NODE - serverBD.vDestination = ") NET_PRINT_VECTOR(serverBD.vDestination) NET_NL()
			NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - GET_SUITABLE_ROAD_NODE - FOUND NODE = ") NET_PRINT_VECTOR(vTemp) NET_NL()
			serverBD.vDestination = vTemp
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Controls finding a waypoint vector
PROC FIND_WAYPOINT_VECTOR()//BOOL &bResetMenuNow)
	IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_WaypointVectorFound)
		IF IS_BIT_SET(serverBD.iServerBitSet, biS_WaypointSearchStarted)
					FLOAT fTemp
					serverBD.vDestination.z = (GET_APPROX_FLOOR_FOR_AREA(serverBD.vDestination.x-R2P_DESTINATION_RADIUS, serverBD.vDestination.y-R2P_DESTINATION_RADIUS, serverBD.vDestination.x+R2P_DESTINATION_RADIUS, serverBD.vDestination.y+R2P_DESTINATION_RADIUS) + 5)
					VECTOR vTemp = serverBD.vDestination
					
					SPAWN_SEARCH_PARAMS SpawnSearchParams
					SpawnSearchParams.bPreferPointsCloserToRoads=TRUE
					SpawnSearchParams.fMinDistFromPlayer=0.0
					SpawnSearchParams.bCloseToOriginAsPossible=TRUE
					
					IF (GET_WATER_HEIGHT_NO_WAVES(vTemp, fTemp)	AND NOT IS_BLOCKED_WATER_AREA(vTemp)) //Check if point is on water
					OR GET_SUITABLE_ROAD_NODE()			//Check for Road Node   -		Below (Check for Safe Entity Coords)																//TRUE - NOW DEFUNCT
					OR GET_SAFE_COORDS_IN_AREA_FOR_CREATING_ENTITY(vTemp, SAFE_COORDS_SEARCH_RADIUS, serverBD.vDestination, fTemp, SpawnSearchParams)
//////////			IF SPAWNPOINTS_IS_SEARCH_COMPLETE()
//////////				IF SPAWNPOINTS_GET_NUM_SEARCH_RESULTS() > 0
					//Use spawn point
					//SPAWNPOINTS_GET_SEARCH_RESULT(0, serverBD.vDestination.x, serverBD.vDestination.y, serverBD.vDestination.z)
					
					//serverBD.vDestination.z = (GET_APPROX_FLOOR_FOR_POINT(serverBD.vDestination.x, serverBD.vDestination.y) + 5)
//					serverBD.vDestination.z = (GET_APPROX_FLOOR_FOR_AREA(serverBD.vDestination.x-R2P_DESTINATION_RADIUS, serverBD.vDestination.y-R2P_DESTINATION_RADIUS, serverBD.vDestination.x+R2P_DESTINATION_RADIUS, serverBD.vDestination.y+R2P_DESTINATION_RADIUS) + 5)
					
					DELETE_WAYPOINTS_FROM_THIS_PLAYER()	//SET_NEW_WAYPOINT(serverBD.vDestination.x, serverBD.vDestination.y)
					
					serverBD.vStoredWaypoint = serverBD.vDestination
					NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - WAYPOINT VECTOR FOUND - USING SPAWNPOINTS - serverBD.vDestination = ") NET_PRINT_VECTOR(serverBD.vDestination) NET_NL()
					///CLEAR_WAYPOINT_VECTOR_LOADING()
					//serverBD.iStandardCashReward = CALCULATE_STANDARD_CASH_REWARD()
					bResetMenuNow = TRUE
					SET_BIT(serverBD.iServerBitSet, biS_WaypointVectorFound)
					SET_BIT(serverBD.iServerBitSet, biS_FirstWaypointVectorFound)
					
					serverBD.sSelection.iSelection[ciRP_TYPE_DESTINATION] = 0
					
//////////				ELSE
//////////					//Try to use road nodes if no spawn point has been found
//////////					//IF ARE_NODES_LOADED_FOR_AREA(serverBD.vDestination.x-WAYPOINT_AREA_SEARCH, serverBD.vDestination.y-WAYPOINT_AREA_SEARCH, serverBD.vDestination.x+WAYPOINT_AREA_SEARCH, serverBD.vDestination.y+WAYPOINT_AREA_SEARCH)
//////////					//IF LOAD_ALL_PATH_NODES(TRUE)
//////////						//VECTOR vNode
//////////						//GET_CLOSEST_VEHICLE_NODE(serverBD.vDestination, vNode, NF_INCLUDE_SWITCHED_OFF_NODES)
//////////						//serverBD.vDestination = vNode
//////////						//RELEASE_PATH_NODES()
//////////					//	LOAD_ALL_PATH_NODES(FALSE)
//////////					
//////////						//GetSafeCoord(serverBD.vDestination)
//////////						//FLOAT fHeading
//////////						NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - WAYPOINT VECTOR FOUND - GET VEHICLE NODE - serverBD.vDestination = ") NET_PRINT_VECTOR(serverBD.vDestination) NET_NL()
//////////						//GetNearestSpawnNode(serverBD.vDestination, fHeading, <<0.0, 0.0, 0.0>>, FALSE, FALSE, DESTINATION_MIN_DISTANCE)
//////////						serverBD.vDestination.z = GET_APPROX_FLOOR_FOR_POINT(serverBD.vDestination.x, serverBD.vDestination.y)
//////////						serverBD.vStoredWaypoint = serverBD.vDestination
//////////						NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - WAYPOINT VECTOR FOUND - FOUND VEHICLE NODE - serverBD.vDestination = ") NET_PRINT_VECTOR(serverBD.vDestination) NET_NL()
//////////						
//////////						serverBD.sSelection.iSelection[ciRP_TYPE_DESTINATION] = 0	//GET_R2P_SELECTION_MAX(ciRP_TYPE_DESTINATION)
//////////						MPGlobalsAmbience.R2Pdata.iLastDestination = serverBD.sSelection.iSelection[ciRP_TYPE_DESTINATION]
//////////						
//////////						DELETE_WAYPOINTS_FROM_THIS_PLAYER()	//SET_NEW_WAYPOINT(serverBD.vDestination.x, serverBD.vDestination.y)
//////////						
//////////						///CLEAR_WAYPOINT_VECTOR_LOADING()
//////////						//serverBD.iStandardCashReward = CALCULATE_STANDARD_CASH_REWARD()
//////////						bResetMenuNow = TRUE
//////////						SET_BIT(serverBD.iServerBitSet, biS_WaypointVectorFound)
//////////						SET_BIT(serverBD.iServerBitSet, biS_FirstWaypointVectorFound)
//////////					//ELSE
//////////					//	LOAD_PATH_NODES_IN_AREA(serverBD.vDestination.x-WAYPOINT_AREA_SEARCH, serverBD.vDestination.y-WAYPOINT_AREA_SEARCH, serverBD.vDestination.x+WAYPOINT_AREA_SEARCH, serverBD.vDestination.y+WAYPOINT_AREA_SEARCH)
//////////					//ENDIF
//////////				ENDIF
//////////			ENDIF
    				ENDIF
		ELIF IS_BIT_SET(serverBD.iServerBitSet, biS_GetWaypointVector)
//////////			//Load navmesh
//////////			IF ARE_ALL_NAVMESH_REGIONS_LOADED()
//////////				SPAWNPOINTS_CANCEL_SEARCH()
//////////				SPAWNPOINTS_START_SEARCH(serverBD.vDestination, WAYPOINT_AREA_SEARCH, 1000, SPAWNPOINTS_FLAG_MAY_SPAWN_IN_EXTERIOR|SPAWNPOINTS_FLAG_ALLOW_NOT_NETWORK_SPAWN_CANDIDATE_POLYS|SPAWNPOINTS_FLAG_ALLOW_ROAD_POLYS)
				SET_BIT(serverBD.iServerBitSet, biS_WaypointSearchStarted)
				NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - START LOOKING FOR WAYPOINT SPAWN - SEARCH AT ") NET_PRINT_VECTOR(serverBD.vDestination) NET_NL()
//////////			ENDIF
		ENDIF
	ENDIF
	
	//Reset if waypoint has been removed
	IF NOT IS_WAYPOINT_ACTIVE()
		IF IS_BIT_SET(iBoolsBitSet, biWayPointActive)
			CLEAR_WAYPOINT_VECTOR_LOADING() 
			CLEAR_BIT(iBoolsBitSet, biWayPointActive)
			NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - WAYPOINT NOT ACTIVE     <----------     ") NET_NL()
		ENDIF
	ELSE
		IF NOT IS_BIT_SET(iBoolsBitSet, biWayPointActive)
			VECTOR vWaypoint
			vWaypoint = GET_BLIP_INFO_ID_COORD(GET_FIRST_BLIP_INFO_ID(GET_WAYPOINT_BLIP_ENUM_ID()))
			NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - WAYPOINT ACTIVE - A - vWaypoint = ") NET_PRINT_VECTOR(vWaypoint) NET_NL()
			vWaypoint.z = GET_APPROX_HEIGHT_FOR_AREA(vWaypoint.x-5, vWaypoint.y-5, vWaypoint.x+5, vWaypoint.y+5)
			NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - WAYPOINT ACTIVE - B - vWaypoint = ") NET_PRINT_VECTOR(vWaypoint) NET_NL()
				
			IF GET_DISTANCE_BETWEEN_COORDS(serverBD.vStart, vWaypoint) > DESTINATION_MIN_DISTANCE		//IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vWaypoint, <<DESTINATION_MIN_DISTANCE, DESTINATION_MIN_DISTANCE, DESTINATION_MIN_DISTANCE>>)
				CLEAR_WAYPOINT_VECTOR_LOADING()
//////////				ADD_NAVMESH_REQUIRED_REGION(vWaypoint.x, vWaypoint.y, WAYPOINT_AREA_SEARCH)
				serverBD.vDestination = vWaypoint
				serverBD.vStoredWaypoint = vWaypoint
				SET_MINIMAP_BLOCK_WAYPOINT(TRUE)
				NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - SET_MINIMAP_BLOCK_WAYPOINT - TRUE - B") NET_NL()
				SET_BIT(serverBD.iServerBitSet, biS_GetWaypointVector)
				NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - START LOOKING FOR WAYPOINT VECTOR - B     <----------     ") NET_NL()
				
				SET_BIT(iBoolsBitSet, biWayPointActive)
				bResetMenuNow = TRUE
				NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - WAYPOINT ACTIVE     <----------     ") NET_NL()
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Calculates the distance from the start to the end fo the race
FUNC INT GET_DESTINATION_DISTANCE_FROM_START()	
	IF ARE_VECTORS_EQUAL(GET_DESTINATION_FROM_MENU(), <<0, 0, -2000>>)
	OR ARE_VECTORS_EQUAL(GET_DESTINATION_FROM_MENU(), <<0, 0, 0>>)
		RETURN 0
	ENDIF
	
	RETURN ROUND(GET_DISTANCE_BETWEEN_COORDS(serverBD.vStart, serverBD.vDestination))
ENDFUNC

//PURPOSE: Calculates the distance the player is from the end
FUNC INT GET_DESTINATION_DISTANCE_FROM_PLAYER(PLAYER_INDEX thisPlayer)	
	RETURN ROUND(GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(thisPlayer), serverBD.vDestination))
ENDFUNC

//PURPOSE: Draws a marker to show the invite/start area for the race to point
//PROC DRAW_R2P_START_MARKER()
//	DRAW_MARKER(MARKER_CYLINDER, serverBD.vStart - <<0.0, 0.0, 2.0>>, <<0,0,0>>, <<0,0,0>>, <<R2P_INVITE_RADIUS, R2P_INVITE_RADIUS, 2.5>>,  255, 255, 0, 75) 
//ENDPROC

//BOOL bResetCheckpointWhenInRange

//PURPOSE: Adds the race destination blip and draws a checkpoint
PROC DRAW_AND_BLIP_DESTINATION()//BOOL &bResetMenuNow)
	VECTOR vDrawPosition = serverBD.vDestination
//	FLOAT fReturnZ
	INT iR, iG, iB, iA
	IF NOT IS_BIT_SET(iBoolsBitSet, biReachedEndDestination)
		//Check if it needs updating
		IF DOES_BLIP_EXIST(DestinationBlip)
			IF NOT ARE_VECTORS_EQUAL(vDrawPosition, GET_BLIP_COORDS(DestinationBlip))
				REMOVE_BLIP(DestinationBlip)
				DELETE_CHECKPOINT(DestinationCheckpoint)
				CLEAR_BIT(iBoolsBitSet, biWagerSet)
				
				NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - BLIP AND CHECKPOINT REMOVED - NEEDS UPDATING      <----------     ") NET_NL()
			ENDIF
		ENDIF
		
		//Add Blip and Checkpoint
		IF NOT DOES_BLIP_EXIST(DestinationBlip)
			IF NOT ARE_VECTORS_EQUAL(vDrawPosition, <<0, 0, 0>>)
			AND NOT ARE_VECTORS_EQUAL(vDrawPosition, <<0, 0, -2000>>)
			AND (NOT IS_BIT_SET(serverBD.iServerBitSet, biS_GetWaypointVector) OR IS_BIT_SET(serverBD.iServerBitSet, biS_WaypointVectorFound) )
				
				GET_HUD_COLOUR(HUD_COLOUR_YELLOW, iR, iG, iB, iA)
				
				DestinationBlip = ADD_BLIP_FOR_COORD(vDrawPosition)
				SET_BLIP_SPRITE(DestinationBlip, RADAR_TRACE_RACEFLAG)
				SET_BLIP_SCALE(DestinationBlip, BLIP_SIZE_NETWORK_CHECKPOINT)
				SET_BLIP_PRIORITY(DestinationBlip, BLIPPRIORITY_MED)
				SET_BLIP_NAME_FROM_TEXT_FILE(DestinationBlip, "R2P_BLIP")
				//SET_IGNORE_NO_GPS_FLAG(TRUE)
				SET_IGNORE_NO_GPS_FLAG_UNTIL_FIRST_NORMAL_NODE(TRUE)
				SET_BLIP_ROUTE(DestinationBlip, TRUE)
				
				DestinationCheckpoint = CREATE_CHECKPOINT(CHECKPOINT_RACE_GROUND_FLAG, vDrawPosition+<<0.0, 0.0, 4.0>>, vDrawPosition, 10.0/*1.3*/ ,iR, iG, iB, 75)
				SET_CHECKPOINT_CYLINDER_HEIGHT(DestinationCheckpoint, 7.5, 7.5, 100)
				
				bResetMenuNow = TRUE
				
				NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - BLIP ADDED AT ") NET_PRINT_VECTOR(vDrawPosition) NET_NL()
			ENDIF
		ELSE
//			IF NOT bResetCheckpointWhenInRange
//				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vDrawPosition) <= 100.0
//					GET_GROUND_Z_FOR_3D_COORD(vDrawPosition+<<0.0, 0.0, 20.0>>, fReturnZ)
//					vDrawPosition.z = fReturnZ
//					GET_HUD_COLOUR(HUD_COLOUR_YELLOW, iR, iG, iB, iA)
//					
//					bResetCheckpointWhenInRange = TRUE
//					NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - CHECKPOINT ADDED      <----------     ") NET_NL()
//				ENDIF
//			ENDIF
		ENDIF
	ELSE
		IF DOES_BLIP_EXIST(DestinationBlip)
			REMOVE_BLIP(DestinationBlip)
			DELETE_CHECKPOINT(DestinationCheckpoint)
			NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - BLIP AND CHECKPOINT REMOVED - RACE ENDED      <----------     ") NET_NL()
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Adds a blip to show the area that the Destination can't be in
PROC ADD_MIN_DISTANCE_RADIUS_BLIP()
	IF NOT DOES_BLIP_EXIST(RadiusBlip)
		RadiusBlip = ADD_BLIP_FOR_RADIUS(serverBD.vStart, DESTINATION_MIN_DISTANCE)
		SET_BLIP_COLOUR(RadiusBlip, BLIP_COLOUR_RED)
		SET_BLIP_ALPHA(RadiusBlip, 220)
		SET_BLIP_NAME_FROM_TEXT_FILE(RadiusBlip, "R2P_BLIPR")
		SET_BLIP_DISPLAY(RadiusBlip, DISPLAY_MAP)
		NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - ADD_MIN_DISTANCE_RADIUS_BLIP") NET_NL()
	ENDIF
ENDPROC

//PURPOSE: Removes the radius blip
PROC REMOVE_MIN_DISTANCE_RADIUS_BLIP()
	IF DOES_BLIP_EXIST(RadiusBlip)
		REMOVE_BLIP(RadiusBlip)
		NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - REMOVE_MIN_DISTANCE_RADIUS_BLIP") NET_NL()
	ENDIF
ENDPROC

//PURPOSE: Adds the race destination blip and draws a checkpoint
//PROC DRAW_DISTANCE_TO_END()
//	IF NOT IS_BIT_SET(iBoolsBitSet, biReachedEndDestination)
//		INT iDistance = (GET_DESTINATION_DISTANCE_FROM_PLAYER(PLAYER_ID())-ROUND(R2P_DESTINATION_RADIUS))
//		IF iDistance < 0
//			iDistance = 0
//		ENDIF
//		DRAW_GENERIC_SCORE(iDistance, "R2P_DISTT", -1, HUD_COLOUR_WHITE, HUDORDER_DONTCARE, FALSE, "R2P_DISTN")
//	ENDIF
//ENDPROC

//PURPOSE: Checks if the player has reached the End Checkpoint and sends an Event
PROC PROCESS_REACHING_DESTINATION()
	IF NOT IS_BIT_SET(iBoolsBitSet, biReachedEndDestination)
		IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), serverBD.vDestination+<<0, 0, (R2P_DESTINATION_RADIUS_HEIGHT/2)+4>>, <<R2P_DESTINATION_RADIUS, R2P_DESTINATION_RADIUS, R2P_DESTINATION_RADIUS_HEIGHT>>)
			IF OKAY_TO_SEND_REACHED_DESTINATION_EVENT(PLAYER_PED_ID())
				iReachedDestinationTime = GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), serverBD.StartTime)	//GET_CLOUD_TIME_AS_INT()
				BROADCAST_RACE_TO_POINT_REACHED_DESTINATION(iReachedDestinationTime, SPECIFIC_PLAYER(NETWORK_GET_PLAYER_INDEX(NETWORK_GET_HOST_OF_THIS_SCRIPT())))
				SET_BIT(iBoolsBitSet, biReachedEndDestination)
				NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - PROCESS_REACHING_DESTINATION - REACHED DESTINATION - SENT EVENT - TIME = ") NET_PRINT_INT(iReachedDestinationTime) NET_NL()
			ENDIF
		ENDIF
	ELSE
		IF HAS_NET_TIMER_EXPIRED(iResendEventTimer, RESEND_EVENT_DELAY)
			BROADCAST_RACE_TO_POINT_REACHED_DESTINATION(iReachedDestinationTime, SPECIFIC_PLAYER(NETWORK_GET_PLAYER_INDEX(NETWORK_GET_HOST_OF_THIS_SCRIPT())))
			RESET_NET_TIMER(iResendEventTimer)
			NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - PROCESS_REACHING_DESTINATION - REACHED DESTINATION - RESENT EVENT - TIME = ") NET_PRINT_INT(iReachedDestinationTime) NET_NL()
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Retursn TRUE if controls are being used for something else
FUNC BOOL SHOULD_R2P_CONTROLS_EXIT()//BOOL bAllowMenu)
	IF IS_PAUSE_MENU_ACTIVE()
		RETURN TRUE
	ENDIF
	IF IS_SCRIPT_HUD_DISPLAYING (HUDPART_TRANSITIONHUD)
		RETURN TRUE
	ENDIF
	IF IS_PHONE_ONSCREEN()
		RETURN TRUE
	ENDIF
	/*IF IS_LOCAL_PLAYER_VIEWING_BETTING()
		RETURN TRUE
	ENDIF*/
	/*IF bAllowMenu = FALSE
		RETURN TRUE
	ENDIF*/
	RETURN FALSE
ENDFUNC

//PURPOSE: Controls the player exiting the menu
PROC MAINTAIN_EXIT_RACE_TO_POINT()
	IF SHOULD_R2P_CONTROLS_EXIT()//bAllowMenu)
		EXIT
	ENDIF
	
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_RELOAD)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
	//IF IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_RELOAD)
	//OR IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
	IF IS_CELLPHONE_CANCEL_JUST_PRESSED_EXCLUDING_MOUSE() //IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CELLPHONE_CANCEL)
	OR IS_MENU_CURSOR_CANCEL_PRESSED()
		
		SWITCH iSubMenu
			CASE SUB_MENU_NONE
				MPGlobals.PlayerInteractionData.bBlockCinematicCam = TRUE
				playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END
				NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - MAINTAIN_EXIT_RACE_TO_POINT - playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END 5    <----------     ") NET_NL()
				#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_END 5")	#ENDIF
				
				//Make Serever Open Up the Interaction Menu
				IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
					//PLAY_SOUND_FRONTEND(-1, "BACK", "HUD_FRONTEND_DEFAULT_SOUNDSET")
					MPGlobals.PlayerInteractionData.iTargetPlayerInt = NATIVE_TO_INT(PLAYER_ID())
					MPGlobals.PlayerInteractionData.iLaunchStage = 0
					MPGlobals.PlayerInteractionData.bOpenOnImpromptuRace = TRUE
					NET_PRINT_TIME() NET_PRINT("     ---------->     FRIEND REQUEST - REOPEN FROM IMPROMPTU RACE -  Target Player = ") NET_PRINT(GET_PLAYER_NAME(PLAYER_ID())) NET_NL()
				ELSE
					PLAY_SOUND_FRONTEND(-1, "CANCEL", "HUD_FRONTEND_DEFAULT_SOUNDSET")
				ENDIF
			BREAK
			
			CASE SUB_MENU_INVITE_PLAYER_LIST
				PLAY_SOUND_FRONTEND(-1, "BACK", "HUD_FRONTEND_DEFAULT_SOUNDSET")
				iSubMenu = SUB_MENU_NONE
				StartMenuData.iCurrentSelection = ciRP_TYPE_SEND_INVITE_2
				bResetMenuNow = TRUE
				NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - MAINTAIN_EXIT_RACE_TO_POINT - iSubMenu = SUB_MENU_NONE    <----------     ") NET_NL()
			BREAK
		ENDSWITCH
	ENDIF 
ENDPROC

FUNC BOOL IS_PLAYER_ON_AGGRESSIVE_FM_EVENT(INT iPlayer)
	SWITCH GlobalplayerBD_FM_3[iPlayer].iFmAmbientEventType
		CASE FMMC_TYPE_MOVING_TARGET	
		CASE FMMC_TYPE_HOLD_THE_WHEEL	
		CASE FMMC_TYPE_HOT_PROPERTY	
		CASE FMMC_TYPE_DEAD_DROP	
		CASE FMMC_TYPE_KING_CASTLE	
//		CASE FMMC_TYPE_INFECTION	
		CASE FMMC_TYPE_KILL_LIST	
		CASE FMMC_TYPE_CRIMINAL_DAMAGE
		CASE FMMC_TYPE_BUSINESS_BATTLES
		CASE FMMC_TYPE_SUPER_BUSINESS_BATTLES
			RETURN  TRUE
	ENDSWITCH 
	
	IF GET_FREEMODE_CONTENT_TYPE(GlobalplayerBD_FM_3[iPlayer].iFmAmbientEventType) = eFMCONTENTTYPE_FREEMODE_EVENT
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_VALID_FOR_LIST(PLAYER_INDEX playerID)
	
	IF NOT IS_NET_PLAYER_OK(playerID, FALSE)
		RETURN FALSE
	ENDIF
	
	IF playerID = PLAYER_ID() 
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_SCTV(playerID)
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_IN_MP_PROPERTY(playerID, FALSE)
		RETURN FALSE
	ENDIF
	
	IF NOT NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID() , playerID)
		RETURN FALSE
	ENDIF
	
	IF GET_PLAYER_TEAM(PLAYER_ID()) != GET_PLAYER_TEAM(playerID)
		IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType != FMMC_TYPE_MISSION
			RETURN FALSE
		ELSE
			IF NOT DOES_TEAM_LIKE_TEAM(GET_PLAYER_TEAM(PLAYER_ID()), GET_PLAYER_TEAM(playerID))
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	//IF NOT IS_BIT_SET(iBoolsBitSet, biFirstPlayerListLoopDone)
		IF IS_NET_PLAYER_OK(playerID)
		AND IS_NET_PLAYER_OK(PLAYER_ID())
			IF NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), GET_PLAYER_PED(playerID), <<R2P_INVITE_RADIUS, R2P_INVITE_RADIUS, R2P_INVITE_RADIUS>>)
				RETURN FALSE
			ENDIF
		ELSE
			RETURN FALSE
		ENDIF
	//ENDIF
	
	IF IS_PLAYER_ON_AGGRESSIVE_FM_EVENT(NATIVE_TO_INT(playerID))
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

// PURPOSE: Returns the total players within the passed range
FUNC INT GET_NUM_PLAYERS_IN_RANGE()
	INT iCount
	INT iPlayer
	
	REPEAT NUM_NETWORK_PLAYERS iPlayer
		PLAYER_INDEX playerId = INT_TO_PLAYERINDEX(iPlayer)
		IF IS_NET_PLAYER_OK(PLAYER_ID())
			IF IS_PLAYER_VALID_FOR_LIST(playerId)
				iCount++
			ENDIF
		ENDIF
	ENDREPEAT
	
	NET_PRINT("     ---------->     RACE TO POINT - GET_NUM_PLAYERS_IN_RANGE - iCount = ") NET_PRINT_INT(iCount) NET_NL()
	RETURN iCount
ENDFUNC

//PURPOSE: Control sending an invite
PROC MAINTAIN_SENDING_RACE_TO_POINT_INVITE()//BOOL &bResetMenuNow)
	//IF IS_BIT_SET(StartMenuData.iBitSet, biM_InviteSent)
	
	//Reset Menu once timer is up
	IF IS_BIT_SET(StartMenuData.iBitSet, biM_InviteSent)
		IF HAS_NET_TIMER_EXPIRED(iInviteTimer, iInviteTimerDelay)
			bResetMenuNow = TRUE
			CLEAR_BIT(StartMenuData.iBitSet, biM_InviteSent)
		ENDIF
	ENDIF
	
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
		EXIT
	ENDIF
	
	IF SHOULD_R2P_CONTROLS_EXIT()//bAllowMenu)
		EXIT
	ENDIF
	
	//IF HAS_NET_TIMER_EXPIRED(iInviteTimer, iInviteTimerDelay)
		IF IS_CELLPHONE_ACCEPT_JUST_PRESSED_EXCLUDING_MOUSE() //IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CELLPHONE_SELECT)	//IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_JUMP)
		OR bMouseAccept
			
			SWITCH iSubMenu
				CASE SUB_MENU_NONE 
					IF bSelectAvailable = TRUE
						IF HAS_NET_TIMER_EXPIRED(iInviteTimer, iInviteTimerDelay)
							IF StartMenuData.iCurrentSelection = ciRP_TYPE_SEND_INVITE_2
								PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
								iSubMenu = SUB_MENU_INVITE_PLAYER_LIST
								StartMenuData.iCurrentSelection = 0
								bResetMenuNow = TRUE
								NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - MAINTAIN_SENDING_RACE_TO_POINT_INVITE - iSubMenu = SUB_MENU_INVITE_PLAYER_LIST    <----------     ") NET_NL()
								EXIT
							ELIF StartMenuData.iCurrentSelection != ciRP_TYPE_SEND_INVITE
								EXIT
							ENDIF
							
							PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
						
							NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - MAINTAIN_SENDING_RACE_TO_POINT_INVITE - 'Your invite has been sent'    <----------     ") NET_NL()

							BROADCAST_RACE_TO_POINT_INVITE(serverBD.vStart, serverBD.vDestination, tlDestination, NETWORK_GET_INSTANCE_ID_OF_THIS_SCRIPT(), GET_PLAYERS_TO_INVITE_BITSET(GET_PLAYER_TEAM(PLAYER_ID()), R2P_INVITE_RADIUS))
							NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - Destination = ") NET_PRINT(tlDestination) NET_PRINT(" vStart = ") NET_PRINT_VECTOR(serverBD.vStart) NET_PRINT(" vDestination = ") NET_PRINT_VECTOR(serverBD.vDestination) NET_NL()
						
							INT iNumPlayers
							iNumPlayers = GET_NUM_PLAYERS_IN_RANGE()
							IF iNumPlayers != 1
								PRINT_TICKER_WITH_INT("R2P_TINVS", iNumPlayers)	//Your invite has been sent to ~1~ players.
							ELSE
								PRINT_TICKER("R2P_TINVS1")	//Your invite has been sent to 1 player.
							ENDIF
							
							RESET_NET_TIMER(iInviteTimer)
							START_NET_TIMER(iInviteTimer)
							iInviteTimerDelay = 5000
							
							bResetMenuNow = TRUE
							SET_BIT(StartMenuData.iBitSet, biM_InviteSent)
							SET_BIT(serverBD.iServerBitSet, biS_LockDestination)
						ENDIF
					ELSE
						NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - MAINTAIN_SENDING_RACE_TO_POINT_INVITE - bSelectAvailable = FALSE - A") NET_NL()
					ENDIF
				BREAK
				
				CASE SUB_MENU_INVITE_PLAYER_LIST
					IF bSelectAvailable = TRUE
						PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
								
						PRINT_TICKER_WITH_PLAYER_NAME("R2P_TINVP", storePlayer)	//Your invite has been sent to ~a~.
						BROADCAST_RACE_TO_POINT_INVITE(serverBD.vStart, serverBD.vDestination, tlDestination, NETWORK_GET_INSTANCE_ID_OF_THIS_SCRIPT(), SPECIFIC_PLAYER(storePlayer))
						NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - Destination = ") NET_PRINT(tlDestination) NET_PRINT(" vStart = ") NET_PRINT_VECTOR(serverBD.vStart) NET_PRINT(" vDestination = ") NET_PRINT_VECTOR(serverBD.vDestination) NET_NL()
						
						SET_BIT(serverBD.iServerBitSet, biS_LockDestination)
						NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - MAINTAIN_SENDING_RACE_TO_POINT_INVITE - INVITE SENT TO ") NET_PRINT(GET_PLAYER_NAME(storePlayer)) NET_NL()
					ELSE
						NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - MAINTAIN_SENDING_RACE_TO_POINT_INVITE - bSelectAvailable = FALSE - B") NET_NL()
					ENDIF
				BREAK
			ENDSWITCH
		//ENDIF
	ENDIF
ENDPROC

//PURPOSE: Setup the Race to Point menu
PROC SETUP_R2P_MENU(BOOL bHostOfScript)
	NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - SETUP_R2P_MENU      <----------     ") NET_NL()
	
	CLEAR_MENU_DATA()
	
	//SET_MENU_ITEM_TOGGLEABLE(FALSE, TRUE)
	
	//Set title
	IF bHostOfScript
		SET_MENU_TITLE("R2P_MENU_S")
	ELSE
		SET_MENU_TITLE("R2P_MENU")
	ENDIF
	//Set Structure
	SET_MENU_ITEM_LAYOUT(MENU_ITEM_TEXT, MENU_ITEM_TEXT)
	SET_MENU_ITEM_JUSTIFICATION(FONT_LEFT, FONT_RIGHT)
	
	/*INT iAlpha = ROUND(255*0.75)
	SET_MENU_HEADER_COLOUR(0, 71, 133, 255, TRUE)		//47, 88, 109, 255
	SET_MENU_BODY_COLOUR(0, 71, 133, iAlpha, TRUE)		//47, 88, 109
	SET_MENU_FOOTER_COLOUR(0, 71, 133, iAlpha, TRUE)	//47, 88, 109
	SET_MENU_HELP_COLOUR(0, 71, 133, iAlpha, TRUE)		//47, 88, 109*/
	/*INT iHudR, iHudG, iHudB, iHudA
	GET_HUD_COLOUR(HUD_COLOUR_FREEMODE, iHudR, iHudG, iHudB, iHudA)
	SET_MENU_HEADER_COLOUR(iHudR, iHudG, iHudB, iHudA, TRUE)
	SET_MENU_FOOTER_COLOUR(iHudR, iHudG, iHudB, iHudA, TRUE)	//47, 88, 109*/
	
	INT iHudR, iHudG, iHudB, iHudA
	GET_HUD_COLOUR(HUD_COLOUR_FREEMODE, iHudR, iHudG, iHudB, iHudA)
	SET_MENU_HEADER_TEXT_COLOUR(iHudR, iHudG, iHudB, iHudA, TRUE)
	
	BOOL bAvailable = FALSE
	BOOL bUseArrows = FALSE
	
	//Get Destination Text Label
	bDoWaypointHelp = FALSE
	StartMenuData.tl15 = "R2P_MENU_DEST"
	StartMenuData.tl15B = "R2P_MENU_DE"
	IF serverBD.sSelection.iSelection[ciRP_TYPE_DESTINATION] <= 0
	OR serverBD.sSelection.iSelection[ciRP_TYPE_DESTINATION] > ciRP_MAX_TYPE_DESTINATION
		serverBD.sSelection.iSelection[ciRP_TYPE_DESTINATION] = 0
		IF NOT bHostOfScript
			StartMenuData.tl15 = "R2P_MENU_DE0"
		ELIF IS_WAYPOINT_ACTIVE() //SET_WAYPOINT_OFF()
		OR IS_BIT_SET(serverBD.iServerBitSet, biS_FirstWaypointVectorFound)
			IF ARE_VECTORS_EQUAL(GET_DESTINATION_FROM_MENU(), <<0, 0, -2000>>)
			OR ARE_VECTORS_EQUAL(GET_DESTINATION_FROM_MENU(), <<0, 0, 0>>)
			OR GET_DISTANCE_BETWEEN_COORDS(serverBD.vStart, GET_DESTINATION_FROM_MENU()) < DESTINATION_MIN_DISTANCE //IS_ENTITY_AT_COORD(PLAYER_PED_ID(), GET_DESTINATION_FROM_MENU(), <<DESTINATION_MIN_DISTANCE, DESTINATION_MIN_DISTANCE, DESTINATION_MIN_DISTANCE>>)
				bDoWaypointHelp = TRUE
				StartMenuData.tl15B = "R2P_MENU_DE0S"
				NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - DESTINATION TEXT - R2P_MENU_DE0S - A") NET_NL()
			ELSE
				StartMenuData.tl15 = "R2P_MENU_DE0"
				NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - DESTINATION TEXT - R2P_MENU_DE0 - B") NET_NL()
			ENDIF
		ELSE
			bDoWaypointHelp = TRUE
			StartMenuData.tl15B = "R2P_MENU_DE0S"
			NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - DESTINATION TEXT - R2P_MENU_DE0S - C") NET_NL()
		ENDIF
	ELSE	//ELIF serverBD.sSelection.iSelection[ciRP_TYPE_DESTINATION] > 0
		StartMenuData.tl15 = "R2P_MENU_DE"
		StartMenuData.tl15 += serverBD.sSelection.iSelection[ciRP_TYPE_DESTINATION]
		StartMenuData.tl15 += "F"
		
		IF serverBD.sSelection.iSelection[ciRP_TYPE_DESTINATION] = 2
			IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_MISSION
			OR IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet6, BI_FM_NMH6_GOTO_GERALD_LTS)
				StartMenuData.tl15 = "R2P_MENU_DE99F"
			ENDIF
		ENDIF
	ENDIF
	tlDestination = StartMenuData.tl15
	
	//Host only has Invite Columns
	IF bHostOfScript
		IF bDoWaypointHelp = FALSE
			bAvailable = HAS_NET_TIMER_EXPIRED(iInviteTimer, iInviteTimerDelay)
		ENDIF
		
		bSelectAvailable = bAvailable
		
		//Send an invite to All Players
		//ADD_MENU_ITEM_ICON(ciTN_TYPE_SEND_INVITE, MENU_ICON_DUMMY)
		//ADD_MENU_ITEM_TEXT(ciRP_TYPE_SEND_INVITE, "R2P_MENU_INT", 0, TRUE)		
		ADD_MENU_ITEM_TEXT(ciRP_TYPE_SEND_INVITE, "R2P_MENU_IN", 0, bAvailable)
		
		//Open Player List	
		//ADD_MENU_ITEM_TEXT(ciRP_TYPE_SEND_INVITE_2, "R2P_MENU_INT", 0, TRUE)
		ADD_MENU_ITEM_TEXT(ciRP_TYPE_SEND_INVITE_2, "R2P_MENU_IN2", 0, bAvailable)
	ENDIF
	
	//Destination
	bAvailable = bHostOfScript
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_LockDestination)
		bAvailable = FALSE
	ENDIF
	bUseArrows = bAvailable
	ADD_MENU_ITEM_TEXT(ciRP_TYPE_DESTINATION, "R2P_MENU_DEST", 0,  TRUE)
	
	IF ARE_STRINGS_EQUAL(StartMenuData.tl15B, "R2P_MENU_DE0S")
		ADD_MENU_ITEM_TEXT(ciRP_TYPE_DESTINATION, StartMenuData.tl15B, 0, bAvailable)
	ELSE
		ADD_MENU_ITEM_TEXT(ciRP_TYPE_DESTINATION, StartMenuData.tl15B, 2, bAvailable)
		ADD_MENU_ITEM_TEXT_COMPONENT_STRING(StartMenuData.tl15)
		ADD_MENU_ITEM_TEXT_COMPONENT_INT(GET_DESTINATION_DISTANCE_FROM_START())
	ENDIF
	
	//Destination Distance
	//ADD_MENU_ITEM_TEXT(ciRP_TYPE_DISTANCE, "R2P_MENU_DIS", 0,  TRUE)		
	//StartMenuData.tl15 = "R2P_MENU_MET"
	//ADD_MENU_ITEM_TEXT(ciRP_TYPE_DISTANCE, StartMenuData.tl15, 1, FALSE)
	//ADD_MENU_ITEM_TEXT_COMPONENT_INT(GET_DESTINATION_DISTANCE_FROM_START())
	
	//Standard Cash Reward
	ADD_MENU_ITEM_TEXT(ciRP_TYPE_STANDARD_CASH, "R2P_MENU_SCT", 0,  TRUE)		
	StartMenuData.tl15 = "R2P_MENU_SC"
	//StartMenuData.tl15 += serverBD.sSelection.iSelection[ciRP_TYPE_STANDARD_CASH]
	ADD_MENU_ITEM_TEXT(ciRP_TYPE_STANDARD_CASH, StartMenuData.tl15, 1, FALSE)
	IF serverBD.iTotalWager >= R2P_MIN_WAGER
		ADD_MENU_ITEM_TEXT_COMPONENT_INT(serverBD.iTotalWager) //Needs to be iTotalWager-playerBD[PARTICIPANT_ID_TO_INT()].iWager	//CALCULATE_STANDARD_CASH_REWARD())
	ELSE
		ADD_MENU_ITEM_TEXT_COMPONENT_INT(R2P_MIN_WAGER)
	ENDIF
	
	//Wager Cash
	/*ADD_MENU_ITEM_TEXT(ciRP_TYPE_WAGER_CASH, "R2P_MENU_BCT", 0,  TRUE)		
	StartMenuData.tl15 = "R2P_MENU_BC"
	//StartMenuData.tl15 += serverBD.sSelection.iSelection[ciRP_TYPE_WAGER_CASH]
	ADD_MENU_ITEM_TEXT(ciRP_TYPE_WAGER_CASH, StartMenuData.tl15, 1, TRUE)
	ADD_MENU_ITEM_TEXT_COMPONENT_INT(GET_WAGER_AMOUNT())*/
	
	
	//Start
	IF bHostOfScript
		bAvailable = TRUE
		IF serverBD.iNumParticpants >= 2
		#IF IS_DEBUG_BUILD OR bRunWithOnePlayer #ENDIF
			IF (serverBD.sSelection.iSelection[ciRP_TYPE_DESTINATION] <= 0 OR serverBD.sSelection.iSelection[ciRP_TYPE_DESTINATION] > ciRP_MAX_TYPE_DESTINATION)
			AND NOT IS_WAYPOINT_ACTIVE()
			AND NOT IS_BIT_SET(serverBD.iServerBitSet, biS_FirstWaypointVectorFound)
				bAvailable = FALSE
				//ADD_MENU_ITEM_TEXT(ciRP_TYPE_START, "R2P_MENU_STU", 1, FALSE)
			ELSE
				IF ARE_VECTORS_EQUAL(GET_DESTINATION_FROM_MENU(), <<0, 0, -2000>>)
				OR ARE_VECTORS_EQUAL(GET_DESTINATION_FROM_MENU(), <<0, 0, 0>>)
				OR GET_DISTANCE_BETWEEN_COORDS(serverBD.vStart, GET_DESTINATION_FROM_MENU()) < DESTINATION_MIN_DISTANCE //IS_ENTITY_AT_COORD(PLAYER_PED_ID(), GET_DESTINATION_FROM_MENU(), <<DESTINATION_MIN_DISTANCE, DESTINATION_MIN_DISTANCE, DESTINATION_MIN_DISTANCE>>)
					bAvailable = FALSE
					//ADD_MENU_ITEM_TEXT(ciRP_TYPE_START, "R2P_MENU_STU", 1, FALSE)
				//ELSE
				//	ADD_MENU_ITEM_TEXT(ciRP_TYPE_START, "R2P_MENU_STA", 1, bHostOfScript)
				ENDIF
			ENDIF
		ELSE
			bAvailable = FALSE
			//ADD_MENU_ITEM_TEXT(ciRP_TYPE_START, "R2P_MENU_STU", 1, FALSE)
		ENDIF
		ADD_MENU_ITEM_TEXT(ciRP_TYPE_START, "R2P_MENU_STT", 0,  bAvailable)
	ENDIF
	
	//Controls	
	IF bHostOfScript
		
		IF StartMenuData.iCurrentSelection = ciRP_TYPE_START
			IF serverBD.iNumParticpants >= 2
			#IF IS_DEBUG_BUILD OR bRunWithOnePlayer #ENDIF
				IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_GetWaypointVector)
				OR IS_BIT_SET(serverBD.iServerBitSet, biS_WaypointVectorFound)
					IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
						ADD_MENU_HELP_KEY_CLICKABLE(INPUT_CURSOR_ACCEPT, "R2P_MENU_LAU")	//START
					ELSE
						ADD_MENU_HELP_KEY_INPUT( INPUT_CELLPHONE_SELECT, "R2P_MENU_LAU")	//START
					ENDIF
				ENDIF
			ENDIF

		ELIF StartMenuData.iCurrentSelection = ciRP_TYPE_SEND_INVITE
			IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
				ADD_MENU_HELP_KEY_CLICKABLE(INPUT_CURSOR_ACCEPT, "R2P_CTRL_INV")			//INVITE
			ELSE
				ADD_MENU_HELP_KEY_INPUT( INPUT_CELLPHONE_SELECT, "R2P_CTRL_INV")			//INVITE
			ENDIF
		
		ELIF StartMenuData.iCurrentSelection = ciRP_TYPE_SEND_INVITE_2
			IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
				ADD_MENU_HELP_KEY_CLICKABLE(INPUT_CURSOR_ACCEPT, "R2P_CTRL_SEL")			//SELECT
			ELSE
				ADD_MENU_HELP_KEY_INPUT( INPUT_CELLPHONE_SELECT, "R2P_CTRL_SEL")			//SELECT
			ENDIF
		
		ELIF StartMenuData.iCurrentSelection = ciRP_TYPE_DESTINATION	
			IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
				ADD_MENU_HELP_KEY_CLICKABLE(INPUT_CURSOR_ACCEPT, "R2P_CTRL_NXT")			//NEXT
			ELSE
				ADD_MENU_HELP_KEY_INPUT( INPUT_CELLPHONE_SELECT, "R2P_CTRL_NXT")			//NEXT
			ENDIF
		ENDIF
		
		IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
			ADD_MENU_HELP_KEY_CLICKABLE(INPUT_CURSOR_CANCEL, "R2P_MENU_EXI")				//EXIT
			ADD_MENU_HELP_KEY_INPUT(INPUT_FRONTEND_PAUSE, "SPEC_PAUSE")						//PAUSE
		ELSE
			ADD_MENU_HELP_KEY_INPUT( INPUT_CELLPHONE_CANCEL, "R2P_MENU_EXI")				//EXIT
		ENDIF

		//ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_Y, "R2P_CTRL_RST")						//RESET\
		
		//IF StartMenuData.iCurrentSelection = ciRP_TYPE_DESTINATION
		//	ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_FRONTEND_DPAD_LR, "R2P_MENU_ADJ")	//ADJUST
		//ENDIF
		//ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_FRONTEND_DPAD_UD, "R2P_MENU_MOV")		//SCROLL
		
		//Control Arrows
		IF StartMenuData.iCurrentSelection = ciRP_TYPE_DESTINATION
		AND bUseArrows = TRUE
			SET_MENU_ITEM_TOGGLEABLE(FALSE, TRUE)
		ELSE
			SET_MENU_ITEM_TOGGLEABLE(FALSE, FALSE)
		ENDIF

	ELSE

		//IF StartMenuData.iCurrentSelection = ciRP_TYPE_SEND_INVITE
		//	ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_ACCEPT, "R2P_CTRL_INV")			//INVITE
		//ENDIF
		IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
			ADD_MENU_HELP_KEY_CLICKABLE(INPUT_CURSOR_CANCEL, "R2P_MENU_EXI")																//EXIT
		ELSE
			ADD_MENU_HELP_KEY_INPUT( INPUT_CELLPHONE_CANCEL, "R2P_MENU_EXI")			//EXIT
		ENDIF
		
	ENDIF
	
	//ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_FRONTEND_DPAD_ALL, "R2P_MENU_MOV")				//MENU
	/*IF IS_BETTING_AVAILABLE_AND_ACCESSIBLE()
		ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_LB, "R2P_MENU_BET")					//BETTING
	ENDIF*/
	
	IF bHostOfScript
		SET_CURRENT_MENU_ITEM(StartMenuData.iCurrentSelection)
	ELSE
		SET_CURRENT_MENU_ITEM(-1)
	ENDIF
	
	SET_CURRENT_MENU_ITEM_DESCRIPTION("") // Pass in "" to clear
ENDPROC

//PURPOSE: Draws a list of players who can be invited to the Race to Point
PROC SETUP_R2P_INVITE_PLAYER_LIST()
	NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - SETUP_R2P_INVITE_PLAYER_LIST      <----------     ") NET_NL()
	
	CLEAR_MENU_DATA()
	
	SET_MENU_ITEM_TOGGLEABLE(FALSE)
	
	//Set title
	SET_MENU_TITLE("R2P_MENU_TINV")	//Player List
	
	//Set Structure
	SET_MENU_ITEM_LAYOUT(MENU_ITEM_TEXT, MENU_ITEM_TEXT)
	SET_MENU_ITEM_JUSTIFICATION(FONT_LEFT, FONT_RIGHT)
	
	/*INT iAlpha = ROUND(255*0.75)
	SET_MENU_HEADER_COLOUR(47, 88, 109, 255, TRUE)
	SET_MENU_BODY_COLOUR(47, 88, 109, iAlpha, TRUE)
	SET_MENU_FOOTER_COLOUR(47, 88, 109, iAlpha, TRUE)
	SET_MENU_HELP_COLOUR(47, 88, 109, iAlpha, TRUE)*/
	
	
	
	//PLAYER + COUNTER
	INT i
	//INT iPlayerAddedCounter
	INT iRow = 0
	storePlayer = INVALID_PLAYER_INDEX()
	PLAYER_INDEX playerID
	//GAMER_HANDLE emptyGH
	//storeGamerHandle = emptyGH
	//storeIndex = -1
	
	PLAYER_INDEX FoundPlayer[NUM_NETWORK_PLAYERS]
	FLOAT fFoundPlayerDistance[NUM_NETWORK_PLAYERS]
//	PLAYER_INDEX SwappingPlayer
//	FLOAT SwappingPlayerDistance
	PED_INDEX PlayerPedId
	
	//SESSION PLAYERS
	REPEAT NUM_NETWORK_PLAYERS i
		//storePlayer = playerList[iPlayerAddedCounter]
		playerID = INT_TO_PLAYERINDEX(i)
		IF IS_PLAYER_VALID_FOR_LIST(playerID)
			IF iRow < NUM_NETWORK_REAL_PLAYERS()
				
				//Store selected player
				/*IF StartMenuData.iCurrentSelection = iRow	//iPlayerAddedCounter	//+1??
					storePlayer = playerID
				ENDIF*/
				
				//iRow = iPlayerAddedCounter
				iRow++
				
				//ADD_MENU_ITEM_TEXT(iRow, "PIM_PLNM", 1, TRUE)
				//ADD_MENU_ITEM_TEXT_COMPONENT_PLAYER_NAME(GET_PLAYER_NAME(playerID))
				
				bSelectAvailable = TRUE
				//iPlayerAddedCounter++
				
				PlayerPedId = GET_PLAYER_PED(PlayerId)
					
				//Loop to see who is closest
				IF iRow > 1
					INT k
					FLOAT fDistance = GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), PlayerPedId)
					
					REPEAT iRow k
						IF fDistance < fFoundPlayerDistance[k]
						OR fFoundPlayerDistance[k] = 0
							
							INT p
							FOR p = (iRow-1) TO (k+1) STEP -1
								IF p > 0
									FoundPlayer[p] = FoundPlayer[p-1]
									fFoundPlayerDistance[p] = fFoundPlayerDistance[p-1]
								ENDIF
							ENDFOR
							
							FoundPlayer[k] = playerID
							fFoundPlayerDistance[k] = GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), PlayerPedId)
							
							k = iRow
						ENDIF
					ENDREPEAT
					
				ELSE
					FoundPlayer[0] = playerID
					fFoundPlayerDistance[0] = GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), PlayerPedId)
					//PRINTLN(" -->>-->> PIM - CHECK 0 - FIRST - PlayerName = ", GET_PLAYER_NAME(FoundPlayer[0]))
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF iRow = 0
		ADD_MENU_ITEM_TEXT(iRow, "PIM_R2PNON", 1, TRUE)	//No Players in Range
		bSelectAvailable = FALSE
		iRow++
	ELSE
		//Loop to Display Peds
		REPEAT iRow i
			ADD_MENU_ITEM_TEXT(i, "PIM_PLNM", 1, TRUE)
			ADD_MENU_ITEM_TEXT_COMPONENT_PLAYER_NAME(GET_PLAYER_NAME(FoundPlayer[i]), FALSE, TRUE)
			IF StartMenuData.iCurrentSelection = i
				storePlayer = FoundPlayer[i]
			ENDIF
		ENDREPEAT
	ENDIF
	
	INVITE_PLAYER_LIST_MAX = iRow	//(iRow+1)
	
	IF bSelectAvailable = TRUE
		IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
			ADD_MENU_HELP_KEY_CLICKABLE(INPUT_CURSOR_ACCEPT, "R2P_CTRL_INV")	//INVITE
		ELSE
			ADD_MENU_HELP_KEY_INPUT(INPUT_CELLPHONE_SELECT, "R2P_CTRL_INV")		//INVITE
		ENDIF
	ENDIF
	
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		ADD_MENU_HELP_KEY_CLICKABLE(INPUT_CURSOR_CANCEL, "R2P_MENU_BAC")		//BACK
	ELSE
		ADD_MENU_HELP_KEY_INPUT( INPUT_CELLPHONE_CANCEL, "R2P_MENU_BAC")		//BACK
	ENDIF
	
	//IF iRow > 1
	//	ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_FRONTEND_DPAD_UD, "R2P_MENU_MOV")		//SCROLL
	//ENDIF
	
	SET_CURRENT_MENU_ITEM(StartMenuData.iCurrentSelection)
	
	SET_CURRENT_MENU_ITEM_DESCRIPTION("")

ENDPROC


//PURPOSE: Resets options
PROC CLIENT_SET_R2P_OPTIONS_TO_DEFAULT()
	INT i
	REPEAT ciRP_TYPE_MAX i
		StartMenuData.iSelection[i] = 0
	ENDREPEAT
ENDPROC

////PURPOSE: Resets server menu items
//PROC SET_SERVER_MENU_ITEMS_TO_DEFAULT()
//	INT i
//	REPEAT ciRP_TYPE_MAX i
//		serverBD.sSelection.iSelection[i] = 0
//	ENDREPEAT
//ENDPROC

//PURPOSE: Controls movement of the menu
FUNC BOOL PROCESS_MENU_MOVEMENT(BOOL bHostOfScript)//, BOOL &bResetMenuNow)
	
	INT i
	IF SHOULD_R2P_CONTROLS_EXIT()//bAllowMenu)
		RETURN FALSE
	ENDIF
	
	//INT iInviteCONST_INT = 0	//GET_INVITE_CONST_INT(StartMenuData, bAmIAloudToVote)
	
	//Move Up and Down
	IF bHostOfScript
		IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CELLPHONE_DOWN)
		OR IS_MENU_CURSOR_SCROLL_DOWN_PRESSED(FALSE, TRUE)
			IF HAS_NET_TIMER_EXPIRED(iMenuMoveTimer, iMenuMoveTimerDelay)
				PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", "HUD_FRONTEND_DEFAULT_SOUNDSET")
				//IF StartMenuData.iCurrentSelection = iInviteCONST_INT
					bResetMenuNow = TRUE	
				//ENDIF
				StartMenuData.iCurrentSelection++
				
				IF iSubMenu = SUB_MENU_INVITE_PLAYER_LIST
					IF StartMenuData.iCurrentSelection > (INVITE_PLAYER_LIST_MAX -1)	
						StartMenuData.iCurrentSelection = 0
					ENDIF
				ELSE
					IF StartMenuData.iCurrentSelection > (ciRP_TYPE_MAX -1)	
						StartMenuData.iCurrentSelection = 0
					ENDIF
				ENDIF
				
				SET_CURRENT_MENU_ITEM(StartMenuData.iCurrentSelection)
				//IF StartMenuData.iCurrentSelection = iInviteCONST_INT
					bResetMenuNow = TRUE	
				//ENDIF
				iMenuMoveTimerDelay = MENU_MOVE_DELAY
				RESET_NET_TIMER(iMenuMoveTimer)
				RETURN TRUE
			ENDIF
			
		ELIF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CELLPHONE_UP)
		OR IS_MENU_CURSOR_SCROLL_UP_PRESSED(FALSE, TRUE)
			IF HAS_NET_TIMER_EXPIRED(iMenuMoveTimer, iMenuMoveTimerDelay)
				PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", "HUD_FRONTEND_DEFAULT_SOUNDSET")
				//IF StartMenuData.iCurrentSelection = iInviteCONST_INT
				bResetMenuNow = TRUE	
				//ENDIF
				StartMenuData.iCurrentSelection--
				
				IF iSubMenu = SUB_MENU_INVITE_PLAYER_LIST
					IF StartMenuData.iCurrentSelection < 0
						StartMenuData.iCurrentSelection = (INVITE_PLAYER_LIST_MAX -1)
					ENDIF
				ELSE
					IF StartMenuData.iCurrentSelection < 0
						StartMenuData.iCurrentSelection = (ciRP_TYPE_MAX -1)
					ENDIF
				ENDIF
				
				SET_CURRENT_MENU_ITEM(StartMenuData.iCurrentSelection)		
				//IF StartMenuData.iCurrentSelection = iInviteCONST_INT
					bResetMenuNow = TRUE	
				//ENDIF
				iMenuMoveTimerDelay = MENU_MOVE_DELAY
				RESET_NET_TIMER(iMenuMoveTimer)
				RETURN TRUE
			ENDIF
			
		ELSE
			IF iMenuMoveTimerDelay != -1
				iMenuMoveTimerDelay = -1
			ENDIF
		ENDIF
	ENDIF
	
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HEADLIGHT)
	//Select with Left and Right
	/*IF StartMenuData.iCurrentSelection = ciRP_TYPE_STANDARD_CASH
		//Nobody can alter this
	ELIF StartMenuData.iCurrentSelection = ciRP_TYPE_WAGER_CASH		//Can be altered by all players
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CELLPHONE_LEFT)
			StartMenuData.iSelection[StartMenuData.iCurrentSelection]--
			IF StartMenuData.iSelection[StartMenuData.iCurrentSelection] < 0
				StartMenuData.iSelection[StartMenuData.iCurrentSelection] = GET_R2P_SELECTION_MAX(StartMenuData.iCurrentSelection)
			ENDIF
			//Check they have the Cash
			INT iPlayerCash = GET_PLAYER_CASH(PLAYER_ID())
			IF GET_WAGER_AMOUNT() > iPlayerCash
				StartMenuData.iSelection[StartMenuData.iCurrentSelection] = FLOOR(TO_FLOAT(iPlayerCash)/WAGER_INCREMENT)
				NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - WAGER LEFT - PLAYER DOESN'T HAVE ENOUGH CASH      <----------     ") NET_NL()
			ENDIF
			MPGlobalsAmbience.R2Pdata.iLastWager = StartMenuData.iSelection[StartMenuData.iCurrentSelection]
			bResetMenuNow = TRUE
			NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - WAGER LEFT      <----------     ") NET_NL()
			RETURN TRUE
		ENDIF
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CELLPHONE_RIGHT)
			StartMenuData.iSelection[StartMenuData.iCurrentSelection]++
			INT iPlayerCash = GET_PLAYER_CASH(PLAYER_ID())
			IF StartMenuData.iSelection[StartMenuData.iCurrentSelection] > GET_R2P_SELECTION_MAX(StartMenuData.iCurrentSelection)
			OR GET_WAGER_AMOUNT() > iPlayerCash
				StartMenuData.iSelection[StartMenuData.iCurrentSelection] = 0
			ENDIF
			MPGlobalsAmbience.R2Pdata.iLastWager = StartMenuData.iSelection[StartMenuData.iCurrentSelection]
			NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - MPGlobalsAmbience.R2Pdata.iLastWager = ") NET_PRINT_INT(MPGlobalsAmbience.R2Pdata.iLastWager) NET_NL()
			bResetMenuNow = TRUE
			NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - WAGER RIGHT      <----------     ") NET_NL()
			RETURN TRUE
		ENDIF
	EL*/
	
	IF  bHostOfScript	//The rest can only be altered by the Host
	AND iSubMenu = SUB_MENU_NONE
	AND StartMenuData.iCurrentSelection = ciRP_TYPE_DESTINATION
	AND NOT IS_BIT_SET(serverBD.iServerBitSet, biS_LockDestination)
		
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CELLPHONE_LEFT)
		OR IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CELLPHONE_LEFT)
		OR iMouseDestCycle = -1
		
			IF HAS_NET_TIMER_EXPIRED(iMenuMoveTimerLR, iMenuMoveTimerDelayLR)
				NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - INPUT_CELLPHONE_LEFT  - iCurrentSelection = ") NET_PRINT_INT(StartMenuData.iCurrentSelection) NET_NL()
				NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - INPUT_CELLPHONE_LEFT  - OLD Selection = ") NET_PRINT_INT(serverBD.sSelection.iSelection[StartMenuData.iCurrentSelection]) NET_NL()
				PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
				serverBD.sSelection.iSelection[StartMenuData.iCurrentSelection]--
				NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - INPUT_CELLPHONE_LEFT  - NEW Selection = ") NET_PRINT_INT(serverBD.sSelection.iSelection[StartMenuData.iCurrentSelection]) NET_NL()
				IF serverBD.sSelection.iSelection[StartMenuData.iCurrentSelection] < 0
					serverBD.sSelection.iSelection[StartMenuData.iCurrentSelection] = GET_R2P_SELECTION_MAX(StartMenuData.iCurrentSelection)
					NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - INPUT_CELLPHONE_LEFT  - BELOW 0 - Selection = ") NET_PRINT_INT(serverBD.sSelection.iSelection[StartMenuData.iCurrentSelection]) NET_NL()
				ENDIF
				
				//Check that Destination isn't too close by
				REPEAT ciRP_MAX_TYPE_DESTINATION i
					IF StartMenuData.iCurrentSelection = ciRP_TYPE_DESTINATION
						IF GET_DISTANCE_BETWEEN_COORDS(serverBD.vStart, GET_DESTINATION_FROM_MENU()) < DESTINATION_MIN_DISTANCE //IS_ENTITY_AT_COORD(PLAYER_PED_ID(), GET_DESTINATION_FROM_MENU(), <<DESTINATION_MIN_DISTANCE, DESTINATION_MIN_DISTANCE, DESTINATION_MIN_DISTANCE>>)
						AND serverBD.sSelection.iSelection[ciRP_TYPE_DESTINATION] != 0
							NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - INPUT_CELLPHONE_LEFT - PLAYER TOO CLOSE TO THIS DESTINATION ") NET_PRINT_INT(serverBD.sSelection.iSelection[StartMenuData.iCurrentSelection]) NET_PRINT(" VECTOR = ")NET_PRINT_VECTOR(GET_DESTINATION_FROM_MENU())NET_NL()
							serverBD.sSelection.iSelection[StartMenuData.iCurrentSelection]--
							IF serverBD.sSelection.iSelection[StartMenuData.iCurrentSelection] < 0
								serverBD.sSelection.iSelection[StartMenuData.iCurrentSelection] = GET_R2P_SELECTION_MAX(StartMenuData.iCurrentSelection)
							ENDIF
						ELSE
							i = 99
							NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - INPUT_CELLPHONE_LEFT - DESTINATION OKAY ") NET_PRINT_INT(serverBD.sSelection.iSelection[StartMenuData.iCurrentSelection]) NET_PRINT(" VECTOR = ")NET_PRINT_VECTOR(GET_DESTINATION_FROM_MENU())NET_NL()
						ENDIF
						MPGlobalsAmbience.R2Pdata.iLastDestination = serverBD.sSelection.iSelection[StartMenuData.iCurrentSelection]
					ENDIF
				ENDREPEAT
				
				
				iMenuMoveTimerDelayLR = MENU_MOVE_DELAY
				RESET_NET_TIMER(iMenuMoveTimerLR)
				
				bResetMenuNow = TRUE
				bChangedDestination = TRUE
				serverBD.sSelection.iSelectionChangeID++
				RETURN TRUE
			ENDIF
		
		ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CELLPHONE_RIGHT)
		OR   IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CELLPHONE_RIGHT)
//		OR   (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CELLPHONE_SELECT) AND StartMenuData.iCurrentSelection = ciRP_TYPE_DESTINATION)
//		OR   (IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CELLPHONE_SELECT) AND StartMenuData.iCurrentSelection = ciRP_TYPE_DESTINATION)
		OR   (IS_CELLPHONE_ACCEPT_PRESSED_EXCLUDING_MOUSE() AND StartMenuData.iCurrentSelection = ciRP_TYPE_DESTINATION)
		OR   iMouseDestCycle = 1

			IF HAS_NET_TIMER_EXPIRED(iMenuMoveTimerLR, iMenuMoveTimerDelayLR)
				NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - INPUT_CELLPHONE_RIGHT  - iCurrentSelection = ") NET_PRINT_INT(StartMenuData.iCurrentSelection) NET_NL()
				NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - INPUT_CELLPHONE_RIGHT  - OLD Selection = ") NET_PRINT_INT(serverBD.sSelection.iSelection[StartMenuData.iCurrentSelection]) NET_NL()
				PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
				serverBD.sSelection.iSelection[StartMenuData.iCurrentSelection]++
				NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - INPUT_CELLPHONE_RIGHT  - NEW Selection = ") NET_PRINT_INT(serverBD.sSelection.iSelection[StartMenuData.iCurrentSelection]) NET_NL()
				IF serverBD.sSelection.iSelection[StartMenuData.iCurrentSelection] > GET_R2P_SELECTION_MAX(StartMenuData.iCurrentSelection)
					serverBD.sSelection.iSelection[StartMenuData.iCurrentSelection] = 0
					NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - INPUT_CELLPHONE_RIGHT  - ABOVE MAX - Selection = ") NET_PRINT_INT(serverBD.sSelection.iSelection[StartMenuData.iCurrentSelection]) NET_NL()
				ENDIF
				
				//Check that Destination isn't too close by
				REPEAT ciRP_MAX_TYPE_DESTINATION i
					IF StartMenuData.iCurrentSelection = ciRP_TYPE_DESTINATION
						IF GET_DISTANCE_BETWEEN_COORDS(serverBD.vStart, GET_DESTINATION_FROM_MENU()) < DESTINATION_MIN_DISTANCE //IS_ENTITY_AT_COORD(PLAYER_PED_ID(), GET_DESTINATION_FROM_MENU(), <<DESTINATION_MIN_DISTANCE, DESTINATION_MIN_DISTANCE, DESTINATION_MIN_DISTANCE>>)
						AND serverBD.sSelection.iSelection[ciRP_TYPE_DESTINATION] != 0
							NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - INPUT_CELLPHONE_RIGHT - PLAYER TOO CLOSE TO THIS DESTINATION ") NET_PRINT_INT(serverBD.sSelection.iSelection[StartMenuData.iCurrentSelection]) NET_PRINT(" VECTOR = ")NET_PRINT_VECTOR(GET_DESTINATION_FROM_MENU())NET_NL()
							serverBD.sSelection.iSelection[StartMenuData.iCurrentSelection]++
							IF serverBD.sSelection.iSelection[StartMenuData.iCurrentSelection] > GET_R2P_SELECTION_MAX(StartMenuData.iCurrentSelection)
								serverBD.sSelection.iSelection[StartMenuData.iCurrentSelection] = 0
							ENDIF
						ELSE
							i = 99
							NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - INPUT_CELLPHONE_RIGHT - DESTINATION OKAY ") NET_PRINT_INT(serverBD.sSelection.iSelection[StartMenuData.iCurrentSelection]) NET_PRINT(" VECTOR = ")NET_PRINT_VECTOR(GET_DESTINATION_FROM_MENU())NET_NL()
						ENDIF
						MPGlobalsAmbience.R2Pdata.iLastDestination = serverBD.sSelection.iSelection[StartMenuData.iCurrentSelection]
					ENDIF
				ENDREPEAT
				
				iMenuMoveTimerDelayLR = MENU_MOVE_DELAY
				RESET_NET_TIMER(iMenuMoveTimerLR)
				
				bResetMenuNow = TRUE
				bChangedDestination = TRUE
				serverBD.sSelection.iSelectionChangeID++
				RETURN TRUE
			ENDIF
			
		ELSE
			
			IF iMenuMoveTimerDelayLR != -1
				iMenuMoveTimerDelayLR = -1
			ENDIF
			
		ENDIF
	ENDIF
	
	/*DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
	//IF IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_ENTER)
	//OR IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_EXIT)
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_Y)
		PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
		IF bHostOfScript = TRUE
			CLIENT_SET_R2P_OPTIONS_TO_DEFAULT()
			SET_SERVER_MENU_ITEMS_TO_DEFAULT()
			serverBD.sSelection.iSelectionChangeID++
		ELSE
			CLIENT_SET_R2P_OPTIONS_TO_DEFAULT()
		ENDIF
		MPGlobalsAmbience.R2Pdata.iLastDestination = 0
		//MPGlobalsAmbience.R2Pdata.iLastWager = 0
		bResetMenuNow = TRUE
		RETURN TRUE
	ENDIF*/
	RETURN TRUE
ENDFUNC

//PURPOSE: Check to see if the Host 
FUNC BOOL HAS_RACE_BEEN_STARTED()
	IF SHOULD_R2P_CONTROLS_EXIT()
		RETURN FALSE
	ENDIF
	
	//IF StartMenuData.iCurrentSelection = ciRP_TYPE_SEND_INVITE
	IF StartMenuData.iCurrentSelection != ciRP_TYPE_START
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_GetWaypointVector)
	AND NOT IS_BIT_SET(serverBD.iServerBitSet, biS_WaypointVectorFound)
		RETURN FALSE
	ENDIF
	
	IF GET_DISTANCE_BETWEEN_COORDS(serverBD.vStart, GET_DESTINATION_FROM_MENU()) < DESTINATION_MIN_DISTANCE //IS_ENTITY_AT_COORD(PLAYER_PED_ID(), GET_DESTINATION_FROM_MENU(), <<DESTINATION_MIN_DISTANCE, DESTINATION_MIN_DISTANCE, DESTINATION_MIN_DISTANCE>>)
		NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - HAS_RACE_BEEN_STARTED = FALSE - TOO CLOSE     <----------     ") NET_NL()
		RETURN FALSE
	ENDIF
	
	IF ARE_VECTORS_EQUAL(GET_DESTINATION_FROM_MENU(), <<0, 0, 0>>)
	OR ARE_VECTORS_EQUAL(GET_DESTINATION_FROM_MENU(), <<0, 0, -2000>>)
		NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - HAS_RACE_BEEN_STARTED = FALSE - NOT VALID     <----------     ") NET_NL()
		RETURN FALSE
	ENDIF
	
	IF IS_CELLPHONE_ACCEPT_JUST_PRESSED_EXCLUDING_MOUSE()
	OR bMouseAccept

		IF serverBD.iNumParticpants >= 2
		#IF IS_DEBUG_BUILD OR bRunWithOnePlayer #ENDIF
			IF (serverBD.sSelection.iSelection[ciRP_TYPE_DESTINATION] <= 0 OR serverBD.sSelection.iSelection[ciRP_TYPE_DESTINATION] > ciRP_MAX_TYPE_DESTINATION)
			AND NOT IS_WAYPOINT_ACTIVE()
			AND NOT IS_BIT_SET(serverBD.iServerBitSet, biS_FirstWaypointVectorFound)
				NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - WAYPOINT NOT SAFE = FALSE     <----------     ") NET_NL()
				RETURN FALSE
			ELSE
				PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
				NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - HAS_RACE_BEEN_STARTED = TRUE     <----------     ") NET_NL()
				RETURN TRUE
			ENDIF
		ELSE
			NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - NOT ENOUGH PARTICIPANTS TO START     <----------     ") NET_NL()
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Draws and controls the Race to point Menu
PROC PROCESS_RACE_TO_POINT_MENU()
	
	//IF NOT IS_LOCAL_PLAYER_VIEWING_BETTING() 
		IF LOAD_MENU_ASSETS()
			//Should the menu be reset
			//BOOL bResetMenuNow
			
			/*IF IS_BETTING_AVAILABLE_AND_ACCESSIBLE()
				IF NOT IS_BIT_SET(iBoolsBitSet, biBettingAvailable)
					bResetMenuNow = TRUE
					SET_BIT(iBoolsBitSet, biBettingAvailable)
				ENDIF
			ELSE
				IF IS_BIT_SET(iBoolsBitSet, biBettingAvailable)
					CLEAR_BIT(iBoolsBitSet, biBettingAvailable)
				ENDIF
			ENDIF*/
			
			BOOL bHostOfScript
			IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
				bHostOfScript = TRUE
			ELSE
				//Reset Menu if Total Wager has changed
				IF serverBD.iTotalWager != iStoredWager
					iStoredWager = serverBD.iTotalWager
					bResetMenuNow = TRUE
					NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - TOTAL WAGER HAS CHANGED - RESET MENU     <----------     ") NET_NL()
				ENDIF
			ENDIF
			
			// Check mouse input
			// (NOTE - bMouseAccept and iMouseDestCycle get reset at the start of every frame, then set here is the button is pressed.)
			// (This is processed as part of the client state loop, the server state loop can also use the result, as it is processed after the client loop)
			IF bHostOfScript
				IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
					
					SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
					SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_CURSOR_CANCEL)
					
					SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_UP)
					SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_DOWN)
				
					SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_CURSOR_X)
					SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_CURSOR_Y)
				
					HANDLE_MENU_CURSOR(FALSE)
					HANDLE_CURSOR_HIGHLIGHTED_MENU_ITEMS()
					
					IF g_iMenuCursorItem > MENU_CURSOR_NO_ITEM
					
						IF StartMenuData.iCurrentSelection = g_iMenuCursorItem

							IF g_iMenuCursorItem = ciRP_TYPE_DESTINATION
							AND IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
								iMouseDestCycle = GET_CURSOR_MENU_ITEM_VALUE_CHANGE()
							ELSE
								IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
									bMouseAccept = TRUE
								ENDIF
							ENDIF
							
						ELSE
							
							IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
							
								PLAY_SOUND_FRONTEND(-1,"NAV_UP_DOWN","HUD_FRONTEND_DEFAULT_SOUNDSET")

								StartMenuData.iCurrentSelection = g_iMenuCursorItem
								SET_CURRENT_MENU_ITEM(StartMenuData.iCurrentSelection)
								bResetMenuNow = TRUE
								
							ENDIF
							
						ENDIF
						
					ENDIF
					
				ENDIF
			ENDIF

			MAINTAIN_EXIT_RACE_TO_POINT()
			MAINTAIN_SENDING_RACE_TO_POINT_INVITE()//bResetMenuNow)
			
			//To check if the menu should be reset if I've become the host or I'm hosting.
			IF bHostOfScript
				IF NOT IS_BIT_SET(StartMenuData.iMenuBitSet, biM_InChargeOfMenu)
					SET_BIT(StartMenuData.iMenuBitSet, biM_InChargeOfMenu)
					NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - NEW HOST - I'm now in charge, set a bit and reset the menu     <----------     ") NET_NL()
					bResetMenuNow = TRUE
					bChangedDestination = TRUE
					CLEAR_BIT(serverBD.iServerBitSet, biS_GetWaypointVector)
					CLEAR_BIT(serverBD.iServerBitSet, biS_WaypointSearchStarted)
					CLEAR_BIT(serverBD.iServerBitSet, biS_WaypointVectorFound)
					SPAWNPOINTS_CANCEL_SEARCH()
					SET_MINIMAP_BLOCK_WAYPOINT(FALSE)
					NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - SET_MINIMAP_BLOCK_WAYPOINT - FALSE - C") NET_NL()
				ENDIF
			ELSE
				IF IS_BIT_SET(StartMenuData.iMenuBitSet, biM_InChargeOfMenu)
					CLEAR_BIT(StartMenuData.iMenuBitSet, biM_InChargeOfMenu)
					NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - NEW HOST - I'm not in charge of this menu, so reset it     <----------     ") NET_NL()
					bResetMenuNow = TRUE
					bChangedDestination = TRUE
				ENDIF		
			ENDIF
			
			//Check if we should reset because the num of participants has changed
			IF serverBD.iNumParticpants != iNumParticipantsStored
				NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - bResetMenuNow - NUM PARTICIPANTS HAS CHANGED - STORED = ") NET_PRINT_INT(iNumParticipantsStored) NET_PRINT(" NEW = ") NET_PRINT_INT(serverBD.iNumParticpants)NET_NL()
				iNumParticipantsStored = serverBD.iNumParticpants
				bResetMenuNow = TRUE
			ENDIF
			
			//If the menu has changed or needs set up for the first time then do so.
			IF PROCESS_MENU_MOVEMENT(bHostOfScript)//, bResetMenuNow)
			OR NOT IS_BIT_SET(StartMenuData.iMenuBitSet, biM_MenuSetup)
			OR HAVE_CONTROLS_CHANGED(FRONTEND_CONTROL)
				
				//If the menu has not been set up then set it up 
				IF NOT IS_BIT_SET(StartMenuData.iMenuBitSet, biM_MenuSetup)
					IF bHostOfScript = TRUE
						CLIENT_SET_R2P_OPTIONS_TO_DEFAULT()	//SET_UP_MENU_ITEM_TO_DEFAULTS(StartMenuData, sSelection, bHostOfScript)
						IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType != FMMC_TYPE_MISSION
						AND NOT IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet6, BI_FM_NMH6_GOTO_GERALD_LTS)
							serverBD.sSelection.iSelection[ciRP_TYPE_DESTINATION] = MPGlobalsAmbience.R2Pdata.iLastDestination
						ELSE
							serverBD.sSelection.iSelection[ciRP_TYPE_DESTINATION] = 2
						ENDIF
					ENDIF
					//StartMenuData.iSelection[ciRP_TYPE_WAGER_CASH] = MPGlobalsAmbience.R2Pdata.iLastWager
				ENDIF
				
				//If the menu needs to reset/redraw then call this
				IF bResetMenuNow = TRUE
				OR NOT IS_BIT_SET(StartMenuData.iMenuBitSet, biM_MenuSetup)
				OR HAVE_CONTROLS_CHANGED(FRONTEND_CONTROL)
				OR StartMenuData.iServerIdOld != serverBD.sSelection.iSelectionChangeID
					StartMenuData.iServerIdOld = serverBD.sSelection.iSelectionChangeID
					
					//SET_GLOBALS_TO_MENU_OPTIONS(StartMenuData.iMenuType, sSelection)
					IF bHostOfScript
						//Make sure initial destination is suitable
						INT i
						REPEAT ciRP_MAX_TYPE_DESTINATION i
							IF GET_DISTANCE_BETWEEN_COORDS(serverBD.vStart, GET_DESTINATION_FROM_MENU()) < DESTINATION_MIN_DISTANCE //IS_ENTITY_AT_COORD(PLAYER_PED_ID(), GET_DESTINATION_FROM_MENU(), <<DESTINATION_MIN_DISTANCE, DESTINATION_MIN_DISTANCE, DESTINATION_MIN_DISTANCE>>)
							AND serverBD.sSelection.iSelection[ciRP_TYPE_DESTINATION] != 0
								NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - bResetMenuNow - PLAYER TOO CLOSE TO THIS DESTINATION ") NET_PRINT_INT(serverBD.sSelection.iSelection[StartMenuData.iCurrentSelection]) NET_PRINT(" VECTOR = ")NET_PRINT_VECTOR(GET_DESTINATION_FROM_MENU())NET_NL()
								NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - bResetMenuNow - OLD serverBD.sSelection.iSelection[ciRP_TYPE_DESTINATION] = ") NET_PRINT_INT(serverBD.sSelection.iSelection[ciRP_TYPE_DESTINATION])NET_NL()
								serverBD.sSelection.iSelection[ciRP_TYPE_DESTINATION]++
								IF serverBD.sSelection.iSelection[ciRP_TYPE_DESTINATION] > GET_R2P_SELECTION_MAX(ciRP_TYPE_DESTINATION)
									serverBD.sSelection.iSelection[ciRP_TYPE_DESTINATION] = 0
								ENDIF
								NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - bResetMenuNow - NEW serverBD.sSelection.iSelection[ciRP_TYPE_DESTINATION] = ") NET_PRINT_INT(serverBD.sSelection.iSelection[ciRP_TYPE_DESTINATION])NET_NL()
							ELSE
								i=99
							ENDIF
						ENDREPEAT
						
						//Double Check
						/*IF serverBD.sSelection.iSelection[ciRP_TYPE_DESTINATION] > GET_R2P_SELECTION_MAX(ciRP_TYPE_DESTINATION)
							serverBD.sSelection.iSelection[ciRP_TYPE_DESTINATION] = 0
							NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - bResetMenuNow - SAFETY CHECK A - serverBD.sSelection.iSelection[ciRP_TYPE_DESTINATION] = ") NET_PRINT_INT(serverBD.sSelection.iSelection[ciRP_TYPE_DESTINATION])NET_NL()
						ELIF serverBD.sSelection.iSelection[ciRP_TYPE_DESTINATION] < 0
							serverBD.sSelection.iSelection[ciRP_TYPE_DESTINATION] = GET_R2P_SELECTION_MAX(ciRP_TYPE_DESTINATION)
							NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - bResetMenuNow - SAFETY CHECK B - serverBD.sSelection.iSelection[ciRP_TYPE_DESTINATION] = ") NET_PRINT_INT(serverBD.sSelection.iSelection[ciRP_TYPE_DESTINATION])NET_NL()
						ENDIF*/
						IF bChangedDestination = TRUE
							serverBD.vDestination = GET_DESTINATION_FROM_MENU()
							NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - serverBD.vDestination = ") NET_PRINT_VECTOR(serverBD.vDestination) NET_NL()
							//serverBD.iStandardCashReward = CALCULATE_STANDARD_CASH_REWARD()
							//NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - serverBD.iStandardCashReward = ") NET_PRINT_INT(serverBD.iStandardCashReward) NET_NL()
							bChangedDestination = FALSE
						ENDIF
					ENDIF
					
					/*NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - X - MPGlobalsAmbience.R2Pdata.iLastWager = ") NET_PRINT_INT(MPGlobalsAmbience.R2Pdata.iLastWager) NET_NL()
					//Check they have the Cash
					INT iPlayerCash = GET_PLAYER_CASH(PLAYER_ID())
					IF GET_WAGER_AMOUNT() > iPlayerCash
						StartMenuData.iSelection[StartMenuData.iCurrentSelection] = FLOOR(TO_FLOAT(iPlayerCash)/WAGER_INCREMENT)
						NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - WAGER LEFT - PLAYER DOESN'T HAVE ENOUGH CASH 1      <----------     ") NET_NL()
					ENDIF
					playerBD[PARTICIPANT_ID_TO_INT()].iCashBet = GET_WAGER_AMOUNT()
					NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - playerBD[PARTICIPANT_ID_TO_INT()].iCashBet = ") NET_PRINT_INT(playerBD[PARTICIPANT_ID_TO_INT()].iCashBet) NET_NL()
					*/
					
					//SET_LOCAL_PLAYER_MENU_OPTIONS(sSelection, StartMenuData, bHostOfScript)
					
					IF iSubMenu = SUB_MENU_INVITE_PLAYER_LIST
					AND bHostOfScript = TRUE
						SETUP_R2P_INVITE_PLAYER_LIST()
					ELSE
						SETUP_R2P_MENU(bHostOfScript)
					ENDIF
					
					bResetMenuNow = FALSE
				ENDIF
				
				IF NOT IS_BIT_SET(StartMenuData.iMenuBitSet, biM_MenuSetup)
					SET_USER_RADIO_CONTROL_ENABLED(FALSE)
					//Set the menu set up bit
					SET_BIT(StartMenuData.iMenuBitSet, biM_MenuSetup)
				ENDIF
			ENDIF
			
			//Help under menu
			IF HAS_NET_TIMER_STARTED(iWarningHelpTimer)
				SET_CURRENT_MENU_ITEM_DESCRIPTION("R2P_WARNH")	//~s~You are leaving the Race to Point start location.
			ELSE
				IF bHostOfScript
					IF iSubMenu = SUB_MENU_INVITE_PLAYER_LIST
						IF bSelectAvailable = TRUE
							SET_CURRENT_MENU_ITEM_DESCRIPTION("R2P_MENU_IVP")	//Send an invite to the selected player.
						ENDIF
					ELIF (serverBD.sSelection.iSelection[ciRP_TYPE_DESTINATION] <= 0 OR serverBD.sSelection.iSelection[ciRP_TYPE_DESTINATION] > ciRP_MAX_TYPE_DESTINATION)
					//AND NOT IS_WAYPOINT_ACTIVE()
					AND bDoWaypointHelp = TRUE
						SET_CURRENT_MENU_ITEM_DESCRIPTION("R2P_MENU_WAY")	//Go to the map ~INPUT_CELLPHONE_PAUSE~ and add a waypoint ~INPUT_CELLPHONE_SELECT~ to use this destination.
					ELIF serverBD.iNumParticpants < 2
						SET_CURRENT_MENU_ITEM_DESCRIPTION("R2P_MENU_MPL")	//A race to point requires at least two players.
					ELSE
						SET_CURRENT_MENU_ITEM_DESCRIPTION("R2P_MENU_DSQ")	//You will be disqualified if you move too far during the countdown.
					ENDIF
				ELSE
					SET_CURRENT_MENU_ITEM_DESCRIPTION("R2P_MENU_WAI")		//Waiting for ~a~ ~s~to set the options. You will be disqualified if you move too far during the countdown.
					ADD_CURRENT_MENU_ITEM_DESCRIPTION_PLAYER_NAME(GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(NETWORK_GET_HOST_OF_THIS_SCRIPT())))
				ENDIF
			ENDIF
			
			IF bHostOfScript = TRUE
				//REQUEST_PATH_NODES_IN_AREA_THIS_FRAME(-3500, -3700, 4200, 7500)
				IF (serverBD.sSelection.iSelection[ciRP_TYPE_DESTINATION] <= 0 OR serverBD.sSelection.iSelection[ciRP_TYPE_DESTINATION] > ciRP_MAX_TYPE_DESTINATION)
					FIND_WAYPOINT_VECTOR()//bResetMenuNow)
				ENDIF
			ENDIF
			
			/*SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_CELLPHONE_SELECT)
			SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_CELLPHONE_CANCEL)
			SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_CELLPHONE_UP)
			SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_CELLPHONE_DOWN)
			SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_CELLPHONE_LEFT)
			SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_CELLPHONE_RIGHT)*/
			
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_UNARMED)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_MELEE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_HANDGUN)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_SHOTGUN)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_SMG)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_AUTO_RIFLE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_SNIPER)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_HEAVY)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_SPECIAL)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_NEXT)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_PREV)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_SPECIAL)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_SPECIAL_TWO)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_BLOCK)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DETONATE)
			//DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PICKUP)
			//DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK)
			//DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2)
			//DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_AIM)
			//DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PASSENGER_ATTACK)
			//DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_NEXT_WEAPON)
			//DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_SELECT_NEXT_WEAPON)
			//DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
			//DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_TALK)
			//DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HEADLIGHT)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_RADIO_WHEEL)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_DUCK)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CONTEXT)
			
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_RELOAD)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DIVE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
		
			HIDE_HELP_TEXT_THIS_FRAME()
			DISABLE_DPADDOWN_THIS_FRAME()
			DRAW_PIM_MENU_HEADER(VHeaderData)
			DRAW_MENU()
			DRAW_PIM_GLARE(GlareData)
		ENDIF
	//ELSE
	//	IF bResetMenuNow = FALSE
	//		bResetMenuNow = TRUE
	//	ENDIF
	//ENDIF
ENDPROC

////** TWH - RLB - Added this for B* 1425701. Ryan, if there is a better way for me to globally tell if the player is in the impromptu race menu then fire in a bug to me with details and I'll change it.
//PROC MAINTAIN_IN_IMPROMPTU_RACE_SUB_MENU_GLOBAL_FLAG()
//	MPGlobalsAmbience.bInInterMenuInpromptuRaceSubMenu = (playerBD[PARTICIPANT_ID_TO_INT()].eRaceStage = eR2P_SETUP)
//ENDPROC

//PURPOSE: Process the RACE TO POINT stages for the Client
PROC PROCESS_RACE_TO_POINT_CLIENT()
	
	//playerBD[PARTICIPANT_ID_TO_INT()].eRaceStage = serverBD.eRaceStage
	
//	MAINTAIN_IN_IMPROMPTU_RACE_SUB_MENU_GLOBAL_FLAG()
	
	SWITCH playerBD[PARTICIPANT_ID_TO_INT()].eRaceStage
		CASE eR2P_SETUP
			DISABLE_CELLPHONE_THIS_FRAME_ONLY()
			//DRAW_R2P_START_MARKER()
			ADD_MIN_DISTANCE_RADIUS_BLIP()
			DRAW_AND_BLIP_DESTINATION()
			REQUEST_MINIGAME_COUNTDOWN_UI(Race_CountDownUI)
			SEND_OUT_JOINED_TICKER()
			NETWORK_PREVENT_SCRIPT_HOST_MIGRATION()
			
			IF IS_NET_PLAYER_OK(PLAYER_ID())
				IF NOT IS_BIT_SET(iBoolsBitSet, biWagerSet)
				
					IF DOES_BLIP_EXIST(DestinationBlip)
				
						INT iPlayerCash
						iPlayerCash = GET_PLAYER_CASH(PLAYER_ID())
						INT iWager
						//**TWH - CMcM - 1425706 - Added new function to calculate player pot contribution for Impromptu Races.
						iWager = CALCULATE_PLAYER_POT_CONTRIBUTION(serverBD.vStart, serverBD.vDestination)
						
						IF NETWORK_CAN_SPEND_MONEY(iWager, FALSE, FALSE, FALSE)
							IF iPlayerCash < iWager
								IF iPlayerCash > 0
									playerBD[PARTICIPANT_ID_TO_INT()].iWager = iPlayerCash
								ELSE
									playerBD[PARTICIPANT_ID_TO_INT()].iWager = 0
								ENDIF
							ELSE
								playerBD[PARTICIPANT_ID_TO_INT()].iWager = iWager
							ENDIF
						ELSE
							playerBD[PARTICIPANT_ID_TO_INT()].iWager = 0
						ENDIF
						bResetMenuNow = TRUE
						SET_BIT(iBoolsBitSet, biWagerSet)
					ELSE
						playerBD[PARTICIPANT_ID_TO_INT()].iWager = ROUND(R2P_MIN_WAGER*g_sMPTunables.fImpromptuRaceExpensesMultiplier)
					ENDIF
				ENDIF
				
				PROCESS_RACE_TO_POINT_MENU()
				
				INT iMyGBD
				iMyGBD = NATIVE_TO_INT(PLAYER_ID())
				IF IS_BIT_SET(GlobalplayerBD_FM_2[iMyGBD].iBitSet, biIwantToVote)
					#IF IS_DEBUG_BUILD NET_LOG("PLAYER IN VOTE CORONA")	#ENDIF
					NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - MISSION END - PLAYER IN VOTE CORONA - SCRIPT CLEANUP D     <----------     ") NET_NL()
					SCRIPT_CLEANUP()
				ENDIF
				
			ENDIF
			
			
				
			IF serverBD.eRaceStage > eR2P_SETUP
				//Clean up an on call if I'm starting it
				IF AM_I_TRANSITION_SESSIONS_STARTING_ON_CALL()
					SET_TRANSITION_SESSIONS_CLEAN_UP_ON_CALL_NOW()
				ENDIF
				REMOVE_MIN_DISTANCE_RADIUS_BLIP()
				SET_USER_RADIO_CONTROL_ENABLED(TRUE)
				CLEANUP_MENU_ASSETS()
				//GIVE_LOCAL_PLAYER_CASH(-playerBD[PARTICIPANT_ID_TO_INT()].iCashBet)	//Remove Wager from Player
				playerBD[PARTICIPANT_ID_TO_INT()].eRaceStage = eR2P_COUNTDOWN
				NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - PLAYER STAGE = eR2P_COUNTDOWN    <----------     ") NET_NL()
			ENDIF
			
		BREAK
		
		CASE eR2P_COUNTDOWN
			
			DISABLE_CELLPHONE_THIS_FRAME_ONLY()
			
			REQUEST_MINIGAME_COUNTDOWN_UI(Race_CountDownUI)
			//DRAW_R2P_START_MARKER()
			//DRAW_DISTANCE_TO_END()
			
			IF NOT IS_BIT_SET(iBoolsBitSet, biStartSetup)
				//SET_FM_MISSION_AS_STARTED_FOR_BETTING()
				
				//Take Wager
				IF playerBD[PARTICIPANT_ID_TO_INT()].iWager > 0
					IF USE_SERVER_TRANSACTIONS()
						INT iTransactionID
						TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_SPEND_IMPROMPTU_RACE_FEE, playerBD[PARTICIPANT_ID_TO_INT()].iWager, iTransactionID)
					ELSE
						GIVE_LOCAL_PLAYER_CASH(-playerBD[PARTICIPANT_ID_TO_INT()].iWager)
						NETWORK_PAY_MATCH_ENTRY_FEE(playerBD[PARTICIPANT_ID_TO_INT()].iWager, "RaceToPoint")
					ENDIF
					//NETWORK_EARN_FROM_RACE_WIN(-playerBD[PARTICIPANT_ID_TO_INT()].iWager)
					NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - WAGER = $") NET_PRINT_INT(playerBD[PARTICIPANT_ID_TO_INT()].iWager) NET_NL()
				#IF IS_DEBUG_BUILD
				ELSE
					NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - PLAYER HAS NO CASH FOR WAGER") NET_NL()
				#ENDIF
				ENDIF
				
				DELETE_WAYPOINTS_FROM_THIS_PLAYER()
				
				SET_R2P_VOICE_CHAT_OVERRIDE(TRUE)
				SHOW_ONLY_BLIPS_OF_PLAYERS_ON_THIS_SCRIPT(TRUE)
				
				vLocalStart = GET_ENTITY_COORDS(PLAYER_PED_ID())
				
				MPGlobalsAmbience.R2Pdata.bOnCountdown = TRUE
				
				SET_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_RACETOPOINT, TRUE)
				IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType != FMMC_TYPE_MISSION
					GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_RACE_TO_POINT
					GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.mdID.idCreator = FMMC_TYPE_RACE_TO_POINT
				ENDIF
				
				DISABLE_MP_PASSIVE_MODE(PMER_START_OF_A_MATCH, FALSE)
				
				SET_BIT(iBoolsBitSet, biStartSetup)
				NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - (FMMC_TYPE_RACE_TO_POINT) - biStartSetup DONE    <----------     ") NET_NL()
			ENDIF
			
			IF serverBD.eRaceStage > eR2P_COUNTDOWN
			AND HAS_MINIGAME_COUNTDOWN_UI_LOADED(Race_CountDownUI)
				IF UPDATE_MINIGAME_COUNTDOWN_UI(Race_CountDownUI, TRUE, FALSE, TRUE, 3, TRUE, TRUE)
					Race_CountDownUI.iBitFlags = 0
			      	CANCEL_TIMER(Race_CountDownUI.CountdownTimer)
			      	RELEASE_MINIGAME_COUNTDOWN_UI(Race_CountDownUI)
					MPGlobalsAmbience.R2Pdata.bOnCountdown = FALSE
					
					SET_MISSION_NAME(TRUE, "R2P_MENU")
					
					playerBD[PARTICIPANT_ID_TO_INT()].eRaceStage = eR2P_RACE
					NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - PLAYER STAGE = eR2P_RACE    <----------     ") NET_NL()
				ELIF NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vLocalStart, <<DISQUALIFIED_RANGE, DISQUALIFIED_RANGE, DISQUALIFIED_RANGE>>)
					Race_CountDownUI.iBitFlags = 0
			      	CANCEL_TIMER(Race_CountDownUI.CountdownTimer)
			      	RELEASE_MINIGAME_COUNTDOWN_UI(Race_CountDownUI)
					
					SET_BIT(iBoolsBitSet, biDisqualified)
					SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GENERIC_TEXT, "R2P_DQBIG", "R2P_DQSTR")
					
					NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - DISQUALIFIED - SCRIPT CLEANUP Q     <----------     ") NET_NL()
					SCRIPT_CLEANUP()
				ENDIF
			ENDIF
		BREAK
		
		CASE eR2P_RACE
			
			DRAW_AND_BLIP_DESTINATION()
			//DRAW_DISTANCE_TO_END()
			PROCESS_REACHING_DESTINATION()
			
			IF serverBD.iWinnerPart > -1
				playerBD[PARTICIPANT_ID_TO_INT()].eRaceStage = eR2P_REWARD
				NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - PLAYER STAGE = eR2P_REWARD    <----------     ") NET_NL()
			ENDIF
			
			#IF IS_DEBUG_BUILD
				DEBUG_SKIPS()
			#ENDIF
		BREAK
		
		CASE eR2P_REWARD
			GIVE_END_RACE_REWARD()
		BREAK
		
		CASE eR2P_CLEANUP
			NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - SCRIPT CLEANUP B     <----------     ") NET_NL()
			SCRIPT_CLEANUP()
		BREAK
	ENDSWITCH
ENDPROC

//PURPOSE: Process the RACE TO POINT stages for the Server
PROC PROCESS_RACE_TO_POINT_SERVER()
	SWITCH serverBD.eRaceStage
		CASE eR2P_SETUP
			//Move on once server has started race
			IF HAS_RACE_BEEN_STARTED()
				DELETE_WAYPOINTS_FROM_THIS_PLAYER()
				BROADCAST_RACE_TO_POINT_CANCEL_INVITE(serverBD.vStart, NETWORK_GET_INSTANCE_ID_OF_THIS_SCRIPT(), GET_PLAYERS_TO_INVITE_BITSET(GET_PLAYER_TEAM(PLAYER_ID()), R2P_INVITE_RADIUS))
				serverBD.eRaceStage = eR2P_COUNTDOWN
				NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - SERVER STAGE = eR2P_COUNTDOWN    <----------     ") NET_NL()
			ENDIF
		BREAK
		
		CASE eR2P_COUNTDOWN
			serverBD.StartTime = GET_NETWORK_TIME()
			serverBD.eRaceStage = eR2P_RACE
			NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - SERVER STAGE = eR2P_RACE    <----------     ") NET_NL()
		BREAK
		
		CASE eR2P_RACE
			IF serverBD.iWinnerPart != -1
				serverBD.eRaceStage = eR2P_REWARD
				NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - SERVER STAGE = eR2P_REWARD    <----------     ") NET_NL()
			ENDIF
		BREAK
		
		CASE eR2P_REWARD
			serverBD.eRaceStage = eR2P_CLEANUP
			NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - SERVER STAGE = eR2P_CLEANUP    <----------     ") NET_NL()
		BREAK
		
		CASE eR2P_CLEANUP
			
		BREAK
	ENDSWITCH
ENDPROC

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                      Main Loop                      //////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
SCRIPT(MP_MISSION_DATA missionScriptArgs)
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		#IF IS_DEBUG_BUILD	
			NET_PRINT_TIME() NET_PRINT("MP: Starting mission ") NET_NL()
		#ENDIF
		
		// Carry out all the initial game starting duties. 
		IF NOT PROCESS_PRE_GAME(missionScriptArgs)
			#IF IS_DEBUG_BUILD NET_LOG("CLEANUP - FAILED TO RECEIVE INITIAL BROADCAST DATA")	#ENDIF
			NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - FAILED TO RECEIVE INITIAL BROADCAST DATA - CLEANING UP    <----------     ") NET_NL()
			SCRIPT_CLEANUP()
		ENDIF
		
		IF NOT IS_PC_VERSION()
			IF IS_CUSTOM_MENU_ON_SCREEN()
				#IF IS_DEBUG_BUILD NET_LOG("CLEANUP - IS_CUSTOM_MENU_ON_SCREEN")	#ENDIF
				NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - MISSION END - IS_CUSTOM_MENU_ON_SCREEN - SCRIPT CLEANUP G     <----------     ") NET_NL()
				SCRIPT_CLEANUP()
			ENDIF
		ENDIF
		
		IF g_bInAtm
		OR g_bBrowserVisible
			#IF IS_DEBUG_BUILD NET_LOG("CLEANUP - IS_CUSTOM_MENU_ON_SCREEN")	#ENDIF
			NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - MISSION END - ATM OR BROWSER ON SCREEN - SCRIPT CLEANUP H     <----------     ") NET_NL()
			SCRIPT_CLEANUP()
		ENDIF
		
		// Setup widgets. Make sure this is done after PROCESS_PRE_GAME()
		#IF IS_DEBUG_BUILD
			CREATE_WIDGETS()
		#ENDIF
	ENDIF
	
	// Main loop
	WHILE TRUE

		// One wait to rule them all. This can be the ONLY wait from here on in.....
		MP_LOOP_WAIT_ZERO()
		
		// Reset menu mouse input
		bMouseAccept = FALSE
		iMouseDestCycle = 0
				
		// If we have a match end event, bail
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
		//OR NOT IS_NET_PLAYER_OK(PLAYER_ID(), TRUE, TRUE)	//FALSE, TRUE)
			#IF IS_DEBUG_BUILD NET_LOG("SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE")	#ENDIF
			NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - MISSION END - SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE - SCRIPT CLEANUP B     <----------     ") NET_NL()
			SCRIPT_CLEANUP()
		ENDIF
		
		//Check if Race to Point shouldn't start
		IF NOT CAN_DO_RACE_TO_POINT(FALSE, FALSE, FALSE)
			#IF IS_DEBUG_BUILD NET_LOG("CAN_DO_RACE_TO_POINT")	#ENDIF
			NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - MISSION END - CAN_DO_RACE_TO_POINT = FALSE - SCRIPT CLEANUP D     <----------     ") NET_NL()
			SCRIPT_CLEANUP()
		ENDIF
		
		//Check if player has swapped from/to tutorial session and end
		IF NETWORK_IS_IN_TUTORIAL_SESSION() != bInTutorialSession
			#IF IS_DEBUG_BUILD NET_LOG("PLAYER SWAPPED TUT SESSION")	#ENDIF
			NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - MISSION END - PLAYER SWAPPED TUTORIAL SESSION - SCRIPT CLEANUP C     <----------     ") NET_NL()
			SCRIPT_CLEANUP()
		ENDIF
		
		
		
		// Deal with the debug
		#IF IS_DEBUG_BUILD		
			UPDATE_WIDGETS()
		#ENDIF		
	
		// KEITH 22/6/13: Re-activated for Impromptu Races
		// If we need to leave the mission, make sure we just straight to the end stage and terminate the script
		IF SHOULD_PLAYER_LEAVE_MP_MISSION() 
			#IF IS_DEBUG_BUILD
			NET_PRINT_TIME() NET_PRINT("IMPROMPTU RACE: Leave mission = TRUE \n")
			#ENDIF
			playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_LEAVE
			#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_LEAVE")	#ENDIF
		ENDIF
		
		// -----------------------------------
		// Process your game logic.....
		SWITCH GET_CLIENT_MISSION_STATE(PARTICIPANT_ID_TO_INT())
			
			// Wait untill the server gives the all go before moving on
			CASE GAME_STATE_INI
				
				DISABLE_CELLPHONE_THIS_FRAME_ONLY()
				
				LOAD_MENU_ASSETS()
				
				IF NOT IS_CUSTOM_MENU_ON_SCREEN()
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_RUNNING
					NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_RUNNING    <----------     ") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_RUNNING")	#ENDIF
				ELSE
					IF HAS_NET_TIMER_EXPIRED(AnotherCustomMenuTimer, 5000)
						NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - MISSION END - ANOTHER CUSTOM MENU IS ON DISPLAY - SCRIPT CLEANUP J     <----------     ") NET_NL()
						SCRIPT_CLEANUP()
					ELSE
						NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - ANOTHER CUSTOM MENU IS ON DISPLAY     <----------     ") NET_NL()
					ENDIF
				ENDIF
				
				//Check if another Custom Menu is being displayed
				/*IF IS_CUSTOM_MENU_ON_SCREEN()
					#IF IS_DEBUG_BUILD NET_LOG("ANOTHER CUSTOM MENU IS ON DISPLAY")	#ENDIF
					NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - MISSION END - ANOTHER CUSTOM MENU IS ON DISPLAY - SCRIPT CLEANUP J     <----------     ") NET_NL()
					SCRIPT_CLEANUP()
				ENDIF*/
				
				// Look for the server say the mission has ended
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_END
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY
					NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY 1    <----------     ") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_TERMINATE_DELAY")	#ENDIF
				ENDIF
			BREAK
			
			// Main gameplay state.
			CASE GAME_STATE_RUNNING
				
				//CONTROL_INVITING_PLAYER_FROM_PAUSE_MENU()
					
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING
					PROCESS_RACE_TO_POINT_CLIENT()
				
				ELIF GET_SERVER_MISSION_STATE() = GAME_STATE_INI
					//Check ifanother CUstom Menu is being displayed
					IF IS_CUSTOM_MENU_ON_SCREEN()
						#IF IS_DEBUG_BUILD NET_LOG("ANOTHER CUSTOM MENU IS ON DISPLAY")	#ENDIF
						NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - MISSION END - ANOTHER CUSTOM MENU IS ON DISPLAY - SCRIPT CLEANUP K     <----------     ") NET_NL()
						SCRIPT_CLEANUP()
					ENDIF
				ELIF GET_SERVER_MISSION_STATE() = GAME_STATE_END
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY
					NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY 2   <----------     ") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_TERMINATE_DELAY")	#ENDIF
				ENDIF
								
				// Make the player end the mission if they leave the Start area
				IF SHOULD_CLEANUP_FROM_SETUP_MENU()
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END
					NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END 6 - MISSION END") NET_PRINT_VECTOR(serverBD.vStart) NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_END 6")	#ENDIF
				ENDIF
			BREAK
			
			CASE GAME_STATE_TERMINATE_DELAY 
				SET_MISSION_FINISHED(serverBD.MissionTerminateDelayTimer)
		      	IF IS_MISSION_READY_TO_CLEANUP(serverBD.MissionTerminateDelayTimer)
		            playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END
					NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END 3    <----------     ") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_END 3")	#ENDIF
		      	ENDIF
			BREAK
			
			// The game stage the local player is placed when we want him to leave a mission
			CASE GAME_STATE_LEAVE
				//LEAVE_MY_MP_MISSION(thisMission)
				playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END
				NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END 2    <----------     ") NET_NL()
				#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_END 2")	#ENDIF
			FALLTHRU
			
			//Cleans up then terminates the mission
			CASE GAME_STATE_END
				NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - SCRIPT CLEANUP A     <----------     ") NET_NL()
				SCRIPT_CLEANUP()
			BREAK

		ENDSWITCH
		
		// -----------------------------------
		// Process server game logic		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			
			SWITCH GET_SERVER_MISSION_STATE()
				
				// For CnC group missions, we may want to wait for all players to have launched the script
				CASE GAME_STATE_INI
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CONTEXT)
					//CONTROL_INVITING_PLAYER_TRIGGERED_WITH()
					serverBD.iServerGameState = GAME_STATE_RUNNING
					NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - serverBD.iServerGameState = GAME_STATE_RUNNING    <----------     ") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG_Host("iServerGameState = GAME_STATE_RUNNING")	#ENDIF
				BREAK
				
				// Look for game end conditions
				CASE GAME_STATE_RUNNING
				
					MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER()
					PROCESS_RACE_TO_POINT_SERVER()
					
					#IF IS_DEBUG_BUILD
						IF bHostEndMissionNow
							serverBD.iServerGameState = GAME_STATE_END
							NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - serverBD.iServerGameState = GAME_STATE_END 1    <----------     ") NET_NL()
							#IF IS_DEBUG_BUILD NET_LOG_Host("iServerGameState = GAME_STATE_END 1")	#ENDIF
						ENDIF
					#ENDIF	
					
					IF HAVE_MISSION_END_CONDITIONS_BEEN_MET()
						serverBD.iServerGameState = GAME_STATE_END
						NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - serverBD.iServerGameState = GAME_STATE_END 4    <----------     ") NET_NL()
						#IF IS_DEBUG_BUILD NET_LOG_Host("iServerGameState = GAME_STATE_END 4")	#ENDIF
					ENDIF
					
				BREAK
				
				CASE GAME_STATE_END
				BREAK
				
			ENDSWITCH
			
		ENDIF
		
	ENDWHILE
	
ENDSCRIPT
