
//////////////////////////////////////////////////////////////////////
/* Apartment_Minigame_Launcher.sc									*/
/* Author: Aleksej Leskin											*/
/* Multiplayer script for laucnhing minigames inside a clubhouse	*/
//////////////////////////////////////////////////////////////////////


USING "globals.sch"
USING "net_mission.sch"

// Game Headers
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_path.sch"
USING "commands_event.sch"

// Network Headers
USING "net_include.sch"
USING "net_events.sch"
USING "net_spawn.sch"

//
USING "net_scoring_common.sch"
USING "net_ambience.sch"

//USING "help_at_location.sch"
USING "commands_object.sch"
USING "net_xp_animation.sch"

USING "net_wait_zero.sch"
USING "freemode_events_header.sch"


#IF IS_DEBUG_BUILD
USING "net_debug_log.sch"
#ENDIF

USING "apartment_minigame_launcher.sch"
USING "net_realty_details.sch"
USING "Apartment_Minigame_General.sch"

BOOL	bPreCleanup = FALSE
BOOL 	bUpdateMenuInput
INT		iCurrentMenuRow = 0
BOOL	bInteractionActive = FALSE
BOOL	bHasMenuOpenSoundPlayed = FALSE

//Looped Vars
BOOL 	bEndScriptWait = FALSE
BOOL 	bAllowScriptLaunch = FALSE

MINIGAME_LAUNCH sMinigameLaunchTimer[CONST_MINIGAME_COUNT]
STRUCT_USER_EVENT sEventServer
MINIGAME_INTERACT_INFO sMGInfoServer
structTimer TimerAfterGame
structTimer stDelaySetupView
structTimer stTimerFailSafe
structTimer stTimerInitialize
structTimer stTimerWaitStarted
structTimer stTimerStartEventWait
// for switching between availible minigame prompts.

STRUCT_UPDATE_MINIGAME sMinigameUpdate

CONST_FLOAT CONST_AFTER_GAME_DELAY 2.0
INT		iLocalFlags
//INT		iCurrentLegs
INT 	iCurrentSets
INT		iSelfID

INT_MENU_ITEM		iArrayDartNumOfLegs[MAX_NUM_DARTS_LEGS]
INT_MENU_ITEM		iArrayDartNumOfSets[MAX_NUM_DARTS_SETS]
MENU_ROW_INFO mriDartsMenu[NUM_MENU_ITEMS_DARTS]
// Game States. 
CONST_INT GAME_STATE_INI 				0
CONST_INT GAME_STATE_RUNNING			1
CONST_INT GAME_STATE_LEAVE				2
CONST_INT GAME_STATE_TERMINATE_DELAY	3
CONST_INT GAME_STATE_END				4

STRUCT_LOCAL_CLIENT_DATA sLocalData
AML_GENERAL_STRUCT sCurrentValues
MAD_STRUCT sStateNow

ServerBroadcastData serverBD

MINIGAME_INTERACT_INFO sMinigameInteractInfo
ENUM_APARTMENT_TYPE atCurrentApartment
structTimer stFailSafeTimer
structTimer stPerLeaderCheck
structTimer stPerSetupCheck

MINIGAME_TYPE_INFO	arrayMinigameInfo[CONST_MINIGAME_COUNT]
INT					arrayMinigameUsers[CONST_MINIGAME_COUNT]

CONST_FLOAT FAIL_TIMER_MAX_TIME 4.5

PlayerBroadcastData playerBD[NUM_NETWORK_PLAYERS]

//Cleanup Mission Data
PROC SCRIPT_CLEANUP()
	CLEAR_MENU_DATA()
	CLEANUP_MENU_ASSETS()
	RESET_HUD(iLocalFlags)
	
	CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "[MPTUT] - CLEANUP APARTMNET MINIGAME LAUNCHER  ")
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC

#IF IS_DEBUG_BUILD
	BOOL bAside
	BOOL bSideSelected
	BOOL bInsideClubhouse
	BOOL bRequestToBeHost
	BOOL bIsHost
	USING "shared_debug.sch"
	PROC ADD_AM_DART_WIDGETS()
		START_WIDGET_GROUP("MG_LAUNCHER")
			ADD_WIDGET_INT_READ_ONLY("Current Active MGID: ", GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iClubhouseMGID)
			ADD_WIDGET_INT_READ_ONLY("Curernt Num Of Darts Players: ", g_iClubhouseDartsPlayerCount) 
			ADD_WIDGET_BOOL("Disable Darts Via Tunable", g_sMPTunables.bBIKER_CLUBHOUSE_DISABLE_DARTS)
			ADD_WIDGET_BOOL("Disable Arm Wrestle Via Tunable", g_sMPTunables.bBIKER_CLUBHOUSE_DISABLE_ARM_WRESTLING)
			
			ADD_WIDGET_BOOL("REQUEST TO BE HOST", bRequestToBeHost)
			ADD_WIDGET_BOOL("Am i Host?", bIsHost)
			ADD_WIDGET_BOOL("Aside", bAside)
			ADD_WIDGET_BOOL("Side Selected", bSideSelected)
			ADD_WIDGET_BOOL("INSIDE CLUBHOUSE?", bInsideClubhouse)
			ADD_WIDGET_INT_READ_ONLY("CURRENT MGID", playerBD[PARTICIPANT_ID_TO_INT()].iCurrentMinigame)
			
			ADD_WIDGET_BOOL("sMinigameUpdate.bSelfInsideSpecific ", sMinigameUpdate.bSelfInsideSpecific)
			ADD_WIDGET_BOOL("sMinigameUpdate.bAnotherInsideMain", sMinigameUpdate.bAnotherInsideMain)
			ADD_WIDGET_BOOL("sMinigameUpdate.bAnotherInsideSpecific", sMinigameUpdate.bAnotherInsideSpecific)
			ADD_WIDGET_BOOL("sMinigameUpdate.bInsideCoords", sMinigameUpdate.bInsideCoords)
		STOP_WIDGET_GROUP()
	ENDPROC
#ENDIF

//Do necessary pre game start ini.
PROC PROCESS_PRE_GAME()
	
	// This marks the script as a net script, and handles any instancing setup. 
	MP_MISSION_DATA missionScriptArgs 
	missionScriptArgs.iInstanceId = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iVisibilityID
	
	#IF FEATURE_CASINO_HEIST
	IF IS_PLAYER_IN_ANY_SIMPLE_INTERIOR(PLAYER_ID())
		missionScriptArgs.iInstanceId = GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iInstance + 32
	ENDIF
	#ENDIF
	
	IF missionScriptArgs.mdID.idMission <> eAM_Safehouse
		CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "Changing missionScriptArgs.mdID.idMission to eAM_Safehouse...")
		missionScriptArgs.mdID.idMission = eAM_Safehouse
	ENDIF
	
	SETUP_MP_MISSION_FOR_NETWORK(GET_MAX_NUM_PARTICIPANTS_FOR_MP_MISSION(missionScriptArgs.mdID.idMission),  missionScriptArgs)	//NUM_NETWORK_PLAYERS, missionScriptArgs)	//
	CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "LAUNCHER INSTANCE: ", GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iVisibilityID)
	// GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideVariation
	
	// This makes sure the net script is active, waits untill it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD, SIZE_OF(playerBD))
	
	// KGM: Wait for the first network broadcast before moving on
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		PRINTLN("[MPTUT] - MISSION END - FAILED TO RECEIVE INITIAL NETWORK BROADCAST - SCRIPT CLEANUP D ")
		SCRIPT_CLEANUP()
	ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		// This script will not be paused if another script calls PAUSE_GAME
		SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)

		playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_INI
		PRINTLN("[MPTUT] - PRE_GAME DONE ")
	ELSE
		PRINTLN("[MPTUT] - MISSION END - NETWORK_IS_GAME_IN_PROGRESS = FALSE - SCRIPT CLEANUP Q ")
		SCRIPT_CLEANUP()
	ENDIF
ENDPROC


/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                  MISSION PROCS                  //////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

PROC MINIGAME_AVAILABILITY_DISPLAY(MAD_STRUCT &sStateCurrent, AML_GENERAL_STRUCT &sGeneralInfo)
	
	MINIGAME_TYPE_INFO mtiCurrentLocal = arrayMinigameInfo[sStateCurrent.iCurrentMinigame]
	
	BOOL bCanDisplayHelp = TRUE
	
	IF bInteractionActive
		bCanDisplayHelp = FALSE
		MAG_SWITCH_STATE(sStateCurrent.eState, MGA_HIDE_AVAILABILITY)
	ENDIF
	
	IF (IS_HELP_MESSAGE_BEING_DISPLAYED()
	AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_MG_TEXT(mtiCurrentLocal, MGT_MG_AVAILIBLE)))
	OR IS_PLAYER_USING_OFFICE_SEATID_VALID()
		bCanDisplayHelp = FALSE
		MAG_SWITCH_STATE(sStateCurrent.eState, MGA_HIDE_AVAILABILITY)
	ENDIF	
	
	IF sGeneralInfo.iMGCurrent >= 0
		bCanDisplayHelp = FALSE
	ENDIF
	
	IF (NOT sGeneralInfo.bCanHaveMorePlayers[sStateCurrent.iCurrentMinigame]
	AND (sStateCurrent.eState >= MGA_DISPLAY_MINIGAME AND sStateCurrent.eState <= MGA_DELAY_AVAILABILITY))
	OR IS_BIT_SET(serverBD.iMGRunning, sStateCurrent.iCurrentMinigame)
		bCanDisplayHelp = FALSE
		MAG_SWITCH_STATE(sStateCurrent.eState, MGA_HIDE_AVAILABILITY)
	ENDIF
	
		SWITCH(sStateCurrent.eState)
			//Select a minigame for display.
			CASE MGA_SELECT_MINIGAME
				CANCEL_TIMER(sStateCurrent.stStageDelay)
				
				BOOL bNewMGFound 
				bNewMGFound = TRUE
				INT iMGID
				FOR iMGID = 0 TO CONST_MINIGAME_COUNT - 1

						IF iMGID <> sStateCurrent.iCurrentMinigame
						AND sGeneralInfo.bCanHaveMorePlayers[iMGID]
						AND NOT IS_BIT_SET(serverBD.iMGRunning, iMGID)
							sStateCurrent.iCurrentMinigame = iMGID
							CDEBUG1LN(DEBUG_MG_LAUNCHER_APART,"MA DISAPLY - new selected, Previous:", sStateCurrent.iCurrentMinigame, " New:", iMGID)
							sGeneralInfo.fDisplayTime = MG_AVALIBLE_DISAPLY_TIME
							MAG_SWITCH_STATE(sStateCurrent.eState, MGA_DISPLAY_MINIGAME)
							CDEBUG1LN(DEBUG_MG_LAUNCHER_APART,"MA DISAPLY - Display for time1:", sGeneralInfo.fDisplayTime)
							BREAK
						ELSE
							IF sGeneralInfo.bCanHaveMorePlayers[sStateCurrent.iCurrentMinigame]
								bNewMGFound = FALSE
							ENDIF
						ENDIF
				ENDFOR
				
				IF NOT bNewMGFound
					sGeneralInfo.fDisplayTime = MG_AVALIBLE_SINGLE_DISAPLY_TIME
					sStateCurrent.iMGDisplayedCount = CONST_MINIGAME_COUNT
					MAG_SWITCH_STATE(sStateCurrent.eState, MGA_DISPLAY_MINIGAME)
					CDEBUG1LN(DEBUG_MG_LAUNCHER_APART,"MA DISAPLY - Display for time2:", sGeneralInfo.fDisplayTime)
				ENDIF
			BREAK
			
			//Disaply minigame availible for X seconds.
			CASE MGA_DISPLAY_MINIGAME
				IF NOT sStateCurrent.bHelpDisplayed
					TOGGLE_HELP_TEXT(GET_MG_TEXT(mtiCurrentLocal, MGT_MG_AVAILIBLE))	
					sStateCurrent.bHelpDisplayed = TRUE
					CDEBUG1LN(DEBUG_MG_LAUNCHER_APART,"MA DISAPLY - SHOW AVAILIBLE TEXT")
				ENDIF
				
				IF NOT IS_TIMER_STARTED(sStateCurrent.stStageDelay)
					START_TIMER_NOW(sStateCurrent.stStageDelay)
					CDEBUG1LN(DEBUG_MG_LAUNCHER_APART,"MA DISAPLY - Display for time:", sGeneralInfo.fDisplayTime)
				ELIF GET_TIMER_IN_SECONDS(sStateCurrent.stStageDelay) >= sGeneralInfo.fDisplayTime
					
					sStateCurrent.iMGDisplayedCount++
					TOGGLE_HELP_TEXT(GET_MG_TEXT(mtiCurrentLocal, MGT_MG_AVAILIBLE) , FALSE)
					sStateCurrent.bHelpDisplayed = FALSE
					RESTART_TIMER_NOW(sStateCurrent.stStageDelay)
					MAG_SWITCH_STATE(sStateCurrent.eState, MGA_DELAY_NEXT_MINIGAME)
				ENDIF
			BREAK
			
			//Wait for x Seconds before dispalying next availible minigame
			CASE MGA_DELAY_NEXT_MINIGAME
				IF NOT IS_TIMER_STARTED(sStateCurrent.stStageDelay)
					START_TIMER_NOW(sStateCurrent.stStageDelay)
				ELIF GET_TIMER_IN_SECONDS(sStateCurrent.stStageDelay) >= MINIGAME_BETWEEN_MINIGAME_TIME
					IF sStateCurrent.iMGDisplayedCount >= CONST_MINIGAME_COUNT
						//stop dispalying minigame availability, for it not to seem like spam.
						CDEBUG1LN(DEBUG_MG_LAUNCHER_APART,"MA DISAPLY - hide availability for:", MINIGAME_AVALIBLE_HIDE_TIME)
						RESTART_TIMER_NOW(sStateCurrent.stStageDelay)
						MAG_SWITCH_STATE(sStateCurrent.eState, MGA_DELAY_AVAILABILITY)
					ELSE
						MAG_SWITCH_STATE(sStateCurrent.eState, MGA_SELECT_MINIGAME)
					ENDIF
				ENDIF
			BREAK
			
			CASE MGA_DELAY_AVAILABILITY
				IF NOT IS_TIMER_STARTED(sStateCurrent.stStageDelay)
					START_TIMER_NOW(sStateCurrent.stStageDelay)
				ELIF GET_TIMER_IN_SECONDS(sStateCurrent.stStageDelay) >= MINIGAME_AVALIBLE_HIDE_TIME
					sStateCurrent.iMGDisplayedCount = 0
					MAG_SWITCH_STATE(sStateCurrent.eState, MGA_SELECT_MINIGAME)
				ENDIF
			BREAK
			
			CASE MGA_HIDE_AVAILABILITY
				IF IS_TIMER_STARTED(sStateCurrent.stStageDelay)
					CANCEL_TIMER(sStateCurrent.stStageDelay)
				ENDIF
				
				IF NOT bCanDisplayHelp
					IF sStateCurrent.bHelpDisplayed
						TOGGLE_HELP_TEXT(GET_MG_TEXT(mtiCurrentLocal, MGT_MG_AVAILIBLE), FALSE)
						sStateCurrent.bHelpDisplayed = FALSE
					ENDIF
				ELSE
					sStateCurrent.iMGDisplayedCount = 0
					MAG_SWITCH_STATE(sStateCurrent.eState, MGA_SELECT_MINIGAME)
				ENDIF
			BREAK
		ENDSWITCH
ENDPROC

PROC REBUILD_DARTS_MENU(MINIGAME_TYPE_INFO minigameInfo)
	CLEAR_MENU_DATA() 
	SET_MENU_ITEM_LAYOUT(MENU_ITEM_TEXT, MENU_ITEM_TEXT)
	SET_MENU_ITEM_JUSTIFICATION(FONT_LEFT, FONT_RIGHT)
	SET_MENU_TITLE("DART_A_SET")
	
	TEXT_LABEL_15 tl15
	ADD_MENU_ITEM_TEXT(0, mriDartsMenu[0].sLabel, 0) 
	tl15 =  "DART_A_S"
	tl15 += iCurrentSets
	ADD_MENU_ITEM_TEXT(0, tl15)
	
	IF IS_BIT_SET(serverBD.iMGCanHaveMorePlayers, playerBD[PARTICIPANT_ID_TO_INT()].iCurrentMinigame)
		ADD_MENU_HELP_KEY_CLICKABLE( INPUT_FRONTEND_ACCEPT, GET_MG_TEXT(minigameInfo, MGT_SINGLE_PLAYER))
	ELSE
		ADD_MENU_HELP_KEY_CLICKABLE( INPUT_FRONTEND_ACCEPT, GET_MG_TEXT(minigameInfo, MGT_MULTI_PLAYER))
	ENDIF

	CONTROL_ACTION caCancelType
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		caCancelType = INPUT_SCRIPT_RRIGHT
	ELSE
		caCancelType = INPUT_FRONTEND_CANCEL
	ENDIF
				
	ADD_MENU_HELP_KEY_CLICKABLE( caCancelType, "DART_A_MENU_C")
					
	SET_MENU_ITEM_TOGGLEABLE(FALSE, TRUE)
	SET_CURRENT_MENU_ITEM(iCurrentMenuRow)
    SET_CURRENT_MENU_ITEM_DESCRIPTION("DART_A_GNT") 
  ENDPROC
  
  PROC MENU_CHECK_INPUT()
  	
	IF NOT bHasMenuOpenSoundPlayed
		PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
		bHasMenuOpenSoundPlayed = TRUE
	ENDIF
	
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
		PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
	ENDIF
  	
	CONTROL_ACTION caCancelType
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		caCancelType = INPUT_SCRIPT_RRIGHT
	ELSE
		caCancelType = INPUT_FRONTEND_CANCEL
	ENDIF
	
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, caCancelType)
		PLAY_SOUND_FRONTEND(-1, "CANCEL", "HUD_FRONTEND_DEFAULT_SOUNDSET")
	ENDIF
	
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CELLPHONE_LEFT)
		PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
		MODIFY_COUNTER(iCurrentSets, MAX_NUM_DARTS_SETS, eC_UP)
		bUpdateMenuInput = TRUE
	ENDIF
	
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CELLPHONE_RIGHT)
		PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
		MODIFY_COUNTER(iCurrentSets, MAX_NUM_DARTS_SETS, eC_DOWN)
		bUpdateMenuInput = TRUE
	ENDIF
	
	IF bUpdateMenuInput
		//Update menu Row
		g_FMMC_STRUCT.iNumberOfLegs = 1
		g_FMMC_STRUCT.iNumberOfSets = iArrayDartNumOfSets[iCurrentSets].iValue
		bUpdateMenuInput = False
	ENDIF
  ENDPROC



PROC UPDATE_MINIGAME(MINIGAME_TYPE_INFO minigame, BOOL bAllowStateUpdate)
	
	CLEAR_MINIGAME_UPDATE(sMinigameUpdate)
	
	IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iBitMaskActivities, ENUM_TO_INT(MGL_CF_MG_START_REQUEST))
		//check because player can get moved..
		IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), minigame.vLocatorCoords) < MIN_DISTANCE_TO_MINIGAME 
				GET_APARTMENT_INTERACT_INFO(sMinigameInteractInfo, atCurrentApartment, minigame.acType)
				sMinigameUpdate.bAcceptableDistance = TRUE
				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(),sMinigameInteractInfo.vMainLocation, sMinigameInteractInfo.vMainLocatorSize, FALSE, TRUE)	
					//&&& Only For Arm Wrestle Table Side Notification
					sMinigameUpdate.bInsideCoords = TRUE
					//UPDATE_MGID(playerBD[PARTICIPANT_ID_TO_INT()], minigame.iMinigameID)
					
					IF minigame.iMinigameID = ENUM_TO_INT(eMGID_ARMWRESTLE)
						
						INT iLocator
						REPEAT sMinigameInteractInfo.iNumAvailible iLocator
							IF  IS_ENTITY_AT_COORD(PLAYER_PED_ID(),sMinigameInteractInfo.MPIavailible[iLocator].vPosition, sMinigameInteractInfo.MPIavailible[iLocator].vLocatorSize, FALSE, TRUE)	
								//local cehck to see if availible
								sMinigameUpdate.bSelfInsideSpecific = TRUE
								sMinigameUpdate.iInsideLocator = iLocator
							ENDIF
						ENDREPEAT		
						
						
						INT iOpponentID
						PARTICIPANT_INDEX piOpponent
						REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iOpponentID
							piOpponent = INT_TO_PARTICIPANTINDEX(iOpponentID)
							
							IF iOpponentID <> PARTICIPANT_ID_TO_INT()
								IF NETWORK_IS_PARTICIPANT_ACTIVE(piOpponent)
									IF playerBD[iOpponentID].iCurrentMinigame = minigame.iMinigameID
									AND IS_BIT_SET(playerBD[iOpponentID].iBitMaskActivities, ENUM_TO_INT(MGL_CG_MG_SIDE_SELECTED))
									AND IS_BIT_SET(playerBD[iOpponentID].iBitMaskActivities, ENUM_TO_INT(MGL_CF_MG_VALID_PLAYER))
										sMinigameUpdate.bAnotherInsideMain = TRUE
										
										IF sMinigameUpdate.iInsideLocator >= 0
											IF sMinigameUpdate.iInsideLocator = CONST_A_SIDE
											AND IS_BIT_SET(playerBD[iOpponentID].iBitMaskActivities, ENUM_TO_INT(MGL_CF_MG_A_SIDE_PLAYER))
												sMinigameUpdate.bAnotherInsideSpecific = TRUE
											ELIF sMinigameUpdate.iInsideLocator = CONST_B_SIDE
											AND NOT IS_BIT_SET(playerBD[iOpponentID].iBitMaskActivities, ENUM_TO_INT(MGL_CF_MG_A_SIDE_PLAYER))
												sMinigameUpdate.bAnotherInsideSpecific = TRUE
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDREPEAT
			
						IF sMinigameUpdate.bSelfInsideSpecific	
							//Not another active palyer inside arm wrestling locator.
							IF NOT sMinigameUpdate.bAnotherInsideSpecific
								sMinigameUpdate.bCorrectSide = TRUE
								IF sMinigameUpdate.iInsideLocator = CONST_A_SIDE
									SET_CLIENT_FLAG(playerBD[PARTICIPANT_ID_TO_INT()], MGL_CF_MG_A_SIDE_PLAYER)
								ENDIF
							ELSE
								SET_CLIENT_FLAG(playerBD[PARTICIPANT_ID_TO_INT()], MGL_CF_MG_A_SIDE_PLAYER, FALSE)
								SET_CLIENT_FLAG(playerBD[PARTICIPANT_ID_TO_INT()], MGL_CG_MG_SIDE_SELECTED, FALSE)
							ENDIF
						ENDIF
						
						IF sLocalData.eCurInteractStage = MIS_IDLE
						//If inside the main locator, and not inside a locator with another player.						
							IF ((NOT sMinigameUpdate.bSelfInsideSpecific AND sMinigameUpdate.bAnotherInsideMain)
							OR (sMinigameUpdate.bSelfInsideSpecific AND sMinigameUpdate.bAnotherInsideSpecific))
							AND IS_BIT_SET(serverBD.iMGCanHaveMorePlayers, minigame.iMinigameID)
								TOGGLE_HELP_TEXT("ARMW_A_SIDE")
							ELSE
								TOGGLE_HELP_TEXT("ARMW_A_SIDE", FALSE)
							ENDIF
						ENDIF
				
					ELSE // Darts
						IF IS_HEADING_ACCEPTABLE(GET_ENTITY_HEADING(PLAYER_PED_ID()), sMinigameInteractInfo.fHeading, 60)
							sMinigameUpdate.bCorrectSide = TRUE
							sMinigameUpdate.bSelfInsideSpecific = TRUE
						ENDIF
					ENDIF
				ELSE
					IF minigame.iMinigameID = ENUM_TO_INT(eMGID_ARMWRESTLE)
						TOGGLE_HELP_TEXT("ARMW_A_SIDE", FALSE)
					ENDIF
				ENDIF	
			ENDIF	
		ENDIF
	ENDIF
	
	IF bAllowStateUpdate
		SWITCH sLocalData.eCurInteractStage
			CASE MIS_IDLE
				IF sMinigameUpdate.bSelfInsideSpecific
				AND sMinigameUpdate.bCorrectSide
					sMinigameUpdate.bHideText = FALSE
							
					UPDATE_MGID(playerBD[PARTICIPANT_ID_TO_INT()], minigame.iMinigameID)
					
					IF IS_BIT_SET(serverBD.iMGRunning, minigame.iMinigameID)
						TOGGLE_HELP_TEXT(GET_MG_TEXT(minigame, MGT_READY_FOR_GAME), FALSE)
						TOGGLE_HELP_TEXT(GET_MG_TEXT(minigame, MGT_TOO_MANY_P), FALSE)
						IF NOT GET_CLIENT_FLAG(playerBD[iSelfID], MGL_CF_MG_VALID_PLAYER)
							TOGGLE_HELP_TEXT(GET_MG_TEXT(minigame, MGT_IN_PROGRESS))
						ENDIF
						
					ELSE
						IF NOT IS_BIT_SET(serverBD.iMGCanHaveMorePlayers, minigame.iMinigameID)
							TOGGLE_HELP_TEXT(GET_MG_TEXT(minigame, MGT_READY_FOR_GAME), FALSE)
							TOGGLE_HELP_TEXT(GET_MG_TEXT(minigame, MGT_TOO_MANY_P))
						ELSE	
							TOGGLE_HELP_TEXT(GET_MG_TEXT(minigame, MGT_IN_PROGRESS), FALSE)
							IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT)
								EVENT_CLIENT_INTERACT_CHANGED(GET_PLAYER_INDEX(), minigame.iMinigameID, TRUE)
								
								//SET_CLIENT_FLAG(playerBD[PARTICIPANT_ID_TO_INT()], MGL_CF_MG_START_REQUEST)
								SET_CLIENT_FLAG(playerBD[PARTICIPANT_ID_TO_INT()], MGL_CF_MG_VALID_PLAYER)
								
								SET_CLIENT_FLAG(playerBD[PARTICIPANT_ID_TO_INT()], MGL_CG_MG_SIDE_SELECTED)
								
								SAFE_BIT_TOGGLE(iLocalFlags,  ENUM_TO_INT(LPF_DISABLE_CELLPHONE))
								SAFE_BIT_TOGGLE(iLocalFlags,  ENUM_TO_INT(LPF_DISABLE_PAUSE_MENU))
								SAFE_BIT_TOGGLE(iLocalFlags,  ENUM_TO_INT(LPF_FREEZE_ENTITY), FALSE)
								SAFE_BIT_TOGGLE(iLocalFlags,  ENUM_TO_INT(LPF_DISABLE_MOVEMENT))

								sMinigameUpdate.bHideText = TRUE
								CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "Interact with minigame, hide text")
								IF NOT IS_TIMER_STARTED(stTimerFailSafe)
									START_TIMER_NOW(stTimerFailSafe)
								ELSE
									RESTART_TIMER_NOW(stTimerFailSafe)
								ENDIF
								
								SET_INTERACTION_STAGE(sLocalData, MIS_INTERACT_CHECK)
							ELSE
								TOGGLE_HELP_TEXT(GET_MG_TEXT(minigame, MGT_TOO_MANY_P), FALSE)
								TOGGLE_HELP_TEXT(GET_MG_TEXT(minigame, MGT_READY_FOR_GAME))
							ENDIF
						ENDIF
					ENDIF
				ENDIF
								
				IF NOT sMinigameUpdate.bSelfInsideSpecific
					TOGGLE_HELP_TEXT(GET_MG_TEXT(minigame, MGT_READY_FOR_GAME), FALSE)
					IF playerBD[PARTICIPANT_ID_TO_INT()].iCurrentMinigame = minigame.iMinigameID
						UPDATE_MGID(playerBD[PARTICIPANT_ID_TO_INT()], ENUM_TO_INT(eMGID_NONE))
						CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "Not Inside and same MGID as current", minigame.iMinigameID)
						sMinigameUpdate.bHideText = TRUE
					ENDIF
				ENDIF
			BREAK
			
			CASE MIS_INTERACT_CHECK
				
				BOOL bStateChanged
				bStateChanged = FALSE
				
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_UD)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_LR)
				
				IF sLocalData.sClientEvents.bAllowInteractReceived = TRUE
					sLocalData.sClientEvents.bAllowInteractReceived = FALSE
					bStateChanged = TRUE
					
					IF sLocalData.sClientEvents.bAllowInteract
						IF minigame.iMinigameID = ENUM_TO_INT(eMGID_DARTS)
							SET_INTERACTION_STAGE(sLocalData, MIS_WAIT_PLAYER_INPUT)
						ELSE
							SET_INTERACTION_STAGE(sLocalData, MIS_NAVIGATE)
						ENDIF
					ELSE
						SET_INTERACTION_STAGE(sLocalData, MIS_CANCEL_READY)
					ENDIF
				ENDIF 
				
				IF NOT IS_TIMER_STARTED(stTimerFailSafe)
					START_TIMER_NOW(stTimerFailSafe)
				ELIF GET_TIMER_IN_SECONDS(stTimerFailSafe) >= MG_TIMER_FAILSAFE_INTERACT_CHECK
					bStateChanged = TRUE
					SET_INTERACTION_STAGE(sLocalData, MIS_CANCEL_READY)
				ENDIF
				
				IF bStateChanged
					IF IS_TIMER_STARTED(stTimerFailSafe)
						CANCEL_TIMER(stTimerFailSafe)
					ENDIF
				ENDIF
			BREAK
			
			CASE MIS_NAVIGATE
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_UD)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_LR)
					
					VECTOR vMoveTo
					FLOAT fDisatnceSideA, fDistanceSideB
					fDisatnceSideA = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), sMinigameInteractInfo.MPIavailible[0].vPosition)
					fDistanceSideB = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), sMinigameInteractInfo.MPIavailible[1].vPosition)
					
					IF fDisatnceSideA < fDistanceSideB
						vMoveTo = sMinigameInteractInfo.MPIavailible[0].vPosition
					ELSE
						vMoveTo = sMinigameInteractInfo.MPIavailible[1].vPosition
					ENDIF
					
					FLOAT fHeadingToPlayBoard 
					fHeadingToPlayBoard = GET_HEADING_BETWEEN_VECTORS_2D(vMoveTo, sMinigameInteractInfo.MPIavailible[0].vPointOfInterest)
					
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), vMoveTo, PEDMOVEBLENDRATIO_WALK, 300, fHeadingToPlayBoard, 0.050)//0.5)
						CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "Players tasked to get into position: ", vMoveTo, " With heading: ", fHeadingToPlayBoard)
					ENDIF
					CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "MOVE TO LOCATION:", vMoveTo)
					CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "CLIENT - LOAD MINIGAME!")

				SET_INTERACTION_STAGE(sLocalData, MIS_WAIT_PLAYER_INPUT)
			BREAK
//			
//			CASE MIS_NAVIGATE_END
//
//				IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_ACHIEVE_HEADING) = FINISHED_TASK
//				OR GET_TIMER_IN_SECONDS(stFailSafeTimer) >= FAIL_TIMER_MAX_TIME
//				OR IS_VECTOR_ZERO(posInfo.vPointOfInterest)
//					CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "MOVE TASK FINISHED")	
//					SET_INTERACTION_STAGE(sLocalData, MIS_START_INTERACTION)
//				ENDIF
//			BREAK
			
//			CASE MIS_START_INTERACTION
//				posInfo = sMinigameInteractInfo.MPIavailible[0]
//				IF NOT IS_STRING_NULL_OR_EMPTY(posInfo.sAnimLib)
//					IF HAS_ANIM_DICT_LOADED(posInfo.sAnimLib)
//						CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "PLAY INTERACTION ANIM")	
//						//TASK_PLAY_ANIM(PLAYER_PED_ID(), posInfo.sAnimLib, posInfo.sIntoAnim, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
//						RESTART_TIMER_NOW(stFailSafeTimer)
//						SET_INTERACTION_STAGE(sLocalData, MIS_WAIT_INTERACTION_END) // skip end - anim is looping
//					ELSE
//						IF (GET_GAME_TIMER() % 3000) < 50
//							CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "REQUEST ANIM DICT FOR INTERACTION ", posInfo.sAnimLib)
//							DEBUG_OUT_MINIGAME_POSITION_INFO(posInfo)
//						ENDIF
//						REQUEST_ANIM_DICT(posInfo.sAnimLib)
//					ENDIF
//				ELSE
//					RESTART_TIMER_NOW(stFailSafeTimer)
//					SET_INTERACTION_STAGE(sLocalData, MIS_WAIT_INTERACTION_END)
//				ENDIF
//			BREAK
//			
//			CASE MIS_WAIT_INTERACTION_END
//				IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_PLAY_ANIM) = PERFORMING_TASK
//				OR GET_TIMER_IN_SECONDS(stFailSafeTimer) >= FAIL_TIMER_MAX_TIME
//				OR IS_STRING_NULL_OR_EMPTY(posInfo.sAnimLib)
//					CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "INTERACT ANIM FINISHED")
//					SET_INTERACTION_STAGE(sLocalData, MIS_WAIT_PLAYER_INPUT)
//				ENDIF
//			BREAK
			
			CASE MIS_WAIT_PLAYER_INPUT
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_UD)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_LR)
				
				//stop the failsafe timer from previous stages.
				IF IS_TIMER_STARTED(stFailSafeTimer)
					CANCEL_TIMER(stFailSafeTimer)
				ENDIF
				
				//@@@ Periodic cehck if host switched.
				IF NOT IS_TIMER_STARTED(stPerLeaderCheck)
					START_TIMER_AT(stPerLeaderCheck, 1.0)
				ELIF GET_TIMER_IN_SECONDS(stPerLeaderCheck) >= 1.0
				OR NOT GET_CLIENT_FLAG(playerBD[PARTICIPANT_ID_TO_INT()], MGL_CF_RECEIVED_SETUP)
					RESTART_TIMER_NOW(stPerLeaderCheck)
					IF IS_PLAYER_SETUP_ON_SERVER(INT_TO_ENUM(ENUM_CLUBHOUSE_MGID, minigame.iMinigameID), serverBD)
						IF SET_CLIENT_FLAG(playerBD[PARTICIPANT_ID_TO_INT()], MGL_CF_MG_SETUP_PLAYER, TRUE)
							SET_CLIENT_FLAG(playerBD[PARTICIPANT_ID_TO_INT()], MGL_CF_RECEIVED_SETUP)
							CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "MIS_WAIT_PLAYER_INPUT - Player became a *Setup player for Minigame: ", GET_CLUBHOUSE_MGID_STRING(INT_TO_ENUM(ENUM_CLUBHOUSE_MGID, minigame.iMinigameID)))
						ENDIF
					ELSE
						IF SET_CLIENT_FLAG(playerBD[PARTICIPANT_ID_TO_INT()], MGL_CF_MG_SETUP_PLAYER, FALSE)
							CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "MIS_WAIT_PLAYER_INPUT - Player became a -Normal player for Minigame: ", GET_CLUBHOUSE_MGID_STRING(INT_TO_ENUM(ENUM_CLUBHOUSE_MGID, minigame.iMinigameID)))
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT IS_TIMER_STARTED(stDelaySetupView)
					START_TIMER_NOW(stDelaySetupView)
				ELIF GET_TIMER_IN_SECONDS(stDelaySetupView) >= CONST_MG_DELAY_SETUP_VIEW
					IF NOT GET_CLIENT_FLAG(playerBD[PARTICIPANT_ID_TO_INT()], MGL_CF_RECEIVED_SETUP) 
						SET_CLIENT_FLAG(playerBD[PARTICIPANT_ID_TO_INT()], MGL_CF_RECEIVED_SETUP)
						CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "RECEIVED SETUP INFO.")
					ENDIF
				ENDIF	
				
				//@@@ Cancel Input wait
				IF (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, GET_PLATFORM_INPUT(ePC_CANCEL))
				OR IS_BIT_SET(serverBD.iMGRunning, minigame.iMinigameID))
				AND NOT bInteractionActive
					CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "CLIENT - MINIGAME READY CANCELED")
					SET_INTERACTION_STAGE(sLocalData, MIS_CANCEL_READY)		
				ENDIF
				
				//@@@ Local Flags
				SAFE_BIT_TOGGLE(iLocalFlags,  ENUM_TO_INT(LPF_DISABLE_IDLE_CAM))
				SAFE_BIT_TOGGLE(iLocalFlags,  ENUM_TO_INT(LPF_DISABLE_CELLPHONE), FALSE)
				SAFE_BIT_TOGGLE(iLocalFlags,  ENUM_TO_INT(LPF_DISABLE_PAUSE_MENU))
				SAFE_BIT_TOGGLE(iLocalFlags,  ENUM_TO_INT(LPF_FREEZE_ENTITY), TRUE)
				//BD
				SET_CLIENT_FLAG(playerBD[PARTICIPANT_ID_TO_INT()], MGL_CF_MG_VALID_PLAYER)

				IF GET_CLIENT_FLAG(playerBD[PARTICIPANT_ID_TO_INT()], MGL_CF_RECEIVED_SETUP)
					IF GET_CLIENT_FLAG(playerBD[PARTICIPANT_ID_TO_INT()], MGL_CF_MG_SETUP_PLAYER)
						
						//### CHECK IF MG START REQUESTED
						IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
						AND IS_BIT_SET(serverBD.iMGHasEnoughPlayers, minigame.iMinigameID)
						AND NOT bInteractionActive
							CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "MINIGAME START CONTROLL PRESSED")
							SET_INTERACTION_STAGE(sLocalData, MIS_START_MINIGAME)
						ENDIF

						//### SHOW DARTS MENU IF DARTS MG ONLY
						IF minigame.acType = eMGID_DARTS
							sMinigameUpdate.hasSetupMenu = TRUE
							IF LOAD_MENU_ASSETS()
								MENU_CHECK_INPUT()
								REBUILD_DARTS_MENU(minigame)
								DRAW_MENU()
							ENDIF
						ENDIF
																	
						TOGGLE_HELP_TEXT(GET_MG_TEXT(minigame, MGT_WAIT_FOR_LEADER), FALSE)
						
						//check if server thinks there is enough players in the area
						IF IS_BIT_SET(serverBD.iMGHasEnoughPlayers, minigame.iMinigameID)

							//If Darts menu , hide normal help text.
							IF sMinigameUpdate.hasSetupMenu 
								TOGGLE_HELP_TEXT(GET_MG_TEXT(minigame, MGT_SINGLE_PLAYER), FALSE)
								TOGGLE_HELP_TEXT(GET_MG_TEXT(minigame, MGT_MULTI_PLAYER), FALSE)	
							ELSE
								IF IS_BIT_SET(serverBD.iMGCanHaveMorePlayers, minigame.iMinigameID)
									TOGGLE_HELP_TEXT(GET_MG_TEXT(minigame, MGT_SINGLE_PLAYER))
								ELSE
									TOGGLE_HELP_TEXT(GET_MG_TEXT(minigame, MGT_MULTI_PLAYER))
								ENDIF
							ENDIF
						ELSE
							TOGGLE_HELP_TEXT(GET_MG_TEXT(minigame, MGT_SINGLE_PLAYER), FALSE)
							TOGGLE_HELP_TEXT(GET_MG_TEXT(minigame, MGT_MULTI_PLAYER), FALSE)
							
							IF NOT sMinigameUpdate.hasSetupMenu
								//text if game did not meet the required user count.
								TOGGLE_HELP_TEXT(GET_MG_TEXT(minigame, MGT_WAIT_FOR_LEADER), FALSE)
								TOGGLE_HELP_TEXT(GET_MG_TEXT(minigame, MGT_WAIT_FOR_PLAYER))
							ENDIF
						ENDIF
						
					ELSE // Not leader
					
						//if max player count was not reached, register palyer as a valid new palyer.
						IF IS_BIT_SET(serverBD.iMGCanHaveMorePlayers, playerBD[PARTICIPANT_ID_TO_INT()].iCurrentMinigame)
							SET_CLIENT_FLAG(playerBD[PARTICIPANT_ID_TO_INT()], MGL_CF_MG_VALID_PLAYER)
						ENDIF
						//all valid players should wait for leader to start the minigame
						IF GET_CLIENT_FLAG(playerBD[PARTICIPANT_ID_TO_INT()], MGL_CF_MG_VALID_PLAYER)
							//wait for leader to start.
							TOGGLE_HELP_TEXT(GET_MG_TEXT(minigame, MGT_WAIT_FOR_LEADER))
						ELSE
							TOGGLE_HELP_TEXT(GET_MG_TEXT(minigame, MGT_WAIT_FOR_LEADER), FALSE)
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE MIS_START_MINIGAME
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_UD)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_LR)
				
				//### REQEST SERVER TO LAUNCH THE SCRIPT FOR ALL PLAYERS
				EVENT_CLIENT_REQUEST_LAUNCH(GET_PLAYER_INDEX(), minigame.iMinigameID)
				
				SET_CLIENT_FLAG(playerBD[PARTICIPANT_ID_TO_INT()], MGL_CF_MG_START_PRESSED)
				
				SAFE_BIT_TOGGLE(iLocalFlags, ENUM_TO_INT(LPF_DISABLE_CELLPHONE))
				SAFE_BIT_TOGGLE(iLocalFlags, ENUM_TO_INT(LPF_HIDE_HUD))
				SAFE_BIT_TOGGLE(iLocalFlags, ENUM_TO_INT(LPF_DISABLE_PAUSE_MENU))
				
				RESTART_TIMER_NOW(TimerAfterGame)
				
				CANCEL_TIMER(stDelaySetupView)
				CANCEL_TIMER(stPerLeaderCheck)
				SET_INTERACTION_STAGE(sLocalData, MIS_IDLE)	
			BREAK
			
			CASE MIS_CANCEL_READY
				bHasMenuOpenSoundPlayed = FALSE
				sMinigameUpdate.bHideText = TRUE
				
				SET_CLIENT_FLAG(playerBD[PARTICIPANT_ID_TO_INT()], MGL_CF_MG_VALID_PLAYER, 	FALSE)
				SET_CLIENT_FLAG(playerBD[PARTICIPANT_ID_TO_INT()], MGL_CF_MG_SETUP_PLAYER, 	FALSE)
				SET_CLIENT_FLAG(playerBD[PARTICIPANT_ID_TO_INT()], MGL_CF_RECEIVED_SETUP, 	FALSE)
				SET_CLIENT_FLAG(playerBD[PARTICIPANT_ID_TO_INT()], MGL_CG_MG_SIDE_SELECTED, FALSE)
				SET_CLIENT_FLAG(playerBD[PARTICIPANT_ID_TO_INT()], MGL_CF_MG_A_SIDE_PLAYER, FALSE)
				
				SET_CLIENT_FLAG(playerBD[PARTICIPANT_ID_TO_INT()], MGL_CF_MG_RUNNING, FALSE)
				
				RESET_HUD(iLocalFlags)
				
				CANCEL_TIMER(stDelaySetupView)
				CANCEL_TIMER(stPerLeaderCheck)
						
				UPDATE_MGID(playerBD[PARTICIPANT_ID_TO_INT()], ENUM_TO_INT(eMGID_NONE))
						
				RESTART_TIMER_NOW(TimerAfterGame)
				CLEAR_MINIGAME_UPDATE(sMinigameUpdate)
				CLEAR_HELP()
				SET_INTERACTION_STAGE(sLocalData, MIS_IDLE)
			BREAK
	ENDSWITCH

	ELSE
		//sMinigameUpdate.bHideText = TRUE
	ENDIF
	
	IF NOT sMinigameUpdate.bSelfInsideSpecific
	AND sLocalData.eCurInteractStage = MIS_IDLE
		IF bHasMenuOpenSoundPlayed
			bHasMenuOpenSoundPlayed = FALSE
		ENDIF
		RESET_HUD(iLocalFlags)
	ENDIF
	
	IF NOT sMinigameUpdate.bSelfInsideSpecific
	AND NOT sMinigameUpdate.bAcceptableDistance
		SET_CLIENT_FLAG(playerBD[PARTICIPANT_ID_TO_INT()], MGL_CF_MG_A_SIDE_PLAYER, FALSE)
		IF sLocalData.eCurInteractStage > MIS_IDLE
		AND sLocalData.eCurInteractStage <> MIS_CANCEL_READY
			CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "MIS_CANCEL_READY.")
			SET_INTERACTION_STAGE(sLocalData, MIS_CANCEL_READY)
		ENDIF
	ENDIF
		
	IF sMinigameUpdate.bHideText
		CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "Hide Help Text.", minigame.iMinigameID)
		TOGGLE_HELP_TEXT(GET_MG_TEXT(minigame, MGT_TOO_MANY_P), FALSE)
		TOGGLE_HELP_TEXT(GET_MG_TEXT(minigame, MGT_IN_PROGRESS), FALSE)
		TOGGLE_HELP_TEXT(GET_MG_TEXT(minigame, MGT_SINGLE_PLAYER), FALSE)
		TOGGLE_HELP_TEXT(GET_MG_TEXT(minigame, MGT_MULTI_PLAYER), FALSE)
		TOGGLE_HELP_TEXT(GET_MG_TEXT(minigame, MGT_WAIT_FOR_PLAYER), FALSE)
		TOGGLE_HELP_TEXT(GET_MG_TEXT(minigame, MGT_WAIT_FOR_LEADER), FALSE)
		TOGGLE_HELP_TEXT(GET_MG_TEXT(minigame, MGT_READY_FOR_GAME), FALSE)
		TOGGLE_HELP_TEXT("ARMW_A_SIDE", FALSE)
	ENDIF
	
ENDPROC

PROC LOOP_LAUNCH_CHECK(MINIGAME_LAUNCH &minigame[CONST_MINIGAME_COUNT])
	INT mini
	FOR mini = 0 TO CONST_MINIGAME_COUNT - 1
		IF minigame[mini].bRequestLaunch
			MINIGAME_LAUNCH_CHECK(minigame[mini])
		ENDIF
	ENDFOR
ENDPROC

PROC UPDATE_MAD(AML_GENERAL_STRUCT &sState)

	BOOL bAllowInteractUpdate = TRUE
	BOOL bIsWalking = TRUE
	
	//if phone is on screen do not update the activity, below still update to check if an instance of a game needs to start for spectator.
	IF IS_PHONE_ONSCREEN()
	OR IS_PAUSE_MENU_ACTIVE()
	OR IS_INTERACTION_MENU_OPEN() 
	OR IS_BROWSER_OPEN()
		bAllowInteractUpdate = FALSE
		bInteractionActive = TRUE //global
	ELSE
		IF bInteractionActive
			RESTART_TIMER_NOW(TimerAfterGame)
			bInteractionActive = FALSE
		ENDIF
	ENDIF
	//SPEED
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		IF NOT IS_PED_DEAD_OR_DYING(PLAYER_PED_ID())
			bIsWalking = NOT IS_PED_RUNNING(PLAYER_PED_ID())
		ENDIF
	ENDIF
	
	//VVV Update Struct
	sState.bAllowIntractUpdate = TRUE
	IF NOT ALLOW_INTERACTION_UPDATE(TimerAfterGame)
	OR NOT bAllowInteractUpdate
	OR NOT bIsWalking
		sState.bAllowIntractUpdate = FALSE
	ENDIF
	
	sState.bPlayerWalking = bIsWalking
	sState.iMGCurrent = playerBD[PARTICIPANT_ID_TO_INT()].iCurrentMinigame
	
	INT iCurrentMinigame
	FOR iCurrentMinigame = 0 TO CONST_MINIGAME_COUNT -1
		sState.bIsMinigameActiveNet[iCurrentMinigame] = NETWORK_IS_SCRIPT_ACTIVE(arrayMinigameInfo[iCurrentMinigame].sScriptName, sState.iApartmentInstance)
		sState.bIsMinigameActiveLocal[iCurrentMinigame] = NETWORK_IS_SCRIPT_ACTIVE(arrayMinigameInfo[iCurrentMinigame].sScriptName, sState.iApartmentInstance, TRUE)
		
		
//		IF (GET_GAME_TIMER() % 1000) < 50
//		AND iCurrentMinigame = 0
//			CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, 
//			"CAn have more?.", IS_BIT_SET(serverBD.iMGCanHaveMorePlayers, iCurrentMinigame),
//			" Has setup?.", IS_BIT_SET(serverBD.iMGHasSetupPlayer, iCurrentMinigame)
//			
//			)	
//		ENDIF
		
		BOOL canHavePlayers = FALSE
		IF IS_BIT_SET(serverBD.iMGCanHaveMorePlayers, iCurrentMinigame)
		//AND IS_BIT_SET(serverBD.iMGHasSetupPlayer, iCurrentMinigame)
		AND NOT NETWORK_IS_SCRIPT_ACTIVE(arrayMinigameInfo[iCurrentMinigame].sScriptName,  sState.iApartmentInstance)
		AND GET_MG_PLAYER_COUNT(playerBD, iCurrentMinigame) > 0
			canHavePlayers = TRUE
		ENDIF
		
		sState.bCanHaveMorePlayers[iCurrentMinigame] = canHavePlayers
	ENDFOR
ENDPROC

PROC LOOP_MINIGAMES(MAD_STRUCT &sState, AML_GENERAL_STRUCT &sGeneralInfo, STRUCT_LOCAL_CLIENT_DATA &sLocalDataL)

	UPDATE_MAD(sGeneralInfo)
		//CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "SETUP??", IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iBitMaskActivities, MGL_CF_RECEIVED_SETUP))
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		IF NOT IS_PED_DEAD_OR_DYING(PLAYER_PED_ID())

			IF playerBD[PARTICIPANT_ID_TO_INT()].iCurrentMinigame = ENUM_TO_INT(eMGID_NONE)
				
				IF ALLOW_INTERACTION_UPDATE(TimerAfterGame)
					MINIGAME_AVAILABILITY_DISPLAY(sState, sGeneralInfo)
				ENDIF
				
				INT iCurrentMinigame
				FOR iCurrentMinigame = 0 TO CONST_MINIGAME_COUNT -1	
					IF playerBD[PARTICIPANT_ID_TO_INT()].iCurrentMinigame = ENUM_TO_INT(eMGID_NONE)
						IF NOT IS_BIT_SET(sLocalDataL.iMGDisabledBitSet, iCurrentMinigame)
							UPDATE_MINIGAME(arrayMinigameInfo[iCurrentMinigame], sGeneralInfo.bAllowIntractUpdate)
						ENDIF
					ENDIF
				ENDFOR
			ELSE
				IF NOT IS_BIT_SET(sLocalDataL.iMGDisabledBitSet, playerBD[PARTICIPANT_ID_TO_INT()].iCurrentMinigame)
					UPDATE_MINIGAME(arrayMinigameInfo[sGeneralInfo.iMGCurrent], sGeneralInfo.bAllowIntractUpdate)
				ELSE
					//If minigame was disabled while in it , update the cancel state.
					SET_INTERACTION_STAGE(sLocalDataL, MIS_CANCEL_READY)
					UPDATE_MINIGAME(arrayMinigameInfo[playerBD[PARTICIPANT_ID_TO_INT()].iCurrentMinigame], TRUE)
				ENDIF
			ENDIF

		ENDIF
	ELSE
		IF (GET_GAME_TIMER() % 1000) < 50
			IF IS_TIMER_STARTED(TimerAfterGame)
				CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "Interaction is not allowed.", GET_TIMER_IN_SECONDS(TimerAfterGame))	
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SERVER_CHECK_ACTIVITY_COUNT()
	
	INT iMGRequested
	iMGRequested = ENUM_TO_INT(eMGID_NONE)
	INT iMGRequestByUser
	iMGRequestByUser = -1
	
	//### RECEIVE EVENTS FROM CLIENTS
	INT iEvent
	iEvent = 0
	REPEAT GET_NUMBER_OF_EVENTS(SCRIPT_EVENT_QUEUE_NETWORK) iEvent
		SWITCH GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_NETWORK, iEvent)		
			CASE EVENT_NETWORK_SCRIPT_EVENT
				sEventServer.Details.Type = SCRIPT_EVENT_GENERAL
				IF RECEIVE_EVENT(iEvent, sEventServer, TRUE)
						CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "SERVER - EVENT RECEIVED: ", GET_MGL_EVENT_STRING(sEventServer.Details.Type))
						SWITCH(sEventServer.Details.Type)
							CASE SCRIPT_EVENT_MG_LAUNCH_REQUEST
								iMGRequested = sEventServer.iMinigameID
								iMGRequestByUser = NATIVE_TO_INT(sEventServer.piRequestPlayerID)
								CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "SERVER - Minigame START Requested: ",iMGRequested, " ByUser:", GET_PLAYER_NAME(INT_TO_NATIVE(PLAYER_INDEX, iMGRequestByUser)))
							BREAK
							
							CASE SCRIPT_EVENT_MG_INTERACT_CHANGED
								CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "SERVER - Interact Changed:", GET_PLAYER_NAME(sEventServer.piRequestPlayerID), 
								" InteractState: ", EVENT_FLAG_GET(sEventServer.iEventFlags, EF_INTERACT_ACTIVE))

								IF EVENT_FLAG_GET(sEventServer.iEventFlags, EF_INTERACT_ACTIVE)
									IF arrayMinigameUsers[sEventServer.iMinigameID] <= arrayMinigameInfo[sEventServer.iMinigameID].iMaxPlayers
										EVENT_SERVER_ALLOW_PARTICIPATE(sEventServer.piRequestPlayerID, sEventServer.iMinigameID)
									ELSE
										EVENT_SERVER_ALLOW_PARTICIPATE(sEventServer.piRequestPlayerID, sEventServer.iMinigameID, FALSE)
									ENDIF							
								ENDIF
							BREAK
						ENDSWITCH
				ENDIF
			BREAK
		ENDSWITCH
	ENDREPEAT

	INT iParticipantID
	INT iMGID
	iMGID = 0
	
	REPEAT CONST_MINIGAME_COUNT iMGID
		arrayMinigameUsers[iMGID] = GET_MG_PLAYER_COUNT(playerBD, iMGID)
		
		PARTICIPANT_INDEX pariCurrent
		BOOL bPlayerCleared = FALSE
		BOOL bActivePlayerFound = FALSE
		
		REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipantID
			pariCurrent = INT_TO_PARTICIPANTINDEX(iParticipantID)
			IF NETWORK_IS_PARTICIPANT_ACTIVE(pariCurrent)
			
				//%%% Check if MG Active
				IF GET_CLIENT_FLAG(playerBD[iParticipantID], MGL_CF_MG_VALID_PLAYER)
				AND GET_CLIENT_FLAG(playerBD[iParticipantID], MGL_CF_MG_RUNNING)
				AND playerBD[iParticipantID].iCurrentMinigame = iMGID
					bActivePlayerFound = TRUE
				ENDIF	
				
				//%%% Check Too Many		
				IF arrayMinigameUsers[iMGID] > arrayMinigameInfo[iMGID].iMaxPlayers
					CDEBUG2LN(DEBUG_MG_LAUNCHER_APART, "Current:", iParticipantID, " MAX", arrayMinigameInfo[iMGID].iMaxPlayers)
					IF NOT bPlayerCleared
						IF playerBD[iParticipantID].iCurrentMinigame = iMGID
						AND GET_CLIENT_FLAG(playerBD[iParticipantID], MGL_CF_MG_VALID_PLAYER)
							EVENT_USER(SCRIPT_EVENT_MG_FORCE_STOP_INTERACT, NETWORK_GET_PLAYER_INDEX(pariCurrent), iMGID)
							bPlayerCleared = TRUE
							CDEBUG2LN(DEBUG_MG_LAUNCHER_APART, "Too Many Players, Sending To Cancel To: ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(pariCurrent)))
						ENDIF
					ENDIF
				ENDIF		

			ENDIF
		ENDREPEAT
			
		SAFE_BIT_TOGGLE(serverBD.iMGRunning, iMGID, bActivePlayerFound)
		
		//### PLAYER AVAILABILITY CHECK
		SAFE_BIT_TOGGLE(serverBD.iMGCanHaveMorePlayers, iMGID, (arrayMinigameUsers[iMGID] < arrayMinigameInfo[iMGID].iMaxPlayers))
		SAFE_BIT_TOGGLE(serverBD.iMGHasEnoughPlayers, iMGID, (arrayMinigameUsers[iMGID] >= arrayMinigameInfo[iMGID].iMinPlayers))
				
		//@@@ Check for Setup Player
		IF arrayMinigameUsers[iMGID] > 0
			IF NOT IS_TIMER_STARTED(stPerSetupCheck)
				START_TIMER_AT(stPerSetupCheck, 1.0)
			ELIF GET_TIMER_IN_SECONDS(stPerSetupCheck) >= 1.0
				RESTART_TIMER_NOW(stPerSetupCheck)
				
				INT iFoundParticipant = -1
				BOOL iFoundSetup = FALSE
				REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipantID
					IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipantID))
						IF playerBD[iParticipantID].iCurrentMinigame = iMGID
						AND GET_CLIENT_FLAG(playerBD[iParticipantID], MGL_CF_MG_VALID_PLAYER)
							iFoundParticipant = iParticipantID
							//CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "SERVER - Found a MG: ", iMGID, " Player: ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipantID))))
							IF GET_CLIENT_FLAG(playerBD[iParticipantID], MGL_CF_MG_SETUP_PLAYER)
								iFoundSetup = TRUE
							ENDIF
						ELSE
							SWITCH iMGID
								CASE 0 //darts
									IF IS_BIT_SET(serverBD.iMGDartsSetupPlayer, iParticipantID)
										CLEAR_BIT(serverBD.iMGDartsSetupPlayer, iParticipantID)
									ENDIF
								BREAK
								
								CASE 1 //arm
									IF IS_BIT_SET(serverBD.iMGArmWSetupPlayer, iParticipantID)
										CLEAR_BIT(serverBD.iMGArmWSetupPlayer, iParticipantID)
									ENDIF
								BREAK
							ENDSWITCH
							
						ENDIF
					ENDIF
				ENDREPEAT
				
				IF NOT iFoundSetup
					CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "SERVER - Setup player was not found for: ", iMGID)
					IF NOT SET_PLAYER_AS_SETUP_ON_SERVER(INT_TO_ENUM(ENUM_CLUBHOUSE_MGID, iMGID), serverBD, iFoundParticipant)
						CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "SERVER - Setting new Setup player failed: ", iMGID)
					ENDIF
				ENDIF
			ENDIF		
		ENDIF
		
		//### MINIGAME START REQUEST CHECK
		IF iMGID = iMGRequested
			CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "SERVER MG START - Server Received Request to Start MinigameID: ", iMGRequested)
			//cehck if  minigame has min users ready.
			IF arrayMinigameUsers[iMGID] >= arrayMinigameInfo[iMGID].iMinPlayers
				CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "SERVER MG START - Minigame has minimum players")
					
				GET_APARTMENT_INTERACT_INFO(sMGInfoServer, atCurrentApartment, INT_TO_ENUM(ENUM_CLUBHOUSE_MGID, iMGID))
				PLAYER_INDEX piClosestToAPos = GET_CLOSEST_TO_MG_POS_A(playerBD, sMGInfoServer, iMGID)

				INT	iPlayersLaunched = 0
				INT	iPlayersMax = arrayMinigameInfo[iMGID].iMaxPlayers
				
				//For debugging
				INT iRandomEventID = GET_RANDOM_INT_IN_RANGE()
				REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipantID
					IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipantID))

						PLAYER_INDEX piParticipant = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipantID))		
						IF 	GET_CLIENT_FLAG(playerBD[iParticipantID], MGL_CF_MG_VALID_PLAYER)
						AND playerBD[iParticipantID].iCurrentMinigame = iMGID
							IF iPlayersLaunched <= iPlayersMax
								CDEBUG1LN(DEBUG_MG_LAUNCHER_APART,"SERVER MG START - Script Launch request as Normal for Player: ", GET_PLAYER_NAME(piParticipant))
								
								BOOL bIsSetupPlayer
								bIsSetupPlayer = FALSE
								IF NATIVE_TO_INT(piParticipant) = iMGRequestByUser
									bIsSetupPlayer = TRUE
									IF NATIVE_TO_INT(piClosestToAPos) = -1
										CDEBUG1LN(DEBUG_MG_LAUNCHER_APART,"Closest not initailized!")
										piClosestToAPos = piParticipant
									ENDIF
								ENDIF
								
								BOOL bIsClosestToPosA
								bIsClosestToPosA = FALSE
								IF piClosestToAPos = piParticipant
									bIsClosestToPosA = TRUE
								ENDIF
								
								EVENT_SERVER_MG_LAUNCH(iRandomEventID, piParticipant, iMGID, FALSE, bIsSetupPlayer, bIsClosestToPosA)
								iPlayersLaunched++
							ENDIF
						ENDIF
					ENDIF
				ENDREPEAT
			ELSE
				CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "SERVER MG START - Not enought users to start, Minigame ", iMGRequested, " Minigame Count: ", arrayMinigameUsers[iMGRequested])
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC CLEAR_USER_MINIGAME_BITS(PlayerBroadcastData& PBD)
	CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "PLAYER BITS CLEARED!")
	SET_CLIENT_FLAG(PBD, MGL_CF_MG_SETUP_PLAYER, FALSE)
	SET_CLIENT_FLAG(PBD, MGL_CF_MG_VALID_PLAYER, FALSE)
	SET_CLIENT_FLAG(PBD, MGL_CF_MG_START_REQUEST, FALSE)
	SET_CLIENT_FLAG(PBD, MGL_CF_MG_A_SIDE_PLAYER, FALSE)
ENDPROC

//PURPOSE: Process the stages for the ClientFFUNC
PROC PROCESS_CLIENT(AML_GENERAL_STRUCT &sCurrentV, MAD_STRUCT &sStateN, MINIGAME_LAUNCH &minigame[CONST_MINIGAME_COUNT])
	
	#IF IS_DEBUG_BUILD
		bAside = GET_CLIENT_FLAG(playerBD[PARTICIPANT_ID_TO_INT()], MGL_CF_MG_A_SIDE_PLAYER)
		bSideSelected = GET_CLIENT_FLAG(playerBD[PARTICIPANT_ID_TO_INT()], MGL_CG_MG_SIDE_SELECTED)
		bInsideClubhouse = IS_INSIDE_CLUBHOUSE()
	
		IF bRequestToBeHost
			bRequestToBeHost = FALSE
			NETWORK_REQUEST_TO_BE_HOST_OF_THIS_SCRIPT()
		ENDIF
		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			bIsHost = TRUE
		ELSE
			bIsHost = FALSE
		ENDIF
	#ENDIF
	
	IF NOT IS_PED_DEAD_OR_DYING(PLAYER_PED_ID())
		IF IS_PLAYER_EXITING_PROPERTY(GET_PLAYER_INDEX())
			SWITCH_LAUNCHER_STAGE(playerBD[PARTICIPANT_ID_TO_INT()], eAD_PRE_CLEANUP)
		ENDIF
	ENDIF
		
	PROCESS_EVENTS(playerBD, sLocalData, minigame, arrayMinigameInfo)
	LOOP_LAUNCH_CHECK(minigame)
	
	SWITCH playerBD[PARTICIPANT_ID_TO_INT()].eStage		
		CASE eAD_IDLE
			IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iBitMaskActivities, ENUM_TO_INT(MGL_CF_MG_START_REQUEST))
				LOOP_MINIGAMES(sStateN, sCurrentV, sLocalData)
			ENDIF
			
			IF IS_BIT_SET_ENUM(playerBD[PARTICIPANT_ID_TO_INT()].iBitMaskActivities, MGL_CF_MG_START_PRESSED)
				IF NOT IS_TIMER_STARTED(stTimerStartEventWait)
					CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "eAD_IDLE - stTimerStartEventWait STARTED")
					START_TIMER_NOW(stTimerStartEventWait)
				ELIF GET_TIMER_IN_SECONDS(stTimerStartEventWait) >= 4.0
					CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "eAD_IDLE - stTimerStartEventWait >= 5 seconds")
					
					SET_CLIENT_FLAG(playerBD[PARTICIPANT_ID_TO_INT()], MGL_CF_MG_START_PRESSED, FALSE)
					SET_CLIENT_FLAG(playerBD[PARTICIPANT_ID_TO_INT()], MGL_CF_MG_START_REQUEST, FALSE)
					SET_CLIENT_FLAG(playerBD[PARTICIPANT_ID_TO_INT()], MGL_CF_MG_RUNNING, FALSE)
					
					UPDATE_MGID(playerBD[PARTICIPANT_ID_TO_INT()], ENUM_TO_INT(eMGID_NONE))
					CLEAR_USER_MINIGAME_BITS(playerBD[PARTICIPANT_ID_TO_INT()])
					SWITCH_LAUNCHER_STAGE(playerBD[PARTICIPANT_ID_TO_INT()], eAD_IDLE)
					SET_INTERACTION_STAGE(sLocalData, MIS_IDLE)
					RESET_HUD(iLocalFlags)
					RESTART_TIMER_NOW(TimerAfterGame)
					CANCEL_TIMER(stTimerStartEventWait)
				ENDIF
			ENDIF
		BREAK	
		
		CASE eAD_LOAD_MINIGAME
			IF playerBD[PARTICIPANT_ID_TO_INT()].iCurrentMinigame >= 0
				IF NOT IS_STRING_NULL_OR_EMPTY(sLocalData.sClientEvents.sEventMGScriptName)
					IF HAS_SCRIPT_LOADED(sLocalData.sClientEvents.sEventMGScriptName)
						CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "eAD_LOAD_MINIGAME - Script Loaded: ", sLocalData.sClientEvents.sEventMGScriptName)						
						SWITCH_LAUNCHER_STAGE(playerBD[PARTICIPANT_ID_TO_INT()], eAD_START_MINIGAME)
					ELSE
						CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "eAD_LOAD_MINIGAME - Script not loaded yet: ", sLocalData.sClientEvents.sEventMGScriptName)
						IF DOES_SCRIPT_EXIST(sLocalData.sClientEvents.sEventMGScriptName)
							REQUEST_SCRIPT(sLocalData.sClientEvents.sEventMGScriptName)
						ELSE
							CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "eAD_LOAD_MINIGAME - Script does not exist", sLocalData.sClientEvents.sEventMGScriptName)
						ENDIF
					ENDIF
				ELSE
					CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "eAD_LOAD_MINIGAME - Invalid Script Name", sLocalData.sClientEvents.sEventMGScriptName)
				ENDIF
			ELSE
				CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "eAD_LOAD_MINIGAME - invalid minigame ID: ", playerBD[PARTICIPANT_ID_TO_INT()].iCurrentMinigame)	
				IF IS_TIMER_STARTED(stTimerStartEventWait)
					CANCEL_TIMER(stTimerStartEventWait)
				ENDIF
				SET_CLIENT_FLAG(playerBD[PARTICIPANT_ID_TO_INT()], MGL_CF_MG_START_PRESSED, FALSE)
				SET_CLIENT_FLAG(playerBD[PARTICIPANT_ID_TO_INT()], MGL_CF_MG_START_REQUEST, FALSE)
				SWITCH_LAUNCHER_STAGE(playerBD[PARTICIPANT_ID_TO_INT()], eAD_IDLE)
			ENDIF
		BREAK
		
		CASE eAD_START_MINIGAME	
		
			IF NOT IS_TIMER_STARTED(stTimerFailSafe)
				START_TIMER_NOW(stTimerFailSafe)
			ENDIF
			
			IF IS_TIMER_STARTED(stTimerStartEventWait)
				CANCEL_TIMER(stTimerStartEventWait)
			ENDIF
				
			SET_CLIENT_FLAG(playerBD[PARTICIPANT_ID_TO_INT()], MGL_CF_MG_START_PRESSED, FALSE)
			SET_CLIENT_FLAG(playerBD[PARTICIPANT_ID_TO_INT()], MGL_CF_MG_RUNNING)	
			bAllowScriptLaunch = TRUE
						
			IF NOT GET_CLIENT_FLAG(playerBD[PARTICIPANT_ID_TO_INT()], MGL_CF_MG_SETUP_PLAYER)
			AND NOT NETWORK_IS_SCRIPT_ACTIVE(sLocalData.sClientEvents.sEventMGScriptName, sLocalData.iApartmentVisibilityID , FALSE)	
				bAllowScriptLaunch = FALSE
				CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "Script Cant Launch, no instance active")
				
				IF GET_TIMER_IN_SECONDS(stTimerFailSafe) >= 30.0
					CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "eAD_START_MINIGAME - minigame: ", sLocalData.sClientEvents.sEventMGScriptName, " was not started, time out after - 30")
					SWITCH_LAUNCHER_STAGE(playerBD[PARTICIPANT_ID_TO_INT()], eAD_WAIT_END_MINIGAME)
				ENDIF
			ENDIF
			
			IF bAllowScriptLaunch
				CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "eAD_START_MINIGAME - START MINIGAME: ", sLocalData.sClientEvents.sEventMGScriptName)
				IF playerBD[PARTICIPANT_ID_TO_INT()].iCurrentMinigame = 0
					CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "GLOBAL LEGS: ", g_FMMC_STRUCT.iNumberOfLegs, " SETS:", g_FMMC_STRUCT.iNumberOfSets)
					CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "LOCAL LEGS: ", iArrayDartNumOfLegs[0].iValue, " SETS:", iArrayDartNumOfSets[iCurrentSets].iValue)
				ENDIF
				
				//@@@ Start Minigame Script
				IF START_CURRENT_MINIGAME(
				sLocalData.sClientEvents.sEventMGScriptName,
				SCRIPT_XML_STACK_SIZE, 
				sLocalData.iApartmentVisibilityID, //Instance ID
				FALSE, //Spec
				GET_CLIENT_FLAG(playerBD[PARTICIPANT_ID_TO_INT()], MGL_CF_MG_SETUP_PLAYER), 
				GET_CLIENT_FLAG(playerBD[PARTICIPANT_ID_TO_INT()], MGL_CF_MG_A_SIDE_PLAYER))
		
					CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "eAD_START_MINIGAME - Minigame script start success: ", sLocalData.sClientEvents.sEventMGScriptName)
					CANCEL_TIMER(TimerAfterGame)
					CANCEL_TIMER(stTimerWaitStarted)
					bEndScriptWait = FALSE
					SET_CLIENT_FLAG(playerBD[PARTICIPANT_ID_TO_INT()], MGL_CF_RECEIVED_SETUP, FALSE)
					SET_CLIENT_FLAG(playerBD[PARTICIPANT_ID_TO_INT()], MGL_CG_MG_SIDE_SELECTED, FALSE)
					SAFE_BIT_TOGGLE(iLocalFlags,  ENUM_TO_INT(LPF_DISABLE_PAUSE_MENU), FALSE)
					bAllowScriptLaunch = FALSE
					
					SWITCH_LAUNCHER_STAGE(playerBD[PARTICIPANT_ID_TO_INT()], eAD_WAIT_ACTIVE_MINIGAME)
				ENDIF
			ENDIF
		BREAK
		
		CASE eAD_WAIT_ACTIVE_MINIGAME
			//Timer
			IF NOT IS_TIMER_STARTED(stTimerWaitStarted)
				START_TIMER_NOW(stTimerWaitStarted)
				bEndScriptWait = FALSE
				CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "eAD_WAIT_ACTIVE_MINIGAME - Started timer, waiting for mg script to be active.")
			ELIF GET_TIMER_IN_SECONDS(stTimerWaitStarted) >= CONST_MG_SCRIPT_ACTIVE_WAIT
				bEndScriptWait = TRUE
				CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "eAD_WAIT_ACTIVE_MINIGAME - Timer reached end.", CONST_MG_SCRIPT_ACTIVE_WAIT)
			ENDIF
			
			IF NOT IS_STRING_NULL_OR_EMPTY(sLocalData.sClientEvents.sEventMGScriptName)
				IF NETWORK_IS_SCRIPT_ACTIVE(sLocalData.sClientEvents.sEventMGScriptName, sLocalData.iApartmentVisibilityID, TRUE)
					CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "eAD_WAIT_ACTIVE_MINIGAME - Script is active.", sLocalData.sClientEvents.sEventMGScriptName, " INSTANCE ID", sLocalData.iApartmentVisibilityID)
					bEndScriptWait = TRUE
				ENDIF
			ELSE
				IF GET_GAME_TIMER() % 1000 > 100
					CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "eAD_WAIT_ACTIVE_MINIGAME - Script name is empty or null.", sLocalData.sClientEvents.sEventMGScriptName)
				ENDIF
			ENDIF
			
			IF bEndScriptWait
				CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "eAD_WAIT_ACTIVE_MINIGAME - EndScriptWait. Script Active?", NETWORK_IS_SCRIPT_ACTIVE(sLocalData.sClientEvents.sEventMGScriptName, sLocalData.iApartmentVisibilityID, TRUE), 
				" Time:", GET_TIMER_IN_SECONDS(stTimerWaitStarted))
				
				bEndScriptWait = FALSE
				CANCEL_TIMER(stTimerWaitStarted)
				SWITCH_LAUNCHER_STAGE(playerBD[PARTICIPANT_ID_TO_INT()], eAD_WAIT_END_MINIGAME)
			ENDIF
		BREAK
		
		CASE eAD_WAIT_END_MINIGAME			
			IF NOT NETWORK_IS_SCRIPT_ACTIVE(sLocalData.sClientEvents.sEventMGScriptName, sLocalData.iApartmentVisibilityID, TRUE)
			OR (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("AM_Darts_Apartment")) = 0 AND sLocalData.sClientEvents.iEventMGID = ENUM_TO_INT(eMGID_DARTS))
			OR (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("AM_Armwrestling_Apartment")) = 0 AND sLocalData.sClientEvents.iEventMGID = ENUM_TO_INT(eMGID_ARMWRESTLE))
				#IF IS_DEBUG_BUILD
					CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "eAD_WAIT_END_MINIGAME - Minigame is no loner active locally, Name: ", sLocalData.sClientEvents.sEventMGScriptName, " InstanceID: ", sLocalData.iApartmentVisibilityID)
					CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "eAD_WAIT_END_MINIGAME - Script Active locally? ", NETWORK_IS_SCRIPT_ACTIVE(sLocalData.sClientEvents.sEventMGScriptName, sLocalData.iApartmentVisibilityID, TRUE))
					CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "eAD_WAIT_END_MINIGAME - Darts threads running? ", GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("AM_Darts_Apartment")) = 0 AND sLocalData.sClientEvents.iEventMGID = ENUM_TO_INT(eMGID_DARTS))
					CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "eAD_WAIT_END_MINIGAME - Arm Wreslte Threads running? ", GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("AM_Armwrestling_Apartment")) = 0 AND sLocalData.sClientEvents.iEventMGID = ENUM_TO_INT(eMGID_ARMWRESTLE))
				#ENDIF
				
				RESET_HUD(iLocalFlags)
				CLEAR_USER_MINIGAME_BITS(playerBD[PARTICIPANT_ID_TO_INT()])	
				SET_CLIENT_FLAG(playerBD[PARTICIPANT_ID_TO_INT()], MGL_CF_MG_RUNNING, FALSE)	
				UPDATE_MGID(playerBD[PARTICIPANT_ID_TO_INT()], ENUM_TO_INT(eMGID_NONE))
				RESTART_TIMER_NOW(TimerAfterGame)
				SET_INTERACTION_STAGE(sLocalData, MIS_IDLE)
				SWITCH_LAUNCHER_STAGE(playerBD[PARTICIPANT_ID_TO_INT()], eAD_IDLE)
			ELSE
				#IF IS_DEBUG_BUILD
					IF GET_FRAME_COUNT() % (60 * 5) = 0 
						CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "eAD_WAIT_END_MINIGAME - Waiting for Minigame: ", sLocalData.sClientEvents.sEventMGScriptName, " with Instance ID: ", sLocalData.iApartmentVisibilityID, " to end Locally")
					ENDIF
				#ENDIF
			ENDIF
		BREAK	
		
		CASE eAD_PRE_CLEANUP
			//run once and wait for player to exit aparmtnet, state will switch to cleanup 
			IF NOT bPreCleanup
				bPreCleanup = TRUE
				CLEAR_HELP()
				CLEAR_MENU_DATA()
			ENDIF
		BREAK
		
		CASE eAD_CLEANUP
			SCRIPT_CLEANUP()
		BREAK
	ENDSWITCH
	
	UPDATE_MG_AVAILABILITY(sLocalData.iMGDisabledBitSet)
	UPDATE_HUD(iLocalFlags)
ENDPROC

//PURPOSE: Process the JOYRIDER stages for the Server
PROC PROCESS_SERVER()
	//CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "Legs Num: ",g_FMMC_STRUCT.iNumberOfLegs, " Sets Num: ", g_FMMC_STRUCT.iNumberOfSets)
	//Add client processing loop here!
	SERVER_CHECK_ACTIVITY_COUNT()
	//Mission Stages
	SWITCH serverBD.eStage	
		CASE eAD_IDLE
		BREAK
	
		CASE eAD_WAIT_END_MINIGAME			
		BREAK
						
		CASE eAD_CLEANUP			
		BREAK
	ENDSWITCH
ENDPROC


//Helper function to get the servers game/mission state
FUNC INT GET_SERVER_MISSION_STATE()
	RETURN serverBD.iServerGameState
ENDFUNC

//Server only function that checks to see if the game mode should now end. For this test script, it if time has passed.
FUNC BOOL HAVE_MISSION_END_CONDITIONS_BEEN_MET()
	RETURN FALSE
ENDFUNC
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                      Main Loop                      //////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
SCRIPT
	
	//###Find what property a player is inside of.
	BOOL bInsideProperty
	INT iCurrentProperty

	IF NOT IS_TIMER_STARTED(stTimerInitialize)
		START_TIMER_NOW(stTimerInitialize)
	ENDIF
	
	BOOL bAllowPrint = FALSE
	CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "MINIGAME LAUNCHER - Start wait, until palyer is inside apartment.")
	WHILE NOT bInsideProperty
		WAIT(0) 
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			PRINTLN("[MPTUT] - MISSION END - SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE - SCRIPT CLEANUP 101 ")
			SCRIPT_CLEANUP()
		ENDIF
		
		IF GET_GAME_TIMER() % 1000 > 50
			bAllowPrint = TRUE
		ELSE
			bAllowPrint = FALSE
		ENDIF
		
		IF (NOT IS_PLAYER_ENTERING_PROPERTY(GET_PLAYER_INDEX())
		AND IS_PLAYER_IN_MP_PROPERTY(GET_PLAYER_INDEX(), FALSE))
		OR IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
		#IF FEATURE_DLC_2_2022
		OR IS_PLAYER_IN_JUGGALO_HIDEOUT_PROPERTY(PLAYER_ID())
		#ENDIF
			CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "INIT - Player Is Inside a Property: ", iCurrentProperty)
			
			IF IS_PLAYER_IN_CLUBHOUSE(GET_PLAYER_INDEX())
				CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "INIT - Player Is Inside a Clubhouse Property: ", iCurrentProperty)
				
				IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty <> -1
					CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "INIT -	Player iCurrentlyInsideProperty is valid: ", iCurrentProperty)
					
					iCurrentProperty = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty
					bInsideProperty = TRUE
					CANCEL_TIMER(stTimerInitialize)
				ELSE
					IF bAllowPrint
						CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "INIT - Player property ID is invalid: ", GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
					ENDIF
				ENDIF
			ELIF IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
				CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "INIT - Player Is Inside an Arcade Property: ", iCurrentProperty)
				
				bInsideProperty = TRUE
				CANCEL_TIMER(stTimerInitialize)
			#IF FEATURE_DLC_2_2022
			ELIF IS_PLAYER_IN_JUGGALO_HIDEOUT_PROPERTY(PLAYER_ID())
				CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "INIT - Player Is Inside a Juggalo Hideout Property: ", iCurrentProperty)
				
				bInsideProperty = TRUE
				CANCEL_TIMER(stTimerInitialize)
			#ENDIF
			ELSE
				IF bAllowPrint
					CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "INIT - Player is not in a clubhouse")
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT bInsideProperty
		AND GET_TIMER_IN_SECONDS_SAFE(stTimerInitialize) >= CONST_APARTMENT_CHECK_TIME
			CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "INIT - Could not initialize player as in property. Time passed:", CONST_APARTMENT_CHECK_TIME)
			SCRIPT_CLEANUP()
		ENDIF
	ENDWHILE

	IF NETWORK_IS_GAME_IN_PROGRESS()
		PRINTLN("MP: Starting mission ")
		// Vehry out all the initial game starting duties. 
		PROCESS_PRE_GAME()	
	ELSE
		PRINTLN("[MPTUT] - MISSION END - NETWORK_IS_GAME_IN_PROGRESS = FALSE - SCRIPT CLEANUP P ")
		SCRIPT_CLEANUP()
	ENDIF
	
//	INT iMGSetup
//	REPEAT CONST_MINIGAME_COUNT iMGSetup
//		iMGSetupParticipants[iMGSetup] = -1
//	ENDREPEAT
	
	iSelfID = PARTICIPANT_ID_TO_INT()
	sLocalData.iApartmentVisibilityID = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iVisibilityID
	
	#IF FEATURE_CASINO_HEIST
	IF IS_PLAYER_IN_ANY_SIMPLE_INTERIOR(PLAYER_ID())
		sLocalData.iApartmentVisibilityID = GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iInstance + 32
	ENDIF
	#ENDIF
	
	CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "INIT - Apartment Visibility ID:", sLocalData.iApartmentVisibilityID)
	//### INITIALIZE
	#IF IS_DEBUG_BUILD
		ADD_AM_DART_WIDGETS()
	#ENDIF
	
	UPDATE_MG_AVAILABILITY(sLocalData.iMGDisabledBitSet)
	PRINT_AVAILIBLE_MINIGAMES(sLocalData.iMGDisabledBitSet)
	MINIGAME_LAUNCH_CHECK_RESET(sMinigameLaunchTimer)

	CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "Property: ", iCurrentProperty)
	CDEBUG1LN(DEBUG_MG_LAUNCHER_APART,"")
	CDEBUG1LN(DEBUG_MG_LAUNCHER_APART,"")
	CDEBUG1LN(DEBUG_MG_LAUNCHER_APART,"%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
	CDEBUG1LN(DEBUG_MG_LAUNCHER_APART,"%%%%%%%%%% MINIGAME LAUNCHER SCRIPT STARTED %%%%%%%%")
	//preload menu assets
	LOAD_MENU_ASSETS()
	SETUP_DART_MENU(mriDartsMenu)
	
	//default values for menu 
	iCurrentMenuRow = 0
	
	SETUP_MENU_DARTS_INFO(iArrayDartNumOfLegs, iArrayDartNumOfSets)
	
	sCurrentValues.iApartmentInstance = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iVisibilityID
	
	CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "Property: ", iCurrentProperty)
	IF IS_PROPERTY_CLUBHOUSE(iCurrentProperty, PROPERTY_CLUBHOUSE_1_BASE_A)
		atCurrentApartment = BIKER_VAR_A
		CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "Biker Apartment type A")
	ELIF IS_PROPERTY_CLUBHOUSE(iCurrentProperty, PROPERTY_CLUBHOUSE_7_BASE_B)
		atCurrentApartment = BIKER_VAR_B
		CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "Biker Apartment type B")
	ELIF IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
		sCurrentValues.iApartmentInstance = GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iInstance + 32
		
		atCurrentApartment = APT_TYPE_ARCADE
		CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "Arcade Property")
	#IF FEATURE_DLC_2_2022
	ELIF IS_PLAYER_IN_JUGGALO_HIDEOUT_PROPERTY(PLAYER_ID())
		sCurrentValues.iApartmentInstance = GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iInstance + 32
		
		atCurrentApartment = APT_TYPE_JUGGALO_HIDEOUT
		CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "Juggalo Hideout Property")
	#ENDIF
	ENDIF
	
	SETUP_MINIGAME_TYPES(arrayMinigameInfo, atCurrentApartment)
	MINIGAME_LAUNCH_INITIALIZE(sMinigameLaunchTimer, arrayMinigameInfo)
		
	//default darts values
	g_FMMC_STRUCT.iNumberOfLegs = 1//iArrayDartNumOfLegs[iCurrentLegs].iValue
	g_FMMC_STRUCT.iNumberOfSets = iArrayDartNumOfSets[iCurrentSets].iValue
	
	//##### Start Check to see if a minigame is already started.
	INT iMGID
	FOR iMGID = 0 TO CONST_MINIGAME_COUNT -1
		IF NOT IS_BIT_SET(sLocalData.iMGDisabledBitSet, iMGID)
			MINIGAME_LAUNCH_REQUEST(sMinigameLaunchTimer, iMGID, 3.0)
		ENDIF
	ENDFOR
	
	// Main loop
	WHILE TRUE

		// One wait to rule them all. This can be the ONLY wait from here on in.....
		WAIT(0)
		
		//MP_LOOP_WAIT_ZERO()
		// If we have a match end event, bail
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			PRINTLN("[MPTUT] - MISSION END - SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE - SCRIPT CLEANUP B ")
			CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE - SCRIPT TERMINATED")
			SCRIPT_CLEANUP()
		ENDIF
		
		//### Check if user is outside the apartment
		IF NOT IS_PLAYER_ENTERING_PROPERTY(GET_PLAYER_INDEX())
			//check if script sohuld terminate - if outside an apartment
			IF NOT IS_PLAYER_IN_MP_PROPERTY(GET_PLAYER_INDEX(), FALSE)
			AND NOT IS_PLAYER_IN_CLUBHOUSE(GET_PLAYER_INDEX())
			AND NOT IS_PLAYER_IN_CUTSCENE(GET_PLAYER_INDEX())
			AND NOT IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
			#IF FEATURE_DLC_2_2022
			AND NOT IS_PLAYER_IN_JUGGALO_HIDEOUT_PROPERTY(PLAYER_ID())
			#ENDIF
			AND playerBD[PARTICIPANT_ID_TO_INT()].eStage < eAD_CLEANUP
				IF SWITCH_LAUNCHER_STAGE(playerBD[PARTICIPANT_ID_TO_INT()], eAD_CLEANUP)
					IF GET_GAME_TIMER() % 3000 > 50
					CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "NOT INSIDE AN APARTMENT TERMINATED TERMINATED TERMINATED")
					ENDIF
				ENDIF
			ENDIF
		ENDIF


		// -----------------------------------
		// Process your game logic.....
		SWITCH playerBD[PARTICIPANT_ID_TO_INT()].iGameState
			
			// Wait untill the server gives the all go before moving on
			CASE GAME_STATE_INI
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING					
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_RUNNING
					PRINTLN("[MPTUT] - playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_RUNNING")
				ELIF GET_SERVER_MISSION_STATE() = GAME_STATE_END
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY
					PRINTLN("[MPTUT] - playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY 1")
				ENDIF
			BREAK
			
			// Main gameplay state.
			CASE GAME_STATE_RUNNING
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING
					PROCESS_CLIENT(sCurrentValues, sStateNow, sMinigameLaunchTimer)
				ELIF GET_SERVER_MISSION_STATE() = GAME_STATE_END
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY
					PRINTLN("[MPTUT] - playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY 2")
				ENDIF
			BREAK
			
			CASE GAME_STATE_TERMINATE_DELAY 
		      	IF IS_MISSION_READY_TO_CLEANUP(serverBD.MissionTerminateDelayTimer)
		            playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END
					PRINTLN("[MPTUT] - playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END 3")
		      	ENDIF
			BREAK
			
			// The game stage the local player is placed when we want him to leave a mission
			CASE GAME_STATE_LEAVE
				playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END
				PRINTLN("[MPTUT] - playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END 2")
			FALLTHRU
			
			//Cleans up then terminates the mission
			CASE GAME_STATE_END
				PRINTLN("[MPTUT] - SCRIPT CLEANUP A ")
				SCRIPT_CLEANUP()
			BREAK

		ENDSWITCH
		
		// -----------------------------------
		// Process server game logic		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			
			SWITCH GET_SERVER_MISSION_STATE()
				
				// For CnC group missions, we may want to wait for all players to have launched the script
				CASE GAME_STATE_INI
					serverBD.iServerGameState = GAME_STATE_RUNNING
					PRINTLN("[MPTUT] - serverBD.iServerGameState = GAME_STATE_RUNNING")
				BREAK
				
				// Look for game end conditions
				CASE GAME_STATE_RUNNING
					
					PROCESS_SERVER()
					
					IF HAVE_MISSION_END_CONDITIONS_BEEN_MET()
						serverBD.iServerGameState = GAME_STATE_END
						PRINTLN("[MPTUT] - serverBD.iServerGameState = GAME_STATE_END 4")
					ENDIF
					
				BREAK
				
				CASE GAME_STATE_END
				
				BREAK
				
			ENDSWITCH
			
		ENDIF
		
//		#IF IS_DEBUG_BUILD
//		MAINTAIN_DEBUG()
//		#ENDIF
	ENDWHILE

ENDSCRIPT
