USING "Apartment_Minigame_General.sch"
USING "rc_helper_functions.sch"
//CONSTS
	CONST_FLOAT		CONST_MG_SCRIPT_ACTIVE_WAIT 30.0
	CONST_FLOAT		CONST_APARTMENT_CHECK_TIME 25.0
	CONST_INT		CONST_A_SIDE 0
	CONST_INT		CONST_B_SIDE 1
	CONST_INT 		CONST_MINIGAME_COUNT 2
	CONST_FLOAT 	CONST_MINIGAME_RETRY_TIME 10.0
	CONST_FLOAT 	CONST_MG_DELAY_SETUP_VIEW 1.0
	CONST_FLOAT		CONST_MG_DELAY_SETUP_ALLOWED 1.0

  	CONST_INT		MAX_NUM_DARTS_LEGS 3
  	CONST_INT		MAX_NUM_DARTS_SETS 5
	CONST_INT		NUM_MENU_ITEMS_DARTS 1
	//min disatnce to activate minigame locator player check
	CONST_INT		MIN_DISTANCE_TO_MINIGAME 2
	
	CONST_FLOAT		MG_AVALIBLE_SINGLE_DISAPLY_TIME 9.0
	CONST_FLOAT		MG_AVALIBLE_DISAPLY_TIME 4.0
	CONST_FLOAT 	MINIGAME_BETWEEN_MINIGAME_TIME 2.0 
	CONST_FLOAT 	MINIGAME_AVALIBLE_HIDE_TIME 9.0 
	
	CONST_FLOAT		MG_TIMER_FAILSAFE_INTERACT_CHECK 3.0
ENUM ENUM_MG_AVAILIBLE_DISPLAY
	MGA_SELECT_MINIGAME,
	MGA_DISPLAY_MINIGAME,
	MGA_DELAY_NEXT_MINIGAME,
	MGA_DELAY_AVAILABILITY,
	MGA_HIDE_AVAILABILITY
ENDENUM

STRUCT STRUCT_UPDATE_MINIGAME
	BOOL bHideText = FALSE
	BOOL hasSetupMenu = FALSE
	BOOL bInsideCoords = FALSE	
	BOOL bAcceptableDistance = FALSE
	//ARM
	BOOL bSelfInsideSpecific = FALSE
	BOOL bAnotherInsideMain = FALSE
	BOOL bAnotherInsideSpecific = FALSE
	BOOL bCorrectSide = FALSE
	
	INT iInsideLocator = 0
ENDSTRUCT

STRUCT STRUCT_CLIENT_ALL_EVENT_VALUES
	//BOOL	bSetAsSetupPlayer 		= FALSE
	//BOOL	bReceivedSetupView 		= FALSE
	BOOL	bAllowInteract			= FALSE
	BOOL	bAllowInteractReceived 	= FALSE
	INT		iEventMGID				= -1
	STRING	sEventMGScriptName
ENDSTRUCT

ENUM ENUM_MINIGAME_INTERACTION_STAGE
	MIS_IDLE,
	MIS_INTERACT_CHECK,
	MIS_NAVIGATE,
	MIS_NAVIGATE_END,
	MIS_START_INTERACTION,
	MIS_WAIT_INTERACTION_END,
	MIS_WAIT_PLAYER_INPUT,
	MIS_START_MINIGAME, 
	MIS_CANCEL_READY
ENDENUM

STRUCT STRUCT_LOCAL_CLIENT_DATA
	STRUCT_CLIENT_ALL_EVENT_VALUES 	sClientEvents
	ENUM_MINIGAME_INTERACTION_STAGE eCurInteractStage = MIS_IDLE
	INT								iMGDisabledBitSet
	INT								iApartmentVisibilityID = -1
ENDSTRUCT

ENUM LAUNCHER_STAGE_ENUM
	eAD_IDLE,
	eAD_LOAD_MINIGAME,
	eAD_START_MINIGAME,
	eAD_WAIT_ACTIVE_MINIGAME,
	eAD_WAIT_END_MINIGAME,
	eAD_PRE_CLEANUP,
	eAD_CLEANUP
ENDENUM

ENUM LOCAL_PLAYER_FLAGS
	LPF_DISABLE_IDLE_CAM,
	LPF_DISABLE_CELLPHONE,
	LPF_DISABLE_PAUSE_MENU,
	LPF_FREEZE_ENTITY,
	LPF_FREEZE_ENTITY_COMPARE,
	LPF_DISABLE_MOVEMENT,
	LPF_DISABLE_MOVEMENT_COMPARE,
	LPF_HIDE_HUD
ENDENUM

// The server broadcast data.
// Everyone can read this data, only the server can update it
STRUCT ServerBroadcastData
	INT iMGAvailible
	INT iMGCanHaveMorePlayers
	INT iMGHasEnoughPlayers
	//INT iMGHasSetupPlayer
	INT iMGRunning
	// Game State
	INT iServerGameState
	// Minigame Setup Users, stores the setup players so client can querry
	INT iMGDartsSetupPlayer
	INT iMGArmWSetupPlayer
	
	//Mission State
	LAUNCHER_STAGE_ENUM eStage = eAD_IDLE
	
	//Mission End timer
	SCRIPT_TIMER MissionTerminateDelayTimer
ENDSTRUCT

// The client broadcast data.
// Everyone can read this data, only the local player only the server can update it
STRUCT PlayerBroadcastData
	//curent minigame player count.
	INT iMinigamePlayerCount = 0
	//-1 no activity
	INT	iCurrentMinigame = -1
	// Player Gamestate
	INT iGameState
	// Client BitMask
	INT	iBitMaskActivities
	// Enums
	LAUNCHER_STAGE_ENUM eStage = eAD_IDLE
ENDSTRUCT

FUNC BOOL UPDATE_MGID(PlayerBroadcastData &PBD, INT newMGID)
	
	IF  PBD.iCurrentMinigame <> newMGID
		PBD.iCurrentMinigame = newMGID
		CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "PLAYER MGID UPDATED: ", PBD.iCurrentMinigame)	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC STRING GET_MGL_EVENT_STRING(SCRIPTED_EVENT_TYPES sEvent)
	//Init to number for unknown event.
	STRING sEventName = "UNKNOWN MGL EVENT"
	SWITCH sEvent
		CASE SCRIPT_EVENT_MG_LAUNCH_REQUEST 
			sEventName = "SCRIPT_EVENT_MG_LAUNCH_REQUEST"
		BREAK
		
		CASE SCRIPT_EVENT_MG_LAUNCH_USER
			sEventName = "SCRIPT_EVENT_MG_LAUNCH_USER"
		BREAK
		
		CASE SCRIPT_EVENT_MG_LEADER
			sEventName = "SCRIPT_EVENT_MG_LEADER"
		BREAK
		
		CASE SCRIPT_EVENT_MG_FINISH
			sEventName = "SCRIPT_EVENT_MG_FINISH"
		BREAK
		
		CASE SCRIPT_EVENT_MG_INTERACT_CHANGED
			sEventName = "SCRIPT_EVENT_MG_INTERACT_CHANGED"
		BREAK
		
		CASE SCRIPT_EVENT_MG_ALLOW_PARTICIPATE
			sEventName = "SCRIPT_EVENT_MG_ALLOW_PARTICIPATE"
		BREAK
		
		CASE SCRIPT_EVENT_MG_FORCE_STOP_INTERACT
			sEventName = "SCRIPT_EVENT_MG_FORCE_STOP_INTERACT"
		BREAK
	ENDSWITCH
	
	RETURN sEventName
ENDFUNC

FUNC BOOL IS_INSIDE_CLUBHOUSE()
	
	IF NOT IS_PLAYER_ENTERING_PROPERTY(GET_PLAYER_INDEX())
		//check if script sohuld terminate - if outside an apartment
		IF NOT IS_PLAYER_IN_MP_PROPERTY(GET_PLAYER_INDEX(), FALSE)
		AND NOT IS_PLAYER_IN_CLUBHOUSE(GET_PLAYER_INDEX())
		AND NOT IS_PLAYER_IN_CUTSCENE(GET_PLAYER_INDEX())
		AND NOT IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
		#IF FEATURE_DLC_2_2022
		AND NOT IS_PLAYER_IN_JUGGALO_HIDEOUT_PROPERTY(PLAYER_ID())
		#ENDIF
			RETURN FALSE
			CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "NOT INSIDE AN APARTMENT TERMINATED")
		ENDIF
	ENDIF
		
	RETURN TRUE
ENDFUNC

FUNC STRING ENUM_MINIGAME_INTERACTION_STAGE_STRING(ENUM_MINIGAME_INTERACTION_STAGE value)
	SWITCH value
		CASE MIS_IDLE RETURN "MIS_IDLE"
		CASE MIS_INTERACT_CHECK RETURN "MIS_INTERACT_CHECK"
		CASE MIS_NAVIGATE RETURN "MIS_NAVIGATE"
		CASE MIS_NAVIGATE_END RETURN "MIS_NAVIGATE_END"
		CASE MIS_START_INTERACTION RETURN "MIS_START_INTERACTION"
		CASE MIS_WAIT_INTERACTION_END RETURN "MIS_WAIT_INTERACTION_END"
		CASE MIS_WAIT_PLAYER_INPUT RETURN "MIS_WAIT_PLAYER_INPUT"
		CASE MIS_START_MINIGAME RETURN "MIS_START_MINIGAME"
		CASE MIS_CANCEL_READY RETURN "MIS_CANCEL_READY"
	ENDSWITCH
	RETURN "INVALID"
ENDFUNC

PROC SET_INTERACTION_STAGE(STRUCT_LOCAL_CLIENT_DATA &sLocalData, ENUM_MINIGAME_INTERACTION_STAGE newValue)
	CDEBUG1LN(DEBUG_MG_LAUNCHER_APART,"")
	CDEBUG1LN(DEBUG_MG_LAUNCHER_APART,"	  IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII")
	CDEBUG1LN(DEBUG_MG_LAUNCHER_APART,"	  MINIGAME LAUNCHER - INTERACTION STAGE CHANGED")
	CDEBUG1LN(DEBUG_MG_LAUNCHER_APART,"	  MINIGAME LAUNCHER - FROM: ", ENUM_MINIGAME_INTERACTION_STAGE_STRING(sLocalData.eCurInteractStage), " TO >>> ", ENUM_MINIGAME_INTERACTION_STAGE_STRING(newValue))
	sLocalData.eCurInteractStage = newValue
	CDEBUG1LN(DEBUG_MG_LAUNCHER_APART,"	  VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV")
ENDPROC

ENUM ENUM_APARTMENT_TYPE
	BIKER_VAR_A,
	BIKER_VAR_B
	, APT_TYPE_ARCADE
	#IF FEATURE_DLC_2_2022
	, APT_TYPE_JUGGALO_HIDEOUT
	#ENDIF
ENDENUM

ENUM MGL_CLIENTFLAG
	MGL_CF_MG_SETUP_PLAYER,	
	MGL_CF_MG_VALID_PLAYER,
	MGL_CF_MG_START_REQUEST,
	MGL_CF_RECEIVED_SETUP,
	MGL_CF_INITIAL_MG_CHECK,
	MGL_CF_MG_A_SIDE_PLAYER,
	MGL_CG_MG_SIDE_SELECTED,
	MGL_CF_MG_RUNNING,
	MGL_CF_MG_START_PRESSED
ENDENUM

STRUCT MINIGAME_POSITION_INFO
	VECTOR 	vPosition
	FLOAT	fHeading
	VECTOR	vPointOfInterest
	VECTOR	vLocatorSize
	STRING  sAnimLib
	STRING	sIntoAnim
	STRING	sOutroAnim
ENDSTRUCT

STRUCT MINIGAME_INTERACT_INFO
	MINIGAME_POSITION_INFO MPIavailible[4]
	VECTOR vMainLocation
	VECTOR	vMainLocatorSize
	FLOAT fHeading
	INT	iNumAvailible
ENDSTRUCT

//##### BIT_SET
FUNC BOOL GET_CLIENT_FLAG(PlayerBroadcastData &lPlayerBD, MGL_CLIENTFLAG eFlag)
	RETURN IS_BIT_SET(lPlayerBD.iBitMaskActivities, ENUM_TO_INT(eFlag))	
ENDFUNC

FUNC BOOL SET_CLIENT_FLAG(PlayerBroadcastData &lPlayerBD, MGL_CLIENTFLAG eNewFlag, BOOL bTrue = TRUE)
	BOOL bIsSet = GET_CLIENT_FLAG(lPlayerBD, eNewFlag)	
	
	IF bTrue
		IF NOT bIsSet
			SET_BIT(lPlayerBD.iBitMaskActivities, ENUM_TO_INT(eNewFlag))
			RETURN TRUE
		ENDIF
	ELSE
		IF bIsSet
			CLEAR_BIT(lPlayerBD.iBitMaskActivities, ENUM_TO_INT(eNewFlag))
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC GET_APARTMENT_INTERACT_INFO(MINIGAME_INTERACT_INFO &info, ENUM_APARTMENT_TYPE eApartType, ENUM_CLUBHOUSE_MGID eMinigameType)
	SWITCH eApartType
		CASE BIKER_VAR_A 
			SWITCH eMinigameType
			
				CASE eMGID_DARTS
					info.iNumAvailible = 1
					info.vMainLocation = <<1119.0640, -3156.9946, -36.56>>
					info.vMainLocatorSize = << 0.8, 0.8, 0.8 >>
					info.fHeading = 270.0
					
					info.MPIavailible[0].vPosition = <<1117.34, -3156.68, -37.0669 >>
					info.MPIavailible[0].vPointOfInterest = <<1119.8805,-3157.0261,-36.3397>>
					info.MPIavailible[0].vLocatorSize = <<1.0, 1.0, 1.0>>
					
					info.MPIavailible[0].sAnimLib = ""
					info.MPIavailible[0].sIntoAnim = "darts_ig_idle_guy1"
					info.MPIavailible[0].sOutroAnim = ""
					

					info.MPIavailible[1].vPosition = << 1117.34, -3157.38, -38.0669 >>
					info.MPIavailible[1].vPointOfInterest = <<1119.8805,-3157.0261,-36.3397>>
					info.MPIavailible[1].vPointOfInterest = <<1.0, 1.0, 1.0>>
					info.MPIavailible[1].sAnimLib = ""
					info.MPIavailible[1].sIntoAnim = "darts_ig_idle_guy1"
					info.MPIavailible[1].sOutroAnim = ""
				BREAK
				
				CASE eMGID_ARMWRESTLE
					info.iNumAvailible = 2
					info.vMainLocation = << 1116.5, -3153.27, -36.56 >>
					info.vMainLocatorSize = << 1.2, 1.2, 1.2 >>
					
					info.MPIavailible[0].vPosition = <<1115.92, -3153.15, -37.036 >>
					info.MPIavailible[0].vPointOfInterest =  <<1116.51, -3153.28, -37.569 >>
					info.MPIavailible[0].vLocatorSize = <<0.5, 0.5, 1>>
					info.MPIavailible[0].sAnimLib = ""
					info.MPIavailible[0].sIntoAnim = "base"
					info.MPIavailible[0].sOutroAnim = ""
					
					info.MPIavailible[1].vPosition = <<1117.11, -3153.4, -37.036>> 
					info.MPIavailible[1].vPointOfInterest =  <<1116.51, -3153.28, -37.569 >>
					info.MPIavailible[1].vLocatorSize = <<0.5, 0.5, 1>>
					info.MPIavailible[1].sAnimLib = ""
					info.MPIavailible[1].sIntoAnim = "base"
					info.MPIavailible[1].sOutroAnim = ""
				BREAK
			ENDSWITCH
		BREAK
		
		CASE BIKER_VAR_B 
			SWITCH eMinigameType
			
				CASE eMGID_DARTS
					info.iNumAvailible = 1
					info.vMainLocation = <<1000.8506, -3163.9783, -34.0646>>
					info.vMainLocatorSize = << 0.8, 0.8, 0.8 >>
					info.fHeading = 0.0
					
					info.MPIavailible[0].vPosition = <<1000.59, -3165.35, -35.0540>> 
					info.MPIavailible[0].vPointOfInterest = <<1000.93, -3162.82, -33.4827>>
					info.MPIavailible[0].vLocatorSize = <<1.0, 1.0, 1.0>>
					info.MPIavailible[0].sAnimLib = ""
					info.MPIavailible[0].sIntoAnim = "darts_ig_idle_guy1"
					info.MPIavailible[0].sOutroAnim = ""
					
					info.MPIavailible[1].vPosition = <<1001.28, -3165.35, -35.0540 >> 
					info.MPIavailible[1].vPointOfInterest = <<1000.93, -3162.82, -33.4827>>
					info.MPIavailible[1].sAnimLib = ""
					info.MPIavailible[1].sIntoAnim = "darts_ig_idle_guy1"
					info.MPIavailible[1].sOutroAnim = ""
				BREAK
				
				CASE eMGID_ARMWRESTLE
					info.iNumAvailible = 2
					info.vMainLocation = <<1003.23, -3165.73, -34.0646>>
					info.vMainLocatorSize = << 1.2, 1.2, 1.2 >>
					
					info.MPIavailible[0].vPosition = <<1003.35, -3165.14, -34.0646>>
					info.MPIavailible[0].vPointOfInterest = <<1003.23, -3165.74, -34.5976>>
					info.MPIavailible[0].vLocatorSize = <<0.5, 0.5, 1>>
					info.MPIavailible[0].sAnimLib = ""
					info.MPIavailible[0].sIntoAnim = "base"
					info.MPIavailible[0].sOutroAnim = ""
					
					info.MPIavailible[1].vPosition = <<1003.11, -3166.33, -34.0646>>
					info.MPIavailible[1].vPointOfInterest = <<1003.23, -3165.74, -34.5976>>
					info.MPIavailible[1].vLocatorSize = <<0.5, 0.5, 1>>
					info.MPIavailible[1].sAnimLib = ""
					info.MPIavailible[1].sIntoAnim = "base"
					info.MPIavailible[1].sOutroAnim = ""
				BREAK
			ENDSWITCH
		BREAK
		
		CASE APT_TYPE_ARCADE
			SWITCH eMinigameType
				CASE eMGID_DARTS
					info.iNumAvailible = 1
					info.vMainLocation = <<2708.4703, -358.8301, -55.0523>>
					info.vMainLocatorSize = <<0.8, 0.8, 0.8>>
					info.fHeading = 0.0
					
					info.MPIavailible[0].vPosition = <<2708.2097, -360.2018, -56.0417>>
					info.MPIavailible[0].vPointOfInterest = <<2708.5497, -357.6718, -54.4704>>
					info.MPIavailible[0].vLocatorSize = <<1.0, 1.0, 1.0>>
					info.MPIavailible[0].sAnimLib = ""
					info.MPIavailible[0].sIntoAnim = "darts_ig_idle_guy1"
					info.MPIavailible[0].sOutroAnim = ""
					
					info.MPIavailible[1].vPosition = <<2708.8997, -360.2018, -56.0417>>
					info.MPIavailible[1].vPointOfInterest = <<2708.5497, -357.6718, -54.4704>>
					info.MPIavailible[1].vLocatorSize = <<1.0, 1.0, 1.0>>
					info.MPIavailible[1].sAnimLib = ""
					info.MPIavailible[1].sIntoAnim = "darts_ig_idle_guy1"
					info.MPIavailible[1].sOutroAnim = ""
				BREAK
			ENDSWITCH
		BREAK
		
		#IF FEATURE_DLC_2_2022
		CASE APT_TYPE_JUGGALO_HIDEOUT
			SWITCH eMinigameType
				CASE eMGID_DARTS
					info.iNumAvailible = 1
					info.vMainLocation = <<556.5399, -420.3187, -69.6347>>
					info.vMainLocatorSize = <<0.75, 0.75, 1.0>>
					info.fHeading = 180.0
					
					info.MPIavailible[0].vPosition = <<556.5399, -420.3187, -69.6347>>
					info.MPIavailible[0].vPointOfInterest = <<556.5532, -420.9970, -67.9379>>
					info.MPIavailible[0].vLocatorSize = <<1.0, 1.0, 1.0>>
					info.MPIavailible[0].sAnimLib = ""
					info.MPIavailible[0].sIntoAnim = "darts_ig_idle_guy1"
					info.MPIavailible[0].sOutroAnim = ""
					
					info.MPIavailible[1].vPosition = <<556.5399, -420.3187, -69.6347>>
					info.MPIavailible[1].vPointOfInterest = <<556.5532, -420.9970, -67.9379>>
					info.MPIavailible[1].vLocatorSize = <<1.0, 1.0, 1.0>>
					info.MPIavailible[1].sAnimLib = ""
					info.MPIavailible[1].sIntoAnim = "darts_ig_idle_guy1"
					info.MPIavailible[1].sOutroAnim = ""
				BREAK
			ENDSWITCH
		BREAK
		#ENDIF
	ENDSWITCH
	
ENDPROC

PROC DEBUG_OUT_MINIGAME_POSITION_INFO(MINIGAME_POSITION_INFO MPIvalue)
	CDEBUG1LN(DEBUG_MG_LAUNCHER_APART,"")
	CDEBUG1LN(DEBUG_MG_LAUNCHER_APART,"%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
	CDEBUG1LN(DEBUG_MG_LAUNCHER_APART,"CURENT MINIGAME POSITION INFO")
	CDEBUG1LN(DEBUG_MG_LAUNCHER_APART,"Locator Position: ", MPIvalue.vPosition)
	CDEBUG1LN(DEBUG_MG_LAUNCHER_APART,"Point Of Interest: ", MPIvalue.vPointOfInterest)
	CDEBUG1LN(DEBUG_MG_LAUNCHER_APART,"Anim Lib: ", MPIvalue.sAnimLib)
	CDEBUG1LN(DEBUG_MG_LAUNCHER_APART,"Intro Anim: ", MPIvalue.sIntoAnim)
	CDEBUG1LN(DEBUG_MG_LAUNCHER_APART,"Outro Anim: ", MPIvalue.sOutroAnim)
	CDEBUG1LN(DEBUG_MG_LAUNCHER_APART,"%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
	CDEBUG1LN(DEBUG_MG_LAUNCHER_APART,"")
ENDPROC

FUNC VECTOR GET_APARTMENT_TYPE_DARTS_LOCATOR(ENUM_APARTMENT_TYPE eApartType)
	SWITCH eApartType
		CASE BIKER_VAR_A 				RETURN <<1118.4894, -3157.1875, -37.5628>>
		CASE BIKER_VAR_B 				RETURN <<1001.0, -3164.3, -33.6>>
		CASE APT_TYPE_ARCADE			RETURN <<2708.6197, -359.1518, -54.5877>>
		#IF FEATURE_DLC_2_2022
		CASE APT_TYPE_JUGGALO_HIDEOUT	RETURN <<556.5399, -420.3187, -69.6347>>
		#ENDIF
	ENDSWITCH
	
	RETURN <<0,0,0>>
ENDFUNC

FUNC VECTOR GET_APARTMENT_TYPE_ARMWRESTLE_LOCATOR(ENUM_APARTMENT_TYPE eApartType)
	SWITCH eApartType
		CASE BIKER_VAR_A RETURN <<1116.5,-3153.2,-37.5>>
		CASE BIKER_VAR_B RETURN <<1003.1, -3165.7, -33.6>>
	ENDSWITCH
		
	RETURN <<0,0,0>>
ENDFUNC

CONST_INT 	CONST_MG_TEXT_NUM 8

ENUM ENUM_MG_TEXT
	MGT_IN_PROGRESS 	= 0,
	MGT_SINGLE_PLAYER 	= 1,
	MGT_MULTI_PLAYER	= 2,
	MGT_MG_AVAILIBLE 	= 3,
	MGT_WAIT_FOR_PLAYER = 4,
	MGT_WAIT_FOR_LEADER = 5,
	MGT_READY_FOR_GAME	= 6,
	MGT_TOO_MANY_P		= 7
	//CONST_MG_TEXT_NUM	= 8
ENDENUM


STRUCT MINIGAME_TYPE_INFO
	INT		iMinigameID
	STRING	sScriptName
	ENUM_CLUBHOUSE_MGID acType
	VECTOR	vLocatorCoords
	VECTOR	vLocatorArea
	BOOL	bHighlightArea
	INT		iMinPlayers
	INT		iMaxPlayers
	
	STRING sMGText[CONST_MG_TEXT_NUM]
  ENDSTRUCT
  
  
PROC SETUP_MINIGAME_TYPES(MINIGAME_TYPE_INFO& minigameInfoArray[], ENUM_APARTMENT_TYPE eApartType)
	MINIGAME_TYPE_INFO minigameInfo
	
	//IF IS_BIT_SET(iAvailibleMinigamesFlag, ENUM_TO_INT(eMGID_DARTS))
		//LOST GANG
		minigameInfo.iMinigameID = ENUM_TO_INT(eMGID_DARTS)
		minigameInfo.sScriptName = "AM_Darts_Apartment"
		minigameInfo.acType = eMGID_DARTS
		minigameInfo.vLocatorCoords = GET_APARTMENT_TYPE_DARTS_LOCATOR(eApartType)
		minigameInfo.vLocatorArea = <<1.0, 1.0, 1.0>>
		minigameInfo.bHighlightArea = FALSE
		minigameInfo.iMinPlayers = 1
		minigameInfo.iMaxPlayers = 2
		
		minigameInfo.sMGText[MGT_IN_PROGRESS] = "DART_A_PROG"
		minigameInfo.sMGText[MGT_SINGLE_PLAYER] = "DART_A_ONE"
		minigameInfo.sMGText[MGT_MULTI_PLAYER] = "DART_A_TWO"
		minigameInfo.sMGText[MGT_MG_AVAILIBLE] = "DART_A_JOIN"
		minigameInfo.sMGText[MGT_WAIT_FOR_PLAYER] = ""
		minigameInfo.sMGText[MGT_WAIT_FOR_LEADER] = "DART_A_LWAIT"
		minigameInfo.sMGText[MGT_READY_FOR_GAME] = "DART_A_PREP"
		minigameInfo.sMGText[MGT_TOO_MANY_P] = "DART_A_MANY"
		
		minigameInfoArray[ENUM_TO_INT(eMGID_DARTS)] = minigameInfo
//	ENDIF
	
//	IF IS_BIT_SET(iAvailibleMinigamesFlag, ENUM_TO_INT(eMGID_ARMWRESTLE)) 
		//LOST GANG
		minigameInfo.iMinigameID = ENUM_TO_INT(eMGID_ARMWRESTLE)
		minigameInfo.sScriptName = "AM_Armwrestling_Apartment"
		minigameInfo.acType = eMGID_ARMWRESTLE
		minigameInfo.vLocatorCoords = GET_APARTMENT_TYPE_ARMWRESTLE_LOCATOR(eApartType)
		minigameInfo.vLocatorArea = <<1.0, 1.0, 1.0>>
		minigameInfo.bHighlightArea = FALSE
		minigameInfo.iMinPlayers = 2
		minigameInfo.iMaxPlayers = 2
		
		minigameInfo.sMGText[MGT_IN_PROGRESS] = "ARMW_A_PROG"
		minigameInfo.sMGText[MGT_SINGLE_PLAYER] = ""
		minigameInfo.sMGText[MGT_MULTI_PLAYER] = "ARMW_A_TWO"
		minigameInfo.sMGText[MGT_MG_AVAILIBLE] = "ARMW_A_JOIN"
		minigameInfo.sMGText[MGT_WAIT_FOR_PLAYER] = "ARMW_A_PWAIT"
		minigameInfo.sMGText[MGT_WAIT_FOR_LEADER] = "ARMW_A_LWAIT"
		minigameInfo.sMGText[MGT_READY_FOR_GAME] = "ARMW_A_PREP"
		minigameInfo.sMGText[MGT_TOO_MANY_P] = "ARMW_A_MANY"
	    minigameInfoArray[ENUM_TO_INT(eMGID_ARMWRESTLE)] = minigameInfo
	//ENDIF
	
  ENDPROC
FUNC STRING GET_STRING_FROM_TEXT_LABEL(STRING text)
	RETURN TEXT
ENDFUNC

  FUNC STRING GET_MG_TEXT(MINIGAME_TYPE_INFO minigameInfo, ENUM_MG_TEXT newText)
  		
		IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
			SWITCH newText

				CASE MGT_WAIT_FOR_LEADER
					IF minigameInfo.iMinigameID = ENUM_TO_INT(eMGID_DARTS)
						RETURN "DART_A_LWAITPC"
					ELSE
						RETURN "ARMW_A_LWAITPC"
					ENDIF
				BREAK

				CASE MGT_MULTI_PLAYER
					IF minigameInfo.iMinigameID = ENUM_TO_INT(eMGID_DARTS)
						RETURN "DART_A_TWO"
					ELSE
						RETURN "ARMW_A_TWOPC"
					ENDIF
				BREAK
				
				CASE MGT_WAIT_FOR_PLAYER
					IF minigameInfo.iMinigameID = ENUM_TO_INT(eMGID_DARTS)
						RETURN ""
					ELSE
						RETURN "ARMW_A_PWAITPC"
					ENDIF
				BREAK

			ENDSWITCH
		ENDIF
		RETURN minigameInfo.sMGText[newText]
  ENDFUNC
    
STRUCT  MENU_ROW_INFO
	STRING 	sLabel
	INT		iNumSubselect
	INT		iCurrentSelect
  ENDSTRUCT

STRUCT INT_MENU_ITEM
	INT iValue
//	STRING sLabel
  ENDSTRUCT

PROC  SETUP_DART_MENU(MENU_ROW_INFO &_mriDartsMenu[])
	//Number of sets
	_mriDartsMenu[0].sLabel = "DART_A_GN"
	_mriDartsMenu[0].iNumSubselect = MAX_NUM_DARTS_SETS
	_mriDartsMenu[0].iCurrentSelect = 0
	
	//Number of sets
//	_mriDartsMenu[1].sLabel = "DART_A_MP_GN"
//	_mriDartsMenu[1].iNumSubselect = MAX_NUM_DARTS_SETS
//	_mriDartsMenu[1].iCurrentSelect = 0
  ENDPROC

  PROC SETUP_MENU_DARTS_INFO(INT_MENU_ITEM &sArrayDartsLegs[], INT_MENU_ITEM &sArrayDartsSets[] )
  		//legs
		sArrayDartsLegs[0].iValue = 0
		sArrayDartsLegs[1].iValue = 1
		sArrayDartsLegs[2].iValue = 2
		//sets
		sArrayDartsSets[0].iValue = 1
		sArrayDartsSets[1].iValue = 2
		sArrayDartsSets[2].iValue = 3
		sArrayDartsSets[3].iValue = 4
		sArrayDartsSets[4].iValue = 5
	ENDPROC
  
  ENUM COUNTER_ENUM
  	eC_UP,
	eC_DOWN
	ENDENUM
	
	PROC MODIFY_COUNTER(INT &Counter, INT CounterMAX, COUNTER_ENUM action, BOOL isArrayCounter = TRUE)
		INT mCounterMAX
		IF isArrayCounter
			mCounterMAX = CounterMAX -1
		ELSE
			mCounterMAX = CounterMAX
		ENDIF
		
		INT CounterMIN = 0
		//counter should not be lower than 0
		IF Counter < CounterMIN
			Counter = CounterMIN
		ENDIF
		
		SWITCH action
			CASE eC_DOWN
				IF Counter < mCounterMAX
					Counter++
				ELIF Counter >= mCounterMAX
					Counter = CounterMIN
				ENDIF
			BREAK
			
			CASE eC_UP
				IF Counter > CounterMIN
					Counter--
				ELIF Counter <= mCounterMAX
					Counter = mCounterMAX
				ENDIF
			BREAK
		  ENDSWITCH
	  ENDPROC
	
	FUNC STRING LABEL_TO_STRING(STRING blockNameLabel)
		RETURN blockNameLabel
	ENDFUNC
	
  //HELPERS
PROC TOGGLE_HELP_TEXT(STRING blockName, BOOL isVisible = TRUE)
	IF NOT IS_STRING_NULL_OR_EMPTY(blockName)
		
		IF isVisible
			IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(blockName)
				CDEBUG2LN(DEBUG_MG_LAUNCHER_APART, "DISPLAY HELP: ", blockName)
				PRINT_HELP_FOREVER(blockName)
			ENDIF
		ELSE
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(blockName)
				CDEBUG2LN(DEBUG_MG_LAUNCHER_APART,"CLEAR HELP: ", blockName)
				CLEAR_HELP()
			ENDIF
		ENDIF
	ENDIF
ENDPROC	

FUNC STRING LAUNCHER_STAGE_ENUM_STRING(LAUNCHER_STAGE_ENUM stage)
	SWITCH(stage)
		CASE eAD_IDLE RETURN "eAD_IDLE"
		CASE eAD_LOAD_MINIGAME RETURN "eAD_LOAD_MINIGAME"
		CASE eAD_START_MINIGAME RETURN "eAD_START_MINIGAME"
		CASE eAD_WAIT_END_MINIGAME RETURN "eAD_WAIT_END_MINIGAME"
		CASE eAD_CLEANUP RETURN "eAD_CLEANUP"
	ENDSWITCH
	RETURN "INVALID"
ENDFUNC

FUNC BOOL SWITCH_LAUNCHER_STAGE(PlayerBroadcastData &playerBD, LAUNCHER_STAGE_ENUM newValue)
	IF playerBD.eStage <> newValue
		CDEBUG1LN(DEBUG_MG_LAUNCHER_APART,"")
		CDEBUG1LN(DEBUG_MG_LAUNCHER_APART,"##################################################")
		CDEBUG1LN(DEBUG_MG_LAUNCHER_APART,"MINIGAME LAUNCHER - LAUNCHER STAGE CHANGED")
		CDEBUG1LN(DEBUG_MG_LAUNCHER_APART,"MINIGAME LAUNCHER - FROM: ", LAUNCHER_STAGE_ENUM_STRING(playerBD.eStage), " TO >>> ", LAUNCHER_STAGE_ENUM_STRING(newValue))
		playerBD.eStage = newValue
		CDEBUG1LN(DEBUG_MG_LAUNCHER_APART,"VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV")
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

STRUCT MINIGAME_LAUNCH
	BOOL 		bSpectator
	STRING		sScriptName
	BOOL		bRequestLaunch
	INT			iInstanceID
	structTimer stTryLaunchTimer
	FLOAT		fLaunchTryTime
ENDSTRUCT

PROC MINIGAME_LAUNCH_CHECK_RESET(MINIGAME_LAUNCH &minigame[CONST_MINIGAME_COUNT])
	
	INT mini
	FOR mini = 0 TO CONST_MINIGAME_COUNT - 1
		minigame[mini].bRequestLaunch = FALSE
		minigame[mini].fLaunchTryTime = CONST_MINIGAME_RETRY_TIME
		CANCEL_TIMER(minigame[mini].stTryLaunchTimer)
	ENDFOR
ENDPROC

PROC MINIGAME_LAUNCH_CHECK(MINIGAME_LAUNCH &minigame)
	
	IF NOT IS_TIMER_STARTED(minigame.stTryLaunchTimer)
	AND minigame.bRequestLaunch
		ASSERTLN(" Minigame Launch Requested, But Timer Not Stared")
	ENDIF
		
	IF IS_TIMER_STARTED(minigame.stTryLaunchTimer)
		IF GET_TIMER_IN_SECONDS(minigame.stTryLaunchTimer) < minigame.fLaunchTryTime
		AND minigame.bRequestLaunch
		AND NOT NETWORK_IS_SCRIPT_ACTIVE(minigame.sScriptName, minigame.iInstanceID, TRUE)

			MP_MISSION_DATA fmmcMissionData
			fmmcMissionData.iInstanceId = minigame.iInstanceID
			SET_BIT(fmmcMissionData.iBitSet, ENUM_TO_INT(EMGPT_JOINED_AS_SPECTATOR))
			
			IF IS_INSIDE_CLUBHOUSE()
				IF NETWORK_IS_SCRIPT_ACTIVE(minigame.sScriptName, minigame.iInstanceID, FALSE)
				AND NOT NETWORK_IS_SCRIPT_ACTIVE(minigame.sScriptName, minigame.iInstanceID, TRUE)
					IF NET_LOAD_AND_LAUNCH_SCRIPT_FM(minigame.sScriptName, SCRIPT_XML_STACK_SIZE, fmmcMissionData,
						FALSE,//screenfade
						FALSE,//is mission
						TRUE,//clear wanted level
						TRUE)//is ambient event
						
						minigame.bRequestLaunch = FALSE
						CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "LAUNCHED SCRIPT: ", minigame.sScriptName, "INSTANCE ID: ",  fmmcMissionData.iInstanceId)
					ELSE
						CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "SCRIPT LAUNCH - COULD NOT START SCRIPT: ", minigame.sScriptName, "INSTANCE ID: ",  fmmcMissionData.iInstanceId)
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
						IF GET_FRAME_COUNT() % 60 = 0
							CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "SCRIPT LAUNCH - NEWTOWRK SCRIPT IS NOT ACTIVE: ", minigame.sScriptName, "INSTANCE ID: ",  fmmcMissionData.iInstanceId)
						ENDIF
					#ENDIF
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
					IF GET_FRAME_COUNT() % 60 = 0
						CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "SCRIPT LAUNCH - NOT INSIDE CLUBHOUSE, DISABLE SCRIPT LAUNCH")
					ENDIF
				#ENDIF
			ENDIF
		ELSE
			CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, 
			"Finished Trying to launch: ", minigame.sScriptName, " instanceID:", minigame.iInstanceID,
			" Script Launched?: ", NETWORK_IS_SCRIPT_ACTIVE(minigame.sScriptName, minigame.iInstanceID, TRUE))
			CANCEL_TIMER(minigame.stTryLaunchTimer)
			minigame.bRequestLaunch = FALSE
		ENDIF
	ENDIF
ENDPROC

PROC MINIGAME_LAUNCH_INITIALIZE(MINIGAME_LAUNCH &minigame[CONST_MINIGAME_COUNT], MINIGAME_TYPE_INFO& minigameInfoArray[CONST_MINIGAME_COUNT])
	INT iMinigame
	FOR iMinigame = 0 TO CONST_MINIGAME_COUNT -1
		minigame[iMinigame].sScriptName = minigameInfoArray[iMinigame].sScriptName
		minigame[iMinigame].fLaunchTryTime = CONST_MINIGAME_RETRY_TIME
		minigame[iMinigame].iInstanceID = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iVisibilityID
		
		#IF FEATURE_CASINO_HEIST
		IF IS_PLAYER_IN_ANY_SIMPLE_INTERIOR(PLAYER_ID())
			minigame[iMinigame].iInstanceID = GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iInstance + 32
		ENDIF
		#ENDIF
	ENDFOR
ENDPROC

PROC MINIGAME_LAUNCH_REQUEST(MINIGAME_LAUNCH &minigame[CONST_MINIGAME_COUNT], INT iMGID, FLOAT fTimeToTry = CONST_MINIGAME_RETRY_TIME)
	
	CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "MINIGAME LAUNCH - REQUESTED MGID: ", iMGID, " Try For Time:", fTimeToTry)
	
	minigame[iMGID].fLaunchTryTime = fTimeToTry
	minigame[iMGID].bRequestLaunch = TRUE
	IF NOT IS_TIMER_STARTED(minigame[iMGID].stTryLaunchTimer)
		START_TIMER_NOW(minigame[iMGID].stTryLaunchTimer)
	ELSE
		RESTART_TIMER_NOW(minigame[iMGID].stTryLaunchTimer)
	ENDIF
	
ENDPROC

FUNC BOOL START_CURRENT_MINIGAME(STRING sScriptName, INT iStackSize, INT iInstanceID, BOOL bStartAsSpectator = FALSE, BOOL bSetupPlayer = FALSE, BOOL bIsPlayerSideA = FALSE)
	
	MP_MISSION_DATA fmmcMissionData
	
	IF bStartAsSpectator
		CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "START_CURRENT_MINIGAME - This player is a spectator for script: ", sScriptName)
		SET_BIT(fmmcMissionData.iBitSet, ENUM_TO_INT(eMGPT_JOINED_AS_SPECTATOR))
	ELSE
		CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "START_CURRENT_MINIGAME - This player is NOT! spectator for script: ", sScriptName)
	ENDIF
	
	IF bSetupPlayer 
		CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "START_CURRENT_MINIGAME - This player is a setup player: ", sScriptName)
		SET_BIT(fmmcMissionData.iBitSet, ENUM_TO_INT(eMGPT_JOINED_AS_SETUP))
	ELSE
		CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "START_CURRENT_MINIGAME - This player is NOT!  a setup palyer: ", sScriptName)
	ENDIF
	
	IF bIsPlayerSideA 
		CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "START_CURRENT_MINIGAME - This player is going to side A: ", sScriptName)
		SET_BIT(fmmcMissionData.iBitSet, ENUM_TO_INT(eMGPT_CLOSEST_TO_A_SIDE))
	ELSE
		CLEAR_BIT(fmmcMissionData.iBitSet, ENUM_TO_INT(eMGPT_CLOSEST_TO_A_SIDE))
		CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "START_CURRENT_MINIGAME - This player is NOT going to side A: ", sScriptName)
	ENDIF
	
	fmmcMissionData.iInstanceId = iInstanceID
	CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "START_CURRENT_MINIGAME - Script Instance ID:", iInstanceID)

	IF NOT NETWORK_IS_SCRIPT_ACTIVE(sScriptName, fmmcMissionData.iInstanceId, TRUE)
		CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "START_CURRENT_MINIGAME - Script is not active")	
		CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "START_CURRENT_MINIGAME - LOADING MINIGAME SCRIPT PAREMETERS : ", sScriptName, " StackSize", iStackSize)
		
		//Set as currently not on mission type 
		GlobalplayerBD_FM[NATIVE_TO_INT(GET_PLAYER_INDEX())].iCurrentMissionType = FMMC_TYPE_INVALID //???
		
		IF NET_LOAD_AND_LAUNCH_SCRIPT_FM(sScriptName, iStackSize, fmmcMissionData,
		FALSE,//screenfade
		FALSE,//is mission
		TRUE,//clear wanted level
		TRUE)//is ambient event
			CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "START_CURRENT_MINIGAME - LAUNCHNG SCRIPT: ", sScriptName, "INSTANCE ID: ",  fmmcMissionData.iInstanceId)
			RETURN TRUE
		ENDIF
	ELSE
		CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "START_CURRENT_MINIGAME - Script is already active:", sScriptName)
	ENDIF
	
	RETURN FALSE
ENDFUNC

CONST_FLOAT AFTER_INTERACTION_DELAY 1.0

FUNC  BOOL ALLOW_INTERACTION_UPDATE(structTimer &delayTimer)
	IF IS_TIMER_STARTED(delayTimer)
		IF GET_TIMER_IN_SECONDS(delayTimer) >= AFTER_INTERACTION_DELAY
			PAUSE_TIMER(delayTimer)
			RETURN TRUE
		ELSE
			RETURN FALSE
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
ENDFUNC
  
// ENUM LAUNCH_EVENT_TYPE_ENUM
// 	LAUNCH_INVALID,
// 	LAUNCH_SCRIPT
// ENDENUM

//STRUCT BROADCAST_LAUNCH_STRUCT
//	STRUCT_EVENT_COMMON_DETAILS Details
//	PLAYER_INDEX piPlayer
//	//LAUNCH_EVENT_TYPE_ENUM eEventType
//	BOOL bIsSpectator
//	INT	 iScriptID
//ENDSTRUCT

//STRUCT CLIENT_EVENTS
//	SCRIPTED_EVENT_TYPES	eEvent
//	BOOL					bReceivedSetupView	
//	BOOL					bSetAsSetupPlayer
//	BOOL					bIsSpectator
//ENDSTRUCT

//PROC BROADCAST_MINIGAME_LAUNCH(PLAYER_INDEX iPlayerIndex, SCRIPTED_EVENT_TYPES EventType, INT iMGID, BOOL bSpectator = FALSE)
//	
//	BROADCAST_LAUNCH_STRUCT sLaunchInfo
//	sLaunchInfo.Details.Type = EventType
//	sLaunchInfo.iScriptID = iMGID
//	sLaunchInfo.piPlayer = iPlayerIndex
//	sLaunchInfo.bIsSpectator = bSpectator
//								
//	CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "Send Event to start Minigame.", NATIVE_TO_INT(iPlayerIndex))	
//	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, sLaunchInfo, SIZE_OF(BROADCAST_LAUNCH_STRUCT), SPECIFIC_PLAYER(iPlayerIndex))
//ENDPROC

//FUNC BOOL PROCESS_MINIGAME_LAUNCH(INT eventID, BROADCAST_LAUNCH_STRUCT &sLaunchInfo)
//	BOOL bAllowLaunch
//	bAllowLaunch = FALSE
//	BROADCAST_LAUNCH_STRUCT sEventInfo
//	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, eventID, sEventInfo, SIZE_OF(BROADCAST_LAUNCH_STRUCT))
//		SWITCH sEventInfo.Details.Type
//			CASE SCRIPT_EVENT_MG_LAUNCH_USER
//				sLaunchInfo = sEventInfo
//				bAllowLaunch = TRUE
//			BREAK
//		ENDSWITCh
//	ENDIF
//	
//	RETURN bAllowLaunch
//ENDFUNC 
  
//  ENUM USER_EVENTS
//  	INVALID_EVENT,
//  	SET_AS_SETUP,
//	FINISHED_MINIGAME,
//	REQUEST_START
//ENDENUM

ENUM ENUM_EVENT_FLAGS
	EF_ALLOW_INTERACT	= 0,
	EF_INTERACT_ACTIVE	= 1, 
	EF_SETUP_PLAYER		= 2, 
	EF_SERVER_EVENT		= 3,
	EF_SPECTATOR		= 4,
	EF_CLOSEST_TO_A_POS = 5
ENDENUM

PROC EVENT_FLAG_TOGGLE(INT &FlagVar, ENUM_EVENT_FLAGS eFlag, BOOL bFlagOn = TRUE)
	BOOL bFlagState = IS_BIT_SET(FlagVar, ENUM_TO_INT(eFlag))
	
	IF bFlagOn
		IF NOT bFlagState
			SET_BIT(FlagVar, ENUM_TO_INT(eFlag))
		ENDIF
	ELSE
		IF bFlagState
			CLEAR_BIT(FlagVar, ENUM_TO_INT(eFlag))
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL EVENT_FLAG_GET(INT FlagVar, ENUM_EVENT_FLAGS eFlag)
	RETURN IS_BIT_SET(FlagVar, ENUM_TO_INT(eFlag))
ENDFUNC

STRUCT STRUCT_USER_EVENT
	STRUCT_EVENT_COMMON_DETAILS Details
	INT				iEventFlags
	INT 			iMinigameID
	PLAYER_INDEX 	piRequestPlayerID
	INT				iEventID
ENDSTRUCT

PROC EVENT_SERVER_MG_LAUNCH(INT iEventID, PLAYER_INDEX piPlayer, INT iMGID, BOOL bIsSpectator = FALSE, BOOL bIsSetupPlayer = TRUE, BOOL bIsClosestToAPos = FALSE)
	
	STRUCT_USER_EVENT sEvent
	sEvent.Details.Type = SCRIPT_EVENT_MG_LAUNCH_USER
	sEvent.Details.FromPlayerIndex = PLAYER_ID()
	EVENT_FLAG_TOGGLE(sEvent.iEventFlags, EF_SPECTATOR, bIsSpectator)
	EVENT_FLAG_TOGGLE(sEvent.iEventFlags, EF_SETUP_PLAYER, bIsSetupPlayer)
	EVENT_FLAG_TOGGLE(sEvent.iEventFlags, EF_CLOSEST_TO_A_POS, bIsClosestToAPos)
	sEvent.piRequestPlayerID = piPlayer
	sEvent.iMinigameID = iMGID
	sEvent.iEventID = iEventID						
	CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "SERVER - Send Event to start Minigame.", GET_PLAYER_NAME(piPlayer),
	" Is Spectator?: ", bIsSpectator, " Is Setup?: ", bIsSetupPlayer, " Is Closest to A?:", bIsClosestToAPos, " EVENTID: ", iEventID)	
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, sEvent, SIZE_OF(STRUCT_USER_EVENT), SPECIFIC_PLAYER(piPlayer))
ENDPROC

PROC EVENT_SERVER_ALLOW_PARTICIPATE(PLAYER_INDEX piPlayer, INT iMGID, BOOL bAllowInteract = TRUE)
	STRUCT_USER_EVENT sEvent
	sEvent.Details.Type = SCRIPT_EVENT_MG_ALLOW_PARTICIPATE
	sEvent.Details.FromPlayerIndex = PLAYER_ID()
	EVENT_FLAG_TOGGLE(sEvent.iEventFlags, EF_ALLOW_INTERACT, bAllowInteract)
	EVENT_FLAG_TOGGLE(sEvent.iEventFlags, EF_SERVER_EVENT, FALSE)
	sEvent.piRequestPlayerID = piPlayer
	sEvent.iMinigameID = iMGID
	
	CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "SERVER - Interact Changed SEND BACK: ", 
	GET_PLAYER_NAME(sEvent.piRequestPlayerID), " Allow palyer to Interact?: ", bAllowInteract)
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, sEvent, SIZE_OF(STRUCT_USER_EVENT), SPECIFIC_PLAYER(piPlayer))
ENDPROC

PROC EVENT_CLIENT_REQUEST_LAUNCH(PLAYER_INDEX piByPlayer, INT iMGID)
	STRUCT_USER_EVENT sEvent
	sEvent.Details.Type = SCRIPT_EVENT_MG_LAUNCH_REQUEST
	sEvent.Details.FromPlayerIndex = PLAYER_ID()
	sEvent.iMinigameID = iMGID
	sEvent.piRequestPlayerID = piByPlayer
	EVENT_FLAG_TOGGLE(sEvent.iEventFlags, EF_SERVER_EVENT, TRUE)
	
	CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "CLIENT - SEND REQUEST TO START THE MINIGAME: ", GET_PLAYER_NAME(sEvent.piRequestPlayerID), " MGID?: ", sEvent.iMinigameID)
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, sEvent, SIZE_OF(STRUCT_USER_EVENT), SCRIPT_HOST())
ENDPROC
//PROC EVENT_SERVER_SELECT_SETUP(PLAYER_INDEX piPlayer, INT iMGID, BOOL bSetupPlayer = TRUE)
//	STRUCT_USER_EVENT sEvent
//	sEvent.Details.Type = SCRIPT_EVENT_MG_LEADER
//	EVENT_FLAG_TOGGLE(sEvent.iEventFlags, EF_SETUP_PLAYER, bSetupPlayer)
//	EVENT_FLAG_TOGGLE(sEvent.iEventFlags, EF_SERVER_EVENT, FALSE)
//	sEvent.piRequestPlayerID = piPlayer
//	sEvent.iMinigameID = iMGID
//	
//	CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "SERVER - SETUP PLAYER SELECTED: ", GET_PLAYER_NAME(sEvent.piRequestPlayerID), " IS SETUP?:", bSetupPlayer)
//	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, sEvent, SIZE_OF(STRUCT_USER_EVENT), SPECIFIC_PLAYER(piPlayer))
//ENDPROC

PROC EVENT_CLIENT_INTERACT_CHANGED(PLAYER_INDEX piByPlayer, INT iMGID, BOOL bInteractActive)
	STRUCT_USER_EVENT sEvent
	sEvent.Details.Type = SCRIPT_EVENT_MG_INTERACT_CHANGED
	sEvent.Details.FromPlayerIndex = PLAYER_ID()
	sEvent.iMinigameID = iMGID
	sEvent.piRequestPlayerID = piByPlayer
	EVENT_FLAG_TOGGLE(sEvent.iEventFlags, EF_INTERACT_ACTIVE, bInteractActive)
	EVENT_FLAG_TOGGLE(sEvent.iEventFlags, EF_SERVER_EVENT, TRUE)

	CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "CLIENT - Interact Changed SEND: ", 
	GET_PLAYER_NAME(sEvent.piRequestPlayerID), " Interact State?: ", bInteractActive)
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, sEvent, SIZE_OF(STRUCT_USER_EVENT), SCRIPT_HOST())
ENDPROC

//PROC EVENT_CLIENT_GAME_END(PLAYER_INDEX piByPlayer, INT iMGID)
//	STRUCT_USER_EVENT sEvent
//	sEvent.Details.Type = SCRIPT_EVENT_MG_FINISH
//	sEvent.iMinigameID = iMGID
//	sEvent.piRequestPlayerID = piByPlayer
//	EVENT_FLAG_TOGGLE(sEvent.iEventFlags, EF_SERVER_EVENT, TRUE)
//	
//	CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "CLIENT- SEND GAME END TO SEREVER BY: ", GET_PLAYER_NAME(sEvent.piRequestPlayerID), " MGID?: ", sEvent.iMinigameID)
//	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, sEvent, SIZE_OF(STRUCT_USER_EVENT), SCRIPT_HOST())
//ENDPROC

PROC EVENT_USER(SCRIPTED_EVENT_TYPES eEvent, PLAYER_INDEX piPlayer, INT iMGID)
	STRUCT_USER_EVENT sEvent
	sEvent.Details.Type = eEvent
	sEvent.Details.FromPlayerIndex = PLAYER_ID()
	sEvent.piRequestPlayerID = piPlayer
	sEvent.iMinigameID = iMGID
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, sEvent, SIZE_OF(STRUCT_USER_EVENT), SPECIFIC_PLAYER(piPlayer))
ENDPROC

//PROC EVENT_ALL(SCRIPTED_EVENT_TYPES eEvent, PLAYER_INDEX piByPlayer, INT iMGID)
//	STRUCT_USER_EVENT sEvent
//	sEvent.Details.Type = eEvent
//	sEvent.iMinigameID = iMGID
//	sEvent.piRequestPlayerID = piByPlayer
//	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, sEvent, SIZE_OF(STRUCT_USER_EVENT), ALL_PLAYERS())
//ENDPROC

FUNC BOOL RECEIVE_EVENT(INT iEventID, STRUCT_USER_EVENT &sEvent, BOOL bAsHost = FALSE)
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, sEvent, SIZE_OF(STRUCT_USER_EVENT))
		IF EVENT_FLAG_GET(sEvent.iEventFlags, EF_SERVER_EVENT) = bAsHost
			RETURN TRUE
		ELSE
		
			RETURN FALSE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

STRUCT MAD_STRUCT
	ENUM_MG_AVAILIBLE_DISPLAY eState
	structTimer stStageDelay
	INT			iCurrentMinigame
	INT 		iMGDisplayedCount
	BOOL		bHelpDisplayed
ENDSTRUCT

PROC MAG_SWITCH_STATE(ENUM_MG_AVAILIBLE_DISPLAY &stateVar, ENUM_MG_AVAILIBLE_DISPLAY newState)
	stateVar = newState
ENDPROC


STRUCT AML_GENERAL_STRUCT
	BOOL	bIsMGNetActiveUpdated
	BOOL	bCanHaveMorePlayers[CONST_MINIGAME_COUNT]
	BOOL	bIsMinigameActiveNet[CONST_MINIGAME_COUNT]
	BOOL	bIsMinigameActiveLocal[CONST_MINIGAME_COUNT]
	INT		iMGCurrent
	BOOL	bPlayerWalking
	BOOL	bAllowIntractUpdate
	INT		iApartmentInstance
	FLOAT	fDisplayTime
ENDSTRUCT

//Checks bit state before setting it to new
FUNC BOOL SAFE_BIT_TOGGLE(INT &iBitSet, INT iBitFlag, BOOL bToggleOn = TRUE)
	BOOL bIsSet = IS_BIT_SET(iBitSet, iBitFlag)
	
	IF bToggleOn
		IF NOT bIsSet
			SET_BIT(iBitSet, iBitFlag)
			RETURN TRUE
		ENDIF
	ELSE
		IF bIsSet
			CLEAR_BIT(iBitSet, iBitFlag)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Toggles a bit in a bitmask
/// PARAMS:
///    iBitSet - 
///    iBitFlag - 
///    bToggleOn - 
/// RETURNS: true if state changed - does not represent the new state
///    
FUNC BOOL SAFE_BIT_TOGGLE_BIT(INT &iBitSet, ENUM_TO_INT iBitFlag, BOOL bToggleOn = TRUE)
	BOOL bIsSet = IS_BIT_SET_ENUM(iBitSet, iBitFlag)
	
	IF bToggleOn
		IF NOT bIsSet
			SET_BIT_ENUM(iBitSet, iBitFlag)
			RETURN TRUE
		ENDIF
	ELSE
		IF bIsSet
			CLEAR_BIT_ENUM(iBitSet, iBitFlag)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC PRINT_WITH_NAME(STRING pTextLabel, STRING PlayerName, INT Duration, INT Colour)
	Colour = Colour
	BEGIN_TEXT_COMMAND_PRINT(pTextLabel)
		ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(PlayerName)
	END_TEXT_COMMAND_PRINT(Duration, TRUE)
ENDPROC

FUNC INT GET_MG_PLAYER_COUNT(PlayerBroadcastData &playerBD[NUM_NETWORK_PLAYERS], INT iMGID)
	INT iPlayerNum = 0
	INT iParticipantID = 0
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipantID 
	PARTICIPANT_INDEX ParticipantID = INT_TO_PARTICIPANTINDEX(iParticipantID)
		IF NETWORK_IS_PARTICIPANT_ACTIVE(ParticipantID)
			PLAYER_INDEX piCurrent = NETWORK_GET_PLAYER_INDEX(ParticipantID)
			IF NETWORK_IS_PLAYER_ACTIVE(piCurrent)	
				IF iMGID >= 0
				AND iMGID <= CONST_MINIGAME_COUNT -1
					IF iMGID = playerBD[iParticipantID].iCurrentMinigame
						IF 	GET_CLIENT_FLAG(playerBD[iParticipantID], MGL_CF_MG_VALID_PLAYER)
							iPlayerNum++
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN iPlayerNum
ENDFUNC

PROC UPDATE_HUD(INT &iBitSet)

	IF IS_BIT_SET(iBitSet, ENUM_TO_INT(LPF_DISABLE_PAUSE_MENU))
		DISABLE_FRONTEND_THIS_FRAME()
	ENDIF
	
	IF IS_BIT_SET_ENUM(iBitSet, LPF_DISABLE_IDLE_CAM)
		INVALIDATE_IDLE_CAM()
	ENDIF
	
	BOOL bShouldFreeze = IS_BIT_SET(iBitSet, ENUM_TO_INT(LPF_FREEZE_ENTITY))
	IF bShouldFreeze <> IS_BIT_SET(iBitSet, ENUM_TO_INT(LPF_FREEZE_ENTITY_COMPARE))
		IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
			SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_UseKinematicModeWhenStationary, bShouldFreeze)
			SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DontActivateRagdollFromExplosions, bShouldFreeze)
		ENDIF
		SAFE_BIT_TOGGLE(iBitSet, ENUM_TO_INT(LPF_FREEZE_ENTITY_COMPARE), bShouldFreeze)
	ENDIF
	
	IF IS_BIT_SET(iBitSet, ENUM_TO_INT(LPF_DISABLE_CELLPHONE))
		DISABLE_CELLPHONE_THIS_FRAME_ONLY()
	ENDIF
	
	//Per Frame 
	IF IS_BIT_SET(iBitSet, ENUM_TO_INT(LPF_DISABLE_MOVEMENT))
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_LR)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_UD)
	ELSE
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_LR)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_UD)
	ENDIF
	
	IF IS_BIT_SET(iBitSet, ENUM_TO_INT(LPF_HIDE_HUD))
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_MP_MESSAGE)
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_FEED)
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_HELP_TEXT)
	ENDIF
ENDPROC

PROC RESET_HUD(INT &iBitSet)
	CLEAR_BIT(iBitSet, ENUM_TO_INT(LPF_DISABLE_PAUSE_MENU))
	
	CLEAR_BIT(iBitSet, ENUM_TO_INT(LPF_FREEZE_ENTITY))
	
	CLEAR_BIT(iBitSet, ENUM_TO_INT(LPF_DISABLE_CELLPHONE))
	
	CLEAR_BIT(iBitSet, ENUM_TO_INT(LPF_DISABLE_MOVEMENT))
	
	CLEAR_BIT(iBitSet, ENUM_TO_INT(LPF_HIDE_HUD))
	
	CLEAR_BITMASK_AS_ENUM(iBitSet, LPF_DISABLE_IDLE_CAM)
	
	UPDATE_HUD(iBitSet)
ENDPROC

FUNC PLAYER_INDEX GET_CLOSEST_TO_MG_POS_A(PlayerBroadcastData &playerBData[NUM_NETWORK_PLAYERS], MINIGAME_INTERACT_INFO sMinigameInteractInfo, INT iMGID)
	
	
	INT 	iClosestPlayerID = -1
	FLOAT	fCurrentDistance = 0
	
	PLAYER_INDEX 	piParticipant
	PED_INDEX		pedInLoop
	INT iParticipantID
	PARTICIPANT_INDEX pariCurrent
	
	CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "A SIDE LOCATION:", sMinigameInteractInfo.MPIavailible[0].vPosition)
	
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipantID
		pariCurrent = INT_TO_PARTICIPANTINDEX(iParticipantID)
		
		IF NETWORK_IS_PARTICIPANT_ACTIVE(pariCurrent)
			piParticipant = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipantID))
			
			
			CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, 
			"ACTIVE:", GET_PLAYER_NAME(piParticipant),
			" MGID", playerBData[iParticipantID].iCurrentMinigame, 
			" VALID?", GET_CLIENT_FLAG(playerBData[iParticipantID], MGL_CF_MG_VALID_PLAYER),
			" ID?", iParticipantID)
			
			
			IF playerBData[iParticipantID].iCurrentMinigame = iMGID
			AND GET_CLIENT_FLAG(playerBData[iParticipantID], MGL_CF_MG_VALID_PLAYER)
				pedInLoop = GET_PLAYER_PED(piParticipant)
				IF IS_ENTITY_ALIVE(pedInLoop)
					#IF IS_DEBUG_BUILD
					VECTOR vCoords = GET_ENTITY_COORDS(pedInLoop)
					CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "VALID PLAYER:", GET_PLAYER_NAME(piParticipant), " LOCATION:", vCoords)
					#ENDIF
						IF IS_ENTITY_ALIVE(pedInLoop)
						
						
							IF iClosestPlayerID = -1
								iClosestPlayerID = iParticipantID
								fCurrentDistance = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(pedInLoop), sMinigameInteractInfo.MPIavailible[0].vPosition)
								CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "DISATNCE1:", fCurrentDistance)
							ELSE
								FLOAT fCheckDistance
								fCheckDistance = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(pedInLoop), sMinigameInteractInfo.MPIavailible[0].vPosition)
								CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "DISATNCE2:", fCurrentDistance)
								IF fCheckDistance < fCurrentDistance
									fCurrentDistance = fCheckDistance
									iClosestPlayerID = iParticipantID
									CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "UPDATED TO NEW CLOSEST:", GET_PLAYER_NAME(piParticipant), " DIST", fCurrentDistance)
								ENDIF
							ENDIF
						ENDIF
				ENDIF
					
			ENDIF
		ENDIF
	ENDREPEAT
	IF iClosestPlayerID >= 0
		piParticipant = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iClosestPlayerID))
		CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "CLOSEST PLAYER NAME FOR MG:", iMGID, " PLAYER NAME:", GET_PLAYER_NAME(piParticipant))
	ELSE
		CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "NOT FOUND!!!:")
	ENDIF
	
	RETURN piParticipant
ENDFUNC

FUNC VECTOR GET_MG_POSITION(MINIGAME_INTERACT_INFO sMinigameInteractInfo,BOOL bASide)
	IF bASide
		RETURN sMinigameInteractInfo.MPIavailible[0].vPosition
	ELSE
		RETURN sMinigameInteractInfo.MPIavailible[1].vPosition
	ENDIF
ENDFUNC

PROC TOGGLE_MG_AVAILABILITY(INT &iMGDisabledBitSet, INT iMGID, BOOL bDisabled)
	IF IS_BIT_SET(iMGDisabledBitSet, iMGID) <> bDisabled 
		SAFE_BIT_TOGGLE(iMGDisabledBitSet, iMGID, bDisabled)
		CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "MGID:", iMGID, " availability updated, Disabled?: ", IS_BIT_SET(iMGDisabledBitSet, iMGID))
	ENDIF
ENDPROC

PROC UPDATE_MG_AVAILABILITY(INT &iMGDisabledBitSet)
	//Darts
	TOGGLE_MG_AVAILABILITY(iMGDisabledBitSet, ENUM_TO_INT(eMGID_DARTS), g_sMPTunables.bBIKER_CLUBHOUSE_DISABLE_DARTS)

	//Arm Wrestling
	TOGGLE_MG_AVAILABILITY(iMGDisabledBitSet, ENUM_TO_INT(eMGID_ARMWRESTLE), g_sMPTunables.bBIKER_CLUBHOUSE_DISABLE_ARM_WRESTLING)  
	
	IF NETWORK_IS_ACTIVITY_SESSION()
		CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "BUG: 3339608 - Disabling Darts and Armwrestling")
		//Darts
		TOGGLE_MG_AVAILABILITY(iMGDisabledBitSet, ENUM_TO_INT(eMGID_DARTS), TRUE)

		//Arm Wrestling
		TOGGLE_MG_AVAILABILITY(iMGDisabledBitSet, ENUM_TO_INT(eMGID_ARMWRESTLE), TRUE) 
	ENDIF
ENDPROC

PROC PRINT_AVAILIBLE_MINIGAMES(INT &iMGDisabledBitSet)
		CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "")
		CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "######### AVAILIBLE MINIGAMES #########")
		CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "Darts Disabled?:", IS_BIT_SET(iMGDisabledBitSet, ENUM_TO_INT(eMGID_DARTS)))
		CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "Arm Wrestling Disabled?:", IS_BIT_SET(iMGDisabledBitSet, ENUM_TO_INT(eMGID_ARMWRESTLE)))
		CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "")
ENDPROC

PROC PROCESS_EVENTS(PlayerBroadcastData &playerBD[NUM_NETWORK_PLAYERS], STRUCT_LOCAL_CLIENT_DATA &sLocalData, MINIGAME_LAUNCH &minigame[CONST_MINIGAME_COUNT], MINIGAME_TYPE_INFO &arrayMinigameInfo[CONST_MINIGAME_COUNT])
	
	//MG EVENTS, received from the minigame scripts.
	STRUCT_SCRIPT_EVENT_MG_APARTMENTS sEventMG
	
	STRUCT_USER_EVENT sEventClient
	INT iCount = 0
	
	REPEAT GET_NUMBER_OF_EVENTS(SCRIPT_EVENT_QUEUE_NETWORK) iCount
		SWITCH GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_NETWORK, iCount)		
			CASE EVENT_NETWORK_SCRIPT_EVENT
				IF RECEIVE_EVENT(iCount, sEventClient)
				AND playerBD[PARTICIPANT_ID_TO_INT()].eStage = eAD_IDLE
					SWITCH(sEventClient.Details.Type)						
						CASE SCRIPT_EVENT_MG_ALLOW_PARTICIPATE
							IF sEventClient.iMinigameID = playerBD[PARTICIPANT_ID_TO_INT()].iCurrentMinigame
							AND sEventClient.piRequestPlayerID = GET_PLAYER_INDEX()
								sLocalData.sClientEvents.bAllowInteractReceived = TRUE
								sLocalData.sClientEvents.iEventMGID = sEventClient.iMinigameID
								sLocalData.sClientEvents.sEventMGScriptName = arrayMinigameInfo[sLocalData.sClientEvents.iEventMGID].sScriptName
								sLocalData.sClientEvents.bAllowInteract = EVENT_FLAG_GET(sEventClient.iEventFlags, EF_ALLOW_INTERACT)
								CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "RECEIVED ALLOW INTERACT: ", GET_PLAYER_NAME(sEventClient.piRequestPlayerID), " Allow?: ",  EVENT_FLAG_GET(sEventClient.iEventFlags, EF_ALLOW_INTERACT)) 
							ENDIF
						BREAK
						
						CASE SCRIPT_EVENT_MG_FORCE_STOP_INTERACT
							IF sEventClient.iMinigameID = playerBD[PARTICIPANT_ID_TO_INT()].iCurrentMinigame
							AND sEventClient.piRequestPlayerID = GET_PLAYER_INDEX()
								sLocalData.sClientEvents.iEventMGID = sEventClient.iMinigameID
								sLocalData.sClientEvents.sEventMGScriptName = arrayMinigameInfo[sLocalData.sClientEvents.iEventMGID].sScriptName
								SET_INTERACTION_STAGE(sLocalData, MIS_CANCEL_READY)
								CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "RECEIVED FORCE STOP INTERACT") 
							ENDIF
						BREAK
						
						CASE SCRIPT_EVENT_MG_LAUNCH_USER
							CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "CLIENT RECEIVED -  MG LAUNCH: EVENTID:", sEventClient.iEventID)
							IF sEventClient.piRequestPlayerID = GET_PLAYER_INDEX()						
								IF sEventClient.iMinigameID >= 0
								AND sEventClient.iMinigameID <= CONST_MINIGAME_COUNT - 1
								
									IF NOT IS_BIT_SET(sLocalData.iMGDisabledBitSet, sEventClient.iMinigameID)
										IF NOT EVENT_FLAG_GET(sEventClient.iEventFlags, EF_SPECTATOR)
											sLocalData.sClientEvents.iEventMGID = sEventClient.iMinigameID
											sLocalData.sClientEvents.sEventMGScriptName = arrayMinigameInfo[sLocalData.sClientEvents.iEventMGID].sScriptName
											SET_CLIENT_FLAG(playerBD[PARTICIPANT_ID_TO_INT()], MGL_CF_MG_START_REQUEST)
											//Correct setup player
											IF SET_CLIENT_FLAG(playerBD[PARTICIPANT_ID_TO_INT()], MGL_CF_MG_SETUP_PLAYER, EVENT_FLAG_GET(sEventClient.iEventFlags, EF_SETUP_PLAYER))
											AND sEventClient.iMinigameID = playerBD[PARTICIPANT_ID_TO_INT()].iCurrentMinigame
												CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "CLIENT - SETUP PLAYER WAS CORRECTED!")
											ENDIF
											CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "IS CLOSEST?: ", EVENT_FLAG_GET(sEventClient.iEventFlags, EF_CLOSEST_TO_A_POS))
											SET_CLIENT_FLAG(playerBD[PARTICIPANT_ID_TO_INT()], MGL_CF_MG_A_SIDE_PLAYER, EVENT_FLAG_GET(sEventClient.iEventFlags, EF_CLOSEST_TO_A_POS))
											
											SWITCH_LAUNCHER_STAGE(playerBD[PARTICIPANT_ID_TO_INT()], eAD_LOAD_MINIGAME)
										ENDIF
									ELSE
										CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "CLIENT - MINIGAME IS DISABLED: ", sEventClient.iMinigameID)
									ENDIF
								ENDIF
							CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, 
							"CLIENT - RECEIVED START MG: ", sEventClient.iMinigameID, 
							" As spec?: ", EVENT_FLAG_GET(sEventClient.iEventFlags, EF_SPECTATOR),
							" Is Setup?: ", EVENT_FLAG_GET(sEventClient.iEventFlags, EF_SETUP_PLAYER),
							" Is A Side?: ", EVENT_FLAG_GET(sEventClient.iEventFlags, EF_CLOSEST_TO_A_POS), " clientside", GET_CLIENT_FLAG(playerBD[PARTICIPANT_ID_TO_INT()], MGL_CF_MG_A_SIDE_PLAYER)) 
							ELSE
								CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "CLIENT RECEIVED -  MG LAUNCH PLAYER DOES NOT MATCH TO LOCAL")
							ENDIF
						BREAK
					ENDSWITCH
				ENDIF
				
				IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, sEventMG, SIZE_OF(STRUCT_SCRIPT_EVENT_MG_APARTMENTS))
					SWITCH sEventMG.Details.Type
						CASE SCRIPT_EVENT_MG_SCRIPT_ACTIVE
							CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "CLIENT RECEIVED -  MG LAUNCH REQUEST FROM MG SCRIPTS")
							IF sEventMG.iInstanceID = sLocalData.iApartmentVisibilityID
							AND sEventMG.piSender <> PLAYER_ID()
								IF NOT (GET_CLIENT_FLAG(playerBD[PARTICIPANT_ID_TO_INT()], MGL_CF_MG_START_REQUEST) AND playerBD[PARTICIPANT_ID_TO_INT()].iCurrentMinigame = ENUM_TO_INT(sEventMG.eMGID))
									IF IS_INSIDE_CLUBHOUSE()
										MINIGAME_LAUNCH_REQUEST(minigame, ENUM_TO_INT(sEventMG.eMGID))
										CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "CLIENT RECEIVED -  Minigame Spec Start Request Received: MGID:", sEventMG.eMGID, " INSTANCE ID:", sEventMG.iInstanceID)
									ELSE
										CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "CLIENT RECEIVED - NOT INSIDE OF A CLUBHOUSE")
									ENDIF
								ELSE
									CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "CLIENT RECEIVED - Already trying to start a minigame like this: ", sEventMG.eMGID)
								ENDIF
							ELSE
								CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "CLIENT RECEIVED -  Instance ids do not match Received:", sEventMG.iInstanceID, " Owner:", sLocalData.iApartmentVisibilityID, " OR self? Sender?", NATIVE_TO_INT(sEventMG.piSender), " and Self?", NATIVE_TO_INT(GET_PLAYER_INDEX()))
							ENDIF
						BREAK
					ENDSWITCh
				ENDIF								
			BREAK
		ENDSWITCH
	ENDREPEAT
ENDPROC

ENUM ENUM_PLATFORM_CONTROL
	ePC_CANCEL
ENDENUM

FUNC CONTROL_ACTION GET_PLATFORM_INPUT(ENUM_PLATFORM_CONTROL ePCControl)
	SWITCH ePCControl
		CASE ePC_CANCEL
			IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
				RETURN INPUT_SCRIPT_RRIGHT
			ELSE
				RETURN INPUT_FRONTEND_CANCEL
			ENDIF
		BREAK
	ENDSWITCH

	RETURN 	INPUT_FRONTEND_CANCEL		
ENDFUNC

FUNC BOOL IS_PLAYER_SETUP_ON_SERVER(ENUM_CLUBHOUSE_MGID eMGID, ServerBroadcastData &serverBD)

	SWITCH eMGID
		CASE eMGID_DARTS
			IF IS_BIT_SET(serverBD.iMGDartsSetupPlayer, PARTICIPANT_ID_TO_INT())
				RETURN TRUE
				CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "IS_PLAYER_SETUP_ON_SERVER - Player: ", GET_PLAYER_NAME(GET_PLAYER_INDEX()), " Is Setup for - ",  GET_CLUBHOUSE_MGID_STRING(eMGID))
			ENDIF
		BREAk
		
		CASE eMGID_ARMWRESTLE
			IF IS_BIT_SET(serverBD.iMGArmWSetupPlayer, PARTICIPANT_ID_TO_INT())
				RETURN TRUE
				CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "IS_PLAYER_SETUP_ON_SERVER - Player: ", GET_PLAYER_NAME(GET_PLAYER_INDEX()), " Is Setup for - ",  GET_CLUBHOUSE_MGID_STRING(eMGID))
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SET_PLAYER_AS_SETUP_ON_SERVER(ENUM_CLUBHOUSE_MGID eMGID, ServerBroadcastData &serverBD, INT iParticipantID)
	
	SWITCH eMGID
		CASE eMGID_DARTS
				CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "SET_PLAYER_ DARTS")
			IF SAFE_BIT_TOGGLE(serverBD.iMGDartsSetupPlayer, iParticipantID)
				
				CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "SET_PLAYER_AS_SETUP_ON_SERVER - Player: ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipantID))), 
				" Is Setup for - ",  GET_CLUBHOUSE_MGID_STRING(eMGID),
				 " Part ID: ", iParticipantID)
				 
				 RETURN TRUE
			ENDIF
			
		BREAk
		
		CASE eMGID_ARMWRESTLE
			CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "SET_PLAYER_ ARM")
			IF SAFE_BIT_TOGGLE(serverBD.iMGArmWSetupPlayer, iParticipantID)
				
				CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "SET_PLAYER_AS_SETUP_ON_SERVER - Player: ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipantID))), 
				" Is Setup for - ",  GET_CLUBHOUSE_MGID_STRING(eMGID),
				 " Part ID: ", iParticipantID)
				 
				 RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC CLEAR_MINIGAME_UPDATE(STRUCT_UPDATE_MINIGAME &sMinigameUpdate)
	sMinigameUpdate.bHideText = 			FALSE
	sMinigameUpdate.hasSetupMenu = 			FALSE
	sMinigameUpdate.bInsideCoords = 		FALSE
	sMinigameUpdate.bAcceptableDistance = 	FALSE
	sMinigameUpdate.bAnotherInsideMain =	FALSE
	sMinigameUpdate.bAnotherInsideSpecific =FALSE
	sMinigameUpdate.bCorrectSide = 			FALSE
	sMinigameUpdate.bSelfInsideSpecific = 	FALSE
	sMinigameUpdate.iInsideLocator = 0
ENDPROC



