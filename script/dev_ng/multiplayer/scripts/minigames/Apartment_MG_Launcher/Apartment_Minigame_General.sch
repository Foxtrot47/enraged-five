////////////////////////////////////////////////////////////////////
//                                                               //
// SCRIPT NAME     :    apartment_minigame_general.sch          //
// AUTHOR          :    Aleksej Leskin						   //
// DESCRIPTION     :    Apartment minigames     			  //
//                                                           //
//////////////////////////////////////////////////////////////

/// PURPOSE: For initialization of the minigame scripts, passed as player setup info.
ENUM ENUM_MG_PLAYER_TYPE
	eMGPT_JOINED_AS_SPECTATOR 	= 1,
	eMGPT_JOINED_AS_SETUP		= 2,
	eMGPT_CLOSEST_TO_A_SIDE		= 3
ENDENUM

ENUM ENUM_CLUBHOUSE_MGID
	eMGID_NONE			= -1,
	//MG
	eMGID_DARTS 		= 0,
	eMGID_ARMWRESTLE 	= 1,
	//Activities
	eMGID_DRINKING		= 2,
	eMGID_SMOKING		= 3,
	eMGID_JUKEBOX		= 4
ENDENUM

FUNC STRING GET_CLUBHOUSE_MGID_STRING(ENUM_CLUBHOUSE_MGID MGID)
	SWITCH MGID
		CASE eMGID_NONE
			RETURN "eMGID_NONE"
		BREAk
		
		CASE eMGID_DARTS
			RETURN "eMGID_DARTS"
		BREAK
		
		CASE eMGID_ARMWRESTLE
			RETURN "eMGID_ARMWRESTLE"
		BREAK
		
		CASE eMGID_DRINKING
			RETURN "eMGID_DRINKING"
		BREAK
		
		CASE eMGID_SMOKING
			RETURN "eMGID_SMOKING"
		BREAK
		
		CASE eMGID_JUKEBOX
			RETURN "eMGID_JUKEBOX"
		BREAK
	ENDSWITCH
	RETURN "INVALID MGID RETURNED!!!"
ENDFUNC

PROC MGID_CLUBHOUSE_SET(ENUM_CLUBHOUSE_MGID newMGID)
	IF GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iClubhouseMGID <> ENUM_TO_INT(newMGID)
		GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iClubhouseMGID = ENUM_TO_INT(newMGID)
		PRINTLN("MGID_CLUBHOUSE_SET: setting local player's MGID = ", ENUM_TO_INT(newMGID))
	ENDIF
ENDPROC

PROC MGID_CLUBHOUSE_RESET()
	IF GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iClubhouseMGID <> ENUM_TO_INT(eMGID_NONE)
		GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iClubhouseMGID = ENUM_TO_INT(eMGID_NONE)
		PRINTLN("MGID_CLUBHOUSE_RESET: setting local player's MGID = ", ENUM_TO_INT(eMGID_NONE))
	ENDIF
ENDPROC

/// PURPOSE:
///    Check which minigame a palyer is currently playing, replicated
/// RETURNS:
///    Refer to ENUM_CLUBHOUSE_MGID
FUNC ENUM_CLUBHOUSE_MGID MGID_CLUBHOUSE_GET()
	RETURN INT_TO_ENUM(ENUM_CLUBHOUSE_MGID, GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iClubhouseMGID)
ENDFUNC

/// For specific player
FUNC ENUM_CLUBHOUSE_MGID MGID_CLUBHOUSE_PLAYER_GET(PLAYER_INDEX piPlayer)
	RETURN INT_TO_ENUM(ENUM_CLUBHOUSE_MGID, GlobalPlayerBD_FM_3[NATIVE_TO_INT(piPlayer)].iClubhouseMGID)
ENDFUNC

/// PURPOSE:
///    Get number of players playing current minigame, value is updated for spectators aswell
/// RETURNS:
/// 0 - no player, 1 , 2    
FUNC INT GET_DARTS_PLAYER_COUNT()
	RETURN g_iClubhouseDartsPlayerCount
ENDFUNC

PROC SET_DARTS_PLAYER_COUNT(INT iNewPlayerCount)
	g_iClubhouseDartsPlayerCount = iNewPlayerCount
	CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "Clubhouse Dart player count updated: ",  g_iClubhouseDartsPlayerCount)
ENDPROC

//### Minigame Launch Events
STRUCT STRUCT_SCRIPT_EVENT_MG_APARTMENTS
	STRUCT_EVENT_COMMON_DETAILS Details
	PLAYER_INDEX piSender
	INT iInstanceID = 			-1
	ENUM_CLUBHOUSE_MGID eMGID = -1
ENDSTRUCT

/// PURPOSE:
///    Broadcast event to all players that the clubhouse minigame started, so spectators could pick this up.
/// PARAMS:
///    iInstanceID - GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iVisibilityID Instance id is the property VisibilityID
/// NOTE:
///    Event.iScore - will represent the instance ID only for this specific event.
PROC BROADCAST_MG_APARTMENTS_LAUNCHED(ENUM_CLUBHOUSE_MGID eMGID, INT iInstanceID)
	STRUCT_SCRIPT_EVENT_MG_APARTMENTS Event
	Event.Details.Type = SCRIPT_EVENT_MG_SCRIPT_ACTIVE
	Event.Details.FromPlayerIndex = PLAYER_ID()
	Event.iInstanceID = iInstanceID
	Event.eMGID = eMGID
	Event.piSender = GET_PLAYER_INDEX()
	
	CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "Broadcasting Darts apartment script started event. MGID:",  GET_CLUBHOUSE_MGID_STRING(eMGID), " InstanceID:", iInstanceID)
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), ALL_PLAYERS())
ENDPROC






