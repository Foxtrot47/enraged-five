
//////////////////////////////////////////////////////////////////////
/* arm_core.sch														*/
/* Author: DJ Jones													*/
/* Setup and cleanup functionality for arm wrestling minigame.		*/
//////////////////////////////////////////////////////////////////////
USING "fmmc_mp_setup_mission.sch"


// Set up the table, wrestlers, etc.
PROC ARM_INIT_GAME(ARM_ARGS& args, ARM_TABLE& table, STREAMED_MODEL& assets[], ARM_UI& armUI, INT iTableIndex, BOOL bApartments = FALSE)
	ARM_SET_ARG_TABLE_INDEX(args, iTableIndex)
	ARM_INIT_TABLE(table, iTableIndex)
	
	// Set up controls, cameras & UI.
	DISPLAY_RADAR(FALSE)
	
	// Stream text, UI, anything needed
	ARM_REQUEST_COUNTDOWN_UI(armUI)
	
	// Streaming requests.
	ADD_STREAMED_MODEL(assets, PROAIR_HOC_PUCK)     // Attachment object
	
	REQUEST_ALL_MODELS(assets)
	REQUEST_ANIM_DICT("mini@arm_wrestling")
	
	TEXT_BLOCK_SLOTS slotToLoad
	IF bApartments
		slotToLoad = PROPERTY_TEXT_SLOT
	ELSE
		slotToLoad = MINIGAME_TEXT_SLOT
	ENDIF
	
	REQUEST_ADDITIONAL_TEXT("ARM_MP", slotToLoad)
	
	armUI.siShardMessage.siMovie	= REQUEST_MG_BIG_MESSAGE()
	
	REGISTER_SCRIPT_WITH_AUDIO()
	CDEBUG1LN(DEBUG_ARM_WRESTLING, "Calling LOAD_STREAM('ARM_WRESTLING_CROWD_MASTER')...")
	LOAD_STREAM("ARM_WRESTLING_CROWD_MASTER")
ENDPROC

// Check for streaming and other init, return true when complete.
FUNC BOOL ARM_IS_STREAMING_COMPLETE(STREAMED_MODEL& assets[], ARM_UI& armUI, BOOL bApartments = FALSE)
	#IF IS_DEBUG_BUILD
		TEXT_LABEL_23 sWaitReason
	#ENDIF
	
	IF NOT HAS_SCALEFORM_MOVIE_LOADED(armUI.siShardMessage.siMovie)
		CDEBUG1LN(DEBUG_ARM_WRESTLING, "armUI.siShardMessage loading")
		RETURN FALSE
	ENDIF
	
	IF NOT REQUEST_SCRIPT_AUDIO_BANK("HUD_AWARDS")	
		CDEBUG1LN(DEBUG_DARTS, "HUD_AWARDS Audio loading")
		RETURN FALSE
	ENDIF
	
	TEXT_BLOCK_SLOTS slotToLoad
	IF bApartments
		slotToLoad = PROPERTY_TEXT_SLOT
	ELSE
		slotToLoad = MINIGAME_TEXT_SLOT
	ENDIF
		
	IF ARE_MODELS_STREAMED(assets)
		IF ARM_IS_COUNTDOWN_UI_LOADED(armUI)
			IF HAS_ANIM_DICT_LOADED("mini@arm_wrestling")
				IF HAS_ADDITIONAL_TEXT_LOADED(slotToLoad)
					//IF LOAD_STREAM("ARM_WRESTLING_CROWD_MASTER")
					IF (GET_CURRENT_GAMEMODE() = GAMEMODE_FM)
							#IF IS_DEBUG_BUILD IF IS_KEYBOARD_KEY_PRESSED(KEY_Z)
								DEBUG_MESSAGE("Streaming complete.")
							ENDIF #ENDIF
							RETURN TRUE
					ELSE
						#IF IS_DEBUG_BUILD IF IS_KEYBOARD_KEY_PRESSED(KEY_Z)
							DEBUG_MESSAGE("Streaming complete.")
						ENDIF #ENDIF
						RETURN TRUE
					//#IF IS_DEBUG_BUILD ELIF IS_KEYBOARD_KEY_PRESSED(KEY_Z)
					//	sWaitReason = "crowd stream" #ENDIF
					//ENDIF
					ENDIF
				#IF IS_DEBUG_BUILD ELIF IS_KEYBOARD_KEY_PRESSED(KEY_Z)
					sWaitReason = "minigame text" #ENDIF
				ENDIF
			#IF IS_DEBUG_BUILD ELIF IS_KEYBOARD_KEY_PRESSED(KEY_Z)
				sWaitReason = "anim dictionary" #ENDIF
			ENDIF
		#IF IS_DEBUG_BUILD ELIF IS_KEYBOARD_KEY_PRESSED(KEY_Z)
			sWaitReason = "countdown UI" #ENDIF
		ENDIF
			
	#IF IS_DEBUG_BUILD ELIF IS_KEYBOARD_KEY_PRESSED(KEY_Z)
		sWaitReason = "models" #ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD IF IS_KEYBOARD_KEY_PRESSED(KEY_Z)
		DEBUG_MESSAGE("Streaming: Waiting for ", sWaitReason, ".")
	ENDIF #ENDIF
	RETURN FALSE
ENDFUNC

// Setup any post-streaming items here.
PROC ARM_SETUP_GAME(ARM_UI& armUI)
	ARM_SETUP_QUIT_UI(armUI, "ARMMP_END_QT", "ARMMP_QUITUI_D")
ENDPROC

// Clean up UI, objects, wrestlers, cams, audio, and network.
PROC ARM_SCRIPT_CLEANUP(ARM_PARTICIPANT_INFO & info, ARM_WRESTLER& wrestlers[], ARM_UI& armUI, PED_INDEX opponentPed, PED_INDEX refPed, OBJECT_INDEX& refAttachment, INT iTable = -1, BOOL bRestoreUI = TRUE, BOOL bTerminate = TRUE, BOOL bWinner = FALSE)
	PLAYER_INDEX broadcastIndex
	
	CDEBUG1LN(DEBUG_ARM_WRESTLING, "Preparing to clean up arm wrestling...")
	
	// Kill widgets.
	#IF IS_DEBUG_BUILD
		ARM_CLEANUP_WIDGETS()
	#ENDIF
	
	SET_TIME_OF_DAY(TIME_OFF, TRUE)
	
	ARM_RELEASE_COUNTDOWN_UI(armUI)
	IF bRestoreUI
		DISABLE_CELLPHONE(FALSE)
//		SET_MP_OVERHEAD_STATS_ACTIVE(TRUE)
//		DISABLE_SCRIPT_HUD(HUDPART_ALL_OVERHEADS, FALSE)
//		ENABLE_RANK_AND_XP_BAR()
//		DISABLE_SCRIPT_HUD(HUDPART_RANKBAR, FALSE)
		ENABLE_ALL_MP_HUD()
		DISPLAY_RADAR(TRUE)
		ENABLE_SELECTOR()
	
		// Broadcast an update to all players.
		broadcastIndex = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(info.iPlayers[eSelfID]))
		CDEBUG1LN(DEBUG_ARM_WRESTLING, "Broadcasting arm wrestle clear to all players on team ", GET_PLAYER_TEAM(broadcastIndex))
		BROADCAST_ARM_WRESTLE_CLEAR(broadcastIndex, GET_PLAYER_TEAM(broadcastIndex))
	ENDIF
	
	// Clear help unless we display the message for leaving the area early.
	IF NOT ARM_GET_UI_FLAG(armUI, ARMUIFLAG_LEAVE_AREA)
		CLEAR_HELP()
		
		// Reset wrestler ped.
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			CDEBUG1LN(DEBUG_ARM_WRESTLING, "Resetting the player ped...")
			DETACH_ENTITY(PLAYER_PED_ID())
			CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
			SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), FALSE)
			FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
		ENDIF
		
		// Restore the wrestler to his normal state.
		IF NOT IS_PLAYER_DEAD(PLAYER_ID())
			SET_PLAYER_INVINCIBLE(PLAYER_ID(), FALSE)
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		ENDIF
	ENDIF
	
	// Reset opponent ped.
	IF DOES_ENTITY_EXIST(opponentPed)
		IF NOT IS_ENTITY_DEAD(opponentPed)
			DETACH_ENTITY(opponentPed)
			CLEAR_PED_TASKS_IMMEDIATELY(opponentPed)
			SET_ENTITY_INVINCIBLE(opponentPed, FALSE)
		ENDIF
	ENDIF
	
	// Clean up referee attachment object.
	IF DOES_ENTITY_EXIST(refAttachment)
		DELETE_OBJECT(refAttachment)
	ELSE
		DEBUG_MESSAGE("Cleanup: Attachment object does not exist!")
	ENDIF
	
	IF DOES_ENTITY_EXIST(refPed)
		IF NOT IS_ENTITY_DEAD(refPed)
			IF IS_ENTITY_ATTACHED(refPed)
			//IF IS_ENTITY_ATTACHED_TO_ENTITY(refPed, refAttachment)
				DEBUG_MESSAGE("Cleanup: Detaching the referee...")
				DETACH_ENTITY(refPed, FALSE, FALSE)
			ELSE
				DEBUG_MESSAGE("Cleanup: Referee is not attached to anything!")
			ENDIF
		ELSE
			DEBUG_MESSAGE("Cleanup: Referee is not alive!")
		ENDIF
	ELSE
		DEBUG_MESSAGE("Cleanup: Referee does not exist!")
	ENDIF
	
	// Clean up wrestler attachment objects. 
	
	DEBUG_MESSAGE("Cleanup: Just before deleting the puck, my participant ID is ", PICK_STRING(eSelfID = ARMWRESTLERID_HOME, "1 (HOST).", "0 (CLIENT)."))
	
	IF DOES_ENTITY_EXIST(wrestlers[ARMWRESTLERID_AWAY].attachmentObject)
		IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(wrestlers[ARMWRESTLERID_AWAY].attachmentNetwork)
			DEBUG_MESSAGE("Cleanup: Attachment object [ARMWRESTLERID_AWAY] deleted!")
			DELETE_OBJECT(wrestlers[ARMWRESTLERID_AWAY].attachmentObject)
		ELSE
			DEBUG_MESSAGE("Cleanup: No control of network ID for Attachment object [ARMWRESTLERID_AWAY]!")
		ENDIF
	ELSE
		DEBUG_MESSAGE("Cleanup: Attachment object [ARMWRESTLERID_AWAY] does not exist!")
	ENDIF
	
	IF DOES_ENTITY_EXIST(wrestlers[ARMWRESTLERID_HOME].attachmentObject)
		IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(wrestlers[ARMWRESTLERID_HOME].attachmentNetwork)
			DEBUG_MESSAGE("Cleanup: Attachment object [ARMWRESTLERID_HOME] deleted!")
			DELETE_OBJECT(wrestlers[ARMWRESTLERID_HOME].attachmentObject)
		ELSE
			DEBUG_MESSAGE("Cleanup: No control of network ID for Attachment object [ARMWRESTLERID_HOME]!")
		ENDIF
	ELSE
		DEBUG_MESSAGE("Cleanup: Attachment object [ARMWRESTLERID_HOME] does not exist!")
	ENDIF
	
	// Restore cams.
	RENDER_SCRIPT_CAMS(FALSE, FALSE)
	
	// Clean audio.
	STOP_STREAM()
	UNREGISTER_SCRIPT_WITH_AUDIO()
	
	//# Conor Changes
	// Used for crowd
	IF iTable != -1
		IF NETWORK_IS_GAME_IN_PROGRESS()
			//NET_PRINT_STRING_INT("Clearing GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iArmWrestlingBitSet: ",iTable)
			CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iArmWrestlingBitSet,iTable)
		ENDIF
	ENDIF
	
	//Clean up freemode variables
	IF GET_CURRENT_GAMEMODE() = GAMEMODE_FM
		IF bWinner
			RESET_FMMC_MISSION_VARIABLES(TRUE, FALSE, ciFMMC_END_OF_MISSION_STATUS_PASSED)	
		ELSE
			RESET_FMMC_MISSION_VARIABLES(TRUE, FALSE, ciFMMC_END_OF_MISSION_STATUS_FAILED)
		ENDIF
	//Clean up CNC
	ELSE
	
	ENDIF
	
	// reset prefered player position broadcast data
	MPGlobals.WrestleData.fBroadcastedDesire = 0.0
	
	// cleanup big splash text
	//CLEANUP_BIG_MESSAGE()
	CLEAR_ALL_BIG_MESSAGES()
	
	IF NETWORK_IS_SIGNED_ONLINE()
		WRITE_ARM_WRESTLING_MP_SCLB_DATA()
		
	ENDIF
	
//	IF IS_SCREEN_FADED_OUT()
//	OR IS_SCREEN_FADING_OUT()
//		DO_SCREEN_FADE_IN(2000)
//	ENDIF
	
	SET_STATIC_EMITTER_ENABLED(GET_ARM_RADIO_EMITTER_NAME(iTable), FALSE)
	
	SET_RESULT_SCREEN_DISPLAYING_STATE(FALSE)
	
	SET_CELEBRATION_SCREEN_AS_ACTIVE(FALSE)
	CLEANUP_CELEBRATION_SCREEN(sCelebrationData)
	
	//gdisablerankupmessage = FALSE
	SET_DISABLE_RANK_UP_MESSAGE(FALSE)
	
	SET_ON_JOB_INTRO(FALSE)
	
	// If we are going to terminate, wait a brief moment and release the player from his tasks.
	IF bTerminate
		IF (NOT ARM_GET_UI_FLAG(armUI, ARMUIFLAG_LEAVE_AREA))
		AND (NOT IS_ENTITY_DEAD(PLAYER_PED_ID()))
			WAIT(200)
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
			ENDIF
		ENDIF
		TERMINATE_THIS_THREAD()
	ENDIF
ENDPROC
