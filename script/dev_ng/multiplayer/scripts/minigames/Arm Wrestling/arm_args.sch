
//////////////////////////////////////////////////////////////////////
/* arm_args.sch														*/
/* Author: DJ Jones													*/
/* Arguments passed in to the arm wrestling minigame.				*/
//////////////////////////////////////////////////////////////////////


// Arguments for multiplayer script.
STRUCT ARM_ARGS
	MODEL_NAMES crowdModels[ARMCROWDMEMBERS]
	INT iTableIndex
ENDSTRUCT


// Accessor for a crowd model.
FUNC MODEL_NAMES ARM_GET_ARG_CROWD_MODEL(ARM_ARGS& args, ARM_CROWD_MEMBER crowdMember)
	IF crowdMember = ARMCROWDMEMBERS
		RETURN DUMMY_MODEL_FOR_SCRIPT
	ENDIF
	
	RETURN args.crowdModels[crowdMember]
ENDFUNC

// Accessor for a crowd model (uses integer).
FUNC MODEL_NAMES ARM_GET_ARG_CROWD_MODEL_INT(ARM_ARGS& args, INT iCrowdMember)
	IF iCrowdMember < 0 OR iCrowdMember >= ENUM_TO_INT(ARMCROWDMEMBERS)
		RETURN DUMMY_MODEL_FOR_SCRIPT
	ENDIF
	
	RETURN args.crowdModels[iCrowdMember]
ENDFUNC

// Mutator for a crowd model.
PROC ARM_SET_ARG_CROWD_MODEL(ARM_ARGS& args, ARM_CROWD_MEMBER crowdMember, MODEL_NAMES crowdModel)
	IF crowdMember = ARMCROWDMEMBERS
		EXIT
	ENDIF
	
	args.crowdModels[crowdMember] = crowdModel
ENDPROC

// Mutator for a crowd model (uses integer).
PROC ARM_SET_ARG_CROWD_MODEL_INT(ARM_ARGS& args, INT iCrowdMember, MODEL_NAMES crowdModel)
	IF iCrowdMember < 0 OR iCrowdMember >= ENUM_TO_INT(ARMCROWDMEMBERS)
		EXIT
	ENDIF
	
	args.crowdModels[iCrowdMember] = crowdModel
ENDPROC

// Accessor for table index.
FUNC INT ARM_GET_ARG_TABLE_INDEX(ARM_ARGS& args)
	RETURN args.iTableIndex
ENDFUNC

// Mutator for game type.
PROC ARM_SET_ARG_TABLE_INDEX(ARM_ARGS& args, INT iTableIndex)
	IF iTableIndex < 1
		SCRIPT_ASSERT("Passing in a zero or less value for table index! Are you sure?")
		EXIT
	ENDIF
	
	args.iTableIndex = iTableIndex
ENDPROC
