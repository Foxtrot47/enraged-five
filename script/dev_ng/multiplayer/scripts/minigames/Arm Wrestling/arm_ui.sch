
//////////////////////////////////////////////////////////////////////
/* arm_ui.sch														*/
/* Author: DJ Jones													*/
/* UI definitions for arm wrestling minigame.						*/
//////////////////////////////////////////////////////////////////////


ENUM ARM_UI_FLAG
	ARMUIFLAG_SHOW_CANT_JOIN,
	ARMUIFLAG_SHOW_WAIT_TO_LAUNCH,
	ARMUIFLAG_SETUP_READY,
	ARMUIFLAG_SHOW_READY,
	ARMUIFLAG_SETUP_WAIT_READY,
	ARMUIFLAG_SHOW_WAIT_READY,
	ARMUIFLAG_SETUP_QUITUI,
	ARMUIFLAG_SHOW_QUITUI,
	ARMUIFLAG_SHOW_TUTORIAL,
	ARMUIFLAG_SHOW_CONTROLS,
	ARMUIFLAG_SETUP_SCORECARD,
	ARMUIFLAG_SHOW_SCORECARD,
	ARMUIFLAG_SHOW_WAIT_REMATCH,
	ARMUIFLAG_SHOW_TERMINATE,
	ARMUIFLAG_LEAVE_AREA,
	ARMUIFLAG_SHOW_LEADERBOARD,
	ARMUIFLAG_SETUP_LEADERBOARD,
	ARMUIFLAG_WHOOSH_PLAYED,
	ARMUIFLAG_SETUP_ENDSCREEN
ENDENUM

ENUM ARM_QUITUI_RETVAL
	ARMQUITUIRETVAL_NONE,
	ARMQUITUIRETVAL_QUIT,
	ARMQUITUIRETVAL_CANCEL
ENDENUM

ENUM ARM_UI_RETVAL
	ARMUIRETVAL_NONE,
	ARMUIRETVAL_TRUE,
	ARMUIRETVAL_FALSE
ENDENUM


STRUCT ARM_UI
	SCRIPT_SCALEFORM_UI uiControls
	COUNTDOWN_UI uiCountdown
	SCALEFORM_INDEX uiQuit
	SCALEFORM_INDEX uiScorecard
	SCALEFORM_INDEX uiLeaderboard
	SC_LEADERBOARD_CONTROL_STRUCT armLB_control
	
	SCRIPT_SCALEFORM_BIG_MESSAGE siBigMessage
	SCRIPT_SHARD_BIG_MESSAGE siShardMessage
	
	ARM_UI_RETVAL returnValue
	
	FLOAT fLastMeter
	INT iScorecardSelection
	INT uiFlags
ENDSTRUCT


// Set a UI flag.
PROC ARM_SET_UI_FLAG(ARM_UI& armUI, ARM_UI_FLAG uiFlag, BOOL bSet)
	IF bSet
		SET_BIT(armUI.uiFlags, ENUM_TO_INT(uiFlag))
	ELSE
		CLEAR_BIT(armUI.uiFlags, ENUM_TO_INT(uiFlag))
	ENDIF
ENDPROC

// Checks to see if a flag is set.
FUNC BOOL ARM_GET_UI_FLAG(ARM_UI& armUI, ARM_UI_FLAG uiFlag)
	RETURN IS_BIT_SET(armUI.uiFlags, ENUM_TO_INT(uiFlag))
ENDFUNC

// Accessor for the UI's minigame controls (used only really for the quit UI, so far).
FUNC SCRIPT_SCALEFORM_UI ARM_GET_UI_CONTROLS(ARM_UI& armUI)
	RETURN armUI.uiControls
ENDFUNC

// Mutator for the UI's minigame controls.
PROC ARM_SET_UI_CONTROLS(ARM_UI& armUI, SCRIPT_SCALEFORM_UI& uiControls)
	armUI.uiControls = uiControls
ENDPROC

// Accessor for the UI's countdown display.
FUNC COUNTDOWN_UI ARM_GET_UI_COUNTDOWN(ARM_UI& armUI)
	RETURN armUI.uiCountdown
ENDFUNC

// Mutator for the UI's countdown display.
PROC ARM_SET_UI_COUNTDOWN(ARM_UI& armUI, COUNTDOWN_UI uiCountdown)
	armUI.uiCountdown = uiCountdown
ENDPROC

// Reset countdown display flags.
PROC ARM_CLEAR_UI_COUNTDOWN_FLAGS(ARM_UI& armUI)
	armUI.uiCountdown.iBitFlags = 0
ENDPROC

// Access for the Quit UI scaleform movie
FUNC SCALEFORM_INDEX ARM_GET_UI_QUIT(ARM_UI& armUI)
	RETURN armUI.uiQuit
ENDFUNC

// Mutator for the UI's Quit UI.
PROC ARM_SET_UI_QUIT(ARM_UI& armUI, SCALEFORM_INDEX& uiQuit)
	armUI.uiQuit = uiQuit
ENDPROC

// General inline funcs & wrappers.
PROC ARM_REQUEST_QUIT_UI(ARM_UI& armUI)
	armUI.uiQuit = REQUEST_QUIT_UI()
ENDPROC

// General inline funcs & wrappers.
PROC ARM_REQUEST_COUNTDOWN_UI(ARM_UI& armUI)
	REQUEST_MINIGAME_COUNTDOWN_UI(armUI.uiCountdown)
ENDPROC

// See if the quit UI has loaded.
FUNC BOOL ARM_IS_QUIT_UI_LOADED(ARM_UI& armUI)
	RETURN HAS_SCALEFORM_MOVIE_LOADED(armUI.uiQuit)
ENDFUNC

// See if the countdown UI has loaded.
FUNC BOOL ARM_IS_COUNTDOWN_UI_LOADED(ARM_UI& armUI)
	RETURN HAS_MINIGAME_COUNTDOWN_UI_LOADED(armUI.uiCountdown)
ENDFUNC

// Release the control UI.
PROC ARM_RELEASE_CONTROL_UI(ARM_UI& armUI)
	CLEANUP_MINIGAME_INSTRUCTIONS(armUI.uiControls)
ENDPROC

// Release the countdown UI.
PROC ARM_RELEASE_COUNTDOWN_UI(ARM_UI& armUI)
	RELEASE_MINIGAME_COUNTDOWN_UI(armUI.uiCountdown)
ENDPROC

// Initialize the quit UI.
PROC ARM_SETUP_QUIT_UI(ARM_UI& armUI, STRING sTitle, STRINg sDetails)
	SETUP_QUIT_UI(armUI.uiQuit, sTitle, sDetails)
ENDPROC

// Draw the quit UI.
PROC ARM_DRAW_QUIT_UI(ARM_UI& armUI)
	DISPLAY_QUIT_UI(armUI.uiQuit)
ENDPROC

// Release the quit UI.
PROC ARM_RELEASE_QUIT_UI(ARM_UI& armUI)
	CLEANUP_QUIT_UI(armUI.uiQuit)
ENDPROC

// Wipes all data slots in the given UI.
PROC ARM_UI_CLEAR_OPTIONS(SCALEFORM_INDEX uiToWipe)
	BEGIN_SCALEFORM_MOVIE_METHOD(uiToWipe, "SET_DATA_SLOT_EMPTY")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
	END_SCALEFORM_MOVIE_METHOD()
	BEGIN_SCALEFORM_MOVIE_METHOD(uiToWipe, "SET_DATA_SLOT_EMPTY")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
	END_SCALEFORM_MOVIE_METHOD()
	BEGIN_SCALEFORM_MOVIE_METHOD(uiToWipe, "SET_DATA_SLOT_EMPTY")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)
	END_SCALEFORM_MOVIE_METHOD()
	BEGIN_SCALEFORM_MOVIE_METHOD(uiToWipe, "SET_DATA_SLOT_EMPTY")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(3)
	END_SCALEFORM_MOVIE_METHOD()
	BEGIN_SCALEFORM_MOVIE_METHOD(uiToWipe, "SET_DATA_SLOT_EMPTY")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(4)
	END_SCALEFORM_MOVIE_METHOD()
	BEGIN_SCALEFORM_MOVIE_METHOD(uiToWipe, "SET_DATA_SLOT_EMPTY")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(5)
	END_SCALEFORM_MOVIE_METHOD()
	BEGIN_SCALEFORM_MOVIE_METHOD(uiToWipe, "SET_DATA_SLOT_EMPTY")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(6)
	END_SCALEFORM_MOVIE_METHOD()
	BEGIN_SCALEFORM_MOVIE_METHOD(uiToWipe, "SET_DATA_SLOT_EMPTY")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(7)
	END_SCALEFORM_MOVIE_METHOD()
	BEGIN_SCALEFORM_MOVIE_METHOD(uiToWipe, "SET_DATA_SLOT_EMPTY")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(8)
	END_SCALEFORM_MOVIE_METHOD()
	BEGIN_SCALEFORM_MOVIE_METHOD(uiToWipe, "SET_DATA_SLOT_EMPTY")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(9)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

// Ask for our scorecard UI
PROC ARM_REQUEST_SCORECARD_MENU_UI(ARM_UI& armUI)
	armUI.uiScorecard = REQUEST_INTRO_UI()
ENDPROC

// Check to see if the scorecard UI has loaded.
FUNC BOOL ARM_IS_SCORECARD_UI_LOADED(ARM_UI& armUI)
	RETURN HAS_SCALEFORM_MOVIE_LOADED(armUI.uiScorecard)
ENDFUNC

// Draw the main menu UI.
PROC ARM_DRAW_SCORECARD_UI(ARM_UI& armUI)
	DISPLAY_INTRO_UI(armUI.uiScorecard)
ENDPROC

// Clean the scorecard UI.
PROC ARM_RELEASE_SCORECARD_UI(ARM_UI& armUI)
	CLEANUP_INTRO_UI(armUI.uiScorecard)
ENDPROC

// Selects the previous item on scorecard. Wraps.
PROC ARM_SCORECARD_SELECT_PREV(ARM_UI& armUI)
	SELECT_INTRO_UI_LIST_ITEM(armUI.uiScorecard, armUI.iScorecardSelection, FALSE)
	
	armUI.iScorecardSelection -= 1
	IF (armUI.iScorecardSelection < 0)
		armUI.iScorecardSelection = 1
	ENDIF
	
	SELECT_INTRO_UI_LIST_ITEM(armUI.uiScorecard, armUI.iScorecardSelection, TRUE)
ENDPROC

// Selects the next item on the scorecard. Wraps.
PROC ARM_SCORECARD_SELECT_NEXT(ARM_UI& armUI)
	SELECT_INTRO_UI_LIST_ITEM(armUI.uiScorecard, armUI.iScorecardSelection, FALSE)
	
	armUI.iScorecardSelection += 1
	IF (armUI.iScorecardSelection > 1)
		armUI.iScorecardSelection = 0
	ENDIF
	
	SELECT_INTRO_UI_LIST_ITEM(armUI.uiScorecard, armUI.iScorecardSelection, TRUE)
ENDPROC

// Resets all flags and timers associated with the countdown UI.
PROC ARM_RESET_COUNTDOWN_UI(ARM_UI& armUI)
	ARM_CLEAR_UI_COUNTDOWN_FLAGS(armUI)
	CANCEL_TIMER(armUI.uiCountdown.CountdownTimer)
ENDPROC

// Accessor for meter value from last frame.
FUNC FLOAT ARM_GET_LAST_METER_VALUE(ARM_UI& ui)
	RETURN ui.fLastMeter
ENDFUNC

// Mutator for meter value from last frame.
PROC ARM_SET_LAST_METER_VALUE(ARM_UI& ui, FLOAT fLastMeter)
	ui.fLastMeter = fLastMeter
ENDPROC
