
//////////////////////////////////////////////////////////////////////
/* arm_input_lib.sch												*/
/* Author: DJ Jones													*/
/* Input functionality for arm wrestling minigame.					*/
//////////////////////////////////////////////////////////////////////

// Collect a frame of local input.
PROC ARM_GET_LOCAL_INPUT(ARM_INPUT& input)
	VECTOR vLeftStick, vRightStick
	INT iLeftX, iLeftY, iRightX, iRightY
	
	// Store last frame's stick data.
	ARM_STORE_LAST_STICKS(input)
	
	// Get analogue stick data.
	GET_CONTROL_VALUE_OF_ANALOGUE_STICKS(iLeftX, iLeftY, iRightX, iRightY)
	vLeftStick.x = TO_FLOAT(iLeftX) / 128.0
	vLeftStick.y = TO_FLOAT(iLeftY) / -128.0
	vRightStick.x = TO_FLOAT(iRightX) / 128.0
	vRightStick.y = TO_FLOAT(iRightY) / -128.0
	
	// Store the stick values.
	ARM_SET_LEFT_STICK_XY(input, vLeftStick.x, vLeftStick.y)
	ARM_SET_RIGHT_STICK_XY(input, vRightStick.x, vRightStick.y)
	
	// Store last frame's button data.
	ARM_STORE_LAST_BUTTONS(input)
	
	// Check for buttons.
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_Y)
		ARM_SET_BUTTON_PRESSED(input, ARMBUTTON_QUIT, TRUE)
	ENDIF
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_X)
		ARM_SET_BUTTON_PRESSED(input, ARMBUTTON_CONFIRM, TRUE)
	ENDIF
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
		ARM_SET_BUTTON_PRESSED(input, ARMBUTTON_CONFIRM, TRUE)
	ENDIF
ENDPROC
