
//////////////////////////////////////////////////////////////////////
/* arm_server.sch													*/
/* Author: DJ Jones													*/
/* Server-side multiplayer definitions for arm wrestling minigame.	*/
//////////////////////////////////////////////////////////////////////


ENUM ARM_SERVER_FLAGS
	ARMSERVERFLAG_QUIT_1CLIENT,
	ARMSERVERFLAG_AWAYWIN,
	ARMSERVERFLAG_HOMEWIN,
	ARMSERVERFLAG_WIN_REACHED,
	ARMSERVERFLAG_MOVETOGAME,
	ARMSERVERFLAG_MOVETOMENU,
	ARMSERVERFLAG_REF_OUTRO_REQUESTED,
	ARMSERVERFLAG_REF_OUTRO_PLAYED,
	ARMSERVERFLAG_SCORE_CHANGING,
	ARMSERVERFLAG_COUNTDOWN_DONE,
	ARMSERVERFLAG_ANIMPHASE_ADJUSTED,
	ARMSERVERFLAG_ANIMPLAY_FINISHED,
	ARMSERVERFLAG_ANIMPLAY_IN_PROGRESS,
	ARMSERVERFLAG_ALL_IN_PLAY_POSITION
ENDENUM

CONST_INT ARM_SCORES 8
ARM_WRESTLER_ID eArmWinner

// Data maintained by the server. Can be read by clients but not written.
STRUCT ARM_SERVER_BROADCAST_DATA
	ARM_MP_STATE mpState
	ARM_GAME_STATE gameState
	structTimer scoreTimer
	FLOAT fScores[ARM_SCORES]
//	FLOAT fLastScore
	INT iTableIndex
	INT iWins[ARMWRESTLERIDS]
	INT iFlags
	INT iClientBucket
	INT iMatchType
	INT iMatchHistoryID
	INT iSceneID
	INT iCurrentHour
	TIME_DATATYPE tdTimeStamps[ARM_SCORES]
	FLOAT fAdjustedPhase
ENDSTRUCT


// Accessor for the multiplayer state.
FUNC ARM_MP_STATE ARM_GET_SERVER_MP_STATE(ARM_SERVER_BROADCAST_DATA& serverData)
	RETURN serverData.mpState
ENDFUNC

// Mutator for the multiplayer state.
PROC ARM_SET_SERVER_MP_STATE(ARM_SERVER_BROADCAST_DATA& serverData, ARM_MP_STATE mpState)
	CDEBUG1LN(DEBUG_ARM_WRESTLING, "am_armwrestling: Setting server mp state: ", ARM_GET_SERVER_MP_STATE_STRING(mpState)) //ENUM_TO_INT(mpState))
	serverData.mpState = mpState
ENDPROC

// Accessor for the multiplayer state.
FUNC ARM_GAME_STATE ARM_GET_SERVER_GAME_STATE(ARM_SERVER_BROADCAST_DATA& serverData)
	RETURN serverData.gameState
ENDFUNC

// String accessor for the multiplayer state, useful for logs
FUNC STRING ARM_GET_SERVER_GAME_STATE_STRING(ARM_GAME_STATE gameState)
	SWITCH gameState
		CASE ARMGAMESTATE_READYMENU
			RETURN "ARMGAMESTATE_READYMENU"
		CASE ARMGAMESTATE_PLAYING
			RETURN "ARMGAMESTATE_PLAYING"
		CASE ARMGAMESTATE_WINNER
			RETURN "ARMGAMESTATE_WINNER"
		DEFAULT
			RETURN "INVALID"
	ENDSWITCH 
ENDFUNC

// Mutator for the multiplayer state.
PROC ARM_SET_SERVER_GAME_STATE(ARM_SERVER_BROADCAST_DATA& serverData, ARM_GAME_STATE gameState)
	CDEBUG1LN(DEBUG_ARM_WRESTLING, "am_armwrestling: Setting server minigame state: ", ARM_GET_SERVER_GAME_STATE_STRING(gameState))//ENUM_TO_INT(gameState))
	serverData.gameState = gameState
ENDPROC


// Checks to see if a flag is set in the server bitfield
FUNC BOOL ARM_SERVER_GET_FLAG(ARM_SERVER_BROADCAST_DATA& serverData, ARM_SERVER_FLAGS eFlagToCheck)
	RETURN IS_BIT_SET(serverData.iFlags, ENUM_TO_INT(eFlagToCheck))
ENDFUNC

// Set or clear a flag for the server.
PROC ARM_SERVER_SET_FLAG(ARM_SERVER_BROADCAST_DATA& serverData, ARM_SERVER_FLAGS eFlagToSet, BOOL bState)
	IF bState
		SET_BIT(serverData.iFlags, ENUM_TO_INT(eFlagToSet))
	ELSE
		CLEAR_BIT(serverData.iFlags, ENUM_TO_INT(eFlagToSet))
	ENDIF
ENDPROC

// Clear all server flags.
PROC ARM_SERVER_CLEAR_FLAGS(ARM_SERVER_BROADCAST_DATA& serverData)
	serverData.iFlags = 0
ENDPROC

// Accessor for score timer state.
FUNC BOOL ARM_IS_SCORE_TIMER_STARTED(ARM_SERVER_BROADCAST_DATA& serverData)
	RETURN IS_TIMER_STARTED(serverData.scoreTimer)
ENDFUNC

// Accessor for score timer value.
FUNC FLOAT ARM_GET_SCORE_TIMER_VALUE(ARM_SERVER_BROADCAST_DATA& serverData)
	IF NOT IS_TIMER_STARTED(serverData.scoreTimer)
		RETURN 0.0
	ENDIF
	
	RETURN GET_TIMER_IN_SECONDS(serverData.scoreTimer)
ENDFUNC

// Mutator to start score timer.
PROC ARM_RESET_SCORE_TIMER(ARM_SERVER_BROADCAST_DATA& serverData, FLOAT fStart = 0.0, BOOL bForceRestart = TRUE)
	IF NOT IS_TIMER_STARTED(serverData.scoreTimer)
		START_TIMER_AT(serverData.scoreTimer, fStart)
	ELIF bForceRestart
		RESTART_TIMER_AT(serverData.scoreTimer, fStart)
	ENDIF
ENDPROC

// Mutator to cancel score timer.
PROC ARM_CANCEL_SCORE_TIMER(ARM_SERVER_BROADCAST_DATA& serverData)
	IF IS_TIMER_STARTED(serverData.scoreTimer)
		CANCEL_TIMER(serverData.scoreTimer)
	ENDIF
ENDPROC

// Accessor for the score.
FUNC FLOAT ARM_GET_SERVER_SCORE(ARM_SERVER_BROADCAST_DATA& serverData)
	INT iCounter
	REPEAT ARM_SCORES iCounter
		IF NOT (serverData.tdTimeStamps[ARM_SCORES-iCounter-1] = NULL)
			RETURN serverData.fScores[ARM_SCORES-iCounter-1]
		ENDIF
	ENDREPEAT
	RETURN 0.0
ENDFUNC

// check if score is changing
//FUNC BOOL ARM_IS_SERVER_SCORE_CHANGING(ARM_SERVER_BROADCAST_DATA& serverData)
//	
//	IF ARM_SERVER_GET_FLAG(serverData, ARMSERVERFLAG_SCORE_CHANGING)
//		CDEBUG1LN(DEBUG_ARM_WRESTLING, " score is changing ")
//		RETURN TRUE
//	ELSE
//		CDEBUG1LN(DEBUG_ARM_WRESTLING, " score is the same as ever ")
//		RETURN FALSE
//	ENDIF
//	
//	RETURN FALSE
//ENDFUNC

// Mutator for the score (marginally add).
PROC ARM_UPDATE_CURRENT_SCORE(ARM_SERVER_BROADCAST_DATA& serverData, FLOAT fScore)
	INT iCounter
	REPEAT ARM_SCORES iCounter
		IF NOT (serverData.tdTimeStamps[ARM_SCORES-iCounter-1] = NULL)
			
			serverData.fScores[ARM_SCORES-iCounter-1] += fScore
			
//			IF serverData.fLastScore != serverData.fScores[ARM_SCORES-iCounter-1]
//				serverData.fLastScore = serverData.fScores[ARM_SCORES-iCounter-1]
//				IF NOT ARM_SERVER_GET_FLAG(serverData, ARMSERVERFLAG_SCORE_CHANGING)
//					CDEBUG1LN(DEBUG_ARM_WRESTLING, " score changing flag set ")
//					ARM_SERVER_SET_FLAG(serverData, ARMSERVERFLAG_SCORE_CHANGING, TRUE)
//				ENDIF
//			ELSE
//				IF ARM_SERVER_GET_FLAG(serverData, ARMSERVERFLAG_SCORE_CHANGING)
//					CDEBUG1LN(DEBUG_ARM_WRESTLING, " score changing flag hella unset ")
//					ARM_SERVER_SET_FLAG(serverData, ARMSERVERFLAG_SCORE_CHANGING, FALSE)
//				ENDIF
//			ENDIF
			
			EXIT
		ENDIF
	ENDREPEAT
ENDPROC

// Add scores to local player based on the net time stamp
PROC ARM_ADD_TIMESTAMPED_SCORE(ARM_SERVER_BROADCAST_DATA& serverData, TIME_DATATYPE tdTimeStamp, FLOAT fScore)
	INT iCounter
	BOOL bIsZero
	REPEAT ARM_SCORES iCounter
		bIsZero = (serverData.tdTimeStamps[iCounter] = NULL)
		IF (iCounter > 0)
			
			serverData.fScores[iCounter-1] = serverData.fScores[iCounter]
			serverData.tdTimeStamps[iCounter-1] = serverData.tdTimeStamps[iCounter]
			
//			CDEBUG1LN(DEBUG_ARM_WRESTLING, "%%%%% serverData.fScores      [", iCounter-1, "] = ", serverData.fScores[iCounter-1])
//			CDEBUG1LN(DEBUG_ARM_WRESTLING, "%%%%% serverData.tdTimeStamps [", iCounter-1, "] = ", GET_TIME_AS_STRING(serverData.tdTimeStamps[iCounter-1]))
			
		ENDIF
		IF bIsZero
		OR (iCounter >= (ARM_SCORES-1))
			serverData.fScores[iCounter] = fScore
			serverData.tdTimeStamps[iCounter] = tdTimeStamp
			
//			CDEBUG1LN(DEBUG_ARM_WRESTLING, "^^^^^ serverData.fScores      [", iCounter, "] = ", serverData.fScores[iCounter])
//			CDEBUG1LN(DEBUG_ARM_WRESTLING, "^^^^^ serverData.tdTimeStamps [", iCounter, "] = ", GET_TIME_AS_STRING(serverData.tdTimeStamps[iCounter]))
			
			iCounter = ARM_SCORES
		ENDIF
	ENDREPEAT
ENDPROC

// Clear Server Scores
PROC ARM_CLEAR_SERVER_SCORES(ARM_SERVER_BROADCAST_DATA& serverData)
	INT iCounter
	REPEAT ARM_SCORES iCounter
		serverData.fScores[iCounter] = 0.0
		serverData.tdTimeStamps[iCounter] = NULL
	ENDREPEAT
ENDPROC

// Accessor for table index.
FUNC INT ARM_GET_SERVER_TABLE_INDEX(ARM_SERVER_BROADCAST_DATA& serverData)
	RETURN serverData.iTableIndex
ENDFUNC

// Mutator for table index.
PROC ARM_SET_SERVER_TABLE_INDEX(ARM_SERVER_BROADCAST_DATA& serverData, INT iTableIndex)
	serverData.iTableIndex = iTableIndex
ENDPROC

// Accessor for one wrestler's win tally.
FUNC INT ARM_GET_SERVER_WINS(ARM_SERVER_BROADCAST_DATA& serverData, ARM_WRESTLER_ID wrestlerID)
	IF wrestlerID = ARMWRESTLERIDS
		RETURN -1
	ENDIF
	RETURN serverData.iWins[wrestlerID]
ENDFUNC

// Accessor for one wrestler's win tally (using int instead of wrestler ID).
FUNC INT ARM_GET_SERVER_WINS_INT(ARM_SERVER_BROADCAST_DATA& serverData, INT iID)
	IF iID < 0 OR iID >= ENUM_TO_INT(ARMWRESTLERIDS)
		RETURN -1
	ENDIF
	RETURN serverData.iWins[iID]
ENDFUNC

// Mutator for one wrestler's win tally.
PROC ARM_SET_SERVER_WINS(ARM_SERVER_BROADCAST_DATA& serverData, ARM_WRESTLER_ID wrestlerID, INT iWins)
	IF wrestlerID = ARMWRESTLERIDS
		EXIT
	ENDIF
	serverData.iWins[wrestlerID] = iWins
ENDPROC

// Add one win for a wrestler.
PROC ARM_ADD_SERVER_WIN(ARM_SERVER_BROADCAST_DATA& serverData, ARM_WRESTLER_ID wrestlerID)
	IF wrestlerID = ARMWRESTLERIDS
		CDEBUG1LN(DEBUG_ARM_WRESTLING, "add server win was sent ARMWRESTLERIDS, exiting")
		EXIT
	ENDIF
	serverData.iWins[wrestlerID] += 1
ENDPROC

// Accessor for current client bucket index.
FUNC INT ARM_GET_SERVER_CLIENT_BUCKET(ARM_SERVER_BROADCAST_DATA& serverData)
	RETURN serverData.iClientBucket
ENDFUNC

// Mutator for current client bucket index.
PROC ARM_SET_SERVER_CLIENT_BUCKET(ARM_SERVER_BROADCAST_DATA& serverData, INT iClientBucket)
	serverData.iClientBucket = iClientBucket
ENDPROC
