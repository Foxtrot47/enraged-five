
//////////////////////////////////////////////////////////////////////
/* arm_data.sch														*/
/* Author: DJ Jones													*/
/* Hard-coded data for arm wrestling minigame.						*/
//////////////////////////////////////////////////////////////////////


CONST_INT iARM_TABLES 12


FUNC STRING GET_ARM_RADIO_EMITTER_NAME(INT iTableIndex)
	
	SWITCH iTableIndex
		CASE 1		RETURN "" // outdated - this is the vagos clubhouse
		CASE 2		RETURN "" // outdated - this is the lost gang hideout
		CASE 3		RETURN "MP_ARM_WRESTLING_RADIO_10" // << 1443.43, -2219.88, 60.3489 >>
		CASE 4		RETURN "MP_ARM_WRESTLING_RADIO_08" // << -82.4445, -1327.78, 28.7965 >>
		CASE 5		RETURN "MP_ARM_WRESTLING_RADIO_07" // << 901.9729, -172.1309, 73.5662 >>
		CASE 6		RETURN "MP_ARM_WRESTLING_RADIO_09" // << 369.092, -2445.45, 5.51899 >>
		CASE 7		RETURN "MP_ARM_WRESTLING_RADIO_03" // << 1221.22, 2726.06, 37.5038 >>
		CASE 8		RETURN "MP_ARM_WRESTLING_RADIO_04" // << 1051.22, 2662.97, 39.0263 >>
		CASE 9		RETURN "MP_ARM_WRESTLING_RADIO_05" // << 270.225, 2603.96, 44.2309 >>
		CASE 10		RETURN "MP_ARM_WRESTLING_RADIO_01" // << -191.917, 6273.26, 30.9709 >>
		CASE 11		RETURN "MP_ARM_WRESTLING_RADIO_02" // << 1685.72, 4970.39, 42.096 >>
		CASE 12		RETURN "MP_ARM_WRESTLING_RADIO_06" // << 2340.87, 3130.47, 47.6839 >>
		DEFAULT		RETURN ""
	ENDSWITCH
	
ENDFUNC

FUNC STRING GET_ARM_TABLE_NAME(INT iTableIndex)
	
	SWITCH iTableIndex
		CASE 1		RETURN "" // outdated - this is the vagos clubhouse
		CASE 2		RETURN "" // outdated - this is the lost gang hideout
		CASE 3		RETURN "ARMMP_TABLE2" // << 1443.43, -2219.88, 60.3489 >>
		CASE 4		RETURN "ARMMP_TABLE3" // << -82.4445, -1327.78, 28.7965 >>
		CASE 5		RETURN "ARMMP_TABLE1" // << 901.9729, -172.1309, 73.5662 >>
		CASE 6		RETURN "ARMMP_TABLE4" // << 369.092, -2445.45, 5.51899 >>
		CASE 7		RETURN "ARMMP_TABLE5" // << 1221.22, 2726.06, 37.5038 >>
		CASE 8		RETURN "ARMMP_TABLE6" // << 1051.22, 2662.97, 39.0263 >>
		CASE 9		RETURN "ARMMP_TABLE7" // << 270.225, 2603.96, 44.2309 >>
		CASE 10		RETURN "ARMMP_TABLE8" // << -191.917, 6273.26, 30.9709 >>
		CASE 11		RETURN "ARMMP_TABLE9" // << 1685.72, 4970.39, 42.096 >>
		CASE 12		RETURN "ARMMP_TABLE10" // << 2340.87, 3130.47, 47.6839 >>
		DEFAULT		RETURN "SPEC_ML_ARM"
	ENDSWITCH
	
ENDFUNC

FUNC FLOAT ARM_GET_INTRO_CAMERA_DATA(INT iTableIndex, FLOAT fTableRot)
	SWITCH iTableIndex
		CASE 3
		CASE 4
		CASE 8	RETURN fTableRot - 180.0
		DEFAULT RETURN fTableRot
	ENDSWITCH
ENDFUNC

// Fill in table data based on a passed index.
PROC ARM_GET_TABLE_DATA(VECTOR& vTablePos, FLOAT& fTableRot, INT iTableIndex, BOOL bUsePropLocation = FALSE)
//	OBJECT_INDEX tableObject 
	VECTOR vTableRot

	// Get the predicted table location
	SWITCH iTableIndex
		// Vagos clubhouse, upstairs
		CASE 1
			vTablePos = <<1116.5148,-3153.2751,-37.569>>
			fTableRot = 90.0
		BREAK
		
		// Lost clubhouse
		CASE 2
			vTablePos = <<1003.2322, -3165.7361, -34.5976>>
			fTableRot = 0.0
		BREAK
		
		// El Burro Heights
		CASE 3
			vTablePos = <<1443.340332,-2219.010254,60.348946>> //<< 1443.43, -2219.88, 60.3489 >> //<<1444.9139, -2219.5825, 60.5789>>
			fTableRot = 105.023308
//			IF NOT g_bMGIntroSwitch
//				fTableRot = 105.023308
//			ELSE
//				fTableRot = -74.976692
//			ENDIF
		BREAK
		
		// South Los Santos
		CASE 4
			vTablePos = << -82.4445, -1327.78, 28.7965 >> //<<-81.6234, -1328.5819, 28.7965>>
			fTableRot = 180.0
//			IF NOT g_bMGIntroSwitch
//				fTableRot = 180.0
//			ELSE
//				fTableRot = 0.0
//			ENDIF
			
		BREAK
		
		// San Andreas
		CASE 5
			vTablePos = <<901.9729, -172.1309, 73.5662>>
			fTableRot = 0.0
		BREAK
		
		// Elysian Island
		CASE 6
			vTablePos = << 369.092, -2445.45, 5.51899 >> //<<368.8853, -2446.1748, 5.0717>>
			fTableRot = -178.027313
		BREAK
		
		// Grande Senora Desert I
		CASE 7
			vTablePos = << 1221.22, 2726.06, 37.5038 >> //<<1222.7200, 2721.2000, 37.0042>>
			fTableRot = -63.207565
		BREAK
		
		// Grande Senora Desert II
		CASE 8
			vTablePos = <<1051.675171,2663.610107,39.026268>> //<<1051.4794, 2667.2903, 38.5509>>
			fTableRot = 111.577721
//			IF NOT g_bMGIntroSwitch
//				fTableRot = 111.577721
//			ELSE
//				fTableRot = -68.422279
//			ENDIF
		BREAK
		
		// Harmony
		CASE 9
			vTablePos = << 270.225, 2603.96, 44.2309 >> //<<273.3710, 2607.7676, 43.7028>>
			fTableRot = 140.468430
		BREAK
		
		// Paleto Bay
		CASE 10
			vTablePos = << -191.917, 6273.26, 30.9709 >> //<<-193.7423, 6277.6753, 30.4892 >>
			fTableRot = -3.810523
		BREAK
		
		// Grapeseed
		CASE 11
			vTablePos = << 1685.72, 4970.39, 42.096 >> //<<1687.1375, 4966.8379, 42.0569>>
			fTableRot = 15.229779
		BREAK
		
		// Senora Freeway
		CASE 12
			vTablePos = << 2340.87, 3130.47, 47.6839 >> //<<2343.9714, 3134.0540, 47.2088>>
			fTableRot = 126.138821 //-53.861179
		BREAK
		
		DEFAULT
			SCRIPT_ASSERT("Invalid table index passed to ARM_GET_TABLE_DATA!")
		BREAK
	ENDSWITCH
	
	// Find the real prop and get its position and heading.
	IF bUsePropLocation
		CDEBUG1LN(DEBUG_ARM_WRESTLING, "get table data:  before looking for object, vTablePos = ", vTablePos)
		CDEBUG1LN(DEBUG_ARM_WRESTLING, "get table data:  before looking for object, fTableRot = ", fTableRot)
		
		IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(vTablePos, 8.0, PROP_ARM_WRESTLE_01)
			GET_COORDS_AND_ROTATION_OF_CLOSEST_OBJECT_OF_TYPE(vTablePos, 8.0, PROP_ARM_WRESTLE_01, vTablePos, vTableRot, EULER_XYZ)
			// these tables have hard coded rotations to account for cam positions and environment
			IF iTableIndex != 12
			AND iTableIndex != 3
			AND iTableIndex != 4
			AND iTableIndex != 8
				fTableRot = vTableRot.z
			ENDIF
			CDEBUG1LN(DEBUG_ARM_WRESTLING, "get table data: object exists at index ", iTableIndex)
			CDEBUG1LN(DEBUG_ARM_WRESTLING, "get table data: actual vTablePos =  ", vTablePos)
			CDEBUG1LN(DEBUG_ARM_WRESTLING, "get table data: actual fTableRot = ", fTableRot)
		ELSE
			CDEBUG1LN(DEBUG_ARM_WRESTLING, "get table data: OBJECT DOES NOT EXIST! index used is ", iTableIndex)
		ENDIF
	ENDIF

ENDPROC

// Find out which table is nearest the local player ped.
FUNC INT ARM_GET_NEAREST_TABLE_INDEX()
	VECTOR vPlayerPos
	VECTOR vTablePos
	FLOAT fCurDist, fShortestDist
	INT iResult, i
	FLOAT fTableRot
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
	ENDIF
	
	fShortestDist = -1.0
	
	REPEAT iARM_TABLES i
		ARM_GET_TABLE_DATA(vTablePos, fTableRot, i + 1, TRUE)
		fCurDist = VDIST2(vPlayerPos, vTablePos)
		IF fShortestDist < 0.0 OR fShortestDist > fCurDist
			fShortestDist = fCurDist
			iResult = i + 1
			CDEBUG1LN(DEBUG_ARM_WRESTLING, "get nearest table index: iResult set to ", iResult)
		ENDIF
	ENDREPEAT
	
	RETURN iResult
ENDFUNC

// Find out which table is nearest the local player ped. Return false if none.
FUNC BOOL ARM_GET_NEAREST_TABLE_LOCATION(VECTOR& vResult, FLOAT fMaxDist = 1000000.0)
	VECTOR vPlayerPos
	VECTOR vTablePos
	FLOAT fCurDist, fShortestDist
	FLOAT fTableRot
	INT i
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
	ENDIF
	
	vResult = <<0,0,0>>
	fShortestDist = -1.0
	
	REPEAT iARM_TABLES i
		ARM_GET_TABLE_DATA(vTablePos, fTableRot, i + 1, TRUE)
		fCurDist = VDIST2(vPlayerPos, vTablePos)
		IF fCurDist < fMaxDist AND (fShortestDist < 0.0 OR fShortestDist > fCurDist)
			fShortestDist = fCurDist
			vResult = vTablePos
		ENDIF
	ENDREPEAT
	
	RETURN NOT IS_VECTOR_ZERO(vResult)
ENDFUNC

// Return the name of the sound
FUNC STRING ARM_GET_CROWD_SOUND_BANK_FROM_INDEX(INT iSoundIndex, BOOL bWin)
	IF bWin
		SWITCH iSoundIndex
			CASE 1 RETURN "ARM_WRESTLING_WIN_01"
			CASE 2 RETURN "ARM_WRESTLING_WIN_02"
			CASE 3 RETURN "ARM_WRESTLING_WIN_03"
		ENDSWITCH
	ELSE
		SWITCH iSoundIndex
			CASE 1 RETURN "ARM_WRESTLING_LOSE_01"
			CASE 2 RETURN "ARM_WRESTLING_LOSE_02"
			CASE 3 RETURN "ARM_WRESTLING_LOSE_03"
		ENDSWITCH
	ENDIF
	
	RETURN ""
ENDFUNC

// Return the name of the sound
FUNC STRING ARM_GET_CROWD_SOUND_FROM_INDEX(INT iSoundIndex, BOOL bWin)
	IF bWin
		SWITCH iSoundIndex
			CASE 1 RETURN "ARM_WRESTLING_WIN_01_MASTER"
			CASE 2 RETURN "ARM_WRESTLING_WIN_02_MASTER"
			CASE 3 RETURN "ARM_WRESTLING_WIN_03_MASTER"
		ENDSWITCH
	ELSE
		SWITCH iSoundIndex
			CASE 1 RETURN "ARM_WRESTLING_LOSE_01_MASTER"
			CASE 2 RETURN "ARM_WRESTLING_LOSE_02_MASTER"
			CASE 3 RETURN "ARM_WRESTLING_LOSE_03_MASTER"
		ENDSWITCH
	ENDIF
	
	RETURN ""
ENDFUNC

// Gets a random cheering animation.
FUNC STRING ARM_GET_RANDOM_CHEER()
	INT iResult = GET_RANDOM_INT_IN_RANGE(0, 4)
	
	SWITCH iResult
		CASE 0 RETURN "cheer_on_fight_a"
		CASE 1 RETURN "cheer_on_fight_b"
		CASE 2 RETURN "cheer_on_fight_c"
		CASE 3 RETURN "cheer_on_fight_d"
		DEFAULT RETURN ""
	ENDSWITCH
ENDFUNC

// Check for whether a string is equivalent to one of the cheering anims.
FUNC BOOL ARM_IS_STRING_A_CHEER(TEXT_LABEL_23 sCheer)
	IF ARE_STRINGS_EQUAL(sCheer, "cheer_on_fight_a")
	OR ARE_STRINGS_EQUAL(sCheer, "cheer_on_fight_b")
	OR ARE_STRINGS_EQUAL(sCheer, "cheer_on_fight_c")
	OR ARE_STRINGS_EQUAL(sCheer, "cheer_on_fight_d")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC
