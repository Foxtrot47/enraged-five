USING "commands_stats.sch"
USING "net_prints.sch"
USING "net_events.sch"
USING "leader_board_common.sch"
USING "socialclub_leaderboard.sch"

INT iArmStats[ARM_STAT_TOTAL]

PROC WRITE_ARM_WRESTLING_MP_SCLB_DATA()
	CDEBUG1LN(DEBUG_DARTS, "-!!- WRITE_ARM_WRESTLING_MP_SCLB_DATA")
	TEXT_LABEL_23 sIdentifier[1]
	TEXT_LABEL_31 categoryName[1]
	IF INIT_LEADERBOARD_WRITE(LEADERBOARD_MINI_GAMES_ARM_WRESTLING,sIdentifier,categoryName,0)
	
		// *SCORE_COLUMN( AGG_LAST ) : COLUMN_ID_LB_SCORE - LBCOLUMNTYPE_INT64
	   // WIN_STREAK( AGG_MAX ) : COLUMN_ID_LB_WIN_STREAK - LBCOLUMNTYPE_INT32
	    //NUMBER_WINS( AGG_SUM ) : COLUMN_ID_LB_NUM_WINS - LBCOLUMNTYPE_INT64
	    //NUMBER_MATCHES( AGG_SUM ) : COLUMN_ID_LB_NUM_MATCHES - LBCOLUMNTYPE_INT32
	    //FASTEST_WIN_TIME( AGG_MAX ) : COLUMN_ID_LB_BEST_TIME - LBCOLUMNTYPE_INT64
	    //TOTAL_WIN_TIME( AGG_SUM ) : COLUMN_ID_LB_TOTAL_TIME - LBCOLUMNTYPE_INT64
		
		//LEADERBOARDS_WRITE_SET_INT(0, iArmStats[ARM_STAT_NUM_WINS])
		//CDEBUG1LN(DEBUG_ARM_WRESTLING, " -!!- SCORE_COLUMN: ", iArmStats[ARM_STAT_NUM_WINS])
		
		LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_WIN_STREAK, iArmStats[ARM_STAT_STREAK], TO_FLOAT(iArmStats[ARM_STAT_STREAK]))
		CDEBUG1LN(DEBUG_ARM_WRESTLING, " -!!- [ARM_STAT_STREAK]: ", iArmStats[ARM_STAT_STREAK])
		
		LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_NUM_WINS, iArmStats[ARM_STAT_NUM_WINS], TO_FLOAT(iArmStats[ARM_STAT_NUM_WINS]))
		CDEBUG1LN(DEBUG_ARM_WRESTLING, " -!!- [ARM_STAT_NUM_WINS]: ", iArmStats[ARM_STAT_NUM_WINS])
		
		LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_BEST_TIME, iArmStats[ARM_STAT_FASTEST_WIN], TO_FLOAT(iArmStats[ARM_STAT_FASTEST_WIN]))
		CDEBUG1LN(DEBUG_ARM_WRESTLING, " -!!- [ARM_STAT_FASTEST_WIN]: ", iArmStats[ARM_STAT_FASTEST_WIN])
		
		LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_TOTAL_TIME, iArmStats[ARM_STAT_TOTAL_TIME], TO_FLOAT(iArmStats[ARM_STAT_TOTAL_TIME]))
		CDEBUG1LN(DEBUG_ARM_WRESTLING, " -!!- [ARM_STAT_TOTAL_TIME]: ", iArmStats[ARM_STAT_TOTAL_TIME])
		
		LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_NUM_MATCHES_LOST, iArmStats[ARM_STAT_NUM_LOSSES], TO_FLOAT(iArmStats[ARM_STAT_NUM_LOSSES]))
		CDEBUG1LN(DEBUG_ARM_WRESTLING, " -!!- [ARM_STAT_NUM_LOSSES]: ", iArmStats[ARM_STAT_NUM_LOSSES])
	ENDIF
	//mpGlobals.g_bFlushLBWrites = TRUE
ENDPROC

// EOF
