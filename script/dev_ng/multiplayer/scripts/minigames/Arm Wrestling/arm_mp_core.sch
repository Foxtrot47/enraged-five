
//////////////////////////////////////////////////////////////////////
/* arm_mp_core.sch													*/
/* Author: DJ Jones													*/
/* Setup and cleanup functionality for arm wrestling multiplayer.	*/
//////////////////////////////////////////////////////////////////////
USING "fmmc_mp_setup_mission.sch"


// Set up network requests and registry.
// Returns FALSE if the script fails to receive and initial network broadcast.
FUNC BOOL ARM_NETWORK_INIT(MP_MISSION_DATA& missionScriptArgs, ARM_SERVER_BROADCAST_DATA& serverData, ARM_PLAYER_BROADCAST_DATA& clientBD[iMAX_ARM_PARTICIPANTS], ARM_PARTICIPANT_INFO & info, BOOL bSetMissionID = TRUE)
	INT iParticipantID
	
	CLEAR_HELP()
	// This marks the script as a net script, and handles any instancing setup. 
	IF missionScriptArgs.mdID.idMission <> eAM_ARMWRESTLING
		PRINTLN("Changing missionScriptArgs.mdID.idMission to eAM_ARMWRESTLING...")
		missionScriptArgs.mdID.idMission = eAM_ARMWRESTLING
	ENDIF
	
	
	//If we are in freemode launch diffrently
	IF GET_CURRENT_GAMEMODE() = GAMEMODE_FM
	
		//Freemode mission start details
		INT iPlayerMissionToLoad = FMMC_MINI_GAME_CREATOR_ID
		INT iMissionVariation 	 = 0
		INT iNumberOfPlayers 	 = iMAX_ARM_PARTICIPANTS //2
		//Call the function that controls the setting up of this script
		FMMC_PROCESS_PRE_GAME_COMMON(missionScriptArgs, iPlayerMissionToLoad, iMissionVariation, iNumberOfPlayers)
		//when minigame launched in apartments dont set this, Interior script checks if there is a current mission and will force out of apartment.
		IF bSetMissionID
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_MG_ARM_WRESTLING
		ENDIF
		//Script has started set the global broadcast data up so every body knows
		SET_FM_MISSION_LAUNCHED_SUCESS(missionScriptArgs.mdID.idCreator, missionScriptArgs.mdID.idVariation, missionScriptArgs.iInstanceId)
	
	//If we are not in freemode deal with it normally
	ELSE
		SETUP_MP_MISSION_FOR_NETWORK(GET_MAX_NUM_PARTICIPANTS_FOR_MP_MISSION(eAM_ARMWRESTLING), missionScriptArgs)
		
		// Wait until the script is properly running (inline wait).
		HANDLE_NET_SCRIPT_INITIALISATION()
		
		// This script will not be paused if another script calls PAUSE_GAME.
		SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
	ENDIF
	
	//Common to all modes	
	
	// Make code aware of our server and player broadcast data variables.
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverData, SIZE_OF(serverData))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(clientBD, SIZE_OF(clientBD))
	
	// KGM: Wait for the first network broadcast before moving on
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		RETURN FALSE
	ENDIF
	
	// Set the server and player states, and server should reserve peds & objects.
	iParticipantID = PARTICIPANT_ID_TO_INT()
	
	INITIALIZE_ARM_PARTICIPANT_INFO(info)
	
//	eSelfID = INT_TO_ENUM(ARM_WRESTLER_ID, iParticipantID)
//	eOpponentID = INT_TO_ENUM(ARM_WRESTLER_ID, 1 - iParticipantID)
	
	PRINTLN("iParticipantID = ", iParticipantID)
	PRINTLN("Reserving ", iNETWORK_OBJECTS, " objects...")
	
//	IF eSelfID = ARMWRESTLERID_AWAY
//		RESERVE_NETWORK_MISSION_OBJECTS(iNETWORK_OBJECTS) // Local attachment obj
//		ARM_SET_PLAYER_MP_STATE(clientBD[ARMWRESTLERID_AWAY], ARMMPSTATE_INIT)
//	ELIF eSelfID = ARMWRESTLERID_HOME
//		RESERVE_NETWORK_MISSION_OBJECTS(iNETWORK_OBJECTS)
//		ARM_SET_SERVER_MP_STATE(serverData, ARMMPSTATE_INIT)
//		ARM_SET_PLAYER_MP_STATE(clientBD[ARMWRESTLERID_HOME], ARMMPSTATE_INIT)
//	ENDIF
	
	RESERVE_NETWORK_MISSION_OBJECTS(iNETWORK_OBJECTS) // Local attachment obj
	ARM_SET_PLAYER_MP_STATE(clientBD[iParticipantID], ARMMPSTATE_INIT)
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		ARM_SET_SERVER_MP_STATE(serverData, ARMMPSTATE_INIT)
		//serverData.iMatchHistoryID = PLAYSTATS_CREATE_MATCH_HISTORY_ID()
		PLAYSTATS_CREATE_MATCH_HISTORY_ID_2(serverData.iMatchType, serverData.iMatchHistoryID)
	ENDIF
	
	RETURN TRUE
ENDFUNC
