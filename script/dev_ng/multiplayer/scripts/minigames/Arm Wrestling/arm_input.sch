
//////////////////////////////////////////////////////////////////////
/* arm_input.sch													*/
/* Author: DJ Jones													*/
/* Input definitions for arm wrestling minigame.					*/
//////////////////////////////////////////////////////////////////////


// Buttons that can be pressed during the game.
ENUM ARM_BUTTONS
	ARMBUTTON_MENU_UP,
	ARMBUTTON_MENU_DOWN,
	ARMBUTTON_CONFIRM,
	ARMBUTTON_QUIT,
	ARMBUTTONS
ENDENUM


// One frame's worth of input data. Stick inputs stored in world space.
STRUCT ARM_INPUT
	structTimer delayTimer
	FLOAT fLeftStickX
	FLOAT fLeftStickY
	FLOAT fRightStickX
	FLOAT fRightStickY
	FLOAT fLastLeftStickX
	FLOAT fLastLeftStickY
	FLOAT fLastRightStickX
	FLOAT fLastRightStickY
	INT iButtons
	INT iLastButtons
ENDSTRUCT


// Accessor for delay timer state.
FUNC BOOL ARM_IS_DELAY_TIMER_STARTED(ARM_INPUT& input)
	RETURN IS_TIMER_STARTED(input.delayTimer)
ENDFUNC

// Accessor for delay timer value.
FUNC FLOAT ARM_GET_DELAY_TIMER_VALUE(ARM_INPUT& input)
	IF NOT IS_TIMER_STARTED(input.delayTimer)
		RETURN 0.0
	ENDIF
	
	RETURN GET_TIMER_IN_SECONDS(input.delayTimer)
ENDFUNC

// Mutator to start delay timer.
PROC ARM_RESET_DELAY_TIMER(ARM_INPUT& input, FLOAT fStart = 0.0, BOOL bForceRestart = TRUE)
	IF NOT IS_TIMER_STARTED(input.delayTimer)
		START_TIMER_AT(input.delayTimer, fStart)
	ELIF bForceRestart
		RESTART_TIMER_AT(input.delayTimer, fStart)
	ENDIF
ENDPROC

// Mutator to cancel delay timer.
PROC ARM_CANCEL_DELAY_TIMER(ARM_INPUT& input)
	IF IS_TIMER_STARTED(input.delayTimer)
		CANCEL_TIMER(input.delayTimer)
	ENDIF
ENDPROC
 
// Accessor for left stick x last frame position.
FUNC FLOAT ARM_GET_LAST_LEFT_STICK_X(ARM_INPUT& input)
	RETURN input.fLastLeftStickX
ENDFUNC

// Accessor for left stick y last frame position.
FUNC FLOAT ARM_GET_LAST_LEFT_STICK_Y(ARM_INPUT& input)
	RETURN input.fLastLeftStickY
ENDFUNC

// Accessor for right stick x last frame position.
FUNC FLOAT ARM_GET_LAST_RIGHT_STICK_X(ARM_INPUT& input)
	RETURN input.fLastRightStickX
ENDFUNC

// Accessor for right stick y last frame position.
FUNC FLOAT ARM_GET_LAST_RIGHT_STICK_Y(ARM_INPUT& input)
	RETURN input.fLastRightStickY
ENDFUNC

// Accessor for left stick last frame position, returned as a vector.
FUNC VECTOR ARM_GET_LAST_LEFT_STICK_AS_VECTOR(ARM_INPUT& input)
	RETURN <<input.fLastLeftStickX, input.fLastLeftStickY, 0.0>>
ENDFUNC

// Accessor for left stick last frame position, returned as a vector.
FUNC VECTOR ARM_GET_LAST_RIGHT_STICK_AS_VECTOR(ARM_INPUT& input)
	RETURN <<input.fLastRightStickX, input.fLastRightStickY, 0.0>>
ENDFUNC

// Accessor to get the change in stick positions.
FUNC FLOAT ARM_GET_LARGEST_STICK_CHANGE(ARM_INPUT& input)
	FLOAT fLeftChangeX = ABSF(input.fLeftStickX - input.fLastLeftStickX)
	FLOAT fLeftChangeY = ABSF(input.fLeftStickY - input.fLastLeftStickY)
	FLOAT fRightChangeX = ABSF(input.fRightStickX - input.fLastRightStickX)
	FLOAT fRightChangeY = ABSF(input.fRightStickY - input.fLastRightStickY)
	FLOAT fLeftChange = fLeftChangeX * fLeftChangeX + fLeftChangeY * fLeftChangeY
	FLOAT fRightChange = fRightChangeX * fRightChangeX + fRightChangeY * fRightChangeY
	
	IF fLeftChange > fRightChange
		RETURN SQRT(fLeftChange)
	ENDIF
	
	RETURN SQRT(fRightChange)
ENDFUNC

// Accessor to get the change in stick positions (ignores y axis).
FUNC FLOAT ARM_GET_LARGEST_STICK_CHANGE_HORIZONTAL(ARM_INPUT& input)
	FLOAT fLeftChange = ABSF(input.fLeftStickX - input.fLastLeftStickX)
	FLOAT fRightChange = ABSF(input.fRightStickX - input.fLastRightStickX)
	
	IF fLeftChange > fRightChange
	OR IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		RETURN SQRT(fLeftChange)
	ENDIF
	
	RETURN SQRT(fRightChange)
ENDFUNC

// Accessor for left stick x position.
FUNC FLOAT ARM_GET_LEFT_STICK_X(ARM_INPUT& input)
	RETURN input.fLeftStickX
ENDFUNC

// Mutator for left stick x position.
PROC ARM_SET_LEFT_STICK_X(ARM_INPUT& input, FLOAT fLeftStickX)
	input.fLeftStickX = fLeftStickX
ENDPROC

// Accessor for left stick y position.
FUNC FLOAT ARM_GET_LEFT_STICK_Y(ARM_INPUT& input)
	RETURN input.fLeftStickY
ENDFUNC

// Mutator for left stick y position.
PROC ARM_SET_LEFT_STICK_Y(ARM_INPUT& input, FLOAT fLeftStickY)
	input.fLeftStickY = fLeftStickY
ENDPROC

// Mutator for left stick x & y positions.
PROC ARM_SET_LEFT_STICK_XY(ARM_INPUT& input, FLOAT fLeftStickX, FLOAT fLeftStickY)
	ARM_SET_LEFT_STICK_X(input, fLeftStickX)
	ARM_SET_LEFT_STICK_Y(input, fLeftStickY)
ENDPROC

// Accessor for right stick x position.
FUNC FLOAT ARM_GET_RIGHT_STICK_X(ARM_INPUT& input)
	RETURN input.fRightStickX
ENDFUNC

// Mutator for right stick x position.
PROC ARM_SET_RIGHT_STICK_X(ARM_INPUT& input, FLOAT fRightStickX)
	input.fRightStickX = fRightStickX
ENDPROC

// Accessor for right stick y position.
FUNC FLOAT ARM_GET_RIGHT_STICK_Y(ARM_INPUT& input)
	RETURN input.fRightStickY
ENDFUNC

// Mutator for right stick y position.
PROC ARM_SET_RIGHT_STICK_Y(ARM_INPUT& input, FLOAT fRightStickY)
	input.fRightStickY = fRightStickY
ENDPROC

// Mutator for left stick x & y positions.
PROC ARM_SET_RIGHT_STICK_XY(ARM_INPUT& input, FLOAT fRightStickX, FLOAT fRightStickY)
	ARM_SET_RIGHT_STICK_X(input, fRightStickX)
	ARM_SET_RIGHT_STICK_Y(input, fRightStickY)
ENDPROC

// Accessor for left stick position, returned as a vector.
FUNC VECTOR ARM_GET_LEFT_STICK_AS_VECTOR(ARM_INPUT& input)
	RETURN <<input.fLeftStickX, input.fLeftStickY, 0.0>>
ENDFUNC

// Mutator for left stick using a vector as an argument.
PROC ARM_SET_LEFT_STICK_AS_VECTOR(ARM_INPUT& input, VECTOR vLeftStick)
	input.fLeftStickX = vLeftStick.x
	input.fLeftStickY = vLeftStick.y
ENDPROC

// Accessor for left stick position, returned as a vector.
FUNC VECTOR ARM_GET_RIGHT_STICK_AS_VECTOR(ARM_INPUT& input)
	RETURN <<input.fRightStickX, input.fRightStickY, 0.0>>
ENDFUNC

// Mutator for left stick using a vector as an argument.
PROC ARM_SET_RIGHT_STICK_AS_VECTOR(ARM_INPUT& input, VECTOR vRightStick)
	input.fRightStickX = vRightStick.x
	input.fRightStickY = vRightStick.y
ENDPROC

// Copy current frame's stick data to last frame and clear.
PROC ARM_STORE_LAST_STICKS(ARM_INPUT& input)
	input.fLastLeftStickX = input.fLeftStickX
	input.fLastLeftStickY = input.fLeftStickY
	input.fLastRightStickX = input.fRightStickX
	input.fLastRightStickY = input.fRightStickY
	input.fLeftStickX = 0.0
	input.fLeftStickY = 0.0
	input.fRightStickX = 0.0
	input.fRightStickY = 0.0
ENDPROC

// Check whether a specific button is down this frame.
FUNC BOOL ARM_IS_CONTROL_PRESSED(ARM_INPUT& input, ARM_BUTTONS button)
	RETURN IS_BIT_SET(input.iButtons, ENUM_TO_INT(button))
ENDFUNC

// Check whether a specific button was pressed this frame (buffered).
FUNC BOOL ARM_IS_CONTROL_JUST_PRESSED(ARM_INPUT& input, ARM_BUTTONS button)
	RETURN IS_BIT_SET(input.iButtons, ENUM_TO_INT(button)) AND NOT IS_BIT_SET(input.iLastButtons, ENUM_TO_INT(button))
ENDFUNC

// Mutator for whether a specific button is down this frame.
PROC ARM_SET_BUTTON_PRESSED(ARM_INPUT& input, ARM_BUTTONS button, BOOL bPressed)
	IF bPressed
		SET_BIT(input.iButtons, ENUM_TO_INT(button))
	ELSE
		CLEAR_BIT(input.iButtons, ENUM_TO_INT(button))
	ENDIF
ENDPROC

// Copy current frame's button data to last frame and clear.
PROC ARM_STORE_LAST_BUTTONS(ARM_INPUT& input)
	input.iLastButtons = input.iButtons
	input.iButtons = 0
ENDPROC
