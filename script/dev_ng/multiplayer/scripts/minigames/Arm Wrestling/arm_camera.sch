
//////////////////////////////////////////////////////////////////////
/* arm_camera.sch													*/
/* Author: DJ Jones													*/
/* Camera definitions for arm wrestling minigame.					*/
//////////////////////////////////////////////////////////////////////


// List of available cameras.
ENUM ARM_CAMERA_ID
	ARMCAMERAID_IDLE,
	ARMCAMERAID_IDLE_PUSH_IN,
	ARMCAMERAID_TUTORIAL,
	ARMCAMERAID_NEUTRAL,
	ARMCAMERAID_WRESTLE,
	ARMCAMERAID_WRESTLE_HAND, // 5
	ARMCAMERAID_WRESTLE_OPP_VIEW,
	ARMCAMERAID_WRESTLE_OPP_VIEW_HAND,
	ARMCAMERAID_WRESTLE_WINNER_START,
	ARMCAMERAID_WRESTLE_WINNER_END,
	ARMCAMERAID_FRONT_FACE_START,
	ARMCAMERAID_FRONT_FACE_END,
	ARMCAMERAID_BACK_FACE_START,
	ARMCAMERAID_BACK_FACE_END,
	ARMCAMERAIDS
ENDENUM

ENUM ARM_WIN_CYCLES
	ARMWIN_IDLE_START,
	ARMWIN_IDLE_END,
	ARMWIN_FRONT_FACE_START,
	ARMWIN_FRONT_FACE_END,
	ARMWIN_BACK_FACE_START,
	ARMWIN_BACK_FACE_END
ENDENUM

// Set of all available cameras.
STRUCT ARM_CAMERA_SET
	CAMERA_INDEX cameras[ARMCAMERAIDS]
	structTimer camTimer
	structTimer shakeTimer
	structTimer switchTimer
	ARM_WIN_CYCLES armWinCamState = ARMWIN_IDLE_START
	ARM_CAMERA_ID activeCam
ENDSTRUCT


// Accessor for camera index.
FUNC CAMERA_INDEX ARM_GET_CAMERA_OBJECT(ARM_CAMERA_SET& cameraSet, ARM_CAMERA_ID camID)
	IF camID >= ARMCAMERAIDS
		SCRIPT_ASSERT("Passed an invalid camera ID to ARM_GET_CAMERA_OBJECT!")
		RETURN NULL
	ENDIF
	
	RETURN cameraSet.cameras[camID]
ENDFUNC

// Accessor for active camera index.
FUNC CAMERA_INDEX ARM_GET_ACTIVE_CAMERA_OBJECT(ARM_CAMERA_SET& cameraSet)
	IF cameraSet.activeCam = ARMCAMERAIDS
		SCRIPT_ASSERT("No active camera for ARM_GET_ACTIVE_CAMERA_OBJECT!")
		RETURN NULL
	ENDIF
	
	RETURN cameraSet.cameras[cameraSet.activeCam]
ENDFUNC

// Mutator for camera index.
PROC ARM_SET_CAMERA_OBJECT(ARM_CAMERA_SET& cameraSet, ARM_CAMERA_ID camID, CAMERA_INDEX camIndex, BOOL bCheckValid = TRUE)
	IF camID >= ARMCAMERAIDS
		SCRIPT_ASSERT("Passed an invalid camera ID to ARM_SET_CAMERA_OBJECT!")
		EXIT
	ENDIF
	
	IF bCheckValid AND NOT DOES_CAM_EXIST(camIndex)
		SCRIPT_ASSERT("Tried to assign nonexistent entity to camera! NOT GONNA HAPPEN!")
		EXIT
	ENDIF
	
	cameraSet.cameras[camID] = camIndex
ENDPROC

// Accessor for current active camera.
FUNC ARM_CAMERA_ID ARM_GET_ACTIVE_CAMERA(ARM_CAMERA_SET& cameraSet)
	RETURN cameraSet.activeCam
ENDFUNC

// Test whether a specific cam is currently active.
FUNC BOOL ARM_IS_CAMERA_ACTIVE(ARM_CAMERA_SET& cameraSet, ARM_CAMERA_ID activeCam)
	RETURN cameraSet.activeCam = activeCam
ENDFUNC

// Mutator for current active camera.
PROC ARM_SET_ACTIVE_CAMERA(ARM_CAMERA_SET& cameraSet, ARM_CAMERA_ID activeCam, BOOL bTurnOn = FALSE, BOOL bInterp = FALSE, ARM_CAMERA_ID interpCam = ARMCAMERAID_NEUTRAL, INT iInterpTime = 5000)
	IF activeCam >= ARMCAMERAIDS
		SCRIPT_ASSERT("Passed an invalid camera ID to ARM_SET_ACTIVE_CAMERA!")
		EXIT
	ENDIF
	
	cameraSet.activeCam = activeCam
	IF DOES_CAM_EXIST(cameraSet.cameras[cameraSet.activeCam])
		IF NOT bInterp
			SET_CAM_ACTIVE(cameraSet.cameras[cameraSet.activeCam], bTurnOn)
		ELSE
			SET_CAM_ACTIVE_WITH_INTERP(cameraSet.cameras[cameraSet.activeCam], cameraSet.cameras[interpCam], iInterpTime)
		ENDIF
	ENDIF
ENDPROC

PROC ARM_TOGGLE_CAMERA_POINTING(ARM_CAMERA_SET& cameraSet, BOOL bTurnOn = FALSE)
	VECTOR vTemp
	
	IF bTurnOn
		POINT_CAM_AT_PED_BONE(ARM_GET_CAMERA_OBJECT(cameraSet, ARMCAMERAID_WRESTLE), PLAYER_PED_ID(), BONETAG_R_HAND, <<0.0, 0.0, 0.0>>, TRUE)
	ELSE
		vTemp = GET_CAM_ROT(cameraSet.cameras[ARMCAMERAID_WRESTLE])
		STOP_CAM_POINTING(cameraSet.cameras[ARMCAMERAID_WRESTLE])
		SET_CAM_ROT(cameraSet.cameras[ARMCAMERAID_WRESTLE], vTemp)
	ENDIF

ENDPROC
