
//////////////////////////////////////////////////////////////////////
/* arm_client.sch													*/
/* Author: DJ Jones													*/
/* Client-side multiplayer definitions for arm wrestling minigame.	*/
//////////////////////////////////////////////////////////////////////


ENUM ARM_CLIENT_FLAGS
	ARMCLIENTFLAG_MATCH_MADE,
	ARMCLIENTFLAG_SYNC_COMPLETE,
	ARMCLIENTFLAG_WIN_LOSS_REACT,
	ARMCLIENTFLAG_REQUEST_ANIM,
	ARMCLIENTFLAG_START_ANIM,
	ARMCLIENTFLAG_FACE_ANIM_STARTED,
	ARMCLIENTFLAG_WINNER_SET,
	ARMCLIENTFLAG_TUTORIAL_SKIP,
	ARMCLIENTFLAG_TUTORIAL_DONE,
	ARMCLIENTFLAG_AT_TUTORIAL,
	ARMCLIENTFLAG_POST_ANIM_DONE,
	ARMCLIENTFLAG_NOT_MOVING_STICKS,
	ARMCLIENTFLAG_QUIT_DONE,
	
	//Arpartments
	ARMCLIENTFLAG_SPECTATOR,
	ARMCLIENTFLAG_SCORE_UPDATED,
	ARMCLIENTFLAG_ANIMPLAY_READY,
	ARMCLIENTFLAG_IN_PLAY_POSITION,
	ARMCLIENTFLAG_POSITION_RECEIVED,
	ARMCLIENTFLAG_CLEANUP_DONE,
	ARMCLIENTFLAG_CLEANUP_STARTED, 
	ARMCLIENTFLAG_VALID_PLAYER,
	ARMCLIENTFLAG_TABLE_SIDE_A,
	ARMCLIENTFLAG_HIDE_OPPONENT,
	ARMCLIENTFLAG_ATTACHED,
	ARMCLIENTFLAG_REACHED_RUNNING,
	ARMCLIENTFLAG_RESET_COUNTDOWN_UI, 
	ARMCLIENTFLAG_HIDE_SPECTATORS
ENDENUM


// Broadcast Data. Client can write SELF, and read all others.
STRUCT ARM_PLAYER_BROADCAST_DATA
	ARM_MP_STATE eMPState
	PLAYER_INDEX curPlayerID
	//TEXT_LABEL_63 sName
	FLOAT fScoreBuckets[iSCORE_BUCKETS]
	FLOAT fReadyTime
	FLOAT fLastUsedScore
	FLOAT fStrengthFactor
	INT iClientFlags
	INT iCurrentBucket
	FLOAT fClientScore
	FLOAT fCurrentPhase
	INT iCurrentMoveState
	BOOL bIsMovNetActive
	BOOL bIsMovNetTransReady
ENDSTRUCT


// Accessor for the player's MP state.
FUNC ARM_MP_STATE ARM_GET_PLAYER_MP_STATE(ARM_PLAYER_BROADCAST_DATA& playerData)
	RETURN playerData.eMPState
ENDFUNC

// String accessor for the multiplayer state, useful for logs
FUNC STRING ARM_GET_SERVER_MP_STATE_STRING(ARM_MP_STATE eMPState)
	SWITCH eMPState
		CASE ARMMPSTATE_INVALID
			RETURN "ARMMPSTATE_INVALID"
		CASE ARMMPSTATE_INIT
			RETURN "ARMMPSTATE_INIT"
		CASE ARMMPSTATE_TRAVEL
			RETURN "ARMMPSTATE_TRAVEL"
		CASE ARMMPSTATE_SETUP
			RETURN "ARMMPSTATE_SETUP"
		CASE ARMMPSTATE_SETUP_POST
			RETURN "ARMMPSTATE_SETUP_POST"
		CASE ARMMPSTATE_WAIT_TO_START_GAME
			RETURN "ARMMPSTATE_WAIT_TO_START_GAME"
		CASE ARMMPSTATE_INTRO_SCENE_ALT
			RETURN "ARMMPSTATE_INTRO_SCENE_ALT"
		CASE ARMMPSTATE_INTRO_SCENE_ALT_END
			RETURN "ARMMPSTATE_INTRO_SCENE_ALT_END"
		CASE ARMMPSTATE_RUNNING
			RETURN "ARMMPSTATE_RUNNING"
		CASE ARMMPSTATE_WAIT_POST_GAME
			RETURN "ARMMPSTATE_WAIT_POST_GAME"
		CASE ARMMPSTATE_TERMINATE_SPLASH
			RETURN "ARMMPSTATE_TERMINATE_SPLASH"
		CASE ARMMPSTATE_VICTORY_AUDIO
			RETURN "ARMMPSTATE_VICTORY_AUDIO"
		CASE ARMMPSTATE_END_LEADERBOARD
			RETURN "ARMMPSTATE_END_LEADERBOARD"
		DEFAULT
			RETURN "INVALID"
	ENDSWITCH 
ENDFUNC

// Mutator for the player's MP state.
PROC ARM_SET_PLAYER_MP_STATE(ARM_PLAYER_BROADCAST_DATA& playerData, ARM_MP_STATE eMPState)
	CDEBUG1LN(DEBUG_ARM_WRESTLING, " ")
	CDEBUG1LN(DEBUG_ARM_WRESTLING, " ")
	CDEBUG1LN(DEBUG_ARM_WRESTLING, "##############################")
	CDEBUG1LN(DEBUG_ARM_WRESTLING, "am_armwrestling: CLIENT MP STATE CHANGED:Setting local client mp state: ", ARM_GET_SERVER_MP_STATE_STRING(eMPState))//ENUM_TO_INT(eMPState))
	CDEBUG1LN(DEBUG_ARM_WRESTLING, "FROM: ", ARM_GET_SERVER_MP_STATE_STRING(playerData.eMPState), " TO: ", ARM_GET_SERVER_MP_STATE_STRING(eMPState))
	CDEBUG1LN(DEBUG_ARM_WRESTLING, "VVVVVVVVVVVVVVVVVVVVVVVVVVVVVV")
	playerData.eMPState = eMPState
ENDPROC

// Accessor for the player's index.
FUNC PLAYER_INDEX ARM_GET_PLAYER_ID(ARM_PLAYER_BROADCAST_DATA& playerData)
	RETURN playerData.curPlayerID
ENDFUNC

// Mutator for the player's index.
PROC ARM_SET_PLAYER_ID(ARM_PLAYER_BROADCAST_DATA& playerData, PLAYER_INDEX piID)
	playerData.curPlayerID = piID
ENDPROC

//// Get the player's network name (i.e. gamertag).
FUNC TEXT_LABEL_63 ARM_GET_PLAYER_NAME(ARM_PLAYER_BROADCAST_DATA& playerData)
	TEXT_LABEL_63 name = GET_PLAYER_NAME(ARM_GET_PLAYER_ID(playerData))
	RETURN name
ENDFUNC

//// Set the player's network name (i.e. gamertag).
//PROC ARM_SET_PLAYER_NAME(ARM_PLAYER_BROADCAST_DATA& playerData, STRING sName)
//	playerData.sName = sName
//ENDPROC

// Accessor for ready time remaining.
FUNC FLOAT ARM_GET_PLAYER_READY_TIME(ARM_PLAYER_BROADCAST_DATA& playerData)
	RETURN playerData.fReadyTime
ENDFUNC

// Mutator for ready time remaining.
PROC ARM_SET_PLAYER_READY_TIME(ARM_PLAYER_BROADCAST_DATA& playerData, FLOAT fReadyTime)
	playerData.fReadyTime = fReadyTime
ENDPROC

// Gets a flag in the client BD.
FUNC BOOL ARM_GET_CLIENT_FLAG(ARM_PLAYER_BROADCAST_DATA& playerData, ARM_CLIENT_FLAGS eFlag)
	RETURN IS_BIT_SET(playerData.iClientFlags, ENUM_TO_INT(eFlag))
ENDFUNC

// Sets a flag in the client BD.
PROC ARM_SET_CLIENT_FLAG(ARM_PLAYER_BROADCAST_DATA& playerData, ARM_CLIENT_FLAGS eFlag, BOOL bState)
	CDEBUG1LN(DEBUG_ARM_WRESTLING, "ARM_SET_CLIENT_FLAG - eFlag: ", ENUM_TO_INT(eFlag), " newState: ", bState)
	IF bState
		SET_BIT(playerData.iClientFlags, ENUM_TO_INT(eFlag))
	ELSE
		CLEAR_BIT(playerData.iClientFlags, ENUM_TO_INT(eFlag))
	ENDIF
ENDPROC

// Accessor for force in a particular bucket.
FUNC FLOAT ARM_GET_SCORE_BUCKET(ARM_PLAYER_BROADCAST_DATA& playerData, INT iBucket)
	IF iBucket < 0 OR iBucket >= iSCORE_BUCKETS
		RETURN 0.0
	ENDIF
	
	RETURN playerData.fScoreBuckets[iBucket]
ENDFUNC

// Mutator for force in a particular bucket.
PROC ARM_SET_SCORE_BUCKET(ARM_PLAYER_BROADCAST_DATA& playerData, INT iBucket, FLOAT fScore)
	IF iBucket < 0 OR iBucket >= iSCORE_BUCKETS
		EXIT
	ENDIF
	
	playerData.fScoreBuckets[iBucket] = fScore
ENDPROC

// Add a score to a bucket.
PROC ARM_ADD_SCORE_TO_BUCKET(ARM_PLAYER_BROADCAST_DATA& playerData, INT iBucket, FLOAT fScore)
	IF iBucket < 0 OR iBucket >= iSCORE_BUCKETS
		EXIT
	ENDIF
	
	playerData.fScoreBuckets[iBucket] += fScore
ENDPROC

// Accessor for current bucket.
FUNC INT ARM_GET_CURRENT_BUCKET(ARM_PLAYER_BROADCAST_DATA& playerData)
	RETURN playerData.iCurrentBucket
ENDFUNC

// Mutator for current bucket.
PROC ARM_SET_CURRENT_BUCKET(ARM_PLAYER_BROADCAST_DATA& playerData, INT iCurrentBucket)
	IF iCurrentBucket < 0 OR iCurrentBucket >= iSCORE_BUCKETS
		EXIT
	ENDIF
	
	playerData.iCurrentBucket = iCurrentBucket
ENDPROC

// Go to the next bucket (loops around).
PROC ARM_NEXT_BUCKET(ARM_PLAYER_BROADCAST_DATA& playerData, BOOL bClearNewBucket = TRUE)
	playerData.iCurrentBucket += 1
	IF playerData.iCurrentBucket >= iSCORE_BUCKETS
		playerData.iCurrentBucket = 0
	ENDIF
	
	IF bClearNewBucket
		playerData.fScoreBuckets[playerData.iCurrentBucket] = 0.0
	ENDIF
ENDPROC

PROC ARM_CLEAR_BUCKETS(ARM_PLAYER_BROADCAST_DATA& playerData)
	INT iCounter
	REPEAT iSCORE_BUCKETS iCounter
		playerData.fScoreBuckets[iCounter] = 0.0
	ENDREPEAT
	playerData.iCurrentBucket = 0
	MPGlobals.WrestleData.iBucketClearBits = 0
ENDPROC

