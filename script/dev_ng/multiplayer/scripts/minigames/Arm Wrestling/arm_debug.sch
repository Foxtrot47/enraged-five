
//////////////////////////////////////////////////////////////////////
/* arm_debug.sch													*/
/* Author: DJ Jones													*/
/* Debug output & drawing for arm wrestling minigame.				*/
//////////////////////////////////////////////////////////////////////


// Widgets for viewing and tweaking values.
STRUCT ARM_WIDGETS
	// Widgets
	WIDGET_GROUP_ID parentGroup
	WIDGET_GROUP_ID gameGroup
	WIDGET_GROUP_ID wrestlerGroup
	WIDGET_GROUP_ID crowdGroup
	WIDGET_GROUP_ID cameraGroup
		WIDGET_GROUP_ID wrestleCamGroup
	WIDGET_GROUP_ID uiGroup
	WIDGET_GROUP_ID readyData
	WIDGET_GROUP_ID countdownData
	WIDGET_GROUP_ID terminateData
	WIDGET_GROUP_ID debugDraw
	WIDGET_GROUP_ID lerpGroup
	
	// Lerp Debug
	FLOAT fPhaseLerpRate
	FLOAT fScoreLerpRate
	
	// Game data
	FLOAT fScore
	INT iClientMPState
	INT iServerMPState
	INT iGameState
	
	// Wrestler data
	FLOAT fWrestlerOffsetX
	FLOAT fWrestlerOffsetY
	FLOAT fWrestlerOffsetZ
	
	// Referee data
	FLOAT fRefereeOffsetX
	FLOAT fRefereeOffsetY
	FLOAT fRefereeOffsetZ
	FLOAT fRefereeHeading
	
	// Fan data
	FLOAT fAwayFan1OffsetX
	FLOAT fAwayFan1OffsetY
	FLOAT fAwayFan2OffsetX
	FLOAT fAwayFan2OffsetY
	FLOAT fAwayFan3OffsetX
	FLOAT fAwayFan3OffsetY
	FLOAT fHomeFan1OffsetX
	FLOAT fHomeFan1OffsetY
	FLOAT fHomeFan2OffsetX
	FLOAT fHomeFan2OffsetY
	FLOAT fHomeFan3OffsetX
	FLOAT fHomeFan3OffsetY
	
	// Wrestle cam data
	FLOAT fWrestleCamX
	FLOAT fWrestleCamY
	FLOAT fWrestleCamZ
	FLOAT fWrestleLookX
	FLOAT fWrestleLookY
	FLOAT fWrestleLookZ
	FLOAT fWrestleFOV
	
	// Debug data
	BOOL bDebugDraw
	BOOL bDebugDrawLast
ENDSTRUCT


ARM_WIDGETS armW


// Create widgets for debugging purposes.
PROC ARM_SETUP_DEBUG()
	// Initialize wrestler values.
	armW.fWrestlerOffsetX = fWRESTLER_OFFSET_X
	IF ARM_IS_PLAYER_LARGE()
		armW.fWrestlerOffsetY = fWRESTLER_OFFSET_LY
		armW.fWrestlerOffsetZ = fWRESTLER_OFFSET_LZ
	ELSE
		armW.fWrestlerOffsetY = fWRESTLER_OFFSET_SY
		armW.fWrestlerOffsetZ = fWRESTLER_OFFSET_SZ
	ENDIF
	
	// Initialize referee values.
	armW.fRefereeOffsetX = fREFEREE_OFFSET_X
	armW.fRefereeOffsetY = fREFEREE_OFFSET_Y
	armW.fRefereeOffsetZ = fREFEREE_OFFSET_Z
	armW.fRefereeHeading = fREFEREE_HEADING
	
	// Initialize fan values.
	armW.fAwayFan1OffsetX = fAWAYFAN1_OFFSET_X
	armW.fAwayFan1OffsetY = fAWAYFAN1_OFFSET_Y
	armW.fAwayFan2OffsetX = fAWAYFAN2_OFFSET_X
	armW.fAwayFan2OffsetY = fAWAYFAN2_OFFSET_Y
	armW.fAwayFan3OffsetX = fAWAYFAN3_OFFSET_X
	armW.fAwayFan3OffsetY = fAWAYFAN3_OFFSET_Y
	armW.fHomeFan1OffsetX = fHOMEFAN1_OFFSET_X
	armW.fHomeFan1OffsetY = fHOMEFAN1_OFFSET_Y
	armW.fHomeFan2OffsetX = fHOMEFAN2_OFFSET_X
	armW.fHomeFan2OffsetY = fHOMEFAN2_OFFSET_Y
	armW.fHomeFan3OffsetX = fHOMEFAN3_OFFSET_X
	armW.fHomeFan3OffsetY = fHOMEFAN3_OFFSET_Y
	
	// Initialize cached camera values.
	armW.fWrestleCamX  = fCAM_WRESTLE_X
	armW.fWrestleCamY  = fCAM_WRESTLE_Y
	armW.fWrestleCamZ  = fCAM_WRESTLE_Z
	armW.fWrestleLookX = fCAM_WRESTLE_LX
	armW.fWrestleLookY = fCAM_WRESTLE_LY
	armW.fWrestleLookZ = fCAM_WRESTLE_LZ
	armW.fWrestleFOV   = fCAM_WRESTLE_FOV
	
	// The main widget group.
	IF DOES_WIDGET_GROUP_EXIST(armW.parentGroup)
		SET_CURRENT_WIDGET_GROUP(armW.parentGroup)
		
		armW.parentGroup = START_WIDGET_GROUP("Arm Wrestling")
			// Game controls.
			armW.uiGroup = START_WIDGET_GROUP("Game")
				ADD_WIDGET_INT_READ_ONLY("Client MP State", armW.iClientMPState)
				ADD_WIDGET_INT_READ_ONLY("Server MP State", armW.iServerMPState)
				ADD_WIDGET_INT_READ_ONLY("Game State", armW.iGameState)
				ADD_WIDGET_FLOAT_READ_ONLY("Score", armW.fScore)
				ADD_WIDGET_FLOAT_SLIDER("Score Change Scalar", fSCORE_CHANGE_SCALAR, 0.0, 10.0, 0.1)
				ADD_WIDGET_FLOAT_SLIDER("Score Update Time", fSCORE_UPDATE_TIME, 0.0, 10.0, 0.1)
				ADD_WIDGET_FLOAT_SLIDER("Bucket Switch Time", fBUCKET_SWITCH_TIME, 0.0, 10.0, 0.1)
			STOP_WIDGET_GROUP()
			
			// Wrestler controls.
			armW.wrestlerGroup = START_WIDGET_GROUP("Wrestlers")
				ADD_WIDGET_FLOAT_SLIDER("Offset X", fWRESTLER_OFFSET_X, -5.0, 5.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Offset Y (Small)", fWRESTLER_OFFSET_SY, -5.0, 5.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Offset Y (Large)", fWRESTLER_OFFSET_LY, -5.0, 5.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Offset Z (Small)", fWRESTLER_OFFSET_SZ, -5.0, 5.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Offset Z (Large)", fWRESTLER_OFFSET_LZ, -5.0, 5.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Arm Ramp Dampening", fARM_RAMP_DAMP, 0.0, 1.0, 0.01)
			STOP_WIDGET_GROUP()
			
			// Crowd controls.
			armW.crowdGroup = START_WIDGET_GROUP("Crowd")
				ADD_WIDGET_FLOAT_SLIDER("Referee Offset X", fREFEREE_OFFSET_X, -5.0, 5.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Referee Offset Y", fREFEREE_OFFSET_Y, -5.0, 5.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Referee Offset Z", fREFEREE_OFFSET_Z, -5.0, 5.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Referee Heading", fREFEREE_HEADING, 0.0, 360.0, 0.1)
				ADD_WIDGET_FLOAT_SLIDER("Away Fan 1 Offset X", fAWAYFAN1_OFFSET_X, -8.0, 8.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Away Fan 1 Offset Y", fAWAYFAN1_OFFSET_Y, -8.0, 8.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Away Fan 2 Offset X", fAWAYFAN2_OFFSET_X, -8.0, 8.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Away Fan 2 Offset Y", fAWAYFAN2_OFFSET_Y, -8.0, 8.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Away Fan 3 Offset X", fAWAYFAN3_OFFSET_X, -8.0, 8.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Away Fan 3 Offset Y", fAWAYFAN3_OFFSET_Y, -8.0, 8.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Home Fan 1 Offset X", fHOMEFAN1_OFFSET_X, -8.0, 8.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Home Fan 1 Offset Y", fHOMEFAN1_OFFSET_Y, -8.0, 8.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Home Fan 2 Offset X", fHOMEFAN2_OFFSET_X, -8.0, 8.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Home Fan 2 Offset Y", fHOMEFAN2_OFFSET_Y, -8.0, 8.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Home Fan 3 Offset X", fHOMEFAN3_OFFSET_X, -8.0, 8.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Home Fan 3 Offset Y", fHOMEFAN3_OFFSET_Y, -8.0, 8.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Fan Offset Z", fFAN_OFFSET_Z, -8.0, 8.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Referee Intro Speed", fREF_INTRO_SPEED, 0.0, 1.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Referee Outro Speed", fREF_OUTRO_SPEED, 0.0, 1.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Crowd Taunt Delay", fCROWD_TAUNT_DELAY, 0.0, 100.0, 0.1)
			STOP_WIDGET_GROUP()
			
			// Camera controls.
			armW.debugDraw = START_WIDGET_GROUP("Cameras")
				ADD_WIDGET_FLOAT_SLIDER("Camera Switch Threshold", fCAM_SWITCH_THRESHOLD, -1.0, 1.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Camera Return Threshold", fCAM_RETURN_THRESHOLD, -1.0, 1.0, 0.01)
				
				// Wrestle cam controls.
				armW.debugDraw = START_WIDGET_GROUP("Wrestle Camera")
					ADD_WIDGET_FLOAT_SLIDER("Wrestle Cam Pos X", fCAM_WRESTLE_X, -10.0, 10.0, 0.01)
					ADD_WIDGET_FLOAT_SLIDER("Wrestle Cam Pos Y", fCAM_WRESTLE_Y, -10.0, 10.0, 0.01)
					ADD_WIDGET_FLOAT_SLIDER("Wrestle Cam Pos Z", fCAM_WRESTLE_Z, -10.0, 10.0, 0.01)
					ADD_WIDGET_FLOAT_SLIDER("Wrestle Cam Look X", fCAM_WRESTLE_LX, -10.0, 10.0, 0.01)
					ADD_WIDGET_FLOAT_SLIDER("Wrestle Cam Look Y", fCAM_WRESTLE_LY, -10.0, 10.0, 0.01)
					ADD_WIDGET_FLOAT_SLIDER("Wrestle Cam Look Z", fCAM_WRESTLE_LZ, -10.0, 10.0, 0.01)
					ADD_WIDGET_FLOAT_SLIDER("Wrestle Cam FOV", fCAM_WRESTLE_FOV, 10.0, 100.0, 1.0)
				STOP_WIDGET_GROUP()
			STOP_WIDGET_GROUP()
			
			// UI controls.
			armW.uiGroup = START_WIDGET_GROUP("UI")
				ADD_WIDGET_FLOAT_SLIDER("Ready Menu Delay", fREADY_DELAY, 0.0, 10.0, 0.1)
				ADD_WIDGET_FLOAT_SLIDER("Ready Menu Timeout", fREADY_TIMEOUT, 0.0, 30.0, 0.1)
				ADD_WIDGET_FLOAT_SLIDER("Rematch Menu Delay", fREMATCH_DELAY, 0.0, 10.0, 0.1)
				ADD_WIDGET_FLOAT_SLIDER("Terminate Screen Timeout", fTERMINATE_TIMEOUT, 0.0, 30.0, 0.1)
				ADD_WIDGET_FLOAT_SLIDER("Meter Dampening", fMETER_DAMP, 0.0, 1.0, 0.01)
				ADD_WIDGET_INT_SLIDER("Fade Out Time (ms)", iFADE_OUT_TIME, 0, 5000, 10)
				ADD_WIDGET_INT_SLIDER("Fade In Time (ms)", iFADE_IN_TIME, 0, 5000, 10)
			STOP_WIDGET_GROUP()
			
			// Data related to ready screen.
			armW.readyData = START_WIDGET_GROUP("Ready Data")
				ADD_WIDGET_FLOAT_SLIDER("Ready Top Left X", fREADY_TL_X, 0.0, 1.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Ready Top Left Y", fREADY_TL_Y, 0.0, 1.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Ready Width", fREADY_WIDTH, 0.0, 1.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Ready Height", fREADY_HEIGHT, 0.0, 1.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Player Column Label X", fREADY_LBL_PLAYERS_X, 0.0, 1.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Status Column Label X", fREADY_LBL_STATUS_X, 0.0, 1.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Column Label Y", fREADY_LABEL_Y, 0.0, 1.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Column Label Scale", fREADY_LABEL_SCALE, 0.0, 10.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Player Column Entry X", fREADY_NAME_X, 0.0, 1.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Status Column Entry X", fREADY_STATUS_X, 0.0, 1.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Local Player Entry Y", fREADY_ENTRY_LCL_Y, 0.0, 1.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Remote Player Entry Y", fREADY_ENTRY_RMT_Y, 0.0, 1.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Entry Scale", fREADY_ENTRY_SCALE, 0.0, 10.0, 0.01)
				ADD_WIDGET_INT_SLIDER("Ready Red", iREADY_RED, 0, 255, 1)
				ADD_WIDGET_INT_SLIDER("Ready Green", iREADY_GREEN, 0, 255, 1)
				ADD_WIDGET_INT_SLIDER("Ready Blue", iREADY_BLUE, 0, 255, 1)
				ADD_WIDGET_INT_SLIDER("Ready Alpha", iREADY_ALPHA, 0, 255, 1)
			STOP_WIDGET_GROUP()
			
			// Data related to countdown screen.
			armW.terminateData = START_WIDGET_GROUP("Countdown Data")
				ADD_WIDGET_FLOAT_SLIDER("Countdown Timer X", fCOUNTDOWN_X, 0.0, 1.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Countdown Timer Y", fCOUNTDOWN_Y, 0.0, 1.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Countdown Timer Scale", fCOUNTDOWN_SCALE, 0.0, 10.0, 0.01)
			STOP_WIDGET_GROUP()
			
			// Data related to terminate screen.
			armW.terminateData = START_WIDGET_GROUP("Terminate Data")
				ADD_WIDGET_FLOAT_SLIDER("Terminate Top Left X", fTERMINATE_TL_X, 0.0, 1.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Terminate Top Left Y", fTERMINATE_TL_Y, 0.0, 1.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Terminate Width", fTERMINATE_WIDTH, 0.0, 1.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Terminate Height", fTERMINATE_HEIGHT, 0.0, 1.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Terminate Wrap Bound Left", fTERMINATE_WRAP_L, 0.0, 1.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Terminate Wrap Bound Right", fTERMINATE_WRAP_R, 0.0, 1.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Terminate Text Y", fTERMINATE_TEXT_Y, 0.0, 1.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Terminate Text Scale", fTERMINATE_TEXT_SCALE, 0.0, 10.0, 0.01)
				ADD_WIDGET_INT_SLIDER("Terminate Red", iTERMINATE_RED, 0, 255, 1)
				ADD_WIDGET_INT_SLIDER("Terminate Green", iTERMINATE_GREEN, 0, 255, 1)
				ADD_WIDGET_INT_SLIDER("Terminate Blue", iTERMINATE_BLUE, 0, 255, 1)
				ADD_WIDGET_INT_SLIDER("Terminate Alpha", iTERMINATE_ALPHA, 0, 255, 1)
			STOP_WIDGET_GROUP()
			
			// Debug draw controls.
			armW.debugDraw = START_WIDGET_GROUP("Debug Draw")
				ADD_WIDGET_BOOL("Enable Debug Draw", armW.bDebugDraw)
			STOP_WIDGET_GROUP()
			
			armW.lerpGroup = START_WIDGET_GROUP("Debug Lerp")
				ADD_WIDGET_FLOAT_SLIDER("Score Lerp", fLerpScore, 0.0, 1.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Phase Lerp", fLerpPhase, 0.0, 1.0, 0.01)
			STOP_WIDGET_GROUP()
			
		STOP_WIDGET_GROUP()
	ENDIF
ENDPROC

// Delete all existing arm wrestling widgets.
PROC ARM_CLEANUP_WIDGETS()
	IF DOES_WIDGET_GROUP_EXIST(armW.lerpGroup)
		DELETE_WIDGET_GROUP(armW.lerpGroup)
	ENDIF
	IF DOES_WIDGET_GROUP_EXIST(armW.debugDraw)
		DELETE_WIDGET_GROUP(armW.debugDraw)
	ENDIF
	IF DOES_WIDGET_GROUP_EXIST(armW.terminateData)
		DELETE_WIDGET_GROUP(armW.terminateData)
	ENDIF
	IF DOES_WIDGET_GROUP_EXIST(armW.countdownData)
		DELETE_WIDGET_GROUP(armW.countdownData)
	ENDIF
	IF DOES_WIDGET_GROUP_EXIST(armW.readyData)
		DELETE_WIDGET_GROUP(armW.readyData)
	ENDIF
	IF DOES_WIDGET_GROUP_EXIST(armW.uiGroup)
		DELETE_WIDGET_GROUP(armW.uiGroup)
	ENDIF
	IF DOES_WIDGET_GROUP_EXIST(armW.wrestleCamGroup)
		DELETE_WIDGET_GROUP(armW.wrestleCamGroup)
	ENDIF
	IF DOES_WIDGET_GROUP_EXIST(armW.cameraGroup)
		DELETE_WIDGET_GROUP(armW.cameraGroup)
	ENDIF
	IF DOES_WIDGET_GROUP_EXIST(armW.crowdGroup)
		DELETE_WIDGET_GROUP(armW.crowdGroup)
	ENDIF
	IF DOES_WIDGET_GROUP_EXIST(armW.wrestlerGroup)
		DELETE_WIDGET_GROUP(armW.wrestlerGroup)
	ENDIF
	IF DOES_WIDGET_GROUP_EXIST(armW.gameGroup)
		DELETE_WIDGET_GROUP(armW.gameGroup)
	ENDIF
	IF DOES_WIDGET_GROUP_EXIST(armW.parentGroup)
		DELETE_WIDGET_GROUP(armW.parentGroup)
	ENDIF
	PRINTLN("ARM_CLEANUP_WIDGETS: Bottom of ARM_CLEANUP_WIDGETS")
ENDPROC

// Update widget variable values.
PROC ARM_UPDATE_WIDGETS(ARM_SERVER_BROADCAST_DATA& serverData, ARM_TABLE& table, ARM_CAMERA_SET& cameraSet, INT iClientMPState, INT iServerMPState, INT iGameState, OBJECT_INDEX wrestlerAttachment, OBJECT_INDEX refereeAttachment, PED_INDEX& crowdPeds[], BOOL bHome)
	BOOL bDirtyPos, bDirtyLook, bDirtyFOV
	
	// Update game and AI states.
	armW.iClientMPState = iClientMPState
	armW.iServerMPState = iServerMPState
	armW.iGameState = iGameState
	armW.fScore = ARM_GET_SERVER_SCORE(serverData)
	
	// Check for new wrestler position.
	IF armW.fWrestlerOffsetX <> fWRESTLER_OFFSET_X
		armW.fWrestlerOffsetX = fWRESTLER_OFFSET_X
		bDirtyPos = TRUE
	ENDIF
	
	IF ARM_IS_PLAYER_LARGE()
		IF armW.fWrestlerOffsetY <> fWRESTLER_OFFSET_LY
			armW.fWrestlerOffsetY = fWRESTLER_OFFSET_LY
			bDirtyPos = TRUE
		ENDIF
		IF armW.fWrestlerOffsetZ <> fWRESTLER_OFFSET_LZ
			armW.fWrestlerOffsetZ = fWRESTLER_OFFSET_LZ
			bDirtyPos = TRUE
		ENDIF
	ELSE
		IF armW.fWrestlerOffsetY <> fWRESTLER_OFFSET_SY
			armW.fWrestlerOffsetY = fWRESTLER_OFFSET_SY
			bDirtyPos = TRUE
		ENDIF
		IF armW.fWrestlerOffsetZ <> fWRESTLER_OFFSET_SZ
			armW.fWrestlerOffsetZ = fWRESTLER_OFFSET_SZ
			bDirtyPos = TRUE
		ENDIF
	ENDIF
	
	IF bDirtyPos AND DOES_ENTITY_EXIST(wrestlerAttachment)
		SET_ENTITY_COORDS(wrestlerAttachment, ARM_GET_TABLE_WRESTLER_POS(table, bHome, ARM_IS_PLAYER_LARGE()))
	ENDIF
	
	bDirtyPos = FALSE
	
	// Check for new referee position.
	IF armW.fRefereeOffsetX <> fREFEREE_OFFSET_X
		armW.fRefereeOffsetX = fREFEREE_OFFSET_X
		bDirtyPos = TRUE
	ENDIF
	IF armW.fRefereeOffsetY <> fREFEREE_OFFSET_Y
		armW.fRefereeOffsetY = fREFEREE_OFFSET_Y
		bDirtyPos = TRUE
	ENDIF
	IF armW.fRefereeOffsetZ <> fREFEREE_OFFSET_Z
		armW.fRefereeOffsetZ = fREFEREE_OFFSET_Z
		bDirtyPos = TRUE
	ENDIF
	
	IF bDirtyPos AND DOES_ENTITY_EXIST(refereeAttachment)
		SET_ENTITY_COORDS(refereeAttachment, ARM_GET_TABLE_CROWD_POS(table, ARMCROWDMEMBER_REFEREE))
	ENDIF
	
	bDirtyPos = FALSE
	
	// Check for new referee heading.
	IF armW.fRefereeHeading <> fREFEREE_HEADING
		armW.fRefereeHeading = fREFEREE_HEADING
		bDirtyPos = TRUE
	ENDIF
	
	IF bDirtyPos AND DOES_ENTITY_EXIST(refereeAttachment)
		SET_ENTITY_HEADING(refereeAttachment, ARM_GET_TABLE_ROTATION(table) + armW.fRefereeHeading)
	ENDIF
	
	bDirtyPos = FALSE
	
	// Check for new away fan 1 position.
	IF armW.fAwayFan1OffsetX <> fAWAYFAN1_OFFSET_X
		armW.fAwayFan1OffsetX = fAWAYFAN1_OFFSET_X
		bDirtyPos = TRUE
	ENDIF
	IF armW.fAwayFan1OffsetY <> fAWAYFAN1_OFFSET_Y
		armW.fAwayFan1OffsetY = fAWAYFAN1_OFFSET_Y
		bDirtyPos = TRUE
	ENDIF
	
	IF bDirtyPos AND (NOT IS_ENTITY_DEAD(crowdPeds[ARMCROWDMEMBER_AWAY_FAN1]))
		SET_ENTITY_COORDS(crowdPeds[ARMCROWDMEMBER_AWAY_FAN1], ARM_GET_TABLE_CROWD_POS(table, ARMCROWDMEMBER_AWAY_FAN1))
		SET_ENTITY_HEADING(crowdPeds[ARMCROWDMEMBER_AWAY_FAN1], GET_HEADING_BETWEEN_VECTORS(ARM_GET_TABLE_CROWD_POS(table, ARMCROWDMEMBER_AWAY_FAN1), ARM_GET_TABLE_CENTER(table)))
	ENDIF
	
	bDirtyPos = FALSE

	// Check for new away fan 2 position.
	IF armW.fAwayFan2OffsetX <> fAWAYFAN2_OFFSET_X
		armW.fAwayFan2OffsetX = fAWAYFAN2_OFFSET_X
		bDirtyPos = TRUE
	ENDIF
	IF armW.fAwayFan2OffsetY <> fAWAYFAN2_OFFSET_Y
		armW.fAwayFan2OffsetY = fAWAYFAN2_OFFSET_Y
		bDirtyPos = TRUE
	ENDIF
	
	IF bDirtyPos AND (NOT IS_ENTITY_DEAD(crowdPeds[ARMCROWDMEMBER_AWAY_FAN2]))
		SET_ENTITY_COORDS(crowdPeds[ARMCROWDMEMBER_AWAY_FAN2], ARM_GET_TABLE_CROWD_POS(table, ARMCROWDMEMBER_AWAY_FAN2))
		SET_ENTITY_HEADING(crowdPeds[ARMCROWDMEMBER_AWAY_FAN2], GET_HEADING_BETWEEN_VECTORS(ARM_GET_TABLE_CROWD_POS(table, ARMCROWDMEMBER_AWAY_FAN2), ARM_GET_TABLE_CENTER(table)))
	ENDIF
	
	bDirtyPos = FALSE

	// Check for new away fan 3 position.
	IF armW.fAwayFan3OffsetX <> fAWAYFAN3_OFFSET_X
		armW.fAwayFan3OffsetX = fAWAYFAN3_OFFSET_X
		bDirtyPos = TRUE
	ENDIF
	IF armW.fAwayFan3OffsetY <> fAWAYFAN3_OFFSET_Y
		armW.fAwayFan3OffsetY = fAWAYFAN3_OFFSET_Y
		bDirtyPos = TRUE
	ENDIF
	
	IF bDirtyPos AND (NOT IS_ENTITY_DEAD(crowdPeds[ARMCROWDMEMBER_AWAY_FAN3]))
		SET_ENTITY_COORDS(crowdPeds[ARMCROWDMEMBER_AWAY_FAN3], ARM_GET_TABLE_CROWD_POS(table, ARMCROWDMEMBER_AWAY_FAN3))
		SET_ENTITY_HEADING(crowdPeds[ARMCROWDMEMBER_AWAY_FAN3], GET_HEADING_BETWEEN_VECTORS(ARM_GET_TABLE_CROWD_POS(table, ARMCROWDMEMBER_AWAY_FAN3), ARM_GET_TABLE_CENTER(table)))
	ENDIF
	
	bDirtyPos = FALSE

	// Check for new home fan 1 position.
	IF armW.fHomeFan1OffsetX <> fHOMEFAN1_OFFSET_X
		armW.fHomeFan1OffsetX = fHOMEFAN1_OFFSET_X
		bDirtyPos = TRUE
	ENDIF
	IF armW.fHomeFan1OffsetY <> fHOMEFAN1_OFFSET_Y
		armW.fHomeFan1OffsetY = fHOMEFAN1_OFFSET_Y
		bDirtyPos = TRUE
	ENDIF
	
	IF bDirtyPos AND (NOT IS_ENTITY_DEAD(crowdPeds[ARMCROWDMEMBER_HOME_FAN1]))
		SET_ENTITY_COORDS(crowdPeds[ARMCROWDMEMBER_HOME_FAN1], ARM_GET_TABLE_CROWD_POS(table, ARMCROWDMEMBER_HOME_FAN1))
		SET_ENTITY_HEADING(crowdPeds[ARMCROWDMEMBER_HOME_FAN1], GET_HEADING_BETWEEN_VECTORS(ARM_GET_TABLE_CROWD_POS(table, ARMCROWDMEMBER_HOME_FAN1), ARM_GET_TABLE_CENTER(table)))
	ENDIF
	
	bDirtyPos = FALSE

	// Check for new home fan 2 position.
	IF armW.fHomeFan2OffsetX <> fHOMEFAN2_OFFSET_X
		armW.fHomeFan2OffsetX = fHOMEFAN2_OFFSET_X
		bDirtyPos = TRUE
	ENDIF
	IF armW.fHomeFan2OffsetY <> fHOMEFAN2_OFFSET_Y
		armW.fHomeFan2OffsetY = fHOMEFAN2_OFFSET_Y
		bDirtyPos = TRUE
	ENDIF
	
	IF bDirtyPos AND (NOT IS_ENTITY_DEAD(crowdPeds[ARMCROWDMEMBER_HOME_FAN2]))
		SET_ENTITY_COORDS(crowdPeds[ARMCROWDMEMBER_HOME_FAN2], ARM_GET_TABLE_CROWD_POS(table, ARMCROWDMEMBER_HOME_FAN2))
		SET_ENTITY_HEADING(crowdPeds[ARMCROWDMEMBER_HOME_FAN2], GET_HEADING_BETWEEN_VECTORS(ARM_GET_TABLE_CROWD_POS(table, ARMCROWDMEMBER_HOME_FAN2), ARM_GET_TABLE_CENTER(table)))
	ENDIF
	
	bDirtyPos = FALSE

	// Check for new home fan 3 position.
	IF armW.fHomeFan3OffsetX <> fHOMEFAN3_OFFSET_X
		armW.fHomeFan3OffsetX = fHOMEFAN3_OFFSET_X
		bDirtyPos = TRUE
	ENDIF
	IF armW.fHomeFan3OffsetY <> fHOMEFAN3_OFFSET_Y
		armW.fHomeFan3OffsetY = fHOMEFAN3_OFFSET_Y
		bDirtyPos = TRUE
	ENDIF
	
	IF bDirtyPos AND (NOT IS_ENTITY_DEAD(crowdPeds[ARMCROWDMEMBER_HOME_FAN3]))
		SET_ENTITY_COORDS(crowdPeds[ARMCROWDMEMBER_HOME_FAN3], ARM_GET_TABLE_CROWD_POS(table, ARMCROWDMEMBER_HOME_FAN3))
		SET_ENTITY_HEADING(crowdPeds[ARMCROWDMEMBER_HOME_FAN3], GET_HEADING_BETWEEN_VECTORS(ARM_GET_TABLE_CROWD_POS(table, ARMCROWDMEMBER_HOME_FAN3), ARM_GET_TABLE_CENTER(table)))
	ENDIF
	
	bDirtyPos = FALSE

	// Check for new values for the wrestle cam.
	IF armW.fWrestleCamX <> fCAM_WRESTLE_X
		armW.fWrestleCamX = fCAM_WRESTLE_X
		bDirtyPos = TRUE
	ENDIF
	IF armW.fWrestleCamY <> fCAM_WRESTLE_Y
		armW.fWrestleCamY = fCAM_WRESTLE_Y
		bDirtyPos = TRUE
	ENDIF
	IF armW.fWrestleCamZ <> fCAM_WRESTLE_Z
		armW.fWrestleCamZ = fCAM_WRESTLE_Z
		bDirtyPos = TRUE
	ENDIF
	IF armW.fWrestleLookX <> fCAM_WRESTLE_LX
		armW.fWrestleLookX = fCAM_WRESTLE_LX
		bDirtyLook = TRUE
	ENDIF
	IF armW.fWrestleLookY <> fCAM_WRESTLE_LY
		armW.fWrestleLookY = fCAM_WRESTLE_LY
		bDirtyLook = TRUE
	ENDIF
	IF armW.fWrestleLookZ <> fCAM_WRESTLE_LZ
		armW.fWrestleLookZ = fCAM_WRESTLE_LZ
		bDirtyLook = TRUE
	ENDIF
	IF armW.fWrestleFOV <> fCAM_WRESTLE_FOV
		armW.fWrestleFOV = fCAM_WRESTLE_FOV
		bDirtyFOV = TRUE
	ENDIF
	
	// Update wrestle cam pos.
	IF DOES_CAM_EXIST(cameraSet.cameras[ARMCAMERAID_WRESTLE])
		IF bDirtyPos
			SET_CAM_COORD(cameraSet.cameras[ARMCAMERAID_WRESTLE], ARM_TABLE_TO_WORLD(table, PICK_VECTOR(bHome, <<fCAM_WRESTLE_X, fCAM_WRESTLE_Y, fCAM_WRESTLE_Z>>, <<-fCAM_WRESTLE_X, -fCAM_WRESTLE_Y, fCAM_WRESTLE_Z>>)))
		ENDIF
		IF bDirtyPos OR bDirtyLook
			POINT_CAM_AT_COORD(cameraSet.cameras[ARMCAMERAID_WRESTLE], ARM_TABLE_TO_WORLD(table, PICK_VECTOR(bHome, <<fCAM_WRESTLE_LX, fCAM_WRESTLE_LY, fCAM_WRESTLE_LZ>>, <<-fCAM_WRESTLE_LX, -fCAM_WRESTLE_LY, fCAM_WRESTLE_LZ>>)))
		ENDIF
		IF bDirtyFOV
			SET_CAM_FOV(cameraSet.cameras[ARMCAMERAID_WRESTLE], fCAM_WRESTLE_FOV)
		ENDIF
	ENDIF
ENDPROC

// Draw stuff to help.
PROC ARM_DEBUG_DRAW()
	
ENDPROC

// Main debug update function.
PROC ARM_UPDATE_DEBUG(ARM_SERVER_BROADCAST_DATA& serverData, ARM_TABLE& table, ARM_CAMERA_SET& cameraSet, INT iClientMPState, INT iServerMPState, INT iGameState, OBJECT_INDEX attachmentObject, OBJECT_INDEX refereeAttachment, PED_INDEX& crowdPeds[])
	// Update widgets.
	ARM_UPDATE_WIDGETS(serverData, table, cameraSet, iClientMPState, iServerMPState, iGameState, attachmentObject, refereeAttachment, crowdPeds, eSelfID = ARMWRESTLERID_HOME)
	
	// Shortcut button for debug draw switch.
	IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
		armW.bDebugDraw = NOT armW.bDebugDraw
	ENDIF
	
//	// Modify the current score.
//	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_N)
//		ARM_SET_SERVER_SCORE(serverData, 0.0)
//	ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
//		ARM_SET_SERVER_SCORE(serverData, -0.9)
//	ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
//		ARM_SET_SERVER_SCORE(serverData, 0.9)
//	ENDIF
	
	// Turn on debug draw and draw.
	IF armW.bDebugDraw
		IF NOT armW.bDebugDrawLast
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
			armW.bDebugDrawLast = TRUE
		ENDIF
		ARM_DEBUG_DRAW()
	
	// Turn off debug draw.
	ELIF armW.bDebugDrawLast
		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(FALSE)
		armW.bDebugDrawLast = FALSE
	ENDIF
ENDPROC
