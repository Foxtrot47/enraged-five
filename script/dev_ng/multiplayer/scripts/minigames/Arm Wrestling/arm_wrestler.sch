
//////////////////////////////////////////////////////////////////////
/* arm_wrestler.sch													*/
/* Author: DJ Jones													*/
/* Wrestler definitions for arm wrestling minigame.					*/
//////////////////////////////////////////////////////////////////////

CONST_INT iARM_TEMPORAL_SHIFT 1000

// An arm wrestler.
STRUCT ARM_WRESTLER
	ARM_INPUT input
	structTimer bucketTimer
//	structTimer scoreGateTimer
	OBJECT_INDEX attachmentObject
	NETWORK_INDEX attachmentNetwork
	PED_INDEX otherWrestler
	STRING sCurAnim
	FLOAT fLocalScoreBucket
ENDSTRUCT

// Add a score to a bucket.
PROC ARM_ADD_SCORE_TO_LOCAL_BUCKET(ARM_WRESTLER& wrestler, FLOAT fScore)
	
	wrestler.fLocalScoreBucket += fScore
ENDPROC

FUNC FLOAT ARM_GET_LOCAL_BUCKET_SCORE(ARM_WRESTLER& wrestler)
	
	RETURN wrestler.fLocalScoreBucket
ENDFUNC

PROC ARM_RESET_LOCAL_BUCKET_SCORE(ARM_WRESTLER& wrestler, FLOAT fScore = 0.0)
	
	wrestler.fLocalScoreBucket = fScore
ENDPROC

//// Accessor for bucket timer state.
//FUNC BOOL ARM_IS_SCORE_GATE_TIMER_STARTED(ARM_WRESTLER& wrestler)
//	RETURN IS_TIMER_STARTED(wrestler.scoreGateTimer)
//ENDFUNC
//
//// Accessor for bucket timer value.
//FUNC FLOAT ARM_GET_SCORE_GATE_TIMER_VALUE(ARM_WRESTLER& wrestler)
//	IF NOT IS_TIMER_STARTED(wrestler.scoreGateTimer)
//		RETURN 0.0
//	ENDIF
//	
//	RETURN GET_TIMER_IN_SECONDS(wrestler.scoreGateTimer)
//ENDFUNC
//
//// Mutator to start bucket timer.
//PROC ARM_RESET_SCORE_GATE_TIMER(ARM_WRESTLER& wrestler, FLOAT fStart = 0.0, BOOL bForceRestart = TRUE)
//	IF NOT IS_TIMER_STARTED(wrestler.scoreGateTimer)
//		START_TIMER_AT(wrestler.scoreGateTimer, fStart)
//	ELIF bForceRestart
//		RESTART_TIMER_AT(wrestler.scoreGateTimer, fStart)
//	ENDIF
//ENDPROC
//
//// Mutator to cancel bucket timer.
//PROC ARM_CANCEL_SCORE_GATE_TIMER(ARM_WRESTLER& wrestler)
//	IF IS_TIMER_STARTED(wrestler.scoreGateTimer)
//		CANCEL_TIMER(wrestler.bucketTimer)
//	ENDIF
//ENDPROC
//

// Accessor for bucket timer state.
FUNC BOOL ARM_IS_BUCKET_TIMER_STARTED(ARM_WRESTLER& wrestler)
	RETURN IS_TIMER_STARTED(wrestler.bucketTimer)
ENDFUNC

// Accessor for bucket timer value.
FUNC FLOAT ARM_GET_BUCKET_TIMER_VALUE(ARM_WRESTLER& wrestler)
	IF NOT IS_TIMER_STARTED(wrestler.bucketTimer)
		RETURN 0.0
	ENDIF
	
	RETURN GET_TIMER_IN_SECONDS(wrestler.bucketTimer)
ENDFUNC

// Mutator to start bucket timer.
PROC ARM_RESET_BUCKET_TIMER(ARM_WRESTLER& wrestler, FLOAT fStart = 0.0, BOOL bForceRestart = TRUE)
	IF NOT IS_TIMER_STARTED(wrestler.bucketTimer)
		START_TIMER_AT(wrestler.bucketTimer, fStart)
	ELIF bForceRestart
		RESTART_TIMER_AT(wrestler.bucketTimer, fStart)
	ENDIF
ENDPROC

// Mutator to cancel bucket timer.
PROC ARM_CANCEL_BUCKET_TIMER(ARM_WRESTLER& wrestler)
	IF IS_TIMER_STARTED(wrestler.bucketTimer)
		CANCEL_TIMER(wrestler.bucketTimer)
	ENDIF
ENDPROC

// Accessor for attachment object.
FUNC OBJECT_INDEX ARM_GET_WRESTLER_ATTACHMENT_OBJECT(ARM_WRESTLER& wrestler)
	RETURN wrestler.attachmentObject
ENDFUNC

// Mutator for attachment object.
PROC ARM_SET_WRESTLER_ATTACHMENT_OBJECT(ARM_WRESTLER& wrestler, OBJECT_INDEX attachmentObject, NETWORK_INDEX attachmentNetwork)
	wrestler.attachmentObject = attachmentObject
	wrestler.attachmentNetwork = attachmentNetwork
ENDPROC

// Accessor for current anim name.
FUNC STRING ARM_GET_CURRENT_WRESTLER_ANIMATION(ARM_WRESTLER& wrestler)
	RETURN wrestler.sCurAnim
ENDFUNC

// Mutator for current anim name.
PROC ARM_SET_CURRENT_WRESTLER_ANIMATION(ARM_WRESTLER& wrestler, STRING sCurAnim)
	wrestler.sCurAnim = sCurAnim
ENDPROC

// Is the player model large (or small)?
FUNC BOOL ARM_IS_PLAYER_LARGE()
	RETURN TRUE
ENDFUNC
