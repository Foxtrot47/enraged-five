
//////////////////////////////////////////////////////////////////////
/* arm_wrestler_lib.sch												*/
/* Author: DJ Jones													*/
/* Player functionality for arm wrestling minigame.					*/
//////////////////////////////////////////////////////////////////////

#IF IS_DEBUG_BUILD
USING "script_debug.sch"
#ENDIF

// Tells a ped to play an animation.
PROC ARM_PLAY_WRESTLER_ANIMATION(ARM_WRESTLER& wrestler, PED_INDEX pedIndex, STRING sAnimDict, STRING sAnimName, FLOAT fBlendInAlpha = NORMAL_BLEND_IN, FLOAT fBlendOutAlpha = NORMAL_BLEND_OUT, ANIMATION_FLAGS flags = AF_DEFAULT, FLOAT fStartPhase = 0.0, BOOL bPhaseControlled = FALSE)
	IF NOT IS_ENTITY_DEAD(pedIndex)
		TASK_PLAY_ANIM(pedIndex, sAnimDict, sAnimName, fBlendInAlpha, fBlendOutAlpha, -1, flags, fStartPhase, bPhaseControlled)
		ARM_SET_CURRENT_WRESTLER_ANIMATION(wrestler, sAnimName)
	ENDIF
ENDPROC

// Teleport wrestler to the home or away position at the table and set an animation, and remove control.
PROC ARM_SETUP_WRESTLERS(ARM_WRESTLER& wrestlers[], ARM_TABLE& table, ARM_CAMERA_SET& cameraSet, BOOL bSetCamera,  FLOAT fMyPref, FLOAT fOppPref)
	OBJECT_INDEX attachmentObject
	NETWORK_INDEX niAttachment
	VECTOR vNewPos
	FLOAT fHeading

	// Create an object to attach the wrestler to.
	vNewPos = ARM_GET_TABLE_WRESTLER_POS(table, (fMyPref > fOppPref), ARM_IS_PLAYER_LARGE())
	fHeading = ARM_GET_TABLE_ROTATION(table)
	IF fMyPref < fOppPref //ARE_VECTORS_ALMOST_EQUAL(ARM_GET_TABLE_WRESTLER_POS(table, FALSE, ARM_IS_PLAYER_LARGE()), GET_ENTITY_COORDS(PLAYER_PED_ID()), 0.3)
		fHeading = WRAP(fHeading + 180.0, 0.0, 360.0)
	ENDIF
	attachmentObject = CREATE_OBJECT_NO_OFFSET(PROAIR_HOC_PUCK, vNewPos, FALSE, FALSE)
	SET_ENTITY_HEADING(attachmentObject, fHeading)
	SET_ENTITY_VISIBLE(attachmentObject, FALSE)
	//SET_ENTITY_COLLISION(attachmentObject, FALSE)
	FREEZE_ENTITY_POSITION(attachmentObject, TRUE)
	 
	niAttachment = OBJ_TO_NET(attachmentObject)
//	IF NET_TO_OBJ(niAttachment) = attachmentObject
//		CDEBUG1LN(DEBUG_ARM_WRESTLING, "arm_wrestling: NETWORK ID SUCCESFULLY GRABBED FOR PUCK, setting to not migrate")
//		SET_NETWORK_ID_CAN_MIGRATE(niAttachment, FALSE)
//	ENDIF
	
	ARM_SET_WRESTLER_ATTACHMENT_OBJECT(wrestlers[eSelfID], attachmentObject, niAttachment)
	
	CDEBUG1LN(DEBUG_ARM_WRESTLING, "arm wrestle attachment set at ", vNewPos)
	
	// Put the wrestler in place at the table.
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		ATTACH_ENTITY_TO_ENTITY(PLAYER_PED_ID(), attachmentObject, -1, <<0,0,0>>, <<0,0,0>>)
		//ATTACH_ENTITY_TO_ENTITY_PHYSICALLY(PLAYER_PED_ID(), attachmentObject, GET_PED_BONE_INDEX(PLAYER_PED_ID(), BONETAG_ROOT), 0, <<0,0,0>>, <<0,0,0>>, <<0,0,0>>, -1.0, TRUE)
		//ATTACH_ENTITY_TO_WORLD_PHYSICALLY(PLAYER_PED_ID(), GET_PED_BONE_INDEX(PLAYER_PED_ID(), BONETAG_ROOT), GET_ENTITY_COORDS(attachmentObject), <<0,0,0>>, -1.0, FALSE)
		SET_ENTITY_HEADING(PLAYER_PED_ID(), fHeading)
		CDEBUG1LN(DEBUG_ARM_WRESTLING, "arm_wrestling: fheading set to ", fHeading)
		
	//	FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
		
		SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
		CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
		//SET_PLAYER_INVINCIBLE(PLAYER_ID(), TRUE)
		
		//IF NOT IS_PLAYER_DEAD(PLAYER_ID())
		//	SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
		//ENDIF
	ENDIF
	
	// Activate the play camera.
	IF bSetCamera
		ARM_SET_ACTIVE_CAMERA(cameraSet, ARMCAMERAID_IDLE, TRUE)
		RENDER_SCRIPT_CAMS(TRUE, FALSE)
	ENDIF
ENDPROC


// 
PROC ARM_GET_NEXT_TIMESTAMP_SCORE(ARM_SERVER_BROADCAST_DATA& serverData, TIME_DATATYPE tdCurrentTime, TIME_DATATYPE& tdReturnTimeStamp, FLOAT& fReturnScore)
	INT iCounter
	REPEAT ARM_SCORES iCounter
		IF IS_TIME_MORE_THAN(serverData.tdTimeStamps[iCounter], tdCurrentTime)
		OR (iCounter = (ARM_SCORES-1))
			tdReturnTimeStamp = serverData.tdTimeStamps[iCounter]
			fReturnScore = serverData.fScores[iCounter]
			EXIT
		ENDIF
	ENDREPEAT
	
ENDPROC


//TIME_DATATYPE tdLastShiftedTime
#IF IS_DEBUG_BUILD
//	INT iDebugThrottle
#ENDIF

CONST_FLOAT fARM_ANIM_UPDATE_FORCE 1.28
CONST_INT iSCORE_TRACK_LENGTH 5

CONST_FLOAT fSCORE_DELAY 0.4
//CONST_FLOAT fSCORE_INCREMENTS 10.5 //14.0 //7.0


structTimer ArmDebugTimer
INT iSFXLimiter
FLOAT fLastScore
FLOAT fUsedScore
FLOAT fScoreInterp
//FLOAT fScoreDiffPrediction

PROC SCORE_TRACK_PUSH(FLOAT & fScoreTrack[], FLOAT fPushValue)
	INT i
	
	fScoreTrack[iSCORE_TRACK_LENGTH-1] = 0
	
	REPEAT iSCORE_TRACK_LENGTH i
		IF i < iSCORE_TRACK_LENGTH-1
			fScoreTrack[iSCORE_TRACK_LENGTH-(i+1)] = fScoreTrack[iSCORE_TRACK_LENGTH-(i+2)]
			//CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling: fscoreTrack at ", (iSCORE_TRACK_LENGTH-(i+1)), "  = ", fScoreTrack[iSCORE_TRACK_LENGTH-(i+1)])
		ENDIF
	ENDREPEAT
	
	fScoreTrack[0] = fPushValue
ENDPROC

FUNC FLOAT GET_SCORE_TRACKER_DIFF_AVERAGE(FLOAT & fScoreTrack[])
	FLOAT fDiffs[iSCORE_TRACK_LENGTH-1]
	FLOAT fTotalDiffScore
	FLOAT fScoreAvg
	INT i
	
	REPEAT (iSCORE_TRACK_LENGTH-2) i
		IF i < (iSCORE_TRACK_LENGTH-2)
			IF fScoreTrack[i] <> 0
			AND fScoreTrack[i+1] <> 0
				fDiffs[i] = fScoreTrack[i] - fScoreTrack[i+1]
				//CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling: fDiffs at ", i, "  = ", fDiffs[i])
			ENDIF
		ENDIF
	ENDREPEAT
	
	REPEAT iSCORE_TRACK_LENGTH-1 i
		IF i < iSCORE_TRACK_LENGTH-1
			IF fDiffs[i] <> 0
				fTotalDiffScore += fDiffs[i]
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF fTotalDiffScore <> 0
		fScoreAvg = (fTotalDiffScore/TO_FLOAT(iSCORE_TRACK_LENGTH-1))
	ELSE
		fScoreAvg = PICK_FLOAT(GET_RANDOM_BOOL(), 0.03, -0.03) 
	ENDIF
	
	//CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling: Sending fScoreAvg of ", fScoreAvg)
	
	RETURN fScoreAvg
ENDFUNC

PROC ARM_ADD_SHAKES(FLOAT & fUsedScoreAdjusted)
	FLOAT fTemp1, fTemp2, fTimeSinceLastFrame
	
	fTimeSinceLastFrame = GET_FRAME_TIME()
	
	fTemp2 = GET_RANDOM_FLOAT_IN_RANGE(0.0 - fRealShakeFactor, 0.0 + fRealShakeFactor)		
	fTemp1 = GET_RANDOM_FLOAT_IN_RANGE(0.0 - fShakeFactor, 0.0 + fShakeFactor)
	
	IF fUsedScoreAdjusted > 0.0
	AND fUsedScoreAdjusted < 1.0
		IF 	(fUsedScoreAdjusted + (fTemp1 * fTimeSinceLastFrame)) > 0.1
		AND (fUsedScoreAdjusted + (fTemp1 * fTimeSinceLastFrame)) < 0.9
			
			fUsedScoreAdjusted += (fTemp2 * fTimeSinceLastFrame)
			fUsedScoreAdjusted += (fTemp1 * fTimeSinceLastFrame)
		
		ELIF (fUsedScoreAdjusted + ((fTemp1 * fTimeSinceLastFrame)/2)) > 0.0
		AND (fUsedScoreAdjusted + ((fTemp1 * fTimeSinceLastFrame)/2)) < 1.0
			
			fUsedScoreAdjusted += (fTemp2 * fTimeSinceLastFrame)/2
			fUsedScoreAdjusted += (fTemp1 * fTimeSinceLastFrame)/2
			
		ENDIF
	ENDIF
ENDPROC

// Update wrestler state.
PROC ARM_UPDATE_WRESTLERS(ARM_WRESTLER& wrestlers[], ARM_SERVER_BROADCAST_DATA& serverData, ARM_PLAYER_BROADCAST_DATA& playerData[], BOOL bLocalWinning, ARM_PARTICIPANT_INFO & info)
	
	bLocalWinning = bLocalWinning
	
	// variables for scoreprediction for animation use
	TIME_DATATYPE tdShiftedTime = GET_TIME_OFFSET(GET_NETWORK_TIME(), iARM_TEMPORAL_SHIFT)
	TIME_DATATYPE tdNextTimeStamp
	INT iCount
	FLOAT fNextScore
	//BOOL bAggressivePlay = FALSE
	
	FLOAT fScore, fUsedScoreAdjusted
	
	IF NOT IS_PAUSE_MENU_ACTIVE()
		ARM_GET_LOCAL_INPUT(wrestlers[eSelfID].input)
	ENDIF
	
	// See if it's time to move to a new bucket.
	IF ARM_IS_BUCKET_TIMER_STARTED(wrestlers[eSelfID])
		IF ARM_GET_BUCKET_TIMER_VALUE(wrestlers[eSelfID]) >= fBUCKET_SWITCH_TIME
//			CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling: BUCKET scores WERE SET. Bucket timer = ", ARM_GET_BUCKET_TIMER_VALUE(wrestlers[eSelfID]))
			
			ARM_ADD_SCORE_TO_BUCKET(playerData[info.iPlayers[eSelfID]], ARM_GET_CURRENT_BUCKET(playerData[info.iPlayers[eSelfID]]), ARM_GET_LOCAL_BUCKET_SCORE(wrestlers[eSelfID]))
			ARM_NEXT_BUCKET(playerData[info.iPlayers[eSelfID]])
			ARM_RESET_LOCAL_BUCKET_SCORE(wrestlers[eSelfID])
			ARM_RESET_BUCKET_TIMER(wrestlers[eSelfID])
			
		ELSE
			
//			IF (GET_GAME_TIMER() % 1000) < 50
//				CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling:  BUCKET scores not ready yet, bucket timer = ", ARM_GET_BUCKET_TIMER_VALUE(wrestlers[eSelfID]))
//			ENDIF
		ENDIF
	ELSE
		ARM_RESET_BUCKET_TIMER(wrestlers[eSelfID])
	ENDIF
	
	// See if the server has instructed us to clear any buckets.
	IF MPGlobals.WrestleData.iBucketClearBits <> 0
		REPEAT iSCORE_BUCKETS iCount
			IF IS_BIT_SET(MPGlobals.WrestleData.iBucketClearBits, iCount)
				#IF IS_DEBUG_BUILD
				DRAW_DEBUG_TEXT_2D("Server asked for reset", <<0.28, 0.28, 0.0>>)
				#ENDIF
				
				CDEBUG1LN(DEBUG_ARM_WRESTLING, "client update resetting bucket # ", iCount)
				ARM_SET_SCORE_BUCKET(playerData[info.iPlayers[eSelfID]], iCount, 0.0)
				CLEAR_BIT(MPGlobals.WrestleData.iBucketClearBits, iCount)
			ENDIF
		ENDREPEAT
	ENDIF
	
	// See if we should put any points in the bucket.
//	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
//		ARM_ADD_SCORE_TO_BUCKET(playerData[eSelfID], ARM_GET_CURRENT_BUCKET(playerData[eSelfID]), GET_FRAME_TIME())
//	ENDIF
	
	ARM_ADD_SCORE_TO_LOCAL_BUCKET(wrestlers[eSelfID], ARM_GET_LARGEST_STICK_CHANGE_HORIZONTAL(wrestlers[eSelfID].input))
	
	IF ARM_GET_LARGEST_STICK_CHANGE_HORIZONTAL(wrestlers[eSelfID].input) = 0.0
		IF NOT ARM_GET_CLIENT_FLAG(playerData[info.iPlayers[eSelfID]], ARMCLIENTFLAG_NOT_MOVING_STICKS)
			ARM_SET_CLIENT_FLAG(playerData[info.iPlayers[eSelfID]], ARMCLIENTFLAG_NOT_MOVING_STICKS, TRUE)
			playerData[info.iPlayers[eSelfID]].fLastUsedScore = fUsedScore
		ENDIF
	ELSE
		IF ARM_GET_CLIENT_FLAG(playerData[info.iPlayers[eSelfID]], ARMCLIENTFLAG_NOT_MOVING_STICKS)
			ARM_SET_CLIENT_FLAG(playerData[info.iPlayers[eSelfID]], ARMCLIENTFLAG_NOT_MOVING_STICKS, FALSE)
		ENDIF
	ENDIF
	
	// DEBUG OUTPUT TO MAKE SURE CONTROL INPUT IS COMING IN
	IF (GET_GAME_TIMER() % 2500) < 50
		//CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling: LOCAL WRESTLER INPUT = ", ARM_GET_LARGEST_STICK_CHANGE_HORIZONTAL(wrestlers[eSelfID].input))
		//CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling: LOCAL BUCKET VALUE = ", ARM_GET_LOCAL_BUCKET_SCORE(wrestlers[eSelfID]))
	ENDIF
	
	// MoVE network script
	// setup score; if winning positive, if losing negative; negative = away is winning
	fScore = ARM_GET_SERVER_SCORE(serverData)
	
	IF fScore = fLastScore
	OR (IS_TIMER_STARTED(ArmDebugTimer) AND GET_TIMER_IN_SECONDS(ArmDebugTimer) < fSCORE_DELAY)
		
		IF ( (fUsedScore <= fLastScore AND fScoreInterp > 0)
			OR (fUsedScore >= fLastScore AND fScoreInterp < 0) )
//			IF  ARM_GET_CLIENT_FLAG(playerData[info.iPlayers[eSelfID]], ARMCLIENTFLAG_NOT_MOVING_STICKS)
//			AND ARM_GET_CLIENT_FLAG(playerData[info.iPlayers[eOpponentID]], ARMCLIENTFLAG_NOT_MOVING_STICKS)
//			AND ARM_SERVER_GET_FLAG(serverData, ARMSERVERFLAG_SCORE_CHANGING)
//				fScoreInterp = 0.001
//			ENDIF
			fUsedScore += fScoreInterp
			//CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling: fUsedScore += fScoreInterp (", fScoreInterp, ") = ", fUsedScore)
		ELSE
			// DEBUG OUTPUT TO SEE WHAT'S GOING ON
			IF (GET_GAME_TIMER() % 5000) < 10
				CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling: STATUS CHECK | fScore = ", fScore, "  | fUsedScore = ", fUsedScore, " | fLastScore = ", fLastScore, " | fScoreInterp = ", fScoreInterp)
			ENDIF
		ENDIF
		
		// Winning conditions ------------------------------------------
		IF fUsedScore > 1
			fUsedScore = 1
			
			CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling: fUsedScore reset to ", fUsedScore)
			//IF ARM_SERVER_GET_FLAG(serverData, ARMSERVERFLAG_AWAYWIN) OR ARM_SERVER_GET_FLAG(serverData, ARMSERVERFLAG_HOMEWIN)
			IF ARM_SERVER_GET_FLAG(serverData, ARMSERVERFLAG_WIN_REACHED)
			AND NOT ARM_GET_CLIENT_FLAG(playerData[info.iPlayers[eSelfID]], ARMCLIENTFLAG_WINNER_SET)
			AND (NOT ARM_GET_CLIENT_FLAG(playerData[info.iPlayers[eSelfID]], ARMCLIENTFLAG_NOT_MOVING_STICKS)
				OR NOT ARM_GET_CLIENT_FLAG(playerData[info.iPlayers[eOpponentID]], ARMCLIENTFLAG_NOT_MOVING_STICKS))
				CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling: ARMCLIENTFLAG_WINNER_SET is now set")
				ARM_SET_CLIENT_FLAG(playerData[info.iPlayers[eSelfID]], ARMCLIENTFLAG_WINNER_SET, TRUE)
			ELSE
				IF ARM_GET_CLIENT_FLAG(playerData[info.iPlayers[eSelfID]], ARMCLIENTFLAG_NOT_MOVING_STICKS)
				AND ARM_GET_CLIENT_FLAG(playerData[info.iPlayers[eOpponentID]], ARMCLIENTFLAG_NOT_MOVING_STICKS)
					IF ARM_GET_CLIENT_FLAG(playerData[info.iPlayers[eSelfID]], ARMCLIENTFLAG_WINNER_SET)
						CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling: resetting winner flag because of lack stick movement from both players")
						ARM_SET_CLIENT_FLAG(playerData[info.iPlayers[eSelfID]], ARMCLIENTFLAG_WINNER_SET, FALSE)
					ENDIF
				ENDIF
			ENDIF
		ELIF fUsedScore < -1
			fUsedScore = -1
			
			CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling: fUsedScore reset to ", fUsedScore)
			//IF ARM_SERVER_GET_FLAG(serverData, ARMSERVERFLAG_AWAYWIN) OR ARM_SERVER_GET_FLAG(serverData, ARMSERVERFLAG_HOMEWIN)
			IF ARM_SERVER_GET_FLAG(serverData, ARMSERVERFLAG_WIN_REACHED)
			AND NOT ARM_GET_CLIENT_FLAG(playerData[info.iPlayers[eSelfID]], ARMCLIENTFLAG_WINNER_SET)
			AND (NOT ARM_GET_CLIENT_FLAG(playerData[info.iPlayers[eSelfID]], ARMCLIENTFLAG_NOT_MOVING_STICKS)
				OR NOT ARM_GET_CLIENT_FLAG(playerData[info.iPlayers[eOpponentID]], ARMCLIENTFLAG_NOT_MOVING_STICKS))
				CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling: ARMCLIENTFLAG_WINNER_SET is now set")
				ARM_SET_CLIENT_FLAG(playerData[info.iPlayers[eSelfID]], ARMCLIENTFLAG_WINNER_SET, TRUE)
			ELSE
				IF ARM_GET_CLIENT_FLAG(playerData[info.iPlayers[eSelfID]], ARMCLIENTFLAG_NOT_MOVING_STICKS)
				AND ARM_GET_CLIENT_FLAG(playerData[info.iPlayers[eOpponentID]], ARMCLIENTFLAG_NOT_MOVING_STICKS)
					IF ARM_GET_CLIENT_FLAG(playerData[info.iPlayers[eSelfID]], ARMCLIENTFLAG_WINNER_SET)
						CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling: resetting winner flag because of lack stick movement")
						ARM_SET_CLIENT_FLAG(playerData[info.iPlayers[eSelfID]], ARMCLIENTFLAG_WINNER_SET, FALSE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		// -----------------------------------------------------------
		
	ELSE
		fLastScore = fScore
		CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling: fLastScore set to ", fLastScore)
		
		IF (fLastScore - fUsedScore) > 0.2
		AND (GET_GAME_TIMER() - iSFXLimiter) > 750
			iSFXLimiter = GET_GAME_TIMER()
			CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling: delta = ", (fLastScore - fUsedScore))
			CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling: PLAYING WOOD CREEK SOUND ~~~~~~~~~~~~~~~~~~~~~~~~~~")
			PLAY_SOUND_FROM_ENTITY(-1, "ARM_WRESTLING_WOOD_CREEK_MASTER", PLAYER_PED_ID())
		ENDIF
		CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling: frame time: ", GET_FRAME_TIME())
		fScoreInterp = (fLastScore - fUsedScore)/PICK_FLOAT(GET_FRAME_TIME() > 0.3, fSCORE_INCREMENTS, fSCORE_INC_FAST) 
		CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling: fScoreInterp set to ", fScoreInterp)
		
		fUsedScore += fScoreInterp
		
		START_TIMER_NOW_SAFE(ArmDebugTimer)
		
	ENDIF
	
	fUsedScoreAdjusted = (fUsedScore/2) + 0.5
	
	//ARM_ADD_SHAKES(fUsedScoreAdjusted)
	
	#IF IS_DEBUG_BUILD
	TEXT_LABEL_31 sDebugAWScore[5]
//	sDebugAWScore[0] = "fScore : "
//	sDebugAWScore[0] += GET_STRING_FROM_FLOAT(fScore)
//	DRAW_DEBUG_TEXT_2D(sDebugAWScore[0], <<0.1, 0.66, 0.0>>)
//	
//	sDebugAWScore[0] = "fUsedScoreAdjusted : "
//	sDebugAWScore[0] += GET_STRING_FROM_FLOAT(fUsedScoreAdjusted)
//	DRAW_DEBUG_TEXT_2D(sDebugAWScore[0], <<0.65, 0.5, 0.0>>)
//	
//	sDebugAWScore[3] = "fScoreInterp : "
//	sDebugAWScore[3] += GET_STRING_FROM_FLOAT(fScoreInterp)
//	DRAW_DEBUG_TEXT_2D(sDebugAWScore[3], <<0.65, 0.47, 0.0>>)
//	
//	sDebugAWScore[4] = "fUsedScore : "
//	sDebugAWScore[4] += GET_STRING_FROM_FLOAT(fUsedScore)
//	DRAW_DEBUG_TEXT_2D(sDebugAWScore[4], <<0.65, 0.44, 0.0>>)
//	
//	IF ARM_GET_CLIENT_FLAG(playerData[info.iPlayers[eSelfID]], ARMCLIENTFLAG_NOT_MOVING_STICKS)
//		sDebugAWScore[2] = "NO! sticks"
//	ELSE
//		sDebugAWScore[2] = "YES sticks"
//	ENDIF
//	DRAW_DEBUG_TEXT_2D(sDebugAWScore[2], <<0.1, 0.7, 0.0>>)
	#ENDIF
	
	IF IS_TASK_MOVE_NETWORK_ACTIVE(PLAYER_PED_ID())
		SET_TASK_MOVE_NETWORK_SIGNAL_FLOAT(PLAYER_PED_ID(), "Phase", fUsedScoreAdjusted)
		SET_TASK_MOVE_NETWORK_SIGNAL_FLOAT(PLAYER_PED_ID(), "Wobble", PICK_FLOAT(ARM_SERVER_GET_FLAG(serverData, ARMSERVERFLAG_SCORE_CHANGING), 1, 0))
	ELSE
		IF (GET_GAME_TIMER() % 1000) < 50
			CDEBUG1LN(DEBUG_ARM_WRESTLING, "am_armwrestling.sc: MoVE network is not Active!  but score is IS GREATAR THAN 0.05")
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(wrestlers[eSelfID].otherWrestler)
	AND NOT IS_PED_INJURED(wrestlers[eSelfID].otherWrestler)
		IF IS_TASK_MOVE_NETWORK_ACTIVE(wrestlers[eSelfID].otherWrestler)
			SET_TASK_MOVE_NETWORK_SIGNAL_FLOAT(wrestlers[eSelfID].otherWrestler, "Phase", fUsedScoreAdjusted)
			SET_TASK_MOVE_NETWORK_SIGNAL_FLOAT(wrestlers[eSelfID].otherWrestler, "Wobble", PICK_FLOAT(ARM_SERVER_GET_FLAG(serverData, ARMSERVERFLAG_SCORE_CHANGING), 1, 0))
		ELSE
			IF (GET_GAME_TIMER() % 1000) < 50
			CDEBUG1LN(DEBUG_ARM_WRESTLING, "am_armwrestling.sc: MoVE network is not Active for wrestlers[eSelfID].otherWrestler! but score is IS GREATAR THAN 0.05")
			ENDIF
		ENDIF
	ENDIF
	
	ARM_GET_NEXT_TIMESTAMP_SCORE(serverData, tdShiftedTime, tdNextTimeStamp, fNextScore)
	
	#IF IS_DEBUG_BUILD
	sDebugAWScore[1] = "fNextScore : "
	sDebugAWScore[1] += GET_STRING_FROM_FLOAT(fNextScore)
	DRAW_DEBUG_TEXT_2D(sDebugAWScore[1], <<0.65, 0.53, 0.0>>)
	
//	sDebugAWScore[2] = "fAnimTime : "
//	sDebugAWScore[2] += GET_STRING_FROM_FLOAT(fAnimTime)
//	DRAW_DEBUG_TEXT_2D(sDebugAWScore[2], <<0.65, 0.56, 0.0>>)
	#ENDIF
	
ENDPROC

// Update wrestler state.
PROC ARM_UPDATE_WRESTLERS_APARTMENT(ARM_WRESTLER& wrestlers[], ARM_SERVER_BROADCAST_DATA& serverData, ARM_PLAYER_BROADCAST_DATA& playerData[], BOOL bLocalWinning, ARM_PARTICIPANT_INFO & info)
	
	bLocalWinning = bLocalWinning
	
	// variables for scoreprediction for animation use
	TIME_DATATYPE tdShiftedTime = GET_TIME_OFFSET(GET_NETWORK_TIME(), iARM_TEMPORAL_SHIFT)
	TIME_DATATYPE tdNextTimeStamp
	INT iCount
	FLOAT fNextScore
	//BOOL bAggressivePlay = FALSE
	
	FLOAT fScore, fUsedScoreAdjusted
	
	IF NOT IS_PAUSE_MENU_ACTIVE()
		ARM_GET_LOCAL_INPUT(wrestlers[eSelfID].input)
	ENDIF
	
	// See if it's time to move to a new bucket.
	IF ARM_IS_BUCKET_TIMER_STARTED(wrestlers[eSelfID])
		IF ARM_GET_BUCKET_TIMER_VALUE(wrestlers[eSelfID]) >= fBUCKET_SWITCH_TIME
//			CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling: BUCKET scores WERE SET. Bucket timer = ", ARM_GET_BUCKET_TIMER_VALUE(wrestlers[eSelfID]))
			
			ARM_ADD_SCORE_TO_BUCKET(playerData[info.iPlayers[eSelfID]], ARM_GET_CURRENT_BUCKET(playerData[info.iPlayers[eSelfID]]), ARM_GET_LOCAL_BUCKET_SCORE(wrestlers[eSelfID]))
			ARM_NEXT_BUCKET(playerData[info.iPlayers[eSelfID]])
			ARM_RESET_LOCAL_BUCKET_SCORE(wrestlers[eSelfID])
			ARM_RESET_BUCKET_TIMER(wrestlers[eSelfID])
			
		ELSE
			
//			IF (GET_GAME_TIMER() % 1000) < 50
//				CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling:  BUCKET scores not ready yet, bucket timer = ", ARM_GET_BUCKET_TIMER_VALUE(wrestlers[eSelfID]))
//			ENDIF
		ENDIF
	ELSE
		ARM_RESET_BUCKET_TIMER(wrestlers[eSelfID])
	ENDIF
	
	// See if the server has instructed us to clear any buckets.
	IF MPGlobals.WrestleData.iBucketClearBits <> 0
		REPEAT iSCORE_BUCKETS iCount
			IF IS_BIT_SET(MPGlobals.WrestleData.iBucketClearBits, iCount)
				#IF IS_DEBUG_BUILD
				DRAW_DEBUG_TEXT_2D("Server asked for reset", <<0.28, 0.28, 0.0>>)
				#ENDIF
				
				CDEBUG1LN(DEBUG_ARM_WRESTLING, "client update resetting bucket # ", iCount)
				ARM_SET_SCORE_BUCKET(playerData[info.iPlayers[eSelfID]], iCount, 0.0)
				CLEAR_BIT(MPGlobals.WrestleData.iBucketClearBits, iCount)
			ENDIF
		ENDREPEAT
	ENDIF
	
	// See if we should put any points in the bucket.
//	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
//		ARM_ADD_SCORE_TO_BUCKET(playerData[eSelfID], ARM_GET_CURRENT_BUCKET(playerData[eSelfID]), GET_FRAME_TIME())
//	ENDIF
	
	ARM_ADD_SCORE_TO_LOCAL_BUCKET(wrestlers[eSelfID], ARM_GET_LARGEST_STICK_CHANGE_HORIZONTAL(wrestlers[eSelfID].input))
	
	IF ARM_GET_LARGEST_STICK_CHANGE_HORIZONTAL(wrestlers[eSelfID].input) = 0.0
		IF NOT ARM_GET_CLIENT_FLAG(playerData[info.iPlayers[eSelfID]], ARMCLIENTFLAG_NOT_MOVING_STICKS)
			ARM_SET_CLIENT_FLAG(playerData[info.iPlayers[eSelfID]], ARMCLIENTFLAG_NOT_MOVING_STICKS, TRUE)
			playerData[info.iPlayers[eSelfID]].fLastUsedScore = fUsedScore
		ENDIF
	ELSE
		IF ARM_GET_CLIENT_FLAG(playerData[info.iPlayers[eSelfID]], ARMCLIENTFLAG_NOT_MOVING_STICKS)
			ARM_SET_CLIENT_FLAG(playerData[info.iPlayers[eSelfID]], ARMCLIENTFLAG_NOT_MOVING_STICKS, FALSE)
		ENDIF
	ENDIF
	
	// DEBUG OUTPUT TO MAKE SURE CONTROL INPUT IS COMING IN
	IF (GET_GAME_TIMER() % 2500) < 50
		//CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling: LOCAL WRESTLER INPUT = ", ARM_GET_LARGEST_STICK_CHANGE_HORIZONTAL(wrestlers[eSelfID].input))
		//CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling: LOCAL BUCKET VALUE = ", ARM_GET_LOCAL_BUCKET_SCORE(wrestlers[eSelfID]))
	ENDIF
	
	// MoVE network script
	// setup score; if winning positive, if losing negative; negative = away is winning
	fScore = ARM_GET_SERVER_SCORE(serverData)
	
	IF fScore = fLastScore
	OR (IS_TIMER_STARTED(ArmDebugTimer) AND GET_TIMER_IN_SECONDS(ArmDebugTimer) < fSCORE_DELAY)
		
		IF ( (fUsedScore <= fLastScore AND fScoreInterp > 0)
			OR (fUsedScore >= fLastScore AND fScoreInterp < 0) )
//			IF  ARM_GET_CLIENT_FLAG(playerData[info.iPlayers[eSelfID]], ARMCLIENTFLAG_NOT_MOVING_STICKS)
//			AND ARM_GET_CLIENT_FLAG(playerData[info.iPlayers[eOpponentID]], ARMCLIENTFLAG_NOT_MOVING_STICKS)
//			AND ARM_SERVER_GET_FLAG(serverData, ARMSERVERFLAG_SCORE_CHANGING)
//				fScoreInterp = 0.001
//			ENDIF
			fUsedScore += fScoreInterp
			//CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling: fUsedScore += fScoreInterp (", fScoreInterp, ") = ", fUsedScore)
		ELSE
			// DEBUG OUTPUT TO SEE WHAT'S GOING ON
			IF (GET_GAME_TIMER() % 5000) < 10
				CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling: STATUS CHECK | fScore = ", fScore, "  | fUsedScore = ", fUsedScore, " | fLastScore = ", fLastScore, " | fScoreInterp = ", fScoreInterp)
			ENDIF
		ENDIF
		
		// Winning conditions ------------------------------------------
		IF fUsedScore > 1
			fUsedScore = 1
			
			CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling: fUsedScore reset to ", fUsedScore)
			//IF ARM_SERVER_GET_FLAG(serverData, ARMSERVERFLAG_AWAYWIN) OR ARM_SERVER_GET_FLAG(serverData, ARMSERVERFLAG_HOMEWIN)
			IF ARM_SERVER_GET_FLAG(serverData, ARMSERVERFLAG_WIN_REACHED)
			AND NOT ARM_GET_CLIENT_FLAG(playerData[info.iPlayers[eSelfID]], ARMCLIENTFLAG_WINNER_SET)
			AND (NOT ARM_GET_CLIENT_FLAG(playerData[info.iPlayers[eSelfID]], ARMCLIENTFLAG_NOT_MOVING_STICKS)
				OR NOT ARM_GET_CLIENT_FLAG(playerData[info.iPlayers[eOpponentID]], ARMCLIENTFLAG_NOT_MOVING_STICKS))
				CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling: ARMCLIENTFLAG_WINNER_SET is now set")
				ARM_SET_CLIENT_FLAG(playerData[info.iPlayers[eSelfID]], ARMCLIENTFLAG_WINNER_SET, TRUE)
			ELSE
				IF ARM_GET_CLIENT_FLAG(playerData[info.iPlayers[eSelfID]], ARMCLIENTFLAG_NOT_MOVING_STICKS)
				AND ARM_GET_CLIENT_FLAG(playerData[info.iPlayers[eOpponentID]], ARMCLIENTFLAG_NOT_MOVING_STICKS)
					IF ARM_GET_CLIENT_FLAG(playerData[info.iPlayers[eSelfID]], ARMCLIENTFLAG_WINNER_SET)
						CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling: resetting winner flag because of lack stick movement from both players")
						ARM_SET_CLIENT_FLAG(playerData[info.iPlayers[eSelfID]], ARMCLIENTFLAG_WINNER_SET, FALSE)
					ENDIF
				ENDIF
			ENDIF
		ELIF fUsedScore < -1
			fUsedScore = -1
			
			CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling: fUsedScore reset to ", fUsedScore)
			//IF ARM_SERVER_GET_FLAG(serverData, ARMSERVERFLAG_AWAYWIN) OR ARM_SERVER_GET_FLAG(serverData, ARMSERVERFLAG_HOMEWIN)
			IF ARM_SERVER_GET_FLAG(serverData, ARMSERVERFLAG_WIN_REACHED)
			AND NOT ARM_GET_CLIENT_FLAG(playerData[info.iPlayers[eSelfID]], ARMCLIENTFLAG_WINNER_SET)
			AND (NOT ARM_GET_CLIENT_FLAG(playerData[info.iPlayers[eSelfID]], ARMCLIENTFLAG_NOT_MOVING_STICKS)
				OR NOT ARM_GET_CLIENT_FLAG(playerData[info.iPlayers[eOpponentID]], ARMCLIENTFLAG_NOT_MOVING_STICKS))
				CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling: ARMCLIENTFLAG_WINNER_SET is now set")
				ARM_SET_CLIENT_FLAG(playerData[info.iPlayers[eSelfID]], ARMCLIENTFLAG_WINNER_SET, TRUE)
			ELSE
				IF ARM_GET_CLIENT_FLAG(playerData[info.iPlayers[eSelfID]], ARMCLIENTFLAG_NOT_MOVING_STICKS)
				AND ARM_GET_CLIENT_FLAG(playerData[info.iPlayers[eOpponentID]], ARMCLIENTFLAG_NOT_MOVING_STICKS)
					IF ARM_GET_CLIENT_FLAG(playerData[info.iPlayers[eSelfID]], ARMCLIENTFLAG_WINNER_SET)
						CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling: resetting winner flag because of lack stick movement")
						ARM_SET_CLIENT_FLAG(playerData[info.iPlayers[eSelfID]], ARMCLIENTFLAG_WINNER_SET, FALSE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		// -----------------------------------------------------------
		
	ELSE
		fLastScore = fScore
		CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling: fLastScore set to ", fLastScore)
		
		IF (fLastScore - fUsedScore) > 0.2
		AND (GET_GAME_TIMER() - iSFXLimiter) > 750
			iSFXLimiter = GET_GAME_TIMER()
			CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling: delta = ", (fLastScore - fUsedScore))
			CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling: PLAYING WOOD CREEK SOUND ~~~~~~~~~~~~~~~~~~~~~~~~~~")
			PLAY_SOUND_FROM_ENTITY(-1, "ARM_WRESTLING_WOOD_CREEK_MASTER", PLAYER_PED_ID())
		ENDIF
		CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling: frame time: ", GET_FRAME_TIME())
		fScoreInterp = (fLastScore - fUsedScore)/PICK_FLOAT(GET_FRAME_TIME() > 0.3, fSCORE_INCREMENTS, fSCORE_INC_FAST) 
		CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling: fScoreInterp set to ", fScoreInterp)
		
		fUsedScore += fScoreInterp
		
		START_TIMER_NOW_SAFE(ArmDebugTimer)
		
	ENDIF
	
	fUsedScoreAdjusted = (fUsedScore/2) + 0.5
	
	//ARM_ADD_SHAKES(fUsedScoreAdjusted)
	
	#IF IS_DEBUG_BUILD
	TEXT_LABEL_31 sDebugAWScore[5]
//	sDebugAWScore[0] = "fScore : "
//	sDebugAWScore[0] += GET_STRING_FROM_FLOAT(fScore)
//	DRAW_DEBUG_TEXT_2D(sDebugAWScore[0], <<0.1, 0.66, 0.0>>)
//	
//	sDebugAWScore[0] = "fUsedScoreAdjusted : "
//	sDebugAWScore[0] += GET_STRING_FROM_FLOAT(fUsedScoreAdjusted)
//	DRAW_DEBUG_TEXT_2D(sDebugAWScore[0], <<0.65, 0.5, 0.0>>)
//	
//	sDebugAWScore[3] = "fScoreInterp : "
//	sDebugAWScore[3] += GET_STRING_FROM_FLOAT(fScoreInterp)
//	DRAW_DEBUG_TEXT_2D(sDebugAWScore[3], <<0.65, 0.47, 0.0>>)
//	
//	sDebugAWScore[4] = "fUsedScore : "
//	sDebugAWScore[4] += GET_STRING_FROM_FLOAT(fUsedScore)
//	DRAW_DEBUG_TEXT_2D(sDebugAWScore[4], <<0.65, 0.44, 0.0>>)
//	
//	IF ARM_GET_CLIENT_FLAG(playerData[info.iPlayers[eSelfID]], ARMCLIENTFLAG_NOT_MOVING_STICKS)
//		sDebugAWScore[2] = "NO! sticks"
//	ELSE
//		sDebugAWScore[2] = "YES sticks"
//	ENDIF
//	DRAW_DEBUG_TEXT_2D(sDebugAWScore[2], <<0.1, 0.7, 0.0>>)
	#ENDIF
	
	IF IS_TASK_MOVE_NETWORK_ACTIVE(PLAYER_PED_ID())
		SET_TASK_MOVE_NETWORK_SIGNAL_FLOAT(PLAYER_PED_ID(), "Phase", fUsedScoreAdjusted)
		SET_TASK_MOVE_NETWORK_SIGNAL_FLOAT(PLAYER_PED_ID(), "Wobble", PICK_FLOAT(ARM_SERVER_GET_FLAG(serverData, ARMSERVERFLAG_SCORE_CHANGING), 1, 0))
	ELSE
		IF (GET_GAME_TIMER() % 1000) < 50
			CDEBUG1LN(DEBUG_ARM_WRESTLING, "am_armwrestling.sc: MoVE network is not Active!  but score is IS GREATAR THAN 0.05")
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(wrestlers[eSelfID].otherWrestler)
	AND NOT IS_PED_INJURED(wrestlers[eSelfID].otherWrestler)
		IF IS_TASK_MOVE_NETWORK_ACTIVE(wrestlers[eSelfID].otherWrestler)
			SET_TASK_MOVE_NETWORK_SIGNAL_FLOAT(wrestlers[eSelfID].otherWrestler, "Phase", fUsedScoreAdjusted)
			SET_TASK_MOVE_NETWORK_SIGNAL_FLOAT(wrestlers[eSelfID].otherWrestler, "Wobble", PICK_FLOAT(ARM_SERVER_GET_FLAG(serverData, ARMSERVERFLAG_SCORE_CHANGING), 1, 0))
		ELSE
			IF (GET_GAME_TIMER() % 1000) < 50
			CDEBUG1LN(DEBUG_ARM_WRESTLING, "am_armwrestling.sc: MoVE network is not Active for wrestlers[eSelfID].otherWrestler! but score is IS GREATAR THAN 0.05")
			ENDIF
		ENDIF
	ENDIF
	
	ARM_GET_NEXT_TIMESTAMP_SCORE(serverData, tdShiftedTime, tdNextTimeStamp, fNextScore)
	
	#IF IS_DEBUG_BUILD
	sDebugAWScore[1] = "fNextScore : "
	sDebugAWScore[1] += GET_STRING_FROM_FLOAT(fNextScore)
	DRAW_DEBUG_TEXT_2D(sDebugAWScore[1], <<0.65, 0.53, 0.0>>)
	
//	sDebugAWScore[2] = "fAnimTime : "
//	sDebugAWScore[2] += GET_STRING_FROM_FLOAT(fAnimTime)
//	DRAW_DEBUG_TEXT_2D(sDebugAWScore[2], <<0.65, 0.56, 0.0>>)
	#ENDIF
	
ENDPROC

// Update wrestler state.
PROC ARM_UPDATE_WRESTLERS_SPECTATOR(ARM_WRESTLER& wrestlers[], ARM_SERVER_BROADCAST_DATA& serverData)
	
	FLOAT fScore
	FLOAT fUsedScoreAdjusted
	
	// MoVE network script
	// setup score; if winning positive, if losing negative; negative = away is winning
	fScore = ARM_GET_SERVER_SCORE(serverData)
	
	IF fScore = fLastScore
	OR (IS_TIMER_STARTED(ArmDebugTimer) AND GET_TIMER_IN_SECONDS(ArmDebugTimer) < fSCORE_DELAY)
		
		IF ( (fUsedScore <= fLastScore AND fScoreInterp > 0)
			OR (fUsedScore >= fLastScore AND fScoreInterp < 0) )

			fUsedScore += fScoreInterp
			//CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling: fUsedScore += fScoreInterp (", fScoreInterp, ") = ", fUsedScore)
		ELSE
			// DEBUG OUTPUT TO SEE WHAT'S GOING ON
			IF (GET_GAME_TIMER() % 5000) < 10
				CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling: STATUS CHECK | fScore = ", fScore, "  | fUsedScore = ", fUsedScore, " | fLastScore = ", fLastScore, " | fScoreInterp = ", fScoreInterp)
			ENDIF
		ENDIF
		
		// Winning conditions ------------------------------------------
		IF fUsedScore > 1
			fUsedScore = 1
			
		ELIF fUsedScore < -1
			fUsedScore = -1
			
		ENDIF
		// -----------------------------------------------------------
		
	ELSE
		fLastScore = fScore
		CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling: fLastScore set to ", fLastScore)
		
		IF (fLastScore - fUsedScore) > 0.2
		AND (GET_GAME_TIMER() - iSFXLimiter) > 750
			iSFXLimiter = GET_GAME_TIMER()
			CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling: delta = ", (fLastScore - fUsedScore))
			CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling: PLAYING WOOD CREEK SOUND ~~~~~~~~~~~~~~~~~~~~~~~~~~")
			PLAY_SOUND_FROM_ENTITY(-1, "ARM_WRESTLING_WOOD_CREEK_MASTER", PLAYER_PED_ID())
		ENDIF
		
		fScoreInterp = (fLastScore - fUsedScore)/fSCORE_INCREMENTS
		CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling: fScoreInterp set to ", fScoreInterp)
		
		fUsedScore += fScoreInterp
		
		START_TIMER_NOW_SAFE(ArmDebugTimer)
		
	ENDIF
	
	fUsedScoreAdjusted = (fUsedScore/2) + 0.5
	
	#IF IS_DEBUG_BUILD
//	TEXT_LABEL_31 sDebugAWScore[5]
//	sDebugAWScore[0] = "fScore : "
//	sDebugAWScore[0] += GET_STRING_FROM_FLOAT(fScore)
//	DRAW_DEBUG_TEXT_2D(sDebugAWScore[0], <<0.1, 0.66, 0.0>>)
//	
//	sDebugAWScore[0] = "fUsedScoreAdjusted : "
//	sDebugAWScore[0] += GET_STRING_FROM_FLOAT(fUsedScoreAdjusted)
//	DRAW_DEBUG_TEXT_2D(sDebugAWScore[0], <<0.65, 0.5, 0.0>>)
//	
//	sDebugAWScore[3] = "fScoreInterp : "
//	sDebugAWScore[3] += GET_STRING_FROM_FLOAT(fScoreInterp)
//	DRAW_DEBUG_TEXT_2D(sDebugAWScore[3], <<0.65, 0.47, 0.0>>)
//	
//	sDebugAWScore[4] = "fUsedScore : "
//	sDebugAWScore[4] += GET_STRING_FROM_FLOAT(fUsedScore)
//	DRAW_DEBUG_TEXT_2D(sDebugAWScore[4], <<0.65, 0.44, 0.0>>)
//	
//	IF ARM_GET_CLIENT_FLAG(playerData[info.iPlayers[eSelfID]], ARMCLIENTFLAG_NOT_MOVING_STICKS)
//		sDebugAWScore[2] = "NO! sticks"
//	ELSE
//		sDebugAWScore[2] = "YES sticks"
//	ENDIF
//	DRAW_DEBUG_TEXT_2D(sDebugAWScore[2], <<0.1, 0.7, 0.0>>)
	#ENDIF
	
	IF DOES_ENTITY_EXIST(wrestlers[ARMWRESTLERID_HOME].otherWrestler)
	AND NOT IS_PED_INJURED(wrestlers[ARMWRESTLERID_HOME].otherWrestler)
		IF IS_TASK_MOVE_NETWORK_ACTIVE(wrestlers[ARMWRESTLERID_HOME].otherWrestler)
			IF (GET_GAME_TIMER() % 1000) < 50
				CDEBUG1LN(DEBUG_ARM_WRESTLING, "am_armwrestling.sc: MoVE network is active and getting updated for ARMWRESTLERID_HOME!")
			ENDIF
			SET_TASK_MOVE_NETWORK_SIGNAL_FLOAT(wrestlers[ARMWRESTLERID_HOME].otherWrestler, "Phase", fUsedScoreAdjusted)
			SET_TASK_MOVE_NETWORK_SIGNAL_FLOAT(wrestlers[ARMWRESTLERID_HOME].otherWrestler, "Wobble", PICK_FLOAT(ARM_SERVER_GET_FLAG(serverData, ARMSERVERFLAG_SCORE_CHANGING), 1, 0))
		ELSE
			IF (GET_GAME_TIMER() % 1000) < 50
				CDEBUG1LN(DEBUG_ARM_WRESTLING, "am_armwrestling.sc: ARMWRESTLERID_HOME MoVE network is not Active!  but score is IS GREATAR THAN 0.05")
			ENDIF
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(wrestlers[ARMWRESTLERID_AWAY].otherWrestler)
	AND NOT IS_PED_INJURED(wrestlers[ARMWRESTLERID_AWAY].otherWrestler)
		IF IS_TASK_MOVE_NETWORK_ACTIVE(wrestlers[ARMWRESTLERID_AWAY].otherWrestler)
			IF (GET_GAME_TIMER() % 1000) < 50
				CDEBUG1LN(DEBUG_ARM_WRESTLING, "am_armwrestling.sc: MoVE network is active and getting updated for ARMWRESTLERID_AWAY!")
			ENDIF
			SET_TASK_MOVE_NETWORK_SIGNAL_FLOAT(wrestlers[ARMWRESTLERID_AWAY].otherWrestler, "Phase", fUsedScoreAdjusted)
			SET_TASK_MOVE_NETWORK_SIGNAL_FLOAT(wrestlers[ARMWRESTLERID_AWAY].otherWrestler, "Wobble", PICK_FLOAT(ARM_SERVER_GET_FLAG(serverData, ARMSERVERFLAG_SCORE_CHANGING), 1, 0))
		ELSE
			IF (GET_GAME_TIMER() % 1000) < 50
				CDEBUG1LN(DEBUG_ARM_WRESTLING, "am_armwrestling.sc: MoVE network is not Active for ARMWRESTLERID_AWAY! but score is IS GREATAR THAN 0.05")
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC




