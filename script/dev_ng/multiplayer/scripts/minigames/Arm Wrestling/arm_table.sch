
//////////////////////////////////////////////////////////////////////
/* arm_table.sch													*/
/* Author: DJ Jones													*/
/* Table definitions for arm wrestling minigame.					*/
//////////////////////////////////////////////////////////////////////


// An arm wrestling table.
STRUCT ARM_TABLE
	VECTOR vCenter
	VECTOR vRight
	VECTOR vForward
	//FLOAT fCenterXDot
	//FLOAT fCenterYDot
	FLOAT fRotation
ENDSTRUCT

FUNC FLOAT GET_ARM_WRESTLING_POS_PREFERENCE(VECTOR vMyPos, VECTOR vTablePos, VECTOR vWrestlePos)
	VECTOR vXAxis, vYAxis
	FLOAT fDotX, fDotY, fEndResult

	vMyPos.z = 0.0
	vTablePos.z = 0.0
	vWrestlePos.z = 0.0

	vYAxis = vWrestlePos - vTablePos
	vYAxis = NORMALISE_VECTOR(vYAxis)

	vXAxis = CROSS_PRODUCT(vYAxis, <<0,0,1>>)

	fDotX = DOT_PRODUCT_XY(vMyPos, vXAxis) - DOT_PRODUCT_XY(vTablePos, vXAxis)
	fDotY = DOT_PRODUCT_XY(vMyPos, vYAxis) - DOT_PRODUCT_XY(vTablePos, vYAxis)
	fEndResult = 1.5 * fDotY / ABSF(fDotX)

	IF fEndResult = 0.0
		CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: GET_ARM_WRESTLING_POS_PREFERENCE was 0.0, returning 0.01 instead.")
		RETURN 0.01
	ENDIF
	
	CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: GET_ARM_WRESTLING_POS_PREFERENCE is: ", fEndResult)
	RETURN fEndResult
ENDFUNC



// Accessor for table's center position.
FUNC VECTOR ARM_GET_TABLE_CENTER(ARM_TABLE& table)
	RETURN table.vCenter
ENDFUNC

// Mutator for table's center position.
PROC ARM_SET_TABLE_CENTER(ARM_TABLE& table, VECTOR vCenter)
	table.vCenter = vCenter
ENDPROC

// Accessor for table's normalized right vector.
FUNC VECTOR ARM_GET_TABLE_RIGHT(ARM_TABLE& table)
	RETURN table.vRight
ENDFUNC

// Mutator for table's normalized right vector.
PROC ARM_SET_TABLE_RIGHT(ARM_TABLE& table, VECTOR vRight)
	table.vRight = vRight
ENDPROC

// Accessor for table's normalized forward vector.
FUNC VECTOR ARM_GET_TABLE_FORWARD(ARM_TABLE& table)
	RETURN table.vForward
ENDFUNC

// Mutator for table's normalized forward vector.
PROC ARM_SET_TABLE_FORWARD(ARM_TABLE& table, VECTOR vForward)
	table.vForward = vForward
ENDPROC

// Accessor for table's center x dot value.
//FUNC FLOAT ARM_GET_TABLE_CENTER_X_DOT(ARM_TABLE& table)
//	RETURN table.fCenterXDot
//ENDFUNC

// Mutator for table's center x dot value.
//PROC ARM_SET_TABLE_CENTER_X_DOT(ARM_TABLE& table, FLOAT fCenterXDot)
//	table.fCenterXDot = fCenterXDot
//ENDPROC

// Accessor for table's center y dot value.
//FUNC FLOAT ARM_GET_TABLE_CENTER_Y_DOT(ARM_TABLE& table)
//	RETURN table.fCenterYDot
//ENDFUNC

// Mutator for table's center x dot value.
//PROC ARM_SET_TABLE_CENTER_Y_DOT(ARM_TABLE& table, FLOAT fCenterYDot)
//	table.fCenterYDot = fCenterYDot
//ENDPROC

// Accessor for table's z-axis rotation.
FUNC FLOAT ARM_GET_TABLE_ROTATION(ARM_TABLE& table)
	RETURN table.fRotation
ENDFUNC

// Mutator for table's z-axis rotation.
PROC ARM_SET_TABLE_ROTATION(ARM_TABLE& table, FLOAT fRotation)
	table.fRotation = fRotation
ENDPROC

// Accessor for the altitude of the table surface.
FUNC FLOAT ARM_GET_TABLE_ALTITUDE(ARM_TABLE& table)
	RETURN table.vCenter.z
ENDFUNC
