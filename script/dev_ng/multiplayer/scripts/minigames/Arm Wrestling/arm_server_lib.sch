
//////////////////////////////////////////////////////////////////////
/* arm_server_lib.sch												*/
/* Author: DJ Jones													*/
/* Server functionality for arm wrestling minigame.					*/
//////////////////////////////////////////////////////////////////////


PROC INITIALIZE_ARM_PARTICIPANT_INFO( ARM_PARTICIPANT_INFO & info )
	
	INT i = 0
	FOR i = 0 TO ( ENUM_TO_INT(ARMWRESTLERIDS) - 1 )
		info.iPlayers[i] = iARM_INVALID_PARTICIPANT_ID
	ENDFOR
	
	i = 0
	FOR i = 0 TO ( iARM_MAX_SCTV - 1 )
		info.iSCTV[i] = iARM_INVALID_PARTICIPANT_ID
	ENDFOR	
	
	info.iPlayerCount = 0
	info.iSCTVCount = 0

ENDPROC


/// PURPOSE:
///    
/// PARAMS:
///    clientBD - []
///    info - 
PROC UPDATE_ARM_PARTICIPANT_INFO(ARM_PARTICIPANT_INFO & info)
	
	INITIALIZE_ARM_PARTICIPANT_INFO( info )

	INT i = 0
	PARTICIPANT_INDEX participantID
	PLAYER_INDEX playerID
	
	FOR i = 0 TO ( iMAX_ARM_PARTICIPANTS - 1 )
		participantID = INT_TO_PARTICIPANTINDEX( i )
		IF NETWORK_IS_PARTICIPANT_ACTIVE( participantID )
			playerID = NETWORK_GET_PLAYER_INDEX( participantID )
			IF NETWORK_IS_PLAYER_ACTIVE( playerID )
				IF IS_PLAYER_SCTV( playerID )
					IF info.iSCTVCount < iARM_MAX_SCTV
						info.iSCTV[info.iSCTVCount] = i
						CDEBUG3LN(DEBUG_ARM_WRESTLING, "UPDATE_ARM_PARTICIPANT_INFO :: info.iSCTV[", info.iSCTVCount, "] = ", i)
						info.iSCTVCount++
					ELSE
						CDEBUG3LN(DEBUG_ARM_WRESTLING, "UPDATE_ARM_PARTICIPANT_INFO :: info.iSCTVCount = ", info.iSCTVCount, " something went wrong! Not updating the info table.")
					ENDIF
				ELSE
					IF info.iPlayerCount < ENUM_TO_INT(ARMWRESTLERIDS)
						info.iPlayers[info.iPlayerCount] = i
						CDEBUG3LN(DEBUG_ARM_WRESTLING, "UPDATE_ARM_PARTICIPANT_INFO :: info.iPlayers[", info.iPlayerCount, "] = ", i)
						info.iPlayerCount++
					ELSE
						CDEBUG3LN(DEBUG_ARM_WRESTLING, "UPDATE_ARM_PARTICIPANT_INFO :: info.iPlayerCount = ", info.iPlayerCount, " something went wrong! Not updating the info table.")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
ENDPROC

PROC PRINT_ARM_PARTICIPANT_INFO( ARM_PARTICIPANT_INFO & info )
	CDEBUG2LN(DEBUG_ARM_WRESTLING, "PRINT_ARM_PARTICIPANT_INFO" )
	
	INT i = 0
	FOR i = 0 TO ( ENUM_TO_INT(ARMWRESTLERIDS) - 1 )
		IF info.iPlayers[i] != iARM_INVALID_PARTICIPANT_ID
			CDEBUG1LN(DEBUG_ARM_WRESTLING, "Player ", i, " : ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(info.iPlayers[i]))))
		ENDIF
	ENDFOR	
	
	i = 0
	FOR i = 0 TO ( iARM_MAX_SCTV - 1 )
		IF info.iSCTV[i] != iARM_INVALID_PARTICIPANT_ID
			CDEBUG1LN(DEBUG_ARM_WRESTLING, "SCTV ", i, " : ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(info.iSCTV[i]))))
		ENDIF
	ENDFOR	
	
ENDPROC

FUNC BOOL ARM_IS_PARTICIPANT_INT_A_PLAYER( ARM_PARTICIPANT_INFO & info, INT participantID )

	IF NOT NETWORK_IS_PARTICIPANT_ACTIVE( INT_TO_PARTICIPANTINDEX( participantID ) )
		RETURN FALSE
	ENDIF	

	INT i = 0
	FOR i = 0 TO ( ENUM_TO_INT(ARMWRESTLERIDS) - 1 )
		IF info.iPlayers[i] = participantID
			RETURN TRUE
		ENDIF
	ENDFOR	
	
	RETURN FALSE

ENDFUNC

FUNC BOOL ARM_IS_PARTICIPANT_A_PLAYER( ARM_PARTICIPANT_INFO & info, PARTICIPANT_INDEX participantID )

	IF NOT NETWORK_IS_PARTICIPANT_ACTIVE( participantID )
		RETURN FALSE
	ENDIF	
		
	RETURN ARM_IS_PARTICIPANT_INT_A_PLAYER( info, NATIVE_TO_INT( participantID ) )

ENDFUNC

FUNC BOOL ARM_IS_PLAYER_A_PLAYER( ARM_PARTICIPANT_INFO & info, PLAYER_INDEX playerID )
	
	IF NOT NETWORK_IS_PLAYER_ACTIVE( playerID )
		RETURN FALSE
	ENDIF	
	
	IF NOT NETWORK_IS_PLAYER_A_PARTICIPANT( playerID )
		RETURN FALSE
	ENDIF
		
	RETURN ARM_IS_PARTICIPANT_A_PLAYER( info, NETWORK_GET_PARTICIPANT_INDEX( playerID ) )

ENDFUNC

FUNC INT ARM_GET_NUM_WRESTLERS( ARM_PARTICIPANT_INFO & info )
	RETURN info.iPlayerCount
ENDFUNC

FUNC INT ARM_GET_OPPOSING_PLAYER( ARM_PARTICIPANT_INFO & info )
	IF NOT ARM_IS_PARTICIPANT_INT_A_PLAYER( info, PARTICIPANT_ID_TO_INT() )
		RETURN info.iPlayers[0]
	ENDIF

	INT i = 0
	FOR i = 0 TO ( ENUM_TO_INT(ARMWRESTLERIDS) - 1 )
		IF info.iPlayers[i] != PARTICIPANT_ID_TO_INT()
			AND info.iPlayers[i] != iARM_INVALID_PARTICIPANT_ID
			RETURN info.iPlayers[i]
		ENDIF
	ENDFOR	
	CERRORLN(DEBUG_ARM_WRESTLING, "ARM_GET_OPPOSING_PLAYER :: Returning info.iPlayers[0]")
	RETURN info.iPlayers[0]
	
ENDFUNC

FUNC INT ARM_GET_MY_PLAYER_PARTICIPANT_INFO_INDEX( ARM_PARTICIPANT_INFO & info )
	IF NOT ARM_IS_PARTICIPANT_INT_A_PLAYER( info, PARTICIPANT_ID_TO_INT() )
		RETURN info.iPlayers[0]
	ENDIF

	INT i = 0
	FOR i = 0 TO ( ENUM_TO_INT(ARMWRESTLERIDS) - 1 )
		IF info.iPlayers[i] = PARTICIPANT_ID_TO_INT()
			AND info.iPlayers[i] != iARM_INVALID_PARTICIPANT_ID
			RETURN i
		ENDIF
	ENDFOR	
	CERRORLN(DEBUG_ARM_WRESTLING, "ARM_GET_MY_PLAYER_PARTICIPANT_INFO_INDEX :: Returning info.iPlayers[0]")
	RETURN 0
	
ENDFUNC

FUNC INT ARM_GET_OPPOSING_PLAYER_PARTICIPANT_INFO_INDEX( ARM_PARTICIPANT_INFO & info )
	IF NOT ARM_IS_PARTICIPANT_INT_A_PLAYER( info, PARTICIPANT_ID_TO_INT() )
		RETURN info.iPlayers[0]
	ENDIF

	INT i = 0
	FOR i = 0 TO ( ENUM_TO_INT(ARMWRESTLERIDS) - 1 )
		IF info.iPlayers[i] != PARTICIPANT_ID_TO_INT()
			AND info.iPlayers[i] != iARM_INVALID_PARTICIPANT_ID
			RETURN i
		ENDIF
	ENDFOR	
	CERRORLN(DEBUG_ARM_WRESTLING, "ARM_GET_OPPOSING_PLAYER_PARTICIPANT_INFO_INDEX :: Returning info.iPlayers[0]")
	RETURN 0
	
ENDFUNC

FUNC INT ARM_GET_OTHER_PARTICIPANT( INT iOurClientID, ARM_PARTICIPANT_INFO & info )
	IF NOT ARM_IS_PARTICIPANT_INT_A_PLAYER( info, iOurClientID )
		RETURN info.iPlayers[0]
	ENDIF

	INT i = 0
	FOR i = 0 TO ( ENUM_TO_INT(ARMWRESTLERIDS) - 1 )
		IF info.iPlayers[i] != iOurClientID
			AND info.iPlayers[i] != iARM_INVALID_PARTICIPANT_ID
			RETURN info.iPlayers[i]
		ENDIF
	ENDFOR	
	CERRORLN(DEBUG_ARM_WRESTLING, "ARM_GET_OTHER_PARTICIPANT :: Returning info.iPlayers[0]")
	RETURN info.iPlayers[0]
	
ENDFUNC

FUNC INT ARM_GET_OTHER_PARTICIPANT_PARTICIPANT_INFO_INDEX( INT iOurClientID, ARM_PARTICIPANT_INFO & info )
	IF NOT ARM_IS_PARTICIPANT_INT_A_PLAYER( info, iOurClientID )
		RETURN info.iPlayers[0]
	ENDIF

	INT i = 0
	FOR i = 0 TO ( ENUM_TO_INT(ARMWRESTLERIDS) - 1 )
		IF info.iPlayers[i] != iOurClientID
			AND info.iPlayers[i] != iARM_INVALID_PARTICIPANT_ID
			RETURN i
		ENDIF
	ENDFOR	
	CERRORLN(DEBUG_ARM_WRESTLING, "ARM_GET_OTHER_PARTICIPANT_PARTICIPANT_INFO_INDEX :: Returning info.iPlayers[0]")
	RETURN info.iPlayers[0]
	
ENDFUNC

FUNC ARM_WRESTLER_ID ARM_GET_PARTICIPANT_ARM_PLAYER_ID( INT iParticipantID, ARM_PARTICIPANT_INFO & info )
	IF NOT ARM_IS_PARTICIPANT_INT_A_PLAYER( info, iParticipantID )
		RETURN ARMWRESTLERIDS
	ENDIF
	
	IF NETWORK_GET_HOST_OF_THIS_SCRIPT() = INT_TO_PARTICIPANTINDEX( iParticipantID )
		RETURN ARMWRESTLERID_HOME
	ENDIF

	RETURN ARMWRESTLERID_AWAY
	
ENDFUNC


FUNC BOOL ARM_IS_PARTICIPANT_INT_SCTV( ARM_PARTICIPANT_INFO & info, INT participantID )

	IF NOT NETWORK_IS_PARTICIPANT_ACTIVE( INT_TO_PARTICIPANTINDEX( participantID ) )
		RETURN FALSE
	ENDIF	

	INT i = 0
	FOR i = 0 TO ( iARM_MAX_SCTV - 1 )
		IF info.iSCTV[i] = participantID
			RETURN TRUE
		ENDIF
	ENDFOR	
	
	RETURN FALSE

ENDFUNC

FUNC BOOL ARM_IS_PARTICIPANT_SCTV( ARM_PARTICIPANT_INFO & info, PARTICIPANT_INDEX participantID )

	IF NOT NETWORK_IS_PARTICIPANT_ACTIVE( participantID )
		RETURN FALSE
	ENDIF	
		
	RETURN ARM_IS_PARTICIPANT_INT_SCTV( info, NATIVE_TO_INT( participantID ) )

ENDFUNC

FUNC BOOL ARM_IS_PLAYER_SCTV( ARM_PARTICIPANT_INFO & info, PLAYER_INDEX playerID )
	
	IF NOT NETWORK_IS_PLAYER_ACTIVE( playerID )
		RETURN FALSE
	ENDIF	
	
	IF NOT NETWORK_IS_PLAYER_A_PARTICIPANT( playerID )
		RETURN FALSE
	ENDIF
		
	RETURN ARM_IS_PARTICIPANT_SCTV( info, NETWORK_GET_PARTICIPANT_INDEX( playerID ) )

ENDFUNC

FUNC INT ARM_GET_NUM_SCTV( ARM_PARTICIPANT_INFO & info )
	RETURN info.iSCTVCount
ENDFUNC

// Look for reasons to terminate the minigame session. Return FALSE on terminate.
FUNC BOOL ARM_SERVER_UPDATE_RUN_CONDITIONS(ARM_SERVER_BROADCAST_DATA& serverData, ARM_MP_TERMINATE_REASON& terminateReason, ARM_PLAYER_BROADCAST_DATA & playerData[], ARM_PARTICIPANT_INFO & info)
	// Are we terminating due to only having 1 player?
	IF (GET_CURRENT_GAMEMODE() = GAMEMODE_FM AND ARM_GET_NUM_WRESTLERS( info ) < ENUM_TO_INT(ARMWRESTLERIDS))
		IF NOT ARM_SERVER_GET_FLAG(serverData, ARMSERVERFLAG_QUIT_1CLIENT)
			CDEBUG1LN(DEBUG_ARM_WRESTLING, "There is only one player, setting a quit flag.")
			ARM_SERVER_SET_FLAG(serverData, ARMSERVERFLAG_QUIT_1CLIENT, TRUE)
			ARM_SET_SERVER_MP_STATE(serverData, ARMMPSTATE_TERMINATE_SPLASH)
			IF info.iPlayers[ARMWRESTLERID_HOME] > -1
			AND info.iPlayers[ARMWRESTLERID_AWAY] > -1 
			AND (  ARM_GET_SERVER_GAME_STATE(serverData) = ARMGAMESTATE_WINNER
				OR ARM_GET_PLAYER_MP_STATE(playerData[info.iPlayers[ARMWRESTLERID_HOME]]) = ARMMPSTATE_WAIT_POST_GAME
				OR ARM_GET_PLAYER_MP_STATE(playerData[info.iPlayers[ARMWRESTLERID_AWAY]]) = ARMMPSTATE_WAIT_POST_GAME	)
					CDEBUG1LN(DEBUG_ARM_WRESTLING, "game had already ended, setting terminate reason to ENDGAME")
					terminateReason = ARMMPTERMINATEREASON_ENDGAME
			ELSE
				CDEBUG1LN(DEBUG_ARM_WRESTLING, "game was still in play, setting terminate reason to PLAYERLEFT")
				terminateReason = ARMMPTERMINATEREASON_PLAYERLEFT
			ENDIF
			
			RETURN FALSE
		ENDIF
	ELSE
//		IF (GET_GAME_TIMER() % 1000) < 50
//			CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling: SERVER RUN CONDITION SAYS NUM PARTICIPANTS = ", NETWORK_GET_NUM_PARTICIPANTS())
//		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

// Monitor score conditions and return a winner if we have one.
FUNC ARM_WRESTLER_ID ARM_SERVER_UPDATE_SCORING(ARM_SERVER_BROADCAST_DATA& serverData, ARM_PLAYER_BROADCAST_DATA& playerData[], ARM_PARTICIPANT_INFO & info)
	FLOAT fScores[ARMWRESTLERIDS]
	FLOAT fScoreChange, fScore
	INT iBits
	INT i, iUpperLimit
	BOOL bBucketCollected, bLoopAround
	
	
	
	// Take points out of our local buckets and emtpy them.
	REPEAT iSCORE_BUCKETS i
		fScore = ARM_GET_SCORE_BUCKET(playerData[info.iPlayers[eSelfID]], i)
		IF fScore > 0.0
			fScores[eSelfID] += fScore
			ARM_SET_SCORE_BUCKET(playerData[info.iPlayers[eSelfID]], i, 0.0)
			//CDEBUG1LN(DEBUG_ARM_WRESTLING, "server update scoring - self.      fscore = ", fScore, ".     fscores[eselfID] = ", fScores[info.iPlayers[eSelfID]], ". bucket = ", i)
		ENDIF
	ENDREPEAT
	
	// See if we need to loop around the remote client's bucket list.
	bLoopAround = ARM_GET_CURRENT_BUCKET(playerData[info.iPlayers[eOpponentID]]) < ARM_GET_SERVER_CLIENT_BUCKET(serverData)
	
	// Loop through to the remote client's last FINISHED bucket, or to the end of the list.
	iUpperLimit = PICK_INT(bLoopAround, iSCORE_BUCKETS, ARM_GET_CURRENT_BUCKET(playerData[info.iPlayers[eOpponentID]]))
	i = ARM_GET_SERVER_CLIENT_BUCKET(serverData)
	WHILE i < iUpperLimit
		fScore = ARM_GET_SCORE_BUCKET(playerData[info.iPlayers[eOpponentID]], i)
		IF fScore > 0.0
			fScores[eOpponentID] += fScore
			//ARM_SET_SCORE_BUCKET(playerData[info.iPlayers[eOpponentID]], i, 0.0)
			SET_BIT(iBits, i)
			bBucketCollected = TRUE
			CDEBUG1LN(DEBUG_ARM_WRESTLING, "server update scoring - opponent.  fscore = ", fScore, ". fscores[eopponentID] = ", fScores[eOpponentID], ". bucket = ", i)
		ENDIF
		i += 1
		ARM_SET_SERVER_CLIENT_BUCKET(serverData, i)
	ENDWHILE
	
	// If we needed to loop around, loop to the last finished bucket.
	IF bLoopAround
		ARM_SET_SERVER_CLIENT_BUCKET(serverData, 0)
		REPEAT ARM_GET_CURRENT_BUCKET(playerData[info.iPlayers[eOpponentID]]) i
			fScore = ARM_GET_SCORE_BUCKET(playerData[info.iPlayers[eOpponentID]], i)
			IF fScore > 0.0
				fScores[eOpponentID] += fScore
				//ARM_SET_SCORE_BUCKET(playerData[info.iPlayers[eOpponentID]], i, 0.0)
				SET_BIT(iBits, i)
				bBucketCollected = TRUE
				CDEBUG1LN(DEBUG_ARM_WRESTLING, "server update scoring - opponent.  fscore = ", fScore, ". fscores[eopponentID] = ", fScores[eOpponentID], ". bucket = ", i)
			ENDIF
		ENDREPEAT
	ENDIF
	
	// Send a message to the remote client telling him to reset his buckets to zero.
	IF bBucketCollected
		BROADCAST_ARM_WRESTLE_BUCKET_COLLECTION(iBits, playerData[info.iPlayers[eOpponentID]].curPlayerID)
	ENDIF
	
	
	IF fScores[ARMWRESTLERID_HOME] > 0
	AND ARM_GET_CLIENT_FLAG(playerData[info.iPlayers[ARMWRESTLERID_HOME]], ARMCLIENTFLAG_NOT_MOVING_STICKS)
		fScores[ARMWRESTLERID_HOME] = 0
		CDEBUG1LN(DEBUG_ARM_WRESTLING, "server update scoring - ARMWRESTLERID_HOME not moving sticks, not registering the score")
	ELIF fScores[ARMWRESTLERID_HOME] > 0
	AND NOT ARM_GET_CLIENT_FLAG(playerData[info.iPlayers[ARMWRESTLERID_HOME]], ARMCLIENTFLAG_NOT_MOVING_STICKS)
		CDEBUG1LN(DEBUG_ARM_WRESTLING, "server update scoring - before strength, ARMWRESTLERID_HOME score = ", fScores[ARMWRESTLERID_HOME])
		fScores[ARMWRESTLERID_HOME] = fScores[ARMWRESTLERID_HOME] * playerData[info.iPlayers[ARMWRESTLERID_HOME]].fStrengthFactor
		CDEBUG1LN(DEBUG_ARM_WRESTLING, "server update scoring - after  strength, ARMWRESTLERID_HOME score = ", fScores[ARMWRESTLERID_HOME])
	ENDIF
	
	IF fScores[ARMWRESTLERID_AWAY] > 0
	AND ARM_GET_CLIENT_FLAG(playerData[info.iPlayers[ARMWRESTLERID_AWAY]], ARMCLIENTFLAG_NOT_MOVING_STICKS)
		fScores[ARMWRESTLERID_AWAY] = 0
		CDEBUG1LN(DEBUG_ARM_WRESTLING, "server update scoring - ARMWRESTLERID_AWAY not moving sticks, not registering the score")
	ELIF fScores[ARMWRESTLERID_AWAY] > 0
	AND NOT ARM_GET_CLIENT_FLAG(playerData[info.iPlayers[ARMWRESTLERID_AWAY]], ARMCLIENTFLAG_NOT_MOVING_STICKS)
		CDEBUG1LN(DEBUG_ARM_WRESTLING, "server update scoring - before strength, ARMWRESTLERID_AWAY score = ", fScores[ARMWRESTLERID_AWAY])
		fScores[ARMWRESTLERID_AWAY] = fScores[ARMWRESTLERID_AWAY] * playerData[info.iPlayers[ARMWRESTLERID_AWAY]].fStrengthFactor
		CDEBUG1LN(DEBUG_ARM_WRESTLING, "server update scoring - after  strength, ARMWRESTLERID_AWAY score = ", fScores[ARMWRESTLERID_AWAY])
	ENDIF
	
	IF ARM_GET_SERVER_SCORE(serverData) > 0.75
	AND fScores[ARMWRESTLERID_HOME] != 0
		CDEBUG1LN(DEBUG_ARM_WRESTLING, "server update scoring - padding the score since their past halfway")
		CDEBUG1LN(DEBUG_ARM_WRESTLING, "fScores[ARMWRESTLERID_HOME] was = ", fScores[ARMWRESTLERID_HOME])
		fScores[ARMWRESTLERID_HOME] = fScores[ARMWRESTLERID_HOME] * 0.85
		CDEBUG1LN(DEBUG_ARM_WRESTLING, "fScores[ARMWRESTLERID_HOME] * 0.85 = ", fScores[ARMWRESTLERID_HOME])
	
	ELIF ARM_GET_SERVER_SCORE(serverData) < -0.75
	AND fScores[ARMWRESTLERID_AWAY] != 0
		CDEBUG1LN(DEBUG_ARM_WRESTLING, "server update scoring - padding the score since their past halfway")
		CDEBUG1LN(DEBUG_ARM_WRESTLING, "fScores[ARMWRESTLERID_AWAY] was = ", fScores[ARMWRESTLERID_AWAY])
		fScores[ARMWRESTLERID_AWAY] = fScores[ARMWRESTLERID_AWAY] * 0.85
		CDEBUG1LN(DEBUG_ARM_WRESTLING, "fScores[ARMWRESTLERID_AWAY] * 0.85 = ", fScores[ARMWRESTLERID_AWAY])
	ENDIF
	
	
	fScoreChange = fSCORE_CHANGE_SCALAR * (fScores[ARMWRESTLERID_HOME] - fScores[ARMWRESTLERID_AWAY])
	
//	IF (ARM_GET_SERVER_SCORE(serverData) > 0.6
//		OR ARM_GET_SERVER_SCORE(serverData) < -0.6)
//	AND fScoreChange != 0
//		CDEBUG1LN(DEBUG_ARM_WRESTLING, "server update scoring - padding the score since their past halfway")
//		CDEBUG1LN(DEBUG_ARM_WRESTLING, "fScoreChange was = ", fScoreChange)
//		fScoreChange = fScoreChange * 0.65
//		CDEBUG1LN(DEBUG_ARM_WRESTLING, "fScoreChange * 0.65 = ", fScoreChange)
//	ENDIF
	
	//CDEBUG1LN(DEBUG_ARM_WRESTLING, "server update scoring - fScores[ARMWRESTLERID_HOME] = ", fScores[ARMWRESTLERID_HOME])
	//CDEBUG1LN(DEBUG_ARM_WRESTLING, "server update scoring - fScores[ARMWRESTLERID_AWAY] = ", fScores[ARMWRESTLERID_AWAY])
	CDEBUG1LN(DEBUG_ARM_WRESTLING, "server update scoring - fScoreChange                = ", fScoreChange)
	
	IF fScoreChange = 0
	AND fScores[ARMWRESTLERID_HOME] = 0
	AND fScores[ARMWRESTLERID_AWAY] = 0
		IF ARM_GET_SERVER_SCORE(serverData) < 0
			IF (ARM_GET_SERVER_SCORE(serverData) + 0.1) <= 0
				IF ARM_GET_SERVER_SCORE(serverData) < -1
					fScoreChange = -1 - ARM_GET_SERVER_SCORE(serverData) + 0.1
				ELSE
					fScoreChange += 0.1
				ENDIF
			ELSE
				fScoreChange -= ARM_GET_SERVER_SCORE(serverData)
			ENDIF
			CDEBUG1LN(DEBUG_ARM_WRESTLING, "fScoreChange has to trend towards 0 from -1, it is now = ", fScoreChange)
			CDEBUG1LN(DEBUG_ARM_WRESTLING, "actual score is = ", ARM_GET_SERVER_SCORE(serverData))
		ELIF ARM_GET_SERVER_SCORE(serverData) > 0
			IF (ARM_GET_SERVER_SCORE(serverData) - 0.1) >= 0
				IF ARM_GET_SERVER_SCORE(serverData) > 1
					fScoreChange = 1 - ARM_GET_SERVER_SCORE(serverData) - 0.1
				ELSE
					fScoreChange -= 0.1
				ENDIF
			ELSE
				fScoreChange -= ARM_GET_SERVER_SCORE(serverData)
			ENDIF
			CDEBUG1LN(DEBUG_ARM_WRESTLING, "fScoreChange has to trend towards 0 from 1, it is now = ", fScoreChange)
			CDEBUG1LN(DEBUG_ARM_WRESTLING, "actual score is = ", ARM_GET_SERVER_SCORE(serverData))
		ENDIF
		
		IF ARM_SERVER_GET_FLAG(serverData, ARMSERVERFLAG_SCORE_CHANGING)
			ARM_SERVER_SET_FLAG(serverData, ARMSERVERFLAG_SCORE_CHANGING, FALSE)
			CDEBUG1LN(DEBUG_ARM_WRESTLING, "score change server flag was unset")
		ENDIF
		
	ELSE
		IF NOT ARM_SERVER_GET_FLAG(serverData, ARMSERVERFLAG_SCORE_CHANGING)
			ARM_SERVER_SET_FLAG(serverData, ARMSERVERFLAG_SCORE_CHANGING, TRUE)
			CDEBUG1LN(DEBUG_ARM_WRESTLING, "score change server flag was set")
		ENDIF
	ENDIF
	
	ARM_UPDATE_CURRENT_SCORE(serverData, fScoreChange)
	
	ARM_ADD_TIMESTAMPED_SCORE(serverData, GET_NETWORK_TIME(), ARM_GET_SERVER_SCORE(serverData))
	
	IF (NOT ARM_GET_CLIENT_FLAG(playerData[info.iPlayers[eSelfID]], ARMCLIENTFLAG_NOT_MOVING_STICKS)
	OR NOT ARM_GET_CLIENT_FLAG(playerData[info.iPlayers[eOpponentID]], ARMCLIENTFLAG_NOT_MOVING_STICKS))
		IF ARM_GET_SERVER_SCORE(serverData) <= -1.0
			IF NOT ARM_SERVER_GET_FLAG(serverData, ARMSERVERFLAG_WIN_REACHED)
				CDEBUG1LN(DEBUG_ARM_WRESTLING, "away win reached, setting server flag win reached")
				ARM_SERVER_SET_FLAG(serverData, ARMSERVERFLAG_WIN_REACHED, TRUE)
			ENDIF
			RETURN ARMWRESTLERID_AWAY
		ELIF ARM_GET_SERVER_SCORE(serverData) >= 1.0
			IF NOT ARM_SERVER_GET_FLAG(serverData, ARMSERVERFLAG_WIN_REACHED)
				CDEBUG1LN(DEBUG_ARM_WRESTLING, "home win reached, setting server flag win reached")
				ARM_SERVER_SET_FLAG(serverData, ARMSERVERFLAG_WIN_REACHED, TRUE)
			ENDIF
			RETURN ARMWRESTLERID_HOME
		ENDIF
	ELSE
		IF (ARM_GET_SERVER_SCORE(serverData) > -1.0
			OR ARM_GET_SERVER_SCORE(serverData) < 1.0)
		AND ARM_SERVER_GET_FLAG(serverData, ARMSERVERFLAG_WIN_REACHED)
			CDEBUG1LN(DEBUG_ARM_WRESTLING, "both players aren't moving sticks, resetting win reach flag")
			ARM_SERVER_SET_FLAG(serverData, ARMSERVERFLAG_WIN_REACHED, FALSE)
		ENDIF
	ENDIF
	
	RETURN ARMWRESTLERIDS
ENDFUNC

// Responsible for monitoring the actual game and updating the clients and game based on what is happening.
PROC ARM_SERVER_UPDATE_GAME(ARM_SERVER_BROADCAST_DATA& serverData, ARM_PLAYER_BROADCAST_DATA& playerData[], ARM_PARTICIPANT_INFO & info, BOOL bApartments = FALSE)
	
	PLAYER_INDEX broadcastIndex
	//TEXT_LABEL_23 sAnimDict = "mini@arm_wrestling"
	//TEXT_LABEL_23 sRefOutroAnim = "ref_outro"
	
	// Make sure the clients and server are all running the game first.
	IF  ARM_GET_PLAYER_MP_STATE(playerData[info.iPlayers[ARMWRESTLERID_AWAY]]) = ARMMPSTATE_RUNNING
	AND ARM_GET_PLAYER_MP_STATE(playerData[info.iPlayers[ARMWRESTLERID_HOME]]) = ARMMPSTATE_RUNNING
		IF ARM_GET_SERVER_GAME_STATE(serverData) = ARMGAMESTATE_PLAYING
			IF NOT ARM_IS_SCORE_TIMER_STARTED(serverData)
				ARM_RESET_SCORE_TIMER(serverData)
			ENDIF
			
		#IF IS_DEBUG_BUILD
			FLOAT fScore, fScore2, fScore3
			TEXT_LABEL_31 sDebugBucketScores, sDebugBucketScores2, sDebugBucketScores3, sDebugBucketScores4
			INT i
			
			REPEAT iSCORE_BUCKETS i
				fScore = ARM_GET_SCORE_BUCKET(playerData[info.iPlayers[eSelfID]], i)
				sDebugBucketScores = "Bucket "
				sDebugBucketScores += GET_STRING_FROM_INT(i)
				sDebugBucketScores += ": "
				sDebugBucketScores += GET_STRING_FROM_FLOAT(fScore)
				//ARM_SET_SCORE_BUCKET(playerData[info.iPlayers[eSelfID]], i, 0.0)
				
				DRAW_DEBUG_TEXT_2D(sDebugBucketScores, <<0.065, (0.065 + (0.02*i)), 0.0>>)
				
				fScore2 = ARM_GET_SCORE_BUCKET(playerData[info.iPlayers[eOpponentID]], i)
				sDebugBucketScores2 = "Bucket "
				sDebugBucketScores2 += GET_STRING_FROM_INT(i)
				sDebugBucketScores2 += ": "
				sDebugBucketScores2 += GET_STRING_FROM_FLOAT(fScore2)
				//ARM_SET_SCORE_BUCKET(playerData[info.iPlayers[eSelfID]], i, 0.0)
				
				DRAW_DEBUG_TEXT_2D(sDebugBucketScores2, <<0.2, (0.065 + (0.02*i)), 0.0>>)
				
				IF fScore > 0 OR fScore2 > 0
					sDebugBucketScores3 = GET_STRING_FROM_FLOAT(fScore - fScore2)
					DRAW_DEBUG_TEXT_2D(sDebugBucketScores3, <<0.35, (0.065 + (0.02*i)), 0.0>>)
				ENDIF
			ENDREPEAT
			
			REPEAT ARM_SCORES i
				fScore3 = serverData.fScores[i]
				sDebugBucketScores4 = "Server score "
				sDebugBucketScores4 += GET_STRING_FROM_INT(i)
				sDebugBucketScores4 += ": "
				sDebugBucketScores4 += GET_STRING_FROM_FLOAT(fScore3)
				DRAW_DEBUG_TEXT_2D(sDebugBucketScores4, <<0.065, (0.5 + (0.02*i)), 0.0>>)
			ENDREPEAT
		#ENDIF
			
			// See if it's time to update the score.
			IF ARM_GET_SCORE_TIMER_VALUE(serverData) >= fSCORE_UPDATE_TIME
			//AND NOT (ARM_SERVER_GET_FLAG(serverData, ARMSERVERFLAG_AWAYWIN) OR ARM_SERVER_GET_FLAG(serverData, ARMSERVERFLAG_HOMEWIN))
				ARM_CANCEL_SCORE_TIMER(serverData)
				
				// Go into the winning state if we have a winner.
				eArmWinner = ARM_SERVER_UPDATE_SCORING(serverData, playerData, info)
//				IF winner = ARMWRESTLERID_AWAY
//					//ARM_ADD_SERVER_WIN(serverData, winner)
//					ARM_SERVER_SET_FLAG(serverData, ARMSERVERFLAG_AWAYWIN, TRUE)
//					//ARM_SET_SERVER_GAME_STATE(serverData, ARMGAMESTATE_WINNER)
//				ELIF winner = ARMWRESTLERID_HOME
//					//ARM_ADD_SERVER_WIN(serverData, winner)
//					ARM_SERVER_SET_FLAG(serverData, ARMSERVERFLAG_HOMEWIN, TRUE)
//					//ARM_SET_SERVER_GAME_STATE(serverData, ARMGAMESTATE_WINNER)
//				ENDIF
			ELSE
				IF (GET_GAME_TIMER() % 2500) < 10
					IF ARM_GET_SCORE_TIMER_VALUE(serverData) < fSCORE_UPDATE_TIME
						CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling: SERVER SAYS score timer is ", ARM_GET_SCORE_TIMER_VALUE(serverData))
					ELSE
						CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling: SERVER SAYS a winner was found")
					ENDIF
				ENDIF
			ENDIF
			
			IF ARM_GET_CLIENT_FLAG(playerData[info.iPlayers[ARMWRESTLERID_AWAY]], ARMCLIENTFLAG_WINNER_SET)
			AND ARM_GET_CLIENT_FLAG(playerData[info.iPlayers[ARMWRESTLERID_HOME]], ARMCLIENTFLAG_WINNER_SET)
			AND ARM_SERVER_GET_FLAG(serverData, ARMSERVERFLAG_WIN_REACHED)
			AND eArmWinner != ARMWRESTLERIDS
				ARM_SET_SERVER_GAME_STATE(serverData, ARMGAMESTATE_WINNER)
				ARM_ADD_SERVER_WIN(serverData, eArmWinner)
				IF eArmWinner = ARMWRESTLERID_AWAY
					ARM_SERVER_SET_FLAG(serverData, ARMSERVERFLAG_AWAYWIN, TRUE)
				ELIF eArmWinner = ARMWRESTLERID_HOME
					ARM_SERVER_SET_FLAG(serverData, ARMSERVERFLAG_HOMEWIN, TRUE)
				ENDIF
				
				// Broadcast an update to all players if we have a winner.
				IF ARM_SERVER_GET_FLAG(serverData, ARMSERVERFLAG_AWAYWIN) OR ARM_SERVER_GET_FLAG(serverData, ARMSERVERFLAG_HOMEWIN)
					broadcastIndex = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(info.iPlayers[eSelfID]))
					CDEBUG1LN(DEBUG_ARM_WRESTLING, "Broadcasting arm wrestle status to all players on team ", GET_PLAYER_TEAM(broadcastIndex))
					BROADCAST_ARM_WRESTLE_UPDATE(
					broadcastIndex, NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(info.iPlayers[eOpponentID])), 
					ARM_GET_SERVER_WINS(serverData, ARMWRESTLERID_AWAY), ARM_GET_SERVER_WINS(serverData, ARMWRESTLERID_HOME), 
					GET_PLAYER_TEAM(broadcastIndex), TRUE, bApartments)
				ENDIF
			ENDIF
		ELSE
			IF (GET_GAME_TIMER() % 2500) < 10
				CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling: SERVER SAYS server state is not ARMGAMESTATE_PLAYING")
			ENDIF
		ENDIF
	ELSE
		IF (GET_GAME_TIMER() % 2500) < 10
			IF  ARM_GET_PLAYER_MP_STATE(playerData[info.iPlayers[ARMWRESTLERID_AWAY]]) != ARMMPSTATE_RUNNING
				CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling: SERVER SAYS AWAY MP state is not ARMMPSTATE_RUNNING")
			ELSE
				CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling: SERVER SAYS HOME MP state is NOT ARMMPSTATE_RUNNING")
			ENDIF
		ENDIF
	ENDIF
	
	// Take care of the referee if he needs to play his outro anim.
	IF ARM_SERVER_GET_FLAG(serverData, ARMSERVERFLAG_REF_OUTRO_REQUESTED)
	AND (NOT ARM_SERVER_GET_FLAG(serverData, ARMSERVERFLAG_REF_OUTRO_PLAYED))
		//ARM_PLAY_CROWD_ANIMATION(crowd, ARMCROWDMEMBER_REFEREE, sAnimDict, sRefOutroAnim, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, AF_HOLD_LAST_FRAME | AF_NOT_INTERRUPTABLE, 0.0, TRUE)
		ARM_SERVER_SET_FLAG(serverdata, ARMSERVERFLAG_REF_OUTRO_PLAYED, TRUE)
	ENDIF
	
	// If both clients are in the wait for server state, kick them into the game.
	IF  ARM_GET_PLAYER_MP_STATE(playerData[info.iPlayers[ARMWRESTLERID_AWAY]]) = ARMMPSTATE_WAIT_TO_START_GAME
	AND ARM_GET_PLAYER_MP_STATE(playerData[info.iPlayers[ARMWRESTLERID_HOME]]) = ARMMPSTATE_WAIT_TO_START_GAME
	AND ARM_GET_SERVER_GAME_STATE(serverData) != ARMGAMESTATE_PLAYING
		// When the clients pick up the flag to move, they will be in the server's playing state, where gameplay happens.
		ARM_SET_SERVER_GAME_STATE(serverData, ARMGAMESTATE_PLAYING)
		ARM_SERVER_SET_FLAG(serverData, ARMSERVERFLAG_MOVETOGAME, TRUE)
		ARM_SERVER_SET_FLAG(serverData, ARMSERVERFLAG_MOVETOMENU, FALSE)
		ARM_SERVER_SET_FLAG(serverData, ARMSERVERFLAG_AWAYWIN, FALSE)
		ARM_SERVER_SET_FLAG(serverData, ARMSERVERFLAG_HOMEWIN, FALSE)
		ARM_CLEAR_SERVER_SCORES(serverData)
		ARM_CLEAR_BUCKETS(playerData[info.iPlayers[eSelfID]])
	ENDIF
	
	// If both clients are in the wait for server state, kick them into the game.
	IF  ARM_GET_PLAYER_MP_STATE(playerData[info.iPlayers[ARMWRESTLERID_AWAY]]) = ARMMPSTATE_WAIT_POST_GAME
	AND ARM_GET_PLAYER_MP_STATE(playerData[info.iPlayers[ARMWRESTLERID_HOME]]) = ARMMPSTATE_WAIT_POST_GAME
	AND ARM_GET_CLIENT_FLAG(playerData[info.iPlayers[ARMWRESTLERID_AWAY]], ARMCLIENTFLAG_POST_ANIM_DONE)
	AND ARM_GET_CLIENT_FLAG(playerData[info.iPlayers[ARMWRESTLERID_HOME]], ARMCLIENTFLAG_POST_ANIM_DONE)
		// When both clients are waiting after the game, server tells them to go to menu, sets itself on the menu.
		ARM_SERVER_SET_FLAG(serverData, ARMSERVERFLAG_MOVETOMENU, TRUE)
		ARM_SERVER_SET_FLAG(serverData, ARMSERVERFLAG_MOVETOGAME, FALSE)
		ARM_SERVER_SET_FLAG(serverData, ARMSERVERFLAG_AWAYWIN, FALSE)
		ARM_SERVER_SET_FLAG(serverData, ARMSERVERFLAG_HOMEWIN, FALSE)
		ARM_SERVER_SET_FLAG(serverData, ARMSERVERFLAG_REF_OUTRO_REQUESTED, FALSE)
		ARM_SERVER_SET_FLAG(serverData, ARMSERVERFLAG_REF_OUTRO_PLAYED, FALSE)
		ARM_CLEAR_SERVER_SCORES(serverData)
		ARM_CLEAR_BUCKETS(playerData[info.iPlayers[eSelfID]])
		ARM_SET_SERVER_GAME_STATE(serverData, ARMGAMESTATE_READYMENU)
	ELSE
		IF (GET_GAME_TIMER() % 2500) < 10
			CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling: SERVER SAYS rematch is not wanted yet")
			IF ARM_GET_PLAYER_MP_STATE(playerData[info.iPlayers[ARMWRESTLERID_AWAY]]) != ARMMPSTATE_WAIT_POST_GAME
				CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling: SERVER SAYS AWAY MP STATE is NOT ARMMPSTATE_WAIT_POST_GAME")
			ELIF ARM_GET_PLAYER_MP_STATE(playerData[info.iPlayers[ARMWRESTLERID_HOME]]) != ARMMPSTATE_WAIT_POST_GAME
				CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling: SERVER SAYS HOME MP STATE is NOT ARMMPSTATE_WAIT_POST_GAME")
			ELIF NOT ARM_GET_CLIENT_FLAG(playerData[info.iPlayers[ARMWRESTLERID_AWAY]], ARMCLIENTFLAG_POST_ANIM_DONE)
				CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling: SERVER SAYS AWAY ARMCLIENTFLAG_POST_ANIM_DONE flag is not set")
			ELIF NOT ARM_GET_CLIENT_FLAG(playerData[info.iPlayers[ARMWRESTLERID_HOME]], ARMCLIENTFLAG_POST_ANIM_DONE)
				CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling: SERVER SAYS HOME ARMCLIENTFLAG_POST_ANIM_DONE flag is not set")
			ENDIF
		ENDIF
	ENDIF
ENDPROC
