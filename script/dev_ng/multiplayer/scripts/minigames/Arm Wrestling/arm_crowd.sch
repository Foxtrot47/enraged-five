
//////////////////////////////////////////////////////////////////////
/* arm_crowd.sch													*/
/* Author: DJ Jones													*/
/* Referee & crowd functionality for arm wrestling minigame.		*/
//////////////////////////////////////////////////////////////////////


// All crowd members, including the referee.
STRUCT ARM_CROWD
	PED_INDEX peds[ARMCROWDMEMBERS]
	OBJECT_INDEX refereeAttachment
	TEXT_LABEL_23 sCurAnim[ARMCROWDMEMBERS]
ENDSTRUCT


// Accessor for ped index.
FUNC PED_INDEX ARM_GET_CROWD_PED_INDEX(ARM_CROWD& crowd, ARM_CROWD_MEMBER crowdMember)
	IF crowdMember = ARMCROWDMEMBERS
		RETURN NULL
	ENDIF
	
	RETURN crowd.peds[crowdMember]
ENDFUNC

// Mutator for ped index.
PROC ARM_SET_CROWD_PED_INDEX(ARM_CROWD& crowd, ARM_CROWD_MEMBER crowdMember, PED_INDEX pedIndex)
	IF crowdMember = ARMCROWDMEMBERS
		EXIT
	ENDIF
	
	crowd.peds[crowdMember] = pedIndex
ENDPROC

// Accessor for referee's attachment object.
FUNC OBJECT_INDEX ARM_GET_REFEREE_ATTACHMENT_OBJECT(ARM_CROWD& crowd)
	RETURN crowd.refereeAttachment
ENDFUNC

// Mutator for referee's attachment object.
PROC ARM_SET_REFEREE_ATTACHMENT_OBJECT(ARM_CROWD& crowd, OBJECT_INDEX attachmentObject)
	crowd.refereeAttachment = attachmentObject
ENDPROC

// Accessor for current anim.
FUNC TEXT_LABEL_23 ARM_GET_CURRENT_CROWD_ANIM(ARM_CROWD& crowd, ARM_CROWD_MEMBER crowdMember)
	IF crowdMember = ARMCROWDMEMBERS
		TEXT_LABEL_23 sReturn = ""
		RETURN sReturn
	ENDIF
	
	RETURN crowd.sCurAnim[crowdMember]
ENDFUNC

// Mutator for ped index.
PROC ARM_SET_SET_CURRENT_CROWD_ANIM(ARM_CROWD& crowd, ARM_CROWD_MEMBER crowdMember, TEXT_LABEL_23 sAnim)
	IF crowdMember = ARMCROWDMEMBERS
		EXIT
	ENDIF
	
	crowd.sCurAnim[crowdMember] = sAnim
ENDPROC
