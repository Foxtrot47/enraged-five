
//////////////////////////////////////////////////////////////////////
/* AM_Armwrestling.sc												*/
/* Author: DJ Jones													*/
/* Multiplayer script for arm wrestling minigame.					*/
//////////////////////////////////////////////////////////////////////


CONST_INT iUSE_Z_VOLUMES 0
#IF iUSE_Z_VOLUMES
	USING "z_volumes.sch"
#ENDIF

USING "net_mission.sch"
USING "net_mission_trigger_overview.sch"
USING "net_hud_activating.sch"
USING "net_big_message.sch"
USING "minigames_helpers.sch"
USING "minigame_uiinputs.sch"
USING "minigame_big_message.sch"
USING "minigame_midsized_message.sch"
USING "shared_hud_displays.sch"
USING "minigame_stats_tracker_helpers.sch"
USING "socialclub_leaderboard.sch"
USING "script_usecontext.sch"
USING "end_screen.sch"
USING "arm.sch"
USING "achievement_public.sch"

USING "net_celebration_screen.sch"
USING "net_wait_zero.sch"

// Our only static variables. Set once after network init.
ARM_WRESTLER_ID eSelfID
ARM_WRESTLER_ID eOpponentID

BOOL bArmIdleCamSet

INT iArmTotalWins

#IF IS_DEBUG_BUILD
INT iDebugThrottle
BOOL bImHost, bImClient
#ENDIF

CELEBRATION_SCREEN_DATA sCelebrationData
CAMERA_INDEX camEndScreen

USING "arm_tweak.sch"
USING "arm_args.sch"
USING "arm_data.sch"
USING "arm_table.sch"
USING "arm_input.sch"
USING "arm_wrestler.sch"
USING "arm_crowd.sch"
USING "arm_ui.sch"
USING "arm_camera.sch"
USING "arm_mp.sch"
USING "arm_client.sch"
USING "arm_server.sch"

USING "arm_table_lib.sch"
USING "arm_input_lib.sch"
USING "arm_wrestler_lib.sch"
//USING "arm_crowd_lib.sch"
USING "arm_ui_lib.sch"
USING "arm_camera_lib.sch"
USING "arm_client_lib.sch"
USING "arm_server_lib.sch"
USING "arm_leaderboard_lib.sch"

#IF IS_DEBUG_BUILD
	USING "arm_debug.sch"
#ENDIF

USING "arm_core.sch"
USING "arm_mp_core.sch"

CONST_FLOAT STRENGTH_SCALE 0.003
CONST_INT	ARM_XP_AMOUNT 20

ARM_PARTICIPANT_INFO sParticipantInfo

PROC RELEASE_SCRIPT_AUDIO_BANKS(INT iWinSound)
	IF iWinSound <> 0
		RELEASE_NAMED_SCRIPT_AUDIO_BANK(ARM_GET_CROWD_SOUND_BANK_FROM_INDEX(iWinSound, TRUE))
		RELEASE_NAMED_SCRIPT_AUDIO_BANK(ARM_GET_CROWD_SOUND_BANK_FROM_INDEX(iWinSound, FALSE))
	ENDIF
ENDPROC

PROC ARM_MAINTAIN_REALTIME_MULTIPLAYER(BOOL bCountdownDone)
	IF NOT bCountdownDone
		NETWORK_DISABLE_REALTIME_MULTIPLAYER()
	ENDIF
ENDPROC

// Script entry point.
SCRIPT (MP_MISSION_DATA missionScriptArgs)
	STREAMED_MODEL assets[iSTREAMS]
	
	ARM_ARGS args
	ARM_TABLE table
	ARM_WRESTLER wrestlers[ARMWRESTLERIDS]
	ARM_CROWD crowd
	ARM_CAMERA_SET cameraSet
	ARM_UI ui
	ARM_SERVER_BROADCAST_DATA serverData
	ARM_PLAYER_BROADCAST_DATA playerData[iMAX_ARM_PARTICIPANTS]
	ARM_MP_TERMINATE_REASON terminateReason
	ARM_MP_LEAVE_STATE leaveState
	ARM_UI_RETVAL uiResult
	//ARM_TUTORIAL_STATE ArmTutorialState = ARMTUTSTATE_INTRO
	SIMPLE_USE_CONTEXT armUseContext
	
	//END_SCREEN_DATASET_TITLE_UPDATE ArmEndscreen
	
	structTimer controlTimer
	structTimer streamTimer
	structTimer countdownTimer
	structTimer wrestleTimer
	structTimer crowdTimer
	structTimer syncPosTimer
	structTimer tutorialTimer
	structTimer introTimer
//	structTimer faceAnimTimer
	structTimer syncSceneFailsafeTimer
	structTimer rematchCamDelay
	
	SCRIPT_TIMER timeArmAwardFailSafe
	
//	PED_INDEX piLocalClone1
//	PED_INDEX piLocalClone2
	
	ARM_MP_STATE eSpectateMPState
	
	GENERIC_MINIGAME_STAGE eGenericMGStage = GENERIC_CELEBRATION_STATS //GENERIC_CELEBRATION_WINNER
	PLAYER_INDEX winningPlayer
	
	PLAYER_INDEX broadcastIndex
	CAMERA_INDEX renderingCam
	CAMERA_INDEX animatedCam
	
	STRING sAnimDict      = "mini@arm_wrestling"
	STRING sRefIntroAnim  = "ref_intro"
	STRING sReadyHelp
	STRING sWhichWalk
	STRING sWhichCam
	//STRING sTableName
	
//	TEXT_LABEL_63 sOpponentName
//	TEXT_LABEL_63 sColoredName
	TEXT_LABEL_23 contentID	
	
	VECTOR vPlayerCoord
	VECTOR vCamCoord
	VECTOR vArmWrestleLocation
	VECTOR vDestination
//	VECTOR vScenePosition// = << 978.861, -94.320, 74.877 >>
//	VECTOR vSceneRotation// = << 0.000, 0.000, 142.000 >>
	FLOAT fTableRot
	FLOAT fMyPreferenceLevel
	FLOAT fRematchTimer = 45.0
	FLOAT fTotalTime
	//FLOAT fScoreTrack[iSCORE_TRACK_LENGTH]
	FLOAT fLocalStrengthFactor
	FLOAT fCamRot
	
	INT iWinSound, iNewWinSound
	INT iWinStreak
	//INT iLocalScene
	INT iLocalSCene2
	INT iStrengthStat
	INT iControlHelpCounter
	INT iMyParticipantID
	
//	BOOL bCamTasked
	BOOL bRematch, bHelp1, bHelp2, bHelp3
	BOOL bLongerIntro
	BOOL bShardSet
	
	TIMEOFDAY eCurrentTime
	
	BOOL bHappy, bPlayApproachAnim, bFoundPlayer, bHome, bShowTutHelp, bSpecRunning
	
	#IF IS_DEBUG_BUILD
	VECTOR vTemp
	#ENDIF
	
	MPGlobals.WrestleData.bArmPlayBusy = FALSE
	
	IF ARM_GET_ARG_TABLE_INDEX(args) <= 0
		ARM_SET_ARG_TABLE_INDEX(args, ARM_GET_NEAREST_TABLE_INDEX())
	ENDIF
	ARM_GET_TABLE_DATA(table.vCenter, table.fRotation, ARM_GET_ARG_TABLE_INDEX(args), TRUE)
	
	CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: args.iTableIndex = ", args.iTableIndex)
	CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: table.vCenter = ", table.vCenter, ". table.fRotation = ", table.fRotation)
	
	// get table name here
	//sTableName = GET_ARM_TABLE_NAME(ARM_GET_ARG_TABLE_INDEX(args))
	//CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: table name = ", sTableName)
	
	// Perform network init and acquire a participant ID.
	BOOL bInitFailCleanup = NOT ARM_NETWORK_INIT(missionScriptArgs, serverData, playerData, sParticipantInfo)
	
	// ID to use when referencing the player array.
	// 1 = Host, HOME
	// 0 = Client, AWAY
	eSelfID = ARMWRESTLERID_AWAY
	eOpponentID = ARMWRESTLERID_HOME
	
	IF bInitFailCleanup
		CDEBUG1LN(DEBUG_ARM_WRESTLING, "Failed to receive an initial network broadcast. Cleaning up.")
		RELEASE_SCRIPT_AUDIO_BANKS(iWinSound)
		ARM_SCRIPT_CLEANUP(sParticipantInfo, wrestlers, ui, NULL, NULL, crowd.refereeAttachment, ARM_GET_ARG_TABLE_INDEX(args), FALSE, TRUE)
	ENDIF
	
	IF NOT DID_I_JOIN_MISSION_AS_SPECTATOR()
	AND NOT IS_PLAYER_SCTV(PLAYER_ID())
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			eSelfID = ARMWRESTLERID_HOME
			eOpponentID = ARMWRESTLERID_AWAY
			CDEBUG1LN(DEBUG_ARM_WRESTLING, "*******************************")
			CDEBUG1LN(DEBUG_ARM_WRESTLING, "eSelfID = ARMWRESTLERID_HOME")
			CDEBUG1LN(DEBUG_ARM_WRESTLING, "eOpponentID = ARMWRESTLERID_AWAY")
			CDEBUG1LN(DEBUG_ARM_WRESTLING, "*******************************")
		ELSE
			CDEBUG1LN(DEBUG_ARM_WRESTLING, "*******************************")
			CDEBUG1LN(DEBUG_ARM_WRESTLING, "eSelfID = ARMWRESTLERID_AWAY")
			CDEBUG1LN(DEBUG_ARM_WRESTLING, "eOpponentID = ARMWRESTLERID_HOME")
			CDEBUG1LN(DEBUG_ARM_WRESTLING, "*******************************")
		ENDIF
		
		CDEBUG1LN(DEBUG_ARM_WRESTLING, "Arm Wrestling MP: Network setup complete. My participant ID is ", PICK_STRING(eSelfID = ARMWRESTLERID_HOME, "1 (HOST).", "0 (CLIENT)."))
	ELSE
		CDEBUG1LN(DEBUG_ARM_WRESTLING, "*******************************")
		CDEBUG1LN(DEBUG_ARM_WRESTLING, "I AM SPECTATING!!")
		CDEBUG1LN(DEBUG_ARM_WRESTLING, "*******************************")
		CDEBUG1LN(DEBUG_ARM_WRESTLING, "Arm Wrestling MP: Network setup complete. My participant ID is ", PARTICIPANT_ID_TO_INT())
	ENDIF
	
	UPDATE_ARM_PARTICIPANT_INFO(sParticipantInfo)
	PRINT_ARM_PARTICIPANT_INFO(sParticipantInfo)
	
	IF NOT IS_PLAYER_SCTV(PLAYER_ID())
		NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_CLEAR_TASKS | NSPC_NO_COLLISION | NSPC_FREEZE_POSITION)
		CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: net control statement made at the start")
	ENDIF
	
	eCurrentTime = GET_CURRENT_TIMEOFDAY()
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		serverData.iCurrentHour = GET_TIMEOFDAY_HOUR(eCurrentTime)
	ENDIF
	
	REQUEST_SYSTEM_ACTIVITY_TYPE_PLAY_ARM_WRESTLING()
	
	// Wait here for a second player.
	WHILE NOT bFoundPlayer
		WAIT(0)
		
		UPDATE_ARM_PARTICIPANT_INFO(sParticipantInfo)
		
		// Make sure the player isn't leaving Cops & Criminals (also allow free mode in debug).
		IF NOT NETWORK_IS_IN_SESSION()
			CDEBUG1LN(DEBUG_ARM_WRESTLING, "Network is not in session, cleaning up")
			RELEASE_SCRIPT_AUDIO_BANKS(iWinSound)
			ARM_SCRIPT_CLEANUP(sParticipantInfo, wrestlers, ui, NULL, NULL, crowd.refereeAttachment, ARM_GET_ARG_TABLE_INDEX(args), FALSE, TRUE)
		
		// Move on when we have another player.
		ELIF ARM_GET_NUM_WRESTLERS(sParticipantInfo) >= ENUM_TO_INT(ARMWRESTLERIDS)
			bFoundPlayer = TRUE
			
			CDEBUG1LN(DEBUG_ARM_WRESTLING, "am_armwrestling: 2 wrestlers found, starting game")
			
			IF IS_TIMER_STARTED(controlTimer)
				CANCEL_TIMER(controlTimer)
			ENDIF
			
			//Stop people joining my mission
			SET_FM_MISSION_AS_NOT_JOINABLE()
			
		// check if player launches another mode	
		ELIF (MPGlobals.WrestleData.bArmPlayBusy = TRUE)
			CDEBUG1LN(DEBUG_ARM_WRESTLING, "am_armwrestling: player started sitting, cleaning up")
			CLEAR_HELP()
			// print some help here
			BROADCAST_ARM_WRESTLE_TABLE_ACTIVATE(PLAYER_ID(), FALSE)
			ARM_SET_UI_FLAG(UI, ARMUIFLAG_LEAVE_AREA, TRUE)
			RELEASE_SCRIPT_AUDIO_BANKS(iWinSound)
			ARM_SCRIPT_CLEANUP(sParticipantInfo, wrestlers, ui, NULL, NULL, crowd.refereeAttachment, ARM_GET_ARG_TABLE_INDEX(args), FALSE, TRUE)
			
		// Update help text and check for quit conditions.
		ELSE
			
			IF (GET_CURRENT_GAMEMODE() = GAMEMODE_FM)
				IF NOT IS_TIMER_STARTED(controlTimer)
					CDEBUG1LN(DEBUG_ARM_WRESTLING, "bFoundPlayer = False, setting controlTimer ")
					START_TIMER_NOW(controlTimer)
				ELIF GET_TIMER_IN_SECONDS(controlTimer) > 10.0
					CDEBUG1LN(DEBUG_ARM_WRESTLING, "controlTimer > 10.0, cleaning up")
					RELEASE_SCRIPT_AUDIO_BANKS(iWinSound)
					ARM_SCRIPT_CLEANUP(sParticipantInfo, wrestlers, ui, NULL, NULL, crowd.refereeAttachment, ARM_GET_ARG_TABLE_INDEX(args), FALSE, TRUE)
				ENDIF
			ENDIF
			
			renderingCam = GET_RENDERING_CAM()
			IF DOES_CAM_EXIST(renderingCam)
				vCamCoord = GET_CAM_COORD(GET_RENDERING_CAM())
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					vPlayerCoord = GET_ENTITY_COORDS(PLAYER_PED_ID())
				ENDIF
			ELSE
				vCamCoord = vPlayerCoord
			ENDIF
			
			// See if we are changing players.
			IF vCamCoord.z > vPlayerCoord.z + 50.0
				CDEBUG1LN(DEBUG_ARM_WRESTLING, "changing characters, cleaning up")
				RELEASE_SCRIPT_AUDIO_BANKS(iWinSound)
				ARM_SCRIPT_CLEANUP(sParticipantInfo, wrestlers, ui, NULL, NULL, crowd.refereeAttachment, ARM_GET_ARG_TABLE_INDEX(args), FALSE, TRUE)
			
			// See if the player has left the area.
//			ELIF (NOT IS_ENTITY_DEAD(PLAYER_PED_ID())) 
//			AND ( ((GET_PLAYER_TEAM(PLAYER_ID()) = TEAM_CRIM_GANG_1) // checking if the vagos player hit the wardrobe area
//					AND (IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<973.7811,-1841.3265,35.1063>>, <<977.2838,-1841.6954,38.1529>>, 1.25 )))
//				OR ((GET_PLAYER_TEAM(PLAYER_ID()) = TEAM_CRIM_GANG_2) // checking the lost player hit the wardrobe area
//					AND (IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<980.8211,-97.8706,73.8451>>, <<982.6016,-96.2375,76.2967>>, 1.25 ))) )
//				CLEAR_HELP()
//				// print some help here
//				BROADCAST_ARM_WRESTLE_TABLE_ACTIVATE(PLAYER_ID(), FALSE)
//				ARM_SET_UI_FLAG(ui, ARMUIFLAG_LEAVE_AREA, TRUE)
//				
//				CDEBUG1LN(DEBUG_ARM_WRESTLING, "Player has left the area, cleaning up")
//				RELEASE_SCRIPT_AUDIO_BANKS(iWinSound)
//				ARM_SCRIPT_CLEANUP(sParticipantInfo, wrestlers, ui, NULL, NULL, crowd.refereeAttachment, ARM_GET_ARG_TABLE_INDEX(args), FALSE, TRUE)
				
			// Show help text.
			ELIF NOT ARM_GET_UI_FLAG(ui, ARMUIFLAG_SHOW_WAIT_TO_LAUNCH)
				// if there are no fade outs, place a help here to describe it
				ARM_SET_UI_FLAG(ui, ARMUIFLAG_SHOW_WAIT_TO_LAUNCH, TRUE)
			ENDIF
		ENDIF		
	ENDWHILE
	
	// Wait for server and client broadcast data to sync up.
	WAIT(500)
	
	// Make sure we aren't trying to join a game in progress.
	IF ARM_GET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[eOpponentID]], ARMCLIENTFLAG_MATCH_MADE)
	AND NOT DID_I_JOIN_MISSION_AS_SPECTATOR()
	AND NOT IS_PLAYER_SCTV(PLAYER_ID())
		WHILE TRUE
			CDEBUG1LN(DEBUG_ARM_WRESTLING, "match is too full ")
			
			// Print a message saying we can't join this game.
			IF NOT ARM_GET_UI_FLAG(ui, ARMUIFLAG_SHOW_CANT_JOIN)
				START_TIMER_NOW(controlTimer)
				// print help here
				ARM_SET_UI_FLAG(ui, ARMUIFLAG_SHOW_CANT_JOIN, TRUE)
			
			// Wait for confirmation from player or timeout.
			ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT) OR GET_TIMER_IN_SECONDS(controlTimer) >= 8.0
				CLEAR_HELP()
				RELEASE_SCRIPT_AUDIO_BANKS(iWinSound)
				ARM_SCRIPT_CLEANUP(sParticipantInfo, wrestlers, ui, NULL, NULL, crowd.refereeAttachment, ARM_GET_ARG_TABLE_INDEX(args), FALSE, TRUE)
			ENDIF
			WAIT(0)
		ENDWHILE
	ENDIF
	
	// Clear the help text.
	IF ARM_GET_UI_FLAG(ui, ARMUIFLAG_SHOW_WAIT_TO_LAUNCH)
		CLEAR_HELP()
		ARM_SET_UI_FLAG(ui, ARMUIFLAG_SHOW_WAIT_TO_LAUNCH, FALSE)
	ENDIF
	
	// Let the other player know our name.
	IF NOT DID_I_JOIN_MISSION_AS_SPECTATOR()
	AND NOT IS_PLAYER_SCTV(PLAYER_ID())
		
//		ARM_SET_PLAYER_NAME(playerData[sParticipantInfo.iPlayers[eSelfID]], GET_PLAYER_NAME(PLAYER_ID()))
		ARM_SET_PLAYER_ID(playerData[sParticipantInfo.iPlayers[eSelfID]], PLAYER_ID())
		
		iStrengthStat = GET_MP_INT_CHARACTER_STAT(MP_STAT_STRENGTH)
		CDEBUG1LN(DEBUG_ARM_WRESTLING, "iStrengthStat = ", iStrengthStat)
		fLocalStrengthFactor = iStrengthStat * STRENGTH_SCALE
		CDEBUG1LN(DEBUG_ARM_WRESTLING, "fLocalStrengthFactor = ", fLocalStrengthFactor)
		playerData[sParticipantInfo.iPlayers[eSelfID]].fStrengthFactor = fLocalStrengthFactor + 1
		CDEBUG1LN(DEBUG_ARM_WRESTLING, "playerData[sParticipantInfo.iPlayers[eSelfID]].fStrengthFactor = ", playerData[sParticipantInfo.iPlayers[eSelfID]].fStrengthFactor)
		
		iControlHelpCounter = GET_MP_INT_CHARACTER_STAT(MP_STAT_CRARMWREST)
		CDEBUG1LN(DEBUG_ARM_WRESTLING, "number of arm wrestling games from this player = ", iControlHelpCounter)
		
		CDEBUG1LN(DEBUG_ARM_WRESTLING, "value of PACKED_MP_ARMWRESTLING_TUT_TEXT = ", GET_PACKED_STAT_BOOL(PACKED_MP_ARMWRESTLING_TUT_TEXT))
		SET_PACKED_STAT_BOOL(PACKED_MP_ARMWRESTLING_TUT_TEXT, TRUE)
		CDEBUG1LN(DEBUG_ARM_WRESTLING, "after setting,  PACKED_MP_ARMWRESTLING_TUT_TEXT = ", GET_PACKED_STAT_BOOL(PACKED_MP_ARMWRESTLING_TUT_TEXT))
		
	ENDIF
	IF bMinigamePlayerQuitFlag
		bMinigamePlayerQuitFlag = FALSE
	ENDIF
	
	IF NOT DID_I_JOIN_MISSION_AS_SPECTATOR()
		ANIMPOSTFX_STOP_ALL()
	ENDIF
	
	//gdisablerankupmessage = TRUE
	SET_DISABLE_RANK_UP_MESSAGE(TRUE)
	
	CDEBUG1LN(DEBUG_ARM_WRESTLING, " ***** HEADING TO MAIN LOOP ***** ")
	
	eSpectateMPState = ARMMPSTATE_INIT
	
	// Main loop.
	WHILE TRUE
		MP_LOOP_WAIT_ZERO()
		
		MAINTAIN_CELEBRATION_PRE_LOAD(sCelebrationData)
		
		UPDATE_ARM_PARTICIPANT_INFO(sParticipantInfo)
		iMyParticipantID = PARTICIPANT_ID_TO_INT()
		
		NETWORK_PREVENT_SCRIPT_HOST_MIGRATION()
		
		NETWORK_OVERRIDE_CLOCK_TIME(serverData.iCurrentHour, 0, 0)
		SET_LOADING_ICON_SUBTITLES_OFFSET_SHIFT_THIS_FRAME()
		
		// Make sure we're in Cops & Criminals (Free Mode ok for debug).
		IF NOT NETWORK_IS_IN_SESSION()
			CDEBUG1LN(DEBUG_ARM_WRESTLING, "network not in session, cleaning up")
			RELEASE_SCRIPT_AUDIO_BANKS(iWinSound)
			ARM_SCRIPT_CLEANUP(sParticipantInfo, wrestlers, ui, NULL, NULL, crowd.refereeAttachment, ARM_GET_ARG_TABLE_INDEX(args), FALSE, TRUE)
		ENDIF
		
		HIDE_HUD_AND_RADAR_THIS_FRAME()
		ARM_MAINTAIN_REALTIME_MULTIPLAYER(ARM_SERVER_GET_FLAG(serverData, ARMSERVERFLAG_COUNTDOWN_DONE))
		
		//CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: iArmStats[ARM_STAT_NUM_WINS] = ", iArmStats[ARM_STAT_NUM_WINS])
		
		IF DID_I_JOIN_MISSION_AS_SPECTATOR()
		OR IS_PLAYER_SCTV(PLAYER_ID())
		
			IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE() #IF IS_DEBUG_BUILD OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F) #ENDIF
				
				CDEBUG1LN(DEBUG_ARM_WRESTLING, "should mp thread terminate returned true")
				
				IF RECEIVED_EVENT(EVENT_NETWORK_END_MATCH)
					bMinigamePlayerQuitFlag = TRUE
				ENDIF
				
				RELEASE_SCRIPT_AUDIO_BANKS(iWinSound)
				ARM_SCRIPT_CLEANUP(sParticipantInfo, wrestlers, ui, NULL, NULL, crowd.refereeAttachment, ARM_GET_ARG_TABLE_INDEX(args), TRUE, TRUE)
				
			ENDIF
			
			
			// Check here for missing player.
			IF eSpectateMPState < ARMMPSTATE_TERMINATE_SPLASH
			AND ARM_GET_NUM_WRESTLERS( sParticipantInfo ) < ENUM_TO_INT(ARMWRESTLERIDS)
				CDEBUG1LN(DEBUG_ARM_WRESTLING, "made it into the missing player condition")
				
				CLEAR_HELP()
				STOP_STREAM()
				
				CLEANUP_BIG_MESSAGE()
				
				CDEBUG1LN(DEBUG_ARM_WRESTLING, "ARM_GET_NUM_WRESTLERS( sParticipantInfo ) < 2 returned true, terminating")
				
				IF ARM_GET_SERVER_GAME_STATE(serverData) = ARMGAMESTATE_WINNER
				OR eSpectateMPState = ARMMPSTATE_WAIT_POST_GAME
					CDEBUG1LN(DEBUG_ARM_WRESTLING, "game had already ended, setting terminate reason to ENDGAME")
					terminateReason = ARMMPTERMINATEREASON_ENDGAME
				ELSE
					CDEBUG1LN(DEBUG_ARM_WRESTLING, "game was still in play, setting terminate reason to PLAYERLEFT")
					terminateReason = ARMMPTERMINATEREASON_PLAYERLEFT
				ENDIF
				
				eSpectateMPState = ARMMPSTATE_TERMINATE_SPLASH
			ENDIF
			
			// check for player quit
			IF eSpectateMPState < ARMMPSTATE_TERMINATE_SPLASH
			AND sParticipantInfo.iPlayers[ARMWRESTLERID_AWAY] != -1
			AND sParticipantInfo.iPlayers[ARMWRESTLERID_HOME] != -1
				IF ARM_GET_PLAYER_MP_STATE(playerData[sParticipantInfo.iPlayers[ARMWRESTLERID_AWAY]]) >= ARMMPSTATE_TERMINATE_SPLASH
				OR ARM_GET_PLAYER_MP_STATE(playerData[sParticipantInfo.iPlayers[ARMWRESTLERID_HOME]]) >= ARMMPSTATE_TERMINATE_SPLASH
					CLEAR_HELP()
					STOP_STREAM()
					
					CDEBUG1LN(DEBUG_ARM_WRESTLING, "made it into the player quit condition")
					
					CLEANUP_BIG_MESSAGE()
					
					CDEBUG1LN(DEBUG_ARM_WRESTLING, "other player is already in terminate splashe, time to cleanup")
					
					IF ARM_GET_SERVER_GAME_STATE(serverData) = ARMGAMESTATE_WINNER
					OR eSpectateMPState = ARMMPSTATE_WAIT_POST_GAME
						CDEBUG1LN(DEBUG_ARM_WRESTLING, "game had already ended, setting terminate reason to ENDGAME")
						terminateReason = ARMMPTERMINATEREASON_ENDGAME
					ELSE
						CDEBUG1LN(DEBUG_ARM_WRESTLING, "game was still in play, setting terminate reason to PLAYERLEFT")
						terminateReason = ARMMPTERMINATEREASON_PLAYERLEFT
					ENDIF
					
					eSpectateMPState = ARMMPSTATE_TERMINATE_SPLASH
				ENDIF
			ENDIF
				
			IF (GET_GAME_TIMER() % 5000) < 50
				CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling: ARM_GET_NUM_WRESTLERS( sParticipantInfo )  = ", ARM_GET_NUM_WRESTLERS( sParticipantInfo ))
			ENDIF
			
			IF eSpectateMPState > ARMMPSTATE_SETUP
				IF NOT IS_PED_INJURED(wrestlers[ARMWRESTLERID_AWAY].otherWrestler)
				AND NOT IS_PED_INJURED(wrestlers[ARMWRESTLERID_HOME].otherWrestler)
					#IF IS_DEBUG_BUILD
					IF (GET_GAME_TIMER() % 3000) < 50
						CDEBUG1LN(DEBUG_ARM_WRESTLING, "************************************")
						vTemp = GET_ENTITY_COORDS(wrestlers[ARMWRESTLERID_AWAY].otherWrestler)
						CDEBUG1LN(DEBUG_ARM_WRESTLING, "ARMWRESTLERID_AWAY coords: ", vTemp)
						vTemp = GET_ENTITY_COORDS(wrestlers[ARMWRESTLERID_HOME].otherWrestler)
						CDEBUG1LN(DEBUG_ARM_WRESTLING, "ARMWRESTLERID_HOME coords: ", vTemp)
						vTemp = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(wrestlers[ARMWRESTLERID_AWAY].otherWrestler, GET_ENTITY_COORDS(wrestlers[ARMWRESTLERID_HOME].otherWrestler))
						CDEBUG1LN(DEBUG_ARM_WRESTLING, "home offset from away coords: ", vTemp)
						vTemp = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(wrestlers[ARMWRESTLERID_HOME].otherWrestler, GET_ENTITY_COORDS(wrestlers[ARMWRESTLERID_AWAY].otherWrestler))
						CDEBUG1LN(DEBUG_ARM_WRESTLING, "away offset from home coords: ", vTemp)
						CDEBUG1LN(DEBUG_ARM_WRESTLING, "vArmWrestleLocation: ", vArmWrestleLocation)
						CDEBUG1LN(DEBUG_ARM_WRESTLING, "fTableRot: ", fTableRot)
						CDEBUG1LN(DEBUG_ARM_WRESTLING, "************************************")
					ENDIF
					#ENDIF
					
					IF NETWORK_IS_PLAYER_ACTIVE(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(sParticipantInfo.iPlayers[ARMWRESTLERID_HOME])))
					AND NETWORK_IS_PLAYER_ACTIVE(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(sParticipantInfo.iPlayers[ARMWRESTLERID_AWAY])))
						NETWORK_OVERRIDE_COORDS_AND_HEADING(wrestlers[ARMWRESTLERID_HOME].otherWrestler,
															GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vArmWrestleLocation, fTableRot, <<fOVERRIDE_x/2, fOVERRIDE_y/2 , fOVERRIDE_Z>>),
															WRAP(fTableRot + fOVERRIDE_heading, 0.0, 360.0))
						NETWORK_OVERRIDE_COORDS_AND_HEADING(wrestlers[ARMWRESTLERID_AWAY].otherWrestler,
															GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vArmWrestleLocation, fTableRot, <<-(fOVERRIDE_x/2), -(fOVERRIDE_y/2), fOVERRIDE_Z>>),
															fTableRot)
					ENDIF
				ENDIF
			ENDIF
			
			SWITCH eSpectateMPState
				CASE ARMMPSTATE_INIT
					
					// Wait until the server moves beyond the init state.
					IF ARM_GET_SERVER_MP_STATE(serverData) > ARMMPSTATE_INIT
						ARM_SET_ARG_TABLE_INDEX(args, ARM_GET_SERVER_TABLE_INDEX(serverData))
						ARM_GET_TABLE_DATA(table.vCenter, table.fRotation, ARM_GET_ARG_TABLE_INDEX(args), TRUE)
						
						DISABLE_ALL_MP_HUD()
						DISABLE_SELECTOR()
						DISABLE_CELLPHONE(TRUE)
						
						ARM_INIT_GAME(args, table, assets, ui, ARM_GET_ARG_TABLE_INDEX(args))
						
						IF GET_CURRENT_GAMEMODE() = GAMEMODE_FM
							ARM_GET_TABLE_DATA(vArmWrestleLocation, fTableRot, ARM_GET_ARG_TABLE_INDEX(args), TRUE)
						ELSE	
//							IF GET_PLAYER_TEAM(PLAYER_ID()) = TEAM_CRIM_GANG_1
//								ARM_GET_TABLE_DATA(vArmWrestleLocation, fTableRot, 1, FALSE)
//							ELIF GET_PLAYER_TEAM(PLAYER_ID()) = TEAM_CRIM_GANG_2
//								ARM_GET_TABLE_DATA(vArmWrestleLocation, fTableRot, 2, FALSE)
//							ENDIF
						ENDIF
						
						CDEBUG1LN(DEBUG_ARM_WRESTLING, "~~~~~~~~~~~AM_Armwrestling.sc: vArmWrestleLocation is now ", vArmWrestleLocation)
						
						CLEAR_AREA(table.vCenter, 4.0, TRUE)
						
						IF HAS_ADDITIONAL_TEXT_LOADED(MINIGAME_TEXT_SLOT)
							eSpectateMPState = ARMMPSTATE_TRAVEL
						ELSE
							CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: ARM_GET_PLAYER_MP_STATE: FAILED TO LOAD MINIGAME_TEXT_SLOT!!")
						ENDIF
					ENDIF
				BREAK
				
				CASE ARMMPSTATE_TRAVEL
					
					IF IS_TIMER_STARTED(syncPosTimer)
						CANCEL_TIMER(syncPosTimer)
					ENDIF
					
					eSpectateMPState = ARMMPSTATE_SETUP
					
				BREAK
				
				CASE ARMMPSTATE_SETUP
					
					IF ARM_IS_STREAMING_COMPLETE(assets, ui)
						
						IF NOT IS_TIMER_STARTED(streamTimer)
							START_TIMER_NOW(streamTimer)
						ENDIF
						
						IF GET_TIMER_IN_SECONDS(streamTimer) > 1.5
						AND sParticipantInfo.iPlayers[ARMWRESTLERID_AWAY] > -1
						AND sParticipantInfo.iPlayers[ARMWRESTLERID_HOME] > -1
							
							//ARM_SETUP_WRESTLERS(wrestlers, table, cameraSet, FALSE, TO_FLOAT(ENUM_TO_INT(eOpponentID)), TO_FLOAT(ENUM_TO_INT(eSelfID)))
							
							wrestlers[ARMWRESTLERID_AWAY].otherWrestler = GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(sParticipantInfo.iPlayers[ARMWRESTLERID_AWAY]))) 
							wrestlers[ARMWRESTLERID_HOME].otherWrestler = GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(sParticipantInfo.iPlayers[ARMWRESTLERID_HOME]))) 
							
							IF DOES_ENTITY_EXIST(wrestlers[ARMWRESTLERID_AWAY].otherWrestler)
							AND NOT IS_PED_INJURED(wrestlers[ARMWRESTLERID_AWAY].otherWrestler)
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: away wrestler EXISTS AND IS NOT INJURED")
							ELSE
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: away wrestler does NOT exist")
							ENDIF
							
							IF DOES_ENTITY_EXIST(wrestlers[ARMWRESTLERID_HOME].otherWrestler)
							AND NOT IS_PED_INJURED(wrestlers[ARMWRESTLERID_HOME].otherWrestler)
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: home wrestler EXISTS AND IS NOT INJURED")
								
								vDestination = ARM_GET_TABLE_WRESTLER_POS(table, TRUE, TRUE)
								ARM_INIT_CAMERAS_SPECTATOR(cameraSet, table, vDestination, ARM_GET_ARG_TABLE_INDEX(args), wrestlers[ARMWRESTLERID_HOME].otherWrestler)
						
							ELSE
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: home wrestler does NOT exist")
							ENDIF
							
							RESTART_TIMER_NOW(streamTimer)
							
							iArmStats[ARM_STAT_NUM_MATCHES]++
							
							RENDER_SCRIPT_CAMS(FALSE, FALSE)
							
							eSpectateMPState = ARMMPSTATE_SETUP_POST
						ENDIF
					ENDIF
					
				BREAK
				
				CASE ARMMPSTATE_SETUP_POST
					
					IF NOT ARM_GET_CLIENT_FLAG(playerData[iMyParticipantID], ARMCLIENTFLAG_AT_TUTORIAL) 
						
						ARM_SET_UI_FLAG(ui, ARMUIFLAG_SETUP_WAIT_READY, FALSE)
						
						CANCEL_TIMER(streamTimer)
						
						IF IS_TASK_MOVE_NETWORK_ACTIVE(wrestlers[ARMWRESTLERID_HOME].otherWrestler)
						AND IS_TASK_MOVE_NETWORK_ACTIVE(wrestlers[ARMWRESTLERID_AWAY].otherWrestler)
							
							ARM_SET_CLIENT_FLAG(playerData[iMyParticipantID], ARMCLIENTFLAG_AT_TUTORIAL, TRUE)
							
							IF IS_SCREEN_FADED_OUT()
								DISPLAY_RADAR(FALSE) // 2nd take here due to 1367016
								DO_SCREEN_FADE_IN(500)
							ENDIF
							
						ELSE
							IF (GET_GAME_TIMER() % 1000) < 50
								IF NOT IS_TASK_MOVE_NETWORK_ACTIVE(wrestlers[ARMWRESTLERID_HOME].otherWrestler)
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: Move network for ARMWRESTLERID_HOME is not active yet")
								ENDIF
								
								IF NOT IS_TASK_MOVE_NETWORK_ACTIVE(wrestlers[ARMWRESTLERID_AWAY].otherWrestler)
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: Move network for ARMWRESTLERID_AWAY is not active yet")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF ARM_GET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[ARMWRESTLERID_HOME]], ARMCLIENTFLAG_AT_TUTORIAL)
					AND ARM_GET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[ARMWRESTLERID_AWAY]], ARMCLIENTFLAG_AT_TUTORIAL)
					AND NOT ARM_GET_CLIENT_FLAG(playerData[iMyParticipantID], ARMCLIENTFLAG_SYNC_COMPLETE)
						
						CANCEL_TIMER(streamTimer)
						
						IF NOT IS_ENTITY_DEAD(wrestlers[ARMWRESTLERID_HOME].otherWrestler)
						AND NOT IS_ENTITY_DEAD(wrestlers[ARMWRESTLERID_AWAY].otherWrestler)
						AND IS_TASK_MOVE_NETWORK_ACTIVE(wrestlers[ARMWRESTLERID_HOME].otherWrestler)
						AND IS_TASK_MOVE_NETWORK_ACTIVE(wrestlers[ARMWRESTLERID_AWAY].otherWrestler)
							CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: home init move state: ", GET_TASK_MOVE_NETWORK_STATE(wrestlers[ARMWRESTLERID_HOME].otherWrestler))
							CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: away init move state: ", GET_TASK_MOVE_NETWORK_STATE(wrestlers[ARMWRESTLERID_AWAY].otherWrestler))
						ELSE
							CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: move not active")
						ENDIF
						
						RENDER_SCRIPT_CAMS(FALSE, FALSE)
						
						eSpectateMPState = ARMMPSTATE_INTRO_SCENE_ALT
					ENDIF
					
				BREAK
				
				CASE ARMMPSTATE_INTRO_SCENE_ALT
					
					IF IS_SCREEN_FADED_OUT()
						DISPLAY_RADAR(FALSE) // 2nd take here due to 1367016
						DO_SCREEN_FADE_IN(500)
					ENDIF
					
					IF ARM_GET_SERVER_MP_STATE(serverData) = ARMMPSTATE_RUNNING
						eSpectateMPState = ARMMPSTATE_WAIT_POST_GAME
						
						IF ARM_GET_ARG_TABLE_INDEX(args) != 12
							ARM_WIN_CAM_CYCLE(cameraset, bHappy)
						ELSE
							IF eSelfID = ARMWRESTLERID_AWAY
								ARM_WIN_CAM_CYCLE(cameraset, FALSE)
							ELSE
								ARM_WIN_CAM_CYCLE(cameraset, TRUE)
							ENDIF
						ENDIF
						CDEBUG1LN(DEBUG_ARM_WRESTLING, "early cams turned on for spectator.")
						RENDER_SCRIPT_CAMS(TRUE, FALSE)
						
						CDEBUG1LN(DEBUG_ARM_WRESTLING, "spectator sensed that the game is already in motion! going to wait in post game for the next round")
						// add help text here?
					ELSE
						IF NOT ( ARM_GET_CLIENT_FLAG(PlayerData[sParticipantInfo.iPlayers[ARMWRESTLERID_HOME]], ARMCLIENTFLAG_TUTORIAL_DONE)
								AND ARM_GET_CLIENT_FLAG(PlayerData[sParticipantInfo.iPlayers[ARMWRESTLERID_AWAY]], ARMCLIENTFLAG_TUTORIAL_DONE) )
							fCamRot = table.fRotation
							bLongerIntro = TRUE
							sWhichWalk = "Walk"
							sWhichCam = "aw_ig_intro_cam"
							CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: playing longer intro")
						ELSE
							fCamRot = ARM_GET_INTRO_CAMERA_DATA(ARM_GET_ARG_TABLE_INDEX(args), table.fRotation)
							bLongerIntro = FALSE
							sWhichWalk = "AltWalk"
							sWhichCam = "aw_ig_intro_alt1_cam"
							CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: playing shorter intro")
						ENDIF
						
						IF NOT IS_ENTITY_DEAD(wrestlers[ARMWRESTLERID_HOME].otherWrestler)
						AND NOT IS_ENTITY_DEAD(wrestlers[ARMWRESTLERID_AWAY].otherWrestler)
						AND IS_TASK_MOVE_NETWORK_ACTIVE(wrestlers[ARMWRESTLERID_HOME].otherWrestler)
						AND IS_TASK_MOVE_NETWORK_ACTIVE(wrestlers[ARMWRESTLERID_AWAY].otherWrestler)
							IF NOT ARE_STRINGS_EQUAL(GET_TASK_MOVE_NETWORK_STATE(wrestlers[ARMWRESTLERID_HOME].otherWrestler), sWhichWalk)
								IF IS_TASK_MOVE_NETWORK_READY_FOR_TRANSITION(wrestlers[ARMWRESTLERID_HOME].otherWrestler)
									IF REQUEST_TASK_MOVE_NETWORK_STATE_TRANSITION(wrestlers[ARMWRESTLERID_HOME].otherWrestler, sWhichWalk)
										CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: requested for home: ", sWhichWalk)
									ELSE
										CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: request was denied for home: ", sWhichWalk)
									ENDIF
								ELSE
									IF (GET_GAME_TIMER() % 1000) < 50
										CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: home not ready for transition in beginning: ", sWhichWalk)
									ENDIF
								ENDIF
							ENDIF
							
							IF NOT ARE_STRINGS_EQUAL(GET_TASK_MOVE_NETWORK_STATE(wrestlers[ARMWRESTLERID_AWAY].otherWrestler), sWhichWalk)
								IF IS_TASK_MOVE_NETWORK_READY_FOR_TRANSITION(wrestlers[ARMWRESTLERID_AWAY].otherWrestler)
									IF REQUEST_TASK_MOVE_NETWORK_STATE_TRANSITION(wrestlers[ARMWRESTLERID_AWAY].otherWrestler, sWhichWalk)
										CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: requested for away: ", sWhichWalk)
									ELSE
										CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: request was denied for away: ", sWhichWalk)
									ENDIF
								ELSE
									IF (GET_GAME_TIMER() % 1000) < 50
										CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: away not ready for transition in beginning: ", sWhichWalk)
									ENDIF
								ENDIF
							ENDIF
							
							IF (ARE_STRINGS_EQUAL(GET_TASK_MOVE_NETWORK_STATE(wrestlers[ARMWRESTLERID_AWAY].otherWrestler), sWhichWalk)
							AND ARE_STRINGS_EQUAL(GET_TASK_MOVE_NETWORK_STATE(wrestlers[ARMWRESTLERID_HOME].otherWrestler), sWhichWalk))
								
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: away move state: ", GET_TASK_MOVE_NETWORK_STATE(wrestlers[ARMWRESTLERID_AWAY].otherWrestler))
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: home move state: ", GET_TASK_MOVE_NETWORK_STATE(wrestlers[ARMWRESTLERID_HOME].otherWrestler))
								
								iLocalSCene2 = CREATE_SYNCHRONIZED_SCENE(table.vCenter, << 0, 0, fCamRot + 180.0 >>)
								animatedCam = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", FALSE)
								PLAY_SYNCHRONIZED_CAM_ANIM(animatedCam, iLocalSCene2, sWhichCam, "mini@arm_wrestling")
								SET_CAM_ACTIVE(animatedCam, TRUE)
								RENDER_SCRIPT_CAMS(TRUE, FALSE)
								
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: local sync scene is ", iLocalSCene2)
								
								RESTART_TIMER_NOW(introTimer)
								
								eSpectateMPState = ARMMPSTATE_INTRO_SCENE_ALT_END
								
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: setting FM Match Start as the intro starts")
								DEAL_WITH_FM_MATCH_START(FMMC_TYPE_MG_ARM_WRESTLING, serverData.iMatchHistoryID, serverData.iMatchType)
								
							ELSE
								IF (GET_GAME_TIMER() % 1000) < 50
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: wrestlers are not at Walk yet")
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: player move state: ", GET_TASK_MOVE_NETWORK_STATE(wrestlers[ARMWRESTLERID_AWAY].otherWrestler))
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: other  move state: ", GET_TASK_MOVE_NETWORK_STATE(wrestlers[ARMWRESTLERID_HOME].otherWrestler))
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE ARMMPSTATE_INTRO_SCENE_ALT_END
					IF DOES_CAM_EXIST(animatedCam)
						IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalSCene2)
							IF GET_SYNCHRONIZED_SCENE_PHASE(iLocalSCene2) = 1.0
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: iLocalSCene2 is ", iLocalSCene2)
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: iLocalSCene2 phase is ", GET_SYNCHRONIZED_SCENE_PHASE(iLocalSCene2))
								
								ARM_SET_CLIENT_FLAG(playerData[iMyParticipantID], ARMCLIENTFLAG_SYNC_COMPLETE, TRUE)
								eSpectateMPState = ARMMPSTATE_RUNNING
								
							ELSE
								
								IF (GET_GAME_TIMER() % 1000) < 50
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: iLocalSCene2 is ", iLocalSCene2)
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: iLocalSCene2 phase is ", GET_SYNCHRONIZED_SCENE_PHASE(iLocalSCene2))
								ENDIF
							ENDIF
						ELSE
							IF (GET_GAME_TIMER() % 1000) < 50
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "client: sync scene not running")
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "client: iLocalSCene2 = ", iLocalSCene2)
							ENDIF
						ENDIF
					ENDIF
					
					IF ARM_GET_SERVER_MP_STATE(serverData) >= ARMMPSTATE_RUNNING
						ARM_SET_CLIENT_FLAG(playerData[iMyParticipantID], ARMCLIENTFLAG_SYNC_COMPLETE, TRUE)
						
						IF ARM_GET_SERVER_GAME_STATE(serverData) > ARMGAMESTATE_READYMENU
							eSpectateMPState = ARMMPSTATE_WAIT_TO_START_GAME
							CDEBUG1LN(DEBUG_ARM_WRESTLING, "wrestlers already wrestling, going to ARMMPSTATE_WAIT_TO_START_GAME")
						ELSE
							eSpectateMPState = ARMMPSTATE_RUNNING
							CDEBUG1LN(DEBUG_ARM_WRESTLING, "wrestlers not wrestling yet, going to ARMMPSTATE_RUNNING")
						ENDIF
					ENDIF
				BREAK
				
				// Wait for the other player to ready up.
				CASE ARMMPSTATE_WAIT_TO_START_GAME
					IF ARM_SERVER_GET_FLAG(serverData, ARMSERVERFLAG_MOVETOGAME)
						IF NOT IS_TIMER_STARTED(introTimer)
							RESTART_TIMER_NOW(introTimer)
							CDEBUG1LN(DEBUG_ARM_WRESTLING, "spectator started intro timer")
						ELIF GET_TIMER_IN_SECONDS(introTimer) > 4.0
						
							// Request the win/loss audio if needed.
							iNewWinSound = GET_RANDOM_INT_IN_RANGE(1, 4)
							IF iWinSound <> iNewWinSound
								IF iWinSound <> 0
									RELEASE_NAMED_SCRIPT_AUDIO_BANK(ARM_GET_CROWD_SOUND_BANK_FROM_INDEX(iWinSound, TRUE))
									RELEASE_NAMED_SCRIPT_AUDIO_BANK(ARM_GET_CROWD_SOUND_BANK_FROM_INDEX(iWinSound, FALSE))
								ENDIF
								iWinSound = iNewWinSound
								REQUEST_SCRIPT_AUDIO_BANK(ARM_GET_CROWD_SOUND_BANK_FROM_INDEX(iWinSound, TRUE))
								REQUEST_SCRIPT_AUDIO_BANK(ARM_GET_CROWD_SOUND_BANK_FROM_INDEX(iWinSound, FALSE))
							ENDIF
							
							CLEAR_HELP()
							RENDER_SCRIPT_CAMS(TRUE, FALSE)
							
							RESTART_TIMER_NOW(wrestleTimer)
							RESTART_TIMER_NOW(countdownTimer)
							
							CANCEL_TIMER(introTimer)
							
							ARM_SET_UI_FLAG(ui, ARMUIFLAG_SHOW_TUTORIAL, TRUE)
							ARM_SET_UI_FLAG(ui, ARMUIFLAG_SHOW_WAIT_READY, FALSE)
							eSpectateMPState = ARMMPSTATE_RUNNING
							CDEBUG1LN(DEBUG_ARM_WRESTLING, "spectator going to running")
						ELSE
							CDEBUG3LN(DEBUG_ARM_WRESTLING, "introTimer is at ", GET_TIMER_IN_SECONDS(introTimer))
						ENDIF
					ELSE					
						ARM_UPDATE_WAIT_READY_UI(ui)					
					ENDIF
				BREAK
				
				CASE ARMMPSTATE_RUNNING
					
					// Act based on the server state.
					SWITCH ARM_GET_SERVER_GAME_STATE(serverData)
						// Waiting for at least one player to ready up.
						CASE ARMGAMESTATE_READYMENU
							
							// We need to start our approach anim.
							IF NOT bPlayApproachAnim
								IF bRematch
								// MoVE network script:
									IF NOT ARM_GET_CLIENT_FLAG(playerData[iMyParticipantID], ARMCLIENTFLAG_REQUEST_ANIM)
										// Move state change to Approach -----------------------------------------------------------------------
										IF NOT IS_ENTITY_DEAD(wrestlers[ARMWRESTLERID_HOME].otherWrestler)
										AND NOT IS_ENTITY_DEAD(wrestlers[ARMWRESTLERID_AWAY].otherWrestler)
										AND IS_TASK_MOVE_NETWORK_ACTIVE(wrestlers[ARMWRESTLERID_HOME].otherWrestler)
										AND IS_TASK_MOVE_NETWORK_ACTIVE(wrestlers[ARMWRESTLERID_AWAY].otherWrestler)	
										
											IF NOT ARE_STRINGS_EQUAL(GET_TASK_MOVE_NETWORK_STATE(wrestlers[ARMWRESTLERID_HOME].otherWrestler), "Approach")
												IF IS_TASK_MOVE_NETWORK_READY_FOR_TRANSITION(wrestlers[ARMWRESTLERID_HOME].otherWrestler)
													IF REQUEST_TASK_MOVE_NETWORK_STATE_TRANSITION(wrestlers[ARMWRESTLERID_HOME].otherWrestler, "Approach")
														CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: Approach requested for ARMWRESTLERID_HOME")
													ELSE
														CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: Approach request was denied for ARMWRESTLERID_HOME")
													ENDIF
												ELSE
													IF (GET_GAME_TIMER() % 1000) < 50
														CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: ARMWRESTLERID_HOME not ready for Approach transition in beginning")
													ENDIF
												ENDIF
											ENDIF
											
											IF NOT ARE_STRINGS_EQUAL(GET_TASK_MOVE_NETWORK_STATE(wrestlers[ARMWRESTLERID_AWAY].otherWrestler), "Approach")
												IF IS_TASK_MOVE_NETWORK_READY_FOR_TRANSITION(wrestlers[ARMWRESTLERID_AWAY].otherWrestler)
													IF REQUEST_TASK_MOVE_NETWORK_STATE_TRANSITION(wrestlers[ARMWRESTLERID_AWAY].otherWrestler, "Approach")
														CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: Approach requested for ARMWRESTLERID_AWAY")
													ELSE
														CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: Approach request was denied for ARMWRESTLERID_AWAY")
													ENDIF
												ELSE
													IF (GET_GAME_TIMER() % 1000) < 50
														CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: ARMWRESTLERID_AWAY not ready for Approach transition")
													ENDIF
												ENDIF
											ENDIF
											
											IF (ARE_STRINGS_EQUAL(GET_TASK_MOVE_NETWORK_STATE(wrestlers[ARMWRESTLERID_HOME].otherWrestler), "Approach")
											AND ARE_STRINGS_EQUAL(GET_TASK_MOVE_NETWORK_STATE(wrestlers[ARMWRESTLERID_AWAY].otherWrestler), "Approach"))
												
												CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: player move state: ", GET_TASK_MOVE_NETWORK_STATE(wrestlers[ARMWRESTLERID_HOME].otherWrestler))
												CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: other  move state: ", GET_TASK_MOVE_NETWORK_STATE(wrestlers[ARMWRESTLERID_AWAY].otherWrestler))
												ARM_SET_CLIENT_FLAG(playerData[iMyParticipantID], ARMCLIENTFLAG_REQUEST_ANIM, TRUE)
												
												bPlayApproachAnim = TRUE
											ELSE
												IF (GET_GAME_TIMER() % 1000) < 50
													CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: wrestlers are not at Approach yet")
													CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: player move state: ", GET_TASK_MOVE_NETWORK_STATE(wrestlers[ARMWRESTLERID_HOME].otherWrestler))
													CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: other  move state: ", GET_TASK_MOVE_NETWORK_STATE(wrestlers[ARMWRESTLERID_AWAY].otherWrestler))
												ENDIF
											ENDIF
										ELSE
											IF (GET_GAME_TIMER() % 1000) < 50
												CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: move network is not active for wrestlers")
											ENDIF
										ENDIF
										// End Move state change to Approach -----------------------------------------------------------------------
									ENDIF
								ELSE
									bPlayApproachAnim = TRUE
									ARM_SET_CLIENT_FLAG(playerData[iMyParticipantID], ARMCLIENTFLAG_REQUEST_ANIM, TRUE)
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "set approach anim flag true, and request anim flag")
								ENDIF
								
							// We have started our approach anim.
							ELSE
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: HIT 3")
								
								// Play and set the speed of the idle to neutral anim.
								// MoVE network script:
								IF NOT ARM_GET_CLIENT_FLAG(playerData[iMyParticipantID], ARMCLIENTFLAG_START_ANIM)
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "triggered start anim flag")
									ARM_SET_CLIENT_FLAG(playerData[iMyParticipantID], ARMCLIENTFLAG_START_ANIM, TRUE)
								ENDIF
							ENDIF
							
							// The player has readied up.
							IF ARM_GET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[ARMWRESTLERID_HOME]], ARMCLIENTFLAG_START_ANIM)
							AND ARM_GET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[ARMWRESTLERID_HOME]], ARMCLIENTFLAG_REQUEST_ANIM)
							AND ARM_GET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[ARMWRESTLERID_AWAY]], ARMCLIENTFLAG_START_ANIM)
							AND ARM_GET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[ARMWRESTLERID_AWAY]], ARMCLIENTFLAG_REQUEST_ANIM)
								bPlayApproachAnim = FALSE
								
								eSpectateMPState = ARMMPSTATE_WAIT_TO_START_GAME
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: HIT 4")
								
							// The player is quitting.
							ELIF uiResult = ARMUIRETVAL_FALSE
								
								eSpectateMPState =  ARMMPSTATE_TERMINATE_SPLASH
							
							ELSE
								
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "home player start anim flag: ", ARM_GET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[ARMWRESTLERID_HOME]], ARMCLIENTFLAG_START_ANIM))
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "home player reque anim flag: ", ARM_GET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[ARMWRESTLERID_HOME]], ARMCLIENTFLAG_REQUEST_ANIM))
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "away player start anim flag: ", ARM_GET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[ARMWRESTLERID_AWAY]], ARMCLIENTFLAG_START_ANIM))
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "away player reque anim flag: ", ARM_GET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[ARMWRESTLERID_AWAY]], ARMCLIENTFLAG_REQUEST_ANIM))
							ENDIF
						BREAK
						
						// Players are currently armwrestling.
						CASE ARMGAMESTATE_PLAYING
							
							// Show the countdown timer.
							IF IS_TIMER_STARTED(countdownTimer)
								
								CDEBUG3LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: countdown timer is at ", GET_TIMER_IN_SECONDS(countdownTimer))
								
								IF (GET_TIMER_IN_SECONDS(countdownTimer) < 4.0)
								OR NOT bSpecRunning
								
									IF ARM_IS_CAMERA_ACTIVE(cameraSet, ARMCAMERAID_IDLE)
									OR (NOT ARM_IS_CAMERA_ACTIVE(cameraSet, ARMCAMERAID_NEUTRAL))
										ARM_SET_ACTIVE_CAMERA(cameraSet, ARMCAMERAID_NEUTRAL, TRUE)
										IF DOES_CAM_EXIST(animatedCam)
											DESTROY_CAM(animatedCam)
										ENDIF
										
										RENDER_SCRIPT_CAMS(TRUE, FALSE)
									ENDIF
									
									#IF IS_DEBUG_BUILD
									DRAW_DEBUG_TEXT_2D(GET_TASK_MOVE_NETWORK_STATE(wrestlers[ARMWRESTLERID_HOME].otherWrestler), <<0.5, 0.25, 0>>)
									DRAW_DEBUG_TEXT_2D(GET_TASK_MOVE_NETWORK_STATE(wrestlers[ARMWRESTLERID_AWAY].otherWrestler), <<0.5, 0.28, 0>>)
									#ENDIF
									
									IF ARM_IS_CAMERA_ACTIVE(cameraSet, ARMCAMERAID_NEUTRAL)
									AND (NOT ARM_IS_CAMERA_ACTIVE(cameraSet, ARMCAMERAID_WRESTLE))
									AND (GET_TIMER_IN_SECONDS(countdownTimer) > 3.0)
										ARM_SET_ACTIVE_CAMERA(cameraSet, ARMCAMERAID_WRESTLE, TRUE)//, TRUE)
									ENDIF	
									
									// Move state change to Running -----------------------------------------------------------------------
									IF (GET_TIMER_IN_SECONDS(countdownTimer) > 3.75)
									AND DOES_ENTITY_EXIST(wrestlers[ARMWRESTLERID_HOME].otherWrestler)
									AND NOT IS_PED_INJURED(wrestlers[ARMWRESTLERID_HOME].otherWrestler)
									AND DOES_ENTITY_EXIST(wrestlers[ARMWRESTLERID_AWAY].otherWrestler)
									AND NOT IS_PED_INJURED(wrestlers[ARMWRESTLERID_AWAY].otherWrestler)
									AND IS_TASK_MOVE_NETWORK_ACTIVE(wrestlers[ARMWRESTLERID_HOME].otherWrestler)
									AND IS_TASK_MOVE_NETWORK_ACTIVE(wrestlers[ARMWRESTLERID_AWAY].otherWrestler)
									AND NOT ARE_STRINGS_EQUAL(GET_TASK_MOVE_NETWORK_STATE(wrestlers[ARMWRESTLERID_HOME].otherWrestler), "Running")
									AND NOT ARE_STRINGS_EQUAL(GET_TASK_MOVE_NETWORK_STATE(wrestlers[ARMWRESTLERID_AWAY].otherWrestler), "Running")
										
										IF IS_TASK_MOVE_NETWORK_READY_FOR_TRANSITION(wrestlers[ARMWRESTLERID_HOME].otherWrestler)
											IF REQUEST_TASK_MOVE_NETWORK_STATE_TRANSITION(wrestlers[ARMWRESTLERID_HOME].otherWrestler, "Running")
												CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: Running requested for ARMWRESTLERID_HOME")
												SET_TASK_MOVE_NETWORK_SIGNAL_FLOAT(wrestlers[ARMWRESTLERID_HOME].otherWrestler, "Phase", 0.5)
											ENDIF
										ELSE
											CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: ARMWRESTLERID_HOME not ready for a transition to running")
										ENDIF
										
										IF IS_TASK_MOVE_NETWORK_READY_FOR_TRANSITION(wrestlers[ARMWRESTLERID_AWAY].otherWrestler)
											IF REQUEST_TASK_MOVE_NETWORK_STATE_TRANSITION(wrestlers[ARMWRESTLERID_AWAY].otherWrestler, "Running")
												CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: Running requested for ARMWRESTLERID_AWAY")
												SET_TASK_MOVE_NETWORK_SIGNAL_FLOAT(wrestlers[ARMWRESTLERID_AWAY].otherWrestler, "Phase", 0.5)
											ENDIF
										ELSE
											CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: ARMWRESTLERID_AWAY not ready for a transition to running")
										ENDIF
									ELSE
										IF (GET_GAME_TIMER() % 2000) < 50
											CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: ARMWRESTLERID_HOME current state is ", GET_TASK_MOVE_NETWORK_STATE(wrestlers[ARMWRESTLERID_HOME].otherWrestler))
											CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: ARMWRESTLERID_AWAY current state is ", GET_TASK_MOVE_NETWORK_STATE(wrestlers[ARMWRESTLERID_AWAY].otherWrestler))
										ENDIF
									ENDIF
									// End Move state change to Running -------------------------------------------------------------------
									
									IF NOT ARM_SERVER_GET_FLAG(serverData, ARMSERVERFLAG_COUNTDOWN_DONE)
										IF (GET_CURRENT_GAMEMODE() = GAMEMODE_FM)
											UPDATE_MINIGAME_COUNTDOWN_UI(ui.uiCountdown)
										ELSE
											UPDATE_MINIGAME_COUNTDOWN_UI_BRANDED(ui.uiCountdown)
										ENDIF
									ENDIF
									
									IF ARE_STRINGS_EQUAL(GET_TASK_MOVE_NETWORK_STATE(wrestlers[ARMWRESTLERID_HOME].otherWrestler), "Running")
									AND ARE_STRINGS_EQUAL(GET_TASK_MOVE_NETWORK_STATE(wrestlers[ARMWRESTLERID_AWAY].otherWrestler), "Running")
										IF DOES_ENTITY_EXIST(wrestlers[ARMWRESTLERID_HOME].otherWrestler)
										AND NOT IS_PED_INJURED(wrestlers[ARMWRESTLERID_HOME].otherWrestler)
											IF IS_TASK_MOVE_NETWORK_ACTIVE(wrestlers[ARMWRESTLERID_HOME].otherWrestler)
												CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: ARMWRESTLERID_HOME initial score set")
												SET_TASK_MOVE_NETWORK_SIGNAL_FLOAT(wrestlers[ARMWRESTLERID_HOME].otherWrestler, "Phase", 0.5)
											ENDIF
										ENDIF
										IF DOES_ENTITY_EXIST(wrestlers[ARMWRESTLERID_AWAY].otherWrestler)
										AND NOT IS_PED_INJURED(wrestlers[ARMWRESTLERID_AWAY].otherWrestler)
											IF IS_TASK_MOVE_NETWORK_ACTIVE(wrestlers[ARMWRESTLERID_AWAY].otherWrestler)
												CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: ARMWRESTLERID_AWAY initial score set")
												SET_TASK_MOVE_NETWORK_SIGNAL_FLOAT(wrestlers[ARMWRESTLERID_AWAY].otherWrestler, "Phase", 0.5)
											ENDIF
										ENDIF
										bSpecRunning = TRUE
									ENDIF
								ELSE								
									CLEANUP_BIG_MESSAGE()
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: Big countdown message cleaned up")
									ARM_SET_UI_FLAG(ui, ARMUIFLAG_SHOW_TUTORIAL, FALSE)
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "Resetting countdown UI...")
									ARM_RESET_COUNTDOWN_UI(ui)
									CANCEL_TIMER(countdownTimer)
									RESTART_TIMER_NOW(wrestleTimer)
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: Restarting wrestleTimer")
								ENDIF
								
								IF NOT IS_PED_INJURED(wrestlers[ARMWRESTLERID_HOME].otherWrestler)
								AND NOT IS_PED_INJURED(wrestlers[ARMWRESTLERID_AWAY].otherWrestler)
									IF IS_TASK_MOVE_NETWORK_ACTIVE(wrestlers[ARMWRESTLERID_HOME].otherWrestler)
										CDEBUG3LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: ARMWRESTLERID_HOME current state is ", GET_TASK_MOVE_NETWORK_STATE(wrestlers[ARMWRESTLERID_HOME].otherWrestler))
									ELSE
										CDEBUG3LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: Home move network is not active")
									ENDIF
									
									IF IS_TASK_MOVE_NETWORK_ACTIVE(wrestlers[ARMWRESTLERID_AWAY].otherWrestler)
										CDEBUG3LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: ARMWRESTLERID_AWAY current state is ", GET_TASK_MOVE_NETWORK_STATE(wrestlers[ARMWRESTLERID_AWAY].otherWrestler))
									ELSE
										CDEBUG3LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: Away move network is not active")
									ENDIF
								ENDIF
							ENDIF
							
							// Gameplay (only when countdown is over).
							IF (NOT IS_TIMER_STARTED(countdownTimer)) OR GET_TIMER_IN_SECONDS_SAFE(countdownTimer) >= 3.0
								
								bHappy = (ARM_GET_SERVER_SCORE(serverData) >= 0.0 AND eSelfID = ARMWRESTLERID_HOME) OR (ARM_GET_SERVER_SCORE(serverData) < 0.0 AND eSelfID = ARMWRESTLERID_AWAY)
								
								ARM_UPDATE_WRESTLERS_SPECTATOR(wrestlers, serverData)
								
								ARM_CONTEXTUAL_CAM_SHAKE(cameraSet, ARM_GET_SERVER_SCORE(serverData), TRUE)
							ENDIF
							
						BREAK
						
						// The match has ended. Ask for a rematch.
						CASE ARMGAMESTATE_WINNER
							
							bSpecRunning = FALSE
							
							IF NOT bArmIdleCamSet
								PLAY_SOUND_FROM_ENTITY(-1, "ARM_WRESTLING_ARM_IMPACT_MASTER", PLAYER_PED_ID())
								SHAKE_CAM(ARM_GET_ACTIVE_CAMERA_OBJECT(cameraSet), "SMALL_EXPLOSION_SHAKE", 0.25)
								bArmIdleCamSet = TRUE
							ENDIF
							
							IF NOT IS_TIMER_STARTED(tutorialTimer)
								RESTART_TIMER_NOW(tutorialTimer)
							ENDIF
							
							IF NOT MPGlobals.DartsData.bResultsDisplayed
								MPGlobals.DartsData.bResultsDisplayed = TRUE
							ENDIF
							
							// Determine our current status.
							// b happy in this case says home win = true, away win = false
							bHappy = (ARM_SERVER_GET_FLAG(serverData, ARMSERVERFLAG_HOMEWIN) AND eSelfID = ARMWRESTLERID_HOME) OR 
									(ARM_SERVER_GET_FLAG(serverData, ARMSERVERFLAG_AWAYWIN) AND eSelfID = ARMWRESTLERID_AWAY)
							
							IF GET_TIMER_IN_SECONDS_SAFE(tutorialTimer) > 0.5
								IF ARM_GET_ARG_TABLE_INDEX(args) != 12
									ARM_WIN_CAM_CYCLE(cameraset, bHappy)
								ELSE
									IF eSelfID = ARMWRESTLERID_AWAY
										ARM_WIN_CAM_CYCLE(cameraset, FALSE)
									ELSE
										ARM_WIN_CAM_CYCLE(cameraset, TRUE)
									ENDIF
								ENDIF
							ENDIF
							
							// See if this was our fastest win ever.
							// Also broadcast the bet data here
							
							CANCEL_TIMER(wrestleTimer)
							
							// See if we are playing our reaction anim.
							IF NOT ARM_GET_CLIENT_FLAG(playerData[iMyParticipantID], ARMCLIENTFLAG_WIN_LOSS_REACT)
								
								// Make sure the quit UI is turned off.
								ARM_SET_UI_FLAG(ui, ARMUIFLAG_SHOW_QUITUI, FALSE)
								
								// Play celebrate/curse anim.
								IF GET_CURRENT_GAMEMODE() != GAMEMODE_FM
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "Playing sound: ", ARM_GET_CROWD_SOUND_FROM_INDEX(iWinSound, bHappy), " from coord: ", table.vCenter)
									PLAY_SOUND_FROM_COORD(-1, ARM_GET_CROWD_SOUND_FROM_INDEX(iWinSound, bHappy), ARM_GET_TABLE_CENTER(table))
								ENDIF
								
								ARM_SET_LAST_METER_VALUE(ui, 0.5)
								
								// Move state change to Results -----------------------------------------------------------------------
								IF NOT IS_ENTITY_DEAD(wrestlers[ARMWRESTLERID_HOME].otherWrestler)
								AND NOT IS_PED_INJURED(wrestlers[ARMWRESTLERID_AWAY].otherWrestler)
									IF IS_TASK_MOVE_NETWORK_ACTIVE(wrestlers[ARMWRESTLERID_HOME].otherWrestler)
									AND IS_TASK_MOVE_NETWORK_ACTIVE(wrestlers[ARMWRESTLERID_AWAY].otherWrestler)
										IF NOT ARE_STRINGS_EQUAL(GET_TASK_MOVE_NETWORK_STATE(wrestlers[ARMWRESTLERID_AWAY].otherWrestler), PICK_STRING(bHappy, "Win", "Loss"))
										AND NOT ARE_STRINGS_EQUAL(GET_TASK_MOVE_NETWORK_STATE(wrestlers[ARMWRESTLERID_HOME].otherWrestler), PICK_STRING(!bHappy, "Win", "Loss"))
											
											IF IS_TASK_MOVE_NETWORK_READY_FOR_TRANSITION(wrestlers[ARMWRESTLERID_HOME].otherWrestler)
												IF REQUEST_TASK_MOVE_NETWORK_STATE_TRANSITION(wrestlers[ARMWRESTLERID_HOME].otherWrestler, PICK_STRING(!bHappy, "Win", "Loss"))
													CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: results requested for ARMWRESTLERID_AWAY")
												ELSE
													CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: results request was denied for ARMWRESTLERID_AWAY")
												ENDIF
											ENDIF
											
											IF IS_TASK_MOVE_NETWORK_READY_FOR_TRANSITION(wrestlers[ARMWRESTLERID_AWAY].otherWrestler)
												IF REQUEST_TASK_MOVE_NETWORK_STATE_TRANSITION(wrestlers[ARMWRESTLERID_AWAY].otherWrestler, PICK_STRING(bHappy, "Win", "Loss"))
													CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: results requested for ARMWRESTLERID_HOME")
												ELSE
													CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: results request was denied for ARMWRESTLERID_HOME")
												ENDIF
											ENDIF
										ENDIF
										
										IF (ARE_STRINGS_EQUAL(GET_TASK_MOVE_NETWORK_STATE(wrestlers[ARMWRESTLERID_AWAY].otherWrestler), PICK_STRING(bHappy, "Win", "Loss"))
										AND ARE_STRINGS_EQUAL(GET_TASK_MOVE_NETWORK_STATE(wrestlers[ARMWRESTLERID_HOME].otherWrestler), PICK_STRING(!bHappy, "Win", "Loss")))
											CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: ARMWRESTLERID_HOME move state: ", GET_TASK_MOVE_NETWORK_STATE(wrestlers[ARMWRESTLERID_HOME].otherWrestler))
											CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: ARMWRESTLERID_AWAY move state: ", GET_TASK_MOVE_NETWORK_STATE(wrestlers[ARMWRESTLERID_AWAY].otherWrestler))
											
											ARM_SET_CLIENT_FLAG(playerData[iMyParticipantID], ARMCLIENTFLAG_WIN_LOSS_REACT, TRUE)
											
										ELSE
											IF (GET_GAME_TIMER() % 1000) < 50
												CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: wrestlers are not at results yet yet")
											ENDIF
										ENDIF
									ELSE
										IF (GET_GAME_TIMER() % 1000) < 50
											CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: wrestlers move network is not active in _winner")
										ENDIF
									ENDIF
								ENDIF
								// End Move state change to Results -------------------------------------------------------------------
							ENDIF
							
							IF GET_TIMER_IN_SECONDS(tutorialTimer) > 0.65
							AND NOT ARM_GET_UI_FLAG(ui, ARMUIFLAG_WHOOSH_PLAYED)
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: whoosh played")
								PLAY_SOUND_FRONTEND(-1, "ARM_WRESTLING_WHOOSH_MASTER")
								ARM_SET_UI_FLAG(ui, ARMUIFLAG_WHOOSH_PLAYED, TRUE)
							ENDIF
							
							// Ask for a rematch.
							IF GET_TIMER_IN_SECONDS(tutorialTimer) > 1.0
							
							// UI STUFF VVV
//								uiResult = ARM_UPDATE_SCORECARD_UI_SPECTATOR(ui, 
//																			ARM_GET_SERVER_WINS(serverData, ARMWRESTLERID_HOME), 
//																			ARM_GET_SERVER_WINS(serverData, ARMWRESTLERID_AWAY), bHappy, 
//																			ARM_GET_PLAYER_NAME(playerData[sParticipantInfo.iPlayers[ARMWRESTLERID_HOME]]), 
//																			ARM_GET_PLAYER_NAME(playerData[sParticipantInfo.iPlayers[ARMWRESTLERID_AWAY]]))
//								
//								IF (fRematchTimer - GET_TIMER_IN_SECONDS(tutorialTimer)) < 10//5
//									fTotalTime = fRematchTimer - GET_TIMER_IN_SECONDS(tutorialTimer)
//									IF fTotalTime < 0
//										fTotalTime = 0
//									ENDIF
//									DRAW_GENERIC_TIMER(ROUND(fTotalTime * 1000), "TIM_TIMER", 0, TIMER_STYLE_DONTUSEMILLISECONDS, -1, PODIUMPOS_NONE, HUDORDER_BOTTOM)
//								ENDIF
											
								ARM_SET_CLIENT_FLAG(playerData[iMyParticipantID], ARMCLIENTFLAG_REQUEST_ANIM, FALSE)
								ARM_SET_CLIENT_FLAG(playerData[iMyParticipantID], ARMCLIENTFLAG_START_ANIM, FALSE)
								
								//resets for arm placement
								ARM_SET_CLIENT_FLAG(playerData[iMyParticipantID], ARMCLIENTFLAG_WINNER_SET, FALSE)
								fLastScore = 0
								fUsedScore = 0
								fScoreInterp = 0
								
								eSpectateMPState = ARMMPSTATE_WAIT_POST_GAME
								
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				
				// Waiting for the server to let us go back to the ready menu.
				CASE ARMMPSTATE_WAIT_POST_GAME
					
					// If server tells us to go back to the menu, do so.
					IF ARM_SERVER_GET_FLAG(serverData, ARMSERVERFLAG_MOVETOMENU)
						//ARM_RESTORE_LAST_PLAY_CAMERA(cameraSet, TRUE)
						CLEAR_HELP()
						CANCEL_TIMER(crowdTimer)
						CANCEL_TIMER(tutorialTimer)
						
						cameraset.armWinCamState = ARMWIN_IDLE_START
						CANCEL_TIMER(cameraset.camTimer)
						CANCEL_TIMER(cameraset.shakeTimer)
						
						bArmIdleCamSet = FALSE
						bRematch = TRUE
						
						ARM_SET_ACTIVE_CAMERA(cameraSet, ARMCAMERAID_WRESTLE, TRUE)//, TRUE)
						
						ARM_SET_UI_FLAG(ui, ARMUIFLAG_WHOOSH_PLAYED, FALSE)
						ARM_SET_UI_FLAG(ui, ARMUIFLAG_SHOW_WAIT_REMATCH, FALSE)
						ARM_SET_UI_FLAG(ui, ARMUIFLAG_SHOW_CONTROLS, FALSE)
						ARM_SET_UI_FLAG(ui, ARMUIFLAG_SETUP_QUITUI, FALSE)
						ARM_SET_UI_FLAG(ui, ARMUIFLAG_SHOW_QUITUI, FALSE)
						ARM_SET_CLIENT_FLAG(playerData[iMyParticipantID], ARMCLIENTFLAG_WIN_LOSS_REACT, FALSE)
						ARM_SET_CLIENT_FLAG(playerData[iMyParticipantID], ARMCLIENTFLAG_POST_ANIM_DONE, FALSE)
						
						eSpectateMPState = ARMMPSTATE_RUNNING
					ELSE
						
						// CAM STUFF VVVVV
						IF ARM_GET_ARG_TABLE_INDEX(args) != 12
							ARM_WIN_CAM_CYCLE(cameraset, bHappy)
						ELSE
							IF eSelfID = ARMWRESTLERID_AWAY
								ARM_WIN_CAM_CYCLE(cameraset, FALSE)
							ELSE
								ARM_WIN_CAM_CYCLE(cameraset, TRUE)
							ENDIF
						ENDIF
						
						IF  IS_TASK_MOVE_NETWORK_ACTIVE(wrestlers[ARMWRESTLERID_HOME].otherWrestler)
						AND IS_TASK_MOVE_NETWORK_ACTIVE(wrestlers[ARMWRESTLERID_AWAY].otherWrestler)
							IF GET_TASK_MOVE_NETWORK_EVENT(wrestlers[ARMWRESTLERID_HOME].otherWrestler, "WinFinished")
							OR GET_TASK_MOVE_NETWORK_EVENT(wrestlers[ARMWRESTLERID_HOME].otherWrestler, "LossFinished")
							OR GET_TASK_MOVE_NETWORK_EVENT(wrestlers[ARMWRESTLERID_AWAY].otherWrestler, "WinFinished")
							OR GET_TASK_MOVE_NETWORK_EVENT(wrestlers[ARMWRESTLERID_AWAY].otherWrestler, "LossFinished")
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: win or loss event just fired off ")
								IF NOT ARM_GET_CLIENT_FLAG(playerData[iMyParticipantID], ARMCLIENTFLAG_POST_ANIM_DONE)
									ARM_SET_CLIENT_FLAG(playerData[iMyParticipantID], ARMCLIENTFLAG_POST_ANIM_DONE, TRUE)
								ENDIF
							ELSE
								IF (GET_GAME_TIMER() % 1000) < 50
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: win or loss event has not fired off in _wait_post_game")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE ARMMPSTATE_TERMINATE_SPLASH
					
					ARM_GET_LOCAL_INPUT(wrestlers[eSelfID].input)
					
					IF HAS_NET_TIMER_STARTED(timeArmAwardFailSafe)
						
						DISABLE_FRONTEND_THIS_FRAME()
							
						// Restore cams.
						RENDER_SCRIPT_CAMS(FALSE, FALSE)
						CLEANUP_BIG_MESSAGE()
						
//							IF (terminateReason = ARMMPTERMINATEREASON_PLAYERLEFT)
//								SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_CUSTOM, "ARMMP_SC_WIN", "ARMMP_TERM_LEFT")
//								INIT_SIMPLE_USE_CONTEXT(armUseContext, FALSE, FALSE, FALSE, TRUE)
//								ADD_SIMPLE_USE_CONTEXT_INPUT(armUseContext, "CMRC_CONT", FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
//								SET_SIMPLE_USE_CONTEXT_FULLSCREEN(armUseContext)
//								leaveState = ARM_MPLEAVESTATE_BIG_MESSAGE
//							ELSE
//								//ARM_UPDATE_TERMINATE_UI(terminateReason)
//								IF terminateReason = ARMMPTERMINATEREASON_ENDGAME
//									PRINT_NOW("ARMMP_TERM_LEFT", 5000, 0)
//								ENDIF
							
							leaveState = ARM_MPLEAVESTATE_LEADERBOARD
//							ENDIF
							
						IF NOT g_TransitionSessionNonResetVars.sPostMissionCleanupData.bRequestLeaderBoardCam
							REQUEST_LEADERBOARD_CAM()
						ELIF IS_LEADERBOARD_CAM_READY(TRUE)
							REINIT_NET_TIMER(timeArmAwardFailSafe)
							SET_RESULT_SCREEN_DISPLAYING_STATE(FALSE)
							
							IF MPGlobals.DartsData.bResultsDisplayed
								MPGlobals.DartsData.bResultsDisplayed = FALSE
							ENDIF
							
							eSpectateMPState = ARMMPSTATE_END_LEADERBOARD
						ENDIF
					ELSE
						START_NET_TIMER(timeArmAwardFailSafe)
					ENDIF
				BREAK
				
				CASE ARMMPSTATE_END_LEADERBOARD
					SWITCH leaveState
					
//						CASE ARM_MPLEAVESTATE_BIG_MESSAGE
//							IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
//							OR HAS_NET_TIMER_EXPIRED(timeArmAwardFailSafe, AWARD_FAILSAFE/2)
//								
//								CLEANUP_BIG_MESSAGE()
//								leaveState = ARM_MPLEAVESTATE_LEADERBOARD
//							ELSE
//								UPDATE_SIMPLE_USE_CONTEXT(armUseContext)
//							ENDIF
//						BREAK
						
						CASE ARM_MPLEAVESTATE_LEADERBOARD
//							IF DISPLAY_ARM_WRESTLING_LEADERBOARD_SPECTATOR(ui, armUseContext, ArmEndscreen, 
//																			ARM_GET_SERVER_WINS(serverData, ARMWRESTLERID_HOME), 
//																			ARM_GET_SERVER_WINS(serverData, ARMWRESTLERID_AWAY), 
//																			ARM_GET_PLAYER_NAME(playerData[sParticipantInfo.iPlayers[ARMWRESTLERID_HOME]]), 
//																			ARM_GET_PLAYER_NAME(playerData[sParticipantInfo.iPlayers[ARMWRESTLERID_AWAY]]),
//																			sTableName)
//							OR HAS_NET_TIMER_EXPIRED(timeArmAwardFailSafe, AWARD_FAILSAFE)
								
//								DEAL_WITH_FM_MATCH_END(FMMC_TYPE_MG_ARM_WRESTLING, ServerData.iMatchHistoryID, missionScriptArgs.mdID.idVariation, 
//														PICK_INT(bHappy, 1, 0), PICK_INT(bHappy, 1, 2))
								
								SET_RESULT_SCREEN_DISPLAYING_STATE(FALSE)
								
								ARM_SCRIPT_CLEANUP(sParticipantInfo, wrestlers, ui, NULL, NULL, crowd.refereeAttachment, 
													ARM_GET_ARG_TABLE_INDEX(args), TRUE, TRUE, (ARM_GET_SERVER_WINS(serverData, eSelfID) > ARM_GET_SERVER_WINS(serverData, eOpponentID)))
//							ENDIF
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
			
		ELSE
		
		// #NORMAL GAMEPLAY HERE VVVVVVVVVVVVVVVVVVVVV
			
			IF sParticipantInfo.iPlayers[eSelfID] = -1
				sParticipantInfo.iPlayers[eSelfID] = iMyParticipantID
			ENDIF
			
			//Uncomment for array overrun debug output
	//		CDEBUG1LN(DEBUG_ARM_WRESTLING, "numPlayers = ", ARM_GET_NUM_WRESTLERS( sParticipantInfo ))
	//		CDEBUG1LN(DEBUG_ARM_WRESTLING, "eSelfID = ", eSelfID)
	//		CDEBUG1LN(DEBUG_ARM_WRESTLING, "iPlayers[eSelfID] = ", sParticipantInfo.iPlayers[eSelfID])
	//		CDEBUG1LN(DEBUG_ARM_WRESTLING, "iMyParticipantID = ", iMyParticipantID)
			
			// If we have a match end event, bail.
			IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE() OR IS_PED_INJURED(PLAYER_PED_ID()) OR IS_PED_IN_COMBAT(PLAYER_PED_ID()) #IF IS_DEBUG_BUILD OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F) #ENDIF
				
				IF ARM_GET_SERVER_GAME_STATE(serverData) < ARMGAMESTATE_WINNER
				AND ARM_GET_PLAYER_MP_STATE(playerData[sParticipantInfo.iPlayers[eSelfID]]) < ARMMPSTATE_WAIT_POST_GAME
				AND (ARM_GET_SERVER_WINS(serverData, eSelfID) = 0)
				AND (ARM_GET_SERVER_WINS(serverData, eOpponentID) = 0)
					CDEBUG1LN(DEBUG_ARM_WRESTLING, "Also giving money back to the player in line 494")
					BROADCAST_BETTING_MISSION_FINISHED(PLAYER_ID())
				ENDIF
				
				IF RECEIVED_EVENT(EVENT_NETWORK_END_MATCH)
					bMinigamePlayerQuitFlag = TRUE
				ENDIF
				
				RELEASE_SCRIPT_AUDIO_BANKS(iWinSound)
				ARM_SCRIPT_CLEANUP(sParticipantInfo, wrestlers, ui, NULL, NULL, crowd.refereeAttachment, ARM_GET_ARG_TABLE_INDEX(args), TRUE, TRUE)
			ENDIF
			
			// Check here for missing player.
			IF ARM_GET_PLAYER_MP_STATE(playerData[iMyParticipantID]) < ARMMPSTATE_TERMINATE_SPLASH
			AND (GET_CURRENT_GAMEMODE() = GAMEMODE_FM AND ARM_GET_NUM_WRESTLERS( sParticipantInfo ) < ENUM_TO_INT(ARMWRESTLERIDS))
				
				CDEBUG1LN(DEBUG_ARM_WRESTLING, "made it into the missing player condition")
				
				CLEAR_HELP()
				STOP_STREAM()
				
				IF ARM_GET_SERVER_GAME_STATE(serverData) < ARMGAMESTATE_WINNER
				AND ARM_GET_PLAYER_MP_STATE(playerData[iMyParticipantID]) < ARMMPSTATE_WAIT_POST_GAME
				AND (ARM_GET_SERVER_WINS(serverData, eSelfID) = 0)
				AND (ARM_GET_SERVER_WINS(serverData, eOpponentID) = 0)
					CDEBUG1LN(DEBUG_ARM_WRESTLING, "Also giving money back to the player in line 508")
					BROADCAST_BETTING_MISSION_FINISHED(PLAYER_ID())
				ENDIF
				
				CLEANUP_BIG_MESSAGE()
				
				CDEBUG1LN(DEBUG_ARM_WRESTLING, "ARM_GET_NUM_WRESTLERS( sParticipantInfo ) < 2 returned true, terminating")
				
				IF ARM_GET_SERVER_GAME_STATE(serverData) = ARMGAMESTATE_WINNER
				OR ARM_GET_PLAYER_MP_STATE(playerData[iMyParticipantID]) = ARMMPSTATE_WAIT_POST_GAME
					CDEBUG1LN(DEBUG_ARM_WRESTLING, "game had already ended, setting terminate reason to ENDGAME")
					terminateReason = ARMMPTERMINATEREASON_ENDGAME
				ELSE
					CDEBUG1LN(DEBUG_ARM_WRESTLING, "game was still in play, setting terminate reason to PLAYERLEFT")
					ARM_SET_CLIENT_FLAG(playerData[iMyParticipantID], ARMCLIENTFLAG_QUIT_DONE, TRUE)
					terminateReason = ARMMPTERMINATEREASON_PLAYERLEFT
				ENDIF
				
				ARM_SET_SERVER_MP_STATE(serverData, ARMMPSTATE_TERMINATE_SPLASH)
				ARM_SET_PLAYER_MP_STATE(playerData[iMyParticipantID], ARMMPSTATE_TERMINATE_SPLASH)
				
			ENDIF
			
			// check for player quit
			IF ARM_GET_PLAYER_MP_STATE(playerData[iMyParticipantID]) < ARMMPSTATE_TERMINATE_SPLASH
			AND sParticipantInfo.iPlayers[eOpponentID] != -1
				IF ARM_GET_PLAYER_MP_STATE(playerData[sParticipantInfo.iPlayers[eOpponentID]]) >= ARMMPSTATE_TERMINATE_SPLASH
					CLEAR_HELP()
					STOP_STREAM()
					
					CDEBUG1LN(DEBUG_ARM_WRESTLING, "made it into the player quit condition")
					
					IF ARM_GET_SERVER_GAME_STATE(serverData) < ARMGAMESTATE_WINNER
					AND ARM_GET_PLAYER_MP_STATE(playerData[iMyParticipantID]) < ARMMPSTATE_WAIT_POST_GAME
					AND (ARM_GET_SERVER_WINS(serverData, eSelfID) = 0)
					AND (ARM_GET_SERVER_WINS(serverData, eOpponentID) = 0)
						CDEBUG1LN(DEBUG_ARM_WRESTLING, "Also giving money back to the player in line 508")
						BROADCAST_BETTING_MISSION_FINISHED(PLAYER_ID())
					ENDIF
					
					CLEANUP_BIG_MESSAGE()
					
					CDEBUG1LN(DEBUG_ARM_WRESTLING, "other player is already in terminate splashe, time to cleanup")
					
					IF ARM_GET_SERVER_GAME_STATE(serverData) = ARMGAMESTATE_WINNER
					OR ARM_GET_PLAYER_MP_STATE(playerData[iMyParticipantID]) = ARMMPSTATE_WAIT_POST_GAME
						CDEBUG1LN(DEBUG_ARM_WRESTLING, "game had already ended, setting terminate reason to ENDGAME")
						terminateReason = ARMMPTERMINATEREASON_ENDGAME
					ELSE
						CDEBUG1LN(DEBUG_ARM_WRESTLING, "game was still in play, setting terminate reason to PLAYERLEFT")
						ARM_SET_CLIENT_FLAG(playerData[iMyParticipantID], ARMCLIENTFLAG_QUIT_DONE, TRUE)
						terminateReason = ARMMPTERMINATEREASON_PLAYERLEFT
					ENDIF
					
					ARM_SET_SERVER_MP_STATE(serverData, ARMMPSTATE_TERMINATE_SPLASH)
					ARM_SET_PLAYER_MP_STATE(playerData[iMyParticipantID], ARMMPSTATE_TERMINATE_SPLASH)
				ENDIF
			ENDIF
				
			IF (GET_GAME_TIMER() % 5000) < 50
				CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling: ARM_GET_NUM_WRESTLERS( sParticipantInfo )  = ", ARM_GET_NUM_WRESTLERS( sParticipantInfo ))
			ENDIF
			
			// Update the volume tool.
			#IF iUSE_Z_VOLUMES
				#IF IS_DEBUG_BUILD
					UPDATE_ZVOLUME_WIDGETS()
				#ENDIF
			#ENDIF
			
			// Player needs to detect flags thrown by the server.
			ARM_PLAYER_PROCESS_SERVER_FLAGS(serverData, playerData[sParticipantInfo.iPlayers[eSelfID]])
			
			IF ARM_GET_PLAYER_MP_STATE(playerData[sParticipantInfo.iPlayers[eSelfID]]) > ARMMPSTATE_SETUP
			//AND ARM_GET_PLAYER_MP_STATE(playerData[sParticipantInfo.iPlayers[eSelfID]]) < ARMMPSTATE_TERMINATE_SPLASH
				IF NOT IS_PED_INJURED(wrestlers[eSelfID].otherWrestler)
					#IF IS_DEBUG_BUILD
					IF (GET_GAME_TIMER() % 3000) < 50
						CDEBUG1LN(DEBUG_ARM_WRESTLING, "************************************")
						CDEBUG1LN(DEBUG_ARM_WRESTLING, "eSelfID: ", eSelfID)
						vTemp = GET_ENTITY_COORDS(PLAYER_PED_ID())
						CDEBUG1LN(DEBUG_ARM_WRESTLING, "PLAYER_PED_ID coords: ", vTemp)
						vTemp = GET_ENTITY_COORDS(wrestlers[eSelfID].otherWrestler)
						CDEBUG1LN(DEBUG_ARM_WRESTLING, "other wrestler coords: ", vTemp)
						vTemp = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(PLAYER_PED_ID(), GET_ENTITY_COORDS(wrestlers[eSelfID].otherWrestler))
						CDEBUG1LN(DEBUG_ARM_WRESTLING, "my offset from their coords: ", vTemp)
						vTemp = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(wrestlers[eSelfID].otherWrestler, GET_ENTITY_COORDS(PLAYER_PED_ID()))
						CDEBUG1LN(DEBUG_ARM_WRESTLING, "their offset from my coords: ", vTemp)
						CDEBUG1LN(DEBUG_ARM_WRESTLING, "vArmWrestleLocation: ", vArmWrestleLocation)
						CDEBUG1LN(DEBUG_ARM_WRESTLING, "fTableRot: ", fTableRot)
						CDEBUG1LN(DEBUG_ARM_WRESTLING, "************************************")
					ENDIF
					#ENDIF
					
					//IF NETWORK_IS_PLAYER_ACTIVE(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(sParticipantInfo.iPlayers[eOpponentID])))
					// making sure both players are active
					IF ARM_GET_NUM_WRESTLERS( sParticipantInfo ) >= ENUM_TO_INT(ARMWRESTLERIDS)
						IF NETWORK_IS_PLAYER_ACTIVE(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(sParticipantInfo.iPlayers[ARMWRESTLERID_HOME])))
						AND NETWORK_IS_PLAYER_ACTIVE(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(sParticipantInfo.iPlayers[ARMWRESTLERID_AWAY])))
							NETWORK_OVERRIDE_COORDS_AND_HEADING(wrestlers[eSelfID].otherWrestler,
																GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), <<fOVERRIDE_x, fOVERRIDE_y, 0.0>>) ,
																WRAP(GET_ENTITY_HEADING(PLAYER_PED_ID()) + fOVERRIDE_heading, 0.0, 360.0))
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			// Player acts according to local game state.
			SWITCH ARM_GET_PLAYER_MP_STATE(playerData[sParticipantInfo.iPlayers[eSelfID]])
				// Init streaming, set up cameras, etc.
				CASE ARMMPSTATE_INIT
					// Wait until the server moves beyond the init state.
					IF ARM_GET_SERVER_MP_STATE(serverData) > ARMMPSTATE_INIT
						ARM_SET_ARG_TABLE_INDEX(args, ARM_GET_SERVER_TABLE_INDEX(serverData))
						ARM_GET_TABLE_DATA(table.vCenter, table.fRotation, ARM_GET_ARG_TABLE_INDEX(args), TRUE)
						//ARM_GET_CROWD_MODEL_DATA(args.crowdModels, GET_PLAYER_TEAM(PLAYER_ID()))
						
						DISABLE_ALL_MP_HUD()
						DISABLE_SELECTOR()
						
						ARM_INIT_GAME(args, table, assets, ui, ARM_GET_ARG_TABLE_INDEX(args))
						#IF IS_DEBUG_BUILD
							ARM_SETUP_DEBUG()
						#ENDIF
						
						// Walk to the table location.
						DISABLE_CELLPHONE(TRUE)
						IF NOT IS_PLAYER_DEAD(PLAYER_ID())
							SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
						ENDIF
						
						IF GET_CURRENT_GAMEMODE() = GAMEMODE_FM
							ARM_GET_TABLE_DATA(vArmWrestleLocation, fTableRot, ARM_GET_ARG_TABLE_INDEX(args), TRUE) //FALSE)
						ELSE	
	//						IF GET_PLAYER_TEAM(PLAYER_ID()) = TEAM_CRIM_GANG_1
	//							ARM_GET_TABLE_DATA(vArmWrestleLocation, fTableRot, 1, FALSE)
	//						ELIF GET_PLAYER_TEAM(PLAYER_ID()) = TEAM_CRIM_GANG_2
	//							ARM_GET_TABLE_DATA(vArmWrestleLocation, fTableRot, 2, FALSE)
	//						ENDIF
						ENDIF
						
						fMyPreferenceLevel = GET_ARM_WRESTLING_POS_PREFERENCE(GET_ENTITY_COORDS(PLAYER_PED_ID()), vArmWrestleLocation, ARM_GET_TABLE_WRESTLER_POS(table, TRUE, ARM_IS_PLAYER_LARGE()))
						
						BROADCAST_ARM_WRESTLE_POSITIONS(fMyPreferenceLevel, PLAYER_ID(), NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(sParticipantInfo.iPlayers[eOpponentID])))
						
						CDEBUG1LN(DEBUG_ARM_WRESTLING, "~~~~~~~~~~~AM_Armwrestling.sc: vArmWrestleLocation is now ", vArmWrestleLocation)
						
						CLEAR_AREA(table.vCenter, 4.0, TRUE)
						
						IF HAS_ADDITIONAL_TEXT_LOADED(MINIGAME_TEXT_SLOT)
							ARM_SET_PLAYER_MP_STATE(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMMPSTATE_TRAVEL)
						ELSE
							CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: ARM_GET_PLAYER_MP_STATE: FAILED TO LOAD MINIGAME_TEXT_SLOT!!")
							PRINTNL()
						ENDIF
					ENDIF
				BREAK
				
				CASE ARMMPSTATE_TRAVEL
					IF (MPGlobals.WrestleData.fBroadcastedDesire = 0.0)
						IF NOT IS_TIMER_STARTED(syncPosTimer)
							CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: MPGlobals.WrestleData.fBroadcastedDesire = 0.0;  THIS IS BAD!!")
							PRINTNL()
							RESTART_TIMER_NOW(syncPosTimer)
						ENDIF
					ELSE
						
						IF IS_TIMER_STARTED(syncPosTimer)
							CANCEL_TIMER(syncPosTimer)
						ENDIF
						
						bHome = fMyPreferenceLevel > MPGlobals.WrestleData.fBroadcastedDesire
						
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							vDestination = ARM_GET_TABLE_WRESTLER_POS(table, bHome, ARM_IS_PLAYER_LARGE())
							ARM_INIT_CAMERAS(cameraSet, table, vDestination, ARM_GET_ARG_TABLE_INDEX(args))
							IF GET_CURRENT_GAMEMODE() != GAMEMODE_FM
								TASK_FOLLOW_NAV_MESH_TO_COORD(PLAYER_PED_ID(), vDestination, PEDMOVEBLENDRATIO_WALK, 3500, 0.10, ENAV_DEFAULT, GET_HEADING_BETWEEN_VECTORS(vDestination, vArmWrestleLocation))
							ENDIF
							CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: My fMyPreferenceLevel: ", fMyPreferenceLevel, "; My MPGlobals.WrestleData.fBroadcastedDesire: ", MPGlobals.WrestleData.fBroadcastedDesire)
							CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: Player_Ped is going to: ", vDestination)						
							ARM_SET_PLAYER_MP_STATE(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMMPSTATE_SETUP)
						ENDIF
					ENDIF
					
					// if both players are stuck, try re broadcasting the preference level after 3 seconds
					IF IS_TIMER_STARTED(syncPosTimer)
					AND GET_TIMER_IN_SECONDS(syncPosTimer) > 1.5
						CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: Re sending preference level in ARMMPSTATE_TRAVEL")
						fMyPreferenceLevel = GET_ARM_WRESTLING_POS_PREFERENCE(GET_ENTITY_COORDS(PLAYER_PED_ID()), vArmWrestleLocation, ARM_GET_TABLE_WRESTLER_POS(table, TRUE, ARM_IS_PLAYER_LARGE()))
						BROADCAST_ARM_WRESTLE_POSITIONS(fMyPreferenceLevel, PLAYER_ID(), NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(sParticipantInfo.iPlayers[eOpponentID])))
						RESTART_TIMER_NOW(syncPosTimer)
					ENDIF
				BREAK
				
				// Put props/wrestlers/cameras into place.
				CASE ARMMPSTATE_SETUP
					IF ARM_IS_STREAMING_COMPLETE(assets, ui)
						IF CAN_REGISTER_MISSION_OBJECTS(iNETWORK_OBJECTS)
							IF NOT IS_TIMER_STARTED(streamTimer)
								START_TIMER_NOW(streamTimer)
							ENDIF
							
							ARM_SET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMCLIENTFLAG_MATCH_MADE, TRUE)
							
							IF GET_TIMER_IN_SECONDS(streamTimer) > 1.5 //((NOT IS_PED_INJURED(PLAYER_PED_ID())) AND GET_ENTITY_DISTANCE_FROM_LOCATION(PLAYER_PED_ID(), ARM_GET_TABLE_CENTER(table)) < 1.4) OR GET_TIMER_IN_SECONDS(streamTimer) > 30.0
							//IF (NOT IS_PED_INJURED(PLAYER_PED_ID())) AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) <> PERFORMING_TASK
							//IF (NOT IS_PED_INJURED(PLAYER_PED_ID())) AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_TO_COORD_ANY_MEANS) <> PERFORMING_TASK
								
								ARM_SETUP_WRESTLERS(wrestlers, table, cameraSet, FALSE, TO_FLOAT(ENUM_TO_INT(eOpponentID)), TO_FLOAT(ENUM_TO_INT(eSelfID)))//fMyPreferenceLevel, MPGlobals.WrestleData.fBroadcastedDesire)
								
								wrestlers[eSelfID].otherWrestler = GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(sParticipantInfo.iPlayers[eOpponentID]))) 
								
								IF DOES_ENTITY_EXIST(wrestlers[eSelfID].otherWrestler)
								AND NOT IS_PED_INJURED(wrestlers[eSelfID].otherWrestler)
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: OTHER ped ID EXISTS AND IS NOT INJURED")
									
									IF IS_PED_WEARING_HIGH_HEELS(wrestlers[eSelfID].otherWrestler)
										CDEBUG1LN(DEBUG_ARM_WRESTLING, "Opponent is wearing high heels!!")
									ELSE
										CDEBUG1LN(DEBUG_ARM_WRESTLING, "Opponent is not wearing high heels...")
									ENDIF
									
									IF NOT IS_PED_INJURED(PLAYER_PED_ID())
										IF IS_PED_WEARING_HIGH_HEELS(PLAYER_PED_ID())
											CDEBUG1LN(DEBUG_ARM_WRESTLING, "Player is wearing high heels!!")
										ELSE
											CDEBUG1LN(DEBUG_ARM_WRESTLING, "Player is not wearing high heels...")
										ENDIF
									ENDIF
									
								ELSE
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: OTHER ped does NOT exist")
								ENDIF
								
								RESTART_TIMER_NOW(streamTimer)
								
								//CDEBUG1LN(DEBUG_ARM_WRESTLING, "Playing crowd stream from: ", table.vCenter)
								//PLAY_STREAM_FROM_POSITION(ARM_GET_TABLE_CENTER(table))
								
								IF GET_CURRENT_GAMEMODE() != GAMEMODE_FM
									// insert cnc welcome here
								ENDIF
								
								IF eSelfID = ARMWRESTLERID_HOME
									TASK_MOVE_NETWORK_BY_NAME(PLAYER_PED_ID(), "arm_wrestling_sweep_paired_a_rev3", 0, TRUE, "mini@arm_wrestling")
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: MoVE network arm__wrestling_sweep_paired_a_rev3 setup for self")
								ELSE
									TASK_MOVE_NETWORK_BY_NAME(PLAYER_PED_ID(), "arm_wrestling_sweep_paired_b_rev3", 0, TRUE, "mini@arm_wrestling")
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: MoVE network arm__wrestling_sweep_paired_b_rev3 setup for self")
								ENDIF
								
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: ", GET_ARM_RADIO_EMITTER_NAME(ARM_GET_ARG_TABLE_INDEX(args)))
								SET_STATIC_EMITTER_ENABLED(GET_ARM_RADIO_EMITTER_NAME(ARM_GET_ARG_TABLE_INDEX(args)), TRUE)
								
								INCREMENT_MP_INT_PLAYER_STAT(MPPLY_ARMWRESTLING_TOTAL_MATCH)
								SET_BIT_FOR_FRANKIE_SAYS_ACHIEVEMENT(ACH45_ARMWRESTLING)
								iArmStats[ARM_STAT_NUM_MATCHES]++
								
								RENDER_SCRIPT_CAMS(FALSE, FALSE)
								
								ARM_SET_PLAYER_MP_STATE(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMMPSTATE_SETUP_POST)
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				// Wait here for server.
				CASE ARMMPSTATE_SETUP_POST
					
					SET_ON_JOB_INTRO(TRUE)
					
					IF NOT ( (GET_CURRENT_GAMEMODE() != GAMEMODE_FM) AND ARM_GET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMCLIENTFLAG_SYNC_COMPLETE) 
						  OR (GET_CURRENT_GAMEMODE() = GAMEMODE_FM) AND ARM_GET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMCLIENTFLAG_AT_TUTORIAL) )
						
						ARM_SET_UI_FLAG(ui, ARMUIFLAG_SETUP_WAIT_READY, FALSE)
						
	//					//Freeze the wrestler here after he's in place
	//					IF IS_ENTITY_ATTACHED(PLAYER_PED_ID())
	//						DETACH_ENTITY(PLAYER_PED_ID())
	//						FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
	//					ENDIF
						
						CANCEL_TIMER(streamTimer)
						
						IF GET_CURRENT_GAMEMODE() != GAMEMODE_FM
							ARM_SET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMCLIENTFLAG_SYNC_COMPLETE, TRUE)
						ELSE
							IF IS_TASK_MOVE_NETWORK_ACTIVE(PLAYER_PED_ID())
							AND IS_TASK_MOVE_NETWORK_ACTIVE(wrestlers[eSelfID].otherWrestler)
								//RESTART_TIMER_NOW(streamTimer)
								ARM_SET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMCLIENTFLAG_AT_TUTORIAL, TRUE)
								
								IF g_bMGIntroSwitch // GET_MP_BOOL_CHARACTER_STAT(MP_STAT_ARMWRESTLING_SEEN_TUTORIAL)
									ARM_SET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMCLIENTFLAG_TUTORIAL_DONE, TRUE)
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: Tutorial will be skipped")
								ELSE
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: Tutorial will not be skipped")
								ENDIF
							
							ELSE
								IF (GET_GAME_TIMER() % 1000) < 50
									IF NOT IS_TASK_MOVE_NETWORK_ACTIVE(PLAYER_PED_ID())
										CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: Move network for self is not active yet")
									ENDIF
									
									IF NOT IS_TASK_MOVE_NETWORK_ACTIVE(wrestlers[eSelfID].otherWrestler)
										CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: Move network for the other player is not active yet")
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						
						
						IF GET_CURRENT_GAMEMODE() = GAMEMODE_FM
							IF NOT ARM_GET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMCLIENTFLAG_AT_TUTORIAL)
								IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
								AND NOT IS_PED_INJURED(wrestlers[eSelfID].otherWrestler)
								
									IF IS_SCREEN_FADED_OUT()
										DISPLAY_RADAR(FALSE) // 2nd take here due to 1367016
										DO_SCREEN_FADE_IN(500)
									ENDIF
								ENDIF
								
								uiResult = ARM_UPDATE_READY_UP_UI(ui, armUseContext, sReadyHelp)
								
								// This player is quitting.
								IF uiResult = ARMUIRETVAL_FALSE
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: early quitter hit this 2")
									
									iArmStats[ARM_STAT_NUM_LOSSES]++
									ARM_SET_PLAYER_MP_STATE(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMMPSTATE_TERMINATE_SPLASH)
								ENDIF
								
							ENDIF
						ELSE
							IF NOT ARM_GET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMCLIENTFLAG_SYNC_COMPLETE)
								IF IS_SCREEN_FADED_OUT()
									DO_SCREEN_FADE_IN(500)
								ENDIF
								
								uiResult = ARM_UPDATE_READY_UP_UI(ui, armUseContext, sReadyHelp)
								
								// This player is quitting.
								IF uiResult = ARMUIRETVAL_FALSE
									
									iArmStats[ARM_STAT_NUM_LOSSES]++
									ARM_SET_PLAYER_MP_STATE(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMMPSTATE_TERMINATE_SPLASH)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					
					
					IF ARM_GET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMCLIENTFLAG_AT_TUTORIAL)
					AND ARM_GET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[eOpponentID]], ARMCLIENTFLAG_AT_TUTORIAL)
					AND NOT ARM_GET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMCLIENTFLAG_SYNC_COMPLETE)
						IF NOT (ARM_GET_CLIENT_FLAG(PlayerData[sParticipantInfo.iPlayers[eSelfID]], ARMCLIENTFLAG_TUTORIAL_DONE)
								AND ARM_GET_CLIENT_FLAG(PlayerData[sParticipantInfo.iPlayers[eOpponentID]], ARMCLIENTFLAG_TUTORIAL_DONE))
						
							//IF ARMWRESTLE_TUTORIAL(playerData, wrestlers, ArmTutorialState, tutorialTimer, cameraSet, introTimer)
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "FINISHED TUTORIAL")
								
								CANCEL_TIMER(streamTimer)
								SET_MP_BOOL_CHARACTER_STAT(MP_STAT_ARMWRESTLING_SEEN_TUTORIAL, TRUE)
								
								IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
								AND NOT IS_PED_INJURED(wrestlers[eSelfID].otherWrestler)
								AND IS_TASK_MOVE_NETWORK_ACTIVE(PLAYER_PED_ID())
								AND IS_TASK_MOVE_NETWORK_ACTIVE(wrestlers[eSelfID].otherWrestler)
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: player init move state: ", GET_TASK_MOVE_NETWORK_STATE(PLAYER_PED_ID()))
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: other init move state: ", GET_TASK_MOVE_NETWORK_STATE(wrestlers[eSelfID].otherWrestler))
								ELSE
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: move not active")
								ENDIF
								
								RENDER_SCRIPT_CAMS(FALSE, FALSE)
								
								ARM_SET_PLAYER_MP_STATE(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMMPSTATE_INTRO_SCENE_ALT)
							//ENDIF
							
						ELSE
							CANCEL_TIMER(streamTimer)
							
							SET_MP_BOOL_CHARACTER_STAT(MP_STAT_ARMWRESTLING_SEEN_TUTORIAL, TRUE)
							
							IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
							AND NOT IS_PED_INJURED(wrestlers[eSelfID].otherWrestler)
							AND IS_TASK_MOVE_NETWORK_ACTIVE(PLAYER_PED_ID())
							AND IS_TASK_MOVE_NETWORK_ACTIVE(wrestlers[eSelfID].otherWrestler)
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: player init move state: ", GET_TASK_MOVE_NETWORK_STATE(PLAYER_PED_ID()))
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: other init move state: ", GET_TASK_MOVE_NETWORK_STATE(wrestlers[eSelfID].otherWrestler))
							ELSE
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: move not active")
							ENDIF
							
							RENDER_SCRIPT_CAMS(FALSE, FALSE)
							
							ARM_SET_PLAYER_MP_STATE(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMMPSTATE_INTRO_SCENE_ALT)
						ENDIF
					ENDIF
					
					// check if other player is stuck*************************************
					IF ARM_GET_PLAYER_MP_STATE(playerData[sParticipantInfo.iPlayers[eOpponentID]]) = ARMMPSTATE_TRAVEL
						IF NOT IS_TIMER_STARTED(syncPosTimer)
							CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: Looks like the other player is stuck, re-sending fMyPreferenceLevel in ARMMPSTATE_SETUP_POST")
							
							fMyPreferenceLevel = GET_ARM_WRESTLING_POS_PREFERENCE(GET_ENTITY_COORDS(PLAYER_PED_ID()), vArmWrestleLocation, ARM_GET_TABLE_WRESTLER_POS(table, TRUE, ARM_IS_PLAYER_LARGE()))
							BROADCAST_ARM_WRESTLE_POSITIONS(fMyPreferenceLevel, PLAYER_ID(), NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(sParticipantInfo.iPlayers[eOpponentID])))
							RESTART_TIMER_NOW(syncPosTimer)
							
						ELIF GET_TIMER_IN_SECONDS(syncPosTimer) > 3.0
							CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: Resetting syncPosTimer")
							PRINTNL()
							CANCEL_TIMER(syncPosTimer)
						ENDIF
						
					ELIF ARM_GET_SERVER_MP_STATE(serverData) >= ARMMPSTATE_RUNNING
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("ARMMP_WAIT_RD")
							CLEAR_HELP()
						ENDIF
						ARM_CLEAR_BUCKETS(playerData[sParticipantInfo.iPlayers[eSelfID]])
						ARM_SET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMCLIENTFLAG_SYNC_COMPLETE, FALSE)
						ARM_SET_PLAYER_MP_STATE(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMMPSTATE_RUNNING)
					ENDIF
					//*******************************************************************
				BREAK
				
				CASE ARMMPSTATE_INTRO_SCENE_ALT
					
					IF IS_SCREEN_FADED_OUT()
						DISPLAY_RADAR(FALSE) // 2nd take here due to 1367016
						DO_SCREEN_FADE_IN(500)
					ENDIF
					
					//IF NOT g_bMGIntroSwitch //MPGlobals.WrestleData.bTutorialShown
					IF NOT ( ARM_GET_CLIENT_FLAG(PlayerData[sParticipantInfo.iPlayers[eSelfID]], ARMCLIENTFLAG_TUTORIAL_DONE)
							AND ARM_GET_CLIENT_FLAG(PlayerData[sParticipantInfo.iPlayers[eOpponentID]], ARMCLIENTFLAG_TUTORIAL_DONE) )
						fCamRot = table.fRotation
						bLongerIntro = TRUE
						sWhichWalk = "Walk"
						sWhichCam = "aw_ig_intro_cam"
						CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: playing longer intro")
					ELSE
						fCamRot = ARM_GET_INTRO_CAMERA_DATA(ARM_GET_ARG_TABLE_INDEX(args), table.fRotation)
						bLongerIntro = FALSE
						sWhichWalk = "AltWalk"
						sWhichCam = "aw_ig_intro_alt1_cam"
						CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: playing shorter intro")
					ENDIF
					
					IF NOT IS_PED_INJURED(wrestlers[eSelfID].otherWrestler)
					AND IS_TASK_MOVE_NETWORK_ACTIVE(PLAYER_PED_ID())
					AND IS_TASK_MOVE_NETWORK_ACTIVE(wrestlers[eSelfID].otherWrestler)
						IF NOT ARE_STRINGS_EQUAL(GET_TASK_MOVE_NETWORK_STATE(wrestlers[eSelfID].otherWrestler), sWhichWalk)
							IF IS_TASK_MOVE_NETWORK_READY_FOR_TRANSITION(wrestlers[eSelfID].otherWrestler)
								IF REQUEST_TASK_MOVE_NETWORK_STATE_TRANSITION(wrestlers[eSelfID].otherWrestler, sWhichWalk)
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: requested for otherWrestler: ", sWhichWalk)
								ELSE
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: request was denied for otherWrestler: ", sWhichWalk)
								ENDIF
							ELSE
								IF (GET_GAME_TIMER() % 1000) < 50
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: otherWrestler not ready for transition in beginning: ", sWhichWalk)
								ENDIF
							ENDIF
						ENDIF
						
						IF NOT ARE_STRINGS_EQUAL(GET_TASK_MOVE_NETWORK_STATE(PLAYER_PED_ID()), sWhichWalk)
							IF IS_TASK_MOVE_NETWORK_READY_FOR_TRANSITION(PLAYER_PED_ID())
								IF REQUEST_TASK_MOVE_NETWORK_STATE_TRANSITION(PLAYER_PED_ID(), sWhichWalk)
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: requested for PLAYER_PED_ID: ", sWhichWalk)
								ELSE
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: request was denied for PLAYER_PED_ID: ", sWhichWalk)
								ENDIF
							ELSE
								IF (GET_GAME_TIMER() % 1000) < 50
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: PLAYER_PED_ID not ready for transition in beginning: ", sWhichWalk)
								ENDIF
							ENDIF
						ENDIF
						
						IF (ARE_STRINGS_EQUAL(GET_TASK_MOVE_NETWORK_STATE(PLAYER_PED_ID()), sWhichWalk)
						AND ARE_STRINGS_EQUAL(GET_TASK_MOVE_NETWORK_STATE(wrestlers[eSelfID].otherWrestler), sWhichWalk))
							
							CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: player move state: ", GET_TASK_MOVE_NETWORK_STATE(PLAYER_PED_ID()))
							CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: other  move state: ", GET_TASK_MOVE_NETWORK_STATE(wrestlers[eSelfID].otherWrestler))
							
							iLocalSCene2 = CREATE_SYNCHRONIZED_SCENE(table.vCenter, << 0, 0, fCamRot + 180.0 >>)
							animatedCam = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", FALSE)
							PLAY_SYNCHRONIZED_CAM_ANIM(animatedCam, iLocalSCene2, sWhichCam, "mini@arm_wrestling")
							SET_CAM_ACTIVE(animatedCam, TRUE)
							RENDER_SCRIPT_CAMS(TRUE, FALSE)
							
							IF NOT IS_ENTITY_VISIBLE(PLAYER_PED_ID())
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: had to set the wrestlers visible at the sync scene")
								SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
							ENDIF
							
							CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: local sync scene is ", iLocalSCene2)
							
							RESTART_TIMER_NOW(introTimer)
							
							CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: setting FM Match Start as the intro starts")
							DEAL_WITH_FM_MATCH_START(FMMC_TYPE_MG_ARM_WRESTLING, serverData.iMatchHistoryID, serverData.iMatchType)
							
							ARM_SET_PLAYER_MP_STATE(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMMPSTATE_INTRO_SCENE_ALT_END)
							
						ELSE
							IF (GET_GAME_TIMER() % 1000) < 50
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: wrestlers are not at Walk yet")
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: player move state: ", GET_TASK_MOVE_NETWORK_STATE(PLAYER_PED_ID()))
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: other  move state: ", GET_TASK_MOVE_NETWORK_STATE(wrestlers[eSelfID].otherWrestler))
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE ARMMPSTATE_INTRO_SCENE_ALT_END
					IF DOES_CAM_EXIST(animatedCam)
						IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalSCene2)
							IF GET_SYNCHRONIZED_SCENE_PHASE(iLocalSCene2) = 1.0
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: iLocalSCene2 is ", iLocalSCene2)
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: iLocalSCene2 phase is ", GET_SYNCHRONIZED_SCENE_PHASE(iLocalSCene2))
								
								
								ARM_CLEAR_BUCKETS(playerData[sParticipantInfo.iPlayers[eSelfID]])
								ARM_SET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMCLIENTFLAG_SYNC_COMPLETE, TRUE)
								//ARM_SET_PLAYER_MP_STATE(playerData[eSelfID], ARMMPSTATE_RUNNING)
								
							ELSE
								IF bLongerIntro //NOT g_bMGIntroSwitch //NOT MPGlobals.WrestleData.bTutorialShown
									IF GET_SYNCHRONIZED_SCENE_PHASE(iLocalSCene2) > 0.05
									AND NOT bHelp1
										//PRINT_HELP("ARMMP_HOW_TO_1")
//										sOpponentName = ARM_GET_PLAYER_NAME(playerData[sParticipantInfo.iPlayers[eOpponentID]])
//										PRINT_HELP_WITH_PLAYER_NAME("ARMMP_HOW_TO_1", sOpponentName, HUD_COLOUR_RED)
										PRINT_HELP("ARMMP_HOW_TO_12")
										bHelp1 = TRUE
									ELIF GET_SYNCHRONIZED_SCENE_PHASE(iLocalSCene2) > 0.3
									AND NOT bHelp2
										PRINT_HELP("ARMMP_HOW_TO_2")
										bHelp2 = TRUE
									ELIF GET_SYNCHRONIZED_SCENE_PHASE(iLocalSCene2) > 0.6
									AND NOT bHelp3
										IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
											PRINT_HELP("ARMMP_HOW_TO_4")
										ELSE
											PRINT_HELP("ARMMP_HOW_TO_3")
										ENDIF
										bHelp3 = TRUE
										g_bMGIntroSwitch = TRUE
									ENDIF
								ELSE
									IF GET_SYNCHRONIZED_SCENE_PHASE(iLocalSCene2) > 0.05
									AND NOT bHelp1
										//PRINT_HELP("ARMMP_REGAME")
//										sOpponentName = ARM_GET_PLAYER_NAME(playerData[sParticipantInfo.iPlayers[eOpponentID]])
//										PRINT_HELP_WITH_PLAYER_NAME("ARMMP_REGAME", sOpponentName, HUD_COLOUR_RED)
										PRINT_HELP("ARMMP_REGAME2")
										bHelp1 = TRUE
									ENDIF
								ENDIF
								
								IF (GET_GAME_TIMER() % 1000) < 50
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: iLocalSCene2 is ", iLocalSCene2)
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: iLocalSCene2 phase is ", GET_SYNCHRONIZED_SCENE_PHASE(iLocalSCene2))
								ENDIF
							ENDIF
						ELSE
							IF (GET_GAME_TIMER() % 1000) < 50
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "client: sync scene not running")
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "client: iLocalSCene2 = ", iLocalSCene2)
							ENDIF
							IF NOT IS_TIMER_STARTED(syncSceneFailsafeTimer)
								
							ELIF ((NOT MPGlobals.WrestleData.bTutorialShown) AND GET_TIMER_IN_SECONDS(syncSceneFailsafeTimer) > 13.0)
							OR (MPGlobals.WrestleData.bTutorialShown AND GET_TIMER_IN_SECONDS(syncSceneFailsafeTimer) > 6.0)
								
	//							IF MPGlobals.WrestleData.bTutorialShown
	//								MPGlobals.WrestleData.bTutorialShown = FALSE
	//							ELSE
	//								MPGlobals.WrestleData.bTutorialShown = TRUE
	//							ENDIF
								ARM_CLEAR_BUCKETS(playerData[sParticipantInfo.iPlayers[eSelfID]])
								ARM_SET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMCLIENTFLAG_SYNC_COMPLETE, TRUE)
							ENDIF
						ENDIF
					ENDIF
					
					IF ARM_GET_SERVER_MP_STATE(serverData) >= ARMMPSTATE_RUNNING
						
						IF eSelfID = ARMWRESTLERID_HOME
							TASK_PLAY_ANIM(PLAYER_PED_ID(), "mini@arm_wrestling", "nuetral_idle_a_face", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_SECONDARY)
						ELSE
							TASK_PLAY_ANIM(PLAYER_PED_ID(), "mini@arm_wrestling", "nuetral_idle_b_face", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_SECONDARY)
						ENDIF
						
	//					IF g_bMGIntroSwitch //MPGlobals.WrestleData.bTutorialShown
	//						g_bMGIntroSwitch = FALSE //MPGlobals.WrestleData.bTutorialShown = FALSE
	//						CDEBUG1LN(DEBUG_ARM_WRESTLING, "client: tutorial flag set to false")
	//					ELSE
	//						g_bMGIntroSwitch = TRUE //MPGlobals.WrestleData.bTutorialShown = TRUE
	//						CDEBUG1LN(DEBUG_ARM_WRESTLING, "client: tutorial flag set to true")
	//					ENDIF
						
						SET_ON_JOB_INTRO(FALSE)
						
						ARM_CLEAR_BUCKETS(playerData[sParticipantInfo.iPlayers[eSelfID]])
						ARM_SET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMCLIENTFLAG_SYNC_COMPLETE, FALSE)
						ARM_SET_PLAYER_MP_STATE(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMMPSTATE_RUNNING)
					ENDIF
					
				BREAK
				
				// Wait for the other player to ready up.
				CASE ARMMPSTATE_WAIT_TO_START_GAME
					IF ARM_SERVER_GET_FLAG(serverData, ARMSERVERFLAG_MOVETOGAME)
						
						IF GET_TIMER_IN_SECONDS_SAFE( rematchCamDelay ) > 1.0
							IF NOT ARM_IS_CAMERA_ACTIVE(cameraSet, ARMCAMERAID_WRESTLE)
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: anims have been requested, switch cams")
								ARM_SET_ACTIVE_CAMERA(cameraSet, ARMCAMERAID_WRESTLE, TRUE)//, TRUE)
								CANCEL_TIMER( rematchCamDelay )
							ENDIF
						ENDIF
						
						IF NOT IS_TIMER_STARTED(introTimer)
							RESTART_TIMER_NOW(introTimer)
						ELIF GET_TIMER_IN_SECONDS(introTimer) > 4.0
						
							// Request the win/loss audio if needed.
							iNewWinSound = GET_RANDOM_INT_IN_RANGE(1, 4)
							IF iWinSound <> iNewWinSound
								IF iWinSound <> 0
									RELEASE_NAMED_SCRIPT_AUDIO_BANK(ARM_GET_CROWD_SOUND_BANK_FROM_INDEX(iWinSound, TRUE))
									RELEASE_NAMED_SCRIPT_AUDIO_BANK(ARM_GET_CROWD_SOUND_BANK_FROM_INDEX(iWinSound, FALSE))
								ENDIF
								iWinSound = iNewWinSound
								REQUEST_SCRIPT_AUDIO_BANK(ARM_GET_CROWD_SOUND_BANK_FROM_INDEX(iWinSound, TRUE))
								REQUEST_SCRIPT_AUDIO_BANK(ARM_GET_CROWD_SOUND_BANK_FROM_INDEX(iWinSound, FALSE))
							ENDIF
							
							ARM_SET_LAST_METER_VALUE(ui, 0.5)
							RESTART_TIMER_NOW(wrestleTimer)
							CLEAR_HELP()
							
							RENDER_SCRIPT_CAMS(TRUE, FALSE)
							
							RESTART_TIMER_NOW(countdownTimer)
							IF ARM_SERVER_GET_FLAG(serverData, ARMSERVERFLAG_COUNTDOWN_DONE)
								ARM_SERVER_SET_FLAG(serverData, ARMSERVERFLAG_COUNTDOWN_DONE, FALSE)
							ENDIF
							
							ARM_CLEAR_BUCKETS(playerData[sParticipantInfo.iPlayers[eSelfID]])
							CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: Buckets cleared in wait to start game")
							
							ARM_RESET_LOCAL_BUCKET_SCORE(wrestlers[eSelfID])
							CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: local Buckets cleared in wait post game")
							
							CANCEL_TIMER(introTimer)
							
							ARM_SET_UI_FLAG(ui, ARMUIFLAG_SHOW_TUTORIAL, TRUE)
							ARM_SET_UI_FLAG(ui, ARMUIFLAG_SHOW_WAIT_READY, FALSE)
							ARM_SET_PLAYER_MP_STATE(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMMPSTATE_RUNNING)
						ENDIF
					ELSE					
						ARM_UPDATE_WAIT_READY_UI(ui)					
					ENDIF
				BREAK
				
				// Game is active.
				CASE ARMMPSTATE_RUNNING
					#IF IS_DEBUG_BUILD
						ARM_UPDATE_DEBUG(serverData, table, cameraSet, ENUM_TO_INT(ARM_GET_PLAYER_MP_STATE(playerData[sParticipantInfo.iPlayers[eSelfID]])), ENUM_TO_INT(ARM_GET_SERVER_MP_STATE(serverData)), ENUM_TO_INT(ARM_GET_SERVER_GAME_STATE(serverData)), ARM_GET_WRESTLER_ATTACHMENT_OBJECT(wrestlers[eSelfID]), ARM_GET_REFEREE_ATTACHMENT_OBJECT(crowd), crowd.peds)
					#ENDIF
					
					// Act based on the server state.
					SWITCH ARM_GET_SERVER_GAME_STATE(serverData)
						// Waiting for at least one player to ready up.
						CASE ARMGAMESTATE_READYMENU
							
							// See if the player has readied up.
							ARM_GET_LOCAL_INPUT(wrestlers[eSelfID].input)						
							uiResult = ARM_UPDATE_READY_UI(ui, armUseContext, wrestlers[eSelfID].input)
							
							//# Conor's Change
							SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iArmWrestlingBitSet,args.iTableIndex)
							
							// We need to start our approach anim.
							IF NOT bPlayApproachAnim
								IF bRematch
								// MoVE network script:
									IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
										IF NOT ARM_GET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMCLIENTFLAG_REQUEST_ANIM)
											// Move state change to Approach -----------------------------------------------------------------------
											IF NOT IS_PED_INJURED(wrestlers[eSelfID].otherWrestler)
											AND IS_TASK_MOVE_NETWORK_ACTIVE(PLAYER_PED_ID())
											AND IS_TASK_MOVE_NETWORK_ACTIVE(wrestlers[eSelfID].otherWrestler)
												
												IF NOT ARE_STRINGS_EQUAL(GET_TASK_MOVE_NETWORK_STATE(wrestlers[eSelfID].otherWrestler), "Approach")
													IF IS_TASK_MOVE_NETWORK_READY_FOR_TRANSITION(wrestlers[eSelfID].otherWrestler)
														IF REQUEST_TASK_MOVE_NETWORK_STATE_TRANSITION(wrestlers[eSelfID].otherWrestler, "Approach")
															CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: Approach requested for otherWrestler")
														ELSE
															CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: Approach request was denied for otherWrestler")
														ENDIF
													ELSE
														IF (GET_GAME_TIMER() % 1000) < 50
															CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: otherWrestler not ready for Approach transition in beginning")
														ENDIF
													ENDIF
												ENDIF
												
												IF NOT ARE_STRINGS_EQUAL(GET_TASK_MOVE_NETWORK_STATE(PLAYER_PED_ID()), "Approach")
													IF IS_TASK_MOVE_NETWORK_READY_FOR_TRANSITION(PLAYER_PED_ID())
														IF REQUEST_TASK_MOVE_NETWORK_STATE_TRANSITION(PLAYER_PED_ID(), "Approach")
															CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: Approach requested for PLAYER_PED_ID")
															IF eSelfID = ARMWRESTLERID_HOME
																TASK_PLAY_ANIM(PLAYER_PED_ID(), "mini@arm_wrestling", "idle_to_nuetral_a_face", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_SECONDARY)
															ELSE
																TASK_PLAY_ANIM(PLAYER_PED_ID(), "mini@arm_wrestling", "idle_to_nuetral_b_face", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_SECONDARY)
															ENDIF
														ELSE
															CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: Approach request was denied for PLAYER_PED_ID")
														ENDIF
													ELSE
														IF (GET_GAME_TIMER() % 1000) < 50
															CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: PLAYER_PED_ID not ready for Approach transition")
														ENDIF
													ENDIF
												ENDIF
												
												IF (ARE_STRINGS_EQUAL(GET_TASK_MOVE_NETWORK_STATE(PLAYER_PED_ID()), "Approach")
												AND ARE_STRINGS_EQUAL(GET_TASK_MOVE_NETWORK_STATE(wrestlers[eSelfID].otherWrestler), "Approach"))
													
													CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: player move state: ", GET_TASK_MOVE_NETWORK_STATE(PLAYER_PED_ID()))
													CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: other  move state: ", GET_TASK_MOVE_NETWORK_STATE(wrestlers[eSelfID].otherWrestler))
													ARM_SET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMCLIENTFLAG_REQUEST_ANIM, TRUE)
													RESTART_TIMER_NOW( rematchCamDelay )
													
													//INCREMENT_MP_INT_PLAYER_STAT(MPPLY_ARMWRESTLING_TOTAL_MATCH)
													//iArmStats[ARM_STAT_NUM_MATCHES]++
													
													bPlayApproachAnim = TRUE
												ELSE
													IF (GET_GAME_TIMER() % 1000) < 50
														CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: wrestlers are not at Approach yet")
														CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: player move state: ", GET_TASK_MOVE_NETWORK_STATE(PLAYER_PED_ID()))
														CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: other  move state: ", GET_TASK_MOVE_NETWORK_STATE(wrestlers[eSelfID].otherWrestler))
													ENDIF
												ENDIF
											ELSE
												IF (GET_GAME_TIMER() % 1000) < 50
													CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: move network is not active for wrestlers")
												ENDIF
											ENDIF
											// End Move state change to Approach -----------------------------------------------------------------------
										ENDIF
									ENDIF
								ELSE
									bPlayApproachAnim = TRUE
									ARM_SET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMCLIENTFLAG_REQUEST_ANIM, TRUE)
								ENDIF
	//							IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
	//								ARM_PLAY_CROWD_ANIMATION(crowd, ARMCROWDMEMBER_REFEREE, sAnimDict, sRefIntroAnim, SLOW_BLEND_IN, SLOW_BLEND_OUT, AF_HOLD_LAST_FRAME | AF_NOT_INTERRUPTABLE, 0.0, TRUE)
	//								ARM_FANS_CHEER(crowd)
	//							ENDIF
								
							// We have started our approach anim.
							ELSE
								
								// Play and set the speed of the idle to neutral anim.
								// MoVE network script:
								IF NOT ARM_GET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMCLIENTFLAG_START_ANIM)
									
									ARM_SET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMCLIENTFLAG_START_ANIM, TRUE)
								ENDIF
								
								// ScriptedAnimation script:
								// Set the speed of the ref intro anim.
								IF NETWORK_IS_HOST_OF_THIS_SCRIPT() AND (NOT IS_ENTITY_DEAD(ARM_GET_CROWD_PED_INDEX(crowd, ARMCROWDMEMBER_REFEREE))) AND IS_ENTITY_PLAYING_ANIM(ARM_GET_CROWD_PED_INDEX(crowd, ARMCROWDMEMBER_REFEREE), sAnimDict, sRefIntroAnim)
									SET_ENTITY_ANIM_SPEED(ARM_GET_CROWD_PED_INDEX(crowd, ARMCROWDMEMBER_REFEREE), sAnimDict, sRefIntroAnim, fREF_INTRO_SPEED)
								ENDIF
								
							ENDIF
							
							// The player has readied up.
							IF uiResult = ARMUIRETVAL_TRUE
							AND ARM_GET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMCLIENTFLAG_START_ANIM)
							AND ARM_GET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMCLIENTFLAG_REQUEST_ANIM)
								bPlayApproachAnim = FALSE
								ARM_SET_PLAYER_MP_STATE(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMMPSTATE_WAIT_TO_START_GAME)
								
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: HIT 4")
								
							// The player is quitting.
							ELIF uiResult = ARMUIRETVAL_FALSE
								
								iArmStats[ARM_STAT_NUM_LOSSES]++
								ARM_SET_PLAYER_MP_STATE(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMMPSTATE_TERMINATE_SPLASH)
							ENDIF
						BREAK
						
						// Players are currently armwrestling.
						CASE ARMGAMESTATE_PLAYING
							
							// Show the countdown timer.
							IF IS_TIMER_STARTED(countdownTimer)
								
								IF (GET_TIMER_IN_SECONDS(countdownTimer) < 4.0)
									
									//figure out the stat, then do this
									IF iControlHelpCounter < 5
									AND NOT bShowTutHelp
										IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
											PRINT_HELP("ARMMP_TUT_2")
										ELSE
											PRINT_HELP("ARMMP_TUT_1")
										ENDIF
										bShowTutHelp = TRUE
									ENDIF
									
									IF ARM_IS_CAMERA_ACTIVE(cameraSet, ARMCAMERAID_IDLE)
									OR (NOT ARM_IS_CAMERA_ACTIVE(cameraSet, ARMCAMERAID_NEUTRAL))
										ARM_SET_ACTIVE_CAMERA(cameraSet, ARMCAMERAID_NEUTRAL, TRUE)
										IF DOES_CAM_EXIST(animatedCam)
											DESTROY_CAM(animatedCam)
										ENDIF
										//ARM_SET_ACTIVE_CAMERA(cameraSet, ARMCAMERAID_IDLE, TRUE)
										RENDER_SCRIPT_CAMS(TRUE, FALSE)
									ENDIF
									
									#IF IS_DEBUG_BUILD
									DRAW_DEBUG_TEXT_2D(GET_TASK_MOVE_NETWORK_STATE(PLAYER_PED_ID()), <<0.5, 0.25,0>>)
									#ENDIF
									
									IF ARM_IS_CAMERA_ACTIVE(cameraSet, ARMCAMERAID_NEUTRAL)
									AND (NOT ARM_IS_CAMERA_ACTIVE(cameraSet, ARMCAMERAID_WRESTLE))
									AND (GET_TIMER_IN_SECONDS(countdownTimer) > 3.0)
										ARM_SET_ACTIVE_CAMERA(cameraSet, ARMCAMERAID_WRESTLE, TRUE, TRUE)
									ENDIF	
									
									// Move state change to Running -----------------------------------------------------------------------
									IF (GET_TIMER_IN_SECONDS(countdownTimer) > 3.75)
									AND DOES_ENTITY_EXIST(wrestlers[eSelfID].otherWrestler)
									AND NOT IS_PED_INJURED(wrestlers[eSelfID].otherWrestler)
									AND IS_TASK_MOVE_NETWORK_ACTIVE(PLAYER_PED_ID())
									AND IS_TASK_MOVE_NETWORK_ACTIVE(wrestlers[eSelfID].otherWrestler)
									AND NOT ARE_STRINGS_EQUAL(GET_TASK_MOVE_NETWORK_STATE(PLAYER_PED_ID()), "Running")
									AND NOT ARE_STRINGS_EQUAL(GET_TASK_MOVE_NETWORK_STATE(wrestlers[eSelfID].otherWrestler), "Running")
										
										IF IS_TASK_MOVE_NETWORK_READY_FOR_TRANSITION(wrestlers[eSelfID].otherWrestler)
											IF REQUEST_TASK_MOVE_NETWORK_STATE_TRANSITION(wrestlers[eSelfID].otherWrestler, "Running")
												CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: Running requested for otherWrestler")
												SET_TASK_MOVE_NETWORK_SIGNAL_FLOAT(wrestlers[eSelfID].otherWrestler, "Phase", 0.5)
											ENDIF
										ELSE
											CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: otherWrestler not ready for a transition to running")
										ENDIF
										
										IF IS_TASK_MOVE_NETWORK_READY_FOR_TRANSITION(PLAYER_PED_ID())
											IF REQUEST_TASK_MOVE_NETWORK_STATE_TRANSITION(PLAYER_PED_ID(), "Running")
												CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: Running requested for PLAYER_PED_ID")
												SET_TASK_MOVE_NETWORK_SIGNAL_FLOAT(PLAYER_PED_ID(), "Phase", 0.5)
											ENDIF
										ELSE
											CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: PLAYER_PED_ID not ready for a transition to running")
										ENDIF
									ELSE
										IF (GET_GAME_TIMER() % 2000) < 50
											CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: PLAYER_PED_ID current state is ", GET_TASK_MOVE_NETWORK_STATE(PLAYER_PED_ID()))
											CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: otherWrestler current state is ", GET_TASK_MOVE_NETWORK_STATE(wrestlers[eSelfID].otherWrestler))
										ENDIF
									ENDIF
									// End Move state change to Running -------------------------------------------------------------------
									
									IF (GET_CURRENT_GAMEMODE() = GAMEMODE_FM)
										UPDATE_MINIGAME_COUNTDOWN_UI(ui.uiCountdown)
									ELSE
										UPDATE_MINIGAME_COUNTDOWN_UI_BRANDED(ui.uiCountdown)
									ENDIF
									
									IF ARE_STRINGS_EQUAL(GET_TASK_MOVE_NETWORK_STATE(PLAYER_PED_ID()), "Running")
									AND ARE_STRINGS_EQUAL(GET_TASK_MOVE_NETWORK_STATE(wrestlers[eSelfID].otherWrestler), "Running")
										IF IS_TASK_MOVE_NETWORK_ACTIVE(PLAYER_PED_ID())
											SET_TASK_MOVE_NETWORK_SIGNAL_FLOAT(PLAYER_PED_ID(), "Phase", 0.5)
										ENDIF
										IF DOES_ENTITY_EXIST(wrestlers[eSelfID].otherWrestler)
										AND NOT IS_PED_INJURED(wrestlers[eSelfID].otherWrestler)
											IF IS_TASK_MOVE_NETWORK_ACTIVE(wrestlers[eSelfID].otherWrestler)
												SET_TASK_MOVE_NETWORK_SIGNAL_FLOAT(wrestlers[eSelfID].otherWrestler, "Phase", 0.5)
											ENDIF
										ENDIF
									ENDIF
								ELSE								
									CLEANUP_BIG_MESSAGE()
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: Big countdown message cleaned up")
									ARM_SET_UI_FLAG(ui, ARMUIFLAG_SHOW_TUTORIAL, FALSE)
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "Resetting countdown UI...")
									ARM_RESET_COUNTDOWN_UI(ui)
									CANCEL_TIMER(countdownTimer)
									RESTART_TIMER_NOW(wrestleTimer)
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: Restarting wrestleTimer")
								ENDIF
							ENDIF
							
							// Gameplay (only when countdown is over).
							IF (NOT IS_TIMER_STARTED(countdownTimer)) OR GET_TIMER_IN_SECONDS_SAFE(countdownTimer) >= 3.0
								IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
									IF NOT ARM_SERVER_GET_FLAG(serverData, ARMSERVERFLAG_REF_OUTRO_REQUESTED)
										ARM_SERVER_SET_FLAG(serverData, ARMSERVERFLAG_REF_OUTRO_REQUESTED, TRUE)
									ENDIF
									IF NOT ARM_SERVER_GET_FLAG(serverData, ARMSERVERFLAG_COUNTDOWN_DONE)
										ARM_SERVER_SET_FLAG(serverData, ARMSERVERFLAG_COUNTDOWN_DONE, TRUE)
									ENDIF
								ENDIF
								
								bHappy = (ARM_GET_SERVER_SCORE(serverData) >= 0.0 AND eSelfID = ARMWRESTLERID_HOME) OR (ARM_GET_SERVER_SCORE(serverData) < 0.0 AND eSelfID = ARMWRESTLERID_AWAY)
								
								ARM_UPDATE_WRESTLERS(wrestlers, serverData, playerData, bHappy, sParticipantInfo)
								
								ARM_CONTEXTUAL_CAM_SHAKE(cameraSet, ARM_GET_SERVER_SCORE(serverData), (eSelfID = ARMWRESTLERID_HOME))
								
								// Update UI, check for quitters.
								IF ARM_UPDATE_INGAME_UI(ui, armUseContext, PICK_FLOAT(eSelfID = ARMWRESTLERID_HOME, ARM_GET_SERVER_SCORE(serverData), -ARM_GET_SERVER_SCORE(serverData)))
									SET_VARIABLE_ON_STREAM("ArmWrestlingIntensity", ABSF(ARM_GET_SERVER_SCORE(serverData)))
								
								ELSE
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: Early Quitter hit this")
									ARM_SET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMCLIENTFLAG_QUIT_DONE, TRUE)
									
									bMinigamePlayerQuitFlag = TRUE
									
									SET_VARS_ON_FMMC_EOM_VOTE_PASSING(ciFMMC_EOM_VOTE_STATUS_QUIT, <<0.0, 0.0, 0.0>>, contentID)
									
									iArmStats[ARM_STAT_NUM_LOSSES]++
									ARM_SET_PLAYER_MP_STATE(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMMPSTATE_TERMINATE_SPLASH)
								ENDIF
								
								IF NOT ARM_GET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMCLIENTFLAG_FACE_ANIM_STARTED)
									IF eSelfID = ARMWRESTLERID_HOME
										TASK_PLAY_ANIM(PLAYER_PED_ID(), "mini@arm_wrestling", "expression_idle_a_face", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_SECONDARY | AF_LOOPING)
									ELSE
										TASK_PLAY_ANIM(PLAYER_PED_ID(), "mini@arm_wrestling", "expression_idle_b_face", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_SECONDARY | AF_LOOPING)
									ENDIF
									ARM_SET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMCLIENTFLAG_FACE_ANIM_STARTED, TRUE)
								ENDIF
							ENDIF
							
						BREAK
						
						// The match has ended. Ask for a rematch.
						CASE ARMGAMESTATE_WINNER
							IF NOT bArmIdleCamSet
								PLAY_SOUND_FROM_ENTITY(-1, "ARM_WRESTLING_ARM_IMPACT_MASTER", PLAYER_PED_ID())
								SHAKE_CAM(ARM_GET_ACTIVE_CAMERA_OBJECT(cameraSet), "SMALL_EXPLOSION_SHAKE", 0.25)
								bArmIdleCamSet = TRUE
							ENDIF
							
							IF NOT IS_TIMER_STARTED(tutorialTimer)
								RESTART_TIMER_NOW(tutorialTimer)
							ENDIF
							
							IF NOT MPGlobals.DartsData.bResultsDisplayed
								MPGlobals.DartsData.bResultsDisplayed = TRUE
							ENDIF
							
							// Determine our current status.
							bHappy = (ARM_SERVER_GET_FLAG(serverData, ARMSERVERFLAG_HOMEWIN) AND eSelfID = ARMWRESTLERID_HOME) OR (ARM_SERVER_GET_FLAG(serverData, ARMSERVERFLAG_AWAYWIN) AND eSelfID = ARMWRESTLERID_AWAY)
							
							IF GET_TIMER_IN_SECONDS_SAFE(tutorialTimer) > 0.5
								
								IF NOT bShardSet
									IF bHappy
										PLAY_CELEB_WIN_POST_FX()
										SET_SHARD_BIG_MESSAGE(ui.siShardMessage, "ARMMP_SC_WIN", "", 3000, SHARD_MESSAGE_CENTERED, HUD_COLOUR_WHITE)
									ELSE
										PLAY_CELEB_LOSE_POST_FX()
										SET_SHARD_BIG_MESSAGE(ui.siShardMessage, "ARMMP_SC_LOSE", "", 3000, SHARD_MESSAGE_CENTERED, HUD_COLOUR_RED)
										PLAY_SOUND_FRONTEND ( -1, "LOSER", "HUD_AWARDS")
									ENDIF
									
									bShardSet = TRUE
								ENDIF
								
								IF ARM_GET_ARG_TABLE_INDEX(args) != 12
									ARM_WIN_CAM_CYCLE(cameraset, bHappy)
								ELSE
									IF eSelfID = ARMWRESTLERID_AWAY
										ARM_WIN_CAM_CYCLE(cameraset, FALSE)
									ELSE
										ARM_WIN_CAM_CYCLE(cameraset, TRUE)
									ENDIF
								ENDIF
							ENDIF
							
							// See if this was our fastest win ever.
							// Also broadcast the bet data here
							IF bHappy AND IS_TIMER_STARTED(wrestleTimer)
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: You win, checking mp Stat for fastest time")
								IF GET_MP_INT_CHARACTER_STAT(MP_STAT_FASTEST_ARM_WRESTLING_WIN) > FLOOR(1000.0 * GET_TIMER_IN_SECONDS(wrestleTimer))
								OR (GET_MP_INT_CHARACTER_STAT(MP_STAT_FASTEST_ARM_WRESTLING_WIN) = 0)
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: That was fastest time, setting mp stat : ", FLOOR(1000.0 * GET_TIMER_IN_SECONDS(wrestleTimer)))
									SET_MP_INT_CHARACTER_STAT(MP_STAT_FASTEST_ARM_WRESTLING_WIN, FLOOR(1000.0 * GET_TIMER_IN_SECONDS(wrestleTimer)))
								ELSE
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: That was not fastest time, not setting mp stat")
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: Fastest time = ", GET_MP_INT_CHARACTER_STAT(MP_STAT_FASTEST_ARM_WRESTLING_WIN))
								ENDIF
								
								IF iArmStats[ARM_STAT_FASTEST_WIN] = 0
								OR iArmStats[ARM_STAT_FASTEST_WIN] > FLOOR(1000.0 * GET_TIMER_IN_SECONDS(wrestleTimer))
									iArmStats[ARM_STAT_FASTEST_WIN] = FLOOR(1000.0 * GET_TIMER_IN_SECONDS(wrestleTimer))
								ENDIF
								
								iArmStats[ARM_STAT_TOTAL_TIME] += FLOOR(1000.0 * GET_TIMER_IN_SECONDS(wrestleTimer))
								
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: Sending BROADCAST_BETTING_MISSION_FINISHED with my own ID")
								BROADCAST_BETTING_MISSION_FINISHED(PLAYER_ID())
								
								INCREMENT_MP_INT_PLAYER_STAT(MPPLY_ARMWRESTLING_TOTAL_WINS)
								iArmStats[ARM_STAT_NUM_WINS]++
								
								iArmTotalWins = GET_MP_INT_PLAYER_STAT(MPPLY_ARMWRESTLING_TOTAL_WINS)
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: total wins is now ", iArmTotalWins)
								IF iArmTotalWins > 0
								AND (iArmTotalWins % 5) = 0
									INCREMENT_PLAYER_PED_STAT(CHAR_MULTIPLAYER, PS_STRENGTH, 2)
									CPRINTLN(DEBUG_ARM_WRESTLING, "STRENGTH STAT INCREASED FROM ARM WRESTLING")
								ENDIF
								
								iWinStreak++
								iArmStats[ARM_STAT_STREAK] = iWinStreak
								
								INT iCurrentWins
								iCurrentWins = GET_MP_INT_CHARACTER_STAT(MP_STAT_MOST_ARM_WRESTLING_WINS)
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "iCurrentWins = ", iCurrentWins)
								iCurrentWins++ 
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "iCurrentWins++ = ", iCurrentWins)
								SET_MP_INT_CHARACTER_STAT(MP_STAT_MOST_ARM_WRESTLING_WINS, iCurrentWins)
								
								CANCEL_TIMER(wrestleTimer)
							ELIF IS_TIMER_STARTED(wrestleTimer)
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: Didn't win, not checking mp Stat")
								
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: Sending BROADCAST_BETTING_MISSION_FINISHED with the opponent's ID")
								BROADCAST_BETTING_MISSION_FINISHED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(sParticipantInfo.iPlayers[eOpponentID])))
								
								iWinStreak = 0
								iArmStats[ARM_STAT_NUM_LOSSES]++
								iArmStats[ARM_STAT_TOTAL_TIME] += FLOOR(1000.0 * GET_TIMER_IN_SECONDS(wrestleTimer))
								
								CANCEL_TIMER(wrestleTimer)
							ENDIF
							
							// See if we are playing our reaction anim.
							IF NOT ARM_GET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMCLIENTFLAG_WIN_LOSS_REACT)
								// Record our new win record if we have broken the old one.
	//							IF GET_MP_INT_CHARACTER_STAT(MP_STAT_MOST_ARM_WRESTLING_WINS) < ARM_GET_SERVER_WINS(serverData, eSelfID)
	//								SET_MP_INT_CHARACTER_STAT(MP_STAT_MOST_ARM_WRESTLING_WINS, ARM_GET_SERVER_WINS(serverData, eSelfID))
	//							ENDIF
								
								// Make sure the quit UI is turned off.
								ARM_SET_UI_FLAG(ui, ARMUIFLAG_SHOW_QUITUI, FALSE)
								
								// Play celebrate/curse anim.
								IF GET_CURRENT_GAMEMODE() != GAMEMODE_FM
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "Playing sound: ", ARM_GET_CROWD_SOUND_FROM_INDEX(iWinSound, bHappy), " from coord: ", table.vCenter)
									PLAY_SOUND_FROM_COORD(-1, ARM_GET_CROWD_SOUND_FROM_INDEX(iWinSound, bHappy), ARM_GET_TABLE_CENTER(table))
								ENDIF
								
								ARM_SET_LAST_METER_VALUE(ui, 0.5)
								
								// Move state change to Results -----------------------------------------------------------------------
								IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
								AND NOT IS_PED_INJURED(wrestlers[eSelfID].otherWrestler)
									IF IS_TASK_MOVE_NETWORK_ACTIVE(PLAYER_PED_ID())
									AND IS_TASK_MOVE_NETWORK_ACTIVE(wrestlers[eSelfID].otherWrestler)
										IF NOT ARE_STRINGS_EQUAL(GET_TASK_MOVE_NETWORK_STATE(PLAYER_PED_ID()), PICK_STRING(bHappy, "Win", "Loss"))
										AND NOT ARE_STRINGS_EQUAL(GET_TASK_MOVE_NETWORK_STATE(wrestlers[eSelfID].otherWrestler), PICK_STRING(!bHappy, "Win", "Loss"))
											IF IS_TASK_MOVE_NETWORK_READY_FOR_TRANSITION(wrestlers[eSelfID].otherWrestler)
												IF REQUEST_TASK_MOVE_NETWORK_STATE_TRANSITION(wrestlers[eSelfID].otherWrestler, PICK_STRING(!bHappy, "Win", "Loss"))
													CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: results requested for otherWrestler")
												ELSE
													CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: results request was denied for otherWrestler")
												ENDIF
											ENDIF
											
											IF IS_TASK_MOVE_NETWORK_READY_FOR_TRANSITION(PLAYER_PED_ID())
												IF REQUEST_TASK_MOVE_NETWORK_STATE_TRANSITION(PLAYER_PED_ID(), PICK_STRING(bHappy, "Win", "Loss"))
													IF eSelfID = ARMWRESTLERID_HOME
														TASK_PLAY_ANIM(PLAYER_PED_ID(), "mini@arm_wrestling", PICK_STRING(bHappy, "win_a_ped_a_face", "win_b_ped_a_face"), SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_SECONDARY)
													ELSE
														TASK_PLAY_ANIM(PLAYER_PED_ID(), "mini@arm_wrestling", PICK_STRING(bHappy, "win_b_ped_b_face", "win_a_ped_b_face"), SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_SECONDARY)
													ENDIF
													CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: results requested for PLAYER_PED_ID")
												ELSE
													CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: results request was denied for PLAYER_PED_ID")
												ENDIF
											ENDIF
										ENDIF
										
										IF (ARE_STRINGS_EQUAL(GET_TASK_MOVE_NETWORK_STATE(PLAYER_PED_ID()), PICK_STRING(bHappy, "Win", "Loss"))
										AND ARE_STRINGS_EQUAL(GET_TASK_MOVE_NETWORK_STATE(wrestlers[eSelfID].otherWrestler), PICK_STRING(!bHappy, "Win", "Loss")))
											CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: player move state: ", GET_TASK_MOVE_NETWORK_STATE(PLAYER_PED_ID()))
											CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: other  move state: ", GET_TASK_MOVE_NETWORK_STATE(wrestlers[eSelfID].otherWrestler))
											
											ARM_SET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMCLIENTFLAG_WIN_LOSS_REACT, TRUE)
										ELSE
											IF (GET_GAME_TIMER() % 1000) < 50
												CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: wrestlers are not at results yet yet")
											ENDIF
										ENDIF
									ELSE
										IF (GET_GAME_TIMER() % 1000) < 50
											CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: wrestlers move network is not active in _winner")
										ENDIF
									ENDIF
								ENDIF
								// End Move state change to Results -------------------------------------------------------------------
							ENDIF
							
							IF IS_TASK_MOVE_NETWORK_ACTIVE(PLAYER_PED_ID())
								IF GET_TASK_MOVE_NETWORK_EVENT(PLAYER_PED_ID(), "WinFinished")
								OR GET_TASK_MOVE_NETWORK_EVENT(PLAYER_PED_ID(), "LossFinished")
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: win or loss event just fired off ")
									IF NOT ARM_GET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMCLIENTFLAG_POST_ANIM_DONE)
										IF eSelfID = ARMWRESTLERID_HOME
											TASK_PLAY_ANIM(PLAYER_PED_ID(), "mini@arm_wrestling", "stand_idle_a_face", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_SECONDARY | AF_LOOPING)
										ELSE
											TASK_PLAY_ANIM(PLAYER_PED_ID(), "mini@arm_wrestling", "stand_idle_b_face", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_SECONDARY | AF_LOOPING)
										ENDIF
										ARM_SET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMCLIENTFLAG_POST_ANIM_DONE, TRUE)
									ENDIF
								ELSE
									IF (GET_GAME_TIMER() % 1000) < 50
										CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: win or loss event has not fired off in _winning")
									ENDIF
								ENDIF
							ENDIF
							
							// Ask for a rematch.
							IF GET_TIMER_IN_SECONDS(tutorialTimer) > 4.0 //3.8
							
								ARM_GET_LOCAL_INPUT(wrestlers[eSelfID].input)
								uiResult = ARM_UPDATE_SCORECARD_UI(ui, armUseContext, wrestlers[eSelfID].input, ARM_GET_SERVER_WINS(serverData, eSelfID), 
																	ARM_GET_SERVER_WINS(serverData, eOpponentID), bHappy, 
																	ARM_GET_PLAYER_NAME(playerData[sParticipantInfo.iPlayers[eSelfID]]), 
																	ARM_GET_PLAYER_NAME(playerData[sParticipantInfo.iPlayers[eOpponentID]]))
								
								IF (fRematchTimer - GET_TIMER_IN_SECONDS(tutorialTimer)) < 10//5
									fTotalTime = fRematchTimer - GET_TIMER_IN_SECONDS(tutorialTimer)
									IF fTotalTime < 0
										fTotalTime = 0
									ENDIF
									DRAW_GENERIC_TIMER(ROUND(fTotalTime * 1000), "TIM_TIMER", 0, TIMER_STYLE_DONTUSEMILLISECONDS, -1, PODIUMPOS_NONE, HUDORDER_BOTTOM)
								ENDIF
								
								IF NOT ARM_GET_UI_FLAG(ui, ARMUIFLAG_WHOOSH_PLAYED)
		//							CANCEL_TIMER(faceAnimTimer)
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: whoosh played")
									PLAY_SOUND_FRONTEND(-1, PICK_STRING(bHappy, "WIN", "LOSER"), "HUD_AWARDS")
									ARM_SET_UI_FLAG(ui, ARMUIFLAG_WHOOSH_PLAYED, TRUE)
								ENDIF
								
								SET_INSTRUCTIONAL_BUTTONS_UNDER_HUD_THIS_FRAME()
								
								IF IS_TASK_MOVE_NETWORK_ACTIVE(PLAYER_PED_ID())
									IF (GET_GAME_TIMER() % 2500) < 50
										CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: current state in _winner is ", GET_TASK_MOVE_NETWORK_STATE(PLAYER_PED_ID()))
									ENDIF
									IF GET_TASK_MOVE_NETWORK_EVENT(PLAYER_PED_ID(), "WinFinished")
									OR GET_TASK_MOVE_NETWORK_EVENT(PLAYER_PED_ID(), "LossFinished")
										CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: win or loss event just fired off ")
										IF NOT ARM_GET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMCLIENTFLAG_POST_ANIM_DONE)
											IF eSelfID = ARMWRESTLERID_HOME
												TASK_PLAY_ANIM(PLAYER_PED_ID(), "mini@arm_wrestling", "stand_idle_a_face", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_SECONDARY | AF_LOOPING)
											ELSE
												TASK_PLAY_ANIM(PLAYER_PED_ID(), "mini@arm_wrestling", "stand_idle_b_face", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_SECONDARY | AF_LOOPING)
											ENDIF
											ARM_SET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMCLIENTFLAG_POST_ANIM_DONE, TRUE)
										ENDIF
									ELSE
										IF (GET_GAME_TIMER() % 2500) < 50
											CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: win or loss event has not fired off")
										ENDIF
									ENDIF
								ENDIF
								
								// This player wants a rematch.
								IF uiResult = ARMUIRETVAL_TRUE
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: [ARMGAMESTATE_WINNER] rematch wanted, ARMMPSTATE_WAIT_POST_GAME set here")
									
									RESET_SHARD_BIG_MESSAGE(ui.siShardMessage)
									//STOP_SHARD_BIG_MESSAGE(ui.siShardMessage)
									
									//ARM_PLAY_WRESTLER_ANIMATION(wrestlers[eSelfID], PLAYER_PED_ID(), sAnimDict, PICK_STRING(bHappy, sWaitAnim, sCurseOutro), SLOW_BLEND_IN, SLOW_BLEND_OUT, AF_HOLD_LAST_FRAME | AF_NOT_INTERRUPTABLE)
									ARM_SET_PLAYER_MP_STATE(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMMPSTATE_WAIT_POST_GAME)
									ARM_SET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMCLIENTFLAG_REQUEST_ANIM, FALSE)
									ARM_SET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMCLIENTFLAG_START_ANIM, FALSE)
									
									//resets for arm placement
									ARM_SET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMCLIENTFLAG_WINNER_SET, FALSE)
									fLastScore = 0
									fUsedScore = 0
									fScoreInterp = 0
								
								// This player is quitting.
								ELIF uiResult = ARMUIRETVAL_FALSE
								OR GET_TIMER_IN_SECONDS(tutorialTimer) > 45.0
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: [ARMGAMESTATE_WINNER] ARMMPSTATE_TERMINATE_SPLASH set here")
									ARM_SET_PLAYER_MP_STATE(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMMPSTATE_TERMINATE_SPLASH)
									
								ENDIF
								
								IF ARM_GET_PLAYER_MP_STATE(playerData[sParticipantInfo.iPlayers[eOpponentID]]) = ARMMPSTATE_WAIT_POST_GAME
								AND NOT ARM_GET_UI_FLAG(ui, ARMUIFLAG_SHOW_WAIT_REMATCH)
								AND NOT ARM_GET_UI_FLAG(ui, ARMUIFLAG_SHOW_QUITUI)
//									TEXT_LABEL_63 tlTemp
//									sColoredName = tlTemp
//									sColoredName += "~HUD_COLOUR_RED~"
//									sColoredName += sOpponentName
//									sColoredName += "~s~"
//									PRINT_HELP_WITH_PLAYER_NAME("ARMMP_REQ_RM", sColoredName, HUD_COLOUR_WHITE)
									PRINT_HELP("ARMMP_REQ_RM2")
									ARM_SET_UI_FLAG(ui, ARMUIFLAG_SHOW_WAIT_REMATCH, TRUE)
								ENDIF
							ELSE
								IF bShardSet
									UPDATE_SHARD_BIG_MESSAGE(ui.siShardMessage)
								ENDIF
							ENDIF
							
						BREAK
					ENDSWITCH
				BREAK
				
				// Waiting for the server to let us go back to the ready menu.
				CASE ARMMPSTATE_WAIT_POST_GAME
						
					#IF IS_DEBUG_BUILD
						ARM_UPDATE_DEBUG(serverData, table, cameraSet, ENUM_TO_INT(ARM_GET_PLAYER_MP_STATE(playerData[sParticipantInfo.iPlayers[eSelfID]])), ENUM_TO_INT(ARM_GET_SERVER_MP_STATE(serverData)), ENUM_TO_INT(ARM_GET_SERVER_GAME_STATE(serverData)), ARM_GET_WRESTLER_ATTACHMENT_OBJECT(wrestlers[eSelfID]), ARM_GET_REFEREE_ATTACHMENT_OBJECT(crowd), crowd.peds)
					#ENDIF
	//				IF NOT ARM_IS_CAMERA_ACTIVE(cameraset, ARMCAMERAID_IDLE)
	//					ARM_SET_ACTIVE_CAMERA(cameraset, ARMCAMERAID_IDLE, TRUE)
	//				ENDIF			
					
					// If server tells us to go back to the menu, do so.
					IF ARM_SERVER_GET_FLAG(serverData, ARMSERVERFLAG_MOVETOMENU)
						
						bShardSet = FALSE
						
						IF bHappy
							ANIMPOSTFX_STOP("MP_Celeb_Win")
							ANIMPOSTFX_PLAY("MP_Celeb_Win_Out", 0, FALSE)
						ELSE
							ANIMPOSTFX_STOP("MP_Celeb_Lose")
							ANIMPOSTFX_PLAY("MP_Celeb_Lose_Out", 0, FALSE)
						ENDIF
						
						//ARM_RESTORE_LAST_PLAY_CAMERA(cameraSet, TRUE)
						CLEAR_HELP()
						CANCEL_TIMER(crowdTimer)
						CANCEL_TIMER(tutorialTimer)
						
						cameraset.armWinCamState = ARMWIN_IDLE_START
						CANCEL_TIMER(cameraset.camTimer)
						CANCEL_TIMER(cameraset.shakeTimer)
						
						ARM_CLEAR_BUCKETS(playerData[sParticipantInfo.iPlayers[eSelfID]])
						CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: Buckets cleared in wait post game")
						
						ARM_RESET_LOCAL_BUCKET_SCORE(wrestlers[eSelfID])
						CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: local Buckets cleared in wait post game")
						
						INCREMENT_MP_INT_PLAYER_STAT(MPPLY_ARMWRESTLING_TOTAL_MATCH)
						SET_BIT_FOR_FRANKIE_SAYS_ACHIEVEMENT(ACH45_ARMWRESTLING)
						iArmStats[ARM_STAT_NUM_MATCHES]++
						
						bArmIdleCamSet = FALSE
						bRematch = TRUE
						ARM_SET_UI_FLAG(ui, ARMUIFLAG_WHOOSH_PLAYED, FALSE)
						ARM_SET_UI_FLAG(ui, ARMUIFLAG_SHOW_WAIT_REMATCH, FALSE)
						ARM_SET_UI_FLAG(ui, ARMUIFLAG_SHOW_CONTROLS, FALSE)
						ARM_SET_UI_FLAG(ui, ARMUIFLAG_SETUP_QUITUI, FALSE)
						ARM_SET_UI_FLAG(ui, ARMUIFLAG_SHOW_QUITUI, FALSE)
						ARM_SET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMCLIENTFLAG_WIN_LOSS_REACT, FALSE)
						ARM_SET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMCLIENTFLAG_POST_ANIM_DONE, FALSE)
						ARM_SET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMCLIENTFLAG_FACE_ANIM_STARTED, FALSE)
						ARM_SET_PLAYER_MP_STATE(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMMPSTATE_RUNNING)
					ELSE
						//ARM_UPDATE_CROWD_TAUNTS(crowdTimer)
						
						IF ARM_GET_ARG_TABLE_INDEX(args) != 12
							ARM_WIN_CAM_CYCLE(cameraset, bHappy)
						ELSE
							IF eSelfID = ARMWRESTLERID_AWAY
								ARM_WIN_CAM_CYCLE(cameraset, FALSE)
							ELSE
								ARM_WIN_CAM_CYCLE(cameraset, TRUE)
							ENDIF
						ENDIF
						
						IF IS_TASK_MOVE_NETWORK_ACTIVE(PLAYER_PED_ID())
							IF GET_TASK_MOVE_NETWORK_EVENT(PLAYER_PED_ID(), "WinFinished")
							OR GET_TASK_MOVE_NETWORK_EVENT(PLAYER_PED_ID(), "LossFinished")
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: win or loss event just fired off ")
								IF NOT ARM_GET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMCLIENTFLAG_POST_ANIM_DONE)
									IF eSelfID = ARMWRESTLERID_HOME
										TASK_PLAY_ANIM(PLAYER_PED_ID(), "mini@arm_wrestling", "stand_idle_a_face", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_SECONDARY | AF_LOOPING)
									ELSE
										TASK_PLAY_ANIM(PLAYER_PED_ID(), "mini@arm_wrestling", "stand_idle_b_face", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_SECONDARY | AF_LOOPING)
									ENDIF
									ARM_SET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMCLIENTFLAG_POST_ANIM_DONE, TRUE)
								ENDIF
							ELSE
								IF (GET_GAME_TIMER() % 1000) < 50
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: win or loss event has not fired off in _wait_post_game")
								ENDIF
							ENDIF
						ENDIF
						
						// run waiting for rematch text and script here
						uiResult = ARM_UPDATE_WAIT_REMATCH_UI(ui, armUseContext)//, playerData)
						// This player is quitting.
						IF uiResult = ARMUIRETVAL_FALSE
						//OR GET_TIMER_IN_SECONDS(tutorialTimer) > 45.0
							ARM_SET_PLAYER_MP_STATE(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMMPSTATE_TERMINATE_SPLASH)
							
						ENDIF
					ENDIF
				BREAK
				
				// We are being informed that the other player has left.
				CASE ARMMPSTATE_TERMINATE_SPLASH
					#IF IS_DEBUG_BUILD
						ARM_UPDATE_DEBUG(serverData, table, cameraSet, ENUM_TO_INT(ARM_GET_PLAYER_MP_STATE(playerData[sParticipantInfo.iPlayers[eSelfID]])), ENUM_TO_INT(ARM_GET_SERVER_MP_STATE(serverData)), ENUM_TO_INT(ARM_GET_SERVER_GAME_STATE(serverData)), ARM_GET_WRESTLER_ATTACHMENT_OBJECT(wrestlers[eSelfID]), ARM_GET_REFEREE_ATTACHMENT_OBJECT(crowd), crowd.peds)
					#ENDIF
					
					ARM_GET_LOCAL_INPUT(wrestlers[eSelfID].input)
					
					IF GET_CURRENT_GAMEMODE() = GAMEMODE_FM
						IF HAS_NET_TIMER_STARTED(timeArmAwardFailSafe)
							
							IF ARM_GET_ARG_TABLE_INDEX(args) != 12
								ARM_WIN_CAM_CYCLE(cameraset, bHappy)
							ELSE
								IF eSelfID = ARMWRESTLERID_AWAY
									ARM_WIN_CAM_CYCLE(cameraset, FALSE)
								ELSE
									ARM_WIN_CAM_CYCLE(cameraset, TRUE)
								ENDIF
							ENDIF
							
							DISABLE_FRONTEND_THIS_FRAME()
							
							// Restore cams.
							//RENDER_SCRIPT_CAMS(FALSE, FALSE)
							CLEANUP_BIG_MESSAGE()
							
							IF (terminateReason = ARMMPTERMINATEREASON_PLAYERLEFT)
								SET_SHARD_BIG_MESSAGE(ui.siShardMessage, "ARMMP_SC_WIN", "ARMMP_TERM_LEFT", (AWARD_FAILSAFE/2), SHARD_MESSAGE_CENTERED, HUD_COLOUR_WHITE)
								INIT_SIMPLE_USE_CONTEXT(armUseContext, FALSE, FALSE, FALSE, TRUE)
								ADD_SIMPLE_USE_CONTEXT_INPUT(armUseContext, "CMRC_CONT", FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
								SET_SIMPLE_USE_CONTEXT_FULLSCREEN(armUseContext)
								
								SET_VARS_ON_FMMC_EOM_VOTE_PASSING(ciFMMC_EOM_VOTE_STATUS_CONTINUE, <<0.0, 0.0, 0.0>>, contentID)
								
								// freeze the other guy somehow?
								//wrestlers[eSelfID].otherWrestler
								
								IF GET_SKYFREEZE_STAGE() = SKYFREEZE_NONE
									SET_SKYFREEZE_STAGE(SKYFREEZE_FROZEN)
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "setting skyfreeze - frozen")
								ELSE
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "skyfreeze = ", GET_SKYFREEZE_STAGE())
								ENDIF
								
								TOGGLE_RENDERPHASES(FALSE)
								
								leaveState = ARM_MPLEAVESTATE_BIG_MESSAGE
							ELSE
								//ARM_UPDATE_TERMINATE_UI(terminateReason)
								IF terminateReason = ARMMPTERMINATEREASON_ENDGAME
									PRINT_NOW("ARMMP_TERM_LEFT", 5000, 0)
								ENDIF
								
								//iArmStats[ARM_STAT_NUM_WINS]++
								
								leaveState = ARM_MPLEAVESTATE_LEADERBOARD
							ENDIF
							
							REINIT_NET_TIMER(timeArmAwardFailSafe)
							SET_RESULT_SCREEN_DISPLAYING_STATE(FALSE)
							
							IF MPGlobals.DartsData.bResultsDisplayed
								MPGlobals.DartsData.bResultsDisplayed = FALSE
							ENDIF
							
							SET_STATIC_EMITTER_ENABLED(GET_ARM_RADIO_EMITTER_NAME(ARM_GET_ARG_TABLE_INDEX(args)), FALSE)
							
							CDEBUG1LN(DEBUG_ARM_WRESTLING, "calling trigger pre load for AW")
							TRIGGER_CELEBRATION_PRE_LOAD(sCelebrationData, TRUE)
							
							ARM_SET_PLAYER_MP_STATE(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMMPSTATE_END_LEADERBOARD)
						ELSE
							START_NET_TIMER(timeArmAwardFailSafe)
						ENDIF
					ELSE
						IF ARM_UPDATE_TERMINATE_UI(terminateReason)
							BROADCAST_ARM_WRESTLE_TABLE_ACTIVATE(PLAYER_ID(), FALSE)
							RELEASE_SCRIPT_AUDIO_BANKS(iWinSound)
							IF ARM_GET_SERVER_WINS(serverData, eSelfID) > ARM_GET_SERVER_WINS(serverData, eOpponentID)
								ARM_SCRIPT_CLEANUP(sParticipantInfo, wrestlers, ui, NULL, NULL, crowd.refereeAttachment, ARM_GET_ARG_TABLE_INDEX(args), TRUE, FALSE)
								ARM_SET_PLAYER_MP_STATE(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMMPSTATE_VICTORY_AUDIO)
							ELSE
								ARM_SCRIPT_CLEANUP(sParticipantInfo, wrestlers, ui, NULL, NULL, crowd.refereeAttachment, ARM_GET_ARG_TABLE_INDEX(args), TRUE, TRUE)
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE ARMMPSTATE_END_LEADERBOARD
					
					SWITCH leaveState
						CASE ARM_MPLEAVESTATE_BIG_MESSAGE
							IF NOT UPDATE_SHARD_BIG_MESSAGE(ui.siShardMessage, TRUE)
							OR HAS_NET_TIMER_EXPIRED(timeArmAwardFailSafe, AWARD_FAILSAFE/2)
								INCREMENT_MP_INT_PLAYER_STAT(MPPLY_ARMWRESTLING_TOTAL_WINS)
								//iArmStats[ARM_STAT_NUM_WINS]++
								
								iArmTotalWins = GET_MP_INT_PLAYER_STAT(MPPLY_ARMWRESTLING_TOTAL_WINS)
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: total wins is now ", iArmTotalWins)
								IF iArmTotalWins > 0
								AND (iArmTotalWins % 5) = 0
									INCREMENT_PLAYER_PED_STAT(CHAR_MULTIPLAYER, PS_STRENGTH, 2)
									CPRINTLN(DEBUG_ARM_WRESTLING, "STRENGTH STAT INCREASED FROM ARM WRESTLING")
								ENDIF
								
								CLEANUP_BIG_MESSAGE()
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "In ARMMPSTATE_END_LEADERBOARD, going to ARM_MPLEAVESTATE_LEADERBOARD")
								leaveState = ARM_MPLEAVESTATE_LEADERBOARD
							ELSE
								IF NOT NETWORK_TEXT_CHAT_IS_TYPING()
									UPDATE_SIMPLE_USE_CONTEXT(armUseContext)
								ENDIF
							ENDIF
						BREAK
						
						CASE ARM_MPLEAVESTATE_LEADERBOARD
							JOB_WIN_STATUS eJobWinStatus
							INT iArmXPAmount
							
							IF ARM_GET_CLIENT_FLAG(playerData[iMyParticipantID], ARMCLIENTFLAG_QUIT_DONE)
								IF SET_SKYSWOOP_UP(TRUE, TRUE, FALSE)
									SET_SKYFREEZE_CLEAR()
									
									DEAL_WITH_FM_MATCH_END(FMMC_TYPE_MG_ARM_WRESTLING, ServerData.iMatchHistoryID, missionScriptArgs.mdID.idVariation, 
															PICK_INT(bHappy, 1, 0), PICK_INT(bHappy, 1, 2))
									//ARM_UPDATE_TERMINATE_UI(terminateReason)
									BROADCAST_ARM_WRESTLE_TABLE_ACTIVATE(PLAYER_ID(), FALSE)
									
									SET_RESULT_SCREEN_DISPLAYING_STATE(FALSE)
									
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "cleaning up here in ARM_MPLEAVESTATE_LEADERBOARD after SET_SKYSWOOP_UP")
									RELEASE_SCRIPT_AUDIO_BANKS(iWinSound)
									ARM_SCRIPT_CLEANUP(sParticipantInfo, wrestlers, ui, NULL, NULL, crowd.refereeAttachment, 
														ARM_GET_ARG_TABLE_INDEX(args), TRUE, TRUE, (ARM_GET_SERVER_WINS(serverData, eSelfID) > ARM_GET_SERVER_WINS(serverData, eOpponentID)))
								ENDIF
							ELSE
								
								IF ARM_GET_ARG_TABLE_INDEX(args) != 12
									ARM_WIN_CAM_CYCLE(cameraset, bHappy)
								ELSE
									IF eSelfID = ARMWRESTLERID_AWAY
										ARM_WIN_CAM_CYCLE(cameraset, FALSE)
									ELSE
										ARM_WIN_CAM_CYCLE(cameraset, TRUE)
									ENDIF
								ENDIF
								
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: iArmStats[ARM_STAT_NUM_WINS] = ", iArmStats[ARM_STAT_NUM_WINS])
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: iArmStats[ARM_STAT_NUM_LOSSES] = ", iArmStats[ARM_STAT_NUM_LOSSES])
								
								IF iArmStats[ARM_STAT_NUM_WINS] > iArmStats[ARM_STAT_NUM_LOSSES]
									eJobWinStatus = JOB_STATUS_WIN
									winningPlayer = PLAYER_ID()
								ELIF iArmStats[ARM_STAT_NUM_WINS] = iArmStats[ARM_STAT_NUM_LOSSES]
									eJobWinStatus = JOB_STATUS_DRAW
									winningPlayer = PLAYER_ID()
								ELSE
									eJobWinStatus = JOB_STATUS_LOSE
									winningPlayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(sParticipantInfo.iPlayers[eOpponentID]))
								ENDIF
								
								// for reference
								// GIVE_LOCAL_PLAYER_XP(eXPTYPE_STANDARD, "ARMMP_XPT",XPTYPE_COMPLETE, XPCATEGORY_COMPLETE_ARM_WRESTING, (100 * iArmStats[ARM_STAT_NUM_WINS]))
								
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "Base XP Amount = ", (ARM_XP_AMOUNT * iArmStats[ARM_STAT_NUM_WINS]))
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "modifier value = ", g_sMPTunables.fxp_tunable_Minigames_Arm_Wrestling)
								iArmXPAmount = ROUND(g_sMPTunables.fxp_tunable_Minigames_Arm_Wrestling * (ARM_XP_AMOUNT * iArmStats[ARM_STAT_NUM_WINS]))
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "iArmXPAmount   = ", iArmXPAmount)
								
								SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_PLAY_ARM_WRESTLING)
								CPRINTLN(DEBUG_ARM_WRESTLING, "DAILY OBJECTIVE COMPLETE FOR ARM WRESTLING")
								
								IF HAS_GENERIC_CELEBRATION_FINISHED(sCelebrationData, camEndScreen, 
																	"ARMMP_XPT", XPTYPE_COMPLETE, XPCATEGORY_COMPLETE_ARM_WRESTING, iArmXPAmount,
																	eJobWinStatus, 0, eGenericMGStage, winningPlayer, TRUE, 
																	0, 0, TRUE, FALSE)
																	//iArmStats[ARM_STAT_NUM_WINS], iArmStats[ARM_STAT_NUM_MATCHES], TRUE) 
									
									IF SET_SKYSWOOP_UP(TRUE)
										RELEASE_SCRIPT_AUDIO_BANKS(iWinSound)
										DEAL_WITH_FM_MATCH_END(FMMC_TYPE_MG_ARM_WRESTLING, ServerData.iMatchHistoryID, missionScriptArgs.mdID.idVariation, 
																PICK_INT(bHappy, 1, 0), PICK_INT(bHappy, 1, 2))
										//ARM_UPDATE_TERMINATE_UI(terminateReason)
										BROADCAST_ARM_WRESTLE_TABLE_ACTIVATE(PLAYER_ID(), FALSE)
										
										SET_VARS_ON_FMMC_EOM_VOTE_PASSING(ciFMMC_EOM_VOTE_STATUS_CONTINUE, <<0.0, 0.0, 0.0>>, contentID)
										
										SET_RESULT_SCREEN_DISPLAYING_STATE(FALSE)
										
										ARM_SCRIPT_CLEANUP(sParticipantInfo, wrestlers, ui, NULL, NULL, crowd.refereeAttachment, 
															ARM_GET_ARG_TABLE_INDEX(args), TRUE, TRUE, (ARM_GET_SERVER_WINS(serverData, eSelfID) > ARM_GET_SERVER_WINS(serverData, eOpponentID)))
									ENDIF
								ENDIF
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				
				// Load and play an audio stream if we had more wins.
				CASE ARMMPSTATE_VICTORY_AUDIO
					MISSION_FLOW_PLAY_END_OF_MISSION_MUSIC(TRUE)
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						CLEAR_PED_TASKS(PLAYER_PED_ID())
					ENDIF
					TERMINATE_THIS_THREAD()
				BREAK
			ENDSWITCH
		ENDIF
		
		// Run server tasks.
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			// Act based on the server state.
			SWITCH ARM_GET_SERVER_MP_STATE(serverData)
				// Do nothing.
				CASE ARMMPSTATE_INVALID
				BREAK
				
				// Waiting for another player.
				CASE ARMMPSTATE_INIT
					IF ARM_GET_NUM_WRESTLERS( sParticipantInfo ) >= ENUM_TO_INT(ARMWRESTLERIDS)
						ARM_SET_SERVER_TABLE_INDEX(serverData, ARM_GET_ARG_TABLE_INDEX(args))
						ARM_SET_SERVER_MP_STATE(serverData, ARMMPSTATE_SETUP)
					ENDIF
				BREAK
				
				// Wait for both clients to sync up, then start the game.
				CASE ARMMPSTATE_SETUP
//					IF ARM_SERVER_UPDATE_RUN_CONDITIONS(serverData, terminateReason)
						IF ARM_GET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMCLIENTFLAG_SYNC_COMPLETE)
						AND ARM_GET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[eOpponentID]], ARMCLIENTFLAG_SYNC_COMPLETE)
							// Wait until we are allowed to create our peds and objects.
							IF CAN_REGISTER_MISSION_OBJECTS(iNETWORK_OBJECTS)
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "Can register ", iNETWORK_OBJECTS, " objects this frame. Starting creation...")
								ARM_SET_SERVER_MP_STATE(serverData, ARMMPSTATE_RUNNING)
								ARM_SET_SERVER_GAME_STATE(serverData, ARMGAMESTATE_READYMENU)
								
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling: server going to intro scene")
								
								// deactivate floating table help
								BROADCAST_ARM_WRESTLE_TABLE_ACTIVATE(playerData[sParticipantInfo.iPlayers[eSelfID]].curPlayerID, FALSE)
								// Broadcast match information to all players.
								broadcastIndex = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(sParticipantInfo.iPlayers[eSelfID]))
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "Broadcasting arm wrestle status to all players on team ", GET_PLAYER_TEAM(broadcastIndex))
								BROADCAST_ARM_WRESTLE_UPDATE(broadcastIndex, NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(sParticipantInfo.iPlayers[eOpponentID])), ARM_GET_SERVER_WINS(serverData, ARMWRESTLERID_AWAY), ARM_GET_SERVER_WINS(serverData, ARMWRESTLERID_HOME), GET_PLAYER_TEAM(broadcastIndex), TRUE)
							ELSE
								#IF IS_DEBUG_BUILD
								DEBUG_MESSAGE_PERIODIC("Host: ARMMPSTATE_SETUP: can't register mission objects", iDebugThrottle)
								#ENDIF
							ENDIF
						ELSE
							#IF IS_DEBUG_BUILD
							DEBUG_MESSAGE_PERIODIC("***************AM_Armwrestling.sc: Host: ARMMPSTATE_SETUP: waiting for ARMCLIENTFLAG_SYNC_COMPLETE", iDebugThrottle)
							#ENDIF
						ENDIF
//					ELSE
//						#IF IS_DEBUG_BUILD
//						DEBUG_MESSAGE_PERIODIC("Host: ARMMPSTATE_SETUP: waiting for ARM_SERVER_UPDATE_RUN_CONDITIONS", iDebugThrottle)
//						#ENDIF
//					ENDIF
				BREAK
				
				// Main state, clients playing.
				CASE ARMMPSTATE_RUNNING
					// If we aren't home, it means the orignal host left and we should just wait to exit.
					IF eSelfID = ARMWRESTLERID_HOME
						IF ARM_SERVER_UPDATE_RUN_CONDITIONS(serverData, terminateReason, playerData, sParticipantInfo)
							ARM_SERVER_UPDATE_GAME(serverData, playerData, sParticipantInfo)
						ELSE
							IF (GET_GAME_TIMER() % 2500) < 50
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling: SERVER RUN CONDITION returning FALSE")
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				// Always look for other players quitting.
				DEFAULT
					ARM_SERVER_UPDATE_RUN_CONDITIONS(serverData, terminateReason, playerData, sParticipantInfo)
				BREAK
			ENDSWITCH
			
			#IF IS_DEBUG_BUILD
				IF bImClient
					CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling: I was once the client but now I'm the host")
					bImClient = FALSE
				ENDIF
				IF NOT bImHost
					CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling: Establishing myself as the host")
					bImHost = TRUE
				ENDIF
			#ENDIF
			
			IF (GET_GAME_TIMER() % 5000) < 50
				CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling: I am the true network host")
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
				IF bImHost
					CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling: I was once the host but now I'm the client")
					bImHost = FALSE
				ENDIF
				IF NOT bImClient
					CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling: Establishing myself as the client")
					bImClient = TRUE
				ENDIF
			#ENDIF
			
			IF (GET_GAME_TIMER() % 5000) < 50
				CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling: I am not the network host")
			ENDIF
		ENDIF
	ENDWHILE
ENDSCRIPT
