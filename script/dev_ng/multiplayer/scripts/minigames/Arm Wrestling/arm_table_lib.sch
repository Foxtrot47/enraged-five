
//////////////////////////////////////////////////////////////////////
/* arm_table_lib.sch												*/
/* Author: DJ Jones													*/
/* Table functionality for arm wrestling minigame.					*/
//////////////////////////////////////////////////////////////////////


// Coordinates in world space, generated from table space coordinates.
FUNC VECTOR ARM_TABLE_TO_WORLD(ARM_TABLE& table, VECTOR vTableCoords)
	VECTOR vWorldCoords = ARM_GET_TABLE_CENTER(table) + ARM_GET_TABLE_RIGHT(table) * vTableCoords.x
	vWorldCoords += ARM_GET_TABLE_FORWARD(table) * vTableCoords.y
	vWorldCoords.z += vTableCoords.z
	RETURN vWorldCoords
ENDFUNC

// Coordinates in table space, generated from world space coordinates.
//FUNC VECTOR ARM_WORLD_TO_TABLE(ARM_TABLE& table, VECTOR vWorldCoords)
//	VECTOR vTableCoords
//	FLOAT fDot = DOT_PRODUCT_XY(vWorldCoords, ARM_GET_TABLE_CENTER(table))
//	vTableCoords.x = fDot - ARM_GET_TABLE_CENTER_X_DOT(table) 
//	fDot = DOT_PRODUCT_XY(vWorldCoords, ARM_GET_TABLE_FORWARD(table))
//	vTableCoords.y = fDot - ARM_GET_TABLE_CENTER_Y_DOT(table)
//	vTableCoords.z = vWorldCoords.z - ARM_GET_TABLE_ALTITUDE(table)
//	RETURN vTableCoords
//ENDFUNC

// Return the home or away start position for the given table in world coords.
FUNC VECTOR ARM_GET_TABLE_WRESTLER_POS(ARM_TABLE& table, BOOL bHome, BOOL bLargeModel)
	VECTOR vWrestlerPos = PICK_VECTOR(bLargeModel, <<fWRESTLER_OFFSET_X, fWRESTLER_OFFSET_LY, fWRESTLER_OFFSET_LZ>>, <<fWRESTLER_OFFSET_X, fWRESTLER_OFFSET_SY, fWRESTLER_OFFSET_SZ>>)
	IF bHome
		vWrestlerPos = <<fWRESTLER_OFFSET_XA, fWRESTLER_OFFSET_YA, fWRESTLER_OFFSET_LZ>>
		
		CDEBUG1LN(DEBUG_ARM_WRESTLING, "get table wrestler pos: home vWrestlerPos = ", vWrestlerPos)
	ELSE
		
		//vWrestlerPos = <<fWRESTLER_OFFSET_XA, fWRESTLER_OFFSET_YA, fWRESTLER_OFFSET_LZ>>
		vWrestlerPos = <<fWRESTLER_OFFSET_X, fWRESTLER_OFFSET_LY, fWRESTLER_OFFSET_LZ>>
		vWrestlerPos.x *= -1.0
		vWrestlerPos.y *= -1.0
		
		CDEBUG1LN(DEBUG_ARM_WRESTLING, "get table wrestler pos: away vWrestlerPos = ", vWrestlerPos)
	ENDIF
	RETURN ARM_TABLE_TO_WORLD(table, vWrestlerPos)
ENDFUNC

// Return a crowd start position for the given table in world coords.
FUNC VECTOR ARM_GET_TABLE_CROWD_POS(ARM_TABLE& table, ARM_CROWD_MEMBER crowdMember)
	SWITCH crowdMember
		CASE ARMCROWDMEMBER_REFEREE   RETURN ARM_TABLE_TO_WORLD(table, <<fREFEREE_OFFSET_X, fREFEREE_OFFSET_Y, fREFEREE_OFFSET_Z>>)
		CASE ARMCROWDMEMBER_AWAY_FAN1 RETURN ARM_TABLE_TO_WORLD(table, <<fAWAYFAN1_OFFSET_X, fAWAYFAN1_OFFSET_Y, fFAN_OFFSET_Z>>)
		CASE ARMCROWDMEMBER_AWAY_FAN2 RETURN ARM_TABLE_TO_WORLD(table, <<fAWAYFAN2_OFFSET_X, fAWAYFAN2_OFFSET_Y, fFAN_OFFSET_Z>>)
		CASE ARMCROWDMEMBER_AWAY_FAN3 RETURN ARM_TABLE_TO_WORLD(table, <<fAWAYFAN3_OFFSET_X, fAWAYFAN3_OFFSET_Y, fFAN_OFFSET_Z>>)
		CASE ARMCROWDMEMBER_HOME_FAN1 RETURN ARM_TABLE_TO_WORLD(table, <<fHOMEFAN1_OFFSET_X, fHOMEFAN1_OFFSET_Y, fFAN_OFFSET_Z>>)
		CASE ARMCROWDMEMBER_HOME_FAN2 RETURN ARM_TABLE_TO_WORLD(table, <<fHOMEFAN2_OFFSET_X, fHOMEFAN2_OFFSET_Y, fFAN_OFFSET_Z>>)
		CASE ARMCROWDMEMBER_HOME_FAN3 RETURN ARM_TABLE_TO_WORLD(table, <<fHOMEFAN3_OFFSET_X, fHOMEFAN3_OFFSET_Y, fFAN_OFFSET_Z>>)
		DEFAULT
			DEBUG_MESSAGE("Invalid crowd member passed into ARM_GET_TABLE_CROWD_POS!")
		RETURN <<0,0,0>>
	ENDSWITCH
ENDFUNC

// Initialize a table's data.
PROC ARM_INIT_TABLE(ARM_TABLE& table, INT iTableIndex)
	VECTOR vForward
	
	// Pull our hard-coded position data for our table.
	ARM_GET_TABLE_DATA(table.vCenter, table.fRotation, iTableIndex, TRUE)
	
	// Calculate the normals for the table.
	vForward.x = -COS(ARM_GET_TABLE_ROTATION(table) + 90.0)
	vForward.y = -SIN(ARM_GET_TABLE_ROTATION(table) + 90.0)
	ARM_SET_TABLE_FORWARD(table, vForward)
	ARM_SET_TABLE_RIGHT(table, ROTATE_VECTOR_ABOUT_Z_ORTHO(vForward, ROTSTEP_NEG_90))
	
	// Precalculate some dot products.
//	ARM_SET_TABLE_CENTER_X_DOT(table, DOT_PRODUCT_XY(ARM_GET_TABLE_CENTER(table), ARM_GET_TABLE_RIGHT(table)))
//	ARM_SET_TABLE_CENTER_Y_DOT(table, DOT_PRODUCT_XY(ARM_GET_TABLE_CENTER(table), ARM_GET_TABLE_FORWARD(table)))
ENDPROC
