
//////////////////////////////////////////////////////////////////////
/* arm_tweak.sch													*/
/* Author: DJ Jones													*/
/* Tweak values for arm wrestling minigame.							*/
//////////////////////////////////////////////////////////////////////


// Game values
TWEAK_FLOAT fSCORE_CHANGE_SCALAR  0.031 // Multiplier to the amount of force applied
TWEAK_FLOAT fSCORE_UPDATE_TIME    0.425 //0.4   // Seconds between server score checks
TWEAK_FLOAT fBUCKET_SWITCH_TIME   0.35	//0.4   // Seconds before switching to a new bucket aka playerdata score sets
TWEAK_FLOAT fSCORE_INCREMENTS 	 10.25	// normal increment level
TWEAK_FLOAT fSCORE_INC_FAST 	 18.25 	// increment level for fast frame rates

// Wrestler values
TWEAK_FLOAT fWRESTLER_OFFSET_X    0.220//0.122 //0.115 // How far from center each wrestler is positioned
TWEAK_FLOAT fWRESTLER_OFFSET_SY   0.500 // How far from center short wrestler is positioned
TWEAK_FLOAT fWRESTLER_OFFSET_LY   0.676 //0.590 // How far from center tall wrestler is positioned
TWEAK_FLOAT fWRESTLER_OFFSET_SZ   1.066 // How far from center short wrestler is positioned
TWEAK_FLOAT fWRESTLER_OFFSET_LZ   0.533 // How far from center tall wrestler is positioned
TWEAK_FLOAT fWRESTLER_OFFSET_XA   0.06
TWEAK_FLOAT fWRESTLER_OFFSET_YA   0.516

TWEAK_FLOAT fARM_RAMP_DAMP        0.120 // How much to dampen our arm movement
TWEAK_FLOAT fOVERRIDE_X			  0.245 // Override offset from local wrestler - x
TWEAK_FLOAT fOVERRIDE_y			  1.195 // Override offset from local wrestler - y
TWEAK_FLOAT fOVERRIDE_Z			  0.533 // Override offset from local wrestler - z - ONLY USED FOR SPECTATORS
TWEAK_FLOAT fOVERRIDE_heading	180.000 // Override offset from local wrestler - heading
TWEAK_FLOAT fShakeFactor		  0.070 // Shake factor for the wrestle sweep
TWEAK_FLOAT fRealShakeFactor	  0.098 // Real Shake factor for the wrestle sweep

// Crowd values
TWEAK_FLOAT fREFEREE_OFFSET_X    -1.000 // How far from center the referee is positioned
TWEAK_FLOAT fREFEREE_OFFSET_Y    -0.550 // How far from center the referee is positioned
TWEAK_FLOAT fREFEREE_OFFSET_Z     0.930 // How far from center the referee is positioned
TWEAK_FLOAT fREFEREE_HEADING    122.960 // Referee's heading
TWEAK_FLOAT fAWAYFAN1_OFFSET_X   -0.688 // How far from center away fan 1 is positioned
TWEAK_FLOAT fAWAYFAN1_OFFSET_Y   -1.312 // How far from center away fan 1 is positioned
TWEAK_FLOAT fAWAYFAN2_OFFSET_X   -0.608 // How far from center away fan 2 is positioned
TWEAK_FLOAT fAWAYFAN2_OFFSET_Y    1.088 // How far from center away fan 2 is positioned
TWEAK_FLOAT fAWAYFAN3_OFFSET_X   -1.392 // How far from center away fan 3 is positioned
TWEAK_FLOAT fAWAYFAN3_OFFSET_Y    0.432 // How far from center away fan 3 is positioned
TWEAK_FLOAT fHOMEFAN1_OFFSET_X    0.832 // How far from center home fan 1 is positioned
TWEAK_FLOAT fHOMEFAN1_OFFSET_Y   -1.088 // How far from center home fan 1 is positioned
TWEAK_FLOAT fHOMEFAN2_OFFSET_X    0.736 // How far from center home fan 2 is positioned
TWEAK_FLOAT fHOMEFAN2_OFFSET_Y    1.264 // How far from center home fan 2 is positioned
TWEAK_FLOAT fHOMEFAN3_OFFSET_X    1.344 // How far from center home fan 3 is positioned
TWEAK_FLOAT fHOMEFAN3_OFFSET_Y    0.000 // How far from center home fan 3 is positioned
TWEAK_FLOAT fFAN_OFFSET_Z         0.000 // Altitude of spawned fans (inconsequential)
TWEAK_FLOAT fREF_INTRO_SPEED      0.400 // Speed of referee's intro anim
TWEAK_FLOAT fREF_OUTRO_SPEED      0.400 // Speed of referee's outro anim
TWEAK_FLOAT fCROWD_TAUNT_DELAY    8.0   // Delay between taunts from the crowd

// Table values
TWEAK_FLOAT fTABLE_OFFSET_Z       0.482 // Vertical offset for the table

// Wrestle camera values
TWEAK_FLOAT fCAM_WRESTLE_X	     -0.800 // Start x pos of wrestle cam (table space)
TWEAK_FLOAT fCAM_WRESTLE_Y        0.520 // Start y pos of wrestle cam (table space)
TWEAK_FLOAT fCAM_WRESTLE_Z	      1.580 // Start z pos of wrestle cam (table space)
TWEAK_FLOAT fCAM_WRESTLE_LX	     -0.110 // Start x look at of wrestle cam (table space)
TWEAK_FLOAT fCAM_WRESTLE_LY      -0.14  // Start y look at of wrestle cam (table space)
TWEAK_FLOAT fCAM_WRESTLE_LZ	      1.300 // Start z look at of wrestle cam (table space)
TWEAK_FLOAT fCAM_WRESTLE_FOV     45.0   // Start fov of wrestle cam
TWEAK_FLOAT fCAM_IDLE_FOV     60.0   // Start fov of wrestle cam

// General camera values
TWEAK_FLOAT fCAM_SWITCH_THRESHOLD 0.6   // Proximity to win/loss before cam switch
TWEAK_FLOAT fCAM_RETURN_THRESHOLD 0.45  // Proximity to win/loss before cam return

// Away winning camera values
TWEAK_FLOAT fCAM_AWAYWIN_X	      0.680 // Start x pos of away cam (table space)
TWEAK_FLOAT fCAM_AWAYWIN_Y       -0.340 // Start y pos of away cam (table space)
TWEAK_FLOAT fCAM_AWAYWIN_Z	      1.260 // Start z pos of away cam (table space)
TWEAK_FLOAT fCAM_AWAYWIN_LX	      0.0   // Start x look at of away cam (table space)
TWEAK_FLOAT fCAM_AWAYWIN_LY       0.100 // Start y look at of away cam (table space)
TWEAK_FLOAT fCAM_AWAYWIN_LZ	      1.240 // Start z look at of away cam (table space)
TWEAK_FLOAT fCAM_AWAYWIN_FOV     50.0   // Start fov of away cam

// Home winning camera values
TWEAK_FLOAT fCAM_HOMEWIN_X	     -0.620 // Start x pos of home cam (table space)
TWEAK_FLOAT fCAM_HOMEWIN_Y        0.300 // Start y pos of home cam (table space)
TWEAK_FLOAT fCAM_HOMEWIN_Z	      1.2   // Start z pos of home cam (table space)
TWEAK_FLOAT fCAM_HOMEWIN_LX	      0.0   // Start x look at of home cam (table space)
TWEAK_FLOAT fCAM_HOMEWIN_LY      -0.12  // Start y look at of home cam (table space)
TWEAK_FLOAT fCAM_HOMEWIN_LZ	      1.26  // Start z look at of home cam (table space)
TWEAK_FLOAT fCAM_HOMEWIN_FOV     50.0   // Start fov of home cam

// UI values
TWEAK_FLOAT fREADY_DELAY          1.5   // Delay before ready option available
TWEAK_FLOAT fREADY_TIMEOUT        6.0   // How long before forced into ready state
TWEAK_FLOAT fREMATCH_DELAY       0.725   // Delay before options available in scorecard
TWEAK_FLOAT fTERMINATE_TIMEOUT    7.0   // How long before kicked out of finished game
TWEAK_FLOAT fMETER_DAMP           0.2   // Dampen meter movement
TWEAK_INT   iFADE_OUT_TIME      500     // Duration of fade out in ms
TWEAK_INT   iFADE_IN_TIME       500     // Duration of fade in in ms

// Ready status screen values
TWEAK_FLOAT fREADY_TL_X           0.680 // X position of ready UI
TWEAK_FLOAT fREADY_TL_Y           0.600 // Y position of ready UI
TWEAK_FLOAT fREADY_WIDTH          0.290 // Width of ready UI
TWEAK_FLOAT fREADY_HEIGHT         0.130 // Height of ready UI
TWEAK_FLOAT fREADY_LBL_PLAYERS_X  0.770 // X position of player column label
TWEAK_FLOAT fREADY_LBL_STATUS_X   0.900 // X position of status column label
TWEAK_FLOAT fREADY_LABEL_Y        0.610 // Y position of column labels
TWEAK_FLOAT fREADY_LABEL_SCALE    0.620 // Scale of column label text
TWEAK_FLOAT fREADY_NAME_X         0.700 // X position of player names
TWEAK_FLOAT fREADY_STATUS_X       0.900 // X position of player statuses
TWEAK_FLOAT fREADY_ENTRY_LCL_Y	  0.655 // Y position of local player entries
TWEAK_FLOAT fREADY_ENTRY_RMT_Y    0.685 // Y position of remote player entries
TWEAK_FLOAT fREADY_ENTRY_SCALE    0.420 // Scale of table entries
TWEAK_INT	iREADY_RED            0	    // Red component of ready bg color
TWEAK_INT	iREADY_GREEN          0	    // Green component of ready bg color
TWEAK_INT	iREADY_BLUE           0	    // Blue component of ready bg color
TWEAK_INT	iREADY_ALPHA        200	    // Alpha component of ready bg color

// Countdown screen values
TWEAK_FLOAT fCOUNTDOWN_X          0.5   // X position of countdown timer
TWEAK_FLOAT fCOUNTDOWN_Y          0.5   // Y position of countdown timer
TWEAK_FLOAT fCOUNTDOWN_SCALE      2.0   // Scale of countdown timer

// Terminate screen values
TWEAK_FLOAT	fTERMINATE_TL_X		  0.331 // X position of terminate UI
TWEAK_FLOAT	fTERMINATE_TL_Y		  0.402 // Y position of terminate UI
TWEAK_FLOAT	fTERMINATE_WIDTH      0.337 // Width of terminate UI
TWEAK_FLOAT	fTERMINATE_HEIGHT	  0.136 // Height of terminate UI
TWEAK_FLOAT	fTERMINATE_WRAP_L     0.33  // Left bound of terminate text
TWEAK_FLOAT	fTERMINATE_WRAP_R     0.67  // Right bound of terminate text
TWEAK_FLOAT	fTERMINATE_TEXT_Y	  0.419 // Y position of terminate text
TWEAK_FLOAT fTERMINATE_TEXT_SCALE 0.740 // Scale of terminate text
TWEAK_INT	iTERMINATE_RED       21	    // Red component of terminate bg color
TWEAK_INT	iTERMINATE_GREEN      0	    // Green component of terminate bg color
TWEAK_INT	iTERMINATE_BLUE       0	    // Blue component of terminate bg color
TWEAK_INT	iTERMINATE_ALPHA    200	    // Alpha component of terminate bg color

TWEAK_FLOAT	fLerpScore			  1.0	// Linear interpolation rate of score parameter
TWEAK_FLOAT	fLerpPhase			  1.0 	// Linear interpolation rate of phase parameter

// Add every new tweak value here!
#IF IS_DEBUG_BUILD
PROC ARM_IGNORE_UNREFERENCED_TWEAKS()
	// Game values
	fSCORE_CHANGE_SCALAR = fSCORE_CHANGE_SCALAR
	fSCORE_UPDATE_TIME   = fSCORE_UPDATE_TIME
	fBUCKET_SWITCH_TIME  = fBUCKET_SWITCH_TIME
	fSCORE_INCREMENTS	 = fSCORE_INCREMENTS
	
	// Wrestler values
	fWRESTLER_OFFSET_X  = fWRESTLER_OFFSET_X
	fWRESTLER_OFFSET_SY = fWRESTLER_OFFSET_SY
	fWRESTLER_OFFSET_LY = fWRESTLER_OFFSET_LY
	fWRESTLER_OFFSET_SZ = fWRESTLER_OFFSET_SZ
	fWRESTLER_OFFSET_LZ = fWRESTLER_OFFSET_LZ
	fARM_RAMP_DAMP      = fARM_RAMP_DAMP
	
	fWRESTLER_OFFSET_XA = fWRESTLER_OFFSET_XA
	fWRESTLER_OFFSET_YA = fWRESTLER_OFFSET_YA
	
	fOVERRIDE_X			= fOVERRIDE_X		
	fOVERRIDE_y			= fOVERRIDE_y		
	fOVERRIDE_Z			= fOVERRIDE_Z
	fOVERRIDE_heading	= fOVERRIDE_heading
	
	fShakeFactor		= fShakeFactor	
	fRealShakeFactor	= fRealShakeFactor
	
	// Crowd values
	fREFEREE_OFFSET_X  = fREFEREE_OFFSET_X
	fREFEREE_OFFSET_Y  = fREFEREE_OFFSET_Y
	fREFEREE_OFFSET_Z  = fREFEREE_OFFSET_Z
	fREFEREE_HEADING   = fREFEREE_HEADING
	fAWAYFAN1_OFFSET_X = fAWAYFAN1_OFFSET_X
	fAWAYFAN1_OFFSET_Y = fAWAYFAN1_OFFSET_Y
	fAWAYFAN2_OFFSET_X = fAWAYFAN2_OFFSET_X
	fAWAYFAN2_OFFSET_Y = fAWAYFAN2_OFFSET_Y
	fAWAYFAN3_OFFSET_X = fAWAYFAN3_OFFSET_X
	fAWAYFAN3_OFFSET_Y = fAWAYFAN3_OFFSET_Y
	fHOMEFAN1_OFFSET_X = fHOMEFAN1_OFFSET_X
	fHOMEFAN1_OFFSET_Y = fHOMEFAN1_OFFSET_Y
	fHOMEFAN2_OFFSET_X = fHOMEFAN2_OFFSET_X
	fHOMEFAN2_OFFSET_Y = fHOMEFAN2_OFFSET_Y
	fHOMEFAN3_OFFSET_X = fHOMEFAN3_OFFSET_X
	fHOMEFAN3_OFFSET_Y = fHOMEFAN3_OFFSET_Y
	fFAN_OFFSET_Z      = fFAN_OFFSET_Z
	fREF_INTRO_SPEED   = fREF_INTRO_SPEED
	fREF_OUTRO_SPEED   = fREF_OUTRO_SPEED
	fCROWD_TAUNT_DELAY = fCROWD_TAUNT_DELAY
	
	// Table values
	fTABLE_OFFSET_Z = fTABLE_OFFSET_Z
	
	// General cam values
	fCAM_SWITCH_THRESHOLD = fCAM_SWITCH_THRESHOLD
	fCAM_RETURN_THRESHOLD = fCAM_RETURN_THRESHOLD
	
	// Wrestle cam values
	fCAM_WRESTLE_X   = fCAM_WRESTLE_X
	fCAM_WRESTLE_Y   = fCAM_WRESTLE_Y
	fCAM_WRESTLE_Z   = fCAM_WRESTLE_Z
	fCAM_WRESTLE_LX  = fCAM_WRESTLE_LX
	fCAM_WRESTLE_LY  = fCAM_WRESTLE_LY
	fCAM_WRESTLE_LZ  = fCAM_WRESTLE_LZ
	fCAM_WRESTLE_FOV = fCAM_WRESTLE_FOV
	
	// Away winning cam values
	fCAM_AWAYWIN_X   = fCAM_AWAYWIN_X
	fCAM_AWAYWIN_Y   = fCAM_AWAYWIN_Y
	fCAM_AWAYWIN_Z   = fCAM_AWAYWIN_Z
	fCAM_AWAYWIN_LX  = fCAM_AWAYWIN_LX
	fCAM_AWAYWIN_LY  = fCAM_AWAYWIN_LY
	fCAM_AWAYWIN_LZ  = fCAM_AWAYWIN_LZ
	fCAM_AWAYWIN_FOV = fCAM_AWAYWIN_FOV
	
	// Home winning cam values
	fCAM_HOMEWIN_X   = fCAM_HOMEWIN_X
	fCAM_HOMEWIN_Y   = fCAM_HOMEWIN_Y
	fCAM_HOMEWIN_Z   = fCAM_HOMEWIN_Z
	fCAM_HOMEWIN_LX  = fCAM_HOMEWIN_LX
	fCAM_HOMEWIN_LY  = fCAM_HOMEWIN_LY
	fCAM_HOMEWIN_LZ  = fCAM_HOMEWIN_LZ
	fCAM_HOMEWIN_FOV = fCAM_HOMEWIN_FOV
	
	// UI values
	fREADY_DELAY       = fREADY_DELAY
	fREADY_TIMEOUT     = fREADY_TIMEOUT
	fREMATCH_DELAY     = fREMATCH_DELAY
	fTERMINATE_TIMEOUT = fTERMINATE_TIMEOUT
	fMETER_DAMP        = fMETER_DAMP
	iFADE_OUT_TIME     = iFADE_OUT_TIME
	iFADE_IN_TIME      = iFADE_IN_TIME
	
	// Ready screen values
	fREADY_TL_X          = fREADY_TL_X
	fREADY_TL_Y          = fREADY_TL_Y
	fREADY_WIDTH         = fREADY_WIDTH
	fREADY_HEIGHT        = fREADY_HEIGHT
	fREADY_LBL_PLAYERS_X = fREADY_LBL_PLAYERS_X
	fREADY_LBL_STATUS_X  = fREADY_LBL_STATUS_X
	fREADY_LABEL_Y       = fREADY_LABEL_Y
	fREADY_LABEL_SCALE   = fREADY_LABEL_SCALE
	fREADY_NAME_X        = fREADY_NAME_X
	fREADY_STATUS_X      = fREADY_STATUS_X
	fREADY_ENTRY_LCL_Y	 = fREADY_ENTRY_LCL_Y
	fREADY_ENTRY_RMT_Y   = fREADY_ENTRY_RMT_Y
	fREADY_ENTRY_SCALE   = fREADY_ENTRY_SCALE
	iREADY_RED           = iREADY_RED
	iREADY_GREEN         = iREADY_GREEN
	iREADY_BLUE          = iREADY_BLUE
	iREADY_ALPHA         = iREADY_ALPHA
	
	// Countdown screen values
	fCOUNTDOWN_X     = fCOUNTDOWN_X
	fCOUNTDOWN_Y     = fCOUNTDOWN_Y
	fCOUNTDOWN_SCALE = fCOUNTDOWN_SCALE
	
	// Terminate screen values
	fTERMINATE_TL_X       = fTERMINATE_TL_X
	fTERMINATE_TL_Y		  = fTERMINATE_TL_Y
	fTERMINATE_WIDTH      = fTERMINATE_WIDTH
	fTERMINATE_HEIGHT	  = fTERMINATE_HEIGHT
	fTERMINATE_WRAP_L     = fTERMINATE_WRAP_L
	fTERMINATE_WRAP_R     = fTERMINATE_WRAP_R
	fTERMINATE_TEXT_Y	  = fTERMINATE_TEXT_Y
	fTERMINATE_TEXT_SCALE = fTERMINATE_TEXT_SCALE
	iTERMINATE_RED        = iTERMINATE_RED
	iTERMINATE_GREEN      = iTERMINATE_GREEN
	iTERMINATE_BLUE       = iTERMINATE_BLUE
	iTERMINATE_ALPHA      = iTERMINATE_ALPHA
	
	fLerpScore 		= fLerpScore
	fLerpPhase 		= fLerpPhase
ENDPROC
#ENDIF
