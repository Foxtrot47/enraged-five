
//////////////////////////////////////////////////////////////////////
/* AM_Armwrestling.sc												*/
/* Author: DJ Jones													*/
/* Multiplayer script for arm wrestling minigame.					*/
//////////////////////////////////////////////////////////////////////


CONST_INT iUSE_Z_VOLUMES 0
#IF iUSE_Z_VOLUMES
	USING "z_volumes.sch"
#ENDIF

USING "net_mission.sch"
USING "net_mission_trigger_overview.sch"
USING "net_hud_activating.sch"
USING "net_big_message.sch"
USING "minigames_helpers.sch"
USING "minigame_uiinputs.sch"
USING "minigame_big_message.sch"
USING "minigame_midsized_message.sch"
USING "shared_hud_displays.sch"
USING "minigame_stats_tracker_helpers.sch"
USING "socialclub_leaderboard.sch"
USING "script_usecontext.sch"
USING "end_screen.sch"
USING "arm.sch"
USING "achievement_public.sch"

USING "net_celebration_screen.sch"
USING "net_wait_zero.sch"

// Our only static variables. Set once after network init.
ARM_WRESTLER_ID eSelfID
ARM_WRESTLER_ID eOpponentID

BOOL bArmIdleCamSet
structTimer stCleanupTimer
INT iArmTotalWins

#IF IS_DEBUG_BUILD
//INT iDebugThrottle
BOOL bImHost, bImClient
#ENDIF

CELEBRATION_SCREEN_DATA sCelebrationData
CAMERA_INDEX camEndScreen

USING "arm_tweak.sch"
USING "arm_args.sch"
USING "arm_data.sch"
USING "arm_table.sch"
USING "arm_input.sch"
USING "arm_wrestler_Apartment.sch"
USING "arm_crowd.sch"
USING "arm_ui.sch"
USING "arm_camera.sch"
USING "arm_mp.sch"
USING "arm_client.sch"
USING "arm_server.sch"

USING "arm_table_lib.sch"
USING "arm_input_lib.sch"
USING "arm_wrestler_lib.sch"
//USING "arm_crowd_lib.sch"
USING "arm_ui_lib.sch"
USING "arm_camera_lib.sch"
USING "arm_client_lib.sch"
USING "arm_server_lib.sch"
USING "arm_leaderboard_lib.sch"

#IF IS_DEBUG_BUILD
	USING "arm_debug.sch"
#ENDIF

USING "arm_core.sch"
USING "arm_mp_core.sch"
USING "am_armwrestling_apartment.sch"
USING "Apartment_Minigame_General.sch"
CONST_FLOAT STRENGTH_SCALE 0.003
CONST_INT	ARM_XP_AMOUNT 20

ARM_PARTICIPANT_INFO sParticipantInfo

//PROC RELEASE_SCRIPT_AUDIO_BANKS(INT iWinSound)
//	IF iWinSound <> 0
//		RELEASE_NAMED_SCRIPT_AUDIO_BANK(ARM_GET_CROWD_SOUND_BANK_FROM_INDEX(iWinSound, TRUE))
//		RELEASE_NAMED_SCRIPT_AUDIO_BANK(ARM_GET_CROWD_SOUND_BANK_FROM_INDEX(iWinSound, FALSE))
//	ENDIF
//ENDPROC


PROC ARM_SETUP_WRESTLERS_APARTMENTS(ARM_WRESTLER& wrestlers[], ARM_TABLE& table, VECTOR vFreezePos, BOOL bIsTableSideA)
	OBJECT_INDEX attachmentObject
	NETWORK_INDEX niAttachment
	
	//SET_ENTITY_COORDS(PLAYER_PED_ID(), vFreezePos, TRUE, TRUE)
	FLOAT fHeading

	// Create an object to attach the wrestler to.
	fHeading = ARM_GET_TABLE_ROTATION(table)
	IF bIsTableSideA
		fHeading = WRAP(fHeading + 180.0, 0.0, 360.0)
	ENDIF
	attachmentObject = CREATE_OBJECT_NO_OFFSET(PROAIR_HOC_PUCK, vFreezePos, FALSE, FALSE)
	SET_ENTITY_HEADING(attachmentObject, fHeading)
	SET_ENTITY_VISIBLE(attachmentObject, FALSE)
	//SET_ENTITY_COLLISION(attachmentObject, FALSE)
	FREEZE_ENTITY_POSITION(attachmentObject, TRUE)
	 
	niAttachment = OBJ_TO_NET(attachmentObject)
//	IF NET_TO_OBJ(niAttachment) = attachmentObject
//		CDEBUG1LN(DEBUG_ARM_WRESTLING, "arm_wrestling: NETWORK ID SUCCESFULLY GRABBED FOR PUCK, setting to not migrate")
//		SET_NETWORK_ID_CAN_MIGRATE(niAttachment, FALSE)
//	ENDIF
	
	ARM_SET_WRESTLER_ATTACHMENT_OBJECT(wrestlers[eSelfID], attachmentObject, niAttachment)
	
	CDEBUG1LN(DEBUG_ARM_WRESTLING, "arm wrestle attachment set at ", vFreezePos)
	
	// Put the wrestler in place at the table.
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		ATTACH_ENTITY_TO_ENTITY(PLAYER_PED_ID(), attachmentObject, -1, <<0,0,0>>, <<0,0,0>>)
		//ATTACH_ENTITY_TO_ENTITY_PHYSICALLY(PLAYER_PED_ID(), attachmentObject, GET_PED_BONE_INDEX(PLAYER_PED_ID(), BONETAG_ROOT), 0, <<0,0,0>>, <<0,0,0>>, <<0,0,0>>, -1.0, TRUE)
		//ATTACH_ENTITY_TO_WORLD_PHYSICALLY(PLAYER_PED_ID(), GET_PED_BONE_INDEX(PLAYER_PED_ID(), BONETAG_ROOT), GET_ENTITY_COORDS(attachmentObject), <<0,0,0>>, -1.0, FALSE)
		SET_ENTITY_HEADING(PLAYER_PED_ID(), fHeading)
		CDEBUG1LN(DEBUG_ARM_WRESTLING, "arm_wrestling: fheading set to ", fHeading)
		
	//	FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
		
		SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
		//CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
		//SET_PLAYER_INVINCIBLE(PLAYER_ID(), TRUE)
		
		//IF NOT IS_PLAYER_DEAD(PLAYER_ID())
		//	SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
		//ENDIF
		
		SET_ENTITY_COLLISION(PLAYER_PED_ID(), TRUE)
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_UseKinematicModeWhenStationary, TRUE)
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DontActivateRagdollFromExplosions, TRUE)
	ENDIF
ENDPROC


PROC ARM_SCRIPT_CLEANUP_APARTMENT(CAMERA_INDEX &animatedCam, ARM_CAMERA_SET &cameraSet, ARM_WRESTLER &wrestlers[ARMWRESTLERIDS], INT iCleanupFlags, ARM_PLAYER_BROADCAST_DATA &playerData[32],
ARM_PARTICIPANT_INFO & info,  
ARM_UI& armUI, INT iTable = -1, BOOL bRestoreUI = TRUE)
		
	iCleanupFlags = iCleanupFlags

	IF NOT IS_TIMER_STARTED(stCleanupTimer)
		START_TIMER_NOW(stCleanupTimer)
	ENDIF
		
	IF NOT ARM_GET_CLIENT_FLAG(playerData[PARTICIPANT_ID_TO_INT()], ARMCLIENTFLAG_SPECTATOR)
		IF NOT ARM_GET_CLIENT_FLAG(playerData[PARTICIPANT_ID_TO_INT()], ARMCLIENTFLAG_CLEANUP_STARTED)
			ARM_SET_CLIENT_FLAG(playerData[PARTICIPANT_ID_TO_INT()], ARMCLIENTFLAG_CLEANUP_STARTED, TRUE)
		ENDIF
			
			IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
				//detach from table
				DETACH_POS_ADJUST(wrestlers)
				IF ARM_GET_CLIENT_FLAG(playerData[PARTICIPANT_ID_TO_INT()], ARMCLIENTFLAG_ATTACHED)
					DETACH_ENTITY(PLAYER_PED_ID())
				ENDIF
				//allow bumping 
				SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_UseKinematicModeWhenStationary, FALSE)
				SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DontActivateRagdollFromExplosions, FALSE)
				
				//Cleanup face anims.
				IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
					FACE_ANIM_STOP(PLAYER_PED_ID())
				ENDIF
			ENDIF	
			
			MGID_CLUBHOUSE_RESET()
			SHOW_SPEC_PLAYERS()
			
			PLAYER_INDEX broadcastIndex
			ANIMPOSTFX_STOP_ALL()
			CDEBUG1LN(DEBUG_ARM_WRESTLING, "Preparing to clean up arm wrestling apartment...")
			
			ENABLE_ALL_MP_HUD()
			
			// Kill widgets.
			#IF IS_DEBUG_BUILD
				ARM_CLEANUP_WIDGETS()
			#ENDIF
			
			SET_TIME_OF_DAY(TIME_OFF, TRUE)
			
			ARM_RELEASE_COUNTDOWN_UI(armUI)
			IF bRestoreUI
				DISABLE_CELLPHONE(FALSE)
				ENABLE_ALL_MP_HUD()
				DISPLAY_RADAR(TRUE)
				ENABLE_SELECTOR()
			
				// Broadcast an update to all players.
				broadcastIndex = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(info.iPlayers[eSelfID]))
				CDEBUG1LN(DEBUG_ARM_WRESTLING, "Broadcasting arm wrestle clear to all players on team ", GET_PLAYER_TEAM(broadcastIndex))
				BROADCAST_ARM_WRESTLE_CLEAR(broadcastIndex, GET_PLAYER_TEAM(broadcastIndex), TRUE)
			ENDIF
			
			// Clear help unless we display the message for leaving the area early.
			IF NOT ARM_GET_UI_FLAG(armUI, ARMUIFLAG_LEAVE_AREA)
				CLEAR_HELP()
				
				// Reset wrestler ped.
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					CDEBUG1LN(DEBUG_ARM_WRESTLING, "Resetting the player ped...")
					//DETACH_ENTITY(PLAYER_PED_ID())
					//CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
					//SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), FALSE)
					FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
				ENDIF
				
				// Restore the wrestler to his normal state.
				IF NOT IS_PLAYER_DEAD(PLAYER_ID())
					//SET_PLAYER_INVINCIBLE(PLAYER_ID(), FALSE)
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				ENDIF
			ENDIF
			
			//CAMERA CLEANUP
			INT iCamID
			CAMERA_INDEX cLoopCam
			REPEAT ARMCAMERAIDS iCamID
				cLoopCam = cameraSet.cameras[iCamID]
				IF DOES_CAM_EXIST(cLoopCam)
					IF IS_CAM_ACTIVE(cLoopCam)
						SET_CAM_ACTIVE(cLoopCam, FALSE)
					ENDIF
					DESTROY_CAM(cLoopCam)
					CDEBUG1LN(DEBUG_ARM_WRESTLING, "Destroyed Cam ID:", iCamID)
				ENDIF
			ENDREPEAT
			
			IF DOES_CAM_EXIST(animatedCam)
				IF IS_CAM_ACTIVE(animatedCam)
					SET_CAM_ACTIVE(animatedCam, FALSE)
				ENDIF
				DESTROY_CAM(animatedCam)
				CDEBUG1LN(DEBUG_ARM_WRESTLING, "Destroyed animatedCam ID:")
			ELSE
					CDEBUG1LN(DEBUG_ARM_WRESTLING, "animatedCam Was not active , not destroyed.")
			ENDIF
				
			// Restore cams.
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
			
			ARM_SET_CLIENT_FLAG(playerData[PARTICIPANT_ID_TO_INT()], ARMCLIENTFLAG_HIDE_SPECTATORS, FALSE)
			
			IF IS_MOVE_NETWORK_ACTIVE_SAFE(PLAYER_PED_ID())
				CLEAR_PED_TASKS(PLAYER_PED_ID())
				CDEBUG1LN(DEBUG_ARM_WRESTLING, "CLEAR PED TASKS!:")
			ENDIF
			
			// Clean audio.
			STOP_STREAM()
			UNREGISTER_SCRIPT_WITH_AUDIO()
			
			//# Conor Changes
			// Used for crowd
			IF iTable != -1
				IF NETWORK_IS_GAME_IN_PROGRESS()
					//NET_PRINT_STRING_INT("Clearing GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iArmWrestlingBitSet: ",iTable)
					CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iArmWrestlingBitSet,iTable)
				ENDIF
			ENDIF
			
			// reset prefered player position broadcast data
			MPGlobals.WrestleData.fBroadcastedDesire = 0.0
			
			// cleanup big splash text
			CLEAR_ALL_BIG_MESSAGES()
			
			IF NETWORK_IS_SIGNED_ONLINE()
				WRITE_ARM_WRESTLING_MP_SCLB_DATA()	
			ENDIF
			
			//##### TELEMETRY FOR GAME WIN, END
			INT iPointsEarned = iArmStats[ARM_STAT_NUM_WINS] * CONST_MC_POINTS_FOR_WIN 
			//Only Add points if won a game.
			IF iPointsEarned > 0
				CDEBUG1LN(DEBUG_ARM_WRESTLING, "ARM_WRESTLING TELEM: Minigame was WON, times: ",iArmStats[ARM_STAT_NUM_WINS], " MC points Awarded: ", iPointsEarned)
			PLAYSTATS_EARNED_MC_POINTS(
					GB_GET_GANG_BOSS_UUID_INT_A(GB_GET_LOCAL_PLAYER_GANG_BOSS()), 
					GB_GET_GANG_BOSS_UUID_INT_B(GB_GET_LOCAL_PLAYER_GANG_BOSS()),
					GB_GET_MATCH_ID_1_FOR_TELEMETRY(), 
					GB_GET_MATCH_ID_2_FOR_TELEMETRY(), 
					BIK_ADD_POINTS_MINIGAME,
					iPointsEarned)
			ENDIF
			
			//Telemetry minigame ended
			PLAYSTATS_MC_CLUBHOUSE_ACTIVITY(
					GB_GET_GANG_BOSS_UUID_INT_A(GB_GET_LOCAL_PLAYER_GANG_BOSS()), 
					GB_GET_GANG_BOSS_UUID_INT_B(GB_GET_LOCAL_PLAYER_GANG_BOSS()),
					GB_GET_MATCH_ID_1_FOR_TELEMETRY(), 
					GB_GET_MATCH_ID_2_FOR_TELEMETRY(), 
					ENUM_TO_INT(eMGID_ARMWRESTLE), 
					-1, 
					0)
			CDEBUG1LN(DEBUG_ARM_WRESTLING, "ARM_WRESTLING TELEM: Minigame Ended")
						
			SET_RESULT_SCREEN_DISPLAYING_STATE(FALSE)
			SET_CELEBRATION_SCREEN_AS_ACTIVE(FALSE)
			CLEANUP_CELEBRATION_SCREEN(sCelebrationData)
			//gdisablerankupmessage = FALSE
			SET_DISABLE_RANK_UP_MESSAGE(FALSE)
			SET_ON_JOB_INTRO(FALSE)
			
			ARM_SET_CLIENT_FLAG(playerData[PARTICIPANT_ID_TO_INT()], ARMCLIENTFLAG_CLEANUP_DONE, TRUE)
	ENDIF
				
	IF (NOT IS_SCREEN_FADING_IN()
		AND IS_SCREEN_FADED_IN()
		AND ARM_GET_CLIENT_FLAG(playerData[PARTICIPANT_ID_TO_INT()], ARMCLIENTFLAG_CLEANUP_DONE))
	//quick exit/cleanup
	OR ARM_GET_CLIENT_FLAG(playerData[PARTICIPANT_ID_TO_INT()], ARMCLIENTFLAG_CLEANUP_DONE)
	OR ARM_GET_CLIENT_FLAG(playerData[PARTICIPANT_ID_TO_INT()], ARMCLIENTFLAG_SPECTATOR)
	//fail safe
	OR GET_TIMER_IN_SECONDS_SAFE(stCleanupTimer) > 6.0
		CDEBUG1LN(DEBUG_ARM_WRESTLING, "All Cleaned Up")
		//SET_AUDIO_FLAG("AllowRadioOverScreenFade", FALSE)
		TERMINATE_THIS_THREAD()
	ENDIF

ENDPROC

// Check for streaming and other init, return true when complete.
FUNC BOOL ARM_IS_STREAMING_COMPLETE_APARTMENT(STREAMED_MODEL& assets[], ARM_UI& armUI, BOOL bSpectator)
	
	IF NOT HAS_ANIM_DICT_LOADED("mini@arm_wrestling")
	OR NOT HAS_ANIM_DICT_LOADED("anim@amb@clubhouse@mini@arm_wrestling@")
		CDEBUG1LN(DEBUG_ARM_WRESTLING, "ARM_IS_STREAMING_COMPLETE_APARTMENT - Anim dictionaries not loaded yet")
		RETURN FALSE	
	ENDIF
		
	IF NOT bSpectator
		IF NOT ARE_MODELS_STREAMED(assets)
			CDEBUG1LN(DEBUG_ARM_WRESTLING, "ARM_IS_STREAMING_COMPLETE_APARTMENT - Not all models are streamed")
			RETURN FALSE
		ENDIF
		
		IF NOT HAS_SCALEFORM_MOVIE_LOADED(armUI.siShardMessage.siMovie)
			CDEBUG1LN(DEBUG_ARM_WRESTLING, "ARM_IS_STREAMING_COMPLETE_APARTMENT - armUI.siShardMessage loading")
			RETURN FALSE
		ENDIF
	
//		IF NOT REQUEST_SCRIPT_AUDIO_BANK("HUD_AWARDS")	
//			CDEBUG1LN(DEBUG_DARTS, "ARM_IS_STREAMING_COMPLETE_APARTMENT - HUD_AWARDS Audio loading")
//			RETURN FALSE
//		ENDIF
		
		IF NOT HAS_ADDITIONAL_TEXT_LOADED(PROPERTY_TEXT_SLOT)
			CDEBUG1LN(DEBUG_DARTS, "ARM_IS_STREAMING_COMPLETE_APARTMENT - PROPERTY_TEXT_SLOT")
			RETURN FALSE
		ENDIF
	ENDIF
	
	CDEBUG1LN(DEBUG_DARTS, "ARM_IS_STREAMING_COMPLETE_APARTMENT - Streaming Complete")
	RETURN TRUE
ENDFUNC

FUNC BOOL ARM_NETWORK_INIT_APARTMENTS(MP_MISSION_DATA& missionScriptArgs, ARM_SERVER_BROADCAST_DATA& serverData, ARM_PLAYER_BROADCAST_DATA& clientBD[32], ARM_PARTICIPANT_INFO & info)
	INT iParticipantID
	
	CLEAR_HELP()
	// This marks the script as a net script, and handles any instancing setup. 
	IF missionScriptArgs.mdID.idMission <> eAM_APARTMENT_ARMWRESTLING
		PRINTLN("Changing missionScriptArgs.mdID.idMission to eAM_ARMWRESTLING...")
		missionScriptArgs.mdID.idMission = eAM_APARTMENT_ARMWRESTLING
	ENDIF
	missionScriptArgs.mdID.idCreator = FMMC_TYPE_MG_ARM_WRESTLING
	//missionScriptArgs.mdID.idVariation = FMMC_TYPE_MG_ARM_WRESTLING
	missionScriptArgs.iInstanceId = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iVisibilityID
	//If we are in freemode launch diffrently

		//Freemode mission start details
		//INT iPlayerMissionToLoad = FMMC_MINI_GAME_CREATOR_ID
		//INT iMissionVariation 	 = 0
		//INT iNumberOfPlayers 	 = 2
		//Call the function that controls the setting up of this script
		//FMMC_PROCESS_PRE_GAME_COMMON(missionScriptArgs, iPlayerMissionToLoad, iMissionVariation, iNumberOfPlayers)
		//when minigame launched in apartments dont set this, Interior script checks if there is a current mission and will force out of apartment.
//		IF bSetMissionID
			//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_MG_ARM_WRESTLING
			//SET_FM_JOB_ENTERY_TYPE
			//CDEBUG1LN(DEBUG_ARM_WRESTLING, "JOBENTRYTYPE:", GET_FM_JOB_ENTERY_TYPE())
//		ENDIF
		//Script has started set the global broadcast data up so every body knows
		
		NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(GET_MAX_NUM_PARTICIPANTS_FOR_MP_MISSION(eAM_APARTMENT_ARMWRESTLING), FALSE, missionScriptArgs.iInstanceId) 
		HANDLE_NET_SCRIPT_INITIALISATION()
		SET_THIS_SCRIPT_CAN_BE_PAUSED(TRUE)
		REGISTER_SCRIPT_WITH_AUDIO(TRUE)
		SET_JOB_ACTIVITY_PROFILE_SETTINGS(PSJA_MINIGAME_STARTED)
		SET_FM_MISSION_LAUNCHED_SUCESS(missionScriptArgs.mdID.idCreator, missionScriptArgs.mdID.idVariation, missionScriptArgs.iInstanceId)

	
	//Common to all modes	
	
	// Make code aware of our server and player broadcast data variables.
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverData, SIZE_OF(serverData))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(clientBD, SIZE_OF(clientBD))
	
	// KGM: Wait for the first network broadcast before moving on
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		RETURN FALSE
	ENDIF
	
	// Set the server and player states, and server should reserve peds & objects.
	iParticipantID = PARTICIPANT_ID_TO_INT()
	
	INITIALIZE_ARM_PARTICIPANT_INFO(info)
	
//	eSelfID = INT_TO_ENUM(ARM_WRESTLER_ID, iParticipantID)
//	eOpponentID = INT_TO_ENUM(ARM_WRESTLER_ID, 1 - iParticipantID)
	
	PRINTLN("iParticipantID = ", iParticipantID)
	PRINTLN("Reserving ", iNETWORK_OBJECTS, " objects...")
	
//	IF eSelfID = ARMWRESTLERID_AWAY
//		RESERVE_NETWORK_MISSION_OBJECTS(iNETWORK_OBJECTS) // Local attachment obj
//		ARM_SET_PLAYER_MP_STATE(clientBD[ARMWRESTLERID_AWAY], ARMMPSTATE_INIT)
//	ELIF eSelfID = ARMWRESTLERID_HOME
//		RESERVE_NETWORK_MISSION_OBJECTS(iNETWORK_OBJECTS)
//		ARM_SET_SERVER_MP_STATE(serverData, ARMMPSTATE_INIT)
//		ARM_SET_PLAYER_MP_STATE(clientBD[ARMWRESTLERID_HOME], ARMMPSTATE_INIT)
//	ENDIF
	
	RESERVE_NETWORK_MISSION_OBJECTS(iNETWORK_OBJECTS) // Local attachment obj
	ARM_SET_PLAYER_MP_STATE(clientBD[iParticipantID], ARMMPSTATE_INIT)
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		ARM_SET_SERVER_MP_STATE(serverData, ARMMPSTATE_INIT)
		//serverData.iMatchHistoryID = PLAYSTATS_CREATE_MATCH_HISTORY_ID()
		PLAYSTATS_CREATE_MATCH_HISTORY_ID_2(serverData.iMatchType, serverData.iMatchHistoryID)
	ENDIF
	
	//DEAL_WITH_FM_MATCH_START(FMMC_TYPE_MG_ARM_WRESTLING, serverData.iMatchHistoryID, serverData.iMatchType)
	
	RETURN TRUE
ENDFUNC

// Set up the table, wrestlers, etc.
PROC ARM_INIT_GAME_APARTMENTS(ARM_ARGS& args, ARM_TABLE& table, STREAMED_MODEL& assets[], ARM_UI& armUI, INT iTableIndex, BOOL bSpectator = FALSE)
	ARM_SET_ARG_TABLE_INDEX(args, iTableIndex)
	ARM_INIT_TABLE(table, iTableIndex)
	
	REQUEST_ANIM_DICT("mini@arm_wrestling")
	REQUEST_ANIM_DICT("anim@amb@clubhouse@mini@arm_wrestling@")
	REGISTER_SCRIPT_WITH_AUDIO()
	IF NOT bSpectator
		// Set up controls, cameras & UI.
		DISPLAY_RADAR(FALSE)
		
		// Stream text, UI, anything needed
		ARM_REQUEST_COUNTDOWN_UI(armUI)
		
		// Streaming requests.
		ADD_STREAMED_MODEL(assets, PROAIR_HOC_PUCK)     // Attachment object
		
		REQUEST_ALL_MODELS(assets)
		
		REQUEST_ADDITIONAL_TEXT("ARM_MP", PROPERTY_TEXT_SLOT)
		
		armUI.siShardMessage.siMovie	= REQUEST_MG_MIDSIZED_MESSAGE()
		
		CDEBUG1LN(DEBUG_ARM_WRESTLING, "Calling LOAD_STREAM('ARM_WRESTLING_CROWD_MASTER')...")
		LOAD_STREAM("ARM_WRESTLING_CROWD_MASTER")
	ENDIF
ENDPROC

// Script entry point.
SCRIPT (MP_MISSION_DATA missionScriptArgs)

	BOOL 	bIsSpectatorPlayer = IS_BIT_SET(missionScriptArgs.iBitSet, ENUM_TO_INT(eMGPT_JOINED_AS_SPECTATOR))
	BOOL	bIsSetupPlayer = IS_BIT_SET(missionScriptArgs.iBitSet, ENUM_TO_INT(eMGPT_JOINED_AS_SETUP))
	BOOL	bIsTableSideA = IS_BIT_SET(missionScriptArgs.iBitSet, ENUM_TO_INT(eMGPT_CLOSEST_TO_A_SIDE))
	INT		iCleanupFlags
	ENUM_ARM_SPEC_STATE	sSpecGameState
	INT					iSpecFlags
	//BOOL	bPlayerPosSelected = FALSE
	//VECTOR 	vPlayerStartPositions[2]
	//PLAYER_INDEX	piPlayerForStartPos[2]
	//structTimer stPlayerPosReceived
//	BOOL	bIsPlayerTableSideA[2]
	
	STREAMED_MODEL assets[iSTREAMS]
//	VECTOR	vPlayerPosition
//	FLOAT	vPlayerHeading
//	VECTOR	vOpponentPosition
//	FLOAT	vOpponentHeading
	
	
	ARM_ARGS args
	ARM_TABLE table
	ARM_WRESTLER wrestlers[ARMWRESTLERIDS]
	ARM_CROWD crowd
	ARM_CAMERA_SET cameraSet
	ARM_UI ui
	ARM_SERVER_BROADCAST_DATA serverData
	ARM_PLAYER_BROADCAST_DATA playerData[32]
	ARM_MP_TERMINATE_REASON terminateReason
	ARM_MP_LEAVE_STATE leaveState
	ARM_UI_RETVAL uiResult
	//ARM_TUTORIAL_STATE ArmTutorialState = ARMTUTSTATE_INTRO
	SIMPLE_USE_CONTEXT armUseContext
	structTimer SpecFailSafeTimer
	//END_SCREEN_DATASET_TITLE_UPDATE ArmEndscreen
	
	structTimer controlTimer
	structTimer streamTimer
	structTimer countdownTimer
	structTimer wrestleTimer
	structTimer crowdTimer
	structTimer syncPosTimer
	structTimer tutorialTimer
	structTimer introTimer
//	structTimer faceAnimTimer
	structTimer syncSceneFailsafeTimer
	structTimer rematchCamDelay
	
	SCRIPT_TIMER timeArmAwardFailSafe
	
//	PED_INDEX piLocalClone1
//	PED_INDEX piLocalClone2
	
	ARM_MP_STATE eSpectateMPState
	
	GENERIC_MINIGAME_STAGE eGenericMGStage = GENERIC_CELEBRATION_STATS //GENERIC_CELEBRATION_WINNER
	PLAYER_INDEX winningPlayer
	
	PLAYER_INDEX broadcastIndex
	//CAMERA_INDEX renderingCam
	CAMERA_INDEX animatedCam
	
	STRING sAnimDict      = "mini@arm_wrestling"
	STRING sRefIntroAnim  = "ref_intro"
	//STRING sReadyHelp
	STRING sWhichWalk
	STRING sWhichCam
	//STRING sTableName
	
//	TEXT_LABEL_63 sOpponentName
//	TEXT_LABEL_63 sColoredName
	TEXT_LABEL_23 contentID	
	
	//VECTOR vPlayerCoord
	//VECTOR vCamCoord
	VECTOR vArmWrestleLocation
	//VECTOR vDestination
//	VECTOR vScenePosition// = << 978.861, -94.320, 74.877 >>
//	VECTOR vSceneRotation// = << 0.000, 0.000, 142.000 >>
	FLOAT fTableRot
	//FLOAT fMyPreferenceLevel
	FLOAT fRematchTimer = 45.0
	FLOAT fTotalTime
	//FLOAT fScoreTrack[iSCORE_TRACK_LENGTH]
	FLOAT fLocalStrengthFactor
	FLOAT fCamRot
	
//	INT iWinSound, iNewWinSound
	INT iWinStreak
	//INT iLocalScene
	INT iLocalSCene2
	INT iStrengthStat
	INT iControlHelpCounter
	INT iMyParticipantID
	
//	BOOL bCamTasked
	BOOL bRematch, bHelp1
	//,bHelp2, bHelp3
	//BOOL bLongerIntro
	BOOL bShardSet
	
	TIMEOFDAY eCurrentTime
	
	BOOL bHappy, bPlayApproachAnim, bFoundPlayer, bShowTutHelp, bSpecRunning
	//bHome,
	
	
	#IF IS_DEBUG_BUILD
	VECTOR vTemp
	#ENDIF
	
	MPGlobals.WrestleData.bArmPlayBusy = FALSE
	
	IF ARM_GET_ARG_TABLE_INDEX(args) <= 0
		ARM_SET_ARG_TABLE_INDEX(args, ARM_GET_NEAREST_TABLE_INDEX())
	ENDIF
	ARM_GET_TABLE_DATA(table.vCenter, table.fRotation, ARM_GET_ARG_TABLE_INDEX(args), TRUE)
	
	CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: args.iTableIndex = ", args.iTableIndex)
	CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: table.vCenter = ", table.vCenter, ". table.fRotation = ", table.fRotation)
	
	// get table name here
	//sTableName = GET_ARM_TABLE_NAME(ARM_GET_ARG_TABLE_INDEX(args))
	//CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: table name = ", sTableName)
	//WAIT(10000)
	// Perform network init and acquire a participant ID.
	BOOL bInitFailCleanup = NOT ARM_NETWORK_INIT_APARTMENTS(missionScriptArgs, serverData, playerData, sParticipantInfo)
	
	// ID to use when referencing the player array.
	// 1 = Host, HOME
	// 0 = Client, AWAY
	eSelfID = ARMWRESTLERID_AWAY
	eOpponentID = ARMWRESTLERID_HOME
		
	IF bInitFailCleanup
		CDEBUG1LN(DEBUG_ARM_WRESTLING, "Failed to receive an initial network broadcast. Cleaning up.")
		//RELEASE_SCRIPT_AUDIO_BANKS(iWinSound)
		ARM_SCRIPT_CLEANUP_APARTMENT(animatedCam, cameraSet, wrestlers, iCleanupFlags, playerData, sParticipantInfo, ui, ARM_GET_ARG_TABLE_INDEX(args), TRUE)
	ENDIF
	
	IF bIsSpectatorPlayer
		CDEBUG1LN(DEBUG_ARM_WRESTLING, "INIT - PALYER SPECTATOR FLAG SET")
	ELSE
		BROADCAST_MG_APARTMENTS_LAUNCHED(eMGID_ARMWRESTLE, missionScriptArgs.iInstanceId)
		
		MGID_CLUBHOUSE_SET(eMGID_ARMWRESTLE)
		ARM_SET_CLIENT_FLAG(playerData[PARTICIPANT_ID_TO_INT()], ARMCLIENTFLAG_TABLE_SIDE_A, bIsTableSideA)
		SET_FM_JOB_ENTERY_TYPE(ciMISSION_ENTERY_TYPE_CLUB_HOUSE)
		ARM_SET_CLIENT_FLAG(playerData[PARTICIPANT_ID_TO_INT()], ARMCLIENTFLAG_VALID_PLAYER, TRUE)
		CDEBUG1LN(DEBUG_ARM_WRESTLING, "INIT - PALYER IS NOT A SPECTATOR")
	ENDIF
	
	ARM_SET_CLIENT_FLAG(playerData[PARTICIPANT_ID_TO_INT()], ARMCLIENTFLAG_SPECTATOR, bIsSpectatorPlayer)
	ARM_GET_TABLE_DATA(vArmWrestleLocation, fTableRot, ARM_GET_ARG_TABLE_INDEX(args), TRUE)
	
	WAIT(500)
	
	IF NOT bIsSpectatorPlayer
	AND NOT IS_PLAYER_SCTV(PLAYER_ID())
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			eSelfID = ARMWRESTLERID_HOME
			eOpponentID = ARMWRESTLERID_AWAY
			CDEBUG1LN(DEBUG_ARM_WRESTLING, "*******************************")
			CDEBUG1LN(DEBUG_ARM_WRESTLING, "eSelfID = ARMWRESTLERID_HOME")
			CDEBUG1LN(DEBUG_ARM_WRESTLING, "eOpponentID = ARMWRESTLERID_AWAY")
			CDEBUG1LN(DEBUG_ARM_WRESTLING, "*******************************")
		ELSE
			CDEBUG1LN(DEBUG_ARM_WRESTLING, "*******************************")
			CDEBUG1LN(DEBUG_ARM_WRESTLING, "eSelfID = ARMWRESTLERID_AWAY")
			CDEBUG1LN(DEBUG_ARM_WRESTLING, "eOpponentID = ARMWRESTLERID_HOME")
			CDEBUG1LN(DEBUG_ARM_WRESTLING, "*******************************")
			
		ENDIF
		
		CDEBUG1LN(DEBUG_ARM_WRESTLING, "Arm Wrestling MP: Network setup complete. My participant ID is ", PICK_STRING(eSelfID = ARMWRESTLERID_HOME, "1 (HOST).", "0 (CLIENT)."))
	ELSE
		CDEBUG1LN(DEBUG_ARM_WRESTLING, "*******************************")
		CDEBUG1LN(DEBUG_ARM_WRESTLING, "I AM SPECTATING!!")
		CDEBUG1LN(DEBUG_ARM_WRESTLING, "*******************************")
		CDEBUG1LN(DEBUG_ARM_WRESTLING, "Arm Wrestling MP: Network setup complete. My participant ID is ", PARTICIPANT_ID_TO_INT())
	ENDIF
	
//	UPDATE_ARM_PARTICIPANT_INFO_APARTMENTS(sParticipantInfo, playerData)
//	PRINT_ARM_PARTICIPANT_INFO(sParticipantInfo)
//	
//	PRINT_PARTICPNAT_INFO(sParticipantInfo, ENUM_TO_INT(eSelfID))
//	PRINT_PARTICPNAT_INFO(sParticipantInfo, ENUM_TO_INT(eOpponentID))
	
	//#### Print participant names.
	#IF IS_DEBUG_BUILD
		CDEBUG1LN(DEBUG_ARM_WRESTLING, "")
		CDEBUG1LN(DEBUG_ARM_WRESTLING, "############# PARTICIPANT NAMES ###############")
		
		
	#ENDIF	
	
	IF NOT IS_PLAYER_SCTV(PLAYER_ID())
	AND NOT bIsSpectatorPlayer
		NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)//, NSPC_CLEAR_TASKS | NSPC_FREEZE_POSITION)
		CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: net control statement made at the start")
	ENDIF
	
	eCurrentTime = GET_CURRENT_TIMEOFDAY()
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		serverData.iCurrentHour = GET_TIMEOFDAY_HOUR(eCurrentTime)
	ENDIF
	
	REQUEST_SYSTEM_ACTIVITY_TYPE_PLAY_ARM_WRESTLING()
	
	// Wait here for a second player.
	WHILE NOT bFoundPlayer
	
		IF bIsSetupPlayer
		AND NOT bIsSpectatorPlayer
			NETWORK_PREVENT_SCRIPT_HOST_MIGRATION()
		ENDIF
		
		UPDATE_ARM_PARTICIPANT_INFO_APARTMENTS(sParticipantInfo, playerData)
		
		// Make sure the player isn't leaving Cops & Criminals (also allow free mode in debug).
		IF NOT NETWORK_IS_IN_SESSION()
			CDEBUG1LN(DEBUG_ARM_WRESTLING, "Network is not in session, cleaning up")
			//RELEASE_SCRIPT_AUDIO_BANKS(iWinSound)
			ARM_SCRIPT_CLEANUP_APARTMENT(animatedCam, cameraSet, wrestlers, iCleanupFlags, playerData, sParticipantInfo, ui, ARM_GET_ARG_TABLE_INDEX(args), TRUE)
		
		// Move on when we have another player.
		ELIF ARM_GET_NUM_WRESTLERS(sParticipantInfo) >= ENUM_TO_INT(ARMWRESTLERIDS)
			bFoundPlayer = TRUE
			
			CDEBUG1LN(DEBUG_ARM_WRESTLING, "am_armwrestling: 2 wrestlers found, starting game")
			
			IF IS_TIMER_STARTED(controlTimer)
				CANCEL_TIMER(controlTimer)
			ENDIF
			
			//Stop people joining my mission
			//SET_FM_MISSION_AS_NOT_JOINABLE()
			
		// check if player launches another mode	
		ELIF (MPGlobals.WrestleData.bArmPlayBusy = TRUE)
			CDEBUG1LN(DEBUG_ARM_WRESTLING, "am_armwrestling: player started sitting, cleaning up")
			CLEAR_HELP()
			// print some help here
			BROADCAST_ARM_WRESTLE_TABLE_ACTIVATE(PLAYER_ID(), FALSE, TRUE)
			ARM_SET_UI_FLAG(UI, ARMUIFLAG_LEAVE_AREA, TRUE)
			//RELEASE_SCRIPT_AUDIO_BANKS(iWinSound)
			ARM_SCRIPT_CLEANUP_APARTMENT(animatedCam, cameraSet, wrestlers, iCleanupFlags, playerData, sParticipantInfo, ui, ARM_GET_ARG_TABLE_INDEX(args), TRUE)
			
		// Update help text and check for quit conditions.
		ELSE
			
			IF NOT IS_TIMER_STARTED(controlTimer)
				CDEBUG1LN(DEBUG_ARM_WRESTLING, "bFoundPlayer = False, setting controlTimer ")
				START_TIMER_NOW(controlTimer)
			ELIF GET_TIMER_IN_SECONDS(controlTimer) > 10.0
				CDEBUG1LN(DEBUG_ARM_WRESTLING, "controlTimer > 10.0, cleaning up")
				//RELEASE_SCRIPT_AUDIO_BANKS(iWinSound)
				ARM_SCRIPT_CLEANUP_APARTMENT(animatedCam, cameraSet, wrestlers, iCleanupFlags, playerData, sParticipantInfo, ui, ARM_GET_ARG_TABLE_INDEX(args), TRUE)
			ENDIF
			
		ENDIF
		
		WAIT(0)
	ENDWHILE
	
	//Swap info around so 2nd player has himself at Index 0 in player array.
	IF NOT bIsSpectatorPlayer
	AND NOT bIsSetupPlayer
		INT iTempID =  sParticipantInfo.iPlayers[eSelfID]
		sParticipantInfo.iPlayers[eSelfID] = sParticipantInfo.iPlayers[eOpponentID] 
		sParticipantInfo.iPlayers[eOpponentID] = iTempID
	ENDIF
	
	
	PRINT_PARTICPNAT_INFO(sParticipantInfo, ENUM_TO_INT(eSelfID))
	PRINT_PARTICPNAT_INFO(sParticipantInfo, ENUM_TO_INT(eOpponentID))
	
	wrestlers[eSelfID].vPlayerPosition = GET_PLAYER_POS(vArmWrestleLocation, fTableRot, bIsTableSideA)
	wrestlers[eSelfID].fPlayerHeading = GET_HEADING_BETWEEN_VECTORS(wrestlers[eSelfID].vPlayerPosition, vArmWrestleLocation)
	CDEBUG1LN(DEBUG_ARM_WRESTLING, "Self Location:", wrestlers[eSelfID].vPlayerPosition)
	
	wrestlers[eOpponentID].vPlayerPosition = GET_PLAYER_POS(vArmWrestleLocation, fTableRot, !bIsTableSideA)
	wrestlers[eOpponentID].fPlayerHeading = GET_HEADING_BETWEEN_VECTORS(wrestlers[eOpponentID].vPlayerPosition, vArmWrestleLocation)
	CDEBUG1LN(DEBUG_ARM_WRESTLING, "Opponenet Location:", wrestlers[eOpponentID].vPlayerPosition)
	// Wait for server and client broadcast data to sync up.
	//WAIT(500)
	
	wrestlers[eSelfID].selfWrestler =  GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(sParticipantInfo.iPlayers[eSelfID]))) 
	wrestlers[eSelfID].otherWrestler = GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(sParticipantInfo.iPlayers[eOpponentID]))) 
	wrestlers[eOpponentID].selfWrestler =  GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(sParticipantInfo.iPlayers[eOpponentID]))) 
	
	CDEBUG1LN(DEBUG_ARM_WRESTLING, "Self Opponent:", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(sParticipantInfo.iPlayers[eOpponentID]))))
	wrestlers[eOpponentID].otherWrestler = GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(sParticipantInfo.iPlayers[eSelfID]))) 
	CDEBUG1LN(DEBUG_ARM_WRESTLING, " Opponents opponent:", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(sParticipantInfo.iPlayers[eSelfID]))))					
	
	// Clear the help text.
	IF ARM_GET_UI_FLAG(ui, ARMUIFLAG_SHOW_WAIT_TO_LAUNCH)
		CLEAR_HELP()
		ARM_SET_UI_FLAG(ui, ARMUIFLAG_SHOW_WAIT_TO_LAUNCH, FALSE)
	ENDIF
	
	// Let the other player know our name.
	IF NOT bIsSpectatorPlayer
	AND NOT IS_PLAYER_SCTV(PLAYER_ID())
		//gdisablerankupmessage = TRUE
		SET_DISABLE_RANK_UP_MESSAGE(TRUE)
		
//		ARM_SET_PLAYER_NAME(playerData[sParticipantInfo.iPlayers[eSelfID]], GET_PLAYER_NAME(PLAYER_ID()))
		ARM_SET_PLAYER_ID(playerData[sParticipantInfo.iPlayers[eSelfID]], PLAYER_ID())
		
		iStrengthStat = GET_MP_INT_CHARACTER_STAT(MP_STAT_STRENGTH)
		CDEBUG1LN(DEBUG_ARM_WRESTLING, "iStrengthStat = ", iStrengthStat)
		fLocalStrengthFactor = iStrengthStat * STRENGTH_SCALE
		CDEBUG1LN(DEBUG_ARM_WRESTLING, "fLocalStrengthFactor = ", fLocalStrengthFactor)
		playerData[sParticipantInfo.iPlayers[eSelfID]].fStrengthFactor = fLocalStrengthFactor + 1
		CDEBUG1LN(DEBUG_ARM_WRESTLING, "playerData[sParticipantInfo.iPlayers[eSelfID]].fStrengthFactor = ", playerData[sParticipantInfo.iPlayers[eSelfID]].fStrengthFactor)
		
		iControlHelpCounter = GET_MP_INT_CHARACTER_STAT(MP_STAT_CRARMWREST)
		CDEBUG1LN(DEBUG_ARM_WRESTLING, "number of arm wrestling games from this player = ", iControlHelpCounter)
		
		CDEBUG1LN(DEBUG_ARM_WRESTLING, "value of PACKED_MP_ARMWRESTLING_TUT_TEXT = ", GET_PACKED_STAT_BOOL(PACKED_MP_ARMWRESTLING_TUT_TEXT))
		SET_PACKED_STAT_BOOL(PACKED_MP_ARMWRESTLING_TUT_TEXT, TRUE)
		CDEBUG1LN(DEBUG_ARM_WRESTLING, "after setting,  PACKED_MP_ARMWRESTLING_TUT_TEXT = ", GET_PACKED_STAT_BOOL(PACKED_MP_ARMWRESTLING_TUT_TEXT))
		
	ENDIF
	IF bMinigamePlayerQuitFlag
		bMinigamePlayerQuitFlag = FALSE
	ENDIF
	
	IF NOT bIsSpectatorPlayer
		ANIMPOSTFX_STOP_ALL()
	ENDIF
	
	CDEBUG1LN(DEBUG_ARM_WRESTLING, " ***** HEADING TO MAIN LOOP ***** ")
	
	eSpectateMPState = ARMMPSTATE_INIT
	
	// Main loop.
	WHILE TRUE
		
	IF bIsSetupPlayer
	AND NOT bIsSpectatorPlayer
		NETWORK_PREVENT_SCRIPT_HOST_MIGRATION()
	ENDIF
	
	UPDATE_PARTICIPANT_COUNT(sParticipantInfo, playerData, wrestlers, bIsSpectatorPlayer)
	
	//IF NETWORK_IS_PARTICIPANT_ACTIVE(PARTICIPANT_ID())
	IF NOT ARM_GET_CLIENT_FLAG(playerData[PARTICIPANT_ID_TO_INT()], ARMCLIENTFLAG_CLEANUP_DONE)
		MP_LOOP_WAIT_ZERO()
		
		MAINTAIN_CELEBRATION_PRE_LOAD(sCelebrationData)
		
		//UPDATE_ARM_PARTICIPANT_INFO_APARTMENTS(sParticipantInfo, playerData)
		iMyParticipantID = PARTICIPANT_ID_TO_INT()
		
		// Make sure we're in Cops & Criminals (Free Mode ok for debug).
		IF NOT NETWORK_IS_IN_SESSION()
			CDEBUG1LN(DEBUG_ARM_WRESTLING, "network not in session, cleaning up")
			//RELEASE_SCRIPT_AUDIO_BANKS(iWinSound)
			ARM_SCRIPT_CLEANUP_APARTMENT(animatedCam, cameraSet, wrestlers, iCleanupFlags, playerData, sParticipantInfo, ui, ARM_GET_ARG_TABLE_INDEX(args), TRUE)
		ENDIF
		
		//############## CHECK NETWORK STATE
		IF  (ARM_GET_PLAYER_MP_STATE(playerData[PARTICIPANT_ID_TO_INT()]) > ARMMPSTATE_INIT 
		AND ARM_GET_PLAYER_MP_STATE(playerData[PARTICIPANT_ID_TO_INT()]) <= ARMMPSTATE_INTRO_SCENE_ALT_END)
		OR bIsSpectatorPlayer
			//IF  HAS_ANIM_DICT_LOADED("anim@amb@clubhouse@mini@arm_wrestling@")
				IF NOT wrestlers[ARMWRESTLERID_HOME].bMoveNetworkStarted
					IF NOT IS_ENTITY_DEAD(wrestlers[ARMWRESTLERID_AWAY].otherWrestler)
						IF IS_TASK_MOVE_NETWORK_ACTIVE(wrestlers[ARMWRESTLERID_AWAY].otherWrestler)
							wrestlers[ARMWRESTLERID_HOME].bMoveNetworkStarted = TRUE
							CDEBUG1LN(DEBUG_ARM_WRESTLING, "Move Network Started For HOME")
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT wrestlers[ARMWRESTLERID_AWAY].bMoveNetworkStarted
					IF NOT IS_ENTITY_DEAD(wrestlers[ARMWRESTLERID_HOME].otherWrestler)
						IF IS_TASK_MOVE_NETWORK_ACTIVE(wrestlers[ARMWRESTLERID_HOME].otherWrestler)
							wrestlers[ARMWRESTLERID_AWAY].bMoveNetworkStarted = TRUE
							CDEBUG1LN(DEBUG_ARM_WRESTLING, "Move Network Started For AWAY")	
						ENDIF
					ENDIF
				ENDIF
			//ENDIF
		ENDIF
		
		//############### SPECTATOR HIDE CHECK, call each frame
		IF NOT bIsSpectatorPlayer
			//NETWORK_OVERRIDE_CLOCK_TIME(serverData.iCurrentHour, 0, 0)
			SET_LOADING_ICON_SUBTITLES_OFFSET_SHIFT_THIS_FRAME()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			
			//############## BLOCK SPECTATE(BETA) MENU IF IS A PLAYER
			IF IS_PAUSE_MENU_ACTIVE()
		    	IF GET_PAUSE_MENU_STATE() = PM_READY
					IF NOT PAUSE_MENU_IS_CONTEXT_ACTIVE(HASH("DisableSpectateScript"))
						CDEBUG1LN(DEBUG_ARM_WRESTLING, "ARM_WRESTLING APARTMENT - DISABLE SPECTATE(BETA) BUTTON")
						PAUSE_MENU_ACTIVATE_CONTEXT(HASH("DisableSpectateScript"))
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//############## VVVV Apartments EXIT Checks
		IF NOT ARM_GET_CLIENT_FLAG(playerData[PARTICIPANT_ID_TO_INT()], ARMCLIENTFLAG_CLEANUP_STARTED)
			//############### NOT INSIDE BUILDING LEAVE CHECK
			IF NOT IS_PLAYER_ENTERING_PROPERTY(GET_PLAYER_INDEX())
				//check if script sohuld terminate - if outside an apartment
				IF NOT IS_PLAYER_IN_MP_PROPERTY(GET_PLAYER_INDEX(), FALSE)
				AND NOT IS_PLAYER_IN_CLUBHOUSE(GET_PLAYER_INDEX())
				OR IS_PLAYER_EXITING_PROPERTY(GET_PLAYER_INDEX())
					CDEBUG1LN(DEBUG_ARM_WRESTLING, "ARM_WRESTLING APARTMENT - TERMINATE NOT INSIDE THE BUILDING")
					ARM_SCRIPT_CLEANUP_APARTMENT(animatedCam, cameraSet, wrestlers, iCleanupFlags, playerData, sParticipantInfo, ui, ARM_GET_ARG_TABLE_INDEX(args), TRUE)
				ENDIF
			ENDIF
			
			//############## IS EXITING WITH WARP TO VEHICLES, UPDATE INTERRIOR, All exit requested.
			IF IS_LOCAL_PLAYER_DOING_A_GROUP_WARP()
			OR g_bPlayerAllExitApartment
			OR GET_UPDATE_CUSTOM_APT()
			OR g_bPlayerShouldBeKickedFromApartment
				SET_BIT(iCleanupFlags, ENUM_TO_INT(eCLEANUP_FLAG_WAIT_FOR_FADED_OUT))
				SET_BIT(iCleanupFlags, ENUM_TO_INT(eCLEANUP_FLAG_DONT_FADE_IN))
				CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "DARTS APARTMENT - TERMINATE: Multi")
				ARM_SCRIPT_CLEANUP_APARTMENT(animatedCam, cameraSet, wrestlers, iCleanupFlags, playerData, sParticipantInfo, ui, ARM_GET_ARG_TABLE_INDEX(args), TRUE)
			ENDIF
		ENDIF
		
		// #### CHECK IF GAME LOOPED AND REACHED START. (not used now)
		IF (NOT bIsSpectatorPlayer AND ARM_GET_PLAYER_MP_STATE(playerData[sParticipantInfo.iPlayers[eSelfID]]) >= ARMMPSTATE_RUNNING)
		OR (bIsSpectatorPlayer AND eSpectateMPState >= ARMMPSTATE_RUNNING)
			IF GET_TASK_MOVE_NETWORK_EVENT_SAFE(wrestlers[ARMWRESTLERID_HOME].otherWrestler, "Intro", "Intro", "Win", "Loss")
				wrestlers[ARMWRESTLERID_AWAY].bReachedIntro = TRUE
				CDEBUG1LN(DEBUG_ARM_WRESTLING, "Player: 1",GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(sParticipantInfo.iPlayers[ARMWRESTLERID_AWAY]))), " ReachedIntro")

			ENDIF
			
			IF GET_TASK_MOVE_NETWORK_EVENT_SAFE(wrestlers[ARMWRESTLERID_AWAY].otherWrestler, "Intro", "Intro", "Win", "Loss")
				wrestlers[ARMWRESTLERID_HOME].bReachedIntro = TRUE
				CDEBUG1LN(DEBUG_ARM_WRESTLING, "Player: 2", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(sParticipantInfo.iPlayers[ARMWRESTLERID_HOME]))), " ReachedIntro")
			ENDIF
		ENDIF
		
			IF GET_TASK_MOVE_NETWORK_EVENT_SAFE(wrestlers[ARMWRESTLERID_HOME].otherWrestler, "Running", "Running", GET_MOVE_NETWORK_STATE_NAME(eMNS_RUNNING), GET_MOVE_NETWORK_STATE_NAME(eMNS_RUNNING))
				wrestlers[ARMWRESTLERID_AWAY].bReachedRunning = TRUE
				CDEBUG1LN(DEBUG_ARM_WRESTLING, "Player: 1",GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(sParticipantInfo.iPlayers[ARMWRESTLERID_AWAY]))), " bReachedRunning")

			ENDIF
			
			IF GET_TASK_MOVE_NETWORK_EVENT_SAFE(wrestlers[ARMWRESTLERID_AWAY].otherWrestler, "Running", "Running", GET_MOVE_NETWORK_STATE_NAME(eMNS_RUNNING), GET_MOVE_NETWORK_STATE_NAME(eMNS_RUNNING))
				wrestlers[ARMWRESTLERID_HOME].bReachedRunning = TRUE
				CDEBUG1LN(DEBUG_ARM_WRESTLING, "Player: 1",GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(sParticipantInfo.iPlayers[ARMWRESTLERID_HOME]))), " bReachedRunning")

			ENDIF
		//ASSERTLN("TEST")
		
		IF bIsSpectatorPlayer
		OR IS_PLAYER_SCTV(PLAYER_ID())
			
			
			RECEIVE_EVENT(iSpecFlags)
			
			// Check here for missing player.
			IF ARM_GET_NUM_WRESTLERS( sParticipantInfo ) < ENUM_TO_INT(ARMWRESTLERIDS)
				CDEBUG1LN(DEBUG_ARM_WRESTLING, "made it into the missing player condition")
				
				STOP_STREAM()
				
				CDEBUG1LN(DEBUG_ARM_WRESTLING, "ARM_GET_NUM_WRESTLERS( sParticipantInfo ) < 2 returned true, terminating")
				
				ARM_SCRIPT_CLEANUP_APARTMENT(animatedCam, cameraSet, wrestlers, iCleanupFlags, playerData, sParticipantInfo, ui, ARM_GET_ARG_TABLE_INDEX(args), TRUE)
			ENDIF
		
			// check for player quit
//			IF eSpectateMPState < ARMMPSTATE_TERMINATE_SPLASH
//			AND sParticipantInfo.iPlayers[ARMWRESTLERID_AWAY] != -1
//			AND sParticipantInfo.iPlayers[ARMWRESTLERID_HOME] != -1
//				IF ARM_GET_PLAYER_MP_STATE(playerData[sParticipantInfo.iPlayers[ARMWRESTLERID_AWAY]]) >= ARMMPSTATE_TERMINATE_SPLASH
//				OR ARM_GET_PLAYER_MP_STATE(playerData[sParticipantInfo.iPlayers[ARMWRESTLERID_HOME]]) >= ARMMPSTATE_TERMINATE_SPLASH
//					//CLEAR_HELP()
//					STOP_STREAM()
//					
//					CDEBUG1LN(DEBUG_ARM_WRESTLING, "made it into the player quit condition")
//					
//					CLEANUP_BIG_MESSAGE()
//					
//					CDEBUG1LN(DEBUG_ARM_WRESTLING, "other player is already in terminate splashe, time to cleanup")
//					
//					IF ARM_GET_SERVER_GAME_STATE(serverData) = ARMGAMESTATE_WINNER
//					OR eSpectateMPState = ARMMPSTATE_WAIT_POST_GAME
//						CDEBUG1LN(DEBUG_ARM_WRESTLING, "game had already ended, setting terminate reason to ENDGAME")
//						terminateReason = ARMMPTERMINATEREASON_ENDGAME
//					ELSE
//						CDEBUG1LN(DEBUG_ARM_WRESTLING, "game was still in play, setting terminate reason to PLAYERLEFT")
//						terminateReason = ARMMPTERMINATEREASON_PLAYERLEFT
//						
//					ENDIF
//					
//					eSpectateMPState = ARMMPSTATE_TERMINATE_SPLASH
//				ENDIF
//			ENDIF
				
			IF (GET_GAME_TIMER() % 5000) < 50
				CDEBUG2LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling: ARM_GET_NUM_WRESTLERS( sParticipantInfo )  = ", ARM_GET_NUM_WRESTLERS( sParticipantInfo ))
			ENDIF
			
			IF eSpectateMPState > ARMMPSTATE_SETUP
			//AND DOES_CAM_EXIST(animatedCam)
				IF NOT IS_PED_INJURED(wrestlers[ARMWRESTLERID_AWAY].otherWrestler)
				AND NOT IS_PED_INJURED(wrestlers[ARMWRESTLERID_HOME].otherWrestler)
					#IF IS_DEBUG_BUILD
					IF (GET_GAME_TIMER() % 3000) < 50
						CDEBUG2LN(DEBUG_ARM_WRESTLING, "************************************")
						vTemp = GET_ENTITY_COORDS(wrestlers[ARMWRESTLERID_AWAY].otherWrestler)
						CDEBUG2LN(DEBUG_ARM_WRESTLING, "ARMWRESTLERID_AWAY coords: ", vTemp)
						vTemp = GET_ENTITY_COORDS(wrestlers[ARMWRESTLERID_HOME].otherWrestler)
						CDEBUG2LN(DEBUG_ARM_WRESTLING, "ARMWRESTLERID_HOME coords: ", vTemp)
						vTemp = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(wrestlers[ARMWRESTLERID_AWAY].otherWrestler, GET_ENTITY_COORDS(wrestlers[ARMWRESTLERID_HOME].otherWrestler))
						CDEBUG2LN(DEBUG_ARM_WRESTLING, "home offset from away coords: ", vTemp)
						vTemp = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(wrestlers[ARMWRESTLERID_HOME].otherWrestler, GET_ENTITY_COORDS(wrestlers[ARMWRESTLERID_AWAY].otherWrestler))
						CDEBUG2LN(DEBUG_ARM_WRESTLING, "away offset from home coords: ", vTemp)
						CDEBUG2LN(DEBUG_ARM_WRESTLING, "vArmWrestleLocation: ", vArmWrestleLocation)
						CDEBUG2LN(DEBUG_ARM_WRESTLING, "fTableRot: ", fTableRot)
						CDEBUG2LN(DEBUG_ARM_WRESTLING, "************************************")
					ENDIF
					#ENDIF
					
					IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(sParticipantInfo.iPlayers[ARMWRESTLERID_HOME]))	
					AND NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(sParticipantInfo.iPlayers[ARMWRESTLERID_AWAY]))

						BOOL bIsTableSideASpec = FALSE
						bIsTableSideASpec = ARM_GET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[ARMWRESTLERID_HOME]], ARMCLIENTFLAG_TABLE_SIDE_A)
						NETWORK_OVERRIDE_COORDS_AND_HEADING(wrestlers[ARMWRESTLERID_HOME].otherWrestler, GET_PLAYER_POS(vArmWrestleLocation, fTableRot, !bIsTableSideASpec), 
						PICK_FLOAT(bIsTableSideASpec,fTableRot, WRAP(fTableRot + fOVERRIDE_heading, 0.0, 360.0)))
															
						bIsTableSideASpec = ARM_GET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[ARMWRESTLERID_AWAY]], ARMCLIENTFLAG_TABLE_SIDE_A)
						NETWORK_OVERRIDE_COORDS_AND_HEADING(wrestlers[ARMWRESTLERID_AWAY].otherWrestler, GET_PLAYER_POS(vArmWrestleLocation, fTableRot, !bIsTableSideASpec), 
						PICK_FLOAT(bIsTableSideASpec,fTableRot, WRAP(fTableRot + fOVERRIDE_heading, 0.0, 360.0)))
					ENDIF

				ENDIF
			ENDIF
			
			SWITCH eSpectateMPState
				CASE ARMMPSTATE_INIT
					IF LOAD_ASSETS()
						// Wait until the server moves beyond the init state.
						IF ARM_GET_SERVER_MP_STATE(serverData) > ARMMPSTATE_INIT
							ARM_SET_ARG_TABLE_INDEX(args, ARM_GET_SERVER_TABLE_INDEX(serverData))
							ARM_GET_TABLE_DATA(table.vCenter, table.fRotation, ARM_GET_ARG_TABLE_INDEX(args), TRUE)
							
							ARM_INIT_GAME_APARTMENTS(args, table, assets, ui, ARM_GET_ARG_TABLE_INDEX(args), TRUE)

							ARM_GET_TABLE_DATA(vArmWrestleLocation, fTableRot, ARM_GET_ARG_TABLE_INDEX(args), TRUE)

							CDEBUG1LN(DEBUG_ARM_WRESTLING, "~~~~~~~~~~~AM_Armwrestling.sc: vArmWrestleLocation is now ", vArmWrestleLocation)
							
							ARM_SET_SPEC_MP_STATE(eSpectateMPState, ARMMPSTATE_TRAVEL)
						ENDIF
					ELSE
						CDEBUG1LN(DEBUG_ARM_WRESTLING, " SPEC ASSETS HAVE NOT LOADED YET")
					ENDIF
				BREAK
				
				CASE ARMMPSTATE_TRAVEL
					
					IF IS_TIMER_STARTED(syncPosTimer)
						CANCEL_TIMER(syncPosTimer)
					ENDIF
					
					ARM_SET_SPEC_MP_STATE(eSpectateMPState, ARMMPSTATE_SETUP)
					
				BREAK
				
				CASE ARMMPSTATE_SETUP
					
					IF ARM_IS_STREAMING_COMPLETE_APARTMENT(assets, ui, TRUE)
						
						IF NOT IS_TIMER_STARTED(streamTimer)
							START_TIMER_NOW(streamTimer)
						ENDIF
						
						IF sParticipantInfo.iPlayers[ARMWRESTLERID_AWAY] > -1
						AND sParticipantInfo.iPlayers[ARMWRESTLERID_HOME] > -1
							
							IF DOES_ENTITY_EXIST(wrestlers[ARMWRESTLERID_AWAY].otherWrestler)
							AND NOT IS_PED_INJURED(wrestlers[ARMWRESTLERID_AWAY].otherWrestler)
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: away wrestler EXISTS AND IS NOT INJURED")
							ELSE
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: away wrestler does NOT exist")
							ENDIF
							
							IF DOES_ENTITY_EXIST(wrestlers[ARMWRESTLERID_HOME].otherWrestler)
							AND NOT IS_PED_INJURED(wrestlers[ARMWRESTLERID_HOME].otherWrestler)
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: home wrestler EXISTS AND IS NOT INJURED")
									
							ELSE
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: home wrestler does NOT exist")
							ENDIF
							
							RESTART_TIMER_NOW(streamTimer)
							
							iArmStats[ARM_STAT_NUM_MATCHES]++
							
							
							ARM_SET_SPEC_MP_STATE(eSpectateMPState, ARMMPSTATE_SETUP_POST)
						ELSE
							CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: spec sParticipantInfo.iPlayers[ARMWRESTLERID_AWAY]: ", sParticipantInfo.iPlayers[ARMWRESTLERID_AWAY], " sParticipantInfo.iPlayers[ARMWRESTLERID_HOME]: ", sParticipantInfo.iPlayers[ARMWRESTLERID_HOME])
						ENDIF
					ELSE
						CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: spec waiting for streaming to finish")
					ENDIF
					
				BREAK
				
				CASE ARMMPSTATE_SETUP_POST
						CANCEL_TIMER(streamTimer)
					// Wait for peds to run the move network.
					IF IS_MOVE_NETWORK_ACTIVE_SAFE(wrestlers[ARMWRESTLERID_HOME].otherWrestler)
					AND IS_MOVE_NETWORK_ACTIVE_SAFE(wrestlers[ARMWRESTLERID_AWAY].otherWrestler)
						CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: home init move state: ", GET_TASK_MOVE_NETWORK_STATE(wrestlers[ARMWRESTLERID_HOME].otherWrestler))
						CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: away init move state: ", GET_TASK_MOVE_NETWORK_STATE(wrestlers[ARMWRESTLERID_AWAY].otherWrestler))
						ARM_SET_SPEC_MP_STATE(eSpectateMPState, ARMMPSTATE_INTRO_SCENE_ALT_END)
					ELSE
						IF (GET_GAME_TIMER() % 1000) < 50
							IF NOT IS_TASK_MOVE_NETWORK_ACTIVE(wrestlers[ARMWRESTLERID_HOME].otherWrestler)
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: Move network for ARMWRESTLERID_HOME is not active yet")
							ENDIF
							
							IF NOT IS_TASK_MOVE_NETWORK_ACTIVE(wrestlers[ARMWRESTLERID_AWAY].otherWrestler)
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: Move network for ARMWRESTLERID_AWAY is not active yet")
							ENDIF
						ENDIF
					ENDIF
				BREAK
				

				CASE ARMMPSTATE_INTRO_SCENE_ALT_END
					

					IF ARM_GET_SERVER_GAME_STATE(serverData) = ARMGAMESTATE_PLAYING 
					OR ARM_GET_SERVER_MP_STATE(serverData) < ARMMPSTATE_RUNNING
						IF IS_MOVE_NETWORK_ACTIVE_SAFE(wrestlers[ARMWRESTLERID_HOME].otherWrestler)
						AND IS_MOVE_NETWORK_ACTIVE_SAFE(wrestlers[ARMWRESTLERID_AWAY].otherWrestler)
							IF ARE_STRINGS_EQUAL(GET_TASK_MOVE_NETWORK_STATE(wrestlers[ARMWRESTLERID_HOME].otherWrestler), "Running")
							AND ARE_STRINGS_EQUAL(GET_TASK_MOVE_NETWORK_STATE(wrestlers[ARMWRESTLERID_AWAY].otherWrestler), "Running")
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "STATE - GAME IS ALREADY RUNNING ON SERVER, AND IN RUNNING NET STATE")
								sSpecGameState = eASS_RUNNING
							
							ELIF ARE_STRINGS_EQUAL(GET_TASK_MOVE_NETWORK_STATE(wrestlers[ARMWRESTLERID_HOME].otherWrestler), "StandIdle")
							AND ARE_STRINGS_EQUAL(GET_TASK_MOVE_NETWORK_STATE(wrestlers[ARMWRESTLERID_AWAY].otherWrestler), "StandIdle")
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "STATE - GAME IS WAITING FOR AN ACTION! GO WAIT")
								sSpecGameState = eASS_WAITING
							
							ELSE
								//INTRO?
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "STATE - GAME IS ALREADY RUNNING NOT IDLE NOT RUNNING - DO INTRO")
								SET_BIT(iSpecFlags, ENUM_TO_INT(eSF_APPROACH))
								sSpecGameState = eASS_WAITING
							ENDIF
						ELSE
						
							CDEBUG1LN(DEBUG_ARM_WRESTLING, "STATE - GAME IS NOT RUNNING..")
						ENDIF
								
					ELIF ARM_GET_SERVER_GAME_STATE(serverData) = ARMGAMESTATE_READYMENU
						CDEBUG1LN(DEBUG_ARM_WRESTLING, "STATE - WAITING READY MENU")
						sSpecGameState = eASS_WAITING
						
					ELIF ARM_GET_SERVER_GAME_STATE(serverData) = ARMGAMESTATE_WINNER
						IF IS_MOVE_NETWORK_ACTIVE_SAFE(wrestlers[ARMWRESTLERID_HOME].otherWrestler)
						AND IS_MOVE_NETWORK_ACTIVE_SAFE(wrestlers[ARMWRESTLERID_AWAY].otherWrestler)
							IF ARE_STRINGS_EQUAL(GET_TASK_MOVE_NETWORK_STATE(wrestlers[ARMWRESTLERID_HOME].otherWrestler), "Running")
							AND ARE_STRINGS_EQUAL(GET_TASK_MOVE_NETWORK_STATE(wrestlers[ARMWRESTLERID_AWAY].otherWrestler), "Running")
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "STATE - WINNER already in running state, put into runing and wait for update?.")//FORCE TO GO BACK NETURAL.
								IF NOT IS_BIT_SET(iSpecFlags, ENUM_TO_INT(eSF_APPROACH)) //if server not requested approach by this time , then try paly neutral when in run.
									SET_BIT(iSpecFlags, ENUM_TO_INT(eSF_NETURAL))
								ENDIF
								sSpecGameState = eASS_RUNNING
							ELSE
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "STATE - WINNER GO TO WAITING not running already")
								sSpecGameState = eASS_WAITING
							ENDIF
							
						ELSE
							CDEBUG1LN(DEBUG_ARM_WRESTLING, "STATE - WINNER GO TO WAITING network not active")
							sSpecGameState = eASS_WAITING
						ENDIF
					

					ENDIF
					
					
					ARM_SET_SPEC_MP_STATE(eSpectateMPState, ARMMPSTATE_RUNNING)
				BREAK
				
				// Wait for the other player to ready up.
				CASE ARMMPSTATE_WAIT_TO_START_GAME
					//IF ARM_SERVER_GET_FLAG(serverData, ARMSERVERFLAG_MOVETOGAME)
//						IF NOT IS_TIMER_STARTED(introTimer)
//							RESTART_TIMER_NOW(introTimer)
//							CDEBUG1LN(DEBUG_ARM_WRESTLING, "spectator started intro timer")
//						ELIF GET_TIMER_IN_SECONDS(introTimer) > 4.0
//						
							// Request the win/loss audio if needed.
//							iNewWinSound = GET_RANDOM_INT_IN_RANGE(1, 4)
//							IF iWinSound <> iNewWinSound
//								IF iWinSound <> 0
//									RELEASE_NAMED_SCRIPT_AUDIO_BANK(ARM_GET_CROWD_SOUND_BANK_FROM_INDEX(iWinSound, TRUE))
//									RELEASE_NAMED_SCRIPT_AUDIO_BANK(ARM_GET_CROWD_SOUND_BANK_FROM_INDEX(iWinSound, FALSE))
//								ENDIF
//								iWinSound = iNewWinSound
//								REQUEST_SCRIPT_AUDIO_BANK(ARM_GET_CROWD_SOUND_BANK_FROM_INDEX(iWinSound, TRUE))
//								REQUEST_SCRIPT_AUDIO_BANK(ARM_GET_CROWD_SOUND_BANK_FROM_INDEX(iWinSound, FALSE))
//							ENDIF
														
							RESTART_TIMER_NOW(wrestleTimer)
							RESTART_TIMER_NOW(countdownTimer)
							
							CANCEL_TIMER(introTimer)
							
//							ARM_SET_UI_FLAG(ui, ARMUIFLAG_SHOW_TUTORIAL, TRUE)
//							ARM_SET_UI_FLAG(ui, ARMUIFLAG_SHOW_WAIT_READY, FALSE)
							//eSpectateMPState = ARMMPSTATE_RUNNING
							ARM_SET_SPEC_MP_STATE(eSpectateMPState, ARMMPSTATE_RUNNING)
							CDEBUG1LN(DEBUG_ARM_WRESTLING, "spectator going to running")

				BREAK
				
				CASE ARMMPSTATE_RUNNING
				
					SWITCH(sSpecGameState)
	
						CASE eASS_WAITING
			
							//WAITING FOR APPROACH ANIM TO START PLAYING
							IF IS_BIT_SET(iSpecFlags, ENUM_TO_INT(eSF_APPROACH))
			
								sWhichWalk = GET_MOVE_NETWORK_STATE_NAME(eMNS_RUNNING)
								bArmIdleCamSet = FALSE
								IF IS_MOVE_NETWORK_ACTIVE_SAFE(wrestlers[ARMWRESTLERID_HOME].otherWrestler)
								AND IS_MOVE_NETWORK_ACTIVE_SAFE(wrestlers[ARMWRESTLERID_AWAY].otherWrestler)
									INT iWrestlerID
									REPEAT ARMWRESTLERIDS iWrestlerID
										IF NOT ARE_STRINGS_EQUAL(GET_TASK_MOVE_NETWORK_STATE(wrestlers[iWrestlerID].otherWrestler), sWhichWalk)
											IF IS_TASK_MOVE_NETWORK_READY_FOR_TRANSITION(wrestlers[iWrestlerID].otherWrestler)
												IF REQUEST_TASK_MOVE_NETWORK_STATE_TRANSITION(wrestlers[iWrestlerID].otherWrestler, sWhichWalk)
													CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: requested for home: ", sWhichWalk)
												ELSE
													CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: request was denied for home: ", sWhichWalk)
												ENDIF
											ELSE
												IF (GET_GAME_TIMER() % 1000) < 50
													CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: home not ready for transition in beginning: ", sWhichWalk)
												ENDIF
											ENDIF
										ENDIF
									ENDREPEAT
									
									
									IF (ARE_STRINGS_EQUAL(GET_TASK_MOVE_NETWORK_STATE(wrestlers[ARMWRESTLERID_AWAY].otherWrestler), sWhichWalk)
									AND ARE_STRINGS_EQUAL(GET_TASK_MOVE_NETWORK_STATE(wrestlers[ARMWRESTLERID_HOME].otherWrestler), sWhichWalk))
										CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: away move state: ", GET_TASK_MOVE_NETWORK_STATE(wrestlers[ARMWRESTLERID_AWAY].otherWrestler))
										CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: home move state: ", GET_TASK_MOVE_NETWORK_STATE(wrestlers[ARMWRESTLERID_HOME].otherWrestler))
										
										CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: Played Intro ")
										CLEAR_BIT(iSpecFlags, ENUM_TO_INT(eSF_APPROACH))
										
										SWITCH_ARM_SPEC_STATE(sSpecGameState, eASS_RUNNING)					
									ELSE
										IF (GET_GAME_TIMER() % 1000) < 50
											CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: wrestlers are not at Walk yet")
											CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: player move state: ", GET_TASK_MOVE_NETWORK_STATE(wrestlers[ARMWRESTLERID_AWAY].otherWrestler))
											CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: other  move state: ", GET_TASK_MOVE_NETWORK_STATE(wrestlers[ARMWRESTLERID_HOME].otherWrestler))
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
							IF IS_BIT_SET(iSpecFlags, ENUM_TO_INT(eSF_GAME_END))
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: SpecreachedRunning But eSF_GAME_END")
								CLEAR_BIT(iSpecFlags, ENUM_TO_INT(eSF_GAME_END))
								SWITCH_ARM_SPEC_STATE(sSpecGameState, eASS_NEUTRAL)
							ENDIF
						BREAK
						
						CASE eASS_RUNNING
							

							
							IF IS_MOVE_NETWORK_ACTIVE_SAFE(wrestlers[ARMWRESTLERID_HOME].otherWrestler)
							AND IS_MOVE_NETWORK_ACTIVE_SAFE(wrestlers[ARMWRESTLERID_AWAY].otherWrestler)
								IF NOT bSpecRunning	
									IF NOT IS_TIMER_STARTED(SpecFailSafeTimer)
										START_TIMER_NOW(SpecFailSafeTimer)
										CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling. start run failsafe timer")
									ENDIF
							
									IF NOT ARE_STRINGS_EQUAL(GET_TASK_MOVE_NETWORK_STATE(wrestlers[ARMWRESTLERID_HOME].otherWrestler), "Running")
									AND NOT ARE_STRINGS_EQUAL(GET_TASK_MOVE_NETWORK_STATE(wrestlers[ARMWRESTLERID_AWAY].otherWrestler), "Running")
										//Wait for network to reach specific event
										IF (wrestlers[ARMWRESTLERID_HOME].bReachedRunning
											AND wrestlers[ARMWRESTLERID_AWAY].bReachedRunning)
										OR GET_TIMER_IN_SECONDS(SpecFailSafeTimer) > 8.0
											
											IF GET_TIMER_IN_SECONDS(SpecFailSafeTimer) > 8.0
												CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling. RUN CHECK TIMER > 8")
											ENDIF
											
											IF (wrestlers[ARMWRESTLERID_HOME].bReachedRunning
											AND wrestlers[ARMWRESTLERID_AWAY].bReachedRunning)
												CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling. BOTH RUNNING")
											ENDIF
											
											INT iWrestlerID
											REPEAT ARMWRESTLERIDS iWrestlerID
												IF IS_TASK_MOVE_NETWORK_READY_FOR_TRANSITION(wrestlers[iWrestlerID].otherWrestler)
													IF REQUEST_TASK_MOVE_NETWORK_STATE_TRANSITION(wrestlers[iWrestlerID].otherWrestler, "Running")
														CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: Running requested for", iWrestlerID)
														SET_TASK_MOVE_NETWORK_SIGNAL_FLOAT(wrestlers[iWrestlerID].otherWrestler, "Phase", 0.5)
													ENDIF
												ELSE
													IF (GET_GAME_TIMER() % 2000) < 50
														CDEBUG1LN(DEBUG_ARM_WRESTLING, 
														"AM_Armwrestling.sc: not ready for a transition to running ", GET_TASK_MOVE_NETWORK_STATE(wrestlers[iWrestlerID].otherWrestler))
													ENDIF
												ENDIF
											ENDREPEAT
										ELSE
											IF (GET_GAME_TIMER() % 2000) < 50
												CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: Have not reached neutral idle finished.")
											ENDIF
										ENDIF
									ELSE
										IF (GET_GAME_TIMER() % 2000) < 50
											CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: ARMWRESTLERID_HOME current state is ", GET_TASK_MOVE_NETWORK_STATE(wrestlers[ARMWRESTLERID_HOME].otherWrestler))
											CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: ARMWRESTLERID_AWAY current state is ", GET_TASK_MOVE_NETWORK_STATE(wrestlers[ARMWRESTLERID_AWAY].otherWrestler))
										ENDIF
									ENDIF
									
									// End Move state change to Running -------------------------------------------------------------------
									IF ARE_STRINGS_EQUAL(GET_TASK_MOVE_NETWORK_STATE(wrestlers[ARMWRESTLERID_HOME].otherWrestler), "Running")
									AND ARE_STRINGS_EQUAL(GET_TASK_MOVE_NETWORK_STATE(wrestlers[ARMWRESTLERID_AWAY].otherWrestler), "Running")
									
										
										
										IF IS_TIMER_STARTED(SpecFailSafeTimer)
											CANCEL_TIMER(SpecFailSafeTimer)
										ENDIF
													
										wrestlers[ARMWRESTLERID_HOME].bReachedRunning = FALSE
										wrestlers[ARMWRESTLERID_AWAY].bReachedRunning = FALSE
										
										bSpecRunning = TRUE
									ENDIF
									
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: Phase set to default 0.5 for both players.")
									SET_TASK_MOVE_NETWORK_SIGNAL_FLOAT(wrestlers[ARMWRESTLERID_HOME].otherWrestler, "Phase", 0.5)
									SET_TASK_MOVE_NETWORK_SIGNAL_FLOAT(wrestlers[ARMWRESTLERID_AWAY].otherWrestler, "Phase", 0.5)
								ELSE
									//SET_TASK_MOVE_NETWORK_SIGNAL_FLOAT(wrestlers[ARMWRESTLERID_HOME].otherWrestler, "Phase", 0.5)
									//SET_TASK_MOVE_NETWORK_SIGNAL_FLOAT(wrestlers[ARMWRESTLERID_AWAY].otherWrestler, "Phase", 0.5)
									//UPDATE WRESTLERS
									IF ARE_STRINGS_EQUAL(GET_TASK_MOVE_NETWORK_STATE(wrestlers[ARMWRESTLERID_HOME].otherWrestler), "Running")
									AND ARE_STRINGS_EQUAL(GET_TASK_MOVE_NETWORK_STATE(wrestlers[ARMWRESTLERID_AWAY].otherWrestler), "Running")
										
										IF IS_BIT_SET(iSpecFlags, ENUM_TO_INT(eSF_GAME_END))
											CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: SpecreachedRunning But eSF_GAME_END")
											CLEAR_BIT(iSpecFlags, ENUM_TO_INT(eSF_GAME_END))
											SWITCH_ARM_SPEC_STATE(sSpecGameState, eASS_NEUTRAL)
										ENDIF
										
										IF IS_BIT_SET(iSpecFlags, ENUM_TO_INT(eSF_NETURAL))
											CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: SpecreachedRunning But eSF_NETURAL")
											CLEAR_BIT(iSpecFlags, ENUM_TO_INT(eSF_NETURAL))
											SWITCH_ARM_SPEC_STATE(sSpecGameState, eASS_NEUTRAL)
										ENDIF
										
										IF IS_BIT_SET(iSpecFlags, ENUM_TO_INT(eSF_WIN_LOSS))
											CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: SpecreachedRunning But eSF_WIN_LOSS")
											CLEAR_BIT(iSpecFlags, ENUM_TO_INT(eSF_WIN_LOSS))
											SWITCH_ARM_SPEC_STATE(sSpecGameState, eASS_WIN_LOSS)		
										ENDIF
										
									ELSE
										CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: SpecreachedRunning But network is no longer in running state")
									ENDIF
								ENDIF
							ELSE
								IF (GET_GAME_TIMER() % 2000) < 50
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: ARMWRESTLERID_HOME In Runnig State MOVE NET Not Active ", IS_MOVE_NETWORK_ACTIVE_SAFE(wrestlers[ARMWRESTLERID_HOME].otherWrestler))
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: ARMWRESTLERID_AWAY In Runnig State MOVE NET Not Active ", IS_MOVE_NETWORK_ACTIVE_SAFE(wrestlers[ARMWRESTLERID_AWAY].otherWrestler))
								ENDIF
							ENDIF	
							
							ARM_UPDATE_WRESTLERS_SPECTATOR_APARTMENTS(wrestlers, serverData, vArmWrestleLocation)
						BREAK
						
						CASE eASS_WIN_LOSS
								
							bSpecRunning = FALSE
							
							IF NOT bArmIdleCamSet
								PLAY_SOUND_FROM_COORD(-1, "ARM_WRESTLING_ARM_IMPACT_MASTER", vArmWrestleLocation)
								bArmIdleCamSet = TRUE
							ENDIF
							
							// Determine our current status.
							// b happy in this case says home win = true, away win = false
							bHappy = !IS_BIT_SET(iSpecFlags, ENUM_TO_INT(eSF_HOST_WON)) 
							//(ARM_SERVER_GET_FLAG(serverData, ARMSERVERFLAG_HOMEWIN) AND eSelfID = ARMWRESTLERID_HOME) OR 
							//		(ARM_SERVER_GET_FLAG(serverData, ARMSERVERFLAG_AWAYWIN) AND eSelfID = ARMWRESTLERID_AWAY)
							
							// See if we are playing our reaction anim.
							IF NOT ARM_GET_CLIENT_FLAG(playerData[iMyParticipantID], ARMCLIENTFLAG_WIN_LOSS_REACT)
																							
								// Move state change to Results -----------------------------------------------------------------------
									IF IS_MOVE_NETWORK_ACTIVE_SAFE(wrestlers[ARMWRESTLERID_HOME].otherWrestler)
									AND IS_MOVE_NETWORK_ACTIVE_SAFE(wrestlers[ARMWRESTLERID_AWAY].otherWrestler)
										IF NOT ARE_STRINGS_EQUAL(GET_TASK_MOVE_NETWORK_STATE(wrestlers[ARMWRESTLERID_AWAY].otherWrestler), PICK_STRING(bHappy, "Win", "Loss"))
										AND NOT ARE_STRINGS_EQUAL(GET_TASK_MOVE_NETWORK_STATE(wrestlers[ARMWRESTLERID_HOME].otherWrestler), PICK_STRING(!bHappy, "Win", "Loss"))
											
											IF IS_TASK_MOVE_NETWORK_READY_FOR_TRANSITION(wrestlers[ARMWRESTLERID_HOME].otherWrestler)
												IF REQUEST_TASK_MOVE_NETWORK_STATE_TRANSITION(wrestlers[ARMWRESTLERID_HOME].otherWrestler, PICK_STRING(!bHappy, "Win", "Loss"))
													CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: results requested for ARMWRESTLERID_AWAY")
												ELSE
													CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: results request was denied for ARMWRESTLERID_AWAY")
												ENDIF
											ENDIF
											
											IF IS_TASK_MOVE_NETWORK_READY_FOR_TRANSITION(wrestlers[ARMWRESTLERID_AWAY].otherWrestler)
												IF REQUEST_TASK_MOVE_NETWORK_STATE_TRANSITION(wrestlers[ARMWRESTLERID_AWAY].otherWrestler, PICK_STRING(bHappy, "Win", "Loss"))
													CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: results requested for ARMWRESTLERID_HOME")
												ELSE
													CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: results request was denied for ARMWRESTLERID_HOME")
												ENDIF
											ENDIF
										ENDIF
										
										IF (ARE_STRINGS_EQUAL(GET_TASK_MOVE_NETWORK_STATE(wrestlers[ARMWRESTLERID_AWAY].otherWrestler), PICK_STRING(bHappy, "Win", "Loss"))
										AND ARE_STRINGS_EQUAL(GET_TASK_MOVE_NETWORK_STATE(wrestlers[ARMWRESTLERID_HOME].otherWrestler), PICK_STRING(!bHappy, "Win", "Loss")))
											CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: ARMWRESTLERID_HOME move state: ", GET_TASK_MOVE_NETWORK_STATE(wrestlers[ARMWRESTLERID_HOME].otherWrestler))
											CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: ARMWRESTLERID_AWAY move state: ", GET_TASK_MOVE_NETWORK_STATE(wrestlers[ARMWRESTLERID_AWAY].otherWrestler))
											
											SWITCH_ARM_SPEC_STATE(sSpecGameState, eASS_WAITING)		
										ELSE
											IF (GET_GAME_TIMER() % 1000) < 50
												CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: wrestlers are not at results yet yet")
											ENDIF
										ENDIF
									ELSE
										IF (GET_GAME_TIMER() % 1000) < 50
											CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: wrestlers move network is not active in _winner")
										ENDIF
									ENDIF
								// End Move state change to Results -------------------------------------------------------------------
							
							
							ELSE
									
								//resets for arm placement
								ARM_SET_CLIENT_FLAG(playerData[iMyParticipantID], ARMCLIENTFLAG_WINNER_SET, FALSE)
								fLastScore = 0
								fUsedScore = 0
								fScoreInterp = 0
								
							
							ENDIF
	
						BREAK
						
						CASE eASS_NEUTRAL
							IF IS_MOVE_NETWORK_ACTIVE_SAFE(wrestlers[ARMWRESTLERID_HOME].otherWrestler)
							AND IS_MOVE_NETWORK_ACTIVE_SAFE(wrestlers[ARMWRESTLERID_AWAY].otherWrestler)	
								IF NOT ARE_STRINGS_EQUAL(GET_TASK_MOVE_NETWORK_STATE(wrestlers[ARMWRESTLERID_HOME].otherWrestler), "CancelNeutral")
								AND NOT ARE_STRINGS_EQUAL(GET_TASK_MOVE_NETWORK_STATE(wrestlers[ARMWRESTLERID_AWAY].otherWrestler), "CancelNeutral")
									INT iWrestlerID
									REPEAT ARMWRESTLERIDS iWrestlerID
										IF IS_TASK_MOVE_NETWORK_READY_FOR_TRANSITION(wrestlers[iWrestlerID].otherWrestler)
											IF REQUEST_TASK_MOVE_NETWORK_STATE_TRANSITION(wrestlers[iWrestlerID].otherWrestler, "CancelNeutral")
												CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: Running requested for ARMWRESTLERID_HOME")
											ENDIF
										ELSE
											IF (GET_GAME_TIMER() % 2000) < 50
												CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: ARMWRESTLERID_HOME not ready for a transition to CancelNeutral ", GET_TASK_MOVE_NETWORK_STATE(wrestlers[iWrestlerID].otherWrestler))
											ENDIF
										ENDIF
									ENDREPEAT
								ELSE
									IF (GET_GAME_TIMER() % 2000) < 50
										CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: ARMWRESTLERID_HOME current state is ", GET_TASK_MOVE_NETWORK_STATE(wrestlers[ARMWRESTLERID_HOME].otherWrestler))
										CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: ARMWRESTLERID_AWAY current state is ", GET_TASK_MOVE_NETWORK_STATE(wrestlers[ARMWRESTLERID_AWAY].otherWrestler))
									ENDIF
									
									SWITCH_ARM_SPEC_STATE(sSpecGameState, eASS_WAITING)	
									IF IS_BIT_SET(iSpecFlags, ENUM_TO_INT(eSF_APPROACH))
										CLEAR_BIT(iSpecFlags, ENUM_TO_INT(eSF_APPROACH))
									ENDIF
								ENDIF
							ENDIF
						BREAK
						
						CASE eASS_CANCEL
						
						BREAK
						
					ENDSWITCH
				BREAK
				
				// Waiting for the server to let us go back to the ready menu.
				CASE ARMMPSTATE_WAIT_POST_GAME
					
					// If server tells us to go back to the menu, do so.
					IF ARM_SERVER_GET_FLAG(serverData, ARMSERVERFLAG_MOVETOMENU)

						CANCEL_TIMER(crowdTimer)
						CANCEL_TIMER(tutorialTimer)
						

						CANCEL_TIMER(cameraset.camTimer)
						CANCEL_TIMER(cameraset.shakeTimer)
						
						bArmIdleCamSet = FALSE
						bRematch = TRUE
						
						
						ARM_SET_UI_FLAG(ui, ARMUIFLAG_WHOOSH_PLAYED, FALSE)
						ARM_SET_UI_FLAG(ui, ARMUIFLAG_SHOW_WAIT_REMATCH, FALSE)
						ARM_SET_UI_FLAG(ui, ARMUIFLAG_SHOW_CONTROLS, FALSE)
						ARM_SET_UI_FLAG(ui, ARMUIFLAG_SETUP_QUITUI, FALSE)
						ARM_SET_UI_FLAG(ui, ARMUIFLAG_SHOW_QUITUI, FALSE)
						ARM_SET_CLIENT_FLAG(playerData[iMyParticipantID], ARMCLIENTFLAG_WIN_LOSS_REACT, FALSE)
						ARM_SET_CLIENT_FLAG(playerData[iMyParticipantID], ARMCLIENTFLAG_POST_ANIM_DONE, FALSE)
						
						eSpectateMPState = ARMMPSTATE_RUNNING
					ELSE
						
						IF  IS_TASK_MOVE_NETWORK_ACTIVE(wrestlers[ARMWRESTLERID_HOME].otherWrestler)
						AND IS_TASK_MOVE_NETWORK_ACTIVE(wrestlers[ARMWRESTLERID_AWAY].otherWrestler)
							IF GET_TASK_MOVE_NETWORK_EVENT(wrestlers[ARMWRESTLERID_HOME].otherWrestler, "WinFinished")
							OR GET_TASK_MOVE_NETWORK_EVENT(wrestlers[ARMWRESTLERID_HOME].otherWrestler, "LossFinished")
							OR GET_TASK_MOVE_NETWORK_EVENT(wrestlers[ARMWRESTLERID_AWAY].otherWrestler, "WinFinished")
							OR GET_TASK_MOVE_NETWORK_EVENT(wrestlers[ARMWRESTLERID_AWAY].otherWrestler, "LossFinished")
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: win or loss event just fired off ")
								IF NOT ARM_GET_CLIENT_FLAG(playerData[iMyParticipantID], ARMCLIENTFLAG_POST_ANIM_DONE)
									ARM_SET_CLIENT_FLAG(playerData[iMyParticipantID], ARMCLIENTFLAG_POST_ANIM_DONE, TRUE)
								ENDIF
							ELSE
								IF (GET_GAME_TIMER() % 1000) < 50
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: win or loss event has not fired off in _wait_post_game")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE ARMMPSTATE_TERMINATE_SPLASH
					ARM_SET_SPEC_MP_STATE(eSpectateMPState, ARMMPSTATE_END_LEADERBOARD)
				BREAK
				
				CASE ARMMPSTATE_END_LEADERBOARD
					SWITCH leaveState
						CASE ARM_MPLEAVESTATE_LEADERBOARD	
							//SET_RESULT_SCREEN_DISPLAYING_STATE(FALSE)
							ARM_SCRIPT_CLEANUP_APARTMENT(animatedCam, cameraSet, wrestlers, iCleanupFlags, playerData, sParticipantInfo, ui, ARM_GET_ARG_TABLE_INDEX(args), TRUE)
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
			//############################################################################
			//############################## SPECTATOR END ###############################
			//############################################################################
			
			
			
		ELSE
		
		
		
		// #NORMAL GAMEPLAY HERE VVVVVVVVVVVVVVVVVVVVV
			
			IF sParticipantInfo.iPlayers[eSelfID] = -1
				sParticipantInfo.iPlayers[eSelfID] = iMyParticipantID
			ENDIF
			
			//Uncomment for array overrun debug output
	//		CDEBUG1LN(DEBUG_ARM_WRESTLING, "numPlayers = ", ARM_GET_NUM_WRESTLERS( sParticipantInfo ))
	//		CDEBUG1LN(DEBUG_ARM_WRESTLING, "eSelfID = ", eSelfID)
	//		CDEBUG1LN(DEBUG_ARM_WRESTLING, "iPlayers[eSelfID] = ", sParticipantInfo.iPlayers[eSelfID])
	//		CDEBUG1LN(DEBUG_ARM_WRESTLING, "iMyParticipantID = ", iMyParticipantID)
			
			// If we have a match end event, bail.
			IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE() OR IS_PED_INJURED(PLAYER_PED_ID()) OR IS_PED_IN_COMBAT(PLAYER_PED_ID()) #IF IS_DEBUG_BUILD OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F) #ENDIF
				
				IF ARM_GET_SERVER_GAME_STATE(serverData) < ARMGAMESTATE_WINNER
				AND ARM_GET_PLAYER_MP_STATE(playerData[sParticipantInfo.iPlayers[eSelfID]]) < ARMMPSTATE_WAIT_POST_GAME
				AND (ARM_GET_SERVER_WINS(serverData, eSelfID) = 0)
				AND (ARM_GET_SERVER_WINS(serverData, eOpponentID) = 0)
					CDEBUG1LN(DEBUG_ARM_WRESTLING, "Also giving money back to the player in line 494")
					//BROADCAST_BETTING_MISSION_FINISHED(PLAYER_ID())
				ENDIF
				
				IF RECEIVED_EVENT(EVENT_NETWORK_END_MATCH)
					bMinigamePlayerQuitFlag = TRUE
				ENDIF
				
			//	RELEASE_SCRIPT_AUDIO_BANKS(iWinSound)
				ARM_SCRIPT_CLEANUP_APARTMENT(animatedCam, cameraSet, wrestlers, iCleanupFlags, playerData, sParticipantInfo, ui, ARM_GET_ARG_TABLE_INDEX(args), TRUE)
				CDEBUG1LN(DEBUG_ARM_WRESTLING, "THREAD SHOULD TERMINATE!")
			ENDIF
			
			// Check here for missing player.
			IF ARM_GET_PLAYER_MP_STATE(playerData[iMyParticipantID]) < ARMMPSTATE_TERMINATE_SPLASH
			AND (GET_CURRENT_GAMEMODE() = GAMEMODE_FM AND ARM_GET_NUM_WRESTLERS( sParticipantInfo ) < ENUM_TO_INT(ARMWRESTLERIDS))
				
				CDEBUG1LN(DEBUG_ARM_WRESTLING, "made it into the missing player condition")
				
				CLEAR_HELP()
				STOP_STREAM()
				
				IF ARM_GET_SERVER_GAME_STATE(serverData) < ARMGAMESTATE_WINNER
				AND ARM_GET_PLAYER_MP_STATE(playerData[iMyParticipantID]) < ARMMPSTATE_WAIT_POST_GAME
				AND (ARM_GET_SERVER_WINS(serverData, eSelfID) = 0)
				AND (ARM_GET_SERVER_WINS(serverData, eOpponentID) = 0)
					CDEBUG1LN(DEBUG_ARM_WRESTLING, "Also giving money back to the player in line 508")
					//BROADCAST_BETTING_MISSION_FINISHED(PLAYER_ID())
				ENDIF
				
				CLEANUP_BIG_MESSAGE()
				
				CDEBUG1LN(DEBUG_ARM_WRESTLING, "ARM_GET_NUM_WRESTLERS( sParticipantInfo ) < 2 returned true, terminating")
				
				IF ARM_GET_SERVER_GAME_STATE(serverData) = ARMGAMESTATE_WINNER
				OR ARM_GET_PLAYER_MP_STATE(playerData[iMyParticipantID]) = ARMMPSTATE_WAIT_POST_GAME
					CDEBUG1LN(DEBUG_ARM_WRESTLING, "game had already ended, setting terminate reason to ENDGAME")
					terminateReason = ARMMPTERMINATEREASON_ENDGAME
				ELSE
					CDEBUG1LN(DEBUG_ARM_WRESTLING, "game was still in play, setting terminate reason to PLAYERLEFT")
					ARM_SET_CLIENT_FLAG(playerData[iMyParticipantID], ARMCLIENTFLAG_QUIT_DONE, TRUE)
					terminateReason = ARMMPTERMINATEREASON_PLAYERLEFT
					ARM_SET_CLIENT_FLAG(playerData[iMyParticipantID], ARMCLIENTFLAG_HIDE_OPPONENT, TRUE)
				ENDIF
				
				ARM_SET_ACTIVE_CAMERA(cameraSet, ARMCAMERAID_FRONT_FACE_END, TRUE)
				ARM_SET_SERVER_MP_STATE(serverData, ARMMPSTATE_TERMINATE_SPLASH)
				ARM_SET_PLAYER_MP_STATE(playerData[iMyParticipantID], ARMMPSTATE_TERMINATE_SPLASH)
				
			ENDIF
			
			// check for player quit
			IF ARM_GET_PLAYER_MP_STATE(playerData[iMyParticipantID]) < ARMMPSTATE_TERMINATE_SPLASH
			AND sParticipantInfo.iPlayers[eOpponentID] != -1
				IF ARM_GET_PLAYER_MP_STATE(playerData[sParticipantInfo.iPlayers[eOpponentID]]) >= ARMMPSTATE_TERMINATE_SPLASH
					CLEAR_HELP()
					STOP_STREAM()
					
					CDEBUG1LN(DEBUG_ARM_WRESTLING, "made it into the player quit condition")
					
					IF ARM_GET_SERVER_GAME_STATE(serverData) < ARMGAMESTATE_WINNER
					AND ARM_GET_PLAYER_MP_STATE(playerData[iMyParticipantID]) < ARMMPSTATE_WAIT_POST_GAME
					AND (ARM_GET_SERVER_WINS(serverData, eSelfID) = 0)
					AND (ARM_GET_SERVER_WINS(serverData, eOpponentID) = 0)
						CDEBUG1LN(DEBUG_ARM_WRESTLING, "Also giving money back to the player in line 508")
					//	BROADCAST_BETTING_MISSION_FINISHED(PLAYER_ID())
					ENDIF
					
					CLEANUP_BIG_MESSAGE()
					
					CDEBUG1LN(DEBUG_ARM_WRESTLING, "other player is already in terminate splashe, time to cleanup")
					
					IF ARM_GET_SERVER_GAME_STATE(serverData) = ARMGAMESTATE_WINNER
					OR ARM_GET_PLAYER_MP_STATE(playerData[iMyParticipantID]) = ARMMPSTATE_WAIT_POST_GAME
						CDEBUG1LN(DEBUG_ARM_WRESTLING, "game had already ended, setting terminate reason to ENDGAME")
						//EVENT_SERVER_SEND_GAME_STATE(SCRIPT_EVENT_MG_ARM_GAME_END)
						terminateReason = ARMMPTERMINATEREASON_ENDGAME
					ELSE
						CDEBUG1LN(DEBUG_ARM_WRESTLING, "game was still in play, setting terminate reason to PLAYERLEFT")
						ARM_SET_CLIENT_FLAG(playerData[iMyParticipantID], ARMCLIENTFLAG_QUIT_DONE, TRUE)
						terminateReason = ARMMPTERMINATEREASON_PLAYERLEFT
						ARM_SET_CLIENT_FLAG(playerData[iMyParticipantID], ARMCLIENTFLAG_HIDE_OPPONENT, TRUE)
					ENDIF
					
					ARM_SET_ACTIVE_CAMERA(cameraSet, ARMCAMERAID_FRONT_FACE_END, TRUE)
					ARM_SET_SERVER_MP_STATE(serverData, ARMMPSTATE_TERMINATE_SPLASH)
					ARM_SET_PLAYER_MP_STATE(playerData[iMyParticipantID], ARMMPSTATE_TERMINATE_SPLASH)
				ENDIF
			ENDIF
				
			IF (GET_GAME_TIMER() % 5000) < 50
				CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling: ARM_GET_NUM_WRESTLERS( sParticipantInfo )  = ", ARM_GET_NUM_WRESTLERS( sParticipantInfo ))
			ENDIF
			
			// Update the volume tool.
//			#IF iUSE_Z_VOLUMES
//				#IF IS_DEBUG_BUILD
//					UPDATE_ZVOLUME_WIDGETS()
//				#ENDIF
//			#ENDIF
			
			// Player needs to detect flags thrown by the server.
			ARM_PLAYER_PROCESS_SERVER_FLAGS(serverData, playerData[sParticipantInfo.iPlayers[eSelfID]])
			
			IF ARM_GET_PLAYER_MP_STATE(playerData[sParticipantInfo.iPlayers[eSelfID]])  > ARMMPSTATE_SETUP
			//AND DOES_CAM_EXIST(animatedCam)
			//AND ARM_GET_PLAYER_MP_STATE(playerData[sParticipantInfo.iPlayers[eSelfID]]) < ARMMPSTATE_TERMINATE_SPLASH
				IF NOT IS_PED_INJURED(wrestlers[eSelfID].otherWrestler)
					#IF IS_DEBUG_BUILD
					IF (GET_GAME_TIMER() % 3000) < 50
						CDEBUG1LN(DEBUG_ARM_WRESTLING, "************************************")
						CDEBUG1LN(DEBUG_ARM_WRESTLING, "eSelfID: ", eSelfID)
						vTemp = GET_ENTITY_COORDS(PLAYER_PED_ID())
						CDEBUG1LN(DEBUG_ARM_WRESTLING, "PLAYER_PED_ID coords: ", vTemp)
						vTemp = GET_ENTITY_COORDS(wrestlers[eSelfID].otherWrestler)
						CDEBUG1LN(DEBUG_ARM_WRESTLING, "other wrestler coords: ", vTemp)
						vTemp = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(PLAYER_PED_ID(), GET_ENTITY_COORDS(wrestlers[eSelfID].otherWrestler))
						CDEBUG1LN(DEBUG_ARM_WRESTLING, "my offset from their coords: ", vTemp)
						vTemp = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(wrestlers[eSelfID].otherWrestler, GET_ENTITY_COORDS(PLAYER_PED_ID()))
						CDEBUG1LN(DEBUG_ARM_WRESTLING, "their offset from my coords: ", vTemp)
						CDEBUG1LN(DEBUG_ARM_WRESTLING, "vArmWrestleLocation: ", vArmWrestleLocation)
						CDEBUG1LN(DEBUG_ARM_WRESTLING, "fTableRot: ", fTableRot)
						CDEBUG1LN(DEBUG_ARM_WRESTLING, "************************************")
					ENDIF
					#ENDIF
					
					
					// making sure both players are active
					IF ARM_GET_NUM_WRESTLERS( sParticipantInfo ) >= ENUM_TO_INT(ARMWRESTLERIDS)
						IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(sParticipantInfo.iPlayers[eOpponentID]))
							//IF NETWORK_IS_PLAYER_ACTIVE(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(eOpponentID)))
								FLOAT fRot
								IF !bIsTableSideA
									fRot = WRAP(fTableRot + fOVERRIDE_heading, 0.0, 360.0)
								ELSE
									fRot = fTableRot
								ENDIF
								IF (GET_GAME_TIMER() % 3000) < 50
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "Fixup Position Of: ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(sParticipantInfo.iPlayers[eOpponentID]))), " OP ID", ENUM_TO_INT(eOpponentID))
									VECTOR v1, v2
									v1 = GET_ENTITY_COORDS(PLAYER_PED_ID())
									v2 = GET_ENTITY_COORDS(GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(sParticipantInfo.iPlayers[eOpponentID]))))
									CDEBUG1LN(DEBUG_ARM_WRESTLING, 
									"Fixup Position SELF: ", v1,
									" OP", v2)
								
								ENDIF
								NETWORK_OVERRIDE_COORDS_AND_HEADING(wrestlers[eSelfID].otherWrestler, GET_PLAYER_POS(vArmWrestleLocation, fTableRot, !bIsTableSideA), fRot)
						ELSE
							IF (GET_GAME_TIMER() % 3000) < 50
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "Cant fixup player pos player invalid part id", sParticipantInfo.iPlayers[eOpponentID])
							ENDIF
						ENDIF
						//ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		
			
			// Player acts according to local game state.
			SWITCH ARM_GET_PLAYER_MP_STATE(playerData[sParticipantInfo.iPlayers[eSelfID]])
				// Init streaming, set up cameras, etc.
				CASE ARMMPSTATE_INIT
					IF LOAD_ASSETS()
						// Wait until the server moves beyond the init state.
						IF ARM_GET_SERVER_MP_STATE(serverData) > ARMMPSTATE_INIT
							ARM_SET_ARG_TABLE_INDEX(args, ARM_GET_SERVER_TABLE_INDEX(serverData))
							ARM_GET_TABLE_DATA(table.vCenter, table.fRotation, ARM_GET_ARG_TABLE_INDEX(args), TRUE)
							//ARM_GET_CROWD_MODEL_DATA(args.crowdModels, GET_PLAYER_TEAM(PLAYER_ID()))
							
							DISABLE_ALL_MP_HUD()
							DISABLE_SELECTOR()
							
							ARM_INIT_GAME_APARTMENTS(args, table, assets, ui, ARM_GET_ARG_TABLE_INDEX(args), FALSE)
							#IF IS_DEBUG_BUILD
								ARM_SETUP_DEBUG()
							#ENDIF
							
							// Walk to the table location.
							DISABLE_CELLPHONE(TRUE)
							IF NOT IS_PLAYER_DEAD(PLAYER_ID())
								SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
							ENDIF

							IF GET_CURRENT_GAMEMODE() = GAMEMODE_FM
								ARM_GET_TABLE_DATA(vArmWrestleLocation, fTableRot, ARM_GET_ARG_TABLE_INDEX(args), TRUE)
							ENDIF

							//fMyPreferenceLevel = GET_ARM_WRESTLING_POS_PREFERENCE(GET_ENTITY_COORDS(PLAYER_PED_ID()), vArmWrestleLocation, ARM_GET_TABLE_WRESTLER_POS(table, TRUE, ARM_IS_PLAYER_LARGE()))
							
							//BROADCAST_ARM_WRESTLE_POSITIONS(fMyPreferenceLevel, PLAYER_ID(), NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(sParticipantInfo.iPlayers[eOpponentID])))
							
							CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: INITIALIZE CAMERAS")
							ARM_INIT_CAMERAS_APARTMENT(cameraSet, table, wrestlers[eSelfID].vPlayerPosition, ARM_GET_ARG_TABLE_INDEX(args))
							
							//ARM_INIT_CAMERAS_APARTMENT
							IF HAS_ADDITIONAL_TEXT_LOADED(PROPERTY_TEXT_SLOT)
							//AND GET_GAME_TIMER() % 1000 > 50
								ARM_SET_PLAYER_MP_STATE(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMMPSTATE_TRAVEL)
							ELSE
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: ARM_GET_PLAYER_MP_STATE: FAILED TO LOAD PROPERTY_TEXT_SLOT!!")
								PRINTNL()
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE ARMMPSTATE_TRAVEL
					ARM_SET_PLAYER_MP_STATE(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMMPSTATE_SETUP)
				BREAK
				
				// Put props/wrestlers/cameras into place.
				CASE ARMMPSTATE_SETUP
				//IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) = FINISHED_TASK
					IF ARM_IS_STREAMING_COMPLETE_APARTMENT(assets, ui, FALSE)
						IF CAN_REGISTER_MISSION_OBJECTS(iNETWORK_OBJECTS)
							IF NOT IS_TIMER_STARTED(streamTimer)
								START_TIMER_NOW(streamTimer)
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "Timer started.")
							ENDIF
							
							ARM_SET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMCLIENTFLAG_MATCH_MADE, TRUE)
							
							IF GET_TIMER_IN_SECONDS(streamTimer) > 1.5 
							
								RESTART_TIMER_NOW(streamTimer)
								//WAIT(5000)
								//CDEBUG1LN(DEBUG_ARM_WRESTLING, "Playing crowd stream from: ", table.vCenter)
								//PLAY_STREAM_FROM_POSITION(ARM_GET_TABLE_CENTER(table))
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: WAIT AFTER COLL SET")
								//WAIT(5000)
								//IF bIsTableSideA
								IF eSelfID <> ARMWRESTLERID_HOME
									TASK_MOVE_NETWORK_BY_NAME(PLAYER_PED_ID(), "arm_wrestling_sweep_paired_a_rev4", 0.5, TRUE, "anim@amb@clubhouse@mini@arm_wrestling@", MOVE_USE_KINEMATIC_PHYSICS)
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: MoVE network arm__wrestling_sweep_paired_a_rev4 setup for self")
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "MOVE NET A")
								ELSE
									TASK_MOVE_NETWORK_BY_NAME(PLAYER_PED_ID(), "arm_wrestling_sweep_paired_b_rev4", 0.5, TRUE, "anim@amb@clubhouse@mini@arm_wrestling@", MOVE_USE_KINEMATIC_PHYSICS)
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: MoVE network arm__wrestling_sweep_paired_b_rev4 setup for self")
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "MOVE NET B")
								ENDIF
								//WAIT(5000)
								
								INCREMENT_MP_INT_PLAYER_STAT(MPPLY_ARMWRESTLING_TOTAL_MATCH)
								SET_BIT_FOR_FRANKIE_SAYS_ACHIEVEMENT(ACH45_ARMWRESTLING)
								iArmStats[ARM_STAT_NUM_MATCHES]++
								
								ARM_SET_PLAYER_MP_STATE(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMMPSTATE_SETUP_POST)
							ELSE
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "Waiting timer.")
							ENDIF
						ELSE
							CDEBUG1LN(DEBUG_ARM_WRESTLING, "Can not register network objects.")
						ENDIF
					ELSE
						CDEBUG1LN(DEBUG_ARM_WRESTLING, "Streaming is not completed.")
					ENDIF
				//ENDIF
				BREAK
				
				// Wait here for server.
				CASE ARMMPSTATE_SETUP_POST
					
					SET_ON_JOB_INTRO(TRUE)

					CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: MoVE NOT SYNC COMPLETE")
					ARM_SET_UI_FLAG(ui, ARMUIFLAG_SETUP_WAIT_READY, FALSE)
											
					CANCEL_TIMER(streamTimer)
						
					IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
					AND IS_ENTITY_ALIVE(wrestlers[eSelfID].otherWrestler)
						IF IS_TASK_MOVE_NETWORK_ACTIVE(PLAYER_PED_ID())
						AND IS_TASK_MOVE_NETWORK_ACTIVE(wrestlers[eSelfID].otherWrestler)
							ARM_CLEAR_BUCKETS(playerData[sParticipantInfo.iPlayers[eSelfID]])
							ARM_SET_PLAYER_MP_STATE(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMMPSTATE_INTRO_SCENE_ALT)
						ELSE
							IF (GET_GAME_TIMER() % 1000) < 50
								IF NOT IS_TASK_MOVE_NETWORK_ACTIVE(PLAYER_PED_ID())
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: Move network for self is not active yet", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX_FROM_PED(PLAYER_PED_ID())))
								ENDIF
								
								IF NOT IS_TASK_MOVE_NETWORK_ACTIVE(wrestlers[eSelfID].otherWrestler)
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: Move network for the other player is not active yet", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX_FROM_PED(wrestlers[eSelfID].otherWrestler)))
								ENDIF
							ENDIF
						ENDIF
					ELSE
						CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: NOT ALIVE")
					ENDIF
				BREAK
				
				CASE ARMMPSTATE_INTRO_SCENE_ALT
					
					fCamRot = ARM_GET_INTRO_CAMERA_DATA(ARM_GET_ARG_TABLE_INDEX(args), table.fRotation)
					sWhichWalk = "StandIdle"
					sWhichCam = "aw_ig_intro_alt1_cam"
					
					PED_INDEX pedPlayer1 
					PLAYER_INDEX piPlayer1
					piPlayer1 = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX( sParticipantInfo.iPlayers[eSelfID]))
					pedPlayer1 =  GET_PLAYER_PED(piPlayer1)
				
					PED_INDEX pedPlayer2 
					PLAYER_INDEX piPlayer2
					piPlayer2 = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX( sParticipantInfo.iPlayers[eOpponentID]))
					pedPlayer2 =  GET_PLAYER_PED(piPlayer2)
					
					IF NOT IS_PED_INJURED(pedPlayer1)
					AND IS_TASK_MOVE_NETWORK_ACTIVE(pedPlayer1)
					AND IS_TASK_MOVE_NETWORK_ACTIVE(pedPlayer2)
						
						IF NOT ARE_STRINGS_EQUAL(GET_TASK_MOVE_NETWORK_STATE(pedPlayer1), sWhichWalk)
							IF IS_TASK_MOVE_NETWORK_READY_FOR_TRANSITION(pedPlayer1)
								IF REQUEST_TASK_MOVE_NETWORK_STATE_TRANSITION(pedPlayer1, sWhichWalk)
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: requested for otherWrestler: ", sWhichWalk)
								ELSE
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: request was denied for otherWrestler: ", sWhichWalk)
								ENDIF
							ELSE
								IF (GET_GAME_TIMER() % 1000) < 50
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: otherWrestler not ready for transition in beginning: ", sWhichWalk)
								ENDIF
							ENDIF
						ENDIF
						
						IF NOT ARE_STRINGS_EQUAL(GET_TASK_MOVE_NETWORK_STATE(pedPlayer2), sWhichWalk)
							IF IS_TASK_MOVE_NETWORK_READY_FOR_TRANSITION(pedPlayer2)
								IF REQUEST_TASK_MOVE_NETWORK_STATE_TRANSITION(pedPlayer2, sWhichWalk)
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: requested for PLAYER_PED_ID: ", sWhichWalk)
								ELSE
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: request was denied for PLAYER_PED_ID: ", sWhichWalk)
								ENDIF
							ELSE
								IF (GET_GAME_TIMER() % 1000) < 50
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: PLAYER_PED_ID not ready for transition in beginning: ", sWhichWalk)
								ENDIF
							ENDIF
						ENDIF
						
						
						IF (ARE_STRINGS_EQUAL(GET_TASK_MOVE_NETWORK_STATE(pedPlayer1), sWhichWalk)
						AND ARE_STRINGS_EQUAL(GET_TASK_MOVE_NETWORK_STATE(pedPlayer2), sWhichWalk))
							
							CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: player move state: ", GET_TASK_MOVE_NETWORK_STATE(PLAYER_PED_ID()))
							CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: other  move state: ", GET_TASK_MOVE_NETWORK_STATE(wrestlers[eSelfID].otherWrestler))
							
							IF NOT ARM_GET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMCLIENTFLAG_ATTACHED)
								ARM_SETUP_WRESTLERS_APARTMENTS(wrestlers, table, wrestlers[eSelfID].vPlayerPosition, bIsTableSideA)
								ARM_SET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMCLIENTFLAG_ATTACHED, TRUE)
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: PARENTED OWNER")
							ENDIF
													
							//### Create/activate intro camera
							IF NOT DOES_CAM_EXIST(animatedCam)
								iLocalSCene2 = CREATE_SYNCHRONIZED_SCENE(table.vCenter, << 0, 0, fCamRot + 180.0 >>)
								animatedCam = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", FALSE)
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "ARM_WRESTLE CAM_CREATE - PRE GAME CAM")
								
								IF DOES_CAM_EXIST(animatedCam)
									PLAY_SYNCHRONIZED_CAM_ANIM(animatedCam, iLocalSCene2, sWhichCam, "mini@arm_wrestling")
									SET_CAM_ACTIVE(animatedCam, TRUE)
									RENDER_SCRIPT_CAMS(TRUE, FALSE)
									ARM_SET_CLIENT_FLAG(playerData[PARTICIPANT_ID_TO_INT()], ARMCLIENTFLAG_HIDE_SPECTATORS, TRUE)
								ELSE
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: CAM DID NOT CREATE")
								ENDIF
							ENDIF
							
							CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: local sync scene is ", iLocalSCene2)
							
							RESTART_TIMER_NOW(introTimer)
							
							CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: setting FM Match Start as the intro starts")
							DEAL_WITH_FM_MATCH_START(FMMC_TYPE_MG_ARM_WRESTLING, serverData.iMatchHistoryID, serverData.iMatchType)
							
							ARM_SET_PLAYER_MP_STATE(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMMPSTATE_INTRO_SCENE_ALT_END)
							
						ELSE
							IF (GET_GAME_TIMER() % 1000) < 50
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: wrestlers are not at Walk yet")
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: player move state: ", GET_TASK_MOVE_NETWORK_STATE(PLAYER_PED_ID()))
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: other  move state: ", GET_TASK_MOVE_NETWORK_STATE(wrestlers[eSelfID].otherWrestler))
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE ARMMPSTATE_INTRO_SCENE_ALT_END
					IF DOES_CAM_EXIST(animatedCam)
						IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalSCene2)
							IF GET_SYNCHRONIZED_SCENE_PHASE(iLocalSCene2) = 1.0
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: iLocalSCene2 is ", iLocalSCene2, " server staet is", serverData.mpState)
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: iLocalSCene2 phase is ", GET_SYNCHRONIZED_SCENE_PHASE(iLocalSCene2))
								
								
								ARM_CLEAR_BUCKETS(playerData[sParticipantInfo.iPlayers[eSelfID]])
								ARM_SET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMCLIENTFLAG_SYNC_COMPLETE, TRUE)
								
							ELSE
								IF GET_SYNCHRONIZED_SCENE_PHASE(iLocalSCene2) > 0.05
								AND NOT bHelp1
									
									IF NOT g_bMGIntroSwitch
										PRINT_HELP("ARMMP_HOW_TO_12")
										CDEBUG1LN(DEBUG_ARM_WRESTLING, "PLAYING WELCOME First Time")
									ELSE
										PRINT_HELP("ARMMP_REGAME2")
										CDEBUG1LN(DEBUG_ARM_WRESTLING, "PLAYING WELCOME Played Before")
									ENDIF
									g_bMGIntroSwitch = TRUE
									bHelp1 = TRUE
								ENDIF
								
								IF (GET_GAME_TIMER() % 1000) < 50
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: iLocalSCene2 is ", iLocalSCene2)
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: iLocalSCene2 phase is ", GET_SYNCHRONIZED_SCENE_PHASE(iLocalSCene2))
								ENDIF
							ENDIF
						ELSE
							IF (GET_GAME_TIMER() % 1000) < 50
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "client: sync scene not running")
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "client: iLocalSCene2 = ", iLocalSCene2)
							ENDIF
							IF NOT IS_TIMER_STARTED(syncSceneFailsafeTimer)
								START_TIMER_NOW(syncSceneFailsafeTimer)
							//ELIF ((NOT MPGlobals.WrestleData.bTutorialShown) AND GET_TIMER_IN_SECONDS(syncSceneFailsafeTimer) > 13.0)
							ELIF (MPGlobals.WrestleData.bTutorialShown OR GET_TIMER_IN_SECONDS(syncSceneFailsafeTimer) > 6.0)
								
								ARM_CLEAR_BUCKETS(playerData[sParticipantInfo.iPlayers[eSelfID]])
								ARM_SET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMCLIENTFLAG_SYNC_COMPLETE, TRUE)
							ENDIF
						ENDIF
					ELSE
						
						IF DOES_CAM_EXIST(animatedCam)
							CDEBUG1LN(DEBUG_ARM_WRESTLING, "Cam activated after")
							PLAY_SYNCHRONIZED_CAM_ANIM(animatedCam, iLocalSCene2, sWhichCam, "mini@arm_wrestling")
							SET_CAM_ACTIVE(animatedCam, TRUE)
							RENDER_SCRIPT_CAMS(TRUE, FALSE)
							ARM_SET_CLIENT_FLAG(playerData[PARTICIPANT_ID_TO_INT()], ARMCLIENTFLAG_HIDE_SPECTATORS, TRUE)
						ENDIF		
						
						IF NOT IS_TIMER_STARTED(syncSceneFailsafeTimer)
							START_TIMER_NOW(syncSceneFailsafeTimer)
							CDEBUG1LN(DEBUG_ARM_WRESTLING, "No intro cam")	
						ELIF GET_TIMER_IN_SECONDS(syncSceneFailsafeTimer) > 6.0
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "manual switch without camera")
								ARM_CLEAR_BUCKETS(playerData[sParticipantInfo.iPlayers[eSelfID]])
								ARM_SET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMCLIENTFLAG_SYNC_COMPLETE, TRUE)
						ENDIF
						IF GET_GAME_TIMER() % 5000 > 100
							CDEBUG1LN(DEBUG_ARM_WRESTLING, "No intro cam")
						ENDIF
					ENDIF
					
					IF ARM_GET_SERVER_MP_STATE(serverData) >= ARMMPSTATE_RUNNING
						
//						IF eSelfID = ARMWRESTLERID_HOME
//							TASK_PLAY_ANIM(PLAYER_PED_ID(), "mini@arm_wrestling", "nuetral_idle_a_face", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_SECONDARY )
//						ELSE
//							TASK_PLAY_ANIM(PLAYER_PED_ID(), "mini@arm_wrestling", "nuetral_idle_b_face", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_SECONDARY )
//						ENDIF
						TASK_PLAY_ANIM(PLAYER_PED_ID(), "mini@arm_wrestling", GET_FACE_ANIM(ePFA_NEUTRAL_IDLE, eSelfID = ARMWRESTLERID_HOME), SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_SECONDARY )
						
	//					IF g_bMGIntroSwitch //MPGlobals.WrestleData.bTutorialShown
	//						g_bMGIntroSwitch = FALSE //MPGlobals.WrestleData.bTutorialShown = FALSE
	//						CDEBUG1LN(DEBUG_ARM_WRESTLING, "client: tutorial flag set to false")
	//					ELSE
	//						g_bMGIntroSwitch = TRUE //MPGlobals.WrestleData.bTutorialShown = TRUE
	//						CDEBUG1LN(DEBUG_ARM_WRESTLING, "client: tutorial flag set to true")
	//					ENDIF
						
						SET_ON_JOB_INTRO(FALSE)
						
						ARM_CLEAR_BUCKETS(playerData[sParticipantInfo.iPlayers[eSelfID]])
						ARM_SET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMCLIENTFLAG_SYNC_COMPLETE, FALSE)
						ARM_SET_PLAYER_MP_STATE(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMMPSTATE_RUNNING)
					ENDIF
					
				BREAK
				
				// Wait for the other player to ready up.
				CASE ARMMPSTATE_WAIT_TO_START_GAME
					IF ARM_SERVER_GET_FLAG(serverData, ARMSERVERFLAG_MOVETOGAME)
						
						IF GET_TIMER_IN_SECONDS_SAFE( rematchCamDelay ) > 1.0
							IF NOT ARM_IS_CAMERA_ACTIVE(cameraSet, ARMCAMERAID_WRESTLE)
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: anims have been requested, switch cams")
								ARM_SET_ACTIVE_CAMERA(cameraSet, ARMCAMERAID_WRESTLE, TRUE)//, TRUE)
								CANCEL_TIMER( rematchCamDelay )
							ENDIF
						ENDIF
						
						IF NOT IS_TIMER_STARTED(introTimer)
							RESTART_TIMER_NOW(introTimer)
						ELIF GET_TIMER_IN_SECONDS(introTimer) > 4.0
						
							// Request the win/loss audio if needed.
//							iNewWinSound = GET_RANDOM_INT_IN_RANGE(1, 4)
//							IF iWinSound <> iNewWinSound
//								IF iWinSound <> 0
//									RELEASE_NAMED_SCRIPT_AUDIO_BANK(ARM_GET_CROWD_SOUND_BANK_FROM_INDEX(iWinSound, TRUE))
//									RELEASE_NAMED_SCRIPT_AUDIO_BANK(ARM_GET_CROWD_SOUND_BANK_FROM_INDEX(iWinSound, FALSE))
//								ENDIF
//								iWinSound = iNewWinSound
//								REQUEST_SCRIPT_AUDIO_BANK(ARM_GET_CROWD_SOUND_BANK_FROM_INDEX(iWinSound, TRUE))
//								REQUEST_SCRIPT_AUDIO_BANK(ARM_GET_CROWD_SOUND_BANK_FROM_INDEX(iWinSound, FALSE))
//							ENDIF
							
							ARM_SET_LAST_METER_VALUE(ui, 0.5)
							RESTART_TIMER_NOW(wrestleTimer)
							CLEAR_HELP()
							
							RENDER_SCRIPT_CAMS(TRUE, FALSE)
							
							RESTART_TIMER_NOW(countdownTimer)
							IF ARM_SERVER_GET_FLAG(serverData, ARMSERVERFLAG_COUNTDOWN_DONE)
								ARM_SERVER_SET_FLAG(serverData, ARMSERVERFLAG_COUNTDOWN_DONE, FALSE)
							ENDIF
							
							ARM_CLEAR_BUCKETS(playerData[sParticipantInfo.iPlayers[eSelfID]])
							CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: Buckets cleared in wait to start game")
							
							ARM_RESET_LOCAL_BUCKET_SCORE(wrestlers[eSelfID])
							CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: local Buckets cleared in wait post game")
							
							CANCEL_TIMER(introTimer)
							
							ARM_SET_UI_FLAG(ui, ARMUIFLAG_SHOW_TUTORIAL, TRUE)
							ARM_SET_UI_FLAG(ui, ARMUIFLAG_SHOW_WAIT_READY, FALSE)
							ARM_SET_PLAYER_MP_STATE(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMMPSTATE_RUNNING)
						ENDIF
					ELSE					
						ARM_UPDATE_WAIT_READY_UI(ui)					
					ENDIF
				BREAK
				
				// Game is active.
				CASE ARMMPSTATE_RUNNING

					
					#IF IS_DEBUG_BUILD
						ARM_UPDATE_DEBUG(serverData, table, cameraSet, ENUM_TO_INT(ARM_GET_PLAYER_MP_STATE(playerData[sParticipantInfo.iPlayers[eSelfID]])), ENUM_TO_INT(ARM_GET_SERVER_MP_STATE(serverData)), ENUM_TO_INT(ARM_GET_SERVER_GAME_STATE(serverData)), ARM_GET_WRESTLER_ATTACHMENT_OBJECT(wrestlers[eSelfID]), ARM_GET_REFEREE_ATTACHMENT_OBJECT(crowd), crowd.peds)
					#ENDIF
					
					// Act based on the server state.
					SWITCH ARM_GET_SERVER_GAME_STATE(serverData)
						// Waiting for at least one player to ready up.
						CASE ARMGAMESTATE_READYMENU
							
							// See if the player has readied up.
							ARM_GET_LOCAL_INPUT(wrestlers[eSelfID].input)						
							uiResult = ARM_UPDATE_READY_UI(ui, armUseContext, wrestlers[eSelfID].input)
							
							//# Conor's Change
							SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iArmWrestlingBitSet,args.iTableIndex)
							
							// We need to start our approach anim.
							IF NOT bPlayApproachAnim
								IF bRematch
									ARM_SET_CLIENT_FLAG(playerData[PARTICIPANT_ID_TO_INT()], ARMCLIENTFLAG_REACHED_RUNNING, FALSE) 
									wrestlers[ARMWRESTLERID_AWAY].bReachedRunning = FALSE
									wrestlers[ARMWRESTLERID_HOME].bReachedRunning = FALSE
									ARM_SET_CLIENT_FLAG(playerData[PARTICIPANT_ID_TO_INT()], ARMCLIENTFLAG_RESET_COUNTDOWN_UI, FALSE) 
								// MoVE network script:
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: PLAY APPROACH")
									//check if reached intro manually , network does not show it...
									IF wrestlers[ARMWRESTLERID_AWAY].bReachedIntro
									AND wrestlers[ARMWRESTLERID_HOME].bReachedIntro
										CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: BOTH PLAYERS REACHED INTRO STATE")
										STRING sApproachState
										sApproachState = "StandIdle"
										
										IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
											IF NOT ARM_GET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMCLIENTFLAG_REQUEST_ANIM)
												// Move state change to Approach -----------------------------------------------------------------------
												IF NOT IS_PED_INJURED(wrestlers[eSelfID].otherWrestler)
												AND IS_TASK_MOVE_NETWORK_ACTIVE(PLAYER_PED_ID())
												AND IS_TASK_MOVE_NETWORK_ACTIVE(wrestlers[eSelfID].otherWrestler)
													
													IF NOT ARE_STRINGS_EQUAL(GET_TASK_MOVE_NETWORK_STATE(wrestlers[eSelfID].otherWrestler), sApproachState)
														IF IS_TASK_MOVE_NETWORK_READY_FOR_TRANSITION(wrestlers[eSelfID].otherWrestler)
															IF REQUEST_TASK_MOVE_NETWORK_STATE_TRANSITION(wrestlers[eSelfID].otherWrestler, sApproachState)
																CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: Approach requested for otherWrestler", sApproachState)
															ELSE
																CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: Approach request was denied for otherWrestler", sApproachState)
															ENDIF
														ELSE
															IF (GET_GAME_TIMER() % 1000) < 50
																CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: otherWrestler not ready for Approach transition in beginning", sApproachState)
															ENDIF
														ENDIF
													ENDIF
													
													IF NOT ARE_STRINGS_EQUAL(GET_TASK_MOVE_NETWORK_STATE(PLAYER_PED_ID()), sApproachState)
														IF IS_TASK_MOVE_NETWORK_READY_FOR_TRANSITION(PLAYER_PED_ID())
															IF REQUEST_TASK_MOVE_NETWORK_STATE_TRANSITION(PLAYER_PED_ID(), sApproachState)
																CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: Approach requested for PLAYER_PED_ID", sApproachState)
																
//																IF eSelfID = ARMWRESTLERID_HOME
//																	TASK_PLAY_ANIM(PLAYER_PED_ID(), "mini@arm_wrestling", "idle_to_nuetral_a_face", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_SECONDARY )
//																ELSE
//																	TASK_PLAY_ANIM(PLAYER_PED_ID(), "mini@arm_wrestling", "idle_to_nuetral_b_face", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_SECONDARY )
//																ENDIF
																TASK_PLAY_ANIM(PLAYER_PED_ID(), "mini@arm_wrestling", GET_FACE_ANIM(ePFA_IDLE_TO_NEUTRAL, eSelfID = ARMWRESTLERID_HOME), SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_SECONDARY )
															ELSE
																CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: Approach request was denied for PLAYER_PED_ID", sApproachState)
															ENDIF
														ELSE
															IF (GET_GAME_TIMER() % 1000) < 50
																CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: PLAYER_PED_ID not ready for Approach transition", sApproachState)
															ENDIF
														ENDIF
													ENDIF
													
													CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc:WAITING FOR AN APPROACH")
													//WAIT(20000)
													
													IF (ARE_STRINGS_EQUAL(GET_TASK_MOVE_NETWORK_STATE(PLAYER_PED_ID()), sApproachState)
													AND ARE_STRINGS_EQUAL(GET_TASK_MOVE_NETWORK_STATE(wrestlers[eSelfID].otherWrestler), sApproachState))
														
														EVENT_SERVER_SEND_GAME_STATE(SCRIPT_EVENT_MG_REMATCH)
														
														CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: player move state: ", GET_TASK_MOVE_NETWORK_STATE(PLAYER_PED_ID()))
														CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: other  move state: ", GET_TASK_MOVE_NETWORK_STATE(wrestlers[eSelfID].otherWrestler))
														ARM_SET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMCLIENTFLAG_REQUEST_ANIM, TRUE)
														RESTART_TIMER_NOW( rematchCamDelay )
														
														//INCREMENT_MP_INT_PLAYER_STAT(MPPLY_ARMWRESTLING_TOTAL_MATCH)
														//iArmStats[ARM_STAT_NUM_MATCHES]++
														wrestlers[ARMWRESTLERID_AWAY].bReachedIntro = FALSE
														wrestlers[ARMWRESTLERID_HOME].bReachedIntro = FALSE
														CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: Reset reached INTRO Bools")
														
														bPlayApproachAnim = TRUE
													ELSE
														IF (GET_GAME_TIMER() % 1000) < 50
															CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: wrestlers are not at Approach yet")
															CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: player move state: ", GET_TASK_MOVE_NETWORK_STATE(PLAYER_PED_ID()))
															CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: other  move state: ", GET_TASK_MOVE_NETWORK_STATE(wrestlers[eSelfID].otherWrestler))
														ENDIF
													ENDIF
												ELSE
													IF (GET_GAME_TIMER() % 1000) < 50
														CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: move network is not active for wrestlers")
													ENDIF
												ENDIF
												// End Move state change to Approach -----------------------------------------------------------------------
											ENDIF
										ENDIF
									ELSE
										CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: PLAYERS HAVE NOT REACHED INTRO YET")
									ENDIF
								ELSE
									bPlayApproachAnim = TRUE
									ARM_SET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMCLIENTFLAG_REQUEST_ANIM, TRUE)
								ENDIF
	//							IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
	//								ARM_PLAY_CROWD_ANIMATION(crowd, ARMCROWDMEMBER_REFEREE, sAnimDict, sRefIntroAnim, SLOW_BLEND_IN, SLOW_BLEND_OUT, AF_HOLD_LAST_FRAME | AF_NOT_INTERRUPTABLE, 0.0, TRUE)
	//								ARM_FANS_CHEER(crowd)
	//							ENDIF
								
							// We have started our approach anim.
							ELSE
								
								// Play and set the speed of the idle to neutral anim.
								// MoVE network script:
								IF NOT ARM_GET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMCLIENTFLAG_START_ANIM)
									
									ARM_SET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMCLIENTFLAG_START_ANIM, TRUE)
								ENDIF
								
								// ScriptedAnimation script:
								// Set the speed of the ref intro anim.
								IF NETWORK_IS_HOST_OF_THIS_SCRIPT() AND (NOT IS_ENTITY_DEAD(ARM_GET_CROWD_PED_INDEX(crowd, ARMCROWDMEMBER_REFEREE))) AND IS_ENTITY_PLAYING_ANIM(ARM_GET_CROWD_PED_INDEX(crowd, ARMCROWDMEMBER_REFEREE), sAnimDict, sRefIntroAnim)
									SET_ENTITY_ANIM_SPEED(ARM_GET_CROWD_PED_INDEX(crowd, ARMCROWDMEMBER_REFEREE), sAnimDict, sRefIntroAnim, fREF_INTRO_SPEED)
								ENDIF
								
							ENDIF
							
							// The player has readied up.
							IF uiResult = ARMUIRETVAL_TRUE
							AND ARM_GET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMCLIENTFLAG_START_ANIM)
							AND ARM_GET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMCLIENTFLAG_REQUEST_ANIM)
								bPlayApproachAnim = FALSE
								ARM_SET_PLAYER_MP_STATE(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMMPSTATE_WAIT_TO_START_GAME)
								
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: HIT 4")
								
							// The player is quitting.
							ELIF uiResult = ARMUIRETVAL_FALSE
								
								iArmStats[ARM_STAT_NUM_LOSSES]++
								ARM_SET_PLAYER_MP_STATE(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMMPSTATE_TERMINATE_SPLASH)
							ENDIF
						BREAK
						
						// Players are currently armwrestling.
						CASE ARMGAMESTATE_PLAYING
							
							
							
							// Show the countdown timer.
							IF IS_TIMER_STARTED(countdownTimer)
								
								
								IF NOT ARM_GET_CLIENT_FLAG(playerData[PARTICIPANT_ID_TO_INT()], ARMCLIENTFLAG_REACHED_RUNNING) 
									//figure out the stat, then do this
									IF iControlHelpCounter < 5
									AND NOT bShowTutHelp
										IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
											PRINT_HELP("ARMMP_TUT_2")
										ELSE
											PRINT_HELP("ARMMP_TUT_1")
										ENDIF
										bShowTutHelp = TRUE
									ENDIF
									
									IF ARM_IS_CAMERA_ACTIVE(cameraSet, ARMCAMERAID_IDLE)
									OR (NOT ARM_IS_CAMERA_ACTIVE(cameraSet, ARMCAMERAID_WRESTLE))
										ARM_SET_ACTIVE_CAMERA(cameraSet, ARMCAMERAID_WRESTLE, TRUE)
										IF DOES_CAM_EXIST(animatedCam)
											DESTROY_CAM(animatedCam)
										ENDIF
										//ARM_SET_ACTIVE_CAMERA(cameraSet, ARMCAMERAID_IDLE, TRUE)
										ARM_SET_CLIENT_FLAG(playerData[PARTICIPANT_ID_TO_INT()], ARMCLIENTFLAG_HIDE_SPECTATORS, TRUE)
										RENDER_SCRIPT_CAMS(TRUE, FALSE)
									ENDIF
									
									#IF IS_DEBUG_BUILD
									DRAW_DEBUG_TEXT_2D(GET_TASK_MOVE_NETWORK_STATE(PLAYER_PED_ID()), <<0.5, 0.25,0>>)
									#ENDIF
									
									//IF ARM_IS_CAMERA_ACTIVE(cameraSet, ARMCAMERAID_WRESTLE)
									IF (NOT ARM_IS_CAMERA_ACTIVE(cameraSet, ARMCAMERAID_WRESTLE))
									AND (GET_TIMER_IN_SECONDS(countdownTimer) > 3.0)
										ARM_SET_ACTIVE_CAMERA(cameraSet, ARMCAMERAID_WRESTLE, TRUE, TRUE)
									ENDIF	
									
									// Move state change to Running -----------------------------------------------------------------------
									IF (GET_TIMER_IN_SECONDS(countdownTimer) > 3.75)
									AND DOES_ENTITY_EXIST(wrestlers[eSelfID].otherWrestler)
									AND NOT IS_PED_INJURED(wrestlers[eSelfID].otherWrestler)
									AND IS_TASK_MOVE_NETWORK_ACTIVE(PLAYER_PED_ID())
									AND IS_TASK_MOVE_NETWORK_ACTIVE(wrestlers[eSelfID].otherWrestler)
									AND NOT ARE_STRINGS_EQUAL(GET_TASK_MOVE_NETWORK_STATE(PLAYER_PED_ID()), "Running")
									AND NOT ARE_STRINGS_EQUAL(GET_TASK_MOVE_NETWORK_STATE(wrestlers[eSelfID].otherWrestler), "Running")
										
										//IF wrestlers[ARMWRESTLERID_AWAY].bReachedRunning
									//	AND wrestlers[ARMWRESTLERID_HOME].bReachedRunning
										
											IF IS_TASK_MOVE_NETWORK_READY_FOR_TRANSITION(wrestlers[eSelfID].otherWrestler)
												IF REQUEST_TASK_MOVE_NETWORK_STATE_TRANSITION(wrestlers[eSelfID].otherWrestler, "Running")
													CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: Running requested for otherWrestler")
													SET_TASK_MOVE_NETWORK_SIGNAL_FLOAT(wrestlers[eSelfID].otherWrestler, "Phase", 0.5)
												ENDIF
											ELSE
												CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: otherWrestler not ready for a transition to running")
											ENDIF
											
											IF IS_TASK_MOVE_NETWORK_READY_FOR_TRANSITION(PLAYER_PED_ID())
												IF REQUEST_TASK_MOVE_NETWORK_STATE_TRANSITION(PLAYER_PED_ID(), "Running")
													CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: Running requested for PLAYER_PED_ID")
													SET_TASK_MOVE_NETWORK_SIGNAL_FLOAT(PLAYER_PED_ID(), "Phase", 0.5)
												ENDIF
											ELSE
												CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: PLAYER_PED_ID not ready for a transition to running")
											ENDIF
									//	ELSE
									//		CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: WRESTTLERS NOT REACHED RUNNING YET")
									//	ENDIF
									ELSE
										IF (GET_GAME_TIMER() % 2000) < 50
											CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: PLAYER_PED_ID current state is ", GET_TASK_MOVE_NETWORK_STATE(PLAYER_PED_ID()))
											CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: otherWrestler current state is ", GET_TASK_MOVE_NETWORK_STATE(wrestlers[eSelfID].otherWrestler))
										ENDIF
									ENDIF
									// End Move state change to Running -------------------------------------------------------------------
									
									IF NOT ARM_GET_CLIENT_FLAG(playerData[PARTICIPANT_ID_TO_INT()], ARMCLIENTFLAG_RESET_COUNTDOWN_UI) 
										IF (GET_CURRENT_GAMEMODE() = GAMEMODE_FM)
											UPDATE_MINIGAME_COUNTDOWN_UI(ui.uiCountdown)
										ELSE
											UPDATE_MINIGAME_COUNTDOWN_UI_BRANDED(ui.uiCountdown)
										ENDIF
									ENDIF
									
									IF DOES_ENTITY_EXIST(wrestlers[eSelfID].otherWrestler)
									AND NOT IS_PED_INJURED(wrestlers[eSelfID].otherWrestler)
									AND IS_TASK_MOVE_NETWORK_ACTIVE(PLAYER_PED_ID())
									AND IS_TASK_MOVE_NETWORK_ACTIVE(wrestlers[eSelfID].otherWrestler)
									
										IF ARE_STRINGS_EQUAL(GET_TASK_MOVE_NETWORK_STATE(PLAYER_PED_ID()), "Running")
										AND ARE_STRINGS_EQUAL(GET_TASK_MOVE_NETWORK_STATE(wrestlers[eSelfID].otherWrestler), "Running")
											CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: REACHED RUNNING FLAG SET")
											ARM_SET_CLIENT_FLAG(playerData[PARTICIPANT_ID_TO_INT()], ARMCLIENTFLAG_REACHED_RUNNING, TRUE)
										ENDIF
									ENDIF

								ENDIF
								
								IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
									IF IS_TASK_MOVE_NETWORK_ACTIVE(PLAYER_PED_ID())
										//IF ARE_STRINGS_EQUAL(GET_TASK_MOVE_NETWORK_STATE(PLAYER_PED_ID()), "Running")
											SET_TASK_MOVE_NETWORK_SIGNAL_FLOAT(PLAYER_PED_ID(), "Phase", 0.5)
											CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: Running correct phase self")
										//ENDIF
									ENDIF
								ENDIF
								
								IF DOES_ENTITY_EXIST(wrestlers[eSelfID].otherWrestler)
									IF IS_TASK_MOVE_NETWORK_ACTIVE(wrestlers[eSelfID].otherWrestler)
										//IF ARE_STRINGS_EQUAL(GET_TASK_MOVE_NETWORK_STATE(wrestlers[eSelfID].otherWrestler), "Running")
											SET_TASK_MOVE_NETWORK_SIGNAL_FLOAT(wrestlers[eSelfID].otherWrestler, "Phase", 0.5)
											CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: Running correct phase opponent")
										//ENDIF
									ENDIF
								ENDIF
								
								IF (GET_TIMER_IN_SECONDS(countdownTimer) >= 4.0)	
								AND NOT ARM_GET_CLIENT_FLAG(playerData[PARTICIPANT_ID_TO_INT()], ARMCLIENTFLAG_RESET_COUNTDOWN_UI) 
									CLEANUP_BIG_MESSAGE()
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: Big countdown message cleaned up")
									ARM_SET_UI_FLAG(ui, ARMUIFLAG_SHOW_TUTORIAL, FALSE)
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "Resetting countdown UI...")
									ARM_RESET_COUNTDOWN_UI(ui)
									ARM_SET_CLIENT_FLAG(playerData[PARTICIPANT_ID_TO_INT()], ARMCLIENTFLAG_RESET_COUNTDOWN_UI, TRUE) 
								ENDIF
								
								IF (GET_TIMER_IN_SECONDS(countdownTimer) >= 4.0)		
								AND ARM_GET_CLIENT_FLAG(playerData[PARTICIPANT_ID_TO_INT()], ARMCLIENTFLAG_REACHED_RUNNING) 
									CANCEL_TIMER(countdownTimer)
									RESTART_TIMER_NOW(wrestleTimer)
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: Restarting wrestleTimer")
								ENDIF
								
							ENDIF
								
								
							// Gameplay (only when countdown is over).
							IF ((NOT IS_TIMER_STARTED(countdownTimer)) OR GET_TIMER_IN_SECONDS_SAFE(countdownTimer) >= 3.0)
							AND ARM_GET_CLIENT_FLAG(playerData[PARTICIPANT_ID_TO_INT()], ARMCLIENTFLAG_REACHED_RUNNING)
								IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
									IF NOT ARM_SERVER_GET_FLAG(serverData, ARMSERVERFLAG_REF_OUTRO_REQUESTED)
										ARM_SERVER_SET_FLAG(serverData, ARMSERVERFLAG_REF_OUTRO_REQUESTED, TRUE)
									ENDIF
									IF NOT ARM_SERVER_GET_FLAG(serverData, ARMSERVERFLAG_COUNTDOWN_DONE)
										ARM_SERVER_SET_FLAG(serverData, ARMSERVERFLAG_COUNTDOWN_DONE, TRUE)
									ENDIF
								ENDIF
								
								
								bHappy = (ARM_GET_SERVER_SCORE(serverData) >= 0.0 AND eSelfID = ARMWRESTLERID_HOME) OR (ARM_GET_SERVER_SCORE(serverData) < 0.0 AND eSelfID = ARMWRESTLERID_AWAY)
								
								ARM_UPDATE_WRESTLERS(wrestlers, serverData, playerData, bHappy, sParticipantInfo)
								
								
								
							//	FLOAT fPhase = 0.5
								
//								IF IS_ENTITY_ALIVE(pedPlayer)
//									IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "anim@amb@clubhouse@mini@arm_wrestling@", sAnimName)
//										RETURN GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), "anim@amb@clubhouse@mini@arm_wrestling@", sAnimName)
//									ENDIF
//								ENDIF
	
								ARM_CONTEXTUAL_CAM_SHAKE(cameraSet, ARM_GET_SERVER_SCORE(serverData), NETWORK_IS_HOST_OF_THIS_SCRIPT())// !bIsTableSideA)
//								BOOL bUpdateUI								
//								IF NOT IS_PAUSE_MENU_ACTIVE()
//								AND NOT IS_PLAYER_SWITCH_IN_PROGRESS()
//								AND NOT IS_TRANSITION_ACTIVE()
//									bUpdateUI = ARM_UPDATE_INGAME_UI(ui, armUseContext, PICK_FLOAT(eSelfID = ARMWRESTLERID_HOME, ARM_GET_SERVER_SCORE(serverData), -ARM_GET_SERVER_SCORE(serverData)))
//								ELSE
//									bUpdateUI = FALSE
//								ENDIF
								
								// Update UI, check for quitters.
								//IF bUpdateUI
								IF ARM_UPDATE_INGAME_UI(ui, armUseContext, PICK_FLOAT(eSelfID = ARMWRESTLERID_HOME, ARM_GET_SERVER_SCORE(serverData), -ARM_GET_SERVER_SCORE(serverData)))
									SET_VARIABLE_ON_STREAM("ArmWrestlingIntensity", ABSF(ARM_GET_SERVER_SCORE(serverData)))
								
								ELSE
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: Early Quitter hit this")
									ARM_SET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMCLIENTFLAG_QUIT_DONE, TRUE)
									
									bMinigamePlayerQuitFlag = TRUE
									
									SET_VARS_ON_FMMC_EOM_VOTE_PASSING(ciFMMC_EOM_VOTE_STATUS_QUIT, <<0.0, 0.0, 0.0>>, contentID)
									
									iArmStats[ARM_STAT_NUM_LOSSES]++
									
									//Set screen to black after quiting
									IF IS_SCREEN_FADED_IN()
									OR IS_SCREEN_FADING_IN()
										DO_SCREEN_FADE_OUT(0)
									ENDIF
									
									ARM_SET_PLAYER_MP_STATE(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMMPSTATE_TERMINATE_SPLASH)
								ENDIF
								
								IF NOT ARM_GET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMCLIENTFLAG_FACE_ANIM_STARTED)
//									IF eSelfID = ARMWRESTLERID_HOME
//										TASK_PLAY_ANIM(PLAYER_PED_ID(), "mini@arm_wrestling", "expression_idle_a_face", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_SECONDARY | AF_LOOPING )
//									ELSE
//										TASK_PLAY_ANIM(PLAYER_PED_ID(), "mini@arm_wrestling", "expression_idle_b_face", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_SECONDARY | AF_LOOPING )
//									ENDIF

									TASK_PLAY_ANIM(PLAYER_PED_ID(), "mini@arm_wrestling", GET_FACE_ANIM(ePFA_EXPRESSION, eSelfID = ARMWRESTLERID_HOME), SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_SECONDARY | AF_LOOPING )
									ARM_SET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMCLIENTFLAG_FACE_ANIM_STARTED, TRUE)
								ENDIF
							ENDIF
							
						BREAK
						
						// The match has ended. Ask for a rematch.
						CASE ARMGAMESTATE_WINNER
							//#### clear help when on win/lose view - 2959414
							IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("ARMMP_TUT_1")
							//IF IS_HELP_MESSAGE_BEING_DISPLAYED()
								CLEAR_HELP()
							ENDIF
							
							IF NOT bArmIdleCamSet
								PLAY_SOUND_FROM_ENTITY(-1, "ARM_WRESTLING_ARM_IMPACT_MASTER", PLAYER_PED_ID())
								SHAKE_CAM(ARM_GET_ACTIVE_CAMERA_OBJECT(cameraSet), "SMALL_EXPLOSION_SHAKE", 0.25)							
								bArmIdleCamSet = TRUE
							ENDIF
							
							IF NOT IS_TIMER_STARTED(tutorialTimer)
								RESTART_TIMER_NOW(tutorialTimer)
							ENDIF
							
							IF NOT MPGlobals.DartsData.bResultsDisplayed
								MPGlobals.DartsData.bResultsDisplayed = TRUE
							ENDIF
							
							// Determine our current status.
							bHappy = (ARM_SERVER_GET_FLAG(serverData, ARMSERVERFLAG_HOMEWIN) AND eSelfID = ARMWRESTLERID_HOME) OR (ARM_SERVER_GET_FLAG(serverData, ARMSERVERFLAG_AWAYWIN) AND eSelfID = ARMWRESTLERID_AWAY)
							
							IF GET_TIMER_IN_SECONDS_SAFE(tutorialTimer) > 0.5
								
								IF NOT bShardSet
									IF bHappy
										PLAY_CELEB_WIN_POST_FX()
										SET_SHARD_BIG_MESSAGE(ui.siShardMessage, "ARMMP_SC_WIN", "ARMMP_SC_GWIN", 4000, SHARD_MESSAGE_MIDSIZED, HUD_COLOUR_WHITE)
																				
										IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_BIKER_GANG()
											BIK_ADD_POINTS_FOR_THIS(BIK_ADD_POINTS_MINIGAME)
										ENDIF
									ELSE
										PLAY_CELEB_LOSE_POST_FX()
										SET_SHARD_BIG_MESSAGE(ui.siShardMessage, "ARMMP_SC_LOSE", "ARMMP_SC_GLOSE", 4000, SHARD_MESSAGE_MIDSIZED, HUD_COLOUR_RED)
									//	PLAY_SOUND_FRONTEND ( -1, "LOSER", "HUD_AWARDS")
									ENDIF
									PLAY_SOUND_FRONTEND(-1, PICK_STRING(bHappy, "WIN", "LOSER"), "HUD_AWARDS")
									bShardSet = TRUE
								ENDIF
								
								ARM_SET_ACTIVE_CAMERA(cameraSet, ARMCAMERAID_FRONT_FACE_END, TRUE)

							ENDIF
							
							// See if this was our fastest win ever.
							// Also broadcast the bet data here
							IF bHappy AND IS_TIMER_STARTED(wrestleTimer)
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: You win, checking mp Stat for fastest time")
								IF GET_MP_INT_CHARACTER_STAT(MP_STAT_FASTEST_ARM_WRESTLING_WIN) > FLOOR(1000.0 * GET_TIMER_IN_SECONDS(wrestleTimer))
								OR (GET_MP_INT_CHARACTER_STAT(MP_STAT_FASTEST_ARM_WRESTLING_WIN) = 0)
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: That was fastest time, setting mp stat : ", FLOOR(1000.0 * GET_TIMER_IN_SECONDS(wrestleTimer)))
									SET_MP_INT_CHARACTER_STAT(MP_STAT_FASTEST_ARM_WRESTLING_WIN, FLOOR(1000.0 * GET_TIMER_IN_SECONDS(wrestleTimer)))
								ELSE
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: That was not fastest time, not setting mp stat")
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: Fastest time = ", GET_MP_INT_CHARACTER_STAT(MP_STAT_FASTEST_ARM_WRESTLING_WIN))
								ENDIF
								
								IF iArmStats[ARM_STAT_FASTEST_WIN] = 0
								OR iArmStats[ARM_STAT_FASTEST_WIN] > FLOOR(1000.0 * GET_TIMER_IN_SECONDS(wrestleTimer))
									iArmStats[ARM_STAT_FASTEST_WIN] = FLOOR(1000.0 * GET_TIMER_IN_SECONDS(wrestleTimer))
								ENDIF
								
								iArmStats[ARM_STAT_TOTAL_TIME] += FLOOR(1000.0 * GET_TIMER_IN_SECONDS(wrestleTimer))
								
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: Sending BROADCAST_BETTING_MISSION_FINISHED with my own ID")
								//BROADCAST_BETTING_MISSION_FINISHED(PLAYER_ID())
								
								INCREMENT_MP_INT_PLAYER_STAT(MPPLY_ARMWRESTLING_TOTAL_WINS)
								iArmStats[ARM_STAT_NUM_WINS]++
								
								iArmTotalWins = GET_MP_INT_PLAYER_STAT(MPPLY_ARMWRESTLING_TOTAL_WINS)
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: total wins is now ", iArmTotalWins)
								IF iArmTotalWins > 0
								AND (iArmTotalWins % 5) = 0
									INCREMENT_PLAYER_PED_STAT(CHAR_MULTIPLAYER, PS_STRENGTH, 2)
									CPRINTLN(DEBUG_ARM_WRESTLING, "STRENGTH STAT INCREASED FROM ARM WRESTLING")
								ENDIF
								
								iWinStreak++
								iArmStats[ARM_STAT_STREAK] = iWinStreak
								
								INT iCurrentWins
								iCurrentWins = GET_MP_INT_CHARACTER_STAT(MP_STAT_MOST_ARM_WRESTLING_WINS)
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "iCurrentWins = ", iCurrentWins)
								iCurrentWins++ 
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "iCurrentWins++ = ", iCurrentWins)
								SET_MP_INT_CHARACTER_STAT(MP_STAT_MOST_ARM_WRESTLING_WINS, iCurrentWins)
								
								CANCEL_TIMER(wrestleTimer)
							ELIF IS_TIMER_STARTED(wrestleTimer)
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: Didn't win, not checking mp Stat")
								
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: Sending BROADCAST_BETTING_MISSION_FINISHED with the opponent's ID")
								//BROADCAST_BETTING_MISSION_FINISHED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(sParticipantInfo.iPlayers[eOpponentID])))
								
								iWinStreak = 0
								iArmStats[ARM_STAT_NUM_LOSSES]++
								iArmStats[ARM_STAT_TOTAL_TIME] += FLOOR(1000.0 * GET_TIMER_IN_SECONDS(wrestleTimer))
								
								CANCEL_TIMER(wrestleTimer)
							ENDIF
							
							// See if we are playing our reaction anim.
							IF NOT ARM_GET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMCLIENTFLAG_WIN_LOSS_REACT)
								// Record our new win record if we have broken the old one.
	//							IF GET_MP_INT_CHARACTER_STAT(MP_STAT_MOST_ARM_WRESTLING_WINS) < ARM_GET_SERVER_WINS(serverData, eSelfID)
	//								SET_MP_INT_CHARACTER_STAT(MP_STAT_MOST_ARM_WRESTLING_WINS, ARM_GET_SERVER_WINS(serverData, eSelfID))
	//							ENDIF
								
								// Make sure the quit UI is turned off.
								ARM_SET_UI_FLAG(ui, ARMUIFLAG_SHOW_QUITUI, FALSE)
								
								// Play celebrate/curse anim.
								IF GET_CURRENT_GAMEMODE() != GAMEMODE_FM
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "Playing sound: ", ARM_GET_CROWD_SOUND_FROM_INDEX(1, bHappy), " from coord: ", table.vCenter)
									PLAY_SOUND_FROM_COORD(-1, ARM_GET_CROWD_SOUND_FROM_INDEX(1, bHappy), ARM_GET_TABLE_CENTER(table))
								ENDIF
								
								ARM_SET_LAST_METER_VALUE(ui, 0.5)
								
								STRING sWin, sLoss
								sWin  = "Win"
								sLoss = "Loss"
								// Move state change to Results -----------------------------------------------------------------------
								IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
								AND NOT IS_PED_INJURED(wrestlers[eSelfID].otherWrestler)
									IF IS_TASK_MOVE_NETWORK_ACTIVE(PLAYER_PED_ID())
									AND IS_TASK_MOVE_NETWORK_ACTIVE(wrestlers[eSelfID].otherWrestler)
										IF NOT ARE_STRINGS_EQUAL(GET_TASK_MOVE_NETWORK_STATE(PLAYER_PED_ID()), PICK_STRING(bHappy, sWin, sLoss))
										AND NOT ARE_STRINGS_EQUAL(GET_TASK_MOVE_NETWORK_STATE(wrestlers[eSelfID].otherWrestler), PICK_STRING(!bHappy, sWin, sLoss))
											
											
											IF IS_TASK_MOVE_NETWORK_READY_FOR_TRANSITION(wrestlers[eSelfID].otherWrestler)
												IF REQUEST_TASK_MOVE_NETWORK_STATE_TRANSITION(wrestlers[eSelfID].otherWrestler, PICK_STRING(!bHappy, sWin, sLoss))
													CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: results requested for otherWrestler")
												ELSE
													CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: results request was denied for otherWrestler")
												ENDIF
											ENDIF
											
											IF IS_TASK_MOVE_NETWORK_READY_FOR_TRANSITION(PLAYER_PED_ID())
												IF REQUEST_TASK_MOVE_NETWORK_STATE_TRANSITION(PLAYER_PED_ID(), PICK_STRING(bHappy, sWin, sLoss))
//													IF eSelfID = ARMWRESTLERID_HOME
//														TASK_PLAY_ANIM(PLAYER_PED_ID(), "mini@arm_wrestling", PICK_STRING(bHappy, "win_b_ped_a_face", "win_b_ped_a_face"), SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_SECONDARY )
//													ELSE
//														TASK_PLAY_ANIM(PLAYER_PED_ID(), "mini@arm_wrestling", PICK_STRING(bHappy, "win_b_ped_b_face", "win_a_ped_b_face"), SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_SECONDARY )
//													ENDIF
													STRING sAnim
													sAnim = PICK_STRING(bHappy, GET_FACE_ANIM(ePFA_WIN, eSelfID = ARMWRESTLERID_HOME), GET_FACE_ANIM(ePFA_LOSS, eSelfID = ARMWRESTLERID_HOME))
													TASK_PLAY_ANIM(PLAYER_PED_ID(), "mini@arm_wrestling", sAnim, SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_SECONDARY )
													CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: results requested for PLAYER_PED_ID")
												ELSE
													CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: results request was denied for PLAYER_PED_ID")
												ENDIF
											ENDIF
										ENDIF
										
										IF (ARE_STRINGS_EQUAL(GET_TASK_MOVE_NETWORK_STATE(PLAYER_PED_ID()), PICK_STRING(bHappy, sWin, sLoss))
										AND ARE_STRINGS_EQUAL(GET_TASK_MOVE_NETWORK_STATE(wrestlers[eSelfID].otherWrestler), PICK_STRING(!bHappy, sWin, sLoss)))
											CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: IN WINNER ")
										//	WAIT(30000)
											
											CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: player move state: ", GET_TASK_MOVE_NETWORK_STATE(PLAYER_PED_ID()))
											CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: other  move state: ", GET_TASK_MOVE_NETWORK_STATE(wrestlers[eSelfID].otherWrestler))
											
										//	EVENT_SERVER_SEND_GAME_STATE(SCRIPT_EVENT_MG_ARM_WIN_LOSS)
											EVENT_SERVER_SEND_GAME_WINNER(bHappy)
											
											ARM_SET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMCLIENTFLAG_WIN_LOSS_REACT, TRUE)
										ELSE
											IF (GET_GAME_TIMER() % 1000) < 50
												CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: wrestlers are not at results yet yet")
											ENDIF
										ENDIF
									ELSE
										IF (GET_GAME_TIMER() % 1000) < 50
											CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: wrestlers move network is not active in _winner")
										ENDIF
									ENDIF
								ENDIF
								// End Move state change to Results -------------------------------------------------------------------
							ENDIF
							
							IF IS_TASK_MOVE_NETWORK_ACTIVE(PLAYER_PED_ID())
								IF GET_TASK_MOVE_NETWORK_EVENT(PLAYER_PED_ID(), "WinFinished")
								OR GET_TASK_MOVE_NETWORK_EVENT(PLAYER_PED_ID(), "LossFinished")

									CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: win or loss event just fired off ")
									IF NOT ARM_GET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMCLIENTFLAG_POST_ANIM_DONE)
//										IF eSelfID = ARMWRESTLERID_HOME
//											TASK_PLAY_ANIM(PLAYER_PED_ID(), "mini@arm_wrestling", "stand_idle_a_face", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_SECONDARY | AF_LOOPING )
//										ELSE
//											TASK_PLAY_ANIM(PLAYER_PED_ID(), "mini@arm_wrestling", "stand_idle_b_face", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_SECONDARY | AF_LOOPING )
//										ENDIF
										
										TASK_PLAY_ANIM(PLAYER_PED_ID(), "mini@arm_wrestling", GET_FACE_ANIM(ePFA_STAND_IDLE, eSelfID = ARMWRESTLERID_HOME), SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_SECONDARY | AF_LOOPING )
										
										ARM_SET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMCLIENTFLAG_POST_ANIM_DONE, TRUE)
									ENDIF
								ELSE
									IF (GET_GAME_TIMER() % 1000) < 50
										CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: win or loss event has not fired off in _winning")
									ENDIF
								ENDIF
							ENDIF
							
							// Ask for a rematch.
							IF GET_TIMER_IN_SECONDS(tutorialTimer) > 4.0 //3.8
							
								ARM_GET_LOCAL_INPUT(wrestlers[eSelfID].input)
								uiResult = ARM_UPDATE_SCORECARD_UI(ui, armUseContext, wrestlers[eSelfID].input, ARM_GET_SERVER_WINS(serverData, eSelfID), 
																	ARM_GET_SERVER_WINS(serverData, eOpponentID), bHappy, 
																	ARM_GET_PLAYER_NAME(playerData[sParticipantInfo.iPlayers[eSelfID]]), 
																	ARM_GET_PLAYER_NAME(playerData[sParticipantInfo.iPlayers[eOpponentID]]))
								
								IF (fRematchTimer - GET_TIMER_IN_SECONDS(tutorialTimer)) < 10//5
									fTotalTime = fRematchTimer - GET_TIMER_IN_SECONDS(tutorialTimer)
									IF fTotalTime < 0
										fTotalTime = 0
									ENDIF
									DRAW_GENERIC_TIMER(ROUND(fTotalTime * 1000), "TIM_TIMER", 0, TIMER_STYLE_DONTUSEMILLISECONDS, -1, PODIUMPOS_NONE, HUDORDER_BOTTOM)
								ENDIF
								
								IF NOT ARM_GET_UI_FLAG(ui, ARMUIFLAG_WHOOSH_PLAYED)
		//							CANCEL_TIMER(faceAnimTimer)
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: whoosh played")
									//PLAY_SOUND_FRONTEND(-1, PICK_STRING(bHappy, "WIN", "LOSER"), "HUD_AWARDS")
									ARM_SET_UI_FLAG(ui, ARMUIFLAG_WHOOSH_PLAYED, TRUE)
								ENDIF
								
								SET_INSTRUCTIONAL_BUTTONS_UNDER_HUD_THIS_FRAME()
								
								IF IS_TASK_MOVE_NETWORK_ACTIVE(PLAYER_PED_ID())
									IF (GET_GAME_TIMER() % 2500) < 50
										CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: current state in _winner is ", GET_TASK_MOVE_NETWORK_STATE(PLAYER_PED_ID()))
									ENDIF
									IF GET_TASK_MOVE_NETWORK_EVENT(PLAYER_PED_ID(), "WinFinished")
									OR GET_TASK_MOVE_NETWORK_EVENT(PLAYER_PED_ID(), "LossFinished")
										CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: win or loss event just fired off ")
										IF NOT ARM_GET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMCLIENTFLAG_POST_ANIM_DONE)
//											IF eSelfID = ARMWRESTLERID_HOME
//												TASK_PLAY_ANIM(PLAYER_PED_ID(), "mini@arm_wrestling", "stand_idle_a_face", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_SECONDARY | AF_LOOPING )
//											ELSE
//												TASK_PLAY_ANIM(PLAYER_PED_ID(), "mini@arm_wrestling", "stand_idle_b_face", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_SECONDARY | AF_LOOPING )
//											ENDIF
											
											TASK_PLAY_ANIM(PLAYER_PED_ID(), "mini@arm_wrestling", GET_FACE_ANIM(ePFA_STAND_IDLE, eSelfID = ARMWRESTLERID_HOME), SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_SECONDARY | AF_LOOPING )
											ARM_SET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMCLIENTFLAG_POST_ANIM_DONE, TRUE)
										ENDIF
									ELSE
										IF (GET_GAME_TIMER() % 2500) < 50
											CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: win or loss event has not fired off")
										ENDIF
									ENDIF
								ENDIF
								
								// This player wants a rematch.
								IF uiResult = ARMUIRETVAL_TRUE
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: [ARMGAMESTATE_WINNER] rematch wanted, ARMMPSTATE_WAIT_POST_GAME set here")
									
									RESET_SHARD_BIG_MESSAGE(ui.siShardMessage)
									//STOP_SHARD_BIG_MESSAGE(ui.siShardMessage)
									
									//ARM_PLAY_WRESTLER_ANIMATION(wrestlers[eSelfID], PLAYER_PED_ID(), sAnimDict, PICK_STRING(bHappy, sWaitAnim, sCurseOutro), SLOW_BLEND_IN, SLOW_BLEND_OUT, AF_HOLD_LAST_FRAME | AF_NOT_INTERRUPTABLE)
									ARM_SET_PLAYER_MP_STATE(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMMPSTATE_WAIT_POST_GAME)
									ARM_SET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMCLIENTFLAG_REQUEST_ANIM, FALSE)
									ARM_SET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMCLIENTFLAG_START_ANIM, FALSE)
									
									//resets for arm placement
									ARM_SET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMCLIENTFLAG_WINNER_SET, FALSE)
									fLastScore = 0
									fUsedScore = 0
									fScoreInterp = 0
								
								// This player is quitting.
								ELIF uiResult = ARMUIRETVAL_FALSE
								OR GET_TIMER_IN_SECONDS(tutorialTimer) > 45.0
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: TIMER TUTORIAL TIMER: ", GET_TIMER_IN_SECONDS(tutorialTimer))
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: [ARMGAMESTATE_WINNER] ARMMPSTATE_TERMINATE_SPLASH set here")
									ARM_SET_PLAYER_MP_STATE(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMMPSTATE_TERMINATE_SPLASH)
									EVENT_SERVER_SEND_GAME_STATE(SCRIPT_EVENT_MG_ARM_GAME_END)
								ENDIF
								
								IF ARM_GET_PLAYER_MP_STATE(playerData[sParticipantInfo.iPlayers[eOpponentID]]) = ARMMPSTATE_WAIT_POST_GAME
								AND NOT ARM_GET_UI_FLAG(ui, ARMUIFLAG_SHOW_WAIT_REMATCH)
								AND NOT ARM_GET_UI_FLAG(ui, ARMUIFLAG_SHOW_QUITUI)
//									TEXT_LABEL_63 tlTemp
//									sColoredName = tlTemp
//									sColoredName += "~HUD_COLOUR_RED~"
//									sColoredName += sOpponentName
//									sColoredName += "~s~"
//									PRINT_HELP_WITH_PLAYER_NAME("ARMMP_REQ_RM", sColoredName, HUD_COLOUR_WHITE)
									PRINT_HELP("ARMMP_REQ_RM2")
									ARM_SET_UI_FLAG(ui, ARMUIFLAG_SHOW_WAIT_REMATCH, TRUE)
								ENDIF
							ELSE
								IF bShardSet
									IF HAS_SCALEFORM_MOVIE_LOADED(ui.siShardMessage.siMovie)
										UPDATE_SHARD_BIG_MESSAGE(ui.siShardMessage)
									ENDIF
								ENDIF
							ENDIF
							
						BREAK
					ENDSWITCH
				BREAK
				
				// Waiting for the server to let us go back to the ready menu.
				CASE ARMMPSTATE_WAIT_POST_GAME
						
					#IF IS_DEBUG_BUILD
						ARM_UPDATE_DEBUG(serverData, table, cameraSet, ENUM_TO_INT(ARM_GET_PLAYER_MP_STATE(playerData[sParticipantInfo.iPlayers[eSelfID]])), ENUM_TO_INT(ARM_GET_SERVER_MP_STATE(serverData)), ENUM_TO_INT(ARM_GET_SERVER_GAME_STATE(serverData)), ARM_GET_WRESTLER_ATTACHMENT_OBJECT(wrestlers[eSelfID]), ARM_GET_REFEREE_ATTACHMENT_OBJECT(crowd), crowd.peds)
					#ENDIF
	//				IF NOT ARM_IS_CAMERA_ACTIVE(cameraset, ARMCAMERAID_IDLE)
	//					ARM_SET_ACTIVE_CAMERA(cameraset, ARMCAMERAID_IDLE, TRUE)
	//				ENDIF			
					
					// If server tells us to go back to the menu, do so.
					IF ARM_SERVER_GET_FLAG(serverData, ARMSERVERFLAG_MOVETOMENU)
						
						bShardSet = FALSE
						
						IF bHappy
							ANIMPOSTFX_STOP("MP_Celeb_Win")
							ANIMPOSTFX_PLAY("MP_Celeb_Win_Out", 0, FALSE)
						ELSE
							ANIMPOSTFX_STOP("MP_Celeb_Lose")
							ANIMPOSTFX_PLAY("MP_Celeb_Lose_Out", 0, FALSE)
						ENDIF
						
						//ARM_RESTORE_LAST_PLAY_CAMERA(cameraSet, TRUE)
						CLEAR_HELP()
						CANCEL_TIMER(crowdTimer)
						CANCEL_TIMER(tutorialTimer)
						
						ARM_SET_ACTIVE_CAMERA(cameraSet, ARMCAMERAID_WRESTLE, TRUE)
						CANCEL_TIMER(cameraset.camTimer)
						CANCEL_TIMER(cameraset.shakeTimer)
						
						ARM_CLEAR_BUCKETS(playerData[sParticipantInfo.iPlayers[eSelfID]])
						CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: Buckets cleared in wait post game")
						
						ARM_RESET_LOCAL_BUCKET_SCORE(wrestlers[eSelfID])
						CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: local Buckets cleared in wait post game")
						
						INCREMENT_MP_INT_PLAYER_STAT(MPPLY_ARMWRESTLING_TOTAL_MATCH)
						SET_BIT_FOR_FRANKIE_SAYS_ACHIEVEMENT(ACH45_ARMWRESTLING)
						iArmStats[ARM_STAT_NUM_MATCHES]++
						
						bArmIdleCamSet = FALSE
						bRematch = TRUE
						ARM_SET_UI_FLAG(ui, ARMUIFLAG_WHOOSH_PLAYED, FALSE)
						ARM_SET_UI_FLAG(ui, ARMUIFLAG_SHOW_WAIT_REMATCH, FALSE)
						ARM_SET_UI_FLAG(ui, ARMUIFLAG_SHOW_CONTROLS, FALSE)
						ARM_SET_UI_FLAG(ui, ARMUIFLAG_SETUP_QUITUI, FALSE)
						ARM_SET_UI_FLAG(ui, ARMUIFLAG_SHOW_QUITUI, FALSE)
						ARM_SET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMCLIENTFLAG_WIN_LOSS_REACT, FALSE)
						ARM_SET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMCLIENTFLAG_POST_ANIM_DONE, FALSE)
						ARM_SET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMCLIENTFLAG_FACE_ANIM_STARTED, FALSE)
						ARM_SET_PLAYER_MP_STATE(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMMPSTATE_RUNNING)
					ELSE
						//ARM_UPDATE_CROWD_TAUNTS(crowdTimer)
						
						IF ARM_GET_ARG_TABLE_INDEX(args) != 12
						//	ARM_WIN_CAM_CYCLE(cameraset, bHappy)
						ARM_SET_ACTIVE_CAMERA(cameraSet, ARMCAMERAID_FRONT_FACE_END, TRUE)
						ELSE
//							IF eSelfID = ARMWRESTLERID_AWAY
//								ARM_WIN_CAM_CYCLE(cameraset, FALSE)
//							ELSE
//								ARM_WIN_CAM_CYCLE(cameraset, TRUE)
//							ENDIF
						ENDIF
						
						IF IS_TASK_MOVE_NETWORK_ACTIVE(PLAYER_PED_ID())
							IF GET_TASK_MOVE_NETWORK_EVENT(PLAYER_PED_ID(), "WinFinished")
							OR GET_TASK_MOVE_NETWORK_EVENT(PLAYER_PED_ID(), "LossFinished")
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: win or loss event just fired off ")
								IF NOT ARM_GET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMCLIENTFLAG_POST_ANIM_DONE)
//									IF eSelfID = ARMWRESTLERID_HOME
//										TASK_PLAY_ANIM(PLAYER_PED_ID(), "mini@arm_wrestling", "stand_idle_a_face", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_SECONDARY | AF_LOOPING )
//									ELSE
//										TASK_PLAY_ANIM(PLAYER_PED_ID(), "mini@arm_wrestling", "stand_idle_b_face", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_SECONDARY | AF_LOOPING )
//									ENDIF

									TASK_PLAY_ANIM(PLAYER_PED_ID(), "mini@arm_wrestling", GET_FACE_ANIM(ePFA_STAND_IDLE, eSelfID = ARMWRESTLERID_HOME), SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_SECONDARY | AF_LOOPING )
									ARM_SET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMCLIENTFLAG_POST_ANIM_DONE, TRUE)
								ENDIF
							ELSE
								IF (GET_GAME_TIMER() % 1000) < 50
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: win or loss event has not fired off in _wait_post_game")
								ENDIF
							ENDIF
						ENDIF
						
						// run waiting for rematch text and script here
						uiResult = ARM_UPDATE_WAIT_REMATCH_UI(ui, armUseContext)//, playerData)
						// This player is quitting.
						IF uiResult = ARMUIRETVAL_FALSE
						//OR GET_TIMER_IN_SECONDS(tutorialTimer) > 45.0
							ARM_SET_PLAYER_MP_STATE(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMMPSTATE_TERMINATE_SPLASH)
							
						ENDIF
					ENDIF
				BREAK
				
				// We are being informed that the other player has left.
				CASE ARMMPSTATE_TERMINATE_SPLASH
					#IF IS_DEBUG_BUILD
						ARM_UPDATE_DEBUG(serverData, table, cameraSet, ENUM_TO_INT(ARM_GET_PLAYER_MP_STATE(playerData[sParticipantInfo.iPlayers[eSelfID]])), ENUM_TO_INT(ARM_GET_SERVER_MP_STATE(serverData)), ENUM_TO_INT(ARM_GET_SERVER_GAME_STATE(serverData)), ARM_GET_WRESTLER_ATTACHMENT_OBJECT(wrestlers[eSelfID]), ARM_GET_REFEREE_ATTACHMENT_OBJECT(crowd), crowd.peds)
					#ENDIF
					
					
//				IF IS_ENTITY_ALIVE(pedPlayer1)
//				AND IS_ENTITY_ALIVE(pedPlayer2)
//					IF IS_TASK_MOVE_NETWORK_ACTIVE(pedPlayer1)
//					AND IS_TASK_MOVE_NETWORK_ACTIVE(pedPlayer2)
//						CDEBUG1LN(DEBUG_ARM_WRESTLING, 
//						" MOV1:", IS_TASK_MOVE_NETWORK_ACTIVE(pedPlayer1), " NAME1", GET_PLAYER_NAME(piPlayer1),
//						" MOV2:", IS_TASK_MOVE_NETWORK_ACTIVE(pedPlayer2), " NAME2", GET_PLAYER_NAME(piPlayer2),
//						" STATE1:", GET_TASK_MOVE_NETWORK_STATE(pedPlayer1), 
//						" STATE2:", GET_TASK_MOVE_NETWORK_STATE(pedPlayer2), 
//						" RDY1?:", IS_TASK_MOVE_NETWORK_READY_FOR_TRANSITION(pedPlayer1),
//						//" RDY2?:", 
//						IS_TASK_MOVE_NETWORK_READY_FOR_TRANSITION(pedPlayer2))
//					ENDIF
//				ENDIF
//					
					
//					STRING sGameEndState
//						FLOAT fRunningPhase
//						fRunningPhase = GET_RUNNING_PHASE()
//
//						IF (fRunningPhase >= 0 OR fRunningPhase < 0.3)
//							sGameEndState = "CancelLeft"
//						ELIF (fRunningPhase >= 0.3 OR fRunningPhase < 0.6)
//							sGameEndState = "CancelNeutral"
//						ELIF (fRunningPhase >= 0.6 OR fRunningPhase <= 1.0)
//							sGameEndState = "CancelRight"
//						ENDIF
						
					IF bMinigamePlayerQuitFlag
					OR terminateReason = ARMMPTERMINATEREASON_PLAYERLEFT
						IF HAS_ANIM_DICT_LOADED("anim@amb@clubhouse@mini@arm_wrestling@")
							TASK_PLAY_ANIM(PLAYER_PED_ID(), "anim@amb@clubhouse@mini@arm_wrestling@", PICK_STRING(eSelfID = ARMWRESTLERID_HOME, "cancel_neutral_ped_a", "cancel_neutral_ped_b"))
						ENDIF
						//CDEBUG1LN(DEBUG_ARM_WRESTLING, "State:", sGameEndState, " Phase:", fRunningPhase)
						//TRANSITION_MOVE_NETWORK(sGameEndState)
					ENDIF
					
					//IF  (ARE_STRINGS_EQUAL(GET_TASK_MOVE_NETWORK_STATE(PLAYER_PED_ID()), sGameEndState))
					//AND ARE_STRINGS_EQUAL(GET_TASK_MOVE_NETWORK_STATE(wrestlers[ARMWRESTLERID_HOME].otherWrestler), sGameEndState))
					//IF terminateReason <> ARMMPTERMINATEREASON_PLAYERLEFT
						CDEBUG1LN(DEBUG_ARM_WRESTLING, "Successfully in GameEndAnim")
						ARM_GET_LOCAL_INPUT(wrestlers[eSelfID].input)
						
						IF GET_CURRENT_GAMEMODE() = GAMEMODE_FM
							IF HAS_NET_TIMER_STARTED(timeArmAwardFailSafe)
								
								IF ARM_GET_ACTIVE_CAMERA(cameraSet) <> ARMCAMERAID_FRONT_FACE_END
									ARM_SET_ACTIVE_CAMERA(cameraSet, ARMCAMERAID_FRONT_FACE_END, TRUE)
								ENDIF
								
								//DISABLE_FRONTEND_THIS_FRAME()
								
								// Restore cams.
								//RENDER_SCRIPT_CAMS(FALSE, FALSE)
								CLEANUP_BIG_MESSAGE()
								
								IF (terminateReason = ARMMPTERMINATEREASON_PLAYERLEFT)
									SET_SHARD_BIG_MESSAGE(ui.siShardMessage, "ARMMP_SC_WIN", "ARMMP_TERM_LEFT", (5000), SHARD_MESSAGE_MIDSIZED, HUD_COLOUR_WHITE) // (AWARD_FAILSAFE/2)
									INIT_SIMPLE_USE_CONTEXT(armUseContext, FALSE, FALSE, FALSE, TRUE)
									ADD_SIMPLE_USE_CONTEXT_INPUT(armUseContext, "CMRC_CONT", FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
									SET_SIMPLE_USE_CONTEXT_FULLSCREEN(armUseContext)
									
									SET_VARS_ON_FMMC_EOM_VOTE_PASSING(ciFMMC_EOM_VOTE_STATUS_CONTINUE, <<0.0, 0.0, 0.0>>, contentID)
									
									// freeze the other guy somehow?
									//wrestlers[eSelfID].otherWrestler
									
									IF GET_SKYFREEZE_STAGE() = SKYFREEZE_NONE
										SET_SKYFREEZE_STAGE(SKYFREEZE_FROZEN)
										CDEBUG1LN(DEBUG_ARM_WRESTLING, "setting skyfreeze - frozen")
									ELSE
										CDEBUG1LN(DEBUG_ARM_WRESTLING, "skyfreeze = ", GET_SKYFREEZE_STAGE())
									ENDIF
									
									TOGGLE_RENDERPHASES(FALSE)
									
									leaveState = ARM_MPLEAVESTATE_BIG_MESSAGE
								ELSE
									//ARM_UPDATE_TERMINATE_UI(terminateReason)
									IF terminateReason = ARMMPTERMINATEREASON_ENDGAME
										PRINT_NOW("ARMMP_TERM_LEFT", 5000, 0)
									ENDIF
									//CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
									//iArmStats[ARM_STAT_NUM_WINS]++
									
									leaveState = ARM_MPLEAVESTATE_OUT_ANIM
								ENDIF
								
								REINIT_NET_TIMER(timeArmAwardFailSafe)
								SET_RESULT_SCREEN_DISPLAYING_STATE(FALSE)
								
								IF MPGlobals.DartsData.bResultsDisplayed
									MPGlobals.DartsData.bResultsDisplayed = FALSE
								ENDIF
								
								//SET_STATIC_EMITTER_ENABLED(GET_ARM_RADIO_EMITTER_NAME(ARM_GET_ARG_TABLE_INDEX(args)), FALSE)
								
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "calling trigger pre load for AW")
								TRIGGER_CELEBRATION_PRE_LOAD(sCelebrationData, TRUE)
								
								ARM_SET_PLAYER_MP_STATE(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMMPSTATE_END_LEADERBOARD)
							ELSE
								START_NET_TIMER(timeArmAwardFailSafe)
							ENDIF
						ELSE
							IF ARM_UPDATE_TERMINATE_UI(terminateReason)
								BROADCAST_ARM_WRESTLE_TABLE_ACTIVATE(PLAYER_ID(), FALSE, TRUE)
								//RELEASE_SCRIPT_AUDIO_BANKS(iWinSound)
								IF ARM_GET_SERVER_WINS(serverData, eSelfID) > ARM_GET_SERVER_WINS(serverData, eOpponentID)
									ARM_SCRIPT_CLEANUP_APARTMENT(animatedCam, cameraSet, wrestlers, iCleanupFlags, playerData, sParticipantInfo, ui, ARM_GET_ARG_TABLE_INDEX(args), TRUE)
									ARM_SET_PLAYER_MP_STATE(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMMPSTATE_VICTORY_AUDIO)
								ELSE
									ARM_SCRIPT_CLEANUP_APARTMENT(animatedCam, cameraSet, wrestlers, iCleanupFlags, playerData, sParticipantInfo, ui, ARM_GET_ARG_TABLE_INDEX(args), TRUE)
								ENDIF
							ENDIF
						ENDIF
					//ENDIF
				BREAK
				
				CASE ARMMPSTATE_END_LEADERBOARD
					
					SWITCH leaveState
						CASE ARM_MPLEAVESTATE_BIG_MESSAGE
							IF HAS_SCALEFORM_MOVIE_LOADED(ui.siShardMessage.siMovie)
								IF NOT UPDATE_SHARD_BIG_MESSAGE(ui.siShardMessage, TRUE)
								OR HAS_NET_TIMER_EXPIRED(timeArmAwardFailSafe, AWARD_FAILSAFE/2)
									INCREMENT_MP_INT_PLAYER_STAT(MPPLY_ARMWRESTLING_TOTAL_WINS)
									//iArmStats[ARM_STAT_NUM_WINS]++
									
									iArmTotalWins = GET_MP_INT_PLAYER_STAT(MPPLY_ARMWRESTLING_TOTAL_WINS)
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: total wins is now ", iArmTotalWins)
									IF iArmTotalWins > 0
									AND (iArmTotalWins % 5) = 0
										INCREMENT_PLAYER_PED_STAT(CHAR_MULTIPLAYER, PS_STRENGTH, 2)
										CPRINTLN(DEBUG_ARM_WRESTLING, "STRENGTH STAT INCREASED FROM ARM WRESTLING")
									ENDIF
									
									CLEANUP_BIG_MESSAGE()
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "In ARMMPSTATE_END_LEADERBOARD, going to ARM_MPLEAVESTATE_LEADERBOARD")
									leaveState = ARM_MPLEAVESTATE_LEADERBOARD
								ELSE
									IF NOT NETWORK_TEXT_CHAT_IS_TYPING()
										UPDATE_SIMPLE_USE_CONTEXT(armUseContext)
									ENDIF
								ENDIF
							ELSE
								IF NOT NETWORK_TEXT_CHAT_IS_TYPING()
									UPDATE_SIMPLE_USE_CONTEXT(armUseContext)
								ENDIF
							ENDIF
						BREAK
						
						CASE ARM_MPLEAVESTATE_LEADERBOARD
							JOB_WIN_STATUS eJobWinStatus
							INT iArmXPAmount
							
							IF ARM_GET_CLIENT_FLAG(playerData[iMyParticipantID], ARMCLIENTFLAG_QUIT_DONE)
								//IF SET_SKYSWOOP_UP(TRUE, TRUE, FALSE)
									//SET_SKYFREEZE_CLEAR()
									
									DEAL_WITH_FM_MATCH_END(FMMC_TYPE_MG_ARM_WRESTLING, ServerData.iMatchHistoryID, missionScriptArgs.mdID.idVariation, 
															PICK_INT(bHappy, 1, 0), PICK_INT(bHappy, 1, 2))
									//ARM_UPDATE_TERMINATE_UI(terminateReason)
									BROADCAST_ARM_WRESTLE_TABLE_ACTIVATE(PLAYER_ID(), FALSE, TRUE)
									
									SET_RESULT_SCREEN_DISPLAYING_STATE(FALSE)
									
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "cleaning up here in ARM_MPLEAVESTATE_LEADERBOARD after SET_SKYSWOOP_UP")
									//RELEASE_SCRIPT_AUDIO_BANKS(iWinSound)
									ARM_SCRIPT_CLEANUP_APARTMENT(animatedCam, cameraSet, wrestlers, iCleanupFlags, playerData, sParticipantInfo, ui, ARM_GET_ARG_TABLE_INDEX(args), TRUE)
								//ENDIF
							ELSE
								
								IF ARM_GET_ARG_TABLE_INDEX(args) != 12
									//ARM_WIN_CAM_CYCLE(cameraset, bHappy)
									ARM_SET_ACTIVE_CAMERA(cameraSet, ARMCAMERAID_FRONT_FACE_END, TRUE)
								ELSE
//									IF eSelfID = ARMWRESTLERID_AWAY
//										
//										ARM_WIN_CAM_CYCLE(cameraset, FALSE)
//									ELSE
//										ARM_WIN_CAM_CYCLE(cameraset, TRUE)
//									ENDIF
								ENDIF
								
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: iArmStats[ARM_STAT_NUM_WINS] = ", iArmStats[ARM_STAT_NUM_WINS])
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: iArmStats[ARM_STAT_NUM_LOSSES] = ", iArmStats[ARM_STAT_NUM_LOSSES])
								
								IF iArmStats[ARM_STAT_NUM_WINS] > iArmStats[ARM_STAT_NUM_LOSSES]
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling.sc: ***** Player Won")
									eJobWinStatus = JOB_STATUS_WIN
									winningPlayer = PLAYER_ID()
								ELIF iArmStats[ARM_STAT_NUM_WINS] = iArmStats[ARM_STAT_NUM_LOSSES]
									eJobWinStatus = JOB_STATUS_DRAW
									winningPlayer = PLAYER_ID()
								ELSE
									eJobWinStatus = JOB_STATUS_LOSE
									winningPlayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(sParticipantInfo.iPlayers[eOpponentID]))
								ENDIF
								
								// for reference
								// GIVE_LOCAL_PLAYER_XP(eXPTYPE_STANDARD, "ARMMP_XPT",XPTYPE_COMPLETE, XPCATEGORY_COMPLETE_ARM_WRESTING, (100 * iArmStats[ARM_STAT_NUM_WINS]))
								
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "Base XP Amount = ", (ARM_XP_AMOUNT * iArmStats[ARM_STAT_NUM_WINS]))
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "modifier value = ", g_sMPTunables.fxp_tunable_Minigames_Arm_Wrestling)
								iArmXPAmount = ROUND(g_sMPTunables.fxp_tunable_Minigames_Arm_Wrestling * (ARM_XP_AMOUNT * iArmStats[ARM_STAT_NUM_WINS]))
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "iArmXPAmount   = ", iArmXPAmount)
								
								SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_PLAY_ARM_WRESTLING)
								CPRINTLN(DEBUG_ARM_WRESTLING, "DAILY OBJECTIVE COMPLETE FOR ARM WRESTLING")
								
								IF HAS_GENERIC_CELEBRATION_FINISHED(sCelebrationData, camEndScreen, 
																	"ARMMP_XPT", XPTYPE_COMPLETE, XPCATEGORY_COMPLETE_ARM_WRESTING, iArmXPAmount,
																	eJobWinStatus, 0, eGenericMGStage, winningPlayer, TRUE, 
																	0, 0, TRUE, FALSE)
																	//iArmStats[ARM_STAT_NUM_WINS], iArmStats[ARM_STAT_NUM_MATCHES], TRUE) 
									
									//IF SET_SKYSWOOP_UP(TRUE)
										//RELEASE_SCRIPT_AUDIO_BANKS(iWinSound)
										DEAL_WITH_FM_MATCH_END(FMMC_TYPE_MG_ARM_WRESTLING, ServerData.iMatchHistoryID, missionScriptArgs.mdID.idVariation, 
																PICK_INT(bHappy, 1, 0), PICK_INT(bHappy, 1, 2))
										//ARM_UPDATE_TERMINATE_UI(terminateReason)
										BROADCAST_ARM_WRESTLE_TABLE_ACTIVATE(PLAYER_ID(), FALSE, TRUE)
										
										SET_VARS_ON_FMMC_EOM_VOTE_PASSING(ciFMMC_EOM_VOTE_STATUS_CONTINUE, <<0.0, 0.0, 0.0>>, contentID)
										
										SET_RESULT_SCREEN_DISPLAYING_STATE(FALSE)
										//DETACH_POS_ADJUST(wrestlers)
										ARM_SCRIPT_CLEANUP_APARTMENT(animatedCam, cameraSet, wrestlers, iCleanupFlags, playerData, sParticipantInfo, ui, ARM_GET_ARG_TABLE_INDEX(args), TRUE)
									//ENDIF
								ENDIF
							ENDIF
						BREAK
						
						CASE ARM_MPLEAVESTATE_OUT_ANIM
				
							DO_SCREEN_FADE_IN(500)
//							TASK_PLAY_ANIM(PLAYER_PED_ID(), 
//							"anim@amb@clubhouse@mini@arm_wrestling@", 
//							"intro_ped_a", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_DEFAULT, -1)				
							leaveState = ARM_MPLEAVESTATE_LEADERBOARD
						BREAK
				
					ENDSWITCH
				BREAK
				
				// Load and play an audio stream if we had more wins.
				CASE ARMMPSTATE_VICTORY_AUDIO
					MISSION_FLOW_PLAY_END_OF_MISSION_MUSIC(TRUE)
//					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//						CLEAR_PED_TASKS(PLAYER_PED_ID())
//					ENDIF
					TERMINATE_THIS_THREAD()
				BREAK
			ENDSWITCH
		ENDIF
		
				//############### SPECTATOR HIDE CHECK, call each frame
		IF NOT bIsSpectatorPlayer
		AND ARM_GET_CLIENT_FLAG(playerData[PARTICIPANT_ID_TO_INT()], ARMCLIENTFLAG_HIDE_SPECTATORS)
			HIDE_SPEC_PLAYERS(playerData)
		ENDIF
		
		// Run server tasks.
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			// Act based on the server state.
			SWITCH ARM_GET_SERVER_MP_STATE(serverData)
				// Do nothing.
				CASE ARMMPSTATE_INVALID
				BREAK
				
				// Waiting for another player.
				CASE ARMMPSTATE_INIT
					IF ARM_GET_NUM_WRESTLERS( sParticipantInfo ) >= ENUM_TO_INT(ARMWRESTLERIDS)
						
//						IF NOT IS_TIMER_STARTED(stPlayerPosReceived)
//							START_TIMER_AT(stPlayerPosReceived, 1.0)
//							CDEBUG1LN(DEBUG_ARM_WRESTLING, "SERVER - POS CHECK TIMER STARTED ", GET_TIMER_IN_SECONDS(stPlayerPosReceived))
//						ELSE
//						
//							IF NOT bPlayerPosSelected
//								//SEND PLAYERS THEIR POSITIONS.
//								ARM_GET_TABLE_DATA(vArmWrestleLocation, fTableRot, ARM_GET_ARG_TABLE_INDEX(args), TRUE)
//										
//								//Table side defined.
//								VECTOR vTableSideA
//								vTableSideA = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vArmWrestleLocation, fTableRot, <<fOVERRIDE_x/2, fOVERRIDE_y/2 , fOVERRIDE_Z>>)
//								
//								GET_PLAYERS_SIDES(sParticipantInfo, vTableSideA, bIsPlayerTableSideA)
//								
//								CDEBUG1LN(DEBUG_ARM_WRESTLING, "SERVER - POSITION FOR PLAYER A", GET_PLAYER_NAME( NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(sParticipantInfo.iPlayers[eTS_A]))), " ASIDE?: ", bIsPlayerTableSideA[eTS_A])
//								CDEBUG1LN(DEBUG_ARM_WRESTLING, "SERVER - POSITION FOR PLAYER B", GET_PLAYER_NAME( NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(sParticipantInfo.iPlayers[eTS_B]))), " ASIDE?: ", bIsPlayerTableSideA[eTS_B])
//
//								bPlayerPosSelected = TRUE
//							ELSE
//								
//								IF GET_TIMER_IN_SECONDS(stPlayerPosReceived) > 1.0
//									
//									INT iPlayerID
//									REPEAT ARMWRESTLERIDS iPlayerID
//										IF NOT ARM_GET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[iPlayerID]], ARMCLIENTFLAG_POSITION_RECEIVED)
//											PLAYER_INDEX piPlayer
//											piPlayer =  NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(sParticipantInfo.iPlayers[iPlayerID]))		
//											CDEBUG1LN(DEBUG_ARM_WRESTLING, "SERVER - PLAYER HAS NOT RECEIVED POS: ", GET_PLAYER_NAME(piPlayer))
//											EVENT_SERVER_SEND_PLAYER_POSITION(piPlayer, bIsPlayerTableSideA[iPlayerID])
//										ENDIF
//									ENDREPEAT
//									
//								ENDIF
//							ENDIF
//							
//							IF  ARM_GET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[eTS_A]], ARMCLIENTFLAG_POSITION_RECEIVED)
//							AND ARM_GET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[eTS_B]], ARMCLIENTFLAG_POSITION_RECEIVED)
							CDEBUG1LN(DEBUG_ARM_WRESTLING, "SERVER - 2 PLAYERS FOUND") 
								ARM_SET_SERVER_TABLE_INDEX(serverData, ARM_GET_ARG_TABLE_INDEX(args))
								ARM_SET_SERVER_MP_STATE(serverData, ARMMPSTATE_SETUP)
//							ENDIF
//							
//						ENDIF
					ENDIF
				BREAK
				
				// Wait for both clients to sync up, then start the game.
				CASE ARMMPSTATE_SETUP
//					IF ARM_SERVER_UPDATE_RUN_CONDITIONS(serverData, terminateReason)

					


						IF ARM_GET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMCLIENTFLAG_SYNC_COMPLETE)
						AND ARM_GET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[eOpponentID]], ARMCLIENTFLAG_SYNC_COMPLETE)
							// Wait until we are allowed to create our peds and objects.
							//IF CAN_REGISTER_MISSION_OBJECTS(iNETWORK_OBJECTS)
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "Can register ", iNETWORK_OBJECTS, " objects this frame. Starting creation...")
								ARM_SET_SERVER_MP_STATE(serverData, ARMMPSTATE_RUNNING)
								ARM_SET_SERVER_GAME_STATE(serverData, ARMGAMESTATE_READYMENU)
								
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling: server going to intro scene")
								
								// deactivate floating table help
								BROADCAST_ARM_WRESTLE_TABLE_ACTIVATE(playerData[sParticipantInfo.iPlayers[eSelfID]].curPlayerID, FALSE, TRUE)
								// Broadcast match information to all players.
								broadcastIndex = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(sParticipantInfo.iPlayers[eSelfID]))
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "Broadcasting arm wrestle status to all players on team ", GET_PLAYER_TEAM(broadcastIndex))
								BROADCAST_ARM_WRESTLE_UPDATE(
								broadcastIndex, 
								NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(sParticipantInfo.iPlayers[eOpponentID])), 
								ARM_GET_SERVER_WINS(serverData, ARMWRESTLERID_AWAY), 
								ARM_GET_SERVER_WINS(serverData, ARMWRESTLERID_HOME), 
								GET_PLAYER_TEAM(broadcastIndex), 
								TRUE, 
								TRUE)
//							ELSE
//								#IF IS_DEBUG_BUILD
//								DEBUG_MESSAGE_PERIODIC("Host: ARMMPSTATE_SETUP: can't register mission objects", iDebugThrottle)
//								#ENDIF
//							ENDIF
						ELSE
//							#IF IS_DEBUG_BUILD
//								IF GET_GAME_TIMER() %1000 > 50 
									CDEBUG1LN(DEBUG_ARM_WRESTLING, "Waiting For Clients to ready  SelfID", eSelfID, 
									" self synccomplete", ARM_GET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[eSelfID]], ARMCLIENTFLAG_SYNC_COMPLETE),
									" oppID", eOpponentID, " opp Sync", ARM_GET_CLIENT_FLAG(playerData[sParticipantInfo.iPlayers[eOpponentID]], ARMCLIENTFLAG_SYNC_COMPLETE)
									)
									PLAYER_INDEX piSelf, piOther
									piSelf = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(sParticipantInfo.iPlayers[eSelfID]))
									piOther	 = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(sParticipantInfo.iPlayers[eOpponentID]))
									CDEBUG1LN(DEBUG_ARM_WRESTLING, " Self Name:", GET_PLAYER_NAME(piSelf), " selfParticipantID", sParticipantInfo.iPlayers[eSelfID])	
									CDEBUG1LN(DEBUG_ARM_WRESTLING, " Opponent Name:", GET_PLAYER_NAME(piOther), " opponentParticipantID", sParticipantInfo.iPlayers[eOpponentID])
//								ENDIF
//							DEBUG_MESSAGE_PERIODIC("***************AM_Armwrestling.sc: Host: ARMMPSTATE_SETUP: waiting for ARMCLIENTFLAG_SYNC_COMPLETE", iDebugThrottle)
//							#ENDIF
						ENDIF
//					ELSE
//						#IF IS_DEBUG_BUILD
//						DEBUG_MESSAGE_PERIODIC("Host: ARMMPSTATE_SETUP: waiting for ARM_SERVER_UPDATE_RUN_CONDITIONS", iDebugThrottle)
//						#ENDIF
//					ENDIF
				BREAK
				
				// Main state, clients playing.
				CASE ARMMPSTATE_RUNNING
					// If we aren't home, it means the orignal host left and we should just wait to exit.
					IF eSelfID = ARMWRESTLERID_HOME
						IF ARM_SERVER_UPDATE_RUN_CONDITIONS(serverData, terminateReason, playerData, sParticipantInfo)
							ARM_SERVER_UPDATE_GAME(serverData, playerData, sParticipantInfo)
						ELSE
							IF (GET_GAME_TIMER() % 2500) < 50
								CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling: SERVER RUN CONDITION returning FALSE")
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				// Always look for other players quitting.
				DEFAULT
					ARM_SERVER_UPDATE_RUN_CONDITIONS(serverData, terminateReason, playerData, sParticipantInfo)
				BREAK
			ENDSWITCH
			
			#IF IS_DEBUG_BUILD
				IF bImClient
					CDEBUG2LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling: I was once the client but now I'm the host")
					bImClient = FALSE
				ENDIF
				IF NOT bImHost
					CDEBUG2LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling: Establishing myself as the host")
					bImHost = TRUE
				ENDIF
			#ENDIF
			
			IF (GET_GAME_TIMER() % 5000) < 50
				CDEBUG2LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling: I am the true network host")
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
				IF bImHost
					CDEBUG2LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling: I was once the host but now I'm the client")
					bImHost = FALSE
				ENDIF
				IF NOT bImClient
					CDEBUG2LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling: Establishing myself as the client")
					bImClient = TRUE
				ENDIF
			#ENDIF
			
			IF (GET_GAME_TIMER() % 5000) < 50
				CDEBUG2LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling: I am not the network host")
			ENDIF
		ENDIF
	ELSE
		CDEBUG1LN(DEBUG_ARM_WRESTLING, "AM_Armwrestling: CLEANUP DONE! NO UPDATE!")
	ENDIF
	ENDWHILE
ENDSCRIPT
