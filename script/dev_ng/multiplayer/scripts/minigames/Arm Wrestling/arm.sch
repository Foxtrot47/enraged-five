
//////////////////////////////////////////////////////////////////////
/* arm.sch															*/
/* Author: DJ Jones													*/
/* Definitions for arm wrestling minigame.							*/
//////////////////////////////////////////////////////////////////////


CONST_INT iSTREAMS 10 // Table + attachment obj + opponent + referee + 6 crowd
CONST_INT iSCORE_BUCKETS 15

CONST_INT iMAX_ARM_PARTICIPANTS 4
CONST_INT iARM_INVALID_PARTICIPANT_ID -1
CONST_INT iARM_MAX_SCTV 2

// Use to identify players/broadcast data in array.
ENUM ARM_WRESTLER_ID
	ARMWRESTLERID_AWAY = 0,
	ARMWRESTLERID_HOME = 1,
	ARMWRESTLERIDS = 2
ENDENUM

// Internal game states.
ENUM ARM_GAME_STATE
	ARMGAMESTATE_READYMENU,
	ARMGAMESTATE_PLAYING,
	ARMGAMESTATE_WINNER
ENDENUM

// Crowd members.
ENUM ARM_CROWD_MEMBER
	ARMCROWDMEMBER_REFEREE,
	ARMCROWDMEMBER_AWAY_FAN1,
	ARMCROWDMEMBER_AWAY_FAN2,
	ARMCROWDMEMBER_AWAY_FAN3,
	ARMCROWDMEMBER_HOME_FAN1,
	ARMCROWDMEMBER_HOME_FAN2,
	ARMCROWDMEMBER_HOME_FAN3,
	ARMCROWDMEMBERS
ENDENUM

STRUCT ARM_PARTICIPANT_INFO
	INT iPlayers[ARMWRESTLERIDS]
	INT iSCTV[iARM_MAX_SCTV]
	INT iRawParticipants[iMAX_ARM_PARTICIPANTS]
	INT iPlayerCount
	INT iSCTVCount
ENDSTRUCT
