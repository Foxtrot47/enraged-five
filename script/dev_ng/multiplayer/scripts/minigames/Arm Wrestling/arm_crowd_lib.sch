
//////////////////////////////////////////////////////////////////////
/* arm_crowd_lib.sch												*/
/* Author: DJ Jones													*/
/* Referee & crowd functionality for arm wrestling minigame.		*/
//////////////////////////////////////////////////////////////////////


// Fill data for the crowd models based on the given team.
PROC ARM_GET_CROWD_MODEL_DATA(MODEL_NAMES& models[ARMCROWDMEMBERS], INT iTeam)
	// Vagos
	IF iTeam = TEAM_CRIM_GANG_1
		models[ARMCROWDMEMBER_REFEREE]   = G_M_Y_Azteca_01
		models[ARMCROWDMEMBER_AWAY_FAN1] = A_F_Y_SOUCENT_03
		models[ARMCROWDMEMBER_AWAY_FAN2] = G_M_Y_MEXGOON_01
		models[ARMCROWDMEMBER_AWAY_FAN3] = G_M_Y_MEXGOON_02
		models[ARMCROWDMEMBER_HOME_FAN1] = G_M_Y_MEXGOON_02
		models[ARMCROWDMEMBER_HOME_FAN2] = G_M_Y_MEXGOON_01
		models[ARMCROWDMEMBER_HOME_FAN3] = A_F_Y_EASTSA_03
	
	// Lost
	ELIF iTeam = TEAM_CRIM_GANG_2
		models[ARMCROWDMEMBER_REFEREE]   = G_M_Y_LOST_02
		models[ARMCROWDMEMBER_AWAY_FAN1] = A_F_M_FATWHITE_01
		models[ARMCROWDMEMBER_AWAY_FAN2] = G_M_Y_LOST_03
		models[ARMCROWDMEMBER_AWAY_FAN3] = G_M_Y_LOST_01
		models[ARMCROWDMEMBER_HOME_FAN1] = G_M_Y_LOST_03
		models[ARMCROWDMEMBER_HOME_FAN2] = G_M_Y_LOST_01
		models[ARMCROWDMEMBER_HOME_FAN3] = A_F_M_SALTON_01
	ENDIF
ENDPROC

// Create and set up referee and crowd.
PROC ARM_SETUP_CROWD(ARM_CROWD& crowd, ARM_TABLE& table, ARM_ARGS& args)
	PED_INDEX curPed
	OBJECT_INDEX attachmentObj
	VECTOR vTableCenter = ARM_GET_TABLE_CENTER(table)
	VECTOR vFanPos
	
	// Create the ref and attachment object.
	curPed = CREATE_PED(PEDTYPE_MISSION, ARM_GET_ARG_CROWD_MODEL(args, ARMCROWDMEMBER_REFEREE), ARM_GET_TABLE_CROWD_POS(table, ARMCROWDMEMBER_REFEREE) + <<3, 3, 0>>)
	attachmentObj = CREATE_OBJECT(PROAIR_HOC_PUCK, ARM_GET_TABLE_CROWD_POS(table, ARMCROWDMEMBER_REFEREE))
	ARM_SET_CROWD_PED_INDEX(crowd, ARMCROWDMEMBER_REFEREE, curPed)
	ARM_SET_REFEREE_ATTACHMENT_OBJECT(crowd, attachmentObj)
	
	// Set up the attachment object.
	SET_ENTITY_HEADING(attachmentObj, ARM_GET_TABLE_ROTATION(table) + fREFEREE_HEADING)
	SET_ENTITY_VISIBLE(attachmentObj, FALSE)
	//SET_ENTITY_COLLISION(attachmentObj, FALSE)
	FREEZE_ENTITY_POSITION(attachmentObj, TRUE)	
	
	// Put the referee in place at the table.
	IF NOT IS_PED_INJURED(curPed)
		ATTACH_ENTITY_TO_ENTITY(curPed, attachmentObj, 0, <<0,0,0>>, <<0,0,0>>)
		SET_CURRENT_PED_WEAPON(curPed, WEAPONTYPE_UNARMED, TRUE)
		CLEAR_PED_TASKS_IMMEDIATELY(curPed)
		//SET_ENTITY_INVINCIBLE(curPed, TRUE)
		TASK_STAND_STILL(curPed, -1)
	ENDIF
	
	// Create and set up the first away fan.
	// WORKS
	vFanPos = ARM_GET_TABLE_CROWD_POS(table, ARMCROWDMEMBER_AWAY_FAN1)
	CDEBUG1LN(DEBUG_ARM_WRESTLING, "Creating away fan 1... ", vFanPos)
	curPed = CREATE_PED(PEDTYPE_MISSION, ARM_GET_ARG_CROWD_MODEL(args, ARMCROWDMEMBER_AWAY_FAN1), vFanPos, GET_HEADING_BETWEEN_VECTORS(vFanPos, vTableCenter))
	TASK_STAND_STILL(curPed, -1)
	ARM_SET_CROWD_PED_INDEX(crowd, ARMCROWDMEMBER_AWAY_FAN1, curPed)
	
	// Create and set up the second away fan.
	// DOES NOT SPAWN
	vFanPos = ARM_GET_TABLE_CROWD_POS(table, ARMCROWDMEMBER_AWAY_FAN2)
	CDEBUG1LN(DEBUG_ARM_WRESTLING, "Creating away fan 2... ", vFanPos)
	curPed = CREATE_PED(PEDTYPE_MISSION, ARM_GET_ARG_CROWD_MODEL(args, ARMCROWDMEMBER_AWAY_FAN2), vFanPos, GET_HEADING_BETWEEN_VECTORS(vFanPos, vTableCenter))
	TASK_STAND_STILL(curPed, -1)
	ARM_SET_CROWD_PED_INDEX(crowd, ARMCROWDMEMBER_AWAY_FAN2, curPed)
	
	// Create and set up the third away fan.
	// DOES NOT SPAWN
	vFanPos = ARM_GET_TABLE_CROWD_POS(table, ARMCROWDMEMBER_AWAY_FAN3)
	CDEBUG1LN(DEBUG_ARM_WRESTLING, "Creating away fan 3... ", vFanPos)
	curPed = CREATE_PED(PEDTYPE_MISSION, ARM_GET_ARG_CROWD_MODEL(args, ARMCROWDMEMBER_AWAY_FAN3), vFanPos, GET_HEADING_BETWEEN_VECTORS(vFanPos, vTableCenter))
	TASK_STAND_STILL(curPed, -1)
	ARM_SET_CROWD_PED_INDEX(crowd, ARMCROWDMEMBER_AWAY_FAN3, curPed)
	
	// Create and set up the first home fan.
	vFanPos = ARM_GET_TABLE_CROWD_POS(table, ARMCROWDMEMBER_HOME_FAN1)
	CDEBUG1LN(DEBUG_ARM_WRESTLING, "Creating home fan 1... ", vFanPos)
	curPed = CREATE_PED(PEDTYPE_MISSION, ARM_GET_ARG_CROWD_MODEL(args, ARMCROWDMEMBER_HOME_FAN1), vFanPos, GET_HEADING_BETWEEN_VECTORS(vFanPos, vTableCenter))
	TASK_STAND_STILL(curPed, -1)
	ARM_SET_CROWD_PED_INDEX(crowd, ARMCROWDMEMBER_HOME_FAN1, curPed)
	
	// Create and set up the second home fan.
	// WORKS
	vFanPos = ARM_GET_TABLE_CROWD_POS(table, ARMCROWDMEMBER_HOME_FAN2)
	CDEBUG1LN(DEBUG_ARM_WRESTLING, "Creating home fan 2... ", vFanPos)
	curPed = CREATE_PED(PEDTYPE_MISSION, ARM_GET_ARG_CROWD_MODEL(args, ARMCROWDMEMBER_HOME_FAN2), vFanPos, GET_HEADING_BETWEEN_VECTORS(vFanPos, vTableCenter))
	TASK_STAND_STILL(curPed, -1)
	ARM_SET_CROWD_PED_INDEX(crowd, ARMCROWDMEMBER_HOME_FAN2, curPed)
	
	// Create and set up the third home fan.
	vFanPos = ARM_GET_TABLE_CROWD_POS(table, ARMCROWDMEMBER_HOME_FAN3)
	CDEBUG1LN(DEBUG_ARM_WRESTLING, "Creating home fan 3... ", vFanPos)
	curPed = CREATE_PED(PEDTYPE_MISSION, ARM_GET_ARG_CROWD_MODEL(args, ARMCROWDMEMBER_HOME_FAN3), vFanPos, GET_HEADING_BETWEEN_VECTORS(vFanPos, vTableCenter))
	TASK_STAND_STILL(curPed, -1)
	ARM_SET_CROWD_PED_INDEX(crowd, ARMCROWDMEMBER_HOME_FAN3, curPed)
ENDPROC

// Have the crowd taunt the players, particularly after a match.
PROC ARM_UPDATE_CROWD_TAUNTS(structTimer& crowdTimer)
	INT iRandom
	
	IF NOT IS_TIMER_STARTED(crowdTimer)
		START_TIMER_AT(crowdTimer, fCROWD_TAUNT_DELAY - 1.0)
	ENDIF
	
	IF GET_TIMER_IN_SECONDS(crowdTimer) > fCROWD_TAUNT_DELAY
		iRandom = GET_RANDOM_INT_IN_RANGE(0, 3)
		
		RESTART_TIMER_NOW(crowdTimer)
	ENDIF
ENDPROC

// Tells a crowd member to play an animation.
PROC ARM_PLAY_CROWD_ANIMATION(ARM_CROWD& crowd, ARM_CROWD_MEMBER crowdMember, TEXT_LABEL_23 sAnimDict, TEXT_LABEL_23 sAnimName, FLOAT fBlendInAlpha = NORMAL_BLEND_IN, FLOAT fBlendOutAlpha = NORMAL_BLEND_OUT, ANIMATION_FLAGS flags = AF_DEFAULT, FLOAT fStartPhase = 0.0, BOOL bPhaseControlled = FALSE)
	IF crowdMember = ARMCROWDMEMBERS
		EXIT
	ENDIF
	
	PED_INDEX crowdPed = ARM_GET_CROWD_PED_INDEX(crowd, crowdMember)
	
	IF NOT IS_ENTITY_DEAD(crowdPed)
		TASK_PLAY_ANIM(crowdPed, sAnimDict, sAnimName, fBlendInAlpha, fBlendOutAlpha, -1, flags, fStartPhase, bPhaseControlled)
		ARM_SET_SET_CURRENT_CROWD_ANIM(crowd, crowdMember, sAnimName)
	ENDIF
ENDPROC

// Start cheering for all fans (no referee).
PROC ARM_FANS_CHEER(ARM_CROWD& crowd)
	TEXT_LABEL_23 sAnimDict = "mini@arm_wrestling"
	TEXT_LABEL_23 sAnim
	INT iIndex
	
	FOR iIndex = ENUM_TO_INT(ARMCROWDMEMBER_AWAY_FAN1) TO ENUM_TO_INT(ARMCROWDMEMBERS) - 1
		sAnim = ARM_GET_RANDOM_CHEER()
		ARM_PLAY_CROWD_ANIMATION(crowd, INT_TO_ENUM(ARM_CROWD_MEMBER, iIndex), sAnimDict, sAnim, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, AF_HOLD_LAST_FRAME | AF_NOT_INTERRUPTABLE)
	ENDFOR
ENDPROC

// Update the referee and crowd.
PROC ARM_UPDATE_CROWD(ARM_CROWD& crowd)
	PED_INDEX refereePed = ARM_GET_CROWD_PED_INDEX(crowd, ARMCROWDMEMBER_REFEREE)
	PED_INDEX crowdPed
	ARM_CROWD_MEMBER crowdMember
	TEXT_LABEL_23 sAnimDict = "mini@arm_wrestling"
	TEXT_LABEL_23 sRefIntroAnim = "ref_intro"
	TEXT_LABEL_23 sRefLoopAnim = "ref_loop"
	TEXT_LABEL_23 sRefOutroAnim = "ref_outro"
	TEXT_LABEL_23 sCurrentAnim = ARM_GET_CURRENT_CROWD_ANIM(crowd, ARMCROWDMEMBER_REFEREE)
	INT iIndex
	BOOL bAnimFinished
	
	IF DOES_ENTITY_EXIST(refereePed) AND (NOT IS_ENTITY_DEAD(refereePed))
		bAnimFinished = HAS_ENTITY_ANIM_FINISHED(refereePed, sAnimDict, sCurrentAnim)
	ENDIF
	
	// If the referee is cheering, make sure he keeps cycling through more cheers.
	IF ARM_IS_STRING_A_CHEER(sCurrentAnim)
		IF bAnimFinished
			sCurrentAnim = ARM_GET_RANDOM_CHEER()
			ARM_PLAY_CROWD_ANIMATION(crowd, ARMCROWDMEMBER_REFEREE, sAnimDict, sCurrentAnim, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, AF_LOOPING | AF_NOT_INTERRUPTABLE | AF_PRIORITY_HIGH)
		ENDIF
	
	// If the referee is playing his intro anim, go to looping when he's done.
	ELIF ARE_STRINGS_EQUAL(sCurrentAnim, sRefIntroAnim)
		IF bAnimFinished
			ARM_PLAY_CROWD_ANIMATION(crowd, ARMCROWDMEMBER_REFEREE, sAnimDict, sRefLoopAnim, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, AF_LOOPING | AF_NOT_INTERRUPTABLE | AF_PRIORITY_HIGH)
		ENDIF
	
	// Modify the speed of the outro anim & play cheer.
	ELIF ARE_STRINGS_EQUAL(sCurrentAnim, sRefOutroAnim)
		IF bAnimFinished
			sCurrentAnim = ARM_GET_RANDOM_CHEER()
			ARM_PLAY_CROWD_ANIMATION(crowd, ARMCROWDMEMBER_REFEREE, sAnimDict, sCurrentAnim, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, AF_LOOPING | AF_NOT_INTERRUPTABLE | AF_PRIORITY_HIGH)
		ELIF (NOT IS_ENTITY_DEAD(refereePed)) AND IS_ENTITY_PLAYING_ANIM(refereePed, sAnimDict, sRefOutroAnim)
			SET_ENTITY_ANIM_SPEED(refereePed, sAnimDict, sRefOutroAnim, fREF_OUTRO_SPEED)
		ENDIF
	ENDIF
	
	// Update cheering for each fan.
	FOR iIndex = ENUM_TO_INT(ARMCROWDMEMBER_AWAY_FAN1) TO ENUM_TO_INT(ARMCROWDMEMBERS) - 1
		crowdMember = INT_TO_ENUM(ARM_CROWD_MEMBER, iIndex)
		crowdPed = ARM_GET_CROWD_PED_INDEX(crowd, crowdMember)
		IF DOES_ENTITY_EXIST(crowdPed) AND (NOT IS_ENTITY_DEAD(crowdPed))
			sCurrentAnim = ARM_GET_CURRENT_CROWD_ANIM(crowd, crowdMember)
			IF ARM_IS_STRING_A_CHEER(sCurrentAnim)
				IF HAS_ENTITY_ANIM_FINISHED(crowdPed, sAnimDict, sCurrentAnim)
					sCurrentAnim = ARM_GET_RANDOM_CHEER()
					ARM_PLAY_CROWD_ANIMATION(crowd, crowdMember, sAnimDict, sCurrentAnim, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, AF_HOLD_LAST_FRAME | AF_NOT_INTERRUPTABLE)
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

// Release the crowd members and referee.
PROC ARM_CLEANUP_CROWD(ARM_CROWD& crowd)
	INT i
	
	REPEAT ARMCROWDMEMBERS i
		IF NOT IS_PED_INJURED(crowd.peds[i])
			CLEAR_PED_TASKS_IMMEDIATELY(crowd.peds[i])
			SET_PED_AS_NO_LONGER_NEEDED(crowd.peds[i])
		ENDIF
	ENDREPEAT
ENDPROC
