
//////////////////////////////////////////////////////////////////////
/* arm_client_lib.sch												*/
/* Author: DJ Jones													*/
/* Client-side multiplayer functionality for arm wrestling minigame.*/
//////////////////////////////////////////////////////////////////////


// If the server has thrown any flags, handle them.
PROC ARM_PLAYER_PROCESS_SERVER_FLAGS(ARM_SERVER_BROADCAST_DATA& serverData, ARM_PLAYER_BROADCAST_DATA& playerData)
	IF ARM_SERVER_GET_FLAG(serverData, ARMSERVERFLAG_QUIT_1CLIENT)
	AND ARM_GET_PLAYER_MP_STATE(playerData) <> ARMMPSTATE_TERMINATE_SPLASH
	AND ARM_GET_PLAYER_MP_STATE(playerData) <> ARMMPSTATE_VICTORY_AUDIO
	AND ARM_GET_PLAYER_MP_STATE(playerData) <> ARMMPSTATE_END_LEADERBOARD
		CLEAR_HELP()
		//STOP_STREAM()
		ARM_SET_PLAYER_MP_STATE(playerData, ARMMPSTATE_TERMINATE_SPLASH)
	ENDIF
ENDPROC
