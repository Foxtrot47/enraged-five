//CONSTS
	CONST_INT 		CONST_MINIGAME_COUNT 2
	CONST_FLOAT 	CONST_MINIGAME_RETRY_TIME 10.0
	CONST_FLOAT 	CONST_MG_DELAY_SETUP_VIEW 2.5

  	CONST_INT		MAX_NUM_DARTS_LEGS 3
  	CONST_INT		MAX_NUM_DARTS_SETS 7
	CONST_INT		NUM_MENU_ITEMS_DARTS 1
	//min disatnce to activate minigame locator player check
	CONST_INT		MIN_DISTANCE_TO_MINIGAME 15
	
	CONST_FLOAT		MG_AVALIBLE_SINGLE_DISAPLY_TIME 9.0
	CONST_FLOAT		MG_AVALIBLE_DISAPLY_TIME 4.0
	CONST_FLOAT 	MINIGAME_BETWEEN_MINIGAME_TIME 2.0 
	CONST_FLOAT 	MINIGAME_AVALIBLE_HIDE_TIME 9.0 
	
	//post fix to add to text label to get pc label
	
ENUM MINIGAME_TYPES_ENUM
	eMT_DARTS 		= 0,
	eMT_ARMWRESTLE 	= 1
ENDENUM

ENUM ENUM_MG_AVAILIBLE_DISPLAY
	MGA_SELECT_MINIGAME,
	MGA_DISPLAY_MINIGAME,
	MGA_DELAY_NEXT_MINIGAME,
	MGA_DELAY_AVAILABILITY,
	MGA_HIDE_AVAILABILITY
ENDENUM

ENUM ENUM_MINIGAME_INTERACTION_STAGE
	MIS_IDLE,
	MIS_NAVIGATE,
	MIS_NAVIGATE_END,
	MIS_START_INTERACTION,
	MIS_WAIT_INTERACTION_END,
	MIS_WAIT_PLAYER_INPUT,
	MIS_START_MINIGAME, 
	MIS_CANCEL_READY
ENDENUM

ENUM LAUNCHER_STAGE_ENUM
	eAD_IDLE,
	eAD_LOAD_MINIGAME,
	eAD_START_MINIGAME,
	eAD_WAIT_END_MINIGAME,
	eAD_PRE_CLEANUP,
	eAD_CLEANUP
ENDENUM


FUNC STRING ENUM_MINIGAME_INTERACTION_STAGE_STRING(ENUM_MINIGAME_INTERACTION_STAGE value)
	SWITCH value
		CASE MIS_IDLE RETURN "MIS_IDLE"
		CASE MIS_NAVIGATE RETURN "MIS_NAVIGATE"
		CASE MIS_NAVIGATE_END RETURN "MIS_NAVIGATE_END"
		CASE MIS_START_INTERACTION RETURN "MIS_START_INTERACTION"
		CASE MIS_WAIT_INTERACTION_END RETURN "MIS_WAIT_INTERACTION_END"
		CASE MIS_WAIT_PLAYER_INPUT RETURN "MIS_WAIT_PLAYER_INPUT"
		CASE MIS_START_MINIGAME RETURN "MIS_START_MINIGAME"
	ENDSWITCH
	RETURN "INVALID"
ENDFUNC

PROC SET_INTERACTION_STAGE(ENUM_MINIGAME_INTERACTION_STAGE &value, ENUM_MINIGAME_INTERACTION_STAGE newValue)
	CDEBUG1LN(DEBUG_MG_LAUNCHER_APART,"")
	CDEBUG1LN(DEBUG_MG_LAUNCHER_APART,"	  IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII")
	CDEBUG1LN(DEBUG_MG_LAUNCHER_APART,"	  MINIGAME LAUNCHER - INTERACTION STAGE CHANGED")
	CDEBUG1LN(DEBUG_MG_LAUNCHER_APART,"	  MINIGAME LAUNCHER - FROM: ", ENUM_MINIGAME_INTERACTION_STAGE_STRING(value), " TO >>> ", ENUM_MINIGAME_INTERACTION_STAGE_STRING(newValue))
	value = newValue
	CDEBUG1LN(DEBUG_MG_LAUNCHER_APART,"	  VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV")
ENDPROC

ENUM ENUM_APARTMENT_TYPE
	BIKER_VAR_A,
	BIKER_VAR_B
ENDENUM

ENUM MGL_CLIENTFLAG
	MGL_CF_MG_SETUP_PLAYER,	
	MGL_CF_MG_VALID_PLAYER,
	MGL_CF_MG_START_REQUEST,
	MGL_CF_RECEIVED_SETUP,
	MGL_CF_INITIAL_MG_CHECK
ENDENUM

STRUCT MINIGAME_POSITION_INFO
	VECTOR 	vPosition
	FLOAT	fHeading
	VECTOR	vPointOfInterest
	STRING  sAnimLib
	STRING	sIntoAnim
	STRING	sOutroAnim
ENDSTRUCT

STRUCT MINIGAME_INTERACT_INFO
	MINIGAME_POSITION_INFO MPIavailible[4]
	INT	iNumAvailible
ENDSTRUCT

PROC GET_APARTMENT_INTERACT_INFO(MINIGAME_INTERACT_INFO &info, ENUM_APARTMENT_TYPE eApartType, MINIGAME_TYPES_ENUM eMinigameType)
	SWITCH eApartType
		CASE BIKER_VAR_A 
			SWITCH eMinigameType
			
				CASE eMT_DARTS
					info.iNumAvailible = 2
				
					info.MPIavailible[0].vPosition = <<1117.1617, -3157.0503, -38.0628>>
					info.MPIavailible[0].vPointOfInterest = <<1119.811,-3157.026,-36.340>>
					info.MPIavailible[0].sAnimLib = ""
					info.MPIavailible[0].sIntoAnim = "darts_ig_idle_guy1"
					info.MPIavailible[0].sOutroAnim = ""
					
					info.MPIavailible[1].vPosition = <<0,0,0>>
					info.MPIavailible[1].vPointOfInterest = <<1119.811,-3157.026,-36.340>>
					info.MPIavailible[1].sAnimLib = ""
					info.MPIavailible[1].sIntoAnim = "darts_ig_idle_guy1"
					info.MPIavailible[1].sOutroAnim = ""
				BREAK
				
				CASE eMT_ARMWRESTLE
					info.iNumAvailible = 2
				
					info.MPIavailible[0].vPosition = <<1113.7177, -3158.3787, -38.0628>>
					info.MPIavailible[0].vPointOfInterest =  <<1113.6326, -3159.1221, -36.9950>>
					info.MPIavailible[0].sAnimLib = ""
					info.MPIavailible[0].sIntoAnim = "base"
					info.MPIavailible[0].sOutroAnim = ""
					
					info.MPIavailible[1].vPosition = <<1113.7969, -3159.6799, -38.0628>> 
					info.MPIavailible[1].vPointOfInterest =  <<1113.6326, -3159.1221, -36.9950>>
					info.MPIavailible[1].sAnimLib = ""
					info.MPIavailible[1].sIntoAnim = "base"
					info.MPIavailible[1].sOutroAnim = ""
				BREAK
			ENDSWITCH
		BREAK
		
		CASE BIKER_VAR_B 
			SWITCH eMinigameType
			
				CASE eMT_DARTS
					info.iNumAvailible = 2
				
					info.MPIavailible[0].vPosition = <<1001.0, -3164.3, -35.0826>> 
					info.MPIavailible[0].vPointOfInterest = <<1000.9350,-3162.8154,-33.4827>>
					info.MPIavailible[0].sAnimLib = ""
					info.MPIavailible[0].sIntoAnim = "darts_ig_idle_guy1"
					info.MPIavailible[0].sOutroAnim = ""
					
					info.MPIavailible[1].vPosition = <<1001.0, -3164.3, -35.0826>> 
					info.MPIavailible[1].vPointOfInterest = <<1000.9350,-3162.8154,-33.4827>>
					info.MPIavailible[1].sAnimLib = ""
					info.MPIavailible[1].sIntoAnim = "darts_ig_idle_guy1"
					info.MPIavailible[1].sOutroAnim = ""
				BREAK
				
				CASE eMT_ARMWRESTLE
					info.iNumAvailible = 2
				
					info.MPIavailible[0].vPosition = <<1001.062,-3167.415,-34.597>>
					info.MPIavailible[0].vPointOfInterest = <<1001.062,-3167.415,-34.597>>
					info.MPIavailible[0].sAnimLib = ""
					info.MPIavailible[0].sIntoAnim = "base"
					info.MPIavailible[0].sOutroAnim = ""
					
					info.MPIavailible[1].vPosition = <<1001.062,-3167.415,-34.597>>
					info.MPIavailible[1].vPointOfInterest = <<1001.062,-3167.415,-34.597>>
					info.MPIavailible[1].sAnimLib = ""
					info.MPIavailible[1].sIntoAnim = "base"
					info.MPIavailible[1].sOutroAnim = ""
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
ENDPROC

PROC DEBUG_OUT_MINIGAME_POSITION_INFO(MINIGAME_POSITION_INFO MPIvalue)
	CDEBUG1LN(DEBUG_MG_LAUNCHER_APART,"")
	CDEBUG1LN(DEBUG_MG_LAUNCHER_APART,"%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
	CDEBUG1LN(DEBUG_MG_LAUNCHER_APART,"CURENT MINIGAME POSITION INFO")
	CDEBUG1LN(DEBUG_MG_LAUNCHER_APART,"Locator Position: ", MPIvalue.vPosition)
	CDEBUG1LN(DEBUG_MG_LAUNCHER_APART,"Point Of Interest: ", MPIvalue.vPointOfInterest)
	CDEBUG1LN(DEBUG_MG_LAUNCHER_APART,"Anim Lib: ", MPIvalue.sAnimLib)
	CDEBUG1LN(DEBUG_MG_LAUNCHER_APART,"Intro Anim: ", MPIvalue.sIntoAnim)
	CDEBUG1LN(DEBUG_MG_LAUNCHER_APART,"Outro Anim: ", MPIvalue.sOutroAnim)
	CDEBUG1LN(DEBUG_MG_LAUNCHER_APART,"%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
	CDEBUG1LN(DEBUG_MG_LAUNCHER_APART,"")
ENDPROC

FUNC VECTOR GET_APARTMENT_TYPE_DARTS_LOCATOR(ENUM_APARTMENT_TYPE eApartType)
	SWITCH eApartType
		CASE BIKER_VAR_A RETURN <<1118.4894, -3157.1875, -37.5628>>
		CASE BIKER_VAR_B RETURN <<1001.0, -3164.3, -33.6>>
	ENDSWITCH
	
	RETURN <<0,0,0>>
ENDFUNC

FUNC VECTOR GET_APARTMENT_TYPE_ARMWRESTLE_LOCATOR(ENUM_APARTMENT_TYPE eApartType)
	SWITCH eApartType
		CASE BIKER_VAR_A RETURN <<1116.5,-3153.2,-37.5>>
		CASE BIKER_VAR_B RETURN <<1003.1, -3165.7, -33.6>>
	ENDSWITCH
		
	RETURN <<0,0,0>>
ENDFUNC

INT			iAvailibleMinigamesFlag
CONST_INT 	CONST_MG_TEXT_NUM 7

ENUM ENUM_MG_TEXT
	MGT_IN_PROGRESS,
	MGT_SINGLE_PLAYER,
	MGT_MULTI_PLAYER,
	MGT_MG_AVAILIBLE,
	MGT_WAIT_FOR_PLAYER,
	MGT_WAIT_FOR_LEADER,
	MGT_READY_FOR_GAME
ENDENUM


STRUCT MINIGAME_TYPE_INFO
	INT		iMinigameID
	STRING	sScriptName
	MINIGAME_TYPES_ENUM acType
	VECTOR	vLocatorCoords
	VECTOR	vLocatorArea
	BOOL	bHighlightArea
	INT		iMinPlayers
	INT		iMaxPlayers
	
	STRING sMGText[CONST_MG_TEXT_NUM]
  ENDSTRUCT
  
  
PROC SETUP_MINIGAME_TYPES(MINIGAME_TYPE_INFO& minigameInfoArray[], ENUM_APARTMENT_TYPE eApartType)
	MINIGAME_TYPE_INFO minigameInfo
	
	//IF IS_BIT_SET(iAvailibleMinigamesFlag, ENUM_TO_INT(eMT_DARTS))
		//LOST GANG
		minigameInfo.iMinigameID = ENUM_TO_INT(eMT_DARTS)
		minigameInfo.sScriptName = "AM_Darts_Apartment"
		minigameInfo.acType = eMT_DARTS
		minigameInfo.vLocatorCoords = GET_APARTMENT_TYPE_DARTS_LOCATOR(eApartType)
		minigameInfo.vLocatorArea = <<1.0, 1.0, 1.0>>
		minigameInfo.bHighlightArea = FALSE
		minigameInfo.iMinPlayers = 1
		minigameInfo.iMaxPlayers = 2
		
		minigameInfo.sMGText[MGT_IN_PROGRESS] = "DART_A_PROG"
		minigameInfo.sMGText[MGT_SINGLE_PLAYER] = "DART_A_ONE"
		minigameInfo.sMGText[MGT_MULTI_PLAYER] = "DART_A_TWO"
		minigameInfo.sMGText[MGT_MG_AVAILIBLE] = "DART_A_JOIN"
		minigameInfo.sMGText[MGT_WAIT_FOR_PLAYER] = ""
		minigameInfo.sMGText[MGT_WAIT_FOR_LEADER] = "DART_A_LWAIT"
		minigameInfo.sMGText[MGT_READY_FOR_GAME] = "DART_A_PREP"
		
		minigameInfoArray[ENUM_TO_INT(eMT_DARTS)] = minigameInfo
//	ENDIF
	
//	IF IS_BIT_SET(iAvailibleMinigamesFlag, ENUM_TO_INT(eMT_ARMWRESTLE)) 
		//LOST GANG
		minigameInfo.iMinigameID = ENUM_TO_INT(eMT_ARMWRESTLE)
		minigameInfo.sScriptName = "AM_Armwrestling_Apartment"
		minigameInfo.acType = eMT_ARMWRESTLE
		minigameInfo.vLocatorCoords = GET_APARTMENT_TYPE_ARMWRESTLE_LOCATOR(eApartType)
		minigameInfo.vLocatorArea = <<1.0, 1.0, 1.0>>
		minigameInfo.bHighlightArea = FALSE
		minigameInfo.iMinPlayers = 2
		minigameInfo.iMaxPlayers = 2
		
		minigameInfo.sMGText[MGT_IN_PROGRESS] = "ARMW_A_PROG"
		minigameInfo.sMGText[MGT_SINGLE_PLAYER] = ""
		minigameInfo.sMGText[MGT_MULTI_PLAYER] = "ARMW_A_TWO"
		minigameInfo.sMGText[MGT_MG_AVAILIBLE] = "ARMW_A_JOIN"
		minigameInfo.sMGText[MGT_WAIT_FOR_PLAYER] = "ARMW_A_PWAIT"
		minigameInfo.sMGText[MGT_WAIT_FOR_LEADER] = "ARMW_A_LWAIT"
		minigameInfo.sMGText[MGT_READY_FOR_GAME] = "ARMW_A_PREP"
		
	    minigameInfoArray[ENUM_TO_INT(eMT_ARMWRESTLE)] = minigameInfo
	//ENDIF
	
  ENDPROC
FUNC STRING GET_STRING_FROM_TEXT_LABEL(STRING text)
	RETURN TEXT
ENDFUNC

  FUNC STRING GET_MG_TEXT(MINIGAME_TYPE_INFO minigameInfo, ENUM_MG_TEXT newText)
		RETURN minigameInfo.sMGText[newText]
  ENDFUNC
  
STRUCT  MENU_ROW_INFO
	STRING 	sLabel
	INT		iNumSubselect
	INT		iCurrentSelect
  ENDSTRUCT

STRUCT INT_MENU_ITEM
	INT iValue
//	STRING sLabel
  ENDSTRUCT

PROC  SETUP_DART_MENU(MENU_ROW_INFO &_mriDartsMenu[])
	//Number of sets
	_mriDartsMenu[0].sLabel = "DART_A_GN"
	_mriDartsMenu[0].iNumSubselect = MAX_NUM_DARTS_SETS
	_mriDartsMenu[0].iCurrentSelect = 0
	
	//Number of sets
//	_mriDartsMenu[1].sLabel = "DART_A_MP_GN"
//	_mriDartsMenu[1].iNumSubselect = MAX_NUM_DARTS_SETS
//	_mriDartsMenu[1].iCurrentSelect = 0
  ENDPROC

  PROC SETUP_MENU_DARTS_INFO(INT_MENU_ITEM &sArrayDartsLegs[], INT_MENU_ITEM &sArrayDartsSets[] )
  		//legs
		sArrayDartsLegs[0].iValue = 0
		sArrayDartsLegs[1].iValue = 1
		sArrayDartsLegs[2].iValue = 2
		//sets
		sArrayDartsSets[0].iValue = 0
		sArrayDartsSets[1].iValue = 1
		sArrayDartsSets[2].iValue = 2
		sArrayDartsSets[3].iValue = 3
		sArrayDartsSets[4].iValue = 4
		sArrayDartsSets[5].iValue = 5
		sArrayDartsSets[6].iValue = 6
	ENDPROC
  
  ENUM COUNTER_ENUM
  	eC_UP,
	eC_DOWN
	ENDENUM
	
	PROC MODIFY_COUNTER(INT &Counter, INT CounterMAX, COUNTER_ENUM action, BOOL isArrayCounter = TRUE)
		INT mCounterMAX
		IF isArrayCounter
			mCounterMAX = CounterMAX -1
		ELSE
			mCounterMAX = CounterMAX
		ENDIF
		
		INT CounterMIN = 0
		//counter should not be lower than 0
		IF Counter < CounterMIN
			Counter = CounterMIN
		ENDIF
		
		SWITCH action
			CASE eC_DOWN
				IF Counter < mCounterMAX
					Counter++
				ELIF Counter >= mCounterMAX
					Counter = CounterMIN
				ENDIF
			BREAK
			
			CASE eC_UP
				IF Counter > CounterMIN
					Counter--
				ELIF Counter <= mCounterMAX
					Counter = mCounterMAX
				ENDIF
			BREAK
		  ENDSWITCH
	  ENDPROC
	
	FUNC STRING LABEL_TO_STRING(STRING blockNameLabel)
		RETURN blockNameLabel
	ENDFUNC
	
  //HELPERS
PROC TOGGLE_HELP_TEXT(STRING blockName, BOOL isVisible = TRUE)
	IF NOT IS_STRING_NULL_OR_EMPTY(blockName)
		//CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "FUNC", isVisible)
		IF isVisible
			IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(blockName)
				PRINT_HELP_FOREVER(blockName)
			ENDIF
		ELSE
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(blockName)
				CDEBUG1LN(DEBUG_MG_LAUNCHER_APART,"CLEAR HELP: ", blockName)
				CLEAR_HELP()
			ENDIF
		ENDIF
	ENDIF
ENDPROC	

FUNC STRING LAUNCHER_STAGE_ENUM_STRING(LAUNCHER_STAGE_ENUM stage)
	SWITCH(stage)
		CASE eAD_IDLE RETURN "eAD_IDLE"
		CASE eAD_LOAD_MINIGAME RETURN "eAD_LOAD_MINIGAME"
		CASE eAD_START_MINIGAME RETURN "eAD_START_MINIGAME"
		CASE eAD_WAIT_END_MINIGAME RETURN "eAD_WAIT_END_MINIGAME"
		CASE eAD_CLEANUP RETURN "eAD_CLEANUP"
	ENDSWITCH
	RETURN "INVALID"
ENDFUNC

PROC SWITCH_LAUNCHER_STAGE(LAUNCHER_STAGE_ENUM &value, LAUNCHER_STAGE_ENUM newValue)
	CDEBUG1LN(DEBUG_MG_LAUNCHER_APART,"")
	CDEBUG1LN(DEBUG_MG_LAUNCHER_APART,"##################################################")
	CDEBUG1LN(DEBUG_MG_LAUNCHER_APART,"MINIGAME LAUNCHER - LAUNCHER STAGE CHANGED")
	CDEBUG1LN(DEBUG_MG_LAUNCHER_APART,"MINIGAME LAUNCHER - FROM: ", LAUNCHER_STAGE_ENUM_STRING(value), " TO >>> ", LAUNCHER_STAGE_ENUM_STRING(newValue))
	value = newValue
	CDEBUG1LN(DEBUG_MG_LAUNCHER_APART,"VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV")
ENDPROC

STRUCT MINIGAME_LAUNCH
	BOOL 		bSpectator
	STRING		sScriptName
	BOOL		bRequestLaunch
	INT			iInstanceID
	structTimer stTryLaunchTimer
	FLOAT		fLaunchTryTime
ENDSTRUCT

PROC MINIGAME_LAUNCH_CHECK_RESET(MINIGAME_LAUNCH &minigame[CONST_MINIGAME_COUNT])
	
	INT mini
	FOR mini = 0 TO CONST_MINIGAME_COUNT - 1
		minigame[mini].bRequestLaunch = FALSE
		minigame[mini].fLaunchTryTime = CONST_MINIGAME_RETRY_TIME
		CANCEL_TIMER(minigame[mini].stTryLaunchTimer)
	ENDFOR
ENDPROC

PROC MINIGAME_LAUNCH_CHECK(MINIGAME_LAUNCH &minigame)
	
	IF NOT IS_TIMER_STARTED(minigame.stTryLaunchTimer)
	AND minigame.bRequestLaunch
		ASSERTLN(" Minigame Launch Requested, But Timer Not Stared")
	ENDIF
		
	IF IS_TIMER_STARTED(minigame.stTryLaunchTimer)
		IF GET_TIMER_IN_SECONDS(minigame.stTryLaunchTimer) < minigame.fLaunchTryTime
		AND minigame.bRequestLaunch
		AND NOT NETWORK_IS_SCRIPT_ACTIVE(minigame.sScriptName, minigame.iInstanceID, TRUE)
			//IF GET_GAME_TIMER() % 1000 > 50
				CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, 
				"Trying To launch: ", minigame.sScriptName,
				" Time remaining: ", GET_TIMER_IN_SECONDS(minigame.stTryLaunchTimer))
			//ENDIF
			
			MP_MISSION_DATA fmmcMissionData
			fmmcMissionData.iInstanceId = minigame.iInstanceID
			SET_BIT(fmmcMissionData.iBitSet, 1)
		
			IF NETWORK_IS_SCRIPT_ACTIVE(minigame.sScriptName, minigame.iInstanceID, FALSE)	
				IF NET_LOAD_AND_LAUNCH_SCRIPT_FM(minigame.sScriptName, SCRIPT_XML_STACK_SIZE, fmmcMissionData,
					FALSE,//screenfade
					FALSE,//is mission
					TRUE,//clear wanted level
					TRUE)//is ambient event
					CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "LAUNCHNG SCRIPT: ", minigame.sScriptName, "INSTANCE ID: ",  fmmcMissionData.iInstanceId)
				ENDIF
			ENDIF
		ELSE
			CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, 
			"Finished Trying to launch: ", minigame.sScriptName, " instanceID:", minigame.iInstanceID,
			" Script Launched?: ", NETWORK_IS_SCRIPT_ACTIVE(minigame.sScriptName, minigame.iInstanceID, TRUE))
			CANCEL_TIMER(minigame.stTryLaunchTimer)
			minigame.bRequestLaunch = FALSE
		ENDIF
	ENDIF
ENDPROC

PROC MINIGAME_LAUNCH_INITIALIZE(MINIGAME_LAUNCH &minigame[CONST_MINIGAME_COUNT], MINIGAME_TYPE_INFO& minigameInfoArray[CONST_MINIGAME_COUNT])
	INT iMinigame
	FOR iMinigame = 0 TO CONST_MINIGAME_COUNT -1
		minigame[iMinigame].sScriptName = minigameInfoArray[iMinigame].sScriptName
		minigame[iMinigame].fLaunchTryTime = CONST_MINIGAME_RETRY_TIME
		minigame[iMinigame].iInstanceID = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iVisibilityID
	ENDFOR
ENDPROC

PROC MINIGAME_LAUNCH_REQUEST(MINIGAME_LAUNCH &minigame[CONST_MINIGAME_COUNT], INT iMGID, FLOAT fTimeToTry = CONST_MINIGAME_RETRY_TIME)
	
	minigame[iMGID].fLaunchTryTime = fTimeToTry
	minigame[iMGID].bRequestLaunch = TRUE
	IF NOT IS_TIMER_STARTED(minigame[iMGID].stTryLaunchTimer)
		START_TIMER_NOW(minigame[iMGID].stTryLaunchTimer)
	ELSE
		RESTART_TIMER_NOW(minigame[iMGID].stTryLaunchTimer)
	ENDIF
	
ENDPROC

FUNC BOOL START_CURRENT_MINIGAME(STRING sScriptName, INT iStackSize, MP_MISSION_DATA fmmcMissionData, BOOL bStartAsSpectator = FALSE, BOOL bSetupPlayer = FALSE)
	
	IF bStartAsSpectator
		CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "This player is a spectator for script: ", sScriptName)
		SET_BIT(fmmcMissionData.iBitSet, 1)
	ELSE
		CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "This player is NOT! spectator for script: ", sScriptName)
	ENDIF
	
	IF bSetupPlayer 
		CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "This player is a setup player: ", sScriptName)
		SET_BIT(fmmcMissionData.iBitSet, 2)
	ELSE
		CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "This player is NOT!  a setup palyer: ", sScriptName)
	ENDIF
	
	BOOL bIsScriptActive = NETWORK_IS_SCRIPT_ACTIVE(sScriptName, fmmcMissionData.iInstanceId, TRUE)
	CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "Is script: ",sScriptName, " Active: ", bIsScriptActive)

	IF NOT bIsScriptActive
		CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "Script is not active")
	//GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("sScriptName")) <= 0	
		CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "LOADING MINIGAME SCRIPT PAREMETERS : ", sScriptName, " StackSize", iStackSize)
		
		//Set as currently not on mission type 
		CDEBUG1LN(DEBUG_SAFEHOUSE, "CURRENT PLAYER INDEX :", GlobalplayerBD_FM[NATIVE_TO_INT(GET_PLAYER_INDEX())].iCurrentMissionType)
		GlobalplayerBD_FM[NATIVE_TO_INT(GET_PLAYER_INDEX())].iCurrentMissionType = FMMC_TYPE_INVALID
		CDEBUG1LN(DEBUG_SAFEHOUSE, "POST SET PLAYER INDEX :", GlobalplayerBD_FM[NATIVE_TO_INT(GET_PLAYER_INDEX())].iCurrentMissionType)
		
		IF NET_LOAD_AND_LAUNCH_SCRIPT_FM(sScriptName, iStackSize, fmmcMissionData,
		FALSE,//screenfade
		FALSE,//is mission
		TRUE,//clear wanted level
		TRUE)//is ambient event
		//IF NET_LOAD_AND_LAUNCH_SCRIPT(sScriptName, iStackSize)
			CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "POST MISSION LAUNCHED PLAYER INDEX :", GlobalplayerBD_FM[NATIVE_TO_INT(GET_PLAYER_INDEX())].iCurrentMissionType)
			CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "LAUNCHNG SCRIPT: ", sScriptName, "INSTANCE ID: ",  fmmcMissionData.iInstanceId)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

CONST_FLOAT AFTER_INTERACTION_DELAY 2.0

FUNC  BOOL ALLOW_INTERACTION_UPDATE(structTimer &delayTimer)
	IF IS_TIMER_STARTED(delayTimer)
		IF GET_TIMER_IN_SECONDS(delayTimer) >= AFTER_INTERACTION_DELAY
			PAUSE_TIMER(delayTimer)
			RETURN TRUE
		ELSE
			RETURN FALSE
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
ENDFUNC
  
 ENUM LAUNCH_EVENT_TYPE_ENUM
 	LAUNCH_INVALID,
 	LAUNCH_SCRIPT
 ENDENUM

STRUCT BROADCAST_LAUNCH_STRUCT
	STRUCT_EVENT_COMMON_DETAILS Details
	INT iPlayerIndex
	//LAUNCH_EVENT_TYPE_ENUM eEventType
	BOOL bIsSpectator
	INT	 iScriptID
ENDSTRUCT

PROC BROADCAST_MINIGAME_LAUNCH(PLAYER_INDEX iPlayerIndex, BROADCAST_LAUNCH_STRUCT sLaunchInfo)
	CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "Send Event to start Minigame.", NATIVE_TO_INT(iPlayerIndex))	
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, sLaunchInfo, SIZE_OF(BROADCAST_LAUNCH_STRUCT), SPECIFIC_PLAYER(iPlayerIndex))
ENDPROC

FUNC BOOL PROCESS_MINIGAME_LAUNCH(INT eventID, BROADCAST_LAUNCH_STRUCT &sLaunchInfo)
	BOOL bAllowLaunch
	bAllowLaunch = FALSE
	BROADCAST_LAUNCH_STRUCT sEventInfo
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, eventID, sEventInfo, SIZE_OF(BROADCAST_LAUNCH_STRUCT))
		SWITCH sEventInfo.Details.Type
			CASE SCRIPT_EVENT_MG_LAUNCH_USER
				sLaunchInfo = sEventInfo
				bAllowLaunch = TRUE
			BREAK
		ENDSWITCh
	ENDIF
	
	RETURN bAllowLaunch
ENDFUNC
  
  
  STRUCT DART_HIT_INFO
  	INT iHitValue
	INT iHitMultiplier
	BOOL bDoneScoring
  ENDSTRUCT
  
  
  
  ENUM USER_EVENTS
  	INVALID_EVENT,
  	SET_AS_SETUP,
	FINISHED_MINIGAME,
	REQUEST_START
ENDENUM

STRUCT STRUCT_USER_EVENT
	STRUCT_EVENT_COMMON_DETAILS Details
	//USER_EVENTS eEventType
	INT iMinigameID
	INT iRequestPlayerID
ENDSTRUCT
  		
PROC EVENT_USER(PLAYER_INDEX piPlayer, STRUCT_USER_EVENT sEvent)
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, sEvent, SIZE_OF(STRUCT_USER_EVENT), SPECIFIC_PLAYER(piPlayer))
ENDPROC

PROC EVENT_ALL(STRUCT_USER_EVENT sEvent)
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, sEvent, SIZE_OF(STRUCT_USER_EVENT), ALL_PLAYERS())
ENDPROC

FUNC BOOL RECEIVE_EVENT_USER(INT iEventID, STRUCT_USER_EVENT &sEvent)
	BOOL bReturn = FALSE
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, sEvent, SIZE_OF(STRUCT_USER_EVENT))
		bReturn =  TRUE
	ENDIF
	RETURN bReturn
ENDFUNC

STRUCT MAD_STRUCT
	ENUM_MG_AVAILIBLE_DISPLAY eState
	structTimer stStageDelay
	INT			iCurrentMinigame
	INT 		iMGDisplayedCount
	BOOL		bHelpDisplayed
ENDSTRUCT

PROC MAG_SWITCH_STATE(ENUM_MG_AVAILIBLE_DISPLAY &stateVar, ENUM_MG_AVAILIBLE_DISPLAY newState)
	stateVar = newState
ENDPROC


STRUCT AML_GENERAL_STRUCT
	BOOL	bIsMGNetActiveUpdated
	BOOL	bCanHaveMorePlayers[CONST_MINIGAME_COUNT]
	BOOL	bIsMinigameActiveNet[CONST_MINIGAME_COUNT]
	BOOL	bIsMinigameActiveLocal[CONST_MINIGAME_COUNT]
	INT		iMGCurrent
	BOOL	bPlayerWalking
	BOOL	bAllowIntractUpdate
	INT		iApartmentInstance
	FLOAT	fDisplayTime
ENDSTRUCT

//ENUM ENUM_SERVER_FLAGS
//	SF_MINIGAME_AVAILIBLE,
//	SF_MINIGAME_CAN_HAVE_MORE_PLAYERS, 
//	SF_MINIGAME_HAS_ENOUGH_PLAYERS
//ENDENUM

//Checks bit state before setting it to new
PROC SAFE_BIT_TOGGLE(INT &iBitSet, INT iBitFlag, BOOL bToggleOn)
	BOOL bIsSet = IS_BIT_SET(iBitSet, iBitFlag)
	
	IF bToggleOn
		IF NOT bIsSet
			SET_BIT(iBitSet, iBitFlag)
		ENDIF
	ELSE
		IF bIsSet
			CLEAR_BIT(iBitSet, iBitFlag)
		ENDIF
	ENDIF
ENDPROC


PROC PRINT_WITH_NAME_AND_NUMBER(STRING pTextLabel, STRING PlayerName, INT Duration, INT Colour)
	Colour = Colour
	BEGIN_TEXT_COMMAND_PRINT(pTextLabel)
		ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(PlayerName)
	END_TEXT_COMMAND_PRINT(Duration, TRUE)
ENDPROC


