USING "globals.sch"
USING "net_mission.sch"

// Game Headers
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_path.sch"
USING "commands_event.sch"

// Network Headers
USING "net_include.sch"
USING "net_events.sch"
USING "net_spawn.sch"

//
USING "net_scoring_common.sch"
USING "net_ambience.sch"

//USING "help_at_location.sch"
USING "commands_object.sch"
USING "net_xp_animation.sch"

USING "net_wait_zero.sch"
USING "freemode_events_header.sch"


#IF IS_DEBUG_BUILD
USING "net_debug_log.sch"
#ENDIF

USING "apartment_minigame_launcher.sch"
USING "net_realty_details.sch"
USING "Apartment_Minigame_General.sch"

BOOL	bPreCleanup = FALSE
BOOL 	bUpdateMenuInput
INT		iCurrentMenuRow = 0
BOOL	bInteractionActive = FALSE



MINIGAME_LAUNCH sMinigameLaunchTimer[CONST_MINIGAME_COUNT]

//MINIGAME_TYPE_INFO MTICurrent
BOOL bAllowPropertyCheck
structTimer TimerPropCheck
structTimer TimerAfterGame
structTimer stDelaySetupView
// for switching between availible minigame prompts.
//structTimer stMGAvailable
//INT			iCurrMGAvailible = 0

CONST_FLOAT CONST_AFTER_GAME_DELAY 2.0
//INT		iCurrentMenuRowSelection
//array ID

INT		iCurrentLegs
INT 	iCurrentSets
BOOL	bSetAsSetupPlayer = FALSE
BOOL	bReceivedSetupView = FALSE
INT_MENU_ITEM		iArrayDartNumOfLegs[MAX_NUM_DARTS_LEGS]
INT_MENU_ITEM		iArrayDartNumOfSets[MAX_NUM_DARTS_SETS]
MENU_ROW_INFO mriDartsMenu[NUM_MENU_ITEMS_DARTS]

// Game States. 
CONST_INT GAME_STATE_INI 				0
CONST_INT GAME_STATE_RUNNING			1
CONST_INT GAME_STATE_LEAVE				2
CONST_INT GAME_STATE_TERMINATE_DELAY	3
CONST_INT GAME_STATE_END				4


AML_GENERAL_STRUCT sCurrentValues
MAD_STRUCT sStateNow

BOOl bArrayPlayerInteract [NUM_NETWORK_PLAYERS]

// The server broadcast data.
// Everyone can read this data, only the server can update it
STRUCT ServerBroadcastData

	
	INT iMGAvailible
	INT iMGCanHaveMorePlayers
	INT iMGHasEnoughPlayers
	INT iMGHasSetupPlayer

	// Game State
	INT iServerGameState
	
	//Mission State
	LAUNCHER_STAGE_ENUM eStage = eAD_IDLE
	
	//Mission End timer
	SCRIPT_TIMER MissionTerminateDelayTimer
ENDSTRUCT
ServerBroadcastData serverBD

INT	iCurrentMinigameInstance = -1
ENUM_MINIGAME_INTERACTION_STAGE eCurInteractStage = MIS_IDLE
MINIGAME_INTERACT_INFO sMinigameInteractInfo
ENUM_APARTMENT_TYPE atCurrentApartment
structTimer stFailSafeTimer


MINIGAME_TYPE_INFO	arrayMinigameInfo[CONST_MINIGAME_COUNT]
INT					arrayMinigameUsers[CONST_MINIGAME_COUNT]
INT					arrayMinigameSetupUserID[CONST_MINIGAME_COUNT]

CONST_FLOAT FAIL_TIMER_MAX_TIME 1.5


CONST_INT biP_FoundPackage 0

// The client broadcast data.
// Everyone can read this data, only the local player only the server can update it
STRUCT PlayerBroadcastData
	//curent minigame player count.
	INT iMinigamePlayerCount = 0
	//-1 no activity
	INT	iCurrentMinigame = -1
	// Player Gamestate
	INT iGameState
	// Client BitMask
	INT	iBitMaskActivities
	// Enums
	LAUNCHER_STAGE_ENUM eStage = eAD_IDLE
ENDSTRUCT
PlayerBroadcastData playerBD[NUM_NETWORK_PLAYERS]

PROC CLEANUP()
	SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	CLEAR_HELP()
	CLEAR_MENU_DATA()
	CLEANUP_MENU_ASSETS()
ENDPROC

//Cleanup Mission Data
PROC SCRIPT_CLEANUP()
	CLEANUP()
	PRINTLN("[MPTUT] - CLEANUP MISSION  ")
	
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC

#IF IS_DEBUG_BUILD
	USING "shared_debug.sch"

	PROC ADD_AM_DART_WIDGETS()
		START_WIDGET_GROUP("Mini_apart")
			ADD_WIDGET_BOOL("Occupy Danger Zone", bSetAsSetupPlayer)
		STOP_WIDGET_GROUP()
	ENDPROC
	
#ENDIF

//##### BIT_SET
FUNC BOOL GET_CLIENT_FLAG(PlayerBroadcastData &lPlayerBD, MGL_CLIENTFLAG eFlag)
	RETURN IS_BIT_SET(lPlayerBD.iBitMaskActivities, ENUM_TO_INT(eFlag))	
ENDFUNC

PROC SET_CLIENT_FLAG(PlayerBroadcastData &lPlayerBD, MGL_CLIENTFLAG eNewFlag, BOOL bTrue = TRUE)
	BOOL bIsSet = GET_CLIENT_FLAG(lPlayerBD, eNewFlag)	
	
	IF bTrue
		IF NOT bIsSet
			SET_BIT(lPlayerBD.iBitMaskActivities, ENUM_TO_INT(eNewFlag))
		ENDIF
	ELSE
		IF bIsSet
			CLEAR_BIT(lPlayerBD.iBitMaskActivities, ENUM_TO_INT(eNewFlag))
		ENDIF
	ENDIF
ENDPROC

//Do necessary pre game start ini.
PROC PROCESS_PRE_GAME()
	
	// This marks the script as a net script, and handles any instancing setup. 
	MP_MISSION_DATA missionScriptArgs 
	missionScriptArgs.iInstanceId = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iVisibilityID
	
	IF missionScriptArgs.mdID.idMission <> eAM_Safehouse
		CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "Changing missionScriptArgs.mdID.idMission to eAM_Safehouse...")
		missionScriptArgs.mdID.idMission = eAM_Safehouse
	ENDIF
	
	SETUP_MP_MISSION_FOR_NETWORK(GET_MAX_NUM_PARTICIPANTS_FOR_MP_MISSION(missionScriptArgs.mdID.idMission),  missionScriptArgs)	//NUM_NETWORK_PLAYERS, missionScriptArgs)	//
	CDEBUG1LN(DEBUG_MG_LAUNCHER_APART,"LAUNCHER INSTANCE: ", GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iVisibilityID)
	// GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideVariation
	
	// This makes sure the net script is active, waits untill it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD, SIZE_OF(playerBD))
	
	// KGM: Wait for the first network broadcast before moving on
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		PRINTLN("[MPTUT] - MISSION END - FAILED TO RECEIVE INITIAL NETWORK BROADCAST - SCRIPT CLEANUP D ")
		SCRIPT_CLEANUP()
	ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		// This script will not be paused if another script calls PAUSE_GAME
		SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
		

		playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_INI
		PRINTLN("[MPTUT] - PRE_GAME DONE ")
	ELSE
		PRINTLN("[MPTUT] - MISSION END - NETWORK_IS_GAME_IN_PROGRESS = FALSE - SCRIPT CLEANUP Q ")
		SCRIPT_CLEANUP()
	ENDIF
ENDPROC


//#IF IS_DEBUG_BUILD
//PROC MAINTAIN_DEBUG()
//	IF IS_DEBUG_KEY_JUST_RELEASED(KEY_T, KEYBOARD_MODIFIER_NONE, "F fail")
//		CDEBUG1LN(DEBUG_MG_LAUNCHER_APART,"[MPTUT] - MAINTAIN_DEBUG - Debug Fail - call SCRIPT_CLEANUP")
//		eCurInteractStage = MIS_IDLE
//		CLEANUP()
//		playerBD[PARTICIPANT_ID_TO_INT()].iCurrentMinigame = -1
////	ELIF IS_DEBUG_KEY_JUST_RELEASED(KEY_S, KEYBOARD_MODIFIER_NONE, "S pass")
////		PRINTLN("[MPTUT] - MAINTAIN_DEBUG - Debug Pass - call SCRIPT_CLEANUP")
////		SCRIPT_CLEANUP()
//	ENDIF
//ENDPROC
//#ENDIF

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                  MISSION PROCS                  //////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////



PROC MINIGAME_AVAILABILITY_DISPALY(MAD_STRUCT &sStateCurrent, AML_GENERAL_STRUCT &sGeneralInfo)
	
	MINIGAME_TYPE_INFO mtiCurrentLocal = arrayMinigameInfo[sStateCurrent.iCurrentMinigame]
	
	BOOL bCanDisplayHelp = TRUE
	
	IF bInteractionActive
		bCanDisplayHelp = FALSE
		MAG_SWITCH_STATE(sStateCurrent.eState, MGA_HIDE_AVAILABILITY)
	ENDIF
	
	IF IS_HELP_MESSAGE_BEING_DISPLAYED()
	AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_MG_TEXT(mtiCurrentLocal, MGT_MG_AVAILIBLE))
		bCanDisplayHelp = FALSE
		MAG_SWITCH_STATE(sStateCurrent.eState, MGA_HIDE_AVAILABILITY)
	ENDIF	
	
	IF sGeneralInfo.iMGCurrent >= 0
		bCanDisplayHelp = FALSE
	ENDIF
	
	IF NOT sGeneralInfo.bCanHaveMorePlayers[sStateCurrent.iCurrentMinigame]
	AND (sStateCurrent.eState >= MGA_DISPLAY_MINIGAME AND sStateCurrent.eState <= MGA_DELAY_AVAILABILITY)
		bCanDisplayHelp = FALSE
		MAG_SWITCH_STATE(sStateCurrent.eState, MGA_HIDE_AVAILABILITY)
	ENDIF
	
		SWITCH(sStateCurrent.eState)
			//Select a minigame for display.
			CASE MGA_SELECT_MINIGAME
				CANCEL_TIMER(sStateCurrent.stStageDelay)
				
				BOOL bNewMGFound 
				bNewMGFound = TRUE
				INT iMGID
				FOR iMGID = 0 TO CONST_MINIGAME_COUNT - 1
//					IF GET_GAME_TIMER() % 1000 > 50
//						CDEBUG1LN(DEBUG_MG_LAUNCHER_APART,"MGID", iMGID, " CAN HAVE?", sGeneralInfo.bCanHaveMorePlayers[iMGID])
//					ENDIF
					
					IF iMGID <> sStateCurrent.iCurrentMinigame
					AND sGeneralInfo.bCanHaveMorePlayers[iMGID]
						sStateCurrent.iCurrentMinigame = iMGID
						CDEBUG1LN(DEBUG_MG_LAUNCHER_APART,"MA DISAPLY - new selected, Previous:", sStateCurrent.iCurrentMinigame, " New:", iMGID)
						sGeneralInfo.fDisplayTime = MG_AVALIBLE_DISAPLY_TIME
						MAG_SWITCH_STATE(sStateCurrent.eState, MGA_DISPLAY_MINIGAME)
						CDEBUG1LN(DEBUG_MG_LAUNCHER_APART,"MA DISAPLY - Display for time1:", sGeneralInfo.fDisplayTime)
						BREAK
					ELSE
						IF sGeneralInfo.bCanHaveMorePlayers[sStateCurrent.iCurrentMinigame]
							bNewMGFound = FALSE
						ENDIF
					ENDIF
				ENDFOR
				
				IF NOT bNewMGFound
					sGeneralInfo.fDisplayTime = MG_AVALIBLE_SINGLE_DISAPLY_TIME
					sStateCurrent.iMGDisplayedCount = CONST_MINIGAME_COUNT
					MAG_SWITCH_STATE(sStateCurrent.eState, MGA_DISPLAY_MINIGAME)
					CDEBUG1LN(DEBUG_MG_LAUNCHER_APART,"MA DISAPLY - Display for time2:", sGeneralInfo.fDisplayTime)
				ENDIF
			BREAK
			
			//Disaply minigame availible for X seconds.
			CASE MGA_DISPLAY_MINIGAME
				TOGGLE_HELP_TEXT(GET_MG_TEXT(mtiCurrentLocal, MGT_MG_AVAILIBLE))	
				sStateCurrent.bHelpDisplayed = TRUE
				
				IF NOT IS_TIMER_STARTED(sStateCurrent.stStageDelay)
					START_TIMER_NOW(sStateCurrent.stStageDelay)
					CDEBUG1LN(DEBUG_MG_LAUNCHER_APART,"MA DISAPLY - Display for time:", sGeneralInfo.fDisplayTime)
				ELIF GET_TIMER_IN_SECONDS(sStateCurrent.stStageDelay) >= sGeneralInfo.fDisplayTime
					
					sStateCurrent.iMGDisplayedCount++
					TOGGLE_HELP_TEXT(GET_MG_TEXT(mtiCurrentLocal, MGT_MG_AVAILIBLE) , FALSE)
					sStateCurrent.bHelpDisplayed = FALSE
					RESTART_TIMER_NOW(sStateCurrent.stStageDelay)
					MAG_SWITCH_STATE(sStateCurrent.eState, MGA_DELAY_NEXT_MINIGAME)
				ENDIF
			BREAK
			
			//Wait for x Seconds before dispalying next availible minigame
			CASE MGA_DELAY_NEXT_MINIGAME
				IF NOT IS_TIMER_STARTED(sStateCurrent.stStageDelay)
					START_TIMER_NOW(sStateCurrent.stStageDelay)
				ELIF GET_TIMER_IN_SECONDS(sStateCurrent.stStageDelay) >= MINIGAME_BETWEEN_MINIGAME_TIME
					IF sStateCurrent.iMGDisplayedCount >= CONST_MINIGAME_COUNT
						//stop dispalying minigame availability, for it not to seem like spam.
						CDEBUG1LN(DEBUG_MG_LAUNCHER_APART,"MA DISAPLY - hide availability for:", MINIGAME_AVALIBLE_HIDE_TIME)
						RESTART_TIMER_NOW(sStateCurrent.stStageDelay)
						MAG_SWITCH_STATE(sStateCurrent.eState, MGA_DELAY_AVAILABILITY)
					ELSE
						MAG_SWITCH_STATE(sStateCurrent.eState, MGA_SELECT_MINIGAME)
					ENDIF
				ENDIF
			BREAK
			
			CASE MGA_DELAY_AVAILABILITY
				IF NOT IS_TIMER_STARTED(sStateCurrent.stStageDelay)
					START_TIMER_NOW(sStateCurrent.stStageDelay)
				ELIF GET_TIMER_IN_SECONDS(sStateCurrent.stStageDelay) >= MINIGAME_AVALIBLE_HIDE_TIME
					sStateCurrent.iMGDisplayedCount = 0
					MAG_SWITCH_STATE(sStateCurrent.eState, MGA_SELECT_MINIGAME)
				ENDIF
			BREAK
			
			CASE MGA_HIDE_AVAILABILITY
				IF IS_TIMER_STARTED(sStateCurrent.stStageDelay)
					CANCEL_TIMER(sStateCurrent.stStageDelay)
				ENDIF
				
				IF NOT bCanDisplayHelp
					IF sStateCurrent.bHelpDisplayed
						TOGGLE_HELP_TEXT(GET_MG_TEXT(mtiCurrentLocal, MGT_MG_AVAILIBLE), FALSE)
						sStateCurrent.bHelpDisplayed = FALSE
					ENDIF
				ELSE
					sStateCurrent.iMGDisplayedCount = 0
					MAG_SWITCH_STATE(sStateCurrent.eState, MGA_SELECT_MINIGAME)
				ENDIF
			BREAK
		ENDSWITCH
ENDPROC

PROC REBUILD_DARTS_MENU(MINIGAME_TYPE_INFO minigameInfo)
	CLEAR_MENU_DATA() 
	SET_MENU_ITEM_LAYOUT(MENU_ITEM_TEXT, MENU_ITEM_TEXT)
	SET_MENU_ITEM_JUSTIFICATION(FONT_LEFT, FONT_RIGHT)
	SET_MENU_TITLE("DART_A_SET")
	
	TEXT_LABEL_15 tl15
	ADD_MENU_ITEM_TEXT(0, mriDartsMenu[0].sLabel, 0) 
	tl15 =  "DART_A_S"
	tl15 += iArrayDartNumOfSets[iCurrentSets].iValue
	ADD_MENU_ITEM_TEXT(0, tl15)
	
	IF IS_BIT_SET(serverBD.iMGCanHaveMorePlayers, playerBD[PARTICIPANT_ID_TO_INT()].iCurrentMinigame)
		ADD_MENU_HELP_KEY_CLICKABLE( INPUT_FRONTEND_ACCEPT, GET_MG_TEXT(minigameInfo, MGT_SINGLE_PLAYER))
	ELSE
		ADD_MENU_HELP_KEY_CLICKABLE( INPUT_FRONTEND_ACCEPT, GET_MG_TEXT(minigameInfo, MGT_MULTI_PLAYER))
	ENDIF
	
	ADD_MENU_HELP_KEY_CLICKABLE( INPUT_SCRIPT_RRIGHT, "DART_A_MENU_C")
					
	SET_MENU_ITEM_TOGGLEABLE(FALSE, TRUE)
	SET_CURRENT_MENU_ITEM(iCurrentMenuRow)
    SET_CURRENT_MENU_ITEM_DESCRIPTION("DART_A_GNT") 
  ENDPROC
  
  PROC MENU_CHECK_INPUT()
  	//HANDLE_CURSOR_HIGHLIGHTED_MENU_ITEMS()
	//CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "CURRENT ROW: ", iCurrentMenuRow)
  	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CELLPHONE_UP)
		MODIFY_COUNTER(iCurrentMenuRow, NUM_MENU_ITEMS_DARTS, eC_UP)
		//iCurrentMenuRowSelection = 0
		bUpdateMenuInput = TRUE
	ENDIF
	
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CELLPHONE_DOWN)
		MODIFY_COUNTER(iCurrentMenuRow, NUM_MENU_ITEMS_DARTS , eC_DOWN)
		//iCurrentMenuRowSelection = 0
		bUpdateMenuInput = TRUE
	ENDIF
	
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CELLPHONE_LEFT)
//		IF iCurrentMenuRow = 0
//			MODIFY_COUNTER(iCurrentLegs, MAX_NUM_DARTS_LEGS, eC_UP)
//		ELSE
			MODIFY_COUNTER(iCurrentSets, MAX_NUM_DARTS_SETS, eC_UP)
//		ENDIF
		bUpdateMenuInput = TRUE
	ENDIF
	
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CELLPHONE_RIGHT)
//		IF iCurrentMenuRow = 0
//			MODIFY_COUNTER(iCurrentLegs, MAX_NUM_DARTS_LEGS, eC_DOWN)
//		ELSE
			MODIFY_COUNTER(iCurrentSets, MAX_NUM_DARTS_SETS, eC_DOWN)
//		ENDIF
		bUpdateMenuInput = TRUE
	ENDIF
	
	IF bUpdateMenuInput
		//Update menu Row
		CDEBUG1LN(DEBUG_MG_LAUNCHER_APART,"UPDATE SET: ", iArrayDartNumOfSets[iCurrentSets].iValue, " LEGS:", iArrayDartNumOfLegs[iCurrentLegs].iValue)
		g_FMMC_STRUCT.iNumberOfLegs = iArrayDartNumOfLegs[iCurrentLegs].iValue
		g_FMMC_STRUCT.iNumberOfSets = iArrayDartNumOfSets[iCurrentSets].iValue
		bUpdateMenuInput = False
	ENDIF
  ENDPROC


PROC UPDATE_MINIGAME(MINIGAME_TYPE_INFO minigame, BOOL bAllowStateUpdate)

	BOOL bHideText = FALSE
	BOOL hasSetupMenu = FALSE
	BOOL bInsideCoords = FALSE
	BOOL bIsMinigameActive = NETWORK_IS_SCRIPT_ACTIVE(minigame.sScriptName, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iVisibilityID, FALSE)
	
	//check because player can get moved..
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), minigame.vLocatorCoords) < MIN_DISTANCE_TO_MINIGAME 
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(),minigame.vLocatorCoords, minigame.vLocatorArea, minigame.bHighlightArea, TRUE)
				bInsideCoords = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF bAllowStateUpdate
		SWITCH eCurInteractStage
			CASE MIS_IDLE
				IF bInsideCoords
					bHideText = FALSE
							
					IF  playerBD[PARTICIPANT_ID_TO_INT()].iCurrentMinigame <> minigame.iMinigameID
						playerBD[PARTICIPANT_ID_TO_INT()].iCurrentMinigame = minigame.iMinigameID
						//MTICurrent = minigame
						CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "PLAYER MINIGAME ID UPDATED: ", playerBD[PARTICIPANT_ID_TO_INT()].iCurrentMinigame)	
					ENDIF
									
					IF bIsMinigameActive
						TOGGLE_HELP_TEXT(GET_MG_TEXT(minigame, MGT_READY_FOR_GAME), FALSE)
								
						IF NOT GET_CLIENT_FLAG(playerBD[PARTICIPANT_ID_TO_INT()], MGL_CF_MG_VALID_PLAYER)
							TOGGLE_HELP_TEXT(GET_MG_TEXT(minigame, MGT_IN_PROGRESS))
						ENDIF
					ELSE
						TOGGLE_HELP_TEXT(GET_MG_TEXT(minigame, MGT_IN_PROGRESS), FALSE)
						IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT)
							
							STRUCT_USER_EVENT sEvent
							sEvent.iRequestPlayerID = PARTICIPANT_ID_TO_INT()								
							sEvent.iMinigameID = 1
							sEvent.Details.Type = SCRIPT_EVENT_MG_INTERACT_CHANGED
							EVENT_ALL(sEvent)
									
							//Update current minigame id, will check only current minigame after this.
							NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_LEAVE_CAMERA_CONTROL_ON)
							SET_CLIENT_FLAG(playerBD[PARTICIPANT_ID_TO_INT()], MGL_CF_MG_START_REQUEST)
							SET_CLIENT_FLAG(playerBD[PARTICIPANT_ID_TO_INT()], MGL_CF_MG_VALID_PLAYER)
									
							IF NOT IS_CELLPHONE_DISABLED()
								DISABLE_CELLPHONE(TRUE)
							ENDIF
							
							IF NOT IS_INTERACTION_MENU_DISABLED()
								DISABLE_INTERACTION_MENU()
							ENDIF
							
							CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "Start interact button , hide pre and prog")
							bHideText = TRUE
							GET_APARTMENT_INTERACT_INFO(sMinigameInteractInfo, atCurrentApartment, minigame.acType)
							SET_INTERACTION_STAGE(eCurInteractStage, MIS_NAVIGATE)
						ELSE
							TOGGLE_HELP_TEXT(GET_MG_TEXT(minigame, MGT_READY_FOR_GAME))
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT bInsideCoords
					IF playerBD[PARTICIPANT_ID_TO_INT()].iCurrentMinigame = minigame.iMinigameID
						playerBD[PARTICIPANT_ID_TO_INT()].iCurrentMinigame = -1
						bHideText = TRUE
					ENDIF
				ENDIF
			BREAK
			
			CASE MIS_NAVIGATE
				MINIGAME_POSITION_INFO posInfo
				posInfo = sMinigameInteractInfo.MPIavailible[0]
				DEBUG_OUT_MINIGAME_POSITION_INFO(posInfo)
				
				IF NOT IS_VECTOR_ZERO(posInfo.vPointOfInterest)
					FLOAT fNewHeading
					fNewHeading = GET_HEADING_BETWEEN_VECTORS_2D(GET_ENTITY_COORDS(PLAYER_PED_ID()), posInfo.vPointOfInterest)
					TASK_ACHIEVE_HEADING(PLAYER_PED_ID(), fNewHeading)
				ENDIF
				RESTART_TIMER_NOW(stFailSafeTimer)
				SET_INTERACTION_STAGE(eCurInteractStage, MIS_NAVIGATE_END)
			BREAK
			
			CASE MIS_NAVIGATE_END
				IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_ACHIEVE_HEADING) = FINISHED_TASK
				OR GET_TIMER_IN_SECONDS(stFailSafeTimer) >= FAIL_TIMER_MAX_TIME
				OR IS_VECTOR_ZERO(posInfo.vPointOfInterest)
					CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "MOVE TASK FINISHED")	
					SET_INTERACTION_STAGE(eCurInteractStage, MIS_START_INTERACTION)
				ENDIF
			BREAK
			
			CASE MIS_START_INTERACTION
				posInfo = sMinigameInteractInfo.MPIavailible[0]
				IF NOT IS_STRING_NULL_OR_EMPTY(posInfo.sAnimLib)
					IF HAS_ANIM_DICT_LOADED(posInfo.sAnimLib)
						CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "PLAY INTERACTION ANIM")	
						//TASK_PLAY_ANIM(PLAYER_PED_ID(), posInfo.sAnimLib, posInfo.sIntoAnim, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
						RESTART_TIMER_NOW(stFailSafeTimer)
						SET_INTERACTION_STAGE(eCurInteractStage, MIS_WAIT_INTERACTION_END) // skip end - anim is looping
					ELSE
						IF (GET_GAME_TIMER() % 3000) < 50
							CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "REQUEST ANIM DICT FOR INTERACTION ", posInfo.sAnimLib)
							DEBUG_OUT_MINIGAME_POSITION_INFO(posInfo)
						ENDIF
						REQUEST_ANIM_DICT(posInfo.sAnimLib)
					ENDIF
				ELSE
					RESTART_TIMER_NOW(stFailSafeTimer)
					SET_INTERACTION_STAGE(eCurInteractStage, MIS_WAIT_INTERACTION_END)
				ENDIF
			BREAK
			
			CASE MIS_WAIT_INTERACTION_END
				IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_PLAY_ANIM) = PERFORMING_TASK
				OR GET_TIMER_IN_SECONDS(stFailSafeTimer) >= FAIL_TIMER_MAX_TIME
					CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "INTERACT ANIM FINISHED")
					//FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
					SET_INTERACTION_STAGE(eCurInteractStage, MIS_WAIT_PLAYER_INPUT)
				ENDIF
			BREAK
			
			CASE MIS_WAIT_PLAYER_INPUT
			
				DISABLE_FRONTEND_THIS_FRAME()
				
				//stop the failsafe timer from previous stages.
				IF IS_TIMER_STARTED(stFailSafeTimer)
					CANCEL_TIMER(stFailSafeTimer)
				ENDIF
				
				IF NOT IS_TIMER_STARTED(stDelaySetupView)
					START_TIMER_NOW(stDelaySetupView)
				ELIF GET_TIMER_IN_SECONDS(stDelaySetupView) >= CONST_MG_DELAY_SETUP_VIEW
				OR bReceivedSetupView
				OR IS_BIT_SET(serverBD.iMGHasSetupPlayer, minigame.iMinigameID)
					//CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "ALLOWED TO MENU RECEIVED?: ", bReceivedSetupView, " HAS SETUP?", IS_BIT_SET(serverBD.iMGHasSetupPlayer, minigame.iMinigameID))
					bReceivedSetupView = FALSE
					SET_CLIENT_FLAG(playerBD[PARTICIPANT_ID_TO_INT()], MGL_CF_RECEIVED_SETUP, TRUE)
				ENDIF
				
				//If setup player does not exist set current player as setup
				IF minigame.iMinigameID >= 0
					IF bSetAsSetupPlayer
						bSetAsSetupPlayer = FALSE
						IF NOT GET_CLIENT_FLAG(playerBD[PARTICIPANT_ID_TO_INT()], MGL_CF_MG_SETUP_PLAYER)
							SET_CLIENT_FLAG(playerBD[PARTICIPANT_ID_TO_INT()], MGL_CF_MG_SETUP_PLAYER)
							SET_CLIENT_FLAG(playerBD[PARTICIPANT_ID_TO_INT()], MGL_CF_MG_VALID_PLAYER)
							CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "PLAYER SET AS SETUP PLAYER: ", minigame.sScriptName)
						ENDIF
					ENDIF
				ENDIF
				
				//Cancel game ready
				IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RRIGHT)
				OR bIsMinigameActive
					CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "MINIGAME READY CANCELED")
					SET_INTERACTION_STAGE(eCurInteractStage, MIS_CANCEL_READY)		
				ENDIF
				
//				CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, 
//				" mgID", minigame.iMinigameID,
//				"RECEIVED SETUP???: ", IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iBitMaskActivities, MGL_CF_RECEIVED_SETUP),
//				" TIMER:",  GET_TIMER_IN_SECONDS(stDelaySetupView),
//				" Received?", bReceivedSetupView,
//				" Server has setup?", IS_BIT_SET(serverBD.iMGHasSetupPlayer, minigame.iMinigameID),
//				" is setup?" , IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iBitMaskActivities,MGL_CF_MG_SETUP_PLAYER))
				
				//IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iBitMaskActivities, MGL_CF_RECEIVED_SETUP)
				
				//ENDIF

				IF GET_CLIENT_FLAG(playerBD[PARTICIPANT_ID_TO_INT()], MGL_CF_RECEIVED_SETUP)
					IF GET_CLIENT_FLAG(playerBD[PARTICIPANT_ID_TO_INT()], MGL_CF_MG_SETUP_PLAYER)
						TOGGLE_HELP_TEXT(GET_MG_TEXT(minigame, MGT_WAIT_FOR_LEADER), FALSE)
						//For darts specific show menu to sertup player only and if there are enough players.
						IF minigame.acType = eMT_DARTS
						AND LOAD_MENU_ASSETS()
							hasSetupMenu = TRUE
							MENU_CHECK_INPUT()
							REBUILD_DARTS_MENU(minigame)
							DRAW_MENU()
						ENDIF
																	
						//Check if game started
						IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
						AND IS_BIT_SET(serverBD.iMGHasEnoughPlayers, minigame.iMinigameID)
							CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "MINIGAME START CONTROLL PRESSED")
							SET_INTERACTION_STAGE(eCurInteractStage, MIS_START_MINIGAME)
							
							STRUCT_USER_EVENT sEvent
							sEvent.Details.Type = SCRIPT_EVENT_MG_LAUNCH
							sEvent.iMinigameID = minigame.iMinigameID
							sEvent.iRequestPlayerID = PARTICIPANT_ID_TO_INT()
							EVENT_ALL(sEvent)
							
						ENDIF
							
						//check if server thinks there is enough players in the area
						IF IS_BIT_SET(serverBD.iMGHasEnoughPlayers, minigame.iMinigameID)
								TOGGLE_HELP_TEXT(GET_MG_TEXT(minigame, MGT_WAIT_FOR_PLAYER), FALSE)
							IF hasSetupMenu
								TOGGLE_HELP_TEXT(GET_MG_TEXT(minigame, MGT_SINGLE_PLAYER), FALSE)
								TOGGLE_HELP_TEXT(GET_MG_TEXT(minigame, MGT_MULTI_PLAYER), FALSE)
							ELSE
								IF IS_BIT_SET(serverBD.iMGCanHaveMorePlayers, minigame.iMinigameID)
									TOGGLE_HELP_TEXT(GET_MG_TEXT(minigame, MGT_SINGLE_PLAYER))
								ELSE
									TOGGLE_HELP_TEXT(GET_MG_TEXT(minigame, MGT_MULTI_PLAYER))
								ENDIF
							ENDIF
						ELSE
							IF NOT hasSetupMenu
								//text if game did not meet the required user count.
								TOGGLE_HELP_TEXT(GET_MG_TEXT(minigame, MGT_WAIT_FOR_PLAYER))
							ENDIF
							TOGGLE_HELP_TEXT(GET_MG_TEXT(minigame, MGT_SINGLE_PLAYER), FALSE)
							TOGGLE_HELP_TEXT(GET_MG_TEXT(minigame, MGT_MULTI_PLAYER), FALSE)
							TOGGLE_HELP_TEXT(GET_MG_TEXT(minigame, MGT_WAIT_FOR_LEADER), FALSE)
						ENDIF	
					ELSE
						//if ended up in this stage but game already launched, go back to idle
		//				IF bIsMinigameActive
		//				AND NOT IS_BIT_SET(serverBD.iAllowPlayerScriptLaunch, PARTICIPANT_ID_TO_INT())
		//					SET_INTERACTION_STAGE(eCurInteractStage, MIS_CANCEL_READY)
		//				ENDIF
					
						//if max player count was not reached, register palyer as a valid new palyer.
						IF playerBD[PARTICIPANT_ID_TO_INT()].iCurrentMinigame >= 0
							IF IS_BIT_SET(serverBD.iMGCanHaveMorePlayers, playerBD[PARTICIPANT_ID_TO_INT()].iCurrentMinigame)
								SET_CLIENT_FLAG(playerBD[PARTICIPANT_ID_TO_INT()], MGL_CF_MG_VALID_PLAYER)
							ENDIF
							//all valid players should wait for leader to start the minigame
							IF GET_CLIENT_FLAG(playerBD[PARTICIPANT_ID_TO_INT()], MGL_CF_MG_VALID_PLAYER)
								//wait for leader to start.
								TOGGLE_HELP_TEXT(GET_MG_TEXT(minigame, MGT_WAIT_FOR_LEADER))
							ELSE
								TOGGLE_HELP_TEXT(GET_MG_TEXT(minigame, MGT_WAIT_FOR_LEADER), FALSE)
							ENDIF
						ELSE
							TOGGLE_HELP_TEXT(GET_MG_TEXT(minigame, MGT_WAIT_FOR_LEADER), FALSE)
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE MIS_START_MINIGAME
				DISABLE_FRONTEND_THIS_FRAME()
				bSetAsSetupPlayer = FALSE
				bReceivedSetupView = FALSE
				RESTART_TIMER_NOW(TimerAfterGame)
				SET_INTERACTION_STAGE(eCurInteractStage, MIS_IDLE)	
			BREAK
			
			CASE MIS_CANCEL_READY
				bSetAsSetupPlayer = FALSE
				bHideText = TRUE
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				posInfo = sMinigameInteractInfo.MPIavailible[0]
				DEBUG_OUT_MINIGAME_POSITION_INFO(posInfo)
				
				SET_CLIENT_FLAG(playerBD[PARTICIPANT_ID_TO_INT()], MGL_CF_MG_START_REQUEST, FALSE)
				SET_CLIENT_FLAG(playerBD[PARTICIPANT_ID_TO_INT()], MGL_CF_MG_VALID_PLAYER, FALSE)
				SET_CLIENT_FLAG(playerBD[PARTICIPANT_ID_TO_INT()], MGL_CF_MG_SETUP_PLAYER, FALSE)
				SET_CLIENT_FLAG(playerBD[PARTICIPANT_ID_TO_INT()], MGL_CF_RECEIVED_SETUP, FALSE)
				
				CANCEL_TIMER(stDelaySetupView)
				
				STRUCT_USER_EVENT sEventFinish
					CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "Minigame Send event Finished Canceled: ", playerBD[PARTICIPANT_ID_TO_INT()].iCurrentMinigame)
				sEventFinish.Details.Type = SCRIPT_EVENT_MG_FINISH
				sEventFinish.iMinigameID = playerBD[PARTICIPANT_ID_TO_INT()].iCurrentMinigame
				sEventFinish.iRequestPlayerID = PARTICIPANT_ID_TO_INT()
				EVENT_ALL(sEventFinish)
				
				//player should not be valid as setup player.
				sEventFinish.iMinigameID = 0
				sEventFinish.Details.Type = SCRIPT_EVENT_MG_INTERACT_CHANGED
				EVENT_ALL(sEventFinish)
							
				
				//if outside , but was jsut inside this one , toggle current off.
				playerBD[PARTICIPANT_ID_TO_INT()].iCurrentMinigame = -1
				IF NOT IS_CELLPHONE_DISABLED()
					DISABLE_CELLPHONE(TRUE)
				ENDIF
							
				IF IS_INTERACTION_MENU_DISABLED()
					ENABLE_INTERACTION_MENU()
				ENDIF						
				//IF HAS_ANIM_DICT_LOADED(posInfo.sAnimLib)
				CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "STOP INTERACTION ANIM")	
				//IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), posInfo.sAnimLib, posInfo.sIntoAnim)
					//STOP_ANIM_TASK(PLAYER_PED_ID(), posInfo.sAnimLib, posInfo.sIntoAnim, NORMAL_BLEND_OUT)
				//ELSE
					CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
				//ENDIF
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				RESTART_TIMER_NOW(TimerAfterGame)
				CLEAR_HELP()
				SET_INTERACTION_STAGE(eCurInteractStage, MIS_IDLE)
			BREAK
	ENDSWITCH

	ELSE
		//CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "DO NOT ALLOW UPDATE")
		bHideText = TRUE
	ENDIF
	
	IF NOT bInsideCoords
	AND eCurInteractStage = MIS_IDLE
		//CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "reset as setup")	
		bSetAsSetupPlayer = FALSE
		bReceivedSetupView = FALSE
	ENDIF
	
	IF NOT bInsideCoords
		IF eCurInteractStage > MIS_IDLE
		AND eCurInteractStage <> MIS_CANCEL_READY
			SET_INTERACTION_STAGE(eCurInteractStage, MIS_CANCEL_READY)
		ENDIF
	ENDIF
		
	IF bHideText
		TOGGLE_HELP_TEXT(GET_MG_TEXT(minigame, MGT_IN_PROGRESS), FALSE)
		TOGGLE_HELP_TEXT(GET_MG_TEXT(minigame, MGT_SINGLE_PLAYER), FALSE)
		TOGGLE_HELP_TEXT(GET_MG_TEXT(minigame, MGT_MULTI_PLAYER), FALSE)
		TOGGLE_HELP_TEXT(GET_MG_TEXT(minigame, MGT_MG_AVAILIBLE), FALSE)
		TOGGLE_HELP_TEXT(GET_MG_TEXT(minigame, MGT_WAIT_FOR_PLAYER), FALSE)
		TOGGLE_HELP_TEXT(GET_MG_TEXT(minigame, MGT_WAIT_FOR_LEADER), FALSE)
		TOGGLE_HELP_TEXT(GET_MG_TEXT(minigame, MGT_READY_FOR_GAME), FALSE)
	ENDIF
	
ENDPROC

PROC LOOP_LAUNCH_CHECK(MINIGAME_LAUNCH &minigame[CONST_MINIGAME_COUNT])
	INT mini
	FOR mini = 0 TO CONST_MINIGAME_COUNT - 1
		IF minigame[mini].bRequestLaunch
			MINIGAME_LAUNCH_CHECK(minigame[mini])
		ENDIF
	ENDFOR
ENDPROC

PROC UPDATE_MAD(AML_GENERAL_STRUCT &sState)

	BOOL bAllowInteractUpdate = TRUE
	BOOL bIsWalking = TRUE
	
	//if phone is on screen do not update the activity, below still update to check if an instance of a game needs to start for spectator.
	IF IS_PHONE_ONSCREEN()
	OR IS_PAUSE_MENU_ACTIVE()
	OR IS_INTERACTION_MENU_OPEN() 
	OR IS_BROWSER_OPEN()
		bAllowInteractUpdate = FALSE
		bInteractionActive = TRUE //global
	ELSE
		IF bInteractionActive
			RESTART_TIMER_NOW(TimerAfterGame)
			bInteractionActive = FALSE
		ENDIF
	ENDIF
	//SPEED
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		IF NOT IS_PED_DEAD_OR_DYING(PLAYER_PED_ID())
			bIsWalking =( GET_ENTITY_SPEED(PLAYER_PED_ID()) > 1.9)	
		ENDIF
	ENDIF
	
	//VVV Update Struct
	sState.bAllowIntractUpdate = TRUE
	IF NOT ALLOW_INTERACTION_UPDATE(TimerAfterGame)
	OR NOT bAllowInteractUpdate
	OR bIsWalking
		sState.bAllowIntractUpdate = FALSE
	ENDIF
	
	sState.bPlayerWalking = bIsWalking
	sState.iMGCurrent = playerBD[PARTICIPANT_ID_TO_INT()].iCurrentMinigame
	
	INT iCurrentMinigame
	FOR iCurrentMinigame = 0 TO CONST_MINIGAME_COUNT -1
		sState.bIsMinigameActiveNet[iCurrentMinigame] = NETWORK_IS_SCRIPT_ACTIVE(arrayMinigameInfo[iCurrentMinigame].sScriptName, sState.iApartmentInstance)
		sState.bIsMinigameActiveLocal[iCurrentMinigame] = NETWORK_IS_SCRIPT_ACTIVE(arrayMinigameInfo[iCurrentMinigame].sScriptName, sState.iApartmentInstance, TRUE)
		
		
//		IF (GET_GAME_TIMER() % 1000) < 50
//		AND iCurrentMinigame = 0
//			CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, 
//			"CAn have more?.", IS_BIT_SET(serverBD.iMGCanHaveMorePlayers, iCurrentMinigame),
//			" Has setup?.", IS_BIT_SET(serverBD.iMGHasSetupPlayer, iCurrentMinigame)
//			
//			)	
//		ENDIF
		
		BOOL canHavePlayers = FALSE
		IF IS_BIT_SET(serverBD.iMGCanHaveMorePlayers, iCurrentMinigame)
		AND IS_BIT_SET(serverBD.iMGHasSetupPlayer, iCurrentMinigame)
		AND NOT NETWORK_IS_SCRIPT_ACTIVE(arrayMinigameInfo[iCurrentMinigame].sScriptName,  sState.iApartmentInstance)
			canHavePlayers = TRUE
		ENDIF
		
		
		sState.bCanHaveMorePlayers[iCurrentMinigame] = canHavePlayers
	ENDFOR
ENDPROC


PROC LOOP_MINIGAMES(MAD_STRUCT &sState, AML_GENERAL_STRUCT &sGeneralInfo)

	UPDATE_MAD(sGeneralInfo)
		//CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "SETUP??", IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iBitMaskActivities, MGL_CF_RECEIVED_SETUP))
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		IF NOT IS_PED_DEAD_OR_DYING(PLAYER_PED_ID())
			IF playerBD[PARTICIPANT_ID_TO_INT()].iCurrentMinigame = -1
			AND ALLOW_INTERACTION_UPDATE(TimerAfterGame)
				MINIGAME_AVAILABILITY_DISPALY(sState, sGeneralInfo)
			ENDIF

			IF playerBD[PARTICIPANT_ID_TO_INT()].iCurrentMinigame = -1
				
				INT iCurrentMinigame
				FOR iCurrentMinigame = 0 TO CONST_MINIGAME_COUNT -1	
				CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "Current Check", iCurrentMinigame)
					IF playerBD[PARTICIPANT_ID_TO_INT()].iCurrentMinigame = -1
						UPDATE_MINIGAME(arrayMinigameInfo[iCurrentMinigame], sGeneralInfo.bAllowIntractUpdate)
					ENDIF
				ENDFOR
			ELSE
				UPDATE_MINIGAME(arrayMinigameInfo[sGeneralInfo.iMGCurrent], sGeneralInfo.bAllowIntractUpdate)	
			ENDIF

		ENDIF
	ELSE
		IF (GET_GAME_TIMER() % 1000) < 50
			IF IS_TIMER_STARTED(TimerAfterGame)
				CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "Interaction is not allowed.", GET_TIMER_IN_SECONDS(TimerAfterGame))	
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SERVER_CHECK_ACTIVITY_COUNT()
	INT iMGRequested
	iMGRequested = -1
	INT iMGRequestByUser
	iMGRequestByUser = -1

	//RECEIVE EVENTS FROM CLIENTS
	INT iEvent
	iEvent = 0
	REPEAT GET_NUMBER_OF_EVENTS(SCRIPT_EVENT_QUEUE_NETWORK) iEvent
		SWITCH GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_NETWORK, iEvent)		
			CASE EVENT_NETWORK_SCRIPT_EVENT
				STRUCT_USER_EVENT sEvent
				//sEvent.eEventType = INVALID_EVENT
				IF RECEIVE_EVENT_USER(iEvent, sEvent)
					SWITCH(sEvent.Details.Type)
						CASE SCRIPT_EVENT_MG_FINISH
							CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "Minigame Finished received from client: ", sEvent.iMinigameID)
							IF sEvent.iRequestPlayerID = arrayMinigameSetupUserID[sEvent.iMinigameID]
								IF sEvent.iMinigameID >= 0
									CLEAR_BIT(serverBD.iMGHasSetupPlayer, sEvent.iMinigameID)
								ENDIF
								arrayMinigameSetupUserID[sEvent.iMinigameID] = -1
								CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "Minigame Finished CLEARED: ", sEvent.iMinigameID) 
							ENDIF
						BREAK
						
						CASE SCRIPT_EVENT_MG_LAUNCH
							iMGRequested = sEvent.iMinigameID
							iMGRequestByUser = sEvent.iRequestPlayerID
							CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "SERVER - Minigame START Requested: ",iMGRequested)
						BREAK
						
						CASE SCRIPT_EVENT_MG_INTERACT_CHANGED
							CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "SERVER - Interact Player:", sEvent.iRequestPlayerID, " Start?", sEvent.iMinigameID)
							IF sEvent.iMinigameID = 0
								bArrayPlayerInteract[sEvent.iRequestPlayerID] =  FALSE
							ELSE
								bArrayPlayerInteract[sEvent.iRequestPlayerID] =  TRUE
							ENDIF
							
						BREAK
					ENDSWITCH
				ENDIF
			BREAK
		ENDSWITCH
	ENDREPEAT


	INT iMGID
	INT ParticipantID
	//FIND A SETUP PLAYER.
	FOR ParticipantID = 0 TO NETWORK_GET_MAX_NUM_PARTICIPANTS()-1 STEP 1
		IF NETWORK_IS_PLAYER_ACTIVE(INT_TO_PLAYERINDEX(ParticipantID))	
			iMGID = playerBD[ParticipantID].iCurrentMinigame
//			IF iMGID >=0
//			AND NOT IS_BIT_SET(serverBD.iMGHasSetupPlayer, iMGID)
//				IF GET_GAME_TIMER() % 1000 > 200
//					CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, 
//					"server current MGID:", iMGID, 
//					" user:", ParticipantID, 
//					"has setup?", IS_BIT_SET(serverBD.iMGHasSetupPlayer, iMGID),
//					" is valid?", IS_BIT_SET(playerBD[ParticipantID].iBitMaskActivities, MGL_CF_MG_VALID_PLAYER),
//					" start requested?", IS_BIT_SET(playerBD[ParticipantID].iBitMaskActivities, MGL_CF_MG_START_REQUEST),
//					" Allowed interat?" , bArrayPlayerInteract[ParticipantID]
//					)
//				ENDIF
//			ENDIF
			
			IF iMGID >= 0
			AND iMGID <= CONST_MINIGAME_COUNT -1
				//UP number of players ready.

				IF GET_CLIENT_FLAG(playerBD[ParticipantID], MGL_CF_MG_VALID_PLAYER)
					arrayMinigameUsers[iMGID] ++
								
					IF NOT IS_BIT_SET(serverBD.iMGHasSetupPlayer, iMGID)
					AND NOT NETWORK_IS_SCRIPT_ACTIVE(arrayMinigameInfo[iMGID].sScriptName,  GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iVisibilityID)
						CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "DOES NOT HAVE SETUP PLAYER:", iMGID)
						IF GET_CLIENT_FLAG(playerBD[ParticipantID], MGL_CF_MG_START_REQUEST)
						AND bArrayPlayerInteract[ParticipantID]
						CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "No Setup Player For minigameID:", iMGID)
						
						SET_BIT(serverBD.iMGHasSetupPlayer, iMGID)
						arrayMinigameSetupUserID[iMGID] = ParticipantID
						
						PLAYER_INDEX piParticipant = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(ParticipantID))
						
						STRUCT_USER_EVENT sEvent
						sEvent.Details.Type = SCRIPT_EVENT_MG_LEADER
						sEvent.iRequestPlayerID = ParticipantID
						sEvent.iMinigameID = iMGID
						//tell all valid palyers of curent minigame who is setup who is normal player.
						EVENT_USER(piParticipant, sEvent)
						CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "SERVER - Event Sent Set As Setup:", GET_PLAYER_NAME(piParticipant))
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
		
		
	//MINIGAME LAUNCH CHECK
	FOR iMGID = 0 TO CONST_MINIGAME_COUNT - 1
		
		IF IS_BIT_SET(serverBD.iMGHasSetupPlayer, iMGID)
			IF playerBD[arrayMinigameSetupUserID[iMGID]].iCurrentMinigame <> iMGID
			OR NOT GET_CLIENT_FLAG(playerBD[arrayMinigameSetupUserID[iMGID]], MGL_CF_MG_SETUP_PLAYER)
				CLEAR_BIT(serverBD.iMGHasSetupPlayer, iMGID)
				arrayMinigameSetupUserID[iMGID] = -1
				CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "SERVER - CLEANED UP SETUP PLAYER")
			ENDIF
		ENDIF
		//Check if can have more players for this minigame
		BOOL bCanHaveMorePlayers = arrayMinigameUsers[iMGID] < arrayMinigameInfo[iMGID].iMaxPlayers
		SAFE_BIT_TOGGLE(serverBD.iMGCanHaveMorePlayers, iMGID, bCanHaveMorePlayers)
//		IF GET_GAME_TIMER() % 1000 > 1000
//		AND bCanHaveMorePlayers
//			CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "CAN HAVE MORE PLAYERS<SERVER<", iMGID)
//		ENDIF
		
		BOOL bHasEnoughPlayers = arrayMinigameUsers[iMGID] >= arrayMinigameInfo[iMGID].iMinPlayers
		SAFE_BIT_TOGGLE(serverBD.iMGHasEnoughPlayers, iMGID, bHasEnoughPlayers)
			
		IF iMGRequested >= 0
		AND iMGRequestByUser >= 0
		AND iMGID = iMGRequested
			CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "SERVER MG START - Server Received Request to Start MinigameID: ", iMGRequested)
			//cehck if  minigame has min users ready.
			IF arrayMinigameUsers[iMGID] >= arrayMinigameInfo[iMGID].iMinPlayers
				CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "SERVER MG START - Minigame has minimum players")
				
				IF iMGRequestByUser = arrayMinigameSetupUserID[iMGID]
					CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "SERVER MG START - Correct Setup User")
					
					FOR ParticipantID = 0 TO NETWORK_GET_MAX_NUM_PARTICIPANTS()-1 STEP 1
						IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(ParticipantID))
						
							//IF IS_BIT_SET(playerBD[ParticipantID].iBitMaskActivities, MGL_CF_MG_VALID_PLAYER)
								PLAYER_INDEX piParticipant = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(ParticipantID))		
	
								BROADCAST_LAUNCH_STRUCT sLaunchInfo
								sLaunchInfo.Details.Type = SCRIPT_EVENT_MG_LAUNCH_USER
								sLaunchInfo.iScriptID = iMGID
								sLaunchInfo.iPlayerIndex = ParticipantID
								sLaunchInfo.bIsSpectator = FALSE
								
								IF 	GET_CLIENT_FLAG(playerBD[ParticipantID], MGL_CF_MG_VALID_PLAYER)
								AND playerBD[ParticipantID].iCurrentMinigame = iMGID
									CDEBUG1LN(DEBUG_MG_LAUNCHER_APART,"SERVER MG START - Script Launch request as Normal for Player: ", GET_PLAYER_NAME(piParticipant))
									BROADCAST_MINIGAME_LAUNCH(piParticipant, sLaunchInfo)
								ELSE
									IF iMGID <> ENUM_TO_INT(eMT_ARMWRESTLE) //temp do not make spec more for armwrestling.
										CDEBUG1LN(DEBUG_MG_LAUNCHER_APART,"SERVER MG START - Script Launch request as Spec for Player: ", GET_PLAYER_NAME(piParticipant))
										sLaunchInfo.bIsSpectator = TRUE
										BROADCAST_MINIGAME_LAUNCH(piParticipant, sLaunchInfo)
									ENDIF
								ENDIF
							//ENDIF
						ENDIF
					ENDFOR
				ENDIF
			ENDIF
		ENDIF
		arrayMinigameUsers[iMGID] = 0	
	ENDFOR
ENDPROC

PROC CLEAR_USER_MINIGAME_BITS(PlayerBroadcastData& PBD)
	CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "PLAYER BITS CLEARED!")
	SET_CLIENT_FLAG(PBD, MGL_CF_MG_SETUP_PLAYER, FALSE)
	SET_CLIENT_FLAG(PBD, MGL_CF_MG_VALID_PLAYER, FALSE)
	SET_CLIENT_FLAG(PBD, MGL_CF_MG_START_REQUEST, FALSE)
ENDPROC

//PURPOSE: Process the stages for the ClientFFUNC
PROC PROCESS_CLIENT(AML_GENERAL_STRUCT &sCurrentV, MAD_STRUCT &sStateN, MINIGAME_LAUNCH &minigame[CONST_MINIGAME_COUNT])

	IF NOT IS_PED_DEAD_OR_DYING(PLAYER_PED_ID())
		IF IS_PLAYER_EXITING_PROPERTY(GET_PLAYER_INDEX())
			SWITCH_LAUNCHER_STAGE(playerBD[PARTICIPANT_ID_TO_INT()].eStage, eAD_PRE_CLEANUP)
		ENDIF
	ENDIF
	IF NOT bAllowPropertyCheck
		IF NOT IS_TIMER_STARTED(TimerPropCheck)
			CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "Started timer, before check if inside property")
			START_TIMER_NOW(TimerPropCheck)
		ELIF GET_TIMER_IN_SECONDS(TimerPropCheck) > 10.0
			CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "Now checking started if inside property ")
			bAllowPropertyCheck = TRUE
		ENDIF				
	ENDIF
		
	IF bAllowPropertyCheck
		IF NOT IS_PLAYER_ENTERING_PROPERTY(GET_PLAYER_INDEX())
			//check if script sohuld terminate - if outside an apartment
			IF NOT IS_PLAYER_IN_MP_PROPERTY(GET_PLAYER_INDEX(), FALSE)
			AND NOT IS_PLAYER_IN_CLUBHOUSE(GET_PLAYER_INDEX())
			AND NOT IS_PLAYER_IN_CUTSCENE(GET_PLAYER_INDEX())
			AND playerBD[PARTICIPANT_ID_TO_INT()].eStage < eAD_CLEANUP
				CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "NOT INSIDE AN APARTMENT TERMINATED TERMINATED TERMINATED")
				SWITCH_LAUNCHER_STAGE(playerBD[PARTICIPANT_ID_TO_INT()].eStage, eAD_CLEANUP)
			ENDIF
		ENDIF
	ENDIF
	
	SWITCH playerBD[PARTICIPANT_ID_TO_INT()].eStage		
		CASE eAD_IDLE
			INT iCount
			iCount = 0
			BROADCAST_LAUNCH_STRUCT sLaunchInfo
			sLaunchInfo.iScriptID = -1
			
			REPEAT GET_NUMBER_OF_EVENTS(SCRIPT_EVENT_QUEUE_NETWORK) iCount
				SWITCH GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_NETWORK, iCount)		
					CASE EVENT_NETWORK_SCRIPT_EVENT
						STRUCT_USER_EVENT sEvent
						IF RECEIVE_EVENT_USER(iCount, sEvent)
							SWITCH(sEvent.Details.Type)
								CASE SCRIPT_EVENT_MG_LEADER
									IF sEvent.iMinigameID = sCurrentV.iMGCurrent
									AND sEvent.iRequestPlayerID = PARTICIPANT_ID_TO_INT()
										bReceivedSetupView = TRUE
										bSetAsSetupPlayer = TRUE
										CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "RECEIVED SET AS SETUP", sEvent.iRequestPlayerID ) 
									ENDIF
								BREAK
							ENDSWITCH
						ENDIF

						IF PROCESS_MINIGAME_LAUNCH(iCount, sLaunchInfo)
							CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "[MINIGAME LAUNCHER] - Server requested scritp Launch ID", sLaunchInfo.iScriptID, 
							" As Spec?", sLaunchInfo.bIsSpectator, 
							" for user",  sLaunchInfo.iPlayerIndex, " self:",  PARTICIPANT_ID_TO_INT())
							IF sLaunchInfo.iPlayerIndex = PARTICIPANT_ID_TO_INT()
								IF sLaunchInfo.iScriptID >= 0
								AND sLaunchInfo.iScriptID <= CONST_MINIGAME_COUNT - 1
									IF sLaunchInfo.bIsSpectator	
										MINIGAME_LAUNCH_REQUEST(minigame, sLaunchInfo.iScriptID)
									ELSE
										SWITCH_LAUNCHER_STAGE(playerBD[PARTICIPANT_ID_TO_INT()].eStage, eAD_LOAD_MINIGAME)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					BREAK
				ENDSWITCH
			ENDREPEAT

			LOOP_MINIGAMES(sStateN, sCurrentV)
			LOOP_LAUNCH_CHECK(minigame)
		BREAK	
		
		CASE eAD_LOAD_MINIGAME
			IF playerBD[PARTICIPANT_ID_TO_INT()].iCurrentMinigame >= 0
				STRING	ScriptName
				ScriptName =  arrayMinigameInfo[playerBD[PARTICIPANT_ID_TO_INT()].iCurrentMinigame].sScriptName
				
				IF HAS_SCRIPT_LOADED(ScriptName)
					CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "Script Loaded: ", ScriptName)	
					SWITCH_LAUNCHER_STAGE(playerBD[PARTICIPANT_ID_TO_INT()].eStage, eAD_START_MINIGAME)
				ELSE
					CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "Script not loaded yet: ", ScriptName)
					IF DOES_SCRIPT_EXIST(ScriptName)
						Request_Load_Scriptstring(ScriptName)
					ELSE
						CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "Script does not exist", ScriptName)
					ENDIF
				ENDIF
			ELSE
				CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "LOAD_MINIGAME_STAGE: invalid minigame ID: ", playerBD[PARTICIPANT_ID_TO_INT()].iCurrentMinigame)	
				SWITCH_LAUNCHER_STAGE(playerBD[PARTICIPANT_ID_TO_INT()].eStage, eAD_IDLE)
			ENDIF
		BREAK
		
		CASE eAD_START_MINIGAME	
		
		BOOL bIsSetupPlayer
		bIsSetupPlayer = GET_CLIENT_FLAG(playerBD[PARTICIPANT_ID_TO_INT()], MGL_CF_MG_SETUP_PLAYER)
		BOOL bAllowScriptLaunch 
		bAllowScriptLaunch = TRUE
		
			//current minigame selectd by the player
			INT iCurrentMinigame
			iCurrentMinigame = playerBD[PARTICIPANT_ID_TO_INT()].iCurrentMinigame
			//get minigame info
			MINIGAME_TYPE_INFO minigameInfo
			minigameInfo  =  arrayMinigameInfo[iCurrentMinigame]
			//minigame info feed
			STRING	sScriptName 
			sScriptName = minigameInfo.sScriptName
			MP_MISSION_DATA fmmcMissionData
			fmmcMissionData.iInstanceId = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iVisibilityID
			iCurrentMinigameInstance = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iVisibilityID
			fmmcMissionData.mdPrimaryCoords =  <<1107.51, -3156.73, -36.0578>>
			fmmcMissionData.mdSecondaryCoords =  <<1107.51, -3156.73, -36.0578>>
		
		IF NOT bIsSetupPlayer
		AND NOT NETWORK_IS_SCRIPT_ACTIVE(sScriptName, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iVisibilityID, FALSE)	
			bAllowScriptLaunch = FALSE
			CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "Script Cant Launch, no instance active")
		ENDIF
		
		IF bAllowScriptLaunch
			CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "START MINIGAME: ", sScriptName)
			IF playerBD[PARTICIPANT_ID_TO_INT()].iCurrentMinigame = 0
				CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "GLOBAL LEGS: ", g_FMMC_STRUCT.iNumberOfLegs, " SETS:", g_FMMC_STRUCT.iNumberOfSets)
				CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "LOCAL LEGS: ", iArrayDartNumOfLegs[iCurrentLegs].iValue, " SETS:", iArrayDartNumOfSets[iCurrentSets].iValue)
			ENDIF
				//Start Minigame on this client
				IF START_CURRENT_MINIGAME(sScriptName, SCRIPT_XML_STACK_SIZE, fmmcMissionData, FALSE, bIsSetupPlayer)
		
					
					CANCEL_TIMER(TimerAfterGame)
					//RESTART_TIMER_NOW(TimerAfterGame)
					SWITCH_LAUNCHER_STAGE(playerBD[PARTICIPANT_ID_TO_INT()].eStage, eAD_WAIT_END_MINIGAME)
					CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "Switching to wait for the minigame instance to finish")
						SET_CLIENT_FLAG(playerBD[PARTICIPANT_ID_TO_INT()], MGL_CF_MG_START_REQUEST, FALSE)
						SET_CLIENT_FLAG(playerBD[PARTICIPANT_ID_TO_INT()], MGL_CF_RECEIVED_SETUP, FALSE)
				ENDIF
		ENDIF
		BREAK

		CASE eAD_WAIT_END_MINIGAME
			iCurrentMinigame = playerBD[PARTICIPANT_ID_TO_INT()].iCurrentMinigame
			minigameInfo  =  arrayMinigameInfo[iCurrentMinigame]
			sScriptName = minigameInfo.sScriptName
			

			IF NOT NETWORK_IS_SCRIPT_ACTIVE(sScriptName, iCurrentMinigameInstance, TRUE)

			
				//IF NOT IS_TIMER_STARTED(TimerAfterGame)
				///OR IS_TIMER_PAUSED(TimerAfterGame)
				//	START_TIMER_NOW(TimerAfterGame)
				//	CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "After Game Delay Timer Started, waiting: ", GET_TIMER_IN_SECONDS(TimerAfterGame))
				//ELIF GET_TIMER_IN_SECONDS(TimerAfterGame) >= CONST_AFTER_GAME_DELAY
						SET_CLIENT_FLAG(playerBD[PARTICIPANT_ID_TO_INT()], MGL_CF_MG_START_REQUEST, FALSE)
						CLEAR_USER_MINIGAME_BITS(playerBD[PARTICIPANT_ID_TO_INT()])	
						
						STRUCT_USER_EVENT sEventFinish
						CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "Minigame Send event Finished: ", playerBD[PARTICIPANT_ID_TO_INT()].iCurrentMinigame)
						sEventFinish.iMinigameID = playerBD[PARTICIPANT_ID_TO_INT()].iCurrentMinigame
						//sEventFinish.eEventType = FINISHED_MINIGAME
						sEventFinish.Details.Type = SCRIPT_EVENT_MG_FINISH
						EVENT_ALL(sEventFinish)
						
						//cancel active
						sEventFinish.iMinigameID = 0
						sEventFinish.Details.Type = SCRIPT_EVENT_MG_INTERACT_CHANGED
						EVENT_ALL(sEventFinish)
					
					CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "Minigame script is no longer active: ", sScriptName)
					IF IS_CELLPHONE_DISABLED()
						DISABLE_CELLPHONE(FALSE)
					ENDIF
					IF IS_INTERACTION_MENU_DISABLED()
						ENABLE_INTERACTION_MENU()
					ENDIF
					
					playerBD[PARTICIPANT_ID_TO_INT()].iCurrentMinigame = -1
					RESTART_TIMER_NOW(TimerAfterGame)
					SET_INTERACTION_STAGE(eCurInteractStage, MIS_IDLE)
					SWITCH_LAUNCHER_STAGE(playerBD[PARTICIPANT_ID_TO_INT()].eStage, eAD_IDLE)
				//ENDIF
			ENDIF
		BREAK	
		
		CASE eAD_PRE_CLEANUP
			//run once and wait for player to exit aparmtnet, state will switch to cleanup 
			IF NOT bPreCleanup
				bPreCleanup = TRUE
				CLEAR_HELP()
				CLEAR_MENU_DATA()
			ENDIF
		BREAK
		
		CASE eAD_CLEANUP
			SCRIPT_CLEANUP()
		BREAK
	ENDSWITCH

ENDPROC

//PURPOSE: Process the JOYRIDER stages for the Server
PROC PROCESS_SERVER()
	//CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "Legs Num: ",g_FMMC_STRUCT.iNumberOfLegs, " Sets Num: ", g_FMMC_STRUCT.iNumberOfSets)
	//Add client processing loop here!
	SERVER_CHECK_ACTIVITY_COUNT()
	//Mission Stages
	SWITCH serverBD.eStage	
		CASE eAD_IDLE
		BREAK
	
		CASE eAD_WAIT_END_MINIGAME			
		BREAK
						
		CASE eAD_CLEANUP			
		BREAK
	ENDSWITCH
ENDPROC


//Helper function to get the servers game/mission state
FUNC INT GET_SERVER_MISSION_STATE()
	RETURN serverBD.iServerGameState
ENDFUNC

//Server only function that checks to see if the game mode should now end. For this test script, it if time has passed.
FUNC BOOL HAVE_MISSION_END_CONDITIONS_BEEN_MET()
	RETURN FALSE
ENDFUNC
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                      Main Loop                      //////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
SCRIPT
	#IF IS_DEBUG_BUILD
		ADD_AM_DART_WIDGETS()
	#ENDIF
	SET_BIT(iAvailibleMinigamesFlag,  ENUM_TO_INT(eMT_ARMWRESTLE) |  ENUM_TO_INT(eMT_DARTS))
	MINIGAME_LAUNCH_CHECK_RESET(sMinigameLaunchTimer)
	
	BOOL bInsideProperty
	INT iCurrentProperty
	iCurrentProperty = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty

	CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "Property: ", iCurrentProperty)
	CDEBUG1LN(DEBUG_MG_LAUNCHER_APART,"")
	CDEBUG1LN(DEBUG_MG_LAUNCHER_APART,"")
	CDEBUG1LN(DEBUG_MG_LAUNCHER_APART,"%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
	CDEBUG1LN(DEBUG_MG_LAUNCHER_APART,"%%%%%%%%%% MINIGAME LAUNCHER SCRIPT STARTED %%%%%%%%")
	//preload menu assets
	LOAD_MENU_ASSETS()
	SETUP_DART_MENU(mriDartsMenu)
	
	//default values for menu 
	iCurrentMenuRow = 0
	
	SETUP_MENU_DARTS_INFO(iArrayDartNumOfLegs, iArrayDartNumOfSets)
				
	CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "Start wait, until palyer is inside apartment.")
	WHILE NOT bInsideProperty
	WAIT(0)
		IF NOT IS_PLAYER_ENTERING_PROPERTY(GET_PLAYER_INDEX())
		AND IS_PLAYER_IN_MP_PROPERTY(GET_PLAYER_INDEX(), FALSE)
			CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "Player Is Inside The Property: ", iCurrentProperty)
			IF IS_PLAYER_IN_CLUBHOUSE(GET_PLAYER_INDEX())
				CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "Player Is Inside The Clubhouse Property: ", iCurrentProperty)
				bInsideProperty = TRUE
			ENDIF
		ENDIF	
	ENDWHILE
	
	sCurrentValues.iApartmentInstance = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iVisibilityID
	
	CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "Property: ", iCurrentProperty)
	IF IS_PROPERTY_CLUBHOUSE(iCurrentProperty, PROPERTY_CLUBHOUSE_1_BASE_A)
		atCurrentApartment = BIKER_VAR_A
		CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "Biker Apartment type A")
	ELIF IS_PROPERTY_CLUBHOUSE(iCurrentProperty, PROPERTY_CLUBHOUSE_7_BASE_B)
		atCurrentApartment = BIKER_VAR_B
		CDEBUG1LN(DEBUG_MG_LAUNCHER_APART, "Biker Apartment type B")
	ENDIF
	
	SETUP_MINIGAME_TYPES(arrayMinigameInfo, atCurrentApartment)
	MINIGAME_LAUNCH_INITIALIZE(sMinigameLaunchTimer, arrayMinigameInfo)
		
	//default darts values
	g_FMMC_STRUCT.iNumberOfLegs = iArrayDartNumOfLegs[iCurrentLegs].iValue
	g_FMMC_STRUCT.iNumberOfSets = iArrayDartNumOfSets[iCurrentSets].iValue
	
	//##### Start Check to see if a minigame is already started.
	INT iMGID
	FOR iMGID = 0 TO CONST_MINIGAME_COUNT -1
		IF iMGID = ENUM_TO_INT(eMT_DARTS)
			MINIGAME_LAUNCH_REQUEST(sMinigameLaunchTimer, iMGID, 3.0)
		ENDIF
	ENDFOR
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		PRINTLN("MP: Starting mission ")
		
		// Vehry out all the initial game starting duties. 
		PROCESS_PRE_GAME()	
	ELSE
		PRINTLN("[MPTUT] - MISSION END - NETWORK_IS_GAME_IN_PROGRESS = FALSE - SCRIPT CLEANUP P ")
		SCRIPT_CLEANUP()
	ENDIF
	
	// Main loop
	WHILE TRUE

		// One wait to rule them all. This can be the ONLY wait from here on in.....
		WAIT(0)	
		//MP_LOOP_WAIT_ZERO()
		// If we have a match end event, bail
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			PRINTLN("[MPTUT] - MISSION END - SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE - SCRIPT CLEANUP B ")
			SCRIPT_CLEANUP()
		ENDIF
		
		//Check if player is in a tutorial session and end


		// -----------------------------------
		// Process your game logic.....
		SWITCH playerBD[PARTICIPANT_ID_TO_INT()].iGameState
			
			// Wait untill the server gives the all go before moving on
			CASE GAME_STATE_INI
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING					
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_RUNNING
					PRINTLN("[MPTUT] - playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_RUNNING")
				ELIF GET_SERVER_MISSION_STATE() = GAME_STATE_END
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY
					PRINTLN("[MPTUT] - playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY 1")
				ENDIF
			BREAK
			
			// Main gameplay state.
			CASE GAME_STATE_RUNNING
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING
					PROCESS_CLIENT(sCurrentValues, sStateNow, sMinigameLaunchTimer)
				ELIF GET_SERVER_MISSION_STATE() = GAME_STATE_END
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY
					PRINTLN("[MPTUT] - playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY 2")
				ENDIF
			BREAK
			
			CASE GAME_STATE_TERMINATE_DELAY 
		      	IF IS_MISSION_READY_TO_CLEANUP(serverBD.MissionTerminateDelayTimer)
		            playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END
					PRINTLN("[MPTUT] - playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END 3")
		      	ENDIF
			BREAK
			
			// The game stage the local player is placed when we want him to leave a mission
			CASE GAME_STATE_LEAVE
				playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END
				PRINTLN("[MPTUT] - playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END 2")
			FALLTHRU
			
			//Cleans up then terminates the mission
			CASE GAME_STATE_END
				PRINTLN("[MPTUT] - SCRIPT CLEANUP A ")
				SCRIPT_CLEANUP()
			BREAK

		ENDSWITCH
		
		// -----------------------------------
		// Process server game logic		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			
			SWITCH GET_SERVER_MISSION_STATE()
				
				// For CnC group missions, we may want to wait for all players to have launched the script
				CASE GAME_STATE_INI
					serverBD.iServerGameState = GAME_STATE_RUNNING
					PRINTLN("[MPTUT] - serverBD.iServerGameState = GAME_STATE_RUNNING")
				BREAK
				
				// Look for game end conditions
				CASE GAME_STATE_RUNNING
					
					PROCESS_SERVER()
					
					IF HAVE_MISSION_END_CONDITIONS_BEEN_MET()
						serverBD.iServerGameState = GAME_STATE_END
						PRINTLN("[MPTUT] - serverBD.iServerGameState = GAME_STATE_END 4")
					ENDIF
					
				BREAK
				
				CASE GAME_STATE_END
				
				BREAK
				
			ENDSWITCH
			
		ENDIF
		
//		#IF IS_DEBUG_BUILD
//		MAINTAIN_DEBUG()
//		#ENDIF
	ENDWHILE

ENDSCRIPT
