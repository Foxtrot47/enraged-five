//////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Name:        Roulette.sch																				
/// Description: Script for running casino roulette tables, this should be included at the end of a roulette 
///																											
/// Written by:  Online Technical Team: Tom Turner,															
/// Date:  		12/02/2019																					
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "Globals.sch"
USING "freemode_header.sch"

USING "RouletteData.sch"
USING "RouletteHelper.sch"
USING "RouletteClient.sch"
USING "RouletteServer.sch"

#IF IS_DEBUG_BUILD
USING "RouletteDebug.sch"
ROULETTE_DEBUG m_sRouletteDebug
#ENDIF

ROULETTE_DATA m_sRouletteData

/// PURPOSE:
///    Cleans up the script
PROC SCRIPT_CLEANUP()
	CPRINTLN(DEBUG_ROULETTE, "ROUETTE_SCRIPT_CLEANUP - Called")
	ROULETTE_CLIENT_CLEANUP(m_sRouletteData)
	ROULETTE_SERVER_CLEANUP(m_sRouletteData)
	
	#IF IS_DEBUG_BUILD
		ROULETTE_DEBUG_CLEAN_UP()
	#ENDIF	
	
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC

/// PURPOSE:
///    Cleans up script if required
FUNC BOOL ROULETTE_MAINTAIN_SCRIPT_CLEANUP_CHECKS()
	IF g_bCleanupRoulette
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_MAINTAIN_SCRIPT_CLEANUP_CHECKS - Cleaning up script: g_bCleanupRoulette set")
		g_bCleanupRoulette = FALSE
		SCRIPT_CLEANUP()
		RETURN TRUE
	ENDIF
	
	IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_MAINTAIN_SCRIPT_CLEANUP_CHECKS - Cleaning up script: Thread should terminate")
		SCRIPT_CLEANUP()
		RETURN TRUE
	ENDIF
	
	IF g_SpawnData.bPassedOutDrunk
	AND INT_TO_ENUM(PLAYER_SPAWN_ACTIVITY, g_TransitionSpawnData.iSpawnActivity) != SPAWN_ACTIVITY_NOTHING
		IF INT_TO_ENUM(PLAYER_SPAWN_ACTIVITY, g_TransitionSpawnData.iSpawnActivity) != SPAWN_ACTIVITY_TOILET_SPEW
			CPRINTLN(DEBUG_ROULETTE, "ROULETTE_MAINTAIN_SCRIPT_CLEANUP_CHECKS - Cleaning up script: g_SpawnData.bPassedOutDrunk")
			SCRIPT_CLEANUP()
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_PLAYER_AUTOWARP_BETWEEN_TWO_INTERIORS()
	AND IS_SCREEN_FADED_OUT()
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_MAINTAIN_SCRIPT_CLEANUP_CHECKS - Cleaning up script: IS_PLAYER_AUTOWARP_BETWEEN_TWO_INTERIORS")
		SCRIPT_CLEANUP()
		RETURN TRUE
	ENDIF
	
	IF ROULETTE_CUSTOM_CLEANUP_CHECKS()
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_MAINTAIN_SCRIPT_CLEANUP_CHECKS - Cleaning up script: ROULETTE_CUSTOM_CLEANUP_CHECKS")
		SCRIPT_CLEANUP()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Processes anything that needs to happen pre game
PROC ROULETTE_PROCESS_PRE_GAME()		
	#IF IS_DEBUG_BUILD
		ROULETTE_DEBUG_INIT()
	#ENDIF
	
	// Reset clean up when starting new instance
	IF g_bCleanupRoulette
		PRINTLN("ROULETTE_PROCESS_PRE_GAME - Setting g_bCleanupRoulette to false on start")
		g_bCleanupRoulette = FALSE
	ENDIF
	
	NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE)
	HANDLE_NET_SCRIPT_INITIALISATION()
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(m_sRouletteData.serverBD, SIZE_OF(m_sRouletteData.serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(m_sRouletteData.playerBD, SIZE_OF(m_sRouletteData.playerBD))	
	
	IF NOT Wait_For_First_Network_Broadcast()
		PRINTLN("ROULETTE_PROCESS_PRE_GAME - Failed to receive initial network broadcast - script cleanup called")
		SCRIPT_CLEANUP()
	ENDIF
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		ROULETTE_SERVER_PRE_GAME_INIT(m_sRouletteData)
	ENDIF
	
	ROULETTE_CLIENT_PRE_GAME_INIT(m_sRouletteData)
		
	#IF IS_DEBUG_BUILD
		ROULETTE_CREATE_DEBUG_WIDGETS(m_sRouletteDebug)
	#ENDIF
	
	/// If transitioning between interiors wait for transition to end
	/// or until clean up is called
	WHILE (IS_PLAYER_AUTOWARP_BETWEEN_TWO_INTERIORS()
	AND IS_SCREEN_FADED_OUT()) 
		
		IF g_bCleanupRoulette
			CPRINTLN(DEBUG_ROULETTE, "ROULETTE_PROCESS_PRE_GAME - Cleaning up script: g_bCleanupRoulette set while waiting for interior transition")
			g_bCleanupRoulette = FALSE
			SCRIPT_CLEANUP()
			EXIT
		ENDIF
	
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			CPRINTLN(DEBUG_ROULETTE, "ROULETTE_PROCESS_PRE_GAME - Cleaning up script: Thread should terminate while waiting for interior transition")
			SCRIPT_CLEANUP()
			EXIT
		ENDIF
		
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_PROCESS_PRE_GAME - Waiting for interior transition to end")	
		WAIT(0)
	ENDWHILE
	
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_PROCESS_PRE_GAME - Processed pre-game")	
ENDPROC

/// PURPOSE:
///    Main update for the roulette script
PROC ROULETTE_SCRIPT_UPDATE()
	IF ROULETTE_MAINTAIN_SCRIPT_CLEANUP_CHECKS()
		EXIT
	ENDIF
	
	ROULETTE_CLIENT_UPDATE(m_sRouletteData)	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		ROULETTE_SERVER_UPDATE(m_sRouletteData)
	ENDIF
	
	#IF IS_DEBUG_BUILD
	ROULETTE_DEBUG_UPDATE(m_sRouletteDebug, m_sRouletteData)
	#ENDIF	
ENDPROC

///---------------------------
///    Main entry point
///---------------------------    

/// PURPOSE:
///    The Script loop
SCRIPT
	ROULETTE_PROCESS_PRE_GAME()
	
	WHILE TRUE
		WAIT(0)
		ROULETTE_SCRIPT_UPDATE()		
	ENDWHILE
ENDSCRIPT
