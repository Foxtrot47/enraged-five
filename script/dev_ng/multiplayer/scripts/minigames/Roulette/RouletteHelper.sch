//////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Name:        RouletteHelper.sch																			
/// Description: Helpers for a game of roulette																
///																											
/// Written by:  Online Technical Team: Tom Turner,															
/// Date:  		12/02/2019																																																																																	
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "Globals.sch"
USING "freemode_header.sch"
USING "RouletteData.sch"
USING "RouletteConstants.sch"

USING "RoulettePropsOverrides.sch"

///--------------------------
///    General use helpers
///--------------------------

/// PURPOSE:
///    Wraps an index between a min and max value   
FUNC INT WRAP_INDEX_BETWEEN_RANGE(INT iIndex, INT iMinIndex, INT iMaxIndex)
	IF iIndex < iMinIndex
		RETURN iMaxIndex
	ENDIF
	
	IF iIndex > iMaxIndex
		RETURN iMinIndex
	ENDIF

	RETURN iIndex
ENDFUNC

/// PURPOSE:
///    Wraps an index between 0 and the max value 
FUNC INT WRAP_INDEX(INT iIndex, INT iMaxIndex)
	RETURN ((iIndex % iMaxIndex) + iMaxIndex) % iMaxIndex
ENDFUNC

/// PURPOSE:
///    Wraps a float between 0 and max value
FUNC FLOAT WRAP_FLOAT(FLOAT iValue, FLOAT iMaxValue)
	RETURN ((iValue % iMaxValue) + iMaxValue) % iMaxValue
ENDFUNC

/// PURPOSE:
///     Checks if the given integer is in the given range (inclusive)  
FUNC BOOL IS_INT_IN_RANGE_INCLUSIVE(INT iValueToCheck, INT iRangeMin, INT iRangeMax)
	RETURN iValueToCheck >= iRangeMin AND iValueToCheck <= iRangeMax
ENDFUNC

/// PURPOSE:
///    Checks if the array of integers contains the given value   
FUNC BOOL DOES_INT_ARRAY_CONTAIN(INT &iArray[], INT iValueToFind)
	INT i = 0
	INT iArrayCount = COUNT_OF(iArray)
	REPEAT iArrayCount i
		IF iArray[i] = iValueToFind
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Maps a value from one range to another   
FUNC FLOAT MAP_VALUE(FLOAT fValueToMap, FLOAT fFromSource, FLOAT fToSource, FLOAT fFromTarget, FLOAT fToTarget)
	 RETURN (fValueToMap - fFromSource) / (fToSource - fFromSource) * (fToTarget - fFromTarget) + fFromTarget
ENDFUNC

/// PURPOSE:
///    Sets every value in an int array
PROC SET_ALL_IN_INT_ARRAY(INT &iArray[], INT iValueToSet)
	INT i = 0
	INT iMaxIndex = COUNT_OF(iArray)	
	REPEAT iMaxIndex i
		iArray[i] = iValueToSet	
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Copies an array of integers from one to another
PROC COPY_INT_ARRAY(INT &iDestination[], INT &iSource[])
	
	IF COUNT_OF(iDestination) != COUNT_OF(iSource)
		PRINTLN("COPY_INT_ARRAY - Incompatible lengths!")
		EXIT
	ENDIF
	
	INT i = 0
	INT iMaxIndex = COUNT_OF(iDestination)
	REPEAT iMaxIndex i
		iDestination[i] = iSource[i]
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Checks if a vector is on the right side of another vector  
FUNC BOOL IS_RIGHT_OF_VECTOR(VECTOR vVectorToCheck, VECTOR vVectorToCheckAgainst)
	// Get vector pointing right
	VECTOR vRight = CROSS_PRODUCT(vVectorToCheckAgainst, <<0, 0, 1>>)
	
	// If negative vector is on the right side
	RETURN DOT_PRODUCT(vRight, vVectorToCheck) > 0
ENDFUNC


///--------------------------
///    State helpers
///--------------------------    

/// PURPOSE:
///    Function defininition for passing roulette data, used for on entry and on exit
///    when transitioning between roulette states
TYPEDEF PROC ROULETTE_TRANSITION_FUNC(ROULETTE_DATA &sRouletteData)

/// PURPOSE:
///    Default on enter for states, for when an on enter is not needed
PROC ROULETTE_DEFAULT_ON_ENTER(ROULETTE_DATA &sRouletteData)
	UNUSED_PARAMETER(sRouletteData)
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_DEFAULT_ON_ENTER - Entered")
ENDPROC

/// PURPOSE:
///    Default on exit for states, for when an on exit is not needed
PROC ROULETTE_DEFAULT_ON_EXIT(ROULETTE_DATA &sRouletteData)
	UNUSED_PARAMETER(sRouletteData)
  	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_DEFAULT_ON_EXIT - Exited")
ENDPROC

/// PURPOSE:
///    Gets the local players total bet for the current round of roulette they are playing   
FUNC INT ROULETTE_HELPER_GET_LOCAL_PLAYERS_TOTAL_BET_FOR_CURRENT_ROUND(ROULETTE_DATA &sRouletteData)
	RETURN sRouletteData.iTotalInsideBetThisRound + sRouletteData.iTotalOutsideBetThisRound
ENDFUNC

#IF IS_DEBUG_BUILD
PROC ROULETTE_HELPER_DEBUG_OVERRIDE_WINNING_NUMBER_INDEX_FOR_CLIENT(ROULETTE_DATA &sRouletteData, INT &iWinningNumber)
	IF sRouletteData.iDebugWinningNumberOverride != -1 
		iWinningNumber = sRouletteData.iDebugWinningNumberOverride
	ENDIF
ENDPROC
#ENDIF

FUNC INT ROULETTE_HELPER_GET_SERVERS_CURRENT_WINNING_NUMBER_FOR_TABLE(ROULETTE_DATA &sRouletteData, INT iTableID)
	INT iWinningSlotIndex = sRouletteData.serverBD.iSpinResult[iTableID]
	#IF IS_DEBUG_BUILD
		ROULETTE_HELPER_DEBUG_OVERRIDE_WINNING_NUMBER_INDEX_FOR_CLIENT(sRouletteData, iWinningSlotIndex)
	#ENDIF
	RETURN iWinningSlotIndex
ENDFUNC

FUNC INT ROULETTE_HELPER_GET_TABLES_BETTING_ROUND_DURATION(ROULETTE_DATA &sRouletteData, INT iTableID)
	RETURN sRouletteData.serverBD.iCurrentBetRoundDuration[iTableID]
ENDFUNC

///--------------------------
///    ID helpers
///--------------------------

/// PURPOSE:
///    Gets the table id the seat is currently at  
FUNC INT ROULETTE_HELPER_GET_TABLE_ID_FROM_SEAT_ID(INT iSeatId)
	FLOAT fConvert = TO_FLOAT(iSeatId) / ROULETTE_TABLE_MAX_NUMBER_OF_PLAYERS_PER_TABLE 
	RETURN FLOOR(fConvert)
ENDFUNC

/// PURPOSE:
///    Gets the local seat id for the table, i.e a value of 0 - max players at a table    
FUNC INT ROULETTE_HELPER_GET_LOCAL_SEAT_ID_FROM_SEAT_ID(INT iSeatId)
	RETURN WRAP_INDEX(iSeatId, ROULETTE_TABLE_MAX_NUMBER_OF_PLAYERS_PER_TABLE)
ENDFUNC

/// PURPOSE:
///    Gets the global seat ID from the local seat ID on the given table   
FUNC INT ROULETTE_HELPER_GET_SEAT_ID_FROM_LOCAL_SEAT_ID_AT_TABLE(INT iLocalSeatID, INT iTableID)
	RETURN (iTableID * ROULETTE_TABLE_MAX_NUMBER_OF_PLAYERS_PER_TABLE) + iLocalSeatID
ENDFUNC

/// PURPOSE:
///    Gets the local ID of the roulette seat the local player is using
FUNC INT ROULETTE_HELPER_GET_LOCAL_PLAYERS_LOCAL_SEAT_ID(ROULETTE_DATA &sRouletteData)
	RETURN ROULETTE_HELPER_GET_LOCAL_SEAT_ID_FROM_SEAT_ID(sRouletteData.iSeatID)
ENDFUNC

///----------------------------
///    Player helpers
///----------------------------  

/// PURPOSE:
///    Checks if the given table ID is valid  
FUNC BOOL ROULETTE_TABLE_IS_TABLE_ID_VALID(INT iTableID)	
	IF iTableID < 0
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_TABLE_IS_TABLE_ID_VALID - tableID invalid")
		DEBUG_PRINTCALLSTACK()
		RETURN FALSE
	ENDIF
	
	IF iTableID >= ROULETTE_TABLE_MAX_NUMBER_OF_TABLES
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_TABLE_IS_TABLE_ID_VALID - tableID invalid")
		DEBUG_PRINTCALLSTACK()
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Checks if the global seat id is valid   
FUNC BOOL ROULETTE_TABLE_IS_SEAT_ID_VALID(INT iSeatID)
	RETURN iSeatID > -1 AND iSeatID < ROULETTE_MAX_SEAT_ID
ENDFUNC

/// PURPOSE:
///    Checks if the players current roulette table id is valid   
FUNC BOOL ROULETTE_TABLE_IS_PLAYERS_TABLE_ID_VALID(ROULETTE_DATA &sRouletteData, INT iPlayerID)
	INT iTableID = sRouletteData.playerBD[iPlayerID].iTableID
	RETURN ROULETTE_TABLE_IS_TABLE_ID_VALID(iTableID)
ENDFUNC

/// PURPOSE:
///		Checks if the table ID the local player is using is valid       
FUNC BOOL ROULETTE_TABLE_IS_LOCAL_PLAYERS_TABLE_ID_VALID(ROULETTE_DATA &sRouletteData)
	INT iLocalPlayerID = NATIVE_TO_INT(PLAYER_ID())
	RETURN ROULETTE_TABLE_IS_PLAYERS_TABLE_ID_VALID(sRouletteData, iLocalPlayerID)
ENDFUNC

/// PURPOSE:
///    Checks if the player in the roulette seat is safe to access    
FUNC BOOL ROULETTE_HELPER_IS_PLAYER_AT_SEAT_STILL_PLAYING(ROULETTE_DATA &sRouletteData, INT iSeatID)	
	
	IF NOT ROULETTE_TABLE_IS_SEAT_ID_VALID(iSeatID)
		RETURN FALSE
	ENDIF
	
	PLAYER_INDEX piPlayerID = sRouletteData.serverBD.playersUsingSeats[iSeatID]
	INT iPlayerID = NATIVE_TO_INT(piPlayerID)
	
	IF piPlayerID = INVALID_PLAYER_INDEX()
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_HELPER_IS_PLAYER_AT_SEAT_STILL_PLAYING - No player in seat ", iSeatID)
		RETURN FALSE
	ENDIF
	
	IF NOT ROULETTE_TABLE_IS_SEAT_ID_VALID(sRouletteData.playerBD[iPlayerID].iSeatID)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_HELPER_IS_PLAYER_AT_SEAT_STILL_PLAYING - Player at seat isn't requesting seat")
		RETURN FALSE
	ENDIF
	
	IF NOT IS_NET_PLAYER_OK(piPlayerID)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_HELPER_IS_PLAYER_AT_SEAT_STILL_PLAYING - Player at seat not ok")
		RETURN FALSE
	ENDIF
	
	IF NOT NETWORK_IS_PLAYER_A_PARTICIPANT(piPlayerID)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_HELPER_IS_PLAYER_AT_SEAT_STILL_PLAYING - Player at seat not a participant")
		RETURN FALSE
	ENDIF
	
	IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(NETWORK_GET_PARTICIPANT_INDEX(piPlayerID))
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_HELPER_IS_PLAYER_AT_SEAT_STILL_PLAYING - Player at seat not active participant")
		RETURN FALSE
	ENDIF
	
	
	INT iTableID = sRouletteData.playerBD[iPlayerID].iTableID
	IF NOT ROULETTE_TABLE_IS_TABLE_ID_VALID(iTableID)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_HELPER_IS_PLAYER_AT_SEAT_STILL_PLAYING - Player at seat does not have valid table id")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

// Checks if the player is still playing roulette
FUNC BOOL ROULETTE_HELPER_IS_PLAYER_ID_STILL_PLAYING(ROULETTE_DATA &sRouletteData, INT iPlayerID)
	RETURN ROULETTE_HELPER_IS_PLAYER_AT_SEAT_STILL_PLAYING(sRouletteData, sRouletteData.playerBD[iPlayerID].iSeatID)
ENDFUNC

/// PURPOSE:
///    Gets the player in the given seat  
FUNC PLAYER_INDEX ROULETTE_HELPER_GET_PLAYER_AT_SEAT(ROULETTE_DATA &sRouletteData, INT iSeatID)
	
	IF NOT ROULETTE_TABLE_IS_SEAT_ID_VALID(iSeatID)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_HELPER_GET_PLAYER_AT_SEAT - invalid seat id")
		RETURN INVALID_PLAYER_INDEX()
	ENDIF
	
	IF NOT ROULETTE_HELPER_IS_PLAYER_AT_SEAT_STILL_PLAYING(sRouletteData, iSeatID)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_HELPER_GET_PLAYER_AT_SEAT - player not playing")
		RETURN INVALID_PLAYER_INDEX()
	ENDIF
		
	RETURN sRouletteData.serverBD.playersUsingSeats[iSeatID]
ENDFUNC

/// PURPOSE:
///    Hides the player at the seat
PROC ROULETTE_HELPER_HIDE_PLAYER_AT_SEAT(ROULETTE_DATA &sRouletteData, INT iLocalSeatID, INT iTableID, BOOL bHide)
	INT iSeatID = ROULETTE_HELPER_GET_SEAT_ID_FROM_LOCAL_SEAT_ID_AT_TABLE(iLocalSeatID, iTableID)
	PLAYER_INDEX piPlayerBlockingWheelCam = ROULETTE_HELPER_GET_PLAYER_AT_SEAT(sRouletteData, iSeatID)
	
	IF piPlayerBlockingWheelCam = INVALID_PLAYER_INDEX()
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_HELPER_HIDE_PLAYER_AT_SEAT - invalid player id, iSeatID: ", iSeatID)
		EXIT	
	ENDIF	
	
	IF NOT DOES_ENTITY_EXIST(GET_PLAYER_PED(piPlayerBlockingWheelCam))
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_HELPER_HIDE_PLAYER_AT_SEAT - player ped doesn't exist")
		EXIT
	ENDIF
	
	IF bHide
		SET_PLAYER_INVISIBLE_LOCALLY(piPlayerBlockingWheelCam)
		EXIT
	ENDIF
	
	SET_PLAYER_VISIBLE_LOCALLY(piPlayerBlockingWheelCam)
ENDPROC

/// PURPOSE:
///    Checks if it is safe for the player to have their controls re-enabled
///    taking into account transitions
FUNC BOOL ROULETTE_HELPER_IS_SAFE_TO_ENABLE_PLAYER_CONTROLS(ROULETTE_DATA &sRouletteData)
	RETURN NOT IS_SKYSWOOP_MOVING()
	AND NOT IS_SKYSWOOP_IN_SKY()
	AND NOT IS_TRANSITION_ACTIVE()
	AND NOT IS_TRANSITION_RUNNING()
	AND NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
	AND NOT IS_PLAYER_TELEPORT_ACTIVE()
	AND NOT g_SpawnData.bPassedOutDrunk
	AND sRouletteData.eClientState > ROULETTE_CLIENT_STATE_PROMPT
ENDFUNC

///----------------------------
///    Bit set helpers 
///---------------------------- 

/// PURPOSE:
///    Sets whether or not the local player needs their chips updating for all other clients
PROC ROULETTE_HELPER_SET_PLAYER_REQUIRES_CHIP_UPDATE(ROULETTE_DATA &sRouletteData)
	INT iLocalPlayerID = NATIVE_TO_INT(PLAYER_ID())
	sRouletteData.playerBD[iLocalPlayerID].tdLastChipUpdate = GET_NETWORK_TIME()
ENDPROC

/// PURPOSE:
///    Sets if the rules screen has cleared this frame
PROC ROULETTE_HELPER_SET_RULES_AS_CLEARED_THIS_FRAME(ROULETTE_DATA &sRouletteData, BOOL bClearedThisFrame)
	IF bClearedThisFrame
		SET_BIT(sRouletteData.iRouletteBS, ROULETTE_BS_RULES_CLEARED_THIS_FRAME)
		EXIT
	ENDIF
	
	CLEAR_BIT(sRouletteData.iRouletteBS, ROULETTE_BS_RULES_CLEARED_THIS_FRAME)
ENDPROC

/// PURPOSE:
///    Checks if the rules screen has cleared this frame
FUNC BOOL ROULETTE_HELPER_HAS_RULES_CLEARED_THIS_FRAME(ROULETTE_DATA &sRouletteData)
	RETURN IS_BIT_SET(sRouletteData.iRouletteBS, ROULETTE_BS_RULES_CLEARED_THIS_FRAME)
ENDFUNC

/// PURPOSE:
///    Sets if the rules screen has cleared this frame
PROC ROULETTE_HELPER_SET_DEALER_CALLED_NO_MORE_BETS(ROULETTE_DATA &sRouletteData, BOOL bCalledNoMoreBets)
	IF bCalledNoMoreBets
		SET_BIT(sRouletteData.iRouletteBS, ROULETTE_BS_HAS_DEALER_CALLED_NO_MORE_BETS)
		EXIT
	ENDIF
	
	CLEAR_BIT(sRouletteData.iRouletteBS, ROULETTE_BS_HAS_DEALER_CALLED_NO_MORE_BETS)
ENDPROC

/// PURPOSE:
///    Checks if the rules screen has cleared this frame
FUNC BOOL ROULETTE_HELPER_HAS_DEALER_CALLED_NO_MORE_BETS(ROULETTE_DATA &sRouletteData)
	RETURN IS_BIT_SET(sRouletteData.iRouletteBS, ROULETTE_BS_HAS_DEALER_CALLED_NO_MORE_BETS)
ENDFUNC

/// PURPOSE:
///    Checks if the dealer for the table has finished the spin sequence   
FUNC BOOL ROULETTE_HELPER_HAS_DEALER_FINISHED_SPIN_SEQUENCE_AT_TABLE(ROULETTE_DATA &sRouletteData, INT iTableID)
	RETURN IS_BIT_SET(sRouletteData.serverBD.iTableBS[iTableID], ROULETTE_SERVER_TABLE_BS_DEALER_FINISHED_SPIN_ANIM)
ENDFUNC

/// PURPOSE:
///    Sets if the dealer has finished the tables spin sequence
PROC ROULETTE_HELPER_SET_HAS_DEALER_FINISHED_SPIN_SEQUENCE_AT_TABLE(ROULETTE_DATA &sRouletteData, INT iTableID, BOOL bIsSet)
	IF bIsSet
		SET_BIT(sRouletteData.serverBD.iTableBS[iTableID], ROULETTE_SERVER_TABLE_BS_DEALER_FINISHED_SPIN_ANIM)
		EXIT
	ENDIF
	CLEAR_BIT(sRouletteData.serverBD.iTableBS[iTableID], ROULETTE_SERVER_TABLE_BS_DEALER_FINISHED_SPIN_ANIM)
ENDPROC

/// PURPOSE:
///    Disables betting controls
PROC ROULETTE_HELPER_DISABLE_BETTING_CONTROLS(ROULETTE_DATA &sRouletteData, BOOL bDisabled)
	IF bDisabled
		SET_BIT(sRouletteData.iRouletteBS, ROULETTE_BS_ARE_BETTING_CONTROLS_DISABLED)
		EXIT
	ENDIF
	
	CLEAR_BIT(sRouletteData.iRouletteBS, ROULETTE_BS_ARE_BETTING_CONTROLS_DISABLED)
ENDPROC

/// PURPOSE:
///    Checks if betting controls are dissabled   
FUNC BOOL ROULETTE_HELPER_ARE_BETTING_CONTROLS_DISABLED(ROULETTE_DATA &sRouletteData)
	RETURN IS_BIT_SET(sRouletteData.iRouletteBS, ROULETTE_BS_ARE_BETTING_CONTROLS_DISABLED)
ENDFUNC

///----------------------------
///    Global helpers
///---------------------------- 
	
/// PURPOSE:
///    Sets global that determines if the player has seen the disclosure
PROC ROULETTE_HELPER_GLOBAL_SET_VIEWED_DISCLOSURE(BOOL bDisclosureViewed)
	IF bDisclosureViewed
		SET_BIT(g_SimpleInteriorData.iSixthBS, BS6_SIMPLE_INTERIOR_VIEWED_ROULETTE_DISCLOSURE)
		EXIT
	ENDIF	
	
	CLEAR_BIT(g_SimpleInteriorData.iSixthBS, BS6_SIMPLE_INTERIOR_VIEWED_ROULETTE_DISCLOSURE)
ENDPROC

/// PURPOSE:
///    Checks if the player has seen the roulette disclosure 
FUNC BOOL ROULETTE_HELPER_GLOBAL_HAS_VIEWED_DISCLOSURE()
	RETURN IS_BIT_SET(g_SimpleInteriorData.iSixthBS, BS6_SIMPLE_INTERIOR_VIEWED_ROULETTE_DISCLOSURE)
ENDFUNC	

/// PURPOSE:
///    Sets global is player playing roulette bit set
PROC ROULETTE_HELPER_GLOBAL_SET_LOCAL_PLAYER_PLAYING_ROULETTE(BOOL bIsPlayingRoulette)
	IF bIsPlayingRoulette
		SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSFive,
		BS_SIMPLE_INTERIOR_GLOBAL_BS_FIVE_PLAYING_ROULETTE)
		EXIT
	ENDIF
	
	CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSFive,
	BS_SIMPLE_INTERIOR_GLOBAL_BS_FIVE_PLAYING_ROULETTE)
ENDPROC

/// PURPOSE:
///    Checks if the global bit set 'is player playing roulette' is set    
FUNC BOOL ROULETTE_HELPER_GLOBAL_IS_LOCAL_PLAYER_PLAYING_ROULETTE()
	RETURN IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSFive,
		BS_SIMPLE_INTERIOR_GLOBAL_BS_FIVE_PLAYING_ROULETTE)
ENDFUNC

///----------------------------
///    Object helpers
///---------------------------- 

/// PURPOSE:
///    Gets the table object for the given table ID 
FUNC OBJECT_INDEX ROULETTE_HELPER_GET_TABLE_OBJECT_FOR_ID(ROULETTE_DATA &sRouletteData, INT iTableID)
	IF NOT ROULETTE_TABLE_IS_TABLE_ID_VALID(iTableID)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_HELPER_GET_TABLE_OBJECT_FOR_ID - Invalid ID, returning null")
		RETURN NULL
	ENDIF
	
	RETURN sRouletteData.sSpin[iTableID].objWheel
ENDFUNC

/// PURPOSE:
///    Gets the table object the local player is currently playing on
FUNC OBJECT_INDEX ROULETTE_HELPER_GET_LOCAL_PLAYERS_CURRENT_TABLE_OBJECT(ROULETTE_DATA &sRouletteData)
	RETURN ROULETTE_HELPER_GET_TABLE_OBJECT_FOR_ID(sRouletteData.iTableID)
ENDFUNC

FUNC BOOL ROULETTE_HELPER_CAN_TABLE_BEE_SEEN_BY_LOCAL_PLAYER(ROULETTE_DATA &sRouletteData, INT iTableID)
	OBJECT_INDEX objTable = ROULETTE_HELPER_GET_TABLE_OBJECT_FOR_ID(sRouletteData, iTableID)
	IF objTable = NULL OR NOT DOES_ENTITY_EXIST(objTable)
		RETURN FALSE
	ENDIF
	
	RETURN IS_ENTITY_ON_SCREEN(objTable)	
ENDFUNC

///--------------------------
///   Ped helpers
///--------------------------    

FUNC BOOL ROULETTE_HELPER_IS_DEALER_AT_TABLE_FEMALE(ROULETTE_DATA &sRouletteData, INT iTableID)
	IF NOT ROULETTE_TABLE_IS_TABLE_ID_VALID(iTableID)
		RETURN FALSE
	ENDIF
	
	NETWORK_INDEX niDealer = sRouletteData.serverBD.niDealerPed[iTableID]
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niDealer)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_HELPER_IS_DEALER_AT_TABLE_FEMALE - dealer net ID invalid")
		DEBUG_PRINTCALLSTACK()
		RETURN FALSE
	ENDIF
	
	PED_INDEX piDealer = NET_TO_PED(niDealer)
	IF NOT DOES_ENTITY_EXIST(piDealer)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_HELPER_IS_DEALER_AT_TABLE_FEMALE - dealer ped entity does not exist")
		DEBUG_PRINTCALLSTACK()
		RETURN FALSE
	ENDIF
	
	// Return true if using the female dealer model
	RETURN GET_ENTITY_MODEL(piDealer) = ROULETTE_PROPS_GET_DEALER_PED_MODEL(TRUE)
ENDFUNC

///----------------------------
///   UI helpers
///---------------------------- 

FUNC BOOL ROULETTE_CAMERA_IS_PEEKING_AT_LAYOUT(ROULETTE_DATA &sRouletteData)
	RETURN IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RT) 
	AND sRouletteData.eCurrentCameraView = ROULETTE_CAM_VIEW_LAYOUT
ENDFUNC

FUNC BOOL ROULETTE_CAMERA_IS_PEEKING_AT_WHEEL(ROULETTE_DATA &sRouletteData)	
	RETURN IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LT) 
	AND sRouletteData.eCurrentCameraView = ROULETTE_CAM_VIEW_WHEEL
ENDFUNC

FUNC BOOL ROULETTE_CAMERA_IS_PEEKING_AT_REACTION(ROULETTE_DATA &sRouletteData)	
	RETURN IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LT) 
	AND sRouletteData.eCurrentCameraView = ROULETTE_CAM_VIEW_REACTION
ENDFUNC

FUNC BOOL ROULETTE_CAMERA_IS_IN_A_PEEK_CAMERA(ROULETTE_DATA &sRouletteData)	
	RETURN ROULETTE_CAMERA_IS_PEEKING_AT_LAYOUT(sRouletteData) 
	OR ROULETTE_CAMERA_IS_PEEKING_AT_WHEEL(sRouletteData)
	OR ROULETTE_CAMERA_IS_PEEKING_AT_REACTION(sRouletteData)
ENDFUNC

/// PURPOSE:
///    Hides the gameplay ui during roulette
PROC ROULETTE_HELPER_HIDE_GAMEPLAY_UI_THIS_FRAME()
	HIDE_HUD_AND_RADAR_THIS_FRAME()
	THEFEED_HIDE_THIS_FRAME()
	DISABLE_ALL_MP_HUD_THIS_FRAME()
	DISABLE_FRONTEND_THIS_FRAME()
	
	IF IS_PAUSE_MENU_ACTIVE()
		SET_PAUSE_MENU_ACTIVE(FALSE)
	ENDIF
ENDPROC
