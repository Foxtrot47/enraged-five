//////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Name:        RouletteServerState.sch																		
/// Description: State enum for the roulette server															
///																											
/// Written by:  Online Technical Team: Tom Turner,															
/// Date:  		12/02/2019																					
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

/// PURPOSE: 
///    Enum for servers overall state
ENUM ROULETTE_SERVER_STATE
	ROULETTE_SERVER_STATE_INIT,
	ROULETTE_SERVER_STATE_IDLE,
	ROULETTE_SERVER_STATE_PLAYING
ENDENUM

/// PURPOSE:
///    Gets the name of the state enum as a string 
FUNC STRING GET_ROULETTE_SERVER_STATE_NAME(ROULETTE_SERVER_STATE eState)
	SWITCH eState
		CASE ROULETTE_SERVER_STATE_INIT			RETURN "ROULETTE_SERVER_STATE_INIT"	
		CASE ROULETTE_SERVER_STATE_IDLE			RETURN "ROULETTE_SERVER_STATE_IDLE"	
		CASE ROULETTE_SERVER_STATE_PLAYING		RETURN "ROULETTE_SERVER_STATE_PLAYING"	
	ENDSWITCH
	
	RETURN "INVALID_STATE"
ENDFUNC
