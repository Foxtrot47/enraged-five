//////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Name:        RouletteDebug.sch																			
/// Description: Header for roulette debugging as to keep it seperate from main roulette code				
///																											
/// Written by:  Online Technical Team: Tom Turner,															
/// Date:  		12/02/2019																					
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

#IF IS_DEBUG_BUILD
USING "RouletteData.sch"
USING "RouletteLayout.sch"
USING "RouletteBetting.sch"
USING "RouletteConstants.sch"
USING "RouletteCamera.sch"
USING "RouletteProps.sch"
USING "RouletteHelper.sch"
USING "RouletteAnimation.sch"
USING "RouletteSeatRequesting.sch"
USING "Globals.sch"

ENUM ROULETTE_LAYOUT_DEBUG_INFO
	LAYOUT_DEBUG_INFO_NONE,
	LAYOUT_DEBUG_INFO_GRID_COORDS,
	LAYOUT_DEBUG_INFO_GRID_INDEX,
	LAYOUT_DEBUG_INFO_BET_TYPE
ENDENUM

STRUCT ROULETTE_DEBUG
	BOOL bDrawSeatPositions = FALSE
	BOOL bDrawSeatAreas = FALSE
	BOOL bDrawSeatBeingChecked = FALSE
	BOOL bDrawSeatIds = FALSE
	BOOL bBettingTime = FALSE
	BOOL bHideUI = FALSE
	BOOL bShowLoseStreak = FALSE
	BOOL bOverrideWinningNumber = FALSE
	INT iWinningNumber = 0
	
	BOOL bDrawTablePositions = FALSE
	BOOL bDrawTableIds = FALSE
	BOOL bDrawDistanceBetweenCollider = FALSE
	
	INT iLayoutInfoToDisplay = 0
	BOOL bDrawCursor = FALSE
	BOOL bDrawCursorBounds = FALSE
	BOOL bDrawBetAmounts = FALSE
	
	BOOL bDrawDealerPosition = FALSE
	BOOL bDrawSeatBonePositions = FALSE
	
	INT iCameraView = 0
	BOOL bViewSelectedCamera = FALSE
	BOOL bDisplayCamSwitchInfo = FALSE
ENDSTRUCT

/// PURPOSE:
///    Creates all debug widgets for roulette minigame debug instance
PROC ROULETTE_CREATE_DEBUG_WIDGETS(ROULETTE_DEBUG &sRouletteDebug)
	START_WIDGET_GROUP("Roulette Minigame")	
		START_WIDGET_GROUP("Game state debug")
			ADD_WIDGET_BOOL("Override winning table numbers", sRouletteDebug.bOverrideWinningNumber)
			ADD_WIDGET_INT_SLIDER("Roulette number to win (00 is 37)", sRouletteDebug.iWinningNumber, 0, 37, 1)
			ADD_WIDGET_BOOL("Host only - Pause betting time", sRouletteDebug.bBettingTime)	
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("UI")
			ADD_WIDGET_BOOL("Hide UI", sRouletteDebug.bHideUI)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Seats debug")
			ADD_WIDGET_BOOL("Draw seat positions", sRouletteDebug.bDrawSeatPositions)
			ADD_WIDGET_BOOL("Draw seat areas", sRouletteDebug.bDrawSeatAreas)
			ADD_WIDGET_BOOL("Draw seat being checked", sRouletteDebug.bDrawSeatBeingChecked)
			ADD_WIDGET_BOOL("Draw seat ids", sRouletteDebug.bDrawSeatIds)	
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Table debug")
			ADD_WIDGET_BOOL("Draw table positions", sRouletteDebug.bDrawTablePositions)
			ADD_WIDGET_BOOL("Draw table ids", sRouletteDebug.bDrawTableIds)		
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Layout debug")	
			ADD_WIDGET_BOOL("Draw cursor", sRouletteDebug.bDrawCursor)
			ADD_WIDGET_BOOL("Draw cursor bounds", sRouletteDebug.bDrawCursorBounds)
			ADD_WIDGET_BOOL("Draw bet amounts", sRouletteDebug.bDrawBetAmounts)
			
			START_NEW_WIDGET_COMBO()
				ADD_TO_WIDGET_COMBO("None")
				ADD_TO_WIDGET_COMBO("Grid positions")
				ADD_TO_WIDGET_COMBO("Grid indexs")
				ADD_TO_WIDGET_COMBO("Bet types")
			STOP_WIDGET_COMBO("Layout display type", sRouletteDebug.iLayoutInfoToDisplay)	
			
			START_WIDGET_GROUP("Layout description debug")
				ADD_WIDGET_FLOAT_SLIDER("Layout Origin X", ROULETTE_LAYOUT_ORIGIN_OFFSET_X, -100, 100, 0.0001)
				ADD_WIDGET_FLOAT_SLIDER("Layout Origin Y", ROULETTE_LAYOUT_ORIGIN_OFFSET_Y, -100, 100, 0.0001)
				
				ADD_WIDGET_FLOAT_SLIDER("ROULETTE_LAYOUT_INSIDE_GRID_CELL_HEIGHT", ROULETTE_LAYOUT_INSIDE_GRID_CELL_HEIGHT,  -100, 100, 0.0001)
				ADD_WIDGET_FLOAT_SLIDER("ROULETTE_LAYOUT_ZEROES_GRID_CELL_HEIGHT", ROULETTE_LAYOUT_ZEROES_GRID_CELL_HEIGHT,  -100, 100, 0.0001)
				ADD_WIDGET_FLOAT_SLIDER("ROULETTE_LAYOUT_OUTSIDE_GRID_CELL_WIDTH", ROULETTE_LAYOUT_OUTSIDE_GRID_CELL_WIDTH,  -100, 100, 0.0001)
				ADD_WIDGET_FLOAT_SLIDER("ROULETTE_LAYOUT_INSIDE_GRID_CELL_WIDTH", ROULETTE_LAYOUT_INSIDE_GRID_CELL_WIDTH,  -100, 100, 0.0001)
				
				ADD_WIDGET_FLOAT_SLIDER("ROULETTE_PROPS_LAYOUT_HIGHLIGHT_OFFSET", ROULETTE_PROPS_LAYOUT_HIGHLIGHT_OFFSET, -100, 100, 0.0001)
				ADD_WIDGET_FLOAT_SLIDER("ROULETTE_PROPS_LAYOUT_ZEROES_HIGHLIGHT_X_OFFSET", ROULETTE_PROPS_LAYOUT_ZEROES_HIGHLIGHT_X_OFFSET, -100, 100, 0.0001)
				ADD_WIDGET_FLOAT_SLIDER("ROULETTE_TABLE_LAYOUT_DECAL_HEIGHT_OFFSET", ROULETTE_TABLE_LAYOUT_DECAL_HEIGHT_OFFSET, -100, 100, 0.0001)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Cursor bounds")	
				ADD_WIDGET_FLOAT_SLIDER("ROULETTE_UI_CURSOR_BOUND_TR_X", ROULETTE_UI_CURSOR_BOUND_TR_X,  -100, 100, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("ROULETTE_UI_CURSOR_BOUND_TR_Y", ROULETTE_UI_CURSOR_BOUND_TR_Y,  -100, 100, 0.01)
				
				ADD_WIDGET_FLOAT_SLIDER("ROULETTE_UI_CURSOR_BOUND_BL_X", ROULETTE_UI_CURSOR_BOUND_BL_X,  -100, 100, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("ROULETTE_UI_CURSOR_BOUND_BL_Y", ROULETTE_UI_CURSOR_BOUND_BL_Y,  -100, 100, 0.01)
			STOP_WIDGET_GROUP()
			
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Anim Debug")
			START_WIDGET_GROUP("Player Anim")
				ADD_WIDGET_BOOL("Show lose streak", sRouletteDebug.bShowLoseStreak)
				ADD_WIDGET_INT_SLIDER("Lose streaks before terrible reaction triggers", ROULETTE_ANIM_MAX_LOSS_STREAK_BEFORE_TERRIBLE_REACTION_CAN_TRIGGER, 0, 100, 1)
			STOP_WIDGET_GROUP()
		
			START_WIDGET_GROUP("Seat Anim")
				ADD_WIDGET_BOOL("Draw seat bones", sRouletteDebug.bDrawSeatBonePositions)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Dealer Anim")
				ADD_WIDGET_BOOL("Draw dealer position", sRouletteDebug.bDrawDealerPosition)
				ADD_WIDGET_FLOAT_SLIDER("Dealer position X", ROULETTE_TABLE_DEALER_POSITION_OFFSET_X, -100, 100, 0.1)
				ADD_WIDGET_FLOAT_SLIDER("Dealer position Y", ROULETTE_TABLE_DEALER_POSITION_OFFSET_Y, -100, 100, 0.1)
				ADD_WIDGET_FLOAT_SLIDER("Dealer position Z", ROULETTE_TABLE_DEALER_POSITION_OFFSET_Z, -100, 100, 0.1)
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Cameras")
			START_NEW_WIDGET_COMBO()
				ADD_TO_WIDGET_COMBO("Layout view")
				ADD_TO_WIDGET_COMBO("Wheel view")
				ADD_TO_WIDGET_COMBO("Dealer view")
				ADD_TO_WIDGET_COMBO("Reaction view")
			STOP_WIDGET_COMBO("Layout display type", sRouletteDebug.iCameraView)		
			
			ADD_WIDGET_BOOL("Show selected camera view", sRouletteDebug.bViewSelectedCamera)
			ADD_WIDGET_FLOAT_SLIDER("Wheel cam ease in target FOV", ROULETTE_CAM_WHEEL_CAM_TARGET_FOV, 0, 100, 0.1)
			ADD_WIDGET_BOOL("Show switch cam info", sRouletteDebug.bDisplayCamSwitchInfo)
			
			START_WIDGET_GROUP("Layout Cam Movement")
				ADD_WIDGET_FLOAT_SLIDER("ROULETTE_CAM_MAX_LAYOUT_MOVEMENT_LEFT", ROULETTE_CAM_MAX_LAYOUT_MOVEMENT_LEFT, 0, 100, 0.1)
				ADD_WIDGET_FLOAT_SLIDER("ROULETTE_CAM_MAX_LAYOUT_MOVEMENT_RIGHT", ROULETTE_CAM_MAX_LAYOUT_MOVEMENT_RIGHT, 0, 100, 0.1)
				ADD_WIDGET_FLOAT_SLIDER("ROULETTE_CAM_MAX_LAYOUT_MOVEMENT_UP", ROULETTE_CAM_MAX_LAYOUT_MOVEMENT_UP, 0, 100, 0.1)
				ADD_WIDGET_FLOAT_SLIDER("ROULETTE_CAM_MAX_LAYOUT_MOVEMENT_DOWN", ROULETTE_CAM_MAX_LAYOUT_MOVEMENT_DOWN, 0, 100, 0.1)
				ADD_WIDGET_FLOAT_SLIDER("ROULETTE_CAM_LAYOUT_MOVEMENT_SPEED", ROULETTE_CAM_LAYOUT_MOVEMENT_SPEED, 0, 100, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("ROULETTE_CAM_LAYOUT_MOVEMENT_PAN_MOVE_PERCENTAGE", ROULETTE_CAM_LAYOUT_MOVEMENT_PAN_MOVE_PERCENTAGE, 0, 1, 0.01)	
			STOP_WIDGET_GROUP()	
		STOP_WIDGET_GROUP()	
	STOP_WIDGET_GROUP()
ENDPROC

/// PURPOSE: 
/// Draws a circle with the given radius
PROC DRAW_CIRCLE(VECTOR vCentre, FLOAT fRadius, INT r = 0, INT g = 0, INT b = 0, INT a = 255, int iSides = 32)
	FLOAT theta = 0
	FLOAT thetaIncrement = 360.0 / iSides 
	VECTOR lastPosition = <<0, 0, 0>>
	
	INT i = 0
	REPEAT (iSides + 1) i 
		
		theta += thetaIncrement
		
		VECTOR lineEndPosition = <<0, 0, 0>>
		lineEndPosition.x = COS(theta) * fRadius
		lineEndPosition.y = SIN(theta) * fRadius
		lineEndPosition += vCentre
		
		IF (i > 0)
			DRAW_DEBUG_LINE(lastPosition, lineEndPosition, r, g, b, a)
		ENDIF
		
		lastPosition = LineEndPosition		
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Draws seat position
PROC ROULETTE_DEBUG_DRAW_SEAT_POSITION(ROULETTE_DATA &sRouletteData, INT iSeatID)	
	VECTOR vSeatPosition = ROULETTE_PROPS_GET_SEAT_BONE_POSITION(sRouletteData, iSeatID)
	DRAW_DEBUG_LINE(vSeatPosition, vSeatPosition + <<0, 0, 2>>, 255, 255, 0, 255)
ENDPROC

/// PURPOSE:
///    Draws seat location check area
PROC ROULETTE_DEBUG_DRAW_SEAT_AREA(ROULETTE_DATA &sRouletteData, INT iSeatID)
	VECTOR vSeatPosition = ROULETTE_PROPS_GET_SEAT_BONE_POSITION(sRouletteData, iSeatID)
	
	// Draw seat area checks
	IF IS_LOCAL_PLAYER_IN_SEAT_AREA(sRouletteData, iSeatID)
		DRAW_CIRCLE(vSeatPosition + <<0, 0, 0.5>>, ROULETTE_SEAT_LOCATE_RADIUS, 255, 255, 0, 255)
	ELSE
		DRAW_CIRCLE(vSeatPosition + <<0, 0, 0.5>>, ROULETTE_SEAT_LOCATE_RADIUS, 255, 0, 0, 255)
	ENDIF
ENDPROC

/// PURPOSE:
///    Draws seat id in floating debug text
PROC ROULETTE_DEBUG_DRAW_SEAT_ID(ROULETTE_DATA &sRouletteData, INT iSeatID)	
	VECTOR vSeatPosition = ROULETTE_PROPS_GET_SEAT_BONE_POSITION(sRouletteData, iSeatID)
	
	TEXT_LABEL_63 tlSeatIDs = ""
	tlSeatIDs += "Seat ID: "
	tlSeatIDs += iSeatID
	
	tlSeatIDs += "\nLocal Seat ID: "
	tlSeatIDs += ROULETTE_HELPER_GET_LOCAL_SEAT_ID_FROM_SEAT_ID(iSeatID)
	
	tlSeatIDs += "\nTable ID: "
	tlSeatIDs += ROULETTE_HELPER_GET_TABLE_ID_FROM_SEAT_ID(iSeatID)
	
	DRAW_DEBUG_TEXT_ABOVE_COORDS(vSeatPosition, GET_STRING_FROM_TL(tlSeatIDs), 1, 255, 255, 255)
ENDPROC

/// PURPOSE:
///    Draws sphere above the seat currently checking if the player is in its locate area
PROC ROULETTE_DEBUG_DRAW_SEAT_BEING_CHECKED(ROULETTE_DATA &sRouletteData, INT iSeatID)
	VECTOR vSeatPosition = ROULETTE_PROPS_GET_SEAT_BONE_POSITION(sRouletteData, iSeatID)
	
	// Draw seat currently being checked
	IF iSeatID = sRouletteData.iSeatID
		DRAW_DEBUG_SPHERE(vSeatPosition + <<0, 0, 2>>, 0.1, 255, 0, 0, 255)	
	ENDIF
ENDPROC

/// PURPOSE:
///    Draws debug for all roulette seats
PROC ROULETTE_DEBUG_DRAW_SEATS(ROULETTE_DEBUG &sRouletteDebug, ROULETTE_DATA &sRouletteData)
	IF NOT sRouletteDebug.bDrawSeatPositions 
	AND NOT sRouletteDebug.bDrawSeatAreas
	AND NOT sRouletteDebug.bDrawSeatIds
	AND NOT sRouletteDebug.bDrawSeatBeingChecked
		EXIT
	ENDIF
	
	INT iSeat
	REPEAT ROULETTE_MAX_SEAT_ID iSeat	
		IF sRouletteDebug.bDrawSeatPositions
			ROULETTE_DEBUG_DRAW_SEAT_POSITION(sRouletteData, iSeat)
		ENDIF

		IF sRouletteDebug.bDrawSeatAreas
			ROULETTE_DEBUG_DRAW_SEAT_AREA(sRouletteData, iSeat)
		ENDIF
		
		IF sRouletteDebug.bDrawSeatIds
			ROULETTE_DEBUG_DRAW_SEAT_ID(sRouletteData, iSeat)
		ENDIF
		
		IF sRouletteDebug.bDrawSeatBeingChecked
			ROULETTE_DEBUG_DRAW_SEAT_BEING_CHECKED(sRouletteData, iSeat)
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Draws the position of dealer for the table
PROC ROULETTE_DEBUG_DRAW_DEALER_POSITION_FOR_TABLE(INT iTableID)
	VECTOR vDealerPosition = ROULETTE_TABLE_GET_DEALER_POSITION(iTableID)
	DRAW_DEBUG_LINE(vDealerPosition, vDealerPosition + <<0, 0, 2>>, 249, 77, 226, 255)
ENDPROC

PROC ROULETTE_DEBUG_DRAW_SEAT_BONE_POSITIONS_FOR_TABLE(ROULETTE_DATA &sRouletteData, INT iTableID)	
	VECTOR vSeatPos
	INT iLocalSeatID = 0	
	REPEAT ROULETTE_TABLE_MAX_NUMBER_OF_PLAYERS_PER_TABLE iLocalSeatID
		vSeatPos = ROULETTE_PROPS_GET_LOCAL_SEAT_BONE_POSITION(sRouletteData, iLocalSeatID, iTableID)
		DRAW_DEBUG_SPHERE(vSeatPos, 0.1, 255, 255, 255, 255)
		STRING strBoneName = ROULETTE_ANIM_GET_SEAT_BONE_NAME(iLocalSeatID)
		DRAW_DEBUG_TEXT_ABOVE_COORDS(vSeatPos, strBoneName, 0.05, 255, 255, 255, 255)
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Draws debug for all roulette tables
PROC ROULETTE_DEBUG_DRAW_TABLES(ROULETTE_DEBUG &sRouletteDebug, ROULETTE_DATA &sRouletteData)
	
	IF NOT sRouletteDebug.bDrawTablePositions
	AND NOT sRouletteDebug.bDrawTableIds
	AND NOT sRouletteDebug.bDrawDealerPosition
	AND NOT sRouletteDebug.bDrawSeatBonePositions
		EXIT
	ENDIF
	
	INT iTable
	REPEAT ROULETTE_TABLE_MAX_NUMBER_OF_TABLES iTable
		VECTOR vTablePosition = ROULETTE_TABLE_GET_POSITION(iTable)
		
		IF sRouletteDebug.bDrawTablePositions
			DRAW_DEBUG_LINE(vTablePosition, vTablePosition + <<0, 0, 2>>, 255, 0, 0, 255)
		ENDIF
		
		IF sRouletteDebug.bDrawTableIds
			STRING sTableId = GET_STRING_FROM_INT(iTable)
			DRAW_DEBUG_TEXT_ABOVE_COORDS(vTablePosition, sTableId, 2, 255, 255, 255)
		ENDIF
		
		IF sRouletteDebug.bDrawDealerPosition
			ROULETTE_DEBUG_DRAW_DEALER_POSITION_FOR_TABLE(iTable)
		ENDIF
		
		IF sRouletteDebug.bDrawSeatBonePositions
			ROULETTE_DEBUG_DRAW_SEAT_BONE_POSITIONS_FOR_TABLE(sRouletteData, iTable)
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Draws debug for the layout on the table according to what info should be displayed
PROC ROULETTE_DEBUG_DRAW_TABLE_LAYOUT(INT iTableID, INT iLayoutInfoToDisplay)
	VECTOR vLayoutOrigin = ROULETTE_LAYOUT_GET_ORIGIN_FOR_TABLE(iTableID)
	DRAW_DEBUG_SPHERE(vLayoutOrigin + <<0, 0, 0.005>>, 0.01, 0, 255, 0, 255)
		
	INT iXCoord = 0
	REPEAT LAYOUT_MAX_INSIDE_BET_X_COORD + 1 iXCoord 		
		INT iYCoord = 0
		REPEAT (ROULETTE_LAYOUT_GET_MAX_Y_FOR_COORD(iXCoord, iYCoord) + 1)  iYCoord		
			IF iXCoord <= ROULETTE_LAYOUT_GET_MAX_X_FOR_COORD(iXCoord, iYCoord)
				VECTOR vGridPos = ROULETTE_LAYOUT_GET_BET_POSITION_ON_TABLE_FOR_LAYOUT_COORD(iTableID, iXCoord, iYCoord)
				DRAW_DEBUG_SPHERE(vGridPos + <<0, 0, 0.005>>, 0.01, 255, 255, 0, 255)

				TEXT_LABEL tlLabel = ""
				
				SWITCH INT_TO_ENUM(ROULETTE_LAYOUT_DEBUG_INFO, iLayoutInfoToDisplay)
					CASE LAYOUT_DEBUG_INFO_GRID_COORDS
						tlLabel += iXCoord
						tlLabel += ", "
						tlLabel += iYCoord
					BREAK
					CASE LAYOUT_DEBUG_INFO_GRID_INDEX
						tlLabel += "("
						tlLabel += ROULETTE_LAYOUT_COORD_TO_LAYOUT_INDEX(iXCoord, iYCoord)
						tlLabel += ")"
					BREAK
					CASE LAYOUT_DEBUG_INFO_BET_TYPE
						tlLabel += ROULETTE_GET_BET_TYPE_NAME_AS_STRING(ROULETTE_BETTING_GET_BET_TYPE_FROM_COORDS(iXCoord, iYCoord))
					BREAK
				ENDSWITCH
				
				STRING sCoord = GET_STRING_FROM_TL(tlLabel)
				DRAW_DEBUG_TEXT(sCoord, vGridPos + <<0, 0, 0.008>>, 255, 255, 255)		
			ENDIF
		ENDREPEAT
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Debug draws the roulette layout for all tables
PROC ROULETTE_DEBUG_DRAW_LAYOUT(ROULETTE_DEBUG &sRouletteDebug)
	IF sRouletteDebug.iLayoutInfoToDisplay = 0
		EXIT
	ENDIF
	
	INT iTable
	REPEAT ROULETTE_TABLE_MAX_NUMBER_OF_TABLES iTable
		ROULETTE_DEBUG_DRAW_TABLE_LAYOUT(iTable, sRouletteDebug.iLayoutInfoToDisplay)
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Debug draws the roulette cursor on the table
PROC ROULETTE_DEBUG_DRAW_CURSOR(ROULETTE_DEBUG &sRouletteDebug, ROULETTE_DATA &sRouletteData)
	IF NOT sRouletteDebug.bDrawCursor
		EXIT
	ENDIF
	
	INT iTableID = sRouletteData.playerBD[NATIVE_TO_INT(PLAYER_ID())].iTableID
	INT iCursorXCoord = sRouletteData.iCursorXCoord
	INT iCursorYCoord = sRouletteData.iCursorYCoord
	
	VECTOR vCursorPos = ROULETTE_LAYOUT_GET_BET_POSITION_ON_TABLE_FOR_LAYOUT_COORD(iTableID, iCursorXCoord, iCursorYCoord)
	DRAW_DEBUG_SPHERE(vCursorPos + <<0, 0, 0.007>>, 0.01, 0, 0, 255, 255)
	
	TEXT_LABEL tlLabel = ""
	tlLabel += iCursorXCoord
	tlLabel += ", "
	tlLabel += iCursorYCoord
				
	STRING sCoord = GET_STRING_FROM_TL(tlLabel)
	DRAW_DEBUG_TEXT(sCoord, vCursorPos + <<0, 0, 0.009>>, 0, 0, 255)
ENDPROC

/// PURPOSE:
///    Debug draws all the local players bets on the current table
PROC ROULETTE_DEBUG_DRAW_LOCAL_PLAYERS_BETS(ROULETTE_DEBUG &sRouletteDebug, ROULETTE_DATA &sRouletteData)
	IF NOT sRouletteDebug.bDrawBetAmounts
		EXIT
	ENDIF
	
	INT iBetIndex
	REPEAT ROULETTE_BETTING_MAX_ALLOWED_BETS iBetIndex
		ROULETTE_BET sBet = sRouletteData.playerBD[NATIVE_TO_INT(PLAYER_ID())].iBets[iBetIndex]
		
		IF sBet.iLayoutIndex != -1		
			INT iXCoord = 0
			INT iYCoord = 0		
			ROULETTE_LAYOUT_INDEX_TO_LAYOUT_COORD(sBet.iLayoutIndex, iXCoord, iYCoord)
			
			INT iTableID = sRouletteData.playerBD[NATIVE_TO_INT(PLAYER_ID())].iTableID
			VECTOR vBetPosition = ROULETTE_LAYOUT_GET_BET_POSITION_ON_TABLE_FOR_LAYOUT_COORD(iTableID, iXCoord, iYCoord)
			
			TEXT_LABEL_63 tlLabel = ""
			tlLabel += sBet.iBetAmount
			
			STRING sAmount = GET_STRING_FROM_TL(tlLabel)
			DRAW_DEBUG_TEXT(sAmount, vBetPosition + <<0, 0, 0.009>>, 255, 100, 0)
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Shows the selected roulette camera view
PROC ROULETTE_DEBUG_SHOW_CAMERA_VIEWS(ROULETTE_DEBUG &sRouletteDebug, ROULETTE_DATA &sRouletteData)
	INT iTableID = sRouletteData.iTableID
	
	IF iTableID < 0
	ROULETTE_CAMERA_SET_MAIN_CAM_ENABLED(sRouletteData, FALSE, FALSE)
		EXIT
	ENDIF

	IF NOT sRouletteDebug.bViewSelectedCamera
		EXIT
	ENDIF	
	
	ROULETTE_CAM_VIEW eSelectedCamera = INT_TO_ENUM(ROULETTE_CAM_VIEW, sRouletteDebug.iCameraView)
	ROULETTE_CAMERA_SHOW_VIEW(sRouletteData, eSelectedCamera)	
ENDPROC

/// PURPOSE:
///    Draws the corners of the cursors boundary
PROC ROULETTE_DEBUG_DRAW_CURSOR_BOUNDS(ROULETTE_DEBUG &sRouletteDebug, ROULETTE_DATA &sRouletteData)	

	INT iTableID = sRouletteData.iTableID
	IF NOT sRouletteDebug.bDrawCursorBounds OR iTableID < 0
		EXIT
	ENDIF
	
	VECTOR vTableLayoutOrigin = ROULETTE_LAYOUT_GET_ORIGIN_FOR_TABLE(iTableID)
	VECTOR vTopRight = ROULETTE_TABLE_GET_VECTOR_RELATIVE_TO_TABLE(ROULETTE_PROPS_GET_CURSOR_BOUNDARY_TOP_RIGHT_POS(), iTableID) + vTableLayoutOrigin
	VECTOR vBottomLeft = ROULETTE_TABLE_GET_VECTOR_RELATIVE_TO_TABLE(ROULETTE_PROPS_GET_CURSOR_BOUNDARY_BOTTOM_LEFT_POS(), iTableID)+ vTableLayoutOrigin		
		
	DRAW_DEBUG_SPHERE(vBottomLeft, 0.02, 0, 255, 255, 255)
	DRAW_DEBUG_SPHERE(vTopRight, 0.02, 0, 0, 255, 255)
ENDPROC

PROC ROULETTE_DEBUG_PRINT_ROULETTE_DATA_BS(ROULETTE_DATA &sRouletteData)
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_DATA - iRouletteBS, ROULETTE_UPDATE_BUTTONS_BS: ",
	IS_BIT_SET(sRouletteData.iRouletteBS, ROULETTE_UPDATE_BUTTONS_BS))
	
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_DATA - iRouletteBS, ROULETTE_UPDATE_BET_AMOUNT_AT_CURSOR_UI: ",
	IS_BIT_SET(sRouletteData.iRouletteBS, ROULETTE_UPDATE_BET_AMOUNT_AT_CURSOR_UI))
	
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_DATA - iRouletteBS, ROULETTE_UPDATE_MIN_BET_CHECK: ",
	IS_BIT_SET(sRouletteData.iRouletteBS, ROULETTE_UPDATE_MIN_BET_CHECK))
	
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_DATA - iRouletteBS, ROULETTE_DISPLAY_INSIDE_MIN_BET_WARNING: ",
	IS_BIT_SET(sRouletteData.iRouletteBS, ROULETTE_DISPLAY_INSIDE_MIN_BET_WARNING))
	
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_DATA - iRouletteBS, ROULETTE_DISPLAY_OUTSIDE_MIN_BET_WARNING: ",
	IS_BIT_SET(sRouletteData.iRouletteBS, ROULETTE_DISPLAY_OUTSIDE_MIN_BET_WARNING))
	
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_DATA - iRouletteBS, ROULETTE_HAS_DISABLED_PLAYER_CONTROL: ",
	IS_BIT_SET(sRouletteData.iRouletteBS, ROULETTE_HAS_DISABLED_PLAYER_CONTROL))
ENDPROC

PROC ROULETTE_DEBUG_PRINT_ROULETTE_DATA_STRUCT(ROULETTE_DATA &sRouletteData)
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_DATA - ROULETTE_DATA ------------------")
	
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_DATA - eClientState: ", ROULETTE_CLIENT_STATE_GET_STATE_NAME(sRouletteData.eClientState))
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_DATA - iContextIntention: ", sRouletteData.iContextIntention)
	
	ROULETTE_DEBUG_PRINT_ROULETTE_DATA_BS(sRouletteData)
	
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_DATA - iSeatID: ", sRouletteData.iSeatID )
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_DATA - stSeatRequestTimeOut: ", GET_NET_TIMER_STRING(sRouletteData.stSeatRequestTimeOut))
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_DATA - sfInstructionalButtons: no debug available")
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_DATA - sfButton: ", NATIVE_TO_INT(sRouletteData.sfButton))
	
	IF DOES_CAM_EXIST(sRouletteData.ciRouletteCamera)
		PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_DATA - ciRouletteCamera, pos: ", GET_STRING_FROM_VECTOR(GET_CAM_COORD(sRouletteData.ciRouletteCamera)))
		PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_DATA - ciRouletteCamera, rot: ", GET_STRING_FROM_VECTOR(GET_CAM_ROT(sRouletteData.ciRouletteCamera)))
	ELSE
		PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_DATA - ciRouletteCamera: NULL")
	ENDIF
	
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_DATA - eCurrentCameraView: ", ROULETTE_CAM_VIEW_GET_CAM_VIEW_AS_STRING(sRouletteData.eCurrentCameraView))
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_DATA - eLastGameplayCameraView: ", ROULETTE_CAM_VIEW_GET_CAM_VIEW_AS_STRING(sRouletteData.eSwitchableCameraView[sRouletteData.iSwitchCamIndex]))
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_DATA - eInitialView(int): ", ENUM_TO_INT(sRouletteData.eInitialView))
	
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_DATA - iTotalInsideBetThisRound: ", sRouletteData.iTotalInsideBetThisRound)
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_DATA - iTotalOutsideBetThisRound: ", sRouletteData.iTotalOutsideBetThisRound)
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_DATA - iBetAmountAtCurrentCursorPosition: ", sRouletteData.iBetAmountAtCurrentCursorPosition)
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_DATA - iTotalWinnings: ", sRouletteData.iTotalWinnings)
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_DATA - iRoundPayout: ", sRouletteData.iRoundPayout)
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_DATA - vCursorPosition: ", GET_STRING_FROM_VECTOR(sRouletteData.vCursorPosition))
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_DATA - stCursorMoveDelay: ", GET_NET_TIMER_STRING(sRouletteData.stCursorMoveDelay))
	
	IF DOES_ENTITY_EXIST(sRouletteData.objCursor)
		PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_DATA - objCursor, pos: ", GET_STRING_FROM_VECTOR(GET_ENTITY_COORDS(sRouletteData.objCursor)))
		PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_DATA - objCursor, rot: ", GET_STRING_FROM_VECTOR(GET_ENTITY_ROTATION(sRouletteData.objCursor)))
	ELSE
		PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_DATA - objCursor: NULL")
	ENDIF
	
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_DATA - objChips: no debug to show")
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_DATA - objHighlights: no debug to show")
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_DATA - objBalls: no debug to show")
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_DATA - sLastPlayerAnimInfo, clip: ",
	ROULETTE_ANIM_CLIP_GET_NAME_OF_CLIP_AS_STRING(sRouletteData.sLastPlayerAnimInfo.eClip))
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_DATA - sLastPlayerAnimInfo, variation: ",
	sRouletteData.sLastPlayerAnimInfo.iVariation)
	
	INT i = 0
	REPEAT ROULETTE_TABLE_MAX_NUMBER_OF_TABLES i
		PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_DATA ------------")
		IF sRouletteData.playerBD[NATIVE_TO_INT(PLAYER_ID())].iTableID = i
			PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_DATA - CURRENT TABLE")
		ENDIF
		
		PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_DATA - eDealerLastAnim[", i,"]: ", ROULETTE_ANIM_CLIP_GET_NAME_OF_CLIP_AS_STRING(sRouletteData.serverBD.sDealerLastAnim[i].eClip))
		PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_DATA - iZonesToClearBS: ", sRouletteData.serverBD.iZonesToClearBS[i])
	ENDREPEAT
	
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_DATA ----------------------------------")
ENDPROC

PROC ROULETTE_DEBUG_PRINT_PLAYER_BD(ROULETTE_DATA &sRouletteData, INT iPlayerID)
	// This player id has no seat
	IF NOT ROULETTE_TABLE_IS_SEAT_ID_VALID(sRouletteData.playerBD[iPlayerID].iSeatID)
		EXIT
	ENDIF
	
	// Get player at seat
	PLAYER_INDEX piPlayerID = sRouletteData.serverBD.playersUsingSeats[sRouletteData.playerBD[iPlayerID].iSeatID]

	IF piPlayerID = INVALID_PLAYER_INDEX()
		EXIT
	ENDIF

	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_DATA - iPlayerID: ", iPlayerID)
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_DATA - Gamer tag: ", GET_PLAYER_NAME(piPlayerID))
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_DATA - iSeatID: ", sRouletteData.playerBD[iPlayerID].iSeatID)
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_DATA - iTableID: ", sRouletteData.playerBD[iPlayerID].iTableID)
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_DATA - eGameState: ", GET_ROULETTE_GAME_STATE_NAME(sRouletteData.playerBD[iPlayerID].eGameState))
	
	INT i
	REPEAT ROULETTE_BETTING_MAX_ALLOWED_BETS i
		PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_DATA - Bet[", i,"], iLayoutIndex: ", 
		sRouletteData.playerBD[iPlayerID].iBets[i].iLayoutIndex,
		" iBetAmount: ", sRouletteData.playerBD[iPlayerID].iBets[i].iBetAmount)
	ENDREPEAT
	
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_DATA ------------")
ENDPROC

PROC ROULETTE_DEBUG_PRINT_ALL_PLAYER_BD(ROULETTE_DATA &sRouletteData)
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_DATA - PLAYER_BD ------------------")
	INT i = 0
		
	REPEAT NUM_NETWORK_PLAYERS i
		ROULETTE_DEBUG_PRINT_PLAYER_BD(sRouletteData, i)
	ENDREPEAT
ENDPROC

PROC ROULETTE_DEBUG_PRINT_SERVER_BD(ROULETTE_DATA &sRouletteData)
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_DATA - SERVER_BD ------------------")
		
	INT iTableID = 0
	INT iMaxTableID = COUNT_OF(sRouletteData.serverBD.eGameState)
	REPEAT iMaxTableID iTableID
		PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_DATA ------------")
		IF sRouletteData.playerBD[NATIVE_TO_INT(PLAYER_ID())].iTableID = iTableID
			PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_DATA - CURRENT TABLE")
		ENDIF
		
		PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_DATA - eServerState[", iTableID,"]: ", GET_ROULETTE_SERVER_STATE_NAME(sRouletteData.serverBD.eServerState[iTableID]))
		PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_DATA - eGameState[", iTableID,"]: ", GET_ROULETTE_GAME_STATE_NAME(sRouletteData.serverBD.eGameState[iTableID]))
		PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_DATA - iSpinResult[", iTableID,"]: ", sRouletteData.serverBD.iSpinResult[iTableID])
		PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_DATA - iSpinMaxLoops[", iTableID,"]: ", sRouletteData.serverBD.iSpinMaxLoops[iTableID])
		PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_DATA - stBettingTimer[", iTableID,"]: ", GET_NET_TIMER_STRING(sRouletteData.serverBD.stBettingTimer[iTableID]))
		PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_DATA - niDealerPed[", iTableID,"]: ", NETWORK_DOES_NETWORK_ID_EXIST(sRouletteData.serverBD.niDealerPed[iTableID]))
	ENDREPEAT
	
	INT iSeatID = 0
	INT iMaxSeatID = COUNT_OF(sRouletteData.serverBD.playersUsingSeats)
	REPEAT iMaxSeatID iSeatID	
		PLAYER_INDEX piPlayer = sRouletteData.serverBD.playersUsingSeats[iSeatID]
		IF piPlayer != INVALID_PLAYER_INDEX()
			PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_DATA - playersUsingSeats[", iSeatID,"]: ", GET_PLAYER_NAME(piPlayer))
		ENDIF
	ENDREPEAT
ENDPROC

PROC ROULETTE_DEBUG_PRINT_DATA(ROULETTE_DATA &sRouletteData)
	PRINTLN(DEBUG_ROULETTE, "")
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_DATA")
	
	ROULETTE_DEBUG_PRINT_ROULETTE_DATA_STRUCT(sRouletteData)
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_DATA")
	
	ROULETTE_DEBUG_PRINT_ALL_PLAYER_BD(sRouletteData)
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_DATA")
	
	ROULETTE_DEBUG_PRINT_SERVER_BD(sRouletteData)
ENDPROC

PROC ROULETTE_DEBUG_OVERRIDE_WINNING_NUMBER(ROULETTE_DEBUG &sRouletteDebug, ROULETTE_DATA &sRouletteData)
	IF NOT sRouletteDebug.bOverrideWinningNumber
		sRouletteData.iDebugWinningNumberOverride = -1
		EXIT
	ENDIF
	sRouletteData.iDebugWinningNumberOverride = sRouletteDebug.iWinningNumber
ENDPROC

PROC ROULETTE_DEBUG_INIT()
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_EnableCasinoGameDebugText")
		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
	ENDIF
ENDPROC

PROC ROULETTE_DEBUG_CLEAN_UP()
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_EnableCasinoGameDebugText")
		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(FALSE)
	ENDIF
ENDPROC


PROC ROULETTE_DEBUG_PRINT_CURRENT_HEAVY_METRICS(ROULETTE_DATA &sRouletteData)
	UNUSED_PARAMETER(sRouletteData)
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_CURRENT_HEAVY_METRICS")
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_CURRENT_HEAVY_METRICS -------------------")
	
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_CURRENT_HEAVY_METRICS - m_iBetStraight: ", sRouletteData.sMetrics.m_iBetStraight)
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_CURRENT_HEAVY_METRICS - m_iBetSplit: ", sRouletteData.sMetrics.m_iBetSplit)
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_CURRENT_HEAVY_METRICS - m_iBetStreet: ", sRouletteData.sMetrics.m_iBetStreet)
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_CURRENT_HEAVY_METRICS - m_iBetCorner: ", sRouletteData.sMetrics.m_iBetCorner)
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_CURRENT_HEAVY_METRICS - m_iBetFiveNumber: ", sRouletteData.sMetrics.m_iBetFiveNumber)
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_CURRENT_HEAVY_METRICS - m_iBetSixLine: ", sRouletteData.sMetrics.m_iBetSixLine)
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_CURRENT_HEAVY_METRICS - m_oBetRed: ", sRouletteData.sMetrics.m_oBetRed)
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_CURRENT_HEAVY_METRICS - m_oBetBlack: ", sRouletteData.sMetrics.m_oBetBlack)
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_CURRENT_HEAVY_METRICS - m_oBetOddNumber: ", sRouletteData.sMetrics.m_oBetOddNumber)
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_CURRENT_HEAVY_METRICS - m_oBetEvenNumber: ", sRouletteData.sMetrics.m_oBetEvenNumber)
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_CURRENT_HEAVY_METRICS - m_oBetLowBracket: ", sRouletteData.sMetrics.m_oBetLowBracket)
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_CURRENT_HEAVY_METRICS - m_oBetHighBracket: ", sRouletteData.sMetrics.m_oBetHighBracket)
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_CURRENT_HEAVY_METRICS - m_oBetDozen: ", sRouletteData.sMetrics.m_oBetDozen)
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_CURRENT_HEAVY_METRICS - m_oBetColumn: ", sRouletteData.sMetrics.m_oBetColumn)
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_CURRENT_HEAVY_METRICS - m_winningNumber: ", sRouletteData.sMetrics.m_winningNumber)
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_CURRENT_HEAVY_METRICS - m_winningColour: ", sRouletteData.sMetrics.m_winningColour)
	
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_CURRENT_HEAVY_METRICS - m_gameType: ",  sRouletteData.sMetrics.m_casinoMetric.m_gameType)
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_CURRENT_HEAVY_METRICS - m_matchType: ", sRouletteData.sMetrics.m_casinoMetric.m_matchType)
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_CURRENT_HEAVY_METRICS - m_tableID: ", sRouletteData.sMetrics.m_casinoMetric.m_tableID)
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_CURRENT_HEAVY_METRICS - m_handID: ", sRouletteData.sMetrics.m_casinoMetric.m_handID)
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_CURRENT_HEAVY_METRICS - m_endReason: ", sRouletteData.sMetrics.m_casinoMetric.m_endReason)
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_CURRENT_HEAVY_METRICS - m_allIn: ", sRouletteData.sMetrics.m_casinoMetric.m_allIn)
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_CURRENT_HEAVY_METRICS - m_chipsDelta: ", sRouletteData.sMetrics.m_casinoMetric.m_chipsDelta)
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_CURRENT_HEAVY_METRICS - m_finalChipBalance: ", sRouletteData.sMetrics.m_casinoMetric.m_finalChipBalance)
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_CURRENT_HEAVY_METRICS - m_totalPot: ", sRouletteData.sMetrics.m_casinoMetric.m_totalPot)
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_CURRENT_HEAVY_METRICS - m_playersAtTable: ", sRouletteData.sMetrics.m_casinoMetric.m_playersAtTable)
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_CURRENT_HEAVY_METRICS - m_viewedLegalScreen: ", sRouletteData.sMetrics.m_casinoMetric.m_viewedLegalScreen)
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_CURRENT_HEAVY_METRICS - m_ownPenthouse: ", sRouletteData.sMetrics.m_casinoMetric.m_ownPenthouse)
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_CURRENT_HEAVY_METRICS - m_betAmount1: ", sRouletteData.sMetrics.m_casinoMetric.m_betAmount1)
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_CURRENT_HEAVY_METRICS - m_betAmount2: ", sRouletteData.sMetrics.m_casinoMetric.m_betAmount2)
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_CURRENT_HEAVY_METRICS - m_winAmount: ", sRouletteData.sMetrics.m_casinoMetric.m_winAmount)
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_CURRENT_HEAVY_METRICS - m_win: ", sRouletteData.sMetrics.m_casinoMetric.m_win)
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_CURRENT_HEAVY_METRICS - m_cheat: ", sRouletteData.sMetrics.m_casinoMetric.m_cheat)
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_CURRENT_HEAVY_METRICS - m_timePlayed: ", sRouletteData.sMetrics.m_casinoMetric.m_timePlayed)
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_CURRENT_HEAVY_METRICS - m_isHost: ", sRouletteData.sMetrics.m_casinoMetric.m_isHost)
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_CURRENT_HEAVY_METRICS - m_hostID: ", sRouletteData.sMetrics.m_casinoMetric.m_hostID)
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_CURRENT_HEAVY_METRICS - m_membership: ", sRouletteData.sMetrics.m_casinoMetric.m_membership)
	
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_CURRENT_HEAVY_METRICS -------------------")
ENDPROC

PROC ROULETTE_DEBUG_PRINT_CURRENT_LIGHT_METRICS(ROULETTE_DATA &sRouletteData)
	UNUSED_PARAMETER(sRouletteData)
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_CURRENT_LIGHT_METRICS")
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_CURRENT_LIGHT_METRICS -------------------")
	
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_CURRENT_LIGHT_METRICS - m_matchType: ", sRouletteData.sMetricsLight.m_matchType)
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_CURRENT_LIGHT_METRICS - m_tableID: ", sRouletteData.sMetricsLight.m_tableID)
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_CURRENT_LIGHT_METRICS - m_endReason: ", sRouletteData.sMetricsLight.m_endReason)
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_CURRENT_LIGHT_METRICS - m_chipsDelta: ", sRouletteData.sMetricsLight.m_chipsDelta)
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_CURRENT_LIGHT_METRICS - m_finalChipBalance: ", sRouletteData.sMetricsLight.m_finalChipBalance)
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_CURRENT_LIGHT_METRICS - m_duration: ", sRouletteData.sMetricsLight.m_duration)
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_CURRENT_LIGHT_METRICS - m_viewedLegalScreen: ", sRouletteData.sMetricsLight.m_viewedLegalScreen)
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_CURRENT_LIGHT_METRICS - m_betAmount1: ", sRouletteData.sMetricsLight.m_betAmount1)
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_CURRENT_LIGHT_METRICS - m_betAmount2: ", sRouletteData.sMetricsLight.m_betAmount2)
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_CURRENT_LIGHT_METRICS - m_cheatCount: ", sRouletteData.sMetricsLight.m_cheatCount)
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_CURRENT_LIGHT_METRICS - m_isHost: ", sRouletteData.sMetricsLight.m_isHost)
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_CURRENT_LIGHT_METRICS - m_hostID: ", sRouletteData.sMetricsLight.m_hostID)
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_CURRENT_LIGHT_METRICS - m_handsPlayed: ", sRouletteData.sMetricsLight.m_handsPlayed)
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_CURRENT_LIGHT_METRICS - m_winCount: ", sRouletteData.sMetricsLight.m_winCount)
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_CURRENT_LIGHT_METRICS - m_loseCount: ", sRouletteData.sMetricsLight.m_loseCount)
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_CURRENT_LIGHT_METRICS - m_membership: ", sRouletteData.sMetricsLight.m_membership)
	
	PRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_CURRENT_LIGHT_METRICS -------------------")
ENDPROC

PROC ROULETTE_DEBUG_PAUSE_BETTING_TIME(ROULETTE_DEBUG &sRouletteDebug, ROULETTE_DATA &sRouletteData)
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
		EXIT
	ENDIF
	
	IF NOT sRouletteDebug.bBettingTime
		EXIT
	ENDIF
	
	INT iTableID = sRouletteData.iTableID
	IF iTableID <= -1
		EXIT
	ENDIF
	
	RESET_NET_TIMER(sRouletteData.serverBD.stBettingTimer[iTableID])
	START_NET_TIMER(sRouletteData.serverBD.stBettingTimer[iTableID])
ENDPROC

PROC ROULETTE_DEBUG_HIDE_UI(ROULETTE_DEBUG &sRouletteDebug, ROULETTE_DATA &sRouletteData)
	sRouletteData.bHideUI = sRouletteDebug.bHideUI
ENDPROC

PROC ROULETTE_DEBUG_SHOW_LOSE_STREAK(ROULETTE_DEBUG &sRouletteDebug, ROULETTE_DATA &sRouletteData)
	IF NOT sRouletteDebug.bShowLoseStreak
		EXIT
	ENDIF
	
	INT iLoseStreak = sRouletteData.iLoseStreak
	
	TEXT_LABEL_63 tlLoseStreak = "Lose Streak: "
	tlLoseStreak += iLoseStreak
	
	IF iLoseStreak >= ROULETTE_ANIM_MAX_LOSS_STREAK_BEFORE_TERRIBLE_REACTION_CAN_TRIGGER
		DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_TL(tlLoseStreak), <<0.1, 0.1, 0.0>>, 255, 0, 0)
	ELSE
		DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_TL(tlLoseStreak), <<0.1, 0.1, 0.0>>, 0, 255, 0)
	ENDIF
ENDPROC

PROC ROULETTE_DEBUG_SHOW_SWITCH_CAMERA_INFO(ROULETTE_DEBUG &sRouletteDebug, ROULETTE_DATA &sRouletteData)
	IF NOT sRouletteDebug.bDisplayCamSwitchInfo
		EXIT
	ENDIF
	
	TEXT_LABEL_63 tlSwitchIndex = "Switch index: "
	tlSwitchIndex += sRouletteData.iSwitchCamIndex
	tlSwitchIndex += ", Last non layout index: "
	tlSwitchIndex += sRouletteData.iLastNonLayoutSwitchCam
	DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_TL(tlSwitchIndex), <<0.1, 0.3, 0.0>>, 255, 255, 255)
	
	TEXT_LABEL_63 tlCamOrder = "Order: "
	
	INT i = 0
	REPEAT 3 i
		tlCamOrder += ROULETTE_CAM_VIEW_GET_CAM_VIEW_AS_STRING(sRouletteData.eSwitchableCameraView[i])
		
		IF i != 2
			tlCamOrder += ", "
		ENDIF		
	ENDREPEAT
	
	DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_TL(tlCamOrder), <<0.1, 0.325, 0.0>>, 255, 255, 255)
ENDPROC

/// PURPOSE:
///    Updates and displays debug for the roulette minigame
PROC ROULETTE_DEBUG_UPDATE(ROULETTE_DEBUG &sRouletteDebug, ROULETTE_DATA &sRouletteData)
	ROULETTE_DEBUG_DRAW_SEATS(sRouletteDebug, sRouletteData)
	ROULETTE_DEBUG_DRAW_TABLES(sRouletteDebug, sRouletteData)
	ROULETTE_DEBUG_DRAW_LAYOUT(sRouletteDebug)
	ROULETTE_DEBUG_DRAW_CURSOR(sRouletteDebug, sRouletteData)
	ROULETTE_DEBUG_DRAW_LOCAL_PLAYERS_BETS(sRouletteDebug, sRouletteData)
	ROULETTE_DEBUG_SHOW_CAMERA_VIEWS(sRouletteDebug, sRouletteData)
	ROULETTE_DEBUG_DRAW_CURSOR_BOUNDS(sRouletteDebug, sRouletteData)
	ROULETTE_DEBUG_OVERRIDE_WINNING_NUMBER(sRouletteDebug, sRouletteData)
	ROULETTE_DEBUG_PAUSE_BETTING_TIME(sRouletteDebug, sRouletteData)
	ROULETTE_DEBUG_HIDE_UI(sRouletteDebug, sRouletteData)
	ROULETTE_DEBUG_SHOW_LOSE_STREAK(sRouletteDebug, sRouletteData)
	ROULETTE_DEBUG_SHOW_SWITCH_CAMERA_INFO(sRouletteDebug, sRouletteData)
	
	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_R, KEYBOARD_MODIFIER_CTRL, "Print roulette data") 
	OR IS_DEBUG_KEY_JUST_PRESSED(KEY_SPACE, KEYBOARD_MODIFIER_NONE, "Print roulette data")
		ROULETTE_DEBUG_PRINT_DATA(sRouletteData)
	ENDIF
ENDPROC
#ENDIF
