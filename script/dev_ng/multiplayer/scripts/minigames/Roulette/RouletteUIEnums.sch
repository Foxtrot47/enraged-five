//////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Name:        RouletteUIEnums.sch																			
/// Description: Header for the roulette UI enums								
///																											
/// Written by:  Online Technical Team: Tom Turner,															
/// Date:  		13/05/2019																					
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
   
ENUM ROULETTE_UI_DISCLOSURE_STATUS
	ROULETTE_UI_DISCLOSURE_WAITING_FOR_INPUT,
	ROULETTE_UI_DISCLOSURE_DECLINED,
	ROULETTE_UI_DISCLOSURE_ACCEPTED
ENDENUM

ENUM ROULETTE_UI_RULES_PAGE
	ROULETTE_UI_RULES_PAGE_INSIDE_BETS,
	ROULETTE_UI_RULES_PAGE_OUTSIDE_BETS,
	ROULETTE_UI_RULES_PAGE_BET_LIMITS
ENDENUM

ENUM ROULETTE_UI_BUTTON_HIDE
	ROULETTE_UI_BUTTON_HIDE_NONE,
	ROULETTE_UI_BUTTON_HIDE_ALL,
	ROULETTE_UI_BUTTON_HIDE_CAMERA_RELATED,
	ROULETTE_UI_BUTTON_HIDE_ACTION_RELATED
ENDENUM
