//////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Name:        	CasinoRouletteOverrides.sch																		
/// Description: 	Header file that contains roulette minigame overrides for casino roulette							
///																											
/// Written by:  Online Technical Team: Tom Turner,															
/// Date:  		28/02/2019
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "Globals.sch"
USING "net_casino.sch"
USING "net_realty_casino.sch"
USING "net_simple_interior.sch"

CONST_INT ROULETTE_TABLE_MAX_NUMBER_OF_TABLES 6
CONST_INT ROULETTE_LOAD_DISTANCE 20
CONST_INT ROULETTE_LOAD_DISTANCE_SQUARED ROULETTE_LOAD_DISTANCE * ROULETTE_LOAD_DISTANCE
CONST_INT ROULETTE_CLEANUP_DISTANCE ROULETTE_LOAD_DISTANCE + 1
CONST_INT ROULETTE_CLEANUP_DISTANCE_SQUARED ROULETTE_CLEANUP_DISTANCE * ROULETTE_CLEANUP_DISTANCE

VECTOR vRouletteLoadOrigin = <<1140.7462, 258.6841, -52.4408>>

ENUM ROULETTE_GANG_MEMBERSHIP_TYPE
	RGMT_INVALID = -1,
	RGMT_VIP,
	RGMT_CEO,
	RGMT_BIKER
ENDENUM
	
GENERIC_TRANSACTION_STATE m_ePaymentTransactionState
GENERIC_TRANSACTION_STATE m_ePayoutTransactionState
INTERIOR_INSTANCE_INDEX m_iInteriorID = NULL
BOOL m_bJoinedWithGangMembership = FALSE
ROULETTE_GANG_MEMBERSHIP_TYPE m_eGangMembershipType = RGMT_INVALID

///-------------------------------------------
///    Function overrides for casino roulette
///-------------------------------------------   

/// PURPOSE:
///    Gets the position of the table in the casino according to its id
FUNC VECTOR ROULETTE_TABLE_GET_POSITION(INT iTableId)
	SWITCH iTableId
		CASE 0	RETURN <<1144.8137, 268.2634, -52.8409>>
		CASE 1	RETURN <<1150.3547, 262.7224, -52.8409>>
		CASE 2	RETURN <<1133.9583, 262.1071, -52.0409>>
		CASE 3	RETURN <<1129.5952, 267.2637, -52.0409>>
		CASE 4	RETURN <<1144.6178, 252.2411, -52.0409>>
		CASE 5	RETURN <<1148.9808, 247.0846, -52.0409>>
	ENDSWITCH
	
	RETURN <<0, 0, 0>>	
ENDFUNC

/// PURPOSE:
///    Gets the tables heading  
FUNC FLOAT ROULETTE_TABLE_GET_HEADING(INT iTableId)
	SWITCH iTableId
		CASE 0	RETURN -135.0
		CASE 1	RETURN 45.0
		CASE 2	RETURN 135.0
		CASE 3	RETURN -45.0
		CASE 4	RETURN -45.0
		CASE 5	RETURN 135.0
	ENDSWITCH
	
	RETURN 0.0	
ENDFUNC

/// PURPOSE:
///    Queries if the table ID is a high stakes table   
FUNC BOOL ROULETTE_TABLE_IS_HIGH_STAKES(INT iTableID) 
	SWITCH iTableId
		CASE 2
		CASE 3
		CASE 4
		CASE 5
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Updates m_iInteriorID with the casino interior ID if it hasn't done so already
PROC UPDATE_CASINO_INTERIOR_ID()
	IF m_iInteriorID != NULL
		EXIT
	ENDIF
	
	TEXT_LABEL_63 sInteriorType
	VECTOR vInteriorPosition
	FLOAT fInteriorHeading
	GET_SIMPLE_INTERIOR_INTERIOR_TYPE_AND_POSITION(SIMPLE_INTERIOR_CASINO, sInteriorType, vInteriorPosition, fInteriorHeading, TRUE)
	m_iInteriorID = GET_INTERIOR_AT_COORDS_WITH_TYPE(vInteriorPosition, sInteriorType)
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_PROPS_PROCESS_CREATED_TABLE - interior ID = ", GET_STRING_FROM_TL(sInteriorType))
ENDPROC

/// PURPOSE:
///    Process the created tables
PROC ROULETTE_PROPS_PROCESS_CREATED_TABLE(OBJECT_INDEX &objTable, INT iTableID)	
	UNUSED_PARAMETER(iTableID)
	UNUSED_PARAMETER(objTable)
	
	UPDATE_CASINO_INTERIOR_ID()
	
	IF NOT IS_VALID_INTERIOR(m_iInteriorID)
		EXIT
	ENDIF
	
	IF NOT IS_INTERIOR_READY(m_iInteriorID)
		EXIT
	ENDIF
	
	IF ROULETTE_TABLE_IS_HIGH_STAKES(iTableID)
		FORCE_ROOM_FOR_ENTITY(objTable, m_iInteriorID, ENUM_TO_INT(CASINO_GAMING_FLOOR_2_ROOM))
	ELSE
		FORCE_ROOM_FOR_ENTITY(objTable, m_iInteriorID, ENUM_TO_INT(CASINO_GAMING_FLOOR_3_ROOM))
	ENDIF
ENDPROC

/// PURPOSE:
///    Casino specific bet placing restrictions   
FUNC BOOL ROULETTE_BETTING_CAN_PLACE_BET(INT iBetValue, INT iTotalBetForRound)
	UNUSED_PARAMETER(iTotalBetForRound)
	UNUSED_PARAMETER(iBetValue)
	RETURN NOT IS_LOCAL_PLAYER_BANNED_FROM_BETTING()
ENDFUNC

/// PURPOSE:
///    Marks the table s disable meaning it isn't updated and doesn't have objects/peds spawned at it
FUNC BOOL ROULETTE_TABLE_IS_DISABLED(INT iTableID)
	RETURN g_iPedReservedRouletteTable = iTableID
ENDFUNC

/// PURPOSE:
///    Used by roulette core to determine how many chips the player has   
FUNC INT ROULETTE_BETTING_GET_LOCAL_PLAYER_CHIP_COUNT()
	RETURN GET_LOCAL_PLAYERS_CASINO_CHIPS_TOTAL()
ENDFUNC

/// PURPOSE:
///    Event triggered when player joins a game of roulette
PROC ROULETTE_EVENT_ON_JOIN_GAME()
	SET_PLAYER_IS_PLAYING_CASINO_GAME(CG_ROULETTE)
ENDPROC

// Event triggered when player leaves a game of roulette 
PROC ROULETTE_EVENT_ON_LEAVE_GAME()
	SET_PLAYER_FINISHED_PLAYING_CASINO_GAME()
	m_bJoinedWithGangMembership = FALSE
	m_eGangMembershipType = RGMT_INVALID
ENDPROC

/// PURPOSE:
///    Checks if the tables dealer should be a woman   
FUNC BOOL ROULETTE_PROPS_SHOULD_TABLE_DEALER_BE_FEMALE(INT iTableID)
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_PROPS_SHOULD_TABLE_DEALER_BE_FEMALE - iTableID: ", iTableID)
	RETURN g_eRouletteDealerPeds[iTableID] >= CASINO_DEALER_FEMALE_VAR1
ENDFUNC

/// PURPOSE:
///    Gets the players current ban reason as a hash    
FUNC INT GET_BAN_REASON_AS_HASH()
	SWITCH GET_CASINO_GAME_BAN_REASON()
		CASE CBR_WON_TOO_MUCH		RETURN GET_HASH_KEY("win cutoff")
		CASE CBR_LOST_TOO_MUCH		RETURN GET_HASH_KEY("loss cutoff")
		CASE CBR_TIME_SPENT_PLAYING	RETURN GET_HASH_KEY("time cutoff")
	ENDSWITCH
	RETURN -1
ENDFUNC

/// PURPOSE:
///    Checks if the player has access to a VIP membership taking into account
///    their bosses membership
FUNC BOOL DOES_PLAYER_HAVE_ACCESS_TO_VIP_MEMBERSHIP()
	RETURN DOES_PLAYER_OWN_A_CASINO_APARTMENT(PLAYER_ID()) 
	OR DOES_LOCAL_PLAYERS_BOSS_OWN_A_CASINO_APARTMENT()
ENDFUNC

/// PURPOSE:
///    handles purchasing a casino membership at the roulette table
///    returns true if transaction was successful
FUNC BOOL PROCESS_MEMBERSHIP_PURCHASE_AT_TABLE() 
	INT iMembershipCost = GET_CASINO_MEMBERSHIP_COST()
	
	IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CAS_MG_MEMB_PF")
	AND NOT IS_CASINO_MEMBERSHIP_TRANSACTION_ONGOING()
		BEGIN_TEXT_COMMAND_DISPLAY_HELP("CAS_MG_MEMB2")
			ADD_TEXT_COMPONENT_INTEGER(iMembershipCost)
		END_TEXT_COMMAND_DISPLAY_HELP(HELP_TEXT_SLOT_STANDARD, FALSE, TRUE)
	ENDIF
	
	#IF IS_DEBUG_BUILD
		// Can't afford membership so show standard no membership message
		IF GET_PLAYER_CASH(PLAYER_ID()) < iMembershipCost
			CPRINTLN(DEBUG_ROULETTE, "PROCESS_MEMBERSHIP_PURCHASE_AT_TABLE - Not enough money")
		ENDIF
	#ENDIF
	
	// If transaction hasn't been made process input to start transaction
	IF NOT IS_CASINO_MEMBERSHIP_TRANSACTION_ONGOING()
	AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CAS_MG_MEMB_PF")
		CPRINTLN(DEBUG_ROULETTE, "PROCESS_MEMBERSHIP_PURCHASE_AT_TABLE - No active transaction - processing input")
		
		// Start transaction if player accepts
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT_SECONDARY)
			CPRINTLN(DEBUG_ROULETTE, "PROCESS_MEMBERSHIP_PURCHASE_AT_TABLE - Input recieved - requesting purchase")
			REQUEST_CASINO_MEMBERSHIP_PURCHASE()
		ENDIF
		
		RETURN FALSE
	ENDIF
	
	CPRINTLN(DEBUG_ROULETTE, "PROCESS_MEMBERSHIP_PURCHASE_AT_TABLE - Transaction in progress - waiting for result")
	
	// Transaction failed
	IF IS_BIT_SET(g_SimpleInteriorData.iSixthBS, BS6_SIMPLE_INTERIOR_CASINO_PURCHASE_MEMBERSHIP_FAILED)
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CAS_MG_MEMB_PF")
		CPRINTLN(DEBUG_ROULETTE, "PROCESS_MEMBERSHIP_PURCHASE_AT_TABLE - Transaction in progess - transaction failed")
		RETURN FALSE
	ENDIF
	
	BOOL bPurchaseSuccess = HAS_LOCAL_PLAYER_PURCHASED_CASINO_MEMBERSHIP()
	
	// If successful clear help
	IF bPurchaseSuccess
		CPRINTLN(DEBUG_ROULETTE, "PROCESS_MEMBERSHIP_PURCHASE_AT_TABLE - Transaction in progess - transaction succeded, clearing help")
		CLEAR_HELP(TRUE)
	ENDIF
	
	RETURN bPurchaseSuccess
ENDFUNC

/// PURPOSE:
///    Override for preventing dealers talking locally if the player
///    is using another minigame
FUNC BOOL ROULETTE_AUDIO_CAN_DEALER_TALK(INT iTableID)
	UNUSED_PARAMETER(iTableID)
	
	IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSFive, BS_SIMPLE_INTERIOR_GLOBAL_BS_FIVE_PLAYING_BLACKJACK)
	OR IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSFive, BS_SIMPLE_INTERIOR_GLOBAL_BS_FIVE_PLAYING_THREE_CARD_POKER)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_AUDIO_CAN_DEALER_TALK - dealer can't talk")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE		
ENDFUNC

/// PURPOSE:
///    Checks if a chip transaction is currently in progress    
FUNC BOOL IS_CHIP_TRANSACTION_IN_PROGRESS()
	
	IF IS_BIT_SET(g_iChipsHudBS, HUD_CHIPS_BS_REQUEST_CHIP_CHANGE_DISPLAY)
		RETURN TRUE
	ENDIF
	
	INT iTransactionIndex = GET_BASKET_TRANSACTION_SCRIPT_INDEX()
	IF iTransactionIndex = -1
		RETURN FALSE
	ENDIF
	
	RETURN NOT IS_CASH_TRANSACTION_COMPLETE(iTransactionIndex)
ENDFUNC

/// PURPOSE:
///    Displays a disbanded message according to the gang type the player 
///    joined the table with
PROC DISPLAY_APPROPRIATE_GANG_DISBANDED_MESSAGE()
	SWITCH m_eGangMembershipType
		CASE RGMT_BIKER PRINT_HELP("ROUL_DISBAND_A", DEFAULT_HELP_TEXT_TIME)	EXIT
		CASE RGMT_CEO PRINT_HELP("ROUL_DISBAND_B", DEFAULT_HELP_TEXT_TIME)	EXIT
		CASE RGMT_VIP PRINT_HELP("ROUL_DISBAND_C", DEFAULT_HELP_TEXT_TIME)	EXIT
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Checks if the local player is playing a casino game other than roulette   
FUNC BOOL IS_PLAYER_ALREADY_PLAYING_A_CASINO_GAME()
	INT iPlayerID = NATIVE_TO_INT(PLAYER_ID())
	RETURN IS_BIT_SET(GlobalplayerBD[iPlayerID].SimpleInteriorBD.iBSFive, BS_SIMPLE_INTERIOR_GLOBAL_BS_FIVE_PLAYING_BLACKJACK)
	OR IS_BIT_SET(GlobalplayerBD[iPlayerID].SimpleInteriorBD.iBSFive, BS_SIMPLE_INTERIOR_GLOBAL_BS_FIVE_PLAYING_THREE_CARD_POKER)
	OR IS_BIT_SET(GlobalplayerBD[iPlayerID].SimpleInteriorBD.iBSFive, BS_SIMPLE_INTERIOR_GLOBAL_BS_FIVE_PLAYING_LUCKY_WHEEL)
	OR IS_BIT_SET(GlobalplayerBD[iPlayerID].SimpleInteriorBD.iBSFive, BS_SIMPLE_INTERIOR_GLOBAL_BS_FIVE_PLAYING_SLOTS)
ENDFUNC

/// PURPOSE:
///    Blocks roulette and displays the casino specific message to why it is unavailable
FUNC BOOL ROULETTE_DISPLAY_ANY_UNAVAILABLE_MESSAGE(INT iSeatID, INT iTableID, INT iMinChipsRequired, INT &iUnavailableReason, BOOL bAlreadyPlaying)
	UNUSED_PARAMETER(iSeatID)	
	BOOL bIsHighStakesTable = ROULETTE_TABLE_IS_HIGH_STAKES(iTableID)	
	INT iMessageDuration = -1
	
	/// If the player is playing, they will be removed from the table
	/// so make sure help messages have duration as they won't be facing the table to see them
	IF bAlreadyPlaying
		iMessageDuration = DEFAULT_HELP_TEXT_TIME
	ENDIF
	
	// url:bugstar:6019286 fix exploit of players merging casino games
	IF IS_PLAYER_ALREADY_PLAYING_A_CASINO_GAME()
	OR ARE_CLOUDS_FADING_IN()
		RETURN TRUE
	ENDIF	
	
	IF SHOULD_ALLOW_CASINO_ENTRY_DURING_MISSION()
		RETURN TRUE
	ENDIF
	
	IF IS_LOCAL_PLAYER_BANNED_FROM_BETTING()
		iUnavailableReason = GET_BAN_REASON_AS_HASH()
		
		IF GET_CASINO_GAME_BAN_REASON() = CBR_TIME_SPENT_PLAYING
			PRINT_HELP("CAS_MG_CTIME", iMessageDuration)
			RETURN TRUE
		ENDIF
		
		PRINT_HELP("CAS_MG_CBAN", iMessageDuration)
		RETURN TRUE
	ENDIF
	
	INT iBlockedReason
	IF IS_PLAYER_BLOCKED_FROM_CASINO_GAME(CG_ROULETTE, iBlockedReason)
		iUnavailableReason = GET_HASH_KEY("roulette is blocked")
		PRINT_HELP("CAS_MG_REGBAN", iMessageDuration)
		RETURN TRUE
	ENDIF
	
	IF NOT DOES_PLAYER_HAVE_ACCESS_TO_VIP_MEMBERSHIP()
	AND bIsHighStakesTable
		iUnavailableReason = GET_HASH_KEY("no access to highstakes tables")
		
		// Different message if they were playing on a gang membership
		IF bAlreadyPlaying AND m_bJoinedWithGangMembership
			DISPLAY_APPROPRIATE_GANG_DISBANDED_MESSAGE()
			RETURN TRUE
		ENDIF
		
		PRINT_HELP("CAS_MG_SUITE2", iMessageDuration)
		RETURN TRUE
	ENDIF
	
	// If the player and their boss doesn't have a casino membership
	IF NOT HAS_LOCAL_PLAYER_OR_THEIR_BOSS_PURCHASED_CASINO_MEMBERSHIP()
		iUnavailableReason = GET_HASH_KEY("no membership")
		
		// Different message if they were playing on a gang membership
		IF bAlreadyPlaying AND m_bJoinedWithGangMembership
			DISPLAY_APPROPRIATE_GANG_DISBANDED_MESSAGE()
			RETURN TRUE
		ENDIF
		
		RETURN NOT PROCESS_MEMBERSHIP_PURCHASE_AT_TABLE()
	ENDIF
	
	INT iPlayerChipCount = GET_LOCAL_PLAYERS_CASINO_CHIPS_TOTAL()
	
	IF iPlayerChipCount <= 0
		iUnavailableReason = GET_HASH_KEY("no chips")
		
		IF NOT IS_CHIP_TRANSACTION_IN_PROGRESS()
			PRINT_HELP("CAS_MG_NOCHIPS4", iMessageDuration)
		ENDIF
		RETURN TRUE
	ENDIF
	
	IF iPlayerChipCount < iMinChipsRequired
		iUnavailableReason = GET_HASH_KEY("low chips")
		
		IF NOT IS_CHIP_TRANSACTION_IN_PROGRESS()
			PRINT_HELP("CAS_MG_LOWCHIPS4", iMessageDuration)
		ENDIF
		RETURN TRUE
	ENDIF
	
	IF g_sMPtunables.bVC_CASINO_DISABLE_ROULETTE
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_DISPLAY_ANY_UNAVAILABLE_MESSAGE - Blocked by VC_CASINO_DISABLE_ROULETTE tunable")
		RETURN TRUE	
	ENDIF	
	
	IF bIsHighStakesTable AND g_sMPtunables.bVC_CASINO_DISABLE_ROULETTE_HIGH
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_DISPLAY_ANY_UNAVAILABLE_MESSAGE - Blocked by VC_CASINO_DISABLE_ROULETTE_HIGH tunable")
		RETURN TRUE
	ENDIF
	
	IF NOT bIsHighStakesTable AND g_sMPtunables.bVC_CASINO_DISABLE_ROULETTE_LOW
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_DISPLAY_ANY_UNAVAILABLE_MESSAGE - Blocked by VC_CASINO_DISABLE_ROULETTE_LOW tunable")
		RETURN TRUE
	ENDIF
	
	IF SHOULD_PLAYER_BE_REMOVED_FROM_CASINO_GAME_FOR_BOSS_LAUNCHING_MISSION()
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_DISPLAY_ANY_UNAVAILABLE_MESSAGE - Blocked by SHOULD_PLAYER_BE_REMOVED_FROM_CASINO_GAME_FOR_BOSS_LAUNCHING_MISSION")
		RETURN TRUE
	ENDIF
	
	IF IS_FM_JOB_ENTRY_OF_TYPE_THAT_SHOULD_BLOCK_ACCESS()
		PRINTLN("ROULETTE_DISPLAY_ANY_UNAVAILABLE_MESSAGE - Blocked GET_FM_JOB_ENTERY_TYPE() = ", GET_FM_JOB_ENTERY_TYPE())
		RETURN TRUE
	ENDIF
	
	// No reason to block casino roulette
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Gets the join roulette table prompt taking into account the players boss type
FUNC STRING GET_PROMPT_FOR_LOCAL_PLAYERS_BOSS_TYPE()
	PLAYER_INDEX piPlayerBoss = GB_GET_THIS_PLAYER_GANG_BOSS(PLAYER_ID())
	IF piPlayerBoss = INVALID_PLAYER_INDEX()
		CPRINTLN(DEBUG_ROULETTE, "GET_PROMPT_FOR_LOCAL_PLAYERS_BOSS_TYPE - boss invalid")
		RETURN "ROUL_PROMPT"
	ENDIF
	
	IF GB_GET_PLAYER_GANG_TYPE(piPlayerBoss) = GT_BIKER
		m_eGangMembershipType = RGMT_BIKER
		RETURN "ROUL_PROMPT_A"
	ELIF DOES_PLAYER_OWN_OFFICE(piPlayerBoss)
		m_eGangMembershipType = RGMT_CEO
		RETURN "ROUL_PROMPT_B"
	ELSE
		m_eGangMembershipType = RGMT_VIP
		RETURN "ROUL_PROMPT_C"
	ENDIF
	
	m_eGangMembershipType = RGMT_INVALID
	CPRINTLN(DEBUG_ROULETTE, "GET_PROMPT_FOR_LOCAL_PLAYERS_BOSS_TYPE - boss type not found")
	RETURN "ROUL_PROMPT"
ENDFUNC

/// PURPOSE:
///    Gets the text label string for the start roulette prompt  
FUNC STRING ROULETTE_UI_GET_START_PROMPT_TEXT_LABEL_STRING(INT iTableID)	
	m_bJoinedWithGangMembership = FALSE
	
	IF DOES_LOCAL_PLAYERS_BOSS_OWN_A_CASINO_APARTMENT()
	AND NOT DOES_PLAYER_OWN_A_CASINO_APARTMENT(PLAYER_ID()) 
	AND ROULETTE_TABLE_IS_HIGH_STAKES(iTableID)
		m_bJoinedWithGangMembership = TRUE
		
		RETURN GET_PROMPT_FOR_LOCAL_PLAYERS_BOSS_TYPE()
	ENDIF
	
	// Using boss membership
	IF HAS_LOCAL_PLAYERS_BOSS_PURCHASED_CASINO_MEMBERSHIP() 
	AND NOT HAS_LOCAL_PLAYER_PURCHASED_CASINO_MEMBERSHIP()
		m_bJoinedWithGangMembership = TRUE
		RETURN GET_PROMPT_FOR_LOCAL_PLAYERS_BOSS_TYPE()
	ENDIF
	
	m_bJoinedWithGangMembership = FALSE
	RETURN "ROUL_PROMPT"
ENDFUNC

/// PURPOSE:
///    Handles casino chip transaction once bets are placed
FUNC GENERIC_TRANSACTION_STATE ROULETTE_BETTING_PLACE_BET_TRANSACTION(INT iTotalBetForRound)	
	IF iTotalBetForRound <= 0
		RETURN TRANSACTION_STATE_SUCCESS
	ENDIF
	
	LOCAL_PLAYER_PLACE_CASINO_BET(iTotalBetForRound, m_ePaymentTransactionState, CG_ROULETTE)
	RETURN m_ePaymentTransactionState
ENDFUNC

/// PURPOSE:
///    Event for when the player loses
PROC ROULETTTE_EVENT_ON_LOSE(INT iAmountLost)
	ADD_TO_CASINO_CHIPS_LOST_ON_BETTING_THIS_GAMEDAY(iAmountLost)
ENDPROC

/// PURPOSE:
///    Handles casino payout transaction after wheel spin
FUNC GENERIC_TRANSACTION_STATE ROULETTE_BETTING_PAYOUT_TRANSACTION(INT iTotalPayoutForRound)
	CASINO_PAYOUT_REWARD_TO_LOCAL_PLAYER(iTotalPayoutForRound, m_ePayoutTransactionState, CG_ROULETTE)
	RETURN m_ePayoutTransactionState
ENDFUNC

/// PURPOSE:
///    Override for the dealer peds components
PROC ROULETTE_PROPS_SET_DEALER_PED_COMPONENTS(PED_INDEX &piDealer, BOOL bIsFemale, INT iTableID)
	UNUSED_PARAMETER(bIsFemale)
	GET_DEALER_PED_DATA(g_eRouletteDealerPeds[iTableID], piDealer)
	
	UPDATE_CASINO_INTERIOR_ID()
	
	IF NOT IS_VALID_INTERIOR(m_iInteriorID)
		EXIT
	ENDIF

	IF NOT IS_INTERIOR_READY(m_iInteriorID)
		EXIT
	ENDIF
	
	IF ROULETTE_TABLE_IS_HIGH_STAKES(iTableID)
		FORCE_ROOM_FOR_ENTITY(piDealer, m_iInteriorID, ENUM_TO_INT(CASINO_GAMING_FLOOR_2_ROOM))
	ELSE
		FORCE_ROOM_FOR_ENTITY(piDealer, m_iInteriorID, ENUM_TO_INT(CASINO_GAMING_FLOOR_3_ROOM))
	ENDIF
ENDPROC

/// PURPOSE:
///    Casino override for making chips only show when in the same room as tables    
FUNC BOOL ROULETTE_PROPS_SHOULD_CHIPS_BE_CREATED()
	RETURN IS_LOCAL_PLAYER_PED_IN_CASINO_ROOM(CASINO_GAMING_FLOOR_2_ROOM)
	OR IS_LOCAL_PLAYER_PED_IN_CASINO_ROOM(CASINO_GAMING_FLOOR_3_ROOM)
ENDFUNC

/// PURPOSE:
///    Override for the dealer peds voice groups
PROC ROULETTE_PROPS_SET_DEALER_PED_VOICE_GROUP(PED_INDEX &piDealer, BOOL bIsFemale, INT iTableID)
	UNUSED_PARAMETER(bIsFemale)
	GET_DEALER_PED_VOICE_GROUP(g_eRouletteDealerPeds[iTableID], piDealer)
ENDPROC

FUNC BOOL SHOULD_ROULETTE_STAY_LOADED_IN_CURRENT_ROOM()
	INT iKey = GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID()) 
	RETURN iKey = HASH("rm_Elevator_01")
	OR iKey = HASH("rm_Stairs_01")
	OR iKey = HASH("rm_Toilet_02")
ENDFUNC

/// PURPOSE:
///    Clean up checks for casino specific roulette   
FUNC BOOL ROULETTE_CUSTOM_CLEANUP_CHECKS()
	// Always trigger a clean up if not in the casino
	IF NOT IS_PLAYER_IN_CASINO(PLAYER_ID())
		RETURN TRUE
	ENDIF
	
	// Clean up if the player is far enough away
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
	AND GET_DISTANCE_BETWEEN_COORDS_SQUARED(GET_ENTITY_COORDS(PLAYER_PED_ID()), vRouletteLoadOrigin) > ROULETTE_CLEANUP_DISTANCE_SQUARED
	AND NOT SHOULD_ROULETTE_STAY_LOADED_IN_CURRENT_ROOM()
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_CUSTOM_CLEANUP_CHECKS - distance clean up")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Hides the ball during the casino cutscene intro   
FUNC BOOL ROULETTE_PROPS_SHOULD_HIDE_BALL(INT iTableID)
	UNUSED_PARAMETER(iTableID)
	RETURN IS_CUTSCENE_ACTIVE()
ENDFUNC
