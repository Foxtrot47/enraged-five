//////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Name:        RouletteGameClient.sch																		
/// Description: Header for running the client side roulette game logic, this is started and stopped 		
///				by the main Roulette client when the player joins a table. Note this does not init or		
///				cleanup assets, it only uses the ones setup by the main script to run the game logic		
///				as the player can join and leave games freely												
///																											
/// Written by:  Online Technical Team: Tom Turner,															
/// Date:  		21/02/2019																					
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "RouletteData.sch"
USING "RouletteHelper.sch"
USING "RouletteLayout.sch"
USING "RouletteUI.sch"
USING "RouletteProps.sch"
USING "RouletteGameState.sch"
USING "RouletteBetting.sch"
USING "RouletteCamera.sch"
USING "RouletteAnimation.sch"
USING "RouletteMetrics.sch"
USING "RouletteAudio.sch"
USING "RouletteEventOverrides.sch"


/// PURPOSE:
///    Sets a new state for the client, will display a state change log
PROC ROULETTE_GAME_CLIENT_SET_STATE(ROULETTE_DATA& sRouletteData, ROULETTE_GAME_STATE eNextState, ROULETTE_TRANSITION_FUNC onExit, ROULETTE_TRANSITION_FUNC onEnter)	
	INT iPlayerID = NATIVE_TO_INT(PLAYER_ID())
	
	// Print debug state change
	CPRINTLN(DEBUG_ROULETTE, "")
	CPRINTLN(DEBUG_ROULETTE, ">>ROULETTE_GAME_CLIENT_SET_STATE - State changed, ",
	GET_ROULETTE_GAME_STATE_NAME(sRouletteData.playerBD[iPlayerID].eGameState),
	" -> ", GET_ROULETTE_GAME_STATE_NAME(eNextState))
	
	// Switch state with on exits and on enters
	CALL onExit(sRouletteData)
	sRouletteData.playerBD[iPlayerID].eGameState = eNextState
	CALL onEnter(sRouletteData)
ENDPROC

/// PURPOSE:
///    Clean up any bets made by the local player
PROC ROULETTE_GAME_CLIENT_CLEAN_UP_LOCAL_PLAYER_BETS(ROULETTE_DATA &sRouletteData)
	ROULETTE_BETTING_CLEAR_BETS(sRouletteData.playerBD[NATIVE_TO_INT(PLAYER_ID())].iBets)
	ROULETTE_PROPS_DELETE_LOCAL_PLAYERS_CHIPS(sRouletteData)
ENDPROC

/// PURPOSE:
///    Polls input for moving the cursor with snapping (dpad)
PROC ROULETTE_GAME_CLIENT_GET_SNAP_MOVE_DIRECTION_INPUT(INT &iMoveXDirection, INT &iMoveYDirection)
	// Note: we now view the layout as landscape so direction axis is swapped
	IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
		iMoveXDirection = 1
	ELIF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
		iMoveXDirection = -1
	ENDIF
	
	IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
		iMoveYDirection = -1
	ELIF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
		iMoveYDirection = 1
	ENDIF
ENDPROC

/// PURPOSE:
///    Polls input for moving the cursor with snapping and updates the cursor position
///    to snap to the roulette layout
FUNC BOOL ROULETTE_GAME_CLIENT_PROCESS_SNAP_MOVING(ROULETTE_DATA &sRouletteData)
	INT iMoveXDirection = 0
	INT iMoveYDirection = 0			
	ROULETTE_GAME_CLIENT_GET_SNAP_MOVE_DIRECTION_INPUT(iMoveXDirection, iMoveYDirection)	
	
	// No movement reset timer
	IF iMoveXDirection = 0
	AND iMoveYDirection = 0
		RETURN FALSE
	ENDIF
	
	// Only update the cursor if enough time has passed since last move
	IF HAS_NET_TIMER_EXPIRED(sRouletteData.stCursorMoveDelay, ROULETTE_CURSOR_DELAY_TIME)
		ROULETTE_LAYOUT_MOVE_COORD(sRouletteData.iCursorXCoord, sRouletteData.iCursorYCoord,
		iMoveXDirection, iMoveYDirection, FALSE)	
		RESET_NET_TIMER(sRouletteData.stCursorMoveDelay)
		ROULETTE_UI_SET_UPDATE_BET_AMOUNT_AT_CURSOR_UI(sRouletteData, TRUE)
		ROULETTE_PROPS_UPDATE_CURSOR_SNAP_POSITION(sRouletteData)
		ROULETTE_PROPS_UPDATE_CHIP_CURSOR_MODEL(sRouletteData)
		RETURN TRUE
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Gets the movement direction input for the cursor
PROC ROULETTE_GAME_CLIENT_GET_CURSOR_CONTROLLER_MOVE_DIRECTION_INPUT(VECTOR &vMovementInput)
	vMovementInput.x = GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_X)
	vMovementInput.y = -GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y)
ENDPROC

/// PURPOSE:
///    Gets the input vector relative to the players current seat   
FUNC VECTOR ROULETTE_GAME_CLIENT_GET_INPUT_RELATIVE_TO_CURRENT_SEAT(ROULETTE_DATA &sRouletteData, VECTOR vMovementInput)
	INT iLocalSeatID = ROULETTE_HELPER_GET_LOCAL_PLAYERS_LOCAL_SEAT_ID(sRouletteData)
	STRING strBoneName = ROULETTE_ANIM_GET_SEAT_BONE_NAME(iLocalSeatID)	
	OBJECT_INDEX objTable = ROULETTE_HELPER_GET_TABLE_OBJECT_FOR_ID(sRouletteData.iTableID)
	VECTOR vSeatRot = ROULETTE_PROPS_GET_ENTITY_BONE_ROTATION_SAFELY(objTable, strBoneName)
	RETURN ROTATE_VECTOR_ABOUT_Z(vMovementInput, vSeatRot.z - 90)
ENDFUNC

/// PURPOSE:
///    Processes the cursor movement using the controller as an input
PROC ROULETTE_GAME_CLIENT_PROCESS_CONTROLLER_CURSOR_MOVING(ROULETTE_DATA &sRouletteData)
	VECTOR vMovementInput
	ROULETTE_GAME_CLIENT_GET_CURSOR_CONTROLLER_MOVE_DIRECTION_INPUT(vMovementInput)
	
	vMovementInput = ROULETTE_TABLE_GET_VECTOR_RELATIVE_TO_TABLE(vMovementInput, sRouletteData.iTableID)
	
	IF vMovementInput.x = 0 AND vMovementInput.y = 0
		EXIT
	ENDIF
	
	sRouletteData.vCursorPosition += (vMovementInput * GET_FRAME_TIME() * ROULETTE_CURSOR_MOVE_SPEED) 
	
	ROULETTE_UI_SET_UPDATE_BET_AMOUNT_AT_CURSOR_UI(sRouletteData, TRUE)	
	ROULETTE_PROPS_UPDATE_CHIP_CURSOR(sRouletteData)
ENDPROC

/// PURPOSE:
///    Processes the cursor positions movement across the layout using the mouse
PROC ROULETTE_GAME_CLIENT_PROCESS_MOUSE_CURSOR_MOVING(ROULETTE_DATA &sRouletteData)	
	VECTOR vMousePos	
	vMousePos.x = GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_CURSOR_X)
	vMousePos.y = GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_CURSOR_Y)
	vMousePos.y = 1 - vMousePos.y// Invert y
	
	VECTOR vTopRight = ROULETTE_PROPS_GET_CURSOR_BOUNDARY_TOP_RIGHT_POS()
	VECTOR vBottomLeft = ROULETTE_PROPS_GET_CURSOR_BOUNDARY_BOTTOM_LEFT_POS()
	
	// Map mouse to table space
	VECTOR vTableSpacePos 
	vTableSpacePos.x = MAP_VALUE(vMousePos.x, 0.0, 1.0, vBottomLeft.x, vTopRight.x)
	vTableSpacePos.y = MAP_VALUE(vMousePos.y, 0.0, 1.0, vBottomLeft.y, vTopRight.y)
	
	INT iTableID = sRouletteData.iTableID
	// Convert to world space
	sRouletteData.vCursorPosition = ROULETTE_TABLE_GET_VECTOR_RELATIVE_TO_TABLE(vTableSpacePos, iTableID) + ROULETTE_LAYOUT_GET_ORIGIN_FOR_TABLE(iTableID)
	ROULETTE_PROPS_CLAMP_CURSOR_POSITION_TO_BOUNDS(sRouletteData)
ENDPROC

/// PURPOSE:
///    Processes the smooth movement of the cursor 
PROC ROULETTE_GAME_CLIENT_PROCESS_CURSOR_MOVING(ROULETTE_DATA &sRouletteData)	
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		ROULETTE_GAME_CLIENT_PROCESS_MOUSE_CURSOR_MOVING(sRouletteData)
		ROULETTE_UI_SET_UPDATE_BET_AMOUNT_AT_CURSOR_UI(sRouletteData, TRUE)	
		ROULETTE_PROPS_UPDATE_CHIP_CURSOR(sRouletteData)
	ELSE
		ROULETTE_GAME_CLIENT_PROCESS_CONTROLLER_CURSOR_MOVING(sRouletteData)
	ENDIF
ENDPROC

/// PURPOSE:
///    Increments the chip the player is currently using
PROC ROULETTE_GAME_CLIENT_INCREMENT_CURRENT_CHIP(ROULETTE_DATA &sRouletteData)
	BOOL bAlreadyAtMax = sRouletteData.eCurrentChipToPlace = ROULETTE_BETTING_GET_MAX_VALID_CHIP_DENOMINATION(sRouletteData)

	IF NOT bAlreadyAtMax
		sRouletteData.eCurrentChipToPlace = ROULETTE_BETTING_INCREMENT_TO_NEXT_VALID_CHIP_DENOMINATION(sRouletteData, sRouletteData.eCurrentChipToPlace, 1)	
		ROULETTE_UI_SET_UPDATE_BETTING_UI_BUTTONS(sRouletteData, TRUE)
		ROULETTE_PROPS_UPDATE_CHIP_CURSOR_MODEL(sRouletteData)
		ROULETTE_AUDIO_PLAY_SFX_ADJUST_CHIPS_UP()
		RESET_NET_TIMER(sRouletteData.stChipSwapDelay)
		CLEAR_BIT(sRouletteData.iRouletteBS, ROULETTE_BS_HAS_PLAYED_HIT_CHIP_LIMIT_SFX)
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(sRouletteData.iRouletteBS, ROULETTE_BS_HAS_PLAYED_HIT_CHIP_LIMIT_SFX)
	OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
		ROULETTE_AUDIO_PLAY_SFX_INVALID_ACTION()	
		SET_BIT(sRouletteData.iRouletteBS, ROULETTE_BS_HAS_PLAYED_HIT_CHIP_LIMIT_SFX)
	ENDIF
ENDPROC

/// PURPOSE:
///    Decrements the chip the player is currently using
PROC ROULETTE_GAME_CLIENT_DECREMENT_CURRENT_CHIP(ROULETTE_DATA &sRouletteData)
	BOOL bAlreadyAtMin = sRouletteData.eCurrentChipToPlace = ROULETTE_BETTING_GET_MIN_VALID_CHIP_DENOMINATION(sRouletteData)
	sRouletteData.eLastChipToPlace = sRouletteData.eCurrentChipToPlace
	
	IF NOT bAlreadyAtMin
		sRouletteData.eCurrentChipToPlace = ROULETTE_BETTING_INCREMENT_TO_NEXT_VALID_CHIP_DENOMINATION(sRouletteData, sRouletteData.eCurrentChipToPlace, -1)
		ROULETTE_UI_SET_UPDATE_BETTING_UI_BUTTONS(sRouletteData, TRUE)
		ROULETTE_PROPS_UPDATE_CHIP_CURSOR_MODEL(sRouletteData)
		RESET_NET_TIMER(sRouletteData.stChipSwapDelay)
		ROULETTE_AUDIO_PLAY_SFX_ADJUST_CHIPS_DOWN()
		CLEAR_BIT(sRouletteData.iRouletteBS, ROULETTE_BS_HAS_PLAYED_HIT_CHIP_LIMIT_SFX)
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(sRouletteData.iRouletteBS, ROULETTE_BS_HAS_PLAYED_HIT_CHIP_LIMIT_SFX)
	OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
		ROULETTE_AUDIO_PLAY_SFX_INVALID_ACTION()
		SET_BIT(sRouletteData.iRouletteBS, ROULETTE_BS_HAS_PLAYED_HIT_CHIP_LIMIT_SFX)
	ENDIF
ENDPROC

/// PURPOSE:
///    Updates the changing of the chip that will be placed, based on the players input
PROC ROULETTE_GAME_CLIENT_PROCESS_CHIP_SWITCHING(ROULETTE_DATA &sRouletteData)
	ROULETTE_CHIP_DENOMINATIONS eMaxDenom = ROULETTE_BETTING_GET_MAX_VALID_CHIP_DENOMINATION(sRouletteData)
	
	// Bet max
	IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_Y) 
	AND sRouletteData.eCurrentChipToPlace != eMaxDenom
		sRouletteData.eCurrentChipToPlace = eMaxDenom
		ROULETTE_UI_SET_UPDATE_BETTING_UI_BUTTONS(sRouletteData, TRUE)
		ROULETTE_PROPS_UPDATE_CHIP_CURSOR_MODEL(sRouletteData)
		ROULETTE_AUDIO_PLAY_SFX_BET_MAX()
		EXIT
	ENDIF
	
	BOOL bIncrementedChip = FALSE
	BOOL bDecrementedChip = FALSE
	BOOL bDelayFinished = HAS_NET_TIMER_EXPIRED(sRouletteData.stChipSwapDelay, ROULETTE_UI_CHIP_SWAP_DELAY_TIME)
	bIncrementedChip = IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP) AND bDelayFinished 
	bDecrementedChip = IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN) AND bDelayFinished
	
	IF bIncrementedChip
		ROULETTE_GAME_CLIENT_INCREMENT_CURRENT_CHIP(sRouletteData)
		EXIT
	ENDIF
	
	IF bDecrementedChip
		ROULETTE_GAME_CLIENT_DECREMENT_CURRENT_CHIP(sRouletteData)
	ENDIF
ENDPROC

/// PURPOSE:
///    Adds a bet at the current cursor position on the roulette layout by the given amount
PROC ROULETTE_GAME_CLIENT_ADD_BET_AT_CURSOR_POSITION(ROULETTE_DATA &sRouletteData)
	CPRINTLN(DEBUG_ROULETTE, " ")
	CPRINTLN(DEBUG_ROULETTE, "ADD_BET_AT_CURRENT_CURSOR_POSITION - Called")
	
	INT iBetToPlaceValue = ROULETTE_GET_CHIP_DENOMINATION_VALUE(sRouletteData.eCurrentChipToPlace)
	INT iTotalBetForRound = ROULETTE_HELPER_GET_LOCAL_PLAYERS_TOTAL_BET_FOR_CURRENT_ROUND(sRouletteData)
	IF NOT ROULETTE_BETTING_CAN_PLACE_BET(iBetToPlaceValue, iTotalBetForRound)
		EXIT
	ENDIF
	
	INT iOriginalValue = sRouletteData.iBetAmountAtCurrentCursorPosition
	ROULETTE_CHIP_DENOMINATIONS eOldDenom = ROULETTE_GET_LARGEST_CHIP_DENOMINATION_FOR_GIVEN_VALUE(sRouletteData.iBetAmountAtCurrentCursorPosition)	
	ROULETTE_BETTING_PLACE_RESULT eResult = ROULETTE_BETTING_ADD_BET_AT_CURSOR_POSITION_FOR_LOCAL_PLAYER(sRouletteData, iBetToPlaceValue)
	VECTOR vSoundPos = ROULETTE_LAYOUT_GET_BET_POSITION_ON_TABLE_FOR_LAYOUT_COORD(sRouletteData.iTableID, sRouletteData.iCursorXCoord, sRouletteData.iCursorYCoord)
	
	SWITCH eResult
		CASE RBPR_SUCCESS
			ROULETTE_UI_SET_UPDATE_BET_AMOUNT_AT_CURSOR_UI(sRouletteData, TRUE)
			ROULETTE_UI_UPDATE_BET_AMOUNT_AT_CURSOR_POSITION(sRouletteData)
			ROULETTE_UI_SET_UPDATE_MIN_BET_CHECK(sRouletteData, TRUE)	
			ROULETTE_PROPS_UPDATE_CHIPS_FOR_LOCAL_PLAYER_BETS(sRouletteData)
			ROULETTE_HELPER_SET_PLAYER_REQUIRES_CHIP_UPDATE(sRouletteData)

			IF iOriginalValue > 0 AND ROULETTE_GET_LARGEST_CHIP_DENOMINATION_FOR_GIVEN_VALUE(sRouletteData.iBetAmountAtCurrentCursorPosition) != eOldDenom
				ROULETTE_AUDIO_PLAY_SFX_PLACE_CHIPS_SWAPPED_MODEL(vSoundPos)
			ELSE
				ROULETTE_AUDIO_PLAY_SFX_PLACE_CHIPS(vSoundPos)
			ENDIF
		BREAK
		CASE RBPR_FAILED_HIT_INSIDE_LIMIT
			CLEAR_HELP(TRUE)
			ROULETTE_UI_DISPLAY_REACHED_MAX_INSIDE_BET_WARNING()
			ROULETTE_AUDIO_PLAY_SFX_INVALID_ACTION()
		BREAK
		CASE RBPR_FAILED_HIT_OUTSIDE_LIMIT
			CLEAR_HELP(TRUE)
			ROULETTE_UI_DISPLAY_REACHED_MAX_OUTSIDE_BET_WARNING()
			ROULETTE_AUDIO_PLAY_SFX_INVALID_ACTION()
		BREAK
		CASE RBPR_FAILED_NO_BET_SLOTS_AVAILABLE
			CLEAR_HELP(TRUE)
			ROULETTE_UI_DISPLAY_NO_BETS_REMAIN_WARNING()
			ROULETTE_AUDIO_PLAY_SFX_INVALID_ACTION()
		BREAK
		CASE RBPR_FAILED_NOT_ENOUGH_CHIPS
			CLEAR_HELP(TRUE)
			ROULETTE_UI_DISPLAY_NOT_ENOUGH_CHIPS_WARNING()
			ROULETTE_AUDIO_PLAY_SFX_INVALID_ACTION()
		BREAK	
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Removes a bet at the current cursor position on the roulette layout by the current selected amount
PROC ROULETTE_GAME_CLIENT_REMOVE_BET_AT_CURSOR_POSITION(ROULETTE_DATA &sRouletteData)
	CPRINTLN(DEBUG_ROULETTE, " ")
	CPRINTLN(DEBUG_ROULETTE, "REMOVE_BET_AT_CURRENT_CURSOR_POSITION - Called")
	
	ROULETTE_BETTING_PLACE_RESULT eResult = ROULETTE_BETTING_ADD_BET_AT_CURSOR_POSITION_FOR_LOCAL_PLAYER(sRouletteData, -sRouletteData.iBetAmountAtCurrentCursorPosition)
	
	// Failed to remove
	IF eResult != RBPR_SUCCESS
		EXIT
	ENDIF	
	
	ROULETTE_UI_SET_UPDATE_BET_AMOUNT_AT_CURSOR_UI(sRouletteData, TRUE)
	ROULETTE_UI_SET_UPDATE_MIN_BET_CHECK(sRouletteData, TRUE)	
	ROULETTE_PROPS_UPDATE_CHIPS_FOR_LOCAL_PLAYER_BETS(sRouletteData)
	ROULETTE_HELPER_SET_PLAYER_REQUIRES_CHIP_UPDATE(sRouletteData)
	ROULETTE_AUDIO_PLAY_SFX_REMOVE_BET()
ENDPROC

/// PURPOSE:
///    Handles placing bets on the layout, detects input and either places or removes
///    the bet from the current cursor position
PROC ROULETTE_GAME_CLIENT_PROCESS_CHANGING_A_BET(ROULETTE_DATA &sRouletteData)	
	BOOL bPlaced = FALSE
	BOOL bRemoved = FALSE
	
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		bPlaced = IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
		bRemoved = IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_CANCEL)
	ELSE
		bPlaced = IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
		bRemoved = IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DELETE)
	ENDIF
	
	IF bPlaced
		ROULETTE_GAME_CLIENT_ADD_BET_AT_CURSOR_POSITION(sRouletteData)
	ELIF bRemoved
		ROULETTE_GAME_CLIENT_REMOVE_BET_AT_CURSOR_POSITION(sRouletteData)
	ENDIF
ENDPROC

/// PURPOSE:
///    Resets the layout cursor to its default starting position
PROC ROULETTE_GAME_CLIENT_RESET_CURSOR_TO_START_POSITION(ROULETTE_DATA &sRouletteData)
	sRouletteData.iCursorXCoord = ROULETTE_CURSOR_START_X
	sRouletteData.iCursorYCoord = ROULETTE_CURSOR_START_Y
	ROULETTE_PROPS_UPDATE_CURSOR_SNAP_POSITION(sRouletteData)
	sRouletteData.vCursorPosition = GET_ENTITY_COORDS(sRouletteData.objCursor)
ENDPROC

/// PURPOSE:
///    Gets the roulette servers game state for the table the local player
///    is playing on
FUNC ROULETTE_GAME_STATE GET_SERVER_GAME_STATE(ROULETTE_DATA &sRouletteData)
	INT iTableID = sRouletteData.playerBD[NATIVE_TO_INT(PLAYER_ID())].iTableID
	RETURN sRouletteData.serverBD.eGameState[iTableID]
ENDFUNC

/// PURPOSE:
///    Gets the servers state for the current table   
FUNC ROULETTE_SERVER_STATE GET_SERVER_STATE(ROULETTE_DATA &sRouletteData)
	INT iTableID = sRouletteData.playerBD[NATIVE_TO_INT(PLAYER_ID())].iTableID
	RETURN sRouletteData.serverBD.eServerState[iTableID]
ENDFUNC

/// PURPOSE:
///    Initializes the betting cursor
PROC ROULETTE_GAME_CLIENT_INIT_CURSOR(ROULETTE_DATA &sRouletteData)
	ROULETTE_GAME_CLIENT_RESET_CURSOR_TO_START_POSITION(sRouletteData)
	ROULETTE_PROPS_UPDATE_CURSOR_SNAP_POSITION(sRouletteData)
	ROULETTE_PROPS_UPDATE_TABLE_HIGHLIGHTS(sRouletteData)
	sRouletteData.eCurrentChipToPlace = ROULETTE_BETTING_GET_MIN_VALID_CHIP_DENOMINATION(sRouletteData)
	ROULETTE_UI_SET_UPDATE_BET_AMOUNT_AT_CURSOR_UI(sRouletteData, TRUE)	
	ROULETTE_PROPS_UPDATE_CHIP_CURSOR(sRouletteData)
	SET_ENTITY_VISIBLE(sRouletteData.objCursor, TRUE)
ENDPROC

/// PURPOSE:
///    Resets the clients local betting timer
PROC ROULETTE_GAME_CLIENT_RESET_LOCAL_BETTING_TIMER(ROULETTE_DATA &sRouletteData)
	IF NOT ROULETTE_TABLE_IS_TABLE_ID_VALID(sRouletteData.iTableID)
		EXIT
	ENDIF
	
	RESET_NET_TIMER(sRouletteData.serverBD.stBettingTimer[sRouletteData.iTableID])
ENDPROC

///------------------------------------
/// On Exit
///------------------------------------

/// PURPOSE:
///    On exit function for the betting state 
PROC ROULETTE_GAME_CLIENT_BETTING_ON_EXIT(ROULETTE_DATA &sRouletteData)
	ROULETTE_BETTING_REMOVE_LOCAL_PLAYER_BETS_THAT_FAIL_TO_MEET_THE_MIN_BET(sRouletteData)
	
	INT iRoundBet = ROULETTE_HELPER_GET_LOCAL_PLAYERS_TOTAL_BET_FOR_CURRENT_ROUND(sRouletteData)
	
	// If they haven't bet increment skipped bet count
	IF iRoundBet <= 0  #IF IS_DEBUG_BUILD AND NOT GET_COMMANDLINE_PARAM_EXISTS("sc_DisableCasinoGameIdleKicks") #ENDIF
		sRouletteData.iRoundsWithoutBet++
	ELSE
		// a bet was made so reset skipped bets
		sRouletteData.iRoundsWithoutBet = 0
	ENDIF
	
	SET_ENTITY_VISIBLE(sRouletteData.objCursor, FALSE)
	ROULETTE_PROPS_HIDE_ALL_HIGHLIGHTS(sRouletteData)
	
	// Inits to the correct fov for lerp in
	ROULETTE_CAMERA_INIT_WHEEL_SPIN_GAMEPLAY_CAMERA(sRouletteData)
	ROULETTE_UI_UPDATE_BETTING_UI(sRouletteData)
ENDPROC

/// PURPOSE:
///    On exit for round resolution state
PROC ROULETTE_GAME_CLIENT_ROUND_RESOLUTION_ON_EXIT(ROULETTE_DATA &sRouletteData)	
	UNUSED_PARAMETER(sRouletteData)
	ROULETTE_EVENT_ON_ROUND_END()
ENDPROC

/// PURPOSE:
///    On exit for the wheel spin state
PROC ROULETTE_GAME_CLIENT_WHEEL_SPIN_ON_EXIT(ROULETTE_DATA &sRouletteData)
	ROULETTE_AUDIO_STOP_WHEEL_SCENE()
	ROULETTE_HELPER_SET_DEALER_CALLED_NO_MORE_BETS(sRouletteData, FALSE)
ENDPROC

///------------------------------------    
///	On Enter
///------------------------------------   

/// PURPOSE:
///    On enter function for the betting state
PROC ROULETTE_GAME_CLIENT_BETTING_ON_ENTER(ROULETTE_DATA &sRouletteData)
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_GAME_CLIENT_BETTING_ON_ENTER - Called")

	// Init the current switchable view to the layout but don't show it just yet
	ROULETTE_CAMERA_INIT_SWITCHABLE_VIEWS(sRouletteData, TRUE)
	
	// Only show layout if player is finished peeking
	IF NOT ROULETTE_CAMERA_IS_IN_A_PEEK_CAMERA(sRouletteData)
		ROULETTE_CAMERA_SHOW_VIEW(sRouletteData, ROULETTE_CAM_VIEW_LAYOUT)
	ENDIF
	
	// Chips will have been removed from last round so update min bet check
	ROULETTE_UI_SET_UPDATE_MIN_BET_CHECK(sRouletteData, TRUE)
	
	// Get player and ui ready for new betting round
	SET_ENTITY_VISIBLE(sRouletteData.objCursor, TRUE)
	ROULETTE_PROPS_UPDATE_TABLE_HIGHLIGHTS(sRouletteData)
	sRouletteData.iBetAmountAtCurrentCursorPosition = 0
	sRouletteData.iTotalInsideBetThisRound = 0
	sRouletteData.iTotalOutsideBetThisRound = 0
	sRouletteData.iRoundPayout = 0
	sRouletteData.iRoundStartingChips = ROULETTE_BETTING_GET_LOCAL_PLAYER_CHIP_COUNT()
	
	ROULETTE_GAME_CLIENT_INIT_CURSOR(sRouletteData)
	ROULETTE_UI_SET_UPDATE_BETTING_UI_BUTTONS(sRouletteData, TRUE)	
	ROULETTE_BETTING_CLEAR_LOCAL_PLAYER_BETS(sRouletteData)
	ROULETTE_PROPS_DELETE_ALL_CHIPS_ON_CURRENT_TABLE(sRouletteData)
	ROULETTE_HELPER_SET_PLAYER_REQUIRES_CHIP_UPDATE(sRouletteData)
	ROULLETTE_METRICS_INIT_FOR_NEW_ROUND(sRouletteData)
	ROULETTE_EVENT_ON_ROUND_BEGIN()
ENDPROC

/// PURPOSE:
///     On enter function for entering the round resolution state
PROC ROULETTE_GAME_CLIENT_ROUND_RESOLUTION_ON_ENTER(ROULETTE_DATA &sRouletteData)
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_GAME_CLIENT_WHEEL_SPIN_ON_ENTER - Called")	
	
	sRouletteData.iRoundPayout = ROULETTE_BETTING_GET_PAYOUT_FOR_LOCAL_PLAYERS_BETS(sRouletteData)
		
	INT iProfit = sRouletteData.iRoundPayout - ROULETTE_HELPER_GET_LOCAL_PLAYERS_TOTAL_BET_FOR_CURRENT_ROUND(sRouletteData)
	sRouletteData.iTotalWinnings += iProfit
	
	// if they didn't profit and lost money
	IF iProfit < 0
		// They lost
		ROULETTTE_EVENT_ON_LOSE(ABSI(iProfit))
		sRouletteData.iLoseStreak++
	ELIF iProfit > 0
		// They won
		ROULETTTE_EVENT_ON_WIN(iProfit)
		ROULETTE_AUDIO_PLAY_SFX_WIN()
		sRouletteData.iLoseStreak = 0
	ENDIF
	
	/// Should never interupt a seat entry animation with a reaction
	/// and we should only ever play one if we are in a ready up loop
	IF ROULETTE_ANIM_HAS_PLAYER_FINISHED_SEAT_ENTER_ANIM()
	AND ROULETTE_ANIM_IS_PLAYER_PLAYING_READY_UP_LOOP_ANIM(sRouletteData)
		ROULETTE_ANIM_PLAY_APPROPRIATE_SEAT_REACTION_CLIP(sRouletteData)
	ENDIF
	
	ROULETTE_CAMERA_PLAY_REACTION_CAMERA_ANIMATION(sRouletteData)	
	ROULETTE_UI_SET_UPDATE_BETTING_UI_BUTTONS(sRouletteData, TRUE)
	
	RESET_NET_TIMER(sRouletteData.stEndRoundTimer)
	START_NET_TIMER(sRouletteData.stEndRoundTimer)
ENDPROC

/// PURPOSE:
///    On enter for the wheel spin state 
PROC ROULETTE_GAME_CLIENT_WHEEL_SPIN_ON_ENTER(ROULETTE_DATA &sRouletteData)
	ROULETTE_UI_SET_UPDATE_BETTING_UI_BUTTONS(sRouletteData, TRUE)
	ROULETTE_HELPER_SET_DEALER_CALLED_NO_MORE_BETS(sRouletteData, FALSE)
ENDPROC

/// PURPOSE:
///    On enter for the wait for next round state
PROC ROULETTE_GAME_CLIENT_WAIT_FOR_NEXT_ROUND_ON_ENTER(ROULETTE_DATA &sRouletteData)
	// Collect and send end of round heavy info
	ROULETTE_METRICS_HEAVY_COLLECT_ROUND_END_INFO(sRouletteData)
	ROULETTE_METRICS_HEAVY_SEND(sRouletteData)
	
	// Collect ond of round light info
	ROULETTE_METRICS_LIGHT_COLLECT_ROUND_END_INFO(sRouletteData)
	ROULETTE_BETTING_CLEAR_LOCAL_PLAYER_BETS(sRouletteData)
	ROULETTE_UI_SET_UPDATE_BETTING_UI_BUTTONS(sRouletteData, TRUE)
ENDPROC

///------------------------------------    
///	On Updates
///------------------------------------   

/// PURPOSE:
///    Skips to the betting state
PROC ROULETTE_CLIENT_SKIP_TO_BETTING_STATE(ROULETTE_DATA &sRouletteData)
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_CLIENT_SKIP_TO_BETTING_STATE - Called")
	ROULETTE_GAME_CLIENT_SET_STATE(sRouletteData, ROULETTE_GAME_STATE_BETTING, 
	&ROULETTE_GAME_CLIENT_BETTING_ON_ENTER, &ROULETTE_DEFAULT_ON_EXIT)
		
	ROULETTE_CAMERA_INIT_SWITCHABLE_VIEWS(sRouletteData, TRUE)
	ROULETTE_CAMERA_SHOW_VIEW(sRouletteData, ROULETTE_CAM_VIEW_LAYOUT)
ENDPROC

/// PURPOSE:
///    Skips to the wheel spin state
PROC ROULETTE_CLIENT_SKIP_TO_WHEEL_SPIN_STATE(ROULETTE_DATA &sRouletteData)
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_CLIENT_SKIP_TO_WHEEL_SPIN_STATE - Called")
	ROULETTE_GAME_CLIENT_SET_STATE(sRouletteData, ROULETTE_GAME_STATE_SPIN_WHEEL, 
	&ROULETTE_GAME_CLIENT_WHEEL_SPIN_ON_ENTER, &ROULETTE_DEFAULT_ON_EXIT)
	
	ROULETTE_CAMERA_INIT_SWITCHABLE_VIEWS(sRouletteData, FALSE)
	NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_LEAVE_CAMERA_CONTROL_ON)
ENDPROC

/// PURPOSE:
///    Skips to the wait for next round state
PROC ROULETTE_CLIENT_SKIP_TO_WAIT_FOR_NEXT_ROUND_STATE(ROULETTE_DATA &sRouletteData)
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_CLIENT_SKIP_TO_WAIT_FOR_NEXT_ROUND_STATE - Called")
	ROULETTE_CAMERA_SHOW_LAST_SWITCHABLE_CAM_VIEW(sRouletteData, FALSE)
	
	ROULETTE_GAME_CLIENT_SET_STATE(sRouletteData, ROULETTE_GAME_STATE_WAIT_FOR_NEXT_ROUND, 
	&ROULETTE_DEFAULT_ON_ENTER, &ROULETTE_DEFAULT_ON_EXIT)
	
	ROULETTE_CAMERA_INIT_SWITCHABLE_VIEWS(sRouletteData, FALSE)
	NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_LEAVE_CAMERA_CONTROL_ON)
ENDPROC

/// PURPOSE:
///    Transitions the player to the correct state given the tables state on the server
PROC ROULETTE_GAME_CLIENT_TRANSITION_TO_CORRECT_STARTING_STATE(ROULETTE_DATA &sRouletteData)
	// Client could be joining during any server state
	ROULETTE_GAME_STATE eServerGameState = GET_SERVER_GAME_STATE(sRouletteData)
	INT iTableID = sRouletteData.iTableID
	INT iBettingRoundDuration = ROULETTE_HELPER_GET_TABLES_BETTING_ROUND_DURATION(sRouletteData, iTableID)
	
	SWITCH eServerGameState
		CASE ROULETTE_GAME_STATE_INIT
		CASE ROULETTE_GAME_STATE_IDLE
			ROULETTE_CLIENT_SKIP_TO_BETTING_STATE(sRouletteData)
		BREAK
		CASE ROULETTE_GAME_STATE_BETTING
			
			// If only 5 seconds left, just put the player in the spin state
			IF HAS_NET_TIMER_EXPIRED(sRouletteData.serverBD.stBettingTimer[iTableID], iBettingRoundDuration - 6000)		
				CPRINTLN(DEBUG_ROULETTE, "ROULETTE_GAME_CLIENT_TRANSITION_TO_CORRECT_STARTING_STATE - Skiping betting state")		
				ROULETTE_CLIENT_SKIP_TO_WHEEL_SPIN_STATE(sRouletteData)
				EXIT
			ENDIF

			ROULETTE_CLIENT_SKIP_TO_BETTING_STATE(sRouletteData)
		BREAK		
		CASE ROULETTE_GAME_STATE_SPIN_WHEEL
			ROULETTE_CLIENT_SKIP_TO_WHEEL_SPIN_STATE(sRouletteData)
		BREAK
		DEFAULT
			// Default to waiting for next round state
			ROULETTE_CLIENT_SKIP_TO_WAIT_FOR_NEXT_ROUND_STATE(sRouletteData)
		BREAK	
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Update state for the roulette game client initialization state
PROC ROULETTE_GAME_CLIENT_STATE_INIT_UPDATE(ROULETTE_DATA &sRouletteData)
	// Make sure movement control is disabled
	NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
	SET_BIT(sRouletteData.iTableBS[sRouletteData.iTableID], ROULETTE_BS_HAS_PLAYER_PLAYED_AT_TABLE)
	
	// Init UI and cursor position
	ROULETTE_UI_INIT(sRouletteData)	
	sRouletteData.iRoundsWithoutBet = 0
	sRouletteData.iStartingChips = ROULETTE_BETTING_GET_LOCAL_PLAYER_CHIP_COUNT()
	
	ROULETTE_GAME_CLIENT_TRANSITION_TO_CORRECT_STARTING_STATE(sRouletteData)
	
	// initialise the metrics
	ROULETTE_METRICS_INIT(sRouletteData)
	ROULETTE_EVENT_ON_JOIN_GAME()
ENDPROC

/// PURPOSE:
///    Process the actions that can be made during the roulette betting phase
PROC ROULETTE_GAME_CLIENT_PROCESS_BETTING_ACTIONS(ROULETTE_DATA &sRouletteData)
	ROULETTE_GAME_CLIENT_PROCESS_CURSOR_MOVING(sRouletteData)
	ROULETTE_GAME_CLIENT_PROCESS_CHIP_SWITCHING(sRouletteData)	
	ROULETTE_GAME_CLIENT_PROCESS_CHANGING_A_BET(sRouletteData)
ENDPROC

/// PURPOSE:
///    Checks if the player has exceeded the max times they can skip betting 
FUNC BOOL ROULETTE_GAME_CLIENT_HAS_PLAYER_EXCEEDED_BET_SKIPPING_LIMIT(ROULETTE_DATA &sRouletteData)
	RETURN sRouletteData.iRoundsWithoutBet >= ROULETTE_BETTING_MAX_BET_SKIPS_BEFORE_KICK 
	AND ROULETTE_BETTING_MAX_BET_SKIPS_BEFORE_KICK >= 0
ENDFUNC

/// PURPOSE:
///    Checks if the player should be removed from the game    
FUNC BOOL ROULETTE_GAME_CLIENT_SHOULD_PLAYER_BE_REMOVED_FROM_TABLE(ROULETTE_DATA &sRouletteData)
	INT iTableID = sRouletteData.iTableID
	INT iTableMinBet = ROULETTE_BETTING_GET_MIN_BET_FOR_TABLE(iTableID)
		
	IF ROULETTE_TABLE_IS_DISABLED(iTableID)
		ROULETTE_METRICS_UPDATE_END_REASON(sRouletteData, "table disabled")
		RETURN TRUE
	ENDIF
	
	BOOL bBlockedByMessage
	bBlockedByMessage = ROULETTE_DISPLAY_ANY_UNAVAILABLE_MESSAGE(sRouletteData.iSeatID, iTableID,
	iTableMinBet, sRouletteData.sMetricsLight.m_endReason, TRUE)	
	
	ROULETTE_METRICS_UPDATE_END_REASON_HASH(sRouletteData, sRouletteData.sMetricsLight.m_endReason)
	
	IF bBlockedByMessage
		RETURN TRUE
	ENDIF
	
	IF ROULETTE_GAME_CLIENT_HAS_PLAYER_EXCEEDED_BET_SKIPPING_LIMIT(sRouletteData)
		ROULETTE_METRICS_UPDATE_END_REASON(sRouletteData, "bet skip limit") 
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Hides the highlights and cursor if they are visible
PROC ROULETTE_GAME_CLIENT_HIDE_HIGHLIGHTS_AND_CURSOR(ROULETTE_DATA &sRouletteData)
	IF IS_BIT_SET(sRouletteData.iRouletteBS, ROULETTE_BS_HIGHLIGHTS_ARE_HIDDEN)
		EXIT	
	ENDIF
	
	ROULETTE_PROPS_HIDE_ALL_HIGHLIGHTS(sRouletteData)
	SET_ENTITY_VISIBLE(sRouletteData.objCursor, FALSE)
	SET_BIT(sRouletteData.iRouletteBS, ROULETTE_BS_HIGHLIGHTS_ARE_HIDDEN)
ENDPROC

/// PURPOSE:
///    Shows the highlights and cursor if they are hidden
PROC ROULETTE_GAME_CLIENT_SHOW_HIGHLIGHTS_AND_CURSOR(ROULETTE_DATA &sRouletteData)
	IF NOT IS_BIT_SET(sRouletteData.iRouletteBS, ROULETTE_BS_HIGHLIGHTS_ARE_HIDDEN)
		EXIT
	ENDIF
	
	SET_ENTITY_VISIBLE(sRouletteData.objCursor, TRUE)
	ROULETTE_PROPS_UPDATE_TABLE_HIGHLIGHTS(sRouletteData)// this should only call once after hidden
	CLEAR_BIT(sRouletteData.iRouletteBS, ROULETTE_BS_HIGHLIGHTS_ARE_HIDDEN)
ENDPROC

/// PURPOSE:
///    Update state for the roulette game client betting state
///    handles the placing of bets on the roulette layout
PROC ROULETTE_GAME_CLIENT_STATE_BETTING_UPDATE(ROULETTE_DATA &sRouletteData)
	
	BOOL bInRulesScreen = ROULETTE_UI_MAINTAIN_RULES_SCREEN(sRouletteData)
	
	BOOL bPlayerWantsToExit
	bPlayerWantsToExit = IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
	AND NOT bInRulesScreen 
	
	IF bPlayerWantsToExit
		ROULETTE_METRICS_UPDATE_END_REASON(sRouletteData, "quit")
	ENDIF
	
	// Check for leaving table, voluntarily or not
	IF bPlayerWantsToExit
	OR ROULETTE_GAME_CLIENT_SHOULD_PLAYER_BE_REMOVED_FROM_TABLE(sRouletteData)			
		ROULETTE_GAME_CLIENT_SET_STATE(sRouletteData, ROULETTE_GAME_STATE_CLEAN_UP,
		&ROULETTE_DEFAULT_ON_EXIT, &ROULETTE_CAMERA_RESTORE_GAMEPLAY_CAMERA)
		EXIT
	ENDIF	
	
	// Disable betting controls if needed
	ROULETTE_HELPER_DISABLE_BETTING_CONTROLS(sRouletteData, 
	bInRulesScreen OR NOT ROULETTE_CAMERA_IS_SHOWING_VIEW(sRouletteData, ROULETTE_CAM_VIEW_LAYOUT) 
	OR ROULETTE_ANIM_IS_TABLES_DEALER_PLAYING_CLIP(sRouletteData, sRouletteData.iTableID, ROULETTE_ANIM_CLIP_DEALER_NO_MORE_BETS))
	
	// If the server has ended the betting phase, move on to spin stage
	IF GET_SERVER_GAME_STATE(sRouletteData) = ROULETTE_GAME_STATE_SPIN_WHEEL
		ROULETTE_GAME_CLIENT_SET_STATE(sRouletteData, ROULETTE_GAME_STATE_PROCESS_PLACE_CHIPS_TRANSACTION,
		&ROULETTE_GAME_CLIENT_BETTING_ON_EXIT, &ROULETTE_DEFAULT_ON_ENTER)
	ENDIF

	ROULETTE_ANIM_MAINTAIN_RANDOM_SEAT_IDLE(sRouletteData)
	
	// Wait for camera to finish switching
	IF ROULETTE_CAMERA_IS_INTERPOLATING(sRouletteData)
		EXIT
	ENDIF
	
	ROULETTE_CAMERA_MAINTAIN_BETTING_GAMEPLAY_CAMERA(sRouletteData)
	ROULLETTE_UI_MAINTAIN_BETTING_UI(sRouletteData, bInRulesScreen)
	
	IF ROULETTE_HELPER_ARE_BETTING_CONTROLS_DISABLED(sRouletteData)
		ROULETTE_GAME_CLIENT_HIDE_HIGHLIGHTS_AND_CURSOR(sRouletteData)
		
		/// On PC the cursor position is used for the camera movement
		/// so keep it updated
		IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
			ROULETTE_GAME_CLIENT_PROCESS_MOUSE_CURSOR_MOVING(sRouletteData)
		ENDIF
		EXIT
	ENDIF
	
	ROULETTE_GAME_CLIENT_SHOW_HIGHLIGHTS_AND_CURSOR(sRouletteData)
	ROULETTE_GAME_CLIENT_PROCESS_BETTING_ACTIONS(sRouletteData)	
ENDPROC

/// PURPOSE:
///    Checks if the current spin sequence for the current table has finished
FUNC BOOL ROULETTE_GAME_CLIENT_HAS_CURRENT_TABLE_WHEEL_SPIN_SEQUENCE_FINISHED(ROULETTE_DATA &sRouletteData)
	INT iTableID = sRouletteData.iTableID
	RETURN sRouletteData.sSpin[iTableID].sData.eWheelSpinState = ROULETTE_ANIM_CLIP_INVALID 
	AND ROULETTE_ANIM_HAS_WHEEL_AT_TABLE_FINISHED_PLAYING_LAST_CLIP(sRouletteData.sSpin[iTableID])
ENDFUNC

/// PURPOSE:
///    Update state for the roulette game client wheel spin state 
///    (when the players wait for the wheel to stop spinning)
PROC ROULETTE_GAME_CLIENT_STATE_SPIN_WHEEL_UPDATE(ROULETTE_DATA &sRouletteData)	
	
	BOOL bInRulesScreen = ROULETTE_UI_MAINTAIN_RULES_SCREEN(sRouletteData)
	
	BOOL bPlayerWantsToExit
	bPlayerWantsToExit = IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
	AND NOT bInRulesScreen
	AND ROULETTE_HELPER_GET_LOCAL_PLAYERS_TOTAL_BET_FOR_CURRENT_ROUND(sRouletteData) <= 0
	
	IF bPlayerWantsToExit
		ROULETTE_METRICS_UPDATE_END_REASON(sRouletteData, "quit")
	ENDIF
	
	// Check for leaving table
	IF bPlayerWantsToExit
		ROULETTE_GAME_CLIENT_SET_STATE(sRouletteData,
		ROULETTE_GAME_STATE_CLEAN_UP,
		&ROULETTE_GAME_CLIENT_WHEEL_SPIN_ON_EXIT, &ROULETTE_CAMERA_RESTORE_GAMEPLAY_CAMERA)
		EXIT
	ENDIF	
	
	// Check if no more bets has been called
	IF ROULETTE_ANIM_HAS_DEALER_EVENT_TRIGGERED(sRouletteData, sRouletteData.iTableID, GET_HASH_KEY("READY_UP_START"))
		ROULETTE_HELPER_SET_DEALER_CALLED_NO_MORE_BETS(sRouletteData, TRUE)
	ENDIF
	
	// Ready up anim if no more bets is called
	IF ROULETTE_HELPER_HAS_DEALER_CALLED_NO_MORE_BETS(sRouletteData)	
		ROULETTE_ANIM_UPDATE_READY_FOR_SPIN_SEQUENCE_ON_LOCAL_PLAYER(sRouletteData)
	ELSE
		ROULETTE_ANIM_MAINTAIN_RANDOM_SEAT_IDLE(sRouletteData)
	ENDIF
	
	ROULETTE_CAMERA_MAINTAIN_WHEEL_SPIN_GAMEPLAY_CAMERA(sRouletteData)
   	
	IF NOT bInRulesScreen
		ROULETTE_UI_MAINTAIN_WHEEL_SPIN_UI(sRouletteData)
	ENDIF
	
	// When table spin animation has finished proceed to next state
	IF ROULETTE_ANIM_HAS_BALL_LANDED_IN_WINNING_SLOT(sRouletteData.sSpin[sRouletteData.iTableID])
	OR ROULETTE_GAME_CLIENT_HAS_CURRENT_TABLE_WHEEL_SPIN_SEQUENCE_FINISHED(sRouletteData)	
		ROULETTE_GAME_CLIENT_SET_STATE(sRouletteData,
		ROULETTE_GAME_STATE_ROUND_RESOLUTION,
		&ROULETTE_GAME_CLIENT_WHEEL_SPIN_ON_EXIT, &ROULETTE_GAME_CLIENT_ROUND_RESOLUTION_ON_ENTER)
	ENDIF
ENDPROC

/// PURPOSE:
///    Update state for processing the place bet transaction
PROC ROULETTE_GAME_CLIENT_STATE_PROCESS_PLACE_BET_TRANSACTION(ROULETTE_DATA &sRouletteData)
	INT iRoundBet = ROULETTE_HELPER_GET_LOCAL_PLAYERS_TOTAL_BET_FOR_CURRENT_ROUND(sRouletteData)
		
	// We need to wait for a successful transaction from the server
	SWITCH ROULETTE_BETTING_PLACE_BET_TRANSACTION(iRoundBet)
		CASE TRANSACTION_STATE_FAILED
			ROULETTE_UI_UPDATE_BETTING_UI(sRouletteData)
			ROULETTE_GAME_CLIENT_SET_STATE(sRouletteData, ROULETTE_GAME_STATE_CLEAN_UP,
			&ROULETTE_DEFAULT_ON_EXIT, &ROULETTE_CAMERA_RESTORE_GAMEPLAY_CAMERA)
			CPRINTLN(DEBUG_ROULETTE, "ROULETTE_GAME_CLIENT_STATE_BETTING_UPDATE - Transaction failed - Kicking player")
		EXIT
		CASE TRANSACTION_STATE_SUCCESS
			ROULETTE_UI_UPDATE_BETTING_UI(sRouletteData)
			ROULETTE_GAME_CLIENT_SET_STATE(sRouletteData, ROULETTE_GAME_STATE_SPIN_WHEEL,
			&ROULETTE_GAME_CLIENT_BETTING_ON_EXIT, &ROULETTE_GAME_CLIENT_WHEEL_SPIN_ON_ENTER)
			CPRINTLN(DEBUG_ROULETTE, "ROULETTE_GAME_CLIENT_STATE_BETTING_UPDATE - Transaction succeeded - Proceeding to wheel spin")
		EXIT
		CASE TRANSACTION_STATE_PENDING
		CASE TRANSACTION_STATE_DEFAULT
			ROULETTE_HELPER_DISABLE_BETTING_CONTROLS(sRouletteData, TRUE)
			CPRINTLN(DEBUG_ROULETTE, "ROULETTE_GAME_CLIENT_STATE_BETTING_UPDATE - Transaction processing - disabling betting actions")
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Update state for resolving the end of a round of roulette
///    handles paying out to the player based on their bets
PROC ROULETTE_GAME_CLIENT_ROUND_RESOLUTION(ROULETTE_DATA &sRouletteData)	
	
	BOOL bInRulesScreen = ROULETTE_UI_MAINTAIN_RULES_SCREEN(sRouletteData)
	
	BOOL bPlayerWantsToExit
	bPlayerWantsToExit = IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
	AND NOT bInRulesScreen
	AND ROULETTE_HELPER_GET_LOCAL_PLAYERS_TOTAL_BET_FOR_CURRENT_ROUND(sRouletteData) <= 0
	
	IF bPlayerWantsToExit
		ROULETTE_METRICS_UPDATE_END_REASON(sRouletteData, "quit")
	ENDIF
	
	// Check for leaving table
	IF bPlayerWantsToExit
		ROULETTE_GAME_CLIENT_SET_STATE(sRouletteData,
		ROULETTE_GAME_STATE_CLEAN_UP,
		&ROULETTE_DEFAULT_ON_EXIT, &ROULETTE_CAMERA_RESTORE_GAMEPLAY_CAMERA)
		EXIT
	ENDIF	
	
	IF NOT sRouletteData.stEndRoundTimer.bInitialisedTimer
		RESET_NET_TIMER(sRouletteData.stEndRoundTimer)
		START_NET_TIMER(sRouletteData.stEndRoundTimer)
	ENDIF
	
	ROULETTE_CAMERA_MAINTAIN_ROUND_RESOLUTION_GAMEPLAY_CAMERA(sRouletteData)
	
	IF NOT bInRulesScreen
		ROULETTE_UI_MAINTAIN_ROUND_RESOLUTION_UI(sRouletteData)
	ENDIF
	
	// If reaction finishs before results screen is gone, seat idle should take over
	ROULETTE_ANIM_MAINTAIN_RANDOM_SEAT_IDLE(sRouletteData)

	IF (ROULETTE_ANIM_HAS_CLIENT_CHIP_CLEARING_FINISHED_FOR_CURRENT_TABLE(sRouletteData)
	AND HAS_NET_TIMER_EXPIRED(sRouletteData.stEndRoundTimer, 5000))// Minimum of 5 seconds if no chip clearing happens 
	OR HAS_NET_TIMER_EXPIRED(sRouletteData.stEndRoundTimer, 10000)// In case chip clearing gets stuck
		
		IF sRouletteData.iRoundPayout <= 0
			// No transaction needed, proceed as normal
			ROULETTE_GAME_CLIENT_SET_STATE(sRouletteData,
			ROULETTE_GAME_STATE_WAIT_FOR_NEXT_ROUND,
			&ROULETTE_GAME_CLIENT_ROUND_RESOLUTION_ON_EXIT, &ROULETTE_GAME_CLIENT_WAIT_FOR_NEXT_ROUND_ON_ENTER)
			EXIT
		ENDIF
		
		/// Process transaction if they won  		
		SWITCH ROULETTE_BETTING_PAYOUT_TRANSACTION(sRouletteData.iRoundPayout)
			CASE TRANSACTION_STATE_FAILED
				ROULETTE_GAME_CLIENT_SET_STATE(sRouletteData, ROULETTE_GAME_STATE_CLEAN_UP,
				&ROULETTE_DEFAULT_ON_EXIT, &ROULETTE_CAMERA_RESTORE_GAMEPLAY_CAMERA)
				CPRINTLN(DEBUG_ROULETTE, "ROULETTE_GAME_CLIENT_ROUND_RESOLUTION - Transaction failed - Kicking player")
			EXIT
			CASE TRANSACTION_STATE_SUCCESS
				ROULETTE_GAME_CLIENT_SET_STATE(sRouletteData, ROULETTE_GAME_STATE_WAIT_FOR_NEXT_ROUND,
				&ROULETTE_GAME_CLIENT_ROUND_RESOLUTION_ON_EXIT, &ROULETTE_GAME_CLIENT_WAIT_FOR_NEXT_ROUND_ON_ENTER)	
				CPRINTLN(DEBUG_ROULETTE, "ROULETTE_GAME_CLIENT_ROUND_RESOLUTION - Transaction succeeded - Proceeding to wheel spin")
			EXIT
			CASE TRANSACTION_STATE_PENDING
			CASE TRANSACTION_STATE_DEFAULT
				CPRINTLN(DEBUG_ROULETTE, "ROULETTE_GAME_CLIENT_ROUND_RESOLUTION - Transaction processing - disabling betting actions")
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

/// PURPOSE:
///    Update state for waiting for the next round
///    waits for server to give the go ahead to progress to a new betting round
PROC ROULETTE_GAME_CLIENT_STATE_WAIT_FOR_NEXT_ROUND_UPDATE(ROULETTE_DATA &sRouletteData)		
	
	BOOL bInRulesScreen = ROULETTE_UI_MAINTAIN_RULES_SCREEN(sRouletteData)
	
	BOOL bPlayerWantsToExit
	bPlayerWantsToExit = IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
	AND NOT bInRulesScreen
	
	IF bPlayerWantsToExit
		ROULETTE_METRICS_UPDATE_END_REASON(sRouletteData, "quit")
	ENDIF
	
	// Check for leaving table
	IF bPlayerWantsToExit
		ROULETTE_GAME_CLIENT_SET_STATE(sRouletteData,
		ROULETTE_GAME_STATE_CLEAN_UP,
		&ROULETTE_DEFAULT_ON_EXIT, &ROULETTE_DEFAULT_ON_ENTER)
		EXIT
	ENDIF	
	
	ROULETTE_ANIM_MAINTAIN_RANDOM_SEAT_IDLE(sRouletteData)
	ROULETTE_CAMERA_MAINTAIN_ROUND_RESOLUTION_GAMEPLAY_CAMERA(sRouletteData)
	
	IF NOT bInRulesScreen
		ROULETTE_UI_MAINTAIN_ROUND_RESOLUTION_UI(sRouletteData, TRUE)
	ENDIF
	
	IF ROULETTE_GAME_CLIENT_SHOULD_PLAYER_BE_REMOVED_FROM_TABLE(sRouletteData)
		ROULETTE_GAME_CLIENT_SET_STATE(sRouletteData,
		ROULETTE_GAME_STATE_CLEAN_UP,
		&ROULETTE_DEFAULT_ON_EXIT, &ROULETTE_CAMERA_RESTORE_GAMEPLAY_CAMERA)
		EXIT
	ENDIF
	
	IF GET_SERVER_GAME_STATE(sRouletteData) = ROULETTE_GAME_STATE_BETTING
		ROULETTE_GAME_CLIENT_SET_STATE(sRouletteData,
		ROULETTE_GAME_STATE_BETTING,
		&ROULETTE_DEFAULT_ON_EXIT, &ROULETTE_GAME_CLIENT_BETTING_ON_ENTER)
	ENDIF
ENDPROC

/// PURPOSE:
///    Clean up for when leaving the roulette game
PROC ROULETTE_GAME_CLIENT_CLEAN_UP(ROULETTE_DATA &sRouletteData)
	sRouletteData.iFinalEndReason = sRouletteData.sMetricsLight.m_endReason
		
	ROULETTE_EVENT_ON_LEAVE_GAME()
	
	IF ROULETTE_UI_IS_VIEWING_RULES_SCREEN(sRouletteData)
		ROULETTE_UI_CLOSE_RULES_SCREEN(sRouletteData)
	ENDIF
		
	ROULETTE_METRICS_LIGHT_SEND(sRouletteData)
	
	IF DOES_ENTITY_EXIST(sRouletteData.objCursor)
		SET_ENTITY_VISIBLE(sRouletteData.objCursor, FALSE)	
	ENDIF
	
	ROULETTE_GAME_CLIENT_RESET_CURSOR_TO_START_POSITION(sRouletteData)
	ROULETTE_GAME_CLIENT_CLEAN_UP_LOCAL_PLAYER_BETS(sRouletteData)
	ROULETTE_CAMERA_SET_MAIN_CAM_ENABLED(sRouletteData, FALSE, FALSE)
	ROULETTE_BETTING_CLEAR_LOCAL_PLAYER_BETS(sRouletteData)
	ROULETTE_PROPS_DELETE_LOCAL_PLAYERS_CHIPS(sRouletteData)
	ROULETTE_PROPS_DELETE_ALL_HIGHLIGHTS(sRouletteData)
	ROULETTE_AUDIO_STOP_WHEEL_SCENE()
	
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
		ROULETTE_GAME_CLIENT_RESET_LOCAL_BETTING_TIMER(sRouletteData)
	ENDIF
	
	sRouletteData.iRoundsWithoutBet = 0
	sRouletteData.iTotalWinnings = 0
	sRouletteData.iTotalInsideBetThisRound = 0
	sRouletteData.iTotalOutsideBetThisRound = 0
	sRouletteData.iBetAmountAtCurrentCursorPosition = 0
	sRouletteData.iStartingChips = 0
	sRouletteData.playerBD[NATIVE_TO_INT(PLAYER_ID())].eGameState = ROULETTE_GAME_STATE_IDLE
ENDPROC

/// PURPOSE:
///    Checks if the server currently running the players current table is cleaning up
///    and transitions client game to clean up state
PROC ROULETTE_GAME_CLIENT_CHECK_FOR_EARLY_SERVER_CLEAN_UP(ROULETTE_DATA &sRouletteData)
	IF GET_SERVER_GAME_STATE(sRouletteData) = ROULETTE_GAME_STATE_CLEAN_UP
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_GAME_CLIENT_CHECK_FOR_EARLY_SERVER_CLEAN_UP - Early clean up triggered")
		ROULETTE_GAME_CLIENT_SET_STATE(sRouletteData,
		ROULETTE_GAME_STATE_CLEAN_UP,
		&ROULETTE_DEFAULT_ON_EXIT, &ROULETTE_DEFAULT_ON_ENTER)
	ENDIF
ENDPROC

/// PURPOSE:
///    Starts the roulette game, if this is not called before updating the game will remain in its idle state
PROC ROULETTE_GAME_CLIENT_START_GAME(ROULETTE_DATA &sRouletteData)
	sRouletteData.playerBD[NATIVE_TO_INT(PLAYER_ID())].eGameState = ROULETTE_GAME_STATE_INIT
ENDPROC

/// PURPOSE:
///    Main entry point for running the roulette games client 
///    logic, ROULETTE_GAME_CLIENT_START_GAME should be called otherwise the game will remain in
///    its idle state
FUNC BOOL ROULETTE_GAME_CLIENT_UPDATE(ROULETTE_DATA &sRouletteData)	
	ROULETTE_GAME_CLIENT_CHECK_FOR_EARLY_SERVER_CLEAN_UP(sRouletteData)
	ROULETTE_HELPER_HIDE_GAMEPLAY_UI_THIS_FRAME()
	
	SWITCH sRouletteData.playerBD[NATIVE_TO_INT(PLAYER_ID())].eGameState		
		CASE ROULETTE_GAME_STATE_INIT
			ROULETTE_GAME_CLIENT_STATE_INIT_UPDATE(sRouletteData)
		BREAK
		CASE ROULETTE_GAME_STATE_BETTING
			ROULETTE_GAME_CLIENT_STATE_BETTING_UPDATE(sRouletteData)
		BREAK	
		CASE ROULETTE_GAME_STATE_PROCESS_PLACE_CHIPS_TRANSACTION
			ROULETTE_GAME_CLIENT_STATE_PROCESS_PLACE_BET_TRANSACTION(sRouletteData)
			ROULETTE_GAME_CLIENT_STATE_SPIN_WHEEL_UPDATE(sRouletteData)
		BREAK
		CASE ROULETTE_GAME_STATE_SPIN_WHEEL
			ROULETTE_GAME_CLIENT_STATE_SPIN_WHEEL_UPDATE(sRouletteData)			
		BREAK
		CASE ROULETTE_GAME_STATE_ROUND_RESOLUTION
			ROULETTE_GAME_CLIENT_ROUND_RESOLUTION(sRouletteData)
		BREAK
		CASE ROULETTE_GAME_STATE_WAIT_FOR_NEXT_ROUND
			ROULETTE_GAME_CLIENT_STATE_WAIT_FOR_NEXT_ROUND_UPDATE(sRouletteData)
		BREAK
		CASE ROULETTE_GAME_STATE_CLEAN_UP
			ROULETTE_GAME_CLIENT_CLEAN_UP(sRouletteData)
		// Participation in game has finished
		RETURN FALSE
	ENDSWITCH

	// Still participating in game
	RETURN TRUE
ENDFUNC
