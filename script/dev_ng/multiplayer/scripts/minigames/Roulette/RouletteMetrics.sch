USING "RouletteData.sch"
USING "RouletteTable.sch"
USING "RouletteBetting.sch"
USING "commands_stats.sch"

#IF IS_DEBUG_BUILD
USING "RouletteDebug.sch"
#ENDIF

/// PURPOSE:
///    Initialises the metrics ready for when the player
///    joins a new game of roulette
PROC ROULETTE_METRICS_INIT(ROULETTE_DATA &sRouletteData)	
	// Light metrics needs to reset its count for each new game
	sRouletteData.sMetricsLight.m_endReason = GET_HASH_KEY("none")
	sRouletteData.sMetricsLight.m_betAmount1 = 0
	sRouletteData.sMetricsLight.m_betAmount2 = 0
	sRouletteData.sMetricsLight.m_handsPlayed = 0
	sRouletteData.sMetricsLight.m_winCount = 0
	sRouletteData.sMetricsLight.m_loseCount = 0
	sRouletteData.sMetricsLight.m_cheatCount = 0
	RESET_NET_TIMER(sRouletteData.stMetricsTimerLight)
	START_NET_TIMER(sRouletteData.stMetricsTimerLight)
	
	// Heavy metrics need to reset these for each new game
	sRouletteData.sMetrics.m_casinoMetric.m_endReason = GET_HASH_KEY("none")
	sRouletteData.sMetrics.m_casinoMetric.m_cheat = FALSE
	RESET_NET_TIMER(sRouletteData.stMetricsTimer)
	START_NET_TIMER(sRouletteData.stMetricsTimer)
ENDPROC

/// PURPOSE:
///    Increments cheat tracking metrics
PROC ROULETTE_METRICS_INCREMENT_CHEAT_COUNT(ROULETTE_DATA &sRouletteData)
	sRouletteData.sMetricsLight.m_cheatCount++
	sRouletteData.sMetrics.m_casinoMetric.m_cheat = TRUE
ENDPROC

/// PURPOSE:
///    Updates the leaving roulette end reason metrics with given string
PROC ROULETTE_METRICS_UPDATE_END_REASON(ROULETTE_DATA &sRouletteData, STRING strEndReason)
	sRouletteData.sMetricsLight.m_endReason = GET_HASH_KEY(strEndReason)
	sRouletteData.sMetrics.m_casinoMetric.m_endReason = GET_HASH_KEY(strEndReason)
ENDPROC

/// PURPOSE:
///    Updates the leaving roulette end reason metrics with given hash
PROC ROULETTE_METRICS_UPDATE_END_REASON_HASH(ROULETTE_DATA &sRouletteData, INT iEndReasonHash)
	sRouletteData.sMetricsLight.m_endReason = iEndReasonHash
	sRouletteData.sMetrics.m_casinoMetric.m_endReason = iEndReasonHash
ENDPROC

/// PURPOSE:
///    Gets the players casino membership level as an int    
FUNC INT ROULETTE_METRICS_GET_PLAYER_MEMBERSHIP_LEVEL()
	IF DOES_PLAYER_OWN_A_CASINO_APARTMENT(PLAYER_ID())
		RETURN GET_HASH_KEY("VIP membership")
	ENDIF
	
	IF HAS_LOCAL_PLAYER_PURCHASED_CASINO_MEMBERSHIP()
		RETURN GET_HASH_KEY("Paid membership")
	ENDIF
	
	IF DOES_LOCAL_PLAYERS_BOSS_OWN_A_CASINO_APARTMENT() 
	OR HAS_LOCAL_PLAYERS_BOSS_PURCHASED_CASINO_MEMBERSHIP() 
		RETURN GET_HASH_KEY("Gang membership") 
	ENDIF
	
	RETURN GET_HASH_KEY("No membership")
ENDFUNC

/// PURPOSE:
///    Collects required heavy metric information for when a new betting round starts
PROC ROULETTE_METRICS_HEAVY_COLLECT_ROUND_BEGIN_INFO(ROULETTE_DATA &sRouletteData)
	sRouletteData.sMetrics.m_casinoMetric.m_gameType = GET_HASH_KEY("Roulette")
	sRouletteData.sMetrics.m_casinoMetric.m_ownPenthouse = DOES_PLAYER_OWN_A_CASINO_APARTMENT(PLAYER_ID())
	sRouletteData.sMetrics.m_casinoMetric.m_membership = ROULETTE_METRICS_GET_PLAYER_MEMBERSHIP_LEVEL()
	
	sRouletteData.sMetrics.m_casinoMetric.m_viewedLegalScreen = ROULETTE_HELPER_GLOBAL_HAS_VIEWED_DISCLOSURE()
	sRouletteData.sMetrics.m_casinoMetric.m_isHost = NETWORK_IS_HOST_OF_THIS_SCRIPT()
	IF NETWORK_GET_HOST_OF_THIS_SCRIPT() != INVALID_PARTICIPANT_INDEX()
		sRouletteData.sMetrics.m_casinoMetric.m_hostID = NETWORK_GET_PLAYER_ACCOUNT_ID(NETWORK_GET_PLAYER_INDEX(NETWORK_GET_HOST_OF_THIS_SCRIPT()))
	ENDIF
	
	INT iTableID = sRouletteData.iTableID
	sRouletteData.sMetrics.m_casinoMetric.m_tableID = iTableID
	IF iTableID <= -1
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_DEBUG_PRINT_CURRENT_HEAVY_METRICS - invalid table ID")
		EXIT
	ENDIF
	
	IF ROULETTE_TABLE_IS_HIGH_STAKES(iTableID)
		sRouletteData.sMetrics.m_casinoMetric.m_matchType = GET_HASH_KEY("high stakes")
		EXIT
	ENDIF
	
	sRouletteData.sMetrics.m_casinoMetric.m_matchType = GET_HASH_KEY("standard")
ENDPROC

/// PURPOSE:
///    Collects required light metric information for when a new betting round starts
PROC ROULETTE_METRICS_LIGHT_COLLECT_ROUND_BEGIN_INFO(ROULETTE_DATA &sRouletteData)	
	sRouletteData.sMetricsLight.m_viewedLegalScreen = ROULETTE_HELPER_GLOBAL_HAS_VIEWED_DISCLOSURE()
	sRouletteData.sMetricsLight.m_isHost = NETWORK_IS_HOST_OF_THIS_SCRIPT()
	IF NETWORK_GET_HOST_OF_THIS_SCRIPT() != INVALID_PARTICIPANT_INDEX()
		sRouletteData.sMetricsLight.m_hostID = NETWORK_GET_PLAYER_ACCOUNT_ID(NETWORK_GET_PLAYER_INDEX(NETWORK_GET_HOST_OF_THIS_SCRIPT()))
	ENDIF
	sRouletteData.sMetricsLight.m_membership = ROULETTE_METRICS_GET_PLAYER_MEMBERSHIP_LEVEL()
	 
	INT iTableID = sRouletteData.playerBD[NATIVE_TO_INT(PLAYER_ID())].iTableID
	IF iTableID <= -1
		EXIT
	ENDIF
	
	sRouletteData.sMetricsLight.m_tableID = iTableID
	
	IF ROULETTE_TABLE_IS_HIGH_STAKES(iTableID)
		sRouletteData.sMetricsLight.m_matchType = GET_HASH_KEY("high stakes")
		EXIT
	ENDIF
	
	sRouletteData.sMetricsLight.m_matchType = GET_HASH_KEY("standard")
ENDPROC

/// PURPOSE:
///    Initialises the light metric info ready for a new betting round 
PROC ROULETTE_METRICS_LIGHT_INIT_FOR_NEW_ROUND(ROULETTE_DATA &sRouletteData)
	ROULETTE_METRICS_LIGHT_COLLECT_ROUND_BEGIN_INFO(sRouletteData)	
	/// Light metrics does not reset timer on new round enter 
	/// as its the duration for their whole game session	
ENDPROC

/// PURPOSE:
///    Initialises the heavy metric info ready for a new betting round
PROC ROULETTE_METRICS_HEAVY_INIT_FOR_NEW_ROUND(ROULETTE_DATA &sRouletteData)
	STRUCT_ROULETTEMETRIC sEmpty
	COPY_SCRIPT_STRUCT(sRouletteData.sMetrics, sEmpty, SIZE_OF(STRUCT_ROULETTEMETRIC))

	ROULETTE_METRICS_HEAVY_COLLECT_ROUND_BEGIN_INFO(sRouletteData)
	RESET_NET_TIMER(sRouletteData.stMetricsTimer)
	START_NET_TIMER(sRouletteData.stMetricsTimer)
ENDPROC

/// PURPOSE:
///    Initialises the metrics info for a  betting new round of roulette
PROC ROULLETTE_METRICS_INIT_FOR_NEW_ROUND(ROULETTE_DATA &sRouletteData)
	ROULETTE_METRICS_LIGHT_INIT_FOR_NEW_ROUND(sRouletteData)
	ROULETTE_METRICS_HEAVY_INIT_FOR_NEW_ROUND(sRouletteData)
ENDPROC

/// PURPOSE:
///    Updates the endReason metric with the end of round outcome, i.e if the player
///    won, lost, or did not bet
PROC ROULETTE_METRICS_UPDATE_END_OF_ROUND_END_REASON(ROULETTE_DATA &sRouletteData)
	// Player won
	IF sRouletteData.iRoundPayout > 0
		ROULETTE_METRICS_UPDATE_END_REASON(sRouletteData, "win")
		EXIT
	ENDIF
	
	// Player lost their bet
	IF ROULETTE_HELPER_GET_LOCAL_PLAYERS_TOTAL_BET_FOR_CURRENT_ROUND(sRouletteData) > 0
		ROULETTE_METRICS_UPDATE_END_REASON(sRouletteData, "lose")
		EXIT
	ENDIF
	
	// Player did not bet
	ROULETTE_METRICS_UPDATE_END_REASON(sRouletteData, "no bet")
ENDPROC

/// PURPOSE:
///    Collects the required end of round metrics info
PROC ROULETTE_METRICS_LIGHT_COLLECT_ROUND_END_INFO(ROULETTE_DATA &sRouletteData)
	sRouletteData.sMetricsLight.m_betAmount1 += sRouletteData.iTotalInsideBetThisRound
	sRouletteData.sMetricsLight.m_betAmount2 += sRouletteData.iTotalOutsideBetThisRound
	sRouletteData.sMetricsLight.m_handsPlayed++
		
	IF (sRouletteData.iRoundPayout > 0)
		sRouletteData.sMetricsLight.m_winCount++
	ELIF ROULETTE_HELPER_GET_LOCAL_PLAYERS_TOTAL_BET_FOR_CURRENT_ROUND(sRouletteData) > 0
		sRouletteData.sMetricsLight.m_loseCount++
	ENDIF
	
	ROULETTE_METRICS_UPDATE_END_OF_ROUND_END_REASON(sRouletteData)
ENDPROC

/// PURPOSE:
///     Checks if the player has placed the max inside and outside bets this round
FUNC BOOL ROULETTE_METRICS_HAS_PLAYER_GONE_ALL_IN_THIS_ROUND(ROULETTE_DATA &sRouletteData)
	INT iInsideBet = sRouletteData.iTotalInsideBetThisRound
	INT iOutsideBet = sRouletteData.iTotalOutsideBetThisRound
	
	INT iMaxInsideBetForTable = ROULETTE_BETTING_GET_MAX_INSIDE_BET_FOR_CURRENT_TABLE(sRouletteData)
	INT iMaxOutsideBetForTable = ROULETTE_BETTING_GET_MAX_OUTSIDE_BET_FOR_CURRENT_TABLE(sRouletteData)
	
	BOOL bPlacedMaxInsideBet = iInsideBet >= iMaxInsideBetForTable
	BOOL bPlacedMaxOutsideBet = iOutsideBet >= iMaxOutsideBetForTable
	
	RETURN bPlacedMaxInsideBet AND bPlacedMaxOutsideBet
ENDFUNC

/// PURPOSE:
///    Collects all the required info for when a betting round ends
PROC ROULETTE_METRICS_HEAVY_COLLECT_ROUND_END_INFO(ROULETTE_DATA &sRouletteData)
	sRouletteData.sMetrics.m_casinoMetric.m_betAmount1 = sRouletteData.iTotalInsideBetThisRound
	sRouletteData.sMetrics.m_casinoMetric.m_betAmount2 = sRouletteData.iTotalOutsideBetThisRound	
	sRouletteData.sMetrics.m_casinoMetric.m_winAmount = sRouletteData.iRoundPayout
	sRouletteData.sMetrics.m_casinoMetric.m_allIn = ROULETTE_METRICS_HAS_PLAYER_GONE_ALL_IN_THIS_ROUND(sRouletteData)
	sRouletteData.sMetrics.m_casinoMetric.m_win = (sRouletteData.iRoundPayout > 0)
	
	INT iWinningNumber = ROULETTE_HELPER_GET_SERVERS_CURRENT_WINNING_NUMBER_FOR_TABLE(sRouletteData, sRouletteData.iTableID)
	sRouletteData.sMetrics.m_winningNumber = iWinningNumber
	sRouletteData.sMetrics.m_winningColour = ROULETTE_GET_COLOR_HASH_OF_ROULETTE_NUMBER(iWinningNumber) 
	sRouletteData.sMetrics.m_casinoMetric.m_timePlayed = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(sRouletteData.stMetricsTimer)
	sRouletteData.sMetrics.m_casinoMetric.m_finalChipBalance = ROULETTE_BETTING_GET_LOCAL_PLAYER_CHIP_COUNT()
	
	ROULETTE_METRICS_UPDATE_END_OF_ROUND_END_REASON(sRouletteData)
	
	INT i = 0
	INT iPlayerID = NATIVE_TO_INT(PLAYER_ID())
	REPEAT ROULETTE_BETTING_MAX_ALLOWED_BETS i
		INT iLayoutIndex = sRouletteData.playerBD[iPlayerID].iBets[i].iLayoutIndex
		ROULETTE_BET_TYPE eBetType = ROULETTE_BETTING_GET_BET_TYPE_FROM_LAYOUT_INDEX(iLayoutIndex)
		
		SWITCH eBetType
			CASE ROULETTE_BET_TYPE_STRAIGHT 	sRouletteData.sMetrics.m_iBetStraight = TRUE 		BREAK
			CASE ROULETTE_BET_TYPE_SPLIT 		sRouletteData.sMetrics.m_iBetSplit = TRUE			BREAK
			CASE ROULETTE_BET_TYPE_STREET 		sRouletteData.sMetrics.m_iBetStreet = TRUE     		BREAK
			CASE ROULETTE_BET_TYPE_TRIO																BREAK
			CASE ROULETTE_BET_TYPE_CORNER 		sRouletteData.sMetrics.m_iBetCorner = TRUE 			BREAK
			CASE ROULETTE_BET_TYPE_FIVE_NUMBER 	sRouletteData.sMetrics.m_iBetFiveNumber = TRUE		BREAK
			CASE ROULETTE_BET_TYPE_SIX_LINE		sRouletteData.sMetrics.m_iBetSixLine = TRUE			BREAK
			
			CASE ROULETTE_BET_TYPE_RED			sRouletteData.sMetrics.m_oBetRed = TRUE				BREAK
			CASE ROULETTE_BET_TYPE_BLACK		sRouletteData.sMetrics.m_oBetBlack = TRUE			BREAK
			CASE ROULETTE_BET_TYPE_ODD			sRouletteData.sMetrics.m_oBetOddNumber = TRUE		BREAK
			CASE ROULETTE_BET_TYPE_EVEN			sRouletteData.sMetrics.m_oBetEvenNumber = TRUE		BREAK
			CASE ROULETTE_BET_TYPE_LOW_BRACKET	sRouletteData.sMetrics.m_oBetLowBracket = TRUE		BREAK
			CASE ROULETTE_BET_TYPE_HIGH_BRACKET	sRouletteData.sMetrics.m_oBetHighBracket = TRUE		BREAK
			
			CASE ROULETTE_BET_TYPE_DOZEN_FIRST	
			CASE ROULETTE_BET_TYPE_DOZEN_SECOND		
			CASE ROULETTE_BET_TYPE_DOZEN_THIRD	
				sRouletteData.sMetrics.m_oBetDozen = TRUE 			
			BREAK
			
			CASE ROULETTE_BET_TYPE_COLUMN_FIRST	 	
			CASE ROULETTE_BET_TYPE_COLUMN_SECOND 	
			CASE ROULETTE_BET_TYPE_COLUMN_THIRD	
				sRouletteData.sMetrics.m_oBetColumn	= TRUE
			BREAK
		ENDSWITCH
	ENDREPEAT
	
	INT iTableID = sRouletteData.iTableID
	IF iTableID <= -1
		EXIT
	ENDIF
	
	sRouletteData.sMetrics.m_casinoMetric.m_playersAtTable = sRouletteData.serverBD.iTablePlayerCount[iTableID]
	sRouletteData.sMetrics.m_casinoMetric.m_handID = sRouletteData.serverBD.iRoundID[iTableID]
ENDPROC

/// PURPOSE:
///    Sends the light metric information if enabled. Collects information that
///    needs collecting on send
PROC ROULETTE_METRICS_LIGHT_SEND(ROULETTE_DATA &sRouletteData)	
	INT iChipCount = ROULETTE_BETTING_GET_LOCAL_PLAYER_CHIP_COUNT()
	sRouletteData.sMetricsLight.m_chipsDelta = iChipCount  - sRouletteData.iStartingChips
	sRouletteData.sMetricsLight.m_finalChipBalance = iChipCount
	sRouletteData.sMetricsLight.m_duration = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(sRouletteData.stMetricsTimerLight)
	sRouletteData.sMetricsLight.m_cheatCount = -1
	
	IF g_sMPTunables.bENABLE_ROULETTE_LIGHT
		#IF IS_DEBUG_BUILD
			ROULETTE_DEBUG_PRINT_CURRENT_LIGHT_METRICS(sRouletteData)
		#ENDIF
	
		PLAYSTATS_CASINO_ROULETTE_LIGHT(sRouletteData.sMetricsLight)
	ENDIF
	
	sRouletteData.sMetricsLight.m_endReason = GET_HASH_KEY("none")
ENDPROC

/// PURPOSE:
///    Sends the heavy metric information if enabled. Collects information that
///    needs collecting on send
PROC ROULETTE_METRICS_HEAVY_SEND(ROULETTE_DATA &sRouletteData)
	INT iChipCount = ROULETTE_BETTING_GET_LOCAL_PLAYER_CHIP_COUNT()	
	sRouletteData.sMetrics.m_casinoMetric.m_chipsDelta = iChipCount  - sRouletteData.iRoundStartingChips
	sRouletteData.sMetrics.m_casinoMetric.m_finalChipBalance = iChipCount
	sRouletteData.sMetrics.m_casinoMetric.m_timePlayed = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(sRouletteData.stMetricsTimer)
	sRouletteData.sMetrics.m_casinoMetric.m_cheat = FALSE
	
	IF g_sMPTunables.bENABLE_ROULETTE_HEAVY	
	AND ROULETTE_HELPER_GET_LOCAL_PLAYERS_TOTAL_BET_FOR_CURRENT_ROUND(sRouletteData) > 0
		
		#IF IS_DEBUG_BUILD
			ROULETTE_DEBUG_PRINT_CURRENT_HEAVY_METRICS(sRouletteData)
		#ENDIF
		
		PLAYSTATS_CASINO_ROULETTE(sRouletteData.sMetrics)	
	ENDIF
	
	// Reset end reason
	sRouletteData.sMetrics.m_casinoMetric.m_endReason = GET_HASH_KEY("none")
ENDPROC

