//////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Name:        	CasinoRoulette.sc																		
/// Description: 	Script file for running the casino roulette tables. Overrides base roulette functions 	
///					and constants to describe the table layout in the casino. 								
///																											
/// Written by:  Online Technical Team: Tom Turner,															
/// Date:  		28/02/2019
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Include overiding functionality, moved to own header so it can be used by other systems in the casino
USING "CasinoRouletteOverrides.sch"

// Include base roulette which has the script update loop
USING "Roulette.sch"
