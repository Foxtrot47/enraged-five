//////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Name:        RouletteTable.sch																			
/// Description: Header that handles the positioning of the table and getting positions relative to the table
///																											
/// Written by:  Online Technical Team: Tom Turner,															
/// Date:  		15/03/2019																					
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "RouletteData.sch"
USING "RouletteTableOverrides.sch"
USING "RouletteHelper.sch"

/// PURPOSE:
///    Converts the vector to one relative to the current table position   
FUNC VECTOR ROULETTE_TABLE_GET_VECTOR_RELATIVE_TO_TABLE(VECTOR vVectorToConvert, INT iTableId)
	FLOAT iTableHeading = ROULETTE_TABLE_GET_HEADING(iTableID)
	RETURN ROTATE_VECTOR_ABOUT_Z(vVectorToConvert, iTableHeading)
ENDFUNC 

FUNC VECTOR ROULETTE_TABLE_GET_VECTOR_ROTATED_TO_TABLE_SPACE(VECTOR vVectorToConvert, INT iTableId)
	FLOAT iTableHeading = -ROULETTE_TABLE_GET_HEADING(iTableID)
	RETURN ROTATE_VECTOR_ABOUT_Z(vVectorToConvert, iTableHeading)
ENDFUNC 

/// PURPOSE:
///    Gets the dealers offset from the table root 
FUNC VECTOR ROULETTE_TABLE_GET_DEALER_OFFSET()
	RETURN <<ROULETTE_TABLE_DEALER_POSITION_OFFSET_X,
	ROULETTE_TABLE_DEALER_POSITION_OFFSET_Y, ROULETTE_TABLE_DEALER_POSITION_OFFSET_Z>>
ENDFUNC

/// PURPOSE:
///    Gets the dealers world position for the given table 
FUNC VECTOR ROULETTE_TABLE_GET_DEALER_POSITION(INT iTableID)	
	RETURN ROULETTE_TABLE_GET_VECTOR_RELATIVE_TO_TABLE(ROULETTE_TABLE_GET_DEALER_OFFSET(), iTableId) +
	ROULETTE_TABLE_GET_POSITION(iTableID)
ENDFUNC

/// PURPOSE:
///    Gets the dealers world heading for the given table  
FUNC FLOAT ROULETTE_TABLE_GET_DEALER_HEADING(INT iTableID)
	// Get in degrees
	FLOAT iDealerHeading = ROULETTE_TABLE_DEALER_HEADING_IN_DEGREES
	FLOAT iTableHeading = ROULETTE_TABLE_GET_HEADING(iTableID)
	
	// Offset by table heading
	FLOAT fDiffDeg = iTableHeading - iDealerHeading
	fDiffDeg = WRAP(fDiffDeg, 0, 360)// Negative should wrap to positives
	
	// Return in rads
	RETURN fDiffDeg
ENDFUNC
