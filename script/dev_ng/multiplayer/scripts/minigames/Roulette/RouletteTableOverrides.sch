//////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Name:        RouletteTableOverrides.sch																			
/// Description: Header containing all overrideable functions and parameters relating to the roulette table
///				these should be overridden when defining, new table or seat positions, or using a different 
///				table asset
///																											
/// Written by:  Online Technical Team: Tom Turner,															
/// Date:  		15/03/2019																					
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "freemode_header.sch"

#IF NOT DEFINED(ROULETTE_TABLE_MAX_NUMBER_OF_TABLES) 
	/// PURPOSE: How many tables there are
	CONST_INT ROULETTE_TABLE_MAX_NUMBER_OF_TABLES 1 
#ENDIF

#IF NOT DEFINED(ROULETTE_TABLE_MAX_NUMBER_OF_PLAYERS_PER_TABLE)
	/// PURPOSE: How many people can play at a table
	CONST_INT ROULETTE_TABLE_MAX_NUMBER_OF_PLAYERS_PER_TABLE 4 
#ENDIF

CONST_INT ROULETTE_MAX_SEAT_ID (ROULETTE_TABLE_MAX_NUMBER_OF_TABLES * ROULETTE_TABLE_MAX_NUMBER_OF_PLAYERS_PER_TABLE)

#IF NOT DEFINED(ROULETTE_TABLE_LAYOUT_DECAL_HEIGHT_OFFSET)
	/// PURPOSE: The offset height of the layout decal from the table surface
	TWEAK_FLOAT ROULETTE_TABLE_LAYOUT_DECAL_HEIGHT_OFFSET  0.947
#ENDIF

#IF NOT DEFINED(ROULETTE_SEAT_LOCATE_RADIUS)
	/// TODO: move these to a seat requesting override file
	/// PURPOSE: The radius to check for a seat before allowing the player to sit in the seat
	CONST_FLOAT ROULETTE_SEAT_LOCATE_RADIUS 1.0
#ENDIF

#IF NOT DEFINED(ROULETTE_SEAT_HEADING_REQUIREMENT)
	/// TODO: move these to a seat requesting override file
	/// PURPOSE: The how much leeway the player has to face the seat in order to use it
	CONST_FLOAT ROULETTE_SEAT_HEADING_REQUIREMENT 60.0
#ENDIF


#IF NOT DEFINED(ROULETTE_SEAT_BLOCK_CHECK_RADIUS)
	/// PURPOSE: The radius to use when checking if the side of a seat is blocked preventing entry
	CONST_FLOAT ROULETTE_SEAT_BLOCK_CHECK_RADIUS 0.1
#ENDIF

#IF NOT DEFINED(ROULETTE_SEAT_REQUEST_TIME_OUT)
	/// PURPOSE: How long a seat request lasts before it times out
	CONST_INT ROULETTE_SEAT_REQUEST_TIME_OUT 5000
#ENDIF

/// PURPOSE: Dealer position and heading offsets relative to the table, tweakable to help with positioning
#IF NOT DEFINED(ROULETTE_TABLE_DEALER_POSITION_OFFSET_X) 	TWEAK_FLOAT ROULETTE_TABLE_DEALER_POSITION_OFFSET_X -0.680 #ENDIF
#IF NOT DEFINED(ROULETTE_TABLE_DEALER_POSITION_OFFSET_Y) 	TWEAK_FLOAT ROULETTE_TABLE_DEALER_POSITION_OFFSET_Y 0.970 #ENDIF
#IF NOT DEFINED(ROULETTE_TABLE_DEALER_POSITION_OFFSET_Z) 	TWEAK_FLOAT ROULETTE_TABLE_DEALER_POSITION_OFFSET_Z 0.0 #ENDIF
#IF NOT DEFINED(ROULETTE_TABLE_DEALER_HEADING_IN_DEGREES) 	TWEAK_FLOAT ROULETTE_TABLE_DEALER_HEADING_IN_DEGREES 0.0 #ENDIF

#IF NOT DEFINED(ROULETTE_TABLE_IS_HIGH_STAKES)
	/// PURPOSE:
	///    Checks if the given table ID is a high stakes table
	FUNC BOOL ROULETTE_TABLE_IS_HIGH_STAKES(INT iTableID)
		UNUSED_PARAMETER(iTableID)
		RETURN FALSE
	ENDFUNC
#ENDIF

#IF NOT DEFINED(ROULETTE_TABLE_GET_POSITION)
	/// PURPOSE:
	///    Default function for getting a table position   
	FUNC VECTOR ROULETTE_TABLE_GET_POSITION(INT iTableId)
		UNUSED_PARAMETER(iTableID)
		RETURN <<0, 0, 0>>	
	ENDFUNC
#ENDIF

#IF NOT DEFINED(ROULETTE_TABLE_GET_HEADING)
	/// PURPOSE:
	///    Default function for getting a table heading
	FUNC FLOAT ROULETTE_TABLE_GET_HEADING(INT iTableId)
		UNUSED_PARAMETER(iTableID)
		RETURN 0.0	
	ENDFUNC
#ENDIF

#IF NOT DEFINED(ROULETTE_TABLE_IS_LOCAL_SEAT_BLOCKED_ON_SIDE)
	/// PURPOSE:
	///    Default function for checking if a seat on a roulette table is blocked on one side
	FUNC BOOL ROULETTE_TABLE_IS_LOCAL_SEAT_BLOCKED_ON_SIDE(INT iLocalSeatID, BOOL bCheckRightSide)

		SWITCH iLocalSeatID
			// Last seat is blocked on the right
			CASE 3	RETURN bCheckRightSide
		ENDSWITCH
	
		RETURN FALSE
	ENDFUNC
#ENDIF

#IF NOT DEFINED(ROULETTE_TABLE_IS_DISABLED)
	FUNC BOOL ROULETTE_TABLE_IS_DISABLED(INT iTableID)
		UNUSED_PARAMETER(iTableID)
		RETURN FALSE
	ENDFUNC
#ENDIF
