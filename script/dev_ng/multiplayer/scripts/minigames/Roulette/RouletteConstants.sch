//////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Name:        RouletteConstants.sch																		
/// Description: Header for containing all constants used by roullette										
///																											
/// Written by:  Online Technical Team: Tom Turner,															
/// Date:  		12/02/2019																					
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "Globals.sch"

// TODO: replace these with with global that can be changed by background script
CONST_INT ROULETTE_MIN_INSIDE_BET 10
CONST_INT ROULETTE_MAX_INSIDE_BET 500

CONST_INT ROULETTE_MIN_HIGH_STAKES_INSIDE_BET 100
CONST_INT ROULETTE_MAX_HIGH_STAKES_INSIDE_BET 5000

CONST_INT ROULETTE_MIN_OUTSIDE_BET 10
CONST_INT ROULETTE_MAX_OUTSIDE_BET 5000

CONST_INT ROULETTE_MIN_HIGH_STAKES_OUTSIDE_BET 100
CONST_INT ROULETTE_MAX_HIGH_STAKES_OUTSIDE_BET 50000

CONST_INT ROULETTE_PAYOUT_STRAIGHT_BET 35
CONST_INT ROULETTE_PAYOUT_SPLIT_BET 17
CONST_INT ROULETTE_PAYOUT_STREET_BET 11
CONST_INT ROULETTE_PAYOUT_TRIO_BET 11
CONST_INT ROULETTE_PAYOUT_CORNER_BET 8
CONST_INT ROULETTE_PAYOUT_FIVE_NUMBER_BET 6
CONST_INT ROULETTE_PAYOUT_SIX_LINE_BET 5
CONST_INT ROULETTE_PAYOUT_COLOUR_BET 1
CONST_INT ROULETTE_PAYOUT_ODDS_EVENS_BET 1
CONST_INT ROULETTE_PAYOUT_HIGH_LOW_BET 1
CONST_INT ROULETTE_PAYOUT_DOZENS_BET 2
CONST_INT ROULETTE_PAYOUT_COLUMNS_BET 2  

// Double 00 is not an actual number so we have an identifer for it
CONST_INT ROULETTE_DOUBLE_ZERO_IDENTIFIER 37

// The max possible number in a game of roulette (37 because of double 0)
CONST_INT ROULETTE_MAX_ROULETTE_NUMBER 37

// Animators don't count from 0
CONST_INT ROULETTE_MAX_ANIM_INDEX 38

// How many numbers a street bet consists of
CONST_INT ROULETTE_NUMBERS_IN_A_STREET_BET_COUNT 3

// Range bet types mins and maxs
CONST_INT ROULETTE_LOW_BRACKET_MIN 1
CONST_INT ROULETTE_LOW_BRACKET_MAX 18

CONST_INT ROULETTE_HIGH_BRACKET_MIN 19
CONST_INT ROULETTE_HIGH_BRACKET_MAX 36

CONST_INT ROULETTE_DOZEN_FIRST_MIN 1 
CONST_INT ROULETTE_DOZEN_FIRST_MAX 12

CONST_INT ROULETTE_DOZEN_SECOND_MIN 13
CONST_INT ROULETTE_DOZEN_SECOND_MAX 24

CONST_INT ROULETTE_DOZEN_THIRD_MIN 25
CONST_INT ROULETTE_DOZEN_THIRD_MAX 36

// How many outside bets ther are
CONST_INT ROULETTE_MAX_OUTSIDE_BET_TYPES 12

// How many numbers could be highlighted at a time, most would be for odds or evens numbers
CONST_INT ROULETTE_MAX_HIGHLIGHTED_NUMBERS 20

// How many winning numbers it will store to display as history
CONST_INT ROULETTE_MAX_WIN_HISTORY 6

/// PURPOSE: 
///    Defines the various types of bets that can be
///    placed on a roulette board
ENUM ROULETTE_BET_TYPE	
	// Inside bets
	ROULETTE_BET_TYPE_STRAIGHT,
	ROULETTE_BET_TYPE_SPLIT,
	ROULETTE_BET_TYPE_STREET,
	ROULETTE_BET_TYPE_TRIO,
	ROULETTE_BET_TYPE_CORNER,
	ROULETTE_BET_TYPE_FIVE_NUMBER,
	ROULETTE_BET_TYPE_SIX_LINE,
	
	// Outside bets
	ROULETTE_BET_TYPE_RED,
	ROULETTE_BET_TYPE_BLACK,
	ROULETTE_BET_TYPE_ODD,
	ROULETTE_BET_TYPE_EVEN,
	ROULETTE_BET_TYPE_LOW_BRACKET,
	ROULETTE_BET_TYPE_HIGH_BRACKET,
	ROULETTE_BET_TYPE_DOZEN_FIRST,
	ROULETTE_BET_TYPE_DOZEN_SECOND,
	ROULETTE_BET_TYPE_DOZEN_THIRD,
	ROULETTE_BET_TYPE_COLUMN_FIRST,
	ROULETTE_BET_TYPE_COLUMN_SECOND,
	ROULETTE_BET_TYPE_COLUMN_THIRD,
	
	ROULETTE_BET_TYPE_INVALID = -1
ENDENUM

/// PURPOSE:
///    Gets the bet type name as a string 
FUNC STRING ROULETTE_GET_BET_TYPE_NAME_AS_STRING(ROULETTE_BET_TYPE eBetType)
	SWITCH eBetType
		CASE ROULETTE_BET_TYPE_STRAIGHT			RETURN "Straight"
		CASE ROULETTE_BET_TYPE_SPLIT			RETURN "Split"
		CASE ROULETTE_BET_TYPE_STREET			RETURN "Street"
		CASE ROULETTE_BET_TYPE_TRIO				RETURN "Trio"
		CASE ROULETTE_BET_TYPE_CORNER			RETURN "Corner"
		CASE ROULETTE_BET_TYPE_FIVE_NUMBER		RETURN "Five number"
		CASE ROULETTE_BET_TYPE_SIX_LINE			RETURN "Six line"
		CASE ROULETTE_BET_TYPE_RED				RETURN "Red"
		CASE ROULETTE_BET_TYPE_BLACK			RETURN "Black"
		CASE ROULETTE_BET_TYPE_ODD				RETURN "Odd"
		CASE ROULETTE_BET_TYPE_EVEN				RETURN "Even"
		CASE ROULETTE_BET_TYPE_LOW_BRACKET		RETURN "Low bracket"
		CASE ROULETTE_BET_TYPE_HIGH_BRACKET		RETURN "High bracket"
		CASE ROULETTE_BET_TYPE_DOZEN_FIRST		RETURN "First dozen"
		CASE ROULETTE_BET_TYPE_DOZEN_SECOND		RETURN "Second dozen"
		CASE ROULETTE_BET_TYPE_DOZEN_THIRD		RETURN "Third dozen"
		CASE ROULETTE_BET_TYPE_COLUMN_FIRST		RETURN "First column"
		CASE ROULETTE_BET_TYPE_COLUMN_SECOND	RETURN "Second column"
		CASE ROULETTE_BET_TYPE_COLUMN_THIRD		RETURN "Third column"
		CASE ROULETTE_BET_TYPE_INVALID			RETURN "Invalid"
	ENDSWITCH
	RETURN "INVALID ENUM"
ENDFUNC

/// PURPOSE: 
///    Denominations used by roulette, they are equal to their values
///    do not try to iterate on them!
ENUM ROULETTE_CHIP_DENOMINATIONS
	INVALID_CHIP_DENOMINATION = -1,
	TEN_DOLLAR_CHIP = 0,
	FIFTY_DOLLAR_CHIP,
	ONE_HUNDRED_DOLLAR_CHIP,
	FIVE_HUNDRED_DOLLAR_CHIP,
	ONE_THOUSAND_DOLLAR_CHIP,
	FIVE_THOUSAND_DOLLAR_CHIP,
	TEN_THOUSAND_DOLLAR_CHIP
ENDENUM

FUNC STRING ROULETTE_CHIP_DENOMINATIONS_GET_ENUM_AS_STRING(ROULETTE_CHIP_DENOMINATIONS eDenomination)

	SWITCH eDenomination
		CASE INVALID_CHIP_DENOMINATION 	RETURN "INVALID_CHIP_DENOMINATION"
		CASE TEN_DOLLAR_CHIP 			RETURN "TEN_DOLLAR_CHIP"
		CASE FIFTY_DOLLAR_CHIP			RETURN "FIFTY_DOLLAR_CHIP"
		CASE ONE_HUNDRED_DOLLAR_CHIP	RETURN "ONE_HUNDRED_DOLLAR_CHIP"
		CASE FIVE_HUNDRED_DOLLAR_CHIP	RETURN "FIVE_HUNDRED_DOLLAR_CHIP"
		CASE ONE_THOUSAND_DOLLAR_CHIP	RETURN "ONE_THOUSAND_DOLLAR_CHIP"
		CASE FIVE_THOUSAND_DOLLAR_CHIP	RETURN "FIVE_THOUSAND_DOLLAR_CHIP"
		CASE TEN_THOUSAND_DOLLAR_CHIP	RETURN "TEN_THOUSAND_DOLLAR_CHIP"
	ENDSWITCH
	
	ASSERTLN(DEBUG_ROULETTE, "Denomination enum(int):", ENUM_TO_INT(eDenomination), 
	" does not have a conversion to string, is it missing from the switch?")
	
	RETURN "ERROR_NO_STRING_CONVERSION"
ENDFUNC

/// PURPOSE:
///    Gets value of chip denomination 
FUNC INT ROULETTE_GET_CHIP_DENOMINATION_VALUE(ROULETTE_CHIP_DENOMINATIONS eChipDenomination)
	SWITCH eChipDenomination
		CASE TEN_DOLLAR_CHIP RETURN 10
		CASE FIFTY_DOLLAR_CHIP RETURN 50
		CASE ONE_HUNDRED_DOLLAR_CHIP RETURN 100
		CASE FIVE_HUNDRED_DOLLAR_CHIP RETURN 500
		CASE ONE_THOUSAND_DOLLAR_CHIP RETURN 1000
		CASE FIVE_THOUSAND_DOLLAR_CHIP RETURN 5000
		CASE TEN_THOUSAND_DOLLAR_CHIP RETURN 10000
	ENDSWITCH
	
	RETURN 0
ENDFUNC

/// PURPOSE:
///    Gets the largest chip denomination that can cover the value   
FUNC ROULETTE_CHIP_DENOMINATIONS ROULETTE_GET_LARGEST_CHIP_DENOMINATION_FOR_GIVEN_VALUE(INT iValue)
	
	IF iValue >= 10000
		RETURN TEN_THOUSAND_DOLLAR_CHIP
	ENDIF
	
	IF iValue >= 5000
		RETURN FIVE_THOUSAND_DOLLAR_CHIP
	ENDIF
	
	IF iValue >= 1000
		RETURN ONE_THOUSAND_DOLLAR_CHIP
	ENDIF
	
	IF iValue >= 500
		RETURN FIVE_HUNDRED_DOLLAR_CHIP
	ENDIF
	
	IF iValue >= 100
		RETURN ONE_HUNDRED_DOLLAR_CHIP
	ENDIF
	
	IF iValue >= 50
		RETURN FIFTY_DOLLAR_CHIP
	ENDIF
	
	RETURN TEN_DOLLAR_CHIP
ENDFUNC

/// PURPOSE:
///    Gets the chip denomination value as a string  
FUNC STRING ROULETTE_GET_CHIP_DENOMINATION_VALUE_AS_STRING(ROULETTE_CHIP_DENOMINATIONS eChipDenomination)
	RETURN CONVERT_INT_TO_STRING(ROULETTE_GET_CHIP_DENOMINATION_VALUE(eChipDenomination))
ENDFUNC


///--------------------------------------------
///    Look ups for roulette numbers
///--------------------------------------------

/// PURPOSE:
///    Gets the index of the given roulette number    
FUNC INT ROULETTE_GET_SLOT_INDEX_FROM_ROULETTE_NUMBER(INT iRouletteNumber)
  SWITCH iRouletteNumber
		CASE  0		RETURN	0		 
		CASE  28	RETURN	1	
		CASE  9		RETURN	2		
		CASE  26	RETURN	3	
		CASE  30	RETURN	4	
		CASE  11	RETURN	5	
		CASE  7		RETURN	6		
		CASE  20	RETURN	7	
		CASE  32	RETURN	8	
		CASE  17	RETURN	9	
		CASE  5		RETURN	10		
		CASE  22	RETURN	11	
		CASE  34	RETURN	12	
		CASE  15	RETURN	13	
		CASE  3		RETURN	14		
		CASE  24	RETURN	15	
		CASE  36	RETURN	16	
		CASE  13	RETURN	17	
		CASE  1		RETURN	18		
		CASE  ROULETTE_DOUBLE_ZERO_IDENTIFIER	RETURN	19			
		CASE  27	RETURN	20	
		CASE  10	RETURN	21	
		CASE  25	RETURN	22	
		CASE  29	RETURN	23	
		CASE  12	RETURN	24	
		CASE  8		RETURN	25		
		CASE  19	RETURN	26	
		CASE  31	RETURN	27	
		CASE  18	RETURN	28	
		CASE  6		RETURN	29		
		CASE  21	RETURN	30	
		CASE  33	RETURN	31	
		CASE  16	RETURN	32	
		CASE  4		RETURN	33		
		CASE  23	RETURN	34	
		CASE  35	RETURN	35	
		CASE  14	RETURN	36	
		CASE  2		RETURN	37	
  ENDSWITCH
  
  RETURN -1
ENDFUNC

/// PURPOSE:
///    Gets the actual roulette number for the given index   
FUNC INT ROULETTE_GET_NUMBER_FROM_SLOT_INDEX(INT iSlotIndex)
  	SWITCH iSlotIndex
		CASE 0		RETURN 0
		CASE 1		RETURN 28
		CASE 2		RETURN 9
		CASE 3		RETURN 26
		CASE 4		RETURN 30
		CASE 5		RETURN 11
		CASE 6		RETURN 7
		CASE 7		RETURN 20
		CASE 8		RETURN 32
		CASE 9		RETURN 17
		CASE 10		RETURN 5
		CASE 11		RETURN 22
		CASE 12		RETURN 34
		CASE 13		RETURN 15
		CASE 14		RETURN 3
		CASE 15		RETURN 24
		CASE 16		RETURN 36
		CASE 17		RETURN 13
		CASE 18		RETURN 1
		CASE 19		RETURN ROULETTE_DOUBLE_ZERO_IDENTIFIER// No such thing as 00 so it is stored as 37
		CASE 20		RETURN 27
		CASE 21		RETURN 10
		CASE 22		RETURN 25
		CASE 23		RETURN 29
		CASE 24		RETURN 12
		CASE 25		RETURN 8
		CASE 26		RETURN 19
		CASE 27		RETURN 31
		CASE 28		RETURN 18
		CASE 29		RETURN 6
		CASE 30		RETURN 21
		CASE 31		RETURN 33
		CASE 32		RETURN 16
		CASE 33		RETURN 4
		CASE 34		RETURN 23
		CASE 35		RETURN 35
		CASE 36		RETURN 14
		CASE 37		RETURN 2
  	ENDSWITCH
  
  	RETURN -1
ENDFUNC

/// PURPOSE:
///    Converts the roulette index to a roulette animation index
///    TODO: Swap to only using animation index to fix inconsistency
FUNC INT ROULETTE_GET_ANIMATION_INDEX_FOR_ROULETTE_NUMBER(INT iRouletteNumber)
	
	SWITCH iRouletteNumber
		CASE  0		RETURN	20		 
		CASE  28	RETURN	21	
		CASE  9		RETURN	22		
		CASE  26	RETURN	23	
		CASE  30	RETURN	24	
		CASE  11	RETURN	25	
		CASE  7		RETURN	26		
		CASE  20	RETURN	27	
		CASE  32	RETURN	28	
		CASE  17	RETURN	29	
		CASE  5		RETURN	30		
		CASE  22	RETURN	31	
		CASE  34	RETURN	32	
		CASE  15	RETURN	33	
		CASE  3		RETURN	34		
		CASE  24	RETURN	35	
		CASE  36	RETURN	36	
		CASE  13	RETURN	37	
		CASE  1		RETURN	38		
		CASE  ROULETTE_DOUBLE_ZERO_IDENTIFIER	RETURN	1			
		CASE  27	RETURN	2	
		CASE  10	RETURN	3	
		CASE  25	RETURN	4	
		CASE  29	RETURN	5	
		CASE  12	RETURN	6	
		CASE  8		RETURN	7		
		CASE  19	RETURN	8	
		CASE  31	RETURN	9	
		CASE  18	RETURN	10	
		CASE  6		RETURN	11		
		CASE  21	RETURN	12	
		CASE  33	RETURN	13	
		CASE  16	RETURN	14	
		CASE  4		RETURN	15		
		CASE  23	RETURN	16	
		CASE  35	RETURN	17	
		CASE  14	RETURN	18	
		CASE  2		RETURN	19	
	ENDSWITCH
	
	RETURN -1
ENDFUNC

/// PURPOSE:
///    Gets the actual roulette number associated with the given layout index   
FUNC INT ROULETTE_GET_NUMBER_FROM_LAYOUT_INDEX(INT iLayoutIndex)	
	SWITCH iLayoutIndex
		CASE 2		RETURN 0
		CASE 4		RETURN ROULETTE_DOUBLE_ZERO_IDENTIFIER// this should be 19
		CASE 19		RETURN 1
		CASE 21		RETURN 2
		CASE 23 	RETURN 3
		CASE 35		RETURN 4
		CASE 37		RETURN 5
		CASE 39		RETURN 6
		CASE 51		RETURN 7
		CASE 53 	RETURN 8
		CASE 55 	RETURN 9
		CASE 67 	RETURN 10
		CASE 69 	RETURN 11
		CASE 71		RETURN 12
		CASE 83		RETURN 13
		CASE 85		RETURN 14
		CASE 87		RETURN 15
		CASE 99		RETURN 16
		CASE 101 	RETURN 17
		CASE 103 	RETURN 18
		CASE 115 	RETURN 19
		CASE 117	RETURN 20
		CASE 119	RETURN 21
		CASE 131	RETURN 22
		CASE 133	RETURN 23
		CASE 135	RETURN 24
		CASE 147	RETURN 25
		CASE 149	RETURN 26
		CASE 151	RETURN 27
		CASE 163	RETURN 28
		CASE 165	RETURN 29
		CASE 167	RETURN 30
		CASE 179	RETURN 31
		CASE 181	RETURN 32
		CASE 183	RETURN 33
		CASE 195	RETURN 34
		CASE 197	RETURN 35
		CASE 199	RETURN 36
  	ENDSWITCH
	
	RETURN -1
ENDFUNC

/// PURPOSE:
///    Gets the layout index associated with the given roulette number    
FUNC INT ROULETTE_GET_LAYOUT_INDEX_FROM_ROULETTE_NUMBER(INT iRouletteNumber)
	
	SWITCH iRouletteNumber
		CASE 0	RETURN 2			
		CASE 1	RETURN 19		
		CASE 2	RETURN 21		
		CASE 3	RETURN 23 	
		CASE 4	RETURN 35		
		CASE 5	RETURN 37		
		CASE 6	RETURN 39		
		CASE 7	RETURN 51		
		CASE 8	RETURN 53 	
		CASE 9	RETURN 55 	
		CASE 10	RETURN 67 	
		CASE 11	RETURN 69 	
		CASE 12	RETURN 71		
		CASE 13	RETURN 83		
		CASE 14	RETURN 85		
		CASE 15	RETURN 87		
		CASE 16	RETURN 99		
		CASE 17	RETURN 101 	
		CASE 18	RETURN 103 	
		CASE 19	RETURN 115 	
		CASE 20	RETURN 117	
		CASE 21	RETURN 119	
		CASE 22	RETURN 131	
		CASE 23	RETURN 133	
		CASE 24	RETURN 135	
		CASE 25	RETURN 147	
		CASE 26	RETURN 149	
		CASE 27	RETURN 151	
		CASE 28	RETURN 163	
		CASE 29	RETURN 165	
		CASE 30	RETURN 167	
		CASE 31	RETURN 179	
		CASE 32	RETURN 181	
		CASE 33	RETURN 183	
		CASE 34	RETURN 195	
		CASE 35	RETURN 197	
		CASE 36	RETURN 199
		CASE ROULETTE_DOUBLE_ZERO_IDENTIFIER	RETURN 4	
  	ENDSWITCH
	
	RETURN -1
ENDFUNC

/// PURPOSE:
///    Gets the roulette number index associated with the given layout index    
FUNC INT ROULETTE_GET_SLOT_INDEX_FROM_LAYOUT_INDEX(INT iLayoutIndex)
	INT iRouletteNumber = ROULETTE_GET_NUMBER_FROM_LAYOUT_INDEX(iLayoutIndex)
	RETURN ROULETTE_GET_SLOT_INDEX_FROM_ROULETTE_NUMBER(iRouletteNumber)
ENDFUNC

/// PURPOSE:
///    Gets the layout index associated with the given roulette slot index   
FUNC INT ROULETTE_GET_LAYOUT_INDEX_FROM_SLOT_INDEX(INT iSlotIndex)
	INT iRouletteNumber = ROULETTE_GET_NUMBER_FROM_SLOT_INDEX(iSlotIndex)
	RETURN ROULETTE_GET_LAYOUT_INDEX_FROM_ROULETTE_NUMBER(iRouletteNumber)
ENDFUNC

/// PURPOSE:
///    Gets the bet type colour for the given roulette number index
///    i.e. black or red
FUNC ROULETTE_BET_TYPE ROULETTE_GET_COLOR_BET_TYPE_OF_ROULETTE_SLOT_INDEX(INT iIndex)
	// If any of the zeroes return invalid as these are green and there is no
	// reason to care about greens
	IF iIndex = ROULETTE_GET_SLOT_INDEX_FROM_ROULETTE_NUMBER(0) OR
	iIndex = ROULETTE_GET_SLOT_INDEX_FROM_ROULETTE_NUMBER(ROULETTE_DOUBLE_ZERO_IDENTIFIER)
		RETURN ROULETTE_BET_TYPE_INVALID
	ENDIF
	
	// Every other index is a red number
	IF iIndex % 2 = 0
		RETURN ROULETTE_BET_TYPE_RED
	ENDIF
	
	RETURN ROULETTE_BET_TYPE_BLACK
ENDFUNC

FUNC ROULETTE_BET_TYPE ROULETTE_GET_COLOR_BET_TYPE_OF_ROULETTE_NUMBER(INT iRouletteNumber)	
	RETURN ROULETTE_GET_COLOR_BET_TYPE_OF_ROULETTE_SLOT_INDEX(ROULETTE_GET_SLOT_INDEX_FROM_ROULETTE_NUMBER(iRouletteNumber))
ENDFUNC

/// PURPOSE:
///    Gets the bet type colour associated with the given layout index
///    i.e. black or red
FUNC ROULETTE_BET_TYPE ROULETTE_GET_COLOR_BET_TYPE_OF_ROULETTE_LAYOUT_INDEX(INT iLayoutIndex)
	INT iRouletteNumberIndex = ROULETTE_GET_SLOT_INDEX_FROM_LAYOUT_INDEX(iLayoutIndex)
	RETURN ROULETTE_GET_COLOR_BET_TYPE_OF_ROULETTE_SLOT_INDEX(iRouletteNumberIndex)
ENDFUNC

FUNC INT ROULETTE_GET_COLOR_HASH_OF_ROULETTE_NUMBER_INDEX(INT iIndex)
	ROULETTE_BET_TYPE iNumberIndex = ROULETTE_GET_COLOR_BET_TYPE_OF_ROULETTE_SLOT_INDEX(iIndex) 
	SWITCH iNumberIndex
		CASE ROULETTE_BET_TYPE_BLACK RETURN GET_HASH_KEY("black")
		CASE ROULETTE_BET_TYPE_RED	 RETURN GET_HASH_KEY("red")
	ENDSWITCH
	RETURN GET_HASH_KEY("green")
ENDFUNC

FUNC INT ROULETTE_GET_COLOR_HASH_OF_ROULETTE_NUMBER(INT iRouletteNumber)
	INT iNumberIndex = ROULETTE_GET_SLOT_INDEX_FROM_ROULETTE_NUMBER(iRouletteNumber)
	RETURN ROULETTE_GET_COLOR_HASH_OF_ROULETTE_NUMBER_INDEX(iNumberIndex)
ENDFUNC

/// PURPOSE:
///    Generates and returns a new random winning number for roulette   
FUNC INT ROULETTE_GENERATE_WINNING_NUMBER()
	RETURN GET_RANDOM_MWC_INT_IN_RANGE(0, ROULETTE_MAX_ROULETTE_NUMBER + 1)
ENDFUNC
