USING "freemode_header.sch"

#IF NOT DEFINED(ROULETTE_EVENT_ON_ROUND_BEGIN)
	PROC ROULETTE_EVENT_ON_ROUND_BEGIN()
		
	ENDPROC
#ENDIF

#IF NOT DEFINED(ROULETTE_EVENT_ON_ROUND_END)
	PROC ROULETTE_EVENT_ON_ROUND_END()
		
	ENDPROC
#ENDIF

#IF NOT DEFINED(ROULETTTE_EVENT_ON_LOSE)
	PROC ROULETTTE_EVENT_ON_LOSE(INT iAmountLost)
		UNUSED_PARAMETER(iAmountLost)
	ENDPROC
#ENDIF

#IF NOT DEFINED(ROULETTTE_EVENT_ON_WIN)
	PROC ROULETTTE_EVENT_ON_WIN(INT iAmountWon)
		UNUSED_PARAMETER(iAmountWon)
	ENDPROC
#ENDIF


#IF NOT DEFINED(ROULETTE_EVENT_ON_JOIN_GAME)
	PROC ROULETTE_EVENT_ON_JOIN_GAME()
		
	ENDPROC
#ENDIF

#IF NOT DEFINED(ROULETTE_EVENT_ON_LEAVE_GAME)
	PROC ROULETTE_EVENT_ON_LEAVE_GAME()
		
	ENDPROC
#ENDIF

