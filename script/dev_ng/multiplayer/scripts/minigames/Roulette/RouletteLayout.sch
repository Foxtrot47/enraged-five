//////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Name:        RouletteLayout.sch																			
/// Description: Header for describing the roulette board (known as the layout) and handling its navigation	
///																											
/// Written by:  Online Technical Team: Tom Turner,															
/// Date:  		12/02/2019																					
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "RouletteHelper.sch"
USING "RouletteConstants.sch"
USING "RouletteData.sch"
USING "RouletteTable.sch"

USING "RouletteLayoutOverrides.sch"

// Inside bets coords range
CONST_INT LAYOUT_MIN_INSIDE_BET_X_COORD 2
CONST_INT LAYOUT_MAX_INSIDE_BET_X_COORD 7
CONST_INT LAYOUT_MIN_INSIDE_BET_Y_COORD 1
CONST_INT LAYOUT_MAX_INSIDE_BET_Y_COORD 25

// Zeroes bet x coords range
CONST_INT LAYOUT_MIN_ZEROES_X_COORD 2
CONST_INT LAYOUT_MAX_ZEROES_X_COORD 4

// Column bets x coords
CONST_INT LAYOUT_MIN_COLUMN_BET_X_COORD 3
CONST_INT LAYOUT_MAX_COLUMN_BET_X_COORD 5
CONST_INT LAYOUT_COLUMN_BET_CENTRE_X_COORD 4

// Middle of the line between the zeroes and inside bets
CONST_INT LAYOUT_INSIDE_BETS_CENTRE_X_COORD 5

// Max y coords for outside bet columns
CONST_INT FIRST_OUTSIDE_BET_MAX_Y_COORD 5
CONST_INT SECOND_OUTSIDE_BET_MAX_Y_COORD 2

// The line between outside and inside bets
CONST_INT OUTSIDE_BET_LINE_MAX_Y_COORD 24

/// PURPOSE:
///    Gets the origin offsets for the layout as a vector    
FUNC VECTOR ROULETTE_LAYOUT_GET_ORIGIN_OFFSET()
	RETURN <<ROULETTE_LAYOUT_ORIGIN_OFFSET_X, 
	ROULETTE_LAYOUT_ORIGIN_OFFSET_Y,
	ROULETTE_TABLE_LAYOUT_DECAL_HEIGHT_OFFSET>>
ENDFUNC

/// PURPOSE:
///    Gets the layouts origin position relative to the given table
FUNC VECTOR ROULETTE_LAYOUT_GET_ORIGIN_FOR_TABLE(INT iTableID)
	RETURN ROULETTE_TABLE_GET_VECTOR_RELATIVE_TO_TABLE(ROULETTE_LAYOUT_GET_ORIGIN_OFFSET(), iTableID) +	ROULETTE_TABLE_GET_POSITION(iTableID)
ENDFUNC

/// PURPOSE:
///    Converts a roulette layout 2d grid coordinate to its 1d equivalent   
FUNC INT ROULETTE_LAYOUT_COORD_TO_LAYOUT_INDEX(INT iXCoord, INT iYCoord)
	RETURN iYCoord * (LAYOUT_MAX_INSIDE_BET_X_COORD + 1) + iXCoord
ENDFUNC

/// PURPOSE:
///    Converts roulette layout 1d index to its 2d grid coord equivalent
PROC ROULETTE_LAYOUT_INDEX_TO_LAYOUT_COORD(INT iIndex, INT &iXCoord, INT &iYCoord)
	iXCoord = iIndex % (LAYOUT_MAX_INSIDE_BET_X_COORD + 1)
	iYCoord = iIndex / (LAYOUT_MAX_INSIDE_BET_X_COORD + 1)
ENDPROC

/// PURPOSE:
///		Checks if the column index is in an outside bet column 
FUNC BOOL ROULETTE_LAYOUT_IS_COORD_IN_AN_OUTSIDE_BET_COLUMN(INT iXCoord, INT iYCoord)
	// Y coord not needed to figure this out but keep it for api reasons
	UNUSED_PARAMETER(iYCoord)
	RETURN iXCoord < LAYOUT_MIN_INSIDE_BET_X_COORD
ENDFUNC

/// PURPOSE:
///		Checks if the column index is in an outside bet column
FUNC BOOL ROULETTE_LAYOUT_IS_COORD_IN_AN_INSIDE_BET_COLUMN(INT iXCoord, INT iYCoord)
	UNUSED_PARAMETER(iYCoord)
	RETURN NOT ROULETTE_LAYOUT_IS_COORD_IN_AN_OUTSIDE_BET_COLUMN(iXCoord, iYCoord)
ENDFUNC

/// PURPOSE:
///    Checks if the position is on the last row of inside bets before the column bet starts  
FUNC BOOL ROULETTE_LAYOUT_IS_COORD_IN_LAST_ROW_OF_INSIDE_BETS(INT iColumnIndex, INT iRowIndex)
	RETURN iColumnIndex >= LAYOUT_MIN_COLUMN_BET_X_COORD AND iRowIndex = OUTSIDE_BET_LINE_MAX_Y_COORD
ENDFUNC

/// PURPOSE:
///    Checks if the coord is in the first outside bet column    
FUNC BOOL ROULETTE_LAYOUT_IS_COORD_IN_FIRST_OUTSIDE_BET_COLUMN(INT iXCoord, INT iYCoord)
	UNUSED_PARAMETER(iYCoord)
	RETURN iXCoord = 0
ENDFUNC

/// PURPOSE:
///    Checks if the coord is in the second outside bet column  
FUNC BOOL ROULETTE_LAYOUT_IS_COORD_IN_SECOND_OUTSIDE_BET_COLUMN(INT iXCoord, INT iYCoord)
UNUSED_PARAMETER(iYCoord)
	RETURN iXCoord = 1
ENDFUNC

/// PURPOSE:
///    Checks if the given layout index is in an outside bet column
FUNC BOOL ROULETTE_LAYOUT_IS_INDEX_IN_AN_OUTSIDE_BET_COLUMN(INT iLayoutIndex)
	INT iXCoord = 0
	INT iYCoord = 0
	ROULETTE_LAYOUT_INDEX_TO_LAYOUT_COORD(iLayoutIndex, iXCoord, iYCoord)
	RETURN ROULETTE_LAYOUT_IS_COORD_IN_AN_OUTSIDE_BET_COLUMN(iXCoord, iYCoord)
ENDFUNC

/// PURPOSE:
///    Checks if the layout position is on the line between outside and inside bets   
FUNC BOOL ROULETTE_LAYOUT_IS_COORD_ON_OUTSIDE_BET_LINE(INT iXCoord, INT iYCoord)
	RETURN iXCoord = LAYOUT_MIN_INSIDE_BET_X_COORD AND iYCoord >= LAYOUT_MIN_INSIDE_BET_Y_COORD
ENDFUNC

/// PURPOSE:
///    Checks if the given layout coord is at the top of the line between outside and inside bets    
FUNC BOOL ROULETTE_LAYOUT_IS_LAYOUT_COORD_ON_TOP_OF_OUTSIDE_BET_LINE(INT iXCoord, INT iYCoord)
	RETURN iXCoord = LAYOUT_MIN_INSIDE_BET_X_COORD AND iYCoord = LAYOUT_MIN_INSIDE_BET_Y_COORD
ENDFUNC

/// PURPOSE:
///    Checks if the layout position is on a column bet (bottom most row of the layout)   
FUNC BOOL ROULETTE_LAYOUT_IS_COORD_ON_A_COLUMN_BET_CELL(INT iXCoord, INT iYCoord)
	RETURN iXCoord >= LAYOUT_MIN_COLUMN_BET_X_COORD AND iYCoord = LAYOUT_MAX_INSIDE_BET_Y_COORD
ENDFUNC

/// PURPOSE:
///    Checks if the layout position is on a zeroes bet cell 
FUNC BOOL ROULETTE_LAYOUT_IS_COORD_ON_A_ZEROES_CELL(INT iXCoord, INT iYCoord)
	RETURN iYCoord = 0 AND iXCoord >= LAYOUT_MIN_ZEROES_X_COORD 
	AND iXCoord != LAYOUT_MIN_ZEROES_X_COORD + 1// Middle is actually a split bet
ENDFUNC

/// PURPOSE:
///    Checks if the layout position is on a zeroes bet (top most row of the layout) 
FUNC BOOL ROULETTE_LAYOUT_IS_COORD_ON_A_ZEROES_ROW(INT iXCoord, INT iYCoord)
	RETURN iYCoord = 0 AND iXCoord >= LAYOUT_MIN_ZEROES_X_COORD 
ENDFUNC

/// PURPOSE:
///     Checks if the layout position is on the line between zeroes and inside bets  
FUNC BOOL ROULETTE_LAYOUT_IS_COORD_ON_ZEROES_LINE(INT iXCoord, INT iYCoord)
	RETURN iXCoord > LAYOUT_MIN_INSIDE_BET_X_COORD AND iYCoord = LAYOUT_MIN_INSIDE_BET_Y_COORD
ENDFUNC

/// PURPOSE:
///    Checks if the given layout coord is on an inside bet
FUNC BOOL ROULETTE_LAYOUT_IS_COORD_ON_AN_INSIDE_BET_CELL(INT iXCoord, INT iYCoord)
	RETURN ROULETTE_LAYOUT_IS_COORD_ON_A_ZEROES_CELL(iXCoord, iYCoord) OR 
	(iXCoord >= LAYOUT_MIN_INSIDE_BET_X_COORD AND iYCoord < LAYOUT_MAX_INSIDE_BET_Y_COORD)
ENDFUNC

/// PURPOSE:
///    Checks if the given coord is on any outside bet cell
FUNC BOOL ROULETTE_LAYOUT_IS_COORD_ON_AN_OUTSIDE_BET_CELL(INT iXCoord, INT iYCoord)
	RETURN ROULETTE_LAYOUT_IS_COORD_IN_AN_OUTSIDE_BET_COLUMN(iXCoord, iYCoord) OR ROULETTE_LAYOUT_IS_COORD_ON_A_COLUMN_BET_CELL(iXCoord, iYCoord)
ENDFUNC

/// PURPOSE:
///    Checks if the layout index on a zeroes bet cell   
FUNC BOOL ROULETTE_LAYOUT_IS_INDEX_ON_A_ZEROES_BET_CELL(INT iLayoutIndex)
	INT iXCoord = 0
	INT iYCoord = 0
	ROULETTE_LAYOUT_INDEX_TO_LAYOUT_COORD(iLayoutIndex, iXCoord, iYCoord)
	RETURN ROULETTE_LAYOUT_IS_COORD_ON_A_ZEROES_CELL(iXCoord, iYCoord)
ENDFUNC

/// PURPOSE:
///    Checks if the layout index on a zeroes bet row
FUNC BOOL ROULETTE_LAYOUT_IS_INDEX_ON_A_ZEROES_BET_ROW(INT iLayoutIndex)
	INT iXCoord = 0
	INT iYCoord = 0
	ROULETTE_LAYOUT_INDEX_TO_LAYOUT_COORD(iLayoutIndex, iXCoord, iYCoord)
	RETURN ROULETTE_LAYOUT_IS_COORD_ON_A_ZEROES_ROW(iXCoord, iYCoord)
ENDFUNC

/// PURPOSE:
///    Checks if the layout index on an inside bet cell   
FUNC BOOL ROULETTE_LAYOUT_IS_INDEX_ON_AN_INSIDE_BET_CELL(INT iLayoutIndex)
	INT iXCoord = 0
	INT iYCoord = 0
	ROULETTE_LAYOUT_INDEX_TO_LAYOUT_COORD(iLayoutIndex, iXCoord, iYCoord)
	RETURN ROULETTE_LAYOUT_IS_COORD_ON_AN_INSIDE_BET_CELL(iXCoord, iYCoord)
ENDFUNC

/// PURPOSE:
///    Checks if the coord is in the third zone of the table  
FUNC BOOL ROULETTE_LAYOUT_IS_COORD_IN_ZONE_ONE(INT iXCoord, INT iYCoord)
	// Outside bets have different cell heights
	SWITCH iXCoord
		CASE 0 RETURN iYCoord < 2
		CASE 1 RETURN iYCoord < 1
	ENDSWITCH
	
	RETURN iYCoord < 9
ENDFUNC

/// PURPOSE:
///    Checks if the coord is in the second zone of the table  
FUNC BOOL ROULETTE_LAYOUT_IS_COORD_IN_ZONE_TWO(INT iXCoord, INT iYCoord)
	SWITCH iXCoord
		CASE 0 RETURN iYCoord < 4 AND iYCoord > 1
		CASE 1 RETURN iYCoord < 2 AND iYCoord > 0
	ENDSWITCH
	
	RETURN iYCoord < 17 AND iYCoord > 8
ENDFUNC

/// PURPOSE:
///    Checks if the coord is in the first zone of the table   
FUNC BOOL ROULETTE_LAYOUT_IS_COORD_IN_ZONE_THREE(INT iXCoord, INT iYCoord)
	SWITCH iXCoord
		CASE 0 RETURN  iYCoord > 3
		CASE 1 RETURN  iYCoord > 1
	ENDSWITCH
	
	RETURN iYCoord > 16 AND iYCoord > 8
ENDFUNC

/// PURPOSE:
///    Checks if the coord is in the given zone  
FUNC BOOL ROULETTE_LAYOUT_IS_COORD_IN_ZONE(INT iXCoord, INT iYCoord, INT iZoneID)
	
	SWITCH iZoneID
		CASE ROULETTE_PLAYER_BS_HAS_BET_IN_ZONE_ONE 	RETURN ROULETTE_LAYOUT_IS_COORD_IN_ZONE_ONE(iXCoord, iYCoord)
		CASE ROULETTE_PLAYER_BS_HAS_BET_IN_ZONE_TWO		RETURN ROULETTE_LAYOUT_IS_COORD_IN_ZONE_TWO(iXCoord, iYCoord)
		CASE ROULETTE_PLAYER_BS_HAS_BET_IN_ZONE_THREE	RETURN ROULETTE_LAYOUT_IS_COORD_IN_ZONE_THREE(iXCoord, iYCoord)	
	ENDSWITCH
	
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_LAYOUT_IS_COORD_IN_ZONE - Invalid zone id: ", iZoneID)
	DEBUG_PRINTCALLSTACK()
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Gets the zone index the given layout coord is in   
FUNC INT ROULETTE_LAYOUT_GET_ZONE_FOR_COORD(INT iXCoord, INT iYCoord)
	// Determine which zone the bet is being removed from
	IF ROULETTE_LAYOUT_IS_COORD_IN_ZONE_THREE(iXCoord, iYCoord)
		RETURN ROULETTE_PLAYER_BS_HAS_BET_IN_ZONE_THREE
	ELIF ROULETTE_LAYOUT_IS_COORD_IN_ZONE_TWO(iXCoord, iYCoord)
		RETURN ROULETTE_PLAYER_BS_HAS_BET_IN_ZONE_TWO
	ELIF ROULETTE_LAYOUT_IS_COORD_IN_ZONE_ONE(iXCoord, iYCoord)
		RETURN ROULETTE_PLAYER_BS_HAS_BET_IN_ZONE_ONE
	ENDIF
	
	RETURN -1
ENDFUNC

/// PURPOSE:
///    Checks if the given layout index is in the given 'column bet' column   
FUNC BOOL ROULETTE_LAYOUT_IS_INDEX_ON_A_COLUMN_BET_CELL(INT iLayoutIndex, ROULETTE_BET_TYPE eRouletteColumnBetType)
	INT iXCoord = 0
	INT iYCoord = 0
	ROULETTE_LAYOUT_INDEX_TO_LAYOUT_COORD(iLayoutIndex, iXCoord, iYCoord)
	
	// First zeroes row doesn't count
	IF iYCoord = 0
		RETURN FALSE
	ENDIF
	
	SWITCH eRouletteColumnBetType
		CASE ROULETTE_BET_TYPE_COLUMN_FIRST
			RETURN iXCoord = LAYOUT_MIN_COLUMN_BET_X_COORD
		CASE ROULETTE_BET_TYPE_COLUMN_SECOND
			RETURN iXCoord = LAYOUT_COLUMN_BET_CENTRE_X_COORD + 1// Skip lines
		CASE ROULETTE_BET_TYPE_COLUMN_THIRD	
			RETURN iXCoord = LAYOUT_MAX_COLUMN_BET_X_COORD + 2// Skip lines
	ENDSWITCH
	
	RETURN FALSE	
ENDFUNC

/// PURPOSE:
///    Gets the minimum value the y coordinate could be, based on the coordinates position on the layout
///    useful for clamping coord to layout extents when incrementing/decrementing coord
FUNC INT ROULETTE_LAYOUT_GET_MIN_Y_FOR_COORD(INT iXCoord, INT iYCoord)
	
	/// Line between outside and inside bets starts at 1 as well as the centre
	/// of inside bets
	IF iXCoord = LAYOUT_MIN_INSIDE_BET_X_COORD AND iYCoord != LAYOUT_MIN_INSIDE_BET_Y_COORD OR iXCoord = LAYOUT_INSIDE_BETS_CENTRE_X_COORD
		RETURN LAYOUT_MIN_INSIDE_BET_Y_COORD
	ENDIF
	RETURN 0
ENDFUNC

/// PURPOSE:
///    Gets the maximum value the y coordinate could be, based on the coordinates position on the layout
///    useful for clamping coord to layout extents when incrementing/decrementing coord
FUNC INT ROULETTE_LAYOUT_GET_MAX_Y_FOR_COORD(INT iXCoord, INT iYCoord)
	UNUSED_PARAMETER(iYCoord)
	SWITCH iXCoord
		CASE 0 RETURN FIRST_OUTSIDE_BET_MAX_Y_COORD
		CASE 1 RETURN SECOND_OUTSIDE_BET_MAX_Y_COORD
		CASE 2 RETURN OUTSIDE_BET_LINE_MAX_Y_COORD
	ENDSWITCH
	
	RETURN LAYOUT_MAX_INSIDE_BET_Y_COORD
ENDFUNC

/// PURPOSE:
///    Gets the minimum value the x coordinate could be, based on the coordinates position on the layout
///    useful for clamping coord to layout extents when incrementing/decrementing coord
FUNC INT ROULETTE_LAYOUT_GET_MIN_X_FOR_COORD(INT iXCoord, INT iYCoord)
	
	// Top of line between inside bets wraps like an inside coord
	IF ROULETTE_LAYOUT_IS_LAYOUT_COORD_ON_TOP_OF_OUTSIDE_BET_LINE(iXCoord, iYCoord)
		RETURN LAYOUT_MIN_INSIDE_BET_X_COORD
	ENDIF
	
	IF ROULETTE_LAYOUT_IS_COORD_ON_A_ZEROES_CELL(iXCoord, iYCoord)
		RETURN LAYOUT_MIN_ZEROES_X_COORD
	ENDIF

	IF ROULETTE_LAYOUT_IS_COORD_ON_A_COLUMN_BET_CELL(iXCoord, iYCoord)
		RETURN LAYOUT_MIN_COLUMN_BET_X_COORD
	ENDIF	
	
	IF ROULETTE_LAYOUT_IS_COORD_IN_AN_INSIDE_BET_COLUMN(iXCoord, iYCoord)
		RETURN LAYOUT_MIN_INSIDE_BET_X_COORD
	ENDIF
	
	RETURN 0
ENDFUNC

/// PURPOSE:
///    Gets the maximum value the x coordinate could be, based on the coordinates position on the layout
///    useful for clamping coord to layout extents when incrementing/decrementing coord  
FUNC INT ROULETTE_LAYOUT_GET_MAX_X_FOR_COORD(INT iXCoord, INT iYCoord)	
	IF ROULETTE_LAYOUT_IS_COORD_ON_A_ZEROES_CELL(iXCoord, iYCoord)
		RETURN LAYOUT_MAX_ZEROES_X_COORD
	ENDIF
	
	IF ROULETTE_LAYOUT_IS_COORD_ON_A_COLUMN_BET_CELL(iXCoord, iYCoord)
		RETURN LAYOUT_MAX_COLUMN_BET_X_COORD
	ENDIF
	
	RETURN LAYOUT_MAX_INSIDE_BET_X_COORD
ENDFUNC

/// PURPOSE:
///    Gets the height of the grid cell for the given grid position   
FUNC FLOAT ROULETTE_LAYOUT_GET_HEIGHT_OF_CELL_AT_COORD(INT iXCoord, INT iYCoord)	
	IF ROULETTE_LAYOUT_IS_COORD_ON_A_ZEROES_CELL(iXCoord, iYCoord)
		RETURN ROULETTE_LAYOUT_ZEROES_GRID_CELL_HEIGHT
	ENDIF
	
	SWITCH iXCoord
		// outside bet first column has cells two high
		CASE 0 RETURN ROULETTE_LAYOUT_INSIDE_GRID_CELL_HEIGHT * 2
		
		// outside bet second column has cells 4 high
		CASE 1 RETURN ROULETTE_LAYOUT_INSIDE_GRID_CELL_HEIGHT * 4
	ENDSWITCH
	
	// All the rest are one cell high
	RETURN ROULETTE_LAYOUT_INSIDE_GRID_CELL_HEIGHT
ENDFUNC

/// PURPOSE:
///    Gets the zeroes grid cells width (calculated using the inside bets cell width) 
FUNC FLOAT ROULETTE_LAYOUT_GET_ZEROES_CELL_WIDTH()
	RETURN (ROULETTE_LAYOUT_INSIDE_GRID_CELL_WIDTH * 3) * 0.5
ENDFUNC

/// PURPOSE:
///    Gets the width of the grid cell for the given grid position    
FUNC FLOAT ROULETTE_LAYOUT_GET_CELL_WIDTH_AT_COORD(INT iXCoord, INT iYCoord)
	IF ROULETTE_LAYOUT_IS_COORD_ON_A_ZEROES_ROW(iXCoord, iYCoord)
		RETURN ROULETTE_LAYOUT_GET_ZEROES_CELL_WIDTH()
	ENDIF
	
	// Cell width is slightly diiferent for outside bet columns
	IF ROULETTE_LAYOUT_IS_COORD_IN_FIRST_OUTSIDE_BET_COLUMN(iXCoord, iYCoord) OR
	ROULETTE_LAYOUT_IS_COORD_IN_SECOND_OUTSIDE_BET_COLUMN(iXCoord, iYCoord)
		RETURN ROULETTE_LAYOUT_OUTSIDE_GRID_CELL_WIDTH
	ENDIF
	
	// Normal inside bet cell width
	RETURN ROULETTE_LAYOUT_INSIDE_GRID_CELL_WIDTH
ENDFUNC

/// PURPOSE:
///    Gets the offset from the layout origin for an inside bet with the given coordinate
///    inside bets are calculated differently as they include lines
FUNC VECTOR ROULETTE_LAYOUT_GET_INSIDE_BET_OFFSET_POSITION_FOR_COORD(INT iXCoord, INT iYCoord)
	FLOAT fXBetCoord = 0
	FLOAT fYBetCoord = 0
	
	FLOAT fWidth = ROULETTE_LAYOUT_GET_CELL_WIDTH_AT_COORD(iXCoord, iYCoord)
	FLOAT fHeight = ROULETTE_LAYOUT_GET_HEIGHT_OF_CELL_AT_COORD(iXCoord, iYCoord)
	
	// Subtract the number of outside bets
	INT iXOrigin = iXCoord - 2
	
	// Offset by the widths of the two outside bets
	fXBetCoord = (iXOrigin * fWidth) + (2 * ROULETTE_LAYOUT_OUTSIDE_GRID_CELL_WIDTH)
	fYBetCoord = (iYCoord * fHeight) 
	
	// half so lines are included
	fYBetCoord *= 0.5
			
	IF iYCoord != LAYOUT_MAX_INSIDE_BET_Y_COORD
		// Move up a half to re-align to lines
		fYBetCoord -= (fHeight * 0.5)
		
		// If the zero row
		IF iYCoord = 0	
			fXBetCoord += fWidth 
		ENDIF
	ELSE
		// Column bet row
	 	INT temp = iXCoord - 3
		fXBetCoord += fWidth * temp
	ENDIF
	
	RETURN <<fYBetCoord, fXBetCoord, 0>>
ENDFUNC

/// PURPOSE:
///    Gets the offset from the layout origin for an outside bet with the given coordinate     
FUNC VECTOR ROULETTE_LAYOUT_GET_OUTSIDE_BET_OFFSET_POSITION_FOR_COORD(INT iXCoord, INT iYCoord)
	FLOAT fXBetCoord = 0
	FLOAT fYBetCoord = 0
	
	FLOAT fWidth = ROULETTE_LAYOUT_GET_CELL_WIDTH_AT_COORD(iXCoord, iYCoord)
	FLOAT fHeight = ROULETTE_LAYOUT_GET_HEIGHT_OF_CELL_AT_COORD(iXCoord, iYCoord)
	
	// Top left coord
	fXBetCoord = (iXCoord * fWidth)
	fYBetCoord = (iYCoord * fHeight)
	
	// Center the coord
	fXBetCoord += (fWidth * 0.5)
	fYBetCoord += (fHeight * 0.5)
	
	RETURN <<fYBetCoord, fXBetCoord, 0>>
ENDFUNC

/// PURPOSE:
///     Gets the postion for the layout coordinate relative to the given table 
FUNC VECTOR ROULETTE_LAYOUT_GET_BET_POSITION_ON_TABLE_FOR_LAYOUT_COORD(INT iTableId, INT iXCoord, INT iYCoord)	
	VECTOR vBetPosition = <<0, 0, 0>>
	
	IF ROULETTE_LAYOUT_IS_COORD_IN_AN_INSIDE_BET_COLUMN(iXCoord, iYCoord)
		vBetPosition = ROULETTE_LAYOUT_GET_INSIDE_BET_OFFSET_POSITION_FOR_COORD(iXCoord, iYCoord)
	ELSE
		vBetPosition = ROULETTE_LAYOUT_GET_OUTSIDE_BET_OFFSET_POSITION_FOR_COORD(iXCoord, iYCoord)
	ENDIF
	
	RETURN ROULETTE_LAYOUT_GET_ORIGIN_FOR_TABLE(iTableID) + ROULETTE_TABLE_GET_VECTOR_RELATIVE_TO_TABLE(vBetPosition, iTableID)
ENDFUNC

/// PURPOSE:
///   	Gets the postion for the layout coordinate relative to the given table using a layout index     
FUNC VECTOR ROULETTE_LAYOUT_GET_BET_POSITION_ON_TABLE_FOR_LAYOUT_INDEX(INT iIndex, INT iTableID)
	INT iXCoord = 0
	INT iYCoord = 0
	ROULETTE_LAYOUT_INDEX_TO_LAYOUT_COORD(iIndex, iXCoord, iYCoord)
	RETURN ROULETTE_LAYOUT_GET_BET_POSITION_ON_TABLE_FOR_LAYOUT_COORD(iTableID, iXCoord, iYCoord)
ENDFUNC

/// PURPOSE:
///    Sets cursor coords to invalid if they are out of the bounds of the layout
PROC ROULETTE_LAYOUT_ENFORCE_VALID_LAYOUT_COORDS(INT &iXCoord, INT &iYCoord)
	IF NOT IS_INT_IN_RANGE_INCLUSIVE(iXCoord,
	ROULETTE_LAYOUT_GET_MIN_X_FOR_COORD(iXCoord, iYCoord),
	ROULETTE_LAYOUT_GET_MAX_X_FOR_COORD(iXCoord, iYCoord))
		iXCoord = -1
		iYCoord = -1
	ENDIF
	
	IF NOT IS_INT_IN_RANGE_INCLUSIVE(iYCoord,
	ROULETTE_LAYOUT_GET_MIN_Y_FOR_COORD(iXCoord, iYCoord),
	ROULETTE_LAYOUT_GET_MAX_Y_FOR_COORD(iXCoord, iYCoord))
		iXCoord = -1
		iYCoord = -1
	ENDIF
ENDPROC

/// PURPOSE:
///    Translates layout cursors real world position to a layout coordinate 
PROC ROULETTE_LAYOUT_GET_WORLD_POSITION_AS_LAYOUT_COORD_ON_TABLE(VECTOR vWorldPosition, INT &iXCoord, INT &iYCoord, INT iTableID)
	VECTOR vTableOrigin = ROULETTE_LAYOUT_GET_ORIGIN_FOR_TABLE(iTableID)
	
	// Convert cursor to the origin space
	vWorldPosition -= vTableOrigin
	vWorldPosition = ROULETTE_TABLE_GET_VECTOR_ROTATED_TO_TABLE_SPACE(vWorldPosition, iTableID)
	
	// Swap axis as the board is viewed landscape however coords are portraight
	FLOAT fXTemp = vWorldPosition.x
	vWorldPosition.x = vWorldPosition.y
	vWorldPosition.y = fXTemp	

	FLOAT fOutsideBetsXOffset = (2 * ROULETTE_LAYOUT_OUTSIDE_GRID_CELL_WIDTH)
	FLOAT fMaxYCoord = ((ROULETTE_LAYOUT_INSIDE_GRID_CELL_HEIGHT * 0.5) * (LAYOUT_MAX_INSIDE_BET_Y_COORD + 1))
	FLOAT fMinYCoord = - ROULETTE_LAYOUT_ZEROES_GRID_CELL_HEIGHT
	FLOAT fMaxXCoord = fOutsideBetsXOffset + (ROULETTE_LAYOUT_INSIDE_GRID_CELL_WIDTH * 2) * 3
	
	// Invalid out of bounds
	IF vWorldPosition.x < 0 OR vWorldPosition.x > fMaxXCoord  
	OR vWorldPosition.y > fMaxYCoord OR vWorldPosition.y < fMinYCoord
		iXCoord = -1
		iYCoord = -1
		EXIT
	ENDIF
	
	// If first outside column
	IF vWorldPosition.x < 1 * ROULETTE_LAYOUT_OUTSIDE_GRID_CELL_WIDTH
		FLOAT fFirstOutsideColumnHeight = ROULETTE_LAYOUT_INSIDE_GRID_CELL_HEIGHT * 2
		iXCoord = 0
		iYCoord = ROUND((vWorldPosition.y - (fFirstOutsideColumnHeight * 0.5))/ fFirstOutsideColumnHeight)
		ROULETTE_LAYOUT_ENFORCE_VALID_LAYOUT_COORDS(iXCoord, iYCoord)
		EXIT
	ENDIF
	
	// If second outside column
	IF vWorldPosition.x < 2 * ROULETTE_LAYOUT_OUTSIDE_GRID_CELL_WIDTH           
		FLOAT fFirstOutsideColumnHeight = ROULETTE_LAYOUT_INSIDE_GRID_CELL_HEIGHT * 4
		iXCoord = 1
		iYCoord = ROUND((vWorldPosition.y - (fFirstOutsideColumnHeight * 0.5))/ fFirstOutsideColumnHeight)
		ROULETTE_LAYOUT_ENFORCE_VALID_LAYOUT_COORDS(iXCoord, iYCoord)
		EXIT
	ENDIF
	
	//  Inside bet
	FLOAT fXOrigin = (vWorldPosition.x - ROULETTE_LAYOUT_INSIDE_GRID_CELL_WIDTH * 0.5) + fOutsideBetsXOffset	
	FLOAT fXBetCoord = (fXOrigin / ROULETTE_LAYOUT_INSIDE_GRID_CELL_WIDTH) - fOutsideBetsXOffset
	iYCoord = ROUND(vWorldPosition.y / (ROULETTE_LAYOUT_INSIDE_GRID_CELL_HEIGHT * 0.5)) + 1				
	iXCoord = ROUND(fXBetCoord) - 1
	
	// Last line of inside bet column isn't used so set it to the column instead
	IF iXCoord = LAYOUT_MAX_INSIDE_BET_X_COORD + 1
		iXCoord = LAYOUT_MAX_INSIDE_BET_X_COORD
	ENDIF
	ROULETTE_LAYOUT_ENFORCE_VALID_LAYOUT_COORDS(iXCoord, iYCoord)
	
	// Zeroes row exception
	IF vWorldPosition.y < -(ROULETTE_LAYOUT_INSIDE_GRID_CELL_HEIGHT * 0.25)
		// First cell
		iYCoord = 0
		iXCoord = 2
		
		FLOAT fSplitZeroDetectionOffset = ROULETTE_LAYOUT_INSIDE_GRID_CELL_WIDTH * 0.8
		
		// Last cell
		IF vWorldPosition.x  > fOutsideBetsXOffset + (ROULETTE_LAYOUT_INSIDE_GRID_CELL_WIDTH * 3) + fSplitZeroDetectionOffset	
			iXCoord = 4
			EXIT
		ENDIF
		
		// Middle cell
		IF vWorldPosition.x  > fOutsideBetsXOffset + (ROULETTE_LAYOUT_INSIDE_GRID_CELL_WIDTH * 2) + fSplitZeroDetectionOffset	
			iXCoord = 3
		ENDIF
		
		EXIT
	ENDIF
	
	// Column bet exception uses full height and width of cell, ignores lines
	IF vWorldPosition.y >= ((ROULETTE_LAYOUT_INSIDE_GRID_CELL_HEIGHT * 0.5) * (LAYOUT_MAX_INSIDE_BET_Y_COORD - 1))	
		
		// Determine X
		IF vWorldPosition.x > fOutsideBetsXOffset + (ROULETTE_LAYOUT_INSIDE_GRID_CELL_WIDTH * 2) * 2
			iXCoord = LAYOUT_MAX_COLUMN_BET_X_COORD
		ELIF vWorldPosition.x > fOutsideBetsXOffset + (ROULETTE_LAYOUT_INSIDE_GRID_CELL_WIDTH * 2)
			iXCoord = LAYOUT_COLUMN_BET_CENTRE_X_COORD
		ELSE
			iXCoord = LAYOUT_MIN_COLUMN_BET_X_COORD
		ENDIF

		// Column bets x
		iYCoord = LAYOUT_MAX_INSIDE_BET_Y_COORD
		ROULETTE_LAYOUT_ENFORCE_VALID_LAYOUT_COORDS(iXCoord, iYCoord)
		EXIT
	ENDIF
		
	// Exception for last row as it uses full height of cell (line at bottom isn't used)
	IF vWorldPosition.y >= ((ROULETTE_LAYOUT_INSIDE_GRID_CELL_HEIGHT * 0.5) * (LAYOUT_MAX_INSIDE_BET_Y_COORD - 2))
		// Last inside bet row	
		iYCoord = LAYOUT_MAX_INSIDE_BET_Y_COORD - 1
		ROULETTE_LAYOUT_ENFORCE_VALID_LAYOUT_COORDS(iXCoord, iYCoord)
	ENDIF
ENDPROC

/// PURPOSE:
///    Applies movement exceptions for moving down from a zeroes position
///    bMovingLeft and bMovingRight define if it is a downwards diagonal movement
PROC ROULETTE_LAYOUT_APPLY_ZEROES_MOVING_DOWN_NAVIGATION_EXCEPTIONS(INT &iXCoord, INT &iYCoord, BOOL bMovingRight, BOOL bMovingLeft)			
	// move down a row
	iYCoord++
	
	// Moving down and to the right
	IF bMovingRight

		// If at the last column move to last column of the inside bets
		IF iXCoord = LAYOUT_MAX_ZEROES_X_COORD 
			iXCoord = LAYOUT_MAX_INSIDE_BET_X_COORD
			EXIT
		ENDIF
		
		// If on the first zero move position to one position left of the middle bet line
		iXCoord = LAYOUT_INSIDE_BETS_CENTRE_X_COORD - 1
		EXIT
	ENDIF
	
	// Moving down and to the left
	IF bMovingLeft
		
		// if at last zero move position to one right from centre bet line
		IF iXCoord = LAYOUT_MAX_ZEROES_X_COORD		
			iXCoord = LAYOUT_INSIDE_BETS_CENTRE_X_COORD + 1
			EXIT
		ENDIF
		
		// If on first zero move one in from start of inside bets
		iXCoord = LAYOUT_MIN_ZEROES_X_COORD + 1
		EXIT
	ENDIF
	
	// Moving down only
	IF iXCoord = LAYOUT_MAX_ZEROES_X_COORD
		iXCoord = LAYOUT_INSIDE_BETS_CENTRE_X_COORD + 1
		EXIT
	ENDIF

	iXCoord = LAYOUT_MIN_ZEROES_X_COORD + 1
ENDPROC

/// PURPOSE:
///   Applies movement exceptions for a zeroes layout position
///   returns true if an exception has been applied
FUNC BOOL ROULETTE_LAYOUT_APPLY_ZEROES_NAVIGATION_EXCEPTION(INT &iXCoord, INT &iYCoord, INT iXIncrement, INT iYIncrement)
	
	IF NOT ROULETTE_LAYOUT_IS_COORD_ON_A_ZEROES_ROW(iXCoord, iYCoord)
		RETURN FALSE
	ENDIF
	
	BOOL bMovingLeft = iXIncrement < 0
	BOOL bMovingRight = iXIncrement > 0
	BOOL bMovingDown = iYIncrement > 0	
	
	IF bMovingDown 
		ROULETTE_LAYOUT_APPLY_ZEROES_MOVING_DOWN_NAVIGATION_EXCEPTIONS(iXCoord, iYCoord, bMovingRight, bMovingLeft)
		RETURN TRUE
	ENDIF
	
	IF bMovingLeft
		iXCoord--	
	ENDIF
	
	IF bMovingRight
		iXCoord++
	ENDIF
		
	iXCoord = CLAMP_INT(iXCoord, LAYOUT_MIN_ZEROES_X_COORD, LAYOUT_MAX_ZEROES_X_COORD)	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///   Applies movement exceptions for positions on the line between zeroes and inside bets
///   returns true if an exception has been applied 
FUNC BOOL ROULETTE_LAYOUT_APPLY_ZEROES_LINE_NAVIGATION_EXCEPTION(INT &iXCoord, INT &iYCoord, INT iXIncrement, INT iYIncrement)
	UNUSED_PARAMETER(iXIncrement)
	
	IF NOT ROULETTE_LAYOUT_IS_COORD_ON_ZEROES_LINE(iXCoord, iYCoord)
		RETURN FALSE
	ENDIF
	
	BOOL bMovingUp = iYIncrement < 0
	
	// only moving up has an exception
	IF bMovingUp			
		// Up from any position left of centre line wraps to bottom left
		IF iXCoord <= LAYOUT_INSIDE_BETS_CENTRE_X_COORD
			iYCoord--	
			iXCoord = LAYOUT_MIN_ZEROES_X_COORD
			RETURN TRUE
		ENDIF
		
		// Up from any position right of centre line wraps to  right
		IF iXCoord > LAYOUT_INSIDE_BETS_CENTRE_X_COORD
			iYCoord--	
			iXCoord = LAYOUT_MAX_ZEROES_X_COORD
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Applies movement exceptions for moving up from a column position
///    bMovingLeft and bMovingRight define if it is an upwards diagonal movement
PROC ROULETTE_LAYOUT_APPLY_COLUMN_BET_MOVING_UP_NAVIGATION_EXCEPTION(INT &iXCoord, INT &iYCoord, BOOL bMovingRight, BOOL bMovingLeft)
	// Move up a row
	iYCoord--
	
	// For only moving up
	IF NOT bMovingLeft AND NOT bMovingRight		
		
		// If in the middle column move up to middle column of the inside bets
		IF iXCoord = LAYOUT_COLUMN_BET_CENTRE_X_COORD
			iXCoord = LAYOUT_INSIDE_BETS_CENTRE_X_COORD
		
		// If in last column move to last column of inside bets 
		ELIF iXCoord = LAYOUT_MAX_COLUMN_BET_X_COORD
			iXCoord = LAYOUT_MAX_INSIDE_BET_X_COORD
		ENDIF
		
		// First column bet already maps correctly to first column of inside bets
		EXIT
	ENDIF
		
	// Left diagonal
	IF bMovingLeft
	
		// Centre column already maps correctly to column above
		IF iXCoord = LAYOUT_COLUMN_BET_CENTRE_X_COORD		
			EXIT
		ENDIF
		
		// Moving left diagonal from last column maps to one left from max inside column
		IF iXCoord = LAYOUT_MAX_COLUMN_BET_X_COORD
			iXCoord = LAYOUT_MAX_INSIDE_BET_X_COORD - 1
			EXIT
		ENDIF
	ENDIF
	
	// Right diagonal
	IF bMovingRight
		IF iXCoord = LAYOUT_COLUMN_BET_CENTRE_X_COORD
			iXCoord = LAYOUT_MAX_INSIDE_BET_X_COORD - 1
			EXIT
		ENDIF
		
		IF iXCoord = LAYOUT_MIN_COLUMN_BET_X_COORD
			iXCoord = LAYOUT_MIN_INSIDE_BET_X_COORD + 1
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Applies movement exceptions for moving down from a column position
PROC ROULETTE_LAYOUT_APPLY_COLUMN_BET_MOVING_DOWN_NAVIGATION_EXCEPTION(INT &iXCoord, INT &iYCoord)
	// Move to top of grid as column bet is the last possible row
	iYCoord = 0
	
	SWITCH	iXCoord		
		// If on first column bet column x is the first zeroes column
		CASE LAYOUT_MIN_COLUMN_BET_X_COORD	
			iXCoord = LAYOUT_MIN_ZEROES_X_COORD
		BREAK
		
		// If in the middle column bet column set position to midle of zeroes line
		CASE LAYOUT_COLUMN_BET_CENTRE_X_COORD		
			iXCoord = LAYOUT_INSIDE_BETS_CENTRE_X_COORD
			iYCoord = LAYOUT_MIN_INSIDE_BET_Y_COORD
		BREAK
		
		// If in last column bet column set x as last zeroes column
		CASE LAYOUT_MAX_COLUMN_BET_X_COORD
			iXCoord = LAYOUT_MAX_ZEROES_X_COORD
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///   Applies movement exceptions for a column bet layout position
///   returns true if an exception has been applied       
FUNC BOOL ROULETTE_LAYOUT_APPLY_COLUMN_BET_NAVIGATION_EXCEPTION(INT &iXCoord, INT &iYCoord, INT iXIncrement, INT iYIncrement)	
	
	IF NOT ROULETTE_LAYOUT_IS_COORD_ON_A_COLUMN_BET_CELL(iXCoord, iYCoord)
		RETURN FALSE
	ENDIF
	
	BOOL bMovingLeft = iXIncrement < 0
	BOOL bMovingRight = iXIncrement > 0
	BOOL bMovingDown = iYIncrement > 0
	BOOL bMovingUp = iYIncrement < 0
	
	// No special exceptions for moving only left or right
	IF NOT bMovingDown AND NOT bMovingUp
		RETURN FALSE
	ENDIF
	
	IF bMovingUp
		ROULETTE_LAYOUT_APPLY_COLUMN_BET_MOVING_UP_NAVIGATION_EXCEPTION(iXCoord, iYCoord, bMovingRight, bMovingLeft)
		RETURN TRUE
	ENDIF	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///   Applies movement exceptions for a position on the last row of inside bets (row before the column bets)
///   returns true if an exception has been applied     
FUNC BOOL ROULETTE_LAYOUT_APPLY_LAST_INSIDE_BET_ROW_NAVIGATION_EXCEPTIONS(INT &iXCoord, INT &iYCoord, INT iXIncrement, INT iYIncrement)
	
	IF NOT ROULETTE_LAYOUT_IS_COORD_IN_LAST_ROW_OF_INSIDE_BETS(iXCoord, iYCoord)
		RETURN FALSE
	ENDIF
	
	BOOL bMovingLeft = iXIncrement < 0
	BOOL bMovingRight = iXIncrement > 0
	BOOL bMovingDown = iYIncrement > 0
	
	// No special exceptions for only moving left or right
	IF NOT bMovingDown
		RETURN FALSE
	ENDIF

	IF bMovingDown
		iYCoord++
		
		// Left diagonal from line centre
		IF bMovingLeft
			IF iXCoord = LAYOUT_INSIDE_BETS_CENTRE_X_COORD - 1
				iXCoord = LAYOUT_MIN_COLUMN_BET_X_COORD
				RETURN TRUE
			ENDIF
			IF iXCoord = LAYOUT_INSIDE_BETS_CENTRE_X_COORD + 1
				iXCoord = LAYOUT_COLUMN_BET_CENTRE_X_COORD
				RETURN TRUE
			ENDIF	
		ENDIF
		
		// Right diagonal from line centre
		IF bMovingRight
			IF iXCoord = LAYOUT_INSIDE_BETS_CENTRE_X_COORD - 1
				RETURN TRUE
			ENDIF
		
			IF iXCoord = LAYOUT_INSIDE_BETS_CENTRE_X_COORD + 1
				iXCoord = LAYOUT_MAX_COLUMN_BET_X_COORD
				RETURN TRUE
			ENDIF
		ENDIF
		
		// Only moving down 
		// Map middle inside bet middle column to column bet middle column
		IF iXCoord = LAYOUT_INSIDE_BETS_CENTRE_X_COORD
			iXCoord = LAYOUT_COLUMN_BET_CENTRE_X_COORD
			RETURN TRUE
		
		// Map last inside bet column to last column bet column
		ELIF iXCoord = LAYOUT_MAX_INSIDE_BET_X_COORD
			iXCoord = LAYOUT_MAX_COLUMN_BET_X_COORD
			RETURN TRUE
		ENDIF
		// First column already maps correclty
		
		// Map first column line to first column bet
		IF iXCoord = LAYOUT_INSIDE_BETS_CENTRE_X_COORD - 1
			iXCoord = LAYOUT_MIN_COLUMN_BET_X_COORD
			RETURN TRUE
		ENDIF
		
		// Map second line to middle column bet
		IF iXCoord = LAYOUT_INSIDE_BETS_CENTRE_X_COORD + 1
			iXCoord = LAYOUT_COLUMN_BET_CENTRE_X_COORD
			RETURN TRUE
		ENDIF
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE	
ENDFUNC

/// PURPOSE:
///    Maps an inside bet row to its equivelent row in the second outside bet column   
FUNC INT ROULETTE_LAYOUT_MAP_INSIDE_BET_Y_COORD_TO_SECOND_OUTSIDE_BET_Y_COORD(INT iXCoord)
	IF iXCoord < 9
		RETURN 0
	ELIF iXCoord < 17
		RETURN 1
	ELSE
		RETURN 2
	ENDIF
	
	RETURN iXCoord
ENDFUNC

/// PURPOSE:
///   Applies movement exceptions for a position on the line between outside and inside bets
///   returns true if an exception has been applied      
FUNC BOOL ROULETTE_LAYOUT_APPLY_OUTSIDE_BET_LINE_NAVIGATION_EXCEPTIONS(INT &iXCoord, INT &iYCoord, INT iXIncrement, INT iYIncrement)
	BOOL bMovingLeft = iXIncrement < 0
	BOOL bMovingRight = iXIncrement > 0
	BOOL bMovingUp = iYIncrement < 0
	BOOL bMovingDown = iYIncrement > 0
	
	IF bMovingLeft	
		// First row clamps normally
		IF iYCoord = LAYOUT_MIN_INSIDE_BET_Y_COORD			
			// False to use normal clamp
			RETURN TRUE
		ENDIF
		
		iXCoord--
		iYCoord = ROULETTE_LAYOUT_MAP_INSIDE_BET_Y_COORD_TO_SECOND_OUTSIDE_BET_Y_COORD(iYCoord)
		RETURN TRUE
	ENDIF
	
	IF (bMovingUp OR bMovingDown) AND bMovingLeft
		// Cannot move diagonal left from this position
		RETURN TRUE
	ENDIF
	
	// Up right diagonal on top row moves cursor to first zero
	IF bMovingUp And bMovingRight AND iYCoord = LAYOUT_MIN_INSIDE_BET_Y_COORD
		iXCoord = LAYOUT_MIN_ZEROES_X_COORD
		iYCoord = 0
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Maps an first outside bet y coord to its inside bet y coord equivelant  
FUNC INT ROULETTE_LAYOUT_MAP_FIRST_OUTSIDE_BET_Y_COORD_TO_INSIDE_BET_Y_COORD(INT iYCoord)
	SWITCH iYCoord
		CASE 0 RETURN 3
		CASE 1 RETURN 7
		CASE 2 RETURN 11	
		CASE 3 RETURN 15	
		CASE 4 RETURN 19	
		CASE 5 RETURN 23	
	ENDSWITCH
	
	RETURN iYCoord
ENDFUNC

/// PURPOSE:
///    Maps an first outside bet row index to its second outside bet row equivelant 
FUNC INT ROULETTE_LAYOUT_MAP_FIRST_OUTSIDE_BET_Y_COORD_TO_SECOND_OUTSIDE_BET_Y_COORD(INT iYCoord)
	SWITCH iYCoord
		CASE 0 
		CASE 1 
			RETURN 0
		CASE 2 	
		CASE 3 
			RETURN 1	
		CASE 4 
		CASE 5 
			RETURN 2
	ENDSWITCH
	
	RETURN iYCoord
ENDFUNC

/// PURPOSE:
///   Applies movement exceptions in the first outside bet column
///   returns true if an exception has been applied         
FUNC BOOL ROULETTE_LAYOUT_APPLY_FIRST_OUTSIDE_BET_NAVIGATION_EXCEPTIONS(INT &iXCoord, INT &iYCoord, INT iXIncrement)	
	BOOL bMovingRight = iXIncrement > 0
	
	IF bMovingRight
		iXCoord++
		iYCoord = ROULETTE_LAYOUT_MAP_FIRST_OUTSIDE_BET_Y_COORD_TO_SECOND_OUTSIDE_BET_Y_COORD(iYCoord)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///   Applies movement exceptions in the second outside bet column
///   returns true if an exception has been applied   
FUNC BOOL ROULETTE_LAYOUT_APPLY_SECOND_OUTSIDE_BET_NAVIGATION_EXCEPTIONS(INT &iXCoord, INT &iYCoord, INT iXIncrement, INT iYIncrement)	
	BOOL bMovingUp = iYIncrement < 0
	BOOL bMovingDown = iYIncrement > 0
	BOOL bMovingLeft = iXIncrement < 0
	BOOL bMovingRight = iXIncrement > 0
	
	IF bMovingUp AND bMovingRight
		iXCoord++
		SWITCH iYCoord
			CASE 0 iYCoord = 4
			BREAK
			CASE 1 iYCoord = 12
			BREAK
			CASE 2 iYCoord = 20
			BREAK
		ENDSWITCH
		
		RETURN TRUE
	ENDIF
	
	IF bMovingRight
		iXCoord++
		SWITCH iYCoord
			CASE 0 iYCoord = 5
			BREAK
			CASE 1 iYCoord = 13
			BREAK
			CASE 2 iYCoord = 21
			BREAK
		ENDSWITCH
		
		RETURN TRUE
	ENDIF
	
	IF bMovingDown AND bMovingLeft
		iXCoord--
		SWITCH iYCoord
			CASE 0 iYCoord = 1
			BREAK
			CASE 1 iYCoord = 3
			BREAK
			CASE 2 iYCoord = 5
			BREAK
		ENDSWITCH
		
		RETURN TRUE
	ENDIF
	
	// Moving up left and moving left give the same row postion
	IF bMovingLeft
		iXCoord--
		SWITCH iYCoord
			CASE 0 iYCoord = 0
			BREAK
			CASE 1 iYCoord = 2
			BREAK
			CASE 2 iYCoord = 4
			BREAK
		ENDSWITCH
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///   Applies movement exceptions, as its a ragged grid navigating from certain positions require
///   different movement behaviours
FUNC BOOL ROULETTE_LAYOUT_APPLY_NAVIGATION_EXCEPTIONS(INT &iXCoord, INT &iYCoord, INT iXIncrement, INT iYIncrement)
	IF ROULETTE_LAYOUT_IS_COORD_ON_A_ZEROES_ROW(iXCoord, iYCoord)
		RETURN ROULETTE_LAYOUT_APPLY_ZEROES_NAVIGATION_EXCEPTION(iXCoord, iYCoord, iXIncrement, iYIncrement)		
	ENDIF
	
	IF ROULETTE_LAYOUT_IS_COORD_ON_ZEROES_LINE(iXCoord, iYCoord)
		RETURN ROULETTE_LAYOUT_APPLY_ZEROES_LINE_NAVIGATION_EXCEPTION(iXCoord, iYCoord, iXIncrement, iYIncrement)
	ENDIF
	
	IF ROULETTE_LAYOUT_IS_COORD_ON_A_COLUMN_BET_CELL(iXCoord, iYCoord)
		RETURN ROULETTE_LAYOUT_APPLY_COLUMN_BET_NAVIGATION_EXCEPTION(iXCoord, iYCoord, iXIncrement, iYIncrement)
	ENDIF
	
	IF ROULETTE_LAYOUT_IS_COORD_IN_LAST_ROW_OF_INSIDE_BETS(iXCoord, iYCoord)
		RETURN ROULETTE_LAYOUT_APPLY_LAST_INSIDE_BET_ROW_NAVIGATION_EXCEPTIONS(iXCoord, iYCoord, iXIncrement, iYIncrement)
	ENDIF
	
	IF ROULETTE_LAYOUT_IS_COORD_ON_OUTSIDE_BET_LINE(iXCoord, iYCoord)
		RETURN ROULETTE_LAYOUT_APPLY_OUTSIDE_BET_LINE_NAVIGATION_EXCEPTIONS(iXCoord, iYCoord, iXIncrement, iYIncrement)
	ENDIF
	
	IF ROULETTE_LAYOUT_IS_COORD_IN_FIRST_OUTSIDE_BET_COLUMN(iXCoord, iYCoord)
		RETURN ROULETTE_LAYOUT_APPLY_FIRST_OUTSIDE_BET_NAVIGATION_EXCEPTIONS(iXCoord, iYCoord, iXIncrement)
	ENDIF
	
	IF ROULETTE_LAYOUT_IS_COORD_IN_SECOND_OUTSIDE_BET_COLUMN(iXCoord, iYCoord)
		RETURN ROULETTE_LAYOUT_APPLY_SECOND_OUTSIDE_BET_NAVIGATION_EXCEPTIONS(iXCoord, iYCoord, iXIncrement, iYIncrement)
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Clamps a layout grid coordinate to the ragged layout grid
PROC ROULETTE_LAYOUT_CLAMP_COORD_TO_LAYOUT_EXTENTS(INT &iXCoord, INT &iYCoord)
	INT iTempY = iYCoord
	INT iTempX = iXCoord
	
	iYCoord = CLAMP_INT(iTempY, ROULETTE_LAYOUT_GET_MIN_Y_FOR_COORD(iTempX, iTempY), ROULETTE_LAYOUT_GET_MAX_Y_FOR_COORD(iTempX, iTempY))
	iXCoord = CLAMP_INT(iTempX, ROULETTE_LAYOUT_GET_MIN_X_FOR_COORD(iTempX, iTempY), ROULETTE_LAYOUT_GET_MAX_X_FOR_COORD(iTempX, iTempY))
ENDPROC
     
/// PURPOSE:
///    Increments the layout coord by a given offset, adhering to the extents of the layout board
PROC ROULETTE_LAYOUT_MOVE_COORD(INT &iXCoord, INT &iYCoord, INT iXIncrement, INT iYIncrement, BOOL bAllowDiagonal = TRUE)			
	// A number of edge cases require movement restrictions and specific wrapping behaviour
	IF ROULETTE_LAYOUT_APPLY_NAVIGATION_EXCEPTIONS(iXCoord, iYCoord, iXIncrement, iYIncrement)
		EXIT
	ENDIF	
	
	IF NOT bAllowDiagonal
		// Diagonal movement not allowed
		IF iXIncrement > 0 OR iXIncrement < 0
			iYIncrement = 0
		ENDIF
	ENDIF
	
	// No exceptions applied so move normally
	iXCoord += iXIncrement
	iYCoord += iYIncrement
	
	ROULETTE_LAYOUT_CLAMP_COORD_TO_LAYOUT_EXTENTS(iXCoord, iYCoord)
ENDPROC
