//////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Name:        RouletteUIOverrides.sch																			
/// Description: Header containing all overrideable functions and parameters relating to the roulette UI
///																											
/// Written by:  Online Technical Team: Tom Turner,															
/// Date:  		15/03/2019																					
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "freemode_header.sch"

#IF NOT DEFINED(ROULETTE_CURSOR_DELAY_TIME)
	/// PURPOSE: Delay between cursor snapping when held
	CONST_INT ROULETTE_CURSOR_DELAY_TIME 50
#ENDIF

#IF NOT DEFINED(ROULETTE_UI_CHIP_SWAP_DELAY_TIME)
	/// PURPOSE: Delay between chip swapping when held
	CONST_INT ROULETTE_UI_CHIP_SWAP_DELAY_TIME 100
#ENDIF

#IF NOT DEFINED(ROULETTE_CURSOR_MOVE_SPEED)
	/// PURPOSE: Speed at which the cursor moves
	CONST_FLOAT ROULETTE_CURSOR_MOVE_SPEED 0.6
#ENDIF

/// PURPOSE: Top right coord of the cursor boundary box
#IF NOT DEFINED(ROULETTE_UI_CURSOR_BOUND_TR_X)TWEAK_FLOAT ROULETTE_UI_CURSOR_BOUND_TR_X 1.09 #ENDIF
#IF NOT DEFINED(ROULETTE_UI_CURSOR_BOUND_TR_Y)TWEAK_FLOAT ROULETTE_UI_CURSOR_BOUND_TR_Y 0.670 #ENDIF

/// PURPOSE: Bottom left coord of the cursor boundary box
#IF NOT DEFINED(ROULETTE_UI_CURSOR_BOUND_BL_X)TWEAK_FLOAT ROULETTE_UI_CURSOR_BOUND_BL_X -0.140 #ENDIF
#IF NOT DEFINED(ROULETTE_UI_CURSOR_BOUND_BL_Y)TWEAK_FLOAT ROULETTE_UI_CURSOR_BOUND_BL_Y -0.030 #ENDIF

#IF NOT DEFINED(ROULETTE_DISPLAY_ANY_UNAVAILABLE_MESSAGE)
	/// PURPOSE:
	///    Default function for displaying unavailble message, when it returns
	///    true playing roulette will be blocked
	FUNC BOOL ROULETTE_DISPLAY_ANY_UNAVAILABLE_MESSAGE(INT iSeatID, INT iTableID, INT iMinChipsRequired, INT &iUnavailableReason, BOOL bAlreadyPlaying)
		UNUSED_PARAMETER(iSeatID)
		UNUSED_PARAMETER(iTableID)
		UNUSED_PARAMETER(iMinChipsRequired)
		UNUSED_PARAMETER(iUnavailableReason)
		RETURN FALSE
	ENDFUNC
#ENDIF

#IF NOT DEFINED(ROULETTE_UI_GET_START_PROMPT_TEXT_LABEL_STRING)
/// PURPOSE:
///    Gets the text label string for the start roulette prompt  
FUNC STRING ROULETTE_UI_GET_START_PROMPT_TEXT_LABEL_STRING(INT iTableID)
	UNUSED_PARAMETER(iTableID)
	RETURN "ROUL_PROMPT"
ENDFUNC
#ENDIF

#IF NOT DEFINED(ROULETTE_UI_GET_MENU_BANNER_TEXTURE_NAME)
/// PURPOSE:
///    Gets the banner texture name for the disclosure screen,
///    bHighStakesBanner will determine if the highstakes variant is returned
FUNC STRING ROULETTE_UI_GET_MENU_BANNER_TEXTURE_NAME(BOOL bHighStakesBanner)
	IF bHighStakesBanner
		RETURN "CasinoUI_Roulette_High"
	ENDIF
	
	RETURN "CasinoUI_Roulette"
ENDFUNC
#ENDIF

