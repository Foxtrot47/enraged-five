//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        RouletteGameClient.sch																		
// Description: Header for running the server side roulette game logic, this is started and stopped 		
//				by the main Roulette server when a player is using a table. Note this does not init or		
//				cleanup assets, it only uses the ones setup by the main script to run the game logic		
//				as the players can join and leave games freely												
//																											
// Written by:  Online Technical Team: Tom Turner,															
// Date:  		21/02/2019																					
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "RouletteData.sch"
USING "RouletteGameState.sch"
USING "RouletteConstants.sch"
USING "RouletteHelper.sch"
USING "RouletteAnimation.sch"
USING "RouletteAudio.sch"

/// PURPOSE: Safety mechanism for transitioning states if somehow a state hangs
///    	after this duration the state will transition out to avoid players getting stuck indefinitely
CONST_INT ROULETTE_GAME_SERVER_STATE_MAX_STATE_DURATION_BEFORE_AUTO_TRANSITION 30000

/// PURPOSE:
///    Generates the winning roulette number for the table ID
PROC ROULETTE_GAME_SERVER_GENERATE_WINNING_NUMBER_FOR_TABLE(ROULETTE_DATA &sRouletteData, INT iTableID)
	sRouletteData.serverBD.iSpinResult[iTableID] = ROULETTE_GENERATE_WINNING_NUMBER()
ENDPROC

/// PURPOSE:
///    Generates a spin duration for the wheel (how many times the loop animation will play before ending)
PROC ROULETTE_GAME_SERVER_GENERATE_RANDOM_WHEEL_SPIN_DURATION_FOR_TABLE(ROULETTE_DATA &sRouletteData, INT iTableID)
	sRouletteData.serverBD.iSpinMaxLoops[iTableID] = GET_RANDOM_INT_IN_RANGE(ROULETTE_ANIM_MIN_WHEEL_LOOPS, ROULETTE_ANIM_MAX_WHEEL_LOOPS)
ENDPROC

/// PURPOSE:
///    Checks if the table has requested a spin on the client   
FUNC BOOL ROULETTE_GAME_SERVER_HAS_SPIN_AT_TABLE_BEEN_REQUESTED(ROULETTE_DATA &sRouletteData, INT iTableID)
	RETURN IS_BIT_SET(sRouletteData.serverBD.iTableBS[iTableID], ROULETTE_SERVER_TABLE_BS_REQUIRES_WHEEL_SPIN)
ENDFUNC

/// PURPOSE:
///    Clears the spin request for the table
PROC ROULETTE_GAME_SERVER_CLEAR_WHEEL_SPIN_REQUEST_FOR_TABLE(ROULETTE_DATA &sRouletteData, INT iTableID)
	CLEAR_BIT(sRouletteData.serverBD.iTableBS[iTableID], ROULETTE_SERVER_TABLE_BS_REQUIRES_WHEEL_SPIN)
ENDPROC

/// PURPOSE:
///    Updates the tables betting round duration so that it
///    scales according to how many players are at the table
PROC ROULETTE_GAME_SERVER_UPDATE_BETTING_ROUND_DURATION(ROULETTE_DATA &sRouletteData, INT iTableID)
	INT iCurrentNumPlayersOnTable = sRouletteData.serverBD.iTablePlayerCount[iTableID]
	sRouletteData.serverBD.iCurrentBetRoundDuration[iTableID] = ROULETTE_GET_BETTING_ROUND_DURATION_MS(iCurrentNumPlayersOnTable)
ENDPROC

/// PURPOSE:
///    Initialization state for the game server
PROC ROULETTE_GAME_SERVER_INIT_UPDATE(ROULETTE_DATA &sRouletteData, INT iTableID)	
	ROULETTE_GAME_SERVER_UPDATE_BETTING_ROUND_DURATION(sRouletteData, iTableID)
	RESET_NET_TIMER(sRouletteData.serverBD.stBettingTimer[iTableID])
	START_NET_TIMER(sRouletteData.serverBD.stBettingTimer[iTableID])
	
	sRouletteData.serverBD.eGameState[iTableID] = ROULETTE_GAME_STATE_BETTING
	sRouletteData.serverBD.eSpinState[iTableID] = RGS_SPIN_WAITING_FOR_START_TIME
	
	CLEAR_BIT(sRouletteData.serverBD.iTableBS[iTableID], ROULETTE_SERVER_TABLE_BS_CALLED_FOR_BETS)
	CLEAR_BIT(sRouletteData.serverBD.iTableBS[iTableID], ROULETTE_SERVER_TABLE_BS_CALLED_NO_MORE_BETS)
	ROULETTE_GAME_SERVER_CLEAR_WHEEL_SPIN_REQUEST_FOR_TABLE(sRouletteData, iTableID)
	ROULETTE_HELPER_SET_HAS_DEALER_FINISHED_SPIN_SEQUENCE_AT_TABLE(sRouletteData, iTableID, FALSE)
	sRouletteData.serverBD.iRoundID[iTableID]++
	
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_GAME_SERVER_INIT_UPDATE - on exit")
ENDPROC

/// PURPOSE:
///    Checks if the table is ready to recive a spin for all script participants  
FUNC BOOL ROULETTE_GAME_SERVER_IS_TABLE_READY_FOR_SPIN_ON_ALL_CLIENTS(ROULETTE_DATA &sRouletteData, INT iTableID)
	INT iParticipant
	// Check all participant player states
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
	    IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
	      PLAYER_INDEX PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))
	     	
		  // Participants table is still spinning
	      IF IS_BIT_SET(sRouletteData.playerBD[NATIVE_TO_INT(PlayerId)].iClientTableBS[iTableID], ROULETTE_CLIENT_TABLE_BS_SPIN_IN_PROGRESS)
	         RETURN FALSE
	      ENDIF 
	    ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Handles calling place your bets if it hasn't already been called
PROC ROULETTE_GAME_SERVER_MAINTAIN_CALLING_PLACE_YOUR_BETS(ROULETTE_DATA &sRouletteData, INT iTableID)
	
	// Don't play if no one is playing
	IF sRouletteData.serverBD.iTablePlayerCount[iTableID] <= 0
		EXIT
	ENDIF	
	
	// Already played so leave
	IF IS_BIT_SET(sRouletteData.serverBD.iTableBS[iTableID], ROULETTE_SERVER_TABLE_BS_CALLED_FOR_BETS)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_GAME_SERVER_MAINTAIN_CALLING_PLACE_YOUR_BETS - already played")
		EXIT
	ENDIF
	
	// Don't play if another clip is playing
	IF ROULETTE_AUDIO_IS_DEALER_PLAYING_AUDIO(sRouletteData, iTableID)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_GAME_SERVER_MAINTAIN_CALLING_PLACE_YOUR_BETS - dealer playing audio")
		EXIT
	ENDIF
	
	// Wait for correct tme to call place your bets
	IF NOT HAS_NET_TIMER_EXPIRED(sRouletteData.serverBD.stBettingTimer[iTableID], ROULETTE_AUDIO_DELAY_BEFORE_CALLING_PLACE_YOUR_BETS_MS)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_GAME_SERVER_MAINTAIN_CALLING_PLACE_YOUR_BETS - time not expired")
		EXIT
	ENDIF
	
	// Try play place your bets audio
	IF NOT ROULETTE_AUDIO_PLAY_DEALER_PLACE_YOUR_BETS_CLIP(sRouletteData, iTableID)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_GAME_SERVER_MAINTAIN_CALLING_PLACE_YOUR_BETS - tried playing but failed")
		EXIT
	ENDIF
	
	// Successfully played
	SET_BIT(sRouletteData.serverBD.iTableBS[iTableID], ROULETTE_SERVER_TABLE_BS_CALLED_FOR_BETS)
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_GAME_SERVER_MAINTAIN_CALLING_PLACE_YOUR_BETS - played successfully")
ENDPROC

/// PURPOSE:
///    Server betting substate for waiting until the spin should start
PROC ROULETTE_GAME_SERVER_BETTING_UPDATE_WAIT_FOR_START_TIME(ROULETTE_DATA &sRouletteData, INT iTableID)
	INT iBettingRoundDuration = ROULETTE_HELPER_GET_TABLES_BETTING_ROUND_DURATION(sRouletteData, iTableID)	
	
	ROULETTE_GAME_SERVER_MAINTAIN_CALLING_PLACE_YOUR_BETS(sRouletteData, iTableID)	
	
	// When 10 seconds left or all players are ready start the wheel spin animation
	IF HAS_NET_TIMER_EXPIRED(sRouletteData.serverBD.stBettingTimer[iTableID],
	iBettingRoundDuration - ROULETTE_BETTING_SPIN_START_TIME_BEFORE_BETTING_ENDS_MS)
		ROULETTE_GAME_SERVER_GENERATE_RANDOM_WHEEL_SPIN_DURATION_FOR_TABLE(sRouletteData, iTableID)
		SET_BIT(sRouletteData.serverBD.iTableBS[iTableID], ROULETTE_SERVER_TABLE_BS_REQUIRES_WHEEL_SPIN)
		CLEAR_BIT(sRouletteData.serverBD.iTableBS[iTableID], ROULETTE_SERVER_TABLE_BS_CALLED_FOR_BETS)
		sRouletteData.serverBD.eSpinState[iTableID] = RGS_SPIN_WAITING_FOR_TABLES_TO_BE_READY
		EXIT
	ENDIF
	
	ROULETTE_ANIM_MAINTAIN_DEALER_IDLE(sRouletteData, iTableID, FALSE)
ENDPROC

/// PURPOSE:
///    Server betting substate for waiting for all players table to be ready to spin
PROC ROULETTE_GAME_SERVER_BETTING_UPDATE_WAIT_FOR_TABLES_TO_BE_READY(ROULETTE_DATA &sRouletteData, INT iTableID)
	INT iBettingRoundDuration = ROULETTE_HELPER_GET_TABLES_BETTING_ROUND_DURATION(sRouletteData, iTableID)	
	
	/// Wait for table to be ready on all clients
	/// if the table gets stuck after betting time expires proceed anyway
	IF ROULETTE_GAME_SERVER_IS_TABLE_READY_FOR_SPIN_ON_ALL_CLIENTS(sRouletteData, iTableID) 
	OR HAS_NET_TIMER_EXPIRED(sRouletteData.serverBD.stBettingTimer[iTableID], iBettingRoundDuration + 10000)	
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_GAME_SERVER_BETTING_UPDATE - ",
		"iTableID: ", iTableID,
		" - Transition to dealer spin sequence")
		ROULETTE_HELPER_SET_HAS_DEALER_FINISHED_SPIN_SEQUENCE_AT_TABLE(sRouletteData, iTableID, FALSE)
		sRouletteData.serverBD.eSpinState[iTableID] = RGS_SPIN_DEALER_SPIN_SEQUENCE
		EXIT
	ENDIF
	
	ROULETTE_ANIM_MAINTAIN_DEALER_IDLE(sRouletteData, iTableID, FALSE)
ENDPROC

/// PURPOSE:
///     Server betting substate for updating the dealers spin sequence
PROC ROULETTE_GAME_SERVER_BETTING_UPDATE_SPIN_SEQUENCE(ROULETTE_DATA &sRouletteData, INT iTableID)
	IF ROULETTE_ANIM_UPDATE_WHEEL_SPIN_ANIM_SEQUENCE_ON_TABLES_DEALER(sRouletteData, iTableID)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_GAME_SERVER_BETTING_UPDATE - ",
		"iTableID: ", iTableID,
		" - Transition to wait for round end")
		ROULETTE_HELPER_SET_HAS_DEALER_FINISHED_SPIN_SEQUENCE_AT_TABLE(sRouletteData, iTableID, TRUE)
		sRouletteData.serverBD.eSpinState[iTableID] = RGS_SPIN_WAIT_FOR_BETTING_END
	ENDIF
ENDPROC

/// PURPOSE:
///    Server betting substate for waiting for betting round to end
PROC ROULETTE_GAME_SERVER_BETTING_UPDATE_WAIT_FOR_BET_END(ROULETTE_DATA &sRouletteData, INT iTableID)
	ROULETTE_ANIM_MAINTAIN_DEALER_IDLE(sRouletteData, iTableID)
	INT iBettingRoundDuration = ROULETTE_HELPER_GET_TABLES_BETTING_ROUND_DURATION(sRouletteData, iTableID)	
	
	// Take control now so we can play no bets audio
	TAKE_CONTROL_OF_NET_ID(sRouletteData.serverBD.niDealerPed[iTableID])
	
	// If full betting time isn't up leave
	IF NOT HAS_NET_TIMER_EXPIRED(sRouletteData.serverBD.stBettingTimer[iTableID], iBettingRoundDuration)	
		EXIT
	ENDIF
	
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_GAME_SERVER_BETTING_UPDATE - ",
	"iTableID: ", iTableID,
	" - Betting time expired")
	
	// Transition to wheel spin state when time is up
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_GAME_SERVER_BETTING_UPDATE - ",
	"iTableID: ", iTableID,
	" - Transition to wait for no more bets")
	
	ROULETTE_GAME_SERVER_GENERATE_WINNING_NUMBER_FOR_TABLE(sRouletteData, iTableID)
	
	// Only announce no more bets if there are people playing
	IF sRouletteData.serverBD.iTablePlayerCount[iTableID] > 0
		ROULETTE_ANIM_PLAY_DEALER_NO_MORE_BETS_ANIM_ON_TABLES_DEALER(sRouletteData, iTableID)
	ENDIF
	
	CLEAR_BIT(sRouletteData.serverBD.iTableBS[iTableID], ROULETTE_SERVER_TABLE_BS_CALLED_NO_MORE_BETS)
	sRouletteData.serverBD.eSpinState[iTableID] = RGS_SPIN_WAIT_FOR_NO_MORE_BETS_TO_FINISH
ENDPROC

/// PURPOSE:
///    Handles calling no more bets bets if it hasn't already been called
PROC ROULETTE_GAME_SERVER_MAINTAIN_CALLING_NO_MORE_BETS(ROULETTE_DATA &sRouletteData, INT iTableID)
	
	// Don't play if no one is playing
	IF sRouletteData.serverBD.iTablePlayerCount[iTableID] <= 0
		EXIT
	ENDIF	
	
	// Already played so leave
	IF IS_BIT_SET(sRouletteData.serverBD.iTableBS[iTableID], ROULETTE_SERVER_TABLE_BS_CALLED_NO_MORE_BETS)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_GAME_SERVER_MAINTAIN_CALLING_NO_MORE_BETS - already played")
		EXIT
	ENDIF
	
	// Don't play if another clip is playing
	IF ROULETTE_AUDIO_IS_DEALER_PLAYING_AUDIO(sRouletteData, iTableID)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_GAME_SERVER_MAINTAIN_CALLING_NO_MORE_BETS - dealer playing audio")
		EXIT
	ENDIF
	
	// Try play place your bets audio
	IF NOT ROULETTE_AUDIO_PLAY_DEALER_NO_MORE_BETS_CLIP(sRouletteData, iTableID)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_GAME_SERVER_MAINTAIN_CALLING_NO_MORE_BETS - tried playing but failed")
		EXIT
	ENDIF
	
	// Successfully played
	SET_BIT(sRouletteData.serverBD.iTableBS[iTableID], ROULETTE_SERVER_TABLE_BS_CALLED_NO_MORE_BETS)
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_GAME_SERVER_MAINTAIN_CALLING_NO_MORE_BETS - played successfully")
ENDPROC

/// PURPOSE:
///    Server betting substate for waiting for no more bets anim to end
PROC ROULETTE_GAME_SERVER_BETTING_WAIT_FOR_NO_MORE_BETS_TO_FINISH(ROULETTE_DATA &sRouletteData, INT iTableID)
	INT iBettingRoundDuration = ROULETTE_HELPER_GET_TABLES_BETTING_ROUND_DURATION(sRouletteData, iTableID)	
	
	ROULETTE_GAME_SERVER_MAINTAIN_CALLING_NO_MORE_BETS(sRouletteData, iTableID)
	
	/// If we havn't finished the no more bets anim and havn't exceeded the safety timer, leave
	IF NOT ROULETTE_ANIM_HAS_TABLES_DEALER_FINISHED_PLAYING_CURRENT_ANIM(sRouletteData, iTableID)
	AND NOT ROULETTE_ANIM_HAS_DEALER_EVENT_TRIGGERED(sRouletteData, iTableID,  GET_HASH_KEY("READY_UP_START"))
	AND NOT HAS_NET_TIMER_EXPIRED(sRouletteData.serverBD.stBettingTimer[iTableID], iBettingRoundDuration + 10000)
		EXIT
	ENDIF
	
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_GAME_SERVER_BETTING_UPDATE - ",
	"iTableID: ", iTableID,
	" - Transitioning out of betting state")
	
	CLEAR_BIT(sRouletteData.serverBD.iTableBS[iTableID], ROULETTE_SERVER_TABLE_BS_CALLED_NO_MORE_BETS)
	sRouletteData.serverBD.eGameState[iTableID] = ROULETTE_GAME_STATE_SPIN_WHEEL	
	sRouletteData.serverBD.eSpinState[iTableID] = RGS_SPIN_WAITING_FOR_START_TIME
ENDPROC

/// PURPOSE:
///    Update state for the betting state of the game server
///    waits for the duration of the betting time before transitioning to wheel spining
PROC ROULETTE_GAME_SERVER_BETTING_UPDATE(ROULETTE_DATA &sRouletteData, INT iTableID)
	ROULETTE_GAME_SERVER_SPIN_STATE eSpinState = sRouletteData.serverBD.eSpinState[iTableID]
	
	SWITCH eSpinState
		CASE RGS_SPIN_WAITING_FOR_START_TIME
			ROULETTE_GAME_SERVER_BETTING_UPDATE_WAIT_FOR_START_TIME(sRouletteData, iTableID)
		BREAK
		CASE RGS_SPIN_WAITING_FOR_TABLES_TO_BE_READY
			ROULETTE_GAME_SERVER_BETTING_UPDATE_WAIT_FOR_TABLES_TO_BE_READY(sRouletteData, iTableID)
		BREAK
		CASE RGS_SPIN_DEALER_SPIN_SEQUENCE
			ROULETTE_GAME_SERVER_BETTING_UPDATE_SPIN_SEQUENCE(sRouletteData, iTableID)
		BREAK
		CASE RGS_SPIN_WAIT_FOR_BETTING_END
			ROULETTE_GAME_SERVER_BETTING_UPDATE_WAIT_FOR_BET_END(sRouletteData, iTableID)
		BREAK
		CASE RGS_SPIN_WAIT_FOR_NO_MORE_BETS_TO_FINISH
			ROULETTE_GAME_SERVER_BETTING_WAIT_FOR_NO_MORE_BETS_TO_FINISH(sRouletteData, iTableID)
		BREAK
	ENDSWITCH	
ENDPROC

/// PURPOSE:
///		Checks if all the players on the table are on the given game state
FUNC BOOL ROULETTE_GAME_SERVER_ARE_ALL_PLAYERS_ON_GAME_STATE(ROULETTE_DATA &sRouletteData, INT iTableID, ROULETTE_GAME_STATE eStateToCheck)
	INT iLocalSeatID
	REPEAT ROULETTE_TABLE_MAX_NUMBER_OF_PLAYERS_PER_TABLE iLocalSeatID
		INT iSeatID = ROULETTE_HELPER_GET_SEAT_ID_FROM_LOCAL_SEAT_ID_AT_TABLE(iLocalSeatID, iTableID)			
		PLAYER_INDEX piPlayerID = sRouletteData.serverBD.playersUsingSeats[iSeatID]
		
		IF piPlayerID != INVALID_PLAYER_INDEX()
			IF ROULETTE_HELPER_IS_PLAYER_AT_SEAT_STILL_PLAYING(sRouletteData, iSeatID)
				INT iPlayerID = NATIVE_TO_INT(piPlayerID)
		
				IF sRouletteData.playerBD[iPlayerID].eGameState != eStateToCheck
					RETURN FALSE
				ENDIF	
			ENDIF
		ENDIF	
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Checks if all players have finished with their roulette spin   
FUNC BOOL ROULETTE_GAME_SERVER_ARE_ALL_PLAYERS_FINISHED_WITH_SPIN(ROULETTE_DATA &sRouletteData, INT iTableID)
	INT iLocalSeatID
	REPEAT ROULETTE_TABLE_MAX_NUMBER_OF_PLAYERS_PER_TABLE iLocalSeatID
		INT iSeatID = ROULETTE_HELPER_GET_SEAT_ID_FROM_LOCAL_SEAT_ID_AT_TABLE(iLocalSeatID, iTableID)			
		PLAYER_INDEX piPlayerID = sRouletteData.serverBD.playersUsingSeats[iSeatID]
		
		IF piPlayerID != INVALID_PLAYER_INDEX()
			IF ROULETTE_HELPER_IS_PLAYER_AT_SEAT_STILL_PLAYING(sRouletteData, iSeatID)
				INT iPlayerID = NATIVE_TO_INT(piPlayerID)
					
				IF sRouletteData.playerBD[iPlayerID].eGameState != ROULETTE_GAME_STATE_ROUND_RESOLUTION 
				AND sRouletteData.playerBD[iPlayerID].eGameState != ROULETTE_GAME_STATE_WAIT_FOR_NEXT_ROUND
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF	
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Update state for the wheel spinning state of the game server
///    Waits for all players to finish thier wheel spins and bet resolution before
///    transitioning to a new betting round
PROC ROULETTE_GAME_SERVER_SPIN_WHEEL_UPDATE(ROULETTE_DATA &sRouletteData, INT iTableID)	
	ROULETTE_ANIM_MAINTAIN_DEALER_IDLE(sRouletteData, iTableID, FALSE)
	
	IF ROULETTE_GAME_SERVER_ARE_ALL_PLAYERS_FINISHED_WITH_SPIN(sRouletteData, iTableID)
		sRouletteData.serverBD.eGameState[iTableID] = ROULETTE_GAME_STATE_ROUND_RESOLUTION
		RESET_NET_TIMER(sRouletteData.serverBD.stBettingTimer[iTableID])
		START_NET_TIMER(sRouletteData.serverBD.stBettingTimer[iTableID])
		ROULETTE_ANIM_DETERMINE_WHICH_ZONES_NEED_CLEARING(sRouletteData, iTableID)
		ROULETTE_GAME_SERVER_CLEAR_WHEEL_SPIN_REQUEST_FOR_TABLE(sRouletteData, iTableID)
		ROULETTE_HELPER_SET_HAS_DEALER_FINISHED_SPIN_SEQUENCE_AT_TABLE(sRouletteData, iTableID, FALSE)
	ENDIF	
ENDPROC

/// PURPOSE:
///    Checks if all players at the table are ready for the next betting round 
FUNC BOOL ROULETTE_GAME_SERVER_ARE_PLAYERS_READY_FOR_NEXT_BETTING_ROUND(ROULETTE_DATA &sRouletteData, INT iTableID)
	IF HAS_NET_TIMER_EXPIRED(sRouletteData.serverBD.stBettingTimer[iTableID], ROULETTE_GAME_SERVER_STATE_MAX_STATE_DURATION_BEFORE_AUTO_TRANSITION)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_GAME_SERVER_ARE_PLAYERS_READY_FOR_NEXT_BETTING_ROUND - Max roulette state duration reached!")
		RETURN TRUE
	ENDIF
	
	RETURN ROULETTE_GAME_SERVER_ARE_ALL_PLAYERS_ON_GAME_STATE(sRouletteData, iTableID, ROULETTE_GAME_STATE_WAIT_FOR_NEXT_ROUND) 
ENDFUNC

/// PURPOSE:
///    Update state for the end of round resolution state, handles playing dealer board clear sequence
///    and waiting for players to be ready for next round
PROC ROULETTE_GAME_SERVER_ROUND_RESOLUTION_UPDATE(ROULETTE_DATA &sRouletteData, INT iTableID)			
	// When players are ready and dealer has finished clearing table anim
	IF ROULETTE_ANIM_UPDATE_CHIP_CLEAR_ANIM_SEQUENCE_ON_TABLES_DEALER(sRouletteData, iTableID) 
		sRouletteData.serverBD.eGameState[iTableID] = ROULETTE_GAME_STATE_WAIT_FOR_NEXT_ROUND
	ENDIF
ENDPROC

/// PURPOSE:
///    Update state for the wait for next round state, 
///    waits until all players a ready for another round of betting
PROC ROULETTE_GAME_SERVER_WAIT_FOR_NEXT_ROUND_UPDATE(ROULETTE_DATA &sRouletteData, INT iTableID)	
	ROULETTE_ANIM_MAINTAIN_DEALER_IDLE(sRouletteData, iTableID)
	
	IF ROULETTE_GAME_SERVER_ARE_PLAYERS_READY_FOR_NEXT_BETTING_ROUND(sRouletteData, iTableID)
		
		IF sRouletteData.serverBD.iTablePlayerCount[iTableID] > 0
			IF NOT ROULETTE_AUDIO_PLAY_DEALER_ANOTHER_ROUND_CLIP(sRouletteData, iTableID)
				EXIT
			ENDIF
		ENDIF
		
		// Make dealer idle
		ROULETTE_ANIM_MAINTAIN_DEALER_IDLE(sRouletteData, iTableID)

		// Reset timer
		ROULETTE_GAME_SERVER_UPDATE_BETTING_ROUND_DURATION(sRouletteData, iTableID)
		RESET_NET_TIMER(sRouletteData.serverBD.stBettingTimer[iTableID])
		START_NET_TIMER(sRouletteData.serverBD.stBettingTimer[iTableID])
		
		ROULETTE_GAME_SERVER_CLEAR_WHEEL_SPIN_REQUEST_FOR_TABLE(sRouletteData, iTableID)
		sRouletteData.serverBD.eGameState[iTableID] = ROULETTE_GAME_STATE_BETTING
		sRouletteData.serverBD.iRoundID[iTableID]++
	ENDIF		
ENDPROC

/// PURPOSE:
///    Checks if at least one player is playing on the table and updates count of players playing
///    at table
FUNC BOOL ROULETTE_GAME_SERVER_IS_ANYONE_PLAYING(ROULETTE_DATA &sRouletteData, INT iTableID)
	sRouletteData.serverBD.iTablePlayerCount[iTableID] = 0
	INT iLocalSeatID
	REPEAT ROULETTE_TABLE_MAX_NUMBER_OF_PLAYERS_PER_TABLE iLocalSeatID
		INT iSeatID = ROULETTE_HELPER_GET_SEAT_ID_FROM_LOCAL_SEAT_ID_AT_TABLE(iLocalSeatID, iTableID)
		
		IF sRouletteData.serverBD.playersUsingSeats[iSeatID] != INVALID_PLAYER_INDEX()
			sRouletteData.serverBD.iTablePlayerCount[iTableID]++
		ENDIF
	ENDREPEAT
	
	RETURN sRouletteData.serverBD.iTablePlayerCount[iTableID] > 0
ENDFUNC

/// PURPOSE:
///    Checks if there are players playing on this table and transitions
///    server to clean up stage if there are no players
PROC ROULETTE_GAME_SERVER_CHECK_FOR_EARLY_CLEANUP(ROULETTE_DATA &sRouletteData, INT iTableID)
	IF NOT ROULETTE_GAME_SERVER_IS_ANYONE_PLAYING(sRouletteData, iTableID)
	AND ROULETTE_GAME_SERVER_IS_TABLE_READY_FOR_SPIN_ON_ALL_CLIENTS(sRouletteData, iTableID)
		sRouletteData.serverBD.eGameState[iTableID] = ROULETTE_GAME_STATE_CLEAN_UP
	ENDIF	
ENDPROC

/// PURPOSE:
///    Cleans up the roulette game server 
PROC ROULETTE_GAME_SERVER_CLEAN_UP(ROULETTE_DATA &sRouletteData, INT iTableID)
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_GAME_SERVER_CLEAN_UP - called")
	ROULETTE_GAME_SERVER_CLEAR_WHEEL_SPIN_REQUEST_FOR_TABLE(sRouletteData, iTableID)	
	sRouletteData.serverBD.eGameState[iTableID] = ROULETTE_GAME_STATE_INIT	
	RESET_NET_TIMER(sRouletteData.serverBD.stBettingTimer[iTableID])
ENDPROC

/// PURPOSE:
///    Main entry point for updating the roulette game server logic
FUNC BOOL ROULLETTE_GAME_SERVER_UPDATE(ROULETTE_DATA &sRouletteData, INT iTableID)
	
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
		CPRINTLN(DEBUG_ROULETTE, "ROULLETTE_GAME_SERVER_UPDATE - Is not network host, bailing")
		RETURN FALSE
	ENDIF
	
	ROULETTE_GAME_SERVER_CHECK_FOR_EARLY_CLEANUP(sRouletteData, iTableID)
	
	SWITCH sRouletteData.serverBD.eGameState[iTableID]	
		CASE ROULETTE_GAME_STATE_INIT
			ROULETTE_GAME_SERVER_INIT_UPDATE(sRouletteData, iTableID)
		BREAK
		CASE ROULETTE_GAME_STATE_BETTING
			ROULETTE_GAME_SERVER_BETTING_UPDATE(sRouletteData, iTableID)
		BREAK
		CASE ROULETTE_GAME_STATE_SPIN_WHEEL
			ROULETTE_GAME_SERVER_SPIN_WHEEL_UPDATE(sRouletteData, iTableID)
		BREAK
		CASE ROULETTE_GAME_STATE_ROUND_RESOLUTION
			ROULETTE_GAME_SERVER_ROUND_RESOLUTION_UPDATE(sRouletteData, iTableId)
		BREAK
		CASE ROULETTE_GAME_STATE_WAIT_FOR_NEXT_ROUND
			ROULETTE_GAME_SERVER_WAIT_FOR_NEXT_ROUND_UPDATE(sRouletteData, iTableId)
		BREAK
		CASE ROULETTE_GAME_STATE_CLEAN_UP
			ROULETTE_GAME_SERVER_CLEAN_UP(sRouletteData, iTableID)
			// Participation in game has finished
		RETURN FALSE
	ENDSWITCH
	
	// Still participating in game
	RETURN TRUE
ENDFUNC
