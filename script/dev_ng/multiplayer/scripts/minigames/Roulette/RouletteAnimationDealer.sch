//////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Name:        RouletteAnimationDealer.sch																		
/// Description: Header for controlling animations for the roulette minigames dealer								
///																											
/// Written by:  Online Technical Team: Tom Turner,															
/// Date:  		07/03/2019 (refactored into own file 15/05/2019)																					
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "RouletteData.sch"
USING "RouletteHelper.sch"
USING "rc_helper_functions.sch"
USING "RouletteTable.sch"
USING "RouletteProps.sch"

///--------------------------------
///    Dictionary Loading
///--------------------------------

/// PURPOSE:
///    Loads all the dictionaries containing the roulette dealer animations
PROC ROULETTE_ANIM_LOAD_DEALER_DICTIONARIES()
	REQUEST_ANIM_DICT(ROULETTE_ANIM_GET_DICTIONARY(ROULETTE_ANIM_DICT_DEALER_MALE))
	REQUEST_ANIM_DICT(ROULETTE_ANIM_GET_DICTIONARY(ROULETTE_ANIM_DICT_DEALER_FEMALE))
ENDPROC

/// PURPOSE:
///    Checks if the dictionaries containg the roulette dealer animations have loaded   
FUNC BOOL ROULETTE_ANIM_HAVE_DEALER_DICTIONARIES_LOADED()
	RETURN HAS_ANIM_DICT_LOADED(ROULETTE_ANIM_GET_DICTIONARY(ROULETTE_ANIM_DICT_DEALER_MALE))
	AND	HAS_ANIM_DICT_LOADED(ROULETTE_ANIM_GET_DICTIONARY(ROULETTE_ANIM_DICT_DEALER_FEMALE))
ENDFUNC

/// PURPOSE:
///    Removes the dictionaries contining the roulette dealer animations
PROC ROULETTE_ANIM_REMOVE_DEALER_DICTIONARIES()
	REMOVE_ANIM_DICT(ROULETTE_ANIM_GET_DICTIONARY(ROULETTE_ANIM_DICT_DEALER_MALE))
	REMOVE_ANIM_DICT(ROULETTE_ANIM_GET_DICTIONARY(ROULETTE_ANIM_DICT_DEALER_FEMALE))
ENDPROC


///--------------------------------
///    Anim clip checking
///-------------------------------- 

/// PURPOSE:
///    Stops the sync scene for the dealer at the table
PROC ROULETTE_ANIM_STOP_DEALER_SYNC_SCENE(ROULETTE_DATA &sRouletteData, INT iTableID) 
	IF sRouletteData.serverBD.iDealerSyncSceneID[iTableID] = -1
		EXIT
	ENDIF
	
	NETWORK_STOP_SYNCHRONISED_SCENE(sRouletteData.serverBD.iDealerSyncSceneID[iTableID])
ENDPROC

/// PURPOSE:
///    Gets the correct dealer dictionary taking into account the dealers gender  
FUNC ROULETTE_ANIM_DICTIONARIES ROULETTE_ANIM_GET_DICTIONARY_FOR_TABLE_DEALER(ROULETTE_DATA &sRouletteData, INT iTableID)
	IF ROULETTE_HELPER_IS_DEALER_AT_TABLE_FEMALE(sRouletteData, iTableID)
		RETURN ROULETTE_ANIM_DICT_DEALER_FEMALE
	ENDIF
	
	RETURN ROULETTE_ANIM_DICT_DEALER_MALE
ENDFUNC

/// PURPOSE:
///    Checks if the anim event has fired on the dealer at the given table   
FUNC BOOL ROULETTE_ANIM_HAS_DEALER_EVENT_TRIGGERED(ROULETTE_DATA &sRouletteData, INT iTableID, INT iEventHash, FLOAT fMinPhase = 0.0)
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(sRouletteData.serverBD.niDealerPed[iTableID])
		RETURN FALSE
	ENDIF
	
	PED_INDEX piDealer = NET_TO_PED(sRouletteData.serverBD.niDealerPed[iTableID])	
	IF NOT IS_ENTITY_ALIVE(piDealer)
		RETURN FALSE
	ENDIF

	IF sRouletteData.serverBD.iDealerSyncSceneID[iTableID] = -1
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_HAS_DEALER_EVENT_TRIGGERED - Dealer: ", iTableID, " - Invalid network sync scene ID")
		RETURN FALSE
	ENDIF
	
	INT iDealerLocalSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sRouletteData.serverBD.iDealerSyncSceneID[iTableID])
	
	IF iDealerLocalSceneID = -1
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_HAS_DEALER_EVENT_TRIGGERED - Dealer: ", iTableID, " - Invalid local sync scene ID")
		RETURN FALSE
	ENDIF
	
	FLOAT fCurrentTime = GET_SYNCHRONIZED_SCENE_PHASE(iDealerLocalSceneID)
	RETURN HAS_ANIM_EVENT_FIRED(piDealer, iEventHash) AND fCurrentTime >= fMinPhase
ENDFUNC

/// PURPOSE:
///    Checks if the tables dealer is playing the given roulette animation clip   
FUNC BOOL ROULETTE_ANIM_IS_TABLES_DEALER_PLAYING_CLIP(ROULETTE_DATA &sRouletteData, INT iTableID, ROULETTE_ANIM_CLIPS eDealerClip, INT iVariation = 0)
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(sRouletteData.serverBD.niDealerPed[iTableID])
		RETURN FALSE
	ENDIF
	
	IF sRouletteData.serverBD.iDealerSyncSceneID[iTableID] = -1
		RETURN FALSE
	ENDIF
	
	PED_INDEX piDealer = NET_TO_PED(sRouletteData.serverBD.niDealerPed[iTableID])	
	IF NOT IS_ENTITY_ALIVE(piDealer)
		RETURN FALSE
	ENDIF
	
	ROULETTE_ANIM_DICTIONARIES eDict = ROULETTE_ANIM_GET_DICTIONARY_FOR_TABLE_DEALER(sRouletteData, iTableID)
	STRING strAnimDict = ROULETTE_ANIM_GET_DICTIONARY(eDict)
	STRING strAnimClip = ROULETTE_ANIM_GET_DEALER_CLIP(eDealerClip, eDict, iVariation)
	
	RETURN IS_ENTITY_PLAYING_ANIM(piDealer, strAnimDict, strAnimClip) 
ENDFUNC

/// PURPOSE:
///     Checks if the tables dealer has finished playing the given clip, takes into account the gender of the dealer to determine which dictionary to check with 
FUNC BOOL ROULETTE_ANIM_HAS_TABLES_DEALER_FINISHED_PLAYING_CLIP(ROULETTE_DATA &sRouletteData, INT iTableID, ROULETTE_ANIM_CLIPS eDealerClip, INT iVariation, FLOAT iPhaseEndPoint = 0.9)
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(sRouletteData.serverBD.niDealerPed[iTableID])
		RETURN FALSE
	ENDIF
	
	PED_INDEX piDealer = NET_TO_PED(sRouletteData.serverBD.niDealerPed[iTableID])
	
	IF NOT IS_ENTITY_ALIVE(piDealer)
		RETURN FALSE
	ENDIF
	
	ROULETTE_ANIM_DICTIONARIES eDict = ROULETTE_ANIM_GET_DICTIONARY_FOR_TABLE_DEALER(sRouletteData, iTableID)
	STRING strAnimDict = ROULETTE_ANIM_GET_DICTIONARY(eDict)
	STRING strAnimClip = ROULETTE_ANIM_GET_DEALER_CLIP(eDealerClip, eDict, iVariation)
	
	IF NOT IS_ENTITY_PLAYING_ANIM(piDealer, strAnimDict, strAnimClip) 
		RETURN FALSE
	ENDIF
	
	IF sRouletteData.serverBD.iDealerSyncSceneID[iTableID] = -1
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_HAS_TABLES_DEALER_FINISHED_PLAYING_CLIP - Dealer: ", iTableID, " - Invalid network sync scene ID")
		RETURN FALSE
	ENDIF
	
	INT iDealerLocalSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sRouletteData.serverBD.iDealerSyncSceneID[iTableID])
	FLOAT fCurrentTime = GET_SYNCHRONIZED_SCENE_PHASE(iDealerLocalSceneID)
	
	RETURN fCurrentTime >= iPhaseEndPoint
	OR (HAS_ANIM_EVENT_FIRED(piDealer, GET_HASH_KEY("BLEND_OUT")) AND fCurrentTime > 0.5)
ENDFUNC

/// PURPOSE:
///    Checks if the tables dealer has finished playing the given anim 
FUNC BOOL ROULETTE_ANIM_HAS_TABLES_DEALER_FINISHED_PLAYING_ANIM(ROULETTE_DATA &sRouletteData, INT iTableID, ROULETTE_ANIM& sAnimToCheck, FLOAT iPhaseEndPoint = 0.9)
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(sRouletteData.serverBD.niDealerPed[iTableID])
		RETURN FALSE
	ENDIF
	
	PED_INDEX piDealer = NET_TO_PED(sRouletteData.serverBD.niDealerPed[iTableID])
	
	IF NOT IS_ENTITY_ALIVE(piDealer)
		RETURN FALSE
	ENDIF
	
	STRING strAnimDict = ROULETTE_ANIM_GET_DICTIONARY(sAnimToCheck.eDictionary)
	STRING strAnimClip = ROULETTE_ANIM_GET_DEALER_CLIP(sAnimToCheck.eClip, sAnimToCheck.eDictionary, sAnimToCheck.iVariation)
	
	IF IS_STRING_NULL_OR_EMPTY(strAnimDict)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_HAS_TABLES_DEALER_FINISHED_PLAYING_ANIM - Dealer: ", iTableID,
		" - Dict missing for ", ENUM_TO_INT(sAnimToCheck.eDictionary))
		RETURN TRUE
	ENDIF
	
	IF IS_STRING_NULL_OR_EMPTY(strAnimClip)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_HAS_TABLES_DEALER_FINISHED_PLAYING_ANIM - Dealer: ", iTableID,
		" - Clip missing for ", ENUM_TO_INT(sAnimToCheck.eClip), " var ", sAnimToCheck.iVariation)
		RETURN TRUE
	ENDIF
	
	IF NOT IS_ENTITY_PLAYING_ANIM(piDealer, strAnimDict, strAnimClip)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_HAS_TABLES_DEALER_FINISHED_PLAYING_ANIM - Dealer: ", iTableID,
		" - Not playing ", strAnimClip, " ", strAnimDict)
		RETURN TRUE
	ENDIF
	
	IF HAS_ANIM_EVENT_FIRED(piDealer, GET_HASH_KEY("BLEND_OUT"))
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_HAS_TABLES_DEALER_FINISHED_PLAYING_ANIM - Dealer: ", iTableID,
		" - Blend event triggered on ", strAnimClip, " ", strAnimDict)
		RETURN TRUE
	ENDIF
	
	IF sRouletteData.serverBD.iDealerSyncSceneID[iTableID] = -1
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_HAS_TABLES_DEALER_FINISHED_PLAYING_ANIM - Dealer: ", iTableID,
		" - Invalid network sync scene ID")
		RETURN FALSE
	ENDIF
	
	INT iDealerLocalSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sRouletteData.serverBD.iDealerSyncSceneID[iTableID])
	
	IF iDealerLocalSceneID = -1
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_HAS_TABLES_DEALER_FINISHED_PLAYING_ANIM - Dealer: ",
		iTableID, " - Invalid local sync scene ID")
		RETURN FALSE
	ENDIF
	
	RETURN GET_SYNCHRONIZED_SCENE_PHASE(iDealerLocalSceneID) >= iPhaseEndPoint 
ENDFUNC

/// PURPOSE:
///    Checks if the tables dealer has finished playing its current animation clip
FUNC BOOL ROULETTE_ANIM_HAS_TABLES_DEALER_FINISHED_PLAYING_CURRENT_ANIM(ROULETTE_DATA &sRouletteData, INT iTableID, FLOAT iPhaseEndPoint = 0.9)	
	RETURN ROULETTE_ANIM_HAS_TABLES_DEALER_FINISHED_PLAYING_ANIM(sRouletteData, iTableID,
	sRouletteData.serverBD.sDealerLastAnim[iTableID], iPhaseEndPoint)
ENDFUNC


///------------------------------
///    Anim clip playing helpers
///------------------------------

/// PURPOSE:
///    Plays a roulette animation clip on the dealer for the given table
PROC ROULETTE_ANIM_PLAY_CLIP_ON_TABLES_DEALER(ROULETTE_DATA &sRouletteData, INT iTableID, ROULETTE_ANIM &sAnim)					
	
	IF NOT HAS_ANIM_DICT_LOADED(ROULETTE_ANIM_GET_DICTIONARY(sAnim.eDictionary))
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_PLAY_CLIP_ON_TABLES_DEALER -  Dealer: ", iTableID, " - Dictionary not loaded")
		EXIT
	ENDIF
	
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(sRouletteData.serverBD.niDealerPed[iTableID])
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_PLAY_CLIP_ON_TABLES_DEALER -  Dealer: ", iTableID, " - Dealer net id does not exist")
		EXIT
	ENDIF	
	
	PED_INDEX piDealer = NET_TO_PED(sRouletteData.serverBD.niDealerPed[iTableID])	
	VECTOR vDealerScenePos = ROULETTE_TABLE_GET_POSITION(iTableID)
	FLOAT fDealerSyncSceneHeading = ROULETTE_TABLE_GET_DEALER_HEADING(iTableID)
	
	IF NOT IS_ENTITY_ALIVE(piDealer)
		EXIT
	ENDIF
	
	IF NOT DOES_ENTITY_HAVE_DRAWABLE(piDealer)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_PLAY_CLIP_ON_TABLES_DEALER - Dealer: ", iTableID, " - Dealer does not have drawable")
		EXIT
	ENDIF
	
	STRING strAnimDict = ROULETTE_ANIM_GET_DICTIONARY(sAnim.eDictionary)
	STRING strAnimClip = ROULETTE_ANIM_GET_DEALER_CLIP(sAnim.eClip, sAnim.eDictionary, sAnim.iVariation)
	
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_PLAY_CLIP_ON_TABLES_DEALER - Dealer: ", iTableID, " - Playing ", strAnimClip, ", ", strAnimDict)
	
	sRouletteData.serverBD.iDealerSyncSceneID[iTableID] = NETWORK_CREATE_SYNCHRONISED_SCENE(vDealerScenePos, <<0.000, 0.000, fDealerSyncSceneHeading>>, DEFAULT, sAnim.bHoldLastFrame, sAnim.bLoop)
	
	NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(piDealer, sRouletteData.serverBD.iDealerSyncSceneID[iTableID], strAnimDict, strAnimClip,
	sAnim.fBlendIn, sAnim.fBlendOut, sAnim.eSyncSceneFlags, RBF_PLAYER_IMPACT)
	
	NETWORK_START_SYNCHRONISED_SCENE(sRouletteData.serverBD.iDealerSyncSceneID[iTableID])
	
	// Keep track of last played animation
	sRouletteData.serverBD.sDealerLastAnim[iTableID] = sAnim
ENDPROC


///--------------------------------
///    Anim clip selection
///--------------------------------  

/// PURPOSE:
///    Gets variation index taking into account the previous variation  
FUNC INT ROULETTE_ANIM_GET_NEXT_RANDOM_VARIATION(INT iLastVariation, INT iMaxVariation)
	// Don't need to subtract one as random max is not inclusive
	INT iRandomIdle = GET_RANDOM_INT_IN_RANGE(0, iMaxVariation)
	
	// If variation is the same as the last variation, get the next one along
	IF iRandomIdle = iLastVariation
		iRandomIdle++
		iRandomIdle = WRAP_INDEX(iRandomIdle, iMaxVariation - 1)
	ENDIF
	
	RETURN iRandomIdle
ENDFUNC

/// PURPOSE:
///    Gets a new idle one shot variation index for the table dealer taking into account the number of available
///    varitations for the dealers gender
FUNC INT ROULETTE_ANIM_GET_IDLE_ONE_SHOT_VARIATION_FOR_TABLES_DEALER(ROULETTE_DATA &sRouletteData, INT iTableID)	
	IF NOT ROULETTE_TABLE_IS_TABLE_ID_VALID(iTableID)
		RETURN 0
	ENDIF
	
	INT iLastDealerVariation = sRouletteData.serverBD.sDealerLastAnim[iTableID].iVariation	
	IF ROULETTE_HELPER_IS_DEALER_AT_TABLE_FEMALE(sRouletteData, iTableID)
		RETURN ROULETTE_ANIM_GET_NEXT_RANDOM_VARIATION(iLastDealerVariation, ROULETTE_ANIM_NUMBER_OF_FEMALE_DEALER_IDLE_VARIATIONS)
	ENDIF
	
	RETURN ROULETTE_ANIM_GET_NEXT_RANDOM_VARIATION(iLastDealerVariation, ROULETTE_ANIM_NUMBER_OF_MALE_DEALER_IDLE_VARIATIONS)
ENDFUNC

/// PURPOSE:
///    Gets a random one shot variation for the empty table idle one shot
FUNC INT ROULETTE_ANIM_GET_IDLE_EMPTY_TABLE_ONE_SHOT_VARIATION_FOR_TABLES_DEALER(ROULETTE_DATA &sRouletteData, INT iTableID)	
	IF NOT ROULETTE_TABLE_IS_TABLE_ID_VALID(iTableID)
		RETURN 0
	ENDIF
	
	INT iLastDealerVariation = sRouletteData.serverBD.sDealerLastAnim[iTableID].iVariation	
	IF ROULETTE_HELPER_IS_DEALER_AT_TABLE_FEMALE(sRouletteData, iTableID)
		RETURN ROULETTE_ANIM_GET_NEXT_RANDOM_VARIATION(iLastDealerVariation, ROULETTE_ANIM_NUMBER_OF_FEMALE_DEALER_EMPTY_TABLE_IDLE_VARIATIONS)
	ENDIF
	
	RETURN ROULETTE_ANIM_GET_NEXT_RANDOM_VARIATION(iLastDealerVariation, ROULETTE_ANIM_NUMBER_OF_MALE_DEALER_IDLE_EMPTY_TABLE_VARIATIONS)
ENDFUNC

///--------------------------------
///    Anim clip info
///-------------------------------- 

/// PURPOSE:
///    Gets a dealer 'idle one shot' animation with a random variation
PROC ROULETTE_ANIM_GET_DEALER_RANDOM_IDLE_ONE_SHOT_ANIM(ROULETTE_DATA &sRouletteData, INT iTableID, ROULETTE_ANIM &sAnim)
	sAnim.eSyncSceneFlags = SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS
	sAnim.fBlendIn = REALLY_SLOW_BLEND_IN 
	sAnim.fBlendOut = REALLY_SLOW_BLEND_OUT
	sAnim.bHoldLastFrame= TRUE
	sAnim.iVariation = ROULETTE_ANIM_GET_IDLE_ONE_SHOT_VARIATION_FOR_TABLES_DEALER(sRouletteData, iTableID)	
	sAnim.eClip = ROULETTE_ANIM_CLIP_DEALER_IDLE_ONE_SHOT
	sAnim.eDictionary = ROULETTE_ANIM_GET_DICTIONARY_FOR_TABLE_DEALER(sRouletteData, iTableID)
ENDPROC

/// PURPOSE:
///    Gets a dealer 'empty table idle one shot' animation with a random variation
PROC ROULETTE_ANIM_GET_DEALER_RANDOM_IDLE_EMPTY_TABLE_ONE_SHOT_ANIM(ROULETTE_DATA &sRouletteData, INT iTableID, ROULETTE_ANIM &sAnim)
	sAnim.eSyncSceneFlags = SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS
	sAnim.fBlendIn = REALLY_SLOW_BLEND_IN 
	sAnim.fBlendOut = REALLY_SLOW_BLEND_OUT
	sAnim.bHoldLastFrame = TRUE
	sAnim.iVariation = ROULETTE_ANIM_GET_IDLE_EMPTY_TABLE_ONE_SHOT_VARIATION_FOR_TABLES_DEALER(sRouletteData, iTableID)	
	sAnim.eClip = ROULETTE_ANIM_CLIP_DEALER_IDLE_EMPTY_TABLE_ONE_SHOT
	sAnim.eDictionary = ROULETTE_ANIM_GET_DICTIONARY_FOR_TABLE_DEALER(sRouletteData, iTableID)
ENDPROC

/// PURPOSE:
///    Gets a dealer 'idle' animation
PROC ROULETTE_ANIM_GET_DEALER_IDLE_ANIM(ROULETTE_DATA &sRouletteData, INT iTableID, ROULETTE_ANIM &sAnim)
	sAnim.eSyncSceneFlags = SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_LOOP_WITHIN_SCENE
	sAnim.fBlendIn = REALLY_SLOW_BLEND_IN 
	sAnim.fBlendOut = REALLY_SLOW_BLEND_OUT
	sAnim.iVariation = 0
	sAnim.bLoop = TRUE
	sAnim.eClip = ROULETTE_ANIM_CLIP_DEALER_IDLE
	sAnim.eDictionary = ROULETTE_ANIM_GET_DICTIONARY_FOR_TABLE_DEALER(sRouletteData, iTableID)
ENDPROC

/// PURPOSE:
///    Gets the dealer 'wheel spin' animation
PROC ROULETTE_ANIM_GET_DEALER_WHEEL_SPIN_ANIM(ROULETTE_DATA &sRouletteData, INT iTableID, ROULETTE_ANIM &sAnim)
	sAnim.eSyncSceneFlags = SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS
	sAnim.fBlendIn = REALLY_SLOW_BLEND_IN
	sAnim.fBlendOut = REALLY_SLOW_BLEND_OUT
	sAnim.bHoldLastFrame = TRUE
	sAnim.eClip = ROULETTE_ANIM_CLIP_DEALER_SPIN
	sAnim.eDictionary = ROULETTE_ANIM_GET_DICTIONARY_FOR_TABLE_DEALER(sRouletteData, iTableID)
ENDPROC

/// PURPOSE:
///    Gets the dealer 'chip clear intro' animation
PROC ROULETTE_ANIM_GET_DEALER_CHIP_CLEAR_INTRO_ANIM(ROULETTE_DATA &sRouletteData, INT iTableID, ROULETTE_ANIM &sAnim)
	sAnim.eSyncSceneFlags = SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS
	sAnim.fBlendIn = REALLY_SLOW_BLEND_IN
	sAnim.fBlendOut = REALLY_SLOW_BLEND_OUT
	sAnim.bHoldLastFrame = TRUE
	sAnim.eClip = ROULETTE_ANIM_CLIP_DEALER_CLEAR_CHIPS_INTRO
	sAnim.eDictionary = ROULETTE_ANIM_GET_DICTIONARY_FOR_TABLE_DEALER(sRouletteData, iTableID)
ENDPROC

/// PURPOSE:
///    Gets the dealer 'chip clear zone 1' animation
PROC ROULETTE_ANIM_GET_DEALER_CHIP_CLEAR_ZONE_ANIM(ROULETTE_DATA &sRouletteData, INT iTableID, ROULETTE_ANIM &sAnim, ROULETTE_ANIM_CLIPS eZoneClip)
	sAnim.eSyncSceneFlags = SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS
	sAnim.fBlendIn = REALLY_SLOW_BLEND_IN
	sAnim.fBlendOut = REALLY_SLOW_BLEND_OUT
	sAnim.bHoldLastFrame = TRUE
	sAnim.eClip = eZoneClip
	sAnim.eDictionary = ROULETTE_ANIM_GET_DICTIONARY_FOR_TABLE_DEALER(sRouletteData, iTableID)
ENDPROC

/// PURPOSE:
///    Gets the play anim info for the dealer 'chip clear outro' animation clip
PROC ROULETTE_ANIM_GET_DEALER_CHIP_CLEAR_OUTRO_ANIM(ROULETTE_DATA &sRouletteData, INT iTableID, ROULETTE_ANIM &sAnim)
	sAnim.eSyncSceneFlags = SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS
	sAnim.fBlendIn = REALLY_SLOW_BLEND_IN
	sAnim.fBlendOut = REALLY_SLOW_BLEND_OUT
	sAnim.bHoldLastFrame = TRUE
	sAnim.eClip = ROULETTE_ANIM_CLIP_DEALER_CLEAR_CHIPS_OUTRO
	sAnim.eDictionary = ROULETTE_ANIM_GET_DICTIONARY_FOR_TABLE_DEALER(sRouletteData, iTableID)
ENDPROC

/// PURPOSE:
///    Gets the play anim info for the dealer 'no more bets' animation clip
PROC ROULETTE_ANIM_GET_DEALER_NO_MORE_BETS_ANIM(ROULETTE_DATA &sRouletteData, INT iTableID, ROULETTE_ANIM &sAnim)
	sAnim.eSyncSceneFlags = SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS
	sAnim.fBlendIn = REALLY_SLOW_BLEND_IN
	sAnim.fBlendOut = REALLY_SLOW_BLEND_OUT
	sAnim.bHoldLastFrame = TRUE
	sAnim.eClip = ROULETTE_ANIM_CLIP_DEALER_NO_MORE_BETS
	sAnim.eDictionary = ROULETTE_ANIM_GET_DICTIONARY_FOR_TABLE_DEALER(sRouletteData, iTableID)
ENDPROC


///--------------------------------
///    Anim clip playing
///--------------------------------   

/// PURPOSE:
///    Plays the dealer 'idle one shot' anim on the dealer at the given table
PROC ROULETTE_ANIM_PLAY_DEALER_IDLE_ONE_SHOT_ANIM_ON_TABLES_DEALER(ROULETTE_DATA &sRouletteData, INT iTableID)
	ROULETTE_ANIM sIdleAnim
	ROULETTE_ANIM_GET_DEALER_RANDOM_IDLE_ONE_SHOT_ANIM(sRouletteData, iTableID, sIdleAnim)
	ROULETTE_ANIM_PLAY_CLIP_ON_TABLES_DEALER(sRouletteData, iTableID, sIdleAnim)
ENDPROC

/// PURPOSE:
///    Plays the dealer 'empty table idle one shot' anim on the dealer at the given table
PROC ROULETTE_ANIM_PLAY_DEALER_IDLE_EMPTY_TABLE_ONE_SHOT_ANIM_ON_TABLES_DEALER(ROULETTE_DATA &sRouletteData, INT iTableID)
	ROULETTE_ANIM sIdleAnim
	ROULETTE_ANIM_GET_DEALER_RANDOM_IDLE_EMPTY_TABLE_ONE_SHOT_ANIM(sRouletteData, iTableID, sIdleAnim)
	ROULETTE_ANIM_PLAY_CLIP_ON_TABLES_DEALER(sRouletteData, iTableID, sIdleAnim)
ENDPROC

/// PURPOSE:
///    Plays the dealer 'idle' anim on the dealer at the given table
PROC ROULETTE_ANIM_PLAY_DEALER_IDLE_ANIM_ON_TABLES_DEALER(ROULETTE_DATA &sRouletteData, INT iTableID,
BOOL bUseRandomPhase = FALSE, FLOAT fMinPhase = 0.0, FLOAT fMaxPhase = 1.0)
	ROULETTE_ANIM sIdleAnim
	ROULETTE_ANIM_GET_DEALER_IDLE_ANIM(sRouletteData, iTableID, sIdleAnim)
	
	IF bUseRandomPhase
		sIdleAnim.fStartPhase = GET_RANDOM_FLOAT_IN_RANGE(fMinPhase, fMaxPhase)
	ENDIF	
	
	ROULETTE_ANIM_PLAY_CLIP_ON_TABLES_DEALER(sRouletteData, iTableID, sIdleAnim)
ENDPROC

/// PURPOSE:
///    Plays the dealer 'idle' animation on every dealer
PROC ROULETTE_ANIM_PLAY_DEALER_IDLE_ANIM_ON_ALL_DEALERS(ROULETTE_DATA &sRouletteData, 
BOOL bUseRandomPhase = FALSE, FLOAT fMinPhase = 0.0, FLOAT fMaxPhase = 1.0)
	ROULETTE_ANIM sIdleAnim
	
	INT i = 0
	REPEAT ROULETTE_TABLE_MAX_NUMBER_OF_TABLES i
		ROULETTE_ANIM_GET_DEALER_IDLE_ANIM(sRouletteData, i, sIdleAnim)
		
		IF bUseRandomPhase
			sIdleAnim.fStartPhase = GET_RANDOM_FLOAT_IN_RANGE(fMinPhase, fMaxPhase)
		ENDIF
		
		ROULETTE_ANIM_PLAY_CLIP_ON_TABLES_DEALER(sRouletteData, i, sIdleAnim)
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Plays an appropriate one shot animation on the dealer at the table, based on in the table is empty or not.
///    The chosen one shot will play with a random variation
PROC ROULETTE_ANIM_PLAY_APPROPRIATE_DEALER_IDLE_ONE_SHOT_ANIM_FOR_TABLE(ROULETTE_DATA &sRouletteData, INT iTableID)
	IF sRouletteData.serverBD.eServerState[iTableID] = ROULETTE_SERVER_STATE_PLAYING
		ROULETTE_ANIM_PLAY_DEALER_IDLE_ONE_SHOT_ANIM_ON_TABLES_DEALER(sRouletteData, iTableID)
		EXIT
	ENDIF
	
	// Table is idle as there are no players, play an empty table one shot
	ROULETTE_ANIM_PLAY_DEALER_IDLE_EMPTY_TABLE_ONE_SHOT_ANIM_ON_TABLES_DEALER(sRouletteData, iTableID)
ENDPROC

/// PURPOSE:
///    Maintains the dealers idle animation sequence. One shots are played in between loops
///    and specific one shots will play if the table is empty of players
PROC ROULETTE_ANIM_MAINTAIN_DEALER_IDLE(ROULETTE_DATA &sRouletteData, INT iTableID, BOOL bCanPlayOneShots = TRUE)		
	// Previous anim hasn't finished, don't start a new one until it has
	IF NOT ROULETTE_ANIM_HAS_TABLES_DEALER_FINISHED_PLAYING_CURRENT_ANIM(sRouletteData, iTableID)
		EXIT
	ENDIF
	
	// If the last anim was a idle loop and can play oneshots, play a oneshot
	IF sRouletteData.serverBD.sDealerLastAnim[iTableID].eClip = ROULETTE_ANIM_CLIP_DEALER_IDLE 
	AND bCanPlayOneShots
		ROULETTE_ANIM_PLAY_APPROPRIATE_DEALER_IDLE_ONE_SHOT_ANIM_FOR_TABLE(sRouletteData, iTableID)	
	ELSE
		ROULETTE_ANIM_PLAY_DEALER_IDLE_ANIM_ON_TABLES_DEALER(sRouletteData, iTableID)		
	ENDIF
ENDPROC

/// PURPOSE:
///    Plays the dealer 'wheel spin' anim on the dealer at the given table  
PROC ROULETTE_ANIM_PLAY_WHEEL_SPIN_ANIM_ON_TABLES_DEALER(ROULETTE_DATA &sRouletteData, INT iTableID)
	ROULETTE_ANIM sWheelSpinAnim
	ROULETTE_ANIM_GET_DEALER_WHEEL_SPIN_ANIM(sRouletteData, iTableID, sWheelSpinAnim)
	ROULETTE_ANIM_PLAY_CLIP_ON_TABLES_DEALER(sRouletteData, iTableID, sWheelSpinAnim)
ENDPROC

/// PURPOSE:
///    Plays the dealer 'chip clear intro' anim on the dealer at the given table     
PROC ROULETTE_ANIM_PLAY_DEALER_CHIP_CLEAR_INTRO_ANIM_ON_TABLES_DEALER(ROULETTE_DATA &sRouletteData, INT iTableID)
	ROULETTE_ANIM sChipClearIntroAnim
	ROULETTE_ANIM_GET_DEALER_CHIP_CLEAR_INTRO_ANIM(sRouletteData, iTableID, sChipClearIntroAnim)
	ROULETTE_ANIM_PLAY_CLIP_ON_TABLES_DEALER(sRouletteData, iTableID, sChipClearIntroAnim)
ENDPROC

/// PURPOSE:
///    Plays a chip clear anim clip on the dealer for the given zone clip  
PROC ROULETTE_ANIM_PLAY_DEALER_CHIP_CLEAR_ZONE_ANIM_ON_TABLES_DEALER(ROULETTE_DATA &sRouletteData, INT iTableID, ROULETTE_ANIM_CLIPS eZoneClip)
	ROULETTE_ANIM sChipClearZoneAnim
	ROULETTE_ANIM_GET_DEALER_CHIP_CLEAR_ZONE_ANIM(sRouletteData, iTableID, sChipClearZoneAnim, eZoneClip)
	ROULETTE_ANIM_PLAY_CLIP_ON_TABLES_DEALER(sRouletteData, iTableID, sChipClearZoneAnim)
ENDPROC

/// PURPOSE:
///    Plays the dealer 'chip clear outro' clip on the dealer at the given table  
PROC ROULETTE_ANIM_PLAY_DEALER_CHIP_CLEAR_OUTRO_ANIM_ON_TABLES_DEALER(ROULETTE_DATA &sRouletteData, INT iTableID)
	ROULETTE_ANIM sDealerChipClearOutroAnim
	ROULETTE_ANIM_GET_DEALER_CHIP_CLEAR_OUTRO_ANIM(sRouletteData, iTableID, sDealerChipClearOutroAnim)
	ROULETTE_ANIM_PLAY_CLIP_ON_TABLES_DEALER(sRouletteData, iTableID, sDealerChipClearOutroAnim)
ENDPROC

/// PURPOSE:
///    Plays the dealer 'no more bets' clip on the dealer at the given table  
PROC ROULETTE_ANIM_PLAY_DEALER_NO_MORE_BETS_ANIM_ON_TABLES_DEALER(ROULETTE_DATA &sRouletteData, INT iTableID)
	ROULETTE_ANIM sDealerNoMoreBetAnim
	ROULETTE_ANIM_GET_DEALER_NO_MORE_BETS_ANIM(sRouletteData, iTableID, sDealerNoMoreBetAnim)
	ROULETTE_ANIM_PLAY_CLIP_ON_TABLES_DEALER(sRouletteData, iTableID, sDealerNoMoreBetAnim)
ENDPROC


///--------------------------------
///    Anim clip sequences
///--------------------------------

/// PURPOSE:
///     Updates the animation sequence for making the dealer spin the roulette wheel 
FUNC BOOL ROULETTE_ANIM_UPDATE_WHEEL_SPIN_ANIM_SEQUENCE_ON_TABLES_DEALER(ROULETTE_DATA &sRouletteData, INT iTableID)
	SWITCH sRouletteData.serverBD.sDealerLastAnim[iTableID].eClip
		CASE ROULETTE_ANIM_CLIP_DEALER_SPIN
			// Spin sequence over when this anim has finished
			RETURN ROULETTE_ANIM_HAS_TABLES_DEALER_FINISHED_PLAYING_CURRENT_ANIM(sRouletteData, iTableID)
			
		CASE ROULETTE_ANIM_CLIP_DEALER_NO_MORE_BETS
			// Don't allow spin anim to play if playing no more bets
			RETURN TRUE
			
		DEFAULT
			// Start dealer spin animation if not already spining and not playing no more bets
			ROULETTE_ANIM_PLAY_WHEEL_SPIN_ANIM_ON_TABLES_DEALER(sRouletteData, iTableID)
			RETURN FALSE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Update the iZoneClearBS with the zone that need clearing, based on which zones each player has bets in
///    e.g. if player A has a bet in zone 1 and player B has a bet in zone 3, we should clear zones 1 and 3
PROC ROULETTE_ANIM_DETERMINE_WHICH_ZONES_NEED_CLEARING(ROULETTE_DATA &sRouletteData, INT iTableID)
	INT iPlayerID = -1
	sRouletteData.serverBD.iZonesToClearBS[iTableID] = 0
	
	INT iLocalSeatID = 0
	REPEAT ROULETTE_TABLE_MAX_NUMBER_OF_PLAYERS_PER_TABLE iLocalSeatID
		
		INT iSeatID = ROULETTE_HELPER_GET_SEAT_ID_FROM_LOCAL_SEAT_ID_AT_TABLE(iLocalSeatID, iTableID)
		IF NOT ROULETTE_HELPER_IS_PLAYER_AT_SEAT_STILL_PLAYING(sRouletteData, iSeatID)
			RELOOP
		ENDIF
		
		PLAYER_INDEX piPlayerID = sRouletteData.serverBD.playersUsingSeats[iSeatID]
			
		IF piPlayerID = INVALID_PLAYER_INDEX()		
			RELOOP
		ENDIF
		
		iPlayerID = NATIVE_TO_INT(piPlayerID)
			
		// Check and store which zones the player has bets in
		IF IS_BIT_SET(sRouletteData.playerBD[iPlayerID].iPlayerBS, ROULETTE_PLAYER_BS_HAS_BET_IN_ZONE_THREE) 
			SET_BIT(sRouletteData.serverBD.iZonesToClearBS[iTableID], ROULETTE_PLAYER_BS_HAS_BET_IN_ZONE_THREE)
		ENDIF
		
		IF IS_BIT_SET(sRouletteData.playerBD[iPlayerID].iPlayerBS, ROULETTE_PLAYER_BS_HAS_BET_IN_ZONE_TWO) 
			SET_BIT(sRouletteData.serverBD.iZonesToClearBS[iTableID], ROULETTE_PLAYER_BS_HAS_BET_IN_ZONE_TWO)
		ENDIF
		
		IF IS_BIT_SET(sRouletteData.playerBD[iPlayerID].iPlayerBS, ROULETTE_PLAYER_BS_HAS_BET_IN_ZONE_ONE) 
			SET_BIT(sRouletteData.serverBD.iZonesToClearBS[iTableID], ROULETTE_PLAYER_BS_HAS_BET_IN_ZONE_ONE)
		ENDIF
		
		// If all zones should be cleared there is no point checking the rest of the bets
		IF IS_BIT_SET(sRouletteData.serverBD.iZonesToClearBS[iTableID], ROULETTE_PLAYER_BS_HAS_BET_IN_ZONE_THREE) 
		AND IS_BIT_SET(sRouletteData.serverBD.iZonesToClearBS[iTableID], ROULETTE_PLAYER_BS_HAS_BET_IN_ZONE_TWO)
		AND IS_BIT_SET(sRouletteData.serverBD.iZonesToClearBS[iTableID], ROULETTE_PLAYER_BS_HAS_BET_IN_ZONE_ONE) 
			EXIT
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Plays the next required zone clear animation based on the zones that have been marked to be cleared   
FUNC BOOL ROULETTE_ANIM_PLAY_NEXT_REQUIRED_ZONE_CLEAR_ANIM(ROULETTE_DATA &sRouletteData, INT iTableID)
	
	// Start clearing from last zone first
	IF IS_BIT_SET(sRouletteData.serverBD.iZonesToClearBS[iTableID], ROULETTE_PLAYER_BS_HAS_BET_IN_ZONE_THREE)	
		ROULETTE_ANIM_PLAY_DEALER_CHIP_CLEAR_ZONE_ANIM_ON_TABLES_DEALER(sRouletteData, iTableID, ROULETTE_ANIM_CLIP_DEALER_CLEAR_CHIPS_ZONE_THREE)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_CHIP_CLEAR - Playing clear zone 3")
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(sRouletteData.serverBD.iZonesToClearBS[iTableID], ROULETTE_PLAYER_BS_HAS_BET_IN_ZONE_TWO)
		ROULETTE_ANIM_PLAY_DEALER_CHIP_CLEAR_ZONE_ANIM_ON_TABLES_DEALER(sRouletteData, iTableID, ROULETTE_ANIM_CLIP_DEALER_CLEAR_CHIPS_ZONE_TWO)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_CHIP_CLEAR - Playing clear zone 2")
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(sRouletteData.serverBD.iZonesToClearBS[iTableID], ROULETTE_PLAYER_BS_HAS_BET_IN_ZONE_ONE)
		ROULETTE_ANIM_PLAY_DEALER_CHIP_CLEAR_ZONE_ANIM_ON_TABLES_DEALER(sRouletteData, iTableID, ROULETTE_ANIM_CLIP_DEALER_CLEAR_CHIPS_ZONE_ONE)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_CHIP_CLEAR - Playing clear zone 1")
		RETURN TRUE
	ENDIF	
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Update state for the chip clear intro
PROC ROULETTE_ANIM_UPDATE_CHIP_CLEAR_INTRO(ROULETTE_DATA &sRouletteData, INT iTableID)
	IF NOT ROULETTE_ANIM_HAS_TABLES_DEALER_FINISHED_PLAYING_CURRENT_ANIM(sRouletteData, iTableID)				
		EXIT			
	ENDIF
	
	IF ROULETTE_ANIM_PLAY_NEXT_REQUIRED_ZONE_CLEAR_ANIM(sRouletteData, iTableID)
		EXIT
	ENDIF
		
	// No more zones to clear proceed to outro
	ROULETTE_ANIM_PLAY_DEALER_CHIP_CLEAR_OUTRO_ANIM_ON_TABLES_DEALER(sRouletteData, iTableID)
ENDPROC

/// PURPOSE:
///    Update state for clearing zone three of chips
PROC ROULETTE_ANIM_UPDATE_CHIP_CLEAR_ZONE_THREE(ROULETTE_DATA &sRouletteData, INT iTableID)
	IF NOT ROULETTE_ANIM_HAS_TABLES_DEALER_FINISHED_PLAYING_CURRENT_ANIM(sRouletteData, iTableID)		
		EXIT
	ENDIF
	
	// Mark zone one as already cleared
	CLEAR_BIT(sRouletteData.serverBD.iZonesToClearBS[iTableID], ROULETTE_PLAYER_BS_HAS_BET_IN_ZONE_THREE)
	IF ROULETTE_ANIM_PLAY_NEXT_REQUIRED_ZONE_CLEAR_ANIM(sRouletteData, iTableID)
		EXIT
	ENDIF
	
	// No more zones to clear proceed to outro
	ROULETTE_ANIM_PLAY_DEALER_CHIP_CLEAR_OUTRO_ANIM_ON_TABLES_DEALER(sRouletteData, iTableID)
ENDPROC

/// PURPOSE:
///    Update state for clearing zone two of chips
PROC ROULETTE_ANIM_UPDATE_CHIP_CLEAR_ZONE_TWO(ROULETTE_DATA &sRouletteData, INT iTableID)
	IF NOT ROULETTE_ANIM_HAS_TABLES_DEALER_FINISHED_PLAYING_CURRENT_ANIM(sRouletteData, iTableID)
		EXIT
	ENDIF
	
	// Mark zone two as already cleared
	CLEAR_BIT(sRouletteData.serverBD.iZonesToClearBS[iTableID], ROULETTE_PLAYER_BS_HAS_BET_IN_ZONE_TWO)
	IF ROULETTE_ANIM_PLAY_NEXT_REQUIRED_ZONE_CLEAR_ANIM(sRouletteData, iTableID)
		EXIT
	ENDIF
	
	// No more zones to clear proceed to outro
	ROULETTE_ANIM_PLAY_DEALER_CHIP_CLEAR_OUTRO_ANIM_ON_TABLES_DEALER(sRouletteData, iTableID)
ENDPROC

/// PURPOSE:
///    Update state for clearing zone one of chips
PROC ROULETTE_ANIM_UPDATE_CHIP_CLEAR_ZONE_ONE(ROULETTE_DATA &sRouletteData, INT iTableID)
	IF ROULETTE_ANIM_HAS_TABLES_DEALER_FINISHED_PLAYING_CURRENT_ANIM(sRouletteData, iTableID)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_CHIP_CLEAR - Playing outro")
		CLEAR_BIT(sRouletteData.serverBD.iZonesToClearBS[iTableID], ROULETTE_PLAYER_BS_HAS_BET_IN_ZONE_ONE)
		ROULETTE_ANIM_PLAY_DEALER_CHIP_CLEAR_OUTRO_ANIM_ON_TABLES_DEALER(sRouletteData, iTableID)
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks if all the zones on the table have been cleared 
FUNC BOOL ROULETTE_ANIM_ALL_ZONES_ARE_CLEAR_OF_CHIPS(ROULETTE_DATA &sRouletteData, INT iTableID)
	RETURN NOT IS_BIT_SET(sRouletteData.serverBD.iZonesToClearBS[iTableID], ROULETTE_PLAYER_BS_HAS_BET_IN_ZONE_ONE) 
	AND NOT IS_BIT_SET(sRouletteData.serverBD.iZonesToClearBS[iTableID], ROULETTE_PLAYER_BS_HAS_BET_IN_ZONE_TWO)
	AND NOT IS_BIT_SET(sRouletteData.serverBD.iZonesToClearBS[iTableID], ROULETTE_PLAYER_BS_HAS_BET_IN_ZONE_THREE)
ENDFUNC

/// PURPOSE:
///     Updates the animation sequence for making the tables dealer clear the chips from the table
FUNC BOOL ROULETTE_ANIM_UPDATE_CHIP_CLEAR_ANIM_SEQUENCE_ON_TABLES_DEALER(ROULETTE_DATA &sRouletteData, INT iTableID)	
		
	SWITCH sRouletteData.serverBD.sDealerLastAnim[iTableID].eClip
		CASE ROULETTE_ANIM_CLIP_DEALER_CLEAR_CHIPS_INTRO
			ROULETTE_ANIM_UPDATE_CHIP_CLEAR_INTRO(sRouletteData, iTableID)
		BREAK				
		CASE ROULETTE_ANIM_CLIP_DEALER_CLEAR_CHIPS_ZONE_THREE
			ROULETTE_ANIM_UPDATE_CHIP_CLEAR_ZONE_THREE(sRouletteData, iTableID)
		BREAK
		CASE ROULETTE_ANIM_CLIP_DEALER_CLEAR_CHIPS_ZONE_TWO
			ROULETTE_ANIM_UPDATE_CHIP_CLEAR_ZONE_TWO(sRouletteData, iTableID)
		BREAK
		CASE ROULETTE_ANIM_CLIP_DEALER_CLEAR_CHIPS_ZONE_ONE
			ROULETTE_ANIM_UPDATE_CHIP_CLEAR_ZONE_ONE(sRouletteData, iTableID)
		BREAK
		
		// If on the outro anim clip, return true for sequence complete if it has finished
		CASE ROULETTE_ANIM_CLIP_DEALER_CLEAR_CHIPS_OUTRO
			RETURN ROULETTE_ANIM_HAS_TABLES_DEALER_FINISHED_PLAYING_CURRENT_ANIM(sRouletteData, iTableID)
		
		// Start animation sequence if none of the previous anim clips are playing
		DEFAULT
			// Leave if no chips need clearing
			IF ROULETTE_ANIM_ALL_ZONES_ARE_CLEAR_OF_CHIPS(sRouletteData, iTableID)
				RETURN TRUE
			ENDIF
			ROULETTE_ANIM_PLAY_DEALER_CHIP_CLEAR_INTRO_ANIM_ON_TABLES_DEALER(sRouletteData, iTableID)
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if the first zone on the current table needs clearing
FUNC BOOL ROULETTE_ANIM_SHOULD_ZONE_ONE_CHIPS_CLEAR(ROULETTE_DATA &sRouletteData, INT iTableID)
	IF NOT IS_BIT_SET(sRouletteData.iTableBS[iTableID], ROULETTE_CLIENT_TABLE_BS_HAS_BET_IN_ZONE_ONE)
		RETURN FALSE
	ENDIF
	
	IF NOT ROULETTE_ANIM_IS_TABLES_DEALER_PLAYING_CLIP(sRouletteData, iTableID, ROULETTE_ANIM_CLIP_DEALER_CLEAR_CHIPS_ZONE_ONE)
		RETURN FALSE
	ENDIF	
	
	IF ROULETTE_ANIM_HAS_DEALER_EVENT_TRIGGERED(sRouletteData, iTableID, GET_HASH_KEY("CLEAR_CHIPS_ZONE_ONE"))
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_SHOULD_ZONE_ONE_CHIPS_CLEAR - Event triggered")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if the second zone on the current table needs clearing
FUNC BOOL ROULETTE_ANIM_SHOULD_ZONE_TWO_CHIPS_CLEAR(ROULETTE_DATA &sRouletteData, INT iTableID)
	IF NOT IS_BIT_SET(sRouletteData.iTableBS[iTableID], ROULETTE_CLIENT_TABLE_BS_HAS_BET_IN_ZONE_TWO)
		RETURN FALSE
	ENDIF
		
	IF NOT ROULETTE_ANIM_IS_TABLES_DEALER_PLAYING_CLIP(sRouletteData, iTableID, ROULETTE_ANIM_CLIP_DEALER_CLEAR_CHIPS_ZONE_TWO)
		RETURN FALSE
	ENDIF
	
	
	IF ROULETTE_ANIM_HAS_DEALER_EVENT_TRIGGERED(sRouletteData, iTableID, GET_HASH_KEY("CLEAR_CHIPS_ZONE_TWO"))
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_SHOULD_ZONE_ONE_CHIPS_CLEAR - Event triggered")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if the third zone on the current table needs clearing
FUNC BOOL ROULETTE_ANIM_SHOULD_ZONE_THREE_CHIPS_CLEAR(ROULETTE_DATA &sRouletteData, INT iTableID)
	IF NOT IS_BIT_SET(sRouletteData.iTableBS[iTableID], ROULETTE_CLIENT_TABLE_BS_HAS_BET_IN_ZONE_THREE)
		RETURN FALSE
	ENDIF
	
	IF NOT ROULETTE_ANIM_IS_TABLES_DEALER_PLAYING_CLIP(sRouletteData, iTableID, ROULETTE_ANIM_CLIP_DEALER_CLEAR_CHIPS_ZONE_THREE)
		RETURN FALSE
	ENDIF
	
	IF ROULETTE_ANIM_HAS_DEALER_EVENT_TRIGGERED(sRouletteData, iTableID, GET_HASH_KEY("CLEAR_CHIPS_ZONE_THREE"))
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_SHOULD_ZONE_ONE_CHIPS_CLEAR - Event triggered")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if the chip clearing sequence for the table has finished   
FUNC BOOL ROULETTE_ANIM_HAS_CLIENT_CHIP_CLEARING_FINISHED(ROULETTE_DATA &sRouletteData, INT iTableID)
	
	IF sRouletteData.serverBD.eGameState[iTableID] != ROULETTE_GAME_STATE_ROUND_RESOLUTION
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_HAS_CLIENT_CHIP_CLEARING_FINISHED - Not in round resolution state")
		RETURN TRUE
	ENDIF
	
	BOOL bZonesStillNeedClearing 
	bZonesStillNeedClearing = IS_BIT_SET(sRouletteData.iTableBS[iTableID], ROULETTE_CLIENT_TABLE_BS_HAS_BET_IN_ZONE_ONE)
	OR IS_BIT_SET(sRouletteData.iTableBS[iTableID], ROULETTE_CLIENT_TABLE_BS_HAS_BET_IN_ZONE_TWO)
	OR IS_BIT_SET(sRouletteData.iTableBS[iTableID], ROULETTE_CLIENT_TABLE_BS_HAS_BET_IN_ZONE_THREE)
	
	// Return false if a zone still hasn't cleared
	RETURN NOT bZonesStillNeedClearing
ENDFUNC

/// PURPOSE:
///    Checks if the chip clearing sequence for the current table has finished  
FUNC BOOL ROULETTE_ANIM_HAS_CLIENT_CHIP_CLEARING_FINISHED_FOR_CURRENT_TABLE(ROULETTE_DATA &sRouletteData)
	INT iTableID = sRouletteData.iTableID
	
	IF NOT ROULETTE_TABLE_IS_TABLE_ID_VALID(iTableID)
		RETURN TRUE
	ENDIF
	
	RETURN ROULETTE_ANIM_HAS_CLIENT_CHIP_CLEARING_FINISHED(sRouletteData, iTableID)
ENDFUNC

/// PURPOSE:
///    Clears the first zone of chips, playing appropriate sounds
PROC ROULETTE_ANIM_CLEAR_ZONE_ONE_CHIPS(ROULETTE_DATA &sRouletteData, INT iTableID)
	INT iChipsDeleted = ROULETTE_PROPS_DELETE_ZONE_CHIPS_FROM_TABLE(sRouletteData, iTableID, ROULETTE_PLAYER_BS_HAS_BET_IN_ZONE_ONE)	
	ROULETTE_AUDIO_PLAY_SFX_APPROPRIATE_CLEAR_SOUND_FOR_ZONE(sRouletteData, iTableID, ROULETTE_PLAYER_BS_HAS_BET_IN_ZONE_ONE, iChipsDeleted)
	CLEAR_BIT(sRouletteData.iTableBS[iTableID], ROULETTE_CLIENT_TABLE_BS_HAS_BET_IN_ZONE_ONE)
ENDPROC

/// PURPOSE:
///    Clears the second zone of chips, playing appropriate sounds
PROC ROULETTE_ANIM_CLEAR_ZONE_TWO_CHIPS(ROULETTE_DATA &sRouletteData, INT iTableID)
	INT iChipsDeleted = ROULETTE_PROPS_DELETE_ZONE_CHIPS_FROM_TABLE(sRouletteData, iTableID, ROULETTE_PLAYER_BS_HAS_BET_IN_ZONE_TWO)	
	ROULETTE_AUDIO_PLAY_SFX_APPROPRIATE_CLEAR_SOUND_FOR_ZONE(sRouletteData, iTableID, ROULETTE_PLAYER_BS_HAS_BET_IN_ZONE_TWO, iChipsDeleted)
	CLEAR_BIT(sRouletteData.iTableBS[iTableID], ROULETTE_CLIENT_TABLE_BS_HAS_BET_IN_ZONE_TWO)
ENDPROC

/// PURPOSE:
///    Clears the third zone of chips, playing appropriate sounds
PROC ROULETTE_ANIM_CLEAR_ZONE_THREE_CHIPS(ROULETTE_DATA &sRouletteData, INT iTableID)
	INT iChipsDeleted = ROULETTE_PROPS_DELETE_ZONE_CHIPS_FROM_TABLE(sRouletteData, iTableID, ROULETTE_PLAYER_BS_HAS_BET_IN_ZONE_THREE)	
	ROULETTE_AUDIO_PLAY_SFX_APPROPRIATE_CLEAR_SOUND_FOR_ZONE(sRouletteData, iTableID, ROULETTE_PLAYER_BS_HAS_BET_IN_ZONE_THREE, iChipsDeleted)
	CLEAR_BIT(sRouletteData.iTableBS[iTableID], ROULETTE_CLIENT_TABLE_BS_HAS_BET_IN_ZONE_THREE)
ENDPROC

/// PURPOSE:
///    Handles deleting the chip objects for each zone based on the dealers animation
FUNC BOOL ROULETTE_ANIM_UPDATE_CHIP_CLEARING_FOR_CLIENT_TABLE(ROULETTE_DATA &sRouletteData, INT iTableID)	
	
	IF ROULETTE_ANIM_SHOULD_ZONE_ONE_CHIPS_CLEAR(sRouletteData, iTableID)
		ROULETTE_ANIM_CLEAR_ZONE_ONE_CHIPS(sRouletteData, iTableID)
		RETURN FALSE
	ENDIF
	
	IF ROULETTE_ANIM_SHOULD_ZONE_TWO_CHIPS_CLEAR(sRouletteData, iTableID)
		ROULETTE_ANIM_CLEAR_ZONE_TWO_CHIPS(sRouletteData, iTableID)
		RETURN FALSE
	ENDIF
	
	IF ROULETTE_ANIM_SHOULD_ZONE_THREE_CHIPS_CLEAR(sRouletteData, iTableID)
		ROULETTE_ANIM_CLEAR_ZONE_THREE_CHIPS(sRouletteData, iTableID)
		RETURN FALSE
	ENDIF
	
	RETURN ROULETTE_ANIM_HAS_CLIENT_CHIP_CLEARING_FINISHED(sRouletteData, iTableID)
ENDFUNC

