//////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Name:        RouletteAnimationWheel.sch																		
/// Description: Header for controlling animations for the roulette minigames wheel spinning								
///																											
/// Written by:  Online Technical Team: Tom Turner,															
/// Date:  		07/03/2019 (refactored into own file 15/05/2019)																					
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "RouletteAnimationOverrides.sch"
USING "RouletteAudioOverrides.sch"
USING "RouletteProps.sch"
USING "script_MISC.sch"

CONST_FLOAT LOOP_PHASE_DIFFERENCE 0.5
CONST_FLOAT ADDITIVE_INTRO_ROTATION_SYNC_THRESHOLD 5.0

///--------------------------------
///    Dictionary Loading
///--------------------------------

/// PURPOSE:
///    Loads all animation dictionaries used in the roulette minigame
PROC ROULETTE_ANIM_LOAD_PROP_DICTIONARY()
	REQUEST_ANIM_DICT(ROULETTE_ANIM_GET_DICTIONARY(ROULETTE_ANIM_DICT_PROP))
ENDPROC

/// PURPOSE:
///    Checks if all the animation dictionaries used in the roulette minigame have loaded   
FUNC BOOL ROULETTE_ANIM_HAS_PROP_DICTIONARY_LOADED()
	RETURN HAS_ANIM_DICT_LOADED(ROULETTE_ANIM_GET_DICTIONARY(ROULETTE_ANIM_DICT_PROP))
ENDFUNC

/// PURPOSE:
///    Removes the roulette prop dictionary that contins animations concerning the ball and wheel
PROC ROULETTE_ANIM_REMOVE_PROP_DICTIONARY()
	REMOVE_ANIM_DICT(ROULETTE_ANIM_GET_DICTIONARY(ROULETTE_ANIM_DICT_PROP))
ENDPROC

///--------------------------------
///    Assets
///--------------------------------

/// PURPOSE:
///   Gets the starting rotation of the wheel on the table with the given table ID    
FUNC VECTOR ROULETTE_ANIM_GET_WHEEL_START_ROTATION(ROULETTE_WHEEL_SPIN &sSpin)
	// We only need to grab it once at the start
	IF sSpin.bWheelStartRotationInit
		RETURN sSpin.vWheelStartRotation	
	ENDIF
	
	sSpin.vWheelStartRotation = ROULETTE_PROPS_GET_WHEEL_CURRENT_ROTATION(sSpin.objWheel)
	sSpin.bWheelStartRotationInit = TRUE
	RETURN sSpin.vWheelStartRotation
ENDFUNC

/// PURPOSE:
///    Places the ball prop in its correct starting position based on the wheel bone position
PROC ROULETTE_PROPS_INIT_BALL_FOR_ANIMATION(ROULETTE_WHEEL_SPIN &sSpin)
	OBJECT_INDEX objBall = sSpin.objBall
	
	IF NOT DOES_ENTITY_EXIST(objBall)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_PROPS_SET_BALL_TO_START_TRANSFORM - ball does not exist")
		EXIT
	ENDIF
	
	VECTOR vTableWheelPos = ROULETTE_PROPS_GET_WHEEL_BONE_POSITION(sSpin.objWheel)
	VECTOR vTableWheelRot = ROULETTE_ANIM_GET_WHEEL_START_ROTATION(sSpin)
	
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_PROPS_SET_BALL_TO_START_TRANSFORM -",
	" pos: ", vTableWheelPos,
	" rot: ", vTableWheelRot)
	
	SET_ENTITY_COORDS(objBall, vTableWheelPos)
	SET_ENTITY_ROTATION(objBall, vTableWheelRot)
ENDPROC

/// PURPOSE:
///    Checks if the wheel assets are ready to be used  
FUNC BOOL ROULETTE_ANIM_ARE_WHEEL_ASSETS_READY(ROULETTE_WHEEL_SPIN &sSpin)
	IF NOT DOES_ENTITY_EXIST(sSpin.objBall)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_ARE_WHEEL_ASSETS_READY - ",
		"ball doesn't exist")
		DEBUG_PRINTCALLSTACK()
		RETURN FALSE
	ENDIF
	
	IF NOT DOES_ENTITY_HAVE_DRAWABLE(sSpin.objBall)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_ARE_WHEEL_ASSETS_READY - ",
		"ball doesn't have drawable")
		DEBUG_PRINTCALLSTACK()
		RETURN FALSE
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(sSpin.objWheel)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_ARE_WHEEL_ASSETS_READY - ",
		"table doesn't exist")
		DEBUG_PRINTCALLSTACK()
		RETURN FALSE
	ENDIF
	
	IF NOT DOES_ENTITY_HAVE_DRAWABLE(sSpin.objWheel)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_ARE_WHEEL_ASSETS_READY - ",
		"table doesn't have drawable")
		DEBUG_PRINTCALLSTACK()
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC


///--------------------------------
///    Anim clip checking - Wheel
///--------------------------------

/// PURPOSE:
///    Gets the phase of the roulette wheels current animation
FUNC FLOAT ROULETTE_ANIM_GET_WHEELS_CURRENT_ANIMATIONS_PHASE(ROULETTE_WHEEL_SPIN &sSpin)
	OBJECT_INDEX objWheel = sSpin.objWheel
	
	IF NOT DOES_ENTITY_EXIST(objWheel)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_GET_WHEELS_CURRENT_ANIMATIONS_PHASE", 
		" - Table entity does not exist")
		RETURN 0.0
	ENDIF
	
	STRING strAnimDict = ROULETTE_ANIM_GET_DICTIONARY(ROULETTE_ANIM_DICT_PROP)
	STRING strAnimClip = sSpin.strWheelLastAnim
	
	IF NOT IS_ENTITY_PLAYING_ANIM(objWheel, strAnimDict, strAnimClip)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_GET_WHEELS_CURRENT_ANIMATIONS_PHASE", 
		" - not playing", strAnimClip)
		RETURN 0.0
	ENDIF
	
	RETURN GET_ENTITY_ANIM_CURRENT_TIME(objWheel, strAnimDict, strAnimClip)
ENDFUNC

/// PURPOSE:
///    Checks if the current animation being played on the current tables wheel has finished playing  
FUNC BOOL ROULETTE_ANIM_HAS_WHEEL_AT_TABLE_FINISHED_PLAYING_LAST_CLIP(ROULETTE_WHEEL_SPIN &sSpin, FLOAT fEndPhase = 1.0)
	FLOAT fCurrentAnimTime
	fCurrentAnimTime = ROULETTE_ANIM_GET_WHEELS_CURRENT_ANIMATIONS_PHASE(sSpin)
	RETURN fCurrentAnimTime >= fEndPhase
ENDFUNC

/// PURPOSE:
///    Safely checks if the given event ID has triggered on the given table ids current animation   
FUNC BOOL ROULETTE_ANIM_HAS_WHEEL_EVENT_TRIGGERED(OBJECT_INDEX objWheel, INT iEventHash)
	IF NOT DOES_ENTITY_EXIST(objWheel)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_HAS_WHEEL_EVENT_TRIGGERED",
		" - table does not exist")
		RETURN FALSE
	ENDIF
	
	IF NOT DOES_ENTITY_HAVE_DRAWABLE(objWheel)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_HAS_WHEEL_EVENT_TRIGGERED",
		" - table does not have drawable")
		RETURN FALSE
	ENDIF
	
	RETURN HAS_ANIM_EVENT_FIRED(objWheel, iEventHash)
ENDFUNC


///--------------------------------
///    Anim clip checking - Ball
///--------------------------------
   
/// PURPOSE:
///    Safely checks if the given event ID has triggered on the ball at the given table ids current animation   
FUNC BOOL ROULETTE_ANIM_HAS_BALL_EVENT_TRIGGERED(ROULETTE_WHEEL_SPIN &sSpin, INT iEventHash)
	IF NOT DOES_ENTITY_EXIST(sSpin.objBall)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_HAS_BALL_EVENT_TRIGGERED",
		" - objBall does not exist")
		RETURN FALSE
	ENDIF
	
	IF NOT DOES_ENTITY_HAVE_DRAWABLE(sSpin.objBall)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_HAS_BALL_EVENT_TRIGGERED",
		" - objBall does not have drawable")
		RETURN FALSE
	ENDIF
		
	STRING strAnimDict = ROULETTE_ANIM_GET_DICTIONARY(ROULETTE_ANIM_DICT_PROP)
	STRING strAnimClip = sSpin.strBallLastAnim
	
	IF NOT IS_ENTITY_PLAYING_ANIM(sSpin.objBall, strAnimDict, strAnimClip)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_HAS_BALL_EVENT_TRIGGERED",
		" - not playing anim", strAnimClip)
		RETURN FALSE
	ENDIF	
		
	RETURN HAS_ANIM_EVENT_FIRED(sSpin.objBall, iEventHash)
ENDFUNC

/// PURPOSE:
///     Gets the phase of the ball props current animation   
FUNC FLOAT ROULETTE_ANIM_GET_BALLS_CURRENT_ANIMATIONS_PHASE(ROULETTE_WHEEL_SPIN &sSpin)
	OBJECT_INDEX objBall = sSpin.objBall
	
	IF NOT DOES_ENTITY_EXIST(objBall)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_GET_BALLS_CURRENT_ANIMATIONS_PHASE",
		" - Ball entity does not exist")
		RETURN 0.0
	ENDIF
	
	STRING strAnimDict = ROULETTE_ANIM_GET_DICTIONARY(ROULETTE_ANIM_DICT_PROP)
	STRING strAnimClip = sSpin.strBallLastAnim
	
	IF NOT IS_ENTITY_PLAYING_ANIM(objBall, strAnimDict, strAnimClip)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_GET_BALLS_CURRENT_ANIMATIONS_PHASE",
		" - not playing", strAnimClip)
		RETURN 0.0
	ENDIF
	
	RETURN GET_ENTITY_ANIM_CURRENT_TIME(objBall, strAnimDict, strAnimClip)
ENDFUNC

/// PURPOSE:
///    Checks if the current animation being played on the current tables wheel has finished playing  
FUNC BOOL ROULETTE_ANIM_HAS_BALL_AT_TABLE_FINISHED_PLAYING_LAST_CLIP(ROULETTE_WHEEL_SPIN &sSpin, FLOAT fEndPhase = 1.0)
	FLOAT fCurrentAnimTime
	fCurrentAnimTime = ROULETTE_ANIM_GET_BALLS_CURRENT_ANIMATIONS_PHASE(sSpin)
	RETURN fCurrentAnimTime >= fEndPhase
ENDFUNC


///--------------------------------
///    Anim clip playing - Wheel
///-------------------------------- 

/// PURPOSE:
///    Plays a roulette animation clip on the wheel for the given table
FUNC BOOL ROULETTE_ANIM_PLAY_CLIP_ON_TABLE_WHEEL(ROULETTE_WHEEL_SPIN &sSpin,
STRING strAnimClip, BOOL bLoop, BOOL bHoldLastFrame, FLOAT fStartPhase = 0.0)

	IF NOT HAS_ANIM_DICT_LOADED(ROULETTE_ANIM_GET_DICTIONARY(ROULETTE_ANIM_DICT_PROP))
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_PLAY_CLIP_ON_TABLE_WHEEL - Dictionary not loaded")
		RETURN FALSE
	ENDIF
	
	OBJECT_INDEX objWheel = sSpin.objWheel
	
	IF NOT DOES_ENTITY_EXIST(objWheel)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_PLAY_CLIP_ON_TABLE_WHEEL",
		" - table entity does not exist")
		RETURN FALSE
	ENDIF
	
	IF NOT DOES_ENTITY_HAVE_DRAWABLE(objWheel)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_PLAY_CLIP_ON_TABLE_WHEEL",
		" - table entity does not have drawable")
		RETURN FALSE
	ENDIF
	
	STRING strAnimDict = ROULETTE_ANIM_GET_DICTIONARY(ROULETTE_ANIM_DICT_PROP)

	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_PLAY_CLIP_ON_TABLE_WHEEL",
	" - playing ", strAnimClip)	
	
	PLAY_ENTITY_ANIM(objWheel, strAnimClip,	strAnimDict, INSTANT_BLEND_IN,
	bLoop, bHoldLastFrame, DEFAULT, fStartPhase, ENUM_TO_INT(AF_FORCE_START))
	FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(objWheel)
	
	sSpin.strWheelLastAnim = strAnimClip
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Returns how many slots the wheel is rotated by when the outro anim for the given anim index ends
FUNC INT ROULETTE_ANIM_GET_OUTRO_SLOT_OFFSET_FOR_SLOT_ANIM_INDEX(INT iAnimIndex)
	SWITCH iAnimIndex
		CASE 1 RETURN 3
		CASE 2 RETURN 7
		CASE 3 RETURN 4
		CASE 4 RETURN 9
		CASE 5 RETURN 1
		CASE 6 RETURN 2
		CASE 7 RETURN 6
		CASE 8 RETURN 8
		CASE 9 RETURN 5
		CASE 10 RETURN 3
		CASE 11 RETURN 7
		CASE 12 RETURN 4
		CASE 13 RETURN 9
		CASE 14 RETURN 1
		CASE 15 RETURN 2
		CASE 16 RETURN 6
		CASE 17 RETURN 8
		CASE 18 RETURN 5
		CASE 19 RETURN 3
		CASE 20 RETURN 7
		CASE 21 RETURN 4
		CASE 22 RETURN 9
		CASE 23 RETURN 1
		CASE 24 RETURN 2
		CASE 25 RETURN 6
		CASE 26 RETURN 8
		CASE 27 RETURN 5
		CASE 28 RETURN 3
		CASE 29 RETURN 7
		CASE 30 RETURN 4
		CASE 31 RETURN 9
		CASE 32 RETURN 1
		CASE 33 RETURN 2
		CASE 34 RETURN 6
		CASE 35 RETURN 8
		CASE 36 RETURN 5
		CASE 37 RETURN 3
		CASE 38 RETURN 7		
	ENDSWITCH
	RETURN 0
ENDFUNC

/// PURPOSE:
///    Calculates the phase at which to start an additive intro based on the previous outro animation
FUNC FLOAT ROULETTE_ANIM_CALCULATE_ADDITIVE_PHASE_FROM_OUTRO_ANIM_INDEX(INT iOutroAnimIndex)
	INT iSlotOffset = ROULETTE_ANIM_GET_OUTRO_SLOT_OFFSET_FOR_SLOT_ANIM_INDEX(iOutroAnimIndex)	
	RETURN (1.0 / ROULETTE_MAX_ANIM_INDEX) * iSlotOffset		
ENDFUNC

/// PURPOSE:
///    Plays the wheel intro animation additively on the given table
FUNC BOOL ROULETTE_ANIM_PLAY_ADDITIVE_INTRO_CLIP_ON_TABLE_WHEEL(ROULETTE_WHEEL_SPIN &sSpin, FLOAT fStartPhase = 0.0)
	IF NOT HAS_ANIM_DICT_LOADED(ROULETTE_ANIM_GET_DICTIONARY(ROULETTE_ANIM_DICT_PROP))
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_PLAY_ADDITIVE_INTRO_CLIP_ON_TABLE_WHEEL - Dictionary not loaded")
		RETURN FALSE
	ENDIF
	
	OBJECT_INDEX objWheel = sSpin.objWheel
	IF NOT DOES_ENTITY_EXIST(objWheel)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_PLAY_ADDITIVE_INTRO_CLIP_ON_TABLE_WHEEL - table entity doesn't exist")
		RETURN FALSE
	ENDIF
	
	IF NOT DOES_ENTITY_HAVE_DRAWABLE(objWheel)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_PLAY_ADDITIVE_INTRO_CLIP_ON_TABLE_WHEEL - table entity doesn't have drawable")
		RETURN FALSE
	ENDIF
	
	STRING strAnimDict = ROULETTE_ANIM_GET_DICTIONARY(ROULETTE_ANIM_DICT_PROP)
	
	// Base loop anim
	ANIM_DATA lowPriority
	lowPriority.anim0 = ROULETTE_ANIM_GET_PROP_CLIP(ROULETTE_ANIM_CLIP_PROP_WHEEL_SPIN_LOOP)
	lowPriority.dictionary0 = strAnimDict
	lowPriority.blendInDelta = INSTANT_BLEND_IN
	lowPriority.blendOutDelta = INSTANT_BLEND_OUT
	lowPriority.flags = AF_FORCE_START
	lowPriority.type = APT_SINGLE_ANIM
	lowPriority.rate0 = 0
	
	lowPriority.phase0 = ROULETTE_ANIM_CALCULATE_ADDITIVE_PHASE_FROM_OUTRO_ANIM_INDEX(sSpin.sData.iLastWheelOutroAnimIndex)
	
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_PLAY_ADDITIVE_INTRO_CLIP_ON_TABLE_WHEEL",
	" - last outro: ", sSpin.sData.iLastWheelOutroAnimIndex,
	" setting phase: ", lowPriority.phase0)
	
	// Additive intro anim
	ANIM_DATA medPriority
	medPriority.anim0 = ROULETTE_ANIM_GET_PROP_CLIP(ROULETTE_ANIM_CLIP_PROP_WHEEL_SPIN_INTRO)
	medPriority.dictionary0 = strAnimDict
	medPriority.blendInDelta = INSTANT_BLEND_IN
	medPriority.blendOutDelta = INSTANT_BLEND_OUT
	medPriority.flags = AF_ADDITIVE
	medPriority.type = APT_SINGLE_ANIM
	medPriority.phase0 = fStartPhase
	
	ANIM_DATA highPriority	
	PLAY_ENTITY_SCRIPTED_ANIM(objWheel, lowPriority, medPriority, highPriority)
	FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(objWheel)
	
	sSpin.strWheelLastAnim = ROULETTE_ANIM_GET_PROP_CLIP(ROULETTE_ANIM_CLIP_PROP_WHEEL_SPIN_INTRO)
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Plays the wheel spin loop clip on the given tables wheel
FUNC BOOL ROULETTE_ANIM_PLAY_WHEEL_PROP_LOOP_CLIP_ON_TABLE(ROULETTE_WHEEL_SPIN &sSpin, FLOAT fStartPhase = 0.0)
	STRING strAnimClip = ROULETTE_ANIM_GET_PROP_CLIP(ROULETTE_ANIM_CLIP_PROP_WHEEL_SPIN_LOOP)
	RETURN ROULETTE_ANIM_PLAY_CLIP_ON_TABLE_WHEEL(sSpin, strAnimClip, TRUE, FALSE, fStartPhase)
ENDFUNC

/// PURPOSE:
///    Plays the wheel spin outro clip on the given tables wheel, taking into account which number it needs to
///    stop on given the winning number for the table
FUNC BOOL ROULETTE_ANIM_PLAY_WHEEL_PROP_OUTRO_CLIP_ON_TABLE(ROULETTE_WHEEL_SPIN &sSpin, INT iExitVariationToPlay, FLOAT fStartPhase = 0.0)		
	STRING strAnimClip = ROULETTE_ANIM_GET_PROP_CLIP(ROULETTE_ANIM_CLIP_PROP_WHEEL_SPIN_OUTRO, iExitVariationToPlay)	
	RETURN ROULETTE_ANIM_PLAY_CLIP_ON_TABLE_WHEEL(sSpin, strAnimClip, FALSE, TRUE, fStartPhase)
ENDFUNC


///--------------------------------
///    Anim clip playing - Ball
///--------------------------------

/// PURPOSE:
///    Forces an anim update on the ball
PROC ROULETTE_ANIM_FORCE_ANIM_UPDATE_ON_TABLE_BALL(ROULETTE_WHEEL_SPIN &sSpin)
	OBJECT_INDEX objBall = sSpin.objBall
	
	IF NOT DOES_ENTITY_EXIST(objBall)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_FORCE_ANIM_UPDATE_ON_TABLE_BALL - objBall doesn't exist")
		EXIT
	ENDIF
	
	IF NOT DOES_ENTITY_HAVE_DRAWABLE(objBall)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_FORCE_ANIM_UPDATE_ON_TABLE_BALL - objBall doesn't have drawable")
		EXIT
	ENDIF
	
	FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(objBall)
ENDPROC

/// PURPOSE:
///    Plays a roulette animation clip on the ball for the given table
FUNC BOOL ROULETTE_ANIM_PLAY_CLIP_ON_TABLE_BALL(ROULETTE_WHEEL_SPIN &sSpin,
STRING strAnimClip, BOOL bLoop, BOOL bHoldLastFrame, FLOAT fStartPhase = 0.0)				
	
	IF NOT HAS_ANIM_DICT_LOADED(ROULETTE_ANIM_GET_DICTIONARY(ROULETTE_ANIM_DICT_PROP))
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_PLAY_CLIP_ON_TABLE_BALL - Dictionary not loaded")
		RETURN FALSE
	ENDIF
	
	OBJECT_INDEX objBall = sSpin.objBall
	
	IF NOT DOES_ENTITY_EXIST(objBall)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_PLAY_CLIP_ON_TABLE_BALL - objBall doesn't exist")
		RETURN FALSE
	ENDIF
	
	IF NOT DOES_ENTITY_HAVE_DRAWABLE(objBall)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_PLAY_CLIP_ON_TABLE_BALL - objBall doesn't have drawable")
		RETURN FALSE
	ENDIF
	
	STRING strAnimDict = ROULETTE_ANIM_GET_DICTIONARY(ROULETTE_ANIM_DICT_PROP)	
	
	ROULETTE_PROPS_INIT_BALL_FOR_ANIMATION(sSpin)
	
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_PLAY_CLIP_ON_TABLE_BALL - playing: ", strAnimClip)
	PLAY_ENTITY_ANIM(objBall, strAnimClip, strAnimDict, INSTANT_BLEND_IN, bLoop, bHoldLastFrame, DEFAULT, fStartPhase,
	ENUM_TO_INT(AF_TURN_OFF_COLLISION | AF_OVERRIDE_PHYSICS | AF_FORCE_START | AF_EXTRACT_INITIAL_OFFSET))
	
	sSpin.strBallLastAnim = strAnimClip
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Sets the ball loop animations play speed
PROC ROULETTE_ANIM_SET_BALL_LOOP_ANIM_PLAY_SPEED(ROULETTE_WHEEL_SPIN &sSpin, FLOAT fPlaySpeed)
	OBJECT_INDEX objBall = sSpin.objBall			
	
	IF NOT DOES_ENTITY_EXIST(objBall)
		EXIT
	ENDIF
	
	STRING strAnimDict = ROULETTE_ANIM_GET_DICTIONARY(ROULETTE_ANIM_DICT_PROP)
	STRING strAnimClip = ROULETTE_ANIM_GET_PROP_CLIP(ROULETTE_ANIM_CLIP_PROP_BALL_SPIN_LOOP)
	
	IF IS_ENTITY_PLAYING_ANIM(objBall, strAnimDict, strAnimClip)
		SET_ENTITY_ANIM_SPEED(objBall, strAnimDict, strAnimClip, fPlaySpeed)
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets the play speed of the current wheel animation 
PROC ROULETTE_ANIM_SET_WHEEL_LOOP_ANIM_PLAY_SPEED(ROULETTE_WHEEL_SPIN &sSpin, FLOAT fPlaySpeed)
	OBJECT_INDEX objWheel = sSpin.objWheel			
	
	IF NOT DOES_ENTITY_EXIST(objWheel)
		EXIT
	ENDIF
	
	STRING strAnimDict = ROULETTE_ANIM_GET_DICTIONARY(ROULETTE_ANIM_DICT_PROP)
	STRING strAnimClip = ROULETTE_ANIM_GET_PROP_CLIP(ROULETTE_ANIM_CLIP_PROP_WHEEL_SPIN_LOOP)
	
	IF IS_ENTITY_PLAYING_ANIM(objWheel, strAnimDict, strAnimClip)
		SET_ENTITY_ANIM_SPEED(objWheel, strAnimDict, strAnimClip, fPlaySpeed)
	ENDIF
ENDPROC

/// PURPOSE:
///    Plays the ball spin intro clip on the given tables ball
FUNC BOOL ROULETTE_ANIM_PLAY_BALL_PROP_INTRO_CLIP_ON_TABLE(ROULETTE_WHEEL_SPIN &sSpin, FLOAT fStartPhase = 0.0)
	STRING strAnim = ROULETTE_ANIM_GET_PROP_CLIP(ROULETTE_ANIM_CLIP_PROP_BALL_SPIN_INTRO)
	RETURN ROULETTE_ANIM_PLAY_CLIP_ON_TABLE_BALL(sSpin, strAnim, FALSE, TRUE, fStartPhase)
ENDFUNC

/// PURPOSE:
///    Releases the sound ID used by the ball loop sound
PROC ROULETTE_AUDIO_RELEASE_SFX_BALL_LOOP(ROULETTE_WHEEL_SPIN &sSpin)
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_AUDIO_RELEASE_SFX_BALL_LOOP - Released")
	RELEASE_SOUND_ID(sSpin.iBallLoopSoundID)
	sSpin.iBallLoopSoundID = -1
ENDPROC

/// PURPOSE:
///    Stops the ball looping sound
PROC ROULETTE_AUDIO_STOP_SFX_BALL_LOOP(ROULETTE_WHEEL_SPIN &sSpin)
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_AUDIO_STOP_SFX_BALL_LOOP - Called")
	IF sSpin.iBallLoopSoundID = -1
		EXIT
	ENDIF
	
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_AUDIO_STOP_SFX_BALL_LOOP - Stopping")
	STOP_SOUND(sSpin.iBallLoopSoundID)
	ROULETTE_AUDIO_RELEASE_SFX_BALL_LOOP(sSpin)
ENDPROC

/// PURPOSE:
///    Plays the looping audio on the ball  
PROC ROULETTE_AUDIO_PLAY_SFX_BALL_LOOP(ROULETTE_WHEEL_SPIN &sSpin)
	IF sSpin.iBallLoopSoundID > -1 AND NOT HAS_SOUND_FINISHED(sSpin.iBallLoopSoundID)
		EXIT
	ENDIF
	
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_AUDIO_PLAY_SFX_BALL_LOOP - Playing")	
	ROULETTE_AUDIO_RELEASE_SFX_BALL_LOOP(sSpin)
	INT iSoundID = GET_SOUND_ID()
	PLAY_SOUND_FROM_ENTITY(iSoundID, ROULETTE_AUDIO_GET_CLIP(RAC_SFX_BALL_LOOP),
	sSpin.objBall, ROULETTE_AUDIO_GET_SOUND_SET(RAS_TABLE_GAME_SOUNDS))
	sSpin.iBallLoopSoundID = iSoundID
ENDPROC

/// PURPOSE:
///    Plays the ball spin loop clip on the given tables ball
FUNC BOOL ROULETTE_ANIM_PLAY_BALL_PROP_LOOP_CLIP_ON_TABLE(ROULETTE_WHEEL_SPIN &sSpin, FLOAT fStartPhase = 0.0)
	STRING strAnim = ROULETTE_ANIM_GET_PROP_CLIP(ROULETTE_ANIM_CLIP_PROP_BALL_SPIN_LOOP)
	ROULETTE_AUDIO_PLAY_SFX_BALL_LOOP(sSpin)	
	RETURN ROULETTE_ANIM_PLAY_CLIP_ON_TABLE_BALL(sSpin, strAnim, TRUE, FALSE, fStartPhase)
ENDFUNC

/// PURPOSE:
///    
PROC ROULETTE_AUDIO_PLAY_SFX_BALL_OUTRO(ROULETTE_WHEEL_SPIN &sSpin, INT iExitVariation)
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_AUDIO_PLAY_SFX_BALL_OUTRO - Playing")

	PLAY_SOUND_FROM_ENTITY(-1, ROULETTE_AUDIO_GET_CLIP(RAC_SFX_BALL_EXIT, iExitVariation), 
	sSpin.objBall, ROULETTE_AUDIO_GET_SOUND_SET(RAS_BALL_EXIT_SOUNDS))
ENDPROC

/// PURPOSE:
///    Plays the ball spin outro clip on the given tables ball, taking into account which number it needs to
///    land on given the winning number for the table
FUNC BOOL ROULETTE_ANIM_PLAY_BALL_PROP_OUTRO_CLIP_ON_TABLE(ROULETTE_WHEEL_SPIN &sSpin, INT iExitVariationToPlay, FLOAT fStartPhase = 0.0)
	STRING strAnim = ROULETTE_ANIM_GET_PROP_CLIP(ROULETTE_ANIM_CLIP_PROP_BALL_SPIN_OUTRO, iExitVariationToPlay)
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_PLAY_BALL_PROP_OUTRO_CLIP_ON_TABLE - outro anim: ", strAnim)	
	ROULETTE_AUDIO_STOP_SFX_BALL_LOOP(sSpin)
	ROULETTE_AUDIO_PLAY_SFX_BALL_OUTRO(sSpin, iExitVariationToPlay)
	RETURN ROULETTE_ANIM_PLAY_CLIP_ON_TABLE_BALL(sSpin, strAnim, FALSE, TRUE, fStartPhase)
ENDFUNC


///-------------------------------------------------------
///    Anim clip playing - Spin (wheel and ball together)
///-------------------------------------------------------

/// PURPOSE:
///    Starts both the ball and wheel spin intro animation clips on the given table
FUNC BOOL ROULETTE_ANIM_PLAY_WHEEL_AND_BALL_INTRO_CLIPS_FOR_TABLE(ROULETTE_WHEEL_SPIN &sSpin)
	RETURN ROULETTE_ANIM_PLAY_BALL_PROP_INTRO_CLIP_ON_TABLE(sSpin) 
	AND	ROULETTE_ANIM_PLAY_ADDITIVE_INTRO_CLIP_ON_TABLE_WHEEL(sSpin)
ENDFUNC

/// PURPOSE:
///    Plays the loop animations on both the wheel and the ball
PROC ROULETTE_ANIM_PLAY_WHEEL_AND_BALL_LOOP_CLIPS_FOR_TABLE(ROULETTE_WHEEL_SPIN &sSpin)
	ROULETTE_ANIM_PLAY_BALL_PROP_LOOP_CLIP_ON_TABLE(sSpin)
	ROULETTE_ANIM_PLAY_WHEEL_PROP_LOOP_CLIP_ON_TABLE(sSpin)
ENDPROC

/// PURPOSE:
///    Starts both the ball and wheel spin outro animation clip on the given table
PROC ROULETTE_ANIM_PLAY_WHEEL_AND_BALL_OUTRO_CLIPS_FOR_TABLE(ROULETTE_WHEEL_SPIN &sSpin, INT iExitVariationToPlay, FLOAT fStartPhase = 0.0)
	ROULETTE_ANIM_PLAY_BALL_PROP_OUTRO_CLIP_ON_TABLE(sSpin, iExitVariationToPlay, fStartPhase)
	ROULETTE_ANIM_PLAY_WHEEL_PROP_OUTRO_CLIP_ON_TABLE(sSpin, iExitVariationToPlay, fStartPhase)
ENDPROC


///-------------------------------------------------------
///    Anim clip duration getters
///-------------------------------------------------------

/// PURPOSE:
///    Gets the balls intro animation duration in seconds
FUNC FLOAT ROULETTE_ANIM_GET_BALL_INTRO_ANIM_DURATION()
	STRING strAnimLoopClip = ROULETTE_ANIM_GET_PROP_CLIP(ROULETTE_ANIM_CLIP_PROP_BALL_SPIN_INTRO)
	STRING strAnimDict = ROULETTE_ANIM_GET_DICTIONARY(ROULETTE_ANIM_DICT_PROP)	
	RETURN GET_ANIM_DURATION(strAnimDict, strAnimLoopClip)
ENDFUNC

/// PURPOSE:
///    Gets the balls loop animation duration in seconds     
FUNC FLOAT ROULETTE_ANIM_GET_BALL_LOOP_ANIM_DURATION()
	STRING strAnimLoopClip = ROULETTE_ANIM_GET_PROP_CLIP(ROULETTE_ANIM_CLIP_PROP_BALL_SPIN_LOOP)
	STRING strAnimDict = ROULETTE_ANIM_GET_DICTIONARY(ROULETTE_ANIM_DICT_PROP)	
	RETURN GET_ANIM_DURATION(strAnimDict, strAnimLoopClip)
ENDFUNC

/// PURPOSE:
///    Gets the wheels intro animation duration in seconds
FUNC FLOAT ROULETTE_ANIM_GET_WHEEL_INTRO_ANIM_DURATION()
	STRING strAnimLoopClip = ROULETTE_ANIM_GET_PROP_CLIP(ROULETTE_ANIM_CLIP_PROP_WHEEL_SPIN_INTRO)
	STRING strAnimDict = ROULETTE_ANIM_GET_DICTIONARY(ROULETTE_ANIM_DICT_PROP)	
	RETURN GET_ANIM_DURATION(strAnimDict, strAnimLoopClip)
ENDFUNC

/// PURPOSE:
///    Gets the wheels loop animation duration in seconds  
FUNC FLOAT ROULETTE_ANIM_GET_WHEEL_LOOP_ANIM_DURATION()
	STRING strAnimLoopClip = ROULETTE_ANIM_GET_PROP_CLIP(ROULETTE_ANIM_CLIP_PROP_WHEEL_SPIN_LOOP)
	STRING strAnimDict = ROULETTE_ANIM_GET_DICTIONARY(ROULETTE_ANIM_DICT_PROP)	
	RETURN GET_ANIM_DURATION(strAnimDict, strAnimLoopClip)
ENDFUNC

///-------------------------------------------------------
///    Spin states
///-------------------------------------------------------

/// PURPOSE:
///    Checks if the roulette wheel missed aligning with its starting rotation between frames    
FUNC BOOL ROULETTE_ANIM_HAS_WHEEL_MISSED_START_ALIGNMENT_BETWEEN_FRAMES(ROULETTE_WHEEL_SPIN &sSpin, FLOAT fCurrentZRotation)
	BOOL bLastFrameRotationBeforeThreshold = sSpin.sData.fRotationLastFrame > ROULETTE_ANIM_WHEEL_INITIAL_LOCAL_Z_ROTATION + ADDITIVE_INTRO_ROTATION_SYNC_THRESHOLD
	BOOL bCurrentRotationAboveThreshold = fCurrentZRotation < ROULETTE_ANIM_WHEEL_INITIAL_LOCAL_Z_ROTATION - ADDITIVE_INTRO_ROTATION_SYNC_THRESHOLD
	BOOL bRotationPositive = fCurrentZRotation > 0
	RETURN bLastFrameRotationBeforeThreshold AND bCurrentRotationAboveThreshold AND bRotationPositive
ENDFUNC

/// PURPOSE:
///    Checks if the wheel has aligned with its starting position
FUNC BOOL ROULETTE_ANIM_HAS_WHEEL_ALIGNED_WITH_STARTING_ROTATION(ROULETTE_WHEEL_SPIN &sSpin, FLOAT fCurrentZRotation)	
	BOOL bMissedThresholdBetweenFrames = ROULETTE_ANIM_HAS_WHEEL_MISSED_START_ALIGNMENT_BETWEEN_FRAMES(sSpin, fCurrentZRotation)
	FLOAT fStartZRotation = ROULETTE_ANIM_WHEEL_INITIAL_LOCAL_Z_ROTATION
	BOOL bInSyncThreshold = ABSF(fStartZRotation - fCurrentZRotation) <= ADDITIVE_INTRO_ROTATION_SYNC_THRESHOLD
	
	IF bInSyncThreshold
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_HAS_WHEEL_ALIGNED_WITH_STARTING_ROTATION",
		" - In alignment threshold")
	ENDIF
		
	IF bMissedThresholdBetweenFrames
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_HAS_WHEEL_ALIGNED_WITH_STARTING_ROTATION", 
		" - Missed alignment threshold between frames!",
		 " LastRotation: ", sSpin.sData.fRotationLastFrame , " CurrentRotation: ", fCurrentZRotation)
	ENDIF
	
	RETURN bInSyncThreshold OR bMissedThresholdBetweenFrames 
ENDFUNC

/// PURPOSE:
///    Flash hides the ball prop
PROC ROULETTE_ANIM_UPDATE_BALL_FLASH_HIDING(ROULETTE_WHEEL_SPIN &sSpin)
	
	IF NOT sSpin.bFlashFadeBall
		EXIT
	ENDIF
	
	// First spin so the ball doesn't need hiding
	IF sSpin.sData.iLastWheelOutroAnimIndex = -1
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_UPDATE_BALL_FLASH_HIDING - no flash required") 
		EXIT
	ENDIF
	
	IF FLASH_FADE_OBJECT(sSpin.sBallFlashFade, sSpin.objBall)
		sSpin.bFlashFadeBall = FALSE
	ENDIF
ENDPROC

/// PURPOSE:
///    Updates the wheel spin sequence intro state
PROC ROULETTE_ANIM_UPDATE_WHEEL_SPIN_SEQUENCE_INTRO_STATE(ROULETTE_WHEEL_SPIN &sSpin)	
	
	IF sSpin.sData.fSpinTimeMS >= ROULETTE_AUDIO_BALL_LOOP_SOUND_START_TIME_MS
		ROULETTE_AUDIO_PLAY_SFX_BALL_LOOP(sSpin)
	ENDIF
	
	// Show ball on intro event
	IF ROULETTE_ANIM_HAS_BALL_EVENT_TRIGGERED(sSpin, GET_HASH_KEY("SHOW_BALL"))
		SET_ENTITY_VISIBLE(sSpin.objBall, TRUE)
		RESET_ENTITY_ALPHA(sSpin.objBall)
		sSpin.bFlashFadeBall = FALSE
		FLASH_FADE_RESET(sSpin.sBallFlashFade)
	ELSE
		ROULETTE_ANIM_UPDATE_BALL_FLASH_HIDING(sSpin)
	ENDIF
	
	VECTOR vWheelCurrentRot = ROULETTE_PROPS_GET_WHEEL_CURRENT_LOCAL_ROTATION(sSpin.objWheel)
	FLOAT fCurrentZRotation = vWheelCurrentRot.z
	
	// Not playing the intro anim 
	IF NOT IS_ENTITY_PLAYING_ANIM(sSpin.objWheel,
	ROULETTE_ANIM_GET_DICTIONARY(ROULETTE_ANIM_DICT_PROP),
	ROULETTE_ANIM_GET_PROP_CLIP(ROULETTE_ANIM_CLIP_PROP_WHEEL_SPIN_INTRO))
		EXIT
	ENDIF
		
	// Additive intro is not in loop so leave
	IF NOT ROULETTE_ANIM_HAS_WHEEL_EVENT_TRIGGERED(sSpin.objWheel, GET_HASH_KEY("default_loop_position"))
		sSpin.sData.fRotationLastFrame = fCurrentZRotation
		EXIT
	ENDIF
	
	BOOL bAnimFinished = ROULETTE_ANIM_HAS_WHEEL_AT_TABLE_FINISHED_PLAYING_LAST_CLIP(sSpin)
	
	// In loop but has not yet synced with wheel start rot
	IF ROULETTE_ANIM_HAS_WHEEL_ALIGNED_WITH_STARTING_ROTATION(sSpin, fCurrentZRotation)
	OR bAnimFinished
	#IF IS_DEBUG_BUILD 
	OR IS_DEBUG_KEY_JUST_PRESSED(KEY_F, KEYBOARD_MODIFIER_NONE, "Skip wheel intro")
	#ENDIF
	
		IF bAnimFinished
			CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_UPDATE_WHEEL_SPIN_SEQUENCE_FOR_TABLE",
			" - INTRO - Animation finished without sync")
		ENDIF
			
		/// Transition to loop anims but only play wheel loop because
		/// the ball may not be finished with its intro
		ROULETTE_ANIM_PLAY_WHEEL_PROP_LOOP_CLIP_ON_TABLE(sSpin)
		sSpin.sData.iLastWheelIntroDurationMS = ROUND(sSpin.sData.fSpinTimeMS)		
		sSpin.sData.eWheelSpinState = ROULETTE_ANIM_CLIP_PROP_WHEEL_SPIN_LOOP
	ENDIF
	
	// Store last frames wheel rotation
	sSpin.sData.fRotationLastFrame = fCurrentZRotation
ENDPROC

/// PURPOSE:
///    Gets the time at which the all the wheel spins loops are finished
FUNC INT ROULETTE_ANIM_GET_LOOPING_END_TIME_MS(ROULETTE_WHEEL_SPIN &sSpin)
	
	INT fWheelActualIntroDurationMS = sSpin.sData.iLastWheelIntroDurationMS
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_GET_LOOPING_END_TIME_MS - ",
	"fWheelActualIntroDurationMS: ", fWheelActualIntroDurationMS)
	
	FLOAT fWheelLoopAnimDurationMS = ROULETTE_ANIM_GET_WHEEL_LOOP_ANIM_DURATION() * 1000
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_GET_LOOPING_END_TIME_MS - ",
	"fWheelLoopAnimDurationMS: ", fWheelLoopAnimDurationMS)
	
	FLOAT fTotalLoopingDuration = sSpin.sData.iTargetLoopCount * fWheelLoopAnimDurationMS
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_GET_LOOPING_END_TIME_MS - ",
	"fTotalLoopingDuration: ", fTotalLoopingDuration)
	
	// Add on how long the intro was
	FLOAT fLoopingEndTime = fWheelActualIntroDurationMS + fTotalLoopingDuration
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_GET_LOOPING_END_TIME_MS - ",
	"fLoopingEndTime: ", fLoopingEndTime)
	
	INT iLoopingEndTime = ROUND(fLoopingEndTime)
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_GET_LOOPING_END_TIME_MS - ",
	"iLoopingEndTime: ", iLoopingEndTime)
	
	RETURN iLoopingEndTime
ENDFUNC

/// PURPOSE:
///    Checks if the wheel has finished the required number of loops for the given table   
FUNC BOOL ROULETTE_ANIM_HAS_WHEEL_FINISHED_REQUIRED_LOOPS(ROULETTE_WHEEL_SPIN &sSpin)
	
	#IF IS_DEBUG_BUILD
		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_F, KEYBOARD_MODIFIER_NONE, "Skip wheel spin") 
			RETURN TRUE
		ENDIF
	#ENDIF	
	
	INT iLoopEndTime = ROULETTE_ANIM_GET_LOOPING_END_TIME_MS(sSpin)
	FLOAT iTimeRemaining = sSpin.sData.fSpinTimeMS
	
	#IF IS_DEBUG_BUILD		
		TEXT_LABEL_31 tlTimeLeft
		tlTimeLeft = "Time to outro: "
		tlTimeLeft += GET_STRING_FROM_FLOAT(iLoopEndTime - iTimeRemaining)
		DRAW_DEBUG_TEXT_ABOVE_COORDS(ROULETTE_PROPS_GET_WHEEL_BONE_POSITION(sSpin.objWheel),
		GET_STRING_FROM_TL(tlTimeLeft), 0.2, 0, 255, 0)
	#ENDIF
	
	IF iTimeRemaining >= iLoopEndTime
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Gets the required play rate the ball has to be for it to sync up with the
///    wheel just before the outro
FUNC FLOAT ROULETTE_ANIM_GET_REQUIRED_PLAY_RATE_FOR_BALL(ROULETTE_WHEEL_SPIN &sSpin)
	// Inputs
	INT iLastWheelIntroDuration = sSpin.sData.iLastWheelIntroDurationMS
	INT iLastBallIntroDuration = sSpin.sData.iLastBallIntroDurationMS
	FLOAT fWheelLoopAnimDuration = ROULETTE_ANIM_GET_WHEEL_LOOP_ANIM_DURATION() * 1000
	FLOAT fBallLoopAnimDuration = ROULETTE_ANIM_GET_BALL_LOOP_ANIM_DURATION() * 1000
	
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_GET_REQUIRED_PLAY_RATE_FOR_BALL")
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_GET_REQUIRED_PLAY_RATE_FOR_BALL - INPUTS-----------")
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_GET_REQUIRED_PLAY_RATE_FOR_BALL - iLastWheelIntroDuration: ", iLastWheelIntroDuration)
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_GET_REQUIRED_PLAY_RATE_FOR_BALL - iLastBallIntroDuration: ", iLastBallIntroDuration)
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_GET_REQUIRED_PLAY_RATE_FOR_BALL - fWheelLoopAnimDuration: ", fWheelLoopAnimDuration)
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_GET_REQUIRED_PLAY_RATE_FOR_BALL - fBallLoopAnimDuration: ", fBallLoopAnimDuration)
	
	FLOAT fWheelLoopingDuration = fWheelLoopAnimDuration * sSpin.sData.iTargetLoopCount
	FLOAT iWheelLoopingEndTime = iLastWheelIntroDuration + fWheelLoopingDuration
	
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_GET_REQUIRED_PLAY_RATE_FOR_BALL - OUTPUTS-----------")
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_GET_REQUIRED_PLAY_RATE_FOR_BALL - fWheelLoopingDuration: ", fWheelLoopingDuration)
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_GET_REQUIRED_PLAY_RATE_FOR_BALL - iWheelLoopingEndTime: ", iWheelLoopingEndTime)
	
	// Ball slow down 
	FLOAT fBallTimeToWheelLoopEnd = iWheelLoopingEndTime - iLastBallIntroDuration
	FLOAT fBallLoopsUntilWheelLoopEnd = fBallTimeToWheelLoopEnd / fBallLoopAnimDuration 
	INT iBallLoopsRoundUpAdjusted = CEIL(fBallLoopsUntilWheelLoopEnd) - 1
	FLOAT fBallLoopingDuration = fBallLoopAnimDuration * iBallLoopsRoundUpAdjusted
	FLOAT fBallLoopEndTime = iLastBallIntroDuration + fBallLoopingDuration
	FLOAT fBallLoopRateScale = 1.0 /(fBallTimeToWheelLoopEnd / fBallLoopingDuration)
	
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_GET_REQUIRED_PLAY_RATE_FOR_BALL - RATE CALC-----------")
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_GET_REQUIRED_PLAY_RATE_FOR_BALL - fBallTimeToWheelLoopEnd: ", fBallTimeToWheelLoopEnd)
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_GET_REQUIRED_PLAY_RATE_FOR_BALL - fBallLoopsUntilWheelLoopEnd: ", fBallLoopsUntilWheelLoopEnd)
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_GET_REQUIRED_PLAY_RATE_FOR_BALL - iBallLoopsRoundUpAdjusted: ", iBallLoopsRoundUpAdjusted)
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_GET_REQUIRED_PLAY_RATE_FOR_BALL - iBallLoopingDuration: ", fBallLoopingDuration)
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_GET_REQUIRED_PLAY_RATE_FOR_BALL - fBallLoopEndTime: ", fBallLoopEndTime)
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_GET_REQUIRED_PLAY_RATE_FOR_BALL - fBallLoopRateScale: ", fBallLoopRateScale)
	
	RETURN fBallLoopRateScale
ENDFUNC

/// PURPOSE:
///    Slows down the roulette balls loop animation play rate so that it syncs up with the current
///    wheel loop animations phase. REturns true if the phases have synced
PROC ROULETTE_ANIM_SLOW_BALL_LOOP_TO_SYNC_WITH_WHEEL(ROULETTE_WHEEL_SPIN &sSpin)
	FLOAT fBallPlayRate = 1.0
	
	/// If still looping we have not synced yet and must use adjusted play rate
	/// for ball
	IF NOT ROULETTE_ANIM_HAS_WHEEL_FINISHED_REQUIRED_LOOPS(sSpin)
		fBallPlayRate = ROULETTE_ANIM_GET_REQUIRED_PLAY_RATE_FOR_BALL(sSpin)
	ENDIF
	
	#IF IS_DEBUG_BUILD
		FLOAT fBallCurrentPhase = ROULETTE_ANIM_GET_BALLS_CURRENT_ANIMATIONS_PHASE(sSpin)
		FLOAT fWheelCurrentPhase = ROULETTE_ANIM_GET_WHEELS_CURRENT_ANIMATIONS_PHASE(sSpin)

		TEXT_LABEL_63 tlWheel
		tlWheel += "Wheel Phase: "
		tlWheel += GET_STRING_FROM_FLOAT(fWheelCurrentPhase)
		DRAW_DEBUG_TEXT_ABOVE_COORDS(ROULETTE_PROPS_GET_WHEEL_BONE_POSITION(sSpin.objWheel), GET_STRING_FROM_TL(tlWheel), 0.5, 0, 255)
		
		TEXT_LABEL_63 tlRandom
		tlRandom += "Total Loops: "
		tlRandom += GET_STRING_FROM_INT(sSpin.sData.iTargetLoopCount)
		DRAW_DEBUG_TEXT_ABOVE_COORDS(ROULETTE_PROPS_GET_WHEEL_BONE_POSITION(sSpin.objWheel), GET_STRING_FROM_TL(tlRandom), 0.4, 0, 255)
		
		TEXT_LABEL_63 tlBall
		tlBall += "Ball Phase: "
		tlBall += GET_STRING_FROM_FLOAT(fBallCurrentPhase)
		tlBall += "\nBall Rate: "
		tlBall += GET_STRING_FROM_FLOAT(fBallPlayRate)
		DRAW_DEBUG_TEXT_ABOVE_COORDS(ROULETTE_PROPS_GET_WHEEL_BONE_POSITION(sSpin.objWheel), GET_STRING_FROM_TL(tlBall), 0.3, 0, 255)	
	#ENDIF
	
	ROULETTE_ANIM_SET_BALL_LOOP_ANIM_PLAY_SPEED(sSpin, fBallPlayRate)
ENDPROC

/// PURPOSE:
///    Updates the wheel spin sequence loop state
FUNC BOOL ROULETTE_ANIM_UPDATE_WHEEL_SPIN_SEQUENCE_LOOP_STATE(ROULETTE_WHEEL_SPIN &sSpin)				
	
	// If looping anim slow to sync ball
	IF sSpin.sData.eBallSpinState = ROULETTE_ANIM_CLIP_PROP_BALL_SPIN_LOOP
		ROULETTE_ANIM_SLOW_BALL_LOOP_TO_SYNC_WITH_WHEEL(sSpin)
	ENDIF	
	
	/// If the loop count has met the number of rquired spins exit to outro as wheel
	/// and ball should now be in sync
	IF ROULETTE_ANIM_HAS_WHEEL_FINISHED_REQUIRED_LOOPS(sSpin)	
		
		// Start outro animation for target number	
		INT iTargetOutroAnimIndex = ROULETTE_GET_ANIMATION_INDEX_FOR_ROULETTE_NUMBER(sSpin.sData.iTargetNumber) 
		ROULETTE_ANIM_PLAY_WHEEL_AND_BALL_OUTRO_CLIPS_FOR_TABLE(sSpin, iTargetOutroAnimIndex)
		sSpin.sData.iLastWheelOutroAnimIndex = iTargetOutroAnimIndex
		
		// Transition to outro state
		sSpin.sData.eWheelSpinState = ROULETTE_ANIM_CLIP_PROP_WHEEL_SPIN_OUTRO
		sSpin.sData.eBallSpinState = ROULETTE_ANIM_CLIP_PROP_BALL_SPIN_OUTRO
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_UPDATE_WHEEL_SPIN_SEQUENCE_FOR_TABLE",
		" - playing spin outro, ", "iTargetNumber: ", sSpin.sData.iTargetNumber,
		" iTargetOutroAnimIndex: ", iTargetOutroAnimIndex)
		RETURN TRUE
	ENDIF
	
	// If ball has finished intro play the loop
	IF ROULETTE_ANIM_HAS_BALL_AT_TABLE_FINISHED_PLAYING_LAST_CLIP(sSpin)
		IF sSpin.sData.eBallSpinState = ROULETTE_ANIM_CLIP_PROP_BALL_SPIN_INTRO
			// Keep track of how long this took
			sSpin.sData.iLastBallIntroDurationMS = ROUND(sSpin.sData.fSpinTimeMS)
			ROULETTE_ANIM_PLAY_BALL_PROP_LOOP_CLIP_ON_TABLE(sSpin)
			sSpin.sData.eBallSpinState = ROULETTE_ANIM_CLIP_PROP_BALL_SPIN_LOOP
		ENDIF	
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Updates the wheel spin sequence outro state
FUNC BOOL ROULETTE_ANIM_UPDATE_WHEEL_SPIN_SEQUENCE_OUTRO_STATE(ROULETTE_WHEEL_SPIN &sSpin)	
	
	// Sequence finished when outro anim finishes so return true
	IF ROULETTE_ANIM_HAS_WHEEL_AT_TABLE_FINISHED_PLAYING_LAST_CLIP(sSpin)
		sSpin.sData.eWheelSpinState = ROULETTE_ANIM_CLIP_INVALID
		sSpin.sData.eBallSpinState = ROULETTE_ANIM_CLIP_INVALID
		sSpin.sData.iTargetNumber = -1
		sSpin.bFlashFadeBall = FALSE
		ROULETTE_AUDIO_RELEASE_SFX_BALL_LOOP(sSpin)
		FLASH_FADE_RESET(sSpin.sBallFlashFade)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_UPDATE_WHEEL_SPIN_SEQUENCE_OUTRO_STATE", 
		" has finished outro anim: ", sSpin.strWheelLastAnim)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Sets up the wheel spin with the required assets, 
///    objWheel is the roulette table with the wheel
PROC ROULETTE_ANIM_INIT_WHEEL_SPIN_ASSETS(ROULETTE_WHEEL_SPIN &sSpin, OBJECT_INDEX objWheel, OBJECT_INDEX objBall)
	sSpin.objWheel = objWheel
	sSpin.objBall = objBall
ENDPROC

/// PURPOSE:
///    Starts the wheel spin process, this should be called before updating the wheel spin and after the
///    wheel spin has been initialised
FUNC BOOL ROULETTE_ANIM_START_WHEEL_SPIN(ROULETTE_WHEEL_SPIN &sSpin, INT iTargetLoopCount, INT iTargetNumber)	
	
	IF NOT ROULETTE_ANIM_ARE_WHEEL_ASSETS_READY(sSpin)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_START_WHEEL_SPIN - ",
		"Can't start spin - Assets aren't ready")
		RETURN FALSE
	ENDIF
	
	// Init the ball
	ROULETTE_PROPS_INIT_BALL_FOR_ANIMATION(sSpin)
	
	sSpin.sData.iTargetLoopCount = IMAX(iTargetLoopCount, 0)
	
	// Keep hold of the target slot
	iTargetNumber = CLAMP_INT(iTargetNumber, 0, ROULETTE_MAX_ROULETTE_NUMBER)
	sSpin.sData.iTargetNumber = iTargetNumber
	
	// Start intro state
	ROULETTE_ANIM_PLAY_WHEEL_AND_BALL_INTRO_CLIPS_FOR_TABLE(sSpin)	
	sSpin.sData.eWheelSpinState = ROULETTE_ANIM_CLIP_PROP_WHEEL_SPIN_INTRO
	sSpin.sData.eBallSpinState = ROULETTE_ANIM_CLIP_PROP_BALL_SPIN_INTRO
	
	// Reset
	sSpin.sData.iLastWheelIntroDurationMS = 0
	sSpin.sData.iLastBallIntroDurationMS = 0
	sSpin.sData.fRotationLastFrame = 0
	sSpin.sData.fSpinTimeMS = 0
	
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_START_WHEEL_SPIN - Started")
	DEBUG_PRINTCALLSTACK()
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Checks if the current spins ball has landed in its winning slot
FUNC BOOL ROULETTE_ANIM_HAS_BALL_LANDED_IN_WINNING_SLOT(ROULETTE_WHEEL_SPIN &sSpin)
	RETURN ROULETTE_ANIM_HAS_BALL_EVENT_TRIGGERED(sSpin, GET_HASH_KEY("end_result"))
ENDFUNC

/// PURPOSE:
///    Starts flash fading out the roulette ball
PROC ROULETTE_ANIM_FLASH_FADE_OUT_BALL(ROULETTE_WHEEL_SPIN &sSpin)
	sSpin.bFlashFadeBall = TRUE
ENDPROC

/// PURPOSE:
///    Updates the wheel spin sequence, this must be called until the wheel spin ends
///    returns true when the wheel spin has finished
FUNC BOOL ROULETTE_ANIM_UPDATE_WHEEL_SPIN(ROULETTE_WHEEL_SPIN &sSpin)
	
	IF NOT ROULETTE_ANIM_ARE_WHEEL_ASSETS_READY(sSpin)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_UPDATE_WHEEL_SPIN - Wheel spin assets not ready!")
		RETURN FALSE
	ENDIF
	
	SWITCH sSpin.sData.eWheelSpinState
		CASE ROULETTE_ANIM_CLIP_PROP_WHEEL_SPIN_INTRO
			sSpin.sData.fSpinTimeMS += GET_FRAME_TIME() * 1000
			ROULETTE_ANIM_UPDATE_WHEEL_SPIN_SEQUENCE_INTRO_STATE(sSpin)	
		BREAK
		CASE ROULETTE_ANIM_CLIP_PROP_WHEEL_SPIN_LOOP
			sSpin.sData.fSpinTimeMS += GET_FRAME_TIME() * 1000
			ROULETTE_ANIM_UPDATE_WHEEL_SPIN_SEQUENCE_LOOP_STATE(sSpin)
		BREAK
		CASE ROULETTE_ANIM_CLIP_PROP_WHEEL_SPIN_OUTRO
			sSpin.sData.fSpinTimeMS += GET_FRAME_TIME() * 1000
			RETURN ROULETTE_ANIM_UPDATE_WHEEL_SPIN_SEQUENCE_OUTRO_STATE(sSpin)
		DEFAULT
			// At this point the wheel must be idle so we can do any flash fade outs at this point	
			ROULETTE_ANIM_UPDATE_BALL_FLASH_HIDING(sSpin)
		BREAK
	ENDSWITCH

	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Cleans up the roulette wheel
PROC ROULETTE_ANIM_WHEEL_CLEAN_UP(ROULETTE_WHEEL_SPIN &sSpin)
	ROULETTE_AUDIO_STOP_SFX_BALL_LOOP(sSpin)
	ROULETTE_AUDIO_RELEASE_SFX_BALL_LOOP(sSpin)
ENDPROC


///------------------------------
///    Wheel state skipping
///------------------------------    

/// PURPOSE:
///    Sets the ball and wheel to the idle state based on the current spin data
PROC ROULETTE_ANIM_INIT_WHEEL_ANIMS_TO_IDLE_STATE(ROULETTE_WHEEL_SPIN &sSpin)
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_INIT_WHEEL_ANIMS_TO_IDLE_STATE - Called")
	
	IF NOT ROULETTE_ANIM_ARE_WHEEL_ASSETS_READY(sSpin)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_INIT_WHEEL_ANIMS_TO_IDLE_STATE - ",
		"Assets weren't ready")
		EXIT
	ENDIF
	
	IF sSpin.sData.iLastWheelOutroAnimIndex <= -1
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_INIT_WHEEL_ANIMS_TO_IDLE_STATE - ",
		"No previous spin")
		EXIT
	ENDIF
	
	// Set phase at end so the ball is in its resting place
	ROULETTE_ANIM_PLAY_WHEEL_AND_BALL_OUTRO_CLIPS_FOR_TABLE(sSpin, sSpin.sData.iLastWheelOutroAnimIndex, 1.0)	
	FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(sSpin.objBall)
	FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(sSpin.objWheel)	
	SET_ENTITY_VISIBLE(sSpin.objBall, TRUE)
ENDPROC

/// PURPOSE:
///    Sets the ball and wheel to the intro state based on the current spin data
PROC ROULETTE_ANIM_INIT_WHEEL_ANIMS_TO_INTRO_STATE(ROULETTE_WHEEL_SPIN &sSpin)
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_INIT_WHEEL_ANIMS_TO_INTRO_STATE - Called")
	
	IF NOT ROULETTE_ANIM_ARE_WHEEL_ASSETS_READY(sSpin)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_INIT_WHEEL_ANIMS_TO_INTRO_STATE - ",
		"Assets weren't ready")
		EXIT
	ENDIF
	
	ROULETTE_ANIM_PLAY_BALL_PROP_INTRO_CLIP_ON_TABLE(sSpin, sSpin.sData.fBallPhase) 
	ROULETTE_ANIM_PLAY_ADDITIVE_INTRO_CLIP_ON_TABLE_WHEEL(sSpin, sSpin.sData.fWheelPhase)
	FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(sSpin.objBall)
	FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(sSpin.objWheel)	
ENDPROC

/// PURPOSE:
///    Sets the ball and wheel to the loop state based on the current spin data 
PROC ROULETTE_ANIM_INIT_WHEEL_ANIMS_TO_LOOP_STATE(ROULETTE_WHEEL_SPIN &sSpin)
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_INIT_WHEEL_ANIMS_TO_LOOP_STATE - Called")
	
	IF NOT ROULETTE_ANIM_ARE_WHEEL_ASSETS_READY(sSpin)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_INIT_WHEEL_ANIMS_TO_LOOP_STATE - ",
		"Assets weren't ready")
		EXIT
	ENDIF
	
	ROULETTE_ANIM_PLAY_BALL_PROP_LOOP_CLIP_ON_TABLE(sSpin, sSpin.sData.fBallPhase)
	ROULETTE_ANIM_PLAY_WHEEL_PROP_LOOP_CLIP_ON_TABLE(sSpin, sSpin.sData.fWheelPhase)
	FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(sSpin.objBall)
	FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(sSpin.objWheel)	
	SET_ENTITY_VISIBLE(sSpin.objBall, TRUE)
ENDPROC

/// PURPOSE:
///    Sets the ball and wheel to the outro state based on the current spin data
PROC ROULETTE_ANIM_INIT_WHEEL_ANIMS_TO_OUTRO_STATE(ROULETTE_WHEEL_SPIN &sSpin)
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_INIT_WHEEL_ANIMS_TO_OUTRO_STATE - Called")
	
	IF NOT ROULETTE_ANIM_ARE_WHEEL_ASSETS_READY(sSpin)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_INIT_WHEEL_ANIMS_TO_OUTRO_STATE - ",
		"Assets weren't ready")
		EXIT
	ENDIF
	
	INT iTargetOutroAnimIndex = ROULETTE_GET_ANIMATION_INDEX_FOR_ROULETTE_NUMBER(sSpin.sData.iTargetNumber) 
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_INIT_WHEEL_ANIMS_TO_OUTRO_STATE - ",
		"Target number: ", sSpin.sData.iTargetNumber,
		" Anim index: ", iTargetOutroAnimIndex)
	
	ROULETTE_ANIM_PLAY_BALL_PROP_OUTRO_CLIP_ON_TABLE(sSpin, iTargetOutroAnimIndex, sSpin.sData.fBallPhase)
	ROULETTE_ANIM_PLAY_WHEEL_PROP_OUTRO_CLIP_ON_TABLE(sSpin, iTargetOutroAnimIndex, sSpin.sData.fWheelPhase)
	FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(sSpin.objBall)
	FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(sSpin.objWheel)	
	SET_ENTITY_VISIBLE(sSpin.objBall, TRUE)
ENDPROC

/// PURPOSE:
///    Sets the wheel spin to the same state as the given spin data
PROC ROULETTE_ANIM_INIT_WHEEL_SPIN_WITH_SPIN_DATA(ROULETTE_WHEEL_SPIN &sSpin, ROULETTE_WHEEL_SPIN_DATA &sSpinData)
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_INIT_WHEEL_SPIN_WITH_SPIN_DATA - Called")
	
	// Copy spin data to wheel spin
	COPY_SCRIPT_STRUCT(sSpin.sData, sSpinData, SIZE_OF(ROULETTE_WHEEL_SPIN_DATA))
	
	// Init the wheel anims given the new data
	SWITCH sSpin.sData.eWheelSpinState
		CASE ROULETTE_ANIM_CLIP_INVALID 
			ROULETTE_ANIM_INIT_WHEEL_ANIMS_TO_IDLE_STATE(sSpin)
		BREAK
		CASE ROULETTE_ANIM_CLIP_PROP_WHEEL_SPIN_INTRO
			ROULETTE_ANIM_INIT_WHEEL_ANIMS_TO_INTRO_STATE(sSpin)
		BREAK
		CASE ROULETTE_ANIM_CLIP_PROP_WHEEL_SPIN_LOOP
			ROULETTE_ANIM_INIT_WHEEL_ANIMS_TO_LOOP_STATE(sSpin)	
		BREAK
		CASE ROULETTE_ANIM_CLIP_PROP_WHEEL_SPIN_OUTRO
			ROULETTE_ANIM_INIT_WHEEL_ANIMS_TO_OUTRO_STATE(sSpin)
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Gets the spin data form the wheel spin struct ensuring the latest values are retrieved  
FUNC ROULETTE_WHEEL_SPIN_DATA ROULETTE_ANIM_GET_WHEEL_SPIN_DATA(ROULETTE_WHEEL_SPIN &sSpin)
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_GET_WHEEL_SPIN_DATA - Called")
	
	IF NOT ROULETTE_ANIM_ARE_WHEEL_ASSETS_READY(sSpin)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_GET_WHEEL_SPIN_DATA - ",
		"Assets weren't ready for grabbing phase")
		RETURN sSpin.sData
	ENDIF
	
	STRING strDict = ROULETTE_ANIM_GET_DICTIONARY(ROULETTE_ANIM_DICT_PROP)
	
	IF NOT IS_STRING_NULL_OR_EMPTY(sSpin.strBallLastAnim)
		IF IS_ENTITY_PLAYING_ANIM(sSpin.objBall, strDict, sSpin.strBallLastAnim)
			sSpin.sData.fBallPhase = GET_ENTITY_ANIM_CURRENT_TIME(sSpin.objBall, strDict, sSpin.strBallLastAnim)
		ELSE
			sSpin.sData.fBallPhase = 0.0
		ENDIF
	ENDIF
	
	IF NOT IS_STRING_NULL_OR_EMPTY(sSpin.strWheelLastAnim)
		IF IS_ENTITY_PLAYING_ANIM(sSpin.objWheel, strDict, sSpin.strWheelLastAnim)
			sSpin.sData.fWheelPhase = GET_ENTITY_ANIM_CURRENT_TIME(sSpin.objWheel, strDict, sSpin.strWheelLastAnim)
		ELSE
			sSpin.sData.fWheelPhase = 0.0			
		ENDIF
	ENDIF
	
	RETURN sSpin.sData
ENDFUNC

