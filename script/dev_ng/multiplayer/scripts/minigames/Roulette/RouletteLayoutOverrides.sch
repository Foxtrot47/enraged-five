//////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Name:        RouletteLayoutOverrides.sch																			
/// Description: Header containing all overrideable functions and parameters relating to the roulette layout
///				 these should be overridden if using a different table asset with a different layout
///    			 position/scale
///																											
/// Written by:  Online Technical Team: Tom Turner,															
/// Date:  		15/03/2019																					
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "freemode_header.sch"

/// PURPOSE: Layout origin offset from the table root position
#IF NOT DEFINED(ROULETTE_LAYOUT_ORIGIN_OFFSET_X) TWEAK_FLOAT ROULETTE_LAYOUT_ORIGIN_OFFSET_X -0.097 #ENDIF
#IF NOT DEFINED(ROULETTE_LAYOUT_ORIGIN_OFFSET_Y) TWEAK_FLOAT ROULETTE_LAYOUT_ORIGIN_OFFSET_Y -0.42 #ENDIF

#IF NOT DEFINED(ROULETTE_LAYOUT_INSIDE_GRID_CELL_HEIGHT)
	/// PURPOSE: Grid cell height for an inside grid cell
	TWEAK_FLOAT ROULETTE_LAYOUT_INSIDE_GRID_CELL_HEIGHT 0.081287
#ENDIF

#IF NOT DEFINED(ROULETTE_LAYOUT_ZEROES_GRID_CELL_HEIGHT)
	/// PURPOSE: Grid cell height for a zeroes cell
	TWEAK_FLOAT ROULETTE_LAYOUT_ZEROES_GRID_CELL_HEIGHT 0.1
#ENDIF

#IF NOT DEFINED(ROULETTE_LAYOUT_OUTSIDE_GRID_CELL_WIDTH)
	/// PURPOSE: Grid cell width for an outside grid cell
	TWEAK_FLOAT ROULETTE_LAYOUT_OUTSIDE_GRID_CELL_WIDTH 0.074
#ENDIF

#IF NOT DEFINED(ROULETTE_LAYOUT_INSIDE_GRID_CELL_WIDTH)
	/// PURPOSE: Grid cell width for an inside bet
	TWEAK_FLOAT ROULETTE_LAYOUT_INSIDE_GRID_CELL_WIDTH 0.083013
#ENDIF
