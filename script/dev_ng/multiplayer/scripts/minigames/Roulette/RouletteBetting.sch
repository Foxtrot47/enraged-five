//////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Name:        RouletteBetting.sch																			
/// Description: Header for betting, handles: bet placing, converting layout indexs to bet types, and paying	
///				 out on bets																												
///																											
/// Written by:  Online Technical Team: Tom Turner,															
/// Date:  		12/02/2019																					
///																											
/// Notes: 		Whenever a payout is processed we use the index of the roulette number on the wheel not the 
///			    actual number itself																
/// TODO:																									
///    	1. Validation on bet processing, should check the bet being passed in is of the right type for the	
///    	   function																							
///		2. Use loops for getting number indexs associated with bets to shorten functions					   																										
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "RouletteLayout.sch"
USING "RouletteConstants.sch"
USING "RouletteData.sch"
USING "RouletteHelper.sch"
USING "commands_security.sch"

FUNC INT ROULETTE_BETTING_GET_MAX_INSIDE_BET_FOR_TABLE(INT iTableID)
	IF ROULETTE_TABLE_IS_HIGH_STAKES(iTableID)
		RETURN ROULETTE_MAX_HIGH_STAKES_INSIDE_BET
	ENDIF
	
	RETURN ROULETTE_MAX_INSIDE_BET
ENDFUNC

/// PURPOSE:
///    Gets the max possible inside bet for the table the local player is playing on 
FUNC INT ROULETTE_BETTING_GET_MAX_INSIDE_BET_FOR_CURRENT_TABLE(ROULETTE_DATA &sRouletteData)
	RETURN ROULETTE_BETTING_GET_MAX_INSIDE_BET_FOR_TABLE(sRouletteData.iTableID)
ENDFUNC

FUNC INT ROULETTE_BETTING_GET_MIN_INSIDE_BET_FOR_TABLE(INT iTableID)
	IF ROULETTE_TABLE_IS_HIGH_STAKES(iTableID)
		RETURN ROULETTE_MIN_HIGH_STAKES_INSIDE_BET
	ENDIF
	
	RETURN ROULETTE_MIN_INSIDE_BET
ENDFUNC

/// PURPOSE:
///    Gets the min possible inside bet for the table the local player is playing on 
FUNC INT ROULETTE_BETTING_GET_MIN_INSIDE_BET_FOR_CURRENT_TABLE(ROULETTE_DATA &sRouletteData)
	RETURN ROULETTE_BETTING_GET_MIN_INSIDE_BET_FOR_TABLE(sRouletteData.iTableID)
ENDFUNC

/// PURPOSE:
///    Checks if the minimum inside bet has been reached   
FUNC BOOL ROULETTE_BETTING_HAS_INSIDE_MIN_BET_BEEN_REACHED(ROULETTE_DATA &sRouletteData)
	// If they haven't made any inside bet they don't need to reach a minimum
	RETURN sRouletteData.iTotalInsideBetThisRound <= 0 OR 
	sRouletteData.iTotalInsideBetThisRound >= ROULETTE_BETTING_GET_MIN_INSIDE_BET_FOR_CURRENT_TABLE(sRouletteData)
ENDFUNC

FUNC INT ROULETTE_BETTING_GET_MAX_OUTSIDE_BET_FOR_TABLE(INT iTableID) 
	IF ROULETTE_TABLE_IS_HIGH_STAKES(iTableID)
		RETURN ROULETTE_MAX_HIGH_STAKES_OUTSIDE_BET
	ENDIF
	
	RETURN ROULETTE_MAX_OUTSIDE_BET
ENDFUNC

/// PURPOSE:
///    Gets the max outside bet for the table the player is currently playing on    
FUNC INT ROULETTE_BETTING_GET_MAX_OUTSIDE_BET_FOR_CURRENT_TABLE(ROULETTE_DATA &sRouletteData) 
	RETURN ROULETTE_BETTING_GET_MAX_OUTSIDE_BET_FOR_TABLE(sRouletteData.iTableID)
ENDFUNC

FUNC INT ROULETTE_BETTING_GET_MIN_OUTSIDE_BET_FOR_TABLE(INT iTableID)
	IF ROULETTE_TABLE_IS_HIGH_STAKES(iTableID)
		RETURN ROULETTE_MIN_HIGH_STAKES_OUTSIDE_BET
	ENDIF
	
	RETURN ROULETTE_MIN_OUTSIDE_BET
ENDFUNC

/// PURPOSE:
///    Gets the min outside bet for the table the player is currently playing on    
FUNC INT ROULETTE_BETTING_GET_MIN_OUTSIDE_BET_FOR_CURRENT_TABLE(ROULETTE_DATA &sRouletteData) 	
	RETURN ROULETTE_BETTING_GET_MIN_OUTSIDE_BET_FOR_TABLE(sRouletteData.iTableID)
ENDFUNC

/// PURPOSE:
///    Gets the max possible bet that can be made at the table,
///    taking into account inside and outside bets max limits
FUNC INT ROULETTE_BETTING_GET_MAX_BET_FOR_TABLE(INT iTableID)
	RETURN IMAX(
	ROULETTE_BETTING_GET_MAX_INSIDE_BET_FOR_TABLE(iTableID),
	ROULETTE_BETTING_GET_MAX_OUTSIDE_BET_FOR_TABLE(iTableID))
ENDFUNC

/// PURPOSE:
///    Gets the min possible bet that can be made at the table,
///    taking into account inside and outside bets min limits
FUNC INT ROULETTE_BETTING_GET_MIN_BET_FOR_TABLE(INT iTableID)
	RETURN IMIN(
	ROULETTE_BETTING_GET_MIN_INSIDE_BET_FOR_TABLE(iTableID),
	ROULETTE_BETTING_GET_MIN_OUTSIDE_BET_FOR_TABLE(iTableID))
ENDFUNC


/// PURPOSE:
///    Checks if the player has met the outside bet minimum   
FUNC BOOL ROULETTE_BETTING_HAS_PLAYER_BET_MET_OUTSIDE_BET_MINIMUM(ROULETTE_DATA &sRouletteData, INT iPlayerID, INT iBetIndex)
	INT iCurrentBetAmount = sRouletteData.playerBD[iPlayerID].iBets[iBetIndex].iBetAmount	
	RETURN iCurrentBetAmount <= 0 OR iCurrentBetAmount >=
	ROULETTE_BETTING_GET_MIN_OUTSIDE_BET_FOR_CURRENT_TABLE(sRouletteData)
ENDFUNC

/// PURPOSE:
///    Checks if the minimum outside bet has been reached. Note that for outside bets
///    minimum cannot be spread so every individual outside bet must be checked
FUNC BOOL ROULETTE_BETTING_HAS_OUTSIDE_MIN_BET_BEEN_REACHED(ROULETTE_DATA &sRouletteData)
	INT iBetIndex = 0
	INT iLocalPlayerID = NATIVE_TO_INT(PLAYER_ID())
	
	REPEAT ROULETTE_BETTING_MAX_ALLOWED_BETS iBetIndex	
		// If an outside bet
		IF NOT ROULETTE_LAYOUT_IS_INDEX_ON_AN_INSIDE_BET_CELL(sRouletteData.playerBD[iLocalPlayerID].iBets[iBetIndex].iLayoutIndex)				
			// If a bet has been placed but not reached the min
			IF NOT ROULETTE_BETTING_HAS_PLAYER_BET_MET_OUTSIDE_BET_MINIMUM(sRouletteData, iLocalPlayerID, iBetIndex)	
				RETURN FALSE
			ENDIF	
		ENDIF	
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Gets the bet type for a row in the first outside bet column  
FUNC ROULETTE_BET_TYPE ROULETTE_BETTING_GET_BET_TYPE_FOR_ROW_IN_FIRST_OUTSIDE_BET_COLUMN(INT iBetRow)
	SWITCH iBetRow
		CASE 0	RETURN ROULETTE_BET_TYPE_LOW_BRACKET
		CASE 1	RETURN ROULETTE_BET_TYPE_EVEN
		CASE 2 	RETURN ROULETTE_BET_TYPE_RED
		CASE 3 	RETURN ROULETTE_BET_TYPE_BLACK
		CASE 4 	RETURN ROULETTE_BET_TYPE_ODD
		CASE 5	RETURN ROULETTE_BET_TYPE_HIGH_BRACKET
	ENDSWITCH
	RETURN ROULETTE_BET_TYPE_INVALID
ENDFUNC

/// PURPOSE:
///    Gets the bet type for a row in the first outside bet column  
FUNC ROULETTE_BET_TYPE ROULETTE_BETTING_GET_BET_TYPE_FOR_ROW_IN_SECOND_OUTSIDE_BET_COLUMN(INT iBetRow)
	SWITCH iBetRow
		CASE 0	RETURN ROULETTE_BET_TYPE_DOZEN_FIRST
		CASE 1	RETURN ROULETTE_BET_TYPE_DOZEN_SECOND
		CASE 2 	RETURN ROULETTE_BET_TYPE_DOZEN_THIRD
	ENDSWITCH
	RETURN ROULETTE_BET_TYPE_INVALID
ENDFUNC

/// PURPOSE:
///    Gets the bet type for a row on the line between inside and outside bets  
FUNC ROULETTE_BET_TYPE ROULETTE_BETTING_GET_BET_TYPE_FOR_ROW_ON_OUTSIDE_BET_LINE(INT iBetRow)
	// First zero is technically on this line in the first row
	IF iBetRow = 0
		RETURN ROULETTE_BET_TYPE_STRAIGHT
	ENDIF
	
	// Second row is the five number
	IF iBetRow = 1
		RETURN ROULETTE_BET_TYPE_FIVE_NUMBER
	ENDIF
	
	// Cannot have a row greater than the max
	IF iBetRow > OUTSIDE_BET_LINE_MAX_Y_COORD
		RETURN ROULETTE_BET_TYPE_INVALID
	ENDIF
	
	// Every other row is a street bet
	IF iBetRow % 2 = 0
		RETURN ROULETTE_BET_TYPE_STREET
	ENDIF
	
	// Only possible is the six line
	RETURN ROULETTE_BET_TYPE_SIX_LINE 	
ENDFUNC

/// PURPOSE:
///		Gets the bet type for a 'column bet' x coord/column
FUNC ROULETTE_BET_TYPE ROULETTE_BETTING_GET_BET_TYPE_FOR_A_COLUMN_BET(INT iColumnBetXCoord)
	SWITCH iColumnBetXCoord
		CASE LAYOUT_MIN_COLUMN_BET_X_COORD		RETURN ROULETTE_BET_TYPE_COLUMN_FIRST
		CASE LAYOUT_COLUMN_BET_CENTRE_X_COORD	RETURN ROULETTE_BET_TYPE_COLUMN_SECOND
		CASE LAYOUT_MAX_COLUMN_BET_X_COORD 		RETURN ROULETTE_BET_TYPE_COLUMN_THIRD
	ENDSWITCH
	RETURN ROULETTE_BET_TYPE_INVALID
ENDFUNC

/// PURPOSE:
///    Gets the bet type for an inside bet   
FUNC ROULETTE_BET_TYPE ROULETTE_BETTING_GET_INSIDE_BET_TYPE(INT iBetColumn, INT iBetRow)		
	// Trios are on first line intersections and centre coordinate
	IF iBetRow = LAYOUT_MIN_INSIDE_BET_Y_COORD AND 
	(iBetColumn % 2 = 0 OR iBetColumn = LAYOUT_INSIDE_BETS_CENTRE_X_COORD)
		RETURN ROULETTE_BET_TYPE_TRIO
	ENDIF
	
	// On intersecting lines
	IF iBetRow % 2 != 0 AND iBetColumn % 2 = 0
		RETURN ROULETTE_BET_TYPE_CORNER
	ENDIF
	
	// Vertical split bet or horizontal split bet
	IF iBetRow % 2 != 0 OR iBetColumn % 2 = 0
	OR iBetRow = 0 AND iBetColumn = LAYOUT_MIN_ZEROES_X_COORD + 1// Special case for split bet for zeroes 
		RETURN ROULETTE_BET_TYPE_SPLIT
	ENDIF
		
	RETURN ROULETTE_BET_TYPE_STRAIGHT
ENDFUNC

/// PURPOSE:
///    Gets the bet type at the given layout coords   
FUNC ROULETTE_BET_TYPE ROULETTE_BETTING_GET_BET_TYPE_FROM_COORDS(INT iXCoord, INT iYCoord)	
	// Zeroes bets
	IF ROULETTE_LAYOUT_IS_COORD_ON_A_ZEROES_CELL(iXCoord, iYCoord)
		RETURN ROULETTE_BET_TYPE_STRAIGHT
	ENDIF
	
	// Outside bets
	SWITCH iXCoord		
		CASE 0	RETURN ROULETTE_BETTING_GET_BET_TYPE_FOR_ROW_IN_FIRST_OUTSIDE_BET_COLUMN(iYCoord)
		CASE 1	RETURN ROULETTE_BETTING_GET_BET_TYPE_FOR_ROW_IN_SECOND_OUTSIDE_BET_COLUMN(iYCoord)
		CASE 2	RETURN ROULETTE_BETTING_GET_BET_TYPE_FOR_ROW_ON_OUTSIDE_BET_LINE(iYCoord)
	ENDSWITCH
	
	// Column bets
	IF iYCoord = LAYOUT_MAX_INSIDE_BET_Y_COORD
		RETURN ROULETTE_BETTING_GET_BET_TYPE_FOR_A_COLUMN_BET(iXCoord)
	ENDIF	
	
	// Inside bets
	RETURN ROULETTE_BETTING_GET_INSIDE_BET_TYPE(iXCoord, iYCoord)
ENDFUNC

/// PURPOSE:
///    Gets the bet type associated to the layout index  
FUNC ROULETTE_BET_TYPE ROULETTE_BETTING_GET_BET_TYPE_FROM_LAYOUT_INDEX(INT iIndex)
	IF iIndex < 0
		RETURN ROULETTE_BET_TYPE_INVALID
	ENDIF
	
	INT iXCoord = -1
	INT iYCoord = -1
	ROULETTE_LAYOUT_INDEX_TO_LAYOUT_COORD(iIndex, iXCoord, iYCoord)
	RETURN ROULETTE_BETTING_GET_BET_TYPE_FROM_COORDS(iXCoord, iYCoord)
ENDFUNC

/// PURPOSE:
///    Checks the bets array for a bet at the given position and returns the bet index if found
///    returns -1 if a bet at the given position has not been made
FUNC INT ROULETTE_BETTING_FIND_BET_INDEX_BY_LAYOUT_INDEX(ROULETTE_BET &iBets[], INT iLayoutIndex)
	INT iSlotToCheck = 0
	INT iMaxBetIndex = COUNT_OF(iBets)
	
	REPEAT iMaxBetIndex iSlotToCheck	
		// If this layout index exists return the slot so we can add to its bet mount
		IF iBets[iSlotToCheck].iLayoutIndex = iLayoutIndex
			RETURN iSlotToCheck
		ENDIF	
	ENDREPEAT
	
	RETURN -1
ENDFUNC

/// PURPOSE:
///    Clears the bet so it can be reused
PROC ROULETTE_BETTING_CLEAR_BET(ROULETTE_BET &sBet)
	sBet.iBetAmount = 0
	sBet.iLayoutIndex = -1
ENDPROC

/// PURPOSE:
///    Clears all bets from the bet array
PROC ROULETTE_BETTING_CLEAR_BETS(ROULETTE_BET &sBets[])
	INT i = 0
	INT iMaxBetIndex = COUNT_OF(sBets)
	
	REPEAT iMaxBetIndex i
		ROULETTE_BETTING_CLEAR_BET(sBets[i])
		UNREGISTER_SCRIPT_VARIABLE(sBets[i].iBetAmount)
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Clears all bets belonging to the player with the given id
PROC ROULETTE_BETTING_CLEAR_PLAYERS_BETS(ROULETTE_DATA &sRouletteData, INT iPlayerID)
	ROULETTE_BETTING_CLEAR_BETS(sRouletteData.playerBD[iPlayerID].iBets)
ENDPROC

PROC ROULETTE_BETTING_CLEAR_ZONE_TRACKING(ROULETTE_DATA &sRouletteData)
	INT iLocalPlayerID = NATIVE_TO_INT(PLAYER_ID())
	CLEAR_BIT(sRouletteData.playerBD[iLocalPlayerID].iPlayerBS, ROULETTE_PLAYER_BS_HAS_BET_IN_ZONE_THREE)
	CLEAR_BIT(sRouletteData.playerBD[iLocalPlayerID].iPlayerBS, ROULETTE_PLAYER_BS_HAS_BET_IN_ZONE_TWO)	
	CLEAR_BIT(sRouletteData.playerBD[iLocalPlayerID].iPlayerBS, ROULETTE_PLAYER_BS_HAS_BET_IN_ZONE_ONE)
ENDPROC

FUNC INT ROULETTE_BETTING_GET_LOCAL_PLAYERS_TOTAL_UNIQUE_BETS(ROULETTE_DATA &sRouletteData)
	INT iLocalPlayerID = NATIVE_TO_INT(PLAYER_ID())
	INT iBetCount = 0
	INT i = 0
	REPEAT ROULETTE_BETTING_MAX_ALLOWED_BETS i
		IF sRouletteData.playerBD[iLocalPlayerID].iBets[i].iLayoutIndex != -1
			iBetCount++
		ENDIF		
	ENDREPEAT
	
	RETURN iBetCount
ENDFUNC

/// PURPOSE:
///    Clears all bets belonging to the local player 
PROC ROULETTE_BETTING_CLEAR_LOCAL_PLAYER_BETS(ROULETTE_DATA &sRouletteData)
	ROULETTE_BETTING_CLEAR_BETS(sRouletteData.playerBD[NATIVE_TO_INT(PLAYER_ID())].iBets)
	ROULETTE_BETTING_CLEAR_ZONE_TRACKING(sRouletteData)
	sRouletteData.iBetsPlaced = 0
ENDPROC

/// PURPOSE:
///    Checks if a bet array contains a bet at the given layout index
FUNC BOOL ROULETTE_BETTING_DOES_BET_ARRAY_CONTAIN_BET_AT_LAYOUT_INDEX(ROULETTE_BET &iBets[], INT iLayoutGridIndex)
	INT iSlotToCheck = 0
	INT iMaxBetIndex = COUNT_OF(iBets)
	
	REPEAT iMaxBetIndex iSlotToCheck	
		// If this layout index exists return the slot so we can add to its bet amount
		IF iBets[iSlotToCheck].iLayoutIndex = iLayoutGridIndex
			RETURN TRUE
		ENDIF	
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Gets the index of an open slot in the iBet array for the given layout grid index,
///    if the layout grid index already exists in the bet array it will return its index
///    as the same slot should be used
FUNC INT ROULETTE_BETTING_GET_OPEN_BET_SLOT_FOR_LAYOUT_INDEX(ROULETTE_BET &iBets[], INT iLayoutGridIndex)	
	INT iSlotToCheck = 0
	INT iFirstEmptySlot = -1
	INT iMaxBetIndex = COUNT_OF(iBets)
	
	REPEAT iMaxBetIndex iSlotToCheck	
		// If this layout index exists return the slot so we can add to its bet amount
		IF iBets[iSlotToCheck].iLayoutIndex = iLayoutGridIndex
			RETURN iSlotToCheck
		ENDIF
		
		// If an empty slot hasn't been found already and this slot is empty, store it
		IF iFirstEmptySlot = -1 AND iBets[iSlotToCheck].iLayoutIndex = -1
			iFirstEmptySlot = iSlotToCheck
		ENDIF		
	ENDREPEAT
	
	// If this layout index isn't already used return a new slot for it
	RETURN iFirstEmptySlot
ENDFUNC

/// PURPOSE:
///    Checks if the given roulette slot index is valid    
FUNC BOOL ROULETTE_BETTING_IS_BET_SLOT_INDEX_VALID(INT iBetSlot)
	RETURN iBetSlot >= 0 AND iBetSlot < ROULETTE_BETTING_MAX_ALLOWED_BETS
ENDFUNC

/// PURPOSE:
///    Updates a bet tracker with the given bet amount
PROC ROULETTE_BETTING_UPDATE_BET_AMOUNT_TRACKER(INT &iBetTracker, INT iBetAmountToAdd)
	// add new bet value
	iBetTracker += iBetAmountToAdd
	
	/// As the bet amount could be negative make sure it doesn't go below zero
	iBetTracker = IMAX(iBetTracker, 0)
ENDPROC

/// PURPOSE:
///    Adds the bet at the given coordinate to the zone tracker based on which zone it lies in
PROC ROULETTE_BETTING_ADD_BET_AT_COORD_TO_ZONE_TRACKING(ROULETTE_DATA &sRouletteData, INT iXCoord, INT iYCoord)
	INT iLocalPlayerID = NATIVE_TO_INT(PLAYER_ID())	
	IF ROULETTE_LAYOUT_IS_COORD_IN_ZONE_THREE(iXCoord, iYCoord)
		SET_BIT(sRouletteData.playerBD[iLocalPlayerID].iPlayerBS, ROULETTE_PLAYER_BS_HAS_BET_IN_ZONE_THREE)
		EXIT
	ENDIF
	
	IF ROULETTE_LAYOUT_IS_COORD_IN_ZONE_TWO(iXCoord, iYCoord)
		SET_BIT(sRouletteData.playerBD[iLocalPlayerID].iPlayerBS, ROULETTE_PLAYER_BS_HAS_BET_IN_ZONE_TWO)
		EXIT
	ENDIF
	
	IF ROULETTE_LAYOUT_IS_COORD_IN_ZONE_ONE(iXCoord, iYCoord)
		SET_BIT(sRouletteData.playerBD[iLocalPlayerID].iPlayerBS, ROULETTE_PLAYER_BS_HAS_BET_IN_ZONE_ONE)
	ENDIF
ENDPROC

/// PURPOSE:
///    Removes the given bet coord, updaint the zone tracker to reflect if the zone has bets in it still
PROC ROULETTE_BETTING_REMOVE_BET_AT_COORD_FROM_ZONE_TRACKER(ROULETTE_DATA &sRouletteData, INT iXCoord, INT iYCoord)	
	INT iZoneToCheck = ROULETTE_LAYOUT_GET_ZONE_FOR_COORD(iXCoord, iYCoord)
	
	INT i = 0
	INT iCurrentBetXCoord = 0
	INT iCurrentBetYCoord = 0
	INT iLocalPlayerID = NATIVE_TO_INT(PLAYER_ID())
	ROULETTE_BET sCurrentBet
	
	// Check for other bets in the zone we are removing from
	REPEAT ROULETTE_BETTING_MAX_ALLOWED_BETS i
		
		COPY_SCRIPT_STRUCT(sCurrentBet, sRouletteData.playerBD[iLocalPlayerID].iBets[i], SIZE_OF(ROULETTE_BET))
		
		IF sCurrentBet.iLayoutIndex != -1					
			ROULETTE_LAYOUT_INDEX_TO_LAYOUT_COORD(sCurrentBet.iLayoutIndex, iCurrentBetXCoord, iCurrentBetYCoord)
			
			// Skip checking the bet being removed
			IF iCurrentBetXCoord != iXCoord AND iCurrentBetYCoord != iYCoord
				/// If this bet is in the same zone as the bet being removed, exit.
				/// The zone will still need clearing
				IF ROULETTE_LAYOUT_IS_COORD_IN_ZONE(iCurrentBetXCoord, iCurrentBetYCoord, iZoneToCheck)
					EXIT
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	// No other bets were in the zone so we should mark the zone as clear
	CLEAR_BIT(sRouletteData.playerBD[iLocalPlayerID].iPlayerBS, iZoneToCheck)
ENDPROC

/// PURPOSE:
///    Completely removes the bet at the bet index for the given player (the bet trackers are updated to reflect its removal)s
PROC ROULETTE_BETTING_REMOVE_PLAYER_BET(ROULETTE_DATA &sRouletteData, INT iBetIndex, INT iPlayerID)
	IF ROULETTE_LAYOUT_IS_INDEX_ON_AN_INSIDE_BET_CELL(sRouletteData.playerBD[iPlayerID].iBets[iBetIndex].iLayoutIndex)
		ROULETTE_BETTING_UPDATE_BET_AMOUNT_TRACKER(sRouletteData.iTotalInsideBetThisRound,
		-sRouletteData.playerBD[iPlayerID].iBets[iBetIndex].iBetAmount)
	ELSE
		ROULETTE_BETTING_UPDATE_BET_AMOUNT_TRACKER(sRouletteData.iTotalOutsideBetThisRound,
		-sRouletteData.playerBD[iPlayerID].iBets[iBetIndex].iBetAmount)
	ENDIF
	
	INT iXCoord = 0
	INT iYCoord = 0
	ROULETTE_LAYOUT_INDEX_TO_LAYOUT_COORD(sRouletteData.playerBD[iPlayerID].iBets[iBetIndex].iLayoutIndex, iXCoord, iYCoord)
	ROULETTE_BETTING_REMOVE_BET_AT_COORD_FROM_ZONE_TRACKER(sRouletteData, iXCoord, iYCoord)
	
	sRouletteData.playerBD[iPlayerID].iBets[iBetIndex].iLayoutIndex = -1
	sRouletteData.playerBD[iPlayerID].iBets[iBetIndex].iBetAmount = 0	
ENDPROC

/// PURPOSE:
///    Removes all the local players bets that fail to meet the minimum
PROC ROULETTE_BETTING_REMOVE_LOCAL_PLAYER_BETS_THAT_FAIL_TO_MEET_THE_MIN_BET(ROULETTE_DATA &sRouletteData)
	BOOL bOutsideBetsShouldBeCleared = FALSE
	BOOL bInsideBetsShouldBeCleared = NOT ROULETTE_BETTING_HAS_INSIDE_MIN_BET_BEEN_REACHED(sRouletteData)
	
	INT i = 0
	INT iLocalPlayerID = NATIVE_TO_INT(PLAYER_ID())
	INT iLayoutIndex = -1
	BOOL bCurrentLayoutIndexIsInsideBet = FALSE
	REPEAT ROULETTE_BETTING_MAX_ALLOWED_BETS i
		iLayoutIndex = sRouletteData.playerBD[iLocalPlayerID].iBets[i].iLayoutIndex
		bCurrentLayoutIndexIsInsideBet = ROULETTE_LAYOUT_IS_INDEX_ON_AN_INSIDE_BET_CELL(iLayoutIndex)
		
		// Outside min bet checking has to be done for every bet as it cannot be spread
		bOutsideBetsShouldBeCleared = NOT ROULETTE_BETTING_HAS_PLAYER_BET_MET_OUTSIDE_BET_MINIMUM(sRouletteData, iLocalPlayerID, i)	
		
		IF bOutsideBetsShouldBeCleared AND NOT bCurrentLayoutIndexIsInsideBet OR
		bInsideBetsShouldBeCleared AND bCurrentLayoutIndexIsInsideBet
			ROULETTE_BETTING_REMOVE_PLAYER_BET(sRouletteData, i, iLocalPlayerID)
		ENDIF	
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Adds an inside roulette bet at the current cursor position   
FUNC ROULETTE_BETTING_PLACE_RESULT ROULETTE_BETTING_ADD_LOCAL_PLAYER_BET_AT_POSITION(ROULETTE_DATA &sRouletteData, INT iBetAmount, INT iXCoord, INT iYCoord)
	INT iLocalPlayerID = NATIVE_TO_INT(PLAYER_ID())	
	INT iLayoutGridIndex = ROULETTE_LAYOUT_COORD_TO_LAYOUT_INDEX(iXCoord, iYCoord)
	INT iBetSlotIndex = ROULETTE_BETTING_GET_OPEN_BET_SLOT_FOR_LAYOUT_INDEX(sRouletteData.playerBD[iLocalPlayerID].iBets, iLayoutGridIndex)
	BOOL bIsInsideBet = ROULETTE_LAYOUT_IS_COORD_ON_AN_INSIDE_BET_CELL(iXCoord, iYCoord)
	
	// No free slot available
	IF NOT ROULETTE_BETTING_IS_BET_SLOT_INDEX_VALID(iBetSlotIndex)
		RETURN RBPR_FAILED_NO_BET_SLOTS_AVAILABLE
	ENDIF
	
	INT iOldBetValue = sRouletteData.playerBD[iLocalPlayerID].iBets[iBetSlotIndex].iBetAmount
	
	// If current bet at this position is 0 or less and the player is removing chips
	IF iOldBetValue <= 0 AND iBetAmount <= 0
		RETURN RBPR_INVALID
	ENDIF
		
	INT iMaxBet
	INT iTotalBetThisRound
	ROULETTE_BETTING_PLACE_RESULT eHitLimit = RBPR_INVALID
	
	IF bIsInsideBet
		iMaxBet = ROULETTE_BETTING_GET_MAX_INSIDE_BET_FOR_CURRENT_TABLE(sRouletteData)
		iTotalBetThisRound = sRouletteData.iTotalInsideBetThisRound
		eHitLimit = RBPR_FAILED_HIT_INSIDE_LIMIT
	ELSE
		iMaxBet = ROULETTE_BETTING_GET_MAX_OUTSIDE_BET_FOR_CURRENT_TABLE(sRouletteData)
		iTotalBetThisRound = sRouletteData.iTotalOutsideBetThisRound
		eHitLimit = RBPR_FAILED_HIT_OUTSIDE_LIMIT
	ENDIF
	
	// If player has bet the max inside bet and is adding chips
	IF iTotalBetThisRound >= iMaxBet AND iBetAmount > 0
		RETURN eHitLimit
	ENDIF
	
	INT iTotalBetForRound = ROULETTE_HELPER_GET_LOCAL_PLAYERS_TOTAL_BET_FOR_CURRENT_ROUND(sRouletteData)
	BOOL bHasEnoughChips = (iTotalBetForRound + iBetAmount) <= ROULETTE_BETTING_GET_LOCAL_PLAYER_CHIP_COUNT()
	IF NOT bHasEnoughChips
		RETURN RBPR_FAILED_NOT_ENOUGH_CHIPS
	ENDIF	
	
	// Work out how much more the player can bet and clamp bet being added by it
	INT iAmountPlayerCanCurrentlyBet = iMaxBet - iTotalBetThisRound
	iBetAmount = CLAMP_INT(iBetAmount, -iOldBetValue, iAmountPlayerCanCurrentlyBet)

	IF bIsInsideBet
		ROULETTE_BETTING_UPDATE_BET_AMOUNT_TRACKER(sRouletteData.iTotalInsideBetThisRound, iBetAmount)
	ELSE
		ROULETTE_BETTING_UPDATE_BET_AMOUNT_TRACKER(sRouletteData.iTotalOutsideBetThisRound, iBetAmount)
	ENDIF
	
	// Calculate the new bet value
	INT iNewBetValue = iOldBetValue + iBetAmount
	iNewBetValue = CLAMP_INT(iNewBetValue, 0, iMaxBet)
	
	// Store the new bet amount in the bet
	sRouletteData.playerBD[iLocalPlayerID].iBets[iBetSlotIndex].iLayoutIndex = iLayoutGridIndex
	sRouletteData.playerBD[iLocalPlayerID].iBets[iBetSlotIndex].iBetAmount = iNewBetValue
	REGISTER_SCRIPT_VARIABLE(sRouletteData.playerBD[iLocalPlayerID].iBets[iBetSlotIndex].iBetAmount)
	
	// If the bet has been removed, mark the bet as unused (invalid layout index)
	IF iNewBetValue <= 0
		sRouletteData.playerBD[iLocalPlayerID].iBets[iBetSlotIndex].iLayoutIndex = -1
		ROULETTE_BETTING_REMOVE_BET_AT_COORD_FROM_ZONE_TRACKER(sRouletteData, iXCoord, iYCoord)
	ELSE
		ROULETTE_BETTING_ADD_BET_AT_COORD_TO_ZONE_TRACKING(sRouletteData, iXCoord, iYCoord)
	ENDIF
	
	sRouletteData.iBetsPlaced = ROULETTE_BETTING_GET_LOCAL_PLAYERS_TOTAL_UNIQUE_BETS(sRouletteData)
	
	// Bet position was altered successfully
	RETURN RBPR_SUCCESS
ENDFUNC

/// PURPOSE:
///    Adds the bet amount at the given layout coords and keeps a record of it in the iBets array
///    returns true if the bet was successfully added, will also update the total bet count for the current
///    round
FUNC ROULETTE_BETTING_PLACE_RESULT ROULETTE_BETTING_ADD_BET_AT_CURSOR_POSITION_FOR_LOCAL_PLAYER(ROULETTE_DATA &sRouletteData, INT iBetAmount)
	
	IF sRouletteData.iCursorXCoord = -1 OR sRouletteData.iCursorYCoord = -1
		RETURN RBPR_INVALID
	ENDIF
	
	IF NOT ROULETTE_TABLE_IS_LOCAL_PLAYERS_TABLE_ID_VALID(sRouletteData)
		RETURN RBPR_INVALID
	ENDIF	
	
	RETURN ROULETTE_BETTING_ADD_LOCAL_PLAYER_BET_AT_POSITION(sRouletteData, iBetAmount,
	sRouletteData.iCursorXCoord, sRouletteData.iCursorYCoord)
ENDFUNC


///------------------------------------------
///    Roulette number index array getters
///------------------------------------------   

/// PURPOSE:
///    Gets the roulette number indexs of the numbers above and bellow the coord for a split bet on
///    a horizontal line
PROC ROULETTE_BETTING_GET_HORIZONTAL_SPLIT_BET_INDEXS_FOR_COORD(INT &iRouletteNumberIndexs[], INT iXCoord, INT iYCoord)	
	INT iNewX = iXCoord
	INT iNewY = iYCoord	
	
	// Move coord up to get above number
	ROULETTE_LAYOUT_MOVE_COORD(iNewX, iNewY, 0, -1)
	INT iAboveIndex = ROULETTE_LAYOUT_COORD_TO_LAYOUT_INDEX(iNewX, iNewY)
	iRouletteNumberIndexs[0] = ROULETTE_GET_SLOT_INDEX_FROM_LAYOUT_INDEX(iAboveIndex)
	
	// reset to start coord
	iNewX = iXCoord
	iNewY = iYCoord
	
	// Move coord down to get number
	ROULETTE_LAYOUT_MOVE_COORD(iNewX, iNewY, 0, 1)
	INT iBellowIndex = ROULETTE_LAYOUT_COORD_TO_LAYOUT_INDEX(iNewX, iNewY)
	iRouletteNumberIndexs[1] = ROULETTE_GET_SLOT_INDEX_FROM_LAYOUT_INDEX(iBellowIndex)
ENDPROC

/// PURPOSE:
///    Gets the roulette number indexs of the numbers left and right of the coord for a split bet on
///    a vertical line
PROC ROULETTE_BETTING_GET_VERTICAL_SPLIT_BET_INDEXS_FOR_COORD(INT &iRouletteNumberIndexs[], INT iXCoord, INT iYCoord)
	INT iNewX = iXCoord
	INT iNewY = iYCoord	
	
	// Move coord left to get left number
	ROULETTE_LAYOUT_MOVE_COORD(iNewX, iNewY, -1, 0)
	INT iLeftIndex = ROULETTE_LAYOUT_COORD_TO_LAYOUT_INDEX(iNewX, iNewY)
	iRouletteNumberIndexs[0] = ROULETTE_GET_SLOT_INDEX_FROM_LAYOUT_INDEX(iLeftIndex)
		
	// reset to start coord
	iNewX = iXCoord
	iNewY = iYCoord
	
	// Move coord right to get right number
	ROULETTE_LAYOUT_MOVE_COORD(iNewX, iNewY, 1, 0)
	INT iRightIndex = ROULETTE_LAYOUT_COORD_TO_LAYOUT_INDEX(iNewX, iNewY)
	iRouletteNumberIndexs[1] = ROULETTE_GET_SLOT_INDEX_FROM_LAYOUT_INDEX(iRightIndex)		
ENDPROC

/// PURPOSE:
///    Fills the array with all the roulette number indexs associated with a split bet layout index position 
PROC ROULETTE_BETTING_GET_SPLIT_BET_ROULETTE_NUMBER_INDEXS_FROM_LAYOUT_INDEX(INT &iRouletteNumberIndexs[], INT iLayoutIndex)
	INT iXCoord = -1
	INT iYCoord = -1
	ROULETTE_LAYOUT_INDEX_TO_LAYOUT_COORD(iLayoutIndex, iXCoord, iYCoord)
	
	IF iYCoord = 0 AND iXCoord = LAYOUT_MIN_ZEROES_X_COORD + 1
		ROULETTE_BETTING_GET_VERTICAL_SPLIT_BET_INDEXS_FOR_COORD(iRouletteNumberIndexs, iXCoord, iYCoord)
		EXIT
	ENDIF
	
	// Check if its a split on a horizontal
	IF iYCoord % 2 != 0
		ROULETTE_BETTING_GET_HORIZONTAL_SPLIT_BET_INDEXS_FOR_COORD(iRouletteNumberIndexs, iXCoord, iYCoord)
		EXIT
	ENDIF
	
	// Must have been a vertical split so get left and right instead
	ROULETTE_BETTING_GET_VERTICAL_SPLIT_BET_INDEXS_FOR_COORD(iRouletteNumberIndexs, iXCoord, iYCoord)
ENDPROC

/// PURPOSE:
///    Fills the array with all the roulette number indexs associated with a street bet layout index position
PROC ROULETTE_BETTING_GET_STREET_BET_ROULETTE_NUMBER_INDEXS_FROM_LAYOUT_INDEX(INT &iRouletteNumberIndexs[], INT iLayoutIndex)
	INT iNumberIndex = -1
	INT iXCoord = -1
	INT iYCoord = -1
	ROULETTE_LAYOUT_INDEX_TO_LAYOUT_COORD(iLayoutIndex, iXCoord, iYCoord)
	
	// First number, move one as we are on a line
	ROULETTE_LAYOUT_MOVE_COORD(iXCoord, iYCoord, 1, 0)
	iNumberIndex = ROULETTE_LAYOUT_COORD_TO_LAYOUT_INDEX(iXCoord, iYCoord)
	iRouletteNumberIndexs[0] = ROULETTE_GET_SLOT_INDEX_FROM_LAYOUT_INDEX(iNumberIndex)
	
	// Second number, move two to skip line
	ROULETTE_LAYOUT_MOVE_COORD(iXCoord, iYCoord, 2, 0)
	iNumberIndex = ROULETTE_LAYOUT_COORD_TO_LAYOUT_INDEX(iXCoord, iYCoord)
	iRouletteNumberIndexs[1] = ROULETTE_GET_SLOT_INDEX_FROM_LAYOUT_INDEX(iNumberIndex)
	
	// Third number, move two to skip line
	ROULETTE_LAYOUT_MOVE_COORD(iXCoord, iYCoord, 2, 0)
	iNumberIndex = ROULETTE_LAYOUT_COORD_TO_LAYOUT_INDEX(iXCoord, iYCoord)
	iRouletteNumberIndexs[2] = ROULETTE_GET_SLOT_INDEX_FROM_LAYOUT_INDEX(iNumberIndex)	
ENDPROC

/// PURPOSE:
///    Fills the array with all the roulette number indexs associated with a trio bet layout index position
PROC ROULETTE_BETTING_GET_TRIO_BET_ROULETTE_NUMBER_INDEXS_FROM_LAYOUT_INDEX(INT &iRouletteNumberIndexs[], INT iLayoutIndex)
	INT iNumberIndex = -1
	INT iXCoord = -1
	INT iYCoord = -1
	ROULETTE_LAYOUT_INDEX_TO_LAYOUT_COORD(iLayoutIndex, iXCoord, iYCoord)
	
	// In the middle the trio is flipped and there is 
	// only one case of this so just assign index directly
	IF iXCoord = LAYOUT_INSIDE_BETS_CENTRE_X_COORD
		iRouletteNumberIndexs[0] = ROULETTE_GET_SLOT_INDEX_FROM_LAYOUT_INDEX(21)
		iRouletteNumberIndexs[1] = ROULETTE_GET_SLOT_INDEX_FROM_LAYOUT_INDEX(2)
		iRouletteNumberIndexs[2] = ROULETTE_GET_SLOT_INDEX_FROM_LAYOUT_INDEX(4)
		EXIT
	ENDIF
	
	INT iNewX = iXCoord
	INT iNewY = iYCoord	
	
	// Get top/bottom number
	ROULETTE_LAYOUT_MOVE_COORD(iNewX, iNewY, 0, -1)
	iNumberIndex = ROULETTE_LAYOUT_COORD_TO_LAYOUT_INDEX(iNewX, iNewY)
	iRouletteNumberIndexs[0] = ROULETTE_GET_SLOT_INDEX_FROM_LAYOUT_INDEX(iNumberIndex)
	
	// Reset to start
	iNewX = iXCoord
	iNewY = iYCoord
	
	// Up/down and left
	ROULETTE_LAYOUT_MOVE_COORD(iNewX, iNewY, -1, 1)
	iNumberIndex = ROULETTE_LAYOUT_COORD_TO_LAYOUT_INDEX(iNewX, iNewY)
	iRouletteNumberIndexs[1] = ROULETTE_GET_SLOT_INDEX_FROM_LAYOUT_INDEX(iNumberIndex)
	
	// Rreset to start
	iNewX = iXCoord
	iNewY = iYCoord
	
	// Up/down and right
	ROULETTE_LAYOUT_MOVE_COORD(iNewX, iNewY, 1, 1)
	iNumberIndex = ROULETTE_LAYOUT_COORD_TO_LAYOUT_INDEX(iNewX, iNewY)
	iRouletteNumberIndexs[2] = ROULETTE_GET_SLOT_INDEX_FROM_LAYOUT_INDEX(iNumberIndex)
ENDPROC

/// PURPOSE:
///    Fills the array with all the roulette number indexs associated with a corner bet layout index position
PROC ROULETTE_BETTING_GET_CORNER_BET_ROULETTE_NUMBER_INDEXS_FROM_LAYOUT_INDEX(INT &iRouletteNumberIndexs[], INT iLayoutIndex)	
	INT iNumberIndex = -1
	INT iXCoord = -1
	INT iYCoord = -1
	ROULETTE_LAYOUT_INDEX_TO_LAYOUT_COORD(iLayoutIndex, iXCoord, iYCoord)
	
	INT iNewX = iXCoord
	INT iNewY = iYCoord	
	
	// Get top left
	ROULETTE_LAYOUT_MOVE_COORD(iNewX, iNewY, -1, -1)
	iNumberIndex = ROULETTE_LAYOUT_COORD_TO_LAYOUT_INDEX(iNewX, iNewY)
	iRouletteNumberIndexs[0] = ROULETTE_GET_SLOT_INDEX_FROM_LAYOUT_INDEX(iNumberIndex)
	
	iNewX = iXCoord
	iNewY = iYCoord
	
	// Get top right
	ROULETTE_LAYOUT_MOVE_COORD(iNewX, iNewY, 1, -1)
	iNumberIndex = ROULETTE_LAYOUT_COORD_TO_LAYOUT_INDEX(iNewX, iNewY)
	iRouletteNumberIndexs[1] = ROULETTE_GET_SLOT_INDEX_FROM_LAYOUT_INDEX(iNumberIndex)
	
	iNewX = iXCoord
	iNewY = iYCoord
	
	// Get bottom left
	ROULETTE_LAYOUT_MOVE_COORD(iNewX, iNewY, -1, 1)
	iNumberIndex = ROULETTE_LAYOUT_COORD_TO_LAYOUT_INDEX(iNewX, iNewY)
	iRouletteNumberIndexs[2] = ROULETTE_GET_SLOT_INDEX_FROM_LAYOUT_INDEX(iNumberIndex)
	
	iNewX = iXCoord
	iNewY = iYCoord
	
	// Get bottom right
	ROULETTE_LAYOUT_MOVE_COORD(iNewX, iNewY, 1, 1)
	iNumberIndex = ROULETTE_LAYOUT_COORD_TO_LAYOUT_INDEX(iNewX, iNewY)
	iRouletteNumberIndexs[3] = ROULETTE_GET_SLOT_INDEX_FROM_LAYOUT_INDEX(iNumberIndex)
ENDPROC

/// PURPOSE:
///    Fills the array with all the roulette number indexs associated with the five number bet layout index position
PROC ROULETTE_BETTTING_GET_FIVE_NUMBER_BET_ROULETTE_NUMBER_INDEXS_FROM_LAYOUT_INDEX(INT &iRouletteNumberIndexs[])		
	// Only one possible five number so just set positions
	iRouletteNumberIndexs[0] = ROULETTE_GET_SLOT_INDEX_FROM_LAYOUT_INDEX(2)// 	Zero layout index	
	iRouletteNumberIndexs[1] = ROULETTE_GET_SLOT_INDEX_FROM_LAYOUT_INDEX(4)// 	Double zero layout index	
	iRouletteNumberIndexs[2] = ROULETTE_GET_SLOT_INDEX_FROM_LAYOUT_INDEX(19)// 	One layout index	
	iRouletteNumberIndexs[3] = ROULETTE_GET_SLOT_INDEX_FROM_LAYOUT_INDEX(21)// 	Two layout index	
	iRouletteNumberIndexs[4] = ROULETTE_GET_SLOT_INDEX_FROM_LAYOUT_INDEX(23)// 	Three layout index		
ENDPROC

/// PURPOSE:
///    Fills the array with all the roulette number indexs associated with a six line bet layout index position 
PROC ROULETTE_BETTING_GET_SIX_LINE_BET_ROULETTE_NUMBER_INDEXS_FROM_LAYOUT_INDEX(INT &iRouletteNumbers[], INT iLayoutIndex)
	INT iNumberIndex = -1
	INT iXCoord = -1
	INT iYCoord = -1
	ROULETTE_LAYOUT_INDEX_TO_LAYOUT_COORD(iLayoutIndex, iXCoord, iYCoord)
	
	INT iNewX = iXCoord
	INT iNewY = iYCoord
	
	//TODO: Use a loop
	
	// Move to row above
	ROULETTE_LAYOUT_MOVE_COORD(iNewX, iNewY, 0, -1)
	
	// First number, move one as we are on a line
	ROULETTE_LAYOUT_MOVE_COORD(iNewX, iNewY, 1, 0)
	iNumberIndex = ROULETTE_LAYOUT_COORD_TO_LAYOUT_INDEX(iNewX, iNewY)
	iRouletteNumbers[0] = ROULETTE_GET_SLOT_INDEX_FROM_LAYOUT_INDEX(iNumberIndex)
	
	// Second number, move two to skip line
	ROULETTE_LAYOUT_MOVE_COORD(iNewX, iNewY, 2, 0)
	iNumberIndex = ROULETTE_LAYOUT_COORD_TO_LAYOUT_INDEX(iNewX, iNewY)
	iRouletteNumbers[1] = ROULETTE_GET_SLOT_INDEX_FROM_LAYOUT_INDEX(iNumberIndex)
	
	// Third number, move two to skip line
	ROULETTE_LAYOUT_MOVE_COORD(iNewX, iNewY, 2, 0)
	iNumberIndex = ROULETTE_LAYOUT_COORD_TO_LAYOUT_INDEX(iNewX, iNewY)
	iRouletteNumbers[2] = ROULETTE_GET_SLOT_INDEX_FROM_LAYOUT_INDEX(iNumberIndex)
	
	// Reset to start coord
	iNewX = iXCoord
	iNewY = iYCoord
	
	// Move to row below
	ROULETTE_LAYOUT_MOVE_COORD(iNewX, iNewY, 0, 1)
	
	// First number, move one as we are on a line
	ROULETTE_LAYOUT_MOVE_COORD(iNewX, iNewY, 1, 0)
	iNumberIndex = ROULETTE_LAYOUT_COORD_TO_LAYOUT_INDEX(iNewX, iNewY)
	iRouletteNumbers[3] = ROULETTE_GET_SLOT_INDEX_FROM_LAYOUT_INDEX(iNumberIndex)
	
	// Second number, move two to skip line
	ROULETTE_LAYOUT_MOVE_COORD(iNewX, iNewY, 2, 0)
	iNumberIndex = ROULETTE_LAYOUT_COORD_TO_LAYOUT_INDEX(iNewX, iNewY)
	iRouletteNumbers[4] = ROULETTE_GET_SLOT_INDEX_FROM_LAYOUT_INDEX(iNumberIndex)
	
	// Third number, move two to skip line
	ROULETTE_LAYOUT_MOVE_COORD(iNewX, iNewY, 2, 0)
	iNumberIndex = ROULETTE_LAYOUT_COORD_TO_LAYOUT_INDEX(iNewX, iNewY)
	iRouletteNumbers[5] = ROULETTE_GET_SLOT_INDEX_FROM_LAYOUT_INDEX(iNumberIndex)
ENDPROC


///-----------------------------
///    Number queries
///-----------------------------

/// PURPOSE:
///    Checks if the roulette number is even (0 and 00 do not count as an even in roulette)
FUNC BOOL ROULETTE_BETTING_IS_ROULETTE_NUMBER_EVEN(INT iRouletteNumber)
	// Zeroes are excluded
	IF iRouletteNumber = 0 OR iRouletteNumber = ROULETTE_DOUBLE_ZERO_IDENTIFIER
		RETURN FALSE
	ENDIF
	
	RETURN iRouletteNumber % 2 = 0
ENDFUNC

/// PURPOSE:
///    Checks if the roulette number is odd (0 and 00 do not count as an odd in roulette)
FUNC BOOL ROULETTE_BETTING_IS_ROULETTE_NUMBER_ODD(INT iRouletteNumber)
	// Zeroes are excluded
	IF iRouletteNumber = 0 OR iRouletteNumber = ROULETTE_DOUBLE_ZERO_IDENTIFIER
		RETURN FALSE
	ENDIF
	RETURN iRouletteNumber % 2 != 0
ENDFUNC

///-----------------------------
///    Number index queries
///-----------------------------    

/// PURPOSE:
///    Checks if the value stored at the given roulette index is an even number    
FUNC BOOL ROULETTE_BETTING_IS_ROULETTE_NUMBER_INDEXS_VALUE_EVEN(INT iRouletteNumberIndex)
	INT iRouletteNumber = ROULETTE_GET_NUMBER_FROM_SLOT_INDEX(iRouletteNumberIndex)
	RETURN ROULETTE_BETTING_IS_ROULETTE_NUMBER_EVEN(iRouletteNumber)
ENDFUNC

/// PURPOSE:
///    Checks if the value stored at the given roulette index is an odd number    
FUNC BOOL ROULETTE_BETTING_IS_ROULETTE_NUMBER_INDEXS_VALUE_ODD(INT iRouletteNumberIndex)
	INT iRouletteNumber = ROULETTE_GET_NUMBER_FROM_SLOT_INDEX(iRouletteNumberIndex)
	RETURN ROULETTE_BETTING_IS_ROULETTE_NUMBER_ODD(iRouletteNumber)
ENDFUNC

/// PURPOSE:
///    Checks if the value stored at the given roulette index is in a given range  
FUNC BOOL ROULETTE_BETTING_IS_ROULETTE_NUMBER_INDEXS_VALUE_IN_RANGE(INT iRouletteNumberIndex, INT iRangeMin, INT iRangeMax)
	INT iRouletteNumber = ROULETTE_GET_NUMBER_FROM_SLOT_INDEX(iRouletteNumberIndex)
	RETURN IS_INT_IN_RANGE_INCLUSIVE(iRouletteNumber, iRangeMin, iRangeMax)
ENDFUNC

/// PURPOSE:
///    Checks if the roulette number index has its number in the given 'column bet' column   
FUNC BOOL ROULETTE_BETTING_IS_ROULETTE_NUMBER_INDEX_IN_OUTSIDE_BET_COLUMN(INT iRouletteNumberIndex, ROULETTE_BET_TYPE eRouletteColumnBetType)
	INT iLayoutIndex = ROULETTE_GET_LAYOUT_INDEX_FROM_SLOT_INDEX(iRouletteNumberIndex)
	RETURN ROULETTE_LAYOUT_IS_INDEX_ON_A_COLUMN_BET_CELL(iLayoutIndex, eRouletteColumnBetType)	
ENDFUNC


///-------------------------------
///    Payout Getters
///-------------------------------

/// PURPOSE:
///    Gets the odds for the given bet type  
FUNC INT ROULETTE_BETTING_GET_ODDS_FOR_BET_TYPE(ROULETTE_BET_TYPE eBetType)
	SWITCH eBetType
		// Inside bets
		CASE ROULETTE_BET_TYPE_STRAIGHT		RETURN ROULETTE_PAYOUT_STRAIGHT_BET
		CASE ROULETTE_BET_TYPE_SPLIT		RETURN ROULETTE_PAYOUT_SPLIT_BET
		CASE ROULETTE_BET_TYPE_STREET 		RETURN ROULETTE_PAYOUT_STREET_BET
		CASE ROULETTE_BET_TYPE_TRIO			RETURN ROULETTE_PAYOUT_TRIO_BET
		CASE ROULETTE_BET_TYPE_CORNER		RETURN ROULETTE_PAYOUT_CORNER_BET
		CASE ROULETTE_BET_TYPE_FIVE_NUMBER	RETURN ROULETTE_PAYOUT_FIVE_NUMBER_BET
		CASE ROULETTE_BET_TYPE_SIX_LINE		RETURN ROULETTE_PAYOUT_SIX_LINE_BET

		// Outside bets
		CASE ROULETTE_BET_TYPE_RED
		CASE ROULETTE_BET_TYPE_BLACK
			RETURN ROULETTE_PAYOUT_COLOUR_BET

		CASE ROULETTE_BET_TYPE_ODD
		CASE ROULETTE_BET_TYPE_EVEN
			RETURN ROULETTE_PAYOUT_ODDS_EVENS_BET

		CASE ROULETTE_BET_TYPE_LOW_BRACKET
		CASE ROULETTE_BET_TYPE_HIGH_BRACKET
			RETURN ROULETTE_PAYOUT_HIGH_LOW_BET

		CASE ROULETTE_BET_TYPE_DOZEN_FIRST
		CASE ROULETTE_BET_TYPE_DOZEN_SECOND
		CASE ROULETTE_BET_TYPE_DOZEN_THIRD
			RETURN ROULETTE_PAYOUT_DOZENS_BET
	
		CASE ROULETTE_BET_TYPE_COLUMN_FIRST
		CASE ROULETTE_BET_TYPE_COLUMN_SECOND
		CASE ROULETTE_BET_TYPE_COLUMN_THIRD	
			RETURN ROULETTE_PAYOUT_COLUMNS_BET
	ENDSWITCH
	
	RETURN 0
ENDFUNC


/// PURPOSE:
///    Gets the calculated payout for an outside bet of the given amount and type    
FUNC INT ROULETTE_BETTING_GET_PAYOUT_AMOUNT_FOR_BET_TYPE(INT iBetAmount, ROULETTE_BET_TYPE eBetType)
	INT iOdds = ROULETTE_BETTING_GET_ODDS_FOR_BET_TYPE(eBetType)
	RETURN iBetAmount + (iBetAmount * iOdds)
ENDFUNC

/// PURPOSE:
///    Gets the payout amount for a straight bet given the winning number  
FUNC INT ROULETTE_BETTING_GET_STRAIGHT_BET_PAYOUT(ROULETTE_BET sBet, INT iWinningNumberIndex)
	// Get the roulette number index from the layout index
	INT iBetRouletteNumberIndex = ROULETTE_GET_SLOT_INDEX_FROM_LAYOUT_INDEX(sBet.iLayoutIndex)
	
	// if the number is the same as the winning number the straight bet gets a payout
	IF iBetRouletteNumberIndex = iWinningNumberIndex
		RETURN sBet.iBetAmount + (sBet.iBetAmount * ROULETTE_PAYOUT_STRAIGHT_BET)	
	ENDIF
	
	RETURN 0
ENDFUNC

/// PURPOSE:
///    Gets the payout for a split bet given the winning roulette number index
FUNC INT ROULETTE_BETTING_GET_SPLIT_BET_PAYOUT(ROULETTE_BET sBet, INT iWinningNumberIndex)
	INT iBetNumberIndexs[2]
	ROULETTE_BETTING_GET_SPLIT_BET_ROULETTE_NUMBER_INDEXS_FROM_LAYOUT_INDEX(iBetNumberIndexs, sBet.iLayoutIndex)
	
	IF DOES_INT_ARRAY_CONTAIN(iBetNumberIndexs, iWinningNumberIndex)
		RETURN sBet.iBetAmount + (sBet.iBetAmount * ROULETTE_PAYOUT_SPLIT_BET)	
	ENDIF
	
	RETURN 0
ENDFUNC

/// PURPOSE:
///    Gets the payout for a street bet given the winning roulette number index
FUNC INT ROULETTE_BETTING_GET_STREET_BET_PAYOUT(ROULETTE_BET sBet, INT iWinningNumberIndex)
	INT iBetNumberIndexs[ROULETTE_NUMBERS_IN_A_STREET_BET_COUNT]
	ROULETTE_BETTING_GET_STREET_BET_ROULETTE_NUMBER_INDEXS_FROM_LAYOUT_INDEX(iBetNumberIndexs, sBet.iLayoutIndex)
	
	IF DOES_INT_ARRAY_CONTAIN(iBetNumberIndexs, iWinningNumberIndex)
		RETURN sBet.iBetAmount + (sBet.iBetAmount * ROULETTE_PAYOUT_STREET_BET)
	ENDIF
	
	RETURN 0
ENDFUNC

/// PURPOSE:
///    Gets the payout for a trio bet given the winning roulette number index
FUNC INT ROULETTE_BETTING_GET_TRIO_BET_PAYOUT(ROULETTE_BET sBet, INT iWinningNumberIndex)
	INT iBetNumberIndexs[3]
	ROULETTE_BETTING_GET_TRIO_BET_ROULETTE_NUMBER_INDEXS_FROM_LAYOUT_INDEX(iBetNumberIndexs, sBet.iLayoutIndex)
	
	IF DOES_INT_ARRAY_CONTAIN(iBetNumberIndexs, iWinningNumberIndex)
		RETURN sBet.iBetAmount + (sBet.iBetAmount * ROULETTE_PAYOUT_TRIO_BET)
	ENDIF
	
	RETURN 0
ENDFUNC

/// PURPOSE:
///    Gets the payout for a corner bet given the winning roulette number index
FUNC INT ROULETTE_BETTING_GET_CORNER_BET_PAYOUT(ROULETTE_BET sBet, INT iWinningNumberIndex)
	INT iBetNumberIndexs[4]
	ROULETTE_BETTING_GET_CORNER_BET_ROULETTE_NUMBER_INDEXS_FROM_LAYOUT_INDEX(iBetNumberIndexs, sBet.iLayoutIndex)
	
	IF DOES_INT_ARRAY_CONTAIN(iBetNumberIndexs, iWinningNumberIndex)
		RETURN sBet.iBetAmount + (sBet.iBetAmount * ROULETTE_PAYOUT_CORNER_BET)
	ENDIF
	
	RETURN 0
ENDFUNC

/// PURPOSE:
///    Gets the payout for a five number bet given the winning roulette number index    
FUNC INT ROULETTE_BETTING_GET_FIVE_NUMBER_BET_PAYOUT(ROULETTE_BET sBet, INT iWinningNumberIndex)
	INT iBetNumberIndexs[5]
	ROULETTE_BETTTING_GET_FIVE_NUMBER_BET_ROULETTE_NUMBER_INDEXS_FROM_LAYOUT_INDEX(iBetNumberIndexs)
	
	IF DOES_INT_ARRAY_CONTAIN(iBetNumberIndexs, iWinningNumberIndex)
		RETURN sBet.iBetAmount + (sBet.iBetAmount * ROULETTE_PAYOUT_FIVE_NUMBER_BET)
	ENDIF
	
	RETURN 0
ENDFUNC

/// PURPOSE:
///    Gets the payout for a six line bet given the winning roulette number index    
FUNC INT ROULETTE_BETTING_GET_SIX_LINE_BET_PAYOUT(ROULETTE_BET sBet, INT iWinningNumberIndex)
	INT iBetNumberIndexs[6]
	ROULETTE_BETTING_GET_SIX_LINE_BET_ROULETTE_NUMBER_INDEXS_FROM_LAYOUT_INDEX(iBetNumberIndexs, sBet.iLayoutIndex)
	
	IF DOES_INT_ARRAY_CONTAIN(iBetNumberIndexs, iWinningNumberIndex)
		RETURN sBet.iBetAmount + (sBet.iBetAmount * ROULETTE_PAYOUT_SIX_LINE_BET)
	ENDIF
	
	RETURN 0
ENDFUNC

/// PURPOSE:
///    Gets the payout for an outside colour bet given the winning roulette number index  
FUNC INT ROULETTE_BETTING_GET_COLOUR_BET_PAYOUT(ROULETTE_BET sBet, INT iWinningNumberIndex, ROULETTE_BET_TYPE eColorBetType)
	IF eColorBetType != ROULETTE_BET_TYPE_RED AND eColorBetType != ROULETTE_BET_TYPE_BLACK
		RETURN 0
	ENDIF
	
	IF ROULETTE_GET_COLOR_BET_TYPE_OF_ROULETTE_SLOT_INDEX(iWinningNumberIndex) = eColorBetType
		RETURN ROULETTE_BETTING_GET_PAYOUT_AMOUNT_FOR_BET_TYPE(sBet.iBetAmount, eColorBetType)// Colours should always have the same value
	ENDIF
	
	RETURN 0
ENDFUNC

/// PURPOSE:
///      Gets the payout for an outside odd bet given the winning roulette number index  
FUNC INT ROULETTE_BETTING_GET_ODD_BET_PAYOUT(ROULETTE_BET sBet, INT iWinningNumberIndex)
	IF ROULETTE_BETTING_IS_ROULETTE_NUMBER_INDEXS_VALUE_ODD(iWinningNumberIndex) 
		RETURN ROULETTE_BETTING_GET_PAYOUT_AMOUNT_FOR_BET_TYPE(sBet.iBetAmount, ROULETTE_BET_TYPE_ODD)
	ENDIF
	
	RETURN 0
ENDFUNC

/// PURPOSE:
///      Gets the payout for an outside even bet given the winning roulette number index  
FUNC INT ROULETTE_BETTING_GET_EVEN_BET_PAYOUT(ROULETTE_BET sBet, INT iWinningNumberIndex)
	IF ROULETTE_BETTING_IS_ROULETTE_NUMBER_INDEXS_VALUE_EVEN(iWinningNumberIndex) 
		RETURN ROULETTE_BETTING_GET_PAYOUT_AMOUNT_FOR_BET_TYPE(sBet.iBetAmount, ROULETTE_BET_TYPE_EVEN)
	ENDIF
	
	RETURN 0
ENDFUNC

/// PURPOSE:
///    Gets the range extents for the given range bet type
PROC ROULETTE_BETTING_GET_RANGE_FOR_RANGE_BET(INT &iRangeMin, INT &iRangeMax, ROULETTE_BET_TYPE eRangeBetType)
	SWITCH	eRangeBetType
		CASE ROULETTE_BET_TYPE_LOW_BRACKET
			iRangeMin = ROULETTE_LOW_BRACKET_MIN
			iRangeMax = ROULETTE_LOW_BRACKET_MAX
		BREAK
		CASE ROULETTE_BET_TYPE_HIGH_BRACKET
			iRangeMin = ROULETTE_HIGH_BRACKET_MIN
			iRangeMax = ROULETTE_HIGH_BRACKET_MAX
		BREAK
		CASE ROULETTE_BET_TYPE_DOZEN_FIRST
			iRangeMin = ROULETTE_DOZEN_FIRST_MIN
			iRangeMax = ROULETTE_DOZEN_FIRST_MAX
		BREAK
		CASE ROULETTE_BET_TYPE_DOZEN_SECOND
			iRangeMin = ROULETTE_DOZEN_SECOND_MIN
			iRangeMax = ROULETTE_DOZEN_SECOND_MAX
		BREAK
		CASE ROULETTE_BET_TYPE_DOZEN_THIRD
			iRangeMin = ROULETTE_DOZEN_THIRD_MIN
			iRangeMax = ROULETTE_DOZEN_THIRD_MAX
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Get payout for a bet type that checks a range of numbers    
FUNC INT ROULETTE_BETTING_GET_RANGE_BET_PAYOUT(ROULETTE_BET sBet, INT iWinningNumberIndex, ROULETTE_BET_TYPE eRangeBetType)
	INT iRangeMin = 0
	INT iRangeMax = 0
	ROULETTE_BETTING_GET_RANGE_FOR_RANGE_BET(iRangeMin, iRangeMax, eRangeBetType)
	
	IF ROULETTE_BETTING_IS_ROULETTE_NUMBER_INDEXS_VALUE_IN_RANGE(iWinningNumberIndex, iRangeMin, iRangeMax) 
		RETURN ROULETTE_BETTING_GET_PAYOUT_AMOUNT_FOR_BET_TYPE(sBet.iBetAmount, eRangeBetType)
	ENDIF
	
	RETURN 0
ENDFUNC

/// PURPOSE:
///    Get the payout for a bet in a 'column bet' column   
FUNC INT ROULETTE_BETTING_GET_COLUMN_BET_PAYOUT(ROULETTE_BET sBet, INT iWinningNumberIndex, ROULETTE_BET_TYPE eColumnBetType)
	
	SWITCH eColumnBetType
		CASE ROULETTE_BET_TYPE_COLUMN_FIRST
		CASE ROULETTE_BET_TYPE_COLUMN_SECOND
		CASE ROULETTE_BET_TYPE_COLUMN_THIRD		
			IF ROULETTE_BETTING_IS_ROULETTE_NUMBER_INDEX_IN_OUTSIDE_BET_COLUMN(iWinningNumberIndex, eColumnBetType) 
				RETURN ROULETTE_BETTING_GET_PAYOUT_AMOUNT_FOR_BET_TYPE(sBet.iBetAmount, eColumnBetType)
			ENDIF
		BREAK
		DEFAULT RETURN 0
	ENDSWITCH
	
	RETURN 0
ENDFUNC

/// PURPOSE:
///    Gets all the roulette number indexs that have an even number as their value
PROC ROULETTE_BETTING_GET_ALL_EVEN_NUMBER_INDEXS(INT &iRouletteNumberIndexs[])
	INT i = 0
	INT iInsertIndex = 0
	REPEAT ROULETTE_MAX_ROULETTE_NUMBER + 1 i		
		IF ROULETTE_BETTING_IS_ROULETTE_NUMBER_INDEXS_VALUE_EVEN(i)
			iRouletteNumberIndexs[iInsertIndex] = i
			iInsertindex++
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Gets all the roulette number indexs that have an odd number as their value 
PROC ROULETTE_BETTING_GET_ALL_ODD_NUMBER_INDEXS(INT &iRouletteNumberIndexs[])
	INT i = 0
	INT iInsertIndex = 0
	REPEAT ROULETTE_MAX_ROULETTE_NUMBER + 1 i
		IF ROULETTE_BETTING_IS_ROULETTE_NUMBER_INDEXS_VALUE_ODD(i)
			iRouletteNumberIndexs[iInsertIndex] = i
			iInsertindex++
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Gets all the roulette number indexs that have values of the given colour
PROC ROULETTE_BETTING_GET_ALL_NUMBER_INDEXS_FOR_COLOUR(INT &iRouletteNumberIndexs[], ROULETTE_BET_TYPE eBetTypeColour)
	IF eBetTypeColour != ROULETTE_BET_TYPE_RED AND
	eBetTypeColour != ROULETTE_BET_TYPE_BLACK
		EXIT
	ENDIF

	INT i = 0
	INT iInsertIndex = 0
	REPEAT ROULETTE_MAX_ROULETTE_NUMBER + 1 i
		IF ROULETTE_GET_COLOR_BET_TYPE_OF_ROULETTE_SLOT_INDEX(i) = eBetTypeColour
			iRouletteNumberIndexs[iInsertIndex] = i
			iInsertindex++
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Gets all the number indexs that have values in the given range bet type
PROC ROULETTE_BETTING_GET_ALL_NUMBER_INDEXS_FOR_RANGE_BET(INT &iRouletteNumberIndexs[], ROULETTE_BET_TYPE eRangeBetType)
	INT iRangeMin = 0
	INT iRangeMax = 0
	ROULETTE_BETTING_GET_RANGE_FOR_RANGE_BET(iRangeMin, iRangeMax, eRangeBetType)

	INT i = 0
	INT iInsertIndex = 0
	REPEAT ROULETTE_MAX_ROULETTE_NUMBER + 1 i
		IF ROULETTE_BETTING_IS_ROULETTE_NUMBER_INDEXS_VALUE_IN_RANGE(i, iRangeMin, iRangeMax)
			iRouletteNumberIndexs[iInsertIndex] = i
			iInsertindex++
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Gets all number indexs in a given 'column bet' column
PROC ROULETTE_BETTING_GET_ALL_NUMBER_INDEXS_FOR_COLUMN_BET(INT &iRouletteNumberIndexs[], ROULETTE_BET_TYPE eBetTypeColumn)

	INT i = 0
	INT iInsertIndex = 0
	REPEAT ROULETTE_MAX_ROULETTE_NUMBER + 1 i	
		IF ROULETTE_BETTING_IS_ROULETTE_NUMBER_INDEX_IN_OUTSIDE_BET_COLUMN(i , eBetTypeColumn)
			iRouletteNumberIndexs[iInsertIndex] = i
			iInsertindex++
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Gets all the number indexs associated with the layouts position
PROC ROULETTE_BETTING_GET_ALL_NUMBER_INDEXS_ASSOCIATED_WITH_LAYOUT_POSITION(INT &iAssociatedNumberIndexs[ROULETTE_MAX_ROULETTE_NUMBER], INT iLayoutIndex)
	ROULETTE_BET_TYPE eBetType = ROULETTE_BETTING_GET_BET_TYPE_FROM_LAYOUT_INDEX(iLayoutIndex)
	
	// Init array to invalid indexs
	SET_ALL_IN_INT_ARRAY(iAssociatedNumberIndexs, -1)
	
	SWITCH eBetType			
		// Don't process if bet type is invalid
		CASE ROULETTE_BET_TYPE_INVALID	EXIT		
		// No additional numbers for a straight bet
		CASE ROULETTE_BET_TYPE_STRAIGHT	EXIT
		
		CASE ROULETTE_BET_TYPE_SPLIT
			ROULETTE_BETTING_GET_SPLIT_BET_ROULETTE_NUMBER_INDEXS_FROM_LAYOUT_INDEX(iAssociatedNumberIndexs, iLayoutIndex)
		BREAK
		CASE ROULETTE_BET_TYPE_STREET
			ROULETTE_BETTING_GET_STREET_BET_ROULETTE_NUMBER_INDEXS_FROM_LAYOUT_INDEX(iAssociatedNumberIndexs, iLayoutIndex)
		BREAK
		CASE ROULETTE_BET_TYPE_TRIO
			ROULETTE_BETTING_GET_TRIO_BET_ROULETTE_NUMBER_INDEXS_FROM_LAYOUT_INDEX(iAssociatedNumberIndexs, iLayoutIndex)
		BREAK
		CASE ROULETTE_BET_TYPE_CORNER
			ROULETTE_BETTING_GET_CORNER_BET_ROULETTE_NUMBER_INDEXS_FROM_LAYOUT_INDEX(iAssociatedNumberIndexs, iLayoutIndex)
		BREAK
		CASE ROULETTE_BET_TYPE_FIVE_NUMBER
			ROULETTE_BETTTING_GET_FIVE_NUMBER_BET_ROULETTE_NUMBER_INDEXS_FROM_LAYOUT_INDEX(iAssociatedNumberIndexs)
		BREAK
		CASE ROULETTE_BET_TYPE_SIX_LINE
			ROULETTE_BETTING_GET_SIX_LINE_BET_ROULETTE_NUMBER_INDEXS_FROM_LAYOUT_INDEX(iAssociatedNumberIndexs, iLayoutIndex)
		BREAK	
		CASE ROULETTE_BET_TYPE_RED
			ROULETTE_BETTING_GET_ALL_NUMBER_INDEXS_FOR_COLOUR(iAssociatedNumberIndexs, ROULETTE_BET_TYPE_RED)	
		BREAK
		CASE ROULETTE_BET_TYPE_BLACK
			ROULETTE_BETTING_GET_ALL_NUMBER_INDEXS_FOR_COLOUR(iAssociatedNumberIndexs, ROULETTE_BET_TYPE_BLACK)
		BREAK
		CASE ROULETTE_BET_TYPE_ODD
			ROULETTE_BETTING_GET_ALL_ODD_NUMBER_INDEXS(iAssociatedNumberIndexs)
		BREAK
		CASE ROULETTE_BET_TYPE_EVEN
			ROULETTE_BETTING_GET_ALL_EVEN_NUMBER_INDEXS(iAssociatedNumberIndexs)
		BREAK
		CASE ROULETTE_BET_TYPE_LOW_BRACKET	
			ROULETTE_BETTING_GET_ALL_NUMBER_INDEXS_FOR_RANGE_BET(iAssociatedNumberIndexs, ROULETTE_BET_TYPE_LOW_BRACKET)
		BREAK
		CASE ROULETTE_BET_TYPE_HIGH_BRACKET
			ROULETTE_BETTING_GET_ALL_NUMBER_INDEXS_FOR_RANGE_BET(iAssociatedNumberIndexs, ROULETTE_BET_TYPE_HIGH_BRACKET)	
		BREAK
		CASE ROULETTE_BET_TYPE_DOZEN_FIRST
			ROULETTE_BETTING_GET_ALL_NUMBER_INDEXS_FOR_RANGE_BET(iAssociatedNumberIndexs, ROULETTE_BET_TYPE_DOZEN_FIRST)
		BREAK
		CASE ROULETTE_BET_TYPE_DOZEN_SECOND
			ROULETTE_BETTING_GET_ALL_NUMBER_INDEXS_FOR_RANGE_BET(iAssociatedNumberIndexs, ROULETTE_BET_TYPE_DOZEN_SECOND)
		BREAK
		CASE ROULETTE_BET_TYPE_DOZEN_THIRD
			ROULETTE_BETTING_GET_ALL_NUMBER_INDEXS_FOR_RANGE_BET(iAssociatedNumberIndexs, ROULETTE_BET_TYPE_DOZEN_THIRD)
		BREAK
		CASE ROULETTE_BET_TYPE_COLUMN_FIRST	
			ROULETTE_BETTING_GET_ALL_NUMBER_INDEXS_FOR_COLUMN_BET(iAssociatedNumberIndexs, ROULETTE_BET_TYPE_COLUMN_FIRST)
		BREAK
		CASE ROULETTE_BET_TYPE_COLUMN_SECOND
			ROULETTE_BETTING_GET_ALL_NUMBER_INDEXS_FOR_COLUMN_BET(iAssociatedNumberIndexs, ROULETTE_BET_TYPE_COLUMN_SECOND)
		BREAK
		CASE ROULETTE_BET_TYPE_COLUMN_THIRD	
			ROULETTE_BETTING_GET_ALL_NUMBER_INDEXS_FOR_COLUMN_BET(iAssociatedNumberIndexs, ROULETTE_BET_TYPE_COLUMN_THIRD)
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Gets the payout for the given bet according to the winning number  
FUNC INT ROULETTE_BETTING_GET_ROULETTE_PAYOUT_FOR_BET(ROULETTE_BET &sBet, INT iWinningNumberIndex)
	INT iPayout = 0	
	ROULETTE_BET_TYPE eBetType = ROULETTE_BETTING_GET_BET_TYPE_FROM_LAYOUT_INDEX(sBet.iLayoutIndex)
	
	// Different bet types need to be processed in specific ways
	// due to them having a different amount of numbers associated with them
	SWITCH eBetType	
		
		// Don't process if bet is invalid
		CASE ROULETTE_BET_TYPE_INVALID	RETURN iPayout
			
		CASE ROULETTE_BET_TYPE_STRAIGHT
			iPayout = ROULETTE_BETTING_GET_STRAIGHT_BET_PAYOUT(sBet, iWinningNumberIndex)
		BREAK
		CASE ROULETTE_BET_TYPE_SPLIT
			iPayout = ROULETTE_BETTING_GET_SPLIT_BET_PAYOUT(sBet, iWinningNumberIndex)
		BREAK
		CASE ROULETTE_BET_TYPE_STREET
			iPayout = ROULETTE_BETTING_GET_STREET_BET_PAYOUT(sBet, iWinningNumberIndex)
		BREAK
		CASE ROULETTE_BET_TYPE_TRIO
			iPayout = ROULETTE_BETTING_GET_TRIO_BET_PAYOUT(sBet, iWinningNumberIndex)
		BREAK
		CASE ROULETTE_BET_TYPE_CORNER
			iPayout = ROULETTE_BETTING_GET_CORNER_BET_PAYOUT(sBet, iWinningNumberIndex)
		BREAK
		CASE ROULETTE_BET_TYPE_FIVE_NUMBER
			iPayout = ROULETTE_BETTING_GET_FIVE_NUMBER_BET_PAYOUT(sBet, iWinningNumberIndex)
		BREAK
		CASE ROULETTE_BET_TYPE_SIX_LINE
			iPayout = ROULETTE_BETTING_GET_SIX_LINE_BET_PAYOUT(sBet, iWinningNumberIndex)
		BREAK	
		CASE ROULETTE_BET_TYPE_RED
			iPayout = ROULETTE_BETTING_GET_COLOUR_BET_PAYOUT(sBet, iWinningNumberIndex, ROULETTE_BET_TYPE_RED)
		BREAK
		CASE ROULETTE_BET_TYPE_BLACK
			iPayout = ROULETTE_BETTING_GET_COLOUR_BET_PAYOUT(sBet, iWinningNumberIndex, ROULETTE_BET_TYPE_BLACK)
		BREAK
		CASE ROULETTE_BET_TYPE_ODD
			iPayout = ROULETTE_BETTING_GET_ODD_BET_PAYOUT(sBet, iWinningNumberIndex)
		BREAK
		CASE ROULETTE_BET_TYPE_EVEN
			iPayout = ROULETTE_BETTING_GET_EVEN_BET_PAYOUT(sBet, iWinningNumberIndex)
		BREAK
		CASE ROULETTE_BET_TYPE_LOW_BRACKET	
			iPayout = ROULETTE_BETTING_GET_RANGE_BET_PAYOUT(sBet, iWinningNumberIndex, ROULETTE_BET_TYPE_LOW_BRACKET)
		BREAK
		CASE ROULETTE_BET_TYPE_HIGH_BRACKET
			iPayout = ROULETTE_BETTING_GET_RANGE_BET_PAYOUT(sBet, iWinningNumberIndex, ROULETTE_BET_TYPE_HIGH_BRACKET)
		BREAK
		CASE ROULETTE_BET_TYPE_DOZEN_FIRST
			iPayout = ROULETTE_BETTING_GET_RANGE_BET_PAYOUT(sBet, iWinningNumberIndex, ROULETTE_BET_TYPE_DOZEN_FIRST)
		BREAK
		CASE ROULETTE_BET_TYPE_DOZEN_SECOND
			iPayout = ROULETTE_BETTING_GET_RANGE_BET_PAYOUT(sBet, iWinningNumberIndex, ROULETTE_BET_TYPE_DOZEN_SECOND)
		BREAK
		CASE ROULETTE_BET_TYPE_DOZEN_THIRD
			iPayout = ROULETTE_BETTING_GET_RANGE_BET_PAYOUT(sBet, iWinningNumberIndex, ROULETTE_BET_TYPE_DOZEN_THIRD)
		BREAK
		CASE ROULETTE_BET_TYPE_COLUMN_FIRST
			iPayout = ROULETTE_BETTING_GET_COLUMN_BET_PAYOUT(sBet, iWinningNumberIndex, ROULETTE_BET_TYPE_COLUMN_FIRST)
		BREAK
		CASE ROULETTE_BET_TYPE_COLUMN_SECOND
			iPayout = ROULETTE_BETTING_GET_COLUMN_BET_PAYOUT(sBet, iWinningNumberIndex, ROULETTE_BET_TYPE_COLUMN_SECOND)
		BREAK
		CASE ROULETTE_BET_TYPE_COLUMN_THIRD	
			iPayout = ROULETTE_BETTING_GET_COLUMN_BET_PAYOUT(sBet, iWinningNumberIndex, ROULETTE_BET_TYPE_COLUMN_THIRD)
		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
		STRING sBetType = ROULETTE_GET_BET_TYPE_NAME_AS_STRING(eBetType)
		INT iBetOdd = ROULETTE_BETTING_GET_ODDS_FOR_BET_TYPE(eBetType)
		CPRINTLN(DEBUG_ROULETTE, "Placed bet amount: ", sBet.iBetAmount, ", Odds: 1:", iBetOdd,
		", Pay out: ", iPayout, ", Type: ", sBetType, ", Layout index: ", sBet.iLayoutIndex)
	#ENDIF
	
	RETURN iPayout
ENDFUNC

/// PURPOSE:
///    Checks if the given bet has won  
FUNC BOOL ROULETTE_BETTING_HAS_BET_AT_CURRENT_TABLE_WON(ROULETTE_DATA &sRouletteData, ROULETTE_BET &sBet)
	INT iCurrentWinningNumber = ROULETTE_HELPER_GET_SERVERS_CURRENT_WINNING_NUMBER_FOR_TABLE(sRouletteData, sRouletteData.iTableID)
	RETURN ROULETTE_BETTING_GET_ROULETTE_PAYOUT_FOR_BET(sBet, iCurrentWinningNumber) > 0
ENDFUNC

/// PURPOSE:
///      Gets the bet amount at the layout coord that was made by the local player
FUNC INT ROULETTE_BETTING_GET_LOCAL_PLAYERS_BET_AMOUNT_AT_LAYOUT_COORD(ROULETTE_DATA &sRouletteData, INT iXCoord, INT iYCoord)
	INT iLocalPlayerID = NATIVE_TO_INT(PLAYER_ID())
	INT iLayoutIndexToCheck = ROULETTE_LAYOUT_COORD_TO_LAYOUT_INDEX(iXCoord, iYCoord)
	
	INT i = 0
	REPEAT ROULETTE_BETTING_MAX_ALLOWED_BETS i
		IF sRouletteData.playerBD[iLocalPlayerID].iBets[i].iLayoutIndex = iLayoutIndexToCheck
			RETURN sRouletteData.playerBD[iLocalPlayerID].iBets[i].iBetAmount
		ENDIF
	ENDREPEAT
	
	RETURN 0
ENDFUNC

/// PURPOSE:
///    Checks if the given slot is next to the winning slot on either side  
FUNC BOOL ROULETTE_BETTING_IS_SLOT_ADJACENT_TO_WINNING_SLOT(INT iSlotToCheck, INT iWinningSlot)
	INT iLeftOfBetSlotIndex = WRAP_INDEX(iSlotToCheck - 1, ROULETTE_MAX_ROULETTE_NUMBER)
	INT iRightOfBetSlotIndex =  WRAP_INDEX(iSlotToCheck + 1, ROULETTE_MAX_ROULETTE_NUMBER)
	RETURN iLeftOfBetSlotIndex = iWinningSlot OR iRightOfBetSlotIndex = iWinningSlot
ENDFUNC

/// PURPOSE:
///    Checks if the bet meets the cirteria for setting for a reaction flag. 
///    e.g. lost on a straight bet that was adjacent to the winning number
PROC ROULETTE_BETTING_DETERMINE_BET_REACTION_FLAGS(ROULETTE_DATA& sRouletteData, ROULETTE_BET &sBet, INT iBetsPayout, INT iWinningSlot)
	ROULETTE_BET_TYPE eBetType = ROULETTE_BETTING_GET_BET_TYPE_FROM_LAYOUT_INDEX(sBet.iLayoutIndex)
	
	IF eBetType != ROULETTE_BET_TYPE_STRAIGHT
		EXIT
	ENDIF
	
	INT iBetSlotIndex = ROULETTE_GET_SLOT_INDEX_FROM_LAYOUT_INDEX(sBet.iLayoutIndex)
	BOOL bNearMiss = ROULETTE_BETTING_IS_SLOT_ADJACENT_TO_WINNING_SLOT(iBetSlotIndex, iWinningSlot)
	INT iMaxBet = ROULETTE_BETTING_GET_MAX_INSIDE_BET_FOR_TABLE(sRouletteData.iTableID)
	BOOL bPlacedMax = sBet.iBetAmount >= iMaxBet
	BOOL bWonBet = iBetsPayout > 0
	
	// Lost max bet on a straight
	IF NOT bWonBet AND bPlacedMax
		SET_BIT(sRouletteData.iRouletteBS, ROULETTE_BS_PAYOUT_LOST_MAX_BET_ON_A_STRAIGHT)
	ENDIF
	
	// Lost bet on number that was adjacent to winning
	IF NOT bWonBet AND bNearMiss
		SET_BIT(sRouletteData.iRouletteBS, ROUELTTE_BS_PAYOUT_LOST_ADJACENT_BET_ON_A_STRAIGHT)
	ENDIF
	
	// Won a max bet on a straight
	IF bWonBet AND bPlacedMax
		SET_BIT(sRouletteData.iRouletteBS, ROULETTE_BS_PAYOUT_WON_MAX_BET_ON_A_STRAIGHT)
	ENDIF
ENDPROC

PROC ROULETTE_BETTING_DETERMINE_BET_READY_UP_FLAGS(ROULETTE_DATA& sRouletteData)
	CLEAR_BIT(sRouletteData.iRouletteBS, ROULETTE_BS_READY_PLACED_MAX_BET_ON_A_STRAIGHT)
	
	INT iMaxBet = ROULETTE_BETTING_GET_MAX_INSIDE_BET_FOR_TABLE(sRouletteData.iTableID)
	INT iPlayerID = NATIVE_TO_INT(PLAYER_ID())
	
	INT i
	REPEAT ROULETTE_BETTING_MAX_ALLOWED_BETS i	
		ROULETTE_BET sCurrentBet = sRouletteData.playerBD[iPlayerID].iBets[i]
		
		// Not a valid bet so skip
		IF sCurrentBet.iLayoutIndex = -1
			RELOOP			
		ENDIF
		
		ROULETTE_BET_TYPE eBetType = ROULETTE_BETTING_GET_BET_TYPE_FROM_LAYOUT_INDEX(sCurrentBet.iLayoutIndex)
	
		IF eBetType = ROULETTE_BET_TYPE_STRAIGHT 
		AND sCurrentBet.iBetAmount >= iMaxBet
			SET_BIT(sRouletteData.iRouletteBS, ROULETTE_BS_READY_PLACED_MAX_BET_ON_A_STRAIGHT)
		ENDIF

	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Clears the flags used to determine the reaction to the bet payouts
PROC ROULETTE_BETTING_CLEAR_BET_REACTION_FLAGS(ROULETTE_DATA& sRouletteData)
	CLEAR_BIT(sRouletteData.iRouletteBS, ROULETTE_BS_PAYOUT_LOST_MAX_BET_ON_A_STRAIGHT)
	CLEAR_BIT(sRouletteData.iRouletteBS, ROUELTTE_BS_PAYOUT_LOST_ADJACENT_BET_ON_A_STRAIGHT)
	CLEAR_BIT(sRouletteData.iRouletteBS, ROULETTE_BS_PAYOUT_WON_MAX_BET_ON_A_STRAIGHT)
ENDPROC

/// PURPOSE:
///    Gets the payout for the local players current bets according
///    to the current winning number for the table the player is sitting at
FUNC INT ROULETTE_BETTING_GET_PAYOUT_FOR_LOCAL_PLAYERS_BETS(ROULETTE_DATA& sRouletteData)

	INT iWinningNumber = ROULETTE_HELPER_GET_SERVERS_CURRENT_WINNING_NUMBER_FOR_TABLE(sRouletteData, sRouletteData.iTableID)
	
	CPRINTLN(DEBUG_ROULETTE, " ")
	CPRINTLN(DEBUG_ROULETTE, "-------------ROULETTE_PAYOUT------------")
	
	// Server likely hasn't generated a winning number so bail
	IF iWinningNumber = -1
		CPRINTLN(DEBUG_ROULETTE, "	Server has not generated winning number!")
		RETURN 0
	ENDIF
	
	INT iSlotIndex = ROULETTE_GET_SLOT_INDEX_FROM_ROULETTE_NUMBER(iWinningNumber)
	
	// Print info about the winning number
	CPRINTLN(DEBUG_ROULETTE, "	Winning slot index is: ", iSlotIndex)
	CPRINTLN(DEBUG_ROULETTE, "	Winning layout index is: ", ROULETTE_GET_LAYOUT_INDEX_FROM_SLOT_INDEX(iSlotIndex))
	CPRINTLN(DEBUG_ROULETTE, "	Winning number is: ", iWinningNumber)
	
	INT iPlayerID = NATIVE_TO_INT(PLAYER_ID())
	INT iChipPayout = 0	
	INT i = 0
	
	ROULETTE_BETTING_CLEAR_BET_REACTION_FLAGS(sRouletteData)
	
	FORCE_CHECK_SCRIPT_VARIABLES()
	REPEAT ROULETTE_BETTING_MAX_ALLOWED_BETS i	
		
		// Get the current bet being processed in the local player bets
		ROULETTE_BET sCurrentBet = sRouletteData.playerBD[iPlayerID].iBets[i]
		INT iBetPayout = ROULETTE_BETTING_GET_ROULETTE_PAYOUT_FOR_BET(sCurrentBet, iSlotIndex)	
		iChipPayout += iBetPayout
		
		// We need to know if this bet triggers certain reaction conditions
		ROULETTE_BETTING_DETERMINE_BET_REACTION_FLAGS(sRouletteData, sCurrentBet, iBetPayout, iSlotIndex)	
	ENDREPEAT

	CPRINTLN(DEBUG_ROULETTE, "	Total payout for bets: ", iChipPayout)
	RETURN iChipPayout
ENDFUNC

/// PURPOSE:
///    Increments the chip denomination enum to the next chip with wrap around behaviour   
FUNC ROULETTE_CHIP_DENOMINATIONS ROULETTE_BETTING_INCREMENT_CHIP_DENOMINATION(ROULETTE_CHIP_DENOMINATIONS eDenomination, INT iIncrement)
	INT iDenom = ENUM_TO_INT(eDenomination)
	iDenom += iIncrement
	iDenom = WRAP_INDEX(iDenom, COUNT_OF(ROULETTE_CHIP_DENOMINATIONS))
	RETURN INT_TO_ENUM(ROULETTE_CHIP_DENOMINATIONS, iDenom)	
ENDFUNC

/// PURPOSE:
///    Gets the min and max bet limits for the given coord on the layout for the current table
PROC ROULETTE_BETTING_GET_BET_MIN_MAX_RANGE_FOR_LAYOUT_COORD(ROULETTE_DATA &sRouletteData, INT iXCoord, INT iYCoord, INT &iMinBet, INT &iMaxBet)
	IF ROULETTE_LAYOUT_IS_COORD_ON_AN_INSIDE_BET_CELL(iXCoord, iYCoord)
		iMinBet = ROULETTE_BETTING_GET_MIN_INSIDE_BET_FOR_CURRENT_TABLE(sRouletteData)
		iMaxBet = ROULETTE_BETTING_GET_MAX_INSIDE_BET_FOR_CURRENT_TABLE(sRouletteData)
		EXIT
	ENDIF
	
	iMinBet = ROULETTE_BETTING_GET_MIN_OUTSIDE_BET_FOR_CURRENT_TABLE(sRouletteData)
	iMaxBet = ROULETTE_BETTING_GET_MAX_OUTSIDE_BET_FOR_CURRENT_TABLE(sRouletteData)
ENDPROC

/// PURPOSE:
///    Gets validates the given denomination changing it to the next valid if it isn't within the current bet limits, 
///    iIncrement determines if it will try lower or higher denoms for getting the next valid one  
FUNC ROULETTE_CHIP_DENOMINATIONS ROULETTE_BETTING_GET_VALID_CHIP_DENOMINATION(ROULETTE_DATA &sRouletteData, ROULETTE_CHIP_DENOMINATIONS eDenomination, INT iIncrement = -1)	
	INT iValue = ROULETTE_GET_CHIP_DENOMINATION_VALUE(eDenomination)	
	INT iRangeMin = 0
	INT iRangeMax = 0
	ROULETTE_BETTING_GET_BET_MIN_MAX_RANGE_FOR_LAYOUT_COORD(sRouletteData, sRouletteData.iCursorXCoord, sRouletteData.iCursorYCoord, iRangeMin, iRangeMax)
	
	INT iSafetyCount = 0
	INT iMaxDenomination = COUNT_OF(ROULETTE_CHIP_DENOMINATIONS)	
	WHILE NOT IS_INT_IN_RANGE_INCLUSIVE(iValue,  iRangeMin, iRangeMax) AND iSafetyCount <= iMaxDenomination
		eDenomination = ROULETTE_BETTING_INCREMENT_CHIP_DENOMINATION(eDenomination, iIncrement)
		iValue = ROULETTE_GET_CHIP_DENOMINATION_VALUE(eDenomination)
		iSafetyCount++
	ENDWHILE
	
	RETURN eDenomination
ENDFUNC

/// PURPOSE:
///    Gets the max possible valid bet for the current cursor position  
FUNC ROULETTE_CHIP_DENOMINATIONS ROULETTE_BETTING_GET_MAX_VALID_CHIP_DENOMINATION(ROULETTE_DATA &sRouletteData)
	INT iRangeMin = 0
	INT iRangeMax = 0
	ROULETTE_BETTING_GET_BET_MIN_MAX_RANGE_FOR_LAYOUT_COORD(sRouletteData, sRouletteData.iCursorXCoord, sRouletteData.iCursorYCoord, iRangeMin, iRangeMax)
	RETURN ROULETTE_GET_LARGEST_CHIP_DENOMINATION_FOR_GIVEN_VALUE(IMIN(iRangeMax, ROULETTE_BETTING_GET_LOCAL_PLAYER_CHIP_COUNT()))
ENDFUNC

/// PURPOSE:
///    Gets the max possible valid bet for the current cursor position   
FUNC ROULETTE_CHIP_DENOMINATIONS ROULETTE_BETTING_GET_MIN_VALID_CHIP_DENOMINATION(ROULETTE_DATA &sRouletteData)
	INT iRangeMin = 0
	INT iRangeMax = 0
	ROULETTE_BETTING_GET_BET_MIN_MAX_RANGE_FOR_LAYOUT_COORD(sRouletteData, sRouletteData.iCursorXCoord, sRouletteData.iCursorYCoord, iRangeMin, iRangeMax)
	
	ROULETTE_CHIP_DENOMINATIONS eDenomination = TEN_DOLLAR_CHIP
	INT iValue = ROULETTE_GET_CHIP_DENOMINATION_VALUE(eDenomination)
	WHILE iValue < iRangeMin
		eDenomination = ROULETTE_BETTING_INCREMENT_CHIP_DENOMINATION(eDenomination, 1)
		iValue = ROULETTE_GET_CHIP_DENOMINATION_VALUE(eDenomination)
	ENDWHILE
	
	RETURN eDenomination
ENDFUNC

/// PURPOSE:
///    Increments the denomination to the next valid one, taking into account betting limits for the cursors position on the current table   
FUNC ROULETTE_CHIP_DENOMINATIONS ROULETTE_BETTING_INCREMENT_TO_NEXT_VALID_CHIP_DENOMINATION(ROULETTE_DATA &sRouletteData, ROULETTE_CHIP_DENOMINATIONS eDenomination, INT iIncrement)	
	ROULETTE_CHIP_DENOMINATIONS eNewDenom =	ROULETTE_BETTING_INCREMENT_CHIP_DENOMINATION(eDenomination, iIncrement)
	RETURN ROULETTE_BETTING_GET_VALID_CHIP_DENOMINATION(sRouletteData, eNewDenom, iIncrement)
ENDFUNC


