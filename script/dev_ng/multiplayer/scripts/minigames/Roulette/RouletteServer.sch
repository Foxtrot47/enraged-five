//////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Name:        RouletteServer.sch																			
/// Description: Header for running the roulette servers overall state, i.e. processing seat requests and	
///				running the server game state when players have joined										
///																											
/// Written by:  Online Technical Team: Tom Turner,															
/// Date:  		12/02/2019																					
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "RouletteData.sch"
USING "RouletteServerState.sch"
USING "RouletteGameServer.sch"
USING "RouletteHelper.sch"
USING "RouletteProps.sch"
USING "RouletteAnimation.sch"
USING "RouletteSeatRequesting.sch"
USING "RouletteTable.sch"

/// PURPOSE:
///    Sets all seats as available
PROC ROULETTE_SERVER_INIT_SEATS(ROULETTE_DATA &sRouletteData)
	INT i = 0
	INT iMaxSeat = COUNT_OF(sRouletteData.serverBD.playersUsingSeats)
	REPEAT iMaxSeat i
		sRouletteData.serverBD.playersUsingSeats[i] = INVALID_PLAYER_INDEX()
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Sets a new state for the server, will display a state change log
PROC ROULETTE_SERVER_SET_STATE(ROULETTE_DATA &sRouletteData, INT iTableID, ROULETTE_SERVER_STATE eNextState, ROULETTE_TRANSITION_FUNC onExit, ROULETTE_TRANSITION_FUNC onEnter)
	CPRINTLN(DEBUG_ROULETTE, "")
	CPRINTLN(DEBUG_ROULETTE, "SET_SERVER_STATE - State changed for table: ",iTableID, ", ",
	GET_ROULETTE_SERVER_STATE_NAME(sRouletteData.serverBD.eServerState[iTableID]),
	" -> ", GET_ROULETTE_SERVER_STATE_NAME(eNextState))
	
	CALL onExit(sRouletteData)
	sRouletteData.serverBD.eServerState[iTableID] = eNextState
	CALL onEnter(sRouletteData)
ENDPROC

///------------------------------------    
///	On Updates
///------------------------------------   

/// PURPOSE:
///     Update state for server init on a specific table
PROC ROULETTE_SERVER_STATE_INIT_UPDATE(ROULETTE_DATA &sRouletteData, INT iTableID)
	IF NOT ROULETTE_PROPS_CREATE_DEALER_PEDS(sRouletteData)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_SERVER_STATE_INIT_UPDATE - Waiting for dealers to be created")
		EXIT
	ENDIF
	
	IF NOT ROULETTE_ANIM_HAVE_DEALER_DICTIONARIES_LOADED()
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_SERVER_STATE_INIT_UPDATE - Waiting for dealers dictionaries to be loaded")
		EXIT
	ENDIF
	
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_SERVER_STATE_INIT_UPDATE - Finished init, playing dealer idles")
	ROULETTE_ANIM_PLAY_DEALER_IDLE_ANIM_ON_ALL_DEALERS(sRouletteData, TRUE)
	ROULETTE_SERVER_SET_STATE(sRouletteData, iTableID, ROULETTE_SERVER_STATE_IDLE,
	&ROULETTE_DEFAULT_ON_EXIT, &ROULETTE_DEFAULT_ON_ENTER)
ENDPROC

/// PURPOSE:
///    Update state for server idle on a specific table
PROC ROULETTE_SERVER_STATE_IDLE_UPDATE(ROULETTE_DATA &sRouletteData, INT iTableID)	
	// Maintain the dealers idle anim even if a game is not in progress
	ROULETTE_ANIM_MAINTAIN_DEALER_IDLE(sRouletteData, iTableID)
	
	INT iLocalSeatID = 0
	REPEAT ROULETTE_TABLE_MAX_NUMBER_OF_PLAYERS_PER_TABLE iLocalSeatID
		INT iSeatID = ROULETTE_HELPER_GET_SEAT_ID_FROM_LOCAL_SEAT_ID_AT_TABLE(iLocalSeatID, iTableID)
		PLAYER_INDEX piPlayerID = sRouletteData.serverBD.playersUsingSeats[iSeatID]
		
		// Check if any player is playing at our table
		IF piPlayerID != INVALID_PLAYER_INDEX() 
		AND IS_BIT_SET(sRouletteData.playerBD[NATIVE_TO_INT(piPlayerID)].iPlayerBS, ROULETTE_PLAYER_BS_FINISHED_JOINING_TABLE)			
			
			// Get game state ready for player
			sRouletteData.serverBD.eGameState[iTableID] = ROULETTE_GAME_STATE_INIT			
			START_NET_TIMER(sRouletteData.serverBD.stBettingTimer[iTableID])
			ROULETTE_SERVER_SET_STATE(sRouletteData, iTableID, ROULETTE_SERVER_STATE_PLAYING,
			&ROULETTE_DEFAULT_ON_EXIT, &ROULETTE_DEFAULT_ON_ENTER)
			EXIT
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Update state for server playing on a specific table
PROC ROULETTE_SERVER_STATE_PLAYING_UPDATE(ROULETTE_DATA &sRouletteData, INT iTableID)	
	// Run the roulette game server until its finished
	IF NOT ROULLETTE_GAME_SERVER_UPDATE(sRouletteData, iTableID)
		ROULETTE_ANIM_MAINTAIN_DEALER_IDLE(sRouletteData, iTableID)
		ROULETTE_SERVER_SET_STATE(sRouletteData, iTableID, ROULETTE_SERVER_STATE_IDLE,
		&ROULETTE_DEFAULT_ON_EXIT, &ROULETTE_DEFAULT_ON_ENTER)	
	ENDIF
ENDPROC

///------------------------------------    
///	State machine
///------------------------------------

/// PURPOSE:
///    Roullette server initialization for anything pre-game
PROC ROULETTE_SERVER_PRE_GAME_INIT(ROULETTE_DATA &sRouletteData)
	ROULETTE_SERVER_INIT_SEATS(sRouletteData)
	ROULETTE_PROPS_RESERVE_REQUIRED_PEDS()
	ROULETTE_PROPS_LOAD_DEALER_PED_MODELS()
	ROULETTE_ANIM_LOAD_DEALER_DICTIONARIES()
	
	SET_ALL_IN_INT_ARRAY(sRouletteData.iWinHistory, -1)
	SET_ALL_IN_INT_ARRAY(sRouletteData.serverBD.iDealerSyncSceneID, -1)
	SET_BIT(sRouletteData.serverBD.iServerBS, ROULETTE_SERVER_BS_UPDATED_INIT_DATA)
	
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_SERVER_PRE_GAME_INIT - Completed")
ENDPROC

/// PURPOSE:
///    Updates the servers wheel spin init data with the hosts local data for the table
PROC ROULETTE_SERVER_UPDATE_WHEEL_SPIN_INIT_DATA_FOR_TABLE(ROULETTE_DATA &sRouletteData, INT iTableID)
	ROULETTE_WHEEL_SPIN_DATA sData = ROULETTE_ANIM_GET_WHEEL_SPIN_DATA(sRouletteData.sSpin[iTableID])
		
	/// If in the outro ensure it has the up to date spin result from the server
	/// this will eventually get cleared by the client after the outro anim is played     
	IF sData.eWheelSpinState = ROULETTE_ANIM_CLIP_PROP_WHEEL_SPIN_OUTRO
		sData.iTargetNumber = sRouletteData.serverBD.iSpinResult[iTableID]
	ENDIF
	
	// Copy over the current state of the tables spin
	COPY_SCRIPT_STRUCT(sRouletteData.serverBD.sSpinInitData[iTableID], sData,
	SIZE_OF(ROULETTE_WHEEL_SPIN_DATA))
ENDPROC

/// PURPOSE:
///    Updates the servers init history with the host local history for the table
PROC ROULETTE_SERVER_UPDATE_TABLE_RESULT_HISTORY_INIT_DATA(ROULETTE_DATA &sRouletteData)
	COPY_INT_ARRAY(sRouletteData.serverBD.iInitWinHistory, sRouletteData.iWinHistory)
	COPY_INT_ARRAY(sRouletteData.serverBD.iInitWinHistoryStoredCount, sRouletteData.iWinHistoryStoredCount)
ENDPROC

/// PURPOSE:
///    Updates all the initialisation data with the hosts current state
PROC ROULETTE_SERVER_UPDATE_INIT_DATA(ROULETTE_DATA &sRouletteData)
	INT iTableID = 0
	
	REPEAT ROULETTE_TABLE_MAX_NUMBER_OF_TABLES iTableID
		ROULETTE_SERVER_UPDATE_WHEEL_SPIN_INIT_DATA_FOR_TABLE(sRouletteData, iTableID)		
	ENDREPEAT	
	
	ROULETTE_SERVER_UPDATE_TABLE_RESULT_HISTORY_INIT_DATA(sRouletteData)
ENDPROC

/// PURPOSE:
///    Processes incoming requests for the server to update its init data
PROC ROULETTE_SERVER_PROCESS_INIT_DATA_REQUESTS(ROULETTE_DATA &sRouletteData)		
	INT i = 0
	
	REPEAT NUM_NETWORK_PLAYERS i
		IF IS_BIT_SET(sRouletteData.playerBD[i].iPlayerBS, ROULETTE_PLAYER_BS_REQUIRES_INIT_DATA)
			CPRINTLN(DEBUG_ROULETTE, "ROULETTE_SERVER_PROCESS_INIT_DATA_REQUESTS - ",
			"Player: ", i, " has requested init data")
			
			ROULETTE_SERVER_UPDATE_INIT_DATA(sRouletteData)
			SET_BIT(sRouletteData.serverBD.iServerBS, ROULETTE_SERVER_BS_UPDATED_INIT_DATA)
			EXIT
		ENDIF
	ENDREPEAT
	
	CLEAR_BIT(sRouletteData.serverBD.iServerBS, ROULETTE_SERVER_BS_UPDATED_INIT_DATA)
ENDPROC

/// PURPOSE:
///    Ensures the dealer exists, creating them if they do not
PROC ROULETTE_SERVER_MAINTAIN_DEALER(ROULETTE_DATA &sRouletteData, INT iTableID)
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(sRouletteData.serverBD.niDealerPed[iTableID]) 
	OR ROULETTE_TABLE_IS_DISABLED(iTableID)
		EXIT
	ENDIF
	
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_SERVER_MAINTAIN_DEALER - dealer missing for table ", iTableID)
	
	// url:bugstar:6069578 - Need to make sure we reserve before creating
	IF NOT IS_BIT_SET(sRouletteData.serverBD.iTableBS[iTableID], ROULETTE_SERVER_TABLE_BS_RECREATING_DEALER)
		RESERVE_NETWORK_MISSION_PEDS(GET_NUM_CREATED_MISSION_PEDS() + 1)
		SET_BIT(sRouletteData.serverBD.iTableBS[iTableID], ROULETTE_SERVER_TABLE_BS_RECREATING_DEALER)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_SERVER_MAINTAIN_DEALER - reserving ped for dealer :", iTableID)
	ENDIF	
	
	IF ROULETTE_PROPS_CREATE_DEALER_PED(sRouletteData, iTableID)
		ROULETTE_ANIM_PLAY_DEALER_IDLE_ANIM_ON_TABLES_DEALER(sRouletteData, iTableID)
		CLEAR_BIT(sRouletteData.serverBD.iTableBS[iTableID], ROULETTE_SERVER_TABLE_BS_RECREATING_DEALER)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_SERVER_MAINTAIN_DEALER - ped created for:", iTableID)
	ENDIF
ENDPROC

/// PURPOSE:
///    Deletes all the dealers
PROC ROULETTE_SERVER_CLEAN_UP_ALL_DEALERS(ROULETTE_DATA &sRouletteData)
	INT i = 0
	
	REPEAT ROULETTE_TABLE_MAX_NUMBER_OF_TABLES i
		ROULETTE_ANIM_STOP_DEALER_SYNC_SCENE(sRouletteData, i)
		ROULETTE_PROPS_CLEAN_UP_DEALER(sRouletteData.serverBD.niDealerPed[i])
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Clean up for the server
PROC ROULETTE_SERVER_CLEANUP(ROULETTE_DATA &sRouletteData)
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_SERVER_CLEANUP - Called")
	
	// If there are no other participants that will become host
	// clean up the dealers
	IF NETWORK_GET_NUM_PARTICIPANTS() <= 1
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_SERVER_CLEANUP - Cleaning up dealers")
		ROULETTE_SERVER_CLEAN_UP_ALL_DEALERS(sRouletteData)
	ENDIF
ENDPROC

/// PURPOSE:
///    Main entry point for updating a roulette servers logic
///    should only be run by the host
PROC ROULETTE_SERVER_UPDATE(ROULETTE_DATA &sRouletteData)	
	// Exit if not the host
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_SERVER_UPDATE - Is not network host, bailing")
		EXIT
	ENDIF

	// Should always run regardless of table 
	ROULETTE_SEATS_MAINTAIN_REQUESTS(sRouletteData)
	
	// Update spin init data if new participant requests it
	ROULETTE_SERVER_PROCESS_INIT_DATA_REQUESTS(sRouletteData)
	
	// In case of host swap make sure dealer dictionaries are always loaded
	ROULETTE_ANIM_LOAD_DEALER_DICTIONARIES()
	
	INT iTableID = 0
	// Update server state for all tables
	REPEAT ROULETTE_TABLE_MAX_NUMBER_OF_TABLES iTableID
		
		IF ROULETTE_TABLE_IS_DISABLED(iTableID)
			RELOOP
		ENDIF		
		
		// Update table state
		SWITCH sRouletteData.serverBD.eServerState[iTableID]
			CASE ROULETTE_SERVER_STATE_INIT
				ROULETTE_SERVER_STATE_INIT_UPDATE(sRouletteData, iTableID)
			BREAK
			CASE ROULETTE_SERVER_STATE_IDLE
				ROULETTE_SERVER_MAINTAIN_DEALER(sRouletteData, iTableID)
				ROULETTE_SERVER_STATE_IDLE_UPDATE(sRouletteData, iTableID)
			BREAK
			CASE ROULETTE_SERVER_STATE_PLAYING
				ROULETTE_SERVER_MAINTAIN_DEALER(sRouletteData, iTableID)
				ROULETTE_SERVER_STATE_PLAYING_UPDATE(sRouletteData, iTableID)
			BREAK			
		ENDSWITCH
	ENDREPEAT
ENDPROC
