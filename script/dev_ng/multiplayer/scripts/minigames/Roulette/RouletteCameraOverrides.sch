//////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Name:        RouletteCameraOverrides.sch																			
/// Description: Header containing all overrideable functions and parameters relating to the roulette camera
///				these should be overridden if different camera views are desired for a new roulette setup
///																											
/// Written by:  Online Technical Team: Tom Turner,															
/// Date:  		15/03/2019																					
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "freemode_header.sch"
USING "RouletteCameraViews.sch"
USING "RouletteTableOverrides.sch"

#IF NOT DEFINED(ROULETTE_CAM_FIRST_PERSON_PITCH_LIMIT) 		CONST_FLOAT ROULETTE_CAM_FIRST_PERSON_PITCH_LIMIT 35.0 		#ENDIF
#IF NOT DEFINED(ROULETTE_CAM_FIRST_PERSON_HEADING_LIMIT)	CONST_FLOAT ROULETTE_CAM_FIRST_PERSON_HEADING_LIMIT 180.0 	#ENDIF

#IF NOT DEFINED(ROULETTE_CAM_MAX_LAYOUT_MOVEMENT_LEFT) 	TWEAK_FLOAT ROULETTE_CAM_MAX_LAYOUT_MOVEMENT_LEFT 0.15 #ENDIF
#IF NOT DEFINED(ROULETTE_CAM_MAX_LAYOUT_MOVEMENT_RIGHT) TWEAK_FLOAT ROULETTE_CAM_MAX_LAYOUT_MOVEMENT_RIGHT 0.15 #ENDIF
#IF NOT DEFINED(ROULETTE_CAM_MAX_LAYOUT_MOVEMENT_UP) 	TWEAK_FLOAT ROULETTE_CAM_MAX_LAYOUT_MOVEMENT_UP 0.15 #ENDIF
#IF NOT DEFINED(ROULETTE_CAM_MAX_LAYOUT_MOVEMENT_DOWN) 	TWEAK_FLOAT ROULETTE_CAM_MAX_LAYOUT_MOVEMENT_DOWN 0.15 #ENDIF
#IF NOT DEFINED(ROULETTE_CAM_LAYOUT_MOVEMENT_SPEED)		TWEAK_FLOAT ROULETTE_CAM_LAYOUT_MOVEMENT_SPEED 3.2 #ENDIF
#IF NOT DEFINED(ROULETTE_CAM_LAYOUT_MOVEMENT_PAN_MOVE_PERCENTAGE) TWEAK_FLOAT ROULETTE_CAM_LAYOUT_MOVEMENT_PAN_MOVE_PERCENTAGE 0.8 #ENDIF

#IF NOT DEFINED(ROULETTE_CAM_WHEEL_CAM_TARGET_FOV) TWEAK_FLOAT ROULETTE_CAM_WHEEL_CAM_TARGET_FOV 40.0 #ENDIF

#IF NOT DEFINED(ROULETTE_CAM_GET_VIEW_POSITION_OFFSET)
	/// PURPOSE:
	///    Default function for getting a cam position
	///    this default offset is for the casino roulette table 
	FUNC VECTOR ROULETTE_CAM_GET_VIEW_POSITION_OFFSET(ROULETTE_CAM_VIEW eCameraOffsetToGet, INT iLocalSeatID)
		UNUSED_PARAMETER(iLocalSeatID)
		SWITCH eCameraOffsetToGet
			CASE ROULETTE_CAM_VIEW_LAYOUT		RETURN  <<0.0680, -0.2737, 3.1523>>
			CASE ROULETTE_CAM_VIEW_WHEEL		RETURN	<<-0.2713, -0.2519, 1.6719>>
		ENDSWITCH
	
		RETURN <<0, 0, 0>>
	ENDFUNC
#ENDIF

#IF NOT DEFINED(ROULETTE_CAM_GET_VIEW_LOOK_AT_OFFSET)
	/// PURPOSE:
	///    Default function for getting a cam look target position
	///    this default offset is for the casino roulette table 
	FUNC VECTOR ROULETTE_CAM_GET_VIEW_LOOK_AT_OFFSET(ROULETTE_CAM_VIEW eCameraOffsetToGet, INT iLocalSeatID)
		UNUSED_PARAMETER(iLocalSeatID)
		SWITCH eCameraOffsetToGet
			CASE ROULETTE_CAM_VIEW_LAYOUT		RETURN  <<0.0677, -0.0155, 0.1634>>
			CASE ROULETTE_CAM_VIEW_WHEEL		RETURN	<<-1.7807, 0.5194, -0.8033>>
		ENDSWITCH
	
		RETURN <<0, 0, 0>>
	ENDFUNC
#ENDIF

#IF NOT DEFINED(ROULETTE_CAM_GET_REACTION_VIEW_FOV)
	/// PURPOSE:
	///    Gets the roulette reaction camera fov depending on the 
	///    local players current seat
	FUNC FLOAT ROULETTE_CAM_GET_REACTION_VIEW_FOV(INT iLocalSeatID)				
		SWITCH iLocalSeatID		
			CASE 0 RETURN 41.3121
			CASE 1 RETURN 34.6329
			CASE 2 RETURN 34.6703
			CASE 3 RETURN 32.7758
		ENDSWITCH
		
		RETURN 34.6329
	ENDFUNC
#ENDIF

#IF NOT DEFINED(ROULETTE_CAM_GET_VIEW_FOV)
	/// PURPOSE:
	///    Default function for getting a cam views fov
	FUNC FLOAT ROULETTE_CAM_GET_VIEW_FOV(ROULETTE_CAM_VIEW eCameraFOVToGet, INT iLocalSeatID)
		SWITCH eCameraFOVToGet
			CASE ROULETTE_CAM_VIEW_LAYOUT		RETURN 41.1502
			CASE ROULETTE_CAM_VIEW_WHEEL		RETURN 50.0
			CASE ROULETTE_CAM_VIEW_REACTION 	RETURN ROULETTE_CAM_GET_REACTION_VIEW_FOV(iLocalSeatID)
		ENDSWITCH
	
		RETURN 0.0
	ENDFUNC
#ENDIF
