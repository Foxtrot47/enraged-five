//////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Name:        RouletteCameras.sch																		
/// Description: Header for controlling the roulette camera (switching between pre-defined views) 														
///																											
/// Written by:  Online Technical Team: Tom Turner,															
/// Date:  		01/03/2019																					
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "RouletteData.sch"
USING "RouletteHelper.sch"
USING "RouletteCameraViews.sch"
USING "RouletteTable.sch"
USING "RouletteUI.sch"
USING "rc_helper_functions.sch"
USING "RouletteAnimationWheel.sch"
USING "RouletteCameraOverrides.sch"
USING "RouletteProps.sch"

/// PURPOSE:
///    Gets the gameplay view mode equivelant of the roulette view.
///    e.g. ROULETTE_CAM_VIEW_FIRST_PERSON returns CAM_VIEW_MODE_FIRST_PERSON
FUNC CAM_VIEW_MODE ROULETTE_CAMERA_GET_GAMEPLAY_VIEW_MODE_FROM_ROULETTE_VIEW(ROULETTE_CAM_VIEW eCamView)	
	IF eCamView = ROULETTE_CAM_VIEW_THIRD_PERSON
		RETURN CAM_VIEW_MODE_THIRD_PERSON_MEDIUM
	ENDIF
	
	IF eCamView = ROULETTE_CAM_VIEW_FIRST_PERSON
		RETURN CAM_VIEW_MODE_FIRST_PERSON	
	ENDIF
	
	CPRINTLN("ROULETTE_CAMERA_GET_GAMEPLAY_VIEW_MODE_FROM_ROULETTE_VIEW - didn't recognise roulette view as a gameplay view: ",
	ROULETTE_CAM_VIEW_GET_CAM_VIEW_AS_STRING(eCamView))
	RETURN CAM_VIEW_MODE_THIRD_PERSON_MEDIUM
ENDFUNC

/// PURPOSE:
///    Checks if the roulette camera is currently interpolating between positions  
FUNC BOOL ROULETTE_CAMERA_IS_INTERPOLATING(ROULETTE_DATA &sRouletteData)
	IF sRouletteData.ciRouletteCamera != NULL
		RETURN IS_CAM_INTERPOLATING(sRouletteData.ciRouletteCamera) OR IS_INTERPOLATING_TO_SCRIPT_CAMS()
	ENDIF

	RETURN IS_INTERPOLATING_TO_SCRIPT_CAMS()
ENDFUNC

/// PURPOSE:
///    Sets if the main roulette camera is enabled or disabled
PROC ROULETTE_CAMERA_SET_MAIN_CAM_ENABLED(ROULETTE_DATA &sRouletteData, BOOL bEnabled, BOOL bInterp = TRUE)
	IF sRouletteData.ciRouletteCamera = NULL
		EXIT
	ENDIF
	
	SET_CAM_ACTIVE(sRouletteData.ciRouletteCamera, bEnabled)
	RENDER_SCRIPT_CAMS(bEnabled, bInterp)	
ENDPROC

/// PURPOSE:
///    Enables the reaction camera
PROC ROULETTE_CAMERA_SET_REACTION_CAM_ENABLED(ROULETTE_DATA &sRouletteData, BOOL bEnabled)
	IF sRouletteData.ciReactionCamera = NULL
		EXIT
	ENDIF
	
	SET_CAM_ACTIVE(sRouletteData.ciReactionCamera, bEnabled)
	RENDER_SCRIPT_CAMS(bEnabled, FALSE)	
ENDPROC

/// PURPOSE:
///    Safely switches the roulette view to the third person gameplay camera
PROC ROULETTE_CAMERA_SWITCH_TO_THIRD_PERSON(ROULETTE_DATA &sRouletteData)
	SET_CAM_ACTIVE(sRouletteData.ciRouletteCamera, FALSE)
	RENDER_SCRIPT_CAMS(FALSE, FALSE)	
	
	NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_LEAVE_CAMERA_CONTROL_ON)
	SET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT, CAM_VIEW_MODE_THIRD_PERSON_MEDIUM)	
	sRouletteData.eCurrentCameraView = ROULETTE_CAM_VIEW_THIRD_PERSON
	sRouletteData.eInitialView = CAM_VIEW_MODE_THIRD_PERSON_MEDIUM
ENDPROC

/// PURPOSE:
///    Safely switches the roulette view to the first person gameplay camera
PROC ROULETTE_CAMERA_SWITCH_TO_FIRST_PERSON(ROULETTE_DATA &sRouletteData)
	SET_CAM_ACTIVE(sRouletteData.ciRouletteCamera, FALSE)
	RENDER_SCRIPT_CAMS(FALSE, FALSE)	
	
	NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_LEAVE_CAMERA_CONTROL_ON)
	SET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT, CAM_VIEW_MODE_FIRST_PERSON)
	sRouletteData.eCurrentCameraView = ROULETTE_CAM_VIEW_FIRST_PERSON
	sRouletteData.eInitialView = CAM_VIEW_MODE_FIRST_PERSON
ENDPROC

/// PURPOSE:
///    Plays the reaction camera animation on the reaction camera according to the local players
///    current seat
PROC ROULETTE_CAMERA_PLAY_REACTION_CAMERA_ANIMATION(ROULETTE_DATA &sRouletteData)
	OBJECT_INDEX objTable = ROULETTE_HELPER_GET_TABLE_OBJECT_FOR_ID(sRouletteData, sRouletteData.iTableID)
	IF NOT IS_ENTITY_ALIVE(objTable)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_CAMERA_SWITCH_TO_ANIMATED_REACTION_CAMERA - Table does not exist")
		EXIT
	ENDIF
		
	INT iLocalSeatID = ROULETTE_HELPER_GET_LOCAL_PLAYERS_LOCAL_SEAT_ID(sRouletteData)	
	STRING strBoneName = ROULETTE_ANIM_GET_SEAT_BONE_NAME(iLocalSeatID)
	VECTOR vPos = ROULETTE_PROPS_GET_ENTITY_BONE_POSITION_SAFELY(objTable, strBoneName)
	VECTOR vRot = ROULETTE_PROPS_GET_ENTITY_BONE_ROTATION_SAFELY(objTable, strBoneName)
	STRING strAnimClip = ROULETTE_ANIM_GET_REACTION_CAMERA_CLIP(iLocalSeatID)
	STRING strAnimDict = ROULETTE_ANIM_GET_DICTIONARY(ROULETTE_ANIM_DICT_PLAYER)
	
	CAMERA_INDEX ciRouletteCam = sRouletteData.ciReactionCamera
	PLAY_CAM_ANIM(ciRouletteCam, strAnimClip, strAnimDict, vPos, vRot)
ENDPROC

/// PURPOSE:
///    Safely switches the roulette view to the reaction camera that can be animated
PROC ROULETTE_CAMERA_SWITCH_TO_ANIMATED_REACTION_CAMERA(ROULETTE_DATA &sRouletteData)	
	// Need to disable camera control when switching from a gameplay camera
	NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
	
	ROULETTE_CAMERA_SET_MAIN_CAM_ENABLED(sRouletteData, FALSE, FALSE)
	ROULETTE_CAMERA_SET_REACTION_CAM_ENABLED(sRouletteData, TRUE)
	sRouletteData.eCurrentCameraView = ROULETTE_CAM_VIEW_REACTION
ENDPROC

/// PURPOSE:
///    Checks if the roulette camera is showing the given camera view   
FUNC BOOL ROULETTE_CAMERA_IS_SHOWING_VIEW(ROULETTE_DATA &sRouletteData, ROULETTE_CAM_VIEW eCameraViewToCheck) 
	RETURN sRouletteData.eCurrentCameraView = eCameraViewToCheck
ENDFUNC

/// PURPOSE:
///    Checks if a gameplay view mode is being shown   
FUNC BOOL ROULETTE_CAMERA_IS_SHOWING_A_GAMEPLAY_CAM_VIEW(ROULETTE_DATA &sRouletteData)
	RETURN ROULETTE_CAMERA_IS_SHOWING_VIEW(sRouletteData, ROULETTE_CAM_VIEW_THIRD_PERSON)
	OR ROULETTE_CAMERA_IS_SHOWING_VIEW(sROuletteData, ROULETTE_CAM_VIEW_FIRST_PERSON)
ENDFUNC

/// PURPOSE:
///    Checks if the main roulette camera is active   
FUNC BOOL ROULETTE_CAMERA_IS_MAIN_CAM_ACTIVE(ROULETTE_DATA &sRouletteData)
	RETURN IS_CAM_ACTIVE(sRouletteData.ciRouletteCamera)
ENDFUNC

/// PURPOSE:
///    Checks if the main roulette camera is rendering 
FUNC BOOL ROULETTE_CAMERA_IS_MAIN_CAM_RENDERING(ROULETTE_DATA &sRouletteData)
	RETURN IS_CAM_RENDERING(sRouletteData.ciRouletteCamera)
ENDFUNC

/// PURPOSE:
///    Checks if the main roulette camera needs to be re-enabled because the we were showing a gameplay view mode 
FUNC BOOL ROULETTE_CAMERA_SHOULD_BE_ENABLED_ON_SWITCH(ROULETTE_DATA &sRouletteData)
	RETURN (NOT ROULETTE_CAMERA_IS_MAIN_CAM_RENDERING(sRouletteData) OR NOT ROULETTE_CAMERA_IS_MAIN_CAM_ACTIVE(sRouletteData))
	OR ROULETTE_CAMERA_IS_SHOWING_A_GAMEPLAY_CAM_VIEW(sRouletteData)
ENDFUNC

/// PURPOSE:
///    Switches the camera to the specified view, must tell function if switching from game camera
PROC ROULETTE_CAMERA_SHOW_VIEW(ROULETTE_DATA &sRouletteData, ROULETTE_CAM_VIEW eCameraViewToShow)	
	IF sRouletteData.ciRouletteCamera = NULL
		EXIT
	ENDIF
	
	IF eCameraViewToShow = ROULETTE_CAM_VIEW_THIRD_PERSON
		ROULETTE_CAMERA_SWITCH_TO_THIRD_PERSON(sRouletteData)
		EXIT
	ENDIF
	
	IF eCameraViewToShow = ROULETTE_CAM_VIEW_FIRST_PERSON
		ROULETTE_CAMERA_SWITCH_TO_FIRST_PERSON(sRouletteData)
		EXIT
	ENDIF
	
	IF eCameraViewToShow = ROULETTE_CAM_VIEW_REACTION
		ROULETTE_CAMERA_SWITCH_TO_ANIMATED_REACTION_CAMERA(sRouletteData)
		EXIT
	ENDIF
	
	OBJECT_INDEX objTable = ROULETTE_HELPER_GET_TABLE_OBJECT_FOR_ID(sRouletteData, sRouletteData.iTableID)
	IF NOT IS_ENTITY_ALIVE(objTable)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_CAMERA_SHOW_VIEW - Table does not exist")
		EXIT
	ENDIF
	
	INT iLocalSeatID = ROULETTE_HELPER_GET_LOCAL_PLAYERS_LOCAL_SEAT_ID(sRouletteData)
	VECTOR vPos = ROULETTE_CAM_GET_VIEW_POSITION_OFFSET(eCameraViewToShow, iLocalSeatID)
	VECTOR vLookAt = ROULETTE_CAM_GET_VIEW_LOOK_AT_OFFSET(eCameraViewToShow, iLocalSeatID)
	FLOAT fFOV = ROULETTE_CAM_GET_VIEW_FOV(eCameraViewToShow, iLocalSeatID)
	CAMERA_INDEX ciRouletteCam = sRouletteData.ciRouletteCamera
	
	IF ROULETTE_CAMERA_SHOULD_BE_ENABLED_ON_SWITCH(sRouletteData)
		ATTACH_CAM_TO_ENTITY(ciRouletteCam, objTable, vPos, TRUE)
		POINT_CAM_AT_ENTITY(ciRouletteCam, objTable, vLookAt, TRUE)
		SET_CAM_FOV(ciRouletteCam, fFOV)
		
		// Need to disable camera control when switching from a gameplay camera
		NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
		
		// Need to make sure camera is active and rendering if switching from game cam
		ROULETTE_CAMERA_SET_MAIN_CAM_ENABLED(sRouletteData, TRUE, FALSE)
	ELSE
		ATTACH_CAM_TO_ENTITY(ciRouletteCam, objTable, vPos, TRUE)
		POINT_CAM_AT_ENTITY(ciRouletteCam, objTable, vLookAt, TRUE)
		SET_CAM_FOV(ciRouletteCam, fFOV)
	ENDIF
	
	sRouletteData.vLastLayoutTargetPos = vPos
	sRouletteData.vLastLayoutTargetLookAt = vLookAt
	sRouletteData.vCurrentLookAt = vLookAt
	sRouletteData.eCurrentCameraView = eCameraViewToShow
ENDPROC

/// PURPOSE:
///    Shows the last switchable camera view 
PROC ROULETTE_CAMERA_SHOW_LAST_SWITCHABLE_CAM_VIEW(ROULETTE_DATA &sRouletteData, BOOL bAllowLayoutView)
	IF NOT bAllowLayoutView
		sRouletteData.iSwitchCamIndex = sRouletteData.iLastNonLayoutSwitchCam
	ENDIF

	ROULETTE_CAMERA_SHOW_VIEW(sRouletteData, sRouletteData.eSwitchableCameraView[sRouletteData.iSwitchCamIndex])
ENDPROC

/// PURPOSE:
///    Maintains peaking at the wheel camera when holding the left trigger
FUNC BOOL ROULETTE_CAMERA_MAINTAIN_WHEEL_CAM_PEEKING(ROULETTE_DATA &sRouletteData)	
	
	IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LT)
	
		IF ROULETTE_CAMERA_IS_PEEKING_AT_LAYOUT(sRouletteData)
			RETURN FALSE
		ENDIF
		
		ROULETTE_AUDIO_START_WHEEL_SCENE()
		
		ROULETTE_HELPER_HIDE_PLAYER_AT_SEAT(sRouletteData, 0, sRouletteData.iTableID, TRUE)
		
		IF sRouletteData.eCurrentCameraView != ROULETTE_CAM_VIEW_WHEEL
			ROULETTE_CAMERA_SHOW_VIEW(sRouletteData, ROULETTE_CAM_VIEW_WHEEL)
			ROULETTE_UI_UPDATE_BETTING_UI_BUTTONS(sRouletteData)
		ENDIF
		RETURN TRUE
	ELIF sRouletteData.eCurrentCameraView = ROULETTE_CAM_VIEW_WHEEL
		ROULETTE_AUDIO_STOP_WHEEL_SCENE()
		ROULETTE_CAMERA_SHOW_LAST_SWITCHABLE_CAM_VIEW(sRouletteData, TRUE)
		ROULETTE_UI_UPDATE_BETTING_UI_BUTTONS(sRouletteData)
	ENDIF
	
	ROULETTE_HELPER_HIDE_PLAYER_AT_SEAT(sRouletteData, 0, sRouletteData.iTableID, FALSE)
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Maintains peaking at the reaction camera when holding the left trigger    
FUNC BOOL ROULETTE_CAMERA_MAINTAIN_REACTION_CAM_PEEKING(ROULETTE_DATA &sRouletteData)	
	IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LT)
		IF sRouletteData.eCurrentCameraView != ROULETTE_CAM_VIEW_REACTION
			ROULETTE_CAMERA_SHOW_VIEW(sRouletteData, ROULETTE_CAM_VIEW_REACTION)
			ROULETTE_UI_UPDATE_BETTING_UI_BUTTONS(sRouletteData)
		ENDIF
		RETURN TRUE
	ELIF sRouletteData.eCurrentCameraView = ROULETTE_CAM_VIEW_REACTION
		ROULETTE_CAMERA_SHOW_LAST_SWITCHABLE_CAM_VIEW(sRouletteData, TRUE)
		ROULETTE_UI_UPDATE_BETTING_UI_BUTTONS(sRouletteData)
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Maintains peaking at the layout camera when holding the right trigger   
FUNC BOOL ROULETTE_CAMERA_MAINTAIN_LAYOUT_PEEKING(ROULETTE_DATA &sRouletteData)
	IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RT)
		IF ROULETTE_CAMERA_IS_PEEKING_AT_WHEEL(sRouletteData)
			RETURN FALSE
		ENDIF
	
		IF sRouletteData.eCurrentCameraView != ROULETTE_CAM_VIEW_LAYOUT
			ROULETTE_CAMERA_SHOW_VIEW(sRouletteData, ROULETTE_CAM_VIEW_LAYOUT)
			ROULETTE_UI_UPDATE_BETTING_UI_BUTTONS(sRouletteData)
		ENDIF
		RETURN TRUE
	ELIF sRouletteData.eCurrentCameraView = ROULETTE_CAM_VIEW_LAYOUT
		ROULETTE_CAMERA_SHOW_LAST_SWITCHABLE_CAM_VIEW(sRouletteData, TRUE)
		ROULETTE_UI_UPDATE_BETTING_UI_BUTTONS(sRouletteData)
	ENDIF
	
	RETURN FALSE
ENDFUNC	 

/// PURPOSE:
///    Maintains switching through the cameras available in the betting state   
FUNC BOOL ROULETTE_CAMERA_MAINTAIN_CAMERA_SWITCHING(ROULETTE_DATA &sRouletteData, BOOL bLayoutCamEnabled)
	
	/// We are showing the layout camera when its not enabled
	/// switch back to last gameplay camera
	IF NOT bLayoutCamEnabled 
	AND sRouletteData.iSwitchCamIndex = 0
		ROULETTE_CAMERA_SHOW_LAST_SWITCHABLE_CAM_VIEW(sRouletteData, FALSE)
		RETURN TRUE
	ENDIF
	
	// Leave if the player isn't switching and we are on a valid switch cam
	IF NOT IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_NEXT_CAMERA)
		RETURN FALSE
	ENDIF
	
	INT iMaxCameraIndex = COUNT_OF(sRouletteData.eSwitchableCameraView)
	
	// Increment to next switchable camera
	sRouletteData.iSwitchCamIndex = WRAP_INDEX(sRouletteData.iSwitchCamIndex + 1, iMaxCameraIndex)
	
	// If the next camera is layout and it isn't enabled, skip over it
	IF NOT bLayoutCamEnabled 
	AND sRouletteData.iSwitchCamIndex = 0
		sRouletteData.iSwitchCamIndex = 1
	ENDIF
	
	// Keep track of last gameplay camera index used
	IF sRouletteData.iSwitchCamIndex != 0
		sRouletteData.iLastNonLayoutSwitchCam = sRouletteData.iSwitchCamIndex
	ELSE
		// If we were in the layout cam use the gameplay cam first intialised
		sRouletteData.iLastNonLayoutSwitchCam = 1
	ENDIF
	
	// Show the new camera
	ROULETTE_CAMERA_SHOW_VIEW(sRouletteData, sRouletteData.eSwitchableCameraView[sRouletteData.iSwitchCamIndex])
	
	IF sRouletteData.iSwitchCamIndex = 0		
		SET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT,
		ROULETTE_CAMERA_GET_GAMEPLAY_VIEW_MODE_FROM_ROULETTE_VIEW(sRouletteData.eSwitchableCameraView[1]))
	ENDIF
	
	ROULETTE_UI_UPDATE_BETTING_UI_BUTTONS(sRouletteData)
	RETURN TRUE
ENDFUNC

PROC ROULETTE_CAMERA_INIT_WHEEL_SPIN_GAMEPLAY_CAMERA(ROULETTE_DATA &sRouletteData)
	INT iLocalSeatID = ROULETTE_HELPER_GET_LOCAL_PLAYERS_LOCAL_SEAT_ID(sRouletteData)
	FLOAT fFOV = ROULETTE_CAM_GET_VIEW_FOV(ROULETTE_CAM_VIEW_WHEEL, iLocalSeatID)
	sRouletteData.fCurrentWheelCamFOV = fFOV
	sRouletteData.fStartWheelCamFOV = fFOV
	
	INT iTableID = sRouletteData.iTableID
	
	IF NOT ROULETTE_TABLE_IS_TABLE_ID_VALID(iTableID)
		EXIT
	ENDIF
	
	STRING strAnimDict = ROULETTE_ANIM_GET_DICTIONARY(ROULETTE_ANIM_DICT_PROP)	
	
	
	INT iWinNumber = ROULETTE_HELPER_GET_SERVERS_CURRENT_WINNING_NUMBER_FOR_TABLE(sRouletteData, iTableID)
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_CAMERA_INIT_WHEEL_SPIN_GAMEPLAY_CAMERA - iWinNumber: ", iWinNumber)
	
	INT iAnimIndex = ROULETTE_GET_ANIMATION_INDEX_FOR_ROULETTE_NUMBER(iWinNumber)
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_CAMERA_INIT_WHEEL_SPIN_GAMEPLAY_CAMERA - iAnimIndex: ", iAnimIndex)
	
	STRING strAnimOutroClip = ROULETTE_ANIM_GET_PROP_CLIP(ROULETTE_ANIM_CLIP_PROP_BALL_SPIN_OUTRO, iAnimIndex)
	FLOAT fOutroDuration = GET_ANIM_DURATION(strAnimDict, strAnimOutroClip)
	
	FLOAT fBallLandStartPhase
	FLOAT fBallLandEndPhase
	FIND_ANIM_EVENT_PHASE(strAnimDict, strAnimOutroClip, "end_result", fBallLandStartPhase, fBallLandEndPhase)
	FLOAT fBallLandDuration = fOutroDuration * fBallLandStartPhase
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_CAMERA_INIT_WHEEL_SPIN_GAMEPLAY_CAMERA - fBallLandStartPhase: ", fBallLandStartPhase)
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_CAMERA_INIT_WHEEL_SPIN_GAMEPLAY_CAMERA - fBallLandDuration: ", fBallLandDuration)
		
	INT iLoopEndTimeMS = ROULETTE_ANIM_GET_LOOPING_END_TIME_MS(sRouletteData.sSpin[sRouletteData.iTableID])
	FLOAT iTimeRemainingMS = sRouletteData.sSpin[iTableID].sData.fSpinTimeMS
	
	// determine random end loops duration
	sRouletteData.fTargetFOVEndTime = (iLoopEndTimeMS - iTimeRemainingMS) / 1000
	
	// Add on time until the ball lands
	sRouletteData.fTargetFOVEndTime += fBallLandDuration
	
	sRouletteData.fFOVLerpTime = 0
ENDPROC

PROC ROULETTE_CAMERA_MAINTAIN_WHEEL_CAM_EASE_IN(ROULETTE_DATA &sRouletteData)
	sRouletteData.fFOVLerpTime += GET_FRAME_TIME()
	
	FLOAT fTime = MAP_VALUE(sRouletteData.fFOVLerpTime, 0, sRouletteData.fTargetFOVEndTime, 0.0, 1.0)
	
	IF fTime > 1.0
		EXIT
	ENDIF
	
	sRouletteData.fCurrentWheelCamFOV = LERP_FLOAT(sRouletteData.fStartWheelCamFOV, ROULETTE_CAM_WHEEL_CAM_TARGET_FOV, fTime)
	
	IF ROULETTE_CAMERA_IS_SHOWING_VIEW(sRouletteData, ROULETTE_CAM_VIEW_WHEEL)
		SET_CAM_FOV(sRouletteData.ciRouletteCamera, sRouletteData.fCurrentWheelCamFOV)
	ENDIF
ENDPROC

/// PURPOSE:
///    Maintains switching through cameras avaialble in the wheel spin state
PROC ROULETTE_CAMERA_MAINTAIN_WHEEL_SPIN_GAMEPLAY_CAMERA(ROULETTE_DATA &sRouletteData)		
	// This switching is handled manually to avoid far third person view
	DISABLE_CAMERA_VIEW_MODE_CYCLE(PLAYER_ID())		
	ROULETTE_CAMERA_MAINTAIN_WHEEL_CAM_EASE_IN(sRouletteData)
	
	// If peeking at the wheel cam leave
	IF ROULETTE_CAMERA_MAINTAIN_WHEEL_CAM_PEEKING(sRouletteData)
		EXIT
	ENDIF
	
	IF ROULETTE_CAMERA_MAINTAIN_LAYOUT_PEEKING(sRouletteData)
		EXIT
	ENDIF
	
	IF ROULETTE_CAMERA_IS_SHOWING_VIEW(sRouletteData, ROULETTE_CAM_VIEW_LAYOUT)
		ROULETTE_CAMERA_SHOW_VIEW(sRouletteData, ROULETTE_CAM_VIEW_THIRD_PERSON)
	ENDIF
	
	ROULETTE_CAMERA_MAINTAIN_CAMERA_SWITCHING(sRouletteData, FALSE)
ENDPROC

/// PURPOSE:
///    Maintains the camera behaviour for the round resolution state
PROC ROULETTE_CAMERA_MAINTAIN_ROUND_RESOLUTION_GAMEPLAY_CAMERA(ROULETTE_DATA &sRouletteData)	
	DISABLE_CAMERA_VIEW_MODE_CYCLE(PLAYER_ID())
	
	// If peeking at the reaction cam leave
	IF ROULETTE_CAMERA_MAINTAIN_REACTION_CAM_PEEKING(sRouletteData)
		EXIT
	ENDIF
	
	IF ROULETTE_CAMERA_MAINTAIN_LAYOUT_PEEKING(sRouletteData)
		EXIT
	ENDIF
	
	IF ROULETTE_CAMERA_IS_SHOWING_VIEW(sRouletteData, ROULETTE_CAM_VIEW_LAYOUT)
		ROULETTE_CAMERA_SHOW_VIEW(sRouletteData, ROULETTE_CAM_VIEW_THIRD_PERSON)
	ENDIF
	
	ROULETTE_CAMERA_MAINTAIN_CAMERA_SWITCHING(sRouletteData, FALSE)
ENDPROC

/// PURPOSE:
///    Smoothly makes a vector approach another moving vector   
FUNC VECTOR SMOOTH_APPROACH(VECTOR vPastPosition, VECTOR vPastTargetPosition, VECTOR vTargetPosition, FLOAT fSpeed)
	FLOAT t = GET_FRAME_TIME() * fSpeed
	VECTOR v = (vTargetPosition - vPastTargetPosition) / t
	VECTOR f = vPastPosition - vPastTargetPosition + v
	RETURN vTargetPosition - v + f * POW(2.718281828, -t)
ENDFUNC

/// PURPOSE:
///    Gets the layout movement target location according to bounds  
FUNC VECTOR ROULETTE_CAMERA_GET_LAYOUT_MOVE_TARGET_FROM_INPUT(VECTOR vMovementInput)
	VECTOR vLookMovement = <<0, 0, 0>>

	IF vMovementInput.x < 0
		vLookMovement.x = COSINE_INTERP_FLOAT(0, -ROULETTE_CAM_MAX_LAYOUT_MOVEMENT_LEFT, -vMovementInput.x)
	ELSE
		vLookMovement.x = COSINE_INTERP_FLOAT(0, ROULETTE_CAM_MAX_LAYOUT_MOVEMENT_RIGHT, vMovementInput.x)
	ENDIF
	
	IF vMovementInput.y < 0
		vLookMovement.y = COSINE_INTERP_FLOAT(0, ROULETTE_CAM_MAX_LAYOUT_MOVEMENT_UP, -vMovementInput.y)
	ELSE
		vLookMovement.y = COSINE_INTERP_FLOAT(0, -ROULETTE_CAM_MAX_LAYOUT_MOVEMENT_DOWN, vMovementInput.y)
	ENDIF
	
	RETURN vLookMovement
ENDFUNC

/// PURPOSE:
///    Resets the smooth layout movement so its back at the start position
PROC ROULETTE_CAMERA_RESET_SMOOTH_MOVE_LAYOUT_CAMERA(ROULETTE_DATA &sRouletteData)
	VECTOR vPosOffset = ROULETTE_CAM_GET_VIEW_POSITION_OFFSET(ROULETTE_CAM_VIEW_LAYOUT, 0)
	VECTOR vLookAtOffset = ROULETTE_CAM_GET_VIEW_LOOK_AT_OFFSET(ROULETTE_CAM_VIEW_LAYOUT, 0)	
	sRouletteData.vLastLayoutTargetPos = vPosOffset
	sRouletteData.vLastLayoutTargetLookAt = vLookAtOffset
ENDPROC

/// PURPOSE:
///    Smoothly move the layout camera given a movemnt input 
PROC ROULETTE_CAMERA_SMOOTH_MOVE_LAYOUT_CAMERA(ROULETTE_DATA &sRouletteData, VECTOR vMovementInput)
	OBJECT_INDEX objTable = ROULETTE_HELPER_GET_TABLE_OBJECT_FOR_ID(sRouletteData, sRouletteData.iTableID)
	IF NOT IS_ENTITY_ALIVE(objTable)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_CAMERA_SHOW_VIEW - Table does not exist")
		EXIT
	ENDIF
	
	VECTOR vLookMovement = ROULETTE_CAMERA_GET_LAYOUT_MOVE_TARGET_FROM_INPUT(vMovementInput)
	VECTOR vPosOffset = ROULETTE_CAM_GET_VIEW_POSITION_OFFSET(ROULETTE_CAM_VIEW_LAYOUT, 0)
	VECTOR vLookAtOffset = ROULETTE_CAM_GET_VIEW_LOOK_AT_OFFSET(ROULETTE_CAM_VIEW_LAYOUT, 0)
	VECTOR vCurrentPos = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(objTable, GET_CAM_COORD(sRouletteData.ciRouletteCamera))
	
	VECTOR vNewTargetPos = vPosOffset + (vLookMovement * ROULETTE_CAM_LAYOUT_MOVEMENT_PAN_MOVE_PERCENTAGE)
	VECTOR vNewTargetLookAt = vLookAtOffset + vLookMovement
	
	VECTOR vNewPos 
	vNewPos = SMOOTH_APPROACH(vCurrentPos, sRouletteData.vLastLayoutTargetPos,
	vNewTargetPos, ROULETTE_CAM_LAYOUT_MOVEMENT_SPEED)
	
	VECTOR vNewLookAt
	vNewLookAt = SMOOTH_APPROACH(sRouletteData.vCurrentLookAt, 
	sRouletteData.vLastLayoutTargetLookAt, vNewTargetLookAt,
	ROULETTE_CAM_LAYOUT_MOVEMENT_SPEED)
	
	sRouletteData.vLastLayoutTargetPos = vNewTargetPos
	sRouletteData.vLastLayoutTargetLookAt = vNewTargetLookAt
	
	ATTACH_CAM_TO_ENTITY(sRouletteData.ciRouletteCamera, objTable, vNewPos)
	POINT_CAM_AT_ENTITY(sRouletteData.ciRouletteCamera, objTable, vNewLookAt)	
	sRouletteData.vCurrentLookAt = vNewLookAt
ENDPROC

/// PURPOSE:
///    Maintains layout movement for PC controls
PROC ROULETTE_CAMERA_MAINTAIN_LAYOUT_CAMERA_MOVEMENT_WITH_MOUSE(ROULETTE_DATA &sRouletteData)
	INT iTableID = sRouletteData.iTableID
	VECTOR vTempCursorPos = sRouletteData.vCursorPosition
	VECTOR vTableOrigin = ROULETTE_LAYOUT_GET_ORIGIN_FOR_TABLE(iTableID)
	
	// Convert cursor to the origin space
	vTempCursorPos -= vTableOrigin
	vTempCursorPos = ROULETTE_TABLE_GET_VECTOR_ROTATED_TO_TABLE_SPACE(vTempCursorPos, iTableID)		
	vTempCursorPos.x = MAP_VALUE(vTempCursorPos.x, ROULETTE_UI_CURSOR_BOUND_BL_X, ROULETTE_UI_CURSOR_BOUND_TR_X, -1.0, 1.0)
	vTempCursorPos.y = MAP_VALUE(vTempCursorPos.y, ROULETTE_UI_CURSOR_BOUND_BL_Y, ROULETTE_UI_CURSOR_BOUND_TR_Y, 1.0, -1.0)
	
	// Only move if halfway to the edge from the centr
	IF VMAG(vTempCursorPos) < 0.5
		vTempCursorPos = <<0.0, 0.0, 0.0>>
	ELSE
		vTempCursorPos *= 0.5
	ENDIF
	
	ROULETTE_CAMERA_SMOOTH_MOVE_LAYOUT_CAMERA(sRouletteData, vTempCursorPos)
ENDPROC

/// PURPOSE:
///    Maintians the layout camera movement for controller
PROC ROULETTE_CAMERA_MAINTAIN_LAYOUT_CAMERA_MOVEMENT_WITH_CONTROLLER(ROULETTE_DATA &sRouletteData)	
	VECTOR vMovementInput
	vMovementInput.x = GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_X)
	vMovementInput.y = GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_Y)
	ROULETTE_CAMERA_SMOOTH_MOVE_LAYOUT_CAMERA(sRouletteData, vMovementInput)
ENDPROC

/// PURPOSE:
///    Maintains the layout camera movement for both controller and PC
PROC ROULETTE_CAMERA_MAINTAIN_LAYOUT_CAMERA_MOVEMENT(ROULETTE_DATA &sRouletteData)
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		ROULETTE_CAMERA_MAINTAIN_LAYOUT_CAMERA_MOVEMENT_WITH_MOUSE(sRouletteData)
		EXIT
	ENDIF
	
	ROULETTE_CAMERA_MAINTAIN_LAYOUT_CAMERA_MOVEMENT_WITH_CONTROLLER(sRouletteData)	
ENDPROC

/// PURPOSE:
///    Maintains the camera behaviour for the betting state
PROC ROULETTE_CAMERA_MAINTAIN_BETTING_GAMEPLAY_CAMERA(ROULETTE_DATA &sRouletteData)	
	// This switching is handled manually to avoid far third person view
	DISABLE_CAMERA_VIEW_MODE_CYCLE(PLAYER_ID())
	
	IF sRouletteData.eCurrentCameraView = ROULETTE_CAM_VIEW_LAYOUT
		ROULETTE_CAMERA_MAINTAIN_LAYOUT_CAMERA_MOVEMENT(sRouletteData)
	ELSE
		ROULETTE_CAMERA_RESET_SMOOTH_MOVE_LAYOUT_CAMERA(sRouletteData)
	ENDIF
	
	// If peeking at the wheel cam leave
	IF ROULETTE_CAMERA_MAINTAIN_WHEEL_CAM_PEEKING(sRouletteData)
		EXIT
	ENDIF
	
	// Can only peek at layout if not in the view
	IF sRouletteData.iSwitchCamIndex != 0
		IF ROULETTE_CAMERA_MAINTAIN_LAYOUT_PEEKING(sRouletteData)
			EXIT
		ENDIF
	ENDIF
	
	ROULETTE_CAMERA_MAINTAIN_CAMERA_SWITCHING(sRouletteData, TRUE)	
ENDPROC


/// PURPOSE:
///    Stores the players gameplay camera view
PROC ROULETTE_CAMERA_STORE_INITIAL_CAMERA_VIEW(ROULETTE_DATA &sRouletteData)
	// View mode should be restored on game client cleanup
	SET_BIT(sRouletteData.iRouletteBS, ROULETTE_BS_SHOULD_RESTORE_VIEW_MODE)
	
	// Store
	sRouletteData.eInitialView = GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT)
ENDPROC


PROC ROULETTE_CAMERA_INIT_SWITCHABLE_VIEWS(ROULETTE_DATA &sRouletteData, BOOL bStartInLayout)
	ROULETTE_CAMERA_STORE_INITIAL_CAMERA_VIEW(sRouletteData)
	
	// Layout always takes first slot
	sRouletteData.eSwitchableCameraView[0] = ROULETTE_CAM_VIEW_LAYOUT	
	
	BOOL bThirdPersonFirst 
	bThirdPersonFirst = sRouletteData.eInitialView = CAM_VIEW_MODE_THIRD_PERSON 
	OR sRouletteData.eInitialView = CAM_VIEW_MODE_THIRD_PERSON_MEDIUM 
	OR sRouletteData.eInitialView = CAM_VIEW_MODE_THIRD_PERSON_NEAR	
	
	IF bThirdPersonFirst
		sRouletteData.eSwitchableCameraView[1] = ROULETTE_CAM_VIEW_THIRD_PERSON
		sRouletteData.eSwitchableCameraView[2] = ROULETTE_CAM_VIEW_FIRST_PERSON
	ELSE
		sRouletteData.eSwitchableCameraView[1] = ROULETTE_CAM_VIEW_FIRST_PERSON
		sRouletteData.eSwitchableCameraView[2] = ROULETTE_CAM_VIEW_THIRD_PERSON
	ENDIF
	
	sRouletteData.iLastNonLayoutSwitchCam = 1
	
	// We need to preemtively set the next gamplay cam view before we can switch to it
	SET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT, 
	ROULETTE_CAMERA_GET_GAMEPLAY_VIEW_MODE_FROM_ROULETTE_VIEW(sRouletteData.eSwitchableCameraView[1]))
	
	IF NOT bStartInLayout	
		// Starting in first gameplay view
		sRouletteData.iSwitchCamIndex = 1
		EXIT
	ENDIF
	
	// Set to layout view which is always in the first slot
	sRouletteData.iSwitchCamIndex = 0
ENDPROC

/// PURPOSE:
///    Restores the plyers camera view to whatever their last gameplay view was
PROC ROULETTE_CAMERA_RESTORE_GAMEPLAY_CAMERA(ROULETTE_DATA &sRouletteData)
	IF NOT IS_BIT_SET(sRouletteData.iRouletteBS, ROULETTE_BS_SHOULD_RESTORE_VIEW_MODE)
		EXIT
	ENDIF
	
	CLEAR_BIT(sRouletteData.iRouletteBS, ROULETTE_BS_SHOULD_RESTORE_VIEW_MODE)
	SET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT, 
	ROULETTE_CAMERA_GET_GAMEPLAY_VIEW_MODE_FROM_ROULETTE_VIEW(sRouletteData.eSwitchableCameraView[sRouletteData.iLastNonLayoutSwitchCam]))
ENDPROC

/// PURPOSE:
///    Creates the camera used for showing the board layout
PROC ROULETTE_CAMERA_INIT(ROULETTE_DATA &sRouletteData)
	VECTOR vPos = ROULETTE_TABLE_GET_POSITION(0)
	sRouletteData.ciRouletteCamera = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, vPos, <<0, 0, 0>>)
	sRouletteData.ciReactionCamera = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_ANIMATED, vPos, <<0, 0, 0>>)
ENDPROC

/// PURPOSE:
///    Cleans up all the roulette cameras
PROC ROULETTE_CAMERA_CLEAN_UP(ROULETTE_DATA &sRouletteData)
	IF sRouletteData.ciRouletteCamera != NULL
		DESTROY_CAM(sRouletteData.ciRouletteCamera)
	ENDIF
	
	IF sRouletteData.ciReactionCamera != NULL
		DESTROY_CAM(sRouletteData.ciReactionCamera)
	ENDIF
	
	RENDER_SCRIPT_CAMS(FALSE, FALSE)
ENDPROC
