//////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Name:        RouletteCameraViews.sch																			
/// Description: Header containing the enum that defines roulette camera views																	
///																											
/// Written by:  Online Technical Team: Tom Turner,															
/// Date:  		15/03/2019																					
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

/// PURPOSE: 
///    Camera views for a game of roulette
ENUM ROULETTE_CAM_VIEW
	ROULETTE_CAM_VIEW_LAYOUT,
	ROULETTE_CAM_VIEW_WHEEL,
	ROULETTE_CAM_VIEW_THIRD_PERSON,
	ROULETTE_CAM_VIEW_FIRST_PERSON,
	ROULETTE_CAM_VIEW_REACTION
ENDENUM


FUNC STRING ROULETTE_CAM_VIEW_GET_CAM_VIEW_AS_STRING(ROULETTE_CAM_VIEW eCamView)
	SWITCH eCamView
		CASE ROULETTE_CAM_VIEW_LAYOUT			RETURN "LAYOUT"
		CASE ROULETTE_CAM_VIEW_WHEEL			RETURN "WHEEL"
		CASE ROULETTE_CAM_VIEW_THIRD_PERSON		RETURN "THIRD_PERSON"
		CASE ROULETTE_CAM_VIEW_FIRST_PERSON		RETURN "FIRST_PERSON"
		CASE ROULETTE_CAM_VIEW_REACTION			RETURN "REACTION"
	ENDSWITCH
	
	ASSERTLN(DEBUG_ROULETTE, "Cam view enum(int): ", ENUM_TO_INT(eCamView), " does not have a conversion to string, is it missing from the switch?")
	
	RETURN "ERROR_NO_STRING_CONVERSION"
ENDFUNC
