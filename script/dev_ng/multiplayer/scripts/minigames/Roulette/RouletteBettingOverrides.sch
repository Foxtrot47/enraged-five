//////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Name:        RouletteBettingOverrides.sch																			
/// Description: Header containing all overrideable functions and parameters relating to roulette betting
///																											
/// Written by:  Online Technical Team: Tom Turner,															
/// Date:  		15/03/2019																					
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "freemode_header.sch"
USING "mp_globals_cash_transactions_tu.sch"

ENUM ROULETTE_BETTING_PLACE_RESULT
	RBPR_INVALID,
	RBPR_SUCCESS,
	RPBP_FAILED_HIT_MAX_INDIVIDUAL_BETS,
	RBPR_FAILED_HIT_INSIDE_LIMIT,
	RBPR_FAILED_HIT_OUTSIDE_LIMIT,
	RBPR_FAILED_NOT_ENOUGH_CHIPS,
	RBPR_FAILED_NO_BET_SLOTS_AVAILABLE
ENDENUM

/// PURPOSE: 
///    Defines a bet on the roulette layout
STRUCT ROULETTE_BET
	INT iLayoutIndex = -1
	INT iBetAmount = 0
ENDSTRUCT

#IF NOT DEFINED(ROULETTE_BETTING_MAX_ALLOWED_BETS)
	/// PURPOSE: How many bets a player is allowed to place each round
	CONST_INT ROULETTE_BETTING_MAX_ALLOWED_BETS 10
#ENDIF

#IF NOT DEFINED(ROULETTE_BETTING_MAX_BET_SKIPS_BEFORE_KICK)
	/// PURPOSE: How many times the player can skip placing any bets
	///     before being removed from the table, -1 means no kicking on idle
	CONST_INT ROULETTE_BETTING_MAX_BET_SKIPS_BEFORE_KICK -1
#ENDIF

#IF NOT DEFINED(ROULETTE_BETTING_SPIN_START_TIME_BEFORE_BETTING_ENDS_MS)
	/// PURPOSE: 
	///     How long before the round finishes the wheel will start
	///     e.g. round is 40 seconds, start time 30 seconds, spin will start 10 seconds into round
	CONST_INT ROULETTE_BETTING_SPIN_START_TIME_BEFORE_BETTING_ENDS_MS 15000
#ENDIF

#IF NOT DEFINED(ROULETTE_BETTING_ROUND_TIMER_RED_START_TIME_MS)
	/// PURPOSE: 
	///     How long before the end of the betting time the timer will display as red
	CONST_INT ROULETTE_BETTING_ROUND_TIMER_RED_START_TIME_MS 5000
#ENDIF

#IF NOT DEFINED(ROULETTE_GET_BETTING_ROUND_DURATION_MS)
	/// PURPOSE:
	///    Default function to get the betting round duration based on how many players
	///    are at the table
	FUNC INT ROULETTE_GET_BETTING_ROUND_DURATION_MS(INT iNumPlayersAtTable)
		SWITCH iNumPlayersAtTable
			CASE 1	
			CASE 2
				RETURN 40000
			CASE 3 RETURN 50000
			CASE 4 RETURN 60000
		ENDSWITCH
		
		RETURN 60000
	ENDFUNC
#ENDIF

#IF NOT DEFINED(ROULETTE_BETTING_PLACE_BET_TRANSACTION)
	/// PURPOSE:
	///    Default function that is called when handling transaction for the bet made
	///    in a round of roulette, should be overridden with appropriate server transaction
	FUNC GENERIC_TRANSACTION_STATE ROULETTE_BETTING_PLACE_BET_TRANSACTION(INT iTotalBetForRound)
		UNUSED_PARAMETER(iTotalBetForRound)
		RETURN TRANSACTION_STATE_SUCCESS
	ENDFUNC
#ENDIF

#IF NOT DEFINED(ROULETTE_BETTING_PAYOUT_TRANSACTION)
	/// PURPOSE:
	///    Default function that is called when handling transaction for paying out 
	///    after a round of roulette, should be overridden with appropriate server transaction
	FUNC GENERIC_TRANSACTION_STATE ROULETTE_BETTING_PAYOUT_TRANSACTION(INT iTotalPayoutForRound)
		UNUSED_PARAMETER(iTotalPayoutForRound)
		RETURN TRANSACTION_STATE_SUCCESS
	ENDFUNC
#ENDIF

#IF NOT DEFINED(ROULETTE_BETTING_CAN_PLACE_BET)
	FUNC BOOL ROULETTE_BETTING_CAN_PLACE_BET(INT iBetValue, INT iTotalBetForRound)
		UNUSED_PARAMETER(iBetValue)
		UNUSED_PARAMETER(iTotalBetForRound)
		RETURN TRUE
	ENDFUNC
#ENDIF

#IF NOT DEFINED(ROULETTE_BETTING_GET_LOCAL_PLAYER_CHIP_COUNT)
	FUNC INT ROULETTE_BETTING_GET_LOCAL_PLAYER_CHIP_COUNT()
		RETURN 0
	ENDFUNC
#ENDIF
