//////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Name:        RouletteProps.sch																			
/// Description: Header for the placement of props/spawned entities used in a game of roulette				
///				e.g. placing chip props on the table														
///																											
///																											
/// Written by:  Online Technical Team: Tom Turner,															
/// Date:  		21/02/2019																					
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "Globals.sch"
USING "RouletteData.sch"
USING "RouletteLayout.sch"
USING "RouletteBetting.sch"
USING "RouletteTable.sch"
USING "RouletteHelper.sch"
USING "RoulettePropsOverrides.sch"
USING "RouletteUIOverrides.sch"
USING "RouletteAudio.sch"


/// PURPOSE:
///    Deletes all roulette chips
PROC ROULETTE_PROPS_HIDE_ALL_HIGHLIGHTS(ROULETTE_DATA &sRouletteData)
	INT i = 0
	REPEAT ROULETTE_MAX_HIGHLIGHTED_NUMBERS i
		IF DOES_ENTITY_EXIST(sRouletteData.objHighlights[i])
			SET_ENTITY_VISIBLE(sRouletteData.objHighlights[i], FALSE)
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Deletes all roulette highlights
PROC ROULETTE_PROPS_DELETE_ALL_HIGHLIGHTS(ROULETTE_DATA &sRouletteData)
	INT i = 0	
	REPEAT ROULETTE_MAX_HIGHLIGHTED_NUMBERS i
		IF DOES_ENTITY_EXIST(sRouletteData.objHighlights[i])
			DELETE_OBJECT(sRouletteData.objHighlights[i])
			CPRINTLN(DEBUG_ROULETTE, "ROULETTE_PROPS_DELETE_ALL_HIGHLIGHTS - deleting highlight: ", i)
			sRouletteData.objHighlights[i] = NULL
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Gets the correct highlight model for the layout index   
FUNC MODEL_NAMES ROULETTE_PROPS_GET_HIGHLIGHT_MODEL_FOR_LAYOUT_INDEX(INT iLayoutIndex)
	RETURN ROULETTE_PROPS_GET_HIGHLIGHT_MODEL(ROULETTE_LAYOUT_IS_INDEX_ON_A_ZEROES_BET_ROW(iLayoutIndex))
ENDFUNC

/// PURPOSE:
///    Creates a highlight at the position and stores it in the array index, if there is a highlight already at that index
///    it will attempt to re-use it (Lazy object pool)
PROC ROULETTE_PROPS_TRY_REUSE_HIGHLIGHT_MODEL_AT_INDEX(ROULETTE_DATA &sRouletteData, INT iHighlightIndex, MODEL_NAMES eRequiredModel, VECTOR vPos)

	// If a highlight at this index hasn't been created yet, create it then leave
	IF sRouletteData.objHighlights[iHighlightIndex] = NULL OR NOT DOES_ENTITY_EXIST(sRouletteData.objHighlights[iHighlightIndex])
		sRouletteData.objHighlights[iHighlightIndex] = 
		CREATE_OBJECT(eRequiredModel, vPos, FALSE)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_PROPS_TRY_REUSE_HIGHLIGHT_MODEL_AT_INDEX - Highlight at index ",
		iHighlightIndex," hasn't been created yet - Creating highlight")
		EXIT
	ENDIF
	
	// If the highlight exists but is the wrong model re-create it with the correct model
	IF GET_ENTITY_MODEL(sRouletteData.objHighlights[iHighlightIndex]) != eRequiredModel			
		IF sRouletteData.objHighlights[iHighlightIndex] != NULL 
		AND DOES_ENTITY_EXIST(sRouletteData.objHighlights[iHighlightIndex])
		AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT(sRouletteData.objHighlights[iHighlightIndex])
			CPRINTLN(DEBUG_ROULETTE, "ROULETTE_PROPS_TRY_REUSE_HIGHLIGHT_MODEL_AT_INDEX - Highlight at index ",
			iHighlightIndex," has been created but has incorrect model - Deleting old highlight")
			DELETE_OBJECT(sRouletteData.objHighlights[iHighlightIndex])
		ENDIF

		sRouletteData.objHighlights[iHighlightIndex] = 
		CREATE_OBJECT(eRequiredModel, vPos, FALSE)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_PROPS_TRY_REUSE_HIGHLIGHT_MODEL_AT_INDEX - Highlight at index ",
		iHighlightIndex," has been created but has incorrect model - Creating highlight")
		EXIT
	ENDIF
	
	// If the model is correct just make the existing highlight visible 
	SET_ENTITY_VISIBLE(sRouletteData.objHighlights[iHighlightIndex], TRUE)
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_PROPS_TRY_REUSE_HIGHLIGHT_MODEL_AT_INDEX - Highlight at index ",
	iHighlightIndex, " exists with correct model - Setting to visible")
ENDPROC

/// PURPOSE:
///    Creates a highlight object on the layout for the given roulette number index
PROC ROULETTE_PROPS_CREATE_HIGHLIGHT_FOR_NUMBER_INDEX(ROULETTE_DATA &sRouletteData, INT iNumberIndex, INT iArrayInsertIndex)
	INT iTableID = sRouletteData.iTableID
	INT iLayoutIndex = ROULETTE_GET_LAYOUT_INDEX_FROM_SLOT_INDEX(iNumberIndex)
	MODEL_NAMES eRequiredModel = ROULETTE_PROPS_GET_HIGHLIGHT_MODEL_FOR_LAYOUT_INDEX(iLayoutIndex)
	
	VECTOR vHighlightPosition = ROULETTE_LAYOUT_GET_BET_POSITION_ON_TABLE_FOR_LAYOUT_INDEX(iLayoutIndex, iTableID)
	vHighlightPosition += <<0, 0, ROULETTE_PROPS_LAYOUT_HIGHLIGHT_OFFSET>>
	
	IF eRequiredModel = ROULETTE_PROPS_GET_HIGHLIGHT_MODEL(TRUE)
		FLOAT fZeroHighlightOffset = ROULETTE_PROPS_LAYOUT_ZEROES_HIGHLIGHT_X_OFFSET
		vHighlightPosition += ROULETTE_TABLE_GET_VECTOR_RELATIVE_TO_TABLE(<<fZeroHighlightOffset, 0, 0>>, iTableID)
	ENDIF

	ROULETTE_PROPS_TRY_REUSE_HIGHLIGHT_MODEL_AT_INDEX(sRouletteData, iArrayInsertIndex,
	eRequiredModel, vHighlightPosition)
	
	IF sRouletteData.objHighlights[iArrayInsertIndex] = NULL 
	OR NOT DOES_ENTITY_EXIST(sRouletteData.objHighlights[iArrayInsertIndex])
		EXIT
	ENDIF
	
	ROTATE_OBJECT(sRouletteData.objHighlights[iArrayInsertIndex], 90, 90, FALSE)	
	SET_ENTITY_COORDS_NO_OFFSET(sRouletteData.objHighlights[iArrayInsertIndex], vHighlightPosition)
	SET_ENTITY_HEADING(sRouletteData.objHighlights[iArrayInsertIndex], ROULETTE_TABLE_GET_HEADING(iTableID))
	SET_ENTITY_DYNAMIC(sRouletteData.objHighlights[iArrayInsertIndex], FALSE)
	SET_ENTITY_HAS_GRAVITY(sRouletteData.objHighlights[iArrayInsertIndex], FALSE)
	SET_ENTITY_COMPLETELY_DISABLE_COLLISION(sRouletteData.objHighlights[iArrayInsertIndex], FALSE)
	SET_ENTITY_ALPHA(sRouletteData.objHighlights[iArrayInsertIndex], 200, TRUE)

	OBJECT_INDEX objTable = ROULETTE_HELPER_GET_TABLE_OBJECT_FOR_ID(sRouletteData, iTableID)
	
	IF NOT DOES_ENTITY_EXIST(objTable)
		EXIT
	ENDIF
	
	// Set to same pallete index as its table
	INT iPalleteIndex = GET_OBJECT_TINT_INDEX(objTable)
	SET_OBJECT_TINT_INDEX(sRouletteData.objHighlights[iArrayInsertIndex], iPalleteIndex)

	IF iLayoutIndex = LAYOUT_MAX_ZEROES_X_COORD
		SET_ENTITY_ROTATION(sRouletteData.objHighlights[iArrayInsertIndex], GET_ENTITY_ROTATION(objTable) + <<180, 0, 0>>)
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Updates the layout highlights on the table used by the local player
///    according to the current cursor position
PROC ROULETTE_PROPS_UPDATE_TABLE_HIGHLIGHTS(ROULETTE_DATA &sRouletteData)	
	
	ROULETTE_PROPS_HIDE_ALL_HIGHLIGHTS(sRouletteData)	
	IF sRouletteData.iCursorXCoord < 0 OR sRouletteData.iCursorYCoord < 0
		EXIT
	ENDIF
	
	INT iAssociatedNumberIndexs[ROULETTE_MAX_ROULETTE_NUMBER]	
	INT iCurrentCursorLayoutIndex = ROULETTE_LAYOUT_COORD_TO_LAYOUT_INDEX(sRouletteData.iCursorXCoord, sRouletteData.iCursorYCoord)
	ROULETTE_BETTING_GET_ALL_NUMBER_INDEXS_ASSOCIATED_WITH_LAYOUT_POSITION(iAssociatedNumberIndexs,
	iCurrentCursorLayoutIndex)
	
	INT iHighlightInsertIndex = 0
	
	// Straight bets just need highlight on their position
	IF ROULETTE_BETTING_GET_BET_TYPE_FROM_LAYOUT_INDEX(iCurrentCursorLayoutIndex) = ROULETTE_BET_TYPE_STRAIGHT	
		ROULETTE_PROPS_CREATE_HIGHLIGHT_FOR_NUMBER_INDEX(sRouletteData,
		ROULETTE_GET_SLOT_INDEX_FROM_LAYOUT_INDEX(iCurrentCursorLayoutIndex), iHighlightInsertIndex)	
		EXIT
	ENDIF
	
	// Create highlights for associated indexs
	INT i = 0
	REPEAT ROULETTE_MAX_ROULETTE_NUMBER i
		INT iCurrentNumberIndex = iAssociatedNumberIndexs[i]
		
		// Valid indexs are always stored at the front of the array
		// if we hit an invalid one the rest must be invalid
		IF iCurrentNumberIndex = -1
			BREAKLOOP
		ENDIF
		
		ROULETTE_PROPS_CREATE_HIGHLIGHT_FOR_NUMBER_INDEX(sRouletteData, iCurrentNumberIndex, iHighlightInsertIndex)			
		iHighlightInsertIndex++
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Delete all chip objects relating to this seat
PROC ROULETTE_PROPS_DELETE_SEAT_CHIPS(ROULETTE_DATA &sRouletteData, INT iSeatID)
	INT iBet = 0
	
	IF iSeatID = -1
		EXIT
	ENDIF
	
	REPEAT ROULETTE_BETTING_MAX_ALLOWED_BETS iBet
		IF NOT DOES_ENTITY_EXIST(sRouletteData.objChips[iSeatID][iBet])
			RELOOP
		ENDIF
		
		DELETE_OBJECT(sRouletteData.objChips[iSeatID][iBet])
		sRouletteData.objChips[iSeatID][iBet] = NULL
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_PROPS_DELETE_SEAT_CHIPS - deleting chip for bet: ", iBet, " at seat: ", iSeatID)
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Delete the chip objects being used by the local player
PROC ROULETTE_PROPS_DELETE_LOCAL_PLAYERS_CHIPS(ROULETTE_DATA &sRouletteData)
	INT iSeatID = sRouletteData.playerBD[NATIVE_TO_INT(PLAYER_ID())].iSeatID
	IF iSeatID > -1
		ROULETTE_PROPS_DELETE_SEAT_CHIPS(sRouletteData, iSeatID)
	ENDIF
ENDPROC

/// PURPOSE:
///    Deletes all roulette chips
PROC ROULETTE_PROPS_DELETE_ALL_CHIPS(ROULETTE_DATA &sRouletteData)
	INT iSeatID = 0
	REPEAT ROULETTE_MAX_SEAT_ID iSeatID
		ROULETTE_PROPS_DELETE_SEAT_CHIPS(sRouletteData, iSeatID)
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Deletes all chips on the given table
PROC ROULETTE_PROPS_DELETE_ALL_CHIPS_FOR_TABLE(ROULETTE_DATA &sRouletteData, INT iTableID)
	INT iLocalSeatID = 0
	
	REPEAT ROULETTE_TABLE_MAX_NUMBER_OF_PLAYERS_PER_TABLE iLocalSeatID
		INT iSeatID = ROULETTE_HELPER_GET_SEAT_ID_FROM_LOCAL_SEAT_ID_AT_TABLE(iLocalSeatID, iTableID)
		ROULETTE_PROPS_DELETE_SEAT_CHIPS(sRouletteData, iSeatID)
	ENDREPEAT
	
	CLEAR_BIT(sRouletteData.iTableBS[iTableID], ROULETTE_CLIENT_TABLE_BS_HAS_BET_IN_ZONE_ONE)
	CLEAR_BIT(sRouletteData.iTableBS[iTableID], ROULETTE_CLIENT_TABLE_BS_HAS_BET_IN_ZONE_TWO)
	CLEAR_BIT(sRouletteData.iTableBS[iTableID], ROULETTE_CLIENT_TABLE_BS_HAS_BET_IN_ZONE_THREE)
ENDPROC

/// PURPOSE:
///    Deletes all chips on the table the local player is playing on
PROC ROULETTE_PROPS_DELETE_ALL_CHIPS_ON_CURRENT_TABLE(ROULETTE_DATA &sRouletteData)
	INT iTableID = sRouletteData.iTableID
	
	IF iTableID < 0
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_PROPS_DELETE_ALL_CHIPS_ON_CURRENT_TABLE - Invalid table ID cannot delete chips")
		EXIT
	ENDIF
	
	ROULETTE_PROPS_DELETE_ALL_CHIPS_FOR_TABLE(sRouletteData, iTableID)
ENDPROC

/// PURPOSE:
///    Gets how many chips objects should be displayed for the bet amount according to the 
///    available chip denominations
FUNC INT ROULETTE_PROPS_GET_NUMBER_OF_CHIPS_TO_DISPLAY_FOR_BET_AMOUNT(INT iBetAmount)	
	INT i = 0
	INT iMaxDenomination = COUNT_OF(ROULETTE_CHIP_DENOMINATIONS)

	// Just get the largest chip we could have for this bet as we don't have a way to
	// have a mixture of chips shown on a model
	REPEAT iMaxDenomination i
		INT iDenominationValue = ROULETTE_GET_CHIP_DENOMINATION_VALUE(INT_TO_ENUM(ROULETTE_CHIP_DENOMINATIONS, iMaxDenomination - i - 1))		
		
		// If the bet amount is within this denomination
		IF iBetAmount >= iDenominationValue	
			INT iMainAmount = (iBetAmount / iDenominationValue)
			// Never return more than can be displayed in a stack
			RETURN CLAMP_INT(iMainAmount, 0, ROULETTE_PROPS_NUMBER_OF_CHIPS_IN_STACK)
		ENDIF
	ENDREPEAT
	
	RETURN 0	
ENDFUNC

/// PURPOSE:
///    Deletes the chips belonging to the player in the given seat if they are in the betting zone one
FUNC INT ROULETTE_DELETE_CHIPS_IN_ZONE_FOR_SEAT(ROULETTE_DATA &sRouletteData, INT iTableID, INT iSeatID, INT iZoneID)	
	IF iSeatID = -1
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_DELETE_CHIPS_IN_ZONE_FOR_SEAT - invalid seat: ", iSeatID)
		RETURN 0
	ENDIF
	
	PLAYER_INDEX piPlayerAtSeat = ROULETTE_HELPER_GET_PLAYER_AT_SEAT(sRouletteData, iSeatID)
	
	IF piPlayerAtSeat = INVALID_PLAYER_INDEX()
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_DELETE_CHIPS_IN_ZONE_FOR_SEAT - invalid player at seat: ", iSeatID)
		RETURN 0
	ENDIF
	
	INT iBet = 0
	INT iXCoord = 0
	INT iYCoord = 0	
	INT iPlayerID = NATIVE_TO_INT(piPlayerAtSeat)
	INT iChipsDeleted = 0
	INT iStacksDeleted = 0
	
	REPEAT ROULETTE_BETTING_MAX_ALLOWED_BETS iBet
		ROULETTE_LAYOUT_INDEX_TO_LAYOUT_COORD(sRouletteData.playerBD[iPlayerID].iBets[iBet].iLayoutIndex, iXCoord, iYCoord)	
		
		IF sRouletteData.objChips[iSeatID][iBet] = NULL
		OR NOT DOES_ENTITY_EXIST(sRouletteData.objChips[iSeatID][iBet])
		OR NOT ROULETTE_LAYOUT_IS_COORD_IN_ZONE(iXCoord, iYCoord, iZoneID)
			RELOOP			
		ENDIF
		
		iStacksDeleted++
		iChipsDeleted += ROULETTE_PROPS_GET_NUMBER_OF_CHIPS_TO_DISPLAY_FOR_BET_AMOUNT(sRouletteData.playerBD[iPlayerID].iBets[iBet].iBetAmount)
		sRouletteData.vZoneAverageDeletedChipPos[iTableID][iZoneID - 1] += GET_ENTITY_COORDS(sRouletteData.objChips[iSeatID][iBet])
		DELETE_OBJECT(sRouletteData.objChips[iSeatID][iBet])
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_DELETE_CHIPS_IN_ZONE_FOR_SEAT - deleting bet: ", iBet, " at seat: ", iSeatID)
		sRouletteData.objChips[iSeatID][iBet] = NULL
	ENDREPEAT
	
	// Validation to avoid divide by 0
	IF iStacksDeleted <= 0
		RETURN iChipsDeleted
	ENDIF
	
	// Calculate average deleted chips
	VECTOR vTemp = sRouletteData.vZoneAverageDeletedChipPos[iTableID][iZoneID - 1]
	sRouletteData.vZoneAverageDeletedChipPos[iTableID][iZoneID - 1] = <<vTemp.X / iStacksDeleted, vTemp.Y / iStacksDeleted, vTemp.Z / iStacksDeleted>>
	
	RETURN iChipsDeleted
ENDFUNC

/// PURPOSE:
///    Deletes the chip props from the given zone id (0-2)   
FUNC INT ROULETTE_PROPS_DELETE_ZONE_CHIPS_FROM_TABLE(ROULETTE_DATA &sRouletteData, INT iTableID, INT iZoneID)
	INT iLocalSeatID = 0
	INT iChipsDeleted = 0
	
	// Reset average deleted chip pos
	sRouletteData.vZoneAverageDeletedChipPos[iTableID][iZoneID - 1] = <<0.0, 0.0, 0.0>>
	
	REPEAT ROULETTE_TABLE_MAX_NUMBER_OF_PLAYERS_PER_TABLE iLocalSeatID
		INT iSeatID = ROULETTE_HELPER_GET_SEAT_ID_FROM_LOCAL_SEAT_ID_AT_TABLE(iLocalSeatID, iTableID)
		iChipsDeleted += ROULETTE_DELETE_CHIPS_IN_ZONE_FOR_SEAT(sRouletteData, iTableID, iSeatID, iZoneID)
	ENDREPEAT
	
	RETURN iChipsDeleted
ENDFUNC

/// PURPOSE:
///    Deletes all chips from the local players current table if they are the betting zone one
FUNC INT ROULETTE_PROPS_DELETE_ZONE_CHIPS_FROM_CURRENT_TABLE(ROULETTE_DATA &sRouletteData, INT iZoneID)
	INT iTableID = sRouletteData.iTableID
	RETURN ROULETTE_PROPS_DELETE_ZONE_CHIPS_FROM_TABLE(sRouletteData, iTableID, iZoneID)
ENDFUNC

/// PURPOSE:
///    Updates the roulette cursor prop to the current position on the layout based on the
///    current snapping
PROC ROULETTE_PROPS_UPDATE_CURSOR_SNAP_POSITION(ROULETTE_DATA &sRouletteData)		
	INT iTableId = ROULETTE_HELPER_GET_TABLE_ID_FROM_SEAT_ID(sRouletteData.iSeatID)
	sRouletteData.vCursorPosition = ROULETTE_LAYOUT_GET_BET_POSITION_ON_TABLE_FOR_LAYOUT_COORD(iTableId, sRouletteData.iCursorXCoord, sRouletteData.iCursorYCoord)
	
	// Set cursor pos to the screen coord equivelant so cursor movement doesn't overwrite the nudge
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		FLOAT fX, fY
		GET_SCREEN_COORD_FROM_WORLD_COORD(sRouletteData.vCursorPosition, fX, fY)
		fX = CLAMP(fX, 0.0, 1.0)
		fY = CLAMP(fY, 0.0, 1.0)
		SET_CURSOR_POSITION(fX, fY)	
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(sRouletteData.objCursor)
		EXIT
	ENDIF
	
	SET_ENTITY_COORDS(sRouletteData.objCursor, sRouletteData.vCursorPosition)
	SET_ENTITY_HEADING(sRouletteData.objCursor, ROULETTE_TABLE_GET_HEADING(iTableID) + ROULETTE_PROPS_OBJECT_ROTATION_OFFSET)
	ROULETTE_PROPS_UPDATE_TABLE_HIGHLIGHTS(sRouletteData)
ENDPROC

/// PURPOSE:
///    Gets the top left corner of the cursor boundary   
FUNC VECTOR ROULETTE_PROPS_GET_CURSOR_BOUNDARY_TOP_RIGHT_POS()
	RETURN <<ROULETTE_UI_CURSOR_BOUND_TR_X, ROULETTE_UI_CURSOR_BOUND_TR_Y, 0>>
ENDFUNC

/// PURPOSE:
///    Gets the bottom right coner of the cursor boundary    
FUNC VECTOR ROULETTE_PROPS_GET_CURSOR_BOUNDARY_BOTTOM_LEFT_POS()
	RETURN <<ROULETTE_UI_CURSOR_BOUND_BL_X, ROULETTE_UI_CURSOR_BOUND_BL_Y, 0>>
ENDFUNC

/// PURPOSE:
///    Clamps the roulette cursors world position to the cursor boundary for the current table
PROC ROULETTE_PROPS_CLAMP_CURSOR_POSITION_TO_BOUNDS(ROULETTE_DATA &sRouletteData)		
	VECTOR vTopRight = ROULETTE_PROPS_GET_CURSOR_BOUNDARY_TOP_RIGHT_POS()
	VECTOR vBottomLeft = ROULETTE_PROPS_GET_CURSOR_BOUNDARY_BOTTOM_LEFT_POS()

	// Get cursor position in local space of the table
	INT iTableID = sRouletteData.iTableID
	sRouletteData.vCursorPosition -= ROULETTE_LAYOUT_GET_ORIGIN_FOR_TABLE(iTableID)
	sRouletteData.vCursorPosition = ROULETTE_TABLE_GET_VECTOR_ROTATED_TO_TABLE_SPACE(sRouletteData.vCursorPosition, iTableID)
	
	// Clamp in table space
	sRouletteData.vCursorPosition.x = FMIN(sRouletteData.vCursorPosition.x, vTopRight.x)
	sRouletteData.vCursorPosition.y = FMIN(sRouletteData.vCursorPosition.y, vTopRight.y)
	
	sRouletteData.vCursorPosition.x = FMAX(sRouletteData.vCursorPosition.x, vBottomLeft.x)
	sRouletteData.vCursorPosition.y = FMAX(sRouletteData.vCursorPosition.y, vBottomLeft.y)
	
	// Convert back to world coords
	sRouletteData.vCursorPosition = ROULETTE_TABLE_GET_VECTOR_RELATIVE_TO_TABLE(sRouletteData.vCursorPosition, iTableID) + ROULETTE_LAYOUT_GET_ORIGIN_FOR_TABLE(iTableID)
ENDPROC

/// PURPOSE:
///    Updates the current layout cursor coords according to the world position of the cursor
///    returns true if the layout coords have changed
FUNC BOOL ROULETTE_PROPS_UPDATE_CURSOR_LAYOUT_COORD_FROM_CURSOR_POSITION(ROULETTE_DATA &sRouletteData)
	ROULETTE_LAYOUT_GET_WORLD_POSITION_AS_LAYOUT_COORD_ON_TABLE(sRouletteData.vCursorPosition,
	sRouletteData.iCursorXCoord, sRouletteData.iCursorYCoord, sRouletteData.iTableID)
	
	BOOL bChangedLayoutPos
	bChangedLayoutPos = sRouletteData.iPrevCursorXCoord != sRouletteData.iCursorXCoord
	OR sRouletteData.iPrevCursorYCoord != sRouletteData.iCursorYCoord
	
	sRouletteData.iPrevCursorXCoord = sRouletteData.iCursorXCoord
	sRouletteData.iPrevCursorYCoord = sRouletteData.iCursorYCoord
	RETURN bChangedLayoutPos
ENDFUNC

/// PURPOSE:
///    Updates the cursor prop position based on the stored position, takes into account cursor bouunds
///    and updates the layout coordinates
PROC ROULETTE_PROPS_UPDATE_CURSOR_POSITION(ROULETTE_DATA &sRouletteData)		
	ROULETTE_PROPS_CLAMP_CURSOR_POSITION_TO_BOUNDS(sRouletteData)
	SET_ENTITY_COORDS(sRouletteData.objCursor, sRouletteData.vCursorPosition)
	INT iTableID = sRouletteData.iTableID
	SET_ENTITY_HEADING(sRouletteData.objCursor, ROULETTE_TABLE_GET_HEADING(iTableID) + ROULETTE_PROPS_OBJECT_ROTATION_OFFSET)
	IF ROULETTE_PROPS_UPDATE_CURSOR_LAYOUT_COORD_FROM_CURSOR_POSITION(sRouletteData)
		ROULETTE_PROPS_UPDATE_TABLE_HIGHLIGHTS(sRouletteData)
		ROULETTE_AUDIO_PLAY_SFX_HIGHLIGHT()
	ENDIF
ENDPROC

/// PURPOSE:
///    Updates the roulette cursor to show the correct model in the correct position
PROC ROULETTE_PROPS_UPDATE_CHIP_CURSOR_MODEL(ROULETTE_DATA &sRouletteData)		
	VECTOR vPosition
	FLOAT fHeading = 0
	
	IF DOES_ENTITY_EXIST(sRouletteData.objCursor)
		vPosition = GET_ENTITY_COORDS(sRouletteData.objCursor)
		fHeading = GET_ENTITY_HEADING(sRouletteData.objCursor)
		DELETE_OBJECT(sRouletteData.objCursor)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_PROPS_UPDATE_CHIP_CURSOR_MODEL - deleting chip cursor")
		sRouletteData.objCursor = NULL
	ELSE
		// First time making cursor place at first table to begin with
		vPosition = ROULETTE_TABLE_GET_POSITION(0)
	ENDIF
	
	sRouletteData.eCurrentChipToPlace = ROULETTE_BETTING_GET_VALID_CHIP_DENOMINATION(sRouletteData, sRouletteData.eCurrentChipToPlace)
	sRouletteData.objCursor = CREATE_OBJECT_NO_OFFSET(ROULETTE_PROPS_GET_CURSOR_MODEL(sRouletteData.eCurrentChipToPlace), vPosition, FALSE, FALSE)
	SET_ENTITY_HEADING(sRouletteData.objCursor, fHeading)
ENDPROC

/// PURPOSE:
///    Updates the chip cursor with the current position and chip model
PROC ROULETTE_PROPS_UPDATE_CHIP_CURSOR(ROULETTE_DATA &sRouletteData)
	ROULETTE_PROPS_UPDATE_CURSOR_POSITION(sRouletteData)
	ROULETTE_PROPS_UPDATE_CHIP_CURSOR_MODEL(sRouletteData)
ENDPROC

/// PURPOSE:
///    Gets the correct chip stack model for the bet amount (will get the highest value chip stack
///    that is within the bet amount)
FUNC MODEL_NAMES ROULETTE_PROPS_GET_CHIP_STACK_MODEL_FOR_BET_AMOUNT(INT iBetAmount)	
	INT i = 0
	INT iMaxDenomination = COUNT_OF(ROULETTE_CHIP_DENOMINATIONS)
	
	// Just get the largest chip we could have for this bet as we don't have a way to
	// have a mixture of chips shown on a model
	REPEAT iMaxDenomination i	
		ROULETTE_CHIP_DENOMINATIONS eDenomination = INT_TO_ENUM(ROULETTE_CHIP_DENOMINATIONS, iMaxDenomination - i - 1)
		IF iBetAmount >= ROULETTE_GET_CHIP_DENOMINATION_VALUE(eDenomination)
			RETURN ROULETTE_PROPS_GET_CHIP_STACK_MODEL(eDenomination)
		ENDIF
	ENDREPEAT
	
	RETURN ROULETTE_PROPS_GET_CHIP_STACK_MODEL(TEN_DOLLAR_CHIP)
ENDFUNC

/// PURPOSE:
///    Checks if the player can update the chip model at the layout index  
FUNC BOOL ROULETTE_PROPS_CAN_PLACE_PLAYERS_CHIP_AT_POSITION(ROULETTE_DATA &sRouletteData, INT iPlayerID, INT iLayoutIndex)
	INT iLocalPlayerID = NATIVE_TO_INT(PLAYER_ID())
	BOOL bIsLocalPlayer = (iPlayerID = iLocalPlayerID)
	
	IF bIsLocalPlayer
		RETURN TRUE
	ENDIF
	
	// Local players chip stack takes priority over other players
	RETURN NOT ROULETTE_BETTING_DOES_BET_ARRAY_CONTAIN_BET_AT_LAYOUT_INDEX(sRouletteData.playerBD[iLocalPlayerID].iBets, iLayoutIndex)
ENDFUNC

/// PURPOSE:
///    Creates a chip stack model on the table for the given bet
FUNC OBJECT_INDEX ROULETTE_PROPS_CREATE_CHIP_STACK_FOR_BET_AT_TABLE(ROULETTE_BET &sBet, INT iTableID)
	INT iBetAmount = sBet.iBetAmount
	
	// Get chip position hidden under the table surface
	VECTOR vChipPosition = ROULETTE_LAYOUT_GET_BET_POSITION_ON_TABLE_FOR_LAYOUT_INDEX(sBet.iLayoutIndex, iTableID)
	vChipPosition -= <<0, 0, (ROULETTE_PROPS_CHIP_STACK_HEIGHT)>>
	
	// Move them up by the individual chip height * how many chips should be displayed for the bet
	INT iChipsToDisplay = ROULETTE_PROPS_GET_NUMBER_OF_CHIPS_TO_DISPLAY_FOR_BET_AMOUNT(iBetAmount) - 1
	vChipPosition += <<0, 0, ((ROULETTE_PROPS_CHIP_STACK_HEIGHT / ROULETTE_PROPS_NUMBER_OF_CHIPS_IN_STACK) * iChipsToDisplay)>>
	
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_PROPS_CREATE_CHIP_STACK_FOR_BET_AT_TABLE - Before chip create at iTableID: ", iTableID)
	
	OBJECT_INDEX objChipStack 
	objChipStack = CREATE_OBJECT(ROULETTE_PROPS_GET_CHIP_STACK_MODEL_FOR_BET_AMOUNT(iBetAmount),
	vChipPosition, FALSE)
	
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_PROPS_CREATE_CHIP_STACK_FOR_BET_AT_TABLE - After chip create at iTableID: ", iTableID)
	SET_ENTITY_HEADING(objChipStack, ROULETTE_TABLE_GET_HEADING(iTableID) + ROULETTE_PROPS_OBJECT_ROTATION_OFFSET)
	
	RETURN objChipStack
ENDFUNC

/// PURPOSE:
///    Updates the tables zone tracking by adding the layout index
PROC ROULETTE_PROPS_UPDATE_TABLE_ZONE_TRACKING_WITH_LAYOUT_INDEX(ROULETTE_DATA &sRouletteData, INT iTableID, INT iLayoutIndex)
	INT iXCoord, iYCoord
	ROULETTE_LAYOUT_INDEX_TO_LAYOUT_COORD(iLayoutIndex, iXCoord, iYCoord)
	
	IF ROULETTE_LAYOUT_IS_COORD_IN_ZONE_ONE(iXCoord, iYCoord)
		SET_BIT(sRouletteData.iTableBS[iTableID], ROULETTE_CLIENT_TABLE_BS_HAS_BET_IN_ZONE_ONE)
	ENDIF
	
	IF ROULETTE_LAYOUT_IS_COORD_IN_ZONE_TWO(iXCoord, iYCoord)
		SET_BIT(sRouletteData.iTableBS[iTableID], ROULETTE_CLIENT_TABLE_BS_HAS_BET_IN_ZONE_TWO)
	ENDIF
	
	IF ROULETTE_LAYOUT_IS_COORD_IN_ZONE_THREE(iXCoord, iYCoord)
		SET_BIT(sRouletteData.iTableBS[iTableID], ROULETTE_CLIENT_TABLE_BS_HAS_BET_IN_ZONE_THREE)
	ENDIF
ENDPROC

/// PURPOSE:
///    Updates the displaying of chips on the layout for the player
PROC ROULETTE_PROPS_UPDATE_CHIPS_FOR_PLAYERS_BETS(ROULETTE_DATA &sRouletteData, INT iPlayerIndex)
	INT iSeatID = sRouletteData.playerBD[iPlayerIndex].iSeatID
	
	// Player being processed might be leaving the table
	IF NOT ROULETTE_HELPER_IS_PLAYER_AT_SEAT_STILL_PLAYING(sRouletteData, iSeatID) 
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_PROPS_UPDATE_CHIPS_FOR_PLAYERS_BETS - player at seat ", iSeatID, " isn't playing" )
		EXIT
	ENDIF	
	
	INT iTableID = sRouletteData.playerBD[iPlayerIndex].iTableID	
	ROULETTE_PROPS_DELETE_SEAT_CHIPS(sRouletteData, iSeatID)
	
	INT i = 0	
	REPEAT ROULETTE_BETTING_MAX_ALLOWED_BETS i
		ROULETTE_BET sCurrentBet = sRouletteData.playerBD[iPlayerIndex].iBets[i]	
		
		IF sCurrentBet.iLayoutIndex = -1
			RELOOP
		ENDIF
		
		// Remove chip if bet value is zero
		IF sCurrentBet.iBetAmount <= 0	
			RELOOP
		ENDIF
			
		IF NOT ROULETTE_PROPS_CAN_PLACE_PLAYERS_CHIP_AT_POSITION(sRouletteData, iPlayerIndex, sCurrentBet.iLayoutIndex)
			CPRINTLN(DEBUG_ROULETTE, "ROULETTE_PROPS_UPDATE_CHIPS_FOR_PLAYERS_BETS - player ",
			GET_PLAYER_NAME(INT_TO_NATIVE(PLAYER_INDEX, iPlayerIndex)), " can't place at index ", sCurrentBet.iLayoutIndex)
			RELOOP
		ENDIF
		
		sRouletteData.objChips[iSeatID][i] = ROULETTE_PROPS_CREATE_CHIP_STACK_FOR_BET_AT_TABLE(sCurrentBet, iTableID)
		ROULETTE_PROPS_UPDATE_TABLE_ZONE_TRACKING_WITH_LAYOUT_INDEX(sRouletteData, iTableID, sCurrentBet.iLayoutIndex)
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Updates the displaying of chips on the layout for the local player
PROC ROULETTE_PROPS_UPDATE_CHIPS_FOR_LOCAL_PLAYER_BETS(ROULETTE_DATA &sRouletteData)
	ROULETTE_PROPS_UPDATE_CHIPS_FOR_PLAYERS_BETS(sRouletteData, NATIVE_TO_INT(PLAYER_ID()))
ENDPROC

/// PURPOSE:
///    Updates the chips on the given table
PROC ROULETTE_PROPS_UPDATE_CHIPS_FOR_TABLE(ROULETTE_DATA &sRouletteData, INT iTableID)
	INT iLocalSeatID
	
	#IF IS_DEBUG_BUILD
		TEXT_LABEL_63 tlUpdateText = "Updating chips for table "
		tlUpdateText += iTableID
		DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_TL(tlUpdateText), <<0.1, 0.5 * (iTableID + 1), 0.0>>, 255, 0, 0)
	#ENDIF
	
	REPEAT ROULETTE_TABLE_MAX_NUMBER_OF_PLAYERS_PER_TABLE iLocalSeatID
		INT iSeatID = ROULETTE_HELPER_GET_SEAT_ID_FROM_LOCAL_SEAT_ID_AT_TABLE(iLocalSeatID, iTableID)
		
		// Check if any player is playing at our table
		IF ROULETTE_HELPER_IS_PLAYER_AT_SEAT_STILL_PLAYING(sRouletteData, iSeatID)
			INT iPlayer = NATIVE_TO_INT(sRouletteData.serverBD.playersUsingSeats[iSeatID])	
			ROULETTE_PROPS_UPDATE_CHIPS_FOR_PLAYERS_BETS(sRouletteData, iPlayer)
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Updates the displaying of chips for the table the player is currently at
PROC ROULETTE_PROPS_UPDATE_CHIPS_FOR_CURRENT_TABLE(ROULETTE_DATA &sRouletteData)	
	INT iTableID = sRouletteData.playerBD[NATIVE_TO_INT(PLAYER_ID())].iTableID
	ROULETTE_PROPS_UPDATE_CHIPS_FOR_TABLE(sRouletteData, iTableID)
ENDPROC

/// PURPOSE:
///    Reserves the required amount of peds for roulette
PROC ROULETTE_PROPS_RESERVE_REQUIRED_PEDS()
	RESERVE_NETWORK_MISSION_PEDS(GET_NUM_CREATED_MISSION_PEDS() + ROULETTE_TABLE_MAX_NUMBER_OF_TABLES)
ENDPROC

/// PURPOSE:
///    Loads the required dealer models
PROC ROULETTE_PROPS_LOAD_DEALER_PED_MODELS()
	REQUEST_MODEL(ROULETTE_PROPS_GET_DEALER_PED_MODEL(TRUE))
	REQUEST_MODEL(ROULETTE_PROPS_GET_DEALER_PED_MODEL(FALSE))
ENDPROC

/// PURPOSE:
///    Checks if the required dealer models have loaded   
FUNC BOOL ROULETTE_PROPS_HAVE_DEALER_PED_MODELS_LOADED()
	RETURN HAS_MODEL_LOADED(ROULETTE_PROPS_GET_DEALER_PED_MODEL(TRUE)) AND 
	HAS_MODEL_LOADED(ROULETTE_PROPS_GET_DEALER_PED_MODEL(FALSE))
ENDFUNC

/// PURPOSE:
///    Releases all the dealer models used by roulette
PROC ROULETTE_PROPS_RELEASE_ALL_DEALER_PED_MODELS()
	SET_MODEL_AS_NO_LONGER_NEEDED(ROULETTE_PROPS_GET_DEALER_PED_MODEL(TRUE))
	SET_MODEL_AS_NO_LONGER_NEEDED(ROULETTE_PROPS_GET_DEALER_PED_MODEL(FALSE))
ENDPROC

/// PURPOSE:
///    Creates a dealer at the table    
FUNC BOOL ROULETTE_PROPS_CREATE_DEALER_PED(ROULETTE_DATA &sRouletteData, INT iTableID)
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
		CWARNINGLN(DEBUG_ROULETTE, "ROULETTE_PROPS_CREATE_DEALER_PED - Called by non-host! should only be called by script host")
		RETURN FALSE
	ENDIF
	
	ROULETTE_PROPS_LOAD_DEALER_PED_MODELS()
	IF NOT ROULETTE_PROPS_HAVE_DEALER_PED_MODELS_LOADED()
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_PROPS_CREATE_DEALER_PED - Dealer models havn't loaded!")
		RETURN FALSE
	ENDIF
	
	IF NOT CAN_REGISTER_MISSION_PEDS(1)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_PROPS_CREATE_DEALER_PED - Can't register mission peds!")
		RETURN FALSE
	ENDIF
	
	BOOL bIsFemale = ROULETTE_PROPS_SHOULD_TABLE_DEALER_BE_FEMALE(iTableID)
	MODEL_NAMES eDealerModel = ROULETTE_PROPS_GET_DEALER_PED_MODEL(bIsFemale)		
	PED_INDEX pedID = CREATE_PED(PEDTYPE_MISSION, eDealerModel,	ROULETTE_TABLE_GET_DEALER_POSITION(iTableID), ROULETTE_TABLE_GET_DEALER_HEADING(iTableID))
	
	SET_ENTITY_CAN_BE_DAMAGED(pedID, FALSE)
	SET_PED_AS_ENEMY(pedID, FALSE)
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedID, TRUE)
	SET_PED_RESET_FLAG(pedID, PRF_DisablePotentialBlastReactions, TRUE)
	SET_PED_CONFIG_FLAG(pedID, PCF_UseKinematicModeWhenStationary, TRUE)
	SET_PED_CONFIG_FLAG(pedID, PCF_DontActivateRagdollFromExplosions, TRUE)
	SET_ENTITY_CAN_ONLY_BE_DAMAGED_BY_SCRIPT_PARTICIPANTS(pedID, TRUE)
	SET_PED_CAN_EVASIVE_DIVE(pedID, FALSE)
	SET_TREAT_AS_AMBIENT_PED_FOR_DRIVER_LOCKON(pedID, TRUE)
	SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(pedID, FALSE)
	SET_PED_CONFIG_FLAG(pedID, PCF_DisableExplosionReactions, TRUE)
	SET_ENTITY_AS_MISSION_ENTITY(pedID)
	ROULETTE_PROPS_SET_DEALER_PED_COMPONENTS(pedID, bIsFemale, iTableID)
	ROULETTE_PROPS_SET_DEALER_PED_VOICE_GROUP(pedID, bIsFemale, iTableID)	
	ROULETTE_PROPS_RELEASE_ALL_DEALER_PED_MODELS()
	
	sRouletteData.serverBD.niDealerPed[iTableID] = PED_TO_NET(pedID)
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_PROPS_CREATE_DEALER_PEDS - Dealer for table ", iTableID, " created")
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Creates the dealer peds for a game of roulette, this should only be
///    used by the script host
FUNC BOOL ROULETTE_PROPS_CREATE_DEALER_PEDS(ROULETTE_DATA &sRouletteData)
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
		CWARNINGLN(DEBUG_ROULETTE, "ROULETTE_PROPS_CREATE_DEALER_PEDS - Called by non-host! should only be called by script host")
		RETURN TRUE
	ENDIF
	
	ROULETTE_PROPS_LOAD_DEALER_PED_MODELS()
	IF NOT ROULETTE_PROPS_HAVE_DEALER_PED_MODELS_LOADED()
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_PROPS_CREATE_DEALER_PEDS - Dealer models havn't loaded!")
		RETURN FALSE
	ENDIF
	
	IF NOT CAN_REGISTER_MISSION_PEDS(ROULETTE_TABLE_MAX_NUMBER_OF_TABLES)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_PROPS_CREATE_DEALER_PEDS - Can't register mission peds!")
		RETURN FALSE
	ENDIF
	
	INT i = 0
	REPEAT ROULETTE_TABLE_MAX_NUMBER_OF_TABLES i			
		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(sRouletteData.serverBD.niDealerPed[i]) 
		AND NOT ROULETTE_TABLE_IS_DISABLED(i)		
			BOOL bIsFemale = ROULETTE_PROPS_SHOULD_TABLE_DEALER_BE_FEMALE(i)
			MODEL_NAMES eDealerModel = ROULETTE_PROPS_GET_DEALER_PED_MODEL(bIsFemale)		
			PED_INDEX pedID = CREATE_PED(PEDTYPE_MISSION, eDealerModel,	ROULETTE_TABLE_GET_DEALER_POSITION(i), ROULETTE_TABLE_GET_DEALER_HEADING(i))
			
			sRouletteData.serverBD.niDealerPed[i] = PED_TO_NET(pedID)
			SET_ENTITY_CAN_BE_DAMAGED(pedID, FALSE)
			SET_PED_AS_ENEMY(pedID, FALSE)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedID, TRUE)
			SET_PED_RESET_FLAG(pedID, PRF_DisablePotentialBlastReactions, TRUE)
			SET_PED_CONFIG_FLAG(pedID, PCF_UseKinematicModeWhenStationary, TRUE)
			SET_PED_CONFIG_FLAG(pedID, PCF_DontActivateRagdollFromExplosions, TRUE)
			SET_ENTITY_CAN_ONLY_BE_DAMAGED_BY_SCRIPT_PARTICIPANTS(pedID, TRUE)
			SET_PED_CAN_EVASIVE_DIVE(pedID, FALSE)
			SET_TREAT_AS_AMBIENT_PED_FOR_DRIVER_LOCKON(pedID, TRUE)
			SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(pedID, FALSE)
			SET_PED_CONFIG_FLAG(pedID, PCF_DisableExplosionReactions, TRUE)
			SET_ENTITY_AS_MISSION_ENTITY(pedID)
			ROULETTE_PROPS_SET_DEALER_PED_COMPONENTS(pedID, bIsFemale, i)
			ROULETTE_PROPS_SET_DEALER_PED_VOICE_GROUP(pedID, bIsFemale, i)
			CPRINTLN(DEBUG_ROULETTE, "ROULETTE_PROPS_CREATE_DEALER_PEDS - Dealer for table ", i, " created")
		ENDIF
	ENDREPEAT
	
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_PROPS_CREATE_DEALER_PEDS - All dealers created")
	ROULETTE_PROPS_RELEASE_ALL_DEALER_PED_MODELS()
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Checks if the bone on the provided entity is safe to be used   
FUNC BOOL ROULETTE_PROPS_IS_ENTITY_BONE_SAFE_TO_ACCESS(OBJECT_INDEX objWithBone, STRING strBoneName)
	IF IS_STRING_NULL_OR_EMPTY(strBoneName) 
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_PROPS_IS_ENTITY_BONE_SAFE_TO_ACCESS - No bone name defined!")
		RETURN FALSE
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(objWithBone)	
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_PROPS_IS_ENTITY_BONE_SAFE_TO_ACCESS - ", strBoneName, " - Entity does not exist!")
		RETURN FALSE
	ENDIF
	
	IF NOT DOES_ENTITY_HAVE_DRAWABLE(objWithBone)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_PROPS_IS_ENTITY_BONE_SAFE_TO_ACCESS - ", strBoneName, " - Entity doesn't have drawable!")
		RETURN FALSE
	ENDIF
	
	INT iBoneIndex = GET_ENTITY_BONE_INDEX_BY_NAME(objWithBone, strBoneName)
	
	IF iBoneIndex = -1
		PRINTLN(DEBUG_ROULETTE, "ROULETTE_PROPS_IS_ENTITY_BONE_SAFE_TO_ACCESS - ", strBoneName, " - bone index not found")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Gets the bone position of an entity making sure it is safe to access, return zero vector if not safe to access   
FUNC VECTOR ROULETTE_PROPS_GET_ENTITY_BONE_POSITION_SAFELY(OBJECT_INDEX objWithBone, STRING strBoneName)	
	IF NOT ROULETTE_PROPS_IS_ENTITY_BONE_SAFE_TO_ACCESS(objWithBone, strBoneName)
		RETURN <<0, 0, 0>>
	ENDIF
	
	INT iBoneIndex = GET_ENTITY_BONE_INDEX_BY_NAME(objWithBone, strBoneName)
	RETURN GET_ENTITY_BONE_POSTION(objWithBone, iBoneIndex)
ENDFUNC

/// PURPOSE:
///    Gets the bone rotation of an entity making sure it is safe to access, return zero vector if not safe to access   
FUNC VECTOR ROULETTE_PROPS_GET_ENTITY_BONE_ROTATION_SAFELY(OBJECT_INDEX objWithBone, STRING strBoneName, BOOL bWorldRotation = TRUE)
	IF NOT ROULETTE_PROPS_IS_ENTITY_BONE_SAFE_TO_ACCESS(objWithBone, strBoneName)
		RETURN <<0, 0, 0>>
	ENDIF
	
	INT iBoneIndex = GET_ENTITY_BONE_INDEX_BY_NAME(objWithBone, strBoneName)
	
	IF bWorldRotation
		RETURN GET_ENTITY_BONE_ROTATION(objWithBone, iBoneIndex)
	ENDIF
	
	RETURN GET_ENTITY_BONE_OBJECT_ROTATION(objWithBone, iBoneIndex)
ENDFUNC

/// PURPOSE:
///    Gets the bone position of the given seat ID
FUNC VECTOR ROULETTE_PROPS_GET_SEAT_BONE_POSITION(ROULETTE_DATA &sRouletteData, INT iSeatID)
	INT iTableID = ROULETTE_HELPER_GET_TABLE_ID_FROM_SEAT_ID(iSeatID)
	OBJECT_INDEX objTable = ROULETTE_HELPER_GET_TABLE_OBJECT_FOR_ID(sRouletteData, iTableID)
	INT iLocalSeatID = ROULETTE_HELPER_GET_LOCAL_SEAT_ID_FROM_SEAT_ID(iSeatID)
	STRING strBoneName = ROULETTE_ANIM_GET_SEAT_BONE_NAME(iLocalSeatID)
	RETURN ROULETTE_PROPS_GET_ENTITY_BONE_POSITION_SAFELY(objTable, strBoneName)
ENDFUNC

/// PURPOSE:
///    Gets the bone rotation of the given seat ID
FUNC VECTOR ROULETTE_PROPS_GET_SEAT_BONE_ROTATION(ROULETTE_DATA &sRouletteData, INT iSeatID)
	INT iTableID = ROULETTE_HELPER_GET_TABLE_ID_FROM_SEAT_ID(iSeatID)
	OBJECT_INDEX objTable = ROULETTE_HELPER_GET_TABLE_OBJECT_FOR_ID(sRouletteData, iTableID)
	INT iLocalSeatID = ROULETTE_HELPER_GET_LOCAL_SEAT_ID_FROM_SEAT_ID(iSeatID)
	STRING strBoneName = ROULETTE_ANIM_GET_SEAT_BONE_NAME(iLocalSeatID)
	RETURN ROULETTE_PROPS_GET_ENTITY_BONE_ROTATION_SAFELY(objTable, strBoneName)
ENDFUNC

/// PURPOSE:
///    Gets the bone position of the given seat ID
FUNC VECTOR ROULETTE_PROPS_GET_LOCAL_SEAT_BONE_POSITION(ROULETTE_DATA &sRouletteData, INT iLocalSeatID, INT iTableID)
	OBJECT_INDEX objTable = ROULETTE_HELPER_GET_TABLE_OBJECT_FOR_ID(sRouletteData, iTableID)
	STRING strBoneName = ROULETTE_ANIM_GET_SEAT_BONE_NAME(iLocalSeatID)
	RETURN ROULETTE_PROPS_GET_ENTITY_BONE_POSITION_SAFELY(objTable, strBoneName)
ENDFUNC

/// PURPOSE:
///    Gets the bone rotation of the given seat ID
FUNC VECTOR ROULETTE_PROPS_GET_LOCAL_SEAT_BONE_ROTATION(INT iLocalSeatID, INT iTableID)
	OBJECT_INDEX objTable = ROULETTE_HELPER_GET_TABLE_OBJECT_FOR_ID(iTableID)
	STRING strBoneName = ROULETTE_ANIM_GET_SEAT_BONE_NAME(iLocalSeatID)
	RETURN ROULETTE_PROPS_GET_ENTITY_BONE_ROTATION_SAFELY(objTable, strBoneName)
ENDFUNC

/// PURPOSE:
///   Gets the current local rotation of the wheel on the table with the given table ID 
FUNC VECTOR ROULETTE_PROPS_GET_WHEEL_CURRENT_LOCAL_ROTATION(OBJECT_INDEX objTable)	
	STRING strBoneName = ROULETTE_ANIM_GET_WHEEL_BONE_NAME()
	RETURN ROULETTE_PROPS_GET_ENTITY_BONE_ROTATION_SAFELY(objTable, strBoneName, FALSE)
ENDFUNC

/// PURPOSE:
///   Gets the current rotation of the wheel on the table with the given table ID 
FUNC VECTOR ROULETTE_PROPS_GET_WHEEL_CURRENT_ROTATION(OBJECT_INDEX objTable)	
	STRING strBoneName = ROULETTE_ANIM_GET_WHEEL_BONE_NAME()
	RETURN ROULETTE_PROPS_GET_ENTITY_BONE_ROTATION_SAFELY(objTable, strBoneName)
ENDFUNC

/// PURPOSE:
///     Gets the bone position of the wheel on the table with the given table ID
FUNC VECTOR ROULETTE_PROPS_GET_WHEEL_BONE_POSITION(OBJECT_INDEX objTable)
	STRING strBoneName = ROULETTE_ANIM_GET_WHEEL_BONE_NAME()
	RETURN ROULETTE_PROPS_GET_ENTITY_BONE_POSITION_SAFELY(objTable, strBoneName)
ENDFUNC

/// PURPOSE:
///    Creates a ball prop for the given table
FUNC BOOL ROULETTE_PROPS_CREATE_BALL_PROP_FOR_TABLE(ROULETTE_DATA &sRouletteData, INT iTableID, BOOL bHideOnCreation = TRUE)	
	
	IF ROULETTE_TABLE_IS_DISABLED(iTableID)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_PROPS_CREATE_BALL_PROP_FOR_TABLE - Table is disabled")
		RETURN TRUE
	ENDIF
	
	IF DOES_ENTITY_EXIST(sRouletteData.sSpin[iTableID].objBall)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_PROPS_CREATE_BALL_PROP_FOR_TABLE - Ball already exists")
		RETURN TRUE
	ENDIF
	
	MODEL_NAMES eBallModel = ROULETTE_PROPS_GET_BALL_MODEL()
	
	IF NOT HAS_MODEL_LOADED(eBallModel)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_PROPS_CREATE_BALL_PROP_FOR_TABLE - Model hasn't loaded")
		RETURN FALSE
	ENDIF
	
	sRouletteData.sSpin[iTableID].objBall = CREATE_OBJECT_NO_OFFSET(eBallModel,
	ROULETTE_TABLE_GET_POSITION(iTableID), FALSE)	
	SET_ENTITY_VISIBLE(sRouletteData.sSpin[iTableID].objBall, NOT bHideOnCreation)
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_PROPS_CREATE_BALL_PROP_FOR_TABLE - created ball ", iTableID)
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Gets the low stakes table model name 
FUNC MODEL_NAMES ROULETTE_PROPS_GET_LOW_STAKES_TABEL_MODEL()
	RETURN INT_TO_ENUM(MODEL_NAMES, ROULETTE_PROPS_LOW_STAKES_TABLE_MODEL)
ENDFUNC

/// PURPOSE:
///    Gets the high stakes table model name 
FUNC MODEL_NAMES ROULETTE_PROPS_GET_HIGH_STAKES_TABEL_MODEL()
	RETURN INT_TO_ENUM(MODEL_NAMES, ROULETTE_PROPS_HIGH_STAKES_TABLE_MODEL)
ENDFUNC

/// PURPOSE:
///    Returns the correct model name for the table ID depending on if it is a high or lowstakes table 
FUNC MODEL_NAMES ROULETTE_PROPS_GET_TABLE_MODEL_FOR_ID(INT iTableID)
	IF ROULETTE_TABLE_IS_HIGH_STAKES(iTableID)
		RETURN ROULETTE_PROPS_GET_HIGH_STAKES_TABEL_MODEL()
	ENDIF
	
	RETURN ROULETTE_PROPS_GET_LOW_STAKES_TABEL_MODEL()
ENDFUNC

/// PURPOSE:
///    Creates the model for the table, returns true if it is successful.
///    bRequestModel determine if the model should be requested
FUNC OBJECT_INDEX ROULETTE_PROPS_CREATE_TABLE(INT iTableID, BOOL bLoadModel = FALSE)			
	MODEL_NAMES eTableModel = ROULETTE_PROPS_GET_TABLE_MODEL_FOR_ID(iTableID)
	
	IF bLoadModel
		REQUEST_MODEL(eTableModel)
	ENDIF
	
	// Not ready to create
	IF NOT HAS_MODEL_LOADED(eTableModel)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_PROPS_CREATE_TABLE - Model isn't loaded for ", iTableID, " model: ", ENUM_TO_INT(eTableModel))
		RETURN NULL
	ENDIF
	
	INT iTintIndex = ROULETTE_PROPS_GET_TABLE_MODEL_TINT_INDEX(iTableID, ROULETTE_TABLE_IS_HIGH_STAKES(iTableID))	
	VECTOR vPos = ROULETTE_TABLE_GET_POSITION(iTableID)
	FLOAT fHeading = ROULETTE_TABLE_GET_HEADING(iTableID)
	
	OBJECT_INDEX objTable
	
	IF g_bRouletteReuseExistingTables
		// Try grab object first
		objTable = GET_CLOSEST_OBJECT_OF_TYPE(vPos, 1, eTableModel, FALSE, FALSE, FALSE)
		
		IF NOT DOES_ENTITY_EXIST(objTable)
			objTable = CREATE_OBJECT_NO_OFFSET(eTableModel, vPos, FALSE, FALSE)
		ELSE
			// Take ownership if object already exists
			SET_ENTITY_AS_MISSION_ENTITY(objTable, FALSE, TRUE)
		ENDIF
	ELSE
		objTable = CREATE_OBJECT_NO_OFFSET(eTableModel, vPos, FALSE, FALSE)
	ENDIF	
	
	SET_ENTITY_HEADING(objTable, fHeading)
	SET_OBJECT_TINT_INDEX(objTable, iTintIndex)
	
	ROULETTE_PROPS_PROCESS_CREATED_TABLE(objTable, iTableID)
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_PROPS_CREATE_TABLE - Created table ", iTableID)
	
	IF bLoadModel
		SET_MODEL_AS_NO_LONGER_NEEDED(eTableModel)
	ENDIF
	
	RETURN objTable
ENDFUNC

/// PURPOSE:
///    Creates all table models and populates the array with them 
FUNC BOOL ROULETTE_PROPS_CREATE_TABLE_MODELS(OBJECT_INDEX &objTables[]) 
	MODEL_NAMES eLowStakesTableModel = ROULETTE_PROPS_GET_LOW_STAKES_TABEL_MODEL()
	MODEL_NAMES eHighStakesTableModel = ROULETTE_PROPS_GET_HIGH_STAKES_TABEL_MODEL()
	REQUEST_MODEL(eLowStakesTableModel)
	REQUEST_MODEL(eHighStakesTableModel)
	
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_PROPS_CREATE_TABLE_MODELS - called")
	
	INT iTableID = 0
	INT iMaxTables = COUNT_OF(objTables)
	REPEAT iMaxTables iTableID
		
		// Already created this table s skip over it
		IF DOES_ENTITY_EXIST(objTables[iTableID])
			CPRINTLN(DEBUG_ROULETTE, "ROULETTE_PROPS_CREATE_TABLE_MODELS - Table ", iTableID, " is already created")
			RELOOP
		ENDIF
		
		objTables[iTableID] = ROULETTE_PROPS_CREATE_TABLE(iTableID)
		
		IF NOT DOES_ENTITY_EXIST(objTables[iTableID])
			CPRINTLN(DEBUG_ROULETTE, "ROULETTE_PROPS_CREATE_TABLE_MODELS - Table ", iTableID, " failed to create")
			RETURN FALSE
		ENDIF
	ENDREPEAT
	
	SET_MODEL_AS_NO_LONGER_NEEDED(eLowStakesTableModel)
	SET_MODEL_AS_NO_LONGER_NEEDED(eHighStakesTableModel)
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Creates ball and table models for the roulette minigame, returns false if a table fails
///    to create all models
FUNC BOOL ROULETTE_PROPS_CREATE_REQUIRED_MODELS(ROULETTE_DATA &sRouletteData)	
	MODEL_NAMES eLowStakesTableModel = ROULETTE_PROPS_GET_LOW_STAKES_TABEL_MODEL()
	MODEL_NAMES eHighStakesTableModel = ROULETTE_PROPS_GET_HIGH_STAKES_TABEL_MODEL()
	REQUEST_MODEL(eLowStakesTableModel)
	REQUEST_MODEL(eHighStakesTableModel)
	
	INT iTableID = 0
	REPEAT ROULETTE_TABLE_MAX_NUMBER_OF_TABLES iTableID
		IF NOT ROULETTE_PROPS_CREATE_BALL_PROP_FOR_TABLE(sRouletteData, iTableID)
			CPRINTLN(DEBUG_ROULETTE, "ROULETTE_PROPS_CREATE_TABLE_MODELS - ball ", iTableID, " didn't create")
			RETURN FALSE
		ENDIF
		
		IF DOES_ENTITY_EXIST(sRouletteData.sSpin[iTableID].objWheel)
			RELOOP
		ENDIF
		
		sRouletteData.sSpin[iTableID].objWheel = ROULETTE_PROPS_CREATE_TABLE(iTableID)
		
		IF NOT DOES_ENTITY_EXIST(sRouletteData.sSpin[iTableID].objWheel)
			RETURN FALSE
		ENDIF
	ENDREPEAT
	
	SET_MODEL_AS_NO_LONGER_NEEDED(eLowStakesTableModel)
	SET_MODEL_AS_NO_LONGER_NEEDED(eHighStakesTableModel)
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Requests all the required chip stack models for a game of roulette
PROC ROULETTE_PROPS_REQUEST_ALL_CHIP_STACKS()
	REQUEST_MODEL(ROULETTE_PROPS_GET_CHIP_STACK_MODEL(TEN_DOLLAR_CHIP))
	REQUEST_MODEL(ROULETTE_PROPS_GET_CHIP_STACK_MODEL(FIFTY_DOLLAR_CHIP))
	REQUEST_MODEL(ROULETTE_PROPS_GET_CHIP_STACK_MODEL(ONE_HUNDRED_DOLLAR_CHIP))
	REQUEST_MODEL(ROULETTE_PROPS_GET_CHIP_STACK_MODEL(FIVE_HUNDRED_DOLLAR_CHIP))
	REQUEST_MODEL(ROULETTE_PROPS_GET_CHIP_STACK_MODEL(ONE_THOUSAND_DOLLAR_CHIP))
	REQUEST_MODEL(ROULETTE_PROPS_GET_CHIP_STACK_MODEL(FIVE_THOUSAND_DOLLAR_CHIP))
	REQUEST_MODEL(ROULETTE_PROPS_GET_CHIP_STACK_MODEL(TEN_THOUSAND_DOLLAR_CHIP))
ENDPROC

/// PURPOSE:
///    Checks if all the required chip stack models for a game of roulette are loaded 
FUNC BOOL ROULETTE_PROPS_HAVE_CHIP_STACK_MODELS_LOADED()
	RETURN HAS_MODEL_LOADED(ROULETTE_PROPS_GET_CHIP_STACK_MODEL(TEN_DOLLAR_CHIP))		
	AND	HAS_MODEL_LOADED(ROULETTE_PROPS_GET_CHIP_STACK_MODEL(FIFTY_DOLLAR_CHIP))
	AND HAS_MODEL_LOADED(ROULETTE_PROPS_GET_CHIP_STACK_MODEL(ONE_HUNDRED_DOLLAR_CHIP))	
	AND	HAS_MODEL_LOADED(ROULETTE_PROPS_GET_CHIP_STACK_MODEL(FIVE_HUNDRED_DOLLAR_CHIP))	
	AND	HAS_MODEL_LOADED(ROULETTE_PROPS_GET_CHIP_STACK_MODEL(ONE_THOUSAND_DOLLAR_CHIP))
	AND HAS_MODEL_LOADED(ROULETTE_PROPS_GET_CHIP_STACK_MODEL(FIVE_THOUSAND_DOLLAR_CHIP))
	AND HAS_MODEL_LOADED(ROULETTE_PROPS_GET_CHIP_STACK_MODEL(TEN_THOUSAND_DOLLAR_CHIP))	
ENDFUNC

/// PURPOSE:
///    Releases all the chip stack models used in a game of roulette
PROC ROULETTE_PROPS_RELEASE_ALL_CHIP_STACKS()
	SET_MODEL_AS_NO_LONGER_NEEDED(ROULETTE_PROPS_GET_CHIP_STACK_MODEL(TEN_DOLLAR_CHIP))
	SET_MODEL_AS_NO_LONGER_NEEDED(ROULETTE_PROPS_GET_CHIP_STACK_MODEL(FIFTY_DOLLAR_CHIP))
	SET_MODEL_AS_NO_LONGER_NEEDED(ROULETTE_PROPS_GET_CHIP_STACK_MODEL(ONE_HUNDRED_DOLLAR_CHIP))
	SET_MODEL_AS_NO_LONGER_NEEDED(ROULETTE_PROPS_GET_CHIP_STACK_MODEL(FIVE_HUNDRED_DOLLAR_CHIP))
	SET_MODEL_AS_NO_LONGER_NEEDED(ROULETTE_PROPS_GET_CHIP_STACK_MODEL(ONE_THOUSAND_DOLLAR_CHIP))
	SET_MODEL_AS_NO_LONGER_NEEDED(ROULETTE_PROPS_GET_CHIP_STACK_MODEL(FIVE_THOUSAND_DOLLAR_CHIP))
	SET_MODEL_AS_NO_LONGER_NEEDED(ROULETTE_PROPS_GET_CHIP_STACK_MODEL(TEN_THOUSAND_DOLLAR_CHIP))
ENDPROC

/// PURPOSE:
///    Requests all the single chip models that are used for the cursor
///    in a game of roulette
PROC ROULETTE_PROPS_REQUEST_ALL_CURSORS()
	REQUEST_MODEL(ROULETTE_PROPS_GET_CURSOR_MODEL(TEN_DOLLAR_CHIP))
	REQUEST_MODEL(ROULETTE_PROPS_GET_CURSOR_MODEL(FIFTY_DOLLAR_CHIP))
	REQUEST_MODEL(ROULETTE_PROPS_GET_CURSOR_MODEL(ONE_HUNDRED_DOLLAR_CHIP))
	REQUEST_MODEL(ROULETTE_PROPS_GET_CURSOR_MODEL(FIVE_HUNDRED_DOLLAR_CHIP))
	REQUEST_MODEL(ROULETTE_PROPS_GET_CURSOR_MODEL(ONE_THOUSAND_DOLLAR_CHIP))
	REQUEST_MODEL(ROULETTE_PROPS_GET_CURSOR_MODEL(FIVE_THOUSAND_DOLLAR_CHIP))
	REQUEST_MODEL(ROULETTE_PROPS_GET_CURSOR_MODEL(TEN_THOUSAND_DOLLAR_CHIP))
ENDPROC

/// PURPOSE:
///    Checks all the single chip models that are used for the cursor
///    in a game of roulette are loaded
FUNC BOOL ROULETTE_PROPS_HAVE_CURSOR_MODELS_LOADED()
	RETURN HAS_MODEL_LOADED(ROULETTE_PROPS_GET_CURSOR_MODEL(TEN_DOLLAR_CHIP)) 	
	AND HAS_MODEL_LOADED(ROULETTE_PROPS_GET_CURSOR_MODEL(FIFTY_DOLLAR_CHIP))
	AND HAS_MODEL_LOADED(ROULETTE_PROPS_GET_CURSOR_MODEL(ONE_HUNDRED_DOLLAR_CHIP))
	AND	HAS_MODEL_LOADED(ROULETTE_PROPS_GET_CURSOR_MODEL(FIVE_HUNDRED_DOLLAR_CHIP))
	AND HAS_MODEL_LOADED(ROULETTE_PROPS_GET_CURSOR_MODEL(ONE_THOUSAND_DOLLAR_CHIP))
	AND HAS_MODEL_LOADED(ROULETTE_PROPS_GET_CURSOR_MODEL(FIVE_THOUSAND_DOLLAR_CHIP))	
	AND HAS_MODEL_LOADED(ROULETTE_PROPS_GET_CURSOR_MODEL(TEN_THOUSAND_DOLLAR_CHIP))
ENDFUNC

/// PURPOSE:
///    Releases all the single chip models that are used for the cursor
///    in a game of roulette
PROC ROULETTE_PROPS_RELEASE_ALL_CURSORS()
	SET_MODEL_AS_NO_LONGER_NEEDED(ROULETTE_PROPS_GET_CURSOR_MODEL(TEN_DOLLAR_CHIP))
	SET_MODEL_AS_NO_LONGER_NEEDED(ROULETTE_PROPS_GET_CURSOR_MODEL(FIFTY_DOLLAR_CHIP))
	SET_MODEL_AS_NO_LONGER_NEEDED(ROULETTE_PROPS_GET_CURSOR_MODEL(ONE_HUNDRED_DOLLAR_CHIP))
	SET_MODEL_AS_NO_LONGER_NEEDED(ROULETTE_PROPS_GET_CURSOR_MODEL(FIVE_HUNDRED_DOLLAR_CHIP))
	SET_MODEL_AS_NO_LONGER_NEEDED(ROULETTE_PROPS_GET_CURSOR_MODEL(ONE_THOUSAND_DOLLAR_CHIP))
	SET_MODEL_AS_NO_LONGER_NEEDED(ROULETTE_PROPS_GET_CURSOR_MODEL(FIVE_THOUSAND_DOLLAR_CHIP))
	SET_MODEL_AS_NO_LONGER_NEEDED(ROULETTE_PROPS_GET_CURSOR_MODEL(TEN_THOUSAND_DOLLAR_CHIP))
ENDPROC

/// PURPOSE:
///    Loads the models required for running a roulette game
PROC ROULETTE_PROPS_LOAD_REQUIRED_MODELS()
	ROULETTE_PROPS_REQUEST_ALL_CURSORS()
	ROULETTE_PROPS_REQUEST_ALL_CHIP_STACKS()
	REQUEST_MODEL(ROULETTE_PROPS_GET_HIGHLIGHT_MODEL())
	REQUEST_MODEL(ROULETTE_PROPS_GET_HIGHLIGHT_MODEL(TRUE))
	REQUEST_MODEL(ROULETTE_PROPS_GET_BALL_MODEL())
ENDPROC

/// PURPOSE:
///    Deletes all the roulette models that are placed on the table
PROC ROULETTE_PROPS_CLEAN_UP_GAMEPLAY_PROPS(ROULETTE_DATA &sRouletteData)
	IF DOES_ENTITY_EXIST(sRouletteData.objCursor)
		DELETE_OBJECT(sRouletteData.objCursor)
		sRouletteData.objCursor = NULL
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_PROPS_CLEAN_UP_GAMEPLAY_PROPS - deleted chip cursor")
	ENDIF
	ROULETTE_PROPS_DELETE_ALL_CHIPS(sRouletteData)
	ROULETTE_PROPS_DELETE_ALL_HIGHLIGHTS(sRouletteData)
ENDPROC

/// PURPOSE:
///    Cleans up all table models and the balls they use
PROC ROULETTE_PROPS_CLEAN_UP_TABLES(ROULETTE_DATA &sRouletteData)
	INT iTableID = 0
	REPEAT ROULETTE_TABLE_MAX_NUMBER_OF_TABLES iTableID	
		IF DOES_ENTITY_EXIST(sRouletteData.sSpin[iTableID].objBall)
			DELETE_OBJECT(sRouletteData.sSpin[iTableID].objBall)
			sRouletteData.sSpin[iTableID].objBall = NULL
			CPRINTLN(DEBUG_ROULETTE, "ROULETTE_PROPS_CLEAN_UP_TABLES - ball ", iTableID, " deleted")
		ENDIF	

		IF DOES_ENTITY_EXIST(sRouletteData.sSpin[iTableID].objWheel)
			DELETE_OBJECT(sRouletteData.sSpin[iTableID].objWheel)
			sRouletteData.sSpin[iTableID].objWheel = NULL
			CPRINTLN(DEBUG_ROULETTE, "ROULETTE_PROPS_CLEAN_UP_TABLES - table ", iTableID, " deleted")
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Releases all the models used by a game of roulette
PROC ROULETTE_PROPS_RELEASE_ALL_MODELS()
	ROULETTE_PROPS_RELEASE_ALL_CURSORS()
	ROULETTE_PROPS_RELEASE_ALL_CHIP_STACKS()
	SET_MODEL_AS_NO_LONGER_NEEDED(ROULETTE_PROPS_GET_HIGHLIGHT_MODEL())
	SET_MODEL_AS_NO_LONGER_NEEDED(ROULETTE_PROPS_GET_HIGHLIGHT_MODEL(TRUE))
	SET_MODEL_AS_NO_LONGER_NEEDED(ROULETTE_PROPS_GET_BALL_MODEL())
ENDPROC

/// PURPOSE:
///    Cleans up the dealer peds used by roulette
PROC ROULETTE_PROPS_CLEAN_UP_DEALER(NETWORK_INDEX& niPed)	
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niPed)
		EXIT
	ENDIF
	
	IF NOT NETWORK_HAS_CONTROL_OF_NETWORK_ID(niPed)
		EXIT
	ENDIF
	
	PED_INDEX pedID = NET_TO_PED(niPed)
	DELETE_PED(pedID)
	DELETE_NET_ID(niPed)
	niPed = NULL			
ENDPROC

/// PURPOSE:
///    Cleans up any models used for a roulette game
PROC ROULETTE_PROPS_CLEAN_UP(ROULETTE_DATA &sRouletteData)
	ROULETTE_PROPS_RELEASE_ALL_MODELS()	
	ROULETTE_PROPS_CLEAN_UP_TABLES(sRouletteData)
	ROULETTE_PROPS_CLEAN_UP_GAMEPLAY_PROPS(sRouletteData)
	ROULETTE_PROPS_CLEAN_UP_TABLES(sRouletteData)
ENDPROC

/// PURPOSE:
///    Checks if the models required for running a roulette game have been loaded
FUNC BOOL ROULETTE_PROPS_HAVE_REQUIRED_MODELS_LOADED()
	RETURN ROULETTE_PROPS_HAVE_CURSOR_MODELS_LOADED() 
	AND ROULETTE_PROPS_HAVE_CHIP_STACK_MODELS_LOADED()
	AND HAS_MODEL_LOADED(ROULETTE_PROPS_GET_HIGHLIGHT_MODEL())
	AND HAS_MODEL_LOADED(ROULETTE_PROPS_GET_HIGHLIGHT_MODEL(TRUE))
	AND HAS_MODEL_LOADED(ROULETTE_PROPS_GET_BALL_MODEL())
ENDFUNC
