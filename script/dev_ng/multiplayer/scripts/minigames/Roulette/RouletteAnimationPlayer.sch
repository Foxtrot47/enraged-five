//////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Name:        RouletteAnimationPlayer.sch																		
/// Description: Header for controlling animations for the roulette minigames players								
///																											
/// Written by:  Online Technical Team: Tom Turner,															
/// Date:  		07/03/2019 (refactored into own file 15/05/2019)																					
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
   
USING "RouletteData.sch"
USING "RouletteHelper.sch"
USING "rc_helper_functions.sch"
USING "RouletteTable.sch"
USING "RouletteLayout.sch"
USING "RouletteBetting.sch"
USING "RouletteProps.sch"

#IF NOT DEFINED(ROULETTE_ANIM_MAX_LOSS_STREAK_BEFORE_TERRIBLE_REACTION_CAN_TRIGGER)
	/// PURPOSE: 
	///     How many losses in a row before we factor it in to the terrible reaction triggering
	TWEAK_INT ROULETTE_ANIM_MAX_LOSS_STREAK_BEFORE_TERRIBLE_REACTION_CAN_TRIGGER 4
#ENDIF

/// PURPOSE:
///    Loads the dictionaries containg the roulette players animations
PROC ROULETTE_ANIM_LOAD_PLAYER_DICTIONARIES()
	REQUEST_ANIM_DICT(ROULETTE_ANIM_GET_DICTIONARY(ROULETTE_ANIM_DICT_PLAYER))
	REQUEST_ANIM_DICT(ROULETTE_ANIM_GET_DICTIONARY(ROULETTE_ANIM_DICT_PLAYER_SHARED))
ENDPROC

/// PURPOSE:
///   	Checks if the dictionaries containg the roulette player animations has loaded   
FUNC BOOL ROULETTE_ANIM_HAVE_PLAYER_DICTIONARIES_LOADED()
	RETURN HAS_ANIM_DICT_LOADED(ROULETTE_ANIM_GET_DICTIONARY(ROULETTE_ANIM_DICT_PLAYER))
	AND HAS_ANIM_DICT_LOADED(ROULETTE_ANIM_GET_DICTIONARY(ROULETTE_ANIM_DICT_PLAYER_SHARED)) 
ENDFUNC

/// PURPOSE:
///    Removes the dictionaries containing the player animations
PROC ROULETTE_ANIM_REMOVE_PLAYER_DICTIONARIES()
	REMOVE_ANIM_DICT(ROULETTE_ANIM_GET_DICTIONARY(ROULETTE_ANIM_DICT_PLAYER))
	REMOVE_ANIM_DICT(ROULETTE_ANIM_GET_DICTIONARY(ROULETTE_ANIM_DICT_PLAYER_SHARED))
ENDPROC


///----------------------------
///    Anim scene positions
///---------------------------- 

/// PURPOSE:
///    Gets the starting position for the enter seat animation  
FUNC VECTOR ROULETTE_ANIM_GET_SHARED_ANIM_START_POSITION(ROULETTE_DATA &sRouletteData, INT iSeatID, ROULETTE_ANIM_CLIPS eAnim, INT iVariation = 0)	
	VECTOR vPos = ROULETTE_PROPS_GET_SEAT_BONE_POSITION(sRouletteData, iSeatID)
	VECTOR vRot = ROULETTE_PROPS_GET_SEAT_BONE_ROTATION(sRouletteData, iSeatID)
	
	INT iLocalSeatID = ROULETTE_HELPER_GET_LOCAL_SEAT_ID_FROM_SEAT_ID(iSeatID)
	STRING strAnimDict = ROULETTE_ANIM_GET_DICTIONARY(ROULETTE_ANIM_DICT_PLAYER_SHARED)
	STRING strAnimClip = ROULETTE_ANIM_GET_PLAYER_CLIP(eAnim, iVariation, iLocalSeatID)
	
	RETURN GET_ANIM_INITIAL_OFFSET_POSITION(strAnimDict, strAnimClip, vPos, vRot, 0.01)
ENDFUNC

/// PURPOSE:
///    Gets the starting position for the enter seat animation  
FUNC VECTOR ROULETTE_ANIM_GET_SHARED_ANIM_START_ROTATION(ROULETTE_DATA &sRouletteData, INT iSeatID, ROULETTE_ANIM_CLIPS eAnim, INT iVariation = 0)	
	INT iTableID = ROULETTE_HELPER_GET_TABLE_ID_FROM_SEAT_ID(iSeatID)
	
	OBJECT_INDEX objTable = ROULETTE_HELPER_GET_TABLE_OBJECT_FOR_ID(sRouletteData, iTableID)	
	IF NOT DOES_ENTITY_EXIST(objTable)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_GET_SEAT_ANIM_START - Table entity doesn't exist")
		RETURN <<0, 0, 0>>
	ENDIF
	
	INT iLocalSeatID = ROULETTE_HELPER_GET_LOCAL_SEAT_ID_FROM_SEAT_ID(iSeatID)
	STRING strBoneName = ROULETTE_ANIM_GET_SEAT_BONE_NAME(iLocalSeatID)
	VECTOR vPos = ROULETTE_PROPS_GET_ENTITY_BONE_POSITION_SAFELY(objTable, strBoneName)
	VECTOR vRot = ROULETTE_PROPS_GET_ENTITY_BONE_ROTATION_SAFELY(objTable, strBoneName)
	
	STRING strAnimDict = ROULETTE_ANIM_GET_DICTIONARY(ROULETTE_ANIM_DICT_PLAYER_SHARED)
	STRING strAnimClip = ROULETTE_ANIM_GET_PLAYER_CLIP(eAnim, iVariation, iLocalSeatID)
	
	RETURN GET_ANIM_INITIAL_OFFSET_ROTATION(strAnimDict, strAnimClip, vPos, vRot, 0.01)
ENDFUNC


///--------------------------------
///    Anim clip checking - Player
///--------------------------------

FUNC BOOL ROULETTE_ANIM_HAS_SHARED_PLAYER_EVENT_TRIGGERED(ROULETTE_DATA &sRouletteData, INT iEventHash)
	IF NOT IS_ENTITY_ALIVE(PLAYER_PED_ID())
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_HAS_SHARED_PLAYER_EVENT_TRIGGERED - Player is not alive")
		RETURN FALSE
	ENDIF

	STRING strAnimDict = ROULETTE_ANIM_GET_DICTIONARY(ROULETTE_ANIM_DICT_PLAYER_SHARED)
	STRING strAnimClip = ROULETTE_ANIM_GET_PLAYER_CLIP(sRouletteData.sLastPlayerAnimInfo.eClip, sRouletteData.sLastPlayerAnimInfo.iVariation)
	
	IF NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), strAnimDict, strAnimClip)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_HAS_SHARED_PLAYER_EVENT_TRIGGERED - Player is not playing anim")
		RETURN FALSE
	ENDIF
	
	IF HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(), iEventHash)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_HAS_SHARED_PLAYER_EVENT_TRIGGERED - Anim event fired")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if the given animation clip has finished playing on the player  
FUNC BOOL ROULETTE_ANIM_HAS_PLAYER_FINISHED_PLAYING_CLIP(ROULETTE_DATA &sRouletteData, ROULETTE_ANIM_CLIPS ePlayerClip,
ROULETTE_ANIM_DICTIONARIES eDictionary, INT iLocalSeatID, INT iVariation = 0, FLOAT iPhaseEndPoint = 0.9)	
	
	PED_INDEX piPlayer = PLAYER_PED_ID()
	
	IF NOT IS_ENTITY_ALIVE(piPlayer)
		RETURN FALSE
	ENDIF
	
	STRING strAnimDict = ROULETTE_ANIM_GET_DICTIONARY(eDictionary)
	STRING strAnimClip = ROULETTE_ANIM_GET_PLAYER_CLIP(ePlayerClip, iVariation, iLocalSeatID)
	
	IF NOT IS_ENTITY_PLAYING_ANIM(piPlayer, strAnimDict, strAnimClip)
		RETURN FALSE
	ENDIF
	
	IF sRouletteData.iPlayerSyncSceneID = -1
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_HAS_PLAYER_FINISHED_PLAYING_CLIP - Invalid network sync scene ID")
		RETURN FALSE
	ENDIF
	
	sRouletteData.iPlayerLocalSyncSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sRouletteData.iPlayerSyncSceneID)
	RETURN GET_SYNCHRONIZED_SCENE_PHASE(sRouletteData.iPlayerLocalSyncSceneID) >= iPhaseEndPoint
	OR HAS_ANIM_EVENT_FIRED(piPlayer, GET_HASH_KEY("BLEND_OUT")) 
ENDFUNC

/// PURPOSE:
///    Checks if the local player is playing a specific animation clip   
FUNC BOOL ROULETTE_ANIM_IS_LOCAL_PLAYER_PLAYING_CLIP(ROULETTE_ANIM_CLIPS ePlayerClip, ROULETTE_ANIM_DICTIONARIES eDictionary, INT iLocalSeatID, INT iVariation = 0)
	
	IF NOT IS_ENTITY_ALIVE(PLAYER_PED_ID())
		RETURN FALSE
	ENDIF
	
	STRING strAnimDict = ROULETTE_ANIM_GET_DICTIONARY(eDictionary)
	STRING strAnimClip = ROULETTE_ANIM_GET_PLAYER_CLIP(ePlayerClip, iVariation, iLocalSeatID)
	RETURN IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), strAnimDict, strAnimClip)
ENDFUNC

/// PURPOSE:
///    Checks if the current animation being played on the player has finished playing 
FUNC BOOL ROULETTE_ANIM_HAS_CURRENT_PLAYER_ANIM_FINISHED(ROULETTE_DATA &sRouletteData, FLOAT iEndPhase = 0.9)	
	PED_INDEX piPlayer = PLAYER_PED_ID()
	
	IF NOT IS_ENTITY_ALIVE(piPlayer)
		RETURN FALSE
	ENDIF
	
	STRING strAnimDict = ROULETTE_ANIM_GET_DICTIONARY(sRouletteData.sLastPlayerAnimInfo.eDictionary)
	STRING strAnimClip
	strAnimClip = ROULETTE_ANIM_GET_PLAYER_CLIP(sRouletteData.sLastPlayerAnimInfo.eClip,
	sRouletteData.sLastPlayerAnimInfo.iVariation,
	ROULETTE_HELPER_GET_LOCAL_PLAYERS_LOCAL_SEAT_ID(sRouletteData))
	
	IF NOT IS_ENTITY_PLAYING_ANIM(piPlayer, strAnimDict, strAnimClip)
		RETURN FALSE
	ENDIF
	
	IF HAS_ANIM_EVENT_FIRED(piPlayer, GET_HASH_KEY("BLEND_OUT"))
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_HAS_CURRENT_PLAYER_ANIM_FINISHED - Blend Out event triggered")
		RETURN TRUE
	ENDIF
	
	IF sRouletteData.iPlayerSyncSceneID = -1
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_HAS_CURRENT_PLAYER_ANIM_FINISHED - Invalid network sync scene ID")
		RETURN FALSE
	ENDIF
	
	sRouletteData.iPlayerLocalSyncSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sRouletteData.iPlayerSyncSceneID)
	RETURN GET_SYNCHRONIZED_SCENE_PHASE(sRouletteData.iPlayerLocalSyncSceneID) >= iEndPhase
ENDFUNC

/// PURPOSE:
///    Checks if the player has finished playing any of the enter seat animations   
FUNC BOOL ROULETTE_ANIM_HAS_PLAYER_FINISHED_SEAT_ENTER_ANIM()
	RETURN NOT ROULETTE_ANIM_IS_LOCAL_PLAYER_PLAYING_CLIP(ROULETTE_ANIM_CLIP_PLAYER_SEAT_ENTER_LEFT, ROULETTE_ANIM_DICT_PLAYER_SHARED, 0) 
	AND NOT ROULETTE_ANIM_IS_LOCAL_PLAYER_PLAYING_CLIP(ROULETTE_ANIM_CLIP_PLAYER_SEAT_ENTER_RIGHT, ROULETTE_ANIM_DICT_PLAYER_SHARED, 0) 
	AND NOT ROULETTE_ANIM_IS_LOCAL_PLAYER_PLAYING_CLIP(ROULETTE_ANIM_CLIP_PLAYER_SEAT_ENTER_BACK, ROULETTE_ANIM_DICT_PLAYER_SHARED, 0)
ENDFUNC

/// PURPOSE:
///    Checks if the player is playing a ready up loop
FUNC BOOL ROULETTE_ANIM_IS_PLAYER_PLAYING_READY_UP_LOOP_ANIM(ROULETTE_DATA &sRouletteData)
	INT iLocalSeatID = ROULETTE_HELPER_GET_LOCAL_PLAYERS_LOCAL_SEAT_ID(sRouletteData)
	RETURN ROULETTE_ANIM_IS_LOCAL_PLAYER_PLAYING_CLIP(ROULETTE_ANIM_CLIP_PLAYER_READY_UP_LOOP_HIGH_BET, ROULETTE_ANIM_DICT_PLAYER, iLocalSeatID)
	OR ROULETTE_ANIM_IS_LOCAL_PLAYER_PLAYING_CLIP(ROULETTE_ANIM_CLIP_PLAYER_READY_UP_LOOP_MID_BET, ROULETTE_ANIM_DICT_PLAYER, iLocalSeatID) 
	OR ROULETTE_ANIM_IS_LOCAL_PLAYER_PLAYING_CLIP(ROULETTE_ANIM_CLIP_PLAYER_READY_UP_LOOP_NO_BET, ROULETTE_ANIM_DICT_PLAYER, iLocalSeatID) 
ENDFUNC


///------------------------------
///    Anim clip playing helpers
///------------------------------    

/// PURPOSE:
///    Stops playing the current animaion on the roulette player
PROC ROULETTE_ANIM_STOP_PLAYER_SYNC_SCENE(ROULETTE_DATA &sRouletteData)
	IF sRouletteData.iPlayerSyncSceneID = -1
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_STOP_PLAYERS_CURRENT_ANIM - Invalid network sync scene ID")
		EXIT
	ENDIF
	
	NETWORK_STOP_SYNCHRONISED_SCENE(sRouletteData.iPlayerSyncSceneID)
	sRouletteData.iPlayerSyncSceneID = -1
	sRouletteData.iPlayerLocalSyncSceneID = -1
ENDPROC

/// PURPOSE:
///    Plays a roulette animation clip on the local player  
PROC ROULETTE_ANIM_PLAY_CLIP_ON_LOCAL_PLAYER(ROULETTE_DATA &sRouletteData, ROULETTE_ANIM &sAnim)				
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_PLAY_CLIP_ON_LOCAL_PLAYER")
	IF NOT HAS_ANIM_DICT_LOADED(ROULETTE_ANIM_GET_DICTIONARY(sAnim.eDictionary))
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_PLAY_CLIP_ON_LOCAL_PLAYER - Dictionary not loaded")
		EXIT
	ENDIF
	
	IF NOT IS_ENTITY_ALIVE(PLAYER_PED_ID())
		EXIT
	ENDIF	
	
	IF NOT DOES_ENTITY_HAVE_DRAWABLE(PLAYER_PED_ID())
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_PLAY_CLIP_ON_LOCAL_PLAYER - Player does not have drawable")
		EXIT
	ENDIF
	
	OBJECT_INDEX objTable = ROULETTE_HELPER_GET_TABLE_OBJECT_FOR_ID(sRouletteData, sRouletteData.iTableID)	
	IF NOT DOES_ENTITY_EXIST(objTable)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_PLAY_CLIP_ON_LOCAL_PLAYER - Table entity doesn't exist")
		EXIT
	ENDIF
	
	INT iLocalSeatID = ROULETTE_HELPER_GET_LOCAL_PLAYERS_LOCAL_SEAT_ID(sRouletteData)
	STRING strBoneName = ROULETTE_ANIM_GET_SEAT_BONE_NAME(iLocalSeatID)
	
	VECTOR vSeatPos = ROULETTE_PROPS_GET_ENTITY_BONE_POSITION_SAFELY(objTable, strBoneName)
	VECTOR vSeatRot = ROULETTE_PROPS_GET_ENTITY_BONE_ROTATION_SAFELY(objTable, strBoneName)		
	
	STRING strAnimDict = ROULETTE_ANIM_GET_DICTIONARY(sAnim.eDictionary)
	STRING strAnimClip = ROULETTE_ANIM_GET_PLAYER_CLIP(sAnim.eClip, sAnim.iVariation, iLocalSeatID)
	
	IF IS_STRING_NULL_OR_EMPTY(strAnimDict)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_PLAY_CLIP_ON_LOCAL_PLAYER - Missing dictionary!: ", ROULETTE_ANIM_CLIP_GET_NAME_OF_CLIP_AS_STRING(sAnim.eClip),
		" ", ROULETTE_ANIM_CLIP_GET_NAME_OF_DICT_AS_STRING(sAnim.eDictionary))	
		EXIT
	ENDIF
	
	IF IS_STRING_NULL_OR_EMPTY(strAnimClip)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_PLAY_CLIP_ON_LOCAL_PLAYER - Missing clip!: ", ROULETTE_ANIM_CLIP_GET_NAME_OF_CLIP_AS_STRING(sAnim.eClip),
		" ", ROULETTE_ANIM_CLIP_GET_NAME_OF_DICT_AS_STRING(sAnim.eDictionary))
		EXIT
	ENDIF
	
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_PLAY_CLIP_ON_LOCAL_PLAYER - playing: ", strAnimClip, " ", strAnimDict)	
	
	sRouletteData.iPlayerSyncSceneID = NETWORK_CREATE_SYNCHRONISED_SCENE(vSeatPos, vSeatRot, DEFAULT, sAnim.bHoldLastFrame, sAnim.bLoop, sAnim.fEndPhase, sAnim.fStartPhase)
	
	NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), sRouletteData.iPlayerSyncSceneID, strAnimDict, strAnimClip,
	sAnim.fBlendIn, sAnim.fBlendOut, sAnim.eSyncSceneFlags, RBF_PLAYER_IMPACT, sAnim.fBlendIn)
	
	NETWORK_START_SYNCHRONISED_SCENE(sRouletteData.iPlayerSyncSceneID)
	sRouletteData.sLastPlayerAnimInfo = sAnim
ENDPROC


///--------------------------------
///    Anim clip selecting
///-------------------------------- 

/// PURPOSE:
///    Gets the appropriate animation clip for entering the current assigned roulette seat, 
///    taking into account blocked sides and closest side
FUNC ROULETTE_ANIM_CLIPS ROULETTE_ANIM_GET_APPROPRIATE_SEAT_ENTRY_CLIP(ROULETTE_DATA &sRouletteData, INT iSeatID)	
	FLOAT fEnterBack = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), ROULETTE_ANIM_GET_SHARED_ANIM_START_POSITION(sRouletteData, iSeatID, ROULETTE_ANIM_CLIP_PLAYER_SEAT_ENTER_BACK))
	FLOAT fEnterLeft = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), ROULETTE_ANIM_GET_SHARED_ANIM_START_POSITION(sRouletteData, iSeatID, ROULETTE_ANIM_CLIP_PLAYER_SEAT_ENTER_LEFT))
	FLOAT fEnterRight = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), ROULETTE_ANIM_GET_SHARED_ANIM_START_POSITION(sRouletteData, iSeatID, ROULETTE_ANIM_CLIP_PLAYER_SEAT_ENTER_RIGHT))	
	
	INT iLocalSeatID = ROULETTE_HELPER_GET_LOCAL_SEAT_ID_FROM_SEAT_ID(iSeatID)
	
	IF fEnterLeft < fEnterRight
	AND fEnterLeft < fEnterBack
		IF ROULETTE_TABLE_IS_LOCAL_SEAT_BLOCKED_ON_SIDE(iLocalSeatID, FALSE)
			RETURN ROULETTE_ANIM_CLIP_PLAYER_SEAT_ENTER_BACK
		ENDIF
	
		RETURN ROULETTE_ANIM_CLIP_PLAYER_SEAT_ENTER_LEFT
	ENDIF
	
	IF fEnterRight < fEnterLeft
	AND fEnterRight < fEnterBack	
		IF ROULETTE_TABLE_IS_LOCAL_SEAT_BLOCKED_ON_SIDE(iLocalSeatID, TRUE)
			RETURN ROULETTE_ANIM_CLIP_PLAYER_SEAT_ENTER_BACK
		ENDIF
	
		RETURN ROULETTE_ANIM_CLIP_PLAYER_SEAT_ENTER_RIGHT
	ENDIF
	
	RETURN ROULETTE_ANIM_CLIP_PLAYER_SEAT_ENTER_BACK
ENDFUNC

/// PURPOSE:
///    Gets the appropriate animation clip for exiting the current assigned roulette seat,
///    based on which side the seat was originally entered on    
FUNC ROULETTE_ANIM_CLIPS ROULETTE_ANIM_GET_APPROPRIATE_SEAT_EXIT_CLIP(ROULETTE_DATA &sRouletteData)
	IF sRouletteData.eSeatEnterClipToUse = ROULETTE_ANIM_CLIP_PLAYER_SEAT_ENTER_RIGHT
		RETURN ROULETTE_ANIM_CLIP_PLAYER_EXIT_RIGHT
	ENDIF
	
	RETURN ROULETTE_ANIM_CLIP_PLAYER_EXIT_LEFT
ENDFUNC

/// PURPOSE:
///    Checks if a terrible reaction should be played if the player lost on a straight bet
///    that they placed the max bet amount on
FUNC BOOL ROULETTE_ANIM_SHOULD_TERRIBLE_REACTION_TRIGGER_FOR_STRAIGHT_MAX_BET_LOSS(ROULETTE_DATA &sRouletteData)	
	// Can only ever trigger if we missed a straight max bet
	IF NOT IS_BIT_SET(sRouletteData.iRouletteBS, ROULETTE_BS_PAYOUT_LOST_MAX_BET_ON_A_STRAIGHT)
		RETURN FALSE
	ENDIF
	
	// Missed max straght bet and was one slot away from hitting it
	IF IS_BIT_SET(sRouletteData.iRouletteBS, ROUELTTE_BS_PAYOUT_LOST_ADJACENT_BET_ON_A_STRAIGHT)
		RETURN TRUE
	ENDIF
	
	// Missed max straght bet and it was the players last chips
	IF ROULETTE_BETTING_GET_LOCAL_PLAYER_CHIP_COUNT() < ROULETTE_BETTING_GET_MIN_BET_FOR_TABLE(sRouletteData.iTableID)
		RETURN TRUE
	ENDIF
	
	// Missed max straight bet and is on a losing streak
	RETURN sRouletteData.iLoseStreak >= ROULETTE_ANIM_MAX_LOSS_STREAK_BEFORE_TERRIBLE_REACTION_CAN_TRIGGER
ENDFUNC

/// PURPOSE:
///    Gets an appropriate reaction for coming out of the high ready up loop
FUNC ROULETTE_ANIM_CLIPS ROULETTE_ANIM_GET_END_REACTION_FOR_A_HIGH_READY_UP(ROULETTE_DATA &sRouletteData)
	// Great reaction if player hit a max bet on a straight
	IF IS_BIT_SET(sRouletteData.iRouletteBS, ROULETTE_BS_PAYOUT_WON_MAX_BET_ON_A_STRAIGHT)
		RETURN ROULETTE_ANIM_CLIP_PLAYER_REACTION_GREAT
	ENDIF
	
	// Terrible reaction
	IF ROULETTE_ANIM_SHOULD_TERRIBLE_REACTION_TRIGGER_FOR_STRAIGHT_MAX_BET_LOSS(sRouletteData)
		RETURN ROULETTE_ANIM_CLIP_PLAYER_REACTION_TERRIBLE
	ENDIF
	
	RETURN ROULETTE_ANIM_CLIP_PLAYER_REACTION_DEFLATED
ENDFUNC

/// PURPOSE:
///    Gets an apropriate reaction for coming out of the mid ready up loop
FUNC ROULETTE_ANIM_CLIPS ROULETTE_ANIM_GET_END_REACTION_FOR_A_MID_READY_UP(ROULETTE_DATA &sRouletteData)	
	// Good reaction if got any payout
	IF sRouletteData.iRoundPayout > 0
		RETURN ROULETTE_ANIM_CLIP_PLAYER_REACTION_GOOD
	ENDIF
	
	// Payout must be less than or equal to 0 but not enough to trigger terrible reaction
	RETURN ROULETTE_ANIM_CLIP_PLAYER_REACTION_BAD
ENDFUNC

/// PURPOSE:
///    Gets an appropriate reaction animation for the current round of roulette
FUNC ROULETTE_ANIM_CLIPS ROULETTE_ANIM_GET_APPROPRIATE_REACTION_CLIP(ROULETTE_DATA &sRouletteData)
	
	SWITCH sRouletteData.eLastReadyUpClip
		CASE ROULETTE_ANIM_CLIP_PLAYER_READY_UP_INTRO_HIGH_BET RETURN ROULETTE_ANIM_GET_END_REACTION_FOR_A_HIGH_READY_UP(sRouletteData)	
		CASE ROULETTE_ANIM_CLIP_PLAYER_READY_UP_INTRO_MID_BET RETURN ROULETTE_ANIM_GET_END_REACTION_FOR_A_MID_READY_UP(sRouletteData)	
		CASE ROULETTE_ANIM_CLIP_PLAYER_READY_UP_INTRO_NO_BET RETURN ROULETTE_ANIM_CLIP_PLAYER_REACTION_IMPARTIAL
	ENDSWITCH
	
	RETURN ROULETTE_ANIM_CLIP_PLAYER_REACTION_IMPARTIAL
ENDFUNC

/// PURPOSE:
///    Returns the number of defined variations for the given reaction clip  
FUNC INT ROULETTE_ANIM_GET_NUMBER_OF_VARIATIONS_FOR_REACTION_CLIP(ROULETTE_ANIM_CLIPS eReactionClip)
	SWITCH eReactionClip
		CASE ROULETTE_ANIM_CLIP_PLAYER_REACTION_GREAT 		RETURN ROULETTE_ANIM_NUMBER_OF_GREAT_REACTION_VARIATIONS
		CASE ROULETTE_ANIM_CLIP_PLAYER_REACTION_GOOD 		RETURN ROULETTE_ANIM_NUMBER_OF_GOOD_REACTION_VARIATIONS
		CASE ROULETTE_ANIM_CLIP_PLAYER_REACTION_IMPARTIAL 	RETURN ROULETTE_ANIM_NUMBER_OF_IMPARTIAL_REACTION_VARIATIONS
		CASE ROULETTE_ANIM_CLIP_PLAYER_REACTION_BAD 		RETURN ROULETTE_ANIM_NUMBER_OF_BAD_REACTION_VARIATIONS
		CASE ROULETTE_ANIM_CLIP_PLAYER_REACTION_TERRIBLE 	RETURN ROULETTE_ANIM_NUMBER_OF_TERRIBLE_REACTION_VARIATIONS
		CASE ROULETTE_ANIM_CLIP_PLAYER_REACTION_DEFLATED	RETURN ROULETTE_ANIM_NUMBER_OF_DEFLATED_REACTION_VARIATIONS
	ENDSWITCH
	RETURN 0
ENDFUNC

/// PURPOSE:
///    Enusures the given reaction variation is different to the old and updates it accordingly
PROC ROULETTE_ANIM_ENSURE_REACTION_VARIATION_IS_DIFFERENT_THAN_PREVIOUS(INT &iNewVariation, INT &iOldVariation, INT iMaxVariation)
	IF iNewVariation != iOldVariation
		iOldVariation = iNewVariation
		EXIT
	ENDIF
	
	iNewVariation = WRAP_INDEX(iNewVariation + 1, iMaxVariation)
	iOldVariation = iNewVariation
ENDPROC

/// PURPOSE:
///    Gets a random reaction variation   
FUNC INT ROULETTE_ANIM_GET_RANDOM_REACTION_VARIATION(ROULETTE_DATA &sRouletteData, ROULETTE_ANIM_CLIPS eReactionClip)
	INT iMaxVariationForReaction = ROULETTE_ANIM_GET_NUMBER_OF_VARIATIONS_FOR_REACTION_CLIP(eReactionClip)
	INT iRandomVar = GET_RANDOM_INT_IN_RANGE(0, iMaxVariationForReaction)
	
	SWITCH eReactionClip
		CASE ROULETTE_ANIM_CLIP_PLAYER_REACTION_BAD
			ROULETTE_ANIM_ENSURE_REACTION_VARIATION_IS_DIFFERENT_THAN_PREVIOUS(iRandomVar, sRouletteData.iLastBadReactionVariation, iMaxVariationForReaction)
		BREAK
		CASE ROULETTE_ANIM_CLIP_PLAYER_REACTION_GOOD
			ROULETTE_ANIM_ENSURE_REACTION_VARIATION_IS_DIFFERENT_THAN_PREVIOUS(iRandomVar, sRouletteData.iLastGoodReactionVariation, iMaxVariationForReaction)
		BREAK		
		CASE ROULETTE_ANIM_CLIP_PLAYER_REACTION_IMPARTIAL
			ROULETTE_ANIM_ENSURE_REACTION_VARIATION_IS_DIFFERENT_THAN_PREVIOUS(iRandomVar, sRouletteData.iLastImpartialReactionVariation, iMaxVariationForReaction)
		BREAK	
		CASE ROULETTE_ANIM_CLIP_PLAYER_REACTION_DEFLATED
			ROULETTE_ANIM_ENSURE_REACTION_VARIATION_IS_DIFFERENT_THAN_PREVIOUS(iRandomVar, sRouletteData.iLastDeflatedReactionVariation, iMaxVariationForReaction)
		BREAK
		CASE ROULETTE_ANIM_CLIP_PLAYER_REACTION_GREAT
			ROULETTE_ANIM_ENSURE_REACTION_VARIATION_IS_DIFFERENT_THAN_PREVIOUS(iRandomVar, sRouletteData.iLastGreatReactionVariation, iMaxVariationForReaction)
		BREAK
		CASE ROULETTE_ANIM_CLIP_PLAYER_REACTION_TERRIBLE
			ROULETTE_ANIM_ENSURE_REACTION_VARIATION_IS_DIFFERENT_THAN_PREVIOUS(iRandomVar, sRouletteData.iLastTerribleReactionVariation, iMaxVariationForReaction)
		BREAK		
	ENDSWITCH
	
	RETURN iRandomVar
ENDFUNC

/// PURPOSE:
///    Gets an appropriate ready up intro clip depending on if the local player has bet or not this round 
FUNC ROULETTE_ANIM_CLIPS ROULETTE_ANIM_GET_APPRORIATE_READY_UP_INTRO_CLIP(ROULETTE_DATA &sRouletteData)	
	// Go through bets checking for ready up scenarios
	ROULETTE_BETTING_DETERMINE_BET_READY_UP_FLAGS(sRouletteData)
		
	// High bet intro as bet stakes are high
	IF IS_BIT_SET(sRouletteData.iRouletteBS, ROULETTE_BS_READY_PLACED_MAX_BET_ON_A_STRAIGHT)
		RETURN ROULETTE_ANIM_CLIP_PLAYER_READY_UP_INTRO_HIGH_BET
	ENDIF
	
	INT iTotalBetThisRound = ROULETTE_HELPER_GET_LOCAL_PLAYERS_TOTAL_BET_FOR_CURRENT_ROUND(sRouletteData)
	
	// Player has made a mid bet
	IF iTotalBetThisRound > 0
		RETURN ROULETTE_ANIM_CLIP_PLAYER_READY_UP_INTRO_MID_BET
	ENDIF
	
	// Player made no bet
	RETURN ROULETTE_ANIM_CLIP_PLAYER_READY_UP_INTRO_NO_BET
ENDFUNC

/// PURPOSE:
///    Gets an appropriate ready up loop clip depending on if the local player has bet or not this round 
FUNC ROULETTE_ANIM_CLIPS ROULETTE_ANIM_GET_APPRORIATE_READY_UP_LOOP_CLIP(ROULETTE_DATA &sRouletteData)
	SWITCH sRouletteData.eLastReadyUpClip
		CASE ROULETTE_ANIM_CLIP_PLAYER_READY_UP_INTRO_HIGH_BET	 	RETURN ROULETTE_ANIM_CLIP_PLAYER_READY_UP_LOOP_HIGH_BET
		CASE ROULETTE_ANIM_CLIP_PLAYER_READY_UP_INTRO_MID_BET 		RETURN ROULETTE_ANIM_CLIP_PLAYER_READY_UP_LOOP_MID_BET
		CASE ROULETTE_ANIM_CLIP_PLAYER_READY_UP_INTRO_NO_BET		RETURN ROULETTE_ANIM_CLIP_PLAYER_READY_UP_LOOP_NO_BET
	ENDSWITCH

	RETURN ROULETTE_ANIM_CLIP_PLAYER_READY_UP_LOOP_NO_BET
ENDFUNC


///--------------------------------
///    Anim clip info
///-------------------------------- 

/// PURPOSE:
///    Gets the 'seat idle' animation
PROC ROULETTE_ANIM_GET_PLAYER_RANDOM_IDLE_LOOP_ANIM(ROULETTE_ANIM &sAnim)
	sAnim.eClip = ROULETTE_ANIM_CLIP_PLAYER_IDLE_LOOP
	sAnim.eDictionary = ROULETTE_ANIM_DICT_PLAYER_SHARED
	sAnim.iVariation = GET_RANDOM_INT_IN_RANGE(0, ROULETTE_ANIM_NUMBER_OF_SEAT_IDLE_LOOP_VARIATIONS)
	sAnim.bLoop = TRUE// Loop in case of network delay playing next idle one shot
	sAnim.fBlendIn = WALK_BLEND_IN
	sAnim.fBlendOut = WALK_BLEND_OUT
	sAnim.eSyncSceneFlags = SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_LOOP_WITHIN_SCENE
ENDPROC

/// PURPOSE:
///    Gets the play anim info for the players 'seat idle' animation clip
PROC ROULETTE_ANIM_GET_PLAYER_RANDOM_SEAT_IDLE_ONE_SHOT_ANIM(ROULETTE_ANIM &sAnim)
	sAnim.eClip = ROULETTE_ANIM_CLIP_PLAYER_IDLE_ONE_SHOT
	sAnim.eDictionary = ROULETTE_ANIM_DICT_PLAYER_SHARED
	sAnim.iVariation = GET_RANDOM_INT_IN_RANGE(0,  ROULETTE_ANIM_NUMBER_OF_SEAT_IDLE_ONE_SHOT_VARIATIONS)
	sAnim.bHoldLastFrame = TRUE// Hold last frame in case of network delay playing next idle
	sAnim.fBlendIn = WALK_BLEND_IN
	sAnim.fBlendOut = WALK_BLEND_OUT
	sAnim.eSyncSceneFlags = SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS
ENDPROC

/// PURPOSE:
///    Gets the play anim info for the players 'seat enter' animation clip
PROC ROULETTE_ANIM_GET_PLAYER_SEAT_ENTER_CLIP_PLAY_INFO(ROULETTE_DATA &sRouletteData, ROULETTE_ANIM &sAnim)
	sAnim.eClip = sRouletteData.eSeatEnterClipToUse
	sAnim.eDictionary = ROULETTE_ANIM_DICT_PLAYER_SHARED
	sAnim.bHoldLastFrame = TRUE// Hold last frame in case of network delay
	sAnim.fBlendIn = REALLY_SLOW_BLEND_IN
	sAnim.fBlendOut = REALLY_SLOW_BLEND_OUT
	sAnim.eSyncSceneFlags = SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS
ENDPROC

/// PURPOSE:
///    Gets the play anim info for the players 'seat exit' animation clip
PROC ROULETTE_ANIM_GET_PLAYER_SEAT_EXIT_CLIP_PLAY_INFO(ROULETTE_DATA &sRouletteData, ROULETTE_ANIM &sAnim)
	sAnim.eSyncSceneFlags = SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS
	sAnim.eClip = ROULETTE_ANIM_GET_APPROPRIATE_SEAT_EXIT_CLIP(sRouletteData)
	sAnim.fBlendIn = REALLY_SLOW_BLEND_IN
	sAnim.fBlendOut = REALLY_SLOW_BLEND_OUT
	sAnim.eDictionary = ROULETTE_ANIM_DICT_PLAYER_SHARED
ENDPROC

/// PURPOSE:
///    Gets the play anim info for the players 'reaction' animation clip
PROC ROULETTE_ANIM_GET_PLAYER_SEAT_REACTION_CLIP_PLAY_INFO(ROULETTE_DATA &sRouletteData, ROULETTE_ANIM &sAnim)
	sAnim.eClip = ROULETTE_ANIM_GET_APPROPRIATE_REACTION_CLIP(sRouletteData)
	sAnim.eDictionary = ROULETTE_ANIM_DICT_PLAYER
	sAnim.iVariation = ROULETTE_ANIM_GET_RANDOM_REACTION_VARIATION(sRouletteData, sAnim.eClip)
	sAnim.bHoldLastFrame = TRUE// Hold last frame in case of network delay
	sAnim.fBlendIn = SLOW_BLEND_IN
	sAnim.fBlendOut = REALLY_SLOW_BLEND_OUT
	sAnim.eSyncSceneFlags = SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS
ENDPROC

/// PURPOSE:
///    Gets the play anim info for the players 'ready up intro' animation clip
PROC ROULETTE_ANIM_GET_PLAYER_SEAT_READY_UP_INTRO_CLIP_PLAY_INFO(ROULETTE_DATA &sRouletteData, ROULETTE_ANIM &sAnim)
	sAnim.eClip = ROULETTE_ANIM_GET_APPRORIATE_READY_UP_INTRO_CLIP(sRouletteData)
	sAnim.eDictionary = ROULETTE_ANIM_DICT_PLAYER
	sAnim.iVariation = 0
	sAnim.bHoldLastFrame = TRUE// Hold last frame in case of network delay
	sAnim.fBlendIn = REALLY_SLOW_BLEND_IN
	sAnim.fBlendOut = REALLY_SLOW_BLEND_OUT
	sAnim.eSyncSceneFlags = SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS
ENDPROC

/// PURPOSE:
///    Gets the play anim info for the players 'ready up loop' animation clip
PROC ROULETTE_ANIM_GET_PLAYER_SEAT_READY_UP_LOOP_CLIP_PLAY_INFO(ROULETTE_DATA &sRouletteData, ROULETTE_ANIM &sAnim)
	sAnim.eClip = ROULETTE_ANIM_GET_APPRORIATE_READY_UP_LOOP_CLIP(sRouletteData)
	sAnim.eDictionary = ROULETTE_ANIM_DICT_PLAYER
	sAnim.bLoop = TRUE
	sAnim.fBlendIn = REALLY_SLOW_BLEND_IN
	sAnim.fBlendOut = REALLY_SLOW_BLEND_OUT
	sAnim.eSyncSceneFlags = SYNCED_SCENE_LOOP_WITHIN_SCENE | SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS
ENDPROC


///--------------------------------
///    Anim clip playing
///-------------------------------- 

/// PURPOSE:
///    Plays a random idle loop animation on the local player
PROC ROULETTE_ANIM_PLAY_RANDOM_IDLE_LOOP_ANIM_ON_LOCAL_PLAYER(ROULETTE_DATA &sRouletteData)
	ROULETTE_ANIM sIdleAnimPlayInfo
	ROULETTE_ANIM_GET_PLAYER_RANDOM_IDLE_LOOP_ANIM(sIdleAnimPlayInfo)
	ROULETTE_ANIM_PLAY_CLIP_ON_LOCAL_PLAYER(sRouletteData, sIdleAnimPlayInfo)
ENDPROC

/// PURPOSE:
///    Plays a random idle oneshot on the local player
PROC ROULETTE_ANIM_PLAY_RANDOM_IDLE_ONE_SHOT_ON_LOCAL_PLAYER(ROULETTE_DATA &sRouletteData)
	ROULETTE_ANIM sIdleOneShot
	ROULETTE_ANIM_GET_PLAYER_RANDOM_SEAT_IDLE_ONE_SHOT_ANIM(sIdleOneShot)
	ROULETTE_ANIM_PLAY_CLIP_ON_LOCAL_PLAYER(sRouletteData, sIdleOneShot)
ENDPROC

/// PURPOSE:
///    Starts the enter seat animation for the local player using the correct blends
PROC ROULETTE_ANIM_PLAY_ENTER_SEAT_ANIM(ROULETTE_DATA &sRouletteData)
	ROULETTE_ANIM sSeatEnterAnimPlayData
	ROULETTE_ANIM_GET_PLAYER_SEAT_ENTER_CLIP_PLAY_INFO(sRouletteData, sSeatEnterAnimPlayData)
	ROULETTE_ANIM_PLAY_CLIP_ON_LOCAL_PLAYER(sRouletteData, sSeatEnterAnimPlayData)
ENDPROC

/// PURPOSE:
///    Starts the exit seat animation for the local player using the correct blends
PROC ROULETTE_ANIM_PLAY_EXIT_SEAT_ANIM(ROULETTE_DATA &sRouletteData)
	ROULETTE_ANIM sExitSeatAnimPlayData
	ROULETTE_ANIM_GET_PLAYER_SEAT_EXIT_CLIP_PLAY_INFO(sRouletteData, sExitSeatAnimPlayData)	
	ROULETTE_ANIM_PLAY_CLIP_ON_LOCAL_PLAYER(sRouletteData, sExitSeatAnimPlayData)
ENDPROC

/// PURPOSE:
///    Plays an appropriate celebration according to the players payout for
///    the current round of roulette
PROC ROULETTE_ANIM_PLAY_APPROPRIATE_SEAT_REACTION_CLIP(ROULETTE_DATA &sRouletteData)	
	ROULETTE_ANIM sReactionAnimPlayData
	ROULETTE_ANIM_GET_PLAYER_SEAT_REACTION_CLIP_PLAY_INFO(sRouletteData, sReactionAnimPlayData)
	ROULETTE_ANIM_PLAY_CLIP_ON_LOCAL_PLAYER(sRouletteData, sReactionAnimPlayData)
ENDPROC

/// PURPOSE:
///    Plays an appropriate ready up intro animation on the local player
PROC ROULETTE_ANIM_PLAY_APPROPRIATE_READY_UP_INTRO_CLIP(ROULETTE_DATA &sRouletteData)
	ROULETTE_ANIM sReadyUpIntroAnimPlayData	
	ROULETTE_ANIM_GET_PLAYER_SEAT_READY_UP_INTRO_CLIP_PLAY_INFO(sRouletteData, sReadyUpIntroAnimPlayData)
	sRouletteData.eLastReadyUpClip = sReadyUpIntroAnimPlayData.eClip
	ROULETTE_ANIM_PLAY_CLIP_ON_LOCAL_PLAYER(sRouletteData, sReadyUpIntroAnimPlayData)
ENDPROC

/// PURPOSE:
///    Plays an appropriate ready up loop animation on the local player
PROC ROULETTE_ANIM_PLAY_APPROPRIATE_READY_UP_LOOP_CLIP(ROULETTE_DATA &sRouletteData)
	ROULETTE_ANIM sReadyUpLoopAnimPlayData
	ROULETTE_ANIM_GET_PLAYER_SEAT_READY_UP_LOOP_CLIP_PLAY_INFO(sRouletteData, sReadyUpLoopAnimPlayData)
	ROULETTE_ANIM_PLAY_CLIP_ON_LOCAL_PLAYER(sRouletteData, sReadyUpLoopAnimPlayData)	
ENDPROC


///----------------------------
///    Anim clip sequences
///----------------------------    

/// PURPOSE:
///    Handles starting a new random seat idle when the last one finishes
PROC ROULETTE_ANIM_MAINTAIN_RANDOM_SEAT_IDLE(ROULETTE_DATA &sRouletteData, BOOL bCanUseOneshot = TRUE)	
	
	// Don't play an idle if the player is moving to a coordinate
	IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) != FINISHED_TASK
		EXIT
	ENDIF
	
	// Don't play a new idle until the previous has finished
	IF NOT ROULETTE_ANIM_HAS_CURRENT_PLAYER_ANIM_FINISHED(sRouletteData)		
		EXIT
	ENDIF
	
	// If the anim that finished last was a loop, play a one shot to break it up but only if
	// there is time for a one shot (10 seconds before round end)
	IF sRouletteData.sLastPlayerAnimInfo.eClip = ROULETTE_ANIM_CLIP_PLAYER_IDLE_LOOP 
	AND bCanUseOneshot
		ROULETTE_ANIM_PLAY_RANDOM_IDLE_ONE_SHOT_ON_LOCAL_PLAYER(sRouletteData)
	ELSE
		ROULETTE_ANIM_PLAY_RANDOM_IDLE_LOOP_ANIM_ON_LOCAL_PLAYER(sRouletteData)
	ENDIF
ENDPROC

/// PURPOSE:
///    Update ready up animation sequence for the local player
PROC ROULETTE_ANIM_UPDATE_READY_FOR_SPIN_SEQUENCE_ON_LOCAL_PLAYER(ROULETTE_DATA &sRouletteData)
	SWITCH sRouletteData.sLastPlayerAnimInfo.eClip
		CASE ROULETTE_ANIM_CLIP_PLAYER_READY_UP_INTRO_HIGH_BET
		CASE ROULETTE_ANIM_CLIP_PLAYER_READY_UP_INTRO_MID_BET
		CASE ROULETTE_ANIM_CLIP_PLAYER_READY_UP_INTRO_NO_BET
			// Start loop when the intro has finished
			IF ROULETTE_ANIM_HAS_CURRENT_PLAYER_ANIM_FINISHED(sRouletteData)
				ROULETTE_ANIM_PLAY_APPROPRIATE_READY_UP_LOOP_CLIP(sRouletteData)
			ENDIF
		BREAK
		
		CASE ROULETTE_ANIM_CLIP_PLAYER_READY_UP_LOOP_NO_BET
		CASE ROULETTE_ANIM_CLIP_PLAYER_READY_UP_LOOP_MID_BET
		CASE ROULETTE_ANIM_CLIP_PLAYER_READY_UP_LOOP_HIGH_BET
			EXIT
		
		// If not looping or not playing intro, start the intro
		DEFAULT 
			ROULETTE_ANIM_PLAY_APPROPRIATE_READY_UP_INTRO_CLIP(sRouletteData)
		BREAK
	ENDSWITCH
ENDPROC
