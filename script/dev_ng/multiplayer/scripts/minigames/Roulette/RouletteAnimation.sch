//////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Name:        RouletteAnimation.sch																		
/// Description: Header for controlling animations for the roulette minigame								
///																											
/// Written by:  Online Technical Team: Tom Turner,															
/// Date:  		07/03/2019																					
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "RouletteAnimationWheel.sch"
USING "RouletteAnimationDealer.sch"
USING "RouletteAnimationPlayer.sch"

/// PURPOSE:
///    Removes all animation dictionaries used in the roulette minigame by the client
PROC ROULETTE_ANIM_REMOVE_ALL_CLIENT_DICTIONARIES()
	ROULETTE_ANIM_REMOVE_PLAYER_DICTIONARIES()
	ROULETTE_ANIM_REMOVE_PROP_DICTIONARY()
ENDPROC

