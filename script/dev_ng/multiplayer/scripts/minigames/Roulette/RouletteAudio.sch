//////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Name:        RouletteAudio.sch																			
/// Description: Header for playing audio for the roulette minigame																			
///																											
/// Written by:  Online Technical Team: Tom Turner,															
/// Date:  		10/06/2019																					
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "rc_helper_functions.sch"
USING "RouletteData.sch"
USING "RouletteHelper.sch"
USING "RouletteAudioOverrides.sch"

/// PURPOSE:
///    Checks if the dealer at the table is playing ambient dialogue 
FUNC BOOL ROULETTE_AUDIO_IS_DEALER_PLAYING_AUDIO(ROULETTE_DATA &sRouletteData, INT iTableID)	
	IF NOT ROULETTE_TABLE_IS_TABLE_ID_VALID(iTableID)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_AUDIO_IS_DEALER_PLAYING_AUDIO - ",
		"Dealer: ", iTableID, " - Invalid table ID")
		RETURN FALSE
	ENDIF
	
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(sRouletteData.serverBD.niDealerPed[iTableID])
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_AUDIO_IS_DEALER_PLAYING_AUDIO - ",
		"Dealer: ", iTableID, " - Dealer net id does not exist")
		RETURN FALSE
	ENDIF	
	
	PED_INDEX piDealer = NET_TO_PED(sRouletteData.serverBD.niDealerPed[iTableID])	
	
	IF NOT IS_ENTITY_ALIVE(piDealer)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_AUDIO_IS_DEALER_PLAYING_AUDIO - ",
		"Dealer: ", iTableID, " - Dealer is not alive")
		RETURN FALSE
	ENDIF
	
	RETURN IS_AMBIENT_SPEECH_PLAYING(piDealer)
ENDFUNC

/// PURPOSE:
///    Plays the given audio clip on the dealer at the given table ID. 
///    bSyncOverNetwork states if it should be played on the delaers clone on remote machines
PROC ROULETTE_AUDIO_PLAY_CLIP_ON_TABLES_DEALER(ROULETTE_DATA &sRouletteData, STRING strAudioClip, INT iTableID, BOOL bInterruptCurrent = FALSE,
SPEECH_PARAMS eMood = SPEECH_PARAMS_STANDARD, BOOL bSyncOverNetwork = TRUE)
	
	IF NOT ROULETTE_TABLE_IS_TABLE_ID_VALID(iTableID)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_AUDIO_PLAY_CLIP_ON_TABLES_DEALER - ",
		"Dealer: ", iTableID, " - Invalid table ID")
		EXIT
	ENDIF
	
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(sRouletteData.serverBD.niDealerPed[iTableID])
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_AUDIO_PLAY_CLIP_ON_TABLES_DEALER - ",
		"Dealer: ", iTableID, " - Dealer net id does not exist")
		EXIT
	ENDIF	
	
	PED_INDEX piDealer = NET_TO_PED(sRouletteData.serverBD.niDealerPed[iTableID])	
	
	IF NOT IS_ENTITY_ALIVE(piDealer)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_AUDIO_PLAY_CLIP_ON_TABLES_DEALER - ",
		"Dealer: ", iTableID, " - Dealer is not alive")
		EXIT
	ENDIF
	
	IF bInterruptCurrent AND IS_AMBIENT_SPEECH_PLAYING(piDealer)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_AUDIO_PLAY_CLIP_ON_TABLES_DEALER - ",
		"Dealer: ", iTableID, " - Not playing: ", strAudioClip,
		" - Cannot interrupt current speech")
		EXIT
	ENDIF
	
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_AUDIO_PLAY_CLIP_ON_TABLES_DEALER - ",
	"Dealer: ", iTableID, " - Playing: ", strAudioClip)
	
	STRING strParams = AUDIO_SPEECH_GET_PARAM_STRING_FROM_ENUM(eMood)
	PLAY_PED_AMBIENT_SPEECH_NATIVE(piDealer, strAudioClip, strParams, bSyncOverNetwork)
ENDPROC


///---------------------------------
///   Audio Clip Selection - Dealer 
///--------------------------------- 

/// PURPOSE:
///    Gets an appropriate audio clip to greet the player with  
FUNC ROULETTE_AUDIO_CLIP ROULETTE_AUDIO_GET_APPROPRIATE_GREET_CLIP(ROULETTE_DATA &sRouletteData, INT iTableID)		
	IF Is_Ped_Drunk(PLAYER_PED_ID())
		RETURN RAC_DEALER_GREET_DRUNK_PLAYER
	ENDIF
	
	IF ROULETTE_TABLE_IS_TABLE_ID_VALID(iTableID)
		// Rejoin table greeting
		IF IS_BIT_SET(sRouletteData.iTableBS[iTableID], ROULETTE_BS_HAS_PLAYER_PLAYED_AT_TABLE) 
			RETURN RAC_DEALER_GREET_REJOINING_PLAYER
		ENDIF
	ELSE
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_AUDIO_PLAY_CLIP_ON_TABLES_DEALER - ",
		"Dealer: ", iTableID, "- invalid table id!")
	ENDIF
	
	// Non VIP greeting
	IF NOT ROULETTE_TABLE_IS_HIGH_STAKES(iTableID)
		RETURN RAC_DEALER_GREET_PLAYER
	ENDIF	
	
	IF IS_PLAYER_FEMALE()
		RETURN RAC_DEALER_GREET_FEMALE_PLAYER
	ENDIF
	
	RETURN RAC_DEALER_GREET_MALE_PLAYER
ENDFUNC

/// PURPOSE:
///    Gets an appropriate audio clip for the dealer to say when the local player leaves the table    
FUNC ROULETTE_AUDIO_CLIP ROULETTE_AUDIO_GET_APPROPRIATE_LEAVE_CLIP(ROULETTE_DATA &sRouletteData)
	INT iChipCount = ROULETTE_BETTING_GET_LOCAL_PLAYER_CHIP_COUNT()	
	INT iChipsDelta = iChipCount  - sRouletteData.iRoundStartingChips
	
	IF iChipsDelta < 0
		RETURN RAC_DEALER_LEAVE_BAD
	ENDIF
	
	IF iChipsDelta > 0
		RETURN RAC_DEALER_LEAVE_GOOD
	ENDIF
	
	RETURN RAC_DEALER_LEAVE_NEUTRAL
ENDFUNC


///---------------------------------
///   Audio Clip Playing - Dealer 
///--------------------------------- 

/// PURPOSE:
///    Plays an appropriate audio greeting from the dealer at the table
FUNC BOOL ROULETTE_AUDIO_PLAY_DEALER_GREET_PLAYER(ROULETTE_DATA &sRouletteData)
	INT iTableID = sRouletteData.iTableID
	
	IF NOT TAKE_CONTROL_OF_NET_ID(sRouletteData.serverBD.niDealerPed[iTableID])
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_AUDIO_PLAY_DEALER_GREET_PLAYER - ",
		"Table: ", iTableID, " - Does not have ownership of ped")
		RETURN FALSE
	ENDIF
	
	ROULETTE_AUDIO_CLIP eAudioClip = ROULETTE_AUDIO_GET_APPROPRIATE_GREET_CLIP(sRouletteData, iTableID)
	STRING strAudioClip = ROULETTE_AUDIO_GET_CLIP(eAudioClip)
	ROULETTE_AUDIO_PLAY_CLIP_ON_TABLES_DEALER(sRouletteData, strAudioClip, iTableID)
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Plays an appropriate player leaving clip from the dealer at the table
FUNC BOOL ROULETTE_AUDIO_PLAY_DEALER_LEAVE_CLIP(ROULETTE_DATA &sRouletteData)
	INT iTableID = sRouletteData.iTableID
	
	IF NOT TAKE_CONTROL_OF_NET_ID(sRouletteData.serverBD.niDealerPed[iTableID])
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_AUDIO_PLAY_DEALER_LEAVE_CLIP - ",
		"Table: ", iTableID, " - Does not have ownership of ped")
		RETURN FALSE
	ENDIF
	
	ROULETTE_AUDIO_CLIP eAudioClip = ROULETTE_AUDIO_GET_APPROPRIATE_LEAVE_CLIP(sRouletteData)
	STRING strAudioClip = ROULETTE_AUDIO_GET_CLIP(eAudioClip)
	ROULETTE_AUDIO_PLAY_CLIP_ON_TABLES_DEALER(sRouletteData, strAudioClip, iTableID)
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Plays no more bets audio on the tables dealer
FUNC BOOL ROULETTE_AUDIO_PLAY_DEALER_NO_MORE_BETS_CLIP(ROULETTE_DATA &sRouletteData, INT iTableID)
	IF NOT TAKE_CONTROL_OF_NET_ID(sRouletteData.serverBD.niDealerPed[iTableID])
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_AUDIO_PLAY_DEALER_NO_MORE_BETS_CLIP - ",
		"Table: ", iTableID, " - Does not have ownership of ped")
		RETURN FALSE
	ENDIF
	
	STRING strAudioClip = ROULETTE_AUDIO_GET_CLIP(RAC_DEALER_NO_MORE_BETS)
	ROULETTE_AUDIO_PLAY_CLIP_ON_TABLES_DEALER(sRouletteData, strAudioClip, iTableID)
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Plays another round audio on the tables dealer
FUNC BOOL ROULETTE_AUDIO_PLAY_DEALER_ANOTHER_ROUND_CLIP(ROULETTE_DATA &sRouletteData, INT iTableID)
	IF NOT TAKE_CONTROL_OF_NET_ID(sRouletteData.serverBD.niDealerPed[iTableID])
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_AUDIO_PLAY_DEALER_NO_MORE_BETS_CLIP - ",
		"Table: ", iTableID, " - Does not have ownership of ped")
		RETURN FALSE
	ENDIF
	
	STRING strAudioClip = ROULETTE_AUDIO_GET_CLIP(RAC_DEALER_ANOTHER_ROUND)
	ROULETTE_AUDIO_PLAY_CLIP_ON_TABLES_DEALER(sRouletteData, strAudioClip, iTableID)
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Plays place your bets audio on the tables dealer
FUNC BOOL ROULETTE_AUDIO_PLAY_DEALER_PLACE_YOUR_BETS_CLIP(ROULETTE_DATA &sRouletteData, INT iTableID)
	IF NOT TAKE_CONTROL_OF_NET_ID(sRouletteData.serverBD.niDealerPed[iTableID])
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_AUDIO_PLAY_DEALER_PLACE_YOUR_BETS_CLIP - ",
		"Table: ", iTableID, " - Does not have ownership of ped")
		RETURN FALSE
	ENDIF
	
	STRING strAudioClip = ROULETTE_AUDIO_GET_CLIP(RAC_DEALER_PLACE_YOUR_BETS)
	ROULETTE_AUDIO_PLAY_CLIP_ON_TABLES_DEALER(sRouletteData, strAudioClip, iTableID)
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Plays place refuse bets audio on the tables dealer
FUNC BOOL ROULETTE_AUDIO_PLAY_DEALER_REFUSE_BETS_CLIP(ROULETTE_DATA &sRouletteData)
	INT iTableID = sRouletteData.iTableID
	
	IF NOT TAKE_CONTROL_OF_NET_ID(sRouletteData.serverBD.niDealerPed[iTableID])
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_AUDIO_PLAY_DEALER_REFUSE_BETS_CLIP - ",
		"Table: ", iTableID, " - Does not have ownership of ped")
		RETURN FALSE
	ENDIF
	
	STRING strAudioClip = ROULETTE_AUDIO_GET_CLIP(RAC_DEALER_REFUSE_BETS)
	ROULETTE_AUDIO_PLAY_CLIP_ON_TABLES_DEALER(sRouletteData, strAudioClip, iTableID)
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Calls the winning number for the given table
FUNC BOOL ROULETTE_AUDIO_PLAY_DEALER_CALL_WINNING_NUMBER(ROULETTE_DATA &sRouletteData, INT iTableID)	
	IF NOT ROULETTE_TABLE_IS_TABLE_ID_VALID(iTableID)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_AUDIO_PLAY_DEALER_CALL_WINNING_NUMBER - ",
		"Table: ", iTableID, " - Invalid table ID")
		RETURN FALSE
	ENDIF
	
	INT iWinNumber = ROULETTE_HELPER_GET_SERVERS_CURRENT_WINNING_NUMBER_FOR_TABLE(sRouletteData, iTableID)
	IF iWinNumber < 0
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_AUDIO_PLAY_DEALER_CALL_WINNING_NUMBER - ",
		"Table: ", iTableID, " - Invalid win result")
		RETURN FALSE
	ENDIF
	
	STRING strAudioClip = ROULETTE_AUDIO_GET_CLIP(RAC_DEALER_CALL_WINNING_NUMBER, iWinNumber)
	ROULETTE_AUDIO_PLAY_CLIP_ON_TABLES_DEALER(sRouletteData, strAudioClip, iTableID, TRUE, DEFAULT, FALSE)
	RETURN TRUE
ENDFUNC


///------------------------------
///    SFX
///------------------------------

/// PURPOSE:
///    Adds the players current dealer to the table mix group so they can be heard better
PROC ROULETTE_AUDIO_ADD_CURRENT_TABLES_DEALER_TO_TABLE_MIX_GROUP(ROULETTE_DATA &sRouletteData)
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_AUDIO_ADD_CURRENT_TABLES_DEALER_TO_TABLE_MIX_GROUP - Called")
	
	INT iTableID = sRouletteData.iTableID
	
	IF NOT ROULETTE_TABLE_IS_TABLE_ID_VALID(iTableID)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_AUDIO_ADD_CURRENT_TABLES_DEALER_TO_TABLE_MIX_GROUP - Invalid table ID")
		EXIT
	ENDIF
	
	NETWORK_INDEX niDealer = sRouletteData.serverBD.niDealerPed[iTableID]
	
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niDealer)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_AUDIO_ADD_CURRENT_TABLES_DEALER_TO_TABLE_MIX_GROUP - Dealer net ID doesn't exist")
		EXIT
	ENDIF
	
	PED_INDEX piDealer = NET_TO_PED(niDealer)
		
	IF NOT DOES_ENTITY_EXIST(piDealer)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_AUDIO_ADD_CURRENT_TABLES_DEALER_TO_TABLE_MIX_GROUP - Dealer ped doesn't exist")
		EXIT
	ENDIF
	
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_AUDIO_ADD_CURRENT_TABLES_DEALER_TO_TABLE_MIX_GROUP - Added dealer to mix group")
	ADD_ENTITY_TO_AUDIO_MIX_GROUP(piDealer, "DLC_VW_Casino_Table_Games_Dealer_Group")
ENDPROC

/// PURPOSE:
///    Removes the players current dealer from the table mix group
PROC ROULETTE_AUDIO_REMOVE_CURRENT_TABLES_DEALER_FROM_TABLE_MIX_GROUP(ROULETTE_DATA &sRouletteData)
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_AUDIO_REMOVE_CURRENT_TABLES_DEALER_FROM_TABLE_MIX_GROUP - Called")
	
	INT iTableID = sRouletteData.iTableID
	
	IF NOT ROULETTE_TABLE_IS_TABLE_ID_VALID(iTableID)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_AUDIO_REMOVE_CURRENT_TABLES_DEALER_FROM_TABLE_MIX_GROUP - Invalid table ID")
		EXIT
	ENDIF
	
	NETWORK_INDEX niDealer = sRouletteData.serverBD.niDealerPed[iTableID]
	
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niDealer)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_AUDIO_REMOVE_CURRENT_TABLES_DEALER_FROM_TABLE_MIX_GROUP - Dealer net ID doesn't exist")
		EXIT
	ENDIF
	
	PED_INDEX piDealer = NET_TO_PED(niDealer)
		
	IF NOT DOES_ENTITY_EXIST(piDealer)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_AUDIO_REMOVE_CURRENT_TABLES_DEALER_FROM_TABLE_MIX_GROUP - Dealer ped doesn't exist")
		EXIT
	ENDIF
	
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_AUDIO_REMOVE_CURRENT_TABLES_DEALER_FROM_TABLE_MIX_GROUP - Removed dealer from mix group")
	REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(piDealer)
ENDPROC

/// PURPOSE:
///    Starts the audio scene so sfx can be heard better
PROC ROULETTE_AUDIO_START_TABLE_SCENE()
	IF NOT IS_AUDIO_SCENE_ACTIVE("DLC_VW_Casino_Table_Games")	
		START_AUDIO_SCENE("DLC_VW_Casino_Table_Games")
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_AUDIO_START_TABLE_SCENE - Started")
	ENDIF
ENDPROC

/// PURPOSE:
///    Stops the roulette audio scene
PROC ROULETTE_AUDIO_STOP_TABLE_SCENE()
	IF IS_AUDIO_SCENE_ACTIVE("DLC_VW_Casino_Table_Games")	
		STOP_AUDIO_SCENE("DLC_VW_Casino_Table_Games")
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_AUDIO_STOP_TABLE_SCENE - Stopped")
	ENDIF
ENDPROC

/// PURPOSE:
///    Starts the wheel audio scene so sfx can be heard better
PROC ROULETTE_AUDIO_START_WHEEL_SCENE()
	IF NOT IS_AUDIO_SCENE_ACTIVE("DLC_VW_Casino_Roulette_Focus_Wheel")	
		START_AUDIO_SCENE("DLC_VW_Casino_Roulette_Focus_Wheel")
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_AUDIO_START_WHEEL_SCENE - Started")
	ENDIF
ENDPROC

/// PURPOSE:
///    Stops the roulette wheel audio scene
PROC ROULETTE_AUDIO_STOP_WHEEL_SCENE()
	IF IS_AUDIO_SCENE_ACTIVE("DLC_VW_Casino_Roulette_Focus_Wheel")	
		STOP_AUDIO_SCENE("DLC_VW_Casino_Roulette_Focus_Wheel")
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_AUDIO_STOP_WHEEL_SCENE - Stopped")
	ENDIF
ENDPROC


//----------------------
// 		Front end
//----------------------

/// PURPOSE:
///    Plays invalid action sound
PROC ROULETTE_AUDIO_PLAY_SFX_INVALID_ACTION()
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_AUDIO_PLAY_SFX_INVALID_ACTION - Called")
	PLAY_SOUND_FRONTEND(-1, ROULETTE_AUDIO_GET_CLIP(RAC_SFX_INVALID_ACTION),
	ROULETTE_AUDIO_GET_SOUND_SET(RAS_FRONTEND_SOUNDS))
ENDPROC

/// PURPOSE:
///    Plays highlight sound
PROC ROULETTE_AUDIO_PLAY_SFX_HIGHLIGHT()
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_AUDIO_PLAY_SFX_HIGHLIGHT - Called")
	PLAY_SOUND_FRONTEND(-1, ROULETTE_AUDIO_GET_CLIP(RAC_SFX_HIGHLIGHT),
	ROULETTE_AUDIO_GET_SOUND_SET(RAS_FRONTEND_SOUNDS))
ENDPROC

/// PURPOSE:
///    Plays sound for adjusting chips up
PROC ROULETTE_AUDIO_PLAY_SFX_ADJUST_CHIPS_UP()
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_AUDIO_PLAY_SFX_ADJUST_CHIPS_UP - Called")
	PLAY_SOUND_FRONTEND(-1, ROULETTE_AUDIO_GET_CLIP(RAC_SFX_ADJUST_CHIPS_UP),
	ROULETTE_AUDIO_GET_SOUND_SET(RAS_FRONTEND_SOUNDS))
ENDPROC

/// PURPOSE:
///    Plays sound for adjusting chips down
PROC ROULETTE_AUDIO_PLAY_SFX_ADJUST_CHIPS_DOWN()
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_AUDIO_PLAY_SFX_ADJUST_CHIPS_DOWN - Called")
	PLAY_SOUND_FRONTEND(-1, ROULETTE_AUDIO_GET_CLIP(RAC_SFX_ADJUST_CHIPS_DOWN), 
	ROULETTE_AUDIO_GET_SOUND_SET(RAS_FRONTEND_SOUNDS))
ENDPROC

/// PURPOSE:
///    Plays sound for showing the rules screen
PROC ROULETTE_AUDIO_PLAY_SFX_SHOW_RULES()
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_AUDIO_PLAY_SFX_SHOW_RULES - Called")
	PLAY_SOUND_FRONTEND(-1, ROULETTE_AUDIO_GET_CLIP(RAC_SFX_SHOW_RULES), 
	ROULETTE_AUDIO_GET_SOUND_SET(RAS_FRONTEND_SOUNDS))
ENDPROC

/// PURPOSE:
///    Plays sound for a continue button
PROC ROULETTE_AUDIO_PLAY_SFX_CONTINUE()
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_AUDIO_PLAY_SFX_CONTINUE - Called")
	PLAY_SOUND_FRONTEND(-1, ROULETTE_AUDIO_GET_CLIP(RAC_SFX_CONTINUE), 
	ROULETTE_AUDIO_GET_SOUND_SET(RAS_FRONTEND_SOUNDS))
ENDPROC

/// PURPOSE:
///    Plays sound for the max bet button
PROC ROULETTE_AUDIO_PLAY_SFX_BET_MAX()
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_AUDIO_PLAY_SFX_BET_MAX - Called")
	PLAY_SOUND_FRONTEND(-1, ROULETTE_AUDIO_GET_CLIP(RAC_SFX_BET_MAX), 
	ROULETTE_AUDIO_GET_SOUND_SET(RAS_FRONTEND_SOUNDS))
ENDPROC

/// PURPOSE:
///    Plays sound for the remove bet button
PROC ROULETTE_AUDIO_PLAY_SFX_REMOVE_BET()
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_AUDIO_PLAY_SFX_REMOVE_BET - Called")
	PLAY_SOUND_FRONTEND(-1, ROULETTE_AUDIO_GET_CLIP(RAC_SFX_REMOVE_BET), 
	ROULETTE_AUDIO_GET_SOUND_SET(RAS_FRONTEND_SOUNDS))
ENDPROC

/// PURPOSE:
///    Plays sound for the win button
PROC ROULETTE_AUDIO_PLAY_SFX_WIN()
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_AUDIO_PLAY_SFX_WIN - Called")
	PLAY_SOUND_FRONTEND(-1, ROULETTE_AUDIO_GET_CLIP(RAC_SFX_WIN), 
	ROULETTE_AUDIO_GET_SOUND_SET(RAS_FRONTEND_SOUNDS))
ENDPROC


//----------------------
// 		World audio
//----------------------

/// PURPOSE:
///    Plays sound for chips being place
PROC ROULETTE_AUDIO_PLAY_SFX_PLACE_CHIPS(VECTOR vPos)
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_AUDIO_PLAY_SFX_PLACE_CHIPS - Called")
	PLAY_SOUND_FROM_COORD(-1, ROULETTE_AUDIO_GET_CLIP(RAC_SFX_PLACE_CHIPS), vPos,
	ROULETTE_AUDIO_GET_SOUND_SET(RAS_TABLE_GAME_SOUNDS), TRUE)
ENDPROC

/// PURPOSE:
///    Plays sound for when a chip is placed and the stack swaps to a higher chip model
PROC ROULETTE_AUDIO_PLAY_SFX_PLACE_CHIPS_SWAPPED_MODEL(VECTOR vPos)
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_AUDIO_PLAY_SFX_PLACE_CHIPS_SWAPPED_MODEL - Called")
	PLAY_SOUND_FROM_COORD(-1, ROULETTE_AUDIO_GET_CLIP(RAC_SFX_PLACE_CHIPS_MODEL_SWAP), vPos,
	ROULETTE_AUDIO_GET_SOUND_SET(RAS_TABLE_GAME_SOUNDS), TRUE)
ENDPROC

/// PURPOSE:
///    Plays sound for clearing a single chip
PROC ROULETTE_AUDIO_PLAY_SFX_CLEAR_CHIPS_SMALL(VECTOR vPos)
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_AUDIO_PLAY_SFX_CLEAR_CHIPS_SMALL - Called")
	PLAY_SOUND_FROM_COORD(-1, ROULETTE_AUDIO_GET_CLIP(RAC_SFX_CLEAR_CHIPS_SMALL), vPos,
	ROULETTE_AUDIO_GET_SOUND_SET(RAS_TABLE_GAME_SOUNDS), TRUE)
ENDPROC

/// PURPOSE:
///    Plays sound for clearing a medium pile of chips
PROC ROULETTE_AUDIO_PLAY_SFX_CLEAR_CHIPS_MEDIUM(VECTOR vPos)
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_AUDIO_PLAY_SFX_CLEAR_CHIPS_MEDIUM - Called")
	PLAY_SOUND_FROM_COORD(-1, ROULETTE_AUDIO_GET_CLIP(RAC_SFX_CLEAR_CHIPS_MEDIUM), vPos,
	ROULETTE_AUDIO_GET_SOUND_SET(RAS_TABLE_GAME_SOUNDS), TRUE)
ENDPROC

/// PURPOSE:
///    Plays sound for clearing a large pile of chips
PROC ROULETTE_AUDIO_PLAY_SFX_CLEAR_CHIPS_LARGE(VECTOR vPos)
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_AUDIO_PLAY_SFX_CLEAR_CHIPS_LARGE - Called")
	PLAY_SOUND_FROM_COORD(-1, ROULETTE_AUDIO_GET_CLIP(RAC_SFX_CLEAR_CHIPS_LARGE), vPos,
	ROULETTE_AUDIO_GET_SOUND_SET(RAS_TABLE_GAME_SOUNDS), TRUE)
ENDPROC

/// PURPOSE:
///    Plays a sound effect at the position
PROC ROULETTE_AUDIO_PLAY_SFX_AT_POSITION(ROULETTE_AUDIO_CLIP eClip, ROULETTE_AUDIO_SET eSoundSet, VECTOR vPos, BOOL bNetwork = FALSE)	
	STRING strClip = ROULETTE_AUDIO_GET_CLIP(eClip)
	STRING strSoundSet = ROULETTE_AUDIO_GET_SOUND_SET(eSoundSet)
	
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_AUDIO_PLAY_SFX_AT_POSITION - Played: ",
	strClip, ", ", strSoundSet, " at: ", GET_STRING_FROM_VECTOR(vPos))
	
	PLAY_SOUND_FROM_COORD(-1, strClip, vPos, strSoundSet, bNetwork)
ENDPROC

/// PURPOSE:
///    Gets an appropriate chip clear to play based on how many chips are being cleared   
FUNC ROULETTE_AUDIO_CLIP ROULETTE_AUDIO_GET_APPROPRIATE_CHIP_CLEAR_CLIP_FOR_AMOUNT(INT iAmountOfChipsCleared)
	
	IF iAmountOfChipsCleared >= ROULETTE_AUDIO_AMOUNT_OF_CHIPS_CLEARED_FOR_LARGE_CLEAR_SFX
		RETURN RAC_SFX_CLEAR_CHIPS_LARGE
	ENDIF
	
	IF iAmountOfChipsCleared >= ROULETTE_AUDIO_AMOUNT_OF_CHIPS_CLEARED_FOR_MEDIUM_CLEAR_SFX
		RETURN RAC_SFX_CLEAR_CHIPS_MEDIUM
	ENDIF
	
	RETURN  RAC_SFX_CLEAR_CHIPS_SMALL
ENDFUNC

/// PURPOSE:
///    Plays an appropriate chip clear sound at the position according to how many chips are being cleared
PROC ROULETTE_AUDIO_PLAY_SFX_CHIP_CLEAR_FOR_AMOUNT_AT_POS(VECTOR vSoundPos, INT iAmountOfChipsCleared)
	ROULETTE_AUDIO_CLIP eClearClip = ROULETTE_AUDIO_GET_APPROPRIATE_CHIP_CLEAR_CLIP_FOR_AMOUNT(iAmountOfChipsCleared)
	ROULETTE_AUDIO_SET eSoundSet = RAS_TABLE_GAME_SOUNDS
	ROULETTE_AUDIO_PLAY_SFX_AT_POSITION(eClearClip, eSoundSet, vSoundPos)
ENDPROC

/// PURPOSE:
///    PLays an appropriate 
PROC ROULETTE_AUDIO_PLAY_SFX_APPROPRIATE_CLEAR_SOUND_FOR_ZONE(ROULETTE_DATA &sRouletteData, INT iTableID, INT iZoneID, INT iAmountOfChipsCleared)
	UNUSED_PARAMETER(sRouletteData)	
	VECTOR vSoundPos = sRouletteData.vZoneAverageDeletedChipPos[iTableID][iZoneID - 1]
	ROULETTE_AUDIO_PLAY_SFX_CHIP_CLEAR_FOR_AMOUNT_AT_POS(vSoundPos, iAmountOfChipsCleared)
ENDPROC

