//////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Name:        RoulettePropsOverrides.sch																			
/// Description: Header containing all overrideable functions and parameters relating to the roulette props
///    			 useful for overriding the models used in the roulette minigame
///																											
/// Written by:  Online Technical Team: Tom Turner,															
/// Date:  		15/03/2019																					
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "freemode_header.sch"
USING "RouletteConstants.sch"

// Height of the chip stack model
#IF NOT DEFINED(ROULETTE_PROPS_CHIP_STACK_HEIGHT)
	/// PURPOSE: The height of the chips stack model
	TWEAK_FLOAT ROULETTE_PROPS_CHIP_STACK_HEIGHT 0.06
#ENDIF

// How many chips are in a stack
#IF NOT DEFINED(ROULETTE_PROPS_NUMBER_OF_CHIPS_IN_STACK)
	/// PURPOSE: The number of chips in a chip stack
	CONST_INT ROULETTE_PROPS_NUMBER_OF_CHIPS_IN_STACK 12
#ENDIF

#IF NOT DEFINED(ROULETTE_PROPS_OBJECT_ROTATION_OFFSET)
	/// PURPOSE: Rotation offset from table heading for chips and cursor
	/// 	     use if chips or cursor are rotated incorectly
	CONST_INT ROULETTE_PROPS_OBJECT_ROTATION_OFFSET 0
#ENDIF

#IF NOT DEFINED(ROULETTE_PROPS_LAYOUT_HIGHLIGHT_OFFSET)
	/// PURPOSE: Highlight z offset from the layout decal
	TWEAK_FLOAT ROULETTE_PROPS_LAYOUT_HIGHLIGHT_OFFSET -0.0025
#ENDIF

#IF NOT DEFINED(ROULETTE_PROPS_LAYOUT_ZEROES_HIGHLIGHT_X_OFFSET)
	/// PURPOSE: Highlight x offset for zeroes highlights
	TWEAK_FLOAT ROULETTE_PROPS_LAYOUT_ZEROES_HIGHLIGHT_X_OFFSET 0.00880
#ENDIF

#IF NOT DEFINED(ROULETTE_PROPS_LOW_STAKES_TABLE_MODEL)
	/// PURPOSE: Hash of the low stakes table model name
	CONST_INT ROULETTE_PROPS_LOW_STAKES_TABLE_MODEL HASH("vw_prop_casino_roulette_01")
#ENDIF

#IF NOT DEFINED(ROULETTE_PROPS_HIGH_STAKES_TABLE_MODEL)
	/// PURPOSE: Hash of the high stakes table model name
	CONST_INT ROULETTE_PROPS_HIGH_STAKES_TABLE_MODEL HASH("vw_prop_casino_roulette_01b")
#ENDIF

#IF NOT DEFINED(ROULETTE_PROPS_GET_TABLE_MODEL_TINT_INDEX)
	/// PURPOSE:
	///    Gets the table model tint index  
	FUNC INT ROULETTE_PROPS_GET_TABLE_MODEL_TINT_INDEX(INT iTableID, BOOL bIsHighStakes)
		UNUSED_PARAMETER(iTableID)
		
		IF bIsHighStakes
			RETURN 3
		ENDIF
		
		RETURN 0
	ENDFUNC
#ENDIF

#IF NOT DEFINED(ROULETTE_PROPS_GET_DEALER_PED_MODEL)
	/// PURPOSE:
	///    Gets the model name of the dealer ped being used  
	FUNC MODEL_NAMES ROULETTE_PROPS_GET_DEALER_PED_MODEL(BOOL bIsFemale)
		IF bIsFemale
			RETURN INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("s_f_y_casino_01"))
		ENDIF
		RETURN INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("S_M_Y_Casino_01"))
	ENDFUNC
#ENDIF

#IF NOT DEFINED(ROULETTE_PROPS_SET_DEALER_PED_COMPONENTS)
	/// PURPOSE:
	///    Default function for setting a vcreated dealers ped components
	PROC ROULETTE_PROPS_SET_DEALER_PED_COMPONENTS(PED_INDEX &piDealer, BOOL bIsFemale, INT iTableID)
		UNUSED_PARAMETER(bIsFemale)
		UNUSED_PARAMETER(iTableID)
		SET_PED_COMPONENT_VARIATION(piDealer, INT_TO_ENUM(PED_COMPONENT, 0), 0, 0, 0) // (head)
		SET_PED_COMPONENT_VARIATION(piDealer, INT_TO_ENUM(PED_COMPONENT, 2), 0, 0, 0) // (hair)
		SET_PED_COMPONENT_VARIATION(piDealer, INT_TO_ENUM(PED_COMPONENT, 3), 0, 0, 0) // (uppr)
		SET_PED_COMPONENT_VARIATION(piDealer, INT_TO_ENUM(PED_COMPONENT, 4), 0, 0, 0) // (lowr)
		SET_PED_COMPONENT_VARIATION(piDealer, INT_TO_ENUM(PED_COMPONENT, 8), 1, 0, 0) // (accs)
	ENDPROC
#ENDIF

#IF NOT DEFINED(ROULETTE_PROPS_SET_DEALER_PED_VOICE_GROUP)
	/// PURPOSE:
	///    Default function for setting a vcreated dealers ped voice group
	PROC ROULETTE_PROPS_SET_DEALER_PED_VOICE_GROUP(PED_INDEX &piDealer, BOOL bIsFemale, INT iTableID)
		UNUSED_PARAMETER(piDealer)
		UNUSED_PARAMETER(bIsFemale)
		UNUSED_PARAMETER(iTableID)
	ENDPROC
#ENDIF

#IF NOT DEFINED(ROULETTE_PROPS_GET_HIGHLIGHT_MODEL)
	/// PURPOSE:
	///    Gets the model name for the highlight based on a bet type (some cells may need different highlight shapes)
	FUNC MODEL_NAMES ROULETTE_PROPS_GET_HIGHLIGHT_MODEL(BOOL bZeroesHighlight = FALSE)
		
		IF bZeroesHighlight
			RETURN INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("vw_prop_vw_marker_01a"))
		ENDIF
		
		RETURN INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("vw_prop_vw_marker_02a"))
	ENDFUNC
#ENDIF

#IF NOT DEFINED(ROULETTE_PROPS_GET_CHIP_STACK_MODEL)
	/// PURPOSE:
	///    Gets the chip stack model name for the given chip denomination   
	FUNC MODEL_NAMES ROULETTE_PROPS_GET_CHIP_STACK_MODEL(ROULETTE_CHIP_DENOMINATIONS eDenomination)		
		SWITCH eDenomination
			CASE TEN_DOLLAR_CHIP			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("vw_Prop_Chip_10dollar_st"))
			CASE FIFTY_DOLLAR_CHIP 			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("vw_prop_chip_50dollar_st"))
			CASE ONE_HUNDRED_DOLLAR_CHIP	RETURN INT_TO_ENUM(MODEL_NAMES, HASH("vw_prop_chip_100dollar_st"))
			CASE FIVE_HUNDRED_DOLLAR_CHIP	RETURN INT_TO_ENUM(MODEL_NAMES, HASH("vw_prop_chip_500dollar_st"))
			CASE ONE_THOUSAND_DOLLAR_CHIP	RETURN INT_TO_ENUM(MODEL_NAMES, HASH("vw_prop_chip_1kdollar_st"))
			CASE FIVE_THOUSAND_DOLLAR_CHIP	RETURN INT_TO_ENUM(MODEL_NAMES, HASH("vw_prop_chip_5kdollar_st"))
			CASE TEN_THOUSAND_DOLLAR_CHIP	RETURN INT_TO_ENUM(MODEL_NAMES, HASH("vw_prop_chip_10kdollar_st"))
		ENDSWITCH
		
		RETURN DUMMY_MODEL_FOR_SCRIPT
	ENDFUNC
#ENDIF

#IF NOT DEFINED(ROULETTE_PROPS_GET_CURSOR_MODEL)
	/// PURPOSE:
	///    Gets the cursor model name for the chip denomination   
	FUNC MODEL_NAMES ROULETTE_PROPS_GET_CURSOR_MODEL(ROULETTE_CHIP_DENOMINATIONS eDenomination)
		SWITCH eDenomination
			CASE TEN_DOLLAR_CHIP			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("vw_Prop_Chip_10dollar_x1"))
			CASE FIFTY_DOLLAR_CHIP 			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("vw_prop_chip_50dollar_x1"))
			CASE ONE_HUNDRED_DOLLAR_CHIP	RETURN INT_TO_ENUM(MODEL_NAMES, HASH("vw_prop_chip_100dollar_x1"))
			CASE FIVE_HUNDRED_DOLLAR_CHIP	RETURN INT_TO_ENUM(MODEL_NAMES, HASH("vw_prop_chip_500dollar_x1"))
			CASE ONE_THOUSAND_DOLLAR_CHIP	RETURN INT_TO_ENUM(MODEL_NAMES, HASH("vw_prop_chip_1kdollar_x1"))
			CASE FIVE_THOUSAND_DOLLAR_CHIP	RETURN INT_TO_ENUM(MODEL_NAMES, HASH("vw_prop_chip_5kdollar_x1"))
			CASE TEN_THOUSAND_DOLLAR_CHIP	RETURN INT_TO_ENUM(MODEL_NAMES, HASH("vw_prop_chip_10kdollar_x1"))
		ENDSWITCH
		
		RETURN INT_TO_ENUM(MODEL_NAMES, HASH("vw_Prop_Chip_10dollar_x1"))
	ENDFUNC
#ENDIF

#IF NOT DEFINED(ROULETTE_PROPS_GET_BALL_MODEL)
	/// PURPOSE:
	///    Gets the model name for the ball model
	FUNC MODEL_NAMES ROULETTE_PROPS_GET_BALL_MODEL()
		RETURN INT_TO_ENUM(MODEL_NAMES, HASH("vw_prop_roulette_ball"))
	ENDFUNC
#ENDIF

#IF NOT DEFINED(ROULETTE_PROPS_SHOULD_TABLE_DEALER_BE_FEMALE)
	FUNC BOOL ROULETTE_PROPS_SHOULD_TABLE_DEALER_BE_FEMALE(INT iTableID)
		UNUSED_PARAMETER(iTableID)
		RETURN GET_RANDOM_BOOL()
	ENDFUNC
#ENDIF

#IF NOT DEFINED(ROULETTE_PROPS_PROCESS_CREATED_TABLE)
	PROC ROULETTE_PROPS_PROCESS_CREATED_TABLE(OBJECT_INDEX &objTable, INT iTableID)
		UNUSED_PARAMETER(objTable)
		UNUSED_PARAMETER(iTableID)
	ENDPROC
#ENDIF

#IF NOT DEFINED(ROULETTE_PROPS_SHOULD_CHIPS_BE_CREATED)
	FUNC BOOL ROULETTE_PROPS_SHOULD_CHIPS_BE_CREATED()
		RETURN TRUE
	ENDFUNC
#ENDIF

#IF NOT DEFINED(ROULETTE_PROPS_SHOULD_HIDE_BALL)
	/// PURPOSE:
	///    Overrideable to checks if the ball should be hidden 
	FUNC BOOL ROULETTE_PROPS_SHOULD_HIDE_BALL(INT iTableID)
		UNUSED_PARAMETER(iTableID)
		RETURN IS_CUTSCENE_ACTIVE()
	ENDFUNC
#ENDIF



