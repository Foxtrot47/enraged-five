//////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Name:        RouletteSeatRequesting.sch																			
/// Description: Header that handles the requesting a roulette seat to play on
///																											
/// Written by:  Online Technical Team: Tom Turner,															
/// Date:  		15/03/2019																					
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "RouletteData.sch"
USING "RouletteTable.sch"
USING "RouletteProps.sch"
USING "RouletteHelper.sch"

CONST_INT ROULETTE_SEAT_REQUEST_MAX_SEATS_CHECKED_PER_FRAME 8

/// PURPOSE:
///      Checks if the player is in the area of the provded seat id   
FUNC BOOL IS_PLAYER_IN_SEAT_AREA(ROULETTE_DATA &sRouletteData, INT iSeatId, PLAYER_INDEX playerToCheck)
	VECTOR vSeatPosition = ROULETTE_PROPS_GET_SEAT_BONE_POSITION(sRouletteData, iSeatId)
	VECTOR vPlayerPosition = GET_ENTITY_COORDS(GET_PLAYER_PED(playerToCheck))
	
	FLOAT fDistance = GET_DISTANCE_BETWEEN_COORDS_SQUARED(vSeatPosition + <<0, 0, 0.5>>, vPlayerPosition)
	RETURN fDistance <= ROULETTE_SEAT_LOCATE_RADIUS + ROULETTE_SEAT_LOCATE_RADIUS
ENDFUNC

/// PURPOSE:
///    Checks if the player is facing the seat
FUNC BOOL IS_PLAYER_FACING_SEAT(ROULETTE_DATA &sRouletteData, INT iSeatID)
	VECTOR vSeatPos = ROULETTE_PROPS_GET_SEAT_BONE_POSITION(sRouletteData, iSeatID)
	VECTOR vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
	FLOAT fPlayerHeading = GET_ENTITY_HEADING(PLAYER_PED_ID())
	FLOAT fDirHeaing = GET_HEADING_BETWEEN_VECTORS_2D(vPlayerPos, vSeatPos)	
	RETURN IS_HEADING_ACCEPTABLE_CORRECTED(fDirHeaing, fPlayerHeading, ROULETTE_SEAT_HEADING_REQUIREMENT)
ENDFUNC

/// PURPOSE:
///    Checks if the local player is in the area of the provded seat id   
FUNC BOOL IS_LOCAL_PLAYER_IN_SEAT_AREA(ROULETTE_DATA &sRouletteData, INT iSeatId)
	RETURN IS_PLAYER_IN_SEAT_AREA(sRouletteData, iSeatId, PLAYER_ID()) 
ENDFUNC

/// PURPOSE:
///    Checks if the local player is alone in the seat area    
FUNC BOOL IS_PLAYER_ALONE_IN_SEATING_AREA(ROULETTE_DATA &sRouletteData, INT iSeatId)
	INT i
	REPEAT NUM_NETWORK_PLAYERS i
		PLAYER_INDEX playerID = INT_TO_PLAYERINDEX(i)
		
		IF IS_NET_PLAYER_OK(playerID)
		AND NOT NETWORK_IS_PLAYER_CONCEALED(playerID)
			IF playerID != PLAYER_ID()
				PED_INDEX pPed = GET_PLAYER_PED(playerID)
				
				IF DOES_ENTITY_EXIST(pPed)
				AND IS_PLAYER_IN_SEAT_AREA(sRouletteData, iSeatId, playerID)
				AND sRouletteData.serverBD.playersUsingSeats[iSeatID] = playerID					
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Checks if the player is in the seat area and facing the seat  
FUNC BOOL HAS_LOCAL_PLAYER_APPROACHED_VALID_SEAT(ROULETTE_DATA &sRouletteData, INT iSeatID)
	RETURN IS_LOCAL_PLAYER_IN_SEAT_AREA(sRouletteData, iSeatID) AND IS_PLAYER_FACING_SEAT(sRouletteData, iSeatID)
ENDFUNC

/// PURPOSE:
///    Releases any seats that players have left
PROC ROULETTE_SEATS_RELEASE_EMPTY_SEATS(ROULETTE_DATA &sRouletteData)
	
	INT iMaxSeat = COUNT_OF(sRouletteData.serverBD.playersUsingSeats)
		
	INT iSeatID = 0
	REPEAT ROULETTE_SEAT_REQUEST_MAX_SEATS_CHECKED_PER_FRAME iSeatID		
		sRouletteData.iSeatBeingReleased++
		sRouletteData.iSeatBeingReleased = WRAP_INDEX(sRouletteData.iSeatBeingReleased, iMaxSeat)
		
		PLAYER_INDEX piPlayerInSeat = sRouletteData.serverBD.playersUsingSeats[sRouletteData.iSeatBeingReleased]
		
		IF piPlayerInSeat = INVALID_PLAYER_INDEX()
			RELOOP
		ENDIF
		
		IF ROULETTE_HELPER_IS_PLAYER_AT_SEAT_STILL_PLAYING(sRouletteData, sRouletteData.iSeatBeingReleased)
			RELOOP
		ENDIF
		
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_SEATS_RELEASE_EMPTY_SEATS - Removing ", GET_PLAYER_NAME(piPlayerInSeat), " from seat ", sRouletteData.iSeatBeingReleased)	
		sRouletteData.serverBD.playersUsingSeats[sRouletteData.iSeatBeingReleased] = INVALID_PLAYER_INDEX()
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Lets player use requested seat
PROC ROULETTE_SEATS_PROCESS_USE_REQUESTS(ROULETTE_DATA &sRouletteData)
	INT iPlayer = 0
	INT iChecksThisFrame
	REPEAT ROULETTE_SEAT_REQUEST_MAX_SEATS_CHECKED_PER_FRAME iChecksThisFrame	
		
		sRouletteData.iProcessedPlayerID++
		sRouletteData.iProcessedPlayerID = WRAP_INDEX(sRouletteData.iProcessedPlayerID, NUM_NETWORK_PLAYERS)
		iPlayer = sRouletteData.iProcessedPlayerID
		
		INT iRequestedSeat = sRouletteData.playerBD[iPlayer].iSeatID
		
		// No request
		IF NOT ROULETTE_TABLE_IS_SEAT_ID_VALID(iRequestedSeat)
			RELOOP
		ENDIF
		
		PLAYER_INDEX piRequestingPlayer = INT_TO_PLAYERINDEX(iPlayer)
		PLAYER_INDEX piPlayerAtRequestedSeat = sRouletteData.serverBD.playersUsingSeats[iRequestedSeat]
		
		IF piRequestingPlayer = piPlayerAtRequestedSeat
			RELOOP
		ENDIF
		
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_SEATS_PROCESS_USE_REQUESTS - Player ",
		GET_PLAYER_NAME(piRequestingPlayer), " requested seat ", iRequestedSeat)	
		
		IF piPlayerAtRequestedSeat != INVALID_PLAYER_INDEX()
			CPRINTLN(DEBUG_ROULETTE, "ROULETTE_SEATS_PROCESS_USE_REQUESTS - Cannot authorise use of seat - seat in use by ",
			GET_PLAYER_NAME(piPlayerAtRequestedSeat))
			RELOOP
		ENDIF
				
		IF NOT IS_NET_PLAYER_OK(piRequestingPlayer)
			CPRINTLN(DEBUG_ROULETTE, "ROULETTE_SEATS_PROCESS_USE_REQUESTS - REquesting player is not ok to use seat")
			RELOOP
		ENDIF
		
		INT iTableID = ROULETTE_HELPER_GET_TABLE_ID_FROM_SEAT_ID(iRequestedSeat)
		IF NOT ROULETTE_TABLE_IS_TABLE_ID_VALID(iTableID)
			CPRINTLN(DEBUG_ROULETTE, "ROULETTE_SEATS_PROCESS_USE_REQUESTS - iTableId invalid")
			RELOOP
		ENDIF
			
		sRouletteData.serverBD.playersUsingSeats[iRequestedSeat] = piRequestingPlayer
		
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_SEATS_PROCESS_USE_REQUESTS - Allowing player ",
		GET_PLAYER_NAME(piRequestingPlayer),
		" use of seat ", iRequestedSeat,
		" at table ", iTableID)
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Processes seat request to keep track of available seats
PROC ROULETTE_SEATS_MAINTAIN_REQUESTS(ROULETTE_DATA &sRouletteData)
	ROULETTE_SEATS_PROCESS_USE_REQUESTS(sRouletteData)
	ROULETTE_SEATS_RELEASE_EMPTY_SEATS(sRouletteData)	
ENDPROC

/// PURPOSE:
///    Requests the server for a specified seat at the roulette table
PROC ROULETTE_REQUEST_SEAT(ROULETTE_DATA &sRouletteData)
	sRouletteData.playerBD[NATIVE_TO_INT(PLAYER_ID())].iSeatID = sRouletteData.iSeatID
	sRouletteData.playerBD[NATIVE_TO_INT(PLAYER_ID())].iTableID = sRouletteData.iTableID
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_REQUEST_SEAT - Requested seat: ", sRouletteData.iSeatID,
	" at table: ", sRouletteData.iTableID)
ENDPROC

/// PURPOSE:
///    Cancels the acitve seat request
PROC ROULETTE_CANCEL_SEAT_REQUEST(ROULETTE_DATA &sRouletteData)
	sRouletteData.playerBD[NATIVE_TO_INT(PLAYER_ID())].iSeatID = -1
	sRouletteData.playerBD[NATIVE_TO_INT(PLAYER_ID())].iTableID = -1
ENDPROC

/// PURPOSE:
///    Checks if the server has approved the active seat request 
FUNC BOOL ROULETTE_HAS_SEAT_BEEN_APPROVED_FOR_USE(ROULETTE_DATA &sRouletteData, INT iSeatID)
	RETURN (sRouletteData.serverBD.playersUsingSeats[iSeatID] = PLAYER_ID())
ENDFUNC
