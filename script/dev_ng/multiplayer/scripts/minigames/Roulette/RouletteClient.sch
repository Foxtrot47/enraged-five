//////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Name:        RouletteClient.sch																			
/// Description: Header for running the roulette clients overall state, i.e. handles player joining and 		
///				leaving a table. As well as keeping the table up to date with on going games so they 		
///				can be observed																				
///																											
/// Written by:  Online Technical Team: Tom Turner,															
/// Date:  		12/02/2019																					
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "RouletteData.sch"
USING "RouletteClientState.sch"
USING "RouletteHelper.sch"
USING "RouletteLayout.sch"
USING "RouletteGameClient.sch"
USING "RouletteAnimation.sch"
USING "RouletteCamera.sch"
USING "RouletteTable.sch"
USING "RouletteSeatRequesting.sch"
USING "RouletteMetrics.sch"

CONST_FLOAT ROULETTE_GO_TO_COORD_TARGET_RADIUS 0.35
CONST_FLOAT ROULETTE_SEAT_ENTRY_POSITION_RADIUS 0.45

/// PURPOSE:
///    Sets a new state for the client, will display a state change log
PROC ROULETTE_CLIENT_SET_STATE(ROULETTE_DATA& sRouletteData, ROULETTE_CLIENT_STATE eNextState, ROULETTE_TRANSITION_FUNC onExit, ROULETTE_TRANSITION_FUNC onEnter)
	CPRINTLN(DEBUG_ROULETTE, "")
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_CLIENT_SET_STATE - State changed, ",
	ROULETTE_CLIENT_STATE_GET_STATE_NAME(sRouletteData.eClientState),
	" -> ", ROULETTE_CLIENT_STATE_GET_STATE_NAME(eNextState))
	
	CALL onExit(sRouletteData)
	sRouletteData.eClientState = eNextState
	CALL onEnter(sRouletteData)
ENDPROC

/// PURPOSE:
///    Checks if the player is currently blocked from joining the table    
FUNC BOOL ROULETTE_CLIENT_IS_PLAYER_BLOCKED_FROM_JOINING_TABLE(ROULETTE_DATA &sRouletteData)
	INT iTableMinBet = ROULETTE_BETTING_GET_MIN_BET_FOR_TABLE(sRouletteData.iTableID)
	
	IF ROULETTE_TABLE_IS_DISABLED(sRouletteData.iTableID)
		RETURN TRUE
	ENDIF	
	
	INT iEndReason = GET_HASH_KEY("none")
	RETURN ROULETTE_DISPLAY_ANY_UNAVAILABLE_MESSAGE(sRouletteData.iSeatID, sRouletteData.iTableID,
	iTableMinBet, iEndReason, FALSE)	
ENDFUNC

/// PURPOSE:
///    Sets if the player controls are neabled and makes player kinematic
PROC ROULETTE_CLIENT_SET_PLAYER_CONTROLS(BOOL bEnabled)
	NET_SET_PLAYER_CONTROL(PLAYER_ID(), bEnabled, NSPC_LEAVE_CAMERA_CONTROL_ON)
	SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_UseKinematicModeWhenStationary, NOT bEnabled)
ENDPROC

/// PURPOSE:
///    Clean up for the roulette client
PROC ROULETTE_CLIENT_CLEANUP(ROULETTE_DATA &sRouletteData)
	
	RELEASE_CONTEXT_INTENTION(sRouletteData.iContextIntention)
	sRouletteData.iContextIntention = NEW_CONTEXT_INTENTION		
	
	// Just in case cleanup triggered mid game
	ROULETTE_HELPER_GLOBAL_SET_LOCAL_PLAYER_PLAYING_ROULETTE(FALSE)
	
	IF ROULETTE_HELPER_IS_SAFE_TO_ENABLE_PLAYER_CONTROLS(sRouletteData)	
		ROULETTE_CLIENT_SET_PLAYER_CONTROLS(TRUE)
	ENDIF
	
	ROULETTE_AUDIO_REMOVE_CURRENT_TABLES_DEALER_FROM_TABLE_MIX_GROUP(sRouletteData)
	ROULETTE_AUDIO_STOP_TABLE_SCENE()
	ENABLE_INTERACTION_MENU()
	
	// Only clean up game if we started it
	IF sRouletteData.eClientState = ROULETTE_CLIENT_STATE_PLAYING
		ROULETTE_GAME_CLIENT_CLEAN_UP(sRouletteData)
	ENDIF
	
	ROULETTE_CAMERA_CLEAN_UP(sRouletteData)
	ROULETTE_CANCEL_SEAT_REQUEST(sRouletteData)
	
	INT i = 0
	REPEAT ROULETTE_TABLE_MAX_NUMBER_OF_TABLES i
		ROULETTE_ANIM_WHEEL_CLEAN_UP(sRouletteData.sSpin[i])
	ENDREPEAT
	
	// Clean up anims first as they may be using props
	ROULETTE_ANIM_REMOVE_ALL_CLIENT_DICTIONARIES()
	ROULETTE_PROPS_CLEAN_UP(sRouletteData)	
	CLEAR_HELP()
ENDPROC

///------------------------------------    
///	Client - On Entrys
///------------------------------------    

/// PURPOSE:
///    On enter for client prompt state
PROC ROULETTE_CLIENT_STATE_PROMPT_ON_ENTER(ROULETTE_DATA &sRouletteData)
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_CLIENT_STATE_PROMPT_ON_ENTER - Called")	
	
	IF ROULETTE_CLIENT_IS_PLAYER_BLOCKED_FROM_JOINING_TABLE(sRouletteData)
		EXIT	
	ENDIF
		
	ROULETTE_UI_REGISTER_START_PROMPT(sRouletteData)
ENDPROC

/// PURPOSE:
///    On enter for disclosure state
PROC ROULETTE_CLIENT_DISCLOSURE_ON_ENTER(ROULETTE_DATA &sRouletteData)
	UNUSED_PARAMETER(sRouletteData)
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_CLIENT_DISCLOSURE_ON_ENTER - Called")
	ROULETTE_CLIENT_SET_PLAYER_CONTROLS(FALSE)
	DISABLE_INTERACTION_MENU()	
	ROULETTE_HELPER_GLOBAL_SET_LOCAL_PLAYER_PLAYING_ROULETTE(TRUE)
ENDPROC

/// PURPOSE:
///    On enter for client wait approval state
PROC ROULETTE_CLIENT_STATE_WAIT_FOR_APPROVAL_ON_ENTER(ROULETTE_DATA &sRouletteData)
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_CLIENT_STATE_WAIT_FOR_APPROVAL_ON_ENTER - Called")	
	ROULETTE_CLIENT_SET_PLAYER_CONTROLS(FALSE)
	RESET_NET_TIMER(sRouletteData.stSeatRequestTimeOut)
	ROULETTE_REQUEST_SEAT(sRouletteData)
ENDPROC

/// PURPOSE:
///    On enter for moving to seat state
PROC ROULETTE_CLIENT_STATE_MOVING_TO_SEAT_ON_ENTER(ROULETTE_DATA &sRouletteData)
	UNUSED_PARAMETER(sRouletteData)
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_CLIENT_STATE_JOINING_TABLE_ON_ENTER - Called")	
	
	// 	Load dictionaries now so we can get the start position for the seat enter animation
	ROULETTE_ANIM_LOAD_PLAYER_DICTIONARIES()
	CLEAR_BIT(sRouletteData.iRouletteBS, ROULETTE_BS_HAS_MOVE_TO_SEAT_TASK_STARTED)
ENDPROC

/// PURPOSE:
///    On enter for client join state
PROC ROULETTE_CLIENT_STATE_JOINING_TABLE_ON_ENTER(ROULETTE_DATA &sRouletteData)
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_CLIENT_STATE_JOINING_TABLE_ON_ENTER - Called")
	SET_TABLE_GAMES_CAMERA_THIS_UPDATE(TABLE_GAMES_CAMERA_TYPE_ROULETTE)
	ROULETTE_ANIM_PLAY_ENTER_SEAT_ANIM(sRouletteData)
	CLEAR_BIT(sRouletteData.iRouletteBS, ROULETTE_BS_HAS_DEALER_GREETED_PLAYER)
	CLEAR_BIT(sRouletteData.playerBD[NATIVE_TO_INT(PLAYER_ID())].iPlayerBS, ROULETTE_PLAYER_BS_FINISHED_JOINING_TABLE)
ENDPROC

/// PURPOSE:
///    On enter for client play state
PROC ROULETTE_CLIENT_PLAYING_ON_ENTER(ROULETTE_DATA &sRouletteData)
	UNUSED_PARAMETER(sRouletteData)
	ROULETTE_GAME_CLIENT_START_GAME(sRouletteData)
	ROULETTE_AUDIO_START_TABLE_SCENE()
	SET_BIT(sRouletteData.playerBD[NATIVE_TO_INT(PLAYER_ID())].iPlayerBS, ROULETTE_PLAYER_BS_FINISHED_JOINING_TABLE)
	ROULETTE_AUDIO_ADD_CURRENT_TABLES_DEALER_TO_TABLE_MIX_GROUP(sRouletteData)
ENDPROC

/// PURPOSE:
///    On enter for leaving table state
PROC ROULETTE_CLIENT_STATE_LEAVING_TABLE_ON_ENTER(ROULETTE_DATA &sRouletteData)
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_CLIENT_STATE_LEAVING_TABLE_ON_ENTER - Called")	
	ROULETTE_CLIENT_SET_PLAYER_CONTROLS(FALSE)
	ROULETTE_ANIM_PLAY_EXIT_SEAT_ANIM(sRouletteData)
	ROULETTE_AUDIO_REMOVE_CURRENT_TABLES_DEALER_FROM_TABLE_MIX_GROUP(sRouletteData)
	ROULETTE_AUDIO_STOP_TABLE_SCENE()
	CLEAR_BIT(sRouletteData.iRouletteBS, ROULETTE_BS_HAS_DEALER_SAID_LEAVE_TABLE_AUDIO)
ENDPROC


///------------------------------------    
///	Client - On Exits
///------------------------------------    

/// PURPOSE:
///    On exit for client prompt state
PROC ROULETTE_CLIENT_STATE_PROMPT_ON_EXIT(ROULETTE_DATA &sRouletteData)
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_CLIENT_STATE_PROMPT_ON_EXIT - Called")
	RELEASE_CONTEXT_INTENTION(sRouletteData.iContextIntention)
	sRouletteData.iContextIntention = NEW_CONTEXT_INTENTION
	CLEAR_HELP()
ENDPROC

/// PURPOSE:
///    On exit for client wait for approval state
PROC ROULETTE_CLIENT_STATE_DISCLOSURE_ON_EXIT(ROULETTE_DATA &sRouletteData)
	UNUSED_PARAMETER(sRouletteData)
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_CLIENT_STATE_DISCLOSURE_ON_EXIT - Called")
	ROULETTE_CLIENT_SET_PLAYER_CONTROLS(TRUE)
	ROULETTE_HELPER_GLOBAL_SET_LOCAL_PLAYER_PLAYING_ROULETTE(FALSE)
	ENABLE_INTERACTION_MENU()
ENDPROC

/// PURPOSE:
///    On exit for client wait for approval state
PROC ROULETTE_CLIENT_STATE_WAIT_FOR_APPROVAL_ON_EXIT(ROULETTE_DATA &sRouletteData)
	UNUSED_PARAMETER(sRouletteData)
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_CLIENT_STATE_WAIT_FOR_APPROVAL_ON_EXIT - Called")
	ROULETTE_CLIENT_SET_PLAYER_CONTROLS(TRUE)
	ROULETTE_HELPER_GLOBAL_SET_LOCAL_PLAYER_PLAYING_ROULETTE(FALSE)
	ROULETTE_CANCEL_SEAT_REQUEST(sRouletteData)
ENDPROC

/// PURPOSE:
///    On exit for joining the table
PROC ROULETTE_CLIENT_STATE_JOINING_TABLE_ON_EXIT(ROULETTE_DATA &sRouletteData)
	UNUSED_PARAMETER(sRouletteData)
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_CLIENT_STATE_JOINING_TABLE_ON_EXIT - Called")
	IF IS_GAMEPLAY_HINT_ACTIVE()
		STOP_GAMEPLAY_HINT()
	ENDIF
		
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_CLIENT_STATE_JOINING_TABLE_UPDATE - Gameplay hint active: ", IS_GAMEPLAY_HINT_ACTIVE())		
ENDPROC

/// PURPOSE:
///    On exit for the leaving table state
PROC ROULETTE_CLIENT_STATE_LEAVING_TABLE_ON_EXIT(ROULETTE_DATA &sRouletteData)
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_CLIENT_STATE_LEAVING_TABLE_ON_EXIT - Called")	
	ROULETTE_ANIM_STOP_PLAYER_SYNC_SCENE(sRouletteData)
	ROULETTE_CANCEL_SEAT_REQUEST(sRouletteData)
	ROULETTE_HELPER_GLOBAL_SET_LOCAL_PLAYER_PLAYING_ROULETTE(FALSE)
	ROULETTE_CLIENT_SET_PLAYER_CONTROLS(TRUE)
	ENABLE_INTERACTION_MENU()
	
	IF IS_GAMEPLAY_HINT_ACTIVE()	
		STOP_GAMEPLAY_HINT()
	ENDIF
	
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_CLIENT_STATE_LEAVING_TABLE_ON_EXIT - Gameplay hint active: ", IS_GAMEPLAY_HINT_ACTIVE())
	
	// Need to manually stop anim
	CLEAR_PED_TASKS(PLAYER_PED_ID())
	ROULETTE_ANIM_REMOVE_PLAYER_DICTIONARIES()
	CLEAR_BIT(sRouletteData.iRouletteBS, ROULETTE_BS_HAS_DEALER_SAID_LEAVE_TABLE_AUDIO)
	CLEAR_BIT(sRouletteData.playerBD[NATIVE_TO_INT(PLAYER_ID())].iPlayerBS, ROULETTE_PLAYER_BS_FINISHED_JOINING_TABLE)
ENDPROC

/// PURPOSE:
///    Checks if the player can play a game of roulette   
FUNC BOOL ROULETTE_CLIENT_CAN_PLAYER_PLAY_ROULETTE()
	RETURN IS_NET_PLAYER_OK(PLAYER_ID())
	AND IS_PLAYER_CONTROL_ON(PLAYER_ID())
	AND NOT IS_ANY_INTERACTION_ANIM_PLAYING()
	AND NOT IS_PAUSE_MENU_ACTIVE_EX()
	AND NOT IS_INTERACTION_MENU_OPEN()
	AND Get_Peds_Drunk_Alcohol_Hit_Count(PLAYER_PED_ID()) <= 9
	AND NOT g_SpawnData.bPassedOutDrunk
	AND NOT IS_PED_RAGDOLL(PLAYER_PED_ID())
	AND NOT IS_TRANSITION_SESSION_LAUNCHING()
	AND NOT AM_I_TRANSITION_SESSIONS_STARTING_ON_CALL()
	AND NOT IS_PLAYER_IS_STARTING_ON_CALL(PLAYER_ID())
	AND NOT IS_SCREEN_FADED_OUT()
	AND NOT IS_SCREEN_FADING_OUT()
	AND NOT g_OfficeHeliDockData.bBossWantsToTakeoff
	AND g_iSimpleInteriorState != SIMPLE_INT_STATE_HELI_TAKEOFF_EXIT
ENDFUNC

/// PURPOSE:
///    Update state for client locate
PROC ROULETTE_CLIENT_STATE_LOCATE_UPDATE(ROULETTE_DATA &sRouletteData)		
	INT i = 0
	REPEAT ROULETTE_SEAT_REQUEST_MAX_SEATS_CHECKED_PER_FRAME i
		// Staggered loop, get next seat to check
		sRouletteData.iSeatID++
		sRouletteData.iSeatID = ((sRouletteData.iSeatID % ROULETTE_MAX_SEAT_ID) + ROULETTE_MAX_SEAT_ID) % ROULETTE_MAX_SEAT_ID
		sRouletteData.iTableID = ROULETTE_HELPER_GET_TABLE_ID_FROM_SEAT_ID(sRouletteData.iSeatID)
		
		// If in current seat being checked, transition to prompt
		IF HAS_LOCAL_PLAYER_APPROACHED_VALID_SEAT(sRouletteData, sRouletteData.iSeatID)
			IF ROULETTE_CLIENT_CAN_PLAYER_PLAY_ROULETTE()
				ROULETTE_CLIENT_SET_STATE(sRouletteData, ROULETTE_CLIENT_STATE_PROMPT,
				&ROULETTE_DEFAULT_ON_EXIT, &ROULETTE_CLIENT_STATE_PROMPT_ON_ENTER)
				EXIT
			ENDIF
		ENDIF	
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Update state for client prompt
PROC ROULETTE_CLIENT_STATE_PROMPT_UPDATE(ROULETTE_DATA &sRouletteData)	
	BOOL bSeatIsFree = IS_PLAYER_ALONE_IN_SEATING_AREA(sRouletteData, sRouletteData.iSeatID) AND NOT ROULETTE_HELPER_IS_PLAYER_AT_SEAT_STILL_PLAYING(sRouletteData, sRouletteData.iSeatID)
	
	IF NOT bSeatIsFree
		PRINT_HELP_NO_SOUND("CAS_MG_SEAT_IN_USE")
	ENDIF
	
	IF NOT HAS_LOCAL_PLAYER_APPROACHED_VALID_SEAT(sRouletteData, sRouletteData.iSeatID)
	OR NOT ROULETTE_CLIENT_CAN_PLAYER_PLAY_ROULETTE()
		ROULETTE_CLIENT_SET_STATE(sRouletteData, ROULETTE_CLIENT_STATE_LOCATE,
		&ROULETTE_CLIENT_STATE_PROMPT_ON_EXIT, &ROULETTE_DEFAULT_ON_ENTER)
		EXIT
	ENDIF
	
	BOOL bBlockedByMessage = ROULETTE_CLIENT_IS_PLAYER_BLOCKED_FROM_JOINING_TABLE(sRouletteData) 
	
	// Sometimes we need to re-add the context if a message blocked it 
	IF NOT bBlockedByMessage
		IF NOT ROULETTE_UI_IS_START_PROMPT_SHOWING(sRouletteData)
			ROULETTE_UI_REGISTER_START_PROMPT(sRouletteData)
		ENDIF
	ENDIF
	
	IF NOT bBlockedByMessage
	AND bSeatIsFree
	AND HAS_CONTEXT_BUTTON_TRIGGERED(sRouletteData.iContextIntention)		
		ROULETTE_CLIENT_SET_STATE(sRouletteData, ROULETTE_CLIENT_STATE_DISCLOSURE,
		&ROULETTE_CLIENT_STATE_PROMPT_ON_EXIT, &ROULETTE_CLIENT_DISCLOSURE_ON_ENTER)
	ENDIF
ENDPROC

/// PURPOSE:
///    Update state for disclosure screen
PROC ROULETTE_CLIENT_STATE_DISCLOSURE_UPDATE(ROULETTE_DATA &sRouletteData)
	ROULETTE_UI_DISCLOSURE_STATUS eStatus = ROULETTE_UI_MAINTAIN_DISCLOSURE(sRouletteData)	
	
	IF eStatus = ROULETTE_UI_DISCLOSURE_ACCEPTED
		ROULETTE_CLIENT_SET_STATE(sRouletteData, ROULETTE_CLIENT_STATE_WAIT_FOR_APPROVAL,
		&ROULETTE_DEFAULT_ON_EXIT, &ROULETTE_CLIENT_STATE_WAIT_FOR_APPROVAL_ON_ENTER)
		EXIT
	ENDIF
	
	IF eStatus = ROULETTE_UI_DISCLOSURE_DECLINED
		// Use on exit to re-enable controls
		ROULETTE_CLIENT_SET_STATE(sRouletteData, ROULETTE_CLIENT_STATE_LOCATE,
		&ROULETTE_CLIENT_STATE_DISCLOSURE_ON_EXIT, &ROULETTE_DEFAULT_ON_ENTER)
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks if the seat currently requested by the player is in use by another player  
FUNC BOOL ROULETTE_CLIENT_WAS_REQUESTED_SEAT_STOLEN_BY_ANOTHER_PLAYER(ROULETTE_DATA &sRouletteData)
	INT iSeatID = sRouletteData.iSeatID
	IF iSeatID < 0
		RETURN FALSE
	ENDIF
	
	RETURN sRouletteData.serverBD.playersUsingSeats[iSeatID] != INVALID_PLAYER_INDEX()
	AND sRouletteData.serverBD.playersUsingSeats[iSeatID] != GET_PLAYER_INDEX()
ENDFUNC

/// PURPOSE:
///    Update state for wait for approval
PROC ROULETTE_CLIENT_STATE_WAIT_FOR_APPROVAL_UPDATE(ROULETTE_DATA &sRouletteData)	
	ROULETTE_HELPER_HIDE_GAMEPLAY_UI_THIS_FRAME()
	
	IF HAS_NET_TIMER_EXPIRED(sRouletteData.stSeatRequestTimeOut, ROULETTE_SEAT_REQUEST_TIME_OUT)
	OR ROULETTE_CLIENT_WAS_REQUESTED_SEAT_STOLEN_BY_ANOTHER_PLAYER(sRouletteData)
		ROULETTE_CLIENT_SET_STATE(sRouletteData, ROULETTE_CLIENT_STATE_PROMPT,
		&ROULETTE_CLIENT_STATE_WAIT_FOR_APPROVAL_ON_EXIT, &ROULETTE_CLIENT_STATE_PROMPT_ON_ENTER)
		EXIT
	ENDIF
	
	IF ROULETTE_HAS_SEAT_BEEN_APPROVED_FOR_USE(sRouletteData, sRouletteData.iSeatID) 
		// Enter move to seat state
		ROULETTE_CLIENT_SET_STATE(sRouletteData, ROULETTE_CLIENT_STATE_MOVING_TO_SEAT,
		&ROULETTE_DEFAULT_ON_EXIT, &ROULETTE_CLIENT_STATE_MOVING_TO_SEAT_ON_ENTER)
	ENDIF
ENDPROC

/// PURPOSE:
///    Makes the local player walk to their assigned seats 'enter anim' start location
PROC ROULETTE_CLIENT_WALK_LOCAL_PLAYER_TO_ASSIGNED_SEAT(ROULETTE_DATA &sRouletteData)
	INT iSeatID = sRouletteData.iSeatID
	
	sRouletteData.eSeatEnterClipToUse = ROULETTE_ANIM_GET_APPROPRIATE_SEAT_ENTRY_CLIP(sRouletteData, iSeatID)
	
	// Get start position depending on which side of the seat is blocked
	VECTOR vEnterSeatAnimStartPos = ROULETTE_ANIM_GET_SHARED_ANIM_START_POSITION(sRouletteData, iSeatID, sRouletteData.eSeatEnterClipToUse)
	VECTOR vEnterSeatAnimStartRot = ROULETTE_ANIM_GET_SHARED_ANIM_START_ROTATION(sRouletteData, iSeatID, sRouletteData.eSeatEnterClipToUse)
	
	// Make the local player go to the starting position for roulette
	TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), vEnterSeatAnimStartPos,
	PEDMOVE_WALK, 5000, vEnterSeatAnimStartRot.z, ROULETTE_GO_TO_COORD_TARGET_RADIUS)
ENDPROC

/// PURPOSE:
///    Calculates a look target for the current chair   
FUNC VECTOR ROULETTE_CLIENT_GET_CURRENT_SEAT_ENTRY_LOOK_TARGET(ROULETTE_DATA &sRouletteData)
	VECTOR vLookTarget = ROULETTE_PROPS_GET_SEAT_BONE_POSITION(sRouletteData, sRouletteData.iSeatID)
	VECTOR vBoneRotation = ROULETTE_PROPS_GET_SEAT_BONE_ROTATION(sRouletteData, sRouletteData.iSeatID)
	VECTOR vOffset = <<0, 1, 1>>
	vLookTarget += ROTATE_VECTOR_ABOUT_Z(vOffset, vBoneRotation.z)
	RETURN vLookTarget
ENDFUNC

/// PURPOSE:
///    Maintains setting a gameplay hint while in first person cam so we are looking at the correct
///    position when entering our seat
PROC ROULETTE_CLIENT_MAINTAIN_FIRST_PERSON_CAMERA_HINT(ROULETTE_DATA &sRouletteData)
	IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
	AND NOT IS_GAMEPLAY_HINT_ACTIVE()
		SET_GAMEPLAY_COORD_HINT(ROULETTE_CLIENT_GET_CURRENT_SEAT_ENTRY_LOOK_TARGET(sRouletteData))
	ENDIF
ENDPROC

/// PURPOSE:
///    Update state for moving to seat update
PROC ROULETTE_CLIENT_STATE_MOVING_TO_SEAT_UPDATE(ROULETTE_DATA &sRouletteData)
	ROULETTE_HELPER_HIDE_GAMEPLAY_UI_THIS_FRAME()
	
	IF NOT ROULETTE_ANIM_HAVE_PLAYER_DICTIONARIES_LOADED()
		EXIT
	ENDIF
	
	SCRIPTTASKSTATUS eMoveToStatus = GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD)
	BOOL bMoveTaskFinished = (eMoveToStatus = FINISHED_TASK AND IS_BIT_SET(sRouletteData.iRouletteBS, ROULETTE_BS_HAS_MOVE_TO_SEAT_TASK_STARTED))
	VECTOR vStartPosition = ROULETTE_ANIM_GET_SHARED_ANIM_START_POSITION(sRouletteData, sRouletteData.iSeatID, sRouletteData.eSeatEnterClipToUse)
	BOOL bIsInPosition = bMoveTaskFinished OR IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), vStartPosition, ROULETTE_SEAT_ENTRY_POSITION_RADIUS)
	
	IF bIsInPosition
		ROULETTE_CLIENT_SET_STATE(sRouletteData, ROULETTE_CLIENT_STATE_JOINING_TABLE,
		&ROULETTE_DEFAULT_ON_EXIT, &ROULETTE_CLIENT_STATE_JOINING_TABLE_ON_ENTER)
		EXIT
	ENDIF	
	
	ROULETTE_CLIENT_MAINTAIN_FIRST_PERSON_CAMERA_HINT(sRouletteData)
	
	// If task not started, start it
	IF NOT IS_BIT_SET(sRouletteData.iRouletteBS, ROULETTE_BS_HAS_MOVE_TO_SEAT_TASK_STARTED)	
		ROULETTE_CLIENT_WALK_LOCAL_PLAYER_TO_ASSIGNED_SEAT(sRouletteData)
		SET_BIT(sRouletteData.iRouletteBS, ROULETTE_BS_HAS_MOVE_TO_SEAT_TASK_STARTED)
	ENDIF
ENDPROC

/// PURPOSE:
///    Update state for joining the table
PROC ROULETTE_CLIENT_STATE_JOINING_TABLE_UPDATE(ROULETTE_DATA &sRouletteData)
	ROULETTE_HELPER_HIDE_GAMEPLAY_UI_THIS_FRAME()
	SET_TABLE_GAMES_CAMERA_THIS_UPDATE(TABLE_GAMES_CAMERA_TYPE_ROULETTE)
	
	// Greet player if we haven't
	IF NOT IS_BIT_SET(sRouletteData.iRouletteBS, ROULETTE_BS_HAS_DEALER_GREETED_PLAYER)
	AND ROULETTE_ANIM_HAS_CURRENT_PLAYER_ANIM_FINISHED(sRouletteData, 0.15)
	AND ROULETTE_AUDIO_PLAY_DEALER_GREET_PLAYER(sRouletteData)		
		SET_BIT(sRouletteData.iRouletteBS, ROULETTE_BS_HAS_DEALER_GREETED_PLAYER)
	ENDIF
	
	IF ROULETTE_ANIM_HAS_SHARED_PLAYER_EVENT_TRIGGERED(sRouletteData, GET_HASH_KEY("SHOWUI"))
	OR ROULETTE_ANIM_HAS_CURRENT_PLAYER_ANIM_FINISHED(sRouletteData)
		ROULETTE_CLIENT_SET_STATE(sRouletteData, ROULETTE_CLIENT_STATE_PLAYING,
		&ROULETTE_CLIENT_STATE_JOINING_TABLE_ON_EXIT, &ROULETTE_CLIENT_PLAYING_ON_ENTER)
	ENDIF
ENDPROC

/// PURPOSE:
///    Update state for playing the game
PROC ROULETTE_CLIENT_STATE_PLAYING_UPDATE(ROULETTE_DATA &sRouletteData)		
	ROULETTE_HELPER_HIDE_GAMEPLAY_UI_THIS_FRAME()
	SET_TABLE_GAMES_CAMERA_THIS_UPDATE(TABLE_GAMES_CAMERA_TYPE_ROULETTE)
	
	IF NOT ROULETTE_GAME_CLIENT_UPDATE(sRouletteData)
		ROULETTE_CLIENT_SET_STATE(sRouletteData, ROULETTE_CLIENT_STATE_LEAVING_TABLE,
		&ROULETTE_DEFAULT_ON_EXIT, &ROULETTE_CLIENT_STATE_LEAVING_TABLE_ON_ENTER)
	ENDIF
ENDPROC

/// PURPOSE:
///    Plays appropriate leave audio based on the last end reason,
///    will not play if ROULETTE_BS_HAS_DEALER_SAID_LEAVE_TABLE_AUDIO is set
PROC ROULETTE_CLIENT_PLAY_LEAVE_TABLE_AUDIO_IF_NEEDED(ROULETTE_DATA &sRouletteData)
	IF IS_BIT_SET(sRouletteData.iRouletteBS, ROULETTE_BS_HAS_DEALER_SAID_LEAVE_TABLE_AUDIO)
		EXIT
	ENDIF
	
	SWITCH sRouletteData.iFinalEndReason
		CASE HASH("win cutoff")
		CASE HASH("loss cutoff")
		CASE HASH("time cutoff")
			IF NOT ROULETTE_AUDIO_PLAY_DEALER_REFUSE_BETS_CLIP(sRouletteData)
			 	EXIT
			ENDIF
		BREAK
		CASE HASH("quit")
		 	IF NOT ROULETTE_AUDIO_PLAY_DEALER_LEAVE_CLIP(sRouletteData)
		 		EXIT
			ENDIF
		BREAK
	ENDSWITCH
	
	SET_BIT(sRouletteData.iRouletteBS, ROULETTE_BS_HAS_DEALER_SAID_LEAVE_TABLE_AUDIO)
ENDPROC

/// PURPOSE:
///    Update state for leaving the table
PROC ROULETTE_CLIENT_STATE_LEAVING_TABLE_UPDATE(ROULETTE_DATA &sRouletteData)
	ROULETTE_HELPER_HIDE_GAMEPLAY_UI_THIS_FRAME()
	
	ROULETTE_CLIENT_PLAY_LEAVE_TABLE_AUDIO_IF_NEEDED(sRouletteData)
	
	VECTOR vLeftInput
	vLeftInput = <<GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_X), GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y), 0.0>>
				
	IF (HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(), GET_HASH_KEY("BREAK_OUT")) AND (VMAG(vLeftInput) >= 0.24)) 
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_LEAVING_TABLE - Finished leaving table - early break out")
		
		// Back to prompt state
		ROULETTE_CLIENT_SET_STATE(sRouletteData, ROULETTE_CLIENT_STATE_PROMPT,
		&ROULETTE_CLIENT_STATE_LEAVING_TABLE_ON_EXIT, &ROULETTE_CLIENT_STATE_PROMPT_ON_ENTER)
		EXIT
	ENDIF
	
	IF ROULETTE_ANIM_HAS_CURRENT_PLAYER_ANIM_FINISHED(sRouletteData)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_LEAVING_TABLE - Finished leaving table - animation finished")
	
		// Back to prompt state
		ROULETTE_CLIENT_SET_STATE(sRouletteData, ROULETTE_CLIENT_STATE_LOCATE,
		&ROULETTE_CLIENT_STATE_LEAVING_TABLE_ON_EXIT, &ROULETTE_DEFAULT_ON_ENTER)
	ENDIF
ENDPROC

/// PURPOSE:
///    Maintains the clients dealers
PROC ROULETTE_CLIENT_MAINTAIN_DEALER_AT_TABLE(ROULETTE_DATA &sRouletteData, INT iTableID)
	
	IF ROULETTE_TABLE_IS_DISABLED(iTableID)
		EXIT
	ENDIF
	
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(sRouletteData.serverBD.niDealerPed[iTableID])
		EXIT
	ENDIF
	
	PED_INDEX piDealer = NET_TO_PED(sRouletteData.serverBD.niDealerPed[iTableID])
	
	IF IS_ENTITY_ALIVE(piDealer) 
		STOP_PED_SPEAKING(piDealer, NOT ROULETTE_AUDIO_CAN_DEALER_TALK(iTableID))
	ENDIF
	
	IF NOT NETWORK_HAS_CONTROL_OF_NETWORK_ID(sRouletteData.serverBD.niDealerPed[iTableID])
		EXIT
	ENDIF

	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
		SET_NETWORK_ID_CAN_MIGRATE(sRouletteData.serverBD.niDealerPed[iTableID], TRUE)
	ENDIF
ENDPROC


///-------------------------------------------
///    Minigame spin requesting from server
///-------------------------------------------    

/// PURPOSE:
///    Calculates the required number of loops so that the wheel spin lasts the entire betting duration 
///    also adds on the random end loops generated by the server 
FUNC INT ROULETTE_CLIENT_GET_REQUIRED_LOOP_COUNT_FOR_MINIGAME(ROULETTE_DATA &sRouletteData, INT iTableID)
	
	INT iBettingRoundDuration = ROULETTE_HELPER_GET_TABLES_BETTING_ROUND_DURATION(sRouletteData, iTableID)
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_CLIENT_GET_REQUIRED_LOOP_COUNT_FOR_MINIGAME - ",
	"iBettingRoundDuration: ", iBettingRoundDuration)
	
	INT iSpinStartTime = iBettingRoundDuration - ROULETTE_BETTING_SPIN_START_TIME_BEFORE_BETTING_ENDS_MS
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_CLIENT_GET_REQUIRED_LOOP_COUNT_FOR_MINIGAME - ",
	"iSpinStartTime: ", iSpinStartTime)
	
	INT iBettingSpinDurationMS = ROULETTE_HELPER_GET_TABLES_BETTING_ROUND_DURATION(sRouletteData, iTableID) - iSpinStartTime
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_CLIENT_GET_REQUIRED_LOOP_COUNT_FOR_MINIGAME - ",
	"iBettingSpinDurationMS: ", iBettingSpinDurationMS)
	
	FLOAT fWheelLoopAnimDurationMS = ROULETTE_ANIM_GET_WHEEL_LOOP_ANIM_DURATION() * 1000
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_CLIENT_GET_REQUIRED_LOOP_COUNT_FOR_MINIGAME - ",
	"fWheelLoopAnimDurationMS: ", fWheelLoopAnimDurationMS)
	
	FLOAT fWheelLoopsUntilBettingEnd = iBettingSpinDurationMS / fWheelLoopAnimDurationMS
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_CLIENT_GET_REQUIRED_LOOP_COUNT_FOR_MINIGAME - ",
	"fWheelLoopsUntilBettingEnd: ", fWheelLoopsUntilBettingEnd)
	
	INT iWheelLoopsRoundUp = CEIL(fWheelLoopsUntilBettingEnd)
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_CLIENT_GET_REQUIRED_LOOP_COUNT_FOR_MINIGAME - ",
	"iWheelLoopsRoundUp: ", iWheelLoopsRoundUp)
	
	INT iSpinMaxLoops = sRouletteData.serverBD.iSpinMaxLoops[iTableID]
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_CLIENT_GET_REQUIRED_LOOP_COUNT_FOR_MINIGAME - ",
	"iSpinMaxLoops: ", iSpinMaxLoops)
	
	INT iTotalLoops = iWheelLoopsRoundUp + iSpinMaxLoops
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_CLIENT_GET_REQUIRED_LOOP_COUNT_FOR_MINIGAME - ",
	"iTotalLoops: ", iTotalLoops)
	
	RETURN iTotalLoops
ENDFUNC

/// PURPOSE:
///    Checks if the server has requested the client to spin the wheel to a new number   
FUNC BOOL ROULETTE_CLIENT_HAS_NEW_SPIN_BEEN_REQUESTED(ROULETTE_DATA &sRouletteData, INT iTableID)
	INT iServersRequestedNumber = ROULETTE_HELPER_GET_SERVERS_CURRENT_WINNING_NUMBER_FOR_TABLE(sRouletteData, iTableID)	
	
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_HAS_NEW_SPIN_BEEN_REQUESTED",
	" - iServersRequestedNumber: ", iServersRequestedNumber)
	
	// If spin number is different to current 
	RETURN IS_BIT_SET(sRouletteData.serverBD.iTableBS[iTableID], ROULETTE_SERVER_TABLE_BS_REQUIRES_WHEEL_SPIN)
ENDFUNC

/// PURPOSE:
///    Makes sure the current tables assets are assigned to the spin struct
PROC ROULETTE_CLIENT_VALIDATE_ASSETS_FOR_TABLE_SPIN(ROULETTE_DATA &sRouletteData, INT iTableID)
	IF NOT DOES_ENTITY_EXIST(sRouletteData.sSpin[iTableID].objBall)
		BOOL bShouldHide = sRouletteData.sSpin[iTableID].sData.eWheelSpinState = ROULETTE_ANIM_CLIP_INVALID
		ROULETTE_PROPS_CREATE_BALL_PROP_FOR_TABLE(sRouletteData, iTableID, bShouldHide)
	ENDIF
ENDPROC

/// PURPOSE:
///    Updates the wheel spin animation sequence for the given table 
PROC ROULETTE_CLIENT_UPDATE_PROCESS_SERVER_SPIN_REQUESTS(ROULETTE_DATA &sRouletteData, INT iTableID)			
	INT iPlayerID = NATIVE_TO_INT(GET_PLAYER_INDEX())
	BOOL bWheelExists = DOES_ENTITY_EXIST(sRouletteData.sSpin[iTableID].objWheel)
	BOOL bWheelHasDrawable = FALSE
	
	// Can only check for drawable if the table exists
	IF bWheelExists
		bWheelHasDrawable = DOES_ENTITY_HAVE_DRAWABLE(sRouletteData.sSpin[iTableID].objWheel)
	ENDIF
	
	// If the table isn't ready to have a spin, clear the spin in progress
	IF NOT bWheelExists	OR NOT bWheelHasDrawable
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_UPDATE_PROCESS_SERVER_SPIN_REQUESTS - ",
		"Table: ", iTableID,
		" bWheelExists: ", bWheelExists, " bWheelHasDrawable: ", bWheelHasDrawable)
		CLEAR_BIT(sRouletteData.playerBD[iPlayerID].iClientTableBS[iTableID],
		ROULETTE_CLIENT_TABLE_BS_SPIN_IN_PROGRESS)
	ENDIF		
	
	// Do not allow additional spins if the current spin hasn't finished
	IF sRouletteData.sSpin[iTableID].sData.eWheelSpinState != ROULETTE_ANIM_CLIP_INVALID
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_UPDATE_PROCESS_SERVER_SPIN_REQUESTS - ",
		"Table: ", iTableID,
		" Wheel spin already in progress")
		EXIT
	ENDIF
	
	// No spin in progress
	CLEAR_BIT(sRouletteData.playerBD[iPlayerID].iClientTableBS[iTableID],
	ROULETTE_CLIENT_TABLE_BS_SPIN_IN_PROGRESS)
	
	// No spin in progress so the result hasn't been called
	CLEAR_BIT(sRouletteData.playerBD[iPlayerID].iClientTableBS[iTableID],
	ROULETTE_CLIENT_TABLE_BS_RESULT_CALLED)
	
	/// Reset to wait for spin request if  server has requested a new wheel spin
	IF NOT ROULETTE_CLIENT_HAS_NEW_SPIN_BEEN_REQUESTED(sRouletteData, iTableID)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_UPDATE_PROCESS_SERVER_SPIN_REQUESTS - ",
		"Table: ", iTableID,
		" spin not requested")
		EXIT
	ENDIF

	// Net ped dealer not starting spin anim yet
	IF NOT ROULETTE_ANIM_IS_TABLES_DEALER_PLAYING_CLIP(sRouletteData, iTableID, ROULETTE_ANIM_CLIP_DEALER_SPIN)		
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_UPDATE_PROCESS_SERVER_SPIN_REQUESTS - ",
		"Table: ", iTableID,
		" dealer not playing")
		EXIT
	ENDIF

	INT iLoopCount = ROULETTE_CLIENT_GET_REQUIRED_LOOP_COUNT_FOR_MINIGAME(sRouletteData, iTableID)	
	
	/// Don't give the spin the target number, this will be given last at the last second
	/// before the outro to avoid cheating
	IF ROULETTE_ANIM_START_WHEEL_SPIN(sRouletteData.sSpin[iTableID], iLoopCount, 0)
		SET_BIT(sRouletteData.playerBD[iPlayerID].iClientTableBS[iTableID],
		ROULETTE_CLIENT_TABLE_BS_SPIN_IN_PROGRESS)
	ELSE
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_ANIM_UPDATE_PROCESS_SERVER_SPIN_REQUESTS - ",
		"Table: ", iTableID,
		" failed to start wheel spin")
	ENDIF
ENDPROC

/// PURPOSE:
///    Retrieves the target win number for the spin when it is actually needed,
///    this avoids keeping the win number in memory for longer than it needs to be to prevent cheating
PROC ROULETTE_CLIENT_MAINTAIN_RETRIEVING_WIN_NUMBER_WHEN_AVAILABLE(ROULETTE_DATA &sRouletteData, INT iTableID)
	// Ensure target number is only stored when it is really needed
	IF sRouletteData.sSpin[iTableID].sData.eWheelSpinState = ROULETTE_ANIM_CLIP_INVALID 
	OR sRouletteData.sSpin[iTableID].sData.eWheelSpinState = ROULETTE_ANIM_CLIP_PROP_WHEEL_SPIN_OUTRO
	#IF IS_DEBUG_BUILD 
	AND NOT IS_DEBUG_KEY_JUST_PRESSED(KEY_F, KEYBOARD_MODIFIER_NONE, "Skip wheel spin") 
	#ENDIF
		sRouletteData.sSpin[iTableID].sData.iTargetNumber = -1
		EXIT
	ENDIF
	
	INT iLoopEndTime = ROULETTE_ANIM_GET_LOOPING_END_TIME_MS(sRouletteData.sSpin[iTableID])
	FLOAT iTimeRemaining = sRouletteData.sSpin[iTableID].sData.fSpinTimeMS
	
	// If there is less than 2 seconds left before the outro is played grab and store the win number
	IF (iLoopEndTime - iTimeRemaining) < 2000 
	#IF IS_DEBUG_BUILD 
	OR IS_DEBUG_KEY_JUST_PRESSED(KEY_F, KEYBOARD_MODIFIER_NONE, "Skip wheel spin")
	#ENDIF
		sRouletteData.sSpin[iTableID].sData.iTargetNumber = ROULETTE_HELPER_GET_SERVERS_CURRENT_WINNING_NUMBER_FOR_TABLE(sRouletteData, iTableID)
	ENDIF
ENDPROC

/// PURPOSE:
///    Adds the tables current winning number to its history
PROC ROULETTE_CLIENT_ADD_WIN_NUMBER_TO_TABLE_HISTORY(ROULETTE_DATA &sRouletteData, INT iTableID)
	INT iWinningNumber = ROULETTE_HELPER_GET_SERVERS_CURRENT_WINNING_NUMBER_FOR_TABLE(sRouletteData, iTableID)
	
	INT iInsertIndex = (iTableID * ROULETTE_TABLE_MAX_NUMBER_OF_TABLES) + (sRouletteData.iWinHistoryStoredCount[iTableID])
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_CLIENT_ADD_WIN_NUMBER_TO_TABLE_HISTORY - ", 
	"iInsertIndex: ", iInsertIndex, " iWinNumber: ", iWinningNumber)
	
	// Increment slot to insert, never resets so that the latest result is always on the end
	sRouletteData.iWinHistoryStoredCount[iTableID]++
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_CLIENT_ADD_WIN_NUMBER_TO_TABLE_HISTORY - ",
	"iWinHistoryStoredCount: ", sRouletteData.iWinHistoryStoredCount[iTableID])
	
	IF iInsertIndex > COUNT_OF(sRouletteData.iWinHistory)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_CLIENT_ADD_WIN_NUMBER_TO_TABLE_HISTORY - ", 
		"Insert index exceeds array length! iInsertIndex: ", iInsertIndex)
		EXIT
	ENDIF
		
	// If inserting in the last position, shift all old entries back
	IF sRouletteData.iWinHistoryStoredCount[iTableID] > ROULETTE_MAX_WIN_HISTORY
		
		// Shift history back removing the oldest
		INT i = 0
		REPEAT ROULETTE_MAX_WIN_HISTORY - 1 i 		
	        sRouletteData.iWinHistory[(iTableID * ROULETTE_TABLE_MAX_NUMBER_OF_TABLES) + i] = 
			sRouletteData.iWinHistory[(iTableID * ROULETTE_TABLE_MAX_NUMBER_OF_TABLES) + (i + 1)]
		ENDREPEAT
		
		// Store new number in last slot
		sRouletteData.iWinHistory[(iTableID * ROULETTE_TABLE_MAX_NUMBER_OF_TABLES) + (ROULETTE_MAX_WIN_HISTORY - 1)] = iWinningNumber
		sRouletteData.iWinHistoryStoredCount[iTableID] = CLAMP_INT(sRouletteData.iWinHistoryStoredCount[iTableID], 0, ROULETTE_MAX_WIN_HISTORY)
		EXIT
	ENDIF	
	
	sRouletteData.iWinHistoryStoredCount[iTableID] = CLAMP_INT(sRouletteData.iWinHistoryStoredCount[iTableID], 0, ROULETTE_MAX_WIN_HISTORY)
		
	// Store the result normally
	sRouletteData.iWinHistory[iInsertIndex] = iWinningNumber	
ENDPROC

/// PURPOSE:
///    Maintains calling out any winning results of a spin and storing it in the history
PROC ROULETTE_CLIENT_MAINTAIN_TABLE_SPIN_RESULT_CALL(ROULETTE_DATA &sRouletteData, INT iTableID)
	INT iPlayerID = NATIVE_TO_INT(GET_PLAYER_INDEX())
	
	IF IS_BIT_SET(sRouletteData.playerBD[iPlayerID].iClientTableBS[iTableID], ROULETTE_CLIENT_TABLE_BS_SPIN_IN_PROGRESS)
	AND NOT IS_BIT_SET(sRouletteData.playerBD[iPlayerID].iClientTableBS[iTableID], ROULETTE_CLIENT_TABLE_BS_RESULT_CALLED)
	AND ROULETTE_ANIM_HAS_BALL_LANDED_IN_WINNING_SLOT(sRouletteData.sSpin[iTableID])
		
		// Only announce the winning number if people are playing
		IF sRouletteData.serverBD.iTablePlayerCount[iTableID] > 0
			ROULETTE_AUDIO_PLAY_DEALER_CALL_WINNING_NUMBER(sRouletteData, iTableID)
		ENDIF		
		
		ROULETTE_CLIENT_ADD_WIN_NUMBER_TO_TABLE_HISTORY(sRouletteData, iTableID)
		SET_BIT(sRouletteData.playerBD[iPlayerID].iClientTableBS[iTableID], ROULETTE_CLIENT_TABLE_BS_RESULT_CALLED)
	ENDIF
ENDPROC

/// PURPOSE:
///    Maintains hiding the roulette ball a few seconds before a spin
PROC ROULETTE_CLIENT_MAINTAIN_HIDE_BALL_BEFORE_SPIN(ROULETTE_DATA &sRouletteData, INT iTableID)
	INT iBettingRoundDuration = ROULETTE_HELPER_GET_TABLES_BETTING_ROUND_DURATION(sRouletteData, iTableID)
	
	// Flash fade the ball away a few seconds before the next spin starts
	IF HAS_NET_TIMER_EXPIRED(sRouletteData.serverBD.stBettingTimer[iTableID],
	iBettingRoundDuration - (ROULETTE_BETTING_SPIN_START_TIME_BEFORE_BETTING_ENDS_MS + ROULETTE_ANIM_BALL_FLASH_FADE_DURATION_MS))
	AND sRouletteData.serverBD.eGameState[iTableID] = ROULETTE_GAME_STATE_BETTING
		ROULETTE_ANIM_FLASH_FADE_OUT_BALL(sRouletteData.sSpin[iTableID])
	ENDIF
ENDPROC

/// PURPOSE:
///    Updates the tables spin sequence according to spin requests from the server
PROC ROULETTE_CLIENT_UPDATE_TABLE_SPIN_SEQUENCE(ROULETTE_DATA &sRouletteData, INT iTableID)	
	ROULETTE_CLIENT_UPDATE_PROCESS_SERVER_SPIN_REQUESTS(sRouletteData, iTableid)
	ROULETTE_CLIENT_VALIDATE_ASSETS_FOR_TABLE_SPIN(sRouletteData, iTableID)
	ROULETTE_CLIENT_MAINTAIN_RETRIEVING_WIN_NUMBER_WHEN_AVAILABLE(sRouletteData, iTableID)	
	ROULETTE_CLIENT_MAINTAIN_HIDE_BALL_BEFORE_SPIN(sRouletteData, iTableID)
	ROULETTE_ANIM_UPDATE_WHEEL_SPIN(sRouletteData.sSpin[iTableID])
	ROULETTE_CLIENT_MAINTAIN_TABLE_SPIN_RESULT_CALL(sRouletteData, iTableID)
ENDPROC

/// PURPOSE:
///    Initialization for the roulette client before the game starts
PROC ROULETTE_CLIENT_PRE_GAME_INIT(ROULETTE_DATA &sRouletteData)
	ROULETTE_CAMERA_INIT(sRouletteData)
	ROULETTE_PROPS_LOAD_REQUIRED_MODELS()
	ROULETTE_ANIM_LOAD_PROP_DICTIONARY()
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_CLIENT_PRE_GAME_INIT - Init finished")
ENDPROC

/// PURPOSE:
///    Requests updated init data from the host so that the tables can be initialised
///    with the correct spi state and history
FUNC BOOL ROULETTE_CLIENT_REQUEST_INIT_DATA(ROULETTE_DATA &sRouletteData)
	// Host will always have the correct spin sequence state initialised		
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		RETURN TRUE
	ENDIF	
	
	BOOL bServerSpinDataReady = IS_BIT_SET(sRouletteData.serverBD.iServerBS, ROULETTE_SERVER_BS_UPDATED_INIT_DATA)
	
	// Request spin data
	IF NOT bServerSpinDataReady
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_CLIENT_PRE_GAME_INIT - Requesting spin init data")
		SET_BIT(sRouletteData.playerBD[NATIVE_TO_INT(PLAYER_ID())].iPlayerBS, ROULETTE_PLAYER_BS_REQUIRES_INIT_DATA)
		RETURN FALSE
	ENDIF
	
	// Initialise the remote players wheel sequence to the same as the hosts
	CPRINTLN(DEBUG_ROULETTE, "ROULETTE_CLIENT_PRE_GAME_INIT - Spin init data recieved")
	CLEAR_BIT(sRouletteData.playerBD[NATIVE_TO_INT(PLAYER_ID())].iPlayerBS, ROULETTE_PLAYER_BS_REQUIRES_INIT_DATA)
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Copies the hosts history for the table to the client
PROC ROULETTE_CLIENT_COPY_TABLE_HISTORY_FROM_HOST(ROULETTE_DATA &sRouletteData)
	COPY_INT_ARRAY(sRouletteData.iWinHistory, sRouletteData.serverBD.iInitWinHistory)
	COPY_INT_ARRAY(sRouletteData.iWinHistoryStoredCount, sRouletteData.serverBD.iInitWinHistoryStoredCount)
ENDPROC

/// PURPOSE:
///    Initialises the wheel spin sequences with data retrieved by the host
PROC ROULETTE_CLIENT_COPY_INIT_DATA_FROM_HOST(ROULETTE_DATA &sRouletteData)
	INT iTableID = 0
	INT iPlayerID = NATIVE_TO_INT(PLAYER_ID())
	
	REPEAT ROULETTE_TABLE_MAX_NUMBER_OF_TABLES iTableID
		ROULETTE_CLIENT_VALIDATE_ASSETS_FOR_TABLE_SPIN(sRouletteData, iTableID)	
		ROULETTE_ANIM_INIT_WHEEL_SPIN_WITH_SPIN_DATA(sRouletteData.sSpin[iTableID],
		sRouletteData.serverBD.sSpinInitData[iTableID])
		
		// Check if its in progress or not
		IF sRouletteData.serverBD.sSpinInitData[iTableID].eWheelSpinState != ROULETTE_ANIM_CLIP_INVALID	
			SET_BIT(sRouletteData.playerBD[iPlayerID].iClientTableBS[iTableID], ROULETTE_CLIENT_TABLE_BS_SPIN_IN_PROGRESS)
		ENDIF
	ENDREPEAT
	
	ROULETTE_CLIENT_COPY_TABLE_HISTORY_FROM_HOST(sRouletteData)
ENDPROC

/// PURPOSE:
///    Update state for client init
PROC ROULETTE_CLIENT_STATE_INIT_UPDATE(ROULETTE_DATA &sRouletteData)	

	IF NOT ROULETTE_PROPS_HAVE_REQUIRED_MODELS_LOADED() 
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_CLIENT_STATE_INIT_UPDATE - Waiting for models to load")
		EXIT
	ENDIF
	
	IF NOT ROULETTE_ANIM_HAS_PROP_DICTIONARY_LOADED() 
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_CLIENT_STATE_INIT_UPDATE - Waiting for prop anim dictionary to load")
		EXIT
	ENDIF
	
	IF NOT ROULETTE_PROPS_CREATE_REQUIRED_MODELS(sRouletteData)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_CLIENT_STATE_INIT_UPDATE - Waiting for props to create")
		EXIT
	ENDIF
	
	IF NOT ROULETTE_CLIENT_REQUEST_INIT_DATA(sRouletteData)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_CLIENT_STATE_INIT_UPDATE - Waiting for spin request data")
		EXIT
	ENDIF
	
	ROULETTE_CLIENT_COPY_INIT_DATA_FROM_HOST(sRouletteData)
	
	ROULETTE_PROPS_UPDATE_CHIP_CURSOR_MODEL(sRouletteData)
	
	IF DOES_ENTITY_EXIST(sRouletteData.objCursor)
		SET_ENTITY_VISIBLE(sRouletteData.objCursor, FALSE)
	ENDIF
	
	sRouletteData.iSeatID = 0
	sRouletteData.iTableID = 0
	
	ROULETTE_CLIENT_SET_STATE(sRouletteData, ROULETTE_CLIENT_STATE_LOCATE,
	&ROULETTE_DEFAULT_ON_EXIT, &ROULETTE_DEFAULT_ON_ENTER)
ENDPROC

/// PURPOSE:
///    Checks if we should performing the local chip clear sequence rather than relying on
///    updating from the bet data
FUNC BOOL ROULETTE_CLIENT_SHOULD_TABLE_PERFORM_CHIP_CLEAR_SEQUENCE(ROULETTE_DATA &sRouletteData, INT iTableID)
	ROULETTE_GAME_STATE eServerGameState = sRouletteData.serverBD.eGameState[iTableID]
	RETURN eServerGameState = ROULETTE_GAME_STATE_ROUND_RESOLUTION
	OR eServerGameState = ROULETTE_GAME_STATE_WAIT_FOR_NEXT_ROUND
ENDFUNC

/// PURPOSE:
///    Maintains hiding the ball according to any external requirements
PROC ROULETTE_CLIENT_MAINTAIN_BALL_HIDING(ROULETTE_DATA &sRouletteData, INT iTableID)
	IF NOT DOES_ENTITY_EXIST(sRouletteData.sSpin[iTableID].objBall)
		EXIT
	ENDIF
	
	IF ROULETTE_PROPS_SHOULD_HIDE_BALL(iTableID)	
		SET_ENTITY_VISIBLE(sRouletteData.sSpin[iTableID].objBall, FALSE)
		CPRINTLN(DEBUG_ROULETTE, "ROULETTE_CLIENT_MAINTAIN_BALL_HIDING - Hiding ball on table ", iTableID)
		EXIT
	ENDIF
	
	SET_ENTITY_VISIBLE(sRouletteData.sSpin[iTableID].objBall, TRUE)
ENDPROC

/// PURPOSE:
///    Maintains the background behaviour of all tables that are active
PROC ROULETTE_CLIENT_MAINTAIN_ACTIVE_TABLES(ROULETTE_DATA &sRouletteData)
	INT iTableID = 0
	REPEAT ROULETTE_TABLE_MAX_NUMBER_OF_TABLES iTableID		
		IF ROULETTE_TABLE_IS_DISABLED(iTableID)
			RELOOP
		ENDIF
		
		#IF IS_DEBUG_BUILD
			IF DOES_ENTITY_EXIST(sRouletteData.sSpin[iTableID].objWheel)
	            IF NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(sRouletteData.sSpin[iTableID].objWheel)
					CASSERTLN(DEBUG_ROULETTE, "ROULETTE_CLIENT_MAINTAIN_ACTIVE_TABLES - roulette table is not owned by this script")
				ENDIF
			ENDIF
		#ENDIF
			
		IF NOT DOES_ENTITY_EXIST(sRouletteData.sSpin[iTableID].objWheel)
			sRouletteData.sSpin[iTableID].objWheel = ROULETTE_PROPS_CREATE_TABLE(iTableID, TRUE)
			CASSERTLN(DEBUG_ROULETTE, "ROULETTE_CLIENT_MAINTAIN_ACTIVE_TABLES - Had to re-create table!")
		ENDIF
		
		ROULETTE_CLIENT_MAINTAIN_BALL_HIDING(sRouletteData, iTableID)
		ROULETTE_CLIENT_MAINTAIN_DEALER_AT_TABLE(sRouletteData, iTableID)	
		ROULETTE_CLIENT_UPDATE_TABLE_SPIN_SEQUENCE(sRouletteData, iTableID)
		
		IF ROULETTE_CLIENT_SHOULD_TABLE_PERFORM_CHIP_CLEAR_SEQUENCE(sRouletteData, iTableID)
			ROULETTE_ANIM_UPDATE_CHIP_CLEARING_FOR_CLIENT_TABLE(sRouletteData, iTableID)
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Maintains keeping table chips up to date
PROC ROULETTE_CLIENT_MAINATIN_CHIP_UPDATING(ROULETTE_DATA &sRouletteData)
	INT iSeatID = 0
	INT iPlayerID = 0
	PLAYER_INDEX piPlayerID
	INT iTableID = -1		
	BOOL bDeleteChips
	
	REPEAT ROULETTE_MAX_SEAT_ID iSeatID	
		
		iTableID = ROULETTE_HELPER_GET_TABLE_ID_FROM_SEAT_ID(iSeatID)
		bDeleteChips = FALSE
		
		IF NOT ROULETTE_HELPER_IS_PLAYER_AT_SEAT_STILL_PLAYING(sRouletteData, iSeatID)
			bDeleteChips = TRUE
		ENDIF
		
		IF NOT ROULETTE_PROPS_SHOULD_CHIPS_BE_CREATED()
			bDeleteChips = TRUE
			sRouletteData.tdLastChipUpdate[iPlayerID] = NULL
		ENDIF
		
		IF bDeleteChips
			ROULETTE_PROPS_DELETE_SEAT_CHIPS(sRouletteData, iSeatID)
			RELOOP
		ENDIF
		
		piPlayerID = ROULETTE_HELPER_GET_PLAYER_AT_SEAT(sRouletteData, iSeatID)
		iPlayerID = NATIVE_TO_INT(piPlayerID)
		
		IF sRouletteData.playerBD[iPlayerID].tdLastChipUpdate = sRouletteData.tdLastChipUpdate[iPlayerID]
			RELOOP
		ENDIF		
		
		// Don't update chips during chip clear process
		IF ROULETTE_CLIENT_SHOULD_TABLE_PERFORM_CHIP_CLEAR_SEQUENCE(sRouletteData, iTableID)
		OR NOT ROULETTE_PROPS_SHOULD_CHIPS_BE_CREATED()
			RELOOP
		ENDIF
		
		// Chips aren't up to date
		sRouletteData.tdLastChipUpdate[iPlayerID] = sRouletteData.playerBD[iPlayerID].tdLastChipUpdate
		ROULETTE_PROPS_UPDATE_CHIPS_FOR_TABLE(sRouletteData, iTableID)
	ENDREPEAT
ENDPROC 

/// PURPOSE:
///    Updates the roulette client side logic
PROC ROULETTE_CLIENT_UPDATE(ROULETTE_DATA &sRouletteData)
	
	SWITCH sRouletteData.eClientState
		CASE ROULETTE_CLIENT_STATE_INIT
			ROULETTE_CLIENT_STATE_INIT_UPDATE(sRouletteData)
		EXIT// Leave as we shouldn't process anytthing other than init
		CASE ROULETTE_CLIENT_STATE_LOCATE
			ROULETTE_CLIENT_STATE_LOCATE_UPDATE(sRouletteData)		
		BREAK
		CASE ROULETTE_CLIENT_STATE_PROMPT
			ROULETTE_CLIENT_STATE_PROMPT_UPDATE(sRouletteData)
		BREAK
		CASE ROULETTE_CLIENT_STATE_DISCLOSURE
			ROULETTE_CLIENT_STATE_DISCLOSURE_UPDATE(sRouletteData)
		BREAK
		CASE ROULETTE_CLIENT_STATE_WAIT_FOR_APPROVAL
			ROULETTE_CLIENT_STATE_WAIT_FOR_APPROVAL_UPDATE(sRouletteData)
		BREAK
		CASE ROULETTE_CLIENT_STATE_MOVING_TO_SEAT
			ROULETTE_CLIENT_STATE_MOVING_TO_SEAT_UPDATE(sRouletteData)
		BREAK		
		CASE ROULETTE_CLIENT_STATE_JOINING_TABLE
			ROULETTE_CLIENT_STATE_JOINING_TABLE_UPDATE(sRouletteData)	
		BREAK
		CASE ROULETTE_CLIENT_STATE_PLAYING
			ROULETTE_CLIENT_STATE_PLAYING_UPDATE(sRouletteData)
		BREAK
		CASE ROULETTE_CLIENT_STATE_LEAVING_TABLE
			ROULETTE_CLIENT_STATE_LEAVING_TABLE_UPDATE(sRouletteData)
		BREAK
	ENDSWITCH
	
	ROULETTE_CLIENT_MAINATIN_CHIP_UPDATING(sRouletteData)
	ROULETTE_CLIENT_MAINTAIN_ACTIVE_TABLES(sRouletteData)
ENDPROC
