	//****************************************************************************************************
//
//	Author: 		Asa Dang
//	Date: 			6/24/11
//	Description:	Minigame shell logic for pilot school. Contains main loop to deal with menus, 
//					preview cut-scenes and launching/updating challenges.
//
//	ALL PILOT SCHOOL SCRIPT LOCATIONS:
//	X:\gta5\script\dev\singleplayer\include\private\Pilot_School
//	X:\gta5\script\dev\singleplayer\scripts\Minigames\PilotSchool
//****************************************************************************************************



//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0



CONST_INT						PS_FORMATION_DEBUG			0
#IF IS_DEBUG_BUILD
	CONST_INT						PS_PRINT_STATS				1
	CONST_INT						PS_DRAW_DEBUG_LINES_AND_SPHERES				0
#ENDIF
#IF NOT IS_DEBUG_BUILD
 	CONST_INT						PS_DRAW_DEBUG_LINES_AND_SPHERES				0 //for compile, functions won't work anyways
#ENDIF

USING "Pilot_School_MP_Data.sch"
PROC DEBUG_PRINT_SCORES(int lessonToPrint,string tag)

	PRINTLN("||||||||||||||||| player data update : ",tag," ||||||||||||||||")
	PRINTLN("PS_Main.myPlayerData:")
	PRINTLN("Elapsed Time = ",PS_Main.myPlayerData.ElapsedTime)
	PRINTLN("Last Elapsed Time = ",PS_Main.myPlayerData.LastElapsedTime)
	PRINTLN("Distance = ",PS_Main.myPlayerData.LandingDistance)
	PRINTLN("Last Distance = ",PS_Main.myPlayerData.LastLandingDistance)
	PRINTLN("Has Lesson Passed = ",PS_Main.myPlayerData.HasPassedLesson)
	PRINTLN("medal = ",PS_Main.myPlayerData.eMedal)
	PRINTLN("last medal = ",PS_Main.myPlayerData.eLastMedal)
	PRINTLN("")
	PRINTLN("Save Data: ")
	PRINTLN("Elapsed Time = ",saveData[lessonToPrint].ElapsedTime)
	PRINTLN("Last Elapsed Time = ",saveData[lessonToPrint].LastElapsedTime)
	PRINTLN("Distance = ",saveData[lessonToPrint].LandingDistance)
	PRINTLN("Last Distance = ",saveData[lessonToPrint].LastLandingDistance)
	PRINTLN("Has Lesson Passed = ",saveData[lessonToPrint].HasPassedLesson)
	PRINTLN("medal = ",saveData[lessonToPrint].eMedal)
	PRINTLN("last medal = ",saveData[lessonToPrint].eLastMedal)

ENDPROC

bool controlState = TRUE
PROC DO_NET_SET_PLAYER_CONTROL(PLAYER_INDEX thisPlayer, bool Control, bool freeze=FALSE, bool cleartasks=TRUE, bool bHaveCollision = FALSE, BOOL bAllowDamage = FALSE)
	IF control != controlState
		IF (Control)
			NET_SET_PLAYER_CONTROL(thisPlayer, TRUE)
		ELSE
			NET_SET_PLAYER_CONTROL_FLAGS eFlags
			IF (cleartasks)
				eFlags = eFlags | NSPC_CLEAR_TASKS 
			ENDIF
			IF NOT (bHaveCollision)
				eFlags = eFlags  | NSPC_NO_COLLISION
			ENDIF
			IF (freeze)
				eFlags = eFlags | NSPC_FREEZE_POSITION 
			ENDIF
			
			if bAllowDamage
				eFlags = eFlags | NSPC_ALLOW_PLAYER_DAMAGE
			ENDIF
			
			NET_SET_PLAYER_CONTROL(thisPlayer, FALSE, eFlags)
		ENDIF
		controlState = control
	ENDIF
ENDPROC
 
bool bwaitingOnSomething
bool bPlayerSpawned

USING "Pilot_School_MP_Menu.sch"


USING "vehicle_gen_private.sch"
USING "emergency_call.sch"
//USING "rgeneral_include.sch"
USING "code_control_public.sch"
USING "minigames_helpers.sch"
USING "script_oddjob_funcs.sch"
USING "PS_MP_Objective_lib.sch"
USING "Pilot_School_MP_Challenge_Helpers.sch"
USING "net_wait_zero.sch"

USING "PS_MP_Spectator_lib.sch"

//new dlc classes headers should be included here
USING "PS_DLC_Outside_Loop.sch"
USING "PS_DLC_Collect_Flags.sch"
USING "PS_DLC_Follow_Leader.sch"
USING "PS_DLC_Fly_Low.sch"
USING "PS_DLC_Vehicle_Landing.sch"
USING "PS_DLC_Chase_Parachute.sch"
USING "PS_DLC_Shooting_Range.sch"
USING "PS_DLC_CityLanding.sch"
USING "PS_DLC_Engine_Failure.sch"
USING "PS_DLC_Formation.sch"

USING "FMMC_MP_Setup_Mission.sch"

#IF IS_DEBUG_BUILD
	USING "Pilot_School_MP_Debug_lib.sch"
#ENDIF

// *********************************** \\
// *********************************** \\
//                                     \\
//				SERVER BD              \\
//                                     \\
// *********************************** \\
// *********************************** \\



//SCRIPT_TIMER TerminationTimer


//Const for number of things to reserver
CONST_INT ciNUMBER_OF_PILOT_VEHICLES	5
CONST_INT ciNUMBER_OF_PILOT_OBJECTS		36
CONST_INT ciNUMBER_OF_PILOT_PEDS		5



FUNC STRING GET_MUSIC_EVENT(PS_MUSIC_ENUM thisMusic)
	SWITCH thisMusic
		CASE PS_MUSIC_FORM_START
			RETURN "FS_FORMATION_START"
		BREAK
		CASE PS_MUSIC_FORM_PASS
			RETURN "FS_FORMATION_STOP"
		BREAK
		CASE PS_MUSIC_FORM_FAIL
			RETURN "FS_FORMATION_FAIL"
		BREAK
		CASE PS_MUSIC_LOOP_START
			RETURN "FS_LOOP_START"
		BREAK
		CASE PS_MUSIC_LOOP_PASS
			RETURN "FS_LOOP_STOP"
		BREAK
		CASE PS_MUSIC_LOOP_FAIL
			RETURN "FS_LOOP_FAIL"
		BREAK
		CASE PS_MUSIC_MOVING_START
			RETURN "FS_MOVING_LANDING_START"
		BREAK
		CASE PS_MUSIC_MOVING_FAIL
			RETURN "FS_MOVING_LANDING_FAIL"
		BREAK
		CASE PS_MUSIC_MOVING_PASS
			RETURN "FS_MOVING_LANDING_STOP"
		BREAK
		CASE PS_MUSIC_PARACHUTE_START
			RETURN "CHASE_PARACHUTE_START"
		BREAK
		CASE PS_MUSIC_PARACHUTE_JUMP
			RETURN "CHASE_PARACHUTE_DEPLOY"
		BREAK
		CASE PS_MUSIC_PARACHUTE_FAIL
			RETURN "FS_OBSTACLE_FAIL"
		BREAK
		CASE PS_MUSIC_PARACHUTE_PASS
			RETURN "FS_OBSTACLE_STOP"
		BREAK
		CASE PS_MUSIC_SHOOT_START
			RETURN "SHOOTING_RANGE_START"
		BREAK
		CASE PS_MUSIC_SHOOT_TIMED
			RETURN "SHOOTING_RANGE_TIMED"
		BREAK
		CASE PS_MUSIC_SHOOT_FAIL
			RETURN "SHOOTING_RANGE_FAIL"
		BREAK
		CASE PS_MUSIC_SHOOT_PASS
			RETURN "SHOOTING_RANGE_STOP"
		BREAK
		CASE PS_MUSIC_CITY_START
			RETURN "CITY_LANDING_START"
		BREAK
		CASE PS_MUSIC_CITY_CUTOUT
			RETURN "CITY_LANDING_ENGINE"
		BREAK
		CASE PS_MUSIC_CITY_PASS
			RETURN "CITY_LANDING_STOP"
		BREAK
		CASE PS_MUSIC_CITY_FAIL
			RETURN "CITY_LANDING_FAIL"
		BREAK
		CASE PS_MUSIC_GROUND_START
			RETURN "GROUND_LEVEL_START"
				
		BREAK
		CASE PS_MUSIC_GROUND_STOP
			RETURN "GROUND_LEVEL_STOP"
				
		BREAK
		CASE PS_MUSIC_GROUND_FAIL
			RETURN "GROUND_LEVEL_FAIL"				
		BREAK
		CASE PS_MUSIC_COLLECT_START
			RETURN "FS_OBSTACLE_START"				
		BREAK
		CASE PS_MUSIC_COLLECT_STOP
			RETURN "FS_OBSTACLE_STOP"				
		BREAK
		CASE PS_MUSIC_COLLECT_FAIL
			RETURN "FS_OBSTACLE_FAIL"				
		BREAK
	ENDSWITCH
	RETURN ""
ENDFUNC

PROC PS_HANDLE_MUSIC()
	IF currentLessonMusic != PS_MUSIC_NONE
		CPRINTLN(DEBUG_TREVOR3,"PS MUSIC ",GET_MUSIC_EVENT(currentLessonMusic))
		IF TRIGGER_MUSIC_EVENT(GET_MUSIC_EVENT(currentLessonMusic))
			currentLessonMusic = PS_MUSIC_NONE
		ENDIF
	ENDIF				
ENDPROC

PROC PS_CLEANUP()

	DISABLE_RADAR_MAP(FALSE)
	DISABLE_NIGHT_VISION_CONTROLLER(FALSE)
	CLEAR_HELP()
	CLEAR_ADDITIONAL_TEXT(MINIGAME_TEXT_SLOT, TRUE)
	
	CLEAR_RANK_REDICTION_DETAILS() 
	
	SC_LEADERBOARD_CACHE_CLEAR_ALL()

	DISABLE_CHEAT(CHEAT_TYPE_ALL, FALSE)
	DISABLE_CELLPHONE(FALSE)
	SET_MINIMAP_COMPONENT(MINIMAP_COMPONENT_RUNWAY_1, FALSE)
	FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
	
	TRIGGER_SCREENBLUR_FADE_OUT(0)
	
	//MG_CLEANUP_FAIL_FADE_EFFECT()
	
	CLEAR_TIMECYCLE_MODIFIER()
	
	STOP_AUDIO_SCENES()
	
	//ensure this audio scene is off
	IF IS_AUDIO_SCENE_ACTIVE("MP_CELEB_SCREEN_SCENE")
		STOP_AUDIO_SCENE("MP_CELEB_SCREEN_SCENE")
	ENDIF

	//added for bug 2139004
	SET_GAME_PAUSES_FOR_STREAMING(TRUE)
	PRINTLN("[SAC] - called SET_GAME_PAUSES_FOR_STREAMING(TRUE). Call J.")
	SET_CELEBRATION_AUDIO_STAGE(eCAS_OFF)
	
	CLEAR_KILL_STRIP_DEATH_EFFECTS()
	
	SET_PLAYER_INVINCIBLE(PLAYER_ID(), FALSE)
	CPRINTLN(debug_trevor3,"CHECK INVINCIBLE F: FALSE")
	cprintln(debug_trevor3,"SET_FAIL_FADE_EFFECT_SCRIPT_CONTROL(FALSE)")
	SET_FAIL_FADE_EFFECT_SCRIPT_CONTROL(FALSE)
	
	BAWSAQ_INCREMENT_MODIFIER(BSMF_SM_PISCO)
	MISSION_FLOW_CLEAR_DISPLAY_MISSION_TITLE()
	
	IF IS_NEW_LOAD_SCENE_ACTIVE()
		NEW_LOAD_SCENE_STOP()
	ENDIF
	REQUEST_IPL("MG-Flight School 5")
	
	DISABLE_SCRIPT_HUD(HUDPART_RANKBAR, FALSE)
	
	int iMusic
	REPEAT COUNT_OF(PS_MUSIC_ENUM) iMusic
		CANCEL_MUSIC_EVENT(GET_MUSIC_EVENT(int_to_enum(PS_MUSIC_ENUM,iMusic)))
	ENDREPEAt

	UNBLOCK_SCENARIOS_AND_AMBIENT(sb_PS)
	
	//make sure the camera views for the helicopter and stunt plane are set to the views before flight school had launched.
	SET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_HELI, cvmHelicopter)
	SET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_AIRCRAFT, cvmStuntPlane)
	
	IF DOES_ENTITY_EXIST(PS_Main.playerVehicle) AND IS_VEHICLE_DRIVEABLE(PS_Main.playerVehicle)
		IF NOT IS_PLAYER_SPECTATING(player_id())
			SET_PLAYERS_LAST_VEHICLE(PS_Main.playerVehicle)
			FREEZE_ENTITY_POSITION(PS_Main.playerVehicle, FALSE)
			ACTIVATE_PHYSICS(PS_Main.playerVehicle)
		ENDIF
	ENDIF
	
	SET_WANTED_LEVEL_MULTIPLIER(1.0)
	SET_MAX_WANTED_LEVEL(5)		
	RELEASE_SUPPRESSED_EMERGENCY_CALLS()
	ENABLE_ALL_DISPATCH_SERVICES(TRUE)
	RELEASE_SUPPRESSED_RESTRICTED_AREA_WANTED_LEVEL(AC_AIRPORT_AIRSIDE)
	RELEASE_SUPPRESSED_RESTRICTED_AREA_WANTED_LEVEL(AC_GOLF_COURSE)
	RELEASE_SUPPRESSED_RESTRICTED_AREA_WANTED_LEVEL(AC_MILITARY_BASE)
	RELEASE_SUPPRESSED_RESTRICTED_AREA_WANTED_LEVEL(AC_PRISON)
	RELEASE_SUPPRESSED_RESTRICTED_AREA_WANTED_LEVEL(AC_BIOTECH)
	RELEASE_SUPPRESSED_RESTRICTED_AREA_WANTED_LEVEL(AC_MILITARY_DOCKS)
	RELEASE_SUPPRESSED_RESTRICTED_AREA_WANTED_LEVEL(AC_MOVIE_STUDIO)
	g_bDeactivateRestrictedAreas = false
		
	//cleanup  if exiting during fail screen
	SET_MINIGAME_SPLASH_SHOWING(FALSE)
	
	CLEANUP_MG_BIG_MESSAGE(PS_UI_BigMessage)			
	SET_AUTOSAVE_IGNORES_ON_MISSION_FLAG(FALSE)

	IF DOES_CAM_EXIST(PS_EndCutCam1)
		DESTROY_CAM(PS_EndCutCam1)
	ENDIF
			
	SET_CINEMATIC_MODE_ACTIVE(FALSE)
	DISPLAY_HUD(TRUE)
			
	PS_HUD_SET_SCORECARD_ACTIVE(FALSE)
	
	//end cleanup if on fail screen
	
	//SET_PLAYER_INVINCIBLE(PLAYER_ID(), FALSE)
	
	IF NOT IS_PLAYER_SPECTATING(player_id())
		STORE_PLAYER_PED_VARIATIONS(PLAYER_PED_ID())
		
		//Remove the parachute on cleanup
		SET_PARACHUTE_STATE(false,99)
		
		RESET_CACHED_PARACHUTE_DRAWABLE_FOR_MP_VARIABLES()
		CLEAR_PLAYER_PARACHUTE_VARIATION_OVERRIDE(PLAYER_ID())
		
		//if player has control, the flight school was terminated while the player was in the lesson
		//so call te below
		IF IS_PLAYER_CONTROL_ON(player_id())
			IF bFM_MATCH_STARTED //should only get called if player exits prematurely mid-lesson
				DEAL_WITH_FM_MATCH_END(	FMMC_TYPE_MG_PILOT_SCHOOL, serverBD.iMatchHistoryID, 
				enum_to_int(g_current_selected_dlc_PilotSchool_class),
		 		ciFMMC_END_OF_MISSION_STATUS_NOT_SET,
				0,
				0,
				0,  
				0,
				0, 
				0,
				0,
				VehicleToUse,
				0)
			ENDIF
		ENDIF
		//make sure the parachute pack is turned off
		
		IF bPlayerStartedWithParachute
			IF NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), GADGETTYPE_PARACHUTE)
				GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), GADGETTYPE_PARACHUTE,1)
			ENDIF
			
			EQUIP_STORED_MP_PARACHUTE(player_id())
		ELSE
			IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), GADGETTYPE_PARACHUTE)
				REMOVE_WEAPON_FROM_PED(PLAYER_PED_ID(), GADGETTYPE_PARACHUTE)
			ENDIF
		ENDIF
			
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			
			SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
			SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
			//SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			//DO_NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, TRUE, FALSE, TRUE)	//modified to turn player control off and freeze player as suggested in bug 1966325
			FREEZE_ENTITY_POSITION(PLAYER_PED_ID(),TRUE)
			//SET_PLAYER_INVINCIBLE(PLAYER_ID(),FALSE)
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
					
			//RESTORE_PLAYER_PED_VARIATIONS(GET_PLAYER_PED(GET_PLAYER_INDEX()))
			//SET_PED_VARIATIONS(PLAYER_PED_ID(), structPlayerProps)
					
			CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
		ENDIF
		
		PS_DLC_Chase_Para_Delete_Vehs() //to fix bug 1979699
		
		RELEASE_PILOT_SCHOOL_ASSETS()
		
		RELEASE_PILOT_SCHOOL_NET_IDS()
		
		#IF IS_DEBUG_BUILD
			#IF PS_PRINT_STATS
				PS_CLEANUP_DATA_FILE_WIDGET()
			#ENDIF
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(FALSE)
		#ENDIF
		IF DOES_CAM_EXIST(camMainMenu)
			DESTROY_CAM(camMainMenu)
		ENDIF
	ELSE
		BLOCK_SPECTATOR_COPYING_TARGET_FADES(FALSE)
	ENDIF
	
	RESET_SCENARIO_TYPES_ENABLED()
	SET_GAME_PAUSED(FALSE)
	
	DISPLAY_HUD(TRUE)
	DISPLAY_RADAR(TRUE)
		
	RENDER_SCRIPT_CAMS(FALSE, FALSE)
	ENABLE_SELECTOR()
	
	PRINTLN("enabling selector!!")
	
	KILL_FACE_TO_FACE_CONVERSATION()
		
	Set_Leave_Area_Flag_For_All_Blipped_Missions()
	
	//reset the saved global bitset for under the bridge flights that were set for preview cutscene
	IF PS_Main.myChallengeData.LessonEnum = PSCD_DLC_OutsideLoop OR PS_Main.myChallengeData.LessonEnum = PSCD_DLC_FlyLow
		PS_RESTORE_BRIDGES()
	ENDIF	
	
	IF NOT IS_PLAYER_SPECTATING(player_id())
		#IF IS_DEBUG_BUILD
		IF NOT bPlayerUsedDebugSkip	
		#ENDIF
			// if player gets to cleanup and the writer is still ctive, do a normal write
			// this will be rare since the predictor kicks off well before the the menu appears, but should be a failsafe against really slow connections
			IF IS_PLAYER_ONLINE() AND PS_IS_LEADERBOARD_WRITER_ACTIVE()
			//	IF Pilot_School_Data_Is_Goal_Bit_Set(PS_Challenges[eLBLessonToWrite], FSG_0_timeTaken)
				cprintln(debug_Trevor3,"sneding to leaderboard: landing dist: ",saveData[eLBLessonToWrite].LastLandingDistance," time: ",saveData[eLBLessonToWrite].LastElapsedTime)
					PS_WRITE_DATA_TO_LEADERBOARD(eLBLessonToWrite, floor(saveData[eLBLessonToWrite].LastLandingDistance), floor(saveData[eLBLessonToWrite].LastElapsedTime), saveData[eLBLessonToWrite].LastLandingDistance)
			//	ELSE
			//		PS_WRITE_DATA_TO_LEADERBOARD(eLBLessonToWrite, Pilot_School_Data_Get_Score(PS_Main.myChallengeData, PS_Main.myPlayerData), DEFAULT, PS_Main.myPlayerData.LandingDistance)
			//	ENDIF
			ENDIF
		#IF IS_DEBUG_BUILD
		ENDIF
		#ENDIF
	ENDIF
	
	g_b_On_Pilotschool = FALSE
	RESET_GAME_STATE_ON_DEATH()
	SET_MANUAL_RESPAWN_STATE(MRS_NULL)
	
	//Clear Special Cam Flag
	IF IS_BIT_SET(MPGlobalsAmbience.iAmbBitSet, iABI_PILOT_SCHOOL_CAM)
		CLEAR_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_PILOT_SCHOOL_CAM)
		PRINTLN(" PILOT SCHOOL - SPECTATOR - iABI_PILOT_SCHOOL_CAM CLEARED - CLEANUP")
	ENDIF
	
	IF NOT IS_PLAYER_SPECTATING(player_id())
		SAVE_PILOT_SCHOOL_DATA_TO_MP(STAT_SAVETYPE_END_MISSION)
	ENDIF
	
	g_current_selected_dlc_PilotSchool_class = PSCD_DLC_OutsideLoop
	SET_USE_DLC_DIALOGUE(FALSE)
	
	ENABLE_INTERACTION_MENU()
	
	MP_TEXT_CHAT_DISABLE(FALSE) // B* 2276436
			
	DEBUG_MESSAGE("******************** END SCRIPT: Pilot_School.sc ********************")
ENDPROC

//Print some useful mp commands
PROC PS_PRINT_USEFUL_MP_DEBUG()
	#IF IS_DEBUG_BUILD 
	
		PRINTLN("------------------------PS MP DEBUG INFO-----------------------------")
	
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT() 
			PRINTLN(" I_AM_PILOT_SCHOOL_HOST ", NETWORK_PLAYER_ID_TO_INT()) 
		ELSE 
			PRINTLN(" I_AM_PILOT_SCHOOL_CLIENT ", NETWORK_PLAYER_ID_TO_INT()) 
		ENDIF
		
		IF IS_PLAYER_SCTV(PLAYER_ID())
			PRINTLN(" I AM SPECTATING ")
		ENDIF
		
		PRINTLN("MY_NAME_IS ", GET_PLAYER_NAME(PLAYER_ID()))
		
		PRINTLN("----------------------END PS MP DEBUG INFO------------------------")
		
	#ENDIF
ENDPROC

PROC PS_INIT()
/*	IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_REPEAT_PLAY|FORCE_CLEANUP_FLAG_DEBUG_MENU|FORCE_CLEANUP_FLAG_SP_TO_MP)
		PRINTSTRING("PILOT SCHOOL FORCE CLEANUP!!")PRINTNL()
   		PS_CLEANUP()
	ENDIF*/ //removing as per instructions in 1930722
	
	bPilotSchoolDLCSelected = TRUE
	g_b_On_Pilotschool = TRUe
	//ENABLE_NIGHTVISION(VISUALAID_OFF)
	//SET_NIGHTVISION(FALSE)
	DISABLE_NIGHT_VISION_CONTROLLER(TRUE)
	
	IF NOT IS_PLAYER_SPECTATING(player_id())
		//gdisablerankupmessage = FALSE
		SET_DISABLE_RANK_UP_MESSAGE(FALSE)
		
		cprintln(debug_trevor3,"#!1 bPlayerSelectingLesson = TRUE")
		serverBD.bPlayerSelectingLesson = TRUE
		
		REQUEST_SYSTEM_ACTIVITY_TYPE_PLAY_PILOT_SCHOOL()
		//needed in order to play dialogue.
		
		SET_GAME_STATE_ON_DEATH(AFTERLIFE_MANUAL_RESPAWN)
		
		DISABLE_SCRIPT_HUD(HUDPART_RANKBAR, TRUE)
		
		//load data from MP stats
		LOAD_MP_STATS_IN_TO_PILOT_SCHOOL_DATA()
		
		IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), GADGETTYPE_PARACHUTE)
			bPlayerStartedWithParachute = TRUE
		ENDIF
		//load up lesson data
		//PS_Load_Pilot_School_Data()
		
		SET_USE_DLC_DIALOGUE(TRUE)
		//g_ForceDLC_DialogueRequest = TRUE
		//g_bUse_MP_DLC_Dialogue = TRUE

		
		
		DISABLE_CHEAT(CHEAT_TYPE_ALL, TRUE)
		
		IF IS_NEW_LOAD_SCENE_ACTIVE()
			NEW_LOAD_SCENE_STOP()
		ENDIF
		REMOVE_IPL("MG-Flight School 5")
		
		//store the player's camera views of the heli and plane immediately, so they can be restored when flight school terminates.
		cvmHelicopter = GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_HELI)
		cvmStuntPlane = GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_AIRCRAFT)
			
		//SET_PLAYER_INVINCIBLE(PLAYER_ID(), TRUE)
		
		MINIGAME_DISPLAY_MISSION_TITLE(ENUM_TO_INT(MINIGAME_PILOT_SCHOOL))
		SET_PLAYERPAD_SHAKES_WHEN_CONTROLLER_DISABLED(FALSE)
		SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_FADE)
	//	DISPLAY_HUD(FALSE)
		DISABLE_SELECTOR()

		
		SET_MINIMAP_COMPONENT(MINIMAP_COMPONENT_RUNWAY_1, FALSE)
		SET_SCENARIO_TYPE_ENABLED("WORLD_VEHICLE_DRIVE_SOLO", FALSE)
		CLEAR_HELP()
		IF NOT GET_MISSION_FLAG()
			SET_MISSION_FLAG(TRUE)
		ENDIF
		
		SET_MAX_WANTED_LEVEL(0)
		SET_WANTED_LEVEL_MULTIPLIER(0)
		SUPPRESS_EMERGENCY_CALLS()
		ENABLE_ALL_DISPATCH_SERVICES(FALSE)
		SUPPRESS_RESTRICTED_AREA_WANTED_LEVEL(AC_AIRPORT_AIRSIDE)
		SUPPRESS_RESTRICTED_AREA_WANTED_LEVEL(AC_GOLF_COURSE)
		SUPPRESS_RESTRICTED_AREA_WANTED_LEVEL(AC_MILITARY_BASE)
		SUPPRESS_RESTRICTED_AREA_WANTED_LEVEL(AC_PRISON)
		SUPPRESS_RESTRICTED_AREA_WANTED_LEVEL(AC_BIOTECH)
		SUPPRESS_RESTRICTED_AREA_WANTED_LEVEL(AC_MILITARY_DOCKS)
		SUPPRESS_RESTRICTED_AREA_WANTED_LEVEL(AC_MOVIE_STUDIO)
		g_bDeactivateRestrictedAreas = true
		

		PS_Main.playerVehicle = GET_PLAYERS_LAST_VEHICLE()
		IF DOES_ENTITY_EXIST(PS_Main.playerVehicle) AND IS_VEHICLE_DRIVEABLE(PS_Main.playerVehicle)
			SET_ENTITY_AS_MISSION_ENTITY(PS_Main.playerVehicle, TRUE, TRUE)
			GET_VEHICLE_TRAILER_VEHICLE(PS_Main.playerVehicle, PS_Main.playerTrailer)
			IF DOES_ENTITY_EXIST(PS_Main.playerTrailer)
				SET_ENTITY_AS_MISSION_ENTITY(PS_Main.playerTrailer, TRUE, TRUE)
			ENDIF
			//IF g_current_selected_dlc_PilotSchool_class != PSCD_DLC_ShootingRange
				FREEZE_ENTITY_POSITION(PS_Main.playerVehicle, TRUE)
			//ENDIF
		ENDIF
		
		PS_PRINT_USEFUL_MP_DEBUG()
		
		DISABLE_CELLPHONE(TRUE)
		DISABLE_INTERACTION_MENU()
		
		KILL_ANY_CONVERSATION()
		#IF IS_DEBUG_BUILD
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
		#ENDIF
		
		FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
		DEBUG_MESSAGE("******************** INIT SCRIPT: Pilot_School.sc ********************")
	ENDIF
ENDPROC

PROC PS_INCREMENT_MAIN_SUBSTATE()
	SWITCH eGameSubState
		CASE PS_SUBSTATE_ENTER
			PRINTLN("[PS_INCREMENT_MAIN_SUBSTATE], PS_SUBSTATE_ENTER >>> PS_SUBSTATE_UPDATE ")
			eGameSubState = PS_SUBSTATE_UPDATE
			BREAK
		CASE PS_SUBSTATE_UPDATE
			PRINTLN("[PS_INCREMENT_MAIN_SUBSTATE], PS_SUBSTATE_UPDATE >>> PS_SUBSTATE_EXIT ")
			eGameSubState = PS_SUBSTATE_EXIT
			BREAK
		CASE PS_SUBSTATE_EXIT
			PRINTLN("[PS_INCREMENT_MAIN_SUBSTATE], PS_SUBSTATE_EXIT >>> PS_SUBSTATE_ENTER ")
			eGameSubState = PS_SUBSTATE_ENTER
			BREAK
	ENDSWITCH
ENDPROC

PROC PS_SETUP_FUNCTION_POINTERS()
	
	
	// DLC POINTERS
	SWITCH(g_current_selected_dlc_PilotSchool_class)
		
		
		// DLC
		CASE PSCD_DLC_OutsideLoop
			PSC_InitProc = &PS_DLC_Outside_Loop_Initialise
			PSC_PreviewFunc = &PS_DLC_Outside_Loop_Preview
			PSC_SetupProc = &PS_DLC_Outside_Loop_MainSetup
			PSC_UpdateFunc = &PS_DLC_Outside_Loop_MainUpdate
			PSC_CleanupProc = &PS_DLC_Outside_Loop_MainCleanup
			#IF IS_DEBUG_BUILD
				PSC_DBG_FORCE_FAIL = &PS_DLC_OUTSIDE_LOOP_FORCE_FAIL
				PSC_DBG_FORCE_PASS = &PS_DLC_OUTSIDE_LOOP_FORCE_PASS
			#ENDIF
			BREAK
			
		CASE PSCD_DLC_CollectFlags
			PSC_InitProc = &PS_DLC_Collect_Flags_Initialise
			PSC_PreviewFunc = &PS_DLC_Collect_Flags_Preview
			PSC_SetupProc = &PS_DLC_Collect_Flags_MainSetup
			PSC_UpdateFunc = &PS_DLC_Collect_Flags_MainUpdate
			PSC_CleanupProc = &PS_DLC_Collect_Flags_MainCleanup
			#IF IS_DEBUG_BUILD
				PSC_DBG_FORCE_FAIL = &PS_DLC_Collect_Flags_FORCE_FAIL
				PSC_DBG_FORCE_PASS = &PS_DLC_Collect_Flags_FORCE_PASS
			#ENDIF
			BREAK
		
		CASE PSCD_DLC_FollowLeader
			PSC_InitProc = &PS_DLC_Follow_Leader_Initialise
			PSC_PreviewFunc = &PS_DLC_Follow_Leader_Preview
			PSC_SetupProc = &PS_DLC_Follow_Leader_MainSetup
			PSC_UpdateFunc = &PS_DLC_Follow_Leader_MainUpdate
			PSC_CleanupProc = &PS_DLC_Follow_Leader_MainCleanup
			#IF IS_DEBUG_BUILD
				PSC_DBG_FORCE_FAIL = &PS_DLC_FOLLOW_LEADER_FORCE_FAIL
				PSC_DBG_FORCE_PASS = &PS_DLC_FOLLOW_LEADER_FORCE_PASS
			#ENDIF
			BREAK
			
		CASE PSCD_DLC_FlyLow
			PSC_InitProc = &PS_DLC_Fly_Low_Initialise
			PSC_PreviewFunc = &PS_DLC_Fly_Low_Preview
			PSC_SetupProc = &PS_DLC_Fly_Low_MainSetup
			PSC_UpdateFunc = &PS_DLC_Fly_Low_MainUpdate
			PSC_CleanupProc = &PS_DLC_Fly_Low_MainCleanup
			#IF IS_DEBUG_BUILD
				PSC_DBG_FORCE_FAIL = &PS_DLC_FLY_LOW_FORCE_FAIL
				PSC_DBG_FORCE_PASS = &PS_DLC_FLY_LOW_FORCE_PASS
			#ENDIF
			BREAK
			
		CASE PSCD_DLC_VehicleLanding
			PSC_InitProc = &PS_DLC_Vehicle_Landing_Initialise
			PSC_PreviewFunc = &PS_DLC_Vehicle_Landing_Preview
			PSC_SetupProc = &PS_DLC_Vehicle_Landing_MainSetup
			PSC_UpdateFunc = &PS_DLC_Vehicle_Landing_MainUpdate
			PSC_CleanupProc = &PS_DLC_Vehicle_Landing_MainCleanup
			#IF IS_DEBUG_BUILD
				PSC_DBG_FORCE_FAIL = &PS_DLC_VEHICLE_LANDING_FORCE_FAIL
				PSC_DBG_FORCE_PASS = &PS_DLC_VEHICLE_LANDING_FORCE_PASS
			#ENDIF
			BREAK
			
		CASE PSCD_DLC_Engine_failure
			PSC_InitProc = &PS_DLC_Engine_Failure_Initialise
			PSC_PreviewFunc = &PS_DLC_Engine_Failure_Preview
			PSC_SetupProc = &PS_DLC_Engine_Failure_MainSetup
			PSC_UpdateFunc = &PS_DLC_Engine_Failure_MainUpdate
			PSC_CleanupProc = &PS_DLC_Engine_Failure_MainCleanup
			#IF IS_DEBUG_BUILD
				PSC_DBG_FORCE_FAIL = &PS_DLC_Engine_Failure_FORCE_FAIL
				PSC_DBG_FORCE_PASS = &PS_DLC_Engine_Failure_FORCE_PASS
			#ENDIF
		BREAK

		CASE PSCD_DLC_CityLanding // titan fall
			PSC_InitProc = &PS_DLC_CITY_Initialise
			PSC_PreviewFunc = &PS_DLC_CITY_Preview
			PSC_SetupProc = &PS_DLC_CITY_MainSetup
			PSC_UpdateFunc = &PS_DLC_CITY_MainUpdate
			PSC_CleanupProc = &PS_DLC_CITY_MainCleanup
			#IF IS_DEBUG_BUILD
				PSC_DBG_FORCE_FAIL = &PS_DLC_CITY_FORCE_FAIL
				PSC_DBG_FORCE_PASS = &PS_DLC_CITY_FORCE_PASS
			#ENDIF
		BREAK
		
		CASE PSCD_DLC_ChaseParachute
			PSC_InitProc = &PS_DLC_Chase_Parachute_Initialise
			PSC_PreviewFunc = &PS_DLC_Chase_Parachute_Preview
			PSC_SetupProc = &PS_DLC_Chase_Parachute_MainSetup
			PSC_UpdateFunc = &PS_DLC_Chase_Parachute_MainUpdate
			PSC_CleanupProc = &PS_DLC_Chase_Parachute_MainCleanup
			#IF IS_DEBUG_BUILD
				PSC_DBG_FORCE_FAIL = &PS_DLC_Chase_Parachute_FORCE_FAIL
				PSC_DBG_FORCE_PASS = &PS_DLC_Chase_Parachute_FORCE_PASS
			#ENDIF
			BREAK
			
		CASE PSCD_DLC_ShootingRange
			PSC_InitProc = &PS_DLC_Shooting_Range_Initialise
			PSC_PreviewFunc = &PS_DLC_Shooting_Range_Preview
			PSC_SetupProc = &PS_DLC_Shooting_Range_MainSetup
			PSC_UpdateFunc = &PS_DLC_Shooting_Range_MainUpdate
			PSC_CleanupProc = &PS_DLC_Shooting_Range_MainCleanup
			#IF IS_DEBUG_BUILD
				PSC_DBG_FORCE_FAIL = &PS_DLC_SHOOTING_RANGE_FORCE_FAIL
				PSC_DBG_FORCE_PASS = &PS_DLC_SHOOTING_RANGE_FORCE_PASS
			#ENDIF
			BREAK
			
		CASE PSCD_DLC_Formation
			PSC_InitProc = &PS_DLC_formation_Initialise
			PSC_PreviewFunc = &PS_DLC_Formation_Preview
			PSC_SetupProc = &PS_DLC_Formation_MainSetup
			PSC_UpdateFunc = &PS_DLC_Formation_MainUpdate
			PSC_CleanupProc = &PS_DLC_Formation_MainCleanup
			#IF IS_DEBUG_BUILD
				PSC_DBG_FORCE_FAIL = &PS_DLC_FORMATION_FORCE_FAIL
				PSC_DBG_FORCE_PASS = &PS_DLC_FORMATION_FORCE_PASS
			#ENDIF
			BREAK
			
		
		DEFAULT
			SCRIPT_ASSERT("selected challenge does not exist for some reason")
			BREAK
	ENDSWITCH
ENDPROC



PROC script_cleanup()

	
	PS_CLEANUP()
		
	MISSION_FLOW_CLEAR_DISPLAY_MISSION_TITLE()
	g_sFMMCEOM.iVoteStatus = ciFMMC_EOM_VOTE_STATUS_CONTINUE			
	RESET_FMMC_MISSION_VARIABLES(TRUE,FALSE,ciFMMC_END_OF_MISSION_STATUS_OVER)	
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC

PS_LAUNCH_ARGS psArgs
INT iPlayerMissionToLoad
INT iMissionVariation

SCRIPT (MP_MISSION_DATA fmmcMissionData)


	
		
	//Call the function that controls the setting up of this script
	FMMC_PROCESS_PRE_GAME_COMMON(fmmcMissionData, iPlayerMissionToLoad, iMissionVariation, MAX_NUM_PILOT_SCHOOL_PLAYERS, FALSE, FALSE)
	//Reserver stuff

	GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_MG_PILOT_SCHOOL

	//Script has started set the global broadcast data up so every body knows
	SET_FM_MISSION_LAUNCHED_SUCESS(fmmcMissionData.mdID.idCreator, fmmcMissionData.mdID.idVariation, fmmcMissionData.iInstanceId)
	//VECTOR vGameCamPos

	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD, SIZE_OF(playerBD))

	PS_INIT()
	

	

	IF IS_PLAYER_SPECTATING(player_id())
		BLOCK_SPECTATOR_COPYING_TARGET_FADES(TRUE)
		WHILE bGameIsRunning
		
			IF NETWORK_IS_GAME_IN_PROGRESS()
				MAINTAIN_SPEC_NOT_HOST(iManageSpechost, iDesiredHost)				
			ENDIF
		
			IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			OR SHOULD_PLAYER_LEAVE_MP_MISSION()
				IF bDoingMidLessonCam = TRUE
					DETACH_CAM(specData.specCamData.camCinematic)
					CLEANUP_SPECIAL_SPECTATOR_CAMERA(specData)
					bDoingMidLessonCam = FALSE
				ENDIF
				BLOCK_SPECTATOR_COPYING_TARGET_FADES(FALSE)
				SCRIPT_CLEANUP()
			ENDIF						
			
			PS_HANDLE_SPECTATOR()
			MP_LOOP_WAIT_ZERO()
		ENDWHILE
		IF BUSYSPINNER_IS_ON()
			BUSYSPINNER_OFF()
		ENDIF
		BLOCK_SPECTATOR_COPYING_TARGET_FADES(FALSE)
		DISABLE_SPECTATOR_FILTER_OPTION(FALSE)
		IF bDoingMidLessonCam = TRUE
			DETACH_CAM(specData.specCamData.camCinematic)
			CLEANUP_SPECIAL_SPECTATOR_CAMERA(specData)
			bDoingMidLessonCam = FALSE
		ENDIF
		SCRIPT_CLEANUP()
	ELSE
		vPilotSchoolSceneCoords = vPilotSchoolSceneCoords
		
		RESERVE_NETWORK_MISSION_VEHICLES(ciNUMBER_OF_PILOT_VEHICLES)	
		RESERVE_NETWORK_MISSION_PEDS(ciNUMBER_OF_PILOT_PEDS)
		RESERVE_NETWORK_MISSION_OBJECTS(ciNUMBER_OF_PILOT_OBJECTS)

		#IF IS_DEBUG_BUILD
			#IF PS_PRINT_STATS
				PS_INIT_DATA_FILE_WIDGET()
			#ENDIF
		#ENDIF				
		
		//main loop	
		WHILE bGameIsRunning
			cprintln(debug_trevor3,"game mode = ",eGameMode)
//			PRINTLN("game mode = ", eGameMode)
			
			NETWORK_PREVENT_SCRIPT_HOST_MIGRATION() //added as part of a potential fix for 1959199

			IF NETWORK_IS_GAME_IN_PROGRESS()
				MAINTAIN_SPEC_NOT_HOST(iManageSpechost, iDesiredHost)				
			ENDIF
			
			IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			OR SHOULD_PLAYER_LEAVE_MP_MISSION()
				SCRIPT_CLEANUP()
			ELSE
			
				MAINTAIN_CELEBRATION_PRE_LOAD(sCelebrationData)

			
				cprintln(debug_trevor3,"Transition = ",IS_TRANSITION_ACTIVE())
				cprintln(debug_trevor3,"Sky swoop = ",IS_SKYSWOOP_AT_GROUND())
			
				IF NOT IS_TRANSITION_ACTIVE()
				OR IS_SKYSWOOP_AT_GROUND()
					SET_IDLE_KICK_DISABLED_THIS_FRAME()
					
					#IF IS_DEBUG_BUILD
						#IF PS_PRINT_STATS
							IF eGameMode = PS_GAME_MODE_MENU
								PS_UPDATE_DATA_FILE_WIDGET()
							ENDIF
						#ENDIF
					#ENDIF
					
					SET_SCENARIO_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0,0.0)
					
					//global HUD stuff... running during menu, preview and challenges
					PS_HUD_UPDATE_FADE()
					
					//Hide hud stuff
					BOOL bHideTheRadar,bHideWeapons
					
					bHideWeapons = TRUE
					
					IF eGameMode = PS_GAME_MODE_CHALLENGE //AND eGameSubState = PS_SUBSTATE_UPDATE
						IF g_current_selected_dlc_PilotSchool_class = PSCD_DLC_ShootingRange
							bHideWeapons = FALSE
						ENDIF
						bHideTheRadar = FALSE
					ELSE
						bHideTheRadar = TRUE //hide the radar when not in a challenge
					ENDIF
					
					PS_SPEC_UPDATE_DATA()
					PS_SPEC_UPDATE_CHECKPOINTS()
					
					PS_HUD_HIDE_GAMEHUD_ELEMENTS(bHideTheRadar,bHideWeapons)
					
					PS_UPDATE_LEADERBOARD_WRITER()
					PS_HANDLE_MUSIC()

					//states: a.load menu b.menu c.course init d.load preview e. preview f.load challenge g.challenge h.quit to menu
					SWITCH(eGameMode)		
						CASE PS_GAME_MODE_INTRO_SCENE
							SWITCH(eGameSubState)
								CASE PS_SUBSTATE_ENTER
								
									IF PLAYSTATS_CREATE_MATCH_HISTORY_ID_2(serverBD.iMatchType, serverBD.iMatchHistoryID)																				
										DEBUG_MESSAGE("############################################################# starting intro scene")
										//cut scene setup stuff
										PS_INIT_INTRO_SCENE(psArgs)
										PS_INCREMENT_MAIN_SUBSTATE()
									ENDIF
								BREAK
								CASE PS_SUBSTATE_UPDATE
									//do cut scene here
									IF NOT IS_PED_INJURED(PLAYER_PED_ID())
										SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(), FALSE)
										IF NOT GET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID())
											cprintln(debug_trevor3,"SET CONTROL OFF A")
											//SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
											DO_NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
											HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(PLAYER_PED_ID(), TRUE)
											SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED)
										ENDIF
									ENDIF
								//	IF NOT PS_UPDATE_INTRO_SCENE(psArgs)
										PS_INCREMENT_MAIN_SUBSTATE()
								//	ENDIF
								BREAK
								CASE PS_SUBSTATE_EXIT
									PS_HANDLE_DLC_HELP_TEXT_COUNTER()
									g_current_selected_dlc_PilotSchool_class = PSCD_DLC_OutsideLoop
									eGameMode = PS_GAME_MODE_LOAD_MENU
									PS_INCREMENT_MAIN_SUBSTATE()
								BREAK
							ENDSWITCH
							BREAK
						CASE PS_GAME_MODE_LOAD_MENU
							IF IS_PC_VERSION()
								DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE)
							ENDIF
							
							REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
							
							SWITCH(eGameSubState)
								CASE PS_SUBSTATE_ENTER
									DEBUG_MESSAGE("############################################################# loading menu")
								//	DEBUG_PRINT_SCORES(g_current_selected_dlc_PilotSchool_class,"Main : PS_GAME_MODE_LOAD_MENU : PS_SUBSTATE_ENTER")
			//						IF bDontFadeToMenu OR PS_HUD_IS_SCREEN_DONE_FADING(PS_SCREEN_FADE_OUT, 1000)
									IF PS_MENU_SETUP()
										PS_CLEANUP_PLAYER_AND_VEHICLES()							
										PS_INCREMENT_MAIN_SUBSTATE()
										FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
										
										// B* 2276436
										CLOSE_MP_TEXT_CHAT()
										MP_TEXT_CHAT_DISABLE(TRUE)
										
									ENDIF
									
			//						ENDIF
						
									BREAK
								CASE PS_SUBSTATE_UPDATE
									//wait while shit is loading
									DEBUG_MESSAGE("waiting for shit to load...")
									PS_INCREMENT_MAIN_SUBSTATE()
									BREAK
								CASE PS_SUBSTATE_EXIT
									eGameMode = PS_GAME_MODE_MENU
									PS_INCREMENT_MAIN_SUBSTATE()
									BREAK
							ENDSWITCH
							BREAK
						
						CASE PS_GAME_MODE_MENU
							DISABLE_RADAR_MAP(TRUE)
							REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
							//draw menu
							IF IS_PC_VERSION()
								DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE)
							ENDIF
							SWITCH(eGameSubState)
								CASE PS_SUBSTATE_ENTER
									IF PS_HUD_IS_SCREEN_DONE_FADING(PS_SCREEN_FADE_IN, 1000)
										PS_HANDLE_DLC_HELP_TEXT_PRINTING()
										DO_NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, TRUE)
										PS_INCREMENT_MAIN_SUBSTATE()
									
									ENDIF 
									BREAK
								CASE PS_SUBSTATE_UPDATE						
									//SET_PLAYER_INVINCIBLE(PLAYER_ID(),TRUE)
									SET_IDLE_KICK_DISABLED_THIS_FRAME()
									IF NOT PS_MENU_UPDATE() //change this to just grab the input
										PS_INCREMENT_MAIN_SUBSTATE()
									ENDIF
									BREAK
								CASE PS_SUBSTATE_EXIT
									IF PS_HUD_IS_SCREEN_DONE_FADING(PS_SCREEN_FADE_OUT, 1000)
										DEBUG_MESSAGE("############################################################# running menu cleanup")
										CPRINTLN(debug_Trevor3,"Menu Cleanup")
										PS_MENU_CLEANUP()
										eGameMode = PS_GAME_MODE_COURSE_INIT
										PS_INCREMENT_MAIN_SUBSTATE()
									ENDIF 
									BREAK
							ENDSWITCH
							BREAK
							
						CASE PS_GAME_MODE_COURSE_INIT
							SWITCH(eGameSubState)
								CASE PS_SUBSTATE_ENTER
									cprintln(debug_trevor3,"PS_GAME_MODE_COURSE_INIT : PS_SUBSTATE_ENTER")
									
									DO_NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE, TRUE)
									GIVE_PLAYER_HEALTH()
									
									RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_PILOTSCHOOL/CITY_LANDING")
									RELEASE_NAMED_SCRIPT_AUDIO_BANK("Alarms")
									
									//Ensure will be checked before progressing
									bPlayerSpawned = FALSE
									
									currentLessonMusic = PS_MUSIC_NONE
									IF PS_HUD_IS_SCREEN_DONE_FADING(PS_SCREEN_FADE_OUT, 1000)										
										serverBD.iSelectedLesson = enum_to_int(g_current_selected_dlc_PilotSchool_class)
										serverBD.bPlayerSelectingLesson = FALSE
										serverBD.bPlayerEndedLesson = FALSE
										cprintln(debug_trevor3,"#!2 bPlayerSelectingLesson = FALSE")
										cprintln(debug_trevor3,"#!2 bPlayerEndedLesson = FALSE")
										cprintln(debug_trevor3,"#!2 iSelectedLesson = ",enum_to_int(g_current_selected_dlc_PilotSchool_class))
										PS_SET_WEATHER_FOR_CHALLENGE()
										IF IS_TIMER_STARTED(PS_Main.tRaceTimer) //this timer doesn't seem to stop after a lesosn
											CANCEL_TIMER(PS_Main.tRaceTimer)
										ENDIF
										
										PRINTLN("g_LastMedalAchieved = ",ENUM_TO_INT(saveData[g_current_selected_dlc_PilotSchool_class].eMedal))
										g_LastMedalAchieved = ENUM_TO_INT(saveData[g_current_selected_dlc_PilotSchool_class].eMedal) //store last achieved medal for celebration calculation
										PS_INCREMENT_MAIN_SUBSTATE()
										
										MP_TEXT_CHAT_DISABLE(FALSE) // B* 2276436
										
									ENDIF
								BREAK
								CASE PS_SUBSTATE_UPDATE
									cprintln(debug_trevor3,"PS_GAME_MODE_COURSE_INIT : PS_SUBSTATE_UPDATE")
										IF NOT bwaitingOnSomething
										OR NOT bPlayerSpawned
											PS_CLEANUP_PLAYER_AND_VEHICLES()
											DEBUG_MESSAGE("############################################################# running course init")
											TRIGGER_SCREENBLUR_FADE_OUT(0)
											PS_SETUP_FUNCTION_POINTERS()
											PS_CLEANUP_PLAYER()
											PS_INIT_CHALLENGE_VARIABLES()
											PS_SETUP_DISPATCHER()
											GIVE_PLAYER_HEALTH()

											//setup for escaping death
											cprintln(debug_trevor3,"SET_FAIL_FADE_EFFECT_SCRIPT_CONTROL(TRUE)")
											SET_FAIL_FADE_EFFECT_SCRIPT_CONTROL(TRUE)
											
											PRINTLN("CHECKING PLAYER IS ALIVE...")
											//Fix for course starting while player dead
											IF IS_PLAYER_DEAD(PLAYER_ID())
											OR IS_PED_DEAD_OR_DYING(PLAYER_PED_ID())
											OR GET_ENTITY_HEALTH(PLAYER_PED_ID()) = 0
											OR IS_PED_INJURED(PLAYER_PED_ID())
												PRINTLN("...PLAYER IS DEAD WAITING TO START COURSE")
												
												//Force the player into a respawn state if dead and null
												IF GET_MANUAL_RESPAWN_STATE() = MRS_NULL
													PRINTLN("PLAYER HAD NO RESPAWN STATE - SETTING ONE IN PS_GAME_MODE_COURSE_INIT")
													SET_MANUAL_RESPAWN_STATE(MRS_RESPAWN_PLAYER_HIDDEN)
												ENDIF
												
												bPlayerSpawned = false
											ELSE
												PRINTLN("...PLAYER IS ALIVE NOT WAITING TO SPAWN bPlayerSpawned = true ")
												SET_PLAYER_INVINCIBLE(PLAYER_ID(), TRUE)
												CPRINTLN(debug_trevor3,"CHECK INVINCIBLE E: TRUE")
												bwaitingOnSomething = TRUE
												bPlayerSpawned = TRUE
											ENDIF	
										ELSE
											cprintln(debug_trevor3,"player not respawning")
											CALL PSC_InitProc()
											IF NOT bwaitingOnSomething
												cprintln(debug_trevor3,"finished init")
												PS_INCREMENT_MAIN_SUBSTATE()
											ENDIF
										ENDIF
									BREAK
								CASE PS_SUBSTATE_EXIT
									IF ANIMPOSTFX_IS_RUNNING("MinigameTransitionIn")
										ANIMPOSTFX_STOP("MinigameTransitionIn")
									ENDIF
									IF PS_Main.myChallengeData.HasBeenPreviewed  OR bRetryChallenge
										//we want to skip preview on retry or if the lesson has been passed
										DEBUG_MESSAGE("############################################################# skipping preview")
										if DOES_ENTITY_EXIST(PS_Main.myVehicle)
											IF IS_VEHICLE_DRIVEABLE(PS_Main.myVehicle)
												FREEZE_ENTITY_POSITION(PS_Main.myVehicle, FALSE)
											ENDIF
										ENDIF
										
										eGameMode = PS_GAME_MODE_PREVIEW
									ELSE
										eGameMode = PS_GAME_MODE_LOAD_PREVIEW
										PS_INCREMENT_MAIN_SUBSTATE()
									ENDIF
									BREAK
							ENDSWITCH	
							BREAK
						
						CASE PS_GAME_MODE_LOAD_PREVIEW
							DISABLE_RADAR_MAP(FALSE)
							SWITCH(eGameSubState)
								CASE PS_SUBSTATE_ENTER
									//we're already faded out here so just start loading
									DEBUG_MESSAGE("############################################################# loading preview")
									//LOAD_SCENE(vPilotSchoolPreviewCoords)
									IF NOT IS_ENTITY_DEAD(PS_Main.myVehicle) AND DOES_ENTITY_EXIST(PS_Main.myVehicle)
										SET_ENTITY_HEALTH(PS_Main.myVehicle, 1500)
										
										// Ensure the vehicle is at the start of the preview to improve the scene loading.
										SET_ENTITY_COORDS(PS_Main.myVehicle, vPilotSchoolPreviewCoords)
									//	IF g_current_selected_dlc_PilotSchool_class != PSCD_DLC_ShootingRange
										IF IS_VEHICLE_DRIVEABLE(PS_Main.myVehicle)
											FREEZE_ENTITY_POSITION(PS_Main.myVehicle, TRUE)
										ENDIF
									//	ENDIF
									ENDIF
									RENDER_SCRIPT_CAMS(FALSE, FALSE)
									
									
								//	SET_OVERRIDE_WEATHER("EXTRASUNNY")
								//	iSavedTimeOfDay = GET_CLOCK_HOURS()
									
									sfTelevisionBorder = REQUEST_SCALEFORM_MOVIE("TV_FRAME")
									START_TIMER_NOW_SAFE(streamingTimer)
									
									//this is what the script is currently using.
									//streamVolume = STREAMVOL_CREATE_FRUSTUM(vPilotSchoolPreviewCoords, PS_CONVERT_ROTATION_TO_DIRECTION(vPilotSchoolPreviewRot), 800, FLAG_MAPDATA | FLAG_COLLISIONS_MOVER)
									
									//attempted to use spheres.  They seem to work, but can't be resized to a large degree without killing the game.
									//streamVolume = STREAMVOL_CREATE_SPHERE(vPilotSchoolPreviewCoords, 400, FLAG_MAPDATA | FLAG_COLLISIONS_MOVER)
									//vPilotSchoolPreviewRot = vPilotSchoolPreviewRot
									
									//vGameCamPos = GET_GAMEPLAY_CAM_COORD()
									
									//testing new load scene.
									IF NEW_LOAD_SCENE_START_SPHERE(vPilotSchoolPreviewCoords, 1200.0)
										vPilotSchoolPreviewRot = vPilotSchoolPreviewRot
										//Set multihead blinders on early
										SET_MULTIHEAD_SAFE(TRUE,TRUE)
										
										PS_INCREMENT_MAIN_SUBSTATE()
									ENDIF
								
									
									//IF STREAMVOL_IS_VALID(streamVolume)
										//PS_INCREMENT_MAIN_SUBSTATE()
									//ENDIF
									BREAK
								CASE PS_SUBSTATE_UPDATE
									IF HAS_SCALEFORM_MOVIE_LOADED(sfTelevisionBorder)
										//draw early to account for delay
										IF HAS_SCALEFORM_MOVIE_LOADED(sfTelevisionBorder)
											DRAW_SCALEFORM_MOVIE_FULLSCREEN(sfTelevisionBorder, 255, 255, 255, 255)
										ENDIF
										
										//IF (STREAMVOL_HAS_LOADED(streamVolume) AND GET_TIMER_IN_SECONDS(streamingTimer) > 3.0) OR TIMER_DO_ONCE_WHEN_READY(streamingTimer, 10.0)
										//	PS_INCREMENT_MAIN_SUBSTATE()
										//ENDIF
										
										//ensure the new scene is active and loaded.  
										IF IS_NEW_LOAD_SCENE_ACTIVE() OR TIMER_DO_WHEN_READY(streamingTimer, 12.0)
											IF IS_NEW_LOAD_SCENE_LOADED() OR TIMER_DO_WHEN_READY(streamingTimer, 12.0)
												CANCEL_TIMER(streamingTimer)
												NEW_LOAD_SCENE_STOP()
												PS_INCREMENT_MAIN_SUBSTATE()
											ENDIF
										ENDIF
											
									ENDIF
									BREAK
								CASE PS_SUBSTATE_EXIT
									//draw early to account for delay
									IF HAS_SCALEFORM_MOVIE_LOADED(sfTelevisionBorder)
										DRAW_SCALEFORM_MOVIE_FULLSCREEN(sfTelevisionBorder, 255, 255, 255, 255)
									ENDIF
									IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
										SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
										SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10.0)
									ELSE
										SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
										SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
									ENDIF
									IF NOT IS_VEHICLE_FUCKED(PS_Main.myVehicle)
										SET_ENTITY_INVINCIBLE(PS_Main.myVehicle, TRUE)
									ENDIF
									// Unfreeze the vehicle before the recording begins.
									if DOES_ENTITY_EXIST(PS_Main.myVehicle)
										IF IS_VEHICLE_DRIVEABLE(PS_Main.myVehicle)
											FREEZE_ENTITY_POSITION(PS_Main.myVehicle, FALSE)
										ENDIF
									ENDIF
									eGameMode = PS_GAME_MODE_PREVIEW
									PS_INCREMENT_MAIN_SUBSTATE()
									BREAK
							ENDSWITCH
							BREAK
							
						CASE PS_GAME_MODE_PREVIEW
							DISABLE_RADAR_MAP(FALSE)
							//playe preview cutscene
							IF CALL PSC_PreviewFunc()
								SWITCH(eGameSubState)
									CASE PS_SUBSTATE_ENTER
										IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
											SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
											SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10.0)
										ENDIF
										IF PS_HUD_IS_SCREEN_DONE_FADING(PS_SCREEN_FADE_IN, 1000)
											
				//							IF IS_NEW_LOAD_SCENE_ACTIVE()
				//								NEW_LOAD_SCENE_STOP()
				//							ENDIF
											DEBUG_MESSAGE("############################################################# running preview start")
											PS_INCREMENT_MAIN_SUBSTATE()
										ENDIF 
										BREAK
									CASE PS_SUBSTATE_UPDATE
										IF PS_PREVIEW_CHECK_FOR_SKIP()
											PS_INCREMENT_MAIN_SUBSTATE()
										ENDIF
										BREAK
									CASE PS_SUBSTATE_EXIT
										IF PS_HUD_IS_SCREEN_DONE_FADING(PS_SCREEN_FADE_OUT, 1000)
								
									
											IF IS_STREAMVOL_ACTIVE()
												STREAMVOL_DELETE(streamVolume)
											ENDIF
											KILL_ANY_CONVERSATION()
											KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()//sometimes can hear dialogue playing after the skipped preview, so kill it and don't finish last line
											
											DEBUG_MESSAGE("############################################################# cleaning up preview")
											PS_PREVIEW_CLEANUP_CUTSCENE()
											//Set multihead blinders off
											SET_MULTIHEAD_SAFE(FALSE)
											//resetting var here so we can skip preview. 
											bRetryChallenge = FALSE
											eGameMode = PS_GAME_MODE_LOAD_CHALLENGE
											PS_INCREMENT_MAIN_SUBSTATE()
										ENDIF 
										BREAK
								ENDSWITCH
							ENDIF
						BREAK
						
						CASE PS_GAME_MODE_LOAD_CHALLENGE
							DISABLE_RADAR_MAP(FALSE)
							SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
							SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10)
							SWITCH(eGameSubState)
								CASE PS_SUBSTATE_ENTER
									//we're already faded out here
									DEBUG_MESSAGE("############################################################# loading challenge")
									IF CALL PSC_SetupProc()
										
										IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
											CLEAR_PED_WETNESS(PLAYER_PED_ID())
											CLEAR_PED_ENV_DIRT(PLAYER_PED_ID())
											CLEAR_PED_BLOOD_DAMAGE(PLAYER_PED_ID())
											RESET_PED_VISIBLE_DAMAGE(PLAYER_PED_ID())
										ENDIF
										
										//if the helicopter view is set to overhead when retrying, ensure it's recent to behind the helicopter.
										IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_HELI) = CAM_VIEW_MODE_THIRD_PERSON_FAR
											SET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_HELI, CAM_VIEW_MODE_THIRD_PERSON_MEDIUM)
										ENDIF
										
										START_TIMER_NOW_SAFE(streamingTimer)
										PS_INCREMENT_MAIN_SUBSTATE()
									ENDIF
								BREAK
								CASE PS_SUBSTATE_UPDATE
									IF (IS_NEW_LOAD_SCENE_ACTIVE() AND IS_NEW_LOAD_SCENE_LOADED()) OR TIMER_DO_ONCE_WHEN_READY(streamingTimer, 10.0)
										NEW_LOAD_SCENE_STOP()
										IF GET_ENTITY_MODEL(PS_Main.myVehicle) <> MAVERICK AND DOES_ENTITY_EXIST(PS_Main.myVehicle)
											IF IS_VEHICLE_DRIVEABLE(PS_Main.myVehicle)
												FREEZE_ENTITY_POSITION(PS_Main.myVehicle, FALSE)
											ENDIF
										ENDIF
										PS_INCREMENT_MAIN_SUBSTATE()
									ENDIF
									BREAK
								CASE PS_SUBSTATE_EXIT
									IF NOT IS_VEHICLE_FUCKED(PS_Main.myVehicle)
										SET_ENTITY_INVINCIBLE(PS_Main.myVehicle, FALSE)
									ENDIF
									
									//Remove invincibility from player
									SET_PLAYER_INVINCIBLE(PLAYER_ID(), FALSE)									
									eGameMode = PS_GAME_MODE_CHALLENGE									
									PS_INCREMENT_MAIN_SUBSTATE()
									BREAK
							ENDSWITCH
							BREAK
							
						CASE PS_GAME_MODE_CHALLENGE
							SWITCH g_current_selected_dlc_PilotSchool_class
								//for some new dlc pilot school classes disable vehicle attacks during challenges
								CASE PSCD_DLC_OutsideLoop
								CASE PSCD_DLC_CollectFlags
								CASE PSCD_DLC_FollowLeader
								CASE PSCD_DLC_FlyLow
								CASE PSCD_DLC_VehicleLanding
								CASE PSCD_DLC_Engine_Failure
								CASE PSCD_DLC_Formation
									PS_DISABLE_VEHICLE_ATTACKS()
								BREAK
							ENDSWITCH
							SWITCH(eGameSubState)
								CASE PS_SUBSTATE_ENTER
									PS_HUD_START_FADE(PS_SCREEN_FADE_IN, 1000)
									IF IS_NEW_LOAD_SCENE_ACTIVE()
										NEW_LOAD_SCENE_STOP()
									ENDIF
									SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10)
									DISABLE_CELLPHONE(TRUE)
									DEBUG_MESSAGE("############################################################# starting challenge")
									
									IF GET_FM_JOB_ENTERY_TYPE() = ciMISSION_ENTERY_TYPE_INVALID
										SET_FM_JOB_ENTERY_TYPE(ciMISSION_ENTERY_TYPE_QUICK_RESTART)
									ENDIF
									
									DEAL_WITH_FM_MATCH_START(FMMC_TYPE_MG_PILOT_SCHOOL, serverBD.iMatchHistoryID, serverBD.iMatchType, DEFAULT, enum_to_int(g_current_selected_dlc_PilotSchool_class)) //bug 2009130. Call when lesson starts
									bFM_MATCH_STARTED = TRUE
									PS_INCREMENT_MAIN_SUBSTATE()
									BREAK
								CASE PS_SUBSTATE_UPDATE									
									IF NOT CALL PSC_UpdateFunc()
										PS_INCREMENT_MAIN_SUBSTATE()
									ENDIF
									BREAK
								CASE PS_SUBSTATE_EXIT
									DEBUG_MESSAGE("############################################################# running game cleanup")
									cprintln(debug_trevor3,"pilot_school_mp : exit state : elapsed time ",savedata[0].ElapsedTime)
									#IF IS_DEBUG_BUILD
										#IF PS_PRINT_STATS
											PS_PRINT_LESSON_RECORD()
										#ENDIF
									#ENDIF
									cprintln(debug_trevor3,"pilot_school_mp : exit stateb : elapsed time ",savedata[0].ElapsedTime)
									CALL PSC_CleanupProc()
									cprintln(debug_trevor3,"pilot_school_mp : exit statec : elapsed time ",savedata[0].ElapsedTime)
									
									//set player vehicle as no longer needed if it still exists
									//removed for bug 1978271. The player's vehicle gets removed as soon as player is repositioned for front end menu.
								/*	IF NETWORK_DOES_NETWORK_ID_EXIST(PS_Main.myVehicle_netID)
										CLEANUP_NET_ID(PS_Main.myVehicle_netID)
									ENDIF */
									
									//SET_PLAYER_INVINCIBLE(PLAYER_ID(),TRUE)							
									currentLessonMusic = PS_MUSIC_NONE
								//	serverBD.bPlayerSelectingLesson = TRUE
									cprintln(debug_trevor3,"#!3 bPlayerSelectingLesson = TRUE")
									
									IF IS_PLAYER_DEAD(player_id())
									OR IS_PED_INJURED(PLAYER_PED_ID())
									OR GET_ENTITY_HEALTH(PLAYER_PED_ID()) = 0.0
										SET_MANUAL_RESPAWN_STATE(MRS_RESPAWN_PLAYER_HIDDEN)
									ENDIF
									
									cprintln(debug_trevor3,"RESET_CELEBRATION_PRE_LOAD")
									RESET_CELEBRATION_PRE_LOAD(sCelebrationData)
									
									IF bEndScreenQuitAlert
										eGameMode = PS_GAME_MODE_QUIT_FROM_MENU
										serverBD.bPlayerEndedLesson = TRUE
									ELIF bRetryChallenge	
										eGameMode = PS_GAME_MODE_COURSE_INIT
									ELSE
										IF IS_PLAYER_PLAYING(PLAYER_ID())
											eGameMode = PS_GAME_MODE_LOAD_MENU											
											serverBD.bPlayerSelectingLesson = TRUE
										ELSE
											eGameMode = PS_GAME_MODE_LOAD_MENU											
											serverBD.bPlayerSelectingLesson = TRUE
										ENDIF
									ENDIF
									
									PS_INCREMENT_MAIN_SUBSTATE()
									BREAK
							ENDSWITCH
							BREAK

						CASE PS_GAME_MODE_QUIT_FROM_MENU
							HIDE_SCRIPTED_HUD_COMPONENT_THIS_FRAME(HUD_MP_RANK_BAR)
							cprintln(debug_trevor3,"pilot_school_mp : exit state d : elapsed time ",savedata[0].ElapsedTime)
							SWITCH(eGameSubState)
								CASE PS_SUBSTATE_ENTER
									//DO_NET_SET_PLAYER_CONTROL(player_id(),FALSE,TRUE)
									FREEZE_ENTITY_POSITION(PLAYER_PED_ID(),TRUE)
									IF PS_HUD_IS_SCREEN_DONE_FADING(PS_SCREEN_FADE_OUT, 1000)
										PS_CLEANUP_PLAYER_AND_VEHICLES()
										DEBUG_MESSAGE("############################################################# quitting from menu")
										#IF IS_DEBUG_BUILD
											#IF PS_PRINT_STATS
												PS_PRINT_FLIGHT_SCHOOL_STATS()
												PS_END_FLIGHT_SCHOOL_DATA_FILE()
											#ENDIF
										#ENDIF
										
										// Freeze our screen on the airport shot
										SET_SKYFREEZE_FROZEN()
										
										IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
											CLEAR_PED_WETNESS(PLAYER_PED_ID())
											CLEAR_PED_ENV_DIRT(PLAYER_PED_ID())
											CLEAR_PED_BLOOD_DAMAGE(PLAYER_PED_ID())
											RESET_PED_VISIBLE_DAMAGE(PLAYER_PED_ID())
										ENDIF
										
										SET_ENTITY_COORDS(PLAYER_PED_ID(), PS_MENU_PLAYER_COORD)
										SET_ENTITY_HEADING(PLAYER_PED_ID(), PS_MENU_PLAYER_HEADING)
										//NEW_LOAD_SCENE_START_SPHERE(PS_MENU_PLAYER_COORD, 50)
										START_TIMER_NOW_SAFE(streamingTimer)
										
										PS_INCREMENT_MAIN_SUBSTATE()
									ENDIF 
									BREAK
								CASE PS_SUBSTATE_UPDATE
									
									// Not needed for MP since we skyswoop out
									//If IS_NEW_LOAD_SCENE_LOADED() OR GET_TIMER_IN_SECONDS_SAFE(streamingTimer) > 10000
									//NEW_LOAD_SCENE_STOP()	
										
									PS_CLEANUP()
									cprintln(debug_trevor3,"pilot_school_mp : exit state e : elapsed time ",savedata[0].ElapsedTime)
									//wait while shit is loading
									DEBUG_MESSAGE("waiting for shit to load...")
									PS_INCREMENT_MAIN_SUBSTATE()
									
									//ENDIF
								BREAK
								CASE PS_SUBSTATE_EXIT
									IF PS_HUD_IS_SCREEN_DONE_FADING(PS_SCREEN_FADE_IN, 1000)
										cprintln(debug_trevor3,"TERMINATED")								
										bGameIsRunning = FALSE
									ENDIF 
									BREAK
							ENDSWITCH
							BREAK
					ENDSWITCH
				ENDIF
			ENDIF
		//	PRINTLN("saveData[g_current_selected_dlc_PilotSchool_class].LandingDistance = ",saveData[g_current_selected_dlc_PilotSchool_class].LandingDistance)
			MP_LOOP_WAIT_ZERO() 
		ENDWHILE
		
		cprintln(debug_trevor3,"GAME IS NO LONGER RUNNING")
		
		script_cleanup()
	ENDIF
ENDSCRIPT
