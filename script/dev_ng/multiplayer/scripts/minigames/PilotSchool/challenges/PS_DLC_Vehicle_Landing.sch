//****************************************************************************************************
//
//	Author: 		Lukasz Bogaj / Mark Beagan
//	Date: 			12/12/13
//	Description:	"Vehicle Landing" Challenge for Pilot School. The player is required to land
//					a helicopter on a moving truck.
//	
//****************************************************************************************************

USING "minigames_helpers.sch"
USING "Pilot_School_MP_Challenge_Helpers.sch"

//Blip for trailer
BLIP_INDEX	bPS_DLCVehicleLandingTargetBlip

//Index of the truck driver
PED_INDEX targetDriver


//Position for vehicle
VECTOR vMovingVeh

//Flag for checkpoints being complete prior to landing truck
BOOL bCourseComplete = FALSE

//Flag for checking if the player has landed for sufficent time
BOOL bLandTimeCheck = FALSE



//Flag for drawing the ring on the trailer
BOOL bVehLandDrawRing = FALSE

//Flag for the angle change
BOOL bLandTimeChange = FALSE

//Value of time before marking landing a success
INT iLandTime = 0

//Value of time to allow change to land time
INT iChangeTime = 0

//CONSTS
CONST_INT PS_DLC_VEHICLE_LANDING_LANDING_CHECKPOINT 5
CONST_INT PS_DLC_VEHICLE_LAND_TIME 1000

//****************************************************************************************************
//****************************************************************************************************
//	Private functions that are used only by Vehicle Landing
//****************************************************************************************************
//****************************************************************************************************

PROC  PS_DLC_Vehicle_Landing_Init_Checkpoints()
	vPS_MovingTarget = << -1048.5212, -3211.0261, 12.9443>>
	PS_RESET_CHECKPOINT_MGR()
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_LANDING, <<-2857.9773, 2171.1316, 33.4674>>)//Final Checkpoint on truck
ENDPROC

//Create the target truck
FUNC BOOL PS_DLC_VEHICLE_LANDING_CREATE_TARGET_TRUCK()

	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(PS_FlatBedTruck_NETID)
		IF NOT PS_CREATE_NET_VEHICLE(PS_FlatBedTruck_NETID,PHANTOM, <<-2857.9773, 2171.1316, 33.4674>>, 299.2812)						
			RETURN FALSE
		ELSE		
			PS_FlatBedTruck = NET_TO_VEH(PS_FlatBedTruck_NETID)
			SET_VEHICLE_FIXED(PS_FlatBedTruck)
			SET_ENTITY_COORDS(PS_FlatBedTruck, <<-2857.9773, 2171.1316, 33.4674>>)
			SET_ENTITY_HEADING(PS_FlatBedTruck, 299.2812)
			SET_ENTITY_LOAD_COLLISION_FLAG(PS_FlatBedTruck,TRUE)
			
			SET_VEHICLE_ON_GROUND_PROPERLY(PS_FlatBedTruck)
		ENDIF
	ENDIF
	
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(PS_FlatBedTrailer_NETID)
		IF IS_VEHICLE_DRIVEABLE(PS_FlatBedTruck)
			IF NOT PS_CREATE_NET_VEHICLE(PS_FlatBedTrailer_NETID,FREIGHTTRAILER, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PS_FlatBedTruck, <<0.0, -10.0, 0.0>>), 299.2812)								
				RETURN FALSE
			ELSE
				PS_FlatBedTrailer = NET_TO_VEH(PS_FlatBedTrailer_NETID)
				SET_ENTITY_DYNAMIC(PS_FlatBedTrailer, TRUE) 
        		ACTIVATE_PHYSICS(PS_FlatBedTrailer)
				//SET_FOCUS_ENTITY(PS_FlatBedTrailer)
				SET_VEHICLE_FIXED(PS_FlatBedTrailer)
				SET_ENTITY_COORDS(PS_FlatBedTrailer, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PS_FlatBedTruck, <<0.0, -10.0, 0.0>>))
				SET_ENTITY_HEADING(PS_FlatBedTrailer, 299.2812)
				SET_VEHICLE_ON_GROUND_PROPERLY(PS_FlatBedTrailer)
				ATTACH_VEHICLE_TO_TRAILER(PS_FlatBedTruck, PS_FlatBedTrailer)
			ENDIF
		ELSE
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(targetDriver_NETID)
		IF IS_VEHICLE_DRIVEABLE(PS_FlatBedTruck)
			IF NOT PS_CREATE_NET_PED_IN_VEHICLE(targetDriver_NETID,PS_FlatBedTruck_NETID,PEDTYPE_MISSION, PilotModel, VS_DRIVER)
				RETURN FALSE
			ELSE
				targetDriver = NET_TO_PED(targetDriver_NETID)
				SET_ENTITY_AS_MISSION_ENTITY(targetDriver)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(targetDriver, TRUE)
				//SPECIAL_FUNCTION_DO_NOT_USE(targetDriver)
				//SET_PED_INTO_VEHICLE(targetDriver, PS_FlatBedTruck, VS_DRIVER)
			ENDIF
		ENDIF
	ENDIF

	RETURN TRUE
ENDFUNC



//Update the last landing checkpoint to the back of the truck location
PROC UPDATE_LANDING_LOCATION()
	//vMovingVeh = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PS_FlatBedTruck, <<0, -5, 1>>)
	if DOES_ENTITY_EXIST(PS_FlatBedTrailer)
		vMovingVeh = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PS_FlatBedTrailer, <<0.0, 0.0, 0.5>>)
		PS_Main.vCheckpointMarkerFlashPosition = vMovingVeh
	ENDIF
	
	if DOES_ENTITY_EXIST(PS_FlatBedTruck)
		PS_Main.myCheckpointz[PS_GET_TOTAL_CHECKPOINTS()].position = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PS_FlatBedTruck, <<0.0, -5.0, 0.0>>)
	ENDIF
ENDPROC

PROC PS_VEH_LAND_DRAW_MARKER_RING()
	if bVehLandDrawRing 
		UPDATE_LANDING_LOCATION()
		//Draw marker ring on the trailer
		DRAW_MARKER(MARKER_RING, <<vMovingVeh.x, vMovingVeh.y, vMovingVeh.z - 1.5>>, <<0,0,1>>, <<0,0,0>>, <<5, 5, 5>>, PS_PARA_TARG_COL_R, PS_PARA_TARG_COL_G, PS_PARA_TARG_COL_B, MG_GET_CHECKPOINT_ALPHA(vMovingVeh, 100), FALSE, FALSE)
	ENDIF
ENDPROC

//Check if the land timer is set and the time is up
FUNC BOOL CHECK_LAND_TIME()

VECTOR vRotLand

	vRotLand = GET_ENTITY_ROTATION(PS_Main.myVehicle, EULER_XYZ)

	IF bLandTimeCheck
		if iLandTime = 0
			IF (vRotLand.X < -10 OR vRotLand.X > 10) OR (vRotLand.Y < -10 OR vRotLand.Y > 10)
				iLandTime = GET_GAME_TIMER() + PS_DLC_VEHICLE_LAND_TIME + 2000
				//Set to false so time can change depending on landed angle
				bLandTimeChange = FALSE
			ELSE
				iLandTime = GET_GAME_TIMER() + PS_DLC_VEHICLE_LAND_TIME
				//Set as true as if change has happened so change won't occur
				bLandTimeChange = TRUE
			ENDIF
		ELSE
			//If the current angle is now not extreme add a timer to check how long it's in an acceptable position
			IF ((vRotLand.X > - 10 OR vRotLand.X < 10) AND (vRotLand.Y > -10 OR vRotLand.Y < 10)) AND bLandTimeChange = FALSE AND iChangeTime = 0
				iChangeTime = GET_GAME_TIMER() + 500
			ELSE
				iChangeTime = 0
			ENDIF
			
			//If the heli has sat in an acceptable angle for a duration of time change the land time
			IF ((vRotLand.X > - 10 OR vRotLand.X < 10) AND (vRotLand.Y > -10 OR vRotLand.Y < 10)) AND bLandTimeChange = FALSE AND GET_GAME_TIMER() > iChangeTime
				iLandTime = GET_GAME_TIMER() + PS_DLC_VEHICLE_LAND_TIME
				bLandTimeChange = TRUE
			ENDIF
			
			//If the angle changes back to a wide angle reset time
			IF ((vRotLand.X < - 10 OR vRotLand.X > 10) OR (vRotLand.Y < -10 OR vRotLand.Y > 10)) AND bLandTimeChange = TRUE
				iLandTime = GET_GAME_TIMER() + PS_DLC_VEHICLE_LAND_TIME + 2000
				bLandTimeChange = FALSE
			ENDIF
			
			//Return a successful land after a set duration
			if GET_GAME_TIMER() > iLandTime
				bLandTimeChange = FALSE
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//Reset the values for checking the land time
PROC RESET_LAND_TIME()
	PRINTSTRING("VEHICLE LANDING - RESETTING LAND TIME") PRINTNL()
	iLandTime = 0
	iChangeTime = 0
	bLandTimeCheck = FALSE
	bLandTimeChange = FALSE
ENDPROC

//Check if player has landed
FUNC BOOL PS_DLC_VEHICLE_LANDING_HAS_PLAYER_LANDED()

VECTOR vPlayerOffset

VECTOR vRotLand

	IF NOT IS_VEHICLE_FUCKED(PS_Main.myVehicle)
		vRotLand = GET_ENTITY_ROTATION(PS_Main.myVehicle, EULER_XYZ)

		vPlayerOffset = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(PS_FlatBedTrailer, GET_ENTITY_COORDS(PLAYER_PED_ID()))

		PRINTSTRING("VEHICLE LANDING - CHECKING IF PLAYER HAS LANDED") PRINTNL()
		PRINTSTRING("VEHICLE LANDING - ROTATION : <<") PRINTFLOAT(vRotLand.X) PRINTSTRING(", ") PRINTFLOAT(vRotLand.Y) PRINTSTRING(", ") PRINTFLOAT(vRotLand.Z) PRINTSTRING(">>") PRINTNL()
		PRINTSTRING("VEHICLE LANDING - OFFSET : <<") PRINTFLOAT(vPlayerOffset.X) PRINTSTRING(", ") PRINTFLOAT(vPlayerOffset.Y) PRINTSTRING(", ") PRINTFLOAT(vPlayerOffset.Z) PRINTSTRING(">>") PRINTNL()
		
		IF NOT IS_ENTITY_IN_AIR(PS_Main.myVehicle)
		AND GET_ENTITY_HEIGHT_ABOVE_GROUND(PS_Main.myVehicle) > 1.2				//Ensure player has landed above the ground
			PRINTSTRING("VEHICLE LANDING - VEHICLE ISNT IN THE AIR") PRINTNL()
			
			IF (vPlayerOffset.z < -2.5 OR vPlayerOffset.z > 4.5)
				RESET_LAND_TIME()
				RETURN FALSE
			ENDIF
			
			IF (vRotLand.X < -10 OR vRotLand.X > 10) OR (vRotLand.Y < -10 OR vRotLand.Y > 10)
				IF (vRotLand.X < -20 OR vRotLand.X > 20) OR (vRotLand.Y < -20 OR vRotLand.Y > 20)
					RESET_LAND_TIME()
					RETURN FALSE
				ELSE
					RETURN TRUE
				ENDIF
			ENDIF
			
			//Check player is near the trailer and landed at the correct height
			if (vPlayerOffset.z >= -0.5 AND vPlayerOffset.z < 0.8) AND GET_DISTANCE_BETWEEN_ENTITIES(PS_Main.myVehicle, PS_FlatBedTrailer) < 30
				PRINTSTRING("VEHICLE LANDING - HAS LANDED ON TRAILER") PRINTNL()
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
				RETURN TRUE
			ELSE
				PRINTSTRING("VEHICLE LANDING - APPEARS TO NOT BE ON TRAILER")
				PRINTSTRING("VEHICLE LANDING OFFSET <<") PRINTFLOAT(vPlayerOffset.x) PRINTSTRING(", ") PRINTFLOAT(vPlayerOffset.y) PRINTSTRING(", ") PRINTFLOAT(vPlayerOffset.z) PRINTSTRING(">>")
			ENDIF
			
			//Check for touching trailer
			/*IF NOT IS_ENTITY_DEAD(PS_FlatBedTrailer) AND NOT IS_ENTITY_DEAD(PS_Main.myVehicle)
				IF IS_ENTITY_TOUCHING_ENTITY(PS_FlatBedTrailer, PS_Main.myVehicle)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
					PRINTSTRING("VEHICLE LANDING - VEHICLE IS ON THE TRAILER") PRINTNL()
					RETURN TRUE
				ENDIF
			ENDIF*/
		ENDIF
	ENDIF

	RESET_LAND_TIME()
	RETURN FALSE
ENDFUNC

//Start the target landing truck driving
PROC PS_DLC_VEHICLE_LANDING_START_TARGET_TRUCK()
	IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(PS_FlatBedTruck)
		IF NOT IS_ENTITY_DEAD(PS_FlatBedTruck)
			//Double checking trailer is attached for #1755684
		//	SET_ENTITY_DYNAMIC(PS_FlatBedTrailer, TRUE) 
        //	ACTIVATE_PHYSICS(PS_FlatBedTrailer)
		//	SET_FOCUS_ENTITY(PS_FlatBedTrailer)
		//	ATTACH_VEHICLE_TO_TRAILER(PS_FlatBedTruck, PS_FlatBedTrailer)
		
		//	SET_VEHICLE_ON_GROUND_PROPERLY(PS_FlatBedTrailer)
		//	FREEZE_ENTITY_POSITION(PS_FlatBedTrailer, FALSE)
			SET_VEHICLE_ENGINE_ON(PS_FlatBedTruck, TRUE, TRUE)
			START_PLAYBACK_RECORDED_VEHICLE(PS_FlatBedTruck, 130, "PilotSchool")
			//START_RECORDING_VEHICLE(PS_FlatBedTrailer,999,"PilotSchool",true)
			SET_PLAYBACK_SPEED(PS_FlatBedTruck, 3.0)
			SET_ENTITY_MAX_SPEED(PS_FlatBedTruck, 40.0)
			SET_ENTITY_LOD_DIST(PS_FlatBedTruck, 2000)
			
			START_PLAYBACK_RECORDED_VEHICLE(PS_FlatBedTrailer, 140, "PilotSchool")
		ENDIF
	ENDIF
ENDPROC

//Cleanup the target truck
PROC PS_DLC_VEHICLE_LANDING_CLEANUP_TRUCK_TARGET()
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(PS_FlatBedTruck_NETID)
		PS_DELETE_NET_ID(PS_FlatBedTruck_NETID)
	ENDIF
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(PS_FlatBedTrailer_NETID)
		PS_DELETE_NET_ID(PS_FlatBedTrailer_NETID)
	ENDIF
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(targetDriver_NETID)
		PS_DELETE_NET_ID(targetDriver_NETID)
	ENDIF
	
ENDPROC

//Stop the target truck
PROC PS_DLC_VEHICLE_LANDING_STOP_TARGET_TRUCK(VECTOR& truckCoords)
	IF NOT IS_VEHICLE_FUCKED(PS_FlatBedTruck)
		targetDriver = GET_PED_IN_VEHICLE_SEAT(PS_FlatBedTruck)
		VECTOR vTrash, vPosition, vForward
		GET_ENTITY_MATRIX(PS_FlatBedTruck, vForward, vTrash, vTrash, vPosition)
		IF NOT IS_PED_INJURED(targetDriver)
			vForward = 10.0 * vForward
			
			TASK_VEHICLE_DRIVE_TO_COORD(targetDriver, PS_FlatBedTruck, vPosition + vForward, GET_ENTITY_SPEED(PS_FlatBedTruck), DRIVINGSTYLE_STRAIGHTLINE, GET_ENTITY_MODEL(PS_FlatBedTruck), DRIVINGMODE_AVOIDCARS, 2.0, 2.0)
		ENDIF
		
		if DOES_ENTITY_EXIST(PS_FlatBedTruck)
			if IS_PLAYBACK_GOING_ON_FOR_VEHICLE(PS_FlatBedTruck)
				STOP_PLAYBACK_RECORDED_VEHICLE(PS_FlatBedTruck)
			ENDIF
		ENDIF
		truckCoords = GET_ENTITY_COORDS(PS_FlatBedTruck)
	ENDIF
ENDPROC

/// PURPOSE:
///    Determines what fail conditions we need to check dependent on the 
/// PARAMS:
///    PS_Main - 
FUNC BOOL PS_DLC_Vehicle_Landing_Fail_Check()
	
	
	PRINTLN("VEHICLE LANDING - CHECKING FOR FAIL")
	SWITCH(PS_DLC_Vehicle_Landing_State)
		CASE PS_DLC_VEHICLE_LANDING_INIT
			RETURN FALSE
			
		CASE PS_DLC_VEHICLE_LANDING_COUNTDOWN
			RETURN FALSE
			
		CASE PS_DLC_VEHICLE_LANDING_TAKEOFF
//				IF PS_HUD_GET_HOURGLASS_TIMER_VALUE() <= 0
//					ePSFailReason = PS_FAIL_TIME_UP
//					RETURN TRUE
//				ENDIF
			//Do default checks
			IF PS_UPDATE_FAIL_CHECKS(PS_Main, FALSE, TRUE, TRUE, TRUE, TRUE, TRUE, FALSE)
				RETURN TRUE
			ENDIF
		BREAK
			
		CASE PS_DLC_VEHICLE_LANDING_OBSTACLE
			IF PS_HUD_GET_HOURGLASS_TIMER_VALUE() <= 0
				PRINTSTRING("VEHICLE LANDING - FAILING FOR TIME UP") PRINTNL()
				ePSFailReason = PS_FAIL_TIME_UP
				RETURN TRUE
			ENDIF
			//Do default checks
			IF PS_GET_CHECKPOINT_PROGRESS() = PS_GET_TOTAL_CHECKPOINTS()
				//make sure we don't fail the player for landing
				IF PS_UPDATE_FAIL_CHECKS(PS_Main, FALSE, FALSE, TRUE, TRUE, TRUE, TRUE, FALSE)
					RETURN TRUE
				ENDIF
				
				//Check for truck entering the tunnel
				IF IS_ENTITY_IN_ANGLED_AREA(PS_FlatBedTruck, <<-2612.8960, 3019.7200, 15.5983>>, <<-2592.2605, 3036.3044, 21.7571>>, 20) AND NOT
					PS_DLC_VEHICLE_LANDING_HAS_PLAYER_LANDED()
					PRINTLN("VEHICLE LANDING - FAILING FOR ENTERING TUNNEL")
					ePSFailReason = PS_FAIL_SPECIAL_REASON
					RETURN TRUE
				ELSE
					RETURN FALSE
				ENDIF
			ELSE
				IF PS_UPDATE_FAIL_CHECKS(PS_Main, FALSE, TRUE, TRUE, TRUE, TRUE, TRUE, FALSE)
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	//If we get here, we havent failed yet
	PRINTLN("DONE FAIL CHECK DIDN'T FAIL")
	RETURN FALSE
ENDFUNC

//Handle the landing blip on the truck
PROC HANDLE_LANDING_BLIP()
	IF DOES_BLIP_EXIST(bPS_DLCVehicleLandingTargetBlip)
		if DOES_ENTITY_EXIST(PS_FlatBedTruck)
			if NOT IS_VEHICLE_FUCKED(PS_FlatBedTruck)
				SET_BLIP_COORDS(bPS_DLCVehicleLandingTargetBlip, GET_ENTITY_COORDS(PS_FlatBedTruck))
			ENDIF
		ENDIF
	ELSE
		if DOES_ENTITY_EXIST(PS_FlatBedTruck)
			if NOT IS_VEHICLE_FUCKED(PS_FlatBedTruck)
				bPS_DLCVehicleLandingTargetBlip = CREATE_BLIP_FOR_COORD(GET_ENTITY_COORDS(PS_FlatBedTruck))
			ENDIF
		ENDIF
		SET_BLIP_SCALE(bPS_DLCVehicleLandingTargetBlip, BLIP_SIZE_NETWORK_CHECKPOINT)
	ENDIF	
ENDPROC

/// PURPOSE:
///    Checks for objective goals completed based on the stage/state we are in of the challenge progression
///    When it returns TRUE, the main loop will increment the challenge state/stage
/// PARAMS:
///    PS_Main - 
FUNC BOOL PS_DLC_Vehicle_Landing_Progress_Check()
	IF NOT IS_PED_INJURED(PLAYER_PED_ID()) AND IS_VEHICLE_DRIVEABLE(PS_Main.myVehicle)
		SWITCH(PS_DLC_Vehicle_Landing_State)
			CASE PS_DLC_VEHICLE_LANDING_INIT
				RETURN FALSE

			CASE PS_DLC_VEHICLE_LANDING_COUNTDOWN
				HANDLE_LANDING_BLIP()
				RETURN FALSE
				
			CASE PS_DLC_VEHICLE_LANDING_OBSTACLE
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_UPDATE
						//See if the player is close to the checkpoint yet
						/*IF PS_IS_LAST_CHECKPOINT_CLEARED()
							//We don't want the last checkpoint to be cleared without landing
							//RETURN TRUE
							PRINTSTRING("VEHICLE LANDING - LAST CHECKPOINT WAS MARKED AS CLEARED!") PRINTNL()
							//PS_DEREGISTER_CHECKPOINT(PS_GET_TOTAL_CHECKPOINTS())
						ELSE*/
							//Check if player has landed on the trailer and manually increment checkpoint and pass
							IF PS_GET_CHECKPOINT_PROGRESS() >= PS_GET_TOTAL_CHECKPOINTS() - 1	
								IF PS_GET_CHECKPOINT_PROGRESS() = PS_GET_TOTAL_CHECKPOINTS()
									//PS_HIDE_CHECKPOINT(PS_GET_CHECKPOINT_PROGRESS())
									HANDLE_LANDING_BLIP()
								ENDIF
							
								IF PS_DLC_VEHICLE_LANDING_HAS_PLAYER_LANDED()
									/*PRINTSTRING("Vehicle Landing - Vehicle has landed on trailer") PRINTNL()
									PRINTSTRING("Vehicle Landing - Total Checkpoints: ") PRINTINT(PS_GET_TOTAL_CHECKPOINTS()) PRINTNL()
									PRINTSTRING("Vehicle Landing - Checkpoint Progre: ") PRINTINT(PS_GET_CHECKPOINT_PROGRESS()) PRINTNL()*/
									
									bLandTimeCheck = TRUE
									
									PRINTSTRING("Vehicle Landing - Player Landed on the trailer") PRINTNL()
									IF CHECK_LAND_TIME()
										bCourseComplete = TRUE
										bVehLandDrawRing = FALSE
										
										SET_VEHICLE_ENGINE_ON(PS_Main.myVehicle, FALSE, TRUE)
										
										//Stop the timer
										PS_SET_TIMER_ACTIVE( FALSE )
										PS_STOP_HOURGLASS_TIMER()
										
										IF PS_GET_CHECKPOINT_PROGRESS() <= PS_GET_TOTAL_CHECKPOINTS()
											PS_INCREMENT_CHECKPOINT()
											PS_Main.vCheckpointMarkerFlashPosition = vMovingVeh	//update flash checkpoint position to match the yellow marker
										ENDIF
										RETURN TRUE
									ENDIF
								ENDIF
							ENDIF
						//ENDIF
					BREAK
				ENDSWITCH
			BREAK		
		ENDSWITCH
	ENDIF
	//if we reach here, we probably haven't passed the check
	RETURN FALSE	
ENDFUNC

PROC PS_DLC_VEHICLE_LANDING_FORCE_FAIL
	ePSFailReason = PS_FAIL_DEBUG
	PS_DLC_Vehicle_Landing_State = PS_DLC_VEHICLE_LANDING_FAIL
	PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
ENDPROC

PROC PS_DLC_VEHICLE_LANDING_FORCE_PASS
	PS_DLC_Vehicle_Landing_State = PS_DLC_VEHICLE_LANDING_SUCCESS
	PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
ENDPROC

//****************************************************************************************************
//****************************************************************************************************
//	Public functions! (used by Pilot_School.sc to run the challenges)
//****************************************************************************************************
//****************************************************************************************************

//Initialise procedure
PROC PS_DLC_VEHICLE_LANDING_INITialise()
	SWITCH g_PS_initStage
		CASE 0
			vStartPosition = <<-3242.5, 2014, 110>>//<<-3200.1758, 2035.1869, 110.2217>> //<<-3030.3662, 2119.2070, 97.2446>>
			vStartRotation = <<0, 0, -66.3441>>
			fStartHeading = 294.5971
			
			SET_VEHICLE_POPULATION_BUDGET(0)
			
			bCourseComplete = FALSE
			bLandTimeCheck = FALSE
			iLandTime = 0
			iChangeTime = 0
			
			VehicleToUse = SWIFT
			iPS_PreviewVehicleRecordingID = iPS_PEVIEW_VEH_REC_ID_DLC_VEHICLE_LANDING
			
			REQUEST_MODEL(FREIGHTTRAILER)
			REQUEST_MODEL(PHANTOM)
			REQUEST_MODEL(PilotModel)
			REQUEST_VEHICLE_RECORDING(iPS_PreviewVehicleRecordingID, sPreviewRecordingName)
			REQUEST_VEHICLE_RECORDING(130, "PilotSchool")
			REQUEST_VEHICLE_RECORDING(140, "PilotSchool")
			g_PS_initStage++
		BREAK
		CASE 1
			IF HAS_MODEL_LOADED(FREIGHTTRAILER)
			AND HAS_MODEL_LOADED(PHANTOM)
			AND HAS_MODEL_LOADED(PilotModel)
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(iPS_PreviewVehicleRecordingID, sPreviewRecordingName)
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(130, "PilotSchool")
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(140, "PilotSchool")
			AND PS_SETUP_CHALLENGE()
				g_PS_initStage++
			ENDIF
		BREAK
		CASE 2
			//Spawn player vehicle				
			IF PS_CREATE_VEHICLE()
				bwaitingOnSomething = FALSE
	
				SET_VEHICLE_LIVERY(PS_Main.myVehicle, 1)
	
				//Offset used to check if player is in the air or on the ground
				PS_SET_GROUND_OFFSET(1.5)
				
				iInstructionCounter = 1
				bFinishedChallenge = FALSE
				
				
				PS_DLC_Vehicle_Landing_Init_Checkpoints()

				PS_Special_Fail_Reason = "PS_FAIL_DLC_TUNNEL"
				
				fPreviewVehicleSkipTime = 100
				fPreviewTime = 15
				
				vPilotSchoolSceneCoords = vStartPosition
				IF HAS_VEHICLE_RECORDING_BEEN_LOADED(iPS_PreviewVehicleRecordingID, sPreviewRecordingName)
					vPilotSchoolPreviewCoords = GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(iPS_PreviewVehicleRecordingID, fPreviewVehicleSkipTime, sPreviewRecordingName)
					vPilotSchoolPreviewRot = GET_ROTATION_OF_VEHICLE_RECORDING_AT_TIME(iPS_PreviewVehicleRecordingID, fPreviewVehicleSkipTime, sPreviewRecordingName)
				ENDIF
				
				//VECTOR vGenBlockMin, VGenBlockMax
				//vGenBlockMin = <<-3093.2959, 1989.0200, -0.2557>>
				//VGenBlockMax = <<-1974.1921, 3109.9678, 106.2217>>
				//SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(vGenBlockMin, VGenBlockMax, FALSE)
				BLOCK_SCENARIOS_AND_AMBIENT(sb_PS, DEFAULT, TRUE)
				
				//SET_ROADS_IN_AREA(vGenBlockMin, VGenBlockMax, FALSE)
				
				scenBlockHelipad = ADD_SCENARIO_BLOCKING_AREA(<<-3093.2959, 1989.0200, -0.2557>>, <<-1974.1921, 3109.9678, 106.2217>>)
				CLEAR_AREA(<<-2684.4749, 2409.2341, 15.6944>>, 2000.0, TRUE, TRUE)
				
			//	NEW_LOAD_SCENE_START(<<-2857.9773, 2171.1316, 33.4674>>,CONVERT_ROTATION_TO_DIRECTION_VECTOR(vStartRotation),200)
				
//				SET_WEATHER_FOR_FMMC_MISSION(1) //sunny

				NETWORK_OVERRIDE_CLOCK_TIME(GET_HOUR_FOR_PS_CHALLENGE(),0,0)
				
				SET_VEHICLE_RADIO_ENABLED(PS_Main.myVehicle, FALSE)
				
				#IF IS_DEBUG_BUILD
					DEBUG_MESSAGE("******************Setting up PS_DLC_Vehicle_Landing.sc******************")
				#ENDIF
				g_PS_initStage=0
			ENDIF
		BREAK
	ENDSWITCH
	
	
ENDPROC

//Main setup procedure
FUNC BOOL PS_DLC_Vehicle_Landing_MainSetup()

	SET_VEHICLE_POPULATION_BUDGET(0)

	PS_DLC_VEHICLE_LANDING_INIT_Checkpoints()
	IF NOT IS_ENTITY_DEAD(PS_Main.myVehicle)
		SET_ENTITY_COORDS(PS_Main.myVehicle, vStartPosition)
		SET_ENTITY_ROTATION(PS_Main.myVehicle, vStartRotation)
		FREEZE_ENTITY_POSITION(PS_Main.myVehicle, TRUE)
	ENDIF
	SET_MINIMAP_COMPONENT(MINIMAP_COMPONENT_RUNWAY_1, FALSE)
	NETWORK_OVERRIDE_CLOCK_TIME(GET_HOUR_FOR_PS_CHALLENGE(),0,0)
	
	NEW_LOAD_SCENE_START_SPHERE(vStartPosition,1200)
	RETURN TRUE
ENDFUNC

//Function for preview playing
FUNC BOOL PS_DLC_Vehicle_Landing_Preview()
		PS_PREVIEW_UPDATE_CUTSCENE()
		SET_DISTANT_CARS_ENABLED(FALSE)
		
		//Have no vehicles on the road
		SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0)
		
		SWITCH(PS_PreviewState)
			CASE PS_PREVIEW_SETUP
			
				IF NOT NETWORK_DOES_NETWORK_ID_EXIST(PS_FlatBedTruck_NETID)
					IF NOT PS_CREATE_NET_VEHICLE(PS_FlatBedTruck_NETID,PHANTOM, <<-2857.9773, 2171.1316, 33.4674>>, 299.2812)						
						RETURN FALSE
					ELSE
						PS_FlatBedTruck = NET_TO_VEH(PS_FlatBedTruck_NETID)
					ENDIF
				ENDIF
				
				IF NOT NETWORK_DOES_NETWORK_ID_EXIST(PS_FlatBedTrailer_NETID)
					IF IS_VEHICLE_DRIVEABLE(PS_FlatBedTruck)
						IF NOT PS_CREATE_NET_VEHICLE(PS_FlatBedTrailer_NETID,FREIGHTTRAILER, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PS_FlatBedTruck, <<0.0, -10.0, 0.0>>), 299.2812)								
							RETURN FALSE
						ELSE
							PS_FlatBedTrailer = NET_TO_VEH(PS_FlatBedTrailer_NETID)
						ENDIF
					ELSE
						RETURN FALSE
					ENDIF
				ENDIF
				
				
				
			
				iPSDialogueCounter = 1
				CLEAR_AREA(<<-2684.4749, 2409.2341, 15.6944>>, 2000.0, TRUE, TRUE)
				PS_PREVIEW_SETUP_PLAYER_FOR_CUTSCENE()
				
			
//				SET_WEATHER_FOR_FMMC_MISSION(1)
				
				IF NOT IS_VEHICLE_FUCKED(PS_Main.myVehicle)
					//Helicopter
					
					START_PLAYBACK_RECORDED_VEHICLE(PS_Main.myVehicle, iPS_PreviewVehicleRecordingID, "PilotSchool")
					SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(PS_Main.myVehicle, fPreviewVehicleSkipTime)
					
					//Create truck and trailer for preview
					//PS_FlatBedTruck = CREATE_VEHICLE(PHANTOM, <<-2857.9773, 2171.1316, 33.4674>>, 299.2812)
					
					START_PLAYBACK_RECORDED_VEHICLE(PS_FlatBedTruck, 130, "PilotSchool")
					SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(PS_FlatBedTruck, 8000 * 3)
					FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(PS_FlatBedTruck)
					
					SET_ENTITY_DYNAMIC(PS_FlatBedTrailer, TRUE) 
        			ACTIVATE_PHYSICS(PS_FlatBedTrailer)
					//SET_FOCUS_ENTITY(PS_FlatBedTrailer)

					
					//PS_FlatBedTrailer = CREATE_VEHICLE(FREIGHTTRAILER, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PS_FlatBedTruck, <<0.0, -10.0, 0.0>>), 299.2812)
					IF NOT IS_VEHICLE_ATTACHED_TO_TRAILER(PS_FlatBedTruck)
						ATTACH_VEHICLE_TO_TRAILER(PS_FlatBedTruck, PS_FlatBedTrailer)
					ENDIF
					SET_PLAYBACK_SPEED(PS_FlatBedTruck, 3.0)
				ENDIF
				bVehLandDrawRing = TRUE
				
				//Play preview dialogue
				PS_PREVIEW_DIALOGUE_PLAY("PS_MLPREV", "PS_MLPREV_1", "PS_MLPREV_2", "PS_MLPREV_3")
				
		
				iPreviewCheckpointIdx = PS_GET_CLOSEST_PREVIEW_CHECKPOINT(iPS_PreviewVehicleRecordingID, PS_Main.myCheckpointz, fPreviewVehicleSkipTime)
				PS_ACTIVATE_CHECKPOINT_MGR(PS_LESSON_OBSTACLE_HELI, iPreviewCheckpointIdx + 1, TRUE)
			BREAK
			
			CASE PS_PREVIEW_PLAYING
				//Update the truck location and draw marker ring
				UPDATE_LANDING_LOCATION()
				PS_VEH_LAND_DRAW_MARKER_RING()
			BREAK
			
			CASE PS_PREVIEW_CLEANUP
				bVehLandDrawRing = FALSE
				PS_PREVIEW_CLEANUP_CUTSCENE()
				if DOES_ENTITY_EXIST(PS_FlatBedTruck)
					if IS_PLAYBACK_GOING_ON_FOR_VEHICLE(PS_FlatBedTruck)
						STOP_PLAYBACK_RECORDED_VEHICLE(PS_FlatBedTruck)
					ENDIF
				ENDIF
				
				IF NETWORK_DOES_NETWORK_ID_EXIST(PS_FlatBedTruck_NETID)
					PS_DELETE_NET_ID(PS_FlatBedTruck_NETID)
				ENDIF
				
				IF NETWORK_DOES_NETWORK_ID_EXIST(PS_FlatBedTrailer_NETID)
					PS_DELETE_NET_ID(PS_FlatBedTrailer_NETID)
				ENDIF
				
				IF NETWORK_DOES_NETWORK_ID_EXIST(targetDriver_NETID)
					PS_DELETE_NET_ID(targetDriver_NETID)
				ENDIF
			BREAK
		ENDSWITCH
	RETURN TRUE	
ENDFUNC

FUNC BOOL PS_DLC_Vehicle_Landing_MainUpdate()

	//Have no vehicles on the road
	CLEAR_AREA_OF_VEHICLES(<<-2684.4749, 2409.2341, 15.6944>>, 2000.0)
	SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0)
	
	SET_DISTANT_CARS_ENABLED(FALSE)
	
	IF TempDebugInput() AND NOT bFinishedChallenge
	
		PS_LESSON_UDPATE()
	
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())			
			PRINTLN("VEHICLE LANDING - MAIN UPDATE LOOP")
			PS_VEH_LAND_DRAW_MARKER_RING()
		ENDIF
		
		SWITCH(PS_DLC_Vehicle_Landing_State)
			
			//Spawn player and helicopter on the runway
			CASE PS_DLC_VEHICLE_LANDING_INIT
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						//Setup vars and vehicle
						iPSDialogueCounter = 1
						
						IF NOT IS_ENTITY_DEAD(PS_Main.myVehicle)
							SET_VEHICLE_ENGINE_ON(PS_Main.myVehicle, TRUE, TRUE)
							SET_HELI_BLADES_FULL_SPEED(PS_Main.myVehicle)
						ENDIF
						
						IF PS_DLC_VEHICLE_LANDING_CREATE_TARGET_TRUCK()
							SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
							PS_DLC_VEHICLE_LANDING_START_TARGET_TRUCK()
							PS_ACTIVATE_CHECKPOINT_MGR(PS_LESSON_OBSTACLE_HELI)
							PS_HIDE_CHECKPOINT(PS_GET_CHECKPOINT_PROGRESS())
							PS_INCREMENT_SUBSTATE()
						ENDIF
					BREAK
					CASE PS_SUBSTATE_UPDATE
						PS_INCREMENT_SUBSTATE()
					BREAK
					CASE PS_SUBSTATE_EXIT
						IF NOT IS_ENTITY_DEAD(PS_Main.myVehicle)
							SET_ENTITY_COORDS(PS_Main.myVehicle, vStartPosition)
							SET_ENTITY_ROTATION(PS_Main.myVehicle, vStartRotation)
						ENDIF
						IF NOT IS_ENTITY_DEAD(GET_PED_IN_VEHICLE_SEAT(PS_Main.myVehicle, VS_DRIVER))
							TASK_VEHICLE_MISSION_COORS_TARGET(GET_PED_IN_VEHICLE_SEAT(PS_Main.myVehicle, VS_DRIVER), PS_Main.myVehicle, <<-3030.3662, 2119.2070, 97.2446>>, MISSION_GOTO, 40.0, DRIVINGMODE_PLOUGHTHROUGH, 0, 100)
						ENDIF
						PS_DLC_Vehicle_Landing_State = PS_DLC_VEHICLE_LANDING_COUNTDOWN
						PS_INCREMENT_SUBSTATE()
					BREAK
				ENDSWITCH
			BREAK
			
			//Play countdown
			CASE PS_DLC_VEHICLE_LANDING_COUNTDOWN
				UPDATE_LANDING_LOCATION()
				bVehLandDrawRing = TRUE
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_INCREMENT_SUBSTATE()
					BREAK
					CASE PS_SUBSTATE_UPDATE
						PS_DLC_Vehicle_Landing_Progress_Check()
						//Update countdown
						IF	PS_HUD_UPDATE_COUNTDOWN(TRUE, FALSE)
							PS_INCREMENT_SUBSTATE()
						ENDIF
					BREAK
					CASE PS_SUBSTATE_EXIT
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							CLEAR_PED_TASKS(PLAYER_PED_ID())
						ENDIF
						PS_HIDE_CHECKPOINT(PS_GET_TOTAL_CHECKPOINTS())
						PS_DLC_Vehicle_Landing_State = PS_DLC_VEHICLE_LANDING_TAKEOFF
						PS_INCREMENT_SUBSTATE()
						PS_TRIGGER_MUSIC(PS_MUSIC_MOVING_START)
					BREAK
				ENDSWITCH
			BREAK
				
			//Take off and initial instructions
			CASE PS_DLC_VEHICLE_LANDING_TAKEOFF
				UPDATE_LANDING_LOCATION()
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						//Setup objective, give the player controls and manage any new help text
						//SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						DO_NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)	
						//SET_PLAYER_INVINCIBLE(PLAYER_ID(),FALSE)
						//SET_PLAYER_INVINCIBLE(PLAYER_ID(), TRUE)
						PS_HIDE_CHECKPOINT(PS_GET_CHECKPOINT_PROGRESS())
						PS_HUD_SET_GUTTER_ICON_ACTIVE(TRUE)
						PS_SET_HINT_CAM_ACTIVE(TRUE)
						PS_SET_HINT_CAM_COORD(PS_GET_CURRENT_CHECKPOINT())
						PS_INCREMENT_SUBSTATE()
					BREAK
					CASE PS_SUBSTATE_UPDATE
						IF PS_DLC_Vehicle_Landing_Fail_Check()
							PS_DLC_Vehicle_Landing_State = PS_DLC_VEHICLE_LANDING_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)				
						ELSE
							PS_INCREMENT_SUBSTATE()
						ENDIF
					BREAK
					CASE PS_SUBSTATE_EXIT
						PS_SET_CHECKPOINT_COUNTER_ACTIVE(TRUE)
						PS_HUD_RESET_TIMER() //keep this for our hourglass
						PS_START_HOURGLASS_TIMER(FLOOR(PS_Challenges[PSCD_DLC_VehicleLanding].BronzeTime * 1000))
						PS_SET_TIMER_ACTIVE(TRUE)
						PS_DLC_Vehicle_Landing_State = PS_DLC_VEHICLE_LANDING_OBSTACLE
						PS_INCREMENT_SUBSTATE()
					BREAK
				ENDSWITCH
			BREAK
		
			//Going through the obstacle course
			CASE PS_DLC_VEHICLE_LANDING_OBSTACLE
				UPDATE_LANDING_LOCATION()
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_KILL_HINT_CAM()
						PS_HIDE_CHECKPOINT(PS_GET_CHECKPOINT_PROGRESS())
 				    	PS_INCREMENT_SUBSTATE()
					BREAK
						
					CASE PS_SUBSTATE_UPDATE
						//PRINTSTRING("CURRENT CHECKPOINT: ") PRINTINT(PS_GET_CHECKPOINT_PROGRESS()) PRINTNL()
						
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND NOT IS_MESSAGE_BEING_DISPLAYED() AND iPSDialogueCounter = 1
							PRINT_HELP("PS_ML_HELP_FOCUS")
							PS_PLAY_DISPATCHER_INSTRUCTION_3_LINES("PS_ML", "PS_ML_1", "PS_ML", "PS_ML_2", "PS_ML", "PS_ML_3")
							iPSDialogueCounter++
						ENDIF
						
						//Comment for touching down
						IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), PS_FlatBedTrailer) < 50 AND iPSDialogueCounter = 2 AND NOT IS_MESSAGE_BEING_DISPLAYED()
							PS_PLAY_DISPATCHER_INSTRUCTION("PS_ML", "PS_ML_4")
							iPSDialogueCounter++
						ENDIF
						
						//Comment for nearly at the tunnel
						IF PS_HUD_GET_HOURGLASS_TIMER_VALUE() <= 20000
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND NOT IS_MESSAGE_BEING_DISPLAYED() AND iPSDialogueCounter = 3
								PS_PLAY_DISPATCHER_INSTRUCTION("PS_MLTun", "PS_MLTun_1")
								iPSDialogueCounter++
							ENDIF
						ENDIF
						
						
						//Check for player landing and activate focus where appropriate
						IF PS_DLC_VEHICLE_LANDING_HAS_PLAYER_LANDED()
							PRINTLN("HINT CAM SHOULD BE DEACTIVATED")
							PS_CLEAR_HINT_CAM_TARGET()
							PS_SET_HINT_CAM_ACTIVE(FALSE, PS_HINT_FROM_SKYDIVING)
							PS_UPDATE_HINT_CAM()
						ELSE
							IF NOT PS_HINT_CAM.bActive
								PS_SET_HINT_CAM_ACTIVE(TRUE, PS_HINT_FROM_SKYDIVING)
								PS_SET_HINT_CAM_ENTITY(PS_FlatBedTrailer)
							ENDIF
						ENDIF

						IF PS_DLC_Vehicle_Landing_Fail_Check()
							PS_DLC_Vehicle_Landing_State = PS_DLC_VEHICLE_LANDING_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELIF PS_DLC_Vehicle_Landing_Progress_Check()
							PRINTSTRING("VEHICLE LANDING - COURSE COMPLETE") PRINTNL()
							PS_INCREMENT_SUBSTATE()
						ELSE
							PRINTSTRING("VEHICLE LANDING - GOING THROUGH COURSE") PRINTNL()
						ENDIF
					BREAK
					
					CASE PS_SUBSTATE_EXIT
						PRINTSTRING("VEHICLE LANDING - CHECKING IF MISSION IS SUCCESS") PRINTNL()
						IF NOT bCourseComplete
							PRINTSTRING("VEHICLE LANDING - MISSION MARKED AS FAIL") PRINTNL()
							PS_DLC_Vehicle_Landing_State = PS_DLC_VEHICLE_LANDING_FAIL
						ELSE
							PRINTSTRING("VEHICLE LANDING - MISSION MARKED AS PASS") PRINTNL()
							PS_DLC_Vehicle_Landing_State = PS_DLC_VEHICLE_LANDING_SUCCESS
						ENDIF
						PS_INCREMENT_SUBSTATE()
					BREAK
					
				ENDSWITCH
			BREAK
								
			//On pass
			CASE PS_DLC_VEHICLE_LANDING_SUCCESS
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
				UPDATE_LANDING_LOCATION()
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						DO_NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, FALSE, FALSE, TRUE)
						PS_TRIGGER_MUSIC(PS_MUSIC_MOVING_PASS)
						PS_UPDATE_RECORDS(PS_GET_LAST_CHECKPOINT())
						PS_PLAY_LESSON_FINISHED_LINE("PS_MLPass", "PS_MLPass_1", "PS_MLPass_2", "PS_MLPass_3", "")
						PS_LESSON_END(TRUE)
						PS_INCREMENT_SUBSTATE()
					BREAK
					
					CASE PS_SUBSTATE_UPDATE
						IF NOT IS_PLAYER_DEAD(player_id())							
						OR GET_MANUAL_RESPAWN_STATE()= MRS_READY_FOR_PLACEMENT
							IF NOT PS_HUD_IS_SCORECARD_ACTIVE()
								PS_INCREMENT_SUBSTATE()
							ENDIF
						ELSE
							PRINTLN("PLAYER IS DEAD SETTING RESPAWN STATE")
							//Set the player to respawn state if dead and doesn't have respawn state set
							IF IS_PLAYER_DEAD(player_id())
							AND GET_MANUAL_RESPAWN_STATE() = MRS_NULL
								SET_MANUAL_RESPAWN_STATE(MRS_RESPAWN_PLAYER_HIDDEN)
							ENDIF
						ENDIF						
					BREAK
					
					CASE PS_SUBSTATE_EXIT
						bFinishedChallenge = TRUE
						PS_INCREMENT_SUBSTATE()
					BREAK
					
				ENDSWITCH
			BREAK
				
			//On fail
			CASE PS_DLC_VEHICLE_LANDING_FAIL
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
				UPDATE_LANDING_LOCATION()
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						DO_NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, FALSE, FALSE, TRUE)
						PS_TRIGGER_MUSIC(PS_MUSIC_MOVING_FAIL)
						PRINTLN("ENTERED VEHICLE LANDING FAIL REASON : ", ePSFailReason)
						bVehLandDrawRing = FALSE
						PS_UPDATE_RECORDS(PS_GET_LAST_CHECKPOINT())
						PS_LESSON_END(FALSE)
						//PS_PLAY_LESSON_FINISHED_LINE("PS_ML", "PS_ML_7", "PS_ML_8", "PS_ML_9", "PS_ML_6")					
						PS_INCREMENT_SUBSTATE()
					BREAK
					
					CASE PS_SUBSTATE_UPDATE
						PRINTLN("UPDATING VEHICLE LANDING FAIL!!")
						IF NOT PS_HUD_IS_SCORECARD_ACTIVE()
							PS_INCREMENT_SUBSTATE()
						ENDIF
					BREAK
					
					CASE PS_SUBSTATE_EXIT
						PRINTLN("EXITING VEHICLE LANDING FAIL!!")
						bFinishedChallenge = TRUE
						PS_INCREMENT_SUBSTATE()
					BREAK
					
				ENDSWITCH
			BREAK	
		ENDSWITCH
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

//Main Cleanup procedure
PROC PS_DLC_Vehicle_Landing_MainCleanup()
	CLEAR_FOCUS()
	PS_SEND_PLAYER_BACK_TO_MENU()
	PS_HUD_SET_GUTTER_ICON_ACTIVE(FALSE)
	PS_HUD_SET_OBJECTIVE_METER_VISIBLE(FALSE)
	PS_SET_TIMER_ACTIVE(FALSE)
	PS_HUD_CLEANUP()
	REMOVE_VEHICLE_RECORDING(iPS_PreviewVehicleRecordingID, "PilotSchool")
	SET_MODEL_AS_NO_LONGER_NEEDED(VehicleToUse)	
	CLEAR_PRINTS()
	CLEAR_HELP()
	
	SET_VEHICLE_POPULATION_BUDGET(3)
	
	PS_DLC_VEHICLE_LANDING_STOP_TARGET_TRUCK(vPS_MovingTarget)
	PS_DLC_VEHICLE_LANDING_CLEANUP_TRUCK_TARGET()
	
	IF (scenBlockHelipad != NULL)
		REMOVE_SCENARIO_BLOCKING_AREA(scenBlockHelipad)
		scenBlockHelipad = NULL
	ENDIF
	
	SET_ROADS_BACK_TO_ORIGINAL(<<-3093.2959, 1989.0200, -0.2557>>, <<-1974.1921, 3109.9678, 106.2217>>)
	
	bLandTimeChange = FALSE
	bLandTimeCheck = FALSE
	bCourseComplete = FALSE

	//Re-enable distance cars
	SET_DISTANT_CARS_ENABLED(TRUE)

	REMOVE_BLIP(bPS_DLCVehicleLandingTargetBlip)
	
	//Reset vehicle generators
	//SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-3093.2959, 1989.0200, -0.2557>>, <<-1974.1921, 3109.9678, 106.2217>>, TRUE)
	UNBLOCK_SCENARIOS_AND_AMBIENT(sb_PS)
	
	//cleanup checkpoints
	PS_RESET_CHECKPOINT_MGR()
	#IF IS_DEBUG_BUILD
		DEBUG_MESSAGE("******************Cleaning up PS_DLC_Vehicle_Landing.sc******************")
	#ENDIF
ENDPROC

// EOF
//****************************************************************************************************
//****************************************************************************************************
