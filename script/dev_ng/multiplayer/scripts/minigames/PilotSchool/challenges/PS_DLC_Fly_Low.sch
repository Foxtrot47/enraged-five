//****************************************************************************************************
//
//	Author: 		Asa Dang
//	Date: 			7/13/11
//	Description:	"Plane Obstacle Course" Challenge for Pilot School. The player has to go through a series
//					of checkpoints placed precariously through the area around the airport. The player needs
//					avoid obstacles while also performing stunts through the coronas
//****************************************************************************************************

USING "minigames_helpers.sch"
USING "Pilot_School_MP_Challenge_Helpers.sch"
USING "commands_vehicle.sch"

//****************************************************************************************************
//****************************************************************************************************
//	Private functions that are used only by Plane Course
//****************************************************************************************************
//****************************************************************************************************

BOOL	bAutoPilotStarted
INT		iAutoPilotCameraProgress
int 	iFlylowDialogueCheckpoint = 2
#IF IS_DEBUG_BUILD
BOOL	PS_DLC_Fly_Low_CameraButtons[PS_MAX_CHECKPOINTS]
VECTOR 	PS_DLC_Fly_Low_CheckpointPositions[PS_MAX_CHECKPOINTS]
#ENDIF

#IF IS_DEBUG_BUILD
	//debug widgets variables
	BOOL		bPS_DLC_Fly_Low_PrintCheckpoints
	BOOL 		bPS_DLC_Fly_Low_UpdateCheckpoints
	CONST_INT 	PS_DLC_FLY_LOW_MAX_CHECKPOINTS	40
#ENDIF

#IF IS_DEBUG_BUILD
PROC PS_DLC_FLY_LOW_CREATE_DEBUG_WIDGETS()

	IF DOES_WIDGET_GROUP_EXIST(wDataFile)
		SET_CURRENT_WIDGET_GROUP(wDataFile)
			START_WIDGET_GROUP("Checkpoints")
			
				INT i
				
				ADD_WIDGET_BOOL("Print Checkpoints", bPS_DLC_Fly_Low_PrintCheckpoints)
				ADD_WIDGET_BOOL("Update Checkpoints", bPS_DLC_Fly_Low_UpdateCheckpoints)
			
				REPEAT PS_DLC_FLY_LOW_MAX_CHECKPOINTS i
					START_WIDGET_GROUP(GET_STRING_FROM_INT(i))
						PS_DLC_Fly_Low_CheckpointPositions[i] = PS_Main.myCheckpointz[i].position
						ADD_WIDGET_BOOL("Get Debug Cam Position", PS_DLC_Fly_Low_CameraButtons[i])
						ADD_WIDGET_VECTOR_SLIDER("Position", PS_DLC_Fly_Low_CheckpointPositions[i], -5000.0, 5000.0, 1.0)
					STOP_WIDGET_GROUP()
				ENDREPEAT
			
			STOP_WIDGET_GROUP()	
		CLEAR_CURRENT_WIDGET_GROUP(wDataFile)
	ENDIF
	
ENDPROC

PROC PS_DLC_FLY_LOW_UPDATE_DEBUG_WIDGETS()

	INT i
	
	IF PS_IS_CHECKPOINT_MGR_ACTIVE()
		REPEAT PS_GET_TOTAL_CHECKPOINTS() i
			IF PS_IS_CHECKPOINT_VALID(i)
				DRAW_DEBUG_TEXT_ABOVE_COORDS(PS_Main.myCheckpointz[i].position, GET_STRING_FROM_INT(i), 0.0)
			ENDIF
			IF PS_DLC_Fly_Low_CameraButtons[i] = TRUE
				PS_DLC_Fly_Low_CheckpointPositions[i] = GET_FINAL_RENDERED_CAM_COORD()
				PS_DLC_Fly_Low_CameraButtons[i] = FALSE
			ENDIF
		ENDREPEAT
	ENDIF

	IF ( bPS_DLC_Fly_Low_UpdateCheckpoints = TRUE )
	
		PS_RESET_CHECKPOINT_MGR()
	
		REPEAT PS_DLC_FLY_LOW_MAX_CHECKPOINTS i
			IF i != 0
				PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, PS_DLC_Fly_Low_CheckpointPositions[i])
			ENDIF
		ENDREPEAT
	
		PS_ACTIVATE_CHECKPOINT_MGR(PS_LESSON_FLAG_COLLECTION)
		PS_SHOW_ALL_CHECKPOINTS()
		
		bPS_DLC_Fly_Low_UpdateCheckpoints = FALSE
		
	ENDIF
	
	IF ( bPS_DLC_Fly_Low_PrintCheckpoints = TRUE )
		
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("PILOT SCHOOL DLC FLY LOW CHECKPOINTS")
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_NEWLINE_TO_DEBUG_FILE()
		
		REPEAT PS_DLC_FLY_LOW_MAX_CHECKPOINTS i
			IF i != 0
				SAVE_STRING_TO_DEBUG_FILE("PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, ")
				SAVE_VECTOR_TO_DEBUG_FILE(PS_DLC_Fly_Low_CheckpointPositions[i])
				SAVE_STRING_TO_DEBUG_FILE(")//")
				SAVE_INT_TO_DEBUG_FILE(i)
				SAVE_NEWLINE_TO_DEBUG_FILE()
			ENDIF
		ENDREPEAT
	
		bPS_DLC_Fly_Low_PrintCheckpoints = FALSE
	
	ENDIF

ENDPROC
#ENDIF

PROC PS_DLC_Fly_Low_Init_Checkpoints()  
	PS_RESET_CHECKPOINT_MGR()
	
	//new revised route with easier checkpoints
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<625.0000, -2052.0000, 16.0000>>)//1
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<673.5000, -1738.2500, 17.2500>>)//2
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<640.0000, -1440.0000, 16.5000>>)//3
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<591.5000, -1062.000, 18.6575>>)//4
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<624.0577, -578.0520, 22.0000>>)//5
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<911.0000, -402.0000, 57.0000>>)//6
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<1053.1263, -278.3180, 77.0000>>)//7
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<1240.0000, -81.0000, 67.0000>>)//8
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<1464.0000, -66.0000, 110.0000>>)//9
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<1664.0000, -10.0000, 183.5000>>)//10
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<1929.5400, 180.7419, 170.0000>>)//11
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<1969.0350, 442.2290, 170.5000>>)//12
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<1891.3009, 604.1917, 209.8104>>)//13
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<1778.0000, 670.3458, 282.5000>>)//14
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<1591.0845, 774.5155, 140.0000>>)//15
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<1548.6243, 931.2653, 100.3189>>)//16
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<1632.7560, 1179.3292, 100.0000>>)//17
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<1808.0000, 1407.7352, 128.5000>>)//18
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<2128.0000, 1678.0000, 105.0000>>)//19
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<2215.0000, 1839.7729, 116.0000>>)//20
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<2162.2473, 2132.1196, 132.5000>>)//21
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<2141.5901, 2346.2683, 116.5000>>)//22
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FINISH, <<2153.0000, 2515.5000, 104.0000>>)//23
	
ENDPROC

PROC PS_DLC_Fly_Low_Update_Dialogue()
	IF PS_IS_CHECKPOINT_MGR_ACTIVE() AND PS_IS_WAITING_FOR_CHECKPOINT_FEEDBACK() AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		SWITCH(PS_GET_CHECKPOINT_PROGRESS())
			CASE 0
				BREAK
			CASE 1
				BREAK
			CASE 3
				//PS_PLAY_DISPATCHER_INSTRUCTION("PS_POC", "PS_POC_3")//"Get ready for the next gate. Since it's green you'll have to perform a knife through it."
				PS_STOP_WAITING_FOR_CHECKPOINT_FEEDBACK()
				BREAK
				
			CASE 5		
				//PS_PLAY_DISPATCHER_INSTRUCTION("PS_POC", "PS_POC_4")//"This next gate is blue, so you're going to have to fly upside down through it."
				PS_STOP_WAITING_FOR_CHECKPOINT_FEEDBACK()
				BREAK
			DEFAULT
				IF PS_GET_PREV_CHECKPOINT_TYPE() != PS_CHECKPOINT_OBSTACLE_PLANE AND PS_GET_PREV_CHECKPOINT_TYPE() != PS_CHECKPOINT_FINISH
					PS_PLAY_RANDOM_FEEDBACK_DIALOGUE(PS_GET_LAST_CHECKPOINT_SCORE())
					PS_STOP_WAITING_FOR_CHECKPOINT_FEEDBACK()
				ENDIF
				BREAK
		ENDSWITCH
	ENDIF
ENDPROC

PROC  flylow_dialogue_update()
	if iFlylowDialogueCheckpoint = PS_GET_CHECKPOINT_PROGRESS()
		iFlylowDialogueCheckpoint = PS_GET_CHECKPOINT_PROGRESS() + 2
	
		IF PS_Main.myCheckpointMgr.heightFromCurCheckpoint < 3
			PS_PLAY_DISPATCHER_INSTRUCTION_CONVERSATION("PS_LOWLOW")
		ELSE
			PS_PLAY_DISPATCHER_INSTRUCTION_CONVERSATION("PS_LOWHIGH")
		ENDIF
	endif
ENDPROC

PROC PS_FLY_LOW_UPDATE_AUTO_PILOT_CAMERA()

	IF IS_VEHICLE_DRIVEABLE(PS_Main.myVehicle)
		SWITCH iAutoPilotCameraProgress
			CASE 0
			
				DESTROY_ALL_CAMS()
				RENDER_SCRIPT_CAMS(FALSE, FALSE)
				
				PS_Main.previewCam1 = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
				
				SET_CAM_COORD(PS_Main.previewCam1, << 673.4, -2623.0, 23.0 >>)
				SET_CAM_FOV(PS_Main.previewCam1, 30.0)
				POINT_CAM_AT_ENTITY(PS_Main.previewCam1, PS_Main.myVehicle, << 0.0, 2.0, 0.0 >>)
				SHAKE_CAM(PS_Main.previewCam1, "HAND_SHAKE", 1.0)
				
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				iAutoPilotCameraProgress++
			BREAK
			CASE 1
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(PS_Main.myVehicle)
					//IF GET_TIME_POSITION_IN_RECORDING(PS_Main.myVehicle) >= 11000.0
					IF IS_BITMASK_AS_ENUM_SET(PS_CountDownUI.iBitFlags, CNTDWN_UI_Played_3)
						DESTROY_ALL_CAMS()
						RENDER_SCRIPT_CAMS(FALSE, FALSE)
						SET_GAMEPLAY_CAM_RELATIVE_PITCH()
						SET_GAMEPLAY_CAM_RELATIVE_HEADING()
						iAutoPilotCameraProgress++
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF	

ENDPROC

PROC PS_FLY_LOW_AUTO_PILOT(PS_MAIN_STRUCT &data, FLOAT fStartTime, FLOAT fEndTime, FLOAT fPlaybackSpeed)

	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MOVE_LR)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MOVE_UD)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_YAW_LEFT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_YAW_RIGHT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_THROTTLE_UP)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_THROTTLE_DOWN)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
	//hopefully one of these will work
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_BEHIND)
	DISABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_LOOK_BEHIND)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_LOOK_BEHIND)
	DISABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_VEH_LOOK_BEHIND)
	//
	DISABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_LOOK_LR)
	DISABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_LOOK_UD)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)

	IF NOT IS_ENTITY_DEAD(data.myVehicle)
	
		IF bAutoPilotStarted = FALSE 
		
			IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(data.myVehicle)
			
				START_PLAYBACK_RECORDED_VEHICLE(data.myVehicle, iPS_PreviewVehicleRecordingID, "PilotSchool")
				SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(data.myVehicle, fStartTime)
				FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(data.myVehicle)
				SET_PLAYBACK_SPEED(data.myVehicle, fPlaybackSpeed)
					
				CONTROL_LANDING_GEAR(PS_Main.myVehicle, LGC_RETRACT_INSTANT)
				MODIFY_VEHICLE_TOP_SPEED(PS_Main.myVehicle, -25)
								
				bAutoPilotStarted = TRUE
				
			ENDIF
			
		ELSE
		
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(data.myVehicle)
			
				IF GET_TIME_POSITION_IN_RECORDING(data.myVehicle) >= fEndTime //OR PS_IS_COUNTDOWN_SHOWING_GO()
					//STOP_PLAYBACK_RECORDED_VEHICLE(data.myVehicle)
				ENDIF
				
			ENDIF
			
		ENDIF
		
	ENDIF

ENDPROC

/// PURPOSE:
///    Determines what fail conditions we need to check dependent on the 
FUNC BOOL PS_DLC_Fly_Low_Fail_Check()
	IF PS_GET_SUBSTATE() = PS_SUBSTATE_UPDATE
		SWITCH(PS_DLC_Fly_Low_State)
			CASE PS_DLC_FLY_LOW_INIT
				//No fail check to do
				RETURN FALSE
				
			CASE PS_DLC_FLY_LOW_COUNTDOWN
				//No fail check to do
				RETURN FALSE
				
			CASE PS_DLC_FLY_LOW_TAKEOFF
				//Do runway check and default checks
				IF PS_UPDATE_FAIL_CHECKS(PS_Main, FALSE, FALSE, TRUE)
					RETURN TRUE
				ENDIF
				BREAK
			
			DEFAULT
				//Do default checks
				IF PS_UPDATE_FAIL_CHECKS(PS_Main, FALSE, TRUE, TRUE,TRUE,TRUE,TRUE,FALSE)
					RETURN TRUE
				ENDIF
				BREAK
		ENDSWITCH
	ENDIF
	//If we get here, we havent failed yet
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks for objective goals completed based on the stage/state we are in of the challenge progression
///    Also updates ChallengeState when we complete an objective 
FUNC BOOL PS_DLC_Fly_Low_Progress_Check()
	IF NOT IS_PED_INJURED(PLAYER_PED_ID()) AND IS_VEHICLE_DRIVEABLE(PS_Main.myVehicle)
		SWITCH(PS_DLC_Fly_Low_State)
			CASE PS_DLC_FLY_LOW_INIT
				RETURN FALSE
				BREAK

			CASE PS_DLC_FLY_LOW_COUNTDOWN
				RETURN FALSE
				BREAK
				
			CASE PS_DLC_FLY_LOW_TAKEOFF
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_UPDATE
						//See if the player is close to the checkpoint yet
						IF PS_GET_CHECKPOINT_PROGRESS() > 1
							RETURN TRUE
						ENDIF
						BREAK
				ENDSWITCH
				BREAK
				
			CASE PS_DLC_FLY_LOW_OBSTACLE
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_UPDATE
						//See if the player is close to the checkpoint yet
						IF PS_IS_LAST_CHECKPOINT_CLEARED()
							RETURN TRUE
						ENDIF
						BREAK
				ENDSWITCH
				BREAK
		ENDSWITCH
	ENDIF
	//if we reach here, we probably haven't passed the check
	RETURN FALSE	
ENDFUNC

PROC PS_DLC_FLY_LOW_FORCE_FAIL
	ePSFailReason = PS_FAIL_DEBUG
	PS_DLC_Fly_Low_State = PS_DLC_FLY_LOW_FAIL
	PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
	
ENDPROC

PROC PS_DLC_FLY_LOW_FORCE_PASS
	PS_DLC_Fly_Low_State = PS_DLC_FLY_LOW_SUCCESS
	PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
ENDPROC

//****************************************************************************************************
//****************************************************************************************************
//	Public functions! (used by Pilot_School.sc to run the challenges)
//****************************************************************************************************
//****************************************************************************************************

PROC PS_DLC_Fly_Low_Initialise()
	SWITCH g_PS_initStage
		CASE 0
			iFlylowDialogueCheckpoint = 2
			//Start pos and heading are hard-coded since it different for each challenge.
			vStartPosition 	= <<803.3491, -2626.5034, 9.7774>>	//<<1883.5802, -2960.7500, 44.9634>>	//use for recording the autopilot
			vStartRotation 	= <<-0.0072, -0.7735, 74.5868>>		//<<-0.0002, -0.0000, 77.0421>>
			fStartHeading 	= 74.50								//77								
			
			iAutoPilotCameraProgress = 0
			bAutoPilotStarted = FALSE
			bFinishedChallenge = FALSE
			VehicleToUse = BESRA

			iPS_PreviewVehicleRecordingID = iPS_PEVIEW_VEH_REC_ID_DLC_FLY_LOW
			REQUEST_VEHICLE_RECORDING(iPS_PreviewVehicleRecordingID, "PilotSchool")
			g_PS_initStage++
			EXIT
		BREAK
		CASE 1
			If HAS_VEHICLE_RECORDING_BEEN_LOADED(iPS_PreviewVehicleRecordingID, "PilotSchool")
			AND PS_SETUP_CHALLENGE()
				SET_AMBIENT_ZONE_LIST_STATE("AZL_BIG_SCORE_2B_WINDFARM_TURBINES",true,true) 
				PS_DLC_Fly_Low_Init_Checkpoints()
				
				fPreviewVehicleSkipTime = 42750 //42600
				fPreviewTime = 15
				
				vPilotSchoolSceneCoords = vStartPosition
				IF HAS_VEHICLE_RECORDING_BEEN_LOADED(iPS_PreviewVehicleRecordingID, sPreviewRecordingName)
					vPilotSchoolPreviewCoords = GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(iPS_PreviewVehicleRecordingID, fPreviewVehicleSkipTime, sPreviewRecordingName)
					vPilotSchoolPreviewRot = GET_ROTATION_OF_VEHICLE_RECORDING_AT_TIME(iPS_PreviewVehicleRecordingID, fPreviewVehicleSkipTime, sPreviewRecordingName)
				ENDIF
				
				//offset for checking whether player is in the air or on the ground
				PS_SET_GROUND_OFFSET(2.5)
				g_PS_initStage++
			ENDIF
		BREAK
		CASE 2
			IF PS_CREATE_VEHICLE(TRUE)
				bwaitingOnSomething = FALSE
				SET_VEHICLE_ENGINE_ON(PS_Main.myVehicle,true,true)
//				SET_WEATHER_FOR_FMMC_MISSION(1) //sunny

				NETWORK_OVERRIDE_CLOCK_TIME(GET_HOUR_FOR_PS_CHALLENGE(),0,0)
				
				#IF IS_DEBUG_BUILD
					PS_DLC_FLY_LOW_CREATE_DEBUG_WIDGETS()
					DEBUG_MESSAGE("******************Setting up PS_DLC_Fly_Low.sc******************")
				#ENDIF
				
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Calling PS_DLC_Fly_Low_Initialise().")
				
				PS_STORE_BRIDGES()
				PS_SET_ALL_BRIDGES_AS_FLOWN_UNDER()
				g_PS_initStage=0
			ENDIF
		BREAK
	ENDSWITCH

ENDPROC

FUNC BOOL PS_DLC_Fly_Low_MainSetup()
	PS_DLC_Fly_Low_Init_Checkpoints()
	SET_MINIMAP_COMPONENT(MINIMAP_COMPONENT_RUNWAY_1, FALSE)
	NETWORK_OVERRIDE_CLOCK_TIME(GET_HOUR_FOR_PS_CHALLENGE(),0,0)
	// Preventing planes from being generated in the two boarding locations, where planes normally pop into view as the challenge starts.
	//VECTOR vGenBlockMin = <<-1514.927490,-3203.546387,26.694818>> - << 200.0, 200.0, 20.0 >>
	//VECTOR VGenBlockMax = <<-1514.927490,-3203.546387,26.694818>> + << 200.0, 200.0, 20.0 >>
	//SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(vGenBlockMin, VGenBlockMax, FALSE)
	BLOCK_SCENARIOS_AND_AMBIENT(sb_PS)
	
	//NEW_LOAD_SCENE_START(<<-1618.1404, -2980.8284, 17.4172>>, NORMALISE_VECTOR(<<-1614.2157, -2983.0940, 17.0217>> - <<-1618.1404, -2980.8284, 17.4172>>), 5000.0)
	NEW_LOAD_SCENE_START(<< 862.10, -2643.81, 11.80 >>, << -0.95, 0.30, 0.00 >>, 5000.0)
	
	PRINTLN(GET_THIS_SCRIPT_NAME(), ": Calling PS_DLC_Fly_Low_MainSetup().")
	RETURN TRUE
ENDFUNC

FUNC BOOL PS_DLC_Fly_Low_Preview()

	//#IF IS_DEBUG_BUILD
	//	DRAW_DEBUG_VEHICLE_RECORDING_INFO(PS_Main.myVehicle, 0.0)
	//#ENDIF
	
	IF PS_PREVIEW_UPDATE_CUTSCENE()
		SWITCH(PS_PreviewState)
			CASE PS_PREVIEW_SETUP
				PS_PREVIEW_SETUP_PLAYER_FOR_CUTSCENE()
				IF NOT IS_VEHICLE_FUCKED(PS_Main.myVehicle)
					START_PLAYBACK_RECORDED_VEHICLE(PS_Main.myVehicle, iPS_PreviewVehicleRecordingID, "PilotSchool")
					SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(PS_Main.myVehicle, fPreviewVehicleSkipTime)
				ENDIF
				iPreviewCheckpointIdx = PS_GET_CLOSEST_PREVIEW_CHECKPOINT(iPS_PreviewVehicleRecordingID, PS_Main.myCheckpointz, fPreviewVehicleSkipTime)
				PS_ACTIVATE_CHECKPOINT_MGR(PS_LESSON_OBSTACLE_PLANE, iPreviewCheckpointIdx + 1, TRUE)
				
				PS_PREVIEW_DIALOGUE_PLAY("PS_LOWPREV", "PS_LOWPREV_1", "PS_LOWPREV_2", "PS_LOWPREV_3", "PS_LOWPREV_4")
				BREAK
				
			CASE PS_PREVIEW_PLAYING
				//let vehicle recording play
				BREAK
				
			CASE PS_PREVIEW_CLEANUP
				PS_PREVIEW_CLEANUP_CUTSCENE()
				BREAK
		ENDSWITCH
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC



FUNC BOOL PS_DLC_Fly_Low_MainUpdate()

	IF TempDebugInput() AND NOT bFinishedChallenge

		/*#IF IS_DEBUG_BUILD
			PS_DLC_FLY_LOW_UPDATE_DEBUG_WIDGETS()
			DRAW_DEBUG_VEHICLE_RECORDING_INFO(PS_Main.myVehicle, 0.0)
			IF PS_IS_CHECKPOINT_MGR_ACTIVE()
				PS_DBG_DrawLiteralStringFloat("CUR: ",PS_Main.myCheckpointMgr.heightFromCurCheckpoint, 1)
				PS_DBG_DrawLiteralStringInt("CHE: ",PS_GET_CLEARED_CHECKPOINTS(), 2)
				PS_DBG_DrawLiteralStringFloat("TOT: ",PS_Main.myCheckpointMgr.totalCheckpointHeight, 3)
				PS_DBG_DrawLiteralStringFloat("AVG: ",PS_Main.myCheckpointMgr.avgCheckpointHeight, 4)
			ENDIF
		#ENDIF*/
		
		PS_LESSON_UDPATE()
	
		PS_FLY_LOW_UPDATE_AUTO_PILOT_CAMERA()
		
		IF PS_DLC_Fly_Low_State = PS_DLC_FLY_LOW_TAKEOFF
		OR PS_DLC_Fly_Low_State = PS_DLC_FLY_LOW_OBSTACLE
			//IF SHOULD_USE_METRIC_MEASUREMENTS()
				IF PS_Main.myCheckpointMgr.avgCheckpointHeight > PS_Challenges[g_current_selected_dlc_PilotSchool_class].BronzeDistance
					DRAW_GENERIC_SCORE(0, "PS_HUDTARG_B",-1,HUD_COLOUR_BRONZE,HUDORDER_SECONDBOTTOM, DEFAULT, "PS_METRE", TRUE, PS_Challenges[g_current_selected_dlc_PilotSchool_class].BronzeDistance)
				ELIF PS_Main.myCheckpointMgr.avgCheckpointHeight > PS_Challenges[g_current_selected_dlc_PilotSchool_class].SilverDistance
					DRAW_GENERIC_SCORE(0, "PS_HUDTARG_S",-1,HUD_COLOUR_SILVER,HUDORDER_SECONDBOTTOM, DEFAULT, "PS_METRE", TRUE, PS_Challenges[g_current_selected_dlc_PilotSchool_class].SilverDistance)				
				ELSE
					DRAW_GENERIC_SCORE(0, "PS_HUDTARG_G",-1,HUD_COLOUR_GOLD,HUDORDER_SECONDBOTTOM, DEFAULT, "PS_METRE", TRUE, PS_Challenges[g_current_selected_dlc_PilotSchool_class].GoldDistance)
				ENDIF
			/*ELSE
				IF PS_Main.myCheckpointMgr.avgCheckpointHeight > PS_Challenges[g_current_selected_dlc_PilotSchool_class].BronzeDistance
					DRAW_GENERIC_SCORE(0, "PS_HUDTARG_B",-1,HUD_COLOUR_BRONZE,HUDORDER_SECONDBOTTOM, DEFAULT, "FMMC_FEET", TRUE, CONVERT_METERS_TO_FEET(PS_Challenges[g_current_selected_dlc_PilotSchool_class].BronzeDistance))
				ELIF PS_Main.myCheckpointMgr.avgCheckpointHeight > PS_Challenges[g_current_selected_dlc_PilotSchool_class].SilverDistance
					DRAW_GENERIC_SCORE(0, "PS_HUDTARG_S",-1,HUD_COLOUR_SILVER,HUDORDER_SECONDBOTTOM, DEFAULT, "FMMC_FEET", TRUE, CONVERT_METERS_TO_FEET(PS_Challenges[g_current_selected_dlc_PilotSchool_class].SilverDistance))				
				ELSE
					DRAW_GENERIC_SCORE(0, "PS_HUDTARG_G",-1,HUD_COLOUR_GOLD,HUDORDER_SECONDBOTTOM, DEFAULT, "FMMC_FEET", TRUE, CONVERT_METERS_TO_FEET(PS_Challenges[g_current_selected_dlc_PilotSchool_class].GoldDistance))
				ENDIF
			ENDIF*/
			
			SET_FAR_RIGHT_TITLE_POSITION_HUD_THIS_FRAME()
		ENDIF

		SWITCH(PS_DLC_Fly_Low_State)
		//Spawn player and plane on the runway
			CASE PS_DLC_FLY_LOW_INIT
				//updating autopilot to start the player out in the air
				PS_FLY_LOW_AUTO_PILOT(PS_Main, 8250.0, 18000.0, 0.90)
				//SET_VEHICLE_FORCE_AFTERBURNER(PS_Main.myVehicle,TRUE)
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						
				
						iFlylowDialogueCheckpoint = 2
						//SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
						DO_NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_EXIT
						PS_DLC_Fly_Low_State = PS_DLC_FLY_LOW_COUNTDOWN
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK

			CASE PS_DLC_FLY_LOW_COUNTDOWN
				//updating autopilot to start the player out in the air
				SET_VEHICLE_FORCE_AFTERBURNER(PS_Main.myVehicle,TRUE)
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_FLY_LOW_AUTO_PILOT(PS_Main, 6650.0, 18000.0, 0.90)
						SET_ON_JOB_INTRO(TRUE)
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						PS_FLY_LOW_AUTO_PILOT(PS_Main, 6650.0, 18000.0, 0.90)						
						//if can start auto pilot countdown
						IF GET_TIME_POSITION_IN_RECORDING(PS_Main.myVehicle) >= 10000.0//9750.0
							SET_ON_JOB_INTRO(FALSE)
							//TASK_VEHICLE_TEMP_ACTION(PLAYER_PED_ID(), PS_Main.myVehicle, TEMPACT_REV_ENGINE, 5500)
							IF PS_HUD_UPDATE_COUNTDOWN(TRUE, FALSE)
								STOP_PLAYBACK_RECORDED_VEHICLE(PS_Main.myVehicle)									
								PS_INCREMENT_SUBSTATE()
							ENDIF
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						PS_RESTORE_BRIDGES()
						STOP_PLAYBACK_RECORDED_VEHICLE(PS_Main.myVehicle)
						cprintln(debug_trevor3,"Stop recording")
						//SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						DO_NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)	
						//SET_PLAYER_INVINCIBLE(PLAYER_ID(),FALSE)
						PS_TRIGGER_MUSIC(PS_MUSIC_GROUND_START)
						PS_DLC_Fly_Low_State = PS_DLC_FLY_LOW_TAKEOFF
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
				
		//Give player control and the first instruction to take off
		//Check if player is staying on the runway and not driving off
			CASE PS_DLC_FLY_LOW_TAKEOFF
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						SET_VEHICLE_FORCE_AFTERBURNER(PS_Main.myVehicle,FALSE)
						PS_DLC_Fly_Low_Init_Checkpoints()
						PS_SET_CHECKPOINT_COUNTER_ACTIVE(TRUE)
						//SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						DO_NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)	
						//SET_PLAYER_INVINCIBLE(PLAYER_ID(),FALSE)
						
						//PS_ACTIVATE_CHECKPOINT_MGR(PS_LESSON_OBSTACLE_PLANE)
						PS_ACTIVATE_CHECKPOINT_MGR(PS_LESSON_HEIGHT_OBSTACLE_PLANE)
					//	PS_PLAY_DISPATCHER_INSTRUCTION_2_LINES("PS_FLYLOW", "PS_FLYLOW_1", "PS_FLYLOW", "PS_FLYLOW_2")
						PS_PLAY_DISPATCHER_INSTRUCTION_CONVERSATION("PS_LOWSTART")
						//PS_HUD_RESET_TIMER()
						PS_HUD_SET_GUTTER_ICON_ACTIVE(TRUE)
						//PS_SET_TIMER_ACTIVE(TRUE)
						PS_SET_HINT_CAM_ACTIVE(TRUE)
						PS_SET_HINT_CAM_COORD(PS_GET_CURRENT_CHECKPOINT())
						PS_SET_AVG_HEIGHT_HUD_ACTIVE(TRUE)
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF PS_DLC_Fly_Low_Fail_Check()
							PS_DLC_Fly_Low_State = PS_DLC_FLY_LOW_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELIF PS_DLC_Fly_Low_Progress_Check()				
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						PS_DLC_Fly_Low_State = PS_DLC_FLY_LOW_OBSTACLE
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
	
		//Obstacle course loop
			CASE PS_DLC_FLY_LOW_OBSTACLE
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_KILL_HINT_CAM()
 				    	PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
					
						flylow_dialogue_update()
						
						IF PS_DLC_Fly_Low_Fail_Check()
							PS_DLC_Fly_Low_State = PS_DLC_FLY_LOW_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELIF PS_DLC_Fly_Low_Progress_Check()
						
						
							//check if we need to play fail dialogue for too high average checkpoint height
							IF ( iFlylowDialogueCheckpoint != -1 )
								IF PS_Main.myCheckpointMgr.avgCheckpointHeight > PS_Main.myChallengeData.BronzeDistance
									PS_PLAY_DISPATCHER_INSTRUCTION("PS_LOWFAIL", "PS_LOWFAIL_1")
									iFlylowDialogueCheckpoint = -1
								ENDIF
							ENDIF
						
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								PS_INCREMENT_SUBSTATE()
							ENDIF
						ELSE
							PS_SET_HINT_CAM_COORD(PS_GET_CURRENT_CHECKPOINT())
							//PS_DLC_Fly_Low_Update_Dialogue()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						PS_UPDATE_RECORDS(VECTOR_ZERO)
						PS_SET_AVG_HEIGHT_HUD_ACTIVE(FALSE)
						IF Pilot_School_Data_Check_Mission_Success(PS_Main.myChallengeData, PS_Main.myPlayerData)
							PS_DLC_Fly_Low_State = PS_DLC_FLY_LOW_SUCCESS
						ELSE
							PS_DLC_Fly_Low_State = PS_DLC_FLY_LOW_FAIL
						ENDIF
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
												
			CASE PS_DLC_FLY_LOW_SUCCESS
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_UPDATE_RECORDS(VECTOR_ZERO)
						PS_PLAY_LESSON_FINISHED_LINE("PS_LOWPASS", "PS_LOWPASS_1", "PS_LOWPASS_2", "PS_LOWPASS_3", "")
						PS_TRIGGER_MUSIC(PS_MUSIC_GROUND_STOP)
						PS_LESSON_END(TRUE)
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF NOT IS_PLAYER_DEAD(player_id())							
						OR GET_MANUAL_RESPAWN_STATE()= MRS_READY_FOR_PLACEMENT
							IF NOT PS_HUD_IS_SCORECARD_ACTIVE()
								PS_INCREMENT_SUBSTATE()
							ENDIF
						ELSE
							PRINTLN("PLAYER IS DEAD SETTING RESPAWN STATE")
							//Set the player to respawn state if dead and doesn't have respawn state set
							IF IS_PLAYER_DEAD(player_id())
							AND GET_MANUAL_RESPAWN_STATE() = MRS_NULL
								SET_MANUAL_RESPAWN_STATE(MRS_RESPAWN_PLAYER_HIDDEN)
							ENDIF
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						bFinishedChallenge = TRUE
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
				
			CASE PS_DLC_FLY_LOW_FAIL
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_TRIGGER_MUSIC(PS_MUSIC_GROUND_FAIL)
						PS_UPDATE_RECORDS(VECTOR_ZERO)
						//PS_PLAY_LESSON_FINISHED_LINE("PS_POC", "PS_POC_5", "PS_POC_6", "PS_POC_7", "PS_POC_8")
						PS_LESSON_END(FALSE)
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF NOT PS_HUD_IS_SCORECARD_ACTIVE()
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						bFinishedChallenge = TRUE
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
		ENDSWITCH
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC PS_DLC_Fly_Low_MainCleanup()
//hud
	PS_HUD_SET_GUTTER_ICON_ACTIVE(FALSE)
	PS_HUD_SET_OBJECTIVE_METER_VISIBLE(FALSE)
	PS_HUD_SET_AVG_HEIGHT_HUD_VISIBLE(FALSE)
	//PS_SET_TIMER_ACTIVE(FALSE)
	PS_HUD_CLEANUP()
	CLEAR_PRINTS()
	CLEAR_HELP()

	SET_AMBIENT_ZONE_LIST_STATE("AZL_BIG_SCORE_2B_WINDFARM_TURBINES",false,true)

//player/objects
	PS_SEND_PLAYER_BACK_TO_MENU()

//cleanup checkpoints
	PS_RESET_CHECKPOINT_MGR()
	
//assets
	REMOVE_VEHICLE_RECORDING(iPS_PreviewVehicleRecordingID, "PilotSchool")
	SET_MODEL_AS_NO_LONGER_NEEDED(VehicleToUse)	
	
	// Allow the blocked planes from being generated in the two boarding locations.
	//VECTOR vGenBlockMin = <<-1514.927490,-3203.546387,26.694818>> - << 200.0, 200.0, 20.0 >>
	//VECTOR VGenBlockMax = <<-1514.927490,-3203.546387,26.694818>> + << 200.0, 200.0, 20.0 >>
	//SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(vGenBlockMin, VGenBlockMax, TRUE)
	UNBLOCK_SCENARIOS_AND_AMBIENT(sb_PS)
	
//reset clock time

	
	#IF IS_DEBUG_BUILD
		DEBUG_MESSAGE("******************Cleaning up PS_Plane_Course.sc******************")
	#ENDIF
ENDPROC

// EOF
//****************************************************************************************************
//****************************************************************************************************


