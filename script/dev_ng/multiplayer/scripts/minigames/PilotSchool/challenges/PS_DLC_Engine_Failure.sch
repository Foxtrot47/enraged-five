//****************************************************************************************************
//
//	Author: 		Kevin Bolt / Mark Beagan
//	Date: 			18/10/13
//	Description:	"Engine Failure" Challenge for Pilot School. The player is required to pilot
//					a plane with the engine failing.
//****************************************************************************************************

USING "minigames_helpers.sch"
USING "Pilot_School_MP_Challenge_Helpers.sch"

CONST_INT efNUM_AMB_VEH 6

//Vector for the location the player touches down
VECTOR vTouchdownCoord

//Getting the plane position
VECTOR vplanePos

//Flag for landing gear
BOOL bLandingGear = FALSE

//Flag for setting the time
BOOL bEngineFailTimeSet = FALSE

//Timer for engines smoking
INT iTimeSmoke = -1
INT iWarningSound = -1
INT iWarningSoundID = 0
INT iWarningTime = 0

INT iLandedTimer = -1

//Timer for dialogue about engines cutting out
INT iDiagEngineTime = 0



//Struct for ambient vehicles
STRUCT PSDLCEFVEHICLE
	VEHICLE_INDEX efveh
	PED_INDEX efped
	MODEL_NAMES efmn
	VECTOR vStart
	FLOAT fStart
ENDSTRUCT

//Array for ambient vehicles
PSDLCEFVEHICLE efvehs[efNUM_AMB_VEH]
//SEQUENCE_INDEX efseq

//****************************************************************************************************
//****************************************************************************************************
//	Private functions that are used only by Engine Failure
//****************************************************************************************************
//****************************************************************************************************

//Initialise the ambient vehicles
PROC PS_DLC_Engine_Failure_Initialise_Vehs()
	efvehs[0].efmn = firetruk
	efvehs[0].vStart = <<-2117.9746, 2892.2708, 31.8099>>
	efvehs[0].fStart = 358.5513
	
	efvehs[1].efmn = ambulance
	efvehs[1].vStart = <<-2093.5266, 2935.1143, 31.8099>>
	efvehs[1].fStart = 108.4106
	
	efvehs[2].efmn = firetruk
	efvehs[2].vStart = <<-1952.0725, 2835.3579, 31.8101>>
	efvehs[2].fStart = 121.9444
	
	efvehs[3].efmn = ambulance
	efvehs[3].vStart = <<-2564.8213, 3146.1328, 31.8189>>
	efvehs[3].fStart = 240.0324
	
	efvehs[4].efmn = firetruk
	efvehs[4].vStart = <<-2709.8123, 3298.1165, 31.8097>>
	efvehs[4].fStart = 241.7277
	
	efvehs[5].efmn = rhino
	efvehs[5].vStart = <<-2293.1748, 3070.4243, 31.8107>>
	efvehs[5].fStart = 67.9030
	
	/*
	efvehs[4].efmn = Lazer
	efvehs[4].vStart = <<-2386.5845, 3001.4751, 31.8101>>
	efvehs[4].fStart = 56.4591
	
	efvehs[5].efmn = Lazer
	efvehs[5].vStart = <<-2438.5662, 3196.9636, 31.8274>>
	efvehs[5].fStart = 235.8421
	
	*/
	//End of runway
	/*efvehs[6].efmn = Lazer
	efvehs[6].vStart = <<-2011.1232, 2860.1121, 31.9013>>
	efvehs[6].fStart = 61.5864*/
ENDPROC

//Create the vehicles
FUNC BOOL PS_DLC_Engine_Failure_Create_Vehs()

INT icount
	cprintln(debug_trevor3,"PS_DLC_Engine_Failure_Create_Vehs()")
	FOR icount = 0 to efNUM_AMB_VEH - 1
		cprintln(debug_trevor3,"Make car: ",icount)
		IF PS_CREATE_NET_VEHICLE(PS_ambVeh_NETID[icount],efvehs[icount].efmn, efvehs[icount].vStart, efvehs[icount].fStart)
			cprintln(debug_trevor3,"Make ped: ",icount)
			IF PS_CREATE_NET_PED_IN_VEHICLE(PS_ambPed_NETID[icount],PS_ambVeh_NETID[icount],PEDTYPE_MISSION, S_M_M_Marine_01, VS_DRIVER)				
				efvehs[icount].efveh = NET_TO_VEH(PS_ambVeh_NETID[icount])
				efvehs[icount].efped = NET_TO_PED(PS_ambPed_NETID[icount])
				
				//Ensure local machine has control of the vehicle / peds
				IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(efvehs[icount].efveh)
					NETWORK_REQUEST_CONTROL_OF_ENTITY(efvehs[icount].efveh)
				ENDIF
				
				IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(efvehs[icount].efped)
					NETWORK_REQUEST_CONTROL_OF_ENTITY(efvehs[icount].efped)
				ENDIF
				
				IF NOT IS_ENTITY_DEAD(efvehs[icount].efped)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(efvehs[icount].efped,TRUE)
					IF NOT IS_ENTITY_DEAD(efvehs[icount].efveh)
						SET_ENTITY_LOAD_COLLISION_FLAG(efvehs[icount].efveh, TRUE)
					ENDIF
				ENDIF
				if efvehs[icount].efmn = AMBULANCE OR efvehs[icount].efmn = FIRETRUK
					IF NOT IS_ENTITY_DEAD(efvehs[icount].efveh)
						IF NETWORK_HAS_CONTROL_OF_ENTITY(efvehs[icount].efveh)
							SET_VEHICLE_SIREN(efvehs[icount].efveh, TRUE)
						ENDIF
					ENDIF
				ENDIF
			ELSE
				cprintln(debug_trevor3,"Fail Make ped: ",icount)
				RETURN FALSE
			ENDIF
		ELSE
			cprintln(debug_trevor3,"Fail Make car: ",icount)
			RETURN FALSE
		ENDIF				
	ENDFOR
	cprintln(debug_trevor3,"finished PS_DLC_Engine_Failure_Create_Vehs()")
	RETURN TRUE
ENDFUNc

//Delete the ambient vehicles
PROC PS_DLC_Engine_Failure_Delete_Vehs()

INT icount

	FOR icount = 0 to efNUM_AMB_VEH - 1
		IF NETWORK_DOES_NETWORK_ID_EXIST(PS_ambVeh_NETID[icount])
			PS_DELETE_NET_ID(PS_ambVeh_NETID[icount])
		ENDIF
	
		IF NETWORK_DOES_NETWORK_ID_EXIST(PS_ambPed_NETID[iCount])
			PS_DELETE_NET_ID(PS_ambPed_NETID[iCount])		
		ENDIF
	ENDFOR
ENDPROC

//Give the emergency vehicle a task
PROC PS_DLC_Engine_Failure_Give_EVeh_Task(PED_INDEX &eped, VEHICLE_INDEX &eveh, VEHICLE_ESCORT_TYPE vehesc = VEHICLE_ESCORT_REAR )
	IF DOES_ENTITY_EXIST( PS_Main.myVehicle )
		if GET_DISTANCE_BETWEEN_ENTITIES(PS_Main.myVehicle, eveh) > 25
			if GET_SCRIPT_TASK_STATUS(eped, SCRIPT_TASK_ANY) <> PERFORMING_TASK
				
				TASK_VEHICLE_ESCORT(eped, eveh,PS_Main.myVehicle,vehesc,14,DRIVINGMODE_AVOIDCARS,25,00,1000)
				
				//TASK_VEHICLE_DRIVE_TO_COORD(efvehs[icount].efped, efvehs[icount].efveh, GET_ENTITY_COORDS(PLAYER_PED_ID()), 70, DRIVINGSTYLE_RACING, efvehs[icount].efmn, DRIVINGMODE_STOPFORCARS, 20, 10000)
			ENDIF				
		ELSE
			//If there is already a success / fail and they are nearby clear the tasks
			/*if PS_DLC_Engine_Failure_State != PS_DLC_Engine_on_runway
				if GET_SCRIPT_TASK_STATUS(efvehs[icount].efped, SCRIPT_TASK_ANY) = PERFORMING_TASK
						CLEAR_PED_TASKS(efvehs[icount].efped)
						TASK_ENTER_VEHICLE(efvehs[icount].efped,efvehs[icount].efveh,1,VS_DRIVER,PEDMOVEBLENDRATIO_RUN,ECF_WARP_PED)
					ENDIF
					//TASK_ENTER_VEHICLE(efvehs[icount].efped,efvehs[icount].efveh,1,VS_DRIVER,PEDMOVEBLENDRATIO_RUN,ECF_WARP_PED)
					//SET_PED_INTO_VEHICLE(efvehs[icount].efped, efvehs[icount].efveh)
				ENDIF
			ENDIF*/
		ENDIF
	ENDIF
ENDPROC

//Task the vehicle to drive to the plane
PROC PS_DLC_Engine_Failure_Drive_To_Plane()
	INT icount
	
	FOR icount = 3 to efNUM_AMB_VEH - 2
		IF DOES_ENTITY_EXIST( efvehs[icount].efveh )
		AND DOES_ENTITY_EXIST( efvehs[icount].efped )
		AND DOES_ENTITY_EXIST( PS_Main.myVehicle )
			
			//Have vehicles move as soon as the player lands
			if NOT IS_VEHICLE_FUCKED(efvehs[icount].efveh) 
			AND NOT IS_ENTITY_DEAD(efvehs[icount].efped) 
			AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			AND NOT IS_ENTITY_DEAD(PS_Main.myVehicle)
			AND NOT IS_ENTITY_IN_AIR(PS_Main.myVehicle)
			AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), PS_Main.myVehicle)
				PS_DLC_Engine_Failure_Give_EVeh_Task(efvehs[icount].efped, efvehs[icount].efveh)
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

//Task the vehicles to drive to the plane
PROC PS_DLC_Engine_Failure_Drive_To_Plane_Low_Speed()
INT icount

	FOR icount = 0 to 1
		IF DOES_ENTITY_EXIST( efvehs[icount].efveh )
		AND DOES_ENTITY_EXIST( efvehs[icount].efped )
			if NOT IS_VEHICLE_FUCKED(efvehs[icount].efveh) 
			AND NOT IS_ENTITY_DEAD(efvehs[icount].efped) 
			AND DOES_ENTITY_EXIST( PS_Main.myVehicle )
			AND NOT IS_ENTITY_DEAD(PS_Main.myVehicle)
			AND GET_ENTITY_SPEED(PS_Main.myVehicle) < 50
			AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), PS_Main.myVehicle)
				//Have vehicles move towards the plane going a low speed
				if icount = 0
					PS_DLC_Engine_Failure_Give_EVeh_Task( efvehs[icount].efped, efvehs[icount].efveh, VEHICLE_ESCORT_RIGHT )
				ELIF icount = 1
					PS_DLC_Engine_Failure_Give_EVeh_Task( efvehs[icount].efped, efvehs[icount].efveh, VEHICLE_ESCORT_LEFT )
				ELSE
					PS_DLC_Engine_Failure_Give_EVeh_Task( efvehs[icount].efped, efvehs[icount].efveh)
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

//Handle the emergency vehicles
PROC PS_DLC_Engine_Failure_Handle_Emergency_Vehs()
	PS_DLC_Engine_Failure_Drive_To_Plane()
	PS_DLC_Engine_Failure_Drive_To_Plane_Low_Speed()
ENDPROC

//Procedure to initialise the checkpoints
PROC PS_DLC_Engine_Failure_Init_Checkpoints(INT iRouteToDisplay = 1)
	PS_RESET_CHECKPOINT_MGR()
		iRouteToDisplay=iRouteToDisplay
	
	//PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<-3726.6357, 3997.1201, 800>>) //start
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_LANDING, <<-2109.8767, 2918.7434, 31.8099>>) //Landing location
ENDPROC

FUNC BOOL PS_IS_Engine_Failure_FLIGHT_COMPLETED()
	RETURN FALSE
ENDFUNC

//Used to boost the players movement
PROC PS_Engine_Failure_Boost_Movement()
//Pitch boost
/*INT iOffSet = 5
INT iupforce = 15*/
Vector vrot

vrot = vrot

//Brake boost
INT iBrakeBoost = 85
INT iBrakeClose = 50
INT iRunwayBB = 100
//INT iBrakeLong = 150
//INT iBrakeNonZ = 500
VECTOR vecVel
VECTOR vecNeg
VECTOR vecRem

	PRINTSTRING("ENGINE FAILURE - Left Axis Y Value : ") PRINTFLOAT(GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y)) PRINTNL()
	vrot = GET_ENTITY_ROTATION(PS_Main.myVehicle, EULER_XYZ)
	
	PRINTSTRING("ENGINE FAILURE - Rotation  XYZ     : <<") PRINTFLOAT(vrot.x) PRINTSTRING(", ") PRINTFLOAT(vrot.y) PRINTSTRING(", ") PRINTFLOAT(vrot.z) PRINTSTRING(">>") PRINTNL()
	//Pitch boost
	/*IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_FLY_PITCH_UP_ONLY) OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_MOVE_UP)
		PRINTSTRING("ENGINE FAILURE - CONTROL TO FLY UP BEING PRESSED") PRINTNL()
		
		IF vrot.X < 10
			//SET_ENTITY_ROTATION(PS_Main.myVehicle, <<vrot.X + 1, vrot.Y, vrot.Z>>)
		ENDIF
		
		/*APPLY_FORCE_TO_ENTITY(PS_Main.myVehicle, APPLY_TYPE_FORCE, <<0,0,iupforce>>, <<0, iOffSet, 0>>, 0, TRUE, TRUE, TRUE)
		APPLY_FORCE_TO_ENTITY(PS_Main.myVehicle, APPLY_TYPE_FORCE, <<0,0,-iupforce>>, <<0, -iOffSet, 0>>, 0, TRUE, TRUE, TRUE)	
	ENDIF*/

	vPlanePos = GET_ENTITY_COORDS(PS_Main.myVehicle)

	if vPlanePos.z < 300
		IF NOT IS_PAUSE_MENU_ACTIVE()
			IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_LT)
				PRINTSTRING("ENGINE FAILURE - PLAYER IS BREAKING")
				GET_ENTITY_SPEED(PS_Main.myVehicle)
				
				if vPlanePos.z < 100
					vecvel = GET_ENTITY_VELOCITY(PS_Main.myVehicle)
					vecNeg = <<0, 0, vecvel.Z/iBrakeClose>>
					vecRem = <<vecvel.X - vecNeg.X, vecvel.Y - vecNeg.Y, vecvel.Z - vecNeg.Z>>
				ELSE
					vecvel = GET_ENTITY_VELOCITY(PS_Main.myVehicle)
					vecNeg = <<0, 0, vecvel.Z/iBrakeBoost>>
					vecRem = <<vecvel.X - vecNeg.X, vecvel.Y - vecNeg.Y, vecvel.Z - vecNeg.Z>>
				ENDIF
				
				if vPlanePos.z < 40
					vecNeg = <<vecvel.X/iRunwayBB, vecvel.Y/iRunwayBB, 0>>
					vecRem = <<vecvel.X - vecNeg.X, vecvel.Y - vecNeg.Y, vecvel.Z - vecNeg.Z>>
				ENDIF
				
				SET_ENTITY_VELOCITY(PS_Main.myVehicle, vecRem)
			ENDIF
		ENDIF
	ELSE
		/*vecvel = GET_ENTITY_VELOCITY(PS_Main.myVehicle)
		vecNeg = <<0, 0, vecvel.Z/iBrakeLong>>
		vecRem = <<vecvel.X - vecNeg.X, vecvel.Y - vecNeg.Y, vecvel.Z - vecNeg.Z>>*/
	ENDIF
ENDPROC

//Gets the current time and sets the clock
PROC PS_DLC_ENGINE_SET_TIME()

	if not bEngineFailTimeSet OR GET_CLOCK_HOURS() != 9				
	
		bEngineFailTimeSet = TRUE
	ENDIF
ENDPROC

//Check for player trying to deploy landing gear
PROC CHECK_LANDING_GEAR()
	IF NOT IS_PAUSE_MENU_ACTIVE()
		IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_LS)
			if bLandingGear
				CONTROL_LANDING_GEAR(PS_Main.myVehicle,LGC_RETRACT)
				bLandingGear = FALSE
			ELSE
				CONTROL_LANDING_GEAR(PS_Main.myVehicle,LGC_DEPLOY)
				bLandingGear = TRUE
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//Update the distance being displayed
PROC PS_Engine_Update_Distance()
	PS_SET_DIST_HUD_ACTIVE(TRUE)
ENDPROC

/// PURPOSE:
///    Determines what fail conditions we need to check dependent on the 
/// PARAMS:
///    PS_Main -
///    
FUNC BOOL PS_DLC_Engine_Failure_Fail_Check()
	IF PS_GET_SUBSTATE() = PS_SUBSTATE_UPDATE
		SWITCH(PS_DLC_Engine_Failure_State)
			CASE PS_DLC_ENGINE_INIT
				//No fail check to do
				RETURN FALSE
				
			CASE PS_DLC_ENGINE_COUNTDOWN
				//No fail check to do
				RETURN FALSE
				
			CASE PS_DLC_ENGINE_CUTOUT
				//Do runway check and default checks
				IF PS_UPDATE_FAIL_CHECKS(PS_Main, FALSE, FALSE,TRUE,TRUE,TRUE,FALSE,TRUE,TRUE,TRUE)
					cprintln(debug_trevor3,"FAIL A")
					RETURN TRUE
				ENDIF
			BREAK
			
			CASE PS_DLC_Engine_on_runway
				IF PS_UPDATE_FAIL_CHECKS(PS_Main, FALSE, FALSE,FALSE,TRUE,FALSE,FALSE,TRUE,TRUE,TRUE)
					cprintln(debug_trevor3,"FAIL B")
					RETURN TRUE
				ENDIF
				
				IF ((GET_ENTITY_SPEED(PS_Main.myVehicle) > -1 AND GET_ENTITY_SPEED(PS_Main.myVehicle) < 1) AND NOT IS_ENTITY_IN_ANGLED_AREA( PS_Main.myVehicle, <<-2857.218506,3349.750977,28.934713>>, <<-1944.094849,2822.453613,67.992554>>, 79.562500))
					ePSFailReason = PS_FAIL_NOT_ON_RUNWAY
					RETURN TRUE
				ENDIF
			BREAK
			
			DEFAULT
				//Do default checks
				IF PS_UPDATE_FAIL_CHECKS(PS_Main, FALSE, FALSE,FALSE,TRUE,TRUE,FALSE,TRUE,TRUE,TRUE)
					cprintln(debug_trevor3,"FAIL C")
					RETURN TRUE
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
	//If we get here, we havent failed yet
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks for objective goals completed based on the stage/state we are in of the challenge progression
///    Also updates ChallengeState when we complete an objective 
/// PARAMS:
///    PS_Main - 
FUNC BOOL PS_DLC_Engine_Failure_Progress_Check()		

	SWITCH(PS_DLC_Engine_Failure_State)
			CASE PS_DLC_Engine_INIT
				RETURN FALSE
			BREAK
				
			CASE PS_DLC_Engine_COUNTDOWN
				RETURN FALSE
			BREAK
				
		//Give player control and the first instruction to take off
		//Check if player is staying on the runway and not driving offroad
			CASE PS_DLC_Engine_CUTSCENE
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_UPDATE
						//See if the player is close to the checkpoint yet
						IF PS_GET_CHECKPOINT_PROGRESS() > 1 OR PS_IS_LAST_CHECKPOINT_CLEARED()
							RETURN TRUE
						ENDIF
						BREAK
				ENDSWITCH
			BREAK
			
			CASE PS_DLC_Engine_on_runway
				IF GET_ENTITY_SPEED(PS_Main.myVehicle) > -0.1 AND GET_ENTITY_SPEED(PS_Main.myVehicle) < 0.1
					RETURN TRUE
				ENDIF
			BREAK
				
			CASE PS_DLC_ENGINE_CUTOUT
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					RETURN TRUE
				ENDIF
			BREAK
		ENDSWITCH
	RETURN FALSE
ENDFUNC

//On Failing with F skip
PROC PS_DLC_Engine_Failure_FORCE_FAIL
	ePSFailReason = PS_FAIL_DEBUG
	PS_DLC_Engine_Failure_State = PS_DLC_Engine_FAIL
	PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
ENDPROC

//On passing with S skip
PROC PS_DLC_Engine_Failure_FORCE_PASS
	PS_DLC_Engine_Failure_State = PS_DLC_Engine_SUCCESS
	PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
ENDPROC

//****************************************************************************************************
//****************************************************************************************************
//	Public functions! (used by Pilot_School.sc to run the challenges)
//****************************************************************************************************
//****************************************************************************************************

//Main initialise procedure
PROC PS_DLC_Engine_Failure_Initialise()
	SWITCH g_PS_initStage
		CASE 0
			iPS_PreviewVehicleRecordingID = iPS_PEVIEW_VEH_REC_ID_DLC_ENGINE_FAILURE
			vStartPosition								= <<-4800.6470, 4482.0015, 1161.8878>> //<<-3726.6357, 3997.1201, 800.6536>>	//<<1939.2792, 3041.4685, 280>>	//mid air countryside <<2120.3811, 4806.7539, 40.1959>>	//mckenzie airfield
			vStartRotation 								=  <<-5, 0, -120.8831>>//<<-39.6548, 1.1660, -120.8831>>		//<<-7.4175, -7.1073, 65.0>>	//mid air countryside <<8.8095, -0.0036, 115.0>> 		//mckenzie airfield
			fStartHeading 								= 237.6284							//65.0							//mid air countryside 115.0								//mckenzie airfield
			iPSDialogueCounter							= 1
			iPSHelpTextCounter							= 1
			iPSObjectiveTextCounter						= 1
			iPreviewCameraProgress						= 0
			iTimeSmoke = -1
			iWarningSound = -1
			iWarningSoundID = 0
			
			//Player vehicle model
			//VehicleToUse = TITAN
			VehicleToUse = miljet
			//PS_SET_GROUND_OFFSET(1.5)
			
			SET_DISTANT_CARS_ENABLED(FALSE)
			
			//Block wanted level
			SET_WANTED_LEVEL_MULTIPLIER(0)
			
			REQUEST_MODEL(Barracks)
			REQUEST_MODEL(ambulance)
			REQUEST_MODEL(firetruk)
			REQUEST_MODEL(rhino)
			REQUEST_MODEL(S_M_M_Marine_01)
			REQUEST_MODEL(lazer)
	
			//load assets
			REQUEST_VEHICLE_RECORDING(iPS_PreviewVehicleRecordingID, sPreviewRecordingName)
			g_PS_initStage++
			EXIT
		BREAK
		CASE 1
			
			IF HAS_VEHICLE_RECORDING_BEEN_LOADED(iPS_PreviewVehicleRecordingID, sPreviewRecordingName)
			AND HAS_MODEL_LOADED(Barracks)
			AND HAS_MODEL_LOADED(ambulance)
			AND HAS_MODEL_LOADED(firetruk)
			AND HAS_MODEL_LOADED(rhino)
			AND HAS_MODEL_LOADED(S_M_M_Marine_01)
			AND HAS_MODEL_LOADED(lazer)
			AND PS_SETUP_CHALLENGE()
				
	
				PS_DLC_Engine_Failure_Init_Checkpoints()

				//init vars
				PS_DLC_Engine_Failure_State = PS_DLC_Engine_INIT
				
				fPreviewVehicleSkipTime = 0
				fPreviewTime = 16
				
//				SET_WEATHER_FOR_FMMC_MISSION(1) //sunny

				NETWORK_OVERRIDE_CLOCK_TIME(GET_HOUR_FOR_PS_CHALLENGE(),0,0)
				
				vPilotSchoolSceneCoords = vStartPosition
				IF HAS_VEHICLE_RECORDING_BEEN_LOADED(iPS_PreviewVehicleRecordingID, sPreviewRecordingName)
					vPilotSchoolPreviewCoords = GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(iPS_PreviewVehicleRecordingID, fPreviewVehicleSkipTime, sPreviewRecordingName)
					vPilotSchoolPreviewRot = GET_ROTATION_OF_VEHICLE_RECORDING_AT_TIME(iPS_PreviewVehicleRecordingID, fPreviewVehicleSkipTime, sPreviewRecordingName)
				ENDIF
				
				PS_DLC_Engine_Failure_Initialise_Vehs()
				g_PS_initStage++
				EXIT
			ENDIF
		BREAK
		CASE 2			
			//Spawn player vehicle and ambient vehicles			
			IF PS_CREATE_VEHICLE()
			AND PS_DLC_Engine_Failure_Create_Vehs()		
				SET_VEHICLE_COLOURS(PS_Main.myVehicle, 121, 21)
				SET_VEHICLE_EXTRA_COLOURS(PS_Main.myVehicle, 8, 156)
				BLOCK_SCENARIOS_AND_AMBIENT(sb_PS, DEFAULT, TRUE)
				#IF IS_DEBUG_BUILD
					DEBUG_MESSAGE("******************Setting up PS_DLC_Engine_Failure.sc******************")
				#ENDIF
				bwaitingOnSomething = FALSE
				g_PS_initStage=0
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

//Main Preview function
FUNC BOOL PS_DLC_Engine_Failure_Preview()
	IF PS_PREVIEW_UPDATE_CUTSCENE()
		SWITCH(PS_PreviewState)
			CASE PS_PREVIEW_SETUP
				PS_PREVIEW_SETUP_PLAYER_FOR_CUTSCENE()
				cprintln(debug_trevor3,"Start playback")
				SET_VEHICLE_CONVERSATIONS_PERSIST(true, true)
				PS_PREVIEW_DIALOGUE_PLAY("PS_EFPREV", "PS_EFPREV_1", "PS_EFPREV_2", "PS_EFPREV_3")
				IF NOT IS_VEHICLE_FUCKED(PS_Main.myVehicle)
					START_PLAYBACK_RECORDED_VEHICLE(PS_Main.myVehicle, iPS_PreviewVehicleRecordingID, "PilotSchool")
					SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(PS_Main.myVehicle, fPreviewVehicleSkipTime)
					SET_PLAYBACK_SPEED(PS_Main.myVehicle, 1.0)
					//SET_VEHICLE_ENGINE_HEALTH(PS_Main.myVehicle,-1000)
					SET_ENTITY_HEALTH(PS_Main.myVehicle,2000)
					SET_VEHICLE_ENGINE_ON(PS_Main.myVehicle,false,false)
					CONTROL_LANDING_GEAR(PS_Main.myVehicle,LGC_RETRACT_INSTANT)
				ENDIF
				
				PS_DLC_ENGINE_SET_TIME()
					
				iPreviewCameraProgress 	= 0
				iPreviewCheckpointIdx 	= PS_GET_CLOSEST_PREVIEW_CHECKPOINT(iPS_PreviewVehicleRecordingID, PS_Main.myCheckpointz, fPreviewVehicleSkipTime)
				PS_ACTIVATE_CHECKPOINT_MGR(PS_LESSON_STUNT, iPreviewCheckpointIdx, TRUE)
			BREAK
			
			CASE PS_PREVIEW_PLAYING
				/*IF GET_IS_VEHICLE_ENGINE_RUNNING(PS_Main.myVehicle)
					SET_VEHICLE_ENGINE_ON(PS_Main.myVehicle,false,true)
				ENDIF*/			
			BREAK
			
			CASE PS_PREVIEW_CLEANUP
				PS_PREVIEW_CLEANUP_CUTSCENE()
			BREAK
			
		ENDSWITCH
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

//Main setup procedure
FUNC BOOL PS_DLC_Engine_Failure_MainSetup()
	PS_DLC_Engine_Failure_Init_Checkpoints()
	SET_MINIMAP_COMPONENT(MINIMAP_COMPONENT_RUNWAY_1, FALSE)
	IF NOT IS_VEHICLE_FUCKED(PS_Main.myVehicle)
		SET_VEHICLE_ENGINE_ON(PS_Main.myVehicle,true,true)
		SET_ENTITY_ROTATION(PS_Main.myVehicle, vStartRotation)
		SET_VEHICLE_ENGINE_HEALTH(PS_Main.myVehicle,1000)
		SET_ENTITY_HEALTH(PS_Main.myVehicle,20000)
		CONTROL_LANDING_GEAR(PS_Main.myVehicle,LGC_RETRACT_INSTANT)
	ENDIF
	
	SET_DISTANT_CARS_ENABLED(FALSE)
	
	PS_DLC_Engine_Failure_Create_Vehs()
	
	//IF NOT IS_VEHICLE_FUCKED(PS_Main.myVehicle)
		//PS_AUTO_PILOT(PS_Main, 70.0, FALSE, vStartRotation.x, vStartRotation.y)
		//SET_VEHICLE_ENGINE_HEALTH(PS_Main.myVehicle,-1000)
	//ENDIF
	NETWORK_OVERRIDE_CLOCK_TIME(GET_HOUR_FOR_PS_CHALLENGE(),0,0)
	PS_DLC_ENGINE_SET_TIME()

	NEW_LOAD_SCENE_START(<<-1154.1101, -2715.2024, 18.8923>>, <<-0.90, -0.44, -0.01>>, 5000.0)
	RETURN TRUE
ENDFUNC

#IF IS_DEBUG_BUILD
	FUNC BOOL PS_DLC_Engine_Failure_J_SKIP()
		/*PS_DLC_Engine_Failure_CHALLENGE nextState = PS_DLC_Engine_Failure_State
		IF PS_DLC_Engine_Failure_State > PS_DLC_Engine_Failure_TAKEOFF
			IF PS_DLC_Engine_Failure_State < PS_DLC_Engine_Failure_LOOP_ONE_PREP
				nextState = PS_DLC_Engine_Failure_LOOP_ONE_PREP			
			ELIF PS_DLC_Engine_Failure_State < PS_DLC_Engine_Failure_LOOP_TWO_PREP
				nextState = PS_DLC_Engine_Failure_LOOP_TWO_PREP
			ENDIF
			IF nextState != PS_DLC_Engine_Failure_State AND NOT IS_ENTITY_DEAD(PS_Main.myVehicle)
				//send plane to the takeoff checkpoint
				SET_ENTITY_COORDS(PS_Main.myVehicle, PS_GET_CHECKPOINT_FROM_ID(1))
				SET_VEHICLE_FORWARD_SPEED(PS_Main.myVehicle, 60.0)
				PS_DLC_Engine_Failure_State = nextState
				PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
				RETURN TRUE
			ENDIF	
		ENDIF*/
		RETURN FALSE
	ENDFUNC
#ENDIF	

//Main update function
FUNC BOOL PS_DLC_Engine_Failure_MainUpdate()			
	IF TempDebugInput() AND NOT bFinishedChallenge
		
		#IF IS_DEBUG_BUILD
			IF PS_CHECK_FOR_STUNT_LESSON_J_SKIP()
				IF PS_DLC_Engine_Failure_J_SKIP()
					RETURN TRUE //if we do a j skip, then skip an update
				ENDIF
			ENDIF
		#ENDIF
		
		PS_LESSON_UDPATE()
		
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_THROTTLE_UP)
		//DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_FLY_UNDERCARRIAGE)
		
		SWITCH(PS_DLC_Engine_Failure_State)
		
			CASE PS_DLC_Engine_INIT
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": PS_DLC_Engine_Failure_INIT")
				//updating autopilot to start the player out in the air
				PS_AUTO_PILOT(PS_Main, 30.0, FALSE, vStartRotation.x, vStartRotation.y)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						iPSDialogueCounter = 1
						IF NOT IS_VEHICLE_FUCKED(PS_Main.myVehicle)
							SET_VEHICLE_ENGINE_ON(PS_Main.myVehicle,true,true)
							FREEZE_ENTITY_POSITION(PS_Main.myVehicle, FALSE)		
							SET_ENTITY_HEALTH(PS_Main.myVehicle,20000)
							SET_VEHICLE_PETROL_TANK_HEALTH(PS_Main.myVehicle,1000)
							//SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
							DO_NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, FALSE, FALSE)
							PS_INCREMENT_SUBSTATE()
						ENDIF
					BREAK
					
					CASE PS_SUBSTATE_UPDATE
						PS_INCREMENT_SUBSTATE()
					BREAK
					
					CASE PS_SUBSTATE_EXIT
						PS_DLC_Engine_Failure_State = PS_DLC_Engine_COUNTDOWN
						PS_INCREMENT_SUBSTATE()
					BREAK
				ENDSWITCH
			BREAK

			//On the countdown at start
			CASE PS_DLC_Engine_COUNTDOWN
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": PS_DLC_Engine_Failure_COUNTDOWN")
				//updating autopilot to start the player out in the air
				PS_AUTO_PILOT(PS_Main, 30.0, FALSE, vStartRotation.x, vStartRotation.y)
				
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_INCREMENT_SUBSTATE()
					BREAK
					
					CASE PS_SUBSTATE_UPDATE
						//SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
						//SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
						IF	PS_HUD_UPDATE_COUNTDOWN(FALSE, FALSE)
							PS_INCREMENT_SUBSTATE()
						ENDIF
					BREAK
					
					CASE PS_SUBSTATE_EXIT
						//SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						DO_NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)	
						//SET_PLAYER_INVINCIBLE(PLAYER_ID(),FALSE)
						PS_DLC_Engine_Failure_State = PS_DLC_Engine_CUTOUT
						iDiagEngineTime = GET_GAME_TIMER() + 4000
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
			BREAK
			
			//On the engine cutting out
			CASE PS_DLC_Engine_CUTOUT
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						cprintln(debug_trevor3,"Got here")
						//SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						DO_NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)	
						//SET_PLAYER_INVINCIBLE(PLAYER_ID(),FALSE)
						SET_VEHICLE_CONVERSATIONS_PERSIST(true, true)
						IF NOT IS_VEHICLE_FUCKED(PS_Main.myVehicle)
							SET_VEHICLE_ENGINE_ON(PS_Main.myVehicle,false,true)
						ENDIF
						//PS_PLAY_DISPATCHER_INSTRUCTION_3_LINES("PS_ENGFA", "PS_ENGFA_1", "PS_ENGFA", "PS_ENGFA_2", "PS_ENGFA", "PS_ENGFA_3")
						PS_ACTIVATE_CHECKPOINT_MGR(PS_LESSON_LANDING)
						PS_HUD_RESET_TIMER()
						PS_HUD_SET_GUTTER_ICON_ACTIVE(TRUE)
						
						PS_Engine_Update_Distance()
						
						iTimeSmoke = GET_GAME_TIMER() + 5000
						
						//PS_SET_TIMER_ACTIVE(TRUE)
						PS_SET_HINT_CAM_ACTIVE(TRUE)
						PS_SET_HINT_CAM_COORD(PS_GET_CURRENT_CHECKPOINT())
						PS_INCREMENT_SUBSTATE()
					BREAK
					
					CASE PS_SUBSTATE_UPDATE
					
						//Movement for player
						//PS_Engine_Failure_Boost_Movement()
						
						IF NOT IS_VEHICLE_FUCKED(PS_Main.myVehicle)			
							SET_ENTITY_HEALTH(PS_Main.myVehicle,20000)
							SET_VEHICLE_PETROL_TANK_HEALTH(PS_Main.myVehicle,1000)
							vPlanePos = GET_ENTITY_COORDS(PS_Main.myVehicle)
						ENDIF
						
						CHECK_LANDING_GEAR()
						
						IF NOT GET_IS_VEHICLE_ENGINE_RUNNING(PS_Main.myVehicle) AND (GET_GAME_TIMER() > iTimeSmoke) AND iTimeSmoke != -1
							iTimeSmoke = -1							
							iWarningSound = GET_GAME_TIMER() + 3000
							PLAY_SOUND_FROM_ENTITY(-1,"Engine_fail",PS_Main.myVehicle,"DLC_PILOT_ENGINE_FAILURE_SOUNDS")
						ENDIF						
						
						IF GET_GAME_TIMER() > iWarningSound AND iWarningSound!= -1
							iWarningSound = -1
							iWarningSoundID = 0
							iWarningSoundID = GET_SOUND_ID()
							PLAY_SOUND_FROM_ENTITY(iWarningSoundID,"Warning_Tones",PS_Main.myVehicle,"DLC_PILOT_ENGINE_FAILURE_SOUNDS")
						ENDIF
						
						
						
						//Dialogue
						if iPSDialogueCounter = 1 AND NOT IS_MESSAGE_BEING_DISPLAYED() AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND GET_GAME_TIMER() > iDiagEngineTime
							PS_PLAY_DISPATCHER_INSTRUCTION("PS_EF", "PS_EF_2")
							iPSDialogueCounter++
						ENDIF
						
						if iPSDialogueCounter = 2 AND NOT IS_MESSAGE_BEING_DISPLAYED() AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							PS_PLAY_DISPATCHER_INSTRUCTION("PS_EF", "PS_EF_3")
							iPSDialogueCounter++
						ENDIF
						
						if iPSDialogueCounter = 3 AND NOT IS_MESSAGE_BEING_DISPLAYED() AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							PRINT("PS_ENGINE_FAILURE_1", DEFAULT_GOD_TEXT_TIME, 1) //Land safely on the ~y~runway.~s~
							iPSDialogueCounter++
						ENDIF
						
						if vPlanePos.z < 500
						AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), PS_GET_CURRENT_CHECKPOINT()) < 2000
							if iPSDialogueCounter = 4 AND NOT IS_MESSAGE_BEING_DISPLAYED() AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								PS_PLAY_DISPATCHER_INSTRUCTION("PS_EF", "PS_EF_4")
								iPSDialogueCounter++
							ENDIF
						ENDIF
						
						//Landing Gear comment
						if vPlanePos.z < 500 AND bLandingGear = FALSE
						AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), PS_GET_CURRENT_CHECKPOINT()) < 2000
							if iPSDialogueCounter = 5 AND NOT IS_MESSAGE_BEING_DISPLAYED() AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								//PS_PRINT_FAKE_SUBTITLE("PS_ENGINE_DIAG5", DEFAULT_GOD_TEXT_TIME / 2)
								PRINT_HELP("PS_DLC_EF_LG")
								iPSDialogueCounter++
							ENDIF
						ENDIF
						
						IF PS_DLC_Engine_Failure_Fail_Check()
							PS_DLC_Engine_Failure_State = PS_DLC_Engine_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ENDIF
						
						//if vehicle has touched down
						IF NOT IS_ENTITY_IN_AIR(PS_Main.myVehicle)
							vTouchdownCoord = GET_ENTITY_COORDS(PS_Main.myVehicle)
							PS_DLC_Engine_Failure_State = PS_DLC_Engine_on_runway
							iWarningTime = GET_GAME_TIMER() + 2000
							PS_SET_SUBSTATE(PS_SUBSTATE_UPDATE)
						ENDIF																		
					BREAK
				ENDSWITCH
			BREAK
			
			//Once the vehicle has touched down and on the runway
			CASE PS_DLC_Engine_on_runway
				SWITCH PS_GET_SUBSTATE()
					
					CASE PS_SUBSTATE_UPDATE
					
						IF NOT IS_VEHICLE_FUCKED(PS_Main.myVehicle)			
							cprintln(debug_Trevor3,"Engine health: ",GET_VEHICLE_ENGINE_HEALTH(PS_Main.myVehicle)," veh health: ",GET_ENTITY_HEALTH(PS_Main.myVehicle))
							SET_ENTITY_HEALTH(PS_Main.myVehicle,2000)
							SET_VEHICLE_PETROL_TANK_HEALTH(PS_Main.myVehicle,1000)				
						ENDIF
						
						IF PS_DLC_Engine_Failure_Fail_Check()
							CPRINTLN(DEBUG_TREVOR3, "Fail state returning true here")
							PS_DLC_Engine_Failure_State = PS_DLC_Engine_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELSE
							PS_UPDATE_PLAYER_DATA(vTouchdownCoord)
						
							IF PS_DLC_Engine_Failure_Progress_Check()
								DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_THROTTLE_DOWN)
								CPRINTLN(DEBUG_TREVOR3, "progress acknowledged")
								IF iLandedTimer = -1
									iLandedTimer = GET_GAME_TIMER() + 2000
								ELSE
									if GET_GAME_TIMER() > iLandedTimer
										PS_INCREMENT_SUBSTATE()
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						IF iWarningSoundID != -1
						AND GET_GAME_TIMER() > iWarningTime
							STOP_SOUND(iWarningSoundID)
							iWarningSoundID = -1
							cprintln(debug_trevor3,"landed sound")							
							PLAY_SOUND_FROM_ENTITY(-1,"Landing_Tone",PS_Main.myVehicle,"DLC_PILOT_ENGINE_FAILURE_SOUNDS")
						ENDIF
						
						PS_DLC_Engine_Failure_Handle_Emergency_Vehs()
					BREAK
					
					CASE PS_SUBSTATE_EXIT
						CLEAR_PRINTS()
						CLEAR_HELP()
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_THROTTLE_DOWN)
					
						IF Pilot_School_Data_Check_Mission_Success(PS_Main.myChallengeData, PS_Main.myPlayerData)
							CPRINTLN(DEBUG_TREVOR3, "mission success found")
							PS_DLC_Engine_Failure_State = PS_DLC_Engine_SUCCESS
						ELSE
							CPRINTLN(DEBUG_TREVOR3, "mission fail at 1")
							PS_DLC_Engine_Failure_State = PS_DLC_Engine_FAIL
						ENDIF
						PS_INCREMENT_SUBSTATE()
					BREAK
				ENDSWITCH
			BREAK
		
			//On mission pass		
			CASE PS_DLC_Engine_SUCCESS
			
				PS_DLC_Engine_Failure_Handle_Emergency_Vehs()
			
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_THROTTLE_DOWN)
				CPRINTLN(DEBUG_TREVOR3, ": PS_DLC_Engine_Failure_SUCCESS")
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_INCREMENT_CHECKPOINT()	//increment checkpoint instead of hiding, so that it can flash
						//PS_HIDE_CHECKPOINT(PS_GET_CHECKPOINT_PROGRESS())
						bPlayerDataHasBeenUpdated = FALSE
						PS_UPDATE_RECORDS(PS_GET_CURRENT_CHECKPOINT())
						PS_LESSON_END(TRUE)
						PS_PLAY_LESSON_FINISHED_LINE("PS_EFPass", "PS_EFPass_1", "PS_EFPass_2", "PS_EFPass_3", "")
						DEBUG_PRINT_SCORES(4,"PS_DLC_Engine_SUCCESS : PS_SUBSTATE_ENTER")
						PS_INCREMENT_SUBSTATE()
					BREAK
					
					CASE PS_SUBSTATE_UPDATE
						IF NOT IS_PLAYER_DEAD(player_id())							
						OR GET_MANUAL_RESPAWN_STATE()= MRS_READY_FOR_PLACEMENT
							IF NOT PS_HUD_IS_SCORECARD_ACTIVE()
								PS_INCREMENT_SUBSTATE()
							ENDIF
						ELSE
							PRINTLN("PLAYER IS DEAD SETTING RESPAWN STATE")
							//Set the player to respawn state if dead and doesn't have respawn state set
							IF IS_PLAYER_DEAD(player_id())
							AND GET_MANUAL_RESPAWN_STATE() = MRS_NULL
								SET_MANUAL_RESPAWN_STATE(MRS_RESPAWN_PLAYER_HIDDEN)
							ENDIF
						ENDIF
					BREAK
					
					CASE PS_SUBSTATE_EXIT
						bFinishedChallenge = TRUE
						DEBUG_PRINT_SCORES(4,"PS_DLC_Engine_SUCCESS : PS_SUBSTATE_EXIT")
						PS_INCREMENT_SUBSTATE()
					BREAK
				ENDSWITCH
			BREAK
				
			//On mission fail
			CASE PS_DLC_Engine_FAIL
				
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_THROTTLE_DOWN)
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": PS_DLC_Engine_Failure_FAIL")
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PRINTLN("ENTERED FAIL")
						PS_UPDATE_RECORDS(PS_GET_CURRENT_CHECKPOINT())
						PS_LESSON_END(FALSE)
						PS_PLAY_LESSON_FINISHED_LINE("PS_EFFail", "", "", "", "PS_EFFail_2")
						PS_INCREMENT_SUBSTATE()
					BREAK
					
					CASE PS_SUBSTATE_UPDATE
						PRINTLN("UPDATING FAIL")
						IF NOT PS_HUD_IS_SCORECARD_ACTIVE()
							PS_INCREMENT_SUBSTATE()
						ENDIF
					BREAK
					
					CASE PS_SUBSTATE_EXIT
						PRINTLN("EXITING FAIL")
						bFinishedChallenge = TRUE
						PS_INCREMENT_SUBSTATE()
					BREAK
					
				ENDSWITCH
			BREAK
		ENDSWITCH
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

//Main cleanup procedure called at end
PROC PS_DLC_Engine_Failure_MainCleanup()
//take care of hud
	PS_HUD_SET_GUTTER_ICON_ACTIVE(FALSE)
	PS_HUD_SET_OBJECTIVE_METER_VISIBLE(FALSE)
	PS_SET_TIMER_ACTIVE(FALSE)
	PS_HUD_CLEANUP()
	CLEAR_PRINTS()
	CLEAR_HELP()
	
	SET_DISTANT_CARS_ENABLED(TRUE)
	
	UNBLOCK_SCENARIOS_AND_AMBIENT(sb_PS)
	iTimeSmoke = 0
	bLandingGear = FALSE

	//player first
	PS_SEND_PLAYER_BACK_TO_MENU()
	
	//then checkpoints
	PS_RESET_CHECKPOINT_MGR()
	
	//Reset the clock time back to original
	
	bEngineFailTimeSet = FALSE
	
	//Resetting wanted level
	SET_WANTED_LEVEL_MULTIPLIER(1.0)

	//loaded assets
	REMOVE_VEHICLE_RECORDING(iPS_PreviewVehicleRecordingID, "PilotSchool")
	SET_MODEL_AS_NO_LONGER_NEEDED(VehicleToUse)
	PS_DLC_Engine_Failure_Delete_Vehs()
	
	DEBUG_PRINT_SCORES(4,"PS_DLC_Engine_Failure_MainCleanup()")
//dont cleaning up
	#IF IS_DEBUG_BUILD
		DEBUG_MESSAGE("******************Cleaning up PS_DLC_Engine_Failure.sc******************")
	#ENDIF
ENDPROC

// EOF
//****************************************************************************************************
//****************************************************************************************************

