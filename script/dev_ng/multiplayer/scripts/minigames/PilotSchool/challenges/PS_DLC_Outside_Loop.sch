//****************************************************************************************************
//
//	Author: 		Lukasz Bogaj
//	Date: 			18/10/13
//	Description:	"Outside Loop" Challenge for Pilot School. The player is required to do an outside
//					loop in a plane.
//****************************************************************************************************

USING "minigames_helpers.sch"
USING "Pilot_School_MP_Challenge_Helpers.sch"

//****************************************************************************************************
//****************************************************************************************************
//	Private functions that are used only by Outside Loop
//****************************************************************************************************
//****************************************************************************************************

INT		iPreviewCameraProgress, iUnderBridgeFlightProgress
BOOL	bEnteredArea1, bEnteredArea2, bExitViaArea1, bExitViaArea2, bPassingThroughAreas, bUnderBridgeFlightCompleted
int iLevelCount,iMissedBridgeCount,iFlewTooFarCount

INT		iSubObjectiveCounter					= 1

VECTOR	vFinalBridgeCheckpointPosition = <<-385.4436, -2345.9001, 200>>

PROC  PS_DLC_Outside_Loop_Init_Checkpoints(INT iRouteToDisplay = 1)
	PS_RESET_CHECKPOINT_MGR()
		
	SWITCH iRouteToDisplay
		CASE 1
			PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<-997.0, -3489.0, 250>>)			//start
		BREAK
		CASE 2
			//PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<-709.4450, -2918.9287, 200>>)	//bridge route
			PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<-562.0705, -2605.0181, 200>>)	//previous height was Z=230
			PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<-472.2387, -2469.1917, 200>>)	//lowered to 200 for LAZER jet
			PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, vFinalBridgeCheckpointPosition)	//slight adjustment, move checkpoint to the left
		BREAK
		CASE 3
			PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, vFinalBridgeCheckpointPosition)	//just the final bridge checkpoint in case stunt needs to be retried
			
			//Manually set next checkpoint vector for arrow position
			PS_Main.myCheckpointz[PS_GET_CHECKPOINT_PROGRESS() + 3].position = <<-323.6444, -2260.6987, 281.2447>>
		BREAK
	ENDSWITCH

ENDPROC

PROC PS_DLC_Outside_Loop_Tutorial_Text_Loop()
/*
	SWITCH iTutorialCounter
		CASE 1
			IF ABSF(PS_Main.myObjectiveData.fPlaneTotalOrient) > LOOP_PREPARE_LEVEL_OUT_ANGLE * 2
				//PS_PLAY_DISPATCHER_INSTRUCTION("PS_OLOOP", "PS_OLOOP_5")//"Get ready to level out the plane when you're near the end of the loop."
				//PS_PRINT_FAKE_SUBTITLE("PS_OLOOP_06", 5000)
				iTutorialCounter++
			ENDIF
			BREAK
		CASE 2
			IF ABSF(PS_Main.myObjectiveData.fPlaneTotalOrient) > LOOP_LEVEL_OUT_ANGLE
				PRINT_HELP("PSLOOP_F") //~s~Let go of ~PAD_LSTICK_NONE~ near the end of the loop
				iTutorialCounter++
			ENDIF
			BREAK
	ENDSWITCH*/
ENDPROC

PROC PS_DLC_OUTSIDE_LOOP_PLAY_RETRY_DIALOGUE()
/*
	IF IS_TIMER_STARTED(tRetryInstructionTimer) AND GET_TIMER_IN_SECONDS(tRetryInstructionTimer) > 10.0 AND TIMERA() > 15
		PS_PLAY_DISPATCHER_INSTRUCTION("PS_INTER", "PS_INTER_7")	//"That wasn't quite right. Level out and retry the stunt from the beginning."
		RESTART_TIMER_NOW(tRetryInstructionTimer)
	ELIF IS_TIMER_STARTED(tRetryInstructionTimer)
		IF GET_TIMER_IN_SECONDS(tRetryInstructionTimer) > 10.0
			PS_PLAY_DISPATCHER_INSTRUCTION("PS_INTER", "PS_INTER_7")	//"That wasn't quite right. Level out and retry the stunt from the beginning."
			RESTART_TIMER_NOW(tRetryInstructionTimer)
		ENDIF
	ELSE
		PS_PLAY_DISPATCHER_INSTRUCTION("PS_INTER", "PS_INTER_7")	//"That wasn't quite right. Level out and retry the stunt from the beginning."
		RESTART_TIMER_NOW(tRetryInstructionTimer)
	ENDIF*/
ENDPROC

PROC PS_RESET_BRIDGE_CHECKS()
	bEnteredArea1 				= FALSE
	bEnteredArea2 				= FALSE
	bExitViaArea1 				= FALSE
	bExitViaArea2				= FALSE
	bPassingThroughAreas		= FALSE
	bUnderBridgeFlightCompleted	= FALSE
		
	iUnderBridgeFlightProgress 	= 0
ENDPROC

FUNC BOOL PS_IS_UNDER_BRIDGE_FLIGHT_COMPLETED()
	RETURN bUnderBridgeFlightCompleted
ENDFUNC

PROC PS_UNDER_BRIDGE_FLIGHT_UPDATE()

	VECTOR 	vArea1Min		= <<-655.146729,-2138.093506,-0.339008>>
	VECTOR	vArea1Max		= <<-146.027893,-2493.724365,60.675671>>
	FLOAT	fAreaWidth		= 47.750000
	VECTOR	vArea2Min 		= <<-672.221313,-2162.674561,-0.082912>>
	VECTOR	vArea2Max		= <<-163.555618,-2519.058105,60.722488>>
	
	SWITCH iUnderBridgeFlightProgress
		
		CASE 0	//check for entry of under bridge locates
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF IS_PED_IN_ANY_PLANE(PLAYER_PED_ID())
					IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vArea1Min, vArea1Max, fAreaWidth)
							bEnteredArea1 = TRUE
							iUnderBridgeFlightProgress = 1
						ENDIF
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vArea2Min, vArea2Max, fAreaWidth)
							bEnteredArea2 = TRUE
							iUnderBridgeFlightProgress = 1
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 1	//check for exit of under bridge locates
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF NOT IS_PED_IN_ANY_PLANE(PLAYER_PED_ID())
					PS_RESET_BRIDGE_CHECKS()
				ELSE
					IF NOT IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
						PS_RESET_BRIDGE_CHECKS()
					ELIF NOT IS_ENTITY_IN_AIR(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
						PS_RESET_BRIDGE_CHECKS()
					ENDIF
				ENDIF
			
				IF bEnteredArea1
					IF bPassingThroughAreas
						IF bExitViaArea2
							IF 	NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vArea1Min, vArea1Max, fAreaWidth)
							AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vArea2Min, vArea2Max, fAreaWidth)
								//Passed Through
								PS_RESET_BRIDGE_CHECKS()
								bUnderBridgeFlightCompleted = TRUE
								cprintln(debug_Trevor3,"Under bridge true A")
							ENDIF
						ELSE
							IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vArea1Min, vArea1Max, fAreaWidth)
							AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vArea2Min, vArea2Max, fAreaWidth)
								bExitViaArea2 = TRUE
							ELSE
								IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vArea1Min, vArea1Max, fAreaWidth)
								AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vArea2Min, vArea2Max, fAreaWidth)
									//Never passed through
									PS_RESET_BRIDGE_CHECKS()
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vArea1Min, vArea1Max, fAreaWidth)
						AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vArea2Min, vArea2Max, fAreaWidth)
							bPassingThroughAreas = TRUE
						ELSE
							IF NOT 	IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vArea1Min, vArea1Max, fAreaWidth)
							AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vArea2Min, vArea2Max, fAreaWidth)
								//Never passed through
								PS_RESET_BRIDGE_CHECKS()
							ENDIF
						ENDIF
					ENDIF
					
				ELIF bEnteredArea2
					IF bPassingThroughAreas
						IF bExitViaArea1
							IF 	NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vArea1Min, vArea1Max, fAreaWidth)
							AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vArea2Min, vArea2Max, fAreaWidth)
								//Passed Through
								PS_RESET_BRIDGE_CHECKS()
								bUnderBridgeFlightCompleted = TRUE
								cprintln(debug_Trevor3,"Under bridge true B")
							ENDIF
						ELSE
							IF 	IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vArea1Min, vArea1Max, fAreaWidth)
							AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vArea2Min, vArea2Max, fAreaWidth)
								bExitViaArea1 = TRUE
							ELSE
								IF 	NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vArea1Min, vArea1Max, fAreaWidth)
								AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vArea2Min, vArea2Max, fAreaWidth)
									//Never passed through
									PS_RESET_BRIDGE_CHECKS()
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF 	IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vArea1Min, vArea1Max, fAreaWidth)
						AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vArea2Min, vArea2Max, fAreaWidth)
							bPassingThroughAreas = TRUE
						ELSE
							IF 	NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vArea1Min, vArea1Max, fAreaWidth)
							AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vArea2Min, vArea2Max, fAreaWidth)
								//Never passed through
								PS_RESET_BRIDGE_CHECKS()
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
	
	ENDSWITCH

ENDPROC

FUNC BOOL PS_IS_PLANE_PERFORMING_OUTSIDE_LOOP()
	
	RETURN	ABSF(PS_Main.myObjectiveData.fPlaneTotalOrient)/PS_LOOPING_ROLL_GOAL_ANGLE > 0.025 AND ABSF(PS_Main.myObjectiveData.fPlaneTotalOrient)/PS_LOOPING_ROLL_GOAL_ANGLE < 0.95

ENDFUNC

/// PURPOSE:
///    Determines what fail conditions we need to check dependent on the 
/// PARAMS:
///    PS_Main - 
FUNC BOOL PS_DLC_Outside_Loop_Fail_Check()
	IF PS_GET_SUBSTATE() = PS_SUBSTATE_UPDATE
		SWITCH(PS_DLC_Outside_Loop_State)
			CASE PS_DLC_OUTSIDE_LOOP_INIT
				//No fail check to do
				RETURN FALSE
				
			CASE PS_DLC_OUTSIDE_LOOP_COUNTDOWN
				//No fail check to do
				RETURN FALSE
				
			CASE PS_DLC_OUTSIDE_LOOP_TAKEOFF
				//Do runway check and default checks
				IF PS_UPDATE_FAIL_CHECKS(PS_Main, FALSE, TRUE)
					RETURN TRUE
				ENDIF
				BREAK
			
			DEFAULT
				//Do default checks
				IF PS_UPDATE_FAIL_CHECKS(PS_Main)
					RETURN TRUE
				ENDIF
				BREAK
		ENDSWITCH
	ENDIF
	//If we get here, we havent failed yet
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks for objective goals completed based on the stage/state we are in of the challenge progression
///    Also updates ChallengeState when we complete an objective 
/// PARAMS:
///    PS_Main - 
FUNC BOOL PS_DLC_Outside_Loop_Progress_Check()
	
	SWITCH(PS_DLC_Outside_Loop_State)
			CASE PS_DLC_OUTSIDE_LOOP_INIT
				RETURN FALSE
				BREAK

			CASE PS_DLC_OUTSIDE_LOOP_COUNTDOWN
				RETURN FALSE
				BREAK
				
		//Give player control and the first instruction to take off
		//Check if player is staying on the runway and not driving offroad
			CASE PS_DLC_OUTSIDE_LOOP_TAKEOFF
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_UPDATE
						//See if the player is close to the checkpoint yet
						IF PS_GET_CHECKPOINT_PROGRESS() > 1 OR PS_IS_LAST_CHECKPOINT_CLEARED()
							RETURN TRUE
						ENDIF
						BREAK
				ENDSWITCH
				BREAK
				
			CASE PS_DLC_OUTSIDE_LOOP_TAKEOFF_REACT
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					RETURN TRUE
				ENDIF
				BREAK
			
			CASE PS_DLC_OUTSIDE_LOOP_LOOP_ONE_PREP
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					RETURN TRUE
				ENDIF
				BREAK
				
			CASE PS_DLC_OUTSIDE_LOOP_LOOP_ONE_START
				IF PS_IS_PLANE_CURRENTLY_LEVEL()
					RETURN TRUE
				ENDIF
				BREAK
				
			CASE PS_DLC_OUTSIDE_LOOP_LOOP_ONE_PERFORM
				IF PS_GET_OBJECTIVE_CURRENT_ORIENT_COUNT() > 0
					RETURN TRUE
				ENDIF
				BREAK
				
			CASE PS_DLC_OUTSIDE_LOOP_LOOP_ONE_REACT
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					RETURN TRUE
				ENDIF
				BREAK
			
			CASE PS_DLC_OUTSIDE_LOOP_OBSTACLE_COURSE
				IF PS_IS_LAST_CHECKPOINT_CLEARED()
					RETURN TRUE
				ENDIF
				BREAK
				
			CASE PS_DLC_OUTSIDE_LOOP_OBSTACLE_COURSE_RETRY
				IF PS_IS_LAST_CHECKPOINT_CLEARED()
					RETURN TRUE
				ENDIF
				BREAK
			
			CASE PS_DLC_OUTSIDE_LOOP_LOOP_TWO_PREP
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					RETURN TRUE
				ENDIF
				BREAK
				
			CASE PS_DLC_OUTSIDE_LOOP_LOOP_TWO_START
				IF PS_IS_PLANE_CURRENTLY_LEVEL()
					RETURN TRUE
				ENDIF
				//RETURN TRUE
				BREAK
			
			CASE PS_DLC_OUTSIDE_LOOP_LOOP_TWO_PERFORM
			
				IF PS_IS_PLANE_PERFORMING_OUTSIDE_LOOP()				
					PS_UNDER_BRIDGE_FLIGHT_UPDATE()
				ENDIF
			
	
				CPRINTLN(debug_trevor3,"A")
				IF PS_GET_OBJECTIVE_CURRENT_ORIENT_COUNT() > 0
					CPRINTLN(debug_trevor3,"B")
					IF PS_IS_UNDER_BRIDGE_FLIGHT_COMPLETED()
						CPRINTLN(debug_trevor3,"C")
						RETURN TRUE
					ELSE
						//play dispatcher line about not looping under a bridge
						CPRINTLN(debug_trevor3,"D")
						PS_RESET_BRIDGE_CHECKS()
					ENDIF
				ENDIF
				BREAK
				
			CASE PS_DLC_OUTSIDE_LOOP_LOOP_TWO_REACT
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					RETURN TRUE
				ENDIF
				BREAK
				
		ENDSWITCH
	RETURN FALSE
ENDFUNC

PROC PS_DLC_OUTSIDE_LOOP_FORCE_FAIL
	ePSFailReason = PS_FAIL_DEBUG
	PS_DLC_Outside_Loop_State = PS_DLC_OUTSIDE_LOOP_FAIL
	PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)

ENDPROC

PROC PS_DLC_OUTSIDE_LOOP_FORCE_PASS
	PS_DLC_Outside_Loop_State = PS_DLC_OUTSIDE_LOOP_SUCCESS
	PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
ENDPROC

//****************************************************************************************************
//****************************************************************************************************
//	Public functions! (used by Pilot_School.sc to run the challenges)
//****************************************************************************************************
//****************************************************************************************************

PROC PS_DLC_OUTSIDE_LOOP_Initialise()
	SWITCH g_PS_initStage
		CASE 0
			float startHeading
			startHeading = GET_HEADING_BETWEEN_VECTORS(<<-1786.5457, -5210.8926, 220>>,<<-1220.0, -3975.0, 250>>)
			iPS_PreviewVehicleRecordingID = iPS_PEVIEW_VEH_REC_ID_DLC_OUTSIDE_LOOP
			vStartPosition								= <<-1786.5457, -5210.8926, 220>> //<<-1630.0, -4875, 185>> //<<-1220.0, -3975.0, 250>>
			vStartRotation 								= <<0.000, 0.000, startHeading>>
			fStartHeading 								= startHeading
			iPSDialogueCounter							= 1
			iPSHelpTextCounter							= 1
			iPSObjectiveTextCounter						= 1
			iPreviewCameraProgress						= 0

			iLevelCount = 0
			iMissedBridgeCount = 0
			iFlewTooFarCount = 0

			

			
			//Player vehicle model
			VehicleToUse = LAZER
			PS_SET_GROUND_OFFSET(1.5)
			CANCEL_TIMER(tRetryInstructionTimer)
			//load assets
			REQUEST_VEHICLE_RECORDING(iPS_PreviewVehicleRecordingID, sPreviewRecordingName)
			IF  HAS_VEHICLE_RECORDING_BEEN_LOADED(iPS_PreviewVehicleRecordingID, sPreviewRecordingName)
			AND PS_SETUP_CHALLENGE()
				PS_DLC_Outside_Loop_Init_Checkpoints()

				//init vars
				PS_DLC_Outside_Loop_State = PS_DLC_OUTSIDE_LOOP_INIT
				
				fPreviewVehicleSkipTime = 40000
				fPreviewTime = 24
				
				vPilotSchoolSceneCoords = vStartPosition
				IF HAS_VEHICLE_RECORDING_BEEN_LOADED(iPS_PreviewVehicleRecordingID, sPreviewRecordingName)
					vPilotSchoolPreviewCoords = GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(iPS_PreviewVehicleRecordingID, fPreviewVehicleSkipTime, sPreviewRecordingName)
					vPilotSchoolPreviewRot = GET_ROTATION_OF_VEHICLE_RECORDING_AT_TIME(iPS_PreviewVehicleRecordingID, fPreviewVehicleSkipTime, sPreviewRecordingName)
				ENDIF
				g_PS_initStage++
			ELSE
				EXIT
			ENDIF
		BREAK
		CASE 1
	
	
	
	
			//Spawn player vehicle			
			IF PS_CREATE_VEHICLE(TRUE)
				bwaitingOnSomething = FALSE
				//multiplayer respawn location setup
				
//				SET_WEATHER_FOR_FMMC_MISSION(1) //sunny

				NETWORK_OVERRIDE_CLOCK_TIME(GET_HOUR_FOR_PS_CHALLENGE(),0,0)


				
				PS_RESET_BRIDGE_CHECKS()
				
				PS_STORE_BRIDGES()
				PS_SET_ALL_BRIDGES_AS_FLOWN_UNDER()
				g_PS_initStage = 0
				#IF IS_DEBUG_BUILD
					DEBUG_MESSAGE("******************Setting up PS_DLC_Outside_Loop.sc******************")
				#ENDIF
			ELSE
				EXIT
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL PS_DLC_OUTSIDE_LOOP_Preview()
	IF PS_PREVIEW_UPDATE_CUTSCENE()
		SWITCH(PS_PreviewState)
			CASE PS_PREVIEW_SETUP
				PS_PREVIEW_SETUP_PLAYER_FOR_CUTSCENE()
				IF NOT IS_VEHICLE_FUCKED(PS_Main.myVehicle)
					START_PLAYBACK_RECORDED_VEHICLE(PS_Main.myVehicle, iPS_PreviewVehicleRecordingID, "PilotSchool") //change name to pilotschooldlc
					SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(PS_Main.myVehicle, fPreviewVehicleSkipTime)
					SET_PLAYBACK_SPEED(PS_Main.myVehicle, 1.20)
				ENDIF
				iPreviewCameraProgress 		= 0
			
				iPreviewCheckpointIdx 		= PS_GET_CLOSEST_PREVIEW_CHECKPOINT(iPS_PreviewVehicleRecordingID, PS_Main.myCheckpointz, fPreviewVehicleSkipTime)
				PS_ACTIVATE_CHECKPOINT_MGR(PS_LESSON_STUNT, iPreviewCheckpointIdx, TRUE)
				PS_PREVIEW_DIALOGUE_PLAY("PS_OLPREV", "PS_OLPREV_1", "PS_OLPREV_2", "PS_OLPREV_3", "PS_OLPREV_4", "PS_OLPREV_5","PS_OLPREV_6")
				
					
			BREAK
			CASE PS_PREVIEW_PLAYING
			
			
				IF IS_VEHICLE_DRIVEABLE(PS_Main.myVehicle)
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(PS_Main.myVehicle)
					
					
						FLOAT fRecordingPlaybackTime
						
						fRecordingPlaybackTime = GET_TIME_POSITION_IN_RECORDING(PS_Main.myVehicle)
						
						SWITCH iPreviewCameraProgress
							CASE 0
								IF fRecordingPlaybackTime >= 55500
								//	PS_PRINT_FAKE_SUBTITLE("PS_OLOOP_P02", 6000)
									iPreviewCameraProgress++
								ENDIF
							BREAK
							CASE 1
							
								DESTROY_ALL_CAMS()
								RENDER_SCRIPT_CAMS(FALSE, FALSE)
								
								PS_Main.previewCam1 = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
								
								SET_CAM_COORD(PS_Main.previewCam1, << -363.3, -2525.0, 11.6 >>)
								SET_CAM_FOV(PS_Main.previewCam1, 30.0)
								POINT_CAM_AT_ENTITY(PS_Main.previewCam1, PS_Main.myVehicle, << 0.0, 2.0, 0.0 >>)
								SHAKE_CAM(PS_Main.previewCam1, "HAND_SHAKE", 1.0)
								
								
								RENDER_SCRIPT_CAMS(TRUE, FALSE)
								iPreviewCameraProgress++
							BREAK
							CASE 2
								IF fRecordingPlaybackTime >= 66500
									DESTROY_ALL_CAMS()
									RENDER_SCRIPT_CAMS(FALSE, FALSE)
									iPreviewCameraProgress++
								ENDIF
							BREAK
						ENDSWITCH
					ENDIF
				ENDIF				
				BREAK
			CASE PS_PREVIEW_CLEANUP		
				PS_PREVIEW_CLEANUP_CUTSCENE()
				BREAK
		ENDSWITCH
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

FUNC BOOL PS_DLC_OUTSIDE_LOOP_MainSetup()
	PS_DLC_Outside_Loop_Init_Checkpoints()
	SET_MINIMAP_COMPONENT(MINIMAP_COMPONENT_RUNWAY_1, FALSE)
	NETWORK_OVERRIDE_CLOCK_TIME(GET_HOUR_FOR_PS_CHALLENGE(),0,0)
	IF NOT IS_VEHICLE_FUCKED(PS_Main.myVehicle)
		SET_ENTITY_ROTATION(PS_Main.myVehicle, vStartRotation)
		PS_AUTO_PILOT(PS_Main, 60.0, FALSE, vStartRotation.x, vStartRotation.y)
	ENDIF
	
	//Adding ground offset for landing check
	GroundOffset = 2.0

	NEW_LOAD_SCENE_START(<<-1434.3361, -4490.2021, 185.70>>, <<-0.90, -0.44, -0.01>>, 5000.0)
	RETURN TRUE
ENDFUNC

#IF IS_DEBUG_BUILD
	FUNC BOOL PS_DLC_OUTSIDE_LOOP_J_SKIP()
		PS_DLC_OUTSIDE_LOOP_CHALLENGE nextState = PS_DLC_Outside_Loop_State
		IF PS_DLC_Outside_Loop_State > PS_DLC_OUTSIDE_LOOP_TAKEOFF
			IF PS_DLC_Outside_Loop_State < PS_DLC_OUTSIDE_LOOP_LOOP_ONE_PREP
				nextState = PS_DLC_OUTSIDE_LOOP_LOOP_ONE_PREP			
			ELIF PS_DLC_Outside_Loop_State < PS_DLC_OUTSIDE_LOOP_LOOP_TWO_PREP
				nextState = PS_DLC_OUTSIDE_LOOP_LOOP_TWO_PREP
			ENDIF
			IF nextState != PS_DLC_Outside_Loop_State AND NOT IS_ENTITY_DEAD(PS_Main.myVehicle)
				//send plane to the takeoff checkpoint
				SET_ENTITY_COORDS(PS_Main.myVehicle, PS_GET_CHECKPOINT_FROM_ID(1))
				SET_VEHICLE_FORWARD_SPEED(PS_Main.myVehicle, 60.0)
				PS_DLC_Outside_Loop_State = nextState
				PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
				RETURN TRUE
			ENDIF	
		ENDIF
		RETURN FALSE
	ENDFUNC
#ENDIF	

enum enumBrief
	BRIEF_NONE,
	BRIEF_START_DIALOGUE,
	BRIEF_START_LOOP_ONE,
	BRIEF_COMPLETED_FIRST_LOOP,
	BRIEF_INSTRUCTOR_REACTION,
	BRIEF_PREP_BRIDGE_LOOP,
	BRIEF_START_BRIDGE_LOOP,
	BRIEF_RETRY_BRIDGE_LOOP,
	BRIEF_GET_TO_BRIDGES,
	BRIEF_MISSED_BRIDGE,
	BRIEF_FLEW_TOO_FAR,
	BRIEF_RETURN_TO_BRIDGE
endenum

enumBrief currentBrief = BRIEF_NONE

PROC PS_TRIGGER_BRIEF(enumBrief thisBrief)
	cprintln(debug_trevor3,"Trigger ",enum_to_int(thisBrief))
	currentBrief = thisBrief
ENDPROC



PROC PS_PLAY_BRIEF()
	SWITCH currentBrief
		CASE BRIEF_START_DIALOGUE
			PS_PLAY_DISPATCHER_INSTRUCTION_CONVERSATION("PS_OLb")
			currentBrief = BRIEF_NONE
		BREAK
		CASE BRIEF_START_LOOP_ONE
			IF PS_IS_PLANE_CURRENTLY_LEVEL()
				IF NOT IS_THIS_PRINT_BEING_DISPLAYED("PSBARREL_E")
					PS_PLAY_DISPATCHER_INSTRUCTION("PS_OLa","PS_OLa_1")
					PRINT_HELP("PS_OLOOP_01")
					currentBrief = BRIEF_NONE
				ENDIF
			ENDIF
		BREAK
		CASE BRIEF_COMPLETED_FIRST_LOOP
			PS_PLAY_DISPATCHER_INSTRUCTION("PS_OLa", "PS_OLa_2")
			cprintln(debug_trevor3,"BRIEF_COMPLETED_FIRST_LOOP")
			currentBrief = BRIEF_INSTRUCTOR_REACTION
		BREAK
		CASE BRIEF_INSTRUCTOR_REACTION
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				cprintln(debug_trevor3,"BRIEF_INSTRUCTOR_REACTION")
				PS_PLAY_DISPATCHER_INSTRUCTION_2_LINES("PS_OLc", "PS_OLc_1","PS_OLc","PS_OLc_2")						
				currentBrief = BRIEF_GET_TO_BRIDGES
			ENDIF
		BREAK
		CASE BRIEF_GET_TO_BRIDGES
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()				
				cprintln(debug_trevor3,"BRIEF_GET_TO_BRIDGES")
				PRINT_NOW("PS_OLOOP_09",6000,1)
				//currentBrief = BRIEF_PREP_BRIDGE_LOOP
				currentBrief = BRIEF_NONE
			ENDIF
		BREAK
		
		CASE BRIEF_PREP_BRIDGE_LOOP		
			IF NOT IS_MESSAGE_BEING_DISPLAYED()// PS_IS_CHECKPOINT_MGR_ACTIVE() AND PS_GET_CHECKPOINT_PROGRESS() > 1
				PS_PLAY_DISPATCHER_INSTRUCTION_2_LINES("PS_OLc", "PS_OLc_3","PS_OLc", "PS_OLc_4")
				currentBrief = BRIEF_NONE
			ENDIF
		BREAK
		
		CASE BRIEF_START_BRIDGE_LOOP
			PS_PLAY_DISPATCHER_INSTRUCTION("PS_OLa", "PS_OLa_4")
			currentBrief = BRIEF_NONE
		BREAK
		
		CASE BRIEF_RETRY_BRIDGE_LOOP
			IF iLevelCount = 0
				PS_PLAY_DISPATCHER_INSTRUCTION("PS_FLbad", "PS_FLbad_3")		//play generic fail line from another lesson			
		//	ELSE
		//		PS_PLAY_DISPATCHER_INSTRUCTION("PS_OLfail", "PS_OLfail_8")
			ENDIF
			iLevelCount++
			currentBrief = BRIEF_RETURN_TO_BRIDGE//BRIEF_NONE					//direct the brief to display god text about returning to the bridge
		BREAK
		
		CASE BRIEF_MISSED_BRIDGE
			IF iMissedBridgeCount = 0
				PS_PLAY_DISPATCHER_INSTRUCTION("PS_OLfail", "PS_OLfail_3")				
	//		ELSE
	//			PS_PLAY_DISPATCHER_INSTRUCTION("PS_OLfail", "PS_OLfail_6")
			ENDIF
			iMissedBridgeCount++
			currentBrief = BRIEF_RETURN_TO_BRIDGE
		BREAK
		
		CASE BRIEF_FLEW_TOO_FAR
			IF iFlewTooFarCount = 0
				PS_PLAY_DISPATCHER_INSTRUCTION("PS_OLfail", "PS_OLfail_4")				
			//ELSE
				//PS_PLAY_DISPATCHER_INSTRUCTION("PS_OLfail", "PS_OLfail_7")
			ENDIF
			iFlewTooFarCount++
			currentBrief = BRIEF_RETURN_TO_BRIDGE
		BREAK
		
		CASE BRIEF_RETURN_TO_BRIDGE
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				PRINT_NOW("PS_OLOOP_02",DEFAULT_GOD_TEXT_TIME,1)
				currentBrief = BRIEF_NONE
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC


FUNC BOOL PS_DLC_OUTSIDE_LOOP_MainUpdate()

	IF TempDebugInput() AND NOT bFinishedChallenge
		
		#IF IS_DEBUG_BUILD
			IF PS_CHECK_FOR_STUNT_LESSON_J_SKIP()
				IF PS_DLC_OUTSIDE_LOOP_J_SKIP()
					RETURN TRUE //if we do a j skip, then skip an update
				ENDIF
			ENDIF
		#ENDIF
		
		PS_LESSON_UDPATE()
		PS_PLAY_BRIEF()
		
		IF PS_DLC_Outside_Loop_State = PS_DLC_OUTSIDE_LOOP_SUCCESS
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_EXIT)
		ENDIF
		
		CPRINTLN(debug_Trevor3,"State: ",PS_DLC_Outside_Loop_State)
		SWITCH(PS_DLC_Outside_Loop_State)
		
			CASE PS_DLC_OUTSIDE_LOOP_INIT
				PS_AUTO_PILOT(PS_Main, 60.0, FALSE, vStartRotation.x, vStartRotation.y)
				
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						IF IS_VEHICLE_DRIVEABLE(PS_Main.myVehicle)
							FREEZE_ENTITY_POSITION(PS_Main.myVehicle, FALSE)
							MODIFY_VEHICLE_TOP_SPEED(PS_Main.myVehicle, -25)
							CONTROL_LANDING_GEAR(PS_Main.myVehicle, LGC_RETRACT_INSTANT)
							SET_VEHICLE_ENGINE_ON(PS_Main.myVehicle, TRUE, TRUE)
						ENDIF						
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_EXIT
						PS_DLC_Outside_Loop_State = PS_DLC_OUTSIDE_LOOP_COUNTDOWN
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				
			BREAK

			CASE PS_DLC_OUTSIDE_LOOP_COUNTDOWN
			
				PS_AUTO_PILOT(PS_Main, 60.0, FALSE, vStartRotation.x, vStartRotation.y)
			
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF	PS_HUD_UPDATE_COUNTDOWN(TRUE,FALSE)
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						PS_RESTORE_BRIDGES()
						PS_TRIGGER_MUSIC(PS_MUSIC_LOOP_START)
						PS_DLC_Outside_Loop_State = PS_DLC_OUTSIDE_LOOP_TAKEOFF
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				//SET_PLAYER_CONTROL(player_id(),FALSE)
				DO_NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
			BREAK
				
			CASE PS_DLC_OUTSIDE_LOOP_TAKEOFF

				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						//SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						DO_NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)	
						//SET_PLAYER_INVINCIBLE(PLAYER_ID(),FALSE)
						PS_ACTIVATE_CHECKPOINT_MGR(PS_LESSON_STUNT)
						PS_TRIGGER_BRIEF(BRIEF_START_DIALOGUE)					
						PS_HUD_RESET_TIMER()
						PS_HUD_SET_GUTTER_ICON_ACTIVE(TRUE)
						PS_SET_TIMER_ACTIVE(TRUE)
						PS_SET_HINT_CAM_ACTIVE(TRUE)
						PS_SET_HINT_CAM_COORD(PS_GET_CURRENT_CHECKPOINT())
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF PS_DLC_Outside_Loop_Fail_Check()
							PS_DLC_Outside_Loop_State = PS_DLC_OUTSIDE_LOOP_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELIF PS_DLC_Outside_Loop_Progress_Check()
							PS_INCREMENT_SUBSTATE()
						ELSE							
							IF iPSObjectiveTextCounter = 1 AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								PRINT("PS_OLOOP_P00", DEFAULT_GOD_TEXT_TIME, 1)	//"~s~Head for the first ~y~checkpoint~s~."
							ENDIF
						ENDIF
						
						BREAK
					CASE PS_SUBSTATE_EXIT
						
						PS_RESET_CHECKPOINT_MGR(FALSE)
						PS_HUD_SET_GUTTER_ICON_ACTIVE(FALSE)
						PS_SET_HINT_CAM_ACTIVE(FALSE)
						PS_KILL_HINT_CAM()
						PS_DLC_Outside_Loop_State = PS_DLC_OUTSIDE_LOOP_TAKEOFF_REACT
						iInstructionCounter = 1
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
				
			CASE PS_DLC_OUTSIDE_LOOP_TAKEOFF_REACT
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_INCREMENT_SUBSTATE()
						CLEAR_HELP()
						CLEAR_PRINTS()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF PS_DLC_Outside_Loop_Fail_Check()
							PS_DLC_Outside_Loop_State = PS_DLC_OUTSIDE_LOOP_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELIF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() 
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						
						PS_DLC_Outside_Loop_State = PS_DLC_OUTSIDE_LOOP_LOOP_ONE_PREP
						
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
			
			CASE PS_DLC_OUTSIDE_LOOP_LOOP_ONE_PREP
						
			
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						//PS_PLAY_DISPATCHER_INSTRUCTION_2_LINES("PS_OLOOP", "PS_OLOOP_3", "PS_OLOOP", "PS_OLOOP_4")
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF PS_DLC_Outside_Loop_Fail_Check()
							PS_DLC_Outside_Loop_State = PS_DLC_OUTSIDE_LOOP_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELIF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						PS_TRIGGER_BRIEF(BRIEF_START_LOOP_ONE)	
						
						PS_DLC_Outside_Loop_State = PS_DLC_OUTSIDE_LOOP_LOOP_ONE_START
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
				
			CASE PS_DLC_OUTSIDE_LOOP_LOOP_ONE_START
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF PS_DLC_Outside_Loop_Fail_Check()
							PS_DLC_Outside_Loop_State = PS_DLC_OUTSIDE_LOOP_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELIF PS_DLC_Outside_Loop_Progress_Check()
							PS_INCREMENT_SUBSTATE()
						ENDIF
						IF NOT PS_IS_PLANE_CURRENTLY_LEVEL()
							PRINT_NOW("PSBARREL_E", DEFAULT_GOD_TEXT_TIME, 1) //level out
						ELSE
							IF IS_THIS_PRINT_BEING_DISPLAYED("PSBARREL_E")
								CLEAR_PRINTS()
							ENDIF
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
					//	CLEAR_PRINTS()
					
						PS_DLC_Outside_Loop_State = PS_DLC_OUTSIDE_LOOP_LOOP_ONE_PERFORM
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
				
			CASE PS_DLC_OUTSIDE_LOOP_LOOP_ONE_PERFORM
			
				
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						//PS_PLAY_DISPATCHER_INSTRUCTION("PS_OLOOPOBJ", "PS_OLOOPOBJ_1") //"Perform an outside loop."	
						//QUEUE_OBJHELPALOGUE_HELP("PS_DLC_H_OL", -1, 1.5)
						//PLAY_OBJHELPALOGUE()
						PS_RESET_OBJECTIVE_DATA()
						SETTIMERA(0)
						PS_SET_OUTSIDE_LOOP_METER_ACTIVE(TRUE)
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF PS_DLC_Outside_Loop_Fail_Check()
							PS_DLC_Outside_Loop_State = PS_DLC_OUTSIDE_LOOP_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELIF PS_DLC_Outside_Loop_Progress_Check()
							PS_TRIGGER_BRIEF(BRIEF_COMPLETED_FIRST_LOOP)
							
							PS_INCREMENT_SUBSTATE()
						ELIF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND ABSF(PS_Main.myObjectiveData.fPlaneTotalOrient) > LOOP_PREPARE_LEVEL_OUT_ANGLE * 2 AND iInstructionCounter = 1
							//PS_PLAY_DISPATCHER_INSTRUCTION("PS_OLOOP", "PS_OLOOP_5")	//"Get ready to level out the plane when you get near the end of the loop."
						
							iInstructionCounter++
						ELSE
							IF PS_GET_OBJECTIVE_RETRY_FLAG() AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							//	IF (ABSF(fPlaneLastOrient) / PS_UI_ObjectiveMeter.fObjBarDemoninator) >= 0.33
							//	OR PS_Main.myObjectiveData.fPlaneTotalOrient < 10.0 									
								
									
								
									PS_SET_OBJECTIVE_RETRY_FLAG(FALSE)
									PS_Main.myObjectiveData.fPlaneTotalOrient = 0
									PS_DLC_OUTSIDE_LOOP_PLAY_RETRY_DIALOGUE()
									SETTIMERA(0)
							//	ENDIF
							ENDIF
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						CLEAR_HELP()
						CLEAR_PRINTS()
						PS_RESET_OBJECTIVE_DATA()
						PS_SET_OUTSIDE_LOOP_METER_ACTIVE(FALSE)
						
						iInstructionCounter = 1
						PS_DLC_Outside_Loop_State = PS_DLC_OUTSIDE_LOOP_LOOP_ONE_REACT
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
				
			CASE PS_DLC_OUTSIDE_LOOP_LOOP_ONE_REACT
			
			
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						//PS_PLAY_DISPATCHER_INSTRUCTION("PS_OLOOP", "PS_OLOOP_6") //"That was good. Looks like you got the hang of it."
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF PS_DLC_Outside_Loop_Fail_Check()
							PS_DLC_Outside_Loop_State = PS_DLC_OUTSIDE_LOOP_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELSE
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						CLEAR_PRINTS()
						PS_DLC_Outside_Loop_State = PS_DLC_OUTSIDE_LOOP_OBSTACLE_COURSE
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
				
			CASE PS_DLC_OUTSIDE_LOOP_OBSTACLE_COURSE
			
			
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						
						PS_DLC_Outside_Loop_Init_Checkpoints(2)
						
						PS_ACTIVATE_CHECKPOINT_MGR(PS_LESSON_OBSTACLE_PLANE)
						PS_SET_HINT_CAM_ACTIVE(TRUE)
						PS_SET_HINT_CAM_COORD(PS_GET_CURRENT_CHECKPOINT())
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF PS_DLC_Outside_Loop_Fail_Check()
							PS_DLC_Outside_Loop_State = PS_DLC_OUTSIDE_LOOP_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELIF PS_DLC_Outside_Loop_Progress_Check()
							PS_DLC_Outside_Loop_State = PS_DLC_OUTSIDE_LOOP_LOOP_TWO_PERFORM
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELSE
							//Clear 
							IF PS_GET_CHECKPOINT_PROGRESS() = 2
							AND iSubObjectiveCounter = 1
								CLEAR_PRINTS()
								KILL_ANY_CONVERSATION()
								iSubObjectiveCounter++
							ENDIF
						
							IF PS_IS_CHECKPOINT_MGR_ACTIVE() AND PS_GET_CHECKPOINT_PROGRESS() >= 2 AND iSubObjectiveCounter > 1
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									//PS_PLAY_DISPATCHER_INSTRUCTION("PS_OLOOP", "PS_OLOOP_10")
									iInstructionCounter++
								ENDIF
							ENDIF
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT				
						PS_DLC_Outside_Loop_State = PS_DLC_OUTSIDE_LOOP_LOOP_TWO_PREP
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
				
			CASE PS_DLC_OUTSIDE_LOOP_OBSTACLE_COURSE_RETRY
			
			
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						//PS_PLAY_DISPATCHER_INSTRUCTION_3_LINES("PS_OLOOP", "PS_OLOOP_7", "PS_OLOOP", "PS_OLOOP_8", "PS_OLOOP", "PS_OLOOP_9")
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() //wait for pilot to instruct player to retry
						AND currentBrief = BRIEF_NONE
							PS_DLC_Outside_Loop_Init_Checkpoints(3)
							PS_ACTIVATE_CHECKPOINT_MGR(PS_LESSON_STUNT)
							PS_SET_HINT_CAM_ACTIVE(TRUE)
							PS_SET_HINT_CAM_COORD(PS_GET_CURRENT_CHECKPOINT())
							PS_INCREMENT_SUBSTATE()
						ENDIF
					BREAK
					CASE PS_SUBSTATE_UPDATE
						IF PS_DLC_Outside_Loop_Fail_Check()
							PS_DLC_Outside_Loop_State = PS_DLC_OUTSIDE_LOOP_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELIF PS_DLC_Outside_Loop_Progress_Check()							
							PS_DLC_Outside_Loop_State = PS_DLC_OUTSIDE_LOOP_LOOP_TWO_PERFORM
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELSE
							
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT				
						PS_SET_HINT_CAM_ACTIVE(FALSE)
						PS_DLC_Outside_Loop_State = PS_DLC_OUTSIDE_LOOP_LOOP_TWO_PERFORM
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
			
				BREAK
			
			CASE PS_DLC_OUTSIDE_LOOP_LOOP_TWO_PREP
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						//PS_PLAY_DISPATCHER_INSTRUCTION("PS_OLOOP", "PS_OLOOP_10")
						cprintln(debug_trevor3,"NOW")
						PS_RESET_OBJECTIVE_DATA()
						PS_SET_OUTSIDE_LOOP_METER_ACTIVE(TRUE)
						PS_HUD_SET_OBJECTIVE_METER_VISIBLE(TRUE)
						PS_SET_HINT_CAM_ACTIVE(FALSE)
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF PS_DLC_Outside_Loop_Fail_Check()
							PS_DLC_Outside_Loop_State = PS_DLC_OUTSIDE_LOOP_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELIF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						KILL_ANY_CONVERSATION()
						CLEAR_PRINTS()
						PS_DLC_Outside_Loop_State = PS_DLC_OUTSIDE_LOOP_LOOP_TWO_START
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
			
			CASE PS_DLC_OUTSIDE_LOOP_LOOP_TWO_START
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_RESET_OBJECTIVE_DATA()
						PS_SET_OUTSIDE_LOOP_METER_ACTIVE(TRUE)
						PS_HUD_SET_OBJECTIVE_METER_VISIBLE(TRUE)
						PS_SET_HINT_CAM_ACTIVE(FALSE)
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE		
						IF PS_DLC_Outside_Loop_Fail_Check()
							PS_DLC_Outside_Loop_State = PS_DLC_OUTSIDE_LOOP_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELIF PS_DLC_Outside_Loop_Progress_Check()
							PS_TRIGGER_BRIEF(BRIEF_START_BRIDGE_LOOP)
							PS_INCREMENT_SUBSTATE()						
						ELSE
							IF NOT PS_IS_PLANE_CURRENTLY_LEVEL()			
								iLevelCount = 0

								IF iLevelCount < 2
									PS_TRIGGER_BRIEF(BRIEF_RETURN_TO_BRIDGE)
									PRINT_NOW("PSBARREL_E", DEFAULT_GOD_TEXT_TIME, 1)
								
									PS_DLC_Outside_Loop_State = PS_DLC_OUTSIDE_LOOP_OBSTACLE_COURSE_RETRY
									PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
								ELSE
									ePSFailReason = PS_FAIL_DIDNT_FOLLOW_INSTRUCTIONS
									PS_DLC_Outside_Loop_State = PS_DLC_OUTSIDE_LOOP_FAIL
									PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
								ENDIF
							ENDIF
						ENDIF
						
						BREAK
					CASE PS_SUBSTATE_EXIT
						CLEAR_PRINTS()
						PS_RESET_OBJECTIVE_DATA()
						PS_SET_OUTSIDE_LOOP_METER_ACTIVE(TRUE)
						PS_HUD_SET_OBJECTIVE_METER_VISIBLE(FALSE)
						iInstructionCounter = 1
						PS_DLC_Outside_Loop_State = PS_DLC_OUTSIDE_LOOP_LOOP_TWO_PERFORM
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
			BREAK
			
			
			
			CASE PS_DLC_OUTSIDE_LOOP_LOOP_TWO_PERFORM
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						//PS_PLAY_DISPATCHER_INSTRUCTION("PS_OLOOPOBJ", "PS_OLOOPOBJ_2") //"Perform an inside loop."
						IF NOT PS_IS_PLANE_CURRENTLY_LEVEL()			
							iLevelCount = 0
							IF iLevelCount < 2
								PS_TRIGGER_BRIEF(BRIEF_RETRY_BRIDGE_LOOP)
								PS_DLC_Outside_Loop_State = PS_DLC_OUTSIDE_LOOP_OBSTACLE_COURSE_RETRY
								PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
							ELSE
								ePSFailReason = PS_FAIL_DIDNT_FOLLOW_INSTRUCTIONS
								PS_DLC_Outside_Loop_State = PS_DLC_OUTSIDE_LOOP_FAIL
								PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
							ENDIF
						ELSE
							PS_TRIGGER_BRIEF(BRIEF_START_BRIDGE_LOOP)
							PS_RESET_OBJECTIVE_DATA()
							PS_SET_OUTSIDE_LOOP_METER_ACTIVE(TRUE)
							PS_HUD_SET_OBJECTIVE_METER_VISIBLE(TRUe)
							iInstructionCounter = 1
							PS_INCREMENT_SUBSTATE()
						ENDIF
				
						
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF PS_DLC_Outside_Loop_Fail_Check()
					
							PS_DLC_Outside_Loop_State = PS_DLC_OUTSIDE_LOOP_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELIF PS_DLC_Outside_Loop_Progress_Check()
						
							PS_INCREMENT_SUBSTATE()
						
						ELSE
							PS_SET_HINT_CAM_ACTIVE(FALSE)
							IF PS_GET_OBJECTIVE_RETRY_FLAG() AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF (ABSF(fPlaneLastOrient) / PS_UI_ObjectiveMeter.fObjBarDemoninator) >= 0.33
								OR PS_Main.myObjectiveData.fPlaneTotalOrient < 10.0 
								/*	IF NOT PS_IS_UNDER_BRIDGE_FLIGHT_COMPLETED()
										PS_TRIGGER_BRIEF(BRIEF_MISSED_BRIDGE)
									ELSE
										PS_TRIGGER_BRIEF(BRIEF_RETURN_TO_BRIDGE)										
									ENDIF*/
									PS_SET_OBJECTIVE_RETRY_FLAG(FALSE)
									PS_Main.myObjectiveData.fPlaneTotalOrient = 0
									PS_DLC_OUTSIDE_LOOP_PLAY_RETRY_DIALOGUE()
									SETTIMERA(0)
								ENDIF
							ENDIF
							
							//check if we need to display the final bridge checkpoint again in case player missed it and needs to have it displayed again
							
							FLOAT fDistanceToCheckpoint
							
							fDistanceToCheckpoint = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PS_Main.myVehicle, FALSE), vFinalBridgeCheckpointPosition)
							
							//DRAW_DEBUG_TEXT_ABOVE_ENTITY(PS_Main.myVehicle, GET_STRING_FROM_FLOAT(fDistanceToCheckpoint), 0.0)
							
						//	IF NOT PS_IS_PLANE_PERFORMING_OUTSIDE_LOOP()
								IF (fDistanceToCheckpoint > 215
								AND NOT PS_IS_PLANE_PERFORMING_OUTSIDE_LOOP())
								OR fDistanceToCheckpoint > 300
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
										IF iFlewTooFarCount < 2																			
											PS_RESET_OBJECTIVE_DATA()
											PS_SET_OUTSIDE_LOOP_METER_ACTIVE(FALSE)
											PS_HUD_SET_OBJECTIVE_METER_VISIBLE(FALSE)
											PS_DLC_Outside_Loop_State = PS_DLC_OUTSIDE_LOOP_OBSTACLE_COURSE_RETRY
											PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)										
											PS_TRIGGER_BRIEF(BRIEF_FLEW_TOO_FAR)		
										ELSE
											ePSFailReason = PS_FAIL_DIDNT_FOLLOW_INSTRUCTIONS
											PS_DLC_Outside_Loop_State = PS_DLC_OUTSIDE_LOOP_FAIL
											PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)											
										ENDIF
									ENDIF
								ENDIF
						//	ENDIF
							
						//	cprintln(debug_trevor3,"plane orient = ",PS_Main.myObjectiveData.fPlaneTotalOrient," performing loop = ",PS_IS_PLANE_PERFORMING_OUTSIDE_LOOP()," completed loop = ",PS_IS_UNDER_BRIDGE_FLIGHT_COMPLETED())
							
							IF PS_Main.myObjectiveData.fPlaneTotalOrient > 30.0
								IF NOT PS_IS_PLANE_PERFORMING_OUTSIDE_LOOP()
									IF NOT PS_IS_UNDER_BRIDGE_FLIGHT_COMPLETED()
										IF iMissedBridgeCount < 2
											PS_RESET_OBJECTIVE_DATA()
											PS_SET_OUTSIDE_LOOP_METER_ACTIVE(FALSE)
											PS_HUD_SET_OBJECTIVE_METER_VISIBLE(FALSE)
											PS_DLC_Outside_Loop_State = PS_DLC_OUTSIDE_LOOP_OBSTACLE_COURSE_RETRY
											PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)			
											PS_TRIGGER_BRIEF(BRIEF_MISSED_BRIDGE)
										ELSE
											PS_DLC_Outside_Loop_State = PS_DLC_OUTSIDE_LOOP_FAIL
											PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)	
											ePSFailReason = PS_FAIL_DIDNT_FOLLOW_INSTRUCTIONS
										ENDIF
									ENDIF								
								ENDIF
							ENDIF
							
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						cprintln(debug_trevor3,"I")
						PS_RESET_OBJECTIVE_DATA()
						PS_SET_OUTSIDE_LOOP_METER_ACTIVE(FALSE)
					
						PS_DLC_Outside_Loop_State = PS_DLC_OUTSIDE_LOOP_LOOP_TWO_REACT
						
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
				
			CASE PS_DLC_OUTSIDE_LOOP_LOOP_TWO_REACT
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						CPRINTLN(debug_trevor3,"PS_DLC_OUTSIDE_LOOP_LOOP_TWO_REACT: PS_SUBSTATE_ENTER")
						//PS_PLAY_DISPATCHER_INSTRUCTION("PS_OLOOP", "PS_OLOOP_11")
						//PS_SET_TIMER_ACTIVE(FALSE)
						//PS_UPDATE_RECORDS(VECTOR_ZERO)	//don't update records until we're in the success stage
						PS_INCREMENT_SUBSTATE()
						
						
						//IF iCurrentChallengeScore >= iSCORE_FOR_GOLD
							//PS_PLAY_DISPATCHER_INSTRUCTION("PS_OLpass","PS_OLpass_3")
						//ELIF iCurrentChallengeScore >= iSCORE_FOR_SILVER
							//PS_PLAY_DISPATCHER_INSTRUCTION("PS_OLpass","PS_OLpass_2")
						//ELIF iCurrentChallengeScore >= iSCORE_FOR_BRONZE
							//PS_PLAY_DISPATCHER_INSTRUCTION("PS_OLpass","PS_OLpass_1")
						//ELSE
						IF GET_TIMER_IN_SECONDS_SAFE(PS_Main.tRaceTimer) > PS_main.myChallengeData.BronzeTime
							//this is a fail, time is more than bronze time
							PS_PLAY_DISPATCHER_INSTRUCTION("PS_OLfail", "PS_OLfail_5")
							//PS_PLAY_DISPATCHER_INSTRUCTION_2_LINES("PS_OLfail", "PS_OLfail_1", "PS_OLfail", "PS_OLfail_2")	
						ENDIF
						cprintln(debug_trevor3,"PS_DLC_OUTSIDE_LOOP_SUCCESS : PS_SUBSTATE_ENTER : elapsed time = ",saveData[0].ElapsedTime)
						
					BREAK
					CASE PS_SUBSTATE_UPDATE
						CPRINTLN(debug_trevor3,"PS_DLC_OUTSIDE_LOOP_LOOP_TWO_REACT: PS_SUBSTATE_UPDATE")
						IF PS_DLC_Outside_Loop_Fail_Check()
							CPRINTLN(debug_trevor3,"fail check")
							PS_DLC_Outside_Loop_State = PS_DLC_OUTSIDE_LOOP_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						//ELIF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							CPRINTLN(debug_trevor3,"No convo")
						ELIF 	IS_TIMER_STARTED(tRetryInstructionTimer)			//added extra delay as per B*1737350, 
								CPRINTLN(debug_trevor3,"check timer ",GET_TIMER_IN_SECONDS(tRetryInstructionTimer))
								IF 	GET_TIMER_IN_SECONDS(tRetryInstructionTimer) > 2.0	//but when dialogue works the extra delay might not be necessary
								AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									CPRINTLN(debug_trevor3,"timer finished")
									PS_INCREMENT_SUBSTATE()
								ENDIF
							//ELSE
							//	CPRINTLN(debug_trevor3,"restart timer")
							//	RESTART_TIMER_NOW(tRetryInstructionTimer)
							//ENDIF
						ELSE
							
							CPRINTLN(debug_trevor3,"restart timer b ")
							RESTART_TIMER_NOW(tRetryInstructionTimer)				//start extra delay when player does the loop but before scoreboard comes up
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						CPRINTLN(debug_trevor3,"PS_DLC_OUTSIDE_LOOP_LOOP_TWO_REACT: PS_SUBSTATE_EXIT")
						
						PS_SET_TIMER_ACTIVE(FALSE)
						PS_UPDATE_RECORDS(VECTOR_ZERO)	//update records now
						
						IF Pilot_School_Data_Check_Mission_Success(PS_Main.myChallengeData, PS_Main.myPlayerData)
							PS_DLC_Outside_Loop_State = PS_DLC_OUTSIDE_LOOP_SUCCESS
						ELSE
							PS_DLC_Outside_Loop_State = PS_DLC_OUTSIDE_LOOP_FAIL
						ENDIF
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
									
			CASE PS_DLC_OUTSIDE_LOOP_SUCCESS
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_TRIGGER_MUSIC(PS_MUSIC_LOOP_PASS)
						CPRINTLN(debug_trevor3,"PS_DLC_OUTSIDE_LOOP_SUCCESS: PS_SUBSTATE_ENTER")
						PS_UPDATE_RECORDS(VECTOR_ZERO)
						PS_PLAY_LESSON_FINISHED_LINE("PS_OLpass", "PS_OLpass_1", "PS_OLpass_2", "PS_OLpass_3", "")
						
						
						PS_LESSON_END(TRUE)
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						CPRINTLN(debug_trevor3,"PS_DLC_OUTSIDE_LOOP_SUCCESS: PS_SUBSTATE_UPDATE")
						IF NOT IS_PLAYER_DEAD(player_id())							
						OR GET_MANUAL_RESPAWN_STATE()= MRS_READY_FOR_PLACEMENT
							IF NOT PS_HUD_IS_SCORECARD_ACTIVE()
								PS_INCREMENT_SUBSTATE()
							ENDIF
						ELSE
							PRINTLN("PLAYER IS DEAD SETTING RESPAWN STATE")
							//Set the player to respawn state if dead and doesn't have respawn state set
							IF IS_PLAYER_DEAD(player_id())
							AND GET_MANUAL_RESPAWN_STATE() = MRS_NULL
								SET_MANUAL_RESPAWN_STATE(MRS_RESPAWN_PLAYER_HIDDEN)
							ENDIF
						ENDIF
						cprintln(debug_trevor3,"PS_DLC_OUTSIDE_LOOP_SUCCESS : PS_SUBSTATE_UPDATE : elapsed time = ",saveData[0].ElapsedTime)
						BREAK
					CASE PS_SUBSTATE_EXIT
						CPRINTLN(debug_trevor3,"PS_DLC_OUTSIDE_LOOP_SUCCESS: PS_SUBSTATE_EXIT")
						bFinishedChallenge = TRUE
						PS_INCREMENT_SUBSTATE()
						cprintln(debug_trevor3,"PS_DLC_OUTSIDE_LOOP_SUCCESS : PS_SUBSTATE_EXIT : elapsed time = ",saveData[0].ElapsedTime)
						BREAK
				ENDSWITCH
				BREAK
				
			CASE PS_DLC_OUTSIDE_LOOP_FAIL
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
					
						//Clear any brief that has been marked
						currentBrief = BRIEF_NONE
					
						PS_TRIGGER_MUSIC(PS_MUSIC_LOOP_FAIL)
						PS_UPDATE_RECORDS(VECTOR_ZERO)
						//PS_PLAY_LESSON_FINISHED_LINE("PS_OLOOP", "PS_OLOOP_12", "PS_OLOOP_13", "PS_OLOOP_14", "PS_OLOOP_15")
						
						//is this needed here?
						//PS_PLAY_DISPATCHER_INSTRUCTION("PS_OLfail","PS_OLfail_1")
						DO_NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, FALSE, FALSE, TRUE)
						PS_LESSON_END(FALSE, FALSE)
						PS_INCREMENT_SUBSTATE()
					BREAK
					CASE PS_SUBSTATE_UPDATE
						IF NOT PS_HUD_IS_SCORECARD_ACTIVE()
							PS_INCREMENT_SUBSTATE()
						ENDIF
					BREAK
					CASE PS_SUBSTATE_EXIT
						bFinishedChallenge = TRUE
						PS_INCREMENT_SUBSTATE()
					BREAK
				ENDSWITCH
				BREAK
		ENDSWITCH
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC PS_DLC_OUTSIDE_LOOP_MainCleanup()
//take care of hud
	PS_HUD_SET_GUTTER_ICON_ACTIVE(FALSE)
	PS_HUD_SET_OBJECTIVE_METER_VISIBLE(FALSE)
	PS_SET_TIMER_ACTIVE(FALSE)
	PS_HUD_CLEANUP()
	CLEAR_PRINTS()
	CLEAR_HELP()

//take care of objects
	//player first
	PS_SEND_PLAYER_BACK_TO_MENU()
	//then vehicle
	
	//then checkpoints
	PS_RESET_CHECKPOINT_MGR()

//loaded assets
	REMOVE_VEHICLE_RECORDING(iPS_PreviewVehicleRecordingID, "PilotSchool")	//change name to pilotschooldlc
	SET_MODEL_AS_NO_LONGER_NEEDED(VehicleToUse)	
	
//dont cleaning up
	#IF IS_DEBUG_BUILD
		DEBUG_MESSAGE("******************Cleaning up PS_DLC_Outside_Loop.sc******************")
	#ENDIF
ENDPROC

// EOF
//****************************************************************************************************
//****************************************************************************************************

