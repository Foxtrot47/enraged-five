//****************************************************************************************************
//
//	Author: 		Kevin Bolt / Mark Beagan
//	Date: 			18/10/13
//	Description:	"Engine Failure" Challenge for Pilot School. The player is required to pilot
//					a plane with the engine failing.
//****************************************************************************************************

USING "minigames_helpers.sch"
USING "Pilot_School_MP_Challenge_Helpers.sch"

VECTOR vTouchdownCoord2
VECTOR vplanePos2

//Flag for landing gear
BOOL bLandingGear2 = FALSE
BOOL bEngineFailTimeSet2 = FALSE

//****************************************************************************************************
//****************************************************************************************************
//	Private functions that are used only by Engine Failure
//****************************************************************************************************
//****************************************************************************************************

PROC  PS_DLC_Engine_Failure2_Init_Checkpoints(INT iRouteToDisplay = 1)
	PS_RESET_CHECKPOINT_MGR()
		iRouteToDisplay=iRouteToDisplay
	
	//PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<-3726.6357, 3997.1201, 800>>) //start
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_LANDING, <<-2109.8767, 2918.7434, 31.8099>>) //Landing location

ENDPROC

FUNC BOOL PS_IS_Engine_Failure2_FLIGHT_COMPLETED()
	RETURN FALSE
ENDFUNC

//Used to boost the players movement
PROC PS_Engine_Failure2_Boost_Movement()
//Pitch boost
/*INT iOffSet = 5
INT iupforce = 15*/
Vector vrot

vrot = vrot

//Brake boost
INT iBrakeBoost = 85
INT iBrakeClose = 50
INT iRunwayBB = 100
//INT iBrakeLong = 150
//INT iBrakeNonZ = 500
VECTOR vecVel
VECTOR vecNeg
VECTOR vecRem

	IF NOT IS_VEHICLE_FUCKED(PS_Main.myVehicle)
		PRINTSTRING("ENGINE FAILURE - Left Axis Y Value : ") PRINTFLOAT(GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y)) PRINTNL()
		vrot = GET_ENTITY_ROTATION(PS_Main.myVehicle, EULER_XYZ)
		
		PRINTSTRING("ENGINE FAILURE - Rotation  XYZ     : <<") PRINTFLOAT(vrot.x) PRINTSTRING(", ") PRINTFLOAT(vrot.y) PRINTSTRING(", ") PRINTFLOAT(vrot.z) PRINTSTRING(">>") PRINTNL()
		//Pitch boost
		/*IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_FLY_PITCH_UP_ONLY) OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_MOVE_UP)
			PRINTSTRING("ENGINE FAILURE - CONTROL TO FLY UP BEING PRESSED") PRINTNL()
			
			IF vrot.X < 10
				//SET_ENTITY_ROTATION(PS_Main.myVehicle, <<vrot.X + 1, vrot.Y, vrot.Z>>)
			ENDIF
			
			/*APPLY_FORCE_TO_ENTITY(PS_Main.myVehicle, APPLY_TYPE_FORCE, <<0,0,iupforce>>, <<0, iOffSet, 0>>, 0, TRUE, TRUE, TRUE)
			APPLY_FORCE_TO_ENTITY(PS_Main.myVehicle, APPLY_TYPE_FORCE, <<0,0,-iupforce>>, <<0, -iOffSet, 0>>, 0, TRUE, TRUE, TRUE)	
		ENDIF*/

		vplanePos2 = GET_ENTITY_COORDS(PS_Main.myVehicle)

		if vplanePos2.z < 300
			IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_LT)
				PRINTSTRING("ENGINE FAILURE - PLAYER IS BREAKING")
				GET_ENTITY_SPEED(PS_Main.myVehicle)
				
				if vplanePos2.z < 100
					vecvel = GET_ENTITY_VELOCITY(PS_Main.myVehicle)
					vecNeg = <<0, 0, vecvel.Z/iBrakeClose>>
					vecRem = <<vecvel.X - vecNeg.X, vecvel.Y - vecNeg.Y, vecvel.Z - vecNeg.Z>>
				ELSE
					vecvel = GET_ENTITY_VELOCITY(PS_Main.myVehicle)
					vecNeg = <<0, 0, vecvel.Z/iBrakeBoost>>
					vecRem = <<vecvel.X - vecNeg.X, vecvel.Y - vecNeg.Y, vecvel.Z - vecNeg.Z>>
				ENDIF
				
				if vplanePos2.z < 40
					vecNeg = <<vecvel.X/iRunwayBB, vecvel.Y/iRunwayBB, 0>>
					vecRem = <<vecvel.X - vecNeg.X, vecvel.Y - vecNeg.Y, vecvel.Z - vecNeg.Z>>
				ENDIF
				
				SET_ENTITY_VELOCITY(PS_Main.myVehicle, vecRem)
			ENDIF
		ELSE
			/*vecvel = GET_ENTITY_VELOCITY(PS_Main.myVehicle)
			vecNeg = <<0, 0, vecvel.Z/iBrakeLong>>
			vecRem = <<vecvel.X - vecNeg.X, vecvel.Y - vecNeg.Y, vecvel.Z - vecNeg.Z>>*/
		ENDIF
	ENDIF
ENDPROC

//Gets the current time and sets the clock
PROC PS_DLC_ENGINE2_SET_TIME()

	if not bEngineFailTimeSet2 OR GET_CLOCK_HOURS() != 9				
		
		bEngineFailTimeSet2 = TRUE
	ENDIF
ENDPROC

//Check for player trying to deploy landing gear
PROC CHECK_LANDING_GEAR2()
	IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_LS)
		if bLandingGear2
			CONTROL_LANDING_GEAR(PS_Main.myVehicle,LGC_RETRACT)
			bLandingGear2 = FALSE
		ELSE
			CONTROL_LANDING_GEAR(PS_Main.myVehicle,LGC_DEPLOY)
			bLandingGear2 = TRUE
		ENDIF
	ENDIF
ENDPROC

//Update the distance being displayed
PROC PS_Engine_Update_Distance2()
	PS_SET_DIST_HUD_ACTIVE(TRUE)
ENDPROC

/// PURPOSE:
///    Determines what fail conditions we need to check dependent on the 
/// PARAMS:
///    PS_Main -
///    
    
FUNC BOOL PS_DLC_Engine_Failure2_Fail_Check()
	IF PS_GET_SUBSTATE() = PS_SUBSTATE_UPDATE
		SWITCH(PS_DLC_Engine_Failure2_State)
			CASE PS_DLC_ENGINE2_INIT
				//No fail check to do
				RETURN FALSE
				
			CASE PS_DLC_Engine2_COUNTDOWN
				//No fail check to do
				RETURN FALSE
				
			CASE PS_DLC_Engine2_CUTOUT
				//Do runway check and default checks
				IF PS_UPDATE_FAIL_CHECKS(PS_Main, FALSE, FALSE,FALSE,TRUE,TRUE,FALSE,TRUE,TRUE,TRUE)
					cprintln(debug_trevor3,"FAIL A")
					RETURN TRUE
				ENDIF
			BREAK
			
			CASE PS_DLC_Engine2_on_runway
				IF PS_UPDATE_FAIL_CHECKS(PS_Main, FALSE, FALSE,FALSE,TRUE,FALSE,FALSE,TRUE,TRUE,TRUE)
					cprintln(debug_trevor3,"FAIL B")
					RETURN TRUE
				ENDIF
			BREAK
			
			DEFAULT
				//Do default checks
				IF PS_UPDATE_FAIL_CHECKS(PS_Main, FALSE, FALSE,FALSE,TRUE,TRUE,FALSE,TRUE,TRUE,TRUE)
					cprintln(debug_trevor3,"FAIL C")
					RETURN TRUE
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
	//If we get here, we havent failed yet
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks for objective goals completed based on the stage/state we are in of the challenge progression
///    Also updates ChallengeState when we complete an objective 
/// PARAMS:
///    PS_Main - 
FUNC BOOL PS_DLC_Engine_Failure2_Progress_Check()		

	SWITCH(PS_DLC_Engine_Failure2_State)
			CASE PS_DLC_ENGINE2_INIT
				RETURN FALSE
			BREAK
				
			CASE PS_DLC_Engine2_COUNTDOWN
				RETURN FALSE
			BREAK
				
		//Give player control and the first instruction to take off
		//Check if player is staying on the runway and not driving offroad
			CASE PS_DLC_Engine2_CUTSCENE
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_UPDATE
						//See if the player is close to the checkpoint yet
						IF PS_GET_CHECKPOINT_PROGRESS() > 1 OR PS_IS_LAST_CHECKPOINT_CLEARED()
							RETURN TRUE
						ENDIF
						BREAK
				ENDSWITCH
			BREAK
			
			CASE PS_DLC_Engine2_on_runway
				IF GET_ENTITY_SPEED(PS_Main.myVehicle) < 20
					IF GET_ENTITY_SPEED(PS_Main.myVehicle) > -1 AND GET_ENTITY_SPEED(PS_Main.myVehicle) < 1
						RETURN TRUE
					ENDIF
					//Prevent player ejecting after slowly landing
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
				ENDIF
			BREAK
				
			CASE PS_DLC_Engine2_CUTOUT
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					RETURN TRUE
				ENDIF
			BREAK
		ENDSWITCH
	RETURN FALSE
ENDFUNC

PROC PS_DLC_Engine_Failure2_FORCE_FAIL
	ePSFailReason = PS_FAIL_DEBUG
	PS_DLC_Engine_Failure2_State = PS_DLC_Engine2_FAIL
	PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
ENDPROC

PROC PS_DLC_Engine_Failure2_FORCE_PASS
	PS_DLC_Engine_Failure2_State = PS_DLC_Engine2_SUCCESS
	PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
ENDPROC

//****************************************************************************************************
//****************************************************************************************************
//	Public functions! (used by Pilot_School.sc to run the challenges)
//****************************************************************************************************
//****************************************************************************************************

PROC PS_DLC_Engine_Failure2_Initialise()
	PS_DLC_Engine_Failure2_Init_Checkpoints()

	//init vars
	PS_DLC_Engine_Failure2_State = PS_DLC_ENGINE2_INIT
	iPS_PreviewVehicleRecordingID = iPS_PEVIEW_VEH_REC_ID_DLC_ENGINE_FAILURE
	vStartPosition								= <<-4324.3662, 4201.3423, 1800.6536>> //<<-3726.6357, 3997.1201, 800.6536>>	//<<1939.2792, 3041.4685, 280>>	//mid air countryside <<2120.3811, 4806.7539, 40.1959>>	//mckenzie airfield
	vStartRotation 								=  <<-5, 0, -120.8831>>//<<-39.6548, 1.1660, -120.8831>>		//<<-7.4175, -7.1073, 65.0>>	//mid air countryside <<8.8095, -0.0036, 115.0>> 		//mckenzie airfield
	fStartHeading 								= 237.6284							//65.0							//mid air countryside 115.0								//mckenzie airfield
	iPSDialogueCounter							= 1
	iPSHelpTextCounter							= 1
	iPSObjectiveTextCounter						= 1
	iPreviewCameraProgress						= 0
	
	//Player vehicle model
	VehicleToUse = LAZER
	//PS_SET_GROUND_OFFSET(1.5)
	
	PS_SETUP_CHALLENGE()
	PS_REQUEST_SHARED_ASSETS()
	
	//Prevent wanted level
	SET_WANTED_LEVEL_MULTIPLIER(0)
	
	//load assets
	REQUEST_VEHICLE_RECORDING(iPS_PreviewVehicleRecordingID, sPreviewRecordingName)
	IF NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(iPS_PreviewVehicleRecordingID, sPreviewRecordingName)
		OR NOT PS_ARE_SHARED_ASSETS_LOADED()
		
	ENDIF
	
	fPreviewVehicleSkipTime = 0
	fPreviewTime = 18
	
	SET_WEATHER_TYPE_NOW("EXTRASUNNY")
	
	vPilotSchoolSceneCoords = vStartPosition
	IF HAS_VEHICLE_RECORDING_BEEN_LOADED(iPS_PreviewVehicleRecordingID, sPreviewRecordingName)
		vPilotSchoolPreviewCoords = GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(iPS_PreviewVehicleRecordingID, fPreviewVehicleSkipTime, sPreviewRecordingName)
		vPilotSchoolPreviewRot = GET_ROTATION_OF_VEHICLE_RECORDING_AT_TIME(iPS_PreviewVehicleRecordingID, fPreviewVehicleSkipTime, sPreviewRecordingName)
	ENDIF
	
	//Spawn player vehicle			
	PS_CREATE_VEHICLE()
	
	#IF IS_DEBUG_BUILD
		DEBUG_MESSAGE("******************Setting up PS_DLC_Engine_Failure2.sc******************")
	#ENDIF
ENDPROC

FUNC BOOL PS_DLC_Engine_Failure2_Preview()
	IF PS_PREVIEW_UPDATE_CUTSCENE()
		SWITCH(PS_PreviewState)
			CASE PS_PREVIEW_SETUP
				PS_PREVIEW_SETUP_PLAYER_FOR_CUTSCENE()
				cprintln(debug_trevor3,"Start playback")
				SET_VEHICLE_CONVERSATIONS_PERSIST(true, true)
				PS_PREVIEW_DIALOGUE_PLAY("PS_EFPREV", "PS_EFPREV_1", "PS_EFPREV_2")
				IF NOT IS_VEHICLE_FUCKED(PS_Main.myVehicle)
					START_PLAYBACK_RECORDED_VEHICLE(PS_Main.myVehicle, iPS_PreviewVehicleRecordingID, "PilotSchool")
					SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(PS_Main.myVehicle, fPreviewVehicleSkipTime)
					SET_PLAYBACK_SPEED(PS_Main.myVehicle, 1.0)
					//SET_VEHICLE_ENGINE_HEALTH(PS_Main.myVehicle,-1000)
					SET_ENTITY_HEALTH(PS_Main.myVehicle,2000)
					SET_VEHICLE_ENGINE_ON(PS_Main.myVehicle,false,true)
					CONTROL_LANDING_GEAR(PS_Main.myVehicle,LGC_RETRACT_INSTANT)
				ENDIF
				
				PS_DLC_ENGINE_SET_TIME()
					
				iPreviewCameraProgress 	= 0
				iPreviewCheckpointIdx 	= PS_GET_CLOSEST_PREVIEW_CHECKPOINT(iPS_PreviewVehicleRecordingID, PS_Main.myCheckpointz, fPreviewVehicleSkipTime)
				PS_ACTIVATE_CHECKPOINT_MGR(PS_LESSON_STUNT, iPreviewCheckpointIdx, TRUE)
			BREAK
			
			CASE PS_PREVIEW_PLAYING
				/*if iPSDialogueCounter = 1 AND NOT IS_MESSAGE_BEING_DISPLAYED()
					PS_PRINT_FAKE_SUBTITLE("PS_ENGINE_PREV1", DEFAULT_GOD_TEXT_TIME / 2)
					iPSDialogueCounter++
				ELIF iPSDialogueCounter = 2 AND NOT IS_MESSAGE_BEING_DISPLAYED()
					PS_PRINT_FAKE_SUBTITLE("PS_ENGINE_PREV2", DEFAULT_GOD_TEXT_TIME / 2)
					iPSDialogueCounter++
				ELIF iPSDialogueCounter = 3 AND NOT IS_MESSAGE_BEING_DISPLAYED()
					PS_PRINT_FAKE_SUBTITLE("PS_ENGINE_PREV3", DEFAULT_GOD_TEXT_TIME / 2)
					iPSDialogueCounter++
				ENDIF*/			
			BREAK
			
			CASE PS_PREVIEW_CLEANUP
				PS_PREVIEW_CLEANUP_CUTSCENE()
			BREAK
			
		ENDSWITCH
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

PROC PS_DLC_Engine_Failure2_MainSetup()
	PS_DLC_Engine_Failure2_Init_Checkpoints()
	SET_MINIMAP_COMPONENT(MINIMAP_COMPONENT_RUNWAY_1, FALSE)
	IF NOT IS_VEHICLE_FUCKED(PS_Main.myVehicle)
		SET_VEHICLE_ENGINE_ON(PS_Main.myVehicle,true,true)
		SET_ENTITY_ROTATION(PS_Main.myVehicle, vStartRotation)
		SET_ENTITY_HEALTH(PS_Main.myVehicle, 1500)
		CONTROL_LANDING_GEAR(PS_Main.myVehicle,LGC_RETRACT_INSTANT)
	ENDIF
	
	PS_DLC_ENGINE_SET_TIME()

	NETWORK_START_LOAD_SCENE(<<-1154.1101, -2715.2024, 18.8923>>)
ENDPROC

#IF IS_DEBUG_BUILD
	FUNC BOOL PS_DLC_Engine_Failure2_J_SKIP()
		/*PS_DLC_Engine_Failure2_CHALLENGE nextState = PS_DLC_Engine_Failure2_State
		IF PS_DLC_Engine_Failure2_State > PS_DLC_Engine_Failure2_TAKEOFF
			IF PS_DLC_Engine_Failure2_State < PS_DLC_Engine_Failure2_LOOP_ONE_PREP
				nextState = PS_DLC_Engine_Failure2_LOOP_ONE_PREP			
			ELIF PS_DLC_Engine_Failure2_State < PS_DLC_Engine_Failure2_LOOP_TWO_PREP
				nextState = PS_DLC_Engine_Failure2_LOOP_TWO_PREP
			ENDIF
			IF nextState != PS_DLC_Engine_Failure2_State AND NOT IS_ENTITY_DEAD(PS_Main.myVehicle)
				//send plane to the takeoff checkpoint
				SET_ENTITY_COORDS(PS_Main.myVehicle, PS_GET_CHECKPOINT_FROM_ID(1))
				SET_VEHICLE_FORWARD_SPEED(PS_Main.myVehicle, 60.0)
				PS_DLC_Engine_Failure2_State = nextState
				PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
				RETURN TRUE
			ENDIF	
		ENDIF*/
		RETURN FALSE
	ENDFUNC
#ENDIF	

FUNC BOOL PS_DLC_Engine_Failure2_MainUpdate()			
	IF TempDebugInput() AND NOT bFinishedChallenge
		
		#IF IS_DEBUG_BUILD
			IF PS_CHECK_FOR_STUNT_LESSON_J_SKIP()
				IF PS_DLC_Engine_Failure2_J_SKIP()
					RETURN TRUE //if we do a j skip, then skip an update
				ENDIF
			ENDIF
		#ENDIF
		
		PS_LESSON_UDPATE()
		
		//Disable control of certain actions
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_THROTTLE_UP)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_NEXT_WEAPON)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_PREV_WEAPON)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_ATTACK)
		//DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_FLY_UNDERCARRIAGE)
		
		SWITCH(PS_DLC_Engine_Failure2_State)
		
			CASE PS_DLC_ENGINE2_INIT
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": PS_DLC_Engine_Failure2_INIT")
				//updating autopilot to start the player out in the air
				PS_AUTO_PILOT(PS_Main, 30.0, FALSE, vStartRotation.x, vStartRotation.y)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						iPSDialogueCounter = 1
						IF NOT IS_VEHICLE_FUCKED(PS_Main.myVehicle)
							SET_VEHICLE_ENGINE_ON(PS_Main.myVehicle,true,true)
							FREEZE_ENTITY_POSITION(PS_Main.myVehicle, FALSE)
						ENDIF
						//SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
						DO_NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
						PS_INCREMENT_SUBSTATE()
					BREAK
					
					CASE PS_SUBSTATE_UPDATE
						PS_INCREMENT_SUBSTATE()
					BREAK
					
					CASE PS_SUBSTATE_EXIT
						PS_DLC_Engine_Failure2_State = PS_DLC_Engine2_COUNTDOWN
						PS_INCREMENT_SUBSTATE()
					BREAK
				ENDSWITCH
			BREAK

			CASE PS_DLC_Engine2_COUNTDOWN
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": PS_DLC_Engine_Failure2_COUNTDOWN")
				//updating autopilot to start the player out in the air
				PS_AUTO_PILOT(PS_Main, 30.0, FALSE, vStartRotation.x, vStartRotation.y)
				
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_INCREMENT_SUBSTATE()
					BREAK
					
					CASE PS_SUBSTATE_UPDATE
						//SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
						//SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
						IF	PS_HUD_UPDATE_COUNTDOWN(FALSE, FALSE)
							PS_INCREMENT_SUBSTATE()
						ENDIF
					BREAK
					
					CASE PS_SUBSTATE_EXIT
						//SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						DO_NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)	
						//SET_PLAYER_INVINCIBLE(PLAYER_ID(),FALSE)
						PS_DLC_Engine_Failure2_State = PS_DLC_Engine2_CUTOUT
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
			BREAK
			
			CASE PS_DLC_Engine2_CUTOUT
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						cprintln(debug_trevor3,"Got here")
						//SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						DO_NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)	
						//SET_PLAYER_INVINCIBLE(PLAYER_ID(),FALSE)
						SET_VEHICLE_CONVERSATIONS_PERSIST(true, true)
						IF NOT IS_VEHICLE_FUCKED(PS_Main.myVehicle)
							SET_VEHICLE_ENGINE_ON(PS_Main.myVehicle,false,true)
						ENDIF
						//SET_VEHICLE_ENGINE_HEALTH(PS_Main.myVehicle,-1000)
						//PS_PLAY_DISPATCHER_INSTRUCTION_3_LINES("PS_ENGFA", "PS_ENGFA_1", "PS_ENGFA", "PS_ENGFA_2", "PS_ENGFA", "PS_ENGFA_3")
						PS_ACTIVATE_CHECKPOINT_MGR(PS_LESSON_LANDING)
						PS_HUD_RESET_TIMER()
						PS_HUD_SET_GUTTER_ICON_ACTIVE(TRUE)
						
						PS_Engine_Update_Distance2()
						
						//PS_SET_TIMER_ACTIVE(TRUE)
						PS_SET_HINT_CAM_ACTIVE(TRUE)
						PS_SET_HINT_CAM_COORD(PS_GET_CURRENT_CHECKPOINT())
						PS_INCREMENT_SUBSTATE()
					BREAK
					
					CASE PS_SUBSTATE_UPDATE
					
						//Movement for player
						PS_Engine_Failure2_Boost_Movement()
						CHECK_LANDING_GEAR2()
					
					
						//Dialogue
						if iPSDialogueCounter = 1 AND NOT IS_MESSAGE_BEING_DISPLAYED()
							PS_PLAY_DISPATCHER_INSTRUCTION("PS_EF", "PS_EF_2")
							//PS_PRINT_FAKE_SUBTITLE("PS_ENGINE_DIAG1", DEFAULT_GOD_TEXT_TIME / 2)
							iPSDialogueCounter++
						ENDIF
						
						if iPSDialogueCounter = 2 AND NOT IS_MESSAGE_BEING_DISPLAYED()
							PS_PLAY_DISPATCHER_INSTRUCTION("PS_EF", "PS_EF_3")
							//PS_PRINT_FAKE_SUBTITLE("PS_ENGINE_DIAG3", DEFAULT_GOD_TEXT_TIME / 2)
							iPSDialogueCounter++
						ENDIF
						
						/*if vplanePos2.z < 800
							if iPSDialogueCounter = 3 AND NOT IS_MESSAGE_BEING_DISPLAYED()
								PS_PRINT_FAKE_SUBTITLE("PS_ENGINE_DIAG3", DEFAULT_GOD_TEXT_TIME / 2)
								iPSDialogueCounter++
							ENDIF
						ENDIF*/
						
						if vplanePos2.z < 500
							if iPSDialogueCounter = 3 AND NOT IS_MESSAGE_BEING_DISPLAYED()
								PS_PLAY_DISPATCHER_INSTRUCTION("PS_EF", "PS_EF_4")
								//PS_PRINT_FAKE_SUBTITLE("PS_ENGINE_DIAG4", DEFAULT_GOD_TEXT_TIME / 2)
								iPSDialogueCounter++
							ENDIF
						ENDIF
						
						if vplanePos2.z < 200 AND bLandingGear2 = FALSE
							if iPSDialogueCounter = 4 AND NOT IS_MESSAGE_BEING_DISPLAYED()
								//PS_PRINT_FAKE_SUBTITLE("PS_ENGINE_DIAG5", DEFAULT_GOD_TEXT_TIME / 2)
								iPSDialogueCounter++
							ENDIF
						ENDIF
						
						IF PS_DLC_Engine_Failure2_Fail_Check()
							PS_DLC_Engine_Failure2_State = PS_DLC_Engine2_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ENDIF
	
						IF NOT IS_VEHICLE_FUCKED(PS_Main.myVehicle)			
							SET_ENTITY_HEALTH(PS_Main.myVehicle,20000)
							SET_VEHICLE_PETROL_TANK_HEALTH(PS_Main.myVehicle,1000)							
						ENDIF
						
						//if vehicle has touched down
						IF NOT IS_ENTITY_IN_AIR(PS_Main.myVehicle)
							vTouchdownCoord2 = GET_ENTITY_COORDS(PS_Main.myVehicle)
							PS_DLC_Engine_Failure2_State = PS_DLC_Engine2_on_runway
						
							PS_SET_SUBSTATE(PS_SUBSTATE_UPDATE)
						ENDIF																		
					BREAK
				ENDSWITCH
			BREAK
			
			CASE PS_DLC_Engine2_on_runway
				SWITCH PS_GET_SUBSTATE()
					
					CASE PS_SUBSTATE_UPDATE
					
						IF NOT IS_VEHICLE_FUCKED(PS_Main.myVehicle)			
							cprintln(debug_Trevor3,"Engine health: ",GET_VEHICLE_ENGINE_HEALTH(PS_Main.myVehicle)," veh health: ",GET_ENTITY_HEALTH(PS_Main.myVehicle))
							SET_ENTITY_HEALTH(PS_Main.myVehicle,2000)
							SET_VEHICLE_PETROL_TANK_HEALTH(PS_Main.myVehicle,1000)				
						ENDIF
						
						IF PS_DLC_Engine_Failure2_Fail_Check()
							CPRINTLN(DEBUG_TREVOR3, "Fail state returning true here")
							PS_DLC_Engine_Failure2_State = PS_DLC_Engine2_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ENDIF
						
						PS_UPDATE_PLAYER_DATA(vTouchdownCoord2)
						
						IF PS_DLC_Engine_Failure2_Progress_Check()
							CPRINTLN(DEBUG_TREVOR3, "progress acknowledged")
							PS_INCREMENT_SUBSTATE()
						ENDIF
					BREAK
					
					CASE PS_SUBSTATE_EXIT
						CLEAR_PRINTS()
						CLEAR_HELP()
					
						IF Pilot_School_Data_Check_Mission_Success(PS_Main.myChallengeData, PS_Main.myPlayerData)
							CPRINTLN(DEBUG_TREVOR3, "mission success found")
							PS_DLC_Engine_Failure2_State = PS_DLC_Engine2_SUCCESS
						ELSE
							CPRINTLN(DEBUG_TREVOR3, "mission fail at 1")
							PS_DLC_Engine_Failure2_State = PS_DLC_Engine2_FAIL
						ENDIF
						PS_INCREMENT_SUBSTATE()
					BREAK
				ENDSWITCH
			BREAK
		
									
			CASE PS_DLC_Engine2_SUCCESS
				CPRINTLN(DEBUG_TREVOR3, ": PS_DLC_Engine_Failure2_SUCCESS")
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_HIDE_CHECKPOINT(PS_GET_CHECKPOINT_PROGRESS())
						bPlayerDataHasBeenUpdated = FALSE
						PS_UPDATE_RECORDS(PS_GET_CURRENT_CHECKPOINT())
						PS_LESSON_END(TRUE)
						PS_INCREMENT_SUBSTATE()
					BREAK
					
					CASE PS_SUBSTATE_UPDATE
						IF NOT PS_HUD_IS_SCORECARD_ACTIVE()
							PS_INCREMENT_SUBSTATE()
						ENDIF
					BREAK
					
					CASE PS_SUBSTATE_EXIT
						bFinishedChallenge = TRUE
						PS_INCREMENT_SUBSTATE()
					BREAK
					
				ENDSWITCH
			BREAK
				
			CASE PS_DLC_Engine2_FAIL
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": PS_DLC_Engine_Failure2_FAIL")
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_UPDATE_RECORDS(PS_GET_CURRENT_CHECKPOINT())
						PS_LESSON_END(FALSE)
						PS_INCREMENT_SUBSTATE()
					BREAK
					
					CASE PS_SUBSTATE_UPDATE
						IF NOT PS_HUD_IS_SCORECARD_ACTIVE()
							PS_INCREMENT_SUBSTATE()
						ENDIF
					BREAK
					
					CASE PS_SUBSTATE_EXIT
						bFinishedChallenge = TRUE
						PS_INCREMENT_SUBSTATE()
					BREAK
					
				ENDSWITCH
			BREAK
		ENDSWITCH
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC PS_DLC_Engine_Failure2_MainCleanup()
//take care of hud
	PS_HUD_SET_GUTTER_ICON_ACTIVE(FALSE)
	PS_HUD_SET_OBJECTIVE_METER_VISIBLE(FALSE)
	PS_SET_TIMER_ACTIVE(FALSE)
	PS_HUD_CLEANUP()
	CLEAR_PRINTS()
	CLEAR_HELP()
	
	bLandingGear2 = FALSE

	//player first
	PS_SEND_PLAYER_BACK_TO_MENU()
	
	//Resetting wanted level
	SET_WANTED_LEVEL_MULTIPLIER(1.0)
	
	//then checkpoints
	PS_RESET_CHECKPOINT_MGR()
	
	//Reset the clock time back to original

	bEngineFailTimeSet2 = FALSE

	//loaded assets
	REMOVE_VEHICLE_RECORDING(iPS_PreviewVehicleRecordingID, "PilotSchool")
	SET_MODEL_AS_NO_LONGER_NEEDED(VehicleToUse)	
	
//dont cleaning up
	#IF IS_DEBUG_BUILD
		DEBUG_MESSAGE("******************Cleaning up PS_DLC_Engine_Failure2.sc******************")
	#ENDIF
ENDPROC

// EOF
//****************************************************************************************************
//****************************************************************************************************

