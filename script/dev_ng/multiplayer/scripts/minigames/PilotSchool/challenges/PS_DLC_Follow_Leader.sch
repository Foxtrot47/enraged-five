//****************************************************************************************************
//
//	Author: 		Lukasz Bogaj
//	Date: 			26/10/13
//	Description:	"Follow Leader" Challenge for Pilot School. The player is required to follow
//					instructor plane and perform stunts showed by the instructor.
//****************************************************************************************************

USING "minigames_helpers.sch"
USING "Pilot_School_MP_Challenge_Helpers.sch"


//****************************************************************************************************
//****************************************************************************************************
//	Private functions that are used only by Follow Leader
//****************************************************************************************************
//****************************************************************************************************

FLOAT fCurrentPlaybackSpeed = 1.0

#IF IS_DEBUG_BUILD
BOOL	PS_DLC_Follow_Leader_CameraButtons[PS_MAX_CHECKPOINTS]
VECTOR 	PS_DLC_Follow_Leader_CheckpointPositions[PS_MAX_CHECKPOINTS]
#ENDIF

int icurrentMarker,iLastMarkerScore,iMarkerIndex,iFailFarCounter,iGapTimer
int iMissed[3]

PED_INDEX piGhostPilot


enum PS_FL_enumBrief
	FL_BRIEF_NONE,	
	FL_BRIEF_GO,
	FL_BRIEF_COMMENTS
endenum

PS_FL_enumBrief FL_currentBrief = FL_BRIEF_NONE
//int iDialogueTimer
//int iMissedFlags
//int iHitFlags

PROC PS_TRIGGER_FL_BRIEF(PS_FL_enumBrief thisFLBrief)
	cprintln(debug_trevor3,"Trigger ",enum_to_int(thisFLBrief))
	FL_currentBrief = thisFLBrief
ENDPROC

FUNC BOOL GET_EMPTY_BIT(int &bitToCheck, int iStart, int iEnd, int &iDataStore)
	
	int iStartCheck = GET_RANDOM_INT_IN_RANGE(iStart,iEnd+1)
	
	
	int iQuit
	WHILE iQuit < iEnd-iStart+1
		IF NOT IS_BIT_SET(bitToCheck,iStartCheck)
			SET_BIT(bitToCheck,iStartCheck)
			iDataStore = iStartCheck + 3
			RETURN TRUE
		ENDIF
		iStartCheck++
		
		If iStartCheck > iEnd
			iStartCheck = iStart
		ENDIF
		iQuit++
	ENDWHILE
	RETURN FALSE
ENDFUNC

FUNC TEXT_LABEL_15 GET_COMMENT_CONV_LABEL(int iTextEntry,TEXT_LABEL_7 txtStart)
	TEXT_LABEL_15 returnLabel
	returnLabel = txtStart
	returnLabel += iTextEntry
	RETURN returnLabel
ENDFUNC

PROC PS_PLAY_FL_BRIEF()

	//cprintln(debug_trevor3,ENUM_TO_INT(GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_HELI)))

	SWITCH FL_currentBrief
		CASE FL_BRIEF_GO
			PS_PLAY_DISPATCHER_INSTRUCTION_2_LINES("PS_FLa","PS_FLa_1","PS_FLa","PS_FLa_2")
			icurrentMarker = 1
			iLastMarkerScore = 0
			iMarkerIndex = 0
			
		
			FL_currentBrief = FL_BRIEF_COMMENTS
		BREAK	
		CASE FL_BRIEF_COMMENTS
		
			IF ( PS_DLC_Follow_Leader_State = PS_DLC_FOLLOW_LEADER_OBSTACLE )	//only play these when we're following the course
			
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF GET_DISTANCE_BETWEEN_ENTITIES(player_ped_id(),PS_Main.myGhostVehicle) > 250.0
						IF iGapTimer = 0
						OR GET_GAME_TIMER() > iGapTimer
							iFailFarCounter++
							IF iFailFarCounter >= 3
							
							ELSE
								iGapTimer = GET_GAME_TIMER() + 5000
								PS_PLAY_DISPATCHER_INSTRUCTION_CONVERSATION("PS_FLbad")															
							ENDIF
						ENDIF
					ELSE
						iFailFarCounter = 0
						iGapTimer = 0
					ENDIF
					
					IF IS_VEHICLE_DRIVEABLE(PS_Main.myGhostVehicle)
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(PS_Main.myGhostVehicle)
							IF GET_TIME_POSITION_IN_RECORDING(PS_Main.myGhostVehicle) > 41000
							AND GET_TIME_POSITION_IN_RECORDING(PS_Main.myGhostVehicle) < 42000
								PS_PLAY_DISPATCHER_INSTRUCTION("PS_close","PS_close_2")
							ENDIF
							
						//	IF GET_TIME_POSITION_IN_RECORDING(PS_Main.myGhostVehicle) > 100000
						//	AND GET_TIME_POSITION_IN_RECORDING(PS_Main.myGhostVehicle) < 101000
						//		PS_PLAY_DISPATCHER_INSTRUCTION("PS_close","PS_close_1")
						//	ENDIF
							
							//inverted
							IF GET_TIME_POSITION_IN_RECORDING(PS_Main.myGhostVehicle) > 19106
							AND GET_TIME_POSITION_IN_RECORDING(PS_Main.myGhostVehicle) < 20106
								PS_PLAY_DISPATCHER_INSTRUCTION_CONVERSATION("PS_FLinv")
							ENDIF
							
							//knife
							IF GET_TIME_POSITION_IN_RECORDING(PS_Main.myGhostVehicle) > 46000
							AND GET_TIME_POSITION_IN_RECORDING(PS_Main.myGhostVehicle) < 47000
								PS_PLAY_DISPATCHER_INSTRUCTION_CONVERSATION("PS_FLknife")
							ENDIF
							
							//double inverted
							IF GET_TIME_POSITION_IN_RECORDING(PS_Main.myGhostVehicle) > 76000
							AND GET_TIME_POSITION_IN_RECORDING(PS_Main.myGhostVehicle) < 77000
								PS_PLAY_DISPATCHER_INSTRUCTION_CONVERSATION("PS_FLdbl")
							ENDIF
							
							//pipes
							IF GET_TIME_POSITION_IN_RECORDING(PS_Main.myGhostVehicle) > 85500
							AND GET_TIME_POSITION_IN_RECORDING(PS_Main.myGhostVehicle) < 86500
								PS_PLAY_DISPATCHER_INSTRUCTION_CONVERSATION("PS_FLpipe")
							ENDIF
							
							//interstate
							IF GET_TIME_POSITION_IN_RECORDING(PS_Main.myGhostVehicle) > 99000
							AND GET_TIME_POSITION_IN_RECORDING(PS_Main.myGhostVehicle) < 100000
								PS_PLAY_DISPATCHER_INSTRUCTION_CONVERSATION("PS_FLinter")
							ENDIF
							
							//construction
							IF GET_TIME_POSITION_IN_RECORDING(PS_Main.myGhostVehicle) > 134000
							AND GET_TIME_POSITION_IN_RECORDING(PS_Main.myGhostVehicle) < 135000
								PS_PLAY_DISPATCHER_INSTRUCTION_CONVERSATION("PS_FLconst")
							ENDIF
						ENDIF
					ENDIF
					
					//inverted
					//knife
					//double inversion
					//pipe
					//interstate
					//construction
					
				ENDIF

				IF PS_Main.myCheckpointMgr.curIdx > icurrentMarker
					IF PS_Main.myPlayerData.CheckpointCount > iLastMarkerScore //hit the last marker					
						iMissed[iMarkerIndex] = 1 //hit
					ELSE
						iMissed[iMarkerIndex] = 0 //missed
					ENDIF
					
					iMarkerIndex++
					iLastMarkerScore = PS_Main.myPlayerData.CheckpointCount
					icurrentMarker = PS_Main.myCheckpointMgr.curIdx				
				
					if iMarkerIndex = 2
						If iMissed[0] = 0 and iMissed[1] = 0 //missed two in a row
							cprintln(debug_trevor3,"Missed two in a row")
							PS_PLAY_DISPATCHER_INSTRUCTION_CONVERSATION("PS_FLbad")
							iMarkerIndex = 0						
						endif
					endif
				
					if iMarkerIndex = 3 //hit three markers. Make a comment base3d on last three performance.
						IF IS_VEHICLE_DRIVEABLE(PS_Main.myGhostVehicle)
							IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(PS_Main.myGhostVehicle)
								IF GET_TIME_POSITION_IN_RECORDING(PS_Main.myGhostVehicle) < 80000
								OR (GET_TIME_POSITION_IN_RECORDING(PS_Main.myGhostVehicle) > 87000
								AND GET_TIME_POSITION_IN_RECORDING(PS_Main.myGhostVehicle) < 96000)
								OR GET_TIME_POSITION_IN_RECORDING(PS_Main.myGhostVehicle) > 105000
									
									if iMissed[0]+ iMissed[1]+ iMissed[2] = 3 //good						
										cprintln(debug_trevor3,"Hit three in a row")
										PS_PLAY_DISPATCHER_INSTRUCTION_CONVERSATION("PS_FLwell")						
									elif iMissed[0]+ iMissed[1]+ iMissed[2] > 1 //average						
										cprintln(debug_trevor3,"Hit two in a row")
										PS_PLAY_DISPATCHER_INSTRUCTION_CONVERSATION("PS_FLok")						
									else //bad						
										cprintln(debug_trevor3,"Hit only one in a row")
										PS_PLAY_DISPATCHER_INSTRUCTION_CONVERSATION("PS_FLbad")						
									endif
								ENDIF
							ENDIF
						ENDIF
						iMarkerIndex = 0
					endif
				endif
						//				cprintln(debug_trevor3,"Checkpoints = ",PS_Main.myPlayerData.CheckpointCount,"passed= ",GetPilotSchoolScore_3_checkpointsPassed(PS_Main.myChallengeData, PS_Main.myPlayerData)," current id = ",PS_Main.myCheckpointMgr.curIdx)

			ENDIF

		BREAK
	ENDSWITCH
ENDPROC

#IF IS_DEBUG_BUILD
	//debug widgets variables
	BOOL		bPS_DLC_Follow_Leader_PrintCheckpoints
	BOOL 		bPS_DLC_Follow_Leader_UpdateCheckpoints
	CONST_INT 	PS_DLC_FOLLOW_LEADER_MAX_CHECKPOINTS	33
#ENDIF

#IF IS_DEBUG_BUILD
PROC PS_DLC_FOLLOW_LEADER_CREATE_DEBUG_WIDGETS()

	IF DOES_WIDGET_GROUP_EXIST(wDataFile)
		SET_CURRENT_WIDGET_GROUP(wDataFile)
			START_WIDGET_GROUP("Checkpoints")
			
				INT i
				
				ADD_WIDGET_BOOL("Print Checkpoints", bPS_DLC_Follow_Leader_PrintCheckpoints)
				ADD_WIDGET_BOOL("Update Checkpoints", bPS_DLC_Follow_Leader_UpdateCheckpoints)
			
				REPEAT PS_DLC_FOLLOW_LEADER_MAX_CHECKPOINTS i
					START_WIDGET_GROUP(GET_STRING_FROM_INT(i))
						PS_DLC_Follow_Leader_CheckpointPositions[i] = PS_Main.myCheckpointz[i].position
						ADD_WIDGET_BOOL("Get Debug Cam Position", PS_DLC_Follow_Leader_CameraButtons[i])
						ADD_WIDGET_VECTOR_SLIDER("Position", PS_DLC_Follow_Leader_CheckpointPositions[i], -5000.0, 5000.0, 1.0)
					STOP_WIDGET_GROUP()
				ENDREPEAT
			
			STOP_WIDGET_GROUP()	
		CLEAR_CURRENT_WIDGET_GROUP(wDataFile)
	ENDIF
	
ENDPROC

PROC PS_DLC_FOLLOW_LEADER_UPDATE_DEBUG_WIDGETS()

	INT i
	
	IF PS_IS_CHECKPOINT_MGR_ACTIVE()
		REPEAT PS_GET_TOTAL_CHECKPOINTS() i
			IF PS_IS_CHECKPOINT_VALID(i)
				//DRAW_DEBUG_TEXT_ABOVE_COORDS(PS_Main.myCheckpointz[i].position, GET_STRING_FROM_INT(i), 0.0)
			ENDIF
			IF PS_DLC_Follow_Leader_CameraButtons[i] = TRUE
				PS_DLC_Follow_Leader_CheckpointPositions[i] = GET_FINAL_RENDERED_CAM_COORD()
				PS_DLC_Follow_Leader_CameraButtons[i] = FALSE
			ENDIF
		ENDREPEAT
	ENDIF

	IF ( bPS_DLC_Follow_Leader_UpdateCheckpoints = TRUE )
	
		PS_RESET_CHECKPOINT_MGR()
	
		REPEAT PS_DLC_FOLLOW_LEADER_MAX_CHECKPOINTS i
			IF i != 0
				PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, PS_DLC_Follow_Leader_CheckpointPositions[i])
			ENDIF
		ENDREPEAT
	
		PS_ACTIVATE_CHECKPOINT_MGR(PS_LESSON_FLAG_COLLECTION)
		PS_SHOW_ALL_CHECKPOINTS()
		
		bPS_DLC_Follow_Leader_UpdateCheckpoints = FALSE
		
	ENDIF
	
	IF ( bPS_DLC_Follow_Leader_PrintCheckpoints = TRUE )
		
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("PILOT SCHOOL FOLLOW LEADER CHECKPOINTS")
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_NEWLINE_TO_DEBUG_FILE()
		
		REPEAT PS_DLC_FOLLOW_LEADER_MAX_CHECKPOINTS i
			IF i != 0
				SAVE_STRING_TO_DEBUG_FILE("PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, ")
				SAVE_VECTOR_TO_DEBUG_FILE(PS_DLC_Follow_Leader_CheckpointPositions[i])
				SAVE_STRING_TO_DEBUG_FILE(")//")
				SAVE_INT_TO_DEBUG_FILE(i)
				SAVE_NEWLINE_TO_DEBUG_FILE()
			ENDIF
		ENDREPEAT
	
		bPS_DLC_Follow_Leader_PrintCheckpoints = FALSE
	
	ENDIF

ENDPROC

#ENDIF

PROC PS_DLC_Follow_Leader_Init_Checkpoints()  
	PS_RESET_CHECKPOINT_MGR()
	
	//new route
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<-1357.0000, -1475.0000, 44.0000>>)//1
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_STUNT_INVERTED, <<-549.0000, -2231.0000, 97.5000>>)//2
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<-271.0000, -2425.0000, 97.5000>>)//3
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<338.5000, -2759.0000, 32.0000>>)//4
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_STUNT_SIDEWAYS_L, <<530.0000, -2880.0000, 45.0000>>)//5
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<936.0000, -2954.0000, 26.0000>>)//6
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_STUNT_SIDEWAYS_L, <<1130.0000, -2954.0000, 26.0000>>)//7
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<1307.9315, -2685.9114, 85.0000>>)//8
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<1168.0000, -2470.0000, 90.0000>>)//9
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<790.0000, -2359.6899, 65.0000>>)//10 //very hard position <<790.0000, -2359.6899, 41.9252>>
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_STUNT_INVERTED, <<355.0000, -2317.0000, 50.0000>>)//11
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_STUNT_INVERTED, <<220.0000, -2320.0000, 50.0000>>)//12
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<-84.0000, -2318.0000, 10.0000>>)//13
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<-330.0000, -2250.0000, 10.0000>>)//14
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<-722.2665, -2042.8671, 35.9405>>)//15
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<-892.0000, -1760.0000, 26.5000>>)//16
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_STUNT_SIDEWAYS_L, <<-929.9216, -1289.7740, 36.0409>>)//17
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<-1058.0000, -1028.0000, 25.0000>>)//18
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_STUNT_SIDEWAYS_R, <<-934.2873, -723.9603, 66.2082>>)//19
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<-767.6172, -656.4745, 62.6283>>)//20
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<-527.2345, -657.7930, 63.1901>>)//21
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<-304.1158, -373.9703, 90.1434>>)//22
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<25.6096, -406.2772, 61.2410>>)//23
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<500.0000, -560.0000, 65.0000>>)//24
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<615.5353, -832.2339, 24.5354>>)//25
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<557.4286, -1213.6224, 25.6591>>)//26
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FINISH, <<630.0000, -1432.0000, 60.0000>>)//27	//this should be PS_CHECKPOINT_FINISH
	//PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FINISH, <<650.9619, -2041.3801, 68.4075>>)//28	//for recordings only

ENDPROC

/// PURPOSE:
///    Determines what fail conditions we need to check dependent on the 
FUNC BOOL PS_DLC_Follow_Leader_Fail_Check()
	IF PS_GET_SUBSTATE() = PS_SUBSTATE_UPDATE
		SWITCH(PS_DLC_Follow_Leader_State)
			CASE PS_DLC_FOLLOW_LEADER_INIT
				//No fail check to do
				RETURN FALSE
				
			CASE PS_DLC_FOLLOW_LEADER_COUNTDOWN
				//No fail check to do
				RETURN FALSE
				
			CASE PS_DLC_FOLLOW_LEADER_TAKEOFF
			
				IF iFailFarCounter >= 3
					ePSFailReason = PS_FAIL_SPECIAL_REASON
					RETURN TRUE
				ENDIF
			
				//Do runway check and default checks
				IF PS_UPDATE_FAIL_CHECKS(PS_Main, FALSE, TRUE, FALSE)
					RETURN TRUE
				ENDIF
			BREAK
			
			CASE PS_DLC_FOLLOW_LEADER_OBSTACLE
				IF iFailFarCounter >= 3
					ePSFailReason = PS_FAIL_SPECIAL_REASON
					RETURN TRUE
				ENDIF
				
				IF PS_UPDATE_FAIL_CHECKS(PS_Main, FALSE, TRUE, FALSE)				
					RETURN TRUE
				ENDIF
			BREAK
			
			DEFAULT
				//Do default checks
										
				IF PS_UPDATE_FAIL_CHECKS(PS_Main, FALSE, TRUE, FALSE)				
					RETURN TRUE
				ENDIF
				BREAK
		ENDSWITCH
	ENDIF
	//If we get here, we havent failed yet
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks for objective goals completed based on the stage/state we are in of the challenge progression
///    Also updates ChallengeState when we complete an objective 
FUNC BOOL PS_DLC_Follow_Leader_Progress_Check()
	IF NOT IS_PED_INJURED(PLAYER_PED_ID()) AND IS_VEHICLE_DRIVEABLE(PS_Main.myVehicle)
		SWITCH(PS_DLC_Follow_Leader_State)
			CASE PS_DLC_FOLLOW_LEADER_INIT
				RETURN FALSE
				BREAK

			CASE PS_DLC_FOLLOW_LEADER_COUNTDOWN
				RETURN FALSE
				BREAK
				
			CASE PS_DLC_FOLLOW_LEADER_TAKEOFF
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_UPDATE
						//See if the player is close to the checkpoint yet
						IF PS_GET_CHECKPOINT_PROGRESS() > 1
							RETURN TRUE
						ENDIF
						BREAK
				ENDSWITCH
				BREAK
				
			CASE PS_DLC_FOLLOW_LEADER_OBSTACLE
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_UPDATE
						//See if the player is close to the checkpoint yet
						IF PS_IS_LAST_CHECKPOINT_CLEARED()
							RETURN TRUE
						ENDIF
						BREAK
				ENDSWITCH
				BREAK
		ENDSWITCH
	ENDIF
	//if we reach here, we probably haven't passed the check
	RETURN FALSE	
ENDFUNC

PROC PS_DLC_FOLLOW_LEADER_FORCE_FAIL
	ePSFailReason = PS_FAIL_DEBUG
	PS_DLC_Follow_Leader_State = PS_DLC_FOLLOW_LEADER_FAIL
	PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)

ENDPROC

PROC PS_DLC_FOLLOW_LEADER_FORCE_PASS
	PS_DLC_Follow_Leader_State = PS_DLC_FOLLOW_LEADER_SUCCESS
	PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
ENDPROC

//****************************************************************************************************
//****************************************************************************************************
//	Public functions! (used by Pilot_School.sc to run the challenges)
//****************************************************************************************************
//****************************************************************************************************

PROC PS_DLC_Follow_Leader_Initialise()
	SWITCH g_PS_initStage
		CASE 0
			iPS_PreviewVehicleRecordingID = iPS_PEVIEW_VEH_REC_ID_DLC_FOLLOW_LEADER
			VehicleToUse = LAZER//STUNT
			
			REQUEST_MODEL(S_M_M_PILOT_02)
			REQUEST_VEHICLE_RECORDING(iPS_PreviewVehicleRecordingID, "PilotSchool")
			g_PS_initStage++
			EXIT
		BREAK
		CASE 1
			IF 	HAS_MODEL_LOADED(S_M_M_PILOT_02)
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(iPS_PreviewVehicleRecordingID, "PilotSchool")
			AND PS_SETUP_CHALLENGE()
				
	
				//Start pos and heading are hard-coded since it different for each challenge.
				vStartPosition = <<-1823.5862, -699.6436, 50.0>>		
				vStartRotation = <<-0.0045, 0.0000, -148.4397>>		
				fStartHeading =	-148.4397
				
				bFinishedChallenge = FALSE
				
				iFailFarCounter	= 0
				PS_Special_Fail_Reason = "PS_FAIL_DLC_08"

				PS_DLC_Follow_Leader_Init_Checkpoints()

				
				PS_REQUEST_SHARED_ASSETS()
				PS_DLC_follow_Leader_State = PS_DLC_FOLLOW_LEADER_INIT
				
				
				
				fPreviewVehicleSkipTime = 38000//45000
				fPreviewTime = 23
				
				fCurrentPlaybackSpeed = 1.0
				
				vPilotSchoolSceneCoords = vStartPosition
				IF HAS_VEHICLE_RECORDING_BEEN_LOADED(iPS_PreviewVehicleRecordingID, sPreviewRecordingName)
					vPilotSchoolPreviewCoords = GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(iPS_PreviewVehicleRecordingID, fPreviewVehicleSkipTime, sPreviewRecordingName)
					vPilotSchoolPreviewRot = GET_ROTATION_OF_VEHICLE_RECORDING_AT_TIME(iPS_PreviewVehicleRecordingID, fPreviewVehicleSkipTime, sPreviewRecordingName)
				ENDIF
				
				//offset for checking whether player is in the air or on the ground
				PS_SET_GROUND_OFFSET(2.5)
				g_PS_initStage++				
			ENDIF
			EXIT
		BREAK
		CASE 2
		
		
		
			IF 	PS_CREATE_VEHICLE(TRUE)
			AND PS_CREATE_NET_VEHICLE(PS_ambVeh_NETID[0],VehicleToUse, GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(iPS_PEVIEW_VEH_REC_ID_DLC_FOLLOW_LEADER, 225, sPreviewRecordingName), fStartHeading)
			AND PS_CREATE_NET_PED_IN_VEHICLE(piGhostPilot_NETID, PS_ambVeh_NETID[0], PEDTYPE_MISSION, S_M_M_PILOT_02, VS_DRIVER)
				piGhostPilot = NET_TO_PED(piGhostPilot_NETID)
				PS_Main.myGhostVehicle = NET_TO_VEH(PS_ambVeh_NETID[0])
				bwaitingOnSomething = FALSE
//				SET_WEATHER_FOR_FMMC_MISSION(1) //sunny

				NETWORK_OVERRIDE_CLOCK_TIME(GET_HOUR_FOR_PS_CHALLENGE(),0,0)
				
				SET_VEHICLE_MISSILE_WARNING_ENABLED(PS_Main.myVehicle,FALSE)
				
				SET_VEHICLE_USED_FOR_PILOT_SCHOOL(PS_Main.myGhostVehicle, TRUE)
				SET_VEHICLE_HAS_STRONG_AXLES(PS_Main.myGhostVehicle, TRUE)
				SET_ENTITY_AS_MISSION_ENTITY(PS_Main.myGhostVehicle)
				SET_VEHICLE_NAME_DEBUG(PS_Main.myGhostVehicle, "Leader")
				CONTROL_LANDING_GEAR(PS_Main.myGhostVehicle, LGC_RETRACT_INSTANT)
				FREEZE_ENTITY_POSITION(PS_Main.myGhostVehicle, TRUE)
				SET_ENTITY_NO_COLLISION_ENTITY(PS_Main.myGhostVehicle, PS_Main.myVehicle, FALSE)
				
				SET_ENTITY_INVINCIBLE(piGhostPilot, TRUE)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(piGhostPilot, TRUE)
				
				#IF IS_DEBUG_BUILD
					PS_DLC_FOLLOW_LEADER_CREATE_DEBUG_WIDGETS()
					DEBUG_MESSAGE("******************Setting up PS_DLC_Follow_Leader.sc******************")
				#ENDIF
				g_PS_initStage=0
			ELSE
				EXIT
			ENDIF
		BREAK
	ENDSWITCH

ENDPROC

FUNC BOOL PS_DLC_Follow_Leader_MainSetup()
	PS_DLC_Follow_Leader_Init_Checkpoints()
	SET_MINIMAP_COMPONENT(MINIMAP_COMPONENT_RUNWAY_1, FALSE)
	NETWORK_OVERRIDE_CLOCK_TIME(GET_HOUR_FOR_PS_CHALLENGE(),0,0)
	fCurrentPlaybackSpeed = 1.0
	
	// Preventing planes from being generated in the two boarding locations, where planes normally pop into view as the challenge starts.
	//VECTOR vGenBlockMin = <<-1514.927490,-3203.546387,26.694818>> - << 200.0, 200.0, 20.0 >>
	//VECTOR VGenBlockMax = <<-1514.927490,-3203.546387,26.694818>> + << 200.0, 200.0, 20.0 >>
	//SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(vGenBlockMin, VGenBlockMax, FALSE)
	BLOCK_SCENARIOS_AND_AMBIENT(sb_PS)
	
	NEW_LOAD_SCENE_START(<<-1788.26, -757.16, 51.83>>, << 0.52, -0.85, -0.01 >>, 5000.0)
	RETURN TRUE
ENDFUNC

FUNC BOOL PS_DLC_Follow_Leader_Preview()
		PS_PREVIEW_UPDATE_CUTSCENE()
		SWITCH(PS_PreviewState)
			CASE PS_PREVIEW_SETUP
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": PS_PREVIEW_SETUP.")
				PS_PREVIEW_SETUP_PLAYER_FOR_CUTSCENE()
				IF NOT IS_VEHICLE_FUCKED(PS_Main.myVehicle)
					START_PLAYBACK_RECORDED_VEHICLE(PS_Main.myVehicle, iPS_PreviewVehicleRecordingID, "PilotSchool")
					SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(PS_Main.myVehicle, fPreviewVehicleSkipTime)
				ENDIF
				PS_ACTIVATE_CHECKPOINT_MGR(PS_LESSON_OBSTACLE_PLANE, 4, TRUE)
				PS_PREVIEW_DIALOGUE_PLAY("PS_FLPREV", "PS_FLPREV_1", "PS_FLPREV_2", "PS_FLPREV_3", "PS_FLPREV_4")
				
				IF IS_VEHICLE_DRIVEABLE(PS_Main.myGhostVehicle)
					START_PLAYBACK_RECORDED_VEHICLE(PS_Main.myGhostVehicle, iPS_PreviewVehicleRecordingID, "PilotSchool")
					SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(PS_Main.myGhostVehicle, fPreviewVehicleSkipTime)
					SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(PS_Main.myGhostVehicle, 750.0)
					FREEZE_ENTITY_POSITION(PS_Main.myGhostVehicle, FALSE)
				ENDIF
				
				BREAK
				
			CASE PS_PREVIEW_PLAYING
				//let vehicle recording play
				BREAK
				
			CASE PS_PREVIEW_CLEANUP
				PS_PREVIEW_CLEANUP_CUTSCENE()
				BREAK
		ENDSWITCH
		RETURN TRUE
	
ENDFUNC



PROC PS_CONVERGE_VALUE(FLOAT &val, FLOAT fDesiredVal, FLOAT fAmountToConverge, BOOL bAdjustForFramerate = FALSE)
	IF val != fDesiredVal
		FLOAT fConvergeAmountThisFrame = fAmountToConverge
		IF bAdjustForFramerate
			fConvergeAmountThisFrame = 0.0 +@ (fAmountToConverge * 30.0)
		ENDIF
	
		IF val - fDesiredVal > fConvergeAmountThisFrame
			val -= fConvergeAmountThisFrame
		ELIF val - fDesiredVal < -fConvergeAmountThisFrame
			val += fConvergeAmountThisFrame
		ELSE
			val = fDesiredVal
		ENDIF
	ENDIF
ENDPROC

BOOL bCanStopCheckingCollision 
// 2469393 disable leader collision as it passes through dodgy lampost
PROC TURN_LEADER_VEH_COLLISION_OFF_BRIEFLY(VEHICLE_INDEX viVeh)
	IF NOT bCanStopCheckingCollision
				
		IF DOES_ENTITY_EXIST(viVeh)
		AND NOT IS_ENTITY_DEAD(viVeh)
			
			VECTOR vPos = <<-348.6747, -2238.6663, 6.6082>>
			
			IF IS_ENTITY_AT_COORD(viVeh, vPos, <<20, 20, 20>>, TRUE)
				IF NOT GET_ENTITY_COLLISION_DISABLED(viVeh)
					SET_ENTITY_COLLISION(viVeh, FALSE)
					PRINTLN("TURN_LEADER_VEH_COLLISION_OFF_BRIEFLY, FALSE ")
				ENDIF
			ELSE
				IF GET_ENTITY_COLLISION_DISABLED(viVeh)
					SET_ENTITY_COLLISION(viVeh, TRUE)
					PRINTLN("TURN_LEADER_VEH_COLLISION_OFF_BRIEFLY, TRUE ")
					bCanStopCheckingCollision = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PS_DLC_FOLLOW_LEADER_UPDATE_LEADER_PLAYBACK()

	cprintln(debug_trevor3,"PS_DLC_FOLLOW_LEADER_UPDATE_LEADER_PLAYBACK(): start")
	IF 	IS_VEHICLE_DRIVEABLE(PS_Main.myVehicle)
	AND IS_VEHICLE_DRIVEABLE(PS_Main.myGhostVehicle)
	
		//Force the local machine to have control if not spectator
		IF NOT IS_PLAYER_SCTV(PLAYER_ID())
			IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(PS_Main.myGhostVehicle)
				NETWORK_REQUEST_CONTROL_OF_ENTITY(PS_Main.myGhostVehicle)
			ENDIF
		ENDIF
	
		cprintln(debug_trevor3,"PS_DLC_FOLLOW_LEADER_UPDATE_LEADER_PLAYBACK(): vehicles driveable")
		
		IF IS_PED_IN_VEHICLE(player_ped_id(),PS_Main.myVehicle)
			cprintln(debug_trevor3,"PS_DLC_FOLLOW_LEADER_UPDATE_LEADER_PLAYBACK(): playback ongoingfor ghost")
		ENDIf
		
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(PS_Main.myGhostVehicle)
			cprintln(debug_trevor3,"PS_DLC_FOLLOW_LEADER_UPDATE_LEADER_PLAYBACK(): playback ongoingfor ghost")
			VECTOR 	vPlayerVehiclePosition		= GET_ENTITY_COORDS(PS_Main.myVehicle)
			VECTOR 	vLeaderVehiclePosition		= GET_ENTITY_COORDS(PS_Main.myGhostVehicle)
			VECTOR 	vOffsetFromLeaderVehicle	= GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(PS_Main.myGhostVehicle, vPlayerVehiclePosition)
			
			FLOAT  	fDistance 					= GET_DISTANCE_BETWEEN_COORDS(vLeaderVehiclePosition, vPlayerVehiclePosition, FALSE)
			FLOAT	fPlaybackTime				= GET_TIME_POSITION_IN_RECORDING(PS_Main.myGhostVehicle)
		
			FLOAT	fCloseDistance				= 30.0
			FLOAT	fIdealDistance				= 50.0
			FLOAT	fMaxDistance 				= 80.0
			FLOAT	fSuperMaxDistance 			= 115.0
			FLOAT	fDesiredPlaybackSpeed 		= 1.0
			
			TURN_LEADER_VEH_COLLISION_OFF_BRIEFLY(PS_Main.myGhostVehicle)
			
			IF ( vOffsetFromLeaderVehicle.y > 1.0 ) 	//leader vehicle is behind player vehicle, need to increase recording playback speed
						
				IF ( fDistance > fSuperMaxDistance )
				
					fDesiredPlaybackSpeed = 5.0
				
				ELIF ( fDistance > fMaxDistance )		

					fDesiredPlaybackSpeed = 4.0
					
				ELIF ( fDistance > fIdealDistance )		

					fDesiredPlaybackSpeed = 3.0
				
				ELSE
					
					fDesiredPlaybackSpeed = 2.5
					
				ENDIF
			
			ELSE										//leader vehicle is front of player vehicle, lower recording playback speed when needed
				
				IF  ( fDistance > fSuperMaxDistance )
				
					IF fPlaybackTime < 3000.0
					
						fDesiredPlaybackSpeed = 0.95
						
					ELSE
					
						fDesiredPlaybackSpeed = 0.85
					
					ENDIF
				
				ELIF ( fDistance > fMaxDistance )
				
					fDesiredPlaybackSpeed = 0.8
					
				ELIF ( fDistance > fIdealDistance )		
					
					fDesiredPlaybackSpeed = 1.15
					
				ELIF ( fDistance > fCloseDistance )
				
					fDesiredPlaybackSpeed = 1.35
				
				ELSE									
								
					fDesiredPlaybackSpeed = 1.5
				
				ENDIF
			
			ENDIF
			
			PS_CONVERGE_VALUE(fCurrentPlaybackSpeed, fDesiredPlaybackSpeed, 0.025, TRUE)
			
			fCurrentPlaybackSpeed = CLAMP(fCurrentPlaybackSpeed, 0.0, 2.0)
			
			IF NOT IS_PLAYER_SCTV(PLAYER_ID())
				IF NETWORK_HAS_CONTROL_OF_ENTITY(PS_Main.myGhostVehicle)
					SET_PLAYBACK_SPEED(PS_Main.myGhostVehicle, fCurrentPlaybackSpeed)
				ENDIF
			ENDIF
			
		//	#IF IS_DEBUG_BUILD
		//		DRAW_DEBUG_VEHICLE_RECORDING_INFO(PS_Main.myGhostVehicle, 2.0)
		//		DRAW_DEBUG_TEXT_ABOVE_ENTITY(PS_Main.myGhostVehicle, GET_STRING_FROM_FLOAT(fCurrentPlaybackSpeed), 1.0)
		//		DRAW_DEBUG_TEXT_ABOVE_ENTITY(PS_Main.myGhostVehicle, GET_STRING_FROM_FLOAT(fDistance), 0.0)
		//	#ENDIF
			
		ELSE
			IF NETWORK_DOES_NETWORK_ID_EXIST(PS_ambVeh_NETID[0]) //delte ghost veh
				PS_DELETE_NET_ID(PS_ambVeh_NETID[0])
			ENDIF
		ENDIF
	
	ENDIF

ENDPROC

FUNC BOOL PS_DLC_Follow_Leader_MainUpdate()
	cprintln(debug_trevor3,"PS_DLC_Follow_Leader_MainUpdate()")
	IF TempDebugInput() AND NOT bFinishedChallenge
		cprintln(debug_trevor3,"PS_DLC_Follow_Leader_MainUpdate() : state : ",PS_DLC_Follow_Leader_State)
		#IF IS_DEBUG_BUILD
			PS_DLC_FOLLOW_LEADER_UPDATE_DEBUG_WIDGETS()	
		#ENDIF

		PS_LESSON_UDPATE()

		PS_PLAY_FL_BRIEF()

		SWITCH(PS_DLC_Follow_Leader_State)
		//Spawn player and plane on the runway
			CASE PS_DLC_FOLLOW_LEADER_INIT
				//updating autopilot to start the player out in the air
				PS_AUTO_PILOT(PS_Main, 70.0, FALSE, vStartRotation.x, vStartRotation.y)
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						
						//create leader plane and start recording playback
						IF 	PS_CREATE_NET_VEHICLE(PS_ambVeh_NETID[0],VehicleToUse, vStartPosition+<<0,0,2>>, fStartHeading)
						AND PS_CREATE_NET_PED_IN_VEHICLE(piGhostPilot_NETID, PS_ambVeh_NETID[0], PEDTYPE_MISSION, S_M_M_PILOT_02, VS_DRIVER)
						
							piGhostPilot = NET_TO_PED(piGhostPilot_NETID)
							PS_Main.myGhostVehicle = NET_TO_VEH(PS_ambVeh_NETID[0])
							
							IF IS_VEHICLE_DRIVEABLE(PS_Main.myGhostVehicle)
								SET_VEHICLE_USED_FOR_PILOT_SCHOOL(PS_Main.myGhostVehicle, TRUE)
								SET_VEHICLE_HAS_STRONG_AXLES(PS_Main.myGhostVehicle, TRUE)
								SET_ENTITY_AS_MISSION_ENTITY(PS_Main.myGhostVehicle)
								SET_VEHICLE_NAME_DEBUG(PS_Main.myGhostVehicle, "Leader")
								CONTROL_LANDING_GEAR(PS_Main.myGhostVehicle, LGC_RETRACT_INSTANT)
								FREEZE_ENTITY_POSITION(PS_Main.myGhostVehicle, TRUE)
								SET_ENTITY_NO_COLLISION_ENTITY(PS_Main.myGhostVehicle, PS_Main.myVehicle, FALSE)
							ENDIF

							IF IS_VEHICLE_DRIVEABLE(PS_Main.myGhostVehicle)
								STOP_PLAYBACK_RECORDED_VEHICLE(PS_Main.myGhostVehicle)
								FREEZE_ENTITY_POSITION(PS_Main.myGhostVehicle, FALSE)
								SET_VEHICLE_ENGINE_ON(PS_Main.myGhostVehicle, TRUE, TRUE)
								SET_HELI_BLADES_FULL_SPEED(PS_Main.myGhostVehicle)
								START_PLAYBACK_RECORDED_VEHICLE(PS_Main.myGhostVehicle, iPS_PEVIEW_VEH_REC_ID_DLC_FOLLOW_LEADER, "PilotSchool")
								SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(PS_Main.myGhostVehicle, 225)//550)
								FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(PS_Main.myGhostVehicle)
								SET_ENTITY_NO_COLLISION_ENTITY(PS_Main.myGhostVehicle, PS_Main.myVehicle, FALSE)															
								
								IF DOES_ENTITY_EXIST(piGhostPilot)
								AND NOT IS_ENTITY_DEAD(piGhostPilot)
								AND NOT IS_PED_INJURED(piGhostPilot)
									SET_ENTITY_INVINCIBLE(piGhostPilot, TRUE)
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(piGhostPilot, TRUE)
								ENDIF
								
								MODIFY_VEHICLE_TOP_SPEED(PS_Main.myVehicle, -25) //slow down player plane //normally -25, for recording -45
								CONTROL_LANDING_GEAR(PS_Main.myVehicle, LGC_RETRACT_INSTANT)
								SET_VEHICLE_ENGINE_ON(PS_Main.myVehicle, TRUE, TRUE)
															
							ENDIF
							
							IF IS_VEHICLE_DRIVEABLE(PS_Main.myVehicle)
								SET_VEHICLE_MISSILE_WARNING_ENABLED(PS_Main.myVehicle,FALSE)
							ENDIF
							
							PS_INCREMENT_SUBSTATE()
							
						ENDIF
		
						BREAK
					CASE PS_SUBSTATE_UPDATE
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_EXIT
						//SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
						DO_NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
						PS_DLC_Follow_Leader_State = PS_DLC_FOLLOW_LEADER_COUNTDOWN
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK

			CASE PS_DLC_FOLLOW_LEADER_COUNTDOWN
				PS_DLC_FOLLOW_LEADER_UPDATE_LEADER_PLAYBACK()
				//updating autopilot to start the player out in the air
				PS_AUTO_PILOT(PS_Main, 70.0, FALSE, vStartRotation.x, vStartRotation.y)
				PS_DISABLE_VEHICLE_ATTACKS()
				
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						TASK_VEHICLE_TEMP_ACTION(PLAYER_PED_ID(), PS_Main.myVehicle, TEMPACT_REV_ENGINE, 5500)
						SET_ENTITY_NO_COLLISION_ENTITY(PS_Main.myGhostVehicle, PS_Main.myVehicle, TRUE)	//reactivate collisions between planes
						SET_ENTITY_NO_COLLISION_ENTITY(PS_Main.myVehicle, PS_Main.myGhostVehicle, TRUE)
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF	PS_HUD_UPDATE_COUNTDOWN(FALSE,FALSE)
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						//SET_PLAYER_CONTROL(PLAYER_ID(), TRUE) 
						DO_NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)	
						//SET_PLAYER_INVINCIBLE(PLAYER_ID(),FALSE)
						PS_TRIGGER_MUSIC(PS_MUSIC_COLLECT_START)
						PS_DLC_Follow_Leader_State = PS_DLC_FOLLOW_LEADER_TAKEOFF
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
				
		//Give player control and the first instruction to take off
		//Check if player is staying on the runway and not driving off
			CASE PS_DLC_FOLLOW_LEADER_TAKEOFF
			
				PS_DLC_FOLLOW_LEADER_UPDATE_LEADER_PLAYBACK()
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_TRIGGER_FL_BRIEF(FL_BRIEF_GO)
						PS_SET_CHECKPOINT_COUNTER_ACTIVE(TRUE)
						//SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						DO_NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)	
						//SET_PLAYER_INVINCIBLE(PLAYER_ID(),FALSE)
						PS_ACTIVATE_CHECKPOINT_MGR(PS_LESSON_OBSTACLE_PLANE)
						//PS_PLAY_DISPATCHER_INSTRUCTION_2_LINES("PS_FLEADER", "PS_FLEADER_1", "PS_FLEADER", "PPS_FLEADER_2")
						//PS_HUD_RESET_TIMER()
						//PS_HUD_SET_GUTTER_ICON_ACTIVE(TRUE)
						//PS_SET_TIMER_ACTIVE(TRUE)
						iFailFarCounter	= 0
						PS_SET_HINT_CAM_ACTIVE(TRUE, PS_HINT_FROM_INSTRUCTOR)
						PS_SET_HINT_CAM_ENTITY(PS_Main.myGhostVehicle)
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF PS_DLC_Follow_Leader_Fail_Check()
							PS_DLC_Follow_Leader_State = PS_DLC_FOLLOW_LEADER_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELIF PS_DLC_Follow_Leader_Progress_Check()				
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						PS_DLC_Follow_Leader_State = PS_DLC_FOLLOW_LEADER_OBSTACLE
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
	
			//Obstacle course loop
			CASE PS_DLC_FOLLOW_LEADER_OBSTACLE
				PS_DLC_FOLLOW_LEADER_UPDATE_LEADER_PLAYBACK()
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PRINT_HELP("PS_HELP_GATE")	//"~g~green gate ~s~- Knife flight~n~"
													//"~b~blue gate ~s~- Inverted flight"			
						PS_HUD_RESET_TIMER()
						PS_HUD_SET_GUTTER_ICON_ACTIVE(TRUE)
						PS_SET_TIMER_ACTIVE(TRUE)				
 				    	PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF PS_DLC_Follow_Leader_Fail_Check()
							cprintln(debug_trevor3,"FAIL COND 1")
							PS_DLC_Follow_Leader_State = PS_DLC_FOLLOW_LEADER_FAIL
							
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELIF PS_DLC_Follow_Leader_Progress_Check()
						
							//check if we need to play fail dialogue for missing to many gates
							//use the same fail check condition as in pilot_school_mp_data.sch
							IF GetPilotSchoolScore_3_checkpointsPassed(PS_Main.myChallengeData, PS_Main.myPlayerData) < 70
								IF FL_currentBrief = FL_BRIEF_COMMENTS
									PS_PLAY_DISPATCHER_INSTRUCTION("PS_FLfail", "PS_FLfail_2")
									FL_currentBrief = FL_BRIEF_NONE //use the brief enum as flag to only play this once
								ENDIF
							ENDIF
						
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								PS_INCREMENT_SUBSTATE()
							ENDIF
						ELSE
							//PS_DLC_Follow_Leader_Update_Dialogue()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
					
						PS_HUD_SET_GUTTER_ICON_ACTIVE(FALSE)
						PS_HUD_SET_OBJECTIVE_METER_VISIBLE(FALSE)
						PS_SET_TIMER_ACTIVE(FALSE)
					
						PS_UPDATE_RECORDS(VECTOR_ZERO)
						
						IF Pilot_School_Data_Check_Mission_Success(PS_Main.myChallengeData, PS_Main.myPlayerData)
							PS_DLC_Follow_Leader_State = PS_DLC_FOLLOW_LEADER_SUCCESS
						ELSE
							cprintln(debug_trevor3,"FAIL COND 2")
							PS_DLC_Follow_Leader_State = PS_DLC_FOLLOW_LEADER_FAIL
						ENDIF
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
												
			CASE PS_DLC_FOLLOW_LEADER_SUCCESS
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_TRIGGER_MUSIC(PS_MUSIC_COLLECT_STOP)
						PS_UPDATE_RECORDS(VECTOR_ZERO)
						PS_PLAY_LESSON_FINISHED_LINE("PS_FLb", "PS_FLb_3", "PS_FLb_2", "PS_FLb_1", "")
						PS_LESSON_END(TRUE)
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF NOT IS_PLAYER_DEAD(player_id())							
						OR GET_MANUAL_RESPAWN_STATE()= MRS_READY_FOR_PLACEMENT
							IF NOT PS_HUD_IS_SCORECARD_ACTIVE()
								PS_INCREMENT_SUBSTATE()
							ENDIF
						ELSE
							PRINTLN("PLAYER IS DEAD SETTING RESPAWN STATE")
							//Set the player to respawn state if dead and doesn't have respawn state set
							IF IS_PLAYER_DEAD(player_id())
							AND GET_MANUAL_RESPAWN_STATE() = MRS_NULL
								SET_MANUAL_RESPAWN_STATE(MRS_RESPAWN_PLAYER_HIDDEN)
							ENDIF
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						bFinishedChallenge = TRUE
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
				
			CASE PS_DLC_FOLLOW_LEADER_FAIL				
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_TRIGGER_MUSIC(PS_MUSIC_COLLECT_FAIL)
						cprintln(debug_trevor3,"PS_DLC_FOLLOW_LEADER_FAIL : PS_SUBSTATE_ENTER")
						PS_UPDATE_RECORDS(VECTOR_ZERO)
						PS_PLAY_LESSON_FINISHED_LINE("PS_fail", "", "", "", "PS_fail_1")
						PS_LESSON_END(FALSE)
						
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF NOT PS_HUD_IS_SCORECARD_ACTIVE()
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						cprintln(debug_trevor3,"PS_DLC_FOLLOW_LEADER_FAIL : PS_SUBSTATE_EXIT")
						bFinishedChallenge = TRUE
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
		ENDSWITCH
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC PS_DLC_Follow_Leader_MainCleanup()
//hud
	PS_HUD_SET_GUTTER_ICON_ACTIVE(FALSE)
	PS_HUD_SET_OBJECTIVE_METER_VISIBLE(FALSE)
	PS_SET_TIMER_ACTIVE(FALSE)
	PS_KILL_HINT_CAM()
	PS_HUD_CLEANUP()
	CLEAR_PRINTS()
	CLEAR_HELP()

//player/objects
	PS_SEND_PLAYER_BACK_TO_MENU()

//cleanup checkpoints
	PS_RESET_CHECKPOINT_MGR()
	
//assets
	REMOVE_VEHICLE_RECORDING(iPS_PreviewVehicleRecordingID, "PilotSchool")
	SET_MODEL_AS_NO_LONGER_NEEDED(S_M_M_PILOT_02)
	SET_MODEL_AS_NO_LONGER_NEEDED(VehicleToUse)
		
	IF NETWORK_DOES_NETWORK_ID_EXIST(piGhostPilot_NETID)
		PS_DELETE_NET_ID(piGhostPilot_NETID)		
	ENDIF
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(PS_ambVeh_NETID[0])
		PS_DELETE_NET_ID(PS_ambVeh_NETID[0])
	ENDIF
	
	// Allow the blocked planes from being generated in the two boarding locations.
	//VECTOR vGenBlockMin = <<-1514.927490,-3203.546387,26.694818>> - << 200.0, 200.0, 20.0 >>
	//VECTOR VGenBlockMax = <<-1514.927490,-3203.546387,26.694818>> + << 200.0, 200.0, 20.0 >>
	//SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(vGenBlockMin, VGenBlockMax, TRUE)
	UNBLOCK_SCENARIOS_AND_AMBIENT(sb_PS)
	
	#IF IS_DEBUG_BUILD
		DEBUG_MESSAGE("******************Cleaning up PS_DLC_Follow_Leader.sc******************")
	#ENDIF
ENDPROC

// EOF
//****************************************************************************************************
//****************************************************************************************************

