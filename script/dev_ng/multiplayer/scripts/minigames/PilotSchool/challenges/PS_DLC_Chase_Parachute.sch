//****************************************************************************************************
//
//	Author: 		Lukasz Bogaj / Mark Beagan
//	Date: 			28/11/13
//	Description:	"Chase Parachute" Challenge for Pilot School. The player is required to do jump out
//					of a cargo plane without a parachute, catch a falling parachute mid air and then land.
//****************************************************************************************************

USING "minigames_helpers.sch"
USING "Pilot_School_MP_Challenge_Helpers.sch"

//****************************************************************************************************
//****************************************************************************************************
//	Private functions that are used only by Chase Parachute
//****************************************************************************************************
//****************************************************************************************************

//Number of created ambient vehicles
CONST_INT cpNUM_AMB_VEH 2

BLIP_INDEX		ParachutePackBlipIndex
OBJECT_INDEX 	ParachutePackIndex


TWEAK_INT	iInterpToGameCamTime		1100
TWEAK_INT	iInterpBetweenCamsTime 		3500 //2500
TWEAK_FLOAT	fJumpCamRelativePitch		-35.0
TWEAK_FLOAT	fInterpToGameCamStartTime	0.875


INT			iParachutePackProgress		= 0
FLOAT 		fParachutePackFallValue 	= 4.5
VECTOR 		vParachutePackOffset 		= << 0.0, -22.0, -10.0 >>
VECTOR		vParachutePreviewOffset		= <<20.0, 0.0, -10.0>>
VECTOR 		vParachutePackRotation 		= << 45.0, 0.0, 180.0 >>

FLOAT		fVerticalVelocityDifference	= 0.0
VECTOR 		vDesiredParachutePackOffset	= << 0.0, 0.0, 0.0>>
VECTOR		vCurrentParachutePackOffset	= << 0.0, 0.0, 0.0 >>

//Used to be debug but are used in release scripts. Moved here so this compiles in release. -BenR
VECTOR		vCameraOffset 				= << -0.1, -7.750, 1.850>>
FLOAT		fValue2 					= -33
FLOAT		fValue3 					= -115.000//-88.515

INT			iParaSound
INT 		iWindSound

BOOL		bDeployed 					= FALSE
BOOL		bJumped						= FALSE

//Struct for ambient vehicles
STRUCT PSDLCCPVEHICLE
	VEHICLE_INDEX efveh
	MODEL_NAMES efmn
	VECTOR vStart
	FLOAT fStart
ENDSTRUCT

//Array for ambient vehicles
PSDLCCPVEHICLE cpvehs[cpNUM_AMB_VEH]

PROC SET_PARACHUTE_STATE(bool give, int iHere)
	iHere=iHere
	if give = true
		cprintln(debug_Trevor3,"Gave parachute: ",iHere)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		AND NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), GADGETTYPE_PARACHUTE)
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), GADGETTYPE_PARACHUTE, 1, TRUE, FALSE)
			SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_HAND, 3, 0)
		ELSE
			IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), GADGETTYPE_PARACHUTE)
				SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_HAND, 3, 0)
			ENDIF
		ENDIF
	else
		cprintln(debug_Trevor3,"Remove parachute: ",iHere)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), GADGETTYPE_PARACHUTE)
			
				cprintln(debug_Trevor3,"parachute removed: ",iHere)
				
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED)
				REMOVE_WEAPON_FROM_PED(PLAYER_PED_ID(), GADGETTYPE_PARACHUTE)
				SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_HAND, 0, 0)
				
				IF NETWORK_IS_GAME_IN_PROGRESS()
					INT iBagIndex = GET_MP_INT_CHARACTER_STAT(MP_STAT_PARACHUTE_CURRENT_PACK)
					SET_PLAYER_PARACHUTE_VARIATION_OVERRIDE(PLAYER_ID(), PED_COMP_HAND, GET_PARACHUTE_DRAWABLE_FOR_MP(PLAYER_PED_ID(), iBagIndex), GET_PARACHUTE_TEXTURE_FOR_MP(PLAYER_PED_ID(), iBagIndex))
					cprintln(debug_Trevor3,"SET_PLAYER_PARACHUTE_VARIATION_OVERRIDE: MP ",iBagIndex)
				ELSE
					SET_PLAYER_PARACHUTE_VARIATION_OVERRIDE(PLAYER_ID(), PED_COMP_HAND,0)
					cprintln(debug_Trevor3,"SET_PLAYER_PARACHUTE_VARIATION_OVERRIDE: SP ")
				ENDIF
				
				SET_PLAYER_PARACHUTE_MODEL_OVERRIDE(PLAYER_ID(), 0)
			ENDIF
		ENDIF
	endif
ENDPROC

#IF IS_DEBUG_BUILD

	//debug widgets variables
	BOOL 		bUpdateCameraPosition
	BOOL 		bUpdateParapackPosition
	FLOAT		vCameraFOV = 26.0
	VECTOR		vCameraRotation = << -6.0, 0.0, -81.500 >>
	FLOAT		fValue1 = -97.4239
	
	VECTOR 		vParapackAttachmentRotation 	= << 45, 0.0, 180.0 >>

	INT			iObjectTint, iPlayerTint
	BOOL		bUpdateObjectTint, bUpdatePlayerTint

	PROC PS_DLC_CHASE_PARACHUTE_CREATE_DEBUG_WIDGETS()

			IF DOES_WIDGET_GROUP_EXIST(wDataFile)
				SET_CURRENT_WIDGET_GROUP(wDataFile)
					START_WIDGET_GROUP("Skydive Look Camera")
						ADD_WIDGET_BOOL("bUpdateCameraPosition", bUpdateCameraPosition)
						ADD_WIDGET_FLOAT_SLIDER("vCameraFOV", vCameraFOV, 0, 90, 0.1)
						ADD_WIDGET_FLOAT_SLIDER("fValue1", fValue1, -360, 360, 0.1)
						ADD_WIDGET_FLOAT_SLIDER("fValue2", fValue2, -360, 360, 0.1)
						ADD_WIDGET_FLOAT_SLIDER("fValue3", fValue3, -360, 360, 0.1)
						ADD_WIDGET_VECTOR_SLIDER("vCameraOffset", vCameraOffset, -20, 20, 0.1)
						ADD_WIDGET_VECTOR_SLIDER("vCameraRotation", vCameraRotation, -360, 360, 0.1)
					STOP_WIDGET_GROUP()
					START_WIDGET_GROUP("Skydive Jump Camera")
						ADD_WIDGET_INT_SLIDER("iInterpBetweenCamsTime", iInterpBetweenCamsTime, 0, 10000, 1)
						ADD_WIDGET_INT_SLIDER("iInterpToGameCamTime", iInterpToGameCamTime, 0, 10000, 1)
						ADD_WIDGET_FLOAT_SLIDER("fJumpCamRelativePitch", fJumpCamRelativePitch, -180.0, 180.0, 1)
						ADD_WIDGET_FLOAT_SLIDER("fInterpToGameCamStartTime", fInterpToGameCamStartTime, 0.0, 3.0, 0.1)
					STOP_WIDGET_GROUP()
					START_WIDGET_GROUP("Parapack")
						ADD_WIDGET_BOOL("bUpdateParapackPosition", bUpdateParapackPosition)
						ADD_WIDGET_FLOAT_SLIDER("fParachutePackFallValue", fParachutePackFallValue, 0.0, 100.0, 0.1)
						ADD_WIDGET_VECTOR_SLIDER("vParachutePackOffset", vParachutePackOffset, -2000, 2000, 0.1)
						ADD_WIDGET_VECTOR_SLIDER("vParapackAttachmentRotation", vParapackAttachmentRotation, -360, 360, 0.1)
					STOP_WIDGET_GROUP()
					START_WIDGET_GROUP("Parachute Colours")
						ADD_WIDGET_INT_SLIDER("Object Tint", iObjectTint, 0, 16, 1)
						ADD_WIDGET_BOOL("bUpdateObjectTint", bUpdateObjectTint)
						ADD_WIDGET_INT_SLIDER("Player Tint", iPlayerTint, 0, 16, 1)
						ADD_WIDGET_BOOL("bUpdatePlayerTint", bUpdatePlayerTint)
					STOP_WIDGET_GROUP()
				CLEAR_CURRENT_WIDGET_GROUP(wDataFile)
			ENDIF
		
	ENDPROC

	PROC PS_DLC_CHASE_PARACHUTE_UPDATE_DEBUG_WIDGETS()

		IF ( bUpdateCameraPosition = TRUE )
		
			VECTOR vBaseRotCopy
		
			IF DOES_CAM_EXIST(PS_Main.previewCam1)
				DESTROY_CAM(PS_Main.previewCam1)
			ENDIF
			
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
		
			PS_Main.myJumpCam.vLookCameraRot = vCameraRotation
			PS_Main.myJumpCam.vFocalOffset = 1.2 * <<SIN(fValue1 + fStartHeading), -COS(fValue1 + fStartHeading), 0.0>>		
			PS_Main.myJumpCam.vFocalPoint = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PS_Main.myVehicle, vCameraOffset)
			VECTOR vCurFocalOffset = ROTATE_VECTOR_ABOUT_Z(PS_Main.myJumpCam.vFocalOffset, PS_Main.myJumpCam.vLookCameraRot.z)
			PS_Main.myJumpCam.vBaseLookCamRot = <<fValue2 + PS_Main.myJumpCam.vLookCameraRot.x, 0.0, fValue3 + fStartHeading>>
			PS_Main.myJumpCam.fBaseLookCamFOV = vCameraFOV
			
			vBaseRotCopy = PS_Main.myJumpCam.vBaseLookCamRot
			vBaseRotCopy.z += PS_Main.myJumpCam.vLookCameraRot.z

			PS_Main.previewCam1 = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED,
				PS_Main.myJumpCam.vFocalPoint + vCurFocalOffset, vBaseRotCopy, PS_Main.myJumpCam.fBaseLookCamFOV, TRUE)
			SET_CAM_NEAR_CLIP(PS_Main.previewCam1, PS_GET_NEAR_CLIP())
			//SHAKE_SCRIPT_GLOBAL("SKY_DIVING_SHAKE", 0.15)
			
			PS_Main.myJumpCam.vCameraVelocity = <<0,0,0>>
			
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			
			bUpdateCameraPosition = FALSE
			
		ENDIF
		
		IF ( bUpdateParapackPosition = TRUE )
		
			IF DOES_ENTITY_EXIST(ParachutePackIndex)
				IF NOT IS_ENTITY_DEAD(ParachutePackIndex)
					DETACH_ENTITY(ParachutePackIndex, FALSE)
					ATTACH_ENTITY_TO_ENTITY(ParachutePackIndex, PS_Main.myVehicle, 0, vParachutePackOffset, vParapackAttachmentRotation)
				ENDIF
			ENDIF
		
			bUpdateParapackPosition = FALSE
			
		ENDIF
		
		IF ( bUpdateObjectTint = TRUE )
			IF DOES_ENTITY_EXIST(ParachutePackIndex) AND NOT IS_ENTITY_DEAD(ParachutePackIndex)
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": GET_OBJECT_TINT_INDEX() is ", GET_OBJECT_TINT_INDEX(ParachutePackIndex), ".")
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": GET_PROP_TINT_INDEX()() is ", GET_PROP_TINT_INDEX(ParachutePackIndex), ".")
				//SET_OBJECT_TINT_INDEX(ParachutePackIndex, iObjectTint)
				SET_PROP_TINT_INDEX(ParachutePackIndex, 2)
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting GET_PROP_TINT_INDEX() and GET_OBJECT_TINT_INDEX() to ", iObjectTint, ".")
			ENDIF
			bUpdateObjectTint = FALSE
		ENDIF
		
		IF ( bUpdatePlayerTint = TRUE )
			IF IS_PLAYER_PLAYING(PLAYER_ID())
				INT iTempPlayerTint
				GET_PLAYER_PARACHUTE_PACK_TINT_INDEX(PLAYER_ID(), iTempPlayerTint)
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": GET_PLAYER_PARACHUTE_PACK_TINT_INDEX() is ", iTempPlayerTint, ".")
				SET_PLAYER_PARACHUTE_PACK_TINT_INDEX(PLAYER_ID(), iPlayerTint)
			ENDIF
			bUpdatePlayerTint = FALSE
		ENDIF

	ENDPROC

#ENDIF

//Initialise the ambient vehicles
PROC PS_DLC_Chase_Para_Initialise_Vehs()
	cpvehs[0].efmn = barracks
	cpvehs[0].vStart = <<-1728.2251, 165.8578, 63.3711>>
	cpvehs[0].fStart = 328.4241
	
	cpvehs[1].efmn = barracks
	cpvehs[1].vStart = <<-1745.4448, 154.5525, 63.3711>>
	cpvehs[1].fStart = 100.7084
ENDPROC

//Create the vehicles
FUNC BOOL PS_DLC_Chase_Para_Create_Vehs()


	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(PS_ambVeh_NETID[0])
		IF NOT PS_CREATE_NET_VEHICLE(PS_ambVeh_NETID[0],cpvehs[0].efmn, cpvehs[0].vStart, cpvehs[0].fStart)
			RETURN FALSE
		ELSE
			PS_ambVeh[0] = NET_TO_VEH(PS_ambVeh_NETID[0])
		ENDIF
	ENDIF
	
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(PS_ambVeh_NETID[1])
		IF NOT PS_CREATE_NET_VEHICLE(PS_ambVeh_NETID[1],cpvehs[1].efmn, cpvehs[1].vStart, cpvehs[1].fStart)
			RETURN FALSE
		ELSE
			PS_ambVeh[1] = NET_TO_VEH(PS_ambVeh_NETID[1])
		ENDIF
	ENDIF
	RETURN TRUE
ENDFUNC

//Delete the ambient vehicles
PROC PS_DLC_Chase_Para_Delete_Vehs()

INT icount

	FOR icount = 0 to cpNUM_AMB_VEH - 1
		IF NETWORK_DOES_NETWORK_ID_EXIST(PS_ambVeh_NETID[icount])
			PS_DELETE_NET_ID(PS_ambVeh_NETID[icount])						
		ENDIF
	ENDFOR
ENDPROC

/// PURPOSE:
///    Sets the parachute landing target checkpoint.
PROC PS_DLC_Chase_Parachute_Init_Checkpoints()  
	PS_RESET_CHECKPOINT_MGR()
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_LANDING, <<-1744.80, 170.50, 63.8>>)
ENDPROC

FUNC BOOL PS_DLC_Chase_Parachute_Fail_Check()
	IF PS_GET_SUBSTATE() = PS_SUBSTATE_UPDATE
		cprintln(debug_trevor3,"PS_DLC_Chase_Parachute_Fail_Check()")
		IF IS_PLAYER_DEAD(player_id())
			ePSFailReason = PS_FAIL_DEAD
			cprintln(debug_trevor3,"Fail for being dead")
			PRINTLN("FAIL DEAD")
			RETURN TRUE
		ENDIF
		
		IF IS_ENTITY_IN_WATER(PLAYER_PED_ID())
			
			ePSFailReason = PS_FAIL_SUBMERGED
				
			
			RETURN TRUE
		ENDIF
		
	
		
		SWITCH(PS_DLC_Chase_Parachute_State)
		
			CASE PS_DLC_CHASE_PARACHUTE_LOOKING
				IF IS_TIMER_STARTED(PS_Main.myJumpCam.camTimer)
					PRINTLN("CHASE PARA TIMER IS CURRENTLY AT: ", GET_TIMER_IN_SECONDS(PS_Main.myJumpCam.camTimer))
					IF GET_TIMER_IN_SECONDS(PS_Main.myJumpCam.camTimer) > 30
						RETURN TRUE
					ENDIF
				ENDIF
			BREAK
			
			CASE PS_DLC_CHASE_PARACHUTE_SKYDIVING
				//Check for player in water 1728604
				PRINTLN("CHASE PARA - CHECKING IF PLAYER IS IN WATER")
				//PS_UPDATE_FAIL_CHECKS(PS_Main, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE)
				IF IS_ENTITY_IN_WATER(PLAYER_PED_ID())
					cprintln(debug_trevor3,"Fail 2 for being in water")
					PRINTLN("PLAYER IS IN WATER SHOULD FAIL!!")
					
					ePSFailReason = PS_FAIL_SUBMERGED
					RETURN TRUE
				ENDIF
				
				IF DOES_ENTITY_EXIST(ParachutePackIndex)
					IF NOT IS_ENTITY_DEAD(ParachutePackIndex)
					
						//Set the parachute pack to have collision again so it lands
						IF GET_ENTITY_HEIGHT_ABOVE_GROUND(ParachutePackIndex) < 20.0
							SET_ENTITY_COLLISION(ParachutePackIndex, TRUE, TRUE)
						ENDIF
					
						vector vPara
						vPara = GET_ENTITY_COORDS(ParachutePackIndex)
						IF vPara.z < 70.0
							IF GET_ENTITY_HEIGHT_ABOVE_GROUND(ParachutePackIndex) < 1.0
							AND GET_ENTITY_HEIGHT_ABOVE_GROUND(PLAYER_PED_ID()) > 50
								
								ePSFailReason = PS_FAIL_PARACHUTE_HIT_GROUND
								RETURN TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE PS_DLC_CHASE_PARACHUTE_LANDING
				//Check for player in water 1728604
				PRINTLN("CHASE PARA - CHECKING IF PLAYER IS IN WATER 2")
				//PS_UPDATE_FAIL_CHECKS(PS_Main, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE)
				IF IS_ENTITY_IN_WATER(PLAYER_PED_ID()) 					
				AND NOT IS_PED_INJURED(player_ped_id())
					cprintln(debug_trevor3,"Fail 3 for being in water")
					//SET_PLAYER_INVINCIBLE(PLAYER_ID(), TRUE)
						PRINTLN("PLAYER IS IN WATER SHOULD FAIL 2!!")
					
						ePSFailReason = PS_FAIL_SUBMERGED
						RETURN TRUE
				
			
				ENDIF
			BREAK
			
			DEFAULT
				//PS_UPDATE_FAIL_CHECKS(PS_Main, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE)
			BREAK
		ENDSWITCH

	ENDIF
	//If we get here, we havent failed yet
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Check if the player has landed near the target checkpoint, and how close.
FUNC PS_DLC_CHASE_PARACHUTE_CHALLENGE PS_DLC_CHASE_PARACHUTE_LANDING_CHECK()
	cprintln(debug_trevor3,"PS_DLC_CHASE_PARACHUTE_LANDING_CHECK()")
	FLOAT 	fDist
	VECTOR 	vTargetToPlayer
	
	IF IS_PED_INJURED(PLAYER_PED_ID())
		cprintln(debug_trevor3,"Landing Fail Player injured")
		ePSFailReason = PS_FAIL_DEAD
	
		RETURN PS_DLC_CHASE_PARACHUTE_FAIL
	ENDIF
	
	IF IS_ENTITY_IN_WATER(PLAYER_PED_ID())
		cprintln(debug_trevor3,"Landing Fail Player in water")
		ePSFailReason = PS_FAIL_SUBMERGED
		
		RETURN PS_DLC_CHASE_PARACHUTE_FAIL
	ENDIF
	
	vTargetToPlayer = GET_ENTITY_COORDS(PLAYER_PED_ID()) - PS_GET_LAST_CHECKPOINT()

	IF IS_ENTITY_IN_AIR(PLAYER_PED_ID())
		IF vTargetToPlayer.z > 100.0
			if ePSFailReason <> PS_FAIL_OUT_OF_RANGE
				//ePSFailReason = PS_FAIL_REMOVED_PARACHUTE
			ENDIF
	
			RETURN PS_DLC_CHASE_PARACHUTE_FAIL
		ELSE
			RETURN PS_DLC_CHASE_PARACHUTE_PROCESS_DATA
		ENDIF
	ENDIF
	
	IF vTargetToPlayer.z < -2.0 OR vTargetToPlayer.z > 5.0
		ePSFailReason = PS_FAIL_TOO_FAR
		
		RETURN PS_DLC_CHASE_PARACHUTE_FAIL
	ENDIF
	
	fDist = vTargetToPlayer.x * vTargetToPlayer.x + vTargetToPlayer.y * vTargetToPlayer.y
	IF fDist > PS_Challenges[PSCD_DLC_ChaseParachute].BronzeDistance * PS_Challenges[PSCD_DLC_ChaseParachute].BronzeDistance
		ePSFailReason = PS_FAIL_TOO_FAR
	
		RETURN PS_DLC_CHASE_PARACHUTE_FAIL
	ENDIF
	
	PS_Main.myPlayerData.LandingDistance = SQRT(fDist)
	
	IF PS_Main.myPlayerData.LandingDistance > PS_Challenges[PSCD_DLC_ChaseParachute].SilverDistance
		iChallengeScores[g_current_selected_dlc_PilotSchool_class] = iSCORE_FOR_BRONZE
	ELIF PS_Main.myPlayerData.LandingDistance > PS_Challenges[PSCD_DLC_ChaseParachute].GoldDistance
		iChallengeScores[g_current_selected_dlc_PilotSchool_class] = iSCORE_FOR_SILVER
	ELSE
		iChallengeScores[g_current_selected_dlc_PilotSchool_class] = iSCORE_FOR_GOLD
	ENDIF
	
	cprintln(debug_trevor3,"Landing player passed")
	RETURN PS_DLC_CHASE_PARACHUTE_SUCCESS
ENDFUNC

FUNC BOOL PS_CP_CREATE_PARACHUTE_PREVIEW()
	IF NOT DOES_ENTITY_EXIST(ParachutePackIndex)
		IF NOT IS_ENTITY_DEAD(PS_Main.prevPed)
			IF NOT PS_CREATE_NET_OBJ(ParachutePackIndex_NETID,P_PARACHUTE_S, GET_ENTITY_COORDS(PS_Main.prevPed))
				RETURN FALSE
			ELSE
				ParachutePackIndex 		= NET_TO_OBJ(ParachutePackIndex_NETID)
				NETWORK_USE_HIGH_PRECISION_BLENDING(ParachutePackIndex_NETID,TRUE)
				SET_ENTITY_INVINCIBLE(ParachutePackIndex, TRUE)
				SET_ENTITY_LOD_DIST(ParachutePackIndex, 500)
				
				SET_ENTITY_COLLISION(ParachutePackIndex, FALSE, TRUE)
				
				SET_ENTITY_NO_COLLISION_ENTITY(ParachutePackIndex, PS_Main.prevPed, FALSE)
				SET_ENTITY_NO_COLLISION_ENTITY(PLAYER_PED_ID(), ParachutePackIndex, FALSE)
				SET_ENTITY_COORDS(ParachutePackIndex, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PS_Main.prevPed, vParachutePreviewOffset))
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_ENTITY_DEAD(PS_Main.prevPed)
			SET_ENTITY_INVINCIBLE(ParachutePackIndex, TRUE)
			SET_ENTITY_LOD_DIST(ParachutePackIndex, 500)
			
			SET_ENTITY_COLLISION(ParachutePackIndex, FALSE, TRUE)
			
			/*SET_ENTITY_NO_COLLISION_ENTITY(ParachutePackIndex, PS_Main.prevPed, FALSE)
			SET_ENTITY_NO_COLLISION_ENTITY(PLAYER_PED_ID(), ParachutePackIndex, FALSE)*/
			SET_ENTITY_COORDS(ParachutePackIndex, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PS_Main.prevPed, vParachutePreviewOffset))
		ENDIF
	ENDIF
	RETURN TRUE
ENDFUNC

PROC PS_CP_DELETE_PARACHUTE()
	IF NETWORK_DOES_NETWORK_ID_EXIST(ParachutePackIndex_NETID)
		PS_DELETE_NET_ID(ParachutePackIndex_NETID)		
	ENDIF
ENDPROC

PROC PS_UPDATE_PLANE_ENGINE()
	IF DOES_ENTITY_EXIST(PS_Main.myVehicle) AND NOT IS_ENTITY_DEAD(PS_Main.myVehicle)
		IF NETWORK_HAS_CONTROL_OF_ENTITY(PS_Main.myVehicle)	
			SET_HELI_BLADES_FULL_SPEED(PS_Main.myVehicle)
		ENDIF
	ENDIF
ENDPROC

PROC PS_DLC_CP_HIDE_PLAYER_PARACHUTE_DRAWABLE_PREVIEW()
	IF NOT IS_PED_INJURED(PS_Main.prevPed)
		SET_PED_COMPONENT_VARIATION(PS_Main.prevPed, PED_COMP_SPECIAL, 2, 0, 0)
	ENDIF
ENDPROC

/// PURPOSE:
///    Hide the player ped parachute drawable (model).
PROC PS_DLC_CHASE_PARACHUTE_HIDE_PLAYER_PARACHUTE_DRAWABLE()
	
/*
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SWITCH GET_CURRENT_PLAYER_PED_ENUM()
			CASE CHAR_MICHAEL
				SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL2, 0, 0, 0)
			BREAK
			CASE CHAR_TREVOR
			CASE CHAR_FRANKLIN
				SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL, 0, 0, 0)
			BREAK
		ENDSWITCH
	ENDIF*/
ENDPROC

/// PURPOSE:
///    Show the player ped parachute drawable (model).
PROC PS_DLC_CHASE_PARACHUTE_SHOW_PLAYER_PARACHUTE_DRAWABLE()
	
	
/*
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SWITCH GET_CURRENT_PLAYER_PED_ENUM()
			CASE CHAR_MICHAEL
				SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL2, 5, 0)
			BREAK
			CASE CHAR_FRANKLIN
				SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL, 1, 0)
			BREAK
			CASE CHAR_TREVOR
				SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL, 3, 0)
			BREAK
		ENDSWITCH
	ENDIF*/
ENDPROC

PROC PS_Chase_Para_Updating_Preview()
	SWITCH (eParachutePreview)
		CASE PS_PARACHUTE_INIT
			//put player in the air and 
			//fPreviewTime = 20
			//RESTART_TIMER_NOW(tParachutePreviewTimer)
			
			IF NOT IS_VEHICLE_FUCKED(PS_Main.myVehicle)
				IF NETWORK_HAS_CONTROL_OF_ENTITY(PS_Main.myVehicle)	
					SET_HELI_BLADES_FULL_SPEED(PS_Main.myVehicle)
					SET_VEHICLE_ENGINE_ON(PS_Main.myVehicle, TRUE, TRUE)
				ENDIF	
			ENDIF
			
			IF NOT IS_VEHICLE_FUCKED(PS_FlatBedTruck)
				vPS_MovingTarget = GET_ENTITY_COORDS(PS_FlatBedTruck)
			ENDIF
			eParachutePreview = PS_PARACHUTE_WAIT
		BREAK

		CASE PS_PARACHUTE_JUMPING
			//wait for a while then pull yor chute?
			/*IF TIMER_DO_WHEN_READY(tParachutePreviewTimer, 5.0)
				IF NOT IS_ENTITY_DEAD(PS_Main.prevPed)
					TASK_PARACHUTE_TO_TARGET(PS_Main.prevPed, vPS_MovingTarget)
				ENDIF*/
				eParachutePreview = PS_PARACHUTE_SKYDIVING
			//ENDIF
		BREAK
		
		CASE PS_PARACHUTE_SKYDIVING
			/*IF TIMER_DO_WHEN_READY(tParachutePreviewTimer, 7.0)
				IF NOT IS_ENTITY_DEAD(PS_Main.prevPed)
					eParachutePreview = PS_PARACHUTE_WAIT
					SET_PED_KEEP_TASK(PS_Main.prevPed, TRUE)
					SET_PARACHUTE_TASK_TARGET(PS_Main.prevPed, vPS_MovingTarget)
				ENDIF
			ENDIF*/
			eParachutePreview = PS_PARACHUTE_WAIT
		BREAK
	
		CASE PS_PARACHUTE_WAIT
			//do nothing
			IF IS_ENTITY_ON_SCREEN(ParachutePackIndex)
				IF NOT IS_GAMEPLAY_HINT_ACTIVE()
					SET_GAMEPLAY_OBJECT_HINT(ParachutePackIndex, VECTOR_ZERO, TRUE, 8000)
				ENDIF
			ENDIF
		BREAK
	
	ENDSWITCH
	
ENDPROC

PROC CONVERGE_VALUE(FLOAT &fCurrentValue, FLOAT fDesiredValue, FLOAT fAmountToConverge, BOOL bAdjustForFramerate = FALSE)
	IF fCurrentValue != fDesiredValue
		FLOAT fConvergeAmountThisFrame = fAmountToConverge
		IF bAdjustForFramerate
			fConvergeAmountThisFrame = 0.0 +@ (fAmountToConverge * 30.0)
		ENDIF
	
		IF fCurrentValue - fDesiredValue > fConvergeAmountThisFrame
			fCurrentValue -= fConvergeAmountThisFrame
		ELIF fCurrentValue - fDesiredValue < -fConvergeAmountThisFrame
			fCurrentValue += fConvergeAmountThisFrame
		ELSE
			fCurrentValue = fDesiredValue
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Update the position of falling parachute object.
PROC PS_UPDATE_PARACHUTE_PACK_POSITION(PED_INDEX pedchute)

	IF DOES_ENTITY_EXIST(ParachutePackIndex)
		IF NOT IS_ENTITY_DEAD(ParachutePackIndex)
			
			If NETWORK_DOES_NETWORK_ID_EXIST(ParachutePackIndex_NETID)
				IF NOT NETWORK_HAS_CONTROL_OF_NETWORK_ID(ParachutePackIndex_NETID)
					IF NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(ParachutePackIndex_NETID)
					ENDIF
				ELSE
					IF GET_DISTANCE_BETWEEN_COORDS(GET_PED_BONE_COORDS(pedchute, BONETAG_ROOT, << 0.0, 0.0, 0.0 >>), GET_ENTITY_COORDS(ParachutePackIndex)) < 2.0
					
						PS_KILL_HINT_CAM()
						REMOVE_BLIP(ParachutePackBlipIndex)
						IF NETWORK_DOES_NETWORK_ID_EXIST(ParachutePackIndex_NETID)
							PS_DELETE_NET_ID(ParachutePackIndex_NETID)		
						ENDIF
						
						IF IS_THIS_PRINT_BEING_DISPLAYED("PS_DLC_O_CP")
							CLEAR_PRINTS()
						ENDIF
						
					ELSE

						vParachutePackRotation.z -= GET_FRAME_TIME() * 300.0
						
						IF NOT IS_ENTITY_DEAD(pedchute)
							IF IS_PED_IN_PARACHUTE_FREE_FALL(pedchute)
								
								SWITCH iParachutePackProgress
									CASE 0
										vDesiredParachutePackOffset =  GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(pedchute, GET_ENTITY_COORDS(ParachutePackIndex))
										iParachutePackProgress++
									BREAK
								ENDSWITCH
								
								INT 		iLeftX, iLeftY, iRightX, iRightY
								FLOAT		fPlayerHeading,	fDesiredHeading
								VECTOR		vPlayerPosition, vPlayerVelocity
								VECTOR 		vParachutePackPosition, vParachutePackVelocity
								
								CONST_FLOAT	MAX_VERTICAL_VELOCITY_DIFFERENCE 4.3053
								
								fPlayerHeading			= GET_ENTITY_HEADING(pedchute)
								vPlayerPosition 		= GET_ENTITY_COORDS(pedchute)
								vPlayerVelocity			= GET_ENTITY_VELOCITY(pedchute)
								vParachutePackPosition	= GET_ENTITY_COORDS(ParachutePackIndex)
								vParachutePackVelocity	= GET_ENTITY_VELOCITY(ParachutePackIndex)
													
								fDesiredHeading 		= -ATAN2(vCurrentParachutePackOffset.x, vCurrentParachutePackOffset.y)
								IF fDesiredHeading < 0.0
									fDesiredHeading += 360.0
								ENDIF
								
								vCurrentParachutePackOffset.x = vParachutePackPosition.x - vPlayerPosition.x
								vCurrentParachutePackOffset.y = vParachutePackPosition.y - vPlayerPosition.y
								
								//convert x and y velocity of player into x and y change in offset, and update the overall desired offset
								IF NOT (ABSF(vDesiredParachutePackOffset.x) < 0.4 AND ABSF(fDesiredHeading - fPlayerHeading) < 30.0)
									vDesiredParachutePackOffset.x -= (vPlayerVelocity.x / 105.0)
								ENDIF
								
								IF NOT (ABSF(vDesiredParachutePackOffset.y) < 0.4 AND ABSF(fDesiredHeading - fPlayerHeading) < 30.0)
									vDesiredParachutePackOffset.y -= (vPlayerVelocity.y / 105.0)
								ENDIF
													
								//update the velocity of the parachute pack so that it travels to the desired offset
								FLOAT f_vel_diff_x = vDesiredParachutePackOffset.x - vCurrentParachutePackOffset.x
								FLOAT f_vel_diff_y = vDesiredParachutePackOffset.y - vCurrentParachutePackOffset.y
								
								vParachutePackVelocity.x = vPlayerVelocity.x + f_vel_diff_x
								vParachutePackVelocity.y = vPlayerVelocity.y + f_vel_diff_y
													
								//check player input during free fall and adjuct parachute pack veritcal speed accordingly
								//this should allow the player to dive and slow down the parachute pack veritcal velocity
								//or to slow down the free fall and and speed up the parachute pack vertical velocity
								GET_CONTROL_VALUE_OF_ANALOGUE_STICKS(iLeftX, iLeftY, iRightX, iRightY)	
								CONVERGE_VALUE(fVerticalVelocityDifference, (TO_FLOAT(iLeftY) / 128.0) * MAX_VERTICAL_VELOCITY_DIFFERENCE, 0.1)
								
								IF vPlayerVelocity.z < -20.0
									vParachutePackVelocity.z = vPlayerVelocity.z - fVerticalVelocityDifference
								ELSE	//initially just let the parachute pack fall faster until player gets to a certain vertical velocity
									vParachutePackVelocity.z = vPlayerVelocity.z - 1.0
								ENDIF
								
								//#IF IS_DEBUG_BUILD
								//	DRAW_DEBUG_TEXT_ABOVE_ENTITY(PLAYER_PED_ID(), GET_STRING_FROM_VECTOR(vPlayerVelocity), 0.0)
								//	DRAW_DEBUG_TEXT_ABOVE_ENTITY(ParachutePackIndex, GET_STRING_FROM_VECTOR(vParachutePackVelocity), 0.0)
								//	DRAW_DEBUG_TEXT_ABOVE_ENTITY(PLAYER_PED_ID(), GET_STRING_FROM_VECTOR(vDesiredParachutePackOffset), 1.0)
								//#ENDIF				
								
								//position and rotate the parachute pack
								SET_ENTITY_ROTATION(ParachutePackIndex, vParachutePackRotation)
								SET_ENTITY_VELOCITY(ParachutePackIndex, vParachutePackVelocity)
								serverBD.vNextCheckpoint = vParachutePackVelocity //need to set here and sync with spectator to avoid jitter of parachute pack for spectator
							ELSE
							
								vParachutePackOffset.y -= GET_FRAME_TIME() * fParachutePackFallValue * 1.25
								vParachutePackOffset.z -= GET_FRAME_TIME() * fParachutePackFallValue
								
								//position the parachute pack relative to the plane
								SET_ENTITY_ROTATION(ParachutePackIndex, vParachutePackRotation)
								SET_ENTITY_COORDS(ParachutePackIndex, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PS_Main.myVehicle, vParachutePackOffset))
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	/*VECTOR vParaCoord
	
	vParaCoord = GET_ENTITY_COORDS(ParachutePackIndex)
	
	PRINTLN("Current Parachute Pack Position: ", vParaCoord)*/
	
ENDPROC

PROC PS_UPDATE_LOOK_CAM_FOR_PLANE(VECTOR vCamOffset, FLOAT fV2, FLOAT fV3)
	VECTOR vRightStick
	INT iLeftX, iLeftY, iRightX, iRightY
	
	ALLOW_ALTERNATIVE_SCRIPT_CONTROLS_LAYOUT(PLAYER_CONTROL)
	ALLOW_ALTERNATIVE_SCRIPT_CONTROLS_LAYOUT(FRONTEND_CONTROL)
	
	GET_ANALOG_STICK_VALUES(iLeftX, iLeftY, iRightX, iRightY, IS_PLAYER_CONTROL_ON(PLAYER_ID()))
	
	vRightStick.x = TO_FLOAT(iRightX) / 128.0
	vRightStick.y = TO_FLOAT(iRightY) / -128.0
	
	IF IS_LOOK_INVERTED()
		vRightStick.y *= -1.0
	ENDIF
	
	// Apply player input forces to the acceleration values.
	PS_Main.myJumpCam.vCameraVelocity.z -= vRightStick.x * GET_FRAME_TIME() * 130.0
	PS_Main.myJumpCam.vCameraVelocity.x += vRightStick.y * GET_FRAME_TIME() * 130.0
	
	// Apply dampening to the acceleration values.
	IF ABSF(PS_Main.myJumpCam.vCameraVelocity.z) > 0.001
		PS_Main.myJumpCam.vCameraVelocity.z -= PS_Main.myJumpCam.vCameraVelocity.z * GET_FRAME_TIME() * 4.0
	ELSE
		PS_Main.myJumpCam.vCameraVelocity.z = 0.0
	ENDIF
	
	IF ABSF(PS_Main.myJumpCam.vCameraVelocity.x) > 0.001
		PS_Main.myJumpCam.vCameraVelocity.x -= PS_Main.myJumpCam.vCameraVelocity.x * GET_FRAME_TIME() * 5.0
	ELSE
		PS_Main.myJumpCam.vCameraVelocity.x = 0.0
	ENDIF
	
	// Update the look offsets based on our velocity.
	PS_Main.myJumpCam.vLookCameraRot.z += PS_Main.myJumpCam.vCameraVelocity.z * GET_FRAME_TIME()
	IF PS_Main.myJumpCam.vLookCameraRot.z > -65 // Range
		PS_Main.myJumpCam.vLookCameraRot.z = -65
		PS_Main.myJumpCam.vCameraVelocity.z = 0.0
	ELIF PS_Main.myJumpCam.vLookCameraRot.z < -95 
		PS_Main.myJumpCam.vLookCameraRot.z = -95
		PS_Main.myJumpCam.vCameraVelocity.z = 0.0
	ENDIF
	
	PS_Main.myJumpCam.vLookCameraRot.x += PS_Main.myJumpCam.vCameraVelocity.x * GET_FRAME_TIME()
	IF PS_Main.myJumpCam.vLookCameraRot.x > 0.5 * 21.6 // Range
		PS_Main.myJumpCam.vLookCameraRot.x = 0.5 * 21.6
		PS_Main.myJumpCam.vCameraVelocity.x = 0.0
	ELIF PS_Main.myJumpCam.vLookCameraRot.x < -0.5 * 21.6
		PS_Main.myJumpCam.vLookCameraRot.x = -0.5 * 21.6
		PS_Main.myJumpCam.vCameraVelocity.x = 0.0
	ENDIF
	
	VECTOR vCurFocalOffset = ROTATE_VECTOR_ABOUT_Z(PS_Main.myJumpCam.vFocalOffset, PS_Main.myJumpCam.vLookCameraRot.z)
	
	PS_Main.myJumpCam.vFocalPoint = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PS_Main.myVehicle, vCamOffset)
	
	SET_CAM_COORD(PS_Main.previewCam1, PS_Main.myJumpCam.vFocalPoint + vCurFocalOffset)
	SET_CAM_ROT(PS_Main.previewCam1, <<fV2 + PS_Main.myJumpCam.vLookCameraRot.x, 0.0, fV3 + fStartHeading + PS_Main.myJumpCam.vLookCameraRot.z>>)
	
	SET_CAM_NEAR_CLIP(PS_Main.previewCam1, PS_GET_NEAR_CLIP())
ENDPROC

PROC PS_SETUP_JUMP_CAM_FROM_PLANE_ATTACHED()	
	IF NOT DOES_CAM_EXIST(PS_Main.previewCam2)
		PS_Main.previewCam2 = CREATE_CAMERA()
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(PS_Main.myVehicle)
		//attach first camera to plane using the current offset from the plane
		
		//VECTOR vCameraCurrentOffset = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(PS_Main.myVehicle, GET_CAM_COORD(PS_Main.previewCam1))
		
		//ATTACH_CAM_TO_ENTITY(PS_Main.previewCam1, PS_Main.myVehicle, (vCameraCurrentOffset))

		ATTACH_CAM_TO_ENTITY(PS_Main.previewCam2, PS_Main.myVehicle, <<-0.0069, -8.4962, 3.2364>>) //<<-0.0069, -8.4962, 3.2364>>
		POINT_CAM_AT_ENTITY(PS_Main.previewCam2, PS_Main.myVehicle, <<0.0899, -9.8005, 0.5365>>) //<<0.0899, -9.8005, 0.5365>>
		
		SET_CAM_FOV(PS_Main.previewCam2, GET_CAM_FOV(PS_Main.previewCam1))
		SET_CAM_NEAR_CLIP(PS_Main.previewCam2, PS_GET_NEAR_CLIP())
	ENDIF
	
	//RENDER_SCRIPT_CAMS(TRUE, FALSE)
	
	//RESTART_TIMER_NOW(PS_Main.myJumpCam.camTimer)
ENDPROC

PROC PS_DLC_CHASE_PARACHUTE_FORCE_FAIL
	ePSFailReason = PS_FAIL_DEBUG
	PS_DLC_Chase_Parachute_State = PS_DLC_CHASE_PARACHUTE_FAIL
	PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
ENDPROC

PROC PS_DLC_CHASE_PARACHUTE_FORCE_PASS
	PS_DLC_Chase_Parachute_State = PS_DLC_CHASE_PARACHUTE_SUCCESS
	PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
ENDPROC

//****************************************************************************************************
//****************************************************************************************************
//	Public functions! (used by Pilot_School.sc to run the challenges)
//****************************************************************************************************
//****************************************************************************************************

PROC PS_DLC_Chase_Parachute_Initialise()
	MODEL_NAMES previewModel = GET_NPC_PED_MODEL(CHAR_DOM)	
	SWITCH g_PS_initStage
		CASE 0
			cprintln(debug_trevor3,"Run PS_DLC_Chase_Parachute_Initialise()")
			PS_Main.myChallengeData.bPrintedChuteHelp = FALSE
			PS_Main.myChallengeData.bPrintedLandingHelp = FALSE
			
			VehicleToUse = TITAN
			SET_PARACHUTE_STATE(false,1)
			iPS_PreviewVehicleRecordingID = iPS_PEVIEW_VEH_REC_ID_DLC_CHASE_PARACHUTE
			REQUEST_MODEL(P_PARACHUTE_S) 
			REQUEST_ANIM_DICT("oddjobs@basejump@ig_15")
			REQUEST_ANIM_DICT("oddjobs@bailbond_mountain")
			REQUEST_MODEL(previewModel)
			REQUEST_MODEL(Barracks)
			REQUEST_VEHICLE_RECORDING(iPS_PreviewVehicleRecordingID, "PilotSchool")
			g_PS_initStage = 1
		BREAK
		CASE 1
			IF HAS_MODEL_LOADED(previewModel)
			AND HAS_MODEL_LOADED(P_PARACHUTE_S)
			AND HAS_MODEL_LOADED(Barracks)
			AND HAS_ANIM_DICT_LOADED("oddjobs@basejump@ig_15")
			AND HAS_ANIM_DICT_LOADED("oddjobs@bailbond_mountain")
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(iPS_PreviewVehicleRecordingID, "PilotSchool")
			AND PS_SETUP_CHALLENGE()
				//Start pos and heading are hard-coded since it different for each challenge.
				vStartPosition = <<-1731.9323, -900.8391, 500.0405>>	
				vStartRotation = <<0.0000, 0.0000, -135.0000>>		
				fStartHeading = -135.0000
				
				//Preview values
				eParachutePreview							= PS_PARACHUTE_INIT
				vPreviewCamCoords = <<-1718.1304, -1058.8328, 1741.9697>>
				vPreviewCamDirection = NORMALISE_VECTOR(<<-1925.6, -2793.5, 614.9>> - vPreviewCamCoords)
				vPilotSchoolPreviewCoords = PS_MENU_CAM_COORDS
				
				bFinishedChallenge = FALSE
				
				PS_SET_GROUND_OFFSET(1.5)
				PS_DLC_Chase_Para_Initialise_Vehs()
				g_PS_initStage = 2
				EXIT
			ENDIF
		BREAK
		
		CASE 2	
			IF NOT DOES_ENTITY_EXIST(PS_Main.prevPed)
				
				IF PS_CREATE_NET_PED(PS_Main.prevPed_netID,PEDTYPE_MISSION, previewModel, <<-1718.1304, -1058.8328, 1741.9697>>,0)
					PS_Main.prevPed = NET_TO_PED(PS_Main.prevPed_netID)
				ENDIF
			ENDIF
			
			IF PS_CREATE_VEHICLE(TRUE)	
			AND PS_DLC_Chase_Para_Create_Vehs()
			AND DOES_ENTITY_EXIST(PS_Main.prevPed)
				
				bwaitingOnSomething = FALSE
	
				PS_Special_Fail_Reason = "PS_FAIL_DLC_02"

				PS_DLC_CHASE_PARACHUTE_INIT_Checkpoints()
				
//				SET_WEATHER_FOR_FMMC_MISSION(1) //sunny

				NETWORK_OVERRIDE_CLOCK_TIME(GET_HOUR_FOR_PS_CHALLENGE(),0,0)
				
				iParaSound = GET_SOUND_ID()
				iWindSound = GET_SOUND_ID()
				
				fPreviewVehicleSkipTime = 10000
				fPreviewTime = 17
				
				vPilotSchoolSceneCoords = vStartPosition
				/*IF HAS_VEHICLE_RECORDING_BEEN_LOADED(iPS_PreviewVehicleRecordingID, sPreviewRecordingName)
					vPilotSchoolPreviewCoords = GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(iPS_PreviewVehicleRecordingID, fPreviewVehicleSkipTime, sPreviewRecordingName)
					vPilotSchoolPreviewRot = GET_ROTATION_OF_VEHICLE_RECORDING_AT_TIME(iPS_PreviewVehicleRecordingID, fPreviewVehicleSkipTime, sPreviewRecordingName)
				ENDIF*/
				
				//offset for checking whether player is in the air or on the ground
				
				
				//SET_HELI_BLADES_FULL_SPEED(PS_Main.myVehicle)
				SET_VEHICLE_ENGINE_ON(PS_Main.myVehicle, TRUE, TRUE)
				SET_VEHICLE_KEEP_ENGINE_ON_WHEN_ABANDONED(PS_Main.myVehicle, TRUE)
				
				
				
				SET_VEHICLE_DOOR_LATCHED(PS_Main.myVehicle, SC_DOOR_BOOT, FALSE, FALSE)
				SET_VEHICLE_DOOR_BROKEN(PS_Main.myVehicle, SC_DOOR_BOOT, TRUE)
				
				SET_PARACHUTE_STATE(true,6)
				
				
				IF NOT IS_PED_INJURED(PS_Main.prevPed )
					SET_ENTITY_HEADING(PS_Main.prevPed, 239.6148)
					//SET_PED_GRAVITY(PS_Main.prevPed, TRUE)
					CLEAR_PED_TASKS_IMMEDIATELY(PS_Main.prevPed)
					TASK_SKY_DIVE(PS_Main.prevPed)
				//	WHILE (NOT IS_ENTITY_DEAD(PS_Main.prevPed)) AND GET_PED_PARACHUTE_STATE(PS_Main.prevPed) <> PPS_SKYDIVING AND TIMERA() < 3000
				//		WAITDEBUG(0)
				//	ENDWHILE
				ENDIF
				
				iParachutePackProgress			= 0
				iPSDialogueCounter				= 1
				fParachutePackFallValue 		= 4.0
				vParachutePackOffset 			= << 0.0, -22.0, -10.0 >>
				vParachutePackRotation 			= << 45.0, 0.0, 180.0 >>
				vCurrentParachutePackOffset		= << 0.0, 0.0, 0.0 >>
				
				g_PS_initStage = 0
				
				#IF IS_DEBUG_BUILD
					vParapackAttachmentRotation = << 45.0, 0.0, 180.0 >>
					PS_DLC_CHASE_PARACHUTE_CREATE_DEBUG_WIDGETS()
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Running PS_DLC_Chase_Parachute_Initialise().")
				#ENDIF
			ELSE
				EXIT
			ENDIF
		BREAK
	ENDSWITCH
	
	
ENDPROC

FUNC BOOL PS_DLC_Chase_Parachute_MainSetup()
	
	//setup the parachute pack object
	IF NOT DOES_ENTITY_EXIST(ParachutePackIndex)
	
		IF PS_CREATE_NET_OBJ(ParachutePackIndex_NETID,P_PARACHUTE_S, vStartPosition)			
			ParachutePackIndex 		= NET_TO_OBJ(ParachutePackIndex_NETID)
			NETWORK_USE_HIGH_PRECISION_BLENDING(ParachutePackIndex_NETID,TRUE)
			ParachutePackBlipIndex	= CREATE_BLIP_FOR_OBJECT(ParachutePackIndex)
			BEGIN_TEXT_COMMAND_SET_BLIP_NAME("PS_BLIP_PARA")
			END_TEXT_COMMAND_SET_BLIP_NAME(ParachutePackBlipIndex)
			SET_ENTITY_INVINCIBLE(ParachutePackIndex, TRUE)
			SET_ENTITY_LOD_DIST(ParachutePackIndex, 500)
			
			SET_ENTITY_COLLISION(ParachutePackIndex, FALSE, TRUE)
			
			/*SET_ENTITY_NO_COLLISION_ENTITY(ParachutePackIndex, PLAYER_PED_ID(), FALSE)
			SET_ENTITY_NO_COLLISION_ENTITY(PLAYER_PED_ID(), ParachutePackIndex, FALSE)*/
			IF DOES_ENTITY_EXIST(PS_Main.myVehicle)
				SET_ENTITY_COORDS(ParachutePackIndex, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PS_Main.myVehicle, vParachutePackOffset))
			ENDIF
		ELSE
			RETURN FALSE
		ENDIF
	ELSE
		ParachutePackBlipIndex	= CREATE_BLIP_FOR_OBJECT(ParachutePackIndex)
		BEGIN_TEXT_COMMAND_SET_BLIP_NAME("PS_BLIP_PARA")
		END_TEXT_COMMAND_SET_BLIP_NAME(ParachutePackBlipIndex)
		SET_ENTITY_INVINCIBLE(ParachutePackIndex, TRUE)
		SET_ENTITY_LOD_DIST(ParachutePackIndex, 500)
		
		SET_ENTITY_COLLISION(ParachutePackIndex, FALSE, TRUE)
		
		/*SET_ENTITY_NO_COLLISION_ENTITY(ParachutePackIndex, PLAYER_PED_ID(), FALSE)
		SET_ENTITY_NO_COLLISION_ENTITY(PLAYER_PED_ID(), ParachutePackIndex, FALSE)*/
		SET_ENTITY_COORDS(ParachutePackIndex, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PS_Main.myVehicle, vParachutePackOffset))
	ENDIF
	
	PS_DLC_CHASE_PARACHUTE_INIT_Checkpoints()
	SET_MINIMAP_COMPONENT(MINIMAP_COMPONENT_RUNWAY_1, FALSE)
	
	// Preventing planes from being generated in the two boarding locations, where planes normally pop into view as the challenge starts.
	//VECTOR vGenBlockMin = <<-1514.927490,-3203.546387,26.694818>> - << 200.0, 200.0, 20.0 >>
	//VECTOR VGenBlockMax = <<-1514.927490,-3203.546387,26.694818>> + << 200.0, 200.0, 20.0 >>
	//SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(vGenBlockMin, VGenBlockMax, FALSE)
	BLOCK_SCENARIOS_AND_AMBIENT(sb_PS)
	
	#IF IS_DEBUG_BUILD
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Running PS_DLC_Chase_Parachute_MainSetup().")
	#ENDIF
	
	//setup player standing in the back of the plane
	
	VECTOR vBaseRotCopy
	VECTOR vCurFocalOffset
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(PS_Main.prevPed_netID)
		PS_DELETE_NET_ID(PS_Main.prevPed_netID)
	ENDIF
	
	//setup the plane
	IF IS_VEHICLE_DRIVEABLE(PS_Main.myVehicle)
	
		//position the plane
		SET_VEHICLE_FIXED(PS_Main.myVehicle)
		SET_ENTITY_COORDS(PS_Main.myVehicle, vStartPosition)
		SET_ENTITY_HEADING(PS_Main.myVehicle, fStartHeading)
		FREEZE_ENTITY_POSITION(PS_Main.myVehicle, TRUE)
		
		//start plane engine
		IF NETWORK_HAS_CONTROL_OF_ENTITY(PS_Main.myVehicle)				
			SET_HELI_BLADES_FULL_SPEED(PS_Main.myVehicle)
		ENDIF
		SET_VEHICLE_ENGINE_ON(PS_Main.myVehicle, TRUE, TRUE)
		
		SET_ENTITY_AS_MISSION_ENTITY(PS_Main.myVehicle)
		SET_VEHICLE_HAS_STRONG_AXLES(PS_Main.myVehicle, TRUE)
		SET_VEHICLE_USED_FOR_PILOT_SCHOOL(PS_Main.myVehicle, TRUE)
		
		//open the back door of the plane
		SET_VEHICLE_DOOR_LATCHED(PS_Main.myVehicle, SC_DOOR_BOOT, FALSE, FALSE)
		SET_VEHICLE_DOOR_BROKEN(PS_Main.myVehicle, SC_DOOR_BOOT, TRUE)
		
		//start vehicle recording playback for plane
		FREEZE_ENTITY_POSITION(PS_Main.myVehicle, FALSE)
		START_PLAYBACK_RECORDED_VEHICLE(PS_Main.myVehicle, iPS_PEVIEW_VEH_REC_ID_DLC_CHASE_PARACHUTE, "PilotSchool")
		SET_POSITION_OFFSET_FOR_RECORDED_VEHICLE_PLAYBACK(PS_Main.myVehicle, << 0.0, 0.0, 1250.0 >>)
		SET_PLAYBACK_SPEED(PS_Main.myVehicle, 0.1)
		FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(PS_Main.myVehicle)
		
		PLAY_SOUND_FROM_ENTITY(iWindSound,"Plane_Wind", PS_Main.myVehicle, "DLC_Pilot_Chase_Parachute_Sounds")
	ENDIF
	
	//setup the player
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
		SET_ENTITY_COORDS(PLAYER_PED_ID(), <<68.6678, -2865.9265, 342.8610>>)
		SET_ENTITY_HEADING(PLAYER_PED_ID(), 342.8610)
	ENDIF
	
	//setup synchronized scene with player in the back of the plane
	IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iSceneID)
		//iSceneID = CREATE_SYNCHRONIZED_SCENE(<<0,0,0>>, <<0,0,0>>)
		iSceneID = NETWORK_CREATE_SYNCHRONISED_SCENE(<< 0.2, -7.9, 0.4 >>, << 0.0, 0.0, 180.0>>, DEFAULT, FALSE,TRUE)
		NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), iSceneID, "oddjobs@bailbond_mountain", "base_jump_idle", INSTANT_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_LOOP_WITHIN_SCENE | SYNCED_SCENE_USE_PHYSICS)
		NETWORK_ATTACH_SYNCHRONISED_SCENE_TO_ENTITY(iSceneID, PS_Main.myVehicle, 0)
		//ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(iSceneID, PS_Main.myVehicle, 0)
		NETWORK_START_SYNCHRONISED_SCENE(iSceneID)
		//SET_SYNCHRONIZED_SCENE_ORIGIN(iSceneID, << 0.2, -7.9, 0.4 >>, << 0.0, 0.0, 180.0>>)	//offset the scene from the plane root
	//	TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), iSceneID, "oddjobs@bailbond_mountain", "base_jump_idle", INSTANT_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_LOOP_WITHIN_SCENE | SYNCED_SCENE_USE_PHYSICS)
		
		FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
		SET_GAMEPLAY_CAM_RELATIVE_PITCH()
		SET_GAMEPLAY_CAM_RELATIVE_HEADING()
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_HAND, 0, 0)
		ENDIF
		SET_PLAYER_PARACHUTE_VARIATION_OVERRIDE(PLAYER_ID(), PED_COMP_HAND,0)
		SET_PLAYER_PARACHUTE_MODEL_OVERRIDE(PLAYER_ID(), 0)
	ENDIF
	
	
					
	//setup scripted camera looking at the player and the parachute pack and start rendering from this camera
		
	PS_Main.myJumpCam.vLookCameraRot = << -6.0, 0.0, -81.500>>
	PS_Main.myJumpCam.vFocalOffset = 1.2 * <<SIN(-97.4239 + fStartHeading), -COS(-97.4239 + fStartHeading), 0.0>>
	PS_Main.myJumpCam.vFocalPoint = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PS_Main.myVehicle, << -0.500, -7.750, 1.850>>)
	vCurFocalOffset = ROTATE_VECTOR_ABOUT_Z(PS_Main.myJumpCam.vFocalOffset, PS_Main.myJumpCam.vLookCameraRot.z)
	PS_Main.myJumpCam.vBaseLookCamRot = <<-33.0000 + PS_Main.myJumpCam.vLookCameraRot.x, 0.0, -88.515 + fStartHeading>>
	PS_Main.myJumpCam.fBaseLookCamFOV = 38.000
						
	vBaseRotCopy = PS_Main.myJumpCam.vBaseLookCamRot
	vBaseRotCopy.z += PS_Main.myJumpCam.vLookCameraRot.z
	
	IF DOES_CAM_EXIST(PS_Main.previewCam1)
		DESTROY_CAM(PS_Main.previewCam1)
	ENDIF
						
	PS_Main.previewCam1 = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, PS_Main.myJumpCam.vFocalPoint + vCurFocalOffset, vBaseRotCopy, PS_Main.myJumpCam.fBaseLookCamFOV, TRUE)
						
	NETWORK_OVERRIDE_CLOCK_TIME(GET_HOUR_FOR_PS_CHALLENGE(),0,0)
						
	SET_CAM_NEAR_CLIP(PS_Main.previewCam1, PS_GET_NEAR_CLIP())
	SHAKE_SCRIPT_GLOBAL("SKY_DIVING_SHAKE", 0.15)
							
	PS_Main.myJumpCam.vCameraVelocity = <<0,0,0>>
	
	RENDER_SCRIPT_CAMS(TRUE, FALSE)
						
	//end scripted camera setup
	
	
	
	iParachutePackProgress			= 0
	iPSDialogueCounter				= 1
	fParachutePackFallValue 		= 4.0
	vParachutePackOffset 			= << 0.0, -22.0, -10.0 >>
	vParachutePackRotation 			= << 45.0, 0.0, 180.0 >>
	vCurrentParachutePackOffset		= << 0.0, 0.0, 0.0 >>

	//NEW_LOAD_SCENE_START(<<-1618.1404, -2980.8284, 17.4172>>, NORMALISE_VECTOR(<<-1614.2157, -2983.0940, 17.0217>> - <<-1618.1404, -2980.8284, 17.4172>>), 5000.0)
	//NETWORK_START_LOAD_SCENE(<< -1730.03, -904.61, 501.32 >>)
	NEW_LOAD_SCENE_START(<< -1730.03, -904.61, 501.32 >>, << -0.37, 0.68, -0.63 >>, 5000.0)
	RETURN TRUE
ENDFUNC

FUNC BOOL PS_DLC_Chase_Parachute_Preview()
	SET_GAMEPLAY_CAM_FOLLOW_PED_THIS_UPDATE(PS_Main.prevPed)
	PS_PREVIEW_UPDATE_CUTSCENE()
	SWITCH(PS_PreviewState)
		CASE PS_PREVIEW_SETUP
			PS_PREVIEW_SETUP_PLAYER_FOR_CUTSCENE()
			IF NOT PS_CP_CREATE_PARACHUTE_PREVIEW()
				RETURN FALSE
			ELSE
				PS_DLC_CP_HIDE_PLAYER_PARACHUTE_DRAWABLE_PREVIEW()
				PS_PREVIEW_DIALOGUE_PLAY("PS_CPPREV", "PS_CPPREV_1", "PS_CPPREV_2", "PS_CPPREV_3", "PS_CPPREV_4")
			ENDIF
		BREAK
			
		CASE PS_PREVIEW_PLAYING
			PS_UPDATE_PARACHUTE_PACK_POSITION(PS_Main.prevPed)
			PS_Chase_Para_Updating_Preview()
			//let vehicle recording play
		BREAK
			
		CASE PS_PREVIEW_CLEANUP
			PS_CP_DELETE_PARACHUTE()
			PS_PREVIEW_CLEANUP_CUTSCENE()
		BREAK
	ENDSWITCH
		
	
	RETURN TRUE
ENDFUNC

FUNC BOOL PS_DLC_Chase_Parachute_MainUpdate()

	VECTOR vPlayerCoords
	cprintln(debug_trevor3,"Main update")
	IF TempDebugInput() AND NOT bFinishedChallenge
		cprintln(debug_trevor3,"state = ",PS_DLC_Chase_Parachute_State)
		PS_LESSON_UDPATE()
		PS_UPDATE_PLANE_ENGINE()
		PS_UPDATE_PARACHUTE_PACK_POSITION(PLAYER_PED_ID())
		
		#IF IS_DEBUG_BUILD
			PS_DLC_CHASE_PARACHUTE_UPDATE_DEBUG_WIDGETS()
		#ENDIF
		
		//Cleanup sound on being a distance away
		IF DOES_ENTITY_EXIST( PS_Main.myVehicle )
		AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), PS_Main.myVehicle) > 200.0
			IF NOT HAS_SOUND_FINISHED( iWindSound )
				STOP_SOUND( iWindSound )
			ENDIF
		ENDIF
		
		SWITCH(PS_DLC_Chase_Parachute_State)
			CASE PS_DLC_CHASE_PARACHUTE_INIT
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PARACHUTE_DEPLOY)
				PS_UPDATE_LOOK_CAM_FOR_PLANE(vCameraOffset, fValue2, fValue3)
				
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						DISPLAY_HUD(FALSE)
						DISPLAY_RADAR(FALSE)
						//SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
						DO_NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
						cprintln(debug_trevor3,"SET CONTROL OFF K")
						SET_PARACHUTE_STATE(false,1)
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_EXIT
						PS_DLC_Chase_Parachute_State = PS_DLC_CHASE_PARACHUTE_COUNTDOWN
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK

			CASE PS_DLC_CHASE_PARACHUTE_COUNTDOWN
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PARACHUTE_DEPLOY)
				REMOVE_PARTICLE_FX_IN_RANGE(GET_ENTITY_COORDS(PLAYER_PED_ID()),10.0)
				PS_UPDATE_LOOK_CAM_FOR_PLANE(vCameraOffset, fValue2, fValue3)
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF PS_HUD_UPDATE_COUNTDOWN(TRUE, FALSE)
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						PS_DLC_Chase_Parachute_State = PS_DLC_CHASE_PARACHUTE_LOOKING
						PS_TRIGGER_MUSIC(PS_MUSIC_PARACHUTE_START)
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
				
			CASE PS_DLC_CHASE_PARACHUTE_LOOKING
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PARACHUTE_DEPLOY)
				REMOVE_PARTICLE_FX_IN_RANGE(GET_ENTITY_COORDS(PLAYER_PED_ID()),10.0)
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_UPDATE_LOOK_CAM_FOR_PLANE(vCameraOffset, fValue2, fValue3)
						PRINT_HELP("PSCHUTE_1")
						PS_PLAY_DISPATCHER_INSTRUCTION("PS_CP", "PS_CP_1") //"You see the parachute? Jump after it"
						START_TIMER_NOW(PS_Main.myJumpCam.camTimer)
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF 	GET_TIMER_IN_SECONDS(PS_Main.myJumpCam.camTimer) > 0.08
						AND IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_JUMP)												
							//Freeze plane position for jump
							IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(PS_Main.myVehicle)
								STOP_PLAYBACK_RECORDED_VEHICLE(PS_Main.myVehicle)
							ENDIF
							bJumped = TRUE
							FREEZE_ENTITY_POSITION(PS_Main.myVehicle, TRUE)
						
							IF NOT IS_ENTITY_DEAD(PS_Main.myVehicle)
								//iSceneId = CREATE_SYNCHRONIZED_SCENE(<<0,0,0>>, <<0,0,0>>)
								//ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(iSceneId, PS_Main.myVehicle,0)
								//SET_SYNCHRONIZED_SCENE_ORIGIN(iSceneID, << 0.2, -7.9, 0.4 >>, << 0.0, 0.0, 180.0>>)
								//TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), iSceneId, "oddjobs@bailbond_mountain", "base_jump_spot", REALLY_SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_TAG_SYNC_OUT)
								
								iSceneID = NETWORK_CREATE_SYNCHRONISED_SCENE(<< 0.2, -7.9, 0.4 >>, << 0.0, 0.0, 180.0>>, DEFAULT, FALSE,FALSE)
								NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), iSceneID, "oddjobs@bailbond_mountain", "base_jump_spot", REALLY_SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_LOOP_WITHIN_SCENE | SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_TAG_SYNC_OUT)
								NETWORK_ATTACH_SYNCHRONISED_SCENE_TO_ENTITY(iSceneID, PS_Main.myVehicle, 0)		
								NETWORK_START_SYNCHRONISED_SCENE(iSceneID)
								
							ENDIF
							
							RESTART_TIMER_NOW(tmrParachuteJump)
						//	START_TIMER_NOW(tmrParachuteJump)
							
							CLEAR_HELP()
							PS_UPDATE_LOOK_CAM_FOR_PLANE(vCameraOffset, fValue2, fValue3)
							PS_DLC_Chase_Parachute_State = PS_DLC_CHASE_PARACHUTE_JUMPING
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						
							PLAY_SOUND_FROM_COORD(-1,"Jump",GET_ENTITY_COORDS(player_ped_id()),"DLC_Pilot_Chase_Parachute_Sounds")
							
						ELSE
							PS_UPDATE_LOOK_CAM_FOR_PLANE(vCameraOffset, fValue2, fValue3)
						ENDIF
						
						cprintln(debug_trevor3,"Timer = ",GET_TIMER_IN_SECONDS(PS_Main.myJumpCam.camTimer)," counter = ",iPSDialogueCounter)
						
						//If player is still looking after a time
						IF GET_TIMER_IN_SECONDS(PS_Main.myJumpCam.camTimer) > 5 
							IF iPSDialogueCounter = 1		
								IF PS_PLAY_DISPATCHER_INSTRUCTION_CONVERSATION("PS_CPDONT") //PS_CPDONT
									iPSDialogueCounter++									
								ENDIF
							ENDIF
						ENDIF
						
						IF GET_TIMER_IN_SECONDS(PS_Main.myJumpCam.camTimer) > 10 
							IF iPSDialogueCounter = 2		
								IF PS_PLAY_DISPATCHER_INSTRUCTION_CONVERSATION("PS_CPDONT") //PS_CPDONT
									iPSDialogueCounter++									
								ENDIF
							ENDIF
						ENDIF
						
						IF GET_TIMER_IN_SECONDS(PS_Main.myJumpCam.camTimer) > 20 
							IF iPSDialogueCounter = 3		
								IF PS_PLAY_DISPATCHER_INSTRUCTION_CONVERSATION("PS_CPDONT") //PS_CPDONT
									iPSDialogueCounter++									
								ENDIF
							ENDIF
						ENDIF
						
						IF PS_DLC_Chase_Parachute_Fail_Check()
							PS_DLC_Chase_Parachute_State = PS_DLC_CHASE_PARACHUTE_FAIL
							ePSFailReason = PS_FAIL_SPECIAL_REASON
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ENDIF
						
						BREAK
					CASE PS_SUBSTATE_EXIT
						// We should never get here
						iPSDialogueCounter = 1
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
				
			CASE PS_DLC_CHASE_PARACHUTE_JUMPING
				PS_DLC_CHASE_PARACHUTE_HIDE_PLAYER_PARACHUTE_DRAWABLE()
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PARACHUTE_DEPLOY)
				
				int iSyncScene
				iSyncScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(iSceneID)
				
				IF (IS_SYNCHRONIZED_SCENE_RUNNING(iSyncScene) AND GET_SYNCHRONIZED_SCENE_PHASE(iSyncScene) >= 0.95)
				OR (NOT IS_SYNCHRONIZED_SCENE_RUNNING(iSyncScene) AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(),SCRIPT_TASK_SKY_DIVE) != PERFORMING_TASK)
				//IF IS_TIMER_STARTED(tmrParachuteJump)
				//AND GET_TIMER_IN_SECONDS(tmrParachuteJump) > 0.5
					IF GET_SCRIPT_TASK_STATUS(player_ped_id(),SCRIPT_TASK_SKY_DIVE) != PERFORMING_TASK						
						TASK_SKY_DIVE(PLAYER_PED_ID())
						PS_DLC_CHASE_PARACHUTE_HIDE_PLAYER_PARACHUTE_DRAWABLE()
						FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_PARACHUTING, FALSE, FAUS_DEFAULT)
					ENDIF
				ENDIF
				
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_UPDATE_LOOK_CAM_FOR_PLANE(vCameraOffset, fValue2, fValue3)
						PS_SETUP_JUMP_CAM_FROM_PLANE_ATTACHED()
						RESTART_TIMER_NOW(PS_Main.myJumpCam.camTimer)
						SET_CAM_ACTIVE_WITH_INTERP(PS_Main.previewCam2, PS_Main.previewCam1, iInterpBetweenCamsTime, GRAPH_TYPE_SIN_ACCEL_DECEL)  //iInterpBetweenCamsTime
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
						SET_GAMEPLAY_CAM_RELATIVE_PITCH(fJumpCamRelativePitch)
						SET_FOLLOW_CAM_IGNORE_ATTACH_PARENT_MOVEMENT_THIS_UPDATE()
						//IF IS_TIMER_STARTED(tmrParachuteJump)
						//AND GET_TIMER_IN_SECONDS(tmrParachuteJump) > 1.0
						IF IS_SYNCHRONIZED_SCENE_RUNNING(iSyncScene) AND GET_SYNCHRONIZED_SCENE_PHASE(iSyncScene) > fInterpToGameCamStartTime
							RENDER_SCRIPT_CAMS(FALSE, TRUE, iInterpToGameCamTime, FALSE)
							PS_INCREMENT_SUBSTATE()
						ENDIF
						
						BREAK
					CASE PS_SUBSTATE_EXIT
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
						SET_GAMEPLAY_CAM_RELATIVE_PITCH(fJumpCamRelativePitch)
						SET_FOLLOW_CAM_IGNORE_ATTACH_PARENT_MOVEMENT_THIS_UPDATE()
						IF GET_TIMER_IN_SECONDS(PS_Main.myJumpCam.camTimer) > 1.1
							//SET_ENTITY_NO_COLLISION_ENTITY(PLAYER_PED_ID(), PS_Main.myVehicle, FALSE)
							//IF  GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_SKY_DIVE) = PERFORMING_TASK
							IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iSyncScene) AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_SKY_DIVE) = PERFORMING_TASK
								DISPLAY_HUD(TRUE)
								DISPLAY_RADAR(TRUE)
								PS_HUD_RESET_TIMER()
								//STOP_SCRIPT_GLOBAL_SHAKING(TRUE)
								//SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
								DO_NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)	
								//SET_PLAYER_INVINCIBLE(PLAYER_ID(),FALSE)
								
								CANCEL_TIMER(PS_Main.myJumpCam.camTimer)
								//God text and initial help
								//PRINT("PS_DLC_O_CP", DEFAULT_GOD_TEXT_TIME, 1)
								PRINT_HELP("PS_DLC_H_PC")
								
								PS_DLC_Chase_Parachute_State = PS_DLC_CHASE_PARACHUTE_SKYDIVING
								PS_INCREMENT_SUBSTATE()
							ENDIF
						ENDIF
						BREAK
				ENDSWITCH				
			BREAK
				
			CASE PS_DLC_CHASE_PARACHUTE_SKYDIVING
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PARACHUTE_DEPLOY)
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
					//	SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						DO_NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE, DEFAULT, DEFAULT, DEFAULT, TRUE)	
						//SET_PLAYER_INVINCIBLE(PLAYER_ID(),FALSE)
						PS_SET_HINT_CAM_ACTIVE(TRUE, PS_HINT_FROM_SKYDIVING)
						PS_SET_HINT_CAM_ENTITY(ParachutePackIndex)
						//PS_SET_HINT_CAM_COORD(GET_ENTITY_COORDS(ParachutePackIndex))
						iPSDialogueCounter = 1 //reset conversation counter
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF PS_DLC_Chase_Parachute_Fail_Check()
							PS_DLC_Chase_Parachute_State = PS_DLC_CHASE_PARACHUTE_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELSE
							//Dialogue
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND NOT IS_MESSAGE_BEING_DISPLAYED() AND iPSDialogueCounter = 1
								PS_PLAY_DISPATCHER_INSTRUCTION_3_LINES("PS_CP", "PS_CP_2", "PS_CP", "PS_CP_3", "PS_CP", "PS_CP_4")
								iPSDialogueCounter++
							ENDIF
							
							//Dialogue if player drops below chute
							
							cprintln(debug_trevor3,"iPSDialogueCounter = ",iPSDialogueCounter)
							IF iPSDialogueCounter = 2
								cprintln(debug_trevor3,"continue miss parachute text")
								vector vHeightDiff
								IF DOES_ENTITY_EXIST(ParachutePackIndex)
								AND NOT IS_PED_INJURED(player_ped_id())
									cprintln(debug_trevor3,"continue miss parachute text b")
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										cprintln(debug_trevor3,"continue miss parachute text c")
										vHeightDiff = GET_ENTITY_COORDS(player_ped_id()) - GET_ENTITY_COORDS(ParachutePackIndex)
										cprintln(debug_trevor3,"height diff = ",vHeightDiff.z)
										if vHeightDiff.z < -5.0							
											cprintln(debug_trevor3,"play miss parachute line")
											PS_PLAY_DISPATCHER_INSTRUCTION_CONVERSATION("PS_CPpast")
											iPSDialogueCounter++
										endif										
									ENDIF
								ENDIF
							ENDIF

							//Check for player being close to parachute
							IF DOES_ENTITY_EXIST(ParachutePackIndex)
								IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), ParachutePackIndex) < 10
									PRINTLN("CHASE PARA - HINT CAM SHOULD BE DEACTIVATED")
									PS_CLEAR_HINT_CAM_TARGET()
									PS_SET_HINT_CAM_ACTIVE(FALSE, PS_HINT_FROM_SKYDIVING)
									PS_UPDATE_HINT_CAM()
								ELSE
									IF NOT PS_HINT_CAM.bActive
										PS_SET_HINT_CAM_ACTIVE(TRUE, PS_HINT_FROM_SKYDIVING)
										PS_SET_HINT_CAM_ENTITY(ParachutePackIndex)
									ENDIF
								ENDIF
							ELSE
								PRINTLN("CHASE PARA - HINT CAM SHOULD BE DEACTIVATED")
								PS_CLEAR_HINT_CAM_TARGET()
								PS_SET_HINT_CAM_ACTIVE(FALSE, PS_HINT_FROM_SKYDIVING)
								PS_UPDATE_HINT_CAM()
							ENDIF
							
							IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), GADGETTYPE_PARACHUTE)
								cprintln(debug_trevor3,"REMOVE PARACHUTE")
								SET_PARACHUTE_STATE(false,2)
							ENDIF
							
							//Once parachute is collected draw on back and move on
							IF NOT DOES_ENTITY_EXIST(ParachutePackIndex)
								IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
									IF HAS_SOUND_FINISHED(iParaSound)
										PLAY_SOUND_FRONTEND(iParaSound, "Grab_Chute_Foley","DLC_Pilot_Chase_Parachute_Sounds")																			
									ENDIF
									
									SET_PARACHUTE_STATE(true,3)
								
									PS_INCREMENT_SUBSTATE()
									
								ENDIF							
							ENDIF
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT	
						ENABLE_SELECTOR()
						PS_DLC_Chase_Parachute_State = PS_DLC_CHASE_PARACHUTE_LANDING
						STOP_SCRIPT_GLOBAL_SHAKING()
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
			BREAK
				
			CASE PS_DLC_CHASE_PARACHUTE_LANDING
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_NEXT_WEAPON)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_AIM)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2)

				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PRINTLN("CHASE PARA - ENTERED PARACHUTE LANDING")
						PRINT_HELP("PSCHUTE_2")
						PS_SET_CHECKPOINT_COUNTER_ACTIVE(TRUE)
						PS_ACTIVATE_CHECKPOINT_MGR(PS_LESSON_SKYDIVING)
						iPSDialogueCounter = 1 //reset conversation counter
						//PS_PLAY_DISPATCHER_INSTRUCTION_2_LINES("PS_CP", "PS_CP_7", "PS_CP", "PS_CP_8")
						//PS_PLAY_DISPATCHER_INSTRUCTION("PS_POT", "PS_POT_2")//"Aim for the target and deploy your parachute when you start getting closer to the ground."
 				    	PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF PS_DLC_Chase_Parachute_Fail_Check()
							PS_DLC_Chase_Parachute_State = PS_DLC_CHASE_PARACHUTE_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELSE
						
							//Dialogue
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND NOT IS_MESSAGE_BEING_DISPLAYED() AND iPSDialogueCounter = 1
								PS_PLAY_DISPATCHER_INSTRUCTION_2_LINES("PS_CP", "PS_CP_7", "PS_CP", "PS_CP_8")
								iPSDialogueCounter++
							ENDIF
						
							//Once used the parachute force set flag for it being deployed
							IF GET_PED_PARACHUTE_STATE(PLAYER_PED_ID()) = PPS_PARACHUTING
							OR GET_PED_PARACHUTE_STATE(PLAYER_PED_ID()) = PPS_DEPLOYING
								if NOT bDeployed
									bDeployed = TRUE
								ENDIF
							ENDIF
							
							//Block deploying parachute again
							if bDeployed
								DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PARACHUTE_DEPLOY)
							ENDIF
						
							cprintln(debug_trevor3,"Parachute state: ",GET_PED_PARACHUTE_STATE(PLAYER_PED_ID()))
							IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_PARACHUTE_DETACH)
							cprintln(debug_trevor3,"IS_CONTROL_JUST_PRESSED")
							ENDIF
							IF IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_PARACHUTE_DETACH)
							cprintln(debug_trevor3,"IS_DISABLED_CONTROL_JUST_PRESSED")	
							ENDIF
							
						
							IF GET_PED_PARACHUTE_STATE(PLAYER_PED_ID()) = PPS_LANDING
							OR (GET_PED_PARACHUTE_STATE(PLAYER_PED_ID()) = PPS_INVALID)
								DO_NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, FALSE, FALSE, TRUE, TRUE)
								//wait until player comes to a stop before scoring.
								cprintln(debug_trevor3,"landing state = ",GET_PED_PARACHUTE_STATE(PLAYER_PED_ID()))
								IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_PARACHUTE_DETACH)
								OR IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_PARACHUTE_DETACH)
									vector playerCoordsB
									playerCoordsB = GET_ENTITY_COORDS(player_ped_id())
									IF playerCoordsB.z > 70.0									
									OR GET_DISTANCE_BETWEEN_COORDS(playerCoordsB,<<-1744.80, 170.50, 63.8>>,FALSE) > 13.0
										cprintln(debug_trevor3,"Ditched too early B")
										ePSFailReason = PS_FAIL_REMOVED_PARACHUTE
										START_TIMER_NOW(PS_Main.tmrEndDelay)
										SET_PARACHUTE_STATE(false,4)
										PS_INCREMENT_SUBSTATE()
									ENDIF
								ELSE
									IF (GET_PED_PARACHUTE_STATE(PLAYER_PED_ID()) = PPS_LANDING
									OR GET_PED_PARACHUTE_STATE(PLAYER_PED_ID()) = PPS_INVALID)
									AND GET_ENTITY_SPEED(PLAYER_PED_ID()) < 0.01
																		
										CLEAR_PRINTS()
										CLEAR_HELP()
																			
										SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED)
										SET_PARACHUTE_STATE(false,4)
										START_TIMER_NOW(PS_Main.tmrEndDelay)
										PS_UPDATE_PLAYER_DATA(PS_GET_CURRENT_CHECKPOINT())

										//PS_UPDATE_RECORDS(PS_Main.myCheckpointz[0].position)
										PS_INCREMENT_SUBSTATE()
									ENDIF
								ENDIF
							ELSE
								IF GET_PED_PARACHUTE_STATE(PLAYER_PED_ID()) = PPS_INVALID
								OR GET_PED_PARACHUTE_STATE(PLAYER_PED_ID()) = PPS_LANDING
									CLEAR_HELP()
								
								ELIF NOT PS_Main.myChallengeData.bPrintedChuteHelp
									IF GET_PED_PARACHUTE_STATE(PLAYER_PED_ID()) = PPS_DEPLOYING
									OR GET_PED_PARACHUTE_STATE(PLAYER_PED_ID()) = PPS_PARACHUTING
										PS_TRIGGER_MUSIC(PS_MUSIC_PARACHUTE_JUMP)
										CLEAR_HELP()
										PRINT_HELP("PSCHUTE_3")
										PS_Main.myChallengeData.bPrintedChuteHelp = TRUE
									ENDIF
								
								ELIF NOT PS_Main.myChallengeData.bPrintedLandingHelp
									vPlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
									IF vPlayerCoords.z <= 110.0
										CLEAR_HELP()
										PS_Main.myCheckpointz[0].position.z -= 2.0 //fix for building LOD messing with the marker drawing									
										IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
											PRINT_HELP("PSCHUTE_4_KM", DEFAULT_HELP_TEXT_TIME)
										ELSE
											PRINT_HELP("PSCHUTE_4", DEFAULT_HELP_TEXT_TIME)
										ENDIF	
										PS_Main.myChallengeData.bPrintedLandingHelp = TRUE
									ENDIF
								ENDIF
								
								IF DOES_BLIP_EXIST(PS_Main.myCheckpointz[1].blip)
									SET_BLIP_NAME_FROM_TEXT_FILE(PS_Main.myCheckpointz[1].blip, "PS_DLC_CP_TARG")
								ENDIF
								
								IF (GET_PED_PARACHUTE_STATE(PLAYER_PED_ID()) = PPS_PARACHUTING
								OR GET_PED_PARACHUTE_STATE(PLAYER_PED_ID()) = PPS_LANDING)
								AND (IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_PARACHUTE_DETACH)
								OR IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_PARACHUTE_DETACH))
									vector playerCoords
									playerCoords = GET_ENTITY_COORDS(player_ped_id())
									IF playerCoords.z > 70.0									
									OR GET_DISTANCE_BETWEEN_COORDS(playerCoords,<<-1744.80, 170.50, 63.8>>,FALSE) > 13.0
										SET_PARACHUTE_STATE(false,6)
										START_TIMER_NOW(PS_Main.tmrEndDelay)
										PS_INCREMENT_SUBSTATE()
										cprintln(debug_trevor3,"Ditched too early A")
										ePSFailReason = PS_FAIL_REMOVED_PARACHUTE
									ENDIF
								ENDIF
								
								IF PS_IS_PLAYER_OUT_OF_RANGE() AND PS_IS_RANGE_TIMER_UP()
									CLEAR_PRINTS()
									CLEAR_HELP()
									
									SET_PARACHUTE_STATE(false,5)
									START_TIMER_NOW(PS_Main.tmrEndDelay)
									PS_INCREMENT_SUBSTATE()
								
									PRINTLN("FAILED BC PLAYER IS OUT OF RANGE")
									ePSFailReason = PS_FAIL_OUT_OF_RANGE
								ENDIF
								
								
							ENDIF
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						PS_Main.myChallengeData.bPrintedChuteHelp = FALSE
						PS_Main.myChallengeData.bPrintedLandingHelp = FALSE
						PS_DLC_Chase_Parachute_State = PS_DLC_CHASE_PARACHUTE_PROCESS_DATA
						iInstructionCounter = 1
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
			BREAK
				
			CASE PS_DLC_CHASE_PARACHUTE_PROCESS_DATA
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						DO_NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, FALSE, FALSE, TRUE, TRUE)
						PRINTLN("CHASE PARA - ENTERED PROCESS DATA")
 				    	PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE	
						IF GET_TIMER_IN_SECONDS(PS_Main.tmrEndDelay) > 0.25
							CANCEL_TIMER(PS_Main.tmrEndDelay)
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
					
						//SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
						//check if we need to play fail dialogue here before redirecting the flow to fail state
						SWITCH PS_DLC_CHASE_PARACHUTE_LANDING_CHECK()
						
							CASE PS_DLC_CHASE_PARACHUTE_FAIL
							
								IF ( ePSFailReason = PS_FAIL_TOO_FAR )
									IF ( iInstructionCounter = 1 )
										PS_PLAY_DISPATCHER_INSTRUCTION("PS_CPfar", "PS_CPfar_1")
										iInstructionCounter = -1
									ENDIF
								ENDIF
								
							
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								
									PS_DLC_Chase_Parachute_State = PS_DLC_CHASE_PARACHUTE_FAIL
								
									iInstructionCounter = 1
									//vAlphaPosition = PS_GET_CURRENT_CHECKPOINT()
									PS_INCREMENT_SUBSTATE()
								
								ENDIF
							
							BREAK
							
							CASE PS_DLC_CHASE_PARACHUTE_SUCCESS
							
								PS_DLC_Chase_Parachute_State = PS_DLC_CHASE_PARACHUTE_SUCCESS
								
								iInstructionCounter = 1
								//vAlphaPosition = PS_GET_CURRENT_CHECKPOINT()
								
								IF PS_DLC_Chase_Parachute_State = PS_DLC_CHASE_PARACHUTE_SUCCESS
									PS_INITIALISE_CHECKPOINT_MARKER_FLASH(PS_GET_CURRENT_CHECKPOINT())
									PS_PLAY_CHECKPOINT_CHIME(PS_CHECKPOINT_OK)
								ENDIF
								
								PS_INCREMENT_SUBSTATE()
							
							BREAK
						
						ENDSWITCH
					
					
						//PS_DLC_Chase_Parachute_State = PS_DLC_CHASE_PARACHUTE_LANDING_CHECK()						
						//IF PS_DLC_Chase_Parachute_State <> PS_DLC_CHASE_PARACHUTE_PROCESS_DATA
						//	iInstructionCounter = 1
						//	vAlphaPosition = PS_GET_CURRENT_CHECKPOINT()
						//	PS_INCREMENT_SUBSTATE()
						//ENDIF
						BREAK
				ENDSWITCH
			BREAK
				
			CASE PS_DLC_CHASE_PARACHUTE_SUCCESS
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_TRIGGER_MUSIC(PS_MUSIC_PARACHUTE_PASS)
						PRINTLN("CHASE PARA - ENTERED PASS")
						//PS_UPDATE_RECORDS(VECTOR_ZERO)
						PS_UPDATE_RECORDS(PS_GET_CURRENT_CHECKPOINT(),true)
						PS_RESET_CHECKPOINT_MGR(FALSE)
						PS_PLAY_LESSON_FINISHED_LINE("PS_CP", "PS_CP_9", "PS_CP_10", "PS_CP_11", "")
						iPSHelpTextCounter++
						RESTART_TIMER_NOW(tmrLandingAlpha)
						PS_LESSON_END(TRUE)
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						//PS_PARACHUTE_UPDATE_TARGET_WITH_ALPHA(vAlphaPosition, tmrLandingAlpha)
						IF NOT IS_PLAYER_DEAD(player_id())							
						OR GET_MANUAL_RESPAWN_STATE()= MRS_READY_FOR_PLACEMENT
							IF NOT PS_HUD_IS_SCORECARD_ACTIVE()
								PS_INCREMENT_SUBSTATE()
							ENDIF
						ELSE
							PRINTLN("PLAYER IS DEAD SETTING RESPAWN STATE")
							//Set the player to respawn state if dead and doesn't have respawn state set
							IF IS_PLAYER_DEAD(player_id())
							AND GET_MANUAL_RESPAWN_STATE() = MRS_NULL
								SET_MANUAL_RESPAWN_STATE(MRS_RESPAWN_PLAYER_HIDDEN)
							ENDIF
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						//PS_PARACHUTE_UPDATE_TARGET_WITH_ALPHA(vAlphaPosition, tmrLandingAlpha)
						bFinishedChallenge = TRUE
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
			BREAK
				
			CASE PS_DLC_CHASE_PARACHUTE_FAIL
				//Update the plane camera if failing for it falling too far
				IF ePSFailReason = PS_FAIL_SPECIAL_REASON
					PS_UPDATE_LOOK_CAM_FOR_PLANE(vCameraOffset, fValue2, fValue3)
				ENDIF
				//PS_FAIL_TOO_FAR
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_TRIGGER_MUSIC(PS_MUSIC_PARACHUTE_FAIL)
						//PRINTLN("CHASE PARA - ENTERED FAIL", ENUM_TO_INT(ePSFailReason))
						PS_UPDATE_RECORDS(VECTOR_ZERO)
						//no fail lines currently
						//cprintln(debug_trevor3,ePSFailReason)
						//IF ePSFailReason = PS_FAIL_TOO_FAR
						//	PS_PLAY_LESSON_FINISHED_LINE("PS_CPfar", "", "", "", "PS_CPfar_1") 
						//ENDIF
						//PS_PLAY_LESSON_FINISHED_LINE("PS_POT", "PS_POT_5", "PS_POT_6", "PS_POT_7", "PS_POT_8")
						iPSHelpTextCounter++
						RESTART_TIMER_NOW(tmrLandingAlpha)
						PS_LESSON_END(FALSE, bJumped, false)
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						//PS_PARACHUTE_UPDATE_TARGET_WITH_ALPHA(vAlphaPosition, tmrLandingAlpha)
						IF NOT PS_HUD_IS_SCORECARD_ACTIVE()
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						//PS_PARACHUTE_UPDATE_TARGET_WITH_ALPHA(vAlphaPosition, tmrLandingAlpha)
						bFinishedChallenge = TRUE
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
			BREAK
			
		ENDSWITCH
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC PS_DLC_Chase_Parachute_MainCleanup()
//hud
	PS_HUD_SET_GUTTER_ICON_ACTIVE(FALSE)
	PS_HUD_SET_OBJECTIVE_METER_VISIBLE(FALSE)
	PS_SET_TIMER_ACTIVE(FALSE)
	PS_HUD_CLEANUP()
	CLEAR_PRINTS()
	CLEAR_HELP()
	
	//Remove the parachute on cleanup
	SET_PARACHUTE_STATE(false,99)
	
	RESET_CACHED_PARACHUTE_DRAWABLE_FOR_MP_VARIABLES()
	CLEAR_PLAYER_PARACHUTE_VARIATION_OVERRIDE(PLAYER_ID())
	
	//Clear the deployed flag
	bDeployed = FALSE
	
	DISABLE_SELECTOR()
	
	CANCEL_TIMER(PS_Main.myJumpCam.camTimer)
	
	STOP_SCRIPT_GLOBAL_SHAKING()
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(ParachutePackIndex_NETID)
		PS_DELETE_NET_ID(ParachutePackIndex_NETID)		
	ENDIF
	
	IF DOES_BLIP_EXIST(ParachutePackBlipIndex)
		REMOVE_BLIP(ParachutePackBlipIndex)
	ENDIF
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(PS_ambVeh_NETID[0])
		PS_DELETE_NET_ID(PS_ambVeh_NETID[0])
	ENDIF
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(PS_ambVeh_NETID[1])
		PS_DELETE_NET_ID(PS_ambVeh_NETID[1])
	ENDIF
	
	//player/objects
	PS_SEND_PLAYER_BACK_TO_MENU()

	//cleanup checkpoints
	PS_RESET_CHECKPOINT_MGR()
	
	//assets
	REMOVE_VEHICLE_RECORDING(iPS_PreviewVehicleRecordingID, "PilotSchool")
	SET_MODEL_AS_NO_LONGER_NEEDED(VehicleToUse)
	SET_MODEL_AS_NO_LONGER_NEEDED(P_PARACHUTE_S)
	SET_MODEL_AS_NO_LONGER_NEEDED(Barracks)
	
	PS_DLC_Chase_Para_Delete_Vehs()
	
	// Allow the blocked planes from being generated in the two boarding locations.
	//VECTOR vGenBlockMin = <<-1514.927490,-3203.546387,26.694818>> - << 200.0, 200.0, 20.0 >>
	//VECTOR VGenBlockMax = <<-1514.927490,-3203.546387,26.694818>> + << 200.0, 200.0, 20.0 >>
	//SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(vGenBlockMin, VGenBlockMax, TRUE)
	UNBLOCK_SCENARIOS_AND_AMBIENT(sb_PS)
	
	
	#IF IS_DEBUG_BUILD
		DEBUG_MESSAGE("******************Cleaning up PS_Plane_Course.sc******************")
	#ENDIF
ENDPROC

// EOF
//****************************************************************************************************
//****************************************************************************************************
