//****************************************************************************************************
//
//	Author: 		Lukasz Bogaj / Mark Beagan
//	Date: 			25/10/13
//	Description:	"Collect Flags" Challenge for Pilot School. The player is required to collect all
//					checkpoints placed around the map using any order they want for shortest time
//****************************************************************************************************

USING "minigames_helpers.sch"
USING "Pilot_School_MP_Challenge_Helpers.sch"

SCENARIO_BLOCKING_INDEX siCollectF

//****************************************************************************************************
//****************************************************************************************************
//	Private functions that are used only by Collect Flags
//****************************************************************************************************
//****************************************************************************************************

//Used for counting checkpoints
INT iCF_NextCheck = 0
INT iNextCommentTime

#IF IS_DEBUG_BUILD
VECTOR CheckpointPositions[PS_MAX_CHECKPOINTS]
#ENDIF
#IF IS_DEBUG_BUILD
	//debug widgets variables
	BOOL		bPrintCheckpoints
	BOOL 		bUpdateCheckpoints
	CONST_INT 	PS_MAX_FLAG_CHECKPOINTS	33
#ENDIF

#IF IS_DEBUG_BUILD
PROC PS_DLC_COLLECT_FLAGS_CREATE_DEBUG_WIDGETS()

	IF DOES_WIDGET_GROUP_EXIST(wDataFile)
		SET_CURRENT_WIDGET_GROUP(wDataFile)
			START_WIDGET_GROUP("Checkpoints")
			
				INT i
				
				ADD_WIDGET_BOOL("Print Checkpoints", bPrintCheckpoints)
				ADD_WIDGET_BOOL("Update Checkpoints", bUpdateCheckpoints)
			
				REPEAT PS_MAX_FLAG_CHECKPOINTS i
					START_WIDGET_GROUP(GET_STRING_FROM_INT(i))
						CheckpointPositions[i] = PS_Main.myCheckpointz[i].position
						ADD_WIDGET_VECTOR_SLIDER("Position", CheckpointPositions[i], -5000.0, 5000.0, 1.0)
					STOP_WIDGET_GROUP()
				ENDREPEAT
			
			STOP_WIDGET_GROUP()	
		CLEAR_CURRENT_WIDGET_GROUP(wDataFile)
	ENDIF
ENDPROC
#ENDIF

#IF IS_DEBUG_BUILD
PROC PS_DLC_COLLECT_FLAGS_UPDATE_DEBUG_WIDGETS()

	INT i

	IF ( bUpdateCheckpoints = TRUE )
	
		PS_RESET_CHECKPOINT_MGR()
	
		REPEAT PS_MAX_FLAG_CHECKPOINTS i
			IF i = 0
				PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_NULL, CheckpointPositions[i])
			ELSE
				PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, CheckpointPositions[i])
			ENDIF
		ENDREPEAT
	
		PS_ACTIVATE_CHECKPOINT_MGR(PS_LESSON_FLAG_COLLECTION)
		
		bUpdateCheckpoints = FALSE
		
	ENDIF
	
	IF ( bPrintCheckpoints = TRUE )
		
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("PILOT SCHOOL COLLECT FLAGS CHECKPOINTS")
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_NEWLINE_TO_DEBUG_FILE()
		
		REPEAT PS_MAX_FLAG_CHECKPOINTS i
			SAVE_STRING_TO_DEBUG_FILE("PS_REGISTER_CHECKPOINT(")
			IF i = 0
				SAVE_STRING_TO_DEBUG_FILE("PS_CHECKPOINT_NULL, ")
			ELSE
				SAVE_STRING_TO_DEBUG_FILE("PS_CHECKPOINT_FLAG, ")
			ENDIF
			SAVE_VECTOR_TO_DEBUG_FILE(CheckpointPositions[i])
			SAVE_STRING_TO_DEBUG_FILE(")")
			SAVE_NEWLINE_TO_DEBUG_FILE()
		ENDREPEAT
	
		bPrintCheckpoints = FALSE
	
	ENDIF

ENDPROC
#ENDIF

PROC PS_DLC_Collect_Flags_Init_Checkpoints()  
	PS_RESET_CHECKPOINT_MGR()
	
	/*
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_NULL, <<0.0, 0.0, 0.0>>)//NULL	//0
	
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-934.0000, -1276.0000, 33.0000>>)
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-633.0000, -1152.0000, 33.0000>>)
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-409.0000, -1070.0000, 63.0000>>)
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-797.0000, -736.0000, 65.0000>>)
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-669.0000, -931.0000, 51.0000>>)
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-521.0000, -707.0000, 49.0000>>)
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-590.0000, -718.0000, 134.0000>>)
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-273.0000, -956.0000, 149.0000>>)
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-304.0000, -815.0000, 101.0000>>)
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-222.0000, -833.0000, 131.0000>>)
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-447.0000, -892.0000, 61.0000>>)
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-410.0000, -1218.0000, 59.0000>>)
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-144.0000, -594.0000, 214.0000>>)
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-3.0000, -690.0000, 255.0000>>)
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-75.0000, -819.0000, 326.0000>>)
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-171.0000, -1003.0000, 290.0000>>)
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-795.0000, -1190.0000, 58.0000>>)
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-528.0000, -1211.0000, 28.0000>>)
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-282.0000, -1063.0000, 86.0000>>)
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-284.0000, -445.0000, 92.0000>>)
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-144.0000, -770.0000, 37.0000>>)
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-269.0000, -773.0000, 62.0000>>)
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-675.0000, -590.0000, 75.0000>>)
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-73.0000, -875.0000, 44.0000>>)
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<78.0000, -844.0000, 89.0000>>)
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-110.0000, -1051.0000, 63.0000>>)
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-12.0000, -588.0000, 160.0000>>)
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<0.0000, -1001.0000, 104.0000>>)
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-804.0000, -1357.0000, 23.0000>>)
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-949.0000, -1684.0000, 104.0000>>)
	
	*/
	
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_NULL, <<0.0000, 0.0000, 0.0000>>)
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-934.0000, -1276.0000, 33.0000>>)
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-633.0000, -1152.0000, 36.0000>>)
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-409.0000, -1070.0000, 60.0000>>)
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-797.0000, -736.0000, 65.0000>>)
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-669.0000, -931.0000, 55.0000>>)
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-521.0000, -707.0000, 52.5000>>)
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-590.0000, -718.0000, 137.0000>>)
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-292.0000, -963.0000, 151.5000>>)
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-304.0000, -815.0000, 105.0000>>)
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-222.0000, -833.0000, 135.0000>>)
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-447.0000, -892.0000, 63.5000>>)
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-410.0000, -1218.0000, 64.0000>>)
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-144.0000, -594.0000, 220.0000>>)
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-3.0000, -690.0000, 260.0000>>)
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-75.0000, -819.0000, 333.0000>>)
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-171.0000, -1003.0000, 287.0000>>)
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-795.0000, -1190.0000, 60.0000>>)
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-528.0000, -1211.0000, 32.0000>>)
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-282.0000, -1063.0000, 89.0000>>)
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-284.0000, -445.0000, 96.0000>>)
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-144.0000, -770.0000, 51.5000>>)
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-269.0000, -773.0000, 65.0000>>)
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-675.0000, -590.0000, 80.0000>>)
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-73.0000, -875.0000, 50.0000>>)
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<78.0000, -832.0000, 98.0000>>)
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-110.0000, -1051.0000, 60.0000>>)
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-12.0000, -588.0000, 161.0000>>)
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<0.0000, -1001.0000, 106.5000>>)
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-804.0000, -1357.0000, 25.5000>>)
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-949.0000, -1684.0000, 104.0000>>)
	
ENDPROC

//Used for commenting on clearing flags
PROC PS_DLC_Collect_Flags_Update_Dialogue()
	//Print vertical flying help text
	IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED() AND iInstructionCounter = 2
		//PRINT_HELP("PS_DLC_CF_VERT")
		iInstructionCounter++
	ENDIF

	PRINTLN("CURRENT CHECKPOINT PROGRESS : ", PS_GET_CLEARED_CHECKPOINTS(), " iCF_NextCheck VALUE : ", iCF_NextCheck)
	IF PS_IS_CHECKPOINT_MGR_ACTIVE() AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		IF iCF_NextCheck = PS_GET_CLEARED_CHECKPOINTS()
			PRINTLN("SHOULD BE PLAYING RANDOM LINE")
			PS_PLAY_DISPATCHER_INSTRUCTION_CONVERSATION("PS_CFano")
			//Increment for every second checkpoint comment
			iCF_NextCheck = iCF_NextCheck + 2
		ENDIF
	ENDIF
	
	IF iNextCommentTime = 0
		iNextCommentTime = GET_GAME_TIMER() + 30000
	ENDIF
	
	IF GET_GAME_TIMER() > iNextCommentTime
		IF GET_TIMER_IN_SECONDS(PS_Main.tRaceTimer) < 180
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				cprintln(debug_trevor3,"**** PLAY COLLECT FLAGS LINE ****")
				iNextCommentTime = GET_GAME_TIMER() + 30000
				PS_GET_CLEARED_CHECKPOINTS()
				//check progress
				IF IS_TIMER_STARTED(PS_Main.tRaceTimer)
					cprintln(debug_trevor3,"Race Time : ",GET_TIMER_IN_SECONDS(PS_Main.tRaceTimer))
					cprintln(debug_trevor3,"Checkpoints cleared : ",PS_GET_CLEARED_CHECKPOINTS())
					IF PS_GET_CLEARED_CHECKPOINTS() > GET_TIMER_IN_SECONDS(PS_Main.tRaceTimer) / 8.0 //ahead of time
						PS_PLAY_DISPATCHER_INSTRUCTION_CONVERSATION("PS_CFAHEAD")
					ELIF PS_GET_CLEARED_CHECKPOINTS() > GET_TIMER_IN_SECONDS(PS_Main.tRaceTimer) / 10.0	
						PS_PLAY_DISPATCHER_INSTRUCTION_CONVERSATION("PS_CFTRACK")
					ELSE
						PS_PLAY_DISPATCHER_INSTRUCTION_CONVERSATION("PS_CFBEHIND")
					ENDIF
				ENDIF
			ELSe
				iNextCommentTime = GET_GAME_TIMER() + 1000
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Determines what fail conditions we need to check dependent on the 
FUNC BOOL PS_DLC_Collect_Flags_Fail_Check()
	IF PS_GET_SUBSTATE() = PS_SUBSTATE_UPDATE
		SWITCH(PS_DLC_Collect_Flags_State)
			CASE PS_DLC_COLLECT_FLAGS_INIT
				//No fail check to do
				RETURN FALSE
				
			CASE PS_DLC_COLLECT_FLAGS_COUNTDOWN
				//No fail check to do
				RETURN FALSE
				
			CASE PS_DLC_COLLECT_FLAGS_TAKEOFF
				//Do runway check and default checks
				IF PS_UPDATE_FAIL_CHECKS(PS_Main, FALSE, FALSE, TRUE, TRUE, TRUE, FALSE)
				//IF PS_UPDATE_FAIL_CHECKS(PS_Main, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE)
					RETURN TRUE
				ENDIF
				BREAK
			
			DEFAULT
				//Do default checks
				IF PS_UPDATE_FAIL_CHECKS(PS_Main, FALSE, FALSE, TRUE, TRUE, TRUE, FALSE)
				//IF PS_UPDATE_FAIL_CHECKS(PS_Main, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE)
					RETURN TRUE
				ENDIF
				BREAK
		ENDSWITCH
	ENDIF
	//If we get here, we havent failed yet
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks for objective goals completed based on the stage/state we are in of the challenge progression
///    Also updates ChallengeState when we complete an objective 
FUNC BOOL PS_DLC_Collect_Flags_Progress_Check()
	IF NOT IS_PED_INJURED(PLAYER_PED_ID()) AND IS_VEHICLE_DRIVEABLE(PS_Main.myVehicle)
		SWITCH(PS_DLC_Collect_Flags_State)
			CASE PS_DLC_COLLECT_FLAGS_INIT
				RETURN FALSE
				BREAK

			CASE PS_DLC_COLLECT_FLAGS_COUNTDOWN
				RETURN FALSE
			BREAK
				
			CASE PS_DLC_COLLECT_FLAGS_TAKEOFF
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_UPDATE
						//See if the player is close to the checkpoint yet
						IF PS_GET_CLEARED_CHECKPOINTS() = 1
							RETURN TRUE
						ENDIF
						BREAK
				ENDSWITCH
			BREAK
				
			CASE PS_DLC_COLLECT_FLAGS_OBSTACLE
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_UPDATE
						//See if the player is close to the checkpoint yet
						IF PS_ARE_ALL_FLAGS_CLEARED()
							RETURN TRUE
						ENDIF
						BREAK
				ENDSWITCH
			BREAK
		ENDSWITCH
	ENDIF
	//if we reach here, we probably haven't passed the check
	RETURN FALSE	
ENDFUNC

PROC PS_DLC_COLLECT_FLAGS_FORCE_FAIL
	ePSFailReason = PS_FAIL_DEBUG
	PS_DLC_Collect_Flags_State = PS_DLC_COLLECT_FLAGS_FAIL
	PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
ENDPROC

PROC PS_DLC_COLLECT_FLAGS_FORCE_PASS
	PS_DLC_Collect_Flags_State = PS_DLC_COLLECT_FLAGS_SUCCESS
	PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
ENDPROC

//****************************************************************************************************
//****************************************************************************************************
//	Public functions! (used by Pilot_School.sc to run the challenges)
//****************************************************************************************************
//****************************************************************************************************

PROC PS_DLC_Collect_Flags_Initialise()
	cprintln(debug_trevor3,"g_PS_initStage = ",g_PS_initStage)
	SWITCH g_PS_initStage
		CASE 0
			//Start pos and heading are hard-coded since it different for each challenge.
			vStartPosition = <<-1512.0549, -2521.5186, 66.5793>>//<<-1612.7721, -2701.6594, 14.1155>>
			vStartRotation = <<-0.0003, -0.0000, -33.8568>>		//<<8.5764, -0.0001, -31.2354>>		
			fStartHeading = -33.0								//-31.0								
					
			//VehicleToUse = LAZER
			VehicleToUse = besra	
			iNextCommentTime = 0
			
			iPS_PreviewVehicleRecordingID = iPS_PEVIEW_VEH_REC_ID_DLC_COLLECT_FLAGS
			REQUEST_VEHICLE_RECORDING(iPS_PreviewVehicleRecordingID, "PilotSchool")
			g_PS_initStage++
			EXIT
		BREAK
		CASE 1
			IF HAS_VEHICLE_RECORDING_BEEN_LOADED(iPS_PreviewVehicleRecordingID, "PilotSchool")
			AND PS_SETUP_CHALLENGE()
				g_PS_initStage++
			ENDIF
			EXIT
		BREAK
		CASE 2
			IF PS_CREATE_VEHICLE(TRUE)
				bwaitingOnSomething = FALSE
				bFinishedChallenge = FALSE
						
				PS_DLC_Collect_Flags_State = PS_DLC_COLLECT_FLAGS_INIT
				PS_DLC_Collect_Flags_Init_Checkpoints()
				
				fPreviewVehicleSkipTime = 33500
				fPreviewTime = 20
				
				vPilotSchoolSceneCoords = vStartPosition
				IF HAS_VEHICLE_RECORDING_BEEN_LOADED(iPS_PreviewVehicleRecordingID, sPreviewRecordingName)
					vPilotSchoolPreviewCoords = GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(iPS_PreviewVehicleRecordingID, fPreviewVehicleSkipTime, sPreviewRecordingName)
					vPilotSchoolPreviewRot = GET_ROTATION_OF_VEHICLE_RECORDING_AT_TIME(iPS_PreviewVehicleRecordingID, fPreviewVehicleSkipTime, sPreviewRecordingName)
				ENDIF
				
				//offset for checking whether player is in the air or on the ground
				PS_SET_GROUND_OFFSET(1.5)
				
//				SET_WEATHER_FOR_FMMC_MISSION(1) //sunny

				iPopSpehere = ADD_POP_MULTIPLIER_SPHERE(<<0,0,0>>,8000,0.0,0.0,FALSE)

				NETWORK_OVERRIDE_CLOCK_TIME(GET_HOUR_FOR_PS_CHALLENGE(),0,0)
				
				siCollectF = ADD_SCENARIO_BLOCKING_AREA(<<-1259.3563, -1788.7102, 2.9080>>, <<1735.0126, 385.3287, 1054.4216>>)
				
				//Disable homing missile lock
				SET_VEHICLE_ALLOW_HOMING_MISSLE_LOCKON(PS_Main.myVehicle, FALSE)
				g_PS_initStage = 0
				#IF IS_DEBUG_BUILD
					PS_DLC_COLLECT_FLAGS_CREATE_DEBUG_WIDGETS()
					DEBUG_MESSAGE("******************Setting up PS_DLC_CollectFlags.sc******************")
				#ENDIF
			ELSE
				EXIT
			ENDIF
		BREAK
	ENDSWITCH		
ENDPROC

FUNC BOOL PS_DLC_Collect_Flags_MainSetup()
	PS_DLC_Collect_Flags_Init_Checkpoints()
	SET_MINIMAP_COMPONENT(MINIMAP_COMPONENT_RUNWAY_1, FALSE)
	NETWORK_OVERRIDE_CLOCK_TIME(GET_HOUR_FOR_PS_CHALLENGE(),0,0)
	// Preventing planes from being generated in the two boarding locations, where planes normally pop into view as the challenge starts.
	//VECTOR vGenBlockMin = <<-1514.927490,-3203.546387,26.694818>> - << 200.0, 200.0, 20.0 >>
	//VECTOR VGenBlockMax = <<-1514.927490,-3203.546387,26.694818>> + << 200.0, 200.0, 20.0 >>
	//SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(vGenBlockMin, VGenBlockMax, FALSE)
	BLOCK_SCENARIOS_AND_AMBIENT(sb_PS)
	NEW_LOAD_SCENE_START(<<-1618.1404, -2980.8284, 17.4172>>, NORMALISE_VECTOR(<<-1614.2157, -2983.0940, 17.0217>> - <<-1618.1404, -2980.8284, 17.4172>>), 5000.0)
	RETURN TRUE
ENDFUNC

//Main preview function
FUNC BOOL PS_DLC_Collect_Flags_Preview()
	IF PS_PREVIEW_UPDATE_CUTSCENE()
		SWITCH(PS_PreviewState)
			CASE PS_PREVIEW_SETUP
				PS_PREVIEW_SETUP_PLAYER_FOR_CUTSCENE()
				IF NOT IS_VEHICLE_FUCKED(PS_Main.myVehicle)
					START_PLAYBACK_RECORDED_VEHICLE(PS_Main.myVehicle, iPS_PreviewVehicleRecordingID, "PilotSchool")
					SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(PS_Main.myVehicle, fPreviewVehicleSkipTime)
				ENDIF
				iPreviewCheckpointIdx = PS_GET_CLOSEST_PREVIEW_CHECKPOINT(iPS_PreviewVehicleRecordingID, PS_Main.myCheckpointz, fPreviewVehicleSkipTime)
				PS_ACTIVATE_CHECKPOINT_MGR(PS_LESSON_FLAG_COLLECTION, iPreviewCheckpointIdx + 1, TRUE)
				PS_PREVIEW_DIALOGUE_PLAY("PS_CFPREV", "PS_CFPREV_1", "PS_CFPREV_2", "PS_CFPREV_3", "PS_CFPREV_4")
				//"The object of this exercise is to see how many flags out can collect within the allotted time."
				//"Try to plan a route that will take you through as many flags as possible. "
				//"Doubling back to pick up flags will lose you time."
				BREAK
				
			CASE PS_PREVIEW_PLAYING
				//let vehicle recording play
				BREAK
				
			CASE PS_PREVIEW_CLEANUP
				PS_PREVIEW_CLEANUP_CUTSCENE()
				BREAK
		ENDSWITCH
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC



FUNC BOOL PS_DLC_Collect_Flags_MainUpdate()
	IF TempDebugInput() AND NOT bFinishedChallenge
		
		#IF IS_DEBUG_BUILD
			PS_DLC_COLLECT_FLAGS_UPDATE_DEBUG_WIDGETS()
			//PS_DBG_DrawLiteralStringInt("CheckpointCount: ", PS_Main.myPlayerData.CheckpointCount, 3)
			//PS_DBG_DrawLiteralStringInt("ClearedCheckpoints: ",PS_Main.myCheckpointMgr.clearedCheckpoints, 4)
			//PS_DBG_DrawLiteralStringInt("CheckpointProgress: ",PS_GET_CHECKPOINT_PROGRESS(), 5)
			//PS_DBG_DrawLiteralStringInt("TotalCheckpoints: ",PS_GET_TOTAL_FLAGS_TO_COLLECT(), 6)
			//PS_DBG_DrawLiteralStringFloat("BronzeTime: ",PS_Main.myChallengeData.BronzeTime, 7)
			//PS_DBG_DrawLiteralStringFloat("ElapsedTime: ",PS_Main.myPlayerData.ElapsedTime, 8)
		#ENDIF

		PS_LESSON_UDPATE()

		
		

		SWITCH(PS_DLC_Collect_Flags_State)
		//Spawn player and plane on the runway
			CASE PS_DLC_COLLECT_FLAGS_INIT
				//updating autopilot to start the player out in the air
				PS_AUTO_PILOT(PS_Main, 70.0, FALSE, vStartRotation.x, vStartRotation.y)
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": PS_DLC_COLLECT_FLAGS_INIT")
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						//SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
						DO_NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
						IF IS_VEHICLE_DRIVEABLE(PS_Main.myVehicle)
							MODIFY_VEHICLE_TOP_SPEED(PS_Main.myVehicle, -10)
							CONTROL_LANDING_GEAR(PS_Main.myVehicle, LGC_RETRACT_INSTANT)
							SET_VEHICLE_ENGINE_ON(PS_Main.myVehicle, TRUE, TRUE)
							//SET_VEHICLE_FLIGHT_NOZZLE_POSITION(PS_Main.myVehicle, 0)
						ENDIF
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_EXIT
						PS_DLC_Collect_Flags_State = PS_DLC_COLLECT_FLAGS_COUNTDOWN
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
			BREAK

			CASE PS_DLC_COLLECT_FLAGS_COUNTDOWN
				//updating autopilot to start the player out in the air
				PS_AUTO_PILOT(PS_Main, 70.0, FALSE, vStartRotation.x, vStartRotation.y)
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": PS_DLC_COLLECT_FLAGS_COUNTDOWN")
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_SET_CHECKPOINT_COUNTER_ACTIVE(TRUE)
						PS_ACTIVATE_CHECKPOINT_MGR(PS_LESSON_FLAG_COLLECTION)	//display the checkpoints during countdown, see B*1737385
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF	PS_HUD_UPDATE_COUNTDOWN(FALSE, FALSE)
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						//SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						DO_NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)	
						//SET_PLAYER_INVINCIBLE(PLAYER_ID(),FALSE)
						PS_DLC_Collect_Flags_State = PS_DLC_COLLECT_FLAGS_TAKEOFF
						PS_TRIGGER_MUSIC(PS_MUSIC_COLLECT_START)
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
			BREAK
				
		//Give player control and the first instruction to take off
		//Check if player is staying on the runway and not driving off
			CASE PS_DLC_COLLECT_FLAGS_TAKEOFF
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": PS_DLC_COLLECT_FLAGS_TAKEOFF")
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						//SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						DO_NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)	
						//SET_PLAYER_INVINCIBLE(PLAYER_ID(),FALSE)
						iInstructionCounter = 1
						PS_PLAY_DISPATCHER_INSTRUCTION("PS_CF", "PS_CF_1")
						PS_HUD_RESET_TIMER()
						PS_HUD_SET_GUTTER_ICON_ACTIVE(TRUE)
						PS_SET_TIMER_ACTIVE(TRUE)
						PS_KILL_HINT_CAM()
						//PS_SET_HINT_CAM_ACTIVE(TRUE)
						//PS_SET_HINT_CAM_COORD(PS_GET_CURRENT_CHECKPOINT())						
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF PS_DLC_Collect_Flags_Fail_Check()
							PS_DLC_Collect_Flags_State = PS_DLC_COLLECT_FLAGS_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELIF PS_DLC_Collect_Flags_Progress_Check()
							PS_PLAY_DISPATCHER_INSTRUCTION("PS_CF", "PS_CF_2")
							iCF_NextCheck = 3
							PS_INCREMENT_SUBSTATE()
						ELIF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND iInstructionCounter = 1
							PRINT_HELP("PS_DLC_H_CF")
							IF PS_IS_WAITING_FOR_CHECKPOINT_FEEDBACK()
								PRINT("PS_DLC_O_CF", DEFAULT_GOD_TEXT_TIME, 1)
								PS_STOP_WAITING_FOR_CHECKPOINT_FEEDBACK()
							ENDIF
							iInstructionCounter++
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						PS_DLC_Collect_Flags_State = PS_DLC_COLLECT_FLAGS_OBSTACLE
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
			BREAK
	
		//Obstacle course loop
			CASE PS_DLC_COLLECT_FLAGS_OBSTACLE
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": PS_DLC_COLLECT_FLAGS_OBSTACLE")
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_KILL_HINT_CAM()
						
						//PRINT_HELP("PS_HELP_FLAG")
												
 				    	PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF PS_DLC_Collect_Flags_Fail_Check()
							PS_DLC_Collect_Flags_State = PS_DLC_COLLECT_FLAGS_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELIF PS_DLC_Collect_Flags_Progress_Check()
							PS_INCREMENT_SUBSTATE()
						ELSE
							PS_DLC_Collect_Flags_Update_Dialogue()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						PS_RESET_OBJECTIVE_DATA()
						PS_SET_OUTSIDE_LOOP_METER_ACTIVE(FALSE)
						PS_UPDATE_RECORDS(VECTOR_ZERO)
						IF Pilot_School_Data_Check_Mission_Success(PS_Main.myChallengeData, PS_Main.myPlayerData)
							PS_DLC_Collect_Flags_State = PS_DLC_COLLECT_FLAGS_SUCCESS
						ELSE
							PS_DLC_Collect_Flags_State = PS_DLC_COLLECT_FLAGS_FAIL
						ENDIF
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
			BREAK
												
			CASE PS_DLC_COLLECT_FLAGS_SUCCESS
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": PS_DLC_COLLECT_FLAGS_SUCCESS")
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PRINTLN("STUCK A")
						PS_UPDATE_RECORDS(VECTOR_ZERO)
						PS_PLAY_LESSON_FINISHED_LINE("PS_CFPass", "PS_CFPass_1", "PS_CFPass_2", "PS_CFPass_3", "")
						PS_LESSON_END(TRUE)
						PS_INCREMENT_SUBSTATE()
						PS_TRIGGER_MUSIC(PS_MUSIC_COLLECT_STOP)
						BREAK
					CASE PS_SUBSTATE_UPDATE
						PRINTLN("STUCK B")
						IF IS_PLAYER_DEAD(player_id())
							PRINTLN("STUCK B-A")
						ENDIF
						
						IF GET_MANUAL_RESPAWN_STATE()!= MRS_READY_FOR_PLACEMENT
							PRINTLN("STUCK B-B")
						ENDIF
						
						IF NOT IS_PLAYER_DEAD(player_id())							
						OR GET_MANUAL_RESPAWN_STATE()= MRS_READY_FOR_PLACEMENT
							PRINTLN("STUCK C")
							IF NOT PS_HUD_IS_SCORECARD_ACTIVE()
								PRINTLN("STUCK D")
								PS_INCREMENT_SUBSTATE()
							ENDIF
						ELSE
							PRINTLN("PLAYER IS DEAD SETTING RESPAWN STATE")
							//Set the player to respawn state if dead and doesn't have respawn state set
							IF IS_PLAYER_DEAD(player_id())
							AND GET_MANUAL_RESPAWN_STATE() = MRS_NULL
								SET_MANUAL_RESPAWN_STATE(MRS_RESPAWN_PLAYER_HIDDEN)
							ENDIF
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						PRINTLN("STUCK E")
						bFinishedChallenge = TRUE
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
			BREAK
				
			CASE PS_DLC_COLLECT_FLAGS_FAIL
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": PS_DLC_COLLECT_FLAGS_FAIL")
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_TRIGGER_MUSIC(PS_MUSIC_COLLECT_FAIL)
						PS_UPDATE_RECORDS(VECTOR_ZERO)
						PS_PLAY_LESSON_FINISHED_LINE("PS_CFFail", "", "", "", "PS_CFFail_1")
						PS_LESSON_END(FALSE)
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF NOT PS_HUD_IS_SCORECARD_ACTIVE()
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						bFinishedChallenge = TRUE
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
			BREAK
		ENDSWITCH
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC PS_DLC_Collect_Flags_MainCleanup()
//hud
	PS_HUD_SET_GUTTER_ICON_ACTIVE(FALSE)
	PS_HUD_SET_OBJECTIVE_METER_VISIBLE(FALSE)
	PS_SET_TIMER_ACTIVE(FALSE)
	PS_HUD_CLEANUP()
	CLEAR_PRINTS()
	CLEAR_HELP()

//player/objects
	PS_SEND_PLAYER_BACK_TO_MENU()

//cleanup checkpoints
	PS_RESET_CHECKPOINT_MGR()
	iCF_NextCheck = 0
	
	REMOVE_SCENARIO_BLOCKING_AREA(siCollectF)
	
//assets
	REMOVE_VEHICLE_RECORDING(iPS_PreviewVehicleRecordingID, "PilotSchool")
	SET_MODEL_AS_NO_LONGER_NEEDED(VehicleToUse)	
	
	// Allow the blocked planes from being generated in the two boarding locations.
	//VECTOR vGenBlockMin = <<-1514.927490,-3203.546387,26.694818>> - << 200.0, 200.0, 20.0 >>
	//VECTOR VGenBlockMax = <<-1514.927490,-3203.546387,26.694818>> + << 200.0, 200.0, 20.0 >>
	//SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(vGenBlockMin, VGenBlockMax, TRUE)
	UNBLOCK_SCENARIOS_AND_AMBIENT(sb_PS)
	
	#IF IS_DEBUG_BUILD
		DEBUG_MESSAGE("******************Cleaning up PS_DLC_Collect_Flags.sc******************")
	#ENDIF
	
	
ENDPROC

// EOF
//****************************************************************************************************
//****************************************************************************************************

