//****************************************************************************************************
//
//	Author: 		Mark Beagan
//	Date: 			7/12/11
//	Description:	City Landing variation of "Daring Landing" challenge for Pilot School.
//****************************************************************************************************


USING "minigames_helpers.sch"
USING "Pilot_School_MP_Challenge_Helpers.sch"

//Consts
CONST_INT MAX_CITYLANDING_CHECKPOINTS	5
CONST_INT LOCATE_SIZE_CITY_LANDING	100
CONST_INT DEFAULT_DIALOGUE_TIME 3000
CONST_INT ENGINE_BLOW_TIME 3000

VECTOR vLandingCoord1 = <<-1969.0068, -745.3975, 0>>
VECTOR vLandingCoord2 = <<-2114.9341, -427.0884, 20.1451>>
//VECTOR vEngineOffset = <<-10.5, -0.35, 1.75>>

//MODEL_NAMES mnPassenger
//PED_INDEX piPassenger

//Turbulence value
FLOAT fturbulence = 0.07

//Turbulence timer
INT ittime = 2000

//Value for the engine cutting out completely
INT iEngineCutOutTime = -1
INT iEngineWarningTime = -1

//Lightning flash timer
INT iFlashTime = 10000

//Cut out control timer
INT iControlTime = 2000

//Initial timer for cutting out
INT iCutOutTime

//Integer for alarm
INT iAlarm

//Flag for starting particle effect
//BOOL bPtfxStart = FALSE

//Flag for cutting out control
BOOL bCutOutControl = FALSE

//Flag for force applied
BOOL bForceApplied = FALSE

//Flag for the engine being blown
BOOL bEngineBlown = FALSE

//Flag for playing alarm
BOOL bAlarmPlay = TRUE

//Flag for early landing
BOOL bEarlyLand = FALSE

//Disable accel
BOOL bDisableAccel = FALSE

//Timer for manually checking landing fail
structTimer TimerLandFail

//Offset for engine particle effect
VECTOR vEngineOffset = <<0, -1.0, -0.4>>

//Particle effect for the burst engine
PTFX_ID engineptfx

//****************************************************************************************************
//****************************************************************************************************
//	Private functions that are used only by City Landing
//****************************************************************************************************
//****************************************************************************************************

PROC PS_Challenge_City_Landing_Init_Checkpoints()
	PS_RESET_CHECKPOINT_MGR()
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<71.8578, -667.6937, 300.7023>>)//1
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<-397.1976, -1532.0588, 251.9922>>)//2
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<-1000.8510, -1700.8878, 169.0133>>)//3
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<-1523.7268, -1262.4178, 60.6281>>)//4
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_LANDING, <<-1941.2341, -720.0712, 2.4948>>)//5
ENDPROC

//Fail check
FUNC BOOL PS_DLC_CITY_Fail_Check()

BOOL bfail = FALSE

	IF PS_GET_SUBSTATE() = PS_SUBSTATE_UPDATE
		IF NOT bEngineBlown
			IF PS_UPDATE_FAIL_CHECKS(PS_Main, FALSE, FALSE, TRUE, TRUE, FALSE)
				bfail = TRUE
			ENDIF
		ELSE
			IF PS_UPDATE_FAIL_CHECKS(PS_Main, FALSE, FALSE, FALSE, TRUE, FALSE)
				bfail = TRUE
			ENDIF
		ENDIF
		
		SWITCH(PS_DLC_City_Landing_State)
			CASE PS_DLC_CITY_LANDING_TAKEOFF
				//Manual check height above ground and speed
				IF NOT IS_ENTITY_DEAD(PS_Main.myVehicle)
					IF NOT (GET_ENTITY_HEIGHT_ABOVE_GROUND(PS_Main.myVehicle) - GroundOffset >= 0) OR NOT IS_ENTITY_IN_AIR(PS_Main.myVehicle)
						IF GET_ENTITY_SPEED(PS_Main.myVehicle) < 1.0
							IF NOT IS_TIMER_STARTED(TimerLandFail)
								START_TIMER_NOW(TimerLandFail)
							ELSE
								IF TIMER_DO_ONCE_WHEN_READY(TimerLandFail, 3)
									bfail = TRUE
									bEarlyLand = TRUE
									ePSFailReason = PS_FAIL_LANDED_VEHICLE
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF IS_TIMER_STARTED(TimerLandFail)
							CANCEL_TIMER(TimerLandFail)
						ENDIF
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
	
	RETURN bfail
ENDFUNC

/// PURPOSE:
///    Checks for objective goals completed based on the stage/state we are in of the challenge progression
///    When it returns TRUE, the main loop will increment the challenge state/stage
FUNC BOOL PS_DLC_CITY_Progress_Check()
	IF NOT IS_PED_INJURED(PLAYER_PED_ID()) AND IS_VEHICLE_DRIVEABLE(PS_Main.myVehicle)
		SWITCH(PS_DLC_City_Landing_State)
			CASE PS_DLC_CITY_LANDING_INIT
				RETURN FALSE
			BREAK

			CASE PS_DLC_CITY_LANDING_COUNTDOWN
				RETURN FALSE
			BREAK
				
			CASE PS_DLC_CITY_LANDING_TAKEOFF
				PRINTLN("CITYLANDING - CHECKING TAKEOFF SECTION")
				PRINTLN("CURRENT CHECKPOINT : ", PS_GET_CHECKPOINT_PROGRESS())
				
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_UPDATE
						//See if the player is close to the checkpoint yet
						IF PS_GET_CHECKPOINT_PROGRESS() > MAX_CITYLANDING_CHECKPOINTS - 1 OR PS_IS_LAST_CHECKPOINT_CLEARED()
							RETURN TRUE
						ELSE
							PS_UPDATE_CHECKPOINT_MGR()
						ENDIF						
					BREAK
				ENDSWITCH
				
				IF GET_ENTITY_SPEED(PS_Main.myVehicle) < 10.0
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
					IF GET_ENTITY_SPEED(PS_Main.myVehicle) < 1.0
						/*PS_DLC_City_Landing_State = PS_DLC_CITY_LANDING_SUCCESS
						PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)*/
					ENDIF
				ENDIF
			BREAK
				
			CASE PS_DLC_CITY_LANDING_LANDING
				PRINTLN("CITYLANDING - CHECKING LANDING SECTION")
				PRINTLN("CURRENT GROUND HEIGHT: ", GET_ENTITY_HEIGHT_ABOVE_GROUND(PS_Main.myVehicle) - GroundOffset)
				IF NOT (GET_ENTITY_HEIGHT_ABOVE_GROUND(PS_Main.myVehicle) - GroundOffset > 0) OR NOT IS_ENTITY_IN_AIR(PS_Main.myVehicle)
					//IF GET_ENTITY_SPEED(PS_Main.myVehicle) < 30.0
						bDisableAccel = TRUE
						IF GET_ENTITY_SPEED(PS_Main.myVehicle) < 1.0
							RETURN TRUE
						ENDIF
					//ENDIF
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
	//if we reach here, we probably haven't passed the check
	RETURN FALSE
ENDFUNC


//Check for the player breaking and give a boost
PROC CHECK_PLAYER_BREAKING()

VECTOR vplanePos
VECTOR vecVel
VECTOR vecNeg
VECTOR vecRem

INT iBrakeBoost = 85

	vPlanePos = GET_ENTITY_COORDS(PS_Main.myVehicle)

	if vPlanePos.z < 10
		IF NOT IS_PAUSE_MENU_ACTIVE()
			IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_LT)
				PRINTSTRING("CITY LANDING - PLAYER IS BREAKING")
				GET_ENTITY_SPEED(PS_Main.myVehicle)
				
				vecvel = GET_ENTITY_VELOCITY(PS_Main.myVehicle)
				vecNeg = <<vecvel.X/iBrakeBoost, vecvel.Y/iBrakeBoost, vecvel.Z/iBrakeBoost>>
				vecRem = <<vecvel.X - vecNeg.X, vecvel.Y - vecNeg.Y, vecvel.Z - vecNeg.Z>>
				
				SET_ENTITY_VELOCITY(PS_Main.myVehicle, vecRem)
			ENDIF
		ENDIF
	ENDIF
ENDPROC


//Shake the camera for lightning
PROC DO_LIGHTNING_SHAKE_CAM()
	SHAKE_GAMEPLAY_CAM("JOLT_SHAKE", 0.07)
ENDPROC

//Shake the camera for explosion
PROC DO_EXPLOSION_SHAKE_CAM()
	SHAKE_GAMEPLAY_CAM("SMALL_EXPLOSION_SHAKE", 0.07)
ENDPROC



//Play alarm audio
PROC PLAY_ALARM()
	if bAlarmPlay
		if HAS_SOUND_FINISHED(iAlarm)
			PLAY_SOUND_FRONTEND(iAlarm, "Generic_Alarm_Fire_Electronic")
		ENDIF
	ENDIF
ENDPROC

//Give the plane turbulence
PROC GIVE_PLANE_TURBULENCE()

VECTOR vPlanePos

	/*INT idownforce
	INT iOffset
	INT iwingdown*/
	
	if DOES_ENTITY_EXIST(PS_Main.myVehicle)
		IF NOT IS_VEHICLE_FUCKED(PS_Main.myVehicle)
			vPlanePos = GET_ENTITY_COORDS(PS_Main.myVehicle)

			if ittime < GET_GAME_TIMER()
				PRINTLN("CITY LANDING - PLANE TURBULENCE GIVEN")
				SET_PLANE_TURBULENCE_MULTIPLIER(PS_Main.myVehicle, fTurbulence)
				DO_EXPLOSION_SHAKE_CAM()
//				SET_PAD_SHAKE(PAD1, 250, 128)
				ittime = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(1000, 2000)
			
				PLAY_ALARM()
				
				IF ARE_PLANE_PROPELLERS_INTACT(PS_Main.myVehicle)
					SET_VEHICLE_DAMAGE(PS_Main.myVehicle, <<-10.5, -0.35, 1.75>>, 50, 50, TRUE)
				ENDIF

				if bEngineBlown = FALSE
					iEngineCutOutTime = GET_GAME_TIMER() + 60000
					iEngineWarningTime = GET_GAME_TIMER() + 40000
					bEngineBlown = TRUE
					
					PLAY_SOUND_FROM_ENTITY(-1, "engineexplosion", PS_Main.myVehicle, "DLC_PILOT_CITY_LANDING" )
				ENDIF
			ENDIF
			
			//On cutting out controls
			IF bCutOutControl
				//If it's 10m above the ground
				IF vPlanePos.Z >= 10.0
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_THROTTLE_UP)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_THROTTLE_DOWN)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_YAW_LEFT)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_YAW_RIGHT)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_ROLL_LR)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_PITCH_UD)
				ENDIF
			ENDIF
			
			IF bCutOutControl	
				IF vPlanePos.Z >= 65.0

					if not bForceApplied
						//Down force removed by request from Imran
						//idownforce = GET_RANDOM_INT_IN_RANGE(5, 15)
						//iwingdown = GET_RANDOM_INT_IN_RANGE(5 ,10)
						//iOffSet = 18
					
						//Nose and tail
						/*APPLY_FORCE_TO_ENTITY(PS_Main.myVehicle, APPLY_TYPE_FORCE, <<0,0,idownforce>>, <<0, -iOffSet, 0>>, 0, TRUE, TRUE, TRUE)
						APPLY_FORCE_TO_ENTITY(PS_Main.myVehicle, APPLY_TYPE_FORCE, <<0,0,-idownforce>>, <<0, iOffSet, 0>>, 0, TRUE, TRUE, TRUE)
						
						//Left wing
						APPLY_FORCE_TO_ENTITY(PS_Main.myVehicle, APPLY_TYPE_FORCE, <<0,0,-iwingdown>>, <<-6.5, 0, 0>>, 0, TRUE, TRUE, TRUE)
						*/
						bForceApplied = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			//Timer for turning control on and off
			if GET_GAME_TIMER() >= iControlTime
				if bCutOutControl
					bCutOutControl = FALSE
					//Check if it's before the last checkpoint
					//IF PS_GET_CHECKPOINT_PROGRESS() >= MAX_CITYLANDING_CHECKPOINTS - 1
					
					//Duration of cut out
					iControlTime = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(3500, 4500)
						
					//Imran / Les request to remove for blanket case TODO 1728566
					/*ELSE
						//Easier for landing more time without cut out
						iControlTime = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(1500, 2000)*/
					//ENDIF
				ELSE
					bCutOutControl = TRUE
					bForceApplied = FALSE
					//Time between cut outs
					iControlTime = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(800, 1200)
				ENDIF
				
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//****************************************************************************************************
//****************************************************************************************************
//	Public functions! (used by Pilot_School.sc to run the challenges)
//****************************************************************************************************
//****************************************************************************************************

PROC PS_DLC_CITY_Initialise()
	SWITCH g_PS_initStage
		CASE 0
		
			DO_NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
		
			iAlarm = GET_SOUND_ID()					
			//mnpassenger = GET_NPC_PED_MODEL(CHAR_DOM)	
			iPS_PreviewVehicleRecordingID = iPS_PEVIEW_VEH_REC_ID_DLC_CITY_LANDING
			VehicleToUse = TITAN
			
			//REQUEST_MODEL(mnpassenger)	
			REQUEST_PTFX_ASSET()
			REQUEST_SCRIPT_AUDIO_BANK("Alarms")
			REQUEST_SCRIPT_AUDIO_BANK("DLC_PILOTSCHOOL/CITY_LANDING")
			REQUEST_VEHICLE_RECORDING(iPS_PreviewVehicleRecordingID, sPreviewRecordingName)
			g_PS_initStage++			
		BREAK
		CASE 1
			
			
			
			IF HAS_VEHICLE_RECORDING_BEEN_LOADED(iPS_PreviewVehicleRecordingID, sPreviewRecordingName)
			//AND HAS_MODEL_LOADED(mnpassenger)
			AND HAS_PTFX_ASSET_LOADED()
			AND REQUEST_SCRIPT_AUDIO_BANK("Alarms")
			AND REQUEST_SCRIPT_AUDIO_BANK("DLC_PILOTSCHOOL/CITY_LANDING")
			AND PS_SETUP_CHALLENGE()
				g_PS_initStage++
			ELSE
				#IF IS_DEBUG_BUILD
				IF NOT REQUEST_SCRIPT_AUDIO_BANK("Alarms")
					PRINTLN("PS_DLC_CITY_Initialise: waiting on Alarms Audio Bank ")
				ENDIF
				IF NOT REQUEST_SCRIPT_AUDIO_BANK("DLC_PILOTSCHOOL/CITY_LANDING")
					PRINTLN("PS_DLC_CITY_Initialise: waiting on CITY_LANDING Audio Bank ")
				ENDIF
				IF NOT HAS_PTFX_ASSET_LOADED()
					PRINTLN("PS_DLC_CITY_Initialise: waiting on PTFX ")
				ENDIF
				IF NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(iPS_PreviewVehicleRecordingID, sPreviewRecordingName)
					PRINTLN("PS_DLC_CITY_Initialise: waiting on VEHICLE_RECORDING ")
				ENDIF
				#ENDIF
			ENDIF
			
		BREAK
		CASE 2
			IF PS_CREATE_VEHICLE()
				bwaitingOnSomething = FALSE
				
				iPopSpehere = ADD_POP_MULTIPLIER_SPHERE(<<0,0,0>>,8000,0.0,0.0,FALSE)

				//Initialise variables
				PS_DLC_City_Landing_State = PS_DLC_CITY_LANDING_INIT
				ePSFailReason = PS_FAIL_NO_REASON
				PS_Challenge_City_Landing_Init_Checkpoints()	
				vStartPosition = <<783.7736, 1615.9418, 596.0633>>//<< -609.1918, 5842.0552, 200.8225 >>//<< -2125, -1095, 199.68>>
				fStartHeading = 162.7695
				vStartRotation = <<-2.0000, 0.0000, fStartHeading>>	
				
				//Bool used to keep the loop going for the entire challenge
				bFinishedChallenge = FALSE
				iInstructionCounter = 1
				iPSDialogueCounter = 1
				
				//offset used for checking if player is on the ground or in the air
				PS_SET_GROUND_OFFSET(1.8)
				
				
				

				NETWORK_OVERRIDE_CLOCK_TIME(GET_HOUR_FOR_PS_CHALLENGE(),0,0)
//				SET_WEATHER_FOR_FMMC_MISSION(2) //raining
				//SET_WEATHER_TYPE_NOW("THUNDER")
				//SET_CLOCK_TIME(0,0,0)
				
				PS_Special_Fail_Reason = "PS_FAIL_3A"
				
				//Player vehicle model
				fCurPlaneSpeed = -1
				
				fPreviewVehicleSkipTime = 17000
				fPreviewTime = 19
				
				vPilotSchoolSceneCoords = vStartPosition
				IF HAS_VEHICLE_RECORDING_BEEN_LOADED(iPS_PreviewVehicleRecordingID, sPreviewRecordingName)
					vPilotSchoolPreviewCoords = GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(iPS_PreviewVehicleRecordingID, fPreviewVehicleSkipTime, sPreviewRecordingName) //<< -1831.4043, 4688.6953, 69.2119 >>
					vPilotSchoolPreviewRot = GET_ROTATION_OF_VEHICLE_RECORDING_AT_TIME(iPS_PreviewVehicleRecordingID, fPreviewVehicleSkipTime, sPreviewRecordingName)
				ENDIF
				
				//Spawn player vehicle
				

				IF NOT IS_ENTITY_DEAD(PS_Main.myVehicle)
					SET_VEHICLE_ACTIVE_DURING_PLAYBACK(PS_Main.myVehicle, TRUE) 
				ENDIF

				//SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				//DO_NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)	
				//SET_PLAYER_INVINCIBLE(PLAYER_ID(),FALSE)
				SET_ENTITY_MAX_SPEED(PS_Main.myVehicle, 90.001)

				g_PS_initStage = 0

				#IF IS_DEBUG_BUILD
					DEBUG_MESSAGE("******************CITY LANDING BEING SET UP BEAGAN******************")
				#ENDIF
			ELSE
				EXIT
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL PS_DLC_CITY_MainSetup()
	//NETWORK_INDEX netID
	//IF PS_CREATE_NET_OBJ_IN_VEHICLE(netID,PS_Main.myVehicle_netID, PEDTYPE_MISSION, mnpassenger, VS_FRONT_RIGHT)
		//piPassenger = NET_TO_PED(netID)
		PRINTSTRING("CITYLANDING - STATE NUM: ") PRINTINT(ENUM_TO_INT(PS_GET_SUBSTATE())) PRINTNL()
		PS_Challenge_City_Landing_Init_Checkpoints()
		SET_MINIMAP_COMPONENT(MINIMAP_COMPONENT_RUNWAY_1, FALSE)
		IF NOT IS_VEHICLE_FUCKED(PS_Main.myVehicle)
			SET_ENTITY_COORDS_NO_OFFSET(PS_Main.myVehicle, vStartPosition)
			SET_ENTITY_ROTATION(PS_Main.myVehicle, vStartRotation)
			SET_ENTITY_HEALTH(PS_Main.myVehicle, 1500)
			PS_AUTO_PILOT(PS_Main, 90.0, FALSE, vStartRotation.x, vStartRotation.y)
			CONTROL_LANDING_GEAR(PS_Main.myVehicle, LGC_RETRACT_INSTANT)
		ENDIF
				
		NETWORK_OVERRIDE_CLOCK_TIME(GET_HOUR_FOR_PS_CHALLENGE(),0,0)
//		SET_WEATHER_FOR_FMMC_MISSION(2)
		
		IF IS_NEW_LOAD_SCENE_ACTIVE()
			NEW_LOAD_SCENE_STOP()
		ENDIF
		NEW_LOAD_SCENE_START(vStartPosition, NORMALISE_VECTOR(<<-855.9717, 5652.1563, 384.8416>> - <<-828.8492, 5675.9849, 384.8416>>), 5000.0)
	//ELSE
	//	RETURN FALSE
	//ENDIF
	RETURN TRUE
ENDFUNC

//Main preview function
FUNC BOOL PS_DLC_CITY_Preview()
	IF PS_PREVIEW_UPDATE_CUTSCENE()
		SWITCH(PS_PreviewState)
			CASE PS_PREVIEW_SETUP
				PS_PREVIEW_SETUP_PLAYER_FOR_CUTSCENE()
				IF NOT IS_VEHICLE_FUCKED(PS_Main.myVehicle)
					START_PLAYBACK_RECORDED_VEHICLE(PS_Main.myVehicle, iPS_PreviewVehicleRecordingID, "PilotSchool")
					fPreviewVehicleSkipTime = 1000.0
					SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(PS_Main.myVehicle, fPreviewVehicleSkipTime)
					SET_HELI_BLADES_FULL_SPEED(PS_Main.myVehicle)
					SET_VEHICLE_ENGINE_ON(PS_Main.myVehicle,true,true)
					
					
//					SET_WEATHER_FOR_FMMC_MISSION(2) //raining
					
					SETTIMERA(0)
				ENDIF
				DO_NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				PS_PREVIEW_DIALOGUE_PLAY("PS_CITYPREV", "PS_CITYPREV_1", "PS_CITYPREV_2", "PS_CITYPREV_3", "PS_CITYPREV_4", "PS_CITYPREV_5")
				//iPreviewCheckpointIdx = PS_GET_CLOSEST_PREVIEW_CHECKPOINT(iPS_PreviewVehicleRecordingID, PS_Main.myCheckpointz, fPreviewVehicleSkipTime)
				//PS_ACTIVATE_CHECKPOINT_MGR(PS_LESSON_OBSTACLE_PLANE, iPreviewCheckpointIdx, TRUE)
			BREAK
			
			CASE PS_PREVIEW_PLAYING
				//Force thunder weather while playing
//				SET_WEATHER_FOR_FMMC_MISSION(2)
			BREAK
			
			CASE PS_PREVIEW_CLEANUP
				PS_PREVIEW_CLEANUP_CUTSCENE()
			BREAK
		ENDSWITCH
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

//Forcing fail with F skip
PROC PS_DLC_CITY_FORCE_FAIL
	ePSFailReason = PS_FAIL_DEBUG
	PS_DLC_City_Landing_State = PS_DLC_CITY_LANDING_FAIL
	PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
	
	
	//temp
		

	PS_HUD_SET_GUTTER_ICON_ACTIVE(FALSE)
	PS_HUD_SET_OBJECTIVE_METER_VISIBLE(FALSE)
	PS_SET_TIMER_ACTIVE(FALSE)
	PS_HUD_CLEANUP()
	CLEAR_PRINTS()
	CLEAR_HELP()
	
	

//take care of objects
	//player first
	PS_SEND_PLAYER_BACK_TO_MENU()
	//then vehicle
	
	//then checkpoints
	PS_RESET_CHECKPOINT_MGR()



	REMOVE_VEHICLE_RECORDING(iPS_PreviewVehicleRecordingID, "PilotSchool")
	SET_MODEL_AS_NO_LONGER_NEEDED(VehicleToUse)		
	//SET_MODEL_AS_NO_LONGER_NEEDED(targetProp)

	//SET_MODEL_AS_NO_LONGER_NEEDED(targetPropMoverB)
	
	#if IS_DEBUG_BUILD
	IF DOES_WIDGET_GROUP_EXIST(shtWidget)
		DELETE_WIDGET_GROUP(shtWidget)
	ENDIF
	#endif
	
		

	PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
ENDPROC

//Forcing pass with S skip
PROC PS_DLC_CITY_FORCE_PASS
	PRINTLN("CITYLAND - FORCE PASS OCCURRED")
	IF NOT IS_PED_INJURED(PLAYER_PED_ID()) AND NOT IS_VEHICLE_FUCKED(PS_Main.myVehicle)
		SET_ENTITY_COORDS(PS_Main.myVehicle, PS_GET_LAST_CHECKPOINT())
		BRING_VEHICLE_TO_HALT(PS_Main.myVehicle, 5.0, 1)
	ENDIF
	PS_DLC_City_Landing_State = PS_DLC_CITY_LANDING_SUCCESS
	PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
ENDPROC

//Main update function
FUNC BOOL PS_DLC_CITY_MainUpdate()
	IF TempDebugInput() AND NOT bFinishedChallenge

		//No peds and minimal vehicles for better streaming
		
		//SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
		//SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.5)
		
		PS_LESSON_UDPATE()
		
		
		//Force lightning flash
		IF TIMERA() > iFlashTime
			FORCE_LIGHTNING_FLASH()
			iFlashTime = GET_RANDOM_INT_IN_RANGE(2000, 8000)
			SETTIMERA(0)
			DO_LIGHTNING_SHAKE_CAM()
		ENDIF
		
		//Set time for cutout
		IF PS_GET_CHECKPOINT_PROGRESS() = 1
			//Set time before engine blows
			iCutOutTime = GET_GAME_TIMER() + ENGINE_BLOW_TIME
		ENDIF
		
		//Give plane turbulence after first checkpoint
		IF PS_GET_CHECKPOINT_PROGRESS() > 1
			if GET_GAME_TIMER() > iCutOutTime
				GIVE_PLANE_TURBULENCE()
			ENDIF
		ENDIF
		
		//If the player has flown around with the engine blown for a minute turn it off
		IF bEngineBlown
			PRINTLN("ENGINE CUT OUT TIME IS : ", iEngineCutOutTime, "CURRENT TIME IS : ", GET_GAME_TIMER())
			
			IF iEngineWarningTime != -1
				IF GET_GAME_TIMER() > iEngineWarningTime
					IF NOT IS_ENTITY_DEAD(PS_Main.myVehicle)
						if IS_ENTITY_IN_AIR(PS_Main.myVehicle)
							PS_PLAY_DISPATCHER_INSTRUCTION("PS_CITYENG", "PS_CITYENG_1")
							iEngineWarningTime = -1
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF iEngineCutOutTime != -1
				IF IS_VEHICLE_DRIVEABLE(PS_Main.myVehicle)
					IF GET_GAME_TIMER() > iEngineCutOutTime
					AND GET_ENTITY_HEIGHT_ABOVE_GROUND(PS_Main.myVehicle) > 10.0
						IF NOT IS_VEHICLE_FUCKED(PS_Main.myVehicle)
							if IS_ENTITY_IN_AIR(PS_Main.myVehicle)
								PS_PLAY_DISPATCHER_INSTRUCTION("PS_CITYENG", "PS_CITYENG_2")
								SET_VEHICLE_ENGINE_ON(PS_Main.myVehicle,false,true)
								iEngineCutOutTime = -1
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//Disable plane controls
		IF PS_DLC_City_Landing_State = PS_DLC_CITY_LANDING_INIT OR PS_DLC_City_Landing_State = PS_DLC_CITY_LANDING_COUNTDOWN
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_ROLL_LR)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_ROLL_LEFT_ONLY)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_ROLL_RIGHT_ONLY)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_PITCH_UD)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_PITCH_UP_ONLY)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_PITCH_DOWN_ONLY)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_UNDERCARRIAGE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_ATTACK)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_SELECT_NEXT_WEAPON)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_SELECT_PREV_WEAPON)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_SELECT_TARGET_LEFT)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_SELECT_TARGET_RIGHT)
		ENDIF
		
		SWITCH(PS_DLC_City_Landing_State)
		
		//Spawn player and plane in the air, aimed towards the landing target, with a forward velocity
			CASE PS_DLC_CITY_LANDING_INIT
				//updating autopilot to start the player out in the air
				PS_AUTO_PILOT(PS_Main, 70.0, FALSE, vStartRotation.x, vStartRotation.y)
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
//						SET_WEATHER_FOR_FMMC_MISSION(2) //raining
						FREEZE_ENTITY_POSITION(PS_Main.myVehicle, FALSE)
						SET_VEHICLE_ENGINE_ON(PS_Main.myVehicle, TRUE, TRUE)
						SET_HELI_BLADES_FULL_SPEED(PS_Main.myVehicle)
						//SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						//DO_NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)	
						//SET_PLAYER_INVINCIBLE(PLAYER_ID(),FALSE)
						SET_GAMEPLAY_CAM_RELATIVE_PITCH()
						SET_GAMEPLAY_CAM_RELATIVE_HEADING()
						iPSDialogueCounter = 1
						iInstructionCounter = 1
						PS_INCREMENT_SUBSTATE()
					BREAK
					CASE PS_SUBSTATE_UPDATE
						PS_INCREMENT_SUBSTATE()
					BREAK
					CASE PS_SUBSTATE_EXIT
						PS_DLC_City_Landing_State = PS_DLC_CITY_LANDING_COUNTDOWN
						PS_INCREMENT_SUBSTATE()
					BREAK
				ENDSWITCH
			BREAK

		//Play countdown
			CASE PS_DLC_CITY_LANDING_COUNTDOWN
				//updating autopilot to start the player out in the air
				PS_AUTO_PILOT(PS_Main, 70.0, FALSE, vStartRotation.x, vStartRotation.y)
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_INCREMENT_SUBSTATE()
					BREAK
					CASE PS_SUBSTATE_UPDATE
						IF PS_HUD_UPDATE_COUNTDOWN()
							PS_INCREMENT_SUBSTATE()
						ENDIF
					BREAK
					CASE PS_SUBSTATE_EXIT
						PS_DLC_City_Landing_State = PS_DLC_CITY_LANDING_TAKEOFF
						PS_TRIGGER_MUSIC(PS_MUSIC_CITY_START)
						PS_INCREMENT_SUBSTATE()
					BREAK
				ENDSWITCH
			BREAK
				
		//Give player control and tell him to head for the checkpoint
			CASE PS_DLC_CITY_LANDING_TAKEOFF
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						//SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						DO_NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)	
						//SET_PLAYER_INVINCIBLE(PLAYER_ID(),FALSE)
						////SET_PLAYER_INVINCIBLE(PLAYER_ID(), TRUE)
						//Setup objective, give the player controls and manage any new help text
						PS_ACTIVATE_CHECKPOINT_MGR(PS_LESSON_LANDING)
						
						//PS_HUD_RESET_TIMER()
						PS_HUD_SET_GUTTER_ICON_ACTIVE(TRUE)
						//PS_SET_TIMER_ACTIVE(TRUE)
						PS_SET_HINT_CAM_ACTIVE(TRUE)
						PS_SET_HINT_CAM_COORD(PS_GET_CURRENT_CHECKPOINT())
						PS_INCREMENT_SUBSTATE()
					BREAK
					CASE PS_SUBSTATE_UPDATE
						PRINTSTRING("INSIDE UPDATE Instruction count: ") PRINTINT(iInstructionCounter) PRINTNL()
						IF PS_DLC_CITY_Fail_Check()
							PS_DLC_City_Landing_State = PS_DLC_CITY_LANDING_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELIF PS_DLC_CITY_Progress_Check()				
							PS_INCREMENT_SUBSTATE()
						ELIF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND iInstructionCounter = 1
							PRINT("PS_CITY_CHECK1", DEFAULT_GOD_TEXT_TIME, 1)//Fly to the ~y~checkpoint.~s~
							iInstructionCounter++
						ENDIF
						
						//On flying to the first checkpoint
						IF PS_GET_CHECKPOINT_PROGRESS() = 1
							if iPSDialogueCounter = 1 AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND NOT IS_MESSAGE_BEING_DISPLAYED()
								//Dialogue for bad weather
								PS_PLAY_DISPATCHER_INSTRUCTION_3_LINES("PS_CITY", "PS_CITY_1", "PS_CITY", "PS_CITY_2", "PS_CITY", "PS_CITY_3")
								iPSDialogueCounter++
							ENDIF
						ENDIF
						
						//On the engine being blown
						IF PS_GET_CHECKPOINT_PROGRESS() >= 2
							if bEngineBlown
								if iPSDialogueCounter = 2 AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND NOT IS_MESSAGE_BEING_DISPLAYED()
									//Dialogue on engine blowing
									PS_TRIGGER_MUSIC(PS_MUSIC_CITY_CUTOUT)
									PS_PLAY_DISPATCHER_INSTRUCTION("PS_CITY", "PS_CITY_4")
									iPSDialogueCounter++
								ENDIF
								
								//Check for particle smoke effect
								IF NOT DOES_PARTICLE_FX_LOOPED_EXIST( engineptfx )
									PRINTLN("PARTICLE EFFECT STARTED")
									engineptfx = START_NETWORKED_PARTICLE_FX_LOOPED_ON_ENTITY_BONE("scr_veh_plane_gen_damage", PS_Main.myVehicle, vEngineOffset, <<0,0,0>>, GET_ENTITY_BONE_INDEX_BY_NAME(PS_Main.myVehicle,"exhaust"),1.0)
									//engineptfx = START_PARTICLE_FX_LOOPED_AT_COORD("scr_veh_plane_gen_damage", <<-154.0036, -938.4574, 268.1998>>, <<0,0,0>>)
									SET_PARTICLE_FX_LOOPED_EVOLUTION(engineptfx, "damage_smoke", 0.5 )
									SET_PARTICLE_FX_LOOPED_EVOLUTION(engineptfx, "damage_fire", 0.2 )
								ELSE
									PRINTLN("PARTICLE EFFECT ALREADY EXISTS")
								ENDIF
								
								//Disable the propeller of the smoking engine
								DISABLE_INDIVIDUAL_PLANE_PROPELLER(PS_Main.myVehicle, 0)
							ENDIF
						ENDIF
						
						//On getting to the second checkpoint
						IF PS_GET_CHECKPOINT_PROGRESS() = 3 
							if iPSDialogueCounter = 3 AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND NOT IS_MESSAGE_BEING_DISPLAYED()
							//Dialogue for runway
								PS_PLAY_DISPATCHER_INSTRUCTION_2_LINES("PS_CITY", "PS_CITY_5", "PS_CITY", "PS_CITY_6")
								iPSDialogueCounter++
							ENDIF
						ENDIF
					BREAK
	
					CASE PS_SUBSTATE_EXIT
						iInstructionCounter = 1
						iPSDialogueCounter = 1
						PS_SET_HINT_CAM_COORD(PS_GET_CURRENT_CHECKPOINT())
						PS_DLC_City_Landing_State = PS_DLC_CITY_LANDING_LANDING
						PS_INCREMENT_SUBSTATE()
					BREAK
				ENDSWITCH
			BREAK
				
			//Tell player to aim for the landing target and check for the plane landing
			CASE PS_DLC_CITY_LANDING_LANDING
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
 				    	PS_SET_DIST_HUD_ACTIVE(TRUE)
						PS_INCREMENT_SUBSTATE()
						SET_ENTITY_HEALTH(PS_Main.myVehicle, 4000)
						SET_VEHICLE_ENGINE_HEALTH(PS_Main.myVehicle, 4000)
					BREAK
						
					CASE PS_SUBSTATE_UPDATE
					PRINTLN("CITYLAND - Waiting for progress")
						IF PS_DLC_CITY_Fail_Check()
							PS_DLC_City_Landing_State = PS_DLC_CITY_LANDING_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELIF PS_DLC_CITY_Progress_Check()
							PS_INCREMENT_SUBSTATE()
						ELSE
							if bEngineBlown
							//Dialogue for obstacles in the sand
								IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), PS_GET_LAST_CHECKPOINT()) < 1000
									if iPSDialogueCounter = 1 AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND NOT IS_MESSAGE_BEING_DISPLAYED()
										PS_PLAY_DISPATCHER_INSTRUCTION_2_LINES("PS_CITY", "PS_CITY_7", "PS_CITY", "PS_CITY_8")
										iPSDialogueCounter++
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						//Dialogue for stopping
						IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), PS_GET_LAST_CHECKPOINT()) < 80 AND GET_ENTITY_SPEED(PLAYER_PED_ID()) > 0
							if iPSDialogueCounter = 2 AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND NOT IS_MESSAGE_BEING_DISPLAYED()
								PS_PLAY_DISPATCHER_INSTRUCTION("PS_CITY", "PS_CITY_9")
								iPSDialogueCounter++
							ENDIF
						ENDIF
						
						//Stop the player accelarating once landed
						if bDisableAccel
							PRINTLN("SHOULDNT BE ABLE TO ACCEL!")
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_THROTTLE_UP)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_PITCH_UP_ONLY)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MOVE_UP_ONLY)
						ENDIF
						
						//Break boost
						CHECK_PLAYER_BREAKING()	
					BREAK
						
					CASE PS_SUBSTATE_EXIT
					PRINTLN("CITYLAND - AT EXIT STATE")
						PS_UPDATE_RECORDS(PS_GET_CURRENT_CHECKPOINT())
						PS_SET_DIST_HUD_ACTIVE(FALSE)
						
						IF NOT HAS_SOUND_FINISHED(iAlarm)
							STOP_SOUND(iAlarm)
							bAlarmPlay = FALSE
						ENDIF
						
						IF Pilot_School_Data_Check_Mission_Success(PS_Main.myChallengeData, PS_Main.myPlayerData)
							IF DOES_ENTITY_EXIST(PS_Main.myVehicle)
							AND NOT IS_ENTITY_DEAD(PS_Main.myVehicle)
								SET_ENTITY_INVINCIBLE(PS_Main.myVehicle, TRUE)
								BRING_VEHICLE_TO_HALT(PS_Main.myVehicle, 3.0, 1)
							ENDIF
							PS_DLC_City_Landing_State = PS_DLC_CITY_LANDING_SUCCESS
						ELSE
							//PRINTLN("CITY LANDING -WANTED TO FAIL HERE")
							PS_DLC_City_Landing_State = PS_DLC_CITY_LANDING_FAIL
						ENDIF
						IF ePSFailReason = PS_FAIL_TOO_FAR OR ePSFailReason = PS_FAIL_LANDED_VEHICLE
							
							//Check for player landing at the beach but not attaining medal
							IF IS_ENTITY_IN_ANGLED_AREA(PS_Main.myVehicle, vLandingCoord1, vLandingCoord2, 90)
								BRING_VEHICLE_TO_HALT(PS_Main.myVehicle, 3.0, 1)
								PS_Main.myChallengeData.bBronzeOverride = TRUE
								ePSFailReason = PS_FAIL_NO_REASON
								PS_DLC_City_Landing_State = PS_DLC_CITY_LANDING_SUCCESS
							ELSE
								PRINTLN("Plane isn't at the beach, failing challenge.")
							ENDIF
							
						ENDIF
						IF PS_DLC_City_Landing_State = PS_DLC_CITY_LANDING_SUCCESS
							PS_INCREMENT_CHECKPOINT()
						ELSE
							PS_HIDE_CHECKPOINT(PS_GET_TOTAL_CHECKPOINTS())
						ENDIF
						PS_INCREMENT_SUBSTATE()
					BREAK
				ENDSWITCH
			BREAK
			
			//Called on pass
			CASE PS_DLC_CITY_LANDING_SUCCESS
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_TRIGGER_MUSIC(PS_MUSIC_CITY_PASS)
						START_AUDIO_SCENE("DLC_PILOT_CITY_LANDING_END_TRANSITION_SCENE")
						PRINTLN("Entering PS_DLC_CITY_SUCCESS 1")
						PS_UPDATE_RECORDS(PS_GET_LAST_CHECKPOINT())
						IF NOT IS_VEHICLE_FUCKED(PS_Main.myVehicle) AND GET_ENTITY_SPEED(PS_Main.myVehicle) < 5.0
							SET_VEHICLE_UNDRIVEABLE(PS_Main.myVehicle, TRUE)
							SET_VEHICLE_ENGINE_ON(PS_Main.myVehicle, FALSE, TRUE)
						ENDIF
						PS_LESSON_END(TRUE)
						PS_PLAY_LESSON_FINISHED_LINE("PS_CITYPASS", "PS_CITYPASS_1", "PS_CITYPASS_2", "PS_CITYPASS_3", "")
						PS_INCREMENT_SUBSTATE()
					BREAK
					CASE PS_SUBSTATE_UPDATE
						
						//Debug for ending camera
						#IF IS_DEBUG_BUILD
							PRINTLN("IN CITY LANDING SUCCESS UPDATE STATE")
							PRINTLN("SCORE CARD STATE : ", eScoreCardState)
							PRINTLN("END CAM TIMER: ", GET_TIMER_IN_SECONDS_SAFE(PS_Main.myChallengeData.EndCamTimer))
							PRINTLN("FORCE END CAM TIMER: ", GET_TIMER_IN_SECONDS_SAFE(PS_Main.myChallengeData.ForceEndPanCamTimer))
							PRINTLN("IS ANY CONVERSATION ONGOING : ", IS_ANY_CONVERSATION_ONGOING_OR_QUEUED())
						#ENDIF
						
						
						IF NOT IS_PLAYER_DEAD(player_id())							
						OR GET_MANUAL_RESPAWN_STATE()= MRS_READY_FOR_PLACEMENT
							#IF IS_DEBUG_BUILD
								PRINTLN("PLAYER IS ALIVE, IS CONVERSATION ONGOING : ", IS_SCRIPTED_CONVERSATION_ONGOING() )
							#ENDIF
							IF NOT PS_HUD_IS_SCORECARD_ACTIVE()
								PRINTLN("SCORECARD ISN'T ACTIVE PROGRESSING")
								PS_INCREMENT_SUBSTATE()
							ELSE
								PRINTLN("SCORECARD ACTIVE WAITING")
								IF g_bCelebrationScreenIsActive
									//Force sunny weather
									PS_SET_WEATHER_FOR_CHALLENGE(1)
								ENDIF
							ENDIF
						ELSE
							PRINTLN("PLAYER IS DEAD SETTING RESPAWN STATE")
							//Set the player to respawn state if dead and doesn't have respawn state set
							IF IS_PLAYER_DEAD(player_id())
							AND GET_MANUAL_RESPAWN_STATE() = MRS_NULL
								SET_MANUAL_RESPAWN_STATE(MRS_RESPAWN_PLAYER_HIDDEN)
							ENDIF
						ENDIF
					BREAK
					CASE PS_SUBSTATE_EXIT
						bFinishedChallenge = TRUE 
						PS_INCREMENT_SUBSTATE()
					BREAK
				ENDSWITCH
			BREAK
	
			//Called on fail
			CASE PS_DLC_CITY_LANDING_FAIL
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_TRIGGER_MUSIC(PS_MUSIC_CITY_FAIL)
						PRINTLN("Entering PS_DLC_CITY_FAIL")
						if not bEarlyLand
							PS_UPDATE_RECORDS(PS_GET_LAST_CHECKPOINT())
						ENDIF
						IF NOT IS_VEHICLE_FUCKED(PS_Main.myVehicle) AND GET_ENTITY_SPEED(PS_Main.myVehicle) < 5.0
							SET_VEHICLE_UNDRIVEABLE(PS_Main.myVehicle, TRUE)
							SET_VEHICLE_ENGINE_ON(PS_Main.myVehicle, FALSE, TRUE)
						ENDIF
						PS_PLAY_LESSON_FINISHED_LINE("PS_CITYFAIL", "", "", "", "PS_CITYFAIL_1")		
						PS_LESSON_END(FALSE)
						PS_INCREMENT_SUBSTATE()
					BREAK
					CASE PS_SUBSTATE_UPDATE
						IF NOT PS_HUD_IS_SCORECARD_ACTIVE()
							PS_INCREMENT_SUBSTATE()
						ELSE
							//Force sunny weather
							PS_SET_WEATHER_FOR_CHALLENGE(1)
						ENDIF
					BREAK
					CASE PS_SUBSTATE_EXIT
						bFinishedChallenge = TRUE
						PS_INCREMENT_SUBSTATE()
					BREAK
				ENDSWITCH
			BREAK
				
		ENDSWITCH
		RETURN TRUE
	ENDIF
	RETURN FALSE
	
ENDFUNC

//Main cleanup procedure called at end
PROC PS_DLC_CITY_MainCleanup()
	
	//hud
	PS_HUD_SET_GUTTER_ICON_ACTIVE(FALSE)
	PS_HUD_SET_OBJECTIVE_METER_VISIBLE(FALSE)
	PS_SET_TIMER_ACTIVE(FALSE)
	PS_HUD_CLEANUP()
	CLEAR_PRINTS()
	CLEAR_HELP()
	PS_RESET_CHECKPOINT_MGR()
	
	IF NOT HAS_SOUND_FINISHED(iAlarm)
		STOP_SOUND(iAlarm)
		bAlarmPlay = FALSE
	ENDIF
	
	//Cleanup the particle effect
	IF DOES_PARTICLE_FX_LOOPED_EXIST( engineptfx )
		REMOVE_PARTICLE_FX( engineptfx )
	ENDIF
	
	REMOVE_PTFX_ASSET()

	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
	AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), FALSE)
	ENDIF
	
	//Reset engine blown flag
	bEngineBlown = FALSE
	
	bEarlyLand = FALSE
	
	//Clear weather type and make it more normal on leaving
	//Now getting called in HUD lib
	//SET_WEATHER_FOR_FMMC_MISSION(1)


	//player/objects
	PS_SEND_PLAYER_BACK_TO_MENU()
	
	
	//Vehicle
	SET_MODEL_AS_NO_LONGER_NEEDED(VehicleToUse)
	REMOVE_VEHICLE_RECORDING(iPS_PreviewVehicleRecordingID, "PilotSchool")
	
	#IF IS_DEBUG_BUILD
		DEBUG_MESSAGE("******************Cleaning up PS_DLC_CITYLanding.sc******************")
	#ENDIF
ENDPROC
