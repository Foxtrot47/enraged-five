//****************************************************************************************************
//
//	Author: 		Asa Dang
//	Date: 			7/13/11
//	Description:	"Plane Obstacle Course" Challenge for Pilot School. The player has to go through a series
//					of checkpoints placed precariously through the area around the airport. The player needs
//					avoid obstacles while also performing stunts through the coronas
//****************************************************************************************************

USING "minigames_helpers.sch"
USING "Pilot_School_MP_Challenge_Helpers.sch"
//USING "rGeneral_include.sch"

//****************************************************************************************************
//****************************************************************************************************
//	Private functions that are used only by Plane Course
//****************************************************************************************************
//****************************************************************************************************




struct struct_chunck
	float ux,vx
	vector v1,v2,v3
endstruct

struct_chunck chunck[7]


int iThisWave
bool bRoundActive
int attackStage
int activeWave[3]
float fShtHeading
int iCurrentTarget
vector vSlideFrom
int iMovementType
vector vTargetNudge
int iTargetCounter
int iLastSoundTime

bool bResetTargets
bool bLoadTargets[3]
bool bCraneDialoguePlayed

//define prop here as it's not currently in any model enum file


model_names targetPropBot, targetPropSide, targetPropArm, targetPropMover
//debug 
#if IS_DEBUG_BUILD
WIDGET_GROUP_ID shtWidget
bool bDrawDebugTargets
bool bAddTarget


bool bIsLooped
bool bTestTrigger
float fTargetScale = 1.0


bool bDrawOrigin
bool bExport
bool bEditMode


PROC ADD_DEBUG_WIDGET()
	IF DOES_WIDGET_GROUP_EXIST(wDataFile)
		SET_CURRENT_WIDGET_GROUP(wDataFile)
		shtWidget = START_WIDGET_GROUP("TARGETS")
			ADD_WIDGET_BOOL("Edit Mode",bEditMode)
			ADD_WIDGET_BOOL("Reset Targets",bResetTargets)
			ADD_WIDGET_BOOL("Load targets 1",bLoadTargets[0])
			ADD_WIDGET_BOOL("Load targets 2",bLoadTargets[1])
			ADD_WIDGET_BOOL("Load targets 3",bLoadTargets[2])
			
			ADD_WIDGET_BOOL("Draw origin pos",bDrawOrigin)
			ADD_WIDGET_BOOL("Debug Targets",bDrawDebugTargets)
			ADD_WIDGET_INT_SLIDER("Target ID",iTargetCounter,0,29,1)
			ADD_WIDGET_BOOL("Create target",bAddTarget)
			ADD_WIDGET_VECTOR_SLIDER("Nudge target",vTargetNudge,-20.0,20.0,0.1)			
			ADD_WIDGET_FLOAT_SLIDER("Heading",fShtHeading,-360.0,360.0,0.5)
			ADD_WIDGET_VECTOR_SLIDER("Slide offset",vSlideFrom,-20.0,20.0,0.1)
			ADD_WIDGET_INT_SLIDER("Movement Type)",iMovementType,0,9,1)
			ADD_WIDGET_BOOL("Looped",bIsLooped)
			ADD_WIDGET_BOOL("Test Trigger",bTestTrigger)
			ADD_WIDGET_FLOAT_SLIDER("Target Scale",fTargetScale,0.1,5.0,0.01)			
			ADD_WIDGET_BOOL("Test Targets",bRoundActive)
			ADD_WIDGET_BOOL("Export data",bExport)
		STOP_WIDGET_GROUP()
		CLEAR_CURRENT_WIDGET_GROUP(wDataFile)
	ENDIF

ENDPROC
#endif

enum PS_SR_enumBrief
	SR_BRIEF_NONE,
	SR_BRIEF_GET_TO_BUILDING_SITE,
	SR_FIRST_INSTRUCTION,
	SR_BRIEF_SHOOT_TARGETS,	
	SR_HELP_USE_GUN_CAM_VIEW,
	SR_REMOVE_CAM_HELP,
	SR_SHOOT_GOD_TEXT,
	SR_SHOOT_HELP_TEXT,
	SR_BRIEF_USE_GUN_CAM_VIEW,
	SR_BRIEF_NEXT_TARGETS,
	SR_BRIEF_USE_ROCKETS,
	SR_HELP_USE_ROCKETS,
	SR_BRIEF_HIDDEN_TARGET,
	SR_REMOVE_ROCKET_HELP,
	SR_BRIEF_TARGETS_IN_COVER,
	SR_BRIEF_FINAL_ROUND,
	SR_BRIEF_FINAL_GOD_TEXT,
	SR_BRIEF_UPDATE_ON_PROGRESS_1,
	SR_BRIEF_UPDATE_ON_PROGRESS_2,
	SR_BRIEF_UPDATE_ON_PROGRESS_3
endenum

PS_SR_enumBrief SR_currentBrief = SR_BRIEF_NONE
int iDialogueTimer

PROC PS_TRIGGER_SR_BRIEF(PS_SR_enumBrief thisSRBrief)
	cprintln(debug_trevor3,"Trigger ",enum_to_int(thisSRBrief))
	SR_currentBrief = thisSRBrief
ENDPROC

//had to include here rather than rgeneral_include.sch
FUNC FLOAT GET_HEADING_FROM_COORDS(vector oldCoords,vector newCoords, bool invert=true)
	float heading
	float dX = newCoords.x - oldCoords.x
	float dY = newCoords.y - oldCoords.y
	if dY != 0
		heading = ATAN2(dX,dY)
	ELSE
		if dX < 0
			heading = -90
		ELSE
			heading = 90
		ENDIF
	ENDIF
	
	//flip because for some odd reason the coders think west is a heading of 90 degrees, so this'll match the output of commands such as GET_ENTITY_HEADING()
	IF invert = TRUE 	
		heading *= -1.0
		//below not necessary but helps for debugging
		IF heading < 0
			heading += 360.0
		ENDIF
	ENDIF
	
	RETURN heading
ENDFUNC

PROC PS_PLAY_SR_BRIEF()

	//cprintln(debug_trevor3,ENUM_TO_INT(GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_HELI)))

	SWITCH SR_currentBrief
		CASE SR_BRIEF_GET_TO_BUILDING_SITE
			PS_PLAY_DISPATCHER_INSTRUCTION("PS_SRa","PS_SRa_1")
			SR_currentBrief = SR_FIRST_INSTRUCTION
		BREAK
		CASE SR_FIRST_INSTRUCTION
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				PRINT_NOW("PS_SHTR_DIA2",DEFAULT_GOD_TEXT_TIME,1)
				SR_currentBrief = SR_BRIEF_NONE
			ENDIF
		BREAK		//FM_IHELP_BUZ
		CASE SR_BRIEF_SHOOT_TARGETS
			PS_PLAY_DISPATCHER_INSTRUCTION("PS_SRa","PS_SRa_2")								
			SR_currentBrief = SR_SHOOT_GOD_TEXT			
		BREAK		
		CASE SR_SHOOT_GOD_TEXT
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				PRINT_NOW("PS_SHTR_INS2",DEFAULT_GOD_TEXT_TIME,1)
				SR_currentBrief = SR_SHOOT_HELP_TEXT
				iDialogueTimer = GET_GAME_TIMER() + 1000
			ENDIF
		BREAK
		CASE SR_SHOOT_HELP_TEXT
			IF GET_GAME_TIMER() > iDialogueTimer
				PRINT_HELP("PS_SHTR_HLP1",6000)
				SR_currentBrief = SR_BRIEF_USE_GUN_CAM_VIEW
				iDialogueTimer = GET_GAME_TIMER() + 8000
			ENDIF
		BREAK
		CASE SR_BRIEF_USE_GUN_CAM_VIEW
			IF IS_HELP_MESSAGE_BEING_DISPLAYED()
				IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL,INPUT_VEH_ATTACK2)
					CLEAR_HELP()
				ENDIF
			ENDIF
			
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()			
			AND NOT IS_MESSAGE_BEING_DISPLAYED()
				IF GET_GAME_TIMER() > iDialogueTimer
					IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_HELI) != CAM_VIEW_MODE_FIRST_PERSON
						PS_PLAY_DISPATCHER_INSTRUCTION("PS_SRa","PS_SRa_3")
						
						SR_currentBrief = SR_HELP_USE_GUN_CAM_VIEW
						iDialogueTimer = GET_GAME_TIMER() + 1000
					ELSE
						SR_currentBrief = SR_BRIEF_NONE
					ENDIF
				ENDIF
			ENDIF
			
		BREAK		
		CASE SR_HELP_USE_GUN_CAM_VIEW
			IF GET_GAME_TIMER() > iDialogueTimer
				PRINT_HELP("PS_SHTR_HLP2")
				SR_currentBrief = SR_REMOVE_CAM_HELP
			ENDIF
		BREAK
		
		CASE SR_REMOVE_CAM_HELP
			IF IS_HELP_MESSAGE_BEING_DISPLAYED()
				IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_HELI) = CAM_VIEW_MODE_FIRST_PERSON
					CLEAR_HELP()
					SR_currentBrief = SR_BRIEF_NONE
				ENDIF
			ENDIF
		BREAK
		
		CASE SR_BRIEF_NEXT_TARGETS
			PS_PLAY_DISPATCHER_INSTRUCTION("PS_SRa","PS_SRa_4")						
			SR_currentBrief = SR_BRIEF_USE_ROCKETS
		BREAK						
		
		CASE SR_BRIEF_USE_ROCKETS
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				CLEAR_HELP()
				PS_PLAY_DISPATCHER_INSTRUCTION("PS_SRa","PS_SRa_5")
				iDialogueTimer = GET_GAME_TIMER() + 1000
				SR_currentBrief = SR_HELP_USE_ROCKETS
			ENDIF
		BREAK
		
		CASE SR_HELP_USE_ROCKETS
			IF GET_GAME_TIMER() > iDialogueTimer
				PRINT_HELP("FM_IHELP_BUZ")
				SR_currentBrief = SR_BRIEF_HIDDEN_TARGET
			ENDIF
		BREAK
		
		CASE SR_REMOVE_ROCKET_HELP
			IF IS_HELP_MESSAGE_BEING_DISPLAYED()
				WEAPON_TYPE chopperWeapon
				GET_CURRENT_PED_VEHICLE_WEAPON(PLAYER_PED_ID(),chopperWeapon)
				//CPRINTLN(debug_trevor3,"Weapon = ",chopperWeapon," WEAPONTYPE_VEHICLE_PLAYER_BULLET = ",WEAPONTYPE_VEHICLE_PLAYER_BULLET," WEAPONTYPE_VEHICLE_PLAYER_BUZZARD = ",WEAPONTYPE_VEHICLE_PLAYER_BUZZARD," WEAPONTYPE_VEHICLE_ROCKET= ",WEAPONTYPE_VEHICLE_ROCKET)
				IF chopperWeapon != WEAPONTYPE_VEHICLE_PLAYER_BUZZARD
					CLEAR_HELP()
				ENDIF
			ELSE
				SR_currentBrief = SR_BRIEF_NONE
			ENDIF
			
		BREAK
		
		CASE SR_BRIEF_HIDDEN_TARGET
		
			IF IS_HELP_MESSAGE_BEING_DISPLAYED()
				WEAPON_TYPE chopperWeapon
				GET_CURRENT_PED_VEHICLE_WEAPON(PLAYER_PED_ID(),chopperWeapon)
				//CPRINTLN(debug_trevor3,"Weapon = ",chopperWeapon," WEAPONTYPE_VEHICLE_PLAYER_BULLET = ",WEAPONTYPE_VEHICLE_PLAYER_BULLET," WEAPONTYPE_VEHICLE_PLAYER_BUZZARD = ",WEAPONTYPE_VEHICLE_PLAYER_BUZZARD," WEAPONTYPE_VEHICLE_ROCKET= ",WEAPONTYPE_VEHICLE_ROCKET)
				IF chopperWeapon != WEAPONTYPE_VEHICLE_PLAYER_BUZZARD
					CLEAR_HELP()
				ENDIF
			ENDIF
		
			IF iThisWave = 5
				PS_PLAY_DISPATCHER_INSTRUCTION("PS_SRa","PS_SRa_6")
				SR_currentBrief = SR_REMOVE_ROCKET_HELP
			ENDIF
		BREAK
		
		CASE SR_BRIEF_FINAL_ROUND
			PS_PLAY_DISPATCHER_INSTRUCTION_2_LINES("PS_SRa","PS_SRa_7","PS_SRa","PS_SRa_8")
			SR_currentBrief = SR_BRIEF_FINAL_GOD_TEXT
		BREAK
		
		CASE SR_BRIEF_FINAL_GOD_TEXT
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				PRINT_NOW("PS_SHTR_INS5",DEFAULT_GOD_TEXT_TIME,1)
				SR_currentBrief = SR_BRIEF_UPDATE_ON_PROGRESS_1
				iDialogueTimer = GET_GAME_TIMER() + 35000
				bCraneDialoguePlayed = FALSE
			ENDIF
		BREAK
		
		CASE SR_BRIEF_UPDATE_ON_PROGRESS_1
		FALLTHRU
		CASE SR_BRIEF_UPDATE_ON_PROGRESS_2
		FALLTHRU
		CASE SR_BRIEF_UPDATE_ON_PROGRESS_3
		
			IF NOT bCraneDialoguePlayed
				IF targetList[11].status > WAITING 
				AND targetList[11].status < EXPLODING
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						PS_PLAY_DISPATCHER_INSTRUCTION_CONVERSATION("PS_SRCRANE")	
						bCraneDialoguePlayed = TRUE
					ENDIF				
				ENDIF
			ENDIF
		
			IF GET_GAME_TIMER() > iDialogueTimer
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF PS_Main.targetsDestroyed < 15
						IF (SR_currentBrief = SR_BRIEF_UPDATE_ON_PROGRESS_1 AND PS_Main.targetsDestroyed >  3)
						OR (SR_currentBrief = SR_BRIEF_UPDATE_ON_PROGRESS_2 AND PS_Main.targetsDestroyed >  6)
						OR (SR_currentBrief = SR_BRIEF_UPDATE_ON_PROGRESS_3 AND PS_Main.targetsDestroyed >  10)												
							PS_PLAY_DISPATCHER_INSTRUCTION_CONVERSATION("PS_SRON")																		
						ELSE						
							PS_PLAY_DISPATCHER_INSTRUCTION_CONVERSATION("PS_SRBEHIND")																			
						ENDIF				
						
						IF SR_currentBrief = SR_BRIEF_UPDATE_ON_PROGRESS_1 
							SR_currentBrief = SR_BRIEF_UPDATE_ON_PROGRESS_2 
						ELIF SR_currentBrief = SR_BRIEF_UPDATE_ON_PROGRESS_2 
							SR_currentBrief = SR_BRIEF_UPDATE_ON_PROGRESS_3
						ELSE
							SR_currentBrief = SR_BRIEF_NONE
						ENDIF
						
						iDialogueTimer = GET_GAME_TIMER() + 30000
					ELSE
						//player reached the minimum ofr brzone. no more comments.
						SR_currentBrief = SR_BRIEF_NONE
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

int nextDebrisClearCheckTime
int iCurrentCheck

PROC CLEAR_DEBRIS()
	IF GET_GAME_TIMER() > nextDebrisClearCheckTime
		nextDebrisClearCheckTime = GET_GAME_TIMER() + 200
		vector vCheck
		SWITCH iCurrentCheck
			CASE 0 vCheck = << 85.40, -372.65, 40.91>> BREAK
			CASE 1 vCheck = << 92.80, -375.67, 40.88>> BREAK
			CASE 2 vCheck = << 99.43, -377.83, 40.88>> BREAK
			CASE 3 vCheck = << 105.03, -380.76, 40.77>> BREAK
			CASE 4 vCheck = << 107.54, -369.34, 54.50>> BREAK
			CASE 5 vCheck = << 95.54, -369.15, 54.50>> BREAK
			CASE 6 vCheck = << 112.72, -356.70, 54.50>> BREAK
			CASE 7 vCheck = << 88.22, -358.15, 54.51>> BREAK
			CASE 8 vCheck = << 81.07, -356.06, 54.51>> BREAK
			CASE 9 vCheck = << 73.69, -347.60, 66.14>> BREAK //6
			CASE 10 vCheck = << 79.18, -349.87, 66.20>> BREAK //6
			CASE 11 vCheck = << 83.64, -351.39, 66.20>> BREAK //6
			CASE 12 vCheck = << 99.75, -356.84, 66.32>> BREAK
			CASE 13 vCheck = << 110.54, -359.82, 66.32>> BREAK
		ENDSWITCH
				
		IF NOT IS_POINT_VISIBLE(vCheck,6,150)
			CLEAR_AREA_OF_OBJECTS(vCheck,6)
		ENDIF
		
		iCurrentCheck++ 
		IF iCurrentCheck > 13
			iCurrentCheck = 0
		ENDIF
	ENDIF
ENDPROC

PROC RESET_ALL_TARGETS(bool deleteEntities = false)

	int i
	REPEAT COUNT_OF(targetList) i
		
		targetList[i].status = DOESNT_EXIST
		targetList[i].vCoord = <<0,0,0>>
		targetList[i].fShtHeading = 0
		targetList[i].iMovementType = 0
		targetList[i].vSlideIn = <<0,0,0>>
		targetList[i].triggerTime = 0.0
		targetList[i].slideFlag = 0
		targetList[i].wave = 0	
		IF DOES_BLIP_EXIST(targetList[i].blip)
			REMOVE_BLIP(targetList[i].blip)
		ENDIF
		IF deleteEntities
		
			IF NETWORK_DOES_NETWORK_ID_EXIST(targetList[i].entity_NETID)
				PS_DELETE_NET_ID(targetList[i].entity_NETID)		
			ENDIF
			
			IF NETWORK_DOES_NETWORK_ID_EXIST(targetList[i].entityBase_NETID)
				PS_DELETE_NET_ID(targetList[i].entityBase_NETID)		
			ENDIF
		ENDIF
	ENDREPEAT
	
	
ENDPROC

FUNC INT GET_RANDOM_NEXT_WAVE()
	int iTargetCheck

	
	iTargetCheck = GET_RANDOM_INT_IN_RANGE(0,COUNT_OF(targetList)-1)
	int iCheckCounter
	WHILE iCheckCounter < COUNT_OF(targetList)-1
		iCheckCounter++
		IF targetList[iTargetCheck].status = WAITING			
			RETURN targetList[iTargetCheck].wave
		ELSE
			iTargetCheck++
			IF iTargetCheck = COUNT_OF(targetList) iTargetCheck = 0 ENDIF
		ENDIF
	ENDWHILE
	
	RETURN -1
	
ENDFUNC

PROC RESET_ALL_NON_ACTIVE_WAVES()
	int i
	REPEAT COUNT_OF(targetList) i
		IF targetList[i].wave != activeWave[0]
		AND targetList[i].wave != activeWave[1]
		AND targetList[i].wave != activeWave[2]
			IF targetList[i].status  > EXPLODING
				targetList[i].status = WAITING
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC


FUNC BOOL IS_WAVE_COMPLETE(int thisWave)
	int i

	REPEAT COUNT_OF(targetList) i
		IF targetList[i].status < EXPLODING
		AND targetList[i].status != DOESNT_EXIST
			IF targetList[i].wave = thisWave			
				RETURN FALSE			
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_WAVE_ACTIVE(int thisWave)
	int i
	REPEAT COUNT_OF(targetList) i
		IF targetList[i].wave = thisWave
		AND targetList[i].status > WAITING
			RETURN TRUE
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

PROC TRIGGER_WAVE(int thisWave)
	int i

	REPEAT COUNT_OF(targetList) i
		IF targetList[i].STATUS != DOESNT_EXIST
			IF targetList[i].wave = thisWave
				targetList[i].status = SPAWNING				
				targetList[i].triggerTime = 0.0
				targetList[i].slideFlag = 0
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC PREP_ATTACK_STAGE(int next_round, int nextWave = 0)
	attackStage = next_round
	//bResetTargets = TRUE
	bLoadTargets[attackStage] = TRUE
	PS_Main.targetsDestroyed = 0
	iThisWave = nextWave
ENDPROC

PROC RESET_ROUND_TARGET_DATA()
	int i
	REPEAT COUNT_OF(targetList) i
		
		IF targetList[i].status = DESTROYED		
			targetList[i].status = RESET
		ENDIF
		
		IF targetList[i].status = EXPLODING
			targetList[i].status = EXPLODE_AND_RESET
		ENDIF
						
	ENDREPEAT
ENDPROC

PROC CONTROL_TARGETS()
			
	int i
	IF bRoundActive
	
		IF attackStage < 2
			IF IS_WAVE_ACTIVE(iThisWave)
			AND IS_WAVE_COMPLETE(iThisWave)
				iThisWave++				
			ENDIF
		
			IF NOT IS_WAVE_ACTIVE(iThisWave)
				TRIGGER_WAVE(iThisWave)
			ENDIF
		ELSE

			int activeWaves
			IF activeWave[0] != -1 
				IF NOT IS_WAVE_COMPLETE( activeWave[0])
					activeWaves++ 
				ELSE
					activeWave[0] = -1
				ENDIF
			ENDIF
			IF activeWave[1] != -1 
				IF NOT IS_WAVE_COMPLETE( activeWave[1])
					activeWaves++ 
				ELSE
					activeWave[1] = -1
				ENDIF
			ENDIF
			IF activeWave[2] != -1 
				IF NOT IS_WAVE_COMPLETE( activeWave[2])
					activeWaves++ 
				ELSE
					activeWave[2] = -1
				ENDIF
			ENDIF
		
			IF activeWaves < 3		
				iThisWave = GET_RANDOM_NEXT_WAVE()
				IF iThisWave != -1
					IF activeWave[0] = -1
						activeWave[0] = iThisWave
						TRIGGER_WAVE(iThisWave)
					ELIF activeWave[1] = -1
						activeWave[1] = iThisWave
						TRIGGER_WAVE(iThisWave)
					ELIF activeWave[2] = -1
						activeWave[2] = iThisWave
						TRIGGER_WAVE(iThisWave)
					ENDIF
				ELSE
					RESET_ALL_NON_ACTIVE_WAVES()
				ENDIF
			ENDIF
		ENDIF
	ENDIF	
	
	If bLoadTargets[0]
			bLoadTargets[0] = FALSE
			targetList[0].status = WAITING
			targetList[0].vCoord = <<34.8018, -423.2538, 54.5387>>
			targetList[0].vSlideIn = <<-2.2250, -4.9750, 0.0000>>
			targetList[0].fShtHeading = 70.2500
			targetList[0].iMovementType = 0
			targetList[0].loop = false
			targetList[0].wave = 0
			targetList[1].status = WAITING
			targetList[1].vCoord = <<28.8060, -439.9322, 54.5605>>
			targetList[1].vSlideIn = <<3.7750, 10.0000, 0.0000>>
			targetList[1].fShtHeading = 66.8750
			targetList[1].iMovementType = 0
			targetList[1].loop = false
			targetList[1].wave = 1
			targetList[2].status = WAITING
			targetList[2].vCoord = <<32.6731, -450.3864, 55.3403>>
			targetList[2].vSlideIn = <<0.0000, 0.0000, 0.0000>>
			targetList[2].fShtHeading = 67.3750
			targetList[2].iMovementType = 1
			targetList[2].loop = false
			targetList[2].wave = 2
			targetList[3].status = WAITING
			targetList[3].vCoord = <<39.7702, -437.7629, 64.0358>>
			targetList[3].vSlideIn = <<-1.6750, -4.5150, 0.0000>>
			targetList[3].fShtHeading = 70.5000
			targetList[3].iMovementType = 0
			targetList[3].loop = false
			targetList[3].wave = 3
			targetList[4].status = WAITING
			targetList[4].vCoord = <<34.7633, -450.5908, 64.0358>>
			targetList[4].vSlideIn = <<1.5250, 3.9500, 0.0000>>
			targetList[4].fShtHeading = 69.2500
			targetList[4].iMovementType = 0
			targetList[4].loop = false
			targetList[4].wave = 3
			
			RESET_ROUND_TARGET_DATA()
			iTargetCounter=0
			vTargetNudge = <<0,0,0>>
			fShtHeading = targetList[0].fShtHeading
			vSlideFrom = targetList[0].vSlideIn
			iMovementType = targetList[0].iMovementType	
			#if IS_DEBUG_BUILD
			bIsLooped = targetList[0].loop
			#endif
		ENDIF
		
		IF bLoadTargets[1]
			bLoadTargets[1]= FALSE
			targetList[5].status = WAITING
			targetList[5].vCoord = <<60.6886, -378.0977, 44.5640>>
			targetList[5].vSlideIn = <<-11.3500, -0.3500, 0.0000>>
			targetList[5].fShtHeading = 30.1250
			targetList[5].iMovementType = 0
			targetList[5].loop = false
			targetList[5].wave = 4			
			targetList[6].status = WAITING
			targetList[6].vCoord = <<58.1259, -381.1689, 44.5640>>
			targetList[6].vSlideIn = <<-7.0500, 1.3750, 0.0000>>
			targetList[6].fShtHeading = 35.0000
			targetList[6].iMovementType = 0
			targetList[6].wave = 4
			targetList[6].loop = false
			targetList[7].status = WAITING
			targetList[7].vCoord = <<54.6405, -382.4567, 44.5640>>
			targetList[7].vSlideIn = <<-3.4250, 0.9250, 0.0000>>
			targetList[7].fShtHeading = 28.7500
			targetList[7].iMovementType = 0
			targetList[7].loop = false
			targetList[7].wave = 4			 
			targetList[8].status = WAITING
			targetList[8].vCoord = <<60.3000, -380.5759, 54.2652>> 
			targetList[8].vSlideIn = <<-9.6250, 3.2000, 0.0000>>
			targetList[8].fShtHeading = 70.2500
			targetList[8].iMovementType = 4
			targetList[8].loop = FALSE
			targetList[8].pauseTime = 0
			targetList[8].wave = 5		
			targetList[9].status = WAITING
			targetList[9].vCoord = <<57.9256, -387.9974, 63.8064>>
			targetList[9].vSlideIn = <<-5.3250, -15.4500, 0.0000>>
			targetList[9].fShtHeading = 67.8750
			targetList[9].iMovementType = 0
			targetList[9].loop = TRUE
			targetList[9].pauseTime = 2000			
			targetList[9].wave = 6
			targetList[10].status = WAITING
			targetList[10].vCoord = <<51.3199, -402.5112, 63.8055>>
			targetList[10].vSlideIn = <<5.1750, 15.8250, 0.0000>>
			targetList[10].fShtHeading = 65.7500
			targetList[10].iMovementType = 0
			targetList[10].wave = 6
			targetList[10].loop = TRUE
			targetList[10].pauseTime = 2000
			
			RESET_ROUND_TARGET_DATA()
			iTargetCounter=0
			vTargetNudge = <<0,0,0>>
			fShtHeading = targetList[0].fShtHeading
			vSlideFrom = targetList[0].vSlideIn
			iMovementType = targetList[0].iMovementType	
			#if IS_DEBUG_BUILD
			bIsLooped = targetList[0].loop
			#endif
			
		ENDIF
		
	
		
		IF bLoadTargets[2]
			bLoadTargets[2]= FALSE
			targetList[0].status = WAITING
			targetList[0].vCoord = <<110.5213, -371.7523, 54.7514>>
			targetList[0].vSlideIn = <<-14.1000, 1.7250, 0.0000>>
			targetList[0].fShtHeading = -21.3750
			targetList[0].iMovementType = 0
			targetList[0].wave = 0
			targetList[0].loop = TRUE
			targetList[0].pauseTime = 2000
			targetList[1].status = WAITING
			targetList[1].vCoord = <<106.6401, -379.8150, 61.3164>>
			targetList[1].vSlideIn = <<-5.6250, 1.9750, 0.0000>>
			targetList[1].fShtHeading = -16.2500
			targetList[1].iMovementType = 3
			targetList[1].wave = 1
			targetList[1].loop = TRUE
			targetList[1].pauseTime = 2000
			targetList[2].status = WAITING
			targetList[2].vCoord = <<111.7201, -370.5628, 60.1449>>
			targetList[2].vSlideIn = <<-6.7000, 2.5250, 0.0000>>
			targetList[2].fShtHeading = -19.6250
			targetList[2].iMovementType = 3
			targetList[2].wave = 2
			targetList[2].loop = TRUE
			targetList[2].pauseTime = 2000
			targetList[3].status = WAITING
			targetList[3].vCoord = <<89.4529, -360.4659, 54.5132>>
			targetList[3].vSlideIn = <<-10.6250, 3.8500, 0.0000>>
			targetList[3].fShtHeading = -19.7500
			targetList[3].iMovementType = 4
			targetList[3].wave = 3
			targetList[3].loop = TRUE
			targetList[3].pauseTime = 3000
			targetList[4].status = WAITING
			targetList[4].vCoord = <<108.5912, -365.3093, 54.5014>>
			targetList[4].vSlideIn = <<5.7200, 17.0250, 0.0000>>
			targetList[4].fShtHeading = -18.3750
			targetList[4].iMovementType = 4
			targetList[4].wave = 4
			targetList[4].loop = TRUE
			targetList[4].pauseTime = 3000
			targetList[5].status = WAITING
			targetList[5].vCoord = <<111.7019, -364.4259, 54.5014>>
			targetList[5].vSlideIn = <<5.6000, 14.3500, 0.0000>>
			targetList[5].fShtHeading = -18.3750
			targetList[5].iMovementType = 4
			targetList[5].wave = 4
			targetList[5].loop = TRUE
			targetList[5].pauseTime = 2000
			targetList[6].status = WAITING
			targetList[6].vCoord = <<95.7450, -371.3139, 57.0514>>
			targetList[6].vSlideIn = <<0.0000, 0.0000, -2.7250>>
			targetList[6].fShtHeading = -19.75
			targetList[6].iMovementType = 5
			targetList[6].wave = 5
			targetList[6].loop = TRUE
			targetList[6].pauseTime = 2000
			targetList[7].status = WAITING
			targetList[7].vCoord = <<92.6534, -370.7400, 57.1096>>
			targetList[7].vSlideIn = <<0.0000, 0.0000, -2.8250>>
			targetList[7].fShtHeading = -17.6250
			targetList[7].iMovementType = 5
			targetList[7].wave = 5
			targetList[7].loop = TRUE
			targetList[7].pauseTime = 2000
			targetList[8].status = WAITING
			targetList[8].vCoord = <<102.2, -375.1, 66.0431>>
			targetList[8].vSlideIn = <<0.0000, 0.0000, 0.0000>>
			targetList[8].fShtHeading = -12.5000
			targetList[8].iMovementType = 2
			targetList[8].wave = 6
			targetList[8].loop = true
			targetList[8].pauseTime = 2000
		//	targetList[9].status = WAITING
			targetList[9].vCoord = <<89.6201, -344.7105, 74.1008>>
			targetList[9].vSlideIn = <<-7.8750, 2.8000, 0.0000>>
			targetList[9].fShtHeading = -20.2500
			targetList[9].iMovementType = 3
			targetList[9].wave = 7
			targetList[9].loop = true
			targetList[9].pauseTime = 2000
		//	targetList[10].status = WAITING
			targetList[10].vCoord = <<126.1410, -346.6832, 67.6468>>
			targetList[10].vSlideIn = <<0.0000, 0.0000, 19.9500>>
			targetList[10].fShtHeading = 0.0000
			targetList[10].iMovementType = 6
			targetList[10].wave = 8
			targetList[10].loop = true
			targetList[10].pauseTime = 2000
			targetList[11].status = WAITING
			targetList[11].vCoord = <<71.3127, -362.0575, 101.4098>>
			targetList[11].vSlideIn = <<20.0000, 5.6500, 0.0000>>
			targetList[11].fShtHeading = 13.1250
			targetList[11].iMovementType = 3
			targetList[11].wave = 9
			targetList[11].loop = true
			targetList[11].pauseTime = 2000
			targetList[12].status = WAITING
			targetList[12].vCoord = <<82.9192, -351.1265, 66.4518>>
			targetList[12].vSlideIn = <<-20.0000, 6.8000, 0.0000>>
			targetList[12].fShtHeading = 20.5000
			targetList[12].iMovementType = 4
			targetList[12].wave = 10
			targetList[12].loop = false
			targetList[12].pauseTime = 0
			targetList[13].status = WAITING
			targetList[13].vCoord = <<80.0507, -353.7059, 66.4268>>
			targetList[13].vSlideIn = <<-18.9400, 6.2250, 0.0000>>
			targetList[13].fShtHeading = 24.1250
			targetList[13].iMovementType = 4
			targetList[13].wave = 10
			targetList[13].loop = false
			targetList[13].pauseTime = 0
			targetList[14].status = WAITING
			targetList[14].vCoord = <<85.4063, -348.3799, 66.2018>>
			targetList[14].vSlideIn = <<-20.0000, 6.4000, 0.0000>>
			targetList[14].fShtHeading = 22.7500
			targetList[14].iMovementType = 4
			targetList[14].wave = 10
			targetList[14].loop = false
			targetList[14].pauseTime = 0
			targetList[15].status = WAITING
			targetList[15].vCoord = <<96.6031, -353.5905, 66.1264>>
			targetList[15].vSlideIn = <<15.9750, -5.6500, 0.0000>>
			targetList[15].fShtHeading = -20.1250
			targetList[15].iMovementType = 7
			targetList[15].wave = 11
			targetList[15].loop = true
			targetList[15].pauseTime = 2000
			targetList[16].status = WAITING
			targetList[16].vCoord = <<86.4900, -373.7033, 65.7471>>
			targetList[16].vSlideIn = <<0.0000, 0.0000, -19.2500>>
			targetList[16].fShtHeading = -16.5000
			targetList[16].iMovementType = 8
			targetList[16].wave = 12
			targetList[16].loop = true
			targetList[16].pauseTime = 2000
			targetList[17].status = WAITING
			targetList[17].vCoord = <<90.8793, -374.4383, 49.6603>>
			targetList[17].vSlideIn = <<14.5500, -5.3000, 0.0000>>
			targetList[17].fShtHeading = -19.7500
			targetList[17].iMovementType = 3
			targetList[17].wave = 13
			targetList[17].loop = true
			targetList[17].pauseTime = 2000

			RESET_ROUND_TARGET_DATA()
			iTargetCounter=0
			vTargetNudge = <<0,0,0>>
			fShtHeading = targetList[0].fShtHeading
			vSlideFrom = targetList[0].vSlideIn
			iMovementType = targetList[0].iMovementType
			#if IS_DEBUG_BUILD
			bIsLooped = targetList[0].loop
			#endif
		ENDIF
		
	REPEAT COUNT_OF(targetList) i
	
		IF targetList[i].status >= MOVING
		AND targetList[i].status <= IN_POSITION
			IF DOES_ENTITY_EXIST(targetList[i].entity)
			
				IF GET_ENTITY_HEALTH(targetList[i].entity) < 10000			
				OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE,GET_ENTITY_COORDS(targetList[i].entity),4.0)
					
					//IF IS_ENTITY_ATTACHED(targetList[i].entity) DETACH_ENTITY(targetList[i].entity) ENDIF
					//SET_ENTITY_DYNAMIC(targetList[i].Entity,true)
					//APPLY_FORCE_TO_ENTITY(targetList[i].entity,APPLY_TYPE_IMPULSE,<<400,0,0>>,<<0,0,0>>,0,true,true,false)
					targetList[i].status = EXPLODING
					targetList[i].triggerTime = 0.0
					FREEZE_ENTITY_POSITION(targetList[i].entity,false)
					PS_Main.targetsDestroyed++
				ELIF HAS_ENTITY_BEEN_DAMAGED_BY_ANY_VEHICLE(targetList[i].entity)
				OR HAS_OBJECT_BEEN_BROKEN(targetList[i].entity)
					targetList[i].status = EXPLODING
					targetList[i].triggerTime = 0.0
					FREEZE_ENTITY_POSITION(targetList[i].entity,false)
					cprintln(debug_trevor3,"Damaged only?")
					
				ENDIF
				
				
			ENDIF
		ENDIF
	
		SWITCH targetList[i].status
			CASE SPAWNING								
				CLEAR_AREA(targetList[i].vCoord+targetList[i].vSlideIn,3.0,true)
				SWITCH targetList[i].iMovementType
					CASE 0
					FALLTHRU
					CASE 4
						IF PS_CREATE_NET_OBJ(targetList[i].entityBase_NETID,targetPropMover,targetList[i].vCoord+targetList[i].vSlideIn)
						AND PS_CREATE_NET_OBJ(targetList[i].Entity_NETID,targetPropArm,targetList[i].vCoord)						
							targetList[i].entityBase = NET_TO_OBJ(targetList[i].entityBase_NETID)
							targetList[i].Entity = NET_TO_OBJ(targetList[i].Entity_NETID)
							ATTACH_ENTITY_TO_ENTITY(targetList[i].Entity,targetList[i].entityBase,0,<<0,0,0.4>>,<<0,0,0>>,true,false,true)													
							FREEZE_ENTITY_POSITION(targetList[i].entityBase,true)
							targetList[i].status = MOVING
						ENDIF
					BREAK
					CASE 1
					FALLTHRU
					CASE 5
					FALLTHRU
					CASE 7
						IF PS_CREATE_NET_OBJ(targetList[i].Entity_NETID,targetPropBot,targetList[i].vCoord+targetList[i].vSlideIn)
							targetList[i].Entity = NET_TO_OBJ(targetList[i].Entity_NETID)
							SET_ENTITY_ROTATION(targetList[i].entity,<<0,0,targetList[i].fShtHeading>>)
							FREEZE_ENTITY_POSITION(targetList[i].Entity,true)							
							targetList[i].status = MOVING
						ENDIF
					BREAK	
					CASE 2
					FALLTHRU
					CASE 6
					FALLTHRU
					CASE 8
						IF PS_CREATE_NET_OBJ(targetList[i].Entity_NETID,targetPropSide,targetList[i].vCoord+targetList[i].vSlideIn)
							targetList[i].Entity = NET_TO_OBJ(targetList[i].Entity_NETID) 
							SET_ENTITY_ROTATION(targetList[i].entity,<<0,0,targetList[i].fShtHeading>>)
							FREEZE_ENTITY_POSITION(targetList[i].Entity	,true)			
							targetList[i].status = MOVING
						ENDIF
					BREAK
					CASE 3
						IF PS_CREATE_NET_OBJ(targetList[i].Entity_NETID,targetPropBot,targetList[i].vCoord+targetList[i].vSlideIn)
							targetList[i].Entity = NET_TO_OBJ(targetList[i].Entity_NETID) 
							SET_ENTITY_ROTATION(targetList[i].entity,<<0,0,targetList[i].fShtHeading>>)
							FREEZE_ENTITY_POSITION(targetList[i].Entity,true)							
							targetList[i].status = MOVING
						ENDIF
					BREAK						
				ENDSWITCH
				
				If targetList[i].status = MOVING
					SET_ENTITY_HEALTH(targetList[i].entity,10000)
					SET_ENTITY_CAN_BE_DAMAGED(targetList[i].Entity,true)
					
					IF DOES_ENTITY_EXIST(targetList[i].entityBase)
						SET_ENTITY_PROOFS(targetList[i].entityBase,true,true,true,true,true,true,true,true)
					ENDIF
										
					PLAY_SOUND_FROM_COORD(-1, "Spawn",targetList[i].vCoord+targetList[i].vSlideIn, "DLC_PILOT_Shooting_Range_Sounds")
					
					i--
				ENDIF
			BREAK
			
			 CASE MOVING	
				IF NOT DOES_ENTITY_EXIST(targetList[i].Entity)
					//targetList[i].Entity = CREATE_OBJECT(targetPropArm,targetList[i].vCoord+targetList[i].vSlideIn)
				ELSE
					DRAW_LIGHT_WITH_RANGE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(targetList[i].Entity,<<0,1,2>>),255,0,0,10.0,1.0)
					DRAW_LIGHT_WITH_RANGE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(targetList[i].Entity,<<0,-0.5,2>>),255,255,255,3.0,3.0)
				ENDIF
						
				SWITCH targetList[i].iMovementType
					CASE 0 //slide in. Arm already vertical
						SWITCH targetList[i].slideFlag
							CASE 0
								targetList[i].triggerTime += timestep() / ((VMAG(targetList[i].vSlideIn)*0.4))	
								IF targetList[i].triggerTime >= 1.0 targetList[i].triggerTime = 1.0 ENDIF
								IF NOT IS_ENTITY_DEAD(targetList[i].Entity)
								AND NOT IS_ENTITY_DEAD(targetList[i].entityBase)								
									SET_ENTITY_COORDS_NO_OFFSET(targetList[i].entityBase,targetList[i].vCoord+(targetList[i].vSlideIn*(1.-targetList[i].triggerTime)))
									SET_ENTITY_ROTATION(targetList[i].entityBase,<<0,0,GET_HEADING_FROM_COORDS(targetList[i].vCoord,targetList[i].vCoord+targetList[i].vSlideIn)>>)
									ATTACH_ENTITY_TO_ENTITY(targetList[i].Entity,targetList[i].entityBase,0,<<0,0,0.4>>,<<0,0,targetList[i].fShtHeading>>-<<0,0,GET_ENTITY_HEADING(targetList[i].entityBase)>>,TRUE,false,true)
									
									//Get sound ID where appropriate
									IF targetList[i].sound = -1
										targetList[i].sound = GET_SOUND_ID()
									ELSE
										//If finished play it again
										IF HAS_SOUND_FINISHED( targetList[i].sound )
											PLAY_SOUND_FROM_ENTITY(targetList[i].sound,"move",targetList[i].Entity,"DLC_PILOT_Shooting_Range_Sounds")
										ENDIF
									ENDIF
								ENDIF
								
								IF targetList[i].triggerTime >= 1.0
									STOP_SOUND(targetList[i].sound)
									PLAY_SOUND_FROM_ENTITY(-1,"stop",targetList[i].Entity,"DLC_PILOT_Shooting_Range_Sounds")
									IF targetList[i].loop
										targetList[i].slideFlag++
										targetList[i].triggerTime=0
									ELSE										
										targetList[i].status = IN_POSITION
									ENDIF
								ENDIF
							BREAK
							CASE 1
								targetList[i].triggerTime += timestep()
								IF targetList[i].triggerTime * 2000 > targetList[i].pauseTime
									targetList[i].slideFlag++
									targetList[i].triggerTime=1.0
									
									//Get sound ID where appropriate
									IF targetList[i].sound = -1
										targetList[i].sound = GET_SOUND_ID()
									ELSE
										IF HAS_SOUND_FINISHED( targetList[i].sound )
											PLAY_SOUND_FROM_ENTITY(targetList[i].sound,"move",targetList[i].Entity,"DLC_PILOT_Shooting_Range_Sounds")
										ENDIF
									ENDIF
								ENDIF
							BREAK
							CASE 2
								targetList[i].triggerTime -= timestep() / ((VMAG(targetList[i].vSlideIn)*0.4))	
								IF targetList[i].triggerTime <= 0.0 targetList[i].triggerTime = 0.0 ENDIF
								
								IF NOT IS_ENTITY_DEAD(targetList[i].Entity)
								AND NOT IS_ENTITY_DEAD(targetList[i].entityBase)
									SET_ENTITY_COORDS_NO_OFFSET(targetList[i].entityBase,targetList[i].vCoord+(targetList[i].vSlideIn*(1.-targetList[i].triggerTime)))
									SET_ENTITY_ROTATION(targetList[i].entityBase,<<0,0,GET_HEADING_FROM_COORDS(targetList[i].vCoord,targetList[i].vCoord+targetList[i].vSlideIn)>>)
									ATTACH_ENTITY_TO_ENTITY(targetList[i].Entity,targetList[i].entityBase,0,<<0,0,0.4>>,<<0,0,targetList[i].fShtHeading>>-<<0,0,GET_ENTITY_HEADING(targetList[i].entityBase)>>,TRUE,false,true)
								ENDIF
								
								IF targetList[i].triggerTime <= 0.0
									STOP_SOUND(targetList[i].sound)
									PLAY_SOUND_FROM_ENTITY(-1,"stop",targetList[i].Entity,"DLC_PILOT_Shooting_Range_Sounds")
									IF targetList[i].loop
										targetList[i].slideFlag++
										targetList[i].triggerTime=0
									ELSE
										targetList[i].status = IN_POSITION
									ENDIF
								ENDIF
							BREAK
							CASE 3
								targetList[i].triggerTime += timestep()
								IF targetList[i].triggerTime * 1000 > targetList[i].pauseTime
									targetList[i].slideFlag=0
									targetList[i].triggerTime=0
								ENDIF
							BREAK
						ENDSWITCH
					BREAK
					
					CASE 1 //rotate up from base (with loop back down)
						SWITCH targetList[i].slideFlag
							CASE 0					
							
								IF targetList[i].triggerTime = 0.0
									//Get sound ID where appropriate
									IF targetList[i].sound = -1
										targetList[i].sound = GET_SOUND_ID()
									ELSE
										IF HAS_SOUND_FINISHED( targetList[i].sound )
											PLAY_SOUND_FROM_ENTITY(targetList[i].sound,"move",targetList[i].Entity,"DLC_PILOT_Shooting_Range_Sounds")
										ENDIF
									ENDIF
								ENDIF
							
								targetList[i].triggerTime += timestep() / 2.0
								IF targetList[i].triggerTime >= 1.0 targetList[i].triggerTime = 1.0 ENDIF
														
								SET_ENTITY_ROTATION(targetList[i].entity,<<-90+(90.0*targetList[i].triggerTime),0,targetList[i].fShtHeading>>)
								
								IF targetList[i].triggerTime >= 1.0
									STOP_SOUND(targetList[i].sound)
									PLAY_SOUND_FROM_ENTITY(-1,"stop",targetList[i].Entity,"DLC_PILOT_Shooting_Range_Sounds")
									IF targetList[i].loop
										targetList[i].slideFlag++
										targetList[i].triggerTime=0
									ELSE
										targetList[i].triggerTime = 1.0
										targetList[i].status = IN_POSITION
									ENDIF
								ENDIF
							BREAK
							CASE 1
								targetList[i].triggerTime += timestep()
								IF targetList[i].triggerTime * 2000 > targetList[i].pauseTime
									//Get sound ID where appropriate
									IF targetList[i].sound = -1
										targetList[i].sound = GET_SOUND_ID()
									ELSE
										IF HAS_SOUND_FINISHED( targetList[i].sound )
											PLAY_SOUND_FROM_ENTITY(targetList[i].sound,"move",targetList[i].Entity,"DLC_PILOT_Shooting_Range_Sounds")
										ENDIF
									ENDIF
									targetList[i].slideFlag++
									targetList[i].triggerTime=1.0
								ENDIF
							BREAK
							CASE 2				
								targetList[i].triggerTime -= timestep() / 2.0
								IF targetList[i].triggerTime <= 0.0								
									STOP_SOUND(targetList[i].sound)
									PLAY_SOUND_FROM_ENTITY(-1,"stop",targetList[i].Entity,"DLC_PILOT_Shooting_Range_Sounds")
									targetList[i].slideFlag=3
									targetList[i].triggerTime=0											
								ENDIF
								SET_ENTITY_ROTATION(targetList[i].entity,<<-90+(90.0*targetList[i].triggerTime),0,targetList[i].fShtHeading>>)
							BREAK
							CASE 3
								targetList[i].triggerTime += timestep()
								IF targetList[i].triggerTime * 1000 > targetList[i].pauseTime
									targetList[i].slideFlag=0
									targetList[i].triggerTime=0.0
								ENDIF
							BREAK
						ENDSWITCH															
					BREAK
					CASE 2 //rotate in from right (with loop back)
						SWITCH targetList[i].slideFlag
							CASE 0
								IF targetList[i].triggerTime = 0.0
									//Get sound ID where appropriate
									IF targetList[i].sound = -1
										targetList[i].sound = GET_SOUND_ID()
									ELSE
										IF HAS_SOUND_FINISHED( targetList[i].sound )
											PLAY_SOUND_FROM_ENTITY(targetList[i].sound,"move",targetList[i].Entity,"DLC_PILOT_Shooting_Range_Sounds")
										ENDIF
									ENDIF
								ENDIF
								targetList[i].triggerTime += timestep() / 2.0
								IF targetList[i].triggerTime >= 1.0 targetList[i].triggerTime = 1.0 ENDIF
								SET_ENTITY_ROTATION(targetList[i].entity,<<0.0,0.0,targetList[i].fShtHeading-(180.0*(1.0-targetList[i].triggerTime))>>)
						
								IF targetList[i].triggerTime >= 1.0
									STOP_SOUND(targetList[i].sound)
									PLAY_SOUND_FROM_ENTITY(-1,"stop",targetList[i].Entity,"DLC_PILOT_Shooting_Range_Sounds")
									IF targetList[i].loop
										targetList[i].slideFlag++
										targetList[i].triggerTime=0
									ELSE
										targetList[i].triggerTime = 1.0
										targetList[i].status = IN_POSITION
									ENDIF
								ENDIF
							BREAK
							CASE 1
								targetList[i].triggerTime += timestep()
								IF targetList[i].triggerTime * 2000 > targetList[i].pauseTime
									//Get sound ID where appropriate
									IF targetList[i].sound = -1
										targetList[i].sound = GET_SOUND_ID()
									ELSE
										IF HAS_SOUND_FINISHED( targetList[i].sound )
											PLAY_SOUND_FROM_ENTITY(targetList[i].sound,"move",targetList[i].Entity,"DLC_PILOT_Shooting_Range_Sounds")
										ENDIF
									ENDIF
									targetList[i].slideFlag++
									targetList[i].triggerTime=1.0
								ENDIF
							BREAK
							CASE 2				
								targetList[i].triggerTime -= timestep() / 2.0
								IF targetList[i].triggerTime <= 0.0								
									targetList[i].slideFlag=3
									targetList[i].triggerTime=0																				
									STOP_SOUND(targetList[i].sound)
									PLAY_SOUND_FROM_ENTITY(-1,"stop",targetList[i].Entity,"DLC_PILOT_Shooting_Range_Sounds")
								ENDIF
								SET_ENTITY_ROTATION(targetList[i].entity,<<0.0,0.0,targetList[i].fShtHeading-(180.0*(1.0-targetList[i].triggerTime))>>)
							BREAK
							CASE 3
								targetList[i].triggerTime += timestep()
								IF targetList[i].triggerTime * 1000 > targetList[i].pauseTime
									
									targetList[i].slideFlag=0
									targetList[i].triggerTime=0.0
								ENDIF
							BREAK
						ENDSWITCH
					BREAK
					CASE 3 //rotate down from top edge then slide
						SWITCH targetList[i].slideFlag
							CASE 0			
								IF targetList[i].triggerTime = 0.0
									//Get sound ID where appropriate
									IF targetList[i].sound = -1
										targetList[i].sound = GET_SOUND_ID()
									ELSE
										IF HAS_SOUND_FINISHED( targetList[i].sound )
											PLAY_SOUND_FROM_ENTITY(targetList[i].sound,"move",targetList[i].Entity,"DLC_PILOT_Shooting_Range_Sounds")
										ENDIF
									ENDIF
									cprintln(debug_trevor3,"move sound")
								ENDIF
								targetList[i].triggerTime += timestep() / 2.0
								IF targetList[i].triggerTime >= 1.0 targetList[i].triggerTime = 1.0 ENDIF
								SET_ENTITY_COORDS_NO_OFFSET(targetList[i].entity,targetList[i].vCoord+targetList[i].vSlideIn)								
								SET_ENTITY_ROTATION(targetList[i].entity,<<90+(-90.0*targetList[i].triggerTime),180,targetList[i].fShtHeading>>)
			
								IF targetList[i].triggerTime >= 1.0
									
									IF targetList[i].loop
										targetList[i].slideFlag++
										targetList[i].triggerTime=0
									ELSE
										targetList[i].triggerTime = 1.0
										targetList[i].status = IN_POSITION
									ENDIF
								ENDIF
							BREAK
							CASE 1
								targetList[i].triggerTime += timestep() / ((VMAG(targetList[i].vSlideIn)*0.4))	
								IF targetList[i].triggerTime >= 1.0 targetList[i].triggerTime = 1.0 ENDIF
																
								SET_ENTITY_COORDS_NO_OFFSET(targetList[i].entity,targetList[i].vCoord+(targetList[i].vSlideIn*(1.0-targetList[i].triggerTime)))							
								
								IF targetList[i].triggerTime >= 1.0
									STOP_SOUND(targetList[i].sound)
									PLAY_SOUND_FROM_ENTITY(-1,"stop",targetList[i].Entity,"DLC_PILOT_Shooting_Range_Sounds")
									println(debug_trevor3,"stop sound")
									IF targetList[i].loop
										targetList[i].slideFlag++
										targetList[i].triggerTime=0
									ELSE
										targetList[i].status = IN_POSITION
									ENDIF
								ENDIF
							BREAK
							CASE 2
								targetList[i].triggerTime += timestep()
								IF targetList[i].triggerTime * 1000 > targetList[i].pauseTime
									//Get sound ID where appropriate
									IF targetList[i].sound = -1
										targetList[i].sound = GET_SOUND_ID()
									ELSE
										IF HAS_SOUND_FINISHED( targetList[i].sound )
											PLAY_SOUND_FROM_ENTITY(targetList[i].sound,"move",targetList[i].Entity,"DLC_PILOT_Shooting_Range_Sounds")
										ENDIF
									ENDIF
									cprintln(debug_trevor3,"move back sound")
									targetList[i].slideFlag++
									targetList[i].triggerTime=1.0
								ENDIF
							BREAK
							CASE 3
								targetList[i].triggerTime -= timestep() / ((VMAG(targetList[i].vSlideIn)*0.4))	
								IF targetList[i].triggerTime <= 0.0 targetList[i].triggerTime = 0.0 ENDIF
																
								SET_ENTITY_COORDS_NO_OFFSET(targetList[i].entity,targetList[i].vCoord+(targetList[i].vSlideIn*(1.-targetList[i].triggerTime)))							
								
								IF targetList[i].triggerTime <= 0.0
									STOP_SOUND(targetList[i].sound)
									PLAY_SOUND_FROM_ENTITY(-1,"stop",targetList[i].Entity,"DLC_PILOT_Shooting_Range_Sounds")
									cprintln(debug_trevor3,"stop sound")
									IF targetList[i].loop
										targetList[i].slideFlag++
										targetList[i].triggerTime=0
									ELSE
										targetList[i].status = IN_POSITION
									ENDIF
								ENDIF
							BREAK
							CASE 4
								targetList[i].triggerTime += timestep()
								IF targetList[i].triggerTime * 1000 > targetList[i].pauseTime
									targetList[i].slideFlag=1									
									targetList[i].triggerTime=0.0
									//Get sound ID where appropriate
									IF targetList[i].sound = -1
										targetList[i].sound = GET_SOUND_ID()
									ELSE
										IF HAS_SOUND_FINISHED( targetList[i].sound )
											PLAY_SOUND_FROM_ENTITY(targetList[i].sound,"move",targetList[i].Entity,"DLC_PILOT_Shooting_Range_Sounds")
										ENDIF
									ENDIF
								ENDIF
							BREAK
						ENDSWITCH
					BREAK
					CASE 4 //slide in and have target rotate up. (with loop back to start)
						SWITCH targetList[i].slideFlag
							CASE 0
								IF targetList[i].triggerTime = 0.0
									//Get sound ID where appropriate
									IF targetList[i].sound = -1
										targetList[i].sound = GET_SOUND_ID()
									ELSE
										IF HAS_SOUND_FINISHED( targetList[i].sound )
											PLAY_SOUND_FROM_ENTITY(targetList[i].sound,"move",targetList[i].Entity,"DLC_PILOT_Shooting_Range_Sounds")
										ENDIF
									ENDIF
								ENDIF
								targetList[i].triggerTime += timestep() / ((VMAG(targetList[i].vSlideIn)*0.4))	
								IF targetList[i].triggerTime >= 1.0 targetList[i].triggerTime = 1.0 ENDIF
			
								IF NOT IS_ENTITY_DEAD(targetList[i].Entity)
								AND NOT IS_ENTITY_DEAD(targetList[i].entityBase)																	
									SET_ENTITY_COORDS_NO_OFFSET(targetList[i].entityBase,targetList[i].vCoord+(targetList[i].vSlideIn*(1.-targetList[i].triggerTime)))
									SET_ENTITY_ROTATION(targetList[i].entityBase,<<0,0,GET_HEADING_FROM_COORDS(targetList[i].vCoord,targetList[i].vCoord+targetList[i].vSlideIn)>>)
									ATTACH_ENTITY_TO_ENTITY(targetList[i].Entity,targetList[i].entityBase,0,<<0,0,0.4>>,<<-90+(90.0*targetList[i].triggerTime),0,targetList[i].fShtHeading>>-<<0,0,GET_ENTITY_HEADING(targetList[i].entityBase)>>,TRUE,false,true)
								ENDIF
								
								IF targetList[i].triggerTime >= 1.0
									STOP_SOUND(targetList[i].sound)
									PLAY_SOUND_FROM_ENTITY(-1,"stop",targetList[i].Entity,"DLC_PILOT_Shooting_Range_Sounds")
									IF targetList[i].loop
										targetList[i].slideFlag++
										targetList[i].triggerTime=0
									ELSE
										targetList[i].status = IN_POSITION
									ENDIF
								ENDIF
							BREAK
							CASE 1
								targetList[i].triggerTime += timestep()
								IF targetList[i].triggerTime * 2000 > targetList[i].pauseTime
									//Get sound ID where appropriate
									IF targetList[i].sound = -1
										targetList[i].sound = GET_SOUND_ID()
									ELSE
										IF HAS_SOUND_FINISHED( targetList[i].sound )
											PLAY_SOUND_FROM_ENTITY(targetList[i].sound,"move",targetList[i].Entity,"DLC_PILOT_Shooting_Range_Sounds")
										ENDIF
									ENDIF
									targetList[i].slideFlag++
									targetList[i].triggerTime=1.0
								ENDIF
							BREAK
							CASE 2
								targetList[i].triggerTime -= timestep() / ((VMAG(targetList[i].vSlideIn)*0.4))	
								IF targetList[i].triggerTime <= 0.0 targetList[i].triggerTime = 0.0 endif
								
								IF NOT IS_ENTITY_DEAD(targetList[i].Entity)
								AND NOT IS_ENTITY_DEAD(targetList[i].entityBase)																
									SET_ENTITY_COORDS_NO_OFFSET(targetList[i].entityBase,targetList[i].vCoord+(targetList[i].vSlideIn*(1.-targetList[i].triggerTime)))
									SET_ENTITY_ROTATION(targetList[i].entityBase,<<0,0,GET_HEADING_FROM_COORDS(targetList[i].vCoord,targetList[i].vCoord+targetList[i].vSlideIn)>>)
									ATTACH_ENTITY_TO_ENTITY(targetList[i].Entity,targetList[i].entityBase,0,<<0,0,0.4>>,<<0,0,targetList[i].fShtHeading>>-<<0,0,GET_ENTITY_HEADING(targetList[i].entityBase)>>,TRUE,false,true)
								ENDIF
								
								IF targetList[i].triggerTime <= 0.0									
									targetList[i].slideFlag++
									targetList[i].triggerTime=0									
								ENDIF
							BREAK
							CASE 3
								targetList[i].triggerTime += timestep()
								IF targetList[i].triggerTime * 1000 > targetList[i].pauseTime
									STOP_SOUND(targetList[i].sound)
									PLAY_SOUND_FROM_ENTITY(-1,"stop",targetList[i].Entity,"DLC_PILOT_Shooting_Range_Sounds")
									targetList[i].slideFlag=4
									targetList[i].triggerTime=0
								ENDIF
							BREAK
							CASE 4
								targetList[i].triggerTime += timestep() / ((VMAG(targetList[i].vSlideIn)*0.4))	
								IF targetList[i].triggerTime >= 1.0 targetList[i].triggerTime = 1.0 endif
								
								IF NOT IS_ENTITY_DEAD(targetList[i].Entity)
								AND NOT IS_ENTITY_DEAD(targetList[i].entityBase)
									SET_ENTITY_COORDS_NO_OFFSET(targetList[i].entityBase,targetList[i].vCoord+(targetList[i].vSlideIn*(1.-targetList[i].triggerTime)))
									SET_ENTITY_ROTATION(targetList[i].entityBase,<<0,0,GET_HEADING_FROM_COORDS(targetList[i].vCoord,targetList[i].vCoord+targetList[i].vSlideIn)>>)
									ATTACH_ENTITY_TO_ENTITY(targetList[i].Entity,targetList[i].entityBase,0,<<0,0,0.4>>,<<0,0,targetList[i].fShtHeading>>-<<0,0,GET_ENTITY_HEADING(targetList[i].entityBase)>>,TRUE,false,true)
								ENDIF
								
								IF targetList[i].triggerTime >= 1.0
									//Get sound ID where appropriate
									IF targetList[i].sound = -1
										targetList[i].sound = GET_SOUND_ID()
									ELSE
										IF HAS_SOUND_FINISHED( targetList[i].sound )
											PLAY_SOUND_FROM_ENTITY(targetList[i].sound,"move",targetList[i].Entity,"DLC_PILOT_Shooting_Range_Sounds")
										ENDIF
									ENDIF
									targetList[i].slideFlag=1
									targetList[i].triggerTime=0									
								ENDIF
							BREAK
						ENDSWITCH
					BREAK
					CASE 5 //just loop slide up and down
						SWITCH targetList[i].slideFlag
							CASE 0			
								IF targetList[i].triggerTime = 0.0
									//Get sound ID where appropriate
									IF targetList[i].sound = -1
										targetList[i].sound = GET_SOUND_ID()
									ELSE
										IF HAS_SOUND_FINISHED( targetList[i].sound )
											PLAY_SOUND_FROM_ENTITY(targetList[i].sound,"move",targetList[i].Entity,"DLC_PILOT_Shooting_Range_Sounds")	
										ENDIF
									ENDIF
								ENDIF
								targetList[i].triggerTime += timestep() / ((VMAG(targetList[i].vSlideIn)*0.4))
								IF targetList[i].triggerTime >= 1.0 targetList[i].triggerTime = 1.0 ENDIF
								SET_ENTITY_COORDS_NO_OFFSET(targetList[i].entity,targetList[i].vCoord+(targetList[i].vSlideIn*(1.0-targetList[i].triggerTime)))							
								IF targetList[i].triggerTime >= 1.0
									STOP_SOUND(targetList[i].sound)
									PLAY_SOUND_FROM_ENTITY(-1,"stop",targetList[i].Entity,"DLC_PILOT_Shooting_Range_Sounds")
									IF targetList[i].loop
										targetList[i].slideFlag++
										targetList[i].triggerTime=0
									ELSE
										targetList[i].triggerTime = 1.0
										targetList[i].status = IN_POSITION
									ENDIF
								ENDIF
							BREAK						
							CASE 1
								targetList[i].triggerTime += timestep()
								IF targetList[i].triggerTime * 1000 > targetList[i].pauseTime
									//Get sound ID where appropriate
									IF targetList[i].sound = -1
										targetList[i].sound = GET_SOUND_ID()
									ELSE
										IF HAS_SOUND_FINISHED( targetList[i].sound )
											PLAY_SOUND_FROM_ENTITY(targetList[i].sound,"move",targetList[i].Entity,"DLC_PILOT_Shooting_Range_Sounds")
										ENDIF
									ENDIF
									targetList[i].slideFlag++
									targetList[i].triggerTime=1.0
								ENDIF
							BREAK
							CASE 2
								targetList[i].triggerTime -= timestep() / ((VMAG(targetList[i].vSlideIn)*0.4))	
								IF targetList[i].triggerTime <= 0.0 targetList[i].triggerTime = 0.0 ENDIF
																
								SET_ENTITY_COORDS_NO_OFFSET(targetList[i].entity,targetList[i].vCoord+(targetList[i].vSlideIn*(1.0-targetList[i].triggerTime)))							
								
								IF targetList[i].triggerTime <= 0.0
									STOP_SOUND(targetList[i].sound)
									PLAY_SOUND_FROM_ENTITY(-1,"stop",targetList[i].Entity,"DLC_PILOT_Shooting_Range_Sounds")
									IF targetList[i].loop
										targetList[i].slideFlag++
										targetList[i].triggerTime=0
									ELSE
										targetList[i].status = IN_POSITION
									ENDIF
								ENDIF
							BREAK
							CASE 3
								targetList[i].triggerTime += timestep()
								IF targetList[i].triggerTime * 1000 > targetList[i].pauseTime
									targetList[i].slideFlag=0
									targetList[i].triggerTime=0.0
								ENDIF
							BREAK
						ENDSWITCH
					BREAK
					CASE 6 //rotate from right then slide
						SWITCH targetList[i].slideFlag
							CASE 0				
								IF targetList[i].triggerTime = 0.0
									//Get sound ID where appropriate
									IF targetList[i].sound = -1
										targetList[i].sound = GET_SOUND_ID()
									ELSE
										IF HAS_SOUND_FINISHED( targetList[i].sound )
											PLAY_SOUND_FROM_ENTITY(targetList[i].sound,"move",targetList[i].Entity,"DLC_PILOT_Shooting_Range_Sounds")
										ENDIF
									ENDIF
								ENDIF
								targetList[i].triggerTime += timestep() / 2.0
								IF targetList[i].triggerTime >= 1.0 targetList[i].triggerTime = 1.0 ENDIF
								SET_ENTITY_COORDS_NO_OFFSET(targetList[i].entity,targetList[i].vCoord+targetList[i].vSlideIn)								
								SET_ENTITY_ROTATION(targetList[i].entity,<<0.0,0.0,targetList[i].fShtHeading-(180.0*(1.0-targetList[i].triggerTime))>>)								
								IF targetList[i].triggerTime >= 1.0
									IF targetList[i].loop
										targetList[i].slideFlag++
										targetList[i].triggerTime=0
									ELSE
										targetList[i].triggerTime = 1.0
										targetList[i].status = IN_POSITION
									ENDIF
								ENDIF
							BREAK
							CASE 1
								targetList[i].triggerTime += timestep() / ((VMAG(targetList[i].vSlideIn)*0.4))	
								IF targetList[i].triggerTime >= 1.0 targetList[i].triggerTime = 1.0 ENDIF
																
								SET_ENTITY_COORDS_NO_OFFSET(targetList[i].entity,targetList[i].vCoord+(targetList[i].vSlideIn*(1.0-targetList[i].triggerTime)))							
								
								IF targetList[i].triggerTime >= 1.0
									STOP_SOUND(targetList[i].sound)
									PLAY_SOUND_FROM_ENTITY(-1,"stop",targetList[i].Entity,"DLC_PILOT_Shooting_Range_Sounds")
									IF targetList[i].loop
										targetList[i].slideFlag++
										targetList[i].triggerTime=0
									ELSE
										targetList[i].status = IN_POSITION
									ENDIF
								ENDIF
							BREAK
							CASE 2
								targetList[i].triggerTime += timestep()
								IF targetList[i].triggerTime * 1000 > targetList[i].pauseTime
									//Get sound ID where appropriate
									IF targetList[i].sound = -1
										targetList[i].sound = GET_SOUND_ID()
									ELSE
										IF HAS_SOUND_FINISHED( targetList[i].sound )
											PLAY_SOUND_FROM_ENTITY(targetList[i].sound,"move",targetList[i].Entity,"DLC_PILOT_Shooting_Range_Sounds")
										ENDIF
									ENDIF
									targetList[i].slideFlag++
									targetList[i].triggerTime=1.0
								ENDIF
							BREAK
							CASE 3
								targetList[i].triggerTime -= timestep() / ((VMAG(targetList[i].vSlideIn)*0.4))	
								IF targetList[i].triggerTime <= 0.0 targetList[i].triggerTime = 0.0 ENDIF
																
								SET_ENTITY_COORDS_NO_OFFSET(targetList[i].entity,targetList[i].vCoord+(targetList[i].vSlideIn*(1.-targetList[i].triggerTime)))							
								
								IF targetList[i].triggerTime <= 0.0
									STOP_SOUND(targetList[i].sound)
									PLAY_SOUND_FROM_ENTITY(-1,"stop",targetList[i].Entity,"DLC_PILOT_Shooting_Range_Sounds")
									IF targetList[i].loop
										targetList[i].slideFlag++
										targetList[i].triggerTime=0
									ELSE
										targetList[i].status = IN_POSITION
									ENDIF
								ENDIF
							BREAK
							CASE 4
								targetList[i].triggerTime += timestep()
								IF targetList[i].triggerTime * 1000 > targetList[i].pauseTime
									targetList[i].slideFlag=1
									targetList[i].triggerTime=0.0
									//Get sound ID where appropriate
									IF targetList[i].sound = -1
										targetList[i].sound = GET_SOUND_ID()
									ELSE
										IF HAS_SOUND_FINISHED( targetList[i].sound )
											PLAY_SOUND_FROM_ENTITY(targetList[i].sound,"move",targetList[i].Entity,"DLC_PILOT_Shooting_Range_Sounds")
										ENDIF
									ENDIF
								ENDIF
							BREAK
						ENDSWITCH
					BREAK
					CASE 7 //rotate from bottom then slide
						SWITCH targetList[i].slideFlag
							CASE 0	
								IF targetList[i].triggerTime = 0.0
									//Get sound ID where appropriate
									IF targetList[i].sound = -1
										targetList[i].sound = GET_SOUND_ID()
									ELSE
										IF HAS_SOUND_FINISHED( targetList[i].sound )
											PLAY_SOUND_FROM_ENTITY(targetList[i].sound,"move",targetList[i].Entity,"DLC_PILOT_Shooting_Range_Sounds")
										ENDIF
									ENDIF
								ENDIF
								targetList[i].triggerTime += timestep() / 2.0
								IF targetList[i].triggerTime >= 1.0 targetList[i].triggerTime = 1.0 ENDIF
								SET_ENTITY_COORDS_NO_OFFSET(targetList[i].entity,targetList[i].vCoord+targetList[i].vSlideIn)								
								SET_ENTITY_ROTATION(targetList[i].entity,<<-90+(90.0*targetList[i].triggerTime),0,targetList[i].fShtHeading>>)
				
								IF targetList[i].triggerTime >= 1.0
									
									IF targetList[i].loop
										targetList[i].slideFlag++
										targetList[i].triggerTime=0
									ELSE
										targetList[i].triggerTime = 1.0
										targetList[i].status = IN_POSITION
									ENDIF
								ENDIF
							BREAK
							CASE 1
								targetList[i].triggerTime += timestep() / ((VMAG(targetList[i].vSlideIn)*0.4))	
								IF targetList[i].triggerTime >= 1.0 targetList[i].triggerTime = 1.0 ENDIF
																
								SET_ENTITY_COORDS_NO_OFFSET(targetList[i].entity,targetList[i].vCoord+(targetList[i].vSlideIn*(1.0-targetList[i].triggerTime)))							
								
								IF targetList[i].triggerTime >= 1.0
									STOP_SOUND(targetList[i].sound)
									PLAY_SOUND_FROM_ENTITY(-1,"stop",targetList[i].Entity,"DLC_PILOT_Shooting_Range_Sounds")
									IF targetList[i].loop
										targetList[i].slideFlag++
										targetList[i].triggerTime=0
									ELSE
										targetList[i].status = IN_POSITION
									ENDIF
								ENDIF
							BREAK
							CASE 2
								targetList[i].triggerTime += timestep()
								IF targetList[i].triggerTime * 1000 > targetList[i].pauseTime
									targetList[i].slideFlag++
									targetList[i].triggerTime=1.0
									//Get sound ID where appropriate
									IF targetList[i].sound = -1
										targetList[i].sound = GET_SOUND_ID()
									ELSE
										IF HAS_SOUND_FINISHED( targetList[i].sound )
											PLAY_SOUND_FROM_ENTITY(targetList[i].sound,"move",targetList[i].Entity,"DLC_PILOT_Shooting_Range_Sounds")
										ENDIF
									ENDIF
								ENDIF
							BREAK
							CASE 3
								targetList[i].triggerTime -= timestep() / ((VMAG(targetList[i].vSlideIn)*0.4))	
								IF targetList[i].triggerTime <= 0.0 targetList[i].triggerTime = 0.0 ENDIF
																
								SET_ENTITY_COORDS_NO_OFFSET(targetList[i].entity,targetList[i].vCoord+(targetList[i].vSlideIn*(1.-targetList[i].triggerTime)))							
								
								IF targetList[i].triggerTime <= 0.0
									STOP_SOUND(targetList[i].sound)
									PLAY_SOUND_FROM_ENTITY(-1,"stop",targetList[i].Entity,"DLC_PILOT_Shooting_Range_Sounds")
									IF targetList[i].loop
										targetList[i].slideFlag++
										targetList[i].triggerTime=0
									ELSE
										targetList[i].status = IN_POSITION
									ENDIF
								ENDIF
							BREAK
							CASE 4
								targetList[i].triggerTime += timestep()
								IF targetList[i].triggerTime * 1000 > targetList[i].pauseTime
									targetList[i].slideFlag=1
									targetList[i].triggerTime=0.0
									//Get sound ID where appropriate
									IF targetList[i].sound = -1
										targetList[i].sound = GET_SOUND_ID()
									ELSE
										IF HAS_SOUND_FINISHED( targetList[i].sound )
											PLAY_SOUND_FROM_ENTITY(targetList[i].sound,"move",targetList[i].Entity,"DLC_PILOT_Shooting_Range_Sounds")
										ENDIF
									ENDIF
								ENDIF
							BREAK
						ENDSWITCH
					BREAK
					CASE 8 //rotate from right 90 deg then slide
						SWITCH targetList[i].slideFlag
							CASE 0			
								IF targetList[i].triggerTime = 0.0
									//Get sound ID where appropriate
									IF targetList[i].sound = -1
										targetList[i].sound = GET_SOUND_ID()
									ELSE
										IF HAS_SOUND_FINISHED( targetList[i].sound )
											PLAY_SOUND_FROM_ENTITY(targetList[i].sound,"move",targetList[i].Entity,"DLC_PILOT_Shooting_Range_Sounds")
										ENDIF
									ENDIF
								ENDIF
								targetList[i].triggerTime += timestep() / 2.0
								IF targetList[i].triggerTime >= 1.0 targetList[i].triggerTime = 1.0 ENDIF
								SET_ENTITY_COORDS_NO_OFFSET(targetList[i].entity,targetList[i].vCoord+targetList[i].vSlideIn)								
								SET_ENTITY_ROTATION(targetList[i].entity,<<0.0,0.0,targetList[i].fShtHeading-(90.0*(1.0-targetList[i].triggerTime))>>)
					
								IF targetList[i].triggerTime >= 1.0
									IF targetList[i].loop
										targetList[i].slideFlag++
										targetList[i].triggerTime=0
									ELSE
										targetList[i].triggerTime = 1.0
										targetList[i].status = IN_POSITION
									ENDIF
								ENDIF
							BREAK
							CASE 1
								targetList[i].triggerTime += timestep() / ((VMAG(targetList[i].vSlideIn)*0.4))	
								IF targetList[i].triggerTime >= 1.0 targetList[i].triggerTime = 1.0 ENDIF
																
								SET_ENTITY_COORDS_NO_OFFSET(targetList[i].entity,targetList[i].vCoord+(targetList[i].vSlideIn*(1.0-targetList[i].triggerTime)))							
								
								IF targetList[i].triggerTime >= 1.0
									STOP_SOUND(targetList[i].sound)
									PLAY_SOUND_FROM_ENTITY(-1,"stop",targetList[i].Entity,"DLC_PILOT_Shooting_Range_Sounds")
									IF targetList[i].loop
										targetList[i].slideFlag++
										targetList[i].triggerTime=0
									ELSE
										targetList[i].status = IN_POSITION
									ENDIF
								ENDIF
							BREAK
							CASE 2
								targetList[i].triggerTime += timestep()
								IF targetList[i].triggerTime * 1000 > targetList[i].pauseTime
									targetList[i].slideFlag++
									targetList[i].triggerTime=1.0
									//Get sound ID where appropriate
									IF targetList[i].sound = -1
										targetList[i].sound = GET_SOUND_ID()
									ELSE
										IF HAS_SOUND_FINISHED( targetList[i].sound )
											PLAY_SOUND_FROM_ENTITY(targetList[i].sound,"move",targetList[i].Entity,"DLC_PILOT_Shooting_Range_Sounds")
										ENDIF
									ENDIF
								ENDIF
							BREAK
							CASE 3
								targetList[i].triggerTime -= timestep() / ((VMAG(targetList[i].vSlideIn)*0.4))	
								IF targetList[i].triggerTime <= 0.0 targetList[i].triggerTime = 0.0 ENDIF
																
								SET_ENTITY_COORDS_NO_OFFSET(targetList[i].entity,targetList[i].vCoord+(targetList[i].vSlideIn*(1.-targetList[i].triggerTime)))							
								
								IF targetList[i].triggerTime <= 0.0
									STOP_SOUND(targetList[i].sound)
									PLAY_SOUND_FROM_ENTITY(-1,"stop",targetList[i].Entity,"DLC_PILOT_Shooting_Range_Sounds")
									IF targetList[i].loop
										targetList[i].slideFlag++
										targetList[i].triggerTime=0
									ELSE
										targetList[i].status = IN_POSITION
									ENDIF
								ENDIF
							BREAK
							CASE 4
								targetList[i].triggerTime += timestep()
								IF targetList[i].triggerTime * 1000 > targetList[i].pauseTime
									targetList[i].slideFlag=1
									targetList[i].triggerTime=0.0
									//Get sound ID where appropriate
									IF targetList[i].sound = -1
										targetList[i].sound = GET_SOUND_ID()
									ELSE
										IF HAS_SOUND_FINISHED( targetList[i].sound )
											PLAY_SOUND_FROM_ENTITY(targetList[i].sound,"move",targetList[i].Entity,"DLC_PILOT_Shooting_Range_Sounds")
										ENDIF
									ENDIF
								ENDIF
							BREAK
						ENDSWITCH
					BREAK
				ENDSWITCH
				
				IF eGameMode = PS_GAME_MODE_CHALLENGE
					IF NOT DOES_BLIP_EXIST(targetList[i].blip)				
						IF DOES_ENTITY_EXIST(targetList[i].entity)
							targetList[i].blip = CREATE_BLIP_FOR_ENTITY(targetList[i].entity,true)
							BEGIN_TEXT_COMMAND_SET_BLIP_NAME("PS_BLIP_TARGET")
							END_TEXT_COMMAND_SET_BLIP_NAME(targetList[i].blip)
							SET_BLIP_AS_FRIENDLY(targetList[i].blip, FALSE)
						ENDIF
					ENDIF
				ENDIF
				
			BREAK
			
			CASE IN_POSITION				
				DRAW_LIGHT_WITH_RANGE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(targetList[i].Entity,<<0,1,2>>),255,0,0,10.0,1.0)
				DRAW_LIGHT_WITH_RANGE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(targetList[i].Entity,<<0,-0.5,2>>),255,255,255,3.0,3.0)
				IF NOT IS_ENTITY_DEAD(targetList[i].Entity)
				AND NOT IS_ENTITY_DEAD(targetList[i].entityBase)					
					ATTACH_ENTITY_TO_ENTITY(targetList[i].Entity,targetList[i].entityBase,0,<<0,0,0.4>>,<<0,0,targetList[i].fShtHeading>>-<<0,0,GET_ENTITY_HEADING(targetList[i].entityBase)>>,TRUE,false,true)
				ENDIF

			BREAK
			
			CASE EXPLODING
			FALLTHRU
			CASE EXPLODE_AND_RESET
				IF DOES_BLIP_EXIST(targetList[i].blip)
					cprintln(debug_trevor3,"Remove target blip")
					REMOVE_BLIP(targetList[i].blip)
					vector soundCoord
					
					//Cleanup any existing sounds
					IF targetList[i].sound != -1
						IF NOT HAS_SOUND_FINISHED(targetList[i].sound)
							STOP_SOUND( targetList[i].sound )
							RELEASE_SOUND_ID( targetList[i].sound )
							targetList[i].sound = -1
						ENDIF
					ENDIF
					
					soundCoord = GET_ENTITY_COORDS(targetList[i].Entity)
					IF HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(targetList[i].Entity, WEAPONTYPE_VEHICLE_PLAYER_BUZZARD)
						PLAY_SOUND_FROM_COORD(-1,"Destroyed",soundCoord,"DLC_PILOT_Shooting_Range_Sounds")
					ENDIF
				ENDIF
				
				
				
				targetList[i].triggerTime += timestep() * 0.3
				IF targetList[i].triggerTime >= 1.0
					targetList[i].triggerTime = 1.0
					IF targetList[i].status = EXPLODING
						targetList[i].status = DESTROYED
					ELSE
						targetList[i].status = RESET
					ENDIF
					cprintln(debug_trevor3,"Delete object ",i," at ",GET_GAME_TIMER())
					
					IF NETWORK_DOES_NETWORK_ID_EXIST(targetList[i].entity_NETID)
						PS_DELETE_NET_ID(targetList[i].entity_NETID)		
					ENDIF
					
					IF NETWORK_DOES_NETWORK_ID_EXIST(targetList[i].entityBase_NETID)
						PS_DELETE_NET_ID(targetList[i].entityBase_NETID)		
					ENDIF
					
				
				ELSE
					IF DOES_ENTITY_EXIST(targetList[i].Entity)
						IF NOT IS_ENTITY_DEAD(targetList[i].Entity)
							SET_ENTITY_ALPHA(targetList[i].Entity,255-(FLOOR(targetList[i].triggerTime*255)),true)
						ENDIF
					ENDIF
					IF DOES_ENTITY_EXIST(targetList[i].entityBase)
						IF NOT IS_ENTITY_DEAD(targetList[i].entityBase)
							SET_ENTITY_ALPHA(targetList[i].entityBase,255-(FLOOR(targetList[i].triggerTime*255)),true)
						ENDIF
					ENDIF
				ENDIF					
			BREAK
			CASE RESET
				
				targetList[i].slideFlag = 0
				IF NETWORK_DOES_NETWORK_ID_EXIST(targetList[i].entity_NETID)
					PS_DELETE_NET_ID(targetList[i].entity_NETID)		
				ENDIF
				
				IF NETWORK_DOES_NETWORK_ID_EXIST(targetList[i].entityBase_NETID)
					PS_DELETE_NET_ID(targetList[i].entityBase_NETID)		
				ENDIF
				
				targetList[i].triggerTime = 0.0
				targetList[i].status = DOESNT_EXIST
			BREAK
		ENDSWITCH
	ENDREPEAt

		
		

ENDPROC


PROC PS_DLC_Shooting_Range_Init_Checkpoints()  
	
	PS_RESET_CHECKPOINT_MGR()
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<115.5911, -467.6580, 69.4141>>)//0

ENDPROC

/// PURPOSE:
///    Determines what fail conditions we need to check dependent on the 
FUNC BOOL PS_DLC_Shooting_Range_Fail_Check()
	IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),<<71.0589, -414.5967, 73.6531>>) > 300
		ePSFailReason = PS_FAIL_OUT_OF_RANGE
		RETURN TRUE
	ENDIF
	
	IF PS_GET_SUBSTATE() = PS_SUBSTATE_UPDATE
		SWITCH(PS_DLC_Shooting_Range_State)
			CASE PS_DLC_SHOOTING_RANGE_INIT
				//No fail check to do
				RETURN FALSE
				
			CASE PS_DLC_SHOOTING_RANGE_COUNTDOWN
				//No fail check to do
				RETURN FALSE
				
			DEFAULT
				//Do runway check and default checks
				IF PS_DLC_Shooting_Range_State < PS_DLC_SHOOTING_RANGE_GUN_TARGETS_ROUND_3
					IF PS_UPDATE_FAIL_CHECKS(PS_Main, FALSE, TRUE, TRUE)
						RETURN TRUE
					ENDIF
				ELSE
					IF PS_UPDATE_FAIL_CHECKS(PS_Main, FALSE, FALSE, TRUE)
						RETURN TRUE
					ENDIF
				ENDIF
			BREAK
			
			
				//Do default checks
			//	IF PS_UPDATE_FAIL_CHECKS(PS_Main, FALSE, FALSE, FALSE)
			//		RETURN TRUE
			//	ENDIF
				BREAK
		ENDSWITCH
	ENDIF
	//If we get here, we havent failed yet
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks for objective goals completed based on the stage/state we are in of the challenge progression
///    Also updates ChallengeState when we complete an objective 
FUNC BOOL PS_DLC_Shooting_Range_Progress_Check()
	IF NOT IS_PED_INJURED(PLAYER_PED_ID()) AND IS_VEHICLE_DRIVEABLE(PS_Main.myVehicle)
		SWITCH(PS_DLC_Shooting_Range_State)
			CASE PS_DLC_SHOOTING_RANGE_INIT
				RETURN FALSE
				BREAK

			CASE PS_DLC_SHOOTING_RANGE_COUNTDOWN
				RETURN FALSE
				BREAK
				
			CASE PS_DLC_SHOOTING_RANGE_TAKEOFF
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_UPDATE
						//See if the player is close to the checkpoint yet
						IF PS_IS_LAST_CHECKPOINT_CLEARED()
							RETURN TRUE
						ENDIF
						BREAK
				ENDSWITCH
				BREAK
				
			CASE PS_DLC_SHOOTING_RANGE_OBSTACLE
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_UPDATE
						//See if the player is close to the checkpoint yet
						IF PS_IS_LAST_CHECKPOINT_CLEARED()
							RETURN TRUE
						ENDIF
						BREAK
				ENDSWITCH
				BREAK
		ENDSWITCH
	ENDIF
	//if we reach here, we probably haven't passed the check
	RETURN FALSE	
ENDFUNC

PROC PS_DLC_SHOOTING_RANGE_FORCE_FAIL
	
	ePSFailReason = PS_FAIL_DEBUG
	PS_DLC_Shooting_Range_State = PS_DLC_SHOOTING_RANGE_FAIL
	
	PS_HUD_SET_GUTTER_ICON_ACTIVE(FALSE)
	PS_HUD_SET_OBJECTIVE_METER_VISIBLE(FALSE)
	PS_SET_TIMER_ACTIVE(FALSE)
	PS_HUD_CLEANUP()
	CLEAR_PRINTS()
	CLEAR_HELP()
	
	

//take care of objects
	//player first
	PS_SEND_PLAYER_BACK_TO_MENU()
	//then vehicle
	
	//then checkpoints
	PS_RESET_CHECKPOINT_MGR()



	REMOVE_VEHICLE_RECORDING(iPS_PreviewVehicleRecordingID, "PilotSchool")
	SET_MODEL_AS_NO_LONGER_NEEDED(VehicleToUse)		
	//SET_MODEL_AS_NO_LONGER_NEEDED(targetProp)

	//SET_MODEL_AS_NO_LONGER_NEEDED(targetPropMoverB)
	
	#if IS_DEBUG_BUILD
	IF DOES_WIDGET_GROUP_EXIST(shtWidget)
		DELETE_WIDGET_GROUP(shtWidget)
	ENDIF
	#endif
	
	PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
ENDPROC

PROC PS_DLC_SHOOTING_RANGE_FORCE_PASS
	PS_DLC_Shooting_Range_State = PS_DLC_SHOOTING_RANGE_SUCCESS
	PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
ENDPROC

//****************************************************************************************************
//****************************************************************************************************
//	Public functions! (used by Pilot_School.sc to run the challenges)
//****************************************************************************************************
//****************************************************************************************************

PROC PS_DLC_Shooting_Range_Initialise()
	int i
	
	SWITCH g_PS_initStage
		CASE 0
			//Start pos and heading are hard-coded since it different for each challenge.
			vStartPosition = <<212.0594, -499.8476, 87.2376>>
			vStartRotation = <<0.3717, 0.0000, 46.6493>> 
			fStartHeading = 46.0000
			iLastSoundTime = 10000
			
			VehicleToUse = BUZZARD
			targetPropBot =   INT_TO_ENUM(MODEL_NAMES,HASH("pil_prop_fs_target_01")) //bottom pivot
			targetPropSide = INT_TO_ENUM(MODEL_NAMES,HASH("pil_prop_fs_target_02")) //right pivot
			targetPropArm = INT_TO_ENUM(MODEL_NAMES,HASH("pil_prop_fs_target_03")) //with stand
			targetPropMover = INT_TO_ENUM(MODEL_NAMES,HASH("pil_prop_fs_target_base")) //with stand
			
			iPS_PreviewVehicleRecordingID = iPS_PEVIEW_VEH_REC_ID_SHOOTING_RANGE
			REQUEST_VEHICLE_RECORDING(iPS_PreviewVehicleRecordingID, "PilotSchool")
			request_model(targetPropBot)
			request_model(targetPropSide)
			request_model(targetPropArm)
			request_model(targetPropMover)
			g_PS_initStage++
		BREAK
		CASE 1
			IF HAS_VEHICLE_RECORDING_BEEN_LOADED(iPS_PreviewVehicleRecordingID, "PilotSchool")
			AND REQUEST_SCRIPT_AUDIO_BANK("TARGET_PRACTICE")
			AND HAS_MODEL_LOADED(targetPropBot)
			AND HAS_MODEL_LOADED(targetPropSide)
			AND HAS_MODEL_LOADED(targetPropArm)
			AND HAS_MODEL_LOADED(targetPropMover)
			AND PS_SETUP_CHALLENGE()
				REPEAT COUNT_OF(targetList) i
					targetList[i].slideFlag = 0
					IF NETWORK_DOES_NETWORK_ID_EXIST(targetList[i].entity_NETID)
						PS_DELETE_NET_ID(targetList[i].entity_NETID)		
					ENDIF
					
					IF NETWORK_DOES_NETWORK_ID_EXIST(targetList[i].entityBase_NETID)
						PS_DELETE_NET_ID(targetList[i].entityBase_NETID)		
					ENDIF
					
					targetList[i].triggerTime = 0.0
					targetList[i].status = DOESNT_EXIST
				endrepeat
				
				//set up target data
				targetList[0].status = WAITING
				targetList[0].vCoord = <<34.8018, -423.2538, 54.5387>>
				targetList[0].vSlideIn = <<-2.2250, -4.9750, 0.0000>>
				targetList[0].fShtHeading = 70.2500
				targetList[0].iMovementType = 0
				targetList[0].loop = false
				targetList[0].wave = 0
				targetList[1].status = WAITING
				targetList[1].vCoord = <<28.8060, -439.9322, 54.5605>>
				targetList[1].vSlideIn = <<3.7750, 10.0000, 0.0000>>
				targetList[1].fShtHeading = 66.8750
				targetList[1].iMovementType = 0
				targetList[1].loop = false
				targetList[1].wave = 1
				targetList[2].status = WAITING
				targetList[2].vCoord = <<32.6731, -450.3864, 55.3403>>
				targetList[2].vSlideIn = <<0.0000, 0.0000, 0.0000>>
				targetList[2].fShtHeading = 67.3750
				targetList[2].iMovementType = 1
				targetList[2].loop = false
				targetList[2].wave = 2
				targetList[3].status = WAITING
				targetList[3].vCoord = <<39.7702, -437.7629, 64.0358>>
				targetList[3].vSlideIn = <<-1.6750, -4.5150, 0.0000>>
				targetList[3].fShtHeading = 70.5000
				targetList[3].iMovementType = 0
				targetList[3].loop = false
				targetList[3].wave = 3
				targetList[4].status = WAITING
				targetList[4].vCoord = <<34.7633, -450.5908, 64.0358>>
				targetList[4].vSlideIn = <<1.5250, 3.9500, 0.0000>>
				targetList[4].fShtHeading = 69.2500
				targetList[4].iMovementType = 0
				targetList[4].loop = false
				targetList[4].wave = 3
				
				activeWave[0] = -1
				activeWave[1] = -1
				activeWave[2] = -1
				
				chunck[0].ux = -2.0
				chunck[0].vx = 0.5
				chunck[0].v1 = <<0,0,0>>
				chunck[0].v2 = <<0,0,1>>
				chunck[0].v3 = <<0.2,0,0.3>>
				
				chunck[1].ux = -0.3
				chunck[1].vx = 1.5
				chunck[1].v1 = <<0,0,1>>
				chunck[1].v2 = <<1,0,1>>
				chunck[1].v3 = <<0.6,0,0.8>>
				
				chunck[2].ux = -1.0
				chunck[2].vx = 1.0
				chunck[2].v1 = <<0,0,1>>
				chunck[2].v2 = <<0.6,0,0.8>>
				chunck[2].v3 = <<0.2,0,0.3>>
				
				chunck[3].ux = 2.0
				chunck[3].vx = 1.5
				chunck[3].v1 = <<1,0,1>>
				chunck[3].v2 = <<1,0,0.5>>
				chunck[3].v3 = <<0.6,0,0.8>>
				
				chunck[4].ux = 1.6
				chunck[4].vx = 0.7
				chunck[4].v1 = <<0.6,0,0.8>>
				chunck[4].v2 = <<1,0,0.5>>
				chunck[4].v3 = <<0.2,0,0.3>>
				
				chunck[5].ux = 1.3
				chunck[5].vx = 0.5
				chunck[5].v1 = <<1,0,0.5>>
				chunck[5].v2 = <<1,0,0>>
				chunck[5].v3 = <<0.2,0,0.3>>
				
				chunck[6].ux = 0.1
				chunck[6].vx = 0.3
				chunck[6].v1 = <<0,0,0.0>>
				chunck[6].v2 = <<0.2,0,0.3>>
				chunck[6].v3 = <<1,0,0>>
				

				
				REPEAT COUNT_OF(chunck) i
					chunck[i].v1.x = (chunck[i].v1.x - 0.5) * 2.0
					chunck[i].v2.x = (chunck[i].v2.x - 0.5) * 2.0
					chunck[i].v3.x = (chunck[i].v3.x - 0.5) * 2.0		
					chunck[i].ux *= 2.5
					chunck[i].vx *= 2.5
				ENDREPEAT
				
				#if IS_DEBUG_BUILD
				fShtHeading = targetList[iCurrentTarget].fShtHeading
				vSlideFrom = targetList[iCurrentTarget].vSlideIn
				iMovementType = targetList[iCurrentTarget].iMovementType
				bIsLooped = targetList[iCurrentTarget].loop
				vTargetNudge = <<0,0,0>>
				iMovementType = 0
				#endif
				
				iThisWave = 0
				bRoundActive = FALSE
				attackStage = 0
				iTargetCounter = 0
				iCurrentTarget = 0
				
				bFinishedChallenge = FALSE
				

				PS_DLC_Shooting_Range_Init_Checkpoints()


				

				
				
				targetPropBot =targetPropBot
				targetPropSide =targetPropSide
				targetPropArm =targetPropArm
				targetPropMover =targetPropMover
				
				
				
				fPreviewVehicleSkipTime = 0
				fPreviewTime = 16
				
				vPilotSchoolSceneCoords = vStartPosition
				IF HAS_VEHICLE_RECORDING_BEEN_LOADED(iPS_PreviewVehicleRecordingID, sPreviewRecordingName)
					vPilotSchoolPreviewCoords = GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(iPS_PreviewVehicleRecordingID, fPreviewVehicleSkipTime, sPreviewRecordingName)
					vPilotSchoolPreviewRot = GET_ROTATION_OF_VEHICLE_RECORDING_AT_TIME(iPS_PreviewVehicleRecordingID, fPreviewVehicleSkipTime, sPreviewRecordingName)
				ENDIF
				
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					SET_CURRENT_PED_VEHICLE_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_VEHICLE_PLAYER_BULLET)
				ENDIf
				//offset for checking whether player is in the air or on the ground
				PS_SET_GROUND_OFFSET(1.5)		
				g_PS_initStage++
			ENDIF
		BREAK
		CASE 2
			IF PS_CREATE_VEHICLE()
				bwaitingOnSomething = FALSE
				SET_VEHICLE_ENGINE_ON(PS_Main.myVehicle,TRUE,TRUE)
				SET_VEHICLE_MISSILE_WARNING_ENABLED(ps_main.myVehicle,FALSE)
				SET_HELI_BLADES_FULL_SPEED(PS_Main.myVehicle)
//				SET_WEATHER_FOR_FMMC_MISSION(1) //sunny

				iPopSpehere = ADD_POP_MULTIPLIER_SPHERE(<<0,0,0>>,8000,0.0,0.0,FALSE)

				NETWORK_OVERRIDE_CLOCK_TIME(GET_HOUR_FOR_PS_CHALLENGE(),0,0)
				
				#IF IS_DEBUG_BUILD
					ADD_DEBUG_WIDGET()
					DEBUG_MESSAGE("******************Setting up PS_DLC_SHOOTING_RANGE.sc******************")
				#ENDIF
				g_PS_initStage=0
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL PS_DLC_Shooting_Range_MainSetup()
	PS_DLC_Shooting_Range_Init_Checkpoints()
	SET_MINIMAP_COMPONENT(MINIMAP_COMPONENT_RUNWAY_1, FALSE)
	NETWORK_OVERRIDE_CLOCK_TIME(GET_HOUR_FOR_PS_CHALLENGE(),0,0)
	// Preventing planes from being generated in the two boarding locations, where planes normally pop into view as the challenge starts.
	//VECTOR vGenBlockMin = <<115.5911, -467.6580, 69.4141>> - << 200.0, 200.0, 20.0 >>
	//VECTOR VGenBlockMax = <<115.5911, -467.6580, 69.4141>> + << 200.0, 200.0, 20.0 >>
	//SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(vGenBlockMin, VGenBlockMax, FALSE)
	BLOCK_SCENARIOS_AND_AMBIENT(sb_PS, DEFAULT, TRUE)
	NEW_LOAD_SCENE_START(<<212.0594, -499.8476, 87.2376>>, NORMALISE_VECTOR(<<115.5911, -467.6580, 69.4141>> - <<212.0594, -499.8476, 87.2376>>), 5000.0)
	
	CLEAR_PED_TASKS_IMMEDIATELY(player_ped_id())
	IF IS_VEHICLE_DRIVEABLE(PS_Main.myVehicle)
		TASK_ENTER_VEHICLE(player_ped_id(),PS_Main.myVehicle,1,VS_DRIVER,PEDMOVEBLENDRATIO_RUN,ECF_WARP_PED)
	ENDIF
//	IF IS_VEHICLE_DRIVEABLE(PS_Main.myVehicle)
//		SET_PED_INTO_VEHICLE(player_ped_id(),PS_Main.myVehicle)
//	ENDIF
	CLEAR_AREA_OF_PROJECTILES(<<90.3830, -370.8150, 57.7257>>,100.0)
	RESET_ALL_TARGETS(TRUE)
	
	IF PS_Main.myChallengeData.HasBeenPreviewed
	OR saveData[g_current_selected_dlc_PilotSchool_class].LastLandingDistance > 0
	OR saveData[g_current_selected_dlc_PilotSchool_class].LastLandingDistance = -2 //special case hack just to skip to the end.
		IF IS_VEHICLE_DRIVEABLE(PS_Main.myVehicle)
			SET_ENTITY_COORDS(PS_Main.myVehicle, <<75.0692, -406.2138, 64.5533>>)
			SET_ENTITY_HEADING(PS_Main.myVehicle, 342.1104)
		ENDIF
		PREP_ATTACK_STAGE(2)
	ELSE		
		PREP_ATTACK_STAGE(0)
	ENDIF
	RETURN TRUE
ENDFUNC


int iShootTimer
int iShootProgress
FUNC BOOL PS_DLC_Shooting_Range_Preview()
	IF PS_PREVIEW_UPDATE_CUTSCENE()
		SWITCH(PS_PreviewState)
			CASE PS_PREVIEW_SETUP
				PS_PREVIEW_SETUP_PLAYER_FOR_CUTSCENE()
				IF NOT IS_VEHICLE_FUCKED(PS_Main.myVehicle)
					START_PLAYBACK_RECORDED_VEHICLE(PS_Main.myVehicle, iPS_PreviewVehicleRecordingID, "PilotSchool")
					SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(PS_Main.myVehicle, fPreviewVehicleSkipTime)
					SET_ENTITY_HEALTH(ps_main.myVehicle,1000)
					SET_VEHICLE_ENGINE_HEALTH(ps_main.myVehicle,1000)
				ENDIF
				NETWORK_OVERRIDE_CLOCK_TIME(GET_HOUR_FOR_PS_CHALLENGE(),0,0)
				iPreviewCheckpointIdx = PS_GET_CLOSEST_PREVIEW_CHECKPOINT(iPS_PreviewVehicleRecordingID, PS_Main.myCheckpointz, fPreviewVehicleSkipTime)
				PS_ACTIVATE_CHECKPOINT_MGR(PS_LESSON_OBSTACLE_PLANE, iPreviewCheckpointIdx + 1, TRUE)
				PS_PREVIEW_DIALOGUE_PLAY("PS_SRPREV", "PS_SRPREV_1", "PS_SRPREV_2", "PS_SRPREV_3")
				iShootTimer = 0
				iShootProgress = 0
				
				targetList[6].status = WAITING
				targetList[6].vCoord = <<95.7450, -371.3139, 57.0514>>
				targetList[6].vSlideIn = <<0.0000, 0.0000, -2.7250>>
				targetList[6].fShtHeading = -19.75
				targetList[6].iMovementType = 5
				targetList[6].wave = 5
				targetList[6].loop = TRUE
				targetList[6].pauseTime = 2000
				targetList[7].status = WAITING
				targetList[7].vCoord = <<92.6534, -370.7400, 57.1096>>
				targetList[7].vSlideIn = <<0.0000, 0.0000, -2.8250>>
				targetList[7].fShtHeading = -17.6250
				targetList[7].iMovementType = 5
				targetList[7].wave = 5
				targetList[7].loop = TRUE
				targetList[0].status = WAITING
				targetList[0].vCoord = <<110.5213, -371.7523, 54.7514>>
				targetList[0].vSlideIn = <<-14.1000, 1.7250, 0.0000>>
				targetList[0].fShtHeading = -21.3750
				targetList[0].iMovementType = 0
				targetList[0].wave = 0
				targetList[0].loop = TRUE
				targetList[0].pauseTime = 2000
				targetList[16].status = WAITING
				targetList[16].vCoord = <<86.4900, -373.7033, 65.7471>>
				targetList[16].vSlideIn = <<0.0000, 0.0000, -19.2500>>
				targetList[16].fShtHeading = -16.5000
				targetList[16].iMovementType = 8
				targetList[16].wave = 12
				targetList[16].loop = true
				targetList[16].pauseTime = 2000
				
				TRIGGER_WAVE(5)
				TRIGGER_WAVE(0)
				TRIGGER_WAVE(12)
				//"Now we're going to combine some of the stunts we learned with an obstacle course."
				//"For your sake, I hope you've been practicing knifing and flying upside-down. "
				//"Now, let's see you pull off extremely dangerous stunts around highly populated city areas!"
			BREAK
				
			CASE PS_PREVIEW_PLAYING
			
				CONTROL_TARGETS()
			
				//let vehicle recording play
				//shoot guns and shit
				IF GET_GAME_TIMER() > iShootTimer
					SWITCH iShootProgress
						CASE 0
						FALLTHRU
						CASE 1					
							TASK_HELI_MISSION(player_ped_id(),PS_Main.myVehicle,null,null,GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PS_Main.myVehicle,<<0,100,0>>),MISSION_ATTACK,1,1,1,10,10)
							//ADD_VEHICLE_SUBTASK_ATTACK_COORD(player_ped_id(),GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PS_Main.myVehicle,<<0,100,0>>))
							//TASK_SHOOT_AT_COORD(player_ped_id(),GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PS_Main.myVehicle,<<0,100,0>>),1000,FIRING_TYPE_CONTINUOUS)
							iShootProgress++
							iShootTimer=GET_GAME_TIMER() + 2000
						BREAK
						DEFAULT
							TASK_HELI_MISSION(player_ped_id(),PS_Main.myVehicle,null,null,GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PS_Main.myVehicle,<<0,100,0>>),MISSION_ATTACK,1,1,1,10,10)
							ADD_VEHICLE_SUBTASK_ATTACK_COORD(player_ped_id(),GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PS_Main.myVehicle,<<0,100,0>>))
							iShootProgress=0
							iShootTimer=GET_GAME_TIMER() + 2000
						BREAK						
					ENDSWITCH
				ENDIF
			BREAK
				
			CASE PS_PREVIEW_CLEANUP
				clear_ped_tasks(player_ped_id())
				PS_PREVIEW_CLEANUP_CUTSCENE()
			BREAK
		ENDSWITCH
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
	
ENDFUNC





#if IS_DEBUG_BUILD
proc editTargetCoords()
	//int iR
		/*TEXT_LABEL_23 txt
		REPEAT COUNT_OF(targetList) iR
			txt = ""
			txt+= enum_to_int(targetList[iR].status)
			DRAW_DEBUG_TEXT_2D(txt,<<0.1,0.1+(iR*0.03),0>>)
		ENDREPEAT
	*/
		
		IF bAddTarget
			targetList[iCurrentTarget].vCoord = GET_SCRIPT_MOUSE_POINTER_IN_WORLD_COORDS()
			IF NOT bDrawDebugTargets
		//		IF NOT DOES_ENTITY_EXIST(targetList[iCurrentTarget].Entity)
		//			targetList[iCurrentTarget].Entity = CREATE_OBJECT(targetPropMoverA,targetList[iCurrentTarget].vCoord)
		//		ENDIF
			ENDIF
			targetList[iCurrentTarget].status = WAITING
			
			bAddTarget=FALSE
		ENDIF
		
		If bTestTrigger
			bTestTrigger = false
			//delete all old instance
			
			IF DOES_ENTITY_EXIST(targetList[iTargetCounter].entity) 
				IF IS_ENTITY_ATTACHED(targetList[iTargetCounter].entity)
					DETACH_ENTITY(targetList[iTargetCounter].entity)
				ENDIF
				IF NETWORK_DOES_NETWORK_ID_EXIST(targetList[iTargetCounter].entity_NETID)
					PS_DELETE_NET_ID(targetList[iTargetCounter].entity_NETID)		
				ENDIF
				cprintln(debug_trevor3,"Delete object (c) ",iTargetCounter," at ",GET_GAME_TIMER())
			ENDIF								
				
			IF NETWORK_DOES_NETWORK_ID_EXIST(targetList[iTargetCounter].entityBase_NETID)
				PS_DELETE_NET_ID(targetList[iTargetCounter].entityBase_NETID)		
			ENDIF
			
			targetList[iTargetCounter].vCoord += vTargetNudge
			vTargetNudge = <<0,0,0>>
			targetList[iTargetCounter].fShtHeading = fShtHeading
			targetList[iTargetCounter].iMovementType = iMovementType
			cprintln(debug_trevor3,"iMovementType = ",iMovementType," .iMovementType= ",targetList[iTargetCounter].iMovementType)
			targetList[iTargetCounter].vSlideIn = vSlideFrom
			targetList[iTargetCounter].triggerTime = 0.0
			targetList[iTargetCounter].slideFlag = 0
			targetList[iTargetCounter].loop = bIsLooped									
			targetList[iTargetCounter].status = MOVING
		ENDIF
		
		IF iCurrentTarget!= iTargetCounter
			IF NOT IS_VECTOR_ZERO(vTargetNudge)
				targetList[iCurrentTarget].vCoord += vTargetNudge
			ENDIF
			
			IF fShtHeading != targetList[iCurrentTarget].fShtHeading
				targetList[iCurrentTarget].fShtHeading = fShtHeading
			ENDIF
			
			targetList[iCurrentTarget].loop = bIsLooped
			targetList[iCurrentTarget].iMovementType = iMovementType
			targetList[iCurrentTarget].vSlideIn = vSlideFrom 
			
			iCurrentTarget = iTargetCounter
			fShtHeading = targetList[iCurrentTarget].fShtHeading
			vSlideFrom = targetList[iCurrentTarget].vSlideIn		
			iMovementType = targetList[iCurrentTarget].iMovementType
			bIsLooped = targetList[iCurrentTarget].loop
			vTargetNudge = <<0,0,0>>
		ENDIF
	
		IF NOT IS_VECTOR_ZERO(vTargetNudge)
			IF targetList[iCurrentTarget].status != DOESNT_EXIST
				IF DOES_ENTITY_EXIST(targetList[iCurrentTarget].Entity)	
					IF NOT IS_ENTITY_ATTACHED(targetList[iCurrentTarget].Entity)
						SET_ENTITY_COORDS_NO_OFFSET(targetList[iCurrentTarget].Entity,targetList[iCurrentTarget].vCoord + vTargetNudge)
					ENDIF
				ENDIF
				
				IF DOES_ENTITY_EXIST(targetList[iCurrentTarget].entityBase)	
					IF NOT IS_ENTITY_ATTACHED(targetList[iCurrentTarget].entityBase)
						SET_ENTITY_COORDS_NO_OFFSET(targetList[iCurrentTarget].entityBase,targetList[iCurrentTarget].vCoord + vTargetNudge)
					ENDIF
				ENDIF
			ENDIF			
		ENDIF
		
		IF fShtHeading != targetList[iCurrentTarget].fShtHeading
			targetList[iCurrentTarget].fShtHeading = fShtHeading
			IF DOES_ENTITY_EXIST(targetList[iCurrentTarget].Entity)
				SET_ENTITY_HEADING(targetList[iCurrentTarget].Entity,fShtHeading)
			ENDIF
		ENDIF
		
		IF NOT ARE_VECTORS_EQUAL(targetList[iCurrentTarget].vSlideIn,vSlideFrom)
			targetList[iCurrentTarget].vSlideIn = vSlideFrom
		ENDIF
		
		/*
		IF fTargetScale != fTargetScale
			fTargetScale = fTargetScale
			int i
			REPEAT count_of(targetList) i
				IF DOES_ENTITY_EXIST(targetList[i].entity)
					
				ENDIF
			ENDREPEAT
		ENDIF
		*/
		
		//draw targets
		IF bDrawDebugTargets

			vector vNudge,vTrigger
			float fShtHeadingToDraw
			vector v1,v2,v3,v4
			vector vRot[4]
		
			int i
			REPEAT count_of(targetList) i				
				IF (targetList[i].STATUS >= MOVING
				AND targetList[i].status < EXPLODING)
				OR bEditMode
					IF NETWORK_DOES_NETWORK_ID_EXIST(targetList[i].entity_NETID)
						PS_DELETE_NET_ID(targetList[i].entity_NETID)		
					ENDIF
						
					IF i = iCurrentTarget
						vNudge = vTargetNudge						
					else						
						vNudge = <<0,0,0>>
					ENDIF
					
					vTrigger = <<0,0,0>>
					
			
					fShtHeadingToDraw = targetList[i].fShtHeading
					
					vRot[0] = <<(cos(fShtHeadingToDraw)*fTargetScale),(sin(fShtHeadingToDraw)*fTargetScale),0>>
					vRot[1] = <<(cos(fShtHeadingToDraw)*fTargetScale),(sin(fShtHeadingToDraw)*fTargetScale),3.0*fTargetScale>>
					vRot[2] = <<(cos(fShtHeadingToDraw+180)*fTargetScale),(sin(fShtHeadingToDraw+180)*fTargetScale),3.0*fTargetScale>>
					vRot[3] = <<(cos(fShtHeadingToDraw+180)*fTargetScale),(sin(fShtHeadingToDraw+180)*fTargetScale),0>>
					
					
					
				
						IF targetList[i].status = MOVING
							
							SWITCH targetList[i].slideFlag
							
							
							
								CASE 1
								FALLTHRU
								CASE 4
									switch targetList[i].iMovementType
										CASE 1 //rotate bottom edge
											vRot[1].x += (sin(fShtHeadingToDraw+180)*fTargetScale*3.0) * (cos(targetList[i].triggerTime*-90.0))
											vRot[1].y += (cos(fShtHeadingToDraw)*fTargetScale*3.0) * (cos(targetList[i].triggerTime*-90.0))
											vRot[2].x += (sin(fShtHeadingToDraw+180)*fTargetScale*3.0) * (cos(targetList[i].triggerTime*-90.0))
											vRot[2].y += (cos(fShtHeadingToDraw)*fTargetScale*3.0) * (cos(targetList[i].triggerTime*-90.0))
											
											vRot[1].z = (3.0*fTargetScale) * (sin(targetList[i].triggerTime*90.0))
											vRot[2].z = vRot[1].z
										BREAK
										CASE 2 //rotate from right edge
											vRot[2].x = vRot[1].x + (fTargetScale * 2 * cos(fShtHeadingToDraw + (targetList[i].triggerTime*180.0)))
											vRot[2].y = vRot[1].y + (fTargetScale * 2 * sin(fShtHeadingToDraw + (targetList[i].triggerTime*180.0)))
											vRot[3].x = vRot[2].x
											vRot[3].y = vRot[2].y
										BREAK
										CASE 3 //rotate on top edge
											vRot[0].x += (sin(fShtHeadingToDraw+180)*fTargetScale*3.0) * (cos(targetList[i].triggerTime*-90.0))
											vRot[0].y += (cos(fShtHeadingToDraw)*fTargetScale*3.0) * (cos(targetList[i].triggerTime*-90.0))
											vRot[3].x += (sin(fShtHeadingToDraw+180)*fTargetScale*3.0) * (cos(targetList[i].triggerTime*-90.0))
											vRot[3].y += (cos(fShtHeadingToDraw)*fTargetScale*3.0) * (cos(targetList[i].triggerTime*-90.0))
											
											vRot[0].z = (3.0*fTargetScale) - ((3.0*fTargetScale) * (sin(targetList[i].triggerTime*90.0)))
											vRot[3].z = vRot[0].z
										BREAK
									ENDSWITCH
									vTrigger = targetList[i].vSlideIn
								BREAK
								CASE 2
								FALLTHRU
								CASE 5
									IF NOT IS_VECTOR_ZERO(targetList[i].vSlideIn)
										vTrigger = targetList[i].vSlideIn * (1.0 - targetList[i].triggerTime)
									ENDIF
								BREAK
								CASE 6
									IF targetList[i].iMovementType = 2
										vRot[2].x = vRot[1].x + (fTargetScale * 2 * cos(fShtHeadingToDraw + 0.0))
										vRot[2].y = vRot[1].y + (fTargetScale * 2 * sin(fShtHeadingToDraw + 0.0))
										vRot[3].x = vRot[2].x
										vRot[3].y = vRot[2].y
									ENDIF
									vTrigger = targetList[i].vSlideIn
								BREAK
							ENDSWITCH
						ENDIF
				
					
					int colR = 0
					int colG = 0
					int colB = 255
					
					IF i = iCurrentTarget
					AND bEditMode
						colR = 0
						colG = 255
						colB = 0
					ENDIF
					
					IF targetList[i].status >= EXPLODING
						colR = 255
						colG = 0
						colB = 0
					ENDIF
				
					targetList[i].vCurrent = targetList[i].vCoord + vNudge + vTrigger
					
					
					
					v1 = targetList[i].vCurrent + vRot[0]//(<<(cos(fShtHeadingToDraw)*fTargetScale)+vRot[0].x,(sin(fShtHeadingToDraw)*fTargetScale)+vRot[0].y,vRot[0].z>>)
					v2 = targetList[i].vCurrent + vRot[1]//(<<(cos(fShtHeadingToDraw)*fTargetScale)+vRot[1].x,(sin(fShtHeadingToDraw)*fTargetScale)+vRot[1].y,vRot[1].z>>)
					v3 = targetList[i].vCurrent + vRot[2]//(<<(cos(fShtHeadingToDraw+180)*fTargetScale)+vRot[2].x,(sin(fShtHeadingToDraw+180)*fTargetScale)+vRot[2].y,vRot[2].z>>)
					v4 = targetList[i].vCurrent + vRot[3]//(<<(cos(fShtHeadingToDraw+180)*fTargetScale)+vRot[3].x,(sin(fShtHeadingToDraw+180)*fTargetScale)+vRot[3].y,vRot[3].z>>)
					DRAW_DEBUG_POLY(v1,v2,v3,colR,colG,colB)
					DRAW_DEBUG_POLY(v1,v3,v2,colR,colG,colB)
					DRAW_DEBUG_POLY(v1,v3,v4,colR,colG,colB)
					DRAW_DEBUG_POLY(v1,v4,v3,colR,colG,colB)
					
					//hit detection
					
					
					
					IF bDrawOrigin
						IF NOT IS_VECTOR_ZERO(targetList[i].vSlideIn)
							vTrigger = targetList[i].vSlideIn
							
							v1 = targetList[i].vCoord + vNudge + vTrigger + (<<cos(fShtHeadingToDraw)*fTargetScale,sin(fShtHeadingToDraw)*fTargetScale,0>>)
							v2 = targetList[i].vCoord + vNudge + vTrigger + (<<cos(fShtHeadingToDraw)*fTargetScale,sin(fShtHeadingToDraw)*fTargetScale,3.0*fTargetScale>>)
							v3 = targetList[i].vCoord + vNudge + vTrigger + (<<cos(fShtHeadingToDraw+180)*fTargetScale,sin(fShtHeadingToDraw+180)*fTargetScale,3.0*fTargetScale>>)
							v4 = targetList[i].vCoord + vNudge + vTrigger + (<<cos(fShtHeadingToDraw+180)*fTargetScale,sin(fShtHeadingToDraw+180)*fTargetScale,0>>)
							DRAW_DEBUG_POLY(v1,v2,v3,0,0,0,120)
							DRAW_DEBUG_POLY(v1,v3,v2,0,0,0,120)
							DRAW_DEBUG_POLY(v1,v3,v4,0,0,0,120)
							DRAW_DEBUG_POLY(v1,v4,v3,0,0,0,120)
						ENDIF
					ENDIF										
				ENDIF
				
				IF targetList[i].status = EXPLODING
					int iX			
					
					vector vArc
					
					FOR iX = 0 to 6
						vArc = <<(chunck[iX].ux*targetList[i].triggerTime),0,(chunck[iX].vx*targetList[i].triggerTime) + (0.5*-40*(targetList[i].triggerTime*targetList[i].triggerTime))>>						
						vArc = targetList[i].vCurrent + <<cos(targetList[i].fShtHeading)*vArc.x,sin(targetList[i].fShtHeading)*vArc.x,vArc.z>>
						
						v1 = vArc + (<<cos(targetList[i].fShtHeading)*(fTargetScale*chunck[iX].v1.x),sin(targetList[i].fShtHeading)*(fTargetScale*chunck[iX].v1.x),chunck[iX].v1.z*fTargetScale*3>>)
						v2 = vArc + (<<cos(targetList[i].fShtHeading)*(fTargetScale*chunck[iX].v2.x),sin(targetList[i].fShtHeading)*(fTargetScale*chunck[iX].v2.x),chunck[iX].v2.z*fTargetScale*3>>)
						v3 = vArc + (<<cos(targetList[i].fShtHeading)*(fTargetScale*chunck[iX].v3.x),sin(targetList[i].fShtHeading)*(fTargetScale*chunck[iX].v3.x),chunck[iX].v3.z*fTargetScale*3>>)
						
						IF v1.z > targetList[i].vCurrent.z -0.4
						AND v2.z > targetList[i].vCurrent.z -0.4
						AND v3.z > targetList[i].vCurrent.z -0.4
							
							DRAW_DEBUG_POLY(v1,v2,v3,0,0,255)
							DRAW_DEBUG_POLY(v1,v3,v2,0,0,255)
						ENDIF				
					ENDFOR
					
					
				
				ENDIF
			ENDREPEAT
		ENDIF
		
		IF bResetTargets
			bResetTargets=FALSE
			int i
			REPEAT count_of(targetList) i
				targetList[i].status = DOESNT_EXIST
				targetList[i].vCoord = <<0,0,0>>
				targetList[i].fShtHeading = 0
				targetList[i].iMovementType = 0
				targetList[i].vSlideIn = <<0,0,0>>
				targetList[i].triggerTime = 0.0
				targetList[i].slideFlag = 0
				targetList[i].wave = 0			
			ENDREPEAT
			
			iTargetCounter=0
			vTargetNudge = <<0,0,0>>
			fShtHeading = targetList[0].fShtHeading
			vSlideFrom = targetList[0].vSlideIn
			iMovementType = targetList[0].iMovementType		
			bIsLooped = targetList[0].loop
			
		ENDIF
		
		
		
		IF bExport
			bExport = false
			int i
			REPEAT count_of(targetList) i	

				IF targetList[i].status != DOESNT_EXIST
					SAVE_STRING_TO_DEBUG_FILE("targetList[")
					SAVE_INT_TO_DEBUG_FILE(i)
					SAVE_STRING_TO_DEBUG_FILE("].status = WAITING")											
					SAVE_NEWLINE_TO_DEBUG_FILE()
				
					SAVE_STRING_TO_DEBUG_FILE("targetList[")
					SAVE_INT_TO_DEBUG_FILE(i)
					SAVE_STRING_TO_DEBUG_FILE("].vCoord = ")
					SAVE_VECTOR_TO_DEBUG_FILE(targetList[i].vCoord)
					SAVE_NEWLINE_TO_DEBUG_FILE()
					
					SAVE_STRING_TO_DEBUG_FILE("targetList[")
					SAVE_INT_TO_DEBUG_FILE(i)
					SAVE_STRING_TO_DEBUG_FILE("].vSlideIn = ")
					SAVE_VECTOR_TO_DEBUG_FILE(targetList[i].vSlideIn)
					SAVE_NEWLINE_TO_DEBUG_FILE()
					
					SAVE_STRING_TO_DEBUG_FILE("targetList[")
					SAVE_INT_TO_DEBUG_FILE(i)
					SAVE_STRING_TO_DEBUG_FILE("].fShtHeading = ")
					SAVE_FLOAT_TO_DEBUG_FILE(targetList[i].fShtHeading)
					SAVE_NEWLINE_TO_DEBUG_FILE()
					
					SAVE_STRING_TO_DEBUG_FILE("targetList[")
					SAVE_INT_TO_DEBUG_FILE(i)
					SAVE_STRING_TO_DEBUG_FILE("].iMovementType = ")
					SAVE_INT_TO_DEBUG_FILE(targetList[i].iMovementType)
					SAVE_NEWLINE_TO_DEBUG_FILE()
					
					SAVE_STRING_TO_DEBUG_FILE("targetList[")
					SAVE_INT_TO_DEBUG_FILE(i)
					SAVE_STRING_TO_DEBUG_FILE("].wave = ")
					SAVE_NEWLINE_TO_DEBUG_FILE()
					
					SAVE_STRING_TO_DEBUG_FILE("targetList[")
					SAVE_INT_TO_DEBUG_FILE(i)
					IF targetList[i].loop
						SAVE_STRING_TO_DEBUG_FILE("].loop = TRUE")
					ELSE
						SAVE_STRING_TO_DEBUG_FILE("].loop = FALSE")
					ENDIF					
					SAVE_NEWLINE_TO_DEBUG_FILE()
					
					SAVE_STRING_TO_DEBUG_FILE("targetList[")
					SAVE_INT_TO_DEBUG_FILE(i)
					SAVE_STRING_TO_DEBUG_FILE("].pauseTime = ")
					SAVE_NEWLINE_TO_DEBUG_FILE()
					
					
				ENDIF
			ENDREPEAT
		ENDIF
endproc
#endif








FUNC BOOL PS_DLC_Shooting_Range_MainUpdate()

	IF TempDebugInput() AND NOT bFinishedChallenge
		SET_WANTED_LEVEL_MULTIPLIER(0.0)
		SET_MAX_WANTED_LEVEL(0)	
		
		SET_SCENARIO_PED_DENSITY_MULTIPLIER_THIS_FRAME(0,0)
		
		PS_LESSON_UDPATE()
	
		PS_PLAY_SR_BRIEF()
		#if is_debug_build editTargetCoords() #endif
		
		CONTROL_TARGETS()
		
		IF DOES_ENTITY_EXIST(PS_Main.playerVehicle)
			FREEZE_ENTITY_POSITION(PS_Main.playerVehicle, FALSE)
		ENDIF

		IF PS_DLC_Shooting_Range_State >= PS_DLC_SHOOTING_RANGE_GUN_TARGETS_ROUND_1
		AND PS_DLC_Shooting_Range_State <= PS_DLC_SHOOTING_RANGE_GUN_TARGETS_ROUND_3
			IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),<<71.0589, -414.5967, 73.6531>>) > 180
				PRINT_NOW("PS_SHTR_INS4",1,1)
			ENDIF			
		ENDIF
		

		SWITCH(PS_DLC_Shooting_Range_State)
		//Spawn player and plane on the runway
			CASE PS_DLC_SHOOTING_RANGE_INIT
				// for compile
				fShtHeading = fShtHeading
				iCurrentTarget = iCurrentTarget
				vSlideFrom = vSlideFrom
				iMovementType = iMovementType
				vTargetNudge = vTargetNudge
				iTargetCounter = iTargetCounter
				bResetTargets = bResetTargets
				bLoadTargets[0] = bLoadTargets[0]
			
				PS_AUTO_PILOT(PS_Main, 0.0,false)
				
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						CLEAR_AREA(GET_ENTITY_COORDS(player_ped_id()),1000,true)
						SET_GAMEPLAY_CAM_RELATIVE_PITCH(-7)
						SET_VEHICLE_MISSILE_WARNING_ENABLED(ps_main.myVehicle,FALSE)
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_EXIT
						PS_DLC_Shooting_Range_State = PS_DLC_SHOOTING_RANGE_COUNTDOWN
						IF DOES_ENTITY_EXIST(ps_main.myVehicle)
							IF IS_VEHICLE_DRIVEABLE(ps_main.myVehicle)							
								SET_ENTITY_VELOCITY(ps_main.myVehicle,<<0,0,0.1>>)
								SET_ENTITY_HEALTH(ps_main.myVehicle,1000)
								SET_VEHICLE_ENGINE_HEALTH(ps_main.myVehicle,1000)
							//	TASK_HELI_MISSION(player_ped_id(),ps_main.myVehicle,null,null,GET_ENTITY_COORDS(player_ped_id()),MISSION_GOTO,1.0,20.0,0.0,0,0) 
							ENDIF
						ENDIF
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				
				clear_ped_tasks(player_ped_id())
			BREAK

			CASE PS_DLC_SHOOTING_RANGE_COUNTDOWN
			
				PS_AUTO_PILOT(PS_Main, 0.0,false)
				PS_DISABLE_VEHICLE_ATTACKS()
				
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						SET_HELI_BLADES_FULL_SPEED( PS_Main.myVehicle )
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF	PS_HUD_UPDATE_COUNTDOWN(TRUE,FALSE) //1935348 set skip to false
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						cprintln(debug_trevor3,"shootingRange. previewed = ",PS_Main.myChallengeData.HasBeenPreviewed)
						cprintln(debug_trevor3,"last score: ",saveData[g_current_selected_dlc_PilotSchool_class].LastLandingDistance)
						
						IF PS_Main.myChallengeData.HasBeenPreviewed
						OR saveData[g_current_selected_dlc_PilotSchool_class].LastLandingDistance > 0
						OR saveData[g_current_selected_dlc_PilotSchool_class].LastLandingDistance = -2 //special case hack just to skip to the end.
							PS_DLC_Shooting_Range_State = PS_DLC_SHOOTING_RANGE_GUN_TARGETS_ROUND_3
						ELSE
							PS_DLC_Shooting_Range_State = PS_DLC_SHOOTING_RANGE_TAKEOFF
						ENDIF
						PS_INCREMENT_SUBSTATE()
						DO_NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						//SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						PS_TRIGGER_MUSIC(PS_MUSIC_SHOOT_START)
						BREAK
				ENDSWITCH
			BREAK
				
		//Give player control and the first instruction to take off
		//Check if player is staying on the runway and not driving off
			CASE PS_DLC_SHOOTING_RANGE_TAKEOFF
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						
						//PS_SET_CHECKPOINT_COUNTER_ACTIVE(TRUE)
						//SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						DO_NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
							
						//SET_PLAYER_INVINCIBLE(PLAYER_ID(),FALSE)
//						//SET_PLAYER_INVINCIBLE(PLAYER_ID(), TRUE)
						PS_ACTIVATE_CHECKPOINT_MGR(PS_LESSON_OBSTACLE_PLANE)
						PS_TRIGGER_SR_BRIEF(SR_BRIEF_GET_TO_BUILDING_SITE)//"Take your best shot at this obstacle course. There are going to be special gates that are going to require you to fly upside down or knife through them."
					//	PS_HUD_RESET_TIMER()
						PS_HUD_SET_GUTTER_ICON_ACTIVE(TRUE)
					//	PS_SET_TIMER_ACTIVE(TRUE)
						PS_SET_HINT_CAM_ACTIVE(TRUE)
						PS_SET_HINT_CAM_COORD(PS_GET_CURRENT_CHECKPOINT())
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF PS_DLC_Shooting_Range_Fail_Check()
							PS_DLC_Shooting_Range_State = PS_DLC_SHOOTING_RANGE_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ELIF PS_DLC_Shooting_Range_Progress_Check()	
							PS_TRIGGER_SR_BRIEF(SR_BRIEF_SHOOT_TARGETS)
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						PS_DLC_Shooting_Range_State = PS_DLC_SHOOTING_RANGE_GUN_TARGETS_ROUND_1
						PS_INCREMENT_SUBSTATE()
						
						BREAK
				ENDSWITCH
				BREAK
		
			CASE PS_DLC_SHOOTING_RANGE_GUN_TARGETS_ROUND_1
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_KILL_HINT_CAM()
						//PRINT_HELP("PS_HELP_GATE")	//"~g~green gate ~s~- Knife flight~n~"
						attackStage = 0
						bRoundActive = TRUE
						//bDrawDebugTargets = TRUE
 				    	PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
					
						
					
					
						IF PS_DLC_Shooting_Range_Fail_Check()
							PS_DLC_Shooting_Range_State = PS_DLC_SHOOTING_RANGE_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ENDIF
						
						IF iThisWave = 4
							PS_DLC_Shooting_Range_State = PS_DLC_SHOOTING_RANGE_GUN_TARGETS_ROUND_2
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
							PS_TRIGGER_SR_BRIEF(SR_BRIEF_NEXT_TARGETS)
						ENDIF
						
					BREAK
					CASE PS_SUBSTATE_EXIT
					
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
			BREAK
			
			CASE PS_DLC_SHOOTING_RANGE_GUN_TARGETS_ROUND_2
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_KILL_HINT_CAM()				
						
						bRoundActive = TRUE
					
 				    	PS_INCREMENT_SUBSTATE()
						PREP_ATTACK_STAGE(1,4)
						
					BREAK
					CASE PS_SUBSTATE_UPDATE
					
						
					
					
						IF PS_DLC_Shooting_Range_Fail_Check()
							PS_DLC_Shooting_Range_State = PS_DLC_SHOOTING_RANGE_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						//ELIF PS_DLC_Shooting_Range_Progress_Check()
						//	PS_INCREMENT_SUBSTATE()
						ENDIF
						
						
						IF iThisWave = 7
							PS_TRIGGER_SR_BRIEF(SR_BRIEF_FINAL_ROUND)
							PS_DLC_Shooting_Range_State = PS_DLC_SHOOTING_RANGE_GUN_TARGETS_ROUND_3_PREP
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ENDIF
						
					BREAK
					CASE PS_SUBSTATE_EXIT
					
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
			BREAK
			
			CASE PS_DLC_SHOOTING_RANGE_GUN_TARGETS_ROUND_3_PREP
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							PS_INCREMENT_SUBSTATE()
						ENDIF
					BREAK
					CASE PS_SUBSTATE_UPDATE
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							PS_DLC_Shooting_Range_State = PS_DLC_SHOOTING_RANGE_GUN_TARGETS_ROUND_3
							PS_Challenges[PSCD_DLC_ShootingRange].HasBeenPreviewed = TRUE
							saveData[PSCD_DLC_ShootingRange].LastLandingDistance = -2 //to stop preview happening a second time
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ENDIF
					BREAK
				ENDSWITCH
			BREAK
			
			CASE PS_DLC_SHOOTING_RANGE_GUN_TARGETS_ROUND_3
	
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_KILL_HINT_CAM()						
						PS_TRIGGER_MUSIC(PS_MUSIC_SHOOT_TIMED)
						bRoundActive = TRUE
						DO_NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						//SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
 				    	PS_INCREMENT_SUBSTATE()
						PREP_ATTACK_STAGE(2)
						PS_HUD_RESET_TIMER()	
						PS_START_HOURGLASS_TIMER(120000)
						PS_TRIGGER_SR_BRIEF(SR_BRIEF_FINAL_GOD_TEXT)
												
						PS_Main.targetsDestroyed = 0
						cprintln(debug_trevor3,"START SHOOTOUT 3")
						//PS_SET_TIMER_ACTIVE(TRUE)
					BREAK
					CASE PS_SUBSTATE_UPDATE
					
						CLEAR_DEBRIS()
					
					
						IF PS_DLC_Shooting_Range_Fail_Check()
							PS_DLC_Shooting_Range_State = PS_DLC_SHOOTING_RANGE_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						//ELIF PS_DLC_Shooting_Range_Progress_Check()
						//	PS_INCREMENT_SUBSTATE()
						ENDIF
						
						
						
						IF PS_UI_RaceHud.bIsHourGlassActive
							IF PS_HUD_GET_HOURGLASS_TIMER_VALUE() < 0
								PLAY_SOUND_FRONTEND(-1, "TIMER_STOP", "HUD_MINI_GAME_SOUNDSET")
								PS_INCREMENT_SUBSTATE()							
							ENDIF
							
							//do countdown sound
							IF PS_HUD_GET_HOURGLASS_TIMER_VALUE() <= iLastSoundTime
								PLAY_SOUND_FRONTEND(-1, "10_SEC_WARNING", "HUD_MINI_GAME_SOUNDSET")
								IF iLastSoundTime <= 5000
									iLastSoundTime -= 500
								ELSE
									iLastSoundTime -= 1000
								ENDIF
							ENDIF
						ENDIF
						
						int MeterNumber
						MeterNumber = /*PS_HUD_GET_HOURGLASS_TIMER_PADDING() +*/ PS_HUD_GET_HOURGLASS_TIMER_VALUE()
						int MeterMaxNum 
						MeterMaxNum = PS_HUD_GET_HOURGLASS_MAX_TIME()
						string MeterTitle 
						MeterTitle = "PS_REMAINING"
						
						DRAW_GENERIC_SCORE(PS_Main.targetsDestroyed, "FS_TARGS")
						IF PS_Main.targetsDestroyed < PS_Challenges[g_current_selected_dlc_PilotSchool_class].BronzeDistance
							DRAW_GENERIC_SCORE(FLOOR(PS_Challenges[g_current_selected_dlc_PilotSchool_class].BronzeDistance), "PS_HUDTARG_B",-1,HUD_COLOUR_BRONZE,HUDORDER_SECONDBOTTOM)
						ELIF PS_Main.targetsDestroyed < PS_Challenges[g_current_selected_dlc_PilotSchool_class].SilverDistance
							DRAW_GENERIC_SCORE(FLOOR(PS_Challenges[g_current_selected_dlc_PilotSchool_class].SilverDistance), "PS_HUDTARG_S",-1,HUD_COLOUR_SILVER,HUDORDER_SECONDBOTTOM)
						ELSE 
							DRAW_GENERIC_SCORE(FLOOR(PS_Challenges[g_current_selected_dlc_PilotSchool_class].GoldDistance), "PS_HUDTARG_G",-1,HUD_COLOUR_GOLD,HUDORDER_SECONDBOTTOM)
						ENDIF
						DRAW_GENERIC_METER(MeterNumber, MeterMaxNum, MeterTitle, HUD_COLOUR_RED, -1, HUDORDER_SIXTHBOTTOM, -1, -1, FALSE, TRUE)
						
					BREAK
					CASE PS_SUBSTATE_EXIT
						PS_UPDATE_RECORDS(VECTOR_ZERO)
						IF Pilot_School_Data_Check_Mission_Success(PS_Main.myChallengeData, PS_Main.myPlayerData)
							PS_DLC_Shooting_Range_State = PS_DLC_SHOOTING_RANGE_SUCCESS
						ELSE
							PS_DLC_Shooting_Range_State = PS_DLC_SHOOTING_RANGE_FAIL
						ENDIF
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
			BREAK
			
		
												
			CASE PS_DLC_SHOOTING_RANGE_SUCCESS
			
				DO_NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, FALSE, FALSE, TRUE)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
				
				#IF IS_DEBUG_BUILD
					PRINTLN("IN SUCCESS LOOP STATE: ", ENUM_TO_INT(PS_GET_SUBSTATE()))
				#ENDIF
				
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_TRIGGER_MUSIC(PS_MUSIC_SHOOT_PASS)
						PS_UPDATE_RECORDS(VECTOR_ZERO)
						//PS_PLAY_LESSON_FINISHED_LINE("PS_POC", "PS_POC_5", "PS_POC_6", "PS_POC_7", "PS_POC_8")
						
						SR_currentBrief = SR_BRIEF_NONE
						
						/* temp below while dialogue screwed */
						IF iCurrentChallengeScore >= iSCORE_FOR_GOLD							
							PS_PLAY_DISPATCHER_INSTRUCTION("PS_SRa","PS_SRa_11")
						ELIF iCurrentChallengeScore >= iSCORE_FOR_SILVER							
							PS_PLAY_DISPATCHER_INSTRUCTION("PS_SRa","PS_SRa_10")
						ELIF iCurrentChallengeScore >= iSCORE_FOR_BRONZE
							PS_PLAY_DISPATCHER_INSTRUCTION("PS_SRa","PS_SRa_9")
						ENDIF
						PS_LESSON_END(TRUE)
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						
						//IF NOT IS_PLAYER_DEAD(player_id())
						#IF IS_DEBUG_BUILD
							PRINTLN("MANUAL RESPAWN STATE STATE (SHOULD BE 3): ", ENUM_TO_INT(GET_MANUAL_RESPAWN_STATE()))
						#ENDIF
						
						IF NOT IS_PLAYER_DEAD(player_id())	
						OR GET_MANUAL_RESPAWN_STATE()= MRS_READY_FOR_PLACEMENT
							IF NOT PS_HUD_IS_SCORECARD_ACTIVE()
								PS_INCREMENT_SUBSTATE()
							ENDIF
						ELSE
							//Set the player to respawn state if dead and doesn't have respawn state set
							IF IS_PLAYER_DEAD(player_id())
							AND GET_MANUAL_RESPAWN_STATE() = MRS_NULL
								SET_MANUAL_RESPAWN_STATE(MRS_RESPAWN_PLAYER_HIDDEN)
							ENDIF
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						bFinishedChallenge = TRUE
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
				
			CASE PS_DLC_SHOOTING_RANGE_FAIL
			
				DO_NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, FALSE, FALSE, TRUE)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
				
				#IF IS_DEBUG_BUILD
					PRINTLN("IN FAIL LOOP STATE: ", ENUM_TO_INT(PS_GET_SUBSTATE()))
				#ENDIF
				
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
					
						SR_currentBrief = SR_BRIEF_NONE
					
						PS_TRIGGER_MUSIC(PS_MUSIC_SHOOT_FAIL)
						PS_UPDATE_RECORDS(VECTOR_ZERO)
						//PS_PLAY_LESSON_FINISHED_LINE("PS_POC", "PS_POC_5", "PS_POC_6", "PS_POC_7", "PS_POC_8")
						PS_LESSON_END(FALSE)
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF NOT PS_HUD_IS_SCORECARD_ACTIVE()
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						bFinishedChallenge = TRUE
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
		ENDSWITCH
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC PS_DLC_Shooting_Range_MainCleanup()
//hud
	int i
	RESET_ALL_TARGETS()	
	PS_HUD_SET_GUTTER_ICON_ACTIVE(FALSE)
	PS_HUD_SET_OBJECTIVE_METER_VISIBLE(FALSE)
	PS_SET_TIMER_ACTIVE(FALSE)
	PS_HUD_CLEANUP()
	CLEAR_PRINTS()
	CLEAR_HELP()

//player/objects
	PS_SEND_PLAYER_BACK_TO_MENU()

//cleanup checkpoints
	PS_RESET_CHECKPOINT_MGR()
	
	SR_currentBrief = SR_BRIEF_NONE
	
	//stop sounds
	REPEAT COUNT_OF(targetList) i
		PRINTLN("TARGET ID: ", i, " SOUND ID: ", targetList[i].sound)
		IF targetList[i].sound != -1
			PRINTLN("CLEANED UP SOUNDS WITH ID: ", targetList[i].sound)
			IF NOT HAS_SOUND_FINISHED(targetList[i].sound)
				STOP_SOUND(targetList[i].sound)
			ENDIF
			RELEASE_SOUND_ID(targetList[i].sound)
			targetList[i].sound = -1
		ENDIF
	ENDREPEAT
	
//assets
	REMOVE_VEHICLE_RECORDING(iPS_PreviewVehicleRecordingID, "PilotSchool")
	SET_MODEL_AS_NO_LONGER_NEEDED(VehicleToUse)	
	SET_MODEL_AS_NO_LONGER_NEEDED(targetPropBot)
	SET_MODEL_AS_NO_LONGER_NEEDED(targetPropSide)
	SET_MODEL_AS_NO_LONGER_NEEDED(targetPropArm)
	SET_MODEL_AS_NO_LONGER_NEEDED(targetPropMover)
	
	// Allow the blocked planes from being generated in the two boarding locations.
	//VECTOR vGenBlockMin = <<-1514.927490,-3203.546387,26.694818>> - << 200.0, 200.0, 20.0 >>
	//VECTOR VGenBlockMax = <<-1514.927490,-3203.546387,26.694818>> + << 200.0, 200.0, 20.0 >>
	//SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(vGenBlockMin, VGenBlockMax, TRUE)
	UNBLOCK_SCENARIOS_AND_AMBIENT(sb_PS)
	
	#IF IS_DEBUG_BUILD
		DEBUG_MESSAGE("******************Cleaning up PS_DLC_SHOOTING_RANGE.sc******************")
	#ENDIF
ENDPROC

// EOF
//****************************************************************************************************
//****************************************************************************************************

