//****************************************************************************************************
//
//	Author: 		Kevin Bolt
//	Date: 			07/01/14
//	Description:	Formation flying challenge
//****************************************************************************************************

USING "minigames_helpers.sch"
USING "Pilot_School_MP_Challenge_Helpers.sch"
//USING "rGeneral_include.sch"

//****************************************************************************************************
//****************************************************************************************************
//	Private functions that are used only by Outside Loop
//****************************************************************************************************
//****************************************************************************************************





int iScoreTimer


float fRecSpeed[3]
int iCountdown

vector vFlyToCoord,vLastFlytoCoord

float fLastRangeCheck
int iRangeWarnings
int allowNextWarningTime


CHECKPOINT_INDEX thisCheckpoint

BOOL bPlayerScoring

enum PS_FO_enumBrief
	FO_BRIEF_NONE,
	FO_BRIEF_TAKEOFF,
	FO_BRIEF_TAKEOFF_INS,
	FO_BRIEF_FORMATION_1_START_DIA,
	FO_BRIEF_FORMATION_1_START_INS,
	FO_BRIEF_FORMATION_1_IN_POSITION,
	FO_BRIEF_FORMATION_1_IN_POSITION_INS,
	FO_BRIEF_FORMATION_1_IN_POSITION_INS_HELP,
	FO_BRIEF_COMMENT_ON_PERFORMANCE,
	FO_BRIEF_FORMATION_2_LINE_PREP,
	FO_BRIEF_FORMATION_2_LINE_IN_POS,
	FO_BRIEF_FORMATION_2_CHECK_IN_FORMATION,
	FO_BRIEF_FORMATION_3_TURN_PREP,
	FO_BRIEF_FORMATION_3_TURN_PERFORM,
	FO_BRIEF_FORMATION_3_TURN_PERFORM_2,
	FO_BRIEF_FORMATION_4_vertical_prep,
	FO_BRIEF_FORMATION_4_vertical_prep_ins,
	FO_BRIEF_FORMATION_4_vertical_perform,
	FO_BRIEF_FORMATION_4_vertical_perform_ins,
	FO_BRIEF_FORMATION_4_vertical_perform_score,
	FO_BRIEF_FORMATION_5_vertical_prep,
	FO_BRIEF_FORMATION_5_vertical_perform,
	FO_BRIEF_FORMATION_5_vertical_perform_ins,
	FO_BRIEF_FORMATION_5_vertical_perform_score,	
	FO_BRIEF_FORMATION_6_loop_prep,
	FO_BRIEF_FORMATION_6_loop_perform,
	FO_BRIEF_FORMATION_6_loop_react,
	FO_BRIEF_FORMATION_6_loop_complete,
	FO_BRIEF_FORMATION_7_halfloop_prep,
	FO_BRIEF_FORMATION_7_halfloop_perf,
	FO_BRIEF_FORMATION_7_halfloop_end,
	FO_BRIEF_FORMATION_8_perf
endenum

int iBriefTimer

PS_FO_enumBrief FO_currentBrief = FO_BRIEF_NONE
PS_FO_enumBrief mostRecentBrief = FO_BRIEF_NONE

PROC PS_TRIGGER_FO_BRIEF(PS_FO_enumBrief thisSRBrief)
	
	IF thisSRBrief > mostRecentBrief
	//AND FO_currentBrief = FO_BRIEF_NONE
		mostRecentBrief = thisSRBrief
		cprintln(debug_trevor3,"Trigger ",enum_to_int(thisSRBrief))
		FO_currentBrief = thisSRBrief
	ENDIF
ENDPROC



PROC PS_PLAY_FO_BRIEF()

	//cprintln(debug_trevor3,ENUM_TO_INT(GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_HELI)))
	
	IF FO_currentBrief != FO_BRIEF_NONE
		IF IS_VEHICLE_DRIVEABLE(vehBESRA[0])
			IF GET_DISTANCE_BETWEEN_ENTITIES(player_ped_id(),vehBESRA[0]) > 150.0
				IF FO_currentBrief > FO_BRIEF_FORMATION_1_IN_POSITION_INS_HELP
					//player off course. Play appropriate line
					FO_currentBrief = FO_BRIEF_NONE
					PS_PLAY_DISPATCHER_INSTRUCTION_CONVERSATION("PS_FOnot")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	SWITCH FO_currentBrief
		CASE FO_BRIEF_TAKEOFF
			PS_PLAY_DISPATCHER_INSTRUCTION("PS_FOa","PS_FOa_1")
			FO_currentBrief = FO_BRIEF_TAKEOFF_INS
		BREAK
		CASE FO_BRIEF_TAKEOFF_INS
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				PRINT_NOW("PS_FRMN_GOD1",DEFAULT_GOD_TEXT_TIME,1)
				FO_currentBrief = FO_BRIEF_NONE
			ENDIF
		BREAK
		CASE FO_BRIEF_FORMATION_1_START_DIA			
			PS_PLAY_DISPATCHER_INSTRUCTION("PS_FOa","PS_FOa_2")
			FO_currentBrief = FO_BRIEF_FORMATION_1_IN_POSITION_INS
		BREAK
	
	
		CASE FO_BRIEF_FORMATION_1_IN_POSITION_INS
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				CLEAR_PRINTS()
				PRINT_NOW("PS_FRMN_GOD3",DEFAULT_GOD_TEXT_TIME,1)				
				FO_currentBrief = FO_BRIEF_FORMATION_1_IN_POSITION_INS_HELP
				iBriefTimer = GET_GAME_TIMER() + 1500
			ENDIF
		BREAK
		
		CASE FO_BRIEF_FORMATION_1_IN_POSITION_INS_HELP
			IF GET_GAME_TIMER() > iBriefTimer
				PRINT_HELP("PS_FRMN_HLP1")
				FO_currentBrief = FO_BRIEF_COMMENT_ON_PERFORMANCE
			ENDIF
		BREAK
		
		CASE FO_BRIEF_COMMENT_ON_PERFORMANCE
			IF IS_VEHICLE_DRIVEABLE(vehBESRA[0])
				IF (GET_TIME_POSITION_IN_RECORDING(vehBESRA[0]) > 22000 AND GET_TIME_POSITION_IN_RECORDING(vehBESRA[0]) < 22500)
				OR (GET_TIME_POSITION_IN_RECORDING(vehBESRA[0]) > 55000 AND GET_TIME_POSITION_IN_RECORDING(vehBESRA[0]) < 55500)
				OR (GET_TIME_POSITION_IN_RECORDING(vehBESRA[0]) > 73000 AND GET_TIME_POSITION_IN_RECORDING(vehBESRA[0]) < 74000)
				OR (GET_TIME_POSITION_IN_RECORDING(vehBESRA[0]) > 93000 AND GET_TIME_POSITION_IN_RECORDING(vehBESRA[0]) < 93500)
				OR (GET_TIME_POSITION_IN_RECORDING(vehBESRA[0]) > 117000 AND GET_TIME_POSITION_IN_RECORDING(vehBESRA[0]) < 118000)
				OR (GET_TIME_POSITION_IN_RECORDING(vehBESRA[0]) > 134000 AND GET_TIME_POSITION_IN_RECORDING(vehBESRA[0]) < 134500)
				
					IF DOES_BLIP_EXIST(thisMovingTargetBlipFormation)
						IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),GET_BLIP_COORDS(thisMovingTargetBlipFormation)) < 15.0
							PS_PLAY_DISPATCHER_INSTRUCTION_CONVERSATION("PS_FOare")
						ELSE
							PS_PLAY_DISPATCHER_INSTRUCTION_CONVERSATION("PS_FOnot")
						ENDIF							 	
						FO_currentBrief = FO_BRIEF_NONE
					ENDIF
				ENDIF
			ENDIF
					
		BREAK
	
		
		CASE FO_BRIEF_FORMATION_2_LINE_PREP
			PS_PLAY_DISPATCHER_INSTRUCTION_CONVERSATION("PS_FOman")
			FO_currentBrief = FO_BRIEF_FORMATION_2_LINE_IN_POS
		BREAK
		
		CASE FO_BRIEF_FORMATION_2_LINE_IN_POS			
			//PS_PLAY_DISPATCHER_INSTRUCTION("PS_FORi","PS_FORi_5")
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				PS_PLAY_DISPATCHER_INSTRUCTION_CONVERSATION("PS_FOnew1")
				FO_currentBrief = FO_BRIEF_NONE
			ENDIF
			//PRINT_NOW("PS_FRMN_GOD4",DEFAULT_GOD_TEXT_TIME,1)
			//FO_currentBrief = FO_BRIEF_FORMATION_2_CHECK_IN_FORMATION //FO_BRIEF_COMMENT_ON_PERFORMANCE			
		BREAK
		
		CASE FO_BRIEF_FORMATION_2_CHECK_IN_FORMATION
			IF IS_MESSAGE_BEING_DISPLAYED()
				IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),GET_BLIP_COORDS(thisMovingTargetBlipFormation)) < 20.0
					FO_currentBrief = FO_BRIEF_NONE
					CLEAR_PRINTS()
				ENDIF
			ELSE
				FO_currentBrief = FO_BRIEF_NONE
			ENDIF
		BREAK
		
		CASE FO_BRIEF_FORMATION_3_TURN_PREP
			PS_PLAY_DISPATCHER_INSTRUCTION("PS_FORi","PS_FORi_6")
			FO_currentBrief = FO_BRIEF_NONE
			//FO_currentBrief = FO_BRIEF_FORMATION_3_TURN_PERFORM
		BREAK
		
		CASE FO_BRIEF_FORMATION_3_TURN_PERFORM
			//PRINT_NOW("PS_FRMN_GODC",DEFAULT_GOD_TEXT_TIME,1)
			PS_PLAY_DISPATCHER_INSTRUCTION("PS_FOa","PS_FOa_2")
			
			FO_currentBrief = FO_BRIEF_FORMATION_3_TURN_PERFORM_2
		
		BREAK
		
		CASE FO_BRIEF_FORMATION_3_TURN_PERFORM_2
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				PS_PLAY_DISPATCHER_INSTRUCTION_CONVERSATION("PS_FOnew2")
				FO_currentBrief = FO_BRIEF_COMMENT_ON_PERFORMANCE
			ENDIF
		BREAK
		
		CASE FO_BRIEF_FORMATION_4_vertical_prep		
			PS_PLAY_DISPATCHER_INSTRUCTION("PS_FOadd","PS_FOadd_3")
			//PRINT_NOW("PS_FRMN_GOD4",DEFAULT_GOD_TEXT_TIME,1)
			FO_currentBrief = FO_BRIEF_FORMATION_4_vertical_perform		
		BREAK
		
	
		CASE FO_BRIEF_FORMATION_4_vertical_perform
			IF GET_TIME_POSITION_IN_RECORDING(vehBESRA[0]) > 66000
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					PS_PLAY_DISPATCHER_INSTRUCTION("PS_FOnew2","PS_FOnew2_3")
					FO_currentBrief = FO_BRIEF_COMMENT_ON_PERFORMANCE
				ENDIF	
			ENDIF
		BREAK
		
		CASE FO_BRIEF_FORMATION_4_vertical_perform_ins
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				//PRINT_NOW("PS_FRMN_GOD5",DEFAULT_GOD_TEXT_TIME,1)	
				FO_currentBrief = FO_BRIEF_COMMENT_ON_PERFORMANCE
				iDialogueTimer = -1
			ENDIF
		BREAK
		

		
		CASE FO_BRIEF_FORMATION_5_vertical_prep
			FO_currentBrief = FO_BRIEF_COMMENT_ON_PERFORMANCE
		//	PRINT_NOW("PS_FRMN_GOD4",DEFAULT_GOD_TEXT_TIME,1)
		//	FO_currentBrief = FO_BRIEF_NONE			
		BREAK
		
		CASE FO_BRIEF_FORMATION_5_vertical_perform
			CLEAR_PRINTS()			
			//PS_PLAY_DISPATCHER_INSTRUCTION("PS_FOa","PS_FOa_10")
			FO_currentBrief = FO_BRIEF_FORMATION_5_vertical_perform_ins		
		BREAK
		
		CASE FO_BRIEF_FORMATION_5_vertical_perform_ins
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()		
				PS_PLAY_DISPATCHER_INSTRUCTION_CONVERSATION("PS_FOman")
				FO_currentBrief = FO_BRIEF_COMMENT_ON_PERFORMANCE
		
			ENDIF
		BREAK
		

		
		CASE FO_BRIEF_FORMATION_6_loop_prep
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				PS_PLAY_DISPATCHER_INSTRUCTION("PS_FORi","PS_FORi_7")
				FO_currentBrief = FO_BRIEF_NONE
			ENDIF
		BREAK
		
		CASE FO_BRIEF_FORMATION_6_loop_perform
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				PS_PLAY_DISPATCHER_INSTRUCTION_CONVERSATION("PS_FOnew5")			
				FO_currentBrief = FO_BRIEF_FORMATION_6_loop_react
			ENDIF
		BREAK
		
		CASE FO_BRIEF_FORMATION_6_loop_react
			IF IS_VEHICLE_DRIVEABLE(PS_Main.myVehicle)
				float jetPitch
				jetPitch = GET_ENTITY_PITCH(PS_Main.myVehicle)
				
				cprintln(debug_trevor3,"pitch = ",jetPitch)
				IF DOES_BLIP_EXIST(thisMovingTargetBlipFormation)
					if jetPitch > 90.0 
					OR jetPitch < -90.0
						cprintln(debug_trevor3,"upside down")
						IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),GET_BLIP_COORDS(thisMovingTargetBlipFormation)) < 40.0
							PS_PLAY_DISPATCHER_INSTRUCTION_CONVERSATION("PS_FOare")
						ELSE
							PS_PLAY_DISPATCHER_INSTRUCTION_CONVERSATION("PS_FOnot")
						ENDIF
					ENDIF
				ENDIF
			ENDIF					
		BREAK
	
		CASE FO_BRIEF_FORMATION_6_loop_complete
			IF DOES_BLIP_EXIST(thisMovingTargetBlipFormation)				
				IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),GET_BLIP_COORDS(thisMovingTargetBlipFormation)) < 40.0
					//PS_PLAY_DISPATCHER_INSTRUCTION("PS_FOa","PS_FOa_13")
					//PRINT_NOW("PS_FRMN_GOD5",DEFAULT_GOD_TEXT_TIME,1)	
					FO_currentBrief = FO_BRIEF_COMMENT_ON_PERFORMANCE
				ENDIF			
			ENDIF			
		BREAK
		
		CASE FO_BRIEF_FORMATION_7_halfloop_prep
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				PS_PLAY_DISPATCHER_INSTRUCTION("PS_FOadd","PS_FOadd_4")
				FO_currentBrief = FO_BRIEF_NONE
			ENDIF
		BREAK
		
		CASE FO_BRIEF_FORMATION_7_halfloop_perf
			PRINT_NOW("PS_FRMN_GODE",DEFAULT_GOD_TEXT_TIME,1)	
			FO_currentBrief = FO_BRIEF_NONE
		BREAK
		
		CASE FO_BRIEF_FORMATION_7_halfloop_end
			//PS_PLAY_DISPATCHER_INSTRUCTION("PS_FOa","PS_FOa_16")
			FO_currentBrief = FO_BRIEF_FORMATION_8_perf
		BREAK
		
		CASE FO_BRIEF_FORMATION_8_perf
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				PRINT_NOW("PS_FRMN_GOD9",DEFAULT_GOD_TEXT_TIME,1)
				FO_currentBrief = FO_BRIEF_COMMENT_ON_PERFORMANCE					
			ENDIF
		BREAK
		
		/*
		CASE FO_FIRST_INSTRUCTION
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				PRINT_NOW("PS_SHTR_DIA2",DEFAULT_GOD_TEXT_TIME,1)
				FO_currentBrief = FO_BRIEF_NONE
			ENDIF
		BREAK		
		CASE FO_BRIEF_SHOOT_TARGETS
			PS_PLAY_DISPATCHER_INSTRUCTION("PS_SRa","PS_SRa_2")
			PRINT_HELP("PS_SHTR_HLP1")			
			
			FO_currentBrief = FO_SHOOT_GOD_TEXT
		BREAK
		CASE FO_SHOOT_GOD_TEXT
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				PRINT_NOW("PS_SHTR_INS2",DEFAULT_GOD_TEXT_TIME,1)
				FO_currentBrief = FO_BRIEF_USE_GUN_CAM_VIEW
				iDialogueTimer = GET_GAME_TIMER() + 6000
			ENDIF
		BREAK
		CASE FO_BRIEF_USE_GUN_CAM_VIEW
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			AND NOT IS_HELP_MESSAGE_ON_SCREEN()
			AND NOT IS_MESSAGE_BEING_DISPLAYED()
				IF GET_GAME_TIMER() > iDialogueTimer
					IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_HELI) != CAM_VIEW_MODE_FIRST_PERSON
						PS_PLAY_DISPATCHER_INSTRUCTION("PS_SRa","PS_SRa_3")
						PRINT_HELP("PS_SHTR_HLP2")
						FO_currentBrief = FO_BRIEF_NONE
					ELSE
						FO_currentBrief = FO_BRIEF_NONE
					ENDIF
				ENDIF
			ENDIF
			
		BREAK		
		CASE FO_BRIEF_NEXT_TARGETS
			PS_PLAY_DISPATCHER_INSTRUCTION_2_LINES("PS_SRa","PS_SRa_4","PS_SRa","PS_SRa_5")
			PRINT_HELP("PS_SHTR_HLP3")
			FO_currentBrief = FO_BRIEF_HIDDEN_TARGET
		BREAK						
		
		CASE FO_BRIEF_HIDDEN_TARGET
			IF iThisWave = 5
				PS_PLAY_DISPATCHER_INSTRUCTION("PS_SRa","PS_SRa_6")
				FO_currentBrief = FO_BRIEF_NONE
			ENDIF
		BREAK
		
		CASE FO_BRIEF_FINAL_ROUND
			PS_PLAY_DISPATCHER_INSTRUCTION_2_LINES("PS_SRa","PS_SRa_7","PS_SRa","PS_SRa_8")
			FO_currentBrief = FO_BRIEF_FINAL_GOD_TEXT
		BREAK
		
		CASE FO_BRIEF_FINAL_GOD_TEXT
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				PRINT_NOW("PS_SHTR_INS3",DEFAULT_GOD_TEXT_TIME,1)
				FO_currentBrief = FO_BRIEF_NONE
			ENDIF
		BREAK*/
	ENDSWITCH
ENDPROC


PROC  PS_DLC_Formation_Init_Checkpoints(INT iRouteToDisplay = 0)
	PS_RESET_CHECKPOINT_MGR()
		iRouteToDisplay=iRouteToDisplay
	


//	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<-1539.4138, -3024.9126, 30.7589>>)
//	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_LANDING, <<1,1,1>>)
	/*PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<-2708.2214, -2348.9553, 76.4800>>)
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_LANDING, <<1,1,1>>)
	PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<-462.0441, -3652.6384, -198.5847>>)*/

ENDPROC



FUNC BOOL TOO_SLOW_HITTING_MARKERS_CHECK()
	float fthisRangeCheck
	IF NOT IS_VECTOR_ZERO(vFlyToCoord)
	
		fthisRangeCheck = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),vFlyToCoord)
		
		cprintln(debug_trevor3,"fthisRangeCheck = ",fthisRangeCheck," fLastRangeCheck= ",fLastRangeCheck," GET_GAME_TIMER() = ",GET_GAME_TIMER()," allowNextWarningTime = ",allowNextWarningTime," iRangeWarnings = ",iRangeWarnings)
		IF fthisRangeCheck > fLastRangeCheck
		AND fLastRangeCheck != 0
		AND GET_DISTANCE_BETWEEN_COORDS(vFlyToCoord,vLastFlytoCoord) < 20 //just to ensure the marker hasn't jumped to a new location.
			IF fthisRangeCheck > 300
				IF GET_GAME_TIMER() > allowNextWarningTime
				AND iRangeWarnings < 3
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						//b*1934364
						//PS_PLAY_DISPATCHER_INSTRUCTION_CONVERSATION("PS_FOd")
						allowNextWarningTime = GET_GAME_TIMER() + 7000
						iRangeWarnings++
					ENDIF
				ENDIF
				//give warning message
			ENDIF
		ENDIF
		
		
	
		IF fthisRangeCheck > 500
		AND GET_GAME_TIMER() > allowNextWarningTime
		AND allowNextWarningTime != 0
			RETURN TRUE
		ENDIF
		
		fLastRangeCheck = fthisRangeCheck
		vLastFlytoCoord = vFlyToCoord
	ENDIF
	

		
	

		
	IF IS_VEHICLE_DRIVEABLE(vehBESRA[0])
	
	
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehBESRA[0])
			SWITCH PS_DLC_Formation_State
				CASE PS_DLC_FORMATION_TAKEOFF
					IF GET_TIME_POSITION_IN_RECORDING(vehBESRA[0]) > 14000
						RETURN TRUE
					ENDIF
				BREAK
				CASE PS_DLC_FORMATION_EXIT_FORMATION_ONE
					IF GET_TIME_POSITION_IN_RECORDING(vehBESRA[0]) > 35000
						RETURN TRUE
					ENDIF
				BREAK
				CASE PS_DLC_FORMATION_EXIT_TRAIL
					IF GET_TIME_POSITION_IN_RECORDING(vehBESRA[0]) > 48000
						RETURN TRUE
					ENDIF
				BREAK
				
				CASE PS_DLC_FORMATION_PREP_STACK
					IF GET_TIME_POSITION_IN_RECORDING(vehBESRA[0]) > 69000
						RETURN TRUE
					ENDIF
				BREAK
				
				CASE PS_DLC_FORMATION_PREP_BOX
					IF GET_TIME_POSITION_IN_RECORDING(vehBESRA[0]) > 92000
						RETURN TRUE
					ENDIF
				BREAK
	
				CASE PS_DLC_FORMATION_PREP_LOOP
					IF GET_TIME_POSITION_IN_RECORDING(vehBESRA[0]) > 112000
						RETURN TRUE
					ENDIF
				BREAK
				
				CASE PS_DLC_FORMATION_PREP_HALF_LOOP
					IF GET_TIME_POSITION_IN_RECORDING(vehBESRA[0]) > 128000
						RETURN TRUE
					ENDIF
				BREAK
			ENDSWITCH
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

    
FUNC BOOL PS_DLC_Formation_Fail_Check()
	//IF PS_GET_SUBSTATE() = PS_SUBSTATE_UPDATE
		SWITCH(PS_DLC_Formation_State)
			CASE PS_DLC_FORMATION_INIT
				//No fail check to do
				RETURN FALSE
			BREAK
			
			
			CASE PS_DLC_FORMATION_COUNTDOWN
				RETURN FALSE
			BREAK
			
			DEFAULT
				//IF PS_UPDATE_FAIL_CHECKS(PS_Main, FALSE, FALSE,TRUE,TRUE,TRUE,TRUE,TRUE,TRUE,TRUE)
				IF PS_UPDATE_FAIL_CHECKS(PS_Main, FALSE, FALSE, TRUE, TRUE, TRUE, FALSE) //don't check for being idle, fix for B*1877226
					RETURN TRUE
				ENDIF
				
				IF TOO_SLOW_HITTING_MARKERS_CHECK()				
					ePSFailReason = PS_FAIL_NOT_IN_FORMATON
					RETURN TRUE
				ENDIF
			BREAK
		

		
		
		/*		
			CASE PS_DLC_ENGINE_COUNTDOWN
				//No fail check to do
				RETURN FALSE
				
			CASE PS_DLC_ENGINE_CUTOUT
				//Do runway check and default checks
				IF PS_UPDATE_FAIL_CHECKS(PS_Main, FALSE, FALSE,FALSE,TRUE,TRUE,FALSE,TRUE,TRUE,TRUE)
					cprintln(debug_trevor3,"FAIL A")
					RETURN TRUE
				ENDIF
			BREAK
			
			CASE PS_DLC_Engine_on_runway
				IF PS_UPDATE_FAIL_CHECKS(PS_Main, FALSE, FALSE,FALSE,TRUE,TRUE,FALSE,TRUE,TRUE,TRUE)
					cprintln(debug_trevor3,"FAIL B")
					RETURN TRUE
				ENDIF
			BREAK
			
			DEFAULT
				//Do default checks
				IF PS_UPDATE_FAIL_CHECKS(PS_Main, FALSE, FALSE,FALSE,TRUE,TRUE,FALSE,TRUE,TRUE,TRUE)
					cprintln(debug_trevor3,"FAIL C")
					RETURN TRUE
				ENDIF
				BREAK*/
		ENDSWITCH
	//ENDIF
	//If we get here, we havent failed yet
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks for objective goals completed based on the stage/state we are in of the challenge progression
///    Also updates ChallengeState when we complete an objective 
/// PARAMS:
///    PS_Main - 
FUNC BOOL PS_DLC_Formation_Progress_Check()		

	SWITCH(PS_DLC_Formation_State)
			CASE PS_DLC_FORMATION_INIT
				RETURN FALSE
			BREAK

			CASE PS_DLC_FORMATION_CUTSCENE_INIT
				RETURN FALSE
			BREAK
				
		//Give player control and the first instruction to take off
		//Check if player is staying on the runway and not driving offroad
			CASE PS_DLC_FORMATION_CUTSCENE			
			BREAK
			
			CASE PS_DLC_FORMATION_COUNTDOWN
				
			BREAK
				
			CASE PS_DLC_FORMATION_TAKEOFF
			
			BREAK
			
		
			
			CASE PS_DLC_FORMATION_SUCCESS
			BREAK
				
		ENDSWITCH
	RETURN FALSE
ENDFUNC

PROC PS_DLC_Formation_FORCE_FAIL
	PS_TRIGGER_MUSIC(PS_MUSIC_FORM_FAIL)
	ePSFailReason = PS_FAIL_DEBUG
	PS_DLC_Formation_State = PS_DLC_FORMATION_FAIL

	PS_HUD_SET_GUTTER_ICON_ACTIVE(FALSE)
	PS_HUD_SET_OBJECTIVE_METER_VISIBLE(FALSE)
	PS_SET_TIMER_ACTIVE(FALSE)
	PS_HUD_CLEANUP()
	CLEAR_PRINTS()
	CLEAR_HELP()

//take care of objects
	//player first
	PS_SEND_PLAYER_BACK_TO_MENU()
	//then vehicle
	
	//then checkpoints
	PS_RESET_CHECKPOINT_MGR()

//loaded assets
	REMOVE_VEHICLE_RECORDING(iPS_PreviewVehicleRecordingID, "PilotSchool")
	SET_MODEL_AS_NO_LONGER_NEEDED(VehicleToUse)	
	


	PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
ENDPROC

PROC PS_DLC_FORMATION_FORCE_PASS
	cprintln(debug_trevor3,"PS_DLC_FORMATION_FORCE_PASS")
	PS_DLC_Formation_State = PS_DLC_FORMATION_SUCCESS
	PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
ENDPROC

//****************************************************************************************************
//****************************************************************************************************
//	Public functions! (used by Pilot_School.sc to run the challenges)
//****************************************************************************************************
//****************************************************************************************************

PROC PS_DLC_FORMATION_Initialise()
	SWITCH g_PS_initStage
		CASE 0

			//init vars
			
			iPS_PreviewVehicleRecordingID = iPS_PEVIEW_VEH_REC_ID_DLC_FORMATION
			vStartPosition								= <<-960.0439, -3360.9133, 12.9444>>	//<<1939.2792, 3041.4685, 280>>	//mid air countryside <<2120.3811, 4806.7539, 40.1959>>	//mckenzie airfield
			vStartRotation 								= <<0, 0.0000, 60.5613>>		//<<-7.4175, -7.1073, 65.0>>	//mid air countryside <<8.8095, -0.0036, 115.0>> 		//mckenzie airfield
			fStartHeading 								= 55.0							//65.0							//mid air countryside 115.0								//mckenzie airfield
			iPSDialogueCounter							= 1
			iPSHelpTextCounter							= 1
			iPSObjectiveTextCounter						= 1
			//iPreviewCameraProgress						= 0
			iScoreTimer = 0
			//Player vehicle model
			VehicleToUse = BESRA
			//PS_SET_GROUND_OFFSET(1.5)
			
			

			
			//load assets
			REQUEST_VEHICLE_RECORDING(iPS_PreviewVehicleRecordingID, sPreviewRecordingName)
			REQUEST_VEHICLE_RECORDING(120, sPreviewRecordingName)
			REQUEST_VEHICLE_RECORDING(121, sPreviewRecordingName)
			REQUEST_VEHICLE_RECORDING(122, sPreviewRecordingName)
			g_PS_initStage++
		BREAK
		CASE 1			
			IF HAS_VEHICLE_RECORDING_BEEN_LOADED(iPS_PreviewVehicleRecordingID, sPreviewRecordingName)
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(120, sPreviewRecordingName)
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(121, sPreviewRecordingName)
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(122, sPreviewRecordingName)
			AND PS_SETUP_CHALLENGE()
				PS_DLC_Formation_Init_Checkpoints()
				PS_DLC_Formation_State = PS_DLC_FORMATION_INIT
				
				fPreviewVehicleSkipTime = 0
				fPreviewTime = 16
				iCountdown = 0
				vPilotSchoolSceneCoords = vStartPosition
				
				FO_currentBrief = FO_BRIEF_NONE
				mostRecentBrief = FO_BRIEF_NONE
				vFlyToCoord = <<0,0,0>>
				vLastFlytoCoord = <<0,0,0>>
				
				fLastRangeCheck = 0
				iRangeWarnings = 0
				allowNextWarningTime = 0
				
				IF HAS_VEHICLE_RECORDING_BEEN_LOADED(iPS_PreviewVehicleRecordingID, sPreviewRecordingName)
					vPilotSchoolPreviewCoords = GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(iPS_PreviewVehicleRecordingID, fPreviewVehicleSkipTime, sPreviewRecordingName)
					vPilotSchoolPreviewRot = GET_ROTATION_OF_VEHICLE_RECORDING_AT_TIME(iPS_PreviewVehicleRecordingID, fPreviewVehicleSkipTime, sPreviewRecordingName)
				ENDIF
				
				cprintln(debug_trevor3,"Restart")
				//Spawn player vehicle		
				g_PS_initStage++
			ENDIF
		BREAK
		CASE 2
			IF PS_CREATE_VEHICLE()
			AND PS_CREATE_NET_VEHICLE(PS_ambVeh_NETID[0],BESRA,GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(120,12600,"pilotSchool"),0)
			AND PS_CREATE_NET_VEHICLE(PS_ambVeh_NETID[1],BESRA,GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(121,14000,"pilotSchool"),0)
			AND PS_CREATE_NET_VEHICLE(PS_ambVeh_NETID[2],BESRA,GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(122,14000,"pilotSchool"),0)
				vehBESRA[0] = NET_TO_VEH(PS_ambVeh_NETID[0])
				vehBESRA[1] = NET_TO_VEH(PS_ambVeh_NETID[1])
				vehBESRA[2] = NET_TO_VEH(PS_ambVeh_NETID[2])
				//cprintln(debug_trevor3,"B PS_ambVeh_NETID[0] = ",PS_ambVeh_NETID[0]," PS_ambVeh_NETID[1] = ",PS_ambVeh_NETID[1]," PS_ambVeh_NETID[2] = ",PS_ambVeh_NETID[2])
					
				SET_VEHICLE_ENGINE_ON(PS_Main.myVehicle,true,true)
				bwaitingOnSomething = false	
																		
//				SET_WEATHER_FOR_FMMC_MISSION(1) //sunny

				NETWORK_OVERRIDE_CLOCK_TIME(GET_HOUR_FOR_PS_CHALLENGE(),0,0)
				
				BLOCK_SCENARIOS_AND_AMBIENT(sb_PS)
				clear_area(vStartPosition,500,true)
				
				#IF IS_DEBUG_BUILD
					DEBUG_MESSAGE("******************Setting up PS_DLC_FORMATION_FAILure.sc******************")
				#ENDIF
				g_PS_initStage=0
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL PS_DLC_FORMATION_Preview()
	IF PS_PREVIEW_UPDATE_CUTSCENE()
		SWITCH(PS_PreviewState)
			CASE PS_PREVIEW_SETUP
				PS_PREVIEW_SETUP_PLAYER_FOR_CUTSCENE()
			
				IF PS_CREATE_NET_VEHICLE(PS_ambVeh_NETID[0],BESRA,GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(120,12600,"pilotSchool"),0)
				AND PS_CREATE_NET_VEHICLE(PS_ambVeh_NETID[1],BESRA,GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(121,14000,"pilotSchool"),0)
				AND PS_CREATE_NET_VEHICLE(PS_ambVeh_NETID[2],BESRA,GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(122,14000,"pilotSchool"),0)
					vehBESRA[0] = NET_TO_VEH(PS_ambVeh_NETID[0])
					vehBESRA[1] = NET_TO_VEH(PS_ambVeh_NETID[1])
					vehBESRA[2] = NET_TO_VEH(PS_ambVeh_NETID[2])
					//cprintln(debug_trevor3,"C PS_ambVeh_NETID[0] = ",PS_ambVeh_NETID[0]," PS_ambVeh_NETID[1] = ",PS_ambVeh_NETID[1]," PS_ambVeh_NETID[2] = ",PS_ambVeh_NETID[2])
				ELSE
					RETURN FALSE
				ENDIF								
			
				IF IS_VEHICLE_DRIVEABLE(vehBESRA[0])
					START_PLAYBACK_RECORDED_VEHICLE(vehBESRA[0],120,"pilotSchool")
					SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehBESRA[0], 12600)
					CONTROL_LANDING_GEAR(vehBESRA[0],LGC_RETRACT_INSTANT)
				ENDIF
				
				IF IS_VEHICLE_DRIVEABLE(vehBESRA[1])
					START_PLAYBACK_RECORDED_VEHICLE(vehBESRA[1],121,"pilotSchool")
					SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehBESRA[1], 13800)
					CONTROL_LANDING_GEAR(vehBESRA[1],LGC_RETRACT_INSTANT)
				ENDIF
				
				IF IS_VEHICLE_DRIVEABLE(vehBESRA[2])
					START_PLAYBACK_RECORDED_VEHICLE(vehBESRA[2],122,"pilotSchool")
					SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehBESRA[2], 13800)
					CONTROL_LANDING_GEAR(vehBESRA[2],LGC_RETRACT_INSTANT)
				ENDIF
		
			//	SET_VEHICLE_CONVERSATIONS_PERSIST(true, true)
				PS_PREVIEW_DIALOGUE_PLAY("PS_FOPREV", "PS_FOPREV_1", "PS_FOPREV_2", "PS_FOPREV_3")
				IF NOT IS_VEHICLE_FUCKED(PS_Main.myVehicle)
					START_PLAYBACK_RECORDED_VEHICLE(PS_Main.myVehicle, 120, "PilotSchool")
					SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(PS_Main.myVehicle, 11800)
					SET_PLAYBACK_SPEED(PS_Main.myVehicle, 1.0)								
					SET_VEHICLE_ENGINE_ON(PS_Main.myVehicle,true,true)
					CONTROL_LANDING_GEAR(PS_Main.myVehicle,LGC_RETRACT_INSTANT)				
				ENDIF
				iPreviewCameraProgress 	= 0
				iPreviewCheckpointIdx 	= PS_GET_CLOSEST_PREVIEW_CHECKPOINT(iPS_PreviewVehicleRecordingID, PS_Main.myCheckpointz, fPreviewVehicleSkipTime)
				PS_ACTIVATE_CHECKPOINT_MGR(PS_LESSON_OBSTACLE_PLANE, iPreviewCheckpointIdx, TRUE)
				
				DESTROY_ALL_CAMS()
				RENDER_SCRIPT_CAMS(FALSE, FALSE)
				
				PS_Main.previewCam1 = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
				
				SET_CAM_COORD(PS_Main.previewCam1, <<-1719.5742, -2724.6924, 272.7329>>)
				SET_CAM_FOV(PS_Main.previewCam1, 16.4)
				POINT_CAM_AT_ENTITY(PS_Main.previewCam1, PS_Main.myVehicle, << 0.0, 25.0, 0.0 >>)
				SHAKE_CAM(PS_Main.previewCam1, "HAND_SHAKE", 1.0)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				
				BREAK
			CASE PS_PREVIEW_PLAYING
			
				IF IS_VEHICLE_DRIVEABLE(PS_Main.myVehicle)
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(PS_Main.myVehicle)
					
					//	#IF IS_DEBUG_BUILD
					//		DRAW_DEBUG_VEHICLE_RECORDING_INFO(PS_Main.myVehicle, 0.0)
					//	#ENDIF
					
						FLOAT fRecordingPlaybackTime
						
						fRecordingPlaybackTime = GET_TIME_POSITION_IN_RECORDING(PS_Main.myVehicle)
						SWITCH iPreviewCameraProgress
							CASE 0
								IF fRecordingPlaybackTime >= 16000
									iPreviewCameraProgress++
								ENDIF
							BREAK
							CASE 1
							
								IF IS_VEHICLE_DRIVEABLE(vehBESRA[0])								
									SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehBESRA[0], -2000)
								ENDIF
								
								IF IS_VEHICLE_DRIVEABLE(vehBESRA[1])							
									SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehBESRA[1], -2000)
								ENDIF
								
								IF IS_VEHICLE_DRIVEABLE(vehBESRA[2])						
									SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehBESRA[2], -2000)
								ENDIf
								
								IF NOT IS_VEHICLE_FUCKED(PS_Main.myVehicle)
									SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(PS_Main.myVehicle, -2000)
								ENDIF
								iPreviewCameraProgress++
							BREAK
							CASE 2
								DESTROY_ALL_CAMS()
								RENDER_SCRIPT_CAMS(FALSE, FALSE)
								
								PS_Main.previewCam1 = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
								
								SET_CAM_COORD(PS_Main.previewCam1, <<-1942.7164, -2827.7783, 2.2664>>)
								SET_CAM_FOV(PS_Main.previewCam1, 30.0)
								POINT_CAM_AT_ENTITY(PS_Main.previewCam1, PS_Main.myVehicle, << 0.0, 8.0, 0.0 >>)
								SHAKE_CAM(PS_Main.previewCam1, "HAND_SHAKE", 1.0)
								
								
								RENDER_SCRIPT_CAMS(TRUE, FALSE)
								iPreviewCameraProgress++
							BREAK
							CASE 3
								IF fRecordingPlaybackTime >= 21000
									iPreviewCameraProgress++
								ENDIF
							BREAK
							CASE 4
							
								IF IS_VEHICLE_DRIVEABLE(vehBESRA[0])								
									SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehBESRA[0], -1000)
								ENDIF
								
								IF IS_VEHICLE_DRIVEABLE(vehBESRA[1])							
									SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehBESRA[1], -1000)
								ENDIF
								
								IF IS_VEHICLE_DRIVEABLE(vehBESRA[2])						
									SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehBESRA[2], -1000)
								ENDIf
								
								IF NOT IS_VEHICLE_FUCKED(PS_Main.myVehicle)
									SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(PS_Main.myVehicle, -1000)
								ENDIF
								iPreviewCameraProgress++
							BREAK
							CASE 5
								DESTROY_ALL_CAMS()
								RENDER_SCRIPT_CAMS(FALSE, FALSE)
								
								PS_Main.previewCam1 = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
								
								SET_CAM_COORD(PS_Main.previewCam1, <<-2381.5198, -2529.8071, 54.7750>>)
								SET_CAM_FOV(PS_Main.previewCam1, 30.0)
								SET_CAM_ROT(PS_Main.previewCam1, <<3.0530, 0.0416, -123.7229>>)
								SHAKE_CAM(PS_Main.previewCam1, "HAND_SHAKE", 1.0)
								
								
								RENDER_SCRIPT_CAMS(TRUE, FALSE)
								iPreviewCameraProgress++
							BREAK
							CASE 6
								IF fRecordingPlaybackTime >= 26000
									DESTROY_ALL_CAMS()
									RENDER_SCRIPT_CAMS(FALSE, FALSE)
									iPreviewCameraProgress++
								ENDIF
							BREAK
						ENDSWITCH
					ENDIF
				ENDIF				
				
			BREAK
			CASE PS_PREVIEW_CLEANUP
				cprintln(debug_trevor3,"RUN PREVIEW CLEANUP")
				IF NETWORK_DOES_NETWORK_ID_EXIST(PS_ambVeh_NETID[0])
					PS_DELETE_NET_ID(PS_ambVeh_NETID[0])
				ENDIF
				IF NETWORK_DOES_NETWORK_ID_EXIST(PS_ambVeh_NETID[1])
					PS_DELETE_NET_ID(PS_ambVeh_NETID[1])
				ENDIF
				IF NETWORK_DOES_NETWORK_ID_EXIST(PS_ambVeh_NETID[2])
					PS_DELETE_NET_ID(PS_ambVeh_NETID[2])
				ENDIF
				PS_PREVIEW_CLEANUP_CUTSCENE()
				
			BREAK
		ENDSWITCH
		RETURN TRUE
	ELSE
		cprintln(debug_Trevor3,"Preview cleanup B")
		IF IS_VEHICLE_DRIVEABLE(vehBESRA[0]) 
			cprintln(debug_Trevor3,"Delete 1a")
			STOP_PLAYBACK_RECORDED_VEHICLE(vehBESRA[0])
		ENDIF
		IF IS_VEHICLE_DRIVEABLE(vehBESRA[1]) 
			cprintln(debug_Trevor3,"Delete 1b")
			STOP_PLAYBACK_RECORDED_VEHICLE(vehBESRA[1])
		ENDIF
		IF IS_VEHICLE_DRIVEABLE(vehBESRA[2]) 
			cprintln(debug_Trevor3,"Delete 1c")
			STOP_PLAYBACK_RECORDED_VEHICLE(vehBESRA[2])
		ENDIF
		
		IF NETWORK_DOES_NETWORK_ID_EXIST(PS_ambVeh_NETID[0])
			PS_DELETE_NET_ID(PS_ambVeh_NETID[0])
		ENDIF
		IF NETWORK_DOES_NETWORK_ID_EXIST(PS_ambVeh_NETID[1])
			PS_DELETE_NET_ID(PS_ambVeh_NETID[1])
		ENDIF
		IF NETWORK_DOES_NETWORK_ID_EXIST(PS_ambVeh_NETID[2])
			PS_DELETE_NET_ID(PS_ambVeh_NETID[2])
		ENDIF
		
		RETURN FALSE
	ENDIF
ENDFUNC

FUNC BOOL PS_DLC_FORMATION_MainSetup()
	cprintln(debug_Trevor3,"PS_DLC_FORMATION_MainSetup")
	IF PS_CREATE_NET_VEHICLE(PS_ambVeh_NETID[0],BESRA,GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(120,12600,"pilotSchool"),0)
	AND PS_CREATE_NET_VEHICLE(PS_ambVeh_NETID[1],BESRA,GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(121,14000,"pilotSchool"),0)
	AND PS_CREATE_NET_VEHICLE(PS_ambVeh_NETID[2],BESRA,GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(122,14000,"pilotSchool"),0)
		vehBESRA[0] = NET_TO_VEH(PS_ambVeh_NETID[0])
		vehBESRA[1] = NET_TO_VEH(PS_ambVeh_NETID[1])
		vehBESRA[2] = NET_TO_VEH(PS_ambVeh_NETID[2])
		
		//cprintln(debug_trevor3,"D PS_ambVeh_NETID[0] = ",PS_ambVeh_NETID[0]," PS_ambVeh_NETID[1] = ",PS_ambVeh_NETID[1]," PS_ambVeh_NETID[2] = ",PS_ambVeh_NETID[2])
		
		IF IS_VEHICLE_DRIVEABLE(vehBESRA[0]) 
			cprintln(debug_Trevor3,"Delete 1a")
			STOP_PLAYBACK_RECORDED_VEHICLE(vehBESRA[0])
		ENDIF
		IF IS_VEHICLE_DRIVEABLE(vehBESRA[1]) 
			cprintln(debug_Trevor3,"Delete 1b")
			STOP_PLAYBACK_RECORDED_VEHICLE(vehBESRA[1])
		ENDIF
		IF IS_VEHICLE_DRIVEABLE(vehBESRA[2]) 
			cprintln(debug_Trevor3,"Delete 1c")
			STOP_PLAYBACK_RECORDED_VEHICLE(vehBESRA[2])
		ENDIF
		
		PS_DLC_Formation_Init_Checkpoints()
		PS_AUTO_PILOT(PS_Main, 0.0, FALSE)
		SET_MINIMAP_COMPONENT(MINIMAP_COMPONENT_RUNWAY_1, FALSE)	

		NEW_LOAD_SCENE_START(<<-937.9153, -3373.6482, 16.5868>>, NORMALISE_VECTOR(<<-1023.3779, -3325.6750, 15.5786>>-<<-937.9153, -3373.6482, 16.5868>>), 5000.0)
		NETWORK_OVERRIDE_CLOCK_TIME(GET_HOUR_FOR_PS_CHALLENGE(),0,0)
		IF IS_VEHICLE_DRIVEABLE(PS_Main.myVehicle)
			SET_ENTITY_HEADING(PS_Main.myVehicle,60.0)
			cprintln(debug_trevor3,"Heading Set")
		ENDIF

		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
	
ENDFUNC

#IF IS_DEBUG_BUILD
	FUNC BOOL PS_DLC_FORMATION_J_SKIP()
		/*PS_DLC_FORMATION_FAILure_CHALLENGE nextState = PS_DLC_Formation_State
		IF PS_DLC_Formation_State > PS_DLC_FORMATION_FAILure_TAKEOFF
			IF PS_DLC_Formation_State < PS_DLC_FORMATION_FAILure_LOOP_ONE_PREP
				nextState = PS_DLC_FORMATION_FAILure_LOOP_ONE_PREP			
			ELIF PS_DLC_Formation_State < PS_DLC_FORMATION_FAILure_LOOP_TWO_PREP
				nextState = PS_DLC_FORMATION_FAILure_LOOP_TWO_PREP
			ENDIF
			IF nextState != PS_DLC_Formation_State AND NOT IS_ENTITY_DEAD(PS_Main.myVehicle)
				//send plane to the takeoff checkpoint
				SET_ENTITY_COORDS(PS_Main.myVehicle, PS_GET_CHECKPOINT_FROM_ID(1))
				SET_VEHICLE_FORWARD_SPEED(PS_Main.myVehicle, 60.0)
				PS_DLC_Formation_State = nextState
				PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
				RETURN TRUE
			ENDIF	
		ENDIF*/
		RETURN FALSE
	ENDFUNC
#ENDIF	


FUNC FLOAT SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehicle_index leadVehicle, vehicle_index vehChasing, vehicle_index vehToAlter, vector vOffset, float fLeaderRecSpeed=1.0, float fMinSpeed=0.8, float fMaxSpeed=1.2)

	float fNewSpeed = 1.0
	IF IS_VEHICLE_DRIVEABLE(leadVehicle)
	AND IS_VEHICLE_DRIVEABLE(vehChasing)
	AND IS_VEHICLE_DRIVEABLE(vehToAlter)
			
				vector vChasing
				
				vector vTarget

				float fHeading
				float fLeadHeading
				float fHeadingVariance
				float fDistanceToLead
				float fOffsetToLead
				float fMultiplyer
		
			
				vChasing = GET_ENTITY_COORDS(vehChasing)
			
				vTarget = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(leadVehicle,vOffset)
				
				fLeadHeading = GET_ENTITY_HEADING(leadVehicle)
				fHeading = GET_HEADING_FROM_COORDS(vChasing,vTarget)
				fHeadingVariance = fHeading - fLeadHeading
				fDistanceToLead = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(vehChasing,vTarget)
				fOffsetToLead = COS(fHeadingVariance) * fDistanceToLead
				
				float pitch = GET_ENTITY_PITCH(leadVehicle)
				
				if pitch > 90.0 
				OR pitch < -90.0
					fOffsetToLead *= -1.0
				endif
				
				
				fMultiplyer = 1.0
				if vehChasing = vehToAlter fMultiplyer*=-1.0 endif
				
				fNewSpeed = fLeaderRecSpeed + ((-fOffsetToLead * fMultiplyer) / 100.0)
							
				if fNewSpeed < fMinSpeed fNewSpeed = fMinSpeed endif				
				if fNewSpeed > fMaxSpeed fNewSpeed = fMaxSpeed endif
				
				if ABSF(fNewSpeed-1.0) < 0.02
					fNewSpeed = 1.0
				endif
				
				SET_PLAYBACK_SPEED(vehToAlter,fNewSpeed)


	ENDIF
	RETURN fNewSpeed
ENDFUNC

float fLastDesiredSpeed
float fZOffset

PROC FORCE_PLAYER_VEH_HEIGHT(vehicle_index vehToAlter,vehicle_index vehToMatch, float offset = 0.0)

	IF IS_VEHICLE_DRIVEABLE(vehToAlter)
	AND IS_VEHICLE_DRIVEABLE(vehToMatch)

		//force player's vehicle to be correct height.
					
		vector vRot
		vector vPLayer,vLead
		float fVDist
		
		vRot = GET_ENTITY_ROTATION(vehToAlter)			
		vRot=vRot
		vLead = GET_ENTITY_COORDS(vehToMatch)
		vPlayer = GET_ENTITY_COORDS(vehToAlter)
			
		if vehToAlter = vehToMatch
			fVDist = offset-vLead.z
		else
			fVDist = vLead.z-vPlayer.z+offset
		endif
					
		vector vPlayerSpeed
		vPlayerspeed = GET_ENTITY_VELOCITY(vehToAlter)
					
		IF absi(GET_CONTROL_VALUE(player_control,INPUT_VEH_FLY_PITCH_UD) - 128) < 5
			and absf(vRot.x) < 5.0
			and absf(vRot.y) < 5.0
			and absf(fVDist) < 6.0
		and IS_ENTITY_IN_AIR(vehToAlter)
		and GET_ENTITY_SPEED(vehToAlter) > 15.0
		and GET_DISTANCE_BETWEEN_COORDS(vLead,vPlayer) < 150.0					
			
			//check if player isn't doing much to the 										
						
			float fRequiredZSpeed
						
			if absf(fVDist) < 3.0 fRequiredZSpeed = absf(fVdist) / 1.0 endif
			if absf(fVdist) >= 3.0 fRequiredZSpeed = absf(fVdist) / 1.0 endif
						
			if fVDist < 0.0 
				fRequiredZSpeed *= -1.0
			endif
						
						
			fLastDesiredSpeed = fRequiredZSpeed

			//vector vCoord
			//vCoord = GET_ENTITY_COORDS(vehToAlter)
					
			//vector vDiff = vCoord - vLastCoord
			//vLastCoord = vCoord
			//float zSpeed
			//zSpeed = vDiff.z / timestep()
											

					
						
			IF fLastDesiredSpeed > vPlayerspeed.z fZOffset += 2.5*timestep() endif
			IF fLastDesiredSpeed < vPlayerspeed.z fZOffset -= 2.5*timestep() endif
						
			fRequiredZSpeed += fZOffset
			
			IF fRequiredZSpeed - vPlayerspeed.z < -1.0 fRequiredZSpeed = vPlayerspeed.z - 1.0 endif
			IF fRequiredZSpeed - vPlayerspeed.z > 1.0 fRequiredZSpeed = vPlayerspeed.z + 1.0 endif	
				//SET_ENTITY_HAS_GRAVITY(vehToAlter,false)
						
			vPlayerspeed = <<vPlayerspeed.x,vPlayerspeed.y,fRequiredZSpeed>>
						
				//	IF fVDist < -1.0 vPlayerspeed -= <<0,0,absf(fRequiredZSpeed)>> ENDIF
				//	IF fVDist > 1.0 vPlayerspeed += <<0,0,absf(fRequiredZSpeed)>> ENDIF
	
			SET_ENTITY_VELOCITY(vehToAlter,vPlayerSpeed)
		endif		
	endif
ENDPROC


																	

proc revise_jet_playback_speeds()
	float fBlah
	
	IF IS_VEHICLE_DRIVEABLE(vehBESRA[0])
	AND IS_VEHICLE_DRIVEABLE(vehBESRA[1])
	AND IS_VEHICLE_DRIVEABLE(vehBESRA[2])						
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehBESRA[0])
			IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehBESRA[1])
				IF GET_TIME_POSITION_IN_RECORDING(vehBESRA[0]) > 700				
					IF IS_VEHICLE_DRIVEABLE(vehBESRA[1])
						START_PLAYBACK_RECORDED_VEHICLE(vehBESRA[1],121,"pilotSchool")
					ENDIF
				ENDIF
			ENDIF
			IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehBESRA[2])
				IF GET_TIME_POSITION_IN_RECORDING(vehBESRA[0]) > 1600				
					IF IS_VEHICLE_DRIVEABLE(vehBESRA[2])
						START_PLAYBACK_RECORDED_VEHICLE(vehBESRA[2],122,"pilotSchool")
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF GET_GAME_TIMER() > iCountdown
			AND iCountdown != 0
				START_PLAYBACK_RECORDED_VEHICLE(vehBESRA[0],120,"pilotSchool")			
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_VEHICLE_DRIVEABLE(vehBESRA[0])
	AND IS_VEHICLE_DRIVEABLE(vehBESRA[1])
	AND IS_VEHICLE_DRIVEABLE(vehBESRA[2])
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehBESRA[0])							
		AND IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehBESRA[1])	
		AND IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehBESRA[2])	
			//DRAW_DEBUG_VEHICLE_RECORDING_INFO(vehBESRA[0],1.0)
			

			fBlah = GET_TIME_POSITION_IN_RECORDING(vehBESRA[0])
			if fBlah < 25000				
				fRecSpeed[0] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehBESRA[0],ps_main.myVehicle,vehBESRA[0],<<0,-60,0>>)
				fRecSpeed[1] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehBESRA[0],vehBESRA[1],vehBESRA[1],<<-20,-30,0>>,fRecSpeed[0])										
				fRecSpeed[2] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehBESRA[0],vehBESRA[2],vehBESRA[2],<<20,-30,0>>,fRecSpeed[0])	
				FORCE_PLAYER_VEH_HEIGHT(ps_main.myVehicle,vehBESRA[0])
			elif fBlah < 31000				
				
				fRecSpeed[0] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehBESRA[0],ps_main.myVehicle,vehBESRA[0],<<0,-90,0>>)
				fRecSpeed[1] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehBESRA[0],vehBESRA[1],vehBESRA[1],<<0,-30,0>>,fRecSpeed[0])
				fRecSpeed[2] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehBESRA[0],vehBESRA[2],vehBESRA[2],<<0,-60,0>>,fRecSpeed[0],fRecSpeed[1]*0.9,fRecSpeed[1]*0.9)
					
				cprintln(debug_trevor3,"playback speed a ",fBlah," ",fRecSpeed[0]," ",fRecSpeed[1]," ",fRecSpeed[2])
				FORCE_PLAYER_VEH_HEIGHT(ps_main.myVehicle,vehBESRA[0])
		//	elif fBlah < 35000 			//stop planes crashing in to each other.
		//		fRecSpeed[0] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehBESRA[0],ps_main.myVehicle,vehBESRA[0],<<0,-90,0>>)
		//		fRecSpeed[1] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehBESRA[0],vehBESRA[1],vehBESRA[1],<<0,-30,0>>,fRecSpeed[0])
		//		fRecSpeed[2] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehBESRA[0],vehBESRA[2],vehBESRA[2],<<0,-60,0>>,fRecSpeed[0],fRecSpeed[1]*0.8,fRecSpeed[1]*0.8)
				
				
		//		cprintln(debug_trevor3,"playback speed b ",fBlah," ",fRecSpeed[0]," ",fRecSpeed[1]," ",fRecSpeed[2])
		//		FORCE_PLAYER_VEH_HEIGHT(ps_main.myVehicle,vehBESRA[0])
			elif fBlah < 60000 			//stop planes crashing in to each other.
				fRecSpeed[0] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehBESRA[0],ps_main.myVehicle,vehBESRA[0],<<0,-90,0>>)
				fRecSpeed[1] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehBESRA[0],vehBESRA[1],vehBESRA[1],<<0,-30,0>>,fRecSpeed[0])
				fRecSpeed[2] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehBESRA[0],vehBESRA[2],vehBESRA[2],<<0,-60,0>>,fRecSpeed[0])
				
				
				cprintln(debug_trevor3,"playback speed b ",fBlah," ",fRecSpeed[0]," ",fRecSpeed[1]," ",fRecSpeed[2])
				FORCE_PLAYER_VEH_HEIGHT(ps_main.myVehicle,vehBESRA[0])
			elif fBlah < 64000
				fRecSpeed[0] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehBESRA[0],ps_main.myVehicle,vehBESRA[0],<<0,-85,30>>)
				fRecSpeed[1] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehBESRA[0],vehBESRA[1],vehBESRA[1],<<0,-30,15>>,fRecSpeed[0])
				fRecSpeed[2] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehBESRA[0],vehBESRA[2],vehBESRA[2],<<0,-55,45>>,fRecSpeed[0])						
				FORCE_PLAYER_VEH_HEIGHT(ps_main.myVehicle,vehBESRA[0],30.0)
			elif fBlah < 84000
				fRecSpeed[0] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehBESRA[0],ps_main.myVehicle,vehBESRA[0],<<0,-25,30>>)
				fRecSpeed[1] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehBESRA[0],vehBESRA[1],vehBESRA[1],<<0,-10,15>>,fRecSpeed[0])
				fRecSpeed[2] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehBESRA[0],vehBESRA[2],vehBESRA[2],<<0,-30,45>>,fRecSpeed[0])						
				FORCE_PLAYER_VEH_HEIGHT(ps_main.myVehicle,vehBESRA[0],30.0)
			elif fBlah < 87000
				fRecSpeed[0] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehBESRA[0],ps_main.myVehicle,vehBESRA[0],<<15,-30,00>>,1.15,1.15,1.15)
				fRecSpeed[1] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehBESRA[0],vehBESRA[1],vehBESRA[1],<<-15,-30,0>>,1.15,1.15,1.15)
				fRecSpeed[2] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehBESRA[0],vehBESRA[2],vehBESRA[2],<<0,-20,0>>,1.15,1.15,1.15)
			elif fBlah < 107000
				fRecSpeed[0] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehBESRA[0],ps_main.myVehicle,vehBESRA[0],<<15,-40,0>>)
				fRecSpeed[1] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehBESRA[0],vehBESRA[1],vehBESRA[1],<<-15,-40,0>>,fRecSpeed[0])
				fRecSpeed[2] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehBESRA[0],vehBESRA[2],vehBESRA[2],<<0,-30,0>>,fRecSpeed[0])
				FORCE_PLAYER_VEH_HEIGHT(ps_main.myVehicle,vehBESRA[0])
			elif fBlah < 110000
				fRecSpeed[0] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehBESRA[0],ps_main.myVehicle,vehBESRA[0],<<15,-40,0>>,1.0,1.0,1.0)
				fRecSpeed[1] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehBESRA[0],vehBESRA[1],vehBESRA[1],<<-15,-40,0>>,0.95,0.95,0.95)
				fRecSpeed[2] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehBESRA[0],vehBESRA[2],vehBESRA[2],<<0,-30,0>>,0.95,0.95,0.95)
			elif fBlah < 115000
				fRecSpeed[0] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehBESRA[0],ps_main.myVehicle,vehBESRA[0],<<15,-40,0>>,1.0,1.0,1.0)
				fRecSpeed[1] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehBESRA[0],vehBESRA[1],vehBESRA[1],<<-15,-40,0>>,1.0,1.0,1.0)
				fRecSpeed[2] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehBESRA[0],vehBESRA[2],vehBESRA[2],<<0,-30,0>>,1.0,1.0,1.0)
			elif fBlah < 123000
				fRecSpeed[0] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehBESRA[0],ps_main.myVehicle,vehBESRA[0],<<15,-40,0>>)
				fRecSpeed[1] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehBESRA[0],vehBESRA[1],vehBESRA[1],<<-15,-40,0>>,fRecSpeed[0])
				fRecSpeed[2] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehBESRA[0],vehBESRA[2],vehBESRA[2],<<0,-30,0>>,fRecSpeed[0])
				FORCE_PLAYER_VEH_HEIGHT(ps_main.myVehicle,vehBESRA[0])
			elif fBlah < 127000
				fRecSpeed[0] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehBESRA[0],ps_main.myVehicle,vehBESRA[0],<<15,-40,0>>,1.0,1.0,1.0)
				fRecSpeed[1] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehBESRA[0],vehBESRA[1],vehBESRA[1],<<-15,-40,0>>,1.0,1.0,1.0)
				fRecSpeed[2] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehBESRA[0],vehBESRA[2],vehBESRA[2],<<0,-30,0>>,1.0,1.0,1.0)
			else
				fRecSpeed[0] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehBESRA[0],ps_main.myVehicle,vehBESRA[0],<<15,-40,0>>)
				fRecSpeed[1] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehBESRA[0],vehBESRA[1],vehBESRA[1],<<-15,-40,0>>,fRecSpeed[0])
				fRecSpeed[2] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehBESRA[0],vehBESRA[2],vehBESRA[2],<<0,-30,0>>,fRecSpeed[0])
				FORCE_PLAYER_VEH_HEIGHT(ps_main.myVehicle,vehBESRA[0])
			endif
		endif
	endif
endproc

proc lift_jet_undercarriage()
	IF IS_VEHICLE_DRIVEABLE(vehBESRA[0])
		IF GET_LANDING_GEAR_STATE(vehBESRA[0]) = LGS_LOCKED_DOWN
			IF GET_ENTITY_HEIGHT_ABOVE_GROUND(vehBESRA[0]) > 5			
				CONTROL_LANDING_GEAR(vehBESRA[0],LGC_RETRACT)
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_VEHICLE_DRIVEABLE(vehBESRA[1])		
		IF GET_LANDING_GEAR_STATE(vehBESRA[1]) = LGS_LOCKED_DOWN
			IF GET_ENTITY_HEIGHT_ABOVE_GROUND(vehBESRA[1]) > 5	
				CONTROL_LANDING_GEAR(vehBESRA[1],LGC_RETRACT)
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_VEHICLE_DRIVEABLE(vehBESRA[2])		
		IF GET_LANDING_GEAR_STATE(vehBESRA[2]) = LGS_LOCKED_DOWN
			IF GET_ENTITY_HEIGHT_ABOVE_GROUND(vehBESRA[2]) > 5	
				CONTROL_LANDING_GEAR(vehBESRA[2],LGC_RETRACT)
			ENDIF
		ENDIF
	ENDIF
endproc


FUNC BOOL PS_DLC_FORMATION_MainUpdate()			
	IF TempDebugInput() AND NOT bFinishedChallenge
		SET_WANTED_LEVEL_MULTIPLIER(0.0)
		SET_MAX_WANTED_LEVEL(0)	
		#IF IS_DEBUG_BUILD
			IF PS_CHECK_FOR_STUNT_LESSON_J_SKIP()
				IF PS_DLC_FORMATION_J_SKIP()
					RETURN TRUE //if we do a j skip, then skip an update
				ENDIF
			ENDIF
		#ENDIF
		
		PS_LESSON_UDPATE()
					
		revise_jet_playback_speeds()
		lift_jet_undercarriage()	
	
		PS_PLAY_FO_BRIEF()
		
		IF PS_DLC_Formation_State >= PS_DLC_Formation_COUNTDOWN
		AND PS_DLC_Formation_State <= PS_DLC_FORMATION_EXIT_TRAINING
			IF PS_DLC_Formation_Fail_Check()
				PS_DLC_Formation_State = PS_DLC_FORMATION_FAIL
				PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
			ENDIF
		ENDIF
		

						
		//update score
		
		bPlayerScoring = FALSE
		
		IF PS_DLC_Formation_State >= PS_DLC_FORMATION_ONE
		AND PS_DLC_Formation_State <= PS_DLC_FORMATION_EXIT_TRAINING		
			VECTOR vJetRot = GET_ENTITY_ROTATION(PS_Main.myVehicle) 
			FLOAT fJetRoll = vJetRot.y
			
			IF PS_DLC_Formation_State = PS_DLC_FORMATION_FINAL AND ((fJetRoll > 130 OR fJetRoll < -130) AND (GET_ENTITY_PITCH(PS_Main.myVehicle) < -135 OR GET_ENTITY_PITCH(PS_Main.myVehicle) > 135))
			OR PS_DLC_Formation_State != PS_DLC_FORMATION_FINAL
				IF DOES_BLIP_EXIST(thisMovingTargetBlipFormation) AND GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),GET_BLIP_COORDS(thisMovingTargetBlipFormation)) < 15.0
					bPlayerScoring = TRUE
					GET_HUD_COLOUR(HUD_COLOUR_GREEN,PS_COL_R,PS_COL_G,PS_COL_B,PS_COL_A)				
				ELSE
					PS_COL_R = PS_PARA_TARG_COL_R
					PS_COL_G = PS_PARA_TARG_COL_G
					PS_COL_B = PS_PARA_TARG_COL_B
					PS_COL_A = PS_PARA_TARG_COL_A
				ENDIF	
			ENDIF
		
			IF GET_GAME_TIMER() > iScoreTimer
				IF iScoreTimer = 0
					iScoreTimer = GET_GAME_TIMER() + 250
				ELSE
					iScoreTimer += 250
				ENDIF
				
				IF DOES_BLIP_EXIST(thisMovingTargetBlipFormation)		
					
					//cprintln(debug_trevor3,fJetRoll," ",GET_ENTITY_PITCH(PS_Main.myVehicle))
					IF PS_DLC_Formation_State = PS_DLC_FORMATION_FINAL AND ((fJetRoll > 130 OR fJetRoll < -130) AND (GET_ENTITY_PITCH(PS_Main.myVehicle) < -135 OR GET_ENTITY_PITCH(PS_Main.myVehicle) > 135))
					OR PS_DLC_Formation_State != PS_DLC_FORMATION_FINAL
					
						IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),GET_BLIP_COORDS(thisMovingTargetBlipFormation)) < 8.0											
							PS_Main.targetsDestroyed += 2			
						ELIF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),GET_BLIP_COORDS(thisMovingTargetBlipFormation)) < 15.0
							PS_Main.targetsDestroyed += 1
						ENDIF	
					ENDIF
				ENDIF
			ENDIF
			
			IF PS_Main.targetsDestroyed < PS_Challenges[g_current_selected_dlc_PilotSchool_class].BronzeDistance
				DRAW_GENERIC_SCORE(FLOOR(PS_Challenges[g_current_selected_dlc_PilotSchool_class].BronzeDistance), "PS_HUDTARG_B",-1,HUD_COLOUR_BRONZE,HUDORDER_SECONDBOTTOM)
			ELIF PS_Main.targetsDestroyed < PS_Challenges[g_current_selected_dlc_PilotSchool_class].SilverDistance
				DRAW_GENERIC_SCORE(FLOOR(PS_Challenges[g_current_selected_dlc_PilotSchool_class].SilverDistance), "PS_HUDTARG_S",-1,HUD_COLOUR_SILVER,HUDORDER_SECONDBOTTOM)
			ELSE 
				DRAW_GENERIC_SCORE(FLOOR(PS_Challenges[g_current_selected_dlc_PilotSchool_class].GoldDistance), "PS_HUDTARG_G",-1,HUD_COLOUR_GOLD,HUDORDER_SECONDBOTTOM)
			ENDIF
			
			DRAW_GENERIC_SCORE(PS_Main.targetsDestroyed, "FS_TARGS")
		ENDIF
		
		/*
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehBESRA[0])
			TEXT_LABEL_15 txtLbl
			float fBlah
			txtLbl = ""
			fBlah = GET_TIME_POSITION_IN_RECORDING(vehBESRA[0])
			txtlbl += ceil(fBlah)
			DRAW_DEBUG_TEXT_2D(txtLbl,<<0.5,0.8,0>>)
		ENDIF
		*/
		
		//PS_SHOW_CHECKPOINT(PS_Main.myCheckpointMgr.curIdx)
		SWITCH(PS_DLC_Formation_State)
		
			CASE PS_DLC_FORMATION_INIT
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": PS_DLC_FORMATION_FAILure_INIT")
				//updating autopilot to start the player out in the air
				//PS_AUTO_PILOT(PS_Main, 70.0, FALSE, vStartRotation.x, vStartRotation.y)
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER										
						IF NETWORK_DOES_NETWORK_ID_EXIST(PS_ambVeh_NETID[0])
							PS_DELETE_NET_ID(PS_ambVeh_NETID[0])
						ENDIF
						IF NETWORK_DOES_NETWORK_ID_EXIST(PS_ambVeh_NETID[1])
							PS_DELETE_NET_ID(PS_ambVeh_NETID[1])
						ENDIF
						IF NETWORK_DOES_NETWORK_ID_EXIST(PS_ambVeh_NETID[2])
							PS_DELETE_NET_ID(PS_ambVeh_NETID[2])
						ENDIF
						PS_INCREMENT_SUBSTATE()	
																
					BREAK
					CASE PS_SUBSTATE_UPDATE
						IF PS_CREATE_NET_VEHICLE(PS_ambVeh_NETID[0],BESRA,GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(120,0,"pilotSchool"),0)
						AND PS_CREATE_NET_VEHICLE(PS_ambVeh_NETID[1],BESRA,GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(121,0,"pilotSchool"),0)
						AND PS_CREATE_NET_VEHICLE(PS_ambVeh_NETID[2],BESRA,GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(122,0,"pilotSchool"),0)
							vehBESRA[0] = NET_TO_VEH(PS_ambVeh_NETID[0])
							vehBESRA[1] = NET_TO_VEH(PS_ambVeh_NETID[1])
							vehBESRA[2] = NET_TO_VEH(PS_ambVeh_NETID[2])						
							
							///cprintln(debug_trevor3,"A PS_ambVeh_NETID[0] = ",PS_ambVeh_NETID[0]," PS_ambVeh_NETID[1] = ",PS_ambVeh_NETID[1]," PS_ambVeh_NETID[2] = ",PS_ambVeh_NETID[2])
							
							IF IS_VEHICLE_DRIVEABLE(vehBESRA[0]) 
								cprintln(debug_Trevor3,"Delete 1a")
								STOP_PLAYBACK_RECORDED_VEHICLE(vehBESRA[0])
							ENDIF
							IF IS_VEHICLE_DRIVEABLE(vehBESRA[1]) 
								cprintln(debug_Trevor3,"Delete 1b")
								STOP_PLAYBACK_RECORDED_VEHICLE(vehBESRA[1])
							ENDIF
							IF IS_VEHICLE_DRIVEABLE(vehBESRA[2]) 
								cprintln(debug_Trevor3,"Delete 1c")
								STOP_PLAYBACK_RECORDED_VEHICLE(vehBESRA[2])
							ENDIF
							
							FREEZE_ENTITY_POSITION(PS_Main.myVehicle, FALSE)						
		
							SET_VEHICLE_USED_FOR_PILOT_SCHOOL(vehBESRA[0],true) 
							SET_VEHICLE_USED_FOR_PILOT_SCHOOL(vehBESRA[1],true) 
							SET_VEHICLE_USED_FOR_PILOT_SCHOOL(vehBESRA[2],true) 
							
							IF IS_VEHICLE_DRIVEABLE(PS_Main.myVehicle)
								SET_PLANE_TURBULENCE_MULTIPLIER(PS_Main.myVehicle,0.0)
								SET_VEHICLE_ENGINE_HEALTH(PS_Main.myVehicle,1000.0)
							ENDIF
							SET_PLANE_TURBULENCE_MULTIPLIER(vehBESRA[0],0.0)
							SET_PLANE_TURBULENCE_MULTIPLIER(vehBESRA[1],0.0)
							SET_PLANE_TURBULENCE_MULTIPLIER(vehBESRA[2],0.0)
							
							SET_ENTITY_ROTATION(vehBESRA[0],GET_ROTATION_OF_VEHICLE_RECORDING_AT_TIME(120,0,"pilotSchool"))
							SET_ENTITY_ROTATION(vehBESRA[1],GET_ROTATION_OF_VEHICLE_RECORDING_AT_TIME(121,0,"pilotSchool"))
							SET_ENTITY_ROTATION(vehBESRA[2],GET_ROTATION_OF_VEHICLE_RECORDING_AT_TIME(122,0,"pilotSchool"))
							PS_AUTO_PILOT(PS_Main, 0.0, FALSE)
							SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
							PS_INCREMENT_SUBSTATE()							
						ENDIF	
						
						BREAK
					CASE PS_SUBSTATE_EXIT
						PS_DLC_Formation_State = PS_DLC_Formation_COUNTDOWN
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
			BREAK

			CASE PS_DLC_Formation_COUNTDOWN
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": PS_DLC_FORMATION_FAILure_COUNTDOWN")
				//updating autopilot to start the player out in the air
		//		PS_AUTO_PILOT(PS_Main, 50.0, FALSE, vStartRotation.x, vStartRotation.y)
				PS_AUTO_PILOT(PS_Main, 0.0, FALSE)
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_INCREMENT_SUBSTATE()
						iCountdown = GET_GAME_TIMER() + 3000
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF	PS_HUD_UPDATE_COUNTDOWN(TRUE,FALSE)
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						PS_DLC_Formation_State = PS_DLC_FORMATION_TAKEOFF
						
						PS_TRIGGER_MUSIC(PS_MUSIC_FORM_START)
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
			BREAK
			
			CASE PS_DLC_FORMATION_TAKEOFF
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						
						
						
						//SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						DO_NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)	
						//SET_PLAYER_INVINCIBLE(PLAYER_ID(),FALSE)
						
						SET_VEHICLE_CONVERSATIONS_PERSIST(true, true)
					//	PS_PLAY_DISPATCHER_INSTRUCTION_3_LINES("PS_ENGFA", "PS_ENGFA_1", "PS_ENGFA", "PS_ENGFA_2", "PS_ENGFA", "PS_ENGFA_3")
						
						
						PS_HUD_RESET_TIMER()
						PS_HUD_SET_GUTTER_ICON_ACTIVE(TRUE)
						
				
						PS_TRIGGER_FO_BRIEF(FO_BRIEF_TAKEOFF)
						
						vFlyToCoord = <<-1307.8129, -3160.4521, 56.8342>>
						
						
						PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE,vFlyToCoord )
					
						
						PS_ACTIVATE_CHECKPOINT_MGR(PS_LESSON_OBSTACLE_PLANE,1)
						
						
		
						
						PS_INCREMENT_SUBSTATE()
					BREAK
					CASE PS_SUBSTATE_UPDATE
					
						
					
					//	DRAW_MARKER(MARKER_RING, PS_Main.myCheckpointz[1].position, NORMALISE_VECTOR(<<-1539.4138, -3024.9126, 30.7589>>-<<-2708.2214, -2348.9553, 76.4800>>), <<0,0,0>>, <<PS_PARA_TARG_SCL_L, PS_PARA_TARG_SCL_L, PS_PARA_TARG_SCL_L>>, PS_COL_R,PS_COL_G,PS_COL_B, 128, FALSE, FALSE)
						
						BEGIN_TEXT_COMMAND_SET_BLIP_NAME("PS_BLPMTARG")
						END_TEXT_COMMAND_SET_BLIP_NAME(thisMovingTargetBlipFormation)
						
						
						IF PS_IS_LAST_CHECKPOINT_CLEARED() //PS_GET_CHECKPOINT_PROGRESS() = 2							
							PS_DLC_Formation_State = PS_DLC_FORMATION_ONE
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ENDIF
					BREAK
				ENDSWITCH
			BREAK
			
			CASE PS_DLC_FORMATION_ONE
			
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_TRIGGER_FO_BRIEF(FO_BRIEF_FORMATION_1_START_DIA)
						
						PS_Main.targetsDestroyed = 0
						PS_INCREMENT_SUBSTATE()
					BREAK
					CASE PS_SUBSTATE_UPDATE
					
						
					
						//PS_Main.myCheckpointMgr.isFinished = false
					
						vector vLead
						vLead = GET_ENTITY_COORDS(vehBESRA[1])
						PS_Main.myCheckpointz[1].position = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehBESRA[0], <<0, -65, 0>>)
						PS_Main.myCheckpointz[1].position.z = vLead.z
						vFlyToCoord = PS_Main.myCheckpointz[1].position
						
						DRAW_MARKER(MARKER_RING, PS_Main.myCheckpointz[1].position, NORMALISE_VECTOR(GET_ENTITY_COORDS(vehBESRA[0])-PS_Main.myCheckpointz[1].position), <<0,180,0>>, <<PS_PARA_TARG_SCL_L, PS_PARA_TARG_SCL_L, PS_PARA_TARG_SCL_L>>, PS_COL_R,PS_COL_G,PS_COL_B, 128, FALSE, FALSE)
				
						
						IF DOES_BLIP_EXIST(thisMovingTargetBlipFormation)
							SET_BLIP_COORDS(thisMovingTargetBlipFormation, PS_Main.myCheckpointz[1].position)							
						ELSE
							thisMovingTargetBlipFormation = CREATE_BLIP_FOR_COORD(PS_Main.myCheckpointz[1].position)
							SET_BLIP_SCALE(thisMovingTargetBlipFormation, BLIP_SIZE_NETWORK_CHECKPOINT)							
						ENDIF
						
						
						IF PS_COL_R = PS_PARA_TARG_COL_R
							SET_BLIP_COLOUR(thisMovingTargetBlipFormation,BLIP_COLOUR_YELLOW) 
						ELSE
							SET_BLIP_COLOUR(thisMovingTargetBlipFormation,BLIP_COLOUR_GREEN) 
						ENDIF
						
					
						
						BEGIN_TEXT_COMMAND_SET_BLIP_NAME("PS_BLPMTARG")
						END_TEXT_COMMAND_SET_BLIP_NAME(thisMovingTargetBlipFormation)
						
						
						
						IF GET_TIME_POSITION_IN_RECORDING(vehBESRA[0]) >= 25000
							IF DOES_BLIP_EXIST(thisMovingTargetBlipFormation)
								REMOVE_BLIP(thisMovingTargetBlipFormation)
							ENDIF
							PS_DLC_Formation_State = PS_DLC_FORMATION_EXIT_FORMATION_ONE
							cprintln(debug_trevor3,"end formation one")
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ENDIF
						
						/*
						//if vehicle has touched down
						IF NOT IS_ENTITY_IN_AIR(PS_Main.myVehicle)
							vTouchdownCoord = GET_ENTITY_COORDS(PS_Main.myVehicle)
							PS_ACTIVATE_CHECKPOINT_MGR(PS_LESSON_NULL)
							PS_DLC_Formation_State = PS_DLC_Engine_on_runway
						
							PS_SET_SUBSTATE(PS_SUBSTATE_UPDATE)
						ENDIF		*/
					BREAK
				ENDSWITCH
			BREAK
			
			CASE PS_DLC_FORMATION_EXIT_FORMATION_ONE
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						cprintln(debug_trevor3,"Get to checkpoint")
						PS_RESET_CHECKPOINT_MGR()
						vFlyToCoord = <<-2509.8916, -2468.0698, 85.1270>>
						
						PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE,vFlyToCoord )
					
						PS_ACTIVATE_CHECKPOINT_MGR(PS_LESSON_OBSTACLE_PLANE)
						
						PS_TRIGGER_FO_BRIEF(FO_BRIEF_FORMATION_2_LINE_PREP)
						PS_INCREMENT_SUBSTATE()
					BREAK
					CASE PS_SUBSTATE_UPDATE
						IF PS_IS_LAST_CHECKPOINT_CLEARED()
							//PS_TRIGGER_FO_BRIEF(FO_BRIEF_FORMATION_2_LINE_IN_POS)
							cprintln(debug_trevor3,"target cleared at ",GET_TIME_POSITION_IN_RECORDING(vehBESRA[0]))
							//PS_HIDE_ALL_CHECKPOINTS()
							PS_DLC_Formation_State = PS_DLC_FORMATION_TRAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ENDIF
					BREAK
				ENDSWITCH
			BREAK
			
			CASE PS_DLC_FORMATION_TRAIL
				SWITCH PS_GET_SUBSTATE()
				CASE PS_SUBSTATE_ENTER
					
						PS_INCREMENT_SUBSTATE()
					BREAK
					CASE PS_SUBSTATE_UPDATE
					
						
						//PS_Main.myCheckpointMgr.isFinished = false
					
						vector vLead
						vLead = GET_ENTITY_COORDS(vehBESRA[2])
						PS_Main.myCheckpointz[1].position = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehBESRA[2], <<0, -30, 0>>)
						PS_Main.myCheckpointz[1].position.z = vLead.z
						vFlyToCoord = PS_Main.myCheckpointz[1].position
						
						DRAW_MARKER(MARKER_RING, PS_Main.myCheckpointz[1].position, NORMALISE_VECTOR(GET_ENTITY_COORDS(vehBESRA[2])-PS_Main.myCheckpointz[1].position), <<0,0,0>>, <<PS_PARA_TARG_SCL_L, PS_PARA_TARG_SCL_L, PS_PARA_TARG_SCL_L>>, PS_COL_R,PS_COL_G,PS_COL_B, 128, FALSE, FALSE)
						
						IF DOES_BLIP_EXIST(thisMovingTargetBlipFormation)
							SET_BLIP_COORDS(thisMovingTargetBlipFormation, PS_Main.myCheckpointz[1].position)
						ELSE
							thisMovingTargetBlipFormation = CREATE_BLIP_FOR_COORD(PS_Main.myCheckpointz[1].position)
							SET_BLIP_SCALE(thisMovingTargetBlipFormation, BLIP_SIZE_NETWORK_CHECKPOINT)
						ENDIF
						
						IF PS_COL_R = PS_PARA_TARG_COL_R
							SET_BLIP_COLOUR(thisMovingTargetBlipFormation,BLIP_COLOUR_YELLOW) 
						ELSE
							SET_BLIP_COLOUR(thisMovingTargetBlipFormation,BLIP_COLOUR_GREEN) 
						ENDIF
						
						BEGIN_TEXT_COMMAND_SET_BLIP_NAME("PS_BLPMTARG")
						END_TEXT_COMMAND_SET_BLIP_NAME(thisMovingTargetBlipFormation)
						
						
						
						IF GET_TIME_POSITION_IN_RECORDING(vehBESRA[0]) >= 38000
							IF DOES_BLIP_EXIST(thisMovingTargetBlipFormation)
								REMOVE_BLIP(thisMovingTargetBlipFormation)
							ENDIF
							cprintln(debug_trevor3,"End of trail")
							PS_DLC_Formation_State = PS_DLC_FORMATION_EXIT_TRAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ENDIF
						
						/*
						//if vehicle has touched down
						IF NOT IS_ENTITY_IN_AIR(PS_Main.myVehicle)
							vTouchdownCoord = GET_ENTITY_COORDS(PS_Main.myVehicle)
							PS_ACTIVATE_CHECKPOINT_MGR(PS_LESSON_NULL)
							PS_DLC_Formation_State = PS_DLC_Engine_on_runway
						
							PS_SET_SUBSTATE(PS_SUBSTATE_UPDATE)
						ENDIF		*/
					BREAK
				ENDSWITCH
			BREAK
			
			CASE PS_DLC_FORMATION_EXIT_TRAIL
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_RESET_CHECKPOINT_MGR()
						vFlyToCoord = <<-3359.8867, -1977.1183, 85.3808>>
						
						PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, vFlyToCoord)
					
						PS_ACTIVATE_CHECKPOINT_MGR(PS_LESSON_OBSTACLE_PLANE)
						PS_TRIGGER_FO_BRIEF(FO_BRIEF_FORMATION_3_TURN_PREP)
						
						PS_INCREMENT_SUBSTATE()
					BREAK
					CASE PS_SUBSTATE_UPDATE
						IF PS_IS_LAST_CHECKPOINT_CLEARED()					
							cprintln(debug_trevor3,"target cleared at ",GET_TIME_POSITION_IN_RECORDING(vehBESRA[0]))
							PS_DLC_Formation_State = PS_DLC_FORMATION_TURN
							PS_TRIGGER_FO_BRIEF(FO_BRIEF_FORMATION_3_TURN_PERFORM)
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ENDIF
					BREAK
				ENDSWITCH
			BREAK
			
			CASE PS_DLC_FORMATION_TURN
				SWITCH PS_GET_SUBSTATE()
				CASE PS_SUBSTATE_ENTER						
						PS_INCREMENT_SUBSTATE()
					BREAK
					CASE PS_SUBSTATE_UPDATE
					
					
					
						//PS_Main.myCheckpointMgr.isFinished = false
					
						vector vLead
						vLead = GET_ENTITY_COORDS(vehBESRA[2])
						PS_Main.myCheckpointz[1].position = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehBESRA[2], <<0, -30, 0>>)
						PS_Main.myCheckpointz[1].position.z = vLead.z
						vFlyToCoord = PS_Main.myCheckpointz[1].position
						
						DRAW_MARKER(MARKER_RING, PS_Main.myCheckpointz[1].position, NORMALISE_VECTOR(GET_ENTITY_COORDS(vehBESRA[2])-PS_Main.myCheckpointz[1].position), <<0,0,0>>, <<PS_PARA_TARG_SCL_L, PS_PARA_TARG_SCL_L, PS_PARA_TARG_SCL_L>>, PS_COL_R,PS_COL_G,PS_COL_B, 128, FALSE, FALSE)
						
						IF DOES_BLIP_EXIST(thisMovingTargetBlipFormation)
							SET_BLIP_COORDS(thisMovingTargetBlipFormation, PS_Main.myCheckpointz[1].position)
						ELSE
							thisMovingTargetBlipFormation = CREATE_BLIP_FOR_COORD(PS_Main.myCheckpointz[1].position)
							SET_BLIP_SCALE(thisMovingTargetBlipFormation, BLIP_SIZE_NETWORK_CHECKPOINT)
						ENDIF
						
						IF PS_COL_R = PS_PARA_TARG_COL_R
							SET_BLIP_COLOUR(thisMovingTargetBlipFormation,BLIP_COLOUR_YELLOW) 
						ELSE
							SET_BLIP_COLOUR(thisMovingTargetBlipFormation,BLIP_COLOUR_GREEN) 
						ENDIF
						
						BEGIN_TEXT_COMMAND_SET_BLIP_NAME("PS_BLPMTARG")
						END_TEXT_COMMAND_SET_BLIP_NAME(thisMovingTargetBlipFormation)
						
						
						
						IF GET_TIME_POSITION_IN_RECORDING(vehBESRA[0]) >= 60000
							IF DOES_BLIP_EXIST(thisMovingTargetBlipFormation)
								REMOVE_BLIP(thisMovingTargetBlipFormation)
							ENDIF
							PS_DLC_Formation_State = PS_DLC_FORMATION_PREP_STACK
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
							PS_TRIGGER_FO_BRIEF(FO_BRIEF_FORMATION_4_vertical_prep)
						ENDIF
						
						/*
						//if vehicle has touched down
						IF NOT IS_ENTITY_IN_AIR(PS_Main.myVehicle)
							vTouchdownCoord = GET_ENTITY_COORDS(PS_Main.myVehicle)
							PS_ACTIVATE_CHECKPOINT_MGR(PS_LESSON_NULL)
							PS_DLC_Formation_State = PS_DLC_Engine_on_runway
						
							PS_SET_SUBSTATE(PS_SUBSTATE_UPDATE)
						ENDIF		*/
					BREAK
				ENDSWITCH
			BREAK
			
			CASE PS_DLC_FORMATION_PREP_STACK
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_RESET_CHECKPOINT_MGR()
						vFlyToCoord = <<-2735.7551, -1201.5635, 102.3877>>
						
						PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, vFlyToCoord)
					
						PS_ACTIVATE_CHECKPOINT_MGR(PS_LESSON_OBSTACLE_PLANE)

						PS_INCREMENT_SUBSTATE()
					BREAK
					CASE PS_SUBSTATE_UPDATE
						IF PS_IS_LAST_CHECKPOINT_CLEARED()	
							cprintln(debug_trevor3,"target cleared at ",GET_TIME_POSITION_IN_RECORDING(vehBESRA[0]))
							PS_TRIGGER_FO_BRIEF(FO_BRIEF_FORMATION_4_vertical_perform)
							PS_DLC_Formation_State = PS_DLC_FORMATION_STACK
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ENDIF
					BREAK
				ENDSWITCH
			BREAK

			CASE PS_DLC_FORMATION_STACK
				SWITCH PS_GET_SUBSTATE()
				CASE PS_SUBSTATE_ENTER

						PS_INCREMENT_SUBSTATE()
					BREAK
					CASE PS_SUBSTATE_UPDATE
					
					
			
						//PS_Main.myCheckpointMgr.isFinished = false
					
				
						PS_Main.myCheckpointz[1].position = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehBESRA[1], <<0, -15, 15>>)
						//PS_Main.myCheckpointz[1].position.z = vLead.z
						vFlyToCoord = PS_Main.myCheckpointz[1].position
						
						DRAW_MARKER(MARKER_RING, PS_Main.myCheckpointz[1].position, NORMALISE_VECTOR(GET_ENTITY_COORDS(vehBESRA[1])+<<0,0,15>>-PS_Main.myCheckpointz[1].position), <<0,0,0>>, <<PS_PARA_TARG_SCL_L, PS_PARA_TARG_SCL_L, PS_PARA_TARG_SCL_L>>, PS_COL_R,PS_COL_G,PS_COL_B, 128, FALSE, FALSE)
						
						IF DOES_BLIP_EXIST(thisMovingTargetBlipFormation)
							SET_BLIP_COORDS(thisMovingTargetBlipFormation, PS_Main.myCheckpointz[1].position)
						ELSE
							thisMovingTargetBlipFormation = CREATE_BLIP_FOR_COORD(PS_Main.myCheckpointz[1].position)
							SET_BLIP_SCALE(thisMovingTargetBlipFormation, BLIP_SIZE_NETWORK_CHECKPOINT)
						ENDIF
						
						IF PS_COL_R = PS_PARA_TARG_COL_R
							SET_BLIP_COLOUR(thisMovingTargetBlipFormation,BLIP_COLOUR_YELLOW) 
						ELSE
							SET_BLIP_COLOUR(thisMovingTargetBlipFormation,BLIP_COLOUR_GREEN) 
						ENDIF
						
						BEGIN_TEXT_COMMAND_SET_BLIP_NAME("PS_BLPMTARG")
						END_TEXT_COMMAND_SET_BLIP_NAME(thisMovingTargetBlipFormation)
						
						
						
						IF GET_TIME_POSITION_IN_RECORDING(vehBESRA[0]) >= 84000
							IF DOES_BLIP_EXIST(thisMovingTargetBlipFormation)
								REMOVE_BLIP(thisMovingTargetBlipFormation)
							ENDIF
							PS_TRIGGER_FO_BRIEF(FO_BRIEF_FORMATION_5_vertical_prep)
							PS_DLC_Formation_State = PS_DLC_FORMATION_PREP_BOX
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ENDIF
						
						/*
						//if vehicle has touched down
						IF NOT IS_ENTITY_IN_AIR(PS_Main.myVehicle)
							vTouchdownCoord = GET_ENTITY_COORDS(PS_Main.myVehicle)
							PS_ACTIVATE_CHECKPOINT_MGR(PS_LESSON_NULL)
							PS_DLC_Formation_State = PS_DLC_Engine_on_runway
						
							PS_SET_SUBSTATE(PS_SUBSTATE_UPDATE)
						ENDIF		*/
					BREAK
				ENDSWITCH
			BREAK
			
			CASE PS_DLC_FORMATION_PREP_BOX
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_RESET_CHECKPOINT_MGR()
						vFlyToCoord = <<-1189.0587, -1215.0637, 106.6409>>
						
						PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, vFlyToCoord)
					
						PS_ACTIVATE_CHECKPOINT_MGR(PS_LESSON_OBSTACLE_PLANE)
						PS_TRIGGER_FO_BRIEF(FO_BRIEF_FORMATION_5_vertical_perform)
						PS_INCREMENT_SUBSTATE()
					BREAK
					CASE PS_SUBSTATE_UPDATE
						IF PS_IS_LAST_CHECKPOINT_CLEARED()	
							
							cprintln(debug_trevor3,"target cleared at ",GET_TIME_POSITION_IN_RECORDING(vehBESRA[0]))
						
							PS_DLC_Formation_State = PS_DLC_FORMATION_BOX
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ENDIF
					BREAK
				ENDSWITCH
			BREAK
			
			CASE PS_DLC_FORMATION_BOX
				SWITCH PS_GET_SUBSTATE()
				CASE PS_SUBSTATE_ENTER

						PS_INCREMENT_SUBSTATE()
					BREAK
					CASE PS_SUBSTATE_UPDATE
					
					
			
						//PS_Main.myCheckpointMgr.isFinished = false
					
				
						PS_Main.myCheckpointz[1].position = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehBESRA[0], <<15, -40, 0>>)
						//PS_Main.myCheckpointz[1].position.z = vLead.z
						vFlyToCoord = PS_Main.myCheckpointz[1].position
						
						DRAW_MARKER(MARKER_RING, PS_Main.myCheckpointz[1].position,<<1,0,0>>, <<0,0,0>>, <<PS_PARA_TARG_SCL_L, PS_PARA_TARG_SCL_L, PS_PARA_TARG_SCL_L>>, PS_COL_R,PS_COL_G,PS_COL_B, 128, FALSE, FALSE)
						
						IF DOES_BLIP_EXIST(thisMovingTargetBlipFormation)
							SET_BLIP_COORDS(thisMovingTargetBlipFormation, PS_Main.myCheckpointz[1].position)
						ELSE
							thisMovingTargetBlipFormation = CREATE_BLIP_FOR_COORD(PS_Main.myCheckpointz[1].position)
							SET_BLIP_SCALE(thisMovingTargetBlipFormation, BLIP_SIZE_NETWORK_CHECKPOINT)
						ENDIF
						
						IF PS_COL_R = PS_PARA_TARG_COL_R
							SET_BLIP_COLOUR(thisMovingTargetBlipFormation,BLIP_COLOUR_YELLOW) 
						ELSE
							SET_BLIP_COLOUR(thisMovingTargetBlipFormation,BLIP_COLOUR_GREEN) 
						ENDIF
						
						BEGIN_TEXT_COMMAND_SET_BLIP_NAME("PS_BLPMTARG")
						END_TEXT_COMMAND_SET_BLIP_NAME(thisMovingTargetBlipFormation)
						
						
						
						IF GET_TIME_POSITION_IN_RECORDING(vehBESRA[0]) >= 102000
							IF DOES_BLIP_EXIST(thisMovingTargetBlipFormation)
								REMOVE_BLIP(thisMovingTargetBlipFormation)
							ENDIF
							PS_TRIGGER_FO_BRIEF(FO_BRIEF_FORMATION_6_loop_prep)
							PS_DLC_Formation_State = PS_DLC_FORMATION_PREP_LOOP
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ENDIF
						

					BREAK
				ENDSWITCH
			BREAK
			
			CASE PS_DLC_FORMATION_PREP_LOOP
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_RESET_CHECKPOINT_MGR()
						vFlyToCoord = <<234.7791, -1230.0016, 85.1393>>
						
						PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, vFlyToCoord)
					
						PS_ACTIVATE_CHECKPOINT_MGR(PS_LESSON_OBSTACLE_PLANE)

						PS_INCREMENT_SUBSTATE()
					BREAK
					CASE PS_SUBSTATE_UPDATE
						IF PS_IS_LAST_CHECKPOINT_CLEARED()		
							cprintln(debug_trevor3,"target cleared at ",GET_TIME_POSITION_IN_RECORDING(vehBESRA[0]))
							PS_DLC_Formation_State = PS_DLC_FORMATION_LOOP
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
							PS_TRIGGER_FO_BRIEF(FO_BRIEF_FORMATION_6_loop_perform)
						ENDIF
					BREAK
				ENDSWITCH
			BREAK
			
			CASE PS_DLC_FORMATION_LOOP
				SWITCH PS_GET_SUBSTATE()
				CASE PS_SUBSTATE_ENTER

						PS_INCREMENT_SUBSTATE()
					BREAK
					CASE PS_SUBSTATE_UPDATE
					
					
			
						//PS_Main.myCheckpointMgr.isFinished = false
						
				
						PS_Main.myCheckpointz[1].position = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehBESRA[0], <<15, -45, 13>>)
						//PS_Main.myCheckpointz[1].position.z = vLead.z
						vFlyToCoord = PS_Main.myCheckpointz[1].position
						
						DRAW_MARKER(MARKER_RING, PS_Main.myCheckpointz[1].position,NORMALISE_VECTOR(GET_ENTITY_COORDS(vehBESRA[0])+<<0,-15,0>>-PS_Main.myCheckpointz[1].position), <<0,0,0>>, <<PS_PARA_TARG_SCL_L, PS_PARA_TARG_SCL_L, PS_PARA_TARG_SCL_L>>, PS_COL_R,PS_COL_G,PS_COL_B, 128, FALSE, FALSE)
						
						IF DOES_BLIP_EXIST(thisMovingTargetBlipFormation)
							SET_BLIP_COORDS(thisMovingTargetBlipFormation, PS_Main.myCheckpointz[1].position)
						ELSE
							thisMovingTargetBlipFormation = CREATE_BLIP_FOR_COORD(PS_Main.myCheckpointz[1].position)
							SET_BLIP_SCALE(thisMovingTargetBlipFormation, BLIP_SIZE_NETWORK_CHECKPOINT)
						ENDIF
						IF PS_COL_R = PS_PARA_TARG_COL_R
							SET_BLIP_COLOUR(thisMovingTargetBlipFormation,BLIP_COLOUR_YELLOW) 
						ELSE
							SET_BLIP_COLOUR(thisMovingTargetBlipFormation,BLIP_COLOUR_GREEN) 
						ENDIF
						
						
						BEGIN_TEXT_COMMAND_SET_BLIP_NAME("PS_BLPMTARG")
						END_TEXT_COMMAND_SET_BLIP_NAME(thisMovingTargetBlipFormation)
						
						
						
						IF GET_TIME_POSITION_IN_RECORDING(vehBESRA[0]) >= 113400
							PS_TRIGGER_FO_BRIEF(FO_BRIEF_FORMATION_6_loop_complete)
							IF DOES_BLIP_EXIST(thisMovingTargetBlipFormation)
								REMOVE_BLIP(thisMovingTargetBlipFormation)
							ENDIF
							PS_DLC_Formation_State = PS_DLC_FORMATION_AFTER_LOOP
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ENDIF
						

					BREAK
				ENDSWITCH
			BREAK
			
			CASE PS_DLC_FORMATION_AFTER_LOOP
				SWITCH PS_GET_SUBSTATE()
				CASE PS_SUBSTATE_ENTER

						PS_INCREMENT_SUBSTATE()
					BREAK
					CASE PS_SUBSTATE_UPDATE
					
					
			
						//PS_Main.myCheckpointMgr.isFinished = false
					
				
						PS_Main.myCheckpointz[1].position = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehBESRA[0], <<15, -40, 0>>)
						//PS_Main.myCheckpointz[1].position.z = vLead.z
						vFlyToCoord = PS_Main.myCheckpointz[1].position
						
						DRAW_MARKER(MARKER_RING, PS_Main.myCheckpointz[1].position,<<1,0,0>>, <<0,0,0>>, <<PS_PARA_TARG_SCL_L, PS_PARA_TARG_SCL_L, PS_PARA_TARG_SCL_L>>, PS_COL_R,PS_COL_G,PS_COL_B, 128, FALSE, FALSE)
						
						IF DOES_BLIP_EXIST(thisMovingTargetBlipFormation)
							SET_BLIP_COORDS(thisMovingTargetBlipFormation, PS_Main.myCheckpointz[1].position)
						ELSE
							thisMovingTargetBlipFormation = CREATE_BLIP_FOR_COORD(PS_Main.myCheckpointz[1].position)
							SET_BLIP_SCALE(thisMovingTargetBlipFormation, BLIP_SIZE_NETWORK_CHECKPOINT)
						ENDIF
						
						IF PS_COL_R = PS_PARA_TARG_COL_R
							SET_BLIP_COLOUR(thisMovingTargetBlipFormation,BLIP_COLOUR_YELLOW) 
						ELSE
							SET_BLIP_COLOUR(thisMovingTargetBlipFormation,BLIP_COLOUR_GREEN) 
						ENDIF
						
						BEGIN_TEXT_COMMAND_SET_BLIP_NAME("PS_BLPMTARG")
						END_TEXT_COMMAND_SET_BLIP_NAME(thisMovingTargetBlipFormation)
						
						
						
						IF GET_TIME_POSITION_IN_RECORDING(vehBESRA[0]) >= 119000
							PS_TRIGGER_FO_BRIEF(FO_BRIEF_FORMATION_7_halfloop_prep)
							IF DOES_BLIP_EXIST(thisMovingTargetBlipFormation)
								REMOVE_BLIP(thisMovingTargetBlipFormation)
							ENDIF
							PS_DLC_Formation_State = PS_DLC_FORMATION_PREP_HALF_LOOP
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ENDIF
						

					BREAK
				ENDSWITCH
			BREAK
			
			CASE PS_DLC_FORMATION_PREP_HALF_LOOP
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_RESET_CHECKPOINT_MGR()
						vFlyToCoord = <<930.7791, -1232.3419, 60.8482>>
						
						PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, vFlyToCoord)
					
						PS_ACTIVATE_CHECKPOINT_MGR(PS_LESSON_OBSTACLE_PLANE)

						PS_INCREMENT_SUBSTATE()
					BREAK
					CASE PS_SUBSTATE_UPDATE
						IF PS_IS_LAST_CHECKPOINT_CLEARED()		
							PS_TRIGGER_FO_BRIEF(FO_BRIEF_FORMATION_7_halfloop_perf)							
							cprintln(debug_trevor3,"target cleared at ",GET_TIME_POSITION_IN_RECORDING(vehBESRA[0]))
							PS_DLC_Formation_State = PS_DLC_FORMATION_HALF_LOOP
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ENDIF
					BREAK
				ENDSWITCH
			BREAK
			
			CASE PS_DLC_FORMATION_HALF_LOOP
				SWITCH PS_GET_SUBSTATE()
				CASE PS_SUBSTATE_ENTER

						PS_INCREMENT_SUBSTATE()
					BREAK
					CASE PS_SUBSTATE_UPDATE
					
					
			
						//PS_Main.myCheckpointMgr.isFinished = false
					
				
						PS_Main.myCheckpointz[1].position = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehBESRA[0], <<15, -40, 17>>)
						//PS_Main.myCheckpointz[1].position.z = vLead.z
						vFlyToCoord = PS_Main.myCheckpointz[1].position
						
						DRAW_MARKER(MARKER_RING, PS_Main.myCheckpointz[1].position,NORMALISE_VECTOR(GET_ENTITY_COORDS(vehBESRA[0])+<<0,-15,0>>-PS_Main.myCheckpointz[1].position), <<0,0,0>>, <<PS_PARA_TARG_SCL_L, PS_PARA_TARG_SCL_L, PS_PARA_TARG_SCL_L>>, PS_COL_R,PS_COL_G,PS_COL_B, 128, FALSE, FALSE)
						
						IF DOES_BLIP_EXIST(thisMovingTargetBlipFormation)
							SET_BLIP_COORDS(thisMovingTargetBlipFormation, PS_Main.myCheckpointz[1].position)
						ELSE
							thisMovingTargetBlipFormation = CREATE_BLIP_FOR_COORD(PS_Main.myCheckpointz[1].position)
							SET_BLIP_SCALE(thisMovingTargetBlipFormation, BLIP_SIZE_NETWORK_CHECKPOINT)
						ENDIF
						
						IF PS_COL_R = PS_PARA_TARG_COL_R
							SET_BLIP_COLOUR(thisMovingTargetBlipFormation,BLIP_COLOUR_YELLOW) 
						ELSE
							SET_BLIP_COLOUR(thisMovingTargetBlipFormation,BLIP_COLOUR_GREEN) 
						ENDIF
						
						BEGIN_TEXT_COMMAND_SET_BLIP_NAME("PS_BLPMTARG")
						END_TEXT_COMMAND_SET_BLIP_NAME(thisMovingTargetBlipFormation)
						
						
						
						IF GET_TIME_POSITION_IN_RECORDING(vehBESRA[0]) >= 127000
							IF DOES_BLIP_EXIST(thisMovingTargetBlipFormation)
								REMOVE_BLIP(thisMovingTargetBlipFormation)
							ENDIF
							PS_DLC_Formation_State = PS_DLC_FORMATION_FINAL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ENDIF
						

					BREAK
				ENDSWITCH
			BREAK
			
			CASE PS_DLC_FORMATION_FINAL
				SWITCH PS_GET_SUBSTATE()
				CASE PS_SUBSTATE_ENTER
						PS_TRIGGER_FO_BRIEF(FO_BRIEF_FORMATION_7_halfloop_end)
						PS_INCREMENT_SUBSTATE()
					BREAK
					CASE PS_SUBSTATE_UPDATE
					
					
			
						//PS_Main.myCheckpointMgr.isFinished = false
					
				
						PS_Main.myCheckpointz[1].position = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehBESRA[0], <<15, -40, 0>>)
						//PS_Main.myCheckpointz[1].position.z = vLead.z
						//DRAW_MARKER(MARKER_RING, PS_Main.myCheckpointz[1].position,<<1,0,0>>, <<0,0,0>>, <<PS_PARA_TARG_SCL_L, PS_PARA_TARG_SCL_L, PS_PARA_TARG_SCL_L>>, PS_COL_R,PS_COL_G,PS_COL_B, 128, FALSE, FALSE)
						
						//int invRed,invBlue,invGreen,invAlpha
						//GET_RGB_FROM_INT(GET_INT_FROM_HUD_COLOUR(HUD_COLOUR_BLUE),invRed,invBlue,invGreen,invAlpha)
				
						vFlyToCoord = PS_Main.myCheckpointz[1].position
						
						IF ( bPlayerScoring = TRUE )
							GET_HUD_COLOUR(HUD_COLOUR_GREEN,PS_COL_R,PS_COL_G,PS_COL_B,PS_COL_A)
						ELSE
							GET_HUD_COLOUR(HUD_COLOUR_BLUE,PS_COL_R,PS_COL_G,PS_COL_B,PS_COL_A)
						ENDIF
						
				
						DELETE_CHECKPOINT(thisCheckpoint)
						thisCheckpoint = CREATE_CHECKPOINT(CHECKPOINT_PLANE_INVERTED,PS_Main.myCheckpointz[1].position,NORMALISE_VECTOR(GET_ENTITY_COORDS(vehBESRA[0])-PS_Main.myCheckpointz[1].position),PS_PARA_TARG_SCL_L,PS_COL_R,PS_COL_G,PS_COL_B,PS_COL_A)
						
						IF DOES_BLIP_EXIST(thisMovingTargetBlipFormation)
							SET_BLIP_COORDS(thisMovingTargetBlipFormation, PS_Main.myCheckpointz[1].position)
						ELSE
							thisMovingTargetBlipFormation = CREATE_BLIP_FOR_COORD(PS_Main.myCheckpointz[1].position)
							SET_BLIP_SCALE(thisMovingTargetBlipFormation, BLIP_SIZE_NETWORK_CHECKPOINT)
						ENDIF
						
						BEGIN_TEXT_COMMAND_SET_BLIP_NAME("PS_BLPMTARG")
						END_TEXT_COMMAND_SET_BLIP_NAME(thisMovingTargetBlipFormation)
						
						IF ( bPlayerScoring = FALSE )
							SET_BLIP_COLOUR(thisMovingTargetBlipFormation,BLIP_COLOUR_BLUE)
						ELSE
							SET_BLIP_COLOUR(thisMovingTargetBlipFormation,BLIP_COLOUR_GREEN)
						ENDIF
						
						PRINT_HELP("PS_FRMN_GODA")
						
						IF GET_TIME_POSITION_IN_RECORDING(vehBESRA[0]) >= 138000
							IF DOES_BLIP_EXIST(thisMovingTargetBlipFormation)
								REMOVE_BLIP(thisMovingTargetBlipFormation)
							ENDIF
							DELETE_CHECKPOINT(thisCheckpoint)
							PS_INCREMENT_SUBSTATE()
						ENDIF
						

					BREAK
					CASE PS_SUBSTATE_EXIT
						PS_UPDATE_RECORDS(VECTOR_ZERO)
						IF Pilot_School_Data_Check_Mission_Success(PS_Main.myChallengeData, PS_Main.myPlayerData)
							PS_DLC_Formation_State = PS_DLC_FORMATION_SUCCESS
						ELSE
							PS_DLC_Formation_State = PS_DLC_FORMATION_FAIL
						ENDIF
						PS_INCREMENT_SUBSTATE()
					BREAK
				ENDSWITCH
			BREAK						
			
			CASE PS_DLC_FORMATION_PREP_FORMATION_TWO
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_RESET_CHECKPOINT_MGR()
						vFlyToCoord = <<-1769.7228, -2892.9221, 32.3576>>
						
						//PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<-2708.2214, -2348.9553, 76.4800>>)
						PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, vFlyToCoord)
						PS_ACTIVATE_CHECKPOINT_MGR(PS_LESSON_OBSTACLE_PLANE)
						//PS_Main.myCheckpointMgr = storeManagerState
						//PS_Main.myCheckpointz[1].position =  <<-2708.2214, -2348.9553, 76.4800>>
						//PS_Main.myCheckpointz[1].type = PS_CHECKPOINT_OBSTACLE_PLANE
						//PS_SHOW_CHECKPOINT(1)
						//PS_Main.myCheckpointMgr.isFinished=false
					//	PS_Main.myCheckpointMgr.totalCheckpoints++
					//	PS_ACTIVATE_CHECKPOINT_MGR(PS_LESSON_STUNT,0)
					//	PS_INCREMENT_CHECKPOINT()
						PS_INCREMENT_SUBSTATE()
					BREAK
					CASE PS_SUBSTATE_UPDATE
						IF PS_IS_LAST_CHECKPOINT_CLEARED()
							//PS_HIDE_ALL_CHECKPOINTS()
							PS_DLC_Formation_State = PS_DLC_FORMATION_ENTER_FORMATION_TWO
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ENDIF
					BREAK
				ENDSWITCH
			BREAK
			
			CASE PS_DLC_FORMATION_ENTER_FORMATION_TWO
			
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						
												
						PS_Main.myCheckpointz[1].position = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehBESRA[0], <<0, -90, 0>>)
						PS_Main.myCheckpointz[1].type = PS_CHECKPOINT_OBSTACLE_PLANE
				
						
						PS_INCREMENT_SUBSTATE()
					BREAK
					CASE PS_SUBSTATE_UPDATE
					
					
					
						vector vLead
						vLead = GET_ENTITY_COORDS(vehBESRA[0])
						PS_Main.myCheckpointz[1].position = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehBESRA[0], <<0, -20, -15>>)
						PS_Main.myCheckpointz[1].position.z = vLead.z - 15.0
						vFlyToCoord = PS_Main.myCheckpointz[1].position
						
						DRAW_MARKER(MARKER_RING, PS_Main.myCheckpointz[1].position, NORMALISE_VECTOR((GET_ENTITY_COORDS(vehBESRA[0])+<<0,0,-15>>)-PS_Main.myCheckpointz[1].position), <<0,0,0>>, <<PS_PARA_TARG_SCL_L, PS_PARA_TARG_SCL_L, PS_PARA_TARG_SCL_L>>, PS_COL_R,PS_COL_G,PS_COL_B, 128, FALSE, FALSE)
						
						IF DOES_BLIP_EXIST(thisMovingTargetBlipFormation)
							SET_BLIP_COORDS(thisMovingTargetBlipFormation, PS_Main.myCheckpointz[1].position)
						ELSE
							thisMovingTargetBlipFormation = CREATE_BLIP_FOR_COORD(PS_Main.myCheckpointz[1].position)
							SET_BLIP_SCALE(thisMovingTargetBlipFormation, BLIP_SIZE_NETWORK_CHECKPOINT)
						ENDIF
						
						IF PS_COL_R = PS_PARA_TARG_COL_R
							SET_BLIP_COLOUR(thisMovingTargetBlipFormation,BLIP_COLOUR_YELLOW) 
						ELSE
							SET_BLIP_COLOUR(thisMovingTargetBlipFormation,BLIP_COLOUR_GREEN) 
						ENDIF
						
						BEGIN_TEXT_COMMAND_SET_BLIP_NAME("PS_BLPMTARG")
						END_TEXT_COMMAND_SET_BLIP_NAME(thisMovingTargetBlipFormation)
						
						
						
						IF GET_TIME_POSITION_IN_RECORDING(vehBESRA[0]) >= 68000
							IF DOES_BLIP_EXIST(thisMovingTargetBlipFormation)
								REMOVE_BLIP(thisMovingTargetBlipFormation)
							ENDIF
							PS_RESET_CHECKPOINT_MGR()
							PS_DLC_Formation_State = PS_DLC_FORMATION_EXIT_FORMATION_TWO
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ENDIF
						
						/*
						//if vehicle has touched down
						IF NOT IS_ENTITY_IN_AIR(PS_Main.myVehicle)
							vTouchdownCoord = GET_ENTITY_COORDS(PS_Main.myVehicle)
							PS_ACTIVATE_CHECKPOINT_MGR(PS_LESSON_NULL)
							PS_DLC_Formation_State = PS_DLC_Engine_on_runway
						
							PS_SET_SUBSTATE(PS_SUBSTATE_UPDATE)
						ENDIF		*/																
					BREAK
				ENDSWITCH
			BREAK
			
			CASE PS_DLC_FORMATION_EXIT_FORMATION_TWO
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						PS_RESET_CHECKPOINT_MGR()
						vFlyToCoord = <<-462.0441, -3652.6384, 45>>
						
						PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_OBSTACLE_PLANE, <<-462.0441, -3652.6384, 45>>)
						PS_ACTIVATE_CHECKPOINT_MGR(PS_LESSON_OBSTACLE_PLANE,1)
			
						PS_INCREMENT_SUBSTATE()
					BREAK
					CASE PS_SUBSTATE_UPDATE
						IF PS_IS_LAST_CHECKPOINT_CLEARED()
					
							PS_DLC_Formation_State = PS_DLC_FORMATION_ENTER_FORMATION_THREE
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ENDIF
					BREAK
				ENDSWITCH
			BREAK
		
			CASE PS_DLC_FORMATION_ENTER_FORMATION_THREE
			
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						
												
						PS_Main.myCheckpointz[1].position = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehBESRA[0], <<0, -90, 0>>)
						PS_Main.myCheckpointz[1].type = PS_CHECKPOINT_OBSTACLE_PLANE
			
						
						PS_INCREMENT_SUBSTATE()
					BREAK
					CASE PS_SUBSTATE_UPDATE
					
						
					
						vector vLead
						vLead = GET_ENTITY_COORDS(vehBESRA[0])
						PS_Main.myCheckpointz[1].position = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehBESRA[0], <<0, -90, 0>>)
						PS_Main.myCheckpointz[1].position.z = vLead.z
						vFlyToCoord = PS_Main.myCheckpointz[1].position
						
						DRAW_MARKER(MARKER_RING, PS_Main.myCheckpointz[1].position, NORMALISE_VECTOR(GET_ENTITY_COORDS(vehBESRA[0])-PS_Main.myCheckpointz[1].position), <<0,0,0>>, <<PS_PARA_TARG_SCL_L, PS_PARA_TARG_SCL_L, PS_PARA_TARG_SCL_L>>, PS_COL_R,PS_COL_G,PS_COL_B, 128, FALSE, FALSE)
						
						IF DOES_BLIP_EXIST(thisMovingTargetBlipFormation)
							SET_BLIP_COORDS(thisMovingTargetBlipFormation, PS_Main.myCheckpointz[1].position)
						ELSE
							thisMovingTargetBlipFormation = CREATE_BLIP_FOR_COORD(PS_Main.myCheckpointz[1].position)
							SET_BLIP_SCALE(thisMovingTargetBlipFormation, BLIP_SIZE_NETWORK_CHECKPOINT)
						ENDIF
						
						IF PS_COL_R = PS_PARA_TARG_COL_R
							SET_BLIP_COLOUR(thisMovingTargetBlipFormation,BLIP_COLOUR_YELLOW) 
						ELSE
							SET_BLIP_COLOUR(thisMovingTargetBlipFormation,BLIP_COLOUR_GREEN) 
						ENDIF
						
						BEGIN_TEXT_COMMAND_SET_BLIP_NAME("PS_BLPMTARG")
						END_TEXT_COMMAND_SET_BLIP_NAME(thisMovingTargetBlipFormation)
						
						
						
						IF GET_TIME_POSITION_IN_RECORDING(vehBESRA[0]) >= 115000
							IF DOES_BLIP_EXIST(thisMovingTargetBlipFormation)
								REMOVE_BLIP(thisMovingTargetBlipFormation)
							ENDIF
							PS_INCREMENT_SUBSTATE()
						ENDIF
						
						/*
						//if vehicle has touched down
						IF NOT IS_ENTITY_IN_AIR(PS_Main.myVehicle)
							vTouchdownCoord = GET_ENTITY_COORDS(PS_Main.myVehicle)
							PS_ACTIVATE_CHECKPOINT_MGR(PS_LESSON_NULL)
							PS_DLC_Formation_State = PS_DLC_Engine_on_runway
						
							PS_SET_SUBSTATE(PS_SUBSTATE_UPDATE)
						ENDIF		*/																
					BREAK
					CASE PS_SUBSTATE_EXIT
						PS_UPDATE_RECORDS(VECTOR_ZERO)
						IF Pilot_School_Data_Check_Mission_Success(PS_Main.myChallengeData, PS_Main.myPlayerData)
							PS_DLC_Formation_State = PS_DLC_FORMATION_SUCCESS
						ELSE
							PS_DLC_Formation_State = PS_DLC_FORMATION_FAIL
						ENDIF
						PS_INCREMENT_SUBSTATE()
					BREAK
				ENDSWITCH
			BREAK
			
			CASE PS_DLC_FORMATION_EXIT_TRAINING
			BREAK
			
		/*	CASE PS_DLC_Engine_on_runway
				SWITCH PS_GET_SUBSTATE()
					
					CASE PS_SUBSTATE_UPDATE
					
						IF NOT IS_VEHICLE_FUCKED(PS_Main.myVehicle)			
							SET_VEHICLE_ENGINE_HEALTH(PS_Main.myVehicle,-700)
							SET_ENTITY_HEALTH(PS_Main.myVehicle,300)
							SET_VEHICLE_PETROL_TANK_HEALTH(PS_Main.myVehicle,1000)
							
						//	DRAW_GENERIC_SCORE(FLOOR(GET_ENTITY_SPEED(PS_Main.myVehicle)), "FS_SPEED")							
						ENDIF
						
						IF PS_DLC_Formation_Fail_Check()
							PS_DLC_Formation_State = PS_DLC_FORMATION_FAIL
							PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						ENDIF
					
						IF NOT IS_VEHICLE_FUCKED(PS_Main.myVehicle)	
							float FDistance
							FDistance = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PS_Main.myVehicle,vTouchdownCoord)
							PS_UPDATE_PLAYER_DATA(vTouchdownCoord)
						//	PS_Main.myPlayerData.LandingDistance = FDistance
						//	PS_Main.myPlayerData.LastLandingDistance = FDistance
							
							DRAW_GENERIC_SCORE(FLOOR(FDistance), "PSER_DISTANCE")							
						ENDIF
						
						IF PS_DLC_Formation_Progress_Check()
							CPRINTLN(DEBUG_TREVOR3, "progress acknowledged")
							PS_INCREMENT_SUBSTATE()
						ENDIF
					BREAK
					CASE PS_SUBSTATE_EXIT
						CLEAR_PRINTS()
						CLEAR_HELP()
					
						IF Pilot_School_Data_Check_Mission_Success(PS_Main.myChallengeData, PS_Main.myPlayerData)
							CPRINTLN(DEBUG_TREVOR3, "mission success found")
							PS_DLC_Formation_State = PS_DLC_FORMATION_SUCCESS
						ELSE
							CPRINTLN(DEBUG_TREVOR3, "mission fail at 1")
							PS_DLC_Formation_State = PS_DLC_FORMATION_FAIL
						ENDIF
						//PS_SET_SUBSTATE(PS_SUBSTATE_ENTER)
						PS_INCREMENT_SUBSTATE()
					BREAK
				ENDSWITCH
			BREAK*/
		
									
			CASE PS_DLC_FORMATION_SUCCESS

				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						IF DOES_BLIP_EXIST(thisMovingTargetBlipFormation)
							REMOVE_BLIP(thisMovingTargetBlipFormation)
						ENDIF
						DELETE_CHECKPOINT(thisCheckpoint)
						bPlayerDataHasBeenUpdated = FALSE
						PS_UPDATE_RECORDS(vTouchdownCoord)
						PS_PLAY_LESSON_FINISHED_LINE("PS_FOe", "PS_FOe_1", "PS_FOe_2", "PS_FOe_3", "")
						PS_LESSON_END(TRUE)
						PS_TRIGGER_MUSIC(PS_MUSIC_FORM_PASS)
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF NOT IS_PLAYER_DEAD(player_id())							
						OR GET_MANUAL_RESPAWN_STATE()= MRS_READY_FOR_PLACEMENT
							IF NOT PS_HUD_IS_SCORECARD_ACTIVE()
								PS_INCREMENT_SUBSTATE()
							ENDIF
						ELSE
							PRINTLN("PLAYER IS DEAD SETTING RESPAWN STATE")
							//Set the player to respawn state if dead and doesn't have respawn state set
							IF IS_PLAYER_DEAD(player_id())
							AND GET_MANUAL_RESPAWN_STATE() = MRS_NULL
								SET_MANUAL_RESPAWN_STATE(MRS_RESPAWN_PLAYER_HIDDEN)
							ENDIF
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						bFinishedChallenge = TRUE
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
				
			CASE PS_DLC_FORMATION_FAIL
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": PS_DLC_FORMATION_FAILure_FAIL")
				SWITCH PS_GET_SUBSTATE()
					CASE PS_SUBSTATE_ENTER
						IF DOES_BLIP_EXIST(thisMovingTargetBlipFormation)
							REMOVE_BLIP(thisMovingTargetBlipFormation)
						ENDIF
						DELETE_CHECKPOINT(thisCheckpoint)
						PS_UPDATE_RECORDS(VECTOR_ZERO)
						//PS_PLAY_LESSON_FINISHED_LINE("PS_OLOOP", "PS_OLOOP_12", "PS_OLOOP_13", "PS_OLOOP_14", "PS_OLOOP_15")
						PS_LESSON_END(FALSE)
						PS_TRIGGER_MUSIC(PS_MUSIC_FORM_FAIL)
						PS_INCREMENT_SUBSTATE()
						BREAK
					CASE PS_SUBSTATE_UPDATE
						IF NOT PS_HUD_IS_SCORECARD_ACTIVE()
							PS_INCREMENT_SUBSTATE()
						ENDIF
						BREAK
					CASE PS_SUBSTATE_EXIT
						bFinishedChallenge = TRUE
						PS_INCREMENT_SUBSTATE()
						BREAK
				ENDSWITCH
				BREAK
		ENDSWITCH
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC PS_DLC_FORMATION_MainCleanup()
//take care of hud
	PS_HUD_SET_GUTTER_ICON_ACTIVE(FALSE)
	PS_HUD_SET_OBJECTIVE_METER_VISIBLE(FALSE)
	PS_SET_TIMER_ACTIVE(FALSE)
	PS_HUD_CLEANUP()
	CLEAR_PRINTS()
	CLEAR_HELP()

//take care of objects
	//player first
	PS_SEND_PLAYER_BACK_TO_MENU()
	
	//then vehicle
	IF IS_VEHICLE_DRIVEABLE(vehBESRA[0]) 
		cprintln(debug_Trevor3,"Delete 1a")
		STOP_PLAYBACK_RECORDED_VEHICLE(vehBESRA[0])
	ENDIF
	IF IS_VEHICLE_DRIVEABLE(vehBESRA[1]) 
		cprintln(debug_Trevor3,"Delete 1b")
		STOP_PLAYBACK_RECORDED_VEHICLE(vehBESRA[1])
	ENDIF
	IF IS_VEHICLE_DRIVEABLE(vehBESRA[2]) 
		cprintln(debug_Trevor3,"Delete 1c")
		STOP_PLAYBACK_RECORDED_VEHICLE(vehBESRA[2])
	ENDIF
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(PS_ambVeh_NETID[0])
		PS_DELETE_NET_ID(PS_ambVeh_NETID[0])
	ENDIF
	IF NETWORK_DOES_NETWORK_ID_EXIST(PS_ambVeh_NETID[1])
		PS_DELETE_NET_ID(PS_ambVeh_NETID[1])
	ENDIF
	IF NETWORK_DOES_NETWORK_ID_EXIST(PS_ambVeh_NETID[2])
		PS_DELETE_NET_ID(PS_ambVeh_NETID[2])
	ENDIF
	
	//then checkpoints
	PS_RESET_CHECKPOINT_MGR()

//loaded assets
	REMOVE_VEHICLE_RECORDING(iPS_PreviewVehicleRecordingID, "PilotSchool")
	SET_MODEL_AS_NO_LONGER_NEEDED(VehicleToUse)	
	REMOVE_BLIP(thisMovingTargetBlipFormation)
	
	UNBLOCK_SCENARIOS_AND_AMBIENT(sb_PS)
	
//dont cleaning up
	#IF IS_DEBUG_BUILD
		DEBUG_MESSAGE("******************Cleaning up PS_DLC_FORMATION_FAILure.sc******************")
	#ENDIF
ENDPROC

// EOF
//****************************************************************************************************
//****************************************************************************************************

