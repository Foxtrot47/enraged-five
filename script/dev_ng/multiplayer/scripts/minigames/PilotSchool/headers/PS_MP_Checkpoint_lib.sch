//****************************************************************************************************
//
//	Author: 		Asa Dang
//	Date: 			12/20/11
//	Description: 	Contains checkpoint info and a manager to update checkpoints for flight school
//	
//****************************************************************************************************

//"Special" checkpoint, for blipping dynamic objects (like if you go out of bounds) is always at index 0
//Regular checkpoints start at an index of 1 and can go up to 49
//How to use:
//use REGISTER_CHECKPOINT to register all of your checkpoints and checkpoints types
//use ACTIVATE_CHECKPOINT_MGR with the type to start the standalone checkpoint manager
//use UPDATE_CHECKPOINT_MGR	to keep the system updated
//use PS_GET_CHECKPOINT_PROGRESS to see what checkpoint we're at
//use PS_IS_PLAYER_NEAR_CURRENT_CHECKPOINT to see if we're within a specific distance to the current checkpoint

//If you register all the checkpoints, then activate the checkpoint mgr correctly, and always run update checkpoint mgr
//the only thing you will have to do in your script is track the progress with PS_GET_CHECKPOINT_PROGRESS or PS_IS_PLAYER_NEAR_CURRENT_CHECKPOINT

//USING "Pilot_School_MP_Definitions.sch"
USING "minigames_helpers.sch"
USING "script_oddjob_funcs.sch"
USING "Pilot_School_MP_Data.sch"
USING "stunt_plane_public.sch"
USING "globals.sch"
USING "net_blips.sch"

USING "script_debug.sch"

structTimer Heli_Landed_Timer

FUNC BOOL PS_IS_CHECKPOINT_VALID(INT thisID)
	IF thisID < 0
		RETURN FALSE
	ELIF PS_Main.myCheckpointz[thisID].type != PS_CHECKPOINT_NULL
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_ENUM thisType, VECTOR thisPosition)
	IF thisType = PS_CHECKPOINT_SPECIAL
//		IF NOT PS_IS_CHECKPOINT_VALID(0)
			PS_Main.myCheckpointz[0].position = thisPosition
			PS_Main.myCheckpointz[0].type = thisType
//		ENDIF
	EXIT
	ENDIF
	
	INT i = 0
	REPEAT PS_Main.myCheckpointMgr.totalCheckpoints+2 i //repeat always goes to count-1, so we're adding 2
		IF i > 0
			IF NOT PS_IS_CHECKPOINT_VALID(i)
				PS_Main.myCheckpointz[i].position = thisPosition
				PS_Main.myCheckpointz[i].type = thisType
				PS_Main.myCheckpointMgr.totalCheckpoints++
				EXIT
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

#IF IS_DEBUG_BUILD
	PROC PS_PRINT_OUT_CHECKPOINTS_DEBUG()
		DEBUG_MESSAGE("------------------------------")
		INT i = 0
		REPEAT PS_Main.myCheckpointMgr.totalcheckpoints i
			DEBUG_MESSAGE("checkpoint id: ", GET_STRING_FROM_INT(i))
			DEBUG_MESSAGE("checkpoint type: ", GET_STRING_FROM_INT(ENUM_TO_INT(PS_Main.myCheckpointz[i].type)))
			DEBUG_MESSAGE("checkpoint coords: ", GET_STRING_FROM_VECTOR(PS_Main.myCheckpointz[i].position))
		ENDREPEAT		
		DEBUG_MESSAGE("------------------------------")
	ENDPROC
#ENDIF

PROC PS_RESET_CHECKPOINT(PS_CHECKPOINT_STRUCT &thisCheckpoint)
	IF DOES_BLIP_EXIST(thisCheckpoint.blip)
		REMOVE_BLIP(thisCheckpoint.blip )
	ENDIF
	DELETE_CHECKPOINT(thisCheckpoint.checkpoint)
	thisCheckpoint.checkpoint = NULL
	thisCheckpoint.position = VECTOR_ZERO
	thisCheckpoint.type = PS_CHECKPOINT_NULL

ENDPROC

PROC PS_DEREGISTER_CHECKPOINT(INT thisID)
	PS_RESET_CHECKPOINT(PS_Main.myCheckpointz[thisID])
ENDPROC

PROC PS_RESET_ALL_CHECKPOINTS(BOOL bResetCheckpointMarkerFlash = TRUE)
	INT i = 0
	REPEAT COUNT_OF(PS_Main.myCheckpointz) i
		PS_RESET_CHECKPOINT(PS_Main.myCheckpointz[i])
	ENDREPEAT
	IF bResetCheckpointMarkerFlash = TRUE
		DELETE_CHECKPOINT(PS_Main.flash_checkpoint)
	ENDIF
	PS_Main.bFlashCheckpointMissed 			= FALSE
	PS_Main.bDrawCheckpointMarkerFlash 		= FALSE
	PS_Main.bDrawParachuteCheckpointMarker 	= FALSE
ENDPROC

PROC PS_HIDE_CHECKPOINT(INT thisIdx)
	IF PS_IS_CHECKPOINT_VALID(thisIdx)
		DELETE_CHECKPOINT(PS_Main.myCheckpointz[thisIdx].checkpoint)
		PS_Main.myCheckpointz[thisIdx].checkpoint = NULL
		IF DOES_BLIP_EXIST(PS_Main.myCheckpointz[thisIdx].blip)
			REMOVE_BLIP(PS_Main.myCheckpointz[thisIdx].blip)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Works out which checkpoint type is needed based upon angle between checkpoints 
FUNC CHECKPOINT_TYPE GET_CHEVRON_CHECKPOINT_TYPE(INT iCheckNum)
	
	//make sure its not just a single checkpoint with no finish.
	IF iCheckNum < PS_Main.myCheckpointMgr.totalCheckpoints
		VECTOR  pos, pos2, pos3
		VECTOR  vec1, vec2

		FLOAT  	fReturnAngle
	//	FLOAT	fChev1 = 180.0 //declared in a header file somwhere
	//	FLOAT  	fChev2 = 140.0
	//	FLOAT  	fChev3 = 80.0
		 
		pos = PS_Main.myCheckpointz[iCheckNum].position
			
		IF iCheckNum+1 = PS_Main.myCheckpointMgr.totalCheckpoints
			pos2 = PS_Main.myCheckpointz[0].position
		ELSE
			pos2 = PS_Main.myCheckpointz[iCheckNum + 1].position
		ENDIF
		
		IF iCheckNum - 1 >= 0
			pos3 = PS_Main.myCheckpointz[iCheckNum-1].position
		ENDIF
		
		vec1 = pos3 - pos
		vec2 = pos2 - pos
		 
		fReturnAngle = GET_ANGLE_BETWEEN_2D_VECTORS(vec1.x, vec1.y, vec2.x, vec2.y)
		
		IF fReturnAngle > 180
			fReturnAngle = (360.0 - fReturnAngle)
		ENDIF
		
		IF fReturnAngle < fChev3
			RETURN CHECKPOINT_RACE_AIR_CHEVRON_3	
	   	ELIF fReturnAngle < fChev2
			RETURN CHECKPOINT_RACE_AIR_CHEVRON_2
		ELIF fReturnAngle < fChev1
			RETURN CHECKPOINT_RACE_AIR_CHEVRON_1
		ENDIF
	ENDIF
	
	RETURN CHECKPOINT_RACE_AIR_CHEVRON_1	 
ENDFUNC

FUNC CHECKPOINT_TYPE PS_GET_CHECKPOINT_TYPE(INT thisIdx)

	IF thisIdx >= PS_MAX_CHECKPOINTS
		RETURN CHECKPOINT_PLANE_FLAT
	ENDIF
	
	SWITCH PS_Main.myCheckpointz[thisIdx].type
	
		CASE PS_CHECKPOINT_SPECIAL	//what to do here?
			RETURN CHECKPOINT_PLANE_FLAT
		BREAK
		CASE PS_CHECKPOINT_OBSTACLE_PLANE
		CASE PS_CHECKPOINT_OBSTACLE_HELI
			RETURN GET_CHEVRON_CHECKPOINT_TYPE(thisIdx)
		BREAK
		CASE PS_CHECKPOINT_STUNT_INVERTED
			RETURN CHECKPOINT_PLANE_INVERTED
		BREAK
		CASE PS_CHECKPOINT_STUNT_SIDEWAYS_L
			RETURN CHECKPOINT_PLANE_SIDE_L
		BREAK
		CASE PS_CHECKPOINT_STUNT_SIDEWAYS_R
			RETURN CHECKPOINT_PLANE_SIDE_R
		BREAK
		CASE PS_CHECKPOINT_STUNT_LEFT
			RETURN CHECKPOINT_PLANE_SIDE_L
		BREAK
		CASE PS_CHECKPOINT_STUNT_RIGHT
			RETURN CHECKPOINT_PLANE_SIDE_R
		BREAK
		CASE PS_CHECKPOINT_FINISH
			RETURN CHECKPOINT_RACE_AIR_FLAG
		BREAK
		CASE PS_CHECKPOINT_FLAG
		CASE PS_CHECKPOINT_LANDING
			RETURN CHECKPOINT_RACE_AIR_FLAG
		BREAK
		DEFAULT
			RETURN CHECKPOINT_PLANE_FLAT
		BREAK
	ENDSWITCH
	
	RETURN CHECKPOINT_PLANE_FLAT

ENDFUNC

/// PURPOSE: Gets appropriate checkpoint colour based on chevron type
FUNC INT PS_GET_RACE_BLIP_COLOUR_FROM_CHECKPOINT(PS_CHECKPOINT_ENUM cpType)
	SWITCH cpType
		CASE PS_CHECKPOINT_STUNT_INVERTED
			RETURN BLIP_COLOUR_BLUE
		BREAK
		CASE PS_CHECKPOINT_STUNT_SIDEWAYS_L 		
		CASE PS_CHECKPOINT_STUNT_SIDEWAYS_R			
		CASE PS_CHECKPOINT_STUNT_LEFT			
		CASE PS_CHECKPOINT_STUNT_RIGHT 			
			RETURN BLIP_COLOUR_GREEN
		BREAK
		CASE PS_CHECKPOINT_FLAG
			RETURN BLIP_COLOUR_YELLOW
		BREAK
		CASE PS_CHECKPOINT_HEIGHT_OBSTACLE_PLANE
			RETURN BLIP_COLOUR_YELLOW
		BREAK
	ENDSWITCH
	RETURN BLIP_COLOUR_YELLOW
ENDFUNC

/// PURPOSE: Gets appropriate checkpoint colour based on chevron type
FUNC HUD_COLOURS PS_GET_RACE_CHECKPOINT_COLOUR(PS_CHECKPOINT_ENUM cpType)
	SWITCH cpType
		CASE PS_CHECKPOINT_STUNT_INVERTED
			RETURN HUD_COLOUR_BLUE
		BREAK
		CASE PS_CHECKPOINT_STUNT_SIDEWAYS_L 		
		CASE PS_CHECKPOINT_STUNT_SIDEWAYS_R			
		CASE PS_CHECKPOINT_STUNT_LEFT			
		CASE PS_CHECKPOINT_STUNT_RIGHT 			
			RETURN HUD_COLOUR_GREEN
		BREAK
		CASE PS_CHECKPOINT_FLAG
			RETURN HUD_COLOUR_YELLOW
		BREAK
		CASE PS_CHECKPOINT_HEIGHT_OBSTACLE_PLANE
			RETURN MG_GET_RACE_CHECKPOINT_DEFAULT_COLOUR()
		BREAK
	ENDSWITCH
	RETURN MG_GET_RACE_CHECKPOINT_DEFAULT_COLOUR()			
ENDFUNC

PROC PS_SHOW_CHECKPOINT(INT thisIdx, BOOL bSecondary = FALSE)
	
	IF ARE_VECTORS_EQUAL(PS_Main.myCheckpointz[thisIdx].position, VECTOR_ZERO)
		EXIT
	ENDIF
	
	IF DOES_BLIP_EXIST(PS_Main.myCheckpointz[thisIdx].blip)
		REMOVE_BLIP(PS_Main.myCheckpointz[thisIdx].blip)
	ENDIF
	
	IF PS_Main.myCheckpointz[thisIdx].checkpoint != NULL
		DELETE_CHECKPOINT(PS_Main.myCheckpointz[thisIdx].checkpoint)
	ENDIF
	
	CHECKPOINT_INDEX tmp
	FLOAT scale = 1.2
	FLOAT fCheckpointScale = 15.0
	BOOL flashes = FALSE
	INT iBlipColor
	TEXT_LABEL BlipName = "PS_BLPNAME"
	INT HudColor_R
	INT HudColor_G
	INT HudColor_B
	INT HudColor_A
	VECTOR nextCheckpointPosition
	
	IF thisIdx = PS_Main.myCheckpointMgr.totalCheckpoints
		IF DOES_ENTITY_EXIST(PS_Main.myVehicle) AND IS_VEHICLE_DRIVEABLE(PS_Main.myVehicle)
			if ARE_VECTORS_EQUAL(PS_Main.myCheckpointz[thisIdx+1].position, <<0,0,0>>)
				nextCheckpointPosition = PS_Main.myCheckpointz[thisIdx].position + NORMALISE_VECTOR(PS_Main.myCheckpointz[thisIdx].position - GET_ENTITY_COORDS(PS_Main.myVehicle))
			ELSE
				PRINTLN("MANUALLY SET CHECKPOINT")
				//If player has manually set the next checkpoint vector
				nextCheckpointPosition = PS_Main.myCheckpointz[thisIdx+1].position
			ENDIF
		ENDIF
	ELSE
		nextCheckpointPosition = PS_Main.myCheckpointz[thisIdx+1].position
	ENDIF
	
	IF PS_Main.myCheckpointz[thisIdx].type = PS_CHECKPOINT_FLAG
		bSecondary = TRUE
	ENDIF
	
	IF bSecondary
		scale = STUNT_PLANE_NEXT_BLIP_SCALE
		fCheckpointScale = 10.0
	ENDIF
	
	iBlipColor = ENUM_TO_INT(PS_GET_RACE_BLIP_COLOUR_FROM_CHECKPOINT(PS_Main.myCheckpointz[thisIdx].type))	
	GET_RGB_FROM_INT(GET_INT_FROM_HUD_COLOUR(PS_GET_RACE_CHECKPOINT_COLOUR(PS_Main.myCheckpointz[thisIdx].type)), HudColor_R, HudColor_G, HudColor_B, HudColor_A)
			
	IF PS_Main.myCheckpointMgr.lessonType <> PS_LESSON_SKYDIVING
		SWITCH PS_Main.myCheckpointz[thisIdx].type
			CASE PS_CHECKPOINT_SPECIAL 
				flashes = TRUE
				//just create the blip. no checkpoint
				BREAK
			CASE PS_CHECKPOINT_OBSTACLE_PLANE
				tmp = CREATE_CHECKPOINT(GET_CHEVRON_CHECKPOINT_TYPE(thisIdx), PS_Main.myCheckpointz[thisIdx].position, nextCheckpointPosition, fCheckpointScale, HudColor_R, HudColor_G, HudColor_B, MG_GET_CHECKPOINT_ALPHA(PS_Main.myCheckpointz[thisIdx].position))
				BREAK
			CASE PS_CHECKPOINT_OBSTACLE_HELI
				tmp = CREATE_CHECKPOINT(GET_CHEVRON_CHECKPOINT_TYPE(thisIdx), PS_Main.myCheckpointz[thisIdx].position, nextCheckpointPosition, fCheckpointScale, HudColor_R, HudColor_G, HudColor_B, MG_GET_CHECKPOINT_ALPHA(PS_Main.myCheckpointz[thisIdx].position))
				BREAK
			CASE PS_CHECKPOINT_STUNT_INVERTED
				tmp = CREATE_CHECKPOINT(CHECKPOINT_PLANE_INVERTED, PS_Main.myCheckpointz[thisIdx].position, nextCheckpointPosition, fCheckpointScale, HudColor_R, HudColor_G, HudColor_B, MG_GET_CHECKPOINT_ALPHA(PS_Main.myCheckpointz[thisIdx].position))
				BlipName = "PS_BLPINVRT"
				BREAK
			CASE PS_CHECKPOINT_STUNT_SIDEWAYS_L
				tmp = CREATE_CHECKPOINT(CHECKPOINT_PLANE_SIDE_L, PS_Main.myCheckpointz[thisIdx].position, nextCheckpointPosition, fCheckpointScale,  HudColor_R, HudColor_G, HudColor_B, MG_GET_CHECKPOINT_ALPHA(PS_Main.myCheckpointz[thisIdx].position))
				BlipName = "PS_BLPKNIFE"
				BREAK
			CASE PS_CHECKPOINT_STUNT_SIDEWAYS_R
				tmp = CREATE_CHECKPOINT(CHECKPOINT_PLANE_SIDE_R, PS_Main.myCheckpointz[thisIdx].position, nextCheckpointPosition, fCheckpointScale,  HudColor_R, HudColor_G, HudColor_B, MG_GET_CHECKPOINT_ALPHA(PS_Main.myCheckpointz[thisIdx].position))
				BlipName = "PS_BLPKNIFE"
				BREAK
			CASE PS_CHECKPOINT_STUNT_LEFT
				tmp = CREATE_CHECKPOINT(CHECKPOINT_PLANE_SIDE_L, PS_Main.myCheckpointz[thisIdx].position, nextCheckpointPosition, fCheckpointScale,  HudColor_R, HudColor_G, HudColor_B, MG_GET_CHECKPOINT_ALPHA(PS_Main.myCheckpointz[thisIdx].position))
				BlipName = "PS_BLPKNIFE"
				BREAK
			CASE PS_CHECKPOINT_STUNT_RIGHT
				tmp = CREATE_CHECKPOINT(CHECKPOINT_PLANE_SIDE_R, PS_Main.myCheckpointz[thisIdx].position, nextCheckpointPosition, fCheckpointScale,  HudColor_R, HudColor_G, HudColor_B, MG_GET_CHECKPOINT_ALPHA(PS_Main.myCheckpointz[thisIdx].position))
				BlipName = "PS_BLPKNIFE"
				BREAK
			CASE PS_CHECKPOINT_FINISH
				tmp = CREATE_CHECKPOINT(CHECKPOINT_RACE_AIR_FLAG, PS_Main.myCheckpointz[thisIdx].position, nextCheckpointPosition, fCheckpointScale,  HudColor_R, HudColor_G, HudColor_B, MG_GET_CHECKPOINT_ALPHA(PS_Main.myCheckpointz[thisIdx].position))
				BREAK
			CASE PS_CHECKPOINT_LANDING
				tmp = CREATE_CHECKPOINT(CHECKPOINT_RACE_AIR_FLAG, PS_Main.myCheckpointz[thisIdx].position, nextCheckpointPosition, fCheckpointScale,  HudColor_R, HudColor_G, HudColor_B, MG_GET_CHECKPOINT_ALPHA(PS_Main.myCheckpointz[thisIdx].position))
			BREAK
			CASE PS_CHECKPOINT_FLAG
				BlipName = "PS_BLIP_FLAG"
				fCheckpointScale *= 1.5
				tmp = CREATE_CHECKPOINT(CHECKPOINT_RACE_AIR_FLAG, PS_Main.myCheckpointz[thisIdx].position, nextCheckpointPosition, fCheckpointScale,  HudColor_R, HudColor_G, HudColor_B, MG_GET_CHECKPOINT_ALPHA(PS_Main.myCheckpointz[thisIdx].position))
			BREAK
			CASE PS_CHECKPOINT_HEIGHT_OBSTACLE_PLANE
				tmp = CREATE_CHECKPOINT(GET_CHEVRON_CHECKPOINT_TYPE(thisIdx), PS_Main.myCheckpointz[thisIdx].position, nextCheckpointPosition, fCheckpointScale, HudColor_R, HudColor_G, HudColor_B, MG_GET_CHECKPOINT_ALPHA(PS_Main.myCheckpointz[thisIdx].position))
			BREAK
		ENDSWITCH
	ENDIF
	
	PS_Main.myCheckpointz[thisIdx].checkpoint = tmp
	PS_Main.myCheckpointz[thisIdx].size = fCheckpointScale
	PS_Main.myCheckpointz[thisIdx].blip = CREATE_BLIP_FOR_COORD(PS_Main.myCheckpointz[thisIdx].position)
	SET_BLIP_SCALE(PS_Main.myCheckpointz[thisIdx].blip, scale)
//	SET_BLIP_DIM(PS_Main.myCheckpointz[thisIdx].blip, FALSE)
	SET_BLIP_COLOUR(PS_Main.myCheckpointz[thisIdx].blip, iBlipColor)
	
	IF PS_Main.myCheckpointz[thisIdx].type = PS_CHECKPOINT_FINISH
		SET_BLIP_SPRITE(PS_Main.myCheckpointz[thisIdx].blip, RADAR_TRACE_RACEFLAG)
		BEGIN_TEXT_COMMAND_SET_BLIP_NAME( "PS_sFINISH" )
		END_TEXT_COMMAND_SET_BLIP_NAME( PS_Main.myCheckpointz[thisIdx].blip )
	ELSE
		BEGIN_TEXT_COMMAND_SET_BLIP_NAME(BlipName)
		END_TEXT_COMMAND_SET_BLIP_NAME(PS_Main.myCheckpointz[thisIdx].blip)
	ENDIF
	SET_BLIP_FLASHES(PS_Main.myCheckpointz[thisIdx].blip, flashes)
ENDPROC

PROC PS_SHOW_ALL_CHECKPOINTS()
	INT i = 0
	REPEAT PS_Main.myCheckpointMgr.totalCheckpoints+2 i //repeat always goes to count-1, so we're adding 2
//		IF i > 0
			IF PS_IS_CHECKPOINT_VALID(i)
				//hide, then show, just in case we're showing a secondary ver it will reset
				PS_HIDE_CHECKPOINT(i)
				PS_SHOW_CHECKPOINT(i)
			ENDIF
//		ENDIF
	ENDREPEAT
ENDPROC

PROC PS_HIDE_ALL_CHECKPOINTS()
	INT i = 0
	REPEAT PS_Main.myCheckpointMgr.totalCheckpoints+2 i //repeat always goes to count-1, so we're adding 2
		IF i > 0
			IF PS_IS_CHECKPOINT_VALID(i)
				PS_HIDE_CHECKPOINT(i)
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

FUNC PS_CHECKPOINT_ENUM PS_GET_CURRENT_CHECKPOINT_TYPE()
	RETURN PS_Main.myCheckpointz[PS_Main.myCheckpointMgr.curIdx].type
ENDFUNC

FUNC PS_CHECKPOINT_ENUM PS_GET_PREV_CHECKPOINT_TYPE()
	IF PS_IS_CHECKPOINT_VALID(PS_Main.myCheckpointMgr.curIdx-1)
		RETURN PS_Main.myCheckpointz[PS_Main.myCheckpointMgr.curIdx-1].type
	ENDIF
	RETURN PS_Main.myCheckpointz[PS_Main.myCheckpointMgr.curIdx].type
ENDFUNC

FUNC PS_LESSON_ENUM PS_GET_CURRENT_LESSON_TYPE()
	RETURN PS_Main.myCheckpointMgr.lessonType
ENDFUNC

FUNC VECTOR PS_GET_NEXT_CHECKPOINT()
	IF PS_IS_CHECKPOINT_VALID(PS_Main.myCheckpointMgr.nextIdx)
		RETURN PS_Main.myCheckpointz[PS_Main.myCheckpointMgr.nextIdx].position
	ENDIF
	RETURN PS_Main.myCheckpointz[PS_Main.myCheckpointMgr.curIdx].position
ENDFUNC

FUNC VECTOR PS_GET_PREV_CHECKPOINT()
	IF PS_IS_CHECKPOINT_VALID(PS_Main.myCheckpointMgr.curIdx-1)
		RETURN PS_Main.myCheckpointz[PS_Main.myCheckpointMgr.curIdx-1].position
	ENDIF
	RETURN PS_Main.myCheckpointz[PS_Main.myCheckpointMgr.curIdx].position
ENDFUNC

FUNC VECTOR PS_GET_LAST_CHECKPOINT()
	RETURN PS_Main.myCheckpointz[PS_Main.myCheckpointMgr.totalCheckpoints].position
ENDFUNC

FUNC VECTOR PS_GET_CHECKPOINT_FROM_ID(INT thisID)
	IF PS_IS_CHECKPOINT_VALID(thisID)
		RETURN PS_Main.myCheckpointz[thisID].position
	ENDIF
	RETURN VECTOR_ZERO
ENDFUNC

FUNC VECTOR PS_GET_CURRENT_CHECKPOINT()
	IF PS_IS_CHECKPOINT_VALID(PS_Main.myCheckpointMgr.curIdx)
		RETURN PS_Main.myCheckpointz[PS_Main.myCheckpointMgr.curIdx].position
	ELSE
		RETURN PS_Main.myCheckpointz[PS_Main.myCheckpointMgr.totalCheckpoints].position
	ENDIF	
ENDFUNC

FUNC BOOL PS_HAS_PLAYER_MISSED_CURRENT_CHECKPOINT()
	RETURN PS_Main.myCheckpointMgr.hasMissed
ENDFUNC

FUNC BOOL PS_IS_LAST_CHECKPOINT_CLEARED()
	RETURN PS_Main.myCheckpointMgr.isFinished
ENDFUNC

FUNC INT PS_GET_CHECKPOINT_PROGRESS()
	RETURN PS_Main.myCheckpointMgr.curIdx
ENDFUNC

FUNC INT PS_GET_CLEARED_CHECKPOINTS()
	RETURN PS_Main.myCheckpointMgr.clearedCheckpoints
ENDFUNC

FUNC INT PS_GET_TOTAL_CHECKPOINTS()
	RETURN PS_Main.myCheckpointMgr.totalCheckpoints
ENDFUNC

FUNC INT PS_GET_TOTAL_FLAGS_TO_COLLECT()
	RETURN PS_GET_TOTAL_CHECKPOINTS() - 1
ENDFUNC

FUNC BOOL PS_ARE_ALL_FLAGS_CLEARED()
	RETURN PS_Main.myCheckpointMgr.clearedCheckpoints = PS_GET_TOTAL_FLAGS_TO_COLLECT()
ENDFUNC

PROC PS_SHOW_SPECIAL_CHECKPOINT()
	PS_SHOW_CHECKPOINT(0)
ENDPROC

FUNC BOOL PS_IS_WAITING_FOR_CHECKPOINT_FEEDBACK()
	RETURN PS_Main.myCheckpointMgr.waitingForFeedback
ENDFUNC

PROC PS_STOP_WAITING_FOR_CHECKPOINT_FEEDBACK()
	PS_Main.myCheckpointMgr.waitingForFeedback = FALSE
ENDPROC

PROC PS_RESET_SPECIAL_CHECKPOINT()
	PS_RESET_CHECKPOINT(PS_Main.myCheckpointz[0])
	IF NOT PS_IS_LAST_CHECKPOINT_CLEARED()
		IF PS_GET_CHECKPOINT_PROGRESS() <= PS_GET_TOTAL_CHECKPOINTS() AND PS_IS_CHECKPOINT_VALID(PS_Main.myCheckpointMgr.curIdx)
			PS_SHOW_CHECKPOINT(PS_Main.myCheckpointMgr.curIdx)
		ENDIF
		IF PS_GET_CURRENT_LESSON_TYPE() = PS_LESSON_OBSTACLE_HELI OR PS_GET_CURRENT_LESSON_TYPE() = PS_LESSON_OBSTACLE_PLANE
			IF PS_GET_CHECKPOINT_PROGRESS() < PS_GET_TOTAL_CHECKPOINTS() AND PS_IS_CHECKPOINT_VALID(PS_Main.myCheckpointMgr.nextIdx)
				PS_SHOW_CHECKPOINT(PS_Main.myCheckpointMgr.nextIdx, TRUE)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL PS_HAS_SPECIAL_CHECKPOINT()
	IF PS_Main.myCheckpointz[0].type = PS_CHECKPOINT_SPECIAL
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL PS_IS_CHECKPOINT_MGR_ACTIVE()
	IF PS_Main.myCheckpointMgr.isActive
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC PS_CHECKPOINT_PASS PS_GET_LAST_CHECKPOINT_SCORE()
	RETURN PS_Main.myCheckpointMgr.lastCheckpointScore
ENDFUNC

FUNC BOOL PS_IS_HELI_LANDED_TIMER_UP()
	IF IS_TIMER_STARTED(Heli_Landed_Timer)
		IF TIMER_DO_ONCE_WHEN_READY(Heli_Landed_Timer, 0.5)
			RETURN TRUE			
		ENDIF
	ELSE
		SCRIPT_ASSERT("Landed fail timer is not running when it should be!")
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL PS_IS_HELI_LANDED()
	IF NOT IS_ENTITY_DEAD(PS_Main.myVehicle)
		IF NOT IS_ENTITY_IN_AIR(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) /*AND IS_VEHICLE_STOPPED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) */AND PS_Main.myCheckpointMgr.distFromCurCheckpoint <= 8 AND IS_VEHICLE_ON_ALL_WHEELS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) AND GET_ENTITY_HEIGHT_ABOVE_GROUND(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) < 1 AND GET_ENTITY_SPEED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()) ) < 5.0 
			IF NOT IS_TIMER_STARTED(Heli_Landed_Timer)
				START_TIMER_NOW(Heli_Landed_Timer)
				RETURN TRUE
			ENDIF
			RETURN TRUE			
		ENDIF
	ENDIF
	IF IS_TIMER_STARTED(Heli_Landed_Timer)
		CANCEL_TIMER(Heli_Landed_Timer)
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL PS_IS_PLAYER_LANDED_AT_CURRENT_CHECKPOINT()
	IF NOT PS_IS_CHECKPOINT_MGR_ACTIVE() OR NOT PS_IS_CHECKPOINT_VALID(PS_Main.myCheckpointMgr.curIdx)
		RETURN FALSE
	ENDIF
	
	//TODO: figure out if we've landed in the right spot
	//temp:
	IF IS_PED_IN_ANY_PLANE(PLAYER_PED_ID())
		IF DOES_ENTITY_EXIST(PS_Main.myVehicle) AND IS_VEHICLE_DRIVEABLE(PS_Main.myVehicle)
			IF GET_ENTITY_SPEED(PS_Main.myVehicle) < 1 AND PS_Main.myCheckpointMgr.distFromCurCheckpoint <= 7.5
				RETURN TRUE
			ENDIF
		ENDIF
	ELIF IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
		IF PS_IS_HELI_LANDED()
			IF PS_IS_HELI_LANDED_TIMER_UP()
//		IF NOT IS_ENTITY_IN_AIR(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) /*AND IS_VEHICLE_STOPPED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) */AND PS_Main.myCheckpointMgr.distFromCurCheckpoint <= 8 AND IS_VEHICLE_ON_ALL_WHEELS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) AND GET_ENTITY_HEIGHT_ABOVE_GROUND(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) < 1 AND GET_ENTITY_SPEED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()) ) < 5.0 
				RETURN TRUE
//		ENDIF
			ENDIF
		ENDIF
	ENDIF
//		DEBUG_MESSAGE("player height above ground is...(drum roll).... ", GET_STRING_FROM_FLOAT(GET_ENTITY_HEIGHT_ABOVE_GROUND(PLAYER_PED_ID())))
//		DEBUG_MESSAGE("player VEHICLE height above ground is...(drum roll).... ", GET_STRING_FROM_FLOAT(GET_ENTITY_HEIGHT_ABOVE_GROUND(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))))
		RETURN FALSE

ENDFUNC

FUNC BOOL PS_IS_PLAYER_NEAR_CURRENT_CHECKPOINT(FLOAT thisLocateDist = STUNT_PLANE_LOCATE_SIZE_CHECKPOINT)
	IF NOT PS_IS_CHECKPOINT_MGR_ACTIVE() OR NOT PS_IS_CHECKPOINT_VALID(PS_Main.myCheckpointMgr.curIdx)
		RETURN FALSE
	ENDIF
	
	IF PS_Main.myCheckpointMgr.distFromCurCheckpoint <= thisLocateDist
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

PROC PS_PLAY_CHECKPOINT_CHIME(PS_CHECKPOINT_PASS thisCheckpointInfo)
	IF PS_Main.myCheckpointMgr.bForPreview	
		EXIT
	ENDIF
	
	SWITCH(thisCheckpointInfo)
		CASE PS_CHECKPOINT_OK
			PLAY_SOUND_FRONTEND(-1, STUNT_PLANE_SFX_CHECKPOINT_CLEAR, "HUD_MINI_GAME_SOUNDSET")
			BREAK
			
		CASE PS_CHECKPOINT_AWESOME
			PLAY_SOUND_FRONTEND(-1, STUNT_PLANE_SFX_CHECKPOINT_PERFECT, "HUD_MINI_GAME_SOUNDSET")
			BREAK
			
		CASE PS_CHECKPOINT_MISS
			PLAY_SOUND_FRONTEND(-1, STUNT_PLANE_SFX_CHECKPOINT_MISS, "HUD_MINI_GAME_SOUNDSET")
			BREAK
	ENDSWITCH
	
ENDPROC

PROC PS_CLEAR_CURRENT_CHECKPOINT(PS_CHECKPOINT_PASS thisState)
	PS_Main.myCheckpointMgr.lastCheckpointScore = thisState
	
	SWITCH thisState
		CASE PS_CHECKPOINT_MISS
			//do any ptfx
			PS_HIDE_CHECKPOINT(PS_Main.myCheckpointMgr.curIdx)
			PS_Play_Checkpoint_Chime(PS_CHECKPOINT_MISS)
			//IF IS_TIMER_STARTED(PS_Main.tRaceTimer)
			//	RESTART_TIMER_AT(PS_Main.tRaceTimer, GET_TIMER_IN_SECONDS(PS_Main.tRaceTimer)-PS_PERFECT_BONUS)	//comment out to fix B*1881079		
			//ENDIF
			PS_UI_RaceHud.iTimeBonus = PS_SKIPPED_BONUS
			PS_Main.bFlashCheckpointMissed = TRUE
			BREAK
		CASE PS_CHECKPOINT_OK
			PS_Main.myCheckpointMgr.clearedCheckpoints++
			PS_HIDE_CHECKPOINT(PS_Main.myCheckpointMgr.curIdx)
			PS_Play_Checkpoint_Chime(PS_CHECKPOINT_OK)
			BREAK
		CASE PS_CHECKPOINT_AWESOME
			PS_Main.myCheckpointMgr.clearedCheckpoints++
			PS_HIDE_CHECKPOINT(PS_Main.myCheckpointMgr.curIdx)
			PS_Play_Checkpoint_Chime(PS_CHECKPOINT_AWESOME)
			//IF IS_TIMER_STARTED(PS_Main.tRaceTimer)
			//	RESTART_TIMER_AT(PS_Main.tRaceTimer, GET_TIMER_IN_SECONDS(PS_Main.tRaceTimer)-PS_PERFECT_BONUS) //comment out to fix B*1881079	
			//ENDIF
			PS_UI_RaceHud.iTimeBonus = PS_PERFECT_BONUS
			BREAK
	ENDSWITCH

	IF NOT PS_Main.myCheckpointMgr.bForPreview	
		PS_Main.myPlayerData.CheckpointCount = PS_Main.myCheckpointMgr.clearedCheckpoints
		PS_Main.myCheckpointMgr.waitingForFeedback = TRUE
	ENDIF
ENDPROC

//new dlc lesson checkpoint type procedures to deal with flag collection

PROC PS_UPDATE_CLOSEST_FLAG_CHECKPOINT()

	IF PS_IS_CHECKPOINT_MGR_ACTIVE()

		IF IS_VEHICLE_DRIVEABLE(PS_Main.myVehicle)

			INT 	i					= 0
			FLOAT	fCurrentDistance	= 0
			FLOAT 	fShortestDistance 	= 100000.0
			
			REPEAT PS_GET_TOTAL_CHECKPOINTS() i
			
				IF PS_IS_CHECKPOINT_VALID(i)
				
					//DRAW_DEBUG_TEXT_ABOVE_COORDS(PS_Main.myCheckpointz[i].position, GET_STRING_FROM_INT(i), 0.0)
				
					fCurrentDistance = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PS_Main.myVehicle), PS_Main.myCheckpointz[i].position)
					
					IF ( fCurrentDistance < fShortestDistance )
					
						fShortestDistance = fCurrentDistance
					
						PS_Main.myCheckpointMgr.curIdx 					= i
						PS_Main.myCheckpointMgr.distFromCurCheckpoint 	= fCurrentDistance
					
					ENDIF
					
				ENDIF
			
			ENDREPEAT
			
		ENDIF
			
	ENDIF

ENDPROC

PROC PS_CLEAR_FLAG_CHECKPOINT(INT i)
	
	IF PS_IS_CHECKPOINT_MGR_ACTIVE() AND PS_IS_CHECKPOINT_VALID(i)
	
		PS_Main.myCheckpointMgr.clearedCheckpoints++
		PS_HIDE_CHECKPOINT(i)
		PS_RESET_CHECKPOINT(PS_Main.myCheckpointz[i])
		PS_PLAY_CHECKPOINT_CHIME(PS_CHECKPOINT_OK)
		
		//what is this for?
		IF NOT PS_Main.myCheckpointMgr.bForPreview	
			PS_Main.myPlayerData.CheckpointCount = PS_Main.myCheckpointMgr.clearedCheckpoints
			PS_Main.myCheckpointMgr.waitingForFeedback = TRUE
		ENDIF
		
		IF PS_ARE_ALL_FLAGS_CLEARED()
			PS_Main.myCheckpointMgr.isFinished = TRUE
			PS_Main.myCheckpointMgr.savedFinishTime = GET_TIMER_IN_SECONDS_SAFE(PS_Main.tRaceTimer)
		ENDIF
	ENDIF
	
ENDPROC


FUNC PS_CHECKPOINT_PASS PS_CHECK_FOR_STUNT_THROUGH_CURRENT_CHECKPOINT()
	VECTOR vFront, vSide, vUp, vPos
	GET_ENTITY_MATRIX(PS_Main.myVehicle, vFront, vSide, vUp, vPos)
	
	//see if we have a stunt checkpoint
	SWITCH PS_GET_CURRENT_CHECKPOINT_TYPE()
		CASE PS_CHECKPOINT_STUNT_INVERTED
			IF vUp.z < 0.0
				IF ABSF(vSide.z) < PS_STUNT_INVERT_PERFECT_THRESHOLD
					RETURN PS_CHECKPOINT_AWESOME 
				ELSE
					RETURN PS_CHECKPOINT_OK 
				ENDIF
			ELSE
				RETURN PS_CHECKPOINT_MISS
			ENDIF
			BREAK
		
		CASE PS_CHECKPOINT_STUNT_SIDEWAYS_L	
		CASE PS_CHECKPOINT_STUNT_SIDEWAYS_R
			IF ABSF(vUp.z) < PS_STUNT_ROLL_PERFECT_THRESHOLD
				RETURN PS_CHECKPOINT_AWESOME
			ELIF ABSF(vUp.z) < STUNT_PLANE_ROLL_THRESHOLD
				RETURN PS_CHECKPOINT_OK 
			ELSE
				RETURN PS_CHECKPOINT_MISS
			ENDIF
			BREAK
			
		CASE PS_CHECKPOINT_STUNT_LEFT
			IF vSide.z <= 0.0
				RETURN PS_CHECKPOINT_MISS
			ELIF ABSF(vUp.z) < PS_STUNT_ROLL_PERFECT_THRESHOLD
				RETURN PS_CHECKPOINT_AWESOME
			ELIF ABSF(vUp.z) < STUNT_PLANE_ROLL_THRESHOLD
				RETURN PS_CHECKPOINT_OK 
			ELSE
				RETURN PS_CHECKPOINT_MISS
			ENDIF
			BREAK
			
		CASE PS_CHECKPOINT_STUNT_RIGHT
			IF vSide.z >= 0.0
				RETURN PS_CHECKPOINT_MISS
			ELIF ABSF(vUp.z) < PS_STUNT_ROLL_PERFECT_THRESHOLD
				RETURN PS_CHECKPOINT_AWESOME
			ELIF ABSF(vUp.z) < STUNT_PLANE_ROLL_THRESHOLD
				RETURN PS_CHECKPOINT_OK 
			ELSE
				RETURN PS_CHECKPOINT_MISS
			ENDIF
			BREAK
	ENDSWITCH
	
	//otherwise its just a reg checkpoints
	RETURN PS_CHECKPOINT_OK 
ENDFUNC

PROC PS_CREATE_FLASHING_CHECKPOINT(INT iCurrentId, INT iNextId)

	//IF NOT PS_Main.myCheckpointMgr.bForPreview
		IF ( PS_Main.flash_checkpoint = NULL )

			INT HudColor_R, HudColor_G, HudColor_B
		
			IF PS_Main.bFlashCheckpointMissed = TRUE
				PS_Main.eFlashHudColour = HUD_COLOUR_RED
			ELSE
				PS_Main.eFlashHudColour = HUD_COLOUR_WHITE
			ENDIF
		
			GET_HUD_COLOUR(PS_Main.eFlashHudColour, HudColor_R, HudColor_G, HudColor_B, PS_Main.iFlashCheckpointAlpha)
			PS_Main.flash_checkpoint = CREATE_CHECKPOINT(PS_GET_CHECKPOINT_TYPE(iCurrentId),
														 PS_Main.myCheckpointz[iCurrentId].position,
														 PS_Main.myCheckpointz[iNextId].position,
														 PS_Main.myCheckpointz[iCurrentId].size, HudColor_R, HudColor_G, HudColor_B, PS_Main.iFlashCheckpointAlpha)
														 
			cprintln(debug_Trevor3,"HOST FLASH CHECKPOINT: ",ENUM_TO_INT(PS_GET_CHECKPOINT_TYPE(iCurrentId))," ",PS_Main.myCheckpointz[iCurrentId].position," ",PS_Main.myCheckpointz[iNextId].position," ",PS_Main.myCheckpointz[iCurrentId].size)
		ENDIF
	//ENDIF
	
ENDPROC

PROC PS_CREATE_FLASHING_CHECKPOINT_FOR_SPECTATOR(vector vCurrent, vector vNext, int iCurType, bool wasMissed)
	IF ( PS_Main.flash_checkpoint = NULL )

		INT HudColor_R, HudColor_G, HudColor_B
		
		
		
		float fCheckpointScale
		IF g_current_selected_dlc_PilotSchool_class = int_to_enum(PILOT_SCHOOL_DLC_CLASSES_ENUM_NEW,PSCD_DLC_CollectFlags)
			fCheckpointScale = 15.0
		ELSE
			fCheckpointScale = 10.0
		ENDIF
		
		/*
		ped_index observedPEd = GET_SPECTATOR_CURRENT_FOCUS_PED()
		
		IF NOT IS_PED_INJURED(observedPed)
			IF IS_VECTOR_ZERO(vNext)							
				vector vGap
				vGap = vCurrent - GET_ENTITY_COORDS(observedPed)
				vNext = vCurrent + vGap
			ENDIF									
			
			//manual check for checkpoint missed rather than rely on passing a bool over the network
			PS_Main.myCheckpointMgr.lessonType = PS_LESSON_OBSTACLE_PLANE
			PS_CLEAR_CURRENT_CHECKPOINT(PS_CHECK_FOR_STUNT_THROUGH_CURRENT_CHECKPOINT())
			PS_Main.bFlashCheckpointMissed = FALSE
			IF NOT ARE_VECTORS_ALMOST_EQUAL(vCurrent, vNext)		
				IF VDIST2(GET_ENTITY_COORDS(observedPed), vCurrent) > VDIST2(GET_ENTITY_COORDS(observedPed), vNext)
					IF (DOT_PRODUCT(GET_ENTITY_COORDS(observedPed), vNext) > 0.0)
						PS_Main.bFlashCheckpointMissed = TRUE
					ENDIF
				ELSE
					VECTOR vDrvVec = GET_ENTITY_COORDS(observedPed) - vCurrent
					VECTOR vGateVec = vNext - vCurrent
					//means we passed curCheckpoint
					IF (DOT_PRODUCT(vDrvVec, vGateVec) > 0.0)
						IF (DOT_PRODUCT(GET_ENTITY_COORDS(observedPed), vNext) > 0.0)
							PS_Main.bFlashCheckpointMissed = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		ENDIF
		*/
		
		
		IF wasMissed = TRUE
			PS_Main.eFlashHudColour = HUD_COLOUR_RED
		ELSE
			PS_Main.eFlashHudColour = HUD_COLOUR_WHITE
		ENDIF						
								
		PS_Main.myCheckpointz[0].type = INT_TO_ENUM(PS_CHECKPOINT_ENUM,iCurType) //a fudge so I can use PS_GET_CHECKPOINT_TYPE() below
			
		GET_HUD_COLOUR(PS_Main.eFlashHudColour, HudColor_R, HudColor_G, HudColor_B, PS_Main.iFlashCheckpointAlpha)
		PS_Main.flash_checkpoint = CREATE_CHECKPOINT(PS_GET_CHECKPOINT_TYPE(0),
														 vCurrent,
														 vNext,
														 fCheckpointScale, HudColor_R, HudColor_G, HudColor_B, PS_Main.iFlashCheckpointAlpha)
														 
		cprintln(debug_Trevor3,"SPEC FLASH CHECKPOINT: ",INT_TO_ENUM(CHECKPOINT_TYPE,iCurType)," ",vCurrent," ",vNext," ",fCheckpointScale)
	ENDIF
ENDPROC

PROC PS_DELETE_FLASHING_CHECKPOINT()

	DELETE_CHECKPOINT(PS_Main.flash_checkpoint)
	PS_Main.bFlashCheckpointMissed = FALSE

ENDPROC

PROC PS_INITIALISE_CHECKPOINT_MARKER_FLASH(VECTOR vTarget, BOOL bForParachute = TRUE)

	IF PS_Main.bDrawCheckpointMarkerFlash = FALSE

		INT HudColor_R, HudColor_G, HudColor_B
				
		GET_HUD_COLOUR(HUD_COLOUR_WHITE, HudColor_R, HudColor_G, HudColor_B, PS_Main.iFlashCheckpointAlpha)

		PS_Main.bDrawParachuteCheckpointMarker	= bForParachute
		PS_Main.vCheckpointMarkerFlashPosition 	= vTarget
		PS_Main.bDrawCheckpointMarkerFlash 		= TRUE
		
	ENDIF
	
ENDPROC

PROC PS_UPDATE_CHECKPOINT_MARKER_FLASH()

	IF PS_Main.bDrawCheckpointMarkerFlash = TRUE

		INT HudColor_R, HudColor_G, HudColor_B, HudColor_A

		PS_Main.iFlashCheckpointAlpha -= 25
		IF PS_Main.iFlashCheckpointAlpha > 0
			GET_HUD_COLOUR(HUD_COLOUR_WHITE, HudColor_R, HudColor_G, HudColor_B, HudColor_A)
			IF ( PS_Main.bDrawParachuteCheckpointMarker = TRUE )
				DRAW_MARKER(MARKER_RING, PS_Main.vCheckpointMarkerFlashPosition , <<0,0,1>>, <<0,0,0>>, <<PS_PARA_TARG_SCL_S, PS_PARA_TARG_SCL_S, PS_PARA_TARG_SCL_S>>, HudColor_R, HudColor_G, HudColor_B, PS_Main.iFlashCheckpointAlpha, FALSE, FALSE)
				DRAW_MARKER(MARKER_RING, PS_Main.vCheckpointMarkerFlashPosition , <<0,0,1>>, <<0,0,0>>, <<PS_PARA_TARG_SCL_M, PS_PARA_TARG_SCL_M, PS_PARA_TARG_SCL_M>>, HudColor_R, HudColor_G, HudColor_B, PS_Main.iFlashCheckpointAlpha, FALSE, FALSE)
				DRAW_MARKER(MARKER_RING, PS_Main.vCheckpointMarkerFlashPosition , <<0,0,1>>, <<0,0,0>>, <<PS_PARA_TARG_SCL_L, PS_PARA_TARG_SCL_L, PS_PARA_TARG_SCL_L>>, HudColor_R, HudColor_G, HudColor_B, PS_Main.iFlashCheckpointAlpha, FALSE, FALSE)	
			ELSE
				DRAW_MARKER(MARKER_RING, <<PS_Main.vCheckpointMarkerFlashPosition.x, PS_Main.vCheckpointMarkerFlashPosition.y, PS_Main.vCheckpointMarkerFlashPosition.z - 1.5>>, <<0,0,1>>, <<0,0,0>>, <<5, 5, 5>>, HudColor_R, HudColor_G, HudColor_B, PS_Main.iFlashCheckpointAlpha, FALSE, FALSE)
			ENDIF
		ELSE
			PS_Main.bDrawCheckpointMarkerFlash 		= FALSE
			PS_Main.bDrawParachuteCheckpointMarker 	= FALSE
	    ENDIF
	ENDIF
	
ENDPROC

PROC PS_INCREMENT_CHECKPOINT()

	IF PS_HAS_PLAYER_MISSED_CURRENT_CHECKPOINT()
		PS_Main.myCheckpointMgr.hasMissed = FALSE//clear the flag we were using to keep track of missed checkpoints
		PS_Main.bFlashCheckpointMissed = TRUE
		PS_CLEAR_CURRENT_CHECKPOINT(PS_CHECKPOINT_MISS)
	ELIF PS_Main.myCheckpointMgr.lessonType = PS_LESSON_OBSTACLE_PLANE
		PS_CLEAR_CURRENT_CHECKPOINT(PS_CHECK_FOR_STUNT_THROUGH_CURRENT_CHECKPOINT())
	ELSE
		PS_CLEAR_CURRENT_CHECKPOINT(PS_CHECKPOINT_OK)		
	ENDIF
	
	//if we're not at the last checkpoint, then incr
	IF PS_GET_CHECKPOINT_PROGRESS() < PS_GET_TOTAL_CHECKPOINTS()
		PS_Main.myCheckpointMgr.curIdx++	
		PS_HIDE_CHECKPOINT(PS_Main.myCheckpointMgr.nextIdx) //hide the secondary checkpoint
		PS_SHOW_CHECKPOINT(PS_Main.myCheckpointMgr.curIdx) //replace it with a new one
	ELIF PS_GET_CHECKPOINT_PROGRESS() = PS_GET_TOTAL_CHECKPOINTS()
		PS_Main.myCheckpointMgr.isFinished = TRUE
//		IF PS_Main.myCheckpointMgr.lessonType > PS_LESSON_STUNT AND IS_TIMER_STARTED(PS_Main.tRaceTimer)
//			PAUSE_TIMER(PS_Main.tRaceTimer)
//		ENDIF

		PS_Main.myCheckpointMgr.savedFinishTime = GET_TIMER_IN_SECONDS_SAFE(PS_Main.tRaceTimer)

		//special case for dlc moving vehicle landing, treat it as special case, as the setup for this lesson is so stange it's best to leave it as is
		//and just work around it when flashing the marker
		IF IS_PILOT_SCHOOL_DLC()
			IF PS_Main.myChallengeData.LessonEnum = PSCD_DLC_VehicleLanding
				IF PS_Main.myCheckpointMgr.lessonType = PS_LESSON_OBSTACLE_HELI
					IF PS_Main.myCheckpointz[PS_GET_TOTAL_CHECKPOINTS()].type = PS_CHECKPOINT_LANDING
						PS_INITIALISE_CHECKPOINT_MARKER_FLASH(PS_Main.myCheckpointz[PS_GET_TOTAL_CHECKPOINTS()].position, FALSE)
						EXIT	//exit and don't process anything elese in this scenario
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		PS_CREATE_FLASHING_CHECKPOINT(PS_Main.myCheckpointMgr.curIdx, PS_Main.myCheckpointMgr.curIdx)
		EXIT
	ENDIF
	
	IF PS_Main.myCheckpointMgr.lessonType = PS_LESSON_OBSTACLE_HELI OR PS_Main.myCheckpointMgr.lessonType = PS_LESSON_OBSTACLE_PLANE
		//if we're still not at the last checkpoint, then increment the next checkpoint, too
		IF PS_GET_CHECKPOINT_PROGRESS() < PS_GET_TOTAL_CHECKPOINTS()		
			PS_Main.myCheckpointMgr.nextIdx++		
			PS_SHOW_CHECKPOINT(PS_Main.myCheckpointMgr.nextIdx, TRUE)
		ENDIF
	ENDIF
	
	//Used for landing DLC challenges (City Landing)
	IF IS_PILOT_SCHOOL_DLC()
		IF PS_Main.myCheckpointMgr.lessonType = PS_LESSON_LANDING
			IF PS_GET_CHECKPOINT_PROGRESS() < PS_GET_TOTAL_CHECKPOINTS()
				IF PS_Main.myChallengeData.LessonEnum != PSCD_DLC_CityLanding
					PS_Main.myCheckpointMgr.nextIdx++		
					PS_SHOW_CHECKPOINT(PS_Main.myCheckpointMgr.nextIdx, TRUE)
				ELSE
					//Show the next checkpoint if it's after the first
					IF PS_GET_CHECKPOINT_PROGRESS() > 1
						PS_Main.myCheckpointMgr.nextIdx++		
						PS_SHOW_CHECKPOINT(PS_Main.myCheckpointMgr.nextIdx, TRUE)	
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//dlc lesson height checkpoints are the same as normal obstacle checkpoints
	IF PS_Main.myCheckpointMgr.lessonType = PS_LESSON_HEIGHT_OBSTACLE_PLANE
		//if we're still not at the last checkpoint, then increment the next checkpoint, too
		IF PS_GET_CHECKPOINT_PROGRESS() < PS_GET_TOTAL_CHECKPOINTS()		
			PS_Main.myCheckpointMgr.nextIdx++		
			PS_SHOW_CHECKPOINT(PS_Main.myCheckpointMgr.nextIdx, TRUE)
		ENDIF
	ENDIF
	
	PS_CREATE_FLASHING_CHECKPOINT(PS_Main.myCheckpointMgr.curIdx - 1, PS_Main.myCheckpointMgr.curIdx)

ENDPROC

PROC PS_CHECK_FOR_MISSED_CHECKPOINT()
	IF PS_GET_CHECKPOINT_PROGRESS() = PS_GET_TOTAL_CHECKPOINTS()
		//cant miss last checkpoint
		EXIT
	ENDIF
	
	IF PS_Main.myChallengeData.LessonEnum = PSCD_DLC_CityLanding
		//Don't want to miss the first checkpoint in city landing
		IF PS_GET_CHECKPOINT_PROGRESS() = 1
			EXIT
		ENDIF
	ENDIF
	
	VECTOR curCheckpoint, nextCheckpoint, playerPos
	IF DOES_ENTITY_EXIST(PS_Main.myVehicle) AND IS_VEHICLE_DRIVEABLE(PS_Main.myVehicle)
		playerPos = GET_ENTITY_COORDS(PS_Main.myVehicle)
	ENDIF
	curCheckpoint = PS_GET_CURRENT_CHECKPOINT()
	nextCheckpoint = PS_GET_NEXT_CHECKPOINT()
	
	IF NOT ARE_VECTORS_ALMOST_EQUAL(curCheckpoint, nextCheckpoint)
		
		IF VDIST2(playerPos, curCheckpoint) > VDIST2(playerPos, nextCheckpoint)
			IF (DOT_PRODUCT(playerPos, nextCheckpoint) > 0.0)
				PS_Main.myCheckpointMgr.hasMissed = TRUE
			ENDIF
		ELSE
			VECTOR vDrvVec = playerPos - curCheckpoint
			VECTOR vGateVec = nextCheckpoint - curCheckpoint
			//means we passed curCheckpoint
			IF (DOT_PRODUCT(vDrvVec, vGateVec) > 0.0)
				IF (DOT_PRODUCT(playerPos, nextCheckpoint) > 0.0)
					PS_Main.myCheckpointMgr.hasMissed = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC 

#IF IS_DEBUG_BUILD
	PROC PS_CHECKPOINT_JUMP()
		FLOAT fSpeed = 0.0
		BOOL bKeepVehicle = TRUE
		
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(PS_Main.myVehicle))
					fSpeed = 0.0
				ELIF IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(PS_Main.myVehicle))
					IF GET_ENTITY_MODEL(PS_Main.myVehicle) = STUNT 
						fSpeed = 45.0
					ELIF GET_ENTITY_MODEL(PS_Main.myVehicle) = CUBAN800
						// handling the cuban800 skips in their scripts to account for landing gear
						EXIT
					//Adding for DLC planes
					ELIF GET_ENTITY_MODEL(PS_Main.myVehicle) = TITAN
						fSpeed = 45.0
					ELIF GET_ENTITY_MODEL(PS_Main.myVehicle) = Hydra
						fSpeed = 45.0
					ELIF GET_ENTITY_MODEL(PS_Main.myVehicle) = miljet
						fSpeed = 45.0
					ELSE
						//in other plane
						//changing to default to keep vehicle. Why would we ever want to delete the vehicle?
						fSpeed = 45.0
						//bKeepVehicle = FALSE	
					ENDIF
				ELSE
					//if we're not in any veh
					bKeepVehicle = FALSE 
				ENDIF
			ELSE
				bKeepVehicle = FALSE
			ENDIF
		ENDIF
		
		IF bKeepVehicle
			VECTOR vFwd = PS_GET_NEXT_CHECKPOINT() - PS_GET_CURRENT_CHECKPOINT()
			FLOAT fHeading = GET_HEADING_FROM_VECTOR_2D(vFwd.x, vFwd.y)
			SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), PS_GET_CURRENT_CHECKPOINT())
			IF PS_GET_CHECKPOINT_PROGRESS() < PS_GET_TOTAL_CHECKPOINTS()
				IF NOT IS_VEHICLE_FUCKED(PS_Main.myVehicle)
					SET_ENTITY_HEADING(PS_Main.myVehicle, fHeading)
					SET_VEHICLE_FORWARD_SPEED(PS_Main.myVehicle, fSpeed)
				ENDIF
			ENDIF
		ELSE
			SET_ENTITY_COORDS(PLAYER_PED_ID(), PS_GET_CURRENT_CHECKPOINT())
		ENDIF
	ENDPROC

	FUNC BOOL PS_CHECK_FOR_J_SKIP_BUTTON()
		// if we're not at the last checkpoint, we can jskip
		IF PS_GET_CHECKPOINT_PROGRESS() <= PS_GET_TOTAL_CHECKPOINTS()
			IF IS_DEBUG_KEY_JUST_PRESSED(KEY_J, KEYBOARD_MODIFIER_NONE, "J-SKIP")
				
					bPlayerUsedDebugSkip = TRUE
				RETURN TRUE
			ENDIF
		ENDIF
		RETURN FALSE
	ENDFUNC
#ENDIF

PROC PS_UPDATE_CHECKPOINT_MARKER(VECTOR vTarget)
	DRAW_MARKER(MARKER_RING, vTarget, <<0,0,1>>, <<0,0,0>>, <<PS_PARA_TARG_SCL_S, PS_PARA_TARG_SCL_S, PS_PARA_TARG_SCL_S>>, PS_PARA_TARG_COL_R, PS_PARA_TARG_COL_G, PS_PARA_TARG_COL_B, MG_GET_CHECKPOINT_ALPHA(vTarget), FALSE, FALSE)
	DRAW_MARKER(MARKER_RING, vTarget, <<0,0,1>>, <<0,0,0>>, <<PS_PARA_TARG_SCL_M, PS_PARA_TARG_SCL_M, PS_PARA_TARG_SCL_M>>, PS_PARA_TARG_COL_R, PS_PARA_TARG_COL_G, PS_PARA_TARG_COL_B, MG_GET_CHECKPOINT_ALPHA(vTarget), FALSE, FALSE)
	DRAW_MARKER(MARKER_RING, vTarget, <<0,0,1>>, <<0,0,0>>, <<PS_PARA_TARG_SCL_L, PS_PARA_TARG_SCL_L, PS_PARA_TARG_SCL_L>>, PS_PARA_TARG_COL_R, PS_PARA_TARG_COL_G, PS_PARA_TARG_COL_B, MG_GET_CHECKPOINT_ALPHA(vTarget), FALSE, FALSE)		
ENDPROC

PROC PS_UPDATE_CHECKPOINT_MGR()
	IF NOT PS_IS_CHECKPOINT_MGR_ACTIVE() 
		EXIT
	ENDIF
	
	//TODO: will this fuck up the special checkpoint system in any other lesson??
	//first check if we have any special checkpoints to show, and override the other checkpoints if this is true
	IF PS_HAS_SPECIAL_CHECKPOINT()
		IF NOT DOES_BLIP_EXIST(PS_Main.myCheckpointz[0].blip)
			PS_SHOW_SPECIAL_CHECKPOINT()
			PS_HIDE_CHECKPOINT(PS_Main.myCheckpointMgr.curIdx)
			IF PS_IS_CHECKPOINT_VALID(PS_Main.myCheckpointMgr.nextIdx)
				PS_HIDE_CHECKPOINT(PS_Main.myCheckpointMgr.nextIdx)	
			ENDIF
		ENDIF	
		EXIT
	ENDIF
	
	IF PS_Main.myCheckpointMgr.isFinished
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF PS_CHECK_FOR_J_SKIP_BUTTON()
			PS_CHECKPOINT_JUMP()
			EXIT
		ENDIF
	#ENDIF
	
	//switch to see what type of lesson we're in
	SWITCH PS_Main.myCheckpointMgr.lessonType
	//case stunt
	CASE PS_LESSON_STUNT
		IF IS_VEHICLE_DRIVEABLE(PS_Main.myVehicle)
			PS_Main.myCheckpointMgr.distFromCurCheckpoint = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PS_Main.myVehicle), PS_Main.myCheckpointz[PS_Main.myCheckpointMgr.curIdx].position)
		ENDIF
		//track the current checkpoint
		IF PS_GET_CHECKPOINT_PROGRESS() <= PS_GET_TOTAL_CHECKPOINTS()	
			IF PS_IS_PLAYER_NEAR_CURRENT_CHECKPOINT() OR PS_HAS_PLAYER_MISSED_CURRENT_CHECKPOINT()
				PS_INCREMENT_CHECKPOINT()
			ENDIF
		ENDIF
		BREAK
		
	//case heli obstacle course
	CASE PS_LESSON_OBSTACLE_HELI
		//track the current checkpoint
		IF NOT IS_VEHICLE_DRIVEABLE(PS_Main.myVehicle)
			BREAK
		ENDIF
		PS_Main.myCheckpointMgr.distFromCurCheckpoint = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PS_Main.myVehicle), PS_Main.myCheckpointz[PS_Main.myCheckpointMgr.curIdx].position)
		//track the current checkpoint and 
		IF PS_GET_CHECKPOINT_PROGRESS() < PS_GET_TOTAL_CHECKPOINTS()			
			IF PS_IS_PLAYER_NEAR_CURRENT_CHECKPOINT() OR PS_HAS_PLAYER_MISSED_CURRENT_CHECKPOINT()
				PS_INCREMENT_CHECKPOINT()
			ENDIF
		ELIF PS_GET_CHECKPOINT_PROGRESS() = PS_GET_TOTAL_CHECKPOINTS()
			IF PS_IS_PLAYER_LANDED_AT_CURRENT_CHECKPOINT()
				PS_INCREMENT_CHECKPOINT()
			ENDIF
		ENDIF
		BREAK
	//case plane obstacle course
	CASE PS_LESSON_OBSTACLE_PLANE
		//track the current checkpoint 
		IS_VEHICLE_FUCKED(PS_Main.myVehicle)
		PS_Main.myCheckpointMgr.distFromCurCheckpoint = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PS_Main.myVehicle), PS_Main.myCheckpointz[PS_Main.myCheckpointMgr.curIdx].position)
		//track the current checkpoint and 
		IF PS_GET_CHECKPOINT_PROGRESS() <= PS_GET_TOTAL_CHECKPOINTS()	
			IF PS_IS_PLAYER_NEAR_CURRENT_CHECKPOINT() OR PS_HAS_PLAYER_MISSED_CURRENT_CHECKPOINT()
				PS_INCREMENT_CHECKPOINT()
			ENDIF
		ENDIF
		BREAK
		
	//case landing
	CASE PS_LESSON_LANDING
		//track the current checkpoint
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			IF DOES_ENTITY_EXIST(PS_Main.myVehicle) AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), PS_Main.myVehicle) //we're in a plane, probably daring landing.
				PS_Main.myCheckpointMgr.distFromCurCheckpoint = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), PS_Main.myCheckpointz[PS_Main.myCheckpointMgr.curIdx].position)
				IF PS_GET_CHECKPOINT_PROGRESS() < PS_GET_TOTAL_CHECKPOINTS()	
					//has player landed?
					//Want to check for missed checkpoints in DLC City Landing revision
					IF IS_PILOT_SCHOOL_DLC()
						IF PS_IS_PLAYER_NEAR_CURRENT_CHECKPOINT() OR PS_HAS_PLAYER_MISSED_CURRENT_CHECKPOINT()
							PS_INCREMENT_CHECKPOINT()
						ENDIF
					ELSE
						IF PS_IS_PLAYER_NEAR_CURRENT_CHECKPOINT()// OR PS_HAS_PLAYER_MISSED_CURRENT_CHECKPOINT()
							PS_INCREMENT_CHECKPOINT()
						ENDIF
					ENDIF
				ELIF PS_GET_CHECKPOINT_PROGRESS() = PS_GET_TOTAL_CHECKPOINTS()	
					IF PS_IS_PLAYER_LANDED_AT_CURRENT_CHECKPOINT()
						PS_INCREMENT_CHECKPOINT()
					ENDIF
				ENDIF
			ELSE //we're not in a plane, its probably parachute
				PS_Main.myCheckpointMgr.distFromCurCheckpoint = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), PS_Main.myCheckpointz[PS_Main.myCheckpointMgr.curIdx].position)
				IF PS_GET_CHECKPOINT_PROGRESS() < PS_GET_TOTAL_CHECKPOINTS()	
					//has player landed?
					IF PS_IS_PLAYER_NEAR_CURRENT_CHECKPOINT() OR PS_HAS_PLAYER_MISSED_CURRENT_CHECKPOINT()
						PS_INCREMENT_CHECKPOINT()
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		BREAK
		
	//case landing
	CASE PS_LESSON_SKYDIVING
		//track the current checkpoint
		PS_UPDATE_CHECKPOINT_MARKER(PS_GET_CURRENT_CHECKPOINT())
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			IF DOES_ENTITY_EXIST(PS_Main.myVehicle) AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), PS_Main.myVehicle) //we're in a plane, probably daring landing.
				PS_Main.myCheckpointMgr.distFromCurCheckpoint = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), PS_Main.myCheckpointz[PS_Main.myCheckpointMgr.curIdx].position)
				IF PS_GET_CHECKPOINT_PROGRESS() < PS_GET_TOTAL_CHECKPOINTS()	
					//has player landed?
					IF PS_IS_PLAYER_NEAR_CURRENT_CHECKPOINT() OR PS_HAS_PLAYER_MISSED_CURRENT_CHECKPOINT()
						PS_INCREMENT_CHECKPOINT()
					ENDIF
				ELIF PS_GET_CHECKPOINT_PROGRESS() = PS_GET_TOTAL_CHECKPOINTS()	
					IF PS_IS_PLAYER_LANDED_AT_CURRENT_CHECKPOINT()
						PS_INCREMENT_CHECKPOINT()
					ENDIF
				ENDIF
			ELSE //we're not in a plane, its probably parachute
				PS_Main.myCheckpointMgr.distFromCurCheckpoint = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), PS_Main.myCheckpointz[PS_Main.myCheckpointMgr.curIdx].position)
				IF PS_GET_CHECKPOINT_PROGRESS() < PS_GET_TOTAL_CHECKPOINTS()	
					//has player landed?
					IF PS_IS_PLAYER_NEAR_CURRENT_CHECKPOINT() OR PS_HAS_PLAYER_MISSED_CURRENT_CHECKPOINT()
						PS_INCREMENT_CHECKPOINT()
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		BREAK
		
	CASE PS_LESSON_FLAG_COLLECTION
		
		IF PS_GET_CLEARED_CHECKPOINTS() < PS_GET_TOTAL_CHECKPOINTS() - 1 //flag number 0 is a not valid checkpoint so subrtact - 1 from total number
		
			PS_UPDATE_CLOSEST_FLAG_CHECKPOINT()
			
			IF PS_IS_CHECKPOINT_VALID(PS_Main.myCheckpointMgr.curIdx)
			
				IF PS_IS_PLAYER_NEAR_CURRENT_CHECKPOINT((STUNT_PLANE_LOCATE_SIZE_CHECKPOINT * 1.5) / 2)
					
					PS_CREATE_FLASHING_CHECKPOINT(PS_Main.myCheckpointMgr.curIdx, PS_Main.myCheckpointMgr.curIdx)
					PS_CLEAR_FLAG_CHECKPOINT(PS_Main.myCheckpointMgr.curIdx)
								
				ENDIF
				
			ENDIF
		
		ENDIF
		
		
		BREAK
		
	CASE PS_LESSON_HEIGHT_OBSTACLE_PLANE
		//track the current checkpoint 
		IS_VEHICLE_FUCKED(PS_Main.myVehicle)
		PS_Main.myCheckpointMgr.distFromCurCheckpoint = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PS_Main.myVehicle), PS_Main.myCheckpointz[PS_Main.myCheckpointMgr.curIdx].position)
		
		VECTOR vPlaneCoords
		
		vPlaneCoords = GET_ENTITY_COORDS(PS_Main.myVehicle)
		
		PS_Main.myCheckpointMgr.heightFromCurCheckpoint = PS_Main.myCheckpointz[PS_Main.myCheckpointMgr.curIdx].position.Z - vPlaneCoords.Z
		
		IF PS_Main.myCheckpointMgr.heightFromCurCheckpoint < 0
			//plane was above the checkpoint
			PS_Main.myCheckpointMgr.heightFromCurCheckpoint = ABSF(PS_Main.myCheckpointMgr.heightFromCurCheckpoint)
		ELSE
			//plane was below or at the level of checkpoint
			PS_Main.myCheckpointMgr.heightFromCurCheckpoint = 0
		ENDIF
		
		IF PS_GET_CHECKPOINT_PROGRESS() <= PS_GET_TOTAL_CHECKPOINTS()	
			IF PS_IS_PLAYER_NEAR_CURRENT_CHECKPOINT(STUNT_PLANE_LOCATE_SIZE_CHECKPOINT / 2) OR PS_HAS_PLAYER_MISSED_CURRENT_CHECKPOINT()

				IF PS_IS_PLAYER_NEAR_CURRENT_CHECKPOINT()		//if player cleared the checkpoint
					PS_Main.myCheckpointMgr.totalCheckpointHeight += PS_Main.myCheckpointMgr.heightFromCurCheckpoint
				ELIF PS_HAS_PLAYER_MISSED_CURRENT_CHECKPOINT()	//if player missed the checkpoint
					PS_Main.myCheckpointMgr.totalCheckpointHeight += PS_Main.myCheckpointMgr.distFromCurCheckpoint
				ENDIF

				PS_INCREMENT_CHECKPOINT()
								
				PS_Main.myCheckpointMgr.avgCheckpointHeight = PS_Main.myCheckpointMgr.totalCheckpointHeight / PS_GET_CLEARED_CHECKPOINTS()
			ENDIF
		ENDIF
	BREAK
		
	ENDSWITCH
		
	IF PS_IS_CHECKPOINT_VALID(PS_GET_CHECKPOINT_PROGRESS())
		IF PS_Main.myCheckpointz[PS_GET_CHECKPOINT_PROGRESS()].checkpoint != NULL
			INT HudColor_R
			INT HudColor_G
			INT HudColor_B
			INT HudColor_A
			GET_RGB_FROM_INT(GET_INT_FROM_HUD_COLOUR(PS_GET_RACE_CHECKPOINT_COLOUR(PS_Main.myCheckpointz[PS_GET_CHECKPOINT_PROGRESS()].type)), HudColor_R, HudColor_G, HudColor_B, HudColor_A)	
			SET_CHECKPOINT_RGBA(PS_Main.myCheckpointz[PS_GET_CHECKPOINT_PROGRESS()].checkpoint, HudColor_R, HudColor_G, HudColor_B, MG_GET_CHECKPOINT_ALPHA(PS_Main.myCheckpointz[PS_GET_CHECKPOINT_PROGRESS()].position))
		ENDIF
	ENDIF
	
	
	PS_CHECK_FOR_MISSED_CHECKPOINT()
	
ENDPROC

PROC PS_UPDATE_CHECKPOINT_FLASH()

	IF PS_Main.flash_checkpoint != NULL

		INT HudColor_R, HudColor_G, HudColor_B, HudColor_A

		PS_Main.iFlashCheckpointAlpha -= 25
		IF PS_Main.iFlashCheckpointAlpha > 0
			GET_HUD_COLOUR(PS_Main.eFlashHudColour, HudColor_R, HudColor_G, HudColor_B, HudColor_A)
	        SET_CHECKPOINT_RGBA(PS_Main.flash_checkpoint, HudColor_R, HudColor_G, HudColor_B, PS_Main.iFlashCheckpointAlpha)
			SET_CHECKPOINT_RGBA2(PS_Main.flash_checkpoint, HudColor_R, HudColor_G, HudColor_B, PS_Main.iFlashCheckpointAlpha)
	    ELSE
	        DELETE_CHECKPOINT(PS_Main.flash_checkpoint)
			PS_Main.flash_checkpoint = NULL
			PS_Main.bFlashCheckpointMissed = FALSE
	    ENDIF
	ENDIF

ENDPROC

PROC PS_RESET_CHECKPOINT_MGR(BOOL bResetCheckpointMarkerFlash = TRUE)
	PS_RESET_ALL_CHECKPOINTS(bResetCheckpointMarkerFlash)
	PS_Main.myCheckpointMgr.clearedCheckpoints = 0
	PS_Main.myPlayerData.CheckpointCount = PS_Main.myCheckpointMgr.clearedCheckpoints //playerdata var needs to be reset 
	PS_Main.myCheckpointMgr.totalCheckpoints = 0
	PS_Main.myCheckpointMgr.distFromCurCheckpoint = -1
	PS_Main.myCheckpointMgr.nextIdx = -1
	PS_Main.myCheckpointMgr.curIdx = -1
	PS_Main.myCheckpointMgr.isActive = FALSE
	PS_Main.myCheckpointMgr.isFinished = FALSE
	PS_Main.myCheckpointMgr.hasMissed = FALSE
	PS_Main.myCheckpointMgr.bForPreview = FALSE
	PS_Main.myCheckpointMgr.waitingForFeedback = FALSE
	PS_Main.myCheckpointMgr.lessonType = PS_LESSON_NULL
	PS_UI_RaceHud.iTimeBonus = 0

	PS_Main.myCheckpointMgr.heightFromCurCheckpoint = 0	//for dlc challenge
	PS_Main.myCheckpointMgr.totalCheckpointHeight	= 0	//for dlc challenge
	PS_Main.myCheckpointMgr.avgCheckpointHeight	= 0		//for dlc challenge

	
	IF IS_TIMER_STARTED(Heli_Landed_Timer)
		CANCEL_TIMER(Heli_Landed_Timer)
	ENDIF
	
	PS_Main.bFlashCheckpointMissed 			= FALSE
	PS_Main.bDrawParachuteCheckpointMarker	= FALSE
	PS_Main.bDrawCheckpointMarkerFlash 		= NOT bResetCheckpointMarkerFlash
ENDPROC

PROC PS_ACTIVATE_CHECKPOINT_MGR(PS_LESSON_ENUM thisLessonType, INT thisStartingCheckpointID = 1, BOOL bForPreview = FALSE)
	IF PS_Main.myCheckpointMgr.isActive = TRUE
		PRINTLN("checkpoint manager is already active!")
		EXIT
	ENDIF

	PS_Main.myCheckpointMgr.lessonType 	= thisLessonType
	PS_Main.myCheckpointMgr.bForPreview = bForPreview
	
	PS_Main.bFlashCheckpointMissed 			= FALSE
	PS_MAin.bDrawCheckpointMarkerFlash 		= FALSE
	PS_MAin.bDrawParachuteCheckpointMarker	= FALSE
	
//switch to see what type of lesson we're in
	SWITCH PS_Main.myCheckpointMgr.lessonType
	//case stunt
	CASE PS_LESSON_STUNT
		//set the current checkpoint as the first registered checkpoint
		PS_Main.myCheckpointMgr.curIdx = thisStartingCheckpointID
		//show the checkpoint
		IF PS_IS_CHECKPOINT_VALID(PS_Main.myCheckpointMgr.curIdx)
			PS_SHOW_CHECKPOINT(PS_Main.myCheckpointMgr.curIdx)
		ENDIF
		PS_Main.myCheckpointMgr.isActive = TRUE
		BREAK
	//case obstacle course
	CASE PS_LESSON_OBSTACLE_HELI
		//set the current checkpoint as the first registered checkpoint
		PS_Main.myCheckpointMgr.curIdx = thisStartingCheckpointID
		IF PS_IS_CHECKPOINT_VALID(PS_Main.myCheckpointMgr.curIdx + 1)
			PS_Main.myCheckpointMgr.nextIdx = PS_Main.myCheckpointMgr.curIdx + 1
		ELSE
			PS_Main.myCheckpointMgr.nextIdx = PS_Main.myCheckpointMgr.curIdx
		ENDIF
		//show the checkpoint
		IF PS_IS_CHECKPOINT_VALID(PS_Main.myCheckpointMgr.curIdx)
			PS_SHOW_CHECKPOINT(PS_Main.myCheckpointMgr.curIdx)
			PS_SHOW_CHECKPOINT(PS_Main.myCheckpointMgr.nextIdx, TRUE)
		ENDIF
		PS_Main.myCheckpointMgr.isActive = TRUE
		BREAK
	
	CASE PS_LESSON_OBSTACLE_PLANE
		//set the current checkpoint as the first registered checkpoint
		PS_Main.myCheckpointMgr.curIdx = thisStartingCheckpointID
		IF PS_IS_CHECKPOINT_VALID(PS_Main.myCheckpointMgr.curIdx + 1)
			PS_Main.myCheckpointMgr.nextIdx = PS_Main.myCheckpointMgr.curIdx + 1
		ELSE
			PS_Main.myCheckpointMgr.nextIdx = PS_Main.myCheckpointMgr.curIdx
		ENDIF
		//show the checkpoint
		IF PS_IS_CHECKPOINT_VALID(PS_Main.myCheckpointMgr.curIdx)
			PS_SHOW_CHECKPOINT(PS_Main.myCheckpointMgr.curIdx)
			PS_SHOW_CHECKPOINT(PS_Main.myCheckpointMgr.nextIdx, TRUE)
		ENDIF
		PS_Main.myCheckpointMgr.isActive = TRUE
		BREAK
	//case landing
	CASE PS_LESSON_LANDING
		//set the current checkpoint as the first registered checkpoint
		PS_Main.myCheckpointMgr.curIdx = thisStartingCheckpointID
		PS_Main.myCheckpointMgr.nextIdx = PS_Main.myCheckpointMgr.curIdx + 1
		//show the checkpoint
		IF PS_IS_CHECKPOINT_VALID(PS_Main.myCheckpointMgr.curIdx)
			PS_SHOW_CHECKPOINT(PS_Main.myCheckpointMgr.curIdx)
			//We want to show the next checkpoint
			IF IS_PILOT_SCHOOL_DLC()
				PS_SHOW_CHECKPOINT(PS_Main.myCheckpointMgr.nextIdx, TRUE)
			ENDIF
		ENDIF
		//B* 1175226. We don't need to show the second checkpoint in daring landing either, so just removing it completely.
//		IF PS_IS_CHECKPOINT_VALID(PS_Main.myCheckpointMgr.nextIdx)	
//			PS_SHOW_CHECKPOINT(PS_Main.myCheckpointMgr.nextIdx, TRUE)
//		ENDIF	
		PS_Main.myCheckpointMgr.isActive = TRUE
		BREAK
	
	CASE PS_LESSON_SKYDIVING
		//set the current checkpoint as the first registered checkpoint
		PS_Main.myCheckpointMgr.curIdx = thisStartingCheckpointID
		//show the checkpoint
		IF PS_IS_CHECKPOINT_VALID(PS_Main.myCheckpointMgr.curIdx)
			PS_SHOW_CHECKPOINT(PS_Main.myCheckpointMgr.curIdx)
		ENDIF
		IF PS_IS_CHECKPOINT_VALID(PS_Main.myCheckpointMgr.nextIdx)	
			PS_SHOW_CHECKPOINT(PS_Main.myCheckpointMgr.nextIdx, TRUE)
		ENDIF
		PS_Main.myCheckpointMgr.nextIdx = PS_Main.myCheckpointMgr.curIdx + 1
		PS_Main.myCheckpointMgr.isActive = TRUE
		BREAK
		
	CASE PS_LESSON_FLAG_COLLECTION
		//set the current checkpoint as the first registered checkpoint
		PS_Main.myCheckpointMgr.curIdx = thisStartingCheckpointID
		IF PS_IS_CHECKPOINT_VALID(PS_Main.myCheckpointMgr.curIdx + 1)
			PS_Main.myCheckpointMgr.nextIdx = PS_Main.myCheckpointMgr.curIdx + 1
		ELSE
			PS_Main.myCheckpointMgr.nextIdx = PS_Main.myCheckpointMgr.curIdx
		ENDIF
		//show the checkpoint
		IF PS_IS_CHECKPOINT_VALID(PS_Main.myCheckpointMgr.curIdx)
			PS_SHOW_ALL_CHECKPOINTS()
		ENDIF
		PS_Main.myCheckpointMgr.isActive = TRUE
		BREAK
		
	CASE PS_LESSON_HEIGHT_OBSTACLE_PLANE
		//set the current checkpoint as the first registered checkpoint
		PS_Main.myCheckpointMgr.curIdx = thisStartingCheckpointID
		IF PS_IS_CHECKPOINT_VALID(PS_Main.myCheckpointMgr.curIdx + 1)
			PS_Main.myCheckpointMgr.nextIdx = PS_Main.myCheckpointMgr.curIdx + 1
		ELSE
			PS_Main.myCheckpointMgr.nextIdx = PS_Main.myCheckpointMgr.curIdx
		ENDIF
		//show the checkpoint
		IF PS_IS_CHECKPOINT_VALID(PS_Main.myCheckpointMgr.curIdx)
			PS_SHOW_CHECKPOINT(PS_Main.myCheckpointMgr.curIdx)
			PS_SHOW_CHECKPOINT(PS_Main.myCheckpointMgr.nextIdx, TRUE)
		ENDIF
		PS_Main.myCheckpointMgr.isActive = TRUE
		BREAK


	ENDSWITCH
ENDPROC

