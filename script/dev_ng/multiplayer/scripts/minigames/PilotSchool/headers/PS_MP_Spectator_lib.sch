//USING "mission_titles_private.sch"
USING "minigames_helpers.sch"
//USING "pilot_school_mp_data.sch"
//USING "ps_mp_checkpoint_lib.sch"
//USING "net_spectator_cam_common.sch"
USING "Pilot_School_MP_Challenge_Helpers.sch"
USING "net_Spectator_cam.sch"

BOOL bDoingMidLessonCam
//SCRIPT_TIMER MidCamTimer

SPECTATOR_DATA_STRUCT specData

//For forsing the host to not be a spectator
INT iManageSpechost = 0
INT iDesiredHost = -1
float savedStartTime
bool bForceSpecChopperBlades
//bool bFirstTimeFade = TRUE
//Deal with the forcing the host on to a player who is not a spectator. 

enum ps_player_spec_stage
	PS_SPEC_STAGE_NULL,
	PS_SPEC_STAGE_START_FLIGHT_SCHOOL,
	PS_SPEC_STAGE_LOAD_MENU,
	PS_SPEC_STAGE_VIEWING_MENU,
	PS_SPEC_STAGE_LOADING_PREVIEW,
	PS_SPEC_STAGE_IN_PREVIEW,
	PS_SPEC_STAGE_LOADING_CHALLENGE,
	PS_SPEC_STAGE_IN_COUNTDOWN,
	PS_SPEC_STAGE_IN_CHALLENGE,
	PS_SPEC_STAGE_QUIT,
	PS_SPEC_STAGE_PASSED_LESSON,
	PS_SPEC_STAGE_FAILED_LESSON
ENDENUM



PROC PS_SPEC_HUD_DISPLAY_LESSON_NAME()
	TEXT_LABEL_23 temp
	INT iLessonID = ENUM_TO_INT(g_current_selected_dlc_PilotSchool_class)
	UPDATE_MISSION_NAME_DISPLAYING(temp, FALSE, FALSE, FALSE, FALSE, iLessonID)
ENDPROC

PROC PS_SPEC_HUD_UPDATE_COUNTDOWN()
	IF serverBD.iFlightSchoolStage = ENUM_TO_INT(PS_SPEC_STAGE_IN_COUNTDOWN) //countdown
		cprintln(debug_trevor3,"PS_SPEC_HUD_UPDATE_COUNTDOWN()")
		PS_SPEC_HUD_DISPLAY_LESSON_NAME()
		IF NOT IS_TIMER_STARTED(PS_CountDownUI.CountdownTimer)		
			IF serverBD.bPlayCountdown
				cprintln(debug_trevor3,"NOT IS_TIMER_STARTED(PS_Main.tCountdownTimer)")
				float hostCountdownTime = TO_FLOAT(serverBD.iGenericDataB) / 1000.0
				
				IF hostCountdownTime < 0.4
					hostCountdownTime = 0.0 //fudge to get the "3" to show if the timer is close enough to 3 seconds.
				ENDIF
				
				IF hostCountdownTime < 3			
					cprintln(debug_trevor3,"Start countdown at: ",hostCountdownTime)
					START_TIMER_AT(PS_CountDownUI.CountdownTimer,hostCountdownTime)								
				ENDIF
			ENDIF
		ELIF TIMER_DO_WHEN_READY(PS_CountDownUI.CountdownTimer, 0.1)
			cprintln(debug_trevor3,"TIMER_DO_WHEN_READY")
			IF IS_TIMER_STARTED(PS_CountDownUI.CountdownTimer)
				IF UPDATE_MINIGAME_COUNTDOWN_UI(PS_CountDownUI, TRUE)
					cprintln(debug_trevor3,"stop countdown")
					CANCEL_TIMER(PS_CountDownUI.CountdownTimer)							
				ENDIF
			ENDIF		
		ENDIF
	ELSE
		IF IS_TIMER_STARTED(PS_CountDownUI.CountdownTimer)
			CANCEL_TIMER(PS_CountDownUI.CountdownTimer)
		ENDIF
	ENDIF
ENDPROC

PROC PS_SPEC_UPDATE_DATA() //host updates various data for spectator
	int i
	cprintln(debug_trevor3,"PS_SPEC_UPDATE_DATA()")
	
	//set script stage. Added this as having trouble making sure we knew when to sync display certain elements or not for spectator
	// as spectator could potentially join the player at any point in their progress.
	//0 = start first time
	//1 = load menu
	//2 = displaying game menu
	//3 = loading lesson
	//4 = showing preview
	//5 = transition to lesson
	//6 = lesson countdown
	//7 = lesson playing
	//8 = lesson completed, transition to next stage
	
	switch eGameMode
		CASE PS_GAME_MODE_INTRO_SCENE
			serverBD.iFlightSchoolStage = ENUM_TO_INT(PS_SPEC_STAGE_START_FLIGHT_SCHOOL)
		BREAK
		CASE PS_GAME_MODE_LOAD_MENU
			serverBD.iFlightSchoolStage = ENUM_TO_INT(PS_SPEC_STAGE_LOAD_MENU)
		BREAK
		CASE PS_GAME_MODE_MENU
			serverBD.iFlightSchoolStage = ENUM_TO_INT(PS_SPEC_STAGE_VIEWING_MENU)
		BREAK
		CASE PS_GAME_MODE_COURSE_INIT
		FALLTHRU
		CASE PS_GAME_MODE_LOAD_PREVIEW
			serverBD.iFlightSchoolStage = ENUM_TO_INT(PS_SPEC_STAGE_LOADING_PREVIEW)
		BREAK
		CASE PS_GAME_MODE_PREVIEW
			serverBD.iFlightSchoolStage = ENUM_TO_INT(PS_SPEC_STAGE_IN_PREVIEW)
		BREAK
		CASE PS_GAME_MODE_LOAD_CHALLENGE
			serverBD.iFlightSchoolStage = ENUM_TO_INT(PS_SPEC_STAGE_LOADING_CHALLENGE)			
		BREAK
		CASE PS_GAME_MODE_CHALLENGE		
		
			serverBD.iFlightSchoolStage = ENUM_TO_INT(PS_SPEC_STAGE_IN_CHALLENGE)
		
			SWITCH g_current_selected_dlc_PilotSchool_class
				CASE PSCD_DLC_OutsideLoop
					IF PS_DLC_Outside_Loop_State < PS_DLC_OUTSIDE_LOOP_COUNTDOWN serverBD.iFlightSchoolStage = ENUM_TO_INT(PS_SPEC_STAGE_LOADING_CHALLENGE) 
					ELIF PS_DLC_Outside_Loop_State = PS_DLC_OUTSIDE_LOOP_COUNTDOWN	serverBD.iFlightSchoolStage = ENUM_TO_INT(PS_SPEC_STAGE_IN_COUNTDOWN) 
					ELIF PS_DLC_Outside_Loop_State = PS_DLC_OUTSIDE_LOOP_FAIL serverBD.iFlightSchoolStage = ENUM_TO_INT(PS_SPEC_STAGE_FAILED_LESSON)
					ELIF PS_DLC_Outside_Loop_State = PS_DLC_OUTSIDE_LOOP_SUCCESS serverBD.iFlightSchoolStage = ENUM_TO_INT(PS_SPEC_STAGE_PASSED_LESSON)
					ENDIF
				BREAK
				CASE PSCD_DLC_ChaseParachute				 
					IF PS_DLC_Chase_Parachute_State < PS_DLC_CHASE_PARACHUTE_COUNTDOWN serverBD.iFlightSchoolStage = ENUM_TO_INT(PS_SPEC_STAGE_LOADING_CHALLENGE) 
					ELIF PS_DLC_Chase_Parachute_State = PS_DLC_CHASE_PARACHUTE_SUCCESS serverBD.iFlightSchoolStage = ENUM_TO_INT(PS_SPEC_STAGE_PASSED_LESSON)
					ELIF PS_DLC_Chase_Parachute_State = PS_DLC_CHASE_PARACHUTE_FAIL serverBD.iFlightSchoolStage = ENUM_TO_INT(PS_SPEC_STAGE_FAILED_LESSON)
					ELIF PS_DLC_Chase_Parachute_State = PS_DLC_CHASE_PARACHUTE_COUNTDOWN	serverBD.iFlightSchoolStage = ENUM_TO_INT(PS_SPEC_STAGE_IN_COUNTDOWN) ENDIF
				BREAK
				CASE PSCD_DLC_CityLanding
					IF PS_DLC_City_Landing_State < PS_DLC_CITY_LANDING_COUNTDOWN serverBD.iFlightSchoolStage = ENUM_TO_INT(PS_SPEC_STAGE_LOADING_CHALLENGE) 
					ELIF PS_DLC_City_Landing_State = PS_DLC_CITY_LANDING_SUCCESS serverBD.iFlightSchoolStage = ENUM_TO_INT(PS_SPEC_STAGE_PASSED_LESSON)
					ELIF PS_DLC_City_Landing_State = PS_DLC_CITY_LANDING_FAIL serverBD.iFlightSchoolStage = ENUM_TO_INT(PS_SPEC_STAGE_FAILED_LESSON)
					ELIF PS_DLC_City_Landing_State = PS_DLC_CITY_LANDING_COUNTDOWN	serverBD.iFlightSchoolStage = ENUM_TO_INT(PS_SPEC_STAGE_IN_COUNTDOWN) ENDIF					
				BREAK
				CASE PSCD_DLC_CollectFlags
					IF PS_DLC_Collect_Flags_State < PS_DLC_COLLECT_FLAGS_COUNTDOWN serverBD.iFlightSchoolStage = ENUM_TO_INT(PS_SPEC_STAGE_LOADING_CHALLENGE) 
					ELIF PS_DLC_Collect_Flags_State = PS_DLC_COLLECT_FLAGS_SUCCESS serverBD.iFlightSchoolStage = ENUM_TO_INT(PS_SPEC_STAGE_PASSED_LESSON)
					ELIF PS_DLC_Collect_Flags_State = PS_DLC_COLLECT_FLAGS_FAIL serverBD.iFlightSchoolStage = ENUM_TO_INT(PS_SPEC_STAGE_FAILED_LESSON)
					ELIF PS_DLC_Collect_Flags_State = PS_DLC_COLLECT_FLAGS_COUNTDOWN	serverBD.iFlightSchoolStage = ENUM_TO_INT(PS_SPEC_STAGE_IN_COUNTDOWN) ENDIF
				BREAK
				CASE PSCD_DLC_Engine_failure
					IF PS_DLC_Engine_Failure_State < PS_DLC_ENGINE_COUNTDOWN serverBD.iFlightSchoolStage = ENUM_TO_INT(PS_SPEC_STAGE_LOADING_CHALLENGE) 
					ELIF PS_DLC_Engine_Failure_State = PS_DLC_ENGINE_SUCCESS serverBD.iFlightSchoolStage = ENUM_TO_INT(PS_SPEC_STAGE_PASSED_LESSON)
					ELIF PS_DLC_Engine_Failure_State = PS_DLC_ENGINE_FAIL serverBD.iFlightSchoolStage = ENUM_TO_INT(PS_SPEC_STAGE_FAILED_LESSON)
					ELIF PS_DLC_Engine_Failure_State = PS_DLC_ENGINE_COUNTDOWN	serverBD.iFlightSchoolStage = ENUM_TO_INT(PS_SPEC_STAGE_IN_COUNTDOWN) ENDIF
				BREAK
				CASE PSCD_DLC_FlyLow
					IF PS_DLC_Fly_Low_State < PS_DLC_FLY_LOW_COUNTDOWN serverBD.iFlightSchoolStage = ENUM_TO_INT(PS_SPEC_STAGE_LOADING_CHALLENGE) 
					ELIF PS_DLC_Fly_Low_State = PS_DLC_FLY_LOW_SUCCESS serverBD.iFlightSchoolStage = ENUM_TO_INT(PS_SPEC_STAGE_PASSED_LESSON)
					ELIF PS_DLC_Fly_Low_State = PS_DLC_FLY_LOW_FAIL serverBD.iFlightSchoolStage = ENUM_TO_INT(PS_SPEC_STAGE_FAILED_LESSON)
					ELIF PS_DLC_Fly_Low_State = PS_DLC_FLY_LOW_COUNTDOWN	serverBD.iFlightSchoolStage = ENUM_TO_INT(PS_SPEC_STAGE_IN_COUNTDOWN) ENDIF
				BREAK
				CASE PSCD_DLC_FollowLeader
					IF PS_DLC_Follow_Leader_State < PS_DLC_FOLLOW_LEADER_COUNTDOWN serverBD.iFlightSchoolStage = ENUM_TO_INT(PS_SPEC_STAGE_LOADING_CHALLENGE) 
					ELIF PS_DLC_Follow_Leader_State = PS_DLC_FOLLOW_LEADER_SUCCESS serverBD.iFlightSchoolStage = ENUM_TO_INT(PS_SPEC_STAGE_PASSED_LESSON)
					ELIF PS_DLC_Follow_Leader_State = PS_DLC_FOLLOW_LEADER_FAIL serverBD.iFlightSchoolStage = ENUM_TO_INT(PS_SPEC_STAGE_FAILED_LESSON)
					ELIF PS_DLC_Follow_Leader_State = PS_DLC_FOLLOW_LEADER_COUNTDOWN	serverBD.iFlightSchoolStage = ENUM_TO_INT(PS_SPEC_STAGE_IN_COUNTDOWN) ENDIF
				BREAK
				CASE PSCD_DLC_Formation
					IF PS_DLC_Formation_State < PS_DLC_FORMATION_COUNTDOWN serverBD.iFlightSchoolStage = ENUM_TO_INT(PS_SPEC_STAGE_LOADING_CHALLENGE) 
					ELIF PS_DLC_Formation_State = PS_DLC_FORMATION_SUCCESS serverBD.iFlightSchoolStage = ENUM_TO_INT(PS_SPEC_STAGE_PASSED_LESSON)
					ELIF PS_DLC_Formation_State = PS_DLC_FORMATION_FAIL serverBD.iFlightSchoolStage = ENUM_TO_INT(PS_SPEC_STAGE_FAILED_LESSON)					
					ELIF PS_DLC_Formation_State = PS_DLC_FORMATION_COUNTDOWN	serverBD.iFlightSchoolStage = ENUM_TO_INT(PS_SPEC_STAGE_IN_COUNTDOWN) ENDIF
				BREAK
				CASE PSCD_DLC_ShootingRange
					IF PS_DLC_Shooting_Range_State < PS_DLC_SHOOTING_RANGE_COUNTDOWN serverBD.iFlightSchoolStage = ENUM_TO_INT(PS_SPEC_STAGE_LOADING_CHALLENGE) 
					ELIF PS_DLC_Shooting_Range_State = PS_DLC_SHOOTING_RANGE_SUCCESS serverBD.iFlightSchoolStage = ENUM_TO_INT(PS_SPEC_STAGE_PASSED_LESSON)
					ELIF PS_DLC_Shooting_Range_State = PS_DLC_SHOOTING_RANGE_FAIL serverBD.iFlightSchoolStage = ENUM_TO_INT(PS_SPEC_STAGE_FAILED_LESSON)										
					ELIF PS_DLC_Shooting_Range_State = PS_DLC_SHOOTING_RANGE_COUNTDOWN	serverBD.iFlightSchoolStage = ENUM_TO_INT(PS_SPEC_STAGE_IN_COUNTDOWN) ENDIF
				BREAK
				CASE PSCD_DLC_VehicleLanding
					IF PS_DLC_Vehicle_Landing_State < PS_DLC_VEHICLE_LANDING_COUNTDOWN serverBD.iFlightSchoolStage = ENUM_TO_INT(PS_SPEC_STAGE_LOADING_CHALLENGE) 
					ELIF PS_DLC_Vehicle_Landing_State = PS_DLC_VEHICLE_LANDING_SUCCESS serverBD.iFlightSchoolStage = ENUM_TO_INT(PS_SPEC_STAGE_PASSED_LESSON)
					ELIF PS_DLC_Vehicle_Landing_State = PS_DLC_VEHICLE_LANDING_FAIL serverBD.iFlightSchoolStage = ENUM_TO_INT(PS_SPEC_STAGE_FAILED_LESSON)															
					ELIF PS_DLC_Vehicle_Landing_State = PS_DLC_VEHICLE_LANDING_COUNTDOWN	serverBD.iFlightSchoolStage = ENUM_TO_INT(PS_SPEC_STAGE_IN_COUNTDOWN) ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE PS_GAME_MODE_QUIT_FROM_MENU
			serverBD.iFlightSchoolStage = ENUM_TO_INT(PS_SPEC_STAGE_QUIT)
		BREAK
			//serverBD.iFlightSchoolStage = ENUM_TO_INT(PS_SPEC_STAGE_IN_COUNTDOWN) is having to be set within each lesson for the countdown
	endswitch
	
	
	IF eGameMode = PS_GAME_MODE_COURSE_INIT
	OR eGameMode = PS_GAME_MODE_LOAD_CHALLENGE
		serverBD.iGenericData = 0
	ENDIF
	
	//update best time
	IF serverBD.iFlightSchoolStage = ENUM_TO_INT(PS_SPEC_STAGE_LOADING_CHALLENGE)
	OR serverBD.iFlightSchoolStage = ENUM_TO_INT(PS_SPEC_STAGE_IN_COUNTDOWN)
		serverBD.bestTime = saveData[g_current_selected_dlc_PilotSchool_class].ElapsedTime
	ENDIF
	
	//update timer for races
	IF serverBD.iFlightSchoolStage = ENUM_TO_INT(PS_SPEC_STAGE_IN_CHALLENGE)
	
		IF IS_TIMER_STARTED(PS_Main.tRaceTimer)
			IF NOT HAS_NET_TIMER_STARTED(serverBD.raceTimer)
				RESET_NET_TIMER(serverBD.raceTimer)
				START_NET_TIMER(serverBD.raceTimer)
			ENDIF
		ELSE
			IF HAS_NET_TIMER_STARTED(serverBD.raceTimer)
				RESET_NET_TIMER(serverBD.raceTimer)
			ENDIF
		ENDIF	
	ELSE
		serverBD.iGenericDataB = 0
		savedStartTime = 0
	ENDIF
	
	IF eGameMode >= PS_GAME_MODE_LOAD_PREVIEW
	AND eGameMode <= PS_GAME_MODE_QUIT_FROM_MENU		
				
		SWITCH g_current_selected_dlc_PilotSchool_class
			CASE PSCD_DLC_ChaseParachute
			
				IF eGameMode <=PS_GAME_MODE_PREVIEW
					IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.vehicleOfInterest)
						IF NETWORK_DOES_NETWORK_ID_EXIST(PS_Main.prevPed_netID)
							serverBD.vehicleOfInterest = PS_Main.prevPed_netID
						ENDIF
					ELSE
						IF NOT NETWORK_DOES_NETWORK_ID_EXIST(PS_Main.prevPed_netID)
							serverBD.vehicleOfInterest = NULL
						ENDIF
					ENDIF
				ELSE		
				
					
					
					IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.netIDStore[0])
						IF NETWORK_DOES_NETWORK_ID_EXIST(PS_Main.myVehicle_netID)
							serverBD.netIDStore[0] = PS_Main.myVehicle_netID
						ENDIF
					ELSE
						IF serverBD.netIDStore[0] != PS_Main.myVehicle_netID
						OR NOT NETWORK_DOES_NETWORK_ID_EXIST(PS_Main.myVehicle_netID)
							serverBD.netIDStore[0] = NULL
						ENDIF
					ENDIF
				
					IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.vehicleOfInterest)
						IF NETWORK_DOES_NETWORK_ID_EXIST(ParachutePackIndex_NETID)
							serverBD.vehicleOfInterest = ParachutePackIndex_NETID
						ENDIF
					ELSE
						IF serverBD.vehicleOfInterest != ParachutePackIndex_NETID
						OR NOT NETWORK_DOES_NETWORK_ID_EXIST(ParachutePackIndex_NETID)
							serverBD.vehicleOfInterest = NULL
						ENDIF
					ENDIF
				ENDIF
				
				IF PS_DLC_Chase_Parachute_State >= PS_DLC_CHASE_PARACHUTE_JUMPING
					serverBD.iGenericDataC = 1
				ELSE
					serverBD.iGenericDataC = 0
				ENDIF
				
			BREAK
			
			CASE  PSCD_DLC_ShootingRange			
				cprintln(debug_trevor3,"update spec shooting range")
				int j
				
				//update score only on final stage
				
				IF PS_UI_RaceHud.bIsHourGlassActive = TRUE
					serverBD.iGenericData = PS_Main.targetsDestroyed
				ELSE
					serverBD.iGenericData = -1
				ENDIF
				
				IF eGameMode >= PS_GAME_MODE_PREVIEW
					IF NETWORK_DOES_NETWORK_ID_EXIST(PS_Main.myVehicle_netID)
						serverBD.vehicleOfInterest = PS_Main.myVehicle_netID
					ENDIF
				ENDIF
				
				REPEAT COUNT_OF(targetList) i				
				
				
				
					if NETWORK_DOES_NETWORK_ID_EXIST(targetList[i].entity_NETID)
					AND (DOES_BLIP_EXIST(targetList[i].blip) OR eGameMode = PS_GAME_MODE_PREVIEW)
						IF NOT IS_BIT_SET(serverBD.iGenericDataC,i) 
							//find a space to store net id
							cprintln(debug_trevor3,"#' Trying to add target : ",i)
							REPEAT COUNT_OF(serverBD.netIDStore) j
								IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.netIDStore[j])
									cprintln(debug_trevor3,"#' Add target : ",i," in tracker ",j)
									serverBD.netIDStore[j] = targetList[i].entity_NETID
									PS_spec_targetIndexTracker[i] = j
									SET_BIT(serverBD.iGenericDataC,i)
									j = COUNT_OF(serverBD.netIDStore)
								ENDIF
							ENDREPEAT
						endif
					endif
										
					if IS_BIT_SET(serverBD.iGenericDataC,i)
						//remove destroyed targets from server data (only if not in preview mode to ensure lights get displayed on unblipped targets)
						IF NOT DOES_BLIP_EXIST(targetList[i].blip)						
						AND eGameMode > PS_GAME_MODE_PREVIEW
							IF serverBD.netIDStore[PS_spec_targetIndexTracker[i]] != null
								serverBD.netIDStore[PS_spec_targetIndexTracker[i]] = null
								CLEAR_BIT(serverBD.iGenericDataC,i)
								cprintln(debug_trevor3,"#' clear bit : ",i)
								cprintln(debug_trevor3,"#' serverBD.netIDStore[",PS_spec_targetIndexTracker[i],"]=null")
							ENDIF
																																			
						endif
						/*
						if targetList[i].status < SPAWNING
						or targetList[i].status >= DESTROYED
							
							
							IF serverBD.netIDStore[PS_spec_targetIndexTracker[i]] != null
								cprintln(debug_trevor3,"#' b) serverBD.netIDStore[",PS_spec_targetIndexTracker[i],"]=null")
								serverBD.netIDStore[PS_spec_targetIndexTracker[i]] = null
							ENDIF
						endif*/
					endif
	
						
				ENDREPEAT
				cprintln(debug_trevor3,"serverBD.iGenericData = ",serverBD.iGenericData)
			BREAK		
			CASE PSCD_DLC_Formation
				cprintln(debug_Trevor3,"host: update formation net veh")
				
				//get latest stage
				int iStage,k
				for k = 0 to 11
					IF IS_BIT_SET(serverBD.iGenericData,k)
						iStage = k
					ENDIF
				endfor
				
				
				IF iStage != 0
				AND iStage != 10
					IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.vehicleOfInterest)
						cprintln(debug_Trevor3,"host: net veh doesn't exist")
						SWITCH iStage
							CASE 1 IF NETWORK_DOES_NETWORK_ID_EXIST(PS_ambVeh_NETID[1]) serverBD.vehicleOfInterest = PS_ambVeh_NETID[0] ENDIF BREAK
							CASE 2 IF NETWORK_DOES_NETWORK_ID_EXIST(PS_ambVeh_NETID[2]) serverBD.vehicleOfInterest = PS_ambVeh_NETID[2] ENDIF BREAK
							CASE 3 IF NETWORK_DOES_NETWORK_ID_EXIST(PS_ambVeh_NETID[2]) serverBD.vehicleOfInterest = PS_ambVeh_NETID[2] ENDIF BREAK
							CASE 4 IF NETWORK_DOES_NETWORK_ID_EXIST(PS_ambVeh_NETID[1]) serverBD.vehicleOfInterest = PS_ambVeh_NETID[1] ENDIF BREAK
							CASE 5 IF NETWORK_DOES_NETWORK_ID_EXIST(PS_ambVeh_NETID[0]) serverBD.vehicleOfInterest = PS_ambVeh_NETID[0] ENDIF BREAK
							CASE 6 IF NETWORK_DOES_NETWORK_ID_EXIST(PS_ambVeh_NETID[0]) serverBD.vehicleOfInterest = PS_ambVeh_NETID[0] ENDIF BREAK
							CASE 7 IF NETWORK_DOES_NETWORK_ID_EXIST(PS_ambVeh_NETID[0]) serverBD.vehicleOfInterest = PS_ambVeh_NETID[0] ENDIF BREAK
							CASE 8 IF NETWORK_DOES_NETWORK_ID_EXIST(PS_ambVeh_NETID[0]) serverBD.vehicleOfInterest = PS_ambVeh_NETID[0] ENDIF BREAK
							CASE 9 IF NETWORK_DOES_NETWORK_ID_EXIST(PS_ambVeh_NETID[0]) serverBD.vehicleOfInterest = PS_ambVeh_NETID[0] ENDIF BREAK
						ENDSWITCH
					ENDIF
				ELSE
					serverBD.vehicleOfInterest = NULL
				ENDIF
				
				//set stage...
				serverBD.iGenericData = 0
				SWITCH PS_DLC_Formation_State
					CASE PS_DLC_FORMATION_ONE SET_BIT(serverBD.iGenericData,1) BREAK
					CASE PS_DLC_FORMATION_TRAIL SET_BIT(serverBD.iGenericData,2) BREAK					
					CASE PS_DLC_FORMATION_TURN SET_BIT(serverBD.iGenericData,3) BREAK
					CASE PS_DLC_FORMATION_STACK SET_BIT(serverBD.iGenericData,4) BREAK
					CASE PS_DLC_FORMATION_BOX SET_BIT(serverBD.iGenericData,5) BREAK
					CASE PS_DLC_FORMATION_LOOP SET_BIT(serverBD.iGenericData,6) BREAK
					CASE PS_DLC_FORMATION_AFTER_LOOP SET_BIT(serverBD.iGenericData,7) BREAK
					CASE PS_DLC_FORMATION_HALF_LOOP SET_BIT(serverBD.iGenericData,8) BREAK
					CASE PS_DLC_FORMATION_FINAL SET_BIT(serverBD.iGenericData,9) BREAK
					DEFAULT
						IF PS_DLC_Formation_State < PS_DLC_FORMATION_ONE
							//change nothing
						ELSE
							SET_BIT(serverBD.iGenericData,10)
						ENDIF
					BREAK
				ENDSWITCH
				
				//update if player is in marker
				IF DOES_BLIP_EXIST(thisMovingTargetBlipFormation)
					IF GET_BLIP_COLOUR(thisMovingTargetBlipFormation) = BLIP_COLOUR_GREEN
						SET_BIT(serverBD.iGenericData,20)
					ELSE
						CLEAR_BIT(serverBD.iGenericData,20)
					ENDIF
				ENDIF
				
				
				serverBD.iGenericDataB = PS_Main.targetsDestroyed //score
				
			BREAK
			
			CASE PSCD_DLC_CityLanding
				IF PS_UI_RaceHud.bIsDistActive
					SET_BIT(serverBD.iGenericDataB,0)
				ENDIF
			BREAK
			
			CASE PSCD_DLC_FlyLow
				IF PS_UI_RaceHud.bIsAvgHeightActive
					serverBD.iGenericDataB = FLOOR(fDistanceLeft * 1000.0)
				ENDIF
			BREAK
			
			CASE PSCD_DLC_FollowLeader
				IF IS_TIMER_STARTED(PS_Main.tRaceTimer)
					IF savedStartTime = 0
						savedStartTime = PS_Main.tRaceTimer.StartTime
					ENDIF
					//update total penalty time
					serverBD.iGenericDataB = floor((savedStartTime - PS_Main.tRaceTimer.StartTime) * 1000.0)
				ELSE
					serverBD.iGenericDataB = 0
				ENDIF
			BREAK
			
			CASE PSCD_DLC_CollectFlags
				serverBD.iGenericDataB = PS_GET_CLEARED_CHECKPOINTS()				
			BREAK
			
			CASE PSCD_DLC_OutsideLoop
				IF PS_UI_ObjectiveMeter.bIsLoopMeterActive
					//only update this every 10 degrees to prevent spamming the network
					cprintln(debug_trevor3,"PS_Main.myObjectiveData.fPlaneTotalOrient = ",PS_Main.myObjectiveData.fPlaneTotalOrient)
					IF ABSI(serverBD.iGenericDataC - FLOOR(PS_Main.myObjectiveData.fPlaneTotalOrient)) > 10.0
					OR serverBD.iGenericDataC = 0
						serverBD.iGenericDataC = FLOOR(PS_Main.myObjectiveData.fPlaneTotalOrient)
					ENDIF
				ELSE
					serverBD.iGenericDataC = 0
				ENDIF
			BREAK
		ENDSWITCH
		
		IF serverBD.Missed = FALSE AND PS_Main.myCheckpointMgr.lastCheckpointScore = PS_CHECKPOINT_MISS
			serverBD.Missed = TRUE
		ELIF serverBD.Missed = TRUE AND PS_Main.myCheckpointMgr.lastCheckpointScore != PS_CHECKPOINT_MISS
			serverBD.Missed = FALSE
		ENDIF
		
	ENDIF
	
	/*
	cprintln(debug_trevor3,"serverBD")
	cprintln(debug_trevor3,serverBD.iSelectedLesson)
	cprintln(debug_trevor3,serverBD.iFlightSchoolStage)
	cprintln(debug_trevor3,serverBD.currentCheckpointType)
	cprintln(debug_trevor3,serverBD.nextCheckpointType)
	cprintln(debug_trevor3,serverBD.iGenericData)
	cprintln(debug_trevor3,serverBD.iGenericDataB)
	cprintln(debug_trevor3,serverBD.iGenericDataC)*/
	
ENDPROC

PROC PS_SPEC_RECEIVE_DATA()
	cprintln(debug_Trevor3,"PS_SPEC_RECEIVE_DATA()")
	int i
	
	IF serverBD.bestTime != saveData[g_current_selected_dlc_PilotSchool_class].ElapsedTime
		saveData[g_current_selected_dlc_PilotSchool_class].ElapsedTime = serverBD.bestTime
	ENDIF
	
	SWITCH g_current_selected_dlc_PilotSchool_class
		CASE PSCD_DLC_ChaseParachute
			IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.vehicleOfInterest)
				object_index oPara
				oPara = NET_TO_OBJ(serverBD.vehicleOfInterest)
				cprintln(debug_trevor3,"Set parachute velocity")
				SET_ENTITY_VELOCITY(oPara,serverBD.vNextCheckpoint)
			ENDIF
		BREAK
		
		CASE PSCD_DLC_ShootingRange
			
			IF IS_SCREEN_FADED_OUT()
				bForceSpecChopperBlades = FALSE
			ENDIF
			
			IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.vehicleOfInterest)
				IF IS_VEHICLE_DRIVEABLE(NET_TO_VEH(serverBD.vehicleOfInterest))
					IF NOT bForceSpecChopperBlades
						cprintln(debug_trevor3,"SET HELI BLAES FOR SPEC")
						SET_HELI_BLADES_FULL_SPEED(NET_TO_VEH(serverBD.vehicleOfInterest))
						bForceSpecChopperBlades = TRUE
					ENDIF
				ENDIF		
			ENDIF
			
			IF REQUEST_SCRIPT_AUDIO_BANK("TARGET_PRACTICE")
			ENDIF
			
			REPEAT COUNT_OF(serverBD.netIDStore) i
				IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.netIDStore[i])
					DRAW_LIGHT_WITH_RANGE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(NET_TO_OBJ(serverBD.netIDStore[i]),<<0,1,2>>),255,0,0,10.0,1.0)
					DRAW_LIGHT_WITH_RANGE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(NET_TO_OBJ(serverBD.netIDStore[i]),<<0,-0.5,2>>),255,255,255,3.0,3.0)
					IF serverBD.iFlightSchoolStage = ENUM_TO_INT(PS_SPEC_STAGE_IN_CHALLENGE)
						IF NOT DOES_BLIP_EXIST(targetList[i].blip)
							cprintln(debug_Trevor3,"Spec Add Target")						
							targetList[i].blip = CREATE_BLIP_FOR_ENTITY(NET_TO_OBJ(serverBD.netIDStore[i]),TRUE)										
							BEGIN_TEXT_COMMAND_SET_BLIP_NAME("PS_BLIP_TARGET")
							END_TEXT_COMMAND_SET_BLIP_NAME(targetList[i].blip)
							SET_BLIP_AS_FRIENDLY(targetList[i].blip, FALSE)
						ENDIF
					ENDIF
					
					targetList[i].vCoord = GET_ENTITY_COORDS(NET_TO_OBJ(serverBD.netIDStore[i]))					
				ELSE
					IF DOES_BLIP_EXIST(targetList[i].blip)						
						PLAY_SOUND_FROM_COORD(-1,"Destroyed",targetList[i].vCoord,"DLC_PILOT_Shooting_Range_Sounds")				
						cprintln(debug_Trevor3,"Spec Remove Target")
						REMOVE_BLIP(targetList[i].blip)
					ENDIF
				ENDIF
			ENDREPEAT
			
			//draw score
			//only draw score for final stage
			IF serverBD.iGenericData != -1
			//	int MeterNumber
			//	MeterNumber = /*PS_HUD_GET_HOURGLASS_TIMER_PADDING() +*/ PS_HUD_GET_HOURGLASS_TIMER_VALUE()
			//	int MeterMaxNum 
			//	MeterMaxNum = PS_HUD_GET_HOURGLASS_MAX_TIME()
			//	string MeterTitle 
			//	MeterTitle = "PS_REMAINING"
				
				IF serverBD.iFlightSchoolStage = ENUM_TO_INT(PS_SPEC_STAGE_IN_CHALLENGE) //main challenge stage
					DRAW_GENERIC_SCORE(serverBD.iGenericData, "FS_TARGS")
					IF serverBD.iGenericData < PS_Challenges[g_current_selected_dlc_PilotSchool_class].BronzeDistance
						DRAW_GENERIC_SCORE(FLOOR(PS_Challenges[g_current_selected_dlc_PilotSchool_class].BronzeDistance), "PS_HUDTARG_B",-1,HUD_COLOUR_BRONZE,HUDORDER_SECONDBOTTOM)
					ELIF serverBD.iGenericData < PS_Challenges[g_current_selected_dlc_PilotSchool_class].SilverDistance
						DRAW_GENERIC_SCORE(FLOOR(PS_Challenges[g_current_selected_dlc_PilotSchool_class].SilverDistance), "PS_HUDTARG_S",-1,HUD_COLOUR_SILVER,HUDORDER_SECONDBOTTOM)
					ELSE 
						DRAW_GENERIC_SCORE(FLOOR(PS_Challenges[g_current_selected_dlc_PilotSchool_class].GoldDistance), "PS_HUDTARG_G",-1,HUD_COLOUR_GOLD,HUDORDER_SECONDBOTTOM)
					ENDIF
				ENDIF
				//DRAW_GENERIC_METER(MeterNumber, MeterMaxNum, MeterTitle, HUD_COLOUR_RED, -1, HUDORDER_SIXTHBOTTOM, -1, -1, FALSE, TRUE)
			ENDIF		
				
		//	PS_SPEC_iMatchGenericData = serverBD.iGenericData
		BREAK
		
		

	ENDSWITCH
ENDPROC

PROC PS_SPEC_DISPLAY_HUD()
	CPRINTLN(debug_Trevor3,"PS_SPEC_DISPLAY_HUD() : stage = ",serverBD.iFlightSchoolStage)
	IF serverBD.iFlightSchoolStage = ENUM_TO_INT(PS_SPEC_STAGE_IN_CHALLENGE)
		CPRINTLN(debug_Trevor3,"In challenge")
		SWITCH g_current_selected_dlc_PilotSchool_class
			CASE PSCD_DLC_OutsideLoop
			CASE PSCD_DLC_CollectFlags
			CASE PSCD_DLC_FollowLeader
			CASE PSCD_DLC_ShootingRange
			CASE PSCD_DLC_VehicleLanding
				CPRINTLN(debug_Trevor3,"PLaying outside loop")
				IF HAS_NET_TIMER_STARTED(serverBD.raceTimer)
					CPRINTLN(debug_Trevor3,"HAS_NET_TIMER_STARTED() = TRUE")
					IF NOT IS_TIMER_STARTED(PS_Main.tRaceTimer)		
						#if IS_DEBUG_BUILD
						CPRINTLN(debug_Trevor3,"IS_TIMER_STARTED() = FALSE")
						float showTimer
						
					
						showtimer = TO_FLOAT(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(),serverBD.raceTimer.Timer))/1000.0
						cprintln(debug_trevor3,"Net timer = ",GET_TIME_AS_STRING(GET_NETWORK_TIME())," started at: ",GET_TIME_AS_STRING(serverBD.raceTimer.Timer)," di ff = ",showTimer)
						#endif
						PS_HUD_RESET_TIMER()
						START_TIMER_AT(PS_Main.tRaceTimer,TO_FLOAT(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(),serverBD.raceTimer.Timer))/1000.0)					
						savedStartTime = PS_Main.tRaceTimer.startTime
						If g_current_selected_dlc_PilotSchool_class != PSCD_DLC_ShootingRange
							PS_SET_TIMER_ACTIVE(TRUE)
							PS_UI_RaceHud.bIsHourGlassActive = FALSE
						ENDIF
						
						
						If g_current_selected_dlc_PilotSchool_class = PSCD_DLC_ShootingRange
							//PS_HUD_SET_TIMER_VISIBLE(FALSE)
							PS_START_HOURGLASS_TIMER(120000-GET_TIME_DIFFERENCE(GET_NETWORK_TIME(),serverBD.raceTimer.Timer))
						ENDIF
						IF g_current_selected_dlc_PilotSchool_class = PSCD_DLC_VehicleLanding
							PS_START_HOURGLASS_TIMER(FLOOR(PS_Challenges[PSCD_DLC_VehicleLanding].BronzeTime * 1000)-GET_TIME_DIFFERENCE(GET_NETWORK_TIME(),serverBD.raceTimer.Timer))
						ENDIF
					ELSE
					
						If g_current_selected_dlc_PilotSchool_class = PSCD_DLC_ShootingRange
							int MeterNumber
							MeterNumber = /*PS_HUD_GET_HOURGLASS_TIMER_PADDING() +*/ PS_HUD_GET_HOURGLASS_TIMER_VALUE()
							int MeterMaxNum 
							MeterMaxNum = 120000 // PS_HUD_GET_HOURGLASS_MAX_TIME()
							string MeterTitle 
							MeterTitle = "PS_REMAINING"
							DRAW_GENERIC_METER(MeterNumber, MeterMaxNum, MeterTitle, HUD_COLOUR_RED, -1, HUDORDER_SIXTHBOTTOM, -1, -1, FALSE, TRUE)
						ENDIF
						
						If g_current_selected_dlc_PilotSchool_class = PSCD_DLC_FollowLeader
							if IS_TIMER_STARTED(PS_Main.tRaceTimer)
							//time offset
							//savedStartTime = TO_FLOAT(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(),serverBD.raceTimer.Timer))/1000.0
								
								if PS_Main.tRaceTimer.startTime != savedStartTime - (to_float(serverBD.iGenericDataB) / 1000.0)
									IF (savedStartTime - PS_Main.tRaceTimer.startTime - (to_float(serverBD.iGenericDataB) / 1000.0) > 1.5
									AND savedStartTime - PS_Main.tRaceTimer.startTime - (to_float(serverBD.iGenericDataB) / 1000.0) < 2.5)
										PS_UI_RaceHud.iTimeBonus = PS_PERFECT_BONUS
										PS_HUD_UPDATE_TIME_BONUS(PS_Main.tRaceTimer,PS_UI_RaceHud.iTimeBonus)
									ELIF (savedStartTime - PS_Main.tRaceTimer.startTime - (to_float(serverBD.iGenericDataB) / 1000.0) < -4.5
									AND savedStartTime - PS_Main.tRaceTimer.startTime - (to_float(serverBD.iGenericDataB) / 1000.0) > -5.5)
										PS_UI_RaceHud.iTimeBonus = PS_SKIPPED_BONUS
										PS_HUD_UPDATE_TIME_BONUS(PS_Main.tRaceTimer,PS_UI_RaceHud.iTimeBonus)
									ELSE
										PS_Main.tRaceTimer.startTime = savedStartTime - (to_float(serverBD.iGenericDataB) / 1000.0)
									ENDIF
									/*
									IF (serverBD.iGenericDataB > 4000 AND serverBD.iGenericDataB < 6000) //apply time penalty
										PS_UI_RaceHud.iTimeBonus = PS_SKIPPED_BONUS
										PS_HUD_UPDATE_TIME_BONUS(PS_Main.tRaceTimer,PS_UI_RaceHud.iTimeBonus)
									ELIF (serverBD.iGenericDataB < -1000 AND serverBD.iGenericDataB > -3000) //apply time bonus
										PS_UI_RaceHud.iTimeBonus = PS_PERFECT_BONUS
										PS_HUD_UPDATE_TIME_BONUS(PS_Main.tRaceTimer,PS_UI_RaceHud.iTimeBonus) //spectator has prob joined mid way through lesson, so just adjust the timer.
									ELSE
										PS_Main.tRaceTimer.startTime = savedStartTime - (to_float(serverBD.iGenericDataB) / 1000.0)
									ENDIF*/
								
									cprintln(debug_Trevor3,"Adjust time = ",serverBD.iGenericDataB)
									
								endif
							endif
							//serverBD.iGenericDataB
						ENDIF
					ENDIF
				ELSE
					CPRINTLN(debug_Trevor3,"HAS_NET_TIMER_STARTED() = FALSE")
					IF IS_TIMER_STARTED(PS_Main.tRaceTimer)
						CPRINTLN(debug_Trevor3,"IS_TIMER_STARTED() = TRUE")
						CANCEL_TIMER(PS_Main.tRaceTimer)
						PS_SET_TIMER_ACTIVE(FALSE)	
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
		
		SWITCH g_current_selected_dlc_PilotSchool_class
			CASE PSCD_DLC_OutsideLoop
				cprintln(debug_trevor3,"serverBD.iGenericDataC = ",serverBD.iGenericDataC)
				IF serverBD.iGenericDataC != 0
					IF NOT PS_UI_ObjectiveMeter.bIsLoopMeterActive
						PS_SET_OUTSIDE_LOOP_METER_ACTIVE(TRUE)									
					ENDIF
					
					IF PS_Main.myObjectiveData.fPlaneTotalOrient = 0
					OR serverBD.iGenericDataC > PS_Main.myObjectiveData.fPlaneTotalOrient
						PS_Main.myObjectiveData.fPlaneTotalOrient = serverBD.iGenericDataC * 1.0					
					ELSE
						IF serverBD.iGenericDataC > 0
							IF ABSF(PS_Main.myObjectiveData.fPlaneTotalOrient - (serverBD.iGenericDataC*1.0)) < 10.0
								PS_Main.myObjectiveData.fPlaneTotalOrient += timestep() * 25.0
							ENDIF
						ENDIF
					ENDIF
				ELSE
					PS_SET_OUTSIDE_LOOP_METER_ACTIVE(FALSE)
					PS_Main.myObjectiveData.fPlaneTotalOrient = 0
				ENDIF
				
				PS_UPDATE_CALCULATIONS()
			BREAK	
			CASE PSCD_DLC_FollowLeader
			FALLTHRU
			CASE PSCD_DLC_VehicleLanding															
			FALLTHRU
			CASE PSCD_DLC_ShootingRange							
				PS_UPDATE_CALCULATIONS()			
			BREAK
			
			CASE PSCD_DLC_Engine_failure
				IF NOT HAS_PS_LESSON_ENDED()
					IF NOT PS_UI_RaceHud.bIsDistActive					
						PS_SET_DIST_HUD_ACTIVE(TRUE)
					ENDIF
				//	PS_Main.myCheckpointMgr.totalCheckpoints = 1
				//	PS_UPDATE_PLAYER_DATA(<<-2109.8767, 2918.7434, 31.8099>>)
					cprintln(debug_Trevor3,"spec hud dist = ",GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(GET_SPECTATOR_CURRENT_FOCUS_PED(),<<-2109.8767, 2918.7434, 31.8099>>))
					PS_HUD_SET_DIST_HUD(GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(GET_SPECTATOR_CURRENT_FOCUS_PED(),<<-2109.8767, 2918.7434, 31.8099>>,FALSE))
				ENDIF
			BREAK
			
			CASE PSCD_DLC_CityLanding
				IF NOT HAS_PS_LESSON_ENDED()
					IF IS_BIT_SET(serverBD.iGenericDataB,0)
						IF NOT PS_UI_RaceHud.bIsDistActive
							PS_SET_DIST_HUD_ACTIVE(TRUE)
						ENDIF
						//PS_Main.myCheckpointMgr.totalCheckpoints = 1
						//PS_UPDATE_PLAYER_DATA(<<-1969.0068, -745.3975, 0>>)
						PS_HUD_SET_DIST_HUD(GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(GET_SPECTATOR_CURRENT_FOCUS_PED(),<<-1969.0068, -745.3975, 0>>,FALSE))
					ENDIF
				ENDIF
			BREAK
			
			CASE PSCD_DLC_Formation
				IF serverBD.iGenericData > 0
					IF serverBD.iGenericDataB < PS_Challenges[g_current_selected_dlc_PilotSchool_class].BronzeDistance
						DRAW_GENERIC_SCORE(FLOOR(PS_Challenges[g_current_selected_dlc_PilotSchool_class].BronzeDistance), "PS_HUDTARG_B",-1,HUD_COLOUR_BRONZE,HUDORDER_SECONDBOTTOM)
					ELIF serverBD.iGenericDataB < PS_Challenges[g_current_selected_dlc_PilotSchool_class].SilverDistance
						DRAW_GENERIC_SCORE(FLOOR(PS_Challenges[g_current_selected_dlc_PilotSchool_class].SilverDistance), "PS_HUDTARG_S",-1,HUD_COLOUR_SILVER,HUDORDER_SECONDBOTTOM)
					ELSE 
						DRAW_GENERIC_SCORE(FLOOR(PS_Challenges[g_current_selected_dlc_PilotSchool_class].GoldDistance), "PS_HUDTARG_G",-1,HUD_COLOUR_GOLD,HUDORDER_SECONDBOTTOM)
					ENDIF
					
					DRAW_GENERIC_SCORE(serverBD.iGenericDataB, "FS_TARGS")
				ENDIF	
			BREAK
			
			CASE PSCD_DLC_CollectFlags
				IF HAS_NET_TIMER_STARTED(serverBD.raceTimer)
					IF NOT IS_TIMER_STARTED(PS_Main.tRaceTimer)								
						//float fTimerNow = GET_TIMER_IN_SECONDS(structTimer &t)
						START_TIMER_AT(PS_Main.tRaceTimer,TO_FLOAT(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(),serverBD.raceTimer.Timer))/1000.0)
						PS_HUD_SET_GUTTER_ICON_ACTIVE(TRUE)
						PS_SET_TIMER_ACTIVE(TRUE)					
					ENDIF
				ELSE
					IF IS_TIMER_STARTED(PS_Main.tRaceTimer)
						CPRINTLN(debug_trevor3,"SPEC: Stop timer")
						CANCEL_TIMER(PS_Main.tRaceTimer)
						PS_HUD_SET_GUTTER_ICON_ACTIVE(FALSE)
						PS_SET_TIMER_ACTIVE(FALSE)
					ENDIF
				ENDIF
								
				DRAW_CHECKPOINT_HUD(serverBD.iGenericDataB, 30, "PS_DLC_HUD_FLAGS", HUD_COLOUR_WHITE, -1)
				
				PS_UPDATE_CALCULATIONS()
			BREAK
			
			CASE PSCD_DLC_FlyLow
			
				
				IF TO_FLOAT(serverBD.iGenericDataB) / 1000.0 > PS_Challenges[g_current_selected_dlc_PilotSchool_class].BronzeDistance
					DRAW_GENERIC_SCORE(0, "PS_HUDTARG_B",-1,HUD_COLOUR_BRONZE,HUDORDER_SECONDBOTTOM, DEFAULT, "PS_METRE", TRUE, PS_Challenges[g_current_selected_dlc_PilotSchool_class].BronzeDistance)
				ELIF TO_FLOAT(serverBD.iGenericDataB) / 1000.0 > PS_Challenges[g_current_selected_dlc_PilotSchool_class].SilverDistance
					DRAW_GENERIC_SCORE(0, "PS_HUDTARG_S",-1,HUD_COLOUR_SILVER,HUDORDER_SECONDBOTTOM, DEFAULT, "PS_METRE", TRUE, PS_Challenges[g_current_selected_dlc_PilotSchool_class].SilverDistance)				
				ELSE
					DRAW_GENERIC_SCORE(0, "PS_HUDTARG_G",-1,HUD_COLOUR_GOLD,HUDORDER_SECONDBOTTOM, DEFAULT, "PS_METRE", TRUE, PS_Challenges[g_current_selected_dlc_PilotSchool_class].GoldDistance)
				ENDIF
												
				IF NOT PS_UI_RaceHud.bIsAvgHeightActive	
					PS_SET_AVG_HEIGHT_HUD_ACTIVE(TRUE)
				ENDIF
				
				PS_HUD_SET_AVG_HEIGHT_HUD(TO_FLOAT(serverBD.iGenericDataB) / 1000.0)
			BREAK
		ENDSWITCH
	
		IF PS_UI_RaceHud.bIsTimerVisible
			PS_UPDATE_RACE_HUD_ELEMENT()	
		ENDIF
		
		IF PS_UI_RaceHud.bIsDistActive
			PS_HUD_UPDATE_DIST_HUD()	
		ENDIF
		
		IF PS_UI_RaceHud.bIsAvgHeightActive
			PS_HUD_UPDATE_AVG_HEIGHT_HUD()	
		ENDIF
			
		IF PS_UI_ObjectiveMeter.bIsObjMeterVisible
			PS_HUD_UPDATE_OBJECTIVE_METER()
		ENDIF
		
		IF bIsAltimeterVisible
			PS_HUD_UPDATE_ALITMETER()
		ENDIF
		
		IF bIsAltitudeIndicatorVisible
			PS_HUD_UPDATE_ALTITUDE_INDICATOR()
		ENDIF
		//PS_HUD_UPDATE() //removing cos this was calling other unwanted shit.
	ELSE
		IF IS_TIMER_STARTED(PS_Main.tRaceTimer)
			CPRINTLN(debug_trevor3,"SPEC: Stop timer")
			CANCEL_TIMER(PS_Main.tRaceTimer)
			PS_HUD_SET_GUTTER_ICON_ACTIVE(FALSE)
			PS_SET_TIMER_ACTIVE(FALSE)
		ENDIF	
		
	//	IF serverBD.iFlightSchoolStage > 7
			PS_HUD_SET_GUTTER_ICON_ACTIVE(FALSE)
			PS_SET_DIST_HUD_ACTIVE(FALSE)
			PS_SET_AVG_HEIGHT_HUD_ACTIVE(FALSE)
			PS_SET_TIMER_ACTIVE(FALSE)
			PS_SET_CHECKPOINT_COUNTER_ACTIVE(FALSE)
			PS_HUD_SET_SCORECARD_ACTIVE(TRUE)
			PS_SET_ROLL_METER_ACTIVE(FALSE)
			PS_SET_LOOP_METER_ACTIVE(FALSE)
			PS_SET_IMMELMAN_METER_ACTIVE(FALSE)
			PS_HUD_SET_OBJECTIVE_METER_VISIBLE(FALSE)
			PS_STOP_HOURGLASS_TIMER()
			PS_SET_ALTIMETER_ACTIVE(FALSE)
		//ENDIF
	ENDIF
	
	IF IS_TIMER_STARTED(PS_Main.tRaceTimer)
		CPRINTLN(debug_trevor3,"SPEC:  PS_Main.tRaceTimer = ",GET_TIMER_IN_SECONDS(PS_Main.tRaceTimer))
	ENDIF
	
ENDPROC

PROC PS_SPEC_UPDATE_CHECKPOINTS() //update by hostr
	int iHostcheck
	CPRINTLN(debug_trevor3,"PS_SPEC_UPDATE_CHECKPOINTS()")
	IF eGameMode >= PS_GAME_MODE_LOAD_PREVIEW
	AND eGameMode <= PS_GAME_MODE_QUIT_FROM_MENU
		CPRINTLN(debug_trevor3,"PS_SPEC_UPDATE_CHECKPOINTS() playing challenege")
		IF (eGameMode = PS_GAME_MODE_LOAD_PREVIEW AND eGameSubState >= PS_SUBSTATE_UPDATE)
		OR eGameMode = PS_GAME_MODE_PREVIEW
			cprintln(debug_trevor3,"#!5 bPlayingPreview = TRUE")
			serverBD.bPlayingPreview = TRUE
		ELSE
			cprintln(debug_trevor3,"#!5 bPlayingPreview = FALSE")
			serverBD.bPlayingPreview = FALSE
		ENDIF
		
		SWITCH g_current_selected_dlc_PilotSchool_class
			CASE  PSCD_DLC_CollectFlags
			
				cprintln(debug_trevor3,"Host. Cleared checkpoints: ",PS_GET_CLEARED_CHECKPOINTS()," PS_SPEC_HOST_iCheckCount: ",PS_SPEC_HOST_iCheckCount)
				
				IF IS_SCREEN_FADED_OUT()
					SERVERBD.PS_SPEC_iCheckpointIndex = -1
					PS_SPEC_HOST_iCheckCount = -1
				ENDIF
				
				IF (PS_GET_CLEARED_CHECKPOINTS() > 0 AND PS_SPEC_HOST_iCheckCount != PS_GET_CLEARED_CHECKPOINTS())
				OR (PS_SPEC_HOST_iCheckCount = -1 AND PS_Main.myCheckpointz[1].checkpoint != NULL)
					PS_SPEC_HOST_iCheckCount = PS_GET_CLEARED_CHECKPOINTS()
					cprintln(debug_trevor3,"Host. Update checkpoints")
					SERVERBD.PS_SPEC_iCheckpointIndex = 0
					For iHostcheck = 0 to 30
						
						IF PS_Main.myCheckpointz[iHostcheck].checkpoint != null
						AND NOT IS_BIT_SET(SERVERBD.PS_SPEC_iCheckpointIndex,iHostcheck)
							cprintln(debug_trevor3,"Host. Checkpoint: ",iHostcheck," exists")
							SET_BIT(SERVERBD.PS_SPEC_iCheckpointIndex,iHostcheck)							
						ENDIF
						
						IF PS_Main.myCheckpointz[iHostcheck].checkpoint = null
						AND IS_BIT_SET(SERVERBD.PS_SPEC_iCheckpointIndex,iHostcheck)
							cprintln(debug_trevor3,"Host. Checkpoint: ",iHostcheck," void")
							CLEAR_BIT(SERVERBD.PS_SPEC_iCheckpointIndex,iHostcheck)
						ENDIF
					ENDFOR
					
					
				ENDIF
			BREAK
			CASE PSCD_DLC_VehicleLanding	
				IF eGameMode = PS_GAME_MODE_LOAD_PREVIEW
					serverBD.vehicleOfInterest = null
				ELSE
					
					IF PS_FlatBedTrailer_NETID != NULL
						IF NOT NETWORK_DOES_NETWORK_ID_EXIST(PS_FlatBedTrailer_NETID)
							CPRINTLN(debug_trevor3,"HOST: reset trailer")
							PS_FlatBedTrailer_NETID = NULL
						ENDIF
					ENDIF
				
					IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.vehicleOfInterest)					
						CPRINTLN(debug_trevor3,"HOST: net index not set for trailer")
						IF NETWORK_DOES_NETWORK_ID_EXIST(PS_FlatBedTrailer_NETID)
							CPRINTLN(debug_trevor3,"HOST: set net index for trailer")
							serverBD.vehicleOfInterest = PS_FlatBedTrailer_NETID
						ENDIF
					ENDIF														
				ENDIF							
			BREAK
			CASE PSCD_DLC_ChaseParachute //special case for parachute landing target
				IF PS_DLC_Chase_Parachute_State = PS_DLC_CHASE_PARACHUTE_LANDING
					serverBD.iGenericData = 2
				ELIF PS_DLC_Chase_Parachute_State = PS_DLC_CHASE_PARACHUTE_SKYDIVING
					serverBD.iGenericData = 1
				ELSE
					serverBD.iGenericData = 0
				ENDIF
			BREAK
			DEFAULT
				CPRINTLN(debug_trevor3,"PS_SPEC_UPDATE_CHECKPOINTS() default checkpoint")
				PS_SPEC_HOST_iCheckCount = -1
				
				vector vThisCheckpoint
				
				//IF PS_IS_CHECKPOINT_VALID(PS_Main.myCheckpointMgr.nextIdx)
				IF (PS_Main.myCheckpointMgr.nextIdx >= 0 AND PS_Main.myCheckpointMgr.nextIdx < COUNT_OF(PS_Main.myCheckpointz))
				AND PS_Main.myCheckpointz[PS_Main.myCheckpointMgr.nextIdx].checkpoint != NULL
					vThisCheckpoint = PS_GET_NEXT_CHECKPOINT()
					IF ARE_VECTORS_EQUAL(vThisCheckpoint,PS_GET_CURRENT_CHECKPOINT())
					OR PS_Main.myCheckpointMgr.nextIdx = PS_Main.myCheckpointMgr.curIdx
						serverBD.vNextCheckpoint = <<0,0,0>>
					ELSE
						IF NOT ARE_VECTORS_EQUAL(vThisCheckpoint,serverBD.vNextCheckpoint)
							CPRINTLN(debug_trevor3,"***Checkpoint update*** : revise next checkpoint")
							serverBD.nextCheckpointType = enum_to_int(PS_Main.myCheckpointz[PS_Main.myCheckpointMgr.nextIdx].type)			
							serverBD.vNextCheckpoint = vThisCheckpoint
						ELSE
							//CPRINTLN(debug_trevor3,"***Checkpoint update*** : do nothing B")
						ENDIF
					ENDIF
				ELSE
					IF NOT IS_VECTOR_ZERO(serverBD.vNextCheckpoint)
						CPRINTLN(debug_trevor3,"***Checkpoint update*** : reset next checkpoint")
						serverBD.vNextCheckpoint = <<0,0,0>>
					ENDIF
				ENDIF

				//IF PS_IS_CHECKPOINT_VALID(PS_Main.myCheckpointMgr.curIdx)
				IF (PS_Main.myCheckpointMgr.curIdx >= 0 AND PS_Main.myCheckpointMgr.curIdx < COUNT_OF(PS_Main.myCheckpointz))
				AND (PS_Main.myCheckpointz[PS_Main.myCheckpointMgr.curIdx].checkpoint != NULL)
					vThisCheckpoint = PS_GET_CURRENT_CHECKPOINT()
					CPRINTLN(debug_trevor3,"PS_GET_CURRENT_CHECKPOINT() = ",vThisCheckpoint," serverBD.vCurrentCheckpoint = ",serverBD.vCurrentCheckpoint)
					IF NOT ARE_VECTORS_EQUAL(vThisCheckpoint,serverBD.vCurrentCheckpoint)
						CPRINTLN(debug_trevor3,"***Checkpoint update*** : revise current checkpoint")
						serverBD.currentCheckpointType = enum_to_int(PS_Main.myCheckpointz[PS_Main.myCheckpointMgr.curIdx].type)	
						serverBD.vCurrentCheckpoint = vThisCheckpoint
					ELSE
						//CPRINTLN(debug_trevor3,"***Checkpoint update*** : do nothing A")
					ENDIF
				ELSE
					IF NOT IS_VECTOR_ZERO(serverBD.vCurrentCheckpoint)
						CPRINTLN(debug_trevor3,"***Checkpoint update*** : reset current checkpoint")
						serverBD.vCurrentCheckpoint = <<0,0,0>>
					ENDIF
				ENDIF
				
				vector vPrint
				CPRINTLN(debug_trevor3,"PS_Main.myCheckpointMgr.curIdx = ",PS_Main.myCheckpointMgr.curIdx," PS_Main.myCheckpointMgr.nextIdx = ",PS_Main.myCheckpointMgr.nextIdx)
				vPrint = PS_GET_CURRENT_CHECKPOINT()
				CPRINTLN(debug_Trevor3,"PS_GET_CURRENT_CHECKPOINT() = ",vPrint)
				
				CPRINTLN(debug_trevor3,"checkpoint 0 : ",native_to_int(PS_Main.myCheckpointz[0].checkpoint)," position = ",PS_Main.myCheckpointz[0].position)
				CPRINTLN(debug_trevor3,"checkpoint 1 : ",native_to_int(PS_Main.myCheckpointz[1].checkpoint)," position = ",PS_Main.myCheckpointz[1].position)
				CPRINTLN(debug_trevor3,"checkpoint 2 : ",native_to_int(PS_Main.myCheckpointz[2].checkpoint)," position = ",PS_Main.myCheckpointz[2].position)
				CPRINTLN(debug_trevor3,"checkpoint 3 : ",native_to_int(PS_Main.myCheckpointz[3].checkpoint)," position = ",PS_Main.myCheckpointz[3].position)
				CPRINTLN(debug_trevor3,"checkpoint 4 : ",native_to_int(PS_Main.myCheckpointz[4].checkpoint)," position = ",PS_Main.myCheckpointz[4].position)
			BREAK		
		ENDSWITCH
	ELSE
		serverBD.iGenericData = 0
	ENDIF
ENDPROC

FUNC CHECKPOINT_INDEX PS_SPEC_create_checkpoint(PS_CHECKPOINT_ENUM thisCheckpointType,vector vCoord,vector vNextCheckpoint,BLIP_INDEX &thisBlip) 
	INT HudColor_R, HudColor_G, HudColor_B, HudColor_A
	float fCheckpointScale
	CHECKPOINT_INDEX tmp
	int iBlipColor
	STRING BlipName
	FLOAT scale = 1.2
	
	iBlipColor = ENUM_TO_INT(PS_GET_RACE_BLIP_COLOUR_FROM_CHECKPOINT(thisCheckpointType))
	GET_RGB_FROM_INT(GET_INT_FROM_HUD_COLOUR(PS_GET_RACE_CHECKPOINT_COLOUR(thisCheckpointType)), HudColor_R, HudColor_G, HudColor_B, HudColor_A)
	
	IF thisCheckpointType = PS_CHECKPOINT_FLAG
		fCheckpointScale = 15.0
		scale = STUNT_PLANE_NEXT_BLIP_SCALE
	ELSE
		fCheckpointScale = 10.0
	ENDIF
	
	
	IF IS_VECTOR_ZERO(vNextCheckpoint)	
		ped_index observedPEd = GET_SPECTATOR_CURRENT_FOCUS_PED()
		IF NOT IS_PED_INJURED(observedPed)
			vector vGap
			vGap = vCoord - GET_ENTITY_COORDS(observedPed)
			vNextCheckpoint = vCoord + vGap
		ENDIF
	ENDIF

	BlipName = "PS_BLPNAME"

	SWITCH thisCheckpointType
		CASE PS_CHECKPOINT_SPECIAL 
			//flashes = TRUE
			//just create the blip. no checkpoint
			BREAK
		CASE PS_CHECKPOINT_OBSTACLE_PLANE
			tmp = CREATE_CHECKPOINT(CHECKPOINT_RACE_AIR_CHEVRON_1, vCoord,vNextCheckpoint, fCheckpointScale, HudColor_R, HudColor_G, HudColor_B, 100)
			BREAK
		CASE PS_CHECKPOINT_OBSTACLE_HELI
			tmp = CREATE_CHECKPOINT(CHECKPOINT_RACE_AIR_CHEVRON_1, vCoord,vNextCheckpoint, fCheckpointScale, HudColor_R, HudColor_G, HudColor_B, 100)
			BREAK
		CASE PS_CHECKPOINT_STUNT_INVERTED
			tmp = CREATE_CHECKPOINT(CHECKPOINT_PLANE_INVERTED, vCoord,vNextCheckpoint, fCheckpointScale, HudColor_R, HudColor_G, HudColor_B, 100)
			//set_checkpoint
			BlipName = "PS_BLPINVRT"
		BREAK
		CASE PS_CHECKPOINT_STUNT_SIDEWAYS_L
			tmp = CREATE_CHECKPOINT(CHECKPOINT_PLANE_SIDE_L, vCoord,vNextCheckpoint, fCheckpointScale,  HudColor_R, HudColor_G, HudColor_B, 100)
			BlipName = "PS_BLPKNIFE"
			BREAK
		CASE PS_CHECKPOINT_STUNT_SIDEWAYS_R
			tmp = CREATE_CHECKPOINT(CHECKPOINT_PLANE_SIDE_R, vCoord,vNextCheckpoint, fCheckpointScale,  HudColor_R, HudColor_G, HudColor_B, 100)
			BlipName = "PS_BLPKNIFE"
			BREAK
		CASE PS_CHECKPOINT_STUNT_LEFT
			tmp = CREATE_CHECKPOINT(CHECKPOINT_PLANE_SIDE_L, vCoord,vNextCheckpoint, fCheckpointScale,  HudColor_R, HudColor_G, HudColor_B, 100)
			BlipName = "PS_BLPKNIFE"
			BREAK
		CASE PS_CHECKPOINT_STUNT_RIGHT
			tmp = CREATE_CHECKPOINT(CHECKPOINT_PLANE_SIDE_R, vCoord,vNextCheckpoint, fCheckpointScale,  HudColor_R, HudColor_G, HudColor_B, 100)
			BlipName = "PS_BLPKNIFE"
			BREAK
		CASE PS_CHECKPOINT_FINISH
			tmp = CREATE_CHECKPOINT(CHECKPOINT_RACE_AIR_FLAG, vCoord,vNextCheckpoint, fCheckpointScale,  HudColor_R, HudColor_G, HudColor_B, 100)
			BREAK
		CASE PS_CHECKPOINT_LANDING
			tmp = CREATE_CHECKPOINT(CHECKPOINT_RACE_AIR_FLAG, vCoord,vNextCheckpoint, fCheckpointScale,  HudColor_R, HudColor_G, HudColor_B, 100)
		BREAK
		CASE PS_CHECKPOINT_FLAG
			BlipName = "PS_BLIP_FLAG"
			//fCheckpointScale *= 1.5
			tmp = CREATE_CHECKPOINT(CHECKPOINT_RACE_AIR_FLAG, vCoord,vNextCheckpoint, fCheckpointScale,  HudColor_R, HudColor_G, HudColor_B, 100)
		BREAK
		CASE PS_CHECKPOINT_HEIGHT_OBSTACLE_PLANE
			tmp = CREATE_CHECKPOINT(CHECKPOINT_RACE_AIR_CHEVRON_1, vCoord,vNextCheckpoint, fCheckpointScale, HudColor_R, HudColor_G, HudColor_B, 100)
		BREAK
	ENDSWITCH
	
	
	thisBlip = CREATE_BLIP_FOR_COORD(vCoord)
	SET_BLIP_SCALE(thisBlip, scale)
//	SET_BLIP_DIM(PS_Main.myCheckpointz[thisIdx].blip, FALSE)
	SET_BLIP_COLOUR(thisBlip, iBlipColor)
	
	IF thisCheckpointType = PS_CHECKPOINT_FINISH
		SET_BLIP_SPRITE(thisBlip, RADAR_TRACE_RACEFLAG)
		BEGIN_TEXT_COMMAND_SET_BLIP_NAME( "PS_sFINISH" )
		END_TEXT_COMMAND_SET_BLIP_NAME(thisBlip)
	ELSE
		BEGIN_TEXT_COMMAND_SET_BLIP_NAME(BlipName)
		END_TEXT_COMMAND_SET_BLIP_NAME(thisBlip)
	ENDIF

	RETURN tmp
ENDFUNC



PROC PS_TIME_WEATHER(int hours, int mins)
	INT iWeather = GET_WEATHER_FOR_PS_CHALLENGE()
	SET_WEATHER_FOR_FMMC_MISSION(iWeather) 
	NETWORK_OVERRIDE_CLOCK_TIME(hours, mins, 0)
ENDPROC

PROC PS_SET_TIME_AND_WEATHER()
	SWITCH g_current_selected_dlc_PilotSchool_class
		cASE PSCD_DLC_OutsideLoop PS_TIME_WEATHER(12,0)		BREAK
		cASE PSCD_DLC_FollowLeader PS_TIME_WEATHER(14,0)	BREAK
		cASE PSCD_DLC_VehicleLanding PS_TIME_WEATHER(18,0)	BREAK
		cASE PSCD_DLC_CollectFlags PS_TIME_WEATHER(15,0)	BREAK
		cASE PSCD_DLC_Engine_failure PS_TIME_WEATHER(11,0)	BREAK
		cASE PSCD_DLC_ChaseParachute PS_TIME_WEATHER(16,0)	BREAK
		cASE PSCD_DLC_FlyLow PS_TIME_WEATHER(11,0)			BREAK
		cASE PSCD_DLC_ShootingRange PS_TIME_WEATHER(5,0)	BREAK
		cASE PSCD_DLC_CityLanding PS_TIME_WEATHER(2,0)		BREAK
		cASE PSCD_DLC_Formation PS_TIME_WEATHER(6,0)		BREAK
	ENDSWITCH
ENDPROC



PROC PS_SPEC_VIEW_CHECPOINTS()	

	int iCheck

	IF IS_VECTOR_ZERO(serverBD.vCurrentCheckpoint)
		IF PS_SPEC_currentCheckpoint != NULL
			PS_CREATE_FLASHING_CHECKPOINT_FOR_SPECTATOR(PS_SPEC_vCurrentCheckpoint,PS_SPEC_vNextCheckpoint,PS_currentType,serverBD.Missed)
			DELETE_CHECKPOINT(PS_SPEC_currentCheckpoint)
			REMOVE_BLIP(PS_SPEC_currentBlip)
			CPRINTLN(debug_trevor3,"***Checkpoint update SPEC*** : delete current checkpoint")
			PS_SPEC_currentCheckpoint= NULL
		ENDIF
		
		IF NOT IS_VECTOR_ZERO(PS_SPEC_vCurrentCheckpoint)
			PS_SPEC_vCurrentCheckpoint = <<0,0,0>>
		ENDIF				
	ENDIF
	
	IF IS_VECTOR_ZERO(serverBD.vNextCheckpoint)
		IF PS_SPEC_nextCheckpoint != NULL
			DELETE_CHECKPOINT(PS_SPEC_nextCheckpoint)
			REMOVE_BLIP(PS_SPEC_nextBlip)
			CPRINTLN(debug_trevor3,"***Checkpoint update SPEC*** : delete next checkpoint")
			PS_SPEC_nextCheckpoint = NULL
		ENDIF
		
		IF NOT IS_VECTOR_ZERO(PS_SPEC_vNextCheckpoint)
			PS_SPEC_vNextCheckpoint = <<0,0,0>>
		ENDIF	
	ENDIF

		
	SWITCH g_current_selected_dlc_PilotSchool_class
		CASE PSCD_DLC_CollectFlags
			IF NOT bCheckpointsRegistered
				cprintln(debug_trevor3,"SPEC: register checkpoints")
				bCheckpointsRegistered = TRUE
				PS_RESET_CHECKPOINT_MGR()
			
				PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-934.0000, -1276.0000, 33.0000>>)
				PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-633.0000, -1152.0000, 36.0000>>)
				PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-409.0000, -1070.0000, 60.0000>>)
				PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-797.0000, -736.0000, 65.0000>>)
				PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-669.0000, -931.0000, 55.0000>>)
				PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-521.0000, -707.0000, 52.5000>>)
				PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-590.0000, -718.0000, 137.0000>>)
				PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-292.0000, -963.0000, 151.5000>>)
				PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-304.0000, -815.0000, 105.0000>>)
				PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-222.0000, -833.0000, 135.0000>>)
				PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-447.0000, -892.0000, 63.5000>>)
				PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-410.0000, -1218.0000, 64.0000>>)
				PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-144.0000, -594.0000, 220.0000>>)
				PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-3.0000, -690.0000, 260.0000>>)
				PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-75.0000, -819.0000, 333.0000>>)
				PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-171.0000, -1003.0000, 287.0000>>)
				PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-795.0000, -1190.0000, 60.0000>>)
				PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-528.0000, -1211.0000, 32.0000>>)
				PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-282.0000, -1063.0000, 89.0000>>)
				PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-284.0000, -445.0000, 96.0000>>)
				PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-144.0000, -770.0000, 51.5000>>)
				PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-269.0000, -773.0000, 65.0000>>)
				PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-675.0000, -590.0000, 80.0000>>)
				PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-73.0000, -875.0000, 50.0000>>)
				PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<78.0000, -832.0000, 98.0000>>)
				PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-110.0000, -1051.0000, 60.0000>>)
				PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-12.0000, -588.0000, 161.0000>>)
				PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<0.0000, -1001.0000, 106.5000>>)
				PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-804.0000, -1357.0000, 25.5000>>)
				PS_REGISTER_CHECKPOINT(PS_CHECKPOINT_FLAG, <<-949.0000, -1684.0000, 104.0000>>)
				PS_SPEC_iCheckpointIndex = 0
			ENDIF
			
			cprintln(debug_trevor3,"SPEC: PS_SPEC_iCheckpointIndex = ",PS_SPEC_iCheckpointIndex," serverBD.PS_SPEC_iCheckpointIndex = ",serverBD.PS_SPEC_iCheckpointIndex)
			IF PS_SPEC_iCheckpointIndex != serverBD.PS_SPEC_iCheckpointIndex //player collect flag index is different to spec
			AND SERVERBD.PS_SPEC_iCheckpointIndex != -1
				PS_SPEC_iCheckpointIndex = serverBD.PS_SPEC_iCheckpointIndex
				cprintln(debug_trevor3,"Spec update checkpoints")
				For iCheck = 0 to 30							
					IF PS_Main.myCheckpointz[iCheck].checkpoint != null
					AND NOT IS_BIT_SET(PS_SPEC_iCheckpointIndex,iCheck)
						cprintln(debug_trevor3,"SPEC: Delete spec checkpoint")
						PS_CREATE_FLASHING_CHECKPOINT_FOR_SPECTATOR(PS_Main.myCheckpointz[iCheck].position,<<0,0,0>>,enum_to_int(PS_CHECKPOINT_FLAG),FALSE)
						
						DELETE_CHECKPOINT(PS_Main.myCheckpointz[iCheck].checkpoint)								
						REMOVE_BLIP(PS_Main.myCheckpointz[iCheck].blip)
						PS_Main.myCheckpointz[iCheck].checkpoint = null
					ENDIF
					
					IF PS_Main.myCheckpointz[iCheck].checkpoint = null
					AND IS_BIT_SET(PS_SPEC_iCheckpointIndex,iCheck)
						cprintln(debug_trevor3,"SPEC: Add spec checkpoint")
						PS_Main.myCheckpointz[iCheck].checkpoint = PS_SPEC_create_checkpoint(PS_Main.myCheckpointz[iCheck].type,PS_Main.myCheckpointz[iCheck].position,<<0,0,0>>,PS_Main.myCheckpointz[iCheck].blip)							
						 
						
					ENDIF
				ENDFOR
			ENDIF
		
		BREAK
		
		CASE PSCD_DLC_ChaseParachute
			SWITCH serverBD.iGenericData
				CASE 1 //chasing parachute
					cprintln(debug_trevor3,"chasing parachute")
					IF NOT DOES_BLIP_EXIST(thisMovingTargetBlipFormation)
						if NETWORK_DOES_NETWORK_ID_EXIST(serverBD.vehicleOfInterest)
							thisMovingTargetBlipFormation = CREATE_BLIP_FOR_OBJECT(NET_TO_OBJ(serverBD.vehicleOfInterest))
							BEGIN_TEXT_COMMAND_SET_BLIP_NAME("PS_BLIP_PARA")
							END_TEXT_COMMAND_SET_BLIP_NAME(thisMovingTargetBlipFormation)
						ENDIF
					ENDIF
					
					if NETWORK_DOES_NETWORK_ID_EXIST(serverBD.vehicleOfInterest)
					
						//IF HAS_PED_GOT_WEAPON(GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_SPECTATOR_CURRENT_FOCUS_PED())), GADGETTYPE_PARACHUTE)
							cprintln(debug_trevor3,"setting parachute component on player spec")
						IF NOT IS_PED_INJURED(GET_SPECTATOR_CURRENT_FOCUS_PED())					
							SET_PED_COMPONENT_VARIATION(GET_SPECTATOR_CURRENT_FOCUS_PED(), PED_COMP_HAND, 0, 0)
							SET_PED_COMPONENT_VARIATION(GET_SPECTATOR_CURRENT_FOCUS_PED(), INT_TO_ENUM(PED_COMPONENT,9), 0, 0, 0) //(task)
							SET_PLAYER_PARACHUTE_VARIATION_OVERRIDE(NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_SPECTATOR_CURRENT_FOCUS_PED()), PED_COMP_HAND,0)						
						ENDIF
						//ENDIF
					ENDIF
				BREAK
			
				CASE 2 //parachuting
					DRAW_MARKER(MARKER_RING, <<-1744.80, 170.50, 64.40>>, <<0,0,1>>, <<0,0,0>>, <<PS_PARA_TARG_SCL_S, PS_PARA_TARG_SCL_S, PS_PARA_TARG_SCL_S>>, PS_PARA_TARG_COL_R, PS_PARA_TARG_COL_G, PS_PARA_TARG_COL_B, MG_GET_CHECKPOINT_ALPHA(<<-1744.80, 170.50, 64.40>>, 100), FALSE, FALSE)
					DRAW_MARKER(MARKER_RING, <<-1744.80, 170.50, 64.40>>, <<0,0,1>>, <<0,0,0>>, <<PS_PARA_TARG_SCL_M, PS_PARA_TARG_SCL_M, PS_PARA_TARG_SCL_M>>, PS_PARA_TARG_COL_R, PS_PARA_TARG_COL_G, PS_PARA_TARG_COL_B, MG_GET_CHECKPOINT_ALPHA(<<-1744.80, 170.50, 64.40>>, 100), FALSE, FALSE)
					DRAW_MARKER(MARKER_RING, <<-1744.80, 170.50, 64.40>>, <<0,0,1>>, <<0,0,0>>, <<PS_PARA_TARG_SCL_L, PS_PARA_TARG_SCL_L, PS_PARA_TARG_SCL_L>>, PS_PARA_TARG_COL_R, PS_PARA_TARG_COL_G, PS_PARA_TARG_COL_B, MG_GET_CHECKPOINT_ALPHA(<<-1744.80, 170.50, 64.40>>, 100), FALSE, FALSE)
					IF NOT IS_PED_INJURED(GET_SPECTATOR_CURRENT_FOCUS_PED())					
						IF HAS_PED_GOT_WEAPON(GET_SPECTATOR_CURRENT_FOCUS_PED(), GADGETTYPE_PARACHUTE)
							SET_PED_COMPONENT_VARIATION(GET_SPECTATOR_CURRENT_FOCUS_PED(), PED_COMP_HAND, 0, 0)
							SET_PED_COMPONENT_VARIATION(GET_SPECTATOR_CURRENT_FOCUS_PED(), INT_TO_ENUM(PED_COMPONENT,9), 5, 0, 0) //(task)
							SET_PLAYER_PARACHUTE_VARIATION_OVERRIDE(NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_SPECTATOR_CURRENT_FOCUS_PED()), PED_COMP_HAND,0)						
						ENDIF
					ENDIF
					IF NOT DOES_BLIP_EXIST(thisMovingTargetBlipFormation)
						thisMovingTargetBlipFormation = CREATE_BLIP_FOR_COORD(<<-1744.80, 170.50, 64.40>>)
					ELSE
						IF NOT ARE_VECTORS_EQUAL(GET_BLIP_COORDS(thisMovingTargetBlipFormation), <<-1744.80, 170.50, 64.40>>)
							remove_blip(thisMovingTargetBlipFormation)
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		
		CASE PSCD_DLC_VehicleLanding
			CPRINTLN(debug_trevor3,"SPEC: update trailer")
			vector PS_SPEC_vMovingVeh
			if NETWORK_DOES_NETWORK_ID_EXIST(serverBD.vehicleOfInterest)
				CPRINTLN(debug_trevor3,"SPEC: trailer net id exists")
				vehicle_index PS_SPEC_trailer
				PS_SPEC_trailer= NET_TO_VEH(serverBD.vehicleOfInterest)		
				IF IS_VEHICLE_DRIVEABLE(PS_SPEC_trailer)
					CPRINTLN(debug_trevor3,"SPEC: trailer driveable and drawing marker")
					PS_SPEC_vMovingVeh = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PS_SPEC_trailer, <<0.0, 0.0, 0.5>>)
					DRAW_MARKER(MARKER_RING, <<PS_SPEC_vMovingVeh.x, PS_SPEC_vMovingVeh.y, PS_SPEC_vMovingVeh.z - 1.5>>, <<0,0,1>>, <<0,0,0>>, <<5, 5, 5>>, PS_PARA_TARG_COL_R, PS_PARA_TARG_COL_G, PS_PARA_TARG_COL_B, MG_GET_CHECKPOINT_ALPHA(PS_SPEC_vMovingVeh, 100), FALSE, FALSE)
					IF DOES_BLIP_EXIST(thisMovingTargetBlipFormation)
						REMOVE_BLIP(thisMovingTargetBlipFormation)
					ENDIF
					thisMovingTargetBlipFormation = CREATE_BLIP_FOR_COORD(<<PS_SPEC_vMovingVeh.x, PS_SPEC_vMovingVeh.y, PS_SPEC_vMovingVeh.z - 1.5>>)
					BEGIN_TEXT_COMMAND_SET_BLIP_NAME("PS_BLPMTARG")
					END_TEXT_COMMAND_SET_BLIP_NAME(thisMovingTargetBlipFormation)				
				ENDIF
			ELSE
				IF DOES_BLIP_EXIST(thisMovingTargetBlipFormation)
					REMOVE_BLIP(thisMovingTargetBlipFormation)
				ENDIF
			ENDIF

		BREAK
		
		DEFAULT
			
			IF g_current_selected_dlc_PilotSchool_class = PSCD_DLC_Formation //special case for rings behind jet.
				vector vMarkerOffset,vCorona
				
				int iFormStage,j
				
				for j = 0 to 11
					IF IS_BIT_SET(serverBD.iGenericData,j)
						cprintln(debug_Trevor3,"From: bit set = ",j)
						iFormStage = j
					ENDIF
				endfor
				
				cprintln(debug_Trevor3,"FORM: iFormStage = ",iFormStage)
				
				IF iFormStage > 0
				AND iFormStage < 10
					SWITCH iFormStage
						CASE 1 vMarkerOffset = <<0, -65, 0>> BREAK
						CASE 2 vMarkerOffset = <<0, -30, 0>> BREAK
						CASE 3 vMarkerOffset = <<0, -30, 0>> BREAK
						CASE 4 vMarkerOffset = <<0, -15, 15>> BREAK
						CASE 5 vMarkerOffset = <<15, -40, 0>> BREAK
						CASE 6 vMarkerOffset = <<15, -45, 13>> BREAK
						CASE 7 vMarkerOffset = <<15, -40, 0>> BREAK
						CASE 8 vMarkerOffset = <<15, -40, 17>> BREAK
						CASE 9 vMarkerOffset = <<15, -40, 0>> BREAK
					ENDSWITCH
					cprintln(debug_Trevor3,"FORM A")
					IF iFormStage < 10
						cprintln(debug_Trevor3,"FORM B")
						if NETWORK_DOES_NETWORK_ID_EXIST(serverBD.vehicleOfInterest)
							cprintln(debug_Trevor3,"FORM C")
							vehicle_index PS_SPEC_jet
							PS_SPEC_jet= NET_TO_VEH(serverBD.vehicleOfInterest)		
							IF IS_VEHICLE_DRIVEABLE(PS_SPEC_jet)
								cprintln(debug_Trevor3,"FORM D")
							
								//IF PS_DLC_Formation_State = PS_DLC_FORMATION_FINAL AND ((fJetRoll > 130 OR fJetRoll < -130) AND (GET_ENTITY_PITCH(PS_Main.myVehicle) < -135 OR GET_ENTITY_PITCH(PS_Main.myVehicle) > 135))
								//OR PS_DLC_Formation_State != PS_DLC_FORMATION_FINAL
								
									
									IF DOES_BLIP_EXIST(thisMovingTargetBlipFormation) 
									AND IS_BIT_SET(serverBD.iGenericData,20)								
										GET_HUD_COLOUR(HUD_COLOUR_GREEN,PS_COL_R,PS_COL_G,PS_COL_B,PS_COL_A)				
										PS_COL_A = PS_PARA_TARG_COL_A //keep alpha same
									ELSE									
										IF iFormStage =  9									
											GET_HUD_COLOUR(HUD_COLOUR_BLUE,PS_COL_R,PS_COL_G,PS_COL_B,PS_COL_A)										
											PS_COL_A = PS_PARA_TARG_COL_A //keep alpha same
										ELSE
											PS_COL_R = PS_PARA_TARG_COL_R
											PS_COL_G = PS_PARA_TARG_COL_G
											PS_COL_B = PS_PARA_TARG_COL_B
											PS_COL_A = PS_PARA_TARG_COL_A
										ENDIF
									ENDIF	
								//ENDIF
							
								vector vLead
								vLead = GET_ENTITY_COORDS(PS_SPEC_jet)
								vCorona = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PS_SPEC_jet, vMarkerOffset)
								
								IF iFormStage != 9
								
									IF iFormStage < 4 vCorona.z = vLead.z ENDIF
									
									IF iFormStage != 4
										DRAW_MARKER(MARKER_RING, vCorona, NORMALISE_VECTOR(GET_ENTITY_COORDS(PS_SPEC_jet)-vCorona), <<0,180,0>>, <<PS_PARA_TARG_SCL_L, PS_PARA_TARG_SCL_L, PS_PARA_TARG_SCL_L>>, PS_COL_R,PS_COL_G,PS_COL_B,PS_COL_A, FALSE, FALSE)
									ELSE
										DRAW_MARKER(MARKER_RING, vCorona, NORMALISE_VECTOR(GET_ENTITY_COORDS(PS_SPEC_jet)+<<0,0,15>>-vCorona), <<0,0,0>>, <<PS_PARA_TARG_SCL_L, PS_PARA_TARG_SCL_L, PS_PARA_TARG_SCL_L>>, PS_COL_R,PS_COL_G,PS_COL_B,PS_COL_A, FALSE, FALSE)
									ENDIF
								ELSE
									//not needed. Covered above
									//int invRed,invBlue,invGreen,invAlpha
									//GET_RGB_FROM_INT(GET_INT_FROM_HUD_COLOUR(HUD_COLOUR_BLUE),invRed,invBlue,invGreen,invAlpha)
							
									DELETE_CHECKPOINT(PS_SPEC_currentCheckpoint)
									PS_SPEC_currentCheckpoint = CREATE_CHECKPOINT(CHECKPOINT_PLANE_INVERTED,vCorona,NORMALISE_VECTOR(GET_ENTITY_COORDS(PS_SPEC_jet)-vCorona),PS_PARA_TARG_SCL_L,PS_COL_R,PS_COL_G,PS_COL_B,PS_COL_A)
									PS_currentType = ENUM_TO_INT(CHECKPOINT_PLANE_INVERTED)
									cprintln(debug_Trevor3,"FORM CREATE CHECKPOINT")
								ENDIF
								
								IF DOES_BLIP_EXIST(thisMovingTargetBlipFormation)
									SET_BLIP_COORDS(thisMovingTargetBlipFormation, vCorona)
								ELSE
									thisMovingTargetBlipFormation = CREATE_BLIP_FOR_COORD(vCorona)
									SET_BLIP_SCALE(thisMovingTargetBlipFormation, 1.2)
								ENDIF
								
								IF PS_COL_R = PS_PARA_TARG_COL_R
									IF iFormStage = 9
										SET_BLIP_COLOUR(thisMovingTargetBlipFormation,BLIP_COLOUR_BLUE)
									ELSE
										SET_BLIP_COLOUR(thisMovingTargetBlipFormation,BLIP_COLOUR_YELLOW) 
									ENDIF
								ELSE
									SET_BLIP_COLOUR(thisMovingTargetBlipFormation,BLIP_COLOUR_GREEN) 
								ENDIF
								
								
								
								IF DOES_BLIP_EXIST(thisMovingTargetBlipFormation)
									BEGIN_TEXT_COMMAND_SET_BLIP_NAME("PS_BLPMTARG")
									END_TEXT_COMMAND_SET_BLIP_NAME(thisMovingTargetBlipFormation)
								ENDIF
							ENDIF
						ENDIF
					
					ENDIF
				ELSE						
				//	DELETE_CHECKPOINT(PS_SPEC_currentCheckpoint)
				
					IF NOT IS_VECTOR_ZERO(serverBD.vNextCheckpoint)
						IF NOT ARE_VECTORS_EQUAL(serverBD.vNextCheckpoint,PS_SPEC_vNextCheckpoint)
						OR PS_SPEC_nextCheckpoint = null
							PS_SPEC_vNextCheckpoint = serverBD.vNextCheckpoint
							IF PS_SPEC_nextCheckpoint != Null
								DELETE_CHECKPOINT(PS_SPEC_nextCheckpoint)
								REMOVE_BLIP(PS_SPEC_nextBlip)
							ENDIF
							PS_SPEC_nextCheckpoint = PS_SPEC_create_checkpoint(INT_TO_ENUM(PS_CHECKPOINT_ENUM,serverBD.nextCheckpointType),PS_SPEC_vNextCheckpoint,<<0,0,0>>,PS_SPEC_nextBlip)
							CPRINTLN(debug_trevor3,"***Checkpoint update SPEC*** : create next checkpoint")
						ENDIF
						//draw next checkpoint
					ENDIF
					
					IF NOT IS_VECTOR_ZERO(serverBD.vCurrentCheckpoint)
						IF NOT ARE_VECTORS_EQUAL(serverBD.vCurrentCheckpoint,PS_SPEC_vCurrentCheckpoint)
						OR PS_SPEC_currentCheckpoint = null
							
							IF PS_SPEC_currentCheckpoint != Null
								PS_CREATE_FLASHING_CHECKPOINT_FOR_SPECTATOR(PS_SPEC_vCurrentCheckpoint,PS_SPEC_vNextCheckpoint,PS_currentType,serverBD.Missed)
								DELETE_CHECKPOINT(PS_SPEC_currentCheckpoint)
								REMOVE_BLIP(PS_SPEC_currentBlip)
							ENDIF
							PS_SPEC_vCurrentCheckpoint = serverBD.vCurrentCheckpoint
							PS_SPEC_currentCheckpoint = PS_SPEC_create_checkpoint(INT_TO_ENUM(PS_CHECKPOINT_ENUM,serverBD.currentCheckpointType),PS_SPEC_vCurrentCheckpoint,PS_SPEC_vNextCheckpoint,PS_SPEC_currentBlip)
							 PS_currentType =  serverBD.currentCheckpointType
							CPRINTLN(debug_trevor3,"***Checkpoint update SPEC*** : create current checkpoint")
						ENDIF
						//draw current checkpoint
					ENDIF
				
					IF DOES_BLIP_EXIST(thisMovingTargetBlipFormation)
						REMOVE_BLIP(thisMovingTargetBlipFormation)
					ENDIF
				ENDIF
			ELSE
				IF NOT IS_VECTOR_ZERO(serverBD.vNextCheckpoint)
					IF NOT ARE_VECTORS_EQUAL(serverBD.vNextCheckpoint,PS_SPEC_vNextCheckpoint)
					OR PS_SPEC_nextCheckpoint = null
						PS_SPEC_vNextCheckpoint = serverBD.vNextCheckpoint
						IF PS_SPEC_nextCheckpoint != Null
							DELETE_CHECKPOINT(PS_SPEC_nextCheckpoint)
							REMOVE_BLIP(PS_SPEC_nextBlip)
						ENDIF
						PS_SPEC_nextCheckpoint = PS_SPEC_create_checkpoint(INT_TO_ENUM(PS_CHECKPOINT_ENUM,serverBD.nextCheckpointType),PS_SPEC_vNextCheckpoint,<<0,0,0>>,PS_SPEC_nextBlip)
						CPRINTLN(debug_trevor3,"***Checkpoint update SPEC*** : create next checkpoint")
					ENDIF
					//draw next checkpoint
				ENDIF
				
				IF NOT IS_VECTOR_ZERO(serverBD.vCurrentCheckpoint)
					IF NOT ARE_VECTORS_EQUAL(serverBD.vCurrentCheckpoint,PS_SPEC_vCurrentCheckpoint)
					OR PS_SPEC_currentCheckpoint = null
						
						IF PS_SPEC_currentCheckpoint != Null
							PS_CREATE_FLASHING_CHECKPOINT_FOR_SPECTATOR(PS_SPEC_vCurrentCheckpoint,PS_SPEC_vNextCheckpoint,PS_currentType,serverBD.Missed)
							DELETE_CHECKPOINT(PS_SPEC_currentCheckpoint)
							REMOVE_BLIP(PS_SPEC_currentBlip)
						ENDIF
						
						PS_SPEC_vCurrentCheckpoint = serverBD.vCurrentCheckpoint
						PS_SPEC_currentCheckpoint = PS_SPEC_create_checkpoint(INT_TO_ENUM(PS_CHECKPOINT_ENUM,serverBD.currentCheckpointType),PS_SPEC_vCurrentCheckpoint,PS_SPEC_vNextCheckpoint,PS_SPEC_currentBlip)
						PS_currentType =  serverBD.currentCheckpointType
						CPRINTLN(debug_trevor3,"***Checkpoint update SPEC*** : create current checkpoint")
					ENDIF
					//draw current checkpoint
				ENDIF
			ENDIF
		
														
		BREAK
	ENDSWITCH
	
	PS_UPDATE_CHECKPOINT_FLASH()
	PS_UPDATE_CHECKPOINT_MARKER_FLASH()
ENDPROC

PROC DO_PREVIEW_STUFF()
	
	IF serverBD.bPlayingPreview	
		HIDE_HUD_AND_RADAR_THIS_FRAME()
		g_current_selected_dlc_PilotSchool_class = int_to_enum(PILOT_SCHOOL_DLC_CLASSES_ENUM_NEW,serverBD.iSelectedLesson)
		PRINTLN(" PILOT SCHOOL - SPECTATOR - PS_SPECMODE_LESSON : IF serverBD.bPlayingPreview	 : lesson = ",serverBD.iSelectedLesson)
		PS_SET_TIME_AND_WEATHER()
		DISABLE_SPECTATOR_FILTER_OPTION(TRUE)
				
		IF HAS_SCALEFORM_MOVIE_LOADED(PS_SPEC_sfTelevisionBorder)
			SET_TIMECYCLE_MODIFIER("scanline_cam")
			DRAW_SCALEFORM_MOVIE_FULLSCREEN(PS_SPEC_sfTelevisionBorder, 255, 255, 255, 255)
			PS_SET_TIME_AND_WEATHER()
			PS_SPEC_VIEW_CHECPOINTS()
		ELSE
			PS_SPEC_sfTelevisionBorder = REQUEST_SCALEFORM_MOVIE("TV_FRAME")
			PRINTLN(" PILOT SCHOOL - SPECTATOR - LOAD PREVIEW EFFECTS - 1")
		ENDIF
		
		SWITCH g_current_selected_dlc_PilotSchool_class
			CASE PSCD_DLC_ChaseParachute
					//special camera case for chase parachute

				IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.vehicleOfInterest)
					ped_index followPEd
					followPed = NET_TO_PED(serverBD.vehicleOfInterest)
					
					IF NOT IS_ENTITY_DEAD(followPed)
						IF NOT DOES_CAM_EXIST(specData.specCamData.camCinematic)
							cprintln(debug_Trevor3,"Create spec cam")
							SET_REMOTE_SCRIPT_IS_USING_SPECIAL_SPECTATOR_CAM(TRUE)
							CREATE_SPECIAL_SPECTATOR_CAMERA(specData,0.15, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(followPed, << -0.500, -7.750, 1.850>>),<<0,0,0>>,38,TRUE)
							
						//	NETWORK_SET_IN_FREE_CAM_MODE(TRUE)                 
	               		//	SET_IN_SPECTATOR_MODE(FALSE)
				
							//PS_Main.previewCam1 = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(followPed, << -0.500, -7.750, 1.850>>), <<0,0,0>>, 38, TRUE)														
							ATTACH_CAM_TO_ENTITY(specData.specCamData.camCinematic,followPed,<<0,-10,4>>)								
							POINT_CAM_AT_ENTITY(specData.specCamData.camCinematic,followPed,<<0,0,0>>)
							SET_CAM_NEAR_CLIP(specData.specCamData.camCinematic, 0.840)
							//SHAKE_SCRIPT_GLOBAL("SKY_DIVING_SHAKE", 0.15)										
							
							//RENDER_SCRIPT_CAMS(TRUE, FALSE)
						ELSE
							
						ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE PSCD_DLC_ShootingRange
				IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.vehicleOfInterest)
					IF IS_VEHICLE_DRIVEABLE(NET_TO_VEH(serverBD.vehicleOfInterest))
						//IF GET_SCRIPT_TASK_STATUS(GET_SPECTATOR_CURRENT_FOCUS_PED(),SCRIPT_TASK_VEHICLE_MISSION) != PERFORMING_TASK
							//TASK_HELI_MISSION(GET_SPECTATOR_CURRENT_FOCUS_PED(),NET_TO_VEH(serverBD.vehicleOfInterest),null,null,GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PS_Main.myVehicle,<<0,100,0>>),MISSION_ATTACK,1,1,1,10,10)
					//	IF NOT IS_PED_INJURED(GET_SPECTATOR_CURRENT_FOCUS_PED())
					//		SET_VEHICLE_SHOOT_AT_TARGET(GET_SPECTATOR_CURRENT_FOCUS_PED(),NET_TO_VEH(serverBD.vehicleOfInterest),<<0,0,0>>)
					//	ENDIF
						
						//SHOOT_SINGLE_BULLET_BETWEEN_COORDS(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(NET_TO_VEH(serverBD.vehicleOfInterest),<<1.1556,-0.1609,0.5588>>),GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(NET_TO_VEH(serverBD.vehicleOfInterest),<<0,100,0>>),10,false,WEAPONTYPE_VEHICLE_PLAYER_BUZZARD)
						//<<-1067.1451, -2671.6929, 12.6988>>
						//<<-1065.9895, -2671.5320, 13.2588>>
						
						
						
						//ENDIF
					ENDIF
				ENDIF
				
			BREAK
		ENDSWITCH
				
		
	ELSE
		cprintln(debug_trevor3,"Spec not playing preview")
		IF HAS_SCALEFORM_MOVIE_LOADED(PS_SPEC_sfTelevisionBorder)
			SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(PS_SPEC_sfTelevisionBorder)
			CLEAR_TIMECYCLE_MODIFIER()
			PRINTLN(" PILOT SCHOOL - SPECTATOR - CLEAR PREVIEW EFFECTS - 2")
		ENDIF
		
		DISABLE_SPECTATOR_FILTER_OPTION(FALSE)
					
		IF PS_specMode = PS_SPECMODE_LESSON
			IF g_current_selected_dlc_PilotSchool_class = PSCD_DLC_ChaseParachute
				IF bDoingMidLessonCam = FALSE
					IF DOES_CAM_EXIST(specData.specCamData.camCinematic)
						cprintln(debug_trevor3,"called this cam destroy cleanup guff.")
						cprintln(debug_Trevor3,"Destroy spec cam")
						DETACH_CAM(specData.specCamData.camCinematic)
						CLEANUP_SPECIAL_SPECTATOR_CAMERA(specData)
						RENDER_SCRIPT_CAMS(FALSE, FALSE, 0, TRUE, TRUE)
						//cprintln(debug_trevor3,"Delete Preview Cam for Spec")
						//STOP_SCRIPT_GLOBAL_SHAKING(TRUE)
						//NETWORK_SET_IN_FREE_CAM_MODE(FALSE)                            
						//RENDER_SCRIPT_CAMS(FALSE,FALSE)
						//DESTROY_CAM(PS_Main.previewCam1)
						//SET_IN_SPECTATOR_MODE(TRUE, GET_SPECTATOR_DESIRED_FOCUS_PED())
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

PROC DO_MID_LESSON_CAM()
	cprintln(debug_Trevor3,"DO_MID A")
	IF serverBD.bPlayingPreview	= FALSE
		cprintln(debug_Trevor3,"DO_MID B")
		IF serverBD.iFlightSchoolStage > ENUM_TO_INT(PS_SPEC_STAGE_LOADING_CHALLENGE)
			cprintln(debug_Trevor3,"DO_MID C")
			IF PS_specMode = PS_SPECMODE_LESSON
				cprintln(debug_Trevor3,"DO_MID D")
				SWITCH int_to_enum(PILOT_SCHOOL_DLC_CLASSES_ENUM_NEW,serverBD.iSelectedLesson)				
					CASE PSCD_DLC_ChaseParachute
						cprintln(debug_Trevor3,"DO_MID E")
						//special camera for when target player is still in the plane
						IF serverBD.iGenericDataC  = 0	//1 = Jumped
							cprintln(debug_Trevor3,"DO_MID F")
							IF NOT DOES_CAM_EXIST(specData.specCamData.camCinematic)
								cprintln(debug_Trevor3,"DO_MID G")
								//IF HAS_NET_TIMER_EXPIRED(MidCamTimer, 1000)
								IF NOT IS_PED_INJURED(GET_SPECTATOR_CURRENT_FOCUS_PED())
									cprintln(debug_Trevor3,"DO_MID H")
									SET_REMOTE_SCRIPT_IS_USING_SPECIAL_SPECTATOR_CAM(TRUE)
									CREATE_SPECIAL_SPECTATOR_CAMERA(specData, 0.15, GET_ENTITY_COORDS(GET_SPECTATOR_CURRENT_FOCUS_PED()), <<0, 0, 0>>, 38, TRUE)
									//CREATE_SPECIAL_SPECTATOR_CAMERA(specData, 0.15, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(GET_SPECTATOR_CURRENT_FOCUS_PED(), <<0.5, -3.0, 1.0>>), <<0, 0, 0>>, 38, TRUE)
									ATTACH_CAM_TO_ENTITY(specData.specCamData.camCinematic, GET_SPECTATOR_CURRENT_FOCUS_PED(), <<0.5, -3.0, 1.0>>)
									//ATTACH_CAM_TO_ENTITY(specData.specCamData.camCinematic, GET_SPECTATOR_CURRENT_FOCUS_PED(), <<0.5, 5.0, -1.0>>)
									//ATTACH_CAM_TO_ENTITY(specData.specCamData.camCinematic, NET_TO_VEH(serverBD.netIDStore[0]), <<0, 0, 0>>)
									POINT_CAM_AT_ENTITY(specData.specCamData.camCinematic, GET_SPECTATOR_CURRENT_FOCUS_PED(), <<0, 0, 0>>)
									
									SET_CAM_NEAR_CLIP(specData.specCamData.camCinematic, 0.840)
									//SHAKE_SCRIPT_GLOBAL("SKY_DIVING_SHAKE", 0.15)
									
									//RESET_NET_TIMER(MidCamTimer)
									bDoingMidLessonCam = TRUE
									cprintln(debug_Trevor3," PILOT SCHOOL - DO_MID_LESSON_CAM - CHASE CHUTE - CREATE MID LESSON CAM")
								ENDIF
							//ELSE
							//	SET_CAM_COORD(specData.specCamData.camCinematic, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(GET_SPECTATOR_CURRENT_FOCUS_PED(), <<0.5, -3.0, 1.0>>))
							//	//cprintln(debug_Trevor3," PILOT SCHOOL - DO_MID_LESSON_CAM - CHASE CHUTE - UPDATE CAM COORDS")
							//	//POINT_CAM_AT_ENTITY(specData.specCamData.camCinematic, GET_SPECTATOR_CURRENT_FOCUS_PED(), <<0, 0, 0>>)
							ENDIF
						ELSE
							cprintln(debug_Trevor3,"DO_MID I")
							IF DOES_CAM_EXIST(specData.specCamData.camCinematic)
								cprintln(debug_Trevor3,"DO_MID J")
								DETACH_CAM(specData.specCamData.camCinematic)
								CLEANUP_SPECIAL_SPECTATOR_CAMERA(specData)
								bDoingMidLessonCam = FALSE
								cprintln(debug_Trevor3," PILOT SCHOOL - DO_MID_LESSON_CAM - CHASE CHUTE - DESTROY MID LESSON CAM")
							ENDIF
						ENDIF
					BREAK
				ENDSWITCH
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC START_MENU_SPINNER() 
	IF NOT BUSYSPINNER_IS_ON()
		IF HAS_NET_TIMER_EXPIRED(SpecSpinnerTimer, 1000)
			IF IS_SKYSWOOP_AT_GROUND()
				BEGIN_TEXT_COMMAND_BUSYSPINNER_ON("FM_JIP_WAITO") 
				END_TEXT_COMMAND_BUSYSPINNER_ON(ENUM_TO_INT(LOADING_ICON_LOADING)) 
				PRINTLN(" PILOT SCHOOL - SPECTATOR - SPIN SET UP TIME - START_MENU_SPINNER")
				RESET_NET_TIMER(SpecSpinnerTimer)
			ENDIF
		ENDIF
	ENDIF
ENDPROC
PROC STOP_MENU_SPINNER() 
	IF BUSYSPINNER_IS_ON()
		BUSYSPINNER_OFF()
		PRINTLN(" PILOT SCHOOL - SPECTATOR - SPIN SET UP TIME - STOP_MENU_SPINNER") 
	ENDIF
ENDPROC

//int lastPreviewStage

PROC PS_HANDLE_SPECTATOR()
	INT i
	INT iPlayer
	PRINTLN(" PILOT : SCHOOL - SPECTATOR - Spec mode = ",PS_specMode)
	PRINTLN(" PILOT : SCHOOL - SPECTATOR - Player selecting lesson = ",serverBD.bPlayerSelectingLesson," lesson = ",serverBD.iSelectedLesson)
	
	//handle fading
	
	/*
	PS_SPEC_STAGE_START_FLIGHT_SCHOOL,
	PS_SPEC_STAGE_LOAD_MENU,
	PS_SPEC_STAGE_VIEWING_MENU,
	PS_SPEC_STAGE_LOADING_PREVIEW,
	PS_SPEC_STAGE_IN_PREVIEW,
	PS_SPEC_STAGE_LOADING_CHALLENGE,
	PS_SPEC_STAGE_IN_COUNTDOWN,
	PS_SPEC_STAGE_IN_CHALLENGE,
	PS_SPEC_STAGE_QUIT
*/
	
	//launch state
	
	
	IF PS_specMode >= PS_SPECMODE_LOADING
		IF DOES_ENTITY_EXIST(GET_SPECTATOR_CURRENT_FOCUS_PED())
			iPlayer = NATIVE_TO_INT(NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_SPECTATOR_CURRENT_FOCUS_PED()))
			IF iPlayer != -1
			AND iPlayer < NUM_NETWORK_PLAYERS
				IF IS_SCREEN_FADED_OUT()
				AND NOT IS_SCREEN_FADING_IN()
				
					IF serverBD.iFlightSchoolStage = enum_to_int(PS_SPEC_STAGE_PASSED_LESSON)
					OR serverBD.iFlightSchoolStage = enum_to_int(PS_SPEC_STAGE_LOAD_MENU)
					OR serverBD.iFlightSchoolStage = enum_to_int(PS_SPEC_STAGE_VIEWING_MENU)
						IF NOT IS_BIT_SET(MPGlobalsAmbience.iAmbBitSet, iABI_PILOT_SCHOOL_CAM)
							//IF NOT HAS_NET_TIMER_STARTED(FadeTimer)					
							RESET_NET_TIMER(FadeTimer)
							START_NET_TIMER(FadeTimer,TRUE)
							//ENDIF
							CPRINTLN(debug_trevor3,"reset net timer A")
						ENDIF
						SET_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_PILOT_SCHOOL_CAM)
					ELSE
						IF IS_BIT_SET(MPGlobalsAmbience.iAmbBitSet, iABI_PILOT_SCHOOL_CAM)
							IF NOT HAS_NET_TIMER_STARTED(FadeTimer)					
								START_NET_TIMER(FadeTimer,TRUE)
							ENDIF
							CPRINTLN(debug_trevor3,"reset net timer B")
						ENDIF
						CLEAR_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_PILOT_SCHOOL_CAM)
					ENDIF
				
				//	IF NOT bFirstTimeFade
						
					
						//IF lastPreviewStage = PS_SPEC_STAGE_NULL
							
						//ENDIF
					
						//if lastPreviewStage != serverBD.iFlightSchoolStage			
						//	IF serverBD.iFlightSchoolStage = enum_to_int(PS_SPEC_STAGE_LOAD_MENU)
						//	or serverBD.iFlightSchoolStage = enum_to_int(PS_SPEC_STAGE_LOADING_PREVIEW)
						//	or serverBD.iFlightSchoolStage = enum_to_int(PS_SPEC_STAGE_PASSED_LESSON)
						//	OR lastPreviewStage = PS_SPEC_STAGE_NULL
						//		RESET_NET_TIMER(FadeTimer)
						//	ENDIF
						//	lastPreviewStage = serverBD.iFlightSchoolStage
						//ENDIF
						
						if serverBD.iFlightSchoolStage = enum_to_int(PS_SPEC_STAGE_VIEWING_MENU)							
						OR serverBD.iFlightSchoolStage =  enum_to_int(PS_SPEC_STAGE_IN_PREVIEW)
						OR serverBD.iFlightSchoolStage = enum_to_int(PS_SPEC_STAGE_IN_COUNTDOWN)
						OR serverBD.iFlightSchoolStage = enum_to_int(PS_SPEC_STAGE_IN_CHALLENGE)
						or serverBD.iFlightSchoolStage = enum_to_int(PS_SPEC_STAGE_PASSED_LESSON)
							
							IF NOT IS_BIT_SET(GlobalplayerBD[iPlayer].iSpecInfoBitset, SPEC_INFO_BS_FADED_OUT)
							AND NOT IS_BIT_SET(GlobalplayerBD[iPlayer].iSpecInfoBitset, SPEC_INFO_BS_FADING_OUT)
								CPRINTLN(debug_trevor3,"Trying to fade spec in")
								
								IF HAS_NET_TIMER_EXPIRED_READ_ONLY(FadeTimer, 4000,TRUE)
									CPRINTLN(debug_trevor3,"TIMER EXPIRED")
								ENDIF
								
								IF NOT HAS_NET_TIMER_STARTED(FadeTimer)
									CPRINTLN(debug_trevor3,"TIMER HASNT STARTED")
								ENDIF
								
								IF HAS_NET_TIMER_EXPIRED_READ_ONLY(FadeTimer, 4000,TRUE)
								OR NOT HAS_NET_TIMER_STARTED(FadeTimer)							
									RESET_NET_TIMER(FadeTimer)
									CPRINTLN(debug_trevor3,"SCREEN FADE IN A")							
									DO_SCREEN_FADE_IN(500)																	
								ENDIF
							ENDIF
						ENDIF
					//ENDIF
				ENDIF
				
				IF IS_SCREEN_FADED_IN()
				
					cprintln(debug_trevor3,"bDoSpecFade = FALSE stage = ",serverBD.iFlightSchoolStage)
			
				//	bFirstTimeFade = FALSE
				
					IF IS_BIT_SET(MPGlobalsAmbience.iAmbBitSet, iABI_PILOT_SCHOOL_CAM)
						cprintln(debug_trevor3,"Flight school cam bit = TRUE")
					ELSE
						cprintln(debug_trevor3,"Flight school cam bit = FALSE")
					ENDIF
				
					if serverBD.iFlightSchoolStage = enum_to_int(PS_SPEC_STAGE_START_FLIGHT_SCHOOL)
					or (serverBD.iFlightSchoolStage = enum_to_int(PS_SPEC_STAGE_LOAD_MENU) AND NOT IS_BIT_SET(MPGlobalsAmbience.iAmbBitSet, iABI_PILOT_SCHOOL_CAM))
					or serverBD.iFlightSchoolStage = enum_to_int(PS_SPEC_STAGE_LOADING_PREVIEW)
					or serverBD.iFlightSchoolStage = enum_to_int(PS_SPEC_STAGE_LOADING_CHALLENGE)
					or serverBD.iFlightSchoolStage = enum_to_int(PS_SPEC_STAGE_FAILED_LESSON)
					or (serverBD.iFlightSchoolStage = enum_to_int(PS_SPEC_STAGE_PASSED_LESSON) AND NOT IS_BIT_SET(MPGlobalsAmbience.iAmbBitSet, iABI_PILOT_SCHOOL_CAM))
					or serverBD.iFlightSchoolStage = enum_to_int(PS_SPEC_STAGE_QUIT)
						CPRINTLN(debug_trevor3,"SCREEN FADE OUT A")
						RESET_NET_TIMER(FadeTimer) //to ensure the lesson pass return to main screen doesn't keep fading in and out.
						DO_SCREEN_FADE_OUT(0)
					ENDIF
					
					
					IF IS_BIT_SET(GlobalplayerBD[iPlayer].iSpecInfoBitset, SPEC_INFO_BS_FADED_OUT)
					OR IS_BIT_SET(GlobalplayerBD[iPlayer].iSpecInfoBitset, SPEC_INFO_BS_FADING_OUT)					
						CPRINTLN(debug_trevor3,"SCREEN FADE OUT B")
						DO_SCREEN_FADE_OUT(0)
					ENDIF
					
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	

	
	
	SWITCH PS_specMode
		CASE PS_SPECMODE_SETUP
			BLOCK_SPECTATOR_COPYING_TARGET_FADES(TRUE)
			REQUEST_PILOT_SCHOOL_ASSETS()
			IF HAS_LOADED_PILOT_SCHOOL_ASSETS()
				PS_specMode = PS_SPECMODE_LOADING
				
				//Fade Out
				
				PS_Load_Pilot_School_Data()
				DO_SCREEN_FADE_OUT(0)
				RESET_NET_TIMER(FadeTimer)
				START_NET_TIMER(FadeTimer,TRUE)
				PRINTLN(" PILOT SCHOOL - SPECTATOR - FADE OUT - 0")
				//Set Special Cam Flag
				
				/*
				IF NOT IS_BIT_SET(MPGlobalsAmbience.iAmbBitSet, iABI_PILOT_SCHOOL_CAM)
					SET_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_PILOT_SCHOOL_CAM)
					PRINTLN(" PILOT SCHOOL - SPECTATOR - iABI_PILOT_SCHOOL_CAM SET - 0")
				ENDIF
				*/
				
				START_MENU_SPINNER()
				
				PRINTLN(" PILOT SCHOOL - SPECTATOR - MOVE TO - PS_SPECMODE_LOADING")
			ENDIF
				
		BREAK
		CASE PS_SPECMODE_LOADING
			
			START_MENU_SPINNER()
			
			IF IS_SCREEN_FADED_OUT()
				PS_HUD_SET_GUTTER_ICON_ACTIVE(FALSE)
				PS_SET_DIST_HUD_ACTIVE(FALSE)
				PS_SET_AVG_HEIGHT_HUD_ACTIVE(FALSE)
				PS_SET_TIMER_ACTIVE(FALSE)
				PS_SET_CHECKPOINT_COUNTER_ACTIVE(FALSE)
				PS_HUD_SET_SCORECARD_ACTIVE(TRUE)
				PS_SET_ROLL_METER_ACTIVE(FALSE)
				PS_SET_LOOP_METER_ACTIVE(FALSE)
				PS_SET_IMMELMAN_METER_ACTIVE(FALSE)
				PS_HUD_SET_OBJECTIVE_METER_VISIBLE(FALSE)
				PS_STOP_HOURGLASS_TIMER()
				PS_SET_ALTIMETER_ACTIVE(FALSE)
			ENDIF
			
			//only set special cam to load if player has properly ended a lesson and selected to return to main menu, rather than do a replay.
			//IF serverBD.bPlayerSelectingLesson
			//	//Set Special Cam Flag
			//	IF NOT IS_BIT_SET(MPGlobalsAmbience.iAmbBitSet, iABI_PILOT_SCHOOL_CAM)
			//		SET_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_PILOT_SCHOOL_CAM)
			//		PRINTLN(" PILOT SCHOOL - SPECTATOR - iABI_PILOT_SCHOOL_CAM SET - 1")
			//	ENDIF
			//ENDIF
			
		
			
			
			IF DOES_BLIP_EXIST(thisMovingTargetBlipFormation)
				REMOVE_BLIP(thisMovingTargetBlipFormation)
			ENDIF
			
			/*
			IF DOES_ENTITY_EXIST(GET_SPECTATOR_CURRENT_FOCUS_PED())
			AND NOT IS_ENTITY_DEAD(GET_SPECTATOR_CURRENT_FOCUS_PED())
				IF IS_ENTITY_VISIBLE(GET_SPECTATOR_CURRENT_FOCUS_PED())
					SET_ENTITY_VISIBLE(GET_SPECTATOR_CURRENT_FOCUS_PED(),FALSE)
				ENDIF
			ENDIF
			*/
			IF NOT serverBD.bPlayerSelectingLesson
				
				g_current_selected_dlc_PilotSchool_class = int_to_enum(PILOT_SCHOOL_DLC_CLASSES_ENUM_NEW,serverBD.iSelectedLesson)
				PS_Main.myChallengeData = PS_Challenges[g_current_selected_dlc_PilotSchool_class]
				bCheckpointsRegistered = FALSE
				
				//Do Fade Before going to Lesson if Target Is
				
				
				//set time of day
				PRINTLN(" PILOT SCHOOL - SPECTATOR - PS_SPECMODE_LOADING : lesson = ",serverBD.iSelectedLesson)
				PS_SET_TIME_AND_WEATHER()
				DO_PREVIEW_STUFF()
												
				STOP_MENU_SPINNER()
				
				PS_specMode = PS_SPECMODE_LESSON
				PRINTLN(" PILOT SCHOOL - SPECTATOR - MOVE TO - PS_SPECMODE_LESSON")
			ELSE
				PS_HUD_SET_GUTTER_ICON_ACTIVE(FALSE)
				PS_SET_DIST_HUD_ACTIVE(FALSE)
				PS_SET_AVG_HEIGHT_HUD_ACTIVE(FALSE)
				PS_SET_TIMER_ACTIVE(FALSE)
				PS_SET_CHECKPOINT_COUNTER_ACTIVE(FALSE)
				PS_HUD_SET_SCORECARD_ACTIVE(TRUE)
				PS_SET_ROLL_METER_ACTIVE(FALSE)
				PS_SET_LOOP_METER_ACTIVE(FALSE)
				PS_SET_IMMELMAN_METER_ACTIVE(FALSE)
				PS_HUD_SET_OBJECTIVE_METER_VISIBLE(FALSE)
				PS_STOP_HOURGLASS_TIMER()
				PS_SET_ALTIMETER_ACTIVE(FALSE)
			ENDIF
			
			
			
		BREAK
		CASE PS_SPECMODE_LESSON
			
			//Clear Special Cam Flag
			IF NOT IS_BIT_SET(MPGlobalsAmbience.iAmbBitSet, iABI_PILOT_SCHOOL_CAM)
				//CLEAR_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_PILOT_SCHOOL_CAM)
				//PRINTLN(" PILOT SCHOOL - SPECTATOR - iABI_PILOT_SCHOOL_CAM CLEARED")
			//ELSE
				DO_MID_LESSON_CAM()
			ENDIF
			
			
			
			/*
			IF DOES_ENTITY_EXIST(GET_SPECTATOR_CURRENT_FOCUS_PED())
				IF NOT IS_ENTITY_VISIBLE(GET_SPECTATOR_CURRENT_FOCUS_PED())
					SET_ENTITY_VISIBLE(GET_SPECTATOR_CURRENT_FOCUS_PED(),TRUE)
				ENDIF
			ENDIF*/
			
			IF g_current_selected_dlc_PilotSchool_class = PSCD_DLC_ShootingRange
			OR g_current_selected_dlc_PilotSchool_class = PSCD_DLC_VehicleLanding
				SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0)
			ENDIF
			
			DO_PREVIEW_STUFF()						
			PS_SPEC_VIEW_CHECPOINTS()
			PS_SPEC_RECEIVE_DATA()
			
		
		
			IF serverBD.bPlayerSelectingLesson
			OR HAS_PS_LESSON_ENDED()
			
				//clean up checkpoints
				int iCheck
				For iCheck = 0 to 30							
					IF PS_Main.myCheckpointz[iCheck].checkpoint != null				
						DELETE_CHECKPOINT(PS_Main.myCheckpointz[iCheck].checkpoint)		
						REMOVE_BLIP(PS_Main.myCheckpointz[iCheck].blip)
					ENDIF
				ENDFOR	
				
				IF PS_SPEC_nextCheckpoint != Null
					DELETE_CHECKPOINT(PS_SPEC_nextCheckpoint)
					REMOVE_BLIP(PS_SPEC_nextBlip)
				ENDIF
					
				IF PS_SPEC_currentCheckpoint != Null
					DELETE_CHECKPOINT(PS_SPEC_currentCheckpoint)
					REMOVE_BLIP(PS_SPEC_currentBlip)
				ENDIF
				
				REPEAT COUNT_OF(targetList) i					
					IF DOES_BLIP_EXIST(targetList[i].blip)
						REMOVE_BLIP(targetList[i].blip)
					ENDIF
				ENDREPEAT
				
				IF DOES_BLIP_EXIST(thisMovingTargetBlipFormation)
					REMOVE_BLIP(thisMovingTargetBlipFormation)
				ENDIF
			
				PS_HUD_SET_GUTTER_ICON_ACTIVE(FALSE)
				PS_SET_DIST_HUD_ACTIVE(FALSE)
				PS_SET_AVG_HEIGHT_HUD_ACTIVE(FALSE)
				PS_SET_TIMER_ACTIVE(FALSE)
				PS_SET_CHECKPOINT_COUNTER_ACTIVE(FALSE)
				PS_HUD_SET_SCORECARD_ACTIVE(TRUE)
				PS_SET_ROLL_METER_ACTIVE(FALSE)
				PS_SET_LOOP_METER_ACTIVE(FALSE)
				PS_SET_IMMELMAN_METER_ACTIVE(FALSE)
				PS_HUD_SET_OBJECTIVE_METER_VISIBLE(FALSE)
				PS_STOP_HOURGLASS_TIMER()
				PS_SET_ALTIMETER_ACTIVE(FALSE)
				
				START_MENU_SPINNER()
								
				PS_specMode = PS_SPECMODE_LOADING
				
	
				PRINTLN(" PILOT SCHOOL - SPECTATOR - MOVE TO - PS_SPECMODE_LOADING")
				PRINTLN(" END BBB!")
			ENDIF
		BREAK
	ENDSWITCH
	
	PS_SPEC_HUD_UPDATE_COUNTDOWN()
	
	IF PS_specMode = PS_SPECMODE_LESSON
		cprintln(debug_trevor3,"Call PS_SPEC_DISPLAY_HUD()")
		PS_SPEC_DISPLAY_HUD()
	ENDIF
	
	IF NOT IS_SKYSWOOP_AT_GROUND()
		STOP_MENU_SPINNER()
	ENDIF
ENDPROC
