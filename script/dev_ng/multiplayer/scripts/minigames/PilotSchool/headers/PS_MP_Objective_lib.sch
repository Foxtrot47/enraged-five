//****************************************************************************************************
//
//	Author: 		Asa Dang
//	Date: 			06/1/12
//	Description: 	Contains methods to track objectives in stunt lessons for Flight School
//	
//****************************************************************************************************

USING "Pilot_School_MP_Definitions.sch"
USING "timer_public.sch"


//---------------------------------------------------------
//Debug/Temp
//---------------------------------------------------------

//Debug stuff
#IF IS_DEBUG_BUILD
	PROC PS_DBG_DrawLiteralString(STRING sLiteral, INT iColumn, HUD_COLOURS eColour = HUD_COLOUR_PURE_WHITE)
		//IF IS_KEYBOARD_KEY_PRESSED(KEY_L)
			INT red, green, blue, iAlpha
			GET_HUD_COLOUR(eColour, red, green, blue, iAlpha)
			SET_TEXT_SCALE(0.45, 0.45)
			SET_TEXT_COLOUR(red, green, blue, ROUND(TO_FLOAT(iAlpha) * 0.5))
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.7795, 0.0305*TO_FLOAT(iColumn+1), "STRING", sLiteral)
		//ENDIF
	ENDPROC

	PROC PS_DBG_DrawLiteralStringInt(STRING sLiteral, INT sInt, INT iColumn, HUD_COLOURS eColour = HUD_COLOUR_PURE_WHITE)
		TEXT_LABEL_63 sNewLiteral = sLiteral
		sNewLiteral += sInt
		
		PS_DBG_DrawLiteralString(sNewLiteral, iColumn, eColour)
	ENDPROC

	PROC PS_DBG_DrawLiteralStringFloat(STRING sLiteral, FLOAT sFloat, INT iColumn, HUD_COLOURS eColour = HUD_COLOUR_PURE_WHITE)
		TEXT_LABEL_63 sNewLiteral = sLiteral
		
		#IF IS_DEBUG_BUILD
		sNewLiteral += GET_STRING_FROM_FLOAT(sFloat)
		#ENDIF
		#IF IS_FINAL_BUILD
		sNewLiteral += ROUND(sFloat)
		#ENDIF
		
		PS_DBG_DrawLiteralString(sNewLiteral, iColumn, eColour)
	ENDPROC
#ENDIF

//Temporary stuff
PROC TempObjMeterHack(VECTOR fPlayerCurrentRotation, FLOAT fPlayerCurrentRoll, FLOAT fPlayerCurrentPitch)
	fPlayerCurrentRotation = fPlayerCurrentRotation
	fPlayerCurrentRoll = fPlayerCurrentRoll
	fPlayerCurrentPitch = fPlayerCurrentPitch
ENDPROC

//---------------------------------------------------------
//Sets/Gets
//---------------------------------------------------------

/// PURPOSE:
///    
/// PARAMS:
///    bFlag - 
PROC PS_SET_OBJECTIVE_RETRY_FLAG(BOOL bFlag)
	PS_Main.myObjectiveData.bRetryFlag = bFlag
ENDPROC

/// PURPOSE:
///    
/// RETURNS:
///    
FUNC BOOL PS_GET_OBJECTIVE_RETRY_FLAG()
	RETURN PS_Main.myObjectiveData.bRetryFlag
ENDFUNC

/// PURPOSE:
///    
/// RETURNS:
///    
FUNC INT PS_GET_OBJECTIVE_CURRENT_ORIENT_COUNT()
	RETURN PS_Main.myObjectiveData.iCurrentOrientCount
ENDFUNC

/// PURPOSE:
///    
/// RETURNS:
///    
FUNC INT PS_GET_OBJECTIVE_TOTAL_ORIENT_COUNT()
	RETURN PS_Main.myObjectiveData.iTotalOrientCount
ENDFUNC

/// PURPOSE:
///    
/// RETURNS:
///    
FUNC BOOL PS_IS_OBJECTIVE_BEING_TRACKED()
	RETURN PS_Main.myObjectiveData.bUpdateData
ENDFUNC

/// PURPOSE:
///    
/// PARAMS:
///    bActivate - 
PROC PS_ACTIVATE_OBJECTIVE_TRACKING(BOOL bActivate)
	PS_Main.myObjectiveData.bUpdateData = bActivate
ENDPROC

/// PURPOSE:
///    resets data used for objective tracking
/// PARAMS:
///    thisData - 
PROC PS_RESET_OBJECTIVE_DATA()
	PS_Main.myObjectiveData.iCurrentOrientCount = 0
	PS_Main.myObjectiveData.iTotalOrientCount = 0
	PS_Main.myObjectiveData.fPlanePrevOrient = 0.0
	PS_Main.myObjectiveData.fPlaneTotalOrient = 0.0
	PS_Main.myObjectiveData.bRetryFlag = FALSE
	CANCEL_TIMER(PS_Main.myObjectiveData.tObjectiveTimer)
ENDPROC

FUNC BOOL PS_IS_PLANE_CURRENTLY_LEVEL()
	VECTOR vFront, vSide, vUp, vPos
	IF IS_ENTITY_DEAD(PS_Main.myVehicle)
		RETURN FALSE
	ENDIF
	GET_ENTITY_MATRIX(PS_Main.myVehicle, vFront, vSide, vUp, vPos)
	RETURN vUp.z >= PS_LEVEL_THRESHOLD
ENDFUNC

FUNC BOOL PS_IS_LEVEL_OUT_COMPLETE(FLOAT fHoldTime)
	IF PS_IS_PLANE_CURRENTLY_LEVEL()
		IF NOT IS_TIMER_STARTED(PS_Main.myObjectiveData.tObjectiveTimer)
			START_TIMER_NOW_SAFE(PS_Main.myObjectiveData.tObjectiveTimer)
		ENDIF
		IF TIMER_DO_ONCE_WHEN_READY(PS_Main.myObjectiveData.tObjectiveTimer, fHoldTime)
			RETURN TRUE
		ENDIF
	ELSE
		IF IS_TIMER_STARTED(PS_Main.myObjectiveData.tObjectiveTimer)
			RESTART_TIMER_NOW(PS_Main.myObjectiveData.tObjectiveTimer)
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL PS_IS_INVERTED_STUNT_COMPLETE(FLOAT fHoldTime)
	IF TIMER_DO_ONCE_WHEN_READY(PS_Main.myObjectiveData.tObjectiveTimer, fHoldTime)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC PS_UPDATE_ROLL_TRACKING(VECTOR fPlayerCurrentRotation, FLOAT fPlayerCurrentRoll, FLOAT fPlayerCurrentPitch)
	
	TempObjMeterHack(fPlayerCurrentRotation, fPlayerCurrentRoll, fPlayerCurrentPitch)
	
//	#IF IS_DEBUG_BUILD
//		PS_DBG_DrawLiteralStringFloat("roll: ", fPlayerCurrentRoll, 0)
//		PS_DBG_DrawLiteralStringFloat("roty: ", fPlayerCurrentRotation.y, 1)
//	#ENDIF
//	#IF IS_DEBUG_BUILD
//		PS_DBG_DrawLiteralStringFloat("pitch: ", fPlayerCurrentPitch, 1)
//	#ENDIF

	IF ABSF(fPlayerCurrentPitch) > 45.0 AND ABSF(fPlayerCurrentPitch) < 135.0 
		PS_SET_OBJECTIVE_RETRY_FLAG(TRUE)
	ENDIF
	
	FLOAT fPlayerRollDiff = fPlayerCurrentRoll-PS_Main.myObjectiveData.fPlanePrevOrient
	
					
	//because of the jump from roll HUGE to TINY
	IF fPlayerRollDiff > 270.0
		fPlayerRollDiff -= 360.0
	ELIF fPlayerRollDiff < -270.0
		fPlayerRollDiff += 360.0
	ENDIF
	
//	#IF IS_DEBUG_BUILD
//		PS_DBG_DrawLiteralStringFloat("fPlayerRollDiff: ", fPlayerRollDiff, 2)
//	#ENDIF
	
	IF fPlayerRollDiff > 0
		IF PS_Main.myObjectiveData.bCurrentlyClockwiseOrient
			PS_Main.myObjectiveData.iTotalOrientCount += PS_Main.myObjectiveData.iCurrentOrientCount
			PS_Main.myObjectiveData.fPlaneTotalOrient = 0//ABSF(fPlayerCurrentPitch)
			PS_Main.myObjectiveData.iCurrentOrientCount = 0
		ENDIF	
		PS_Main.myObjectiveData.bCurrentlyClockwiseOrient = FALSE
	ELSE	
		IF NOT PS_Main.myObjectiveData.bCurrentlyClockwiseOrient
			PS_Main.myObjectiveData.iTotalOrientCount += PS_Main.myObjectiveData.iCurrentOrientCount
			PS_Main.myObjectiveData.fPlaneTotalOrient = 0//ABSF(fPlayerCurrentPitch)
			PS_Main.myObjectiveData.iCurrentOrientCount = 0
		ENDIF
		PS_Main.myObjectiveData.bCurrentlyClockwiseOrient = TRUE
	ENDIF			
	PS_Main.myObjectiveData.iCurrentOrientCount	= FLOOR(ABSF(PS_Main.myObjectiveData.fPlaneTotalOrient)/PS_UI_ObjectiveMeter.fObjBarDemoninator)	
	
	IF ABSF(fPlayerRollDiff) > 90
		fPlayerRollDiff = 0
	ENDIF
	
	PS_Main.myObjectiveData.fPlanePrevOrient		= fPlayerCurrentRoll
	PS_Main.myObjectiveData.fPlaneTotalOrient		+= fPlayerRollDiff
//	
//	#IF IS_DEBUG_BUILD
//		PS_DBG_DrawLiteralStringFloat("fPlaneTotalOrient: ", PS_Main.myObjectiveData.fPlaneTotalOrient, 3)
//	#ENDIF
ENDPROC

PROC PS_UPDATE_INVERTED_TRACKING(VECTOR fPlayerCurrentRotation, FLOAT fPlayerCurrentRoll, FLOAT fPlayerCurrentPitch)
	VECTOR vFront, vSide, vUp, vPos
	IF IS_ENTITY_DEAD(PS_Main.myVehicle)
		IF IS_TIMER_STARTED(PS_Main.myObjectiveData.tObjectiveTimer)
			PAUSE_TIMER(PS_Main.myObjectiveData.tObjectiveTimer)
		ENDIF
		EXIT
	ENDIF
	
	TempObjMeterHack(fPlayerCurrentRotation, fPlayerCurrentRoll, fPlayerCurrentPitch)
	GET_ENTITY_MATRIX(PS_main.myVehicle, vFront, vSide, vUp, vPos)
	
	IF vUp.z <= STUNT_PLANE_INVERT_THRESHOLD
		IF NOT IS_TIMER_STARTED(PS_Main.myObjectiveData.tObjectiveTimer)
			START_TIMER_NOW_SAFE(PS_Main.myObjectiveData.tObjectiveTimer)
		ENDIF
	ELSE
		IF IS_TIMER_STARTED(PS_Main.myObjectiveData.tObjectiveTimer)
			RESTART_TIMER_NOW(PS_Main.myObjectiveData.tObjectiveTimer)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL PS_IS_PLANE_KNIFING()
	VECTOR vFront, vSide, vUp, vPos
	IF IS_ENTITY_DEAD(PS_Main.myVehicle)
		RETURN FALSE
	ENDIF
	GET_ENTITY_MATRIX(PS_Main.myVehicle, vFront, vSide, vUp, vPos)
	IF ABSF(vUp.z) > STUNT_PLANE_KNIFE_THRESHOLD
		RETURN FALSE
	ENDIF
	RETURN vSide.z <> 0.0
ENDFUNC

FUNC BOOL PS_IS_PLANE_KNIFING_LEFT()
	VECTOR vFront, vSide, vUp, vPos
	IF IS_ENTITY_DEAD(PS_Main.myVehicle)
		RETURN FALSE
	ENDIF
	GET_ENTITY_MATRIX(PS_Main.myVehicle, vFront, vSide, vUp, vPos)
	IF ABSF(vUp.z) > STUNT_PLANE_KNIFE_THRESHOLD
		RETURN FALSE
	ENDIF
	RETURN vSide.z > 0.0
ENDFUNC

FUNC BOOL PS_IS_PLANE_KNIFING_RIGHT()
	VECTOR vFront, vSide, vUp, vPos
	IF IS_ENTITY_DEAD(PS_Main.myVehicle)
		RETURN FALSE
	ENDIF
	GET_ENTITY_MATRIX(PS_Main.myVehicle, vFront, vSide, vUp, vPos)
	IF ABSF(vUp.z) > STUNT_PLANE_KNIFE_THRESHOLD
		RETURN FALSE
	ENDIF
	RETURN vSide.z < 0.0
ENDFUNC

FUNC BOOL PS_IS_KNIFE_METER_FULL(FLOAT fullMeter)
	RETURN TIMER_DO_WHEN_READY(PS_Main.myObjectiveData.tObjectiveTimer, fullMeter)
ENDFUNC

PROC PS_UPDATE_KNIFE_LEFT_TRACKING(VECTOR fPlayerCurrentRotation, FLOAT fPlayerCurrentRoll, FLOAT fPlayerCurrentPitch)
	TempObjMeterHack(fPlayerCurrentRotation, fPlayerCurrentRoll, fPlayerCurrentPitch)

	IF PS_IS_PLANE_KNIFING_LEFT()
		IF NOT IS_TIMER_STARTED(PS_Main.myObjectiveData.tObjectiveTimer)
			START_TIMER_NOW_SAFE(PS_Main.myObjectiveData.tObjectiveTimer)
		ELIF IS_TIMER_PAUSED(PS_Main.myObjectiveData.tObjectiveTimer)
			UNPAUSE_TIMER(PS_Main.myObjectiveData.tObjectiveTimer)
		ENDIF
	ELSE
		IF IS_TIMER_STARTED(PS_Main.myObjectiveData.tObjectiveTimer)
			CANCEL_TIMER(PS_Main.myObjectiveData.tObjectiveTimer)
		ENDIF
	ENDIF
ENDPROC

PROC PS_UPDATE_KNIFE_RIGHT_TRACKING(VECTOR fPlayerCurrentRotation, FLOAT fPlayerCurrentRoll, FLOAT fPlayerCurrentPitch)
	TempObjMeterHack(fPlayerCurrentRotation, fPlayerCurrentRoll, fPlayerCurrentPitch)
	
	IF PS_IS_PLANE_KNIFING_RIGHT()
		IF NOT IS_TIMER_STARTED(PS_Main.myObjectiveData.tObjectiveTimer)
			START_TIMER_NOW_SAFE(PS_Main.myObjectiveData.tObjectiveTimer)
		ELIF IS_TIMER_PAUSED(PS_Main.myObjectiveData.tObjectiveTimer)
			UNPAUSE_TIMER(PS_Main.myObjectiveData.tObjectiveTimer)
		ENDIF
	ELSE
		IF IS_TIMER_STARTED(PS_Main.myObjectiveData.tObjectiveTimer)
			CANCEL_TIMER(PS_Main.myObjectiveData.tObjectiveTimer)
		ENDIF
	ENDIF	
ENDPROC

PROC PS_UPDATE_INSIDE_LOOP_TRACKING(VECTOR vPlayerCurrentSide, VECTOR fPlayerCurrentRotation, FLOAT fPlayerCurrentRoll, FLOAT fPlayerCurrentPitch)
	TempObjMeterHack(fPlayerCurrentRotation, fPlayerCurrentRoll, fPlayerCurrentPitch)

//	#IF IS_DEBUG_BUILD
//		PS_DBG_DrawLiteralStringFloat("fPlanePrevOrient: ", PS_Main.myObjectiveData.fPlanePrevOrient, 0)
//		PS_DBG_DrawLiteralStringFloat("fPlayerCurrentPitch: ", fPlayerCurrentPitch, 1)
//	#ENDIF
//	
	FLOAT fPlayerLoopDiff = fPlayerCurrentPitch-PS_Main.myObjectiveData.fPlanePrevOrient

	//because of the jump from Loop HUGE to TINY
	IF fPlayerLoopDiff > 270.0
		fPlayerLoopDiff -= 360.0
	ELIF fPlayerLoopDiff < -270.0
		fPlayerLoopDiff += 360.0
	ENDIF
	
//	#IF IS_DEBUG_BUILD
//		PS_DBG_DrawLiteralStringFloat("fPlayerLoopDiff: ", fPlayerLoopDiff, 2)
//	#ENDIF
//	
	//check we're not rolling the plane
	IF ABSF(vPlayerCurrentSide.z) > 0.574 // sin(35)
		PS_Main.myObjectiveData.fPlaneTotalOrient = 0
		PS_SET_OBJECTIVE_RETRY_FLAG(TRUE)
		
		EXIT
	ENDIF
	
	//If the pitch dif is pos, we're looping with our nose going up, otherwise we're looping with our nose going down
	IF fPlayerLoopDiff > PS_LOOPING_MIN_PITCH_DIFFERENCE
		
		IF PS_Main.myObjectiveData.bCurrentlyClockwiseOrient
	
			IF PS_Main.myObjectiveData.fPlaneTotalOrient > PS_RETRY_STUNT_ANGLE
			OR (fPlayerCurrentPitch < -45 AND fPlayerCurrentPitch > -135)
			
				PS_Main.myObjectiveData.fPlaneTotalOrient = 0
				PS_SET_OBJECTIVE_RETRY_FLAG(TRUE)
			ENDIF
			PS_Main.myObjectiveData.fPlaneTotalOrient = 0.0
			PS_Main.myObjectiveData.iTotalOrientCount += PS_Main.myObjectiveData.iCurrentOrientCount
			PS_Main.myObjectiveData.iCurrentOrientCount = 0
		ENDIF
		
		PS_Main.myObjectiveData.bCurrentlyClockwiseOrient = FALSE
	ELIF fPlayerLoopDiff < -PS_LOOPING_MIN_PITCH_DIFFERENCE
	
		IF NOT PS_Main.myObjectiveData.bCurrentlyClockwiseOrient
			IF PS_Main.myObjectiveData.fPlaneTotalOrient > PS_RETRY_STUNT_ANGLE
			OR (fPlayerCurrentPitch < -45 AND fPlayerCurrentPitch > -135)
			
				PS_Main.myObjectiveData.fPlaneTotalOrient = 0
				PS_SET_OBJECTIVE_RETRY_FLAG(TRUE)
			ENDIF
		
			PS_Main.myObjectiveData.fPlaneTotalOrient = 0.0
			PS_Main.myObjectiveData.iTotalOrientCount += PS_Main.myObjectiveData.iCurrentOrientCount
			PS_Main.myObjectiveData.iCurrentOrientCount = 0
		ENDIF
		
		PS_Main.myObjectiveData.bCurrentlyClockwiseOrient = TRUE
	ENDIF

	PS_Main.myObjectiveData.iCurrentOrientCount	= FLOOR(ABSF(PS_Main.myObjectiveData.fPlaneTotalOrient)/PS_LOOPING_LOOP_GOAL_ANGLE)
	IF PS_Main.myObjectiveData.iCurrentOrientCount = 0 AND ABSF(PS_Main.myObjectiveData.fPlaneTotalOrient)/PS_LOOPING_LOOP_GOAL_ANGLE > 0.75		
		//check if plane is level-ish
		IF fPlayerCurrentPitch > -5 AND fPlayerCurrentRoll < 10	
			
			PS_Main.myObjectiveData.iCurrentOrientCount++
			PS_Main.myObjectiveData.fPlaneTotalOrient = 0
			PS_Main.myObjectiveData.fPlanePrevOrient = 0
		ENDIF
	ENDIF
	
	fPlayerLoopDiff = CLAMP(fPlayerLoopDiff, 0, 2.0)
	
	PS_Main.myObjectiveData.fPlaneTotalOrient += fPlayerLoopDiff			
	PS_Main.myObjectiveData.fPlanePrevOrient = fPlayerCurrentPitch
ENDPROC

PROC PS_UPDATE_OUTSIDE_LOOP_TRACKING(VECTOR vPlayerCurrentSide, VECTOR fPlayerCurrentRotation, FLOAT fPlayerCurrentRoll, FLOAT fPlayerCurrentPitch)
	TempObjMeterHack(fPlayerCurrentRotation, fPlayerCurrentRoll, fPlayerCurrentPitch)
	cprintln(debug_trevor3,"PS_UPDATE_OUTSIDE_LOOP_TRACKING()")
	//#IF IS_DEBUG_BUILD
	//	PS_DBG_DrawLiteralStringFloat("fPlanePrevOrient: ", PS_Main.myObjectiveData.fPlanePrevOrient, 0)
	//	PS_DBG_DrawLiteralStringFloat("fPlayerCurrentPitch: ", fPlayerCurrentPitch, 1)
	//#ENDIF
	
	FLOAT fPlayerLoopDiff = fPlayerCurrentPitch-PS_Main.myObjectiveData.fPlanePrevOrient
	
	cprintln(debug_trevor3,"fPlayerCurrentPitch = ",fPlayerCurrentPitch," fPlanePrevOrient = ",PS_Main.myObjectiveData.fPlanePrevOrient)

	//because of the jump from Loop HUGE to TINY
	IF fPlayerLoopDiff > 270.0
		fPlayerLoopDiff -= 360.0
	ELIF fPlayerLoopDiff < -270.0
		fPlayerLoopDiff += 360.0
	ENDIF
	
	cprintln(debug_trevor3,"fPlayerLoopDiff = ",fPlayerLoopDiff)

	//check we're not rolling the plane
	IF ABSF(vPlayerCurrentSide.z) > 0.574 // sin(35)
		cprintln(debug_trevor3,"Plane was rolled too far! fPlaneTotalOrient=0")
		PS_Main.myObjectiveData.fPlaneTotalOrient = 0
		PS_SET_OBJECTIVE_RETRY_FLAG(TRUE)
	
		EXIT
	ENDIF
	
	//#IF IS_DEBUG_BUILD
	//	PS_DBG_DrawLiteralStringFloat("fPlayerLoopDiff: ", fPlayerLoopDiff, 2)
	//#ENDIF
	
	//If the pitch dif is pos, we're looping with our nose going up, otherwise we're looping with our nose going down
	IF fPlayerLoopDiff > PS_LOOPING_MIN_PITCH_DIFFERENCE * 2 //give player leeway when moving the stick 
		/*
		IF PS_Main.myObjectiveData.bCurrentlyClockwiseOrient
			IF PS_Main.myObjectiveData.fPlaneTotalOrient > PS_RETRY_STUNT_ANGLE
			OR (fPlayerCurrentPitch < -45 AND fPlayerCurrentPitch > -135)
			OR 
		
				//PS_Main.myObjectiveData.fPlaneTotalOrient = 0
				cprintln(debug_trevor3,"Retry flag A")
				PS_SET_OBJECTIVE_RETRY_FLAG(TRUE)
			ENDIF
			IF PS_Main.myObjectiveData.fPlaneTotalOrient > 0
				cprintln(debug_trevor3,"Retry flag b")
				//PS_Main.myObjectiveData.fPlaneTotalOrient = 0
				PS_SET_OBJECTIVE_RETRY_FLAG(TRUE)
			ENDIF
			
			//PS_Main.myObjectiveData.fPlaneTotalOrient = 0.0
			PS_Main.myObjectiveData.iTotalOrientCount += PS_Main.myObjectiveData.iCurrentOrientCount
			PS_Main.myObjectiveData.iCurrentOrientCount = 0
			cprintln(debug_trevor3,"bCurrentlyClockwiseOrient = true, iTotalOrientCount = ",PS_Main.myObjectiveData.iTotalOrientCount," current = 0")
		ENDIF*/
		
		PS_Main.myObjectiveData.bCurrentlyClockwiseOrient = FALSE
		
	ELIF fPlayerLoopDiff < -PS_LOOPING_MIN_PITCH_DIFFERENCE
		cprintln(debug_trevor3," fPlayerLoopDiff < -PS_LOOPING_MIN_PITCH_DIFFERENCE")
		IF NOT PS_Main.myObjectiveData.bCurrentlyClockwiseOrient
			IF PS_Main.myObjectiveData.fPlaneTotalOrient > PS_RETRY_STUNT_ANGLE
			OR (fPlayerCurrentPitch < -45 AND fPlayerCurrentPitch > -135)
				
				//PS_Main.myObjectiveData.fPlaneTotalOrient = 0
				cprintln(debug_trevor3,"Retry D")
				PS_SET_OBJECTIVE_RETRY_FLAG(TRUE)
			ENDIF
			
			//PS_Main.myObjectiveData.fPlaneTotalOrient = 0.0
			PS_Main.myObjectiveData.iTotalOrientCount += PS_Main.myObjectiveData.iCurrentOrientCount
			PS_Main.myObjectiveData.iCurrentOrientCount = 0
			
		ENDIF
		
		PS_Main.myObjectiveData.bCurrentlyClockwiseOrient = TRUE
	ENDIF

	PS_Main.myObjectiveData.iCurrentOrientCount	= FLOOR(ABSF(PS_Main.myObjectiveData.fPlaneTotalOrient)/PS_LOOPING_ROLL_GOAL_ANGLE)
	cprintln(debug_trevor3,"iCurrentOrientCount = ",PS_Main.myObjectiveData.iCurrentOrientCount)
	
	IF PS_Main.myObjectiveData.iCurrentOrientCount = 0 AND ABSF(PS_Main.myObjectiveData.fPlaneTotalOrient)/PS_LOOPING_ROLL_GOAL_ANGLE > 0.925
		//check if plane is level-ish
		IF ABSF(fPlayerCurrentPitch) < 5 AND fPlayerCurrentRoll < 10
	
			PS_Main.myObjectiveData.iCurrentOrientCount++
			//PS_Main.myObjectiveData.fPlaneTotalOrient = 0
			//PS_Main.myObjectiveData.fPlanePrevOrient = 0
		ENDIF
	ENDIF
	
	fPlayerLoopDiff = CLAMP(fPlayerLoopDiff, -2.0, 0.0)
	
	cprintln(debug_trevor3,"fPlayerLoopDiff= ",fPlayerLoopDiff)
	cprintln(debug_trevor3,"PS_Main.myObjectiveData.fPlanePrevOrient = ",PS_Main.myObjectiveData.fPlanePrevOrient)
	
	IF fPlayerCurrentPitch > 90.0 AND PS_Main.myObjectiveData.fPlanePrevOrient < -90.0
		PS_Main.myObjectiveData.fPlanePrevOrient += 360.0
	ENDIF
	
	cprintln(debug_trevor3,"fPlaneTotalOrient = ",PS_Main.myObjectiveData.fPlaneTotalOrient)
	cprintln(debug_trevor3,"fPlayerCurrentPitch = ",fPlayerCurrentPitch)
	cprintln(debug_trevor3,"PS_Main.myObjectiveData.fPlanePrevOrient = ",PS_Main.myObjectiveData.fPlanePrevOrient)
	
	
	IF PS_Main.myObjectiveData.fPlaneTotalOrient > 0
		if (PS_Main.myObjectiveData.fPlaneTotalOrient - (fPlayerCurrentPitch * -1.0) > 30.0)
			IF ABSF((fPlayerCurrentPitch * -1.0)-PS_Main.myObjectiveData.fPlaneTotalOrient) < 300.0
				cprintln(debug_trevor3,"Faile E")
				PS_SET_OBJECTIVE_RETRY_FLAG(TRUE)
			ENDIF
		endif
	endif
	
	IF fPlayerCurrentPitch < PS_Main.myObjectiveData.fPlanePrevOrient
		IF fPlayerCurrentPitch < 0
			IF fPlayerCurrentPitch * -1.0 > PS_Main.myObjectiveData.fPlaneTotalOrient
				IF ABSF((fPlayerCurrentPitch * -1.0)-PS_Main.myObjectiveData.fPlaneTotalOrient) < 30.0		
					PS_Main.myObjectiveData.fPlaneTotalOrient = fPlayerCurrentPitch * -1.0
					cprintln(debug_trevor3,"fPlaneTotalOrient a = ",PS_Main.myObjectiveData.fPlaneTotalOrient)
				ELSE
					
					IF ABSF((fPlayerCurrentPitch * -1.0)-PS_Main.myObjectiveData.fPlaneTotalOrient) < 300.0
						cprintln(debug_trevor3,"Faile F")
						PS_SET_OBJECTIVE_RETRY_FLAG(TRUE)
					ENDIF
				ENDIf
			ENDIF
		ELSE
			IF PS_Main.myObjectiveData.fPlaneTotalOrient > 90.0
				IF 180.0+(180.0 - fPlayerCurrentPitch) > PS_Main.myObjectiveData.fPlaneTotalOrient
					PS_Main.myObjectiveData.fPlaneTotalOrient = 180.0+(180.0 - fPlayerCurrentPitch)
					cprintln(debug_trevor3,"fPlaneTotalOrient b = ",PS_Main.myObjectiveData.fPlaneTotalOrient)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//PS_Main.myObjectiveData.fPlaneTotalOrient += ABSF(fPlayerLoopDiff)
	PS_Main.myObjectiveData.fPlanePrevOrient = fPlayerCurrentPitch
	
ENDPROC

PROC PS_UPDATE_IMMELMAN_TURN_TRACKING(VECTOR vPlayerCurrentSide, VECTOR fPlayerCurrentRotation, FLOAT fPlayerCurrentRoll, FLOAT fPlayerCurrentPitch)
	TempObjMeterHack(fPlayerCurrentRotation, fPlayerCurrentRoll, fPlayerCurrentPitch)
	
	FLOAT fPlayerLoopDiff = fPlayerCurrentPitch-PS_Main.myObjectiveData.fPlanePrevOrient

	//because of the jump from Loop HUGE to TINY
	IF fPlayerLoopDiff > 270.0
		fPlayerLoopDiff -= 360.0
	ELIF fPlayerLoopDiff < -270.0
		fPlayerLoopDiff += 360.0
	ENDIF
	
	//check we're not rolling the plane
	
	IF ABSF(vPlayerCurrentSide.z) > 0.819 // cos(35)
		PS_Main.myObjectiveData.fPlaneTotalOrient = 0
		PS_SET_OBJECTIVE_RETRY_FLAG(TRUE)
		EXIT
	ENDIF
	
	//If the pitch dif is pos, we're looping with our nose going up, otherwise we're looping with our nose going down
	IF fPlayerLoopDiff > PS_LOOPING_MIN_PITCH_DIFFERENCE
		
		IF PS_Main.myObjectiveData.bCurrentlyClockwiseOrient
			IF PS_Main.myObjectiveData.fPlaneTotalOrient > PS_RETRY_STUNT_ANGLE
			OR (fPlayerCurrentPitch < -45 AND fPlayerCurrentPitch > -135)
				PS_Main.myObjectiveData.fPlaneTotalOrient = 0
				PS_SET_OBJECTIVE_RETRY_FLAG(TRUE)
			ENDIF
			PS_Main.myObjectiveData.fPlaneTotalOrient = fPlayerCurrentPitch//0.0
			PS_Main.myObjectiveData.iTotalOrientCount += PS_Main.myObjectiveData.iCurrentOrientCount
			PS_Main.myObjectiveData.iCurrentOrientCount = 0
		ENDIF
		
		PS_Main.myObjectiveData.bCurrentlyClockwiseOrient = FALSE
	ELIF fPlayerLoopDiff < -PS_LOOPING_MIN_PITCH_DIFFERENCE
		
		IF NOT PS_Main.myObjectiveData.bCurrentlyClockwiseOrient
			IF PS_Main.myObjectiveData.fPlaneTotalOrient > PS_RETRY_STUNT_ANGLE
			OR (fPlayerCurrentPitch < -45 AND fPlayerCurrentPitch > -135)
				PS_Main.myObjectiveData.fPlaneTotalOrient = 0
				PS_SET_OBJECTIVE_RETRY_FLAG(TRUE)
			ENDIF
			PS_Main.myObjectiveData.fPlaneTotalOrient = fPlayerCurrentPitch//0.0
			PS_Main.myObjectiveData.iTotalOrientCount += PS_Main.myObjectiveData.iCurrentOrientCount
			PS_Main.myObjectiveData.iCurrentOrientCount = 0
		ENDIF
		
		PS_Main.myObjectiveData.bCurrentlyClockwiseOrient = TRUE
	ENDIF

	PS_Main.myObjectiveData.iCurrentOrientCount	= FLOOR(ABSF(PS_Main.myObjectiveData.fPlaneTotalOrient)/LOOP_IMMELMAN_ANGLE)
	IF PS_Main.myObjectiveData.iCurrentOrientCount = 0 AND ABSF(PS_Main.myObjectiveData.fPlaneTotalOrient)/LOOP_IMMELMAN_ANGLE > 0.75
		//check if plane is level-ish
		IF fPlayerCurrentPitch > -5 AND fPlayerCurrentRoll < 10
			PS_Main.myObjectiveData.iCurrentOrientCount++
			PS_Main.myObjectiveData.fPlaneTotalOrient = 0
			PS_Main.myObjectiveData.fPlanePrevOrient = 0
		ENDIF
	ENDIF
	fPlayerLoopDiff = CLAMP(fPlayerLoopDiff, -2.0, 2.0)
	PS_Main.myObjectiveData.fPlanePrevOrient		= fPlayerCurrentPitch
	FLOAT tempTotalOrient = PS_Main.myObjectiveData.fPlaneTotalOrient	+ fPlayerLoopDiff	
	IF tempTotalOrient > 0
		PS_Main.myObjectiveData.fPlaneTotalOrient	= tempTotalOrient
	ELSE
		PS_Main.myObjectiveData.fPlaneTotalOrient	= 0 
	ENDIF
	
ENDPROC

PROC PS_UPDATE_IMMELMAN_ROLL_TRACKING(VECTOR fPlayerCurrentRotation, FLOAT fPlayerCurrentRoll, FLOAT fPlayerCurrentPitch)
	TempObjMeterHack(fPlayerCurrentRotation, fPlayerCurrentRoll, fPlayerCurrentPitch)
	
ENDPROC

PROC PS_UPDATE_FORMATION_TRACKING(VECTOR fPlayerCurrentRotation, FLOAT fPlayerCurrentRoll, FLOAT fPlayerCurrentPitch)
	TempObjMeterHack(fPlayerCurrentRotation, fPlayerCurrentRoll, fPlayerCurrentPitch)
	
ENDPROC

//updates the info the objective meter displays. updates depending on what type
PROC PS_UPDATE_OBJECTIVE_METER_DATA()
	IF NOT IS_VEHICLE_DRIVEABLE(PS_Main.myVehicle)
		PRINTLN(">>>>> Can't update objective meter the vehicle is undriveable!!!!")
		EXIT
	ENDIF
	
	VECTOR fPlayerCurrentRotation = GET_ENTITY_ROTATION(PS_Main.myVehicle)
	FLOAT fPlayerCurrentRoll = GET_ENTITY_ROLL(PS_Main.myVehicle)
	FLOAT fPlayerCurrentPitch = GET_ENTITY_PITCH(PS_Main.myVehicle)

	VECTOR vForward, vSide, vUp, vPos
	GET_ENTITY_MATRIX(PS_Main.myVehicle, vForward, vSide, vUp, vPos)
	
	SWITCH PS_UI_ObjectiveMeter.eObjMeterType
		CASE PS_OBJECTIVE_TYPE_BARREL_ROLL
			PS_UPDATE_ROLL_TRACKING(fPlayerCurrentRotation, fPlayerCurrentRoll, fPlayerCurrentPitch)
			BREAK
		
		CASE PS_OBJECTIVE_TYPE_INVERTED
			PS_UPDATE_INVERTED_TRACKING(fPlayerCurrentRotation, fPlayerCurrentRoll, fPlayerCurrentPitch)
			BREAK
		
		CASE PS_OBJECTIVE_TYPE_KNIFE
			IF PS_IS_PLANE_KNIFING_LEFT()
				PS_UPDATE_KNIFE_LEFT_TRACKING(fPlayerCurrentRotation, fPlayerCurrentRoll, fPlayerCurrentPitch)
			ELIF PS_IS_PLANE_KNIFING_RIGHT()
				PS_UPDATE_KNIFE_RIGHT_TRACKING(fPlayerCurrentRotation, fPlayerCurrentRoll, fPlayerCurrentPitch)
			ELSE
				IF IS_TIMER_STARTED(PS_Main.myObjectiveData.tObjectiveTimer)
					CANCEL_TIMER(PS_Main.myObjectiveData.tObjectiveTimer)
				ENDIF
			ENDIF
			BREAK
		
		CASE PS_OBJECTIVE_TYPE_KNIFE_L
			PS_UPDATE_KNIFE_LEFT_TRACKING(fPlayerCurrentRotation, fPlayerCurrentRoll, fPlayerCurrentPitch)
			BREAK
		
		CASE PS_OBJECTIVE_TYPE_KNIFE_R
			PS_UPDATE_KNIFE_RIGHT_TRACKING(fPlayerCurrentRotation, fPlayerCurrentRoll, fPlayerCurrentPitch)
			BREAK
		
		CASE PS_OBJECTIVE_TYPE_INSIDE_LOOP
			fPlaneLastOrient = PS_Main.myObjectiveData.fPlaneTotalOrient
			PS_UPDATE_INSIDE_LOOP_TRACKING(vSide, fPlayerCurrentRotation, fPlayerCurrentRoll, fPlayerCurrentPitch)
			BREAK
		
		CASE PS_OBJECTIVE_TYPE_IMMELMAN_TURN
			fPlaneLastOrient = PS_Main.myObjectiveData.fPlaneTotalOrient
			PS_UPDATE_INSIDE_LOOP_TRACKING(vSide, fPlayerCurrentRotation, fPlayerCurrentRoll, fPlayerCurrentPitch)
			BREAK
		
		CASE PS_OBJECTIVE_TYPE_IMMELMAN_ROLL
			//stuff
			BREAK
		
		CASE PS_OBJECTIVE_TYPE_FORMATION
			//stuff
			BREAK
		
		CASE PS_OBJECTIVE_TYPE_OUTSIDE_LOOP
			fPlaneLastOrient = PS_Main.myObjectiveData.fPlaneTotalOrient
			PS_UPDATE_OUTSIDE_LOOP_TRACKING(vSide, fPlayerCurrentRotation, fPlayerCurrentRoll, fPlayerCurrentPitch)
			BREAK	
	ENDSWITCH
		
ENDPROC
