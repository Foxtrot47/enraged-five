
USING "commands_stats.sch"
USING "socialclub_leaderboard.sch"
USING "net_leaderboards.sch"
USING "buildtype.sch"

ENUM PS_LB_TYPE
	PS_LB_TIME,
	PS_LB_DISTANCE
ENDENUM

//PS_LB_TYPE myLBType
BOOL							bIsPSLeaderboardWriting					= FALSE
SC_LEADERBOARD_CONTROL_STRUCT psLBControl
INT iBS = 0

PROC PS_WRITE_DATA_TO_LEADERBOARD(PILOT_SCHOOL_DLC_CLASSES_ENUM_NEW eLesson, INT iScore, INT iTime = 0, FLOAT fDistance = 0.0)
	PRINTLN("PS_WRITE_TIME_TO_LEADERBOARD: Writing leaderboards for ")
	/*
		PICK_STRING(eLesson = PSCD_Takeoff, "Takeoff", 
		PICK_STRING(eLesson = PSCD_Landing, "Landing", 
		PICK_STRING(eLesson = PSCD_Inverted, "Inverted",
		PICK_STRING(eLesson = PSCD_Knifing, "Knifing",
		PICK_STRING(eLesson = PSCD_loopTheLoop, "Loop the Loop",
		PICK_STRING(eLesson = PSCD_FlyLow, "Fly Low",
		PICK_STRING(eLesson = PSCD_DaringLanding, "Daring Landing",
		PICK_STRING(eLesson = PSCD_heliCourse, "Heli Obstacle Course",
		PICK_STRING(eLesson = PSCD_heliSpeedRun, "Heli Speed Run",
		PICK_STRING(eLesson = PSCD_parachuteOntoTarget, "Skydiving",
		PICK_STRING(eLesson = PSCD_chuteOntoMovingTarg, "Skydiving - Moving Target",
		PICK_STRING(eLesson = PSCD_planeCourse, "Plane Obstacle Course", "Invalid Enum")))))))))))))
	*/ //removed for mp pilot school
	
	IF iTime < 0
		iTime = 0
	ENDIF
	
	INT iLeaderboardID
	
	//if we have a really big distance, reduce it to 999,999
	IF fDistance < 0 OR fDistance > 999999
		fDistance = 999999
	ENDIF

	
	INT numBronzeMedals = 0, numSilverMedals = 0, numGoldMedals = 0
		
//	PRINTLN("iScore to send to LBs is... ", iScore)
	PRINTLN("Writing for Lesson: ",eLesson)
	PRINTLN("iTime to send to LBs is... ", iTime)
	PRINTLN("fDistance to send to LBs is... ", fDistance)
	
	
	IF iScore >= iSCORE_FOR_GOLD
		numGoldMedals++
	ELIF iScore >= iSCORE_FOR_SILVER
		numSilverMedals++
	ELIF iScore >= iSCORE_FOR_BRONZE
		numBronzeMedals++
	ENDIF
	
	// NOW PASS THE DATA IN FOR SUBMISSION
	TEXT_LABEL_23 sIdentifier[1]
	TEXT_LABEL_31 categoryName[1]
	categoryName[0] = ""  //not sure it needed this??
	// Begin setting the leaderboard data types
	SWITCH eLesson
		// DLC	
		CASE PSCD_DLC_OutsideLoop 
			//myLBType = PS_LB_TIME
		 	sIdentifier[0] = "LESSON_DLC_1"
			iLeaderboardID = ENUM_TO_INT(LEADERBOARD_FLIGHT_SCHOOL_OUTSIDE_LOOP)
			BREAK
		CASE PSCD_DLC_CollectFlags 
			//myLBType = PS_LB_TIME
			sIdentifier[0]  = "LESSON_DLC_2"
			iLeaderboardID = ENUM_TO_INT(LEADERBOARD_FLIGHT_SCHOOL_COLLECT_FLAGS)
			BREAK
		CASE PSCD_DLC_FollowLeader
			//myLBType = PS_LB_TIME
			sIdentifier[0]  = "LESSON_DLC_3"
			iLeaderboardID = ENUM_TO_INT(LEADERBOARD_FLIGHT_SCHOOL_FOLLOW_LEADER)
			BREAK
		CASE PSCD_DLC_FlyLow
			//myLBType = PS_LB_DISTANCE
			sIdentifier[0]  = "LESSON_DLC_4"
			iLeaderboardID = ENUM_TO_INT(LEADERBOARD_FLIGHT_SCHOOL_GROUND_LEVEL)
			BREAK
		CASE PSCD_DLC_VehicleLanding
			//myLBType = PS_LB_TIME
			sIdentifier[0]  = "LESSON_DLC_5"
			iLeaderboardID = ENUM_TO_INT(LEADERBOARD_FLIGHT_SCHOOL_MOVING_LANDING)
			BREAK
		CASE PSCD_DLC_CityLanding 
			//myLBType = PS_LB_DISTANCE
			sIdentifier[0]  = "LESSON_DLC_6"
			iLeaderboardID = ENUM_TO_INT(LEADERBOARD_FLIGHT_SCHOOL_CITY_LANDING)
			BREAK
		CASE PSCD_DLC_ChaseParachute
			//myLBType = PS_LB_TIME 
			sIdentifier[0]  = "LESSON_DLC_7"
			iLeaderboardID = ENUM_TO_INT(LEADERBOARD_FLIGHT_SCHOOL_CHASE_PARACHUTE)
			BREAK
		CASE PSCD_DLC_Engine_failure 
			//myLBType = PS_LB_DISTANCE
			sIdentifier[0]  = "LESSON_DLC_8"
			iLeaderboardID = ENUM_TO_INT(LEADERBOARD_FLIGHT_SCHOOL_ENGINE_FAILURE)
			BREAK
		CASE PSCD_DLC_ShootingRange 
			//myLBType = PS_LB_TIME
			sIdentifier[0]  = "LESSON_DLC_9"
			iLeaderboardID = ENUM_TO_INT(LEADERBOARD_FLIGHT_SCHOOL_SHOOTING_RANGE)
			BREAK
		CASE PSCD_DLC_Formation
			//myLBType = PS_LB_TIME
			sIdentifier[0]  = "LESSON_DLC_10"
			iLeaderboardID = ENUM_TO_INT(LEADERBOARD_FLIGHT_SCHOOL_FORMATION_FLIGHT)
			BREAK		
	ENDSWITCH
	IF INIT_LEADERBOARD_WRITE(INT_TO_ENUM(LEADERBOARDS_ENUM,iLeaderboardID), sIdentifier, categoryName, 0, DEFAULT, TRUE)
		SWITCH eLesson
			// DLC	
			CASE PSCD_DLC_OutsideLoop
			CASE PSCD_DLC_FollowLeader
			CASE PSCD_DLC_VehicleLanding
			CASE PSCD_DLC_CollectFlags
				//
				WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(iLeaderboardID, LB_INPUT_COL_SCORE_DOUBLE, 0, TO_FLOAT(-iTime))

				//Time in MS 
				WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(iLeaderboardID, LB_INPUT_COL_BEST_TIME, iTime, 0)
			
				//write 1 if player got a bronze
				WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(iLeaderboardID, LB_INPUT_COL_BRONZE_MEDALS, numBronzeMedals, 0)

				//write 1 if player got a silver
				WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(iLeaderboardID, LB_INPUT_COL_SILVER_MEDALS, numSilverMedals, 0)

				//writing 1 if gold medal
				
				WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(iLeaderboardID, LB_INPUT_COL_GOLD_MEDALS, numGoldMedals, 0)

				
				//write 1 for each attempt
				WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(iLeaderboardID, LB_INPUT_COL_NUM_MATCHES, 1, 0)

				WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(iLeaderboardID, LB_INPUT_COL_MEDAL, CLAMP_INT(numBronzeMedals + numSilverMedals*2 + numGoldMedals*3, 0, 3), 0)

			BREAK
			CASE PSCD_DLC_Engine_failure
			CASE PSCD_DLC_ChaseParachute
			CASE PSCD_DLC_CityLanding
			CASE PSCD_DLC_FlyLow //column input is still called distance so can use functionality
				PRINTLN("fDistance is.... ", fDistance)
				PRINTLN("Calling LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_SCORE_DOUBLE, 0, ", -fDistance)
				WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(iLeaderboardID, LB_INPUT_COL_SCORE_DOUBLE, 0, -fDistance) //is this correct you want least distance as best?

				//write accuracy
				PRINTLN("Calling LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_DISTANCE, 0, ", fDistance)
				WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(iLeaderboardID, LB_INPUT_COL_DISTANCE, 0,  fDistance)
				
				//write 1 if player got a bronze
				WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(iLeaderboardID, LB_INPUT_COL_BRONZE_MEDALS, numBronzeMedals, 0)

				//write 1 if player got a silver
				WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(iLeaderboardID, LB_INPUT_COL_SILVER_MEDALS, numSilverMedals, 0)

				//writing 1 if gold medal
				WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(iLeaderboardID, LB_INPUT_COL_GOLD_MEDALS, numGoldMedals, 0)

				//write 1 for each attempt
				WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(iLeaderboardID, LB_INPUT_COL_NUM_MATCHES, 1, 0)

				WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(iLeaderboardID, LB_INPUT_COL_MEDAL, CLAMP_INT(numBronzeMedals + numSilverMedals*2 + numGoldMedals*3, 0, 3), 0)			
			BREAK

			CASE PSCD_DLC_ShootingRange
			CASE PSCD_DLC_Formation
				//
				WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(iLeaderboardID, LB_INPUT_COL_SCORE_DOUBLE, 0, fDistance)
			
				//write 1 if player got a bronze
				WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(iLeaderboardID, LB_INPUT_COL_BRONZE_MEDALS, numBronzeMedals, 0)

				//write 1 if player got a silver
				WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(iLeaderboardID, LB_INPUT_COL_SILVER_MEDALS, numSilverMedals, 0)

				//writing 1 if gold medal
				
				WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(iLeaderboardID, LB_INPUT_COL_GOLD_MEDALS, numGoldMedals, 0)

				
				//write 1 for each attempt
				WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(iLeaderboardID, LB_INPUT_COL_NUM_MATCHES, 1, 0)

				WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(iLeaderboardID, LB_INPUT_COL_MEDAL, CLAMP_INT(numBronzeMedals + numSilverMedals*2 + numGoldMedals*3, 0, 3), 0)
			BREAK		
		ENDSWITCH
	ENDIF
ENDPROC

//Name: MINI_GAMES_FLIGHT_SCHOOL_BY_DIST
//ID: 192 - LEADERBOARD_MINI_GAMES_FLIGHT_SCHOOL_BY_DIST
//Inputs: 8
//	LB_INPUT_COL_SCORE (long)
//	LB_INPUT_COL_BEST_TIME (long)
//	LB_INPUT_COL_DISTANCE (int)
//	LB_INPUT_COL_BRONZE_MEDALS (int)
//	LB_INPUT_COL_SILVER_MEDALS (int)
//	LB_INPUT_COL_GOLD_MEDALS (int)
//	LB_INPUT_COL_NUM_MATCHES (int)
//	LB_INPUT_COL_MEDAL (int)
//Columns: 8
//	SCORE_COLUMN ( AGG_Max ) - InputId: LB_INPUT_COL_SCORE
//	BEST_TIME ( AGG_Min ) - InputId: LB_INPUT_COL_BEST_TIME
//	BEST_LANDING_ACC ( AGG_Max ) - InputId: LB_INPUT_COL_DISTANCE
//	NUM_BRONZE_MEDALS ( AGG_Sum ) - InputId: LB_INPUT_COL_BRONZE_MEDALS
//	NUM_SILVER_MEDALS ( AGG_Sum ) - InputId: LB_INPUT_COL_SILVER_MEDALS
//	NUM_GOLD_MEDALS ( AGG_Sum ) - InputId: LB_INPUT_COL_GOLD_MEDALS
//	NUM_ATTEMPTS ( AGG_Sum ) - InputId: LB_INPUT_COL_NUM_MATCHES
//	MEDAL ( AGG_Last ) - InputId: LB_INPUT_COL_MEDAL
//Instances: 3 
//	Location
//	Location,Challenge
//	Location,ScEvent
//
//-----------------------------------------------------
//Name: MINI_GAMES_FLIGHT_SCHOOL_BY_TIME
//ID: 850 - LEADERBOARD_MINI_GAMES_FLIGHT_SCHOOL_BY_TIME
//Inputs: 8
//	LB_INPUT_COL_SCORE (long)
//	LB_INPUT_COL_BEST_TIME (long)
//	LB_INPUT_COL_DISTANCE (int)
//	LB_INPUT_COL_BRONZE_MEDALS (int)
//	LB_INPUT_COL_SILVER_MEDALS (int)
//	LB_INPUT_COL_GOLD_MEDALS (int)
//	LB_INPUT_COL_NUM_MATCHES (int)
//	LB_INPUT_COL_MEDAL (int)
//Columns: 8
//	SCORE_COLUMN ( AGG_Max ) - InputId: LB_INPUT_COL_SCORE
//	BEST_TIME ( AGG_Min ) - InputId: LB_INPUT_COL_BEST_TIME
//	BEST_LANDING_ACC ( AGG_Max ) - InputId: LB_INPUT_COL_LAND_ACCURACY
//	NUM_BRONZE_MEDALS ( AGG_Sum ) - InputId: LB_INPUT_COL_BRONZE_MEDALS
//	NUM_SILVER_MEDALS ( AGG_Sum ) - InputId: LB_INPUT_COL_SILVER_MEDALS
//	NUM_GOLD_MEDALS ( AGG_Sum ) - InputId: LB_INPUT_COL_GOLD_MEDALS
//	NUM_ATTEMPTS ( AGG_Sum ) - InputId: LB_INPUT_COL_NUM_MATCHES
//	MEDAL ( AGG_Last ) - InputId: LB_INPUT_COL_MEDAL
//Instances: 3
//	Location
//	Location,Challenge
//	Location,ScEvent
//



PROC PS_LOAD_SOCIAL_CLUB_LEADERBOARD(PILOT_SCHOOL_DLC_CLASSES_ENUM_NEW eLesson, STRING sTitle)
	TEXT_LABEL_23 sUniqueIdentifier
	INT fmmcType
	SWITCH eLesson
		
		
		
		// DLC
		CASE PSCD_DLC_OutsideLoop //DLC1
			//fmmcType = FMMC_TYPE_SP_FLIGHT_SCHOOL_TIME
			fmmcType = FMMC_TYPE_MG_PILOT_SCHOOL
		 	sUniqueIdentifier = "LESSON_DLC_1"
			sTitle = "PSLBT_DLC_1"
			BREAK
		CASE PSCD_DLC_FollowLeader //DLC2
			//fmmcType = FMMC_TYPE_SP_FLIGHT_SCHOOL_TIME
			fmmcType = FMMC_TYPE_MG_PILOT_SCHOOL
			sUniqueIdentifier = "LESSON_DLC_2"
			sTitle = "PSLBT_DLC_2"
			BREAK
		CASE PSCD_DLC_VehicleLanding //DLC3
			//fmmcType = FMMC_TYPE_SP_FLIGHT_SCHOOL_DIST
			fmmcType = FMMC_TYPE_MG_PILOT_SCHOOL
			sUniqueIdentifier = "LESSON_DLC_3"
			sTitle = "PSLBT_DLC_3"
			BREAK
		CASE PSCD_DLC_CollectFlags//DLC4
			//fmmcType = FMMC_TYPE_SP_FLIGHT_SCHOOL_TIME
			fmmcType = FMMC_TYPE_MG_PILOT_SCHOOL
			sUniqueIdentifier = "LESSON_DLC_4"
			sTitle = "PSLBT_DLC_4"
			BREAK
		CASE PSCD_DLC_Engine_failure //DLC5
			//fmmcType = FMMC_TYPE_SP_FLIGHT_SCHOOL_TIME
			fmmcType = FMMC_TYPE_MG_PILOT_SCHOOL
			sUniqueIdentifier = "LESSON_DLC_5"
			sTitle = "PSLBT_DLC_5"
			BREAK
		CASE PSCD_DLC_ChaseParachute //DLC6
			//fmmcType = FMMC_TYPE_SP_FLIGHT_SCHOOL_TIME
			fmmcType = FMMC_TYPE_MG_PILOT_SCHOOL
			sUniqueIdentifier = "LESSON_DLC_6"
			sTitle = "PSLBT_DLC_6"
			BREAK
		CASE PSCD_DLC_FlyLow //DLC7
			//fmmcType = FMMC_TYPE_SP_FLIGHT_SCHOOL_DIST
			fmmcType = FMMC_TYPE_MG_PILOT_SCHOOL
			sUniqueIdentifier = "LESSON_DLC_7"
			sTitle = "PSLBT_DLC_7"
			BREAK
		CASE PSCD_DLC_ShootingRange //DLC8
			//fmmcType = FMMC_TYPE_SP_FLIGHT_SCHOOL_TIME
			fmmcType = FMMC_TYPE_MG_PILOT_SCHOOL
			sUniqueIdentifier = "LESSON_DLC_8"
			sTitle = "PSLBT_DLC_8"
			BREAK
		CASE PSCD_DLC_CityLanding //DLC9
			//fmmcType = FMMC_TYPE_SP_FLIGHT_SCHOOL_TIME
			fmmcType = FMMC_TYPE_MG_PILOT_SCHOOL
			sUniqueIdentifier = "LESSON_DLC_9"
			sTitle = "PSLBT_DLC_9"
			BREAK
		CASE PSCD_DLC_Formation //DLC10
			//fmmcType = FMMC_TYPE_SP_FLIGHT_SCHOOL_DIST
			fmmcType = FMMC_TYPE_MG_PILOT_SCHOOL
			sUniqueIdentifier = "LESSON_DLC_10"
			sTitle = "PSLBT_DLC_10"
			BREAK
					
	ENDSWITCH
	
	SETUP_SOCIAL_CLUB_LEADERBOARD_READ_DATA(psLBControl, fmmcType, sUniqueIdentifier, sTitle,ENUM_TO_INT(eLesson))
ENDPROC


FUNC BOOL PS_DO_RANK_PREDICTION(PILOT_SCHOOL_DLC_CLASSES_ENUM_NEW eLesson, INT iScore, INT iTime = 0, FLOAT fDistance = 0.0)
	
	IF scLB_rank_predict.bFinishedRead
	AND NOT scLB_rank_predict.bFinishedWrite
		
		PS_WRITE_DATA_TO_LEADERBOARD(eLesson, iScore, iTime, fDistance)
		
		#IF IS_DEBUG_BUILD
		PRINTLN("GET_RANK_PREDICTION_DETAILS CURRENT RUN values (JUST AFTER WRITE) " ) 
			PRINT_RANK_PREDICTION_STRUCT(scLB_rank_predict.currentResult)
		#ENDIF
	
		scLB_rank_predict.bFinishedWrite = TRUE
	ENDIF
	IF GET_RANK_PREDICTION_DETAILS(psLBControl)
		sclb_useRankPrediction = TRUE
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC


PROC PS_DISPLAY_SOCIAL_CLUB_LEADERBOARD(SCALEFORM_INDEX &uiLeaderboard, PILOT_SCHOOL_DLC_CLASSES_ENUM_NEW eLesson, STRING sTitle)
 	DISABLE_FRONTEND_THIS_FRAME() //prevent player from pulling up the pausmenu while LBs are loading
	PS_LOAD_SOCIAL_CLUB_LEADERBOARD(eLesson, sTitle)
	DRAW_SC_SCALEFORM_LEADERBOARD(uiLeaderboard, psLBControl)
ENDPROC

PROC PS_CLEANUP_SOCIAL_CLUB_LEADERBOARD()
	scLB_DisplayedData.bResetTriggered = TRUE
	CLEANUP_SOCIAL_CLUB_LEADERBOARD(psLBControl)
	SOCIAL_CLUB_CLEAR_CONTROL_STRUCT(psLBControl)
	SOCIAL_CLUB_CLEAR_NAVIGATION_STRUCT(psLBControl)
	CLEAR_FINAL_DATA_STRUCT(tempFinalData)
ENDPROC

PROC PS_CLEANUP_LEADERBOARD(SCALEFORM_INDEX &uiLeaderboard)
	CLEANUP_SC_LEADERBOARD_UI(uiLeaderboard)
ENDPROC

