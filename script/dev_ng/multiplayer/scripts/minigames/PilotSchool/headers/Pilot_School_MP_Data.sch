



//****************************************************************************************************
//
//	Author: 		Asa Dang
//	Date: 			6/24/11
//	Description:	Contains shared funcs and procs for data management in the pilot school, including load/save player data
//					and retrieving any stored data (strings, scores, times, etc)
//	
//****************************************************************************************************


//USING "globals.sch"
//USING "rage_builtins.sch"
USING "minigame_uiinputs.sch"
USING "socialclub_leaderboard.sch"
USING "Pilot_School_MP_Definitions.sch"
USING "Pilot_School_MP_Objhelpalogue.sch"
USING "minigame_big_message.sch"
USING "screen_placements.sch"
USING "UIUtil.sch"
USING "script_usecontext.sch"
USING "ps_MP_sc_leaderboard_lib.sch"
//USING "globals_sp_pilotschool.sch"

//************************************************************
// DLC for multiplayer
//enumCharacterList g_playerPedEnum
BOOL g_bApplyNewMedalMultiplier
INT g_LastMedalAchieved
BOOL g_selectNextClass
//************************************************************

//************************************************************
// General Variables & Assets
//************************************************************

VECTOR							VECTOR_ZERO								= <<0.0, 0.0, 0.0>>
MODEL_NAMES 					PilotModel 								= S_M_M_PILOT_02
TEXT_LABEL						txdDictName								= "pilotSchool"
MODEL_NAMES 					VehicleToUse 							= STUNT

//************************************************************
// Game Logic
//************************************************************


UI_INPUT_DATA					PS_uiInput //tracks analog input
PS_CHALLENGE_SUBSTATE 			eSubState 								= PS_SUBSTATE_ENTER
structTimer						Fail_Timer_Runway, Fail_Timer_Landed, Fail_Timer_Range, Fail_Timer_Water, Fail_Timer_Idling
VECTOR 							vPilotSchoolSceneCoords
VECTOR							vStartPosition
VECTOR							vStartRotation 
FLOAT							fStartHeading


SCENARIO_BLOCKING_INDEX sb_PS
//CAM_VIEW_MODE					ePS_CamViewMode

BLIP_INDEX						strip_blip[10]

STREAMVOL_ID streamVolume

SCENARIO_BLOCKING_INDEX			scenBlockHelipad 						= NULL

structTimer						streamingTimer

INT								iNumMissedCheckpoints					= 0	
INT								iRangeTimerWarning						= 1
INT								iSceneID
//INT								iFailDialogue							= 1
BOOL							bRetryChallenge							= FALSE
BOOL							bKeepVehicleSetup						= FALSE
BOOL							bPlayerDataHasBeenUpdated				= FALSE
BOOL							bPlayerInVehicle						= FALSE
BOOL 							bStoppedNearVecEndCoords
BOOL 							bEnteredLocate 							= FALSE
BOOL 							bFinishedChallenge 						= FALSE
BOOL							bGameIsRunning							= TRUE
#IF IS_DEBUG_BUILD	
BOOL 							bPlayerUsedDebugSkip 					= FALSE
#ENDIF
PS_CHALLENGE_SUBSTATE			eGameSubState							= PS_SUBSTATE_ENTER
PILOT_SCHOOL_GAME_MODE			eGameMode								= PS_GAME_MODE_INTRO_SCENE

//pilot model
INT								iCurrentChallengeScore					= 0
INT 							g_PS_initStage							= 0
FLOAT							GroundOffset
FLOAT							fCheckpointDistAway 					= -1.0

FLOAT							fPlaneLastOrient

PS_MAIN_STRUCT 					PS_Main
PS_PLAYER_DATA_STRUCT			saveData[NUMBER_OF_DLC_PILOT_SCHOOL_CLASSES]
INT 							iChallengeScores[NUMBER_OF_DLC_PILOT_SCHOOL_CLASSES]

//camera views of stunt and helicopter before the player starts a lesson.
CAM_VIEW_MODE					cvmHelicopter
CAM_VIEW_MODE					cvmStuntPlane

//Out of range vector
VECTOR							vInRangeCoord							= <<0, 0, 0>>

//************************************************************
// Cutscene stuff
//************************************************************

// Skydiving Cutscene
//********************
//VECTOR							PS_CARGOPLANE_PLAYER_START_OFFSET		= <<0.0, -22.58, -3.15>>//<<0.0, -38.1477, -4.8>>//<<0, -39.0, -5.0>> //starts the player -20m behind the center of the plane (right around the cargo doors)
VECTOR 							PS_PARACHUTE_JUMP_CAM_OFFSET_1			= <<-12.6533, -49.3726, -16.0138>> //starts the camera 10m to the right, 40m behind and 10m below the plane
VECTOR 							PS_PARACHUTE_JUMP_CAM_OFFSET_2			= <<-5.0812, -36.9571, -0.4403>>//<<0, -50, -20>> //starts the camera 50m back and 20m below
VECTOR 							PS_PARACHUTE_JUMP_CAM_TARG_OFFSET_1		= <<-10.9340, -47.6023, -14.3079>>
VECTOR 							PS_PARACHUTE_JUMP_CAM_TARG_OFFSET_2		= <<-3.2242, -38.1790, -2.4549>>
structTimer						tRetryInstructionTimer
PS_PARACHUTE_FLIGHT_CHALLENGE 	eParachutePreview


// Preview Cutscenes
//********************
VECTOR 							vPilotSchoolPreviewCoords
VECTOR							vPilotSchoolPreviewRot
VECTOR							vPreviewCamCoords
VECTOR							vPreviewCamDirection
//VECTOR							vChallengeScenePos
//VECTOR							vChallengeSceneRot
FLOAT							fPreviewVehicleSkipTime					= 0.0
FLOAT							fPreviewTime							= PS_DEFAULT_PREVIEW_TIME
BOOL							bSkippedPreview							= FALSE
BOOL							bPreviewCleanupOkay						= FALSE
CAMERA_INDEX					cPS_Parachute_Jump_Cam
//structTimer						tJumpCamTimer
//PS_PARACHUTE_JUMP_CUT_STATE		ePS_Parachute_Jump_State
PED_PARACHUTE_STATE 			ppsPlayerParachuteState
TEXT_PLACEMENT					previewTitlePlacement
TEXT_STYLE						previewTitleStyle
STRING							sPreviewRecordingName = "PilotSchool"
CAMERA_INDEX					PS_CutsceneCamera
structTimer						tPS_PreviewTimer
PS_STARTING_CUTSCENE_STATE		PS_CutsceneSkipState			 		= PS_STARTING_CUTSCENE_STATE_01
PS_PREVIEW_STATE				PS_PreviewState							= PS_PREVIEW_INIT
INT								iPreviewCheckpointIdx					= 0
INT								iPS_PreviewVehicleRecordingID			= 0

//flying under bridge for preview cutscene
INT								iPSLocalBridgesFlowUnderFlags			= 0
INT								iPSLocalBridgesFlowUnderFlags2			= 0
INT								iPSLocalBridgesFlowUnderFlags3			= 0
BOOL							bPSGlobalBridgesFlownUnderModified		= FALSE

//************************************************************
// HUD/UI Stuff
//************************************************************


//Dialogue stuff
//keeps track of dialogue we've already played
INT iPlayedOutEncourageDialogue[PS_FEEDBACK_DIALOGUE_REPLAY_COUNT]
INT iPlayedOutRewardDialogue[PS_FEEDBACK_DIALOGUE_REPLAY_COUNT]
INT iPlayedOutDiscourageDialogue[PS_FEEDBACK_DIALOGUE_REPLAY_COUNT]
 
// Front End UI
//********************
CONST_INT						PS_QUIT_UI_TRANS_TIME 					1000

VECTOR							PS_MENU_PLAYER_COORD 					= <<-1154.1101, -2715.2026, 18.8824>>
//UI_INPUT_DATA					uiInput
PS_CHALLENGE_DATA_STRUCT		PS_Challenges[NUMBER_OF_DLC_PILOT_SCHOOL_CLASSES]
//PS_PLAYER_DATA_STRUCT			saveDatA[NUMBER_OF_DLC_PILOT_SCHOOL_CLASSES]
MEGA_PLACEMENT_TOOLS			PS_UI_Placement
SCALEFORM_INDEX 				quitUI 									= NULL

//SIMPLE_USE_CONTEXT				quitInstructions
SIMPLE_USE_CONTEXT 				menuInstructions
BOOL 							bEndScreenQuitAlert								= FALSE

//structTimer						tPSQuitUITimer

BOOL 							bIsLaunchingChallenge 					= FALSE
BOOL 							bShowQuitMenu 							= FALSE
//BOOL 							bDontFadeToMenu							= TRUE //We're going to use this if we need to fade out before returning to the menu (if the player is on screen)
BOOL							bLBToggle								= FALSE
BOOL							bLBViewProfile							= FALSE
//BOOL 							bIsQuitTrans 							= FALSE
//BOOL 							bIsWaitingToQuit 						= FALSE
//BOOL 							bShowQuitInstructions					= FALSE
INT								iHourGlassTimeToRun						= 0
INT								iHourGlassPadding						= 0 //so the hour glass doesn't look empty before reaching 0
structTimer 					tRetryMenuTransition
BOOL							bPilotSchoolDLCSelected					= FALSE

INT								iPilotSchoolFailTimeSaveData
INT								iPilotSchoolPreviousFailTimeSaveData
SCALEFORM_INDEX					siPilotSchoolButtons
SCALEFORM_INSTRUCTIONAL_BUTTONS sibPilotSchoolButtons

CONST_FLOAT						ciPILOT_SCHOOL_FAIL_TIME				8.0

// End Results UI AKA Score Card
//********************
MEGA_PLACEMENT_TOOLS			PSER_Placement
#IF IS_DEBUG_BUILD
	WIDGET_GROUP_ID				PSER_Widget
#ENDIF
BOOL							bIsScoreCardVisible 					= FALSE
STRING							PS_Special_Fail_Reason					= ""
//SCRIPT_SCALEFORM_MEDAL_TOAST	PS_UI_MedalToast
ENDING_SCREEN_STATE				eScoreCardState							= PS_ENDING_INIT

SCALEFORM_INDEX					PS_UI_Leaderboard
CAMERA_INDEX					PS_EndCutCam, PS_EndCutCam1


PILOT_SCHOOL_DLC_CLASSES_ENUM_NEW		eLBLessonToWrite
BOOL									bShowingOfflineLBButton

//VECTOR							vFinalCoord

BOOL 							bIsEndCutActive 						= FALSE
BOOL							bTransitionUp
BOOL							bTransitionOut
MG_FAIL_FADE_EFFECT				PS_UI_FailFadeEffect
MG_FAIL_SPLASH					PS_UI_FailSplash
SCRIPT_SCALEFORM_BIG_MESSAGE	PS_UI_BigMessage
STRING							sPSFailStrapline

// HUD
//********************
PS_HUD_OBJECTIVE_BAR_STRUCT		PS_UI_ObjectiveMeter
PS_HUD_GUTTER_ICON_STRUCT		PS_UI_GutterIcon
PS_HUD_RACE_HUD_STRUCT			PS_UI_RaceHud

structPedsForConversation 		PedDispatcher
COUNTDOWN_UI					PS_CountDownUI 

//Altimeter
FLOAT 							fAltitude								= 0
FLOAT							fDistanceLeft							= 0 
FLOAT 							fAltitudeMax							= 100
//Altitude Indicator
FLOAT 							fRoll									= 0
//screen fade
PS_SCREEN_FADE_STATE			eFadeState								= PS_SCREEN_FADE_IDLE
FLOAT							fScreenFadeWaitTime						= PS_SCREEN_FADE_DEFAULT_WAIT
structTimer						tPS_FadeTimer
INT								iScreenFadeTime							= PS_SCREEN_FADE_DEFAULT_TIME
BOOL							bScreenFadeVisible 						= TRUE


//hint cam stuff
PS_HINT_CAM_STRUCT				PS_HINT_CAM
CAMERA_INDEX 					cPS_HintCam
CONST_FLOAT						PS_HINT_CAM_TIMER_THRESHOLD			1.0			// Threshold for going back from hint cam, like Alwyn's stuff does


BOOL							bIsAltimeterVisible 					= FALSE
BOOL							bIsAltitudeIndicatorVisible 			= FALSE
BOOL 							bIsAltimeterActive 						= FALSE
BOOL							bIsAltitudeIndicatorActive				= FALSE
BOOL							bQuickTapActivates


INT 							PS_SoundID_Alt_Meter_Alarm 				= -1
INT 							PS_SoundID_Formation_Alarm				= -1


INT								iInstructionCounter						= 1
INT								iPSObjectiveTextCounter					= 1
INT								iPSHelpTextCounter						= 1
INT								iPSDialogueCounter						= 1

//************************************************************
// Lesson specific stuff
//************************************************************

//Landing

//VECTOR							vDialogueMarker1
//VECTOR							vDialogueMarker2
//VECTOR							vLandingSpot

//Inverted
//structTimer tInvertedLevelOutTimer

//Knifing
//PS_OBJECTIVE_TYPE_ENUM			eCurrentKnifingObjective

//Looping
//keep track of tutorial dialogue
//INT								iTutorialCounter						= 1
//structTimer						tLevelOutTimer
//structTimer						tRetryInstructionTimer

//Moving Chute
NETWORK_INDEX 					piGhostPilot_NETID
NETWORK_INDEX					PS_FlatBedTruck_NETID
NETWORK_INDEX					PS_FlatBedTrailer_NETID
NETWORK_INDEX					PS_ambVeh_NETID[6]
NETWORK_INDEX					PS_ambPed_NETID[6]
NETWORK_INDEX 					targetDriver_NETID
VEHICLE_INDEX					PS_FlatBedTruck
VEHICLE_INDEX					PS_FlatBedTrailer
VEHICLE_INDEX 					PS_ambVeh[5]
PTFX_ID		 					PTFX_SkydivingTargetFlare
PS_TRUCK_LANDING_STATE 			ePS_TruckLandingState 					= PS_TRUCK_LANDING_STATE_01
PS_TRUCK_CRASHING_STATE 		ePS_TruckCrashingState 					= PS_TRUCK_CRASHING_STATE_01
BOOL 							bFinishedTruckLandingScene				= FALSE
BOOL 							bFinishedTruckCrashingScene				= FALSE
//BOOL							bRagdollingOnTruck						= FALSE
VECTOR							vPS_MovingTarget							= VECTOR_ZERO


//daring landing
FLOAT 							fCurPlaneSpeed							= -1
//BOOL 							bHasPlaneSpedUp 						= FALSE

//skydiving
structTimer						tmrLandingAlpha
structTimer						tmrParachuteJump
//VECTOR							vAlphaPosition
//PED_VARIATION_STRUCT 			structPlayerProps

//MUSIC
PS_MUSIC_ENUM currentLessonMusic = PS_MUSIC_NONE

//shooting range
enum enumTargetState
	DOESNT_EXIST,
	WAITING,
	SPAWNING,
	MOVING,
	IN_POSITION,
	EXPLODING,
	EXPLODE_AND_RESET,
	DESTROYED,
	RESET
endenum

struct sTargets
	enumTargetState status
	vector vCoord
	float fShtHeading

	vector vSlideIn
	vector vCurrent
	OBJECT_INDEX entity, entityBase
	NETWORK_INDEX entity_NETID, entityBase_NETID
	float triggerTime
	int slideFlag
	int wave
	bool loop
	int pauseTime
	int iMovementType //0: move to point, 1: fix, rotate about right edge, 2: rotate about bottom edge, 3: rotate about right then move, 4: rotate about top edge & move, 5: rotate about bottom edge & move
	BLIP_INDEX blip
	int sound = -1
endstruct

sTargets targetList[18]

PROC DataTempHack()
	
	#IF IS_DEBUG_BUILD
		PSER_Widget = PSER_Widget
	#ENDIF
	PS_ambVeh[0]=PS_ambVeh[0]
	UNUSED_PARAMETER(vPreviewCamDirection)
	PS_PARACHUTE_JUMP_CAM_OFFSET_1 = PS_PARACHUTE_JUMP_CAM_OFFSET_1
	PS_PARACHUTE_JUMP_CAM_OFFSET_2 = PS_PARACHUTE_JUMP_CAM_OFFSET_2
	PS_PARACHUTE_JUMP_CAM_TARG_OFFSET_1 = PS_PARACHUTE_JUMP_CAM_TARG_OFFSET_1
	PS_PARACHUTE_JUMP_CAM_TARG_OFFSET_2 = PS_PARACHUTE_JUMP_CAM_TARG_OFFSET_2
	bRetryChallenge = bRetryChallenge
	PS_CountDownUI = PS_CountDownUI
	cPS_Parachute_Jump_Cam = cPS_Parachute_Jump_Cam
	iCurrentChallengeScore = iCurrentChallengeScore
	PS_Main = PS_Main
	PS_CutsceneSkipState = PS_CutsceneSkipState
	tPS_PreviewTimer = tPS_PreviewTimer
	PS_PreviewState = PS_PreviewState
	bPreviewCleanupOkay = bPreviewCleanupOkay
	bSkippedPreview = bSkippedPreview
	iPreviewCheckpointIdx = iPreviewCheckpointIdx
	fPreviewVehicleSkipTime =fPreviewVehicleSkipTime
	iPS_PreviewVehicleRecordingID = iPS_PreviewVehicleRecordingID
	ppsPlayerParachuteState = ppsPlayerParachuteState
	iPSObjectiveTextCounter = iPSObjectiveTextCounter
	iPSHelpTextCounter = iPSHelpTextCounter
	iPSDialogueCounter = iPSDialogueCounter
	PTFX_SkydivingTargetFlare = PTFX_SkydivingTargetFlare
	bFinishedTruckLandingScene = bFinishedTruckLandingScene
	bFinishedTruckCrashingScene = bFinishedTruckCrashingScene
	iNumMissedCheckpoints = iNumMissedCheckpoints
	fCurPlaneSpeed = fCurPlaneSpeed
ENDPROC

PROC REQUEST_PILOT_SCHOOL_ASSETS()
	REQUEST_ADDITIONAL_TEXT("PSCHOOL", MINIGAME_TEXT_SLOT)
	REQUEST_ADDITIONAL_TEXT_FOR_DLC("DSCHOOL", DLC_TEXT_SLOT1)
	REQUEST_STREAMED_TEXTURE_DICT("pilotSchool")
	REQUEST_STREAMED_TEXTURE_DICT("PS_Menu")
	REQUEST_STREAMED_TEXTURE_DICT("PS_MenuDLC")
	REQUEST_STREAMED_TEXTURE_DICT("MPMedals_FEED")
	REQUEST_MINIGAME_COUNTDOWN_UI(PS_CountDownUI)
	quitUI = REQUEST_QUIT_UI()
	PS_UI_Leaderboard = REQUEST_SC_LEADERBOARD_UI()
	
ENDPROC
		
FUNC BOOL HAS_LOADED_PILOT_SCHOOL_ASSETS()

	IF HAS_THIS_ADDITIONAL_TEXT_LOADED("PSCHOOL", MINIGAME_TEXT_SLOT)
		CPRINTLN(debug_trevor3,"LOAD PSCHOOL")
	ENDIF
	
	IF HAS_THIS_ADDITIONAL_TEXT_LOADED("DSCHOOL", DLC_TEXT_SLOT1)
		CPRINTLN(debug_trevor3,"LOAD PSCHOOL_DLC")
	ENDIF

	IF HAS_STREAMED_TEXTURE_DICT_LOADED("pilotSchool")
		AND HAS_THIS_ADDITIONAL_TEXT_LOADED("PSCHOOL", MINIGAME_TEXT_SLOT)
		AND HAS_THIS_ADDITIONAL_TEXT_LOADED("DSCHOOL", DLC_TEXT_SLOT1)
		AND HAS_STREAMED_TEXTURE_DICT_LOADED("PS_Menu")
		AND HAS_STREAMED_TEXTURE_DICT_LOADED("PS_MenuDLC")
		AND HAS_STREAMED_TEXTURE_DICT_LOADED("MPMedals_FEED")
		AND HAS_SCALEFORM_MOVIE_LOADED(quitUI)
		AND HAS_SCALEFORM_MOVIE_LOADED(PS_UI_Leaderboard)
		AND HAS_MINIGAME_COUNTDOWN_UI_LOADED(PS_CountDownUI)
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC 

PROC RELEASE_PILOT_SCHOOL_ASSETS()
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("pilotSchool")
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("PS_Menu")
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("PS_MenuDLC")
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPMedals_FEED")
	RELEASE_MINIGAME_COUNTDOWN_UI(PS_CountDownUI)
	SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(quitUI)
	PS_CLEANUP_LEADERBOARD(PS_UI_Leaderboard)
ENDPROC

PROC RELEASE_NETWORK_ID(network_index thisNetID)
	//IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		IF NETWORK_DOES_NETWORK_ID_EXIST(thisNetID)
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(thisNetID)
				SET_NETWORK_ID_CAN_MIGRATE(thisNetID,TRUE)
			ENDIF
		ENDIF
	//ENDIF
ENDPROC

PROC RELEASE_PILOT_SCHOOL_NET_IDS()
	int i
	RELEASE_NETWORK_ID(piGhostPilot_NETID)
	RELEASE_NETWORK_ID(PS_FlatBedTruck_NETID)
	RELEASE_NETWORK_ID(PS_FlatBedTrailer_NETID)
	REPEAT COUNT_OF(PS_ambVeh_NETID) i
		RELEASE_NETWORK_ID(PS_ambVeh_NETID[i])
		RELEASE_NETWORK_ID(PS_ambPed_NETID[i])
	ENDREPEAT
	RELEASE_NETWORK_ID(targetDriver_NETID)
	REPEAT COUNT_OF(targetList) i
		RELEASE_NETWORK_ID(targetList[i].entity_NETID)
		RELEASE_NETWORK_ID(targetList[i].entityBase_NETID)
	ENDREPEAT
	RELEASE_NETWORK_ID(PS_Main.myVehicle_netID)
	RELEASE_NETWORK_ID(ParachutePackIndex_NETID)
ENDPROC

FUNC INT GET_HOUR_FOR_PS_CHALLENGE()
	SWITCH (g_current_selected_dlc_PilotSchool_class)		
		CASE PSCD_DLC_OutsideLoop		RETURN  12
		CASE PSCD_DLC_Engine_failure 	RETURN  11
		CASE PSCD_DLC_ShootingRange 	RETURN  05
		CASE PSCD_DLC_CityLanding 		RETURN  2
		CASE PSCD_DLC_ChaseParachute 	RETURN  16
		CASE PSCD_DLC_VehicleLanding 	RETURN  18
		CASE PSCD_DLC_Formation 		RETURN  06
		CASE PSCD_DLC_FlyLow 			RETURN  11
		CASE PSCD_DLC_CollectFlags 		RETURN  15
		CASE PSCD_DLC_FollowLeader 		RETURN  14
	ENDSWITCH
	
	RETURN 00
ENDFUNC

FUNC INT GET_WEATHER_FOR_PS_CHALLENGE()
	SWITCH (g_current_selected_dlc_PilotSchool_class)
		CASE PSCD_DLC_CityLanding RETURN ciFMMC_WEATHER_OPTION_RAINING 
	ENDSWITCH
	
	RETURN ciFMMC_WEATHER_OPTION_SUNNY
ENDFUNC

//Set the weather for the Pilot School
//1 for sunny, 2 for raining
PROC PS_SET_WEATHER_FOR_CHALLENGE(INT iWeatherOverride = -1)
	INT iWeather
	iWeather = GET_WEATHER_FOR_PS_CHALLENGE()
	
	if iWeatherOverride = -1
		SET_WEATHER_FOR_FMMC_MISSION(iWeather)
		cprintln(debug_trevor3, "SET_WEATHER_FOR_FMMC_MISSION =  ", iWeather, " g_current_selected_dlc_PilotSchool_class = ", ENUM_TO_INT(g_current_selected_dlc_PilotSchool_class))
	ELSE
		SET_WEATHER_FOR_FMMC_MISSION(iWeatherOverride)
		cprintln(debug_trevor3, "SET_WEATHER_FOR_FMMC_MISSION =  ", iWeatherOverride, " g_current_selected_dlc_PilotSchool_class = ", ENUM_TO_INT(g_current_selected_dlc_PilotSchool_class))
	ENDIF
ENDPROC

//INFO: 
//PARAM NOTES: The index (BitToCheckIndex) passed must be between 0 and 31 or the command will ASSERT. 
//PURPOSE: Checks if the integer (IntToCheck) passed has the bit set at the index (BitToCheckIndex) passed.More info..
FUNC BOOL Pilot_School_Data_Is_Goal_Bit_Set(PS_CHALLENGE_DATA_STRUCT thisData, PILOT_SCHOOL_GOAL_BITS BitIndex)
	RETURN IS_BIT_SET(thisData.GoalBits, ENUM_TO_INT(BitIndex))
ENDFUNC

//INFO: 
//PARAM NOTES:The index (BitToSetIndex) passed must be between 0 and 31 or the command will ASSERT.
//PURPOSE: Sets the bit at the index (BitToSetIndex) of the integer (IntToModify) passed. More info..
PROC Pilot_School_Data_Set_Goal_Bit(PS_CHALLENGE_DATA_STRUCT &thisData, PILOT_SCHOOL_GOAL_BITS BitIndex)
	SET_BIT(thisData.GoalBits, ENUM_TO_INT(BitIndex))
ENDPROC

//INFO: 
//PARAM NOTES:The index (BitToClearIndex) passed must be between 0 and 31 or the command will ASSERT. 
//PURPOSE: Clear the bit at the index (BitToClearIndex) of the integer (IntToModify) passed. More info..
PROC Pilot_School_Data_Clear_Goal_Bit(PS_CHALLENGE_DATA_STRUCT &thisData, PILOT_SCHOOL_GOAL_BITS BitIndex)
	CLEAR_BIT(thisData.GoalBits, ENUM_TO_INT(BitIndex))
ENDPROC

//INFO: 
//PARAM NOTES: The index (BitToCheckIndex) passed must be between 0 and 31 or the command will ASSERT. 
//PURPOSE: Checks if the integer (IntToCheck) passed has the bit set at the index (BitToCheckIndex) passed.More info..
FUNC BOOL Pilot_School_Data_Is_PreReq_Bit_Set(PS_CHALLENGE_DATA_STRUCT thisData, PILOT_SCHOOL_DLC_CLASSES_ENUM_NEW BitIndex)
	RETURN IS_BIT_SET(thisData.PreReqBits, ENUM_TO_INT(BitIndex))
ENDFUNC

//INFO: 
//PARAM NOTES:The index (BitToSetIndex) passed must be between 0 and 31 or the command will ASSERT.
//PURPOSE: Sets the bit at the index (BitToSetIndex) of the integer (IntToModify) passed. More info..
PROC Pilot_School_Data_Set_PreReq_Bit(PS_CHALLENGE_DATA_STRUCT &thisData, PILOT_SCHOOL_DLC_CLASSES_ENUM_NEW BitIndex)
	SET_BIT(thisData.PreReqBits, ENUM_TO_INT(BitIndex))
ENDPROC
//INFO: 

//PARAM NOTES:The index (BitToClearIndex) passed must be between 0 and 31 or the command will ASSERT. 
//PURPOSE: Clear the bit at the index (BitToClearIndex) of the integer (IntToModify) passed. More info..
PROC Pilot_School_Data_Clear_PreReq_Bit(PS_CHALLENGE_DATA_STRUCT &thisData, PILOT_SCHOOL_DLC_CLASSES_ENUM_NEW BitIndex)
	CLEAR_BIT(thisData.PreReqBits, ENUM_TO_INT(BitIndex))
ENDPROC

FUNC INT GetPilotSchoolScore_0_timeTaken(PS_CHALLENGE_DATA_STRUCT thisChallengeData, PS_PLAYER_DATA_STRUCT thisPlayerData)
	INT iTempPlayerTime = FLOOR(thisPlayerData.ElapsedTime*1000)//convert to ms and cut off the end
	IF iTempPlayerTime <= 0
		RETURN 0
	ELIF iTempPlayerTime <= FLOOR(thisChallengeData.GoldTime*1000)
		//GOLD
		RETURN iSCORE_FOR_GOLD+10
	ELIF iTempPlayerTime <= FLOOR(thisChallengeData.SilverTime*1000)
		//SILVER
		RETURN iSCORE_FOR_SILVER
	ELIF thisChallengeData.BronzeTime = -1 OR iTempPlayerTime <= FLOOR(thisChallengeData.BronzeTime*1000)
		//BRONZE
		RETURN iSCORE_FOR_BRONZE
	ELSE
		RETURN -1
	ENDIF
ENDFUNC

FUNC INT GetPilotSchoolScore_2_distanceFromTarget(PS_CHALLENGE_DATA_STRUCT thisChallengeData, PS_PLAYER_DATA_STRUCT thisPlayerData)
	IF thisPlayerData.LandingDistance < 0
		RETURN -1
	ENDIF
	IF thisPlayerData.LandingDistance <= thisChallengeData.GoldDistance
		//gold
		RETURN iSCORE_FOR_GOLD+10
	ELIF thisPlayerData.LandingDistance > thisChallengeData.GoldDistance AND thisPlayerData.LandingDistance <= thisChallengeData.SilverDistance
		//Silver
		RETURN iSCORE_FOR_SILVER
	ELIF thisChallengeData.BronzeDistance = -1 OR (thisPlayerData.LandingDistance > thisChallengeData.SilverDistance AND thisPlayerData.LandingDistance <= thisChallengeData.BronzeDistance)
		//bronze
		RETURN iSCORE_FOR_BRONZE
	ELSE
		//grey you suck
		RETURN -1
	ENDIF	
ENDFUNC

FUNC INT GetPilotSchoolScore_3_checkpointsPassed(PS_CHALLENGE_DATA_STRUCT thisChallengeData, PS_PLAYER_DATA_STRUCT thisPlayerData)
	RETURN (thisPlayerData.CheckpointCount*100)/thisChallengeData.TotalCheckpoints
ENDFUNC

FUNC INT GetPilotSchoolScore_4_formationCompletion(PS_CHALLENGE_DATA_STRUCT thisChallengeData, PS_PLAYER_DATA_STRUCT thisPlayerData)
	RETURN 100* ROUND((thisChallengeData.FormationTotal - thisPlayerData.FormationTimer)/thisChallengeData.FormationTotal)
ENDFUNC

FUNC INT GetPilotSchoolScore_5_avgCheckpointHeight(PS_CHALLENGE_DATA_STRUCT thisChallengeData, PS_PLAYER_DATA_STRUCT thisPlayerData)

	IF thisPlayerData.LandingDistance < 0
		RETURN -1
	ENDIF
	IF thisPlayerData.LandingDistance <= thisChallengeData.GoldDistance
		//gold
		RETURN iSCORE_FOR_GOLD+10
	ELIF thisPlayerData.LandingDistance > thisChallengeData.GoldDistance AND thisPlayerData.LandingDistance <= thisChallengeData.SilverDistance
		//Silver
		RETURN iSCORE_FOR_SILVER
	ELIF thisChallengeData.BronzeDistance = -1 OR (thisPlayerData.LandingDistance > thisChallengeData.SilverDistance AND thisPlayerData.LandingDistance <= thisChallengeData.BronzeDistance)
		//bronze
		RETURN iSCORE_FOR_BRONZE
	ELSE
		//grey you suck
		RETURN -1
	ENDIF

ENDFUNC

FUNC INT GetPilotSchoolScore_6_targetsDestroyed(PS_CHALLENGE_DATA_STRUCT thisChallengeData, PS_PLAYER_DATA_STRUCT thisPlayerData)
	cprintln(debug_trevor3,"GetPilotSchoolScore_6_targetsDestroyed(), LandingDistance = ",thisPlayerData.LandingDistance)
	cprintln(debug_trevor3,"GetPilotSchoolScore_6_targetsDestroyed(), bronze dist = ",thisChallengeData.BronzeDistance)
	IF thisPlayerData.LandingDistance < 0
		RETURN -1
	ENDIF
	IF thisPlayerData.LandingDistance >= thisChallengeData.GoldDistance
		//gold
		RETURN iSCORE_FOR_GOLD+10
	ELIF thisPlayerData.LandingDistance < thisChallengeData.GoldDistance AND thisPlayerData.LandingDistance >= thisChallengeData.SilverDistance
		//Silver
		RETURN iSCORE_FOR_SILVER
	ELIF thisChallengeData.BronzeDistance = -1 OR (thisPlayerData.LandingDistance < thisChallengeData.SilverDistance AND thisPlayerData.LandingDistance >= thisChallengeData.BronzeDistance)
		//bronze
		RETURN iSCORE_FOR_BRONZE
	ELSE
		//grey you suck
		RETURN -1
	ENDIF	
ENDFUNC

PROC Pilot_School_Data_Check_Failed_Goals(PS_CHALLENGE_DATA_STRUCT thisChallengeData, PS_PLAYER_DATA_STRUCT thisPlayerData)
	IF ePSFailReason = PS_FAIL_NO_REASON
		INT tempscore
		
		IF Pilot_School_Data_Is_Goal_Bit_Set(thisChallengeData, FSG_6_targetsDestroyed)
			tempscore = GetPilotSchoolScore_6_targetsDestroyed(thisChallengeData, thisPlayerData)
			cprintln(debug_trevor3,"Pilot_School_Data_Check_Failed_Goals() tempscore = ",tempscore)
			IF tempscore < 70
				DEBUG_MESSAGE("FAILED BC PLAYER HAD TOO LOW SCORE")
				ePSFailReason = PS_FAIL_LOW_SCORE
			ENDIF	
		ENDIF
		
		IF Pilot_School_Data_Is_Goal_Bit_Set(thisChallengeData, FSG_5_avgCheckpointHeight)
			tempscore = GetPilotSchoolScore_5_avgCheckpointHeight(thisChallengeData, thisPlayerData)
			IF tempscore < 70
				DEBUG_MESSAGE("FAILED BC PLAYER AVERAGE HEIGHT IS TOO HIGH")
				ePSFailReason = PS_FAIL_TOO_HIGH
			ENDIF	
		ENDIF
		
		IF Pilot_School_Data_Is_Goal_Bit_Set(thisChallengeData, FSG_4_formationCompletion)
			tempscore = GetPilotSchoolScore_4_formationCompletion(thisChallengeData, thisPlayerData)
			IF tempscore < 70
				DEBUG_MESSAGE("FAILED BC PLAYER WAS OUT OF FORMATION!")
				ePSFailReason = PS_FAIL_FORMATION
			ENDIF	
		ENDIF
		
		IF Pilot_School_Data_Is_Goal_Bit_Set(thisChallengeData, FSG_3_checkpointsPassed)
			tempscore = GetPilotSchoolScore_3_checkpointsPassed(thisChallengeData, thisPlayerData)
			IF tempscore < 70	
				DEBUG_MESSAGE("FAILED BC PLAYER MISSED TOO MANY GATES")
				ePSFailReason = PS_FAIL_MISSED_GATES
			ENDIF
		ENDIF

		IF Pilot_School_Data_Is_Goal_Bit_Set(thisChallengeData, FSG_2_distanceFromTarget)
			tempscore = GetPilotSchoolScore_2_distanceFromTarget(thisChallengeData, thisPlayerData)
			IF tempscore < 70
				DEBUG_MESSAGE("FAILED BC PLAYER IS TOO FAR FROM FINISH")
				ePSFailReason = PS_FAIL_TOO_FAR
			ENDIF	
		ENDIF
		
		IF Pilot_School_Data_Is_Goal_Bit_Set(thisChallengeData, FSG_0_timeTaken)
			tempscore = GetPilotSchoolScore_0_timeTaken(thisChallengeData, thisPlayerData)
			IF tempscore < 70
				DEBUG_MESSAGE("FAILED BC PLAYER RAN OUT OF TIME!")
				ePSFailReason = PS_FAIL_TIME_UP
			ENDIF	
		ENDIF
		
	ENDIF
ENDPROC

FUNC INT Pilot_School_Data_Get_Score(PS_CHALLENGE_DATA_STRUCT thisChallengeData, PS_PLAYER_DATA_STRUCT thisPlayerData, BOOL bForMenuUse = FALSE)
	PRINTLN("*************************** Pilot_school_mp_data.sch Pilot_School_Data_Get_Score() **************************************")
	IF bForMenuUse
		IF thisChallengeData.LockStatus = PSS_LOCKED
			PRINTLN("ABORT SCORE DUE TO LOCKED CHALLENGE")
			PRINTLN("---------------------- END Pilot_School_Data_Get_Score() -------------------------------")
			RETURN -1
		ENDIF
	ENDIF
	
	INT iScores[5]
	INT iGoal = 0

	//gather scores into the array, and keep track of how many goals we're comparing
	//time
	IF Pilot_School_Data_Is_Goal_Bit_Set(thisChallengeData, FSG_0_timeTaken)
		IF thisPlayerData.ElapsedTime < 0
			PRINTLN("ABORT SCORE A : thisPlayerData.ElapsedTime < 0")
			PRINTLN("---------------------- END Pilot_School_Data_Get_Score() -------------------------------")
			RETURN -1
		ENDIF
		iScores[iGoal] = GetPilotSchoolScore_0_timeTaken(thisChallengeData, thisPlayerData)
		IF iScores[iGoal] < iSCORE_FOR_BRONZE
			//early return if we didnt pass time check
			PRINTLN("ABORT SCORE B : iScores[iGoal] < iSCORE_FOR_BRONZE")
			PRINTLN("---------------------- END Pilot_School_Data_Get_Score() -------------------------------")
			RETURN -1
		ENDIF
		iGoal++
	ENDIF

	//landing dist
	IF Pilot_School_Data_Is_Goal_Bit_Set(thisChallengeData, FSG_2_distanceFromTarget)
		iScores[iGoal] = GetPilotSchoolScore_2_distanceFromTarget(thisChallengeData, thisPlayerData)
		IF iScores[iGoal] < iSCORE_FOR_BRONZE
			//early return if we didnt pass distance check
			PRINTLN("ABORT SCORE C : iScores[iGoal] < iSCORE_FOR_BRONZE")
			PRINTLN("---------------------- END Pilot_School_Data_Get_Score() -------------------------------")
			RETURN -1
		ENDIF
		iGoal++
	ENDIF
	
	//checkpoints
	/* //removed as no lessons need to do this check.
	IF Pilot_School_Data_Is_Goal_Bit_Set(thisChallengeData, FSG_3_checkpointsPassed)
		iScores[iGoal] = GetPilotSchoolScore_3_checkpointsPassed(thisChallengeData, thisPlayerData)
		IF iScores[iGoal] < iSCORE_FOR_BRONZE
			PRINTLN("ABORT SCORE D : iScores[iGoal] < iSCORE_FOR_BRONZE")
			PRINTLN("---------------------- END Pilot_School_Data_Get_Score() -------------------------------")
			//early return if we didnt pass checkpoint count check
			RETURN -1
		ENDIF
		iGoal++
	ENDIF
	*/
	
	//formation
	IF Pilot_School_Data_Is_Goal_Bit_Set(thisChallengeData, FSG_4_formationCompletion)
		iScores[iGoal] = GetPilotSchoolScore_4_formationCompletion(thisChallengeData, thisPlayerData)
		IF iScores[iGoal] < iSCORE_FOR_BRONZE
			//early return if we didnt pass formation completion check
			PRINTLN("ABORT SCORE E : iScores[iGoal] < iSCORE_FOR_BRONZE")
			PRINTLN("---------------------- END Pilot_School_Data_Get_Score() -------------------------------")
			RETURN -1
		ENDIF
		iGoal++
	ENDIF
	
	//dlc average height
	IF Pilot_School_Data_Is_Goal_Bit_Set(thisChallengeData, FSG_5_avgCheckpointHeight)
		iScores[iGoal] = GetPilotSchoolScore_5_avgCheckpointHeight(thisChallengeData, thisPlayerData)
		IF iScores[iGoal] < iSCORE_FOR_BRONZE
			//early return if we didnt pass avg height check
			PRINTLN("ABORT SCORE F : iScores[iGoal] < iSCORE_FOR_BRONZE")
			PRINTLN("---------------------- END Pilot_School_Data_Get_Score() -------------------------------")
			RETURN -1
		ENDIF
		iGoal++
	ENDIF
	
	//dlc targets destroyed
	IF Pilot_School_Data_Is_Goal_Bit_Set(thisChallengeData, FSG_6_targetsDestroyed)
		iScores[iGoal] = GetPilotSchoolScore_6_targetsDestroyed(thisChallengeData, thisPlayerData)
		IF iScores[iGoal] < iSCORE_FOR_BRONZE
			//early return if we didnt pass avg height check
			PRINTLN("ABORT SCORE G : iScores[iGoal] < iSCORE_FOR_BRONZE")
			PRINTLN("---------------------- END Pilot_School_Data_Get_Score() -------------------------------")
			RETURN -1
		ENDIF
		iGoal++
	ENDIF
	
	//use a bool to make sure we actually have a score to return
	BOOL bReturnLowest = FALSE
	//Set a medal ceiling so your score is calculated by the lowest goal reached
	INT iLowestMedalScore = 101
	INT i = 0
	IF iGoal > 1
		REPEAT iGoal i
			IF iScores[i] >= 0
				IF iScores[i] < iLowestMedalScore
					IF iScores[i] < iSCORE_FOR_BRONZE
						PRINTLN("SCore < bronze")
						iScores[i] = -1
					ELIF iScores[i] < iSCORE_FOR_SILVER
						PRINTLN("Score = bronze")
						iLowestMedalScore = iScores[i] 
					ELIF iScores[i] < iSCORE_FOR_GOLD
						PRINTLN("Score = silver")
						iLowestMedalScore = iScores[i] 
					ELSE
						PRINTLN("Score = gold")
						iLowestMedalScore = iScores[i] 
					ENDIF
					IF iLowestMedalScore = iScores[i]
						//if we actually used one of the scores, make sure we return it
						bReturnLowest = TRUE
					ENDIF
				ELSE
				ENDIF
			ELSE
			ENDIF
		ENDREPEAT
	ELSE
		//we only have one score...
		PRINTLN("NOT SURE WHAT HAPPENS HERE")
		iGoal = 0
		iLowestMedalScore = iScores[iGoal]
		bReturnLowest = TRUE
	ENDIF

	//make sure we actually got a valid score, and make sure its above a bronze.
	IF bReturnLowest AND iLowestMedalScore > 0	AND iLowestMedalScore < 101	
		PRINTLN("Return score = ",iLowestMedalScore)
		PRINTLN("---------------------- END Pilot_School_Data_Get_Score() -------------------------------")
		RETURN iLowestMedalScore
	ELSE
		PRINTLN("Return -1")
		PRINTLN("---------------------- END Pilot_School_Data_Get_Score() -------------------------------")
		RETURN -1
	ENDIF
ENDFUNC

//see if we have a fail reason that occurs AFTER the lesson (for cases where we want to know if the player at least completed the course)
FUNC BOOL Pilot_School_Data_Has_Ignorable_Fail_Reason()
//	SWITCH(ePSFailReason)
//		CASE PS_FAIL_TIME_UP
//			RETURN TRUE
//			BREAK
//	ENDSWITCH
	RETURN FALSE
ENDFUNC


FUNC BOOL Pilot_School_Data_Has_Fail_Reason()
	IF ePSFailReason = PS_FAIL_NO_REASON
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

FUNC BOOL Pilot_School_Data_Check_Mission_Success(PS_CHALLENGE_DATA_STRUCT thisChallengeData, PS_PLAYER_DATA_STRUCT thisPlayerData)
	Pilot_School_Data_Check_Failed_Goals(thisChallengeData, thisPlayerData)
	IF ePSFailReason = PS_FAIL_NO_REASON //if we haven't gotten a reason to fail yet
		IF Pilot_School_Data_Get_Score(thisChallengeData, thisPlayerData) >= iSCORE_FOR_BRONZE
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

#IF IS_DEBUG_BUILD
	PROC SAVE_PILOT_SCHOOL_STRUCT_TO_NAMED_DEBUG_FILE(PS_CHALLENGE_DATA_STRUCT thisChallengeData, PS_PLAYER_DATA_STRUCT thisPlayerData, string sFilePath, string sFileName)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("		<RecordNode name = \"", sFilePath, sFileName)
		SAVE_STRING_TO_NAMED_DEBUG_FILE(thisChallengeData.Title, sFilePath, sFileName)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("\" status = \"", sFilePath, sFileName)
		SAVE_INT_TO_NAMED_DEBUG_FILE(ENUM_TO_INT(thisChallengeData.LockStatus), sFilePath, sFileName)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("\" goals = \"", sFilePath, sFileName)
		SAVE_INT_TO_NAMED_DEBUG_FILE(thisChallengeData.GoalBits, sFilePath, sFileName)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("\" preReq = \"", sFilePath, sFileName)
		SAVE_INT_TO_NAMED_DEBUG_FILE(thisChallengeData.PreReqBits, sFilePath, sFileName)
		
		IF Pilot_School_Data_Is_Goal_Bit_Set(thisChallengeData, FSG_0_timeTaken)
			SAVE_STRING_TO_NAMED_DEBUG_FILE("\" elapsedTime = \"", sFilePath, sFileName)
			SAVE_FLOAT_TO_NAMED_DEBUG_FILE(thisPlayerData.ElapsedTime, sFilePath, sFileName)
			SAVE_STRING_TO_NAMED_DEBUG_FILE("\" timeGold = \"", sFilePath, sFileName)
			SAVE_FLOAT_TO_NAMED_DEBUG_FILE(thisChallengeData.GoldTime, sFilePath, sFileName)
			SAVE_STRING_TO_NAMED_DEBUG_FILE("\" timeBronze = \"", sFilePath, sFileName)
			SAVE_FLOAT_TO_NAMED_DEBUG_FILE(thisChallengeData.BronzeTime, sFilePath, sFileName)
		ENDIF
		
		IF Pilot_School_Data_Is_Goal_Bit_Set(thisChallengeData, FSG_2_distanceFromTarget)
			SAVE_STRING_TO_NAMED_DEBUG_FILE("\" landingDistance = \"", sFilePath, sFileName)
			SAVE_FLOAT_TO_NAMED_DEBUG_FILE(thisPlayerData.LandingDistance, sFilePath, sFileName)
			SAVE_STRING_TO_NAMED_DEBUG_FILE("\" distGold = \"", sFilePath, sFileName)
			SAVE_FLOAT_TO_NAMED_DEBUG_FILE(thisChallengeData.GoldDistance, sFilePath, sFileName)
			SAVE_STRING_TO_NAMED_DEBUG_FILE("\" distBronze = \"", sFilePath, sFileName)
			SAVE_FLOAT_TO_NAMED_DEBUG_FILE(thisChallengeData.BronzeDistance, sFilePath, sFileName)
		ENDIF
		
		IF Pilot_School_Data_Is_Goal_Bit_Set(thisChallengeData, FSG_3_checkpointsPassed)
			SAVE_STRING_TO_NAMED_DEBUG_FILE("\" checkpointCount = \"", sFilePath, sFileName)
			SAVE_INT_TO_NAMED_DEBUG_FILE(thisPlayerData.CheckpointCount, sFilePath, sFileName)
			SAVE_STRING_TO_NAMED_DEBUG_FILE("\" checkTotal = \"", sFilePath, sFileName)
			SAVE_INT_TO_NAMED_DEBUG_FILE(thisChallengeData.TotalCheckpoints, sFilePath, sFileName)
		ENDIF
		
		IF Pilot_School_Data_Is_Goal_Bit_Set(thisChallengeData, FSG_4_formationCompletion)
			SAVE_STRING_TO_NAMED_DEBUG_FILE("\" checkpointCount = \"", sFilePath, sFileName)
			SAVE_INT_TO_NAMED_DEBUG_FILE(thisPlayerData.FormationTimer, sFilePath, sFileName)
			SAVE_STRING_TO_NAMED_DEBUG_FILE("\" checkTotal = \"", sFilePath, sFileName)
			SAVE_INT_TO_NAMED_DEBUG_FILE(thisChallengeData.FormationTotal, sFilePath, sFileName)
		ENDIF
		
		IF Pilot_School_Data_Is_Goal_Bit_Set(thisChallengeData, FSG_5_avgCheckpointHeight)
			SAVE_STRING_TO_NAMED_DEBUG_FILE("\" landingDistance = \"", sFilePath, sFileName)
			SAVE_FLOAT_TO_NAMED_DEBUG_FILE(thisPlayerData.LandingDistance, sFilePath, sFileName)
			SAVE_STRING_TO_NAMED_DEBUG_FILE("\" distGold = \"", sFilePath, sFileName)
			SAVE_FLOAT_TO_NAMED_DEBUG_FILE(thisChallengeData.GoldDistance, sFilePath, sFileName)
			SAVE_STRING_TO_NAMED_DEBUG_FILE("\" distBronze = \"", sFilePath, sFileName)
			SAVE_FLOAT_TO_NAMED_DEBUG_FILE(thisChallengeData.BronzeDistance, sFilePath, sFileName)
		ENDIF

		SAVE_STRING_TO_NAMED_DEBUG_FILE("\"  />		<!--  ", sFilePath, sFileName)
		SAVE_STRING_TO_NAMED_DEBUG_FILE(GET_STRING_FROM_TEXT_FILE(thisChallengeData.Title), sFilePath, sFileName)
		SAVE_STRING_TO_NAMED_DEBUG_FILE(" -->", sFilePath, sFileName)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sFilePath, sFileName)
	ENDPROC
	
	PROC SAVE_PILOT_SCHOOL_STRUCT_TO_DEBUG_FILE(PS_CHALLENGE_DATA_STRUCT thisChallengeData, PS_PLAYER_DATA_STRUCT thisPlayerData)
		string sFilePath	= "X:/gta5/build/dev"
		string sFileName	= "temp_debug.txt"		
		SAVE_PILOT_SCHOOL_STRUCT_TO_NAMED_DEBUG_FILE(thisChallengeData, thisPlayerData, sFilePath, sFileName)
	ENDPROC
#ENDIF

PROC Pilot_School_Data_Reset_PS_Struct(PS_PLAYER_DATA_STRUCT thisPlayerData)
	thisPlayerData = thisPlayerData
	thisPlayerData.ElapsedTime = -1
	thisPlayerData.LandingDistance = -1
	thisPlayerData.CheckpointCount = -1
ENDPROC

PROC PS_Load_Pilot_School_Data()

	if IS_STRING_NULL_OR_EMPTY(PS_Challenges[PSCD_DLC_OutsideLoop].Title)
		PS_Challenges[PSCD_DLC_OutsideLoop].Title = "MGFS_12"
		PS_Challenges[PSCD_DLC_OutsideLoop].LessonEnum = PSCD_DLC_OutsideLoop
		PS_Challenges[PSCD_DLC_OutsideLoop].LockStatus = INT_TO_ENUM(PILOT_SCHOOL_STATUS_ENUM, 0)
		Pilot_School_Data_Set_Goal_Bit(PS_Challenges[PSCD_DLC_OutsideLoop],	(FSG_0_timeTaken))
	//	Pilot_School_Data_Set_PreReq_Bit(PS_Challenges[PSCD_DLC_OutsideLoop],	(PSCD_Inverted))
	//	Pilot_School_Data_Set_PreReq_Bit(PS_Challenges[PSCD_DLC_OutsideLoop],	(PSCD_Knifing))
		PS_Challenges[PSCD_DLC_OutsideLoop].eEndscreenType = ENDSCREEN_AUTOPILOT
		PS_Challenges[PSCD_DLC_OutsideLoop].eEndCamType = PS_END_CAMERA_FOLLOW_SHOT
		PS_Challenges[PSCD_DLC_OutsideLoop].GoldTime = 58.0000
		PS_Challenges[PSCD_DLC_OutsideLoop].SilverTime = 80.0000
		PS_Challenges[PSCD_DLC_OutsideLoop].BronzeTime = 120.0000
		
		PS_Challenges[PSCD_DLC_FollowLeader].Title = "MGFS_13"
		PS_Challenges[PSCD_DLC_FollowLeader].LessonEnum = PSCD_DLC_FollowLeader
		PS_Challenges[PSCD_DLC_FollowLeader].LockStatus = INT_TO_ENUM(PILOT_SCHOOL_STATUS_ENUM, 0)
		Pilot_School_Data_Set_Goal_Bit(PS_Challenges[PSCD_DLC_FollowLeader],	(FSG_0_timeTaken))
		Pilot_School_Data_Set_Goal_Bit(PS_Challenges[PSCD_DLC_FollowLeader],	(FSG_3_checkpointsPassed))
	//	Pilot_School_Data_Set_PreReq_Bit(PS_Challenges[PSCD_DLC_FollowLeader],	(PSCD_Inverted))
	//	Pilot_School_Data_Set_PreReq_Bit(PS_Challenges[PSCD_DLC_FollowLeader],	(PSCD_Knifing))
		PS_Challenges[PSCD_DLC_FollowLeader].eEndscreenType = ENDSCREEN_AUTOPILOT
		PS_Challenges[PSCD_DLC_FollowLeader].eEndCamType = PS_END_CAMERA_FOLLOW_SHOT
		PS_Challenges[PSCD_DLC_FollowLeader].GoldTime = 145.0000
		PS_Challenges[PSCD_DLC_FollowLeader].SilverTime = 160.0000
		PS_Challenges[PSCD_DLC_FollowLeader].BronzeTime = -1
		PS_Challenges[PSCD_DLC_FollowLeader].TotalCheckpoints = 19
		
		PS_Challenges[PSCD_DLC_VehicleLanding].Title = "MGFS_14"
		PS_Challenges[PSCD_DLC_VehicleLanding].LessonEnum = PSCD_DLC_VehicleLanding
		PS_Challenges[PSCD_DLC_VehicleLanding].LockStatus = INT_TO_ENUM(PILOT_SCHOOL_STATUS_ENUM, 0)
		Pilot_School_Data_Set_Goal_Bit(PS_Challenges[PSCD_DLC_VehicleLanding],	(FSG_0_timeTaken))
	//	Pilot_School_Data_Set_PreReq_Bit(PS_Challenges[PSCD_DLC_VehicleLanding],	(PSCD_Inverted))
	//	Pilot_School_Data_Set_PreReq_Bit(PS_Challenges[PSCD_DLC_VehicleLanding],	(PSCD_Knifing))
		PS_Challenges[PSCD_DLC_VehicleLanding].eEndscreenType = ENDSCREEN_AUTOPILOT
		PS_Challenges[PSCD_DLC_VehicleLanding].eEndCamType = PS_END_CAMERA_DYNAMIC_SHOT
		PS_Challenges[PSCD_DLC_VehicleLanding].GoldTime = 19.0000
		PS_Challenges[PSCD_DLC_VehicleLanding].SilverTime = 25.0000
		PS_Challenges[PSCD_DLC_VehicleLanding].BronzeTime = 60.0000
		
		PS_Challenges[PSCD_DLC_CollectFlags].Title = "MGFS_15"
		PS_Challenges[PSCD_DLC_CollectFlags].LessonEnum = PSCD_DLC_CollectFlags
		PS_Challenges[PSCD_DLC_CollectFlags].LockStatus = INT_TO_ENUM(PILOT_SCHOOL_STATUS_ENUM, 0)
		Pilot_School_Data_Set_Goal_Bit(PS_Challenges[PSCD_DLC_CollectFlags],	(FSG_0_timeTaken))
	//	Pilot_School_Data_Set_PreReq_Bit(PS_Challenges[PSCD_DLC_CollectFlags],	(PSCD_Inverted))
	//	Pilot_School_Data_Set_PreReq_Bit(PS_Challenges[PSCD_DLC_CollectFlags],	(PSCD_Knifing))
		PS_Challenges[PSCD_DLC_CollectFlags].eEndscreenType = ENDSCREEN_AUTOPILOT
		PS_Challenges[PSCD_DLC_CollectFlags].eEndCamType = PS_END_CAMERA_FOLLOW_SHOT
		PS_Challenges[PSCD_DLC_CollectFlags].GoldTime = 160.0000
		PS_Challenges[PSCD_DLC_CollectFlags].SilverTime = 180.0000
		PS_Challenges[PSCD_DLC_CollectFlags].BronzeTime = -1//320.0000 //just let the player get bronze medal for collection all flags by default
		PS_Challenges[PSCD_DLC_CollectFlags].TotalCheckpoints = 30
			
		PS_Challenges[PSCD_DLC_Engine_failure].Title = "MGFS_16"
		PS_Challenges[PSCD_DLC_Engine_failure].LessonEnum = PSCD_DLC_Engine_failure
		PS_Challenges[PSCD_DLC_Engine_failure].LockStatus = INT_TO_ENUM(PILOT_SCHOOL_STATUS_ENUM, 0)
		Pilot_School_Data_Set_Goal_Bit(PS_Challenges[PSCD_DLC_Engine_failure],	(FSG_2_distanceFromTarget))
	//	Pilot_School_Data_Set_PreReq_Bit(PS_Challenges[PSCD_DLC_Engine_failure],	(PSCD_Inverted))
	//	Pilot_School_Data_Set_PreReq_Bit(PS_Challenges[PSCD_DLC_Engine_failure],	(PSCD_Knifing))
		PS_Challenges[PSCD_DLC_Engine_failure].eEndscreenType = ENDSCREEN_AUTOPILOT
		PS_Challenges[PSCD_DLC_Engine_failure].eEndCamType = PS_END_CAMERA_DYNAMIC_SHOT
		PS_Challenges[PSCD_DLC_Engine_failure].GoldDistance = 10.0
		PS_Challenges[PSCD_DLC_Engine_failure].SilverDistance = 30.0
		PS_Challenges[PSCD_DLC_Engine_failure].BronzeDistance = -1
		PS_Challenges[PSCD_DLC_Engine_failure].TotalCheckpoints = 2
		
		PS_Challenges[PSCD_DLC_ChaseParachute].Title = "MGFS_17"
		PS_Challenges[PSCD_DLC_ChaseParachute].LessonEnum = PSCD_DLC_ChaseParachute
		PS_Challenges[PSCD_DLC_ChaseParachute].LockStatus = INT_TO_ENUM(PILOT_SCHOOL_STATUS_ENUM, 0)
		Pilot_School_Data_Set_Goal_Bit(PS_Challenges[PSCD_DLC_ChaseParachute],	(FSG_2_distanceFromTarget))
	//	Pilot_School_Data_Set_PreReq_Bit(PS_Challenges[PSCD_DLC_ChaseParachute],	(PSCD_Inverted))
	//	Pilot_School_Data_Set_PreReq_Bit(PS_Challenges[PSCD_DLC_ChaseParachute],	(PSCD_Knifing))
		PS_Challenges[PSCD_DLC_ChaseParachute].eEndscreenType = ENDSCREEN_AUTOPILOT
		PS_Challenges[PSCD_DLC_ChaseParachute].eEndCamType = PS_END_CAMERA_DYNAMIC_SHOT
		PS_Challenges[PSCD_DLC_ChaseParachute].GoldDistance = 2.0
		PS_Challenges[PSCD_DLC_ChaseParachute].SilverDistance = 4.5
		PS_Challenges[PSCD_DLC_ChaseParachute].BronzeDistance = 7.0
		
		PS_Challenges[PSCD_DLC_FlyLow].Title = "MGFS_18"
		PS_Challenges[PSCD_DLC_FlyLow].LessonEnum = PSCD_DLC_FlyLow
		PS_Challenges[PSCD_DLC_FlyLow].LockStatus = INT_TO_ENUM(PILOT_SCHOOL_STATUS_ENUM, 0)
		Pilot_School_Data_Set_Goal_Bit(PS_Challenges[PSCD_DLC_FlyLow],	(FSG_5_avgCheckpointHeight))
	//	Pilot_School_Data_Set_PreReq_Bit(PS_Challenges[PSCD_DLC_FlyLow],	(PSCD_Inverted))
	//	Pilot_School_Data_Set_PreReq_Bit(PS_Challenges[PSCD_DLC_FlyLow],	(PSCD_Knifing))
		PS_Challenges[PSCD_DLC_FlyLow].eEndscreenType = ENDSCREEN_AUTOPILOT
		PS_Challenges[PSCD_DLC_FlyLow].eEndCamType = PS_END_CAMERA_FOLLOW_SHOT
		PS_Challenges[PSCD_DLC_FlyLow].GoldDistance = 1.0
		PS_Challenges[PSCD_DLC_FlyLow].SilverDistance = 3.0
		PS_Challenges[PSCD_DLC_FlyLow].BronzeDistance = 10.0
		
		PS_Challenges[PSCD_DLC_ShootingRange].Title = "MGFS_19"
		PS_Challenges[PSCD_DLC_ShootingRange].LessonEnum = PSCD_DLC_ShootingRange
		PS_Challenges[PSCD_DLC_ShootingRange].LockStatus = INT_TO_ENUM(PILOT_SCHOOL_STATUS_ENUM, 0)
		Pilot_School_Data_Set_Goal_Bit(PS_Challenges[PSCD_DLC_ShootingRange],	(FSG_6_targetsDestroyed))
		PS_Challenges[PSCD_DLC_ShootingRange].eEndscreenType = ENDSCREEN_AUTOPILOT
		PS_Challenges[PSCD_DLC_ShootingRange].eEndCamType = PS_END_CAMERA_FOLLOW_SHOT
		PS_Challenges[PSCD_DLC_ShootingRange].vStaticCamPos = <<83.0041, -455.2445, 38.0703>> 
		PS_Challenges[PSCD_DLC_ShootingRange].vStaticCamRot = <<16.3874, -0.0000, 6.4160>>
		PS_Challenges[PSCD_DLC_ShootingRange].fStaticCamFOV = 35.2742
		PS_Challenges[PSCD_DLC_ShootingRange].GoldDistance = 25.0
		PS_Challenges[PSCD_DLC_ShootingRange].SilverDistance = 15.0
		PS_Challenges[PSCD_DLC_ShootingRange].BronzeDistance = 10.0
		
		PS_Challenges[PSCD_DLC_CityLanding].Title = "MGFS_20"
		PS_Challenges[PSCD_DLC_CityLanding].LessonEnum = PSCD_DLC_CityLanding
		PS_Challenges[PSCD_DLC_CityLanding].LockStatus = INT_TO_ENUM(PILOT_SCHOOL_STATUS_ENUM, 0)
		Pilot_School_Data_Set_Goal_Bit(PS_Challenges[PSCD_DLC_CityLanding],	(FSG_2_distanceFromTarget))
	//	Pilot_School_Data_Set_PreReq_Bit(PS_Challenges[PSCD_DLC_CityLanding],	(PSCD_Inverted))
	//	Pilot_School_Data_Set_PreReq_Bit(PS_Challenges[PSCD_DLC_CityLanding],	(PSCD_Knifing))
		PS_Challenges[PSCD_DLC_CityLanding].eEndscreenType = ENDSCREEN_ENGINE_OFF
		PS_Challenges[PSCD_DLC_CityLanding].eEndCamType = PS_END_CAMERA_DYNAMIC_SHOT
		PS_Challenges[PSCD_DLC_CityLanding].GoldDistance = 10.0
		PS_Challenges[PSCD_DLC_CityLanding].SilverDistance = 30.0
		PS_Challenges[PSCD_DLC_CityLanding].BronzeDistance = -1
		
		PS_Challenges[PSCD_DLC_Formation].Title = "MGFS_21"
		PS_Challenges[PSCD_DLC_Formation].LessonEnum = PSCD_DLC_Formation
		PS_Challenges[PSCD_DLC_Formation].LockStatus = INT_TO_ENUM(PILOT_SCHOOL_STATUS_ENUM, 0)
		Pilot_School_Data_Set_Goal_Bit(PS_Challenges[PSCD_DLC_Formation],	(FSG_6_targetsDestroyed))
		PS_Challenges[PSCD_DLC_Formation].eEndscreenType = ENDSCREEN_AUTOPILOT
		PS_Challenges[PSCD_DLC_Formation].eEndCamType = PS_END_CAMERA_STATIC_SHOT
		PS_Challenges[PSCD_DLC_Formation].vStaticCamPos = <<-34.7165, -1219.2566, 193.3193>>
		PS_Challenges[PSCD_DLC_Formation].vStaticCamRot = <<-4.0945, 0.0000, -88.2194>>
		PS_Challenges[PSCD_DLC_Formation].fStaticCamFOV = 71.8
		PS_Challenges[PSCD_DLC_Formation].GoldDistance = 600.0
		PS_Challenges[PSCD_DLC_Formation].SilverDistance = 450.0
		PS_Challenges[PSCD_DLC_Formation].BronzeDistance = 300.0
	ENDIF
	
ENDPROC

FUNC INT GET_ENTRY_FOR_SET_PILOT_SCHOOL()
	RETURN ENUM_TO_INT(g_current_selected_dlc_PilotSchool_class)
ENDFUNC

FUNC BOOL IS_PILOT_SCHOOL_DLC()
	RETURN g_current_selected_dlc_PilotSchool_class >= PSCD_DLC_OutsideLoop
ENDFUNC


PROC save_lesson(int lesson)
	int i
	i = lesson
	SWITCH i
		CASE 0				
			SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_ELAPSEDTIME_0,saveData[i].ElapsedTime)
			SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_LASTELAPSEDTIME_0,saveData[i].LastElapsedTime)
			SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_LASTLANDDISTANCE_0,saveData[i].LastLandingDistance)
			SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_LANDINGDISTANCE_0,saveData[i].LandingDistance)
			SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_MULTIPLIER_0,saveData[i].Multiplier)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_PILOT_CHECKPOINTCOUNT_0,saveData[i].CheckpointCount)
			SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_FORMATIONTIMER_0,saveData[i].FormationTimer)
			SET_MP_BOOL_CHARACTER_STAT(MP_STAT_PILOT_ASPASSEDLESSON_0,saveData[i].HasPassedLesson)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_PILOT_SCHOOL_MEDAL_0,enum_to_int(saveData[i].eMedal))
			SET_MP_INT_CHARACTER_STAT(MP_STAT_PILOT_SCHOOL_LASTMEDAL_0,enum_to_int(saveData[i].eLastMedal))				
		BREAK
		CASE 1			
			SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_ELAPSEDTIME_1,saveData[i].ElapsedTime)
			SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_LASTELAPSEDTIME_1,saveData[i].LastElapsedTime)
			SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_LASTLANDDISTANCE_1,saveData[i].LastLandingDistance)
			SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_LANDINGDISTANCE_1,saveData[i].LandingDistance)
			SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_MULTIPLIER_1,saveData[i].Multiplier)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_PILOT_CHECKPOINTCOUNT_1,saveData[i].CheckpointCount)
			SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_FORMATIONTIMER_1,saveData[i].FormationTimer)
			SET_MP_BOOL_CHARACTER_STAT(MP_STAT_PILOT_ASPASSEDLESSON_1,saveData[i].HasPassedLesson)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_PILOT_SCHOOL_MEDAL_1,enum_to_int(saveData[i].eMedal))
			SET_MP_INT_CHARACTER_STAT(MP_STAT_PILOT_SCHOOL_LASTMEDAL_1,enum_to_int(saveData[i].eLastMedal))	
		BREAK
		CASE 2	
			SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_ELAPSEDTIME_2,saveData[i].ElapsedTime)
			SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_LASTELAPSEDTIME_2,saveData[i].LastElapsedTime)
			SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_LASTLANDDISTANCE_2,saveData[i].LastLandingDistance)
			SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_LANDINGDISTANCE_2,saveData[i].LandingDistance)
			SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_MULTIPLIER_2,saveData[i].Multiplier)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_PILOT_CHECKPOINTCOUNT_2,saveData[i].CheckpointCount)
			SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_FORMATIONTIMER_2,saveData[i].FormationTimer)
			SET_MP_BOOL_CHARACTER_STAT(MP_STAT_PILOT_ASPASSEDLESSON_2,saveData[i].HasPassedLesson)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_PILOT_SCHOOL_MEDAL_2,enum_to_int(saveData[i].eMedal))
			SET_MP_INT_CHARACTER_STAT(MP_STAT_PILOT_SCHOOL_LASTMEDAL_2,enum_to_int(saveData[i].eLastMedal))	
		BREAK
		CASE 3		
			SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_ELAPSEDTIME_3,saveData[i].ElapsedTime)
			SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_LASTELAPSEDTIME_3,saveData[i].LastElapsedTime)
			SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_LASTLANDDISTANCE_3,saveData[i].LastLandingDistance)
			SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_LANDINGDISTANCE_3,saveData[i].LandingDistance)
			SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_MULTIPLIER_3,saveData[i].Multiplier)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_PILOT_CHECKPOINTCOUNT_3,saveData[i].CheckpointCount)
			SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_FORMATIONTIMER_3,saveData[i].FormationTimer)
			SET_MP_BOOL_CHARACTER_STAT(MP_STAT_PILOT_ASPASSEDLESSON_3,saveData[i].HasPassedLesson)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_PILOT_SCHOOL_MEDAL_3,enum_to_int(saveData[i].eMedal))
			SET_MP_INT_CHARACTER_STAT(MP_STAT_PILOT_SCHOOL_LASTMEDAL_3,enum_to_int(saveData[i].eLastMedal))	
		BREAK
		CASE 4	
			SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_ELAPSEDTIME_4,saveData[i].ElapsedTime)
			SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_LASTELAPSEDTIME_4,saveData[i].LastElapsedTime)
			SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_LASTLANDDISTANCE_4,saveData[i].LastLandingDistance)
			SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_LANDINGDISTANCE_4,saveData[i].LandingDistance)
			SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_MULTIPLIER_4,saveData[i].Multiplier)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_PILOT_CHECKPOINTCOUNT_4,saveData[i].CheckpointCount)
			SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_FORMATIONTIMER_4,saveData[i].FormationTimer)
			SET_MP_BOOL_CHARACTER_STAT(MP_STAT_PILOT_ASPASSEDLESSON_4,saveData[i].HasPassedLesson)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_PILOT_SCHOOL_MEDAL_4,enum_to_int(saveData[i].eMedal))
			SET_MP_INT_CHARACTER_STAT(MP_STAT_PILOT_SCHOOL_LASTMEDAL_4,enum_to_int(saveData[i].eLastMedal))	
		BREAK		
		CASE 5		
			SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_ELAPSEDTIME_5,saveData[i].ElapsedTime)
			SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_LASTELAPSEDTIME_5,saveData[i].LastElapsedTime)
			SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_LASTLANDDISTANCE_5,saveData[i].LastLandingDistance)
			SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_LANDINGDISTANCE_5,saveData[i].LandingDistance)
			SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_MULTIPLIER_5,saveData[i].Multiplier)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_PILOT_CHECKPOINTCOUNT_5,saveData[i].CheckpointCount)
			SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_FORMATIONTIMER_5,saveData[i].FormationTimer)
			SET_MP_BOOL_CHARACTER_STAT(MP_STAT_PILOT_ASPASSEDLESSON_5,saveData[i].HasPassedLesson)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_PILOT_SCHOOL_MEDAL_5,enum_to_int(saveData[i].eMedal))
			SET_MP_INT_CHARACTER_STAT(MP_STAT_PILOT_SCHOOL_LASTMEDAL_5,enum_to_int(saveData[i].eLastMedal))	
		BREAK
		CASE 6		
			SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_ELAPSEDTIME_6,saveData[i].ElapsedTime)
			SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_LASTELAPSEDTIME_6,saveData[i].LastElapsedTime)
			SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_LASTLANDDISTANCE_6,saveData[i].LastLandingDistance)
			SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_LANDINGDISTANCE_6,saveData[i].LandingDistance)
			SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_MULTIPLIER_6,saveData[i].Multiplier)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_PILOT_CHECKPOINTCOUNT_6,saveData[i].CheckpointCount)
			SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_FORMATIONTIMER_6,saveData[i].FormationTimer)
			SET_MP_BOOL_CHARACTER_STAT(MP_STAT_PILOT_ASPASSEDLESSON_6,saveData[i].HasPassedLesson)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_PILOT_SCHOOL_MEDAL_6,enum_to_int(saveData[i].eMedal))
			SET_MP_INT_CHARACTER_STAT(MP_STAT_PILOT_SCHOOL_LASTMEDAL_6,enum_to_int(saveData[i].eLastMedal))	
		BREAK
		CASE 7			
			SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_ELAPSEDTIME_7,saveData[i].ElapsedTime)
			SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_LASTELAPSEDTIME_7,saveData[i].LastElapsedTime)
			SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_LASTLANDDISTANCE_7,saveData[i].LastLandingDistance)
			SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_LANDINGDISTANCE_7,saveData[i].LandingDistance)
			SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_MULTIPLIER_7,saveData[i].Multiplier)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_PILOT_CHECKPOINTCOUNT_7,saveData[i].CheckpointCount)
			SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_FORMATIONTIMER_7,saveData[i].FormationTimer)
			SET_MP_BOOL_CHARACTER_STAT(MP_STAT_PILOT_ASPASSEDLESSON_7,saveData[i].HasPassedLesson)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_PILOT_SCHOOL_MEDAL_7,enum_to_int(saveData[i].eMedal))
			SET_MP_INT_CHARACTER_STAT(MP_STAT_PILOT_SCHOOL_LASTMEDAL_7,enum_to_int(saveData[i].eLastMedal))	
		BREAK
		CASE 8		
			SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_ELAPSEDTIME_8,saveData[i].ElapsedTime)
			SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_LASTELAPSEDTIME_8,saveData[i].LastElapsedTime)
			SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_LASTLANDDISTANCE_8,saveData[i].LastLandingDistance)
			SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_LANDINGDISTANCE_8,saveData[i].LandingDistance)
			SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_MULTIPLIER_8,saveData[i].Multiplier)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_PILOT_CHECKPOINTCOUNT_8,saveData[i].CheckpointCount)
			SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_FORMATIONTIMER_8,saveData[i].FormationTimer)
			SET_MP_BOOL_CHARACTER_STAT(MP_STAT_PILOT_ASPASSEDLESSON_8,saveData[i].HasPassedLesson)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_PILOT_SCHOOL_MEDAL_8,enum_to_int(saveData[i].eMedal))
			SET_MP_INT_CHARACTER_STAT(MP_STAT_PILOT_SCHOOL_LASTMEDAL_8,enum_to_int(saveData[i].eLastMedal))	
		BREAK
		CASE 9		
			SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_ELAPSEDTIME_9,saveData[i].ElapsedTime)
			SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_LASTELAPSEDTIME_9,saveData[i].LastElapsedTime)
			SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_LASTLANDDISTANCE_9,saveData[i].LastLandingDistance)
			SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_LANDINGDISTANCE_9,saveData[i].LandingDistance)
			SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_MULTIPLIER_9,saveData[i].Multiplier)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_PILOT_CHECKPOINTCOUNT_9,saveData[i].CheckpointCount)
			SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_FORMATIONTIMER_9,saveData[i].FormationTimer)
			SET_MP_BOOL_CHARACTER_STAT(MP_STAT_PILOT_ASPASSEDLESSON_9,saveData[i].HasPassedLesson)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_PILOT_SCHOOL_MEDAL_9,enum_to_int(saveData[i].eMedal))
			SET_MP_INT_CHARACTER_STAT(MP_STAT_PILOT_SCHOOL_LASTMEDAL_9,enum_to_int(saveData[i].eLastMedal))	
		BREAK
	ENDSWITCH
	i=i
ENDPROC

PROC SAVE_PILOT_SCHOOL_DATA_TO_MP(save_type setSaveType,int lesson = -1)
	CPRINTLN(DEBUG_TREVOR3,"*********************************SAVING PILOT SCHOOL DATA***************************************")
	int i
	
	IF lesson = -1
		REPEAT COUNT_OF(saveData) i
			save_lesson(i)
		ENDREPEAT
	ELSE
		save_lesson(lesson)
	ENDIF
	
	REQUEST_SAVE(SSR_REASON_PILOT_SCHOOL, setSaveType) 
ENDPROC

PROC LOAD_MP_STATS_IN_TO_PILOT_SCHOOL_DATA()
	int i
	REPEAT COUNT_OF(saveData) i
			SWITCH i
				CASE 0
					IF GET_MP_BOOL_CHARACTER_STAT(MP_STAT_PILOT_ASPASSEDLESSON_0)
						saveData[i].ElapsedTime = GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_ELAPSEDTIME_0)
						saveData[i].LastElapsedTime = GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_LASTELAPSEDTIME_0)
						saveData[i].LastLandingDistance = GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_LASTLANDDISTANCE_0)
						saveData[i].LandingDistance = GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_LANDINGDISTANCE_0)
						saveData[i].Multiplier = GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_MULTIPLIER_0)
						saveData[i].CheckpointCount = GET_MP_INT_CHARACTER_STAT(MP_STAT_PILOT_CHECKPOINTCOUNT_0)
						saveData[i].FormationTimer = GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_FORMATIONTIMER_0)
						saveData[i].HasPassedLesson = GET_MP_BOOL_CHARACTER_STAT(MP_STAT_PILOT_ASPASSEDLESSON_0)
						saveData[i].eMedal = INT_TO_ENUM(PILOT_SCHOOL_MEDAL,GET_MP_INT_CHARACTER_STAT(MP_STAT_PILOT_SCHOOL_MEDAL_0))
						saveData[i].eLastMedal = INT_TO_ENUM(PILOT_SCHOOL_MEDAL,GET_MP_INT_CHARACTER_STAT(MP_STAT_PILOT_SCHOOL_LASTMEDAL_0))
					ENDIF
				BREAK
				CASE 1
					IF GET_MP_BOOL_CHARACTER_STAT(MP_STAT_PILOT_ASPASSEDLESSON_1)
						saveData[i].ElapsedTime = GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_ELAPSEDTIME_1)
						saveData[i].LastElapsedTime = GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_LASTELAPSEDTIME_1)
						saveData[i].LastLandingDistance = GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_LASTLANDDISTANCE_1)
						saveData[i].LandingDistance = GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_LANDINGDISTANCE_1)
						saveData[i].Multiplier = GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_MULTIPLIER_1)
						saveData[i].CheckpointCount = GET_MP_INT_CHARACTER_STAT(MP_STAT_PILOT_CHECKPOINTCOUNT_1)
						saveData[i].FormationTimer = GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_FORMATIONTIMER_1)
						saveData[i].HasPassedLesson = GET_MP_BOOL_CHARACTER_STAT(MP_STAT_PILOT_ASPASSEDLESSON_1)
						saveData[i].eMedal = INT_TO_ENUM(PILOT_SCHOOL_MEDAL,GET_MP_INT_CHARACTER_STAT(MP_STAT_PILOT_SCHOOL_MEDAL_1))
						saveData[i].eLastMedal = INT_TO_ENUM(PILOT_SCHOOL_MEDAL,GET_MP_INT_CHARACTER_STAT(MP_STAT_PILOT_SCHOOL_LASTMEDAL_1))
					ENDIF
				BREAK
				CASE 2
					IF GET_MP_BOOL_CHARACTER_STAT(MP_STAT_PILOT_ASPASSEDLESSON_2)
						saveData[i].ElapsedTime = GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_ELAPSEDTIME_2)
						saveData[i].LastElapsedTime = GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_LASTELAPSEDTIME_2)
						saveData[i].LastLandingDistance = GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_LASTLANDDISTANCE_2)
						saveData[i].LandingDistance = GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_LANDINGDISTANCE_2)
						saveData[i].Multiplier = GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_MULTIPLIER_2)
						saveData[i].CheckpointCount = GET_MP_INT_CHARACTER_STAT(MP_STAT_PILOT_CHECKPOINTCOUNT_2)
						saveData[i].FormationTimer = GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_FORMATIONTIMER_2)
						saveData[i].HasPassedLesson = GET_MP_BOOL_CHARACTER_STAT(MP_STAT_PILOT_ASPASSEDLESSON_2)
						saveData[i].eMedal = INT_TO_ENUM(PILOT_SCHOOL_MEDAL,GET_MP_INT_CHARACTER_STAT(MP_STAT_PILOT_SCHOOL_MEDAL_2))
						saveData[i].eLastMedal = INT_TO_ENUM(PILOT_SCHOOL_MEDAL,GET_MP_INT_CHARACTER_STAT(MP_STAT_PILOT_SCHOOL_LASTMEDAL_2))
					ENDIF
				BREAK
				CASE 3	
					IF GET_MP_BOOL_CHARACTER_STAT(MP_STAT_PILOT_ASPASSEDLESSON_3)
						saveData[i].ElapsedTime = GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_ELAPSEDTIME_3)
						saveData[i].LastElapsedTime = GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_LASTELAPSEDTIME_3)
						saveData[i].LastLandingDistance = GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_LASTLANDDISTANCE_3)
						saveData[i].LandingDistance = GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_LANDINGDISTANCE_3)
						saveData[i].Multiplier = GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_MULTIPLIER_3)
						saveData[i].CheckpointCount = GET_MP_INT_CHARACTER_STAT(MP_STAT_PILOT_CHECKPOINTCOUNT_3)
						saveData[i].FormationTimer = GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_FORMATIONTIMER_3)
						saveData[i].HasPassedLesson = GET_MP_BOOL_CHARACTER_STAT(MP_STAT_PILOT_ASPASSEDLESSON_3)
						saveData[i].eMedal = INT_TO_ENUM(PILOT_SCHOOL_MEDAL,GET_MP_INT_CHARACTER_STAT(MP_STAT_PILOT_SCHOOL_MEDAL_3))
						saveData[i].eLastMedal = INT_TO_ENUM(PILOT_SCHOOL_MEDAL,GET_MP_INT_CHARACTER_STAT(MP_STAT_PILOT_SCHOOL_LASTMEDAL_3))
					ENDIF
				BREAK
				CASE 4	
					IF GET_MP_BOOL_CHARACTER_STAT(MP_STAT_PILOT_ASPASSEDLESSON_4)
						saveData[i].ElapsedTime = GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_ELAPSEDTIME_4)
						saveData[i].LastElapsedTime = GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_LASTELAPSEDTIME_4)
						saveData[i].LastLandingDistance = GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_LASTLANDDISTANCE_4)
						saveData[i].LandingDistance = GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_LANDINGDISTANCE_4)
						saveData[i].Multiplier = GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_MULTIPLIER_4)
						saveData[i].CheckpointCount = GET_MP_INT_CHARACTER_STAT(MP_STAT_PILOT_CHECKPOINTCOUNT_4)
						saveData[i].FormationTimer = GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_FORMATIONTIMER_4)
						saveData[i].HasPassedLesson = GET_MP_BOOL_CHARACTER_STAT(MP_STAT_PILOT_ASPASSEDLESSON_4)
						saveData[i].eMedal = INT_TO_ENUM(PILOT_SCHOOL_MEDAL,GET_MP_INT_CHARACTER_STAT(MP_STAT_PILOT_SCHOOL_MEDAL_4))
						saveData[i].eLastMedal = INT_TO_ENUM(PILOT_SCHOOL_MEDAL,GET_MP_INT_CHARACTER_STAT(MP_STAT_PILOT_SCHOOL_LASTMEDAL_4))
					ENDIF
				BREAK		
				CASE 5
					IF GET_MP_BOOL_CHARACTER_STAT(MP_STAT_PILOT_ASPASSEDLESSON_5)
						saveData[i].ElapsedTime = GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_ELAPSEDTIME_5)
						saveData[i].LastElapsedTime = GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_LASTELAPSEDTIME_5)
						saveData[i].LastLandingDistance = GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_LASTLANDDISTANCE_5)
						saveData[i].LandingDistance = GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_LANDINGDISTANCE_5)
						saveData[i].Multiplier = GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_MULTIPLIER_5)
						saveData[i].CheckpointCount = GET_MP_INT_CHARACTER_STAT(MP_STAT_PILOT_CHECKPOINTCOUNT_5)
						saveData[i].FormationTimer = GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_FORMATIONTIMER_5)
						saveData[i].HasPassedLesson = GET_MP_BOOL_CHARACTER_STAT(MP_STAT_PILOT_ASPASSEDLESSON_5)
						saveData[i].eMedal = INT_TO_ENUM(PILOT_SCHOOL_MEDAL,GET_MP_INT_CHARACTER_STAT(MP_STAT_PILOT_SCHOOL_MEDAL_5))
						saveData[i].eLastMedal = INT_TO_ENUM(PILOT_SCHOOL_MEDAL,GET_MP_INT_CHARACTER_STAT(MP_STAT_PILOT_SCHOOL_LASTMEDAL_5))
					ENDIF
				BREAK
				CASE 6	
					IF GET_MP_BOOL_CHARACTER_STAT(MP_STAT_PILOT_ASPASSEDLESSON_6)
						saveData[i].ElapsedTime = GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_ELAPSEDTIME_6)
						saveData[i].LastElapsedTime = GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_LASTELAPSEDTIME_6)
						saveData[i].LastLandingDistance = GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_LASTLANDDISTANCE_6)
						saveData[i].LandingDistance = GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_LANDINGDISTANCE_6)
						saveData[i].Multiplier = GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_MULTIPLIER_6)
						saveData[i].CheckpointCount = GET_MP_INT_CHARACTER_STAT(MP_STAT_PILOT_CHECKPOINTCOUNT_6)
						saveData[i].FormationTimer = GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_FORMATIONTIMER_6)
						saveData[i].HasPassedLesson = GET_MP_BOOL_CHARACTER_STAT(MP_STAT_PILOT_ASPASSEDLESSON_6)
						saveData[i].eMedal = INT_TO_ENUM(PILOT_SCHOOL_MEDAL,GET_MP_INT_CHARACTER_STAT(MP_STAT_PILOT_SCHOOL_MEDAL_6))
						saveData[i].eLastMedal = INT_TO_ENUM(PILOT_SCHOOL_MEDAL,GET_MP_INT_CHARACTER_STAT(MP_STAT_PILOT_SCHOOL_LASTMEDAL_6))
					ENDIF
				BREAK
				CASE 7
					IF GET_MP_BOOL_CHARACTER_STAT(MP_STAT_PILOT_ASPASSEDLESSON_7)
						saveData[i].ElapsedTime = GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_ELAPSEDTIME_7)
						saveData[i].LastElapsedTime = GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_LASTELAPSEDTIME_7)
						saveData[i].LastLandingDistance = GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_LASTLANDDISTANCE_7)
						saveData[i].LandingDistance = GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_LANDINGDISTANCE_7)
						saveData[i].Multiplier = GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_MULTIPLIER_7)
						saveData[i].CheckpointCount = GET_MP_INT_CHARACTER_STAT(MP_STAT_PILOT_CHECKPOINTCOUNT_7)
						saveData[i].FormationTimer = GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_FORMATIONTIMER_7)
						saveData[i].HasPassedLesson = GET_MP_BOOL_CHARACTER_STAT(MP_STAT_PILOT_ASPASSEDLESSON_7)
						saveData[i].eMedal = INT_TO_ENUM(PILOT_SCHOOL_MEDAL,GET_MP_INT_CHARACTER_STAT(MP_STAT_PILOT_SCHOOL_MEDAL_7))
						saveData[i].eLastMedal = INT_TO_ENUM(PILOT_SCHOOL_MEDAL,GET_MP_INT_CHARACTER_STAT(MP_STAT_PILOT_SCHOOL_LASTMEDAL_7))
					ENDIF
				BREAK
				CASE 8
					IF GET_MP_BOOL_CHARACTER_STAT(MP_STAT_PILOT_ASPASSEDLESSON_8)
						saveData[i].ElapsedTime = GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_ELAPSEDTIME_8)
						saveData[i].LastElapsedTime = GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_LASTELAPSEDTIME_8)
						saveData[i].LastLandingDistance = GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_LASTLANDDISTANCE_8)
						saveData[i].LandingDistance = GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_LANDINGDISTANCE_8)
						saveData[i].Multiplier = GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_MULTIPLIER_8)
						saveData[i].CheckpointCount = GET_MP_INT_CHARACTER_STAT(MP_STAT_PILOT_CHECKPOINTCOUNT_8)
						saveData[i].FormationTimer = GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_FORMATIONTIMER_8)
						saveData[i].HasPassedLesson = GET_MP_BOOL_CHARACTER_STAT(MP_STAT_PILOT_ASPASSEDLESSON_8)
						saveData[i].eMedal = INT_TO_ENUM(PILOT_SCHOOL_MEDAL,GET_MP_INT_CHARACTER_STAT(MP_STAT_PILOT_SCHOOL_MEDAL_8))
						saveData[i].eLastMedal = INT_TO_ENUM(PILOT_SCHOOL_MEDAL,GET_MP_INT_CHARACTER_STAT(MP_STAT_PILOT_SCHOOL_LASTMEDAL_8))
					ENDIF
				BREAK
				CASE 9
					IF GET_MP_BOOL_CHARACTER_STAT(MP_STAT_PILOT_ASPASSEDLESSON_9)
						saveData[i].ElapsedTime = GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_ELAPSEDTIME_9)
						saveData[i].LastElapsedTime = GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_LASTELAPSEDTIME_9)
						saveData[i].LastLandingDistance = GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_LASTLANDDISTANCE_9)
						saveData[i].LandingDistance = GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_LANDINGDISTANCE_9)
						saveData[i].Multiplier = GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_MULTIPLIER_9)
						saveData[i].CheckpointCount = GET_MP_INT_CHARACTER_STAT(MP_STAT_PILOT_CHECKPOINTCOUNT_9)
						saveData[i].FormationTimer = GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PILOT_FORMATIONTIMER_9)
						saveData[i].HasPassedLesson = GET_MP_BOOL_CHARACTER_STAT(MP_STAT_PILOT_ASPASSEDLESSON_9)
						saveData[i].eMedal = INT_TO_ENUM(PILOT_SCHOOL_MEDAL,GET_MP_INT_CHARACTER_STAT(MP_STAT_PILOT_SCHOOL_MEDAL_9))
						saveData[i].eLastMedal = INT_TO_ENUM(PILOT_SCHOOL_MEDAL,GET_MP_INT_CHARACTER_STAT(MP_STAT_PILOT_SCHOOL_LASTMEDAL_9))
					ENDIF
				BREAK
			ENDSWITCH
	ENDREPEAT
	
	//Checking loaded values
	#IF IS_DEBUG_BUILD
		FOR i = 0 to 9
			PRINTLN("PS STAT CHECK - LESSON NUM: ", i, " LANDING DISTANCE VAL: ", saveData[i].LandingDistance)
		ENDFOR
	#ENDIF
ENDPROC



