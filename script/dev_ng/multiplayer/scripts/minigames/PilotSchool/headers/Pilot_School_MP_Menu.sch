

//****************************************************************************************************
//
//	Author: 		Asa Dang
//	Date: 			6/24/11
//	Description:	Main menu logic for pilot school. Loads in data to be shown in the menu, then allows 
//					the player to launch a challenge from the UI. Everything here should only deal with 
//					menu flow/logic.
//	
//****************************************************************************************************



USING "minigame_uiinputs.sch"
USING "flow_help_public.sch"
USING "Pilot_School_MP_Challenge_Helpers.sch"
USING "Pilot_School_MP_Definitions.sch"
USING "Pilot_School_MP_Data.sch"
USING "PS_MP_Menu_lib.sch"
#IF IS_DEBUG_BUILD
	USING "Pilot_School_MP_Debug_lib.sch"
#ENDIF



PROC PS_MENU_INIT()

	

	DEBUG_MESSAGE("******************** INIT SCRIPT: Pilot_School_Menu.sch ********************")
	bLBToggle = FALSE
	CLEAR_HELP()
	
	SC_LEADERBOARD_CACHE_CLEAR_ALL()
	
	SET_MAX_WANTED_LEVEL(0)

	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		CLEAR_PED_TASKS(PLAYER_PED_ID())
	ENDIF
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(PS_Main.myVehicle_netID)
		PS_DELETE_NET_ID(PS_Main.myVehicle_netID)
	ENDIF
	
	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)

	SET_FRONTEND_ACTIVE(FALSE)

	bIsLaunchingChallenge = FALSE
	
		
	//setup placements
	INIT_PS_MENU(PS_UI_Placement)


ENDPROC

FUNC BOOL PS_MENU_SETUP()
	
	REQUEST_PILOT_SCHOOL_ASSETS()
	REQUEST_STREAMED_TEXTURE_DICT("Shared")
	
	PS_HUD_HIDE_GAMEHUD_ELEMENTS(TRUE)
	
	IF NOT HAS_LOADED_PILOT_SCHOOL_ASSETS()
		PRINTLN("[PILOT SCHOOL] - HAS_LOADED_PILOT_SCHOOL_ASSETS() = FALSE")
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED("Shared")
		PRINTLN("[PILOT SCHOOL] - HAS_STREAMED_TEXTURE_DICT_LOADED(Shared) = FALSE")
		RETURN FALSE
	ENDIF
	
	IF NOT PS_MENU_SETUP_CAMERA()
		PRINTLN("[PILOT SCHOOL] - PS_MENU_SETUP_CAMERA() = FALSE")
		RETURN FALSE
	ENDIF
	
	PS_HUD_HIDE_GAMEHUD_ELEMENTS(TRUE)
		
	PS_MENU_LOAD_VARIABLES()
	PS_MENU_INIT() //need this here so can tell if menu entries are locked or not

	SET_CURSOR_POSITION(0.5, 0.5)

	RETURN TRUE
ENDFUNC



FUNC BOOL PS_MENU_UPDATE()
	
	PS_HUD_HIDE_GAMEHUD_ELEMENTS(TRUE)

	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		SET_MOUSE_CURSOR_THIS_FRAME()		// Allow user to click on menu items + instructional buttons
	ENDIF
	
	IF bLBToggle
	
		CLEANUP_FLIGHT_SCHOOL_LESSON_GLOBAL_BEST(iWorldRecordLoadLesson,iWolrdRecordReadStage,iWorldRecordLoadStage,bWorldRecordStore)
	
		IF IS_PLAYER_ONLINE()
			
			IF NOT IS_PAUSE_MENU_ACTIVE()
				IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
					bLBToggle = !bLBToggle
					PS_CLEANUP_SOCIAL_CLUB_LEADERBOARD()
					PS_MENU_SFX_PLAY_NAV_BACK()
					INIT_PS_MENU_BUTTONS()
				ENDIF
			ENDIF
			
			IF GET_CONFIRM_INVITE_INTO_GAME_STATE()
				bLBToggle = !bLBToggle
				PS_CLEANUP_SOCIAL_CLUB_LEADERBOARD()
			ENDIF
			
			IF bLBToggle //if it's not been cleaned up above
				PS_DISPLAY_SOCIAL_CLUB_LEADERBOARD(PS_UI_Leaderboard, g_current_selected_dlc_PilotSchool_class, PS_Challenges[g_current_selected_dlc_PilotSchool_class].Title)
			ENDIF
			
			IF NOT bLBViewProfile AND SHOULD_PROFILE_BUTTON_BE_AVAILABLE_FOR_LEADERBOARD(psLBControl)
				bLBViewProfile = TRUE
				INIT_PS_LB_BUTTONS(bLBViewProfile)
			ENDIF
			UPDATE_SIMPLE_USE_CONTEXT(menuInstructions)
		ELSE
			IF DO_SIGNED_OUT_WARNING(iBS)
				iBS = 0
				bLBToggle = !bLBToggle //leave lbs
				INIT_PS_MENU_BUTTONS()
			ENDIF
		ENDIF
		RETURN TRUE
	ELSE
		IF bShowingOfflineLBButton AND IS_PLAYER_ONLINE()
			INIT_PS_MENU_BUTTONS()
			bShowingOfflineLBButton = FALSE
		ENDIF
		
		IF NOT IS_PAUSE_MENU_ACTIVE()
			IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_LEADERBOARD) 
			AND (NOT IS_PLAYER_ONLINE() OR NOT PS_IS_LEADERBOARD_WRITER_ACTIVE()) 
			AND !bShowQuitMenu
				bLBToggle = !bLBToggle
				PS_MENU_SFX_PLAY_NAV_SELECT()
				bLBViewProfile = SHOULD_PROFILE_BUTTON_BE_AVAILABLE_FOR_LEADERBOARD(psLBControl)
				INIT_PS_LB_BUTTONS(bLBViewProfile)
				RETURN TRUE
			ELIF PS_MENU_GET_INPUT(bIsLaunchingChallenge) //Keep going till we either push the button to Launch a challenge or Exit
				//are we transitioning, or is quit menu is fully on
				HIDE_HUD_AND_RADAR_THIS_FRAME()
				IF bShowQuitMenu
					//if either, then display UI
					SET_WARNING_MESSAGE_WITH_HEADER("PS_QTITLE", "PS_QUIT_DLC", FE_WARNING_YES | FE_WARNING_NO)
				ELIF (DOES_CAM_EXIST(camMainMenu) AND NOT IS_CAM_INTERPOLATING(camMainMenu))
					IF IS_CAM_ACTIVE(camMainMenu)
						RENDER_SCRIPT_CAMS(TRUE, FALSE)
					ENDIF
					PROCESS_SCREEN_PS_MENU(PS_UI_Placement)
				ELIF NOT DOES_CAM_EXIST(camMainMenu)
					PROCESS_SCREEN_PS_MENU(PS_UI_Placement)
				ENDIF

				RETURN TRUE

			ENDIF
		ELSE
			RETURN TRUE
		ENDIF
	ENDIF
		
	RETURN FALSE
	
	
ENDFUNC

PROC PS_MENU_CLEANUP()
	
	#IF IS_DEBUG_BUILD
		IF DOES_WIDGET_GROUP_EXIST(PS_Widget_DLC)
			DELETE_WIDGET_GROUP(PS_Widget_DLC)
		ENDIF
	#ENDIF	
	
	//Launching or exiting?
	IF bIsLaunchingChallenge
		CPRINTLN(debug_Trevor3,"update challenge data")
		PS_Main.myChallengeData = PS_Challenges[g_current_selected_dlc_PilotSchool_class]
	ENDIF
	
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("PS_Menu")
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("PS_MenuDLC")
	
	//SET_MAX_WANTED_LEVEL(5) //removed for bug 1957962. Can see no reason why we'd want to reset the 
	
	CLEAR_RANK_REDICTION_DETAILS() 
	
	SC_LEADERBOARD_CACHE_CLEAR_ALL()

	//stop getting world record data.
	g_worldrecordLoadState = WORLD_RECORD_WAITING_FOR_CHANGE
	
	CLEANUP_FLIGHT_SCHOOL_LESSON_GLOBAL_BEST(iWorldRecordLoadLesson,iWolrdRecordReadStage,iWorldRecordLoadStage,bWorldRecordStore)

	CLEAR_HELP()
	
	SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(quitUI)
	SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(PS_UI_Leaderboard)
	
	TRIGGER_SCREENBLUR_FADE_OUT(0)
	
	
	KILL_FACE_TO_FACE_CONVERSATION()
	DEBUG_MESSAGE("******************** END SCRIPT: Pilot_School_Menu.sch ********************")
ENDPROC



