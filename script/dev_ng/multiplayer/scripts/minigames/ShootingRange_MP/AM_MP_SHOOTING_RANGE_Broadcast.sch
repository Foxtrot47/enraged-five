
USING "net_events.sch"

STRUCT RANGE_MP_EVENT
	STRUCT_EVENT_COMMON_DETAILS Details
	INT	iTargetHitIndex
	BOOL bIgnoreSound
ENDSTRUCT

PROC BROADCAST_TARGET_HIT(INT iTargetID, BOOL bIgnoreSound)
	RANGE_MP_EVENT sToSend
	sToSend.Details.Type = SCRIPT_EVENT_SHOOT_RANGE_TARGET_HIT
	sToSend.Details.FromPlayerIndex	= PLAYER_ID()
	sToSend.iTargetHitIndex = iTargetID
	sToSend.bIgnoreSound = bIgnoreSound
	
	INT iPlayers = ALL_PLAYERS_ON_SCRIPT(FALSE)
	IF iPlayers <> 0
		CDEBUG1LN(DEBUG_SHOOTRANGE, "BROADCAST_TARGET_HIT - sToSend.iTargetHitIndex: ", sToSend.iTargetHitIndex, " iTargetID: ", iTargetID)
		SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, sToSend, SIZE_OF(sToSend), iPlayers)
	ENDIF
ENDPROC

PROC BROADCAST_TARGET_SPAWN(INT iTargetID, BOOL bIgnoreSound)
	RANGE_MP_EVENT sToSend
	sToSend.Details.Type = SCRIPT_EVENT_SHOOT_RANGE_TARGET_SPAWN
	sToSend.Details.FromPlayerIndex	= PLAYER_ID()
	sToSend.iTargetHitIndex = iTargetID
	sToSend.bIgnoreSound = bIgnoreSound
	
	INT iPlayers = ALL_PLAYERS_ON_SCRIPT(FALSE)
	IF iPlayers <> 0
		CDEBUG1LN(DEBUG_SHOOTRANGE, "BROADCAST_TARGET_SPAWN - sToSend.iTargetHitIndex: ", sToSend.iTargetHitIndex, " iTargetID: ", iTargetID)
		SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, sToSend, SIZE_OF(sToSend), iPlayers)
	ENDIF
ENDPROC


