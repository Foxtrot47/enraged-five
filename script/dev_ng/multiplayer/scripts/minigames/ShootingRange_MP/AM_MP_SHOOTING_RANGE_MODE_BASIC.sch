USING "AM_MP_SHOOTING_RANGE_PLAYER.sch"
USING "model_enums.sch"
USING "commands_entity.sch"
USING "AM_MP_SHOOTING_RANGE_Target.sch"
USING "AM_MP_SHOOTING_RANGE_LOCATIONS.sch"
USING "AM_MP_SHOOTING_RANGE_Core.sch"
USING "AM_MP_SHOOTING_RANGE_Core.sch"

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════════════╡ CONSTANTS  ╞══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛
CONST_INT	CI_NO_TARGET_HIT					-1
//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════════╡ VARS  ╞═════════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

SCRIPT_TIMER TargetRespawnTimer 
INT			 iNextTargetSpawnDelay = 0
//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════════════╡ FUNCTIONS  ╞══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

//ASSETS
PROC REQUEST_TARGET_ASSETS_MODE_BASIC()
	REQUEST_MODEL(PROP_TARGET_ORA_PURP_01)
	CDEBUG3LN(DEBUG_SHOOTRANGE, "REQUESTED TARGET MODELS")
ENDPROC

FUNC BOOL HAVE_TARGET_ASSETS_LOADED_MODE_BASIC()
	BOOL bHaveAllTargetAssetsLoaded = TRUE
	
	REQUEST_MODEL(PROP_TARGET_ORA_PURP_01)
	IF NOT HAS_MODEL_LOADED(PROP_TARGET_ORA_PURP_01)
		CDEBUG3LN(DEBUG_SHOOTRANGE, "PROP_TARGET_ORA_PURP_01 - NOT LOADED")
		bHaveAllTargetAssetsLoaded = FALSE
	ENDIF
	
	REQUEST_MODEL(prop_golf_ball)
	IF NOT HAS_MODEL_LOADED(prop_golf_ball)
		CDEBUG3LN(DEBUG_SHOOTRANGE, "PROP_TARGET_ORA_PURP_01 - NOT LOADED")
		bHaveAllTargetAssetsLoaded = FALSE
	ENDIF
	
	CDEBUG3LN(DEBUG_SHOOTRANGE, "ALL ASSETS LOADED")
	RETURN bHaveAllTargetAssetsLoaded
ENDFUNC

PROC TOGGLE_TARGET_ALIVE(TARGET_STRUCT &sTarget, BOOL bAlive = TRUE, BOOL bBroadcastEvent = TRUE, BOOL bIgnoreSound = FALSE)
	IF DOES_ENTITY_EXIST(sTarget.entityTarget)
		IF bAlive
			sTarget.iTargetExtraHitCount = 0
			sTarget.fTargetAliveTime = 0.0
			sTarget.iTargetHitCount = 0
			SET_ENTITY_HEALTH(sTarget.entityTarget, CI_TARGET_ALIVE_HEALTH)
		ELSE
			SET_ENTITY_HEALTH(sTarget.entityTarget, CI_TARGET_ALIVE_HEALTH) // Allow Extra hits, due to this just reset health.
		ENDIF
		
		IF bBroadcastEvent
			IF bAlive
				BROADCAST_TARGET_SPAWN(sTarget.iTargetID, bIgnoreSound)
			ELSE
				BROADCAST_TARGET_HIT(sTarget.iTargetID, bIgnoreSound)
			ENDIF
		ENDIF
		TOGGLE_TARGET_FLAG(sTarget, TF_ALIVE, bAlive)
		TOGGLE_TARGET_FLAG(sTarget, TF_FLIP)
		
		IF NOT bIgnoreSound
			IF bAlive
				CDEBUG1LN(DEBUG_SHOOTRANGE, "TOGGLE_TARGET_ALIVE - Playing up sound")
				PLAY_SOUND_FROM_ENTITY(-1, "Target_Activate", sTarget.entityTarget, "DLC_GR_Bunker_Shooting_Range_Sounds")
			ELSE
				CDEBUG1LN(DEBUG_SHOOTRANGE, "TOGGLE_TARGET_ALIVE - Playing down sound")
				PLAY_SOUND_FROM_ENTITY(-1, "Target_Deactivate", sTarget.entityTarget, "DLC_GR_Bunker_Shooting_Range_Sounds")
			ENDIF
		ENDIF
		
		CLEAR_ENTITY_LAST_DAMAGE_ENTITY(sTarget.entityTarget)
		
		CDEBUG1LN(DEBUG_SHOOTRANGE, "TOGGLE_TARGET_ALIVE - TARGETID: ", sTarget.iTargetID, " bAlive: ", bAlive)
	ELSE
		CDEBUG1LN(DEBUG_SHOOTRANGE, "TOGGLE_TARGET_ALIVE - Entity does not exist")
	ENDIF
ENDPROC

/// PURPOSE:
//		store all the posible target locations, for all players.
PROC INITIALIZE_TARGETS_MODE_BASIC(LOCAL_PLAYER_DATA &lpd)
	POPULATE_TARGET_LOCATIONS(lpd)
	
	INT iTarget
	REPEAT CI_MAX_TARGETS iTarget
		IF DOES_ENTITY_EXIST(lpd.oTargets[iTarget].entityTarget)
			TOGGLE_TARGET_ALIVE(lpd.oTargets[iTarget], FALSE, FALSE, TRUE)
		ENDIF
	ENDREPEAT
	
ENDPROC

PROC INITIALIZE_MODE_BASIC(LOCAL_PLAYER_DATA &lpd, TARGET_SPAWN_SETUP_STRUCT sTargetSpawnSetup, GAME_RANGE eNewGameRange = eGR_INVALID, WEAPON_TYPE eWeapon = WEAPONTYPE_INVALID)
	lpd.CurrentGameRange = eNewGameRange
	lpd.eWeapon = eWeapon
	lpd.sTargetSpawnSetup = sTargetSpawnSetup
	
	INT iTarget
	REPEAT CI_MAX_TARGETS iTarget
		TOGGLE_TARGET_FLAG(lpd.oTargets[iTarget], TF_RANGE_VALID, IS_BITMASK_AS_ENUM_SET(lpd.oTargets[iTarget].iTargetRangeBitSet, lpd.CurrentGameRange))
		CDEBUG3LN(DEBUG_SHOOTRANGE, "TOGGLE_TARGET_FLAG: iTarget: ", iTarget, " TF_RANGE_VALID: ", IS_BITMASK_AS_ENUM_SET(lpd.oTargets[iTarget].iTargetRangeBitSet, lpd.CurrentGameRange))
	ENDREPEAT
	
	CDEBUG3LN(DEBUG_SHOOTRANGE, "")
	CDEBUG3LN(DEBUG_SHOOTRANGE, "############### INITIALIZE ##############")
	lpd.iTargetStartIndex = CI_MAX_TARGET_PER_PLAYER_BASIC * lpd.iShooterID
	lpd.iTargetEndIndex = CI_MAX_TARGET_PER_PLAYER_BASIC * lpd.iShooterID + CI_MAX_TARGET_PER_PLAYER_BASIC - 1
	CDEBUG3LN(DEBUG_SHOOTRANGE, "INITIALIZE_TARGETS_MODE_BASIC - lpd.iTargetStartIndex: ", lpd.iTargetStartIndex, " lpd.iTargetEndIndex: ", lpd.iTargetEndIndex)
	CDEBUG3LN(DEBUG_SHOOTRANGE, "INITIALIZE_TARGETS_MODE_BASIC - iShooterID: ", lpd.iShooterID)
	CDEBUG3LN(DEBUG_SHOOTRANGE, "INITIALIZE_TARGETS_MODE_BASIC - eWeapon: ", ENUM_TO_INT(lpd.eWeapon))
	CDEBUG3LN(DEBUG_SHOOTRANGE, "INITIALIZE_TARGETS_MODE_BASIC - CurrentGameRange: ", ENUM_TO_INT(lpd.CurrentGameRange))
	CDEBUG3LN(DEBUG_SHOOTRANGE, "INITIALIZE_TARGETS_MODE_BASIC - iNumActiveTargets: ", ENUM_TO_INT(lpd.iNumActiveTargets))
	CDEBUG3LN(DEBUG_SHOOTRANGE, "############### ########## ##############")
	CDEBUG3LN(DEBUG_SHOOTRANGE, "")
ENDPROC

/// PURPOSE:
///    Check if a target was hit by a weapon
FUNC BOOL HAS_TARGET_BEEN_HIT(TARGET_STRUCT oTarget, VECTOR &vHitLocation)	

	IF HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(oTarget.entityTarget, WEAPONTYPE_INVALID, GENERALWEAPON_TYPE_ANYWEAPON)
		
		CDEBUG1LN(DEBUG_SHOOTRANGE, "**** HAS_TARGET_BEEN_HIT - TARGET HIT, ID: ")
		CLEAR_ENTITY_LAST_DAMAGE_ENTITY(oTarget.entityTarget)
				
		//Normal bullets
		IF GET_PED_LAST_WEAPON_IMPACT_COORD(PLAYER_PED_ID(), vHitLocation)
			CDEBUG1LN(DEBUG_SHOOTRANGE, "**** HAS_TARGET_BEEN_HIT - LAST HIT COORD: ", vHitLocation)
			RETURN TRUE
		ELSE
			CDEBUG2LN(DEBUG_SHOOTRANGE, "**** HAS_TARGET_BEEN_HIT - No impact")
			RETURN FALSE
		ENDIF
	ELSE
		CDEBUG1LN(DEBUG_SHOOTRANGE, "**** HAS_TARGET_BEEN_HIT - WAS NOT HIT!, ID: ", oTarget.iTargetID)
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC VECTOR TRANSFORM_HIT_COORD_TO_TARGET(VECTOR vTargetPos, VECTOR vTargetRot, VECTOR vHitPos)
	VECTOR vLocalHitPosition

	vLocalHitPosition = vHitPos - vTargetPos
	ROTATE_VECTOR_FMMC(vLocalHitPosition, <<vTargetRot.x * -1, vTargetRot.y *-1, vTargetRot.z *-1>>)
	
	RETURN vLocalHitPosition
ENDFUNC

FUNC BOOL IS_WEAPON_ALLOWED_A_MISS(WEAPON_TYPE eWeapon)
	IF eWeapon = WEAPONTYPE_CARBINERIFLE
	OR eWeapon = WEAPONTYPE_ASSAULTRIFLE
	OR eWeapon = WEAPONTYPE_COMBATMG
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Check if local player hit one of his targets, assign score, broadcast.
PROC UPDATE_HIT_SCAN_MODE_BASIC(LOCAL_PLAYER_DATA &lpd)
	BOOL bMissed = TRUE
	
	lpd.bTargetDiedThisFrame = FALSE
	
	IF lpd.bPlayerShot
		INT iTarget
		
		//loop only targets dedicated to specific player.
		FOR iTarget = lpd.iTargetStartIndex TO lpd.iTargetEndIndex
//			CDEBUG2LN(DEBUG_SHOOTRANGE, "UPDATE_HIT_SCAN_MODE_BASIC - Loop Target: ", iTarget, 
//			" Alive: ", GET_TARGET_FLAG(lpd.oTargets[iTarget], TF_ALIVE),
//			" Valid: ", GET_TARGET_FLAG(lpd.oTargets[iTarget], TF_RANGE_VALID), 
//			" Init: ", GET_TARGET_FLAG(lpd.oTargets[iTarget], TF_INITIALIZED),
//			" extra? ", lpd.bTargetsTakeExtraHits, 
//			" threshold? ", GET_TARGET_FLAG(lpd.oTargets[iTarget], TF_PAST_MIN_HITABLE_THRESHOLD))	
			
			IF (GET_TARGET_FLAG(lpd.oTargets[iTarget], TF_INITIALIZED)
				AND GET_TARGET_FLAG(lpd.oTargets[iTarget], TF_ALIVE)
				AND GET_TARGET_FLAG(lpd.oTargets[iTarget], TF_RANGE_VALID))
			
			OR (GET_TARGET_FLAG(lpd.oTargets[iTarget], TF_INITIALIZED)
			AND	GET_TARGET_FLAG(lpd.oTargets[iTarget], TF_RANGE_VALID)
			AND (lpd.bTargetsTakeExtraHits AND NOT GET_TARGET_FLAG(lpd.oTargets[iTarget], TF_PAST_MIN_HITABLE_THRESHOLD))
			//Check to see how amny extra times can be hit when going down.
			AND (lpd.iMaxNumExtraHits = -1 OR lpd.oTargets[iTarget].iTargetExtraHitCount < lpd.iMaxNumExtraHits))
				IF DOES_ENTITY_EXIST(lpd.oTargets[iTarget].entityTarget)
					CDEBUG2LN(DEBUG_SHOOTRANGE, "UPDATE_HIT_SCAN_MODE_BASIC - Alive: targetID: ", iTarget)
					TARGET_HIT_LOCATION_ENUM hitLocation = THL_NONE
					VECTOR vHitCoord = <<0.0, 0.0, 0.0>>
					
					IF HAS_TARGET_BEEN_HIT(lpd.oTargets[iTarget], vHitCoord)		
						BOOL bValidHit = FALSE
						
						INT iScore
						iScore = TARGET_GET_HIT_LOCATION_SCORE(lpd.oTargets[iTarget], vHitCoord, hitLocation, bValidHit)
						
						IF bValidHit
							lpd.bCheckNextFrame = FALSE
							
							bMissed = FALSE
							lpd.iMissedCount = 0
							
							iScore *= lpd.iMultiplier
													
							CDEBUG2LN(DEBUG_SHOOTRANGE, "UPDATE_HIT_SCAN_MODE_BASIC - LAST HIT iScore: ", iScore)	
							lpd.iTargetScore += iScore
							CDEBUG2LN(DEBUG_SHOOTRANGE, "UPDATE_HIT_SCAN_MODE_BASIC - Player Score: ", lpd.iTargetScore)
							
							TARGET_PLAY_HIT_SOUND(lpd.oTargets[iTarget], hitLocation)
							SHOOTING_RANGE_ADD_FLOATING_SCORE(iScore, HUD_COLOUR_WHITE, lpd.floatingScores)
							lpd.oTargets[iTarget].iTargetHitCount++
							lpd.iTargetsHit++
							
							//IF not alive and can take extra hits, register them.
							IF lpd.bTargetsTakeExtraHits
							AND NOT GET_TARGET_FLAG(lpd.oTargets[iTarget], TF_ALIVE)
								CDEBUG1LN(DEBUG_SHOOTRANGE, "UPDATE_HIT_SCAN_MODE_BASIC - Extra Hit Count")
								lpd.oTargets[iTarget].iTargetExtraHitCount ++
							ENDIF
							
							IF lpd.iMultiplier < 3
								lpd.iCurrentHitCount++
								
								IF lpd.iCurrentHitCount > lpd.iMaxHitCount
									lpd.iCurrentHitCount = 0
									lpd.iMultiplier++
								ENDIF
							ELIF lpd.iMultiplier = 3
								IF lpd.iCurrentHitCount < lpd.iMaxHitCount
									lpd.iCurrentHitCount++
								ENDIF
							ENDIF
							
							IF NOT lpd.bTargetsTakeExtraHits	
							OR (lpd.bTargetsTakeExtraHits AND GET_TARGET_FLAG(lpd.oTargets[iTarget], TF_ALIVE))
								IF lpd.oTargets[iTarget].iTargetHitCount >= GET_MIN_TARGET_HIT_FOR_WEAPON(lpd.eWeapon) 
									lpd.iLastDeadTargetID = iTarget
									lpd.bTargetDiedThisFrame = TRUE
									lpd.oTargets[iTarget].iTargetHitCount = 0
									
									TOGGLE_TARGET_ALIVE(lpd.oTargets[iTarget], FALSE)
									//#IF FEATURE_GUNRUNNING
									//BROADCAST_TARGET_HIT(iTarget)
									//#ENDIF
								ENDIF
							ENDIF
							
							REINIT_NET_TIMER(TargetRespawnTimer)
						ELSE
							CDEBUG2LN(DEBUG_SHOOTRANGE, "UPDATE_HIT_SCAN_MODE_BASIC - Not a valid hit") 
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
		
		IF bMissed
		AND lpd.bCheckNextFrame
			lpd.bCheckNextFrame = FALSE
			
			IF IS_WEAPON_ALLOWED_A_MISS(lpd.eWeapon)
				IF (lpd.iMissedCount < g_sMPTunables.IGR_SHOOTING_RANGE_MULTIPLIER_MISS_ALLOWANCE)
					lpd.iMissedCount++
				ELSE
					lpd.iCurrentHitCount = 0
					lpd.iMultiplier = 1
					lpd.iMissedCount = 0
				ENDIF
			ELSE
				lpd.iCurrentHitCount = 0
				lpd.iMultiplier = 1
				lpd.iMissedCount = 0
			ENDIF
		ENDIF
		
		IF bMissed
		AND lpd.bPlayerJustShot
			lpd.bCheckNextFrame = TRUE
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    populates the array returning new size of active elements.
/// PARAMS:
///    iArray - 
///    iActiveElementCount - Last unused index
///    iNewValue - which value to assign to the availible index   
FUNC BOOL ASSIGN_EMPTY_ELEMENT(INT & iArray[], INT & iActiveElementCount, INT iNewValue)
	IF iActiveElementCount >= COUNT_OF(iArray)
		ASSERTLN("SET_ARRAY_ELEMENT - can not count up iActiveElementCount: ", iActiveElementCount, " CountOF: ", COUNT_OF(iArray))
		RETURN FALSE
	ENDIF
	
	iArray[iActiveElementCount] = iNewValue
	iActiveElementCount++
	RETURN TRUE
ENDFUNC

FUNC BOOL GET_RANDOM_TARGET_SIMPLE_ARRAY(INT &iPossibleInternalArray[], LOCAL_PLAYER_DATA &lpd, INT & iAliveArray[], INT iAliveArrayCount,  INT &iOutRandomTargetIndex)

	CDEBUG2LN(DEBUG_SAFEHOUSE, "")
	CDEBUG2LN(DEBUG_SAFEHOUSE, "############## TARGET SELECT START ###############")
	IF iAliveArrayCount = 0
		CDEBUG2LN(DEBUG_SAFEHOUSE, "iAliveArrayCount = 0")
	ENDIF

	CDEBUG2LN(DEBUG_SAFEHOUSE, "GET_RANDOM_TARGET_SIMPLE - iTargetStartID: ", lpd.iTargetStartIndex, " iTargetEndID: ", lpd.iTargetEndIndex)
	INT iPossibleLastID = 0
	INT iPossibleNum = 0
	INT iExludeID = 0
	INT iTargetID = 0
	BOOL bIsExcluded
	
	FOR iTargetID = lpd.iTargetStartIndex TO lpd.iTargetEndIndex
		bIsExcluded = FALSE
		
		IF iAliveArrayCount > 0
			FOR iExludeID = 0 TO iAliveArrayCount - 1
				IF iTargetID = iAliveArray[iExludeID]
					bIsExcluded = TRUE
					CDEBUG2LN(DEBUG_SAFEHOUSE, "GET_RANDOM_TARGET_SIMPLE_ARRAY - ID: ", iTargetID, " is excluded")
				ENDIF
			ENDFOR
		ENDIF
		
		IF NOT bIsExcluded
			ASSIGN_EMPTY_ELEMENT(iPossibleInternalArray, iPossibleLastID, iTargetID)
			iPossibleNum++
			CDEBUG2LN(DEBUG_SAFEHOUSE, "GET_RANDOM_TARGET_SIMPLE_ARRAY - POSSIBLE ID: ", iTargetID, " iNumOfPossible: ", iPossibleNum)
		ENDIF
	ENDFOR
	
	//If there is at least one possible target, try grabing random.
	IF iPossibleNum > 0
		INT iRandomIndex = GET_RANDOM_INT_IN_RANGE(0, iPossibleLastID)
		iOutRandomTargetIndex = iPossibleInternalArray[iRandomIndex]
		
		CDEBUG2LN(DEBUG_SAFEHOUSE, "GET_RANDOM_TARGET_SIMPLE - RANDOM ID: ", iRandomIndex, " TARGET ID: ", iOutRandomTargetIndex, " LAST POSSIBLE ID: ", iPossibleLastID)
		CDEBUG2LN(DEBUG_SAFEHOUSE, "############## TARGET SELECT END ###############")
		CDEBUG2LN(DEBUG_SAFEHOUSE, "")
		RETURN TRUE
	ENDIF
	
	CDEBUG2LN(DEBUG_SAFEHOUSE, "GET_RANDOM_TARGET_SIMPLE - WAS UNABLE TO FIND A TARGET NO POSSIBLE TARETS")
	CDEBUG2LN(DEBUG_SAFEHOUSE, "############## TARGET SELECT END ###############")
	RETURN FALSE
ENDFUNC

PROC UPDATE_TARGET_ALIVE_TIME(LOCAL_PLAYER_DATA &lpd)
	IF lpd.fMaxTargetAliveTime > 0.0
		INT iTarget
		FOR iTarget = lpd.iTargetStartIndex TO lpd.iTargetEndIndex
			IF GET_TARGET_FLAG(lpd.oTargets[iTarget], TF_INITIALIZED)
			AND GET_TARGET_FLAG(lpd.oTargets[iTarget], TF_RANGE_VALID)
			AND GET_TARGET_FLAG(lpd.oTargets[iTarget], TF_ALIVE)			
				IF DOES_ENTITY_EXIST(lpd.oTargets[iTarget].entityTarget)
					lpd.oTargets[iTarget].fTargetAliveTime += GET_FRAME_TIME()
					IF lpd.oTargets[iTarget].fTargetAliveTime >= lpd.fMaxTargetAliveTime
						TOGGLE_TARGET_ALIVE(lpd.oTargets[iTarget], FALSE)
						lpd.bTargetDiedThisFrame = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
ENDPROC

/// PURPOSE:
///    Check if new target needs to be spawned. 
PROC UPDATE_TARGET_SPAWN_MODE_BASIC(LOCAL_PLAYER_DATA &lpd)
	BOOL bFindNewTarget = FALSE
	INT iExcludeTargetIndexArray[CI_MAX_TARGET_PER_PLAYER_BASIC]
	INT iExcludeTargetNum = 0
	INT iAliveTargetNum = 0
	INT iTarget = 0
	
	//Check if timers reached a new target request time.
	IF NOT HAS_NET_TIMER_STARTED(TargetRespawnTimer)
		START_NET_TIMER(TargetRespawnTimer)
		IF iNextTargetSpawnDelay <> lpd.sTargetSpawnSetup.iTargetNoHitSpawnDelay
		CDEBUG1LN(DEBUG_SHOOTRANGE, "+++&&&&&&&&&&&&& FIND NEW TARGET > : Not Started Timer")
		ENDIF
		iNextTargetSpawnDelay = lpd.sTargetSpawnSetup.iTargetNoHitSpawnDelay
		bFindNewTarget = TRUE	
	ELIF HAS_NET_TIMER_EXPIRED(TargetRespawnTimer, iNextTargetSpawnDelay)
		IF iNextTargetSpawnDelay <> lpd.sTargetSpawnSetup.iTargetNoHitSpawnDelay
		CDEBUG1LN(DEBUG_SHOOTRANGE, "+++&&&&&&&&&&&&& FIND NEW TARGET > : ", iNextTargetSpawnDelay)
		ENDIF
		iNextTargetSpawnDelay = lpd.sTargetSpawnSetup.iTargetNoHitSpawnDelay
		bFindNewTarget = TRUE	
	ELIF HAS_NET_TIMER_EXPIRED(TargetRespawnTimer, lpd.sTargetSpawnSetup.iTargetNoHitSpawnDelay)
		//CDEBUG1LN(DEBUG_SHOOTRANGE, "+++&&&&&&&&&&&&& FIND NEW TARGET > : ", lpd.sTargetSpawnSetup.iTargetNoHitSpawnDelay)
		bFindNewTarget = TRUE	
	ENDIF
	
	//Calc, store number of alive players.
	IF bFindNewTarget
	OR lpd.bTargetDiedThisFrame
		FOR iTarget = lpd.iTargetStartIndex TO lpd.iTargetEndIndex
			IF iTarget = lpd.iLastDeadTargetID
			OR NOT GET_TARGET_FLAG(lpd.oTargets[iTarget], TF_RANGE_VALID)
				//CDEBUG1LN(DEBUG_SHOOTRANGE, "UPDATE_TARGET_SPAWN_MODE_BASIC - added empty element: ", iTarget)
				ASSIGN_EMPTY_ELEMENT(iExcludeTargetIndexArray, iExcludeTargetNum, iTarget)
			ELSE
				IF GET_TARGET_FLAG(lpd.oTargets[iTarget], TF_INITIALIZED)
				AND GET_TARGET_FLAG(lpd.oTargets[iTarget], TF_ALIVE)// OR (GET_TARGET_FLAG(lpd.oTargets[iTarget], TF_ALIVE) AND GET_TARGET_FLAG(lpd.oTargets[iTarget], TF_FLIP))))			
					IF DOES_ENTITY_EXIST(lpd.oTargets[iTarget].entityTarget)
						IF GET_ENTITY_HEALTH(lpd.oTargets[iTarget].entityTarget) > CI_TARGET_DEAD_HEALTH
							//CDEBUG1LN(DEBUG_SHOOTRANGE, "UPDATE_TARGET_SPAWN_MODE_BASIC - added valid target: ", iTarget)
							ASSIGN_EMPTY_ELEMENT(iExcludeTargetIndexArray, iExcludeTargetNum, iTarget)
							iAliveTargetNum++
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
	
	//Set time delay to spawn new target based on current num of targets alive.
	IF lpd.bTargetDiedThisFrame
		IF iAliveTargetNum = 0
			iNextTargetSpawnDelay = lpd.sTargetSpawnSetup.iTargetDeadHitRespawnDelay
			CDEBUG1LN(DEBUG_SHOOTRANGE, "&&&&&&&&&&&&&&&& bTargetHitThisFrame - : ", lpd.sTargetSpawnSetup.iTargetDeadHitRespawnDelay)
		ELSE	
			iNextTargetSpawnDelay = lpd.sTargetSpawnSetup.iTargetAliveHitRespawnDelay
			CDEBUG1LN(DEBUG_SHOOTRANGE, "&&&&&&&&&&&&&&&& bTargetHitThisFrame - : ", lpd.sTargetSpawnSetup.iTargetAliveHitRespawnDelay)
		ENDIF
	ENDIF
	
	//If not enough targets spawn new.
	IF bFindNewTarget
		IF iAliveTargetNum < lpd.sTargetSpawnSetup.iMaxAliveTargets				
			INT iPossibleTargetIndexArray[CI_MAX_TARGETS]
			INT iNewRandomTarget = 0
			
			CDEBUG1LN(DEBUG_SHOOTRANGE, "UPDATE_TARGET_SPAWN_MODE_BASIC - lpd.ID: ", lpd.iShooterID)		
			IF GET_RANDOM_TARGET_SIMPLE_ARRAY(iPossibleTargetIndexArray, lpd, iExcludeTargetIndexArray, iExcludeTargetNum, iNewRandomTarget)
				TOGGLE_TARGET_ALIVE(lpd.oTargets[iNewRandomTarget], TRUE)
				CDEBUG1LN(DEBUG_SHOOTRANGE, "UPDATE_TARGET_SPAWN_MODE_BASIC - Spawned a new target. TargetID: ", iNewRandomTarget)
				REINIT_NET_TIMER(TargetRespawnTimer)
			ELSE
				CDEBUG1LN(DEBUG_SHOOTRANGE, "UPDATE_TARGET_SPAWN_MODE_BASIC - was unable to find a new target:")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

#IF IS_DEBUG_BUILD
PROC DEBUG_TARGET_STATE(LOCAL_PLAYER_DATA &lpd)	
	INT iColour
	INT iTarget
	FOR iTarget = lpd.iTargetStartIndex TO lpd.iTargetEndIndex
		IF GET_TARGET_FLAG(lpd.oTargets[iTarget], TF_INITIALIZED)
		AND GET_TARGET_FLAG(lpd.oTargets[iTarget], TF_RANGE_VALID)
			IF GET_TARGET_FLAG(lpd.oTargets[iTarget], TF_ALIVE)
				iColour = 0
			ELSE
				iColour = 1
			ENDIF
			
			IF lpd.bTargetsTakeExtraHits
				IF NOT GET_TARGET_FLAG(lpd.oTargets[iTarget], TF_PAST_MIN_HITABLE_THRESHOLD)
				AND NOT GET_TARGET_FLAG(lpd.oTargets[iTarget], TF_ALIVE)
					iColour = 2
				ENDIF
				
				IF lpd.iMaxNumExtraHits <> -1 
				AND lpd.oTargets[iTarget].iTargetExtraHitCount >= lpd.iMaxNumExtraHits
					iColour = 3
				ENDIF
			ENDIF
			
			IF lpd.fMaxTargetAliveTime <> -1
				IF lpd.oTargets[iTarget].fTargetAliveTime >= lpd.fMaxTargetAliveTime
					iColour = 4
				ENDIF
			ENDIF
			
			SWITCH iColour
				CASE 0
					DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(lpd.oTargets[iTarget].entityTarget), 0.1, 0, 255, 0)
				BREAK
				
				CASE 1
					DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(lpd.oTargets[iTarget].entityTarget), 0.1, 255, 0, 0)
				BREAK
				//Has not reached the min target angle threshold
				CASE 2
					DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(lpd.oTargets[iTarget].entityTarget), 0.1, 255, 255, 0)
				BREAK
				//Target was hit max extra times.
				CASE 3
					DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(lpd.oTargets[iTarget].entityTarget), 0.1, 0, 0, 255)
				BREAK
				//Died due to alive timer
				CASE 4
					DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(lpd.oTargets[iTarget].entityTarget), 0.1, 255, 140, 0)
				BREAK
			ENDSWITCH
		ENDIF
	ENDFOR
ENDPROC
#ENDIF

PROC UPDATE_MODE_BASIC(LOCAL_PLAYER_DATA &lpd)		
	lpd.bPlayerShot = FALSE
	lpd.bPlayerJustShot = FALSE
	
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
	AND NOT IS_PED_INJURED(PLAYER_PED_ID())
	AND IS_PED_SHOOTING(PLAYER_PED_ID())
		lpd.bPlayerJustShot = TRUE
		RESET_NET_TIMER(lpd.stTargetUpdate)
		START_NET_TIMER(lpd.stTargetUpdate)
		CDEBUG1LN(DEBUG_SHOOTRANGE, "UPDATE_MODE_BASIC[TUT] - Player shot starting timer.")
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(lpd.stTargetUpdate)
		IF NOT HAS_NET_TIMER_EXPIRED(lpd.stTargetUpdate, 1000)
			#IF IS_DEBUG_BUILD
			IF GET_FRAME_COUNT() % 5 = 0
				CDEBUG1LN(DEBUG_SHOOTRANGE, "UPDATE_MODE_BASIC[TUT] - lpd.bPlayerShot = TRUE.")
			ENDIF
			#ENDIF
			lpd.bPlayerShot = TRUE
		ELSE
			RESET_NET_TIMER(lpd.stTargetUpdate)
			CDEBUG1LN(DEBUG_SHOOTRANGE, "UPDATE_MODE_BASIC[TUT] - timer stopped, expired.")
		ENDIF
	ENDIF
	
	UPDATE_HIT_SCAN_MODE_BASIC(lpd)
	UPDATE_TARGET_SPAWN_MODE_BASIC(lpd)
	SHOOTING_RANGE_DRAW_FLOATING_SCORES(lpd.floatingScores)
	UPDATE_TARGET_ALIVE_TIME(lpd)
	
	#IF IS_DEBUG_BUILD
	//DEBUG 
	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_T, KEYBOARD_MODIFIER_CTRL, "ShootingRange Target State Debug Toggled")
		lpd.DebugTargetState = NOT lpd.DebugTargetState
	ENDIF
	
	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_S, KEYBOARD_MODIFIER_CTRL, "ShootingRange Target Slow Debug Toggled")
		lpd.DebugTargetSlow = NOT lpd.DebugTargetSlow
		INT iTarget
		FOR iTarget = lpd.iTargetStartIndex TO lpd.iTargetEndIndex
			IF GET_TARGET_FLAG(lpd.oTargets[iTarget], TF_INITIALIZED)
			AND GET_TARGET_FLAG(lpd.oTargets[iTarget], TF_RANGE_VALID)
				lpd.oTargets[iTarget].bDebugSpeed = lpd.DebugTargetSlow
			ENDIF
		ENDFOR
	ENDIF
	
	IF lpd.DebugTargetState
		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
		DEBUG_TARGET_STATE(lpd)
	ENDIF
	#ENDIF
ENDPROC

PROC CLEANUP_TARGETS_ROUND_END_MODE_BASIC(LOCAL_PLAYER_DATA &lpd)
	INT iTarget
	
	REPEAT CI_MAX_TARGETS iTarget		
			IF DOES_ENTITY_EXIST(lpd.oTargets[iTarget].entityTarget)
			//AND DOES_ENTITY_EXIST(lpd.oTargets[iTarget].entityArm)
			AND DOES_ENTITY_EXIST(lpd.oTargets[iTarget].entityBoard)
				REMOVE_DECALS_FROM_OBJECT(GET_OBJECT_INDEX_FROM_ENTITY_INDEX(lpd.oTargets[iTarget].entityTarget))
				//REMOVE_DECALS_FROM_OBJECT(GET_OBJECT_INDEX_FROM_ENTITY_INDEX(lpd.oTargets[iTarget].entityArm))
				REMOVE_DECALS_FROM_OBJECT(GET_OBJECT_INDEX_FROM_ENTITY_INDEX(lpd.oTargets[iTarget].entityBoard))
				TOGGLE_TARGET_ALIVE(lpd.oTargets[iTarget], FALSE, DEFAULT, TRUE)
				TOGGLE_TARGET_FLAG(lpd.oTargets[iTarget], TF_RANGE_VALID, FALSE)
				lpd.oTargets[iTarget].iTargetHitCount = 0
			ENDIF
	ENDREPEAT
	
	REPEAT CI_NUM_FAKE_TARGETS iTarget		
			IF DOES_ENTITY_EXIST(lpd.oFakeTargets[iTarget].entityTarget)
			//AND DOES_ENTITY_EXIST(lpd.oFakeTargets[iTarget].entityArm)
			AND DOES_ENTITY_EXIST(lpd.oFakeTargets[iTarget].entityBoard)
				REMOVE_DECALS_FROM_OBJECT(GET_OBJECT_INDEX_FROM_ENTITY_INDEX(lpd.oFakeTargets[iTarget].entityTarget))
				//REMOVE_DECALS_FROM_OBJECT(GET_OBJECT_INDEX_FROM_ENTITY_INDEX(lpd.oFakeTargets[iTarget].entityArm))
				REMOVE_DECALS_FROM_OBJECT(GET_OBJECT_INDEX_FROM_ENTITY_INDEX(lpd.oFakeTargets[iTarget].entityBoard))
			ENDIF
	ENDREPEAT
	
	lpd.CurrentGameRange = eGR_INVALID
	lpd.eWeapon = WEAPONTYPE_INVALID
	lpd.bTargetDiedThisFrame = FALSE
	lpd.iLastDeadTargetID = CI_NO_TARGET_HIT
	lpd.iTargetsHit = 0
	lpd.iBullseyesHit = 0
	lpd.iTargetScore = 0
	lpd.bPlayerShot = FALSE
	lpd.iCurrentHitCount = 0
	lpd.iMultiplier = 1
	lpd.iMissedCount = 0
ENDPROC

PROC CLEANUP_TARGETS_GAME_END_MODE_BASIC(LOCAL_PLAYER_DATA &lpd)
	INT iTarget
	
	REPEAT CI_MAX_TARGETS iTarget
		IF DOES_ENTITY_EXIST(lpd.oTargets[iTarget].entityTarget)
			DELETE_ENTITY(lpd.oTargets[iTarget].entityTarget)
		ENDIF
		
//		IF DOES_ENTITY_EXIST(lpd.oTargets[iTarget].entityArm)
//			DELETE_ENTITY(lpd.oTargets[iTarget].entityArm)
//		ENDIF
		
		IF DOES_ENTITY_EXIST(lpd.oTargets[iTarget].entityBoard)
			DELETE_ENTITY(lpd.oTargets[iTarget].entityBoard)
		ENDIF
		
		TOGGLE_TARGET_FLAG(lpd.oTargets[iTarget], TF_ALIVE, FALSE)
		
	ENDREPEAT
	
	//Destroy fake targets lane 5
	REPEAT CI_NUM_FAKE_TARGETS iTarget
		IF DOES_ENTITY_EXIST(lpd.oFakeTargets[iTarget].entityTarget)
			DELETE_ENTITY(lpd.oFakeTargets[iTarget].entityTarget)
		ENDIF
		
//		IF DOES_ENTITY_EXIST(lpd.oFakeTargets[iTarget].entityArm)
//			DELETE_ENTITY(lpd.oFakeTargets[iTarget].entityArm)
//		ENDIF
		
		IF DOES_ENTITY_EXIST(lpd.oFakeTargets[iTarget].entityBoard)
			DELETE_ENTITY(lpd.oFakeTargets[iTarget].entityBoard)
		ENDIF
	ENDREPEAT
	
	INT iShooterID
	REPEAT CI_MAX_PLAYERS_MODE_BASIC iShooterID
		lpd.iPlayerTargets[iShooterID] = 0
	ENDREPEAT
	
	lpd.iNumActiveTargets = 0
	lpd.iFakeTargetsNum = 0
	
	lpd.bTargetsSpawned = FALSE
ENDPROC

#IF IS_DEBUG_BUILD
PROC SETUP_TARGET_DEBUG(LOCAL_PLAYER_DATA &lpd)
	TEXT_LABEL_31 txName, txID
	INT iTarget
	
	BOOL RightDir = TRUE
	
	VECTOR vNewOffsetPos = <<0.0, 0.0, 0.0>>
	VECTOR vOffsetLeft, vOffsetLeftMiddle, vOffsetMiddle, vOffsetMiddleRight, vOffsetRight
		vOffsetLeft = <<0.0, -0.320, 0.0>>
		vOffsetLeftMiddle = <<0.0, -0.160, 0.0>>
		vOffsetMiddle = <<0.0, 0.0, 0.0>>
		vOffsetMiddleRight = <<0.0, 0.160, 0.0>>
		vOffsetRight = <<0.0, 0.320, 0.0>>
	
	INT iPosition = 0
		
	REPEAT CI_MAX_TARGETS iTarget
		vNewOffsetPos = <<0.0, 0.0, 0.0>>
		
		txName = "TargetID: "
		txID = iTarget
		txName += txID
		txName += " Offset: "
		
		
		IF iPosition = 0
			vNewOffsetPos += vOffsetLeft
		ELIF iPosition = 1
			vNewOffsetPos += vOffsetLeftMiddle
		ELIF iPosition = 2
			vNewOffsetPos += vOffsetMiddle
		ELIF iPosition = 3
			vNewOffsetPos += vOffsetMiddleRight
		ELIF iPosition = 4
			vNewOffsetPos += vOffsetRight
		ENDIF
		
		IF iPosition >= 4
		AND RightDir
			RightDir = FALSE
		ENDIF
		
		IF iPosition <= 0
		AND NOT RightDir
			RightDir = TRUE
		ENDIF
		
		IF RightDir
			IF iPosition < 4
				iPosition++
			ENDIF
		ELSE
			IF iPosition > 0
				iPosition--
			ENDIF
		ENDIF
		
		SWITCH lpd.oTargets[iTarget].eTargetType
			CASE eTARGETTYPE_HUMAN_TOP
				vNewOffsetPos += <<0.0, 0.0, 2.800>>
			BREAK
			CASE eTARGETTYPE_HUMAN_BOTTOM
				vNewOffsetPos += <<0.0, 0.0, 0.0>>
			BREAK
		ENDSWITCH
		
		FLOAT fXOffset
		
		IF iTarget % 2 = 0
			fXOffset = 0.300
			fXOffset += iTarget / 2 * 3.0
			vNewOffsetPos.x = fXOffset
			lpd.oTargets[iTarget].vDebugPosition = vNewOffsetPos
			CDEBUG1LN(DEBUG_SHOOTRANGE, "iTarget % 2 = 0: ", iTarget, " fXO: ", fXOffset, " vNewOffset: ", vNewOffsetPos)
		ELSE
			fXOffset = 2.100
			IF iTarget != 1
			fXOffset += iTarget / 2 * 3.0
			ENDIF
			vNewOffsetPos.x = fXOffset
			vNewOffsetPos.z = 2.700
			lpd.oTargets[iTarget].vDebugPosition = vNewOffsetPos
			CDEBUG1LN(DEBUG_SHOOTRANGE, "iTarget % UNEVEN: ", iTarget, " fXO: ", fXOffset, " vNewOffset: ", vNewOffsetPos)
		ENDIF
		
		ADD_WIDGET_VECTOR_SLIDER(txName, lpd.oTargets[iTarget].vDebugPosition, -99999, 99999, 0.1)
	ENDREPEAT
	
	//DEBUG_TARGET_HIT_POINTS_WIDGET(lpd.DebugTargetHitPoints)
ENDPROC

PROC UPDATE_TARGET_DEBUG(LOCAL_PLAYER_DATA &lpd)
	INT iTarget
	VECTOR vForward, vRight, vUp, vNewPosition
	
	REPEAT CI_MAX_TARGETS iTarget
		IF DOES_ENTITY_EXIST(lpd.oTargets[iTarget].entityBoard)
		AND DOES_ENTITY_EXIST(lpd.oTargets[iTarget].entityTarget)
		//AND DOES_ENTITY_EXIST(lpd.oTargets[iTarget].entityArm)
			
			
			vForward = GET_ENTITY_FORWARD_VECTOR(lpd.oTargets[iTarget].entityBoard)
			vUp = <<0.0, 0.0, 1.0>>
			vRight = CROSS_PRODUCT(vForward, vUp)
			vForward = CROSS_PRODUCT(vUp, vRight)
			
			//INT iTargetForPlayer
//			IF iTarget < CI_MAX_TARGET_PER_PLAYER_BASIC
			//	iTargetForPlayer = CI_PLAYER_ONE
//			ELIF iTarget > CI_MAX_TARGET_PER_PLAYER_BASIC
//			AND iTarget <= CI_MAX_TARGET_PER_PLAYER_BASIC * 2
//				iTargetForPlayer = CI_PLAYER_TWO
//			ELIF iTarget > CI_MAX_TARGET_PER_PLAYER_BASIC * 2
//			AND iTarget <= CI_MAX_TARGET_PER_PLAYER_BASIC * 3
//				iTargetForPlayer = CI_PLAYER_THREE
//			ELIF iTarget > CI_MAX_TARGET_PER_PLAYER_BASIC * 3
//			AND iTarget <= CI_MAX_TARGET_PER_PLAYER_BASIC * 4
//				iTargetForPlayer = CI_PLAYER_FOUR
//			ENDIF
			
			
			vNewPosition = GET_TARGET_RANGE_CENTER(lpd, CI_PLAYER_ONE)
			vNewPosition = vNewPosition + (vForward * lpd.oTargets[iTarget].vDebugPosition.x + vRight * lpd.oTargets[iTarget].vDebugPosition.y + vUp * lpd.oTargets[iTarget].vDebugPosition.z)
			SET_ENTITY_COORDS(lpd.oTargets[iTarget].entityBoard, vNewPosition)
			
			SWITCH lpd.oTargets[iTarget].eTargetType			
				CASE eTARGETTYPE_HUMAN_TOP
					SET_ENTITY_COORDS(lpd.oTargets[iTarget].entityTarget, vNewPosition + <<0.0, 0.178, -0.120>>)
				BREAK
				
				CASE eTARGETTYPE_HUMAN_BOTTOM
					SET_ENTITY_COORDS(lpd.oTargets[iTarget].entityTarget, vNewPosition + <<0.0, 0.178, 0.075>>)
				BREAK
			ENDSWITCH
		ENDIF
	ENDREPEAT
	
	//DEBUG_TARGET_HIT_POINTS_UPDATE(lpd.DebugTargetHitPoints)
ENDPROC

#ENDIF
