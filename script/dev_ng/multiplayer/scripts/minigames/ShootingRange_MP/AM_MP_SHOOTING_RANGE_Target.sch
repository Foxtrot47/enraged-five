USING "am_mp_shooting_range_helper.sch"
USING "model_enums.sch"
USING "commands_entity.sch"
USING "commands_misc.sch"
USING "script_maths.sch"
USING "commands_graphics.sch"
USING "commands_streaming.sch"
USING "am_mp_shooting_range_core.sch"
USING "fmmc_camera.sch"

//HIT LOCATION STUFF
CONST_INT CI_HIT_LOCATION_HUMAN_TARGET_HEAD	 0
CONST_INT CI_HIT_LOCATION_HUMAN_TARGET_CHEST 1
CONST_INT CI_HIT_LOCATION_HUMAN_TARGET_HEAD_OVERALL	 2

ENUM TARGET_HIT_LOCATION_ENUM
	THL_NONE,
	THL_HEAD, 
	THL_HEAD_BULLSEYE, 
	THL_BODY,
	THL_BODY_BULLSEYE
ENDENUM
ENUM TARGET_HIT_LOCATION_SHAPE_TYPE
	TARGET_HIT_LOCATION_SHAPE_INVALID,
	TARGET_HIT_LOCATION_SHAPE_CIRCLE,
	TARGET_HIT_LOCATION_SHAPE_RECTANGLE
ENDENUM

STRUCT TARGET_HIT_LOCATION_SHAPE
	TARGET_HIT_LOCATION_SHAPE_TYPE eShapeType = TARGET_HIT_LOCATION_SHAPE_INVALID
	VECTOR size //X as radius for circle
	VECTOR offset // Offset from the HitLocationCenter.
ENDSTRUCT

CONST_INT CI_MAX_TARGET_HIT_LOCATONS 3
CONST_INT CI_MAX_TARGET_HIT_LOCATION_SHAPES 5

STRUCT HIT_LOCATION_STRUCT
	INT	iHitPoint = 0
	VECTOR offset // offset from model pivot.
	TARGET_HIT_LOCATION_SHAPE sShape[CI_MAX_TARGET_HIT_LOCATION_SHAPES]
	TARGET_HIT_LOCATION_ENUM eHitLocation = THL_NONE
ENDSTRUCT

FUNC BOOL TARGET_IS_POINT_IN_HIT_LOCATION(HIT_LOCATION_STRUCT sHitLocation, VECTOR vWeaponHitLocation)
	sHitLocation = sHitLocation
	vWeaponHitLocation = vWeaponHitLocation
//	SWITCH sHitLocation.sShape.eShapeType
//		CASE TARGET_HIT_LOCATION_SHAPE_CIRCLE
//			IF GET_DISTANCE_BETWEEN_COORDS(sHitLocation.center, vWeaponHitLocation, TRUE) < sHitLocations[iHitLocation].maxSize
//				RETURN TRUE
//			ENDIF
//		BREAK
//		
//		CASE TARGET_HIT_LOCATION_SHAPE_RECTANGLE
//		
//		BREAK
//	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC STRING TARGET_GET_HIT_LOCATION_NAME(TARGET_HIT_LOCATION_ENUM eHitLocation)
	SWITCH eHitLocation
		CASE THL_NONE RETURN "THL_NONE"
		CASE THL_HEAD RETURN "THL_HEAD"
		CASE THL_HEAD_BULLSEYE RETURN "THL_HEAD_BULLSEYE"
		CASE THL_BODY RETURN "THL_BODY"
		CASE THL_BODY_BULLSEYE RETURN "THL_BODY_BULLSEYE"
	ENDSWITCH
	
	RETURN "TARGET_GET_HIT_LOCATION_NAME - undefined hit location"
ENDFUNC

ENUM TARGET_FLAG_ENUM
	TF_FLIP,
	TF_ALIVE, 
	TF_INITIALIZED,
	TF_RANGE_VALID,
	TF_PAST_MIN_HITABLE_THRESHOLD
ENDENUM

ENUM TARGET_TYPES_ENUM
	eTARGETTYPE_ROUND_PLATE,
	eTARGETTYPE_FRAG_BAORD,
	eTARGETTYPE_FRAG_BEER_BOTTLE,
	eTARGETTYPE_HUMAN_TOP,
	eTARGETTYPE_HUMAN_BOTTOM,
	eTARGETTYPE_INVALID
ENDENUM

#IF IS_DEBUG_BUILD
STRUCT TARGET_HIT_POINT_DEBUG
	CAMERA_INDEX		camera
	BOOL				DrawWholeHitPoint
	BOOL				bTargetHitPointDebug
	TARGET_TYPES_ENUM 	eTargetType
	ENTITY_INDEX		entityTarget
	VECTOR 				targePosition
	VECTOR				targetRotation
	
	VECTOR				vHitLocationOffset
	VECTOR				vHitLocationPointOffset
	VECTOR				vHitLocationPointSize
	INT					iShape
	
	INT					iCurrentHitLocation
	INT					iPrevHitLocation
	INT					iCurrentHitLocationPoint
	INT					iPrevHitLocationPoint
	HIT_LOCATION_STRUCT sHitLocationArray[CI_MAX_TARGET_HIT_LOCATONS]
ENDSTRUCT
#ENDIF

STRUCT TARGET_STRUCT
	TARGET_TYPES_ENUM 	eTargetType
	ENTITY_INDEX 		entityTarget
	//ENTITY_INDEX		entityArm
	ENTITY_INDEX		entityBoard
	INT					iTargetID = 0
	INT					iTargetFlag = 0
	FLOAT				fTargetFlipSpeed = 0.0
	FLOAT 				fTargetRotationBias = 0.0
	FLOAT				fAliveRoll = 0.0
	FLOAT				fDeadRoll = 0.0
	//target availible for ranges X
	INT 				iTargetRangeBitSet = 0
	INT					iTargetHitCount = 0
	INT					iTargetExtraHitCount = 0
	#IF IS_DEBUG_BUILD
	VECTOR				vDebugPosition
	BOOL				bDebugSpeed
	#ENDIF
	// min ration of target rotation before which the target will not be registred as a valid hittable target. REF:url:bugstar:3536759 
	// 1.0 - alive / 0.0 - dead.
	FLOAT				fMinRatioDead = 0.2
	//Timer
	FLOAT				fTargetAliveTime = 0.0
	//Hit Locations, currently max 2 hit points, 5 shapes for each max
	//HIT_LOCATION_STRUCT sHitLocationArray[CI_MAX_TARGET_HIT_LOCATONS]
ENDSTRUCT

FUNC MODEL_NAMES TARET_GET_MODEL_FROM_TYPE(TARGET_TYPES_ENUM eTargetType)
	SWITCH eTargetType
		CASE eTARGETTYPE_HUMAN_TOP	
		CASE eTARGETTYPE_HUMAN_BOTTOM
		RETURN INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_target_02b"))
	ENDSWITCH
	
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

/// PURPOSE:
///    Define target models as enum, used for _GET_SCORE_FROM_TARGET_ to calc score
/// PARAMS:
///    model - model of the target hit.
/// RETURNS:
///    
FUNC TARGET_TYPES_ENUM GET_TARGET_TYPE_FROM_MODEL(MODEL_NAMES model)
	SWITCH model
		CASE PROP_TARGET_ORA_PURP_01 RETURN eTARGETTYPE_ROUND_PLATE
		CASE prop_target_frag_board RETURN eTARGETTYPE_FRAG_BAORD
		CASE PROP_BEER_BOTTLE RETURN eTARGETTYPE_FRAG_BEER_BOTTLE
	ENDSWITCH
	
	RETURN eTARGETTYPE_INVALID
ENDFUNC


FUNC BOOL TARGET_HAS_HIT_LOCATIONS(TARGET_TYPES_ENUM eTargetType)
	SWITCH eTargetType
		CASE eTARGETTYPE_HUMAN_BOTTOM
		CASE eTARGETTYPE_HUMAN_TOP
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Get center of hit lcoations for a specific target.
/// PARAMS:
///    eTargetType - 
///    vHitLocations - 
/// RETURNS:
///    
PROC TARGET_GET_HIT_LOCATIONS(TARGET_TYPES_ENUM eTargetType, HIT_LOCATION_STRUCT &sHitLocations[])
	SWITCH eTargetType
		CASE eTARGETTYPE_HUMAN_TOP
			//HEAD
			sHitLocations[CI_HIT_LOCATION_HUMAN_TARGET_HEAD].offset = <<0.0, 0.0, -0.205>>
			sHitLocations[CI_HIT_LOCATION_HUMAN_TARGET_HEAD].iHitPoint = 15
			sHitLocations[CI_HIT_LOCATION_HUMAN_TARGET_HEAD].eHitLocation = THL_HEAD_BULLSEYE
			sHitLocations[CI_HIT_LOCATION_HUMAN_TARGET_HEAD].sShape[0].eShapeType = TARGET_HIT_LOCATION_SHAPE_CIRCLE
			sHitLocations[CI_HIT_LOCATION_HUMAN_TARGET_HEAD].sShape[0].size = <<0.0, 0.0, 0.052>>
			sHitLocations[CI_HIT_LOCATION_HUMAN_TARGET_HEAD].sShape[0].offset = <<0.0, 0.0, 0.0>>
			
			//CHEST
			sHitLocations[CI_HIT_LOCATION_HUMAN_TARGET_CHEST].offset = <<0.0, 0.0, -0.723>>
			sHitLocations[CI_HIT_LOCATION_HUMAN_TARGET_CHEST].iHitPoint = 10
			sHitLocations[CI_HIT_LOCATION_HUMAN_TARGET_CHEST].eHitLocation = THL_BODY_BULLSEYE
			//1
			sHitLocations[CI_HIT_LOCATION_HUMAN_TARGET_CHEST].sShape[0].eShapeType = TARGET_HIT_LOCATION_SHAPE_CIRCLE
			sHitLocations[CI_HIT_LOCATION_HUMAN_TARGET_CHEST].sShape[0].size = <<0.0, 0.0, 0.067>>
			sHitLocations[CI_HIT_LOCATION_HUMAN_TARGET_CHEST].sShape[0].offset = <<0.0, 0.0, 0.020>>
			//2
			sHitLocations[CI_HIT_LOCATION_HUMAN_TARGET_CHEST].sShape[1].eShapeType = TARGET_HIT_LOCATION_SHAPE_CIRCLE
			sHitLocations[CI_HIT_LOCATION_HUMAN_TARGET_CHEST].sShape[1].size = <<0.0, 0.0, 0.067>>
			sHitLocations[CI_HIT_LOCATION_HUMAN_TARGET_CHEST].sShape[1].offset = <<0.0, 0.0, -0.028>>
			
			//Head Not bullseye
			sHitLocations[CI_HIT_LOCATION_HUMAN_TARGET_HEAD_OVERALL].offset = <<0.0, 0.0, -0.205>>
			sHitLocations[CI_HIT_LOCATION_HUMAN_TARGET_HEAD_OVERALL].iHitPoint = 5
			sHitLocations[CI_HIT_LOCATION_HUMAN_TARGET_HEAD_OVERALL].eHitLocation = THL_HEAD
			sHitLocations[CI_HIT_LOCATION_HUMAN_TARGET_HEAD_OVERALL].sShape[0].eShapeType = TARGET_HIT_LOCATION_SHAPE_CIRCLE
			sHitLocations[CI_HIT_LOCATION_HUMAN_TARGET_HEAD_OVERALL].sShape[0].size = <<0.0, 0.0, 0.170>>
			sHitLocations[CI_HIT_LOCATION_HUMAN_TARGET_HEAD_OVERALL].sShape[0].offset = <<0.0, 0.0, 0.0>>
		BREAK
		
		CASE eTARGETTYPE_HUMAN_BOTTOM
			//HEAD
			sHitLocations[CI_HIT_LOCATION_HUMAN_TARGET_HEAD].offset = <<0.005, 0.0, 1.328>>
			sHitLocations[CI_HIT_LOCATION_HUMAN_TARGET_HEAD].iHitPoint = 15
			sHitLocations[CI_HIT_LOCATION_HUMAN_TARGET_HEAD].eHitLocation = THL_HEAD_BULLSEYE
			sHitLocations[CI_HIT_LOCATION_HUMAN_TARGET_HEAD].sShape[0].eShapeType = TARGET_HIT_LOCATION_SHAPE_CIRCLE
			sHitLocations[CI_HIT_LOCATION_HUMAN_TARGET_HEAD].sShape[0].size = <<0.0, 0.0, 0.050>>
			sHitLocations[CI_HIT_LOCATION_HUMAN_TARGET_HEAD].sShape[0].offset = <<0.0, 0.0, 0.0>>
			
			//CHEST
			sHitLocations[CI_HIT_LOCATION_HUMAN_TARGET_CHEST].offset = <<0.003, 0.0, 0.805>>
			sHitLocations[CI_HIT_LOCATION_HUMAN_TARGET_CHEST].iHitPoint = 10
			sHitLocations[CI_HIT_LOCATION_HUMAN_TARGET_CHEST].eHitLocation = THL_BODY_BULLSEYE
			//1
			sHitLocations[CI_HIT_LOCATION_HUMAN_TARGET_CHEST].sShape[0].eShapeType = TARGET_HIT_LOCATION_SHAPE_CIRCLE
			sHitLocations[CI_HIT_LOCATION_HUMAN_TARGET_CHEST].sShape[0].size = <<0.000, 0.0, 0.065>>
			sHitLocations[CI_HIT_LOCATION_HUMAN_TARGET_CHEST].sShape[0].offset = <<0.0, 0.0, 0.022>>
			//2
			sHitLocations[CI_HIT_LOCATION_HUMAN_TARGET_CHEST].sShape[1].eShapeType = TARGET_HIT_LOCATION_SHAPE_CIRCLE
			sHitLocations[CI_HIT_LOCATION_HUMAN_TARGET_CHEST].sShape[1].size = <<0.000, 0.0, 0.065>>
			sHitLocations[CI_HIT_LOCATION_HUMAN_TARGET_CHEST].sShape[1].offset = <<0.0, 0.0, -0.028>>
			
			//Head Not bullseye
			sHitLocations[CI_HIT_LOCATION_HUMAN_TARGET_HEAD_OVERALL].offset = <<0.005, 0.0, 1.300>>
			sHitLocations[CI_HIT_LOCATION_HUMAN_TARGET_HEAD_OVERALL].iHitPoint = 5
			sHitLocations[CI_HIT_LOCATION_HUMAN_TARGET_HEAD_OVERALL].eHitLocation = THL_HEAD
			sHitLocations[CI_HIT_LOCATION_HUMAN_TARGET_HEAD_OVERALL].sShape[0].eShapeType = TARGET_HIT_LOCATION_SHAPE_CIRCLE
			sHitLocations[CI_HIT_LOCATION_HUMAN_TARGET_HEAD_OVERALL].sShape[0].size = <<0.0, 0.0, 0.150>>
			sHitLocations[CI_HIT_LOCATION_HUMAN_TARGET_HEAD_OVERALL].sShape[0].offset = <<0.0, 0.0, 0.0>>
		BREAK
	ENDSWITCH
ENDPROC

FUNC INT TARGET_GET_HIT_LOCATION_SCORE(TARGET_STRUCT &sTarget, VECTOR vWeaponHitLocationLocal, TARGET_HIT_LOCATION_ENUM &returnHitLocation, BOOL &bValidHit)
	IF NOT DOES_ENTITY_EXIST(sTarget.entityTarget)
		ASSERTLN("TARGET_GET_HIT_LOCATION_SCORE - target does not exist")
		bValidHit = FALSE
		RETURN 0
	ENDIF
	
	//Rotate the hitpoint back 
	OBJECT_INDEX tempHitPoint
	tempHitPoint = CREATE_OBJECT_NO_OFFSET(prop_golf_ball, <<10.0, 10.0, 0.0>>, FALSE, FALSE, FALSE)
	SET_ENTITY_COLLISION(tempHitPoint, FALSE)
	SET_ENTITY_VISIBLE(tempHitPoint, FALSE)
	
	//Target specific, hit bounds.
	bValidHit = TRUE
	VECTOR vTargetPos
	vTargetPos = GET_ENTITY_COORDS(sTarget.entityTarget)
	SWITCH sTarget.eTargetType
		CASE eTARGETTYPE_HUMAN_BOTTOM
			IF ABSF(vTargetPos.z - vWeaponHitLocationLocal.z) < 0.440
				CDEBUG1LN(DEBUG_SHOOTRANGE, "TARGET_GET_HIT_LOCATION_SCORE[SPECIFICEXCLUDE] - eTARGETTYPE_HUMAN_BOTTOM excluded due to height: ", vWeaponHitLocationLocal)
				bValidHit = FALSE
				RETURN 0
			ENDIF
		BREAK
		
		CASE eTARGETTYPE_HUMAN_TOP
			CDEBUG1LN(DEBUG_SHOOTRANGE, "TARGET_GET_HIT_LOCATION_SCORE[SPECIFICEXCLUDE] - 22hit arm of the top target, vHitLocationLocal: ", vWeaponHitLocationLocal)
			IF ABSF(vTargetPos.z - vWeaponHitLocationLocal.z) < 0.440
				CDEBUG1LN(DEBUG_SHOOTRANGE, "TARGET_GET_HIT_LOCATION_SCORE[SPECIFICEXCLUDE] - hit on level of top exclude: ", vWeaponHitLocationLocal)
				INT iOutOfExclusion
				
				ATTACH_ENTITY_TO_ENTITY(tempHitPoint, sTarget.entityTarget, -1, <<0.0, 0.0, -0.250>>, <<0.0, 0.0, 0.0>>)
				
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(tempHitPoint), vWeaponHitLocationLocal, TRUE) >= 0.130
					CDEBUG1LN(DEBUG_SHOOTRANGE, "TARGET_GET_HIT_LOCATION_SCORE[SPECIFICEXCLUDE] - Head circle distance, exceeded: ", vWeaponHitLocationLocal)
					iOutOfExclusion++
				ENDIF
				
				IF IS_ENTITY_ATTACHED(tempHitPoint)
					CDEBUG1LN(DEBUG_SHOOTRANGE, "TARGET_GET_HIT_LOCATION_SCORE[SPECIFICEXCLUDE] - 1detached")
					DETACH_ENTITY(tempHitPoint)
				ENDIF
				
				ATTACH_ENTITY_TO_ENTITY(tempHitPoint, sTarget.entityTarget, -1, <<0.0, 0.0, -0.410>>, <<0.0, 0.0, 0.0>>)
				
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(tempHitPoint), vWeaponHitLocationLocal, TRUE) >= 0.140
					CDEBUG1LN(DEBUG_SHOOTRANGE, "TARGET_GET_HIT_LOCATION_SCORE[SPECIFICEXCLUDE] - Head circle distance, exceeded: ", vWeaponHitLocationLocal)
					iOutOfExclusion++
				ENDIF
				
				IF iOutOfExclusion = 2
					IF DOES_ENTITY_EXIST(tempHitPoint)
						DELETE_OBJECT(tempHitPoint)
					ENDIF
					bValidHit = FALSE
					RETURN 0
				ELSE
					CDEBUG1LN(DEBUG_SHOOTRANGE, "TARGET_GET_HIT_LOCATION_SCORE[SPECIFICEXCLUDE] - iOutOfExclusion: ", iOutOfExclusion)	
				ENDIF
				
				IF IS_ENTITY_ATTACHED(tempHitPoint)
					CDEBUG1LN(DEBUG_SHOOTRANGE, "TARGET_GET_HIT_LOCATION_SCORE[SPECIFICEXCLUDE] - 2detached")
					DETACH_ENTITY(tempHitPoint)
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	

	HIT_LOCATION_STRUCT sHitLocations[3]
	TARGET_GET_HIT_LOCATIONS(sTarget.eTargetType, sHitLocations)
	
	INT iHitLocation
	INT iHitLocationPoint
	REPEAT COUNT_OF(sHitLocations) iHitLocation
		REPEAT CI_MAX_TARGET_HIT_LOCATION_SHAPES iHitLocationPoint
			SWITCH sHitLocations[iHitLocation].sShape[iHitLocationPoint].eShapeType
				CASE TARGET_HIT_LOCATION_SHAPE_CIRCLE
					VECTOR vHitPoint
					vHitPoint = sHitLocations[iHitLocation].offset + sHitLocations[iHitLocation].sShape[iHitLocationPoint].offset
					
					ATTACH_ENTITY_TO_ENTITY(tempHitPoint, sTarget.entityTarget, -1, vHitPoint, <<0.0, 0.0, 0.0>>)
					
					IF (GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(tempHitPoint), vWeaponHitLocationLocal, TRUE) < sHitLocations[iHitLocation].sShape[iHitLocationPoint].size.z)
						CDEBUG1LN(DEBUG_SHOOTRANGE, "TARGET_GET_HIT_LOCATION_SCORE - HITLOCATION: ", iHitLocationPoint, " HITPOINT: ", iHitLocationPoint, " HitPoint: ", sHitLocations[iHitLocation].iHitPoint,  " HIT!!!!!!!: ", vHitPoint)
						IF DOES_ENTITY_EXIST(tempHitPoint)
							DELETE_OBJECT(tempHitPoint)
						ENDIF
						
						returnHitLocation = sHitLocations[iHitLocation].eHitLocation
						RETURN sHitLocations[iHitLocation].iHitPoint
					ELSE
						CDEBUG1LN(DEBUG_SHOOTRANGE, "TARGET_GET_HIT_LOCATION_SCORE - HITLOCATION: ", iHitLocationPoint, " HITPOINT: ", iHitLocationPoint, " NOT HIT: ", vHitPoint)
					ENDIF
				BREAK
				
				CASE TARGET_HIT_LOCATION_SHAPE_RECTANGLE
					
				BREAK
			ENDSWITCH
			
		ENDREPEAT
	ENDREPEAT
	
	IF DOES_ENTITY_EXIST(tempHitPoint)
		DELETE_OBJECT(tempHitPoint)
	ENDIF
	//If target was hit , but not at hit location
	returnHitLocation = THL_BODY
	RETURN 5
ENDFUNC

PROC TARGET_PLAY_HIT_SOUND(TARGET_STRUCT &sTarget, TARGET_HIT_LOCATION_ENUM hitLocation)
	CDEBUG1LN(DEBUG_SHOOTRANGE, "Â£Â£Â£Â£Â£ - TARGET_PLAY_HIT_SOUND - TargetType: ",  ENUM_TO_INT(sTarget.eTargetType), " Sounds: ", TARGET_GET_HIT_LOCATION_NAME(hitLocation))
	IF NOT DOES_ENTITY_EXIST(sTarget.entityTarget)
		CDEBUG1LN(DEBUG_SHOOTRANGE, "Â£Â£Â£Â£Â£ - TARGET_PLAY_HIT_SOUND - can not play sound target does not exist")
		EXIT
	ENDIF
	
	SWITCH sTarget.eTargetType
		CASE eTARGETTYPE_HUMAN_BOTTOM
		CASE eTARGETTYPE_HUMAN_TOP
			SWITCH hitLocation
				CASE THL_HEAD
					CDEBUG1LN(DEBUG_SHOOTRANGE, "Â£Â£Â£Â£Â£ - head shot")
					PLAY_SOUND_FROM_ENTITY(-1,"Target_Hit_Head_Black", sTarget.entityTarget, "DLC_GR_Bunker_Shooting_Range_Sounds")
				BREAK
				
				CASE THL_HEAD_BULLSEYE
					PLAY_SOUND_FROM_ENTITY(-1,"Target_Hit_Head_Red", sTarget.entityTarget, "DLC_GR_Bunker_Shooting_Range_Sounds")
				BREAK
				
				CASE THL_BODY
					CDEBUG1LN(DEBUG_SHOOTRANGE, "Â£Â£Â£Â£Â£ - body shot")
					PLAY_SOUND_FROM_ENTITY(-1,"Target_Hit_Body_Black", sTarget.entityTarget, "DLC_GR_Bunker_Shooting_Range_Sounds")
				BREAK
				
				CASE THL_BODY_BULLSEYE
					PLAY_SOUND_FROM_ENTITY(-1,"Target_Hit_Body_Red", sTarget.entityTarget, "DLC_GR_Bunker_Shooting_Range_Sounds")
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Calcualte score based on target type and hit.
/// PARAMS:
///    eTargetType - GET_TARGET_TYPE_FROM_MODEL
///    fDistanceFromCenter - 
/// RETURNS:
///    
FUNC INT GET_SCORE_FROM_TARGET(WEAPON_TYPE &withWeapon, BOOL HitHeadshot)
	
	SWITCH withWeapon
		CASE WEAPONTYPE_PUMPSHOTGUN
		CASE WEAPONTYPE_ASSAULTSHOTGUN
			RETURN 10
	ENDSWITCH
	
	//Invalid target
	IF HitHeadshot
		RETURN 10
	ENDIF
	
	RETURN 5
ENDFUNC

FUNC INT GET_MIN_TARGET_HIT_FOR_WEAPON(WEAPON_TYPE eWeapon)
	SWITCH eWeapon
		CASE WEAPONTYPE_PISTOL 			RETURN 1
		CASE WEAPONTYPE_SMG 			RETURN 3
		CASE WEAPONTYPE_ASSAULTRIFLE 	RETURN 5
		CASE WEAPONTYPE_CARBINERIFLE	RETURN 5
		CASE WEAPONTYPE_COMBATMG		RETURN 10
		CASE WEAPONTYPE_HEAVYSNIPER		RETURN 1
	ENDSWITCH
	
	RETURN 1
ENDFUNC

FUNC BOOL GET_TARGET_FLAG(TARGET_STRUCT sTarget, TARGET_FLAG_ENUM eFlag)
	RETURN IS_BIT_SET_ENUM(sTarget.iTargetFlag, eFlag)
ENDFUNC

PROC TOGGLE_TARGET_FLAG(TARGET_STRUCT & sTarget, TARGET_FLAG_ENUM eFlag, BOOL bTrue = TRUE)
	IF bTrue
		SET_BIT_ENUM(sTarget.iTargetFlag, eFlag)
	ELSE
		CLEAR_BIT_ENUM(sTarget.iTargetFlag, eFlag)
	ENDIF
ENDPROC

PROC SET_TARGET_RANGE(TARGET_STRUCT& sTarget, GAME_RANGE eForRange = eGR_INVALID)
	sTarget.iTargetRangeBitSet = 0
	SET_BITMASK_AS_ENUM(sTarget.iTargetRangeBitSet, eForRange)
ENDPROC

ENUM TARGET_CONSMETICS_ENUM
	eTARGET_COSMETICS_DEFAULT,
	eTARGET_COSMETICS_BLACK_OLD,
	eTARGET_COSMETICS_BLACK_NEW,
	eTARGET_COSMETICS_WHITE_OLD,
	eTARGET_COSMETICS_WHITE_NEW
ENDENUM

STRUCT TARGET_MODELS_STRUCT
	MODEL_NAMES	target
	MODEL_NAMES base
	MODEL_NAMES arm
ENDSTRUCT

FUNC TARGET_MODELS_STRUCT TARGET_GET_MODEL_BASED_ON_TYPE(TARGET_TYPES_ENUM eTargetType, TARGET_CONSMETICS_ENUM eCosmetics)
	TARGET_MODELS_STRUCT sOutModels
	
	SWITCH eTargetType
		CASE eTARGETTYPE_HUMAN_TOP
			SWITCH eCosmetics
				CASE eTARGET_COSMETICS_BLACK_OLD
					sOutModels.target = INT_TO_ENUM(MODEL_NAMES, HASH("gr_Prop_GR_Target_04c"))
					sOutModels.base = INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_target_01a"))
					//sOutModels.arm = INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_target_03a"))
				BREAK
				
				CASE eTARGET_COSMETICS_BLACK_NEW
				CASE eTARGET_COSMETICS_DEFAULT
					sOutModels.target = INT_TO_ENUM(MODEL_NAMES, HASH("gr_Prop_GR_Target_05c"))
					sOutModels.base = INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_target_01b"))
					//sOutModels.arm = INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_target_03b"))
				BREAK
				
				CASE eTARGET_COSMETICS_WHITE_OLD
					sOutModels.target = INT_TO_ENUM(MODEL_NAMES, HASH("gr_Prop_GR_Target_04d"))
					sOutModels.base = INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_target_01a"))
					//sOutModels.arm = INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_target_03a"))
				BREAK
				
				CASE eTARGET_COSMETICS_WHITE_NEW
					sOutModels.target = INT_TO_ENUM(MODEL_NAMES, HASH("gr_Prop_GR_Target_05d"))
					sOutModels.base = INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_target_01b"))
					//sOutModels.arm = INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_target_03b"))
				BREAK
			ENDSWITCH

		BREAK
		CASE eTARGETTYPE_HUMAN_BOTTOM
			SWITCH eCosmetics
				CASE eTARGET_COSMETICS_BLACK_OLD
					sOutModels.target = INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_target_04a"))
					sOutModels.base = INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_target_01a"))
					//sOutModels.arm = INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_target_03a"))
				BREAK
				
				CASE eTARGET_COSMETICS_BLACK_NEW
				CASE eTARGET_COSMETICS_DEFAULT
					sOutModels.target = INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_target_05a"))
					sOutModels.base = INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_target_01b"))
					//sOutModels.arm = INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_target_03b"))
				BREAK
				
				CASE eTARGET_COSMETICS_WHITE_OLD
					sOutModels.target = INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_target_04b"))
					sOutModels.base = INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_target_01a"))
					//sOutModels.arm = INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_target_03a"))
				BREAK
				
				CASE eTARGET_COSMETICS_WHITE_NEW
					sOutModels.target = INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_target_05b"))
					sOutModels.base = INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_target_01b"))
					//sOutModels.arm = INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_target_03b"))
				BREAK
			ENDSWITCH

		BREAK
	ENDSWITCH
	
	RETURN sOutModels
ENDFUNC

/// PURPOSE:
///    
/// PARAMS:
///    sTarget - 
///    eTargetType - 
///    vPosition - 
///    vRotAlive - heading/pitch/roll
///    vRotDead -  heading/pitch/roll
///    iLODdist - 
///    fTargetFlipSpeed - 
FUNC BOOL CREATE_COVERED_ROUND_CONTROL_TARGET(TARGET_STRUCT & sTarget, TARGET_TYPES_ENUM eTargetType, TARGET_CONSMETICS_ENUM eCosmetics, VECTOR vPosition, FLOAT fHeading, INT iLODdist, FLOAT fTargetFlipSpeed = 2.3)
	ENTITY_INDEX eiTarget, eiBase//, eiArm
	VECTOR vRot
	
	TARGET_MODELS_STRUCT sTargetModels
	sTargetModels = TARGET_GET_MODEL_BASED_ON_TYPE(eTargetType, eCosmetics)
	
	//fake ones
	MODEL_NAMES MODEL_DEFAULT_TARGET = sTargetModels.target
	MODEL_NAMES MODEL_DEFAULT_BASE = sTargetModels.base
	//MODEL_NAMES MODEL_DEFAULT_ARM = sTargetModels.arm

	SWITCH eTargetType
		CASE eTARGETTYPE_HUMAN_TOP
			eiTarget = GET_ENTITY_FROM_PED_OR_VEHICLE(CREATE_OBJECT_NO_OFFSET(MODEL_DEFAULT_TARGET, vPosition, FALSE, FALSE))
			SET_ENTITY_LOD_DIST(eiTarget, iLODdist)
			
//			eiArm =  GET_ENTITY_FROM_PED_OR_VEHICLE(CREATE_OBJECT_NO_OFFSET(MODEL_DEFAULT_ARM, vPosition, FALSE, FALSE))
//			SET_ENTITY_LOD_DIST(eiArm, iLODdist)
//			ATTACH_ENTITY_TO_ENTITY(eiTarget, eiArm, 0, <<0.0, 0.0, 1.100>>, <<0.0, 180.0, 0.0>>)
//			SET_ENTITY_LOD_DIST(eiArm, iLODdist)
			
			//Base Setup
			eiBase = GET_ENTITY_FROM_PED_OR_VEHICLE(CREATE_OBJECT_NO_OFFSET(MODEL_DEFAULT_BASE, vPosition, FALSE, FALSE))
			SET_ENTITY_LOD_DIST(eiBase, iLODdist)
			vRot = GET_ENTITY_ROTATION(eiBase)
			vRot.x = -20.0
			vRot.y = 180.0
			vRot.z = fHeading
			SET_ENTITY_ROTATION(eiBase, vRot)
			
			//Arm Position
			vPosition = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPosition, fHeading, <<0.0, 0.178, -0.120>>)
			SET_ENTITY_COORDS_NO_OFFSET(eiTarget, vPosition)
			
			
			vRot = GET_ENTITY_ROTATION(eiTarget)
			vRot.y = 0.0
			vRot.z = fHeading
			//CDEBUG1LN(DEBUG_SHOOTRANGE, "TGTID: ", sTarget.iTargetID, " 1TGTID: ", vRot)
			SET_ENTITY_ROTATION(eiTarget, vRot)
			//vRot = GET_ENTITY_ROTATION(eiTarget)
			//CDEBUG1LN(DEBUG_SHOOTRANGE, "TGTID: ", sTarget.iTargetID, " 2TGTID: ", vRot)
			
			sTarget.fAliveRoll = 0.0
			sTarget.fDeadRoll = 80.0
			sTarget.fTargetFlipSpeed = fTargetFlipSpeed
			sTarget.fTargetRotationBias = 0.0
			
		BREAK
		
		CASE eTARGETTYPE_HUMAN_BOTTOM
			eiTarget = GET_ENTITY_FROM_PED_OR_VEHICLE(CREATE_OBJECT_NO_OFFSET(MODEL_DEFAULT_TARGET, vPosition, FALSE, FALSE))
			SET_ENTITY_LOD_DIST(eiTarget, iLODdist)
			
			//
			//eiArm =  GET_ENTITY_FROM_PED_OR_VEHICLE(CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_target_05b")), vPosition, FALSE, FALSE))
			//ATTACH_ENTITY_TO_ENTITY(eiTarget, eiArm, 0, <<0.0, 0.0, 0.415>>, <<0.0, 0.0, 0.0>>)
			//SET_ENTITY_LOD_DIST(eiArm, iLODdist)
			
			eiBase = GET_ENTITY_FROM_PED_OR_VEHICLE(CREATE_OBJECT_NO_OFFSET(MODEL_DEFAULT_BASE, vPosition, FALSE, FALSE))
			SET_ENTITY_LOD_DIST(eiBase, iLODdist)
			vRot = GET_ENTITY_ROTATION(eiBase)
			vRot.y = 0.0
			vRot.z = fHeading
			SET_ENTITY_ROTATION(eiBase, vRot)			
			
			vPosition = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPosition, fHeading, <<0.0, 0.178, 0.075>>)
			SET_ENTITY_COORDS_NO_OFFSET(eiTarget, vPosition)
			
			vRot = GET_ENTITY_ROTATION(eiTarget)
			vRot.y = 0.0
			vRot.z = fHeading
			//CDEBUG1LN(DEBUG_SHOOTRANGE, "TGTID: ", sTarget.iTargetID, " 1TGTID: ", vRot)
			SET_ENTITY_ROTATION(eiTarget, vRot)
			vRot = GET_ENTITY_ROTATION(eiTarget)
			//CDEBUG1LN(DEBUG_SHOOTRANGE, "TGTID: ", sTarget.iTargetID, " 2TGTID: ", vRot)
			
			sTarget.fAliveRoll = 0.0
			sTarget.fDeadRoll = -80.0
			sTarget.fTargetFlipSpeed = fTargetFlipSpeed
			sTarget.fTargetRotationBias = 0.0
			
		BREAK

	ENDSWITCH

	IF DOES_ENTITY_EXIST(eiTarget)
	//IF DOES_ENTITY_EXIST(eiArm)
	AND DOES_ENTITY_EXIST(eiBase)
		SET_ENTITY_LOD_DIST(eiTarget, iLODdist)
		//SET_ENTITY_LOD_DIST(eiArm, iLODdist)
		SET_ENTITY_LOD_DIST(eiBase, iLODdist)
		
		FREEZE_ENTITY_POSITION(eiBase, TRUE)
		//FREEZE_ENTITY_POSITION(eiArm, TRUE)
		//SET_ENTITY_INVINCIBLE(eiArm, TRUE)
		SET_ENTITY_INVINCIBLE(eiBase, TRUE)
		TOGGLE_TARGET_FLAG(sTarget, TF_INITIALIZED)
		
		sTarget.entityTarget = eiTarget
		//sTarget.entityArm = eiArm
		sTarget.entityBoard = eiBase			
	
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

PROC UPDATE_TARGET_FLIP_STATE(TARGET_STRUCT & sTarget)
	IF GET_TARGET_FLAG(sTarget, TF_INITIALIZED)
	AND GET_TARGET_FLAG(sTarget, TF_FLIP)	
		IF DOES_ENTITY_EXIST(sTarget.entityTarget)
			FLOAT fFlipSpeed = sTarget.fTargetFlipSpeed
			#IF IS_DEBUG_BUILD
				IF sTarget.bDebugSpeed
					fFlipSpeed = 0.1
				ENDIF
			#ENDIF
			
			IF GET_TARGET_FLAG(sTarget, TF_ALIVE)
				sTarget.fTargetRotationBias +=  fFlipSpeed * GET_FRAME_TIME()
			ELSE
				sTarget.fTargetRotationBias -= fFlipSpeed* GET_FRAME_TIME()
			ENDIF
			
			sTarget.fTargetRotationBias = CLAMP(sTarget.fTargetRotationBias, 0.0, 1.0)	
			VECTOR vRotation = GET_ENTITY_ROTATION(sTarget.entityTarget)
			//CDEBUG1LN(DEBUG_SHOOTRANGE, "")
			//CDEBUG1LN(DEBUG_SHOOTRANGE, "DEFAUTL ROT: ", vRotation)
			vRotation.x = LERP_FLOAT(sTarget.fDeadRoll, sTarget.fAliveRoll, sTarget.fTargetRotationBias)
			//CDEBUG1LN(DEBUG_SHOOTRANGE, " ROT AFTER: ", vRotation, " fDeadRoll: ", sTarget.fDeadRoll, " fAliveRoll: ", sTarget.fAliveRoll)
			IF DOES_ENTITY_EXIST(sTarget.entityTarget)
				SET_ENTITY_ROTATION(sTarget.entityTarget, vRotation)
			ENDIF
			
			//vRotation = GET_ENTITY_ROTATION(sTarget.entityTarget)
			//CDEBUG1LN(DEBUG_SHOOTRANGE, "UPDATE_TARGET_FLIP_STATE - TARGETID: ", sTarget.iTargetID, " FlipBias: ", sTarget.fTargetRotationBias, " ENTITY ROT: ", vRotation)
			IF NOT GET_TARGET_FLAG(sTarget, TF_ALIVE) AND sTarget.fTargetRotationBias <= 0.2
				TOGGLE_TARGET_FLAG(sTarget, TF_PAST_MIN_HITABLE_THRESHOLD, TRUE) 
			ELSE
				TOGGLE_TARGET_FLAG(sTarget, TF_PAST_MIN_HITABLE_THRESHOLD, FALSE) 
			ENDIF
			
			IF GET_TARGET_FLAG(sTarget, TF_ALIVE)
				TOGGLE_TARGET_FLAG(sTarget, TF_PAST_MIN_HITABLE_THRESHOLD, FALSE) 
			ENDIF
			
			IF (GET_TARGET_FLAG(sTarget, TF_ALIVE) AND sTarget.fTargetRotationBias = 1.0)
			OR (NOT GET_TARGET_FLAG(sTarget, TF_ALIVE) AND sTarget.fTargetRotationBias = 0.0)
				TOGGLE_TARGET_FLAG(sTarget, TF_FLIP, FALSE)
				//CDEBUG1LN(DEBUG_SHOOTRANGE, "UPDATE_TARGET_FLIP_STATE - TARGETID: ", sTarget.iTargetID, " Flip Finished")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

#IF IS_DEBUG_BUILD

PROC DEBUG_TARGET_HIT_POINTS_WIDGET(TARGET_HIT_POINT_DEBUG &debug)
	START_WIDGET_GROUP("SHOOTING RANGE - TARGET HIT POINTS")
		ADD_WIDGET_BOOL("Enable Target Editing", debug.bTargetHitPointDebug)
		ADD_WIDGET_VECTOR_SLIDER("TargetRotation", debug.targetRotation, -99999.9, 99999.9, 0.01)
		ADD_WIDGET_INT_SLIDER("iCurrentHitLocation", debug.iCurrentHitLocation, 0, CI_MAX_TARGET_HIT_LOCATONS - 1, 1)
		ADD_WIDGET_INT_SLIDER("iCurrentHitLocationPoint", debug.iCurrentHitLocationPoint, 0, CI_MAX_TARGET_HIT_LOCATION_SHAPES - 1, 1)
		ADD_WIDGET_STRING("----------------------------")
		ADD_WIDGET_BOOL("DrawWholeHitPoint", debug.DrawWholeHitPoint)
		ADD_WIDGET_STRING("----------------------------")
		ADD_WIDGET_VECTOR_SLIDER("HitLocationOffset", debug.vHitLocationOffset, -99999.9, 99999.9, 0.01)
		ADD_WIDGET_STRING("----------------------------")
		ADD_WIDGET_VECTOR_SLIDER("HitLocationPointOffset", debug.vHitLocationPointOffset, -99999.9, 99999.9, 0.01)
		ADD_WIDGET_STRING("----------------------------")
		ADD_WIDGET_VECTOR_SLIDER("HitLocationPointSize", debug.vHitLocationPointSize, -99999.9, 99999.9, 0.01)
		ADD_WIDGET_STRING("----------------------------")
		START_NEW_WIDGET_COMBO()
			ADD_TO_WIDGET_COMBO("INVALID")
			ADD_TO_WIDGET_COMBO("SPHERE")
			ADD_TO_WIDGET_COMBO("BOX")
		STOP_WIDGET_COMBO("HitPointShape", debug.iShape)
	STOP_WIDGET_GROUP()
ENDPROC

PROC DEBUG_DRAW_HIT_LOCATION(VECTOR vTargetRoot, HIT_LOCATION_STRUCT sHitLocation, INT iPoint = 0)
	IF sHitLocation.sShape[iPoint].eShapeType <> TARGET_HIT_LOCATION_SHAPE_INVALID
		SWITCH sHitLocation.sShape[iPoint].eShapeType
			CASE TARGET_HIT_LOCATION_SHAPE_CIRCLE
				DRAW_DEBUG_SPHERE(vTargetRoot + sHitLocation.offset + sHitLocation.sShape[iPoint].offset, sHitLocation.sShape[iPoint].size.z)
			BREAK
			
			CASE TARGET_HIT_LOCATION_SHAPE_RECTANGLE
				DRAW_DEBUG_BOX(vTargetRoot + sHitLocation.offset, vTargetRoot + sHitLocation.offset + sHitLocation.sShape[iPoint].offset, 255,0,0)
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

PROC DEBUG_TARGET_HIT_POINTS_UPDATE(TARGET_HIT_POINT_DEBUG &debug)
	IF debug.iCurrentHitLocation <> debug.iPrevHitLocation
	OR debug.iCurrentHitLocationPoint <> debug.iPrevHitLocationPoint
		debug.vHitLocationOffset = debug.sHitLocationArray[debug.iCurrentHitLocation].offset
		debug.vHitLocationPointOffset = debug.sHitLocationArray[debug.iCurrentHitLocation].sShape[debug.iCurrentHitLocationPoint].offset
		debug.vHitLocationPointSize = debug.sHitLocationArray[debug.iCurrentHitLocation].sShape[debug.iCurrentHitLocationPoint].size
		debug.iShape = ENUM_TO_INT(debug.sHitLocationArray[debug.iCurrentHitLocation].sShape[debug.iCurrentHitLocationPoint].eShapeType)
	ENDIF
	
	IF debug.bTargetHitPointDebug
		debug.eTargetType = eTARGETTYPE_HUMAN_BOTTOM
		IF debug.eTargetType <> eTARGETTYPE_INVALID
			IF NOT DOES_ENTITY_EXIST(debug.entityTarget)
				REQUEST_MODEL(TARET_GET_MODEL_FROM_TYPE(debug.eTargetType))
				
				IF NOT HAS_MODEL_LOADED(TARET_GET_MODEL_FROM_TYPE(debug.eTargetType))
					EXIT
				ENDIF
				
				
				debug.entityTarget = CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_target_05c")), GET_ENTITY_COORDS(PLAYER_PED_ID()) + GET_ENTITY_FORWARD_VECTOR(PLAYER_PED_ID()) * 5.0)
			ENDIF
			
			IF NOT DOES_CAM_EXIST(debug.camera)
				debug.camera = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
				//RENDER_SCRIPT_CAMS(TRUE, FALSE)
				SET_CAM_PARAMS(debug.camera, <<0.0, -1.0, 0.0>>, <<0.0, 0.0, 0.0>>, 70)
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(debug.entityTarget)
			SET_ENTITY_ROTATION(debug.entityTarget, debug.targetRotation, EULER_YXZ, FALSE)
			IF NOT debug.DrawWholeHitPoint
				DEBUG_DRAW_HIT_LOCATION(GET_ENTITY_COORDS(debug.entityTarget), debug.sHitLocationArray[debug.iCurrentHitLocation], debug.iCurrentHitLocationPoint)
//				VECTOR vWorld
//				vWorld = GET_ENTITY_COORDS(debug.entityTarget) + debug.sHitLocationArray[debug.iCurrentHitLocation].offset + debug.sHitLocationArray[debug.iCurrentHitLocation].sShape[debug.iCurrentHitLocationPoint].offset
//				VECTOR vPos
//				vPos = GET_ENTITY_COORDS(debug.entityTarget)
			ELSE
				INT iPoint
				REPEAT CI_MAX_TARGET_HIT_LOCATION_SHAPES iPoint
					DEBUG_DRAW_HIT_LOCATION(GET_ENTITY_COORDS(debug.entityTarget), debug.sHitLocationArray[debug.iCurrentHitLocation], iPoint)
				ENDREPEAT
			ENDIF
		ENDIF
		
	ELSE
		IF DOES_ENTITY_EXIST(debug.entityTarget)
			DELETE_ENTITY(debug.entityTarget)
		ENDIF
		
		IF DOES_CAM_EXIST(debug.camera)
			//RENDER_SCRIPT_CAMS(FALSE, FALSE)
			DESTROY_CAM(debug.camera)
		ENDIF
	ENDIF
	
	debug.sHitLocationArray[debug.iCurrentHitLocation].offset = debug.vHitLocationOffset
	debug.sHitLocationArray[debug.iCurrentHitLocation].sShape[debug.iCurrentHitLocationPoint].offset = debug.vHitLocationPointOffset
	debug.sHitLocationArray[debug.iCurrentHitLocation].sShape[debug.iCurrentHitLocationPoint].size = debug.vHitLocationPointSize
	debug.sHitLocationArray[debug.iCurrentHitLocation].sShape[debug.iCurrentHitLocationPoint].eShapeType = INT_TO_ENUM(TARGET_HIT_LOCATION_SHAPE_TYPE, debug.iShape)
	
	debug.iPrevHitLocation = debug.iCurrentHitLocation
	debug.iPrevHitLocationPoint = debug.iCurrentHitLocationPoint
ENDPROC
#ENDIF
