USING "AM_MP_SHOOTING_RANGE_Core.sch"
USING "AM_MP_SHOOTING_RANGE_Target.sch"
USING "AM_MP_SHOOTING_RANGE_Broadcast.sch"

STRUCT LOCAL_PLAYER_DATA
	GAME_MODES					CurrentGameMode = eGM_INVALID
	GAME_RANGE					CurrentGameRange = eGR_INVALID
	SIMPLE_INTERIORS			CurrentLocation
	INT							iTargetScore = 0
	INT							iShooterID
	BOOL						bTargetDiedThisFrame = FALSE
	INT 						iLastDeadTargetID = -1
	//Num of active targets/alive/can shoot
	INT							iNumActiveTargets = 0
	BOOL						bTargetsSpawned = FALSE //True if all targets were spawned 
	TARGET_SPAWN_SETUP_STRUCT	sTargetSpawnSetup //setup on how targets should spawn time/ammount...
	
	//Number of initailized real targets.
	INT 						iPlayerTargets[CI_MAX_PLAYERS_MODE_BASIC]
	TARGET_STRUCT 				oTargets[CI_MAX_TARGETS]
	
	//Number of initailized fake targets, used as placeholders
	INT 						iFakeTargetsNum = 0
	TARGET_STRUCT 				oFakeTargets[CI_NUM_FAKE_TARGETS]
	
	SHOOTING_RANGE_FLOATING_SCORE floatingScores[SHOOTING_RANGE_MAX_FS]
	INT 						iTargetsHit = 0 //Num of times weapon impact hit a target.
	INT 						iBullseyesHit = 0
	
	WEAPON_TYPE 				eWeapon = WEAPONTYPE_INVALID //Weapon that is currently used for the shooting range.
	BOOL						bPlayerShot = FALSE //Player fired a weapon this frame
	BOOL						bPlayerJustShot = FALSE
	BOOL						bCheckNextFrame = FALSE
	
	//Start/end index in target array for current palyer - iShooterID
	INT  						iTargetStartIndex = 0
	INT	 						iTargetEndIndex = 0
	
	INT 						iTier = 1 //Difficulty level 
	
	INT							iCurrentHitCount = 0
	INT							iMaxHitCount = 4
	INT							iMultiplier = 1
	//Addition
	BOOL 						bTargetsTakeExtraHits = TRUE
	INT							iMissedCount = 0
	INT							iMaxNumExtraHits = -1 // -1 unlimited.
	//Alive Time
	FLOAT						fMaxTargetAliveTime = -1.0 //-1 stays up.
	//Timer to allow target hit update to run.
	SCRIPT_TIMER				stTargetUpdate
	//DEBUG
	#IF IS_DEBUG_BUILD
	BOOL						DebugTargetState = FALSE
	BOOL						DebugTargetSlow = FALSE
	TARGET_HIT_POINT_DEBUG		DebugTargetHitPoints
	VECTOR vHitLocationWorld
	VECTOR vHitLocationLocal
	#ENDIF
ENDSTRUCT

FUNC BOOL SET_SHOOTERID(LOCAL_PLAYER_DATA &lpd, INT iShooterID)
	IF iShooterID >= CI_PLAYER_ONE
	AND iShooterID <= CI_PLAYER_FOUR
		CDEBUG1LN(DEBUG_SHOOTRANGE, "SET_SHOOTERID - New ShooterID set: ", iShooterID)
		lpd.iShooterID = iShooterID
		RETURN TRUE
	ELSE
		CDEBUG1LN(DEBUG_SHOOTRANGE, "SET_SHOOTERID - atempt to assign invalid ShooterID: ", iShooterID)
		RETURN FALSE
	ENDIF
ENDFUNC

PROC SET_SHOOTING_RANGE_TIER(LOCAL_PLAYER_DATA &lpd, INT iTier)
	CDEBUG1LN(DEBUG_SHOOTRANGE, "SET_SHOOTING_RANGE_TIER - New iTier set: ", iTier)
	
	lpd.iTier = iTier
ENDPROC
