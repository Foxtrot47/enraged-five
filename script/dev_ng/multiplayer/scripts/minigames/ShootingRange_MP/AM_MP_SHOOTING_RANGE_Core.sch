USING "UIUtil.sch"
USING "script_drawing.sch"

CONST_INT CI_NUM_FAKE_TARGETS 4 //targets for row 5
CONST_INT CI_MAX_PLAYERS_MODE_BASIC 4
CONST_INT CI_MAX_TARGET_PER_PLAYER_BASIC 18
CONST_INT CI_MAX_TARGETS CI_MAX_TARGET_PER_PLAYER_BASIC * CI_MAX_PLAYERS_MODE_BASIC
CONST_INT CI_TARGET_ALIVE_HEALTH 9999
CONST_INT CI_TARGET_DEAD_HEALTH 0

CONST_INT CI_PLAYER_INVALID -1
CONST_INT CI_PLAYER_ONE 0
CONST_INT CI_PLAYER_TWO 1
CONST_INT CI_PLAYER_THREE 2
CONST_INT CI_PLAYER_FOUR 3
CONST_INT CI_PLAYER_FIVE 4
CONST_INT CI_MAX_PLAYER_COUNT 4

CONST_INT 	SHOOTING_RANGE_MAX_FS				4		// The maximum number of floating score texts we can have.
CONST_FLOAT	SHOOTING_RANGE_SCORE_LIFETIME		0.66	// How long should a floating score live for?
CONST_FLOAT SHOOTING_RANGE_SCORE_SCALE 			0.73333 // 22px/30px (desired/default)
CONST_FLOAT SHOOTING_RANGE_SCORE_SPACING 		0.03472

ENUM GAME_MODES
	eGM_BASIC, 
	eGM_BOTTLES,
	eGM_INVALID
ENDENUM

ENUM GAME_RANGE
	eGR_INVALID = BIT0,
	eGR_NORMAL_RANGE = BIT1,
	eGR_LONG_RANGE = BIT2,
	eGR_NORMAL_GROUND = BIT3,
	eGR_LONG_GROUND = BIT4
ENDENUM

#IF IS_DEBUG_BUILD
BOOL g_bResetShootingRangeTargets = FALSE
BOOL g_bUpgradeShootingRangeTargets = FALSE
BOOL g_bWhiteShootingRangeTargets = FALSE
#ENDIF

STRUCT SHOOTING_RANGE_FLOATING_SCORE
	BOOL bActive
	
	FLOAT fPosX
	FLOAT fPosY
	FLOAT fFrameTime
	
	HUD_COLOURS color
	
	INT iAlpha
	INT iPtValue
ENDSTRUCT

STRUCT TARGET_SPAWN_SETUP_STRUCT
	INT		iMaxAliveTargets = 1 //Max alive tergets at the same time.
	INT		iTargetNoHitSpawnDelay = 2500 //If no target was hit within X time, spawn a new target.
	INT		iTargetAliveHitRespawnDelay = 1000 // if there is a target already alive after a target was hit, how long will it take to spawn a new one.
	INT		iTargetDeadHitRespawnDelay = 250 //How long will it take to spawn a new target if no targets are alive.
ENDSTRUCT

FUNC STRING GAME_MODES_STRING(GAME_MODES eGM)
	SWITCH eGM
		CASE eGM_BASIC RETURN "eGM_BASIC"
		CASE eGM_BOTTLES RETURN "eGM_BOTTLES"
		CASE eGM_INVALID RETURN "eGM_INVALID"
	ENDSWITCH
	
	RETURN "eGM_UNKNOWN"
ENDFUNC

/// PURPOSE:
///    Sets up all the basics for the floating score queue.
PROC SETUP_FLOATING_SCORE_QUEUE(SHOOTING_RANGE_FLOATING_SCORE& floatingScores[])
	INT index
	REPEAT SHOOTING_RANGE_MAX_FS index
		floatingScores[index].fPosX = 0.5 + 0.00234 // middle of screen plus small offset for centering
		floatingScores[index].fPosY = PIXEL_Y_TO_FLOAT(300)
		floatingScores[index].iAlpha = 255
		floatingScores[index].iPtValue = 0
		floatingScores[index].fFrameTime = -1
		floatingScores[index].bActive = FALSE
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Sorts the floating score queue based off of time spent alive.
PROC SHOOTING_RANGE_SORT_FLOATING_SCORES(SHOOTING_RANGE_FLOATING_SCORE& floatingScores[])
	INT iI, iJ
	SHOOTING_RANGE_FLOATING_SCORE tempScore
	
	iI = 1
	WHILE iI < SHOOTING_RANGE_MAX_FS
		tempScore = floatingScores[iI]
		
		iJ = iI
		WHILE (iJ > 0) AND (tempScore.fFrameTime < floatingScores[iJ - 1].fFrameTime)
			floatingScores[iJ] = floatingScores[iJ - 1]
			iJ--
		ENDWHILE
		
		floatingScores[iJ] = tempScore
		iI++
	ENDWHILE
ENDPROC

/// PURPOSE:
///    Finds the next open slot.
FUNC INT SHOOTING_RANGE_GET_NEXT_FLOATING_SCORE_SLOT(SHOOTING_RANGE_FLOATING_SCORE& floatingScores[])
	INT index
	
	// Sort first.
	SHOOTING_RANGE_SORT_FLOATING_SCORES(floatingScores)
	
	// First, go through all scores, and make sure we have an active spot.
	REPEAT SHOOTING_RANGE_MAX_FS index
		IF NOT (floatingScores[index].bActive)
			RETURN index
		ENDIF
	ENDREPEAT
	
	RETURN SHOOTING_RANGE_MAX_FS - 1
ENDFUNC

/// PURPOSE:
///    Adds a floating score to our array.
PROC SHOOTING_RANGE_ADD_FLOATING_SCORE(INT iValue, HUD_COLOURS eColor, SHOOTING_RANGE_FLOATING_SCORE& floatingScores[])
	// Get our next slot
	INT iSlot = SHOOTING_RANGE_GET_NEXT_FLOATING_SCORE_SLOT(floatingScores)
	
	// Set the score up.
	floatingScores[iSlot].fPosY = PIXEL_Y_TO_FLOAT(300)
	floatingScores[iSlot].iAlpha = 255
	floatingScores[iSlot].iPtValue = iValue
	floatingScores[iSlot].bActive = TRUE
	floatingScores[iSlot].fFrameTime = 0
	floatingScores[iSlot].color = eColor
	
	SHOOTING_RANGE_SORT_FLOATING_SCORES(floatingScores)
ENDPROC

/// PURPOSE:
///    Makes a floating score move up the screen.
PROC SHOOTING_RANGE_SCROLL_FLOATING_SCORE(SHOOTING_RANGE_FLOATING_SCORE& singularScore)
	FLOAT fStart,fEnd
	FLOAT fInterpTime
	FLOAT fAlphaStartFadeOut = 0.5
	
	// Increment our frame count
	singularScore.fFrameTime += GET_FRAME_TIME()
	
	// Find out how much we should be flipping
	fInterpTime = singularScore.fFrameTime / SHOOTING_RANGE_SCORE_LIFETIME
	
	// Keep going until we reach 1.0 (means we are at our destination)
	IF fInterpTime < 1.0
		// Position
		fStart = PIXEL_Y_TO_FLOAT(300)
		fEnd = PIXEL_Y_TO_FLOAT(260)
		
		// Quad OUT
		singularScore.fPosY = -(fEnd - fStart) * (fInterpTime / SHOOTING_RANGE_SCORE_LIFETIME) * (fInterpTime - 2) + fStart
		
		// Alpha
		IF fInterpTime < fAlphaStartFadeOut
			singularScore.iAlpha = 255
		ELSE
			singularScore.iAlpha = 255 - ROUND((fInterpTime - fAlphaStartFadeOut) / fAlphaStartFadeOut * 255)
		ENDIF
		
		singularScore.bActive = TRUE
	ELSE
		// Dissapear
		singularScore.iAlpha = 0
		singularScore.bActive = FALSE
		singularScore.fFrameTime = -1
	ENDIF
ENDPROC

/// PURPOSE:
///    Updates all floating scores in the floating score queue.
PROC SHOOTING_RANGE_UPDATE_FLOATING_SCORES(SHOOTING_RANGE_FLOATING_SCORE& floatingScores[])
	INT index, iActiveCount
	BOOL bNeedSort = FALSE
	
	// Scroll the text up after its been displayed
	REPEAT SHOOTING_RANGE_MAX_FS index
		// Is this active
		IF floatingScores[index].bActive
			SHOOTING_RANGE_SCROLL_FLOATING_SCORE(floatingScores[index])
			iActiveCount++
			
			// Sort SPTs when SPT becomes inactive
			IF NOT floatingScores[index].bActive
				bNeedSort = TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF iActiveCount > 0
		IF bNeedSort
			SHOOTING_RANGE_SORT_FLOATING_SCORES(floatingScores)
		ENDIF
		
		// Reposition based on latest entry
		FOR index = 0 TO SHOOTING_RANGE_MAX_FS - 2
			IF floatingScores[index].bActive
			AND floatingScores[index + 1].bActive
			AND floatingScores[index+1].fPosY > (floatingScores[index].fPosY - SHOOTING_RANGE_SCORE_SPACING)
				floatingScores[index+1].fPosY = floatingScores[index].fPosY - SHOOTING_RANGE_SCORE_SPACING
			ENDIF
		ENDFOR
	ENDIF
ENDPROC

/// PURPOSE:
///    Helper function to get a color that matches hud
/// PARAMS:
///    hudColor - The hud color you want to match
/// RETURNS:
///    The INT_COLOR of the hud color you pass in
FUNC INT GET_TEXT_COLOUR(HUD_COLOURS hudColour, INT iAlpha = -1)
	INT iR, iG, iB, iA
	GET_HUD_COLOUR(hudColour, iR, iG, iB, iA)
	
	IF iAlpha = -1
		RETURN INT_COLOUR(iR, iG, iB, iA)
	ELSE
		RETURN INT_COLOUR(iR, iG, iB, iAlpha)
	ENDIF
ENDFUNC

/// PURPOSE:
///    Draws all active scores in the floating score queue.
PROC SHOOTING_RANGE_DRAW_FLOATING_SCORES(SHOOTING_RANGE_FLOATING_SCORE& floatingScores[])
	INT index
	
	// First, update all of the text.
	SHOOTING_RANGE_UPDATE_FLOATING_SCORES(floatingScores)
	
	// This text will use an outline.
	SET_TEXT_OUTLINE()
	
	index = SHOOTING_RANGE_MAX_FS - 1
	WHILE (index >= 0)
		IF floatingScores[index].bActive
			SET_TEXT_OUTLINE()
			
			// Use a different string if these points are positive or negative.
			STRING sToUse = "NUMBER"
			IF (floatingScores[index].iPtValue > 0)
				sToUse = "AMMO_REWARD"
			ENDIF
			
			DISPLAY_TEXT_LABEL_WITH_NUMBER(sToUse,
				floatingScores[index].fPosX - GET_STRING_WIDTH_WITH_NUMBER("AMMO_REWARD", floatingScores[index].iPtValue) * SHOOTING_RANGE_SCORE_SCALE / 2.0,
				floatingScores[index].fPosY - GET_RENDERED_CHARACTER_HEIGHT(SHOOTING_RANGE_SCORE_SCALE) * SHOOTING_RANGE_SCORE_SCALE / 2.0,
				SHOOTING_RANGE_SCORE_SCALE, SHOOTING_RANGE_SCORE_SCALE,	1.0, 1.0,
				GET_TEXT_COLOUR(floatingScores[index].color, floatingScores[index].iAlpha),
				floatingScores[index].iPtValue,
				0,
				0,
				FONT_STANDARD)
		ENDIF
		
		index--
	ENDWHILE
ENDPROC
