USING "mp_globals_simple_interior_consts.sch"
USING "AM_MP_SHOOTING_RANGE_Core.sch"
USING "AM_MP_SHOOTING_RANGE_Target.sch"
USING "script_maths.sch"

/// PURPOSE:
///   	Center position of Player target Row. For target duplication reference
/// PARAMS:
///    lpd - 
///    iForPlayer - 
/// RETURNS:
///    
FUNC VECTOR GET_TARGET_RANGE_CENTER(SIMPLE_INTERIORS eInterior, GAME_MODES eGameMode, INT iForPlayer)
	
	SWITCH eInterior
		CASE SIMPLE_INTERIOR_BUNKER_1
		CASE SIMPLE_INTERIOR_BUNKER_2
		CASE SIMPLE_INTERIOR_BUNKER_3
		CASE SIMPLE_INTERIOR_BUNKER_4
		CASE SIMPLE_INTERIOR_BUNKER_5
		CASE SIMPLE_INTERIOR_BUNKER_6
		CASE SIMPLE_INTERIOR_BUNKER_7
//		CASE SIMPLE_INTERIOR_BUNKER_8
		CASE SIMPLE_INTERIOR_BUNKER_9
		CASE SIMPLE_INTERIOR_BUNKER_10
		CASE SIMPLE_INTERIOR_BUNKER_11
		CASE SIMPLE_INTERIOR_BUNKER_12
			SWITCH eGameMode
				CASE eGM_BASIC
					SWITCH iForPlayer
						CASE CI_PLAYER_ONE RETURN <<893.9187, -3164.739, -98.125>>
						CASE CI_PLAYER_TWO RETURN <<895.5187, -3165.010, -98.125>>
						CASE CI_PLAYER_THREE RETURN <<897.0687, -3165.290, -98.125>>
						CASE CI_PLAYER_FOUR RETURN <<898.680, -3165.560, -98.125>>
						CASE CI_PLAYER_FIVE RETURN <<900.2844, -3165.860, -98.125>>
					ENDSWITCH
				BREAK
			ENDSWITCH
	ENDSWITCH
	
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

/// Target offsets from player 1
FUNC VECTOR GET_TARGET_OFFSET_FROM_CENTER(LOCAL_PLAYER_DATA &lpd, INT iTarget)
	
	SWITCH lpd.CurrentLocation
		CASE SIMPLE_INTERIOR_BUNKER_1
		CASE SIMPLE_INTERIOR_BUNKER_2
		CASE SIMPLE_INTERIOR_BUNKER_3
		CASE SIMPLE_INTERIOR_BUNKER_4
		CASE SIMPLE_INTERIOR_BUNKER_5
		CASE SIMPLE_INTERIOR_BUNKER_6
		CASE SIMPLE_INTERIOR_BUNKER_7
//		CASE SIMPLE_INTERIOR_BUNKER_8
		CASE SIMPLE_INTERIOR_BUNKER_9
		CASE SIMPLE_INTERIOR_BUNKER_10
		CASE SIMPLE_INTERIOR_BUNKER_11
		CASE SIMPLE_INTERIOR_BUNKER_12
			SWITCH lpd.CurrentGameMode
				CASE eGM_BASIC
					SWITCH iTarget
						//NORMAL
						CASE 0 RETURN <<-0.263123, 0.351074, 0.000000>>
						CASE 1 RETURN <<0.207214, 2.095947, 2.860001>>
						CASE 2 RETURN <<0.572998, 3.249756, 0.000000>>
						CASE 3 RETURN <<1.025696, 4.896240, 2.860001>>
						CASE 4 RETURN <<1.409180, 6.148682, 0.000000>>
						CASE 5 RETURN <<1.624817, 8.293945, 2.860001>>
						CASE 6 RETURN <<1.666992, 9.454102, 0.000000>>
						CASE 7 RETURN <<2.047852, 12.534912, 2.860001>>
						CASE 8 RETURN <<2.185364, 14.236816, 0.000000>>
						//LONG
						CASE 9  RETURN <<5.419006, 29.811768, 2.860001>>
						CASE 10 RETURN <<5.574097, 31.612305, 0.000000>>
						CASE 11 RETURN <<6.467224, 37.598145, 2.860001>>
						CASE 12 RETURN <<6.613342, 39.349365, 0.000000>>
						CASE 13 RETURN <<7.951904, 46.018311, 2.860001>>
						CASE 14 RETURN <<8.474060, 48.058594, 0.000000>>
						CASE 15 RETURN <<10.147461, 56.628174, 2.860001>>
						CASE 16 RETURN <<10.751465, 59.131348, 0.000000>>
						CASE 17 RETURN <<12.390991, 69.351807, 2.860001>>
					ENDSWITCH
				BREAK
			ENDSWITCH
	ENDSWITCH
	
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

/// PURPOSE:
///    Each player can have 9 targets max in this game mode, add target info to the target pool
/// PARAMS:
///    iPlayer - for which player the target is for.
PROC ADD_TARGET(LOCAL_PLAYER_DATA &lpd, INT iPlayer, TARGET_TYPES_ENUM eTargetType, TARGET_CONSMETICS_ENUM eTargetCosmetics, VECTOR vPosition, FLOAT fHeading, INT iLODdistance, GAME_RANGE eForRange = eGR_INVALID, BOOL bPositionAsOffset = TRUE)
	IF lpd.iPlayerTargets[iPlayer] < CI_MAX_TARGET_PER_PLAYER_BASIC
		VECTOR vTargetPostition
		INT iTarget
		
		iTarget = iPlayer * CI_MAX_TARGET_PER_PLAYER_BASIC + lpd.iPlayerTargets[iPlayer]
		
		IF eForRange != eGR_INVALID
			SET_BITMASK_AS_ENUM(lpd.oTargets[iTarget].iTargetRangeBitSet, eForRange)
		ENDIF
		
		IF NOT DOES_ENTITY_EXIST(lpd.oTargets[iTarget].entityTarget)
			lpd.oTargets[iTarget].iTargetID = iTarget
			lpd.oTargets[iTarget].eTargetType = eTargetType
			
			IF bPositionAsOffset
				vTargetPostition = GET_TARGET_RANGE_CENTER(lpd.CurrentLocation, lpd.CurrentGameMode, iPlayer) + vPosition
			ELSE
				vTargetPostition = vPosition
			ENDIF
			
			VECTOR vCenter = GET_TARGET_RANGE_CENTER(lpd.CurrentLocation, lpd.CurrentGameMode, iPlayer)
			
			CDEBUG1LN(DEBUG_SHOOTRANGE, "TARGET SPAWNED: ID: ", iTarget, " BASE: ", vCenter, " Offset: ", vPosition, " Position: ", vTargetPostition)
			CREATE_COVERED_ROUND_CONTROL_TARGET(lpd.oTargets[iTarget], eTargetType, eTargetCosmetics, vTargetPostition, fHeading, iLODdistance)
			IF iPlayer = lpd.iShooterID
				lpd.iNumActiveTargets ++
			ENDIF
			#IF IS_DEBUG_BUILD
				lpd.oTargets[iTarget].vDebugPosition = vPosition
			#ENDIF
			CDEBUG3LN(DEBUG_SHOOTRANGE, "TARGET SPAWNED ID: ", iTarget, " FOR PLAYER: ", lpd.iShooterID, " iNumActiveTargets: ", lpd.iNumActiveTargets)
		ENDIF
		
		CDEBUG3LN(DEBUG_SHOOTRANGE, "ADD_TARGET - Target Added for Player: ", iPlayer, " Target ID: ", iTarget)
		lpd.iPlayerTargets[iPlayer]++
	ELSE
		CDEBUG3LN(DEBUG_SHOOTRANGE, "ADD_TARGET - Trying to add too many targets Player: ", iPlayer, " Target MaxNum: ", CI_MAX_TARGET_PER_PLAYER_BASIC)
	ENDIF
ENDPROC

PROC ADD_FAKE_TARGET(LOCAL_PLAYER_DATA &lpd, TARGET_TYPES_ENUM eTargetType, TARGET_CONSMETICS_ENUM eTargetCosmetics, VECTOR vPosition, FLOAT fHeading, INT iLODdistance)
	IF lpd.iFakeTargetsNum < CI_NUM_FAKE_TARGETS
		IF NOT DOES_ENTITY_EXIST(lpd.oFakeTargets[lpd.iFakeTargetsNum].entityTarget)
			CREATE_COVERED_ROUND_CONTROL_TARGET(lpd.oFakeTargets[lpd.iFakeTargetsNum], eTargetType, eTargetCosmetics, vPosition, fHeading, iLODdistance)
		ENDIF
		CDEBUG1LN(DEBUG_SHOOTRANGE, "ADD_FAKE_TARGET - Fake target added, lpd.iFakeTargetsNum: ", lpd.iFakeTargetsNum, " Target MaxNum: ", CI_NUM_FAKE_TARGETS)
		lpd.iFakeTargetsNum++
	ELSE
		CDEBUG1LN(DEBUG_SHOOTRANGE, "ADD_FAKE_TARGET - Trying to add too many fake targets lpd.iFakeTargetsNum: ", lpd.iFakeTargetsNum, " Target MaxNum: ", CI_NUM_FAKE_TARGETS)
	ENDIF
ENDPROC

PROC POPULATE_TARGET_LOCATIONS(LOCAL_PLAYER_DATA &lpd)
	
	IF lpd.bTargetsSpawned
		EXIT
	ENDIF
	
	TARGET_CONSMETICS_ENUM eTargetCosmetics, eFakeTargetCosmetics
		
	IF IS_PLAYER_BUNKER_FIRING_RANGE_WHITE_PURCHASED(g_ownerOfBunkerPropertyIAmIn)
	#IF IS_DEBUG_BUILD
	OR g_bWhiteShootingRangeTargets
	#ENDIF
		eTargetCosmetics = eTARGET_COSMETICS_WHITE_NEW
		eFakeTargetCosmetics = eTARGET_COSMETICS_WHITE_OLD
		CDEBUG1LN(DEBUG_SHOOTRANGE, "POPULATE_TARGET_LOCATIONS - white new targets")
	ELIF IS_PLAYER_BUNKER_FIRING_RANGE_BLACK_PURCHASED(g_ownerOfBunkerPropertyIAmIn)
		eTargetCosmetics = eTARGET_COSMETICS_BLACK_NEW
		eFakeTargetCosmetics = eTARGET_COSMETICS_BLACK_OLD
		CDEBUG1LN(DEBUG_SHOOTRANGE, "POPULATE_TARGET_LOCATIONS - black new targets")
	ELSE
		eTargetCosmetics = eTARGET_COSMETICS_BLACK_NEW
		eFakeTargetCosmetics = eTARGET_COSMETICS_BLACK_OLD
		CDEBUG1LN(DEBUG_SHOOTRANGE, "POPULATE_TARGET_LOCATIONS - no cosmetics purchased defaulting to black.")
	ENDIF
	
	SWITCH lpd.CurrentLocation
		CASE SIMPLE_INTERIOR_BUNKER_1
		CASE SIMPLE_INTERIOR_BUNKER_2
		CASE SIMPLE_INTERIOR_BUNKER_3
		CASE SIMPLE_INTERIOR_BUNKER_4
		CASE SIMPLE_INTERIOR_BUNKER_5
		CASE SIMPLE_INTERIOR_BUNKER_6
		CASE SIMPLE_INTERIOR_BUNKER_7
//		CASE SIMPLE_INTERIOR_BUNKER_8
		CASE SIMPLE_INTERIOR_BUNKER_9
		CASE SIMPLE_INTERIOR_BUNKER_10
		CASE SIMPLE_INTERIOR_BUNKER_11
		CASE SIMPLE_INTERIOR_BUNKER_12
			SWITCH lpd.CurrentGameMode
				CASE eGM_BASIC
					TARGET_TYPES_ENUM eTargetType
					INT iPlayer, iTarget
					//Create targets
					REPEAT CI_MAX_PLAYER_COUNT iPlayer
						//first target always Bottom
						eTargetType = eTARGETTYPE_HUMAN_BOTTOM
						
						REPEAT CI_MAX_TARGET_PER_PLAYER_BASIC iTarget
							CDEBUG1LN(DEBUG_SHOOTRANGE, "ADD TARGET EXCLUSIONS: iTarget: ", iTarget)
							ADD_TARGET(lpd, iPlayer, eTargetType, eTargetCosmetics, GET_TARGET_OFFSET_FROM_CENTER(lpd, iTarget), 350.0, 150)
						//Swap next target top/bottom
						IF eTargetType = eTARGETTYPE_HUMAN_BOTTOM
							eTargetType = eTARGETTYPE_HUMAN_TOP
						ELSE
							eTargetType = eTARGETTYPE_HUMAN_BOTTOM
						ENDIF
						ENDREPEAT
					ENDREPEAT
					CDEBUG1LN(DEBUG_SHOOTRANGE, "POPULATE_TARGET_LOCATIONS - all player targets created!")
					//Set target ranges
					REPEAT CI_MAX_PLAYER_COUNT iPlayer
						INT iTargetIDStart
						iTargetIDStart = iPlayer * CI_MAX_TARGET_PER_PLAYER_BASIC
						CDEBUG1LN(DEBUG_SHOOTRANGE, "iTargetIDStart: ", iTargetIDStart)

						SET_TARGET_RANGE(lpd.oTargets[iTargetIDStart + 0], eGR_NORMAL_RANGE | eGR_NORMAL_GROUND)
						SET_TARGET_RANGE(lpd.oTargets[iTargetIDStart + 1], eGR_NORMAL_RANGE)
						SET_TARGET_RANGE(lpd.oTargets[iTargetIDStart + 2], eGR_NORMAL_RANGE | eGR_NORMAL_GROUND)
						SET_TARGET_RANGE(lpd.oTargets[iTargetIDStart + 3], eGR_NORMAL_RANGE)
						SET_TARGET_RANGE(lpd.oTargets[iTargetIDStart + 4], eGR_NORMAL_RANGE | eGR_NORMAL_GROUND)
						SET_TARGET_RANGE(lpd.oTargets[iTargetIDStart + 5], eGR_NORMAL_RANGE)
						SET_TARGET_RANGE(lpd.oTargets[iTargetIDStart + 6], eGR_NORMAL_RANGE | eGR_NORMAL_GROUND)
						SET_TARGET_RANGE(lpd.oTargets[iTargetIDStart + 7], eGR_NORMAL_RANGE)
						SET_TARGET_RANGE(lpd.oTargets[iTargetIDStart + 8], eGR_NORMAL_RANGE | eGR_NORMAL_GROUND)
						
						SET_TARGET_RANGE(lpd.oTargets[iTargetIDStart + 9],  eGR_LONG_RANGE)
						SET_TARGET_RANGE(lpd.oTargets[iTargetIDStart + 10], eGR_LONG_RANGE | eGR_LONG_GROUND)
						SET_TARGET_RANGE(lpd.oTargets[iTargetIDStart + 11], eGR_LONG_RANGE)
						SET_TARGET_RANGE(lpd.oTargets[iTargetIDStart + 12], eGR_LONG_RANGE | eGR_LONG_GROUND)
						SET_TARGET_RANGE(lpd.oTargets[iTargetIDStart + 13], eGR_LONG_RANGE)
						SET_TARGET_RANGE(lpd.oTargets[iTargetIDStart + 14], eGR_LONG_RANGE | eGR_LONG_GROUND)
						SET_TARGET_RANGE(lpd.oTargets[iTargetIDStart + 15], eGR_LONG_RANGE)
						SET_TARGET_RANGE(lpd.oTargets[iTargetIDStart + 16], eGR_LONG_RANGE | eGR_LONG_GROUND)
						SET_TARGET_RANGE(lpd.oTargets[iTargetIDStart + 17], eGR_LONG_RANGE)
						
					ENDREPEAT
					CDEBUG1LN(DEBUG_SHOOTRANGE, "POPULATE_TARGET_LOCATIONS - all player targets range setup set!")
					
					REPEAT CI_NUM_FAKE_TARGETS iTarget
						ADD_FAKE_TARGET(lpd, eTARGETTYPE_HUMAN_BOTTOM, eFakeTargetCosmetics, GET_TARGET_OFFSET_FROM_CENTER(lpd, 2) + GET_TARGET_RANGE_CENTER(lpd.CurrentLocation, lpd.CurrentGameMode, CI_PLAYER_FIVE), 350.0, 150)
						ADD_FAKE_TARGET(lpd, eTARGETTYPE_HUMAN_BOTTOM, eFakeTargetCosmetics, GET_TARGET_OFFSET_FROM_CENTER(lpd, 4) + GET_TARGET_RANGE_CENTER(lpd.CurrentLocation, lpd.CurrentGameMode, CI_PLAYER_FIVE), 350.0, 150)
						ADD_FAKE_TARGET(lpd, eTARGETTYPE_HUMAN_BOTTOM, eFakeTargetCosmetics, GET_TARGET_OFFSET_FROM_CENTER(lpd, 6) + GET_TARGET_RANGE_CENTER(lpd.CurrentLocation, lpd.CurrentGameMode, CI_PLAYER_FIVE), 350.0, 150)
						ADD_FAKE_TARGET(lpd, eTARGETTYPE_HUMAN_BOTTOM, eFakeTargetCosmetics, GET_TARGET_OFFSET_FROM_CENTER(lpd, 8) + GET_TARGET_RANGE_CENTER(lpd.CurrentLocation, lpd.CurrentGameMode, CI_PLAYER_FIVE), 350.0, 150)
					ENDREPEAT
					CDEBUG1LN(DEBUG_SHOOTRANGE, "POPULATE_TARGET_LOCATIONS - all lane 5 fake targets created!")
					
					lpd.bTargetsSpawned = TRUE
//DEBUG POSITIONING
//			VECTOR vForward, vRight, vUp
//			
//			
//			vForward = GET_ENTITY_FORWARD_VECTOR(lpd.oTargets[0].entityBoard)
//			vUp = <<0.0, 0.0, 1.0>>
//			vRight = CROSS_PRODUCT(vForward, vUp)
//			vForward = CROSS_PRODUCT(vUp, vRight)
//			
//					CDEBUG1LN(DEBUG_SHOOTRANGE, "654")
//					INT iLocation
//					VECTOR vOffset, vTargetOffset
//					FOR iLocation = 0 TO 23 STEP 1
//						vTargetOffset = GET_TARGET_OFFSET_FROM_CENTER(lpd, iLocation)
//							
//						vOffset = GET_TARGET_RANGE_CENTER(lpd, 0) + (vForward * vTargetOffset.x + vRight * vTargetOffset.y + vUp * vTargetOffset.z)
//			
//						vOffset =  GET_TARGET_RANGE_CENTER(lpd, 0) - vOffset
//						
//						CDEBUG1LN(DEBUG_SHOOTRANGE, "CASE ", iLocation, " RETURN <<", vOffset.x, ", ", vOffset.y, ", ", vOffset.z, ">>")
//						//CDEBUG1LN(DEBUG_SHOOTRANGE, "CASEvtwo ", iLocation, " RETURN <<", vtwo.x, ", ", vtwo.y, ", ", vtwo.z, ">>")
//					ENDFOR
//					CDEBUG1LN(DEBUG_SHOOTRANGE, "")
					
					
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
ENDPROC

