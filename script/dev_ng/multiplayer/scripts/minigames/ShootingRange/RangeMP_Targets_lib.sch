// RangeMP_Targets_lib.sch
USING "RangeMP_Targets.sch"
USING "range_public.sch"


/// PURPOSE:
///    Creates a target for the grid round.
/// RETURNS:
///    The created target.
FUNC RangeTarget CREATE_GRID_TARGET(CI_GRID_OBJECT_INDEXES eGridToSpawnIn, TARGET_FLAGS eColor, ENTITY_INDEX oGridObj)
	RangeTarget sRetVal
	sRetVal.oTarget = GET_ENTITY_FROM_PED_OR_VEHICLE(CREATE_OBJECT(PROP_TARGET_ORA_PURP_01, <<-10,-10,-10>>, FALSE, FALSE))
	
	// Attach the target to the grid. for some reason, we need to bring them up half of the target size.
	VECTOR vOffset
	
	// Z offset based on the grid. (Our up and down movement)
	IF (eGridToSpawnIn >= CI_GRID_A1) AND (eGridToSpawnIn <= CI_GRID_A7)
		vOffset.z = 0.96
	ELIF (eGridToSpawnIn >= CI_GRID_B1) AND (eGridToSpawnIn <= CI_GRID_B7)
		vOffset.z = 0.32
	ELIF (eGridToSpawnIn >= CI_GRID_C1) AND (eGridToSpawnIn <= CI_GRID_C7)
		vOffset.z = -0.32
	ELSE
		vOffset.z = -0.96
	ENDIF
	
	// X Offset based on the grid to spawn in. (Our left to right movement)
	IF (eGridToSpawnIn = CI_GRID_A1) OR (eGridToSpawnIn = CI_GRID_B1) OR (eGridToSpawnIn = CI_GRID_C1) OR (eGridToSpawnIn = CI_GRID_D1)
		vOffset.x = -1.975
	ELIF (eGridToSpawnIn = CI_GRID_A2) OR (eGridToSpawnIn = CI_GRID_B2) OR (eGridToSpawnIn = CI_GRID_C2) OR (eGridToSpawnIn = CI_GRID_D2)
		vOffset.x = -1.315	
	ELIF (eGridToSpawnIn = CI_GRID_A3) OR (eGridToSpawnIn = CI_GRID_B3) OR (eGridToSpawnIn = CI_GRID_C3) OR (eGridToSpawnIn = CI_GRID_D3)
		vOffset.x = -0.66
	ELIF (eGridToSpawnIn = CI_GRID_A5) OR (eGridToSpawnIn = CI_GRID_B5) OR (eGridToSpawnIn = CI_GRID_C5) OR (eGridToSpawnIn = CI_GRID_D5)
		vOffset.x = 0.66
	ELIF (eGridToSpawnIn = CI_GRID_A6) OR (eGridToSpawnIn = CI_GRID_B6) OR (eGridToSpawnIn = CI_GRID_C6) OR (eGridToSpawnIn = CI_GRID_D6)
		vOffset.x = 1.315
	ELIF (eGridToSpawnIn = CI_GRID_A7) OR (eGridToSpawnIn = CI_GRID_B7) OR (eGridToSpawnIn = CI_GRID_C7) OR (eGridToSpawnIn = CI_GRID_D7)
		vOffset.x = 1.975
	ENDIF
			
	IF (eColor = TARG_F_IS_RED)
		//SET_ENTITY_HEADING(sRetVal.oTarget, CI_GRID_TARGET_RED_ORI)
		ATTACH_ENTITY_TO_ENTITY(sRetVal.oTarget, oGridObj, 0, vOffset, <<0, 0, 0>>)  //CI_GRID_TARGET_RED_ORI+25>>)
	ELSE
		//SET_ENTITY_HEADING(sRetVal.oTarget, CI_GRID_TARGET_BLUE_ORI)
		ATTACH_ENTITY_TO_ENTITY(sRetVal.oTarget, oGridObj, 0, vOffset, <<0, 0, 180>>)  //CI_GRID_TARGET_BLUE_ORI-25>>)
	ENDIF
	
	//SET_ENTITY_INVINCIBLE(sRetVal.oTarget, TRUE)
	SET_BITMASK_AS_ENUM(sRetVal.iFlags, eColor)
	sRetVal.fFlipTime = 0.0

	RETURN sRetVal
ENDFUNC

#IF IS_DEBUG_BUILD
	PROC RANGE_DISPLAY_ONSCREEN_TEXT(STRING sPrint, FLOAT displayY, FLOAT displayX=0.05, FLOAT scaleX=0.3, FLOAT scaleY=0.5)
		SET_TEXT_FONT(FONT_STANDARD)
		SET_TEXT_SCALE(scaleX, scaleY)
		SET_TEXT_COLOUR(255, 255, 0, 255)
		DISPLAY_TEXT_WITH_LITERAL_STRING(displayX, displayY, "STRING", sPrint)
	ENDPROC
#ENDIF


/// PURPOSE:
///    Turns the target around.
/// RETURNS:
///    TRUE when it's done flipping.
FUNC BOOL FLIP_GRID_TARGET(RangeMP_Round & sRndInfo, RangeTarget & sRangeTarget, INT iTarget)
	FLOAT fStart,fEnd
	FLOAT fInterpTime
	
	iTarget = iTarget
	
	// If the target flip time has never been updated, play the sound, because we jsut started flipping.
	IF (sRangeTarget.fFlipTime = 0.0)
		// Play the sound to flip.
		PLAY_SOUND_FROM_ENTITY(-1, "TARGET_PRACTICE_FLIP_MASTER", sRangeTarget.oTarget)
		CDEBUG2LN(DEBUG_SHOOTRANGE, "FLIP_GRID_TARGET: Played sound to flip target at grid index: ", iTarget)
	ENDIF
	
	// Increment our frame count
	sRangeTarget.fFlipTime += GET_FRAME_TIME()
	
	// Find out how much we should be flipping
	fInterpTime = sRangeTarget.fFlipTime / TARGET_FLIP_TIME
	
	// Keep going until we reach 1.0 (means we are at our destination)
	IF fInterpTime < 1.0
		// Are we turning red or blue?
		IF CHECK_TARGET_FLAG(sRangeTarget, TARG_F_FLIPPING_TO_BLUE)
			fStart = sRndInfo.sGridData.fRedTargetHead
			fEnd = sRndInfo.sGridData.fBlueTargetHead
			CDEBUG2LN(DEBUG_SHOOTRANGE, "Target flipping to blue")
		ELSE
			fStart = sRndInfo.sGridData.fBlueTargetHead
			fEnd = sRndInfo.sGridData.fRedTargetHead
			CDEBUG2LN(DEBUG_SHOOTRANGE, "Target flipping to red")
		ENDIF
		IF NOT DOES_ENTITY_EXIST(sRangeTarget.oTarget)
			CDEBUG2LN(DEBUG_SHOOTRANGE, "iTarget ", iTarget, " cannot be flipped, failed DOES_ENTITY_EXIST check")
		ENDIF
		IF IS_ENTITY_DEAD(sRangeTarget.oTarget)
			CDEBUG2LN(DEBUG_SHOOTRANGE, "iTarget ", iTarget, " cannot be flipped, IS_ENTITY_DEAD check returned TRUE")
		ENDIF
		
		CDEBUG2LN(DEBUG_SHOOTRANGE, "Heading before call to set: ", GET_ENTITY_HEADING(sRangeTarget.oTarget))
		SET_ENTITY_HEADING(sRangeTarget.oTarget, fStart + (fEnd - fStart) * fInterpTime)
		CDEBUG2LN(DEBUG_SHOOTRANGE, "Heading after call to set: ", GET_ENTITY_HEADING(sRangeTarget.oTarget))
		
		CDEBUG2LN(DEBUG_SHOOTRANGE, "fStart:	", fStart)
		CDEBUG2LN(DEBUG_SHOOTRANGE, "fEnd:		", fEnd)
		CDEBUG2LN(DEBUG_SHOOTRANGE, "Calc:		", fStart + (fEnd - fStart) * fInterpTime)
		
		CDEBUG2LN(DEBUG_SHOOTRANGE, "FLIP_GRID_TARGET: Returning False, fInterpTime: ", fInterpTime, " for target at grid index: ", iTarget)
		
		#IF IS_DEBUG_BUILD
			IF RANGE_SHOULD_SHOW_ONSCREEN_TEXT
				FLOAT fAlpha = TO_FLOAT(iTarget)/(MAX_TARGETS - 1)
				FLOAT fYPlacement = LERP_FLOAT(0.037, 0.88, fAlpha)
				TEXT_LABEL_63 texPrint = "Flipping Target "
		  		texPrint += iTarget
				texPrint += ", heading is now "
				texPrint += FLOOR(GET_ENTITY_HEADING(sRangeTarget.oTarget))
				RANGE_DISPLAY_ONSCREEN_TEXT(texPrint, fYPlacement)
			ENDIF
		#ENDIF
		
		RETURN FALSE
	ELSE	
		// Reset our frame time
		sRangeTarget.fFlipTime = 0.0
		
		// Pop it to the other color.
		IF CHECK_TARGET_FLAG(sRangeTarget, TARG_F_FLIPPING_TO_BLUE)
			SET_ENTITY_HEADING(sRangeTarget.oTarget, sRndInfo.sGridData.fBlueTargetHead)
			CLEAR_TARGET_FLAG(sRangeTarget, TARG_F_IS_RED)
			SET_TARGET_FLAG(sRangeTarget, TARG_F_IS_BLUE)
		ELSE
			SET_ENTITY_HEADING(sRangeTarget.oTarget, sRndInfo.sGridData.fRedTargetHead)
			CLEAR_TARGET_FLAG(sRangeTarget, TARG_F_IS_BLUE)
			SET_TARGET_FLAG(sRangeTarget, TARG_F_IS_RED)
		ENDIF
		
		CDEBUG2LN(DEBUG_SHOOTRANGE, "FLIP_GRID_TARGET: Returning TRUE, for target at grid index: ", iTarget)
		
		// Set our entityi to not entering		
		RETURN TRUE
	ENDIF
ENDFUNC


/// PURPOSE:
///    Makes the target behave as it's supposed to on this system.
PROC UPDATE_GRID_TARGET(RangeMP_Round & sRndInfo, RangeTarget & oTarget, INT iTarget)
	// First, see if we're still flipping.
	IF CHECK_TARGET_FLAG(oTarget, TARG_F_FLIPPING_TO_BLUE) OR CHECK_TARGET_FLAG(oTarget, TARG_F_FLIPPING_TO_RED)
		IF FLIP_GRID_TARGET(sRndInfo, oTarget, iTarget)
			// Done flipping!
			CLEAR_TARGET_FLAG(oTarget, TARG_F_FLIPPING_TO_BLUE)
			CLEAR_TARGET_FLAG(oTarget, TARG_F_FLIPPING_TO_RED)
			CDEBUG2LN(DEBUG_SHOOTRANGE, "oTarget at grid index: ", iTarget, " has flipped")
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
			IF RANGE_SHOULD_SHOW_ONSCREEN_TEXT
				FLOAT fAlpha = TO_FLOAT(iTarget)/(MAX_TARGETS - 1)
				FLOAT fYPlacement = LERP_FLOAT(0.037, 0.88, fAlpha)
				TEXT_LABEL_63 texPrint = "Not Flipping Target "
		  		texPrint += iTarget
				RANGE_DISPLAY_ONSCREEN_TEXT(texPrint, fYPlacement)
			ENDIF
		#ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    This is for use with the RANDOM TARGETs round.
///    Given the A1 - D5 GRID_INDEX, gives the vector creation position.
///    This is a pretty expensive function. Try not to invoke it a TON.
/// RETURNS:
///    The vector correcsponding to that grind indes
FUNC VECTOR GET_TARGET_LOC_FROM_GRID_INDEX(RangeMP_CoreData & sCoreInfo, GRID_INDEX eCreateIndex)
	// Hard coding this to save on mem usage.
	//FLOAT rowA_Y = 0.20			//FLOAT col1_X = 0.05
	//FLOAT rowB_Y = 0.40			//FLOAT col2_X = 0.275
	//FLOAT rowC_Y = 0.60			//FLOAT col3_X = 0.50
	//FLOAT rowD_Y = 0.80			//FLOAT col4_X = 0.725
									//FLOAT col5_X = 0.95

	VECTOR vRangeForward = sCoreInfo.vRangeFwdTarget - sCoreInfo.vRangeFwdClose
	DRAW_DEBUG_SPHERE(vRangeForward, 0.25, 255, 0, 0)
	DRAW_DEBUG_SPHERE(sCoreInfo.vRangeFwdTarget, 0.1, 255, 0, 0)
	DRAW_DEBUG_SPHERE(sCoreInfo.vRangeFwdClose, 0.1, 0, 255, 0)
	
	VECTOR vRowColResult, vFwdRowResult
	VECTOR vRowCol1X = sCoreInfo.vRangeFwdClose + sCoreInfo.vRangeOffsetCol1
	VECTOR vRowCol2X = sCoreInfo.vRangeFwdClose + sCoreInfo.vRangeOffsetCol2
	VECTOR vRowCol3X = sCoreInfo.vRangeFwdClose + sCoreInfo.vRangeOffsetCol3
	VECTOR vRowCol4X = sCoreInfo.vRangeFwdClose + sCoreInfo.vRangeOffsetCol4
	VECTOR vRowCol5X = sCoreInfo.vRangeFwdClose + sCoreInfo.vRangeOffsetCol5
	
	// First, get us the fwd for that ROW.
	SWITCH (eCreateIndex)
		CASE gridA1
		CASE gridA2
		CASE gridA3
		CASE gridA4
		CASE gridA5
			vFwdRowResult = vRangeForward * 0.2
		BREAK
		
		CASE gridB1
		CASE gridB2
		CASE gridB3
		CASE gridB4
		CASE gridB5
			vFwdRowResult = vRangeForward * 0.4
		BREAK
		
		CASE gridC1
		CASE gridC2
		CASE gridC3
		CASE gridC4
		CASE gridC5
			vFwdRowResult = vRangeForward * 0.6
		BREAK
		
		CASE gridD1
		CASE gridD2
		CASE gridD3
		CASE gridD4
		CASE gridD5
			vFwdRowResult = vRangeForward * 0.8
		BREAK
	ENDSWITCH
	
	// Now multiply our fwdRow result by the column position to find our point.
	SWITCH (eCreateIndex)
		CASE gridA1
		CASE gridB1
		CASE gridC1
		CASE gridD1
			vRowColResult = vRowCol1X + vFwdRowResult
		BREAK
		
		CASE gridA2
		CASE gridB2
		CASE gridC2
		CASE gridD2
			vRowColResult = vRowCol2X + vFwdRowResult
		BREAK
		
		CASE gridA3
		CASE gridB3
		CASE gridC3
		CASE gridD3
			vRowColResult = vRowCol3X + vFwdRowResult
		BREAK
		
		CASE gridA4
		CASE gridB4
		CASE gridC4
		CASE gridD4
			vRowColResult = vRowCol4X + vFwdRowResult
		BREAK
		
		CASE gridA5
		CASE gridB5
		CASE gridC5
		CASE gridD5
			vRowColResult = vRowCol5X + vFwdRowResult
		BREAK
	ENDSWITCH

	RETURN vRowColResult
ENDFUNC

/// PURPOSE:
///    Creates a target for the RANDOM round.
PROC CREATE_RANDOM_TARGET(RangeMP_CoreData & sCoreInfo, RangeMP_Round & sRndInfo, RangeTarget & sToFillOut, GRID_INDEX eGridToSpawnIn, BOOL bSpawnBlue = TRUE)	
	VECTOR vTargetPos = GET_TARGET_LOC_FROM_GRID_INDEX(sCoreInfo, eGridToSpawnIn)
	sToFillOut.eGridLoc = eGridToSpawnIn
	sToFillOut.iFlags = 0
	
	// Create a medium height target.
	OBJECT_INDEX oBacking = CREATE_OBJECT_NO_OFFSET(PROP_TARGET_BACKBOARD_B, vTargetPos, FALSE, FALSE)

	// Create the target itself.
	IF bSpawnBlue
		sToFillOut.oTarget = GET_ENTITY_FROM_PED_OR_VEHICLE(CREATE_OBJECT_NO_OFFSET(PROP_TARGET_ORA_PURP_01, vTargetPos, FALSE, FALSE))
		ATTACH_ENTITY_TO_ENTITY(sToFillOut.oTarget, oBacking, 0, <<0.0,-0.04,-0.410>>, <<0.0,0.0,180.0>>)
		SET_TARGET_FLAG(sToFillOut, TARG_F_IS_BLUE)
	ELSE
		sToFillOut.oTarget = GET_ENTITY_FROM_PED_OR_VEHICLE(CREATE_OBJECT_NO_OFFSET(PROP_TARGET_ORA_PURP_01, vTargetPos, FALSE, FALSE))
		ATTACH_ENTITY_TO_ENTITY(sToFillOut.oTarget, oBacking, 0, <<0.0,-0.04,-0.410>>, <<0.0,0.0,0.0>>)
		SET_TARGET_FLAG(sToFillOut, TARG_F_IS_RED)
	ENDIF

	OBJECT_INDEX oArm = CREATE_OBJECT_NO_OFFSET(PROP_TARGET_ARM, vTargetPos, FALSE, FALSE)
	ATTACH_ENTITY_TO_ENTITY(oBacking, oArm, 0, <<0.0,0.007,-0.760>>, <<0.0,0.0,180.0>>)
	
	SET_ENTITY_ROTATION(oArm, sRndInfo.sRandData.vStartRot)	//<<-90.0,0.0,160.0>>)
	
	FREEZE_ENTITY_POSITION(oArm, TRUE)
	SET_ENTITY_INVINCIBLE(oArm, TRUE)
	SET_ENTITY_INVINCIBLE(oBacking, TRUE)
	
	sToFillOut.oMisc = GET_ENTITY_FROM_PED_OR_VEHICLE(oArm)
	sToFillOut.fFlipTime = 0.0
ENDPROC

/// PURPOSE:
///    Rotates in a target for the RANDOM round. This function rotates it around the X.
/// RETURNS:
///    TRUE when done rotating.
FUNC BOOL ROTATE_RANDOM_TARGET(RangeMP_Round & sRndInfo, RangeTarget & sRangeTarget, BOOL bEntering = TRUE, BOOL bPlaySound = TRUE)
	VECTOR vStart, vEnd
	
	// If the target flip time has never been updated, play the sound, because we just started flipping.
	IF bPlaySound
		IF (sRangeTarget.fFlipTime = 0.0)
			// Play the sound to flip.
			PLAY_SOUND_FROM_ENTITY(-1, "TARGET_PRACTICE_FLIP_MASTER", sRangeTarget.oTarget)
		ENDIF
	ENDIF
	
	// Increment our frame count
	sRangeTarget.fFlipTime += GET_FRAME_TIME()
	FLOAT fInterpTime = sRangeTarget.fFlipTime / CONST_ROT_TIME_ENTER

	// Keep going until we reach 1.0 (means we are at our destination)
	IF fInterpTime < 1.0
		// Are we coming in or going out..
		IF bEntering
			vEnd = sRndInfo.sRandData.vEndRot
			vStart = sRndInfo.sRandData.vStartRot
		ELSE
			vStart = sRndInfo.sRandData.vEndRot
			vEnd = sRndInfo.sRandData.vStartRot
		ENDIF		
		SET_ENTITY_ROTATION(sRangeTarget.oMisc, vStart + (vEnd - vStart) * fInterpTime)
				
		RETURN FALSE
	ELSE	
		// Pop em
		IF bEntering
			SET_ENTITY_ROTATION(sRangeTarget.oMisc, sRndInfo.sRandData.vEndRot)
			CDEBUG2LN(DEBUG_SHOOTRANGE, "RotateArm -- (ENTERING) Finished Rotating!!")	
		ELSE
			SET_ENTITY_ROTATION(sRangeTarget.oMisc, sRndInfo.sRandData.vStartRot)
			CDEBUG2LN(DEBUG_SHOOTRANGE, "RotateArm -- (EXITING) Finished Rotating!!")	
		ENDIF
		
		// Reset our frame time
		sRangeTarget.fFlipTime = 0.0		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE	
ENDFUNC


/// PURPOSE:
///    Makes the target behave as it's supposed to on this system.
PROC UPDATE_RANDOM_TARGET(RangeMP_Round & sRndInfo, RangeTarget & oTarget)
	// First, see if we're still flipping.
	IF CHECK_TARGET_FLAG(oTarget, TARG_F_ROTATING_OUT)
		IF ROTATE_RANDOM_TARGET(sRndInfo, oTarget, FALSE)			
			// Done flipping!
			CLEAR_TARGET_FLAG(oTarget, TARG_F_ROTATING_OUT)
			CLEAR_TARGET_FLAG(oTarget, TARG_F_IS_BLUE)
			CLEAR_TARGET_FLAG(oTarget, TARG_F_IS_RED)
			
			// Clean.
			IF NOT IS_ENTITY_DEAD(oTarget.oTarget)
				ENTITY_INDEX oBacking = GET_ENTITY_ATTACHED_TO(oTarget.oTarget)
				IF NOT IS_ENTITY_DEAD(oBacking)
					DELETE_ENTITY(oBacking)
				ENDIF
				DELETE_ENTITY(oTarget.oTarget)
			ENDIF
		
			IF NOT IS_ENTITY_DEAD(oTarget.oMisc)
				DELETE_ENTITY(oTarget.oMisc)
			ENDIF
		ENDIF
		
	ELIF CHECK_TARGET_FLAG(oTarget, TARG_F_ROTATING_IN)
		IF ROTATE_RANDOM_TARGET(sRndInfo, oTarget, TRUE)
			// Done flipping!
			CLEAR_TARGET_FLAG(oTarget, TARG_F_ROTATING_IN)
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE:
///    This function creates one of the control targets the are in the center of the range, which are used
///    to cover/uncover the other targets.
/// PARAMS:
///    oTarget - The target that this function will create and return by reference.
///    iVerticalSlot - There are going to be 2-4 vertical slots going down the middle (haven't decided how many yet...)
///    						this specifies which one to put the target in.
///    bSpawnBlue - Should this target be blue? (Or Red.)
PROC CREATE_COVERED_ROUND_CONTROL_TARGET(RangeMP_Round & sRndInfo, RangeTarget & oTarget, INT iVerticalSlot, BOOL bSpawnBlue = TRUE)
	// Only real difference is the height.
	VECTOR vTargetPos = sRndInfo.sCovData.vControl0Pos
	IF(iVerticalSlot = 1)
		vTargetPos.z -= 0.65
	ELIF (iVerticalSlot = 2)
		vTargetPos.z -= 1.3
	ELIF (iVerticalSlot = 3)
		vTargetPos.z -= 1.95
	ENDIF
	
	// Reset all flags.
	oTarget.iFlags = 0
	
	// Create a medium height target.
	OBJECT_INDEX oBacking = CREATE_OBJECT_NO_OFFSET(PROP_TARGET_BACKBOARD_B, vTargetPos, FALSE, FALSE)

	// Create the target itself.
	// Covered targets get both a front and a back, since they're control.
	// Was RED BLUE RED BLUE
	IF bSpawnBlue
		// Blue side showing gets flagged.
		oTarget.oTarget = CREATE_OBJECT(PROP_TARGET_RED_CROSS, vTargetPos, FALSE, FALSE)
		CDEBUG2LN(DEBUG_SHOOTRANGE, "DOES_ENTITY_EXIST(oTarget)=", DOES_ENTITY_EXIST(oTarget.oTarget), ", DOES_ENTITY_EXIST(oBacking)=", DOES_ENTITY_EXIST(oBacking))
		ATTACH_ENTITY_TO_ENTITY(oTarget.oTarget, oBacking, 0, <<0.0,-0.03,-0.410>>, <<0.0,90.0,180.0>>)
		SET_TARGET_FLAG(oTarget, TARG_F_IS_BLUE)
		
		// Red side on back does not get flagged.
		oTarget.oTarget2 = GET_ENTITY_FROM_PED_OR_VEHICLE(CREATE_OBJECT_NO_OFFSET(PROP_TARGET_RED_CROSS, vTargetPos, FALSE, FALSE))
		ATTACH_ENTITY_TO_ENTITY(oTarget.oTarget2, oBacking, 0, <<0.0,0.03,-0.410>>, <<0.0,90.0,0.0>>)
	ELSE
		// Red side showing gets flagged.
		oTarget.oTarget2 = GET_ENTITY_FROM_PED_OR_VEHICLE(CREATE_OBJECT_NO_OFFSET(PROP_TARGET_RED_CROSS, vTargetPos, FALSE, FALSE))
		ATTACH_ENTITY_TO_ENTITY(oTarget.oTarget2, oBacking, 0, <<0.0,-0.03,-0.410>>, <<0.0,90.0,0.0>>)
		SET_TARGET_FLAG(oTarget, TARG_F_IS_RED)
		
		// Blue side on back does not get flagged.
		oTarget.oTarget = GET_ENTITY_FROM_PED_OR_VEHICLE(CREATE_OBJECT_NO_OFFSET(PROP_TARGET_RED_CROSS, vTargetPos, FALSE, FALSE))
		ATTACH_ENTITY_TO_ENTITY(oTarget.oTarget, oBacking, 0, <<0.0,0.03,-0.410>>, <<0.0,90.0,0.0>>)
	ENDIF

	OBJECT_INDEX oArm = CREATE_OBJECT_NO_OFFSET(PROP_TARGET_ARM_SM, vTargetPos, FALSE, FALSE)
	ATTACH_ENTITY_TO_ENTITY(oBacking, oArm, 0, <<0.0,0.007,-0.450>>, <<0.0,0.0,180.0>>)
	
	IF bSpawnBlue
		SET_ENTITY_ROTATION(oArm, sRndInfo.sCovData.vBlueCtrlStartRot)
	ELSE
		SET_ENTITY_ROTATION(oArm, sRndInfo.sCovData.vRedCtrlStartRot)
	ENDIF
	
	FREEZE_ENTITY_POSITION(oArm, TRUE)
	SET_ENTITY_INVINCIBLE(oArm, TRUE)
	SET_ENTITY_INVINCIBLE(oBacking, TRUE)
	
	oTarget.oMisc = GET_ENTITY_FROM_PED_OR_VEHICLE(oArm)
	oTarget.fFlipTime = 0.0
ENDPROC


/// PURPOSE:
///    Responsible for making the control targets rotate around the center tree.
/// RETURNS:
///    TRUE when done.
FUNC BOOL FLIP_CONTROL_TARGET(RangeMP_Round & sRndInfo, RangeTarget & sRangeTarget, INT iTargIndex)
	VECTOR vStart, vEnd
	
	// Are we coming in or going out..
	// All of these verified working as of 11/21/2011 - RP
	IF (iTargIndex = 0) OR (iTargIndex = 2)
		IF CHECK_TARGET_FLAG(sRangeTarget, TARG_F_IS_RED)
			vStart = sRndInfo.sCovData.vCtrlRedBlueStartRot
			vEnd = sRndInfo.sCovData.vCtrlRedBlueEndRot
		ELSE
			vStart = sRndInfo.sCovData.vCtrlRedBlueEndRot
			vEnd = sRndInfo.sCovData.vCtrlRedBlueStartRot
		ENDIF
	ELSE
		IF CHECK_TARGET_FLAG(sRangeTarget, TARG_F_IS_RED)
			// This one works.
			vStart = sRndInfo.sCovData.vCtrl2RedBlueStartRot
			vEnd = sRndInfo.sCovData.vCtrl2RedBlueEndRot
		ELSE
			vStart = sRndInfo.sCovData.vCtrl2RedBlueEndRot
			vEnd = sRndInfo.sCovData.vCtrl2RedBlueStartRot
		ENDIF
	ENDIF		
		
		
	// If the target flip time has never been updated, play the sound, because we just started flipping.
	IF (sRangeTarget.fFlipTime = 0.0)
		// Play the sound to flip.
		PLAY_SOUND_FROM_ENTITY(-1, "TARGET_PRACTICE_FLIP_MASTER", sRangeTarget.oTarget)
	ENDIF

	// Increment our frame count
	sRangeTarget.fFlipTime += GET_FRAME_TIME()
	FLOAT fInterpTime = sRangeTarget.fFlipTime / CONST_COVERED_ROTATE_TIME

	// Keep going until we reach 1.0 (means we are at our destination)
	IF fInterpTime < 1.0
		SET_ENTITY_ROTATION(sRangeTarget.oMisc, vStart + (vEnd - vStart) * fInterpTime)
				
		RETURN FALSE
	ELSE	
		// Pop em
		SET_ENTITY_ROTATION(sRangeTarget.oMisc, vEnd)
		CDEBUG2LN(DEBUG_SHOOTRANGE, "RotateArm -- (ENTERING) Finished Rotating!!")	

		// Reset our frame time
		sRangeTarget.fFlipTime = 0.0		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE	
ENDFUNC


/// PURPOSE:
///    Handles the targets flipping when shot.
PROC UPDATE_CONTROL_TARGET(RangeMP_Round & sRndInfo, RangeTarget & oTarget, INT iTargIndex)
	IF CHECK_TARGET_FLAG(oTarget, TARG_F_FLIPPING_TO_BLUE)
		IF FLIP_CONTROL_TARGET(sRndInfo, oTarget, iTargIndex)
			CLEAR_TARGET_FLAG(oTarget, TARG_F_FLIPPING_TO_BLUE)
			CLEAR_TARGET_FLAG(oTarget, TARG_F_IS_RED)
			SET_TARGET_FLAG(oTarget, TARG_F_IS_BLUE)
		ENDIF
		
	ELIF CHECK_TARGET_FLAG(oTarget, TARG_F_FLIPPING_TO_RED)
		IF FLIP_CONTROL_TARGET(sRndInfo, oTarget, iTargIndex)
			CLEAR_TARGET_FLAG(oTarget, TARG_F_FLIPPING_TO_RED)
			CLEAR_TARGET_FLAG(oTarget, TARG_F_IS_BLUE)
			SET_TARGET_FLAG(oTarget, TARG_F_IS_RED)
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE:
///    
/// PARAMS:
///    oTarget - the target this function create, returned by ref.
///    iRow - the row to create the target in (0-3)
///    iCol - the column to create the target in (0-7)
///    bSpawnBlue - should this be a blue target?
PROC CREATE_COVERED_ROUND_GOAL_TARGET(RangeMP_Round & sRndInfo, RangeTarget & oTarget, INT iRow, INT iCol, BOOL bSpawnBlue = TRUE)
	// Reset all flags.
	oTarget.iFlags = 0
	
	// This is the top-left taget pos.
	VECTOR vTargetPos = sRndInfo.sCovData.vTopLeftTargPos
	
	// If we're on the other side of the divide, account for that.
	IF (iCol > 3)
		iCol -= 4
		vTargetPos.x += sRndInfo.sCovData.fGoal_DivXOff
		vTargetPos.y += sRndInfo.sCovData.fGoal_DivYOff
	ENDIF
	
	// Account for Cols.
	vTargetPos.x += (iCol * sRndInfo.sCovData.fGoalXOffset)
	vTargetPos.y += (iCol * sRndInfo.sCovData.fGoalYOffset)
	
	// Account for Rows.
	vTargetPos.z += (iRow * -0.655)	
	
	// Create a target with backing.
	OBJECT_INDEX oBacking = CREATE_OBJECT_NO_OFFSET(PROP_TARGET_BACKBOARD_B, vTargetPos, FALSE, FALSE)
	SET_ENTITY_ROTATION(oBacking, sRndInfo.sCovData.vBlockedStartRot)
	
	// Create the target itself.
	// Covered targets get both a front and a back, since they're control.
	IF bSpawnBlue
		// Blue side showing gets flagged.
		oTarget.oTarget = GET_ENTITY_FROM_PED_OR_VEHICLE(CREATE_OBJECT_NO_OFFSET(PROP_TARGET_ORA_PURP_01, vTargetPos, FALSE, FALSE))
		ATTACH_ENTITY_TO_ENTITY(oTarget.oTarget, oBacking, 0, <<0.0,-0.03,-0.410>>, <<0.0,0.0,180.0>>)
		SET_TARGET_FLAG(oTarget, TARG_F_IS_BLUE)
		
		// Certain targets are blocked at start.
		IF (iRow = 0) OR (iRow = 2)
			// Targets are covered!
			SET_ENTITY_ROTATION(oBacking, sRndInfo.sCovData.vBlockedStartRot)
			SET_TARGET_FLAG(oTarget, TARG_F_BLOCKED)
		ELSE
			SET_ENTITY_ROTATION(oBacking, sRndInfo.sCovData.vUnblockedStartRot)
			CLEAR_TARGET_FLAG(oTarget, TARG_F_BLOCKED)
		ENDIF
	ELSE
		// Red side showing gets flagged.
		oTarget.oTarget = GET_ENTITY_FROM_PED_OR_VEHICLE(CREATE_OBJECT_NO_OFFSET(PROP_TARGET_ORA_PURP_01, vTargetPos, FALSE, FALSE))
		ATTACH_ENTITY_TO_ENTITY(oTarget.oTarget, oBacking, 0, <<0.0,-0.03,-0.410>>, <<0.0,0.0,0.0>>)
		SET_TARGET_FLAG(oTarget, TARG_F_IS_RED)
		
		// Certain targets are blocked at start.
		IF (iRow = 1) OR (iRow = 3)
			// Targets are covered!
			SET_ENTITY_ROTATION(oBacking, sRndInfo.sCovData.vBlockedStartRot)
			SET_TARGET_FLAG(oTarget, TARG_F_BLOCKED)
		ELSE
			SET_ENTITY_ROTATION(oBacking, sRndInfo.sCovData.vUnblockedStartRot)
			CLEAR_TARGET_FLAG(oTarget, TARG_F_BLOCKED)
		ENDIF
	ENDIF
	
	FREEZE_ENTITY_POSITION(oBacking, TRUE)
	SET_ENTITY_INVINCIBLE(oBacking, TRUE)
	
	oTarget.oMisc = GET_ENTITY_FROM_PED_OR_VEHICLE(oBacking)
	oTarget.fFlipTime = 0.0
ENDPROC

/// PURPOSE:
///    Responsible for making the control targets rotate around the center tree.
/// RETURNS:
///    TRUE when done.
FUNC BOOL ROTATE_GOAL_TARGET(RangeMP_Round & sRndInfo, RangeTarget & sRangeTarget, BOOL bRotateToFace)
	VECTOR vStart, vEnd
	
	// Are we coming in or going out..
	IF (bRotateToFace)
		vStart = sRndInfo.sCovData.vBlockedStartRot
		vEnd = sRndInfo.sCovData.vUnblockedStartRot
	ELSE
		vStart = sRndInfo.sCovData.vUnblockedStartRot
		vEnd = sRndInfo.sCovData.vBlockedStartRot
	ENDIF		
		
	// If the target flip time has never been updated, play the sound, because we just started flipping.
	IF (sRangeTarget.fFlipTime = 0.0)
		// Play the sound to flip.
		PLAY_SOUND_FROM_ENTITY(-1, "TARGET_PRACTICE_FLIP_MASTER", sRangeTarget.oTarget)
	ENDIF

	// Increment our frame count
	sRangeTarget.fFlipTime += GET_FRAME_TIME()
	FLOAT fInterpTime = sRangeTarget.fFlipTime / CONST_COVERED_ROTATE_TIME

	// Keep going until we reach 1.0 (means we are at our destination)
	IF fInterpTime < 1.0
		SET_ENTITY_ROTATION(sRangeTarget.oMisc, vStart + (vEnd - vStart) * fInterpTime)
				
		RETURN FALSE
	ELSE	
		// Pop em
		SET_ENTITY_ROTATION(sRangeTarget.oMisc, vEnd)
		CDEBUG2LN(DEBUG_SHOOTRANGE, "RotateArm -- (ENTERING) Finished Rotating!!")	

		// Reset our frame time
		sRangeTarget.fFlipTime = 0.0		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE	
ENDFUNC

/// PURPOSE:
///    Makes a goal target leave the range.
/// RETURNS:
///    TRUE when done.
FUNC BOOL REMOVE_GOAL_TARGET(RangeMP_Round & sRndInfo, RangeTarget & sRangeTarget)
	VECTOR vStart, vEnd
	vStart = sRndInfo.sCovData.vUnblockedStartRot
	vEnd = sRndInfo.sCovData.vDestroyedEndRot
		
	// If the target flip time has never been updated, play the sound, because we just started flipping.
	IF (sRangeTarget.fFlipTime = 0.0)
		// Play the sound to flip.
		PLAY_SOUND_FROM_ENTITY(-1, "TARGET_PRACTICE_FLIP_MASTER", sRangeTarget.oTarget)
	ENDIF

	// Increment our frame count
	sRangeTarget.fFlipTime += GET_FRAME_TIME()
	FLOAT fInterpTime = sRangeTarget.fFlipTime / CONST_COVERED_ROTATE_TIME
	
	// Keep going until we reach 1.0 (means we are at our destination)
	IF fInterpTime < 1.0
		SET_ENTITY_ROTATION(sRangeTarget.oMisc, vStart + (vEnd - vStart) * fInterpTime)
				
		RETURN FALSE
	ELSE	
		// Pop em
		SET_ENTITY_ROTATION(sRangeTarget.oMisc, vEnd)
		CDEBUG2LN(DEBUG_SHOOTRANGE, "RotateArm -- (ENTERING) Finished Rotating!!")	

		// Reset our frame time
		sRangeTarget.fFlipTime = 0.0		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE	
ENDFUNC


/// PURPOSE:
///     Handles the rotations of the actual targets.
/// PARAMS:
///    oTarget - 
PROC UPDATE_GOAL_TARGET(RangeMP_Round & sRndInfo, RangeTarget & oTarget, INT iTargLoc, 
			BOOL bBlockR1IsBlue, BOOL bBlockR2IsBlue, BOOL bBlockR3IsBlue, BOOL bBlockR4IsBlue)
	// First, if we're done, do nothing.
	IF CHECK_TARGET_FLAG(oTarget, TARG_F_DONE)
		EXIT
	ENDIF
	
	// First, we need to see if the control target for this target is marked as blocked.
	// figure out which row we're in.
	SWITCH (INT_TO_ENUM(CI_COVERED_OBJECT_INDEXES, iTargLoc))
		CASE CI_GOAL_1_1
		CASE CI_GOAL_1_2
		CASE CI_GOAL_1_3
		CASE CI_GOAL_1_4
			// First row. Left side.
			// If the blocking target is red, and we're not blocked or rotating out, rotate us out.
			IF NOT bBlockR1IsBlue AND NOT CHECK_TARGET_FLAG(oTarget, TARG_F_ROTATING_OUT) AND NOT CHECK_TARGET_FLAG(oTarget, TARG_F_BLOCKED)
				SET_TARGET_FLAG(oTarget, TARG_F_ROTATING_OUT)
			ELIF bBlockR1IsBlue
				// If the lock target is the other color and we're blocks, we're not supposed to be! Rotate in.
				IF CHECK_TARGET_FLAG(oTarget, TARG_F_BLOCKED)
					SET_TARGET_FLAG(oTarget, TARG_F_ROTATING_IN)
				ENDIF
			ENDIF
		BREAK
		
		CASE CI_GOAL_1_5
		CASE CI_GOAL_1_6
		CASE CI_GOAL_1_7
		CASE CI_GOAL_1_8
			// First row. Right side.
			// First row. Left side.
			// If the blocking target is blue, and we're not blocked or rotating out, rotate us out.
			IF bBlockR1IsBlue AND NOT CHECK_TARGET_FLAG(oTarget, TARG_F_ROTATING_OUT) AND NOT CHECK_TARGET_FLAG(oTarget, TARG_F_BLOCKED)
				SET_TARGET_FLAG(oTarget, TARG_F_ROTATING_OUT)
			ELIF NOT bBlockR1IsBlue
				// If the lock target is the other color and we're blocks, we're not supposed to be! Rotate in.
				IF CHECK_TARGET_FLAG(oTarget, TARG_F_BLOCKED)
					SET_TARGET_FLAG(oTarget, TARG_F_ROTATING_IN)
				ENDIF
			ENDIF
		BREAK
		
		CASE CI_GOAL_2_1
		CASE CI_GOAL_2_2
		CASE CI_GOAL_2_3
		CASE CI_GOAL_2_4
			// Second row. Left side.
			// If the blocking target is red, and we're not blocked or rotating out, rotate us out.
			IF NOT bBlockR2IsBlue AND NOT CHECK_TARGET_FLAG(oTarget, TARG_F_ROTATING_OUT) AND NOT CHECK_TARGET_FLAG(oTarget, TARG_F_BLOCKED)
				SET_TARGET_FLAG(oTarget, TARG_F_ROTATING_OUT)
			ELIF bBlockR2IsBlue
				// If the lock target is the other color and we're blocks, we're not supposed to be! Rotate in.
				IF CHECK_TARGET_FLAG(oTarget, TARG_F_BLOCKED)
					SET_TARGET_FLAG(oTarget, TARG_F_ROTATING_IN)
				ENDIF
			ENDIF
		BREAK
		
		CASE CI_GOAL_2_5
		CASE CI_GOAL_2_6
		CASE CI_GOAL_2_7
		CASE CI_GOAL_2_8
			// Second row. Right side.
			// If the blocking target is blue, and we're not blocked or rotating out, rotate us out.
			IF bBlockR2IsBlue AND NOT CHECK_TARGET_FLAG(oTarget, TARG_F_ROTATING_OUT) AND NOT CHECK_TARGET_FLAG(oTarget, TARG_F_BLOCKED)
				SET_TARGET_FLAG(oTarget, TARG_F_ROTATING_OUT)
			ELIF NOT bBlockR2IsBlue
				// If the lock target is the other color and we're blocks, we're not supposed to be! Rotate in.
				IF CHECK_TARGET_FLAG(oTarget, TARG_F_BLOCKED)
					SET_TARGET_FLAG(oTarget, TARG_F_ROTATING_IN)
				ENDIF
			ENDIF
		BREAK
		
		CASE CI_GOAL_3_1
		CASE CI_GOAL_3_2
		CASE CI_GOAL_3_3
		CASE CI_GOAL_3_4
			// Third row. Left side.
			// If the blocking target is red, and we're not blocked or rotating out, rotate us out.
			IF NOT bBlockR3IsBlue AND NOT CHECK_TARGET_FLAG(oTarget, TARG_F_ROTATING_OUT) AND NOT CHECK_TARGET_FLAG(oTarget, TARG_F_BLOCKED)
				SET_TARGET_FLAG(oTarget, TARG_F_ROTATING_OUT)
			ELIF bBlockR3IsBlue
				// If the lock target is the other color and we're blocks, we're not supposed to be! Rotate in.
				IF CHECK_TARGET_FLAG(oTarget, TARG_F_BLOCKED)
					SET_TARGET_FLAG(oTarget, TARG_F_ROTATING_IN)
				ENDIF
			ENDIF
		BREAK
		
		CASE CI_GOAL_3_5
		CASE CI_GOAL_3_6
		CASE CI_GOAL_3_7
		CASE CI_GOAL_3_8
			// Third row. Right side.
			// If the blocking target is blue, and we're not blocked or rotating out, rotate us out.
			IF bBlockR3IsBlue AND NOT CHECK_TARGET_FLAG(oTarget, TARG_F_ROTATING_OUT) AND NOT CHECK_TARGET_FLAG(oTarget, TARG_F_BLOCKED)
				SET_TARGET_FLAG(oTarget, TARG_F_ROTATING_OUT)
			ELIF NOT bBlockR3IsBlue
				// If the lock target is the other color and we're blocks, we're not supposed to be! Rotate in.
				IF CHECK_TARGET_FLAG(oTarget, TARG_F_BLOCKED)
					SET_TARGET_FLAG(oTarget, TARG_F_ROTATING_IN)
				ENDIF
			ENDIF
		BREAK
		
		CASE CI_GOAL_4_1
		CASE CI_GOAL_4_2
		CASE CI_GOAL_4_3
		CASE CI_GOAL_4_4
			// Fourth row. Left side.
			// If the blocking target is red, and we're not blocked or rotating out, rotate us out.
			IF NOT bBlockR4IsBlue AND NOT CHECK_TARGET_FLAG(oTarget, TARG_F_ROTATING_OUT) AND NOT CHECK_TARGET_FLAG(oTarget, TARG_F_BLOCKED)
				SET_TARGET_FLAG(oTarget, TARG_F_ROTATING_OUT)
			ELIF bBlockR4IsBlue
				// If the lock target is the other color and we're blocks, we're not supposed to be! Rotate in.
				IF CHECK_TARGET_FLAG(oTarget, TARG_F_BLOCKED)
					SET_TARGET_FLAG(oTarget, TARG_F_ROTATING_IN)
				ENDIF
			ENDIF
		BREAK
		
		CASE CI_GOAL_4_5
		CASE CI_GOAL_4_6
		CASE CI_GOAL_4_7
		CASE CI_GOAL_4_8
			// Fourth row. Right side.
			// If the blocking target is blue, and we're not blocked or rotating out, rotate us out.
			IF bBlockR4IsBlue AND NOT CHECK_TARGET_FLAG(oTarget, TARG_F_ROTATING_OUT) AND NOT CHECK_TARGET_FLAG(oTarget, TARG_F_BLOCKED)
				SET_TARGET_FLAG(oTarget, TARG_F_ROTATING_OUT)
			ELIF NOT bBlockR4IsBlue
				// If the lock target is the other color and we're blocks, we're not supposed to be! Rotate in.
				IF CHECK_TARGET_FLAG(oTarget, TARG_F_BLOCKED)
					SET_TARGET_FLAG(oTarget, TARG_F_ROTATING_IN)
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	
	// Three things a target can do: Rotate to face the player, rotate away, and rotate after being shot.
	IF CHECK_TARGET_FLAG(oTarget, TARG_F_FLIP_OUT)
		// Target has been hit, should leave.
		IF REMOVE_GOAL_TARGET(sRndInfo, oTarget)
			CLEAR_TARGET_FLAG(oTarget, TARG_F_FLIP_OUT)
			SET_TARGET_FLAG(oTarget, TARG_F_DONE)
			DELETE_ENTITY(oTarget.oTarget)
			DELETE_ENTITY(oTarget.oMisc)
		ENDIF
		
	ELIF CHECK_TARGET_FLAG(oTarget, TARG_F_ROTATING_IN)
		// Target should rotate to face the player.
		IF ROTATE_GOAL_TARGET(sRndInfo, oTarget, TRUE)
			CLEAR_TARGET_FLAG(oTarget, TARG_F_BLOCKED)
			CLEAR_TARGET_FLAG(oTarget, TARG_F_ROTATING_IN)
		ENDIF
		
	ELIF CHECK_TARGET_FLAG(oTarget, TARG_F_ROTATING_OUT)
		// Target should rotate away from the player.
		IF ROTATE_GOAL_TARGET(sRndInfo, oTarget, FALSE)
			SET_TARGET_FLAG(oTarget, TARG_F_BLOCKED)
			CLEAR_TARGET_FLAG(oTarget, TARG_F_ROTATING_OUT)
		ENDIF
	ENDIF
ENDPROC
