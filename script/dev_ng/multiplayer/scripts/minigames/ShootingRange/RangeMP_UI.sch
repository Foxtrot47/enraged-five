// RangeMP_UI.sch
USING "Minigame_UIInputs.sch"
USING "UIUtil.sch"
USING "screen_placements.sch"
USING "script_usecontext.sch"
USING "RangeMP_Scores.sch"
USING "MP_Scaleform_Functions.sch"
USING "minigame_big_message.sch"
USING "minigame_midsized_message.sch"
USING "leader_board_common.sch"
USING "net_celebration_screen.sch"

CONST_FLOAT QUIT_SCREEN_TIMEOUT			8.0
CONST_FLOAT RANGE_MP_UWIN_OFFSET		0.05
CONST_INT	RANGE_MP_NUM_MENUS			3		// Weapon Cat, Weapons, Challenges
CONST_INT 	RANGE_MP_MENU_TIMEOUT		30
CONST_INT 	RANGE_MP_SCORECARD_TIMEOUT 	30
CONST_INT	RANGE_MP_TABULATE_UPDATES	90
CONST_INT	RANGE_MP_WEAPONS_IN_CAT		10		// arbitrary, can be larger as needed. 10 is currently more than needed.
CONST_FLOAT	RANGE_STATE_PRINT_HEIGHT	0.50


ENUM RANGE_MP_MENU_INDEX
	RANGE_MP_MENU_CATEGORIES = 0,
	RANGE_MP_MENU_WEAPONS,
	RANGE_MP_MENU_CHALLENGES
ENDENUM

STRUCT RangeMP_Menu	
	INT 					iNumElements
	INT 					iCurElement
	WEAPON_TYPE				eWeapons[ RANGE_MP_WEAPONS_IN_CAT ]
ENDSTRUCT

ENUM RANGE_MP_MENU_FLAGS
	RMPM_SHOWING_SCLB				= BIT0,
	RMPM_SCLB_PROFILE_BUTTON_SHOWN	= BIT1
ENDENUM

FUNC STRING GET_STRING_FROM_RANGE_MP_MENU_FLAG(RANGE_MP_MENU_FLAGS eFlag)
	SWITCH eFlag
		CASE RMPM_SHOWING_SCLB					RETURN "RMPM_SHOWING_SCLB"
		CASE RMPM_SCLB_PROFILE_BUTTON_SHOWN		RETURN "RMPM_SCLB_PROFILE_BUTTON_SHOWN"
	ENDSWITCH
	RETURN "Unknown RANGE_MP_MENU FLAG enum"
ENDFUNC

ENUM RANGE_MP_ROUND_MENU
	RANGE_MP_RoundMenu_Init = 0,
	RANGE_MP_RoundMenu_DisplayPrompt,
	RANGE_MP_RoundMenu_DisplaySplash
ENDENUM

#IF IS_DEBUG_BUILD
FUNC STRING GET_STRING_FROM_RANGE_MP_ROUND_MENU(RANGE_MP_ROUND_MENU eMenu)
	SWITCH eMenu
		CASE RANGE_MP_RoundMenu_Init				RETURN "RANGE_MP_RoundMenu_Init"
		CASE RANGE_MP_RoundMenu_DisplayPrompt		RETURN "RANGE_MP_RoundMenu_DisplayPrompt"
		CASE RANGE_MP_RoundMenu_DisplaySplash		RETURN "RANGE_MP_RoundMenu_DisplaySplash"
	ENDSWITCH
	RETURN "unknown RANGE_MP_ROUND_MENU enum"
ENDFUNC
#ENDIF

ENUM RANGE_MP_WAITING_MENU
	RANGE_MP_WaitMenu_Init = 0,
	RANGE_MP_WaitMenu_DisplayPrompt
ENDENUM

CONST_INT MAIN_MENU_FUN_SWITCH_TIME		4000
CONST_INT MAIN_MENU_FUN_ITEMS			5

CONST_INT MAIN_MENU_MAX_RANDOM_CROSSES	10

STRUCT RANGEMP_LOADING_SPINNER
	SCALEFORM_INDEX				spinnerMovie
	SCALEFORM_LOADING_ICON		loadingStruct
ENDSTRUCT

/// PURPOSE: The manipulateable data such as, which menu we're on, etc.
STRUCT RangeMP_MenuData
	RANGE_MP_MENU_INDEX 	eActiveMenu
	RangeMP_Menu			sMenu[RANGE_MP_NUM_MENUS]
	
	LEADERBOARD_PLACEMENT_TOOLS placement
	
	RANGE_MP_MENU_FLAGS		eMenuFlags
	
	structTimer				sMenuTimer
	BOOL					bSwappedMenu10Sec = FALSE
	BOOL 					bAcceptStickInput
	BOOL 					bHasSetSplashMsg
	BOOL					bSetLeaveReason = FALSE
	BOOL					bSetupBettingCard = FALSE
	BOOL					bTransitionActive = FALSE
	
	BOOL					bSetupRandom_Menu = FALSE
	FLOAT					fRandomMenu_X[MAIN_MENU_MAX_RANDOM_CROSSES][2]
	FLOAT					fRandomMenu_Y[MAIN_MENU_MAX_RANDOM_CROSSES][2]
	
	RANGE_MP_ROUND_MENU		eInRoundMenuState = RANGE_MP_RoundMenu_Init
	RANGE_MP_WAITING_MENU	eWaitingMenuState = RANGE_MP_WaitMenu_Init
	
	CELEBRATION_SCREEN_DATA 		sCelebData
	GENERIC_MINIGAME_STAGE			eGenericMGStage
	SCRIPT_SHARD_BIG_MESSAGE		uiShardBM
	BOOL					bTransitionUp
	BOOL					bTransitionOut
	COUNTDOWN_UI			uiCountdown
	
	MEGA_PLACEMENT_TOOLS	uiPlacement
	UI_INPUT_DATA			uiInput
	
	RANGE_MP_FLOATING_SCORE	floatingScores[RANGE_MP_MAX_FS]
	
	SCALEFORM_INDEX			uiLeaderboard
	BOOL					bSCLBUpdated
	
	STRING					sExitString
	TEXT_LABEL_23			texSpinner
	
	FLOAT 					fReticleDelay
	INT 					iMenuHoldDelay
	
	FLOAT					fCachedAspectRatio
ENDSTRUCT



//*******************************************************************************************************************
//***************************************** ACCESSORS AND MUTATORS **************************************************
//*******************************************************************************************************************

PROC SET_RANGE_MP_MENU_FLAG(RangeMP_MenuData &sMenuData, RANGE_MP_MENU_FLAGS eflag)
	CDEBUG2LN(DEBUG_SHOOTRANGE, "SET_RANGE_MENU_FLAG :: ", GET_STRING_FROM_RANGE_MP_MENU_FLAG(eFlag))
	sMenuData.eMenuFlags = sMenuData.eMenuFlags | eFlag
ENDPROC
PROC CLEAR_RANGE_MP_MENU_FLAG(RangeMP_MenuData &sMenuData, RANGE_MP_MENU_FLAGS eflag)
	CDEBUG2LN(DEBUG_SHOOTRANGE, "CLEAR_RANGE_MENU_FLAG :: ", GET_STRING_FROM_RANGE_MP_MENU_FLAG(eFlag))
	sMenuData.eMenuFlags -= sMenuData.eMenuFlags & eFlag
ENDPROC
FUNC BOOL IS_RANGE_MP_MENU_FLAG_SET(RangeMP_MenuData &sMenuData, RANGE_MP_MENU_FLAGS eflag)
	RETURN (ENUM_TO_INT(sMenuData.eMenuFlags) & ENUM_TO_INT(eFlag)) <> 0
ENDFUNC

PROC SET_RANGE_MENU_HOLD_DELAY(RangeMP_MenuData &sMenuData, INT iNewHoldDelay)
//	CDEBUG2LN(DEBUG_SHOOTRANGE, "SET_RANGE_MENU_HOLD_DELAY :: iNewHoldDelay=", iNewHoldDelay)
	sMenuData.iMenuHoldDelay = iNewHoldDelay
ENDPROC

FUNC INT GET_RANGE_MENU_HOLD_DELAY(RangeMP_MenuData &sMenuData)
	RETURN sMenuData.iMenuHoldDelay
ENDFUNC

PROC SET_RANGE_MENU_EXIT_STRING(RangeMP_MenuData &sMenuData, STRING sNewExitString)
	sMenuData.sExitString = sNewExitString
ENDPROC

FUNC STRING GET_RANGE_MENU_EXIT_STRING(RangeMP_MenuData &sMenuData)
	RETURN sMenuData.sExitString
ENDFUNC

FUNC RANGE_MP_MENU_INDEX RANGE_MP_GET_ACTIVE_MENU_INDEX(RangeMP_MenuData & sMenuData)
	RETURN sMenuData.eActiveMenu
ENDFUNC

FUNC INT RANGE_MP_GET_MENU_CURRENT_ELEMENT(RangeMP_Menu & sMenu)
	RETURN sMenu.iCurElement
ENDFUNC

PROC RANGE_MP_SET_MENU_CURRENT_ELEMENT(RangeMP_Menu & sMenu, INT iElementIndex)
	CDEBUG2LN(DEBUG_SHOOTRANGE, "[rangemp_ui.sch->RANGE_MP_SET_MENU_CURRENT_ELEMENT] Setting to ", iElementIndex)
	sMenu.iCurElement = iElementIndex
ENDPROC

/// PURPOSE:
///    Wraps the commands for start the spinner text command
/// PARAMS:
///    sText - 
PROC RANGE_START_BUSYSPINNER(RangeMP_MenuData & sMenuInfo)
	CDEBUG2LN(DEBUG_SHOOTRANGE, "RANGE_START_BUSYSPINNER :: spinner 5, sMenuInfo.texSpinner=", sMenuInfo.texSpinner)
	BEGIN_TEXT_COMMAND_BUSYSPINNER_ON(sMenuInfo.texSpinner)
	END_TEXT_COMMAND_BUSYSPINNER_ON(5)
ENDPROC

PROC RANGE_SET_BUSYSPINNER_STRING(RangeMP_MenuData & sMenuInfo, STRING sText)
	DEBUG_PRINTCALLSTACK()
	CDEBUG2LN(DEBUG_SHOOTRANGE, "RANGE_SET_BUSYSPINNER_STRING :: sText=", sText)
	sMenuInfo.texSpinner = sText
ENDPROC

PROC RANGE_BUSYSPINNER_OFF()
	IF BUSYSPINNER_IS_ON()
		CDEBUG2LN(DEBUG_SHOOTRANGE, "RANGE_BUSYSPINNER_OFF")
		// Turn it off
		BUSYSPINNER_OFF()
		// To keep it in memory and appear instantly
		PRELOAD_BUSYSPINNER()
	ENDIF
ENDPROC

FUNC STRING GET_STRING_FROM_RANGE_WEAPON_TYPE(WEAPON_TYPE eWeapon)
	SWITCH eWeapon
		CASE WEAPONTYPE_PISTOL				RETURN "WEAPONTYPE_PISTOL"
		CASE WEAPONTYPE_COMBATPISTOL		RETURN "WEAPONTYPE_COMBATPISTOL"
		CASE WEAPONTYPE_APPISTOL			RETURN "WEAPONTYPE_APPISTOL"
		CASE WEAPONTYPE_MICROSMG			RETURN "WEAPONTYPE_MICROSMG"
		CASE WEAPONTYPE_SMG					RETURN "WEAPONTYPE_SMG"
		CASE WEAPONTYPE_ASSAULTRIFLE		RETURN "WEAPONTYPE_ASSAULTRIFLE"
		CASE WEAPONTYPE_CARBINERIFLE		RETURN "WEAPONTYPE_CARBINERIFLE"
		CASE WEAPONTYPE_ADVANCEDRIFLE		RETURN "WEAPONTYPE_ADVANCEDRIFLE"
		CASE WEAPONTYPE_PUMPSHOTGUN			RETURN "WEAPONTYPE_PUMPSHOTGUN"
		CASE WEAPONTYPE_SAWNOFFSHOTGUN		RETURN "WEAPONTYPE_SAWNOFFSHOTGUN"
		CASE WEAPONTYPE_ASSAULTSHOTGUN		RETURN "WEAPONTYPE_ASSAULTSHOTGUN"
		CASE WEAPONTYPE_MG					RETURN "WEAPONTYPE_MG"
		CASE WEAPONTYPE_COMBATMG			RETURN "WEAPONTYPE_COMBATMG"
		CASE WEAPONTYPE_MINIGUN				RETURN "WEAPONTYPE_MINIGUN"
		CASE WEAPONTYPE_DLC_SNSPISTOL		RETURN "WEAPONTYPE_DLC_SNSPISTOL"
		CASE WEAPONTYPE_DLC_GUSENBERG		RETURN "WEAPONTYPE_DLC_GUSENBERG"
		CASE WEAPONTYPE_DLC_BULLPUPRIFLE	RETURN "WEAPONTYPE_DLC_BULLPUPRIFLE"
		CASE WEAPONTYPE_DLC_HEAVYPISTOL		RETURN "WEAPONTYPE_DLC_HEAVYPISTOL"
		CASE WEAPONTYPE_DLC_SPECIALCARBINE	RETURN "WEAPONTYPE_DLC_SPECIALCARBINE"
		CASE WEAPONTYPE_DLC_HEAVYRIFLE		RETURN "WEAPONTYPE_DLC_HEAVYRIFLE"
		CASE WEAPONTYPE_DLC_ASSAULTMG		RETURN "WEAPONTYPE_DLC_ASSAULTMG"
		CASE WEAPONTYPE_DLC_BULLPUPSHOTGUN	RETURN "WEAPONTYPE_DLC_BULLPUPSHOTGUN"
		CASE WEAPONTYPE_DLC_ASSAULTSMG		RETURN "WEAPONTYPE_DLC_ASSAULTSMG"
		CASE WEAPONTYPE_DLC_PISTOL50		RETURN "WEAPONTYPE_DLC_PISTOL50"
		CASE WEAPONTYPE_DLC_VINTAGEPISTOL	RETURN "WEAPONTYPE_DLC_VINTAGEPISTOL"
	ENDSWITCH
	RETURN "Unknown Weapon Enum"
ENDFUNC





