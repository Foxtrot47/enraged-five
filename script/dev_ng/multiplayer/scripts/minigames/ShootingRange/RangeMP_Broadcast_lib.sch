// RangeMP_Broadcast_lib.sch
USING "RangeMP_Player.sch"

STRUCT RANGE_MP_EVENT
	STRUCT_EVENT_COMMON_DETAILS Details
	INT	iTargetHitIndex					// Also used by RANDOM to sa where a target should spawn.
	BOOL bBlueColor						// Used by the server in RANDOM to denote target color.
	INT iSlotToTake						// Used by RANDOM to tell the receiving client where to spawn a target.
	
	INT iShotsFired
	INT iShotsHit
	INT iShotsMissed
	INT iTargetsTurned
	FLOAT fGeneric
	
	INT iHitByClientID					// Used to track who hit the target.
ENDSTRUCT

/// PURPOSE:
///    Sends a message to all clients.
/// PARAMS:
///    sBroadcast - The message to broadcast.
///    iPlayerFlags - the players to send this to. for example: ALL_PLAYERS(), SPECIFIC_PLAYER(), etc
PROC BROADCAST_RANGE_MP_EVENT(RANGE_MP_EVENT & sBroadcast, INT iPlayerFlags)
	IF iPlayerFlags != 0
		SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, sBroadcast, SIZE_OF(sBroadcast), iPlayerFlags)
	ENDIF
	//CDEBUG2LN(DEBUG_SHOOTRANGE, "BROADCAST_RANGE_MP_EVENT event ", PICK_STRING(iParticipants > 1, "sent", "not sent"), ", iParticipants=", iParticipants, ", iPlayerFlags=", iPlayerFlags)
ENDPROC

/// PURPOSE:
///    Let the other player know we quit
/// PARAMS:
///    sPlayerBD - []
PROC BROADCAST_RANGE_QUIT_EVENT(INT iBroadcasterClientID, RangeMP_PlayerBD & sPlayerBD[])
	RANGE_MP_EVENT sToSend
	sToSend.Details.Type = SCRIPT_EVENT_PlayerQuitRange
	iBroadcasterClientID = iBroadcasterClientID
	sPlayerBD[0].iFlags = sPlayerBD[0].iFlags
//	INT iSendTo = 0
//	IF (iBroadcasterClientID = 0)
//		iSendTo = 1
//	ENDIF
		
	BROADCAST_RANGE_MP_EVENT(sToSend, ALL_PLAYERS( FALSE ))
ENDPROC

/// PURPOSE:
///    Broadcasts a target hit message to all players other than the sender.
/// PARAMS:
///    iIndexHit - 
///    iBroadcasterClientID - 
PROC BROADCAST_HIT_TO_OTHER_PLAYERS(INT iIndexHit, INT iBroadcasterClientID, RangeMP_PlayerBD & sPlayerBD[])
	RANGE_MP_EVENT sToSend
	sToSend.Details.Type = SCRIPT_EVENT_TargetStatusChange
	sToSend.iTargetHitIndex = iIndexHit
	sToSend.iHitByClientID = iBroadcasterClientID
	iBroadcasterClientID = iBroadcasterClientID
	sPlayerBD[0].iFlags = sPlayerBD[0].iFlags
//	INT iSendTo = 0
//	IF (iBroadcasterClientID = 0)
//		iSendTo = 1
//	ENDIF
	
	BROADCAST_RANGE_MP_EVENT(sToSend, ALL_PLAYERS( FALSE ))
ENDPROC

/// PURPOSE:
///    Broadcasts our shooting skill to other players.
PROC BROADCAST_SHOOTINGSKILL_TO_OTHER_PLAYER(INT iShootingSkill, INT iBroadcasterClientID, RangeMP_PlayerBD & sPlayerBD[])
	RANGE_MP_EVENT sToSend
	sToSend.Details.Type = SCRIPT_EVENT_Skill
	sToSend.fGeneric = TO_FLOAT(iShootingSkill)
	
	CDEBUG2LN(DEBUG_SHOOTRANGE, "Player ", iBroadcasterClientID, ", broadcasting skill of ", iShootingSkill)
	iBroadcasterClientID = iBroadcasterClientID
	sPlayerBD[0].iFlags = sPlayerBD[0].iFlags
//	INT iSendTo = 0
//	IF (iBroadcasterClientID = 0)
//		iSendTo = 1
//	ENDIF
	
	BROADCAST_RANGE_MP_EVENT(sToSend, ALL_PLAYERS( FALSE ))
ENDPROC

/// PURPOSE:
///    Message that's going to be used by the server in the RANDOM round to spawn targets on clients.
///    WENT A DIFFERENT DIRECTION. STILL HANDY TO COMMUNICATE.
PROC BROADCAST_TARGET_CREATE_TO_ALL_PLAYERS(GRID_INDEX eToSpawnAt, BOOL bIsBlue, RangeMP_PlayerBD & sPlayerBD[])
	CDEBUG2LN(DEBUG_SHOOTRANGE, "Broadcasting target creation.")
	
	RANGE_MP_EVENT sToSend
	sToSend.Details.Type = SCRIPT_EVENT_CreateRandomTarget
	sToSend.iTargetHitIndex = ENUM_TO_INT(eToSpawnAt)
	sToSend.bBlueColor = bIsBlue
	sPlayerBD[0].iFlags = sPlayerBD[0].iFlags
	
	// Send it to everyone. Even us.
	BROADCAST_RANGE_MP_EVENT(sToSend, ALL_PLAYERS())
ENDPROC

/// PURPOSE:
///    Send the hits and shots to the other players.
PROC BROADCAST_SHOOTINGRESULTS_TO_OTHERS_PLAYERS(INT iFired, INT iHits, INT iBroadcasterClientID, RangeMP_PlayerBD & sPlayerBD[])
	CDEBUG2LN(DEBUG_SHOOTRANGE, "Player ", iBroadcasterClientID, " Broadcasting hits/shots.")
	RANGE_MP_EVENT sToSend
	sToSend.Details.Type = SCRIPT_EVENT_Shots
	sToSend.iShotsFired = iFired
	sToSend.iShotsHit = iHits
	iBroadcasterClientID = iBroadcasterClientID
	sPlayerBD[0].iFlags = sPlayerBD[0].iFlags
//	INT iSendTo = 0
//	IF (iBroadcasterClientID = 0)
//		iSendTo = 1
//	ENDIF
	
	// Send it to everyone. Even us.
	BROADCAST_RANGE_MP_EVENT(sToSend, ALL_PLAYERS( FALSE ))
ENDPROC

/// PURPOSE:
///    Send the hits and shots to the other players.
PROC BROADCAST_ROUND_SHOOTINGRESULTS_TO_OTHERS_PLAYERS(INT iFiredThisRound, INT iHitThisRound, INT iMissedThisRound, INT iBroadcasterClientID, 
				RangeMP_PlayerBD & sPlayerBD[])
	CDEBUG2LN(DEBUG_SHOOTRANGE, "Player ", iBroadcasterClientID, " Broadcasting hits/shots this round.")
	RANGE_MP_EVENT sToSend
	
	sToSend.Details.Type = SCRIPT_EVENT_ShotsRound
	sToSend.iShotsFired = iFiredThisRound
	sToSend.iShotsHit = iHitThisRound
	sToSend.iShotsMissed = iMissedThisRound
	iBroadcasterClientID = iBroadcasterClientID
	sPlayerBD[0].iFlags = sPlayerBD[0].iFlags
//	INT iSendTo = 0
//	IF (iBroadcasterClientID = 0)
//		iSendTo = 1
//	ENDIF
	
	// Send it to everyone. Even us.
	BROADCAST_RANGE_MP_EVENT(sToSend, ALL_PLAYERS( FALSE ))
ENDPROC

/// PURPOSE:
///    Message that's going to be used by the server in the RANDOM round to spawn targets on clients.
///    WENT A DIFFERENT DIRECTION. STILL HANDY TO COMMUNICATE.
PROC BROADCAST_TARGET_CREATE_TO_OTHER_PLAYER(GRID_INDEX eToSpawnAt, BOOL bIsBlue, INT iSlot, INT iBroadcasterID, 
			RangeMP_PlayerBD & sPlayerBD[])
	CDEBUG2LN(DEBUG_SHOOTRANGE, "Player ", iBroadcasterID, " Broadcasting target creation.")
	RANGE_MP_EVENT sToSend
	sToSend.Details.Type = SCRIPT_EVENT_CreateRandomTarget
	sToSend.iTargetHitIndex = ENUM_TO_INT(eToSpawnAt)
	sToSend.bBlueColor = bIsBlue
	sToSend.iSlotToTake = iSlot
	iBroadcasterID = iBroadcasterID
	sPlayerBD[0].iFlags = sPlayerBD[0].iFlags
//	INT iSendTo = 0
//	IF (iBroadcasterID = 0)
//		iSendTo = 1
//	ENDIF
	
	// Send it to everyone. Even us.
	BROADCAST_RANGE_MP_EVENT(sToSend, ALL_PLAYERS( FALSE ))
ENDPROC
