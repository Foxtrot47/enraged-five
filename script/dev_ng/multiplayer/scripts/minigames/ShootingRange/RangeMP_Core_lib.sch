// RangeMP_Core_lib.sch
USING "RangeMP_Core.sch"
USING "RangeMP_GridRound_lib.sch"
USING "RangeMP_RandomRound_lib.sch"
USING "RangeMP_CoveredRound_lib.sch"
USING "RangeMP_Round_lib.sch"
USING "RangeMP_Server_lib.sch"
USING "RangeMP_Player_lib.sch"
USING "RangeMP_UI_lib.sch"
USING "clothes_shop_private.sch"
USING "commands_streaming.sch"

/// PURPOSE:
///    
/// PARAMS:
///    sPlayerBD - client broadcast array, []
PROC RANGE_MP_HANDLE_LOCAL_GUSENBERG_AVAILABILITY(RangeMP_PlayerBD &sPlayerBD[])
	
	IF g_sMPTunables.bturnonvalentinesevent
	OR IS_MP_WEAPON_PURCHASED(WEAPONTYPE_DLC_GUSENBERG, -1, TRUE)
	OR IS_MP_WEAPON_EQUIPPED(WEAPONTYPE_DLC_GUSENBERG) 
	OR GET_MP_BOOL_CHARACTER_STAT(GET_WEAPON_AS_GIFT_STAT(WEAPONTYPE_DLC_GUSENBERG) )
	OR HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_DLC_GUSENBERG)
	OR g_sMPTunables.bTURN_ON_VALENTINE_WEAPON	
		SET_CLIENT_FLAG(sPlayerBD[PARTICIPANT_ID_TO_INT()], RANGE_MP_F_GusenbergUnlocked)
		
	ENDIF
	
ENDPROC

FUNC INT RANGE_MP_AWARD_MATCH_RP(RangeMP_ServerBD & sServerBD, INT iClient, BOOL bWinner)
	INT iRPAward = 50									// Participation
	CPRINTLN(DEBUG_SHOOTRANGE, "RANGE_MP_AWARD_MATCH_RP :: iRPAward Participation added, new total: ", iRPAward)
	IF iClient = 0
		iRPAward += sServerBD.iClient0TotalRndWins * 10		// per Round bonus
		CPRINTLN(DEBUG_SHOOTRANGE, "RANGE_MP_AWARD_MATCH_RP :: iRPAward iClient0TotalRndWins added, new total: ", iRPAward)
		iRPAward += sServerBD.iClient0Chal1Wins * 20		// per Challenge bonus
		CPRINTLN(DEBUG_SHOOTRANGE, "RANGE_MP_AWARD_MATCH_RP :: iRPAward iClient0Chal1Wins added, new total: ", iRPAward)
		iRPAward += sServerBD.iClient0Chal2Wins * 20		// per Challenge bonus
		CPRINTLN(DEBUG_SHOOTRANGE, "RANGE_MP_AWARD_MATCH_RP :: iRPAward iClient0Chal2Wins added, new total: ", iRPAward)
		iRPAward += sServerBD.iClient0Chal3Wins * 20		// per Challenge bonus
		CPRINTLN(DEBUG_SHOOTRANGE, "RANGE_MP_AWARD_MATCH_RP :: iRPAward iClient0Chal3Wins added, new total: ", iRPAward)
	ELIF iClient = 1
		iRPAward += sServerBD.iClient1TotalRndWins * 10		// per Round bonus
		CPRINTLN(DEBUG_SHOOTRANGE, "RANGE_MP_AWARD_MATCH_RP :: iRPAward iClient1TotalRndWins added, new total: ", iRPAward)
		iRPAward += sServerBD.iClient1Chal1Wins * 20		// per Challenge bonus
		CPRINTLN(DEBUG_SHOOTRANGE, "RANGE_MP_AWARD_MATCH_RP :: iRPAward iClient1Chal1Wins added, new total: ", iRPAward)
		iRPAward += sServerBD.iClient1Chal2Wins * 20		// per Challenge bonus
		CPRINTLN(DEBUG_SHOOTRANGE, "RANGE_MP_AWARD_MATCH_RP :: iRPAward iClient1Chal2Wins added, new total: ", iRPAward)
		iRPAward += sServerBD.iClient1Chal3Wins * 20		// per Challenge bonus
		CPRINTLN(DEBUG_SHOOTRANGE, "RANGE_MP_AWARD_MATCH_RP :: iRPAward iClient1Chal3Wins added, new total: ", iRPAward)
	ENDIF
	IF bWinner
		iRPAward += 100
		CPRINTLN(DEBUG_SHOOTRANGE, "RANGE_MP_AWARD_MATCH_RP :: iRPAward bWinner added, new total: ", iRPAward)
	ENDIF
	
	iRPAward = ROUND(iRPAward * g_sMPTunables.fxp_tunable_Minigames_Shooting_Range)
	CPRINTLN(DEBUG_SHOOTRANGE, "RANGE_MP_AWARD_MATCH_RP :: tunable(", g_sMPTunables.fxp_tunable_Minigames_Shooting_Range, ") applied, iRPAward=", iRPAward)
	
	RETURN iRPAward
//	GIVE_LOCAL_PLAYER_XP(eXPTYPE_CO_OP_JOBFM, "XPT_RANGE", XPTYPE_COMPLETE, XPCATEGORY_COMPLETE_SHOOTING_RANGE, iRPAward)
ENDFUNC

/// PURPOSE:
///    Makes goggles invisible when in first person cam.
PROC RANGE_MP_HANDLE_SAFETY_GOGGLE_VISIBILITY(RangeMP_CoreData & sCoreData)
	
	IF NOT IS_PLAYER_IN_FIRST_PERSON_CAMERA()
		EXIT
	ENDIF
	
	IF DOES_ENTITY_EXIST(sCoreData.oEarmuffs)
		SET_ENTITY_LOCALLY_INVISIBLE(sCoreData.oEarmuffs)
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Sets player's mask from cached data
PROC RANGE_SET_PLAYER_MASK(RangeMP_CoreData &sCoreData)
	IF sCoreData.eMaskProp != DUMMY_PED_COMP
		CPRINTLN(DEBUG_SHOOTRANGE, "RANGE_SET_PLAYER_MASK Trying to set mask to ", sCoreData.eMaskProp)
		SET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_BERD, sCoreData.eMaskProp, FALSE)
		sCoreData.eMaskProp = DUMMY_PED_COMP
	ENDIF
ENDPROC

/// PURPOSE:
///    Removes and caches the player's mask
PROC RANGE_REMOVE_PLAYER_MASK(RangeMP_CoreData &sCoreData)
	sCoreData.eMaskProp = GET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_BERD)
	SET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_BERD, BERD_FMM_0_0, FALSE)
	
	CPRINTLN(DEBUG_SHOOTRANGE, "RANGE_REMOVE_PLAYER_MASK Saving player mask to ", sCoreData.eMaskProp)
ENDPROC

/// PURPOSE:
///    Sets the player's hat from cached data
PROC RANGE_SET_PLAYER_HAT(RangeMP_CoreData &sCoreData)
	CPRINTLN(DEBUG_SHOOTRANGE, "RANGE_SET_PLAYER_HAT Trying to set hat to ", sCoreData.eHeadProp)
	IF sCoreData.eHeadProp != DUMMY_PED_COMP
		SET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_PROPS, sCoreData.eHeadProp, FALSE)
		sCoreData.eHeadProp = DUMMY_PED_COMP
	ENDIF
ENDPROC

/// PURPOSE:
///    Removes and caches the player's hat
PROC RANGE_REMOVE_PLAYER_HAT(RangeMP_CoreData &sCoreData)
	sCoreData.eHeadProp = GET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_PROPS, ENUM_TO_INT(ANCHOR_HEAD))
	SET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_PROPS, PROPS_HEAD_NONE, FALSE)
	
	CPRINTLN(DEBUG_SHOOTRANGE, "RANGE_REMOVE_PLAYER_HAT Saving player hat to ", sCoreData.eHeadProp)
ENDPROC

/// PURPOSE:
///    Sets the player's special accs from cached data
PROC RANGE_SET_PLAYER_SPECIAL(RangeMP_CoreData &sCoreData)
	CPRINTLN(DEBUG_SHOOTRANGE, "RANGE_SET_PLAYER_SPECIAL Trying to set special to ", sCoreData.eSpecProp)
	IF sCoreData.eSpecProp != DUMMY_PED_COMP
		SET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_SPECIAL, sCoreData.eSpecProp, FALSE)
		sCoreData.eSpecProp = DUMMY_PED_COMP
	ENDIF
ENDPROC

/// PURPOSE:
///    Removes and caches the player's special accs data
/// PARAMS:
///    sCoreData - 
PROC RANGE_REMOVE_PLAYER_SPECIAL(RangeMP_CoreData &sCoreData)
	
	PED_INDEX pedPlayer = PLAYER_PED_ID()

	sCoreData.eSpecProp = GET_PED_COMP_ITEM_CURRENT_MP(pedPlayer, COMP_TYPE_SPECIAL, ENUM_TO_INT(ANCHOR_HEAD))

	CPRINTLN(DEBUG_SHOOTRANGE, "RANGE_REMOVE_PLAYER_SPECIAL Saving player special to ", sCoreData.eSpecProp)
	
	SWAP_CHEM_SUIT_HOOD_FOR_ALTERNATE(pedPlayer)
	
ENDPROC

PROC PUT_ON_RANGE_SAFETY_GEAR(RangeMP_CoreData &sCoreData)
	VECTOR vMuffOffset = <<0.111, 0, 0>>
	VECTOR vMuffRot = <<0, 90, 0>>
	
	IF DOES_PED_HAVE_ANY_PROPS_EQUIPPED( playerPed )
		CLEAR_PED_PROP( playerPed, ANCHOR_HEAD )
	ENDIF
	
	RANGE_REMOVE_PLAYER_MASK(sCoreData)
	RANGE_REMOVE_PLAYER_HAT(sCoreData)
	RANGE_REMOVE_PLAYER_SPECIAL(sCoreData)
	
	SET_PED_COMPONENT_VARIATION( playerPed, PED_COMP_TEETH, 0, 0 )
	
	IF DOES_ENTITY_EXIST(sCoreData.oEarmuffs)
		DELETE_OBJECT(sCoreData.oEarmuffs)
	ENDIF
	sCoreData.oEarmuffs = CREATE_OBJECT(PROP_EAR_DEFENDERS_01, GET_ENTITY_COORDS(playerPed), TRUE, FALSE)
	ATTACH_ENTITY_TO_ENTITY(
		sCoreData.oEarmuffs, 
		playerPed, 
		GET_PED_BONE_INDEX(playerPed, BONETAG_HEAD), 
		vMuffOffset, vMuffRot)
	
	IF IS_PLAYER_MALE(PLAYER_ID())
		// Male
		SET_PED_PROP_INDEX(playerPed, ANCHOR_EYES, 15, 6)
	ELSE
		// Female
		SET_PED_PROP_INDEX(playerPed, ANCHOR_EYES, 9, 1)
	ENDIF
	
	CDEBUG2LN(DEBUG_SHOOTRANGE, "PUT_ON_RANGE_SAFETY_GEAR called")
ENDPROC

/// PURPOSE:
///    Triggers the flash transition effect, and the screenblur for rnge applications.
PROC RANGEMP_UI_TRIGGER_TRANSITION(RangeMP_MenuData & sMenuInfo, BOOL bActivate)
	// Don't process this transition while active.
	IF (bActivate AND sMenuInfo.bTransitionActive)
		EXIT
	ELIF (NOT bActivate AND NOT sMenuInfo.bTransitionActive)
		EXIT
	ENDIF
	
	CPRINTLN( DEBUG_SHOOTRANGE, "RANGEMP_UI_TRIGGER_TRANSITION bActivate = ", PICK_STRING(bActivate, "TRUE", "FALSE") )
	
	// Effect is different for each player.
	TEXT_LABEL_31 txtTransition = "MenuMG"
	IF bActivate
		// Turn the effect on.
		txtTransition += "In"
		ANIMPOSTFX_PLAY(txtTransition, 0, TRUE)
	ELSE
		// Turn the effect off.
		TEXT_LABEL_31 txtTransIn = txtTransition
		txtTransIn += "In"
		txtTransition += "Out"
		ANIMPOSTFX_STOP(txtTransIn)
		ANIMPOSTFX_PLAY(txtTransition, 0, TRUE)
	ENDIF
	sMenuInfo.bTransitionActive = bActivate
ENDPROC

/// PURPOSE:
///    Tells us when it's time to terminate.
/// RETURNS:
///    TRUE if we should terminate. FALSE if not.
FUNC BOOL HAVE_MISSION_END_CONDITIONS_BEEN_MET(RANGE_END_CONDITION & eEndReason, RangeMP_ParticipantInfo & sParticipantInfo)	//, RangeMP_PlayerBD & sAllPlayerBD[])	
	eEndReason = RANGE_END_None
	
	IF (RANGE_GET_NUM_SHOOTERS( sParticipantInfo ) <> k_MaxShooters) AND NOT IS_RANGE_ONE_PLAYER()
		CDEBUG2LN(DEBUG_SHOOTRANGE, "HAVE_MISSION_END_CONDITIONS_BEEN_MET :: eEndReason = RANGE_END_PlayerLeft")
		eEndReason = RANGE_END_PlayerLeft
		RETURN TRUE
	ENDIF
	
//	CDEBUG2LN(DEBUG_SHOOTRANGE, "HAVE_MISSION_END_CONDITIONS_BEEN_MET returning FALSE")
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Return the scorecard UI
FUNC SCALEFORM_INDEX REQUEST_SCORECARD_UI()
	// Shooting Range Scorecard UI
	RETURN REQUEST_SCALEFORM_MOVIE("mission_complete")
ENDFUNC


/// PURPOSE:
///    Requests all the models and other items we'll need for the MP range.
PROC RANGE_MP_INSERT_STREAMS(STREAMED_MODEL & sToStream[], RangeMP_MenuData & sMenuData)
	// Request odds n ends here.
	REQUEST_ADDITIONAL_TEXT("S_RANGE", MINIGAME_TEXT_SLOT)
	REQUEST_ADDITIONAL_TEXT("MP_RNG", MISSION_TEXT_SLOT)
	
	// Request the range assets.
	ADD_STREAMED_MODEL(sToStream, PROP_TARGET_FRAME_01)
	ADD_STREAMED_MODEL(sToStream, PROP_TARGET_ORA_PURP_01)
	ADD_STREAMED_MODEL(sToStream, PROP_TARGET_BACKBOARD_B)
	ADD_STREAMED_MODEL(sToStream, PROP_TARGET_ARM)
	ADD_STREAMED_MODEL(sToStream, PROP_TARGET_RED_CROSS)
	REQUEST_ALL_MODELS(sToStream)
	
	REQUEST_SCRIPT_AUDIO_BANK("TARGET_PRACTICE")
	REQUEST_SCRIPT_AUDIO_BANK("HUD_321_GO")
	
	// New Menu & scorecard
	REQUEST_STREAMED_TEXTURE_DICT("MPHUD")
	REQUEST_STREAMED_TEXTURE_DICT("MPSRange")
	REQUEST_STREAMED_TEXTURE_DICT("SRange_Gen")
	REQUEST_STREAMED_TEXTURE_DICT("SRange_Chal")
	REQUEST_STREAMED_TEXTURE_DICT("SRange_Chal2")
	REQUEST_STREAMED_TEXTURE_DICT("SRange_Weap")
	REQUEST_STREAMED_TEXTURE_DICT("SRange_Weap2")
	REQUEST_STREAMED_TEXTURE_DICT("Shared")
	
	// Reqeust earmuffs
	REQUEST_MODEL(PROP_EAR_DEFENDERS_01)
	REQUEST_MODEL(Prop_Safety_Glasses)
	
	REQUEST_CELEBRATION_SCREEN(sMenuData.sCelebData)
	
	sMenuData.uiLeaderboard = REQUEST_SC_LEADERBOARD_UI()
	REQUEST_MINIGAME_COUNTDOWN_UI(sMenuData.uiCountdown)
	sMenuData.uiShardBM.siMovie = REQUEST_MG_BIG_MESSAGE()
	
	LOAD_MENU_ASSETS()
ENDPROC

PROC RANGE_MP_CREATE_NULL_ROUND(RangeMP_CoreData & sCoreInfo, RangeMP_Round &sRndInfo, RangeMP_PlayerBD &sPlayerBD)
	sCoreInfo.iWinnings = sCoreInfo.iWinnings
	sRndInfo.iSkunkCounter = sRndInfo.iSkunkCounter
	sPlayerBD.iTargetsField1 = sPlayerBD.iTargetsField1
ENDPROC

FUNC BOOL RANGE_MP_SETUP_NULL_ROUND(RangeMP_Round & sCurRoundInfo)
	RETURN sCurRoundInfo.iSkunkCounter = 0
ENDFUNC

PROC RANGE_MP_END_NULL_ROUND(RangeMP_Round & sRndInfo)
	sRndInfo.iSkunkCounter = sRndInfo.iSkunkCounter
ENDPROC

PROC RANGE_MP_INIT_FUNC_POINTERS(RangeMP_RoundFuncs& sRoundFuncs)
	sRoundFuncs.fpRoundCreate = &RANGE_MP_CREATE_NULL_ROUND
	sRoundFuncs.fpRoundSetup = &RANGE_MP_SETUP_NULL_ROUND
	sRoundFuncs.fpRoundEnd = &RANGE_MP_END_NULL_ROUND
ENDPROC


/// PURPOSE:
///    Lets us know when all range assets are done streaming.
/// RETURNS:
///    TRUE when we're done streaming.
FUNC BOOL IS_RANGE_DONE_STREAMING(STREAMED_MODEL & sToStream[], INT & iStreamCounter, RangeMP_MenuData & sMenuData)
	IF NOT HAS_ADDITIONAL_TEXT_LOADED(MINIGAME_TEXT_SLOT) OR NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
		CDEBUG2LN(DEBUG_SHOOTRANGE, "Waiting on: HAS_ADDITIONAL_TEXT_LOADED(MINIGAME_TEXT_SLOT) OR NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)")
		RETURN FALSE
	ENDIF
	
	IF NOT ARE_MODELS_STREAMED(sToStream)
		CDEBUG2LN(DEBUG_SHOOTRANGE, "Waiting on: ARE_MODELS_STREAMED(sToStream)")
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_SCALEFORM_MOVIE_LOADED(sMenuData.uiLeaderboard)
		CDEBUG2LN(DEBUG_SHOOTRANGE, "Waiting on: HAS_SCALEFORM_MOVIE_LOADED(sMenuData.uiLeaderboard)")
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_SCALEFORM_MOVIE_LOADED(sMenuData.uiShardBM.siMovie)
		CDEBUG2LN(DEBUG_SHOOTRANGE, "Waiting on: HAS_SCALEFORM_MOVIE_LOADED(sMenuData.uiShardBM.siMovie)")
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_MINIGAME_COUNTDOWN_UI_LOADED(sMenuData.uiCountdown)
		CDEBUG2LN(DEBUG_SHOOTRANGE, "Waiting on: HAS_MINIGAME_COUNTDOWN_UI_LOADED(sMenuData.uiCountdown)")
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED("MPHUD")
		CDEBUG2LN(DEBUG_SHOOTRANGE, "Have not streamed text dict: MPUD")
		RETURN FALSE
	ENDIF
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED("MPSRange")
		CDEBUG2LN(DEBUG_SHOOTRANGE, "Have not streamed text dict: MPSRange")
		RETURN FALSE
	ENDIF
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED("SRange_Gen")
		CDEBUG2LN(DEBUG_SHOOTRANGE, "Have not streamed text dict: SRange_Gen")
		RETURN FALSE
	ENDIF
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED("SRange_Chal")
		CDEBUG2LN(DEBUG_SHOOTRANGE, "Have not streamed text dict: SRange_Chal")
		RETURN FALSE
	ENDIF
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED("SRange_Chal2")
		CDEBUG2LN(DEBUG_SHOOTRANGE, "Have not streamed text dict: SRange_Chal2")
		RETURN FALSE
	ENDIF
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED("SRange_Weap")
		CDEBUG2LN(DEBUG_SHOOTRANGE, "Have not streamed text dict: SRange_Weap")
		RETURN FALSE
	ENDIF
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED("SRange_Weap2")
		CDEBUG2LN(DEBUG_SHOOTRANGE, "Have not streamed text dict: SRange_Weap2")
		RETURN FALSE
	ENDIF
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED("Shared")
		CDEBUG2LN(DEBUG_SHOOTRANGE, "Have not streamed text dict: Shared")
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_MODEL_LOADED(PROP_EAR_DEFENDERS_01)
		CDEBUG2LN(DEBUG_SHOOTRANGE, "NOT HAS_MODEL_LOADED(PROP_EAR_DEFENDERS_01)")
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_MODEL_LOADED(Prop_Safety_Glasses)
		CDEBUG2LN(DEBUG_SHOOTRANGE, "NOT HAS_MODEL_LOADED(Prop_Safety_Glasses)")
		RETURN FALSE
	ENDIF
	
	IF NOT LOAD_MENU_ASSETS()
		CDEBUG2LN(DEBUG_SHOOTRANGE, "NOT LOAD_MENU_ASSETS()")
		RETURN FALSE
	ENDIF
	
	// Don't try to stream the audio bank forever... could lead to badness.
	IF (iStreamCounter < AUDIO_STREAM_MAX_ATTEMPTS)
		IF NOT REQUEST_SCRIPT_AUDIO_BANK("TARGET_PRACTICE")
		OR NOT REQUEST_SCRIPT_AUDIO_BANK("HUD_321_GO")
			iStreamCounter++
			RETURN FALSE
		ENDIF
	ELSE
		CDEBUG2LN(DEBUG_SHOOTRANGE, "We've failed to load the Range MP audio bank after AUDIO_STREAM_MAX_ATTEMPTS attempts! May not have audio..")
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Turns control for the player on an off, can be called every frame without multiple Native calls made.
/// PARAMS:
///    thisActivity - Will work if the activity is anything but TA_AI_VS_AI
///    bControl - 
PROC ALLOW_RANGE_PLAYER_CONTROL(BOOL bControl)
	IF bControl
		IF NOT IS_PED_INJURED(PLAYER_PED_ID()) AND NOT IS_PLAYER_CONTROL_ON(GET_PLAYER_INDEX())
			SET_PLAYER_CONTROL(GET_PLAYER_INDEX(),TRUE)
			CDEBUG2LN(DEBUG_SHOOTRANGE, DEBUG_TENNIS, "ALLOW_RANGE_PLAYER_CONTROL: Turning Player Control ON")
			
			#IF IS_DEBUG_BUILD
				DEBUG_RECORD_STRING("SET_PLAYER_CONTROL", "TRUE")
			#ENDIF
		ENDIF
	ELSE
		IF NOT IS_PED_INJURED(PLAYER_PED_ID()) AND IS_PLAYER_CONTROL_ON(GET_PLAYER_INDEX())
			SET_PLAYER_CONTROL(GET_PLAYER_INDEX(),FALSE)
			CDEBUG2LN(DEBUG_SHOOTRANGE, DEBUG_TENNIS, "ALLOW_RANGE_PLAYER_CONTROL: Turning Player Control OFF")
			
			#IF IS_DEBUG_BUILD
				DEBUG_RECORD_STRING("SET_PLAYER_CONTROL", "FALSE")
			#ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC FORCE_RANGE_MENU_CHOICE(RangeMP_PlayerBD &sClientBD[], RangeMP_MenuData &sAllMenus)
	// Just made a selection.
	// If we're on menu 0 (weap cats menu), go to weapons. If we're on 1 (weapons menu), go to challenges. If we're on 2, we're done.
	INT iCurMenuElement
	INT iParticipantID = PARTICIPANT_ID_TO_INT()
	iCurMenuElement = RANGE_MP_GET_MENU_CURRENT_ELEMENT(sAllMenus.sMenu[sAllMenus.eActiveMenu])
	CDEBUG2LN(DEBUG_SHOOTRANGE, "FORCE_RANGE_MENU_CHOICE :: iCurMenuElement=", iCurMenuElement)
	IF (sAllMenus.eActiveMenu = RANGE_MP_MENU_CATEGORIES)
		CDEBUG2LN(DEBUG_SHOOTRANGE, "This was our selected weapon category: ", iCurMenuElement)
		
		RANGE_WEAPON_CATEGORY eCatToSet
		eCatToSet = INT_TO_ENUM(RANGE_WEAPON_CATEGORY, iCurMenuElement)
		SET_ROUND_WEAPON_CATEGORY(sClientBD[iParticipantID].sRoundDesc, eCatToSet)
		
		RANGE_MP_InitWeaponsMenu(sClientBD, sAllMenus.sMenu[RANGE_MP_MENU_WEAPONS], eCatToSet)
		sAllMenus.eActiveMenu = RANGE_MP_MENU_WEAPONS
		iCurMenuElement = 0
	ENDIF
	CDEBUG2LN(DEBUG_SHOOTRANGE, "FORCE_RANGE_MENU_CHOICE :: iCurMenuElement=", iCurMenuElement)
	IF (sAllMenus.eActiveMenu = RANGE_MP_MENU_WEAPONS)
		CDEBUG2LN(DEBUG_SHOOTRANGE, "This was our selected weapon: ", iCurMenuElement)
		
		SET_ROUND_WEAPON(sClientBD[iParticipantID].sRoundDesc, sAllMenus.sMenu[ RANGE_MP_MENU_WEAPONS ].eWeapons[ iCurMenuElement ] )
		RANGE_MP_SET_MENU_CURRENT_ELEMENT( sAllMenus.sMenu[RANGE_MP_MENU_WEAPONS], iCurMenuElement )
		
		sAllMenus.eActiveMenu = RANGE_MP_MENU_CHALLENGES
		
		// Can't select item -1 in challenges.
		IF (sAllMenus.sMenu[RANGE_MP_MENU_CHALLENGES].iCurElement = -1)
			sAllMenus.sMenu[RANGE_MP_MENU_CHALLENGES].iCurElement = 0
		ENDIF
		iCurMenuElement = 0
	ENDIF
	CDEBUG2LN(DEBUG_SHOOTRANGE, "FORCE_RANGE_MENU_CHOICE :: iCurMenuElement=", iCurMenuElement)
	IF (sAllMenus.eActiveMenu = RANGE_MP_MENU_CHALLENGES)
		CDEBUG2LN(DEBUG_SHOOTRANGE, "This was our selected weapon: ", iCurMenuElement)
		// Set the entire selection here, regardless of the fact that we've picked it before.
		// This is to account for silly things like timeouts, where this may have been erased for us.
		SET_ROUND_WEAPON_CATEGORY(sClientBD[iParticipantID].sRoundDesc, INT_TO_ENUM(RANGE_WEAPON_CATEGORY, sAllMenus.sMenu[RANGE_MP_MENU_CATEGORIES].iCurElement))
		SET_ROUND_WEAPON(sClientBD[iParticipantID].sRoundDesc, sAllMenus.sMenu[ RANGE_MP_MENU_WEAPONS ].eWeapons[ RANGE_MP_GET_MENU_CURRENT_ELEMENT( sAllMenus.sMenu[RANGE_MP_MENU_WEAPONS] ) ] )
		SET_ROUND_CHALLENGE(sClientBD[iParticipantID].sRoundDesc, INT_TO_ENUM(CHALLENGE_INDEX, iCurMenuElement))
		
		SET_BITMASK_AS_ENUM(sClientBD[iParticipantID].iFlags, RANGE_MP_F_DoneWithMenuSelections)
		iCurMenuElement = 0
	ENDIF
ENDPROC

PROC SET_UP_RANGE_FUNCTION_POINTERS(RangeMP_RoundFuncs& sRndFuncs, RangeMP_PlayerBD &sAllPlayerBD[], INT iClientID)
	// Store our round function pointers.
	CDEBUG1LN( DEBUG_SHOOTRANGE, "SET_UP_RANGE_FUNCTION_POINTERS - started")
	IF (sAllPlayerBD[iClientID].sRoundDesc.eChallenge = CI_GRID)
		sRndFuncs.fpRoundCreate = &RANGE_MP_CREATE_GRID_ROUND
		sRndFuncs.fpRoundStart 	= &RANGE_MP_START_GRID_ROUND
		sRndFuncs.fpRoundSetup 	= &RANGE_MP_SETUP_GRID_ROUND
		sRndFuncs.fpRoundUpdate = &RANGE_MP_UPDATE_GRID_ROUND
		sRndFuncs.fpRoundEnd 	= &RANGE_MP_END_GRID_ROUND
		
		CDEBUG1LN( DEBUG_SHOOTRANGE, "SET_UP_RANGE_FUNCTION_POINTERS - sAllPlayerBD[iClientID].sRoundDesc.eChallenge = CI_GRID")
		EXIT
		
	ELIF (sAllPlayerBD[iClientID].sRoundDesc.eChallenge = CI_RANDOM)
		sRndFuncs.fpRoundCreate = &RANGE_MP_CREATE_RANDOM_ROUND
		sRndFuncs.fpRoundStart 	= &RANGE_MP_START_RANDOM_ROUND
		sRndFuncs.fpRoundSetup 	= &RANGE_MP_SETUP_RANDOM_ROUND
		sRndFuncs.fpRoundUpdate = &RANGE_MP_UPDATE_RANDOM_ROUND
		sRndFuncs.fpRoundEnd 	= &RANGE_MP_END_RANDOM_ROUND
		
		CDEBUG1LN( DEBUG_SHOOTRANGE, "SET_UP_RANGE_FUNCTION_POINTERS - sAllPlayerBD[iClientID].sRoundDesc.eChallenge = CI_RANDOM")
		EXIT
	
	ELIF (sAllPlayerBD[iClientID].sRoundDesc.eChallenge = CI_COVERED)
		sRndFuncs.fpRoundCreate = &RANGE_MP_CREATE_COVERED_ROUND
		sRndFuncs.fpRoundStart 	= &RANGE_MP_START_COVERED_ROUND
		sRndFuncs.fpRoundSetup 	= &RANGE_MP_SETUP_COVERED_ROUND
		sRndFuncs.fpRoundUpdate = &RANGE_MP_UPDATE_COVERED_ROUND
		sRndFuncs.fpRoundEnd 	= &RANGE_MP_END_COVERED_ROUND
		
		CDEBUG1LN( DEBUG_SHOOTRANGE, "SET_UP_RANGE_FUNCTION_POINTERS - sAllPlayerBD[iClientID].sRoundDesc.eChallenge = CI_COVERED")
		EXIT
		
	ENDIF
	CERRORLN( DEBUG_SHOOTRANGE, "SET_UP_RANGE_FUNCTION_POINTERS - sAllPlayerBD[iClientID].sRoundDesc.eChallenge = NOT SET!!!")
ENDPROC

PROC GET_RANGE_SPECTATOR_ROUND_UP_TO_SPEED(CHALLENGE_INDEX eChallenge, RangeMP_RoundFuncs& sFuncs)
	SWITCH eChallenge
		CASE CI_GRID
			sFuncs.fpRoundCreate 	= &RANGE_MP_CREATE_GRID_ROUND
			sFuncs.fpRoundStart 	= &RANGE_MP_START_GRID_ROUND
			sFuncs.fpRoundSetup 	= &RANGE_MP_SETUP_GRID_ROUND
			sFuncs.fpRoundUpdate 	= &RANGE_MP_UPDATE_GRID_ROUND
			sFuncs.fpRoundEnd 		= &RANGE_MP_END_GRID_ROUND
		BREAK
		CASE CI_RANDOM
			sFuncs.fpRoundCreate	= &RANGE_MP_CREATE_RANDOM_ROUND
			sFuncs.fpRoundStart		= &RANGE_MP_START_RANDOM_ROUND
			sFuncs.fpRoundSetup		= &RANGE_MP_SETUP_RANDOM_ROUND
			sFuncs.fpRoundUpdate 	= &RANGE_MP_UPDATE_RANDOM_ROUND
			sFuncs.fpRoundEnd 		= &RANGE_MP_END_RANDOM_ROUND
		BREAK
		CASE CI_COVERED
			sFuncs.fpRoundCreate 	= &RANGE_MP_CREATE_COVERED_ROUND
			sFuncs.fpRoundStart 	= &RANGE_MP_START_COVERED_ROUND
			sFuncs.fpRoundSetup 	= &RANGE_MP_SETUP_COVERED_ROUND
			sFuncs.fpRoundUpdate 	= &RANGE_MP_UPDATE_COVERED_ROUND
			sFuncs.fpRoundEnd 		= &RANGE_MP_END_COVERED_ROUND
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    REP: this needs finishing
/// PARAMS:
///    sAllPlayerBD - 
///    iClientID - 
///    sCurRound - 
///    sFuncs - 
PROC GET_RANGE_SPECTATOR_UP_TO_SPEED(RangeMP_MenuData &sAllMenuData, RangeMP_ServerBD &sServerBD, RangeMP_PlayerData &thisPlayer, RangeMP_PlayerBD & sAllPlayerBD[], INT iClientID, RangeMP_Round & sCurRound, RangeMP_RoundFuncs& sFuncs, RangeMP_CoreData & sCoreInfo, RangeMP_ParticipantInfo & sParticipantInfo ) //RangeMP_ServerBD & sServerBD, RangeMP_MenuData & sAllMenuData, RangeMP_PlayerData & sPlayerData[]
	
	INT iRedPlayer = sParticipantInfo.iShooters[RED_PLAYER_CLIENT_ID]
	
	IF iRedPlayer = k_InvalidParticipantID
		CDEBUG3LN( DEBUG_SHOOTRANGE, "GET_RANGE_SPECTATOR_UP_TO_SPEED - iRedPlayer = k_InvalidParticipantID, this is potentially fatal." )
	ENDIF
	
	RANGE_STATE eState = GET_CLIENT_RANGE_STATE(sAllPlayerBD[iRedPlayer])
	SET_CLIENT_RANGE_STATE(sAllPlayerBD[iClientID], eState)
	sAllPlayerBD[iClientID].iTargetsField1 = sAllPlayerBD[iRedPlayer].iTargetsField1
	sAllPlayerBD[iClientID].iTargetsField2 = sAllPlayerBD[iRedPlayer].iTargetsField2
	
	RANGE_MP_InitMainMenu(sAllMenuData, iClientID, sServerBD, sAllPlayerBD)
	thisPlayer.eRSCState = RSC_PLAYERS1
	GET_RANGE_SPECTATOR_ROUND_UP_TO_SPEED(sAllPlayerBD[iRedPlayer].sRoundDesc.eChallenge, sFuncs)
	
	SWITCH eState
		CASE RANGE_STATE_InRound
			CDEBUG2LN(DEBUG_SHOOTRANGE, "GET_RANGE_SPECTATOR_UP_TO_SPEED :: RANGE_STATE_InRound")
			SET_BIT(sAllPlayerBD[iClientID].iTargetsField2, 31)
			
			sAllPlayerBD[iClientID].iCatElem = sAllPlayerBD[iRedPlayer].iCatElem
			sAllPlayerBD[iClientID].iWeapElem = sAllPlayerBD[iRedPlayer].iWeapElem
			sAllPlayerBD[iClientID].iChalElem = sAllPlayerBD[iRedPlayer].iChalElem
			
			RANGE_MP_InitWeaponsMenu(sAllPlayerBD, sAllMenuData.sMenu[RANGE_MP_MENU_WEAPONS], INT_TO_ENUM(RANGE_WEAPON_CATEGORY, sAllPlayerBD[iClientID].iCatElem))
			sAllMenuData.sMenu[RANGE_MP_MENU_CATEGORIES].iCurElement = sAllPlayerBD[iClientID].iCatElem
			sAllMenuData.sMenu[RANGE_MP_MENU_WEAPONS].iCurElement = sAllPlayerBD[iClientID].iWeapElem
			sAllMenuData.sMenu[RANGE_MP_MENU_CHALLENGES].iCurElement = sAllPlayerBD[iClientID].iChalElem
			
			// Make sure the player is in the furthest menu...
			sAllMenuData.eActiveMenu = RANGE_MP_MENU_CHALLENGES
			sAllPlayerBD[iClientID].sRoundDesc = sAllPlayerBD[iRedPlayer].sRoundDesc
			
			RANGE_MP_INIT_IMPACT_COORDS( sAllPlayerBD[iClientID] )

			CALL sFuncs.fpRoundCreate(sCoreInfo, sCurRound, sAllPlayerBD[iClientID])
		BREAK
		CASE RANGE_STATE_MenuUpdate
		CASE RANGE_STATE_DisplayRoundOver
			CDEBUG2LN(DEBUG_SHOOTRANGE, "GET_RANGE_SPECTATOR_UP_TO_SPEED :: RANGE_STATE_MenuUpdate")
			RANGEMP_UI_TRIGGER_TRANSITION(sAllMenuData, TRUE)
			
			IF (eState = RANGE_STATE_DisplayRoundOver)
				sAllPlayerBD[iClientID].iCatElem = sAllPlayerBD[iRedPlayer].iCatElem
				sAllPlayerBD[iClientID].iWeapElem = sAllPlayerBD[iRedPlayer].iWeapElem
				sAllPlayerBD[iClientID].iChalElem = sAllPlayerBD[iRedPlayer].iChalElem
				
				RANGE_MP_InitWeaponsMenu(sAllPlayerBD, sAllMenuData.sMenu[RANGE_MP_MENU_WEAPONS], INT_TO_ENUM(RANGE_WEAPON_CATEGORY, sAllPlayerBD[iClientID].iCatElem))
				sAllMenuData.sMenu[RANGE_MP_MENU_CATEGORIES].iCurElement = sAllPlayerBD[iClientID].iCatElem
				sAllMenuData.sMenu[RANGE_MP_MENU_WEAPONS].iCurElement = sAllPlayerBD[iClientID].iWeapElem
				sAllMenuData.sMenu[RANGE_MP_MENU_CHALLENGES].iCurElement = sAllPlayerBD[iClientID].iChalElem
				
				// Make sure the player is in the furthest menu...
				sAllMenuData.eActiveMenu = RANGE_MP_MENU_CHALLENGES
				sAllPlayerBD[iClientID].sRoundDesc = sAllPlayerBD[iRedPlayer].sRoundDesc
			ENDIF
		BREAK
		DEFAULT
			CDEBUG2LN(DEBUG_SHOOTRANGE, "GET_RANGE_SPECTATOR_UP_TO_SPEED :: DEFAULT, eState=", GET_STRING_FROM_RANGE_STATE(eState))
		BREAK
	ENDSWITCH
ENDPROC

PROC HANDLE_RANGE_SPECTATOR_CAMERA(RangeMP_PlayerData &thisPlayer, RangeMP_CoreData &sCoreData)
	INT iCamInterpTime = 10000
	IF IS_DISABLED_CONTROL_JUST_RELEASED(PLAYER_CONTROL, INPUT_SCRIPT_SELECT)
		SET_RANGE_PLAYER_CAM_STAMP(thisPlayer, 0)
		CDEBUG2LN(DEBUG_SHOOTRANGE, "HANDLE_RANGE_SPECTATOR_CAMERA :: SET_RANGE_PLAYER_CAM_STAMP(thisPlayer, 0)")
	ENDIF
	SWITCH thisPlayer.eRSCState
		// Cycle between cams that look at the targets
		CASE RSC_DRIFT_RIGHT
			IF GET_GAME_TIMER() > GET_RANGE_PLAYER_CAM_STAMP(thisPlayer)
				CDEBUG2LN(DEBUG_SHOOTRANGE, "HANDLE_RANGE_SPECTATOR_CAMERA :: Switching to RSC_DRIFT_DOWN")
				SET_CAM_ACTIVE(thisPlayer.camSpectator, TRUE)
				SET_CAM_PARAMS(thisPlayer.camSpectator, sCoreData.vSpec1[0], sCoreData.vSpec1[1], 50, default, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
				SET_CAM_PARAMS(thisPlayer.camSpectator, sCoreData.vSpec1Goto[0], sCoreData.vSpec1Goto[1], 50, 10000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
				RENDER_SCRIPT_CAMS(TRUE,FALSE)
				thisPlayer.eRSCState = RSC_DRIFT_DOWN
				SET_RANGE_PLAYER_CAM_STAMP(thisPlayer, GET_GAME_TIMER() + iCamInterpTime)
				IF IS_THIS_PRINT_BEING_DISPLAYED("SR_SPEC_WAITING")
					CLEAR_THIS_PRINT("SR_SPEC_WAITING")
				ENDIF
			ENDIF
		BREAK
		CASE RSC_DRIFT_DOWN
			IF GET_GAME_TIMER() > GET_RANGE_PLAYER_CAM_STAMP(thisPlayer)
				CDEBUG2LN(DEBUG_SHOOTRANGE, "HANDLE_RANGE_SPECTATOR_CAMERA :: Switching to RSC_TIGHTEN_ON_TARGETS")
				SET_CAM_ACTIVE(thisPlayer.camSpectator, TRUE)
				SET_CAM_PARAMS(thisPlayer.camSpectator, sCoreData.vSpec2[0], sCoreData.vSpec2[1], 50, default, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
				SET_CAM_PARAMS(thisPlayer.camSpectator, sCoreData.vSpec2Goto[0], sCoreData.vSpec2Goto[1], 50, 10000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
				RENDER_SCRIPT_CAMS(TRUE,FALSE)
				thisPlayer.eRSCState = RSC_TIGHTEN_ON_TARGETS
				SET_RANGE_PLAYER_CAM_STAMP(thisPlayer, GET_GAME_TIMER() + iCamInterpTime)
				IF IS_THIS_PRINT_BEING_DISPLAYED("SR_SPEC_WAITING")
					CLEAR_THIS_PRINT("SR_SPEC_WAITING")
				ENDIF
			ENDIF
		BREAK
		CASE RSC_TIGHTEN_ON_TARGETS
			IF GET_GAME_TIMER() > GET_RANGE_PLAYER_CAM_STAMP(thisPlayer)
				CDEBUG2LN(DEBUG_SHOOTRANGE, "HANDLE_RANGE_SPECTATOR_CAMERA :: Switching to RSC_DRIFT_RIGHT")
				SET_CAM_ACTIVE(thisPlayer.camSpectator, TRUE)
				SET_CAM_PARAMS(thisPlayer.camSpectator, sCoreData.vSpec3[0], sCoreData.vSpec3[1], 50, default, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
				SET_CAM_PARAMS(thisPlayer.camSpectator, sCoreData.vSpec3Goto[0], sCoreData.vSpec3Goto[1], 50, 10000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
				RENDER_SCRIPT_CAMS(TRUE,FALSE)
				thisPlayer.eRSCState = RSC_DRIFT_RIGHT
				SET_RANGE_PLAYER_CAM_STAMP(thisPlayer, GET_GAME_TIMER() + iCamInterpTime)
				IF IS_THIS_PRINT_BEING_DISPLAYED("SR_SPEC_WAITING")
					CLEAR_THIS_PRINT("SR_SPEC_WAITING")
				ENDIF
			ENDIF
		BREAK
		// Cycle between cams that look at the player
		CASE RSC_PLAYERS1
			IF GET_GAME_TIMER() > GET_RANGE_PLAYER_CAM_STAMP(thisPlayer)
				CDEBUG2LN(DEBUG_SHOOTRANGE, "HANDLE_RANGE_SPECTATOR_CAMERA :: Switching to RSC_PLAYERS1")
				SET_CAM_ACTIVE(thisPlayer.camSpectator, TRUE)
				SET_CAM_PARAMS(thisPlayer.camSpectator, sCoreData.vSpec4[0], sCoreData.vSpec4[1], 50, default, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
				SET_CAM_PARAMS(thisPlayer.camSpectator, sCoreData.vSpec4Goto[0], sCoreData.vSpec4Goto[1], 50, 10000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
				RENDER_SCRIPT_CAMS(TRUE,FALSE)
				thisPlayer.eRSCState = RSC_PLAYERS2
				SET_RANGE_PLAYER_CAM_STAMP(thisPlayer, GET_GAME_TIMER() + iCamInterpTime)
				PRINT_NOW("SR_SPEC_WAITING", iCamInterpTime, 0)
			ENDIF
		BREAK
		CASE RSC_PLAYERS2
			IF GET_GAME_TIMER() > GET_RANGE_PLAYER_CAM_STAMP(thisPlayer)
				CDEBUG2LN(DEBUG_SHOOTRANGE, "HANDLE_RANGE_SPECTATOR_CAMERA :: Switching to RSC_PLAYERS2")
				SET_CAM_ACTIVE(thisPlayer.camSpectator, TRUE)
				SET_CAM_PARAMS(thisPlayer.camSpectator, sCoreData.vSpec5[0], sCoreData.vSpec5[1], 50, default, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
				SET_CAM_PARAMS(thisPlayer.camSpectator, sCoreData.vSpec5Goto[0], sCoreData.vSpec5Goto[1], 50, 10000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
				RENDER_SCRIPT_CAMS(TRUE,FALSE)
				thisPlayer.eRSCState = RSC_PLAYERS3
				SET_RANGE_PLAYER_CAM_STAMP(thisPlayer, GET_GAME_TIMER() + iCamInterpTime)
				PRINT_NOW("SR_SPEC_WAITING", iCamInterpTime, 0)
			ENDIF
		BREAK
		CASE RSC_PLAYERS3
			IF GET_GAME_TIMER() > GET_RANGE_PLAYER_CAM_STAMP(thisPlayer)
				CDEBUG2LN(DEBUG_SHOOTRANGE, "HANDLE_RANGE_SPECTATOR_CAMERA :: Switching to RSC_PLAYERS3")
				SET_CAM_ACTIVE(thisPlayer.camSpectator, TRUE)
				SET_CAM_PARAMS(thisPlayer.camSpectator, sCoreData.vSpec6[0], sCoreData.vSpec6[1], 50, default, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
				SET_CAM_PARAMS(thisPlayer.camSpectator, sCoreData.vSpec6Goto[0], sCoreData.vSpec6Goto[1], 50, 10000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
				RENDER_SCRIPT_CAMS(TRUE,FALSE)
				thisPlayer.eRSCState = RSC_PLAYERS1
				SET_RANGE_PLAYER_CAM_STAMP(thisPlayer, GET_GAME_TIMER() + iCamInterpTime)
				PRINT_NOW("SR_SPEC_WAITING", iCamInterpTime, 0)
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    
/// PARAMS:
///    sAllPlayerBD - []
///    iClientID - 
///    sAllMenuData - 
///    sPlayerData - []
///    sCurRound - 
///    eTerminateReason - 
FUNC BOOL STANDARD_RANGE_UPDATE_CALLS(RangeMP_ServerBD & sServerBD, RangeMP_PlayerBD & sAllPlayerBD[], INT iClientID, RangeMP_MenuData & sAllMenuData, RangeMP_PlayerData & sPlayerData[], RangeMP_Round & sCurRound, RANGE_END_CONDITION	& eTerminateReason, RangeMP_CoreData & sCoreInfo, RangeMP_ParticipantInfo & sParticipantInfo)
			
	// The player needs to process certain events here...
	PLAYER_PROCESS_EVENTS(iClientID, sCurRound, sPlayerData, sAllPlayerBD[iClientID].sRoundDesc.eChallenge, sAllPlayerBD[iClientID], eTerminateReason, sCoreInfo, sParticipantInfo)
	
	// Handle the emergency teleport for the players...
	RANGEMP_EMERGENCY_TELEPORT(sPlayerData[iClientID])
	
	// Handle the player trying to abort, here.
	IF PLAYER_PROCESS_LEAVING(sServerBD, sPlayerData[iClientID], sCoreInfo, iClientID, sParticipantInfo) OR eTerminateReason = RANGE_END_PlayerLeft
		CDEBUG2LN(DEBUG_SHOOTRANGE, "IF PLAYER_PROCESS_LEAVING(sPlayerData[iClientID]) OR eTerminateReason = RANGE_END_PlayerLeft")
		SET_LOADING_ICON_INACTIVE()
		SET_CLIENT_GAME_STATE(sAllPlayerBD[iClientID], GAME_STATE_LEAVE)
		IF eTerminateReason = RANGE_END_PlayerLeft
			SET_SHARD_BIG_MESSAGE(sAllMenuData.uiShardBM, "SC_COM_TIMEUP", "SHR_MP_Q_LV", 4000)
			RANGE_CLEAR_ENDED_MISSION_AT_LEADERBOARD_VOTE_OR_TIME_OUT()
			SET_CLIENT_FLAG(sAllPlayerBD[iClientID], RANGE_MP_F_ContinueAfterVotePassing)
			SETTIMERA(0)
			BUSYSPINNER_OFF()
			
			IF sServerBD.iClient0MatchWins > sServerBD.iClient1MatchWins AND iClientID = sParticipantInfo.iShooters[0]
			OR sServerBD.iClient0MatchWins < sServerBD.iClient1MatchWins AND iClientID = sParticipantInfo.iShooters[1]
				BROADCAST_BETTING_MISSION_FINISHED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(PARTICIPANT_ID_TO_INT())))
				CDEBUG2LN(DEBUG_SHOOTRANGE, "STANDARD_RANGE_UPDATE_CALLS() :: BROADCAST_BETTING sServerBD.iClient0MatchWins=", sServerBD.iClient0MatchWins, ", sServerBD.iClient1MatchWins=", sServerBD.iClient1MatchWins, ", iClientID=", iClientID)
			ELSE
				BROADCAST_BETTING_MISSION_FINISHED_TIED()
				CDEBUG2LN(DEBUG_SHOOTRANGE, "STANDARD_RANGE_UPDATE_CALLS() :: BROADCAST_BETTING TIED sServerBD.iClient0MatchWins=", sServerBD.iClient0MatchWins, ", sServerBD.iClient1MatchWins=", sServerBD.iClient1MatchWins, ", iClientID=", iClientID)
			ENDIF
		ENDIF
		RANGEMP_UI_TRIGGER_TRANSITION(sAllMenuData, TRUE)
		RETURN TRUE
	ENDIF
	
	IF IS_CLIENT_FLAG_SET(sAllPlayerBD[iClientID], RANGE_MP_F_IsSpectator)
		AND NOT IS_PLAYER_SCTV( PLAYER_ID() )
		SET_PLAYER_INVISIBLE_LOCALLY(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iClientID)))
	ENDIF
	
	// Process various other every-frame things the player needs.
	PROCESS_RANGE_PLAYER(sAllPlayerBD[iClientID])
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Handles the button presses and quit commands coming from the spectator.
/// PARAMS:
///    sAllPlayerBD - 
///    iClientID - 
///    sAllMenuData - 
///    sPlayerData - 
///    sCurRound - 
///    sFuncs - 
PROC RANGE_MP_UPDATE_SPECTATOR_QUIT(RangeMP_PlayerBD & sAllPlayerBD[], INT iClientID, RangeMP_MenuData & sAllMenuData, RangeMP_PlayerData & sPlayerData[], RangeMP_Round & sCurRound, RangeMP_RoundFuncs& sFuncs )
	
	//SCTV Spectator has their own Quit
	IF IS_PLAYER_SCTV(PLAYER_ID())
		EXIT
	ENDIF
	
	// Handle quitting
	IF IS_PLAYER_FLAG_SET(sPlayerData[iClientID], RANGE_MP_F_QUIT_SCREEN)
		SET_WARNING_MESSAGE_WITH_HEADER("SHR_QUIT_RANGE", "SHR_QUIT_DET", FE_WARNING_NO | FE_WARNING_YES)
		IF IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
			CDEBUG2LN(DEBUG_SHOOTRANGE, "RANGE_MP_UPDATE_SPECTATOR_QUIT -- Spectator quitting!!")
			
			SET_CLIENT_GAME_STATE(sAllPlayerBD[iClientID], GAME_STATE_LEAVE)
			IF GET_CLIENT_RANGE_STATE(sAllPlayerBD[iClientID]) = RANGE_STATE_InRound
				CALL sFuncs.fpRoundEnd(sCurRound)
			ENDIF
		ENDIF
		IF IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
			CDEBUG2LN(DEBUG_SHOOTRANGE, "RANGE_MP_UPDATE_SPECTATOR_QUIT -- Spectator cancelled quitting!!")
			
			CLEAR_PLAYER_FLAG(sPlayerData[iClientID], RANGE_MP_F_QUIT_SCREEN)
			IF GET_CLIENT_RANGE_STATE(sAllPlayerBD[iClientID]) <> RANGE_STATE_DisplayRoundOver
				RANGE_BUSYSPINNER_OFF()
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_RANGE_MP_MENU_FLAG_SET(sAllMenuData, RMPM_SHOWING_SCLB)
			RANGE_STATE eCurState = GET_CLIENT_RANGE_STATE(sAllPlayerBD[iClientID])
			IF (eCurState = RANGE_STATE_MenuUpdate OR eCurState = RANGE_STATE_DisplayRoundOver) 
			 	IF IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL) OR IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
					SET_PLAYER_FLAG(sPlayerData[iClientID], RANGE_MP_F_QUIT_SCREEN)
					SET_WARNING_MESSAGE_WITH_HEADER("SHR_QUIT_RANGE", "SHR_QUIT_DET", FE_WARNING_NO | FE_WARNING_YES)
				ENDIF
			ELSE
				IF IS_DISABLED_CONTROL_JUST_RELEASED(PLAYER_CONTROL, INPUT_CONTEXT)
					SET_PLAYER_FLAG(sPlayerData[iClientID], RANGE_MP_F_QUIT_SCREEN)
					SET_WARNING_MESSAGE_WITH_HEADER("SHR_QUIT_RANGE", "SHR_QUIT_DET", FE_WARNING_NO | FE_WARNING_YES)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL RANGE_MP_UPDATE_SPECTATOR(RangeMP_ServerBD & sServerBD, RangeMP_PlayerBD & sAllPlayerBD[], INT iClientID, RangeMP_MenuData & sAllMenuData, 
		RangeMP_PlayerData & sPlayerData[], RangeMP_Round & sCurRound, RangeMP_RoundFuncs& sFuncs, RANGE_END_CONDITION	& eTerminateReason, RangeMP_CoreData & sCoreInfo, RangeMP_ParticipantInfo & sParticipantInfo )
	HANDLE_RANGE_SPECTATOR_CAMERA(sPlayerData[iClientID], sCoreInfo)
	
	INT iRedPlayer = sParticipantInfo.iShooters[RED_PLAYER_CLIENT_ID]
	
	IF iRedPlayer = k_InvalidParticipantID
		CDEBUG3LN( DEBUG_SHOOTRANGE, "RANGE_MP_UPDATE_SPECTATOR - iRedPlayer = k_InvalidParticipantID, this is potentially fatal." )
		RETURN FALSE
	ENDIF
	
	RANGE_MP_UPDATE_SPECTATOR_QUIT( sAllPlayerBD, iClientID, sAllMenuData, sPlayerData, sCurRound, sFuncs )
	
	// Used for various function returns.
	THREE_STATE_RETVAL eRetval
	
	IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
		SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
	ENDIF
	
	IF STANDARD_RANGE_UPDATE_CALLS(sServerBD, sAllPlayerBD, iClientID, sAllMenuData, sPlayerData, sCurRound, eTerminateReason, sCoreInfo, sParticipantInfo)
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_FLAG_SET(sPlayerData[iClientID], RANGE_MP_F_SPECTATOR_LOCKSTEP) 
	AND GET_CLIENT_RANGE_STATE(sAllPlayerBD[iClientID]) <> GET_CLIENT_RANGE_STATE(sAllPlayerBD[iRedPlayer])
		SET_PLAYER_FLAG(sPlayerData[iClientID], RANGE_MP_F_SPECTATOR_MOVE_ON)
		CLEAR_PLAYER_FLAG(sPlayerData[iClientID], RANGE_MP_F_SPECTATOR_LOCKSTEP)
	ENDIF
	
	SWITCH GET_CLIENT_RANGE_STATE(sAllPlayerBD[iClientID])
		CASE RANGE_STATE_Init	// no lockstepping
			CDEBUG3LN(DEBUG_SHOOTRANGE, "RANGE_STATE_Init - SPECTATOR")
			RANGE_PLAYER_WIPE_DATA(sPlayerData[iClientID])
			CLEAR_PLAYER_FLAG(sPlayerData[iClientID], RANGE_MP_F_MATCH_OVER)
			RANGE_SET_ROUND_COUNTDOWN_SOUND_ID( sCurRound, GET_SOUND_ID() )
			
			IF GET_RANGE_SERVER_TOTAL_MATCHES(sServerBD) <> -1 #IF IS_DEBUG_BUILD AND NOT IS_SERVER_FLAG_SET(sServerBD, RANGE_MP_SERVER_F_NEVER_END) #ENDIF
				IF sServerBD.iClient0MatchWins > (GET_RANGE_SERVER_TOTAL_MATCHES(sServerBD) / 2.0) OR sServerBD.iClient1MatchWins > (GET_RANGE_SERVER_TOTAL_MATCHES(sServerBD) / 2.0)
					SET_CLIENT_GAME_STATE(sAllPlayerBD[iClientID], GAME_STATE_LEAVE)
					CDEBUG2LN(DEBUG_SHOOTRANGE, "GET_RANGE_SERVER_TOTAL_MATCHES(", GET_RANGE_SERVER_TOTAL_MATCHES(sServerBD), "), limit reached, sServerBD.iClient0MatchWins=", sServerBD.iClient0MatchWins, ", sServerBD.iClient1MatchWins=", sServerBD.iClient1MatchWins)
					SET_SHARD_BIG_MESSAGE(sAllMenuData.uiShardBM, "RANGE_OVER", "")
					SETTIMERA(0)
				ENDIF
			ENDIF
			
			CLEAR_PLAYER_FLAG(sPlayerData[iClientID], RANGE_MP_F_SPECTATOR_MOVE_ON)
			
			SET_CLIENT_RANGE_STATE(sAllPlayerBD[iClientID], RANGE_STATE_Init_Wait)
		BREAK
		CASE RANGE_STATE_Init_Wait	// no lockstepping
			CDEBUG3LN(DEBUG_SHOOTRANGE, "RANGE_STATE_Init_Wait - SPECTATOR")
			RANGE_MP_InitMainMenu(sAllMenuData, iClientID, sServerBD, sAllPlayerBD)
			SET_CLIENT_RANGE_STATE(sAllPlayerBD[iClientID], RANGE_STATE_MenuUpdate)
			CANCEL_TIMER(sAllMenuData.sMenuTimer)
			
			CLEAR_PLAYER_FLAG(sPlayerData[iClientID], RANGE_MP_F_SPECTATOR_MOVE_ON)
			RANGEMP_UI_TRIGGER_TRANSITION(sAllMenuData, TRUE)
			REMOVE_MENU_HELP_KEYS()
			
			ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_SELECT, "SR_SPEC_CAM")
			ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL, "IB_QUIT")
		BREAK
		CASE RANGE_STATE_MenuUpdate
			CDEBUG3LN(DEBUG_SHOOTRANGE, "RANGE_STATE_MenuUpdate - SPECTATOR")
			IF IS_PLAYER_FLAG_SET(sPlayerData[iClientID], RANGE_MP_F_SPECTATOR_MOVE_ON)
				CLEAR_PLAYER_FLAG(sPlayerData[iClientID], RANGE_MP_F_SPECTATOR_MOVE_ON)
				SET_CLIENT_RANGE_STATE(sAllPlayerBD[iClientID], RANGE_STATE_WaitForAllClients_PostMenu)
//				RANGE_BUSYSPINNER_OFF()
				BREAK
			ENDIF
			IF NOT IS_PLAYER_FLAG_SET(sPlayerData[iClientID], RANGE_MP_F_SPECTATOR_LOCKSTEP)
				eRetVal = RANGE_MP_UpdateMainMenu(sCoreInfo, sAllPlayerBD, iClientID, sAllMenuData, sFuncs, sServerBD, sPlayerData, sCurRound, sParticipantInfo)
				IF (eRetVal = RETVAL_TRUE)
					RESTART_TIMER_NOW(sPlayerData[iClientID].genericTimer)
					CANCEL_TIMER(sAllMenuData.sMenuTimer)
					SET_PLAYER_FLAG(sPlayerData[iClientID], RANGE_MP_F_SPECTATOR_LOCKSTEP)
				ENDIF
			ENDIF
		BREAK
		CASE RANGE_STATE_WaitForAllClients_PostMenu
			CDEBUG3LN(DEBUG_SHOOTRANGE, "RANGE_STATE_WaitForAllClients_PostMenu - SPECTATOR")
			IF IS_PLAYER_FLAG_SET(sPlayerData[iClientID], RANGE_MP_F_SPECTATOR_MOVE_ON)
				CLEAR_PLAYER_FLAG(sPlayerData[iClientID], RANGE_MP_F_SPECTATOR_MOVE_ON)
				SET_CLIENT_RANGE_STATE(sAllPlayerBD[iClientID], RANGE_STATE_DisplayRound_Countdown)
				RANGE_BUSYSPINNER_OFF()
				RANGEMP_UI_TRIGGER_TRANSITION(sAllMenuData, FALSE)
				CLEAR_RANGE_MP_ROUND_TIMER_FLAGS(sCurRound)
				BREAK
			ENDIF
			
			IF NOT IS_PLAYER_FLAG_SET(sPlayerData[iClientID], RANGE_MP_F_SPECTATOR_LOCKSTEP)
				SET_PLAYER_FLAG(sPlayerData[iClientID], RANGE_MP_F_SPECTATOR_LOCKSTEP)
				// Start creating the round.
				CALL sFuncs.fpRoundCreate(sCoreInfo, sCurRound, sAllPlayerBD[iClientID])
				RANGE_MP_INIT_IMPACT_COORDS( sAllPlayerBD[iClientID] )
				
				IF sPlayerData[iClientID].eRSCState >= RSC_PLAYERS1
					CDEBUG2LN(DEBUG_SHOOTRANGE, "Allowing spectator to view targets for a new round")
					sPlayerData[iClientID].eRSCState = RSC_TIGHTEN_ON_TARGETS
				ENDIF
				
				// Stop the UI countdown from processing.
				sAllMenuData.uiCountdown.iBitFlags = 0
				CANCEL_TIMER(sAllMenuData.uiCountdown.CountdownTimer)
				CLEAR_BITMASK_AS_ENUM(sAllPlayerBD[iClientID].iFlags, RANGE_MP_F_CountdownDone)
				
				CDEBUG2LN(DEBUG_SHOOTRANGE, "SPECTATOR - RANGE_STATE_WaitForAllClients_PostMenu: Players starting a new round")
			ENDIF
		BREAK
		CASE RANGE_STATE_DisplayRound_Countdown
			CDEBUG3LN(DEBUG_SHOOTRANGE, "RANGE_STATE_DisplayRound_Countdown - SPECTATOR")
			IF IS_PLAYER_FLAG_SET(sPlayerData[iClientID], RANGE_MP_F_SPECTATOR_MOVE_ON)
				CLEAR_PLAYER_FLAG(sPlayerData[iClientID], RANGE_MP_F_SPECTATOR_MOVE_ON)
				SET_CLIENT_RANGE_STATE(sAllPlayerBD[iClientID], RANGE_STATE_InRound)
				BREAK
			ENDIF
			BOOL bCDRetval
			bCDRetval = UPDATE_MINIGAME_COUNTDOWN_UI(sAllMenuData.uiCountdown, FALSE, FALSE, FALSE)
				
			// Display the countdown and weapon type, as long as the server hasn't moved us to the round:
			IF IS_CLIENT_FLAG_SET(sAllPlayerBD[iClientID], RANGE_MP_F_RoundStartUpdateDone) AND IS_CLIENT_FLAG_SET(sAllPlayerBD[iClientID], RANGE_MP_F_CountdownDone)
			AND NOT IS_PLAYER_FLAG_SET(sPlayerData[iClientID], RANGE_MP_F_SPECTATOR_LOCKSTEP)
				SET_PLAYER_FLAG(sPlayerData[iClientID], RANGE_MP_F_SPECTATOR_LOCKSTEP)
				
				// Has the server moved us to the actual game yet?
				CALL sFuncs.fpRoundStart(sCurRound)
				
				CANCEL_TIMER(sCurRound.sRoundTimer)
				sAllMenuData.fReticleDelay = 0
			ELSE
				IF NOT IS_CLIENT_FLAG_SET(sAllPlayerBD[iClientID], RANGE_MP_F_CountdownDone)
					IF bCDRetval
						SET_CLIENT_FLAG(sAllPlayerBD[iClientID], RANGE_MP_F_CountdownDone)
					ENDIF
				ENDIF
				
				// Move the grid.
				IF NOT IS_CLIENT_FLAG_SET(sAllPlayerBD[iClientID], RANGE_MP_F_RoundStartUpdateDone)					
					IF CALL sFuncs.fpRoundSetup(sCurRound)						
						SET_CLIENT_FLAG(sAllPlayerBD[iClientID], RANGE_MP_F_RoundStartUpdateDone)
						// Setup any UI needs...
						RANGE_MP_SETUP_FLOATING_SCORE_QUEUE(sAllMenuData.floatingScores)
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE RANGE_STATE_InRound
			CDEBUG3LN(DEBUG_SHOOTRANGE, "RANGE_STATE_InRound - SPECTATOR")
			PRINT_HELP_FOREVER("SR_SPEC_BUTTONS")
			
			IF NOT IS_PLAYER_FLAG_SET(sPlayerData[iClientID], RANGE_MP_F_SPECTATOR_LOCKSTEP)
				SET_PLAYER_FLAG(sPlayerData[iClientID], RANGE_MP_F_SPECTATOR_LOCKSTEP)	// Signals that we wait on the observed player to move us on.
			ENDIF
			
			IF ROUND_UPDATE_TIME(sAllPlayerBD[iClientID].sRoundDesc, sCurRound, sAllMenuData, TRUE)	// the round time telling us to move on
			AND NOT IS_SERVER_FLAG_SET(sServerBD, RANGE_MP_SERVER_F_MoveTo_RoundOver_Sk)		// actual server telling us to move on
			AND NOT IS_PLAYER_FLAG_SET(sPlayerData[iClientID], RANGE_MP_F_SPECTATOR_MOVE_ON)	// our "server" telling us to move on
				CALL sFuncs.fpRoundUpdate(sCurRound, iClientID, sPlayerData[iClientID], sAllPlayerBD, sServerBD.iClient0Score, 
						sServerBD.iClient1Score, sAllMenuData, sParticipantInfo, sAllMenuData.eInRoundMenuState <> RANGE_MP_RoundMenu_DisplaySplash)
			ELSE
				CLEAR_PLAYER_FLAG(sPlayerData[iClientID], RANGE_MP_F_SPECTATOR_MOVE_ON)
				CLEAR_PRINTS()
				CLEAR_ALL_FLOATING_HELP()
				
				// Stop the UI countdown from processing.
				sAllMenuData.uiCountdown.iBitFlags = 0
				CANCEL_TIMER(sAllMenuData.uiCountdown.CountdownTimer)
				
				// Need to kill all sounds here... the countdown sound is sometimes playing.
				STOP_SOUND( RANGE_GET_ROUND_COUNTDOWN_SOUND_ID( sCurRound ) )
				PLAY_SOUND_FRONTEND( RANGE_GET_ROUND_COUNTDOWN_SOUND_ID( sCurRound ), "TIMER_STOP_MASTER")
							
				RESTART_TIMER_NOW(sCurRound.sRoundTimer)
				SET_CLIENT_RANGE_STATE(sAllPlayerBD[iClientID], RANGE_STATE_RoundOver)
				
				IF IS_SERVER_FLAG_SET(sServerBD, RANGE_MP_SERVER_F_MoveTo_RoundOver_Sk)
					// Someone's been skunked for too long. Server is now ending the round.
					sCurRound.eRoundEndReason = ROR_Skunked
					CDEBUG2LN(DEBUG_SHOOTRANGE, "Ending due to Skunk")
				ELSE
					sCurRound.eRoundEndReason = ROR_TimeUp
					CDEBUG2LN(DEBUG_SHOOTRANGE, "Ending due to Time Out")
				ENDIF
			ENDIF
		BREAK
		CASE RANGE_STATE_RoundOver
			CDEBUG3LN(DEBUG_SHOOTRANGE, "RANGE_STATE_RoundOver - SPECTATOR")
			IF IS_PLAYER_FLAG_SET(sPlayerData[iClientID], RANGE_MP_F_SPECTATOR_MOVE_ON)
				CLEAR_PLAYER_FLAG(sPlayerData[iClientID], RANGE_MP_F_SPECTATOR_MOVE_ON)
				SET_CLIENT_RANGE_STATE(sAllPlayerBD[iClientID], RANGE_STATE_RoundOver_ScoreUs)
				BREAK
			ENDIF
			
			CALL sFuncs.fpRoundUpdate(sCurRound, iClientID, sPlayerData[iClientID], sAllPlayerBD, sServerBD.iClient0Score, 
					sServerBD.iClient1Score, sAllMenuData, sParticipantInfo, FALSE)
			
			// Display the screen for the round depending on the reason it ended
			// TimeUps don't show the splash text yet, they need extra time to tabulate scores
			IF NOT IS_PLAYER_FLAG_SET(sPlayerData[iClientID], RANGE_MP_F_SPECTATOR_LOCKSTEP) AND NOT IS_ROUND_WAIT_MESSAGE_SHOWING(sAllPlayerBD, sParticipantInfo, sCurRound)
				// Move to the "score us" phase. We had a nice 3 second window for the server to continue processing
				// all scores.
				CLEAR_RANGE_MP_ROUND_TIMER_FLAGS(sCurRound)
				SET_PLAYER_FLAG(sPlayerData[iClientID], RANGE_MP_F_SPECTATOR_LOCKSTEP)
			ENDIF
		BREAK
		CASE RANGE_STATE_RoundOver_ScoreUs
			CDEBUG3LN(DEBUG_SHOOTRANGE, "RANGE_STATE_RoundOver_ScoreUs - SPECTATOR")
			IF IS_PLAYER_FLAG_SET(sPlayerData[iClientID], RANGE_MP_F_SPECTATOR_MOVE_ON)
				CLEAR_PLAYER_FLAG(sPlayerData[iClientID], RANGE_MP_F_SPECTATOR_MOVE_ON)
				SET_CLIENT_RANGE_STATE(sAllPlayerBD[iClientID], RANGE_STATE_RoundOverWait)
				RANGEMP_UI_TRIGGER_TRANSITION(sAllMenuData, TRUE)
				CLEAR_HELP()
				BREAK
			ENDIF
			
			CALL sFuncs.fpRoundUpdate(sCurRound, iClientID, sPlayerData[iClientID], sAllPlayerBD, sServerBD.iClient0Score, 
					sServerBD.iClient1Score, sAllMenuData, sParticipantInfo, FALSE)
			
			// We wait for the server to finish processing scores. Should be done already. Basically, just wiping data
			// here, and getting ready for the scorecard.
			IF NOT IS_PLAYER_FLAG_SET(sPlayerData[iClientID], RANGE_MP_F_SPECTATOR_LOCKSTEP)
				
				// Make both players clear it, just for giggles. Technically, only the host needs to clear.
				CLEAR_BITMASK_AS_ENUM(sAllPlayerBD[iClientID].iFlags, RANGE_MP_F_DoneWithMenuSelections)
				CLEAR_BITMASK_AS_ENUM(sAllPlayerBD[iClientID].iFlags, RANGE_MP_F_MenuTimeExpired)
				CLEAR_BITMASK_AS_ENUM(sAllPlayerBD[iClientID].iFlags, RANGE_MP_F_CountdownDone)
				CLEAR_BITMASK_AS_ENUM(sAllPlayerBD[iClientID].iFlags, RANGE_MP_F_RoundStartUpdateDone)
				
				CANCEL_TIMER(sCurRound.sRoundTimer)
				
				// Set up the closing screen instruction data.
				SETUP_SCORE_CARD(sAllMenuData)
				
				SET_PLAYER_FLAG(sPlayerData[iClientID], RANGE_MP_F_SPECTATOR_LOCKSTEP)
			ENDIF
		BREAK
		CASE RANGE_STATE_RoundOverWait
			CDEBUG3LN(DEBUG_SHOOTRANGE, "RANGE_STATE_RoundOverWait - SPECTATOR")
			IF IS_PLAYER_FLAG_SET(sPlayerData[iClientID], RANGE_MP_F_SPECTATOR_MOVE_ON)
				CLEAR_PLAYER_FLAG(sPlayerData[iClientID], RANGE_MP_F_SPECTATOR_MOVE_ON)
				
				// Setup our next menu!
				REMOVE_MENU_HELP_KEYS()
				RANGE_SET_BUSYSPINNER_STRING(sAllMenuData, "SHR_MP_WAITSC")
				RANGE_START_BUSYSPINNER(sAllMenuData)
				ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL, "IB_QUIT")
				CLEAR_RANGE_MP_MENU_FLAG(sAllMenuData, RMPM_SHOWING_SCLB)
				
				SET_CLIENT_RANGE_STATE(sAllPlayerBD[iClientID], RANGE_STATE_DisplayRoundOver)
				BREAK
			ENDIF
			
			CALL sFuncs.fpRoundUpdate(sCurRound, iClientID, sPlayerData[iClientID], sAllPlayerBD, sServerBD.iClient0Score, 
					sServerBD.iClient1Score, sAllMenuData, sParticipantInfo, FALSE)
			
			// Wait until the message is done
			IF NOT IS_PLAYER_FLAG_SET(sPlayerData[iClientID], RANGE_MP_F_SPECTATOR_LOCKSTEP) AND NOT DISPLAY_ROUND_OVER_MESSAGE(sAllMenuData, sServerBD, iClientID, sAllPlayerBD, sParticipantInfo)
				SET_PLAYER_FLAG(sPlayerData[iClientID], RANGE_MP_F_SPECTATOR_LOCKSTEP)
				IF (sServerBD.eClientWhoWon = CWC_CLIENT0_MATCH) OR (sServerBD.eClientWhoWon = CWC_CLIENT1_MATCH) OR (sServerBD.eClientWhoWon = CWC_NOWINNER_MATCH)
					SET_PLAYER_FLAG(sPlayerData[iClientID], RANGE_MP_F_MATCH_OVER)
				ENDIF
			ENDIF
		BREAK
		
		CASE RANGE_STATE_DisplayRoundOver
			// This should basically mirror the main menu. The one caveat is that we have different prompts.			
			CDEBUG3LN(DEBUG_SHOOTRANGE, "RANGE_STATE_DisplayRoundOver - SPECTATOR")
			
			// We don't want to move on just because the palyer is on the confirmation screen.
			IF (GET_CLIENT_RANGE_STATE(sAllPlayerBD[iRedPlayer]) != RANGE_STATE_ExitConfSplash_MainMenu)
			AND (GET_CLIENT_RANGE_STATE(sAllPlayerBD[iRedPlayer]) != RANGE_STATE_ExitConfSplash_Results)
			AND (GET_CLIENT_RANGE_STATE(sAllPlayerBD[iRedPlayer]) != RANGE_STATE_DisplayRoundOver)
				IF IS_PLAYER_FLAG_SET(sPlayerData[iClientID], RANGE_MP_F_SPECTATOR_MOVE_ON)
					CLEAR_PLAYER_FLAG(sPlayerData[iClientID], RANGE_MP_F_SPECTATOR_MOVE_ON)
					
					// Wipe the current round.
					CALL sFuncs.fpRoundEnd(sCurRound)
					
					SET_CLIENT_RANGE_STATE(sAllPlayerBD[iClientID], RANGE_STATE_WaitForAllClients_PostScorecard)
//					RANGE_BUSYSPINNER_OFF()
					BREAK
				ENDIF
			ENDIF
						
			//CLEAR_RANGE_MP_MENU_FLAG(sAllMenuData, RMPM_SHOWING_SCLB)
			
			// Process the menu for the spectator, though show results.
			RANGE_MP_UpdateMainMenu(sCoreInfo, sAllPlayerBD, iClientID, sAllMenuData, sFuncs, sServerBD, sPlayerData, sCurRound, sParticipantInfo, TRUE, TRUE)
			SET_PLAYER_FLAG(sPlayerData[iClientID], RANGE_MP_F_SPECTATOR_LOCKSTEP)
		
			CALL sFuncs.fpRoundUpdate(sCurRound, iClientID, sPlayerData[iClientID], sAllPlayerBD, sServerBD.iClient0Score, 
					sServerBD.iClient1Score, sAllMenuData, sParticipantInfo, FALSE)
			
			IF IS_HELP_MESSAGE_BEING_DISPLAYED()
				CLEAR_HELP()
			ENDIF
			
			IF sPlayerData[iClientID].eRSCState >= RSC_PLAYERS1
				CDEBUG2LN(DEBUG_SHOOTRANGE, "Allowing spectator to view targets for a new round")
				sPlayerData[iClientID].eRSCState = RSC_TIGHTEN_ON_TARGETS
			ENDIF
		BREAK
		
		CASE RANGE_STATE_WaitForAllClients_PostScorecard
			CDEBUG3LN(DEBUG_SHOOTRANGE, "RANGE_STATE_WaitForAllClients_PostScorecard - SPECTATOR")
			
			IF NOT IS_PLAYER_FLAG_SET(sPlayerData[iClientID], RANGE_MP_F_SPECTATOR_LOCKSTEP)
				SET_PLAYER_FLAG(sPlayerData[iClientID], RANGE_MP_F_SPECTATOR_LOCKSTEP)
			ENDIF
			
			// Just display a message that we're waiting for all clients. Server will signal us when it's time to leave.
			// Check that message first.
			IF IS_PLAYER_FLAG_SET(sPlayerData[iClientID], RANGE_MP_F_MATCH_OVER) AND IS_PLAYER_FLAG_SET(sPlayerData[iClientID], RANGE_MP_F_SPECTATOR_MOVE_ON)
				CLEAR_PLAYER_FLAG(sPlayerData[iClientID], RANGE_MP_F_SPECTATOR_MOVE_ON)
				CANCEL_TIMER(sCurRound.sRoundTimer)
				
				// Reset our sound
				IF ( RANGE_GET_ROUND_COUNTDOWN_SOUND_ID( sCurRound ) <> -1)
					RELEASE_SOUND_ID( RANGE_GET_ROUND_COUNTDOWN_SOUND_ID( sCurRound ) )
					RANGE_SET_ROUND_COUNTDOWN_SOUND_ID( sCurRound, -1 )
				ENDIF
								
				// Go back to the menu.
				SET_CLIENT_RANGE_STATE(sAllPlayerBD[iClientID], RANGE_STATE_Init)
								
			ELIF NOT IS_PLAYER_FLAG_SET(sPlayerData[iClientID], RANGE_MP_F_MATCH_OVER) AND IS_PLAYER_FLAG_SET(sPlayerData[iClientID], RANGE_MP_F_SPECTATOR_MOVE_ON)
				CLEAR_PLAYER_FLAG(sPlayerData[iClientID], RANGE_MP_F_SPECTATOR_MOVE_ON)
				// That was just one round. Go back.
				CALL sFuncs.fpRoundCreate(sCoreInfo, sCurRound, sAllPlayerBD[iClientID])
				RANGE_MP_INIT_IMPACT_COORDS( sAllPlayerBD[iClientID] )
				RANGEMP_UI_TRIGGER_TRANSITION(sAllMenuData, FALSE)
				
				// Reset countdown data.
				sAllMenuData.uiCountdown.iBitFlags = 0
				CANCEL_TIMER(sAllMenuData.uiCountdown.CountdownTimer)
				CLEAR_BITMASK_AS_ENUM(sAllPlayerBD[iClientID].iFlags, RANGE_MP_F_CountdownDone)
				
				SETUP_PLAYER_FOR_ANOTHER_ROUND(sAllPlayerBD[iClientID], sPlayerData[iClientID])
				
				IF IS_CLIENT_FLAG_SET(sAllPlayerBD[iClientID], RANGE_MP_F_IsSpectator)
					SET_ENTITY_COORDS(playerPed, sCoreInfo.vSpectatorPos)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), sCoreInfo.fSpectatorHead)
				ELSE
					SET_ENTITY_COORDS(PLAYER_PED_ID(), sPlayerData[iClientID].vPlayerStartPos)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), sPlayerData[iClientID].fPlayerStartHead)
				ENDIF
				CDEBUG2LN(DEBUG_SHOOTRANGE, "RANGE_STATE_MenuUpdate Setting coords and heading because we're at a crap view")
				
				RANGEMP_SET_PLAYER_CAM_DOWNRANGE(sPlayerData[iClientID], sAllPlayerBD[iClientID].sRoundDesc.eChallenge = CI_GRID)

			ELSE
				// Okay, it's not time to continue on, just display a message that we're waiting for all players...
				DISPLAY_WAIT_FOR_SCORECARD_EXIT(sAllMenuData)
			ENDIF
			
			sAllMenuData.bHasSetSplashMsg = FALSE
		BREAK
		CASE RANGE_STATE_ExitConfSplash_MainMenu
		CASE RANGE_STATE_ExitConfSplash_Results
			// This is a holding state...
			//CDEBUG3LN(DEBUG_SHOOTRANGE, "RANGE_STATE_ExitConfSplash_MainMenu - SPECTATOR")
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

FUNC BOOL HAS_LB_TIMER_TIMED_OUT(RangeMP_ServerBD & sServerBD) 
    IF NOT HAS_NET_TIMER_STARTED(sServerBD.tLeaderboardTimeout) 
        RETURN FALSE 
    ELSE 
        IF GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), sServerBD.tLeaderboardTimeout.timer) >= VIEW_LEADERBOARD_TIME  
			RETURN TRUE 
        ENDIF 
    ENDIF 
	
    RETURN FALSE 
ENDFUNC


PROC RANGE_MP_UPDATE_REMATCH_POLLING(RangeMP_ServerBD & sServerBD, RangeMP_PlayerBD & sThisPlayerBD, RangeMP_PlayerData & sThisPlayerData)
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		IF NOT HAS_NET_TIMER_STARTED(sServerBD.tLeaderboardTimeout)
			START_NET_TIMER(sServerBD.tLeaderboardTimeout)
		ENDIF

		FMMC_EOM_SERVER_RESET_PLAYER_COUNTS(sServerBD.sPlayerCounts)
		INT iRematchLoop
		IF NETWORK_IS_GAME_IN_PROGRESS()
			REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iRematchLoop
				IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iRematchLoop))
					FMMC_EOM_SERVER_PLAYER_PROCESSING(sServerBD.sPlayerCounts, iRematchLoop)
				ENDIF
			ENDREPEAT
		ELSE
			CERRORLN(DEBUG_SHOOTRANGE, "RANGE_MP_UPDATE_REMATCH_POLLING :: NOT NETWORK_IS_GAME_IN_PROGRESS()")
		ENDIF

		FMMC_EOM_SERVER_END_OF_FRAME_PROCESSING(sServerBD.sPlayerCounts, sServerBD.sFMMC_EOM, FMMC_TYPE_MG_SHOOTING_RANGE, 0)
	ENDIF
	IF HAS_NET_TIMER_STARTED(sServerBD.tLeaderboardTimeout)
		IF MAINTAIN_MINI_GAME_END_OF_MISSION_SCREEN(sThisPlayerData.sFMMC_EOMDetails, sServerBD.sFMMC_EOM, HAS_LB_TIMER_TIMED_OUT(sServerBD), sServerBD.tLeaderboardTimeout.timer, VIEW_LEADERBOARD_TIME, "END_LBD_CONTN")
			//Go to cleanup state.
			SET_CLIENT_GAME_STATE(sThisPlayerBD, GAME_STATE_TERMINATE)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles the exit splash state
/// PARAMS:
///    sServerBD - 
///    sAllPlayerBD - 
///    iClientID - 
///    sAllMenuData - 
///    sPlayerData - 
///    sCurRound - 
///    sFuncs - 
///    sParticipantInfo - 
PROC RANGE_HANDLE_EXIT_SPLASH(RangeMP_ServerBD & sServerBD, RangeMP_PlayerBD & sAllPlayerBD[], INT iClientID, RangeMP_MenuData & sAllMenuData, RangeMP_PlayerData & sPlayerData[], RangeMP_Round & sCurRound, RangeMP_RoundFuncs& sFuncs, RangeMP_ParticipantInfo & sParticipantInfo)
	
	RANGE_PRINT_TO_SCREEN("RANGE_STATE_ExitConfSplash_MainMenu", RANGE_STATE_PRINT_HEIGHT)
	// The player attempted to quit during the main menu. This means we need to display the splash, but if the round starts, force us to move.
	THREE_STATE_RETVAL eRetval = UPDATE_EXIT_SPLASH_MAINMENU(sServerBD, iClientID, sParticipantInfo)
		
	IF IS_PLAYER_FLAG_SET(sPlayerData[iClientID], RANGE_MP_F_QUIT_CONFIRMED)
		
		IF GET_PLAYER_SWITCH_STATE() >= SWITCH_STATE_JUMPCUT_ASCENT
			SET_CLIENT_GAME_STATE(sAllPlayerBD[iClientID], GAME_STATE_TERMINATE)
			RANGE_CLEAR_ENDED_MISSION_AT_LEADERBOARD_VOTE_OR_TIME_OUT()
			BUSYSPINNER_OFF()
		ENDIF
		
		EXIT
		
	ENDIF
	
	IF (eRetval = RETVAL_TRUE)
		// RETVAL_TRUE means we're leaving the range.
		CDEBUG2LN(DEBUG_SHOOTRANGE, "RANGE_HANDLE_EXIT_SPLASH setting skyswoop, eRetval = RETVAL_TRUE")
		SET_SKYSWOOP_UP(default, default, FALSE, SWITCH_TYPE_LONG, TRUE)
		SET_PLAYER_FLAG(sPlayerData[iClientID], RANGE_MP_F_QUIT_CONFIRMED)
		
	ELIF (eRetval = RETVAL_FALSE)
		CDEBUG2LN(DEBUG_SHOOTRANGE, "IF (eRetval = RETVAL_FALSE)")
		
		IF IS_CLIENT_FLAG_SET(sAllPlayerBD[iClientID], RANGE_MP_F_IsSpectator)
			// Spectators always get the same thing. Spectating and quit.
			REMOVE_MENU_HELP_KEYS()
			SET_RESULT_SCREEN_DISPLAYING_STATE(FALSE)
			RANGE_SET_BUSYSPINNER_STRING(sAllMenuData, "SHR_SPEC_RND")
			RANGE_START_BUSYSPINNER(sAllMenuData)
			ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL, "IB_QUIT")
		ENDIF
		
		CDEBUG2LN(DEBUG_SHOOTRANGE, "Setting up keys for RANGE_STATE_ExitConfSplash_MainMenu/RANGE_STATE_ExitConfSplash_Results")
		
		// Never stack.
		SET_MENU_HELP_KEYS_STACKED(FALSE)
		
		// Put us back wherever we were.
		IF (GET_CLIENT_RANGE_STATE(sAllPlayerBD[iClientID]) = RANGE_STATE_ExitConfSplash_MainMenu)
			SET_CLIENT_RANGE_STATE(sAllPlayerBD[iClientID], RANGE_STATE_MenuUpdate)
		ELSE
			SET_CLIENT_RANGE_STATE(sAllPlayerBD[iClientID], RANGE_STATE_DisplayRoundOver)
		ENDIF
	ENDIF
	
	// We can also time out!
	IF NOT IS_RANGE_ONE_PLAYER()
		// Got RETVAL_NONE, which means we need to just handle calls to move into the range as normal.
		// Basically, we just need to worry about the timeout..
		IF (GET_TIMER_IN_SECONDS(sAllMenuData.sMenuTimer) > RANGE_MP_MENU_TIMEOUT)
			// A player has missed the menu selection. Move states. Wait for server.
			CANCEL_TIMER(sAllMenuData.sMenuTimer)
			
			IF (GET_CLIENT_RANGE_STATE(sAllPlayerBD[iClientID]) = RANGE_STATE_ExitConfSplash_MainMenu)
				SET_CLIENT_RANGE_STATE(sAllPlayerBD[iClientID], RANGE_STATE_MenuFail_ServerUpdatePicker)
			ELSE
				SET_CLIENT_RANGE_STATE(sAllPlayerBD[iClientID], RANGE_STATE_WaitForAllClients_PostScorecard)
				CLEAR_HELP()
				CLEAR_RANGE_MP_MENU_FLAG(sAllMenuData, RMPM_SHOWING_SCLB)
				
				IF IS_SERVER_FLAG_SET(sServerBD, RANGE_MP_SERVER_F_Match_Over)
					// Handle the weapon while it's holstered.
					RANGE_PLAYER_REMOVE_WEAPON(sPlayerData[iClientID])
				ENDIF
				
				// Destroy the current round props.
				CALL sFuncs.fpRoundEnd(sCurRound)
			ENDIF
			
			IF NOT IS_RANGE_ONE_PLAYER()
				STRING sMsg
				sMsg = PICK_STRING( iClientID = SERVER_GET_PLAYER_IN_CONTROL( sServerBD ), "O_CHOOSE", "U_CHOOSE" )
				RANGE_SET_BUSYSPINNER_STRING( sAllMenuData, sMsg )
				RANGE_START_BUSYSPINNER( sAllMenuData )
			ENDIF
		ENDIF
		
		// We're sitting on the screen that asks us if we want to leave.
		IF (iClientID != SERVER_GET_PLAYER_IN_CONTROL(sServerBD))
			IF HANDLE_REPLICATE(sAllPlayerBD, iClientID, sAllMenuData, sParticipantInfo, sFuncs)
				RESTART_TIMER_NOW(sPlayerData[iClientID].genericTimer)
				CANCEL_TIMER(sAllMenuData.sMenuTimer)
				
				IF sParticipantInfo.iShooters[0] != k_InvalidParticipantID
					RANGEMP_SET_PLAYER_CAM_DOWNRANGE(sPlayerData[sParticipantInfo.iShooters[0]])
				ENDIF
				CLEAR_CLIENT_FLAG(sAllPlayerBD[iClientID], RANGE_MP_F_GivenWeapon)
				SET_CLIENT_RANGE_STATE(sAllPlayerBD[iClientID], RANGE_STATE_WaitForAllClients_PostMenu)
//						RANGE_BUSYSPINNER_OFF()
			ENDIF
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE:
///    General client update state for the range.
/// RETURNS:
///    TRUE to continue running. FALSE to stop.
FUNC BOOL RANGE_MP_UPDATE(RangeMP_ServerBD & sServerBD, RangeMP_PlayerBD & sAllPlayerBD[], INT iClientID, RangeMP_MenuData & sAllMenuData, 
		RangeMP_PlayerData & sPlayerData[], RangeMP_Round & sCurRound, RangeMP_RoundFuncs& sFuncs, RANGE_END_CONDITION	& eTerminateReason, RangeMP_CoreData & sCoreData, RangeMP_ParticipantInfo & sParticipantInfo )
	// Used for various function returns.
	THREE_STATE_RETVAL eRetval
	
	IF STANDARD_RANGE_UPDATE_CALLS(sServerBD, sAllPlayerBD, iClientID, sAllMenuData, sPlayerData, sCurRound, eTerminateReason, sCoreData, sParticipantInfo)
		RETURN TRUE
	ENDIF
	
	// TEMP:
	GRID_INDEX eIdx 
	REPEAT MAX_GRID_LOCS eIdx
		GET_TARGET_LOC_FROM_GRID_INDEX(sCoreData, eIdx)
	ENDREPEAT
			
	INT iRedPlayer = sParticipantInfo.iShooters[RED_PLAYER_CLIENT_ID]
	INT iBluePlayer = sParticipantInfo.iShooters[BLUE_PLAYER_CLIENT_ID]
	
	IF iRedPlayer = k_InvalidParticipantID
		CDEBUG3LN( DEBUG_SHOOTRANGE, "RANGE_MP_UPDATE - iRedPlayer = k_InvalidParticipantID, this is potentially fatal." )
	ENDIF
	
	IF iBluePlayer = k_InvalidParticipantID
		CDEBUG3LN( DEBUG_SHOOTRANGE, "RANGE_MP_UPDATE - iBluePlayer = k_InvalidParticipantID, this is potentially fatal." )
	ENDIF
			
	SWITCH GET_CLIENT_RANGE_STATE(sAllPlayerBD[iClientID])
		CASE RANGE_STATE_Init
			RANGE_PRINT_TO_SCREEN("RANGE_STATE_Init", RANGE_STATE_PRINT_HEIGHT)
			
			// Wipe all data stored about previous rounds.
			RANGE_PLAYER_WIPE_DATA(sPlayerData[iClientID])
			
			IF NOT IS_RANGE_ONE_PLAYER()
				// Store our shooting skill locally, and send it to the other guy.
				GET_AND_SEND_RANGE_PLAYER_SHOOTING_SKILL(sAllPlayerBD, iClientID)
			ENDIF
			
			// Get a sound ID for the countdowns
			RANGE_SET_ROUND_COUNTDOWN_SOUND_ID( sCurRound, GET_SOUND_ID() )
			RANGE_PLAYER_DISABLE_SHOOTING()
			
			IF GET_RANGE_MP_SEEN_TUTORIAL()
				SET_CLIENT_FLAG(sAllPlayerBD[iClientID], RANGE_MP_F_TutorialSkipped)
			ENDIF
			
			SET_CLIENT_RANGE_STATE(sAllPlayerBD[iClientID], RANGE_STATE_Init_Wait)
			
			IF NOT IS_CLIENT_FLAG_SET(sAllPlayerBD[iClientID], RANGE_MP_F_IsSpectator)
				PUT_ON_RANGE_SAFETY_GEAR(sCoreData)
			ENDIF
			
			CDEBUG2LN( DEBUG_SHOOTRANGE, "GET_RANGE_SERVER_TOTAL_MATCHES(sServerBD) = ", GET_RANGE_SERVER_TOTAL_MATCHES(sServerBD) )
			IF NOT IS_RANGE_ONE_PLAYER() #IF IS_DEBUG_BUILD AND NOT IS_SERVER_FLAG_SET(sServerBD, RANGE_MP_SERVER_F_NEVER_END) #ENDIF
				IF sServerBD.iClient0MatchWins > TO_FLOAT(GET_RANGE_SERVER_TOTAL_MATCHES(sServerBD)) / 2.0
					HIDE_UI_DURING_RANGE()
					SET_CLIENT_GAME_STATE(sAllPlayerBD[iClientID], GAME_STATE_LEAVE)
					sCoreData.iWinnings = BROADCAST_BETTING_MISSION_FINISHED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(sParticipantInfo.iShooters[0])))
					CDEBUG2LN(DEBUG_SHOOTRANGE, "BETTING RESULT: ", sCoreData.iWinnings)
					CDEBUG2LN(DEBUG_SHOOTRANGE, "BROADCAST_BETTING_MISSION_FINISHED(sServerBD.iClient0MatchWins=", sServerBD.iClient0MatchWins, "), GET_RANGE_SERVER_TOTAL_MATCHES(", GET_RANGE_SERVER_TOTAL_MATCHES(sServerBD), ")")
					INT iChallengesWon
					HUD_COLOURS eColor
					IF iClientID = sParticipantInfo.iShooters[0]
						iChallengesWon = sServerBD.iClient0MatchWins
						eColor = HUD_COLOUR_WHITE
					ELSE
						iChallengesWon = sServerBD.iClient1MatchWins
						eColor = HUD_COLOUR_RED
					ENDIF
					SET_SHARD_BIG_MESSAGE_WITH_2_NUMBERS_IN_STRAPLINE(sAllMenuData.uiShardBM, "RANGE_OVER", iChallengesWon, GET_RANGE_SERVER_TOTAL_MATCHES(sServerBD), "ROUNDS_WON", default, default, eColor)
					RANGE_SET_ENDED_MISSION_AT_LEADERBOARD_VOTE_OR_TIME_OUT()
					SETTIMERA(0)
					
					IF RANGE_GET_NUM_SHOOTERS( sParticipantInfo ) > 1
						sAllPlayerBD[iClientID].bRematchEligible = TRUE
					ENDIF
					SET_CLIENT_FLAG(sAllPlayerBD[iClientID], RANGE_MP_F_ContinueAfterVotePassing)
					SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_AM_PLAY_SHOOTING_RANGE)
					
					sAllPlayerBD[iClientID].iXPAward = RANGE_MP_AWARD_MATCH_RP(sServerBD, PICK_INT(sParticipantInfo.iShooters[0] = iClientID, 0, 1), sParticipantInfo.iShooters[0] = iClientID)
					
				ELIF sServerBD.iClient1MatchWins > TO_FLOAT(GET_RANGE_SERVER_TOTAL_MATCHES(sServerBD)) / 2.0
					HIDE_UI_DURING_RANGE()
					SET_CLIENT_GAME_STATE(sAllPlayerBD[iClientID], GAME_STATE_LEAVE)
					sCoreData.iWinnings = BROADCAST_BETTING_MISSION_FINISHED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(sParticipantInfo.iShooters[1])))
					CDEBUG2LN(DEBUG_SHOOTRANGE, "BETTING RESULT: ", sCoreData.iWinnings)
					CDEBUG2LN(DEBUG_SHOOTRANGE, "BROADCAST_BETTING_MISSION_FINISHED(sServerBD.iClient1MatchWins=", sServerBD.iClient1MatchWins, "), GET_RANGE_SERVER_TOTAL_MATCHES(", GET_RANGE_SERVER_TOTAL_MATCHES(sServerBD), ")")
					INT iChallengesWon
					HUD_COLOURS eColor
					IF iClientID = sParticipantInfo.iShooters[0]
						iChallengesWon = sServerBD.iClient0MatchWins
						eColor = HUD_COLOUR_RED
					ELSE
						iChallengesWon = sServerBD.iClient1MatchWins
						eColor = HUD_COLOUR_WHITE
					ENDIF
					SET_SHARD_BIG_MESSAGE_WITH_2_NUMBERS_IN_STRAPLINE(sAllMenuData.uiShardBM, "RANGE_OVER", iChallengesWon, GET_RANGE_SERVER_TOTAL_MATCHES(sServerBD), "ROUNDS_WON", default, default, eColor)
					RANGE_SET_ENDED_MISSION_AT_LEADERBOARD_VOTE_OR_TIME_OUT()
					SETTIMERA(0)
					
					IF RANGE_GET_NUM_SHOOTERS( sParticipantInfo ) > 1
						sAllPlayerBD[iClientID].bRematchEligible = TRUE
					ENDIF
					SET_CLIENT_FLAG(sAllPlayerBD[iClientID], RANGE_MP_F_ContinueAfterVotePassing)
					SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_AM_PLAY_SHOOTING_RANGE)
					
					sAllPlayerBD[iClientID].iXPAward = RANGE_MP_AWARD_MATCH_RP(sServerBD, PICK_INT(sParticipantInfo.iShooters[0] = iClientID, 0, 1), sParticipantInfo.iShooters[1] = iClientID)
				ENDIF
			ELSE
				IF GET_RANGE_SERVER_TOTAL_MATCHES(sServerBD) <> -1 #IF IS_DEBUG_BUILD AND NOT IS_SERVER_FLAG_SET(sServerBD, RANGE_MP_SERVER_F_NEVER_END) #ENDIF
					IF sServerBD.iClient0MatchWins > GET_RANGE_SERVER_TOTAL_MATCHES(sServerBD) / 2 OR sServerBD.iClient1MatchWins > GET_RANGE_SERVER_TOTAL_MATCHES(sServerBD) / 2
						SET_CLIENT_GAME_STATE(sAllPlayerBD[iClientID], GAME_STATE_LEAVE)
						CDEBUG2LN(DEBUG_SHOOTRANGE, "GET_RANGE_SERVER_TOTAL_MATCHES(", GET_RANGE_SERVER_TOTAL_MATCHES(sServerBD), "), limit reached, sServerBD.iClient0MatchWins=", sServerBD.iClient0MatchWins, ", sServerBD.iClient1MatchWins=", sServerBD.iClient1MatchWins)
						SET_SHARD_BIG_MESSAGE(sAllMenuData.uiShardBM, "RANGE_OVER", "")
						RANGE_SET_ENDED_MISSION_AT_LEADERBOARD_VOTE_OR_TIME_OUT()
						SETTIMERA(0)
						SET_CLIENT_FLAG(sAllPlayerBD[iClientID], RANGE_MP_F_ContinueAfterVotePassing)
						SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_AM_PLAY_SHOOTING_RANGE)
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE RANGE_STATE_Init_Wait
			RANGE_PRINT_TO_SCREEN("RANGE_STATE_Init_Wait", RANGE_STATE_PRINT_HEIGHT)	// This state waits for the shooting skill to finish broadcasting. 
			IF IS_SERVER_FLAG_SET(sServerBD, RANGE_MP_SERVER_F_FirstInit)	// Only needed this once because Skill is lockstepped elsewhere after rounds are over.
				// Set up our menu regardless. Client will just replicate host.
				RANGE_MP_InitMainMenu(sAllMenuData, iClientID, sServerBD, sAllPlayerBD)
				RANGEMP_UI_TRIGGER_TRANSITION(sAllMenuData, TRUE)
				
				REMOVE_MENU_HELP_KEYS()
				IF IS_CLIENT_FLAG_SET(sAllPlayerBD[iClientID], RANGE_MP_F_IsSpectator)
					// Spectator gets QUIT, SPINNER
					RANGE_SET_BUSYSPINNER_STRING(sAllMenuData, "SHR_SPEC_RND")
					ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL, "IB_QUIT")
					RANGE_START_BUSYSPINNER(sAllMenuData)
				
				ELIF (iClientID = SERVER_GET_PLAYER_IN_CONTROL(sServerBD)) 
					// You're in control. There may be another player. Who knows.
					IF NOT IS_RANGE_ONE_PLAYER()
						RANGE_SET_BUSYSPINNER_STRING(sAllMenuData, "U_CHOOSE")
						RANGE_START_BUSYSPINNER(sAllMenuData)
					ENDIF
					ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_ACCEPT, "FE_HLP4")
					ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL, "IB_QUIT")
				ELSE
					// You're not in control, and you're not a spectator. You're the second player.
					RANGE_SET_BUSYSPINNER_STRING(sAllMenuData, "O_CHOOSE")
					RANGE_START_BUSYSPINNER(sAllMenuData)
					ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL, "IB_QUIT")
				ENDIF
				SET_MENU_HELP_KEYS_STACKED(FALSE)
				
				IF IS_CLIENT_FLAG_SET(sAllPlayerBD[iClientID], RANGE_MP_F_IsSpectator)
					SET_ENTITY_COORDS(playerPed, sCoreData.vSpectatorPos)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), sCoreData.fSpectatorHead)
				ELSE
					SET_ENTITY_COORDS(PLAYER_PED_ID(), sPlayerData[iClientID].vPlayerStartPos)
					SET_ENTITY_HEADING(PLAYER_PED_ID(),sPlayerData[iClientID].fPlayerStartHead)
				ENDIF
				
				// this to turn control fully back on:
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE, NSPC_CAN_BE_TARGETTED | NSPC_ALLOW_PLAYER_DAMAGE )
				SET_PLAYER_CONTROL( PLAYER_ID(), FALSE )
				IF NOT IS_PED_INJURED( PLAYER_PED_ID() )
					FORCE_PED_AI_AND_ANIMATION_UPDATE( PLAYER_PED_ID() )
				ENDIF
				
				CDEBUG2LN(DEBUG_SHOOTRANGE, "NET_SET_PLAYER_CONTROL to TRUE, Setting position and heading")
				SET_CLIENT_RANGE_STATE(sAllPlayerBD[iClientID], RANGE_STATE_MenuUpdate)
				CDEBUG2LN(DEBUG_SHOOTRANGE, "Created Main Menu and moving state on to RANGE_STATE_MenuUpdate!")
			ENDIF
		BREAK
		CASE RANGE_STATE_MenuUpdate	
			RANGE_PRINT_TO_SCREEN("RANGE_STATE_MenuUpdate", RANGE_STATE_PRINT_HEIGHT)
			
			IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
			ENDIF
			
			IF IS_RANGE_ONE_PLAYER() AND SERVER_GET_PLAYER_IN_CONTROL(sServerBD) <> iClientID
				SET_RANGE_SERVER_MENU_PICKER(sServerBD, iClientID)
			ENDIF
			
			// Display the menu no matter what. Client will replicate host.
			eRetVal = RANGE_MP_UpdateMainMenu(sCoreData, sAllPlayerBD, iClientID, sAllMenuData, sFuncs, sServerBD, sPlayerData, sCurRound, sParticipantInfo)
			IF (eRetVal = RETVAL_TRUE)
				// We are supposed to be making a selection! If we got here, that means that we've selected a category, weapon and challenge.
				// Need to communicate to the server that we've selected a round, and he'll move us on.
				RESTART_TIMER_NOW(sPlayerData[iClientID].genericTimer)
				CANCEL_TIMER(sAllMenuData.sMenuTimer)
				
				IF IS_CLIENT_FLAG_SET(sAllPlayerBD[iClientID], RANGE_MP_F_IsSpectator)
					SET_ENTITY_COORDS(playerPed, sCoreData.vSpectatorPos)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), sCoreData.fSpectatorHead)
				ELSE
					SET_ENTITY_COORDS(PLAYER_PED_ID(), sPlayerData[iClientID].vPlayerStartPos)
					SET_ENTITY_HEADING(PLAYER_PED_ID(),sPlayerData[iClientID].fPlayerStartHead)
				ENDIF
				
				CDEBUG2LN(DEBUG_SHOOTRANGE, "RANGE_STATE_MenuUpdate Setting coords and heading because B*1136404")
				
				RANGEMP_SET_PLAYER_CAM_DOWNRANGE(sPlayerData[iClientID], sAllPlayerBD[iClientID].sRoundDesc.eChallenge = CI_GRID)
				CLEAR_CLIENT_FLAG(sAllPlayerBD[iClientID], RANGE_MP_F_GivenWeapon)
				SET_LOADING_ICON_INACTIVE()				
				SET_CLIENT_RANGE_STATE(sAllPlayerBD[iClientID], RANGE_STATE_WaitForAllClients_PostMenu)
//				RANGE_BUSYSPINNER_OFF()
				
			ELIF (eRetVal = RETVAL_FALSE)
				// A player has missed the menu selection. Move states. Wait for server.
				SET_CLIENT_RANGE_STATE(sAllPlayerBD[iClientID], RANGE_STATE_MenuFail_ServerUpdatePicker)
				IF NOT IS_RANGE_ONE_PLAYER()
					STRING sMsg
					sMsg = PICK_STRING( iClientID = SERVER_GET_PLAYER_IN_CONTROL( sServerBD ), "O_CHOOSE", "U_CHOOSE" )
					RANGE_SET_BUSYSPINNER_STRING( sAllMenuData, sMsg )
					RANGE_START_BUSYSPINNER( sAllMenuData )
				ENDIF
			ENDIF
			
			RANGE_PLAYER_DISABLE_SHOOTING()
		BREAK
		
		CASE RANGE_STATE_WaitForAllClients_PostMenu
			// Here, we're just waiting for the server to move us to the countdown.
			IF IS_SERVER_FLAG_SET(sServerBD, RANGE_MP_SERVER_F_MoveTo_Countdown)
				// Start creating the round.
				CALL sFuncs.fpRoundCreate(sCoreData, sCurRound, sAllPlayerBD[iClientID])
				RANGE_MP_INIT_IMPACT_COORDS( sAllPlayerBD[iClientID] )
				
				// Stop the UI countdown from processing.
				sAllMenuData.uiCountdown.iBitFlags = 0
				CANCEL_TIMER(sAllMenuData.uiCountdown.CountdownTimer)
				CLEAR_BITMASK_AS_ENUM(sAllPlayerBD[iClientID].iFlags, RANGE_MP_F_CountdownDone)
				
				IF NOT IS_PLAYER_DEAD(PLAYER_ID()) AND NOT IS_PED_INJURED(PLAYER_PED_ID())
					CDEBUG2LN(DEBUG_SHOOTRANGE, "CASE RANGE_STATE_WaitForAllClients_PostMenu :: SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), TRUE)")
					SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), TRUE)
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE, SPC_REMOVE_EXPLOSIONS|SPC_REMOVE_PROJECTILES|SPC_REMOVE_FIRES|SPC_CLEAR_TASKS)
				ENDIF
				SET_LOADING_ICON_INACTIVE()
				RANGEMP_UI_TRIGGER_TRANSITION(sAllMenuData, FALSE)
				
				SET_CLIENT_RANGE_STATE(sAllPlayerBD[iClientID], RANGE_STATE_DisplayRound_Countdown)
				CLEAR_RANGE_MP_ROUND_TIMER_FLAGS(sCurRound)
				CDEBUG2LN(DEBUG_SHOOTRANGE, "RANGE_STATE_WaitForAllClients_PostMenu: Players starting a new round")
			ENDIF
		BREAK
		
		CASE RANGE_STATE_MenuFail_ServerUpdatePicker
			RANGE_PRINT_TO_SCREEN("RANGE_STATE_MenuFail_ServerUpdatePicker", RANGE_STATE_PRINT_HEIGHT)
			
			// Display the menu no matter what. Client will replicate host. We're not taking info from this, just keeping the visual up.
			eRetVal = RANGE_MP_UpdateMainMenu(sCoreData, sAllPlayerBD, iClientID, sAllMenuData, sFuncs, sServerBD, sPlayerData, sCurRound, sParticipantInfo, FALSE)
			
			// We're sitting in here, waiting for the server to tell us what to do.
			// If it was the first time a player missed his state, we'll get sent back to the menu with a new picker.
			// If it was the second time, the server is going to order us to quit.
			IF IS_SERVER_FLAG_SET(sServerBD, RANGE_MP_SERVER_F_Quit_NoMenuInput) OR IS_RANGE_ONE_PLAYER()
				FORCE_RANGE_MENU_CHOICE(sAllPlayerBD, sAllMenuData)
				
				// We are supposed to be making a selection! If we got here, that means that we've selected a category, weapon and challenge.
				// Need to communicate to the server that we've selected a round, and he'll move us on.
				RESTART_TIMER_NOW(sPlayerData[iClientID].genericTimer)
				CANCEL_TIMER(sAllMenuData.sMenuTimer)
				
				IF IS_CLIENT_FLAG_SET(sAllPlayerBD[iClientID], RANGE_MP_F_IsSpectator)
					SET_ENTITY_COORDS(playerPed, sCoreData.vSpectatorPos)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), sCoreData.fSpectatorHead)
				ELSE
					SET_ENTITY_COORDS(PLAYER_PED_ID(), sPlayerData[iClientID].vPlayerStartPos)
					SET_ENTITY_HEADING(PLAYER_PED_ID(),sPlayerData[iClientID].fPlayerStartHead)
				ENDIF
				CDEBUG2LN(DEBUG_SHOOTRANGE, "RANGE_STATE_MenuUpdate Setting coords and heading because B*1136404")
				
				RANGEMP_SET_PLAYER_CAM_DOWNRANGE(sPlayerData[iClientID], sAllPlayerBD[iClientID].sRoundDesc.eChallenge = CI_GRID)
				CLEAR_CLIENT_FLAG(sAllPlayerBD[iClientID], RANGE_MP_F_GivenWeapon)		
				SET_CLIENT_RANGE_STATE(sAllPlayerBD[iClientID], RANGE_STATE_WaitForAllClients_PostMenu)
//				RANGE_BUSYSPINNER_OFF()
				
				SET_UP_RANGE_FUNCTION_POINTERS(sFuncs, sAllPlayerBD, iClientID)
				
			ELIF IS_SERVER_FLAG_SET(sServerBD, RANGE_MP_SERVER_F_GoBackToMenu)
				// Slap on the wrist. Resume menu selections. Server has logged it.
				CANCEL_TIMER(sAllMenuData.sMenuTimer)
				SET_CLIENT_RANGE_STATE(sAllPlayerBD[iClientID], RANGE_STATE_MenuUpdate)
				PLAY_SOUND_FRONTEND(-1, "BACK", "HUD_FRONTEND_DEFAULT_SOUNDSET")
				CDEBUG2LN(DEBUG_SHOOTRANGE, "IS_SERVER_FLAG_SET(sServerBD, RANGE_MP_SERVER_F_GoBackToMenu), playing sound")

			ELIF IS_SERVER_FLAG_SET(sServerBD, RANGE_MP_SERVER_F_MenuFailDesync)
				CDEBUG2LN(DEBUG_SHOOTRANGE, "Server is telling us to move on from a MenuFail desync!")
				// Get the menu hosts selections
				HANDLE_REPLICATE(sAllPlayerBD, iClientID, sAllMenuData, sParticipantInfo, sFuncs)
				RESTART_TIMER_NOW(sPlayerData[iClientID].genericTimer)
				CANCEL_TIMER(sAllMenuData.sMenuTimer)
				
				RANGEMP_SET_PLAYER_CAM_DOWNRANGE(sPlayerData[iClientID], sAllPlayerBD[iClientID].sRoundDesc.eChallenge = CI_GRID)
				CLEAR_CLIENT_FLAG(sAllPlayerBD[iClientID], RANGE_MP_F_GivenWeapon)
				SET_CLIENT_RANGE_STATE(sAllPlayerBD[iClientID], RANGE_STATE_WaitForAllClients_PostMenu)
//				RANGE_BUSYSPINNER_OFF()
			ENDIF
		BREAK
		
		CASE RANGE_STATE_DisplayRound_Countdown
			RANGE_PRINT_TO_SCREEN("RANGE_STATE_DisplayRound_Countdown", RANGE_STATE_PRINT_HEIGHT)
			
			// This is the first flag set in a sequence of three for the tutorial print helps
			// If it is set, we can just clear all
			IF IS_PLAYER_FLAG_SET(sPlayerData[iClientID], RANGE_MP_F_CLEARED_COVERED_HELP1)
				CLEAR_PLAYER_FLAG(sPlayerData[iClientID], RANGE_MP_F_CLEARED_COVERED_HELP1)
				CLEAR_PLAYER_FLAG(sPlayerData[iClientID], RANGE_MP_F_SHOWED_COVERED_HELP2)
				CLEAR_PLAYER_FLAG(sPlayerData[iClientID], RANGE_MP_F_SHOWED_BLOCKER_HELP)
			ENDIF

			BOOL bCDRetval
			bCDRetval = UPDATE_MINIGAME_COUNTDOWN_UI(sAllMenuData.uiCountdown, FALSE, FALSE, FALSE)
			
			IF IS_CLIENT_FLAG_SET(sAllPlayerBD[iClientID], RANGE_MP_F_WaitingOnScoreCard)
				CLEAR_CLIENT_FLAG(sAllPlayerBD[iClientID], RANGE_MP_F_WaitingOnScoreCard)
				GIVE_PLAYER_RANGE_WEAPON(sAllPlayerBD[iClientID].sRoundDesc, sAllPlayerBD[iClientID], sPlayerData[iClientID], TRUE)
			ENDIF
				
			// We base when we're going to start off of the server logged timer. 
			CDEBUG3LN(DEBUG_SHOOTRANGE, "Server time & my time: ", GET_RANGE_SERVER_NET_TIME_COUNTDOWN_OVER( sServerBD ), " // ", NATIVE_TO_INT(GET_NETWORK_TIME()))
			
			IF (GET_RANGE_SERVER_NET_TIME_COUNTDOWN_OVER( sServerBD ) != -1)
			AND (NATIVE_TO_INT(GET_NETWORK_TIME()) > GET_RANGE_SERVER_NET_TIME_COUNTDOWN_OVER( sServerBD ))
			AND IS_CLIENT_FLAG_SET(sAllPlayerBD[iClientID], RANGE_MP_F_RoundStartUpdateDone)
//				CDEBUG2LN(DEBUG_SHOOTRANGE, "RANGE_STATE_DisplayRound_Countdown - ORDERED TO MOVE!")
				// Wipe our stat for shots fired this round.
				sPlayerData[iClientID].iShotsFiredThisRound = 0
				sPlayerData[iClientID].iShotsHitThisRound = 0
				sPlayerData[iClientID].iShotsMissedThisRound = 0
				
				// Has the server moved us to the actual game yet?
				CALL sFuncs.fpRoundStart(sCurRound)
				
				// Wipe our leaderboard here.
				CDEBUG1LN(DEBUG_SHOOTRANGE, "~~~~~ CLEARING PREDICTION DATA! ~~~~~")
				CLEAR_RANK_REDICTION_DETAILS()
				RANGE_CLEANUP_SOCIAL_CLUB_LEADERBOARD(sCoreData.rangeLBD)
				
				CANCEL_TIMER(sCurRound.sRoundTimer)
				SET_CLIENT_RANGE_STATE(sAllPlayerBD[iClientID], RANGE_STATE_InRound)
				// Prime the buttons for when they draw again
				REMOVE_MENU_HELP_KEYS()
				ADD_MENU_HELP_KEY_CLICKABLE(INPUT_CONTEXT, "IB_QUIT")
				RANGE_BUSYSPINNER_OFF()
				sAllMenuData.fReticleDelay = 0
			ELSE
				IF NOT IS_CLIENT_FLAG_SET(sAllPlayerBD[iClientID], RANGE_MP_F_CountdownDone)
//					CDEBUG2LN(DEBUG_SHOOTRANGE, "RANGE_STATE_DisplayRound_Countdown - CD not done.")
					IF bCDRetval
//						CDEBUG2LN(DEBUG_SHOOTRANGE, "RANGE_STATE_DisplayRound_Countdown - but countdown is done!")
						SET_CLIENT_FLAG(sAllPlayerBD[iClientID], RANGE_MP_F_CountdownDone)
					ENDIF
				ENDIF
				
				// Move the grid.
				IF NOT IS_CLIENT_FLAG_SET(sAllPlayerBD[iClientID], RANGE_MP_F_RoundStartUpdateDone)
//					CDEBUG2LN(DEBUG_SHOOTRANGE, "RANGE_STATE_DisplayRound_Countdown - start not done...")
					
					IF CALL sFuncs.fpRoundSetup(sCurRound)
//						CDEBUG2LN(DEBUG_SHOOTRANGE, "RANGE_STATE_DisplayRound_Countdown - start is done.")
						
						SET_CLIENT_FLAG(sAllPlayerBD[iClientID], RANGE_MP_F_RoundStartUpdateDone)
						
						// Setup any UI needs...
						RANGE_MP_SETUP_FLOATING_SCORE_QUEUE(sAllMenuData.floatingScores)
					ENDIF
				ENDIF
				
				// Give the correct weapon, if we haven't already.
				IF NOT IS_CLIENT_FLAG_SET(sAllPlayerBD[iClientID], RANGE_MP_F_GivenWeapon)
					IF NOT IS_PLAYER_DEAD(PLAYER_ID())
						SET_PLAYER_CONTROL(PLAYER_ID(), TRUE, SPC_REMOVE_EXPLOSIONS|SPC_REMOVE_PROJECTILES|SPC_REMOVE_FIRES|SPC_CLEAR_TASKS)
						SET_PLAYER_INVINCIBLE(PLAYER_ID(), TRUE)
						sAllMenuData.eInRoundMenuState = RANGE_MP_RoundMenu_Init
						NETWORK_SET_FRIENDLY_FIRE_OPTION(FALSE)
//						CDEBUG2LN(DEBUG_SHOOTRANGE, "RANGE_STATE_DisplayRound_Countdown - making player invincible and moving the eInRoundMenuState on.")
					ENDIF
					
					IF GET_RANGE_MP_ROUNDS_PLAYED(sServerBD) = 0
						GIVE_PLAYER_RANGE_WEAPON(sAllPlayerBD[iClientID].sRoundDesc, sAllPlayerBD[iClientID], sPlayerData[iClientID])
//						CDEBUG2LN(DEBUG_SHOOTRANGE, "RANGE_STATE_DisplayRound_Countdown: Giving the player a weapon for the round.")
					ENDIF
				ENDIF
			ENDIF
			
			// Tell the player which targets to shoot at
			IF iClientID = iRedPlayer AND sAllPlayerBD[iClientID].sRoundDesc.eChallenge = CI_RANDOM
				PRINT_HELP_FOREVER("CDESC1_ORAN")
			ELIF iClientID = iRedPlayer AND sAllPlayerBD[iClientID].sRoundDesc.eChallenge = CI_GRID
				PRINT_HELP_FOREVER("CDESC2_ORAN")
			ELIF iClientID = iRedPlayer AND sAllPlayerBD[iClientID].sRoundDesc.eChallenge = CI_COVERED
				PRINT_HELP_FOREVER("CDESC3_ORAN")
			ELIF iClientID = iBluePlayer AND sAllPlayerBD[iClientID].sRoundDesc.eChallenge = CI_RANDOM
				PRINT_HELP_FOREVER("CDESC1_PURP")
			ELIF iClientID = iBluePlayer AND sAllPlayerBD[iClientID].sRoundDesc.eChallenge = CI_GRID
				PRINT_HELP_FOREVER("CDESC2_PURP")
			ELIF iClientID = iBluePlayer AND sAllPlayerBD[iClientID].sRoundDesc.eChallenge = CI_COVERED
				PRINT_HELP_FOREVER("CDESC3_PURP")
			ENDIF

			RANGE_PLAYER_DISABLE_SHOOTING()
			
			IF GET_RANGE_SERVER_NET_TIME_COUNTDOWN_OVER( sServerBD ) = -1
				CERRORLN( DEBUG_SHOOTRANGE, "GET_RANGE_SERVER_NET_TIME_COUNTDOWN_OVER( sServerBD ) = -1, Something went wrong, fixing it with hacks" )
				SET_RANGE_SERVER_NET_TIME_COUNTDOWN_OVER( sServerBD, NATIVE_TO_INT(GET_NETWORK_TIME()) + 3500 )	// dirty hack, the game can hang if the value is -1 so this makes it not -1.
			ENDIF
		BREAK
		
		CASE RANGE_STATE_InRound
			RANGE_PRINT_TO_SCREEN("RANGE_STATE_InRound", RANGE_STATE_PRINT_HEIGHT)
			IF IS_PLAYER_FLAG_SET(sPlayerData[iClientID], RANGE_MP_F_DONT_COUNT_SHOTS_FIRED)
				CLEAR_PLAYER_FLAG(sPlayerData[iClientID], RANGE_MP_F_DONT_COUNT_SHOTS_FIRED)
			ENDIF
			SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_ICON)
			UPDATE_MINIGAME_COUNTDOWN_UI(sAllMenuData.uiCountdown, TRUE, FALSE, FALSE, 3, FALSE, TRUE)
			IF sAllMenuData.fReticleDelay < 0.5
				sAllMenuData.fReticleDelay += GET_FRAME_TIME()
				RANGE_PLAYER_DISABLE_SHOOTING()
			ELSE
				IF IS_SHOOTING_RANGE_unSAVED_BITFLAG_SET(SRB_HideBetsMsg)
					CLEAR_SHOOTING_RANGE_unSAVED_BITFLAG(SRB_HideBetsMsg)
				ENDIF
			ENDIF
			
			// If the player ever gets low on ammo.
			IF GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), sPlayerData[iClientID].eWeaponGiven) <= 100
				RANGE_SET_PLAYER_MAX_AMMO(sPlayerData[iClientID].eWeaponGiven)
			ENDIF
			
			IF ROUND_UPDATE_TIME(sAllPlayerBD[iClientID].sRoundDesc, sCurRound, sAllMenuData) 
			AND NOT IS_SERVER_FLAG_SET(sServerBD, RANGE_MP_SERVER_F_MoveTo_RoundOver_Sk)
				
				// Print the second tutorial message if we have cleared the previous message
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				AND NOT IS_PLAYER_FLAG_SET(sPlayerData[iClientID], RANGE_MP_F_SHOWED_COVERED_HELP2)
				AND IS_PLAYER_FLAG_SET(sPlayerData[iClientID], RANGE_MP_F_CLEARED_COVERED_HELP1)
					IF iClientID = iRedPlayer AND sAllPlayerBD[iClientID].sRoundDesc.eChallenge = CI_COVERED
						CPRINTLN(DEBUG_SHOOTRANGE, "RANGE_STATE_InRound Print orange")
						PRINT_HELP("CDESC3_ORAN2")
					ELIF iClientID = iBluePlayer AND sAllPlayerBD[iClientID].sRoundDesc.eChallenge = CI_COVERED
						CPRINTLN(DEBUG_SHOOTRANGE, "RANGE_STATE_InRound Print blue")
						PRINT_HELP("CDESC3_PURP2")
					ENDIF
					
					CPRINTLN(DEBUG_SHOOTRANGE, "RANGE_STATE_InRound setting RANGE_MP_F_SHOWED_COVERED_HELP2")
					SET_PLAYER_FLAG(sPlayerData[iClientID], RANGE_MP_F_SHOWED_COVERED_HELP2)
				ENDIF
			
				IF NOT UPDATE_MID_ROUND_HELP_UI(sServerBD, sAllMenuData, iClientID, sAllPlayerBD[iClientID].sRoundDesc.eChallenge, sPlayerData[iClientID], sParticipantInfo)
					CDEBUG2LN(DEBUG_SHOOTRANGE, "UPDATE_MID_ROUND_HELP_UI returned FALSE")
//						CALL sFuncs.fpRoundEnd(sCurRound)
					SET_LOADING_ICON_INACTIVE()
					SET_CLIENT_GAME_STATE(sAllPlayerBD[iClientID], GAME_STATE_LEAVE)
					RANGE_CLEAR_ENDED_MISSION_AT_LEADERBOARD_VOTE_OR_TIME_OUT()
				ELSE
					CALL sFuncs.fpRoundUpdate(sCurRound, iClientID, sPlayerData[iClientID], sAllPlayerBD, sServerBD.iClient0Score, 
							sServerBD.iClient1Score, sAllMenuData, sParticipantInfo, sAllMenuData.eInRoundMenuState <> RANGE_MP_RoundMenu_DisplaySplash)
				ENDIF
			ELSE
				CLEAR_HELP()
				CLEAR_ALL_FLOATING_HELP()
				
				// Stop the UI countdown from processing.
				sAllMenuData.uiCountdown.iBitFlags = 0
				CANCEL_TIMER(sAllMenuData.uiCountdown.CountdownTimer)
				
				// Need to kill all sounds here... the countdown sound is sometimes playing.
				STOP_SOUND( RANGE_GET_ROUND_COUNTDOWN_SOUND_ID( sCurRound ) )
				PLAY_SOUND_FRONTEND( RANGE_GET_ROUND_COUNTDOWN_SOUND_ID( sCurRound ), "TIMER_STOP_MASTER")
							
				RESTART_TIMER_NOW(sCurRound.sRoundTimer)
				SET_LOADING_ICON_INACTIVE()
				SET_CLIENT_RANGE_STATE(sAllPlayerBD[iClientID], RANGE_STATE_RoundOver)

				BROADCAST_ROUND_SHOOTINGRESULTS_TO_OTHERS_PLAYERS(sPlayerData[iClientID].iShotsFiredThisRound, 
					sPlayerData[iClientID].iShotsHitThisRound, sPlayerData[iClientID].iShotsMissedThisRound,
					iClientID, sAllPlayerBD)
				
				IF IS_SERVER_FLAG_SET(sServerBD, RANGE_MP_SERVER_F_MoveTo_RoundOver_Sk)
					// Someone's been skunked for too long. Server is now ending the round.
					sCurRound.eRoundEndReason = ROR_Skunked
					CDEBUG2LN(DEBUG_SHOOTRANGE, "Ending due to Skunk")
				ELSE
					sCurRound.eRoundEndReason = ROR_TimeUp
					CDEBUG2LN(DEBUG_SHOOTRANGE, "Ending due to Time Out")
				ENDIF
			ENDIF
		BREAK
		
		CASE RANGE_STATE_RoundOver
			RANGE_PRINT_TO_SCREEN("RANGE_STATE_RoundOver", RANGE_STATE_PRINT_HEIGHT)
			
			// Fix for B*1610445 - Should register the fact that we've played shooting when a round finishes.
			SET_BIT_FOR_FRANKIE_SAYS_ACHIEVEMENT(ACH45_SHOOTING)
			
			IF IS_RANGE_ONE_PLAYER()
				SET_CLIENT_FLAG(sAllPlayerBD[iClientID], RANGE_MP_F_ProcessedPostRound)
			ENDIF
			
			CALL sFuncs.fpRoundUpdate(sCurRound, iClientID, sPlayerData[iClientID], sAllPlayerBD, sServerBD.iClient0Score, 
					sServerBD.iClient1Score, sAllMenuData, sParticipantInfo, FALSE)
			
			// Display the screen for the round depending on the reason it ended
			// TimeUps don't show the splash text yet, they need extra time to tabulate scores
			IF NOT IS_ROUND_WAIT_MESSAGE_SHOWING(sAllPlayerBD, sParticipantInfo, sCurRound) AND IS_SERVER_FLAG_SET(sServerBD, RANGE_MP_SERVER_F_MoveTo_ScoreUs)
				// Move to the "score us" phase. We had a nice 3 second window for the server to continue processing
				// all scores.
				CLEAR_RANGE_MP_ROUND_TIMER_FLAGS(sCurRound)
				SET_LOADING_ICON_INACTIVE()
				SET_CLIENT_RANGE_STATE(sAllPlayerBD[iClientID], RANGE_STATE_RoundOver_ScoreUs)

				// Set our shooting skill based on the round and send it to the other player
				GET_AND_SEND_RANGE_PLAYER_SHOOTING_SKILL(sAllPlayerBD, iClientID)
				
				IF NOT IS_PLAYER_DEAD(PLAYER_ID()) 
				AND NOT IS_PED_INJURED(PLAYER_PED_ID())
				AND (sPlayerData[iClientID].eWeaponGiven != WEAPONTYPE_MINIGUN)
					CDEBUG2LN(DEBUG_SHOOTRANGE, "CASE RANGE_STATE_RoundOver :: SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), FALSE)")
					SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), FALSE)
				ENDIF
			ENDIF
		BREAK
		
		CASE RANGE_STATE_RoundOver_ScoreUs
			RANGE_PRINT_TO_SCREEN("RANGE_STATE_RoundOver_ScoreUs", RANGE_STATE_PRINT_HEIGHT)
			
			CALL sFuncs.fpRoundUpdate(sCurRound, iClientID, sPlayerData[iClientID], sAllPlayerBD, sServerBD.iClient0Score, 
					sServerBD.iClient1Score, sAllMenuData, sParticipantInfo, FALSE)
			
			// We wait for the server to finish processing scores. Should be done already. Basically, just wiping data
			// here, and getting ready for the scorecard.
			IF (IS_SERVER_FLAG_SET(sServerBD, RANGE_MP_SERVER_F_MoveTo_DispRndOver))
				
				// Make both players clear it, just for giggles. Technically, only the host needs to clear.
				CLEAR_BITMASK_AS_ENUM(sAllPlayerBD[iClientID].iFlags, RANGE_MP_F_DoneWithMenuSelections)
				CLEAR_BITMASK_AS_ENUM(sAllPlayerBD[iClientID].iFlags, RANGE_MP_F_MenuTimeExpired)
				CLEAR_BITMASK_AS_ENUM(sAllPlayerBD[iClientID].iFlags, RANGE_MP_F_CountdownDone)
				CLEAR_BITMASK_AS_ENUM(sAllPlayerBD[iClientID].iFlags, RANGE_MP_F_RoundStartUpdateDone)
				
				CANCEL_TIMER(sCurRound.sRoundTimer)
				
				// Handle our score maybe being stored as a stat.
				RANGEMP_HANDLE_SCORE_STAT_XP_AND_BETTING(sCoreData, sServerBD, iClientID, sAllPlayerBD[iClientID].sRoundDesc.eChallenge, sCurRound.eRoundEndReason, sParticipantInfo, sAllPlayerBD[iClientID])
				
				// Set up the closing screen instruction data.
				SETUP_SCORE_CARD(sAllMenuData)
				RANGEMP_UI_TRIGGER_TRANSITION(sAllMenuData, TRUE)
				
				// Clear the Player Flag that shortens the tabulating lockstep
				CLEAR_CLIENT_FLAG(sAllPlayerBD[iClientID], RANGE_MP_F_ProcessedPostRound)
				SET_CLIENT_RANGE_STATE(sAllPlayerBD[iClientID], RANGE_STATE_RoundOverWait)
				
				// Store the prediction data here for the future write.
				INT iWin
				iWin = -1
				IF (iClientID = sParticipantInfo.iShooters[0])
					IF (SERVER_GET_CLIENT_WINNER(sServerBD) = CWC_CLIENT0_MATCH)
						iWin = 1
					ELIF (SERVER_GET_CLIENT_WINNER(sServerBD) = CWC_CLIENT1_MATCH)
						iWin = 0
					ENDIF
				ELSE
					IF (SERVER_GET_CLIENT_WINNER(sServerBD) = CWC_CLIENT1_MATCH)
						iWin = 1
					ELIF (SERVER_GET_CLIENT_WINNER(sServerBD) = CWC_CLIENT0_MATCH)
						iWin = 0
					ENDIF
				ENDIF
				// If iWin has a valid value then we want to send leaderboard data
				IF iWin <> -1 OR SERVER_GET_CLIENT_WINNER(sServerBD) = CWC_NOWINNER_MATCH
					sPlayerData[iClientID].bPredictionDone = FALSE
				ENDIF
				sPlayerData[iClientID].sPredict.eChallenge = sAllPlayerBD[iClientID].sRoundDesc.eChallenge
				sPlayerData[iClientID].sPredict.iWin = iWin
				sPlayerData[iClientID].sPredict.iShotsFiredThisRound += sPlayerData[iClientID].iShotsFiredThisRound
				sPlayerData[iClientID].sPredict.iShotsHitThisRound += sPlayerData[iClientID].iShotsHitThisRound
				sPlayerData[iClientID].sPredict.eWeaponGiven = sPlayerData[iClientID].eWeaponGiven
				
				// Send the message for our perceived stats this round.
				BROADCAST_SHOOTINGRESULTS_TO_OTHERS_PLAYERS(sPlayerData[iClientID].iShotsFired, sPlayerData[iClientID].iShotsHit, iClientID, sAllPlayerBD)
				BROADCAST_ROUND_SHOOTINGRESULTS_TO_OTHERS_PLAYERS(sPlayerData[iClientID].iShotsFiredThisRound, sPlayerData[iClientID].iShotsHitThisRound, 
					sPlayerData[iClientID].iShotsMissedThisRound, iClientID, sAllPlayerBD)
			ENDIF
		BREAK
		
		CASE RANGE_STATE_RoundOverWait
			RANGE_PRINT_TO_SCREEN("RANGE_STATE_RoundOverWait", RANGE_STATE_PRINT_HEIGHT)
			IF NOT IS_PLAYER_FLAG_SET(sPlayerData[iClientID], RANGE_MP_F_DONT_COUNT_SHOTS_FIRED)
				SET_PLAYER_FLAG(sPlayerData[iClientID], RANGE_MP_F_DONT_COUNT_SHOTS_FIRED)
			ENDIF
			IF NOT IS_SERVER_FLAG_SET(sServerBD, RANGE_MP_SERVER_F_Match_Over) AND NOT IS_PLAYER_FLAG_SET(sPlayerData[iClientID], RANGE_MP_F_FORCED_RELOAD)
				IF (sPlayerData[iClientID].eWeaponGiven != WEAPONTYPE_MINIGUN)
					INT iAmmo, iMaxAmmo
					GET_AMMO_IN_CLIP(PLAYER_PED_ID(), sPlayerData[iClientID].eWeaponGiven, iAmmo)
					iMaxAmmo = GET_MAX_AMMO_IN_CLIP(PLAYER_PED_ID(), sPlayerData[iClientID].eWeaponGiven)
					IF iMaxAmmo > iAmmo
						MAKE_PED_RELOAD(PLAYER_PED_ID())
						CDEBUG2LN(DEBUG_SHOOTRANGE, "RANGE_STATE_RoundOverWait: Reloading Weapon, iAmmo=", iAmmo, ", iMaxAmmo=", iMaxAmmo)
					ENDIF
				ENDIF
				SET_PLAYER_FLAG(sPlayerData[iClientID], RANGE_MP_F_FORCED_RELOAD)
			ENDIF
			
			CALL sFuncs.fpRoundUpdate(sCurRound, iClientID, sPlayerData[iClientID], sAllPlayerBD, sServerBD.iClient0Score, 
					sServerBD.iClient1Score, sAllMenuData, sParticipantInfo, FALSE)
			
			// Wait until the message is done
			IF NOT DISPLAY_ROUND_OVER_MESSAGE(sAllMenuData, sServerBD, iClientID, sAllPlayerBD, sParticipantInfo)
				CLEAR_PLAYER_FLAG(sPlayerData[iClientID], RANGE_MP_F_DONT_COUNT_SHOTS_FIRED)
				SET_CLIENT_RANGE_STATE(sAllPlayerBD[iClientID], RANGE_STATE_DisplayRoundOver)
				CANCEL_TIMER(sAllMenuData.sMenuTimer)
				CLEAR_PLAYER_FLAG(sPlayerData[iClientID], RANGE_MP_F_FORCED_RELOAD)
				
				IF IS_SERVER_FLAG_SET(sServerBD, RANGE_MP_SERVER_F_Match_Over)
					RANGE_SET_PLAYER_INFINITE_AMMO(sPlayerData[iClientID].eWeaponGiven, FALSE)
					RANGE_PLAYER_HOLSTER_WEAPON()
					
					// When we're all done, turn off action mode for minigun here.
					IF (sPlayerData[iClientID].eWeaponGiven = WEAPONTYPE_MINIGUN)
						CDEBUG2LN(DEBUG_SHOOTRANGE, "CASE RANGE_STATE_RoundOverWait :: SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), FALSE)")
						SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), FALSE)
					ENDIF
					
					CDEBUG2LN(DEBUG_SHOOTRANGE, "RANGE_STATE_RoundOverWait: Holstering Weapon")
				ENDIF
			ENDIF
		BREAK
		
		CASE RANGE_STATE_DisplayRoundOver
			RANGE_PRINT_TO_SCREEN("RANGE_STATE_DisplayRoundOver", RANGE_STATE_PRINT_HEIGHT)

			CALL sFuncs.fpRoundUpdate(sCurRound, iClientID, sPlayerData[iClientID], sAllPlayerBD, sServerBD.iClient0Score, 
					sServerBD.iClient1Score, sAllMenuData, sParticipantInfo, FALSE)

			// Update the main menu, but remind it that this is jsut to watch it.
			eRetVal = RANGE_MP_UpdateMainMenu(sCoreData, sAllPlayerBD, iClientID, sAllMenuData, sFuncs, sServerBD, sPlayerData, sCurRound, sParticipantInfo, TRUE, TRUE)
			
			// We need to display the round over screen, and let the player sit there until input is offered, or a timer is up.
			// Timer because we don't want one player to grief another on the score screen.
			IF (eRetVal = RETVAL_TRUE)
				CLEAR_HELP()
				CLEAR_RANGE_MP_MENU_FLAG(sAllMenuData, RMPM_SHOWING_SCLB)
				
				IF IS_SERVER_FLAG_SET(sServerBD, RANGE_MP_SERVER_F_Match_Over)
					// Handle the weapon while it's holstered.
					RANGE_PLAYER_REMOVE_WEAPON(sPlayerData[iClientID])
				ENDIF
				
				// Destroy the current round props.
				CALL sFuncs.fpRoundEnd(sCurRound)
				
				// We're leaving the scorecard. Set our transition string.
				REMOVE_MENU_HELP_KEYS()
				IF NOT IS_RANGE_ONE_PLAYER()
					RANGE_SET_BUSYSPINNER_STRING(sAllMenuData, "SHR_MP_WAITSC")
					RANGE_START_BUSYSPINNER(sAllMenuData)
				ENDIF
				
				//Server wants to do things with both players in this state
				IF IS_SERVER_FLAG_SET( sServerBD, RANGE_MP_SERVER_F_MoveTo_PostScorecard )
					// Send us to a state where we're just going to wait for all clients to be done with the scorecard.
					SET_CLIENT_RANGE_STATE(sAllPlayerBD[iClientID], RANGE_STATE_WaitForAllClients_PostScorecard)
				ENDIF

				// Clear the Leaderboard scaleform
				//RANGE_CLEANUP_SOCIAL_CLUB_LEADERBOARD()
				
				// Clear the menu updated flag
				sAllMenuData.bSCLBUpdated = FALSE
				
			ELIF (eRetVal = RETVAL_FALSE)
					
				SET_CLIENT_RANGE_STATE(sAllPlayerBD[iClientID], RANGE_STATE_ExitConfSplash_Results)
				UPDATE_EXIT_SPLASH_MAINMENU(sServerBD, iClientID, sParticipantInfo)
				CDEBUG2LN(DEBUG_SHOOTRANGE, "RANGE_STATE_DisplayRoundOver :: back button pressed")
			ENDIF
		BREAK
		
		CASE RANGE_STATE_WaitForAllClients_PostScorecard
			RANGE_PRINT_TO_SCREEN("RANGE_STATE_WaitForAllClients_PostScorecard", RANGE_STATE_PRINT_HEIGHT)
			
			// Just display a message that we're waiting for all clients. Server will signal us when it's time to leave.
			// Check that message first.
			IF IS_SERVER_FLAG_SET(sServerBD, RANGE_MP_SERVER_F_MoveTo_MainMenu)
			   	// Re-init the main menu NOW.
				RANGE_MP_InitMainMenu(sAllMenuData, iClientID, sServerBD, sAllPlayerBD)
				
				CANCEL_TIMER(sCurRound.sRoundTimer)
				
				// Reset our sound
				IF (RANGE_GET_ROUND_COUNTDOWN_SOUND_ID( sCurRound ) <> -1)
					RELEASE_SOUND_ID( RANGE_GET_ROUND_COUNTDOWN_SOUND_ID( sCurRound ) )
					RANGE_SET_ROUND_COUNTDOWN_SOUND_ID( sCurRound, -1 )
				ENDIF
				
				// If we're going back to the main menu, wipe some data.
				sPlayerData[iClientID].iShotsHitThisRound = 0
				sPlayerData[iClientID].iShotsFiredThisRound = 0
				sPlayerData[iClientID].iShotsMissedThisRound = 0
				
				BROADCAST_SHOOTINGRESULTS_TO_OTHERS_PLAYERS(sPlayerData[iClientID].iShotsFired, sPlayerData[iClientID].iShotsHit, iClientID, sAllPlayerBD)
				BROADCAST_ROUND_SHOOTINGRESULTS_TO_OTHERS_PLAYERS(sPlayerData[iClientID].iShotsFiredThisRound, sPlayerData[iClientID].iShotsHitThisRound, 
					sPlayerData[iClientID].iShotsMissedThisRound, iClientID, sAllPlayerBD)
					
				// Go back to the menu.
				SET_MP_INT_PLAYER_STAT(MPPLY_SHOOTINGRANGE_TOTAL_MATCH, GET_MP_INT_PLAYER_STAT(MPPLY_SHOOTINGRANGE_TOTAL_MATCH) + 1)
				SET_BIT_FOR_FRANKIE_SAYS_ACHIEVEMENT(ACH45_SHOOTING)
				SET_CLIENT_RANGE_STATE(sAllPlayerBD[iClientID], RANGE_STATE_Init)
								
			ELIF IS_SERVER_FLAG_SET(sServerBD, RANGE_MP_SERVER_F_ClientRepeatRound)
				// That was just one round. Go back.
				CALL sFuncs.fpRoundCreate(sCoreData, sCurRound, sAllPlayerBD[iClientID])
				RANGE_MP_INIT_IMPACT_COORDS( sAllPlayerBD[iClientID] )
				
				// Reset countrodnw data.
				sAllMenuData.uiCountdown.iBitFlags = 0
				CANCEL_TIMER(sAllMenuData.uiCountdown.CountdownTimer)
				CLEAR_BITMASK_AS_ENUM(sAllPlayerBD[iClientID].iFlags, RANGE_MP_F_CountdownDone)
				
				SETUP_PLAYER_FOR_ANOTHER_ROUND(sAllPlayerBD[iClientID], sPlayerData[iClientID])
				
				RANGEMP_UI_TRIGGER_TRANSITION(sAllMenuData, FALSE)
				IF IS_CLIENT_FLAG_SET(sAllPlayerBD[iClientID], RANGE_MP_F_IsSpectator)
					SET_ENTITY_COORDS(playerPed, sCoreData.vSpectatorPos)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), sCoreData.fSpectatorHead)
				ELSE
					SET_ENTITY_COORDS(PLAYER_PED_ID(), sPlayerData[iClientID].vPlayerStartPos)
					SET_ENTITY_HEADING(PLAYER_PED_ID(),sPlayerData[iClientID].fPlayerStartHead)
				ENDIF
				CDEBUG2LN(DEBUG_SHOOTRANGE, "RANGE_STATE_MenuUpdate Setting coords and heading because we're at a crap view")
				
				RANGEMP_SET_PLAYER_CAM_DOWNRANGE(sPlayerData[iClientID], sAllPlayerBD[iClientID].sRoundDesc.eChallenge = CI_GRID)
			ELSE
				// Okay, it's not time to continue on, just display a message that we're waiting for all players...
				DISPLAY_WAIT_FOR_SCORECARD_EXIT(sAllMenuData)
			ENDIF
			
			sAllMenuData.bHasSetSplashMsg = FALSE
		BREAK
		

		CASE RANGE_STATE_ExitConfSplash_MainMenu
		CASE RANGE_STATE_ExitConfSplash_Results
			RANGE_HANDLE_EXIT_SPLASH(sServerBD, sAllPlayerBD, iClientID, sAllMenuData, sPlayerData, sCurRound, sFuncs, sParticipantInfo)
		BREAK
		
		DEFAULT
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Handles all end conditions.
/// RETURNS:
///    TRUE if some end condition has been met.
FUNC BOOL PROCESS_END_CONDITIONS(RangeMP_ServerBD & sServerBD, RANGE_END_CONDITION & eTerminateReason, RangeMP_PlayerBD & sAllPlayerBD[], INT iClientID, 
		RangeMP_MenuData & sMenuData, RangeMP_Round & sRoundData, RangeMP_CoreData & sCoreInfo, RangeMP_ParticipantInfo & sParticipantInfo)
		
	//Do this only if we're not trying to quit already.
	RANGE_MP_GAME_STATE eCurState = GET_CLIENT_GAME_STATE(sAllPlayerBD[iClientID])
	IF (eCurState <= GAME_STATE_RUNNING)
		IF HAVE_MISSION_END_CONDITIONS_BEEN_MET(eTerminateReason, sParticipantInfo)	//, sAllPlayerBD)
			CDEBUG2LN(DEBUG_SHOOTRANGE, "GAME_STATE_RUNNING: End conditions met. Terminating.")
			
			IF (eTerminateReason = RANGE_END_PlayerLeft) OR (eTerminateReason = RANGE_END_None)
				CDEBUG2LN(DEBUG_SHOOTRANGE, "Going to the splash screen for game over...")
				// Give the continue prompt.
				REMOVE_MENU_HELP_KEYS()
				ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_ACCEPT, "CELL_286")
				
				// Need to freeze the palyer, temporarily.
				IF NOT IS_ENTITY_DEAD(playerPed)
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_REMOVE_EXPLOSIONS|SPC_REMOVE_PROJECTILES|SPC_REMOVE_FIRES|SPC_CLEAR_TASKS)
				ENDIF
				
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(500)
				ENDIF
				
				// Need to kill the warning sound. - #535669
				IF (RANGE_GET_ROUND_COUNTDOWN_SOUND_ID( sRoundData ) <> -1)
					STOP_SOUND( RANGE_GET_ROUND_COUNTDOWN_SOUND_ID( sRoundData ) )
					RELEASE_SOUND_ID( RANGE_GET_ROUND_COUNTDOWN_SOUND_ID( sRoundData ) )
					RANGE_SET_ROUND_COUNTDOWN_SOUND_ID( sRoundData, -1 )
				ENDIF
				
				CLEAR_HELP()
				SETTIMERA(0)
				RESTART_TIMER_NOW(sMenuData.sMenuTimer)
				SET_CLIENT_GAME_STATE(sAllPlayerBD[iClientID], GAME_STATE_END)
				
				// Okay, if we're about to print that the other player left, make sure that we haven't left too...
				STRING sLeaveReason = "SHR_MP_Q_LV"
				IF (eTerminateReason = RANGE_END_None)
				OR GET_ENTITY_DISTANCE_FROM_LOCATION(playerPed, sCoreInfo.vRangeLeftCoords) < 1.5
					sLeaveReason = "SHR_MP_Q_NO"
				ELSE
					SET_CLIENT_FLAG(sAllPlayerBD[iClientID], RANGE_MP_F_ContinueAfterVotePassing)
					IF sServerBD.iClient0MatchWins > sServerBD.iClient1MatchWins AND iClientID = sParticipantInfo.iShooters[0]
					OR sServerBD.iClient0MatchWins < sServerBD.iClient1MatchWins AND iClientID = sParticipantInfo.iShooters[1]
						BROADCAST_BETTING_MISSION_FINISHED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(PARTICIPANT_ID_TO_INT())))
						CDEBUG2LN(DEBUG_SHOOTRANGE, "PROCESS_END_CONDITIONS() :: BROADCAST_BETTING sServerBD.iClient0MatchWins=", sServerBD.iClient0MatchWins, ", sServerBD.iClient1MatchWins=", sServerBD.iClient1MatchWins, ", iClientID=", iClientID)
					ELSE
						BROADCAST_BETTING_MISSION_FINISHED_TIED()
						CDEBUG2LN(DEBUG_SHOOTRANGE, "PROCESS_END_CONDITIONS() :: BROADCAST_BETTING TIED sServerBD.iClient0MatchWins=", sServerBD.iClient0MatchWins, ", sServerBD.iClient1MatchWins=", sServerBD.iClient1MatchWins, ", iClientID=", iClientID)
					ENDIF
				ENDIF
				
				SET_RANGE_MENU_EXIT_STRING(sMenuData, sLeaveReason)
				SET_SHARD_BIG_MESSAGE(sMenuData.uiShardBM, "SC_COM_TIMEUP", sLeaveReason, 4000)
				BUSYSPINNER_OFF()
				//SET_LOADING_ICON_INACTIVE()
				RETURN FALSE
			ELSE
				// We weren't given a termination reason...
				CDEBUG2LN(DEBUG_SHOOTRANGE, "Uh oh, no splash!")
				SET_CLIENT_GAME_STATE(sAllPlayerBD[iClientID], GAME_STATE_END)
			ENDIF
			
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Handles any teleports and sends the player to the correct state.
PROC RANGE_MP_SETUP_START(RangeMP_CoreData & sCoreData, RangeMP_PlayerData & sPlayerData, RangeMP_ParticipantInfo & sParticipantInfo, INT iClientID, RangeMP_Round & sRndInfo)
	// About to fade out. Time to handle some stuffs.
	SET_CURRENT_PED_WEAPON(playerPed, WEAPONTYPE_UNARMED, FALSE)
	TASK_SWAP_WEAPON(playerPed, TRUE)
	SET_PED_CAN_SWITCH_WEAPON(playerPed, FALSE)

	SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_REMOVE_EXPLOSIONS|SPC_REMOVE_PROJECTILES|SPC_REMOVE_FIRES|SPC_CLEAR_TASKS)

	SET_PLAYER_INVINCIBLE(PLAYER_ID(), TRUE)
	SET_PLAYER_LOCKON(PLAYER_ID(), FALSE)
	SET_PED_CAN_BE_TARGETTED(playerPed, FALSE)
	
	NETWORK_SET_FRIENDLY_FIRE_OPTION(FALSE)
	
	// The server says we're ready to run. Let's do it. Depending on distance, this may mean a teleport.
	CLEAR_HELP()
	
	SET_DPADDOWN_ACTIVE(FALSE)
			
	DO_SCREEN_FADE_OUT(500)
	
	// Figure out which range this is, and setup needed data for that location.
	IF VDIST(<<6.5940, -1100.1622, 28.7970>>, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)) < 250.0
		// This data remains constant across clients
		sCoreData.eLocation = RANGELOC_PILLBOX_HILL
		sCoreData.vRangeLeftCoords = << 7.39, -1098.50, 28.80 >>
		sCoreData.vExitNormal = << 6.97, -1099.78, 28.80 >> - sCoreData.vRangeLeftCoords
		sCoreData.vInteriorCoords = <<13.4784, -1097.4746, 28.8347>>
		sCoreData.vKillFireCoords = << 17.7, -1085.9, 28.7 >>
		
		sCoreData.vSpectatorPos = <<18.3750, -1101.7654, 28.7970>>
		sCoreData.fSpectatorHead = 345.171
		
		sCoreData.vRangeFwdTarget = << 16.475, -1081.5134, 31.580 >>
		sCoreData.vRangeFwdClose = << 12.617, -1092.350, 31.580 >>
		
		sCoreData.vRangeOffsetCol1 = << 0.2354, -0.087, 0>> 
		sCoreData.vRangeOffsetCol2 = << 1.2947, -0.4785, 0 >>
		sCoreData.vRangeOffsetCol3 = << 2.354, -0.87, 0 >>
		sCoreData.vRangeOffsetCol4 = << 3.4133, -1.2615, 0 >>
		sCoreData.vRangeOffsetCol5 = << 4.4726, -1.653, 0>>
		
		// This data will change based on which client we are.
		IF (iClientID = sParticipantInfo.iShooters[BLUE_PLAYER_CLIENT_ID])
			sPlayerData.vPlayerStartPos = <<14.2448, -1097.8669, 28.8348>>
			sPlayerData.fPlayerStartHead = 345.1710
			sPlayerData.fPlayerHeadSkewed = 1.4400
		ELIF (iClientID = sParticipantInfo.iShooters[RED_PLAYER_CLIENT_ID])
			sPlayerData.vPlayerStartPos = << 12.2249, -1096.9102, 28.8347 >>
			sPlayerData.fPlayerStartHead = 345.1710
			sPlayerData.fPlayerHeadSkewed = 322.3211
		ENDIF
				
		// Round specific data
		sRndInfo.sGridData.vGridAnchorPos = <<15.763, -1091.25, 31.12>>
		sRndInfo.sGridData.vGridFramePos = <<15.763, -1091.25, 30.595>>
		sRndInfo.sGridData.vGridStartRot = <<90.0, 0.0, 339.99>>
		sRndInfo.sGridData.vGridEndRot = <<0.0, 0.0, 339.99>>
		sRndInfo.sGridData.fBlueTargetHead = 159.99
		sRndInfo.sGridData.fRedTargetHead = 339.99
		
		sRndInfo.sCovData.vControl0Pos = <<18.6, -1083.363, 31.25>>
		sRndInfo.sCovData.vBlueCtrlStartRot = <<0.0,90.0,160.0>>
		sRndInfo.sCovData.vRedCtrlStartRot = <<0.0,-90.0,160.0>>
		sRndInfo.sCovData.vCtrlRedBlueStartRot = <<0.0, 90.0, 340.0>>
		sRndInfo.sCovData.vCtrlRedBlueEndRot = <<0.0, 90.0, 160.0>>
		sRndInfo.sCovData.vCtrl2RedBlueStartRot = <<0.0, -90.0, 160.0>>
		sRndInfo.sCovData.vCtrl2RedBlueEndRot = <<0.0, -90.0, 340.0>>
		sRndInfo.sCovData.vTopLeftTargPos = <<15.25, -1082.063, 31.675>>
		sRndInfo.sCovData.vBackingRotation = <<0.0,0.0,160.0>>
		sRndInfo.sCovData.vBlockedStartRot = <<0.0,0.0,160.0>>
		sRndInfo.sCovData.vUnblockedStartRot = <<0.0,0.0,-20.0>>
		sRndInfo.sCovData.vDestroyedEndRot = <<90.0, 0.0, -20.0>>
		sRndInfo.sCovData.vGrid0Pos = <<16.163, -1082.4, 29.295>>
		sRndInfo.sCovData.vGrid1Pos = <<21.113, -1084.2, 29.295>>
		sRndInfo.sCovData.fGridHead = -20.0
		sRndInfo.sCovData.fGoalXOffset = 0.61
		sRndInfo.sCovData.fGoalYOffset = -0.224
		sRndInfo.sCovData.fGoal_DivXOff = 4.95
		sRndInfo.sCovData.fGoal_DivYOff = -1.802
		
		sRndInfo.sRandData.vStartRot = <<-90.0,0.0,160.0>>
		sRndInfo.sRandData.vEndRot = <<0.0,0.0,160.0>>
		
		sCoreData.vSpec1[0] = <<8.3763, -1094.0554, 30.5164>>
		sCoreData.vSpec1[1] = <<-5.2747, 0.0000, -46.7513>>
		sCoreData.vSpec1Goto[0] = <<9.6973, -1094.8466, 30.2163>>
		sCoreData.vSpec1Goto[1] = <<-3.4959, -0.0000, -39.3956>>
		sCoreData.vSpec2[0] = <<18.5829, -1102.1842, 31.0391>>
		sCoreData.vSpec2[1] = <<-8.4774, -0.0000, 28.6713>>
		sCoreData.vSpec2Goto[0] = <<18.6086, -1102.2319, 30.7796>>
		sCoreData.vSpec2Goto[1] = <<-6.2610, 0.0000, 30.7503>>
		sCoreData.vSpec3[0] = <<12.7629, -1100.0939, 30.6691>>
		sCoreData.vSpec3[1] = <<-9.2942, 0.0000, -16.5602>>
		sCoreData.vSpec3Goto[0] = <<12.9133, -1099.5881, 30.5828>>
		sCoreData.vSpec3Goto[1] = <<-9.2942, 0.0000, -16.5602>>
		
		sCoreData.vSpec4[0] 	= <<15.9129, -1091.7334, 29.7577>>
		sCoreData.vSpec4[1] 	= <<1.1872, 0.0000, 156.8667>>
		sCoreData.vSpec4Goto[0] = <<15.6863, -1092.2635, 29.7696>>
		sCoreData.vSpec4Goto[1] = <<1.1872, 0.0000, 156.8667>>
		sCoreData.vSpec5[0] 	= <<18.6746, -1100.9468, 31.5060>>
		sCoreData.vSpec5[1] 	= <<-9.0526, -0.0000, 66.8913>>
		sCoreData.vSpec5Goto[0] = <<18.7223, -1100.9696, 31.2648>>
		sCoreData.vSpec5Goto[1] = <<-12.6367, -0.0000, 64.4660>>
		sCoreData.vSpec6[0] 	= <<20.2207, -1096.5844, 30.1910>>
		sCoreData.vSpec6[1] 	= <<-0.4164, 0.0000, 109.9062>>
		sCoreData.vSpec6Goto[0] = <<19.9853, -1095.9342, 30.1910>>
		sCoreData.vSpec6Goto[1] = <<-0.4164, 0.0000, 109.9062>>
		
		GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bComingOutOfShootingRange[SHOOTING_RANGE_1] = TRUE
		GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bComingOutOfShootingRange[SHOOTING_RANGE_2] = FALSE

	ELSE
		sCoreData.eLocation = RANGELOC_CYPRESS_FLATS
		sCoreData.vRangeLeftCoords = << 826.84, -2160.31, 28.62 >>
		sCoreData.vExitNormal = << 826.86, -2159.08, 28.62 >> - sCoreData.vRangeLeftCoords
		sCoreData.vInteriorCoords = <<823.7017, -2162.8562, 29.8710>>
		sCoreData.vKillFireCoords = <<823.7017, -2162.8562, 29.8710>>
		
		sCoreData.vSpectatorPos = <<824.8112, -2167.2612, 28.6568>>
		sCoreData.fSpectatorHead = 177.1491
		
		sCoreData.vRangeFwdTarget = << 824.3217, -2179.7732, 31.3470 >>
		sCoreData.vRangeFwdClose = << 824.2375, -2168.0388, 31.3470 >>
		
		// This data will change based on which client we are.
		IF (iClientID = sParticipantInfo.iShooters[BLUE_PLAYER_CLIENT_ID])
			sPlayerData.vPlayerStartPos = <<820.8112, -2163.2612, 28.6568>>
			sPlayerData.fPlayerStartHead = 177.1491
			sPlayerData.fPlayerHeadSkewed = 196.989807
		ELIF (iClientID = sParticipantInfo.iShooters[RED_PLAYER_CLIENT_ID])
			sPlayerData.vPlayerStartPos = <<822.7182, -2163.6189, 28.6567>>
			sPlayerData.fPlayerStartHead = 177.8039
			sPlayerData.fPlayerHeadSkewed = 165.072296
		ENDIF
		
		sCoreData.vRangeOffsetCol1 = <<-0.330, -0.012, 0>>
		sCoreData.vRangeOffsetCol2 = <<-1.465, -0.024, 0>>
		sCoreData.vRangeOffsetCol3 = <<-2.665, -0.020, 0>>
		sCoreData.vRangeOffsetCol4 = <<-3.935, -0.022, 0>>
		sCoreData.vRangeOffsetCol5 = <<-5.170, -0.018, 0>>

		// Round specific data
		sRndInfo.sGridData.vGridAnchorPos = << 821.569, -2170.467, 30.85 >>
		sRndInfo.sGridData.vGridFramePos = << 821.569, -2170.467, 30.425 >>
		sRndInfo.sGridData.vGridStartRot = <<90.0,0.0,179.0>>
		sRndInfo.sGridData.vGridEndRot = <<0.0,0.0,179.0>>
		sRndInfo.sGridData.fBlueTargetHead = 359.99
		sRndInfo.sGridData.fRedTargetHead = 179.99
		
		sRndInfo.sCovData.vControl0Pos = <<821.713, -2179.743, 31.0>>
		sRndInfo.sCovData.vBlueCtrlStartRot = << 0, 90, 359.0>>
		sRndInfo.sCovData.vRedCtrlStartRot = << 0, -90, 359.0>>
		sRndInfo.sCovData.vCtrlRedBlueStartRot = <<0.0, 90.0, 179.0>>
		sRndInfo.sCovData.vCtrlRedBlueEndRot = <<0.0, 90.0, 359.0>>
		sRndInfo.sCovData.vCtrl2RedBlueStartRot = <<0.0, -90.0, 359.0>>
		sRndInfo.sCovData.vCtrl2RedBlueEndRot = <<0.0, -90.0, 179.0>>
		sRndInfo.sCovData.vTopLeftTargPos = <<825.280, -2179.743, 31.400>>
		sRndInfo.sCovData.vBackingRotation = <<0.0,0.0,359.0>>
		sRndInfo.sCovData.vBlockedStartRot = <<0.0,0.0,359.0>>
		sRndInfo.sCovData.vUnblockedStartRot = <<0.0,0.0,179.0>>
		sRndInfo.sCovData.vDestroyedEndRot = <<90.0, 0.0,179.0>>
		sRndInfo.sCovData.vGrid0Pos = << 824.313, -2179.743, 29.03>>
		sRndInfo.sCovData.vGrid1Pos = <<819.113, -2179.743, 29.030>>
		sRndInfo.sCovData.fGridHead = 179.0
		sRndInfo.sCovData.fGoalXOffset = -0.647
		sRndInfo.sCovData.fGoalYOffset = 0.0
		sRndInfo.sCovData.fGoal_DivXOff = -5.198
		sRndInfo.sCovData.fGoal_DivYOff = 0.0
		
		sRndInfo.sRandData.vStartRot = <<-90.0,0.0,359.0>>
		sRndInfo.sRandData.vEndRot = <<0.0,0.0,359.0>>
		
		sCoreData.vSpec1[0] = <<815.8572, -2160.8508, 31.2122>>
		sCoreData.vSpec1[1] = <<-10.3794, 0.0000, -129.4315>>
		sCoreData.vSpec1Goto[0] = <<815.7922, -2160.7976, 30.7523>>
		sCoreData.vSpec1Goto[1] = <<-9.2686, -0.0000, -129.3356>>
		
		
		sCoreData.vSpec2[0] = <<821.6832, -2160.7554, 30.8157>>
		sCoreData.vSpec2[1] = <<-13.2147, 0.0000, 178.3251>>
		sCoreData.vSpec2Goto[0] = <<821.6750, -2161.0371, 30.7495>>
		sCoreData.vSpec2Goto[1] = <<-13.2147, 0.0000, 178.3251>>
		sCoreData.vSpec3[0] = <<827.4696, -2165.5745, 29.3737>>
		sCoreData.vSpec3[1] = <<-1.1145, -0.0000, 152.6825>>
		sCoreData.vSpec3Goto[0] = <<826.2807, -2165.0198, 29.3737>>
		sCoreData.vSpec3Goto[1] = <<-1.1145, 0.0000, 157.9606>>
		
		sCoreData.vSpec4[0] 	= <<815.2161, -2162.3398, 31.5205>>
		sCoreData.vSpec4[1] 	= <<-12.0415, -0.0000, -91.7865>>
		sCoreData.vSpec4Goto[0] = <<815.1027, -2162.3362, 30.9886>>
		sCoreData.vSpec4Goto[1] = <<-12.0415, -0.0000, -91.7865>>
		sCoreData.vSpec5[0] 	= <<821.4435, -2169.3044, 30.4398>>
		sCoreData.vSpec5[1] 	= <<-7.6294, 0.0000, -0.9855>>
		sCoreData.vSpec5Goto[0] = <<821.4552, -2168.6128, 30.3471>>
		sCoreData.vSpec5Goto[1] = <<-7.6294, 0.0000, -0.9855>>
		sCoreData.vSpec6[0] 	= <<815.6099, -2166.8992, 29.3998>>
		sCoreData.vSpec6[1] 	= <<4.7843, 0.0000, -47.3683>>
		sCoreData.vSpec6Goto[0] = <<816.5186, -2166.7764, 29.4628>>
		sCoreData.vSpec6Goto[1] = <<4.7843, -0.0000, -51.8508>>
		
		GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bComingOutOfShootingRange[SHOOTING_RANGE_1] = FALSE
		GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bComingOutOfShootingRange[SHOOTING_RANGE_2] = TRUE
		
	ENDIF	
ENDPROC

/// PURPOSE:
///    Handles forcing the interior of the range to pop in, be pinned, and putting the player in the range.
/// PARAMS:
///    sCoreData - 
///    sPlayerBD - []
///    sPlayerData - 
///    iClientID - 
/// RETURNS:
///    
FUNC BOOL RANGE_MP_HANDLE_TELEPORT(RangeMP_CoreData & sCoreData, RangeMP_PlayerBD &sPlayerBD[], RangeMP_PlayerData & sPlayerData[], INT iClientID)
	IF IS_SCREEN_FADED_OUT()
		IF (sCoreData.RangeInterior = NULL)
			CDEBUG2LN(DEBUG_SHOOTRANGE, "RANGE_MP_HANDLE_TELEPORT: Screen faded out and interior is NULL")	
			sCoreData.RangeInterior = GET_INTERIOR_AT_COORDS_WITH_TYPE(sCoreData.vInteriorCoords, "V_Gun")
			PIN_INTERIOR_IN_MEMORY(sCoreData.RangeInterior)
			SET_ROOM_FOR_GAME_VIEWPORT_BY_NAME("V_7_ShootRm")
			NEW_LOAD_SCENE_START_SPHERE(sCoreData.vInteriorCoords, 20)
			
			// #530894 - Need to kill all fires in the shooting range.
			STOP_FIRE_IN_RANGE(sCoreData.vKillFireCoords, 20.0)
			
			// Wap the player into there to help with loading.
			IF NOT IS_ENTITY_DEAD(playerPed)
				CDEBUG2LN(DEBUG_SHOOTRANGE, "RANGE_MP_HANDLE_TELEPORT: Player is not dead, moving them near the range.")	
				IF IS_CLIENT_FLAG_SET(sPlayerBD[iClientID], RANGE_MP_F_IsSpectator)
					SET_ENTITY_COORDS(playerPed, sCoreData.vSpectatorPos)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), sCoreData.fSpectatorHead)
				ELSE
					SET_ENTITY_COORDS(PLAYER_PED_ID(), sPlayerData[iClientID].vPlayerStartPos)
					SET_ENTITY_HEADING(PLAYER_PED_ID(),sPlayerData[iClientID].fPlayerStartHead)
				ENDIF
				FREEZE_ENTITY_POSITION(playerPed, TRUE)
				
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
                DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CHARACTER_WHEEL)
				//DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
				CLEAR_PED_TASKS_IMMEDIATELY(playerPed)
				SET_PED_DUCKING(playerPed, FALSE)
				SET_PED_STEALTH_MOVEMENT(playerPed, FALSE)
				
				RANGEMP_STORE_PLAYER_WEAPONS(sPlayerData[iClientID], TRUE)
			ENDIF
		ELSE
			// If the interior has loaded, warp us there, fade in.
			IF IS_INTERIOR_READY(sCoreData.RangeInterior) 
				IF (NOT IS_NEW_LOAD_SCENE_ACTIVE() OR IS_NEW_LOAD_SCENE_LOADED())
					
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
					
					DISPLAY_RADAR(FALSE)
					
					IF NOT IS_ENTITY_DEAD(playerPed)
						FREEZE_ENTITY_POSITION(playerPed, FALSE)
					ENDIF
					NEW_LOAD_SCENE_STOP()

					CDEBUG2LN(DEBUG_SHOOTRANGE, "RANGE_MP_HANDLE_TELEPORT: DONE")
					RETURN TRUE
				ELSE
					CDEBUG2LN(DEBUG_SHOOTRANGE, "RANGE_MP_HANDLE_TELEPORT: LOAD SCENE NOT READY")
				ENDIF
			ELSE
				CDEBUG2LN(DEBUG_SHOOTRANGE, "RANGE_MP_HANDLE_TELEPORT: IS_INTERIOR_READY = FALSE")
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC




