// RangeMP_Server.sch
USING "RangeMP_Round.sch"
USING "fmmc_restart_header.sch"

CONST_FLOAT	MENU_FAIL_DESYNC_TIME	2.0

ENUM CLIENT_WIN_CODES
	CWC_NONE = 0,
	
	CWC_CLIENT0_ROUND = 10,
	CWC_CLIENT1_ROUND = 20,
	
	CWC_CLIENT0_MATCH = 30,
	CWC_CLIENT1_MATCH = 40,
	CWC_NOWINNER_MATCH = 50
ENDENUM

CONST_INT k_MaxShooters		2
CONST_INT k_MaxSpectators	4
CONST_INT k_MaxSCTV			2
CONST_INT k_InvalidParticipantID	-1

STRUCT RangeMP_ParticipantInfo
	INT iShooters[k_MaxShooters]
	INT iSpectators[k_MaxSpectators]
	INT iSCTV[k_MaxSCTV]
	INT iShooterCount
	INT iSpectatorCount
	INT iSCTVCount
ENDSTRUCT

STRUCT RangeMP_ServerBD
	RANGE_MP_GAME_STATE 	eGameState = GAME_STATE_INIT
	
	INT 					iFlags
	INT						iClientPicking = -1		// The client in charge of picking the round.
	
	INT						iClient0Score = 0
	INT 					iClient1Score = 0
	
	CLIENT_WIN_CODES		eClientWhoWon = CWC_NONE		
	
	// Track the number of wins for each client.
	INT						iClient0RndWins
	INT						iClient1RndWins
	INT						iClient0TotalRndWins
	INT						iClient1TotalRndWins
	INT						iClient0MatchWins
	INT						iClient1MatchWins
	
	// Track times each client has won a particular challenge.
	INT						iClient0Chal1Wins
	INT						iClient0Chal2Wins
	INT						iClient0Chal3Wins
	INT						iClient1Chal1Wins
	INT						iClient1Chal2Wins
	INT						iClient1Chal3Wins
	
	// The number of rounds played in the current match.
	INT						iRndsPlayedInMatch = 0
	
	//INT						iMatchesPlayed = 0
	
	// Desync timer
	FLOAT 					fMenuFailDesyncTimer = 0.0
	
	// History ID
	INT						iMatchHistoryID
	INT 					iHashedMac
	INT						iMatchType
	
	// number of matches
	INT						iTotalMatches
	
	INT						iNetTime_CountdownOver = -1
	
	SCRIPT_TIMER			tLeaderboardTimeout
	SERVER_EOM_VARS 		sFMMC_EOM
	//FMMC_EOM_DETAILS		sFMMC_EOMDetails
	PLAYER_CURRENT_STATUS_COUNTS sPlayerCounts
ENDSTRUCT

ENUM RANGE_MP_SERVER_FLAGS
	RANGE_MP_SERVER_F_MoveTo_InRound 		= BIT0,
	RANGE_MP_SERVER_F_MoveTo_MainMenu 		= BIT1,
	RANGE_MP_SERVER_F_MoveTo_RndOverWait	= BIT2,
	RANGE_MP_SERVER_F_MoveTo_DispRndOver	= BIT3,
	RANGE_MP_SERVER_F_MoveTo_RoundOver_Sk	= BIT4,		// Forced round end due to skunk.
	RANGE_MP_SERVER_F_MoveTo_Countdown		= BIT5,
	RANGE_MP_SERVER_F_Match_Over			= BIT6,
	RANGE_MP_SERVER_F_MoveTo_ScoreUs		= BIT7,
	RANGE_MP_SERVER_F_ForceMenuChoice		= BIT8,
	RANGE_MP_SERVER_F_MoveTo_PostScorecard	= BIT9,
	RANGE_MP_SERVER_F_Client0WonMatch		= BIT10,
	RANGE_MP_SERVER_F_Client1WonMatch		= BIT11,
	RANGE_MP_SERVER_F_FirstSCLBUpdate		= BIT12,
	RANGE_MP_SERVER_F_NEVER_END				= BIT13,
	RANGE_MP_SERVER_F_FirstInit				= BIT14,
	RANGE_MP_SERVER_F_1stNoMenuInput		= BIT15,	
	RANGE_MP_SERVER_F_PreventReflag			= BIT16,
	RANGE_MP_SERVER_F_GoBackToMenu			= BIT17,
	RANGE_MP_SERVER_F_ClientRepeatRound		= BIT18,
	
	// Desync codes.
	RANGE_MP_SERVER_F_MenuFailDesync		= BIT20,
	
	// Quit codes.
	RANGE_MP_SERVER_F_Quit_NoMenuInput		= BIT25
ENDENUM

#IF IS_DEBUG_BUILD
FUNC STRING GET_STRING_FROM_RANGE_SERVER_FLAG( RANGE_MP_SERVER_FLAGS eFlag )
	SWITCH eFlag
		CASE RANGE_MP_SERVER_F_MoveTo_InRound			RETURN "RANGE_MP_SERVER_F_MoveTo_InRound"
		CASE RANGE_MP_SERVER_F_MoveTo_MainMenu			RETURN "RANGE_MP_SERVER_F_MoveTo_MainMenu"
		CASE RANGE_MP_SERVER_F_MoveTo_RndOverWait		RETURN "RANGE_MP_SERVER_F_MoveTo_RndOverWait"
		CASE RANGE_MP_SERVER_F_MoveTo_DispRndOver		RETURN "RANGE_MP_SERVER_F_MoveTo_DispRndOver"
		CASE RANGE_MP_SERVER_F_MoveTo_RoundOver_Sk		RETURN "RANGE_MP_SERVER_F_MoveTo_RoundOver_Sk"
		CASE RANGE_MP_SERVER_F_MoveTo_Countdown			RETURN "RANGE_MP_SERVER_F_MoveTo_Countdown"
		CASE RANGE_MP_SERVER_F_Match_Over				RETURN "RANGE_MP_SERVER_F_Match_Over"
		CASE RANGE_MP_SERVER_F_MoveTo_ScoreUs			RETURN "RANGE_MP_SERVER_F_MoveTo_ScoreUs"
		CASE RANGE_MP_SERVER_F_ForceMenuChoice			RETURN "RANGE_MP_SERVER_F_ForceMenuChoice"
		CASE RANGE_MP_SERVER_F_MoveTo_PostScorecard		RETURN "RANGE_MP_SERVER_F_MoveTo_PostScorecard"
		CASE RANGE_MP_SERVER_F_Client0WonMatch			RETURN "RANGE_MP_SERVER_F_Client0WonMatch"
		CASE RANGE_MP_SERVER_F_Client1WonMatch			RETURN "RANGE_MP_SERVER_F_Client1WonMatch"
		CASE RANGE_MP_SERVER_F_FirstSCLBUpdate			RETURN "RANGE_MP_SERVER_F_FirstSCLBUpdate"
		CASE RANGE_MP_SERVER_F_NEVER_END				RETURN "RANGE_MP_SERVER_F_NEVER_END"
		CASE RANGE_MP_SERVER_F_FirstInit				RETURN "RANGE_MP_SERVER_F_FirstInit"
		CASE RANGE_MP_SERVER_F_1stNoMenuInput			RETURN "RANGE_MP_SERVER_F_1stNoMenuInput"
		CASE RANGE_MP_SERVER_F_PreventReflag			RETURN "RANGE_MP_SERVER_F_PreventReflag"
		CASE RANGE_MP_SERVER_F_GoBackToMenu				RETURN "RANGE_MP_SERVER_F_GoBackToMenu"
		CASE RANGE_MP_SERVER_F_ClientRepeatRound		RETURN "RANGE_MP_SERVER_F_ClientRepeatRound"
		CASE RANGE_MP_SERVER_F_MenuFailDesync			RETURN "RANGE_MP_SERVER_F_MenuFailDesync"
		CASE RANGE_MP_SERVER_F_Quit_NoMenuInput			RETURN "RANGE_MP_SERVER_F_Quit_NoMenuInput"
	ENDSWITCH
	RETURN "Empty RANGE_MP_SERVER_FLAGS"
ENDFUNC
#ENDIF



//*******************************************************************************************************************
//***************************************** ACCESSORS AND MUTATORS **************************************************
//*******************************************************************************************************************
PROC SET_RANGE_SERVER_NET_TIME_COUNTDOWN_OVER(RangeMP_ServerBD &serverBD, INT iNewTime)
	CDEBUG2LN( DEBUG_SHOOTRANGE, "SET_RANGE_SERVER_NET_TIME_COUNTDOWN_OVER ::  iNewTime = ", iNewTime )
	serverBD.iNetTime_CountdownOver = iNewTime
ENDPROC
FUNC INT GET_RANGE_SERVER_NET_TIME_COUNTDOWN_OVER(RangeMP_ServerBD &serverBD)
	RETURN serverBD.iNetTime_CountdownOver
ENDFUNC

PROC SET_RANGE_SERVER_TOTAL_MATCHES(RangeMP_ServerBD &serverBD, INT iNewTot)
	serverBD.iTotalMatches = iNewTot
ENDPROC

FUNC INT GET_RANGE_SERVER_TOTAL_MATCHES(RangeMP_ServerBD &serverBD)
	RETURN serverBD.iTotalMatches
ENDFUNC

PROC SET_RANGE_MP_ROUNDS_PLAYED(RangeMP_ServerBD& sServerBD, INT iRoundsPlayed)
	sServerBD.iRndsPlayedInMatch = iRoundsPlayed
ENDPROC

FUNC INT GET_RANGE_MP_ROUNDS_PLAYED(RangeMP_ServerBD& sServerBD)
	RETURN sServerBD.iRndsPlayedInMatch
ENDFUNC

PROC INCREMENT_RANGE_SERVER_DESYNC_TIMER(RangeMP_ServerBD & sServerBD)
	sServerBD.fMenuFailDesyncTimer += GET_FRAME_TIME()
ENDPROC

FUNC FLOAT GET_RANGE_SERVER_DESYNC_TIMER(RangeMP_ServerBD & sServerBD)
	RETURN sServerBD.fMenuFailDesyncTimer
ENDFUNC

PROC RESET_RANGE_SERVER_DESYNC_TIMER(RangeMP_ServerBD & sServerBD)
	sServerBD.fMenuFailDesyncTimer = 0
ENDPROC

FUNC RANGE_MP_GAME_STATE GET_SERVER_GAME_STATE(RangeMP_ServerBD & sServerBD)
	RETURN sServerBD.eGameState
ENDFUNC 

PROC SET_SERVER_GAME_STATE(RangeMP_ServerBD & sServerBD, RANGE_MP_GAME_STATE gameState)
	NET_SCRIPT_HOST_ONLY_COMMAND_ASSERT("SET_SERVER_GAME_STATE")
	
	sServerBD.eGameState = gameState
ENDPROC

PROC SET_SERVER_FLAG(RangeMP_ServerBD & sServerBD, RANGE_MP_SERVER_FLAGS eFlagToSet)
	NET_SCRIPT_HOST_ONLY_COMMAND_ASSERT("SET_SERVER_FLAG")
	
	#IF IS_DEBUG_BUILD
	IF NOT IS_BITMASK_AS_ENUM_SET( sServerBD.iFlags, eFlagToSet )
		CDEBUG3LN( DEBUG_SHOOTRANGE, "SET_SERVER_FLAG :: Setting ", GET_STRING_FROM_RANGE_SERVER_FLAG( eFlagToSet ) )
	ENDIF
	#ENDIF
	
	sServerBD.iFlags |= ENUM_TO_INT(eFlagToSet)
ENDPROC

PROC CLEAR_SERVER_FLAG(RangeMP_ServerBD & sServerBD, RANGE_MP_SERVER_FLAGS eFlagToSet)
	NET_SCRIPT_HOST_ONLY_COMMAND_ASSERT("CLEAR_SERVER_FLAG")
	
	#IF IS_DEBUG_BUILD
	IF IS_BITMASK_AS_ENUM_SET( sServerBD.iFlags, eFlagToSet )
		CDEBUG3LN( DEBUG_SHOOTRANGE, "CLEAR_SERVER_FLAG :: Clearing ", GET_STRING_FROM_RANGE_SERVER_FLAG( eFlagToSet ) )
	ENDIF
	#ENDIF
	
	sServerBD.iFlags -= sServerBD.iFlags & ENUM_TO_INT(eFlagToSet)
ENDPROC

FUNC BOOL IS_SERVER_FLAG_SET(RangeMP_ServerBD & sServerBD, RANGE_MP_SERVER_FLAGS eFlagToCheck)
	RETURN (sServerBD.iFlags & ENUM_TO_INT(eFlagToCheck)) <> 0
ENDFUNC

FUNC INT SERVER_GET_PLAYER_IN_CONTROL(RangeMP_ServerBD & sServerBD)
	RETURN sServerBD.iClientPicking
ENDFUNC

PROC SERVER_SET_OTHER_CLIENT_AS_PICKER(RangeMP_ServerBD & sServerBD, RangeMP_ParticipantInfo & sParticipantInfo)
	NET_SCRIPT_HOST_ONLY_COMMAND_ASSERT("SERVER_SET_OTHER_CLIENT_AS_PICKER")
	
	INT iRedPlayer = sParticipantInfo.iShooters[RED_PLAYER_CLIENT_ID]
	INT iBluePlayer = sParticipantInfo.iShooters[BLUE_PLAYER_CLIENT_ID]
	
	IF iRedPlayer = k_InvalidParticipantID
		CDEBUG3LN( DEBUG_SHOOTRANGE, "SERVER_SET_OTHER_CLIENT_AS_PICKER - iRedPlayer = k_InvalidParticipantID, this is potentially fatal." )
	ENDIF	
	IF iBluePlayer = k_InvalidParticipantID
		CDEBUG3LN( DEBUG_SHOOTRANGE, "SERVER_SET_OTHER_CLIENT_AS_PICKER - iBluePlayer = k_InvalidParticipantID, this is potentially fatal." )
	ENDIF	
	
	IF (sServerBD.iClientPicking = iRedPlayer)
		sServerBD.iClientPicking = iBluePlayer
	ELSE
		sServerBD.iClientPicking = iRedPlayer
	ENDIF
ENDPROC

PROC SET_RANGE_SERVER_MENU_PICKER(RangeMP_ServerBD & sServerBD, INT iNewPicker)
	CDEBUG2LN(DEBUG_SHOOTRANGE, "SET_RANGE_SERVER_MENU_PICKER set to ", iNewPicker)
	NET_SCRIPT_HOST_ONLY_COMMAND_ASSERT("SET_RANGE_SERVER_MENU_PICKER")
	sServerBD.iClientPicking = iNewPicker
ENDPROC

FUNC INT GET_RANGE_SERVER_MENU_PICKER(RangeMP_ServerBD & sServerBD)
	RETURN sServerBD.iClientPicking
ENDFUNC

FUNC INT SERVER_GET_CLIENT_ROUND_WINS(RangeMP_ServerBD & sServerBD, RangeMP_ParticipantInfo & sParticipantInfo, INT iClientID)
	IF (iClientID = sParticipantInfo.iShooters[0])
		RETURN sServerBD.iClient0RndWins
	ENDIF
	
	RETURN sServerBD.iClient1RndWins
ENDFUNC

FUNC INT SERVER_GET_CLIENT_TOTAL_ROUND_WINS(RangeMP_ServerBD & sServerBD, RangeMP_ParticipantInfo & sParticipantInfo, INT iClientID)
	IF (iClientID = sParticipantInfo.iShooters[0])
		RETURN sServerBD.iClient0TotalRndWins
	ENDIF
	
	RETURN sServerBD.iClient1TotalRndWins
ENDFUNC

FUNC INT SERVER_GET_CLIENT_TOTAL_MATCH_WINS(RangeMP_ServerBD & sServerBD, RangeMP_ParticipantInfo & sParticipantInfo, INT iClientID)
	IF (iClientID = sParticipantInfo.iShooters[0])
		RETURN sServerBD.iClient0MatchWins
	ENDIF
	
	RETURN sServerBD.iClient1MatchWins
ENDFUNC

FUNC CLIENT_WIN_CODES SERVER_GET_CLIENT_WINNER(RangeMP_ServerBD & sServerBD)
	RETURN sServerBD.eClientWhoWon
ENDFUNC

FUNC INT SERVER_GET_TOTAL_WINS_FOR_CHALLENGE(RangeMP_ServerBD & sServerBD, CHALLENGE_INDEX eChallenge)
	IF (eChallenge = CI_GRID)
		RETURN (sServerBD.iClient0Chal1Wins + sServerBD.iClient1Chal1Wins)
	ELIF (eChallenge = CI_RANDOM)
		RETURN (sServerBD.iClient0Chal2Wins + sServerBD.iClient1Chal2Wins)
	ELIF (eChallenge = CI_COVERED)
		RETURN (sServerBD.iClient0Chal3Wins + sServerBD.iClient1Chal3Wins)
	ENDIF
	
	RETURN 0
ENDFUNC

FUNC INT SERVER_GET_CLIENT_WINS_FOR_CHALLENGE(RangeMP_ServerBD & sServerBD, RangeMP_ParticipantInfo & sParticipantInfo, INT iClientID, CHALLENGE_INDEX eChallenge)
	IF (eChallenge = CI_GRID)
		IF (iClientID = sParticipantInfo.iShooters[0])
			RETURN sServerBD.iClient0Chal1Wins
		ENDIF
		RETURN sServerBD.iClient1Chal1Wins
		
	ELIF (eChallenge = CI_RANDOM)
		IF (iClientID = sParticipantInfo.iShooters[0])
			RETURN sServerBD.iClient0Chal2Wins
		ENDIF
		RETURN sServerBD.iClient1Chal2Wins
		
	ELIF (eChallenge = CI_COVERED)
		IF (iClientID = sParticipantInfo.iShooters[0])
			RETURN sServerBD.iClient0Chal3Wins
		ENDIF
		RETURN sServerBD.iClient1Chal3Wins
	ENDIF
	
	RETURN 0
ENDFUNC

FUNC INT SERVER_GET_CLIENT_SCORE(RangeMP_ServerBD & sServerBD, RangeMP_ParticipantInfo & sParticipantInfo, INT iClientID)
	IF (iClientID = sParticipantInfo.iShooters[0])
		RETURN sServerBD.iClient0Score
	ENDIF
	RETURN sServerBD.iClient1Score
ENDFUNC

PROC RANGE_GET_SHOOTER_RANGE_STATES( RangeMP_PlayerBD & sPlayerBD[], RangeMP_ParticipantInfo & info, RANGE_STATE & eState[] )
	INT i = 0
	FOR i = 0 TO ( k_MaxShooters - 1 )
		IF info.iShooters[i] != k_InvalidParticipantID
			eState[i] = sPlayerBD[info.iShooters[i]].eRangeState
		ENDIF
	ENDFOR
ENDPROC

PROC RANGE_GET_SPECTATOR_RANGE_STATES( RangeMP_PlayerBD & sPlayerBD[], RangeMP_ParticipantInfo & info, RANGE_STATE & eState[] )
	INT i = 0
	FOR i = 0 TO ( k_MaxSpectators - 1 )
		IF info.iSpectators[i] != k_InvalidParticipantID
			eState[i] = sPlayerBD[info.iSpectators[i]].eRangeState
		ENDIF
	ENDFOR
ENDPROC

PROC RANGE_GET_SCTV_RANGE_STATES( RangeMP_PlayerBD & sPlayerBD[], RangeMP_ParticipantInfo & info, RANGE_STATE & eState[] )
	INT i = 0
	FOR i = 0 TO ( k_MaxSCTV - 1 )
		IF info.iSCTV[i] != k_InvalidParticipantID
			eState[i] = sPlayerBD[info.iSCTV[i]].eRangeState
		ENDIF
	ENDFOR
ENDPROC

FUNC BOOL RANGE_ARE_SHOOTERS_RANGE_STATE_READY( RANGE_STATE eState, RANGE_STATE & eClientStates[], BOOL bAny = FALSE )

	INT i = 0
	IF bAny
		FOR i = 0 TO ( k_MaxShooters - 1 )
			IF eClientStates[i] = eState
				RETURN TRUE
			ENDIF
		ENDFOR
		RETURN FALSE
	ENDIF
	
	FOR i = 0 TO ( k_MaxShooters - 1 )
		IF eClientStates[i] != eState
			RETURN FALSE
		ENDIF
	ENDFOR
	
	RETURN TRUE
	
ENDFUNC

PROC RANGE_GET_SHOOTER_GAME_STATES( RangeMP_PlayerBD & sPlayerBD[], RangeMP_ParticipantInfo & info, RANGE_MP_GAME_STATE & eState[] )
	INT i = 0
	FOR i = 0 TO ( k_MaxShooters - 1 )
		IF info.iShooters[i] != k_InvalidParticipantID
			eState[i] = sPlayerBD[info.iShooters[i]].eGameState
		ENDIF
	ENDFOR
ENDPROC

PROC RANGE_GET_SPECTATOR_GAME_STATES( RangeMP_PlayerBD & sPlayerBD[], RangeMP_ParticipantInfo & info, RANGE_MP_GAME_STATE & eState[] )
	INT i = 0
	FOR i = 0 TO ( k_MaxSpectators - 1 )
		IF info.iSpectators[i] != k_InvalidParticipantID
			eState[i] = sPlayerBD[info.iSpectators[i]].eGameState
		ENDIF
	ENDFOR
ENDPROC

PROC RANGE_GET_SCTV_GAME_STATES( RangeMP_PlayerBD & sPlayerBD[], RangeMP_ParticipantInfo & info, RANGE_MP_GAME_STATE & eState[] )
	INT i = 0
	FOR i = 0 TO ( k_MaxSCTV - 1 )
		IF info.iSCTV[i] != k_InvalidParticipantID
			eState[i] = sPlayerBD[info.iSCTV[i]].eGameState
		ENDIF
	ENDFOR
ENDPROC

FUNC BOOL RANGE_ARE_SHOOTERS_GAME_STATE_READY( RANGE_MP_GAME_STATE eState, RANGE_MP_GAME_STATE & eClientStates[], BOOL bAny = FALSE )

	INT i = 0
	IF bAny
		FOR i = 0 TO ( k_MaxShooters - 1 )
			IF eClientStates[i] = eState
				RETURN TRUE
			ENDIF
		ENDFOR
		RETURN FALSE
	ENDIF
	
	FOR i = 0 TO ( k_MaxShooters - 1 )
		IF eClientStates[i] != eState
			RETURN FALSE
		ENDIF
	ENDFOR
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL RANGE_ARE_SHOOTERS_FLAGS_SET( RANGE_CLIENT_FLAGS eFlag, RangeMP_PlayerBD & sPlayerBD[], RangeMP_ParticipantInfo & info, BOOL bAny = FALSE )
	INT i = 0
	IF bAny
		FOR i = 0 TO ( k_MaxShooters - 1 )
			IF info.iShooters[i] != k_InvalidParticipantID
				IF (sPlayerBD[info.iShooters[i]].iFlags & ENUM_TO_INT(eFlag)) != 0
					RETURN TRUE
				ENDIF
			ENDIF
		ENDFOR
		RETURN FALSE
	ENDIF
	
	FOR i = 0 TO ( k_MaxShooters - 1 )
		IF info.iShooters[i] != k_InvalidParticipantID	
			IF (sPlayerBD[info.iShooters[i]].iFlags & ENUM_TO_INT(eFlag)) = 0	
				RETURN FALSE
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN TRUE
	
ENDFUNC






