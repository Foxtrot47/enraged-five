// RangeMP_Player_lib.sch
USING "RangeMP_Player.sch"
USING "RangeMP_UI.sch"
USING "RangeMP_UI_lib.sch"
USING "RangeMP_Support_lib.sch"

PROC RANGE_SET_PLAYER_INFINITE_AMMO(WEAPON_TYPE eWeapon, BOOL bInfinite)
	CDEBUG2LN(DEBUG_SHOOTRANGE, "RANGE_SET_PLAYER_INFINITE_AMMO :: eWeapon=", eWeapon, ", bInfinite=", PICK_STRING(bInfinite, "TRUE", "FALSE"))
	IF HAS_PED_GOT_WEAPON(playerPed, eWeapon)
		SET_PED_INFINITE_AMMO(playerPed, bInfinite, eWeapon)
//		SET_PED_INFINITE_AMMO_CLIP(playerPed, bInfinite)
	ELSE
		CDEBUG2LN(DEBUG_SHOOTRANGE, "RANGE_SET_PLAYER_INFINITE_AMMO ped doesn't have weapon=", eWeapon, ", bInfinite=", PICK_STRING(bInfinite, "TRUE", "FALSE"))
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets the player to have max ammo. 
PROC RANGE_SET_PLAYER_MAX_AMMO(WEAPON_TYPE eWeapon)
	IF HAS_PED_GOT_WEAPON(playerPed, eWeapon)
		INT iMaxAmmo
		GET_MAX_AMMO(playerPed, eWeapon, iMaxAmmo)
		SET_PED_AMMO(playerPed, eWeapon, iMaxAmmo)
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles net player setup for the range, including:
///    - teleporting
///    - beginning streaming
PROC RANGE_MP_HandlePlayerInit(RangeMP_PlayerBD & sPlayerBD[], RangeMP_PlayerData & sLocalPlayerData, RangeMP_ParticipantInfo & sParticipantInfo, INT iClientID)
	IF DOES_ENTITY_EXIST(playerPed)
		IF NOT IS_ENTITY_DEAD(playerPed)
			IF (iClientID = sParticipantInfo.iShooters[0])
				SET_BITMASK_AS_ENUM(sPlayerBD[iClientID].iFlags, RANGE_MP_F_IS_BLUE_PLAYER)
			ELIF (iClientID = sParticipantInfo.iShooters[1])
				SET_BITMASK_AS_ENUM(sPlayerBD[iClientID].iFlags, RANGE_MP_F_IS_RED_PLAYER)
			ENDIF
			sPlayerBD[iClientID].piCurPlayerID = PLAYER_ID()
			
			SET_CLIENT_RANGE_STATE(sPlayerBD[iClientID], RANGE_STATE_Init)
		ELSE
			DEBUG_MESSAGE("Local player ped not alive")
		ENDIF
	ELSE
		DEBUG_MESSAGE("Local player ped does not exist")
	ENDIF
	
	// Disable wanted & the cell phone.
	SET_WANTED_LEVEL_MULTIPLIER(0.0)
	SET_MAX_WANTED_LEVEL(0)
	DISABLE_CELLPHONE(TRUE)
	SET_PLAYER_NOISE_MULTIPLIER(PLAYER_ID(), 0.0)
	
	// Store this palyer's variations.
	GET_PED_VARIATIONS(PLAYER_PED_ID(), sLocalPlayerData.sVariations)
			
	// Now law
	ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE,FALSE)
	ENABLE_DISPATCH_SERVICE(DT_POLICE_HELICOPTER,FALSE)
	//SET_EVERYONE_IGNORE_PLAYER(GET_PLAYER_INDEX(), TRUE)	// Not available in freemode
	
	// Set the other player as in invalid target.
	SET_PED_CAN_BE_TARGETTED(playerPed, FALSE)
ENDPROC

/// PURPOSE:
///    Disables the shooting for the player for one frame.
PROC RANGE_PLAYER_DISABLE_SHOOTING()
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_RETICLE)
	DRAW_DEBUG_TEXT_2D( "Shooting Disabled", ( << 0.5, 0.5, 0.0 >> ) )
ENDPROC

PROC SETUP_PLAYER_FOR_ANOTHER_ROUND(RangeMP_PlayerBD & sThisPlayerBD, RangeMP_PlayerData & sThisPlayerData)
	RANGEMP_SET_PLAYER_CAM_DOWNRANGE(sThisPlayerData)
	CLEAR_CLIENT_FLAG(sThisPlayerBD, RANGE_MP_F_GivenWeapon)
	SET_CLIENT_RANGE_STATE(sThisPlayerBD, RANGE_STATE_DisplayRound_Countdown)
	RANGE_BUSYSPINNER_OFF()
	RESTART_TIMER_NOW(sThisPlayerData.genericTimer)
	CDEBUG2LN(DEBUG_SHOOTRANGE, "SETUP_PLAYER_FOR_ANOTHER_ROUND complete")
ENDPROC

/// PURPOSE:
///    Handles every frame updates for the player.
PROC PROCESS_RANGE_PLAYER(RangeMP_PlayerBD & sThisPlayerBD)
	// Player cannot melee at any time during the range.
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK1)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK2)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_BLOCK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DUCK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_COVER)
	
	// In certain states, the player doesn't get an ammo display...
	IF (sThisPlayerBD.eRangeState != RANGE_STATE_DisplayRound_Countdown) AND (sThisPlayerBD.eRangeState != RANGE_STATE_InRound)
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_ICON)
	ENDIF
	
//	CDEBUG2LN(DEBUG_SHOOTRANGE, "Disabling thru PROCESS_RANGE_PLAYER()")
ENDPROC

/// PURPOSE:
///    Player processes any events meant for him.
PROC PLAYER_PROCESS_EVENTS(INT iThisClientID, RangeMP_Round & sCurRound, RangeMP_PlayerData & sAllPlayerData[], CHALLENGE_INDEX eChalType, RangeMP_PlayerBD & myPlayerBD, RANGE_END_CONDITION &eTerminateReason, RangeMP_CoreData & sCoreInfo, RangeMP_ParticipantInfo & sParticipantInfo )
	iThisClientID = iThisClientID
	
	INT iCount
	RANGE_MP_EVENT Details
	
	REPEAT GET_NUMBER_OF_EVENTS(SCRIPT_EVENT_QUEUE_NETWORK) iCount
		EVENT_NAMES eventType = GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_NETWORK, iCount)
		
		SWITCH(eventType)
			CASE EVENT_NETWORK_SCRIPT_EVENT
				GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, Details, SIZE_OF(Details))
				
				// This client was ordered to update a target.
				IF (Details.Details.Type = SCRIPT_EVENT_TargetStatusChange)	
					CDEBUG2LN(DEBUG_SHOOTRANGE, "Client #", iThisClientID, " just got a call to swap target #", Details.iTargetHitIndex)
					
					// Set the target to flip!
					IF (eChalType = CI_GRID)
						IF (CHECK_TARGET_FLAG(sCurRound.oRoundObjects[Details.iTargetHitIndex], TARG_F_IS_RED))
							// Was red. Make blue.
							CLEAR_TARGET_FLAG(sCurRound.oRoundObjects[Details.iTargetHitIndex], TARG_F_IS_RED)
							SET_TARGET_FLAG(sCurRound.oRoundObjects[Details.iTargetHitIndex], TARG_F_FLIPPING_TO_BLUE)
							
							CDEBUG2LN(DEBUG_SHOOTRANGE, "Target was RED! Setting TARG_F_FLIPPING_TO_BLUE")
						ELSE
							// Was blue. Make red.
							CLEAR_TARGET_FLAG(sCurRound.oRoundObjects[Details.iTargetHitIndex], TARG_F_IS_BLUE)
							SET_TARGET_FLAG(sCurRound.oRoundObjects[Details.iTargetHitIndex], TARG_F_FLIPPING_TO_RED)
							CDEBUG2LN(DEBUG_SHOOTRANGE, "Target was BLUE! Setting TARG_F_FLIPPING_TO_RED")
						ENDIF
						
					ELIF (eChalType = CI_RANDOM)
						// Set it to rotate out.
						IF NOT CHECK_TARGET_FLAG(sCurRound.oRoundObjects[Details.iTargetHitIndex], TARG_F_ROTATING_OUT)
							// If it's rotating in, need to set the start time properly.
							IF CHECK_TARGET_FLAG(sCurRound.oRoundObjects[Details.iTargetHitIndex], TARG_F_ROTATING_IN)
								sCurRound.oRoundObjects[Details.iTargetHitIndex].fFlipTime = CONST_ROT_TIME_ENTER - sCurRound.oRoundObjects[Details.iTargetHitIndex].fFlipTime
							ENDIF
						
							CLEAR_TARGET_FLAG(sCurRound.oRoundObjects[Details.iTargetHitIndex], TARG_F_ROTATING_IN)
							SET_TARGET_FLAG(sCurRound.oRoundObjects[Details.iTargetHitIndex], TARG_F_ROTATING_OUT)
							SET_TARGET_FLAG(sCurRound.oRoundObjects[Details.iTargetHitIndex], TARG_F_COUNT_ME)
						ENDIF
						
						//See if the proper player hit the target.
						CLEAR_TARGET_FLAG(sCurRound.oRoundObjects[Details.iTargetHitIndex], TARG_F_HIT_BY_PROPER_COLOR)
						
						INT iRedPlayer, iBluePlayer
						iRedPlayer = sParticipantInfo.iShooters[RED_PLAYER_CLIENT_ID]
						iBluePlayer = sParticipantInfo.iShooters[BLUE_PLAYER_CLIENT_ID]
							
						IF iRedPlayer = k_InvalidParticipantID
							CDEBUG3LN( DEBUG_SHOOTRANGE, "PLAYER_PROCESS_EVENTS - iRedPlayer = k_InvalidParticipantID, this is potentially fatal." )
						ENDIF

						IF iBluePlayer = k_InvalidParticipantID
							CDEBUG3LN( DEBUG_SHOOTRANGE, "PLAYER_PROCESS_EVENTS - iBluePlayer = k_InvalidParticipantID, this is potentially fatal." )
						ENDIF
							
						IF CHECK_TARGET_FLAG(sCurRound.oRoundObjects[Details.iTargetHitIndex], TARG_F_IS_RED)
							IF (Details.iHitByClientID = iRedPlayer)
								SET_TARGET_FLAG(sCurRound.oRoundObjects[Details.iTargetHitIndex], TARG_F_HIT_BY_PROPER_COLOR)
							ENDIF
						ELSE
							IF (Details.iHitByClientID = iBluePlayer)
								SET_TARGET_FLAG(sCurRound.oRoundObjects[Details.iTargetHitIndex], TARG_F_HIT_BY_PROPER_COLOR)
							ENDIF
						ENDIF
					
					ELIF (eChalType = CI_COVERED)
						// Need to flip this target, but differently depending on the type of target.
						IF (Details.iTargetHitIndex >= ENUM_TO_INT(CI_GOAL_1_1))
							SET_TARGET_FLAG(sCurRound.oRoundObjects[Details.iTargetHitIndex], TARG_F_FLIP_OUT)
						ELSE	
							IF CHECK_TARGET_FLAG(sCurRound.oRoundObjects[Details.iTargetHitIndex], TARG_F_IS_RED)
								SET_TARGET_FLAG(sCurRound.oRoundObjects[Details.iTargetHitIndex], TARG_F_FLIPPING_TO_BLUE)
							ELSE
								SET_TARGET_FLAG(sCurRound.oRoundObjects[Details.iTargetHitIndex], TARG_F_FLIPPING_TO_RED)
							ENDIF
						ENDIF
					ENDIF
					
				ELIF (Details.Details.Type = SCRIPT_EVENT_CreateRandomTarget)
					// Clean.
					IF NOT IS_ENTITY_DEAD(sCurRound.oRoundObjects[Details.iSlotToTake].oTarget)
						ENTITY_INDEX oBacking
						oBacking = GET_ENTITY_ATTACHED_TO(sCurRound.oRoundObjects[Details.iSlotToTake].oTarget)
						IF NOT IS_ENTITY_DEAD(oBacking)
							DELETE_ENTITY(oBacking)
						ENDIF
						DELETE_ENTITY(sCurRound.oRoundObjects[Details.iSlotToTake].oTarget)
					ENDIF
				
					IF NOT IS_ENTITY_DEAD(sCurRound.oRoundObjects[Details.iSlotToTake].oMisc)
						DELETE_ENTITY(sCurRound.oRoundObjects[Details.iSlotToTake].oMisc)
					ENDIF
				
					// Just got a packet telling us where to create a target.
					CREATE_RANDOM_TARGET(sCoreInfo, sCurRound, sCurRound.oRoundObjects[Details.iSlotToTake], INT_TO_ENUM(GRID_INDEX, Details.iTargetHitIndex), Details.bBlueColor)						
					SET_TARGET_FLAG(sCurRound.oRoundObjects[Details.iSlotToTake], TARG_F_ROTATING_IN)
					
				ELIF (Details.Details.Type = SCRIPT_EVENT_Skill)
					// Nothing.
	
				ELIF (Details.Details.Type = SCRIPT_EVENT_Shots)
					// We were just handed the other guy's shooting stats.
					INT iOtherClient
					iOtherClient = RANGE_GET_OTHER_SHOOTER(iThisClientID, sParticipantInfo)
					sAllPlayerData[iOtherClient].iShotsFired = Details.iShotsFired
					sAllPlayerData[iOtherClient].iShotsHit = Details.iShotsHit
					
				ELIF (Details.Details.Type = SCRIPT_EVENT_ShotsRound)
					// We were just handed the other guy's shooting stats for this one round.
					INT iOtherClient
					iOtherClient = RANGE_GET_OTHER_SHOOTER(iThisClientID, sParticipantInfo)
					sAllPlayerData[iOtherClient].iShotsFiredThisRound = Details.iShotsFired
					sAllPlayerData[iOtherClient].iShotsHitThisRound = Details.iShotsHit
					sAllPlayerData[iOtherClient].iShotsMissedThisRound = Details.iShotsMissed
					
					SET_CLIENT_FLAG(myPlayerBD, RANGE_MP_F_ProcessedPostRound)
					CDEBUG2LN(DEBUG_SHOOTRANGE, "Processing eEventType SCRIPT_EVENT_ShotsRound")
				ELIF (Details.Details.Type = SCRIPT_EVENT_PlayerQuitRange)
					eTerminateReason = RANGE_END_PlayerLeft
				ENDIF
			BREAK
			
			DEFAULT
			BREAK
		ENDSWITCH
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Watches to make sure the player isn't leaving the range.
FUNC BOOL PLAYER_PROCESS_LEAVING(RangeMP_ServerBD & sServerBD, RangeMP_PlayerData & sThisPlayerData, RangeMP_CoreData & sCoreInfo, INT iClientID, RangeMP_ParticipantInfo & sParticipantInfo)
	DRAW_DEBUG_SPHERE( sCoreInfo.vRangeLeftCoords, RANGE_EXIT_WARNING_RADIUS, 255, 60, 0, 100 )
	DRAW_DEBUG_LINE( sCoreInfo.vRangeLeftCoords, sCoreInfo.vRangeLeftCoords + sCoreInfo.vExitNormal )
	DRAW_DEBUG_LINE( sCoreInfo.vRangeLeftCoords, GET_ENTITY_COORDS( PLAYER_PED_ID(), FALSE ) )
	IF NOT IS_PLAYER_FLAG_SET(sThisPlayerData, RANGE_MP_F_WARNED_ABOUT_LEAVING)
		IF NOT IS_ENTITY_DEAD(playerPed)
			IF GET_ENTITY_DISTANCE_FROM_LOCATION(playerPed, sCoreInfo.vRangeLeftCoords) < RANGE_EXIT_WARNING_RADIUS
				PRINT_HELP_FOREVER("SHR_MP_LEAVE")
				
				SET_PLAYER_FLAG(sThisPlayerData, RANGE_MP_F_WARNED_ABOUT_LEAVING)
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_ENTITY_DEAD(playerPed)
			IF GET_ENTITY_DISTANCE_FROM_LOCATION(playerPed, sCoreInfo.vRangeLeftCoords) > RANGE_EXIT_WARNING_RADIUS
				CLEAR_PLAYER_FLAG(sThisPlayerData, RANGE_MP_F_WARNED_ABOUT_LEAVING)
			ENDIF
			VECTOR vExitToPlayer = GET_ENTITY_COORDS( PLAYER_PED_ID(), FALSE ) - sCoreInfo.vRangeLeftCoords
			IF DOT_PRODUCT( sCoreInfo.vExitNormal, vExitToPlayer ) > 0
				IF NOT IS_RANGE_ONE_PLAYER()
					SET_SHOOTING_RANGE_UNSAVED_BITFLAG(SRB_QuitFromRange)
					IF sServerBD.iClient0MatchWins > sServerBD.iClient1MatchWins AND iClientID = sParticipantInfo.iShooters[1]
					OR sServerBD.iClient0MatchWins < sServerBD.iClient1MatchWins AND iClientID = sParticipantInfo.iShooters[0]
						BROADCAST_BETTING_MISSION_FINISHED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(RANGE_GET_OTHER_SHOOTER(iClientID, sParticipantInfo))))
						CDEBUG2LN(DEBUG_SHOOTRANGE, "PLAYER_PROCESS_LEAVING() :: BROADCAST_BETTING sServerBD.iClient0MatchWins=", sServerBD.iClient0MatchWins, ", sServerBD.iClient1MatchWins=", sServerBD.iClient1MatchWins, ", iClientID=", iClientID)
					ENDIF
				ENDIF
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Gives the palyer the correct weapon for the round.
PROC GIVE_PLAYER_RANGE_WEAPON(RoundDescription & sRoundDesc, RangeMP_PlayerBD & sPlayerBD, RangeMP_PlayerData & sPlayerData, BOOL bMidRoundReplenish = FALSE)	
	// Assign weapons.
	WEAPON_TYPE weapToGive = sRoundDesc.eWeaponType
	INT iAmmo = 0
	
	IF (sRoundDesc.eWeaponCategory = WEAPCAT_PISTOL)
		iAmmo = 500
		
	ELIF (sRoundDesc.eWeaponCategory = WEAPCAT_SMG)
		iAmmo = 500
		
	ELIF (sRoundDesc.eWeaponCategory = WEAPCAT_AR)
		iAmmo = 1000
		
	ELIF (sRoundDesc.eWeaponCategory = WEAPCAT_SHOTGUN)
		iAmmo = 200
	
	ELIF (sRoundDesc.eWeaponCategory = WEAPCAT_LIGHT_MG)
		iAmmo = 1000
		
	ELIF (sRoundDesc.eWeaponCategory = WEAPCAT_MINIGUN)
		iAmmo = 15000
	ENDIF
	
	IF NOT bMidRoundReplenish
		// Give that weapon.
		IF HAS_PED_GOT_WEAPON(playerPed, weapToGive)
			sPlayerData.bHadThisRoundsWeapon = TRUE
			sPlayerData.iPrevAmmo = GET_AMMO_IN_PED_WEAPON(playerPed, weapToGive)
			
			INT iClipAmmo
			IF GET_AMMO_IN_CLIP(playerPed, weapToGive, iClipAmmo)
				sPlayerData.iPrevAmmo += iClipAmmo
			ENDIF
			SET_PED_AMMO(playerPed, weapToGive, iAmmo)
			CDEBUG2LN(DEBUG_SHOOTRANGE, "HAS_PED_GOT_WEAPON(playerPed, ", GET_STRING_FROM_RANGE_WEAPON_TYPE(weapToGive), ") has ammo=", sPlayerData.iPrevAmmo)
		ELSE
			sPlayerData.bHadThisRoundsWeapon = FALSE
			GIVE_WEAPON_TO_PED(playerPed, weapToGive, iAmmo, FALSE)	
			CDEBUG2LN(DEBUG_SHOOTRANGE, "HAS_PED_GOT_WEAPON is false, giving the ped the weapon, iAmmo=", iAmmo)
		ENDIF
		
		SET_CURRENT_PED_WEAPON(playerPed, weapToGive, FALSE)
		TASK_SWAP_WEAPON(playerPed, TRUE)
		sPlayerData.eWeaponGiven = weapToGive
		SET_AMMO_IN_CLIP( playerPed, weapToGive, 500 )
		
		CDEBUG2LN(DEBUG_SHOOTRANGE, "GIVE_PLAYER_RANGE_WEAPON: ", GET_STRING_FROM_RANGE_WEAPON_TYPE(weapToGive), " :: ", weapToGive)
		
		// Flag us.
		SET_CLIENT_FLAG(sPlayerBD, RANGE_MP_F_GivenWeapon)
	ELIF bMidRoundReplenish
		IF HAS_PED_GOT_WEAPON(playerPed, weapToGive)
			IF GET_AMMO_IN_PED_WEAPON(playerPed, weapToGive) = 0
				GIVE_WEAPON_TO_PED(playerPed, weapToGive, iAmmo, TRUE)
			ELSE
				SET_AMMO_IN_CLIP( playerPed, weapToGive, iAmmo - GET_AMMO_IN_PED_WEAPON( playerPed, weapToGive ) )
			ENDIF
		ELSE
			GIVE_WEAPON_TO_PED(playerPed, weapToGive, iAmmo, TRUE)
		ENDIF
		
		SET_PED_AMMO(playerPed, weapToGive, iAmmo)
		CDEBUG2LN(DEBUG_SHOOTRANGE, "GIVE_PLAYER_RANGE_WEAPON: refilling ammunition, iAmmo=", iAmmo)
	ENDIF
	
	// Only the minigun gets infinite ammo.
	IF (weapToGive = WEAPONTYPE_MINIGUN)
		RANGE_SET_PLAYER_INFINITE_AMMO(sPlayerData.eWeaponGiven, TRUE)
	ELSE
		RANGE_SET_PLAYER_MAX_AMMO(sPlayerData.eWeaponGiven)
	ENDIF
ENDPROC

/// PURPOSE:
///    Forces the palyer to holster
PROC RANGE_PLAYER_HOLSTER_WEAPON()
	SET_CURRENT_PED_WEAPON(playerPed, WEAPONTYPE_UNARMED, FALSE)
	TASK_SWAP_WEAPON(playerPed, TRUE)
ENDPROC

/// PURPOSE:
///    Removes the weapon from the player if he didn't own it.
/// PARAMS:
///    sPlayerData - 
PROC RANGE_PLAYER_REMOVE_WEAPON(RangeMP_PlayerData & sPlayerData)
	IF NOT (sPlayerData.bHadThisRoundsWeapon)
		// Player didn't have the weapon for this round. Take it away from him.
		REMOVE_WEAPON_FROM_PED(playerPed, sPlayerData.eWeaponGiven)
	ELSE
		// He already owned it, but we gave him ammo. Restore his ammo.
		SET_PED_AMMO(playerPed, sPlayerData.eWeaponGiven, sPlayerData.iPrevAmmo)
	ENDIF
	sPlayerData.bHadThisRoundsWeapon = FALSE
ENDPROC


/// PURPOSE:
///    Erases all the palyer data for the range (weapons given, round timer, etc)
PROC RANGE_PLAYER_WIPE_DATA(RangeMP_PlayerData & sPlayerData)
	CDEBUG2LN(DEBUG_SHOOTRANGE, "RANGE_PLAYER_WIPE_DATA called")
	CANCEL_TIMER(sPlayerData.genericTimer)
	sPlayerData.eWeaponGiven = WEAPONTYPE_UNARMED
	sPlayerData.sPredict.iShotsFiredThisRound = 0
	sPlayerData.sPredict.iShotsHitThisRound = 0
	
	//sPlayerData.iShotsFired = 0
	//sPlayerData.iShotsHit = 0
	//sPlayerData.iTargetsAffected = 0
ENDPROC

/// PURPOSE:
///    Stores our shooting skill locally, and sends it to the other player.
PROC GET_AND_SEND_RANGE_PLAYER_SHOOTING_SKILL(RangeMP_PlayerBD & sPlayerBD[], INT iClientID)
	BROADCAST_SHOOTINGSKILL_TO_OTHER_PLAYER(GET_RANGE_MP_SHOOTING_ABILITY(), iClientID, sPlayerBD)
ENDPROC







