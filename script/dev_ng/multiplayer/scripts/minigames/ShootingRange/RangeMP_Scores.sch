// RangeMP_Scores.sch
USING "UIUtil.sch"

CONST_INT	RANGE_MP_MAX_FS				4		// The maximum number of floating score texts we can have.
CONST_FLOAT	RANGE_MP_SCORE_LIFETIME		0.66	// How long should a floating score live for?
CONST_FLOAT RANGE_MP_SCORE_SCALE 		0.73333 // 22px/30px (desired/default)
CONST_FLOAT RANGE_MP_SCORE_SPACING 		0.03472

STRUCT RANGE_MP_FLOATING_SCORE
	FLOAT 			fPosX
	FLOAT 			fPosY
	FLOAT			fFrameTime
	HUD_COLOURS		color
	INT				iAlpha
	INT				iPtValue
	BOOL			bActive
ENDSTRUCT

