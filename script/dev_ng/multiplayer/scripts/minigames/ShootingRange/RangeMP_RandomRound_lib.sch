// RangeMP_RandomRound_lib.sch3
USING "RangeMP_Core.sch"
USING "RangeMP_Round.sch"
USING "RangeMP_Targets_lib.sch"
USING "RangeMP_Broadcast_lib.sch"
USING "RangeMP_Scores_lib.sch"
USINg "RangeMP_UI.sch"
USING "shared_hud_displays.sch"
USING "RangeMP_Support_lib.sch"

CONST_INT	MAX_RANDOM_ROUND_TARGETS		8

#IF IS_DEBUG_BUILD
	WIDGET_GROUP_ID RR_Widgets
	
	PROC INIT_RANDOM_ROUND_WIDGETS(RangeMP_CoreData & sCoreInfo)		
		RR_Widgets = START_WIDGET_GROUP("Shooting Range")
			ADD_WIDGET_FLOAT_SLIDER("RR Col1 X", sCoreInfo.vRangeOffsetCol1.x, -3000.0, 3000.0, 0.005)
			ADD_WIDGET_FLOAT_SLIDER("RR Col1 Y", sCoreInfo.vRangeOffsetCol1.y, -3000.0, 3000.0, 0.005)
			ADD_WIDGET_FLOAT_SLIDER("RR Col1 Z", sCoreInfo.vRangeOffsetCol1.z, -3000.0, 3000.0, 0.005)
			
			ADD_WIDGET_FLOAT_SLIDER("RR Col2 X", sCoreInfo.vRangeOffsetCol2.x, -3000.0, 3000.0, 0.005)
			ADD_WIDGET_FLOAT_SLIDER("RR Col2 Y", sCoreInfo.vRangeOffsetCol2.y, -3000.0, 3000.0, 0.005)
			ADD_WIDGET_FLOAT_SLIDER("RR Col2 Z", sCoreInfo.vRangeOffsetCol2.z, -3000.0, 3000.0, 0.005)
			
			ADD_WIDGET_FLOAT_SLIDER("RR Col3 X", sCoreInfo.vRangeOffsetCol3.x, -3000.0, 3000.0, 0.005)
			ADD_WIDGET_FLOAT_SLIDER("RR Col3 Y", sCoreInfo.vRangeOffsetCol3.y, -3000.0, 3000.0, 0.005)
			ADD_WIDGET_FLOAT_SLIDER("RR Col3 Z", sCoreInfo.vRangeOffsetCol3.z, -3000.0, 3000.0, 0.005)
			
			ADD_WIDGET_FLOAT_SLIDER("RR Col4 X", sCoreInfo.vRangeOffsetCol4.x, -3000.0, 3000.0, 0.005)
			ADD_WIDGET_FLOAT_SLIDER("RR Col4 Y", sCoreInfo.vRangeOffsetCol4.y, -3000.0, 3000.0, 0.005)
			ADD_WIDGET_FLOAT_SLIDER("RR Col4 Z", sCoreInfo.vRangeOffsetCol4.z, -3000.0, 3000.0, 0.005)
			
			ADD_WIDGET_FLOAT_SLIDER("RR Col5 X", sCoreInfo.vRangeOffsetCol5.x, -3000.0, 3000.0, 0.005)
			ADD_WIDGET_FLOAT_SLIDER("RR Col5 Y", sCoreInfo.vRangeOffsetCol5.y, -3000.0, 3000.0, 0.005)
			ADD_WIDGET_FLOAT_SLIDER("RR Col5 Z", sCoreInfo.vRangeOffsetCol5.z, -3000.0, 3000.0, 0.005)
		STOP_WIDGET_GROUP()
	ENDPROC
	
	PROC UPDATE_RANDOM_ROUND_WIDGETS(RangeMP_CoreData & sCoreInfo)
		DRAW_DEBUG_SPHERE(sCoreInfo.vRangeFwdClose + sCoreInfo.vRangeOffsetCol1, 0.1, 255,255,255)
		DRAW_DEBUG_SPHERE(sCoreInfo.vRangeFwdClose + sCoreInfo.vRangeOffsetCol2, 0.1, 192,192,192)
		DRAW_DEBUG_SPHERE(sCoreInfo.vRangeFwdClose + sCoreInfo.vRangeOffsetCol3, 0.1, 128,128,128)
		DRAW_DEBUG_SPHERE(sCoreInfo.vRangeFwdClose + sCoreInfo.vRangeOffsetCol4, 0.1, 64,64,64)
		DRAW_DEBUG_SPHERE(sCoreInfo.vRangeFwdClose + sCoreInfo.vRangeOffsetCol5, 0.1, 0,0,0)
	ENDPROC
	
	PROC CLEANUP_RANDOM_ROUND_WIDGETS()
		DELETE_WIDGET_GROUP(RR_Widgets)
	ENDPROC
#ENDIF

/// PURPOSE:
///    Invoked only within the internal update. Private.
PROC RANGE_MP_UPDATE_RANDOM_ROUND_UI(INT iRedScore, INT iBlueScore, INT iClientID, RangeMP_PlayerBD & sPlayerBD[], RangeMP_ParticipantInfo & sParticipantInfo )	
	// Red is on left, Blue is on right.	
	Range_HUD_Data sHUDData
	sHUDData.sCurScoreLabel = ""
	
	INT iRedPlayer = sParticipantInfo.iShooters[RED_PLAYER_CLIENT_ID]
	
	IF iRedPlayer = k_InvalidParticipantID
		CDEBUG3LN( DEBUG_SHOOTRANGE, "RANGE_MP_UPDATE_RANDOM_ROUND_UI - iRedPlayer = k_InvalidParticipantID, this is potentially fatal." )
	ENDIF	
	
	// Red is on left, Blue is on right.
	IF (iClientID = iRedPlayer)
		sHUDData.iCurScore = iRedScore
		sHUDData.eCurScoreColor = HUD_COLOUR_ORANGE
		
		sHUDData.sOpponentScoreLabel = "SHR_MP_OSCORE_C"
		sHUDData.iOpponentScore = iBlueScore
		sHUDData.eOpponentScoreColor = HUD_COLOUR_PURPLE	
	ELSE
		sHUDData.iCurScore = iBlueScore
		sHUDData.eCurScoreColor = HUD_COLOUR_PURPLE
		
		sHUDData.sOpponentScoreLabel = "SHR_MP_OSCORE_C"
		sHUDData.iOpponentScore = iRedScore
		sHUDData.eOpponentScoreColor = HUD_COLOUR_ORANGE	
	ENDIF
	
	// Get the player names to display.
	sHUDData.bDrawPlayerNames = TRUE
	sHUDData.sCurScoreLabel = GET_PLAYER_NAME(sPlayerBD[iClientID]. piCurPlayerID)
	
	IF NOT IS_RANGE_ONE_PLAYER()
		sHUDData.sOpponentScoreLabel = GET_PLAYER_NAME(sPlayerBD[RANGE_GET_OPPOSING_SHOOTER(sParticipantInfo)]. piCurPlayerID)
	ENDIF
	
	IF IS_RANGE_ONE_PLAYER()
		sHUDData.iOpponentScore = -1
	ENDIF
	
	IF IS_CLIENT_FLAG_SET(sPlayerBD[iClientID], RANGE_MP_F_IsSpectator)
		sHUDData.iCurScore = -1
	ENDIF
	
	// Trigger the draw.
	DRAW_SHOOTING_RANGE_HUD(sHUDData, TRUE)
ENDPROC

/// PURPOSE:
///    Round init.
PROC RANGE_MP_CREATE_RANDOM_ROUND(RangeMP_CoreData & sCoreInfo, RangeMP_Round& sRndInfo, RangeMP_PlayerBD & sPlayerBD)
	DEBUG_MESSAGE("RANGE_MP_CREATE_RANDOM_ROUND: Creating RANDOM variant...")
	
	IF NOT IS_BIT_SET(sPlayerBD.iTargetsField2, 31)
		// Create 8 targets here in the style:
		// - - - - -
		// - - - - -
		// R R - B B
		// R R - B B
		CREATE_RANDOM_TARGET(sCoreInfo, sRndInfo, sRndInfo.oRoundObjects[0], gridA1, FALSE)
		CREATE_RANDOM_TARGET(sCoreInfo, sRndInfo, sRndInfo.oRoundObjects[1], gridA2, FALSE)
		CREATE_RANDOM_TARGET(sCoreInfo, sRndInfo, sRndInfo.oRoundObjects[2], gridB1, FALSE)
		CREATE_RANDOM_TARGET(sCoreInfo, sRndInfo, sRndInfo.oRoundObjects[3], gridB2, FALSE)
		SET_BIT(sPlayerBD.iTargetsField1, ENUM_TO_INT(gridA1))
		SET_BIT(sPlayerBD.iTargetsField1, ENUM_TO_INT(gridA2))
		SET_BIT(sPlayerBD.iTargetsField1, ENUM_TO_INT(gridB1))
		SET_BIT(sPlayerBD.iTargetsField1, ENUM_TO_INT(gridB2))
		
		CREATE_RANDOM_TARGET(sCoreInfo, sRndInfo, sRndInfo.oRoundObjects[4], gridA4)
		CREATE_RANDOM_TARGET(sCoreInfo, sRndInfo, sRndInfo.oRoundObjects[5], gridA5)
		CREATE_RANDOM_TARGET(sCoreInfo, sRndInfo, sRndInfo.oRoundObjects[6], gridB4)
		CREATE_RANDOM_TARGET(sCoreInfo, sRndInfo, sRndInfo.oRoundObjects[7], gridB5)
		SET_BIT(sPlayerBD.iTargetsField2, ENUM_TO_INT(gridA4))
		SET_BIT(sPlayerBD.iTargetsField2, ENUM_TO_INT(gridA5))
		SET_BIT(sPlayerBD.iTargetsField2, ENUM_TO_INT(gridB4))
		SET_BIT(sPlayerBD.iTargetsField2, ENUM_TO_INT(gridB5))
		
		SET_TARGET_FLAG(sRndInfo.oRoundObjects[0], TARG_F_ROTATING_IN)
		SET_TARGET_FLAG(sRndInfo.oRoundObjects[1], TARG_F_ROTATING_IN)
		SET_TARGET_FLAG(sRndInfo.oRoundObjects[2], TARG_F_ROTATING_IN)
		SET_TARGET_FLAG(sRndInfo.oRoundObjects[3], TARG_F_ROTATING_IN)
		SET_TARGET_FLAG(sRndInfo.oRoundObjects[4], TARG_F_ROTATING_IN)
		SET_TARGET_FLAG(sRndInfo.oRoundObjects[5], TARG_F_ROTATING_IN)
		SET_TARGET_FLAG(sRndInfo.oRoundObjects[6], TARG_F_ROTATING_IN)
		SET_TARGET_FLAG(sRndInfo.oRoundObjects[7], TARG_F_ROTATING_IN)
	ELSE	// spectator joining midsession bit isn't set
		CLEAR_BIT(sPlayerBD.iTargetsField2, 31)
		INT index, iRoundObjects=0
		FOR index = gridA1 TO MAX_GRID_LOCS
			IF IS_BIT_SET(sPlayerBD.iTargetsField1, index) AND iRoundObjects < 8
				CDEBUG2LN(DEBUG_SHOOTRANGE, "RANGE_MP_CREATE_RANDOM_ROUND :: ORANGE target at index=", index)
				CREATE_RANDOM_TARGET(sCoreInfo, sRndInfo, sRndInfo.oRoundObjects[iRoundObjects], INT_TO_ENUM(GRID_INDEX, index), FALSE)
				SET_ENTITY_ROTATION(sRndInfo.oRoundObjects[iRoundObjects].oMisc, (<<0.0,0.0,160.0>>))
				iRoundObjects++
			ELIF IS_BIT_SET(sPlayerBD.iTargetsField2, index) AND iRoundObjects < 8
				CDEBUG2LN(DEBUG_SHOOTRANGE, "RANGE_MP_CREATE_RANDOM_ROUND :: PURPLE target at index=", index)
				CREATE_RANDOM_TARGET(sCoreInfo, sRndInfo, sRndInfo.oRoundObjects[iRoundObjects], INT_TO_ENUM(GRID_INDEX, index), TRUE)
				SET_ENTITY_ROTATION(sRndInfo.oRoundObjects[iRoundObjects].oMisc, (<<0.0,0.0,160.0>>))
				iRoundObjects++
			ENDIF
		ENDFOR
		CLEAR_TARGET_FLAG(sRndInfo.oRoundObjects[0], TARG_F_ROTATING_IN)
		CLEAR_TARGET_FLAG(sRndInfo.oRoundObjects[1], TARG_F_ROTATING_IN)
		CLEAR_TARGET_FLAG(sRndInfo.oRoundObjects[2], TARG_F_ROTATING_IN)
		CLEAR_TARGET_FLAG(sRndInfo.oRoundObjects[3], TARG_F_ROTATING_IN)
		CLEAR_TARGET_FLAG(sRndInfo.oRoundObjects[4], TARG_F_ROTATING_IN)
		CLEAR_TARGET_FLAG(sRndInfo.oRoundObjects[5], TARG_F_ROTATING_IN)
		CLEAR_TARGET_FLAG(sRndInfo.oRoundObjects[6], TARG_F_ROTATING_IN)
		CLEAR_TARGET_FLAG(sRndInfo.oRoundObjects[7], TARG_F_ROTATING_IN)
	ENDIF
	
//#IF IS_DEBUG_BUILD
//	INIT_RANDOM_ROUND_WIDGETS(sCoreInfo)
//#ENDIF
ENDPROC

/// PURPOSE:
///    Round update.
PROC RANGE_MP_START_RANDOM_ROUND(RangeMP_Round& sRndInfo)
	DEBUG_MESSAGE("RANGE_MP_START_RANDOM_ROUND: Updating RANDOM variant...")
	UNUSED_PARAMETER(sRndInfo)
ENDPROC

/// PURPOSE:
///    Setup up round, if needed.
/// RETURNS:
///    TRUE when done.
FUNC BOOL RANGE_MP_SETUP_RANDOM_ROUND(RangeMP_Round & sRndInfo)	
	// Need to rotate in all of our targets.
	BOOL bAllDone = TRUE
	
	INT index 
	REPEAT MAX_RANDOM_ROUND_TARGETS index
		IF CHECK_TARGET_FLAG(sRndInfo.oRoundObjects[index], TARG_F_ROTATING_IN)
			BOOL bSound = ((index = 0) OR (index = 3) OR (index = 4) OR (index = 7))
			IF ROTATE_RANDOM_TARGET(sRndInfo, sRndInfo.oRoundObjects[index], TRUE, bSound)
				CLEAR_TARGET_FLAG(sRndInfo.oRoundObjects[index], TARG_F_ROTATING_IN)
			ENDIF
			bAllDone = FALSE
		ENDIF
	ENDREPEAT

	RETURN bAllDone
ENDFUNC

/// PURPOSE:
///    Resets the impact coord array and the impact coord counter
/// PARAMS:
///    sPlayerData - the local data for this player
PROC RANGE_MP_INIT_IMPACT_COORDS( RangeMP_PlayerBD & playerBD )

	INT i = 0, j = 0
	REPEAT RANGE_MAX_STORED_IMPACT_ARRAYS j
		REPEAT RANGE_MAX_STORED_IMPACT_COORDS i
			playerBD.vImpactCoordArray[ j ][ i ] = << 0,0,0 >>
		ENDREPEAT
	ENDREPEAT
	
	SET_RANGE_IMPACT_COORD_ARRAY_COUNTER_ORANGE( playerBD, 0 )
	SET_RANGE_IMPACT_COORD_ARRAY_COUNTER_PURPLE( playerBD, 0 )
	
ENDPROC

/// PURPOSE:
///    Stores a vector into a rolling array of impact coords that's calculated for the post-round menu images
/// PARAMS:
///    sPlayerData - the local data for this player
///    oTarget - The target that was hit
PROC RANGE_MP_STORE_IMPACT_COORD( RangeMP_PlayerBD & playerBD, ENTITY_INDEX oTarget, BOOL bIsTargetOrange )

	VECTOR vHitCoord
	GET_PED_LAST_WEAPON_IMPACT_COORD( PLAYER_PED_ID(), vHitCoord )
	VECTOR vFront, vSide, vUp, vPos
	GET_ENTITY_MATRIX( oTarget, vFront, vSide, vUp, vPos )
	VECTOR vHitRelative = vHitCoord - vPos
	VECTOR vImpactCoord
	
	vImpactCoord.x = DOT_PRODUCT( vHitRelative, vSide ) * PICK_INT( bIsTargetOrange, 1, -1 )	// PICK_INT because targets are reverse/obverse sides of the same prop.
	vImpactCoord.y = DOT_PRODUCT( vHitRelative, vUp ) * -1	// Prop seems to be upside down in both color instances.
	vImpactCoord.z = DOT_PRODUCT( vFront, vHitRelative )	// Tells us if the collision is from a shotgun and, potentially, wildly off.
	
	INT iArrayIndex
	
	// Get the temp values we're using to index into the matrix
	RANGE_IMPACT_COORD_ARRAY eArray
	IF bIsTargetOrange
		eArray = RICA_ORANGE_IMPACTS
		iArrayIndex = GET_RANGE_IMPACT_COORD_ARRAY_COUNTER_ORANGE( playerBD )
	ELSE
		eArray = RICA_PURPLE_IMPACTS
		iArrayIndex = GET_RANGE_IMPACT_COORD_ARRAY_COUNTER_PURPLE( playerBD )
	ENDIF
	
	playerBD.vImpactCoordArray[ eArray ][ iArrayIndex ] = vImpactCoord
	
	iArrayIndex++
	
	IF iArrayIndex >= RANGE_MAX_STORED_IMPACT_COORDS
		iArrayIndex = 0
	ENDIF
	
	// Re-store the incremented counter variables in the appropriate spots for our matrix
	IF bIsTargetOrange
		SET_RANGE_IMPACT_COORD_ARRAY_COUNTER_ORANGE( playerBD, iArrayIndex )
	ELSE
		SET_RANGE_IMPACT_COORD_ARRAY_COUNTER_PURPLE( playerBD, iArrayIndex )
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Round update.
PROC RANGE_MP_UPDATE_RANDOM_ROUND(RangeMP_Round& sRndInfo, INT iClientID, RangeMP_PlayerData & sThisPlayerData, 
		RangeMP_PlayerBD & sPlayerBD[], INT iClient0Score, INT iClient1Score, RangeMP_MenuData & sMenuData, RangeMP_ParticipantInfo & sParticipantInfo,  BOOL bRenderHUD = TRUE)
	// Go through all of the round targets, and figure out if we've shot one.
	INT index = 0
		
	// Store our current player once.
	ENTITY_INDEX playerEntity = GET_ENTITY_FROM_PED_OR_VEHICLE(playerPed)
	
	// If we've fired a shot this frame, store it:
	BOOL bTrackingForMiss = FALSE
	VECTOR vDontCare
	IF GET_PED_LAST_WEAPON_IMPACT_COORD(playerPed, vDontCare) AND NOT IS_PLAYER_FLAG_SET(sThisPlayerData, RANGE_MP_F_DONT_COUNT_SHOTS_FIRED)
		sThisPlayerData.iShotsFired += 1
		sThisPlayerData.iShotsFiredThisRound += 1
		bTrackingForMiss = TRUE
	ENDIF
	
	INT iRedPlayer = sParticipantInfo.iShooters[RED_PLAYER_CLIENT_ID]
	
	IF iRedPlayer = k_InvalidParticipantID
		CDEBUG3LN( DEBUG_SHOOTRANGE, "RANGE_MP_UPDATE_RANDOM_ROUND - iRedPlayer = k_InvalidParticipantID, this is potentially fatal." )
	ENDIF	
	
	// HANDLE THE RANDOM ROUND UPDATE. 
	// General update.
	REPEAT MAX_RANDOM_ROUND_TARGETS index
		// Always heal the arm and backing.
		IF NOT IS_ENTITY_DEAD(sRndInfo.oRoundObjects[index].oMisc)
			SET_ENTITY_HEALTH(sRndInfo.oRoundObjects[index].oMisc, TARGET_STARTING_HEALTH)
		ENDIF
		
		// Detect if the target was hit.
		IF NOT IS_ENTITY_DEAD(sRndInfo.oRoundObjects[index].oTarget)
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(sRndInfo.oRoundObjects[index].oTarget, playerEntity)
				SET_ENTITY_HEALTH(sRndInfo.oRoundObjects[index].oTarget, TARGET_STARTING_HEALTH)
				
				// Only register the hit proper if we weren't already rotating out.
				// #1335326 :: Allow targets to be shot on their way in, as well. 
				IF NOT (CHECK_TARGET_FLAG(sRndInfo.oRoundObjects[index], TARG_F_ROTATING_OUT)) //AND NOT
						//CHECK_TARGET_FLAG(sRndInfo.oRoundObjects[index], TARG_F_ROTATING_IN)
					
					RANGE_MP_STORE_IMPACT_COORD( sPlayerBD[ PARTICIPANT_ID_TO_INT() ], sRndInfo.oRoundObjects[index].oTarget, CHECK_TARGET_FLAG(sRndInfo.oRoundObjects[index], TARG_F_IS_RED) )
					
					// Can hit a target of any color!
					// Need to broadcast to tell other players to flip this target.
					CDEBUG2LN(DEBUG_SHOOTRANGE, "Player ID ", iClientID, " broadcasting a hit on target ", index)
					BROADCAST_HIT_TO_OTHER_PLAYERS(index, iClientID, sPlayerBD)
					
					// If w're rotating in, set our start time properly for the rotate out.
					IF CHECK_TARGET_FLAG(sRndInfo.oRoundObjects[index], TARG_F_ROTATING_IN)
						sRndInfo.oRoundObjects[index].fFlipTime = CONST_ROT_TIME_ENTER - sRndInfo.oRoundObjects[index].fFlipTime
					ELSE
						sRndInfo.oRoundObjects[index].fFlipTime = 0.0
					ENDIF

					CLEAR_TARGET_FLAG(sRndInfo.oRoundObjects[index], TARG_F_ROTATING_IN)
					SET_TARGET_FLAG(sRndInfo.oRoundObjects[index], TARG_F_ROTATING_OUT)
					SET_TARGET_FLAG(sRndInfo.oRoundObjects[index], TARG_F_COUNT_ME)
					
					// Clear the bit that says this target is down
					IF CHECK_TARGET_FLAG(sRndInfo.oRoundObjects[index], TARG_F_IS_RED)
						CLEAR_BIT(sPlayerBD[iClientID].iTargetsField1, ENUM_TO_INT(sRndInfo.oRoundObjects[index].eGridLoc))
					ELSE
						CLEAR_BIT(sPlayerBD[iClientID].iTargetsField2, ENUM_TO_INT(sRndInfo.oRoundObjects[index].eGridLoc))
					ENDIF
					
					// Did we hit a proper target color?
					BOOL bGoodHit = FALSE
					IF (iClientID = iRedPlayer)
						IF CHECK_TARGET_FLAG(sRndInfo.oRoundObjects[index], TARG_F_IS_RED)
							SET_TARGET_FLAG(sRndInfo.oRoundObjects[index], TARG_F_HIT_BY_PROPER_COLOR)
							bGoodHit = TRUE
						ENDIF
					ELSE
						IF CHECK_TARGET_FLAG(sRndInfo.oRoundObjects[index], TARG_F_IS_BLUE)
							SET_TARGET_FLAG(sRndInfo.oRoundObjects[index], TARG_F_HIT_BY_PROPER_COLOR)
							bGoodHit = TRUE
						ENDIF
					ENDIF
					
					// Add a floating score based on what we just did.
					IF bGoodHit
						RANGE_MP_ADD_FLOATING_SCORE(CI_RANDOM_PTS_PER_TGT, HUD_COLOUR_WHITE, sMenuData.floatingScores)
						sThisPlayerData.iShotsHit += 1
						sThisPlayerData.iShotsHitThisRound += 1
						INCREMENT_MP_INT_CHARACTER_STAT(MP_STAT_HITS_PEDS_VEHICLES)
						bTrackingForMiss = FALSE
					ELSE
						RANGE_MP_ADD_FLOATING_SCORE(CI_RANDOM_PTS_LOST, HUD_COLOUR_ORANGE, sMenuData.floatingScores)
					ENDIF
				ENDIF
				
				// Clear damage so it's not counted twice.
				CLEAR_ENTITY_LAST_DAMAGE_ENTITY(sRndInfo.oRoundObjects[index].oTarget)
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Did we miss?
	IF bTrackingForMiss
		sThisPlayerData.iShotsMissedThisRound += 1
	ENDIF
	
	// Draw our score onscreen.
	IF bRenderHUD
		RANGE_MP_UPDATE_RANDOM_ROUND_UI(iClient0Score, iClient1Score, iClientID, sPlayerBD, sParticipantInfo)
		
		RANGE_MP_DRAW_FLOATING_SCORES(sMenuData.floatingScores)
	ENDIF
	
	// Update the targets.
	REPEAT MAX_RANDOM_ROUND_TARGETS index
		UPDATE_RANDOM_TARGET(sRndInfo, sRndInfo.oRoundObjects[index])
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Round cleanup.
PROC RANGE_MP_END_RANDOM_ROUND(RangeMP_Round& sRndInfo)
	CDEBUG2LN(DEBUG_SHOOTRANGE, "RANGE_MP_END_RANDOM_ROUND: Ending RANDOM variant...")
	
	// Destroy the misc object (not using this yet... may?)
	IF NOT IS_ENTITY_DEAD(sRndInfo.oMisc)
		DELETE_ENTITY(sRndInfo.oMisc)
	ENDIF
	
	// Destroy all targets.
	INT index
	REPEAT CI_MAX_GRID_OBJECTS index
		IF NOT IS_ENTITY_DEAD(sRndInfo.oRoundObjects[index].oTarget)
			ENTITY_INDEX oBacking = GET_ENTITY_ATTACHED_TO(sRndInfo.oRoundObjects[index].oTarget)
			IF NOT IS_ENTITY_DEAD(oBacking)
				DELETE_ENTITY(oBacking)
			ENDIF
			DELETE_ENTITY(sRndInfo.oRoundObjects[index].oTarget)
		ENDIF
		
		IF NOT IS_ENTITY_DEAD(sRndInfo.oRoundObjects[index].oMisc)
			DELETE_ENTITY(sRndInfo.oRoundObjects[index].oMisc)
		ENDIF
		sRndInfo.oRoundObjects[index].iFlags = 0
	ENDREPEAT
	
//#IF IS_DEBUG_BUILD
//	CLEANUP_RANDOM_ROUND_WIDGETS()
//#ENDIF
ENDPROC



