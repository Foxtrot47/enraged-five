// RangeMP_core.sch
USING "commands_network.sch"
USING "net_include.sch"
USING "minigames_helpers.sch"
USING "shared_hud_displays.sch"
USING "socialclub_leaderboard.sch"

CONST_INT	MP_RANGE_MAX_WEAPONS		5		// How many weapon categories are in the range.
CONST_INT	AUDIO_STREAM_MAX_ATTEMPTS	200		// How many frames we'll try to stream the audio bank before we just give up.
CONST_INT	RANGE_BLUR_DURATION			250		// Blur in/out effect in ms
TWEAK_FLOAT	RANGE_EXIT_WARNING_RADIUS	4.00
CONST_INT	k_RANGE_MALE_HEIST_SPEC_VALUE		493
CONST_INT	k_RANGE_FEMALE_HEIST_SPEC_VALUE		301

ENUM RANGE_MP_GAME_STATE
	GAME_STATE_INIT = 0,
	
	GAME_STATE_TELEPORT,				// Client only - needed for joining from afar.
	GAME_STATE_FADE_IN,					// Client only - needed for joining from afar.
	
	GAME_STATE_STREAM,					// Client only state.
	
	GAME_STATE_WAIT_ALL_LOAD,			// Wait for all the clients to be done loading.
	
	GAME_STATE_RUN_INITAL,
	GAME_STATE_RUN_SPECTATOR,
	GAME_STATE_RUN_SCTV,
	GAME_STATE_SPAWN_ROUND,
	GAME_STATE_RUNNING,
	GAME_STATE_FAILED,
	GAME_STATE_LEAVE,
	GAME_STATE_END,
	GAME_STATE_END_SPLASH,
	
	GAME_STATE_TERMINATE
ENDENUM

ENUM RANGE_END_CONDITION
	RANGE_END_None = 0,
	RANGE_END_PlayerLeft = 1,
	RANGE_END_NoMenuInput = 2
ENDENUM

ENUM RANGE_STATE
	RANGE_STATE_NONE = -1000,
	RANGE_STATE_Init = 0,
	RANGE_STATE_Init_Wait,
	RANGE_STATE_MenuUpdate,
	
	RANGE_STATE_MenuFail_ServerUpdatePicker,
	
	RANGE_STATE_WaitForAllClients_PostMenu,
	RANGE_STATE_DisplayRound_Countdown,
	RANGE_STATE_InRound,
	RANGE_STATE_RoundOver,
	RANGE_STATE_RoundOverWait,
	RANGE_STATE_RoundOver_ScoreUs,
	RANGE_STATE_DisplayRoundOver,
	RANGE_STATE_WaitForAllClients_PostScorecard,
	RANGE_STATE_ExitConfSplash_MainMenu,
	RANGE_STATE_ExitConfSplash_Results
ENDENUM

FUNC STRING GET_STRING_FROM_RANGE_STATE(RANGE_STATE eState)
	SWITCH eState
		CASE RANGE_STATE_NONE								RETURN "RANGE_STATE_NONE"
		CASE RANGE_STATE_Init								RETURN "RANGE_STATE_Init"
		CASE RANGE_STATE_Init_Wait							RETURN "RANGE_STATE_Init_Wait"
		CASE RANGE_STATE_MenuUpdate							RETURN "RANGE_STATE_MenuUpdate"
		CASE RANGE_STATE_MenuFail_ServerUpdatePicker		RETURN "RANGE_STATE_MenuFail_ServerUpdatePicker"
		CASE RANGE_STATE_WaitForAllClients_PostMenu			RETURN "RANGE_STATE_WaitForAllClients_PostMenu"
		CASE RANGE_STATE_DisplayRound_Countdown				RETURN "RANGE_STATE_DisplayRound_Countdown"
		CASE RANGE_STATE_InRound							RETURN "RANGE_STATE_InRound"
		CASE RANGE_STATE_RoundOver							RETURN "RANGE_STATE_RoundOver"
		CASE RANGE_STATE_RoundOverWait						RETURN "RANGE_STATE_RoundOverWait"
		CASE RANGE_STATE_RoundOver_ScoreUs					RETURN "RANGE_STATE_RoundOver_ScoreUs"
		CASE RANGE_STATE_DisplayRoundOver					RETURN "RANGE_STATE_DisplayRoundOver"
		CASE RANGE_STATE_WaitForAllClients_PostScorecard	RETURN "RANGE_STATE_WaitForAllClients_PostScorecard"
		CASE RANGE_STATE_ExitConfSplash_MainMenu			RETURN "RANGE_STATE_ExitConfSplash_MainMenu"
	ENDSWITCH
	RETURN "Unknown RANGE_STATE enum"
ENDFUNC

ENUM THREE_STATE_RETVAL
	RETVAL_NONE = 0,
	RETVAL_TRUE,
	RETVAL_FALSE
ENDENUM

STRUCT RangeMP_CoreData
	INTERIOR_INSTANCE_INDEX RangeInterior = NULL
	OBJECT_INDEX			oEarmuffs
	
	INT 					iWinnings
	
	RANGE_LOCATION			eLocation
	
	VECTOR					vSpectatorPos
	FLOAT					fSpectatorHead
	
	VECTOR					vRangeLeftCoords
	VECTOR					vExitNormal
	VECTOR 					vInteriorCoords
	VECTOR 					vKillFireCoords
	
	VECTOR					vRangeFwdTarget
	VECTOR					vRangeFwdClose
	
	VECTOR 					vRangeOffsetCol1
	VECTOR 					vRangeOffsetCol2
	VECTOR 					vRangeOffsetCol3
	VECTOR 					vRangeOffsetCol4
	VECTOR 					vRangeOffsetCol5	
	
	
	VECTOR					vSpec1[2]
	VECTOR					vSpec1Goto[2]
	
	VECTOR					vSpec2[2]
	VECTOR					vSpec2Goto[2]
	
	VECTOR					vSpec3[2]
	VECTOR					vSpec3Goto[2]
	
	VECTOR					vSpec4[2]
	VECTOR					vSpec4Goto[2]
	
	VECTOR					vSpec5[2]
	VECTOR					vSpec5Goto[2]
	
	VECTOR					vSpec6[2]
	VECTOR					vSpec6Goto[2]
	
	SC_LEADERBOARD_CONTROL_STRUCT rangeLBD
	
	PED_COMP_NAME_ENUM		eMaskProp = DUMMY_PED_COMP
	PED_COMP_NAME_ENUM		eHeadProp = DUMMY_PED_COMP
	PED_COMP_NAME_ENUM		eSpecProp = DUMMY_PED_COMP
	
ENDSTRUCT

// Model Names
CONST_INT		iMAX_ASSETS		5
// Models so far:
/*
PROP_TARGET_FRAME_01
PROP_TARGET_RED
PROP_TARGET_ORA_PURP_01

PROP_TARGET_BACKBOARD_B
PROP_TARGET_ARM
PROP_TARGET_ORANGE_ARROW
PROP_TARGET_PURP_ARROW
*/

