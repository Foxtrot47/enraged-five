// RangeMP_Round_lib.sch
USING "RangeMP_Round.sch"
USING "RangeMP_Server.sch"
USING "RangeMP_Player.sch"
USING "RangeMP_Targets_lib.sch"

USING "RangeMP_UI_lib.sch"
USING "RangeMP_Support_lib.sch"

/// PURPOSE:
///    Lets us know when the round time is over.
/// RETURNS:
///    FALSE when time is up.
FUNC BOOL ROUND_UPDATE_TIME(RoundDescription & sRoundDesc, RangeMP_Round & sRoundData, RangeMP_MenuData & sMenuData, BOOL bSpectator = FALSE)
	#IF IS_DEBUG_BUILD
	IF DEBUG_INFINITE_ROUND_TIME
		DRAW_DEBUG_TEXT_2D( "DEBUG_INFINITE_ROUND_TIME active", ( << 0.1, 0.9, 0.0 >> ) )
		RETURN TRUE
	ENDIF
	#ENDIF
	// Make sure we've started timing the round.
	IF NOT IS_TIMER_STARTED(sRoundData.sRoundTimer)
		RESTART_TIMER_NOW(sRoundData.sRoundTimer)
	ENDIF
	
	// Depending on our variant, we need to handle time differently.
	FLOAT fTotalLength = 60.0
	IF (sRoundDesc.eChallenge = CI_GRID)
		fTotalLength = GRID_ROUND_LENGTH
	ELIF (sRoundDesc.eChallenge = CI_RANDOM)
		fTotalLength = RAND_ROUND_LENGTH
	ELIF (sRoundDesc.eChallenge = CI_COVERED)
		fTotalLength = COVR_ROUND_LENGTH
	ENDIF
	
	// Okay, subtract the times so that we display what's left.
	FLOAT fTimeLeft = fTotalLength - GET_TIMER_IN_SECONDS(sRoundData.sRoundTimer)
	
	// Out of time?
	BOOL bRetVal = TRUE
	IF (fTimeLeft < 0.0)
		fTimeLeft = 0.0
		bRetVal = FALSE
		
		// Wipe the help to end the round.
		CLEAR_HELP()
		
		// Going to use the round timer to display other things now. 
		RESTART_TIMER_NOW(sRoundData.sRoundTimer)
		STOP_SOUND( RANGE_GET_ROUND_COUNTDOWN_SOUND_ID( sRoundData ) )
		PLAY_SOUND_FRONTEND( RANGE_GET_ROUND_COUNTDOWN_SOUND_ID( sRoundData ), "TIMER_STOP_MASTER")
		SET_BITMASK_AS_ENUM(sRoundData.iFlags, ROUND_PLAYED_TIMEOVER_SOUND)
		
		// Freeze us.
		IF NOT IS_ENTITY_DEAD(playerPed)
			SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_REMOVE_EXPLOSIONS|SPC_REMOVE_PROJECTILES|SPC_REMOVE_FIRES|SPC_CLEAR_TASKS)
		ENDIF
	ENDIF
	
	// Draw, unless we're a spectator.
	IF NOT bSpectator
		DISPLAY_ROUND_TIMER(fTimeLeft, sMenuData, sRoundData)
	ENDIF

	RETURN bRetVal
ENDFUNC

/// PURPOSE:
///    Clears all the data stored in the passed in round description.
PROC RANGE_ROUND_WIPE_DATA(RoundDescription & sRoundDesc)
	sRoundDesc.eWeaponCategory = INT_TO_ENUM(RANGE_WEAPON_CATEGORY, -1)
	sRoundDesc.eChallenge = INT_TO_ENUM(CHALLENGE_INDEX, -1)
	sRoundDesc.eWeaponType = WEAPONTYPE_INVALID
ENDPROC

/// PURPOSE:
///    Forcefully destroys any objects created for the round.
PROC ROUND_WIPE_ALL_OBJECTS(RangeMP_Round & sRoundData)	
	// Destroy all targets.
	INT index
	FOR index = 0 TO ENUM_TO_INT(CI_GOAL_END)
		IF DOES_ENTITY_EXIST(sRoundData.oRoundObjects[index].oTarget)
			IF NOT IS_ENTITY_DEAD(sRoundData.oRoundObjects[index].oTarget)
				// There may be backing to destroy!
				ENTITY_INDEX oBacking = GET_ENTITY_ATTACHED_TO(sRoundData.oRoundObjects[index].oTarget)
				IF NOT IS_ENTITY_DEAD(oBacking)
					DELETE_ENTITY(oBacking)
				ENDIF
				DELETE_ENTITY(sRoundData.oRoundObjects[index].oTarget)
			ENDIF
		ENDIF
		
		// May be a secondary target.
		IF DOES_ENTITY_EXIST(sRoundData.oRoundObjects[index].oTarget2)
			IF NOT IS_ENTITY_DEAD(sRoundData.oRoundObjects[index].oTarget2)
				DELETE_ENTITY(sRoundData.oRoundObjects[index].oTarget2)
			ENDIF
		ENDIF
		
		// May be a misc.
		IF DOES_ENTITY_EXIST(sRoundData.oRoundObjects[index].oMisc)
			IF NOT IS_ENTITY_DEAD(sRoundData.oRoundObjects[index].oMisc)
				DELETE_ENTITY(sRoundData.oRoundObjects[index].oMisc)
			ENDIF
		ENDIF
	ENDFOR
	
	// Rounds may have some misc items, like grids.
	IF NOT IS_ENTITY_DEAD(sRoundData.oMisc)
		DELETE_ENTITY(sRoundData.oMisc)
	ENDIF
	IF NOT IS_ENTITY_DEAD(sRoundData.oMisc2)
		DELETE_ENTITY(sRoundData.oMisc2)
	ENDIF
	
	// Destroy invisible holder.
	IF NOT IS_ENTITY_DEAD(sRoundData.oAnchorObject)
		DELETE_ENTITY(sRoundData.oAnchorObject)
	ENDIF
ENDPROC





