USING "commands_stats.sch"
USING "net_prints.sch"
USING "rangemp_round.sch"
USING "socialclub_leaderboard.sch"
USING "net_leaderboards.sch"

FUNC BOOL RANGE_MP_SHOULD_SCLB_SHOW_PROFILE_BUTTON(SC_LEADERBOARD_CONTROL_STRUCT & rangeLBD)
	RETURN SHOULD_PROFILE_BUTTON_BE_AVAILABLE_FOR_LEADERBOARD(rangeLBD)
ENDFUNC

/// PURPOSE:
///    Wrapper for REQUEST_LEADERBOARD_CAM
PROC REQUEST_RANGE_LEADERBOARD_CAM()
	REQUEST_LEADERBOARD_CAM()
ENDPROC

/// PURPOSE:
///    Wrapper for IS_LEADERBOARD_CAM_READY
/// RETURNS:
///    
FUNC BOOL IS_RANGE_LEADERBOARD_CAM_READY()
	RETURN IS_LEADERBOARD_CAM_READY(TRUE)
ENDFUNC

/// PURPOSE:
///    Wrapper for MPglobals.g_bRequestLBCam which is set in REQUEST_LEADERBOARD_CAM
/// RETURNS:
///    
FUNC BOOL HAS_RANGE_LEADERBOARD_BEEN_REQUESTED()
	RETURN g_TransitionSessionNonResetVars.sPostMissionCleanupData.bRequestLeaderBoardCam
ENDFUNC

/// PURPOSE:
///    
/// PARAMS:
///    eChallenge - 
///    iMatches - 1 for a win, 0 for a match loss, -1 for no change.
///    iShotsFired - 
///    iShotsHit - 
///    eWeaponUsed - 
PROC WRITE_RANGE_MP_SCLB_DATA(CHALLENGE_INDEX eChallenge, INT iMatches, INT iShotsFired, INT iShotsHit, WEAPON_TYPE eWeaponUsed)
	TEXT_LABEL_23 sIdentifier[2]
	TEXT_LABEL_31 categoryName[2]
	categoryName[0] = "Type"
	IF eChallenge = CI_GRID
		sIdentifier[0] = "GRID"
	ELIF eChallenge = CI_RANDOM
		sIdentifier[0] = "RANDOM"
	ELIF eChallenge = CI_COVERED
		sIdentifier[0] = "COVERED"
	ENDIF
	categoryName[1] = "WeaponId"
	SWITCH eWeaponUsed
		CASE WEAPONTYPE_APPISTOL
		CASE WEAPONTYPE_COMBATPISTOL
		CASE WEAPONTYPE_PISTOL
		CASE WEAPONTYPE_DLC_PISTOL50
		CASE WEAPONTYPE_DLC_SNSPISTOL
		CASE WEAPONTYPE_DLC_HEAVYPISTOL
		CASE WEAPONTYPE_DLC_VINTAGEPISTOL
			sIdentifier[1] = "Pistols"
		BREAK
		CASE WEAPONTYPE_SMG
		CASE WEAPONTYPE_DLC_ASSAULTSMG
		CASE WEAPONTYPE_MICROSMG
			sIdentifier[1] = "SMGs"
		BREAK
		CASE WEAPONTYPE_ASSAULTRIFLE
		CASE WEAPONTYPE_ADVANCEDRIFLE
		CASE WEAPONTYPE_CARBINERIFLE
		CASE WEAPONTYPE_DLC_SPECIALCARBINE
		CASE WEAPONTYPE_DLC_HEAVYRIFLE
		CASE WEAPONTYPE_DLC_BULLPUPRIFLE
			sIdentifier[1] = "AssaultRifles"
		BREAK
		CASE WEAPONTYPE_PUMPSHOTGUN
		CASE WEAPONTYPE_SAWNOFFSHOTGUN
		CASE WEAPONTYPE_ASSAULTSHOTGUN
		CASE WEAPONTYPE_DLC_BULLPUPSHOTGUN
			sIdentifier[1] = "Shotguns"
		BREAK
		CASE WEAPONTYPE_MG
		CASE WEAPONTYPE_COMBATMG
		CASE WEAPONTYPE_DLC_ASSAULTMG
		CASE WEAPONTYPE_DLC_GUSENBERG
			sIdentifier[1] = "LMGs"
		BREAK
		CASE WEAPONTYPE_MINIGUN
			sIdentifier[1] = "Heavies"
		BREAK
	ENDSWITCH
	
	IF INIT_LEADERBOARD_WRITE(LEADERBOARD_MINI_GAMES_MP_SRANGE, sIdentifier, categoryName, 2, DEFAULT, TRUE)
		CDEBUG1LN(DEBUG_SHOOTRANGE, "~ WRK_PRITE_RANGE_MP_SCLB_DATA - Writing to leaderboard! ~")
		WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(ENUM_TO_INT(LEADERBOARD_MINI_GAMES_MP_SRANGE), LB_INPUT_COL_TOTAL_SHOTS_FIRED, iShotsFired, 0)
		WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(ENUM_TO_INT(LEADERBOARD_MINI_GAMES_MP_SRANGE), LB_INPUT_COL_TOTAL_SHOTS_HIT, iShotsHit, 0)
		CDEBUG1LN(DEBUG_SHOOTRANGE, "~ WRK_PRITE_RANGE_MP_SCLB_DATA - Writing to LB_INPUT_COL_TOTAL_SHOTS_FIRED, iShotsFired=", iShotsFired)
		CDEBUG1LN(DEBUG_SHOOTRANGE, "~ WRK_PRITE_RANGE_MP_SCLB_DATA - Writing to LB_INPUT_COL_TOTAL_SHOTS_HIT, iShotsHit=", iShotsHit)
		IF (iMatches = -1)
			WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(ENUM_TO_INT(LEADERBOARD_MINI_GAMES_MP_SRANGE), LB_INPUT_COL_NUM_MATCHES, 0, 0)
			WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(ENUM_TO_INT(LEADERBOARD_MINI_GAMES_MP_SRANGE), LB_INPUT_COL_NUM_MATCHES_LOST, 0, 0)
			CDEBUG1LN(DEBUG_SHOOTRANGE, "~ WRK_PRITE_RANGE_MP_SCLB_DATA - iMatches = -1, Writing to LB_INPUT_COL_NUM_MATCHES, 0")
			CDEBUG1LN(DEBUG_SHOOTRANGE, "~ WRK_PRITE_RANGE_MP_SCLB_DATA - iMatches = -1, Writing to LB_INPUT_COL_NUM_MATCHES_LOST, 0")
		ELSE
			WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(ENUM_TO_INT(LEADERBOARD_MINI_GAMES_MP_SRANGE), LB_INPUT_COL_NUM_MATCHES, iMatches, 0)
			WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(ENUM_TO_INT(LEADERBOARD_MINI_GAMES_MP_SRANGE), LB_INPUT_COL_NUM_MATCHES_LOST, 1 - iMatches, 0)
			CDEBUG1LN(DEBUG_SHOOTRANGE, "~ WRK_PRITE_RANGE_MP_SCLB_DATA - Writing to LB_INPUT_COL_NUM_MATCHES, iMatches=", iMatches)
			CDEBUG1LN(DEBUG_SHOOTRANGE, "~ WRK_PRITE_RANGE_MP_SCLB_DATA - Writing to LB_INPUT_COL_NUM_MATCHES_LOST, 1 - iMatches=", 1 - iMatches)
		ENDIF
	ELSE
		CDEBUG1LN(DEBUG_SHOOTRANGE, "~ WRITE_RANGE_MP_SCLB_DATA - Couldn't write to leaderboard! ~")
	ENDIF
ENDPROC

PROC RANGE_READ_SOCIAL_CLUB_LEADERBOARD(SC_LEADERBOARD_CONTROL_STRUCT & rangeLBD, CHALLENGE_INDEX eChallenge, RANGE_WEAPON_CATEGORY eWeaponCat)
	SC_LEADERBOARD_CACHE_CLEAR_ALL()
	CDEBUG1LN(DEBUG_SHOOTRANGE, "RANGE_READ_SOCIAL_CLUB_LEADERBOARD - ", eChallenge, " :: ", eWeaponCat)
	
	INT iSubType
	IF eChallenge = CI_RANDOM
		iSubType = 0
	ELIF eChallenge = CI_GRID
		iSubType = 1
	ELIF eChallenge = CI_COVERED
		iSubType = 2
	ELSE
		CDEBUG2LN(DEBUG_SHOOTRANGE, "iSubType: ", iSubType)
		SCRIPT_ASSERT("RANGE_LOAD_SOCIAL_CLUB_LEADERBOARD :: INVALID ENUM")
	ENDIF
	INT iWeaponCat	// passed into iLaps parameter
	SWITCH eWeaponCat
		CASE WEAPCAT_PISTOL		iWeaponCat = 0		BREAK
		CASE WEAPCAT_SMG		iWeaponCat = 1		BREAK
		CASE WEAPCAT_AR			iWeaponCat = 2		BREAK
		CASE WEAPCAT_SHOTGUN	iWeaponCat = 3		BREAK
		CASE WEAPCAT_LIGHT_MG	iWeaponCat = 4		BREAK
		CASE WEAPCAT_MINIGUN	iWeaponCat = 5		BREAK
	ENDSWITCH
	
	CDEBUG1LN(DEBUG_SHOOTRANGE, "SETUP_SOCIAL_CLUB_LEADERBOARD_READ_DATA - Running actual read.")
	SETUP_SOCIAL_CLUB_LEADERBOARD_READ_DATA(rangeLBD, FMMC_TYPE_MG_SHOOTING_RANGE, "", "", iSubType, iWeaponCat, FALSE, TRUE)
	CDEBUG1LN(DEBUG_SHOOTRANGE, "Post SETUP_SOCIAL_CLUB_LEADERBOARD_READ_DATA - ID: ", rangeLBD.ReadDataStruct.m_LeaderboardId)
ENDPROC

PROC RANGE_DISPLAY_SOCIAL_CLUB_LEADERBOARD(SCALEFORM_INDEX &uiLeaderboard, SC_LEADERBOARD_CONTROL_STRUCT & rangeLBD)
	CDEBUG1LN(DEBUG_SHOOTRANGE, "Attempting to draw: ", rangeLBD.ReadDataStruct.m_LeaderboardId)
	
	SET_CORONA_UNDER_HUD_THIS_FRAME()
	DRAW_SC_SCALEFORM_LEADERBOARD(uiLeaderboard, rangeLBD)
ENDPROC

PROC RANGE_CLEANUP_SOCIAL_CLUB_LEADERBOARD(SC_LEADERBOARD_CONTROL_STRUCT & rangeLBD)
	CDEBUG1LN(DEBUG_SHOOTRANGE, "~ CLEANUP_SOCIAL_CLUB_LEADERBOARD! ~")
	CDEBUG1LN(DEBUG_SHOOTRANGE, "Attempting to cleanup: ", rangeLBD.ReadDataStruct.m_LeaderboardId)
	CLEANUP_SOCIAL_CLUB_LEADERBOARD(rangeLBD)
	SC_LEADERBOARD_CACHE_CLEAR_ALL()
ENDPROC

