// RangeMP_Targets.sch
CONST_FLOAT TARGET_FLIP_TIME			0.35
CONST_INT	MAX_TARGETS					28
CONST_FLOAT	GRID_RAISE_TIME				2.0

CONST_FLOAT GRID_START_Z				26.595
CONST_FLOAT GRID_END_Z					30.595

CONST_FLOAT	CONST_ROT_TIME_ENTER		0.6		// How long it should take to enter
CONST_FLOAT	CONST_ROT_TIME_EXIT			0.5		// How long it should take to exit
CONST_FLOAT	CONST_COVERED_ROTATE_TIME	0.4		// How long covered round targets take to flip. Made this shot to make the round a little more intense.

CONST_INT 	CI_RANDOM_PTS_PER_TGT		100
CONST_INT	CI_RANDOM_PTS_LOST			-150

STRUCT RangeTarget
	ENTITY_INDEX	oMisc
	ENTITY_INDEX	oTarget, oTarget2
	INT 			iFlags
	
	FLOAT 			fFlipTime
	GRID_INDEX		eGridLoc		// Used for random targets round
ENDSTRUCT

ENUM TARGET_FLAGS
	TARG_F_IS_RED 				= BIT0,
	TARG_F_IS_BLUE 				= BIT1,
	
	TARG_F_FLIPPING_TO_BLUE 	= BIT2,
	TARG_F_FLIPPING_TO_RED 		= BIT3,

	TARG_F_ROTATING_OUT			= BIT5,
	TARG_F_ROTATING_IN			= BIT6,
	
	TARG_F_HIT_BY_PROPER_COLOR	= BIT7,		// Used by RANDOM CHALLENGE. If the target was red, did a red player hit it?
	TARG_F_COUNT_ME				= BIT8,
	
	TARG_F_BLOCKED				= BIT9,		// Used by COVERED CHALLENGE. If a target is marked as "BLOCKED" then it is simply facing away.
	TARG_F_FLIP_OUT				= BIT10,
	TARG_F_DONE					= BIT11
ENDENUM

FUNC STRING GET_STRING_FROM_RANGE_TARGET_FLAGS(TARGET_FLAGS eFlag)
	SWITCH eFlag
		CASE TARG_F_IS_RED					RETURN "TARG_F_IS_RED"
		CASE TARG_F_IS_BLUE					RETURN "TARG_F_IS_BLUE"
		CASE TARG_F_FLIPPING_TO_BLUE		RETURN "TARG_F_FLIPPING_TO_BLUE"
		CASE TARG_F_FLIPPING_TO_RED			RETURN "TARG_F_FLIPPING_TO_RED"
		CASE TARG_F_ROTATING_OUT			RETURN "TARG_F_ROTATING_OUT"
		CASE TARG_F_ROTATING_IN				RETURN "TARG_F_ROTATING_IN"
		CASE TARG_F_HIT_BY_PROPER_COLOR		RETURN "TARG_F_HIT_BY_PROPER_COLOR"
		CASE TARG_F_COUNT_ME				RETURN "TARG_F_COUNT_ME"
		CASE TARG_F_BLOCKED					RETURN "TARG_F_BLOCKED"
		CASE TARG_F_FLIP_OUT				RETURN "TARG_F_FLIP_OUT"
		CASE TARG_F_DONE					RETURN "TARG_F_DONE"
	ENDSWITCH
	RETURN "Unknown TARGET_FLAGS flag"
ENDFUNC

ENUM CI_GRID_OBJECT_INDEXES
	CI_GRID_A1 = 0,
	CI_GRID_A2,
	CI_GRID_A3,
	CI_GRID_A4,
	CI_GRID_A5,
	CI_GRID_A6,
	CI_GRID_A7,
	
	CI_GRID_B1,
	CI_GRID_B2,
	CI_GRID_B3,
	CI_GRID_B4,
	CI_GRID_B5,
	CI_GRID_B6,
	CI_GRID_B7,
	
	CI_GRID_C1,
	CI_GRID_C2,
	CI_GRID_C3,
	CI_GRID_C4,
	CI_GRID_C5,
	CI_GRID_C6,
	CI_GRID_C7,
	
	CI_GRID_D1,
	CI_GRID_D2,
	CI_GRID_D3,
	CI_GRID_D4,
	CI_GRID_D5,
	CI_GRID_D6,
	CI_GRID_D7,
	
	CI_MAX_GRID_OBJECTS
ENDENUM

CONST_INT	COVERED_ROUND_FIELD_BREAK	20

ENUM CI_COVERED_OBJECT_INDEXES
	// These are the 4 targets in the middle that control the other targets' visibility.
	CI_COVERED_CONTROL_1 = 0,
	CI_COVERED_CONTROL_2,
	CI_COVERED_CONTROL_3,
	CI_COVERED_CONTROL_4,
	
	// The targets that are hidden or uncovered.
	// R_C -- LEFT SIDE.
	CI_GOAL_1_1,
	CI_GOAL_1_2,
	CI_GOAL_1_3,
	CI_GOAL_1_4,
	CI_GOAL_1_5,
	CI_GOAL_1_6,
	CI_GOAL_1_7,
	CI_GOAL_1_8,
	CI_GOAL_2_1,
	CI_GOAL_2_2,
	CI_GOAL_2_3,
	CI_GOAL_2_4,
	CI_GOAL_2_5,
	CI_GOAL_2_6,
	CI_GOAL_2_7,
	CI_GOAL_2_8,
	CI_GOAL_3_1,	// 20
	CI_GOAL_3_2,
	CI_GOAL_3_3,
	CI_GOAL_3_4,
	CI_GOAL_3_5,
	CI_GOAL_3_6,
	CI_GOAL_3_7,
	CI_GOAL_3_8,
	CI_GOAL_4_1,
	CI_GOAL_4_2,
	CI_GOAL_4_3,
	CI_GOAL_4_4,
	CI_GOAL_4_5,
	CI_GOAL_4_6,
	CI_GOAL_4_7,
	CI_GOAL_4_8,
	
	CI_COVERED_END_CONTROL = CI_COVERED_CONTROL_4,
	CI_GOAL_END = CI_GOAL_4_8
ENDENUM


//*******************************************************************************************************************
//***************************************** ACCESSORS AND MUTATORS **************************************************
//*******************************************************************************************************************

/// PURPOSE:
///    Checks to see if a flag is set on a target.
/// RETURNS:
///    TRUE if the flag is set.
FUNC BOOL CHECK_TARGET_FLAG(RangeTarget & oObjToCheck, TARGET_FLAGS eFlagToCheck)
	RETURN IS_BITMASK_AS_ENUM_SET(oObjToCheck.iFlags, eFlagToCheck)
ENDFUNC

/// PURPOSE:
///    Checks to see if a flag is set on a target.
/// RETURNS:
///    TRUE if the flag is set.
PROC CLEAR_TARGET_FLAG(RangeTarget & oObj, TARGET_FLAGS eFlagToClear)
	CLEAR_BITMASK_AS_ENUM(oObj.iFlags, eFlagToClear)
ENDPROC

/// PURPOSE:
///    Checks to see if a flag is set on a target.
/// RETURNS:
///    TRUE if the flag is set.
PROC SET_TARGET_FLAG(RangeTarget & oObj, TARGET_FLAGS eFlagToSet)
	CDEBUG2LN(DEBUG_SHOOTRANGE, "SET_TARGET_FLAG :: setting ", GET_STRING_FROM_RANGE_TARGET_FLAGS(eFlagToSet), " on ", NATIVE_TO_INT(oObj.oTarget))
	SET_BITMASK_AS_ENUM(oObj.iFlags, eFlagToSet)
ENDPROC

