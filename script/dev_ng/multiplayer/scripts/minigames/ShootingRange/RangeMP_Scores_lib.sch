// RangeMP_Scores_lib.sch
USING "RangeMP_Scores.sch"
USING "minigames_helpers.sch"

/// PURPOSE:
///    Sets up all the basics for the floating score queue.
PROC RANGE_MP_SETUP_FLOATING_SCORE_QUEUE(RANGE_MP_FLOATING_SCORE & floatingScores[])
	INT index
	REPEAT RANGE_MP_MAX_FS index
		floatingScores[index].fPosX = 0.5 + 0.00234 // middle of screen plus small offset for centering
		floatingScores[index].fPosY = PIXEL_Y_TO_FLOAT(300)
		floatingScores[index].iAlpha = 255
		floatingScores[index].iPtValue = 0
		floatingScores[index].fFrameTime = -1
		floatingScores[index].bActive = FALSE
	ENDREPEAT	
ENDPROC

/// PURPOSE:
///    Sorts the floating score queue based off of time spent alive.
PROC RANGE_MP_SORT_FLOATING_SCORES(RANGE_MP_FLOATING_SCORE & floatingScores[])
	INT iI, iJ
	RANGE_MP_FLOATING_SCORE tempScore
	
	iI = 1
	WHILE  iI < RANGE_MP_MAX_FS
		tempScore = floatingScores[iI]
		
		// To change Sort ordering
		// > for Big to Small
		// < for Small to Big
		iJ = iI
		WHILE (iJ > 0) AND (tempScore.fFrameTime < floatingScores[iJ-1].fFrameTime)
			floatingScores[iJ] = floatingScores[iJ-1]
			iJ--
		ENDWHILE
		floatingScores[iJ] = tempScore
		
		iI++
	ENDWHILE
ENDPROC

/// PURPOSE:
///    Finds the next open SPT slot.
FUNC INT RANGE_MP_GET_NEXT_FLOATING_SCORE_SLOT(RANGE_MP_FLOATING_SCORE & floatingScores[])
	INT index
	
	// Sort first.
	RANGE_MP_SORT_FLOATING_SCORES(floatingScores)
	
	// First, go through all scores, and make sure we have an active spot.
	REPEAT RANGE_MP_MAX_FS index
		IF NOT (floatingScores[index].bActive)
			RETURN index
		ENDIF
	ENDREPEAT	
	
	RETURN RANGE_MP_MAX_FS - 1
ENDFUNC

/// PURPOSE:
///    Adds a floating score to our array.
PROC RANGE_MP_ADD_FLOATING_SCORE(INT iValue, HUD_COLOURS eColor, RANGE_MP_FLOATING_SCORE & floatingScores[])
	// Get our next slot...
	INT iSlot = RANGE_MP_GET_NEXT_FLOATING_SCORE_SLOT(floatingScores)
	
	// Set the score up.
	floatingScores[iSlot].fPosY = PIXEL_Y_TO_FLOAT(300)
	floatingScores[iSlot].iAlpha = 255
	floatingScores[iSlot].iPtValue = iValue
	floatingScores[iSlot].bActive = TRUE
	floatingScores[iSlot].fFrameTime = 0
	floatingScores[iSlot].color = eColor
	
	// Lastly, sort.
	RANGE_MP_SORT_FLOATING_SCORES(floatingScores)
ENDPROC

/// PURPOSE:
///    Makes a floating score move up the screen.
PROC RANGE_MP_SCROLL_FLOATING_SCORE(RANGE_MP_FLOATING_SCORE & singularScore)
	FLOAT fStart,fEnd
	FLOAT fInterpTime
	
	FLOAT fAlphaStartFadeOut = 0.5
	
	// Increment our frame count
	singularScore.fFrameTime += GET_FRAME_TIME()
	
	// Find out how much we should be flipping
	fInterpTime = singularScore.fFrameTime / RANGE_MP_SCORE_LIFETIME

	
	// Keep going until we reach 1.0 (means we are at our destination)
	IF fInterpTime < 1.0
		// Position
		fStart = PIXEL_Y_TO_FLOAT(300)
		fEnd = PIXEL_Y_TO_FLOAT(260)
		
		// Quad OUT
		singularScore.fPosY = -(fEnd-fStart)*(fInterpTime/RANGE_MP_SCORE_LIFETIME)*(fInterpTime-2)+fStart
		
		// Alpha
		IF fInterpTime < fAlphaStartFadeOut
			singularScore.iAlpha = 255
		ELSE
			singularScore.iAlpha = 255 - ROUND((fInterpTime - fAlphaStartFadeOut)/fAlphaStartFadeOut * 255)
		ENDIF
		
		singularScore.bActive = TRUE
	ELSE			
		// Dissapear!
		singularScore.iAlpha = 0
		singularScore.bActive = FALSE
		singularScore.fFrameTime = -1
	ENDIF
ENDPROC

/// PURPOSE:
///    Updates all floating scores in the floating score queue.
PROC RANGE_MP_UPDATE_FLOATING_SCORES(RANGE_MP_FLOATING_SCORE & floatingScores[])
	INT index, iActiveCount
	BOOL bNeedSort = FALSE
		
	// Scroll the text up after its been displayed
	REPEAT RANGE_MP_MAX_FS index
		// Is this active
		IF floatingScores[index].bActive
			RANGE_MP_SCROLL_FLOATING_SCORE(floatingScores[index])
			iActiveCount++
			
			// Sort SPTs when SPT becomes inactive
			IF (NOT floatingScores[index].bActive)
				bNeedSort = TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF iActiveCount > 0
		IF bNeedSort
			RANGE_MP_SORT_FLOATING_SCORES(floatingScores)
		ENDIF
		
		// Reposition based on latest entry
		FOR index = 0 TO RANGE_MP_MAX_FS - 2
			IF (floatingScores[index].bActive) AND (floatingScores[index + 1].bActive) AND 
					(floatingScores[index+1].fPosY > floatingScores[index].fPosY - RANGE_MP_SCORE_SPACING)
				floatingScores[index+1].fPosY = floatingScores[index].fPosY - RANGE_MP_SCORE_SPACING
			ENDIF
		ENDFOR
	ELSE
		EXIT		
	ENDIF
ENDPROC

/// PURPOSE:
///    Draws all active scores in the floating score queue.
PROC RANGE_MP_DRAW_FLOATING_SCORES(RANGE_MP_FLOATING_SCORE & floatingScores[])
	INT index
	
	// First, update all of the text.
	RANGE_MP_UPDATE_FLOATING_SCORES(floatingScores)
	
	// This text will use an outline.
	SET_TEXT_OUTLINE()
	
	index = RANGE_MP_MAX_FS - 1
	WHILE (index >= 0)
		IF (floatingScores[index].bActive)
			SET_TEXT_OUTLINE()
			
			// Use a different string if these points are positive or negative.
			STRING sToUse = "NUMBER"
			IF (floatingScores[index].iPtValue > 0)
				sToUse = "SHR_SPT_POINTS"
			ENDIF
			
			DISPLAY_TEXT_LABEL_WITH_NUMBER(sToUse,
				floatingScores[index].fPosX - GET_STRING_WIDTH_WITH_NUMBER("SHR_SPT_POINTS", floatingScores[index].iPtValue) * RANGE_MP_SCORE_SCALE/2.0,
				floatingScores[index].fPosY - GET_RENDERED_CHARACTER_HEIGHT(RANGE_MP_SCORE_SCALE) * RANGE_MP_SCORE_SCALE/2.0,
				RANGE_MP_SCORE_SCALE, RANGE_MP_SCORE_SCALE,	1.0, 1.0,
				GetTextColor(floatingScores[index].color, floatingScores[index].iAlpha),
				floatingScores[index].iPtValue,
				0,
				0,
				FONT_STANDARD)
		ENDIF
		index--
	ENDWHILE
ENDPROC




