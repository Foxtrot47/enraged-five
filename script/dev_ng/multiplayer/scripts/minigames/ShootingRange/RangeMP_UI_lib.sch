// RangeMP_UI_lib.sch
USING "RangeMP_UI.sch"
USING "RangeMP_Player.sch"
USING "RangeMP_Round.sch"
USING "RangeMP_GridRound_lib.sch"
USING "RangeMP_RandomRound_lib.sch"
USING "RangeMP_CoveredRound_lib.sch"
USING "RangeMP_Server.sch"
USING "shared_hud_displays.sch"
USING "RangeMP_MainMenu_lib.sch"
USING "RangeMP_Scorecard_lib.sch"

USING "script_usecontext.sch"
USING "MPHud_Loading.sch"
USING "menu_cursor_public.sch"

PROC RANGE_PRINT_TO_SCREEN( STRING sDisplay, FLOAT fDisplayAtY = 0.2 )
	DRAW_DEBUG_TEXT_2D( sDisplay, (<< 0.05, fDisplayAtY, 0.0 >>) )
ENDPROC

PROC HIDE_UI_DURING_RANGE()
	RANGE_PRINT_TO_SCREEN( "Hiding UI", 0.9 )
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_MP_CASH)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_FEED)
ENDPROC

#IF IS_DEBUG_BUILD
	PROC CLIENT_SERVER_DEBUG_DISPLAY(INT iClientID, RangeMP_ServerBD & sServerBD, RangeMP_ParticipantInfo & sParticipantInfo )
		// If we're in debug, print which client we are:
		TEXT_LABEL_63 debugTxt = "Client #"
		debugTxt += iClientID
		SET_TEXT_FONT(FONT_STANDARD)
		SET_TEXT_SCALE(0.3, 0.5)
		IF (iClientID = sParticipantInfo.iShooters[0])
			SET_TEXT_COLOUR(0, 0, 255, 255)
		ELSE
			SET_TEXT_COLOUR(255, 0, 0, 255)
		ENDIF
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.05, 0.1, "STRING", debugTxt)
		
		IF (iClientID = SERVER_GET_PLAYER_IN_CONTROL(sServerBD))
			SET_TEXT_SCALE(0.3, 0.5)
			SET_TEXT_COLOUR(255, 255, 255, 255)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.05, 0.15, "STRING", "In Control")
		ENDIF
	ENDPROC
#ENDIF

/// PURPOSE:
///    Triggers the splash screen for the player passing or failing a range round.
PROC SET_RANGE_SPLASH(SCRIPT_SHARD_BIG_MESSAGE &siStruct, STRING sMsg, STRING sChallenge, HUD_COLOURS eEndFlash = HUD_COLOUR_WHITE)
	STRING sMethodName = GET_STRING_FROM_SHARD_BIG_MESSAGE_TYPE(SHARD_MESSAGE_CENTERED)
	siStruct.eEndFlash = eEndFlash
	
	BEGIN_SCALEFORM_MOVIE_METHOD(siStruct.siMovie, "RESET_MOVIE")
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(siStruct.siMovie, sMethodName)
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(sMsg)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING(sChallenge)
		END_TEXT_COMMAND_SCALEFORM_STRING()
	END_SCALEFORM_MOVIE_METHOD()
	
	// Store the duration.
	siStruct.iDuration = 5000
ENDPROC

FUNC BOOL UPDATE_RANGE_SPLASH(RangeMP_MenuData & sMenuInfo)
	// Check our timer.
	FLOAT fTimerVal = 0.0
	IF NOT IS_TIMER_STARTED(sMenuInfo.uiShardBM.movieTimer)
		RESTART_TIMER_NOW(sMenuInfo.uiShardBM.movieTimer)
		PLAY_SOUND_FRONTEND(-1, "SHOOTING_RANGE_ROUND_OVER", "HUD_AWARDS")
	ELSE
		fTimerVal = GET_TIMER_IN_SECONDS(sMenuInfo.uiShardBM.movieTimer)
	ENDIF
	
	// Draw victory message. When it's done, move on.
	IF NOT sMenuInfo.bTransitionOut
		IF fTimerVal > 3.25
			END_SHARD_BIG_MESSAGE(sMenuInfo.uiShardBM)
			sMenuInfo.bTransitionOut = TRUE
		ENDIF
	ENDIF
	
	// Draw.
	DRAW_SCALEFORM_MOVIE_FULLSCREEN(sMenuInfo.uiShardBM.siMovie, 255, 255, 255, 255)
	
	// Are we done?
	IF fTimerVal > 4.0
		CANCEL_TIMER(sMenuInfo.uiShardBM.movieTimer)
		sMenuInfo.bTransitionUp = FALSE
		sMenuInfo.bTransitionOut = FALSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC STRING RANGE_MP_GetCurChallengeName(CHALLENGE_INDEX eChallengeType)
	SWITCH (eChallengeType)
		CASE CI_GRID		RETURN "SHR_MP_CHALL2"		BREAK
		CASE CI_RANDOM		RETURN "SHR_MP_CHALL1"		BREAK
		CASE CI_COVERED		RETURN "SHR_MP_CHALL3"		BREAK
	ENDSWITCH
	RETURN "CHAL STRING MISSING"
ENDFUNC


PROC RANGE_MP_SetupElement(RangeMP_Menu & theMenu)
	theMenu.iNumElements += 1
	DEBUG_MESSAGE("Element added!")
ENDPROC

/// PURPOSE:
///    When we slect a category, this fills in the proper weapon data.
PROC RANGE_MP_InitWeaponsMenu(RangeMP_PlayerBD & sPlayerBD[], RangeMP_Menu & sWeaponsMenu, RANGE_WEAPON_CATEGORY weaponCat, BOOL bBlank=TRUE)
	// Wipe us.
	RangeMP_Menu blankMenu
	INT iWeaponCounter = 0
	
	IF bBlank
		sWeaponsMenu = blankMenu
	ENDIF
	
	IF (weaponCat = WEAPCAT_PISTOL)
		sWeaponsMenu.iNumElements = 3
		
		sWeaponsMenu.eWeapons[ iWeaponCounter ] = WEAPONTYPE_PISTOL
		iWeaponCounter++
		sWeaponsMenu.eWeapons[ iWeaponCounter ] = WEAPONTYPE_COMBATPISTOL
		iWeaponCounter++
		sWeaponsMenu.eWeapons[ iWeaponCounter ] = WEAPONTYPE_APPISTOL
		iWeaponCounter++
		
		IF IS_WEAPON_AVAILABLE_FOR_GAME( WEAPONTYPE_DLC_PISTOL50 )
		#IF IS_DEBUG_BUILD OR DEBUG_ALL_DLC #ENDIF
			sWeaponsMenu.eWeapons[ iWeaponCounter ] = WEAPONTYPE_DLC_PISTOL50
			iWeaponCounter++
			sWeaponsMenu.iNumElements++
		ENDIF
		
		IF IS_WEAPON_AVAILABLE_FOR_GAME( WEAPONTYPE_DLC_SNSPISTOL )
		#IF IS_DEBUG_BUILD OR DEBUG_ALL_DLC #ENDIF
			sWeaponsMenu.eWeapons[ iWeaponCounter ] = WEAPONTYPE_DLC_SNSPISTOL
			iWeaponCounter++
			sWeaponsMenu.iNumElements++
		ENDIF
		
		IF IS_WEAPON_AVAILABLE_FOR_GAME( WEAPONTYPE_DLC_HEAVYPISTOL )
		#IF IS_DEBUG_BUILD OR DEBUG_ALL_DLC #ENDIF
			sWeaponsMenu.eWeapons[ iWeaponCounter ] = WEAPONTYPE_DLC_HEAVYPISTOL
			iWeaponCounter++
			sWeaponsMenu.iNumElements++
		ENDIF
		
		IF IS_WEAPON_AVAILABLE_FOR_GAME( WEAPONTYPE_DLC_VINTAGEPISTOL )
		#IF IS_DEBUG_BUILD OR DEBUG_ALL_DLC #ENDIF
			sWeaponsMenu.eWeapons[ iWeaponCounter ] = WEAPONTYPE_DLC_VINTAGEPISTOL
			iWeaponCounter++
			sWeaponsMenu.iNumElements++
		ENDIF
		
	ELIF (weaponCat = WEAPCAT_SMG)
		sWeaponsMenu.iNumElements = 2
		
		sWeaponsMenu.eWeapons[ iWeaponCounter ] = WEAPONTYPE_MICROSMG
		iWeaponCounter++
		sWeaponsMenu.eWeapons[ iWeaponCounter ] = WEAPONTYPE_SMG
		iWeaponCounter++
		
		IF IS_WEAPON_AVAILABLE_FOR_GAME(WEAPONTYPE_DLC_ASSAULTSMG)
		#IF IS_DEBUG_BUILD OR DEBUG_ALL_DLC #ENDIF
			sWeaponsMenu.eWeapons[ iWeaponCounter ] = WEAPONTYPE_DLC_ASSAULTSMG
			iWeaponCounter++
			sWeaponsMenu.iNumElements++
		ENDIF
		
	ELIF (weaponCat = WEAPCAT_AR)
		sWeaponsMenu.iNumElements = 3
		
		sWeaponsMenu.eWeapons[ iWeaponCounter ] = WEAPONTYPE_ASSAULTRIFLE
		iWeaponCounter++
		sWeaponsMenu.eWeapons[ iWeaponCounter ] = WEAPONTYPE_CARBINERIFLE
		iWeaponCounter++
		sWeaponsMenu.eWeapons[ iWeaponCounter ] = WEAPONTYPE_ADVANCEDRIFLE
		iWeaponCounter++
		
		IF IS_WEAPON_AVAILABLE_FOR_GAME(WEAPONTYPE_DLC_HEAVYRIFLE)
		#IF IS_DEBUG_BUILD OR DEBUG_ALL_DLC #ENDIF
			sWeaponsMenu.eWeapons[ iWeaponCounter ] = WEAPONTYPE_DLC_HEAVYRIFLE
			iWeaponCounter++
			sWeaponsMenu.iNumElements++
		ENDIF
		
		IF IS_WEAPON_AVAILABLE_FOR_GAME(WEAPONTYPE_DLC_BULLPUPRIFLE)
		#IF IS_DEBUG_BUILD OR DEBUG_ALL_DLC #ENDIF
			sWeaponsMenu.eWeapons[ iWeaponCounter ] = WEAPONTYPE_DLC_BULLPUPRIFLE
			iWeaponCounter++
			sWeaponsMenu.iNumElements++
		ENDIF
		
		IF IS_WEAPON_AVAILABLE_FOR_GAME(WEAPONTYPE_DLC_SPECIALCARBINE)
		#IF IS_DEBUG_BUILD OR DEBUG_ALL_DLC #ENDIF
			sWeaponsMenu.eWeapons[ iWeaponCounter ] = WEAPONTYPE_DLC_SPECIALCARBINE
			iWeaponCounter++
			sWeaponsMenu.iNumElements++
		ENDIF
		
	ELIF (weaponCat = WEAPCAT_SHOTGUN)
		sWeaponsMenu.iNumElements = 3
		
		sWeaponsMenu.eWeapons[ iWeaponCounter ] = WEAPONTYPE_PUMPSHOTGUN
		iWeaponCounter++
		sWeaponsMenu.eWeapons[ iWeaponCounter ] = WEAPONTYPE_SAWNOFFSHOTGUN
		iWeaponCounter++
		sWeaponsMenu.eWeapons[ iWeaponCounter ] = WEAPONTYPE_ASSAULTSHOTGUN
		iWeaponCounter++
		
		IF IS_WEAPON_AVAILABLE_FOR_GAME(WEAPONTYPE_DLC_BULLPUPSHOTGUN)
		#IF IS_DEBUG_BUILD OR DEBUG_ALL_DLC #ENDIF
			sWeaponsMenu.eWeapons[ iWeaponCounter ] = WEAPONTYPE_DLC_BULLPUPSHOTGUN
			iWeaponCounter++
			sWeaponsMenu.iNumElements++
		ENDIF
	
	ELIF (weaponCat = WEAPCAT_LIGHT_MG)
		sWeaponsMenu.iNumElements = 2
		
		sWeaponsMenu.eWeapons[ iWeaponCounter ] = WEAPONTYPE_MG
		iWeaponCounter++
		sWeaponsMenu.eWeapons[ iWeaponCounter ] = WEAPONTYPE_COMBATMG
		iWeaponCounter++
		
		IF IS_WEAPON_AVAILABLE_FOR_GAME(WEAPONTYPE_DLC_ASSAULTMG)
		#IF IS_DEBUG_BUILD OR DEBUG_ALL_DLC #ENDIF
			sWeaponsMenu.eWeapons[ iWeaponCounter ] = WEAPONTYPE_DLC_ASSAULTMG
			iWeaponCounter++
			sWeaponsMenu.iNumElements++
		ENDIF
		
		IF RANGE_IS_ANY_CLIENT_FLAG_SET(sPlayerBD, RANGE_MP_F_GusenbergUnlocked, FALSE)
		#IF IS_DEBUG_BUILD OR DEBUG_ALL_DLC #ENDIF
			sWeaponsMenu.eWeapons[ iWeaponCounter ] = WEAPONTYPE_DLC_GUSENBERG
			iWeaponCounter++
			sWeaponsMenu.iNumElements++
		ENDIF
		
	ELIF (weaponCat = WEAPCAT_MINIGUN)
		sWeaponsMenu.iNumElements = 1
		
		sWeaponsMenu.eWeapons[ iWeaponCounter ] = WEAPONTYPE_MINIGUN
		iWeaponCounter++
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets up the menu elements for drawing and updating.
PROC RANGE_MP_InitMainMenu(RangeMP_MenuData & sAllMenus, INT iClientID, RangeMP_ServerBD & sServerBD, RangeMP_PlayerBD & sPlayerBD[])
	// Wipe the old help heys, and set up the new ones.
	REMOVE_MENU_HELP_KEYS()
	
	IF sServerBD.iClient0MatchWins < TO_FLOAT(GET_RANGE_SERVER_TOTAL_MATCHES(sServerBD)) / 2.0
	AND sServerBD.iClient1MatchWins < TO_FLOAT(GET_RANGE_SERVER_TOTAL_MATCHES(sServerBD)) / 2.0
		
		IF IS_CLIENT_FLAG_SET(sPlayerBD[iClientID], RANGE_MP_F_IsSpectator)
			// Spectator gets QUIT, SPINNER
			RANGE_SET_BUSYSPINNER_STRING(sAllMenus, "SHR_SPEC_RND")
			RANGE_START_BUSYSPINNER(sAllMenus)
			ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL, "IB_QUIT")
		
		ELIF (iClientID = SERVER_GET_PLAYER_IN_CONTROL(sServerBD)) 
			// You're in control. There may be another player. Who knows.
			IF NOT IS_RANGE_ONE_PLAYER()
				RANGE_SET_BUSYSPINNER_STRING(sAllMenus, "U_CHOOSE")
				RANGE_START_BUSYSPINNER(sAllMenus)
			ENDIF
			ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_ACCEPT, "FE_HLP4")
			ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL, "IB_QUIT")
		ELSE
			// You're not in control, and you're not a spectator. You're the second player.
			RANGE_SET_BUSYSPINNER_STRING(sAllMenus, "O_CHOOSE")
			RANGE_START_BUSYSPINNER(sAllMenus)
			ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL, "IB_QUIT")
		ENDIF
		SET_MENU_HELP_KEYS_STACKED(FALSE)
		
	ENDIF
	
	CDEBUG2LN(DEBUG_SHOOTRANGE, "Setting up keys for RANGE_MP_InitMainMenu")
	
	sAllMenus.bSwappedMenu10Sec = FALSE

	RangeMP_Menu blankMenu
	
	// Just wipe weapons.
	sAllMenus.sMenu[RANGE_MP_MENU_WEAPONS] = blankMenu
	
	// INIT THE WEAPON MENU =====================================================================
	// Background
	sAllMenus.sMenu[RANGE_MP_MENU_CATEGORIES] = blankMenu
	
	// Add the weapon categories:
	sAllMenus.sMenu[RANGE_MP_MENU_CATEGORIES].iNumElements = 6
	
	// Tell which weapons menu element we're on.
	sAllMenus.sMenu[RANGE_MP_MENU_CATEGORIES].iCurElement = 0
	
	// INIT THE CHALLENGE MENU =================================================================
	// Background.
	sAllMenus.sMenu[RANGE_MP_MENU_CHALLENGES] = blankMenu
	
	// Add the rounds (1 for the time being).
	sAllMenus.sMenu[RANGE_MP_MENU_CHALLENGES].iNumElements = 3
	
	// Tell which challenge menu element we're on.
	sAllMenus.sMenu[RANGE_MP_MENU_CHALLENGES].iCurElement = -1
	
	// Initialize variables.
	sAllMenus.eActiveMenu = RANGE_MP_MENU_CATEGORIES
	sAllMenus.sMenu[RANGE_MP_MENU_CATEGORIES].iCurElement = 0
	sAllMenus.bAcceptStickInput = TRUE
	
	// Restart our tracking timer.
	RESTART_TIMER_NOW(sAllMenus.sMenuTimer)
	
	// Fix the placement values.
	INIT_RANGEMP_MAINMENU(sAllMenus)
ENDPROC

/// PURPOSE:
///    Returns the item value for the weapon selected by a cursor rather than a gamepad.
/// PARAMS:
///    iCursorValue - The menu cursor item currently selected
///    iCatValue - The weapon category currently selected.
/// RETURNS:
///    
FUNC INT GET_CURSOR_SELECTED_WEAPON_ITEM( INT iCursorValue, INT iCatValue, INT iNumWeapons )

	INT iMinValue = iCatValue + 1
	INT iMaxValue = iMinValue + (iNumWeapons - 1)
	
	IF iCursorValue < iMinValue
	OR iCursorValue > iMaxValue
		RETURN MENU_CURSOR_NO_ITEM
	ENDIF
	
	RETURN iCursorValue - iMinValue

ENDFUNC


/// PURPOSE:
///    Returns a weapon category from the categories outside of the currently selected weapon menu.
/// PARAMS:
///    iCursorValue - The menu cursor item currently selected
///    iCatValue - The weapon category currently selected.
///    iNumWeapons - The number of weapons in the category
/// RETURNS:
///    MENU_CURSOR_NO_ITEM if the cursor is inside the current category, or the category index if outside
///    
FUNC INT GET_CURSOR_SELECTED_WEAPON_CATEGORY_FROM_WEAPON_MENU( INT iCursorValue, INT iCatValue, INT iNumWeapons )

	// Below the category item
	
	IF iCursorValue > MENU_CURSOR_NO_ITEM
	AND iCursorValue < iCatValue
		RETURN iCursorValue
	ENDIF
	
	IF iCursorValue > iCatValue + iNumWeapons
		RETURN iCursorValue - iNumWeapons
	ENDIF
	
	IF iCursorValue = iCatValue
		RETURN -2
	ENDIF

	RETURN MENU_CURSOR_NO_ITEM

ENDFUNC



/// PURPOSE:
///    Processes all the menu input.
///    Should be run on host only.
///    sHostBroadcastData - 
/// RETURNS:
///    RETVAL_TRUE when done. 
FUNC THREE_STATE_RETVAL RANGE_MP_PROCESS_UI_INPUT_HOST(RangeMP_MenuData & sAllMenus, RangeMP_PlayerBD & sClientBD[])
	THREE_STATE_RETVAL eRetVal = RETVAL_NONE
	
	INT iCurMenuElement
	RANGE_WEAPON_CATEGORY eCatToSet
	
	////////
	///     PC Mouse support
	///
	BOOL bCursorWeaponCatSelect = FALSE
	BOOL bCursorWeaponSelect = 	  FALSE
	BOOL bCursorChallengeAccept = FALSE
	BOOL bCursorBack = 			  FALSE
	
	INT iCursorWeaponMenuItem	 = 		MENU_CURSOR_NO_ITEM
	INT iCursorChallengeMenuItem = 		MENU_CURSOR_NO_ITEM
	INT iCursorWeaponSelection = 		MENU_CURSOR_NO_ITEM
	INT iCursorWeaponCatOutsideItem = 	MENU_CURSOR_NO_ITEM
	
	INT iParticipantID = PARTICIPANT_ID_TO_INT()

	IF IS_PC_VERSION()
		IF IS_USING_CURSOR(FRONTEND_CONTROL)
		
			// Get the highlighted menu items
					
			iCursorWeaponMenuItem = GET_CURSOR_HIGHLIGHTED_ITEM_IN_CUSTOM_MENU( 0.201, 0.409, 0.198, 0.0375, 0.034, iNumMenuElements, 32 ) // Main Menu
			
			IF sAllMenus.eActiveMenu = RANGE_MP_MENU_CHALLENGES
				iCursorChallengeMenuItem = GET_CURSOR_HIGHLIGHTED_ITEM_IN_CUSTOM_MENU( 0.401, 0.408, 0.198, 0.111, 0.11, sAllMenus.sMenu[RANGE_MP_MENU_CHALLENGES].iNumElements, 32)  // Challenge Menu
			ENDIF
				
			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
				
				//////////////////////////////////////////////////////////////////////////////////////
				// Weapon category menu
				//////////////////////////////////////////////////////////////////////////////////////
		
				IF  sAllMenus.eActiveMenu  = RANGE_MP_MENU_CATEGORIES
								
					IF iCursorWeaponMenuItem > MENU_CURSOR_NO_ITEM
								
						// Select new item
						IF iCursorWeaponMenuItem != sAllMenus.sMenu[RANGE_MP_MENU_CATEGORIES].iCurElement
				
							RANGE_MP_SET_MENU_CURRENT_ELEMENT( sAllMenus.sMenu[RANGE_MP_MENU_CATEGORIES], iCursorWeaponMenuItem)
							
							CDEBUG2LN(DEBUG_SHOOTRANGE, "RANGEMENU MP CATEGORIS CURSOR SELECT - CUR POS = ", sAllMenus.sMenu[RANGE_MP_MENU_WEAPONS].iCurElement)
						
							PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", "HUD_FRONTEND_DEFAULT_SOUNDSET")
						ELSE
							// Confirm current item
							bCursorWeaponCatSelect = TRUE
							
						ENDIF
						
					ENDIF
					
					
				//////////////////////////////////////////////////////////////////////////////////////
				// Weapon selection menu
				//////////////////////////////////////////////////////////////////////////////////////
				ELIF sAllMenus.eActiveMenu  = RANGE_MP_MENU_WEAPONS
				OR (sAllMenus.eActiveMenu  = RANGE_MP_MENU_CHALLENGES AND iCursorChallengeMenuItem = MENU_CURSOR_NO_ITEM) // This is so we can select stuff from this menu when in the challenge menu too.
			
					sAllMenus.eActiveMenu  = RANGE_MP_MENU_WEAPONS	
					
					// Item is highlghted
					IF iCursorWeaponMenuItem > MENU_CURSOR_NO_ITEM
								
						// Get which category is highlighted.
						iCursorWeaponSelection = GET_CURSOR_SELECTED_WEAPON_ITEM(iCursorWeaponMenuItem, sAllMenus.sMenu[RANGE_MP_MENU_CATEGORIES].iCurElement, sAllMenus.sMenu[RANGE_MP_MENU_WEAPONS].iNumElements)
											
						//The player is selecting outside the category sub-menu, so try to handle this and select the new category
						IF iCursorWeaponSelection = MENU_CURSOR_NO_ITEM
							
							iCursorWeaponCatOutsideItem = GET_CURSOR_SELECTED_WEAPON_CATEGORY_FROM_WEAPON_MENU(iCursorWeaponMenuItem, sAllMenus.sMenu[RANGE_MP_MENU_CATEGORIES].iCurElement, sAllMenus.sMenu[RANGE_MP_MENU_WEAPONS].iNumElements)
							
							// Tab closes down when the player clicks on it.
							IF iCursorWeaponCatOutsideItem = -2 
							
								bCursorBack = TRUE
							
							// Player selects new weapon category.
							ELIF iCursorWeaponCatOutsideItem != MENU_CURSOR_NO_ITEM
							
									RANGE_MP_SET_MENU_CURRENT_ELEMENT( sAllMenus.sMenu[RANGE_MP_MENU_CATEGORIES], iCursorWeaponCatOutsideItem) // Switch weapon category
									RANGE_MP_SET_MENU_CURRENT_ELEMENT( sAllMenus.sMenu[RANGE_MP_MENU_WEAPONS], 0) // Select first item
									RANGE_MP_SET_MENU_CURRENT_ELEMENT( sAllMenus.sMenu[RANGE_MP_MENU_CHALLENGES], 0) // Select first challenge
									
									// Reset category items when using keyboard and mouse if a category has been clicked on while in the weapon menu.
									eCatToSet = INT_TO_ENUM(RANGE_WEAPON_CATEGORY, iCursorWeaponCatOutsideItem)
									SET_ROUND_WEAPON_CATEGORY(sClientBD[iParticipantID].sRoundDesc, eCatToSet)
									RANGE_MP_InitWeaponsMenu(sClientBD, sAllMenus.sMenu[RANGE_MP_MENU_WEAPONS], eCatToSet)
														
									bCursorWeaponCatSelect = TRUE
							ENDIF
						
						ELSE
						
							// Select weapon
						
							RANGE_MP_SET_MENU_CURRENT_ELEMENT( sAllMenus.sMenu[RANGE_MP_MENU_WEAPONS], iCursorWeaponSelection) // Set weapon
							RANGE_MP_SET_MENU_CURRENT_ELEMENT( sAllMenus.sMenu[RANGE_MP_MENU_CHALLENGES], 0) // Select first challenge
	
							CDEBUG2LN(DEBUG_SHOOTRANGE, "RANGEMENU MP WEAPONS CURSOR SELECT - CUR POS = ", sAllMenus.sMenu[RANGE_MP_MENU_WEAPONS].iCurElement)
						
							PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", "HUD_FRONTEND_DEFAULT_SOUNDSET")

							bCursorWeaponSelect = TRUE
							
						ENDIF
						

					ENDIF
					
					
				//////////////////////////////////////////////////////////////////////////////////////
				// Challenge menu
				//////////////////////////////////////////////////////////////////////////////////////
				
				ELIF sAllMenus.eActiveMenu = RANGE_MP_MENU_CHALLENGES
							
					IF iCursorChallengeMenuItem > MENU_CURSOR_NO_ITEM
					
						// Select new item
						IF iCursorChallengeMenuItem != sAllMenus.sMenu[RANGE_MP_MENU_CHALLENGES].iCurElement
						
							RANGE_MP_SET_MENU_CURRENT_ELEMENT( sAllMenus.sMenu[RANGE_MP_MENU_CHALLENGES], iCursorChallengeMenuItem)
						
							PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", "HUD_FRONTEND_DEFAULT_SOUNDSET")
					
						ELSE
							// Accept item
							bCursorChallengeAccept = TRUE
						ENDIF
						
					ENDIF
				ENDIF
			
			ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_CANCEL)
				bCursorBack = TRUE
			ENDIF
			
			SET_MOUSE_CURSOR_STYLE(MOUSE_CURSOR_STYLE_ARROW) // Normal arrow when over menus

		ENDIF
		
	ENDIF
	
	//////////////////////////////////////////////////////////////////////////////////////
	// End of mouse specific code
	//////////////////////////////////////////////////////////////////////////////////////
	
	// Process the input we have.
	IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
	OR bCursorWeaponCatSelect = TRUE
	OR bCursorWeaponSelect = TRUE
	OR bCursorChallengeAccept = TRUE
	
		// First off, we're forward a menu...
		REMOVE_MENU_HELP_KEYS()
		
		IF NOT IS_RANGE_ONE_PLAYER() AND NOT IS_CLIENT_FLAG_SET(sClientBD[iParticipantID], RANGE_MP_F_IsSpectator)
			RANGE_SET_BUSYSPINNER_STRING(sAllMenus, "U_CHOOSE")
			RANGE_START_BUSYSPINNER(sAllMenus)
		ENDIF
		ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_ACCEPT, "FE_HLP4")
		ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL, "FE_HLP3")
		SET_MENU_HELP_KEYS_STACKED(FALSE)
		
		// Just made a selection.
		// If we're on menu 0 (weap cats menu), go to weapons. If we're on 1 (weapons menu), go to challenges. If we're on 2, we're done.
		iCurMenuElement = RANGE_MP_GET_MENU_CURRENT_ELEMENT(sAllMenus.sMenu[sAllMenus.eActiveMenu])
		PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
		
		IF (sAllMenus.eActiveMenu = RANGE_MP_MENU_CATEGORIES)
			CDEBUG2LN(DEBUG_SHOOTRANGE, "This was our selected weapon category: ", iCurMenuElement)
			
			eCatToSet = INT_TO_ENUM(RANGE_WEAPON_CATEGORY, iCurMenuElement)
			SET_ROUND_WEAPON_CATEGORY(sClientBD[iParticipantID].sRoundDesc, eCatToSet)

			RANGE_MP_InitWeaponsMenu(sClientBD, sAllMenus.sMenu[RANGE_MP_MENU_WEAPONS], eCatToSet)
			sAllMenus.eActiveMenu = RANGE_MP_MENU_WEAPONS
			
			// This is needed so when using mouse the challenge menu gets selected automatically
			// when you click on a weapon.
				
			IF IS_PC_VERSION()
				IF bCursorWeaponCatSelect = TRUE
					sAllMenus.eActiveMenu = RANGE_MP_MENU_WEAPONS
				ELSE
					RETURN RETVAL_NONE
				ENDIF
			ELSE
													
				RETURN RETVAL_NONE
				
			ENDIF
			
		ENDIF
			
		IF (sAllMenus.eActiveMenu = RANGE_MP_MENU_WEAPONS)
			CDEBUG2LN(DEBUG_SHOOTRANGE, "sAllMenus.eActiveMenu = RANGE_MP_MENU_WEAPONS :: This was our selected weapon: ", iCurMenuElement)
				
			SET_ROUND_WEAPON(sClientBD[iParticipantID].sRoundDesc, sAllMenus.sMenu[ RANGE_MP_MENU_WEAPONS ].eWeapons[ iCurMenuElement ] )
			
			RANGE_MP_SET_MENU_CURRENT_ELEMENT( sAllMenus.sMenu[RANGE_MP_MENU_WEAPONS], iCurMenuElement )
			RANGE_MP_SET_MENU_CURRENT_ELEMENT( sAllMenus.sMenu[RANGE_MP_MENU_CHALLENGES], 0 )
			
			// Select first element if new weapon category is selected by a mouse.
			IF IS_PC_VERSION()
				IF bCursorWeaponCatSelect
					RANGE_MP_SET_MENU_CURRENT_ELEMENT( sAllMenus.sMenu[RANGE_MP_MENU_WEAPONS], 0 )
				ENDIF
			ENDIF
			
			sAllMenus.eActiveMenu = RANGE_MP_MENU_CHALLENGES
			
			// Can't select item -1 in challenges.
			IF (sAllMenus.sMenu[RANGE_MP_MENU_CHALLENGES].iCurElement = -1)
				sAllMenus.sMenu[RANGE_MP_MENU_CHALLENGES].iCurElement = 0
			ENDIF
						
		ELIF (sAllMenus.eActiveMenu = RANGE_MP_MENU_CHALLENGES)
		
			bCursorChallengeAccept = FALSE
			
			CDEBUG2LN(DEBUG_SHOOTRANGE, "sAllMenus.eActiveMenu = RANGE_MP_MENU_CHALLENGES :: This was our selected weapon: ", iCurMenuElement)
			// Set the entire selection here, regardless of the fact that we've picked it before.
			// This is to account for silly things like timeouts, where this may have been erased for us.
			SET_ROUND_WEAPON_CATEGORY(sClientBD[iParticipantID].sRoundDesc, INT_TO_ENUM(RANGE_WEAPON_CATEGORY, sAllMenus.sMenu[RANGE_MP_MENU_CATEGORIES].iCurElement))
			SET_ROUND_WEAPON(sClientBD[iParticipantID].sRoundDesc, sAllMenus.sMenu[ RANGE_MP_MENU_WEAPONS ].eWeapons[ RANGE_MP_GET_MENU_CURRENT_ELEMENT( sAllMenus.sMenu[RANGE_MP_MENU_WEAPONS]) ])
			SET_ROUND_CHALLENGE(sClientBD[iParticipantID].sRoundDesc, INT_TO_ENUM(CHALLENGE_INDEX, iCurMenuElement))
			
			SET_BITMASK_AS_ENUM(sClientBD[iParticipantID].iFlags, RANGE_MP_F_DoneWithMenuSelections)
			
			eRetVal = RETVAL_TRUE
		ENDIF
		
	ENDIF
		
	IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
	OR bCursorBack = TRUE
		PLAY_SOUND_FRONTEND(-1, "BACK", "HUD_FRONTEND_DEFAULT_SOUNDSET")
		
		IF (sAllMenus.eActiveMenu = RANGE_MP_MENU_WEAPONS)
			// We're going back to categories? There is no "back" button here.
			REMOVE_MENU_HELP_KEYS()
			
			IF NOT IS_RANGE_ONE_PLAYER() AND NOT IS_CLIENT_FLAG_SET(sClientBD[iParticipantID], RANGE_MP_F_IsSpectator)
				RANGE_SET_BUSYSPINNER_STRING(sAllMenus, "U_CHOOSE")
				RANGE_START_BUSYSPINNER(sAllMenus)
			ENDIF
			ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_ACCEPT, "FE_HLP4")
			ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL, "IB_QUIT")
			SET_MENU_HELP_KEYS_STACKED(FALSE)
			
			sAllMenus.eActiveMenu = RANGE_MP_MENU_CATEGORIES
			
		ELIF (sAllMenus.eActiveMenu = RANGE_MP_MENU_CHALLENGES)
			// We're going back to weapons? No need to reset the button prompts here. We could go back before!
			sAllMenus.eActiveMenu = RANGE_MP_MENU_WEAPONS
		ELIF sAllMenus.eActiveMenu = RANGE_MP_MENU_CATEGORIES
			eRetVal = RETVAL_FALSE
			CDEBUG2LN(DEBUG_SHOOTRANGE, "RANGE_MP_PROCESS_UI_INPUT_HOST :: Quit button pressed")
		ENDIF
		
	ELIF (IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_UP) OR IS_LEFT_ANALOG_AS_DPAD(LEFT_ANALOG_AS_DPAD_UP, sAllMenus.uiInput))
	OR ((IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)) AND GET_GAME_TIMER() > GET_RANGE_MENU_HOLD_DELAY(sAllMenus))
		INT iNewElem
		IF (sAllMenus.sMenu[sAllMenus.eActiveMenu].iCurElement = 0)
			iNewElem = sAllMenus.sMenu[sAllMenus.eActiveMenu].iNumElements - 1
		ELSE
			iNewElem = sAllMenus.sMenu[sAllMenus.eActiveMenu].iCurElement - 1
		ENDIF
		
		IF (iNewElem != sAllMenus.sMenu[sAllMenus.eActiveMenu].iCurElement)
			PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", "HUD_FRONTEND_DEFAULT_SOUNDSET")
			sAllMenus.sMenu[sAllMenus.eActiveMenu].iCurElement = iNewElem
		ENDIF
		
		sAllMenus.bAcceptStickInput = FALSE
		SET_RANGE_MENU_HOLD_DELAY(sAllMenus, GET_GAME_TIMER() + DELAY_BEFORE_REACTIVATED)	
		
	ELIF (IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN) OR IS_LEFT_ANALOG_AS_DPAD(LEFT_ANALOG_AS_DPAD_DOWN, sAllMenus.uiInput))
	OR ((IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)) AND GET_GAME_TIMER() > GET_RANGE_MENU_HOLD_DELAY(sAllMenus))
		INT iNewElem
		IF (sAllMenus.sMenu[sAllMenus.eActiveMenu].iCurElement = sAllMenus.sMenu[sAllMenus.eActiveMenu].iNumElements - 1)
			iNewElem = 0
		ELSE
			iNewElem = sAllMenus.sMenu[sAllMenus.eActiveMenu].iCurElement + 1
		ENDIF
		
		IF (iNewElem != sAllMenus.sMenu[sAllMenus.eActiveMenu].iCurElement)
			PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", "HUD_FRONTEND_DEFAULT_SOUNDSET")
			sAllMenus.sMenu[sAllMenus.eActiveMenu].iCurElement = iNewElem
		ENDIF
		
		sAllMenus.bAcceptStickInput = FALSE
		SET_RANGE_MENU_HOLD_DELAY(sAllMenus, GET_GAME_TIMER() + DELAY_BEFORE_REACTIVATED)
	ENDIF
	
	// For syncing with client.
	sClientBD[iParticipantID].iCatElem = sAllMenus.sMenu[RANGE_MP_MENU_CATEGORIES].iCurElement
	sClientBD[iParticipantID].iWeapElem = sAllMenus.sMenu[RANGE_MP_MENU_WEAPONS].iCurElement
	sClientBD[iParticipantID].iChalElem = sAllMenus.sMenu[RANGE_MP_MENU_CHALLENGES].iCurElement
	
	sClientBD[iParticipantID].iCurMenu = ENUM_TO_INT(sAllMenus.eActiveMenu)
	
	
	// Mouse debug stuff
//
//	DISPLAY_TEXT_WITH_NUMBER( 0.1,0.1, "NUMBER", iCursorWeaponMenuItem)
//	DISPLAY_TEXT_WITH_NUMBER( 0.1,0.2, "NUMBER", sAllMenus.sMenu[RANGE_MP_MENU_CATEGORIES].iCurElement)
//	DISPLAY_TEXT_WITH_NUMBER( 0.1,0.3, "NUMBER", sAllMenus.sMenu[RANGE_MP_MENU_WEAPONS].iCurElement)
//	DISPLAY_TEXT_WITH_NUMBER( 0.1,0.4, "NUMBER", sAllMenus.sMenu[RANGE_MP_MENU_CHALLENGES].iCurElement)
//	DISPLAY_TEXT_WITH_NUMBER( 0.1,0.5,"NUMBER", GET_CURSOR_SELECTED_WEAPON_ITEM(iCursorWeaponMenuItem, sAllMenus.sMenu[RANGE_MP_MENU_CATEGORIES].iCurElement, sAllMenus.sMenu[RANGE_MP_MENU_WEAPONS].iNumElements))
//	DISPLAY_TEXT_WITH_NUMBER( 0.1,0.6,"NUMBER", GET_CURSOR_SELECTED_WEAPON_CATEGORY_FROM_WEAPON_MENU(iCursorWeaponMenuItem, sAllMenus.sMenu[RANGE_MP_MENU_CATEGORIES].iCurElement, sAllMenus.sMenu[RANGE_MP_MENU_WEAPONS].iNumElements))
//	DISPLAY_TEXT_WITH_NUMBER( 0.1,0.7, "NUMBER", sAllMenus.sMenu[RANGE_MP_MENU_WEAPONS].iNumElements)
//	DISPLAY_TEXT_WITH_NUMBER( 0.1,0.8, "NUMBER", iCurMenuElement)
	
											
	
	
	RETURN eRetVal
ENDFUNC


/// PURPOSE:
///    Player is responsible for making sure his menu data is equal to that of the host.
/// RETURNS:
///    TRUE if we're due to leave the menu. FALSE to keep replicating.
FUNC BOOL REPLICATE_HOST_MENU(RangeMP_PlayerBD & sAllPlayerBD[], INT iClientID, RangeMP_MenuData & sMenuData, RangeMP_ParticipantInfo & sParticipantInfo )
	INT iHostClientID = RANGE_GET_OTHER_SHOOTER(iClientID, sParticipantInfo)
	
	// Just copy everything the host is doing...
	sAllPlayerBD[iClientID].iCurMenu = sAllPlayerBD[iHostClientID].iCurMenu
	sMenuData.eActiveMenu = INT_TO_ENUM(RANGE_MP_MENU_INDEX, sAllPlayerBD[iClientID].iCurMenu)
	
	sMenuData.sMenu[RANGE_MP_MENU_CATEGORIES].iCurElement = sAllPlayerBD[iHostClientID].iCatElem
	
	// If we're in the weapons menu or later, build the weapons menu.
	IF (sMenuData.eActiveMenu > RANGE_MP_MENU_CATEGORIES)
		RANGE_MP_InitWeaponsMenu(sAllPlayerBD, sMenuData.sMenu[RANGE_MP_MENU_WEAPONS], INT_TO_ENUM(RANGE_WEAPON_CATEGORY, sMenuData.sMenu[RANGE_MP_MENU_CATEGORIES].iCurElement), FALSE)
	ENDIF
	
	sMenuData.sMenu[RANGE_MP_MENU_WEAPONS].iCurElement = sAllPlayerBD[iHostClientID].iWeapElem
	sMenuData.sMenu[RANGE_MP_MENU_CHALLENGES].iCurElement = sAllPlayerBD[iHostClientID].iChalElem
	
	// Wait for the host to be done.
	IF (IS_BITMASK_AS_ENUM_SET(sAllPlayerBD[iHostClientID].iFlags, RANGE_MP_F_DoneWithMenuSelections))
		CDEBUG2LN(DEBUG_SHOOTRANGE, "Getting last set of host syncing data before starting round!")
		SET_ROUND_WEAPON_CATEGORY(sAllPlayerBD[iClientID].sRoundDesc, sAllPlayerBD[iHostClientID].sRoundDesc.eWeaponCategory)
		SET_ROUND_WEAPON(sAllPlayerBD[iClientID].sRoundDesc, sAllPlayerBD[iHostClientID].sRoundDesc.eWeaponType )
		SET_ROUND_CHALLENGE(sAllPlayerBD[iClientID].sRoundDesc, sAllPlayerBD[iHostClientID].sRoundDesc.eChallenge)
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HANDLE_REPLICATE(RangeMP_PlayerBD & sAllPlayerBD[], INT iClientID, RangeMP_MenuData & sMenuData, RangeMP_ParticipantInfo & sParticipantInfo, RangeMP_RoundFuncs& sRndFuncs)
	CDEBUG1LN( DEBUG_SHOOTRANGE, "HANDLE_REPLICATE - starting")
	IF REPLICATE_HOST_MENU(sAllPlayerBD, iClientID, sMenuData, sParticipantInfo)
		// Let the server know we're done too.
		SET_BITMASK_AS_ENUM(sAllPlayerBD[iClientID].iFlags, RANGE_MP_F_DoneWithMenuSelections)
		
		CDEBUG1LN( DEBUG_SHOOTRANGE, "HANDLE_REPLICATE - starting function pointer assignment")
		// Store our round function pointers.
		IF (sAllPlayerBD[iClientID].sRoundDesc.eChallenge = CI_GRID)
			sRndFuncs.fpRoundCreate = &RANGE_MP_CREATE_GRID_ROUND
			sRndFuncs.fpRoundStart 	= &RANGE_MP_START_GRID_ROUND
			sRndFuncs.fpRoundSetup 	= &RANGE_MP_SETUP_GRID_ROUND
			sRndFuncs.fpRoundUpdate = &RANGE_MP_UPDATE_GRID_ROUND
			sRndFuncs.fpRoundEnd 	= &RANGE_MP_END_GRID_ROUND
			
			CDEBUG1LN( DEBUG_SHOOTRANGE, "HANDLE_REPLICATE - sAllPlayerBD[iClientID].sRoundDesc.eChallenge = CI_GRID")
			RETURN TRUE
			
		ELIF (sAllPlayerBD[iClientID].sRoundDesc.eChallenge = CI_RANDOM)
			sRndFuncs.fpRoundCreate = &RANGE_MP_CREATE_RANDOM_ROUND
			sRndFuncs.fpRoundStart 	= &RANGE_MP_START_RANDOM_ROUND
			sRndFuncs.fpRoundSetup 	= &RANGE_MP_SETUP_RANDOM_ROUND
			sRndFuncs.fpRoundUpdate = &RANGE_MP_UPDATE_RANDOM_ROUND
			sRndFuncs.fpRoundEnd 	= &RANGE_MP_END_RANDOM_ROUND
			
			CDEBUG1LN( DEBUG_SHOOTRANGE, "HANDLE_REPLICATE - sAllPlayerBD[iClientID].sRoundDesc.eChallenge = CI_RANDOM")
			RETURN TRUE
		
		ELIF (sAllPlayerBD[iClientID].sRoundDesc.eChallenge = CI_COVERED)
			sRndFuncs.fpRoundCreate = &RANGE_MP_CREATE_COVERED_ROUND
			sRndFuncs.fpRoundStart 	= &RANGE_MP_START_COVERED_ROUND
			sRndFuncs.fpRoundSetup 	= &RANGE_MP_SETUP_COVERED_ROUND
			sRndFuncs.fpRoundUpdate = &RANGE_MP_UPDATE_COVERED_ROUND
			sRndFuncs.fpRoundEnd 	= &RANGE_MP_END_COVERED_ROUND
			
			CDEBUG1LN( DEBUG_SHOOTRANGE, "HANDLE_REPLICATE - sAllPlayerBD[iClientID].sRoundDesc.eChallenge = CI_COVERED")
			RETURN TRUE
			
		ENDIF

		CERRORLN( DEBUG_SHOOTRANGE, "HANDLE_REPLICATE - sAllPlayerBD[iClientID].sRoundDesc.eChallenge = NOT SET!!!!")
		RETURN TRUE
	ENDIF
	
	CDEBUG1LN( DEBUG_SHOOTRANGE, "HANDLE_REPLICATE - ending.  function pointers not yet assigned.")
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Handles the splash screen that asks if we're sure we want to leave.
/// RETURNS:
///    RETVAL_TRUE to leave. RETVAL_FALSE to remain. RETVAL_NONE for no input.
///    RETVAL_NONE also tells the system that if the host has picked a round, quit this screen and move us into the game.
FUNC THREE_STATE_RETVAL UPDATE_EXIT_SPLASH_MAINMENU(RangeMP_ServerBD & sServerBD, INT iClientID, RangeMP_ParticipantInfo & sParticipantInfo)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_RELOAD)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPRINT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_BEHIND)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PHONE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPECIAL_ABILITY)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPECIAL_ABILITY_SECONDARY)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_LR)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_UD)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2)
			
	SET_WARNING_MESSAGE_WITH_HEADER("SHR_QUIT_RANGE", "SHR_QUIT_DET", FE_WARNING_NO | FE_WARNING_YES)
	
	
	INT iScreenX, iScreenY
	GET_SCREEN_RESOLUTION(iScreenX, iScreenY)
	DRAW_MENU_HELP_SCALEFORM(iScreenX, -1, 0, FALSE)

	IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
		IF NOT IS_RANGE_ONE_PLAYER()
			SET_SHOOTING_RANGE_UNSAVED_BITFLAG(SRB_QuitFromRange)
			
			IF sServerBD.iClient0MatchWins > sServerBD.iClient1MatchWins AND iClientID = sParticipantInfo.iShooters[1]
			OR sServerBD.iClient0MatchWins < sServerBD.iClient1MatchWins AND iClientID = sParticipantInfo.iShooters[0]
				BROADCAST_BETTING_MISSION_FINISHED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(RANGE_GET_OTHER_SHOOTER(iClientID, sParticipantInfo))))
				CDEBUG2LN(DEBUG_SHOOTRANGE, "UPDATE_EXIT_SPLASH_MAINMENU() :: BROADCAST_BETTING sServerBD.iClient0MatchWins=", sServerBD.iClient0MatchWins, ", sServerBD.iClient1MatchWins=", sServerBD.iClient1MatchWins, ", iClientID=", iClientID)
			ENDIF
		ENDIF
		RETURN RETVAL_TRUE
	ELIF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
		RETURN RETVAL_FALSE
	ENDIF
	
	RETURN RETVAL_NONE
ENDFUNC

/// PURPOSE:
///    This function handles the drawing and update of the main menu, where the host selects weapon and round type.
/// PARAMS:
///    sCoreInfo - 
///    sAllPlayerBD - 
///    iClientID - 
///    sMenuData - 
///    sRndFuncs - 
///    sServerBD - 
///    sPlayerData - 
///    sRndInfo - 
///    sParticipantInfo - 
///    bContinueRunningTimer - TRUE if the timer should run and the menu should accept input.
///    bShowingResults - 
/// RETURNS:
///    RETVAL_TRUE when the host has made all selections. RETVAL_NONE to keep updating. RETVAL_FALSE when this player has missed his timer.
FUNC THREE_STATE_RETVAL RANGE_MP_UpdateMainMenu(RangeMP_CoreData & sCoreInfo, RangeMP_PlayerBD & sAllPlayerBD[], INT iClientID, RangeMP_MenuData & sMenuData, 
			RangeMP_RoundFuncs& sRndFuncs, RangeMP_ServerBD & sServerBD, RangeMP_PlayerData & sPlayerData[], RangeMP_Round & sRndInfo, RangeMP_ParticipantInfo & sParticipantInfo,
			BOOL bContinueRunningTimer = TRUE, BOOL bShowingResults = FALSE)
	BOOL bThisIsHost = (iClientID = SERVER_GET_PLAYER_IN_CONTROL(sServerBD))
	
	IF IS_PAUSE_MENU_ACTIVE()
		CDEBUG3LN(DEBUG_SHOOTRANGE, "RANGE_MP_UpdateMainMenu Pause menu is active, returning RETVAL_NONE")
		RETURN RETVAL_NONE
	ENDIF

	IF NOT g_bResultScreenDisplaying
		SET_RESULT_SCREEN_DISPLAYING_STATE(TRUE)
	ENDIF
	
	// If we haven't started our timer yet, depending on whether we're host or not, give us a different set of controls.
	IF NOT IS_TIMER_STARTED(sMenuData.sMenuTimer)
		REMOVE_MENU_HELP_KEYS()
		
		IF bShowingResults
			// If there's going to be another round, show "Loading next round".
			// If there's not, show "Waiting to exit..."
			IF IS_SERVER_FLAG_SET(sServerBD, RANGE_MP_SERVER_F_Match_Over)
				FLOAT fMatchesReqd = TO_FLOAT(GET_RANGE_SERVER_TOTAL_MATCHES(sServerBD)) / 2.0
				IF ((sServerBD.iClient0MatchWins < fMatchesReqd) AND (sServerBD.iClient1MatchWins < fMatchesReqd))
				OR (fMatchesReqd < 0)
					// Just this match is over.
					RANGE_SET_BUSYSPINNER_STRING(sMenuData, "SHR_MP_MATCHOV")
					RANGE_START_BUSYSPINNER(sMenuData)
				ELSE
					// Everything is over.
					RANGE_SET_BUSYSPINNER_STRING(sMenuData, "RANGE_OVER")
					RANGE_START_BUSYSPINNER(sMenuData)
				ENDIF
			ELSE
				// Always be notified that we're going to the next round.
				RANGE_SET_BUSYSPINNER_STRING(sMenuData, "SHR_MP_NRND")
				RANGE_START_BUSYSPINNER(sMenuData)
			ENDIF
			// Controlling player gets a continue button.
			IF NOT IS_CLIENT_FLAG_SET(sAllPlayerBD[iClientID], RANGE_MP_F_IsSpectator)
				ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_ACCEPT, "SHR_SC_CONT")
			ENDIF
			ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL, "IB_QUIT")			
			//ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_LEADERBOARD, "SCLB")
		ELSE
			IF IS_CLIENT_FLAG_SET(sAllPlayerBD[iClientID], RANGE_MP_F_IsSpectator)
				// Spectator gets QUIT, SPINNER
				RANGE_SET_BUSYSPINNER_STRING(sMenuData, "SHR_SPEC_RND")
				RANGE_START_BUSYSPINNER(sMenuData)
				ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL, "IB_QUIT")
			
			ELIF bThisIsHost
				// You're in control. There may be another player. Who knows. 
				IF NOT IS_RANGE_ONE_PLAYER() AND GET_CLIENT_RANGE_STATE( sAllPlayerBD[iClientID] ) <> RANGE_STATE_MenuFail_ServerUpdatePicker
					RANGE_SET_BUSYSPINNER_STRING(sMenuData, "U_CHOOSE")
				ENDIF
				ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_ACCEPT, "FE_HLP4")
				IF (sMenuData.eActiveMenu = RANGE_MP_MENU_CATEGORIES)
					ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL, "IB_QUIT")
				ELSE
					ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL, "FE_HLP3")
				ENDIF
			ELSE
				// You're not in control, and you're not a spectator. You're the second player.
				IF GET_CLIENT_RANGE_STATE( sAllPlayerBD[iClientID] ) <> RANGE_STATE_MenuFail_ServerUpdatePicker
					RANGE_SET_BUSYSPINNER_STRING(sMenuData, "O_CHOOSE")
				ENDIF
				ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL, "IB_QUIT")
			ENDIF
		ENDIF
		SET_MENU_HELP_KEYS_STACKED(FALSE)
		
		CDEBUG2LN(DEBUG_SHOOTRANGE, "Setting up keys for RANGE_MP_UpdateMainMenu AFTER TIMER")
		
		sMenuData.bSwappedMenu10Sec = FALSE
		RESTART_TIMER_NOW(sMenuData.sMenuTimer)
	ENDIF
	
	IF NOT sPlayerData[iClientID].bPredictionDone
		IF scLB_rank_predict.bFinishedRead
		AND NOT scLB_rank_predict.bFinishedWrite
			CDEBUG2LN(DEBUG_SHOOTRANGE, "Doing WRITE to Social Club Leaderboard ID: ", sCoreInfo.rangeLBD.ReadDataStruct.m_LeaderboardId)
			WRITE_RANGE_MP_SCLB_DATA(sPlayerData[iClientID].sPredict.eChallenge, sPlayerData[iClientID].sPredict.iWin, 
					sPlayerData[iClientID].sPredict.iShotsFiredThisRound, sPlayerData[iClientID].sPredict.iShotsHitThisRound, 
					sPlayerData[iClientID].sPredict.eWeaponGiven)
			scLB_rank_predict.bFinishedWrite = TRUE
			
			CDEBUG2LN(DEBUG_SHOOTRANGE, "After writing LBID: ", sCoreInfo.rangeLBD.ReadDataStruct.m_LeaderboardId)
		ENDIF
		
		CDEBUG2LN(DEBUG_SHOOTRANGE, "Attempting to get rank prediction details...")
		IF GET_RANK_PREDICTION_DETAILS(sCoreInfo.rangeLBD)
			sclb_useRankPrediction = TRUE
			sPlayerData[iClientID].bPredictionDone = TRUE
			
			REMOVE_MENU_HELP_KEYS()
		
			// If there's going to be another round, show "Loading next round".
			// If there's not, show "Waiting to exit..."
			IF IS_SERVER_FLAG_SET(sServerBD, RANGE_MP_SERVER_F_Match_Over)
				FLOAT fMatchesReqd = TO_FLOAT(GET_RANGE_SERVER_TOTAL_MATCHES(sServerBD)) / 2.0
				IF ((sServerBD.iClient0MatchWins < fMatchesReqd) AND (sServerBD.iClient1MatchWins < fMatchesReqd))
				OR (fMatchesReqd < 0)
					// Just this match is over.
					RANGE_SET_BUSYSPINNER_STRING(sMenuData, "SHR_MP_MATCHOV")
					RANGE_START_BUSYSPINNER(sMenuData)
				ELSE
					// Everything is over.
					RANGE_SET_BUSYSPINNER_STRING(sMenuData, "RANGE_OVER")
					RANGE_START_BUSYSPINNER(sMenuData)
				ENDIF
			ELSE
				// Always be notified that we're going to the next round.
				RANGE_SET_BUSYSPINNER_STRING(sMenuData, "SHR_MP_NRND")
				RANGE_START_BUSYSPINNER(sMenuData)
			ENDIF
			// Controlling player gets a continue button.
			IF NOT IS_CLIENT_FLAG_SET(sAllPlayerBD[iClientID], RANGE_MP_F_IsSpectator)
				ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_ACCEPT, "SHR_SC_CONT")
			ENDIF
			ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL, "IB_QUIT")		
			IF NOT IS_PLAYER_SPECTATING(PLAYER_ID())
			ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_LEADERBOARD, "SCLB")
			PRINTLN("player not spectating adding leaderboard button")
			ENDIF
			SET_MENU_HELP_KEYS_STACKED(FALSE)
			
			CDEBUG2LN(DEBUG_SHOOTRANGE, "Prediction details gotten! Prediction done for LBD: ", sCoreInfo.rangeLBD.ReadDataStruct.m_LeaderboardId)
		ENDIF
	ENDIF

	
	// RUN THE MENU
	PROCESS_RANGEMP_MAINMENU(sCoreInfo, sMenuData, sServerBD, iClientID, sPlayerData, sAllPlayerBD, sRndInfo, sParticipantInfo, bContinueRunningTimer, bShowingResults)
	
	FLOAT fTimerVal = GET_TIMER_IN_SECONDS(sMenuData.sMenuTimer)

	// If we're only drawing for the scorecard, we should leave here
	IF bShowingResults
		IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
			SET_MOUSE_CURSOR_THIS_FRAME()
		ENDIF
	
		IF ((fTimerVal > RANGE_MP_MENU_TIMEOUT) AND NOT (IS_RANGE_ONE_PLAYER() OR IS_CLIENT_FLAG_SET(sAllPlayerBD[iClientID], RANGE_MP_F_IsSpectator)))
		OR IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
			CANCEL_TIMER(sMenuData.sMenuTimer)
			SET_RESULT_SCREEN_DISPLAYING_STATE(FALSE)
			REMOVE_MENU_HELP_KEYS()
			
			CDEBUG2LN(DEBUG_SHOOTRANGE, "RANGE_MP_UpdateMainMenu :: RETURNING RETVAL_TRUE results")
			RETURN RETVAL_TRUE
		ENDIF
		
		IF (IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_LEADERBOARD) 
		OR IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_LEADERBOARD))
		AND NOT (IS_PLAYER_SPECTATING(PLAYER_ID()) OR IS_PLAYER_SCTV(PLAYER_ID()))
			IF NOT IS_RANGE_MP_MENU_FLAG_SET( sMenuData, RMPM_SHOWING_SCLB )
				SET_RANGE_MP_MENU_FLAG(sMenuData, RMPM_SHOWING_SCLB)
				REMOVE_MENU_HELP_KEYS()
				
				IF IS_SERVER_FLAG_SET(sServerBD, RANGE_MP_SERVER_F_Match_Over)
					FLOAT fMatchesReqd = TO_FLOAT(GET_RANGE_SERVER_TOTAL_MATCHES(sServerBD)) / 2.0
					IF (sServerBD.iClient0MatchWins < fMatchesReqd) AND (sServerBD.iClient1MatchWins < fMatchesReqd)
					OR (fMatchesReqd < 0)
						// Just this match is over.
						RANGE_SET_BUSYSPINNER_STRING(sMenuData, "SHR_MP_MATCHOV")
						RANGE_START_BUSYSPINNER(sMenuData)
					ELSE
						// Everything is over.
						RANGE_SET_BUSYSPINNER_STRING(sMenuData, "RANGE_OVER")
						RANGE_START_BUSYSPINNER(sMenuData)
					ENDIF
				ELSE
					IF NOT IS_RANGE_ONE_PLAYER() AND NOT IS_CLIENT_FLAG_SET(sAllPlayerBD[iClientID], RANGE_MP_F_IsSpectator) 
						RANGE_SET_BUSYSPINNER_STRING(sMenuData, "SHR_MP_NRND")
						RANGE_START_BUSYSPINNER(sMenuData)
					ENDIF
				ENDIF
				ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_ACCEPT, "SHR_SC_CONT")
				ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL, "FE_HLP3")
				IF RANGE_MP_SHOULD_SCLB_SHOW_PROFILE_BUTTON(sCoreInfo.rangeLBD)
					SET_RANGE_MP_MENU_FLAG(sMenuData, RMPM_SCLB_PROFILE_BUTTON_SHOWN)
					ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_SELECT, "SCLB_PROFILE")
				ENDIF
				SET_MENU_HELP_KEYS_STACKED(FALSE)
				
				PLAY_SOUND_FRONTEND(-1, "LEADER_BOARD", "HUD_FRONTEND_DEFAULT_SOUNDSET")
			ENDIF
		ENDIF
		
		IF IS_RANGE_MP_MENU_FLAG_SET(sMenuData, RMPM_SHOWING_SCLB)
			IF RANGE_MP_SHOULD_SCLB_SHOW_PROFILE_BUTTON(sCoreInfo.rangeLBD) AND NOT IS_RANGE_MP_MENU_FLAG_SET(sMenuData, RMPM_SCLB_PROFILE_BUTTON_SHOWN)
				SET_RANGE_MP_MENU_FLAG(sMenuData, RMPM_SCLB_PROFILE_BUTTON_SHOWN)
				REMOVE_MENU_HELP_KEYS()
				
				IF IS_SERVER_FLAG_SET(sServerBD, RANGE_MP_SERVER_F_Match_Over)
					FLOAT fMatchesReqd = TO_FLOAT(GET_RANGE_SERVER_TOTAL_MATCHES(sServerBD)) / 2.0
					IF (sServerBD.iClient0MatchWins < fMatchesReqd) AND (sServerBD.iClient1MatchWins < fMatchesReqd)
					OR (fMatchesReqd < 0)
						// Just this match is over.
						RANGE_SET_BUSYSPINNER_STRING(sMenuData, "SHR_MP_MATCHOV")
						RANGE_START_BUSYSPINNER(sMenuData)
					ELSE
						// Everything is over.
						RANGE_SET_BUSYSPINNER_STRING(sMenuData, "RANGE_OVER")
						RANGE_START_BUSYSPINNER(sMenuData)
					ENDIF
				ELSE
					IF NOT IS_RANGE_ONE_PLAYER() AND NOT IS_CLIENT_FLAG_SET(sAllPlayerBD[iClientID], RANGE_MP_F_IsSpectator) 
						RANGE_SET_BUSYSPINNER_STRING(sMenuData, "SHR_MP_NRND")
						RANGE_START_BUSYSPINNER(sMenuData)
					ENDIF
				ENDIF
				ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_ACCEPT, "SHR_SC_CONT")
				ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL, "FE_HLP3")
				ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_SELECT, "SCLB_PROFILE")
				SET_MENU_HELP_KEYS_STACKED(FALSE)
			ELIF NOT RANGE_MP_SHOULD_SCLB_SHOW_PROFILE_BUTTON(sCoreInfo.rangeLBD) AND IS_RANGE_MP_MENU_FLAG_SET(sMenuData, RMPM_SCLB_PROFILE_BUTTON_SHOWN)
				CLEAR_RANGE_MP_MENU_FLAG(sMenuData, RMPM_SCLB_PROFILE_BUTTON_SHOWN)
				REMOVE_MENU_HELP_KEYS()
				IF IS_SERVER_FLAG_SET(sServerBD, RANGE_MP_SERVER_F_Match_Over)
					FLOAT fMatchesReqd = TO_FLOAT(GET_RANGE_SERVER_TOTAL_MATCHES(sServerBD)) / 2.0
					IF (sServerBD.iClient0MatchWins < fMatchesReqd) AND (sServerBD.iClient1MatchWins < fMatchesReqd)
					OR (fMatchesReqd < 0)
						// Just this match is over.
						RANGE_SET_BUSYSPINNER_STRING(sMenuData, "SHR_MP_MATCHOV")
						RANGE_START_BUSYSPINNER(sMenuData)
					ELSE
						// Everything is over.
						RANGE_SET_BUSYSPINNER_STRING(sMenuData, "RANGE_OVER")
						RANGE_START_BUSYSPINNER(sMenuData)
					ENDIF
				ELSE
					IF NOT IS_RANGE_ONE_PLAYER() AND NOT IS_CLIENT_FLAG_SET(sAllPlayerBD[iClientID], RANGE_MP_F_IsSpectator) 
						RANGE_SET_BUSYSPINNER_STRING(sMenuData, "SHR_MP_NRND")
						RANGE_START_BUSYSPINNER(sMenuData)
					ENDIF
				ENDIF
				ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_ACCEPT, "SHR_SC_CONT")
				ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL, "FE_HLP3")
				SET_MENU_HELP_KEYS_STACKED(FALSE)
			ENDIF
		ENDIF
		
		IF IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
		OR IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
			IF IS_RANGE_MP_MENU_FLAG_SET(sMenuData, RMPM_SHOWING_SCLB)
				CLEAR_RANGE_MP_MENU_FLAG(sMenuData, RMPM_SHOWING_SCLB)
				REMOVE_MENU_HELP_KEYS()
				
				IF IS_SERVER_FLAG_SET(sServerBD, RANGE_MP_SERVER_F_Match_Over)
					FLOAT fMatchesReqd = TO_FLOAT(GET_RANGE_SERVER_TOTAL_MATCHES(sServerBD)) / 2.0
					IF ((sServerBD.iClient0MatchWins < fMatchesReqd) AND (sServerBD.iClient1MatchWins < fMatchesReqd))
					OR (fMatchesReqd < 0)
						// Just this match is over.
						RANGE_SET_BUSYSPINNER_STRING(sMenuData, "SHR_MP_MATCHOV")
						RANGE_START_BUSYSPINNER(sMenuData)
					ELSE
						// Everything is over.
						RANGE_SET_BUSYSPINNER_STRING(sMenuData, "RANGE_OVER")
						RANGE_START_BUSYSPINNER(sMenuData)
					ENDIF
				ELSE
					IF NOT IS_RANGE_ONE_PLAYER() AND NOT IS_CLIENT_FLAG_SET(sAllPlayerBD[iClientID], RANGE_MP_F_IsSpectator) 
						RANGE_SET_BUSYSPINNER_STRING(sMenuData, "SHR_MP_NRND")
						RANGE_START_BUSYSPINNER(sMenuData)
					ENDIF
				ENDIF
				ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_ACCEPT, "SHR_SC_CONT")
				ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL, "IB_QUIT")
				IF NOT (IS_PLAYER_SPECTATING(PLAYER_ID()) OR IS_PLAYER_SCTV(PLAYER_ID()))
					ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_LEADERBOARD, "SCLB")
					PRINTLN("player not spectating adding leaderboard button-124")
				ENDIF
				SET_MENU_HELP_KEYS_STACKED(FALSE)
				
				PLAY_SOUND_FRONTEND(-1, "BACK", "HUD_FRONTEND_DEFAULT_SOUNDSET")
			ELSE
				SET_RESULT_SCREEN_DISPLAYING_STATE(FALSE)
				
				CDEBUG2LN(DEBUG_SHOOTRANGE, "RANGE_MP_UpdateMainMenu :: RETURNING RETVAL_FALSE results")
				RETURN RETVAL_FALSE
			ENDIF
		ENDIF
			
		//CDEBUG2LN(DEBUG_SHOOTRANGE, "RANGE_MP_UpdateMainMenu :: bShowingResults = TRUE. No input processing  results.")
		RETURN RETVAL_NONE
	ENDIF

	// TEMP
	IF (fTimerVal > RANGE_MP_MENU_TIMEOUT) AND NOT IS_RANGE_ONE_PLAYER()
		// We've timed out. Cancel our timer. If we're the host, flag us as failing the menu.
		CANCEL_TIMER(sMenuData.sMenuTimer)
		SET_RESULT_SCREEN_DISPLAYING_STATE(FALSE)
		
		RETURN RETVAL_FALSE
	ENDIF
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////
  
	// Okay update the input:
	IF NOT bShowingResults
		IF bThisIsHost
			RANGE_PRINT_TO_SCREEN("RANGE_MP_UpdateMainMenu, host update", 0.22)
			// When processing the input is done, we have selected a round. It's data lives in sCurRoundData.
			THREE_STATE_RETVAL eRet = RETVAL_NONE
			
			IF NOT IS_PAUSE_MENU_ACTIVE() AND bContinueRunningTimer
				eRet = RANGE_MP_PROCESS_UI_INPUT_HOST(sMenuData, sAllPlayerBD)
			#IF IS_DEBUG_BUILD
			ELSE
				CDEBUG2LN(DEBUG_SHOOTRANGE, "bContinueRunningTimer=", PICK_STRING(bContinueRunningTimer, "TRUE", "FALSE"))
			#ENDIF
			ENDIF
			
			IF (eRet = RETVAL_TRUE)
				// Store our round function pointers.
				IF (sAllPlayerBD[iClientID].sRoundDesc.eChallenge = CI_GRID)
					sRndFuncs.fpRoundCreate = &RANGE_MP_CREATE_GRID_ROUND
					sRndFuncs.fpRoundStart 	= &RANGE_MP_START_GRID_ROUND
					sRndFuncs.fpRoundSetup 	= &RANGE_MP_SETUP_GRID_ROUND
					sRndFuncs.fpRoundUpdate = &RANGE_MP_UPDATE_GRID_ROUND
					sRndFuncs.fpRoundEnd 	= &RANGE_MP_END_GRID_ROUND
					
				ELIF (sAllPlayerBD[iClientID].sRoundDesc.eChallenge = CI_RANDOM)
					sRndFuncs.fpRoundCreate = &RANGE_MP_CREATE_RANDOM_ROUND
					sRndFuncs.fpRoundStart 	= &RANGE_MP_START_RANDOM_ROUND
					sRndFuncs.fpRoundSetup 	= &RANGE_MP_SETUP_RANDOM_ROUND
					sRndFuncs.fpRoundUpdate = &RANGE_MP_UPDATE_RANDOM_ROUND
					sRndFuncs.fpRoundEnd 	= &RANGE_MP_END_RANDOM_ROUND
				
				ELIF (sAllPlayerBD[iClientID].sRoundDesc.eChallenge = CI_COVERED)
					sRndFuncs.fpRoundCreate = &RANGE_MP_CREATE_COVERED_ROUND
					sRndFuncs.fpRoundStart 	= &RANGE_MP_START_COVERED_ROUND
					sRndFuncs.fpRoundSetup 	= &RANGE_MP_SETUP_COVERED_ROUND
					sRndFuncs.fpRoundUpdate = &RANGE_MP_UPDATE_COVERED_ROUND
					sRndFuncs.fpRoundEnd 	= &RANGE_MP_END_COVERED_ROUND
				ENDIF
				
				SET_RESULT_SCREEN_DISPLAYING_STATE(FALSE)
				RETURN RETVAL_TRUE
				
			ELIF (eRet = RETVAL_FALSE)
				// Okay, so the host has hit Y/Triangle to try and leave the range. Force him to quit.
				
				SET_CLIENT_RANGE_STATE(sAllPlayerBD[iClientID], RANGE_STATE_ExitConfSplash_MainMenu)
				UPDATE_EXIT_SPLASH_MAINMENU(sServerBD, iClientID, sParticipantInfo)
				SET_RESULT_SCREEN_DISPLAYING_STATE(FALSE)
			ENDIF
		ELSE
			CDEBUG2LN(DEBUG_SHOOTRANGE, "RANGE_MP_UpdateMainMenu, client update")
			// We're not the host, but we need to replicate his data.
			IF NOT HANDLE_REPLICATE(sAllPlayerBD, iClientID, sMenuData, sParticipantInfo, sRndFuncs)
				IF NOT IS_CLIENT_FLAG_SET(sAllPlayerBD[iClientID], RANGE_MP_F_IsSpectator)
					IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
						PLAY_SOUND_FRONTEND(-1, "BACK", "HUD_FRONTEND_DEFAULT_SOUNDSET")
						
						SET_CLIENT_RANGE_STATE(sAllPlayerBD[iClientID], RANGE_STATE_ExitConfSplash_MainMenu)
						UPDATE_EXIT_SPLASH_MAINMENU(sServerBD, iClientID, sParticipantInfo)
						CDEBUG2LN(DEBUG_SHOOTRANGE, "RANGE_MP_UpdateMainMenu :: back button pressed")
					ENDIF
				ENDIF
			ELSE
				SET_RESULT_SCREEN_DISPLAYING_STATE(FALSE)
				RETURN RETVAL_TRUE
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_CLIENT_FLAG_SET(sAllPlayerBD[iClientID], RANGE_MP_F_IsSpectator)
			IF NOT IS_RANGE_MP_MENU_FLAG_SET(sMenuData, RMPM_SHOWING_SCLB)
				IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
					PLAY_SOUND_FRONTEND(-1, "BACK", "HUD_FRONTEND_DEFAULT_SOUNDSET")
					
					SET_CLIENT_RANGE_STATE(sAllPlayerBD[iClientID], RANGE_STATE_ExitConfSplash_MainMenu)
					UPDATE_EXIT_SPLASH_MAINMENU(sServerBD, iClientID, sParticipantInfo)
					CDEBUG2LN(DEBUG_SHOOTRANGE, "RANGE_MP_UpdateMainMenu :: back button pressed")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN RETVAL_NONE
ENDFUNC

/// PURPOSE:
///    Displays the time remaining at the top of the screen.
PROC DISPLAY_ROUND_TIMER(FLOAT fTimeToDisplay, RangeMP_MenuData & sMenuData, RangeMP_Round & sRndInfo, BOOL bDrawTimer=TRUE)	
	// Draw timer.
	IF (sMenuData.eInRoundMenuState <> RANGE_MP_RoundMenu_DisplaySplash) AND bDrawTimer
		IF fTimeToDisplay < 11.0
			DRAW_SHOOTING_RANGE_HUD_TIMERONLY(FLOOR(fTimeToDisplay * 1000.0), HUD_COLOUR_RED)
		ELSE
			DRAW_SHOOTING_RANGE_HUD_TIMERONLY(FLOOR(fTimeToDisplay * 1000.0), HUD_COLOUR_WHITE)
		ENDIF
		
		SET_INSTRUCTIONAL_BUTTONS_UNDER_HUD_THIS_FRAME(1)
		INT iScreenX, iScreenY
		GET_SCREEN_RESOLUTION(iScreenX, iScreenY)
		DRAW_MENU_HELP_SCALEFORM(iScreenX, -1, 0, FALSE)
	#IF IS_DEBUG_BUILD
	ELSE
		CDEBUG3LN(DEBUG_SHOOTRANGE, "DISPLAY_ROUND_TIMER not drawing, reasons below")
		CDEBUG3LN(DEBUG_SHOOTRANGE, "sMenuData.eInRoundMenuState = ", GET_STRING_FROM_RANGE_MP_ROUND_MENU(sMenuData.eInRoundMenuState) )
		CDEBUG3LN(DEBUG_SHOOTRANGE, "bDrawTimer                  = ", PICK_STRING(bDrawTimer, "TRUE", "FALSE") )
		CDEBUG3LN(DEBUG_SHOOTRANGE, "fTimeToDisplay              = ", fTimeToDisplay )
	#ENDIF
	ENDIF
	
	HANDLE_RANGE_ROUND_TIMER_SOUNDS(fTimeToDisplay, sRndInfo)
ENDPROC


/// PURPOSE:
///    Waits for 90 frames to allow the server to tabulate scores
/// PARAMS:
///    sAllMenuData - 
///    sCurRound - 
/// RETURNS:
///    TRUE when the message should be showing, FALSE otherwise
FUNC BOOL IS_ROUND_WAIT_MESSAGE_SHOWING( RangeMP_PlayerBD & sAllPlayerBD[], RangeMP_ParticipantInfo & sParticipantInfo, RangeMP_Round & sCurRound )
	
//	DISPLAY_WAIT_FOR_TABULATE(sAllMenuData)
	
	IF RANGE_ARE_SHOOTERS_FLAGS_SET( RANGE_MP_F_ProcessedPostRound, sAllPlayerBD, sParticipantInfo, IS_RANGE_ONE_PLAYER () )
		CDEBUG2LN(DEBUG_SHOOTRANGE, "Wait Message should go away, both clients processed the post round via flags")
		RETURN FALSE
	ENDIF
	
	IF sCurRound.iBetweenRoundTimer < RANGE_MP_TABULATE_UPDATES
		sCurRound.iBetweenRoundTimer++
		RETURN TRUE
	ENDIF
	
	sCurRound.iBetweenRoundTimer = 0
	CDEBUG2LN(DEBUG_SHOOTRANGE, "Wait Message should go away, both clients processed the post round via timeout")
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Displays the message on the screen that the round is over.
FUNC BOOL DISPLAY_ROUND_OVER_MESSAGE(RangeMP_MenuData & sMenuInfo, RangeMP_ServerBD & sServerBD, INT iClientID, RangeMP_PlayerBD &sPlayerBD[], RangeMP_ParticipantInfo & sParticipantInfo)
	IF NOT sMenuInfo.bHasSetSplashMsg
		CLIENT_WIN_CODES eWinner = SERVER_GET_CLIENT_WINNER(sServerBD)
		CDEBUG2LN(DEBUG_SHOOTRANGE, "DISPLAY_ROUND_OVER_MESSAGE: sServerBD.iRndsPlayedInMatch: ", GET_RANGE_MP_ROUNDS_PLAYED(sServerBD))
		
		// Grab the challenge for the tagline.
		STRING sChallenge = RANGE_MP_GetCurChallengeName(sPlayerBD[iClientID].sRoundDesc.eChallenge)
		
		IF IS_RANGE_ONE_PLAYER() OR IS_CLIENT_FLAG_SET(sPlayerBD[iClientID], RANGE_MP_F_IsSpectator)
			SET_RANGE_SPLASH(sMenuInfo.uiShardBM, "SHR_MP_RNDO", sChallenge)
		
		ELIF (eWinner = CWC_NOWINNER_MATCH)
			CDEBUG2LN(DEBUG_SHOOTRANGE, "SET_RANGE_BIG_MESSAGE off of CWC_NOWINNER_MATCH")
			SET_RANGE_SPLASH(sMenuInfo.uiShardBM, "IMP_DM_DRAW", sChallenge)
			
		ELIF (eWinner = CWC_CLIENT0_MATCH)
			CDEBUG2LN(DEBUG_SHOOTRANGE, "SET_RANGE_SPLASH off of CWC_CLIENT0_MATCH")
			IF (iClientID = sParticipantInfo.iShooters[0])
				SET_RANGE_SPLASH(sMenuInfo.uiShardBM, "BM_R2P_WIN", sChallenge)
			ELSE
				SET_RANGE_SPLASH(sMenuInfo.uiShardBM, "BM_R2P_LOSS", sChallenge, HUD_COLOUR_RED)
			ENDIF
			
		ELIF (eWinner = CWC_CLIENT1_MATCH)
			CDEBUG2LN(DEBUG_SHOOTRANGE, "SET_RANGE_SPLASH off of CWC_CLIENT1_MATCH")
			IF (iClientID = sParticipantInfo.iShooters[1])
				SET_RANGE_SPLASH(sMenuInfo.uiShardBM, "BM_R2P_WIN", sChallenge)
			ELSE
				SET_RANGE_SPLASH(sMenuInfo.uiShardBM, "BM_R2P_LOSS", sChallenge, HUD_COLOUR_RED)
			ENDIF
			
		ELIF (eWinner = CWC_NONE)
			CDEBUG2LN(DEBUG_SHOOTRANGE, "SET_RANGE_SPLASH off of CWC_NONE")
			SET_RANGE_SPLASH(sMenuInfo.uiShardBM, "IMP_DM_DRAW", sChallenge)
			
		ELIF (eWinner = CWC_CLIENT0_ROUND) 
			CDEBUG2LN(DEBUG_SHOOTRANGE, "SET_RANGE_SPLASH off of CWC_CLIENT0_ROUND")
			IF (iClientID = sParticipantInfo.iShooters[0])
				SET_RANGE_SPLASH(sMenuInfo.uiShardBM, "BM_R2P_WIN", sChallenge)
			ELSE
				SET_RANGE_SPLASH(sMenuInfo.uiShardBM, "BM_R2P_LOSS", sChallenge, HUD_COLOUR_RED)
			ENDIF
			
		ELIF (eWinner = CWC_CLIENT1_ROUND)
			CDEBUG2LN(DEBUG_SHOOTRANGE, "SET_RANGE_SPLASH off of CWC_CLIENT1_ROUND")
			IF (iClientID = sParticipantInfo.iShooters[1])
				SET_RANGE_SPLASH(sMenuInfo.uiShardBM, "BM_R2P_WIN", sChallenge)
			ELSE
				SET_RANGE_SPLASH(sMenuInfo.uiShardBM, "BM_R2P_LOSS", sChallenge, HUD_COLOUR_RED)
			ENDIF
		ENDIF
		
		SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_REMOVE_EXPLOSIONS|SPC_REMOVE_PROJECTILES|SPC_REMOVE_FIRES)	//|SPC_CLEAR_TASKS)
		sMenuInfo.bHasSetSplashMsg = TRUE
	ENDIF

	// Render, but we only want to draw for around 5 seconds.
	IF UPDATE_RANGE_SPLASH(sMenuInfo)
		sMenuInfo.bHasSetSplashMsg = FALSE
		
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Fill in the scorecard scaleform with data.
PROC SETUP_SCORE_CARD(RangeMP_MenuData & sMenuInfo)
	RESTART_TIMER_NOW(sMenuInfo.sMenuTimer)
	
	// Print the help controls.
	REMOVE_MENU_HELP_KEYS()
	
	IF NOT IS_RANGE_ONE_PLAYER()
		RANGE_SET_BUSYSPINNER_STRING(sMenuInfo, "U_CHOOSE")
	ENDIF
	ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_ACCEPT, "SHR_SC_CONT")
	IF NOT (IS_PLAYER_SPECTATING(PLAYER_ID()) OR IS_PLAYER_SCTV(PLAYER_ID()))
		ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_LEADERBOARD, "SCLB")
		PRINTLN("player not spectating adding leaderboard button - 127")
	ENDIF
	
	SET_MENU_HELP_KEYS_STACKED(FALSE)
	
	sMenuInfo.bSwappedMenu10Sec = FALSE
	
	// Restart our tracking timer.
	RESTART_TIMER_NOW(sMenuInfo.sMenuTimer)
	
	// We're going to reinit the main menu here.
	INIT_RANGEMP_MAINMENU(sMenuInfo)

	//INIT_RANGEMP_SCORECARD(sMenuInfo.uiPlacement)
ENDPROC

/// PURPOSE:
///    Displays the screen that we are waiting for all palyers to exit the scorecard.
///    THIS FUNCTION IS GOOD.
FUNC BOOL DISPLAY_WAIT_FOR_SCORECARD_EXIT(RangeMP_MenuData & sMenuData)
	// Timer should have been restarted in the RANGE_STATE_DisplayRoundOver state... but JUST IN CASE...
	IF NOT IS_TIMER_STARTED(sMenuData. sMenuTimer)
		RESTART_TIMER_NOW(sMenuData. sMenuTimer)
	ENDIF	

	FLOAT fTimerVal = GET_TIMER_IN_SECONDS(sMenuData. sMenuTimer)
	INT iSeconds = ABSI(FLOOR(fTimerVal) - RANGE_MP_SCORECARD_TIMEOUT)
	IF (iSeconds = 0)
		// Out of time!
		RETURN FALSE
	ENDIF

//	SPRITE_PLACEMENT ScaleformSprite = GET_SCALEFORM_LOADING_ICON_POSITION()
//	sMenuData.spinnerData.loadingStruct.sMainStringSlot = GET_TRANSITION_STRING()
//	RUN_SCALEFORM_LOADING_ICON(sMenuData.spinnerData.loadingStruct, SHOULD_REFRESH_SCALEFORM_LOADING_ICON(sMenuData.spinnerData.loadingStruct))

	// Draw in the waiting on other client to exit scorecard...
	INT iScreenX, iScreenY
	GET_SCREEN_RESOLUTION(iScreenX, iScreenY)
	DRAW_MENU_HELP_SCALEFORM(iScreenX, -1, 0, FALSE)
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Display a screen letting the player know that the range is over, and maybe a reason why.
/// RETURNS:
///    TRUE to quit the range. FALSE to continue.
FUNC BOOL UPDATE_END_SPLASH(RANGE_END_CONDITION eTerminateReason, RangeMP_MenuData & sMenuData)	
	IF NOT sMenuData.bSetLeaveReason
		REMOVE_MENU_HELP_KEYS()
		
		IF (eTerminateReason = RANGE_END_None)
			RANGE_SET_BUSYSPINNER_STRING(sMenuData, "SHR_MP_Q_NO")
		ELIF (eTerminateReason = RANGE_END_PlayerLeft)
			RANGE_SET_BUSYSPINNER_STRING(sMenuData, "SHR_MP_Q_LV")
		ELIF (eTerminateReason = RANGE_END_NoMenuInput)
			RANGE_SET_BUSYSPINNER_STRING(sMenuData, "SHR_MP_Q_NM")
		ENDIF
		RANGE_START_BUSYSPINNER(sMenuData)
		sMenuData.bSetLeaveReason = TRUE
	ENDIF
					
	// TODO: Draw something here?
	INT iScreenX, iScreenY
	GET_SCREEN_RESOLUTION(iScreenX, iScreenY)
	DRAW_MENU_HELP_SCALEFORM(iScreenX, -1, 0, FALSE)

	IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
		CDEBUG2LN(DEBUG_SHOOTRANGE, "We have confirmed, and are exiting the shooting range!")
		sMenuData.bSetLeaveReason = FALSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Handle the UI for doing a mid round quit.
/// RETURNS:
///    FALSE to leave the game.
FUNC BOOL UPDATE_MID_ROUND_HELP_UI(RangeMP_ServerBD & sServerBD, RangeMP_MenuData & sMenuData, INT iClientID, CHALLENGE_INDEX eChallenge, RangeMP_PlayerData& sPlayerData, RangeMP_ParticipantInfo & sParticipantInfo )

	INT iRedPlayer = sParticipantInfo.iShooters[RED_PLAYER_CLIENT_ID]
	INT iBluePlayer = sParticipantInfo.iShooters[BLUE_PLAYER_CLIENT_ID]
	
	IF iRedPlayer = k_InvalidParticipantID
		CDEBUG3LN( DEBUG_SHOOTRANGE, "UPDATE_MID_ROUND_HELP_UI - iRedPlayer = k_InvalidParticipantID, this is potentially fatal." )
	ENDIF	
	IF iBluePlayer = k_InvalidParticipantID
		CDEBUG3LN( DEBUG_SHOOTRANGE, "UPDATE_MID_ROUND_HELP_UI - iBluePlayer = k_InvalidParticipantID, this is potentially fatal." )
	ENDIF
	
	SWITCH (sMenuData.eInRoundMenuState)
		CASE RANGE_MP_RoundMenu_Init
			// Tell the player which targets to shoot at
			IF iClientID = iRedPlayer AND eChallenge = CI_RANDOM
				PRINT_HELP_FOREVER("CDESC1_ORAN")
			ELIF iClientID = iRedPlayer AND eChallenge = CI_GRID
				PRINT_HELP_FOREVER("CDESC2_ORAN")
			ELIF iClientID = iRedPlayer AND eChallenge = CI_COVERED
				PRINT_HELP_FOREVER("CDESC3_ORAN")
			ELIF iClientID = iBluePlayer AND eChallenge = CI_RANDOM
				PRINT_HELP_FOREVER("CDESC1_PURP")
			ELIF iClientID = iBluePlayer AND eChallenge = CI_GRID
				PRINT_HELP_FOREVER("CDESC2_PURP")
			ELIF iClientID = iBluePlayer AND eChallenge = CI_COVERED
				PRINT_HELP_FOREVER("CDESC3_PURP")
			ENDIF
			
			REMOVE_MENU_HELP_KEYS()
			ADD_MENU_HELP_KEY_CLICKABLE(INPUT_CONTEXT, "IB_QUIT")
			RANGE_BUSYSPINNER_OFF()
			
			SETTIMERA(0)
			
			sMenuData.eInRoundMenuState = RANGE_MP_RoundMenu_DisplayPrompt
		BREAK
		
		CASE RANGE_MP_RoundMenu_DisplayPrompt
			IF TIMERA() > DEFAULT_GOD_TEXT_TIME AND NOT IS_PLAYER_FLAG_SET(sPlayerData, RANGE_MP_F_WARNED_ABOUT_LEAVING)
				// Notify the player that they can quit.
//				PRINT_HELP_FOREVER("SHR_EXIT_HELP")
				IF IS_HELP_MESSAGE_BEING_DISPLAYED()
				AND NOT IS_PLAYER_FLAG_SET(sPlayerData, RANGE_MP_F_CLEARED_COVERED_HELP1)
					CLEAR_HELP()
					SET_PLAYER_FLAG(sPlayerData, RANGE_MP_F_CLEARED_COVERED_HELP1)
				ENDIF
			ENDIF
			IF NOT IS_PLAYER_SCTV( PLAYER_ID() )
				// Update the "SELECT - Exit" Prompt.
				IF IS_CONTROL_JUST_RELEASED(PLAYER_CONTROL, INPUT_CONTEXT)
					CLEAR_HELP()

					REMOVE_MENU_HELP_KEYS()
					ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_ACCEPT, "FE_HLP29")
					ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL, "FE_HLP31")
					
					sMenuData.eInRoundMenuState = RANGE_MP_RoundMenu_DisplaySplash
				ENDIF
			ENDIF
		BREAK
		
		CASE RANGE_MP_RoundMenu_DisplaySplash
			// Block the player from doing a bunch of unnecessary action here - #529060, 530869
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_RELOAD)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPRINT)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_BEHIND)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PHONE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPECIAL_ABILITY)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPECIAL_ABILITY_SECONDARY)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_LR)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_UD)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
   			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2)
			
			// Draw the dialogue box for leaving. 
			THREE_STATE_RETVAL eRetVal 
			eRetVal = UPDATE_EXIT_SPLASH_MAINMENU(sServerBD, iClientID, sParticipantInfo)
			
			IF (eRetVal = RETVAL_TRUE)
				// Leave!
				RETURN FALSE
			ELIF (eRetVal = RETVAL_FALSE)
				// Back to the game!
				sMenuData.eInRoundMenuState = RANGE_MP_RoundMenu_Init
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Display a screen when a player can't join. Constantly update that
/// RETURNS:
///    
PROC UPDATE_CANT_JOIN(RangeMP_MenuData & sMenuData)
	// Display "Continue (X)"
	REMOVE_MENU_HELP_KEYS()
	RANGE_SET_BUSYSPINNER_STRING(sMenuData, "SHR_MP_NOJOIN")
	RANGE_START_BUSYSPINNER(sMenuData)
	ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_ACCEPT, "CELL_286")
	INT iScreenX, iScreenY
	GET_SCREEN_RESOLUTION(iScreenX, iScreenY)
	
	WHILE TRUE
		DRAW_MENU_HELP_SCALEFORM(iScreenX, -1, 0, FALSE)

		IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
			EXIT
		ENDIF	
		
		WAIT(0)
	ENDWHILE
ENDPROC

/// PURPOSE:
///    This is the function that lets a player who has finished streaming faster than another that we're 
///    "Waiting for the other player to finish loading." He can opt to quit here.
/// RETURNS:
///    RETVAL_FALSE to quit. RETVAL_NONE if nothing has happened.
FUNC THREE_STATE_RETVAL UPDATE_WAITING_FOR_OTHERS_SCREEN(RangeMP_MenuData & sMenuData)
	SWITCH (sMenuData.eWaitingMenuState)
		CASE RANGE_MP_WaitMenu_Init
			REMOVE_MENU_HELP_KEYS()
			RANGE_SET_BUSYSPINNER_STRING(sMenuData, "SHR_MP_W_OTHER")
			RANGE_START_BUSYSPINNER(sMenuData)
			ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL, "IB_QUIT")

			sMenuData.eWaitingMenuState = RANGE_MP_WaitMenu_DisplayPrompt
			CDEBUG2LN(DEBUG_SHOOTRANGE, "RANGE_MP_WaitMenu_Init :: INIT_SIMPLE_USE_CONTEXT(sMenuData.menuContexts, IB_QUIT")
		BREAK
		
		CASE RANGE_MP_WaitMenu_DisplayPrompt			
			INT iScreenX, iScreenY
			GET_SCREEN_RESOLUTION(iScreenX, iScreenY)
			DRAW_MENU_HELP_SCALEFORM(iScreenX, -1, 0, FALSE)

			IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
				CDEBUG2LN(DEBUG_SHOOTRANGE, "UPDATE_WAITING_FOR_OTHERS_SCREEN :: back button pressed")
				RETURN RETVAL_FALSE
			ENDIF	
		BREAK
	ENDSWITCH
	
	RETURN RETVAL_NONE
ENDFUNC


