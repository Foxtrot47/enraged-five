// RangeMP_CoveredRound_lib.sch
USING "RangeMP_Round.sch"
USING "RangeMP_Targets_lib.sch"
USING "RangeMP_Broadcast_lib.sch"
USING "shared_hud_displays.sch"
USING "RangeMP_Support_lib.sch"

#IF IS_DEBUG_BUILD
	INT iContTarget = 0
	VECTOR vContTargPos  = <<18.6, -1083.363, 31.25>>, vContTargRot = <<0.0,90.0,160.0>>
	VECTOR vGoalTargPos = <<18.6, -1083.363, 31.25>>, vGoalTargRot = <<0.0,0.0,160.0>>
	
	VECTOR vGridPos0 = <<16.163, -1082.4, 29.295>>, vGridRot0 = <<0.0, 90.0, -20.0>>
	VECTOR vGridPos1 = <<21.113, -1084.2, 29.295>>, vGridRot1 = <<0.0, 90.0, -20.0>>

	// All targets.
	//VECTOR vGoalPos[32], vGoalRot[32]
	
	BOOL bUseContTargetWidgetData = FALSE
	BOOL bUseGridWidgetData = FALSE
	BOOL bUseGoalWidget = FALSE
	WIDGET_GROUP_ID CR_Widgets
	
	PROC INIT_COVERED_ROUND_WIDGETS()
		CR_Widgets = START_WIDGET_GROUP("Shooting Range")
			ADD_WIDGET_BOOL("Use Control Target Widget Data", bUseContTargetWidgetData)
			ADD_WIDGET_INT_SLIDER("Which Control Target", iContTarget, 0, 3, 1)
			ADD_WIDGET_FLOAT_SLIDER("Control Targ X Pos", vContTargPos.x, -3000.0, 3000.0, 0.05)
			ADD_WIDGET_FLOAT_SLIDER("Control Targ Y Pos", vContTargPos.y, -3000.0, 3000.0, 0.05)
			ADD_WIDGET_FLOAT_SLIDER("Control Targ Z Pos", vContTargPos.z, -3000.0, 3000.0, 0.05)
			ADD_WIDGET_FLOAT_SLIDER("Control Targ X Rot", vContTargRot.x, -359.0, 359.0, 0.05)
			ADD_WIDGET_FLOAT_SLIDER("Control Targ Y Rot", vContTargRot.y, -359.0, 359.0, 0.05)
			ADD_WIDGET_FLOAT_SLIDER("Control Targ Z Rot", vContTargRot.z, -359.0, 359.0, 0.05)
			
			ADD_WIDGET_BOOL("Use Grid Widget Data", bUseGridWidgetData)
			ADD_WIDGET_FLOAT_SLIDER("Grid0 X Pos", vGridPos0.x, -3000.0, 3000.0, 0.05)
			ADD_WIDGET_FLOAT_SLIDER("Grid0 Y Pos", vGridPos0.y, -3000.0, 3000.0, 0.05)
			ADD_WIDGET_FLOAT_SLIDER("Grid0 Z Pos", vGridPos0.z, -3000.0, 3000.0, 0.05)
			ADD_WIDGET_FLOAT_SLIDER("Grid0 X Rot", vGridRot0.x, -359.0, 359.0, 0.05)
			ADD_WIDGET_FLOAT_SLIDER("Grid0 Y Rot", vGridRot0.y, -359.0, 359.0, 0.05)
			ADD_WIDGET_FLOAT_SLIDER("Grid0 Z Rot", vGridRot0.z, -359.0, 359.0, 0.05)
			
			ADD_WIDGET_FLOAT_SLIDER("Grid1 X Pos", vGridPos1.x, -3000.0, 3000.0, 0.05)
			ADD_WIDGET_FLOAT_SLIDER("Grid1 Y Pos", vGridPos1.y, -3000.0, 3000.0, 0.05)
			ADD_WIDGET_FLOAT_SLIDER("Grid1 Z Pos", vGridPos1.z, -3000.0, 3000.0, 0.05)
			ADD_WIDGET_FLOAT_SLIDER("Grid1 X Rot", vGridRot1.x, -359.0, 359.0, 0.05)
			ADD_WIDGET_FLOAT_SLIDER("Grid1 Y Rot", vGridRot1.y, -359.0, 359.0, 0.05)
			ADD_WIDGET_FLOAT_SLIDER("Grid1 Z Rot", vGridRot1.z, -359.0, 359.0, 0.05)
			
			ADD_WIDGET_BOOL("Use Goal Widget Data", bUseGoalWidget)
			ADD_WIDGET_FLOAT_SLIDER("Goal Targ X Pos", vGoalTargPos.x, -3000.0, 3000.0, 0.05)
			ADD_WIDGET_FLOAT_SLIDER("Goal Targ Y Pos", vGoalTargPos.y, -3000.0, 3000.0, 0.05)
			ADD_WIDGET_FLOAT_SLIDER("Goal Targ Z Pos", vGoalTargPos.z, -3000.0, 3000.0, 0.05)
			ADD_WIDGET_FLOAT_SLIDER("Goal Targ X Rot", vGoalTargRot.x, -359.0, 359.0, 0.05)
			ADD_WIDGET_FLOAT_SLIDER("Goal Targ Y Rot", vGoalTargRot.y, -359.0, 359.0, 0.05)
			ADD_WIDGET_FLOAT_SLIDER("Goal Targ Z Rot", vGoalTargRot.z, -359.0, 359.0, 0.05)
		STOP_WIDGET_GROUP()
	ENDPROC

	PROC UPDATE_COVERED_ROUND_WIDGETS(RangeMP_Round & sRndInfo)
		IF bUseContTargetWidgetData
			// Only going to set one, because it's kind of a pain to have a ton of widgets for all cont targets.
			SET_ENTITY_COORDS(sRndInfo.oRoundObjects[iContTarget].oMisc, vContTargPos)			
			SET_ENTITY_ROTATION(sRndInfo.oRoundObjects[iContTarget].oMisc, vContTargRot)
		ENDIF
		
		// Controls where the grids are in the range.
		IF bUseGridWidgetData
			SET_ENTITY_COORDS(sRndInfo.oMisc, vGridPos0)
			SET_ENTITY_COORDS(sRndInfo.oMisc2, vGridPos1)
			
			SET_ENTITY_ROTATION(sRndInfo.oMisc, vGridRot0)
			SET_ENTITY_ROTATION(sRndInfo.oMisc2, vGridRot1)
		ENDIF
		
		// Controls a single target on the range. Use this to extrapolate the other positions.
		IF bUseGoalWidget
			SET_ENTITY_COORDS(sRndInfo.oRoundObjects[CI_GOAL_1_1].oMisc, vGoalTargPos)
			SET_ENTITY_ROTATION(sRndInfo.oRoundObjects[CI_GOAL_1_1].oMisc, vGoalTargRot)
		ENDIF
	ENDPROC
	
	PROC CLEANUP_COVERED_ROUND_WIDGETS()
		DELETE_WIDGET_GROUP(CR_Widgets)
	ENDPROC
#ENDIF

/// PURPOSE:
///    Invoked only within the internal update. Private.
PROC RANGE_MP_UPDATE_COVERED_ROUND_UI(INT iRedScore, INT iBlueScore, INT iClientID, RangeMP_PlayerBD & sPlayerBD[], RangeMP_ParticipantInfo & sParticipantInfo )	
	Range_HUD_Data sHUDData
	sHUDData.sCurScoreLabel = ""
	
	INT iRedPlayer = sParticipantInfo.iShooters[RED_PLAYER_CLIENT_ID]
	
	IF iRedPlayer = k_InvalidParticipantID
		CDEBUG3LN( DEBUG_SHOOTRANGE, "RANGE_MP_UPDATE_COVERED_ROUND_UI - iRedPlayer = k_InvalidParticipantID, this is potentially fatal." )
	ENDIF
	
	// Red is on left, Blue is on right.
	IF (iClientID = iRedPlayer)
		sHUDData.iCurScore = iRedScore
		sHUDData.eCurScoreColor = HUD_COLOUR_ORANGE
		sHUDData.iOpponentScore = iBlueScore
		sHUDData.eOpponentScoreColor = HUD_COLOUR_PURPLE	
	ELSE
		sHUDData.iCurScore = iBlueScore
		sHUDData.eCurScoreColor = HUD_COLOUR_PURPLE
		sHUDData.iOpponentScore = iRedScore
		sHUDData.eOpponentScoreColor = HUD_COLOUR_ORANGE	
	ENDIF
	
	// Get the player names to display.
	sHUDData.bDrawPlayerNames = TRUE
	sHUDData.sCurScoreLabel = GET_PLAYER_NAME(sPlayerBD[iClientID].piCurPlayerID)
	sHUDData.sOpponentScoreLabel = GET_PLAYER_NAME(sPlayerBD[RANGE_GET_OPPOSING_SHOOTER(sParticipantInfo)].piCurPlayerID)
	
	IF IS_RANGE_ONE_PLAYER()
		sHUDData.iOpponentScore = -1
	ENDIF
	
	IF IS_CLIENT_FLAG_SET(sPlayerBD[iClientID], RANGE_MP_F_IsSpectator)
		sHUDData.iCurScore = -1
	ENDIF
	
	// Trigger the draw.
	DRAW_SHOOTING_RANGE_HUD(sHUDData, TRUE)
ENDPROC


PROC RANGE_MP_CREATE_COVERED_ROUND(RangeMP_CoreData & sCoreInfo, RangeMP_Round & sRndInfo, RangeMP_PlayerBD & sPlayerBD)
	// Create the targets that will control accessability to the others.
	sCoreInfo.iWinnings = sCoreInfo.iWinnings
	
	IF NOT IS_BIT_SET(sPlayerBD.iTargetsField2, 31)
		CREATE_COVERED_ROUND_CONTROL_TARGET(sRndInfo, sRndInfo.oRoundObjects[CI_COVERED_CONTROL_1], 0, TRUE)
		CREATE_COVERED_ROUND_CONTROL_TARGET(sRndInfo, sRndInfo.oRoundObjects[CI_COVERED_CONTROL_2], 1, FALSE)
		CREATE_COVERED_ROUND_CONTROL_TARGET(sRndInfo, sRndInfo.oRoundObjects[CI_COVERED_CONTROL_3], 2, TRUE)
		CREATE_COVERED_ROUND_CONTROL_TARGET(sRndInfo, sRndInfo.oRoundObjects[CI_COVERED_CONTROL_4], 3, FALSE)
		CLEAR_BIT(sPlayerBD.iTargetsField1, ENUM_TO_INT(CI_COVERED_CONTROL_1))
		SET_BIT(sPlayerBD.iTargetsField1, 	ENUM_TO_INT(CI_COVERED_CONTROL_2))
		CLEAR_BIT(sPlayerBD.iTargetsField1, ENUM_TO_INT(CI_COVERED_CONTROL_3))
		SET_BIT(sPlayerBD.iTargetsField1, 	ENUM_TO_INT(CI_COVERED_CONTROL_4))
	ELSE
		CREATE_COVERED_ROUND_CONTROL_TARGET(sRndInfo, sRndInfo.oRoundObjects[CI_COVERED_CONTROL_1], 0, NOT IS_BIT_SET(sPlayerBD.iTargetsField1, ENUM_TO_INT(CI_COVERED_CONTROL_1)))
		CREATE_COVERED_ROUND_CONTROL_TARGET(sRndInfo, sRndInfo.oRoundObjects[CI_COVERED_CONTROL_2], 1, NOT IS_BIT_SET(sPlayerBD.iTargetsField1, ENUM_TO_INT(CI_COVERED_CONTROL_2)))
		CREATE_COVERED_ROUND_CONTROL_TARGET(sRndInfo, sRndInfo.oRoundObjects[CI_COVERED_CONTROL_3], 2, NOT IS_BIT_SET(sPlayerBD.iTargetsField1, ENUM_TO_INT(CI_COVERED_CONTROL_3)))
		CREATE_COVERED_ROUND_CONTROL_TARGET(sRndInfo, sRndInfo.oRoundObjects[CI_COVERED_CONTROL_4], 3, NOT IS_BIT_SET(sPlayerBD.iTargetsField1, ENUM_TO_INT(CI_COVERED_CONTROL_4)))
	ENDIF
	
	// Create the two grids -- We follow up creation by resetting the position and rotation because it's somehow different
	// from creating the grid at that position, then rotating it.
	sRndInfo.oMisc = GET_ENTITY_FROM_PED_OR_VEHICLE(CREATE_OBJECT(PROP_TARGET_FRAME_01, <<10.0, -10.0, 10.0>>, FALSE, FALSE))
	sRndInfo.oMisc2 = GET_ENTITY_FROM_PED_OR_VEHICLE(CREATE_OBJECT(PROP_TARGET_FRAME_01, <<20.0, -20.0, 20.0>>, FALSE, FALSE))
	SET_ENTITY_COORDS(sRndInfo.oMisc, sRndInfo.sCovData.vGrid0Pos)
	SET_ENTITY_COORDS(sRndInfo.oMisc2, sRndInfo.sCovData.vGrid1Pos)
	SET_ENTITY_ROTATION(sRndInfo.oMisc, <<0.0, 90.0, sRndInfo.sCovData.fGridHead>>)
	SET_ENTITY_ROTATION(sRndInfo.oMisc2, <<0.0, 90.0, sRndInfo.sCovData.fGridHead>>)
	SET_ENTITY_INVINCIBLE(sRndInfo.oMisc, TRUE)
	SET_ENTITY_INVINCIBLE(sRndInfo.oMisc2, TRUE)
	
	IF NOT IS_BIT_SET(sPlayerBD.iTargetsField2, 31)
		// Create all of the goal targets. -- LEFT SIDE.
		CREATE_COVERED_ROUND_GOAL_TARGET(sRndInfo, sRndInfo.oRoundObjects[CI_GOAL_1_1], 0, 0, FALSE)
		CREATE_COVERED_ROUND_GOAL_TARGET(sRndInfo, sRndInfo.oRoundObjects[CI_GOAL_1_2], 0, 1, FALSE)
		CREATE_COVERED_ROUND_GOAL_TARGET(sRndInfo, sRndInfo.oRoundObjects[CI_GOAL_1_3], 0, 2, FALSE)
		CREATE_COVERED_ROUND_GOAL_TARGET(sRndInfo, sRndInfo.oRoundObjects[CI_GOAL_1_4], 0, 3, FALSE)
		CREATE_COVERED_ROUND_GOAL_TARGET(sRndInfo, sRndInfo.oRoundObjects[CI_GOAL_2_1], 1, 0, FALSE)
		CREATE_COVERED_ROUND_GOAL_TARGET(sRndInfo, sRndInfo.oRoundObjects[CI_GOAL_2_2], 1, 1, FALSE)
		CREATE_COVERED_ROUND_GOAL_TARGET(sRndInfo, sRndInfo.oRoundObjects[CI_GOAL_2_3], 1, 2, FALSE)
		CREATE_COVERED_ROUND_GOAL_TARGET(sRndInfo, sRndInfo.oRoundObjects[CI_GOAL_2_4], 1, 3, FALSE)
		CREATE_COVERED_ROUND_GOAL_TARGET(sRndInfo, sRndInfo.oRoundObjects[CI_GOAL_3_1], 2, 0, FALSE)
		CREATE_COVERED_ROUND_GOAL_TARGET(sRndInfo, sRndInfo.oRoundObjects[CI_GOAL_3_2], 2, 1, FALSE)
		CREATE_COVERED_ROUND_GOAL_TARGET(sRndInfo, sRndInfo.oRoundObjects[CI_GOAL_3_3], 2, 2, FALSE)
		CREATE_COVERED_ROUND_GOAL_TARGET(sRndInfo, sRndInfo.oRoundObjects[CI_GOAL_3_4], 2, 3, FALSE)
		CREATE_COVERED_ROUND_GOAL_TARGET(sRndInfo, sRndInfo.oRoundObjects[CI_GOAL_4_1], 3, 0, FALSE)
		CREATE_COVERED_ROUND_GOAL_TARGET(sRndInfo, sRndInfo.oRoundObjects[CI_GOAL_4_2], 3, 1, FALSE)
		CREATE_COVERED_ROUND_GOAL_TARGET(sRndInfo, sRndInfo.oRoundObjects[CI_GOAL_4_3], 3, 2, FALSE)
		CREATE_COVERED_ROUND_GOAL_TARGET(sRndInfo, sRndInfo.oRoundObjects[CI_GOAL_4_4], 3, 3, FALSE)
		// Use field 1 for targets Covered Control 1 to 2-8
		SET_BIT(sPlayerBD.iTargetsField1, ENUM_TO_INT(CI_GOAL_1_1))
		SET_BIT(sPlayerBD.iTargetsField1, ENUM_TO_INT(CI_GOAL_1_2))
		SET_BIT(sPlayerBD.iTargetsField1, ENUM_TO_INT(CI_GOAL_1_3))
		SET_BIT(sPlayerBD.iTargetsField1, ENUM_TO_INT(CI_GOAL_1_4))
		SET_BIT(sPlayerBD.iTargetsField1, ENUM_TO_INT(CI_GOAL_2_1))
		SET_BIT(sPlayerBD.iTargetsField1, ENUM_TO_INT(CI_GOAL_2_2))
		SET_BIT(sPlayerBD.iTargetsField1, ENUM_TO_INT(CI_GOAL_2_3))
		SET_BIT(sPlayerBD.iTargetsField1, ENUM_TO_INT(CI_GOAL_2_4))
		// Use field2 for targets 3-1 and above
		SET_BIT(sPlayerBD.iTargetsField2, ENUM_TO_INT(CI_GOAL_3_1) - COVERED_ROUND_FIELD_BREAK)
		SET_BIT(sPlayerBD.iTargetsField2, ENUM_TO_INT(CI_GOAL_3_2) - COVERED_ROUND_FIELD_BREAK)
		SET_BIT(sPlayerBD.iTargetsField2, ENUM_TO_INT(CI_GOAL_3_3) - COVERED_ROUND_FIELD_BREAK)
		SET_BIT(sPlayerBD.iTargetsField2, ENUM_TO_INT(CI_GOAL_3_4) - COVERED_ROUND_FIELD_BREAK)
		SET_BIT(sPlayerBD.iTargetsField2, ENUM_TO_INT(CI_GOAL_4_1) - COVERED_ROUND_FIELD_BREAK)
		SET_BIT(sPlayerBD.iTargetsField2, ENUM_TO_INT(CI_GOAL_4_2) - COVERED_ROUND_FIELD_BREAK)
		SET_BIT(sPlayerBD.iTargetsField2, ENUM_TO_INT(CI_GOAL_4_3) - COVERED_ROUND_FIELD_BREAK)
		SET_BIT(sPlayerBD.iTargetsField2, ENUM_TO_INT(CI_GOAL_4_4) - COVERED_ROUND_FIELD_BREAK)
		
		// Create all of the goal targets. -- RIGHT SIDE.
		CREATE_COVERED_ROUND_GOAL_TARGET(sRndInfo, sRndInfo.oRoundObjects[CI_GOAL_1_5], 0, 4)
		CREATE_COVERED_ROUND_GOAL_TARGET(sRndInfo, sRndInfo.oRoundObjects[CI_GOAL_1_6], 0, 5)
		CREATE_COVERED_ROUND_GOAL_TARGET(sRndInfo, sRndInfo.oRoundObjects[CI_GOAL_1_7], 0, 6)
		CREATE_COVERED_ROUND_GOAL_TARGET(sRndInfo, sRndInfo.oRoundObjects[CI_GOAL_1_8], 0, 7)
		CREATE_COVERED_ROUND_GOAL_TARGET(sRndInfo, sRndInfo.oRoundObjects[CI_GOAL_2_5], 1, 4)
		CREATE_COVERED_ROUND_GOAL_TARGET(sRndInfo, sRndInfo.oRoundObjects[CI_GOAL_2_6], 1, 5)
		CREATE_COVERED_ROUND_GOAL_TARGET(sRndInfo, sRndInfo.oRoundObjects[CI_GOAL_2_7], 1, 6)
		CREATE_COVERED_ROUND_GOAL_TARGET(sRndInfo, sRndInfo.oRoundObjects[CI_GOAL_2_8], 1, 7)
		CREATE_COVERED_ROUND_GOAL_TARGET(sRndInfo, sRndInfo.oRoundObjects[CI_GOAL_3_5], 2, 4)
		CREATE_COVERED_ROUND_GOAL_TARGET(sRndInfo, sRndInfo.oRoundObjects[CI_GOAL_3_6], 2, 5)
		CREATE_COVERED_ROUND_GOAL_TARGET(sRndInfo, sRndInfo.oRoundObjects[CI_GOAL_3_7], 2, 6)
		CREATE_COVERED_ROUND_GOAL_TARGET(sRndInfo, sRndInfo.oRoundObjects[CI_GOAL_3_8], 2, 7)
		CREATE_COVERED_ROUND_GOAL_TARGET(sRndInfo, sRndInfo.oRoundObjects[CI_GOAL_4_5], 3, 4)
		CREATE_COVERED_ROUND_GOAL_TARGET(sRndInfo, sRndInfo.oRoundObjects[CI_GOAL_4_6], 3, 5)
		CREATE_COVERED_ROUND_GOAL_TARGET(sRndInfo, sRndInfo.oRoundObjects[CI_GOAL_4_7], 3, 6)
		CREATE_COVERED_ROUND_GOAL_TARGET(sRndInfo, sRndInfo.oRoundObjects[CI_GOAL_4_8], 3, 7)
		// Use field 1 for targets Covered Control 1 to 2-8
		SET_BIT(sPlayerBD.iTargetsField1, ENUM_TO_INT(CI_GOAL_1_5))
		SET_BIT(sPlayerBD.iTargetsField1, ENUM_TO_INT(CI_GOAL_1_6))
		SET_BIT(sPlayerBD.iTargetsField1, ENUM_TO_INT(CI_GOAL_1_7))
		SET_BIT(sPlayerBD.iTargetsField1, ENUM_TO_INT(CI_GOAL_1_8))
		SET_BIT(sPlayerBD.iTargetsField1, ENUM_TO_INT(CI_GOAL_2_5))
		SET_BIT(sPlayerBD.iTargetsField1, ENUM_TO_INT(CI_GOAL_2_6))
		SET_BIT(sPlayerBD.iTargetsField1, ENUM_TO_INT(CI_GOAL_2_7))
		SET_BIT(sPlayerBD.iTargetsField1, ENUM_TO_INT(CI_GOAL_2_8))
		// Use field 2 for targets 3-1 and above
		SET_BIT(sPlayerBD.iTargetsField2, ENUM_TO_INT(CI_GOAL_3_5) - COVERED_ROUND_FIELD_BREAK)
		SET_BIT(sPlayerBD.iTargetsField2, ENUM_TO_INT(CI_GOAL_3_6) - COVERED_ROUND_FIELD_BREAK)
		SET_BIT(sPlayerBD.iTargetsField2, ENUM_TO_INT(CI_GOAL_3_7) - COVERED_ROUND_FIELD_BREAK)
		SET_BIT(sPlayerBD.iTargetsField2, ENUM_TO_INT(CI_GOAL_3_8) - COVERED_ROUND_FIELD_BREAK)
		SET_BIT(sPlayerBD.iTargetsField2, ENUM_TO_INT(CI_GOAL_4_5) - COVERED_ROUND_FIELD_BREAK)
		SET_BIT(sPlayerBD.iTargetsField2, ENUM_TO_INT(CI_GOAL_4_6) - COVERED_ROUND_FIELD_BREAK)
		SET_BIT(sPlayerBD.iTargetsField2, ENUM_TO_INT(CI_GOAL_4_7) - COVERED_ROUND_FIELD_BREAK)
		SET_BIT(sPlayerBD.iTargetsField2, ENUM_TO_INT(CI_GOAL_4_8) - COVERED_ROUND_FIELD_BREAK)
	ELSE
		CLEAR_BIT(sPlayerBD.iTargetsField2, 31)
		INT index, iRow, iCol
		BOOL bR1Blue = NOT IS_BIT_SET(sPlayerBD.iTargetsField1, ENUM_TO_INT(CI_COVERED_CONTROL_1))
		BOOL bR2Blue = NOT IS_BIT_SET(sPlayerBD.iTargetsField1, ENUM_TO_INT(CI_COVERED_CONTROL_2))
		BOOL bR3Blue = NOT IS_BIT_SET(sPlayerBD.iTargetsField1, ENUM_TO_INT(CI_COVERED_CONTROL_3))
		BOOL bR4Blue = NOT IS_BIT_SET(sPlayerBD.iTargetsField1, ENUM_TO_INT(CI_COVERED_CONTROL_4))
		
		FOR index = CI_GOAL_1_1 TO CI_GOAL_2_8
			iRow = index / 4
			IF IS_BIT_SET(sPlayerBD.iTargetsField1, index)	// AND IS_BIT_SET(sPlayerBD.iTargetsField1, iRow)	// checking the target by index and the control target by iRow
				iCol = index % 4
				CREATE_COVERED_ROUND_GOAL_TARGET(sRndInfo, sRndInfo.oRoundObjects[index], iRow, iCol, FALSE)
				UPDATE_GOAL_TARGET(sRndInfo, sRndInfo.oRoundObjects[index], index, bR1Blue, bR2Blue, bR3Blue, bR4Blue)
				CDEBUG2LN(DEBUG_SHOOTRANGE, "RANGE_MP_CREATE_COVERED_ROUND :: ORANGE at index=", index, ", iRow=", iRow, ", iCol=", iCol)
			ENDIF
		ENDFOR
		FOR index = CI_GOAL_3_1 TO CI_GOAL_4_8
			iRow = (index - COVERED_ROUND_FIELD_BREAK)/ 4
			IF IS_BIT_SET(sPlayerBD.iTargetsField2, (index - COVERED_ROUND_FIELD_BREAK))	// AND NOT IS_BIT_SET(sPlayerBD.iTargetsField1, iRow)
				iCol = (index % 4) + 4
				CREATE_COVERED_ROUND_GOAL_TARGET(sRndInfo, sRndInfo.oRoundObjects[index], iRow, iCol, TRUE)
				UPDATE_GOAL_TARGET(sRndInfo, sRndInfo.oRoundObjects[index], index, bR1Blue, bR2Blue, bR3Blue, bR4Blue)
				CDEBUG2LN(DEBUG_SHOOTRANGE, "RANGE_MP_CREATE_COVERED_ROUND :: PURPLE at index=", index, ", iRow=", iRow, ", iCol=", iCol)
			ENDIF
		ENDFOR
	ENDIF
	
	// Debug widget setup.
//#IF IS_DEBUG_BUILD
//	INIT_COVERED_ROUND_WIDGETS()
//#ENDIF
ENDPROC

FUNC BOOL RANGE_MP_SETUP_COVERED_ROUND(RangeMP_Round & sCurRoundInfo)
	UNUSED_PARAMETER(sCurRoundInfo)
	
	RETURN TRUE
ENDFUNC

PROC RANGE_MP_START_COVERED_ROUND(RangeMP_Round & sRndInfo)
	UNUSED_PARAMETER(sRndInfo)
ENDPROC

PROC RANGE_MP_UPDATE_COVERED_ROUND(RangeMP_Round & sRndInfo, INT iClientID, RangeMP_PlayerData & sThisPlayerData, 
		RangeMP_PlayerBD & sPlayerBD[], INT iClient0Score, INT iClient1Score, RangeMP_MenuData & sMenuData, RangeMP_ParticipantInfo & sParticipantInfo, BOOL bRenderHUD = TRUE)
	UNUSED_PARAMETER(sThisPlayerData)
	iClient0Score = iClient0Score
	iClient1Score = iClient1Score
	UNUSED_PARAMETER(sMenuData)
	
	// Store our current player once.
	ENTITY_INDEX playerEntity = GET_ENTITY_FROM_PED_OR_VEHICLE(playerPed)
	
	// If we've fired a shot this frame, store it:
	BOOL bTrackingForMiss = FALSE
	VECTOR vDontCare
	IF GET_PED_LAST_WEAPON_IMPACT_COORD(playerPed, vDontCare) AND NOT IS_PLAYER_FLAG_SET(sThisPlayerData, RANGE_MP_F_DONT_COUNT_SHOTS_FIRED)
		sThisPlayerData.iShotsFired += 1
		sThisPlayerData.iShotsFiredThisRound += 1
		bTrackingForMiss = TRUE
	ENDIF
	
	INT index = 0
	
	INT iRedPlayer = sParticipantInfo.iShooters[RED_PLAYER_CLIENT_ID]
	INT iBluePlayer = sParticipantInfo.iShooters[BLUE_PLAYER_CLIENT_ID]
	
	IF iRedPlayer = k_InvalidParticipantID
		CDEBUG3LN( DEBUG_SHOOTRANGE, "RANGE_MP_UPDATE_COVERED_ROUND - iRedPlayer = k_InvalidParticipantID, this is potentially fatal." )
	ENDIF	
	
	IF iBluePlayer = k_InvalidParticipantID
		CDEBUG3LN( DEBUG_SHOOTRANGE, "RANGE_MP_UPDATE_COVERED_ROUND - iBluePlayer = k_InvalidParticipantID, this is potentially fatal." )
	ENDIF	
	
	// Detect any hits on the control targets.
	// If the target is on the side where 
	FOR index = CI_COVERED_CONTROL_1 TO CI_COVERED_END_CONTROL
		// Can flip if we're not flipping.
		IF NOT CHECK_TARGET_FLAG(sRndInfo.oRoundObjects[index], TARG_F_FLIPPING_TO_RED) AND NOT 
					CHECK_TARGET_FLAG(sRndInfo.oRoundObjects[index], TARG_F_FLIPPING_TO_BLUE)
			IF (iClientID = iBluePlayer)
				IF CHECK_TARGET_FLAG(sRndInfo.oRoundObjects[index], TARG_F_IS_BLUE)
					// Blue targets are the oTarget
					IF NOT IS_ENTITY_DEAD(sRndInfo.oRoundObjects[index].oTarget)
						IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(sRndInfo.oRoundObjects[index].oTarget, playerEntity)
							SET_ENTITY_HEALTH(sRndInfo.oRoundObjects[index].oTarget, TARGET_STARTING_HEALTH)
							
							sThisPlayerData.iShotsHit += 1
							sThisPlayerData.iShotsHitThisRound += 1
							INCREMENT_MP_INT_CHARACTER_STAT(MP_STAT_HITS_PEDS_VEHICLES)
							bTrackingForMiss = FALSE
							
							CDEBUG2LN(DEBUG_SHOOTRANGE, "Player ID ", iClientID, " broadcasting a hit on control target ", index)
							
							BROADCAST_HIT_TO_OTHER_PLAYERS(index, iClientID, sPlayerBD)
							SET_TARGET_FLAG(sRndInfo.oRoundObjects[index], TARG_F_FLIPPING_TO_RED)
							SET_BIT(sPlayerBD[iClientID].iTargetsField1, index)
							
							// Clear damage so it's not counted twice.
							CLEAR_ENTITY_LAST_DAMAGE_ENTITY(sRndInfo.oRoundObjects[index].oTarget)
						ENDIF
					ENDIF
				ENDIF
			ELIF (iClientID = iRedPlayer)
				IF CHECK_TARGET_FLAG(sRndInfo.oRoundObjects[index], TARG_F_IS_RED)
					// Red targets are the oTarget2
					IF NOT IS_ENTITY_DEAD(sRndInfo.oRoundObjects[index].oTarget2)
						IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(sRndInfo.oRoundObjects[index].oTarget2, playerEntity)
							SET_ENTITY_HEALTH(sRndInfo.oRoundObjects[index].oTarget2, TARGET_STARTING_HEALTH)
							
							sThisPlayerData.iShotsHit += 1
							sThisPlayerData.iShotsHitThisRound += 1
							INCREMENT_MP_INT_CHARACTER_STAT(MP_STAT_HITS_PEDS_VEHICLES)
							bTrackingForMiss = FALSE
							
							CDEBUG2LN(DEBUG_SHOOTRANGE, "Player ID ", iClientID, " broadcasting a hit on control target ", index)
							
							BROADCAST_HIT_TO_OTHER_PLAYERS(index, iClientID, sPlayerBD)
							SET_TARGET_FLAG(sRndInfo.oRoundObjects[index], TARG_F_FLIPPING_TO_BLUE)
							CLEAR_BIT(sPlayerBD[iClientID].iTargetsField1, index)
							
							// Clear damage so it's not counted twice.
							CLEAR_ENTITY_LAST_DAMAGE_ENTITY(sRndInfo.oRoundObjects[index].oTarget2)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELIF CHECK_TARGET_FLAG(sRndInfo.oRoundObjects[index], TARG_F_FLIPPING_TO_RED) OR CHECK_TARGET_FLAG(sRndInfo.oRoundObjects[index], TARG_F_FLIPPING_TO_BLUE)
			CLEAR_ENTITY_LAST_DAMAGE_ENTITY(sRndInfo.oRoundObjects[index].oTarget)
			CLEAR_ENTITY_LAST_DAMAGE_ENTITY(sRndInfo.oRoundObjects[index].oTarget2)
		ENDIF
		
		// Update the control targets...
		UPDATE_CONTROL_TARGET(sRndInfo, sRndInfo.oRoundObjects[index], index)
	ENDFOR
	
	// Detect any hits on the actual targets if they're uncovered.
	BOOL bR1Blue = CHECK_TARGET_FLAG(sRndInfo.oRoundObjects[CI_COVERED_CONTROL_1], TARG_F_IS_BLUE)
	BOOL bR2Blue = CHECK_TARGET_FLAG(sRndInfo.oRoundObjects[CI_COVERED_CONTROL_2], TARG_F_IS_BLUE)
	BOOL bR3Blue = CHECK_TARGET_FLAG(sRndInfo.oRoundObjects[CI_COVERED_CONTROL_3], TARG_F_IS_BLUE)
	BOOL bR4Blue = CHECK_TARGET_FLAG(sRndInfo.oRoundObjects[CI_COVERED_CONTROL_4], TARG_F_IS_BLUE)
	
//	INT iCnt = 0
//	IF bR1Blue		iCnt += 1		ENDIF
//	IF bR2Blue		iCnt += 1		ENDIF
//	IF bR3Blue		iCnt += 1		ENDIF
//	IF bR4Blue		iCnt += 1		ENDIF
//	CPRINTLN(DEBUG_SHOOTRANGE, "There are this many blue targets: ", iCnt)
	
	IF (iClientID = iRedPlayer) 
		// If all targets are on my side, and I haven't scored a point
		IF NOT bR1Blue AND NOT bR2Blue AND NOT bR3Blue AND NOT bR4Blue
			IF (iClient1Score = 0)
				// If we haven't showed the tutorial message for being blocked, but have already
				// showed second help message
				IF NOT IS_PLAYER_FLAG_SET(sThisPlayerData, RANGE_MP_F_CLEARED_COVERED_HELP1)
					PRINT_HELP("CDESC4_ORAN")
					SET_PLAYER_FLAG(sThisPlayerData, RANGE_MP_F_CLEARED_COVERED_HELP1)
				ENDIF
			ENDIF 
		ENDIF 
	ELSE 
		// If all targets are on my side, and I haven't scored a point
		IF bR1Blue AND bR2Blue AND bR3Blue AND bR4Blue 
			IF (iClient0Score = 0)
				// If we haven't showed the tutorial message for being blocked, but have already
				// showed second help message
				IF NOT IS_PLAYER_FLAG_SET(sThisPlayerData, RANGE_MP_F_CLEARED_COVERED_HELP1)
					PRINT_HELP("CDESC4_PURP")
					SET_PLAYER_FLAG(sThisPlayerData, RANGE_MP_F_CLEARED_COVERED_HELP1)
				ENDIF
			ENDIF 
		ENDIF 
	ENDIF

	FOR index = CI_GOAL_1_1 TO CI_GOAL_END	
		// See if we've shot the target!
		IF NOT IS_ENTITY_DEAD(sRndInfo.oRoundObjects[index].oTarget)
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(sRndInfo.oRoundObjects[index].oTarget, playerEntity)
				SET_ENTITY_HEALTH(sRndInfo.oRoundObjects[index].oTarget, TARGET_STARTING_HEALTH)
				
				// Does it count?
				IF NOT CHECK_TARGET_FLAG(sRndInfo.oRoundObjects[index], TARG_F_ROTATING_IN) AND NOT 
						CHECK_TARGET_FLAG(sRndInfo.oRoundObjects[index], TARG_F_ROTATING_OUT) AND NOT 
						CHECK_TARGET_FLAG(sRndInfo.oRoundObjects[index], TARG_F_DONE)
				
					IF (iClientID = iBluePlayer)
						// Did blue player shoot a blue target?
						IF CHECK_TARGET_FLAG(sRndInfo.oRoundObjects[index], TARG_F_IS_BLUE)
							CDEBUG2LN(DEBUG_SHOOTRANGE, "Player ID ", iClientID, " broadcasting a hit on goal target ", index)
							BROADCAST_HIT_TO_OTHER_PLAYERS(index, iClientID, sPlayerBD)
							SET_TARGET_FLAG(sRndInfo.oRoundObjects[index], TARG_F_FLIP_OUT)
							sThisPlayerData.iShotsHitThisRound += 1
							sThisPlayerData.iShotsHit += 1
							INCREMENT_MP_INT_CHARACTER_STAT(MP_STAT_HITS_PEDS_VEHICLES)
							bTrackingForMiss = FALSE
							IF index < COVERED_ROUND_FIELD_BREAK
								CLEAR_BIT(sPlayerBD[iClientID].iTargetsField1, index)
							ELSE
								CLEAR_BIT(sPlayerBD[iClientID].iTargetsField2, index - COVERED_ROUND_FIELD_BREAK)
							ENDIF
						ENDIF
					ELSE
						// Did red player shoot a red target?
						IF CHECK_TARGET_FLAG(sRndInfo.oRoundObjects[index], TARG_F_IS_RED)
							CDEBUG2LN(DEBUG_SHOOTRANGE, "Player ID ", iClientID, " broadcasting a hit on goal target ", index)
							BROADCAST_HIT_TO_OTHER_PLAYERS(index, iClientID, sPlayerBD)
							SET_TARGET_FLAG(sRndInfo.oRoundObjects[index], TARG_F_FLIP_OUT)
							sThisPlayerData.iShotsHitThisRound += 1
							sThisPlayerData.iShotsHit += 1
							INCREMENT_MP_INT_CHARACTER_STAT(MP_STAT_HITS_PEDS_VEHICLES)
							bTrackingForMiss = FALSE
							IF index < COVERED_ROUND_FIELD_BREAK
								CLEAR_BIT(sPlayerBD[iClientID].iTargetsField1, index)
							ELSE
								CLEAR_BIT(sPlayerBD[iClientID].iTargetsField2, index - COVERED_ROUND_FIELD_BREAK)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				CLEAR_ENTITY_LAST_DAMAGE_ENTITY(sRndInfo.oRoundObjects[index].oTarget)
			ENDIF
			
			UPDATE_GOAL_TARGET(sRndInfo, sRndInfo.oRoundObjects[index], index, bR1Blue, bR2Blue, bR3Blue, bR4Blue)
		ENDIF
	ENDFOR
	
	// Did we miss?
	IF bTrackingForMiss
		sThisPlayerData.iShotsMissedThisRound += 1
	ENDIF
	
	// Draw our score onscreen.
	IF bRenderHUD
		RANGE_MP_UPDATE_COVERED_ROUND_UI(iClient0Score, iClient1Score, iClientID, sPlayerBD, sParticipantInfo)
	ENDIF
	
	// Update our widgets!
//#IF IS_DEBUG_BUILD
//	UPDATE_COVERED_ROUND_WIDGETS(sRndInfo)
//#ENDIF
ENDPROC

PROC RANGE_MP_END_COVERED_ROUND(RangeMP_Round & sRndInfo)
	CDEBUG2LN(DEBUG_SHOOTRANGE, "RANGE_MP_END_RANDOM_ROUND: Ending COVERED variant...")
	
	// Destroy the grids
	IF NOT IS_ENTITY_DEAD(sRndInfo.oMisc)
		DELETE_ENTITY(sRndInfo.oMisc)
	ENDIF
	IF NOT IS_ENTITY_DEAD(sRndInfo.oMisc2)
		DELETE_ENTITY(sRndInfo.oMisc2)
	ENDIF
	
	// Destroy all targets.
	INT index
	FOR index = 0 TO ENUM_TO_INT(CI_GOAL_END)
		IF NOT IS_ENTITY_DEAD(sRndInfo.oRoundObjects[index].oTarget)
			ENTITY_INDEX oBacking = GET_ENTITY_ATTACHED_TO(sRndInfo.oRoundObjects[index].oTarget)
			IF NOT IS_ENTITY_DEAD(oBacking)
				DELETE_ENTITY(oBacking)
			ENDIF
			DELETE_ENTITY(sRndInfo.oRoundObjects[index].oTarget)
		ENDIF
		
		IF NOT IS_ENTITY_DEAD(sRndInfo.oRoundObjects[index].oMisc)
			DELETE_ENTITY(sRndInfo.oRoundObjects[index].oMisc)
		ENDIF
		IF NOT IS_ENTITY_DEAD(sRndInfo.oRoundObjects[index].oTarget2)
			DELETE_ENTITY(sRndInfo.oRoundObjects[index].oTarget2)
		ENDIF
		sRndInfo.oRoundObjects[index].iFlags = 0
	ENDFOR
	
	// Debug widget cleanup.
//#IF IS_DEBUG_BUILD
//	CLEANUP_COVERED_ROUND_WIDGETS()
//#ENDIF
ENDPROC




