// RangeMP_Player.sch
USING "RangeMP_Round.sch"
USING "RangeMP_Leaderboard_lib.sch"

CONST_INT	MP_RANGE_MAX_PLAYERS			4		// Increased to 4 to compensate for SCTV
CONST_INT	BLUE_PLAYER_CLIENT_ID			1		// Blue starts on the right.
CONST_INT	RED_PLAYER_CLIENT_ID			0		// Red starts on the left.
CONST_FLOAT	MAX_SHOOTING_SKILL_INCREASE		10.0	// Maximum increase possible for shooting skill off one round
CONST_INT	RANGE_MAX_STORED_IMPACT_ARRAYS	2
CONST_INT	RANGE_MAX_STORED_IMPACT_COORDS	10
TWEAK_FLOAT	RANGE_IMPACT_COORD_SCALAR_X		0.118
TWEAK_FLOAT	RANGE_IMPACT_COORD_SCALAR_Y		0.195
TWEAK_FLOAT RANGE_IMPACT_COORD_MIN			-0.030		// +/-0.030 works for shotguns to keep the data on the target
TWEAK_FLOAT RANGE_IMPACT_COORD_MAX			0.030

ENUM RANGE_CLIENT_FLAGS	
	RANGE_MP_F_DoneWithMenuSelections 		= BIT0,
	RANGE_MP_F_CountdownDone 				= BIT1,
	RANGE_MP_F_CreatedRound					= BIT2,
	RANGE_MP_F_GivenWeapon					= BIT3,
	RANGE_MP_F_MenuTimeExpired				= BIT4,
	RANGE_MP_F_RoundStartUpdateDone			= BIT5,
	RANGE_MP_F_ProcessedPostRound			= BIT6,
	RANGE_MP_F_TutorialSkipped				= BIT7,
	RANGE_MP_F_WaitingOnScoreCard			= BIT8,
	RANGE_MP_F_QuitInTutorialWait			= BIT9,
	RANGE_MP_F_ClientIsPlaying				= BIT10,	// Used in the Participant Info Struct to gate out arrivals who haven't fully synced yet.
	RANGE_MP_F_IsSpectator					= BIT11,
	RANGE_MP_F_ContinueAfterVotePassing		= BIT12,
	RANGE_MP_F_CelebrationStarted			= BIT13,
	RANGE_MP_F_GusenbergUnlocked			= BIT14,
	RANGE_MP_F_IS_BLUE_PLAYER				= BIT29,
	RANGE_MP_F_IS_RED_PLAYER				= BIT30
ENDENUM

ENUM RANGE_SPECTATOR_CAM_STATE
	RSC_DRIFT_DOWN		= 0,
	RSC_DRIFT_RIGHT,
	RSC_TIGHTEN_ON_TARGETS,
	RSC_PLAYERS1,
	RSC_PLAYERS2,
	RSC_PLAYERS3
ENDENUM

FUNC STRING GET_STRING_FROM_RANGE_CLIENT_FLAG(RANGE_CLIENT_FLAGS eFlag)
	SWITCH eFlag
		CASE RANGE_MP_F_CountdownDone				RETURN "RANGE_MP_F_CountdownDone"
		CASE RANGE_MP_F_DoneWithMenuSelections		RETURN "RANGE_MP_F_DoneWithMenuSelections"
		CASE RANGE_MP_F_CreatedRound				RETURN "RANGE_MP_F_CreatedRound"
		CASE RANGE_MP_F_GivenWeapon					RETURN "RANGE_MP_F_GivenWeapon"
		CASE RANGE_MP_F_MenuTimeExpired				RETURN "RANGE_MP_F_MenuTimeExpired"
		CASE RANGE_MP_F_RoundStartUpdateDone		RETURN "RANGE_MP_F_RoundStartUpdateDone"
		CASE RANGE_MP_F_ProcessedPostRound			RETURN "RANGE_MP_F_ProcessedPostRound"
		CASE RANGE_MP_F_TutorialSkipped				RETURN "RANGE_MP_F_TutorialSkipped"
		CASE RANGE_MP_F_WaitingOnScoreCard			RETURN "RANGE_MP_F_WaitingOnScoreCard"
		CASE RANGE_MP_F_QuitInTutorialWait			RETURN "RANGE_MP_F_QuitInTutorialWait"
		CASE RANGE_MP_F_ClientIsPlaying				RETURN "RANGE_MP_F_ClientIsPlaying"
		CASE RANGE_MP_F_IsSpectator					RETURN "RANGE_MP_F_IsSpectator"
		CASE RANGE_MP_F_ContinueAfterVotePassing	RETURN "RANGE_MP_F_ContinueAfterVotePassing"
		CASE RANGE_MP_F_CelebrationStarted			RETURN "RANGE_MP_F_CelebrationStarted"
		CASE RANGE_MP_F_GusenbergUnlocked			RETURN "RANGE_MP_F_GusenbergUnlocked"
		CASE RANGE_MP_F_IS_BLUE_PLAYER				RETURN "RANGE_MP_F_IS_BLUE_PLAYER"
		CASE RANGE_MP_F_IS_RED_PLAYER				RETURN "RANGE_MP_F_IS_RED_PLAYER"
	ENDSWITCH
	RETURN "Unknown RANGE_CLIENT_FLAGS enum"
ENDFUNC

ENUM RANGE_TUT_FLAGS
	RTF_GRID_CREATED		= BIT0,
	RTF_RANDOM_CREATED		= BIT1,
	RTF_COVERED_CREATED		= BIT2
ENDENUM

// Local only!!!
ENUM RANGE_PLAYER_FLAGS
	RANGE_MP_F_TOOK_WEAPONS				= BIT0,
	RANGE_MP_F_WARNED_ABOUT_LEAVING		= BIT1,
	RANGE_MP_F_FORCED_RELOAD			= BIT2,
	RANGE_MP_F_SPECTATOR_LOCKSTEP		= BIT3,		// Spectators use this flag in place of changing state, state always keys off RED_PLAYER_CLIENT_ID
	RANGE_MP_F_SPECTATOR_MOVE_ON		= BIT4,
	RANGE_MP_F_MATCH_OVER				= BIT5,
	RANGE_MP_F_QUIT_SCREEN				= BIT6,
	RANGE_MP_F_CLEARED_COVERED_HELP1	= BIT7,
	RANGE_MP_F_SHOWED_COVERED_HELP2		= BIT8,
	RANGE_MP_F_SHOWED_BLOCKER_HELP		= BIT9,
	RANGE_MP_F_DONT_COUNT_SHOTS_FIRED	= BIT10,
	RANGE_MP_F_QUIT_CONFIRMED			= BIT11	
ENDENUM

FUNC STRING GET_STRING_FROM_RANGE_PLAYER_FLAG(RANGE_PLAYER_FLAGS eFlag)
	SWITCH eFlag
		CASE RANGE_MP_F_TOOK_WEAPONS			RETURN "RANGE_MP_F_TOOK_WEAPONS"
		CASE RANGE_MP_F_WARNED_ABOUT_LEAVING	RETURN "RANGE_MP_F_WARNED_ABOUT_LEAVING"
		CASE RANGE_MP_F_FORCED_RELOAD			RETURN "RANGE_MP_F_FORCED_RELOAD"
		CASE RANGE_MP_F_SPECTATOR_LOCKSTEP		RETURN "RANGE_MP_F_SPECTATOR_LOCKSTEP"
		CASE RANGE_MP_F_SPECTATOR_MOVE_ON		RETURN "RANGE_MP_F_SPECTATOR_MOVE_ON"
		CASE RANGE_MP_F_MATCH_OVER				RETURN "RANGE_MP_F_MATCH_OVER"
		CASE RANGE_MP_F_QUIT_SCREEN				RETURN "RANGE_MP_F_QUIT_SCREEN"
		CASE RANGE_MP_F_CLEARED_COVERED_HELP1	RETURN "RANGE_MP_F_CLEARED_COVERED_HELP1"
		CASE RANGE_MP_F_SHOWED_COVERED_HELP2	RETURN "RANGE_MP_F_SHOWED_COVERED_HELP2"
		CASE RANGE_MP_F_SHOWED_BLOCKER_HELP		RETURN "RANGE_MP_F_SHOWED_BLOCKER_HELP"
		CASE RANGE_MP_F_DONT_COUNT_SHOTS_FIRED	RETURN "RANGE_MP_F_DONT_COUNT_SHOTS_FIRED"
		CASE RANGE_MP_F_QUIT_CONFIRMED			RETURN "RANGE_MP_F_QUIT_CONFIRMED"
	ENDSWITCH
	RETURN "unknown RANGE_PLAYER_FLAGS"
ENDFUNC

STRUCT RangeMP_PlayerBD
	RANGE_MP_GAME_STATE 	eGameState							// Player's MP state
	RANGE_STATE  			eRangeState							// Player's shooting range state
	
	INT						iCurMenu							// Holds which menu we're actually on.
	
	INT						iCatElem							// Holds which elementss we're actually on.
	INT						iWeapElem
	INT						iChalElem						
	
	INT						iFlags								// Flags to send back and forth.
	PLAYER_INDEX			piCurPlayerID
	INT						iXPAward
	
	/// Notes on iTargetsField1 and iTargetsField2
	///     collectively represents up to 36 targets
	///     Covered Control Targets - SET means ORANGE (0-3 in iTargetsField1)
	///     Covered Targets			- Field1 for ORANGE, field2 for PURPLE, SET means IN. Separation between the two cutoff at COVERED_ROUND_FIELD_BREAK
	///     Grid Targets			- SET means ORANGE
	///     Random Targets			- Field1 for ORANGE, field2 for PURPLE, SET means DOWN
	///     iTargetsField2 bit 31 is a control for the setup funcs. When set it will cause the targets to set up according to the bits passed in.
	INT						iTargetsField1, iTargetsField2			
	
	RoundDescription		sRoundDesc
	
	BOOL					bRematchEligible = FALSE
	
	VECTOR					vImpactCoordArray[ RANGE_MAX_STORED_IMPACT_ARRAYS ][ RANGE_MAX_STORED_IMPACT_COORDS ]
	INT						iImpactCoordArrayCounterOrange
	INT						iImpactCoordArrayCounterPurple
ENDSTRUCT

STRUCT RangeMP_PlayerWeaponsStored
	WEAPON_TYPE		eSlotWeapon[NUM_PLAYER_PED_WEAPON_SLOTS]
	INT				iSlotAmmo[NUM_PLAYER_PED_WEAPON_SLOTS]
	INT				iSlotMods[NUM_PLAYER_PED_WEAPON_SLOTS]
	WEAPON_INFO 	sDLCWeaponInfo[NUMBER_OF_DLC_WEAPONS_MP]
ENDSTRUCT

STRUCT RangeMP_PredictData
	CHALLENGE_INDEX eChallenge
	INT iWin
	INT iShotsFiredThisRound
	INT iShotsHitThisRound 
	WEAPON_TYPE eWeaponGiven
ENDSTRUCT

ENUM RANGE_IMPACT_COORD_ARRAY
	RICA_ORANGE_IMPACTS		=	0,
	RICA_PURPLE_IMPACTS		=	1
ENDENUM

STRUCT RangeMP_PlayerData
	structTimer				genericTimer
	WEAPON_TYPE				eWeaponGiven
	
	BOOL					bHadThisRoundsWeapon
	INT						iPrevAmmo
	
	INT						iShotsFired
	INT						iShotsFiredThisRound
	INT						iShotsHit
	INT						iShotsHitThisRound
	INT						iShotsMissedThisRound
	
	INT						iFlags
	
	INT						iTutorialFlags
	
	CAMERA_INDEX			camSpectator
	INT						iCamStamp
	RANGE_SPECTATOR_CAM_STATE eRSCState
	
	PED_VARIATION_STRUCT 	sVariations
	
	
	// Weapons stored/restored
	RangeMP_PlayerWeaponsStored	storedWeapons
	
	VECTOR					vPlayerStartPos
	FLOAT					fPlayerStartHead
	FLOAT 					fPlayerHeadSkewed
	
	FMMC_EOM_DETAILS		sFMMC_EOMDetails
	
	BOOL					bPredictionDone = TRUE		// TRUE, so we don't predict on the first go through.
	
	RangeMP_PredictData		sPredict
ENDSTRUCT



//*******************************************************************************************************************
//***************************************** ACCESSORS AND MUTATORS **************************************************
//*******************************************************************************************************************

/// PURPOSE:
///    Accessor for impactCoordArrayCounter, this is an indexer into the orange array
PROC SET_RANGE_IMPACT_COORD_ARRAY_COUNTER_ORANGE( RangeMP_PlayerBD & playerBD, INT iNewArrayCounter )
	IF iNewArrayCounter < 0 OR iNewArrayCounter >= RANGE_MAX_STORED_IMPACT_COORDS
		iNewArrayCounter = 0
		CERRORLN( DEBUG_SHOOTRANGE, "SET_RANGE_IMPACT_COORD_ARRAY_COUNTER_ORANGE error, iNewArrayCounter = ", iNewArrayCounter, ", Contact Rob Pearsall" )
		DEBUG_PRINTCALLSTACK()
	ENDIF
	CDEBUG3LN( DEBUG_SHOOTRANGE, "SET_RANGE_IMPACT_COORD_ARRAY_COUNTER_ORANGE playerBD.iImpactCoordArrayCounter = ", iNewArrayCounter )
	playerBD.iImpactCoordArrayCounterOrange = iNewArrayCounter
ENDPROC
/// PURPOSE:
///    Accessor. Gets the indexer into the orange array
FUNC INT GET_RANGE_IMPACT_COORD_ARRAY_COUNTER_ORANGE( RangeMP_PlayerBD & playerBD )
	RETURN playerBD.iImpactCoordArrayCounterOrange
ENDFUNC
/// PURPOSE:
///    Accessor for impactCoordArrayCounter, this is an indexer into the purple array
PROC SET_RANGE_IMPACT_COORD_ARRAY_COUNTER_PURPLE( RangeMP_PlayerBD & playerBD, INT iNewArrayCounter )
	IF iNewArrayCounter < 0 OR iNewArrayCounter >= RANGE_MAX_STORED_IMPACT_COORDS
		iNewArrayCounter = 0
		CERRORLN( DEBUG_SHOOTRANGE, "SET_RANGE_IMPACT_COORD_ARRAY_COUNTER_PURPLE error, iNewArrayCounter = ", iNewArrayCounter, ", Contact Rob Pearsall" )
		DEBUG_PRINTCALLSTACK()
	ENDIF
	CDEBUG3LN( DEBUG_SHOOTRANGE, "SET_RANGE_IMPACT_COORD_ARRAY_COUNTER_PURPLE playerBD.iImpactCoordArrayCounter = ", iNewArrayCounter )
	playerBD.iImpactCoordArrayCounterPurple = iNewArrayCounter
ENDPROC
/// PURPOSE:
///    Accessor. Gets the indexer into the purple array
FUNC INT GET_RANGE_IMPACT_COORD_ARRAY_COUNTER_PURPLE( RangeMP_PlayerBD & playerBD )
	RETURN playerBD.iImpactCoordArrayCounterPurple
ENDFUNC

PROC CLEAR_RANGE_PLAYER_BD_TARGET_BITS(RangeMP_PlayerBD &playerBD)
	playerBD.iTargetsField1 = 0
	playerBD.iTargetsField2 = 0
ENDPROC

PROC SET_RANGE_PLAYER_CAM_STAMP(RangeMP_PlayerData &thisPlayer, INT iNewStamp)
	thisPlayer.iCamStamp = iNewStamp
ENDPROC

FUNC INT GET_RANGE_PLAYER_CAM_STAMP(RangeMP_PlayerData &thisPlayer)
	RETURN thisPlayer.iCamStamp
ENDFUNC

PROC SET_RANGE_TUTORIAL_FLAG(RangeMP_PlayerData &thisPlayer, RANGE_TUT_FLAGS eFlag)
	IF NOT IS_BITMASK_AS_ENUM_SET(thisPlayer.iTutorialFlags, eFlag)
		SET_BITMASK_AS_ENUM(thisPlayer.iTutorialFlags, eFlag)
	ENDIF
ENDPROC

FUNC BOOL IS_RANGE_TUTORIAL_FLAG_SET(RangeMP_PlayerData &thisPlayer, RANGE_TUT_FLAGS eFlag)
	RETURN IS_BITMASK_AS_ENUM_SET(thisPlayer.iTutorialFlags, eFlag)
ENDFUNC

FUNC RANGE_MP_GAME_STATE GET_CLIENT_GAME_STATE(RangeMP_PlayerBD & sPlayerData)
	RETURN sPlayerData.eGameState
ENDFUNC

PROC SET_CLIENT_GAME_STATE(RangeMP_PlayerBD & sPlayerData, RANGE_MP_GAME_STATE eGameState)
	sPlayerData.eGameState = eGameState
ENDPROC

FUNC RANGE_STATE GET_CLIENT_RANGE_STATE(RangeMP_PlayerBD & sPlayerData)
	RETURN sPlayerData.eRangeState
ENDFUNC

PROC SET_CLIENT_RANGE_STATE(RangeMP_PlayerBD & sPlayerData, RANGE_STATE eRangeState)
	sPlayerData.eRangeState = eRangeState
	CDEBUG2LN(DEBUG_SHOOTRANGE, "Changing Client Range State to ", GET_STRING_FROM_RANGE_STATE(eRangeState), " :: ", eRangeState)
ENDPROC

FUNC BOOL IS_CLIENT_FLAG_SET(RangeMP_PlayerBD & sPlayerBD, RANGE_CLIENT_FLAGS eFlagToCheck)
	RETURN (sPlayerBD.iFlags & ENUM_TO_INT(eFlagToCheck)) <> 0
ENDFUNC

PROC SET_CLIENT_FLAG(RangeMP_PlayerBD & sPlayerBD, RANGE_CLIENT_FLAGS eFlagToSet)
	CDEBUG2LN(DEBUG_SHOOTRANGE, "SET_CLIENT_FLAG :: setting ", GET_STRING_FROM_RANGE_CLIENT_FLAG(eFlagToSet))
	sPlayerBD.iFlags |= ENUM_TO_INT(eFlagToSet)
ENDPROC

PROC CLEAR_CLIENT_FLAG(RangeMP_PlayerBD & sPlayerBD, RANGE_CLIENT_FLAGS eFlagToClear)
	CDEBUG2LN(DEBUG_SHOOTRANGE, "CLEAR_CLIENT_FLAG :: setting ", GET_STRING_FROM_RANGE_CLIENT_FLAG(eFlagToClear))
	sPlayerBD.iFlags -= sPlayerBD.iFlags & ENUM_TO_INT(eFlagToClear)
ENDPROC

PROC SET_PLAYER_FLAG(RangeMP_PlayerData & sPlayerData, RANGE_PLAYER_FLAGS eFlagToSet)
	CDEBUG2LN(DEBUG_SHOOTRANGE, "SET_PLAYER_FLAG setting ", GET_STRING_FROM_RANGE_PLAYER_FLAG(eFlagToSet))
	sPlayerData.iFlags |= ENUM_TO_INT(eFlagToSet)
ENDPROC

FUNC BOOL IS_PLAYER_FLAG_SET(RangeMP_PlayerData & sPlayerData, RANGE_PLAYER_FLAGS eFlagToCheck)
	RETURN (sPlayerData.iFlags & ENUM_TO_INT(eFlagToCheck)) <> 0
ENDFUNC
PROC CLEAR_PLAYER_FLAG(RangeMP_PlayerData & sPlayerData, RANGE_PLAYER_FLAGS eFlagToClear)
	CDEBUG2LN(DEBUG_SHOOTRANGE, "CLEAR_PLAYER_FLAG clearing ", GET_STRING_FROM_RANGE_PLAYER_FLAG(eFlagToClear))
	sPlayerData.iFlags -= sPlayerData.iFlags & ENUM_TO_INT(eFlagToClear)
ENDPROC

/// PURPOSE:
///    Checks if any client has the given flag set. Also make sure that
/// PARAMS:
///    sPlayerBD - The client data array
///    eFlagToCheck - The flag you want to check each client for
///    bCheckSpectators - Ignores spectators by default
/// RETURNS:
///    TRUE if any client has the flag checked. FALSE otherwise.
FUNC BOOL RANGE_IS_ANY_CLIENT_FLAG_SET(RangeMP_PlayerBD & sPlayerBD[], RANGE_CLIENT_FLAGS eFlagToCheck, BOOL bCheckSpectators = TRUE)

	INT i
	PARTICIPANT_INDEX participantIndex
	
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
	
		participantIndex = INT_TO_PARTICIPANTINDEX(i)
		
		IF NETWORK_IS_PARTICIPANT_ACTIVE(participantIndex)
		
			IF IS_CLIENT_FLAG_SET(sPlayerBD[i], RANGE_MP_F_ClientIsPlaying) OR bCheckSpectators
			
				IF IS_CLIENT_FLAG_SET(sPlayerBD[i], eFlagToCheck)
					RETURN TRUE
				ENDIF
				
			ENDIF
			
		ENDIF
		
	ENDREPEAT
	
	RETURN FALSE
	
ENDFUNC







