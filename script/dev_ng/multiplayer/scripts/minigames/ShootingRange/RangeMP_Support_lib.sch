// RangeMP_Support_lib.sch
USING "RangeMP_Player.sch"
USING "RangeMP_Server.sch"
USING "RangeMP_Leaderboard_lib.sch"
USING "fmmc_mp_setup_mission.sch"

PROC RANGE_CLEAR_ENDED_MISSION_AT_LEADERBOARD_VOTE_OR_TIME_OUT()
	CDEBUG2LN(DEBUG_SHOOTRANGE, "RANGE_CLEAR_ENDED_MISSION_AT_LEADERBOARD_VOTE_OR_TIME_OUT called")
	CLEAR_ENDED_MISSION_AT_LEADERBOARD_VOTE_OR_TIME_OUT()
ENDPROC

PROC RANGE_SET_ENDED_MISSION_AT_LEADERBOARD_VOTE_OR_TIME_OUT()
	CDEBUG2LN(DEBUG_SHOOTRANGE, "RANGE_SET_ENDED_MISSION_AT_LEADERBOARD_VOTE_OR_TIME_OUT called")
	SET_ENDED_MISSION_AT_LEADERBOARD_VOTE_OR_TIME_OUT()
ENDPROC

PROC INCREASE_RANGE_SHOOTING_STAT()
	INCREMENT_MP_INT_CHARACTER_STAT(MP_STAT_SHOOTING_ABILITY)
	CDEBUG2LN(DEBUG_SHOOTRANGE, "GET_MP_INT_CHARACTER_STAT(MP_STAT_SHOOTING_ABILITY, PARTICIPANT_ID_TO_INT())", GET_MP_INT_CHARACTER_STAT(MP_STAT_SHOOTING_ABILITY, PARTICIPANT_ID_TO_INT()))
ENDPROC

FUNC INT GET_RANGE_MP_SHOOTING_ABILITY()
	INT iReturn = -1
	iReturn = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].scoreData.iShootingSkill//GET_MP_INT_CHARACTER_STAT(MP_STAT_SHOOTING_ABILITY, PARTICIPANT_ID_TO_INT())
	CDEBUG2LN(DEBUG_SHOOTRANGE, "GET_RANGE_MP_SHOOTING_ABILITY :: MP_STAT_SHOOTING_ABILITY=", iReturn)
	RETURN iReturn
ENDFUNC

PROC SET_RANGE_MP_SEEN_TUTORIAL(BOOL bSeen)
	SET_MP_BOOL_CHARACTER_STAT(MP_STAT_SHOOTINGRANGE_SEEN_TUT, bSeen)
ENDPROC

FUNC BOOL GET_RANGE_MP_SEEN_TUTORIAL()
	RETURN GET_MP_BOOL_CHARACTER_STAT(MP_STAT_SHOOTINGRANGE_SEEN_TUT)
ENDFUNC

PROC INITIALIZE_RANGE_PARTICIPANT_INFO( RangeMP_ParticipantInfo & info )
	
	INT i = 0
	FOR i = 0 TO ( k_MaxShooters - 1 )
		info.iShooters[i] = k_InvalidParticipantID
	ENDFOR	
	
	i = 0
	FOR i = 0 TO ( k_MaxSpectators - 1 )
		info.iSpectators[i] = k_InvalidParticipantID
	ENDFOR	
	
	i = 0
	FOR i = 0 TO ( k_MaxSCTV - 1 )
		info.iSCTV[i] = k_InvalidParticipantID
	ENDFOR	
	
	info.iShooterCount = 0
	info.iSpectatorCount = 0
	info.iSCTVCount = 0

ENDPROC

PROC PRINT_RANGE_PARTICIPANT_INFO( RangeMP_ParticipantInfo & info )
	CDEBUG2LN(DEBUG_SHOOTRANGE, "PRINT_RANGE_PARTICIPANT_INFO" )
	
	FLOAT fDisplayAtX = 0.1
	FLOAT fDisplayAtY = 0.7
	FLOAT fDisplayAdd = 0.0125
	TEXT_LABEL_63 texDisplay
	
	INT i = 0
	FOR i = 0 TO ( k_MaxShooters - 1 )
		IF info.iShooters[i] != k_InvalidParticipantID
			texDisplay = "Shooter "
			texDisplay += i
			texDisplay += " : "
			texDisplay += GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(info.iShooters[i])))
			CDEBUG3LN(DEBUG_SHOOTRANGE, texDisplay)
			DRAW_DEBUG_TEXT_2D( texDisplay, (<< fDisplayAtX, fDisplayAtY, 0.0 >>) )
			fDisplayAtY += fDisplayAdd
		ENDIF
	ENDFOR	
	
	i = 0
	FOR i = 0 TO ( k_MaxSpectators - 1 )
		IF info.iSpectators[i] != k_InvalidParticipantID
			texDisplay = "Spectator "
			texDisplay += i
			texDisplay += " : "
			texDisplay += GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(info.iSpectators[i])))
			CDEBUG3LN(DEBUG_SHOOTRANGE, texDisplay)
			DRAW_DEBUG_TEXT_2D( texDisplay, (<< fDisplayAtX, fDisplayAtY, 0.0 >>) )
			fDisplayAtY += fDisplayAdd
		ENDIF
	ENDFOR	
	
	i = 0
	FOR i = 0 TO ( k_MaxSCTV - 1 )
		IF info.iSCTV[i] != k_InvalidParticipantID
			texDisplay = "SCTV "
			texDisplay += i
			texDisplay += " : "
			texDisplay += GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(info.iSCTV[i])))
			CDEBUG3LN(DEBUG_SHOOTRANGE, texDisplay)
			DRAW_DEBUG_TEXT_2D( texDisplay, (<< fDisplayAtX, fDisplayAtY, 0.0 >>) )
			fDisplayAtY += fDisplayAdd
		ENDIF
	ENDFOR	
	
ENDPROC

PROC UPDATE_RANGE_PARTICIPANT_INFO( RangeMP_PlayerBD & sPlayerBD[], RangeMP_ParticipantInfo & info )
	
	INITIALIZE_RANGE_PARTICIPANT_INFO( info )

	INT i = 0
	PARTICIPANT_INDEX participantID
	PLAYER_INDEX playerID
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		FOR i = 0 TO ( NETWORK_GET_MAX_NUM_PARTICIPANTS() - 1 )
			participantID = INT_TO_PARTICIPANTINDEX( i )
			IF NETWORK_IS_PARTICIPANT_ACTIVE( participantID )
				playerID = NETWORK_GET_PLAYER_INDEX( participantID )
				IF NETWORK_IS_PLAYER_ACTIVE( playerID )
					IF IS_PLAYER_SCTV( playerID )
						info.iSCTV[info.iSCTVCount] = i
						info.iSCTVCount++
					ELIF IS_CLIENT_FLAG_SET( sPlayerBD[i], RANGE_MP_F_ClientIsPlaying )
						info.iShooters[info.iShooterCount] = i
						info.iShooterCount++
					ELSE
						info.iSpectators[info.iSpectatorCount] = i
						info.iSpectatorCount++
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
	ELSE
		CERRORLN(DEBUG_SHOOTRANGE, "UPDATE_RANGE_PARTICIPANT_INFO :: NOT NETWORK_IS_GAME_IN_PROGRESS()")
	ENDIF
	
//	PRINT_RANGE_PARTICIPANT_INFO( info )
	
ENDPROC

FUNC BOOL RANGE_IS_PARTICIPANT_INT_A_SHOOTER( RangeMP_ParticipantInfo & info, INT participantID )

	IF NOT NETWORK_IS_PARTICIPANT_ACTIVE( INT_TO_PARTICIPANTINDEX( participantID ) )
		RETURN FALSE
	ENDIF	

	INT i = 0
	FOR i = 0 TO ( k_MaxShooters - 1 )
		IF info.iShooters[i] = participantID
			RETURN TRUE
		ENDIF
	ENDFOR	
	
	RETURN FALSE

ENDFUNC

FUNC BOOL RANGE_IS_PARTICIPANT_A_SHOOTER( RangeMP_ParticipantInfo & info, PARTICIPANT_INDEX participantID )

	IF NOT NETWORK_IS_PARTICIPANT_ACTIVE( participantID )
		RETURN FALSE
	ENDIF	
		
	RETURN RANGE_IS_PARTICIPANT_INT_A_SHOOTER( info, NATIVE_TO_INT( participantID ) )

ENDFUNC

FUNC BOOL RANGE_IS_PLAYER_A_SHOOTER( RangeMP_ParticipantInfo & info, PLAYER_INDEX playerID )
	
	IF NOT NETWORK_IS_PLAYER_ACTIVE( playerID )
		RETURN FALSE
	ENDIF	
	
	IF NOT NETWORK_IS_PLAYER_A_PARTICIPANT( playerID )
		RETURN FALSE
	ENDIF
		
	RETURN RANGE_IS_PARTICIPANT_A_SHOOTER( info, NETWORK_GET_PARTICIPANT_INDEX( playerID ) )

ENDFUNC

FUNC INT RANGE_GET_NUM_SHOOTERS( RangeMP_ParticipantInfo & info )
	RETURN info.iShooterCount
ENDFUNC

FUNC INT RANGE_GET_OPPOSING_SHOOTER( RangeMP_ParticipantInfo & info )
	IF NOT RANGE_IS_PARTICIPANT_INT_A_SHOOTER( info, PARTICIPANT_ID_TO_INT() )
		RETURN info.iShooters[0]
	ENDIF

	INT i = 0
	FOR i = 0 TO ( k_MaxShooters - 1 )
		IF info.iShooters[i] != PARTICIPANT_ID_TO_INT()
			AND info.iShooters[i] != k_InvalidParticipantID
			RETURN info.iShooters[i]
		ENDIF
	ENDFOR	
	
	RETURN info.iShooters[0]
	
ENDFUNC

FUNC INT RANGE_GET_OTHER_SHOOTER( INT iOurClientID, RangeMP_ParticipantInfo & info )
	IF NOT RANGE_IS_PARTICIPANT_INT_A_SHOOTER( info, iOurClientID )
		RETURN info.iShooters[0]
	ENDIF

	INT i = 0
	FOR i = 0 TO ( k_MaxShooters - 1 )
		IF info.iShooters[i] != iOurClientID
			AND info.iShooters[i] != k_InvalidParticipantID
			RETURN info.iShooters[i]
		ENDIF
	ENDFOR	
	
	RETURN info.iShooters[0]
	
ENDFUNC

FUNC BOOL RANGE_IS_PARTICIPANT_INT_A_SPECTATOR( RangeMP_ParticipantInfo & info, INT participantID )

	IF NOT NETWORK_IS_PARTICIPANT_ACTIVE( INT_TO_PARTICIPANTINDEX( participantID ) )
		RETURN FALSE
	ENDIF	

	INT i = 0
	FOR i = 0 TO ( k_MaxSpectators - 1 )
		IF info.iSpectators[i] = participantID
			RETURN TRUE
		ENDIF
	ENDFOR	
	
	RETURN FALSE

ENDFUNC

FUNC BOOL RANGE_IS_PARTICIPANT_A_SPECTATOR( RangeMP_ParticipantInfo & info, PARTICIPANT_INDEX participantID )

	IF NOT NETWORK_IS_PARTICIPANT_ACTIVE( participantID )
		RETURN FALSE
	ENDIF	
		
	RETURN RANGE_IS_PARTICIPANT_INT_A_SPECTATOR( info, NATIVE_TO_INT( participantID ) )

ENDFUNC

FUNC BOOL RANGE_IS_PLAYER_A_SPECTATOR( RangeMP_ParticipantInfo & info, PLAYER_INDEX playerID )
	
	IF NOT NETWORK_IS_PLAYER_ACTIVE( playerID )
		RETURN FALSE
	ENDIF	
	
	IF NOT NETWORK_IS_PLAYER_A_PARTICIPANT( playerID )
		RETURN FALSE
	ENDIF
		
	RETURN RANGE_IS_PARTICIPANT_A_SPECTATOR( info, NETWORK_GET_PARTICIPANT_INDEX( playerID ) )

ENDFUNC

FUNC INT RANGE_GET_NUM_SPECTATORS( RangeMP_ParticipantInfo & info )
	RETURN info.iSpectatorCount
ENDFUNC

FUNC BOOL RANGE_IS_PARTICIPANT_INT_SCTV( RangeMP_ParticipantInfo & info, INT participantID )

	IF NOT NETWORK_IS_PARTICIPANT_ACTIVE( INT_TO_PARTICIPANTINDEX( participantID ) )
		RETURN FALSE
	ENDIF	

	INT i = 0
	FOR i = 0 TO ( k_MaxSCTV - 1 )
		IF info.iSCTV[i] = participantID
			RETURN TRUE
		ENDIF
	ENDFOR	
	
	RETURN FALSE

ENDFUNC

FUNC BOOL RANGE_IS_PARTICIPANT_SCTV( RangeMP_ParticipantInfo & info, PARTICIPANT_INDEX participantID )

	IF NOT NETWORK_IS_PARTICIPANT_ACTIVE( participantID )
		RETURN FALSE
	ENDIF	
		
	RETURN RANGE_IS_PARTICIPANT_INT_SCTV( info, NATIVE_TO_INT( participantID ) )

ENDFUNC

FUNC BOOL RANGE_IS_PLAYER_SCTV( RangeMP_ParticipantInfo & info, PLAYER_INDEX playerID )
	
	IF NOT NETWORK_IS_PLAYER_ACTIVE( playerID )
		RETURN FALSE
	ENDIF	
	
	IF NOT NETWORK_IS_PLAYER_A_PARTICIPANT( playerID )
		RETURN FALSE
	ENDIF
		
	RETURN RANGE_IS_PARTICIPANT_SCTV( info, NETWORK_GET_PARTICIPANT_INDEX( playerID ) )

ENDFUNC

FUNC INT RANGE_GET_NUM_SCTV( RangeMP_ParticipantInfo & info )
	RETURN info.iSCTVCount
ENDFUNC

PROC UPDATE_SPECTATOR_VISIBLITY( RangeMP_ParticipantInfo & info )

	INT i = 0
	FOR i = 0 TO ( k_MaxSpectators - 1 )
		IF info.iSpectators[i] != k_InvalidParticipantID
			SET_PLAYER_INVISIBLE_LOCALLY(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(info.iSpectators[i])))
		ENDIF
	ENDFOR	
	
ENDPROC

/// PURPOSE:
///    If the player has found his way underground, snap him up!
PROC RANGEMP_EMERGENCY_TELEPORT(RangeMP_PlayerData & sPlayerData)
	IF NOT IS_ENTITY_DEAD(playerPed)
		VECTOR vClientPos = GET_ENTITY_COORDS(playerPed)
		
		IF (vClientPos.z < 28.0)
			SET_ENTITY_COORDS(playerPed, sPlayerData.vPlayerStartPos)
			SET_ENTITY_HEADING(playerPed, sPlayerData.fPlayerStartHead)
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
		ENDIF	
	ENDIF
ENDPROC


// Set the camera heading to look at the range.
PROC RANGEMP_SET_PLAYER_CAM_DOWNRANGE(RangeMP_PlayerData & sPlayerData, BOOL bSkewed=FALSE)
	FLOAT fSkewed = sPlayerData.fPlayerHeadSkewed
	FLOAT fHeading = PICK_FLOAT(bSkewed, fSkewed, sPlayerData.fPlayerStartHead)
	IF NOT IS_ENTITY_DEAD(playerPed)
//		FLOAT fPlayerHead = GET_ENTITY_HEADING(playerPed)
		SET_GAMEPLAY_CAM_RELATIVE_HEADING()
		SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
		SET_ENTITY_HEADING(playerPed, fHeading)
	ENDIF
	CDEBUG2LN(DEBUG_SHOOTRANGE, "RANGEMP_SET_PLAYER_CAM_DOWNRANGE :: fHeading=", fHeading, ", bSkewed=", PICK_STRING(bSkewed, "TRUE", "FALSE"))
ENDPROC

USING "player_ped_public.sch"

/// PURPOSE:
///    Stores all the weapons that the player currently has.
/// PARAMS:
///    bRemove - Should we also remove the weapons?
PROC RANGEMP_STORE_PLAYER_WEAPONS(RangeMP_PlayerData & sPlayerData, BOOL bRemove = FALSE)
	CDEBUG2LN( DEBUG_SHOOTRANGE, "RANGEMP_STORE_PLAYER_WEAPONS called" )
	IF IS_ENTITY_DEAD(playerPed)
		CWARNINGLN(DEBUG_SHOOTRANGE, "Cannot store player weapons. Player not alive.")
		EXIT
	ENDIF
	
	INT i, j, iClipAmmo
	WEAPONCOMPONENT_TYPE weaponComp
	
	FOR i = 0 TO (NUM_PLAYER_PED_WEAPON_SLOTS-1)
		// Grab the weapon type and reset ammo/mod counts.
		WEAPON_SLOT ePedWepFromSlot = GET_PLAYER_PED_WEAPON_SLOT_FROM_INT(i)
		IF ePedWepFromSlot <> WEAPONSLOT_INVALID
	       	sPlayerData.storedWeapons.eSlotWeapon[i] = GET_PED_WEAPONTYPE_IN_SLOT(playerPed, ePedWepFromSlot)
			sPlayerData.storedWeapons.iSlotAmmo[i] = 0
			sPlayerData.storedWeapons.iSlotMods[i] = 0
		ENDIF
		
		// Grab the ammo/mod counts for valid items.
		IF (sPlayerData.storedWeapons.eSlotWeapon[i] != WEAPONTYPE_INVALID) AND (sPlayerData.storedWeapons.eSlotWeapon[i] != WEAPONTYPE_UNARMED) AND HAS_PED_GOT_WEAPON( PLAYER_PED_ID(), sPlayerData.storedWeapons.eSlotWeapon[i] )
        	sPlayerData.storedWeapons.iSlotAmmo[i] = GET_AMMO_IN_PED_WEAPON(playerPed, sPlayerData.storedWeapons.eSlotWeapon[i])
			
			AMMO_TYPE eAmmoType = GET_GUNCLUB_AMMO_TYPE_FROM_PLAYER(sPlayerData.storedWeapons.eSlotWeapon[i])
			IF IS_GUNCLUB_AMMO_TYPE_SPECIAL(eAmmoType)
				sPlayerData.storedWeapons.iSlotAmmo[i] = GET_PED_AMMO_BY_TYPE(PLAYER_PED_ID(), eAmmoType)
			ENDIF
			
			GET_AMMO_IN_CLIP(playerPed, sPlayerData.storedWeapons.eSlotWeapon[i], iClipAmmo)
			sPlayerData.storedWeapons.iSlotAmmo[i] += iClipAmmo
			CDEBUG2LN(DEBUG_SHOOTRANGE, "sPlayerData.storedWeapons.eSlotWeapon[", GET_WEAPON_NAME( sPlayerData.storedWeapons.eSlotWeapon[i] ), "] ammo=", sPlayerData.storedWeapons.iSlotAmmo[i], ", i=", i)
			
			// Go through all mods, and store them.
			j = 0
			weaponComp = GET_PLAYER_PED_WEAPON_COMP_FROM_INT(sPlayerData.storedWeapons.eSlotWeapon[i], j)
			WHILE (weaponComp != WEAPONCOMPONENT_INVALID)
				IF HAS_PED_GOT_WEAPON_COMPONENT(playerPed, sPlayerData.storedWeapons.eSlotWeapon[i], weaponComp)
					SET_BIT(sPlayerData.storedWeapons.iSlotMods[i], j)
					REMOVE_WEAPON_COMPONENT_FROM_PED(playerPed, sPlayerData.storedWeapons.eSlotWeapon[i], weaponComp)
					REMOVE_SECONDARY_WEAPON_COMPONENT_FROM_PED(playerPed, sPlayerData.storedWeapons.eSlotWeapon[i], weaponComp)
				ENDIF
				j++
				weaponComp = GET_PLAYER_PED_WEAPON_COMP_FROM_INT(sPlayerData.storedWeapons.eSlotWeapon[i], j)
			ENDWHILE
		ENDIF
    ENDFOR
	
	// Now go through the DLC weapons
	INT iDLCIndex
	INT iDLCWeapons = GET_NUM_DLC_WEAPONS()
	scrShopWeaponData weaponData
	scrShopWeaponComponentData compData
	WEAPON_INFO newValue
	INT iAvailableComp
	
	REPEAT iDLCWeapons iDLCIndex
		IF GET_DLC_WEAPON_DATA(iDLCIndex, weaponData)
			IF NOT IS_CONTENT_ITEM_LOCKED(weaponData.m_lockHash)
				// Grab the weapon type and reset ammo/mod counts
				newValue.eWeaponType = INT_TO_ENUM(WEAPON_TYPE, weaponData.m_nameHash)
				newValue.iAmmoCount = 0
				newValue.iModsAsBitfield = 0
				newValue.iTint = 0
				newValue.iCamo = 0
				
				IF HAS_PED_GOT_WEAPON( PLAYER_PED_ID(), newValue.eWeaponType )
				
					// Grab the ammo/mod counts
					newValue.iAmmoCount = GET_AMMO_IN_PED_WEAPON(playerPed, newValue.eWeaponType)
					
					// Store MK2 ammo type
					AMMO_TYPE eAmmoType = GET_GUNCLUB_AMMO_TYPE_FROM_PLAYER(newValue.eWeaponType)
					IF IS_GUNCLUB_AMMO_TYPE_SPECIAL(eAmmoType)
						newValue.iAmmoCount = GET_PED_AMMO_BY_TYPE(PLAYER_PED_ID(), eAmmoType)
					ENDIF
					
					CDEBUG2LN(DEBUG_SHOOTRANGE, "...weapon ", GET_WEAPON_NAME(newValue.eWeaponType), " has ", newValue.iAmmoCount, " ammo")
					
					// Store the tints
					IF HAS_PED_GOT_WEAPON(playerPed, newValue.eWeaponType)
						newValue.iTint = GET_PED_WEAPON_TINT_INDEX(playerPed, newValue.eWeaponType)
						newValue.iCamo = GET_PED_WEAPON_CAMO_INDEX(playerPed, newValue.eWeaponType)
					ENDIF
					
					// Fix for code returning -1 to signal infinite ammo.
					IF newValue.iAmmoCount = -1	
						IF NOT GET_MAX_AMMO(playerPed,newValue.eWeaponType, newValue.iAmmoCount)
							newValue.iAmmoCount = 0
						ENDIF
						CDEBUG2LN(DEBUG_SHOOTRANGE, "....infinite ammo detected, changing to ", newValue.iAmmoCount)
					ENDIF
					
					sPlayerData.storedWeapons.sDLCWeaponInfo[iDLCIndex].iAmmoCount = newValue.iAmmoCount
					
					iAvailableComp = 0
					REPEAT GET_NUM_DLC_WEAPON_COMPONENTS(iDLCIndex) j
						IF GET_DLC_WEAPON_COMPONENT_DATA(iDLCIndex, j, compData)
							IF NOT IGNORE_DLC_WEAPON_COMPONENT(INT_TO_ENUM(WEAPONCOMPONENT_TYPE, compData.m_componentName))
								IF HAS_PED_GOT_WEAPON_COMPONENT(playerPed, newValue.eWeaponType, INT_TO_ENUM(WEAPONCOMPONENT_TYPE, compData.m_componentName))
									SET_BIT(newValue.iModsAsBitfield, iAvailableComp)
								ENDIF
								iAvailableComp++
							ENDIF
						ENDIF
					ENDREPEAT
					
					// update stored info for this weapon slot
					sPlayerData.storedWeapons.sDLCWeaponInfo[iDLCIndex] = newValue
					CDEBUG2LN( DEBUG_SHOOTRANGE, "sPlayerData.storedWeapons.sDLCWeaponInfo[", iDLCIndex, "] = ", GET_WEAPON_NAME( sPlayerData.storedWeapons.sDLCWeaponInfo[iDLCIndex].eWeaponType ) )
					
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF bRemove
		REMOVE_ALL_PED_WEAPONS(playerPed)
		CDEBUG2LN(DEBUG_SHOOTRANGE, "RANGEMP_STORE_PLAYER_WEAPONS :: bRemove=TRUE, REMOVE_ALL_PED_WEAPONS(playerPed)")
	ENDIF
	
	CDEBUG2LN(DEBUG_SHOOTRANGE, "Stored player weapons. Flagging player.")
	SET_PLAYER_FLAG(sPlayerData, RANGE_MP_F_TOOK_WEAPONS)
ENDPROC

/// PURPOSE:
///    Gives the player back all of the weapons he had when he started the range, complete with that ammo state as well.
PROC RANGEMP_RESTORE_PLAYER_WEAPONS(RangeMP_PlayerData & sPlayerData)
	CDEBUG2LN( DEBUG_SHOOTRANGE, "RANGEMP_RESTORE_PLAYER_WEAPONS called" )
	IF IS_ENTITY_DEAD(playerPed)
		CWARNINGLN(DEBUG_SHOOTRANGE, "Cannot restore player weapons. Player not alive.")
		EXIT
	ENDIF
	
	IF NOT IS_PLAYER_FLAG_SET(sPlayerData, RANGE_MP_F_TOOK_WEAPONS)
		CWARNINGLN(DEBUG_SHOOTRANGE, "Cannot restore player weapons. Never took them.")
		EXIT
	ENDIF
	
	REMOVE_ALL_PED_WEAPONS(playerPed)
	
	INT ammoStored
	WEAPON_TYPE weaponStored
	WEAPONCOMPONENT_TYPE weaponComp
	
    INT i, j
    FOR i = 0 TO NUM_PLAYER_PED_WEAPON_SLOTS-1
		// Get the weapon an ammo for that slot.
		ammoStored = sPlayerData.storedWeapons.iSlotAmmo[i]
		weaponStored = sPlayerData.storedWeapons.eSlotWeapon[i]
		
		IF (weaponStored != WEAPONTYPE_INVALID) AND (weaponStored != WEAPONTYPE_UNARMED)
			// Give the player weapons and ammo.
			GIVE_WEAPON_TO_PED(playerPed, weaponStored, 0, FALSE, FALSE)
			SET_PED_AMMO(playerPed, weaponStored, ammoStored)
			CDEBUG2LN(DEBUG_SHOOTRANGE, "sPlayerData.storedWeapons.eSlotWeapon[", GET_WEAPON_NAME( sPlayerData.storedWeapons.eSlotWeapon[i] ), "], ammoStored=", ammoStored, ", i=", i)
			
			// Give the player the mods.
			j = 0
			weaponComp = GET_PLAYER_PED_WEAPON_COMP_FROM_INT(weaponStored, j)
			WHILE (weaponComp != WEAPONCOMPONENT_INVALID)
				// If we had the mod, give it back.
				IF IS_BIT_SET(sPlayerData.storedWeapons.iSlotMods[i], j)
					IF NOT HAS_PED_GOT_WEAPON_COMPONENT(playerPed, weaponStored, weaponComp)
						GIVE_WEAPON_COMPONENT_TO_PED(playerPed, weaponStored, weaponComp)
						GIVE_SECONDARY_WEAPON_COMPONENT_TO_PED(playerPed, weaponStored, weaponComp)
					ENDIF
				ENDIF
				
				j++
				weaponComp = GET_PLAYER_PED_WEAPON_COMP_FROM_INT(weaponStored, j)
			ENDWHILE
		ENDIF
	ENDFOR
	
	
	// Now go through the DLC weapons
	INT iDLCIndex
	INT iDLCWeapons = GET_NUM_DLC_WEAPONS()
	scrShopWeaponData weaponData
	scrShopWeaponComponentData compData
	INT iAvailableComp
	
	REPEAT iDLCWeapons iDLCIndex
		IF GET_DLC_WEAPON_DATA(iDLCIndex, weaponData)
		
			weaponStored = sPlayerData.storedWeapons.sDLCWeaponInfo[iDLCIndex].eWeaponType
			ammoStored = sPlayerData.storedWeapons.sDLCWeaponInfo[iDLCIndex].iAmmoCount
			
			// Fix for bug 1472000 - SCEE First Sub #094 - DRM Debug options cause issues with test DLC packages
			// - we need to make sure player doesnt keep weapons that they have uninstalled.
			IF IS_CONTENT_ITEM_LOCKED(weaponData.m_lockHash)
				sPlayerData.storedWeapons.sDLCWeaponInfo[iDLCIndex].eWeaponType = WEAPONTYPE_INVALID
				weaponStored = WEAPONTYPE_INVALID
			ENDIF
			
			// GIVE PED THE WEAPON WITH THE STORED AMMO
			IF weaponStored != WEAPONTYPE_INVALID
				
				IF weaponStored != WEAPONTYPE_UNARMED
				
					IF NOT HAS_PED_GOT_WEAPON(playerPed, weaponStored)
						CDEBUG2LN(DEBUG_SHOOTRANGE, "...giving weapon ", GET_WEAPON_NAME(weaponStored))
						GIVE_WEAPON_TO_PED(playerPed, weaponStored, 0, FALSE, FALSE)					
					ENDIF
					
					// Make sure the ammo counts are the same or more than.
					IF NOT IS_WEAPON_AN_MK2_WEAPON(weaponStored)
						IF GET_AMMO_IN_PED_WEAPON(playerPed, weaponStored) < ammoStored
							SET_PED_AMMO(playerPed, weaponStored, ammoStored)
							CDEBUG2LN(DEBUG_SHOOTRANGE, "....setting stored ammo for ", GET_WEAPON_NAME(weaponStored), " to ", ammoStored)
						ENDIF
					ENDIF
					
					// Set the tints
					SET_PED_WEAPON_TINT_INDEX(playerPed, weaponStored, sPlayerData.storedWeapons.sDLCWeaponInfo[iDLCIndex].iTint)
					
					// Make sure we get all the weapon mods back
					iAvailableComp = 0
					REPEAT GET_NUM_DLC_WEAPON_COMPONENTS(iDLCIndex) j
						IF GET_DLC_WEAPON_COMPONENT_DATA(iDLCIndex, j, compData)
							IF NOT IGNORE_DLC_WEAPON_COMPONENT(INT_TO_ENUM(WEAPONCOMPONENT_TYPE, compData.m_componentName))
								IF IS_BIT_SET(sPlayerData.storedWeapons.sDLCWeaponInfo[iDLCIndex].iModsAsBitfield, iAvailableComp)
									IF NOT HAS_PED_GOT_WEAPON_COMPONENT(playerPed, weaponStored, INT_TO_ENUM(WEAPONCOMPONENT_TYPE, compData.m_componentName))
										GIVE_WEAPON_COMPONENT_TO_PED(playerPed, weaponStored, INT_TO_ENUM(WEAPONCOMPONENT_TYPE, compData.m_componentName))
										GIVE_SECONDARY_WEAPON_COMPONENT_TO_PED(playerPed, weaponStored, INT_TO_ENUM(WEAPONCOMPONENT_TYPE, compData.m_componentName))
										
										// Set camo tint
										IF IS_WEAPON_COMP_A_CAMO_MOD(INT_TO_ENUM(WEAPONCOMPONENT_TYPE, compData.m_componentName))
	//										sPlayerData.storedWeapons.sDLCWeaponInfo[iDLCIndex].iCamo = GET_PED_WEAPON_COMPONENT_TINT_INDEX(playerPed, weaponStored, INT_TO_ENUM(WEAPONCOMPONENT_TYPE, compData.m_componentName))
											SET_PED_WEAPON_COMPONENT_TINT_INDEX(playerPed, weaponStored, INT_TO_ENUM(WEAPONCOMPONENT_TYPE, compData.m_componentName), sPlayerData.storedWeapons.sDLCWeaponInfo[iDLCIndex].iCamo)
											SET_PED_WEAPON_SECONDARY_COMPONENT_TINT_INDEX(playerPed, weaponStored, INT_TO_ENUM(WEAPONCOMPONENT_TYPE, compData.m_componentName), sPlayerData.storedWeapons.sDLCWeaponInfo[iDLCIndex].iCamo)
										ENDIF
									ENDIF
								ELSE
									IF HAS_PED_GOT_WEAPON_COMPONENT(playerPed, weaponStored, INT_TO_ENUM(WEAPONCOMPONENT_TYPE, compData.m_componentName))
										REMOVE_WEAPON_COMPONENT_FROM_PED(playerPed, weaponStored, INT_TO_ENUM(WEAPONCOMPONENT_TYPE, compData.m_componentName))
										REMOVE_SECONDARY_WEAPON_COMPONENT_FROM_PED(playerPed, weaponStored, INT_TO_ENUM(WEAPONCOMPONENT_TYPE, compData.m_componentName))
									ENDIF
								ENDIF
								iAvailableComp++
							ENDIF
						ENDIF
					ENDREPEAT
					
					// Give MK2 ammo now mods are reattached
					IF IS_WEAPON_AN_MK2_WEAPON(weaponStored)
						AMMO_TYPE eAmmoType = GET_GUNCLUB_AMMO_TYPE_FROM_PLAYER(weaponStored)
						
						IF GET_PED_AMMO_BY_TYPE(playerPed, eAmmoType) < ammoStored
							SET_PED_AMMO_BY_TYPE(playerPed, eAmmoType, ammoStored)
							GIVE_WEAPON_TO_PED(playerPed, weaponStored, ammoStored)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	CDEBUG2LN(DEBUG_SHOOTRANGE, "Removed all weapons the player had and restored his old weapons.")
	CLEAR_PLAYER_FLAG(sPlayerData, RANGE_MP_F_TOOK_WEAPONS)
ENDPROC


/// PURPOSE:
///    Given a score and a round, stores that as our high score if it's better than what was previously stored.
PROC RANGEMP_HANDLE_SCORE_STAT_XP_AND_BETTING(RangeMP_CoreData & sCoreInfo, RangeMP_ServerBD & sServerBD, INT iClientID, CHALLENGE_INDEX eTheChallenge, RANGEMP_ROUND_OVER_REASON eReason, RangeMP_ParticipantInfo & sParticipantInfo, RangeMP_PlayerBD & sPlayerBD)
	IF NOT IS_RANGE_ONE_PLAYER()
		IF (iClientID = sParticipantInfo.iShooters[0] AND IS_SERVER_FLAG_SET(sServerBD, RANGE_MP_SERVER_F_Client0WonMatch))
		OR (iClientID = sParticipantInfo.iShooters[1] AND IS_SERVER_FLAG_SET(sServerBD, RANGE_MP_SERVER_F_Client1WonMatch))
			SET_MP_INT_PLAYER_STAT(MPPLY_SHOOTINGRANGE_WINS, GET_MP_INT_PLAYER_STAT(MPPLY_SHOOTINGRANGE_WINS) + 1)
			
			// Start increasing the matches won
			IF (eTheChallenge = CI_GRID)
				CDEBUG2LN(DEBUG_SHOOTRANGE, "Increasing our matches won for CI_GRID")
				INCREMENT_BY_MP_INT_CHARACTER_AWARD(MP_AWARD_FM_SHOOTRANG_TG_WON, 1)
				IF eReason = ROR_Skunked
					SET_MP_BOOL_CHARACTER_AWARD( MP_AWARD_FM_SHOOTRANG_GRAN_WON, TRUE)
					CDEBUG2LN(DEBUG_SHOOTRANGE, "Awarding Granny for grid match skunk win")
				ENDIF
			ELIF (eTheChallenge = CI_RANDOM)
				CDEBUG2LN(DEBUG_SHOOTRANGE, "Increasing our matches won for CI_RANDOM")
				INCREMENT_BY_MP_INT_CHARACTER_AWARD(MP_AWARD_FM_SHOOTRANG_RT_WON, 1)
			ELIF (eTheChallenge = CI_COVERED)
				CDEBUG2LN(DEBUG_SHOOTRANGE, "Increasing our matches won for CI_COVERED")
				INCREMENT_BY_MP_INT_CHARACTER_AWARD(MP_AWARD_FM_SHOOTRANG_CT_WON, 1)
			ENDIF
			
		ELIF (iClientID = sParticipantInfo.iShooters[1] AND IS_SERVER_FLAG_SET(sServerBD, RANGE_MP_SERVER_F_Client0WonMatch))
		OR 	 (iClientID = sParticipantInfo.iShooters[0] AND IS_SERVER_FLAG_SET(sServerBD, RANGE_MP_SERVER_F_Client1WonMatch))
			SET_MP_INT_PLAYER_STAT(MPPLY_SHOOTINGRANGE_LOSSES, GET_MP_INT_PLAYER_STAT(MPPLY_SHOOTINGRANGE_LOSSES) + 1)
			CDEBUG2LN(DEBUG_SHOOTRANGE, "Incrementing Match Losses")
			
		ELIF (iClientID = sParticipantInfo.iShooters[0] AND sServerBD.eClientWhoWon = CWC_CLIENT0_ROUND)
		OR (iClientID = sParticipantInfo.iShooters[1] AND sServerBD.eClientWhoWon = CWC_CLIENT1_ROUND)
			IF eReason = ROR_Skunked AND eTheChallenge = CI_GRID
				SET_MP_BOOL_CHARACTER_AWARD( MP_AWARD_FM_SHOOTRANG_GRAN_WON, TRUE)
				CDEBUG2LN(DEBUG_SHOOTRANGE, "Awarding Granny for grid round skunk win")
			ENDIF
		ENDIF
	ELSE
		IF (iClientID = sParticipantInfo.iShooters[0] AND IS_SERVER_FLAG_SET(sServerBD, RANGE_MP_SERVER_F_Client0WonMatch))
		OR (iClientID = sParticipantInfo.iShooters[1] AND IS_SERVER_FLAG_SET(sServerBD, RANGE_MP_SERVER_F_Client1WonMatch))
			INT iSoloRP = ROUND(10 * g_sMPTunables.fxp_tunable_Minigames_Shooting_Range)
			
			XPCATEGORY xpCat = XPCATEGORY_COMPLETE_SHOOTING_RANGE
			
			IF IS_PLAYER_IN_BUNKER(PLAYER_ID())
				xpCat = XPCATEGORY_COMPLETE_SHOOTING_RANGE_GUNRUN
				CPRINTLN(DEBUG_SHOOTRANGE, "RANGEMP_HANDLE_SCORE_STAT_XP_AND_BETTING :: XPCATEGORY_COMPLETE_SHOOTING_RANGE_GUNRUN ")
			ENDIF
			
			CPRINTLN(DEBUG_SHOOTRANGE, "RANGEMP_HANDLE_SCORE_STAT_XP_AND_BETTING :: Solo RP award given, iSoloRP=", iSoloRP)
			GIVE_LOCAL_PLAYER_XP(eXPTYPE_STANDARD, "XPT_RANGE", XPTYPE_COMPLETE, xpCat, iSoloRP)	// default RP given for playing Solo and finishing a challenge
		ENDIF
	ENDIF
	
	// Always do a read. We'll need this for prediction.
	CDEBUG1LN(DEBUG_SHOOTRANGE, "~~~~~ SETTING RANGE READ DATA! ", eTheChallenge, " :: ", sPlayerBD.sRoundDesc.eWeaponCategory, " ~~~~~")
	RANGE_READ_SOCIAL_CLUB_LEADERBOARD(sCoreInfo.rangeLBD, eTheChallenge, sPlayerBD.sRoundDesc.eWeaponCategory)
ENDPROC

/// PURPOSE:
///    Function to draw the player's name with clan tag.
PROC DRAW_TEXT_WITH_PLAYER_NAME_AND_CLAN(TEXT_PLACEMENT &inText, TEXT_STYLE &Style, PLAYER_INDEX piPlayer, 
		HUD_COLOURS aColour = HUD_COLOUR_WHITE, eTextJustification Justified = FONT_LEFT)	
	IF IS_SAFE_TO_DRAW_ON_SCREEN()
		SET_TEXT_STYLE(Style)	
		SET_TEXT_JUSTIFICATION(Justified)
		
		// To get the crew tag for us.
		GAMER_HANDLE gamerHandle = GET_GAMER_HANDLE_PLAYER(piPlayer)               
		NETWORK_CLAN_DESC clanDescription
		IF NETWORK_CLAN_SERVICE_IS_VALID() AND NETWORK_CLAN_PLAYER_IS_ACTIVE(gamerHandle)
			IF NETWORK_CLAN_PLAYER_GET_DESC(clanDescription, SIZE_OF(clanDescription), gamerHandle)
				CDEBUG2LN(DEBUG_SHOOTRANGE, "PlayerClan.ClanTag  = ", clanDescription.ClanTag)
			ELSE
				clanDescription.ClanTag = ""
			ENDIF
		ENDIF
		
		IF NOT IS_STRING_EMPTY_HUD(clanDescription.ClanTag)
			// Have a crew tag, draw it.
			#IF IS_DEBUG_BUILD
				BEGIN_TEXT_COMMAND_DISPLAY_TEXT("SHR_MP_NAMETAG")
					SET_COLOUR_OF_NEXT_TEXT_COMPONENT(aColour)
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(GET_PLAYER_NAME(piPlayer))
					
					// DON'T HAVE A WAY TO PROPERLY DRAW THIS
					ADD_TEXT_COMPONENT_SUBSTRING_LITERAL_STRING(clanDescription.ClanTag)		// Draw the clan tag.
				END_TEXT_COMMAND_DISPLAY_TEXT(ADJUST_SCREEN_X_POSITION(inText.x), ADJUST_SCREEN_Y_POSITION(inText.y))
			#ENDIF
			
			#IF NOT IS_DEBUG_BUILD
				// No way to do tags in release.
				BEGIN_TEXT_COMMAND_DISPLAY_TEXT("STRING")
					SET_COLOUR_OF_NEXT_TEXT_COMPONENT(aColour)
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(GET_PLAYER_NAME(piPlayer))
				END_TEXT_COMMAND_DISPLAY_TEXT(ADJUST_SCREEN_X_POSITION(inText.x), ADJUST_SCREEN_Y_POSITION(inText.y))
			#ENDIF
		ELSE
			// No crew tag provided.
			BEGIN_TEXT_COMMAND_DISPLAY_TEXT("STRING")
				SET_COLOUR_OF_NEXT_TEXT_COMPONENT(aColour)
				ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(GET_PLAYER_NAME(piPlayer))
			END_TEXT_COMMAND_DISPLAY_TEXT(ADJUST_SCREEN_X_POSITION(inText.x), ADJUST_SCREEN_Y_POSITION(inText.y))
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Draws the time remaining string at the bottom of the main menu.
PROC RANGEMP_DRAW_TIME_REMAINING_MAIN_MENU(TEXT_PLACEMENT &inText, TEXT_STYLE &Style, STRING sMain, STRING inPlayerName, INT iSecTens, INT iSecOnes)
	IF NOT IS_STRING_EMPTY_HUD(inPlayerName)			
		IF IS_SAFE_TO_DRAW_ON_SCREEN()
			SET_TEXT_STYLE(Style)	
			SET_TEXT_JUSTIFICATION(FONT_LEFT)

			BEGIN_TEXT_COMMAND_DISPLAY_TEXT(sMain)
				ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(inPlayerName)
				ADD_TEXT_COMPONENT_INTEGER(iSecTens)
				ADD_TEXT_COMPONENT_INTEGER(iSecOnes)
			END_TEXT_COMMAND_DISPLAY_TEXT(ADJUST_SCREEN_X_POSITION(inText.x), ADJUST_SCREEN_Y_POSITION(inText.y))
		ENDIF
	ENDIF
ENDPROC


