// RangeMP_GridRound_lib.sch
USING "RangeMP_Core.sch"
USING "RangeMP_Round.sch"
USING "RangeMP_Targets_lib.sch"
USING "RangeMP_Broadcast_lib.sch"
USING "shared_hud_displays.sch"
USING "RangeMP_Support_lib.sch"


#IF IS_DEBUG_BUILD
	WIDGET_GROUP_ID GR_Widgets
	
	PROC INIT_GRID_ROUND_WIDGETS(RangeMP_Round & sRndInfo)		
		GR_Widgets = START_WIDGET_GROUP("Shooting Range")
			ADD_WIDGET_FLOAT_SLIDER("GR Anchor Pos X", sRndInfo.sGridData.vGridAnchorPos.x, -3000.0, 3000.0, 0.005)
			ADD_WIDGET_FLOAT_SLIDER("GR Anchor Pos Y", sRndInfo.sGridData.vGridAnchorPos.y, -3000.0, 3000.0, 0.005)
			ADD_WIDGET_FLOAT_SLIDER("GR Anchor Pos Z", sRndInfo.sGridData.vGridAnchorPos.z, -3000.0, 3000.0, 0.005)
		STOP_WIDGET_GROUP()
	ENDPROC
	
	PROC UPDATE_GRID_ROUND_WIDGETS(RangeMP_Round & sRndInfo)
		IF DOES_ENTITY_EXIST(sRndInfo.oAnchorObject)
			//SET_ENTITY_COORDS(sRndInfo.oAnchorObject, sRndInfo.sGridData.vGridAnchorPos, FALSE)
		ENDIF
	ENDPROC

	
	PROC CLEANUP_GRID_ROUND_WIDGETS()
		DELETE_WIDGET_GROUP(GR_Widgets)
	ENDPROC
#ENDIF




/// PURPOSE:
///    Invoked only within the internal update. Private.
PROC RANGE_MP_UPDATE_GRID_ROUND_UI(INT iRedScore, INT iBlueScore, INT iClientID, RangeMP_PlayerBD & sPlayerBD[], RangeMP_ParticipantInfo & sParticipantInfo )		
	// Red is on left, Blue is on right.	
	Range_HUD_Data sHUDData
	sHUDData.sCurScoreLabel = ""
	
	INT iRedPlayer = sParticipantInfo.iShooters[RED_PLAYER_CLIENT_ID]
	
	IF iRedPlayer = k_InvalidParticipantID
		CDEBUG3LN( DEBUG_SHOOTRANGE, "RANGE_MP_UPDATE_GRID_ROUND_UI - iRedPlayer = k_InvalidParticipantID, this is potentially fatal." )
	ENDIF	
		
	// Red is on left, Blue is on right.
	IF (iClientID = iRedPlayer)
		sHUDData.iCurScore = iRedScore
		sHUDData.eCurScoreColor = HUD_COLOUR_ORANGE
		
		sHUDData.sOpponentScoreLabel = "SHR_MP_OSCORE_C"
		sHUDData.iOpponentScore = iBlueScore
		sHUDData.eOpponentScoreColor = HUD_COLOUR_PURPLE	
	ELSE
		sHUDData.iCurScore = iBlueScore
		sHUDData.eCurScoreColor = HUD_COLOUR_PURPLE
		
		sHUDData.sOpponentScoreLabel = "SHR_MP_OSCORE_C"
		sHUDData.iOpponentScore = iRedScore
		sHUDData.eOpponentScoreColor = HUD_COLOUR_ORANGE
	ENDIF
	
	// Get the player names to display.
	sHUDData.bDrawPlayerNames = TRUE
	sHUDData.sCurScoreLabel = GET_PLAYER_NAME(sPlayerBD[iClientID]. piCurPlayerID)
	sHUDData.sOpponentScoreLabel = GET_PLAYER_NAME(sPlayerBD[RANGE_GET_OPPOSING_SHOOTER(sParticipantInfo)]. piCurPlayerID)
	
	IF IS_RANGE_ONE_PLAYER()
		sHUDData.iOpponentScore = -1
	ENDIF
	
	IF IS_CLIENT_FLAG_SET(sPlayerBD[iClientID], RANGE_MP_F_IsSpectator)
		sHUDData.iCurScore = -1
	ENDIF
	
	// Trigger the draw.
	DRAW_SHOOTING_RANGE_HUD(sHUDData)
ENDPROC

//B*552253
/// PURPOSE:
///    Round init.
PROC RANGE_MP_CREATE_GRID_ROUND(RangeMP_CoreData & sCoreInfo, RangeMP_Round& sRndInfo, RangeMP_PlayerBD & sPlayerBD)
	DEBUG_MESSAGE("CREATE_RANGE_ROUND: Creating grid variant...")
	sCoreInfo.iWinnings = sCoreInfo.iWinnings
	
	// Hate to do this, but we need to make an invisible anchor object, then attach the grid to it.
	sRndInfo.oAnchorObject = GET_ENTITY_FROM_PED_OR_VEHICLE(CREATE_OBJECT(PROP_TARGET_ORA_PURP_01, sRndInfo.sGridData.vGridAnchorPos, FALSE, FALSE))
	SET_ENTITY_VISIBLE(sRndInfo.oAnchorObject, FALSE)

	// Create the grid frame.
	sRndInfo.oMisc = GET_ENTITY_FROM_PED_OR_VEHICLE(CREATE_OBJECT(PROP_TARGET_FRAME_01, sRndInfo.sGridData.vGridFramePos, FALSE, FALSE))
	
	IF NOT IS_BIT_SET(sPlayerBD.iTargetsField2, 31)
		// Create A Row - R R R R B B B
		sRndInfo.oRoundObjects[CI_GRID_A1] = CREATE_GRID_TARGET(CI_GRID_A1, TARG_F_IS_RED, sRndInfo.oMisc)
		sRndInfo.oRoundObjects[CI_GRID_A2] = CREATE_GRID_TARGET(CI_GRID_A2, TARG_F_IS_RED, sRndInfo.oMisc)
		sRndInfo.oRoundObjects[CI_GRID_A3] = CREATE_GRID_TARGET(CI_GRID_A3, TARG_F_IS_RED, sRndInfo.oMisc)
		sRndInfo.oRoundObjects[CI_GRID_A4] = CREATE_GRID_TARGET(CI_GRID_A4, TARG_F_IS_RED, sRndInfo.oMisc)
		sRndInfo.oRoundObjects[CI_GRID_A5] = CREATE_GRID_TARGET(CI_GRID_A5, TARG_F_IS_BLUE, sRndInfo.oMisc)
		sRndInfo.oRoundObjects[CI_GRID_A6] = CREATE_GRID_TARGET(CI_GRID_A6, TARG_F_IS_BLUE, sRndInfo.oMisc)
		sRndInfo.oRoundObjects[CI_GRID_A7] = CREATE_GRID_TARGET(CI_GRID_A7, TARG_F_IS_BLUE, sRndInfo.oMisc)
		SET_BIT(sPlayerBD.iTargetsField1, ENUM_TO_INT(CI_GRID_A1))
		SET_BIT(sPlayerBD.iTargetsField1, ENUM_TO_INT(CI_GRID_A2))
		SET_BIT(sPlayerBD.iTargetsField1, ENUM_TO_INT(CI_GRID_A3))
		SET_BIT(sPlayerBD.iTargetsField1, ENUM_TO_INT(CI_GRID_A4))
		CLEAR_BIT(sPlayerBD.iTargetsField1, ENUM_TO_INT(CI_GRID_A5))
		CLEAR_BIT(sPlayerBD.iTargetsField1, ENUM_TO_INT(CI_GRID_A6))
		CLEAR_BIT(sPlayerBD.iTargetsField1, ENUM_TO_INT(CI_GRID_A7))
		
		// Create B Row - R R R B B B B
		sRndInfo.oRoundObjects[CI_GRID_B1] = CREATE_GRID_TARGET(CI_GRID_B1, TARG_F_IS_RED, sRndInfo.oMisc)
		sRndInfo.oRoundObjects[CI_GRID_B2] = CREATE_GRID_TARGET(CI_GRID_B2, TARG_F_IS_RED, sRndInfo.oMisc)
		sRndInfo.oRoundObjects[CI_GRID_B3] = CREATE_GRID_TARGET(CI_GRID_B3, TARG_F_IS_RED, sRndInfo.oMisc)
		sRndInfo.oRoundObjects[CI_GRID_B4] = CREATE_GRID_TARGET(CI_GRID_B4, TARG_F_IS_BLUE, sRndInfo.oMisc)
		sRndInfo.oRoundObjects[CI_GRID_B5] = CREATE_GRID_TARGET(CI_GRID_B5, TARG_F_IS_BLUE, sRndInfo.oMisc)
		sRndInfo.oRoundObjects[CI_GRID_B6] = CREATE_GRID_TARGET(CI_GRID_B6, TARG_F_IS_BLUE, sRndInfo.oMisc)
		sRndInfo.oRoundObjects[CI_GRID_B7] = CREATE_GRID_TARGET(CI_GRID_B7, TARG_F_IS_BLUE, sRndInfo.oMisc)
		SET_BIT(sPlayerBD.iTargetsField1, ENUM_TO_INT(CI_GRID_B1))
		SET_BIT(sPlayerBD.iTargetsField1, ENUM_TO_INT(CI_GRID_B2))
		SET_BIT(sPlayerBD.iTargetsField1, ENUM_TO_INT(CI_GRID_B3))
		CLEAR_BIT(sPlayerBD.iTargetsField1, ENUM_TO_INT(CI_GRID_B4))
		CLEAR_BIT(sPlayerBD.iTargetsField1, ENUM_TO_INT(CI_GRID_B5))
		CLEAR_BIT(sPlayerBD.iTargetsField1, ENUM_TO_INT(CI_GRID_B6))
		CLEAR_BIT(sPlayerBD.iTargetsField1, ENUM_TO_INT(CI_GRID_B7))
		
		// Create C Row - R R R R B B B
		sRndInfo.oRoundObjects[CI_GRID_C1] = CREATE_GRID_TARGET(CI_GRID_C1, TARG_F_IS_RED, sRndInfo.oMisc)
		sRndInfo.oRoundObjects[CI_GRID_C2] = CREATE_GRID_TARGET(CI_GRID_C2, TARG_F_IS_RED, sRndInfo.oMisc)
		sRndInfo.oRoundObjects[CI_GRID_C3] = CREATE_GRID_TARGET(CI_GRID_C3, TARG_F_IS_RED, sRndInfo.oMisc)
		sRndInfo.oRoundObjects[CI_GRID_C4] = CREATE_GRID_TARGET(CI_GRID_C4, TARG_F_IS_RED, sRndInfo.oMisc)
		sRndInfo.oRoundObjects[CI_GRID_C5] = CREATE_GRID_TARGET(CI_GRID_C5, TARG_F_IS_BLUE, sRndInfo.oMisc)
		sRndInfo.oRoundObjects[CI_GRID_C6] = CREATE_GRID_TARGET(CI_GRID_C6, TARG_F_IS_BLUE, sRndInfo.oMisc)
		sRndInfo.oRoundObjects[CI_GRID_C7] = CREATE_GRID_TARGET(CI_GRID_C7, TARG_F_IS_BLUE, sRndInfo.oMisc)
		SET_BIT(sPlayerBD.iTargetsField1, ENUM_TO_INT(CI_GRID_C1))
		SET_BIT(sPlayerBD.iTargetsField1, ENUM_TO_INT(CI_GRID_C2))
		SET_BIT(sPlayerBD.iTargetsField1, ENUM_TO_INT(CI_GRID_C3))
		SET_BIT(sPlayerBD.iTargetsField1, ENUM_TO_INT(CI_GRID_C4))
		CLEAR_BIT(sPlayerBD.iTargetsField1, ENUM_TO_INT(CI_GRID_C5))
		CLEAR_BIT(sPlayerBD.iTargetsField1, ENUM_TO_INT(CI_GRID_C6))
		CLEAR_BIT(sPlayerBD.iTargetsField1, ENUM_TO_INT(CI_GRID_C7))
		
		// Create D Row -  - R R R B B B B
		sRndInfo.oRoundObjects[CI_GRID_D1] = CREATE_GRID_TARGET(CI_GRID_D1, TARG_F_IS_RED, sRndInfo.oMisc)
		sRndInfo.oRoundObjects[CI_GRID_D2] = CREATE_GRID_TARGET(CI_GRID_D2, TARG_F_IS_RED, sRndInfo.oMisc)
		sRndInfo.oRoundObjects[CI_GRID_D3] = CREATE_GRID_TARGET(CI_GRID_D3, TARG_F_IS_RED, sRndInfo.oMisc)
		sRndInfo.oRoundObjects[CI_GRID_D4] = CREATE_GRID_TARGET(CI_GRID_D4, TARG_F_IS_BLUE, sRndInfo.oMisc)
		sRndInfo.oRoundObjects[CI_GRID_D5] = CREATE_GRID_TARGET(CI_GRID_D5, TARG_F_IS_BLUE, sRndInfo.oMisc)
		sRndInfo.oRoundObjects[CI_GRID_D6] = CREATE_GRID_TARGET(CI_GRID_D6, TARG_F_IS_BLUE, sRndInfo.oMisc)
		sRndInfo.oRoundObjects[CI_GRID_D7] = CREATE_GRID_TARGET(CI_GRID_D7, TARG_F_IS_BLUE, sRndInfo.oMisc)
		SET_BIT(sPlayerBD.iTargetsField1, ENUM_TO_INT(CI_GRID_D1))
		SET_BIT(sPlayerBD.iTargetsField1, ENUM_TO_INT(CI_GRID_D2))
		SET_BIT(sPlayerBD.iTargetsField1, ENUM_TO_INT(CI_GRID_D3))
		CLEAR_BIT(sPlayerBD.iTargetsField1, ENUM_TO_INT(CI_GRID_D4))
		CLEAR_BIT(sPlayerBD.iTargetsField1, ENUM_TO_INT(CI_GRID_D5))
		CLEAR_BIT(sPlayerBD.iTargetsField1, ENUM_TO_INT(CI_GRID_D6))
		CLEAR_BIT(sPlayerBD.iTargetsField1, ENUM_TO_INT(CI_GRID_D7))
	
		SET_ENTITY_ROTATION(sRndInfo.oAnchorObject, sRndInfo.sGridData.vGridStartRot)
		ATTACH_ENTITY_TO_ENTITY(sRndInfo.oMisc, sRndInfo.oAnchorObject, 0, <<0,-0.1,-1.20>>, <<0,0,0>>)
	ELSE	// spectator joining midsession bit isn't set
		CLEAR_BIT(sPlayerBD.iTargetsField2, 31)
		INT index
		FOR index = CI_GRID_A1 TO CI_MAX_GRID_OBJECTS
			IF IS_BIT_SET(sPlayerBD.iTargetsField1, index)
				CDEBUG2LN(DEBUG_SHOOTRANGE, "RANGE_MP_CREATE_GRID_ROUND :: Orange target at index=", index)
				sRndInfo.oRoundObjects[index] = CREATE_GRID_TARGET(INT_TO_ENUM(CI_GRID_OBJECT_INDEXES, index), TARG_F_IS_RED, sRndInfo.oMisc)
			ELSE
				CDEBUG2LN(DEBUG_SHOOTRANGE, "RANGE_MP_CREATE_GRID_ROUND :: Purple target at index=", index)
				sRndInfo.oRoundObjects[index] = CREATE_GRID_TARGET(INT_TO_ENUM(CI_GRID_OBJECT_INDEXES, index), TARG_F_IS_BLUE, sRndInfo.oMisc)
			ENDIF
		ENDFOR
	
		SET_ENTITY_ROTATION(sRndInfo.oAnchorObject, sRndInfo.sGridData.vGridEndRot)
		ATTACH_ENTITY_TO_ENTITY(sRndInfo.oMisc, sRndInfo.oAnchorObject, 0, <<0,-0.1,-1.20>>, <<0,0,0>>)
	ENDIF
	
//#IF IS_DEBUG_BUILD
//	INIT_GRID_ROUND_WIDGETS(sRndInfo)
//#ENDIF
ENDPROC


/// PURPOSE:
///    Goes through all targets and peds, and makes sure they're not flagged as having caused damage.
PROC RANGE_MP_START_GRID_ROUND(RangeMP_Round & sRndInfo)
	//CLEAR_PED_LAST_WEAPON_DAMAGE(playerPed)
	
	IF NOT IS_ENTITY_DEAD(sRndInfo.oMisc)
		CLEAR_ENTITY_LAST_DAMAGE_ENTITY(sRndInfo.oMisc)
	ENDIF
	
	// Clear all targets.
	INT index
	FOR index = 0 TO ENUM_TO_INT(CI_MAX_GRID_OBJECTS) - 1
		IF NOT IS_ENTITY_DEAD(sRndInfo.oRoundObjects[index].oTarget)
			CLEAR_ENTITY_LAST_DAMAGE_ENTITY(sRndInfo.oRoundObjects[index].oTarget)
		ENDIF
	ENDFOR
	
	// Clear invisible holder.
	IF NOT IS_ENTITY_DEAD(sRndInfo.oAnchorObject)
		CLEAR_ENTITY_LAST_DAMAGE_ENTITY(sRndInfo.oAnchorObject)
	ENDIF
ENDPROC


/// PURPOSE:
///    This function is responsible for moving the grid into position in that range round.
/// RETURNS:
///    TRUE when done moving.
FUNC BOOL RANGE_MP_SETUP_GRID_ROUND(RangeMP_Round & sRndInfo)
	IF IS_BITMASK_AS_ENUM_SET(sRndInfo.iFlags, ROUND_GRID_FINISHED_ROTATION)
		RETURN TRUE
	ENDIF
	
	// If we just started moving, play a sound.
	IF (sRndInfo.fMiscTime = 0.0)
		sRndInfo.iMiscSound = GET_SOUND_ID()
		PLAY_SOUND_FROM_ENTITY(sRndInfo.iMiscSound, "TARGET_PRACTICE_SLIDE_MASTER", sRndInfo.oMisc)
	ENDIF
	
	// Update the time.
	sRndInfo.fMiscTime += GET_FRAME_TIME()
	FLOAT fInterpTime
	
	// Find out how much we should be flipping
	fInterpTime = sRndInfo.fMiscTime / GRID_RAISE_TIME
	
	// Keep going until we reach 1.0 (means we are at our destination)
	IF fInterpTime < 1.0
		VECTOR vStart = sRndInfo.sGridData.vGridStartRot
		VECTOR vEnd = sRndInfo.sGridData.vGridEndRot
		VECTOR vRotation = vStart + (vEnd - vStart) * fInterpTime
		
		CDEBUG2LN(DEBUG_SHOOTRANGE, "Grid and targets still rotating in during setup...")
		SET_ENTITY_ROTATION(sRndInfo.oAnchorObject, vRotation)
		
		RETURN FALSE
	ELSE
		// When done, detach all targets and return true so that we stop moving.
		CDEBUG2LN(DEBUG_SHOOTRANGE, "Grid and targets DONE with with their setup!")
		SET_ENTITY_ROTATION(sRndInfo.oAnchorObject, sRndInfo.sGridData.vGridEndRot)
		
		INT index
		FOR index = 0 TO ENUM_TO_INT(CI_MAX_GRID_OBJECTS) - 1
			CDEBUG2LN(DEBUG_SHOOTRANGE, "Detaching target ", index, " from the grid!")
			DETACH_ENTITY(sRndInfo.oRoundObjects[index].oTarget, FALSE)
		ENDFOR
		
		// Reset timer.
		sRndInfo.fMiscTime = 0.0
		
		// Stop sound.
		IF (sRndInfo.iMiscSound != -1)
			STOP_SOUND(sRndInfo.iMiscSound)
			RELEASE_SOUND_ID(sRndInfo.iMiscSound)
			sRndInfo.iMiscSound = -1
		ENDIF
		
		// Play a new one!
		PLAY_SOUND_FROM_ENTITY(-1, "TARGET_PRACTICE_STOP_MASTER", sRndInfo.oMisc)
		
		SET_BITMASK_AS_ENUM(sRndInfo.iFlags, ROUND_GRID_FINISHED_ROTATION)
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Round update.
PROC RANGE_MP_UPDATE_GRID_ROUND(RangeMP_Round& sRndInfo, INT iClientID, RangeMP_PlayerData & sThisPlayerData, 
		RangeMP_PlayerBD & sPlayerBD[], INT iClient0Score, INT iClient1Score, RangeMP_MenuData & sMenuData, RangeMP_ParticipantInfo & sParticipantInfo, BOOL bRenderHUD = TRUE)
	UNUSED_PARAMETER(sMenuData)
	
	// Go through all of the round targets, and figure out if we've shot one.
	INT index = 0
	
	// Store our current player once.
	ENTITY_INDEX playerEntity = GET_ENTITY_FROM_PED_OR_VEHICLE(playerPed)
	
	// If we've fired a shot this frame, store it:
	BOOL bTrackingForMiss = FALSE
	VECTOR vDontCare
	IF GET_PED_LAST_WEAPON_IMPACT_COORD(playerPed, vDontCare) AND NOT IS_PLAYER_FLAG_SET(sThisPlayerData, RANGE_MP_F_DONT_COUNT_SHOTS_FIRED)
		sThisPlayerData.iShotsFired += 1
		sThisPlayerData.iShotsFiredThisRound += 1
		bTrackingForMiss = TRUE
	ENDIF
	
	// Always heal the grid.
	IF NOT IS_ENTITY_DEAD(sRndInfo.oMisc)
		SET_ENTITY_HEALTH(sRndInfo.oMisc, TARGET_STARTING_HEALTH)
	ENDIF
		
	INT iRedPlayer = sParticipantInfo.iShooters[RED_PLAYER_CLIENT_ID]
	INT iBluePlayer = sParticipantInfo.iShooters[BLUE_PLAYER_CLIENT_ID]
	
	IF iRedPlayer = k_InvalidParticipantID
		CDEBUG3LN( DEBUG_SHOOTRANGE, "RANGE_MP_UPDATE_GRID_ROUND - iRedPlayer = k_InvalidParticipantID, this is potentially fatal." )
	ENDIF	
	IF iBluePlayer = k_InvalidParticipantID
		CDEBUG3LN( DEBUG_SHOOTRANGE, "RANGE_MP_UPDATE_GRID_ROUND - iBluePlayer = k_InvalidParticipantID, this is potentially fatal." )
	ENDIF	
	
	// HANDLE THE GRID ROUND UPDATE. 
	// General update.
	FOR index = index TO ENUM_TO_INT(CI_MAX_GRID_OBJECTS) - 1		
		// Detect if we've been shot. Can only happen while not flipping.
		IF NOT IS_ENTITY_DEAD(sRndInfo.oRoundObjects[index].oTarget)
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(sRndInfo.oRoundObjects[index].oTarget, playerEntity)
				SET_ENTITY_HEALTH(sRndInfo.oRoundObjects[index].oTarget, TARGET_STARTING_HEALTH)
				
				IF NOT (CHECK_TARGET_FLAG(sRndInfo.oRoundObjects[index], TARG_F_FLIPPING_TO_BLUE) OR 
						CHECK_TARGET_FLAG(sRndInfo.oRoundObjects[index], TARG_F_FLIPPING_TO_RED))
					// Need to be the right color player, shooting the right color target. Red player shoots red targets.
					IF ((iClientID = iRedPlayer) AND CHECK_TARGET_FLAG(sRndInfo.oRoundObjects[index], TARG_F_IS_BLUE)) OR 
							((iClientID = iBluePlayer) AND CHECK_TARGET_FLAG(sRndInfo.oRoundObjects[index], TARG_F_IS_RED))
						// Need to broadcast to tell other players to flip this target.
						CDEBUG2LN(DEBUG_SHOOTRANGE, "Player ID ", iClientID, " broadcasting a hit on target ", index)
						BROADCAST_HIT_TO_OTHER_PLAYERS(index, iClientID, sPlayerBD)
				
						// Register a hit. This isn't to say we caused it to flip, we just actually hit something.
						sThisPlayerData.iShotsHit += 1
						sThisPlayerData.iShotsHitThisRound += 1
						INCREMENT_MP_INT_CHARACTER_STAT(MP_STAT_HITS_PEDS_VEHICLES)
						bTrackingForMiss = FALSE
						
						// We'll swap it locally. Other players must be notified to do this.
						IF (CHECK_TARGET_FLAG(sRndInfo.oRoundObjects[index], TARG_F_IS_RED))
							// Was red. Make blue.
							CLEAR_BIT(sPlayerBD[iClientID].iTargetsField1, index)	// bits are the targets, index is a specific target, cleared means blue
							CLEAR_TARGET_FLAG(sRndInfo.oRoundObjects[index], TARG_F_IS_RED)
							SET_TARGET_FLAG(sRndInfo.oRoundObjects[index], TARG_F_FLIPPING_TO_BLUE)
						ELSE
							// Was blue. Make red.
							SET_BIT(sPlayerBD[iClientID].iTargetsField1, index)	// bits are the targets, index is a specific target, set means orange
							CLEAR_TARGET_FLAG(sRndInfo.oRoundObjects[index], TARG_F_IS_BLUE)
							SET_TARGET_FLAG(sRndInfo.oRoundObjects[index], TARG_F_FLIPPING_TO_RED)
						ENDIF
					ENDIF
				ENDIF
				
				// Clear damage so it's not counted twice.
				CLEAR_ENTITY_LAST_DAMAGE_ENTITY(sRndInfo.oRoundObjects[index].oTarget)
			ENDIF
		ELSE
			CDEBUG2LN(DEBUG_SHOOTRANGE, "RANGE_MP_UPDATE_GRID_ROUND: Entity is dead!")
		ENDIF
	ENDFOR
	
	// Did we miss?
	IF bTrackingForMiss
		sThisPlayerData.iShotsMissedThisRound += 1
	ENDIF
	
	// Draw our score onscreen.
	// GRAB THE SCORE FROM THE SERVER.
	IF bRenderHUD
		RANGE_MP_UPDATE_GRID_ROUND_UI(iClient0Score, iClient1Score, iClientID, sPlayerBD, sParticipantInfo)
	ENDIF
	
	// Update all targets
	FOR index = 0 TO MAX_TARGETS - 1
		UPDATE_GRID_TARGET(sRndInfo, sRndInfo.oRoundObjects[index], index)
	ENDFOR

//#IF IS_DEBUG_BUILD
//	UPDATE_GRID_ROUND_WIDGETS(sRndInfo)
//#ENDIF
ENDPROC

/// PURPOSE:
///    Round cleanup.
PROC RANGE_MP_END_GRID_ROUND(RangeMP_Round& sRndInfo)
	DEBUG_MESSAGE("CREATE_RANGE_ROUND: Detroying grid variant...")
	
	CLEAR_BITMASK_AS_ENUM(sRndInfo.iFlags, ROUND_GRID_FINISHED_ROTATION)
			
	// Destroy the grid frame.
	IF NOT IS_ENTITY_DEAD(sRndInfo.oMisc)
		DELETE_ENTITY(sRndInfo.oMisc)
	ENDIF
	
	// Destroy all targets.
	INT index
	FOR index = 0 TO ENUM_TO_INT(CI_MAX_GRID_OBJECTS) - 1
		IF NOT IS_ENTITY_DEAD(sRndInfo.oRoundObjects[index].oTarget)
			DELETE_ENTITY(sRndInfo.oRoundObjects[index].oTarget)
		ENDIF
		sRndInfo.oRoundObjects[index].iFlags = 0
	ENDFOR
	
	// Destroy invisible holder.
	IF NOT IS_ENTITY_DEAD(sRndInfo.oAnchorObject)
		DELETE_ENTITY(sRndInfo.oAnchorObject)
	ENDIF

//#IF IS_DEBUG_BUILD
//	CLEANUP_GRID_ROUND_WIDGETS()
//#ENDIF
ENDPROC

