// RangeMP_MainMenu_lib.sch
USING "RangeMP_MainMenu.sch"
USING "screens_header.sch"
USING "RangeMP_Round.sch"
USING "RangeMP_Support_lib.sch"
USING "transition_common.sch"

BOOL bIsWidescreen
//FLOAT DEBUG_Y_NUDGE = 0.000

INT iNumMenuElements

/// PURPOSE:
///    Sets up the MP Range's main menu.
PROC INIT_RANGEMP_MAINMENU(RangeMP_MenuData & sMenuInfo)
	// Restore all previous values.
	RESET_ALL_SPRITE_PLACEMENT_VALUES(sMenuInfo.uiPlacement.SpritePlacement)
	SET_STANDARD_INGAME_TEXT_DETAILS(sMenuInfo.uiPlacement.aStyle)
	
	// Set up the menu with our new values.
	INIT_SCREEN_RANGEMP_MENU(sMenuInfo.uiPlacement)
	bIsWidescreen = GET_IS_WIDESCREEN()
	
	// Reset the random crosses.
	sMenuInfo.bSetupRandom_Menu = FALSE
ENDPROC

FUNC FLOAT RANGE_GET_TEXT_BASE_FOR_RESOLUTION()
	FLOAT fAspectRatio = GET_SCREEN_ASPECT_RATIO()
	
	IF fAspectRatio >=   RANGE_RESOLUTION_21x9			RETURN RANGE_TEXT_SCALE_21x9
	ELIF fAspectRatio >= RANGE_RESOLUTION_17x9			RETURN RANGE_TEXT_SCALE_17x9
	ELIF fAspectRatio >= RANGE_RESOLUTION_16x9			RETURN RANGE_TEXT_SCALE_16x9
	ELIF fAspectRatio >= RANGE_RESOLUTION_5x3			RETURN RANGE_TEXT_SCALE_5x3
	ELIF fAspectRatio >= RANGE_RESOLUTION_16x10			RETURN RANGE_TEXT_SCALE_16x10
	ELIF fAspectRatio >= RANGE_RESOLUTION_3x2			RETURN RANGE_TEXT_SCALE_3x2
	ELIF fAspectRatio >= RANGE_RESOLUTION_4x3			RETURN RANGE_TEXT_SCALE_4x3
	ELIF fAspectRatio >= RANGE_RESOLUTION_5x4			RETURN RANGE_TEXT_SCALE_5x4
	ELSE RETURN RANGE_TEXT_SCALE_16x9
	ENDIF
ENDFUNC

/// PURPOSE:
///    Adjusts the scale of the text for the resolution. Currently only supports any ratio between 1 and 2.
/// PARAMS:
///    style - passed by reference and adjusted
PROC RANGE_ADJUST_STYLE_FOR_RESOLUTION(TEXT_STYLE & style)
	
	FLOAT fTextBase = RANGE_GET_TEXT_BASE_FOR_RESOLUTION()
	
	style.XScale = fTextBase
	style.YScale = fTextBase
	
ENDPROC

/// PURPOSE:
///    Wrapper so we can resize text for non widescreen resolutions
/// PARAMS:
///    inText - passed by reference and cached
///    style - passed by reference and cached
///    inLabel - 
///    bBeforeHUD - 
PROC DRAW_RANGE_TEXT(TEXT_PLACEMENT & inText, TEXT_STYLE & style, STRING inLabel, BOOL bBeforeHUD = FALSE)
	TEXT_PLACEMENT placementText = inText
	TEXT_STYLE styleText = style
	RANGE_ADJUST_STYLE_FOR_RESOLUTION(styleText)
	DRAW_TEXT(placementText, styleText, inLabel, bBeforeHUD)
ENDPROC

/// PURPOSE:
///    Wrapper so we can resize text for non widescreen resolutions
/// PARAMS:
///    inText - passed by reference and cached
///    style - passed by reference and cached
///    inLabel - 
///    num1 - 
///    Justified - 
PROC DRAW_RANGE_TEXT_WITH_NUMBER(TEXT_PLACEMENT & inText, TEXT_STYLE & style, STRING inLabel, INT num1, eTextJustification Justified = FONT_LEFT)
	TEXT_PLACEMENT placementText = inText
	TEXT_STYLE styleText = style
	RANGE_ADJUST_STYLE_FOR_RESOLUTION(styleText)
	DRAW_TEXT_WITH_NUMBER(placementText, styleText, inLabel, num1, Justified)
ENDPROC

/// PURPOSE:
///    Wrapper so we can resize text for non widescreen resolutions
/// PARAMS:
///    inText - passed by reference and cached
///    style - passed by reference and cached
///    inLabel - 
///    bAlignCenter - 
///    bAlignRight - 
PROC DRAW_RANGE_TEXT_WITH_ALIGNMENT(TEXT_PLACEMENT & inText, TEXT_STYLE & style, STRING inLabel, BOOL bAlignCenter, BOOL bAlignRight)
	TEXT_PLACEMENT placementText = inText
	TEXT_STYLE styleText = style
	RANGE_ADJUST_STYLE_FOR_RESOLUTION(styleText)
	DRAW_TEXT_WITH_ALIGNMENT(placementText, styleText, inLabel, bAlignCenter, bAlignRight)
ENDPROC

/// PURPOSE:
///    Wrapper so we can resize text for non widescreen resolutions
/// PARAMS:
///    inText - passed by reference and cached
///    style - passed by reference and cached
///    inPlayerName - 
///    textLabel - 
///    eColor - 
///    Justified - 
PROC DRAW_RANGE_TEXT_WITH_PLAYER_NAME(TEXT_PLACEMENT & inText, TEXT_STYLE & style, STRING inPlayerName, STRING textLabel = NULL, HUD_COLOURS eColor = HUD_COLOUR_WHITE, eTextJustification Justified = FONT_LEFT)
	TEXT_PLACEMENT placementText = inText
	TEXT_STYLE styleText = style
	RANGE_ADJUST_STYLE_FOR_RESOLUTION(styleText)
	DRAW_TEXT_WITH_PLAYER_NAME(placementText, styleText, inPlayerName, textLabel, eColor, Justified)
ENDPROC

/// PURPOSE:
///    Draws the grid round results in the top right corner.
/// PARAMS:
///    bDrawBlank - Should draw a normal round as if it hasn't been played previously.
PROC RANGEMP_MAINMENU_DRAW_GRID(RangeMP_MenuData & sMenuInfo, RangeMP_Round & sRndInfo, BOOL bDrawBlank)
	// Always draw the actual grid.
	DRAW_2D_SPRITE("SRange_Chal", "Chal_Grid_Empty", sMenuInfo.uiPlacement.SpritePlacement[RANGEMP_MEDAL_TITLE_IMAGE])
	
	// Setup one dot.
	SPRITE_PLACEMENT sprDot
	sprDot.w = 0.0375
	sprDot.h = 0.066
	sprDot.a = 255
	
	INT iNumRows = 4, iNumCols = 7, idxRow, idxCol
	REPEAT iNumRows idxRow
		REPEAT iNumCols idxCol
			// Reset width and height values
			sprDot.w = 0.0375
			sprDot.h = 0.066
			sprDot.a = 255
			
			// Draw a dot here.
			sprDot.x = (idxCol * 0.0231) + 0.631
			sprDot.y = (idxRow * 0.041) + 0.250
			
			// Color is going to be based on either what happened, or just a standard round.
			IF bDrawBlank
				IF (idxCol < 3)
					SET_SPRITE_HUD_COLOUR(sprDot, HUD_COLOUR_ORANGE)
				ELIF (idxCol > 3)
					SET_SPRITE_HUD_COLOUR(sprDot, HUD_COLOUR_PURPLE)
				ELSE
					// Middle column.
					IF (idxRow = 0) OR (idxRow = 2)
						SET_SPRITE_HUD_COLOUR(sprDot, HUD_COLOUR_ORANGE)
					ELSE
						SET_SPRITE_HUD_COLOUR(sprDot, HUD_COLOUR_PURPLE)
					ENDIF
				ENDIF
			ELSE
				// Default to orange
				SET_SPRITE_HUD_COLOUR(sprDot, HUD_COLOUR_ORANGE)
				
				// Set the color for this target based on the flag set for that target.
				IF CHECK_TARGET_FLAG(sRndInfo.oRoundObjects[idxCol + (idxRow * 7)], TARG_F_IS_RED)
					SET_SPRITE_HUD_COLOUR(sprDot, HUD_COLOUR_ORANGE)
				ELIF CHECK_TARGET_FLAG(sRndInfo.oRoundObjects[idxCol + (idxRow * 7)], TARG_F_IS_BLUE)
					SET_SPRITE_HUD_COLOUR(sprDot, HUD_COLOUR_PURPLE)
				ENDIF
			ENDIF
			
			IF NOT bIsWidescreen
				FIXUP_SPRITE_FOR_NON_WIDESCREEN(sprDot)
			ENDIF
			
			// Finally, draw it!
			DRAW_2D_SPRITE("CommonMenu", "Common_Medal", sprDot)
		ENDREPEAT
	ENDREPEAT
ENDPROC


/// PURPOSE:
///    Draws the random round results in the top right corner.
/// PARAMS:
///    bDrawBlank - Should draw a normal round as if it hasn't been played previously.
PROC RANGEMP_MAINMENU_DRAW_RANDOM(RangeMP_MenuData & sMenuInfo, BOOL bDrawBlank, RangeMP_PlayerData & sPlayerData[], INT iRedClientID, INT iBlueClientID, RangeMP_PlayerBD & playerBD[] )
	// Always draw the targets.
	DRAW_2D_SPRITE("SRange_Chal", "Chal_Random", sMenuInfo.uiPlacement.SpritePlacement[RANGEMP_MEDAL_TITLE_IMAGE])
	UNUSED_PARAMETER( iRedClientID )
	UNUSED_PARAMETER( iBlueClientID )
	UNUSED_PARAMETER( sPlayerData )

	IF NOT bDrawBlank
		
		// Okay, now draw a little X for each hit we've got.
		SPRITE_PLACEMENT sprCross
		SET_SPRITE_HUD_COLOUR(sprCross, HUD_COLOUR_BLACK)
		sprCross.a = 255
		sprCross.w = 0.0100
		sprCross.h = 0.0180
		
		INT idx, iArray
		INT iParticipantID
		REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipantID
			REPEAT RANGE_MAX_STORED_IMPACT_ARRAYS iArray
				REPEAT RANGE_MAX_STORED_IMPACT_COORDS idx
					
					IF NOT IS_VECTOR_ZERO( playerBD[ iParticipantID ].vImpactCoordArray[ iArray ][ idx ] )
						sprCross.a = 255
						sprCross.w = 0.0100
						sprCross.h = 0.0180
						
						FLOAT fBaseX = PICK_FLOAT( INT_TO_ENUM( RANGE_IMPACT_COORD_ARRAY, iArray ) = RICA_ORANGE_IMPACTS, 0.652, 0.751 )
						FLOAT fBaseY = PICK_FLOAT( INT_TO_ENUM( RANGE_IMPACT_COORD_ARRAY, iArray ) = RICA_ORANGE_IMPACTS, 0.329, 0.329 )
						
						FLOAT fImpactX = ( playerBD[iParticipantID].vImpactCoordArray[iArray][idx].x * RANGE_IMPACT_COORD_SCALAR_X )
						FLOAT fImpactY = ( playerBD[iParticipantID].vImpactCoordArray[iArray][idx].y * RANGE_IMPACT_COORD_SCALAR_Y )
						
						BOOL bClamp = FALSE
						// The z holds the dot product of the forward vector, this tells us if the shot is in the same plane as the target
						IF playerBD[iParticipantID].vImpactCoordArray[iArray][idx].z > 0.05
						OR playerBD[iParticipantID].vImpactCoordArray[iArray][idx].z < -0.05
							bClamp = TRUE
						ENDIF
						
						sMenuInfo.fRandomMenu_X[idx][iArray] = fBaseX + PICK_FLOAT( bClamp, CLAMP( fImpactX, RANGE_IMPACT_COORD_MIN, RANGE_IMPACT_COORD_MAX ), fImpactX )
						sMenuInfo.fRandomMenu_Y[idx][iArray] = fBaseY + PICK_FLOAT( bClamp, CLAMP( fImpactY, RANGE_IMPACT_COORD_MIN, RANGE_IMPACT_COORD_MAX ), fImpactY )
						
						sprCross.x = sMenuInfo.fRandomMenu_X[idx][iArray]
						sprCross.y = sMenuInfo.fRandomMenu_Y[idx][iArray]
						
						IF NOT bIsWidescreen
							FIXUP_SPRITE_FOR_NON_WIDESCREEN(sprCross)
						ENDIF
						DRAW_2D_SPRITE("SRange_Gen", "Hit_Cross", sprCross)
					ENDIF
						
				ENDREPEAT
			ENDREPEAT
		ENDREPEAT
		
		#IF IS_DEBUG_BUILD
		TEXT_LABEL_63 texDisplay
		VECTOR vDraw
		FLOAT fDisplayX, fDisplayY
		REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipantID
			REPEAT RANGE_MAX_STORED_IMPACT_ARRAYS iArray
				REPEAT RANGE_MAX_STORED_IMPACT_COORDS idx
					
					texDisplay = "x="
					texDisplay += ROUND( playerBD[iParticipantID].vImpactCoordArray[iArray][idx].x * 100 )
					texDisplay += ", y="
					texDisplay += ROUND( playerBD[iParticipantID].vImpactCoordArray[iArray][idx].y * 100 )
					texDisplay += ", z="
					texDisplay += ROUND( playerBD[iParticipantID].vImpactCoordArray[iArray][idx].z * 100 )
					
					fDisplayX = 0.6 + ( 0.15 * iArray )
					fDisplayY = 0.2 + ( 0.15 * iParticipantID ) + ( 0.0125 * idx )
					
					vDraw = << fDisplayX, fDisplayY, 0.0 >>
					
					DRAW_DEBUG_TEXT_2D( texDisplay, vDraw )
						
				ENDREPEAT
			ENDREPEAT
		ENDREPEAT
		#ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Draw the covered round targets.
PROC RANGEMP_MAINMENU_DRAW_COVERED(RangeMP_MenuData & sMenuInfo, RangeMP_Round & sRndInfo, BOOL bDrawBlank) 
	// Always going to draw the grids at least.
	//IF bDrawBlank
	//	DRAW_2D_SPRITE("SRange_Chal", "Chal_Covered", sMenuInfo.uiPlacement.SpritePlacement[RANGEMP_MEDAL_TITLE_IMAGE])
	//ELSE
		DRAW_2D_SPRITE("SRange_Chal", "Chal_Covered_Active", sMenuInfo.uiPlacement.SpritePlacement[RANGEMP_MEDAL_TITLE_IMAGE])
		
		SPRITE_PLACEMENT sprDot, sprBack
		sprDot.w = 0.025
		sprDot.h = 0.044
		sprDot.a = 255
	
		// Draw the 4 covered targets where they belong.
		INT idxControl
		REPEAT 4 idxControl
			sprDot.w = 0.025
			sprDot.h = 0.044
			sprDot.a = 255
			// Shift down as we go!
			sprDot.y = (idxControl * 0.0275) + 0.27
			
			IF bDrawBlank
				// Need to position this ourselves, as there won't be any flags set.
				IF (idxControl = 1) OR (idxControl = 3)
					sprDot.x = 0.6875
				ELSE
					sprDot.x = 0.7125
				ENDIF
				
				// Draw the panel back.
				sprBack = sprDot
				sprBack.w = 0.0125
				sprBack.h = 0.022
				SET_SPRITE_HUD_COLOUR(sprBack, HUD_COLOUR_GREYDARK)
				IF NOT bIsWidescreen
					FIXUP_SPRITE_FOR_NON_WIDESCREEN(sprBack)
				ENDIF
				DRAW_2D_SPRITE("MPSRange", "PanelBack", sprBack)
				
				// Draw the dot.
				SET_SPRITE_HUD_COLOUR(sprDot, HUD_COLOUR_RED)
				IF NOT bIsWidescreen
					FIXUP_SPRITE_FOR_NON_WIDESCREEN(sprDot)
				ENDIF
				DRAW_2D_SPRITE("CommonMenu", "Common_Medal", sprDot)
			ELSE
				// Draw the dot.
				IF CHECK_TARGET_FLAG(sRndInfo.oRoundObjects[idxControl], TARG_F_IS_RED)
					sprDot.x = 0.6875
					
					// Draw the panel back.
					sprBack = sprDot
					sprBack.w = 0.0125
					sprBack.h = 0.022
					SET_SPRITE_HUD_COLOUR(sprBack, HUD_COLOUR_GREYDARK)
					IF NOT bIsWidescreen
						FIXUP_SPRITE_FOR_NON_WIDESCREEN(sprBack)
					ENDIF
					DRAW_2D_SPRITE("MPSRange", "PanelBack", sprBack)
				
					SET_SPRITE_HUD_COLOUR(sprDot, HUD_COLOUR_RED)
					IF NOT bIsWidescreen
						FIXUP_SPRITE_FOR_NON_WIDESCREEN(sprDot)
					ENDIF
					DRAW_2D_SPRITE("CommonMenu", "Common_Medal", sprDot)
				ELIF CHECK_TARGET_FLAG(sRndInfo.oRoundObjects[idxControl], TARG_F_IS_BLUE)
					sprDot.x = 0.7125
					
					// Draw the panel back.
					sprBack = sprDot
					sprBack.w = 0.0125
					sprBack.h = 0.022
					SET_SPRITE_HUD_COLOUR(sprBack, HUD_COLOUR_GREYDARK)
					IF NOT bIsWidescreen
						FIXUP_SPRITE_FOR_NON_WIDESCREEN(sprBack)
					ENDIF
					DRAW_2D_SPRITE("MPSRange", "PanelBack", sprBack)
				
					SET_SPRITE_HUD_COLOUR(sprDot, HUD_COLOUR_RED)
					IF NOT bIsWidescreen
						FIXUP_SPRITE_FOR_NON_WIDESCREEN(sprDot)
					ENDIF
					DRAW_2D_SPRITE("CommonMenu", "Common_Medal", sprDot)
				ENDIF
			ENDIF
		ENDREPEAT
	
		// Now we need to draw the dots, starting at the first DOT index. We'll draw one row at a time to keep it simple...
		INT idx
		
		// ORANGE SIDE -- Either orange or grey
		FOR idx = CI_GOAL_1_1 TO CI_GOAL_1_4
			sprDot.w = 0.025
			sprDot.h = 0.044
			sprDot.a = 255
			sprDot.y = 0.270
			sprDot.x = 0.6159 + ((idx % 4) * 0.0155)
			
			sprBack = sprDot
			sprBack.w = 0.0125
			sprBack.h = 0.022
			
			IF bDrawBlank
				// Draw the panel back.
				SET_SPRITE_HUD_COLOUR(sprBack, HUD_COLOUR_GREYDARK)
				IF NOT bIsWidescreen
					FIXUP_SPRITE_FOR_NON_WIDESCREEN(sprBack)
				ENDIF
				DRAW_2D_SPRITE("MPSRange", "PanelBack", sprBack)
				
				// Draw the dot.
				SET_SPRITE_HUD_COLOUR(sprDot, HUD_COLOUR_ORANGE)
				IF NOT bIsWidescreen
					FIXUP_SPRITE_FOR_NON_WIDESCREEN(sprDot)
				ENDIF
				DRAW_2D_SPRITE("CommonMenu", "Common_Medal", sprDot)
			ELSE
				IF NOT CHECK_TARGET_FLAG(sRndInfo.oRoundObjects[idx], TARG_F_DONE)
					// Draw the panel back.
					SET_SPRITE_HUD_COLOUR(sprBack, HUD_COLOUR_GREYDARK)
					IF NOT bIsWidescreen
						FIXUP_SPRITE_FOR_NON_WIDESCREEN(sprBack)
					ENDIF
					DRAW_2D_SPRITE("MPSRange", "PanelBack", sprBack)
					
					// Draw the dot.
					IF NOT (CHECK_TARGET_FLAG(sRndInfo.oRoundObjects[idx], TARG_F_ROTATING_OUT) OR CHECK_TARGET_FLAG(sRndInfo.oRoundObjects[idx], TARG_F_BLOCKED))
						SET_SPRITE_HUD_COLOUR(sprDot, HUD_COLOUR_ORANGE)
						IF NOT bIsWidescreen
							FIXUP_SPRITE_FOR_NON_WIDESCREEN(sprDot)
						ENDIF
						DRAW_2D_SPRITE("CommonMenu", "Common_Medal", sprDot)
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
		FOR idx = CI_GOAL_2_1 TO CI_GOAL_2_4
			sprDot.w = 0.025
			sprDot.h = 0.044
			sprDot.a = 255
			sprDot.y = 0.2975
			sprDot.x = 0.6159 + ((idx % 4) * 0.0155)
			
			sprBack = sprDot
			sprBack.w = 0.0125
			sprBack.h = 0.022
			
			IF bDrawBlank
				// Draw the panel back.
				SET_SPRITE_HUD_COLOUR(sprBack, HUD_COLOUR_GREYDARK)
				IF NOT bIsWidescreen
					FIXUP_SPRITE_FOR_NON_WIDESCREEN(sprBack)
				ENDIF
				DRAW_2D_SPRITE("MPSRange", "PanelBack", sprBack)
			ELSE
				IF NOT CHECK_TARGET_FLAG(sRndInfo.oRoundObjects[idx], TARG_F_DONE)
					// Draw the panel back.
					SET_SPRITE_HUD_COLOUR(sprBack, HUD_COLOUR_GREYDARK)
					IF NOT bIsWidescreen
						FIXUP_SPRITE_FOR_NON_WIDESCREEN(sprBack)
					ENDIF
					DRAW_2D_SPRITE("MPSRange", "PanelBack", sprBack)
					
					// Draw the dot.
					IF NOT (CHECK_TARGET_FLAG(sRndInfo.oRoundObjects[idx], TARG_F_ROTATING_OUT) OR CHECK_TARGET_FLAG(sRndInfo.oRoundObjects[idx], TARG_F_BLOCKED))
						SET_SPRITE_HUD_COLOUR(sprDot, HUD_COLOUR_ORANGE)
						IF NOT bIsWidescreen
							FIXUP_SPRITE_FOR_NON_WIDESCREEN(sprDot)
						ENDIF
						DRAW_2D_SPRITE("CommonMenu", "Common_Medal", sprDot)
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
		FOR idx = CI_GOAL_3_1 TO CI_GOAL_3_4
			sprDot.w = 0.025
			sprDot.h = 0.044
			sprDot.a = 255
			sprDot.y = 0.325
			sprDot.x = 0.6159 + ((idx % 4) * 0.0155)
			
			sprBack = sprDot
			sprBack.w = 0.0125
			sprBack.h = 0.022
				
			IF bDrawBlank
				// Draw the panel back.
				SET_SPRITE_HUD_COLOUR(sprBack, HUD_COLOUR_GREYDARK)
				IF NOT bIsWidescreen
					FIXUP_SPRITE_FOR_NON_WIDESCREEN(sprBack)
				ENDIF
				DRAW_2D_SPRITE("MPSRange", "PanelBack", sprBack)
				
				// Draw the dot.
				SET_SPRITE_HUD_COLOUR(sprDot, HUD_COLOUR_ORANGE)
				IF NOT bIsWidescreen
					FIXUP_SPRITE_FOR_NON_WIDESCREEN(sprDot)
				ENDIF
				DRAW_2D_SPRITE("CommonMenu", "Common_Medal", sprDot)
			ELSE
				IF NOT CHECK_TARGET_FLAG(sRndInfo.oRoundObjects[idx], TARG_F_DONE)
					// Draw the panel back.
					SET_SPRITE_HUD_COLOUR(sprBack, HUD_COLOUR_GREYDARK)
					IF NOT bIsWidescreen
						FIXUP_SPRITE_FOR_NON_WIDESCREEN(sprBack)
					ENDIF
					DRAW_2D_SPRITE("MPSRange", "PanelBack", sprBack)
					
					// Draw the dot.
					IF NOT (CHECK_TARGET_FLAG(sRndInfo.oRoundObjects[idx], TARG_F_ROTATING_OUT) OR CHECK_TARGET_FLAG(sRndInfo.oRoundObjects[idx], TARG_F_BLOCKED))
						SET_SPRITE_HUD_COLOUR(sprDot, HUD_COLOUR_ORANGE)
						IF NOT bIsWidescreen
							FIXUP_SPRITE_FOR_NON_WIDESCREEN(sprDot)
						ENDIF
						DRAW_2D_SPRITE("CommonMenu", "Common_Medal", sprDot)
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
		FOR idx = CI_GOAL_4_1 TO CI_GOAL_4_4
			sprDot.w = 0.025
			sprDot.h = 0.044
			sprDot.a = 255
			sprDot.y = 0.353
			sprDot.x = 0.6159 + ((idx % 4) * 0.0155)
			
			sprBack = sprDot
			sprBack.w = 0.0125
			sprBack.h = 0.022
				
			IF bDrawBlank
				// Draw the panel back.
				SET_SPRITE_HUD_COLOUR(sprBack, HUD_COLOUR_GREYDARK)
				IF NOT bIsWidescreen
					FIXUP_SPRITE_FOR_NON_WIDESCREEN(sprBack)
				ENDIF
				DRAW_2D_SPRITE("MPSRange", "PanelBack", sprBack)
			ELSE
				IF NOT CHECK_TARGET_FLAG(sRndInfo.oRoundObjects[idx], TARG_F_DONE)
					// Draw the panel back.
					SET_SPRITE_HUD_COLOUR(sprBack, HUD_COLOUR_GREYDARK)
					IF NOT bIsWidescreen
						FIXUP_SPRITE_FOR_NON_WIDESCREEN(sprBack)
					ENDIF
					DRAW_2D_SPRITE("MPSRange", "PanelBack", sprBack)
					
					// Draw the dot.
					IF NOT (CHECK_TARGET_FLAG(sRndInfo.oRoundObjects[idx], TARG_F_ROTATING_OUT) OR CHECK_TARGET_FLAG(sRndInfo.oRoundObjects[idx], TARG_F_BLOCKED))
						SET_SPRITE_HUD_COLOUR(sprDot, HUD_COLOUR_ORANGE)
						IF NOT bIsWidescreen
							FIXUP_SPRITE_FOR_NON_WIDESCREEN(sprDot)
						ENDIF
						DRAW_2D_SPRITE("CommonMenu", "Common_Medal", sprDot)
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
		
		// PURPLE SIDE -- Either purple or grey
		FOR idx = CI_GOAL_1_5 TO CI_GOAL_1_8
			sprDot.w = 0.025
			sprDot.h = 0.044
			sprDot.a = 255
			sprDot.y = 0.270
			sprDot.x = 0.7376 + ((idx % 4) * 0.0155)
			
			sprBack = sprDot
			sprBack.w = 0.0125
			sprBack.h = 0.022
				
			IF bDrawBlank
				// Draw the panel back.
				SET_SPRITE_HUD_COLOUR(sprBack, HUD_COLOUR_GREYDARK)
				IF NOT bIsWidescreen
					FIXUP_SPRITE_FOR_NON_WIDESCREEN(sprBack)
				ENDIF
				DRAW_2D_SPRITE("MPSRange", "PanelBack", sprBack)
			ELSE
				IF NOT CHECK_TARGET_FLAG(sRndInfo.oRoundObjects[idx], TARG_F_DONE)
					// Draw the panel back.
					SET_SPRITE_HUD_COLOUR(sprBack, HUD_COLOUR_GREYDARK)
					IF NOT bIsWidescreen
						FIXUP_SPRITE_FOR_NON_WIDESCREEN(sprBack)
					ENDIF
					DRAW_2D_SPRITE("MPSRange", "PanelBack", sprBack)
					
					// Draw the dot.
					IF NOT (CHECK_TARGET_FLAG(sRndInfo.oRoundObjects[idx], TARG_F_ROTATING_OUT) OR CHECK_TARGET_FLAG(sRndInfo.oRoundObjects[idx], TARG_F_BLOCKED))
						SET_SPRITE_HUD_COLOUR(sprDot, HUD_COLOUR_PURPLE)
						IF NOT bIsWidescreen
							FIXUP_SPRITE_FOR_NON_WIDESCREEN(sprDot)
						ENDIF
						DRAW_2D_SPRITE("CommonMenu", "Common_Medal", sprDot)
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
		FOR idx = CI_GOAL_2_5 TO CI_GOAL_2_8
			sprDot.w = 0.025
			sprDot.h = 0.044
			sprDot.a = 255
			sprDot.y = 0.2975
			sprDot.x = 0.7376 + ((idx % 4) * 0.0155)
			
			sprBack = sprDot
			sprBack.w = 0.0125
			sprBack.h = 0.022
			
			IF bDrawBlank
				// Draw the panel back.
				SET_SPRITE_HUD_COLOUR(sprBack, HUD_COLOUR_GREYDARK)
				IF NOT bIsWidescreen
					FIXUP_SPRITE_FOR_NON_WIDESCREEN(sprBack)
				ENDIF
				DRAW_2D_SPRITE("MPSRange", "PanelBack", sprBack)
				
				// Draw the dot.
				SET_SPRITE_HUD_COLOUR(sprDot, HUD_COLOUR_PURPLE)
				IF NOT bIsWidescreen
					FIXUP_SPRITE_FOR_NON_WIDESCREEN(sprDot)
				ENDIF
				DRAW_2D_SPRITE("CommonMenu", "Common_Medal", sprDot)
			ELSE
				IF NOT CHECK_TARGET_FLAG(sRndInfo.oRoundObjects[idx], TARG_F_DONE)
					// Draw the panel back.
					SET_SPRITE_HUD_COLOUR(sprBack, HUD_COLOUR_GREYDARK)
					IF NOT bIsWidescreen
						FIXUP_SPRITE_FOR_NON_WIDESCREEN(sprBack)
					ENDIF
					DRAW_2D_SPRITE("MPSRange", "PanelBack", sprBack)
					
					// Draw the dot.
					IF NOT (CHECK_TARGET_FLAG(sRndInfo.oRoundObjects[idx], TARG_F_ROTATING_OUT) OR CHECK_TARGET_FLAG(sRndInfo.oRoundObjects[idx], TARG_F_BLOCKED))
						SET_SPRITE_HUD_COLOUR(sprDot, HUD_COLOUR_PURPLE)
						IF NOT bIsWidescreen
							FIXUP_SPRITE_FOR_NON_WIDESCREEN(sprDot)
						ENDIF
						DRAW_2D_SPRITE("CommonMenu", "Common_Medal", sprDot)
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
		FOR idx = CI_GOAL_3_5 TO CI_GOAL_3_8
			sprDot.w = 0.025
			sprDot.h = 0.044
			sprDot.a = 255
			sprDot.y = 0.325
			sprDot.x = 0.7376 + ((idx % 4) * 0.0155)
			
			sprBack = sprDot
			sprBack.w = 0.0125
			sprBack.h = 0.022
				
			IF bDrawBlank
				// Draw the panel back.
				SET_SPRITE_HUD_COLOUR(sprBack, HUD_COLOUR_GREYDARK)
				IF NOT bIsWidescreen
					FIXUP_SPRITE_FOR_NON_WIDESCREEN(sprBack)
				ENDIF
				DRAW_2D_SPRITE("MPSRange", "PanelBack", sprBack)
			ELSE
				IF NOT CHECK_TARGET_FLAG(sRndInfo.oRoundObjects[idx], TARG_F_DONE)
					// Draw the panel back.
					SET_SPRITE_HUD_COLOUR(sprBack, HUD_COLOUR_GREYDARK)
					IF NOT bIsWidescreen
						FIXUP_SPRITE_FOR_NON_WIDESCREEN(sprBack)
					ENDIF
					DRAW_2D_SPRITE("MPSRange", "PanelBack", sprBack)
					
					// Draw the dot.
					IF NOT (CHECK_TARGET_FLAG(sRndInfo.oRoundObjects[idx], TARG_F_ROTATING_OUT) OR CHECK_TARGET_FLAG(sRndInfo.oRoundObjects[idx], TARG_F_BLOCKED))
						SET_SPRITE_HUD_COLOUR(sprDot, HUD_COLOUR_PURPLE)
						IF NOT bIsWidescreen
							FIXUP_SPRITE_FOR_NON_WIDESCREEN(sprDot)
						ENDIF
						DRAW_2D_SPRITE("CommonMenu", "Common_Medal", sprDot)
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
		FOR idx = CI_GOAL_4_5 TO CI_GOAL_4_8
			sprDot.w = 0.025
			sprDot.h = 0.044
			sprDot.a = 255
			sprDot.y = 0.353
			sprDot.x = 0.7376 + ((idx % 4) * 0.0155)
			
			sprBack = sprDot
			sprBack.w = 0.0125
			sprBack.h = 0.022
				
			IF bDrawBlank
				// Draw the panel back.
				SET_SPRITE_HUD_COLOUR(sprBack, HUD_COLOUR_GREYDARK)
				IF NOT bIsWidescreen
					FIXUP_SPRITE_FOR_NON_WIDESCREEN(sprBack)
				ENDIF
				DRAW_2D_SPRITE("MPSRange", "PanelBack", sprBack)
				
				// Draw the dot.
				SET_SPRITE_HUD_COLOUR(sprDot, HUD_COLOUR_PURPLE)
				IF NOT bIsWidescreen
					FIXUP_SPRITE_FOR_NON_WIDESCREEN(sprDot)
				ENDIF
				DRAW_2D_SPRITE("CommonMenu", "Common_Medal", sprDot)
			ELSE
				IF NOT CHECK_TARGET_FLAG(sRndInfo.oRoundObjects[idx], TARG_F_DONE)
					// Draw the panel back.
					SET_SPRITE_HUD_COLOUR(sprBack, HUD_COLOUR_GREYDARK)
					IF NOT bIsWidescreen
						FIXUP_SPRITE_FOR_NON_WIDESCREEN(sprBack)
					ENDIF
					DRAW_2D_SPRITE("MPSRange", "PanelBack", sprBack)
					
					// Draw the dot.
					IF NOT (CHECK_TARGET_FLAG(sRndInfo.oRoundObjects[idx], TARG_F_ROTATING_OUT) OR CHECK_TARGET_FLAG(sRndInfo.oRoundObjects[idx], TARG_F_BLOCKED))
						SET_SPRITE_HUD_COLOUR(sprDot, HUD_COLOUR_PURPLE)
						IF NOT bIsWidescreen
							FIXUP_SPRITE_FOR_NON_WIDESCREEN(sprDot)
						ENDIF
						DRAW_2D_SPRITE("CommonMenu", "Common_Medal", sprDot)
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
	//ENDIF
ENDPROC


/// PURPOSE:
///    Draws the main menu for the MP shooting range.
PROC PROCESS_RANGEMP_MAINMENU(RangeMP_CoreData & sCoreInfo, RangeMP_MenuData & sMenuInfo, RangeMP_ServerBD & sServerBD, INT iClientID, RangeMP_PlayerData & sPlayerData[],
		RangeMP_PlayerBD & sPlayerBD[], RangeMP_Round & sRndInfo, RangeMP_ParticipantInfo & sParticipantInfo, BOOL bContinueRunningTimer = TRUE, BOOL bShowingResults = FALSE)
	
	bIsWidescreen = GET_IS_WIDESCREEN()
	
	// Cache and check the aspect ratio, re-init menu sizes on changes.
	FLOAT fAspectRatio = GET_SCREEN_ASPECT_RATIO()
	IF sMenuInfo.fCachedAspectRatio <> fAspectRatio
		INIT_SCREEN_RANGEMP_MENU(sMenuInfo.uiPlacement)
		CDEBUG1LN(DEBUG_SHOOTRANGE, "PROCESS_RANGEMP_MAINMENU fCachedAspectRatio changed, refreshing the menu")
		CDEBUG2LN(DEBUG_SHOOTRANGE, "fCachedAspectRatio = ", sMenuInfo.fCachedAspectRatio)
		CDEBUG2LN(DEBUG_SHOOTRANGE, "fAspectRatio       = ", fAspectRatio)
	ENDIF
	sMenuInfo.fCachedAspectRatio = fAspectRatio
	
	#IF IS_DEBUG_BUILD
	INT iX, iY
	GET_ACTUAL_SCREEN_RESOLUTION(iX, iY)
	FLOAT fActualResolution = TO_FLOAT(iX) / TO_FLOAT(iY)
	
	FLOAT fDispAtX = 0.8
	FLOAT fDispAtY = 0.5
	FLOAT fDispAdd = 0.0125
	TEXT_LABEL_63 texDisp = PICK_STRING( bIsWidescreen, "bIsWidescreen = TRUE", "bIsWidescreen = FALSE" )
	DRAW_DEBUG_TEXT_2D( texDisp, (<< fDispAtX, fDispAtY, 0.0 >>) )
	fDispAtY += fDispAdd
	
	texDisp = "fActualResolution = "
	texDisp += GET_STRING_FROM_FLOAT( fActualResolution )
	DRAW_DEBUG_TEXT_2D( texDisp, (<< fDispAtX, fDispAtY, 0.0 >>) )
	fDispAtY += fDispAdd
	
	FLOAT fNativeResolution = TO_FLOAT(k_RANGE_DEFAULT_WIDTH) / TO_FLOAT(k_RANGE_DEFAULT_HEIGHT)
	texDisp = "fNativeResolution = "
	texDisp += GET_STRING_FROM_FLOAT( fNativeResolution )
	DRAW_DEBUG_TEXT_2D( texDisp, (<< fDispAtX, fDispAtY, 0.0 >>) )
	fDispAtY += fDispAdd
	
	texDisp = "iScreenX = "
	texDisp += iX
	DRAW_DEBUG_TEXT_2D( texDisp, (<< fDispAtX, fDispAtY, 0.0 >>) )
	fDispAtY += fDispAdd
	
	texDisp = "iScreenY = "
	texDisp += iY
	DRAW_DEBUG_TEXT_2D( texDisp, (<< fDispAtX, fDispAtY, 0.0 >>) )
	fDispAtY += fDispAdd
	
	texDisp = "fAspectRatio = "
	texDisp += GET_STRING_FROM_FLOAT(fAspectRatio)
	DRAW_DEBUG_TEXT_2D( texDisp, (<< fDispAtX, fDispAtY, 0.0 >>) )
	fDispAtY += fDispAdd
	
	IF fAspectRatio >=   RANGE_RESOLUTION_21x9			texDisp = "RANGE_TEXT_SCALE_21x9"
	ELIF fAspectRatio >= RANGE_RESOLUTION_17x9			texDisp = "RANGE_TEXT_SCALE_17x9"
	ELIF fAspectRatio >= RANGE_RESOLUTION_16x9			texDisp = "RANGE_TEXT_SCALE_16x9"
	ELIF fAspectRatio >= RANGE_RESOLUTION_5x3			texDisp = "RANGE_TEXT_SCALE_5x3"
	ELIF fAspectRatio >= RANGE_RESOLUTION_16x10			texDisp = "RANGE_TEXT_SCALE_16x10"
	ELIF fAspectRatio >= RANGE_RESOLUTION_3x2			texDisp = "RANGE_TEXT_SCALE_3x2"
	ELIF fAspectRatio >= RANGE_RESOLUTION_4x3			texDisp = "RANGE_TEXT_SCALE_4x3"
	ELIF fAspectRatio >= RANGE_RESOLUTION_5x4			texDisp = "RANGE_TEXT_SCALE_5x4"
	ELSE texDisp = "Default Text Scale"
	ENDIF
	
	DRAW_DEBUG_TEXT_2D( texDisp, (<< fDispAtX, fDispAtY, 0.0 >>) )
	fDispAtY += fDispAdd
	
	IF IS_DEBUG_KEY_JUST_RELEASED(KEY_F5, KEYBOARD_MODIFIER_CTRL, "refreshUIPlacement")
		INIT_SCREEN_RANGEMP_MENU(sMenuInfo.uiPlacement)
		CPRINTLN(DEBUG_SHOOTRANGE, "PROCESS_RANGEMP_MAINMENU reinitialized uiplacement")
	ENDIF
	
	#ENDIF
	
	// See if we're to draw the leaderboard, or the menu.
	IF bShowingResults
		// Populate the range social club leaderboard		
		IF IS_RANGE_MP_MENU_FLAG_SET(sMenuInfo, RMPM_SHOWING_SCLB)
			// Draw the range Social Club Leaderboard UI
			RANGE_DISPLAY_SOCIAL_CLUB_LEADERBOARD(sMenuInfo.uiLeaderboard, sCoreInfo.rangeLBD)
			INT iScreenX, iScreenY
			GET_SCREEN_RESOLUTION(iScreenX, iScreenY)
			DRAW_MENU_HELP_SCALEFORM(iScreenX, -1, 0, FALSE)
		
			EXIT
		ENDIF
	ENDIF
	
	BOOL bInControl = (iClientID = SERVER_GET_PLAYER_IN_CONTROL(sServerBD))
	
	IF NOT bIsWidescreen AND RANGE_WIDESCREEN_FORMAT <> RANGE_SET_WIDESCREEN_FORMAT_OFF
		SET_WIDESCREEN_FORMAT(INT_TO_ENUM(eWIDESCREEN_FORMAT, RANGE_WIDESCREEN_FORMAT))
	ENDIF
	
	
	INT iRedPlayer = sParticipantInfo.iShooters[RED_PLAYER_CLIENT_ID]
	INT iBluePlayer = sParticipantInfo.iShooters[BLUE_PLAYER_CLIENT_ID]
	
	IF iRedPlayer = k_InvalidParticipantID
		CDEBUG3LN( DEBUG_SHOOTRANGE, "PROCESS_RANGEMP_MAINMENU - iRedPlayer = k_InvalidParticipantID, this is potentially fatal." )
	ENDIF	
	IF iBluePlayer = k_InvalidParticipantID
		CDEBUG3LN( DEBUG_SHOOTRANGE, "PROCESS_RANGEMP_MAINMENU - iBluePlayer = k_InvalidParticipantID, this is potentially fatal." )
	ENDIF	
	
	
	// Draw the background gradient.
	DRAW_2D_SPRITE("Shared", "BGGradient_32x1024", sMenuInfo.uiPlacement.SpritePlacement[RANGEMP_BACKGROUND], FALSE, HIGHLIGHT_OPTION_NORMAL, GFX_ORDER_BEFORE_HUD)
	
	// Draw the title
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD) //make sure help text draws over the menu
	SET_TEXT_COLOUR_WITH_HUD_COLOUR(sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_TITLE, HUD_COLOUR_WHITE)
	DRAW_RANGE_TEXT(sMenuInfo.uiPlacement.TextPlacement[RANGEMP_MENU_MAIN_TITLE], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_TITLE, "SHR_TITLE")
	
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
	
	// ==========================================================================================================
	
	// Draw the weapon column title bar
	DRAW_RECTANGLE(sMenuInfo.uiPlacement.RectPlacement[RANGEMP_WEAPON_BACKGROUD])
	DRAW_RECTANGLE(sMenuInfo.uiPlacement.RectPlacement[RANGEMP_WEAPON_EDGING])
	DRAW_RECTANGLE(sMenuInfo.uiPlacement.RectPlacement[RANGEMP_WEAPON_IMAGE_BACKGROUD])
	
	// Weapon column header
	SET_TEXT_BLACK(sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_PRIMARY)
	DRAW_RANGE_TEXT_WITH_ALIGNMENT(sMenuInfo.uiPlacement.TextPlacement[RANGEMP_WEAPON_TITLE], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_PRIMARY, "SHR_CHOOSE_WEAP", TRUE, FALSE)
	
	// Draw a weapon image based on the category selected.
	RANGE_MP_MENU_INDEX eCurMenu = RANGE_MP_GET_ACTIVE_MENU_INDEX(sMenuInfo)
	IF (eCurMenu = RANGE_MP_MENU_CATEGORIES) AND bInControl
		RANGE_WEAPON_CATEGORY eTempCat = INT_TO_ENUM(RANGE_WEAPON_CATEGORY, sMenuInfo.sMenu[RANGE_MP_MENU_CATEGORIES].iCurElement)
		STRING sTextureDictionary, sSpriteName
		SWITCH (eTempCat)
			CASE WEAPCAT_PISTOL
				PIXEL_POSITION_AND_SIZE_SPRITE(sMenuInfo.uiPlacement.SpritePlacement[RANGEMP_WEAPON_TITLE_IMAGE], 384.0000, 224.0000, 230.4, 230.4)
				sTextureDictionary = "SRange_Weap"
				sSpriteName = "Weap_HG_1"
			BREAK
			CASE WEAPCAT_SMG
				PIXEL_POSITION_AND_SIZE_SPRITE(sMenuInfo.uiPlacement.SpritePlacement[RANGEMP_WEAPON_TITLE_IMAGE], 384.0000, 224.0000, 256.0000, 256.0000)
				sTextureDictionary = "SRange_Weap2"
				sSpriteName = "Weap_SMG_1"
			BREAK
			CASE WEAPCAT_AR
				PIXEL_POSITION_AND_SIZE_SPRITE(sMenuInfo.uiPlacement.SpritePlacement[RANGEMP_WEAPON_TITLE_IMAGE], 384.0000, 224.0000, 256.0000, 256.0000)
				sTextureDictionary = "SRange_Weap"
				sSpriteName = "Weap_AR_1"
			BREAK
			CASE WEAPCAT_SHOTGUN
				PIXEL_POSITION_AND_SIZE_SPRITE(sMenuInfo.uiPlacement.SpritePlacement[RANGEMP_WEAPON_TITLE_IMAGE], 384.0000, 224.0000, 256.0000, 256.0000)
				sTextureDictionary = "SRange_Weap2"
				sSpriteName = "Weap_SG_1"
			BREAK
			CASE WEAPCAT_LIGHT_MG
				PIXEL_POSITION_AND_SIZE_SPRITE(sMenuInfo.uiPlacement.SpritePlacement[RANGEMP_WEAPON_TITLE_IMAGE], 384.0000, 224.0000, 256.0000, 256.0000)
				sTextureDictionary = "SRange_Weap2"
				sSpriteName = "Weap_LMG_1"
			BREAK
			CASE WEAPCAT_MINIGUN
				PIXEL_POSITION_AND_SIZE_SPRITE(sMenuInfo.uiPlacement.SpritePlacement[RANGEMP_WEAPON_TITLE_IMAGE], 384.0000, 224.0000, 256.0000, 256.0000)
				sTextureDictionary = "SRange_Gen"
				sSpriteName = "Weapcat_HVY"
			BREAK
		ENDSWITCH
		IF NOT bIsWidescreen
			FIXUP_SPRITE_FOR_NON_WIDESCREEN(sMenuInfo.uiPlacement.SpritePlacement[RANGEMP_WEAPON_TITLE_IMAGE])
		ENDIF
		DRAW_2D_SPRITE(sTextureDictionary, sSpriteName, sMenuInfo.uiPlacement.SpritePlacement[RANGEMP_WEAPON_TITLE_IMAGE])
	ELIF bInControl OR bShowingResults
		// If we're not in the weapon category menu, draw the image for the weapon we're on.
		RANGE_WEAPON_CATEGORY eTempCat = INT_TO_ENUM(RANGE_WEAPON_CATEGORY, sMenuInfo.sMenu[RANGE_MP_MENU_CATEGORIES].iCurElement)
		TEXT_LABEL_15 szDirName
		TEXT_LABEL_15 szWeapName = GET_RANGE_MP_WEAPON_SPRITE_NAME(eTempCat, sMenuInfo.sMenu[ RANGE_MP_MENU_WEAPONS ].eWeapons[ sMenuInfo.sMenu[RANGE_MP_MENU_WEAPONS].iCurElement ], szDirName)
		
		// Pistols need to be downsized 10%.
		IF (eCurMenu > RANGE_MP_MENU_CATEGORIES)
		AND (eTempCat = WEAPCAT_PISTOL)
			PIXEL_POSITION_AND_SIZE_SPRITE(sMenuInfo.uiPlacement.SpritePlacement[RANGEMP_WEAPON_TITLE_IMAGE], 384.0000, 224.0000, 230.4, 230.4)
		ELSE
			PIXEL_POSITION_AND_SIZE_SPRITE(sMenuInfo.uiPlacement.SpritePlacement[RANGEMP_WEAPON_TITLE_IMAGE], 384.0000, 224.0000, 256.0000, 256.0000)			
		ENDIF
		IF NOT bIsWidescreen
			FIXUP_SPRITE_FOR_NON_WIDESCREEN(sMenuInfo.uiPlacement.SpritePlacement[RANGEMP_WEAPON_TITLE_IMAGE])
		ENDIF
		DRAW_DEBUG_TEXT_2D( szDirName, (<< sMenuInfo.uiPlacement.SpritePlacement[RANGEMP_WEAPON_TITLE_IMAGE].x, sMenuInfo.uiPlacement.SpritePlacement[RANGEMP_WEAPON_TITLE_IMAGE].y - 0.0125, 0.0 >>) )
		DRAW_DEBUG_TEXT_2D( szWeapName, (<< sMenuInfo.uiPlacement.SpritePlacement[RANGEMP_WEAPON_TITLE_IMAGE].x, sMenuInfo.uiPlacement.SpritePlacement[RANGEMP_WEAPON_TITLE_IMAGE].y, 0.0 >>) )
		DRAW_2D_SPRITE(szDirName, szWeapName, sMenuInfo.uiPlacement.SpritePlacement[RANGEMP_WEAPON_TITLE_IMAGE])
	ELSE
		DRAW_2D_SPRITE("SRange_Gen", "Weapcat_HG", sMenuInfo.uiPlacement.SpritePlacement[RANGEMP_WEAPON_TITLE_IMAGE])
	ENDIF
	
	// Dynamic draw for weapons & categories
	INT iCurrentMenuSlot = 0
	RANGE_WEAPON_CATEGORY iCategoryIndex
	RANGE_WEAPON_TYPE iWeaponIndex
	RANGEMP_SCREEN_RECT eCurrentRect
	RANGEMP_SCREEN_TEXT eCurrentText
	RANGEMP_SCREEN_SPRITE eCurrentSprite
	
	INT iNumLoops = ENUM_TO_INT( NUM_WEAPON_CATS ) - 1		// Railgun is SP only.

	REPEAT iNumLoops iCategoryIndex
		eCurrentRect = RANGEMP_WEAPON_ITEM_BG_1 + INT_TO_ENUM(RANGEMP_SCREEN_RECT, iCurrentMenuSlot)
		eCurrentText = RANGEMP_WEAPON_ITEM_TEXT_1 + INT_TO_ENUM(RANGEMP_SCREEN_TEXT, iCurrentMenuSlot)	
		eCurrentSprite = RANGEMP_WEAPON_IMG_1 + INT_TO_ENUM(RANGEMP_SCREEN_SPRITE, iCurrentMenuSlot)	
		
		IF (eCurMenu = RANGE_MP_MENU_CATEGORIES)
			IF (iCategoryIndex = INT_TO_ENUM(RANGE_WEAPON_CATEGORY, sMenuInfo.sMenu[RANGE_MP_MENU_CATEGORIES].iCurElement)) AND (bInControl OR bShowingResults)
				SET_RECT_TO_THIS_HUD_COLOUR(sMenuInfo.uiPlacement.RectPlacement[eCurrentRect], HUD_COLOUR_WHITE, TRUE)
				SET_TEXT_BLACK(sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
			ELSE
				SET_TEXT_WHITE(sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
			ENDIF
		ELSE
//			IF (iCategoryIndex = INT_TO_ENUM(RANGE_WEAPON_CATEGORY, sMenuInfo.sMenu[RANGE_MP_MENU_CATEGORIES].iCurElement)) AND (bInControl OR bShowingResults)
//				SET_RECT_TO_THIS_HUD_COLOUR(sMenuInfo.uiPlacement.RectPlacement[eCurrentRect], HUD_COLOUR_WHITE, TRUE)
//				SET_TEXT_BLACK(sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
//			ELSE
				SET_RECT_TO_THIS_HUD_COLOUR(sMenuInfo.uiPlacement.RectPlacement[eCurrentRect], HUD_COLOUR_PAUSE_BG, TRUE)
				SET_TEXT_WHITE(sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
//			ENDIF
		ENDIF
		DRAW_RECTANGLE(sMenuInfo.uiPlacement.RectPlacement[eCurrentRect])
		DRAW_RANGE_TEXT(sMenuInfo.uiPlacement.TextPlacement[eCurrentText], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, GET_CATEGORY_STRING_INFO(iCategoryIndex)) 

		// Reset things that might have been adjusted
		SET_RECT_TO_THIS_HUD_COLOUR(sMenuInfo.uiPlacement.RectPlacement[eCurrentRect], HUD_COLOUR_PAUSE_BG, TRUE)
		SET_TEXT_WHITE(sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
		SET_TEXT_WHITE(sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY)
		iCurrentMenuSlot++
		
		// Responsible for drawing the weapons in the category.
		IF (eCurMenu != RANGE_MP_MENU_CATEGORIES) AND (bInControl OR bShowingResults)
			IF (iCategoryIndex = INT_TO_ENUM(RANGE_WEAPON_CATEGORY, sMenuInfo.sMenu[RANGE_MP_MENU_CATEGORIES].iCurElement)) AND (bInControl OR bShowingResults)
				// Draw the little down arrow.
				SET_SPRITE_WHITE(sMenuInfo.uiPlacement.SpritePlacement[eCurrentSprite])
				DRAW_2D_SPRITE("Shared", "MenuArrow_32", sMenuInfo.uiPlacement.SpritePlacement[eCurrentSprite])
				
				REPEAT sMenuInfo.sMenu[RANGE_MP_MENU_WEAPONS].iNumElements iWeaponIndex
					eCurrentRect = RANGEMP_WEAPON_ITEM_BG_1 + INT_TO_ENUM(RANGEMP_SCREEN_RECT, iCurrentMenuSlot)
					eCurrentText = RANGEMP_WEAPON_ITEM_TEXT_1 + INT_TO_ENUM(RANGEMP_SCREEN_TEXT, iCurrentMenuSlot)
					
					IF (eCurMenu = RANGE_MP_MENU_WEAPONS)
						IF (iWeaponIndex = INT_TO_ENUM(RANGE_WEAPON_TYPE, sMenuInfo.sMenu[RANGE_MP_MENU_WEAPONS].iCurElement))
							SET_RECT_TO_THIS_HUD_COLOUR(sMenuInfo.uiPlacement.RectPlacement[eCurrentRect], HUD_COLOUR_WHITE, TRUE)
							SET_TEXT_BLACK(sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
						ELSE
							SET_RECT_TO_THIS_HUD_COLOUR(sMenuInfo.uiPlacement.RectPlacement[eCurrentRect], HUD_COLOUR_PAUSE_BG, TRUE)
							SET_TEXT_WHITE(sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
						ENDIF
					ELSE
						// We're not in the weapons menu. Highlighted item is red w/ black text. Everything else is dimmed w/ grey text.
						IF (iWeaponIndex = INT_TO_ENUM(RANGE_WEAPON_TYPE, sMenuInfo.sMenu[RANGE_MP_MENU_WEAPONS].iCurElement))
							SET_RECT_TO_THIS_HUD_COLOUR(sMenuInfo.uiPlacement.RectPlacement[eCurrentRect], HUD_COLOUR_WHITE, TRUE)
							SET_TEXT_BLACK(sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
						ELSE
							SET_RECT_TO_THIS_HUD_COLOUR(sMenuInfo.uiPlacement.RectPlacement[eCurrentRect], HUD_COLOUR_PAUSE_BG, TRUE)
							SET_TEXT_WHITE(sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
						ENDIF
					ENDIF
					DRAW_RECTANGLE(sMenuInfo.uiPlacement.RectPlacement[eCurrentRect])
					SET_TEXT_TABBED(sMenuInfo.uiPlacement.TextPlacement[eCurrentText])

					// Drawing expanded weapon names.
					TEXT_LABEL texTemp = GET_WEAPON_NAME( sMenuInfo.sMenu[ RANGE_MP_MENU_WEAPONS ].eWeapons[ iWeaponIndex ] )
					DRAW_DEBUG_TEXT_2D( texTemp, (<< sMenuInfo.uiPlacement.TextPlacement[eCurrentText].x, sMenuInfo.uiPlacement.TextPlacement[eCurrentText].y, 0.0 >>) )
					DRAW_RANGE_TEXT(sMenuInfo.uiPlacement.TextPlacement[eCurrentText], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, texTemp )

					// Reset things that might have been adjusted
					SET_TEXT_UNTABBED(sMenuInfo.uiPlacement.TextPlacement[eCurrentText])
					SET_TEXT_WHITE(sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
					SET_TEXT_WHITE(sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY)
					iCurrentMenuSlot++
				ENDREPEAT
			ENDIF
		ELSE
			// Okay, our current menu is categories. Need to place a '+' on the right hand side of the row, to symbolize that we can expand.
			IF (iCategoryIndex = INT_TO_ENUM(RANGE_WEAPON_CATEGORY, sMenuInfo.sMenu[RANGE_MP_MENU_CATEGORIES].iCurElement)) AND (bInControl OR bShowingResults)
				SET_SPRITE_BLACK(sMenuInfo.uiPlacement.SpritePlacement[eCurrentSprite])
				DRAW_2D_SPRITE("Shared", "MenuPlus_32", sMenuInfo.uiPlacement.SpritePlacement[eCurrentSprite])
			ENDIF
		ENDIF
	ENDREPEAT

	iNumMenuElements = iCurrentMenuSlot // Needed for mouse menu support
	
	// ==================================================================================================
	
	// Challenge column title bar
	DRAW_RECTANGLE(sMenuInfo.uiPlacement.RectPlacement[RANGEMP_CHALLENGE_BACKGROUND])
	DRAW_RECTANGLE(sMenuInfo.uiPlacement.RectPlacement[RANGEMP_CHALLENGE_EDGING])
	DRAW_RECTANGLE(sMenuInfo.uiPlacement.RectPlacement[RANGEMP_CHALLENGE_IMAGE_BACKGROUND])
	
	// Challenge column header
	SET_TEXT_BLACK(sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_PRIMARY)
	DRAW_RANGE_TEXT_WITH_ALIGNMENT(sMenuInfo.uiPlacement.TextPlacement[RANGEMP_CHALLENGE_TITLE], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_PRIMARY, "SHR_CHALLENGES", TRUE, FALSE)
	
	// Draw the LSGC logo w/ wins on either side.
	DRAW_2D_SPRITE("SRange_Gen", "LSGC_Logo", sMenuInfo.uiPlacement.SpritePlacement[RANGEMP_CHALLENGE_TITLE_IMAGE])

	// Draw wins here, but only if it's not solo.
	SET_TEXT_COLOUR_WITH_HUD_COLOUR(sMenuInfo.uiPlacement.aStyle.TS_TITLE, HUD_COLOUR_ORANGE)
	sMenuInfo.uiPlacement.aStyle.TS_TITLE.YScale = 1.05
	INT iWins = SERVER_GET_CLIENT_TOTAL_MATCH_WINS(sServerBD, sParticipantInfo, iRedPlayer)
	IF (iWins = 0) AND IS_RANGE_ONE_PLAYER()
		DRAW_RANGE_TEXT_WITH_ALIGNMENT(sMenuInfo.uiPlacement.TextPlacement[RANGEMP_ORANGE_CHALLENGES_WON], sMenuInfo.uiPlacement.aStyle.TS_TITLE, "SHR_ENDASH", TRUE, FALSE)
	ELSE
		DRAW_RANGE_TEXT_WITH_NUMBER(sMenuInfo.uiPlacement.TextPlacement[RANGEMP_ORANGE_CHALLENGES_WON], sMenuInfo.uiPlacement.aStyle.TS_TITLE, "NUMBER", iWins, FONT_CENTRE)
	ENDIF
	
	SET_TEXT_COLOUR_WITH_HUD_COLOUR(sMenuInfo.uiPlacement.aStyle.TS_TITLE, HUD_COLOUR_PURPLE)
	sMenuInfo.uiPlacement.aStyle.TS_TITLE.YScale = 1.05
	iWins = SERVER_GET_CLIENT_TOTAL_MATCH_WINS(sServerBD, sParticipantInfo, iBluePlayer)
	IF (iWins = 0) AND (IS_CLIENT_FLAG_SET(sPlayerBD[iClientID], RANGE_MP_F_IsSpectator) OR IS_RANGE_ONE_PLAYER())
		DRAW_RANGE_TEXT_WITH_ALIGNMENT(sMenuInfo.uiPlacement.TextPlacement[RANGEMP_PURPLE_CHALLENGES_WON], sMenuInfo.uiPlacement.aStyle.TS_TITLE, "SHR_ENDASH", TRUE, FALSE)
	ELSE	
		DRAW_RANGE_TEXT_WITH_NUMBER(sMenuInfo.uiPlacement.TextPlacement[RANGEMP_PURPLE_CHALLENGES_WON], sMenuInfo.uiPlacement.aStyle.TS_TITLE, "NUMBER", iWins, FONT_CENTRE)
	ENDIF
	
	// Challenge column details
	INT iChallengeIndex
	RANGEMP_SCREEN_RECT eCurrentInfoRect
	RANGEMP_SCREEN_TEXT eCurrentInfoText
	TEXT_LABEL_15 challengeTitle, challengeDesc
	REPEAT sMenuInfo.sMenu[RANGE_MP_MENU_CHALLENGES].iNumElements iChallengeIndex
		eCurrentRect = RANGEMP_CHALLENGE_1_TITLE_BG + INT_TO_ENUM(RANGEMP_SCREEN_RECT, iChallengeIndex)
		eCurrentText = RANGEMP_CHALLENGE_CATEGORY_TITLE_1 + INT_TO_ENUM(RANGEMP_SCREEN_TEXT, iChallengeIndex)
		eCurrentInfoRect = RANGEMP_CHALLENGE_1_INFO_BG + INT_TO_ENUM(RANGEMP_SCREEN_RECT, iChallengeIndex)
		eCurrentInfoText = RANGEMP_CHALLENGE_CATEGORY_INFO_1 + INT_TO_ENUM(RANGEMP_SCREEN_TEXT, iChallengeIndex)
		challengeTitle = "SHR_MP_CHALL"
		challengeTitle += iChallengeIndex + 1
		
		IF (iChallengeIndex = sMenuInfo.sMenu[RANGE_MP_MENU_CHALLENGES].iCurElement) AND (bInControl OR bShowingResults)
			IF (eCurMenu = RANGE_MP_MENU_CHALLENGES) 
				// We're on the challenges menu, and there's an item we've selected.
				SET_RECT_TO_THIS_HUD_COLOUR(sMenuInfo.uiPlacement.RectPlacement[eCurrentRect], HUD_COLOUR_WHITE, TRUE)
				SET_TEXT_BLACK(sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
			ELSE
				// This isn't the currently selected item.
				SET_RECT_TO_THIS_HUD_COLOUR(sMenuInfo.uiPlacement.RectPlacement[eCurrentRect], HUD_COLOUR_PAUSE_BG, TRUE)
				SET_TEXT_WHITE(sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
			ENDIF
		ELSE
			// This isn't the currently selected item.
			SET_RECT_TO_THIS_HUD_COLOUR(sMenuInfo.uiPlacement.RectPlacement[eCurrentRect], HUD_COLOUR_PAUSE_BG, TRUE)
			SET_TEXT_WHITE(sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
		ENDIF
		
		// Draw challenge rect and title.
		DRAW_RECTANGLE(sMenuInfo.uiPlacement.RectPlacement[eCurrentRect])
		DRAW_RANGE_TEXT(sMenuInfo.uiPlacement.TextPlacement[eCurrentText], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, challengeTitle)	
		
		// Draw challenge info rect and info.
		DRAW_RECTANGLE(sMenuInfo.uiPlacement.RectPlacement[eCurrentInfoRect])
		SET_TEXT_WRAPPED_TO_RECT(sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY, sMenuInfo.uiPlacement.RectPlacement[eCurrentInfoRect], MINIGAME_X_PADDING_LEFT, MINIGAME_X_PADDING_RIGHT)
		challengeDesc = "SHR_MP_CDESC"
		challengeDesc += iChallengeIndex + 1
		SET_TEXT_LEADING(0)
		DRAW_RANGE_TEXT(sMenuInfo.uiPlacement.TextPlacement[eCurrentInfoText], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_TERTIARY, challengeDesc)
		
		// #611796 - Add winner tally to center column. Only if there's ever been a winner this match.
		IF (SERVER_GET_TOTAL_WINS_FOR_CHALLENGE(sServerBD, INT_TO_ENUM(CHALLENGE_INDEX , iChallengeIndex)) != 0)
			CLEAR_TEXT_WRAPPED(sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_NOTIFICATION)
		ENDIF
	ENDREPEAT
	// =============================================================================================================
	
	// Game Info column. Going to deviate from SP, and use this column to display information so far about the range.
	DRAW_RECTANGLE(sMenuInfo.uiPlacement.RectPlacement[RANGEMP_GAMEINFO_BACKGROUND])
	DRAW_RECTANGLE(sMenuInfo.uiPlacement.RectPlacement[RANGEMP_GAMEINFO_EDGING])
	DRAW_RECTANGLE(sMenuInfo.uiPlacement.RectPlacement[RANGEMP_GAMEINFO_IMAGE_BACKGROUND])
	
	// Award column header
	SET_TEXT_BLACK(sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_PRIMARY)
	DRAW_RANGE_TEXT_WITH_ALIGNMENT(sMenuInfo.uiPlacement.TextPlacement[RANGEMP_AWARDS_TITLE], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_PRIMARY, "SHR_RESULTS", TRUE, FALSE)
	
	// DRAW PLAYER NAMES WITH TAGS:
	// =============================================================================================
	DRAW_RECTANGLE(sMenuInfo.uiPlacement.RectPlacement[RANGEMP_AWARDS_ITEM_BG_PLAYER1])
	DRAW_RECTANGLE(sMenuInfo.uiPlacement.RectPlacement[RANGEMP_AWARDS_P1NAME_BG_BLOCK])
	DRAW_RECTANGLE(sMenuInfo.uiPlacement.RectPlacement[RANGEMP_AWARDS_ITEM_BG_PLAYER2])
	DRAW_RECTANGLE(sMenuInfo.uiPlacement.RectPlacement[RANGEMP_AWARDS_P2NAME_BG_BLOCK])
	
//	#IF IS_DEBUG_BUILD
//	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_UP)
//		DEBUG_Y_NUDGE -= 0.1
//		CDEBUG2LN(DEBUG_SHOOTRANGE, "DEBUG_Y_NUDGE=", DEBUG_Y_NUDGE)
//	ENDIF
//	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_DOWN)
//		DEBUG_Y_NUDGE += 0.1
//		CDEBUG2LN(DEBUG_SHOOTRANGE, "DEBUG_Y_NUDGE=", DEBUG_Y_NUDGE)
//	ENDIF
//	PIXEL_POSITION_TEXT(sMenuInfo.uiPlacement.TextPlacement[RANGEMP_AWARDS_ITEM_TEXT_PLAYER1NAME], 769.0+FLOAT_X_TO_PIXEL(MINIGAME_X_PADDING_LEFT+MINIGAME_X_PADDING_LEFT, 1280, TRUE), 294.700 + DEBUG_Y_NUDGE)
//	PIXEL_POSITION_TEXT(sMenuInfo.uiPlacement.TextPlacement[RANGEMP_AWARDS_ITEM_TEXT_PLAYER2NAME], 769.0+FLOAT_X_TO_PIXEL(MINIGAME_X_PADDING_LEFT+MINIGAME_X_PADDING_LEFT, 1280, TRUE), 321.700 + DEBUG_Y_NUDGE)
//	#ENDIF

	// Orange player's name:
	DRAW_RANGE_TEXT_WITH_PLAYER_NAME(sMenuInfo.uiPlacement.TextPlacement[RANGEMP_AWARDS_ITEM_TEXT_PLAYER1NAME],
		sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_GAMERTAG, GET_PLAYER_NAME(sPlayerBD[iRedPlayer].piCurPlayerID), "STRING", HUD_COLOUR_WHITE, FONT_LEFT)
	
	IF NOT IS_RANGE_ONE_PLAYER() AND iBluePlayer != k_InvalidParticipantID
		// Purple player's name:
		DRAW_RANGE_TEXT_WITH_PLAYER_NAME(sMenuInfo.uiPlacement.TextPlacement[RANGEMP_AWARDS_ITEM_TEXT_PLAYER2NAME],
			sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_GAMERTAG, GET_PLAYER_NAME(sPlayerBD[iBluePlayer].piCurPlayerID), "STRING", HUD_COLOUR_WHITE, FONT_LEFT)
	ELSE
		SET_TEXT_WHITE(sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY)
		DRAW_RANGE_TEXT(sMenuInfo.uiPlacement.TextPlacement[RANGEMP_AWARDS_ITEM_TEXT_PLAYER2NAME], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "SHR_ENDASH")
	ENDIF	
	
	// BACKGROUNDS AND TITLES:
	// =============================================================================================
	DRAW_RECTANGLE(sMenuInfo.uiPlacement.RectPlacement[RANGEMP_AWARDS_BG_ROUNDSWON])
	DRAW_RECTANGLE(sMenuInfo.uiPlacement.RectPlacement[RANGEMP_AWARDS_BG_SCORE])
	DRAW_RECTANGLE(sMenuInfo.uiPlacement.RectPlacement[RANGEMP_AWARDS_BG_FIRED])
	DRAW_RECTANGLE(sMenuInfo.uiPlacement.RectPlacement[RANGEMP_AWARDS_BG_HITS])
	DRAW_RECTANGLE(sMenuInfo.uiPlacement.RectPlacement[RANGEMP_AWARDS_BG_ACCURACY])
	
	SET_TEXT_COLOUR_WITH_HUD_COLOUR(sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, HUD_COLOUR_WHITE)
	DRAW_RANGE_TEXT(sMenuInfo.uiPlacement.TextPlacement[RANGEMP_AWARDS_ITEM_TEXT_ROUNDS_WON], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "SHR_MP_RNDSWON")
	DRAW_RANGE_TEXT(sMenuInfo.uiPlacement.TextPlacement[RANGEMP_AWARDS_ITEM_TEXT_SCORE], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "SHR_MP_SCORE")
	DRAW_RANGE_TEXT(sMenuInfo.uiPlacement.TextPlacement[RANGEMP_AWARDS_ITEM_TEXT_SHOTS_FIRED], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "SHR_MP_FIRED")
	DRAW_RANGE_TEXT(sMenuInfo.uiPlacement.TextPlacement[RANGEMP_AWARDS_ITEM_TEXT_SHOTS_HIT], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "SHR_MP_HITS")
	DRAW_RANGE_TEXT(sMenuInfo.uiPlacement.TextPlacement[RANGEMP_AWARDS_ITEM_TEXT_ACCURACY], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "SHR_MP_ACCURACY")
	
	// CALCULATE STATS:
	// =============================================================================================
	INT iRedRnds, iBlueRnds
	INT iRedScore, iBlueScore
	INT iRedHits, iBlueHits
	INT iRedShots, iBlueShots
	INT iRedAcc, iBlueAcc
	
	IF iRedPlayer != k_InvalidParticipantID
		iRedRnds = SERVER_GET_CLIENT_ROUND_WINS(sServerBD, sParticipantInfo, iRedPlayer)
		iRedScore = SERVER_GET_CLIENT_SCORE(sServerBD, sParticipantInfo, iRedPlayer)
		iRedHits = sPlayerData[iRedPlayer].iShotsHitThisRound
		iRedShots = sPlayerData[iRedPlayer].iShotsFiredThisRound
		IF (sPlayerData[iRedPlayer].iShotsFiredThisRound = 0)
			iRedAcc = 0
		ELSE
			iRedAcc = ROUND((1.0 - (TO_FLOAT(sPlayerData[iRedPlayer].iShotsMissedThisRound) / TO_FLOAT(sPlayerData[iRedPlayer].iShotsFiredThisRound))) * 100.0)
		ENDIF
	ENDIF
	
	IF iBluePlayer != k_InvalidParticipantID
		iBlueRnds = SERVER_GET_CLIENT_ROUND_WINS(sServerBD, sParticipantInfo, iBluePlayer)
		iBlueScore = SERVER_GET_CLIENT_SCORE(sServerBD, sParticipantInfo, iBluePlayer)
		iBlueHits = sPlayerData[iBluePlayer].iShotsHitThisRound
		iBlueShots = sPlayerData[iBluePlayer].iShotsFiredThisRound
		IF (sPlayerData[iBluePlayer].iShotsFiredThisRound = 0)
			iBlueAcc = 0
		ELSE
			iBlueAcc = ROUND((1.0 - (TO_FLOAT(sPlayerData[iBluePlayer].iShotsMissedThisRound) / TO_FLOAT(sPlayerData[iBluePlayer].iShotsFiredThisRound))) * 100.0)
		ENDIF
	ENDIF
		
	BOOL bBlankRound = TRUE
	
	// ORANGE STATS:
	// =============================================================================================
	SET_TEXT_COLOUR_WITH_HUD_COLOUR(sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, HUD_COLOUR_ORANGE)
	IF (iRedRnds = 0)
		DRAW_RANGE_TEXT(sMenuInfo.uiPlacement.TextPlacement[RANGEMP_AWARDS_ITEM_TEXT_ROUNDS_WON_ORANGE], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "SHR_ENDASH")
	ELSE
		DRAW_RANGE_TEXT_WITH_NUMBER(sMenuInfo.uiPlacement.TextPlacement[RANGEMP_AWARDS_ITEM_TEXT_ROUNDS_WON_ORANGE], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "NUMBER", iRedRnds)
	ENDIF
	IF (iRedScore = 0) AND (iBlueScore = 0) AND (iRedShots + iBlueShots = 0)
		DRAW_RANGE_TEXT(sMenuInfo.uiPlacement.TextPlacement[RANGEMP_AWARDS_ITEM_TEXT_SCORE_ORANGE], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "SHR_ENDASH")
		DRAW_RANGE_TEXT(sMenuInfo.uiPlacement.TextPlacement[RANGEMP_AWARDS_ITEM_TEXT_SHOTS_FIRED_ORANGE], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "SHR_ENDASH")
		DRAW_RANGE_TEXT(sMenuInfo.uiPlacement.TextPlacement[RANGEMP_AWARDS_ITEM_TEXT_SHOTS_HIT_ORANGE], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "SHR_ENDASH")
		DRAW_RANGE_TEXT(sMenuInfo.uiPlacement.TextPlacement[RANGEMP_AWARDS_ITEM_TEXT_ACCURACY_ORANGE], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "SHR_ENDASH")
	ELSE
		DRAW_RANGE_TEXT_WITH_NUMBER(sMenuInfo.uiPlacement.TextPlacement[RANGEMP_AWARDS_ITEM_TEXT_SCORE_ORANGE], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "NUMBER", iRedScore)
		DRAW_RANGE_TEXT_WITH_NUMBER(sMenuInfo.uiPlacement.TextPlacement[RANGEMP_AWARDS_ITEM_TEXT_SHOTS_FIRED_ORANGE], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "NUMBER", iRedShots)
		DRAW_RANGE_TEXT_WITH_NUMBER(sMenuInfo.uiPlacement.TextPlacement[RANGEMP_AWARDS_ITEM_TEXT_SHOTS_HIT_ORANGE], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "NUMBER", iRedHits)
		DRAW_RANGE_TEXT_WITH_NUMBER(sMenuInfo.uiPlacement.TextPlacement[RANGEMP_AWARDS_ITEM_TEXT_ACCURACY_ORANGE], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "SHR_MP_PERC", iRedAcc)
		bBlankRound = FALSE
	ENDIF
	
	// PURPLE STATS: 
	// =============================================================================================
	SET_TEXT_COLOUR_WITH_HUD_COLOUR(sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, HUD_COLOUR_PURPLE)
	IF (iBlueRnds = 0)
		DRAW_RANGE_TEXT(sMenuInfo.uiPlacement.TextPlacement[RANGEMP_AWARDS_ITEM_TEXT_ROUNDS_WON_PURPLE], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "SHR_ENDASH")
	ELSE
		DRAW_RANGE_TEXT_WITH_NUMBER(sMenuInfo.uiPlacement.TextPlacement[RANGEMP_AWARDS_ITEM_TEXT_ROUNDS_WON_PURPLE], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "NUMBER", iBlueRnds)
	ENDIF
	
	IF ((iBlueScore = 0) AND (iBlueShots = 0)) OR IS_RANGE_ONE_PLAYER()
		DRAW_RANGE_TEXT(sMenuInfo.uiPlacement.TextPlacement[RANGEMP_AWARDS_ITEM_TEXT_SCORE_PURPLE], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "SHR_ENDASH")
		DRAW_RANGE_TEXT(sMenuInfo.uiPlacement.TextPlacement[RANGEMP_AWARDS_ITEM_TEXT_SHOTS_FIRED_PURPLE], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "SHR_ENDASH")
		DRAW_RANGE_TEXT(sMenuInfo.uiPlacement.TextPlacement[RANGEMP_AWARDS_ITEM_TEXT_SHOTS_HIT_PURPLE], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "SHR_ENDASH")
		DRAW_RANGE_TEXT(sMenuInfo.uiPlacement.TextPlacement[RANGEMP_AWARDS_ITEM_TEXT_ACCURACY_PURPLE], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "SHR_ENDASH")
	ELSE
		DRAW_RANGE_TEXT_WITH_NUMBER(sMenuInfo.uiPlacement.TextPlacement[RANGEMP_AWARDS_ITEM_TEXT_SCORE_PURPLE], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "NUMBER", iBlueScore)
		DRAW_RANGE_TEXT_WITH_NUMBER(sMenuInfo.uiPlacement.TextPlacement[RANGEMP_AWARDS_ITEM_TEXT_SHOTS_FIRED_PURPLE], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "NUMBER", iBlueShots)
		DRAW_RANGE_TEXT_WITH_NUMBER(sMenuInfo.uiPlacement.TextPlacement[RANGEMP_AWARDS_ITEM_TEXT_SHOTS_HIT_PURPLE], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "NUMBER", iBlueHits)
		DRAW_RANGE_TEXT_WITH_NUMBER(sMenuInfo.uiPlacement.TextPlacement[RANGEMP_AWARDS_ITEM_TEXT_ACCURACY_PURPLE], sMenuInfo.uiPlacement.aStyle.TS_MINIGAME_MENU_SECONDARY, "SHR_MP_PERC", iBlueAcc)
		bBlankRound = FALSE
	ENDIF
	
	// If the last round was a score round, draw the appropriate image for it.
	IF (sPlayerBD[iClientID].sRoundDesc.eChallenge = CI_GRID AND bShowingResults) 
	OR (NOT bShowingResults AND sMenuInfo.sMenu[RANGE_MP_MENU_CHALLENGES].iCurElement = ENUM_TO_INT(CI_GRID) AND bInControl)
		RANGEMP_MAINMENU_DRAW_GRID(sMenuInfo, sRndInfo, bBlankRound OR IS_CLIENT_FLAG_SET(sPlayerBD[iClientID], RANGE_MP_F_IsSpectator))
		
	ELIF (sPlayerBD[iClientID].sRoundDesc.eChallenge = CI_RANDOM AND bShowingResults) 
	OR (NOT bShowingResults AND sMenuInfo.sMenu[RANGE_MP_MENU_CHALLENGES].iCurElement = ENUM_TO_INT(CI_RANDOM) AND bInControl)
	OR (eCurMenu != RANGE_MP_MENU_CHALLENGES)
		RANGEMP_MAINMENU_DRAW_RANDOM(sMenuInfo, bBlankRound, sPlayerData, iRedPlayer, iBluePlayer, sPlayerBD)
		
	ELIF (sPlayerBD[iClientID].sRoundDesc.eChallenge = CI_COVERED AND bShowingResults) 
	OR (NOT bShowingResults AND sMenuInfo.sMenu[RANGE_MP_MENU_CHALLENGES].iCurElement = ENUM_TO_INT(CI_COVERED) AND bInControl)
		RANGEMP_MAINMENU_DRAW_COVERED(sMenuInfo, sRndInfo, bBlankRound OR IS_CLIENT_FLAG_SET(sPlayerBD[iClientID], RANGE_MP_F_IsSpectator))
	ENDIF
	IF NOT bInControl AND NOT bShowingResults
		RANGEMP_MAINMENU_DRAW_RANDOM(sMenuInfo, bBlankRound, sPlayerData, iRedPlayer, iBluePlayer, sPlayerBD)
	ENDIF

	IF NOT IS_RANGE_ONE_PLAYER() AND NOT IS_CLIENT_FLAG_SET(sPlayerBD[iClientID], RANGE_MP_F_IsSpectator)
		IF bContinueRunningTimer
			// Display who's picking, if we're the host, it's us.
			// This will change depending on who's making the selection.
			FLOAT fTimerVal = GET_TIMER_IN_SECONDS(sMenuInfo.sMenuTimer)
			HANDLE_RANGE_ROUND_TIMER_SOUNDS(TO_FLOAT(RANGE_MP_MENU_TIMEOUT) - fTimerVal, sRndInfo)
			CDEBUG3LN(DEBUG_SHOOTRANGE, "bContinueRunningTimer, HANDLE_RANGE_ROUND_TIMER_SOUNDS")
			INT iScreenX, iScreenY
			GET_SCREEN_RESOLUTION(iScreenX, iScreenY)
			DRAW_MENU_HELP_SCALEFORM(iScreenX, -1, 0, FALSE)
		ENDIF
	ELSE
		INT iScreenX, iScreenY
		GET_SCREEN_RESOLUTION(iScreenX, iScreenY)
		DRAW_MENU_HELP_SCALEFORM(iScreenX, -1, 0, FALSE)
	ENDIF

	SET_TEXT_WHITE(sMenuInfo.uiPlacement.aStyle.TS_STANDARDMEDIUM)
	
	// Spectators and single players don't get timers.
	IF NOT IS_RANGE_ONE_PLAYER() AND NOT IS_CLIENT_FLAG_SET(sPlayerBD[iClientID], RANGE_MP_F_IsSpectator) 
	AND (GET_CLIENT_RANGE_STATE(sPlayerBD[iClientID]) = RANGE_STATE_MenuUpdate OR GET_CLIENT_RANGE_STATE(sPlayerBD[iClientID]) = RANGE_STATE_DisplayRoundOver)
		// Draw the time remaining.
		FLOAT fTimerVal = GET_TIMER_IN_SECONDS(sMenuInfo.sMenuTimer)
		FLOAT fTimeRem = TO_FLOAT(RANGE_MP_MENU_TIMEOUT) - fTimerVal
		HANDLE_RANGE_ROUND_TIMER_SOUNDS(fTimeRem, sRndInfo)
		INT iTimeRem = ROUND(fTimeRem)

		CDEBUG3LN(DEBUG_SHOOTRANGE, "Time remaining on menu: ", fTimeRem, " --- Rounded: ", iTimeRem)
		SET_INSTRUCTIONAL_BUTTONS_UNDER_HUD_THIS_FRAME(1)
		DRAW_GENERIC_TIMER(iTimeRem * 1000, "MBOX_TIME")
	ENDIF
ENDPROC

