//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	Range_Modern_MP.sc											//
//		AUTHOR			:	Ryan Paradis												//
//		DESCRIPTION		:	Multiplayer indoor shooting range.							//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////
BOOL	DEBUG_ONE_PLAYER = FALSE
FUNC BOOL IS_RANGE_ONE_PLAYER()
	RETURN DEBUG_ONE_PLAYER
ENDFUNC

#IF IS_DEBUG_BUILD
BOOL		RANGE_SHOULD_SHOW_ONSCREEN_TEXT	=	FALSE
BOOL		DEBUG_ALL_DLC					=	FALSE
BOOL		DEBUG_INFINITE_ROUND_TIME		=	FALSE
BOOL		DEBUG_HIDE_RETICLE				=	FALSE
#ENDIF

USING "Range_Public.sch"
USING "commands_network.sch"
USING "net_include.sch"
USING "minigames_helpers.sch"
USING "Minigame_UIInputs.sch"
USING "fmmc_mp_setup_mission.sch"

// Global to everything. Get it once.
PED_INDEX playerPed

USING "Range_Modern_MP.sch"
USING "net_wait_zero.sch"

// MP globals.
RangeMP_ServerBD 	sServerBD
RangeMP_PlayerBD 	sPlayerBD[MP_RANGE_MAX_PLAYERS]

RangeMP_ParticipantInfo		sParticipantInfo

CONST_FLOAT	SOLO_RANGE_WAIT_TIME	5.0

#IF IS_DEBUG_BUILD
/// PURPOSE:
///    Setup the widgets for Range MP
PROC SETUP_RANGE_MP_WIDGETS(RangeMP_MenuData & sMenuInfo)
	START_WIDGET_GROUP("Range MP")
		ADD_WIDGET_BOOL( "RANGE_SHOULD_SHOW_ONSCREEN_TEXT", RANGE_SHOULD_SHOW_ONSCREEN_TEXT )
		ADD_WIDGET_BOOL( "DEBUG_INFINITE_ROUND_TIME", DEBUG_INFINITE_ROUND_TIME )
		ADD_WIDGET_BOOL( "DEBUG_HIDE_RETICLE", DEBUG_HIDE_RETICLE )
		ADD_WIDGET_FLOAT_SLIDER( "RANGE_IMPACT_COORD_SCALAR_X", RANGE_IMPACT_COORD_SCALAR_X, 0.0, 1.0, 0.001 )
		ADD_WIDGET_FLOAT_SLIDER( "RANGE_IMPACT_COORD_SCALAR_Y", RANGE_IMPACT_COORD_SCALAR_Y, 0.0, 1.0, 0.001 )
		ADD_WIDGET_FLOAT_SLIDER( "RANGE_EXIT_WARNING_RADIUS", RANGE_EXIT_WARNING_RADIUS, 0.0, 10.0, 0.5 )
		ADD_WIDGET_FLOAT_SLIDER( "RANGE_IMPACT_COORD_MIN", RANGE_IMPACT_COORD_MIN, -10.0, 10.0, 0.001 )
		ADD_WIDGET_FLOAT_SLIDER( "RANGE_IMPACT_COORD_MAX", RANGE_IMPACT_COORD_MAX, -10.0, 10.0, 0.001 )
		ADD_WIDGET_FLOAT_SLIDER( "Base Text Scale", RANGE_TEXT_BASE, 0.0, 2.0, 0.001 )
		START_NEW_WIDGET_COMBO()
			ADD_TO_WIDGET_COMBO("WIDESCREEN_FORMAT_STRETCH")
			ADD_TO_WIDGET_COMBO("WIDESCREEN_FORMAT_CENTRE")
			ADD_TO_WIDGET_COMBO("WIDESCREEN_FORMAT_LEFT")
			ADD_TO_WIDGET_COMBO("WIDESCREEN_FORMAT_RIGHT")
			ADD_TO_WIDGET_COMBO("WIDESCREEN_FORMAT_AUTO")
			ADD_TO_WIDGET_COMBO("OFF")	// This index must match RANGE_SET_WIDESCREEN_FORMAT_OFF. Change the constant if this widget changes!
		STOP_WIDGET_COMBO("Widescreen Format", RANGE_WIDESCREEN_FORMAT)
		
		CREATE_MEGA_PLACEMENT_WIDGETS( sMenuInfo.uiPlacement )
		
	STOP_WIDGET_GROUP()
ENDPROC
#ENDIF

FUNC INT RANGE_GET_NUM_JOINING_PLAYERS()
	INT iJoiningBitfield = Get_Bitfield_Of_Players_On_Or_Joining_Mission(Get_UniqueID_For_This_Players_Mission(PLAYER_ID()))
	CDEBUG2LN(DEBUG_SHOOTRANGE, "RANGE_GET_NUM_JOINING_PLAYERS :: iJoiningBitfield=", iJoiningBitfield, ", MP_RANGE_MAX_PLAYERS=", MP_RANGE_MAX_PLAYERS)
	INT iPlayerCount = 0
	INT iPlayer = 0
	REPEAT MP_RANGE_MAX_PLAYERS iPlayer
		IF IS_BIT_SET(iJoiningBitfield, iPlayer)
			iPlayerCount++
		ENDIF
	ENDREPEAT
	CDEBUG2LN(DEBUG_SHOOTRANGE, "RANGE_GET_NUM_JOINING_PLAYERS :: iPlayerCount=", iPlayerCount)
	RETURN iPlayerCount
ENDFUNC


/// PURPOSE:
///    The multiplayer script needs to init certain multiplayer settings.
///    RETURNS:		FALSE if the init routine fails to receive initial broadcast data. The shooting range script
///    				will need to clean up if this occurs.
FUNC BOOL PROCESS_INIT(MP_MISSION_DATA fmmcMissionData)
	CDEBUG2LN(DEBUG_SHOOTRANGE, "CLIENT PROCESS_INIT")
	
	//If we are in freemode launch diffrently
	IF (GET_CURRENT_GAMEMODE() = GAMEMODE_FM)
		CDEBUG2LN(DEBUG_SHOOTRANGE, "Launching shooting range in freemode.")
		
		//Freemode mission start details
		INT iPlayerMissionToLoad = FMMC_MINI_GAME_CREATOR_ID
		INT iMissionVariation 	 = 0
		
		//Call the function that controls the setting up of this script
		FMMC_PROCESS_PRE_GAME_COMMON(fmmcMissionData, iPlayerMissionToLoad, iMissionVariation, MP_RANGE_MAX_PLAYERS, default, default, FALSE)
		GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_MG_SHOOTING_RANGE
		SET_BIT(MPSpecGlobals.SCTVData.iBitSet, SCTV_BIT_GLOBAL_REFRESH_BUTTONS)
		
		//Script has started set the global broadcast data up so every body knows
		SET_FM_MISSION_LAUNCHED_SUCESS(fmmcMissionData.mdID.idCreator, fmmcMissionData.mdID.idVariation, fmmcMissionData.iInstanceId)
	ELSE
		// Set us to be a net script.
		CDEBUG2LN(DEBUG_SHOOTRANGE, "Set this script as a network script with 2 players...")
		NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(MP_RANGE_MAX_PLAYERS, FALSE)
		
		// Wait until the script is properly running.
		CDEBUG2LN(DEBUG_SHOOTRANGE, "Run net script initialization...")
		HANDLE_NET_SCRIPT_INITIALISATION()
		
		// This script will not be paused if another script calls PAUSE_GAME
		CDEBUG2LN(DEBUG_SHOOTRANGE, "Script cannot be paused...")
		SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
	ENDIF
	
	//Common to all modes
	
	// Make code aware of our server and player broadcast data variables.
	CDEBUG2LN(DEBUG_SHOOTRANGE, "Setup broadcast variables...")
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(sServerBD, SIZE_OF(sServerBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(sPlayerBD, SIZE_OF(sPlayerBD))
	
	// KGM: Wait for the first network broadcast before moving on
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		RETURN FALSE
	ENDIF

	// We own audio.
	REGISTER_SCRIPT_WITH_AUDIO()

	// Set the server and player states!
	CDEBUG2LN(DEBUG_SHOOTRANGE, "Setup server and player states...")
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		sServerBD.eGameState = GAME_STATE_INIT
		PLAYSTATS_CREATE_MATCH_HISTORY_ID_2(sServerBD.iHashedMac, sServerBD.iMatchHistoryID)		
		SET_RANGE_SERVER_TOTAL_MATCHES(sServerBD, GET_NUMBER_OF_SHOOTING_RANGE_MATCHES())
	ENDIF
	sPlayerBD[PARTICIPANT_ID_TO_INT()].eGameState = GAME_STATE_INIT
	
	INITIALIZE_RANGE_PARTICIPANT_INFO( sParticipantInfo )
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Wrapping up all the ways the Range could exit forcefully
/// RETURNS:
///    TRUE if the range should shutdown.
FUNC BOOL RANGE_SHOULD_MP_GAME_END()
	
	IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
		CDEBUG1LN(DEBUG_SHOOTRANGE, "RANGE_SHOULD_MP_GAME_END returning TRUE, SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()")
		RETURN TRUE
	ENDIF
	
	IF IS_SC_COMMUNITY_PLAYLIST_LAUNCHING(ciEVENT_TOURNAMENT_PLAYLIST)
		CDEBUG1LN(DEBUG_SHOOTRANGE, "RANGE_SHOULD_MP_GAME_END returning TRUE, IS_SC_COMMUNITY_PLAYLIST_LAUNCHING(ciEVENT_TOURNAMENT_PLAYLIST)")
		RETURN TRUE
	ENDIF
	
	IF NOT IS_SKYSWOOP_AT_GROUND() 
	AND GAME_STATE_RUN_INITAL = GET_CLIENT_GAME_STATE(sPlayerBD[PARTICIPANT_ID_TO_INT()])
	AND GET_CLIENT_RANGE_STATE(sPlayerBD[PARTICIPANT_ID_TO_INT()]) < RANGE_STATE_ExitConfSplash_MainMenu
		CDEBUG1LN(DEBUG_SHOOTRANGE, "RANGE_SHOULD_MP_GAME_END returning TRUE, NOT IS_SKYSWOOP_AT_GROUND()")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC



// Main script loop.
SCRIPT(MP_MISSION_DATA fmmcMissionData)
	CDEBUG2LN(DEBUG_SHOOTRANGE, "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   Shooting Range MP First Post Sanity   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
	RangeMP_MenuData		sMenuData
	STREAMED_MODEL			sStreamables[iMAX_ASSETS]
	RangeMP_Round			sRangeRound
	RangeMP_RoundFuncs		sRoundFuncs
	RANGE_END_CONDITION		eTerminateReason			// Sometimes, we need to catch why we're being forced out.
	RangeMP_CoreData		sCoreData
	
	FLOAT 					fSoloRangeTimer = 0

	//gdisablerankupmessage = TRUE
	SET_DISABLE_RANK_UP_MESSAGE(TRUE)
	ANIMPOSTFX_STOP_ALL()
	DISABLE_COMPOSITE_SHOTGUN_DECALS(TRUE)
	REQUEST_SYSTEM_ACTIVITY_TYPE_PLAY_SHOOTING_RANGE()
	
	SET_SHOOTING_RANGE_UNSAVED_BITFLAG(SRB_RangeInSession)
	SET_SHOOTING_RANGE_UNSAVED_BITFLAG(SRB_HideBetsMsg)
	IF IS_SHOOTING_RANGE_UNSAVED_BITFLAG_SET(SRB_QuitFromRange)
		CDEBUG2LN(DEBUG_SHOOTRANGE, "SRB_QuitFromRange was set, clearing it")
		CLEAR_SHOOTING_RANGE_UNSAVED_BITFLAG(SRB_QuitFromRange)
	ENDIF
	
	// Keeping this as 2, even though local only cares about himself.
	// When the round is over, he'll need to grab the other guy's data and store it in here for display.
	RangeMP_PlayerData		sPlayerData[MP_RANGE_MAX_PLAYERS]		
	
	// Just a generic counter that we'll need from time to time.
	// Currently used for:		- Timing out streaming on audio banks.
	INT 					iGenericCounter = 0
	
	#IF IS_DEBUG_BUILD
	SETUP_RANGE_MP_WIDGETS(sMenuData)
	TRACE_NATIVE_COMMAND("HIDE_HUD_COMPONENT_THIS_FRAME")
	#ENDIF
	
//	//Please call this to freeze the player:
//	NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, TRUE, TRUE, TRUE, TRUE, FALSE, TRUE, FALSE, FALSE)
	
	// Handle script setup.
	IF NOT PROCESS_INIT(fmmcMissionData)
		CDEBUG2LN(DEBUG_SHOOTRANGE, "Failed to receive initial broadcast data. Cleaning up.")
		SCRIPT_CLEANUP(sRangeRound, sCoreData, sMenuData, sPlayerData[PARTICIPANT_ID_TO_INT()], ciFMMC_END_OF_MISSION_STATUS_CANCELLED, sServerBD.iMatchHistoryID, fmmcMissionData.mdID.idVariation)
	ENDIF
	
	INT iClientID = PARTICIPANT_ID_TO_INT()
	CDEBUG2LN(DEBUG_SHOOTRANGE, "Player Namer Tag: ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iClientID))), ", iClientID=", iClientID)
	
	SET_AUTOSAVE_IGNORES_ON_MISSION_FLAG(TRUE)
	SET_MINIGAME_IN_PROGRESS(TRUE)

	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
		SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
	ENDIF
	
	IF DID_I_JOIN_MISSION_AS_SPECTATOR()
		SET_CLIENT_FLAG(sPlayerBD[iClientID], RANGE_MP_F_IsSpectator)
		sPlayerData[iClientID].camSpectator = CREATE_CAMERA(CAMTYPE_SCRIPTED, FALSE)
	ELSE
		SET_CLIENT_FLAG(sPlayerBD[iClientID], RANGE_MP_F_ClientIsPlaying)
	ENDIF
	
	RANGE_MP_HANDLE_LOCAL_GUSENBERG_AVAILABILITY(sPlayerBD)
	
	RANGE_MP_GAME_STATE eGameStates[ k_MaxShooters ]
	RANGE_GET_SHOOTER_GAME_STATES( sPlayerBD, sParticipantInfo, eGameStates )
	
	// Make sure we're allowed to run this at all.
	IF ( RANGE_ARE_SHOOTERS_GAME_STATE_READY( GAME_STATE_FAILED, eGameStates, TRUE )
		OR RANGE_ARE_SHOOTERS_GAME_STATE_READY( GAME_STATE_LEAVE, eGameStates, TRUE )
		OR RANGE_ARE_SHOOTERS_GAME_STATE_READY( GAME_STATE_END, eGameStates, TRUE )
		OR RANGE_ARE_SHOOTERS_GAME_STATE_READY( GAME_STATE_END_SPLASH, eGameStates, TRUE )
		OR RANGE_ARE_SHOOTERS_GAME_STATE_READY( GAME_STATE_TERMINATE, eGameStates, TRUE ) )
		UPDATE_CANT_JOIN(sMenuData)
		SCRIPT_CLEANUP(sRangeRound, sCoreData, sMenuData, sPlayerData[iClientID], ciFMMC_END_OF_MISSION_STATUS_CANCELLED, sServerBD.iMatchHistoryID, fmmcMissionData.mdID.idVariation)
	ENDIF
	
	MPGlobals.sMinigameMPGlobals.bHideMPAwards = TRUE
	CDEBUG2LN(DEBUG_SHOOTRANGE, "g_bUsingShootingRange = TRUE")
	g_bUsingShootingRange = TRUE
	
	CDEBUG2LN(DEBUG_SHOOTRANGE, "Going into WHILE loop")	
	
	WHILE TRUE
		MP_LOOP_WAIT_ZERO()
		
		SET_IDLE_KICK_DISABLED_THIS_FRAME()
		ALLOW_ALTERNATIVE_SCRIPT_CONTROLS_LAYOUT(PLAYER_CONTROL)
		
		UPDATE_RANGE_PARTICIPANT_INFO( sPlayerBD, sParticipantInfo )
		
		MAINTAIN_CELEBRATION_PRE_LOAD(sMenuData.sCelebData)

		IF IS_TRANSITION_ACTIVE()
			SET_LOADING_ICON_INACTIVE()
		ENDIF
		
		// Make all spectators invisible.
		UPDATE_SPECTATOR_VISIBLITY( sParticipantInfo )
		
		RANGE_MP_HANDLE_SAFETY_GOGGLE_VISIBILITY(sCoreData)
		
		// Always do this!
		playerPed = PLAYER_PED_ID()
		
		// If we have a match end event, bail
		IF RANGE_SHOULD_MP_GAME_END()
			IF NOT IS_RANGE_ONE_PLAYER() AND RECEIVED_EVENT(EVENT_NETWORK_END_MATCH)	// Player quit from pause menu
				SET_SHOOTING_RANGE_UNSAVED_BITFLAG(SRB_QuitFromRange)
				IF sServerBD.iClient0MatchWins > sServerBD.iClient1MatchWins AND iClientID = sParticipantInfo.iShooters[1]
				OR sServerBD.iClient0MatchWins < sServerBD.iClient1MatchWins AND iClientID = sParticipantInfo.iShooters[0]
					BROADCAST_BETTING_MISSION_FINISHED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(RANGE_GET_OTHER_SHOOTER(iClientID, sParticipantInfo))))
					CDEBUG2LN(DEBUG_SHOOTRANGE, "SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE() :: BROADCAST_BETTING sServerBD.iClient0MatchWins=", sServerBD.iClient0MatchWins, ", sServerBD.iClient1MatchWins=", sServerBD.iClient1MatchWins, ", iClientID=", iClientID)
				ENDIF
			ENDIF
			SCRIPT_CLEANUP(sRangeRound, sCoreData, sMenuData, sPlayerData[iClientID], ciFMMC_END_OF_MISSION_STATUS_CANCELLED, sServerBD.iMatchHistoryID, fmmcMissionData.mdID.idVariation)
		ENDIF
		
		#IF IS_DEBUG_BUILD
			IF IS_KEYBOARD_KEY_JUST_RELEASED(KEY_F)
				SET_RANGE_MP_SEEN_TUTORIAL(FALSE)
				SCRIPT_CLEANUP(sRangeRound, sCoreData, sMenuData, sPlayerData[iClientID], ciFMMC_END_OF_MISSION_STATUS_CANCELLED, sServerBD.iMatchHistoryID, fmmcMissionData.mdID.idVariation)
			ENDIF
			IF IS_KEYBOARD_KEY_JUST_RELEASED(KEY_LEFT)
			ENDIF
			IF IS_KEYBOARD_KEY_JUST_RELEASED(KEY_RIGHT)
				DEBUG_HIDE_RETICLE = NOT DEBUG_HIDE_RETICLE
			ENDIF
			IF	DEBUG_HIDE_RETICLE
				RANGE_PLAYER_DISABLE_SHOOTING()
			ENDIF
			IF IS_KEYBOARD_KEY_JUST_RELEASED(KEY_UP)
				IF NOT IS_SERVER_FLAG_SET(sServerBD, RANGE_MP_SERVER_F_NEVER_END)
					SET_SERVER_FLAG(sServerBD, RANGE_MP_SERVER_F_NEVER_END)
					DEBUG_ALL_DLC = TRUE
				ELIF IS_SERVER_FLAG_SET(sServerBD, RANGE_MP_SERVER_F_NEVER_END)
					CLEAR_SERVER_FLAG(sServerBD, RANGE_MP_SERVER_F_NEVER_END)
					DEBUG_ALL_DLC = FALSE
				ENDIF
			ENDIF
			IF IS_SERVER_FLAG_SET(sServerBD, RANGE_MP_SERVER_F_NEVER_END)
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.3, 0.858, "STRING", "hacks enabled")
			ENDIF
		#ENDIF
		
		// Information needed every loop.
		iClientID = PARTICIPANT_ID_TO_INT()
		
		// Handle required reset flags and disable certain functionality
		
		// -----------------------------------
		// Process client game logic
		SWITCH GET_CLIENT_GAME_STATE(sPlayerBD[iClientID])
			CASE GAME_STATE_INIT
				CDEBUG2LN(DEBUG_SHOOTRANGE, "Client in GAME_STATE_INIT")
				// When the server moves states, we need to init and change states too.
				IF (GET_SERVER_GAME_STATE(sServerBD) > GAME_STATE_INIT)
					#IF IS_DEBUG_BUILD
						PRINT_RANGE_PARTICIPANT_INFO( sParticipantInfo )
					#ENDIF
					DEAL_WITH_FM_MATCH_START(FMMC_TYPE_MG_SHOOTING_RANGE, sServerBD.iMatchHistoryID, sServerBD.iHashedMac)
					
					//Stop other people joining this mission
					SET_FM_MISSION_AS_NOT_JOINABLE()
					
					// Do init here:
					CDEBUG2LN(DEBUG_SHOOTRANGE, "Setting up player local data.")
					RANGE_MP_HandlePlayerInit(sPlayerBD, sPlayerData[iClientID], sParticipantInfo, iClientID)
					RANGE_MP_INSERT_STREAMS(sStreamables, sMenuData)
					RANGE_MP_INIT_FUNC_POINTERS(sRoundFuncs)
					
					// Control where we go next.
					RANGE_MP_SETUP_START(sCoreData, sPlayerData[iClientID], sParticipantInfo, iClientID, sRangeRound)
					
					CDEBUG2LN(DEBUG_SHOOTRANGE, "Moving to teleport...")
					SET_CLIENT_GAME_STATE(sPlayerBD[iClientID], GAME_STATE_TELEPORT)
				ENDIF
			BREAK
			
			CASE GAME_STATE_TELEPORT
				// Waiting on the screen to fade out to load the area and warp the player in.
				CDEBUG2LN(DEBUG_SHOOTRANGE, "Fading, teleporting...")
				
				IF IS_TRANSITION_ACTIVE()
					CDEBUG2LN(DEBUG_SHOOTRANGE, "GAME_STATE_TELEPORT IS_TRANSITION_ACTIVE = TRUE")
					IF SET_SKYSWOOP_DOWN()
						TAKE_CONTROL_OF_TRANSITION()
					ENDIF
					BREAK
				ELIF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
					IF RANGE_MP_HANDLE_TELEPORT(sCoreData, sPlayerBD, sPlayerData, iClientID)
						CDEBUG2LN(DEBUG_SHOOTRANGE, "Moving onto streaming.")
						SET_CLIENT_GAME_STATE(sPlayerBD[iClientID], GAME_STATE_STREAM)
					ENDIF
				ENDIF
				IF NOT IS_SCREEN_FADED_OUT()
				AND NOT IS_SCREEN_FADING_OUT()
					DO_SCREEN_FADE_OUT(100)
				ENDIF
			BREAK
			
			CASE GAME_STATE_STREAM
				//CDEBUG2LN(DEBUG_SHOOTRANGE, "waiting on streaming...")
				IF IS_RANGE_DONE_STREAMING(sStreamables, iGenericCounter, sMenuData)
//					DO_SCREEN_FADE_IN(500)
					iGenericCounter = 0
					
					CDEBUG2LN(DEBUG_SHOOTRANGE, "Done streaming! Waiting until we're all done loading.")
					SET_LOADING_ICON_INACTIVE()
					SET_CLIENT_GAME_STATE(sPlayerBD[iClientID], GAME_STATE_WAIT_ALL_LOAD)
				ENDIF
			BREAK
			
			CASE GAME_STATE_WAIT_ALL_LOAD
				// This is a holding state. The server will push out out of here.
				IF (GET_SERVER_GAME_STATE(sServerBD) > GAME_STATE_STREAM)
					CDEBUG2LN(DEBUG_SHOOTRANGE, "Server has moved on! So should we!")
					STORE_PLAYER_INTERACTION_ANIM_VALUES_FOR_CELEBRATION_SCREEN()
					SET_LOADING_ICON_INACTIVE()
					SET_CLIENT_GAME_STATE(sPlayerBD[iClientID], GAME_STATE_RUN_INITAL)
					CLEAR_AREA_OF_PEDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), 50.0)
					IF IS_CLIENT_FLAG_SET(sPlayerBD[iClientID], RANGE_MP_F_IsSpectator)
						SET_ENTITY_COORDS(playerPed, sCoreData.vSpectatorPos)
						IF IS_PLAYER_SCTV( PLAYER_ID() )
							SET_CLIENT_GAME_STATE(sPlayerBD[iClientID], GAME_STATE_RUN_SCTV)
						ELSE
							SET_CLIENT_GAME_STATE(sPlayerBD[iClientID], GAME_STATE_RUN_SPECTATOR)
						ENDIF
						GET_RANGE_SPECTATOR_UP_TO_SPEED(sMenuData, sServerBD, sPlayerData[iClientID], sPlayerBD, iClientID, sRangeRound, sRoundFuncs, sCoreData, sParticipantInfo)
					ENDIF
				ENDIF
			BREAK
			
			CASE GAME_STATE_RUN_SPECTATOR
				//CDEBUG3LN(DEBUG_SHOOTRANGE, "GAME_STATE_RUN_SPECTATOR")
				
				IF NOT RANGE_MP_UPDATE_SPECTATOR(sServerBD, sPlayerBD, iClientID, sMenuData, sPlayerData, sRangeRound, sRoundFuncs, eTerminateReason, sCoreData, sParticipantInfo)
					SET_CLIENT_GAME_STATE(sPlayerBD[iClientID], GAME_STATE_END)
				ELIF IS_SCREEN_FADED_OUT() AND GET_CLIENT_RANGE_STATE(sPlayerBD[iClientID]) > RANGE_STATE_Init_Wait
					DO_SCREEN_FADE_IN(500)
					CDEBUG2LN(DEBUG_SHOOTRANGE, "Fading in from streaming")
				ENDIF
			BREAK
			
			CASE GAME_STATE_RUN_SCTV
				//CDEBUG3LN(DEBUG_SHOOTRANGE, "GAME_STATE_RUN_SCTV")
				
				IF NOT RANGE_MP_UPDATE_SPECTATOR(sServerBD, sPlayerBD, iClientID, sMenuData, sPlayerData, sRangeRound, sRoundFuncs, eTerminateReason, sCoreData, sParticipantInfo)
					SCRIPT_CLEANUP(sRangeRound, sCoreData, sMenuData, sPlayerData[iClientID], ciFMMC_END_OF_MISSION_STATUS_CANCELLED, sServerBD.iMatchHistoryID, fmmcMissionData.mdID.idVariation)
					//SET_CLIENT_GAME_STATE(sPlayerBD[iClientID], GAME_STATE_END)
				ELIF IS_SCREEN_FADED_OUT() AND GET_CLIENT_RANGE_STATE(sPlayerBD[iClientID]) > RANGE_STATE_Init_Wait
					DO_SCREEN_FADE_IN(500)
					CDEBUG2LN(DEBUG_SHOOTRANGE, "Fading in from streaming")
				ENDIF
			BREAK
			
			CASE GAME_STATE_RUN_INITAL
				//CDEBUG3LN(DEBUG_SHOOTRANGE, "GAME_STATE_RUN_INITAL")
				
				// Disable the MP menu from showing during the game.
				IF GET_CLIENT_RANGE_STATE(sPlayerBD[iClientID]) >= RANGE_STATE_DisplayRound_Countdown
				AND GET_CLIENT_RANGE_STATE(sPlayerBD[iClientID]) <= RANGE_STATE_DisplayRoundOver
					//DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CHARACTER_WHEEL)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
				ENDIF
				
				// Nested switch here handles the actual game.
				IF NOT RANGE_MP_UPDATE(sServerBD, sPlayerBD, iClientID, sMenuData, sPlayerData, sRangeRound, sRoundFuncs, eTerminateReason, sCoreData, sParticipantInfo)
				OR PROCESS_END_CONDITIONS(sServerBD, eTerminateReason, sPlayerBD, iClientID, sMenuData, sRangeRound, sCoreData, sParticipantInfo)
					CDEBUG2LN(DEBUG_SHOOTRANGE, "CLIENT RANGE_MP_UPDATE returned FALSE or PROCESS_END_CONDITIONS returned TRUE")
					SET_LOADING_ICON_INACTIVE()
					SET_CLIENT_GAME_STATE(sPlayerBD[iClientID], GAME_STATE_END)
				ELIF IS_SCREEN_FADED_OUT() AND GET_CLIENT_RANGE_STATE(sPlayerBD[iClientID]) > RANGE_STATE_Init_Wait
					// Should only fade in once,	B*724376
					DO_SCREEN_FADE_IN(500)
					CDEBUG2LN(DEBUG_SHOOTRANGE, "Fading in from streaming")
				ENDIF
			BREAK
			
			CASE GAME_STATE_FAILED
				CDEBUG2LN(DEBUG_SHOOTRANGE, "CLIENT GAME_STATE_FAILED")
				SET_LOADING_ICON_INACTIVE()
				SET_CLIENT_GAME_STATE(sPlayerBD[iClientID], GAME_STATE_END)
			BREAK
			
			CASE GAME_STATE_LEAVE
				HIDE_UI_DURING_RANGE()
				CDEBUG2LN(DEBUG_SHOOTRANGE, "CLIENT GAME_STATE_LEAVE")
				SET_LOADING_ICON_INACTIVE()
				SET_CLIENT_GAME_STATE(sPlayerBD[iClientID], GAME_STATE_END)
				TRIGGER_CELEBRATION_PRE_LOAD(sMenuData.sCelebData)
			FALLTHRU
			
			CASE GAME_STATE_END
				HIDE_UI_DURING_RANGE()
				RANGE_PRINT_TO_SCREEN("CLIENT GAME_STATE_END", 0.24)
				
				ALLOW_RANGE_PLAYER_CONTROL(FALSE)
				
				IF (TIMERA() <= 4000)
					// The name has already been set in Game State Init. Display the winner.
					UPDATE_RANGE_SPLASH(sMenuData)
					
				ELIF TIMERA() > 4000
					IF sPlayerBD[iClientID].bRematchEligible AND NOT IS_RANGE_ONE_PLAYER()
						JOB_WIN_STATUS eJobStatus
						PLAYER_INDEX winningPlayer
						IF sServerBD.iClient0MatchWins > sServerBD.iClient1MatchWins AND iClientID = sParticipantInfo.iShooters[0]
						OR sServerBD.iClient0MatchWins < sServerBD.iClient1MatchWins AND iClientID = sParticipantInfo.iShooters[1]
							eJobStatus = JOB_STATUS_WIN
							winningPlayer = PLAYER_ID()
						ELSE
							eJobStatus = JOB_STATUS_LOSE
							winningPlayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(RANGE_GET_OTHER_SHOOTER(iClientID, sParticipantInfo)))
						ENDIF
						IF NOT HAS_RANGE_LEADERBOARD_BEEN_REQUESTED()
							IF HAS_GENERIC_CELEBRATION_FINISHED(sMenuData.sCelebData, sPlayerData[iClientID].camSpectator, "XPT_RANGE", XPTYPE_COMPLETE, XPCATEGORY_COMPLETE_SHOOTING_RANGE, 
							sPlayerBD[iClientID].iXPAward, eJobStatus, g_iMyBetWinnings, sMenuData.eGenericMGStage, winningPlayer, default, default, default, default, FALSE)
								CDEBUG2LN(DEBUG_SHOOTRANGE, "CASE GAME_STATE_END :: RANGE_MP_HAS_CELEBRATION_FINISHED, REQUEST_RANGE_LEADERBOARD_CAM()")
								SET_SKYSWOOP_UP()
								REQUEST_RANGE_LEADERBOARD_CAM()
								CLEAR_CLIENT_FLAG(sPlayerBD[iClientID], RANGE_MP_F_CelebrationStarted)
							ELIF NOT IS_CLIENT_FLAG_SET(sPlayerBD[iClientID], RANGE_MP_F_CelebrationStarted)
								SET_CLIENT_FLAG(sPlayerBD[iClientID], RANGE_MP_F_CelebrationStarted)
							ENDIF
						ENDIF
					ELSE
						IF NOT HAS_RANGE_LEADERBOARD_BEEN_REQUESTED()
							CDEBUG2LN(DEBUG_SHOOTRANGE, "CASE GAME_STATE_END :: !bRematchEligible OR IS_RANGE_ONE_PLAYER(), REQUEST_RANGE_LEADERBOARD_CAM()")
							SET_SKYSWOOP_UP()
							REQUEST_RANGE_LEADERBOARD_CAM()
						ENDIF
					ENDIF
					
					IF NOT IS_CLIENT_FLAG_SET(sPlayerBD[iClientID], RANGE_MP_F_CelebrationStarted)
						IF IS_RANGE_LEADERBOARD_CAM_READY() AND (IS_CLIENT_FLAG_SET(sPlayerBD[iClientID], RANGE_MP_F_IsSpectator) OR SET_SKYSWOOP_UP())	//IS_SWITCH_TO_MULTI_FIRSTPART_FINISHED())
							// We're not even eligible for betting. Just quit.
							SET_LOADING_ICON_INACTIVE()
							
							SET_CLIENT_GAME_STATE(sPlayerBD[iClientID], GAME_STATE_TERMINATE)
							CDEBUG2LN(DEBUG_SHOOTRANGE, "CASE GAME_STATE_END :: LB Cam Ready and IS_RANGE_ONE_PLAYER()=", PICK_STRING(IS_RANGE_ONE_PLAYER(), "TRUE", "FALSE"), ", and sCoreData.iWinnings=", sCoreData.iWinnings)
						ENDIF
					ENDIF
					CDEBUG3LN(DEBUG_SHOOTRANGE, "IS_RANGE_LEADERBOARD_CAM_READY() = ", PICK_STRING(IS_RANGE_LEADERBOARD_CAM_READY(), "TRUE", "FALSE"))
					CDEBUG3LN(DEBUG_SHOOTRANGE, "IS_CLIENT_FLAG_SET(sPlayerBD[iClientID], RANGE_MP_F_IsSpectator) = ", PICK_STRING(IS_CLIENT_FLAG_SET(sPlayerBD[iClientID], RANGE_MP_F_IsSpectator), "TRUE", "FALSE"))
					CDEBUG3LN(DEBUG_SHOOTRANGE, "IS_SWITCH_TO_MULTI_FIRSTPART_FINISHED = ", PICK_STRING(IS_SWITCH_TO_MULTI_FIRSTPART_FINISHED(), "TRUE", "FALSE"))
					CDEBUG3LN(DEBUG_SHOOTRANGE, "TIMERA = ", TIMERA())
					CDEBUG3LN(DEBUG_SHOOTRANGE, "iClientID = ", iClientID)
					CDEBUG3LN(DEBUG_SHOOTRANGE, "sPlayerBD[iClientID].bRematchEligible = ", PICK_STRING(sPlayerBD[iClientID].bRematchEligible, "TRUE", "FALSE"))
				ENDIF
			BREAK
			
			CASE GAME_STATE_TERMINATE
				IF IS_PLAYER_FLAG_SET(sPlayerData[iClientID], RANGE_MP_F_QUIT_CONFIRMED)
					UPDATE_EXIT_SPLASH_MAINMENU(sServerBD, iClientID, sParticipantInfo)
				ENDIF
				REQUEST_RANGE_LEADERBOARD_CAM()
				IF IS_RANGE_LEADERBOARD_CAM_READY()
					CALL sRoundFuncs.fpRoundEnd(sRangeRound)
					ROUND_WIPE_ALL_OBJECTS(sRangeRound)
					SET_AUTOSAVE_IGNORES_ON_MISSION_FLAG(TRUE)
					MAKE_AUTOSAVE_REQUEST()
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
					INT iFMMCEndValue
					IF (sServerBD.iClient0MatchWins > sServerBD.iClient1MatchWins AND iClientID = sParticipantInfo.iShooters[0])
					OR (sServerBD.iClient1MatchWins > sServerBD.iClient0MatchWins AND iClientID = sParticipantInfo.iShooters[1])
						iFMMCEndValue = ciFMMC_END_OF_MISSION_STATUS_PASSED
					ELSE
						iFMMCEndValue = ciFMMC_END_OF_MISSION_STATUS_FAILED
					ENDIF
					CLEAR_HELP()
					SET_LOADING_ICON_INACTIVE()
					TEXT_LABEL_23 texContentID
					texContentID = ""
					IF IS_CLIENT_FLAG_SET(sPlayerBD[iClientID], RANGE_MP_F_ContinueAfterVotePassing)
						SET_VARS_ON_FMMC_EOM_VOTE_PASSING(ciFMMC_EOM_VOTE_STATUS_CONTINUE, <<0.0, 0.0, 0.0>>, texContentID)
					ELSE
						SET_VARS_ON_FMMC_EOM_VOTE_PASSING(ciFMMC_EOM_VOTE_STATUS_QUIT, <<0.0, 0.0, 0.0>>, texContentID)
					ENDIF
					SCRIPT_CLEANUP(sRangeRound, sCoreData, sMenuData, sPlayerData[iClientID], iFMMCEndValue, sServerBD.iMatchHistoryID, fmmcMissionData.mdID.idVariation, IS_STRING_NULL(GET_RANGE_MENU_EXIT_STRING(sMenuData)))
				ENDIF
			BREAK
			
			CASE GAME_STATE_END_SPLASH
				// The other player left.. Splash screen letting us know.
				CLEAR_HELP()
				CDEBUG2LN(DEBUG_SHOOTRANGE, "Printing game leave splash!")
				PRINT_HELP(GET_RANGE_MENU_EXIT_STRING(sMenuData), DEFAULT_GOD_TEXT_TIME * 3)
				IF TIMERA() > 2000
					SET_LOADING_ICON_INACTIVE()
					SET_CLIENT_GAME_STATE(sPlayerBD[iClientID], GAME_STATE_LEAVE)
					RANGE_CLEAR_ENDED_MISSION_AT_LEADERBOARD_VOTE_OR_TIME_OUT()
				ENDIF
			BREAK
		ENDSWITCH
		
	
		// -----------------------------------
		// Process server game logic		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
//			CDEBUG2LN(DEBUG_SHOOTRANGE, "I AM THE HOST NOW!")
			
			NETWORK_PREVENT_SCRIPT_HOST_MIGRATION()
			
			SWITCH GET_SERVER_GAME_STATE(sServerBD)
				CASE GAME_STATE_INIT
					INT iJoiningPlayers
					iJoiningPlayers = RANGE_GET_NUM_JOINING_PLAYERS()
					IF iJoiningPlayers > 0
						// When we get the players to launch, do so.
						fSoloRangeTimer += GET_FRAME_TIME()
						BOOL bSoloRange
						bSoloRange = (RANGE_GET_NUM_SHOOTERS( sParticipantInfo ) <= 1 AND fSoloRangeTimer > SOLO_RANGE_WAIT_TIME)
						IF (RANGE_GET_NUM_SHOOTERS( sParticipantInfo ) >= iJoiningPlayers OR bSoloRange)
							CDEBUG2LN(DEBUG_SHOOTRANGE, "Server found enough participants! Moving on! bSoloRange=", PICK_STRING(bSoloRange, "TRUE", "FALSE"))
							
							DEBUG_ONE_PLAYER = bSoloRange OR iJoiningPlayers = 1
							CDEBUG2LN(DEBUG_SHOOTRANGE, "SERVER :: GAME_STATE_INIT :: RANGE_GET_NUM_SHOOTERS(", RANGE_GET_NUM_SHOOTERS( sParticipantInfo ), "), fSoloRangeTimer=", fSoloRangeTimer, ", RANGE_GET_NUM_JOINING_PLAYERS(", RANGE_GET_NUM_JOINING_PLAYERS(), "), DEBUG_ONE_PLAYER=", PICK_STRING(DEBUG_ONE_PLAYER, "TRUE", "FALSE"))
							
							// Wipe the stored round desc.
							SERVER_INIT(sServerBD, sParticipantInfo)
							
							SET_SERVER_GAME_STATE(sServerBD, GAME_STATE_STREAM)
							SET_SERVER_FLAG(sServerBD, RANGE_MP_SERVER_F_FirstSCLBUpdate)
						ELSE
							CDEBUG2LN(DEBUG_SHOOTRANGE, "Server is waiting on more participants...")
						ENDIF
					#IF IS_DEBUG_BUILD
					ELSE
						CDEBUG3LN(DEBUG_SHOOTRANGE, "Waiting on iJoiningPlayers, =", iJoiningPlayers)
					#ENDIF
					ENDIF
				BREAK
				
				CASE GAME_STATE_STREAM
//					CDEBUG2LN(DEBUG_SHOOTRANGE, "GAME_STATE_STREAM")
					IF NOT PROCESS_END_CONDITIONS(sServerBD, eTerminateReason, sPlayerBD, iClientID, sMenuData, sRangeRound, sCoreData, sParticipantInfo)
						// If clients are done streaming, move us on.
						CDEBUG2LN(DEBUG_SHOOTRANGE, "End Conditions have not been met")
						RANGE_GET_SHOOTER_GAME_STATES( sPlayerBD, sParticipantInfo, eGameStates )
						
						IF RANGE_ARE_SHOOTERS_GAME_STATE_READY( GAME_STATE_WAIT_ALL_LOAD, eGameStates, IS_RANGE_ONE_PLAYER())
							CDEBUG2LN(DEBUG_SHOOTRANGE, "Player game states are GAME_STATE_WAIT_ALL_LOAD")
							SET_SERVER_GAME_STATE(sServerBD, GAME_STATE_RUNNING)
						ENDIF
					ENDIF
				BREAK
				
				CASE GAME_STATE_RUNNING										
//					CDEBUG2LN(DEBUG_SHOOTRANGE, "GAME_STATE_RUNNING")
					// Detect when both users are back to the main menu.
					// If the clients are in the round over state, just go ahead and clear our flag that we've moved them
					IF NOT PROCESS_END_CONDITIONS(sServerBD, eTerminateReason, sPlayerBD, iClientID, sMenuData, sRangeRound, sCoreData, sParticipantInfo)
						SERVER_UPDATE_RUNNING(sServerBD, sPlayerBD, sRangeRound, iClientID, sPlayerData, sCoreData, sParticipantInfo)
					ENDIF
				BREAK
				
				DEFAULT
					CDEBUG2LN(DEBUG_SHOOTRANGE, "Default Case Hitting :: ", GET_SERVER_GAME_STATE(sServerBD))
				BREAK
			ENDSWITCH
		ENDIF
		
		
		
		#IF IS_DEBUG_BUILD
			//UPDATE_RANDOM_ROUND_WIDGETS(sCoreData)
			//UPDATE_ALL_DEBUG_WIDGETS()
		#ENDIF
	ENDWHILE
ENDSCRIPT



