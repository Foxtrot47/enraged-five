// RangeMP_Server_lib.sch
USING "RangeMP_Server.sch"
USING "RangeMP_Player_lib.sch"
USING "net_mission.sch"

/// PURPOSE:
///    Responsible for making sure that there's 4 targets of each colors at all times.
/// PARAMS:
///    sRoundData - 
PROC SERVER_UPDATE_RANDOM_TARGETS(RangeMP_Round & sRoundData, INT iThisClientID, RangeMP_PlayerBD & sPlayerBD[], RangeMP_CoreData & sCoreInfo)
	NET_SCRIPT_HOST_ONLY_COMMAND_ASSERT("SERVER_UPDATE_RANDOM_TARGETS")
	
	// Look through round data. See if there's a target in indicies 0-7 that doesn't have a color.
	INT iColorlessSlot = -1, index
	FOR index = 0 TO 7
		IF NOT (CHECK_TARGET_FLAG(sRoundData.oRoundObjects[index], TARG_F_IS_RED) OR CHECK_TARGET_FLAG(sRoundData.oRoundObjects[index], TARG_F_IS_BLUE))
			iColorlessSlot = index
			index = 99999
		ENDIF
	ENDFOR
	
	// Did we find one?
	IF (iColorlessSlot <> -1)
		CDEBUG2LN(DEBUG_SHOOTRANGE, "Colorless slot is: ", iColorlessSlot)
		// Okay, one doesn't have a color. Get a count of our red and blue targets.
		INT iRed = 0, iBlue = 0, iTakenSpots[8], iTakenCnt = 0
		FOR index = 0 TO 7
			IF CHECK_TARGET_FLAG(sRoundData.oRoundObjects[index], TARG_F_IS_RED)
				iRed++
			ELIF CHECK_TARGET_FLAG(sRoundData.oRoundObjects[index], TARG_F_IS_BLUE)
				iBlue++
			ENDIF
			iTakenSpots[iTakenCnt] = ENUM_TO_INT(sRoundData.oRoundObjects[index].eGridLoc)
			iTakenCnt++
		ENDFOR
		
		// Find an empty spot on the grid.
		INT iNuspot = GET_RANDOM_INT_IN_RANGE_NOT_IN_ARRAY(iTakenSpots, 8, ENUM_TO_INT(gridA1), ENUM_TO_INT(gridD5))
		IF (iNuspot <> -1)
			// Broadcast that create to the other client.
			// IMPORTANT: SPAWN ONE FOR THIS PLAYER NOW!!!
			IF (iRed < 4)
				BROADCAST_TARGET_CREATE_TO_OTHER_PLAYER(INT_TO_ENUM(GRID_INDEX, iNuspot), FALSE, iColorlessSlot, iThisClientID, sPlayerBD)
				CREATE_RANDOM_TARGET(sCoreInfo, sRoundData, sRoundData.oRoundObjects[iColorlessSlot], INT_TO_ENUM(GRID_INDEX, iNuspot), FALSE)
				SET_TARGET_FLAG(sRoundData.oRoundObjects[iColorlessSlot], TARG_F_ROTATING_IN)
				SET_BIT(sPlayerBD[iThisClientID].iTargetsField1, iNuspot)	// Set the bit for a new red target coming in
				
			ELIF (iBlue < 4)
				BROADCAST_TARGET_CREATE_TO_OTHER_PLAYER(INT_TO_ENUM(GRID_INDEX, iNuspot), TRUE, iColorlessSlot, iThisClientID, sPlayerBD)
				CREATE_RANDOM_TARGET(sCoreInfo, sRoundData, sRoundData.oRoundObjects[iColorlessSlot], INT_TO_ENUM(GRID_INDEX, iNuspot))
				SET_TARGET_FLAG(sRoundData.oRoundObjects[iColorlessSlot], TARG_F_ROTATING_IN)
				SET_BIT(sPlayerBD[iThisClientID].iTargetsField2, iNuspot)	// Set the bit for a new blue target coming in
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Setup all server variables.
PROC SERVER_INIT(RangeMP_ServerBD & sServerBD, RangeMP_ParticipantInfo & sParticipantInfo )
	NET_SCRIPT_HOST_ONLY_COMMAND_ASSERT("SERVER_INIT")
	
	INT iRedPlayer = sParticipantInfo.iShooters[RED_PLAYER_CLIENT_ID]
	INT iBluePlayer = sParticipantInfo.iShooters[BLUE_PLAYER_CLIENT_ID]
	
	IF iRedPlayer = k_InvalidParticipantID
		CDEBUG3LN( DEBUG_SHOOTRANGE, "SERVER_INIT - iRedPlayer = k_InvalidParticipantID, this is potentially fatal." )
	ENDIF	
	IF iBluePlayer = k_InvalidParticipantID
		CDEBUG3LN( DEBUG_SHOOTRANGE, "SERVER_INIT - iBluePlayer = k_InvalidParticipantID, this is potentially fatal." )
	ENDIF	
	
	CDEBUG2LN(DEBUG_SHOOTRANGE, "GlobalplayerBD_FM[iMyGBD].sClientCoronaData.iLastCoronaLeader=", GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iLastCoronaLeader)

	// Decide who will be picking the first round.
	IF IS_RANGE_ONE_PLAYER()
		sServerBD.iClientPicking = iRedPlayer
	ELSE
		IF WAS_I_HOST_OF_LAST_CORONA()
			sServerBD.iClientPicking = iBluePlayer
		ELSE
			sServerBD.iClientPicking = iRedPlayer
		ENDIF
	ENDIF
	
	sServerBD.iClient0Score = 0
	sServerBD.iClient1Score = 0
	sServerBD.eClientWhoWon = CWC_NONE
	
	CDEBUG2LN(DEBUG_SHOOTRANGE, "SERVER_INIT called")
ENDPROC

/// PURPOSE:
///    Calculates each player's score. Passes back via reference.
PROC SERVER_UPDATE_GRID_SCORE(RangeMP_Round & sRoundData, RangeMP_ServerBD & sServerData)
	NET_SCRIPT_HOST_ONLY_COMMAND_ASSERT("SERVER_UPDATE_GRID_SCORE")
	
	// We store the scores in temp data, because if we change server data and it's not required, it's going to send that to everyone
	// repeatedly. We cache the scores, then check against existing to see if we need to update.
	INT index, iRedScore = 0, iBlueScore = 0
	FOR index = 0 TO MAX_RANGE_MP_TARGETS - 1
		IF CHECK_TARGET_FLAG(sRoundData.oRoundObjects[index], TARG_F_IS_RED) OR 
				CHECK_TARGET_FLAG(sRoundData.oRoundObjects[index], TARG_F_FLIPPING_TO_RED)
			iRedScore += 1
		ELIF CHECK_TARGET_FLAG(sRoundData.oRoundObjects[index], TARG_F_IS_BLUE) OR 
				CHECK_TARGET_FLAG(sRoundData.oRoundObjects[index], TARG_F_FLIPPING_TO_BLUE)
			iBlueScore += 1
		ENDIF
	ENDFOR
	
	// Update only if the scores have changed.
	IF (iRedScore <> sServerData.iClient0Score)
		sServerData.iClient0Score = iRedScore
	ENDIF
	IF (iBlueScore <> sServerData.iClient1Score)
		sServerData.iClient1Score = iBlueScore
	ENDIF	
ENDPROC

/// PURPOSE:
///    Calculates each player's score during the grid round.
PROC SERVER_UPDATE_RANDOM_SCORE(RangeMP_Round & sRoundData, RangeMP_ServerBD & sServerData)
	NET_SCRIPT_HOST_ONLY_COMMAND_ASSERT("SERVER_UPDATE_RANDOM_SCORE")
	
	// Go through each of the targets. If one is dead, and has a color, give points to that player.
	INT index, iRedScore = sServerData.iClient0Score, iBlueScore = sServerData.iClient1Score
	FOR index = 0 TO 7
		IF CHECK_TARGET_FLAG(sRoundData.oRoundObjects[index], TARG_F_COUNT_ME)
			IF CHECK_TARGET_FLAG(sRoundData.oRoundObjects[index], TARG_F_IS_RED)
				//CLEAR_TARGET_FLAG(sRoundData.oRoundObjects[index], TARG_F_IS_RED)
				
				// Add points to Red. (Client 1)
				IF CHECK_TARGET_FLAG(sRoundData.oRoundObjects[index], TARG_F_HIT_BY_PROPER_COLOR)
					iRedScore += CI_RANDOM_PTS_PER_TGT
				ELSE
					iBlueScore += CI_RANDOM_PTS_LOST
				ENDIF
				
			ELIF CHECK_TARGET_FLAG(sRoundData.oRoundObjects[index], TARG_F_IS_BLUE)
				//CLEAR_TARGET_FLAG(sRoundData.oRoundObjects[index], TARG_F_IS_BLUE)
				
				// Add points to Blue. (Client 0)
				IF CHECK_TARGET_FLAG(sRoundData.oRoundObjects[index], TARG_F_HIT_BY_PROPER_COLOR)
					iBlueScore += CI_RANDOM_PTS_PER_TGT
				ELSE
					iRedScore += CI_RANDOM_PTS_LOST
				ENDIF
			ENDIF
			CLEAR_TARGET_FLAG(sRoundData.oRoundObjects[index], TARG_F_COUNT_ME)
		ENDIF
	ENDFOR
	
	// Update only if the scores have changed.
	// We could just add points in the above update, but I'm concerned that that may send our the Server BD multiple
	// times. Safer to just store it, and update.
	IF (iRedScore <> sServerData.iClient0Score)
		sServerData.iClient0Score = iRedScore
	ENDIF
	IF (iBlueScore <> sServerData.iClient1Score)
		sServerData.iClient1Score = iBlueScore
	ENDIF	
ENDPROC

/// PURPOSE:
///    Handles the score update for the covered round. This is then passed onto the clients in the broadcast data.
PROC SERVER_UPDATE_COVERED_SCORE(RangeMP_Round & sRoundData, RangeMP_ServerBD & sServerData)
	NET_SCRIPT_HOST_ONLY_COMMAND_ASSERT("SERVER_UPDATE_COVERED_SCORE")
	
	INT index, iRedScore = 0, iBlueScore = 0
	FOR index = CI_GOAL_1_1 TO CI_GOAL_4_8
		IF CHECK_TARGET_FLAG(sRoundData.oRoundObjects[index], TARG_F_DONE)
			IF CHECK_TARGET_FLAG(sRoundData.oRoundObjects[index], TARG_F_IS_BLUE)
				iBlueScore += 1
			ELSE
				iRedScore  += 1 
			ENDIF
		ENDIF
	ENDFOR
	
	// Update only if the scores have changed.
	IF (iRedScore <> sServerData.iClient0Score)
		sServerData.iClient0Score = iRedScore
	ENDIF
	IF (iBlueScore <> sServerData.iClient1Score)
		sServerData.iClient1Score = iBlueScore
	ENDIF
ENDPROC

/// PURPOSE:
///    Updates the server during the round.
PROC SERVER_UPDATE_RUNNING(RangeMP_ServerBD & sServerData, RangeMP_PlayerBD & sPlayerBD[], RangeMP_Round & sRoundData, INT iHostClientID,
			RangeMP_PlayerData & sAllPlayerData[], RangeMP_CoreData & sCoreInfo, RangeMP_ParticipantInfo & sParticipantInformation)
	NET_SCRIPT_HOST_ONLY_COMMAND_ASSERT("SERVER_UPDATE_RUNNING")
	
	// Need to know where our clients are.
	RANGE_STATE eClientStates[k_MaxShooters]
	RANGE_GET_SHOOTER_RANGE_STATES( sPlayerBD, sParticipantInformation, eClientStates )
	
	BOOL bOnePlayerDebug = IS_RANGE_ONE_PLAYER()
	
	IF RANGE_ARE_SHOOTERS_RANGE_STATE_READY( RANGE_STATE_Init_Wait, eClientStates, bOnePlayerDebug )
		RANGE_PRINT_TO_SCREEN("SERVER_UPDATE_RUNNING - RANGE_STATE_Init_Wait")
//		IF ((IS_PLAYER_FLAG_SET(sAllPlayerData[0], RANGE_MP_F_PROCESSED_FIRST_INIT) AND IS_PLAYER_FLAG_SET(sAllPlayerData[1], RANGE_MP_F_PROCESSED_FIRST_INIT)) OR some timeout)
//		AND NOT IS_SERVER_FLAG_SET(sServerData, RANGE_MP_SERVER_F_FirstInit)
		IF NOT IS_SERVER_FLAG_SET(sServerData, RANGE_MP_SERVER_F_FirstInit)
			SET_SERVER_FLAG(sServerData, RANGE_MP_SERVER_F_FirstInit)	// Never clear this flag, it's only needed once for the beginning update.
			CDEBUG2LN(DEBUG_SHOOTRANGE, "Setting flag to clear first init state, we should only see this message once per session")
		ENDIF
		
	ELIF RANGE_ARE_SHOOTERS_RANGE_STATE_READY( RANGE_STATE_MenuUpdate, eClientStates, bOnePlayerDebug )
		RANGE_PRINT_TO_SCREEN("SERVER_UPDATE_RUNNING - RANGE_STATE_MenuUpdate")
		// If we're in this state, and we're here because we've been kicked back after a Flag warning, clear that flag
		IF (IS_SERVER_FLAG_SET(sServerData, RANGE_MP_SERVER_F_PreventReflag))
			CLEAR_SERVER_FLAG(sServerData, RANGE_MP_SERVER_F_PreventReflag)
			CLEAR_SERVER_FLAG(sServerData, RANGE_MP_SERVER_F_GoBackToMenu)
		ENDIF
		IF IS_SERVER_FLAG_SET(sServerData, RANGE_MP_SERVER_F_MoveTo_Countdown)
			CLEAR_SERVER_FLAG(sServerData, RANGE_MP_SERVER_F_MoveTo_Countdown)
		ENDIF
	
	ELIF RANGE_ARE_SHOOTERS_RANGE_STATE_READY( RANGE_STATE_WaitForAllClients_PostMenu, eClientStates, bOnePlayerDebug )		
		RANGE_PRINT_TO_SCREEN("SERVER_UPDATE_RUNNING - RANGE_STATE_WaitForAllClients_PostMenu")
		//================================================
		
		IF IS_SERVER_FLAG_SET(sServerData, RANGE_MP_SERVER_F_GoBackToMenu)
			CLEAR_SERVER_FLAG(sServerData, RANGE_MP_SERVER_F_GoBackToMenu)
		ENDIF
		IF IS_SERVER_FLAG_SET(sServerData, RANGE_MP_SERVER_F_1stNoMenuInput)
			CLEAR_SERVER_FLAG(sServerData, RANGE_MP_SERVER_F_1stNoMenuInput)
		ENDIF
		IF IS_SERVER_FLAG_SET(sServerData, RANGE_MP_SERVER_F_PreventReflag)
			CLEAR_SERVER_FLAG(sServerData, RANGE_MP_SERVER_F_PreventReflag)
		ENDIF
		IF IS_SERVER_FLAG_SET(sServerData, RANGE_MP_SERVER_F_Quit_NoMenuInput)
			CLEAR_SERVER_FLAG(sServerData, RANGE_MP_SERVER_F_Quit_NoMenuInput)
		ENDIF
		IF IS_SERVER_FLAG_SET(sServerData, RANGE_MP_SERVER_F_ClientRepeatRound)
			CLEAR_SERVER_FLAG(sServerData, RANGE_MP_SERVER_F_ClientRepeatRound)
		ENDIF
		IF IS_SERVER_FLAG_SET(sServerData, RANGE_MP_SERVER_F_MoveTo_MainMenu)
			CLEAR_SERVER_FLAG(sServerData, RANGE_MP_SERVER_F_MoveTo_MainMenu)
		ENDIF
		IF IS_SERVER_FLAG_SET(sServerData, RANGE_MP_SERVER_F_MoveTo_Countdown)
			CLEAR_SERVER_FLAG(sServerData, RANGE_MP_SERVER_F_MoveTo_Countdown)
		ENDIF
		IF IS_SERVER_FLAG_SET(sServerData, RANGE_MP_SERVER_F_Match_Over)
			CLEAR_SERVER_FLAG(sServerData, RANGE_MP_SERVER_F_Match_Over)
		ENDIF
		
		// Init the score here, once
		IF (sPlayerBD[iHostClientID].sRoundDesc.eChallenge = CI_GRID) 
			sServerData.iClient0Score = 14
			sServerData.iClient1Score = 14
			
		ELIF (sPlayerBD[iHostClientID].sRoundDesc.eChallenge = CI_RANDOM) 
			sServerData.iClient0Score = 0
			sServerData.iClient1Score = 0
			
		ELIF (sPlayerBD[iHostClientID].sRoundDesc.eChallenge = CI_COVERED) 
			sServerData.iClient0Score = 0
			sServerData.iClient1Score = 0
		ENDIF
		
		// Okay, both clients are reado to start the countdown. Do it.
		sServerData.eClientWhoWon = CWC_NONE		//-- Don't blitz this until we've picked a new round so we can use it for UI
		SET_RANGE_MP_ROUNDS_PLAYED(sServerData, 0)	//-- Clearing this here so we can use it for UI
		CDEBUG2LN(DEBUG_SHOOTRANGE, "SERVER_UPDATE_RUNNING sServerData.iRndsPlayedInMatch set to 0")
		SET_SERVER_FLAG(sServerData, RANGE_MP_SERVER_F_MoveTo_Countdown)
		SET_SERVER_FLAG(sServerData, RANGE_MP_SERVER_F_FirstSCLBUpdate)	// Set the leaederboard to update at the beginning of the round
		// Reset the desync timer, if there's lag it could have increased while waiting for messages to travel the world wide web.
		RESET_RANGE_SERVER_DESYNC_TIMER(sServerData)
		CLEAR_SERVER_FLAG(sServerData, RANGE_MP_SERVER_F_MenuFailDesync)
		
		// Log the time we told them to go to the countdown. This is when we'll allow shooting.
		CDEBUG1LN(DEBUG_SHOOTRANGE, "SERVER_UPDATE_RUNNING - Setting countdown over time.")
		SET_RANGE_SERVER_NET_TIME_COUNTDOWN_OVER( sServerData, NATIVE_TO_INT(GET_NETWORK_TIME()) + 3500 )
		
		
		SET_SERVER_FLAG(sServerData, RANGE_MP_SERVER_F_MoveTo_InRound)
		
		//================================================
		
	ELIF RANGE_ARE_SHOOTERS_RANGE_STATE_READY( RANGE_STATE_MenuFail_ServerUpdatePicker, eClientStates, bOnePlayerDebug )
		RANGE_PRINT_TO_SCREEN("SERVER_UPDATE_RUNNING - RANGE_STATE_MenuFail_ServerUpdatePicker")
		
		// Seriously, our clients are trying to do everything they can to mess us up... They got here because no one picked a menu option.
		// If it's our first time here, flag it.
		// If we're here and that flag is already set, it's our second time here. Make them quit.
		IF NOT IS_SERVER_FLAG_SET(sServerData, RANGE_MP_SERVER_F_Quit_NoMenuInput) AND NOT IS_SERVER_FLAG_SET(sServerData, RANGE_MP_SERVER_F_PreventReflag)
			IF NOT IS_SERVER_FLAG_SET(sServerData, RANGE_MP_SERVER_F_1stNoMenuInput) AND NOT IS_RANGE_ONE_PLAYER()
				// First time. Flag it. Pick new controller.
				SET_SERVER_FLAG(sServerData, RANGE_MP_SERVER_F_GoBackToMenu)
				SET_SERVER_FLAG(sServerData, RANGE_MP_SERVER_F_1stNoMenuInput)
				IF NOT bOnePlayerDebug
				SERVER_SET_OTHER_CLIENT_AS_PICKER(sServerData, sParticipantInformation)
				ENDIF
			ELSE
				// Flag's already set. Make them quit.
				SET_SERVER_FLAG(sServerData, RANGE_MP_SERVER_F_Quit_NoMenuInput)
			ENDIF
			
			// We set this so we don't revisit this state. This gets cleared if the clients ever go back to the menu.
			SET_SERVER_FLAG(sServerData, RANGE_MP_SERVER_F_PreventReflag)
		ENDIF
		
		IF IS_SERVER_FLAG_SET(sServerData, RANGE_MP_SERVER_F_MoveTo_Countdown)
			CLEAR_SERVER_FLAG(sServerData, RANGE_MP_SERVER_F_MoveTo_Countdown)
		ENDIF
		
	//ELIF RANGE_ARE_SHOOTERS_RANGE_STATE_READY( RANGE_STATE_DisplayRound_Countdown, eClientStates, bOnePlayerDebug )
	// Do nothing state here.
		
	ELIF RANGE_ARE_SHOOTERS_RANGE_STATE_READY( RANGE_STATE_InRound, eClientStates, bOnePlayerDebug )
		RANGE_PRINT_TO_SCREEN("SERVER_UPDATE_RUNNING - RANGE_STATE_InRound")
		
		// Do we still need to clear flags?
		// Also increase the rounds played
		IF IS_SERVER_FLAG_SET(sServerData, RANGE_MP_SERVER_F_MoveTo_InRound)
			CLEAR_SERVER_FLAG(sServerData, RANGE_MP_SERVER_F_MoveTo_InRound)
			SET_RANGE_MP_ROUNDS_PLAYED(sServerData, GET_RANGE_MP_ROUNDS_PLAYED(sServerData) + 1)
			CDEBUG2LN(DEBUG_SHOOTRANGE, "Incrementing Rounds Played to: ", sServerData.iRndsPlayedInMatch)
		ENDIF
		IF IS_SERVER_FLAG_SET(sServerData, RANGE_MP_SERVER_F_ClientRepeatRound)
			CLEAR_SERVER_FLAG(sServerData, RANGE_MP_SERVER_F_ClientRepeatRound)
		ENDIF

		// Constatnly update the score while in round. Uses the sRoundData, as this is supplied by the host.
		IF NOT IS_SERVER_FLAG_SET(sServerData, RANGE_MP_SERVER_F_MoveTo_RoundOver_Sk)
			IF (sPlayerBD[iHostClientID].sRoundDesc.eChallenge = CI_GRID)
				SERVER_UPDATE_GRID_SCORE(sRoundData, sServerData)
				
				// Check for skunk win.
				IF (sServerData.iClient0Score = 28) AND (sServerData.iClient1Score < 28)
					sRoundData.iSkunkCounter++
				ELIF (sServerData.iClient1Score = 28) AND (sServerData.iClient0Score < 28)
					sRoundData.iSkunkCounter++
				ELSE
					sRoundData.iSkunkCounter = 0
				ENDIF
					
				// Has a player been skunked?
				IF (sRoundData.iSkunkCounter > UPDATES_UNTIL_SKUNK)
					// Set who won...
					IF (sServerData.iClient0Score = 28)
						sServerData.eClientWhoWon = CWC_CLIENT0_ROUND
					ELSE
						sServerData.eClientWhoWon = CWC_CLIENT1_ROUND
					ENDIF
					
					// Move clients to correct next state.
					SET_SERVER_FLAG(sServerData, RANGE_MP_SERVER_F_MoveTo_RoundOver_Sk)
					CDEBUG2LN(DEBUG_SHOOTRANGE, "Skunk flag set during CI_GRID")
				ENDIF
				
			ELIF (sPlayerBD[iHostClientID].sRoundDesc.eChallenge = CI_RANDOM)
				SERVER_UPDATE_RANDOM_TARGETS(sRoundData, iHostClientID, sPlayerBD, sCoreInfo)
				// Update the random round score.
				SERVER_UPDATE_RANDOM_SCORE(sRoundData, sServerData)
				
			ELIF (sPlayerBD[iHostClientID].sRoundDesc.eChallenge = CI_COVERED)	
				// Update the covered round scoring.
				SERVER_UPDATE_COVERED_SCORE(sRoundData, sServerData)
				
				// Can do a skunk win in the covered round.
				IF (sServerData.iClient0Score = 16) AND (sServerData.iClient1Score < 16)
					sRoundData.iSkunkCounter++
					CDEBUG2LN(DEBUG_SHOOTRANGE, "iClient0 is nearing a skunk win...")
				ELIF (sServerData.iClient1Score = 16) AND (sServerData.iClient0Score < 16)
					sRoundData.iSkunkCounter++
					CDEBUG2LN(DEBUG_SHOOTRANGE, "iClient1 is nearing a skunk win...")
				ELIF (sServerData.iClient1Score = 16) AND (sServerData.iClient0Score = 16)
					sRoundData.iSkunkCounter++
					CDEBUG2LN(DEBUG_SHOOTRANGE, "Both Clients are nearing a skunk draw...")
				ELSE
					sRoundData.iSkunkCounter = 0
				ENDIF
				
				// Shorter skunk timer.
				IF (sRoundData.iSkunkCounter > 5)
					// Set who won...
					IF (sServerData.iClient0Score = 16) AND (sServerData.iClient1Score < 16)
						sServerData.eClientWhoWon = CWC_CLIENT0_ROUND
						
					ELIF (sServerData.iClient1Score = 16) AND (sServerData.iClient0Score < 16)
						sServerData.eClientWhoWon = CWC_CLIENT1_ROUND
						
					ELIF (sServerData.iClient0Score = 16) AND (sServerData.iClient1Score = 16)
						sServerData.eClientWhoWon = CWC_NOWINNER_MATCH
					ENDIF
						
					SET_SERVER_FLAG(sServerData, RANGE_MP_SERVER_F_MoveTo_RoundOver_Sk)
					CDEBUG2LN(DEBUG_SHOOTRANGE, "Skunk flag set during CI_COVERED")
				ENDIF
			ENDIF
		ENDIF		

	ELIF RANGE_ARE_SHOOTERS_RANGE_STATE_READY( RANGE_STATE_RoundOver, eClientStates, bOnePlayerDebug )
		RANGE_PRINT_TO_SCREEN("SERVER_UPDATE_RUNNING - RANGE_STATE_RoundOver")
		
		IF NOT IS_SERVER_FLAG_SET(sServerData, RANGE_MP_SERVER_F_MoveTo_ScoreUs)
			CDEBUG1LN(DEBUG_SHOOTRANGE, "SERVER_UPDATE_RUNNING - Clearing countdown over time.")
			SET_RANGE_SERVER_NET_TIME_COUNTDOWN_OVER( sServerData, -1 )
			
			// Both clients are done with the round. Calculate final score.
			IF (sPlayerBD[iHostClientID].sRoundDesc.eChallenge = CI_GRID)
				RANGE_END_CONDITION eDummyReason
				// SERVER NEEDS TO CONTINUE TO HANDLE PACKAGES HERE.
				PLAYER_PROCESS_EVENTS(iHostClientID, sRoundData, sAllPlayerData, sPlayerBD[iHostClientID].sRoundDesc.eChallenge, sPlayerBD[iHostClientID], eDummyReason, sCoreInfo, sParticipantInformation)
				SERVER_UPDATE_GRID_SCORE(sRoundData, sServerData)
			
			ELIF (sPlayerBD[iHostClientID].sRoundDesc.eChallenge = CI_RANDOM)
				// Update the random round score.
				SERVER_UPDATE_RANDOM_SCORE(sRoundData, sServerData)
				
			ELIF (sPlayerBD[iHostClientID].sRoundDesc.eChallenge = CI_COVERED)
				// Update the covered round scoring.
				SERVER_UPDATE_COVERED_SCORE(sRoundData, sServerData)
			ELSE
				CDEBUG2LN(DEBUG_SHOOTRANGE, "sRoundDesc.eChallenge is not Grid, Random, or Covered!")
			ENDIF
			
			IF IS_SERVER_FLAG_SET(sServerData, RANGE_MP_SERVER_F_MoveTo_RoundOver_Sk)
				CLEAR_SERVER_FLAG(sServerData, RANGE_MP_SERVER_F_MoveTo_RoundOver_Sk)
				CDEBUG2LN(DEBUG_SHOOTRANGE, "Clearing Skunk Flag")
			ENDIF
			sRoundData.iSkunkCounter = 0
			SET_SERVER_FLAG(sServerData, RANGE_MP_SERVER_F_MoveTo_ScoreUs)
		ENDIF
	
	ELIF RANGE_ARE_SHOOTERS_RANGE_STATE_READY( RANGE_STATE_RoundOver_ScoreUs, eClientStates, bOnePlayerDebug )
		RANGE_PRINT_TO_SCREEN("SERVER_UPDATE_RUNNING - RANGE_STATE_RoundOver_ScoreUs")
		
		IF NOT IS_SERVER_FLAG_SET(sServerData, RANGE_MP_SERVER_F_MoveTo_DispRndOver)
			IF IS_SERVER_FLAG_SET(sServerData, RANGE_MP_SERVER_F_MoveTo_ScoreUs)
				CLEAR_SERVER_FLAG(sServerData, RANGE_MP_SERVER_F_MoveTo_ScoreUs)
			ENDIF
			// Calculate winners.
			IF (sServerData.iClient0Score > sServerData.iClient1Score)
				sServerData.iClient0RndWins++
				sServerData.eClientWhoWon = CWC_CLIENT0_ROUND
				CDEBUG2LN(DEBUG_SHOOTRANGE, "Increasing iClient0RndWins->", sServerData.iClient0RndWins)
				
			ELIF (sServerData.iClient0Score < sServerData.iClient1Score)
				sServerData.iClient1RndWins++
				sServerData.eClientWhoWon = CWC_CLIENT1_ROUND
				CDEBUG2LN(DEBUG_SHOOTRANGE, "Increasing iClient1RndWins->", sServerData.iClient1RndWins)
			ELSE
				sServerData.eClientWhoWon = CWC_NONE
				CDEBUG2LN(DEBUG_SHOOTRANGE, "Setting winner to CWC_NONE")
			ENDIF
			
			// This should put us on a "Match won" screen.
			IF (sServerData.iClient0RndWins > (TO_FLOAT(ROUNDS_PER_MATCH)) * 0.5)
				sServerData.eClientWhoWon = CWC_CLIENT0_MATCH
				
			ELIF (sServerData.iClient1RndWins > (TO_FLOAT(ROUNDS_PER_MATCH)) * 0.5)
				sServerData.eClientWhoWon = CWC_CLIENT1_MATCH
			
			ELIF (GET_RANGE_MP_ROUNDS_PLAYED(sServerData) = ROUNDS_PER_MATCH)
				IF (sServerData.iClient0RndWins > sServerData.iClient1RndWins)
					sServerData.eClientWhoWon = CWC_CLIENT0_MATCH		// Client 0 wins.
				ELIF (sServerData.iClient0RndWins < sServerData.iClient1RndWins)
					sServerData.eClientWhoWon = CWC_CLIENT1_MATCH		// Client 1 wins.
				ELSE
					sServerData.eClientWhoWon = CWC_NOWINNER_MATCH		// Match is a draw.
				ENDIF
			ENDIF
			IF sServerData.eClientWhoWon = CWC_CLIENT0_MATCH
			OR sServerData.eClientWhoWon = CWC_CLIENT1_MATCH
			OR GET_RANGE_MP_ROUNDS_PLAYED(sServerData) = ROUNDS_PER_MATCH
				SET_SERVER_FLAG(sServerData, RANGE_MP_SERVER_F_Match_Over)	// Used to determine when to holster weapons and remove them.
			ENDIF
			
			// Okay, based on what we just did, we need to credit for the match.
			IF (sServerData.eClientWhoWon = CWC_CLIENT0_MATCH)
				IF (sPlayerBD[iHostClientID].sRoundDesc.eChallenge = CI_GRID)
					sServerData.iClient0Chal1Wins += 1
				ELIF (sPlayerBD[iHostClientID].sRoundDesc.eChallenge = CI_RANDOM)
					sServerData.iClient0Chal2Wins += 1
				ELIF (sPlayerBD[iHostClientID].sRoundDesc.eChallenge = CI_COVERED)	
					sServerData.iClient0Chal3Wins += 1
				ENDIF
				sServerData.iClient0MatchWins += 1
				SET_SERVER_FLAG(sServerData, RANGE_MP_SERVER_F_Client0WonMatch)
			ELIF (sServerData.eClientWhoWon = CWC_CLIENT1_MATCH)
				IF (sPlayerBD[iHostClientID].sRoundDesc.eChallenge = CI_GRID)
					sServerData.iClient1Chal1Wins += 1
				ELIF (sPlayerBD[iHostClientID].sRoundDesc.eChallenge = CI_RANDOM)
					sServerData.iClient1Chal2Wins += 1
				ELIF (sPlayerBD[iHostClientID].sRoundDesc.eChallenge = CI_COVERED)	
					sServerData.iClient1Chal3Wins += 1
				ENDIF
				sServerData.iClient1MatchWins += 1
				SET_SERVER_FLAG(sServerData, RANGE_MP_SERVER_F_Client1WonMatch)
			ENDIF
				
			// Move clients.
			SET_SERVER_FLAG(sServerData, RANGE_MP_SERVER_F_MoveTo_DispRndOver)
		ENDIF
	
	ELIF RANGE_ARE_SHOOTERS_RANGE_STATE_READY( RANGE_STATE_DisplayRoundOver, eClientStates, bOnePlayerDebug )
		RANGE_PRINT_TO_SCREEN("SERVER_UPDATE_RUNNING - RANGE_STATE_DisplayRoundOver")
		
		// Clients are viewing the scorecard.
		// Clear the fact that we told them to move to the scorecard.
		IF IS_SERVER_FLAG_SET(sServerData, RANGE_MP_SERVER_F_MoveTo_DispRndOver)
			CLEAR_SERVER_FLAG(sServerData, RANGE_MP_SERVER_F_MoveTo_DispRndOver)
		ENDIF
		IF IS_SERVER_FLAG_SET(sServerData, RANGE_MP_SERVER_F_Client1WonMatch)
			CLEAR_SERVER_FLAG(sServerData, RANGE_MP_SERVER_F_Client1WonMatch)
		ENDIF
		IF IS_SERVER_FLAG_SET(sServerData, RANGE_MP_SERVER_F_Client0WonMatch)
			CLEAR_SERVER_FLAG(sServerData, RANGE_MP_SERVER_F_Client0WonMatch)
		ENDIF
		SET_SERVER_FLAG(sServerData, RANGE_MP_SERVER_F_MoveTo_PostScorecard)
	
	ELIF RANGE_ARE_SHOOTERS_RANGE_STATE_READY( RANGE_STATE_WaitForAllClients_PostScorecard, eClientStates, bOnePlayerDebug )
		RANGE_PRINT_TO_SCREEN("SERVER_UPDATE_RUNNING - RANGE_STATE_WaitForAllClients_PostScorecard")
		
		// Both clients are done with the scorecard, waiting to move to the next round.
		// Only do the following if we haven't already given the clients the signal to move on.
		IF IS_SERVER_FLAG_SET(sServerData, RANGE_MP_SERVER_F_FirstSCLBUpdate)
			CLEAR_SERVER_FLAG(sServerData, RANGE_MP_SERVER_F_FirstSCLBUpdate)
		ENDIF
		
		CLEAR_SERVER_FLAG(sServerData, RANGE_MP_SERVER_F_MoveTo_PostScorecard)
		
		IF NOT (IS_SERVER_FLAG_SET(sServerData, RANGE_MP_SERVER_F_MoveTo_MainMenu) OR IS_SERVER_FLAG_SET(sServerData, RANGE_MP_SERVER_F_ClientRepeatRound))
			IF (sServerData.eClientWhoWon = CWC_CLIENT0_MATCH) OR (sServerData.eClientWhoWon = CWC_CLIENT1_MATCH) OR (sServerData.eClientWhoWon = CWC_NOWINNER_MATCH)
				// We're going back to the menu.
				SET_SERVER_FLAG(sServerData, RANGE_MP_SERVER_F_MoveTo_MainMenu)
				
				sServerData.iClient0TotalRndWins += sServerData.iClient0RndWins
				sServerData.iClient1TotalRndWins += sServerData.iClient1RndWins
				sServerData.iClient0RndWins = 0
				sServerData.iClient1RndWins = 0
				
				sServerData.iClient0Score = 0
				sServerData.iClient1Score = 0
				
				CDEBUG1LN(DEBUG_SHOOTRANGE, "SERVER_UPDATE_RUNNING - Setting countdown over time post-scorecard.")
				SET_RANGE_SERVER_NET_TIME_COUNTDOWN_OVER( sServerData, -1 )
					
				//sServerData.eClientWhoWon = CWC_NONE		-- Don't blitz this until we've picked a new round so we can use it for UI
				//sServerData.iRndsPlayedInMatch = 0		-- Don't clear this until we've checked it for UI
				
				// Switch the client in control to the other client.
				IF NOT bOnePlayerDebug
					SERVER_SET_OTHER_CLIENT_AS_PICKER(sServerData, sParticipantInformation)
				ENDIF
			ELSE
				SET_SERVER_FLAG(sServerData, RANGE_MP_SERVER_F_MoveTo_InRound)
				// We're going to play another round.
				IF NOT IS_SERVER_FLAG_SET(sServerData, RANGE_MP_SERVER_F_ClientRepeatRound)
					sServerData.iClient0Score = 0
					sServerData.iClient1Score = 0
					SET_SERVER_FLAG(sServerData, RANGE_MP_SERVER_F_ClientRepeatRound)
					
					// Log the time we told them to go to the countdown. This is when we'll allow shooting.
					SET_RANGE_SERVER_NET_TIME_COUNTDOWN_OVER( sServerData, NATIVE_TO_INT(GET_NETWORK_TIME()) + 3500 )
				ENDIF
			ENDIF
		ENDIF
		
	ELSE	// Handle Desyncs
		RANGE_PRINT_TO_SCREEN("SERVER_UPDATE_RUNNING - ELSE")
		
		// This desync occurs when one the client selects a game to play when the counter is near zero, host player moves to MenuFail while the client goes to PostMenu
		IF RANGE_ARE_SHOOTERS_RANGE_STATE_READY( RANGE_STATE_MenuFail_ServerUpdatePicker, eClientStates, TRUE )
			AND RANGE_ARE_SHOOTERS_RANGE_STATE_READY( RANGE_STATE_WaitForAllClients_PostMenu, eClientStates, TRUE )
			// Update our desync timer, we allow 2 seconds
			INCREMENT_RANGE_SERVER_DESYNC_TIMER(sServerData)
			IF GET_RANGE_SERVER_DESYNC_TIMER(sServerData) > MENU_FAIL_DESYNC_TIME
				CDEBUG2LN(DEBUG_SHOOTRANGE, "MenuFail desync has occured too long, server telling both players to move on.")
				SET_SERVER_FLAG(sServerData, RANGE_MP_SERVER_F_MenuFailDesync)
				RESET_RANGE_SERVER_DESYNC_TIMER(sServerData)
			ENDIF
		ENDIF
	ENDIF
	
	// Need to update score!
ENDPROC






