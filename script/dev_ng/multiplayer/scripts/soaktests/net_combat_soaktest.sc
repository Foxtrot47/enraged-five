//--------------------------------------------------------------------------------------------
// Performs a soaktest involving multiple network bots joining the session and entering
// combat with one another. Intended to test the gun tasks, death and resurrection code
//--------------------------------------------------------------------------------------------
USING "globals.sch"

// Game Headers
USING "commands_network.sch"
//USING "script_player.sch"

// Network Headers																		   
USING "net_include.sch"
//



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////EXAMPLE COMMAND LINE USAGE
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///    
//BASIC NEEDED
//
//-netAutoJoin=match //match or room
//-netGameMode=6 //GAMEMODE_SP=-1, GAMEMODE_CnC=0, GAMEMODE_FM=1, GAMEMODE_COOPTEST=2, GAMEMODE_DM=3, GAMEMODE_TDM=4, GAMEMODE_RACES=5, GAMEMODE_MPTESTBED=6, GAMEMODE_CREATOR=7
//-netsoaktest=net_combat_soaktest
//
//OPTIONAL
//-invincible
//
//SPECIFY THE PARAMS YOU WANT FOR THE TEST
//
//   
//ALL IDLE
//14 ai, 1-allspawninonelocation,1-remainidle
//-netsoaktestparams=14,1,1
//
//ALL ATTACKING - INVINCIBLE
//14 ai, 1-allspawninonelocation,0-remainidle,0-specialwantedlevel5,1-botinvincible
//-netsoaktestparams=14,1,0,0,1
//
//SPAWN TOGETHER - WITH GRENADE UNDERNEATH
//14 ai, 1-allspawninonelocation,1-remainidle,0-specialwantedlevel5,0-botinvincible,5000-spawngrenadeunderneathplayertimeafterfirstspawninms
//-netsoaktestparams=14,1,1,0,0,5000
//
//ALL ATTACKING - INVINCIBLE - ALL WANTED LEVEL 5
//14 ai, 1-allspawninonelocation,0-remainidle,1-specialwantedlevel5,1-botinvincible,0-spawngrenadeunderneathplayertimeafterfirstspawninms
//-netsoaktestparams=14,1,0,1,1,0
//
//ALL ATTACKING - NORMAL SPAWN POSITIONS
//14 ai, 0-allspawninonelocation,0-remainidle,0-specialwantedlevel5,0-botinvincible,0-spawngrenadeunderneathplayertimeafterfirstspawninms
//-netsoaktestparams=14


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////// VARIABLES /////////////////////////////////////////////////////////////////////////////////////
#IF IS_DEBUG_BUILD

//Main States 

//Const Ints
CONST_INT MAX_BOTS_TO_CREATE 31
CONST_INT NUM_RESPAWN_NODES 10

// Soak test bot states
CONST_INT STATE_PICK_TEAM 0
CONST_INT STATE_IDLE 1
CONST_INT STATE_COMBAT 2

CONST_FLOAT SEEK_TARGET_IDLE_THRESHOLD 20.0
CONST_FLOAT SEEK_TARGET_IN_COMBAT_THRESHOLD 200.0
//Ints
INT iGrenadeTimer
INT maxGrenadeTime

INT NUM_BOTS_TO_CREATE

//Bools
BOOL GRENADE
BOOL ONESPAWN
BOOL REMAINIDLE
BOOL WANTED5
BOOL BOTINVINCIBLE
BOOL PRESSRESET

//Vectors
VECTOR SpawnNodes[NUM_RESPAWN_NODES]
//Strings

//Player_index

//Ped_index

STRUCT NetworkBotData
	INT botIndex
	INT currentState
	INT lastState
	TIME_DATATYPE timeToIdle
	PLAYER_INDEX playerTarget
	BOOL hasDiedRecently
	BOOL blipAdded
ENDSTRUCT

NetworkBotData NETWORK_BOTS[MAX_BOTS_TO_CREATE]

INT BotCountdownForWidgets[MAX_BOTS_TO_CREATE]

#ENDIF
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                  MAIN PROCS                            ////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#IF IS_DEBUG_BUILD
//PURPOSE: Initialise the bot
PROC INITIALISE_SOAK_TEST()
	INT i
	
	PRINTLN("[soaktest]Initialising combat soak test params=",NETWORK_SOAK_TEST_GET_NUM_PARAMETERS())

	NUM_BOTS_TO_CREATE = 1

	IF(NETWORK_SOAK_TEST_GET_NUM_PARAMETERS() >= 1)
		NUM_BOTS_TO_CREATE = NETWORK_SOAK_TEST_GET_INT_PARAMETER(0)
		
		IF NUM_BOTS_TO_CREATE > MAX_BOTS_TO_CREATE
			NUM_BOTS_TO_CREATE = MAX_BOTS_TO_CREATE
		ENDIF
		
		PRINTLN("[soaktest]Going to try to create ",NUM_BOTS_TO_CREATE," network bots")
	ENDIF
	
	REPEAT MAX_BOTS_TO_CREATE i
		IF i < NUM_BOTS_TO_CREATE
			NETWORK_BOTS[i].botIndex = NETWORK_BOT_ADD()
			IF NETWORK_BOTS[i].botIndex != -1
				PRINTLN("[soaktest]Added network bot ",i)
			ENDIF
		ELSE
			NETWORK_BOTS[i].botIndex = -1
		ENDIF
		NETWORK_BOTS[i].currentState = STATE_PICK_TEAM
		NETWORK_BOTS[i].lastState = STATE_PICK_TEAM
		NETWORK_BOTS[i].playerTarget = NULL
		NETWORK_BOTS[i].hasDiedRecently = FALSE
		NETWORK_BOTS[i].blipAdded = FALSE
	ENDREPEAT

	// Set up spawn nodes
	SpawnNodes[0] = <<-57.0, -61.0, 1.0>>
	SpawnNodes[1] = <<-51.0, -25.0, 1.56>>
	SpawnNodes[2] = <<-38.0, -1.0, 1.0>>
	SpawnNodes[3] = <<-35.0, 30.0, 1.0>>
	SpawnNodes[4] = <<-1.0, 65.0, 1.0>>
	SpawnNodes[5] = <<43.0, 50.0, 1.0>>
	SpawnNodes[6] = <<31.0, 28.0, 1.0>>
	SpawnNodes[7] = <<22.0, 7.0, 1.0>>
	SpawnNodes[8] = <<22.0, -25.0, 1.0>>
	SpawnNodes[9] = <<55.0, -57.0, 1.0>>
	
	// Set up configurables based on input parameters
	//-------------------------------------------------------------
	ONESPAWN = FALSE
	IF(NETWORK_SOAK_TEST_GET_NUM_PARAMETERS() >= 2)
		IF (NETWORK_SOAK_TEST_GET_INT_PARAMETER(1) = 1)
			PRINTLN("[soaktest] ONESPAWN")
			ONESPAWN = TRUE
		ENDIF	
	ENDIF

	REMAINIDLE = FALSE
	IF(NETWORK_SOAK_TEST_GET_NUM_PARAMETERS() >= 3)
		IF (NETWORK_SOAK_TEST_GET_INT_PARAMETER(2) = 1)
			PRINTLN("[soaktest] REMAINIDLE")
			REMAINIDLE = TRUE
		ENDIF	
	ENDIF
	
	WANTED5 = FALSE
	IF(NETWORK_SOAK_TEST_GET_NUM_PARAMETERS() >= 4)
		IF (NETWORK_SOAK_TEST_GET_INT_PARAMETER(3) = 1)
			PRINTLN("[soaktest] WANTED5")
			WANTED5 = TRUE
		ENDIF
	ENDIF
	
	IF (WANTED5)
		SET_PLAYER_WANTED_LEVEL(GET_PLAYER_INDEX(), 5)
	ELSE
		SET_PLAYER_WANTED_LEVEL(GET_PLAYER_INDEX(), 0)
	ENDIF
								
	BOTINVINCIBLE = FALSE
	IF(NETWORK_SOAK_TEST_GET_NUM_PARAMETERS() >= 5)
		IF (NETWORK_SOAK_TEST_GET_INT_PARAMETER(4) = 1)
			PRINTLN("[soaktest] BOTINVINCIBLE")
			BOTINVINCIBLE = TRUE
		ENDIF
	ENDIF
	
	GRENADE = FALSE
	IF(NETWORK_SOAK_TEST_GET_NUM_PARAMETERS() >= 6)
		IF (NETWORK_SOAK_TEST_GET_INT_PARAMETER(5) > 0)
			PRINTLN("[soaktest] GRENADE")
			GRENADE = TRUE
			iGrenadeTimer = 0
			maxGrenadeTime = NETWORK_SOAK_TEST_GET_INT_PARAMETER(5)
		ENDIF
	ENDIF
	//-------------------------------------------------------------	
	
	PRINTLN("[soaktest]Initialising combat soak test - complete")

ENDPROC

PROC DECIDE_NEW_BOT_STATE(INT botDataIndex)
	PRINTLN("[soaktest] Deciding new bot state ",botDataIndex)
	IF NETWORK_GET_NUM_PARTICIPANTS() > 1
		PRINTLN("[soaktest] Going to combat state ",botDataIndex)
		NETWORK_BOTS[botDataIndex].currentState = STATE_COMBAT
	ENDIF
ENDPROC

PROC START_COMBAT_STATE(INT botDataIndex)
	
	PRINTLN("[soaktest] Starting combat state ", botDataIndex)
	
	PLAYER_INDEX botPlayerIndex = NETWORK_BOT_GET_PLAYER_ID(NETWORK_BOTS[botDataIndex].botIndex)
	PED_INDEX 	 botPed 		= GET_PLAYER_PED(botPlayerIndex)
	
	IF NOT IS_ENTITY_DEAD(botPed)
			
		IF NOT NETWORK_BOTS[botDataIndex].blipAdded
			ADD_BLIP_FOR_ENTITY(botPed)
			NETWORK_BOTS[botDataIndex].blipAdded = TRUE
		ENDIF
		
		// Try to find a target
//		INT numPlayers     = NETWORK_GET_NUM_PARTICIPANTS()
		PLAYER_INDEX bestPlayer = GET_PLAYER_INDEX()
		
		INT i
		VECTOR botPos = GET_ENTITY_COORDS(botPed)
		FLOAT bestDistance = 100000.0
		
		REPEAT NUM_NETWORK_PLAYERS i
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
				PLAYER_INDEX playerIndex = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
				
				IF playerIndex != botPlayerIndex
					IF NETWORK_PLAYER_HAS_PED(playerIndex)
						PED_INDEX playerPed = GET_PLAYER_PED(playerIndex)
						
						IF NOT IS_ENTITY_DEAD(playerPed)
							VECTOR playerPos = GET_ENTITY_COORDS(playerPed)
							FLOAT distance = GET_DISTANCE_BETWEEN_COORDS(playerPos, botPos, FALSE)
							
							IF distance < bestDistance
								bestPlayer   = playerIndex
								bestDistance = distance
							ENDIF
							
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
				
		IF bestPlayer != INVALID_PLAYER_INDEX()
			PRINTLN("[soaktest] Selected ",GET_PLAYER_NAME(bestPlayer)," as target")
			NETWORK_BOTS[botDataIndex].playerTarget = bestPlayer
			NETWORK_BOTS[botDataIndex].lastState = NETWORK_BOTS[botDataIndex].currentState
		ENDIF
	ENDIF
ENDPROC

PROC UPDATE_COMBAT_STATE(INT botDataIndex)
	TIME_DATATYPE currentTime = GET_NETWORK_TIME()
	
	BOOL returnToIdleState = TRUE
	
	PED_INDEX botPed = GET_PLAYER_PED(NETWORK_BOT_GET_PLAYER_ID(NETWORK_BOTS[botDataIndex].botIndex))
	
	IF NOT IS_ENTITY_DEAD(botPed)	
	
		VECTOR botPos = GET_ENTITY_COORDS(botPed)
		
		PLAYER_INDEX playerIndex = NETWORK_BOTS[botDataIndex].playerTarget
		
		IF NETWORK_PLAYER_HAS_PED(playerIndex)
			PED_INDEX playerPed = GET_PLAYER_PED(playerIndex)
			
			IF NOT IS_ENTITY_DEAD(playerPed)
				VECTOR playerPos = GET_ENTITY_COORDS(playerPed)
				FLOAT distance = GET_DISTANCE_BETWEEN_COORDS(playerPos, botPos, FALSE)
				
				BOOL runningCombatTask = FALSE
				
				IF GET_SCRIPT_TASK_STATUS(botPed, SCRIPT_TASK_COMBAT) = PERFORMING_TASK
					runningCombatTask = TRUE
				ENDIF
				
				BOOL seekTarget = FALSE
				
				IF runningCombatTask
					IF distance > SEEK_TARGET_IN_COMBAT_THRESHOLD
						seekTarget = TRUE
					ENDIF
				ELSE
					IF distance > SEEK_TARGET_IDLE_THRESHOLD
						IF GET_SCRIPT_TASK_STATUS(botPed, SCRIPT_TASK_GO_TO_ENTITY) != PERFORMING_TASK
							seekTarget = TRUE
						ENDIF
					ELSE
						IF NOT HAS_PED_GOT_WEAPON(botPed, WEAPONTYPE_ASSAULTRIFLE)
							NET_PRINT("[soaktest] Giving bot an AK47") NET_NL()
							GIVE_WEAPON_TO_PED(botPed, WEAPONTYPE_ASSAULTRIFLE, INFINITE_AMMO, TRUE)
						ENDIF
						PRINTLN("[soaktest] Giving bot a combat task ",botDataIndex)
						CLEAR_PED_TASKS_IMMEDIATELY(botPed)
						TASK_COMBAT_PED(botPed, playerPed)
					ENDIF
				ENDIF
				
				IF seekTarget
					PRINTLN("[soaktest] Starting to seek target ",botDataIndex)
					CLEAR_PED_TASKS_IMMEDIATELY(botPed)
					TASK_GO_TO_ENTITY(botPed, playerPed, DEFAULT_TIME_NEVER_WARP)
				ENDIF
				
				returnToIdleState = FALSE
			ENDIF
		ENDIF
		
		IF returnToIdleState
			PRINTLN("[soaktest] Returning to idle state ",botDataIndex)
			NETWORK_BOTS[botDataIndex].currentState = STATE_IDLE
			NETWORK_BOTS[botDataIndex].timeToIdle = GET_TIME_OFFSET(currentTime , GET_RANDOM_INT_IN_RANGE(1000, 5000))
		ENDIF
	ENDIF
ENDPROC

PROC RESET()
	
	PRINTLN("RESET")

	INT i
	REPEAT MAX_BOTS_TO_CREATE i
		INT botIndex = NETWORK_BOTS[i].botIndex

		IF i < NUM_BOTS_TO_CREATE
			IF botIndex = -1
				NETWORK_BOTS[i].botIndex = NETWORK_BOT_ADD()
			ENDIF
		ELSE
			IF botIndex != -1
				NETWORK_BOT_REMOVE(NETWORK_BOTS[i].botIndex)
				NETWORK_BOTS[i].botIndex = -1
			ENDIF
		ENDIF
	ENDREPEAT
	
	WAIT(500)

	REPEAT MAX_BOTS_TO_CREATE i
		INT botIndex = NETWORK_BOTS[i].botIndex
		IF botIndex != -1
			IF NATIVE_TO_INT(NETWORK_BOT_GET_PLAYER_ID(botIndex)) != -1
				IF NETWORK_PLAYER_HAS_PED(NETWORK_BOT_GET_PLAYER_ID(botIndex))			
					PED_INDEX botPed = GET_PLAYER_PED(NETWORK_BOT_GET_PLAYER_ID(botIndex))
				
					SET_ENTITY_INVINCIBLE(botPed, FALSE)

					CLEAR_ENTITY_LAST_DAMAGE_ENTITY(botPed)

					SET_ENTITY_HEALTH(botPed, 0)
					
					NETWORK_BOTS[i].currentState = STATE_PICK_TEAM
					NETWORK_BOTS[i].lastState = STATE_PICK_TEAM
					NETWORK_BOTS[i].playerTarget = NULL
					NETWORK_BOTS[i].hasDiedRecently = FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	PRESSRESET = FALSE

ENDPROC

PROC PROCESS_SOAK_TEST()
	INT i
	TIME_DATATYPE currentTime = GET_NETWORK_TIME()
	INT team
	INT respawnIndex
	VECTOR playerpos = GET_ENTITY_COORDS(GET_PLAYER_PED(GET_PLAYER_INDEX()))
	VECTOR offset

	// update the countdown timers array (used for widget debugging
	REPEAT MAX_BOTS_TO_CREATE i
	BotCountdownForWidgets[i] = GET_TIME_DIFFERENCE(NETWORK_BOTS[i].timeToIdle , currentTime)
	
	IF BotCountdownForWidgets[i] < 0
		BotCountdownForWidgets[i] = 0
	ENDIF
	ENDREPEAT
	
	REPEAT MAX_BOTS_TO_CREATE i
		INT botIndex = NETWORK_BOTS[i].botIndex
		IF botIndex != -1
			IF NATIVE_TO_INT(NETWORK_BOT_GET_PLAYER_ID(botIndex)) != -1
				IF NETWORK_PLAYER_HAS_PED(NETWORK_BOT_GET_PLAYER_ID(botIndex))			
					PED_INDEX botPed = GET_PLAYER_PED(NETWORK_BOT_GET_PLAYER_ID(botIndex))
				
					IF NOT IS_ENTITY_DEAD(botPed)
						IF NETWORK_BOTS[i].hasDiedRecently
							PRINTLN("[soaktest] Bot has died recently, respawning ",i)
							respawnIndex = GET_RANDOM_INT_IN_RANGE(0, 9)
							SET_ENTITY_COORDS(botPed, SpawnNodes[respawnIndex])
							NETWORK_BOTS[i].hasDiedRecently = FALSE
						ENDIF
						
						SWITCH NETWORK_BOTS[i].currentState
							CASE STATE_PICK_TEAM
								NETWORK_BOT_JOIN_THIS_SCRIPT(NETWORK_BOTS[i].botIndex)
								NETWORK_BOT_JOIN_SCRIPT(g_MainMissionScriptID, NETWORK_BOTS[i].botIndex)
								team = i
								SET_PLAYER_TEAM(NETWORK_BOT_GET_PLAYER_ID(botIndex), team)
								NETWORK_BOTS[i].currentState = STATE_IDLE
								NETWORK_BOTS[i].timeToIdle = GET_TIME_OFFSET(currentTime , GET_RANDOM_INT_IN_RANGE(1000, 3000))		
								
								//-------------------------------------------------------------
								//Configure freshly spawned bot based on configuration information
								
								IF (ONESPAWN)
									offset.x = GET_RANDOM_FLOAT_IN_RANGE(-2, 2)
									offset.y = GET_RANDOM_FLOAT_IN_RANGE(-2, 2)
									offset.z = 0
									offset += playerpos
									SET_ENTITY_COORDS(botPed, offset)
								ELSE								
									respawnIndex = GET_RANDOM_INT_IN_RANGE(0, 9)
									SET_ENTITY_COORDS(botPed, SpawnNodes[respawnIndex])
								ENDIF
								
								IF (BOTINVINCIBLE)
									SET_ENTITY_INVINCIBLE(botPed, TRUE)
								ELSE
									SET_ENTITY_INVINCIBLE(botPed, FALSE)
								ENDIF
																
								//-------------------------------------------------------------
																
								PRINTLN("[soaktest] Picked team, going to idle state ",i)
							BREAK
							CASE STATE_IDLE
								IF (NOT REMAINIDLE) 
									IF IS_TIME_MORE_THAN(currentTime , NETWORK_BOTS[i].timeToIdle)
										DECIDE_NEW_BOT_STATE(i)
									ENDIF
								ENDIF
							BREAK
							CASE STATE_COMBAT
								IF NETWORK_BOTS[i].currentState != NETWORK_BOTS[i].lastState
									START_COMBAT_STATE(i)
								ELSE
									UPDATE_COMBAT_STATE(i)
								ENDIF
							BREAK
						ENDSWITCH
					ELSE
						NETWORK_BOTS[i].hasDiedRecently = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	//-------------------------------------------------------------
	IF GRENADE
		IF (iGrenadeTimer = 0)
			iGrenadeTimer = GET_GAME_TIMER()
		ENDIF
		
		IF ((GET_GAME_TIMER() - iGrenadeTimer) > maxGrenadeTime)
			ADD_EXPLOSION(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(GET_PLAYER_PED(GET_PLAYER_INDEX()), << 0.0, 0.0, -0.5>>), EXP_TAG_GRENADE, 1.0)
			GRENADE = FALSE
			iGrenadeTimer = 0 //reset to 0 so subsequent grenades if initiated will actuate
		ENDIF
	ENDIF
	//-------------------------------------------------------------
ENDPROC


//PURPOSE: Check whether the brain should terminate
FUNC BOOL SHOULD_THIS_SOAK_TEST_TERMINATE()
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()	
		NET_PRINT_TIME() NET_PRINT_THREAD_ID() NET_PRINT(GET_THIS_SCRIPT_NAME())
		NET_PRINT(" SHOULD_THIS_MULTIPLAYER_SCRIPT_TERMINATE NETWORK_IS_GAME_IN_PROGRESS() = FALSE, bail.") NET_NL()
		RETURN(TRUE)
	ENDIF
	
	IF RECEIVED_EVENT(EVENT_NETWORK_END_MATCH)	
		NET_PRINT_TIME() NET_PRINT_THREAD_ID() NET_PRINT(GET_THIS_SCRIPT_NAME())
		NET_PRINT(" SHOULD_THIS_MULTIPLAYER_SCRIPT_TERMINATE RECEIVED match end event") NET_NL()		
		RETURN(TRUE)
	ENDIF
	
	IF RECEIVED_EVENT(EVENT_NETWORK_SESSION_END)	
		NET_PRINT_TIME() NET_PRINT_THREAD_ID() NET_PRINT(GET_THIS_SCRIPT_NAME())
		NET_PRINT(" SHOULD_THIS_MULTIPLAYER_SCRIPT_TERMINATE RECEIVED session end event") NET_NL()		
		RETURN(TRUE)
	ENDIF	

	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_OF_LAUNCH_SCRIPT_NAME_SCRIPT()) = 0				
		NET_PRINT_TIME() NET_PRINT_THREAD_ID() NET_PRINT(GET_THIS_SCRIPT_NAME())
		NET_PRINT(" SHOULD_THIS_MULTIPLAYER_SCRIPT_TERMINATE Launch script not running, bail.") NET_NL()
		RETURN(TRUE)	
	ENDIF

	RETURN(FALSE)
ENDFUNC

PROC CREATE_WIDGETS()
	INT i
	TEXT_LABEL_63 tl63
	
	START_WIDGET_GROUP("Combat Soak Test")
		ADD_WIDGET_BOOL("PRESSRESET", PRESSRESET)
		ADD_WIDGET_BOOL("ONESPAWN", ONESPAWN)
		ADD_WIDGET_BOOL("REMAINIDLE", REMAINIDLE)
		ADD_WIDGET_BOOL("WANTED5", WANTED5)
		ADD_WIDGET_BOOL("BOTINVINCIBLE", BOTINVINCIBLE)
		ADD_WIDGET_BOOL("GRENADE", GRENADE)	
		ADD_WIDGET_INT_SLIDER("maxGrenadeTime ", maxGrenadeTime, 0, 30000, 1)
		START_WIDGET_GROUP("Bots") 		
			REPEAT MAX_BOTS_TO_CREATE i
				tl63 = "Bot "
				tl63 += i
				START_WIDGET_GROUP(tl63)
					ADD_WIDGET_INT_READ_ONLY("Bot index", NETWORK_BOTS[i].botIndex)
					ADD_WIDGET_INT_READ_ONLY("Current state", NETWORK_BOTS[i].currentState)
					ADD_WIDGET_INT_READ_ONLY("Last state", NETWORK_BOTS[i].lastState)
					ADD_WIDGET_INT_READ_ONLY("Time to idle", BotCountdownForWidgets[i])
				STOP_WIDGET_GROUP()
			ENDREPEAT
		STOP_WIDGET_GROUP()	
	STOP_WIDGET_GROUP()
ENDPROC

#ENDIF
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                      Main Loop                             ////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
SCRIPT
	#IF IS_DEBUG_BUILD

	g_bProcessBots = FALSE
											
	NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE)
	
	NETWORK_SCRIPT_STATE scriptState = NETWORK_GET_SCRIPT_STATUS()

	WHILE (scriptState != NETSCRIPT_PLAYING)
		scriptState = NETWORK_GET_SCRIPT_STATUS()
		WAIT(500)
	ENDWHILE
	
	//Induce a delay to get the systems in a stable state
	WAIT(20000)

	//Only allow the soaktest to run on the host
	IF NOT NETWORK_IS_HOST()
		TERMINATE_THIS_THREAD()
	ENDIF
	
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	WHILE NOT g_bMainMissionScriptActive
		PRINTLN("[soaktest]Waiting for main script to become active")
		WAIT(0)
	ENDWHILE

	// Setup widgets						 
	CREATE_WIDGETS()
	
	//Initialise the bot
	INITIALISE_SOAK_TEST()

	// Main loop.
	WHILE TRUE

		//Only have ONE wait in your scripts
		WAIT(0) 
		
		PROCESS_SOAK_TEST()
		
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S))
			IF ONESPAWN
				ONESPAWN = FALSE
			ELSE
				ONESPAWN = TRUE
			ENDIF
			PRINTLN("ONESPAWN=",ONESPAWN)
		ENDIF
	
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_A))
			IF REMAINIDLE
				REMAINIDLE = FALSE
			ELSE
				REMAINIDLE = TRUE
			ENDIF
			PRINTLN("REMAINIDLE=",REMAINIDLE)
		ENDIF
		
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_W))
			IF WANTED5
				WANTED5 = FALSE
			ELSE
				WANTED5 = TRUE
			ENDIF
			
			IF (WANTED5)
				SET_PLAYER_WANTED_LEVEL(GET_PLAYER_INDEX(), 5)
			ELSE
				SET_PLAYER_WANTED_LEVEL(GET_PLAYER_INDEX(), 0)
			ENDIF
	
			PRINTLN("WANTED5=",WANTED5)
		ENDIF
		
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_I))
			IF BOTINVINCIBLE
				BOTINVINCIBLE = FALSE
			ELSE
				BOTINVINCIBLE = TRUE
			ENDIF
			
			INT i
			REPEAT MAX_BOTS_TO_CREATE i
				INT botIndex = NETWORK_BOTS[i].botIndex
				IF botIndex != -1
					IF NATIVE_TO_INT(NETWORK_BOT_GET_PLAYER_ID(botIndex)) != -1
						IF NETWORK_PLAYER_HAS_PED(NETWORK_BOT_GET_PLAYER_ID(botIndex))			
							PED_INDEX botPed = GET_PLAYER_PED(NETWORK_BOT_GET_PLAYER_ID(botIndex))
				
							IF (BOTINVINCIBLE)
								SET_ENTITY_INVINCIBLE(botPed, TRUE)
							ELSE
								SET_ENTITY_INVINCIBLE(botPed, FALSE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
				
			PRINTLN("BOTINVINCIBLE=",BOTINVINCIBLE)
		ENDIF

		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_G))
			maxGrenadeTime = 0
			GRENADE = TRUE
			PRINTLN("GRENADE=",GRENADE)
		ENDIF
		
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_R))
			PRESSRESET = TRUE
			PRINTLN("PRESSRESET=",PRESSRESET)
		ENDIF
		
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_T))
			NUM_BOTS_TO_CREATE = NUM_BOTS_TO_CREATE - 1
			IF (NUM_BOTS_TO_CREATE <= 1)
				NUM_BOTS_TO_CREATE = 1
			ENDIF
			PRINTLN("NUM_BOTS_TO_CREATE=",NUM_BOTS_TO_CREATE)
		ENDIF
		
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_Y))
			NUM_BOTS_TO_CREATE = NUM_BOTS_TO_CREATE + 1
			IF NUM_BOTS_TO_CREATE > MAX_BOTS_TO_CREATE
				NUM_BOTS_TO_CREATE = MAX_BOTS_TO_CREATE
			ENDIF
			PRINTLN("NUM_BOTS_TO_CREATE=",NUM_BOTS_TO_CREATE)
		ENDIF
				
		IF PRESSRESET
			RESET()
		ENDIF
		
		// If we have a match end event, bail.
		IF SHOULD_THIS_SOAK_TEST_TERMINATE()
			g_bProcessBots = TRUE
			TERMINATE_THIS_THREAD()
		ENDIF

	ENDWHILE
	#ENDIF
ENDSCRIPT

//EOF//////
