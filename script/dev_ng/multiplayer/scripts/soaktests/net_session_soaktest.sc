//--------------------------------------------------------------------------------------------
// Performs a soaktest involving multiple network bots joining and leaving the network session.
// Helps debug issues with leaving and joining sessions as well as testing the network object
// reassignment code
//--------------------------------------------------------------------------------------------
USING "globals.sch"

// Game Headers
USING "commands_network.sch"
//USING "script_player.sch"

// Network Headers																		   
USING "net_include.sch"
//





//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////// VARIABLES /////////////////////////////////////////////////////////////////////////////////////
#IF IS_DEBUG_BUILD

//Main States 

//Const Ints
CONST_INT MAX_BOTS_TO_CREATE 31
CONST_INT NUM_RESPAWN_NODES 10
// Soak test bot states

//Ints

//Bools

//Vectors
VECTOR SpawnNodes[NUM_RESPAWN_NODES]
//Strings

//Player_index

//Ped_index

STRUCT NetworkBotData
	INT botIndex
	TIME_DATATYPE timeToLeaveSession
	TIME_DATATYPE timeToJoinSession
	BOOL isActive
	BOOL hasJoinedScript
ENDSTRUCT

NetworkBotData NETWORK_BOTS[MAX_BOTS_TO_CREATE]

INT BotCountdownForJoining[MAX_BOTS_TO_CREATE]
INT BotCountdownForLeaving[MAX_BOTS_TO_CREATE]

#ENDIF
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                  MAIN PROCS                            ////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#IF IS_DEBUG_BUILD
//PURPOSE: Initialise the bot
PROC INITIALISE_SOAK_TEST()
	INT i
	INT NUM_BOTS_TO_CREATE
	TIME_DATATYPE currentTime = GET_NETWORK_TIME()
	
	NET_PRINT("[soaktest]Initialising session soak test") NET_NL()
	
	NUM_BOTS_TO_CREATE = 1

	IF(NETWORK_SOAK_TEST_GET_NUM_PARAMETERS() >= 1)
		NUM_BOTS_TO_CREATE = NETWORK_SOAK_TEST_GET_INT_PARAMETER(0)
		
		IF NUM_BOTS_TO_CREATE > MAX_BOTS_TO_CREATE
			NUM_BOTS_TO_CREATE = MAX_BOTS_TO_CREATE
		ENDIF
		
		NET_PRINT("[soaktest]Going to try to create ") 
		NET_PRINT_INT(NUM_BOTS_TO_CREATE)
		NET_PRINT(" network bots") 
		NET_NL()
	ENDIF

	REPEAT MAX_BOTS_TO_CREATE i
		NETWORK_BOTS[i].botIndex = -1
		
		IF i < NUM_BOTS_TO_CREATE
			NETWORK_BOTS[i].timeToJoinSession = GET_TIME_OFFSET(currentTime , GET_RANDOM_INT_IN_RANGE(10000, 45000))
			NETWORK_BOTS[i].isActive = TRUE
			NETWORK_BOTS[i].hasJoinedScript = FALSE
		ELSE
			NETWORK_BOTS[i].isActive = FALSE
		ENDIF
	ENDREPEAT
	
	// Set up spawn nodes
	SpawnNodes[0] = <<-57.0, -61.0, 1.0>>
	SpawnNodes[1] = <<-51.0, -25.0, 1.56>>
	SpawnNodes[2] = <<-38.0, -1.0, 1.0>>
	SpawnNodes[3] = <<-35.0, 30.0, 1.0>>
	SpawnNodes[4] = <<-1.0, 65.0, 1.0>>
	SpawnNodes[5] = <<43.0, 50.0, 1.0>>
	SpawnNodes[6] = <<31.0, 28.0, 1.0>>
	SpawnNodes[7] = <<22.0, 7.0, 1.0>>
	SpawnNodes[8] = <<22.0, -25.0, 1.0>>
	SpawnNodes[9] = <<55.0, -57.0, 1.0>>
ENDPROC

PROC UPDATE_BOT_COUNTDOWN_TIMERS(TIME_DATATYPE currentTime)
	INT i
	// update the countdown timers array (used for widget debugging
	REPEAT MAX_BOTS_TO_CREATE i
		IF NETWORK_BOTS[i].isActive
			IF NETWORK_BOTS[i].botIndex != -1
				BotCountdownForLeaving[i] = GET_TIME_DIFFERENCE(NETWORK_BOTS[i].timeToLeaveSession , currentTime)

				IF BotCountdownForLeaving[i] < 0
					BotCountdownForLeaving[i] = 0
				ENDIF
			ELSE
				BotCountdownForJoining[i] = GET_TIME_DIFFERENCE(NETWORK_BOTS[i].timeToJoinSession , currentTime)
				
				IF BotCountdownForJoining[i] < 0
					BotCountdownForJoining[i] = 0
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC PROCESS_SOAK_TEST()
	INT i
	TIME_DATATYPE currentTime = GET_NETWORK_TIME()
	
	UPDATE_BOT_COUNTDOWN_TIMERS(currentTime)

	REPEAT MAX_BOTS_TO_CREATE i
		INT botIndex = NETWORK_BOTS[i].botIndex
		IF botIndex != -1
			IF NATIVE_TO_INT(NETWORK_BOT_GET_PLAYER_ID(botIndex)) != -1
				IF NETWORK_PLAYER_HAS_PED(NETWORK_BOT_GET_PLAYER_ID(botIndex))			
					IF NOT NETWORK_BOTS[i].hasJoinedScript
						PED_INDEX botPed = GET_PLAYER_PED(NETWORK_BOT_GET_PLAYER_ID(botIndex))
						INT respawnIndex = GET_RANDOM_INT_IN_RANGE(0, 9)
						SET_ENTITY_COORDS(botPed, SpawnNodes[respawnIndex])
						NETWORK_BOT_JOIN_THIS_SCRIPT(NETWORK_BOTS[i].botIndex)
						NETWORK_BOT_JOIN_SCRIPT(g_MainMissionScriptID, NETWORK_BOTS[i].botIndex)
						NETWORK_BOTS[i].hasJoinedScript = TRUE
					ELSE
						IF IS_TIME_MORE_THAN(currentTime , NETWORK_BOTS[i].timeToLeaveSession)
							NET_PRINT("[soaktest]Removing bot") NET_NL()
							NETWORK_BOT_REMOVE(botIndex)
							NETWORK_BOTS[i].botIndex = -1
							NETWORK_BOTS[i].hasJoinedScript = FALSE
							NETWORK_BOTS[i].timeToJoinSession = GET_TIME_OFFSET(currentTime , GET_RANDOM_INT_IN_RANGE(10000, 45000))
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF NETWORK_BOTS[i].isActive
				IF IS_TIME_MORE_THAN(currentTime , NETWORK_BOTS[i].timeToJoinSession)
					NETWORK_BOTS[i].botIndex = NETWORK_BOT_ADD()
					IF NETWORK_BOTS[i].botIndex != -1
						NET_PRINT("[soaktest]Added network bot") NET_NL()
						NETWORK_BOTS[i].timeToLeaveSession = GET_TIME_OFFSET(currentTime , GET_RANDOM_INT_IN_RANGE(10000, 45000))
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC


//PURPOSE: Check whether the brain should terminate
FUNC BOOL SHOULD_THIS_SOAK_TEST_TERMINATE()
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()	
		NET_PRINT_TIME() NET_PRINT_THREAD_ID() NET_PRINT(GET_THIS_SCRIPT_NAME())
		NET_PRINT(" SHOULD_THIS_MULTIPLAYER_SCRIPT_TERMINATE NETWORK_IS_GAME_IN_PROGRESS() = FALSE, bail.") NET_NL()
		RETURN(TRUE)
	ENDIF
	
	IF RECEIVED_EVENT(EVENT_NETWORK_END_MATCH)	
		NET_PRINT_TIME() NET_PRINT_THREAD_ID() NET_PRINT(GET_THIS_SCRIPT_NAME())
		NET_PRINT(" SHOULD_THIS_MULTIPLAYER_SCRIPT_TERMINATE RECEIVED match end event") NET_NL()		
		RETURN(TRUE)
	ENDIF
	
	IF RECEIVED_EVENT(EVENT_NETWORK_SESSION_END)	
		NET_PRINT_TIME() NET_PRINT_THREAD_ID() NET_PRINT(GET_THIS_SCRIPT_NAME())
		NET_PRINT(" SHOULD_THIS_MULTIPLAYER_SCRIPT_TERMINATE RECEIVED session end event") NET_NL()		
		RETURN(TRUE)
	ENDIF	

	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_OF_LAUNCH_SCRIPT_NAME_SCRIPT()) = 0				
		NET_PRINT_TIME() NET_PRINT_THREAD_ID() NET_PRINT(GET_THIS_SCRIPT_NAME())
		NET_PRINT(" SHOULD_THIS_MULTIPLAYER_SCRIPT_TERMINATE Launch script not running, bail.") NET_NL()
		RETURN(TRUE)	
	ENDIF

	RETURN(FALSE)
ENDFUNC

PROC CREATE_WIDGETS()
	INT i
	TEXT_LABEL_63 tl63
	
	START_WIDGET_GROUP("Session Soak Test")
		START_WIDGET_GROUP("Bots") 		
			REPEAT MAX_BOTS_TO_CREATE i
				tl63 = "Bot "
				tl63 += i
				START_WIDGET_GROUP(tl63)
					ADD_WIDGET_INT_READ_ONLY("Bot index", NETWORK_BOTS[i].botIndex)
					ADD_WIDGET_INT_READ_ONLY("Time until join", BotCountdownForJoining[i])
					ADD_WIDGET_INT_READ_ONLY("Time until leave", BotCountdownForLeaving[i])
				STOP_WIDGET_GROUP()
			ENDREPEAT
		STOP_WIDGET_GROUP()	
	STOP_WIDGET_GROUP()
ENDPROC

#ENDIF
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                      Main Loop                             ////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
SCRIPT
	#IF IS_DEBUG_BUILD

	g_bProcessBots = FALSE

	NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE)
	
	HANDLE_NET_SCRIPT_INITIALISATION()

	WHILE NOT g_bMainMissionScriptActive
		NET_PRINT("[soaktest]Waiting for main script to become active") NET_NL()
		WAIT(0)
	ENDWHILE
	
	//Initialise the bot
	INITIALISE_SOAK_TEST()

	// Setup widgets						 
	CREATE_WIDGETS()

	// Main loop.
	WHILE TRUE

		//Only have ONE wait in your scripts
		WAIT(0) 
		
		PROCESS_SOAK_TEST()
		
		// If we have a match end event, bail.
		IF SHOULD_THIS_SOAK_TEST_TERMINATE()
			g_bProcessBots = TRUE
			TERMINATE_THIS_THREAD()
		ENDIF

	ENDWHILE
	#ENDIF
ENDSCRIPT

//EOF//////
