//--------------------------------------------------------------------------------------------
// Performs a soaktest involving multiple network players entering and exiting vehicles. The
// players include the local player and a configurable number of network bots. Players are
// separated into groups with the same team, which allows the test to use both the jacking and
// enter as passenger code
//--------------------------------------------------------------------------------------------
USING "globals.sch"

// Game Headers
USING "commands_network.sch"
//USING "script_player.sch"

// Network Headers																		   
USING "net_include.sch"
//





//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////// VARIABLES /////////////////////////////////////////////////////////////////////////////////////
#IF IS_DEBUG_BUILD

//Main States 

//Const Ints
CONST_INT MAX_BOTS_TO_CREATE 31
CONST_INT MAX_CARS_TO_CREATE 16

// Soak test bot states
CONST_INT STATE_PICK_TEAM			0
CONST_INT STATE_IDLE                1
CONST_INT STATE_ENTER_EMPTY_VEHICLE 2
CONST_INT STATE_ENTER_AS_PASSENGER  3
CONST_INT STATE_JACK_DRIVER         4
CONST_INT STATE_JACK_PASSENGER      5
CONST_INT STATE_LEAVE_VEHICLE       6

//Ints

//Bools

//Vectors

//Strings

//Player_index

//Ped_index
VEHICLE_INDEX NETWORK_CARS[MAX_CARS_TO_CREATE]

STRUCT NetworkBotData
	INT botIndex
	INT currentState
	INT lastState
	TIME_DATATYPE timeToIdle
	VEHICLE_INDEX targetVehicle
ENDSTRUCT

NetworkBotData NETWORK_BOTS[MAX_BOTS_TO_CREATE]

// The server broadcast data.
// Everyone can read this data, only the server can update it. 
STRUCT GlobalServerBroadcastDataJST
	INT TARGET_CARS[MAX_CARS_TO_CREATE]	
ENDSTRUCT

GlobalServerBroadcastDataJST GlobalServerBD_JST

INT BotCountdownForWidgets[MAX_BOTS_TO_CREATE]

#ENDIF
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                  MAIN PROCS                            ////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#IF IS_DEBUG_BUILD
//PURPOSE: Initialise the bot
PROC INITIALISE_SOAK_TEST()
	INT i
	INT NUM_BOTS_TO_CREATE
	INT NUM_CARS_TO_CREATE
	VECTOR playerPos
	VECTOR forwardVec
	VECTOR carPos
	
	NET_PRINT("[soaktest]Initialising Jacking soak test") NET_NL()
	
	NUM_BOTS_TO_CREATE = 1
	NUM_CARS_TO_CREATE = 1

	IF(NETWORK_SOAK_TEST_GET_NUM_PARAMETERS() >= 1)
		NUM_BOTS_TO_CREATE = NETWORK_SOAK_TEST_GET_INT_PARAMETER(0)
		
		IF NUM_BOTS_TO_CREATE > MAX_BOTS_TO_CREATE
			NUM_BOTS_TO_CREATE = MAX_BOTS_TO_CREATE
		ENDIF
		
		NET_PRINT("[soaktest]Going to try to create ") 
		NET_PRINT_INT(NUM_BOTS_TO_CREATE)
		NET_PRINT(" network bots") 
		NET_NL()
	ENDIF
	
	IF(NETWORK_SOAK_TEST_GET_NUM_PARAMETERS() >= 2)
		NUM_CARS_TO_CREATE = NETWORK_SOAK_TEST_GET_INT_PARAMETER(1)
		
		IF NUM_CARS_TO_CREATE > MAX_CARS_TO_CREATE
			NUM_CARS_TO_CREATE = MAX_CARS_TO_CREATE
		ENDIF
		
		NET_PRINT("[soaktest]Going to try to create ") 
		NET_PRINT_INT(NUM_CARS_TO_CREATE)
		NET_PRINT(" cars") 
		NET_NL()
	ENDIF
	
	REPEAT MAX_BOTS_TO_CREATE i
		IF i < NUM_BOTS_TO_CREATE
			NETWORK_BOTS[i].botIndex = NETWORK_BOT_ADD()
			IF NETWORK_BOTS[i].botIndex != -1
				NET_PRINT("[soaktest]Added network bot") NET_NL()
			ENDIF
		ELSE
			NETWORK_BOTS[i].botIndex = -1
		ENDIF
		NETWORK_BOTS[i].currentState = STATE_PICK_TEAM
		NETWORK_BOTS[i].lastState = STATE_PICK_TEAM
		NETWORK_BOTS[i].targetVehicle = NULL
	ENDREPEAT
	
	REQUEST_MODEL(POLICE)
	WHILE NOT HAS_MODEL_LOADED(POLICE)
		WAIT(0)
	ENDWHILE
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		RESERVE_NETWORK_MISSION_VEHICLES(NUM_CARS_TO_CREATE)
		
		IF CAN_REGISTER_MISSION_VEHICLES(NUM_CARS_TO_CREATE)
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				playerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
				forwardVec = GET_ENTITY_FORWARD_VECTOR(PLAYER_PED_ID())
				
				REPEAT MAX_CARS_TO_CREATE i
					GlobalServerBD_JST.TARGET_CARS[i] = 0
					
					IF i < NUM_CARS_TO_CREATE
						carPos = playerPos + (forwardVec * (5.0*(i+1)) )
						NETWORK_CARS[i] = CREATE_VEHICLE(POLICE, carPos)
						IF NETWORK_CARS[i] != NULL
							GlobalServerBD_JST.TARGET_CARS[i] = NATIVE_TO_INT(VEH_TO_NET(NETWORK_CARS[i]))
							NET_PRINT("[soaktest]Added car") NET_NL()
						ENDIF
					ELSE
						NETWORK_CARS[i] = NULL
					ENDIF
				ENDREPEAT
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC DECIDE_NEW_BOT_STATE(INT botDataIndex)
	INT i
	
	PED_INDEX botPed = GET_PLAYER_PED(NETWORK_BOT_GET_PLAYER_ID(NETWORK_BOTS[botDataIndex].botIndex))
	
	IF NOT IS_ENTITY_DEAD(botPed)
		IF IS_PED_IN_ANY_VEHICLE(botPed)
			NET_PRINT("[soaktest] New state: LEAVE_VEHICLE") NET_NL()
			NETWORK_BOTS[botDataIndex].currentState = STATE_LEAVE_VEHICLE
		ELSE
			INT botTeam = GET_PLAYER_TEAM(NETWORK_BOT_GET_PLAYER_ID(botDataIndex))
			
			REPEAT MAX_CARS_TO_CREATE i
				VEHICLE_INDEX carIndex = NETWORK_CARS[i]
				
				IF carIndex != NULL
					IF NOT IS_ENTITY_DEAD(carIndex)
						BOOL validDriver    = FALSE
						BOOL validPassenger = FALSE
						INT driverTeam		= -1
						INT passengerTeam	= -1
						
						PED_INDEX driverPed = GET_PED_IN_VEHICLE_SEAT(carIndex, VS_DRIVER)
						
						IF driverPed != NULL
							IF NOT IS_ENTITY_DEAD(driverPed) AND IS_PED_A_PLAYER(driverPed)
								validDriver = TRUE
								driverTeam  = GET_PLAYER_TEAM(NETWORK_GET_PLAYER_INDEX_FROM_PED(driverPed))
							ENDIF
						ENDIF
						
						PED_INDEX passengerPed = GET_PED_IN_VEHICLE_SEAT(carIndex, VS_FRONT_RIGHT)
						
						IF passengerPed != NULL
							IF NOT IS_ENTITY_DEAD(passengerPed) AND IS_PED_A_PLAYER(passengerPed)
								validPassenger = TRUE
								passengerTeam  = GET_PLAYER_TEAM(NETWORK_GET_PLAYER_INDEX_FROM_PED(passengerPed))
							ENDIF
						ENDIF

						IF validDriver AND validPassenger								
							IF driverTeam != botTeam AND passengerTeam != botTeam
								NET_PRINT("[soaktest] New state: JACK_PASSENGER") NET_NL()
								NETWORK_BOTS[botDataIndex].currentState = STATE_JACK_PASSENGER
								NETWORK_BOTS[botDataIndex].targetVehicle = carIndex
							ENDIF
						ELSE
							IF validDriver
								IF driverTeam != botTeam
									NET_PRINT("[soaktest] New state: JACK_DRIVER") NET_NL()
									NETWORK_BOTS[botDataIndex].currentState = STATE_JACK_DRIVER
									NETWORK_BOTS[botDataIndex].targetVehicle = carIndex
								ELSE
									NET_PRINT("[soaktest] New state: ENTER_AS_PASSENGER") NET_NL()
									NETWORK_BOTS[botDataIndex].currentState = STATE_ENTER_AS_PASSENGER
									NETWORK_BOTS[botDataIndex].targetVehicle = carIndex
								ENDIF
							ELSE
								NET_PRINT("[soaktest] New state: ENTER_EMPTY_VEHICLE") NET_NL()
								NETWORK_BOTS[botDataIndex].currentState = STATE_ENTER_EMPTY_VEHICLE
								NETWORK_BOTS[botDataIndex].targetVehicle = carIndex
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
	ENDIF
ENDPROC

PROC START_ENTER_EMPTY_VEHICLE_STATE(INT botDataIndex)

	BOOL targetVehicleValid = FALSE
	
	VEHICLE_INDEX carIndex = NETWORK_BOTS[botDataIndex].targetVehicle
	IF carIndex != NULL
		IF NOT IS_ENTITY_DEAD(carIndex)
			IF IS_VEHICLE_SEAT_FREE(carIndex, VS_DRIVER)
				targetVehicleValid = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT targetVehicleValid
		NET_PRINT("[soaktest] New state: IDLE") NET_NL()
		NETWORK_BOTS[botDataIndex].targetVehicle = NULL
		NETWORK_BOTS[botDataIndex].currentState = STATE_IDLE
		NETWORK_BOTS[botDataIndex].timeToIdle = GET_TIME_OFFSET(GET_NETWORK_TIME() , GET_RANDOM_INT_IN_RANGE(10000, 30000))
	ELSE
		PED_INDEX botPed = GET_PLAYER_PED(NETWORK_BOT_GET_PLAYER_ID(NETWORK_BOTS[botDataIndex].botIndex))
		IF NOT IS_ENTITY_DEAD(botPed)
			TASK_ENTER_VEHICLE(botPed, carIndex, DEFAULT_TIME_NEVER_WARP, VS_DRIVER)
			NETWORK_BOTS[botDataIndex].lastState = NETWORK_BOTS[botDataIndex].currentState
		ENDIF
	ENDIF
ENDPROC

PROC UPDATE_ENTER_EMPTY_VEHICLE_STATE(INT botDataIndex)

	PED_INDEX botPed = GET_PLAYER_PED(NETWORK_BOT_GET_PLAYER_ID(NETWORK_BOTS[botDataIndex].botIndex))
	
	IF NOT IS_ENTITY_DEAD(botPed)
		IF GET_SCRIPT_TASK_STATUS(botPed, SCRIPT_TASK_ENTER_VEHICLE) = PERFORMING_TASK
			IF IS_ENTITY_DEAD(NETWORK_BOTS[botDataIndex].targetVehicle)
				CLEAR_PED_TASKS_IMMEDIATELY(botPed)
			ELSE 
				IF NOT IS_VEHICLE_SEAT_FREE(NETWORK_BOTS[botDataIndex].targetVehicle, VS_DRIVER) AND
					GET_PED_IN_VEHICLE_SEAT(NETWORK_BOTS[botDataIndex].targetVehicle, VS_DRIVER) != botPed
					CLEAR_PED_TASKS_IMMEDIATELY(botPed)
				ENDIF
			ENDIF
		ELSE
			NET_PRINT("[soaktest] New state: IDLE") NET_NL()
			NETWORK_BOTS[botDataIndex].currentState = STATE_IDLE
			NETWORK_BOTS[botDataIndex].timeToIdle = GET_TIME_OFFSET(GET_NETWORK_TIME() , GET_RANDOM_INT_IN_RANGE(10000, 30000))
		ENDIF
	ENDIF
ENDPROC

PROC START_ENTER_AS_PASSENGER_STATE(INT botDataIndex)

	BOOL targetVehicleValid = FALSE
	
	VEHICLE_INDEX carIndex = NETWORK_BOTS[botDataIndex].targetVehicle
	IF carIndex != NULL
		IF NOT IS_ENTITY_DEAD(carIndex)
			IF IS_VEHICLE_SEAT_FREE(carIndex, VS_FRONT_RIGHT)
				targetVehicleValid = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT targetVehicleValid
		NET_PRINT("[soaktest] New state: IDLE") NET_NL()
		NETWORK_BOTS[botDataIndex].targetVehicle = NULL
		NETWORK_BOTS[botDataIndex].currentState = STATE_IDLE
		NETWORK_BOTS[botDataIndex].timeToIdle = GET_TIME_OFFSET(GET_NETWORK_TIME() , GET_RANDOM_INT_IN_RANGE(10000, 30000))
	ELSE
		PED_INDEX botPed = GET_PLAYER_PED(NETWORK_BOT_GET_PLAYER_ID(NETWORK_BOTS[botDataIndex].botIndex))
		IF NOT IS_ENTITY_DEAD(botPed)
			TASK_ENTER_VEHICLE(botPed, carIndex, DEFAULT_TIME_NEVER_WARP, VS_FRONT_RIGHT)
			NETWORK_BOTS[botDataIndex].lastState = NETWORK_BOTS[botDataIndex].currentState
		ENDIF
	ENDIF
ENDPROC

PROC UPDATE_ENTER_AS_PASSENGER_STATE(INT botDataIndex)

	
	PED_INDEX botPed = GET_PLAYER_PED(NETWORK_BOT_GET_PLAYER_ID(NETWORK_BOTS[botDataIndex].botIndex))
	
	IF NOT IS_ENTITY_DEAD(botPed)
		IF GET_SCRIPT_TASK_STATUS(botPed, SCRIPT_TASK_ENTER_VEHICLE) = PERFORMING_TASK
			IF IS_ENTITY_DEAD(NETWORK_BOTS[botDataIndex].targetVehicle)
				CLEAR_PED_TASKS_IMMEDIATELY(botPed)
			ELSE 
				IF NOT IS_VEHICLE_SEAT_FREE(NETWORK_BOTS[botDataIndex].targetVehicle, VS_FRONT_RIGHT) AND
					GET_PED_IN_VEHICLE_SEAT(NETWORK_BOTS[botDataIndex].targetVehicle, VS_FRONT_RIGHT) != botPed
					CLEAR_PED_TASKS_IMMEDIATELY(botPed)
				ENDIF
			ENDIF
		ELSE
			NET_PRINT("[soaktest] New state: IDLE") NET_NL()
			NETWORK_BOTS[botDataIndex].currentState = STATE_IDLE
			NETWORK_BOTS[botDataIndex].timeToIdle = GET_TIME_OFFSET(GET_NETWORK_TIME() , GET_RANDOM_INT_IN_RANGE(10000, 30000))
		ENDIF
	ENDIF
ENDPROC

PROC START_JACK_DRIVER_STATE(INT botDataIndex)

	BOOL targetVehicleValid = FALSE
	
	VEHICLE_INDEX carIndex = NETWORK_BOTS[botDataIndex].targetVehicle
	IF carIndex != NULL
		IF NOT IS_ENTITY_DEAD(carIndex)
			PED_INDEX driverPed = GET_PED_IN_VEHICLE_SEAT(carIndex, VS_DRIVER)
			IF driverPed != NULL
				targetVehicleValid = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT targetVehicleValid
		NET_PRINT("[soaktest] New state: IDLE") NET_NL()
		NETWORK_BOTS[botDataIndex].targetVehicle = NULL
		NETWORK_BOTS[botDataIndex].currentState = STATE_IDLE
		NETWORK_BOTS[botDataIndex].timeToIdle = GET_TIME_OFFSET(GET_NETWORK_TIME() , GET_RANDOM_INT_IN_RANGE(10000, 30000))
	ELSE
		PED_INDEX botPed = GET_PLAYER_PED(NETWORK_BOT_GET_PLAYER_ID(NETWORK_BOTS[botDataIndex].botIndex))
		IF NOT IS_ENTITY_DEAD(botPed)
			TASK_ENTER_VEHICLE(botPed, carIndex, DEFAULT_TIME_NEVER_WARP, VS_DRIVER, PEDMOVE_RUN, ECF_JACK_ANYONE | ECF_RESUME_IF_INTERRUPTED)
			NETWORK_BOTS[botDataIndex].lastState = NETWORK_BOTS[botDataIndex].currentState
		ENDIF
	ENDIF
ENDPROC

PROC UPDATE_JACK_DRIVER_STATE(INT botDataIndex)
	
	PED_INDEX botPed = GET_PLAYER_PED(NETWORK_BOT_GET_PLAYER_ID(NETWORK_BOTS[botDataIndex].botIndex))
	
	IF NOT IS_ENTITY_DEAD(botPed)
		IF GET_SCRIPT_TASK_STATUS(botPed, SCRIPT_TASK_ENTER_VEHICLE) = PERFORMING_TASK
			IF IS_ENTITY_DEAD(NETWORK_BOTS[botDataIndex].targetVehicle)
				CLEAR_PED_TASKS_IMMEDIATELY(botPed)
			ENDIF
		ELSE
			NET_PRINT("[soaktest] New state: IDLE") NET_NL()
			NETWORK_BOTS[botDataIndex].currentState = STATE_IDLE
			NETWORK_BOTS[botDataIndex].timeToIdle = GET_TIME_OFFSET(GET_NETWORK_TIME() , GET_RANDOM_INT_IN_RANGE(10000, 30000))
		ENDIF
	ENDIF
ENDPROC

PROC START_JACK_PASSENGER_STATE(INT botDataIndex)

	BOOL targetVehicleValid = FALSE
	
	VEHICLE_INDEX carIndex = NETWORK_BOTS[botDataIndex].targetVehicle
	IF carIndex != NULL
		IF NOT IS_ENTITY_DEAD(carIndex)
			PED_INDEX passengerPed = GET_PED_IN_VEHICLE_SEAT(carIndex, VS_FRONT_RIGHT)
			IF passengerPed != NULL
				targetVehicleValid = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT targetVehicleValid
		NET_PRINT("[soaktest] New state: IDLE") NET_NL()
		NETWORK_BOTS[botDataIndex].targetVehicle = NULL
		NETWORK_BOTS[botDataIndex].currentState = STATE_IDLE
		NETWORK_BOTS[botDataIndex].timeToIdle = GET_TIME_OFFSET(GET_NETWORK_TIME() , GET_RANDOM_INT_IN_RANGE(10000, 30000))
	ELSE
		PED_INDEX botPed = GET_PLAYER_PED(NETWORK_BOT_GET_PLAYER_ID(NETWORK_BOTS[botDataIndex].botIndex))
		IF NOT IS_ENTITY_DEAD(botPed)
			TASK_ENTER_VEHICLE(botPed, carIndex, DEFAULT_TIME_NEVER_WARP, VS_FRONT_RIGHT, PEDMOVE_RUN, ECF_JACK_ANYONE | ECF_RESUME_IF_INTERRUPTED)
			NETWORK_BOTS[botDataIndex].lastState = NETWORK_BOTS[botDataIndex].currentState
		ENDIF
	ENDIF
ENDPROC

PROC UPDATE_JACK_PASSENGER_STATE(INT botDataIndex)
	
	PED_INDEX botPed = GET_PLAYER_PED(NETWORK_BOT_GET_PLAYER_ID(NETWORK_BOTS[botDataIndex].botIndex))
	
	IF NOT IS_ENTITY_DEAD(botPed)
		IF GET_SCRIPT_TASK_STATUS(botPed, SCRIPT_TASK_ENTER_VEHICLE) = PERFORMING_TASK
			IF IS_ENTITY_DEAD(NETWORK_BOTS[botDataIndex].targetVehicle)
				CLEAR_PED_TASKS_IMMEDIATELY(botPed)
			ELSE 
			ENDIF
		ELSE
			NET_PRINT("[soaktest] New state: IDLE") NET_NL()
			NETWORK_BOTS[botDataIndex].currentState = STATE_IDLE
			NETWORK_BOTS[botDataIndex].timeToIdle = GET_TIME_OFFSET(GET_NETWORK_TIME() , GET_RANDOM_INT_IN_RANGE(10000, 30000))
		ENDIF
	ENDIF
ENDPROC

PROC START_LEAVE_VEHICLE_STATE(INT botDataIndex)

	PED_INDEX botPed = GET_PLAYER_PED(NETWORK_BOT_GET_PLAYER_ID(NETWORK_BOTS[botDataIndex].botIndex))
	
	IF NOT IS_ENTITY_DEAD(botPed)
		IF IS_PED_IN_ANY_VEHICLE(botPed)
			TASK_LEAVE_ANY_VEHICLE(botPed)
			NETWORK_BOTS[botDataIndex].lastState = NETWORK_BOTS[botDataIndex].currentState
		ENDIF
	ENDIF
ENDPROC

PROC UPDATE_LEAVE_VEHICLE_STATE(INT botDataIndex)
	
	PED_INDEX botPed = GET_PLAYER_PED(NETWORK_BOT_GET_PLAYER_ID(NETWORK_BOTS[botDataIndex].botIndex))
	
	IF NOT IS_ENTITY_DEAD(botPed)
		IF GET_SCRIPT_TASK_STATUS(botPed, SCRIPT_TASK_LEAVE_VEHICLE) != PERFORMING_TASK
			NETWORK_BOTS[botDataIndex].currentState = STATE_IDLE
			NETWORK_BOTS[botDataIndex].timeToIdle = GET_TIME_OFFSET(GET_NETWORK_TIME() , GET_RANDOM_INT_IN_RANGE(10000, 30000))
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SOAK_TEST()
	INT i
	INT team
	//INT timeToWait
	
	// update the countdown timers array (used for widget debugging
	REPEAT MAX_BOTS_TO_CREATE i
	BotCountdownForWidgets[i] = GET_TIME_DIFFERENCE(NETWORK_BOTS[i].timeToIdle , GET_NETWORK_TIME())
	
	IF BotCountdownForWidgets[i] < 0
		BotCountdownForWidgets[i] = 0
	ENDIF
	ENDREPEAT

	// update the list of cars if we are not the host
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
		REPEAT MAX_CARS_TO_CREATE i
			NETWORK_INDEX netIndex = INT_TO_NATIVE(NETWORK_INDEX, GlobalServerBD_JST.TARGET_CARS[i])
			IF NETWORK_DOES_NETWORK_ID_EXIST(netIndex)
				NETWORK_CARS[i] = NET_TO_VEH(netIndex)
			ELSE
				NETWORK_CARS[i] = NULL
			ENDIF
		ENDREPEAT
	ENDIF
	
	REPEAT MAX_BOTS_TO_CREATE i
		INT botIndex = NETWORK_BOTS[i].botIndex
		IF botIndex != -1
			IF NATIVE_TO_INT(NETWORK_BOT_GET_PLAYER_ID(botIndex)) != -1
				IF NETWORK_PLAYER_HAS_PED(NETWORK_BOT_GET_PLAYER_ID(botIndex))			
					PED_INDEX botPed = GET_PLAYER_PED(NETWORK_BOT_GET_PLAYER_ID(botIndex))
				
					IF NOT IS_ENTITY_DEAD(botPed)
						SWITCH NETWORK_BOTS[i].currentState
							CASE STATE_PICK_TEAM
								NETWORK_BOT_JOIN_THIS_SCRIPT(NETWORK_BOTS[i].botIndex)
								NETWORK_BOT_JOIN_SCRIPT(g_MainMissionScriptID, NETWORK_BOTS[i].botIndex)
								team = i%2
								SET_PLAYER_TEAM(NETWORK_BOT_GET_PLAYER_ID(botIndex), team)
								NETWORK_BOTS[i].currentState = STATE_IDLE
								NETWORK_BOTS[i].timeToIdle = GET_TIME_OFFSET(GET_NETWORK_TIME() , GET_RANDOM_INT_IN_RANGE(10000, 30000))
							BREAK
							CASE STATE_IDLE
								IF IS_TIME_MORE_THAN(GET_NETWORK_TIME() , NETWORK_BOTS[i].timeToIdle)
									DECIDE_NEW_BOT_STATE(i)
								ELSE
									//timeToWait = NETWORK_BOTS[i].timeToIdle - currentTime
									//NET_PRINT("[soaktest]Time to wait in idle state: ") 
									//NET_PRINT_INT(timeToWait)
									//NET_NL()
								ENDIF
							BREAK
							CASE STATE_ENTER_EMPTY_VEHICLE
								IF NETWORK_BOTS[i].currentState != NETWORK_BOTS[i].lastState
									START_ENTER_EMPTY_VEHICLE_STATE(i)
								ELSE
									UPDATE_ENTER_EMPTY_VEHICLE_STATE(i)
								ENDIF
							BREAK
							CASE STATE_ENTER_AS_PASSENGER
								IF NETWORK_BOTS[i].currentState != NETWORK_BOTS[i].lastState
									START_ENTER_AS_PASSENGER_STATE(i)
								ELSE
									UPDATE_ENTER_AS_PASSENGER_STATE(i)
								ENDIF
							BREAK
							CASE STATE_JACK_DRIVER
								IF NETWORK_BOTS[i].currentState != NETWORK_BOTS[i].lastState
									START_JACK_DRIVER_STATE(i)
								ELSE
									UPDATE_JACK_DRIVER_STATE(i)
								ENDIF
							BREAK
							CASE STATE_JACK_PASSENGER
								IF NETWORK_BOTS[i].currentState != NETWORK_BOTS[i].lastState
									START_JACK_PASSENGER_STATE(i)
								ELSE
									UPDATE_JACK_PASSENGER_STATE(i)
								ENDIF
							BREAK
							CASE STATE_LEAVE_VEHICLE
								IF NETWORK_BOTS[i].currentState != NETWORK_BOTS[i].lastState
									START_LEAVE_VEHICLE_STATE(i)
								ELSE
									UPDATE_LEAVE_VEHICLE_STATE(i)
								ENDIF
							BREAK
						ENDSWITCH
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC


//PURPOSE: Check whether the brain should terminate
FUNC BOOL SHOULD_THIS_SOAK_TEST_TERMINATE()
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()	
		NET_PRINT_TIME() NET_PRINT_THREAD_ID() NET_PRINT(GET_THIS_SCRIPT_NAME())
		NET_PRINT(" SHOULD_THIS_MULTIPLAYER_SCRIPT_TERMINATE NETWORK_IS_GAME_IN_PROGRESS() = FALSE, bail.") NET_NL()
		RETURN(TRUE)
	ENDIF
	
	IF RECEIVED_EVENT(EVENT_NETWORK_END_MATCH)	
		NET_PRINT_TIME() NET_PRINT_THREAD_ID() NET_PRINT(GET_THIS_SCRIPT_NAME())
		NET_PRINT(" SHOULD_THIS_MULTIPLAYER_SCRIPT_TERMINATE RECEIVED match end event") NET_NL()		
		RETURN(TRUE)
	ENDIF
	
	IF RECEIVED_EVENT(EVENT_NETWORK_SESSION_END)	
		NET_PRINT_TIME() NET_PRINT_THREAD_ID() NET_PRINT(GET_THIS_SCRIPT_NAME())
		NET_PRINT(" SHOULD_THIS_MULTIPLAYER_SCRIPT_TERMINATE RECEIVED session end event") NET_NL()		
		RETURN(TRUE)
	ENDIF	

	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_OF_LAUNCH_SCRIPT_NAME_SCRIPT()) = 0				
		NET_PRINT_TIME() NET_PRINT_THREAD_ID() NET_PRINT(GET_THIS_SCRIPT_NAME())
		NET_PRINT(" SHOULD_THIS_MULTIPLAYER_SCRIPT_TERMINATE Launch script not running, bail.") NET_NL()
		RETURN(TRUE)	
	ENDIF

	RETURN(FALSE)
ENDFUNC

PROC CREATE_WIDGETS()
	INT i
	TEXT_LABEL_63 tl63
	
	START_WIDGET_GROUP("Car Jacking Soak Test")
		START_WIDGET_GROUP("Server BD") 
			REPEAT MAX_CARS_TO_CREATE i
				ADD_WIDGET_INT_READ_ONLY("Car ID", GlobalServerBD_JST.TARGET_CARS[i])
			ENDREPEAT
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Bots") 		
			REPEAT MAX_BOTS_TO_CREATE i
				tl63 = "Bot "
				tl63 += i
				START_WIDGET_GROUP(tl63)
					ADD_WIDGET_INT_READ_ONLY("Bot index", NETWORK_BOTS[i].botIndex)
					ADD_WIDGET_INT_READ_ONLY("Current state", NETWORK_BOTS[i].currentState)
					ADD_WIDGET_INT_READ_ONLY("Last state", NETWORK_BOTS[i].lastState)
					ADD_WIDGET_INT_READ_ONLY("Time to idle", BotCountdownForWidgets[i])
				STOP_WIDGET_GROUP()
			ENDREPEAT
		STOP_WIDGET_GROUP()	
	STOP_WIDGET_GROUP()
ENDPROC

#ENDIF
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                      Main Loop                             ////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
SCRIPT
	#IF IS_DEBUG_BUILD

	g_bProcessBots = FALSE
	
	NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE)
	
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	WHILE NOT g_bMainMissionScriptActive
		NET_PRINT("[soaktest]Waiting for main script to become active") NET_NL()
		WAIT(0)
	ENDWHILE
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(GlobalServerBD_JST, SIZE_OF(GlobalServerBD_JST))
	
	// KGM: Wait for the first network broadcast before moving on
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		NET_PRINT("[soaktest] Failed to receive an initial network broadcast. Terminating.") NET_NL()
		TERMINATE_THIS_THREAD()
	ENDIF
	
	//Initialise the bot
	INITIALISE_SOAK_TEST()

	// Setup widgets						 
	CREATE_WIDGETS()

	// Main loop.
	WHILE TRUE

		//Only have ONE wait in your scripts
		WAIT(0) 
		
		PROCESS_SOAK_TEST()
		
		// If we have a match end event, bail.
		IF SHOULD_THIS_SOAK_TEST_TERMINATE()
			g_bProcessBots = TRUE
			TERMINATE_THIS_THREAD()
		ENDIF

	ENDWHILE
	#ENDIF
ENDSCRIPT

//EOF//////








