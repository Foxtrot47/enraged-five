USING "hud_drawing.sch"
USING "screens_header.sch"
USING "screen_placements.sch"


#IF COMPILE_WIDGET_OUTPUT
PROC GET_SPRITE_PLACEMENT_STRINGS(SPRITE_PLACEMENT &SpritePlacement[])
	INT iPlacementIndex
	REPEAT COUNT_OF(SpritePlacement) iPlacementIndex			
		IF DOES_TEXT_WIDGET_EXIST(SpritePlacement[iPlacementIndex].widSpriteName)
			SpritePlacement[iPlacementIndex].txtSpriteName = GET_CONTENTS_OF_TEXT_WIDGET(SpritePlacement[iPlacementIndex].widSpriteName)
		ENDIF
	ENDREPEAT
ENDPROC

PROC GET_RECT_PLACEMENT_STRINGS(RECT &RectPlacement[])
	INT iPlacementIndex
	REPEAT COUNT_OF(RectPlacement) iPlacementIndex			
		IF DOES_TEXT_WIDGET_EXIST(RectPlacement[iPlacementIndex].widRectName)
			RectPlacement[iPlacementIndex].txtRectName = GET_CONTENTS_OF_TEXT_WIDGET(RectPlacement[iPlacementIndex].widRectName)
		ENDIF
	ENDREPEAT
ENDPROC

PROC GET_TEXT_PLACEMENT_STRINGS(TEXT_PLACEMENT &TextPlacement[])
	INT iPlacementIndex
	REPEAT COUNT_OF(TextPlacement) iPlacementIndex			
		IF DOES_TEXT_WIDGET_EXIST(TextPlacement[iPlacementIndex].widTextName)
			TextPlacement[iPlacementIndex].strName = GET_CONTENTS_OF_TEXT_WIDGET(TextPlacement[iPlacementIndex].widTextName)
		ENDIF
	ENDREPEAT
ENDPROC

PROC GET_ALL_PLACEMENT_STRINGS(MEGA_PLACEMENT_TOOLS& Placements)
	Placements.txtScreenName = GET_CONTENTS_OF_TEXT_WIDGET(Placements.widScreenName)
	Placements.txtFileName = GET_CONTENTS_OF_TEXT_WIDGET(Placements.widFileName)
	Placements.txtFilePath = GET_CONTENTS_OF_TEXT_WIDGET(Placements.widFilePath)
	GET_SPRITE_PLACEMENT_STRINGS(Placements.SpritePlacement)
	GET_RECT_PLACEMENT_STRINGS(Placements.RectPlacement)
	GET_TEXT_PLACEMENT_STRINGS(Placements.TextPlacement)
ENDPROC

PROC WRITE_SPRITE_PLACEMENT_ARRAY(SPRITE_PLACEMENT &SpritePlacement, STRING txtScreenName, BOOL bWithTab = TRUE)
	IF bWithTab
		SAVE_STRING_TO_DEBUG_FILE("	")
	ENDIF
	SAVE_STRING_TO_DEBUG_FILE("thisPlacement.SpritePlacement[")
	SAVE_STRING_TO_DEBUG_FILE(txtScreenName)
	SAVE_STRING_TO_DEBUG_FILE("_")
	SAVE_STRING_TO_DEBUG_FILE(SpritePlacement.txtSpriteName)
	SAVE_STRING_TO_DEBUG_FILE("]")
ENDPROC

PROC WRITE_TEXT_PLACEMENT_ARRAY(TEXT_PLACEMENT &TextPlacement, STRING txtScreenName, BOOL bWithTab = TRUE)
	IF bWithTab
		SAVE_STRING_TO_DEBUG_FILE("	")
	ENDIF
	SAVE_STRING_TO_DEBUG_FILE("thisPlacement.TextPlacement[")
	SAVE_STRING_TO_DEBUG_FILE(txtScreenName)
	SAVE_STRING_TO_DEBUG_FILE("_")
	SAVE_STRING_TO_DEBUG_FILE(TextPlacement.strName)
	SAVE_STRING_TO_DEBUG_FILE("]")
ENDPROC

PROC WRITE_RECT_PLACEMENT_ARRAY(RECT &RectPlacement, STRING txtScreenName, BOOL bWithTab = TRUE)
	IF bWithTab
		SAVE_STRING_TO_DEBUG_FILE("	")
	ENDIF
	SAVE_STRING_TO_DEBUG_FILE("thisPlacement.RectPlacement[")
	SAVE_STRING_TO_DEBUG_FILE(txtScreenName)
	SAVE_STRING_TO_DEBUG_FILE("_")
	SAVE_STRING_TO_DEBUG_FILE(RectPlacement.txtRectName)
	SAVE_STRING_TO_DEBUG_FILE("]")
ENDPROC


PROC WRITE_SPRITE_PLACEMENT_STRINGS(SPRITE_PLACEMENT &SpritePlacement[], STRING txtScreenName)
	INT iPlacementIndex
	REPEAT COUNT_OF(SpritePlacement) iPlacementIndex
		IF NOT IS_STRING_EMPTY_HUD(SpritePlacement[iPlacementIndex].txtSpriteName)
			WRITE_SPRITE_PLACEMENT_ARRAY(SpritePlacement[iPlacementIndex], txtScreenName)
			SAVE_STRING_TO_DEBUG_FILE(".txtSpriteName = \"")
			SAVE_STRING_TO_DEBUG_FILE(SpritePlacement[iPlacementIndex].txtSpriteName)
			SAVE_STRING_TO_DEBUG_FILE("\"")
			SAVE_NEWLINE_TO_DEBUG_FILE()
		ENDIF
	ENDREPEAT
ENDPROC

PROC WRITE_TEXT_PLACEMENT_STRINGS(TEXT_PLACEMENT &TextPlacement[], STRING txtScreenName)
	INT iPlacementIndex
	REPEAT COUNT_OF(TextPlacement) iPlacementIndex
		IF NOT IS_STRING_EMPTY_HUD(TextPlacement[iPlacementIndex].strName)
			WRITE_TEXT_PLACEMENT_ARRAY(TextPlacement[iPlacementIndex], txtScreenName)
			SAVE_STRING_TO_DEBUG_FILE(".strName = \"")
			SAVE_STRING_TO_DEBUG_FILE(TextPlacement[iPlacementIndex].strName)
			SAVE_STRING_TO_DEBUG_FILE("\"")
			SAVE_NEWLINE_TO_DEBUG_FILE()
		ENDIF
	ENDREPEAT
ENDPROC
	
PROC WRITE_RECT_PLACEMENT_STRINGS(RECT &RectPlacement[], STRING txtScreenName)
	INT iPlacementIndex
	REPEAT COUNT_OF(RectPlacement) iPlacementIndex
		IF NOT IS_STRING_EMPTY_HUD(RectPlacement[iPlacementIndex].txtRectName)
			WRITE_RECT_PLACEMENT_ARRAY(RectPlacement[iPlacementIndex], txtScreenName)
			SAVE_STRING_TO_DEBUG_FILE(".txtRectName = \"")
			SAVE_STRING_TO_DEBUG_FILE(RectPlacement[iPlacementIndex].txtRectName)
			SAVE_STRING_TO_DEBUG_FILE("\"")
			SAVE_NEWLINE_TO_DEBUG_FILE()
		ENDIF
	ENDREPEAT
ENDPROC



PROC WRITE_MEGA_PLACEMENT_FUNCTION_HEADER(MEGA_PLACEMENT_TOOLS &thisPlacement)
	SAVE_NEWLINE_TO_DEBUG_FILE()
	SAVE_STRING_TO_DEBUG_FILE("PROC INIT_SCREEN_")
	SAVE_STRING_TO_DEBUG_FILE(thisPlacement.txtScreenName)
	SAVE_STRING_TO_DEBUG_FILE("_MENU(MEGA_PLACEMENT_TOOLS &thisPlacement)")
	SAVE_NEWLINE_TO_DEBUG_FILE()
	SAVE_STRING_TO_DEBUG_FILE("#IF COMPILE_WIDGET_OUTPUT")
	SAVE_NEWLINE_TO_DEBUG_FILE()
	SAVE_STRING_TO_DEBUG_FILE("	thisPlacement.txtScreenName = \"")
	SAVE_STRING_TO_DEBUG_FILE(thisPlacement.txtScreenName)
	SAVE_STRING_TO_DEBUG_FILE("\"")
	SAVE_NEWLINE_TO_DEBUG_FILE()
	SAVE_STRING_TO_DEBUG_FILE("	thisPlacement.txtFileName = \"")
	SAVE_STRING_TO_DEBUG_FILE(thisPlacement.txtFileName)
	SAVE_STRING_TO_DEBUG_FILE("\"")
	SAVE_NEWLINE_TO_DEBUG_FILE()
	SAVE_STRING_TO_DEBUG_FILE("	thisPlacement.txtFilePath = \"")
	SAVE_STRING_TO_DEBUG_FILE(thisPlacement.txtFilePath)
	SAVE_STRING_TO_DEBUG_FILE("\"")
	SAVE_NEWLINE_TO_DEBUG_FILE()
	
	WRITE_SPRITE_PLACEMENT_STRINGS(thisPlacement.SpritePlacement, thisPlacement.txtScreenName)
	WRITE_TEXT_PLACEMENT_STRINGS(thisPlacement.TextPlacement, thisPlacement.txtScreenName)
	WRITE_RECT_PLACEMENT_STRINGS(thisPlacement.RectPlacement, thisPlacement.txtScreenName)
		
	SAVE_STRING_TO_DEBUG_FILE("#ENDIF")
	SAVE_NEWLINE_TO_DEBUG_FILE()
ENDPROC

PROC WRITE_LARGE_PLACEMENT_FUNCTION_HEADER(LARGE_PLACEMENT_TOOLS &thisPlacement)
	SAVE_NEWLINE_TO_DEBUG_FILE()
	SAVE_STRING_TO_DEBUG_FILE("PROC INIT_SCREEN_")
	SAVE_STRING_TO_DEBUG_FILE(thisPlacement.txtScreenName)
	SAVE_STRING_TO_DEBUG_FILE("_MENU(LARGE_PLACEMENT_TOOLS &thisPlacement)")
	SAVE_NEWLINE_TO_DEBUG_FILE()
	SAVE_STRING_TO_DEBUG_FILE("#IF COMPILE_WIDGET_OUTPUT")
	SAVE_NEWLINE_TO_DEBUG_FILE()
	SAVE_STRING_TO_DEBUG_FILE("	thisPlacement.txtScreenName = \"")
	SAVE_STRING_TO_DEBUG_FILE(thisPlacement.txtScreenName)
	SAVE_STRING_TO_DEBUG_FILE("\"")
	SAVE_NEWLINE_TO_DEBUG_FILE()
	SAVE_STRING_TO_DEBUG_FILE("	thisPlacement.txtFileName = \"")
	SAVE_STRING_TO_DEBUG_FILE(thisPlacement.txtFileName)
	SAVE_STRING_TO_DEBUG_FILE("\"")
	SAVE_NEWLINE_TO_DEBUG_FILE()
	SAVE_STRING_TO_DEBUG_FILE("	thisPlacement.txtFilePath = \"")
	SAVE_STRING_TO_DEBUG_FILE(thisPlacement.txtFilePath)
	SAVE_STRING_TO_DEBUG_FILE("\"")
	SAVE_NEWLINE_TO_DEBUG_FILE()
	
	WRITE_SPRITE_PLACEMENT_STRINGS(thisPlacement.SpritePlacement, thisPlacement.txtScreenName)
	WRITE_TEXT_PLACEMENT_STRINGS(thisPlacement.TextPlacement, thisPlacement.txtScreenName)
	WRITE_RECT_PLACEMENT_STRINGS(thisPlacement.RectPlacement, thisPlacement.txtScreenName)
		
	SAVE_STRING_TO_DEBUG_FILE("#ENDIF")
	SAVE_NEWLINE_TO_DEBUG_FILE()
ENDPROC


PROC WRITE_PLACEMENT_FUNCTION_HEADER(PLACEMENT_TOOLS &thisPlacement)
	SAVE_NEWLINE_TO_DEBUG_FILE()
	SAVE_STRING_TO_DEBUG_FILE("PROC INIT_SCREEN_")
	SAVE_STRING_TO_DEBUG_FILE(thisPlacement.txtScreenName)
	SAVE_STRING_TO_DEBUG_FILE("_MENU(PLACEMENT_TOOLS &thisPlacement)")
	SAVE_NEWLINE_TO_DEBUG_FILE()
	SAVE_STRING_TO_DEBUG_FILE("#IF COMPILE_WIDGET_OUTPUT")
	SAVE_NEWLINE_TO_DEBUG_FILE()
	SAVE_STRING_TO_DEBUG_FILE("	thisPlacement.txtScreenName = \"")
	SAVE_STRING_TO_DEBUG_FILE(thisPlacement.txtScreenName)
	SAVE_STRING_TO_DEBUG_FILE("\"")
	SAVE_NEWLINE_TO_DEBUG_FILE()
	SAVE_STRING_TO_DEBUG_FILE("	thisPlacement.txtFileName = \"")
	SAVE_STRING_TO_DEBUG_FILE(thisPlacement.txtFileName)
	SAVE_STRING_TO_DEBUG_FILE("\"")
	SAVE_NEWLINE_TO_DEBUG_FILE()
	SAVE_STRING_TO_DEBUG_FILE("	thisPlacement.txtFilePath = \"")
	SAVE_STRING_TO_DEBUG_FILE(thisPlacement.txtFilePath)
	SAVE_STRING_TO_DEBUG_FILE("\"")
	SAVE_NEWLINE_TO_DEBUG_FILE()
	
	WRITE_SPRITE_PLACEMENT_STRINGS(thisPlacement.SpritePlacement, thisPlacement.txtScreenName)
	WRITE_TEXT_PLACEMENT_STRINGS(thisPlacement.TextPlacement, thisPlacement.txtScreenName)
	WRITE_RECT_PLACEMENT_STRINGS(thisPlacement.RectPlacement, thisPlacement.txtScreenName)
		
	SAVE_STRING_TO_DEBUG_FILE("#ENDIF")
	SAVE_NEWLINE_TO_DEBUG_FILE()
ENDPROC

//PROC WRITE_SMALL_PLACEMENT_FUNCTION_HEADER(SMALL_PLACEMENT_TOOLS &thisPlacement)
//	SAVE_NEWLINE_TO_DEBUG_FILE()
//	SAVE_STRING_TO_DEBUG_FILE("PROC INIT_SCREEN_")
//	SAVE_STRING_TO_DEBUG_FILE(thisPlacement.txtScreenName)
//	SAVE_STRING_TO_DEBUG_FILE("_MENU(SMALL_PLACEMENT_TOOLS &thisPlacement)")
//	SAVE_NEWLINE_TO_DEBUG_FILE()
//	SAVE_STRING_TO_DEBUG_FILE("#IF COMPILE_WIDGET_OUTPUT")
//	SAVE_NEWLINE_TO_DEBUG_FILE()
//	SAVE_STRING_TO_DEBUG_FILE("	thisPlacement.txtScreenName = \"")
//	SAVE_STRING_TO_DEBUG_FILE(thisPlacement.txtScreenName)
//	SAVE_STRING_TO_DEBUG_FILE("\"")
//	SAVE_NEWLINE_TO_DEBUG_FILE()
//	SAVE_STRING_TO_DEBUG_FILE("	thisPlacement.txtFileName = \"")
//	SAVE_STRING_TO_DEBUG_FILE(thisPlacement.txtFileName)
//	SAVE_STRING_TO_DEBUG_FILE("\"")
//	SAVE_NEWLINE_TO_DEBUG_FILE()
//	SAVE_STRING_TO_DEBUG_FILE("	thisPlacement.txtFilePath = \"")
//	SAVE_STRING_TO_DEBUG_FILE(thisPlacement.txtFilePath)
//	SAVE_STRING_TO_DEBUG_FILE("\"")
//	SAVE_NEWLINE_TO_DEBUG_FILE()
//	
//	WRITE_SPRITE_PLACEMENT_STRINGS(thisPlacement.SpritePlacement, thisPlacement.txtScreenName)
//	WRITE_TEXT_PLACEMENT_STRINGS(thisPlacement.TextPlacement, thisPlacement.txtScreenName)
//	WRITE_RECT_PLACEMENT_STRINGS(thisPlacement.RectPlacement, thisPlacement.txtScreenName)
//		
//	SAVE_STRING_TO_DEBUG_FILE("#ENDIF")
//	SAVE_NEWLINE_TO_DEBUG_FILE()
//ENDPROC


PROC WRITE_SINGLE_SPRITE_PLACEMENT(SPRITE_PLACEMENT &SpritePlacement, STRING txtScreenName, BOOL bExportAsPixels = FALSE)
	IF NOT bExportAsPixels
		// Float values (ie: 0.0065)
		WRITE_SPRITE_PLACEMENT_ARRAY(SpritePlacement, txtScreenName)
		SAVE_STRING_TO_DEBUG_FILE(".x = ")
		SAVE_FLOAT_TO_DEBUG_FILE(SpritePlacement.x)
		SAVE_NEWLINE_TO_DEBUG_FILE()

		WRITE_SPRITE_PLACEMENT_ARRAY(SpritePlacement, txtScreenName)
		SAVE_STRING_TO_DEBUG_FILE(".y = ")
		SAVE_FLOAT_TO_DEBUG_FILE(SpritePlacement.y)
		SAVE_NEWLINE_TO_DEBUG_FILE()

		WRITE_SPRITE_PLACEMENT_ARRAY(SpritePlacement, txtScreenName)
		SAVE_STRING_TO_DEBUG_FILE(".w = ")
		SAVE_FLOAT_TO_DEBUG_FILE(SpritePlacement.w)
		SAVE_NEWLINE_TO_DEBUG_FILE()

		WRITE_SPRITE_PLACEMENT_ARRAY(SpritePlacement, txtScreenName)
		SAVE_STRING_TO_DEBUG_FILE(".h = ")
		SAVE_FLOAT_TO_DEBUG_FILE(SpritePlacement.h)
		SAVE_NEWLINE_TO_DEBUG_FILE()
	ELSE
		// Pixel-perfect way. (ie: 205.000)
		SAVE_STRING_TO_DEBUG_FILE("	PIXEL_POSITION_AND_SIZE_SPRITE(")
		WRITE_SPRITE_PLACEMENT_ARRAY(SpritePlacement, txtScreenName, FALSE)
		SAVE_STRING_TO_DEBUG_FILE(", ")
		SAVE_FLOAT_TO_DEBUG_FILE(FLOAT_X_TO_PIXEL(SpritePlacement.x))
		SAVE_STRING_TO_DEBUG_FILE(", ")
		SAVE_FLOAT_TO_DEBUG_FILE(FLOAT_Y_TO_PIXEL(SpritePlacement.y))
		SAVE_STRING_TO_DEBUG_FILE(", ")
		SAVE_FLOAT_TO_DEBUG_FILE(FLOAT_X_TO_PIXEL(SpritePlacement.w))
		SAVE_STRING_TO_DEBUG_FILE(", ")
		SAVE_FLOAT_TO_DEBUG_FILE(FLOAT_Y_TO_PIXEL(SpritePlacement.h))
		//SAVE_STRING_TO_DEBUG_FILE(", TRUE")
		SAVE_STRING_TO_DEBUG_FILE(")")
		SAVE_NEWLINE_TO_DEBUG_FILE()
	ENDIF
	
	// Always write the rotations and RGBA.
	WRITE_SPRITE_PLACEMENT_ARRAY(SpritePlacement, txtScreenName)
	SAVE_STRING_TO_DEBUG_FILE(".fRotation = ")
	SAVE_FLOAT_TO_DEBUG_FILE(SpritePlacement.fRotation)
	SAVE_NEWLINE_TO_DEBUG_FILE()

	WRITE_SPRITE_PLACEMENT_ARRAY(SpritePlacement, txtScreenName)
	SAVE_STRING_TO_DEBUG_FILE(".r = ")
	SAVE_INT_TO_DEBUG_FILE(SpritePlacement.r)
	SAVE_NEWLINE_TO_DEBUG_FILE()

	WRITE_SPRITE_PLACEMENT_ARRAY(SpritePlacement, txtScreenName)
	SAVE_STRING_TO_DEBUG_FILE(".g = ")
	SAVE_INT_TO_DEBUG_FILE(SpritePlacement.g)
	SAVE_NEWLINE_TO_DEBUG_FILE()

	WRITE_SPRITE_PLACEMENT_ARRAY(SpritePlacement, txtScreenName)
	SAVE_STRING_TO_DEBUG_FILE(".b = ")
	SAVE_INT_TO_DEBUG_FILE(SpritePlacement.b)
	SAVE_NEWLINE_TO_DEBUG_FILE()

	WRITE_SPRITE_PLACEMENT_ARRAY(SpritePlacement, txtScreenName)
	SAVE_STRING_TO_DEBUG_FILE(".a = ")
	SAVE_INT_TO_DEBUG_FILE(SpritePlacement.a)
	SAVE_NEWLINE_TO_DEBUG_FILE()
ENDPROC

	
PROC WRITE_SINGLE_TEXT_PLACEMENT(TEXT_PLACEMENT &TextPlacement, STRING txtScreenName, BOOL bExportAsPixels = FALSE)
	IF NOT bExportAsPixels
		// Float values (ie: 0.0065)
		WRITE_TEXT_PLACEMENT_ARRAY(TextPlacement, txtScreenName)
		SAVE_STRING_TO_DEBUG_FILE(".x = ")
		SAVE_FLOAT_TO_DEBUG_FILE(TextPlacement.x)
		SAVE_NEWLINE_TO_DEBUG_FILE()

		WRITE_TEXT_PLACEMENT_ARRAY(TextPlacement, txtScreenName)
		SAVE_STRING_TO_DEBUG_FILE(".y = ")
		SAVE_FLOAT_TO_DEBUG_FILE(TextPlacement.y)
		SAVE_NEWLINE_TO_DEBUG_FILE()
	ELSE
		// Pixel-perfect way. (ie: 205.000)
		SAVE_STRING_TO_DEBUG_FILE("	PIXEL_POSITION_TEXT(")
		WRITE_TEXT_PLACEMENT_ARRAY(TextPlacement, txtScreenName, FALSE)
		SAVE_STRING_TO_DEBUG_FILE(", ")
		SAVE_FLOAT_TO_DEBUG_FILE(FLOAT_X_TO_PIXEL(TextPlacement.x))
		SAVE_STRING_TO_DEBUG_FILE(", ")
		SAVE_FLOAT_TO_DEBUG_FILE(FLOAT_Y_TO_PIXEL(TextPlacement.y))
		SAVE_STRING_TO_DEBUG_FILE(")")
		SAVE_NEWLINE_TO_DEBUG_FILE()
	ENDIF
ENDPROC


PROC WRITE_SINGLE_RECT_PLACEMENT(RECT &RectPlacement, STRING txtScreenName, BOOL bExportAsPixels = FALSE)
	IF NOT bExportAsPixels
		// Float values (ie: 0.0065)
		WRITE_RECT_PLACEMENT_ARRAY(RectPlacement, txtScreenName)
		SAVE_STRING_TO_DEBUG_FILE(".x = ")
		SAVE_FLOAT_TO_DEBUG_FILE(RectPlacement.x)
		SAVE_NEWLINE_TO_DEBUG_FILE()

		WRITE_RECT_PLACEMENT_ARRAY(RectPlacement, txtScreenName)
		SAVE_STRING_TO_DEBUG_FILE(".y = ")
		SAVE_FLOAT_TO_DEBUG_FILE(RectPlacement.y)
		SAVE_NEWLINE_TO_DEBUG_FILE()

		WRITE_RECT_PLACEMENT_ARRAY(RectPlacement, txtScreenName)
		SAVE_STRING_TO_DEBUG_FILE(".w = ")
		SAVE_FLOAT_TO_DEBUG_FILE(RectPlacement.w)
		SAVE_NEWLINE_TO_DEBUG_FILE()

		WRITE_RECT_PLACEMENT_ARRAY(RectPlacement, txtScreenName)
		SAVE_STRING_TO_DEBUG_FILE(".h = ")
		SAVE_FLOAT_TO_DEBUG_FILE(RectPlacement.h)
		SAVE_NEWLINE_TO_DEBUG_FILE()
	ELSE
		// Pixel-perfect way. (ie: 205.000)
		SAVE_STRING_TO_DEBUG_FILE("	PIXEL_POSITION_AND_SIZE_RECT(")
		WRITE_RECT_PLACEMENT_ARRAY(RectPlacement, txtScreenName, FALSE)
		SAVE_STRING_TO_DEBUG_FILE(", ")
		SAVE_FLOAT_TO_DEBUG_FILE(FLOAT_X_TO_PIXEL(RectPlacement.x))
		SAVE_STRING_TO_DEBUG_FILE(", ")
		SAVE_FLOAT_TO_DEBUG_FILE(FLOAT_Y_TO_PIXEL(RectPlacement.y))
		SAVE_STRING_TO_DEBUG_FILE(", ")
		SAVE_FLOAT_TO_DEBUG_FILE(FLOAT_X_TO_PIXEL(RectPlacement.w))
		SAVE_STRING_TO_DEBUG_FILE(", ")
		SAVE_FLOAT_TO_DEBUG_FILE(FLOAT_Y_TO_PIXEL(RectPlacement.h))
		SAVE_STRING_TO_DEBUG_FILE(")")
		SAVE_NEWLINE_TO_DEBUG_FILE()
	ENDIF
	
	// Always write the RGBA.
	WRITE_RECT_PLACEMENT_ARRAY(RectPlacement, txtScreenName)
	SAVE_STRING_TO_DEBUG_FILE(".r = ")
	SAVE_INT_TO_DEBUG_FILE(RectPlacement.r)
	SAVE_NEWLINE_TO_DEBUG_FILE()

	WRITE_RECT_PLACEMENT_ARRAY(RectPlacement, txtScreenName)
	SAVE_STRING_TO_DEBUG_FILE(".g = ")
	SAVE_INT_TO_DEBUG_FILE(RectPlacement.g)
	SAVE_NEWLINE_TO_DEBUG_FILE()

	WRITE_RECT_PLACEMENT_ARRAY(RectPlacement, txtScreenName)
	SAVE_STRING_TO_DEBUG_FILE(".b = ")
	SAVE_INT_TO_DEBUG_FILE(RectPlacement.b)
	SAVE_NEWLINE_TO_DEBUG_FILE()

	WRITE_RECT_PLACEMENT_ARRAY(RectPlacement, txtScreenName)
	SAVE_STRING_TO_DEBUG_FILE(".a = ")
	SAVE_INT_TO_DEBUG_FILE(RectPlacement.a)
	SAVE_NEWLINE_TO_DEBUG_FILE()
ENDPROC



PROC WRITE_SPRITE_PLACEMENTS(SPRITE_PLACEMENT &SpritePlacement[], STRING txtScreenName, BOOL bExportPixelValues = FALSE)
	INT iPlacementIndex
	REPEAT COUNT_OF(SpritePlacement) iPlacementIndex
		IF SpritePlacement[iPlacementIndex].a > 0
			WRITE_SINGLE_SPRITE_PLACEMENT(SpritePlacement[iPlacementIndex], txtScreenName, bExportPixelValues)
			SAVE_NEWLINE_TO_DEBUG_FILE()
		ENDIF
	ENDREPEAT
ENDPROC

PROC WRITE_TEXT_PLACEMENTS(TEXT_PLACEMENT &TextPlacement[], STRING txtScreenName, BOOL bExportPixelValues = FALSE)
	INT iPlacementIndex
	REPEAT COUNT_OF(TextPlacement) iPlacementIndex
		IF TextPlacement[iPlacementIndex].x != 0.0 AND TextPlacement[iPlacementIndex].y != 0.0
			WRITE_SINGLE_TEXT_PLACEMENT(TextPlacement[iPlacementIndex], txtScreenName, bExportPixelValues)
			SAVE_NEWLINE_TO_DEBUG_FILE()
		ENDIF
	ENDREPEAT
ENDPROC

PROC WRITE_RECT_PLACEMENTS(RECT &RectPlacement[], STRING txtScreenName, BOOL bExportPixelValues = FALSE)
	INT iPlacementIndex
	REPEAT COUNT_OF(RectPlacement) iPlacementIndex
		IF RectPlacement[iPlacementIndex].a > 0
			WRITE_SINGLE_RECT_PLACEMENT(RectPlacement[iPlacementIndex], txtScreenName, bExportPixelValues)
			SAVE_NEWLINE_TO_DEBUG_FILE()
		ENDIF
	ENDREPEAT
ENDPROC

PROC WRITE_PLACEMENT_FUNCTION_FOOTER()
	SAVE_STRING_TO_DEBUG_FILE("	thisPlacement.bHudScreenInitialised = TRUE")
	SAVE_NEWLINE_TO_DEBUG_FILE()
	SAVE_STRING_TO_DEBUG_FILE("ENDPROC")
	SAVE_NEWLINE_TO_DEBUG_FILE()
ENDPROC

PROC WRITE_TEXT_PLACEMENTS_ENUM(TEXT_PLACEMENT &TextPlacement[], STRING txtScreenName)
	SAVE_STRING_TO_DEBUG_FILE("ENUM ")
	SAVE_STRING_TO_DEBUG_FILE(txtScreenName)
	SAVE_STRING_TO_DEBUG_FILE("_SCREEN_TEXT")
	SAVE_NEWLINE_TO_DEBUG_FILE()
	INT iPlacementIndex
	BOOL bFirst = TRUE
	REPEAT COUNT_OF(TextPlacement) iPlacementIndex
		IF TextPlacement[iPlacementIndex].x != 0.0 AND TextPlacement[iPlacementIndex].y != 0.0
			IF NOT bFirst
				SAVE_STRING_TO_DEBUG_FILE(",")
				SAVE_NEWLINE_TO_DEBUG_FILE()
			ELSE
				bFirst = FALSE
			ENDIF
			SAVE_STRING_TO_DEBUG_FILE("	")
			SAVE_STRING_TO_DEBUG_FILE(txtScreenName)
			SAVE_STRING_TO_DEBUG_FILE("_")
			SAVE_STRING_TO_DEBUG_FILE(TextPlacement[iPlacementIndex].strName)
		ENDIF
	ENDREPEAT
	SAVE_NEWLINE_TO_DEBUG_FILE()
	SAVE_STRING_TO_DEBUG_FILE("ENDENUM")
	SAVE_NEWLINE_TO_DEBUG_FILE()
	SAVE_NEWLINE_TO_DEBUG_FILE()
ENDPROC

PROC WRITE_SPRITE_PLACEMENTS_ENUM(SPRITE_PLACEMENT &SpritePlacement[], STRING txtScreenName)
	SAVE_STRING_TO_DEBUG_FILE("ENUM ")
	SAVE_STRING_TO_DEBUG_FILE(txtScreenName)
	SAVE_STRING_TO_DEBUG_FILE("_SCREEN_SPRITE")
	SAVE_NEWLINE_TO_DEBUG_FILE()
	INT iPlacementIndex
	BOOL bFirst = TRUE
	REPEAT COUNT_OF(SpritePlacement) iPlacementIndex
		IF SpritePlacement[iPlacementIndex].a > 0
			IF NOT bFirst
				SAVE_STRING_TO_DEBUG_FILE(",")
				SAVE_NEWLINE_TO_DEBUG_FILE()
			ELSE
				bFirst = FALSE
			ENDIF
			SAVE_STRING_TO_DEBUG_FILE("	")
			SAVE_STRING_TO_DEBUG_FILE(txtScreenName)
			SAVE_STRING_TO_DEBUG_FILE("_")
			SAVE_STRING_TO_DEBUG_FILE(SpritePlacement[iPlacementIndex].txtSpriteName)
		ENDIF
	ENDREPEAT
	SAVE_NEWLINE_TO_DEBUG_FILE()
	SAVE_STRING_TO_DEBUG_FILE("ENDENUM")
	SAVE_NEWLINE_TO_DEBUG_FILE()
	SAVE_NEWLINE_TO_DEBUG_FILE()
ENDPROC



PROC WRITE_RECT_PLACEMENTS_ENUM(RECT &RectPlacement[], STRING txtScreenName)
	SAVE_STRING_TO_DEBUG_FILE("ENUM ")
	SAVE_STRING_TO_DEBUG_FILE(txtScreenName)
	SAVE_STRING_TO_DEBUG_FILE("_SCREEN_RECT")
	SAVE_NEWLINE_TO_DEBUG_FILE()
	INT iPlacementIndex
	BOOL bFirst = TRUE
	REPEAT COUNT_OF(RectPlacement) iPlacementIndex
		IF RectPlacement[iPlacementIndex].a > 0
			IF NOT bFirst
				SAVE_STRING_TO_DEBUG_FILE(",")
				SAVE_NEWLINE_TO_DEBUG_FILE()
			ELSE
				bFirst = FALSE
			ENDIF
			SAVE_STRING_TO_DEBUG_FILE("	")
			SAVE_STRING_TO_DEBUG_FILE(txtScreenName)
			SAVE_STRING_TO_DEBUG_FILE("_")
			SAVE_STRING_TO_DEBUG_FILE(RectPlacement[iPlacementIndex].txtRectName)
		ENDIF
	ENDREPEAT
	SAVE_NEWLINE_TO_DEBUG_FILE()
	SAVE_STRING_TO_DEBUG_FILE("ENDENUM")
	SAVE_NEWLINE_TO_DEBUG_FILE()
	SAVE_NEWLINE_TO_DEBUG_FILE()
ENDPROC



PROC EXPORT_SCREEN_MEGA_PLACEMENT(MEGA_PLACEMENT_TOOLS &thisPlacement)
	IF NOT thisPlacement.bOutputAllWidgets AND NOT thisPlacement.bOutputEnums// AND NOT thisPlacement.bOutputPixelValues
		thisPlacement.bOutputAllWidgets = FALSE
		thisPlacement.bOutputEnums = FALSE
		//thisPlacement.bOutputPixelValues = FALSE
		EXIT
	ENDIF
	
	GET_ALL_PLACEMENT_STRINGS(thisPlacement)
	
	IF IS_STRING_EMPTY_HUD(thisPlacement.txtFileName) OR IS_STRING_EMPTY_HUD(thisPlacement.txtFilePath)
		SCRIPT_ASSERT("Cannot export with an invalid file!")
		thisPlacement.bOutputAllWidgets = FALSE
		thisPlacement.bOutputEnums = FALSE
		EXIT
	ENDIF
	
	IF IS_STRING_EMPTY_HUD(thisPlacement.txtScreenName)
		SCRIPT_ASSERT("Cannot export with an invalid screen name!")
		thisPlacement.bOutputAllWidgets = FALSE
		thisPlacement.bOutputEnums = FALSE
		EXIT
	ENDIF
	
	// Open file (depending on if we are outputting widgets or enums
	OPEN_DEBUG_FILE()
	
	IF thisPlacement.bOutputAllWidgets// OR thisPlacement.bOutputPixelValues
		WRITE_MEGA_PLACEMENT_FUNCTION_HEADER(thisPlacement)
		WRITE_TEXT_PLACEMENTS(thisPlacement.TextPlacement, thisPlacement.txtScreenName, thisPlacement.bOutputPixelValues)
		WRITE_SPRITE_PLACEMENTS(thisPlacement.SpritePlacement, thisPlacement.txtScreenName, thisPlacement.bOutputPixelValues)
		WRITE_RECT_PLACEMENTS(thisPlacement.RectPlacement, thisPlacement.txtScreenName, thisPlacement.bOutputPixelValues)
		WRITE_PLACEMENT_FUNCTION_FOOTER()
	ELSE // We are writing enums
//		WRITE_PLACEMENT_ENUM_HEADER()
		WRITE_TEXT_PLACEMENTS_ENUM(thisPlacement.TextPlacement, thisPlacement.txtScreenName)
		WRITE_SPRITE_PLACEMENTS_ENUM(thisPlacement.SpritePlacement, thisPlacement.txtScreenName)
		WRITE_RECT_PLACEMENTS_ENUM(thisPlacement.RectPlacement, thisPlacement.txtScreenName)
//		WRITE_PLACEMENT_ENUM_FOOTER()
	ENDIF
	CLOSE_DEBUG_FILE()
	// Close file
	thisPlacement.bOutputAllWidgets = FALSE
	thisPlacement.bOutputEnums = FALSE
	thisPlacement.bOutputPixelValues = FALSE
ENDPROC

PROC EXPORT_SCREEN_LARGE_PLACEMENT(LARGE_PLACEMENT_TOOLS &thisPlacement)
	IF NOT thisPlacement.bOutputAllWidgets AND NOT thisPlacement.bOutputEnums
		thisPlacement.bOutputAllWidgets = FALSE
		thisPlacement.bOutputEnums = FALSE
		EXIT
	ENDIF
	
	GET_ALL_PLACEMENT_STRINGS(thisPlacement)
	
	IF IS_STRING_EMPTY_HUD(thisPlacement.txtFileName) OR IS_STRING_EMPTY_HUD(thisPlacement.txtFilePath)
		SCRIPT_ASSERT("Cannot export with an invalid file!")
		thisPlacement.bOutputAllWidgets = FALSE
		thisPlacement.bOutputEnums = FALSE
		EXIT
	ENDIF
	
	IF IS_STRING_EMPTY_HUD(thisPlacement.txtScreenName)
		SCRIPT_ASSERT("Cannot export with an invalid screen name!")
		thisPlacement.bOutputAllWidgets = FALSE
		thisPlacement.bOutputEnums = FALSE
		EXIT
	ENDIF
	
	// Open file (depending on if we are outputting widgets or enums
	OPEN_DEBUG_FILE()
	
	IF thisPlacement.bOutputAllWidgets
		WRITE_LARGE_PLACEMENT_FUNCTION_HEADER(thisPlacement)
		WRITE_TEXT_PLACEMENTS(thisPlacement.TextPlacement, thisPlacement.txtScreenName)
		WRITE_SPRITE_PLACEMENTS(thisPlacement.SpritePlacement, thisPlacement.txtScreenName)
		WRITE_RECT_PLACEMENTS(thisPlacement.RectPlacement, thisPlacement.txtScreenName)
		WRITE_PLACEMENT_FUNCTION_FOOTER()
	ELSE // We are writing enums
//		WRITE_PLACEMENT_ENUM_HEADER()
		WRITE_TEXT_PLACEMENTS_ENUM(thisPlacement.TextPlacement, thisPlacement.txtScreenName)
		WRITE_SPRITE_PLACEMENTS_ENUM(thisPlacement.SpritePlacement, thisPlacement.txtScreenName)
		WRITE_RECT_PLACEMENTS_ENUM(thisPlacement.RectPlacement, thisPlacement.txtScreenName)
//		WRITE_PLACEMENT_ENUM_FOOTER()
	ENDIF
	CLOSE_DEBUG_FILE()
	// Close file
	thisPlacement.bOutputAllWidgets = FALSE
	thisPlacement.bOutputEnums = FALSE
ENDPROC

PROC EXPORT_SCREEN_PLACEMENT(PLACEMENT_TOOLS &thisPlacement)
	IF NOT thisPlacement.bOutputAllWidgets AND NOT thisPlacement.bOutputEnums
		thisPlacement.bOutputAllWidgets = FALSE
		thisPlacement.bOutputEnums = FALSE
		EXIT
	ENDIF
	
	GET_ALL_PLACEMENT_STRINGS(thisPlacement)
	
	IF IS_STRING_EMPTY_HUD(thisPlacement.txtFileName) OR IS_STRING_EMPTY_HUD(thisPlacement.txtFilePath)
		SCRIPT_ASSERT("Cannot export with an invalid file!")
		thisPlacement.bOutputAllWidgets = FALSE
		thisPlacement.bOutputEnums = FALSE
		EXIT
	ENDIF
	
	IF IS_STRING_EMPTY_HUD(thisPlacement.txtScreenName)
		SCRIPT_ASSERT("Cannot export with an invalid screen name!")
		thisPlacement.bOutputAllWidgets = FALSE
		thisPlacement.bOutputEnums = FALSE
		EXIT
	ENDIF
	
	// Open file (depending on if we are outputting widgets or enums
	OPEN_DEBUG_FILE()
	
	IF thisPlacement.bOutputAllWidgets
		WRITE_PLACEMENT_FUNCTION_HEADER(thisPlacement)
		WRITE_TEXT_PLACEMENTS(thisPlacement.TextPlacement, thisPlacement.txtScreenName)
		WRITE_SPRITE_PLACEMENTS(thisPlacement.SpritePlacement, thisPlacement.txtScreenName)
		WRITE_RECT_PLACEMENTS(thisPlacement.RectPlacement, thisPlacement.txtScreenName)
		WRITE_PLACEMENT_FUNCTION_FOOTER()
	ELSE // We are writing enums
//		WRITE_PLACEMENT_ENUM_HEADER()
		WRITE_TEXT_PLACEMENTS_ENUM(thisPlacement.TextPlacement, thisPlacement.txtScreenName)
		WRITE_SPRITE_PLACEMENTS_ENUM(thisPlacement.SpritePlacement, thisPlacement.txtScreenName)
		WRITE_RECT_PLACEMENTS_ENUM(thisPlacement.RectPlacement, thisPlacement.txtScreenName)
//		WRITE_PLACEMENT_ENUM_FOOTER()
	ENDIF
	CLOSE_DEBUG_FILE()
	// Close file
	thisPlacement.bOutputAllWidgets = FALSE
	thisPlacement.bOutputEnums = FALSE
ENDPROC

//PROC EXPORT_SCREEN_SMALL_PLACEMENT(SMALL_PLACEMENT_TOOLS &thisPlacement)
//	IF NOT thisPlacement.bOutputAllWidgets AND NOT thisPlacement.bOutputEnums
//		thisPlacement.bOutputAllWidgets = FALSE
//		thisPlacement.bOutputEnums = FALSE
//		EXIT
//	ENDIF
//	
//	GET_ALL_PLACEMENT_STRINGS(thisPlacement)
//	
//	IF IS_STRING_EMPTY_HUD(thisPlacement.txtFileName) OR IS_STRING_EMPTY_HUD(thisPlacement.txtFilePath)
//		SCRIPT_ASSERT("Cannot export with an invalid file!")
//		thisPlacement.bOutputAllWidgets = FALSE
//		thisPlacement.bOutputEnums = FALSE
//		EXIT
//	ENDIF
//	
//	IF IS_STRING_EMPTY_HUD(thisPlacement.txtScreenName)
//		SCRIPT_ASSERT("Cannot export with an invalid screen name!")
//		thisPlacement.bOutputAllWidgets = FALSE
//		thisPlacement.bOutputEnums = FALSE
//		EXIT
//	ENDIF
//	
//	// Open file (depending on if we are outputting widgets or enums
//	OPEN_DEBUG_FILE()
//	
//	IF thisPlacement.bOutputAllWidgets
//		WRITE_SMALL_PLACEMENT_FUNCTION_HEADER(thisPlacement)
//		WRITE_TEXT_PLACEMENTS(thisPlacement.TextPlacement, thisPlacement.txtScreenName)
//		WRITE_SPRITE_PLACEMENTS(thisPlacement.SpritePlacement, thisPlacement.txtScreenName)
//		WRITE_RECT_PLACEMENTS(thisPlacement.RectPlacement, thisPlacement.txtScreenName)
//		WRITE_PLACEMENT_FUNCTION_FOOTER()
//	ELSE // We are writing enums
////		WRITE_PLACEMENT_ENUM_HEADER()
//		WRITE_TEXT_PLACEMENTS_ENUM(thisPlacement.TextPlacement, thisPlacement.txtScreenName)
//		WRITE_SPRITE_PLACEMENTS_ENUM(thisPlacement.SpritePlacement, thisPlacement.txtScreenName)
//		WRITE_RECT_PLACEMENTS_ENUM(thisPlacement.RectPlacement, thisPlacement.txtScreenName)
////		WRITE_PLACEMENT_ENUM_FOOTER()
//	ENDIF
//	CLOSE_DEBUG_FILE()
//	// Close file
//	thisPlacement.bOutputAllWidgets = FALSE
//	thisPlacement.bOutputEnums = FALSE
//ENDPROC
#ENDIF

