// ___________________________________________
//
//	Game: GTAV - CnC.
//  Script: DPAD DOWN INFO BOXES
//  Author: Kevin Wong
//	Description: Display boxes for gang members on mission, Drug purse.
// ___________________________________________ 



USING "screens_header.sch"
USING "net_events.sch"
USING "screen_tabs.sch"
USING "script_network.sch"

//USING "net_mission_details_overlay.sch"
USING "script_hud_enums.sch"

//USING "mp_stat_friend_list.sch"
USING "commands_hud.sch"
USING "Net_Entity_Controller.sch"
USING "net_entity_icons.sch"
//USING "net_hud_displays.sch"

STRUCT mission_display_struct


		
	RECT rectangle_header2
	RECT rectangle_large_array_gang_box[8]
	
	
	
	TEXT_STYLE text_styles_gang_box
	TEXT_STYLE text_styles_alpha
	TEXT_STYLE text_black_header
	TEXT_STYLE text_inventry_labels_style
	//#IF IS_DEBUG_BUILD

		TEXT_PLACEMENT slot_players_names_gang_box[8]
		//TEXT_PLACEMENT slot_players_icons[8] 
		TEXT_PLACEMENT text_number_team_members_active
		SPRITE_PLACEMENT sprite_tickbox[7]
		SPRITE_PLACEMENT sprite_inventory[7]
		SPRITE_PLACEMENT sprite_headset[7]
		SPRITE_PLACEMENT sprite_arrest_status[7]
		
	// stuff for the drug purse
		//SPRITE_PLACEMENT sprite_drug_purse[4]
		//TEXT_PLACEMENT text_drugbox[4]
		INT TABLE_POSITION 
		//INT CURRENT_TABLE_POSITION
		SPRITE_PLACEMENT sprite_list_inventory_slot[7]
		TEXT_PLACEMENT text_inventory_list[7]
	//	STRING string_texturename_inventory_slot[7]
	//	STRING string_textname_inventory_slot[7]
		//STRING spec_box_temp_mission_string[7]
		//SCALEFORM_INDEX scaleform_drug_purse[4]
	//	BOOL is_drug_hud_activated = FALSE
		//BOOL have_drug_hud_assets_loaded = FALSE
	//#ENDIF



		FLOAT  x_offset= -0.017
		FLOAT  y_offset = 0.291
		
		FLOAT x_offset_drug_purse
		FLOAT y_offset_drug_purse
		
		
	
	INT current_selected_column = 0
	INT index_number_player
	
	BOOL on_second_page = FALSE
	BOOL hide_spec_table = FALSE




	FLOAT NAME_POSITION_X_WITHOUTTICK =  0.0743
	FLOAT NAME_POSITION_X =  0.0943
	INT sprite_inventory_sprite_type[7]
	INT sprite_headset_type[7]
	INT sprite_arrest_status_type[7]

	INT number_active_players
	//onMissionInventHud.number_active_players = 3
	
	INT arrest_state_scaleform_inventory[NUM_NETWORK_PLAYERS]
	INT headset_state_scaleform_inventory[NUM_NETWORK_PLAYERS]
	INT number_of_participants_scaleform_inventory
	
	INT update_inventory_scaleform_quantities[7]
	//INT recorded_number_or_participants
ENDSTRUCT

PROC EMPTY_MY_OVERHEAD_INVENTORY()
	//If the player is in the GBD Array
	IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(PLAYER_ID())
		INT iGbdSlot = NATIVE_TO_INT(PLAYER_ID())
		//Empty the inventory broadcast data and stop the proc running. 
		GlobalplayerBD[iGbdSlot].iMyOverHeadInventory = 0
		GlobalplayerBD[iGbdSlot].bRunOverHeadinventory = FALSE
	ENDIF
ENDPROC



PROC CLEAR_INVENTORY_BOX_CONTENT()
//	PRINTSTRING(" \n clearing inventry box \n ")
	INT i
	FOR i =0 TO 6
		flag_display_UPDATE_INVENTORY_BOX_CONTENT[i] = 0
		
		spec_cam_quantities[i] = 0
		
						


		IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(PLAYER_ID())
			INT iGbdSlot = NATIVE_TO_INT(PLAYER_ID())
			//Clear the bit that says that this item is in the inventory
			CLEAR_BIT(GlobalplayerBD[iGbdSlot].iMyOverHeadInventory, ENUM_TO_INT(ARRAY_OF_INVENTORY_SPRITE_ENUMS[i]))
			//If there is nothing in the inventory then turn the bool off that makes the over head inventory run
			IF GlobalplayerBD[iGbdSlot].iMyOverHeadInventory = 0
				GlobalplayerBD[iGbdSlot].bRunOverHeadinventory = FALSE
			ENDIF
		ENDIF
		
	ENDFOR

	//is_inventory_hud_active = FALSE
	EMPTY_MY_OVERHEAD_INVENTORY()
		flag_display_UPDATE_INVENTORY_BOX_CONTENT[0] = 0
		flag_display_UPDATE_INVENTORY_BOX_CONTENT[1] = 0
		flag_display_UPDATE_INVENTORY_BOX_CONTENT[2] = 0
		flag_display_UPDATE_INVENTORY_BOX_CONTENT[3] = 0
		flag_display_UPDATE_INVENTORY_BOX_CONTENT[4] = 0
		flag_display_UPDATE_INVENTORY_BOX_CONTENT[5] = 0
		flag_display_UPDATE_INVENTORY_BOX_CONTENT[6] = 0
							
		is_inventory_hud_active = FALSE
ENDPROC




PROC RESET_INVENTORY_BOX_CONTENT()
//	PRINTSTRING(" \n clearing inventry box \n ")
	INT i
	FOR i =0 TO 6
		//onMissionInventHud.sprite_inventory_sprite_type[i] = 0
//		string_texturename_inventory_slot[i] = NULL
//		string_textname_inventory_slot[i] = NULL

		//SET_TEXT_WHITE(onMissionInventHud.text_inventory_list[i])
		flag_display_UPDATE_INVENTORY_BOX_CONTENT[i] = 0
		spec_cam_quantities[i] = 0
		//SET_TEXT_WHITE(onMissionInventHud.text_inventry_labels_style)
		//is_slot_on_display[i] = 0
		IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(PLAYER_ID())
			INT iGbdSlot = NATIVE_TO_INT(PLAYER_ID())
			//Clear the bit that says that this item is in the inventory
			CLEAR_BIT(GlobalplayerBD[iGbdSlot].iMyOverHeadInventory, ENUM_TO_INT(ARRAY_OF_INVENTORY_SPRITE_ENUMS[i]))
			//If there is nothing in the inventory then turn the bool off that makes the over head inventory run
			IF GlobalplayerBD[iGbdSlot].iMyOverHeadInventory = 0
				GlobalplayerBD[iGbdSlot].bRunOverHeadinventory = FALSE
			ENDIF
		ENDIF
	ENDFOR
	
		flag_display_UPDATE_INVENTORY_BOX_CONTENT[0] = 0
		flag_display_UPDATE_INVENTORY_BOX_CONTENT[1] = 0
		flag_display_UPDATE_INVENTORY_BOX_CONTENT[2] = 0
		flag_display_UPDATE_INVENTORY_BOX_CONTENT[3] = 0
		flag_display_UPDATE_INVENTORY_BOX_CONTENT[4] = 0
		flag_display_UPDATE_INVENTORY_BOX_CONTENT[5] = 0
		flag_display_UPDATE_INVENTORY_BOX_CONTENT[6] = 0
							
		is_inventory_hud_active = FALSE
	EMPTY_MY_OVERHEAD_INVENTORY()
	
ENDPROC

//INFO: 
//PARAM NOTES:
//PURPOSE: Gets a free inventory slot for the command UPDATE_INVENTORY_BOX_CONTENT
FUNC INT GET_INVENTORY_FREE_SLOT() 
	INT i
	FOR i =1 TO 6
		IF flag_display_UPDATE_INVENTORY_BOX_CONTENT[i] = 0
		//Empty the inventory broadcast data and stop the proc running. 
		AND GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iMyOverHeadInventory = 0
		AND GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bRunOverHeadinventory = FALSE
			RETURN i
		ENDIF
	ENDFOR
	

	RETURN 3
	
ENDFUNC



FUNC BOOL CAN_LOCAL_DPADDOWN_DISPLAY_player_On_Screen_hud()

	#IF IS_DEBUG_BUILD
	SCRIPT_TIMER iDpadDownPrintTimer

	#ENDIF

	IF IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
		#IF IS_DEBUG_BUILD
//			IF GET_THE_NETWORK_TIMER() - iDpadDownPrintTimer > 5000
			IF HAS_NET_TIMER_EXPIRED_ONE_FRAME(iDpadDownPrintTimer, 5000)
				NET_NL()NET_PRINT("CAN_LOCAL_DPADDOWN_DISPLAY: IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD) = FALSE ")
//				iDpadDownPrintTimer = GET_THE_NETWORK_TIMER()
			ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	RETURN TRUE
	
ENDFUNC

#IF IS_DEBUG_BUILD
	PROC CREATE_GANG_MEMBER_WIDGETS(mission_display_struct& onMissionInventHud)
		START_WIDGET_GROUP("Spec Cam")  
			// Gameplay debug, sever only.
			START_WIDGET_GROUP("SCALEFORM Gang Mission Box") 
				ADD_WIDGET_FLOAT_SLIDER("X Value", onMissionInventHud.rectangle_header2.x,0.0, 1.0, 0.01)		
				ADD_WIDGET_FLOAT_SLIDER("Y Value", onMissionInventHud.rectangle_header2.y,0.0, 1.0, 0.01)		
			STOP_WIDGET_GROUP()				
		STOP_WIDGET_GROUP()
	ENDPROC		
#ENDIF

//INFO: 
//PARAM NOTES:
//PURPOSE: Checks if there is a free slot in inventory
FUNC BOOL IS_INVENTORY_SLOTS_FREE() 
	INT i
	FOR i =1 TO 6
		IF flag_display_UPDATE_INVENTORY_BOX_CONTENT[i] = 0
		//Empty the inventory broadcast data and stop the proc running. 
		//AND GlobalplayerBD_CNC[NATIVE_TO_INT(PLAYER_ID())].iMyOverHeadInventory = 0
		//AND GlobalplayerBD_CNC[NATIVE_TO_INT(PLAYER_ID())].bRunOverHeadinventory = FALSE
			RETURN TRUE
		ENDIF
	ENDFOR
	

	RETURN FALSE
	
ENDFUNC

PROC gang_hud_screencleanup()
	TERMINATE_THIS_THREAD()
ENDPROC
PROC GET_INFO_FOR_ENUM(eMP_TAG InventorySprite, TEXT_LABEL_31 &return_texture_name, TEXT_LABEL_31 &return_text_name, FLOAT &x_offset, inventory_ped_name_following_player &name_of_ped, /*MP_inventory_vehicle_type &invent_vehicle_type,*/  eSCRIPT_HUD_COMPONENT &return_inventory_scaleform_enumtype )
	// draw local inventry box
	
	switch InventorySprite
	
		//case MP_SpecItem_BoatPickup
//		CASE MP_TAG_INV_VEHICLE
//			return_texture_name = "MP_SpecItem_BoatPickup"
//			return_text_name = "MP_INVEN_5"
//			x_offset = 0.873 -0.017
//			
//		break
			
		//case MP_SpecItem_Cash
//		CASE MP_TAG_CASH_FROM_BANK
//			return_texture_name = "MP_SpecItem_Cash"
//			return_text_name = "MP_INVEN_10"
//			x_offset = 0.842 -0.017
//			return_inventory_scaleform_enumtype = HUD_MP_TAG_CASH_FROM_BANK
//		break
			
		//case MP_SpecItem_Package
		CASE MP_TAG_PACKAGES
			return_texture_name = "MP_SpecItem_Package"
			return_text_name = "MP_INVEN_11"
			x_offset = 0.878 -0.017
			return_inventory_scaleform_enumtype = HUD_MP_TAG_PACKAGES
			IF return_inventory_scaleform_enumtype = HUD_MP_TAG_PACKAGES
			ENDIF
		BREAK
			
			
		CASE MP_TAG_PACKAGE_LARGE
			return_texture_name = "MP_SpecItem_Package"
			return_text_name = "MP_INVEN_11"
			x_offset = 0.878 -0.017
			return_inventory_scaleform_enumtype = HUD_MP_TAG_LARGE_PACKAGES
			IF return_inventory_scaleform_enumtype = HUD_MP_TAG_LARGE_PACKAGES
			ENDIF
		BREAK
		
		CASE MP_TAG_GANG_CEO
			return_texture_name = "MP_SpecItem_Package"
			return_text_name = "MP_INVEN_11"
			x_offset = 0.878 -0.017
			return_inventory_scaleform_enumtype = HUD_MP_TAG_GANG_CEO
			IF return_inventory_scaleform_enumtype = HUD_MP_TAG_GANG_CEO
			ENDIF
			
		BREAK
		
		CASE MP_TAG_GANG_BIKER
				return_texture_name = "MP_SpecItem_Package"
			return_text_name = "MP_INVEN_11"
			x_offset = 0.878 -0.017
			return_inventory_scaleform_enumtype = HUD_MP_TAG_GANG_BIKER
			IF return_inventory_scaleform_enumtype = HUD_MP_TAG_GANG_BIKER
			ENDIF
		BREAK	
			
		//case MP_SpecItem_CuffKeys
//		CASE MP_TAG_CUFF_KEYS
//			return_texture_name = "MP_SpecItem_CuffKeys"
//			return_text_name = "MP_INVEN_7"	
//			x_offset = 0.874 -0.017
//			return_inventory_scaleform_enumtype = HUD_MP_TAG_CUFF_KEYS
//		break
			

		//case MP_SpecItem_Data
//		CASE MP_TAG_DOWNLOAD_DATA
//			return_texture_name = "MP_SpecItem_Data"
//			return_text_name = "MP_INVEN_2"
//			x_offset = 0.876 -0.017
//			return_inventory_scaleform_enumtype = HUD_MP_TAG_DOWNLOAD_DATA
//		break

		CASE MP_TAG_IF_PED_FOLLOWING // new enum for specific ped names
		//case MP_SpecItem_Ped
			return_texture_name = "MP_SpecItem_Ped"
			
			x_offset = 0.872 -0.017
			return_inventory_scaleform_enumtype = HUD_MP_TAG_IF_PED_FOLLOWING
			SWITCH name_of_ped
				CASE MP_INVENTORY_CHAR_NAME_NONE // no name
					return_text_name = "MP_INVEN_13"
				BREAK
		
				CASE	MP_INVENTORY_CHAR_NAME_HOOKER_GENERIC
					return_text_name = "MP_INVEN_17"
				BREAK
				
				CASE	MP_INVENTORY_CHAR_NAME_EDGAR
					return_text_name = "MP_INVEN_18"
				BREAK
				
				CASE	MP_INVENTORY_CHAR_NAME_RUBEN
					return_text_name = "MP_INVEN_19"
				BREAK
				
				CASE	MP_INVENTORY_CHAR_NAME_RAY
					return_text_name = "MP_INVEN_20"
				BREAK
				
				CASE	MP_INVENTORY_CHAR_NAME_ROBERTO
					return_text_name = "MP_INVEN_21"
				BREAK
				
				CASE	MP_INVENTORY_CHAR_NAME_HOSTAGE
					return_text_name = "MP_INVEN_22"
				BREAK
				
				CASE	MP_INVENTORY_CHAR_NAME_RAFAEL
					return_text_name = "MP_INVEN_23"
				BREAK
				
				CASE	MP_INVENTORY_CHAR_NAME_BOSS
					return_text_name = "MP_INVEN_27"
				BREAK
				
				CASE MP_INVENTORY_CHAR_NAME_VIP
					return_text_name = "MP_INVEN_15"
				BREAK
				
				CASE MP_INVENTORY_CHAR_NAME_ERNIE
					return_text_name = "MP_INVEN_24"
				BREAK
				
				CASE MP_INVENTORY_CHAR_NAME_INFORMANT
					return_text_name = "MP_INVEN_25"
				BREAK
				
				CASE MP_INVENTORY_CHAR_NAME_JULIET
					return_text_name = "MP_INVEN_26"
				BREAK
				
				CASE MP_INVENTORY_CHAR_NAME_WITNESS
					return_text_name = "MP_INVEN_28"
				BREAK
				
			ENDSWITCH
		BREAK
		
		// NEW  BALL
	/*	CASE MP_TAG_BALL // new enum for specific ped names
			return_texture_name = "MP_SpecItem_Ped"  // needs to be updated
			return_text_name = "MP_INVEN_11"
			x_offset = 0.878 -0.017
			return_inventory_scaleform_enumtype = HUD_MP_BALL
			IF return_inventory_scaleform_enumtype = HUD_MP_BALL
			ENDIF
		BREAK*/
		
		//case MP_SpecItem_KeyCard
//		CASE MP_TAG_KEY_CARD
//			return_texture_name = "MP_SpecItem_KeyCard"
//			return_text_name = "MP_INVEN_1"
//			x_offset = 0.877 -0.017
//			return_inventory_scaleform_enumtype = HUD_MP_TAG_KEY_CARD
//		break
			
			
	//	case MP_SpecItem_RandomObject
//		CASE MP_TAG_RANDOM_OBJECT
//			return_texture_name = "MP_SpecItem_RandomObject"
//			return_text_name = "MP_INVEN_12"
//			x_offset = 0.849 -0.017
//			return_inventory_scaleform_enumtype = HUD_MP_TAG_RANDOM_OBJECT
//		break
		
		//case MP_SpecItem_Remote
//		case MP_TAG_REMOTE_CONTROL
//			return_texture_name = "MP_SpecItem_Remote"
//			return_text_name = "MP_INVEN_9"
//			x_offset = 0.867 -0.017
//			return_inventory_scaleform_enumtype = HUD_MP_TAG_REMOTE_CONTROL
//		break
		
		//case MP_SpecItem_Safe
//		case MP_TAG_CASH_FROM_SAFE
//			//return_texture_name = "MP_SpecItem_Safe"
//			return_texture_name = "MP_SpecItem_Cash"
//			return_text_name = "MP_INVEN_4"
//			x_offset = 0.873 -0.017
//			return_inventory_scaleform_enumtype = HUD_MP_TAG_CASH_FROM_SAFE
//		break
		
//		case MP_TAG_WEAPONS_PACKAGE
//			return_texture_name = "MP_SpecItem_Weapons"
//			return_text_name = "MP_INVEN_8"
//			x_offset = 0.831 -0.017
//			return_inventory_scaleform_enumtype = HUD_MP_TAG_WEAPONS_PACKAGE
//		break

//		case MP_TAG_KEYS
//			return_inventory_scaleform_enumtype = HUD_MP_TAG_KEYS
//		break
		
//		case MP_TAG_INV_VEHICLE
//			return_inventory_scaleform_enumtype = HUD_MP_VEHICLE
//			SWITCH invent_vehicle_type
//				case MP_inventory_vehicle_none
//					return_texture_name = "MP_SpecItem_Car"
//					return_text_name = "MP_INVEN_14"
//					x_offset = 0.878 -0.017
//					return_inventory_scaleform_enumtype = HUD_MP_VEHICLE
//				break
//				case MP_inventory_vehicle_plane
//					return_texture_name = "MP_SpecItem_Plane"
//					return_text_name = "MP_INVEN_14"
//					x_offset = 0.878 -0.017
//					return_inventory_scaleform_enumtype = HUD_MP_VEHICLE_PLANE
//				break
//				
//				case MP_inventory_vehicle_heli
//					return_texture_name = "MP_SpecItem_Heli"
//					return_text_name = "MP_INVEN_14"
//					x_offset = 0.878 -0.017
//					return_inventory_scaleform_enumtype = HUD_MP_VEHICLE_HELI
//				break
//				case MP_inventory_vehicle_car
//					return_texture_name = "MP_SpecItem_Car"
//					return_text_name = "MP_INVEN_14"
//					x_offset = 0.878 -0.017
//					return_inventory_scaleform_enumtype = HUD_MP_VEHICLE
//				break
//			ENDSWITCH
//		break
		
		

		/*
//		case MP_TAG_PLANE
//			return_texture_name = "MP_SpecItem_Plane"
//			return_text_name = "MP_INVEN_14"
//			x_offset = 0.878 -0.017
//		break
//		
//		case MP_TAG_HELI
//			return_texture_name = "MP_SpecItem_Heli"
//			return_text_name = "MP_INVEN_14"
//			x_offset = 0.878 -0.017
//		break
//		case MP_TAG_CAR
//			return_texture_name = "MP_SpecItem_Car"
//			return_text_name = "MP_INVEN_14"
//			x_offset = 0.878 -0.017
//		break
		*/
		

	ENDSWITCH
	
ENDPROC

PROC CLEAR_A_INVENTORY_BOX_CONTENT(INT column_number)
	//For the overhead inventory
	//If the player is in the GBD Array
	IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(PLAYER_ID())
		INT iGbdSlot = NATIVE_TO_INT(PLAYER_ID())
		//Clear the bit that says that this item is in the inventory
		CLEAR_BIT(GlobalplayerBD[iGbdSlot].iMyOverHeadInventory, ENUM_TO_INT(ARRAY_OF_INVENTORY_SPRITE_ENUMS[column_number]))
		//CLEAR_BIT(GlobalplayerBD[iGbdSlot].iMyOverHeadInventory, ENUM_TO_INT(MP_TAG_BALL))
		CLEAR_BIT(GlobalplayerBD[iGbdSlot].iMyOverHeadInventory, ENUM_TO_INT(MP_TAG_PACKAGE_LARGE))
		CLEAR_BIT(GlobalplayerBD[iGbdSlot].iMyOverHeadInventory, ENUM_TO_INT(MP_TAG_TRACKIFY))
		CLEAR_BIT(GlobalplayerBD[iGbdSlot].iMyOverHeadInventory, ENUM_TO_INT(MP_TAG_GANG_CEO  ))
		CLEAR_BIT(GlobalplayerBD[iGbdSlot].iMyOverHeadInventory, ENUM_TO_INT(MP_TAG_GANG_BIKER))
		//If there is nothing in the inventory then turn the bool off that makes the over head inventory run
		IF GlobalplayerBD[iGbdSlot].iMyOverHeadInventory = 0
			GlobalplayerBD[iGbdSlot].bRunOverHeadinventory = FALSE
		ENDIF
	ENDIF
	flag_display_UPDATE_INVENTORY_BOX_CONTENT[column_number] = 0
	spec_cam_quantities[column_number] = 0
	
	#IF IS_DEBUG_BUILD
		PRINTLN("INSIDE CLEAR_A_INVENTORY_BOX_CONTENT FUNCTION Removing scaleform number: ", column_number)
	#ENDIF
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bdisplayinventoverplayershead = FALSE
	//is_slot_on_display[column_number] = 0
	
ENDPROC




//INFO: 
//PARAM NOTES: Int = The selected column number (1 - 7), eMP_TAG  = The type of srpite you wish to display, BOOL = Highlight the icon if the item has been achieved, INT The quantities. Usually set to 1. inventory_ped_name_following_player= Name of the ped you are escorting (optional)
//PURPOSE: Gets a free inventory slot for the command UPDATE_INVENTORY_BOX_CONTENT

PROC UPDATE_INVENTORY_BOX_CONTENT(INT column_number, eMP_TAG invent_sptnames, BOOL achieved_objective, INT quantities = 1, inventory_ped_name_following_player name_of_ped = MP_INVENTORY_CHAR_NAME_NONE, MP_inventory_vehicle_type vehicle_type = MP_inventory_vehicle_car, BOOL bdisplay_local_player = FALSE, BOOL buse_ball_icon = FALSE, BOOL bdisplay_arrow_to_gang_only = FALSE)
		NET_NL()NET_PRINT("++++++++++++++++++++++++ ")
		NET_NL()NET_PRINT("Drawing Inventory Box ")
		NET_NL()NET_PRINT("++++++++++++++++++++++++ ")
		PRINTLN("UPDATE_INVENTORY_BOX_CONTENT column_number = ", column_number)
		
		ARRAY_OF_INVENTORY_SPRITE_ENUMS[column_number] = invent_sptnames
		ARRAY_OF_INVENTORY_CHARACTER_NAMES[column_number] = name_of_ped
		ARRAY_OF_VEHICLE_TYPE[column_number] = vehicle_type
		
		
		IF bdisplay_local_player
			GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bdisplayinventoverplayershead = TRUE
			NET_NL()NET_PRINT(" UPDATE_INVENTORY_BOX_CONTENT bdisplay_local_player = TRUE ")
		ELSE
			GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bdisplayinventoverplayershead = FALSE
			NET_NL()NET_PRINT(" UPDATE_INVENTORY_BOX_CONTENT bdisplay_local_player = FALSE ")
		ENDIF
		
		IF buse_ball_icon
			//invent_sptnames = MP_TAG_BALL
		//	PRINTLN("Player is using the ball icon.")
		ENDIF
		
		//For the overhead inventory
		//If the player is in the gbd array
		IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(PLAYER_ID())
			INT iGbdSlot = NATIVE_TO_INT(PLAYER_ID())
			//Set the item in the inventory
			SET_BIT(GlobalplayerBD[iGbdSlot].iMyOverHeadInventory, ENUM_TO_INT(invent_sptnames))
			//Make the over head stuff run
			GlobalplayerBD[iGbdSlot].bRunOverHeadinventory = TRUE
		ENDIF

		// set colour	
		IF achieved_objective = TRUE
			spec_cam_inventory_achieved[column_number] = TRUE
		ELSE
			spec_cam_inventory_achieved[column_number] = FALSE
		ENDIF

		flag_display_UPDATE_INVENTORY_BOX_CONTENT[column_number] = 1
		
		
		// JA: Draws pickup name and animates upwards like xp/ammo
		//IF spec_cam_quantities[column_number] != quantities
		//	SET_PICKUP_ANIM_FOR_ENTITY(invent_sptnames)		//THIS DIDN'T DO ANYTHING
		//ENDIF
		
		spec_cam_quantities[column_number] = quantities
			
		// displays only for players team
		IF bdisplay_arrow_to_gang_only
			IF invent_sptnames = MP_TAG_ARROW 
				GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bdisplayarrowoverplayersheadteamonly = TRUE
			ENDIF
		ENDIF
		
		
		//Check if item is not already displayed
		#IF IS_DEBUG_BUILD
			int column_check_index
			
			FOR column_check_index =1 TO 6
				IF flag_display_UPDATE_INVENTORY_BOX_CONTENT[column_check_index] = 1 // is another column is on
				AND ARRAY_OF_INVENTORY_SPRITE_ENUMS[column_check_index] = invent_sptnames // is another column is the same asthe inventory about to switch on
				AND column_check_index != column_number
					SCRIPT_ASSERT("This inventory item is already displayed. Do not display the same item type twice in different columns -Speak to Kevin")
				ENDIF
			ENDFOR
		#ENDIF
	
ENDPROC

PROC UPDATE_MISSION_GANG_BOX_CONTENT(PLAYER_INDEX PLAYERIindex, mission_gang_box_sprite_types invent_sptnames, mission_display_struct& onMissionInventHud)

	INT local_player_index
	INT i
	
	PLAYER_INDEX playerindexes[NUM_NETWORK_PLAYERS]
	//Get_Player_Index_on_mission(g_sMDOverlayMP.mdoMissionData.mission, g_sMDOverlayMP.mdoMissionData.iInstanceId, playerindexes)
	// get player id
	FOR i = 0 TO 5
	 	IF playerindexes[i]= PLAYERIindex				
		//entered player = i // record id 
			local_player_index = NATIVE_TO_INT(playerindexes[i])
		ENDIF
			
	ENDFOR

		
	// draw gang member box
	IF invent_sptnames = MP_GETAWAYDRIVER
		onMissionInventHud.sprite_inventory_sprite_type[local_player_index] = 1//"GETAWAYDRIVER"
	ELIF invent_sptnames = MP_COMPUTERHACKER
		onMissionInventHud.sprite_inventory_sprite_type[local_player_index] = 2//"MP_SpecItem_Cash"
	ELIF invent_sptnames = MP_SNIPER
		onMissionInventHud.sprite_inventory_sprite_type[local_player_index]= 3// "MP_SpecItem_Package"
	ELIF invent_sptnames =  MP_LEADER 
		onMissionInventHud.sprite_inventory_sprite_type[local_player_index]= 4// "MP_SpecItem_CuffKeys"
	ELIF invent_sptnames = MP_PARACHUTE
		onMissionInventHud.sprite_inventory_sprite_type[local_player_index] = 5//  "MP_SpecItem_Data"
	ELIF invent_sptnames = MP_NO_INVENTORY
		onMissionInventHud.sprite_inventory_sprite_type[local_player_index] = 6 // "MP_SpecItem_Ped"
	ENDIF
		
	//SPRITE_PLACEMENT[local_player_index].bDrawTextNameOnscreen = TRUE
		
	//DRAW_2D_SPRITE("MPInventory", string_texturename_inventory_slot, onMissionInventHud.sprite_list_inventory_slot[column_number], FALSE ) // boat cash
	//must record player_name
	//assign inventory icon to player array number
	//position in table
	// record if sprite is achieved 
	
ENDPROC





FUNC INT GET_CURRENT_SPECT_CAM_PLAYER_HIGHLIGHTED_GANG_ON_MISSION_BOX(mission_display_struct& onMissionInventHud)
	INT currently_highlighted_player
	IF onMissionInventHud.on_second_page = FALSE
		currently_highlighted_player = onMissionInventHud.current_selected_column
	ELSE
		onMissionInventHud.current_selected_column += 15
		
	ENDIF
	
	RETURN currently_highlighted_player
	
ENDFUNC

FUNC INT GetNoOfDecimalPoints(FLOAT fFloat, INT iMinNoOfPlaces=0)

      FLOAT fNumber
      FLOAT fNewNum
      INT i
      INT iReturn
	  
      IF fFloat > 0.0
	      fNumber = fFloat - TO_FLOAT(FLOOR(fFloat))
	 
	      REPEAT 3 i
	            fNewNum = POW(10.0, TO_FLOAT(i)) * fNumber
	            IF NOT (FLOOR(fNewNum) = 0)
	                  iReturn = i       
	            ENDIF
	            fNewNum -= TO_FLOAT(FLOOR(fNewNum))
	            fNumber = fNewNum / POW(10.0, TO_FLOAT(i))
	      ENDREPEAT

	      IF (iReturn >= iMinNoOfPlaces)
	            RETURN(iReturn)
	      ENDIF
	      
	      RETURN(iMinNoOfPlaces)
		  
		
		ENDIF
	 RETURN(0)
ENDFUNC 






#IF IS_DEBUG_BUILD
	PROC MAINTAIN_INVENTORY_WIDGET_FUNCTION()

			IF WIDGET_INVENTORY_ON_OFF
			AND flag_display_UPDATE_INVENTORY_BOX_CONTENT[number_of_inventory_columns] = 0
					//ARRAY_OF_INVENTORY_SPRITE_ENUMS[number_of_inventory_columns] = INT_TO_ENUM(eMP_TAG, Inventory_type_widget)
					//flag_display_UPDATE_INVENTORY_BOX_CONTENT[number_of_inventory_columns] = 1
				
				//UPDATE_INVENTORY_BOX_CONTENT(number_of_inventory_columns, INT_TO_ENUM(eMP_TAG, Inventory_type_widget), TRUE)
				//UPDATE_INVENTORY_BOX_CONTENT(number_of_inventory_columns, MP_TAG_PACKAGES, TRUE)
				CLEAR_INVENTORY_BOX_CONTENT()
//				IF Inventory_type_widget = 0 
//					WIDGET_INVENTORY_ITEM = MP_TAG_KEYS
//				ELIF Inventory_type_widget = 1
//					WIDGET_INVENTORY_ITEM = MP_TAG_CUFFS
//				ELIF Inventory_type_widget = 2
//					WIDGET_INVENTORY_ITEM = MP_TAG_KEY_CARD
//				ELIF Inventory_type_widget = 3
//					WIDGET_INVENTORY_ITEM = MP_TAG_DOWNLOAD_DATA
//				ELIF Inventory_type_widget = 4
//					WIDGET_INVENTORY_ITEM = MP_TAG_CASH_FROM_SAFE
//				ELIF Inventory_type_widget = 5
//					WIDGET_INVENTORY_ITEM = MP_TAG_CASH_FROM_BANK
//				ELIF Inventory_type_widget = 6
//					WIDGET_INVENTORY_ITEM = MP_TAG_INV_VEHICLE
				IF Inventory_type_widget = 1
					WIDGET_INVENTORY_ITEM = MP_TAG_PACKAGES
//				ELIF Inventory_type_widget = 5
//					WIDGET_INVENTORY_ITEM = MP_TAG_REMOTE_CONTROL
//				ELIF Inventory_type_widget = 6
//					WIDGET_INVENTORY_ITEM = MP_TAG_WEAPONS_PACKAGE
//				ELIF Inventory_type_widget = 7
//					WIDGET_INVENTORY_ITEM = MP_TAG_RANDOM_OBJECT
				ELIF Inventory_type_widget = 2
					WIDGET_INVENTORY_ITEM = MP_TAG_IF_PED_FOLLOWING
				ELIF Inventory_type_widget = 3
					WIDGET_INVENTORY_ITEM = MP_TAG_PACKAGE_LARGE
//				ELIF Inventory_type_widget = 9
//					WIDGET_INVENTORY_ITEM = MP_TAG_CUFF_KEYS
				ENDIF
				
				
				UPDATE_INVENTORY_BOX_CONTENT(number_of_inventory_columns, WIDGET_INVENTORY_ITEM, TRUE)
				PRINTLN(" Drawing inventory column: ",number_of_inventory_columns," Item: ",  INT_TO_ENUM(eMP_TAG, Inventory_type_widget))
				WIDGET_INVENTORY_ON_OFF = FALSE
			ENDIF
				
			IF WIDGET_INVENTORY_ON_OFF
			AND flag_display_UPDATE_INVENTORY_BOX_CONTENT[number_of_inventory_columns] = 1
				CLEAR_A_INVENTORY_BOX_CONTENT(number_of_inventory_columns)
				WIDGET_INVENTORY_ON_OFF = FALSE
			ENDIF
			
			
			IF RESET_WIDGET_INVENTORY
				CLEAR_INVENTORY_BOX_CONTENT()
				RESET_WIDGET_INVENTORY = FALSE
			ENDIF
	ENDPROC
#ENDIF	



PROC RENDER_INVENTORY_HUD( mission_display_struct& onMissionInventHud)
	#IF IS_DEBUG_BUILD
		MAINTAIN_INVENTORY_WIDGET_FUNCTION()
	#ENDIF			


	
		INT i 
		//INT team_name_player
		

		FOR i =1 TO 6
			TEXT_LABEL_31 texture_name_for_this_slot = ""
			TEXT_LABEL_31 text_name_for_this_slot = ""
			FLOAT x_offset_for_this_slot = 0.0
			//TEXT_LABEL_31 text_label_for_inventry_quantities_to_build = ""
	
			IF flag_display_UPDATE_INVENTORY_BOX_CONTENT[i] = 1
			AND NOT IS_CUTSCENE_ACTIVE()
			AND NOT IS_CUTSCENE_PLAYING()
			AND NOT IS_SCRIPT_HUD_DISABLED(HUDPART_ALL_OVERHEADS)
			AND NOT IS_SKYSWOOP_MOVING()
			AND NOT IS_SCRIPT_HUD_DISABLED(HUDPART_INVENTORY )
				DISPLAYING_SCRIPT_HUD(HUDPART_INVENTORY, TRUE)

					/*                           
						BEGIN_SCALEFORM_MOVIE_METHOD(ScaleformIndex, "SET_TEXT")
                                  BEGIN_TEXT_COMMAND_SCALEFORM_STRING(DisplayStruct.sMainStringSlot)
                                         ADD_TEXT_COMPONENT_INTEGER(DisplayStruct.TopNumber)
                                  END_TEXT_COMMAND_SCALEFORM_STRING()
                           END_SCALEFORM_MOVIE_METHOD()

*/
				
				IF 	is_slot_on_display[i] = 0
				
					GET_INFO_FOR_ENUM(ARRAY_OF_INVENTORY_SPRITE_ENUMS[i], texture_name_for_this_slot, text_name_for_this_slot, x_offset_for_this_slot, ARRAY_OF_INVENTORY_CHARACTER_NAMES[i], /*ARRAY_OF_VEHICLE_TYPE[i],*/ scaleform_inventory[i] )
					#IF IS_DEBUG_BUILD
						NET_PRINT("\n COLUMN: " ) NET_PRINT_INT(i)
						NET_PRINT("\n ARRAY_OF_INVENTORY_SPRITE_ENUMS: " ) NET_PRINT_INT(ENUM_TO_INT(ARRAY_OF_INVENTORY_SPRITE_ENUMS[i]))
						NET_PRINT("\n ARRAY_OF_VEHICLE_TYPE: " ) NET_PRINT_INT(ENUM_TO_INT(ARRAY_OF_VEHICLE_TYPE[i]))
						NET_PRINT("\n ARRAY_OF_VEHICLE_TYPE: " ) NET_PRINT_STRINGS(texture_name_for_this_slot, text_name_for_this_slot)
					
						NET_PRINT("\n ARRAY_OF_VEHICLE_TYPE SCALEFORM: " )NET_PRINT_INT(ENUM_TO_INT(scaleform_inventory[i]))
					#ENDIF
					onMissionInventHud.update_inventory_scaleform_quantities[i] = spec_cam_quantities[i] // recording initial quantities
					
					/*IF onMissionInventHud.update_inventory_scaleform_quantities[i] != spec_cam_quantities[i]
					AND is_inventory_hud_active = TRUE
					AND is_slot_on_display[i] = 1
						is_slot_on_display[i] = 0
						OLD_scaleform_inventory[i] = scaleform_inventory[i]
					ENDIF
				*/
					//IF OLD_scaleform_inventory[i] != scaleform_inventory[i]
					
						IF HAS_SCALEFORM_SCRIPT_HUD_MOVIE_LOADED(scaleform_inventory[i])	
						
							SET_SCRIPT_GFX_ALIGN(UI_ALIGN_RIGHT, UI_ALIGN_TOP)
							//SET_SCRIPT_GFX_ALIGN_PARAMS(DisplayStruct.AlignX, DisplayStruct.AlignY, DisplayStruct.SizeX, DisplayStruct.SizeY)
						
							BEGIN_SCALEFORM_SCRIPT_HUD_MOVIE_METHOD(scaleform_inventory[i], "SHOW_MP_INVENTORY_ITEM_WITH_TEXT")	
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(scaleform_inventory[i])) //This is the icon
								IF spec_cam_quantities[i] > 1
									text_name_for_this_slot += "S"
									BEGIN_TEXT_COMMAND_SCALEFORM_STRING(text_name_for_this_slot)
		                            	ADD_TEXT_COMPONENT_INTEGER(spec_cam_quantities[i])
		                            END_TEXT_COMMAND_SCALEFORM_STRING()
								ELSE
									SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(text_name_for_this_slot) //This is the string associated to the icon if you need one
								ENDIF
							END_SCALEFORM_MOVIE_METHOD()
							#IF IS_DEBUG_BUILD
								PRINTLN("Drawing scaleform inventory")
							#ENDIF
							is_inventory_hud_active = TRUE
							is_slot_on_display[i] = 1
						//	OLD_scaleform_inventory[i] = scaleform_inventory[i]
							#IF IS_DEBUG_BUILD
								PRINTLN("is_slot_on_display[",i,"]", is_slot_on_display[i])
							#ENDIF
							RESET_SCRIPT_GFX_ALIGN()
						ELSE
							REQUEST_SCALEFORM_SCRIPT_HUD_MOVIE(scaleform_inventory[i])
							
						ENDIF	
					//ELSE
					//	flag_display_UPDATE_INVENTORY_BOX_CONTENT[i] = 0
				//	ENDIF
				ENDIF
					
					
				// update if something changes
				IF onMissionInventHud.update_inventory_scaleform_quantities[i] != spec_cam_quantities[i]
				AND is_slot_on_display[i] = 1
					is_slot_on_display[i] = 0
				ENDIF
				

					
			ELSE
						
				
					
					//IF flag_display_UPDATE_INVENTORY_BOX_CONTENT[i] = 0
						IF is_slot_on_display[i] = 1
						//OR flag_display_UPDATE_INVENTORY_BOX_CONTENT[i] = 0
							IF HAS_SCALEFORM_SCRIPT_HUD_MOVIE_LOADED(scaleform_inventory[i])
								//BEGIN_SCALEFORM_SCRIPT_HUD_MOVIE_METHOD(scaleform_inventory[i], "REMOVE_MP_INVENTORY_ITEM")
								//END_SCALEFORM_MOVIE_METHOD()
								//REMOVE_SCALEFORM_SCRIPT_HUD_MOVIE(ARRAY_OF_INVENTORY_SPRITE_ENUMS[i])
								REMOVE_SCALEFORM_SCRIPT_HUD_MOVIE(scaleform_inventory[i])
								#IF IS_DEBUG_BUILD
									PRINTLN("INSIDE MAIN RENDER LOOP Removing scaleform inventory:", i)
								#ENDIF
								//OLD_scaleform_inventory[i] = HUD_DRUGS_PURSE_01
								#IF IS_DEBUG_BUILD
									PRINTLN("is_slot_on_display[",i,"]", is_slot_on_display[i])
								#ENDIF
								is_slot_on_display[i]  = 0
							ENDIF
							
						ENDIF
					//ENDIF

						//is_inventory_hud_active = FALSE
					
						IF is_inventory_hud_active = TRUE
							IF flag_display_UPDATE_INVENTORY_BOX_CONTENT[0] = 0
							AND flag_display_UPDATE_INVENTORY_BOX_CONTENT[1] = 0
							AND flag_display_UPDATE_INVENTORY_BOX_CONTENT[2] = 0
							AND flag_display_UPDATE_INVENTORY_BOX_CONTENT[3] = 0
							AND flag_display_UPDATE_INVENTORY_BOX_CONTENT[4] = 0
							AND flag_display_UPDATE_INVENTORY_BOX_CONTENT[5] = 0
							AND flag_display_UPDATE_INVENTORY_BOX_CONTENT[6] = 0
							
								is_inventory_hud_active = FALSE
								//is_slot_on_display[0] = 0
								//is_slot_on_display[1] = 0
								//is_slot_on_display[2] = 0
								//is_slot_on_display[3] = 0
								//is_slot_on_display[4] = 0
								//is_slot_on_display[5] = 0
								//is_slot_on_display[6] = 0
									#IF IS_DEBUG_BUILD
										PRINTLN("Setting is_inventory_hud_active back to false")
									#ENDIF
								
								
							ENDIF
						ENDIF
					
			ENDIF
		ENDFOR
		

	//ENDIF
ENDPROC

PROC RENDER_DRUG_HUD()

		IF NOT IS_SCRIPT_HUD_DISABLED(HUDPART_DRUG_HUD)
		OR  spec_hud_display_drug_hud = TRUE
			DISPLAYING_SCRIPT_HUD(HUDPART_DRUG_HUD, TRUE)
		
			
			IF is_drug_hud_activated = FALSE
			//	println("drug render 6)")
				IF HAS_SCALEFORM_SCRIPT_HUD_MOVIE_LOADED(HUD_DRUGS_PURSE_01)
				AND HAS_SCALEFORM_SCRIPT_HUD_MOVIE_LOADED(HUD_DRUGS_PURSE_02)
				AND HAS_SCALEFORM_SCRIPT_HUD_MOVIE_LOADED(HUD_DRUGS_PURSE_03)
				AND HAS_SCALEFORM_SCRIPT_HUD_MOVIE_LOADED(HUD_DRUGS_PURSE_04)
				AND is_drug_hud_activated = FALSE
				//AND is_drug_hud_activated = FALSE
						//WAIT(4000) // REMOVE THIS WAIT!
					//println("drug render 7)")
					SET_SCRIPT_GFX_ALIGN(UI_ALIGN_RIGHT, UI_ALIGN_TOP)
					is_drug_hud_activated = TRUE
					#IF IS_DEBUG_BUILD
						PRINTLN("\n Drug hud assets loaded and now displaying drug hud 1 \n")
					#ENDIF
					RESET_SCRIPT_GFX_ALIGN()
				ELSE
					#IF IS_DEBUG_BUILD
						PRINTLN("\n Waiting for drug hud assets to load \n")
					#ENDIF
					IF NOT HAS_SCALEFORM_SCRIPT_HUD_MOVIE_LOADED(HUD_DRUGS_PURSE_01)
						REQUEST_SCALEFORM_SCRIPT_HUD_MOVIE(HUD_DRUGS_PURSE_01)
					ENDIF
					IF NOT HAS_SCALEFORM_SCRIPT_HUD_MOVIE_LOADED(HUD_DRUGS_PURSE_02)
						REQUEST_SCALEFORM_SCRIPT_HUD_MOVIE(HUD_DRUGS_PURSE_02)
					ENDIF
					IF NOT HAS_SCALEFORM_SCRIPT_HUD_MOVIE_LOADED(HUD_DRUGS_PURSE_03)
						REQUEST_SCALEFORM_SCRIPT_HUD_MOVIE(HUD_DRUGS_PURSE_03)
					ENDIF
					IF NOT HAS_SCALEFORM_SCRIPT_HUD_MOVIE_LOADED(HUD_DRUGS_PURSE_04)
						REQUEST_SCALEFORM_SCRIPT_HUD_MOVIE(HUD_DRUGS_PURSE_04)
					ENDIF
					
						//onMissionInventHud.have_drug_hud_assets_loaded = FALSE
					is_drug_hud_activated = FALSE				
				ENDIF				
			ENDIF
				
					
		
		ENDIF
	
	
	//ENDIF
ENDPROC



PROC remove_drug_hud()


		IF is_drug_hud_activated = TRUE
		//AND spec_hud_display_drug_hud = FALSE	
			
			IF NOT HAS_SCALEFORM_SCRIPT_HUD_MOVIE_LOADED(HUD_DRUGS_PURSE_01)
			AND NOT HAS_SCALEFORM_SCRIPT_HUD_MOVIE_LOADED(HUD_DRUGS_PURSE_02)
			AND NOT HAS_SCALEFORM_SCRIPT_HUD_MOVIE_LOADED(HUD_DRUGS_PURSE_03)
			AND NOT HAS_SCALEFORM_SCRIPT_HUD_MOVIE_LOADED(HUD_DRUGS_PURSE_04)
				is_drug_hud_activated = FALSE
				spec_hud_display_drug_hud = FALSE
				#IF IS_DEBUG_BUILD
					PRINTLN("\n REMOVING DRUGHUD \n")
				#ENDIF
			ENDIF
		ENDIF
ENDPROC







