
/* ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//																															//
//	Date:			22/04/2014																								//
//																															//
// 	Script:			Celebrations.sc																							//
//																															//
// 	Description:	Handles running the end celebration screen independently of the mission controller script. 				//
// 					Originally requested so we can do session to session transitions behind the celebration screen.			//
//																															//
// 	Author:			William.Kennedy@RockstarNorth.com																		//
//																															//
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// */



// **************** //
// 	USING INCLUDES 	//
// ****************	//

USING "globals.sch"
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_path.sch"
USING "commands_fire.sch"
USING "building_control_public.sch"
USING "cutscene_public.sch"
USING "commands_streaming.sch"
USING "net_events.sch"
USING "MP_SkyCam.sch"
USING "net_spawn.sch"
USING "net_mission.sch"
USING "net_scoring_common.sch"
USING "net_ambience.sch"
USING "net_rank_ui.sch"
USING "net_hud_activating.sch"
USING "selector_public.sch"
USING "help_at_location.sch"
USING "net_objective_text.sch"
USING "net_cutscene.sch"
USING "carmod_shop_private.sch"
USING "freemode_header.sch"
USING "net_mission_locate_message.sch"
USING "NET_TAXI.sch"
USING "cheat_handler.sch"
USING "net_wait_zero.sch"
USING "net_celebration_screen.sch"

USING "net_heists_common.sch"
USING "net_heist_summary.sch"

#IF IS_DEBUG_BUILD
	USING "net_debug_log.sch"
#ENDIF



// **************************** //
// 	ENUMS, STRUCTS & CONSTS		//
// **************************** //


CONST_INT CELEB_GENERAL_BITSET_PRE_LOAD_WARP_TRIGGERED			0
CONST_INT CELEB_GENERAL_BITSET_COMPLETED_PLAYER_PRE_LOAD_WARP	1
CONST_INT CELEB_GENERAL_BITSET_SETUP_WINNER_SCENE_MOVIE			2
CONST_INT CELEB_GENERAL_BITSET_MOCAP_PRE_LOAD_COMPLETE			3
CONST_INT CELEB_GENERAL_BITSET_TRANSITIONED_INTO_BW				4
CONST_INT CELEB_GENERAL_BITSET_TRANSITIONED_OUT_OF_BW			5
CONST_INT CELEB_GENERAL_BITSET_PLAYED_CELEB_END_FOR_1_SEC		6
CONST_INT CELEB_GENERAL_COMPLETED_POINT_AT_PLAYER_CAMERA_CUT	7
CONST_INT CELEB_GENERAL_STARTED_WHITE_RECTANGLE_FADE			8
CONST_INT CELEB_GENERAL_STARTED_STATS_SCREEN_MUSIC_EVENT		9
CONST_INT CELEB_GENERAL_STOPPED_STATS_SCREEN_MUSIC_EVENT		10
CONST_INT CELEB_GENERAL_PLAYED_SUCCESS_NEUTRAL_FX_STACK			11
CONST_INT CELEB_GENERAL_REQUESTED_CUTSCENE_MUSIC_EVENT			12
CONST_INT CELEB_GENERAL_SET_ALLOW_RADIO_OVER_FADE_FLAG			13
CONST_INT CELEB_GENERAL_SET_ALLOW_CUTSCENE_OVER_FADE_FLAG		14
CONST_INT CELEB_GENERAL_BITSET_FREEZE_MICROPHONE				15
CONST_INT CELEB_GENERAL_STARTED_FADE_BLACK_SCENE				16
CONST_INT CELEB_GENERAL_FIX_POSITION							17

CONST_INT HEIST_WINNER_SCENE_SET_PLAYER_FOR_MOCAP				0
CONST_INT HEIST_WINNER_SCENE_INITIALISED_DATA					1
CONST_INT HEIST_WINNER_SCENE_SWITCHED_OF_LOADING_SPINNER		2
CONST_INT HEIST_WINNER_SCENE_STARTED_WAITING_SPINNER			3
CONST_INT HEIST_WINNER_SCENE_INITIALISED_APARTMENT_DATA			4
CONST_INT HEIST_WINNER_SCENE_CREATED_ENTITIES					5
CONST_INT HEIST_WINNER_SCENE_SET_FOCUS_POS						6

CONST_INT HEIST_WINNER_MOCAP_LOAD_FINISHED						9999

CONST_INT HEIST_MAX_WINNER_SCENE_OBJECTS						5

CONST_INT HEIST_WINNER_SCENE_STRIP_GLASS						0
CONST_INT HEIST_WINNER_SCENE_STRIP_GLASS_1						1
CONST_INT HEIST_WINNER_SCENE_STRIP_GLASS_2						2
CONST_INT HEIST_WINNER_SCENE_STRIP_GLASS_3						3
CONST_INT HEIST_WINNER_SCENE_STRIP_WHISKEY						4

CONST_INT HEIST_WINNER_SCENE_APT_CHAMP_2						0
CONST_INT HEIST_WINNER_SCENE_APT_CHAMP_3						1
CONST_INT HEIST_WINNER_SCENE_APT_CHAMP_4						2

CONST_INT HEIST_WINNER_SCENE_BAR_BOTTLE							0
CONST_INT HEIST_WINNER_SCENE_BAR_BOTTLE_1						1


CONST_INT WHITE_SCREEN_HOLD_TIME								3000
CONST_INT DELAY_FOR_MUSIC_TIME									2000

// Backup winner scene peds. 
INT iNumPlayersInScene
CONST_INT NUM_PLAYERS_NEEDED_FOR_STRIP_SCENE		4
CONST_INT NUM_PLAYERS_NEEDED_FOR_TREVOR_SCENE		0
CONST_INT NUM_PLAYERS_NEEDED_FOR_ROOFTOP_SCENE		4
CONST_INT NUM_PLAYERS_NEEDED_FOR_APARTMENT_SCENE	4
CONST_INT NUM_PLAYERS_NEEDED_FOR_CASINO_HEIST		4
CONST_INT NUM_PLAYERS_NEEDED_FOR_BAR_SCENE			2
#IF FEATURE_HEIST_ISLAND
CONST_INT NUM_PLAYERS_NEEDED_FOR_ISLAND_HEIST		4

CONST_INT COMPANION_PED_ID 1
#ENDIF

//Scene specific vehicles/props
CONST_INT CASINO_HEIST_ROOF_SCENE_VEHICLES 	13
CONST_INT CASINO_HEIST_BAR_SCENE_PROPS		3
CONST_INT CASINO_ISLAND_HEIST_SCENE_PROPS	4

// Game States
ENUM eGAME_STATE
	eGAME_STATE_INI = 0,
	eGAME_STATE_INI_SPAWN,
	eGAME_STATE_RUNNING,
	eGAME_STATE_END	
ENDENUM

// Celebration States
ENUM eCELEBRATION_STATE
	eCELEBRATIONSTATE_INIT_MOVIE = 0,
	eCELEBRATIONSTATE_LOAD_MOVIE,
	eCELEBRATIONSTATE_DRAW_MOVIE,
	eCELEBRATIONSTATE_DO_WINNER_SCENE,
	eCELEBRATIONSTATE_FADE_IN,
	eCELEBRATIONSTATE_APARTMENT_FAILSAFE,
	eCELEBRATIONSTATE_CLEANUP
ENDENUM


// Heist Winner Scene
ENUM eHEIST_WINNER_SCENE_STAGE
	eHEISTWINNERSCENESTAGE_INIT = 0,
	eHEISTWINNERSCENESTAGE_LOAD_SCENE,
	eHEISTWINNERSCENESTAGE_LOAD_SIMPLE_INTERIOR,
	eHEISTWINNERSCENESTAGE_DELAY_BEFORE_CUTSCENE_START,
	eHEISTWINNERSCENESTAGE_START_SCENE,
	eHEISTWINNERSCENESTAGE_START_CELEBRATION_MOVIE,
	eHEISTWINNERSCENESTAGE_RUN_SCENE,
	eHEISTWINNERSCENESTAGE_WARP_TO_OUTSIDE_STRIP_CLUB,
	eHEISTWINNERSCENESTAGE_DO_JOINED_MISSION_AS_SPECTATOR_WARP,
	eHEISTWINNERSCENESTAGE_WARP_TO_POST_MISSION_VECTOR,
	eHEISTWINNERSCENESTAGE_SCTV_FADE_IN_DELAY,
	eHEISTWINNERSCENESTAGE_SHARD_OVER_FADE,
	eHEISTWINNERSCENESTAGE_SHARD_OVER_PAUSED_RENDER,
	eHEISTWINNERSCENESTAGE_END
ENDENUM

ENUM eHEIST_WINNER_SCENE_TYPE
	eHEISTWINNERSCENETYPE_ROOFTOP = 0,
	eHEISTWINNERSCENETYPE_BAR,
	eHEISTWINNERSCENETYPE_APARTMENT,
	eHEISTWINNERSCENETYPE_STRIP_CLUB,
	eHEISTWINNERSCENETYPE_TREVOR,
	eHEISTWINNERSCENETYPE_CASINO_HEIST_ROOF,
	eHEISTWINNERSCENETYPE_CASINO_HEIST_BAR
	#IF FEATURE_HEIST_ISLAND
	,eHEISTWINNERSCENETYPE_ISLAND_HEIST
	#ENDIF
ENDENUM


INT iCelebrationCloningStage
SCRIPT_TIMER timerFadeFlashDelay
SCRIPT_TIMER timerDelayForCutsceneMusic
SCRIPT_TIMER waitForApartmentFadeIntimer

//SCALEFORM_RETURN_INDEX movieLengthReturnIndex
INT iCelebrationGeneralBitset
INT iPreLoadStage
INT iApartmentFailsafeStage

INT iJoinedMissionAsSpectatorWarpStage

BOOL bActivateInteractionMenuOnCleanup
BOOL bWaitingOnPlayersTimeout

SCRIPT_TIMER timerWaitingOnPlayers
SCRIPT_TIMER timerFrozenForCelebrationScreen
SCRIPT_TIMER timerShardFallenAway
SCRIPT_TIMER timerSctvFadeInDelay

SCRIPT_TIMER timerPlaceCameraFailSafeTimer

// url:bugstar:2255042
INT iFreezeMicrophoneDelay = 0

STRUCT STAND_ALONE_CELEBRATION_DATA_STRUCT
	eGAME_STATE eGameState
	eCELEBRATION_STATE eCelebrationState
	INT iGameState
	INT iFlagsBitset
	SCRIPT_TIMER timeTerminate
ENDSTRUCT


STRUCT STRUCT_HEIST_WINNER_SCENE_PED_DATA
	
	PED_INDEX pedId
	VECTOR vSpawnPosition
	FLOAT fSpawnHeading
	
	#IF IS_DEBUG_BUILD
		VECTOR vPedPosDebug
		FLOAT fPedHeadingDebug
		BOOL bOverridePedWorldData
	#ENDIF
	
ENDSTRUCT

STRUCT STRUCT_HEIST_WINNER_SCENE_EXTRA_PED_DATA
	
	PED_INDEX pedId
	VECTOR vSpawnPosition
	FLOAT fSpawnHeading
	MODEL_NAMES eModel
	TEXT_LABEL_23 tl23_handle
	BOOL bAppliedHeadBlood
	
ENDSTRUCT

STRUCT STRUCT_HEIST_WINNER_SCENE_OBJECT_DATA
	
	ENTITY_INDEX entityId
	VECTOR vSpawnPosition
	FLOAT fSpawnHeading
	MODEL_NAMES eModel
	TEXT_LABEL_23 tl23_handle
	
ENDSTRUCT

ENUM SHARD_OVER_FADE_STAGE
	SOF_FADE_OUT,
	SOF_SETUP_SHARD,
	SOF_DRAW,
	SOF_CLEANUP
ENDENUM

STRUCT STRUCT_HEIST_END_WINNER_SCENE_DATA
	
	eHEIST_WINNER_SCENE_TYPE eSceneType
	eHEIST_WINNER_SCENE_STAGE eSceneStage
	
	VECTOR vPlayerPosForLoadingScene
	FLOAT fPlayerHeadingForLoadingScene
	
	VECTOR vPlayerPosForAfterScene
	FLOAT fPlayerHeadingForafterScene
	
	VECTOR vFocusPosition
	
	INT iMyScenePositionIndex
	
	INT iLoadMocapStage
	INT iMocapRegisteredPedsCount
	
	STRUCT_HEIST_WINNER_SCENE_PED_DATA playerPedClone[4]
	STRUCT_HEIST_WINNER_SCENE_EXTRA_PED_DATA extraPed
	STRUCT_HEIST_WINNER_SCENE_OBJECT_DATA object[HEIST_MAX_WINNER_SCENE_OBJECTS]
	
	INT iBitset
	SCRIPT_TIMER sceneTimer
	
	BOOL bCreatedPlayerPedClones
	
	INTERIOR_INSTANCE_INDEX interiorIndex
	STRING strInteriorName
	STRING strRoomName
	
	SCRIPT_TIMER mocapNotPlayingTimer
	
	BOOL bInteriorNeedsReCapped
	BOOL bInteriorNeedsDisabled
	
	BOOL bDrawCongratsShard
	
	BOOL bActivatedRequiredIpls
	BOOL bSetupSceneEntities
	SHARD_OVER_FADE_STAGE eShardOverFadeStage
	SCRIPT_TIMER shardOverFadeDisplayTime
	
	eCELEBRATION_AUDIO_STAGE eWinnerAudioScene
	
	PLAYER_INDEX pOwner
	SIMPLE_INTERIORS eTargetInteriorID
	BOOL bPinnedAndRequestedInterior
	
	GAMER_HANDLE playerLeader	//Script launch data passes us the leader of the last mission
	
	#IF FEATURE_HEIST_ISLAND
	MODEL_NAMES eCompanionPedModel = DUMMY_MODEL_FOR_SCRIPT
	#ENDIF
	
	#IF IS_DEBUG_BUILD
		BOOL bRunEndHeistScene
		BOOL bTerminateEndHeistScene
		BOOL bEditMode
	#ENDIF
	
ENDSTRUCT

STRUCT_HEIST_END_WINNER_SCENE_DATA sHeistWinnerSceneData


STAND_ALONE_CELEBRATION_DATA_STRUCT standAloneData[1]
CELEBRATION_SCREEN_DATA sCelebrationData



// ******************** //
// 	LOCAL VARIABLES 	//
// ******************** //

CAMERA_INDEX pointAtPlayerCam
BOOL bPointAtPlayerCamSuccessfullyPlaced
INT iPointAtPlayerCameraCutStage
INTERIOR_INSTANCE_INDEX apartmentInteriorIndex



// ******************** //
// 	DEBUG VARIABLES 	//
// ******************** //

#IF IS_DEBUG_BUILD

BOOL bHostEndMissionNow
TEXT_WIDGET_ID twID_GameState
TEXT_WIDGET_ID twID_FailReason
TEXT_WIDGET_ID twID_LiteralFailReason 
TEXT_WIDGET_ID twID_lbPlayerRating[4]
TEXT_WIDGET_ID twID_lbPlayerName[4]
INT iWidgetPlayerIds[4]



// ******************** //
// 	DEBUG FUNCTIONS		//
// ******************** //
	
FUNC STRING GET_GAME_STATE_NAME(eGAME_STATE eState)
	
	SWITCH eState
		CASE eGAME_STATE_INI				RETURN "eGAME_STATE_INI"
		CASE eGAME_STATE_INI_SPAWN			RETURN "eGAME_STATE_INI_SPAWN"
		CASE eGAME_STATE_RUNNING			RETURN "eGAME_STATE_RUNNING"
		CASE eGAME_STATE_END				RETURN "eGAME_STATE_END"
	ENDSWITCH
	
	RETURN "INVALID GAME STATE"
	
ENDFUNC

FUNC STRING GET_CELEBRATION_STATE_NAME(eCELEBRATION_STATE eState)
	
	SWITCH eState
		CASE eCELEBRATIONSTATE_INIT_MOVIE			RETURN "eCELEBRATIONSTATE_INIT_MOVIE"
		CASE eCELEBRATIONSTATE_LOAD_MOVIE			RETURN "eCELEBRATIONSTATE_LOAD_MOVIE"
		CASE eCELEBRATIONSTATE_DRAW_MOVIE			RETURN "eCELEBRATIONSTATE_DRAW_MOVIE"
		CASE eCELEBRATIONSTATE_DO_WINNER_SCENE		RETURN "eCELEBRATIONSTATE_DO_WINNER_SCENE"
		CASE eCELEBRATIONSTATE_FADE_IN				RETURN "eCELEBRATIONSTATE_FADE_IN"
		CASE eCELEBRATIONSTATE_APARTMENT_FAILSAFE 	RETURN "eCELEBRATIONSTATE_APARTMENT_FAILSAFE"
		CASE eCELEBRATIONSTATE_CLEANUP				RETURN "eCELEBRATIONSTATE_CLEANUP"
	ENDSWITCH
	
	RETURN "INVALID CELEBRATION STATE"
	
ENDFUNC
#ENDIF

#IF IS_DEBUG_BUILD

PROC ADD_HEIST_WINNER_SCENE_WIDGETS()
	INT i
	TEXT_LABEL_63 tl63
	START_WIDGET_GROUP("Heist Winner Scene")
		ADD_WIDGET_BOOL("Run Scene", sHeistWinnerSceneData.bRunEndHeistScene)
		ADD_WIDGET_BOOL("Terminate Scene", sHeistWinnerSceneData.bTerminateEndHeistScene)
		ADD_WIDGET_BOOL("Edit Mode Active", sHeistWinnerSceneData.bEditMode)
		START_WIDGET_GROUP("Scene Peds")
			REPEAT 4 i
				tl63 = "Ped "
				tl63 += i
				tl63 += " Activate Override World Data"
				ADD_WIDGET_BOOL(tl63, sHeistWinnerSceneData.playerPedClone[i].bOverridePedWorldData)
				tl63 = "Ped "
				tl63 += i
				tl63 += " World Position"
				ADD_WIDGET_VECTOR_SLIDER(tl63, sHeistWinnerSceneData.playerPedClone[i].vPedPosDebug, -10000.0, 10000.0, 0.01)
				tl63 = "Ped "
				tl63 += i
				tl63 += " World Heading"
				ADD_WIDGET_FLOAT_SLIDER(tl63, sHeistWinnerSceneData.playerPedClone[i].fPedHeadingDebug, -10000.0, 10000.0, 0.01)
			ENDREPEAT
		STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP()
ENDPROC

#ENDIF



// ************ //
// 	FUNCTIONS	//
// ************ //

/// PURPOSE:
///    Helper function to get a clients game/mission state
/// PARAMS:
///    iPlayer - The player's game state you want.
/// RETURNS:
///    The game state.
FUNC eGAME_STATE GET_GAME_STATE(INT iParticipant)
	
	RETURN standAloneData[iParticipant].eGameState
	
ENDFUNC

/// PURPOSE:
///    Sets the game state on the local participant.
/// PARAMS:
///    eState - state to set to.
PROC SET_GAME_STATE(eGAME_STATE eState)
	
	#IF IS_DEBUG_BUILD
		IF standAloneData[0].eGameState != eState
			STRING sTemp = GET_GAME_STATE_NAME(eState)
			PRINTLN("[SAC] - setting local participant game state to ", sTemp)
		ENDIF
	#ENDIF
	
	standAloneData[0].eGameState = eState
	
ENDPROC

FUNC eCELEBRATION_STATE GET_CELEBRATION_STATE(INT iParticipant)
	
	PRINTLN("[SAC] - get local participant celebration state is ", standAloneData[iParticipant].eCelebrationState)
	RETURN standAloneData[iParticipant].eCelebrationState
	
ENDFUNC

/// PURPOSE:
///    Sets the game state on the local participant.
/// PARAMS:
///    eState - state to set to.
PROC SET_CELEBRATION_STATE(eCELEBRATION_STATE eState)
	
	#IF IS_DEBUG_BUILD
		IF standAloneData[0].eCelebrationState != eState
			STRING sTemp = GET_CELEBRATION_STATE_NAME(eState)
			PRINTLN("[SAC] - setting local participant celebration state to ", sTemp)
		ENDIF
	#ENDIF
	
	standAloneData[0].eCelebrationState = eState
	
ENDPROC

FUNC BOOL GET_SHOULD_END_SCRIPT()
	
	#IF IS_DEBUG_BUILD
		IF bHostEndMissionNow
			PRINTLN("[SAC] - bHostEndMissionNow = TRUE, making GET_SHOULD_END_SCRIPT return TRUE.")
			RETURN TRUE
		ENDIF
	#ENDIF	
	
	RETURN FALSE
	
ENDFUNC


PROC SET_MY_SCENE_POSITION_INDEX(STRUCT_HEIST_END_WINNER_SCENE_DATA &sData)
	
	INT iPlayer, iCount
	PLAYER_INDEX playerId
	
	REPEAT NUM_NETWORK_PLAYERS iPlayer
		IF IS_NET_PLAYER_OK(INT_TO_NATIVE(PLAYER_INDEX, iPlayer), FALSE)
			playerId = INT_TO_NATIVE(PLAYER_INDEX, iPlayer)
			IF NOT IS_PLAYER_SCTV(playerId)
			AND NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bJoinedMissionAsSpectator)//GlobalplayerBD_FM[NATIVE_TO_INT(playerId)].bJoinedMissionAsSpectator
				IF playerId = PLAYER_ID()
					IF iCount <= 3
						sData.iMyScenePositionIndex = iCount
						PRINTLN("[SAC] - [NETCELEBRATION] - [PMC] - set my scene position index to ", sData.iMyScenePositionIndex)
					#IF IS_DEBUG_BUILD
					ELSE
						PRINTLN("[SAC] - [NETCELEBRATION] - [PMC] - SET_MY_SCENE_POSITION_INDEX - iCount > 3. iCount = ", iCount)
					#ENDIF
					ENDIF
				ENDIF
				iCount++
			ENDIF
		ENDIF
	ENDREPEAT
	
ENDPROC

PROC SET_APARTMENT_EXTERIOR_SPAWN_DATA(STRUCT_HEIST_END_WINNER_SCENE_DATA &sData)
	
	MP_PROP_OFFSET_STRUCT sOffsetStruct
	INT iBuilding = GET_PROPERTY_BUILDING(g_TransitionSessionNonResetVars.iAptIndex)
	INT iSpawnIndex
	
	IF NOT IS_PLAYER_SCTV(PLAYER_ID())
	AND NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bJoinedMissionAsSpectator)//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bJoinedMissionAsSpectator
		iSpawnIndex = sData.iMyScenePositionIndex
	ELSE
		iSpawnIndex = 0
	ENDIF
	
	GET_TRANSITION_ANIM_CUSTOM_EXIT_SPAWN_COORDS(iBuilding, 0, iSpawnIndex, sOffsetStruct)
	sData.vPlayerPosForAfterScene = sOffsetStruct.vLoc
	sData.fPlayerHeadingForafterScene = sOffsetStruct.vRot.z
	
	IF IS_PLAYER_SCTV(PLAYER_ID())
	OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bJoinedMissionAsSpectator)//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bJoinedMissionAsSpectator
		GET_TRANSITION_ANIM_CUSTOM_EXIT_SPAWN_COORDS(iBuilding, 0, 0, sOffsetStruct)
		sData.vPlayerPosForAfterScene = sOffsetStruct.vLoc
		sData.fPlayerHeadingForafterScene = sOffsetStruct.vRot.z
		IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bJoinedMissionAsSpectator)//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bJoinedMissionAsSpectator
			IF IS_VECTOR_ZERO(sData.vPlayerPosForAfterScene)
				GET_TRANSITION_ANIM_CUSTOM_EXIT_SPAWN_COORDS(1, 0, 0, sOffsetStruct)
				sData.vPlayerPosForAfterScene = sOffsetStruct.vLoc
				sData.fPlayerHeadingForafterScene = sOffsetStruct.vRot.z
				PRINTLN("[SAC] - Something has gone wrong with getting apartment experior coords. Force getting for building 1, so we dont fall through the map.")
			ENDIF
		ENDIF
	ENDIF
	
	PRINTLN("[SAC] - Called GET_TRANSITION_ANIM_CUSTOM_EXIT_SPAWN_COORDS with the following args:")
	PRINTLN("[SAC] - iAptIndex = ", g_TransitionSessionNonResetVars.iAptIndex)
	PRINTLN("[SAC] - iApartmentVariation = ", g_TransitionSessionNonResetVars.iApartmentVariation)
	PRINTLN("[SAC] - iBuilding = GET_PROPERTY_BUILDING(g_TransitionSessionNonResetVars.iAptIndex) = ", iBuilding)
	PRINTLN("[SAC] - iEntrance = ", 0)
	PRINTLN("[SAC] - iSpawnIndex = ", iSpawnIndex)
	PRINTLN("[SAC] - GET_TRANSITION_ANIM_CUSTOM_EXIT_SPAWN_COORDS results: ")
	PRINTLN("[SAC] - sData.vPlayerPosForAfterScene = sOffsetStruct.vLoc = ", sOffsetStruct.vLoc)
	PRINTLN("[SAC] - sData.fPlayerHeadingForafterScene = sOffsetStruct.vRot.z = ", sOffsetStruct.vRot.z)
	
ENDPROC

PROC SET_APARTMENT_INTERIOR_SPAWN_DATA(STRUCT_HEIST_END_WINNER_SCENE_DATA &sData)
	
	sData.vPlayerPosForAfterScene = sData.vPlayerPosForLoadingScene
	sData.fPlayerHeadingForafterScene = sData.fPlayerHeadingForLoadingScene
	
	SET_POST_MISSION_PLACE_IN_APARTMENT_FLAG(TRUE)
	
	PRINTLN("[SAC] - set vPlayerPosForAfterScene = vPlayerPosForLoadingScene = ", sData.vPlayerPosForAfterScene)
	PRINTLN("[SAC] - set fPlayerHeadingForafterScene = fPlayerHeadingForLoadingScene = ", sData.fPlayerHeadingForafterScene)
	
ENDPROC

PROC INITIALISE_APARTMENT_SCENE_DATA(STRUCT_HEIST_END_WINNER_SCENE_DATA &sData)
	
	INT iBase
	MP_PROP_OFFSET_STRUCT sOffsetStruct
	
	
	
	IF IS_HEIST_PROPERTY_TYPE_DLC(g_TransitionSessionNonResetVars.iAptIndex)
		sData.strInteriorName = "hei_dlc_apart_high2_new"
		sData.strRoomName = "mp_apt_h_01_main"
		iBase = PROPERTY_BUS_HIGH_APT_1
	
	ELIF IS_PROPERTY_CUSTOM_APARTMENT(g_TransitionSessionNonResetVars.iAptIndex)
		IF g_TransitionSessionNonResetVars.iApartmentVariation = PROPERTY_VARIATION_1	
			sData.strInteriorName = "apa_v_mp_h_01"
			sData.strRoomName = "mp_h_01_living"
		ENDIF
		IF g_TransitionSessionNonResetVars.iApartmentVariation = PROPERTY_VARIATION_2
			sData.strInteriorName = "apa_v_mp_h_02"
			sData.strRoomName = "mp_h_02_living"
		ENDIF
		IF g_TransitionSessionNonResetVars.iApartmentVariation = PROPERTY_VARIATION_3
			sData.strInteriorName = "apa_v_mp_h_03"
			sData.strRoomName = "mp_h_03_living"
		ENDIF
		IF g_TransitionSessionNonResetVars.iApartmentVariation = PROPERTY_VARIATION_4
			sData.strInteriorName = "apa_v_mp_h_04"
			sData.strRoomName = "mp_h_04_living"
		ENDIF
		IF g_TransitionSessionNonResetVars.iApartmentVariation = PROPERTY_VARIATION_5
			sData.strInteriorName = "apa_v_mp_h_05"
			sData.strRoomName = "mp_h_05_living"
		ENDIF
		IF g_TransitionSessionNonResetVars.iApartmentVariation = PROPERTY_VARIATION_6
			sData.strInteriorName = "apa_v_mp_h_06"
			sData.strRoomName = "mp_h_06_living"
		ENDIF
		IF g_TransitionSessionNonResetVars.iApartmentVariation = PROPERTY_VARIATION_7
			sData.strInteriorName = "apa_v_mp_h_07"
			sData.strRoomName = "mp_h_07_living"
		ENDIF
		IF g_TransitionSessionNonResetVars.iApartmentVariation = PROPERTY_VARIATION_8
			sData.strInteriorName = "apa_v_mp_h_08"
			sData.strRoomName = "mp_h_08_living"
		ENDIF
		iBase = PROPERTY_CUSTOM_APT_1_BASE
		
	ELIF IS_PROPERTY_STILT_APARTMENT(g_TransitionSessionNonResetVars.iAptIndex, PROPERTY_STILT_APT_5_BASE_A)
		sData.strInteriorName = "apa_v_mp_stilts_a"
		sData.strRoomName = "mr_mp_stilts_a_living"
		iBase = PROPERTY_STILT_APT_5_BASE_A

	ELIF IS_PROPERTY_STILT_APARTMENT(g_TransitionSessionNonResetVars.iAptIndex, PROPERTY_STILT_APT_1_BASE_B)
		sData.strInteriorName = "apa_v_mp_stilts_b"
		sData.strRoomName = "mr_mp_stilts_b_living"
		iBase = PROPERTY_STILT_APT_1_BASE_B
		
	ELSE
		sData.strInteriorName = "hei_dlc_apart_high_new"
		sData.strRoomName = "rm_high_lounge"
		iBase = PROPERTY_HIGH_APT_1
		
	ENDIF
	
	GET_POSITION_AS_OFFSET_FOR_PROPERTY(g_TransitionSessionNonResetVars.iAptIndex, MP_END_HEIST_WINNER_SCENE_PED_0_COORDS, sOffsetStruct, iBase)
	sData.playerPedClone[0].vSpawnPosition = sOffsetStruct.vLoc
	sData.playerPedClone[0].fSpawnHeading = sOffsetStruct.vRot.z
	
	GET_POSITION_AS_OFFSET_FOR_PROPERTY(g_TransitionSessionNonResetVars.iAptIndex, MP_END_HEIST_WINNER_SCENE_PED_1_COORDS, sOffsetStruct, iBase)
	sData.playerPedClone[1].vSpawnPosition = sOffsetStruct.vLoc
	sData.playerPedClone[1].fSpawnHeading = sOffsetStruct.vRot.z
	
	GET_POSITION_AS_OFFSET_FOR_PROPERTY(g_TransitionSessionNonResetVars.iAptIndex, MP_END_HEIST_WINNER_SCENE_PED_2_COORDS, sOffsetStruct, iBase)
	sData.playerPedClone[2].vSpawnPosition = sOffsetStruct.vLoc
	sData.playerPedClone[2].fSpawnHeading = sOffsetStruct.vRot.z
	
	GET_POSITION_AS_OFFSET_FOR_PROPERTY(g_TransitionSessionNonResetVars.iAptIndex, MP_END_HEIST_WINNER_SCENE_PED_3_COORDS, sOffsetStruct, iBase)
	sData.playerPedClone[3].vSpawnPosition = sOffsetStruct.vLoc
	sData.playerPedClone[3].fSpawnHeading = sOffsetStruct.vRot.z
	
	sData.object[HEIST_WINNER_SCENE_APT_CHAMP_2].vSpawnPosition = sData.playerPedClone[0].vSpawnPosition
	sData.object[HEIST_WINNER_SCENE_APT_CHAMP_2].vSpawnPosition += << 0.5, 0.0, 0.0 >>
	sData.object[HEIST_WINNER_SCENE_APT_CHAMP_2].fSpawnHeading = sData.playerPedClone[0].fSpawnHeading
	sData.object[HEIST_WINNER_SCENE_APT_CHAMP_2].eModel = PROP_CS_CHAMP_FLUTE
	sData.object[HEIST_WINNER_SCENE_APT_CHAMP_2].tl23_handle = "MPH_CHAMP_2"
	
	sData.object[HEIST_WINNER_SCENE_APT_CHAMP_3].vSpawnPosition = sData.playerPedClone[0].vSpawnPosition
	sData.object[HEIST_WINNER_SCENE_APT_CHAMP_3].vSpawnPosition += << 0.0, 0.5, 0.0 >>
	sData.object[HEIST_WINNER_SCENE_APT_CHAMP_3].fSpawnHeading = sData.playerPedClone[0].fSpawnHeading
	sData.object[HEIST_WINNER_SCENE_APT_CHAMP_3].eModel = PROP_CS_CHAMP_FLUTE
	sData.object[HEIST_WINNER_SCENE_APT_CHAMP_3].tl23_handle = "MPH_CHAMP_3"
	
	sData.object[HEIST_WINNER_SCENE_APT_CHAMP_4].vSpawnPosition = sData.playerPedClone[0].vSpawnPosition
	sData.object[HEIST_WINNER_SCENE_APT_CHAMP_4].vSpawnPosition += << 0.0, 0.0, 0.5 >>
	sData.object[HEIST_WINNER_SCENE_APT_CHAMP_4].fSpawnHeading = sData.playerPedClone[0].fSpawnHeading
	sData.object[HEIST_WINNER_SCENE_APT_CHAMP_4].eModel = PROP_CS_CHAMP_FLUTE
	sData.object[HEIST_WINNER_SCENE_APT_CHAMP_4].tl23_handle = "MPH_CHAMP_4"
	
	sData.eWinnerAudioScene = eCAS_PRISON_SCENE
	
	sData.vFocusPosition = sData.playerPedClone[0].vSpawnPosition
	
	sData.vPlayerPosForLoadingScene = sData.playerPedClone[sData.iMyScenePositionIndex].vSpawnPosition
	sData.fPlayerHeadingForLoadingScene = sData.playerPedClone[sData.iMyScenePositionIndex].fSpawnHeading
	
	SET_APARTMENT_INTERIOR_SPAWN_DATA(sData)
	
	PRINTLN("[SAC] - [NETCELEBRATION] - [PMC] - scene data print - iBase = ", iBase)
	
ENDPROC

PROC SETUP_SIMPLE_INTERIOR_PROPERTY_OWNER(STRUCT_HEIST_END_WINNER_SCENE_DATA &sData)
	IF sData.eSceneType = eHEISTWINNERSCENETYPE_CASINO_HEIST_BAR
		IF IS_GAMER_HANDLE_VALID(sHeistWinnerSceneData.playerLeader)
			sData.pOwner = NETWORK_GET_PLAYER_FROM_GAMER_HANDLE(sHeistWinnerSceneData.playerLeader)
			
			IF sData.pOwner = INVALID_PLAYER_INDEX()
				PRINTLN("[CASINO_HEIST_FLOW][CHF_CS][SAC] | SETUP_SIMPLE_INTERIOR_PROPERTY_OWNER - Gamer handle is valid. Not able to convert to player ID!")
			ELSE
				sData.eTargetInteriorID = GET_OWNED_SIMPLE_INTERIOR_OF_TYPE(sData.pOwner, SIMPLE_INTERIOR_TYPE_ARCADE)
				PRINTLN("[CASINO_HEIST_FLOW][CHF_CS][SAC] | SETUP_SIMPLE_INTERIOR_PROPERTY_OWNER - Gamer handle is valid. Owner: ", NATIVE_TO_INT(sData.pOwner))
				EXIT
			ENDIF
		ENDIF
		
		IF NOT GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG()
			PRINTLN("[CASINO_HEIST_FLOW][CHF_CS][SAC] | SETUP_SIMPLE_INTERIOR_PROPERTY_OWNER - Not in a gang, using local player.")
			sData.pOwner = PLAYER_ID()
		ELIF GB_GET_LOCAL_PLAYER_GANG_BOSS() != INVALID_PLAYER_INDEX()
		AND NETWORK_IS_PLAYER_ACTIVE(GB_GET_LOCAL_PLAYER_GANG_BOSS())
			PRINTLN("[CASINO_HEIST_FLOW][CHF_CS][SAC] | SETUP_SIMPLE_INTERIOR_PROPERTY_OWNER - In a gang, using ths boss.")
			sData.pOwner = GB_GET_LOCAL_PLAYER_GANG_BOSS()
		ELSE
			PRINTLN("[CASINO_HEIST_FLOW][CHF_CS][SAC] | SETUP_SIMPLE_INTERIOR_PROPERTY_OWNER - Fallback check, using ths boss.")
			sData.pOwner = GB_GET_LOCAL_PLAYER_GANG_BOSS()
		ENDIF
		
		sData.eTargetInteriorID = GET_OWNED_SIMPLE_INTERIOR_OF_TYPE(sData.pOwner, SIMPLE_INTERIOR_TYPE_ARCADE)
	ENDIF
ENDPROC

PROC SET_NAME_HASHES_FOR_PLAYERS_IN_SCENE(eHEIST_WINNER_SCENE_TYPE eSceneType)
	IF NOT GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG()
		PRINTLN("[CHF_CS][SAC] | SET_NAME_HASHES_FOR_PLAYERS_IN_SCENE - not in a gang!.")
		g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPlayerNameHash[0] = NETWORK_HASH_FROM_PLAYER_HANDLE(PLAYER_ID())
		EXIT
	ENDIF
	
	PRINTLN("[CHF_CS][SAC] | SET_NAME_HASHES_FOR_PLAYERS_IN_SCENE - Doing this.")
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPlayerNameHash[0] = NETWORK_HASH_FROM_PLAYER_HANDLE(GB_GET_LOCAL_PLAYER_GANG_BOSS())
	
	INT i, iPlayerLoop
	
	SWITCH eSceneType
		CASE eHEISTWINNERSCENETYPE_CASINO_HEIST_ROOF
		CASE eHEISTWINNERSCENETYPE_CASINO_HEIST_BAR
			iPlayerLoop = NUM_PLAYERS_NEEDED_FOR_CASINO_HEIST
		BREAK
		#IF FEATURE_HEIST_ISLAND
		CASE eHEISTWINNERSCENETYPE_ISLAND_HEIST
			iPlayerLoop = NUM_PLAYERS_NEEDED_FOR_ISLAND_HEIST
		BREAK
		#ENDIF
	ENDSWITCH
	
	
	REPEAT iPlayerLoop i
		IF GlobalplayerBD_FM_3[NATIVE_TO_INT(GB_GET_LOCAL_PLAYER_GANG_BOSS())].sMagnateGangBossData.GangMembers[i] != INVALID_PLAYER_INDEX()				
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPlayerNameHash[i + 1] = NETWORK_HASH_FROM_PLAYER_HANDLE(GlobalplayerBD_FM_3[NATIVE_TO_INT(GB_GET_LOCAL_PLAYER_GANG_BOSS())].sMagnateGangBossData.GangMembers[i] )
		ENDIF
	ENDREPEAT
ENDPROC

PROC SET_UP_STRAND_MISSION_FINISHED_FINAL_CUTSCENES()
	IF NOT CHECK_TO_SEE_IF_THIS_IS_THE_LAST_STRAND_MISSION()
		PRINTLN("[SAC] - Exiting - Not the last mission of a strand.")
		EXIT
	ENDIF
	
	PRINTLN("[SAC] - This is the last mission of a strand.")
	
	IF NOT CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_FINALE()
		PRINTLN("[SAC] - Not the heist finale.")
		EXIT
	ENDIF
	
	IF IS_BIT_SET(g_sCasinoHeistFlowData.iBitSet, ciCASINO_HEIST_FLOW_BITSET__PLAY_LESTER_POST_MISSION_ONE_TIME_SCENE)
		PRINTLN("[SAC] - PLAY_LESTER_POST_MISSION_ONE_TIME_SCENE already set.")
		EXIT	
	ENDIF
	
	IF GB_GET_LOCAL_PLAYER_GANG_BOSS() = INVALID_PLAYER_INDEX()
		PRINTLN("[SAC] - LOCAL_PLAYER_GANG_BOSS is invalid.")
		EXIT
	ENDIF
	
	IF IS_BIT_SET(GlobalplayerBD_CasinoHeist[NATIVE_TO_INT(GB_GET_LOCAL_PLAYER_GANG_BOSS())].iFlowBitSet,
	ciCASINO_HEIST_FLOW_STAT_BITSET__LESTER_POST_MISSION_ONE_TIME_SCENE_VIEWED)
		PRINTLN("[SAC] - LESTER_POST_MISSION_ONE_TIME_SCENE_VIEWED already set in BD.")
		EXIT
	ENDIF
	
	PRINTLN("[SAC] - Casino Heist - Setting ciCASINO_HEIST_FLOW_BITSET__PLAY_LESTER_POST_MISSION_ONE_TIME_SCENE")
	SET_BIT(g_sCasinoHeistFlowData.iBitSet, ciCASINO_HEIST_FLOW_BITSET__PLAY_LESTER_POST_MISSION_ONE_TIME_SCENE)
ENDPROC

PROC SET_CASINO_HEIST_FINALE_VEHICLE_STAT()
	IF GB_IS_PLAYER_BOSS_OF_A_GANG(PLAYER_ID())
		PRINTLN("[SAC] - Casino Heist - SET_CASINO_HEIST_FINALE_VEHICLE_STAT")
		
		INT iCasinoHeistFinaleVehicleBS = GET_MP_INT_CHARACTER_STAT(MP_STAT_H3_VEHICLESUSED)
		
		IF GET_LOCAL_PLAYER_CASINO_HEIST_APPROACH() = CASINO_HEIST_APPROACH_TYPE__SUBTERFUGE
			SWITCH INT_TO_ENUM(CASINO_HEIST_OUTFITS, GlobalplayerBD_CasinoHeist[NATIVE_TO_INT(PLAYER_ID())].sMissionConfig.iSubterfugeOutfitsIn)
				CASE CASINO_HEIST_OUTFIT_IN__BUGSTAR
					SET_BIT(iCasinoHeistFinaleVehicleBS, ciCASINO_HEIST_FINALE_BURRITO2)
					PRINTLN("[SAC] - Casino Heist - SET_CASINO_HEIST_FINALE_VEHICLE_STAT - ciCASINO_HEIST_FINALE_BURRITO2")
				BREAK
				CASE CASINO_HEIST_OUTFIT_IN__CELEBRITY
					SET_BIT(iCasinoHeistFinaleVehicleBS, ciCASINO_HEIST_FINALE_FURIA)
					PRINTLN("[SAC] - Casino Heist - SET_CASINO_HEIST_FINALE_VEHICLE_STAT - ciCASINO_HEIST_FINALE_FURIA")
				BREAK
				CASE CASINO_HEIST_OUTFIT_IN__GRUPPE_SECHS
					SET_BIT(iCasinoHeistFinaleVehicleBS, ciCASINO_HEIST_FINALE_STOCKADE)
					PRINTLN("[SAC] - Casino Heist - SET_CASINO_HEIST_FINALE_VEHICLE_STAT - ciCASINO_HEIST_FINALE_STOCKADE")
				BREAK
				CASE CASINO_HEIST_OUTFIT_IN__MECHANIC
					SET_BIT(iCasinoHeistFinaleVehicleBS, ciCASINO_HEIST_FINALE_BOXVILLE)
					PRINTLN("[SAC] - Casino Heist - SET_CASINO_HEIST_FINALE_VEHICLE_STAT - ciCASINO_HEIST_FINALE_BOXVILLE")
				BREAK
			ENDSWITCH
			
			SWITCH INT_TO_ENUM(CASINO_HEIST_OUTFITS, GlobalplayerBD_CasinoHeist[NATIVE_TO_INT(PLAYER_ID())].sMissionConfig.iSubterfugeOutfitsOut)
				CASE CASINO_HEIST_OUTFIT_OUT__FIREMAN
					SET_BIT(iCasinoHeistFinaleVehicleBS, ciCASINO_HEIST_FINALE_FIRETRUK)
					PRINTLN("[SAC] - Casino Heist - SET_CASINO_HEIST_FINALE_VEHICLE_STAT - ciCASINO_HEIST_FINALE_FIRETRUK")
				BREAK
			ENDSWITCH
		ENDIF
		
		SWITCH GET_LOCAL_PLAYER_CASINO_HEIST_DRIVER()
			CASE CASINO_HEIST_DRIVER__KARIM_DENZ
				SWITCH GET_LOCAL_PLAYER_CASINO_HEIST_VEHICLE_SELECTION()
					CASE CASINO_HEIST_VEHICLE_SELECTION__ALTERNATE
						SET_BIT(iCasinoHeistFinaleVehicleBS, ciCASINO_HEIST_FINALE_ASBO)
						PRINTLN("[SAC] - Casino Heist - SET_CASINO_HEIST_FINALE_VEHICLE_STAT - ciCASINO_HEIST_FINALE_ASBO")
					BREAK
					CASE CASINO_HEIST_VEHICLE_SELECTION__ALTERNATE_2
						SET_BIT(iCasinoHeistFinaleVehicleBS, ciCASINO_HEIST_FINALE_KANJO)
						PRINTLN("[SAC] - Casino Heist - SET_CASINO_HEIST_FINALE_VEHICLE_STAT - ciCASINO_HEIST_FINALE_KANJO")
					BREAK
					CASE CASINO_HEIST_VEHICLE_SELECTION__ALTERNATE_3
						SET_BIT(iCasinoHeistFinaleVehicleBS, ciCASINO_HEIST_FINALE_SENTINEL3)
						PRINTLN("[SAC] - Casino Heist - SET_CASINO_HEIST_FINALE_VEHICLE_STAT - ciCASINO_HEIST_FINALE_SENTINEL3")
					BREAK
				ENDSWITCH
			BREAK
			
			CASE CASINO_HEIST_DRIVER__TALIANA_MARTINEZ
				SWITCH GET_LOCAL_PLAYER_CASINO_HEIST_VEHICLE_SELECTION()
					CASE CASINO_HEIST_VEHICLE_SELECTION__DEFAULT
						SET_BIT(iCasinoHeistFinaleVehicleBS, ciCASINO_HEIST_FINALE_RETINUE2)
						PRINTLN("[SAC] - Casino Heist - SET_CASINO_HEIST_FINALE_VEHICLE_STAT - ciCASINO_HEIST_FINALE_RETINUE2")
					BREAK
					CASE CASINO_HEIST_VEHICLE_SELECTION__ALTERNATE
						SET_BIT(iCasinoHeistFinaleVehicleBS, ciCASINO_HEIST_FINALE_YOSEMITE2)
						PRINTLN("[SAC] - Casino Heist - SET_CASINO_HEIST_FINALE_VEHICLE_STAT - ciCASINO_HEIST_FINALE_YOSEMITE2")
					BREAK
					CASE CASINO_HEIST_VEHICLE_SELECTION__ALTERNATE_2
						SET_BIT(iCasinoHeistFinaleVehicleBS, ciCASINO_HEIST_FINALE_SUGOI)
						PRINTLN("[SAC] - Casino Heist - SET_CASINO_HEIST_FINALE_VEHICLE_STAT - ciCASINO_HEIST_FINALE_SUGOI")
					BREAK
					CASE CASINO_HEIST_VEHICLE_SELECTION__ALTERNATE_3
						SET_BIT(iCasinoHeistFinaleVehicleBS, ciCASINO_HEIST_FINALE_JUGULAR)
						PRINTLN("[SAC] - Casino Heist - SET_CASINO_HEIST_FINALE_VEHICLE_STAT - ciCASINO_HEIST_FINALE_JUGULAR")
					BREAK
				ENDSWITCH
			BREAK
			
			CASE CASINO_HEIST_DRIVER__EDDIE_TOH
				SWITCH GET_LOCAL_PLAYER_CASINO_HEIST_VEHICLE_SELECTION()
					CASE CASINO_HEIST_VEHICLE_SELECTION__DEFAULT
						SET_BIT(iCasinoHeistFinaleVehicleBS, ciCASINO_HEIST_FINALE_SULTAN2)
						PRINTLN("[SAC] - Casino Heist - SET_CASINO_HEIST_FINALE_VEHICLE_STAT - ciCASINO_HEIST_FINALE_SULTAN2")
					BREAK
					CASE CASINO_HEIST_VEHICLE_SELECTION__ALTERNATE
						SET_BIT(iCasinoHeistFinaleVehicleBS, ciCASINO_HEIST_FINALE_GAUNTLET3)
						PRINTLN("[SAC] - Casino Heist - SET_CASINO_HEIST_FINALE_VEHICLE_STAT - ciCASINO_HEIST_FINALE_GAUNTLET3")
					BREAK
					CASE CASINO_HEIST_VEHICLE_SELECTION__ALTERNATE_2
						SET_BIT(iCasinoHeistFinaleVehicleBS, ciCASINO_HEIST_FINALE_ELLIE)
						PRINTLN("[SAC] - Casino Heist - SET_CASINO_HEIST_FINALE_VEHICLE_STAT - ciCASINO_HEIST_FINALE_ELLIE")
					BREAK
					CASE CASINO_HEIST_VEHICLE_SELECTION__ALTERNATE_3
						SET_BIT(iCasinoHeistFinaleVehicleBS, ciCASINO_HEIST_FINALE_KOMODA)
						PRINTLN("[SAC] - Casino Heist - SET_CASINO_HEIST_FINALE_VEHICLE_STAT - ciCASINO_HEIST_FINALE_KOMODA")
					BREAK
				ENDSWITCH
			BREAK
			
			CASE CASINO_HEIST_DRIVER__ZACH
				SWITCH GET_LOCAL_PLAYER_CASINO_HEIST_VEHICLE_SELECTION()
					CASE CASINO_HEIST_VEHICLE_SELECTION__DEFAULT
						SET_BIT(iCasinoHeistFinaleVehicleBS, ciCASINO_HEIST_FINALE_MANCHEZ)
						PRINTLN("[SAC] - Casino Heist - SET_CASINO_HEIST_FINALE_VEHICLE_STAT - ciCASINO_HEIST_FINALE_MANCHEZ")
					BREAK
					CASE CASINO_HEIST_VEHICLE_SELECTION__ALTERNATE
						SET_BIT(iCasinoHeistFinaleVehicleBS, ciCASINO_HEIST_FINALE_STRYDER)
						PRINTLN("[SAC] - Casino Heist - SET_CASINO_HEIST_FINALE_VEHICLE_STAT - ciCASINO_HEIST_FINALE_STRYDER")
					BREAK
					CASE CASINO_HEIST_VEHICLE_SELECTION__ALTERNATE_2
						SET_BIT(iCasinoHeistFinaleVehicleBS, ciCASINO_HEIST_FINALE_DEFILER)
						PRINTLN("[SAC] - Casino Heist - SET_CASINO_HEIST_FINALE_VEHICLE_STAT - ciCASINO_HEIST_FINALE_DEFILER")
					BREAK
				ENDSWITCH
			BREAK
			
			CASE CASINO_HEIST_DRIVER__WEAPONS_EXPERT
				SWITCH GET_LOCAL_PLAYER_CASINO_HEIST_VEHICLE_SELECTION()
					CASE CASINO_HEIST_VEHICLE_SELECTION__DEFAULT
						SET_BIT(iCasinoHeistFinaleVehicleBS, ciCASINO_HEIST_FINALE_ZHABA)
						PRINTLN("[SAC] - Casino Heist - SET_CASINO_HEIST_FINALE_VEHICLE_STAT - ciCASINO_HEIST_FINALE_ZHABA")
					BREAK
					CASE CASINO_HEIST_VEHICLE_SELECTION__ALTERNATE
						SET_BIT(iCasinoHeistFinaleVehicleBS, ciCASINO_HEIST_FINALE_VAGRANT)
						PRINTLN("[SAC] - Casino Heist - SET_CASINO_HEIST_FINALE_VEHICLE_STAT - ciCASINO_HEIST_FINALE_VAGRANT")
					BREAK
					CASE CASINO_HEIST_VEHICLE_SELECTION__ALTERNATE_2
						SET_BIT(iCasinoHeistFinaleVehicleBS, ciCASINO_HEIST_FINALE_OUTLAW)
						PRINTLN("[SAC] - Casino Heist - SET_CASINO_HEIST_FINALE_VEHICLE_STAT - ciCASINO_HEIST_FINALE_OUTLAW")
					BREAK
					CASE CASINO_HEIST_VEHICLE_SELECTION__ALTERNATE_3
						SET_BIT(iCasinoHeistFinaleVehicleBS, ciCASINO_HEIST_FINALE_EVERON)
						PRINTLN("[SAC] - Casino Heist - SET_CASINO_HEIST_FINALE_VEHICLE_STAT - ciCASINO_HEIST_FINALE_EVERON")
					BREAK
				ENDSWITCH
			BREAK
		ENDSWITCH
		
		SET_MP_INT_CHARACTER_STAT(MP_STAT_H3_VEHICLESUSED, iCasinoHeistFinaleVehicleBS)
	ENDIF
ENDPROC

#IF FEATURE_HEIST_ISLAND
PROC SET_ISLAND_HEIST_COMPANION_PED_MODEL(STRUCT_HEIST_END_WINNER_SCENE_DATA &sData)
	
	IF HAS_PLAYER_COMPLETED_ISLAND_HEIST_AS_LEADER_FOR_SECOND_TIME(GB_GET_LOCAL_PLAYER_GANG_BOSS())
		sData.eCompanionPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("s_m_m_highsec_04"))
	ENDIF
	
	sData.eCompanionPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("ig_miguelmadrazo"))
ENDPROC
#ENDIF

FUNC BOOL INITIALISE_HEIST_END_WINNER_SCENE_DATA(STRUCT_HEIST_END_WINNER_SCENE_DATA &sData)
	
	IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.iHeistType = ciCELEBRATION_HEIST_TYPE__PREP
		
		PRINTLN("[SAC] - init winner scene data - iHeistType = 1 (setup).")
		IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPostMissionSceneId = SIS_POST_APARTMENT
			PRINTLN("[SAC] - init winner scene data - iPostMissionSceneId = SIS_POST_APARTMENT.")
			INITIALISE_APARTMENT_SCENE_DATA(sData)
		ELSE
			PRINTLN("[SAC] - init winner scene data - iPostMissionSceneId != SIS_POST_APARTMENT.")
		ENDIF
		SET_BIT(sData.iBitset, HEIST_WINNER_SCENE_INITIALISED_DATA)
		PRINTLN("[SAC] - init winner scene data - set bit HEIST_WINNER_SCENE_INITIALISED_DATA.")
		
	ELIF g_TransitionSessionNonResetVars.sGlobalCelebrationData.iHeistType = ciCELEBRATION_HEIST_TYPE__FINALE
		
		PRINTLN("[SAC] - init winner scene data - iHeistType = 2 (finale).")
		
		IF NOT IS_BIT_SET(sData.iBitset, HEIST_WINNER_SCENE_INITIALISED_DATA)
				
			SWITCH g_TransitionSessionNonResetVars.sGlobalCelebrationData.iWinnerSceneType
				CASE FMMC_CELEBRATION_TYPE_ROOFTOP		
					sData.eSceneType = eHEISTWINNERSCENETYPE_ROOFTOP		
					PRINTLN("[SAC] - [NETCELEBRATION] - g_TransitionSessionNonResetVars.sGlobalCelebrationData.iWinnerSceneType = FMMC_CELEBRATION_TYPE_ROOFTOP.")
				BREAK
				CASE FMMC_CELEBRATION_TYPE_BAR
					sData.eSceneType = eHEISTWINNERSCENETYPE_BAR		
					PRINTLN("[SAC] - [NETCELEBRATION] - g_TransitionSessionNonResetVars.sGlobalCelebrationData.iWinnerSceneType = FMMC_CELEBRATION_TYPE_BAR.")
				BREAK
				CASE FMMC_CELEBRATION_TYPE_STRIP_CLUB	
					sData.eSceneType = eHEISTWINNERSCENETYPE_STRIP_CLUB	
					PRINTLN("[SAC] - [NETCELEBRATION] - g_TransitionSessionNonResetVars.sGlobalCelebrationData.iWinnerSceneType = FMMC_CELEBRATION_TYPE_STRIP_CLUB.")
				BREAK
				CASE FMMC_CELEBRATION_TYPE_APARTMENT 	
					sData.eSceneType = eHEISTWINNERSCENETYPE_APARTMENT		
					PRINTLN("[SAC] - [NETCELEBRATION] - g_TransitionSessionNonResetVars.sGlobalCelebrationData.iWinnerSceneType = FMMC_CELEBRATION_TYPE_APARTMENT.")
				BREAK
				CASE FMMC_CELEBRATION_TYPE_TREVOR
					sData.eSceneType = eHEISTWINNERSCENETYPE_TREVOR
					PRINTLN("[SAC] - [NETCELEBRATION] - g_TransitionSessionNonResetVars.sGlobalCelebrationData.iWinnerSceneType = FMMC_CELEBRATION_TYPE_TREVOR.")
				BREAK
				#IF FEATURE_HEIST_ISLAND
				CASE FMMC_CELEBRATION_TYPE_ISLAND_HEIST
					sData.eSceneType = eHEISTWINNERSCENETYPE_ISLAND_HEIST
					g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPostMissionSceneId  = SIS_POST_ISLAND_HEIST
					SET_NAME_HASHES_FOR_PLAYERS_IN_SCENE(sData.eSceneType)
					SET_ISLAND_HEIST_COMPANION_PED_MODEL(sData)
					PREPARE_MUSIC_EVENT("HEI4_CELEBRATION_CHEERS_MUSIC")
					PRINTLN("[SAC] - [NETCELEBRATION] - g_TransitionSessionNonResetVars.sGlobalCelebrationData.iWinnerSceneType = FMMC_CELEBRATION_TYPE_ISLAND_HEIST, setting to eHEISTWINNERSCENETYPE_ISLAND_HEIST.")
				BREAK
				#ENDIF
				CASE FMMC_CELEBRATION_TYPE_DEFAULT
					sData.eSceneType = eHEISTWINNERSCENETYPE_ROOFTOP
					
					PRINTLN("[SAC] - [NETCELEBRATION] - g_TransitionSessionNonResetVars.sGlobalCelebrationData.iWinnerSceneType = FMMC_CELEBRATION_TYPE_DEFAULT, setting to eHEISTWINNERSCENETYPE_ROOFTOP.")
				BREAK
			ENDSWITCH 
			
			IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.iWinnerSceneType = FMMC_CELEBRATION_TYPE_DEFAULT
				IF CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_FINALE()
					SET_UP_STRAND_MISSION_FINISHED_FINAL_CUTSCENES()
					
					SET_CASINO_HEIST_FINALE_VEHICLE_STAT()
					
					IF IS_BIT_SET(g_sCasinoHeistFlowData.iBitSet, ciCASINO_HEIST_FLOW_BITSET__PLAY_LESTER_POST_MISSION_ONE_TIME_SCENE)
						sData.eSceneType = eHEISTWINNERSCENETYPE_CASINO_HEIST_ROOF
						g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPostMissionSceneId  = SIS_POST_CASINO_HEIST
						SET_NAME_HASHES_FOR_PLAYERS_IN_SCENE(sData.eSceneType)					
						PRINTLN("[SAC] - [NETCELEBRATION] - g_TransitionSessionNonResetVars.sGlobalCelebrationData.iWinnerSceneType = override to eHEISTWINNERSCENETYPE_CASINO_HEIST_ROOF.")
					ELSE
						sData.eSceneType = eHEISTWINNERSCENETYPE_CASINO_HEIST_BAR
						g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPostMissionSceneId  = SIS_POST_CASINO_HEIST_2
						SET_NAME_HASHES_FOR_PLAYERS_IN_SCENE(sData.eSceneType)	
						PRINTLN("[SAC] - [NETCELEBRATION] - g_TransitionSessionNonResetVars.sGlobalCelebrationData.iWinnerSceneType = override to eHEISTWINNERSCENETYPE_CASINO_HEIST_BAR.")
					ENDIF
					
					CLEAR_BIT(g_sCasinoHeistFlowData.iBitSet, ciCASINO_HEIST_FLOW_BITSET__PLAY_LESTER_POST_MISSION_ONE_TIME_SCENE)
				ENDIF
			ENDIF
			
			IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPostMissionSceneId = SIS_POST_APARTMENT	
				sData.eSceneType = eHEISTWINNERSCENETYPE_APARTMENT
				PRINTLN("[SAC] - [NETCELEBRATION] - g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPostMissionSceneId = SIS_POST_APARTMENT.")
				PRINTLN("[SAC] - [NETCELEBRATION] - g_TransitionSessionNonResetVars.sGlobalCelebrationData.iWinnerSceneType = FMMC_CELEBRATION_TYPE_APARTMENT.")
			ENDIF
			
	//		sData.eSceneType = eHEISTWINNERSCENETYPE_STRIP_CLUB
			
			SWITCH sData.eSceneType
			
				#IF FEATURE_HEIST_ISLAND
				CASE eHEISTWINNERSCENETYPE_ISLAND_HEIST
					sData.playerPedClone[0].vSpawnPosition = <<-1754.7, 438.9, 127.8>>
					sData.playerPedClone[0].fSpawnHeading = 237.2271
					
					sData.playerPedClone[1].vSpawnPosition = <<-1755.7, 438.9, 127.8>>
					sData.playerPedClone[1].fSpawnHeading = 245.8436 
					
					sData.playerPedClone[2].vSpawnPosition = <<-1756.7, 438.9, 127.8>>
					sData.playerPedClone[2].fSpawnHeading = 242.1219
					
					sData.playerPedClone[3].vSpawnPosition = <<-1757.7, 438.9, 127.8>>
					sData.playerPedClone[3].fSpawnHeading = 238.3524
					
					sData.vPlayerPosForLoadingScene = sData.playerPedClone[sData.iMyScenePositionIndex].vSpawnPosition
					sData.fPlayerHeadingForLoadingScene = sData.playerPedClone[sData.iMyScenePositionIndex].fSpawnHeading
					
					sData.eWinnerAudioScene = eCAS_FADE_WHITE_SCENE
					
					sData.vFocusPosition = sData.playerPedClone[0].vSpawnPosition
					
					IF IS_PLAYER_SCTV(PLAYER_ID())
					OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bJoinedMissionAsSpectator)//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bJoinedMissionAsSpectator
						sData.vPlayerPosForLoadingScene = sData.playerPedClone[0].vSpawnPosition
						sData.fPlayerHeadingForLoadingScene = sData.playerPedClone[0].fSpawnHeading
					ENDIF
				BREAK
				#ENDIF
				
				CASE eHEISTWINNERSCENETYPE_CASINO_HEIST_ROOF
										
					sData.playerPedClone[0].vSpawnPosition = <<788.1093, 202.0296, 80.4982>>
					sData.playerPedClone[0].fSpawnHeading = 237.2271
					
					sData.playerPedClone[1].vSpawnPosition = <<787.3500, 200.9037, 80.4979>>
					sData.playerPedClone[1].fSpawnHeading = 245.8436 
					
					sData.playerPedClone[2].vSpawnPosition = <<786.5688, 199.3651, 80.5096>>
					sData.playerPedClone[2].fSpawnHeading = 242.1219
					
					sData.playerPedClone[3].vSpawnPosition = <<785.8018, 197.8395, 80.5189>>
					sData.playerPedClone[3].fSpawnHeading = 238.3524
					
					sData.vPlayerPosForLoadingScene = sData.playerPedClone[sData.iMyScenePositionIndex].vSpawnPosition
					sData.fPlayerHeadingForLoadingScene = sData.playerPedClone[sData.iMyScenePositionIndex].fSpawnHeading
					
					sData.eWinnerAudioScene = eCAS_FADE_WHITE_SCENE
					
					sData.vFocusPosition = sData.playerPedClone[0].vSpawnPosition
					
					IF IS_PLAYER_SCTV(PLAYER_ID())
					OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bJoinedMissionAsSpectator)//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bJoinedMissionAsSpectator
						sData.vPlayerPosForLoadingScene = sData.playerPedClone[0].vSpawnPosition
						sData.fPlayerHeadingForLoadingScene = sData.playerPedClone[0].fSpawnHeading
					ENDIF
					
				BREAK
				
				CASE eHEISTWINNERSCENETYPE_CASINO_HEIST_BAR
					sData.playerPedClone[0].vSpawnPosition = <<2726.3704, -381.3114, -49.9999>>
					sData.playerPedClone[0].fSpawnHeading = 264.5209
					
					sData.playerPedClone[1].vSpawnPosition = <<2726.8015, -383.3425, -49.9999>>
					sData.playerPedClone[1].fSpawnHeading = 305.0945
					
					sData.playerPedClone[2].vSpawnPosition = <<2726.7837, -385.0965, -49.9999>>
					sData.playerPedClone[2].fSpawnHeading = 221.9951
					
					sData.playerPedClone[3].vSpawnPosition = <<2727.0337, -386.7577, -49.9999>>
					sData.playerPedClone[3].fSpawnHeading = 289.0450
					
					sData.vPlayerPosForLoadingScene = sData.playerPedClone[sData.iMyScenePositionIndex].vSpawnPosition
					sData.fPlayerHeadingForLoadingScene = sData.playerPedClone[sData.iMyScenePositionIndex].fSpawnHeading
					
					sData.eWinnerAudioScene = eCAS_FADE_WHITE_SCENE
					
					sData.vFocusPosition = sData.playerPedClone[0].vSpawnPosition					
					
					IF IS_PLAYER_SCTV(PLAYER_ID())
					OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bJoinedMissionAsSpectator)//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bJoinedMissionAsSpectator
						sData.vPlayerPosForLoadingScene = sData.playerPedClone[0].vSpawnPosition
						sData.fPlayerHeadingForLoadingScene = sData.playerPedClone[0].fSpawnHeading
					ENDIF
				BREAK
				
				CASE eHEISTWINNERSCENETYPE_ROOFTOP
					
					sData.playerPedClone[0].vSpawnPosition = << 705.515, -964.901, 35.859 >>
					sData.playerPedClone[0].fSpawnHeading = 74.187
					
					sData.playerPedClone[1].vSpawnPosition = << 706.374, -964.040, 35.859 >>
					sData.playerPedClone[1].fSpawnHeading = 80.000 
					
					sData.playerPedClone[2].vSpawnPosition = << 706.166, -962.865, 35.859 >>
					sData.playerPedClone[2].fSpawnHeading = 90.000
					
					sData.playerPedClone[3].vSpawnPosition = << 705.842, -965.960, 35.859 >>
					sData.playerPedClone[3].fSpawnHeading = 70.652
					
					sData.vPlayerPosForLoadingScene = sData.playerPedClone[sData.iMyScenePositionIndex].vSpawnPosition
					sData.fPlayerHeadingForLoadingScene = sData.playerPedClone[sData.iMyScenePositionIndex].fSpawnHeading
					
					sData.eWinnerAudioScene = eCAS_PACIFIC_SCENE
					
					sData.vFocusPosition = sData.playerPedClone[0].vSpawnPosition
					
					IF IS_PLAYER_SCTV(PLAYER_ID())
					OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bJoinedMissionAsSpectator)//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bJoinedMissionAsSpectator
						sData.vPlayerPosForLoadingScene = sData.playerPedClone[0].vSpawnPosition
						sData.fPlayerHeadingForLoadingScene = sData.playerPedClone[0].fSpawnHeading
					ENDIF
					
				BREAK
				
				CASE eHEISTWINNERSCENETYPE_STRIP_CLUB
					
					sData.strInteriorName = "v_strip3"
					sData.strRoomName = "strp3mainrm"
					
					sData.playerPedClone[0].vSpawnPosition = <<115.3036, -1286.7184, 27.2615>>
					sData.playerPedClone[0].fSpawnHeading = 106.5682
					
					sData.playerPedClone[1].vSpawnPosition = <<114.7243, -1285.9166, 27.2625>>
					sData.playerPedClone[1].fSpawnHeading = 103.7247 
					
					sData.playerPedClone[2].vSpawnPosition = <<114.2871, -1284.9615, 27.2642>>
					sData.playerPedClone[2].fSpawnHeading = 135.9676
					
					sData.playerPedClone[3].vSpawnPosition = <<113.8246, -1284.0287, 27.2658>>
					sData.playerPedClone[3].fSpawnHeading = 152.4515
					
					sData.extraPed.vSpawnPosition = <<115.3036, -1286.7184, 27.2615>>
					sData.extraPed.fSpawnHeading = 106.5682
					sData.extraPed.eModel = S_F_Y_Stripper_01
					sData.extraPed.tl23_handle = "Bar_Maid"
					
					sData.object[HEIST_WINNER_SCENE_STRIP_GLASS].vSpawnPosition = <<115.3036, -1286.2184, 27.2615>>
					sData.object[HEIST_WINNER_SCENE_STRIP_GLASS].fSpawnHeading = 106.5682
					sData.object[HEIST_WINNER_SCENE_STRIP_GLASS].eModel = P_CS_Shot_Glass_2_S
					sData.object[HEIST_WINNER_SCENE_STRIP_GLASS].tl23_handle = "Glass"
					
					sData.object[HEIST_WINNER_SCENE_STRIP_GLASS_1].vSpawnPosition = <<115.3036, -1286.4184, 27.2615>>
					sData.object[HEIST_WINNER_SCENE_STRIP_GLASS_1].fSpawnHeading = 106.5682
					sData.object[HEIST_WINNER_SCENE_STRIP_GLASS_1].eModel = P_CS_Shot_Glass_2_S
					sData.object[HEIST_WINNER_SCENE_STRIP_GLASS_1].tl23_handle = "Glass_1"
					
					sData.object[HEIST_WINNER_SCENE_STRIP_GLASS_2].vSpawnPosition = <<115.3036, -1286.6184, 27.2615>>
					sData.object[HEIST_WINNER_SCENE_STRIP_GLASS_2].fSpawnHeading = 106.5682
					sData.object[HEIST_WINNER_SCENE_STRIP_GLASS_2].eModel = P_CS_Shot_Glass_2_S
					sData.object[HEIST_WINNER_SCENE_STRIP_GLASS_2].tl23_handle = "Glass_2"
					
					sData.object[HEIST_WINNER_SCENE_STRIP_GLASS_3].vSpawnPosition = <<115.3036, -1286.8184, 27.2615>>
					sData.object[HEIST_WINNER_SCENE_STRIP_GLASS_3].fSpawnHeading = 106.5682
					sData.object[HEIST_WINNER_SCENE_STRIP_GLASS_3].eModel = P_CS_Shot_Glass_2_S
					sData.object[HEIST_WINNER_SCENE_STRIP_GLASS_3].tl23_handle = "Glass_3"
					
					sData.object[HEIST_WINNER_SCENE_STRIP_WHISKEY].vSpawnPosition = <<115.3036, -1286.7184, 27.6615>>
					sData.object[HEIST_WINNER_SCENE_STRIP_WHISKEY].fSpawnHeading = 106.5682
					sData.object[HEIST_WINNER_SCENE_STRIP_WHISKEY].eModel = P_Whiskey_Bottle_S
					sData.object[HEIST_WINNER_SCENE_STRIP_WHISKEY].tl23_handle = "Whisky"
					
					sData.vPlayerPosForLoadingScene = sData.playerPedClone[sData.iMyScenePositionIndex].vSpawnPosition
					sData.fPlayerHeadingForLoadingScene = sData.playerPedClone[sData.iMyScenePositionIndex].fSpawnHeading
					
					SWITCH sData.iMyScenePositionIndex
						CASE 0
							sData.vPlayerPosForAfterScene = <<132.4727, -1304.2257, 28.2104>>
							sData.fPlayerHeadingForafterScene = 212.0879
						BREAK
						CASE 1
							sData.vPlayerPosForAfterScene = <<129.9843, -1302.3949, 28.2327>>
							sData.fPlayerHeadingForafterScene = 212.6229
						BREAK
						CASE 2
							sData.vPlayerPosForAfterScene = <<135.6028, -1303.9547, 28.1685>>
							sData.fPlayerHeadingForafterScene = 163.7941
						BREAK
						CASE 3
							sData.vPlayerPosForAfterScene = <<128.0055, -1305.1724, 28.2327>>
							sData.fPlayerHeadingForafterScene = 246.5913
						BREAK
					ENDSWITCH
					
					g_TransitionSessionNonResetVars.sGlobalCelebrationData.bBlockStripClubPedsForCongratsScreen = TRUE
					
					sData.eWinnerAudioScene = eCAS_HUMANE_SCENE
					
					sData.vFocusPosition = sData.playerPedClone[0].vSpawnPosition
					
					IF IS_PLAYER_SCTV(PLAYER_ID())
					OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bJoinedMissionAsSpectator)//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bJoinedMissionAsSpectator
						sData.vPlayerPosForLoadingScene = sData.playerPedClone[0].vSpawnPosition
						sData.fPlayerHeadingForLoadingScene = sData.playerPedClone[0].fSpawnHeading
					ENDIF
					
				BREAK
				
				CASE eHEISTWINNERSCENETYPE_BAR
					
					sData.strInteriorName = "v_rockclub"
					
					sData.playerPedClone[0].vSpawnPosition = <<-562.7222, 285.1675, 84.3764>>
					sData.playerPedClone[0].fSpawnHeading = 76.7772 
					
					sData.playerPedClone[1].vSpawnPosition = <<-562.6874, 286.8524, 84.3764>>
					sData.playerPedClone[1].fSpawnHeading = 101.7949
					
					sData.object[HEIST_WINNER_SCENE_BAR_BOTTLE].vSpawnPosition = <<-562.7222, 285.1675, 84.3764>>
					sData.object[HEIST_WINNER_SCENE_BAR_BOTTLE].fSpawnHeading = 253.0338
					sData.object[HEIST_WINNER_SCENE_BAR_BOTTLE].eModel = PROP_CS_BEER_BOT_01
					sData.object[HEIST_WINNER_SCENE_BAR_BOTTLE].tl23_handle = "MPH_Bottle"
					
					sData.object[HEIST_WINNER_SCENE_BAR_BOTTLE_1].vSpawnPosition = <<-562.7222, 285.1675, 84.7764>>
					sData.object[HEIST_WINNER_SCENE_BAR_BOTTLE_1].fSpawnHeading = 253.0338
					sData.object[HEIST_WINNER_SCENE_BAR_BOTTLE_1].eModel = PROP_CS_BEER_BOT_01
					sData.object[HEIST_WINNER_SCENE_BAR_BOTTLE_1].tl23_handle = "MPH_Bottle^1"
					
					sData.vPlayerPosForLoadingScene = sData.playerPedClone[sData.iMyScenePositionIndex].vSpawnPosition
					sData.fPlayerHeadingForLoadingScene = sData.playerPedClone[sData.iMyScenePositionIndex].fSpawnHeading
					
					sData.eWinnerAudioScene = eCAS_FLEECA_SCENE
					
					sData.vFocusPosition = sData.playerPedClone[0].vSpawnPosition
					
					IF IS_PLAYER_SCTV(PLAYER_ID())
					OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bJoinedMissionAsSpectator)//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bJoinedMissionAsSpectator
						sData.vPlayerPosForLoadingScene = sData.playerPedClone[0].vSpawnPosition
						sData.fPlayerHeadingForLoadingScene = sData.playerPedClone[0].fSpawnHeading
					ENDIF
					
				BREAK
				
				CASE eHEISTWINNERSCENETYPE_APARTMENT
					
					INITIALISE_APARTMENT_SCENE_DATA(sData)
					
				BREAK
				
				CASE eHEISTWINNERSCENETYPE_TREVOR
					
					sData.playerPedClone[0].vSpawnPosition = <<2825.8787, 4818.5977, 46.8845>>
					sData.playerPedClone[0].fSpawnHeading = 45.8519
					
					sData.playerPedClone[1].vSpawnPosition = <<2825.8787, 4818.5977, 46.8845>>
					sData.playerPedClone[1].fSpawnHeading = 11.7321 
					
					sData.playerPedClone[2].vSpawnPosition = <<2825.8787, 4818.5977, 46.8845>>
					sData.playerPedClone[2].fSpawnHeading = 81.2564
					
					sData.playerPedClone[3].vSpawnPosition = <<2825.8787, 4818.5977, 46.8845>>
					sData.playerPedClone[3].fSpawnHeading = 152.4515
					
					sData.extraPed.vSpawnPosition = <<2825.8787, 4818.5977, 46.8845>>
					sData.extraPed.fSpawnHeading = 303.5638
					sData.extraPed.eModel = player_two
					sData.extraPed.tl23_handle = "TREVOR"
					
					sData.vPlayerPosForLoadingScene = sData.extraPed.vSpawnPosition
					sData.fPlayerHeadingForLoadingScene = sData.extraPed.fSpawnHeading
					
					sData.eWinnerAudioScene = eCAS_SERIES_A_SCENE
					
					sData.vFocusPosition = sData.extraPed.vSpawnPosition
					
					IF IS_PLAYER_SCTV(PLAYER_ID())
					OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bJoinedMissionAsSpectator)//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bJoinedMissionAsSpectator
						sData.vPlayerPosForLoadingScene = sData.playerPedClone[0].vSpawnPosition
						sData.fPlayerHeadingForLoadingScene = sData.playerPedClone[0].fSpawnHeading
					ENDIF
					
					SWITCH sData.iMyScenePositionIndex
						CASE 0
							sData.vPlayerPosForAfterScene = <<2702.9031, 4335.1421, 44.8519>>
							sData.fPlayerHeadingForafterScene = 45.8519
						BREAK
						CASE 1
							sData.vPlayerPosForAfterScene = <<2698.0510, 4331.7847, 44.8526>>
							sData.fPlayerHeadingForafterScene = 11.7321 
						BREAK
						CASE 2
							sData.vPlayerPosForAfterScene = <<2705.3345, 4339.2905, 44.8519>>
							sData.fPlayerHeadingForafterScene = 81.2564
						BREAK
						CASE 3
							sData.vPlayerPosForAfterScene = <<113.8246, -1284.0287, 27.2658>>
							sData.fPlayerHeadingForafterScene = 152.4515
						BREAK
					ENDSWITCH
					
				BREAK
				
			ENDSWITCH
			
			#IF IS_DEBUG_BUILD
				
				// Setup widget values.
				sData.playerPedClone[0].vPedPosDebug = sData.playerPedClone[0].vSpawnPosition
				sData.playerPedClone[0].fPedHeadingDebug = sData.playerPedClone[0].fSpawnHeading
				sData.playerPedClone[1].vPedPosDebug = sData.playerPedClone[1].vSpawnPosition
				sData.playerPedClone[1].fPedHeadingDebug = sData.playerPedClone[1].fSpawnHeading
				sData.playerPedClone[2].vPedPosDebug = sData.playerPedClone[2].vSpawnPosition
				sData.playerPedClone[2].fPedHeadingDebug = sData.playerPedClone[2].fSpawnHeading
				sData.playerPedClone[3].vPedPosDebug = sData.playerPedClone[3].vSpawnPosition
				sData.playerPedClone[3].fPedHeadingDebug = sData.playerPedClone[3].fSpawnHeading
				
				// Print data values to logs.
				PRINTLN("[SAC] - [NETCELEBRATION] - [PMC] - scene data print")
				
				PRINTLN("[SAC] - [NETCELEBRATION] - [PMC] - scene data print - strInteriorName = ", sData.strInteriorName)
				
				PRINTLN("[SAC] - [NETCELEBRATION] - [PMC] - scene data print - iAptIndex = ", g_TransitionSessionNonResetVars.iAptIndex)
				
				PRINTLN("[SAC] - [NETCELEBRATION] - [PMC] - scene data print - vPlayerPosForLoadingScene = ", sData.vPlayerPosForLoadingScene)
				PRINTLN("[SAC] - [NETCELEBRATION] - [PMC] - scene data print - fPlayerHeadingForLoadingScene = ", sData.fPlayerHeadingForLoadingScene)
				
				PRINTLN("[SAC] - [NETCELEBRATION] - [PMC] - scene data print - playerPedClone[0].vSpawnPosition = ", sData.playerPedClone[0].vSpawnPosition)
				PRINTLN("[SAC] - [NETCELEBRATION] - [PMC] - scene data print - playerPedClone[0].fSpawnHeading", sData.playerPedClone[0].fSpawnHeading)
				PRINTLN("[SAC] - [NETCELEBRATION] - [PMC] - scene data print - playerPedClone[1].vSpawnPosition = ", sData.playerPedClone[1].vSpawnPosition)
				PRINTLN("[SAC] - [NETCELEBRATION] - [PMC] - scene data print - playerPedClone[1].fSpawnHeading", sData.playerPedClone[1].fSpawnHeading)
				PRINTLN("[SAC] - [NETCELEBRATION] - [PMC] - scene data print - playerPedClone[2].vSpawnPosition = ", sData.playerPedClone[2].vSpawnPosition)
				PRINTLN("[SAC] - [NETCELEBRATION] - [PMC] - scene data print - playerPedClone[2].fSpawnHeading", sData.playerPedClone[2].fSpawnHeading)
				PRINTLN("[SAC] - [NETCELEBRATION] - [PMC] - scene data print - playerPedClone[3].vSpawnPosition = ", sData.playerPedClone[3].vSpawnPosition)
				PRINTLN("[SAC] - [NETCELEBRATION] - [PMC] - scene data print - playerPedClone[3].fSpawnHeading", sData.playerPedClone[3].fSpawnHeading)
				
				PRINTLN("[SAC] - [NETCELEBRATION] - [PMC] - scene data print - vPlayerPosForAfterScene = ", sData.vPlayerPosForAfterScene)
				PRINTLN("[SAC] - [NETCELEBRATION] - [PMC] - scene data print - fPlayerHeadingForafterScene", sData.fPlayerHeadingForafterScene)
				
			#ENDIF
			
			SET_BIT(sData.iBitset, HEIST_WINNER_SCENE_INITIALISED_DATA)
			
		ENDIF
	
	ELSE
		
		PRINTLN("[SAC] - init winner scene data - iHeistType has invalid value. iHeistType = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iHeistType)
		
		g_TransitionSessionNonResetVars.sGlobalCelebrationData.iHeistType = ciCELEBRATION_HEIST_TYPE__PREP
		
		PRINTLN("[SAC] - init winner scene data - defaulted iHeistType to 1 (setup) so we can continue into freemode.")
		
		SCRIPT_ASSERT("iHeistType has invalid value. defaulted iHeistType to 1 (setup).")
		
	ENDIF
	
	RETURN IS_BIT_SET(sData.iBitset, HEIST_WINNER_SCENE_INITIALISED_DATA)
	
ENDFUNC

FUNC MODEL_NAMES GET_BACKUP_PED_MODEL(INT iBackupPed)
	
	SWITCH iBackupPed
		CASE 0
//			RETURN A_F_Y_VINEWOOD_01
			RETURN A_F_Y_GENHOT_01
		CASE 1
//			RETURN A_M_Y_VINEWOOD_01
			RETURN A_F_Y_GENHOT_01
	ENDSWITCH
	
	RETURN A_F_Y_GENHOT_01
	
ENDFUNC

PROC DELETE_HEIST_END_WINNER_SCENE_ENTITIES(STRUCT_HEIST_END_WINNER_SCENE_DATA &sData)
	
	INT iObj
	
	IF DOES_ENTITY_EXIST(sData.playerPedClone[0].pedId)
		DELETE_PED(sData.playerPedClone[0].pedId)
		SET_MODEL_AS_NO_LONGER_NEEDED(GET_BACKUP_PED_MODEL(0))
		PRINTLN("[SAC] - [NETCELEBRATION] - DELETE_HEIST_END_WINNER_SCENE_ENTITIES - deleted clone ped 0.")
	ENDIF
	
	IF DOES_ENTITY_EXIST(sData.playerPedClone[1].pedId)
		DELETE_PED(sData.playerPedClone[1].pedId)
		SET_MODEL_AS_NO_LONGER_NEEDED(GET_BACKUP_PED_MODEL(1))
		PRINTLN("[SAC] - [NETCELEBRATION] - DELETE_HEIST_END_WINNER_SCENE_ENTITIES - deleted clone ped 1.")
	ENDIF
	
	IF DOES_ENTITY_EXIST(sData.playerPedClone[2].pedId)
		DELETE_PED(sData.playerPedClone[2].pedId)
		SET_MODEL_AS_NO_LONGER_NEEDED(GET_BACKUP_PED_MODEL(2))
		PRINTLN("[SAC] - [NETCELEBRATION] - DELETE_HEIST_END_WINNER_SCENE_ENTITIES - deleted clone ped 2.")
	ENDIF
	
	IF DOES_ENTITY_EXIST(sData.playerPedClone[3].pedId)
		DELETE_PED(sData.playerPedClone[3].pedId)
		SET_MODEL_AS_NO_LONGER_NEEDED(GET_BACKUP_PED_MODEL(3))
		PRINTLN("[SAC] - [NETCELEBRATION] - DELETE_HEIST_END_WINNER_SCENE_ENTITIES - deleted clone ped 3.")
	ENDIF
	
	IF DOES_ENTITY_EXIST(sData.extraPed.pedId)
		DELETE_PED(sData.extraPed.pedId)
		SET_MODEL_AS_NO_LONGER_NEEDED(sData.extraPed.eModel)
		PRINTLN("[SAC] - [NETCELEBRATION] - DELETE_HEIST_END_WINNER_SCENE_ENTITIES - deleted extra ped.")
	ENDIF
	
	REPEAT HEIST_MAX_WINNER_SCENE_OBJECTS iObj
		IF DOES_ENTITY_EXIST(sData.object[iObj].entityId)
			DELETE_ENTITY(sData.object[iObj].entityId)
			SET_MODEL_AS_NO_LONGER_NEEDED(sData.object[iObj].eModel)
			PRINTLN("[SAC] - [NETCELEBRATION] - DELETE_HEIST_END_WINNER_SCENE_ENTITIES - deleted object ", iObj)
		ENDIF
	ENDREPEAT
	
ENDPROC

#IF IS_DEBUG_BUILD
PROC UPDATE_HEIST_END_WINNER_SCENE_PEDS_WITH_WIDGET_DATA(STRUCT_HEIST_END_WINNER_SCENE_DATA &sData)
	
	INT i
	
	REPEAT 4 i
		IF sData.playerPedClone[i].bOverridePedWorldData
			IF DOES_ENTITY_EXIST(sData.playerPedClone[i].pedId)
				IF NOT IS_ENTITY_DEAD(sData.playerPedClone[i].pedId)
					SET_ENTITY_COORDS(sData.playerPedClone[i].pedId, sData.playerPedClone[i].vPedPosDebug)
					SET_ENTITY_HEADING(sData.playerPedClone[i].pedId, sData.playerPedClone[i].fPedHeadingDebug)
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
ENDPROC
#ENDIF

PROC TRIGGER_POST_HEIST_POSITION_SYSTEMS()
	
	IF IS_PLAYER_SCTV(PLAYER_ID())
	OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bJoinedMissionAsSpectator)//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bJoinedMissionAsSpectator
		EXIT
	ENDIF
	
	IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPostMissionSceneId = SIS_POST_APARTMENT
		PRINTLN("[SAC] - TRIGGER_POST_HEIST_POSITION_SYSTEMS - iPostMissionSceneId = SIS_POST_APARTMENT.")
		#IF IS_DEBUG_BUILD
		INT i
		REPEAT COUNT_OF(g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPlayerNameHash) i
			PRINTLN("[RCC MISSION] - [SAC] - TRIGGER_POST_HEIST_POSITION_SYSTEMS - iPlayerNameHash[", i, "] = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPlayerNameHash[i])
			PRINTLN("[RCC MISSION] - [SAC] - TRIGGER_POST_HEIST_POSITION_SYSTEMS - strLeaderBoardPlayerName[", i, "] = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.strLeaderBoardPlayerName[i])
		ENDREPEAT
		#ENDIF
		CLEANUP_MP_CUTSCENE(TRUE, FALSE)
		TRIGGER_APARTMENT_HANDOVER(g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPlayerNameHash, (NOT g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingHeistEndWinnerScene))
	ELIF g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPostMissionSceneId = SIS_POST_STRIP_CLUB
		PRINTLN("[SAC] - TRIGGER_POST_HEIST_POSITION_SYSTEMS - iPostMissionSceneId = SIS_POST_STRIP_CLUB.")
		#IF IS_DEBUG_BUILD
		INT i
		REPEAT COUNT_OF(g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPlayerNameHash) i
			PRINTLN("[SAC] - TRIGGER_POST_HEIST_POSITION_SYSTEMS - iPlayerNameHash[", i, "] = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPlayerNameHash[i])
			PRINTLN("[RCC MISSION] - [SAC] - TRIGGER_POST_HEIST_POSITION_SYSTEMS - strLeaderBoardPlayerName[", i, "] = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.strLeaderBoardPlayerName[i])
		ENDREPEAT
		#ENDIF
		CLEANUP_MP_CUTSCENE(TRUE, FALSE)
//		g_TransitionSessionNonResetVars.sGlobalCelebrationData.enumPostMissionScenePossibleResult = PMSP_FAILED
		TRIGGER_STRIP_CLUB_POST_SCENE_SYSTEM(g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPlayerNameHash)
	ELSE
		#IF IS_DEBUG_BUILD
		INT i
		REPEAT COUNT_OF(g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPlayerNameHash) i
			PRINTLN("[SAC] - TRIGGER_POST_HEIST_POSITION_SYSTEMS - iPlayerNameHash[", i, "] = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPlayerNameHash[i])
			PRINTLN("[RCC MISSION] - [SAC] - TRIGGER_POST_HEIST_POSITION_SYSTEMS - strLeaderBoardPlayerName[", i, "] = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.strLeaderBoardPlayerName[i])
		ENDREPEAT
		#ENDIF
		SET_NO_LOADING_SCREEN(TRUE)
		PRINTLN("[SAC] - called SET_NO_LOADING_SCREEN(TRUE). At request of Neil Ferguson for 2230423.")
		START_SYNCED_INTERACTION_SCENE(g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPostMissionSceneId, g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPlayerNameHash)
		PRINTLN("[SAC] - TRIGGER_POST_HEIST_POSITION_SYSTEMS - iPostMissionSceneId != SIS_POST_STRIP_CLUB and != SIS_POST_APARTMENT, using START_SYNCED_INTERACTION_SCENE().")
		PRINTLN("[SAC] - TRIGGER_POST_HEIST_POSITION_SYSTEMS - iPostMissionSceneId", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPostMissionSceneId)
	ENDIF
	
	DISABLE_INTERACTION_MENU()
	SET_CELEBRATION_SCREEN_AS_ACTIVE(FALSE, FALSE)
	
	THEFEED_FLUSH_QUEUE()
	PRINTLN("[RCC MISSION] - [NETCELEBRATION] - [SAC] - called THEFEED_FLUSH_QUEUE.")
	
	#IF IS_DEBUG_BUILD
	PRINTLN("[SAC] - TRIGGER_POST_HEIST_POSITION_SYSTEMS has been called:")
	DEBUG_PRINTCALLSTACK()
	#ENDIF
	
ENDPROC

FUNC BOOL LOAD_INTERIOR_FOR_HEIST_END_WINNER_SCENE(STRUCT_HEIST_END_WINNER_SCENE_DATA &sData)
	
	#IF IS_DEBUG_BUILD
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		VECTOR vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
		PRINTLN("[SAC] - LOAD_INTERIOR_FOR_HEIST_END_WINNER_SCENE been called with player at pos ", vPlayerPos)
	ENDIF
	#ENDIF
	
	IF NOT sData.bActivatedRequiredIpls
		
		IF IS_PROPERTY_CUSTOM_APARTMENT(g_TransitionSessionNonResetVars.iAptIndex)
			ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY(g_TransitionSessionNonResetVars.iAptIndex, g_TransitionSessionNonResetVars.iApartmentVariation)
			PRINTLN("[SAC] - LOAD_INTERIOR_FOR_HEIST_END_WINNER_SCENE is for custom apartment, activated ipls with ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY(", g_TransitionSessionNonResetVars.iAptIndex, " ,", g_TransitionSessionNonResetVars.iApartmentVariation, ")")
		ELIF g_TransitionSessionNonResetVars.iAptIndex = PROPERTY_STILT_APT_8_A
			ACTIVATE_IPLS_FOR_STILT_APARTMENT(g_TransitionSessionNonResetVars.iAptIndex)
			PRINTLN("[SAC] - LOAD_INTERIOR_FOR_HEIST_END_WINNER_SCENE is for PROPERTY_STILT_APT_8_A, activated ipls with ACTIVATE_IPLS_FOR_STILT_APARTMENT(", g_TransitionSessionNonResetVars.iAptIndex, ")")
		ENDIF
		
		sData.bActivatedRequiredIpls = TRUE
		
	ELSE
		
		IF NOT IS_VALID_INTERIOR(sData.interiorIndex)
			PRINTLN("[SAC] - interior not valid yet, grabbing form coords ", sData.vPlayerPosForLoadingScene, ", interior type = ", sData.strInteriorName)
			sData.interiorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(sData.vPlayerPosForLoadingScene, sData.strInteriorName)
		ELSE
			IF IS_INTERIOR_CAPPED(sData.interiorIndex)
				CAP_INTERIOR(sData.interiorIndex, FALSE)
				sData.bInteriorNeedsReCapped = TRUE
				PRINTLN("[SAC] - Trying to load a capped interior,un-capping.")
			ENDIF
			IF IS_INTERIOR_DISABLED(sData.interiorIndex)
				DISABLE_INTERIOR(sData.interiorIndex, FALSE)
				sData.bInteriorNeedsDisabled = TRUE
				PRINTLN("[SAC] - Trying to load a disabled interior, re-enabling.")
			ENDIF
			PIN_INTERIOR_IN_MEMORY(sData.interiorIndex)
			IF IS_INTERIOR_READY(sData.interiorIndex)
				RETAIN_ENTITY_IN_INTERIOR(PLAYER_PED_ID(), sData.interiorIndex)
				PRINTLN("[SAC] - pinned interior into memory.")
				RETURN TRUE
			ELSE
				PRINTLN("[SAC] - pinning interior into memory.")
			ENDIF
		ENDIF
		
	ENDIF
		
	RETURN FALSE
	
ENDFUNC

FUNC STRING GET_MOCAP_CUTSCENE_NAME(STRUCT_HEIST_END_WINNER_SCENE_DATA &sData)
	
	SWITCH sData.eSceneType
		#IF FEATURE_HEIST_ISLAND
		CASE eHEISTWINNERSCENETYPE_ISLAND_HEIST
			RETURN "HS4F_DRP_CEL"
		BREAK
		#ENDIF
		CASE eHEISTWINNERSCENETYPE_CASINO_HEIST_ROOF
			RETURN "HS3_EXT"
		BREAK
		CASE eHEISTWINNERSCENETYPE_CASINO_HEIST_BAR
			RETURN "HS3_CEL_ARC"
		BREAK
		CASE eHEISTWINNERSCENETYPE_ROOFTOP
			RETURN "MPH_FIN_CEL_ROO"
		BREAK
		CASE eHEISTWINNERSCENETYPE_BAR
			RETURN "MPH_FIN_CEL_BAR"
		BREAK
		CASE eHEISTWINNERSCENETYPE_APARTMENT
			PRINTLN("[SAC] - [NETCELEBRATION] - [RCC BUSINESS] [KW CUTSCENES] GET_MOCAP_CUTSCENE_NAME g_TransitionSessionNonResetVars.iAptIndex = ", g_TransitionSessionNonResetVars.iAptIndex)
			PRINTLN("[SAC] - [NETCELEBRATION] - [RCC BUSINESS] [KW CUTSCENES] GET_MOCAP_CUTSCENE_NAME g_TransitionSessionNonResetVars.iApartmentVariation = ", g_TransitionSessionNonResetVars.iApartmentVariation)
			IF IS_HEIST_PROPERTY_TYPE_DLC(g_TransitionSessionNonResetVars.iAptIndex)
				PRINTLN("[RCC BUSINESS] [KW CUTSCENES] playing MPH_FIN_CEL_APT1")
				RETURN "MPH_FIN_CEL_APT1"
			ELIF IS_PROPERTY_CUSTOM_APARTMENT(g_TransitionSessionNonResetVars.iAptIndex)
				PRINTLN("[SAC] - [NETCELEBRATION] - [RCC BUSINESS] [KW CUTSCENES] - eHEISTWINNERSCENESTAGE_START_SCENE (IS_PROPERTY_CUSTOM_APARTMENT = TRUE), playing APA_FIN_CEL_APT4")
				RETURN "APA_FIN_CEL_APT4"
			ELIF IS_PROPERTY_STILT_APARTMENT(g_TransitionSessionNonResetVars.iAptIndex, PROPERTY_STILT_APT_5_BASE_A)
				PRINTLN("[SAC] - [NETCELEBRATION] - [RCC BUSINESS] [KW CUTSCENES] - eHEISTWINNERSCENESTAGE_START_SCENE (PROPERTY_STILT_APT_5_BASE_A = TRUE), playing APA_FIN_CEL_APT2")
				RETURN "APA_FIN_CEL_APT2"
			ELIF IS_PROPERTY_STILT_APARTMENT(g_TransitionSessionNonResetVars.iAptIndex, PROPERTY_STILT_APT_1_BASE_B)
				PRINTLN("[SAC] - [NETCELEBRATION] - [RCC BUSINESS] [KW CUTSCENES] - eHEISTWINNERSCENESTAGE_START_SCENE (PROPERTY_STILT_APT_1_BASE_B = TRUE), playing APA_FIN_CEL_APT3")
				RETURN "APA_FIN_CEL_APT3"
			ELSE
				PRINTLN("[SAC] - [NETCELEBRATION] - [RCC BUSINESS] [KW CUTSCENES] playing MPH_FIN_CEL_APT")
				RETURN "MPH_FIN_CEL_APT"
			ENDIF
		BREAK
		CASE eHEISTWINNERSCENETYPE_STRIP_CLUB
			RETURN "MPH_FIN_CEL_STR"
		BREAK
		CASE eHEISTWINNERSCENETYPE_TREVOR
			RETURN "MPH_FIN_CEL_TRE"
		BREAK
	ENDSWITCH
	
	RETURN ""
	
ENDFUNC

PROC SET_COMPONENT_VARIATIONS_FOR_CUTSCENE_PEDS()
	
	SWITCH g_TransitionSessionNonResetVars.sGlobalCelebrationData.iWinnerSceneType
		CASE FMMC_CELEBRATION_TYPE_ROOFTOP
		#IF FEATURE_HEIST_ISLAND
		CASE FMMC_CELEBRATION_TYPE_ISLAND_HEIST
		#ENDIF
		
			SET_CUTSCENE_ENTITY_STREAMING_FLAGS("MP_1", DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS)
			SET_CUTSCENE_ENTITY_STREAMING_FLAGS("MP_2", DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS)
			SET_CUTSCENE_ENTITY_STREAMING_FLAGS("MP_3", DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS)
			SET_CUTSCENE_ENTITY_STREAMING_FLAGS("MP_4", DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS)
		
		BREAK
		CASE FMMC_CELEBRATION_TYPE_BAR
			
			SET_CUTSCENE_ENTITY_STREAMING_FLAGS("MP_1", DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS)
			SET_CUTSCENE_ENTITY_STREAMING_FLAGS("MP_2", DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS)
			
		BREAK
		CASE FMMC_CELEBRATION_TYPE_STRIP_CLUB	
			
			SET_CUTSCENE_ENTITY_STREAMING_FLAGS("MP_1", DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS)
			SET_CUTSCENE_ENTITY_STREAMING_FLAGS("MP_2", DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS)
			SET_CUTSCENE_ENTITY_STREAMING_FLAGS("MP_3", DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS)
			SET_CUTSCENE_ENTITY_STREAMING_FLAGS("MP_4", DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS)
			SET_CUTSCENE_ENTITY_STREAMING_FLAGS("Bar_Maid", DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS)
		
		BREAK
		CASE FMMC_CELEBRATION_TYPE_APARTMENT 	
			
			SET_CUTSCENE_ENTITY_STREAMING_FLAGS("MP_1", DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS)
			SET_CUTSCENE_ENTITY_STREAMING_FLAGS("MP_2", DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS)
			SET_CUTSCENE_ENTITY_STREAMING_FLAGS("MP_3", DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS)
			SET_CUTSCENE_ENTITY_STREAMING_FLAGS("MP_4", DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS)
			
		BREAK
		CASE FMMC_CELEBRATION_TYPE_TREVOR
			
		BREAK
		CASE FMMC_CELEBRATION_TYPE_DEFAULT
			
			SET_CUTSCENE_ENTITY_STREAMING_FLAGS("MP_1", DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS)
			SET_CUTSCENE_ENTITY_STREAMING_FLAGS("MP_2", DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS)
			SET_CUTSCENE_ENTITY_STREAMING_FLAGS("MP_3", DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS)
			SET_CUTSCENE_ENTITY_STREAMING_FLAGS("MP_4", DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS)
			
		BREAK
	ENDSWITCH 
	
ENDPROC

FUNC BOOL HAS_MOCAP_CUTSCENE_LOADED(STRUCT_HEIST_END_WINNER_SCENE_DATA &sData)
	RETURN (sData.iLoadMocapStage = HEIST_WINNER_MOCAP_LOAD_FINISHED)
ENDFUNC

FUNC BOOL LOAD_WINNER_SCENE_MOCAP(STRUCT_HEIST_END_WINNER_SCENE_DATA &sData)
	
	STRING strCutscene = GET_MOCAP_CUTSCENE_NAME(sData)
	
	SWITCH sData.iLoadMocapStage
		
		CASE 0
			REMOVE_CUTSCENE()
			PRINTLN("[SAC] - called REMOVE_CUTSCENE.")
			sData.iLoadMocapStage++
		BREAK
		
		CASE 1
			IF NOT HAS_CUTSCENE_LOADED()
			AND NOT IS_CUTSCENE_ACTIVE()
				PRINTLN("[SAC] - HAS_CUTSCENE_LOADED = FALSE and IS_CUTSCENE_ACTIVE = FALSE.")
				sData.iLoadMocapStage++
			ENDIF
		BREAK
		
		CASE 2
			IF HAS_THIS_CUTSCENE_LOADED(strCutscene)
				PRINTLN("[SAC] - loaded cutscene ", strCutscene)
				sData.iLoadMocapStage = HEIST_WINNER_MOCAP_LOAD_FINISHED
			ELSE
				PRINTLN("[SAC] - requesting cutscene ", strCutscene)
				REQUEST_CUTSCENE(strCutscene)
				IF NOT IS_BIT_SET(iCelebrationGeneralBitset, CELEB_GENERAL_REQUESTED_CUTSCENE_MUSIC_EVENT)
					IF sHeistWinnerSceneData.eSceneType = eHEISTWINNERSCENETYPE_STRIP_CLUB
						ENABLE_PREPARE_CELEBRATION_STRIP_CLUB_MUSIC_EVENT()
					ELIF sHeistWinnerSceneData.eSceneType = eHEISTWINNERSCENETYPE_APARTMENT
						ENABLE_PREPARE_CELEBRATION_APARTMENT_MUSIC_EVENT()
					ENDIF
					PRINTLN("[SAC] - LOAD_WINNER_SCENE_MOCAP = requested prepare mmusic events for celebration winner scene.")
					SET_BIT(iCelebrationGeneralBitset, CELEB_GENERAL_REQUESTED_CUTSCENE_MUSIC_EVENT)
				ENDIF
				IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
					PRINTLN("[SAC] - CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY = TRUE for cutscene ", strCutscene)
					IF (g_TransitionSessionNonResetVars.sGlobalCelebrationData.iWinnerSceneType != FMMC_CELEBRATION_TYPE_TREVOR)
						PRINTLN("[SAC] - calling SET_COMPONENT_VARIATIONS_FOR_CUTSCENE_PEDS for cutscene ", strCutscene)
						SET_COMPONENT_VARIATIONS_FOR_CUTSCENE_PEDS()
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
	ENDSWITCH
	
	RETURN HAS_MOCAP_CUTSCENE_LOADED(sData)
	
ENDFUNC

PROC SET_LOCAL_PLAYER_FOR_MOCAP(BOOL bSetInvisible = TRUE, BOOL bClearTasks = TRUE)
	
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		IF bSetInvisible
			IF bClearTasks
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_CLEAR_TASKS | NSPC_REMOVE_FIRES | NSPC_REMOVE_EXPLOSIONS | NSPC_FREEZE_POSITION | NSPC_SET_INVISIBLE)
			ELSE
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_REMOVE_FIRES | NSPC_REMOVE_EXPLOSIONS | NSPC_FREEZE_POSITION | NSPC_SET_INVISIBLE)
			ENDIF
		ELSE
			IF bClearTasks
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_CLEAR_TASKS | NSPC_REMOVE_FIRES | NSPC_REMOVE_EXPLOSIONS | NSPC_FREEZE_POSITION)
			ELSE
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_REMOVE_FIRES | NSPC_REMOVE_EXPLOSIONS | NSPC_FREEZE_POSITION)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC PLACE_PLAYER_AT_SYNCED_INTERACTION_SCENE_COORDS()
	
	VECTOR vDestination
	
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			IF sHeistWinnerSceneData.eSceneType = eHEISTWINNERSCENETYPE_BAR // Special case so since post mission scene vector is inside an interior we need to cap, and we need to move the player out of it.
				vDestination = << -565.4, 273.1, 90.0 >>
				CLEAR_ROOM_FOR_ENTITY(PLAYER_PED_ID())
				PRINTLN("[SAC] - doing eHEISTWINNERSCENETYPE_BAR, setting vDestination = ", vDestination)
			ELIF sHeistWinnerSceneData.eSceneType = eHEISTWINNERSCENETYPE_TREVOR // url:bugstar:2208758 - [Series A - Finale] In the ending cutscene various foliage and textures pop into view. 
				vDestination = << 2828.7, 4810.4, 48.5 >>
				PRINTLN("[SAC] - iPostMissionSceneId = eHEISTWINNERSCENETYPE_TREVOR, setting player to trevors coords = ", vDestination)
			ELIF g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPostMissionSceneId = (-1)
				vDestination = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
				vDestination.z += 2.0 // Make sure we're not inside or below the ground.
				PRINTLN("[SAC] - iPostMissionSceneId = -1, grabbing players current coords (= 2.0 on the z) = ", vDestination)
			ELSE
				vDestination = GET_SYNCED_INTERACTION_SCENE_COORDS(g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPostMissionSceneId)
				vDestination.z += 2.0 // Make sure we're not inside or below the ground.
				PRINTLN("[SAC] - iPostMissionSceneId is valid, called GET_SYNCED_INTERACTION_SCENE_COORDS + 2.0m on the Z. Coords = ", vDestination)
			ENDIF
			NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_CLEAR_TASKS | NSPC_FREEZE_POSITION | NSPC_NO_COLLISION | NSPC_SET_INVISIBLE) 
			IF NOT IS_VECTOR_ZERO(vDestination)
				SET_ENTITY_COORDS(PLAYER_PED_ID(), vDestination, FALSE) 
				PRINTLN("[SAC] - set player to coords ", vDestination)
			ELSE
				PRINTLN("[SAC] - leaving player at current coords, vDestination = ", vDestination)
			ENDIF
			SET_BIT(iCelebrationGeneralBitset, CELEB_GENERAL_BITSET_COMPLETED_PLAYER_PRE_LOAD_WARP)
			PRINTLN("[SAC] - setting bit CELEB_GENERAL_BITSET_COMPLETED_PLAYER_PRE_LOAD_WARP.")
		ENDIF
	ENDIF
	
ENDPROC

PROC MAINTAIN_WARP_TO_POST_CELEB_STATS_SHARD_LOCATION()
	
	BOOL bLoadInterior
	BOOL bWarpToPostMissionSceneLocation = TRUE
	VECTOR vWarpToLocation
	BOOL bSnapToGround = TRUE
	BOOL bDoQuickWarp
	
	IF IS_BIT_SET(iCelebrationGeneralBitset, CELEB_GENERAL_BITSET_PRE_LOAD_WARP_TRIGGERED)
		
		IF NOT IS_BIT_SET(iCelebrationGeneralBitset, CELEB_GENERAL_BITSET_COMPLETED_PLAYER_PRE_LOAD_WARP)
			
			IF IS_BIT_SET(iCelebrationGeneralBitset, CELEB_GENERAL_COMPLETED_POINT_AT_PLAYER_CAMERA_CUT)
				
				IF ( g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingHeistEndWinnerScene AND NOT IS_FAKE_MULTIPLAYER_MODE_SET() )
					IF NOT IS_BIT_SET(sHeistWinnerSceneData.iBitset, HEIST_WINNER_SCENE_SET_FOCUS_POS)
						IF NOT IS_PLAYER_SCTV(PLAYER_ID())
						AND NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bJoinedMissionAsSpectator)//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bJoinedMissionAsSpectator
							SET_FOCUS_POS_AND_VEL(sHeistWinnerSceneData.vFocusPosition, << 0.0, 0.0, 0.0 >>)
							PRINTLN("[SAC] - MAINTAIN_WARP_TO_POST_CELEB_STATS_SHARD_LOCATION - set focus pos to ", sHeistWinnerSceneData.vFocusPosition)
						ELSE
							SET_SPECTATOR_OVERRIDE_COORDS(sHeistWinnerSceneData.vFocusPosition)
							PRINTLN("[SAC] - MAINTAIN_WARP_TO_POST_CELEB_STATS_SHARD_LOCATION - set spectator override coords to ", sHeistWinnerSceneData.vFocusPosition)
						ENDIF
						SET_GAME_PAUSES_FOR_STREAMING(FALSE)
						PRINTLN("[SAC] - MAINTAIN_WARP_TO_POST_CELEB_STATS_SHARD_LOCATION - SET_GAME_PAUSES_FOR_STREAMING(FALSE).")
						SET_BIT(sHeistWinnerSceneData.iBitset, HEIST_WINNER_SCENE_SET_FOCUS_POS)
					ENDIF
				ENDIF
				
				// If doing an end winner scene, don't warp to the post mission scene location.
				IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingHeistEndWinnerScene
					vWarpToLocation = sHeistWinnerSceneData.vFocusPosition
					bWarpToPostMissionSceneLocation = FALSE
				ELSE
					// If we need to go into an interior for a mocap, don't warp to the post mission scene location. 
					IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPostMissionSceneId = SIS_POST_APARTMENT	
					OR g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPostMissionSceneId = SIS_POST_STRIP_CLUB
						vWarpToLocation = sHeistWinnerSceneData.vPlayerPosForLoadingScene
						bWarpToPostMissionSceneLocation = FALSE
					ENDIF
				ENDIF
				
				IF bWarpToPostMissionSceneLocation
					
					IF NOT IS_PLAYER_SCTV(PLAYER_ID())
					AND NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bJoinedMissionAsSpectator)//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bJoinedMissionAsSpectator
						PLACE_PLAYER_AT_SYNCED_INTERACTION_SCENE_COORDS()
						PRINTLN("[SAC] - MAINTAIN_WARP_TO_POST_CELEB_STATS_SHARD_LOCATION - player not SCTV - completed warp to winner scene location. Setting bit CELEB_GENERAL_BITSET_COMPLETED_PLAYER_PRE_LOAD_WARP.")
						SET_BIT(iCelebrationGeneralBitset, CELEB_GENERAL_BITSET_COMPLETED_PLAYER_PRE_LOAD_WARP)
					ELSE
						PRINTLN("[SAC] - MAINTAIN_WARP_TO_POST_CELEB_STATS_SHARD_LOCATION - player is SCTV - completed warp to winner scene location. Setting bit CELEB_GENERAL_BITSET_COMPLETED_PLAYER_PRE_LOAD_WARP.")
						SET_BIT(iCelebrationGeneralBitset, CELEB_GENERAL_BITSET_COMPLETED_PLAYER_PRE_LOAD_WARP)
					ENDIf
					
				ELSE
					
					IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
						SWITCH iPreLoadStage
							CASE 0
								IF sHeistWinnerSceneData.eSceneType = eHEISTWINNERSCENETYPE_BAR
									bLoadInterior = TRUE
									PRINTLN("[SAC] - MAINTAIN_WARP_TO_POST_CELEB_STATS_SHARD_LOCATION - eSceneType = eHEISTWINNERSCENETYPE_BAR, setting bLoadInterior = TRUE")
								ENDIF
								IF sHeistWinnerSceneData.eSceneType = eHEISTWINNERSCENETYPE_STRIP_CLUB
									bLoadInterior = TRUE
									PRINTLN("[SAC] - MAINTAIN_WARP_TO_POST_CELEB_STATS_SHARD_LOCATION - eSceneType = eHEISTWINNERSCENETYPE_STRIP_CLUB, setting bLoadInterior = TRUE")
								ENDIF
								IF sHeistWinnerSceneData.eSceneType = eHEISTWINNERSCENETYPE_APARTMENT
								OR g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPostMissionSceneId = SIS_POST_APARTMENT
									IF NOT IS_PLAYER_SCTV(PLAYER_ID())
									AND NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bJoinedMissionAsSpectator)//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bJoinedMissionAsSpectator
										bLoadInterior = TRUE
										PRINTLN("[SAC] - MAINTAIN_WARP_TO_POST_CELEB_STATS_SHARD_LOCATION - eSceneType = eHEISTWINNERSCENETYPE_APARTMENT, setting bLoadInterior = TRUE")
									ELSE
										PRINTLN("[SAC] - MAINTAIN_WARP_TO_POST_CELEB_STATS_SHARD_LOCATION - eSceneType = eHEISTWINNERSCENETYPE_APARTMENT, player is SCTV, leaving bLoadInterior = FALSE")
									ENDIF
								ENDIF
								IF NOT bLoadInterior
									iPreLoadStage++
									PRINTLN("[SAC] - MAINTAIN_WARP_TO_POST_CELEB_STATS_SHARD_LOCATION - NOT bLoadInterior, set iPreLoadStage = ", iPreLoadStage)
								ELSE
									IF LOAD_INTERIOR_FOR_HEIST_END_WINNER_SCENE(sHeistWinnerSceneData)
										iPreLoadStage++
										PRINTLN("[SAC] - MAINTAIN_WARP_TO_POST_CELEB_STATS_SHARD_LOCATION - LOAD_INTERIOR_FOR_HEIST_END_WINNER_SCENE = TRUE, set iPreLoadStage = ", iPreLoadStage)
									ENDIF
								ENDIF
							BREAK
							CASE 1
								IF IS_PLAYER_SCTV(PLAYER_ID())
								OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bJoinedMissionAsSpectator)//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bJoinedMissionAsSpectator
									PRINTLN("[SAC] - MAINTAIN_WARP_TO_POST_CELEB_STATS_SHARD_LOCATION - IS_PLAYER_SCTV = TRUE, setting bit CELEB_GENERAL_BITSET_COMPLETED_PLAYER_PRE_LOAD_WARP.")
									SET_BIT(iCelebrationGeneralBitset, CELEB_GENERAL_BITSET_COMPLETED_PLAYER_PRE_LOAD_WARP)
								ELSE
									IF sHeistWinnerSceneData.eSceneType = eHEISTWINNERSCENETYPE_APARTMENT
									OR g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPostMissionSceneId = SIS_POST_APARTMENT
//										bDoQuickWarp = TRUE
										bSnapToGround = FALSE
										PRINTLN("[SAC] - MAINTAIN_WARP_TO_POST_CELEB_STATS_SHARD_LOCATION - not snapping player to ground for warp.") //2220835
									ENDIF
									IF NET_WARP_TO_COORD(vWarpToLocation, sHeistWinnerSceneData.fPlayerHeadingForLoadingScene, FALSE, FALSE, TRUE, TRUE, FALSE, bDoQuickWarp, bSnapToGround, TRUE)
										PRINTLN("[SAC] - MAINTAIN_WARP_TO_POST_CELEB_STATS_SHARD_LOCATION - completed warp to winner scene location. Setting bit CELEB_GENERAL_BITSET_COMPLETED_PLAYER_PRE_LOAD_WARP.")
										FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
										SET_BIT(iCelebrationGeneralBitset, CELEB_GENERAL_BITSET_COMPLETED_PLAYER_PRE_LOAD_WARP)
										IF sHeistWinnerSceneData.eSceneType = eHEISTWINNERSCENETYPE_APARTMENT // url:bugstar:2226484 - ST
										OR sHeistWinnerSceneData.eSceneType = eHEISTWINNERSCENETYPE_STRIP_CLUB
											SET_BIT(iCelebrationGeneralBitset, CELEB_GENERAL_BITSET_FREEZE_MICROPHONE)
										ENDIF
										iPreLoadStage++
										PRINTLN("[SAC] - MAINTAIN_WARP_TO_POST_CELEB_STATS_SHARD_LOCATION - completed warp, set iPreLoadStage = ", iPreLoadStage)
									ELSE
										PRINTLN("[SAC] - MAINTAIN_WARP_TO_POST_CELEB_STATS_SHARD_LOCATION - waiting on NET_WARP_TO_COORD.")
									ENDIF
								ENDIF
							BREAK
							CASE 2
								// Done. 
							BREAK
						ENDSWITCH
					ENDIF
					
				ENDIF
				
			ENDIF
			
		ENDIF
		
	ENDIF
	
ENDPROC

PROC MAINTAIN_DRAW_BLACK_RECTANGLE_FOR_FADED_MOCAP()
	
	IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDrawBlackRectangle[0]
		DRAW_RECT(0.5, 0.5, 3.0, 3.0, 0, 0, 0, 255)
		g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDrawBlackRectangle[1] = TRUE
	ENDIF
	
ENDPROC

PROC MAINTAIN_CLONE_EVERYONE_FOR_HEIST_CELEBRATION_SCREENS(STRUCT_HEIST_END_WINNER_SCENE_DATA &sData)
	
	INT iCreatedCount
	INT iPlayer
	PLAYER_INDEX playerId
	PED_INDEX pedId
	
	VECTOR vPos = <<0,0,0>>
	PED_INDEX piLocalPlayerPed = PLAYER_PED_ID()
	
	SWITCH iCelebrationCloningStage
		
		CASE 0
			
			IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingHeistEndWinnerScene
				IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bStreamingRequestsComplete
					iCelebrationCloningStage++
					PRINTLN("[RCC MISSION] - [PMC] - [NETCELEBRATION] - [SAC] - MAINTAIN_CLONE_EVERYONE_FOR_HEIST_CELEBRATION_SCREENS - bDoingHeistEndWinnerScene = TRUE. Going to stage ", iCelebrationCloningStage)
				ENDIF
			ENDIF
			
		BREAK
		
		CASE 1
			
			IF (g_TransitionSessionNonResetVars.sGlobalCelebrationData.iWinnerSceneType != FMMC_CELEBRATION_TYPE_TREVOR)
				
				REPEAT NUM_NETWORK_PLAYERS iPlayer
					IF IS_NET_PLAYER_OK(INT_TO_NATIVE(PLAYER_INDEX, iPlayer), FALSE)
						playerId = INT_TO_NATIVE(PLAYER_INDEX, iPlayer)
						pedId = GET_PLAYER_PED(playerId)
						PRINTLN("[RCC MISSION] - [PMC] - [NETCELEBRATION] - [SAC] - MAINTAIN_CLONE_EVERYONE_FOR_HEIST_CELEBRATION_SCREENS - player ", iPlayer, " is net ok.")
						IF NOT IS_PLAYER_SCTV(playerId)
						AND NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerId)].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bJoinedMissionAsSpectator)//GlobalplayerBD_FM[NATIVE_TO_INT(playerId)].bJoinedMissionAsSpectator
							IF IS_ENTITY_ALIVE(piLocalPlayerPed)
								PRINTLN("[RCC MISSION] - [PMC] - [NETCELEBRATION] - [SAC] - MAINTAIN_CLONE_EVERYONE_FOR_HEIST_CELEBRATION_SCREENS - local player ped is alive, getting coords")
								vPos = GET_ENTITY_COORDS(piLocalPlayerPed)
							ENDIF
							CREATE_CLONE_OF_PED_AT_COORDS(sData.playerPedClone[iCreatedCount].pedId, pedId, vPos + <<0.0,0.0,100.0>>, 0.0, iPlayer, TRUE)
							iCreatedCount++
							PRINTLN("[RCC MISSION] - [PMC] - [NETCELEBRATION] - [SAC] - MAINTAIN_CLONE_EVERYONE_FOR_HEIST_CELEBRATION_SCREENS - created clone of player ", iPlayer)
						ENDIF
					ENDIF
				ENDREPEAT
				
				iCelebrationCloningStage++
				PRINTLN("[RCC MISSION] - [PMC] - [NETCELEBRATION] - [SAC] - MAINTAIN_CLONE_EVERYONE_FOR_HEIST_CELEBRATION_SCREENS - created clones of all players. Going to stage ", iCelebrationCloningStage)
				
			ELSE
				
				iCelebrationCloningStage++
				PRINTLN("[RCC MISSION] - [PMC] - [NETCELEBRATION] - [SAC] - MAINTAIN_CLONE_EVERYONE_FOR_HEIST_CELEBRATION_SCREENS - cut scene is Trevor one, not cloning any players. Going to stage ", iCelebrationCloningStage)
				
			ENDIF
			
		BREAK
		
		CASE 2
			
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bCloningComplete = TRUE
			
		BREAK
		
	ENDSWITCH
	
ENDPROC

PROC SET_PED_BACKUP_PED_VARIATIONS(STRUCT_HEIST_END_WINNER_SCENE_DATA &sData, INT iBackupPed)
	
	SWITCH iBackupPed
		CASE 0
			// A_F_Y_VINEWOOD_01
			SET_PED_COMPONENT_VARIATION(sData.playerPedClone[iBackupPed].pedId, PED_COMP_HEAD, 0, 1)
		    SET_PED_COMPONENT_VARIATION(sData.playerPedClone[iBackupPed].pedId, PED_COMP_BERD, 0, 0)
		    SET_PED_COMPONENT_VARIATION(sData.playerPedClone[iBackupPed].pedId, PED_COMP_HAIR, 1, 1)
		    SET_PED_COMPONENT_VARIATION(sData.playerPedClone[iBackupPed].pedId, PED_COMP_TORSO, 1, 0)
		    SET_PED_COMPONENT_VARIATION(sData.playerPedClone[iBackupPed].pedId, PED_COMP_LEG, 1, 0)
		    SET_PED_COMPONENT_VARIATION(sData.playerPedClone[iBackupPed].pedId, PED_COMP_HAND, 0, 0)
		    SET_PED_COMPONENT_VARIATION(sData.playerPedClone[iBackupPed].pedId, PED_COMP_FEET, 0, 0)
		    SET_PED_COMPONENT_VARIATION(sData.playerPedClone[iBackupPed].pedId, PED_COMP_TEETH, 0, 0)
		    SET_PED_COMPONENT_VARIATION(sData.playerPedClone[iBackupPed].pedId, PED_COMP_SPECIAL, 0, 0)
		    SET_PED_COMPONENT_VARIATION(sData.playerPedClone[iBackupPed].pedId, PED_COMP_SPECIAL2, 0, 0)
		   	SET_PED_COMPONENT_VARIATION(sData.playerPedClone[iBackupPed].pedId, PED_COMP_DECL, 0, 0)
		    SET_PED_COMPONENT_VARIATION(sData.playerPedClone[iBackupPed].pedId, PED_COMP_JBIB, 0, 0)
		BREAK
		CASE 1
			// A_M_Y_VINEWOOD_01
			SET_PED_COMPONENT_VARIATION(sData.playerPedClone[iBackupPed].pedId, PED_COMP_HEAD, 0, 1)
		    SET_PED_COMPONENT_VARIATION(sData.playerPedClone[iBackupPed].pedId, PED_COMP_BERD, 0, 0)
		    SET_PED_COMPONENT_VARIATION(sData.playerPedClone[iBackupPed].pedId, PED_COMP_HAIR, 0, 0)
		    SET_PED_COMPONENT_VARIATION(sData.playerPedClone[iBackupPed].pedId, PED_COMP_TORSO, 0, 1)
		    SET_PED_COMPONENT_VARIATION(sData.playerPedClone[iBackupPed].pedId, PED_COMP_LEG, 0, 1)
		    SET_PED_COMPONENT_VARIATION(sData.playerPedClone[iBackupPed].pedId, PED_COMP_HAND, 0, 0)
		    SET_PED_COMPONENT_VARIATION(sData.playerPedClone[iBackupPed].pedId, PED_COMP_FEET, 0, 0)
		    SET_PED_COMPONENT_VARIATION(sData.playerPedClone[iBackupPed].pedId, PED_COMP_TEETH, 0, 0)
		    SET_PED_COMPONENT_VARIATION(sData.playerPedClone[iBackupPed].pedId, PED_COMP_SPECIAL, 0, 1)
		    SET_PED_COMPONENT_VARIATION(sData.playerPedClone[iBackupPed].pedId, PED_COMP_SPECIAL2, 0, 0)
		   	SET_PED_COMPONENT_VARIATION(sData.playerPedClone[iBackupPed].pedId, PED_COMP_DECL, 0, 0)
		    SET_PED_COMPONENT_VARIATION(sData.playerPedClone[iBackupPed].pedId, PED_COMP_JBIB, 0, 0)
		BREAK
		CASE 2
			// A_F_Y_GENHOT_01
			SET_PED_COMPONENT_VARIATION(sData.playerPedClone[iBackupPed].pedId, PED_COMP_HEAD, 0, 1)
		    SET_PED_COMPONENT_VARIATION(sData.playerPedClone[iBackupPed].pedId, PED_COMP_BERD, 0, 0)
		    SET_PED_COMPONENT_VARIATION(sData.playerPedClone[iBackupPed].pedId, PED_COMP_HAIR, 0, 0)
		    SET_PED_COMPONENT_VARIATION(sData.playerPedClone[iBackupPed].pedId, PED_COMP_TORSO, 0, 0)
		    SET_PED_COMPONENT_VARIATION(sData.playerPedClone[iBackupPed].pedId, PED_COMP_LEG, 0, 2)
		    SET_PED_COMPONENT_VARIATION(sData.playerPedClone[iBackupPed].pedId, PED_COMP_HAND, 0, 0)
		    SET_PED_COMPONENT_VARIATION(sData.playerPedClone[iBackupPed].pedId, PED_COMP_FEET, 0, 0)
		    SET_PED_COMPONENT_VARIATION(sData.playerPedClone[iBackupPed].pedId, PED_COMP_TEETH, 0, 0)
		    SET_PED_COMPONENT_VARIATION(sData.playerPedClone[iBackupPed].pedId, PED_COMP_SPECIAL, 1, 1)
		    SET_PED_COMPONENT_VARIATION(sData.playerPedClone[iBackupPed].pedId, PED_COMP_SPECIAL2, 0, 0)
		   	SET_PED_COMPONENT_VARIATION(sData.playerPedClone[iBackupPed].pedId, PED_COMP_DECL, 0, 0)
		    SET_PED_COMPONENT_VARIATION(sData.playerPedClone[iBackupPed].pedId, PED_COMP_JBIB, 0, 0)
		BREAK
		CASE 3
			// A_F_Y_GENHOT_01
			SET_PED_COMPONENT_VARIATION(sData.playerPedClone[iBackupPed].pedId, PED_COMP_HEAD, 0, 0)
		    SET_PED_COMPONENT_VARIATION(sData.playerPedClone[iBackupPed].pedId, PED_COMP_BERD, 0, 0)
		    SET_PED_COMPONENT_VARIATION(sData.playerPedClone[iBackupPed].pedId, PED_COMP_HAIR, 1, 1)
		    SET_PED_COMPONENT_VARIATION(sData.playerPedClone[iBackupPed].pedId, PED_COMP_TORSO, 0, 2)
		    SET_PED_COMPONENT_VARIATION(sData.playerPedClone[iBackupPed].pedId, PED_COMP_LEG, 0, 1)
		    SET_PED_COMPONENT_VARIATION(sData.playerPedClone[iBackupPed].pedId, PED_COMP_HAND, 0, 0)
		    SET_PED_COMPONENT_VARIATION(sData.playerPedClone[iBackupPed].pedId, PED_COMP_FEET, 0, 0)
		    SET_PED_COMPONENT_VARIATION(sData.playerPedClone[iBackupPed].pedId, PED_COMP_TEETH, 0, 0)
		    SET_PED_COMPONENT_VARIATION(sData.playerPedClone[iBackupPed].pedId, PED_COMP_SPECIAL, 0, 0)
		    SET_PED_COMPONENT_VARIATION(sData.playerPedClone[iBackupPed].pedId, PED_COMP_SPECIAL2, 0, 0)
		   	SET_PED_COMPONENT_VARIATION(sData.playerPedClone[iBackupPed].pedId, PED_COMP_DECL, 0, 0)
		    SET_PED_COMPONENT_VARIATION(sData.playerPedClone[iBackupPed].pedId, PED_COMP_JBIB, 0, 0)
		BREAK
	ENDSWITCH
ENDPROC

FUNC INT GET_NUM_PLAYER_PEDS_REQUIRED_FOR_SCENE(STRUCT_HEIST_END_WINNER_SCENE_DATA &sData)
	
	SWITCH sData.eSceneType
		CASE eHEISTWINNERSCENETYPE_APARTMENT
			RETURN NUM_PLAYERS_NEEDED_FOR_APARTMENT_SCENE
		CASE eHEISTWINNERSCENETYPE_BAR
			RETURN NUM_PLAYERS_NEEDED_FOR_BAR_SCENE
		CASE eHEISTWINNERSCENETYPE_ROOFTOP
			RETURN NUM_PLAYERS_NEEDED_FOR_ROOFTOP_SCENE
		CASE eHEISTWINNERSCENETYPE_STRIP_CLUB
			RETURN NUM_PLAYERS_NEEDED_FOR_STRIP_SCENE
		CASE eHEISTWINNERSCENETYPE_TREVOR
			RETURN NUM_PLAYERS_NEEDED_FOR_TREVOR_SCENE
		CASE eHEISTWINNERSCENETYPE_CASINO_HEIST_ROOF
		CASE eHEISTWINNERSCENETYPE_CASINO_HEIST_BAR
			RETURN NUM_PLAYERS_NEEDED_FOR_CASINO_HEIST
		#IF FEATURE_HEIST_ISLAND
		CASE eHEISTWINNERSCENETYPE_ISLAND_HEIST
			RETURN NUM_PLAYERS_NEEDED_FOR_ISLAND_HEIST
		#ENDIF
	ENDSWITCH
	
	RETURN NUM_PLAYERS_NEEDED_FOR_STRIP_SCENE
	
ENDFUNC

FUNC BOOL CREATE_HEIST_END_WINNER_SCENE_ENTITIES(STRUCT_HEIST_END_WINNER_SCENE_DATA &sData)
	
	INT i, iObj, iNumObjsToCreate, iNumExtraPedsToCreate, iObjsCreated, iNumExtraPedsCreated//, iNumPlayerClonesNeeded
	
	IF NOT IS_BIT_SET(sHeistWinnerSceneData.iBitset, HEIST_WINNER_SCENE_CREATED_ENTITIES)
		
		IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bCloningComplete
		
			IF sData.eSceneType = eHEISTWINNERSCENETYPE_TREVOR
			OR sData.eSceneType = eHEISTWINNERSCENETYPE_CASINO_HEIST_ROOF
			OR sData.eSceneType = eHEISTWINNERSCENETYPE_CASINO_HEIST_BAR
				sData.bCreatedPlayerPedClones = TRUE
				PRINTLN("[NETCELEBRATION] - [SAC] - winner scene = eHEISTWINNERSCENETYPE_TREVOR, skipping creating clones of players. Only Trevor is in the scene.")
			#IF FEATURE_HEIST_ISLAND
			ELIF sData.eSceneType = eHEISTWINNERSCENETYPE_ISLAND_HEIST
				IF DOES_ENTITY_EXIST(sData.playerPedClone[COMPANION_PED_ID].pedId)
					sData.bCreatedPlayerPedClones = TRUE
					PRINTLN("[NETCELEBRATION] - [SAC] - winner scene = eHEISTWINNERSCENETYPE_ISLAND_HEIST, skipping creating clones of players. This is handled elsewhere.")
				ELSE
					IF sData.eCompanionPedModel = DUMMY_MODEL_FOR_SCRIPT
						sData.bCreatedPlayerPedClones = TRUE
						PRINTLN("[NETCELEBRATION] - [SAC] - winner scene = eHEISTWINNERSCENETYPE_ISLAND_HEIST, skipping creating clones of players. Failed to clone companion.")
						SCRIPT_ASSERT("Heist celebration - Failed to clone companion")
					ELSE
						//We do not have another player with us on the heist so we need to create someone to celebrate with
						IF REQUEST_LOAD_MODEL(sData.eCompanionPedModel)
							sData.playerPedClone[COMPANION_PED_ID].pedId = CREATE_PED(PEDTYPE_MISSION, sData.eCompanionPedModel, sData.playerPedClone[1].vSpawnPosition, sData.playerPedClone[1].fSpawnHeading, FALSE, FALSE)
							
							IF DOES_ENTITY_EXIST(sData.playerPedClone[COMPANION_PED_ID].pedId)
								SET_ENTITY_INVINCIBLE(sData.playerPedClone[COMPANION_PED_ID].pedId, TRUE)
								SET_MODEL_AS_NO_LONGER_NEEDED(sData.eCompanionPedModel)
								sData.bCreatedPlayerPedClones = TRUE
								PRINTLN("[NETCELEBRATION] - [SAC] - winner scene = eHEISTWINNERSCENETYPE_ISLAND_HEIST, skipping creating clones of players. Created a clone of companion ped.")
							ENDIF
						ELSE
							PRINTLN("[NETCELEBRATION] - [SAC] - winner scene = eHEISTWINNERSCENETYPE_ISLAND_HEIST, Loading companion ped: ", sData.eCompanionPedModel)
						ENDIF
					ENDIF
				ENDIF
			#ENDIF
			ELSE
				
				IF NOT sData.bCreatedPlayerPedClones
					
					iNumPlayersInScene = 0
					
					REPEAT 4 i
						IF NOT DOES_ENTITY_EXIST(sData.playerPedClone[i].pedId)
							IF i <= (GET_NUM_PLAYER_PEDS_REQUIRED_FOR_SCENE(sData) - 1)
								IF REQUEST_LOAD_MODEL(GET_BACKUP_PED_MODEL(i))
									sData.playerPedClone[i].pedId = CREATE_PED(PEDTYPE_MISSION, GET_BACKUP_PED_MODEL(i), sData.playerPedClone[i].vSpawnPosition, sData.playerPedClone[i].fSpawnHeading, FALSE, FALSE)
	//								SET_PED_BACKUP_PED_VARIATIONS(sData, i)
									SET_ENTITY_INVINCIBLE(sData.playerPedClone[i].pedId, TRUE)
								ENDIF
							ENDIF
						ENDIF
					ENDREPEAT
					
					REPEAT 4 i
						IF DOES_ENTITY_EXIST(sData.playerPedClone[i].pedId)
							IF NOT IS_ENTITY_DEAD(sData.playerPedClone[i].pedId)
								SET_ENTITY_COORDS(sData.playerPedClone[i].pedId, sData.playerPedClone[i].vSpawnPosition + <<0.0,0.0,1.0>>)
								SET_ENTITY_HEADING(sData.playerPedClone[i].pedId, sData.playerPedClone[i].fSpawnHeading)
								PRINTLN("[NETCELEBRATIONS] - [SAC] - moved cloned ped ", i, " since they exist. Set to coords ", 
											sData.playerPedClone[i].vSpawnPosition, 
											", and heading ", 
											sData.playerPedClone[i].fSpawnHeading)
								iNumPlayersInScene++
							ENDIF
						ELSE
							
						ENDIF
					ENDREPEAT
					
					IF iNumPlayersInScene = GET_NUM_PLAYER_PEDS_REQUIRED_FOR_SCENE(sData)
						sData.bCreatedPlayerPedClones = TRUE
					ENDIF
					
				ENDIF
				
			ENDIF
			
			IF NOT ARE_VECTORS_EQUAL(<< 0.0, 0.0, 0.0 >>, sData.extraPed.vSpawnPosition)
				iNumExtraPedsToCreate++
				IF NOT DOES_ENTITY_EXIST(sData.extraPed.pedId)
					IF REQUEST_LOAD_MODEL(sData.extraPed.eModel)
						sData.extraPed.pedId = CREATE_PED(PEDTYPE_MISSION, sData.extraPed.eModel, sData.extraPed.vSpawnPosition, sData.extraPed.fSpawnHeading, FALSE, FALSE)
						IF sData.eSceneType = eHEISTWINNERSCENETYPE_STRIP_CLUB
							SET_PED_COMPONENT_VARIATION(sData.extraPed.pedId, PED_COMP_HEAD, 0, 0)
						    SET_PED_COMPONENT_VARIATION(sData.extraPed.pedId, PED_COMP_BERD, 0, 0)
						    SET_PED_COMPONENT_VARIATION(sData.extraPed.pedId, PED_COMP_HAIR, 2, 1)
						    SET_PED_COMPONENT_VARIATION(sData.extraPed.pedId, PED_COMP_TORSO, 1, 2)
						    SET_PED_COMPONENT_VARIATION(sData.extraPed.pedId, PED_COMP_LEG, 1, 1)
						    SET_PED_COMPONENT_VARIATION(sData.extraPed.pedId, PED_COMP_HAND, 0, 0)
						    SET_PED_COMPONENT_VARIATION(sData.extraPed.pedId, PED_COMP_FEET, 0, 0)
						    SET_PED_COMPONENT_VARIATION(sData.extraPed.pedId, PED_COMP_TEETH, 0, 0)
						    SET_PED_COMPONENT_VARIATION(sData.extraPed.pedId, PED_COMP_SPECIAL, 0, 1)
						    SET_PED_COMPONENT_VARIATION(sData.extraPed.pedId, PED_COMP_SPECIAL2, 0, 0)
						   	SET_PED_COMPONENT_VARIATION(sData.extraPed.pedId,  PED_COMP_DECL, 0, 0)
						    SET_PED_COMPONENT_VARIATION(sData.extraPed.pedId, PED_COMP_JBIB, 0, 1)
						ENDIF
						PRINTLN("[NETCELEBRATIONS] - [SAC] - created extra ped at coords ", sData.extraPed.vSpawnPosition, " and heading ", sData.extraPed.fSpawnHeading)
					ENDIF
				ELSE
					iNumExtraPedsCreated++
				ENDIF
			ENDIF
			
			REPEAT HEIST_MAX_WINNER_SCENE_OBJECTS iObj
				IF NOT ARE_VECTORS_EQUAL(<< 0.0, 0.0, 0.0 >>, sData.object[iObj].vSpawnPosition)
					iNumObjsToCreate++
					IF NOT DOES_ENTITY_EXIST(sData.object[iObj].entityId)
						IF REQUEST_LOAD_MODEL(sData.object[iObj].eModel)
							sData.object[iObj].entityId = CREATE_OBJECT(sData.object[iObj].eModel, sData.object[iObj].vSpawnPosition, FALSE, FALSE)
							PRINTLN("[NETCELEBRATIONS] - [SAC] - created scene object ", iObj, " ped at coords ", sData.object[iObj].vSpawnPosition)
						ENDIF
					ELSE
						iObjsCreated++
					ENDIF
				ENDIF
			ENDREPEAT
			
			IF (iNumObjsToCreate = iObjsCreated)
			AND (iNumExtraPedsToCreate = iNumExtraPedsCreated)
			AND sData.bCreatedPlayerPedClones
				 SET_BIT(sHeistWinnerSceneData.iBitset, HEIST_WINNER_SCENE_CREATED_ENTITIES)
			ELSE
				PRINTLN("[NETCELEBRATION] - [SAC] - iNumObjsToCreate = ", iNumObjsToCreate)
				PRINTLN("[NETCELEBRATION] - [SAC] - iObjsCreated = ", iObjsCreated)
				PRINTLN("[NETCELEBRATION] - [SAC] - iNumExtraPedsToCreate = ", iNumExtraPedsToCreate)
				PRINTLN("[NETCELEBRATION] - [SAC] - iNumExtraPedsCreated = ", iNumExtraPedsCreated)
				PRINTLN("[NETCELEBRATION] - [SAC] - sData.bCreatedPlayerPedClones = ", sData.bCreatedPlayerPedClones)
			ENDIF
		
		ELSE
		
			PRINTLN("[NETCELEBRATION] - [SAC] - cannot create entities, cloning not complete.")
		
		ENDIF
		
	ENDIF
	
	RETURN IS_BIT_SET(sHeistWinnerSceneData.iBitset, HEIST_WINNER_SCENE_CREATED_ENTITIES)
	
ENDFUNC

PROC REGISTER_PLAYER_PED_FOR_MOCAP(PED_INDEX ped, INT& iRegisterCount)

	TEXT_LABEL_23 sPlayerString
	
	IF NOT IS_PED_INJURED(ped)
		sPlayerString = "MP_"
		sPlayerString += iRegisterCount+1
		SET_ENTITY_VISIBLE(ped, TRUE)
		REGISTER_ENTITY_FOR_CUTSCENE(ped, sPlayerString, CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)	
		
		PRINTLN("[NETCELEBRATIONS] - [SAC] - registered player ped ", sPlayerString, " with args (CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME).")
		iRegisterCount++
    ENDIF 
	
ENDPROC

PROC REGISTER_EXTRA_PED_FOR_MOCAP(PED_INDEX ped, STRING strHandle)
	
	IF NOT IS_PED_INJURED(ped)
		SET_ENTITY_VISIBLE(ped, TRUE)
		REGISTER_ENTITY_FOR_CUTSCENE(ped, strHandle, CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)
		PRINTLN("[NETCELEBRATIONS] - [SAC] - registered extra ped ", strHandle, " with args (CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME).")
    ENDIF 
	
ENDPROC

PROC REGISTER_OBJECT_FOR_MOCAP(ENTITY_INDEX entity, STRING strHandle)
	
	IF DOES_ENTITY_EXIST(entity)
		IF NOT IS_ENTITY_DEAD(entity)
			SET_ENTITY_VISIBLE(entity, TRUE)
			REGISTER_ENTITY_FOR_CUTSCENE(entity, strHandle, CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)
			PRINTLN("[NETCELEBRATIONS] - [SAC] - registered object ", strHandle, " with args (CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME).")
		ENDIF
	ENDIF
	
ENDPROC

PROC REMOVE_PED_FROM_MOCAP(INT iPed)
	
	TEXT_LABEL_23 sPlayerString
	
	iPed += 1
	sPlayerString = "MP_"
	sPlayerString += iPed
	REGISTER_ENTITY_FOR_CUTSCENE(NULL, sPlayerString, CU_DONT_ANIMATE_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)
	PRINTLN("[NETCELEBRATIONS] - [SAC] - removed player ped ", sPlayerString, " by registering them as NULL entity with args (CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME).")
	
ENDPROC

PROC REGISTER_MOCAP_PEDS(STRUCT_HEIST_END_WINNER_SCENE_DATA &sData)
	
	// Add peds to mocap.
	IF (sData.eSceneType != eHEISTWINNERSCENETYPE_TREVOR)
		REGISTER_PLAYER_PED_FOR_MOCAP(sData.playerPedClone[0].pedId, sData.iMocapRegisteredPedsCount)
		REGISTER_PLAYER_PED_FOR_MOCAP(sData.playerPedClone[1].pedId, sData.iMocapRegisteredPedsCount)
		REGISTER_PLAYER_PED_FOR_MOCAP(sData.playerPedClone[2].pedId, sData.iMocapRegisteredPedsCount)
		REGISTER_PLAYER_PED_FOR_MOCAP(sData.playerPedClone[3].pedId, sData.iMocapRegisteredPedsCount)
	ENDIF
	
	REGISTER_EXTRA_PED_FOR_MOCAP(sData.extraPed.pedId, sData.extraPed.tl23_handle)
	
	// Remove unregistered peds from mocap.
	IF (sData.eSceneType = eHEISTWINNERSCENETYPE_BAR)
		IF sData.iMocapRegisteredPedsCount = 0
			REMOVE_PED_FROM_MOCAP(0)
			REMOVE_PED_FROM_MOCAP(1)
		ELIF sData.iMocapRegisteredPedsCount = 1
			REMOVE_PED_FROM_MOCAP(1)
		ENDIF
	ELIF (sData.eSceneType != eHEISTWINNERSCENETYPE_TREVOR)
		IF sData.iMocapRegisteredPedsCount = 0
			REMOVE_PED_FROM_MOCAP(0)
			REMOVE_PED_FROM_MOCAP(1)
			REMOVE_PED_FROM_MOCAP(2)
			REMOVE_PED_FROM_MOCAP(3)
		ELIF sData.iMocapRegisteredPedsCount = 1
			REMOVE_PED_FROM_MOCAP(1)
			REMOVE_PED_FROM_MOCAP(2)
			REMOVE_PED_FROM_MOCAP(3)
		ELIF sData.iMocapRegisteredPedsCount = 2
			REMOVE_PED_FROM_MOCAP(2)
			REMOVE_PED_FROM_MOCAP(3)
		ELIF sData.iMocapRegisteredPedsCount = 3
			REMOVE_PED_FROM_MOCAP(3)
		ENDIF
	ENDIF
	
ENDPROC

PROC REGISTER_MOCAP_OBJECTS(STRUCT_HEIST_END_WINNER_SCENE_DATA &sData)
	
	INT i
	
	REPEAT HEIST_MAX_WINNER_SCENE_OBJECTS i
		REGISTER_OBJECT_FOR_MOCAP(sData.object[i].entityId, sData.object[i].tl23_handle)
	ENDREPEAT
	
ENDPROC

FUNC BOOL IS_MOCAP_FINISHED()
	
	IF NOT IS_CUTSCENE_PLAYING()
		RETURN TRUE
	ENDIF
	
	IF (GET_CUTSCENE_TOTAL_DURATION() - GET_CUTSCENE_TIME()) <= 250
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC SET_POST_MISSION_VECTOR_SPAWN_LOCATION()
	FLOAT fRadius = 100
	IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.fPostMissionPosSearchRadius != 0
		fRadius = g_TransitionSessionNonResetVars.sPostMissionCleanupData.fPostMissionPosSearchRadius
	ENDIF
	
	SETUP_SPECIFIC_SPAWN_LOCATION(	g_TransitionSessionNonResetVars.sPostMissionCleanupData.vPostMissionPos, 0.0,
									fRadius, TRUE, 1, FALSE, FALSE, 10, DEFAULT, g_TransitionSessionNonResetVars.sPostMissionCleanupData.bPostMissionPosIgnoreExclusionZones)
	PRINTLN("[PMC] - [SAC] - SET_POST_MISSION_VECTOR_SPAWN_LOCATION - setup specific spawn near location. Position = ", g_TransitionSessionNonResetVars.sPostMissionCleanupData.vPostMissionPos, ", search radius = ", g_TransitionSessionNonResetVars.sPostMissionCleanupData.fPostMissionPosSearchRadius, ", visible checks = TRUE, min dis from coords = 1, consider interiors = FALSE, near roads = FALSE, min dis from other players = 65.")
	
	SET_PLAYER_WILL_SPAWN_FACING_COORDS(g_TransitionSessionNonResetVars.sPostMissionCleanupData.vPostMissionPointOfInterest, TRUE, FALSE)
	PRINTLN("[PMC] - [SAC] - SET_POST_MISSION_VECTOR_SPAWN_LOCATION - setup player will spoawn facing coords ", g_TransitionSessionNonResetVars.sPostMissionCleanupData.vPostMissionPointOfInterest)
	
	// No being placed inside the grounds of the Humane Labs.
	SET_MISSION_SPAWN_OCCLUSION_ANGLED_AREA(<<3410.697998,3751.923828,9.634556>>, <<3657.413818,3700.305420,72.006607>>, 250.000000, 0, TRUE)
	PRINTLN("[PMC] - [SAC] - called SET_MISSION_SPAWN_OCCLUSION_ANGLED_AREA(<<3410.697998,3751.923828,9.634556>>, <<3657.413818,3700.305420,72.006607>>, 250.000000) - call 2.")
	
ENDPROC

FUNC BOOL SHOULD_DRAW_CONGRATS_CHARD()
	
	IF NOT sHeistWinnerSceneData.bDrawCongratsShard
		
		IF DOES_ENTITY_EXIST(sHeistWinnerSceneData.playerPedClone[0].pedId)
			IF NOT IS_ENTITY_DEAD(sHeistWinnerSceneData.playerPedClone[0].pedId)
				IF HAS_ANIM_EVENT_FIRED(sHeistWinnerSceneData.playerPedClone[0].pedId, GET_HASH_KEY("CongratsSplash"))
					sHeistWinnerSceneData.bDrawCongratsShard = TRUE
					PRINTLN("[SAC] - ped 0, CongratsSplash event has fired.")
				ENDIF
				IF HAS_ANIM_EVENT_FIRED(sHeistWinnerSceneData.playerPedClone[0].pedId, GET_HASH_KEY("CongratSplash"))
					sHeistWinnerSceneData.bDrawCongratsShard = TRUE
					PRINTLN("[SAC] - ped 0, CongratSplash event has fired.")
				ENDIF
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(sHeistWinnerSceneData.playerPedClone[1].pedId)
			IF NOT IS_ENTITY_DEAD(sHeistWinnerSceneData.playerPedClone[1].pedId)
				IF HAS_ANIM_EVENT_FIRED(sHeistWinnerSceneData.playerPedClone[1].pedId, GET_HASH_KEY("CongratsSplash"))
					sHeistWinnerSceneData.bDrawCongratsShard = TRUE
					PRINTLN("[SAC] - ped 1, CongratsSplash event has fired.")
				ENDIF
				IF HAS_ANIM_EVENT_FIRED(sHeistWinnerSceneData.playerPedClone[1].pedId, GET_HASH_KEY("CongratSplash"))
					sHeistWinnerSceneData.bDrawCongratsShard = TRUE
					PRINTLN("[SAC] - ped 1, CongratSplash event has fired.")
				ENDIF
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(sHeistWinnerSceneData.playerPedClone[2].pedId)
			IF NOT IS_ENTITY_DEAD(sHeistWinnerSceneData.playerPedClone[2].pedId)
				IF HAS_ANIM_EVENT_FIRED(sHeistWinnerSceneData.playerPedClone[2].pedId, GET_HASH_KEY("CongratsSplash"))
					sHeistWinnerSceneData.bDrawCongratsShard = TRUE
					PRINTLN("[SAC] - ped 2, CongratsSplash event has fired.")
				ENDIF
				IF HAS_ANIM_EVENT_FIRED(sHeistWinnerSceneData.playerPedClone[2].pedId, GET_HASH_KEY("CongratSplash"))
					sHeistWinnerSceneData.bDrawCongratsShard = TRUE
					PRINTLN("[SAC] - ped 2, CongratSplash event has fired.")
				ENDIF
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(sHeistWinnerSceneData.playerPedClone[3].pedId)
			IF NOT IS_ENTITY_DEAD(sHeistWinnerSceneData.playerPedClone[3].pedId)
				IF HAS_ANIM_EVENT_FIRED(sHeistWinnerSceneData.playerPedClone[3].pedId, GET_HASH_KEY("CongratsSplash"))
					sHeistWinnerSceneData.bDrawCongratsShard = TRUE
					PRINTLN("[SAC] - ped 3, CongratsSplash event has fired.")
				ENDIF
				IF HAS_ANIM_EVENT_FIRED(sHeistWinnerSceneData.playerPedClone[3].pedId, GET_HASH_KEY("CongratSplash"))
					sHeistWinnerSceneData.bDrawCongratsShard = TRUE
					PRINTLN("[SAC] - ped 3, CongratSplash event has fired.")
				ENDIF
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(sHeistWinnerSceneData.extraPed.pedId)
			IF NOT IS_ENTITY_DEAD(sHeistWinnerSceneData.extraPed.pedId)
				IF HAS_ANIM_EVENT_FIRED(sHeistWinnerSceneData.extraPed.pedId, GET_HASH_KEY("CongratsSplash"))
					sHeistWinnerSceneData.bDrawCongratsShard = TRUE
					PRINTLN("[SAC] - extra ped, CongratsSplash event has fired.")
				ENDIF
				IF HAS_ANIM_EVENT_FIRED(sHeistWinnerSceneData.extraPed.pedId, GET_HASH_KEY("CongratSplash"))
					sHeistWinnerSceneData.bDrawCongratsShard = TRUE
					PRINTLN("[SAC] - extra ped, CongratSplash event has fired.")
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF
	
	RETURN sHeistWinnerSceneData.bDrawCongratsShard
	
ENDFUNC

PROC MAINTAIN_TREVOR_HEAD_BLOOD()
	
	IF NOT sHeistWinnerSceneData.extraPed.bAppliedHeadBlood
		IF DOES_ENTITY_EXIST(sHeistWinnerSceneData.extraPed.pedId)
			IF NOT IS_ENTITY_DEAD(sHeistWinnerSceneData.extraPed.pedId)
				IF HAS_ANIM_EVENT_FIRED(sHeistWinnerSceneData.extraPed.pedId, GET_HASH_KEY("forehead_blood"))
					//APPLY_PED_BLOOD_SPECIFIC(sHeistWinnerSceneData.extraPed.pedId, ENUM_TO_INT(PDZ_HEAD), 0.500, 0.677, 144.815, 0.660, 2, 0.00, "SCR_TrevorTreeBang") 
					APPLY_PED_DAMAGE_PACK(sHeistWinnerSceneData.extraPed.pedId, "SCR_TrevorTreeBang", 1.0, 1.0)
					sHeistWinnerSceneData.extraPed.bAppliedHeadBlood = TRUE
					PRINTLN("[SAC] - anim event forehead_blood has fired, called APPLY_PED_DAMAGE_PACK(sHeistWinnerSceneData.extraPed.pedId, SCR_TrevorTreeBang, 1.0, 1.0). ")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC SET_PLAYERS_INVISIBLE_THIS_FRAME()
	
	INT i
	
	SET_LOCAL_PLAYER_INVISIBLE_LOCALLY(TRUE)
	
	REPEAT NUM_NETWORK_PLAYERS i
		IF IS_NET_PLAYER_OK(INT_TO_NATIVE(PLAYER_INDEX, i), FALSE)
			SET_PLAYER_INVISIBLE_LOCALLY(INT_TO_NATIVE(PLAYER_INDEX, i), TRUE)
		ENDIF
	ENDREPEAT
	
ENDPROC

FUNC BOOL WARP_TO_OUTSIDE_APARTMENT(STRUCT_HEIST_END_WINNER_SCENE_DATA &sData)
	
	RETURN NET_WARP_TO_COORD(sData.vPlayerPosForAfterScene, sData.fPlayerHeadingForafterScene,FALSE, FALSE, TRUE, TRUE, FALSE, FALSE, TRUE, FALSE)
	
ENDFUNC

FUNC STRING GET_CELEBRATION_SCREEN_NAME()
	
	IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingPilotSchoolCelebration
		RETURN "ch"
	ENDIF
	
	RETURN "SUMMARY"
	
ENDFUNC

PROC MAINTAIN_GET_APARTMENT_INTERIOR_INSTANCE_INDEX_EARLY()
	
	IF IS_PLAYER_SCTV(PLAYER_ID())
	OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bJoinedMissionAsSpectator)//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bJoinedMissionAsSpectator
		EXIT
	ENDIF
	
	IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPostMissionSceneId = SIS_POST_APARTMENT		
		IF NOT IS_VECTOR_ZERO(sHeistWinnerSceneData.vPlayerPosForLoadingScene)
			IF NOT IS_VALID_INTERIOR(apartmentInteriorIndex)
				apartmentInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(sHeistWinnerSceneData.vPlayerPosForLoadingScene, sHeistWinnerSceneData.strInteriorName)
				PRINTLN("[SAC] - MAINTAIN_GET_APARTMENT_INTERIOR_INSTANCE_INDEX_EARLY - grabbed apartment interior.")
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

// url:bugstar:2226484
PROC MAINTAIN_FREEZE_MICROPHONE()
	
	IF IS_BIT_SET(iCelebrationGeneralBitset, CELEB_GENERAL_BITSET_FREEZE_MICROPHONE)
		
		// url:bugstar:2255042
		SWITCH iFreezeMicrophoneDelay
			CASE 0
				PRINTLN("[SAC] [FREEZE_MICROPHONE] - Microphone is ***not***being Frozen within MAINTAIN_FREEZE_MICROPHONE")
				iFreezeMicrophoneDelay++
			BREAK
			
			CASE 1
				PRINTLN("[SAC] [FREEZE_MICROPHONE] - Microphone is ***not*** being Frozen within MAINTAIN_FREEZE_MICROPHONE")
				iFreezeMicrophoneDelay++
			BREAK
			
			CASE 2
				PRINTLN("[SAC] [FREEZE_MICROPHONE] - Microphone is ***not*** being Frozen within MAINTAIN_FREEZE_MICROPHONE")
				iFreezeMicrophoneDelay++
			BREAK
			
			CASE 3
				PRINTLN("[SAC] [FREEZE_MICROPHONE] - Microphone is being Frozen within MAINTAIN_FREEZE_MICROPHONE")
				FREEZE_MICROPHONE()
				IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPostMissionSceneId = SIS_POST_APARTMENT	
					bUseHeistTransitionInviteMic = TRUE // Set the apartment microphone to freeze too, so there's no gaps with it unfreezing for a few frames.
				ENDIF
			BREAK
		ENDSWITCH	
		
		#IF IS_DEBUG_BUILD
			VECTOR vecTemp1 = GET_ENTITY_COORDS(PLAYER_PED_ID())
			VECTOR vecTemp2 = GET_FINAL_RENDERED_CAM_COORD()		
			PRINTLN("[SAC] [FREEZE_MICROPHONE] - Player's current position for Microphone freeze: ", vecTemp1)
			PRINTLN("[SAC] [FREEZE_MICROPHONE] - GET_FINAL_RENDERED_CAM_COORD for Microphone freeze: ", vecTemp2)	
		#ENDIF
		
	ENDIF
ENDPROC

FUNC BOOL STAND_ALONE_CELEBRATION_IS_PLAYER_IN_APARTMENT_INTERIOR()
	
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		IF IS_VALID_INTERIOR(apartmentInteriorIndex)
			IF GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) = apartmentInteriorIndex
				RETURN TRUE
			ENDIF
		ELSE
			apartmentInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(sHeistWinnerSceneData.vPlayerPosForLoadingScene, sHeistWinnerSceneData.strInteriorName)
		ENDIF
	ENDIF
	
	IF IS_VECTOR_ZERO(sHeistWinnerSceneData.vPlayerPosForLoadingScene)
		RETURN FALSE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL STAND_ALONE_CELEBRATION_IS_PLAYER_RUNNING_APARTMENT_INTERIOR_SCRIPT()
	
	IF IS_PLAYER_IN_MP_PROPERTY(PLAYER_ID(), FALSE)
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_PLAYER_USING_PROPERTY)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL STAND_ALONE_CELEBRATION_IS_PLAYER_IN_AN_APARTMENT()
	
	IF STAND_ALONE_CELEBRATION_IS_PLAYER_RUNNING_APARTMENT_INTERIOR_SCRIPT()
		PRINTLN("[SAC] - STAND_ALONE_CELEBRATION_IS_PLAYER_IN_AN_APARTMENT - STAND_ALONE_CELEBRATION_IS_PLAYER_RUNNING_APARTMENT_INTERIOR_SCRIPT = TRUE.")
		RETURN TRUE
	ENDIF
	
	IF STAND_ALONE_CELEBRATION_IS_PLAYER_IN_APARTMENT_INTERIOR()
		PRINTLN("[SAC] - STAND_ALONE_CELEBRATION_IS_PLAYER_IN_AN_APARTMENT - STAND_ALONE_CELEBRATION_IS_PLAYER_IN_APARTMENT_INTERIOR = TRUE.")
		RETURN TRUE
	ENDIF
	
	PRINTLN("[SAC] - STAND_ALONE_CELEBRATION_IS_PLAYER_IN_AN_APARTMENT - RETURNING FALSE.")
	RETURN FALSE
	
ENDFUNC

PROC SET_WINNER_SCENE_INTERIOR_TO_NORMAL_FREEMODE_STATE()
	
	IF IS_VALID_INTERIOR(sHeistWinnerSceneData.interiorIndex)
		IF IS_INTERIOR_READY(sHeistWinnerSceneData.interiorIndex)
			UNPIN_INTERIOR(sHeistWinnerSceneData.interiorIndex)
			PRINTLN("[SAC] - un-pinned interior.")
		ENDIF
		IF sHeistWinnerSceneData.bInteriorNeedsReCapped
			IF NOT IS_INTERIOR_CAPPED(sHeistWinnerSceneData.interiorIndex)
				CAP_INTERIOR(sHeistWinnerSceneData.interiorIndex, TRUE)
				PRINTLN("[SAC] - re-capping interior.")
			ENDIF
		ENDIF
		IF sHeistWinnerSceneData.bInteriorNeedsDisabled
			IF IS_INTERIOR_DISABLED(sHeistWinnerSceneData.interiorIndex)
				DISABLE_INTERIOR(sHeistWinnerSceneData.interiorIndex, TRUE)
				PRINTLN("[SAC] - re-disabling interior.")
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL CAN_START_WINNER_SCENE()
	
	BOOL bCanStart = TRUE
	
	IF NOT IS_BIT_SET(sHeistWinnerSceneData.iBitset, HEIST_WINNER_SCENE_CREATED_ENTITIES)
		PRINTLN("[SAC] - CAN_START_WINNER_SCENE = FALSE, HEIST_WINNER_SCENE_CREATED_ENTITIES bit not set.")
		bCanStart = FALSE
	ENDIF
	
	IF NOT IS_BIT_SET(iCelebrationGeneralBitset, CELEB_GENERAL_BITSET_COMPLETED_PLAYER_PRE_LOAD_WARP)
		PRINTLN("[SAC] - CAN_START_WINNER_SCENE = FALSE, CELEB_GENERAL_BITSET_COMPLETED_PLAYER_PRE_LOAD_WARP bit not set.")
		bCanStart = FALSE
	ENDIF
	
	IF NOT IS_BIT_SET(iCelebrationGeneralBitset, CELEB_GENERAL_BITSET_MOCAP_PRE_LOAD_COMPLETE)
		PRINTLN("[SAC] - CAN_START_WINNER_SCENE = FALSE, CELEB_GENERAL_BITSET_MOCAP_PRE_LOAD_COMPLETE bit not set.")
		bCanStart = FALSE
	ENDIF
	
	IF NOT HAS_MOCAP_CUTSCENE_LOADED(sHeistWinnerSceneData)
		PRINTLN("[SAC] - CAN_START_WINNER_SCENE = FALSE, HAS_MOCAP_CUTSCENE_LOADED = FALSE")
		bCanStart = FALSE
	ENDIF
	
	RETURN bCanStart 
	
ENDFUNC

///PURPOSE: Should turn off all nightvision-related things
PROC CELEB_TURN_NIGHTVISION_OFF()
	
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
		REMOVE_MP_HEIST_GEAR(PLAYER_PED_ID(), HEIST_GEAR_NIGHTVISION)
	ENDIF
	IF GET_REQUESTINGNIGHTVISION()
	OR GET_USINGNIGHTVISION()
		ENABLE_NIGHTVISION(VISUALAID_OFF)
	ENDIF
	
	DISABLE_NIGHT_VISION_CONTROLLER(TRUE) // Added under instruction of Alwyn for B*2196822
	
ENDPROC

PROC SETUP_JOINED_MISSION_AS_SPECTATOR_WARP()

	INT iTemp
	iTemp = GET_RANDOM_INT_IN_RANGE(0, 5)
	VECTOR vWarpLoc
	FLOAT fWarpHeading
	
	SWITCH iTemp
		CASE 0
			vWarpLoc = <<-664.0847, 9.2700, 38.1992>>
			fWarpHeading = 153.2955 
		BREAK
		CASE 1
			vWarpLoc = <<-224.8966, -38.2854, 48.5775>>
			fWarpHeading = 140.1033 
		BREAK
		CASE 2
			vWarpLoc = <<-292.0316, -1455.0347, 30.3251>>
			fWarpHeading = 305.0253 
		BREAK
		CASE 3
			vWarpLoc = <<-1227.5524, -1169.9088, 6.6146>>
			fWarpHeading = 324.8094 
		BREAK
		CASE 4
			vWarpLoc = <<-1759.1628, -426.1342, 41.8913>>
			fWarpHeading = 271.1274 
		BREAK
	ENDSWITCH
	
	SETUP_SPECIFIC_SPAWN_LOCATION(vWarpLoc, fWarpHeading, 250, TRUE, 1, FALSE, TRUE)
	PRINTLN("[SAC] - SETUP_CREATOR_POST_MISSION_WARP_LOCATION - setup specific spawn near location. Position = ", vWarpLoc)
	
	// No being placed inside the grounds of the Humane Labs.
	SET_MISSION_SPAWN_OCCLUSION_ANGLED_AREA(<<3410.697998,3751.923828,9.634556>>, <<3657.413818,3700.305420,72.006607>>, 250.000000, 0, TRUE)
	PRINTLN("[PMC] - [SAC] - called SET_MISSION_SPAWN_OCCLUSION_ANGLED_AREA(<<3410.697998,3751.923828,9.634556>>, <<3657.413818,3700.305420,72.006607>>, 250.000000) - call 2.")

ENDPROC

FUNC BOOL LOAD_SIMPLE_INTERIOR_SYSTEM_FOR_POST_HEIST_MOCAP(STRUCT_HEIST_END_WINNER_SCENE_DATA &sData)
	IF sData.eTargetInteriorID != SIMPLE_INTERIOR_INVALID
		IF NOT sData.bPinnedAndRequestedInterior
			INTERIOR_INSTANCE_INDEX interiorID = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<2730.0, -380.0, -50.0>>, "ch_DLC_Arcade")
			
			IF PIN_INTERIOR_IN_MEMORY_VALIDATE(interiorID)
				PRINTLN("[SAC] - LOAD_SIMPLE_INTERIOR_SYSTEM_FOR_POST_HEIST_MOCAP - Pinned ch_DLC_Arcade")
				SET_BIT(g_SimpleInteriorData.iSeventhBS, BS7_SIMPLE_INTERIOR_ENTERING_INTERIOR_FOR_POST_HEIST_MOCAP)
				SET_BIT(g_SimpleInteriorData.iSeventhBS, BS7_SIMPLE_INTERIOR_ENTERING_PLAYING_POST_HEIST_MOCAP)
				SET_SIMPLE_INTERIOR_AUTOWARP_OVERRIDE(sData.pOwner)
				REQUEST_SCRIPT("AM_MP_SMPL_INTERIOR_EXT")
				REQUEST_SCRIPT("AM_MP_SMPL_INTERIOR_INT")
				REQUEST_SCRIPT("AM_MP_ARCADE")
				REQUEST_RELAUCHING_OF_SIMPLE_INTERIOR_INT_SCRIPT(sData.eTargetInteriorID)
				sData.bPinnedAndRequestedInterior = TRUE
			ENDIF
		ELSE
			RETURN NOT IS_BIT_SET(g_SimpleInteriorData.iSeventhBS, BS7_SIMPLE_INTERIOR_ENTERING_INTERIOR_FOR_POST_HEIST_MOCAP)
		ENDIF
	ELSE
		sData.eTargetInteriorID = SIMPLE_INTERIOR_ARCADE_PALETO_BAY
		SCRIPT_ASSERT("LOAD_SIMPLE_INTERIOR_SYSTEM_FOR_POST_HEIST_MOCAP - Invalid interior ID!")
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC STRING GET_CASINO_HEIST_ROOF_SCENE_VEHICLE_NAME(INT iVehID)
	STRING sReturn
	
	SWITCH iVehID
		CASE 0	sReturn = "Riot1"		BREAK
		CASE 1	sReturn = "Riot2"		BREAK
		CASE 2	sReturn = "Riot3"		BREAK
		CASE 3	sReturn = "Riot4"		BREAK
		CASE 4	sReturn = "Police1"		BREAK
		CASE 5	sReturn = "Police2"		BREAK
		CASE 6	sReturn = "Police3"		BREAK
		CASE 7	sReturn = "Police4"		BREAK
		CASE 8	sReturn = "Police5"		BREAK
		CASE 9	sReturn = "Police6"		BREAK
		CASE 10	sReturn = "Police7"		BREAK
		CASE 11	sReturn = "Police8"		BREAK
		CASE 12	sReturn = "NewsVan4"	BREAK
	ENDSWITCH
	
	RETURN sReturn
ENDFUNC

FUNC STRING GET_CASINO_BAR_SCENE_PROP_NAME(INT iPropID)
	STRING sReturn
	
	SWITCH iPropID
		CASE 0	sReturn = "player_bottle_4"		BREAK
		CASE 1	sReturn = "player_glass_3"		BREAK
		CASE 2	sReturn = "player_glass_4"		BREAK
	ENDSWITCH
	
	RETURN sReturn
ENDFUNC

FUNC BOOL GET_CASINO_HEIST_ROOF_SCENE_VEHICLE_NEEDS_HEADLIGHTS_ON(INT iVehID)
	IF iVehID = 10
	OR iVehID = 11
	OR iVehID = 12
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL GET_CASINO_HEIST_ROOF_SCENE_VEHICLE_NEEDS_SIRENS_ON(INT iVehID)
	IF iVehID = 12
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL DOES_CASINO_BAR_SCENE_PROP_NEED_HIDING(STRUCT_HEIST_END_WINNER_SCENE_DATA &sData, INT iPropID)

	SWITCH iPropID
		CASE 0
		CASE 2
			IF sData.iMocapRegisteredPedsCount <= 3
				RETURN TRUE
			ENDIF
		BREAK
		CASE 1	RETURN (sData.iMocapRegisteredPedsCount < 3) BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC STRING GET_ISLAND_HEIST_CELEBRATION_SCENE_PROP_NAME(INT iPropID)
	STRING sReturn
	
	SWITCH iPropID
		CASE 0	sReturn = "Shot_Glass_3"	BREAK
		CASE 1	sReturn = "MP_3_Lime"		BREAK
		CASE 2	sReturn = "Shot_Glass_4"	BREAK
		CASE 3	sReturn = "MP_4_Lime"		BREAK
	ENDSWITCH
	
	RETURN sReturn
ENDFUNC

FUNC BOOL DOES_ISLAND_HEIST_CELEBRATION_SCENE_PROP_NEED_HIDING(STRUCT_HEIST_END_WINNER_SCENE_DATA &sData, INT iPropID)

	SWITCH iPropID
		CASE 0
		CASE 1
			IF sData.iMocapRegisteredPedsCount < 3
				RETURN TRUE
			ENDIF
		BREAK
		CASE 2
		CASE 3
			IF sData.iMocapRegisteredPedsCount < 4
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC SETUP_CUTSCENE_ENTITIES(STRUCT_HEIST_END_WINNER_SCENE_DATA &sData)
	IF sData.eSceneType = eHEISTWINNERSCENETYPE_CASINO_HEIST_ROOF
		//This mocap requires script to grab all vehicles and activate their lights
		IF NOT sData.bSetupSceneEntities
			INT i
			
			REPEAT CASINO_HEIST_ROOF_SCENE_VEHICLES i
				STRING sSceneEntityName = GET_CASINO_HEIST_ROOF_SCENE_VEHICLE_NAME(i)
				ENTITY_INDEX sceneEntity = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY(sSceneEntityName)
				
				IF DOES_ENTITY_EXIST(sceneEntity)
					IF IS_ENTITY_A_VEHICLE(sceneEntity)
						VEHICLE_INDEX sceneVeh = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(sceneEntity)
						
						IF IS_ENTITY_ALIVE(sceneVeh)
							IF GET_CASINO_HEIST_ROOF_SCENE_VEHICLE_NEEDS_HEADLIGHTS_ON(i)
								SET_VEHICLE_LIGHTS(sceneVeh, FORCE_VEHICLE_LIGHTS_ON)
								PRINTLN("[SAC] - SETUP_CUTSCENE_ENTITIES - Turn lights on for vehicle: ", i, " ", sSceneEntityName)
							ENDIF
							IF GET_CASINO_HEIST_ROOF_SCENE_VEHICLE_NEEDS_SIRENS_ON(i)
								SET_VEHICLE_SIREN(sceneVeh, TRUE)
								PRINTLN("[SAC] - SETUP_CUTSCENE_ENTITIES - Turn sirens on for vehicle: ", i, " ", sSceneEntityName)
							ENDIF							
						ELSE
							PRINTLN("[SAC] - SETUP_CUTSCENE_ENTITIES - FAILED to turn on lights for vehicle: ", i, " ", sSceneEntityName, " IS_ENTITY_ALIVE(sceneVeh) = FALSE")
						ENDIF
					ELSE
						PRINTLN("[SAC] - SETUP_CUTSCENE_ENTITIES - FAILED to turn on lights for vehicle: ", i, " ", sSceneEntityName, " IS_ENTITY_A_VEHICLE(sceneEntity) = FALSE")
					ENDIF
				ELSE
					PRINTLN("[SAC] - SETUP_CUTSCENE_ENTITIES - FAILED to turn on lights for vehicle: ", i, " ", sSceneEntityName, " DOES_ENTITY_EXIST(sceneEntity)= FALSE")
				ENDIF
			ENDREPEAT
			
			sData.bSetupSceneEntities = TRUE
		ENDIF
	ELIF sData.eSceneType = eHEISTWINNERSCENETYPE_CASINO_HEIST_BAR
		IF NOT sData.bSetupSceneEntities
		AND sData.iMocapRegisteredPedsCount < 4
			INT i
			
			REPEAT CASINO_HEIST_BAR_SCENE_PROPS i
				STRING sSceneEntityName = GET_CASINO_BAR_SCENE_PROP_NAME(i)
				ENTITY_INDEX sceneEntity = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY(sSceneEntityName)
				
				IF DOES_ENTITY_EXIST(sceneEntity)
				AND DOES_CASINO_BAR_SCENE_PROP_NEED_HIDING(sData, i)
					SET_ENTITY_VISIBLE(sceneEntity, FALSE)
					sData.bSetupSceneEntities = TRUE
					
					PRINTLN("[SAC] - SETUP_CUTSCENE_ENTITIES - hide ", sSceneEntityName)
				ENDIF
			ENDREPEAT
		ENDIF
	#IF FEATURE_HEIST_ISLAND
	ELIF sData.eSceneType = eHEISTWINNERSCENETYPE_ISLAND_HEIST
		IF NOT sData.bSetupSceneEntities
		AND sData.iMocapRegisteredPedsCount < 4
			INT i
			
			REPEAT CASINO_ISLAND_HEIST_SCENE_PROPS i
				STRING sSceneEntityName = GET_ISLAND_HEIST_CELEBRATION_SCENE_PROP_NAME(i)
				ENTITY_INDEX sceneEntity = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY(sSceneEntityName)
				
				IF DOES_ENTITY_EXIST(sceneEntity)
				AND DOES_ISLAND_HEIST_CELEBRATION_SCENE_PROP_NEED_HIDING(sData, i)
					SET_ENTITY_VISIBLE(sceneEntity, FALSE)
					sData.bSetupSceneEntities = TRUE
					
					PRINTLN("[SAC] - SETUP_CUTSCENE_ENTITIES - hide ", sSceneEntityName)
				ENDIF
			ENDREPEAT
		ENDIF
	#ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Returns the time from the end of the scene we should start fading the screen
FUNC INT GET_SCENE_FADE_OUT_START_TIME(eHEIST_WINNER_SCENE_TYPE eSceneType)
	IF eSceneType = eHEISTWINNERSCENETYPE_CASINO_HEIST_ROOF
	#IF FEATURE_HEIST_ISLAND
	OR eSceneType = eHEISTWINNERSCENETYPE_ISLAND_HEIST
	#ENDIF
		RETURN 3000
	ENDIF
	
	RETURN 1000
ENDFUNC

FUNC INT GET_SCENE_FADE_OUT_DURATION(eHEIST_WINNER_SCENE_TYPE eSceneType)
	IF eSceneType = eHEISTWINNERSCENETYPE_CASINO_HEIST_ROOF
	#IF FEATURE_HEIST_ISLAND
	OR eSceneType = eHEISTWINNERSCENETYPE_ISLAND_HEIST
	#ENDIF
		RETURN 3000
	ENDIF
	
	RETURN 500
ENDFUNC

FUNC BOOL DOES_SCENE_PAUSE_RENDER_ON_END(eHEIST_WINNER_SCENE_TYPE eSceneType)
	IF eSceneType = eHEISTWINNERSCENETYPE_CASINO_HEIST_BAR	
	#IF FEATURE_HEIST_ISLAND
	OR eSceneType = eHEISTWINNERSCENETYPE_ISLAND_HEIST
	#ENDIF
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_SCENE_START_FADE_OUT_AUDIO_WITH_FADE(eHEIST_WINNER_SCENE_TYPE eSceneType)
	IF eSceneType = eHEISTWINNERSCENETYPE_CASINO_HEIST_ROOF
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL MAINTAIN_HEIST_END_WINNER_SCENE(STRUCT_HEIST_END_WINNER_SCENE_DATA &sData)
	
	BOOL bBeginScene
	BOOL bSkipSceneAndGoToEnd
	
	#IF IS_DEBUG_BUILD
		IF sHeistWinnerSceneData.bTerminateEndHeistScene
			sData.eSceneStage = eHEISTWINNERSCENESTAGE_END
		ENDIF
	#ENDIF
	
	SWITCH sData.eSceneStage
		
		CASE eHEISTWINNERSCENESTAGE_INIT
			
			HIDE_UI_FOR_CELEBRATION_SCREEN_THIS_FRAME()
			
			DRAW_CELEBRATION_SCREEN_THIS_FRAME(sCelebrationData, FALSE)
			
			IF CAN_START_WINNER_SCENE()
				bBeginScene = TRUE
			ENDIF
			
			#IF IS_DEBUG_BUILD
				IF sHeistWinnerSceneData.bRunEndHeistScene
					bBeginScene = TRUE
				ENDIF
			#ENDIF
			
			IF bBeginScene
				
				PRINTLN("[SAC] - MAINTAIN_HEIST_END_WINNER_SCENE - bBeginScene = TRUE.")
				
				IF IS_PLAYER_SCTV(PLAYER_ID())
					PRINTLN("[SAC] - MAINTAIN_HEIST_END_WINNER_SCENE - bBeginScene - IS_PLAYER_SCTV(PLAYER_ID()) - TRUE")
					IF sData.eSceneType = eHEISTWINNERSCENETYPE_APARTMENT
//					OR sData.eSceneType = eHEISTWINNERSCENETYPE_STRIP_CLUB	
						bSkipSceneAndGoToEnd = TRUE
						PRINTLN("[SAC] - MAINTAIN_HEIST_END_WINNER_SCENE - set bSkipSceneAndGoToEnd = TRUE because we're doing the strip club or apartment scene.")
					ENDIF
				ENDIF
				
				IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bJoinedMissionAsSpectator)//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bJoinedMissionAsSpectator
					PRINTLN("[SAC] - MAINTAIN_HEIST_END_WINNER_SCENE - bBeginScene - bJoinedMissionAsSpectator - TRUE")
//					IF sData.eSceneType = eHEISTWINNERSCENETYPE_APARTMENT
//					OR sData.eSceneType = eHEISTWINNERSCENETYPE_STRIP_CLUB	
						bSkipSceneAndGoToEnd = TRUE
						PRINTLN("[SAC] - MAINTAIN_HEIST_END_WINNER_SCENE - set bSkipSceneAndGoToEnd = TRUE because we're doing the strip club or apartment scene.")
//					ENDIF
				ENDIF
				
				IF bSkipSceneAndGoToEnd
					TOGGLE_RENDERPHASES(TRUE)
					ACTIVATE_RECTANGLE_FADE_IN(FALSE)
					ACTIVATE_RECTANGLE_FADE_OUT(TRUE)
					IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bJoinedMissionAsSpectator)//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bJoinedMissionAsSpectator
						sData.eSceneStage = eHEISTWINNERSCENESTAGE_DO_JOINED_MISSION_AS_SPECTATOR_WARP
						PRINTLN("[SAC] - MAINTAIN_HEIST_END_WINNER_SCENE - going to stage eHEISTWINNERSCENESTAGE_DO_JOINED_MISSION_AS_SPECTATOR_WARP.")
					ELSE
						sData.eSceneStage = eHEISTWINNERSCENESTAGE_END
						PRINTLN("[SAC] - MAINTAIN_HEIST_END_WINNER_SCENE - going to stage eHEISTWINNERSCENESTAGE_END.")
					ENDIF
					SET_GAME_PAUSES_FOR_STREAMING(TRUE)
					PRINTLN("[SAC] - SET_GAME_PAUSES_FOR_STREAMING(TRUE). Call 0.")
				ELSE
					SET_CELEBRATION_SCREEN_AS_ACTIVE(TRUE)
					
					IF sData.eSceneType = eHEISTWINNERSCENETYPE_CASINO_HEIST_BAR
						SETUP_SIMPLE_INTERIOR_PROPERTY_OWNER(sData)
						sData.eSceneStage = eHEISTWINNERSCENESTAGE_LOAD_SIMPLE_INTERIOR
						PRINTLN("[SAC] - MAINTAIN_HEIST_END_WINNER_SCENE - going to stage eHEISTWINNERSCENESTAGE_LOAD_SIMPLE_INTERIOR.")
					ELSE
						sData.eSceneStage = eHEISTWINNERSCENESTAGE_LOAD_SCENE
						PRINTLN("[SAC] - MAINTAIN_HEIST_END_WINNER_SCENE - going to stage eHEISTWINNERSCENESTAGE_LOAD_SCENE.")
					ENDIF
				ENDIF
				
			ENDIF
			
		BREAK
		
		CASE eHEISTWINNERSCENESTAGE_LOAD_SIMPLE_INTERIOR
			HIDE_UI_FOR_CELEBRATION_SCREEN_THIS_FRAME()
			
			IF LOAD_SIMPLE_INTERIOR_SYSTEM_FOR_POST_HEIST_MOCAP(sData)
				sData.eSceneStage = eHEISTWINNERSCENESTAGE_LOAD_SCENE
				PRINTLN("[SAC] - MAINTAIN_HEIST_END_WINNER_SCENE - going to stage eHEISTWINNERSCENESTAGE_LOAD_SCENE.")
			ENDIF
		BREAK
		
		CASE eHEISTWINNERSCENESTAGE_LOAD_SCENE
			
			HIDE_UI_FOR_CELEBRATION_SCREEN_THIS_FRAME()
			
			DRAW_CELEBRATION_SCREEN_THIS_FRAME(sCelebrationData, FALSE)
			
			IF IS_BIT_SET(iCelebrationGeneralBitset, CELEB_GENERAL_BITSET_COMPLETED_PLAYER_PRE_LOAD_WARP)
				PRINTLN("[SAC] - CELEB_GENERAL_BITSET_COMPLETED_PLAYER_PRE_LOAD_WARP bit is set, player is in winner scene location.")
				PRINTLN("[SAC] - going to stage eHEISTWINNERSCENESTAGE_DELAY_BEFORE_CUTSCENE_START.")
				IF sData.eSceneType = eHEISTWINNERSCENETYPE_STRIP_CLUB
					SET_ROOM_FOR_GAME_VIEWPORT_BY_NAME(sData.strRoomName)
					PRINTLN("[SAC] - SET_ROOM_FOR_GAME_VIEWPORT_BY_NAME - ", sData.strRoomName)
				ELIF sData.eSceneType = eHEISTWINNERSCENETYPE_APARTMENT
					SET_ROOM_FOR_GAME_VIEWPORT_BY_NAME(sData.strRoomName)
					PRINTLN("[SAC] - SET_ROOM_FOR_GAME_VIEWPORT_BY_NAME - ", sData.strRoomName)
					IF NOT g_bChampaignCelebrationCutsceneIsActive
						g_bChampaignCelebrationCutsceneIsActive = TRUE
						PRINTLN("[SAC] - set g_bChampaignCelebrationCutsceneIsActive to TRUE.")
					ENDIF
				ENDIF
				sData.eSceneStage = eHEISTWINNERSCENESTAGE_DELAY_BEFORE_CUTSCENE_START
			ENDIF
			
		BREAK
		
		CASE eHEISTWINNERSCENESTAGE_DELAY_BEFORE_CUTSCENE_START
			
			BOOL bGoToStartScene
			bGoToStartScene = TRUE
			
			IF NOT HAS_NET_TIMER_EXPIRED(timerFadeFlashDelay, WHITE_SCREEN_HOLD_TIME)
				bGoToStartScene = FALSE
			ENDIF
			
			IF sData.eSceneType = eHEISTWINNERSCENETYPE_STRIP_CLUB
				
				IF NOT HAS_NET_TIMER_STARTED(timerDelayForCutsceneMusic)
					START_NET_TIMER(timerDelayForCutsceneMusic)
					START_CELEBRATION_STRIP_CLUB_MUSIC_EVENT()
					SET_BIT(iCelebrationGeneralBitset, CELEB_GENERAL_BITSET_FREEZE_MICROPHONE)
					PRINTLN("[SAC] - started net timer timerDelayForCutsceneMusic.")
					PRINTLN("[SAC] - called START_CELEBRATION_STRIP_CLUB_MUSIC_EVENT().")
					PRINTLN("[SAC] - set bit CELEB_GENERAL_BITSET_FREEZE_MICROPHONE.")
					bGoToStartScene = FALSE
				ELSE
					IF NOT HAS_NET_TIMER_EXPIRED(timerDelayForCutsceneMusic, DELAY_FOR_MUSIC_TIME)
						bGoToStartScene = FALSE
					ENDIF
				ENDIF
				
			ELIF sData.eSceneType = eHEISTWINNERSCENETYPE_APARTMENT
				
				IF NOT HAS_NET_TIMER_STARTED(timerDelayForCutsceneMusic)
					START_NET_TIMER(timerDelayForCutsceneMusic)
					START_CELEBRATION_APARTMENT_MUSIC_EVENT()
					SET_BIT(iCelebrationGeneralBitset, CELEB_GENERAL_BITSET_FREEZE_MICROPHONE)
					PRINTLN("[SAC] - started net timer timerDelayForCutsceneMusic.")
					PRINTLN("[SAC] - called START_CELEBRATION_APARTMENT_MUSIC_EVENT().")
					PRINTLN("[SAC] - set bit CELEB_GENERAL_BITSET_FREEZE_MICROPHONE.")
					bGoToStartScene = FALSE
				ELSE
					IF NOT HAS_NET_TIMER_EXPIRED(timerDelayForCutsceneMusic, DELAY_FOR_MUSIC_TIME)
						bGoToStartScene = FALSE
					ENDIF
				ENDIF
			
			ELIF sData.eSceneType = eHEISTWINNERSCENETYPE_CASINO_HEIST_BAR
			OR sData.eSceneType = eHEISTWINNERSCENETYPE_CASINO_HEIST_ROOF
			#IF FEATURE_HEIST_ISLAND
			OR sHeistWinnerSceneData.eSceneType = eHEISTWINNERSCENETYPE_ISLAND_HEIST
			#ENDIF
				SET_CELEBRATION_AUDIO_STAGE(eCAS_OFF)
			ENDIF
			
			IF bGoToStartScene
				sData.eSceneStage = eHEISTWINNERSCENESTAGE_START_SCENE
				PRINTLN("[SAC] - going to stage eHEISTWINNERSCENESTAGE_START_SCENE.")
			ENDIF
			
		BREAK
		
		CASE eHEISTWINNERSCENESTAGE_START_SCENE
			
			HIDE_UI_FOR_CELEBRATION_SCREEN_THIS_FRAME()
			
			IF NOT IS_PLAYER_SCTV(PLAYER_ID())
			AND NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bJoinedMissionAsSpectator)//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bJoinedMissionAsSpectator
				SET_LOCAL_PLAYER_FOR_MOCAP()
				PRINTLN("[SAC] - [NETCELEBRATION] - SET_LOCAL_PLAYER_FOR_MOCAP() call 0.")
			ENDIF
			
			PRINTLN("[SAC] - BUSY SPINNER OFF - call 6.")
			BUSYSPINNER_OFF()
			
			SET_BIT(sHeistWinnerSceneData.iBitset, HEIST_WINNER_SCENE_SET_PLAYER_FOR_MOCAP)
			PRINTLN("[SAC] - MAINTAIN_HEIST_END_WINNER_SCENE - set HEIST_WINNER_SCENE_SET_PLAYER_FOR_MOCAP bit.")
			
			CELEB_TURN_NIGHTVISION_OFF()
			
			TOGGLE_RENDERPHASES(TRUE)
			ANIMPOSTFX_STOP_ALL()
			SET_GAME_PAUSES_FOR_STREAMING(TRUE)
			PRINTLN("[SAC] - SET_GAME_PAUSES_FOR_STREAMING(TRUE). Call 1.")
									
			STOP_CELEBRATION_SCREEN_ANIM(sCelebrationData, GET_CELEBRATION_SCREEN_NAME())
			
			CLEANUP_CELEBRATION_SCREEN(sCelebrationData, FALSE, GET_CELEBRATION_SCREEN_NAME())
			
			RESET_CELEBRATION_DATA(sCelebrationData)
			
			REGISTER_MOCAP_PEDS(sData)
			REGISTER_MOCAP_OBJECTS(sData)
			
			SET_LOCAL_PLAYER_VISIBLE_IN_CUTSCENE(FALSE, FALSE)
			
			START_MP_CUTSCENE()
			
			IF (sData.eSceneType = eHEISTWINNERSCENETYPE_APARTMENT)
				
				PRINTLN("[SAC] - [NETCELEBRATION] - eHEISTWINNERSCENESTAGE_START_SCENE (sData.eSceneType = eHEISTWINNERSCENETYPE_APARTMENT)")
				
				IF IS_HEIST_PROPERTY_TYPE_DLC(g_TransitionSessionNonResetVars.iAptIndex)
					
					PRINTLN("[SAC] - [NETCELEBRATION] - eHEISTWINNERSCENESTAGE_START_SCENE (IS_HEIST_PROPERTY_TYPE_DLC = TRUE)")
					
					// DLC high end apartment
					Move_Heist_Cutscene_To_Heist_Apartment(1, g_TransitionSessionNonResetVars.iAptIndex, TRUE)
					
				ELIF (	IS_PROPERTY_CUSTOM_APARTMENT(g_TransitionSessionNonResetVars.iAptIndex)
						OR IS_PROPERTY_STILT_APARTMENT(g_TransitionSessionNonResetVars.iAptIndex, PROPERTY_STILT_APT_5_BASE_A)
						OR IS_PROPERTY_STILT_APARTMENT(g_TransitionSessionNonResetVars.iAptIndex, PROPERTY_STILT_APT_1_BASE_B)	)
						
						#IF IS_DEBUG_BUILD
						IF 	IS_PROPERTY_CUSTOM_APARTMENT(g_TransitionSessionNonResetVars.iAptIndex)
							PRINTLN("[SAC] - [NETCELEBRATION] - eHEISTWINNERSCENESTAGE_START_SCENE (IS_PROPERTY_CUSTOM_APARTMENT = TRUE)")
						ELIF IS_PROPERTY_STILT_APARTMENT(g_TransitionSessionNonResetVars.iAptIndex, PROPERTY_STILT_APT_5_BASE_A)
							PRINTLN("[SAC] - [NETCELEBRATION] - eHEISTWINNERSCENESTAGE_START_SCENE (PROPERTY_STILT_APT_5_BASE_A = TRUE)")
						ELIF IS_PROPERTY_STILT_APARTMENT(g_TransitionSessionNonResetVars.iAptIndex, PROPERTY_STILT_APT_1_BASE_B)
							PRINTLN("[SAC] - [NETCELEBRATION] - eHEISTWINNERSCENESTAGE_START_SCENE (PROPERTY_STILT_APT_1_BASE_B = TRUE)")
						ENDIF
						#ENDIF
						
					// Stilt or customisable apartments
					Move_Heist_Cutscene_To_Heist_Apartment(1, g_TransitionSessionNonResetVars.iAptIndex, FALSE, MP_END_HEIST_WINNER_SCENE_CAMERA_COORDS)
					
				ELSE
					
					PRINTLN("[SAC] - [NETCELEBRATION] - eHEISTWINNERSCENESTAGE_START_SCENE (apartment is not high end, stilt or custom)")
					
					// All other apartments
					Move_Heist_Cutscene_To_Heist_Apartment(1, g_TransitionSessionNonResetVars.iAptIndex)
					
				ENDIF
			ELSE
				START_CUTSCENE()
			ENDIF
			
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDrawBlackRectangle[0] = FALSE
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDrawBlackRectangle[1] = FALSE
			
//			TRIGGER_MUSIC_EVENT("MP_MC_SILENT")
//			PRINTLN("[SAC] - MAINTAIN_HEIST_END_WINNER_SCENE - called TRIGGER_MUSIC_EVENT('MP_MC_SILENT').")
			
			#IF FEATURE_HEIST_ISLAND
			IF sData.eSceneType = eHEISTWINNERSCENETYPE_ISLAND_HEIST
				TRIGGER_MUSIC_EVENT("HEI4_CELEBRATION_CHEERS_MUSIC")
			ENDIF
			#ENDIF
			
			ACTIVATE_RECTANGLE_FADE_IN(FALSE)
			ACTIVATE_RECTANGLE_FADE_OUT(TRUE)
			
			START_NET_TIMER(sData.sceneTimer)
			
			PRINTLN("[SAC] - MAINTAIN_HEIST_END_WINNER_SCENE - toggled renderphases, cleaned up old celebrations screen.")
			PRINTLN("[SAC] - MAINTAIN_HEIST_END_WINNER_SCENE - going to stage eHEISTWINNERSCENESTAGE_START_CELEBRATION_MOVIE.")
			
			sData.eSceneStage = eHEISTWINNERSCENESTAGE_START_CELEBRATION_MOVIE
			
		BREAK
		
		CASE eHEISTWINNERSCENESTAGE_START_CELEBRATION_MOVIE
			
			BOOL bContinue
			
			CELEBRATION_SCREEN_TYPE eCelebType
			eCelebType = CELEBRATION_HEIST
			IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bGangOpsTransition
				eCelebType = CELEBRATION_GANGOPS
			ENDIF
			IF sData.eSceneType = eHEISTWINNERSCENETYPE_CASINO_HEIST_ROOF
			OR sData.eSceneType = eHEISTWINNERSCENETYPE_CASINO_HEIST_BAR
			#IF FEATURE_HEIST_ISLAND
			OR sHeistWinnerSceneData.eSceneType = eHEISTWINNERSCENETYPE_ISLAND_HEIST
			#ENDIF
				eCelebType = CELEBRATION_HEIST
			ENDIF
			
			REQUEST_CELEBRATION_SCREEN(sCelebrationData, eCelebType)
			
			HIDE_UI_FOR_CELEBRATION_SCREEN_THIS_FRAME()
			
			IF HAS_CELEBRATION_SCREEN_LOADED(sCelebrationData)
				
				IF NOT IS_CUTSCENE_PLAYING()
					PRINTLN("[SAC] - MAINTAIN_HEIST_END_WINNER_SCENE - IS_CUTSCENE_PLAYING returning FALSE.")
					IF NOT HAS_NET_TIMER_STARTED(sData.mocapNotPlayingTimer)
						START_NET_TIMER(sData.mocapNotPlayingTimer, TRUE)
					ELSE
						IF HAS_NET_TIMER_EXPIRED(sData.mocapNotPlayingTimer, 5000, TRUE)
							PRINTLN("[SAC] - MAINTAIN_HEIST_END_WINNER_SCENE - IS_CUTSCENE_PLAYING has been returning FALSE for >= 5 seconds. Something wrong with the cutscene?")
							SCRIPT_ASSERT("[SAC] - MAINTAIN_HEIST_END_WINNER_SCENE - IS_CUTSCENE_PLAYING has been returning FALSE for >= 5 seconds. Something wrong with the cutscene?")
							bContinue = TRUE
						ENDIF
					ENDIF
				ELSE
					bContinue = TRUE
				ENDIF
				
				IF bContinue
					
					// url:bugstar:6151724
					IF sData.eSceneType = eHEISTWINNERSCENETYPE_CASINO_HEIST_ROOF
						SET_TIMECYCLE_MODIFIER("bokeh_removebuzz")
						PRINTLN("[SAC] - mocap start, adding timceycle modifier")
					ENDIF
					
//					IF HAS_NET_TIMER_EXPIRED(sData.sceneTimer, 2000)
						
						PRINTLN("[SAC] - MAINTAIN_HEIST_END_WINNER_SCENE - going to stage eHEISTWINNERSCENESTAGE_RUN_SCENE.")
						sData.eSceneStage = eHEISTWINNERSCENESTAGE_RUN_SCENE
						
//					ENDIF
					
				ENDIF
				
			ELSE
				
				PRINTLN("[SAC] - MAINTAIN_HEIST_END_WINNER_SCENE - requesting congratulations scaleform movie.")
				
			ENDIF
			
		BREAK
		
		CASE eHEISTWINNERSCENESTAGE_RUN_SCENE
			
			HIDE_UI_FOR_CELEBRATION_SCREEN_THIS_FRAME()
			
			IF SHOULD_DRAW_CONGRATS_CHARD()
			
				IF NOT IS_BIT_SET(iCelebrationGeneralBitset, CELEB_GENERAL_BITSET_SETUP_WINNER_SCENE_MOVIE)
					CREATE_CELEBRATION_SCREEN(sCelebrationData, "WINNER", "HUD_COLOUR_HSHARD", FALSE)
					SET_CELEBRATION_SCREEN_STAT_DISPLAY_TIME(sCelebrationData, CELEBRATION_SCREEN_WINNER_STAT_DISPLAY_TIME - CELEBRATION_SCREEN_STAT_WIPE_TIME)
					ADD_FINALE_COMPLETE_TO_WALL(sCelebrationData, "WINNER")
					ADD_BACKGROUND_TO_CELEBRATION_SCREEN(sCelebrationData, "WINNER", 0, 0)
					SHOW_CELEBRATION_SCREEN(sCelebrationData, "WINNER")
					PRINTLN("[SAC] - MAINTAIN_HEIST_END_WINNER_SCENE - populated movie with data.")
					#IF IS_DEBUG_BUILD
					INT iTempValue
					iTempValue = GET_GLOBAL_ACTIONSCRIPT_FLAG(ACTIONSCRIPT_GLOBAL_ID)
					PRINTLN("[SAC] - GET_GLOBAL_ACTIONSCRIPT_FLAG(ACTIONSCRIPT_GLOBAL_ID) = ", iTempValue)
					#ENDIF
					SET_BIT(iCelebrationGeneralBitset, CELEB_GENERAL_BITSET_SETUP_WINNER_SCENE_MOVIE)
				ENDIF
				
				DRAW_CELEBRATION_SCREEN_THIS_FRAME(sCelebrationData, FALSE)
				
				IF NOT IS_BIT_SET(iCelebrationGeneralBitset, CELEB_GENERAL_PLAYED_SUCCESS_NEUTRAL_FX_STACK)
					IF HAS_CELEBRATION_SHARD_HEIST_COMPLETE_HIT_SCREEN_CENTRE()
						//ANIMPOSTFX_PLAY("SuccessNeutral", 1000, TRUE)
						PLAY_HEIST_PASS_FX("HeistCelebToast")
						SET_BIT(iCelebrationGeneralBitset, CELEB_GENERAL_PLAYED_SUCCESS_NEUTRAL_FX_STACK)
						PRINTLN("[SAC] - MAINTAIN_HEIST_END_WINNER_SCENE - played stack HeistCelebToast.")
					ENDIF
				ENDIF
			ELSE
				// Re-enable subtitles after shard
				SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_SUBTITLE_TEXT) 
			ENDIF
			
			#IF IS_DEBUG_BUILD
			UPDATE_HEIST_END_WINNER_SCENE_PEDS_WITH_WIDGET_DATA(sData)
			#ENDIF
			
			IF IS_CUTSCENE_PLAYING()
				SETUP_CUTSCENE_ENTITIES(sData)
				
				IF sData.eSceneType = eHEISTWINNERSCENETYPE_STRIP_CLUB
				OR sData.eSceneType = eHEISTWINNERSCENETYPE_APARTMENT
					IF NOT IS_BIT_SET(iCelebrationGeneralBitset, CELEB_GENERAL_SET_ALLOW_RADIO_OVER_FADE_FLAG)
						SET_CELEBRATION_ALLOW_OVER_RADIO_SCREEN_AUDIO_FLAG()
						SET_BIT(iCelebrationGeneralBitset, CELEB_GENERAL_SET_ALLOW_RADIO_OVER_FADE_FLAG)
					ENDIF
				ENDIF
				IF sData.eSceneType = eHEISTWINNERSCENETYPE_BAR
				OR sData.eSceneType = eHEISTWINNERSCENETYPE_ROOFTOP
				OR sData.eSceneType = eHEISTWINNERSCENETYPE_CASINO_HEIST_ROOF
				OR sData.eSceneType = eHEISTWINNERSCENETYPE_CASINO_HEIST_BAR
				#IF FEATURE_HEIST_ISLAND
				OR sData.eSceneType = eHEISTWINNERSCENETYPE_ISLAND_HEIST
				#ENDIF
					IF NOT IS_BIT_SET(iCelebrationGeneralBitset, CELEB_GENERAL_SET_ALLOW_CUTSCENE_OVER_FADE_FLAG)
						SET_CELEBRATION_ALLOW_CUTSCENE_OVER_SCREEN_AUDIO_FLAG()
						SET_BIT(iCelebrationGeneralBitset, CELEB_GENERAL_SET_ALLOW_CUTSCENE_OVER_FADE_FLAG)
					ENDIF
				ENDIF
				
				IF DOES_SCENE_PAUSE_RENDER_ON_END(sData.eSceneType)
					//Next stage will pause the render phases
					
					IF (GET_CUTSCENE_TOTAL_DURATION() - GET_CUTSCENE_TIME()) <= 1000
						//Preload the shard
						IF sData.eSceneType = eHEISTWINNERSCENETYPE_CASINO_HEIST_BAR
						OR sData.eSceneType = eHEISTWINNERSCENETYPE_ISLAND_HEIST

							MAINTAIN_MISSION_END_SHARD(missionEndShardData, TRUE, TRUE)

							REQUEST_STREAMED_TEXTURE_DICT("CommonMenu")
							REQUEST_STREAMED_TEXTURE_DICT("MPLeaderboard")
							REQUEST_STREAMED_TEXTURE_DICT("MPHud")
							REQUEST_SCALEFORM_MOVIE("MP_BIG_MESSAGE_FREEMODE")
							REQUEST_SCALEFORM_MOVIE_INSTANCE("INSTRUCTIONAL_BUTTONS")
						ENDIF
					ENDIF
				ELSE
					IF (GET_CUTSCENE_TOTAL_DURATION() - GET_CUTSCENE_TIME()) <= GET_SCENE_FADE_OUT_START_TIME(sData.eSceneType)
					OR IS_MOCAP_FINISHED()
						IF NOT IS_SCREEN_FADED_OUT()
						AND NOT IS_SCREEN_FADING_OUT()
							IF SHOULD_SCENE_START_FADE_OUT_AUDIO_WITH_FADE(sData.eSceneType)
								PRINTLN("[SAC] - mocap near end fading and set eCAS_FADE_BLACK_SCENE.")
								SET_CELEBRATION_AUDIO_STAGE(eCAS_FADE_BLACK_SCENE)
							ENDIF
							DO_SCREEN_FADE_OUT(GET_SCENE_FADE_OUT_DURATION(sData.eSceneType))
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_MOCAP_FINISHED()
				PRINTLN("[SAC] - MAINTAIN_HEIST_END_WINNER_SCENE - scene timer has expired.")
				IF IS_SCREEN_FADED_OUT()
					IF IS_BIT_SET(iCelebrationGeneralBitset, CELEB_GENERAL_BITSET_FREEZE_MICROPHONE)
						CLEAR_BIT(iCelebrationGeneralBitset, CELEB_GENERAL_BITSET_FREEZE_MICROPHONE)
					ENDIF
					IF NOT IS_PLAYER_SCTV(PLAYER_ID())
						CLEAR_FOCUS()
						PRINTLN("[SAC] - mocap finished, called CLEAR_FOCUS.")
					ELSE
						CLEAR_SPECTATOR_OVERRIDE_COORDS()
						PRINTLN("[SAC] - mocap finished, called CLEAR_SPECTATOR_OVERRIDE_COORDS.")
					ENDIF
					
					// url:bugstar:6151724
					IF sData.eSceneType = eHEISTWINNERSCENETYPE_CASINO_HEIST_ROOF
						PRINTLN("[SAC] - mocap finished, clearing time cycle modifier")
						CLEAR_TIMECYCLE_MODIFIER()
					ENDIF
					
					DEACTIVATE_CELEBRATION_AUDIO_CELEB_SCREEN_SCENE()
					SET_CELEBRATION_AUDIO_STAGE(eCAS_FADE_BLACK_SCENE)
					RESET_NET_TIMER(timerFadeFlashDelay)
					START_NET_TIMER(timerFadeFlashDelay)
					
					IF NOT IS_PLAYER_SCTV(PLAYER_ID())
					AND NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bJoinedMissionAsSpectator)//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bJoinedMissionAsSpectator
						IF (g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPostMissionSceneId = SIS_POST_STRIP_CLUB)
							TRIGGER_POST_HEIST_POSITION_SYSTEMS()
							PRINTLN("[SAC] - MAINTAIN_HEIST_END_WINNER_SCENE - going to stage eHEISTWINNERSCENESTAGE_WARP_TO_OUTSIDE_STRIP_CLUB.")
							sData.eSceneStage = eHEISTWINNERSCENESTAGE_WARP_TO_OUTSIDE_STRIP_CLUB
						ELIF sData.eSceneType = eHEISTWINNERSCENETYPE_CASINO_HEIST_ROOF
							IF IS_GAMER_HANDLE_VALID(sHeistWinnerSceneData.playerLeader)
							AND NETWORK_GET_PLAYER_FROM_GAMER_HANDLE(sHeistWinnerSceneData.playerLeader) = PLAYER_ID()
								PRINTLN("[SAC] - MAINTAIN_HEIST_END_WINNER_SCENE - eHEISTWINNERSCENESTAGE_RUN_SCENE - eHEISTWINNERSCENETYPE_CASINO_HEIST_ROOF - Player is boss setting stat")
								CASINO_HEIST_FLOW__SET_STAT_BIT(ciCASINO_HEIST_FLOW_STAT_BITSET__LESTER_POST_MISSION_ONE_TIME_SCENE_VIEWED)
							ELSE
								PRINTLN("[SAC] - MAINTAIN_HEIST_END_WINNER_SCENE - eHEISTWINNERSCENESTAGE_RUN_SCENE - eHEISTWINNERSCENETYPE_CASINO_HEIST_ROOF - Player is NOT boss dont't set stat")
							ENDIF
							
							sData.eShardOverFadeStage 	= SOF_FADE_OUT
							sData.eSceneStage 			= eHEISTWINNERSCENESTAGE_SHARD_OVER_FADE
						ELSE
							TRIGGER_POST_HEIST_POSITION_SYSTEMS()
							IF (g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPostMissionSceneId != SIS_POST_APARTMENT)
								PLACE_PLAYER_AT_SYNCED_INTERACTION_SCENE_COORDS()
							ENDIF
							PRINTLN("[SAC] - MAINTAIN_HEIST_END_WINNER_SCENE - going to stage eHEISTWINNERSCENESTAGE_END.")
							sData.eSceneStage = eHEISTWINNERSCENESTAGE_END
						ENDIF
					ELSE
						IF (g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPostMissionSceneId != SIS_POST_STRIP_CLUB)
						AND (g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPostMissionSceneId != SIS_POST_APARTMENT)
							PRINTLN("[SAC] - not doing strip club or apartment post mission scene, using Neil's system, calling DO_SCTV_EXTERNAL_FADE_IN_AFTER_POST_CELEB.")
							DO_SCTV_EXTERNAL_FADE_IN_AFTER_POST_CELEB(TRUE)
							PRINTLN("[SAC] - MAINTAIN_HEIST_END_WINNER_SCENE - player is SCTV and not doing apartment or strip club, going to stage eHEISTWINNERSCENESTAGE_END.")
							sData.eSceneStage = eHEISTWINNERSCENESTAGE_END
						ELSE
							PRINTLN("[SAC] - MAINTAIN_HEIST_END_WINNER_SCENE - player is SCTV and is doing apartment or strip club, going to stage eHEISTWINNERSCENESTAGE_SCTV_FADE_IN_DELAY.")
							sData.eSceneStage = eHEISTWINNERSCENESTAGE_SCTV_FADE_IN_DELAY
						ENDIF
						
					ENDIF				
				ELIF sData.eSceneType = eHEISTWINNERSCENETYPE_CASINO_HEIST_BAR
				#IF FEATURE_HEIST_ISLAND
				OR sData.eSceneType = eHEISTWINNERSCENETYPE_ISLAND_HEIST
				#ENDIF
					PRINTLN("[SAC] - MAINTAIN_HEIST_END_WINNER_SCENE - going to stage eHEISTWINNERSCENESTAGE_SHARD_OVER_PAUSED_RENDER.")
					sData.eShardOverFadeStage 	= SOF_SETUP_SHARD
					sData.eSceneStage 			= eHEISTWINNERSCENESTAGE_SHARD_OVER_PAUSED_RENDER
					g_bDelayShardStart 			= FALSE
					PRINTLN("[SAC] - MAINTAIN_HEIST_END_WINNER_SCENE - g_bDelayShardStart = FALSE.")					
				ENDIF
			ELSE
				PRINTLN("[SAC] - MAINTAIN_HEIST_END_WINNER_SCENE - IS_MOCAP_FINISHED returning FALSE.")
				IF sData.eSceneType = eHEISTWINNERSCENETYPE_TREVOR
					MAINTAIN_TREVOR_HEAD_BLOOD()
				ENDIF
			ENDIF
			
		BREAK
		
		CASE eHEISTWINNERSCENESTAGE_SCTV_FADE_IN_DELAY
			
			IF NOT HAS_NET_TIMER_STARTED(timerSctvFadeInDelay)
				START_NET_TIMER(timerSctvFadeInDelay)
				PRINTLN("[SAC] - started timer timerSctvFadeInDelay.")
			ELSE
				IF HAS_NET_TIMER_EXPIRED(timerSctvFadeInDelay, WHITE_SCREEN_HOLD_TIME)
					PRINTLN("[SAC] - timerSctvFadeInDelay has expired, calling DO_SCTV_EXTERNAL_FADE_IN_AFTER_POST_CELEB to trigger Ryan's SCTV fade in.")
					DO_SCTV_EXTERNAL_FADE_IN_AFTER_POST_CELEB(TRUE)
					PRINTLN("[SAC] - MAINTAIN_HEIST_END_WINNER_SCENE - going to stage eHEISTWINNERSCENESTAGE_END.")
					sData.eSceneStage = eHEISTWINNERSCENESTAGE_END
				ENDIF
			ENDIF
		
		BREAK
		
		CASE eHEISTWINNERSCENESTAGE_DO_JOINED_MISSION_AS_SPECTATOR_WARP
			
			IF NOT IS_SCREEN_FADED_OUT()
			AND NOT IS_SCREEN_FADING_OUT()
				
				DO_SCREEN_FADE_OUT(1000)
				
			ELIF IS_SCREEN_FADED_OUT()
				
				SETUP_JOINED_MISSION_AS_SPECTATOR_WARP()
				sData.eSceneStage = eHEISTWINNERSCENESTAGE_WARP_TO_POST_MISSION_VECTOR
				PRINTLN("[SAC] - MAINTAIN_HEIST_END_WINNER_SCENE - going to stage eHEISTWINNERSCENESTAGE_WARP_TO_POST_MISSION_VECTOR.")
				
			ENDIF
			
		BREAK
		
		CASE eHEISTWINNERSCENESTAGE_WARP_TO_OUTSIDE_STRIP_CLUB
			
			HIDE_UI_FOR_CELEBRATION_SCREEN_THIS_FRAME()
			
			IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.enumPostMissionScenePossibleResult = PMSP_PASSED
				
				sData.eSceneStage = eHEISTWINNERSCENESTAGE_END
				
			ELIF g_TransitionSessionNonResetVars.sGlobalCelebrationData.enumPostMissionScenePossibleResult = PMSP_FAILED
					
				IF NET_WARP_TO_COORD(sData.vPlayerPosForAfterScene, sData.fPlayerHeadingForafterScene, FALSE, FALSE, TRUE, TRUE, FALSE, FALSE, TRUE)
					
					PRINTLN("[SAC] - MAINTAIN_HEIST_END_WINNER_SCENE - completed warp to outside strip club.")
					PRINTLN("[SAC] - MAINTAIN_HEIST_END_WINNER_SCENE - vPlayerPosForAfterScene = ", sData.vPlayerPosForAfterScene)
					PRINTLN("[SAC] - MAINTAIN_HEIST_END_WINNER_SCENE - fPlayerHeadingForafterScene", sData.fPlayerHeadingForafterScene)
					PRINTLN("[SAC] - MAINTAIN_HEIST_END_WINNER_SCENE - going to stage eHEISTWINNERSCENESTAGE_END.")
					
					IF IS_NET_PLAYER_OK(PLAYER_ID())
						
						IF NOT IS_SCREEN_FADED_IN()
						AND NOT IS_SCREEN_FADING_IN()
							
							DO_SCREEN_FADE_IN(500)
							
							bActivateInteractionMenuOnCleanup = TRUE
							
							NET_SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
							SET_PED_CURRENT_WEAPON_VISIBLE(PLAYER_PED_ID(),TRUE)
							SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
							
							SET_GAMEPLAY_CAM_RELATIVE_HEADING()
							SET_GAMEPLAY_CAM_RELATIVE_PITCH()
							
							CLEANUP_MP_CUTSCENE()
							
							STOP_CUTSCENE_AUDIO()
							
							Make_Ped_Drunk(PLAYER_PED_ID(), 60000)
							Activate_Drunk_Camera(60000)
			//				force_ped_motion_state(player_ped_id(), ms_on_foot_walk, false) 
			                SIMULATE_PLAYER_INPUT_GAIT(player_id(), pedmove_walk, 6000) 
			                force_ped_ai_and_animation_update(player_ped_id(), true)
							Player_Takes_Alcohol_Hit(player_ped_id())
							
						ELSE
							
							SET_GAME_PAUSES_FOR_STREAMING(TRUE)
							PRINTLN("[SAC] - called SET_GAME_PAUSES_FOR_STREAMING(TRUE). Call A.")
							
							SET_CELEBRATION_AUDIO_STAGE(eCAS_OFF)
							sData.eSceneStage = eHEISTWINNERSCENESTAGE_END
							
						ENDIF
						
					ENDIF
					
				ENDIF
				
			ENDIF
				
		BREAK
		
		CASE eHEISTWINNERSCENESTAGE_WARP_TO_POST_MISSION_VECTOR
			
			HIDE_UI_FOR_CELEBRATION_SCREEN_THIS_FRAME()
			
			IF WARP_TO_SPAWN_LOCATION(SPAWN_LOCATION_AT_SPECIFIC_COORDS_IF_POSSIBLE, FALSE, FALSE, FALSE, FALSE)
				PRINTLN("[SAC] - MAINTAIN_HEIST_END_WINNER_SCENE - completed warp to vPostMissionVector.")
				PRINTLN("[SAC] - MAINTAIN_HEIST_END_WINNER_SCENE - going to stage eHEISTWINNERSCENESTAGE_END.")
				IF NOT IS_SCREEN_FADED_IN()
				AND NOT IS_SCREEN_FADING_IN()
					DO_SCREEN_FADE_IN(500)
				ENDIF
				IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bJoinedMissionAsSpectator)//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bJoinedMissionAsSpectator
					NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				ENDIF
				bActivateInteractionMenuOnCleanup = TRUE
				sData.eSceneStage = eHEISTWINNERSCENESTAGE_END
			ENDIF
			
		BREAK
		
		CASE eHEISTWINNERSCENESTAGE_SHARD_OVER_FADE
			SWITCH sData.eShardOverFadeStage
				CASE SOF_FADE_OUT
					IF IS_SCREEN_FADED_OUT()
						sData.eShardOverFadeStage = SOF_SETUP_SHARD						
						PRINTLN("[SAC] - MAINTAIN_HEIST_END_WINNER_SCENE - eHEISTWINNERSCENESTAGE_SHARD_OVER_FADE - completed fade out.")
					ELIF NOT IS_SCREEN_FADED_OUT()
						DO_SCREEN_FADE_OUT(GET_SCENE_FADE_OUT_DURATION(sData.eSceneType))
					ENDIF
				BREAK
				CASE SOF_SETUP_SHARD
					REINIT_NET_TIMER(sData.shardOverFadeDisplayTime)
					MAINTAIN_MISSION_END_SHARD(missionEndShardData, TRUE)
					sData.eShardOverFadeStage = SOF_DRAW
					PRINTLN("[SAC] - MAINTAIN_HEIST_END_WINNER_SCENE - eHEISTWINNERSCENESTAGE_SHARD_OVER_FADE - Shard setup.")
				BREAK
				CASE SOF_DRAW
					IF HAS_NET_TIMER_EXPIRED(sData.shardOverFadeDisplayTime, 10000)
						PRINTLN("[SAC] - MAINTAIN_HEIST_END_WINNER_SCENE - eHEISTWINNERSCENESTAGE_SHARD_OVER_FADE - Finished - Move to cleanup.")
						CLEAR_ALL_BIG_MESSAGES()
						sData.eShardOverFadeStage = SOF_CLEANUP
					ELIF NOT HAS_NET_TIMER_STARTED(sData.shardOverFadeDisplayTime)
						REINIT_NET_TIMER(sData.shardOverFadeDisplayTime)
					ENDIF
				BREAK
				CASE SOF_CLEANUP
					IF NOT IS_ANY_BIG_MESSAGE_BEING_DISPLAYED()						
						TRIGGER_POST_HEIST_POSITION_SYSTEMS()
						IF (g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPostMissionSceneId != SIS_POST_APARTMENT)
							PLACE_PLAYER_AT_SYNCED_INTERACTION_SCENE_COORDS()
						ENDIF
						PRINTLN("[SAC] - MAINTAIN_HEIST_END_WINNER_SCENE - going to stage eHEISTWINNERSCENESTAGE_END.")
						sData.eSceneStage = eHEISTWINNERSCENESTAGE_END
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		
		CASE eHEISTWINNERSCENESTAGE_SHARD_OVER_PAUSED_RENDER
			SWITCH sData.eShardOverFadeStage
				CASE SOF_SETUP_SHARD
					sData.eShardOverFadeStage = SOF_DRAW
				BREAK
				CASE SOF_DRAW
					
					IF g_bDelayShardStart
						g_bDelayShardStart = FALSE
						PRINTLN("[SAC] - MAINTAIN_HEIST_END_WINNER_SCENE - g_bDelayShardStart = FALSE.")						
					ELSE
						IF GET_TOGGLE_PAUSED_RENDERPHASES_STATUS()
							TOGGLE_RENDERPHASES(FALSE, TRUE)
							
							FLOAT fNearCam
							FLOAT fFarCam
							fNearCam = GET_FINAL_RENDERED_CAM_NEAR_DOF()
							fFarCam = GET_FINAL_RENDERED_CAM_FAR_DOF()
							
							SET_HIDOF_OVERRIDE(TRUE, TRUE, 0, fNearCam, fFarCam, fFarCam + 25.0)							
						ENDIF
					ENDIF
					
					IF HAS_NET_TIMER_EXPIRED(sData.shardOverFadeDisplayTime, 10000)
						PRINTLN("[SAC] - MAINTAIN_HEIST_END_WINNER_SCENE - eHEISTWINNERSCENESTAGE_SHARD_OVER_FADE - Finished - Move to cleanup.")
						DO_SCREEN_FADE_OUT(500)
						CLEAR_ALL_BIG_MESSAGES()
						sData.eShardOverFadeStage = SOF_CLEANUP
					ELIF NOT HAS_NET_TIMER_STARTED(sData.shardOverFadeDisplayTime)
						REINIT_NET_TIMER(sData.shardOverFadeDisplayTime)
					ENDIF
				BREAK
				CASE SOF_CLEANUP
					IF IS_SCREEN_FADED_OUT()						
						TOGGLE_PAUSED_RENDERPHASES(TRUE)
						
						IF sData.eSceneType = eHEISTWINNERSCENETYPE_CASINO_HEIST_BAR		
							CLEAR_BIT(g_SimpleInteriorData.iSeventhBS, BS7_SIMPLE_INTERIOR_ENTERING_PLAYING_POST_HEIST_MOCAP)
						ENDIF
						
						SET_HIDOF_OVERRIDE(FALSE,FALSE,0,0,0,0)
						
						TRIGGER_POST_HEIST_POSITION_SYSTEMS()
						IF (g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPostMissionSceneId != SIS_POST_APARTMENT)
							PLACE_PLAYER_AT_SYNCED_INTERACTION_SCENE_COORDS()
						ENDIF
						PRINTLN("[SAC] - MAINTAIN_HEIST_END_WINNER_SCENE - going to stage eHEISTWINNERSCENESTAGE_END.")
						sData.eSceneStage = eHEISTWINNERSCENESTAGE_END
					ELIF NOT IS_SCREEN_FADING_OUT()
						DO_SCREEN_FADE_OUT(500)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		
		CASE eHEISTWINNERSCENESTAGE_END
			
			IF HAS_NET_TIMER_EXPIRED(timerFadeFlashDelay, WHITE_SCREEN_HOLD_TIME)
								
				IF NOT sCelebrationData.bCleanedUpEndWinnerScene
					
					SET_WINNER_SCENE_INTERIOR_TO_NORMAL_FREEMODE_STATE()
					
					STOP_CELEBRATION_SCREEN_ANIM(sCelebrationData, GET_CELEBRATION_SCREEN_NAME())
					CLEANUP_CELEBRATION_SCREEN(sCelebrationData, FALSE, GET_CELEBRATION_SCREEN_NAME())
					
					CLEANUP_MP_CUTSCENE(TRUE, FALSE)
					
					DELETE_HEIST_END_WINNER_SCENE_ENTITIES(sData)
					
					REMOVE_CUTSCENE()
					
					TOGGLE_PLAYER_DAMAGE_OVERLAY(TRUE)
					
					TOGGLE_RENDERPHASES(TRUE)
					PRINTLN("[SAC] - TOGGLE_RENDERPHASES(TRUE)")
					
					SET_GAME_PAUSES_FOR_STREAMING(TRUE)
					PRINTLN("[SAC] - SET_GAME_PAUSES_FOR_STREAMING(TRUE). Call 2.")
				
					sCelebrationData.bCleanedUpEndWinnerScene = TRUE
					
				ENDIF
			
			ENDIF
			
			IF IS_SCREEN_FADED_IN()
				IF sData.eSceneType = eHEISTWINNERSCENETYPE_APARTMENT
					IF IS_PLAYER_SCTV(PLAYER_ID())
					OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bJoinedMissionAsSpectator)//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bJoinedMissionAsSpectator
						PRINTLN("[SAC] - MAINTAIN_HEIST_END_WINNER_SCENE - IS_PLAYER_SCTV(PLAYER_ID()) = TRUE.")
						STRUCT_HEIST_END_WINNER_SCENE_DATA sTemp
						sData = sTemp
						PRINTLN("[SAC] - MAINTAIN_HEIST_END_WINNER_SCENE - screen faded in, completed cleanup of end winner scene and movie.")
						RETURN TRUE
					ELSE
						IF STAND_ALONE_CELEBRATION_IS_PLAYER_IN_APARTMENT_INTERIOR()
							PRINTLN("[SAC] - STAND_ALONE_CELEBRATION_IS_PLAYER_IN_APARTMENT_INTERIOR = TRUE.")
							IF STAND_ALONE_CELEBRATION_IS_PLAYER_RUNNING_APARTMENT_INTERIOR_SCRIPT()
								PRINTLN("[SAC] - STAND_ALONE_CELEBRATION_IS_PLAYER_RUNNING_APARTMENT_INTERIOR_SCRIPT = TRUE.")
								STRUCT_HEIST_END_WINNER_SCENE_DATA sTemp
								sData = sTemp
								PRINTLN("[SAC] - MAINTAIN_HEIST_END_WINNER_SCENE - screen faded in, completed cleanup of end winner scene and movie.")
								RETURN TRUE
							ENDIF
						ELSE
							PRINTLN("[SAC] - STAND_ALONE_CELEBRATION_IS_PLAYER_IN_APARTMENT_INTERIOR = FALSE.")
							STRUCT_HEIST_END_WINNER_SCENE_DATA sTemp
							sData = sTemp
							PRINTLN("[SAC] - MAINTAIN_HEIST_END_WINNER_SCENE - screen faded in, completed cleanup of end winner scene and movie.")
							RETURN TRUE
						ENDIF
					ENDIF
				ELSE				
					PRINTLN("[SAC] - sData.eSceneType != eHEISTWINNERSCENETYPE_APARTMENT.")
					STRUCT_HEIST_END_WINNER_SCENE_DATA sTemp
					sData = sTemp
					PRINTLN("[SAC] - MAINTAIN_HEIST_END_WINNER_SCENE - screen faded in, completed cleanup of end winner scene and movie.")
					RETURN TRUE
				ENDIF
			ELSE
				IF NOT IS_PLAYER_SCTV(PLAYER_ID())
				AND NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bJoinedMissionAsSpectator)//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bJoinedMissionAsSpectator
					IF sData.eSceneType = eHEISTWINNERSCENETYPE_APARTMENT
						IF IS_SCREEN_FADED_OUT()
							IF NOT HAS_NET_TIMER_STARTED(waitForApartmentFadeIntimer)
								START_NET_TIMER(waitForApartmentFadeIntimer)
							ELSE
								IF HAS_NET_TIMER_EXPIRED(waitForApartmentFadeIntimer, 50000)
									SET_CELEBRATION_STATE(eCELEBRATIONSTATE_APARTMENT_FAILSAFE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		BREAK
		
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC


/// PURPOSE:
///    Cleans up variables and calls any required commands just before the script terminates.
PROC SCRIPT_CLEANUP()
	
	PRINTLN("[SAC] - starting script cleanup.")
	DEBUG_PRINTCALLSTACK()
	
	DELETE_HEIST_END_WINNER_SCENE_ENTITIES(sHeistWinnerSceneData)
	
	STOP_CELEBRATION_SCREEN_ANIM(sCelebrationData, GET_CELEBRATION_SCREEN_NAME())
	
	g_sTransitionSessionData.bMaintainCelebrationsScript = FALSE
	
	Clear_Any_Objective_Text_From_This_Script()
	
	PRINTLN("[SAC] - BUSY SPINNER OFF - script cleanup - call 5.")
	BUSYSPINNER_OFF()
	
	IF IS_AUDIO_SCENE_ACTIVE("MP_CELEB_SCREEN_SCENE")
   		STOP_AUDIO_SCENE("MP_CELEB_SCREEN_SCENE")
	ENDIF
	
	SET_LOCAL_PLAYER_VISIBLE_IN_CUTSCENE(TRUE, TRUE)
	
	IF DOES_CAM_EXIST(pointAtPlayerCam)
		SET_CAM_ACTIVE(pointAtPlayerCam, FALSE)
		DESTROY_CAM(pointAtPlayerCam)
		PRINTLN("[SAC] - cleaned up pointAtPlayerCam.")
	ENDIF
	
	PRINTLN("[SAC] - reset flag bOkToKillCelebrationScript, call 1.")
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.bOkToKillCelebrationScript = FALSE
	g_TransitionSessionNonResetVars.sPostMissionCleanupData.vPostMissionPos = << 0.0, 0.0, 0.0 >>
	g_TransitionSessionNonResetVars.sPostMissionCleanupData.fPostMissionPosSearchRadius = 0.0
	g_TransitionSessionNonResetVars.sPostMissionCleanupData.vPostMissionPointOfInterest = << 0.0, 0.0, 0.0 >>
	g_TransitionSessionNonResetVars.sPostMissionCleanupData.bPostMissionPosIgnoreExclusionZones = FALSE
	g_TransitionSessionNonResetVars.sPostMissionCleanupData.bForcePostMissionWarpCreatorPosition = FALSE
	g_TransitionSessionNonResetVars.sPostMissionCleanupData.fDistanceFromFreemodeEvent = 0.0
	g_TransitionSessionNonResetVars.sPostMissionCleanupData.vFreemodeActionCoords = << 0.0, 0.0, 0.0 >>
	g_TransitionSessionNonResetVars.sPostMissionCleanupData.bSpawnNearEvent = FALSE
	g_TransitionSessionNonResetVars.sPostMissionCleanupData.iWarpForPvAtEvent = 0
	g_TransitionSessionNonResetVars.sPostMissionCleanupData.bPausePvCreation = FALSE
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.bGangOpsTransition = FALSE
	
	IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bGangOpsTransition
		IF NOT IS_BIT_SET(iCelebrationGeneralBitset, CELEB_GENERAL_STOPPED_STATS_SCREEN_MUSIC_EVENT)
		AND NOT IS_BIT_SET(iCelebrationGeneralBitset, CELEB_GENERAL_STARTED_STATS_SCREEN_MUSIC_EVENT)
			TRIGGER_MUSIC_EVENT("HEIST_STATS_SCREEN_STOP")
			PRINTLN("[SAC] - triggered music event HEIST_STATS_SCREEN_STOP.")
		ENDIF
	ENDIF
	
	INT iCounter
	REPEAT COUNT_OF(g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPlayerNameHash) iCounter
		g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPlayerNameHash[iCounter] = -1
		PRINTLN("[SAC] - reset iPlayerNameHash[", iCounter, "] to -1.")
	ENDREPEAT
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPostMissionSceneId = -1
	
	GAMER_HANDLE tempHandle
	g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPackedInt  = 0
	g_TransitionSessionNonResetVars.sPostMissionCleanupData.ownerHandle = tempHandle
	SET_POST_MISSION_PLACE_IN_APARTMENT_FLAG(FALSE)
	g_TransitionSessionNonResetVars.sPostMissionCleanupData.vApartmentCoords = << 0.0, 0.0, 0.0 >>
	g_TransitionSessionNonResetVars.sPostMissionCleanupData.iApartmentLocation = -1
	g_TransitionSessionNonResetVars.sPostMissionCleanupData.iInvitedPropertyNum = -1
	g_TransitionSessionNonResetVars.sPostMissionCleanupData.iInvitedPropertyInstance = -1
	g_TransitionSessionNonResetVars.sPostMissionCleanupData.iInvitedPropertyEntrance = -1
	g_TransitionSessionNonResetVars.sPostMissionCleanupData.iApartmentCustomVariation = -1
	g_TransitionSessionNonResetVars.sPostMissionCleanupData.bActivatedCutsomaprtmentIpls = FALSE
	PRINTLN("[SAC] - reset packed int, call 0.")
	
	CELEB_TURN_NIGHTVISION_OFF()
	TOGGLE_PLAYER_DAMAGE_OVERLAY(TRUE)
	CLEAR_ALL_CELEBRATION_POST_FX()
	TOGGLE_RENDERPHASES(TRUE)
	SET_GAME_PAUSES_FOR_STREAMING(TRUE)
	PRINTLN("[SAC] - called TOGGLE_PLAYER_DAMAGE_OVERLAY(TRUE)")
	PRINTLN("[SAC] - called CLEAR_ALL_CELEBRATION_POST_FX()")	
	PRINTLN("[SAC] - TOGGLE_RENDERPHASES(TRUE)")
	PRINTLN("[SAC] - CELEB_TURN_NIGHTVISION_OFF()")
	PRINTLN("[SAC] - SET_GAME_PAUSES_FOR_STREAMING(TRUE). Call 3.")
	DISABLE_NIGHT_VISION_CONTROLLER(FALSE)
	
	IF NOT IS_RECTANGLE_FADED_OUT()
	AND NOT IS_RECTANGLE_FADING_OUT()
		PRINTLN("[SAC] - rectangle is faded in while in script cleanup, setting to fade out.")
		ACTIVATE_RECTANGLE_FADE_IN(FALSE)
		ACTIVATE_RECTANGLE_FADE_OUT(TRUE)
	ENDIF
	
	IF NOT IS_PLAYER_SCTV(PLAYER_ID())
	//AND NOT GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bJoinedMissionAsSpectator
		CLEAR_FOCUS()
		PRINTLN("[SAC] - script_cleanup, called CLEAR_FOCUS.")
	ELSE
		CLEAR_SPECTATOR_OVERRIDE_COORDS()
		PRINTLN("[SAC] - script_cleanup, called CLEAR_SPECTATOR_OVERRIDE_COORDS.")
	ENDIF
						
	IF IS_PLAYER_SCTV(PLAYER_ID())
	//OR GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bJoinedMissionAsSpectator
		CLEANUP_MP_CUTSCENE(TRUE, FALSE)
	ENDIF
	
	IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingPilotSchoolCelebration
	OR IS_PLAYER_SCTV(PLAYER_ID())
	OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bJoinedMissionAsSpectator)//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bJoinedMissionAsSpectator
		SET_CELEBRATION_AUDIO_STAGE(eCAS_OFF)
		SET_GAME_PAUSES_FOR_STREAMING(TRUE)
		PRINTLN("[SAC] - called SET_GAME_PAUSES_FOR_STREAMING(TRUE). Call B.")
		STOP_CUTSCENE_AUDIO()
	ENDIF
	
	SET_HIDOF_OVERRIDE(FALSE, FALSE, 0, 0, 0, 0)
	
	IF IS_NET_PLAYER_OK(PLAYER_ID(), FALSE)
		//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bDoingHeistCelebration = FALSE
		CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bDoingHeistCelebration)
		//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bHeistCelebrationShardHasFallenAway = FALSE
		CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bHeistCelebrationShardHasFallenAway)
	ENDIF
	
	RELEASE_NAMED_SCRIPT_AUDIO_BANK("HUD_321_GO")
	
	RESET_CELEBRATION_DATA(sCelebrationData)
	
	CLEANUP_STAND_ALONE_CELEBRATION_SCREEN_GLOBALS(FALSE)
	
	SET_CELEBRATION_SCREEN_AS_ACTIVE(FALSE, bActivateInteractionMenuOnCleanup)
	
	SET_CELEB_BLOCK_RESURRECT_RENDERPHASE_UNPAUSE(FALSE)
	
	PRINTLN("[SAC] - completed script cleanup.")
	
	TERMINATE_THIS_THREAD()

ENDPROC

PROC PLAY_HEIST_CELEB_PASS_POST_FX()
	
	PLAY_HEIST_PASS_FX("HeistCelebPass")
	PLAY_SOUND_FRONTEND(-1, "Hit_1", "LONG_PLAYER_SWITCH_SOUNDS")
	
ENDPROC

PROC FREEZE_SCREEN_FOR_CELEBRATION(BOOL bPlayHeistCelebPassPostFx = TRUE, BOOL bFreezeScreen = TRUE, BOOL bRemoveNightvision = TRUE, BOOL bTerminatePreLoadfx = TRUE)
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[SAC] - FREEZE_SCREEN_FOR_CELEBRATION has been called:")
		DEBUG_PRINTCALLSTACK()
	#ENDIF
	
	// Turn off some annoying overlays.
	IF bRemoveNightvision
		CELEB_TURN_NIGHTVISION_OFF()
		TOGGLE_PLAYER_DAMAGE_OVERLAY(FALSE)
	ENDIF
	
	// Pause the rendering.
	IF bFreezeScreen
		PRINTLN("[SAC] - FREEZE_SCREEN_FOR_CELEBRATION - bFreezeScreen = TRUE")
		TOGGLE_RENDERPHASES(FALSE, TRUE)
		
		
		IF Is_Player_Currently_On_MP_Heist(PLAYER_ID())
		OR Is_Player_Currently_On_MP_Heist_Planning(PLAYER_ID())
			
			//2230247
			FLOAT fNearCam = GET_FINAL_RENDERED_CAM_NEAR_DOF()
			FLOAT fFarCam = GET_FINAL_RENDERED_CAM_FAR_DOF()
			
			SET_HIDOF_OVERRIDE(TRUE, TRUE, 0, fNearCam, fFarCam, fFarCam + 25.0)
		
			IF Is_Player_Currently_On_MP_Heist(PLAYER_ID())
				PLAY_SOUND_FRONTEND(-1,"Pre_Screen_Stinger","DLC_HEISTS_FINALE_SCREEN_SOUNDS")
				PRINTLN("[SAC] - [RCC MISSION] - [NETCELEBRATION] - [CELEB_AUDIO] - FREEZE_SCREEN_FOR_CELEBRATION - played Pre_Screen_Stinger, DLC_HEISTS_FINALE_SCREEN_SOUNDS")
			ELIF Is_Player_Currently_On_MP_Heist_Planning(PLAYER_ID())
				PLAY_SOUND_FRONTEND(-1,"Pre_Screen_Stinger","DLC_HEISTS_PREP_SCREEN_SOUNDS")
				PRINTLN("[SAC] - [RCC MISSION] - [NETCELEBRATION] - [CELEB_AUDIO] - FREEZE_SCREEN_FOR_CELEBRATION - played Pre_Screen_Stinger, DLC_HEISTS_PREP_SCREEN_SOUNDS")
			ENDIF
			
		ENDIF
	ELSE
		PRINTLN("[SAC] - FREEZE_SCREEN_FOR_CELEBRATION - bFreezeScreen = FALSE")
	ENDIF
	
	IF bTerminatePreLoadfx
		IF ANIMPOSTFX_IS_RUNNING("MP_Celeb_Preload_Fade")
			ANIMPOSTFX_STOP("MP_Celeb_Preload_Fade")
		ENDIF
	ENDIF
	
	// Stop any previous post fx and play the pass fx.
	IF bPlayHeistCelebPassPostFx
		PLAY_HEIST_CELEB_PASS_POST_FX()
	ENDIF
	
	// start timer for initial fx.
	START_NET_TIMER(timerFrozenForCelebrationScreen)
	
ENDPROC

/// PURPOSE:
///    Do necessary pre game start initialisation.
PROC PROCESS_PRE_GAME()
	
	//This marks the script as a net script, and handles any instancing setup. 
	//SETUP_MP_MISSION_FOR_NETWORK(GET_MAX_NUM_PARTICIPANTS_FOR_MP_MISSION(missionScriptArgs.mission),  missionScriptArgs)
	
//	NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(MAX_NUM_SCRIPT_PARTICIPANTS, FALSE)
	
	NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()
	g_sTransitionSessionData.bMaintainCelebrationsScript = TRUE
	//This makes sure the net script is active, waits until it is.
//	HANDLE_NET_SCRIPT_INITIALISATION()
	
//	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(standAloneData, SIZE_OF(standAloneData))
//	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(standAloneData, SIZE_OF(standAloneData))
	
	// KGM: Wait for the first network broadcast before moving on
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
//	IF NOT Wait_For_First_Network_Broadcast()
//		PRINTLN("[SAC] - Wait_For_First_Network_Broadcast() returned FALSE, calling SCRIPT_CLEANUP and bailing.")
//		SCRIPT_CLEANUP()
//	ENDIF

	//This script will not be paused if another script calls PAUSE_GAME
	SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
	
	IF SHOULD_LAUNCH_HEIST_TUTORIAL_MID_STRAND_CUT_SCENE()
		g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingAfterApartmentPanHeistCelebration = TRUE
		PRINTLN("[RCC MISSION] - [PMC] - [NETCELEBRATION] - [SAC] - GET_HEIST_TUTORIAL_MID_CUTSCENE_REQUIRED = TRUE, setting bDoingAfterApartmentPanHeistCelebration = TRUE.")
	ENDIF
	
	SET_MY_SCENE_POSITION_INDEX(sHeistWinnerSceneData)
	
	IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPostMissionSceneId != SIS_POST_APARTMENT
	AND g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPostMissionSceneId != SIS_POST_STRIP_CLUB
		g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingHandOverToNeilCelebration = TRUE
		PRINTLN("[SAC] - Set bDoingHandOverToNeilCelebration to TRUE.")
	ELSE
		PRINTLN("[SAC] - set bDoingHandOverToNeilCelebration to FALSE.")
	ENDIF
	
	IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPostMissionSceneId = SIS_POST_STRIP_CLUB
		g_TransitionSessionNonResetVars.sGlobalCelebrationData.bBlockStripClubAssets = TRUE
		PRINTLN("[SAC] - set bBlockStripClubAssets = TRUE.")
	ENDIF
	
	IF NOT IS_PLAYER_SCTV(PLAYER_ID())
	AND NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bJoinedMissionAsSpectator)//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bJoinedMissionAsSpectator
		SET_LOCAL_PLAYER_FOR_MOCAP(FALSE, FALSE)
	ENDIF
	
	IF HAS_TRANSITION_SESSION_JIPED_FROM_OTHER_SESSION()
	AND NOT SHOULD_TRANSITION_SESSION_KILL_ALL_MP_SCRIPTS()
		PRINTLN("[SAC] - HAS_TRANSITION_SESSION_JIPED_FROM_OTHER_SESSION = TRUE and SHOULD_TRANSITION_SESSION_KILL_ALL_MP_SCRIPTS = FALSE, calling CLEAR_TRANSITION_SESSION_JIPED_FROM_OTHER_SESSION under instruction of Bobby.")
		CLEAR_TRANSITION_SESSION_JIPED_FROM_OTHER_SESSION()
	ENDIF
	
	IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingHeistCelebration
		SET_GAME_PAUSES_FOR_STREAMING(FALSE)
		PRINTLN("[SAC] - called SET_GAME_PAUSES_FOR_STREAMING(FALSE).")
	ENDIF
	
	SET_CELEB_BLOCK_RESURRECT_RENDERPHASE_UNPAUSE(TRUE)
	
	PRINTLN("[SAC] - Completed process pre game.")
	
	SET_GAME_STATE(eGAME_STATE_INI)
	
ENDPROC

PROC MAINTAIN_POINT_AT_PLAYER_CAMERA_CUT()
	
	IF NOT IS_BIT_SET(iCelebrationGeneralBitset, CELEB_GENERAL_COMPLETED_POINT_AT_PLAYER_CAMERA_CUT)
		
		BOOL bFailsafeEnabled
		
		IF NOT HAS_NET_TIMER_STARTED(timerPlaceCameraFailSafeTimer)
			START_NET_TIMER(timerPlaceCameraFailSafeTimer)
		ELSE
			IF HAS_NET_TIMER_EXPIRED(timerPlaceCameraFailSafeTimer, 5000)
				bFailsafeEnabled = TRUE
			ENDIF
		ENDIF
		
		IF bFailsafeEnabled
			
			FREEZE_SCREEN_FOR_CELEBRATION(FALSE, TRUE, TRUE, TRUE)
			SET_BIT(iCelebrationGeneralBitset, CELEB_GENERAL_COMPLETED_POINT_AT_PLAYER_CAMERA_CUT)
			PRINTLN("[SAC] - MAINTAIN_POINT_AT_PLAYER_CAMERA_CUT - bFailsafeEnabled - forcing freeze screen and setting bit CELEB_GENERAL_COMPLETED_POINT_AT_PLAYER_CAMERA_CUT.")
			SCRIPT_ASSERT("[SAC] - MAINTAIN_POINT_AT_PLAYER_CAMERA_CUT failsafe enabled.")
			
		ELSE
			
			IF NOT g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoFacePlayerCamCut
				
				SWITCH iPointAtPlayerCameraCutStage
					CASE 0
						PRINTLN("[SAC] - bDoFacePlayerCamCut = FALSE.")
						FREEZE_SCREEN_FOR_CELEBRATION(FALSE, FALSE, FALSE)
						SET_CELEBRATION_AUDIO_STAGE(eCAS_BLUR_SCREEN_SCENE)
						PRINTLN("[SAC] - started MP_BLUR_PRE_CELEB_SCREEN_SCENE.")
						iPointAtPlayerCameraCutStage++
					BREAK
					CASE 1
						SET_BIT(iCelebrationGeneralBitset, CELEB_GENERAL_COMPLETED_POINT_AT_PLAYER_CAMERA_CUT)
						PRINTLN("[SAC] - set bit CELEB_GENERAL_COMPLETED_POINT_AT_PLAYER_CAMERA_CUT.")
					BREAK
				ENDSWITCH
				
			ELSE
				
				SWITCH iPointAtPlayerCameraCutStage
				
					CASE 0
						IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							IF ACTIVATE_CINEMATIC_CAM_FOR_IN_VEHICLE_CELEBRATION_SCREEN()
								PRINTLN("[SAC] - ACTIVATE_CINEMATIC_CAM_FOR_IN_VEHICLE_CELEBRATION_SCREEN(TRUE)")
								SET_CELEBRATION_AUDIO_STAGE(eCAS_BLUR_SCREEN_SCENE)
								FREEZE_SCREEN_FOR_CELEBRATION()
								iPointAtPlayerCameraCutStage = 2
							ENDIF
						ELSE
							IF PLACE_CAMERA_FOR_CELEBRATION_SCREEN(sCelebrationData, pointAtPlayerCam, bPointAtPlayerCamSuccessfullyPlaced)
								PRINTLN("[SAC] - PLACE_CAMERA_FOR_CELEBRATION_SCREEN = TRUE, bPointAtPlayerCamSuccessfullyPlaced = ", bPointAtPlayerCamSuccessfullyPlaced)
								IF bPointAtPlayerCamSuccessfullyPlaced
									PRINTLN("[SAC] - bPointAtPlayerCamSuccessfullyPlaced = TRUE")
									SET_CAM_ACTIVE(pointAtPlayerCam, TRUE)
									RENDER_SCRIPT_CAMS(TRUE, FALSE)
									SET_CELEBRATION_AUDIO_STAGE(eCAS_BLUR_SCREEN_SCENE)
									FREEZE_SCREEN_FOR_CELEBRATION(TRUE, FALSE, TRUE)
									iPointAtPlayerCameraCutStage++
								ELSE
									PRINTLN("[SAC] - bPointAtPlayerCamSuccessfullyPlaced = FALSE")
									FREEZE_SCREEN_FOR_CELEBRATION()
									iPointAtPlayerCameraCutStage = 2
									PRINTLN("[SAC] - set bit CELEB_GENERAL_COMPLETED_POINT_AT_PLAYER_CAMERA_CUT.")
								ENDIF
							ENDIF
						ENDIF
					BREAK
					
					CASE 1
						IF IS_CAM_ACTIVE(pointAtPlayerCam)
							IF IS_CAM_RENDERING(pointAtPlayerCam)
								BOOL bPlayPostHeistPassedPostFx
								BOOL bFreezeScreen
								BOOL bRemoveNightVision
								IF bPointAtPlayerCamSuccessfullyPlaced
									bFreezeScreen = TRUE
								ENDIF
								FREEZE_SCREEN_FOR_CELEBRATION(bPlayPostHeistPassedPostFx, bFreezeScreen, bRemoveNightVision)
								iPointAtPlayerCameraCutStage++
								PRINTLN("[SAC] - set bit CELEB_GENERAL_COMPLETED_POINT_AT_PLAYER_CAMERA_CUT.")
							ENDIF
						ENDIF
					BREAK
					
					CASE 2
						IF NOT GET_TOGGLE_PAUSED_RENDERPHASES_STATUS()
							SET_BIT(iCelebrationGeneralBitset, CELEB_GENERAL_COMPLETED_POINT_AT_PLAYER_CAMERA_CUT)
							PRINTLN("[SAC] - set bit CELEB_GENERAL_COMPLETED_POINT_AT_PLAYER_CAMERA_CUT.")
						ENDIF
					BREAK
					
				ENDSWITCH
				
			ENDIF
			
		ENDIF
	
	ENDIF
		
ENDPROC

FUNC BOOL IS_PLAYER_FROM_MY_HEIST_CELEBRATION(PLAYER_INDEX playerId)
	
	INT iHashCount
	
	REPEAT COUNT_OF(g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPlayerNameHash) iHashCount
		IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPlayerNameHash[iHashCount] = NETWORK_HASH_FROM_PLAYER_HANDLE(playerId)
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL HAVE_ALL_PLAYERS_HEIST_CELEBRATION_SHARD_FALLEN_AWAY()
	
	INT i
	
	REPEAT NUM_NETWORK_PLAYERS i
		IF IS_NET_PLAYER_OK(INT_TO_NATIVE(PLAYER_INDEX, i), FALSE)
			IF IS_PLAYER_FROM_MY_HEIST_CELEBRATION(INT_TO_NATIVE(PLAYER_INDEX, i))
				IF IS_BIT_SET(GlobalplayerBD_FM[i].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bDoingHeistCelebration)//GlobalplayerBD_FM[i].bDoingHeistCelebration
					IF NOT IS_BIT_SET(GlobalplayerBD_FM[i].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bHeistCelebrationShardHasFallenAway)//GlobalplayerBD_FM[i].bHeistCelebrationShardHasFallenAway
						PRINTLN("[SAC] - HAVE_ALL_PLAYERS_HEIST_CELEBRATION_SHARD_FALLEN_AWAY retuning FALSE for player ", i)
						g_TransitionSessionNonResetVars.sGlobalCelebrationData.bHaveAllHeistPlayersCelebrationShardsFallenAway = FALSE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
	
ENDFUNC

/// PURPOSE:
///    Processes server 
PROC PROCESS_STAND_ALONE_CELEBRATION_LOGIC()
	
	STRING strScreenName
	STRING strColourBackground
	
	STOP_CONTROL_SHAKE(PLAYER_CONTROL) // No shaky pads during the celebrations - B*1837406.
	
	// No pausing the celebration screen.
	IF IS_PAUSE_MENU_ACTIVE()
		SET_FRONTEND_ACTIVE(FALSE)
		PRINTLN("[RCC MISSION] - [SAC] - called SET_FRONTEND_ACTIVE(FALSE) to remove pause menu for celebration screen.")
	ENDIF
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_FRONTEND_PAUSE)
	DISABLE_FRONTEND_THIS_FRAME()
	
	IF NOT g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingAfterApartmentPanHeistCelebration
		IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingHeistCelebration
			IF NOT IS_BIT_SET(iCelebrationGeneralBitset, CELEB_GENERAL_BITSET_PRE_LOAD_WARP_TRIGGERED)
				IF IS_BIT_SET(iCelebrationGeneralBitset, CELEB_GENERAL_COMPLETED_POINT_AT_PLAYER_CAMERA_CUT) // Need this to be done first before we warp player around.
					PRINTLN("[SAC] - CELEB_GENERAL_COMPLETED_POINT_AT_PLAYER_CAMERA_CUT bit is set.")
					INITIALISE_HEIST_END_WINNER_SCENE_DATA(sHeistWinnerSceneData)
					IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingHeistEndWinnerScene
						PRINTLN("[SAC] - bDoingHeistEndWinnerScene = TRUE, creating winner scene entities.")
						IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bCloningComplete
							PRINTLN("[SAC] - bCloningComplete = TRUE, creating winner scene entities.")
							IF CREATE_HEIST_END_WINNER_SCENE_ENTITIES(sHeistWinnerSceneData)
								PRINTLN("[SAC] - CREATE_HEIST_END_WINNER_SCENE_ENTITIES = TRUE, setting CELEB_GENERAL_BITSET_PRE_LOAD_WARP_TRIGGERED.")
								SET_BIT(iCelebrationGeneralBitset, CELEB_GENERAL_BITSET_PRE_LOAD_WARP_TRIGGERED)
							ENDIF
						ENDIF
					ELSE
						PRINTLN("[SAC] - bDoingHeistEndWinnerScene = FALSE, setting CELEB_GENERAL_BITSET_PRE_LOAD_WARP_TRIGGERED.")
						SET_BIT(iCelebrationGeneralBitset, CELEB_GENERAL_BITSET_PRE_LOAD_WARP_TRIGGERED)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	// Update broadcast version of if I joined heist as spectator. 
	IF NETWORK_IS_GAME_IN_PROGRESS()
		//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bJoinedMissionAsSpectator = g_TransitionSessionNonResetVars.sGlobalCelebrationData.bJoinedMissionAsSpectator
		IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bJoinedMissionAsSpectator
			SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bJoinedMissionAsSpectator)
		ELSE
			CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bJoinedMissionAsSpectator)
		ENDIF
	ENDIf
	
	SWITCH GET_CELEBRATION_STATE(0)
		
		CASE eCELEBRATIONSTATE_INIT_MOVIE
			
			IF IS_BIT_SET(iCelebrationGeneralBitset, CELEB_GENERAL_COMPLETED_POINT_AT_PLAYER_CAMERA_CUT)
				
				INITIALISE_CELEBRATION_SCREEN_DATA(sCelebrationData)
				SET_CELEBRATION_SCREEN_AS_ACTIVE(TRUE)
				KILL_UI_FOR_CELEBRATION_SCREEN()
				HIDE_UI_FOR_CELEBRATION_SCREEN_THIS_FRAME()
				
				IF NOT IS_SCREEN_FADED_IN()
					IF NOT IS_SCREEN_FADING_IN()
						DO_SCREEN_FADE_IN(500)
						PRINTLN("[SAC] - screen not faded or fading in. Called DO_SCREEN_FADE_IN(1000).")	
					ENDIF
				ENDIF
				
				IF NOT g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingAfterApartmentPanHeistCelebration
					IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
						SET_PLAYER_CLOTHES_FOR_RETURN_TO_FREEMODE()
						PRINTLN("[SAC] - called SET_PED_VARIATIONS and removed other props etc from the player.")
					ENDIF
				ENDIF
				
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_CLEAR_TASKS | NSPC_REMOVE_FIRES | NSPC_SET_INVISIBLE | NSPC_REMOVE_EXPLOSIONS | NSPC_FREEZE_POSITION)
										
				PRINTLN("[SAC] - completed inititialisation of celebration data.")
				SET_CELEBRATION_STATE(eCELEBRATIONSTATE_LOAD_MOVIE)
			ELSE
				PRINTLN("[SAC] - CELEB_GENERAL_COMPLETED_POINT_AT_PLAYER_CAMERA_CUT not set.")
			ENDIF
			
		BREAK
		
		CASE eCELEBRATIONSTATE_LOAD_MOVIE
			
			INT iMoneyTexture
			
			CELEBRATION_SCREEN_TYPE eCelebType
			eCelebType = CELEBRATION_STANDARD
			IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bGangOpsTransition
				eCelebType = CELEBRATION_GANGOPS
			ELIF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingHeistCelebration
				eCelebType = CELEBRATION_HEIST
			ENDIF
			IF sHeistWinnerSceneData.eSceneType = eHEISTWINNERSCENETYPE_CASINO_HEIST_ROOF
			OR sHeistWinnerSceneData.eSceneType = eHEISTWINNERSCENETYPE_CASINO_HEIST_BAR
			#IF FEATURE_HEIST_ISLAND
			OR sHeistWinnerSceneData.eSceneType = eHEISTWINNERSCENETYPE_ISLAND_HEIST
			#ENDIF
				eCelebType = CELEBRATION_HEIST
			ENDIF
			
			REQUEST_CELEBRATION_SCREEN(sCelebrationData, eCelebType)
			
			IF HAS_CELEBRATION_SCREEN_LOADED(sCelebrationData)
			AND REQUEST_SCRIPT_AUDIO_BANK("HUD_321_GO")
				
				PRINTLN("[SAC] - celebration movies loaded.")
				
				IF IS_HELP_MESSAGE_BEING_DISPLAYED()
					CLEAR_HELP()
				ENDIF
				
				IF IS_AUDIO_SCENE_ACTIVE("MP_LEADERBOARD_SCENE")
					STOP_AUDIO_SCENE("MP_LEADERBOARD_SCENE")
				ENDIF
				
				strScreenName = GET_CELEBRATION_SCREEN_NAME()
				
				// Create celebration screen and add data to it.
				IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingPilotSchoolCelebration
					
					SET_CELEBRATION_AUDIO_STAGE(eCAS_CELEB_SCREEN_SCENE)
					CREATE_CELEBRATION_SCREEN(sCelebrationData, strScreenName, "HUD_COLOUR_BLACK")
					ADD_PILOT_SCHOOL_DATA_TO_CELEBRATION_SCREEN(sCelebrationData)
					ADD_BACKGROUND_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName, -1, iMoneyTexture)
					SHOW_CELEBRATION_SCREEN(sCelebrationData, strScreenName)
					SET_CHAT_ENABLED_FOR_CELEBRATION_SCREEN(TRUE)
					RESET_NET_TIMER(sCelebrationData.sCelebrationTimer)
					START_NET_TIMER(sCelebrationData.sCelebrationTimer, g_TransitionSessionNonResetVars.sGlobalCelebrationData.bInTestMode)
					
					IF NOT IS_SCREEN_FADED_IN()
						IF NOT IS_SCREEN_FADING_IN()
							DO_SCREEN_FADE_IN(500)
							PRINTLN("[SAC] - screen not faded or fading in. Called DO_SCREEN_FADE_IN(500).")	
						ENDIF
					ENDIF
					
					SET_CELEBRATION_STATE(eCELEBRATIONSTATE_DRAW_MOVIE)
				
				
				ELIF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingHeistCelebration
					
					IF HAS_NET_TIMER_EXPIRED(timerFrozenForCelebrationScreen, 1000)
						
						INT iCreateStatWallAudioType
						IF NOT g_TransitionSessionNonResetVars.sGlobalCelebrationData.bPassedMission
							iCreateStatWallAudioType = 2
							PRINTLN("[SAC] - failed mission - set iCreateStatWallAudioType = ", iCreateStatWallAudioType)
						ELSE
							IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.iHeistType = ciCELEBRATION_HEIST_TYPE__PREP
								iCreateStatWallAudioType = 1
							PRINTLN("[SAC] - failed mission - set iCreateStatWallAudioType = ", iCreateStatWallAudioType)
							ELIF g_TransitionSessionNonResetVars.sGlobalCelebrationData.iHeistType = ciCELEBRATION_HEIST_TYPE__FINALE
								iCreateStatWallAudioType = 3
							PRINTLN("[SAC] - failed mission - set iCreateStatWallAudioType = ", iCreateStatWallAudioType)
							ENDIF
						ENDIF
						
						strColourBackground = "HUD_COLOUR_HSHARD"
						
						CREATE_CELEBRATION_SCREEN(sCelebrationData, strScreenName, strColourBackground, FALSE, iCreateStatWallAudioType)
						ADD_END_HEIST_DATA_TO_CELEBRATION_SCREEN(sCelebrationData)
						
						sCelebrationData.iEstimatedScreenDuration += 5000
						iMoneyTexture = 2
						
						#IF IS_DEBUG_BUILD
						INT iTempValue
						iTempValue = GET_GLOBAL_ACTIONSCRIPT_FLAG(ACTIONSCRIPT_GLOBAL_ID)
						PRINTLN("[SAC] - GET_GLOBAL_ACTIONSCRIPT_FLAG(ACTIONSCRIPT_GLOBAL_ID) = ", iTempValue)
						#ENDIF
						
						ADD_BACKGROUND_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName, -1, iMoneyTexture)
						SHOW_CELEBRATION_SCREEN(sCelebrationData, strScreenName)
						SET_CHAT_ENABLED_FOR_CELEBRATION_SCREEN(TRUE)
						RESET_NET_TIMER(sCelebrationData.sCelebrationTimer)
						START_NET_TIMER(sCelebrationData.sCelebrationTimer, g_TransitionSessionNonResetVars.sGlobalCelebrationData.bInTestMode)

						IF NOT IS_SCREEN_FADED_IN()
							IF NOT IS_SCREEN_FADING_IN()
								DO_SCREEN_FADE_IN(500)
								PRINTLN("[SAC] - screen not faded or fading in. Called DO_SCREEN_FADE_IN(500).")	
							ENDIF
						ENDIF
						
						SET_CELEBRATION_STATE(eCELEBRATIONSTATE_DRAW_MOVIE)
						
					ENDIF
					
				
				ELSE
					PRINTLN("Celebrations - no celebration type has been set, don't know which data to add to movie.")
					SCRIPT_ASSERT("Celebrations - no celebration type has been set, don't know which data to add to movie.")
				ENDIF
				
			ENDIF
			
		BREAK
		
		CASE eCELEBRATIONSTATE_DRAW_MOVIE	
			
			HIDE_UI_FOR_CELEBRATION_SCREEN_THIS_FRAME()
			
			REQUEST_SCRIPT_AUDIO_BANK("HUD_321_GO")
			
			DRAW_CELEBRATION_SCREEN_THIS_FRAME(sCelebrationData, FALSE)
			
			IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingPilotSchoolCelebration
				
				IF HAS_NET_TIMER_EXPIRED(sCelebrationData.sCelebrationTimer, sCelebrationData.iEstimatedScreenDuration, g_TransitionSessionNonResetVars.sGlobalCelebrationData.bInTestMode)
					
					PRINTLN("[SAC] - celebration finished.")
					PRINTLN("[SAC] - bDoingPilotSchoolCelebration = TRUE.")
					
					IF NOT IS_BIT_SET(sHeistWinnerSceneData.iBitset, HEIST_WINNER_SCENE_SWITCHED_OF_LOADING_SPINNER)
						PRINTLN("[SAC] - BUSY SPINNER OFF - call 0.")
						BUSYSPINNER_OFF()
						SET_BIT(sHeistWinnerSceneData.iBitset, HEIST_WINNER_SCENE_SWITCHED_OF_LOADING_SPINNER)
					ENDIF
					
					STOP_CELEBRATION_SCREEN_ANIM(sCelebrationData, GET_CELEBRATION_SCREEN_NAME())
					CLEANUP_CELEBRATION_SCREEN(sCelebrationData, FALSE, GET_CELEBRATION_SCREEN_NAME())
					SET_CELEBRATION_STATE(eCELEBRATIONSTATE_CLEANUP)
					
				ENDIF
					
			ELIF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingHeistCelebration
				
				IF NOT IS_BIT_SET(iCelebrationGeneralBitset, CELEB_GENERAL_BITSET_TRANSITIONED_INTO_BW)
					IF HAS_CELEBRATION_SHARD_BEGAN_TO_FALL_IN()
						ANIMPOSTFX_STOP("SuccessNeutral")
						PLAY_HEIST_PASS_FX("HeistCelebPassBW")
						SET_CELEBRATION_AUDIO_STAGE(eCAS_CELEB_SCREEN_SCENE)						
						SET_BIT(iCelebrationGeneralBitset, CELEB_GENERAL_BITSET_TRANSITIONED_INTO_BW)
						PRINTLN("[SAC] - set bit CELEB_GENERAL_BITSET_TRANSITIONED_INTO_BW.")
						IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bGangOpsTransition
							SET_LOCAL_PLAYER_FOR_MOCAP()
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT IS_BIT_SET(iCelebrationGeneralBitset, CELEB_GENERAL_BITSET_TRANSITIONED_OUT_OF_BW)
					IF HAS_CELEBRATION_SHARD_BEGAN_TO_FALL_AWAY()
						ANIMPOSTFX_STOP("SuccessNeutral")
						PLAY_HEIST_PASS_FX("HeistCelebEnd")
						SET_BIT(iCelebrationGeneralBitset, CELEB_GENERAL_BITSET_TRANSITIONED_OUT_OF_BW)
						PRINTLN("[SAC] - set bit CELEB_GENERAL_BITSET_TRANSITIONED_OUT_OF_BW.")
					ENDIF
				ENDIF
				
				IF HAS_CELEBRATION_SHARD_BEGAN_TO_FALL_IN()
					SET_CELEBRATION_AUDIO_STAGE(eCAS_CELEB_SCREEN_SCENE)
					IF NOT IS_BIT_SET(iCelebrationGeneralBitset, CELEB_GENERAL_STARTED_STATS_SCREEN_MUSIC_EVENT)
						SCRIPT_TRIGGER_MUSIC_EVENT_STOP()
						PRINTLN("[SAC] - triggered music event MP_MC_STOP.")
						TRIGGER_MUSIC_EVENT("HEIST_STATS_SCREEN_START")
						PRINTLN("[SAC] - triggered music event HEIST_STATS_SCREEN_START.")
						IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingHeistCelebration
						AND NOT g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingHeistEndWinnerScene
							// Prepare prep stop music event.
							ENABLE_PREPARE_CELEBRATION_STATS_SCREEN_STOP_MUSIC_EVENT()
						ENDIF
						SET_BIT(iCelebrationGeneralBitset, CELEB_GENERAL_STARTED_STATS_SCREEN_MUSIC_EVENT)
						PRINTLN("[SAC] - set bit CELEB_GENERAL_STARTED_STATS_SCREEN_MUSIC_EVENT.")
					ENDIF
				ENDIF
				
				IF HAS_CELEBRATION_SHARD_BEGAN_TO_FALL_AWAY()
					IF NOT IS_BIT_SET(iCelebrationGeneralBitset, CELEB_GENERAL_STOPPED_STATS_SCREEN_MUSIC_EVENT)
						IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingHeistEndWinnerScene
						OR g_TransitionSessionNonResetVars.sGlobalCelebrationData.bGangOpsTransition
							TRIGGER_MUSIC_EVENT("HEIST_STATS_SCREEN_STOP")
							PRINTLN("[SAC] - triggered music event HEIST_STATS_SCREEN_STOP.")
						ENDIF
						SET_BIT(iCelebrationGeneralBitset, CELEB_GENERAL_STOPPED_STATS_SCREEN_MUSIC_EVENT)
						PRINTLN("[SAC] - set bit CELEB_GENERAL_STOPPED_STATS_SCREEN_MUSIC_EVENT.")
					ENDIF
				ENDIF
				
				IF HAS_CELEBRATION_SHARD_FINISHED_FALL_AWAY()
					IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bGangOpsTransition
						IF NOT IS_BIT_SET(iCelebrationGeneralBitset, CELEB_GENERAL_BITSET_PLAYED_CELEB_END_FOR_1_SEC)
							PRINTLN("[SAC] - GANG_OPS")
							SET_BIT(iCelebrationGeneralBitset, CELEB_GENERAL_BITSET_PLAYED_CELEB_END_FOR_1_SEC)
						ENDIF
					ELIF NOT HAS_NET_TIMER_STARTED(timerShardFallenAway)
						START_NET_TIMER(timerShardFallenAway)
						PRINTLN("[SAC] - started net timer timerShardFallenAway.")
					ELSE
						IF HAS_NET_TIMER_EXPIRED(timerShardFallenAway, 2000)
							SET_BIT(iCelebrationGeneralBitset, CELEB_GENERAL_BITSET_PLAYED_CELEB_END_FOR_1_SEC)
							PRINTLN("[SAC] - net timer timerShardFallenAway >= 2000")
							PRINTLN("[SAC] - set bit CELEB_GENERAL_BITSET_PLAYED_CELEB_END_FOR_1_SEC.")
						ENDIF
					ENDIF	
				ENDIF
				
				IF NOT g_TransitionSessionNonResetVars.sGlobalCelebrationData.bHeistCelebrationShardHasFallenAway
					IF IS_BIT_SET(iCelebrationGeneralBitset, CELEB_GENERAL_BITSET_PLAYED_CELEB_END_FOR_1_SEC)
						g_TransitionSessionNonResetVars.sGlobalCelebrationData.bHeistCelebrationShardHasFallenAway = TRUE
						PRINTLN("[SAC] - set bHeistCelebrationShardHasFallenAway = TRUE.")
					ENDIF
				ENDIF
				
				IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bHaveAllHeistPlayersCelebrationShardsFallenAway	
				OR bWaitingOnPlayersTimeout // Assert fires if this gets set, something has gone wrong so we force our way through so nobody gets stuck.
					
					// Sort fading out as soon as possible.
					IF IS_BIT_SET(iCelebrationGeneralBitset, CELEB_GENERAL_BITSET_PLAYED_CELEB_END_FOR_1_SEC)
						IF ( g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingHeistEndWinnerScene AND NOT IS_FAKE_MULTIPLAYER_MODE_SET() )
						OR g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingAfterApartmentPanHeistCelebration
							//PRINTLN("[SAC] - BUSY SPINNER OFF - call 2.")
							BUSYSPINNER_OFF()
							IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bJoinedMissionAsSpectator)//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bJoinedMissionAsSpectator
								IF NOT IS_BIT_SET(iCelebrationGeneralBitset, CELEB_GENERAL_STARTED_WHITE_RECTANGLE_FADE)
									IF NOT IS_RECTANGLE_FADING_IN()
										IF sHeistWinnerSceneData.eSceneType = eHEISTWINNERSCENETYPE_TREVOR
											SET_RECTANGLE_COLOUR(FALSE)
										ELSE
											SET_RECTANGLE_COLOUR(TRUE)
										ENDIF
										ACTIVATE_RECTANGLE_FADE_OUT(FALSE)
										ACTIVATE_RECTANGLE_FADE_IN(TRUE)
										SET_CELEBRATION_AUDIO_STAGE(eCAS_FADE_WHITE_SCENE)
									ENDIF
									SET_BIT(iCelebrationGeneralBitset, CELEB_GENERAL_STARTED_WHITE_RECTANGLE_FADE)
								ENDIF
							ELSE
								PRINTLN("[SAC] - BUSY SPINNER OFF - call 20.")
								BUSYSPINNER_OFF()
								IF NOT IS_SCREEN_FADED_OUT()
								AND NOT IS_SCREEN_FADING_OUT()
									DO_SCREEN_FADE_OUT(500)
								ENDIF
							ENDIF
						ELSE
							PRINTLN("[SAC] - BUSY SPINNER OFF - call 3.")
							BUSYSPINNER_OFF()
							IF NOT IS_SCREEN_FADED_OUT()
							AND NOT IS_SCREEN_FADING_OUT()
								DO_SCREEN_FADE_OUT(500)
							ENDIF
						ENDIF
					ENDIF
					
					IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bOkToKillCelebrationScript
					OR g_TransitionSessionNonResetVars.sGlobalCelebrationData.bInTestMode
						
						PRINTLN("[SAC] - celebration finished.")
						PRINTLN("[SAC] - bDoingHeistCelebration = TRUE")
						
						PRINTLN("[SAC] - bOkToKillCelebrationScript = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.bOkToKillCelebrationScript)
						PRINTLN("[SAC] - bInTestMode = ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.bInTestMode)
						
						IF NOT IS_BIT_SET(sHeistWinnerSceneData.iBitset, HEIST_WINNER_SCENE_SWITCHED_OF_LOADING_SPINNER)
							PRINTLN("[SAC] - BUSY SPINNER OFF - call 1.")
							BUSYSPINNER_OFF()
							SET_BIT(sHeistWinnerSceneData.iBitset, HEIST_WINNER_SCENE_SWITCHED_OF_LOADING_SPINNER)
						ENDIF
						
						IF IS_BIT_SET(iCelebrationGeneralBitset, CELEB_GENERAL_BITSET_PLAYED_CELEB_END_FOR_1_SEC)
							
							IF IS_BIT_SET(iCelebrationGeneralBitset, CELEB_GENERAL_BITSET_COMPLETED_PLAYER_PRE_LOAD_WARP)
							OR g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingAfterApartmentPanHeistCelebration
								
								IF ( g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingHeistEndWinnerScene AND NOT IS_FAKE_MULTIPLAYER_MODE_SET() )
									IF IS_RECTANGLE_FADED_IN()
									OR IS_SCREEN_FADED_OUT()
										SET_CELEBRATION_AUDIO_STAGE(sHeistWinnerSceneData.eWinnerAudioScene)
										START_NET_TIMER(timerFadeFlashDelay)
										SET_CELEBRATION_STATE(eCELEBRATIONSTATE_DO_WINNER_SCENE)
									ELSE
										PRINTLN("[SAC] - waiting on IS_RECTANGLE_FADED_IN.")
									ENDIF
								ELSE
									PRINTLN("[SAC] - bDoingHeistEndWinnerScene = FALSE.")
									IF NOT GET_HEIST_TUTORIAL_MID_CUTSCENE_PRELOAD()
										PRINTLN("[SAC] - GET_HEIST_TUTORIAL_MID_CUTSCENE_PRELOAD = FALSE.")
										IF NOT g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingAfterApartmentPanHeistCelebration
											IF IS_SCREEN_FADED_OUT()
												IF NOT IS_BIT_SET(iCelebrationGeneralBitset, CELEB_GENERAL_STARTED_FADE_BLACK_SCENE)
													SET_CELEBRATION_AUDIO_STAGE(eCAS_FADE_BLACK_SCENE)
													SET_BIT(iCelebrationGeneralBitset, CELEB_GENERAL_STARTED_FADE_BLACK_SCENE)
												ENDIF
												IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bJoinedMissionAsSpectator)//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bJoinedMissionAsSpectator
													SWITCH iJoinedMissionAsSpectatorWarpStage
														CASE 0
															SETUP_JOINED_MISSION_AS_SPECTATOR_WARP()
															iJoinedMissionAsSpectatorWarpStage++
														BREAK
														CASE 1
															IF WARP_TO_SPAWN_LOCATION(SPAWN_LOCATION_AT_SPECIFIC_COORDS_IF_POSSIBLE, FALSE, FALSE, FALSE, FALSE)
																IF NOT IS_SCREEN_FADED_IN()
																AND NOT IS_SCREEN_FADING_IN()
																	DO_SCREEN_FADE_IN(500)
																ENDIF
																IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bJoinedMissionAsSpectator)//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bJoinedMissionAsSpectator
																	NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
																ENDIF
																CLEANUP_CELEBRATION_SCREEN(sCelebrationData, FALSE, GET_CELEBRATION_SCREEN_NAME())
																SET_CELEBRATION_STATE(eCELEBRATIONSTATE_CLEANUP)
															ENDIF
														BREAK
													ENDSWITCH
												ELSE
													IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPostMissionSceneId = SIS_POST_APARTMENT
														IF NOT STAND_ALONE_CELEBRATION_IS_PLAYER_IN_AN_APARTMENT()
															PRINTLN("[SAC] - [NETCELEBRATION] - player is not already inside an apartment.")
															IF NOT IS_BIT_SET(sHeistWinnerSceneData.iBitset, HEIST_WINNER_SCENE_INITIALISED_APARTMENT_DATA)
																SET_APARTMENT_EXTERIOR_SPAWN_DATA(sHeistWinnerSceneData)
																SET_BIT(sHeistWinnerSceneData.iBitset, HEIST_WINNER_SCENE_INITIALISED_APARTMENT_DATA)
																PRINTLN("[SAC] - [NETCELEBRATION] - set HEIST_WINNER_SCENE_INITIALISED_APARTMENT_DATA.")
															ELSE
																IF WARP_TO_OUTSIDE_APARTMENT(sHeistWinnerSceneData)
																	PRINTLN("[SAC] - [NETCELEBRATION] - WARP_TO_OUTSIDE_APARTMENT() complete.")
																	bActivateInteractionMenuOnCleanup = TRUE
																	SET_CELEBRATION_STATE(eCELEBRATIONSTATE_FADE_IN)
																ELSE
																	PRINTLN("[SAC] - [NETCELEBRATION] - waiting for WARP_TO_OUTSIDE_APARTMENT().")
																ENDIF
															ENDIF
														ELSE
															PRINTLN("[SAC] - [NETCELEBRATION] - player is already inside an apartment.")
															TRIGGER_POST_HEIST_POSITION_SYSTEMS()
															CLEANUP_CELEBRATION_SCREEN(sCelebrationData, FALSE, GET_CELEBRATION_SCREEN_NAME())
															SET_CELEBRATION_STATE(eCELEBRATIONSTATE_CLEANUP)
														ENDIF
													ELSE
														IF IS_PLAYER_SCTV(PLAYER_ID())
															IF (g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPostMissionSceneId != SIS_POST_STRIP_CLUB)
															AND (g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPostMissionSceneId != SIS_POST_APARTMENT)
																PRINTLN("[SAC] - not doing apartment setup post mission scene, using Neil's system, calling DO_SCTV_EXTERNAL_FADE_IN_AFTER_POST_CELEB.")
																DO_SCTV_EXTERNAL_FADE_IN_AFTER_POST_CELEB(TRUE)
															ENDIF
														ENDIF
														TRIGGER_POST_HEIST_POSITION_SYSTEMS()
														CLEANUP_CELEBRATION_SCREEN(sCelebrationData, FALSE, GET_CELEBRATION_SCREEN_NAME())
														SET_CELEBRATION_STATE(eCELEBRATIONSTATE_CLEANUP)
													ENDIF
												ENDIF
											ENDIF
										ELSE
											IF NOT IS_RECTANGLE_FADED_OUT()
												IF NOT IS_RECTANGLE_FADING_OUT()
													ACTIVATE_RECTANGLE_FADE_IN(FALSE)
													ACTIVATE_RECTANGLE_FADE_OUT(TRUE)
													CLEAR_ALL_CELEBRATION_POST_FX()		//	url:bugstar:2221031 - clears up HeistCelebEnd animpostfx. ST.
													SET_GAME_PAUSES_FOR_STREAMING(TRUE)
													PRINTLN("[SAC] - called SET_GAME_PAUSES_FOR_STREAMING(TRUE). Call C.")
													SET_CELEBRATION_AUDIO_STAGE(eCAS_OFF)
													TRIGGER_MUSIC_EVENT("HEIST_STATS_SCREEN_STOP")
													PRINTLN("[SAC] - triggered music event HEIST_STATS_SCREEN_STOP for flow cutscene.")
//													START_CELEBRATION_STATS_SCREEN_STOP_MUSIC_EVENT()
													CLEANUP_CELEBRATION_SCREEN(sCelebrationData, FALSE, GET_CELEBRATION_SCREEN_NAME())
												ENDIF
											ELSE
												CLEAR_ALL_CELEBRATION_POST_FX()		//	url:bugstar:2221031 - clears up HeistCelebEnd animpostfx. ST.
												SET_CELEBRATION_AUDIO_STAGE(eCAS_OFF)
												CLEANUP_CELEBRATION_SCREEN(sCelebrationData, FALSE, GET_CELEBRATION_SCREEN_NAME())
												SET_CELEBRATION_STATE(eCELEBRATIONSTATE_CLEANUP)	
											ENDIF
										ENDIF
									ELSE
										IF IS_RECTANGLE_FADED_IN()
											IF NOT HAS_NET_TIMER_STARTED(timerFadeFlashDelay)
												START_NET_TIMER(timerFadeFlashDelay)
												PRINTLN("[SAC] - [NETCELEBRATION] - started timer timerFadeFlashDelay.")
											ELSE
												IF HAS_NET_TIMER_EXPIRED(timerFadeFlashDelay, WHITE_SCREEN_HOLD_TIME)
													PRINTLN("[SAC] - [NETCELEBRATION] - timer timerFadeFlashDelay has expired = ", WHITE_SCREEN_HOLD_TIME)
													IF g_bCelebrationScreenIsActive
														PRINTLN("[SAC] - [NETCELEBRATION] - calling SET_CELEBRATION_SCREEN_AS_ACTIVE(FALSE) so SET_HEIST_TUTORIAL_MID_CUTSCENE_PRELOAD can be set to FALSE.")
														SET_CELEBRATION_SCREEN_AS_ACTIVE(FALSE, bActivateInteractionMenuOnCleanup)
													ENDIF
												ENDIF
											ENDIF
										ELSE
											PRINTLN("[SAC] - [NETCELEBRATION] - waiting for IS_RECTANGLE_FADED_IN to return TRUE.")
										ENDIF
									ENDIF
								ENDIF
								
							ELSE
								
								PRINTLN("[SAC] - [NETCELEBRATION] - waiting for pre-load warp to complete.")
								
							ENDIF
						
						ELSE
						
							PRINTLN("[SAC] - waiting on CELEB_GENERAL_BITSET_PLAYED_CELEB_END_FOR_1_SEC.")
						
						ENDIF
						
					ENDIF
				
				ELSE
					
					IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bHeistCelebrationShardHasFallenAway
						IF NOT HAS_NET_TIMER_STARTED(timerWaitingOnPlayers)
							START_NET_TIMER(timerWaitingOnPlayers)
							PRINTLN("[SAC] - started timer timerWaitingOnPlayers.")
						ELSE
							IF HAS_NET_TIMER_EXPIRED(timerWaitingOnPlayers, 4000)
								IF NOT IS_BIT_SET(sHeistWinnerSceneData.iBitset, HEIST_WINNER_SCENE_STARTED_WAITING_SPINNER)
									IF NOT BUSYSPINNER_IS_ON()
										BEGIN_TEXT_COMMAND_BUSYSPINNER_ON("CELEB_WPLYRS")
										END_TEXT_COMMAND_BUSYSPINNER_ON(ENUM_TO_INT(LOADING_ICON_LOADING))
										SET_BIT(sHeistWinnerSceneData.iBitset, HEIST_WINNER_SCENE_STARTED_WAITING_SPINNER)
										PRINTLN("[SAC] - timer timerWaitingOnPlayers has expired (4000).")
										PRINTLN("[SAC] - CELEB_WPLYRS BUSY SPINNER ON")
									ENDIF
								ENDIF
							ENDIF
							IF HAS_NET_TIMER_EXPIRED(timerWaitingOnPlayers, 60000)
								bWaitingOnPlayersTimeout = TRUE
								PRINTLN("[SAC] - other players have taken >= 60000 to finish their celebrations. Setting force through flag bWaitingOnPlayersTimeout.")
							ENDIF
						ENDIF
					ENDIF
					
				ENDIF
			
			
			ENDIF
			
		BREAK
		
		CASE eCELEBRATIONSTATE_DO_WINNER_SCENE
			
			IF MAINTAIN_HEIST_END_WINNER_SCENE(sHeistWinnerSceneData)
				g_TransitionSessionNonResetVars.sGlobalCelebrationData.bOkToKillCelebrationScript = FALSE
				SET_CELEBRATION_STATE(eCELEBRATIONSTATE_CLEANUP)
			ENDIF
			
		BREAK	
		
		CASE eCELEBRATIONSTATE_FADE_IN
				
			IF NOT IS_SCREEN_FADED_IN()
			AND NOT IS_SCREEN_FADING_IN()
				DO_SCREEN_FADE_IN(500)
				TOGGLE_PLAYER_DAMAGE_OVERLAY(TRUE)
				ANIMPOSTFX_STOP_ALL()
				TOGGLE_RENDERPHASES(TRUE)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				SET_GAME_PAUSES_FOR_STREAMING(TRUE)
				PRINTLN("[SAC] - called SET_GAME_PAUSES_FOR_STREAMING(TRUE). Call D.")
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			ENDIF
			
			IF IS_SCREEN_FADED_IN()
				STOP_CELEBRATION_SCREEN_ANIM(sCelebrationData, GET_CELEBRATION_SCREEN_NAME())
				CLEANUP_CELEBRATION_SCREEN(sCelebrationData, FALSE, GET_CELEBRATION_SCREEN_NAME())
				IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingHeistCelebration
					IF NOT g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingHeistEndWinnerScene
						START_CELEBRATION_STATS_SCREEN_STOP_MUSIC_EVENT()
					ENDIF
				ENDIF
				SET_CELEBRATION_AUDIO_STAGE(eCAS_OFF)
				SET_CELEBRATION_STATE(eCELEBRATIONSTATE_CLEANUP)
			ENDIF
			
		BREAK
		
		CASE eCELEBRATIONSTATE_APARTMENT_FAILSAFE
			
			SWITCH iApartmentFailsafeStage
				
				CASE 0
					IF NOT IS_BIT_SET(sHeistWinnerSceneData.iBitset, HEIST_WINNER_SCENE_INITIALISED_APARTMENT_DATA)
						SET_APARTMENT_EXTERIOR_SPAWN_DATA(sHeistWinnerSceneData)
						SET_BIT(sHeistWinnerSceneData.iBitset, HEIST_WINNER_SCENE_INITIALISED_APARTMENT_DATA)
						PRINTLN("[SAC] - [NETCELEBRATION] - set HEIST_WINNER_SCENE_INITIALISED_APARTMENT_DATA.")
					ELSE
						IF WARP_TO_OUTSIDE_APARTMENT(sHeistWinnerSceneData)
							PRINTLN("[SAC] - [NETCELEBRATION] - eCELEBRATIONSTATE_APARTMENT_FAILSAFE - WARP_TO_OUTSIDE_APARTMENT() complete.")
							bActivateInteractionMenuOnCleanup = TRUE
							iApartmentFailsafeStage++
							PRINTLN("[SAC] - [NETCELEBRATION] - eCELEBRATIONSTATE_APARTMENT_FAILSAFE - iApartmentFailsafeStage = ", iApartmentFailsafeStage)
						ELSE
							PRINTLN("[SAC] - [NETCELEBRATION] - eCELEBRATIONSTATE_APARTMENT_FAILSAFE - waiting for WARP_TO_OUTSIDE_APARTMENT().")
						ENDIF
					ENDIF
				BREAK
				
				CASE 1
				
					TOGGLE_PLAYER_DAMAGE_OVERLAY(TRUE)
					ANIMPOSTFX_STOP_ALL()
					TOGGLE_RENDERPHASES(TRUE)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					SET_GAMEPLAY_CAM_RELATIVE_PITCH()
					SET_GAME_PAUSES_FOR_STREAMING(TRUE)
					PRINTLN("[SAC] - called SET_GAME_PAUSES_FOR_STREAMING(TRUE). Call Z.")
					NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					iApartmentFailsafeStage++
					PRINTLN("[SAC] - [NETCELEBRATION] - eCELEBRATIONSTATE_APARTMENT_FAILSAFE - iApartmentFailsafeStage = ", iApartmentFailsafeStage)
					
				BREAK
				
				CASE 2
				
					IF NOT IS_SCREEN_FADED_IN()
					AND NOT IS_SCREEN_FADING_IN()
						DO_SCREEN_FADE_IN(500)
					ENDIF
					
					IF IS_SCREEN_FADED_IN()
						STOP_CELEBRATION_SCREEN_ANIM(sCelebrationData, GET_CELEBRATION_SCREEN_NAME())
						CLEANUP_CELEBRATION_SCREEN(sCelebrationData, FALSE, GET_CELEBRATION_SCREEN_NAME())
						IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingHeistCelebration
							IF NOT g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingHeistEndWinnerScene
								START_CELEBRATION_STATS_SCREEN_STOP_MUSIC_EVENT()
							ENDIF
						ENDIF
						SET_CELEBRATION_AUDIO_STAGE(eCAS_OFF)
						SET_CELEBRATION_STATE(eCELEBRATIONSTATE_CLEANUP)
					ENDIF
					
				BREAK
				
			ENDSWITCH
			
		BREAK
		
		CASE eCELEBRATIONSTATE_CLEANUP		
			
			BOOL bgoToGameStateEnd
			
			IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPostMissionSceneId = SIS_POST_APARTMENT
				
				IF IS_PLAYER_SCTV(PLAYER_ID())
				OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bJoinedMissionAsSpectator)//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bJoinedMissionAsSpectator
					PRINTLN("[SAC] - IS_PLAYER_SCTV(PLAYER_ID()) = TRUE.")
					bgoToGameStateEnd = TRUE
				ELSE
				
					IF STAND_ALONE_CELEBRATION_IS_PLAYER_IN_APARTMENT_INTERIOR()
						PRINTLN("[SAC] - STAND_ALONE_CELEBRATION_IS_PLAYER_IN_APARTMENT_INTERIOR = TRUE.")
						IF STAND_ALONE_CELEBRATION_IS_PLAYER_RUNNING_APARTMENT_INTERIOR_SCRIPT()
							PRINTLN("[SAC] - STAND_ALONE_CELEBRATION_IS_PLAYER_RUNNING_APARTMENT_INTERIOR_SCRIPT = TRUE, bgoToGameStateEnd = TRUE")
							bgoToGameStateEnd = TRUE
						ELSE
							PRINTLN("[SAC] - STAND_ALONE_CELEBRATION_IS_PLAYER_RUNNING_APARTMENT_INTERIOR_SCRIPT = FALSE.")
						ENDIF
					ELSE
						PRINTLN("[SAC] - STAND_ALONE_CELEBRATION_IS_PLAYER_IN_APARTMENT_INTERIOR = FALSE.")
					ENDIF
					
					IF IS_SCREEN_FADED_OUT()
						IF NOT HAS_NET_TIMER_STARTED(waitForApartmentFadeIntimer)
							START_NET_TIMER(waitForApartmentFadeIntimer)
						ELSE
							IF HAS_NET_TIMER_EXPIRED(waitForApartmentFadeIntimer, 50000)
								SET_CELEBRATION_STATE(eCELEBRATIONSTATE_APARTMENT_FAILSAFE)
							ENDIF
						ENDIF
					ELIF IS_SCREEN_FADED_IN()
						PRINTLN("[SAC] - screen is faded in, bgoToGameStateEnd = TRUE")
						bgoToGameStateEnd = TRUE
					ENDIF
					
				ENDIF
				
			ELSE
				PRINTLN("[SAC] - iPostMissionSceneId != SIS_POST_APARTMENT, bgoToGameStateEnd = TRUE")
				bgoToGameStateEnd = TRUE
			ENDIF
			
			IF bgoToGameStateEnd
				SET_GAME_STATE(eGAME_STATE_END)
			ENDIF
			
		BREAK
		
	ENDSWITCH
	
ENDPROC


// ************ //
// 	WIDGETS		//
// ************ //

#IF IS_DEBUG_BUILD

PROC CREATE_WIDGETS()	

	#IF IS_DEBUG_BUILD
	INT i
	TEXT_LABEL_63 tl63_temp
	#ENDIF
	
	START_WIDGET_GROUP("Stand Alone Celebrations")
	
		ADD_WIDGET_BOOL("bHostEndMissionNow", bHostEndMissionNow)
		#IF IS_DEBUG_BUILD
		twID_GameState = ADD_TEXT_WIDGET("Game State")
		#ENDIF
		ADD_WIDGET_INT_READ_ONLY("Screen Estimated Duration", sCelebrationData.iEstimatedScreenDuration)
		
		#IF IS_DEBUG_BUILD
		ADD_HEIST_WINNER_SCENE_WIDGETS()
		#ENDIF
		
		START_WIDGET_GROUP("Global Values")
			
			ADD_WIDGET_BOOL("bPassedMission", g_TransitionSessionNonResetVars.sGlobalCelebrationData.bPassedMission)
			
			#IF IS_DEBUG_BUILD
			ADD_WIDGET_BOOL("I am leader", g_TransitionSessionNonResetVars.sGlobalCelebrationData.bIAmHeistLeader)
			ADD_WIDGET_INT_READ_ONLY("Num Peds Killed", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iNumPedKills)
//			ADD_WIDGET_INT_READ_ONLY("Num Choppers Destroyed", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iChoppersDestroyed)
//			ADD_WIDGET_INT_READ_ONLY("Max Choppers Can Be Destroyed", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iMaximumChoppersDestroyed)
//			ADD_WIDGET_INT_READ_ONLY("Num Vehicles Destroyed", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iVehiclesDestoryed)
//			ADD_WIDGET_INT_READ_ONLY("Max Vehicles Can Be Destoryed", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iMaximumVehDestroyed)
//			ADD_WIDGET_INT_READ_ONLY("Ending Accuracty", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iEndingAccuracy)
//			ADD_WIDGET_INT_READ_ONLY("Num Prisoners Killed", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPrisonersKilled)
//			ADD_WIDGET_INT_READ_ONLY("VIP Damage", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iVipDamage)
//			ADD_WIDGET_INT_READ_ONLY("Lose Wanted Time", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iLoseWantedTime)
//			ADD_WIDGET_INT_READ_ONLY("Bike highest Speed", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iBikehighSpeed)
			ADD_WIDGET_INT_READ_ONLY("Max Take", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iMaximumTake)
			ADD_WIDGET_INT_READ_ONLY("My Take", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iMyTake)
//			ADD_WIDGET_INT_READ_ONLY("Maximum Choppers Destroyed", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iMaximumChoppersDestroyed)
//			ADD_WIDGET_INT_READ_ONLY("Maximum Vehicles Destroyed", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iMaximumVehDestroyed)
			ADD_WIDGET_BOOL("OK to Kill Celebration Script", g_TransitionSessionNonResetVars.sGlobalCelebrationData.bOkToKillCelebrationScript)
			twID_FailReason = ADD_TEXT_WIDGET("Fail Reason")
			twID_LiteralFailReason = ADD_TEXT_WIDGET("Literal Fail Reason")
			START_WIDGET_GROUP("Player Details")
				REPEAT 4 i
					tl63_temp = "LB Player "
					tl63_temp += i
					tl63_temp += " ID"
					ADD_WIDGET_INT_READ_ONLY(tl63_temp, iWidgetPlayerIds[i])
					tl63_temp = "LB Player "
					tl63_temp += i
					tl63_temp += " name"
					twID_lbPlayerName[i] = ADD_TEXT_WIDGET(tl63_temp)
					tl63_temp = "LB Player "
					tl63_temp += i
					tl63_temp += " rating"
					twID_lbPlayerRating[i] = ADD_TEXT_WIDGET(tl63_temp)
				ENDREPEAT	
			STOP_WIDGET_GROUP()
			#ENDIF
			
		STOP_WIDGET_GROUP()
		
	STOP_WIDGET_GROUP()
	
ENDPROC

PROC UPDATE_WIDGETS()
	#IF IS_DEBUG_BUILD
	INT i
	SET_CONTENTS_OF_TEXT_WIDGET(twID_GameState, GET_GAME_STATE_NAME(GET_GAME_STATE(0)))
	SET_CONTENTS_OF_TEXT_WIDGET(twID_FailReason, g_TransitionSessionNonResetVars.sGlobalCelebrationData.strFailReason)
	SET_CONTENTS_OF_TEXT_WIDGET(twID_LiteralFailReason, g_TransitionSessionNonResetVars.sGlobalCelebrationData.strFailReasonLiteral)
	REPEAT 3 i
		SET_CONTENTS_OF_TEXT_WIDGET(twID_lbPlayerName[i], g_TransitionSessionNonResetVars.sGlobalCelebrationData.strLeaderBoardPlayerName[i])
		SET_CONTENTS_OF_TEXT_WIDGET(twID_lbPlayerRating[i], g_TransitionSessionNonResetVars.sGlobalCelebrationData.strRatingColour[i])
	ENDREPEAT
	#ENDIF
ENDPROC

#ENDIF

// ************************ //
// 	MAIN SCRIPT PROCESSING	//
// ************************ //

SCRIPT (CELEBRATION_SCRIPT_DATA launchData)
	
	// Carry out all the initial game starting duties.
	PROCESS_PRE_GAME()	
	
	// Setup widgets. Make sure this is done after PROCESS_PRE_GAME()
	#IF IS_DEBUG_BUILD
		CREATE_WIDGETS()
	#ENDIF
	
	sHeistWinnerSceneData.playerLeader = launchData.leaderHandle
	
	// Main loop.
	WHILE TRUE

		MP_LOOP_WAIT_ZERO() 
		
		// If we have a match end event, bail.
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
//			PRINTLN("[SAC] - SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE(). NOt doing anything though as we want this script to continue during transitions.")
//			SCRIPT_CLEANUP()
		ENDIF
					
		// Deal with the debug.
		#IF IS_DEBUG_BUILD		
			UPDATE_WIDGETS()
		#ENDIF		
		
		// If we need to leave the mission, make sure we just straight to the end stage and terminate the script.
		IF SHOULD_PLAYER_LEAVE_MP_MISSION() 
			PRINTLN("[SAC] - SHOULD_PLAYER_LEAVE_MP_MISSION() = TRUE.")
			SET_GAME_STATE(eGAME_STATE_END)
		ENDIF
		
		IF GET_SHOULD_END_SCRIPT()
			PRINTLN("[SAC] - GET_SHOULD_END_SCRIPT = TRUE, going to game state end call 0.")
			SET_GAME_STATE(eGAME_STATE_END)
		ENDIF
		
		IF NOT g_sTransitionSessionData.bMaintainCelebrationsScript
			PRINTLN("[SAC] - bMaintainCelebrationsScript = FALSE, going to game state end call 7.")
			SET_GAME_STATE(eGAME_STATE_END)
		ENDIF
		
		IF HAS_TRANSITION_SESSION_JIPED_FROM_OTHER_SESSION()
		AND SHOULD_TRANSITION_SESSION_KILL_ALL_MP_SCRIPTS()
			PRINTLN("[SAC] - HAS_TRANSITION_SESSION_JIPED_FROM_OTHER_SESSION = TRUE and SHOULD_TRANSITION_SESSION_KILL_ALL_MP_SCRIPTS = TRUE, going to game state end call 8.")
			SET_GAME_STATE(eGAME_STATE_END)
		ENDIF
		
		
		// Load the heist mocap as soon as possible.
		IF NOT IS_BIT_SET(iCelebrationGeneralBitset, CELEB_GENERAL_BITSET_MOCAP_PRE_LOAD_COMPLETE)
			IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingHeistEndWinnerScene
				IF IS_BIT_SET(sHeistWinnerSceneData.iBitset, HEIST_WINNER_SCENE_INITIALISED_DATA)
					IF LOAD_WINNER_SCENE_MOCAP(sHeistWinnerSceneData)
						PRINTLN("[SAC] - LOAD_WINNER_SCENE_MOCAP = TRUE, setting CELEB_GENERAL_BITSET_MOCAP_PRE_LOAD_COMPLETE.")
						SET_BIT(iCelebrationGeneralBitset, CELEB_GENERAL_BITSET_MOCAP_PRE_LOAD_COMPLETE)
					ENDIF
				ENDIF
			ELSE
				PRINTLN("[SAC] - bDoingHeistEndWinnerScene = FALSE, setting CELEB_GENERAL_BITSET_MOCAP_PRE_LOAD_COMPLETE.")
				SET_BIT(iCelebrationGeneralBitset, CELEB_GENERAL_BITSET_MOCAP_PRE_LOAD_COMPLETE)
			ENDIF
		ENDIF
		
		// So the strip club will load it's assets if required, once the warp to location has completed.
		IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bBlockStripClubAssets
			IF IS_BIT_SET(iCelebrationGeneralBitset, CELEB_GENERAL_BITSET_COMPLETED_PLAYER_PRE_LOAD_WARP)
				g_TransitionSessionNonResetVars.sGlobalCelebrationData.bBlockStripClubAssets = FALSE
				PRINTLN("[SAC] - CELEB_GENERAL_BITSET_COMPLETED_PLAYER_PRE_LOAD_WARP bit is set, set bBlockStripClubAssets = FALSE.")
			ENDIF
		ENDIF
		
		// Cut cam to face player if required (base don global set in misison controller.
		MAINTAIN_POINT_AT_PLAYER_CAMERA_CUT()
		
		// Maintain cloining players as early as possible.
		MAINTAIN_CLONE_EVERYONE_FOR_HEIST_CELEBRATION_SCREENS(sHeistWinnerSceneData)
		
		// Warp to the end of celebration location as soon as possible.
		MAINTAIN_WARP_TO_POST_CELEB_STATS_SHARD_LOCATION()
		
		// Draw black rectangle on faded mocaps so screen behind celebration stays black. 
		MAINTAIN_DRAW_BLACK_RECTANGLE_FOR_FADED_MOCAP()
		
		// Maintain fake rectangle.
		MAINTAIN_RECTANGLE()
		
		// Get the apartment interior as early as posisble.
		MAINTAIN_GET_APARTMENT_INTERIOR_INSTANCE_INDEX_EARLY()
		
		// Freeze microphone for audio inapartment and strip club finle complete scenes. 
		MAINTAIN_FREEZE_MICROPHONE()
		
		
		// -----------------------------------
		// Process your game logic.....
		SWITCH GET_GAME_STATE(0)
						
			// Wait untill the server gives the all go before moving on.
			CASE eGAME_STATE_INI
				
				SET_GAME_STATE(eGAME_STATE_RUNNING)
				
			BREAK
			
			// Main gameplay state.
			CASE eGAME_STATE_RUNNING
				
				PROCESS_STAND_ALONE_CELEBRATION_LOGIC()
				
			BREAK
			
			//Cleans up then terminates the mission
			CASE eGAME_STATE_END
				
				SCRIPT_CLEANUP()
				
			BREAK

		ENDSWITCH
		
	ENDWHILE
ENDSCRIPT

