/* ------------------------------------------------------------------
* Name: net_mission_summary_screen.sch
* Author: James Adwick
* Date: 20/11/2012
* Purpose: End of mission screen (common) that appears at the same
* 		   time as MISSION PASSED big message.
* ------------------------------------------------------------------*/

USING "net_mission_summary_screen_private.sch"

// ***************************************************************************
//		Methods called in scripts to add stats for mission summary screen
// ***************************************************************************

/// PURPOSE: Sets the title of the mission summary screen
PROC ADD_MISSION_SUMMARY_TITLE(STRING tl15MissionTitle)

	NET_PRINT("[JA@MESSAGE] -----> Adding Title to Summary Screen <-----") NET_NL()
	NET_PRINT("[JA@MESSAGE]		tl15MissionTitle: ") NET_PRINT(tl15MissionTitle) NET_NL()

	MPGlobals.g_missionSummary.summaryScreenState = MISSION_SUMMARY_INIT

	SET_ENDSCREEN_DATASET_HEADER(MPGlobals.g_missionSummary.endScreenStats, "", tl15MissionTitle)
ENDPROC

PROC ADD_MISSION_SUMMARY_TITLE_STRING(STRING stMissionTitle)

	NET_PRINT("[JA@MESSAGE] -----> Adding Title (String) to Summary Screen <-----") NET_NL()
	NET_PRINT("[JA@MESSAGE]		stMissionTitle: ") NET_PRINT(stMissionTitle) NET_NL()

	MPGlobals.g_missionSummary.summaryScreenState = MISSION_SUMMARY_INIT

	SET_ENDSCREEN_DATASET_HEADER(MPGlobals.g_missionSummary.endScreenStats, "", stMissionTitle, TRUE)
ENDPROC

PROC ADD_MISSION_SUMMARY_TITLE_STRING_WITH_SUB_LITERAL_STRING(STRING stMissionTitle, STRING subString)

	NET_PRINT("[JA@MESSAGE] -----> Adding Title (String) to Summary Screen <-----") NET_NL()
	NET_PRINT("[JA@MESSAGE]		stMissionTitle: ") NET_PRINT(stMissionTitle) NET_NL()
	NET_PRINT("[JA@MESSAGE]		subString: ") NET_PRINT(subString) NET_NL()

	MPGlobals.g_missionSummary.summaryScreenState = MISSION_SUMMARY_INIT
	
	SET_ENDSCREEN_DATASET_HEADER(MPGlobals.g_missionSummary.endScreenStats, "", stMissionTitle, TRUE)
	SET_ENDSCREEN_DATASET_HEADER_SINGLE_SUB_LITERAL_STRING(MPGlobals.g_missionSummary.endScreenStats, subString)
ENDPROC

PROC ADD_MISSION_SUMMARY_TITLE_STRING_WITH_SUB_STRING(STRING stMissionTitle, STRING subString)

	NET_PRINT("[JA@MESSAGE] -----> Adding Title (String) to Summary Screen <-----") NET_NL()
	NET_PRINT("[JA@MESSAGE]		stMissionTitle: ") NET_PRINT(stMissionTitle) NET_NL()
	NET_PRINT("[JA@MESSAGE]		subString: ") NET_PRINT(subString) NET_NL()

	MPGlobals.g_missionSummary.summaryScreenState = MISSION_SUMMARY_INIT
	
	SET_ENDSCREEN_DATASET_HEADER(MPGlobals.g_missionSummary.endScreenStats, "", stMissionTitle, TRUE)
	SET_ENDSCREEN_DATASET_HEADER_SINGLE_SUB_STRING(MPGlobals.g_missionSummary.endScreenStats, subString)
ENDPROC

PROC ADD_MISSION_SUMMARY_TITLE_WITH_SUB_LITERAL_STRING(STRING stMissionTitle, STRING subString)

	NET_PRINT("[JA@MESSAGE] -----> Adding Title to Summary Screen <-----") NET_NL()
	NET_PRINT("[JA@MESSAGE]		stMissionTitle: ") NET_PRINT(stMissionTitle) NET_NL()
	NET_PRINT("[JA@MESSAGE]		subString: ") NET_PRINT(subString) NET_NL()

	MPGlobals.g_missionSummary.summaryScreenState = MISSION_SUMMARY_INIT
	
	SET_ENDSCREEN_DATASET_HEADER(MPGlobals.g_missionSummary.endScreenStats, "", stMissionTitle)
	SET_ENDSCREEN_DATASET_HEADER_SINGLE_SUB_LITERAL_STRING(MPGlobals.g_missionSummary.endScreenStats, subString)
ENDPROC

CONST_INT THUMB_TIMEOUT_SECONDS						15

PROC ADD_MISSION_THUMB_VOTES(END_SCREEN_MP_GAME_TYPE esmgtType)
	PRINTLN("[JA@MESSAGE] ADD_MISSION_THUMB_VOTES")
	g_b_OnLeaderboard = FALSE
	MPGlobals.g_missionSummary.endScreenStats.bShowSkipperPrompt = TRUE
	MPGlobals.g_missionSummary.endScreenStats.button = NULL
	ENDSCREEN_CONFIGURE_VOTE_PANEL(MPGlobals.g_missionSummary.endScreenStats, THUMB_TIMEOUT_SECONDS, esmgtType)
	MPGlobals.g_missionSummary.summaryScreenState = MISSION_SUMMARY_INIT
ENDPROC

/// PURPOSE: Add a stat with an int as the value
PROC ADD_MISSION_SUMMARY_STAT_WITH_INT(STRING tl15StatTitle, INT iStatValue)

	NET_PRINT("[JA@MESSAGE] -----> ADD_MISSION_SUMMARY_STAT_WITH_INT <-----") NET_NL()
	NET_PRINT("[JA@MESSAGE]		tl15StatTitle: ") NET_PRINT(tl15StatTitle) NET_NL()
	NET_PRINT("[JA@MESSAGE]		iStatValue: ") NET_PRINT_INT(iStatValue) NET_NL()

	ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(MPGlobals.g_missionSummary.endScreenStats, ESEF_RAW_INTEGER, tl15StatTitle, "", iStatValue, 0, ESCM_NO_MARK)
	MPGlobals.g_missionSummary.summaryScreenState = MISSION_SUMMARY_INIT
ENDPROC

/// PURPOSE: Add a stat with the player name as label and int as the value
PROC ADD_MISSION_SUMMARY_STAT_PLAYER_WITH_INT(PLAYER_INDEX playerID, INT iStatValue)
	
	NET_PRINT("[JA@MESSAGE] -----> ADD_MISSION_SUMMARY_STAT_PLAYER_WITH_INT <-----") NET_NL()
	NET_PRINT("[JA@MESSAGE]		playerID: ") NET_PRINT(GET_PLAYER_NAME(playerID)) NET_NL()
	NET_PRINT("[JA@MESSAGE]		iStatValue: ") NET_PRINT_INT(iStatValue) NET_NL()
	
	ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(MPGlobals.g_missionSummary.endScreenStats, ESEF_RAW_USERNAME, GET_PLAYER_NAME(playerID), "", iStatValue, 0, ESCM_NO_MARK, TRUE)
	MPGlobals.g_missionSummary.summaryScreenState = MISSION_SUMMARY_INIT
	
ENDPROC

/// PURPOSE: Add a stat with the player name as label and int as the value
PROC ADD_MISSION_SUMMARY_STAT_PLAYER_WITH_CASH(PLAYER_INDEX playerID, INT iStatValue)
	
	NET_PRINT("[JA@MESSAGE] -----> ADD_MISSION_SUMMARY_STAT_PLAYER_WITH_INT <-----") NET_NL()
	NET_PRINT("[JA@MESSAGE]		playerID: ") NET_PRINT(GET_PLAYER_NAME(playerID)) NET_NL()
	NET_PRINT("[JA@MESSAGE]		iStatValue: ") NET_PRINT_INT(iStatValue) NET_NL()
	
	ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(MPGlobals.g_missionSummary.endScreenStats, ESEF_DOLLAR_VALUE, GET_PLAYER_NAME(playerID), "", iStatValue, 0, ESCM_NO_MARK, TRUE)
	MPGlobals.g_missionSummary.summaryScreenState = MISSION_SUMMARY_INIT
	
ENDPROC

/// PURPOSE: Add a stat with the player name as label and int as the value
PROC ADD_MISSION_SUMMARY_STAT_TWO_SUBSTRINGS_WITH_INTS(STRING tlLeftTitle, INT iLeftValue, STRING tlRightTitle, INT iRightValue)
	
	NET_PRINT("[JA@MESSAGE] -----> ADD_MISSION_SUMMARY_STAT_TWO_SUBSTRINGS_WITH_INTS <-----") NET_NL()
	NET_PRINT("[JA@MESSAGE]		tlLeftTitle: ") NET_PRINT(tlLeftTitle) NET_NL()
	NET_PRINT("[JA@MESSAGE]		iLeftValue: ") NET_PRINT_INT(iLeftValue) NET_NL()
	NET_PRINT("[JA@MESSAGE]		tlRightTitle: ") NET_PRINT(tlRightTitle) NET_NL()
	NET_PRINT("[JA@MESSAGE]		iRightValue: ") NET_PRINT_INT(iRightValue) NET_NL()
	
	ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(MPGlobals.g_missionSummary.endScreenStats, ESEF_LEFT_AND_RIGHT_SUBSTRINGS, tlLeftTitle, tlRightTitle, iLeftValue, iRightValue, ESCM_NO_MARK, FALSE)
	MPGlobals.g_missionSummary.summaryScreenState = MISSION_SUMMARY_INIT
ENDPROC

/// PURPOSE: Add two substrings
PROC ADD_MISSION_SUMMARY_STAT_TWO_STRINGS(STRING tlLeftTitle, STRING tlRightTitle)
	
	NET_PRINT("[JA@MESSAGE] -----> ADD_MISSION_SUMMARY_STAT_TWO_STRINGS <-----") NET_NL()
	NET_PRINT("[JA@MESSAGE]		tlLeftTitle: ") NET_PRINT(tlLeftTitle) NET_NL()
	NET_PRINT("[JA@MESSAGE]		tlRightTitle: ") NET_PRINT(tlRightTitle) NET_NL()
	
	ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(MPGlobals.g_missionSummary.endScreenStats, ESEF_TWO_STRINGS, tlLeftTitle, tlRightTitle,0,0, ESCM_NO_MARK, FALSE)
	MPGlobals.g_missionSummary.summaryScreenState = MISSION_SUMMARY_INIT
ENDPROC

/// PURPOSE: Add string with tick box
PROC ADD_MISSION_SUMMARY_STAT_STRING_WITH_TICK_BOX(STRING tlMainString, END_SCREEN_CHECK_MARK_STATUS markStatus)
	
	NET_PRINT("[JA@MESSAGE] -----> ADD_MISSION_SUMMARY_STAT_STRING_WITH_TICK_BOX <-----") NET_NL()
	NET_PRINT("[JA@MESSAGE]		tlMainString: ") NET_PRINT(tlMainString) NET_NL()
	PRINTLN("[JA@MESSAGE]		markStatus: ",markStatus)
	
	ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(MPGlobals.g_missionSummary.endScreenStats, ESEF_NAME_ONLY, tlMainString, "",0,0, markStatus, FALSE)
	MPGlobals.g_missionSummary.summaryScreenState = MISSION_SUMMARY_INIT
ENDPROC

/// PURPOSE: Add string with tick box
PROC ADD_MISSION_SUMMARY_STAT_STRING_AND_CASH_WITH_TICK_BOX(STRING tlMainString, INT iValue1, END_SCREEN_CHECK_MARK_STATUS markStatus)
	
	NET_PRINT("[JA@MESSAGE] -----> ADD_MISSION_SUMMARY_STAT_STRING_WITH_TICK_BOX <-----") NET_NL()
	NET_PRINT("[JA@MESSAGE]		tlMainString: ") NET_PRINT(tlMainString) NET_NL()
	NET_PRINT("[JA@MESSAGE]		iValue1: ") NET_PRINT_INT(iValue1) NET_NL()
	PRINTLN("[JA@MESSAGE]		markStatus: ",markStatus)
	
	ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(MPGlobals.g_missionSummary.endScreenStats, ESEF_DOLLAR_VALUE, tlMainString, "",iValue1,0, markStatus, FALSE)
	MPGlobals.g_missionSummary.summaryScreenState = MISSION_SUMMARY_INIT
ENDPROC

/// PURPOSE: Add a stat with the player name as label and int as the value
PROC ADD_MISSION_SUMMARY_STAT_STRING_WITH_TWO_INTS(STRING tlMainString, INT iValue1, INT iValue2)
	
	NET_PRINT("[JA@MESSAGE] -----> ADD_MISSION_SUMMARY_STAT_STRING_WITH_TWO_INTS <-----") NET_NL()
	NET_PRINT("[JA@MESSAGE]		tlMainString: ") NET_PRINT(tlMainString) NET_NL()
	NET_PRINT("[JA@MESSAGE]		iValue1: ") NET_PRINT_INT(iValue1) NET_NL()
	NET_PRINT("[JA@MESSAGE]		iValue2: ") NET_PRINT_INT(iValue2) NET_NL()
	
	ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(MPGlobals.g_missionSummary.endScreenStats, ESEF_DOUBLE_NUMBER_SUB_LEFT, tlMainString, "", iValue1, iValue2, ESCM_NO_MARK, FALSE)
	MPGlobals.g_missionSummary.summaryScreenState = MISSION_SUMMARY_INIT
ENDPROC

/// PURPOSE: Add a stat with the player name as label and int as the value
PROC ADD_MISSION_SUMMARY_STAT_STRING_AND_CASH(STRING tlMainString, INT iValue1)
	
	NET_PRINT("[JA@MESSAGE] -----> ADD_MISSION_SUMMARY_STAT_STRING_AND_CASH <-----") NET_NL()
	NET_PRINT("[JA@MESSAGE]		tlMainString: ") NET_PRINT(tlMainString) NET_NL()
	NET_PRINT("[JA@MESSAGE]		iValue1: ") NET_PRINT_INT(iValue1) NET_NL()
	
	ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(MPGlobals.g_missionSummary.endScreenStats, ESEF_DOLLAR_VALUE, tlMainString, "", iValue1, 0, ESCM_NO_MARK, FALSE)
	MPGlobals.g_missionSummary.summaryScreenState = MISSION_SUMMARY_INIT
ENDPROC

/// PURPOSE: Add a stat with the player name as label and int as the value
PROC ADD_MISSION_SUMMARY_STAT_LITERAL_STRING_AND_CASH(STRING tlMainString, INT iValue1)
	
	NET_PRINT("[JA@MESSAGE] -----> ADD_MISSION_SUMMARY_STAT_LITERALSTRING_AND_CASH <-----") NET_NL()
	NET_PRINT("[JA@MESSAGE]		tlMainString: ") NET_PRINT(tlMainString) NET_NL()
	NET_PRINT("[JA@MESSAGE]		iValue1: ") NET_PRINT_INT(iValue1) NET_NL()
	
	ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(MPGlobals.g_missionSummary.endScreenStats, ESEF_DOLLAR_VALUE, tlMainString, "", iValue1, 0, ESCM_NO_MARK,TRUE)
	MPGlobals.g_missionSummary.summaryScreenState = MISSION_SUMMARY_INIT
ENDPROC

/// PURPOSE: Add a stat with the player name as label and int as the value
PROC ADD_MISSION_SUMMARY_STAT_STRING_AND_FRACTION(STRING tlMainString, INT iValue1, INT iValue2)
	
	NET_PRINT("[JA@MESSAGE] -----> ADD_MISSION_SUMMARY_STAT_STRING_AND_FRACTION <-----") NET_NL()
	NET_PRINT("[JA@MESSAGE]		tlMainString: ") NET_PRINT(tlMainString) NET_NL()
	NET_PRINT("[JA@MESSAGE]		iValue1: ") NET_PRINT_INT(iValue1) NET_NL()
	NET_PRINT("[JA@MESSAGE]		iValue2: ") NET_PRINT_INT(iValue2) NET_NL()
	
	ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(MPGlobals.g_missionSummary.endScreenStats, ESEF_FRACTION, tlMainString, "", iValue1, iValue2, ESCM_NO_MARK, FALSE)
	MPGlobals.g_missionSummary.summaryScreenState = MISSION_SUMMARY_INIT
ENDPROC

/// PURPOSE: Add a stat with the player name as label and int as the value
PROC ADD_MISSION_SUMMARY_STAT_STRING_AND_PERCENTAGE(STRING tlMainString, INT iValue1)
	
	NET_PRINT("[JA@MESSAGE] -----> ADD_MISSION_SUMMARY_STAT_STRING_AND_PERCENTAGE <-----") NET_NL()
	NET_PRINT("[JA@MESSAGE]		tlMainString: ") NET_PRINT(tlMainString) NET_NL()
	NET_PRINT("[JA@MESSAGE]		iValue1: ") NET_PRINT_INT(iValue1) NET_NL()
	
	ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(MPGlobals.g_missionSummary.endScreenStats, ESEF_RAW_PERCENT, tlMainString, "", iValue1, 0, ESCM_NO_MARK, FALSE)
	MPGlobals.g_missionSummary.summaryScreenState = MISSION_SUMMARY_INIT
ENDPROC


/// PURPOSE: Add a stat with the player name as label and int as the value
PROC ADD_MISSION_SUMMARY_END_COMPLETION_XP(STRING tlMainString, INT iXPValue)
	
	NET_PRINT("[JA@MESSAGE] -----> ADD_MISSION_SUMMARY_END_COMPLETION_XP <-----") NET_NL()
	NET_PRINT("[JA@MESSAGE]		tlMainString: ") NET_PRINT(tlMainString) NET_NL()
	NET_PRINT("[JA@MESSAGE]		iXPValue: ") NET_PRINT_INT(iXPValue) NET_NL()
	
	SET_ENDSCREEN_COMPLETION_LINE_STATE(MPGlobals.g_missionSummary.endScreenStats, TRUE, tlMainString, iXPValue, 0, ESC_XP_COMPLETION)
	MPGlobals.g_missionSummary.summaryScreenState = MISSION_SUMMARY_INIT
ENDPROC

/// PURPOSE: Add a stat with the player name as label and int as the value
PROC ADD_MISSION_SUMMARY_END_COMPLETION_CASH(STRING tlMainString, INT iCashValue)
	
	NET_PRINT("[JA@MESSAGE] -----> ADD_MISSION_SUMMARY_END_COMPLETION_CASH <-----") NET_NL()
	NET_PRINT("[JA@MESSAGE]		tlMainString: ") NET_PRINT(tlMainString) NET_NL()
	NET_PRINT("[JA@MESSAGE]		iCashValue: ") NET_PRINT_INT(iCashValue) NET_NL()
	
	SET_ENDSCREEN_COMPLETION_LINE_STATE(MPGlobals.g_missionSummary.endScreenStats, TRUE, tlMainString, iCashValue, 0, ESC_CASH_COMPLETION)
	MPGlobals.g_missionSummary.summaryScreenState = MISSION_SUMMARY_INIT
ENDPROC

/// PURPOSE: Add a stat with the player name as label and int as the value
PROC ADD_MISSION_SUMMARY_END_COMPLETION_CASH_AND_MEDAL(STRING tlMainString, INT iCashValue, END_SCREEN_MEDAL_STATUS eMedal = ESMS_NO_MEDAL)
	
	NET_PRINT("[JA@MESSAGE] -----> ADD_MISSION_SUMMARY_END_COMPLETION_CASH <-----") NET_NL()
	NET_PRINT("[JA@MESSAGE]		tlMainString: ") NET_PRINT(tlMainString) NET_NL()
	NET_PRINT("[JA@MESSAGE]		iCashValue: ") NET_PRINT_INT(iCashValue) NET_NL()
	
	SET_ENDSCREEN_COMPLETION_LINE_STATE(MPGlobals.g_missionSummary.endScreenStats, TRUE, tlMainString, iCashValue, 0, ESC_CASH_COMPLETION,eMedal)
	MPGlobals.g_missionSummary.summaryScreenState = MISSION_SUMMARY_INIT
ENDPROC

/// PURPOSE: Add a stat with the player name as label and int as the value
PROC ADD_MISSION_SUMMARY_END_COMPLETION_LITERAL_STRING_AND_CASH(STRING tlMainString,STRING tlSubString, INT iCashValue)
	
	NET_PRINT("[JA@MESSAGE] -----> DD_MISSION_SUMMARY_END_COMPLETION_LITERAL_STRING_AND_CASH <-----") NET_NL()
	NET_PRINT("[JA@MESSAGE]		tlMainString: ") NET_PRINT(tlMainString) NET_NL()
	PRINTLN("[JA@MESSAGE]		tlSubString: ",tlSubString)
	NET_PRINT("[JA@MESSAGE]		iCashValue: ") NET_PRINT_INT(iCashValue) NET_NL()
	
	SET_ENDSCREEN_COMPLETION_LINE_STATE_WITH_LITERAL_STRING(MPGlobals.g_missionSummary.endScreenStats,TRUE,tlMainString,tlSubString, iCashValue, 0, ESC_CASH_COMPLETION)
	MPGlobals.g_missionSummary.summaryScreenState = MISSION_SUMMARY_INIT
ENDPROC

/// PURPOSE: Add a stat with the player name as label and int as the value
PROC ADD_MISSION_SUMMARY_END_COMPLETION_TIME(STRING tlMainString, INT iTimeMS)
	
	NET_PRINT("[JA@MESSAGE] -----> ADD_MISSION_SUMMARY_END_COMPLETION_TIME <-----") NET_NL()
	NET_PRINT("[JA@MESSAGE]		tlMainString: ") NET_PRINT(tlMainString) NET_NL()
	NET_PRINT("[JA@MESSAGE]		iTimeMS: ") NET_PRINT_INT(iTimeMS) NET_NL()
	
	ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(MPGlobals.g_missionSummary.endScreenStats, ESEF_TIME_H_M_S, tlMainString, "", iTimeMS, 0, ESCM_NO_MARK, FALSE)
	MPGlobals.g_missionSummary.summaryScreenState = MISSION_SUMMARY_INIT
ENDPROC


/// PURPOSE: Add a stat with the player name as label and int as the value
PROC ADD_MISSION_SUMMARY_STAT_STRING_AND_CASH_OF_TYPE(STRING tlMainString, INT iValue1, END_SCREEN_ELEMENT_FORMATTING endScreenFormat)
	
	NET_PRINT("[JA@MESSAGE] -----> ADD_MISSION_SUMMARY_STAT_STRING_AND_CASH_OF_TYPE <-----") NET_NL()
	NET_PRINT("[JA@MESSAGE]		tlMainString: ") NET_PRINT(tlMainString) NET_NL()
	NET_PRINT("[JA@MESSAGE]		iValue1: ") NET_PRINT_INT(iValue1) NET_NL()
	
	ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(MPGlobals.g_missionSummary.endScreenStats, endScreenFormat, tlMainString, "", iValue1, 0, ESCM_NO_MARK, FALSE)
	MPGlobals.g_missionSummary.summaryScreenState = MISSION_SUMMARY_INIT
ENDPROC

/// PURPOSE: Add a stat with the player name as label and int as the value 
PROC ADD_MISSION_SUMMARY_END_COMPLETION_FRACTION(STRING tlMainString, INT left,INT right) 
        
    NET_PRINT("[JA@MESSAGE] -----> ADD_MISSION_SUMMARY_END_COMPLETION_FRACTION <-----") NET_NL() 
    NET_PRINT("[JA@MESSAGE] tlMainString: ") NET_PRINT(tlMainString) NET_NL() 
    NET_PRINT("[JA@MESSAGE] left: ") NET_PRINT_INT(left) NET_NL() 
    NET_PRINT("[JA@MESSAGE] right: ") NET_PRINT_INT(right) NET_NL() 

    SET_ENDSCREEN_COMPLETION_LINE_STATE(MPGlobals.g_missionSummary.endScreenStats, TRUE, tlMainString, left, right, ESC_FRACTION_COMPLETION) 

    MPGlobals.g_missionSummary.summaryScreenState = MISSION_SUMMARY_INIT 
ENDPROC

/// PURPOSE: Add a stat with the player name as label and int as the value
PROC ADD_MISSION_SUMMARY_STAT_PLAYER_NAME_WITH_INT(PLAYER_INDEX playerID, INT iStatValue)
	
	NET_PRINT("[JA@MESSAGE] -----> ADD_MISSION_SUMMARY_STAT_PLAYER_WITH_INT <-----") NET_NL()
	NET_PRINT("[JA@MESSAGE]		playerID: ") NET_PRINT(GET_PLAYER_NAME(playerID)) NET_NL()
	NET_PRINT("[JA@MESSAGE]		iStatValue: ") NET_PRINT_INT(iStatValue) NET_NL()
	
	ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(MPGlobals.g_missionSummary.endScreenStats, ESEF_RAW_INTEGER, GET_PLAYER_NAME(playerID), "", iStatValue, 0, ESCM_NO_MARK, TRUE)
	MPGlobals.g_missionSummary.summaryScreenState = MISSION_SUMMARY_INIT
	
ENDPROC

/// PURPOSE: Add final line with only string, no symbols or medals
PROC ADD_MISSION_SUMMARY_END_COMPLETION_STRING(STRING tlMainString)
    NET_PRINT("[JA@MESSAGE] -----> ADD_MISSION_SUMMARY_END_COMPLETION_STRING <-----") NET_NL()
    NET_PRINT("[JA@MESSAGE] tlMainString: ") NET_PRINT(tlMainString) NET_NL()
	
    SET_ENDSCREEN_COMPLETION_LINE_STATE(MPGlobals.g_missionSummary.endScreenStats, TRUE, tlMainString, -1, -1, ESC_STRING_COMPLETION)
	
    MPGlobals.g_missionSummary.summaryScreenState = MISSION_SUMMARY_INIT
ENDPROC



/// PURPOSE: Add final line with nothing
PROC ADD_MISSION_SUMMARY_END_MULTI_COMPLETION(INT iMidIndex)
    NET_PRINT("[JA@MESSAGE] -----> ADD_MISSION_SUMMARY_END_COMPLETION_EMPTY <-----") NET_NL()
	
    SET_ENDSCREEN_COMPLETION_LINE_STATE(MPGlobals.g_missionSummary.endScreenStats, TRUE, "", -1, -1, ESC_MULTI_COMPLETION, DEFAULT, iMidIndex)
	
    MPGlobals.g_missionSummary.summaryScreenState = MISSION_SUMMARY_INIT
ENDPROC

// ------------------------------------------------------------------------------------
// !!!! Add any further stat types to MP_globals_common.sch and add command above !!!!
// ------------------------------------------------------------------------------------


/// PURPOSE: Returns if a summary screen has been initialised with data
FUNC BOOL IS_MISSION_SUMMARY_SCREEN_AVAILABLE()
	RETURN MPGlobals.g_missionSummary.summaryScreenState != MISSION_SUMMARY_NULL
ENDFUNC

/// PURPOSE: Returns TRUE if summary screen is currently on screen   
FUNC BOOL IS_MISSION_SUMMARY_SCREEN_DISPLAYED()
	RETURN MPGlobals.g_missionSummary.bSummaryScreenDisplayed
ENDFUNC





