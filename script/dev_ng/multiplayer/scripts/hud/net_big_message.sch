// Name: net_big_message.sch
// Author: James Adwick
// Date: 28/08/2012
// Purpose: Header file to trigger big messages in all modes

// NOTE: 525553: Provides us with RESET_MOVIE() which allows us to clean everything up while movie is 
// still loaded so we can display another big message immediately.


USING "net_big_message_private.sch"
USING "commands_network.sch"

// ----------------------- PUBLIC FUNCTION -------------------------

/// PURPOSE: Allow script to request a basic big message to display across the screen
/// PARAMS:
///    messageType - Type of big message. See BIG_MESSAGE_TYPE enum
///    tlMsgTitle - Main text within the big message
///    tlStrapline - Subtext displayed beneath above text
///    hcOverride - Colour of big message. This is the colour the background appears as it animates
///    iOverrideTime - Pass in a time if you want to override the display time of the message.
///    hcStraplineVar - Pass to colour the next ~v~ before the tlStrapline
FUNC BOOL SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_TYPE messageType, STRING tlMsgTitle = NULL, STRING tlStrapline = NULL, HUD_COLOURS hcOverride = HUD_COLOUR_WHITE, INT iOverrideTime = -1, HUD_COLOURS hcBGOverride = HUD_COLOUR_BLACK, HUD_COLOURS hcStraplineVar = HUD_COLOUR_WHITE, BIG_MESSAGE_BIT_SET eOptionalBitSet = BIG_MESSAGE_BIT_INVALID)

	#IF IS_DEBUG_BUILD
		NET_PRINT("[JA@MESSAGE] SETUP_NEW_BIG_MESSAGE - initialising a new big message of type: ") NET_PRINT(GET_BIG_MESSAGE_DEBUG_TEXT(messageType)) NET_NL()
	#ENDIF

	BIG_MESSAGE newBigMessage
	INIT_BIG_MESSAGE(messageType, newBigMessage, -1, tlStrapline, tlMsgTitle)
	newBigMessage.bigMessageColour = hcOverride
	newBigMessage.iOverrideTime = iOverrideTime
	newBigMessage.bigBGColour = hcBGOverride
	newBigMessage.hcStraplineVar = hcStraplineVar
	
	IF eOptionalBitSet != BIG_MESSAGE_BIT_INVALID
		SET_BITMASK_AS_ENUM(newBigMessage.iBigMessageBitSet, eOptionalBitSet)
	ENDIF
	
	RETURN SAVE_BIG_MESSAGE(newBigMessage)
ENDFUNC

/// PURPOSE: Called by any script to force big message with a player name in the strapline (delivered, busted). See SETUP_NEW_BIG_MESSAGE() for param list
FUNC BOOL SETUP_NEW_BIG_MESSAGE_WITH_PLAYER(BIG_MESSAGE_TYPE messageType, PLAYER_INDEX playerID, INT iNumber = -1, STRING tlStrapline = NULL, STRING tlMsgTitle = NULL, HUD_COLOURS hcOverride = HUD_COLOUR_WHITE, INT iOverrideTime = -1, HUD_COLOURS hcBGOverride = HUD_COLOUR_BLACK, BIG_MESSAGE_BIT_SET eOptionalBitSet = BIG_MESSAGE_BIT_INVALID)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("[JA@MESSAGE] SETUP_NEW_BIG_MESSAGE_WITH_PLAYER - initialising a new big message of type: ") NET_PRINT(GET_BIG_MESSAGE_DEBUG_TEXT(messageType)) NET_NL()
	#ENDIF
	
	BIG_MESSAGE newBigMessage
	INIT_BIG_MESSAGE(messageType, newBigMessage, iNumber, tlStrapline, tlMsgTitle)
	newBigMessage.playerID = playerID
	newBigMessage.bigMessageColour = hcOverride
	newBigMessage.iOverrideTime = iOverrideTime
	newBigMessage.bigBGColour = hcBGOverride
	
	IF eOptionalBitSet != BIG_MESSAGE_BIT_INVALID
		SET_BITMASK_AS_ENUM(newBigMessage.iBigMessageBitSet, eOptionalBitSet)
	ENDIF
	
	RETURN SAVE_BIG_MESSAGE(newBigMessage)
ENDFUNC

/// PURPOSE: Called by any script to force big message with a player name in the strapline (delivered, busted). See SETUP_NEW_BIG_MESSAGE() for param list
FUNC BOOL SETUP_NEW_BIG_MESSAGE_WITH_PLAYER_AND_2_INTS(BIG_MESSAGE_TYPE messageType, PLAYER_INDEX playerID, INT iNumber1 = -1,  INT iNumber2 = -1, STRING tlStrapline = NULL, STRING tlMsgTitle = NULL, HUD_COLOURS hcOverride = HUD_COLOUR_WHITE, INT iOverrideTime = -1, HUD_COLOURS hcBGOverride = HUD_COLOUR_BLACK, BIG_MESSAGE_BIT_SET eOptionalBitSet = BIG_MESSAGE_BIT_INVALID)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("[JA@MESSAGE] SETUP_NEW_BIG_MESSAGE_WITH_PLAYER - initialising a new big message of type: ") NET_PRINT(GET_BIG_MESSAGE_DEBUG_TEXT(messageType)) NET_NL()
	#ENDIF
	
	BIG_MESSAGE newBigMessage
	INIT_BIG_MESSAGE(messageType, newBigMessage, iNumber1, tlStrapline, tlMsgTitle)
	newBigMessage.playerID = playerID
	newBigMessage.iNumber2 = iNumber2
	newBigMessage.bigMessageColour = hcOverride
	newBigMessage.iOverrideTime = iOverrideTime
	newBigMessage.bigBGColour = hcBGOverride
	
	IF eOptionalBitSet != BIG_MESSAGE_BIT_INVALID
		SET_BITMASK_AS_ENUM(newBigMessage.iBigMessageBitSet, eOptionalBitSet)
	ENDIF
	
	RETURN SAVE_BIG_MESSAGE(newBigMessage)
ENDFUNC

/// PURPOSE: Called by any script to force big message with two player names in the strapline (delivered, busted). See SETUP_NEW_BIG_MESSAGE() for param list
FUNC BOOL SETUP_NEW_BIG_MESSAGE_WITH_2_PLAYERS(BIG_MESSAGE_TYPE messageType, PLAYER_INDEX playerID, PLAYER_INDEX playerID2, INT iNumber = -1, STRING tlStrapline = NULL, STRING tlMsgTitle = NULL, HUD_COLOURS hcOverride = HUD_COLOUR_WHITE, INT iOverrideTime = -1, 
												HUD_COLOURS hcBGOverride = HUD_COLOUR_BLACK, STRING strLiteral1 = NULL, STRING strLiteral2 = NULL, STRING tlStraplineSubString = NULL)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("[JA@MESSAGE] SETUP_NEW_BIG_MESSAGE_WITH_2_PLAYERS - initialising a new big message of type: ") NET_PRINT(GET_BIG_MESSAGE_DEBUG_TEXT(messageType)) NET_NL()
	#ENDIF
	
	BIG_MESSAGE newBigMessage
	INIT_BIG_MESSAGE(messageType, newBigMessage, iNumber, tlStrapline, tlMsgTitle)
	newBigMessage.playerID = playerID
	newBigMessage.playerID2 = playerID2
	newBigMessage.bigMessageColour = hcOverride
	newBigMessage.iOverrideTime = iOverrideTime
	newBigMessage.bigBGColour = hcBGOverride
	newBigMessage.tlLiteralText = strLiteral1
	newBigMessage.tlLiteralText2 = strLiteral2
	newBigMessage.tlStrapLineSubString = tlStraplineSubString
	
	RETURN SAVE_BIG_MESSAGE(newBigMessage)
ENDFUNC

/// PURPOSE: Called by any script to force big message with two player names in the strapline (delivered, busted). See SETUP_NEW_BIG_MESSAGE() for param list
FUNC BOOL SETUP_NEW_BIG_MESSAGE_WITH_3_PLAYERS(BIG_MESSAGE_TYPE messageType, PLAYER_INDEX playerID, PLAYER_INDEX playerID2, PLAYER_INDEX playerID3,
			INT iNumber = -1, STRING tlStrapline = NULL, STRING tlMsgTitle = NULL, HUD_COLOURS hcOverride = HUD_COLOUR_WHITE, INT iOverrideTime = -1, HUD_COLOURS hcBGOverride = HUD_COLOUR_BLACK)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("[JA@MESSAGE] SETUP_NEW_BIG_MESSAGE_WITH_3_PLAYERS - initialising a new big message of type: ") NET_PRINT(GET_BIG_MESSAGE_DEBUG_TEXT(messageType)) NET_NL()
		NET_PRINT("[JA@MESSAGE] SETUP_NEW_BIG_MESSAGE_WITH_3_PLAYERS - playerID: ") NET_PRINT(GET_PLAYER_NAME(playerID)) NET_NL()
		NET_PRINT("[JA@MESSAGE] SETUP_NEW_BIG_MESSAGE_WITH_3_PLAYERS - playerID2: ") NET_PRINT(GET_PLAYER_NAME(playerID2)) NET_NL()
		NET_PRINT("[JA@MESSAGE] SETUP_NEW_BIG_MESSAGE_WITH_3_PLAYERS - playerID3: ") NET_PRINT(GET_PLAYER_NAME(playerID3)) NET_NL()
	#ENDIF
	
	BIG_MESSAGE newBigMessage
	INIT_BIG_MESSAGE(messageType, newBigMessage, iNumber, tlStrapline, tlMsgTitle)
	newBigMessage.playerID = playerID
	newBigMessage.playerID2 = playerID2
	newBigMessage.playerID3 = playerID3
	newBigMessage.bigMessageColour = hcOverride
	newBigMessage.iOverrideTime = iOverrideTime
	newBigMessage.bigBGColour = hcBGOverride
	
	RETURN SAVE_BIG_MESSAGE(newBigMessage)
ENDFUNC

/// PURPOSE: Called by any script to force big message with two player names in the strapline (delivered, busted). See SETUP_NEW_BIG_MESSAGE() for param list
FUNC BOOL SETUP_NEW_BIG_MESSAGE_WITH_4_PLAYERS(BIG_MESSAGE_TYPE messageType, PLAYER_INDEX playerID, PLAYER_INDEX playerID2, PLAYER_INDEX playerID3, PLAYER_INDEX playerID4,
			INT iNumber = -1, STRING tlStrapline = NULL, STRING tlMsgTitle = NULL, HUD_COLOURS hcOverride = HUD_COLOUR_WHITE, INT iOverrideTime = -1, HUD_COLOURS hcBGOverride = HUD_COLOUR_BLACK)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("[JA@MESSAGE] SETUP_NEW_BIG_MESSAGE_WITH_4_PLAYERS - initialising a new big message of type: ") NET_PRINT(GET_BIG_MESSAGE_DEBUG_TEXT(messageType)) NET_NL()
		NET_PRINT("[JA@MESSAGE] SETUP_NEW_BIG_MESSAGE_WITH_4_PLAYERS - playerID: ") NET_PRINT(GET_PLAYER_NAME(playerID)) NET_NL()
		NET_PRINT("[JA@MESSAGE] SETUP_NEW_BIG_MESSAGE_WITH_4_PLAYERS - playerID2: ") NET_PRINT(GET_PLAYER_NAME(playerID2)) NET_NL()
		NET_PRINT("[JA@MESSAGE] SETUP_NEW_BIG_MESSAGE_WITH_4_PLAYERS - playerID3: ") NET_PRINT(GET_PLAYER_NAME(playerID3)) NET_NL()
		NET_PRINT("[JA@MESSAGE] SETUP_NEW_BIG_MESSAGE_WITH_4_PLAYERS - playerID4: ") NET_PRINT(GET_PLAYER_NAME(playerID4)) NET_NL()
	#ENDIF
	
	BIG_MESSAGE newBigMessage
	INIT_BIG_MESSAGE(messageType, newBigMessage, iNumber, tlStrapline, tlMsgTitle)
	newBigMessage.playerID = playerID
	newBigMessage.playerID2 = playerID2
	newBigMessage.playerID3 = playerID3
	newBigMessage.playerID4 = playerID4
	newBigMessage.bigMessageColour = hcOverride
	newBigMessage.iOverrideTime = iOverrideTime
	newBigMessage.bigBGColour = hcBGOverride
	
	RETURN SAVE_BIG_MESSAGE(newBigMessage)
ENDFUNC
	
/// PURPOSE: Called by any script to force big message with two player names in the strapline (delivered, busted). See SETUP_NEW_BIG_MESSAGE() for param list
FUNC BOOL SETUP_NEW_BIG_MESSAGE_WITH_4_PLAYERS_AND_4_COLOURS(BIG_MESSAGE_TYPE messageType, PLAYER_INDEX playerID, PLAYER_INDEX playerID2, PLAYER_INDEX playerID3, PLAYER_INDEX playerID4,
			INT iNumber = -1, STRING tlStrapline = NULL, STRING tlMsgTitle = NULL, HUD_COLOURS hcOverride = HUD_COLOUR_WHITE, INT iOverrideTime = -1, HUD_COLOURS hcBGOverride = HUD_COLOUR_BLACK, HUD_COLOURS hcColour1 = HUD_COLOUR_WHITE,  HUD_COLOURS hcColour2 = HUD_COLOUR_WHITE,  HUD_COLOURS hcColour3 = HUD_COLOUR_WHITE,  HUD_COLOURS hcColour4 = HUD_COLOUR_WHITE)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("[JA@MESSAGE] SETUP_NEW_BIG_MESSAGE_WITH_4_PLAYERS_AND_4_COLOURS - initialising a new big message of type: ") NET_PRINT(GET_BIG_MESSAGE_DEBUG_TEXT(messageType)) NET_NL()
		IF playerID != INVALID_PLAYER_INDEX()
			NET_PRINT("[JA@MESSAGE] SETUP_NEW_BIG_MESSAGE_WITH_4_PLAYERS_AND_4_COLOURS - playerID: ") NET_PRINT(GET_PLAYER_NAME(playerID)) NET_NL()
		ELSE
			NET_PRINT("[JA@MESSAGE] SETUP_NEW_BIG_MESSAGE_WITH_4_PLAYERS_AND_4_COLOURS - playerID Not Sent.")NET_NL()
		ENDIF
		
		IF playerID2 != INVALID_PLAYER_INDEX()
			NET_PRINT("[JA@MESSAGE] SETUP_NEW_BIG_MESSAGE_WITH_4_PLAYERS_AND_4_COLOURS - playerID2: ") NET_PRINT(GET_PLAYER_NAME(playerID2)) NET_NL()
		ELSE
			NET_PRINT("[JA@MESSAGE] SETUP_NEW_BIG_MESSAGE_WITH_4_PLAYERS_AND_4_COLOURS - playerID2 Not Sent.")NET_NL()
		ENDIF
		
		IF playerID3 != INVALID_PLAYER_INDEX()
			NET_PRINT("[JA@MESSAGE] SETUP_NEW_BIG_MESSAGE_WITH_4_PLAYERS_AND_4_COLOURS - playerID3: ") NET_PRINT(GET_PLAYER_NAME(playerID3)) NET_NL()
		ELSE
			NET_PRINT("[JA@MESSAGE] SETUP_NEW_BIG_MESSAGE_WITH_4_PLAYERS_AND_4_COLOURS - playerID3 Not Sent.")NET_NL()
		ENDIF
		
		IF playerID4 != INVALID_PLAYER_INDEX()
			NET_PRINT("[JA@MESSAGE] SETUP_NEW_BIG_MESSAGE_WITH_4_PLAYERS_AND_4_COLOURS - playerID4: ") NET_PRINT(GET_PLAYER_NAME(playerID4)) NET_NL()
		ELSE
			NET_PRINT("[JA@MESSAGE] SETUP_NEW_BIG_MESSAGE_WITH_4_PLAYERS_AND_4_COLOURS - playerID4 Not Sent.")NET_NL()
		ENDIF
		NET_PRINT("[JA@MESSAGE] SETUP_NEW_BIG_MESSAGE_WITH_4_PLAYERS_AND_4_COLOURS - hc1: ") NET_PRINT_INT(ENUM_TO_INT(hcColour1)) NET_NL()
		NET_PRINT("[JA@MESSAGE] SETUP_NEW_BIG_MESSAGE_WITH_4_PLAYERS_AND_4_COLOURS - hc2: ") NET_PRINT_INT(ENUM_TO_INT(hcColour2)) NET_NL()
		NET_PRINT("[JA@MESSAGE] SETUP_NEW_BIG_MESSAGE_WITH_4_PLAYERS_AND_4_COLOURS - hc3: ") NET_PRINT_INT(ENUM_TO_INT(hcColour3)) NET_NL()
		NET_PRINT("[JA@MESSAGE] SETUP_NEW_BIG_MESSAGE_WITH_4_PLAYERS_AND_4_COLOURS - hc4: ") NET_PRINT_INT(ENUM_TO_INT(hcColour4)) NET_NL()
	#ENDIF
	
	BIG_MESSAGE newBigMessage
	INIT_BIG_MESSAGE(messageType, newBigMessage, iNumber, tlStrapline, tlMsgTitle)
	newBigMessage.playerID = playerID
	newBigMessage.playerID2 = playerID2
	newBigMessage.playerID3 = playerID3
	newBigMessage.playerID4 = playerID4
	newBigMessage.bigMessageColour = hcOverride
	newBigMessage.iOverrideTime = iOverrideTime
	newBigMessage.bigBGColour = hcBGOverride
	newBigMessage.hcColour1 = hcColour1
	newBigMessage.hcColour2 = hcColour2
	newBigMessage.hcColour3 = hcColour3
	newBigMessage.hcColour4 = hcColour4
	
	RETURN SAVE_BIG_MESSAGE(newBigMessage)
ENDFUNC

/// PURPOSE: Called by any script to force big message with a player name in the strapline (delivered, busted). See SETUP_NEW_BIG_MESSAGE() for param list
///    Beware tlStraplineSubstring2 as this comes before any other sub element in the text label
FUNC BOOL SETUP_NEW_BIG_MESSAGE_WITH_PLAYER_AND_STRING(	BIG_MESSAGE_TYPE messageType, PLAYER_INDEX playerID, STRING tlStraplineSubString, STRING tlStrapline = NULL, STRING tlMsgTitle = NULL, 
														INT iOverrideTime = -1, STRING tlStraplineSubstring2 = NULL, INT iNumber = -1, FLOAT fOptionalFloat = -1.0, HUD_COLOURS hcOverride = HUD_COLOUR_WHITE, 
														HUD_COLOURS hcBGOverride = HUD_COLOUR_BLACK, STRING literalString = NULL)
	
	NET_PRINT("[JA@MESSAGE] SETUP_NEW_BIG_MESSAGE_WITH_PLAYER - initialising a new big message of type: ") NET_PRINT(GET_BIG_MESSAGE_DEBUG_TEXT(messageType)) NET_NL()
	
	BIG_MESSAGE newBigMessage
	INIT_BIG_MESSAGE(messageType, newBigMessage, iNumber, tlStrapline, tlMsgTitle)
	newBigMessage.playerID = playerID
	newBigMessage.tlStrapLineSubString = tlStraplineSubString
	newBigMessage.tlStrapLineSubString1 = tlStraplineSubstring2
	newBigMessage.tlLiteralText = literalString
	newBigMessage.iOverrideTime = iOverrideTime
	newBigMessage.fNumber = fOptionalFloat
	newBigMessage.bigMessageColour = hcOverride
	newBigMessage.bigBGColour = hcBGOverride
	
	RETURN SAVE_BIG_MESSAGE(newBigMessage)
ENDFUNC

/// PURPOSE: Called by any script to force big message with a player name in the strapline (delivered, busted). See SETUP_NEW_BIG_MESSAGE() for param list
FUNC BOOL SETUP_NEW_BIG_MESSAGE_WITH_STRING(BIG_MESSAGE_TYPE messageType, STRING tlStraplineSubString, STRING tlStrapline = NULL, STRING tlMsgTitle = NULL, INT iOverrideTime = -1, INT iNumber = -1, FLOAT fOptionalFloat = -1.0, STRING tlStraplineSubstring2 = NULL, HUD_COLOURS hcOverride = HUD_COLOUR_WHITE, HUD_COLOURS hcBGOverride = HUD_COLOUR_BLACK, BIG_MESSAGE_BIT_SET eOptionalBitSet = BIG_MESSAGE_BIT_INVALID)
	
	NET_PRINT("[JA@MESSAGE] SETUP_NEW_BIG_MESSAGE_WITH_STRING - initialising a new big message of type: ") NET_PRINT(GET_BIG_MESSAGE_DEBUG_TEXT(messageType)) NET_NL()
	
	BIG_MESSAGE newBigMessage
	INIT_BIG_MESSAGE(messageType, newBigMessage, iNumber, tlStrapline, tlMsgTitle)
	newBigMessage.tlStrapLineSubString = tlStraplineSubString
	newBigMessage.tlStrapLineSubString1 = tlStraplineSubstring2
	newBigMessage.iOverrideTime = iOverrideTime
	newBigMessage.fNumber = fOptionalFloat
	newBigMessage.bigMessageColour = hcOverride
	newBigMessage.bigBGColour = hcBGOverride
	
	IF eOptionalBitSet != BIG_MESSAGE_BIT_INVALID
		SET_BITMASK_AS_ENUM(newBigMessage.iBigMessageBitSet, eOptionalBitSet)
	ENDIF
	
	RETURN SAVE_BIG_MESSAGE(newBigMessage)
ENDFUNC

/// PURPOSE: Called by any script to force big message with a player name in the strapline (delivered, busted). See SETUP_NEW_BIG_MESSAGE() for param list
FUNC BOOL SETUP_NEW_BIG_MESSAGE_WITH_THREE_STRINGS(BIG_MESSAGE_TYPE messageType, STRING tlMsgTitle = NULL, STRING tlStrapline = NULL, STRING tlStraplineSubString = NULL, STRING tlStraplineSubstring2 = NULL,  STRING tlStraplineSubstring3 = NULL, INT iOverrideTime = -1, INT iNumber = -1, FLOAT fOptionalFloat = -1.0, HUD_COLOURS hcOverride = HUD_COLOUR_WHITE, HUD_COLOURS hcBGOverride = HUD_COLOUR_BLACK)
	
	NET_PRINT("[JA@MESSAGE] SETUP_NEW_BIG_MESSAGE_WITH_STRING - initialising a new big message of type: ") NET_PRINT(GET_BIG_MESSAGE_DEBUG_TEXT(messageType)) NET_NL()
	
	BIG_MESSAGE newBigMessage
	INIT_BIG_MESSAGE(messageType, newBigMessage, iNumber, tlStrapline, tlMsgTitle)
	newBigMessage.tlStrapLineSubString = tlStraplineSubString
	newBigMessage.tlStrapLineSubString1 = tlStraplineSubstring2
	newBigMessage.tlStrapLineSubString2 = tlStraplineSubstring3
	newBigMessage.iOverrideTime = iOverrideTime
	newBigMessage.fNumber = fOptionalFloat
	newBigMessage.bigMessageColour = hcOverride
	newBigMessage.bigBGColour = hcBGOverride
	
	RETURN SAVE_BIG_MESSAGE(newBigMessage)
ENDFUNC

/// PURPOSE: Called by any script to force big message with a player name in the strapline (delivered, busted). See SETUP_NEW_BIG_MESSAGE() for param list
FUNC BOOL SETUP_NEW_BIG_MESSAGE_WITH_TWO_STRINGS_TWO_INTS(BIG_MESSAGE_TYPE messageType, STRING tlMsgTitle = NULL, STRING tlStrapline = NULL, STRING tlStraplineSubString1 = NULL, STRING tlStraplineSubstring2 = NULL, INT iOverrideTime = -1, INT iNumber1 = -1,INT iNumber2 = -1, HUD_COLOURS hcOverride = HUD_COLOUR_WHITE, HUD_COLOURS hcBGOverride = HUD_COLOUR_BLACK, BIG_MESSAGE_BIT_SET eOptionalBitSet = BIG_MESSAGE_BIT_INVALID)
	
	NET_PRINT("[JA@MESSAGE] SETUP_NEW_BIG_MESSAGE_WITH_STRING - initialising a new big message of type: ") NET_PRINT(GET_BIG_MESSAGE_DEBUG_TEXT(messageType)) NET_NL()
	
	BIG_MESSAGE newBigMessage
	INIT_BIG_MESSAGE(messageType, newBigMessage, iNumber1, tlStrapline, tlMsgTitle)
	newBigMessage.tlStrapLineSubString = tlStraplineSubString1
	newBigMessage.tlStrapLineSubString1 = tlStraplineSubstring2
	newBigMessage.iOverrideTime = iOverrideTime
	newBigMessage.iNumber2 = iNumber2
	newBigMessage.bigMessageColour = hcOverride
	newBigMessage.bigBGColour = hcBGOverride
	
	IF eOptionalBitSet != BIG_MESSAGE_BIT_INVALID
		SET_BITMASK_AS_ENUM(newBigMessage.iBigMessageBitSet, eOptionalBitSet)
	ENDIF
	
	RETURN SAVE_BIG_MESSAGE(newBigMessage)
ENDFUNC

/// PURPOSE: Called by any script to force big message with int value and strapline string (rank up). See SETUP_NEW_BIG_MESSAGE() for param list
FUNC BOOL SETUP_NEW_BIG_MESSAGE_WITH_INT_AND_STRING(BIG_MESSAGE_TYPE messageType, INT number = -1, STRING strapline = NULL, STRING tlMsgTitle = NULL, HUD_COLOURS hcOverride = HUD_COLOUR_WHITE, INT iOverrideTime = -1, FLOAT fOptionalFloat = -1.0, HUD_COLOURS hcBGOverride = HUD_COLOUR_BLACK, BIG_MESSAGE_BIT_SET eOptionalBitSet = BIG_MESSAGE_BIT_INVALID)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("[JA@MESSAGE] SETUP_NEW_BIG_MESSAGE_WITH_INT_AND_STRING - initialising a new big message of type: ") NET_PRINT(GET_BIG_MESSAGE_DEBUG_TEXT(messageType))
		NET_PRINT(" and string: ") NET_PRINT(strapline)
		NET_PRINT(" and float: ") NET_PRINT_FLOAT(fOptionalFloat) NET_NL()
	#ENDIF
	
	BIG_MESSAGE newBigMessage
	INIT_BIG_MESSAGE(messageType, newBigMessage, number, strapline, tlMsgTitle)
	newBigMessage.bigMessageColour = hcOverride
	newBigMessage.iOverrideTime = iOverrideTime
	newBigMessage.fNumber = fOptionalFloat
	newBigMessage.bigBGColour = hcBGOverride
	
	IF eOptionalBitSet != BIG_MESSAGE_BIT_INVALID
		SET_BITMASK_AS_ENUM(newBigMessage.iBigMessageBitSet, eOptionalBitSet)
	ENDIF
	
	RETURN SAVE_BIG_MESSAGE(newBigMessage)
ENDFUNC

/// PURPOSE: Called by any script to force big message with int value and strapline string (rank up). See SETUP_NEW_BIG_MESSAGE() for param list
FUNC BOOL SETUP_NEW_BIG_MESSAGE_WITH_INT_AND_STRING_AND_COLOUR(BIG_MESSAGE_TYPE messageType, INT number = -1, STRING strapline = NULL, STRING tlMsgTitle = NULL, HUD_COLOURS hcOverride = HUD_COLOUR_WHITE, INT iOverrideTime = -1, FLOAT fOptionalFloat = -1.0, HUD_COLOURS hcBGOverride = HUD_COLOUR_BLACK, HUD_COLOURS hcColour1 = HUD_COLOUR_WHITE, STRING tlStraplineSubString1 = NULL)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("[JA@MESSAGE] SETUP_NEW_BIG_MESSAGE_WITH_INT_AND_STRING - initialising a new big message of type: ") NET_PRINT(GET_BIG_MESSAGE_DEBUG_TEXT(messageType))
		NET_PRINT(" and string: ") NET_PRINT(strapline)
		NET_PRINT(" and float: ") NET_PRINT_FLOAT(fOptionalFloat) NET_NL()
	#ENDIF
	
	BIG_MESSAGE newBigMessage
	INIT_BIG_MESSAGE(messageType, newBigMessage, number, strapline, tlMsgTitle)
	newBigMessage.bigMessageColour = hcOverride
	newBigMessage.iOverrideTime = iOverrideTime
	newBigMessage.fNumber = fOptionalFloat
	newBigMessage.bigBGColour = hcBGOverride
	newBigMessage.hcColour1 = hcColour1
	newBigMessage.tlStrapLineSubString = tlStraplineSubString1
	
	RETURN SAVE_BIG_MESSAGE(newBigMessage)
ENDFUNC

/// PURPOSE: Called by any script to force a big message to be initialised with literal string in strapline. See SETUP_NEW_BIG_MESSAGE() for param list
FUNC BOOL SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_AND_COLOUR(BIG_MESSAGE_TYPE messageType, STRING tlStrapline, STRING literalString, STRING tlMsgTitle = NULL, HUD_COLOURS hcOverride = HUD_COLOUR_WHITE, INT iOverrideTime = -1, HUD_COLOURS hcBGOverride = HUD_COLOUR_BLACK, HUD_COLOURS hcColour1 = HUD_COLOUR_WHITE, BOOL bColourLiteralString = FALSE)

	#IF IS_DEBUG_BUILD
		NET_PRINT("[JA@MESSAGE] SETUP_NEW_BIG_MESSAGE_WITH_PLAYER_AND_LITERAL_STRING - initialising a new big message of type: ") NET_PRINT(GET_BIG_MESSAGE_DEBUG_TEXT(messageType)) NET_NL()
	#ENDIF

	BIG_MESSAGE newBigMessage
	INIT_BIG_MESSAGE(messageType, newBigMessage, -1, tlStrapline, tlMsgTitle)
	
	IF bColourLiteralString		
		SET_BITMASK_AS_ENUM(newBigMessage.iBigMessageBitSet, BIG_MESSAGE_BIT_COLOUR_LITERAL_STRING)
	ENDIF
	
	newBigMessage.tlLiteralText = literalString
	newBigMessage.bigMessageColour = hcOverride
	newBigMessage.iOverrideTime = iOverrideTime
	newBigMessage.bigBGColour = hcBGOverride
	newBigMessage.hcColour1 = hcColour1

	RETURN SAVE_BIG_MESSAGE(newBigMessage)
ENDFUNC

/// PURPOSE: Called by any script to force big message with 2 ints value (e.g. Lap). See SETUP_NEW_BIG_MESSAGE() for param list
FUNC BOOL SETUP_NEW_BIG_MESSAGE_WITH_2_INTS(BIG_MESSAGE_TYPE messageType, INT iNumber1 = -1, INT iNumber2 = -1, STRING strapline = NULL, HUD_COLOURS hcOverride = HUD_COLOUR_WHITE, INT iOverrideTime = -1, HUD_COLOURS hcBGOverride = HUD_COLOUR_BLACK, STRING tlMsgTitle = NULL, STRING tlStraplineSubString = NULL)

	#IF IS_DEBUG_BUILD
		NET_PRINT("[JA@MESSAGE] SETUP_NEW_BIG_MESSAGE_WITH_2_INTS - initialising a new big message of type: ") NET_PRINT(GET_BIG_MESSAGE_DEBUG_TEXT(messageType)) NET_NL()
	#ENDIF
	
	BIG_MESSAGE newBigMessage
	INIT_BIG_MESSAGE(messageType, newBigMessage, iNumber1, strapline, tlMsgTitle)
		
	newBigMessage.iNumber2 = iNumber2
	newBigMessage.bigMessageColour = hcOverride
	newBigMessage.iOverrideTime = iOverrideTime
	newBigMessage.bigBGColour = hcBGOverride
	newBigMessage.tlStrapLineSubString = tlStraplineSubString
	
	RETURN SAVE_BIG_MESSAGE(newBigMessage)
ENDFUNC

/// PURPOSE: Called by any script to force big message with 2 ints value (e.g. Lap). See SETUP_NEW_BIG_MESSAGE() for param list
FUNC BOOL SETUP_NEW_BIG_MESSAGE_WITH_2_INTS_AND_COLOUR(BIG_MESSAGE_TYPE messageType, INT iNumber1 = -1, INT iNumber2 = -1, STRING strapline = NULL, HUD_COLOURS hcOverride = HUD_COLOUR_WHITE, INT iOverrideTime = -1, HUD_COLOURS hcBGOverride = HUD_COLOUR_BLACK, STRING tlMsgTitle = NULL, STRING tlStraplineSubString = NULL,
											HUD_COLOURS hcColour1 = HUD_COLOUR_WHITE, HUD_COLOURS hcColour2 = HUD_COLOUR_WHITE)

	#IF IS_DEBUG_BUILD
		NET_PRINT("[JA@MESSAGE] SETUP_NEW_BIG_MESSAGE_WITH_2_INTS - initialising a new big message of type: ") NET_PRINT(GET_BIG_MESSAGE_DEBUG_TEXT(messageType)) NET_NL()
	#ENDIF
	
	BIG_MESSAGE newBigMessage
	INIT_BIG_MESSAGE(messageType, newBigMessage, iNumber1, strapline, tlMsgTitle)
		
	newBigMessage.iNumber2 = iNumber2
	newBigMessage.bigMessageColour = hcOverride
	newBigMessage.iOverrideTime = iOverrideTime
	newBigMessage.bigBGColour = hcBGOverride
	newBigMessage.tlStrapLineSubString = tlStraplineSubString
	newBigMessage.hcColour1 = hcColour1
	newBigMessage.hcColour2 = hcColour2
	
	RETURN SAVE_BIG_MESSAGE(newBigMessage)
ENDFUNC

/// PURPOSE: Called by any script to force a big message to be initialised with literal string in strapline. See SETUP_NEW_BIG_MESSAGE() for param list
FUNC BOOL SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_IN_STRAPLINE(BIG_MESSAGE_TYPE messageType, STRING tlStrapline, STRING literalString, HUD_COLOURS hcOverride = HUD_COLOUR_WHITE, INT iOverrideTime = -1, STRING tlMsgTitle = NULL, HUD_COLOURS hcBGOverride = HUD_COLOUR_BLACK)

	#IF IS_DEBUG_BUILD
		NET_PRINT("[JA@MESSAGE] SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_IN_STRAPLINE - initialising a new big message of type: ") NET_PRINT(GET_BIG_MESSAGE_DEBUG_TEXT(messageType)) NET_NL()
	#ENDIF

	BIG_MESSAGE newBigMessage
	INIT_BIG_MESSAGE(messageType, newBigMessage, -1, tlStrapline, tlMsgTitle)

	newBigMessage.tlLiteralText = literalString
	newBigMessage.bigMessageColour = hcOverride
	newBigMessage.iOverrideTime = iOverrideTime
	newBigMessage.bigBGColour = hcBGOverride

	RETURN SAVE_BIG_MESSAGE(newBigMessage)
ENDFUNC


/// PURPOSE: Called by any script to force a big message to be initialised with literal string in strapline. See SETUP_NEW_BIG_MESSAGE() for param list
FUNC BOOL SETUP_NEW_BIG_MESSAGE_WITH_PLAYER_AND_LITERAL_STRING(BIG_MESSAGE_TYPE messageType, PLAYER_INDEX playerID, STRING tlStrapline, STRING literalString, STRING tlMsgTitle = NULL, HUD_COLOURS hcOverride = HUD_COLOUR_WHITE, INT iOverrideTime = -1, HUD_COLOURS hcBGOverride = HUD_COLOUR_BLACK)

	#IF IS_DEBUG_BUILD
		NET_PRINT("[JA@MESSAGE] SETUP_NEW_BIG_MESSAGE_WITH_PLAYER_AND_LITERAL_STRING - initialising a new big message of type: ") NET_PRINT(GET_BIG_MESSAGE_DEBUG_TEXT(messageType)) NET_NL()
	#ENDIF

	BIG_MESSAGE newBigMessage
	INIT_BIG_MESSAGE(messageType, newBigMessage, -1, tlStrapline, tlMsgTitle)

	newBigMessage.playerID = playerID
	newBigMessage.tlLiteralText = literalString
	newBigMessage.bigMessageColour = hcOverride
	newBigMessage.iOverrideTime = iOverrideTime
	newBigMessage.bigBGColour = hcBGOverride

	RETURN SAVE_BIG_MESSAGE(newBigMessage)
ENDFUNC


/// PURPOSE: Called by any script to force a big message to be initialised with literal string and int in strapline. See SETUP_NEW_BIG_MESSAGE() for param list
FUNC BOOL SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_AND_INT(BIG_MESSAGE_TYPE messageType,INT number = -1, STRING tlStrapline = NULL, STRING literalString = NULL, STRING tlMsgTitle = NULL, HUD_COLOURS hcOverride = HUD_COLOUR_WHITE, INT iOverrideTime = -1, HUD_COLOURS hcBGOverride = HUD_COLOUR_BLACK)

	#IF IS_DEBUG_BUILD
		NET_PRINT("[JA@MESSAGE] SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_AND_INT - initialising a new big message of type: ") NET_PRINT(GET_BIG_MESSAGE_DEBUG_TEXT(messageType)) NET_NL()
	#ENDIF

	BIG_MESSAGE newBigMessage
	INIT_BIG_MESSAGE(messageType, newBigMessage, number, tlStrapline,tlMsgTitle)

	newBigMessage.tlLiteralText = literalString
	newBigMessage.bigMessageColour = hcOverride
	newBigMessage.iOverrideTime = iOverrideTime
	newBigMessage.bigBGColour = hcBGOverride

	RETURN SAVE_BIG_MESSAGE(newBigMessage)
ENDFUNC

/// PURPOSE: Called by any script to force big message with 3 ints value (e.g. Pos and time)
///    Please check with James before using this command (specific order of where ints are inserted into strings). See SETUP_NEW_BIG_MESSAGE() for param list
FUNC BOOL SETUP_NEW_BIG_MESSAGE_WITH_3_INTS(BIG_MESSAGE_TYPE messageType, INT iNumber1 = -1, INT iNumber2 = -1, INT iNumber3 = -1, STRING strapline = NULL, STRING tlMsgTitle = NULL, HUD_COLOURS hcOverride = HUD_COLOUR_WHITE, INT iOverrideTime = -1, HUD_COLOURS hcBGOverride = HUD_COLOUR_BLACK)

	#IF IS_DEBUG_BUILD
		NET_PRINT("[JA@MESSAGE] SETUP_NEW_BIG_MESSAGE_WITH_3_INTS - initialising a new big message of type: ") NET_PRINT(GET_BIG_MESSAGE_DEBUG_TEXT(messageType)) NET_NL()
	#ENDIF
	
	BIG_MESSAGE newBigMessage
	INIT_BIG_MESSAGE(messageType, newBigMessage, iNumber1, strapline, tlMsgTitle)
		
	newBigMessage.iNumber2 = iNumber2
	newBigMessage.iNumber3 = iNumber3
	
	newBigMessage.bigMessageColour = hcOverride
	newBigMessage.iOverrideTime = iOverrideTime
	newBigMessage.bigBGColour = hcBGOverride
	
	RETURN SAVE_BIG_MESSAGE(newBigMessage)
ENDFUNC

/// PURPOSE: Called by any script to force big message with 2 ints in the title.
///    Please check with James before using this command. See SETUP_NEW_BIG_MESSAGE() for param list
FUNC BOOL SETUP_NEW_BIG_MESSAGE_WITH_2_INTS_IN_TITLE(BIG_MESSAGE_TYPE messageType, STRING tlMsgTitle, INT iNumber1 = -1, INT iNumber2 = -1, PLAYER_INDEX playerID = NULL, STRING strapline = NULL, HUD_COLOURS hcColour1 = HUD_COLOUR_WHITE, HUD_COLOURS hcColour2 = HUD_COLOUR_WHITE,
													HUD_COLOURS hcOverride = HUD_COLOUR_WHITE, INT iOverrideTime = -1, HUD_COLOURS hcBGOverride = HUD_COLOUR_BLACK)

	#IF IS_DEBUG_BUILD
		NET_PRINT("[JA@MESSAGE] SETUP_NEW_BIG_MESSAGE_WITH_2_INTS_IN_TITLE - initialising a new big message of type: ") NET_PRINT(GET_BIG_MESSAGE_DEBUG_TEXT(messageType)) NET_NL()
	#ENDIF
	
	BIG_MESSAGE newBigMessage
	INIT_BIG_MESSAGE(messageType, newBigMessage, iNumber1, strapline, tlMsgTitle)
		
	newBigMessage.iNumber2 = iNumber2
	newBigMessage.playerID = playerID
	newBigMessage.bigMessageColour = hcOverride
	newBigMessage.iOverrideTime = iOverrideTime
	newBigMessage.bigBGColour = hcBGOverride
	newBigMessage.hcColour1 = hcColour1
	newBigMessage.hcColour2 = hcColour2
	
	RETURN SAVE_BIG_MESSAGE(newBigMessage)
ENDFUNC

//BIG_MESSAGE_2_INTS_IN_TITLE

/// PURPOSE: Generic Setup function for Gang Boss Organisation Shards
FUNC BOOL SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION_WITH_PLAYERS(	BIG_MESSAGE_TYPE messageType,
																STRING tlMsgTitle,
																STRING strapline,
																STRING organizationString,
																HUD_COLOURS hcOrganizationColour,
																PLAYER_INDEX playerID,
																PLAYER_INDEX playerID2,
																PLAYER_INDEX playerID3,
																STRING tlStraplineSubString = NULL,
																INT iNumber1 = -1,
																INT iNumber2 = -1,
																INT iNumber3 = -1,
																HUD_COLOURS hcBGOverride = HUD_COLOUR_BLACK,
																INT iOverrideTime = -1,
																HUD_COLOURS hcAnimateOut = HUD_COLOUR_BLACK)

	PRINTLN("[JA@MESSAGE] SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION - ", GET_BIG_MESSAGE_DEBUG_TEXT(messageType))
	PRINTLN("[JA@MESSAGE] 				tlMsgTitle:				", tlMsgTitle)
	PRINTLN("[JA@MESSAGE] 				strapline:	 			", strapline)
	PRINTLN("[JA@MESSAGE] 				organizationString:	 	", organizationString)
	PRINTLN("[JA@MESSAGE] 				tlStraplineSubString: 	", tlStraplineSubString)	
	PRINTLN("[JA@MESSAGE] 				hcOrganizationColour:	", ENUM_TO_INT(hcOrganizationColour))
	PRINTLN("[JA@MESSAGE] 				hcAnimateOut:	        ", ENUM_TO_INT(hcAnimateOut))
	DEBUG_PRINTCALLSTACK()
	
	BIG_MESSAGE newBigMessage
	INIT_BIG_MESSAGE(messageType, newBigMessage, iNumber1, strapline, tlMsgTitle)
	
	newBigMessage.iNumber2 = iNumber2
	newBigMessage.iNumber3 = iNumber3
	
	newBigMessage.playerID = playerID
	newBigMessage.playerID2 = playerID2
	newBigMessage.playerID3 = playerID3
	
	newBigMessage.tlLiteralText = organizationString
	newBigMessage.tlStrapLineSubString = tlStraplineSubString
	
	newBigMessage.hcPlayerName = hcOrganizationColour
	newBigMessage.bigMessageColour = hcOrganizationColour
	newBigMessage.iOverrideTime = iOverrideTime
	newBigMessage.bigBGColour = hcBGOverride
	
	IF hcAnimateOut != HUD_COLOUR_BLACK
		PRINTLN("[JA@MESSAGE] 				override animate out to:	", ENUM_TO_INT(hcAnimateOut))	
		newBigMessage.bigMessageColour = hcAnimateOut
	ENDIF
	
	SET_BITMASK_AS_ENUM(newBigMessage.iBigMessageBitSet, BIG_MESSAGE_BIT_LITERAL_STRING_IS_ORG)
	
	RETURN SAVE_BIG_MESSAGE(newBigMessage)
ENDFUNC

FUNC BOOL SETUP_NEW_BIG_MESSAGE_FOR_COLOURED_ORGANIZATION_WITH_PLAYERS(	BIG_MESSAGE_TYPE messageType,
																STRING tlMsgTitle,
																STRING strapline,
																STRING organizationString,
																HUD_COLOURS hcOrganizationColour,
																PLAYER_INDEX playerID,
																PLAYER_INDEX playerID2,
																PLAYER_INDEX playerID3,
																STRING tlStraplineSubString = NULL,
																INT iNumber1 = -1,
																INT iNumber2 = -1,
																INT iNumber3 = -1,
																HUD_COLOURS hcBGOverride = HUD_COLOUR_BLACK,
																INT iOverrideTime = -1,
																HUD_COLOURS hcAnimateOut = HUD_COLOUR_BLACK)
	
	PRINTLN("[JA@MESSAGE] SETUP_NEW_BIG_MESSAGE_FOR_COLOURED_ORGANIZATION_WITH_PLAYERS - ", GET_BIG_MESSAGE_DEBUG_TEXT(messageType))
	PRINTLN("[JA@MESSAGE] 				tlMsgTitle:				", tlMsgTitle)
	PRINTLN("[JA@MESSAGE] 				strapline:	 			", strapline)
	PRINTLN("[JA@MESSAGE] 				organizationString:	 	", organizationString)
	PRINTLN("[JA@MESSAGE] 				tlStraplineSubString: 	", tlStraplineSubString)	
	PRINTLN("[JA@MESSAGE] 				hcOrganizationColour:	", ENUM_TO_INT(hcOrganizationColour))
	PRINTLN("[JA@MESSAGE] 				hcAnimateOut:	        ", ENUM_TO_INT(hcAnimateOut))
	DEBUG_PRINTCALLSTACK()
	
	BIG_MESSAGE newBigMessage
	INIT_BIG_MESSAGE(messageType, newBigMessage, iNumber1, strapline, tlMsgTitle)
	
	newBigMessage.iNumber2 = iNumber2
	newBigMessage.iNumber3 = iNumber3
	
	newBigMessage.playerID = playerID
	newBigMessage.playerID2 = playerID2
	newBigMessage.playerID3 = playerID3
	
	newBigMessage.tlLiteralText = organizationString
	newBigMessage.tlStrapLineSubString = tlStraplineSubString
	
	newBigMessage.hcPlayerName = hcOrganizationColour
	newBigMessage.bigMessageColour = hcOrganizationColour
	newBigMessage.iOverrideTime = iOverrideTime
	newBigMessage.bigBGColour = hcBGOverride
	
	
	IF hcAnimateOut != HUD_COLOUR_BLACK
		PRINTLN("[JA@MESSAGE] 				override animate out to:	", ENUM_TO_INT(hcAnimateOut))	
		newBigMessage.bigMessageColour = hcAnimateOut
	ENDIF
	
	SET_BITMASK_AS_ENUM(newBigMessage.iBigMessageBitSet, BIG_MESSAGE_BIT_LITERAL_STRING_IS_ORG | BIG_MESSAGE_BIT_COLOURED_ORG_ONLY)
	
	RETURN SAVE_BIG_MESSAGE(newBigMessage)
ENDFUNC

FUNC BOOL SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(	BIG_MESSAGE_TYPE messageType,
													STRING tlMsgTitle,
													STRING strapline,
													STRING organizationString,
													HUD_COLOURS hcOrganizationColour,
													STRING tlStraplineSubString = NULL,
													INT iNumber1 = -1,
													INT iNumber2 = -1,
													INT iNumber3 = -1,
													HUD_COLOURS hcBGOverride = HUD_COLOUR_BLACK,
													INT iOverrideTime = -1)
	
	PLAYER_INDEX playerID = INVALID_PLAYER_INDEX(), playerID2 = INVALID_PLAYER_INDEX(), playerID3 = INVALID_PLAYER_INDEX()
	RETURN SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION_WITH_PLAYERS(messageType, tlMsgTitle, strapline, organizationString, hcOrganizationColour, playerID, playerID2, playerID3, tlStraplineSubString,
														iNumber1,iNumber2,iNumber3,hcBGOverride,iOverrideTime)
ENDFUNC

// PURPOSE: Allows you to update a already displaying custom big message
PROC UPDATE_CUSTOM_BIG_MESSAGE(STRING tlTitle)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("[JA@MESSAGE] UPDATE_CUSTOM_BIG_MESSAGE - ") NET_PRINT(tlTitle) NET_NL()
	#ENDIF
	
	
	IF MPGlobals.g_BigMessage[0].messageType = BIG_MESSAGE_CUSTOM
	OR MPGlobals.g_BigMessage[0].messageType = BIG_MESSAGE_GENERIC_MIDSIZED_CUSTOM
	OR MPGlobals.g_BigMessage[0].messageType = BIG_MESSAGE_GENERIC_MIDSIZED_CUSTOM_TITLE_INT
		IF HAS_SCALEFORM_MOVIE_LOADED(MPGlobals.g_BigMessage[0].movieID)
			BEGIN_SCALEFORM_MOVIE_METHOD(MPGlobals.g_BigMessage[0].movieID, GET_BIG_MESSAGE_SCALEFORM_METHOD(MPGlobals.g_BigMessage[0]))
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(tlTitle)
			END_SCALEFORM_MOVIE_METHOD()
		ENDIF
	ENDIF
ENDPROC

// Returns the currently displayed big messaged text.
FUNC TEXT_LABEL_15 GET_BIG_MESSAGE_TEXT_BEING_DRAWN()
	TEXT_LABEL_15 tl15
	IF (MPGlobals.g_BigMessage[0].messageType != BIG_MESSAGE_NULL)
		tl15 = MPGlobals.g_BigMessage[0].tlTitle
	ENDIF
	RETURN tl15
ENDFUNC

// Query if specific type of big message is being displayed
FUNC BOOL IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_TYPE messageType)
	RETURN (MPGlobals.g_BigMessage[0].messageType = messageType)
ENDFUNC

/// PURPOSE:
///     Query if the big message is in the drawing state
FUNC BOOL IS_BIG_MESSAGE_BEING_DRAWN()
	RETURN (MPGlobals.g_BigMessage[0].iMessageState = BIG_MESSAGE_STATE_ACTIVE)
ENDFUNC

// Query if any big message is being displayed
FUNC BOOL IS_ANY_BIG_MESSAGE_BEING_DISPLAYED()
	RETURN (MPGlobals.g_BigMessage[0].messageType != BIG_MESSAGE_NULL)
ENDFUNC

/// PURPOSE:
///    Returns the current big message type
FUNC BIG_MESSAGE_TYPE GET_TYPE_OF_CURRENT_BIG_MESSAGE()
	RETURN MPGlobals.g_BigMessage[0].messageType
ENDFUNC

FUNC BOOL IS_ANY_GB_BIG_MESSAGE_BEING_DISPLAYED()
	
	
	IF IS_ANY_BIG_MESSAGE_BEING_DISPLAYED()
		IF IS_BIG_MESSAGE_TYPE_FOR_GANG_BOSS(MPGlobals.g_BigMessage[0].messageType)
			RETURN TRUE
		ENDIF
	ENDIF
	
	 // #IF FEATURE_GANG_BOSS
		
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ANY_FM_EVENTS_BIG_MESSAGE_BEING_DISPLAYED()
	
	
	IF IS_ANY_BIG_MESSAGE_BEING_DISPLAYED()
		IF IS_BIG_MESSAGE_TYPE_FOR_FM_EVENTS(MPGlobals.g_BigMessage[0].messageType)
			RETURN TRUE
		ENDIF
	ENDIF
	
	 // #IF FEATURE_NEW_AMBIENT_EVENTS
		
	RETURN FALSE
ENDFUNC


PROC CLEANUP_BIG_MESSAGE()
	#IF IS_DEBUG_BUILD
	PRINTLN("CLEANUP_BIG_MESSAGE - been called:")
	DEBUG_PRINTCALLSTACK()
	#ENDIF
	//CLEANUP_BIG_MESSAGE_FROM_INDEX(0)
	IF MPGlobals.g_BigMessage[0].iMessageState != BIG_MESSAGE_STATE_IDLE
		NET_PRINT("[JA@MESSAGE] CLEANUP_BIG_MESSAGE - set big message index 0 to BIG_MESSAGE_STATE_CLEANUP") NET_NL()
		MPGlobals.g_BigMessage[0].iMessageState = BIG_MESSAGE_STATE_CLEANUP
	ENDIF
ENDPROC

PROC CLEAR_ALL_BIG_MESSAGES()
	#IF IS_DEBUG_BUILD
		NET_PRINT("[JA@MESSAGE] CLEAR_ALL_BIG_MESSAGE - cleaning up all big messages") NET_NL()
		DEBUG_PRINTCALLSTACK()
	#ENDIF
	INT i
	FOR i = 0 TO NUM_BIG_MESSAGE-1 STEP 1
		IF MPGlobals.g_BigMessage[i].iMessageState != BIG_MESSAGE_STATE_IDLE
			//CLEANUP_BIG_MESSAGE_FROM_INDEX(i)
			NET_PRINT("[JA@MESSAGE] CLEAR_ALL_BIG_MESSAGE - set big message index ") NET_PRINT_INT(i) NET_PRINT(" to BIG_MESSAGE_STATE_CLEANUP") NET_NL()
			MPGlobals.g_BigMessage[i].iMessageState = BIG_MESSAGE_STATE_CLEANUP
			SET_BITMASK_AS_ENUM(MPGlobals.g_BigMessage[i].iBigMessageBitSet, BIG_MESSAGE_BIT_CLEANUP_ALL)
		ENDIF
	ENDFOR
ENDPROC

PROC CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_TYPE messageType)
	#IF IS_DEBUG_BUILD
		PRINTLN("[JA@MESSAGE] CLEAR_ALL_BIG_MESSAGES_OF_TYPE - cleaning up all big messages of specific type: ", GET_BIG_MESSAGE_DEBUG_TEXT(messageType))
		DEBUG_PRINTCALLSTACK()
	#ENDIF

	INT i
	FOR i = 0 TO NUM_BIG_MESSAGE-1 STEP 1
		IF MPGlobals.g_BigMessage[i].messageType = messageType
			//CLEANUP_BIG_MESSAGE_FROM_INDEX(i)
			PRINTLN("[JA@MESSAGE] CLEAR_ALL_BIG_MESSAGES_OF_TYPE - set big message index ", i, " to BIG_MESSAGE_STATE_CLEANUP")
			MPGlobals.g_BigMessage[i].iMessageState = BIG_MESSAGE_STATE_CLEANUP
			SET_BITMASK_AS_ENUM(MPGlobals.g_BigMessage[i].iBigMessageBitSet, BIG_MESSAGE_BIT_CLEANUP_ALL)
		ENDIF
	ENDFOR
ENDPROC

/// PURPOSE: Allows script / mission to allow big messages to be displayed while player is in spectator cam
/// PARAMS:
///    bEnabled - Should be allowed or not
PROC SET_BIG_MESSAGE_ENABLED_WHEN_SPECTATING(BOOL bEnabled)
	MPGlobals.g_bDisplayMsgOnSpecCam = bEnabled
ENDPROC

/// PURPOSE: Allows scripts to suppress WASTED msg (Use with care - if moving player to spectator cam etc.)
PROC SET_BIG_MESSAGE_SUPPRESS_WASTED(BOOL bSuppress)
	PRINTLN("[JA@MESSAGE] SET_BIG_MESSAGE_SUPPRESS_WASTED - ", PICK_STRING(bSuppress, "TRUE", "FALSE"))
	DEBUG_PRINTCALLSTACK()
	
	MPGlobals.g_bSuppressWastedBM = bSuppress
ENDPROC

/// PURPOSE: Returns TRUE if message of type passed is already queued.  
FUNC BOOL IS_BIG_MESSAGE_ALREADY_REQUESTED(BIG_MESSAGE_TYPE messageType)

	INT i
	REPEAT NUM_BIG_MESSAGE i
		
		IF MPGlobals.g_BigMessage[i].messageType = messageType
			RETURN TRUE
		ENDIF
		
		IF MPGlobals.g_BigMessage[i].messageType = BIG_MESSAGE_NULL
			RETURN FALSE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC



/// PURPOSE:
///    Called every frame to update any current big message or check if new one needs to be initialised
PROC MAINTAIN_BIG_MESSAGE()
	
	IF g_i_cashForEndEventShard != 0
		
		IF MPGlobals.g_BigMessage[0].messageType = BIG_MESSAGE_GB_START_OF_JOB
			g_i_cashForEndEventShard = 0
			IF IS_PC_VERSION()
				IF g_sMptunables.bBigMessageCallEndFakeCashTRUE
					USE_FAKE_MP_CASH(TRUE)
				ELIF g_sMptunables.bBigMessageCallEndFakeCashFALSE
					USE_FAKE_MP_CASH(FALSE)
				ENDIF
			ENDIF
			RESET_NET_TIMER(st_ScriptTimers_BIG_MESSAGE_CashDisplay)
			PRINTLN("[JA@MESSAGE][FAKECASH] MAINTAIN_BIG_MESSAGE - Message type: BIG_MESSAGE_GB_START_OF_JOB. g_i_cashForEndEventShard = 0")
		ENDIF
	
		// Confirm the global has not been reset from above check
		IF g_i_cashForEndEventShard != 0
			IF HAS_NET_TIMER_STARTED(st_ScriptTimers_BIG_MESSAGE_CashDisplay)
				IF HAS_NET_TIMER_EXPIRED_READ_ONLY(st_ScriptTimers_BIG_MESSAGE_CashDisplay, ciFM_EVENTS_END_UI_SHARD_TIME)
					REMOVE_MULTIPLAYER_BANK_CASH()
					REMOVE_MULTIPLAYER_WALLET_CASH()
					CHANGE_FAKE_MP_CASH(0, 0)
					IF IS_PC_VERSION()
						IF g_sMptunables.bBigMessageCallEndFakeCashTRUE
							USE_FAKE_MP_CASH(TRUE)
						ELIF g_sMptunables.bBigMessageCallEndFakeCashFALSE
							USE_FAKE_MP_CASH(FALSE)
						ENDIF
					ENDIF
					PRINTLN("[JA@MESSAGE][FAKECASH] MAINTAIN_BIG_MESSAGE - remove the cash display this frame ")
					g_i_cashForEndEventShard = 0
					RESET_NET_TIMER(st_ScriptTimers_BIG_MESSAGE_CashDisplay)
				ELSE
					IF MPGlobals.g_BigMessage[0].iMessageState != BIG_MESSAGE_STATE_CLEANUP
						IF IS_PC_VERSION()
							IF g_i_cashForEndEventShard != 0		
								IF g_sMptunables.bBigMessageCallFakeCashTRUE
									USE_FAKE_MP_CASH(TRUE)
								ELIF g_sMptunables.bBigMessageCallFakeCashFALSE
									USE_FAKE_MP_CASH(FALSE)
								ENDIF
								PRINTLN("[JA@MESSAGE][FAKECASH] DisplayBackup fake cash PC g_i_cashForEndEventShard = ", g_i_cashForEndEventShard, " " )
								SET_MULTIPLAYER_BANK_CASH()
								REMOVE_MULTIPLAYER_WALLET_CASH()
								CHANGE_FAKE_MP_CASH(0, g_i_cashForEndEventShard)
							
							ENDIF
						ELSE	
							IF g_i_cashForEndEventShard != 0
								PRINTLN("[JA@MESSAGE][FAKECASH] DisplayBackup fake cash console g_i_cashForEndEventShard = ", g_i_cashForEndEventShard, " ")
								CHANGE_FAKE_MP_CASH(g_i_cashForEndEventShard, 0)
								SET_MULTIPLAYER_WALLET_CASH()
								REMOVE_MULTIPLAYER_BANK_CASH()
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	BOOL bPerformFullLoopCheck = FALSE
	SWITCH MPGlobals.g_BigMessage[0].iMessageState		// Always look at first in queue
	
		CASE BIG_MESSAGE_STATE_IDLE
			// Do nothing this frame -> no big messages queued
		BREAK
	
		CASE BIG_MESSAGE_STATE_QUEUED
			IF HAS_BIG_MESSAGE_LOADED(MPGlobals.g_BigMessage[0])		
				PRINTLN("[JA@MESSAGE] MAINTAIN_BIG_MESSAGE - loaded change to BIG_MESSAGE_STATE_INIT: ", GET_BIG_MESSAGE_DEBUG_TEXT(MPGlobals.g_BigMessage[0].messageType))

				MPGlobals.g_BigMessage[0].iMessageState = BIG_MESSAGE_STATE_INIT
			ENDIF
		BREAK
		
		CASE BIG_MESSAGE_STATE_INIT
			IF BEGIN_BIG_MESSAGE(MPGlobals.g_BigMessage[0])
				PRINTLN("[JA@MESSAGE] MAINTAIN_BIG_MESSAGE - scaleform methods called, change to BIG_MESSAGE_STATE_ACTIVE: ", GET_BIG_MESSAGE_DEBUG_TEXT(MPGlobals.g_BigMessage[0].messageType))
				
				IF g_i_cashForEndEventShard != 0
					IF IS_BIG_MESSAGE_TYPE_FOR_FM_EVENTS_END(MPGlobals.g_BigMessage[0].messageType)
					OR IS_BIG_MESSAGE_TYPE_FOR_GANG_BOSS_END(MPGlobals.g_BigMessage[0].messageType)
						REINIT_NET_TIMER(st_ScriptTimers_BIG_MESSAGE_CashDisplay)
						PRINTLN("[JA@MESSAGE][FAKECASH] MAINTAIN_BIG_MESSAGE - start cash timer ")
					ENDIF
				ENDIF
				
				MPGlobals.g_BigMessage[0].iMessageState = BIG_MESSAGE_STATE_ACTIVE
			ENDIF
		BREAK
	
		CASE BIG_MESSAGE_STATE_ACTIVE
			// Draw scaleform if its safe to do so, if not delay
			IF IS_SAFE_TO_DISPLAY_BIG_MESSAGE()
				IF IS_BIG_MESSAGE_VALID(MPGlobals.g_BigMessage[0])
					DRAW_BIG_MESSAGE(MPGlobals.g_BigMessage[0])
				ELSE
					PRINTLN("[JA@MESSAGE] MAINTAIN_BIG_MESSAGE - big message no longer valid, change to BIG_MESSAGE_STATE_CLEANUP: ", GET_BIG_MESSAGE_DEBUG_TEXT(MPGlobals.g_BigMessage[0].messageType))
				
					MPGlobals.g_BigMessage[0].iMessageState = BIG_MESSAGE_STATE_CLEANUP
				ENDIF
				
				// Begin the animation out if the time has expired
				IF SHOULD_BIG_MESSAGE_ANIMATE_OUT(MPGlobals.g_BigMessage[0])
					MPGlobals.g_BigMessage[0].bWillAnimate = TRUE
										
					PRINTLN("[JA@MESSAGE] MAINTAIN_BIG_MESSAGE - Big message will animate out: ", GET_BIG_MESSAGE_DEBUG_TEXT(MPGlobals.g_BigMessage[0].messageType))
					
					ANIMATE_BIG_MESSAGE_OUT(MPGlobals.g_BigMessage[0])
				ENDIF
				
				// If we are finished with this big message, push it to the cleanup stage (this will currently be a frame out)
				IF HAS_BIG_MESSAGE_TIME_EXPIRED(MPGlobals.g_BigMessage[0])
					PRINTLN("[JA@MESSAGE] MAINTAIN_BIG_MESSAGE - big message timer expired, change to BIG_MESSAGE_STATE_CLEANUP: ", GET_BIG_MESSAGE_DEBUG_TEXT(MPGlobals.g_BigMessage[0].messageType))
				
					MPGlobals.g_BigMessage[0].iMessageState = BIG_MESSAGE_STATE_CLEANUP
				ENDIF
			ELSE
				// We only delay specific big messages until it is safe to display again.
				IF DELAY_BIG_MESSAGE(MPGlobals.g_BigMessage[0])
					PRINTLN("[JA@MESSAGE] MAINTAIN_BIG_MESSAGE - not safe to display, so delaying message, change to BIG_MESSAGE_STATE_DELAY: ", GET_BIG_MESSAGE_DEBUG_TEXT(MPGlobals.g_BigMessage[0].messageType))

					MPGlobals.g_BigMessage[0].iMessageState = BIG_MESSAGE_STATE_DELAY
				ELSE
					PRINTLN("[JA@MESSAGE] MAINTAIN_BIG_MESSAGE - not delaying message, change to BIG_MESSAGE_STATE_CLEANUP: ", GET_BIG_MESSAGE_DEBUG_TEXT(MPGlobals.g_BigMessage[0].messageType))
				
					MPGlobals.g_BigMessage[0].iMessageState = BIG_MESSAGE_STATE_CLEANUP
				ENDIF
			ENDIF
			
			IF IS_BIG_MESSAGE_TYPE_FOR_FM_EVENTS_END(MPGlobals.g_BigMessage[0].messageType)
				IF MPGlobals.g_BigMessage[0].iMessageState != BIG_MESSAGE_STATE_CLEANUP
					SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH_CHANGE)					
				ENDIF
			ENDIF
		BREAK
		
		CASE BIG_MESSAGE_STATE_DELAY
			// Wait until we can display them again before processing normal logic
			IF IS_SAFE_TO_DISPLAY_BIG_MESSAGE()
				PRINTLN("[JA@MESSAGE] MAINTAIN_BIG_MESSAGE - Safe to resume delayed message, change to BIG_MESSAGE_STATE_ACTIVE: ", GET_BIG_MESSAGE_DEBUG_TEXT(MPGlobals.g_BigMessage[0].messageType))
				IF DELAY_BIG_MESSAGE(MPGlobals.g_BigMessage[0])
					MPGlobals.g_BigMessage[0].iMessageEndTime = GET_TIME_OFFSET(GET_NETWORK_TIME(), GET_TIME_FOR_THIS_BIG_MESSAGE(MPGlobals.g_BigMessage[0]))
				ELSE
					MPGlobals.g_BigMessage[0].iMessageEndTime = GET_TIME_OFFSET(GET_NETWORK_TIME(), MPGlobals.g_BigMessage[0].iDelayedTime)
				ENDIF
				MPGlobals.g_BigMessage[0].iMessageState = BIG_MESSAGE_STATE_ACTIVE
			ENDIF
		BREAK
		
		CASE BIG_MESSAGE_STATE_CLEANUP
			PRINTLN("[JA@MESSAGE] MAINTAIN_BIG_MESSAGE - cleaning up big message: ", GET_BIG_MESSAGE_DEBUG_TEXT(MPGlobals.g_BigMessage[0].messageType))

			
			IF IS_BITMASK_AS_ENUM_SET(MPGlobals.g_BigMessage[0].iBigMessageBitSet, BIG_MESSAGE_BIT_CLEANUP_ALL)
				bPerformFullLoopCheck = TRUE
			ENDIF
			
			BIG_MESSAGE newBigMessage
			newBigMessage = MPGlobals.g_BigMessage[0]
			
			CLEANUP_BIG_MESSAGE_FROM_INDEX(0)
			
			// If we were cleaned up for the wasted screen and are a big message type which requires to be shown after death, requeue it
			IF IS_BITMASK_AS_ENUM_SET(newBigMessage.iBigMessageBitSet, BIG_MESSAGE_BIT_CLEANED_UP_FOR_WASTED)
				IF SHOULD_BIG_MESSAGE_BE_SAVED_FOR_PLAYER_DEATH(newBigMessage)
					PRINTLN("[JA@MESSAGE] MAINTAIN_BIG_MESSAGE - message is being cleaned up for wasted. Resave: ", GET_BIG_MESSAGE_DEBUG_TEXT(newBigMessage.messageType))
					newBigMessage.iBigMessageBitSet = 0
					newBigMessage.iMessageState = BIG_MESSAGE_STATE_QUEUED
					SAVE_BIG_MESSAGE(newBigMessage)
				ENDIF
			ENDIF
		BREAK
	
	ENDSWITCH
	
	// If we need to do a full loop, check each big message for a cleanup
	IF bPerformFullLoopCheck
		INT i
		FOR i = 0 TO NUM_BIG_MESSAGE-1 STEP 1
			IF i >= 0
				IF MPGlobals.g_BigMessage[i].iMessageState = BIG_MESSAGE_STATE_CLEANUP
					CLEANUP_BIG_MESSAGE_FROM_INDEX(i)
					i--
					PRINTLN("[JA@MESSAGE] MAINTAIN_BIG_MESSAGE - Performing bPerformFullLoopCheck, CLEANUP_BIG_MESSAGE_FROM_INDEX(", i, ")")
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
	

	// DEBUG WIDGET
	#IF IS_DEBUG_BUILD

		IF bForceBigMessage
			bForceBigMessage = FALSE
			
			CLEAR_ALL_BIG_MESSAGES()
		
			SWITCH iDebugBigMessage
				CASE 0
					SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_WASTED)
				BREAK
				CASE 3
					SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_MISSION_PASSED)
				BREAK
				CASE 4
					SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_MISSION_FAILED)
				BREAK
				CASE 5
					SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_MISSION_OVER)
				BREAK
				CASE 6
					SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_MISSION_STOPPED)
				BREAK
				CASE 7
					SETUP_NEW_BIG_MESSAGE_WITH_INT_AND_STRING(BIG_MESSAGE_RANKUP, 11)
				BREAK
				CASE 11
					SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_REVENGE_KILL, "", "", HUD_COLOUR_FREEMODE)
				BREAK
				CASE 12
					SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_KILLS_SMALL)
				BREAK
				CASE 13
					SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_KILLS_MED)
				BREAK
				CASE 14
					SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_KILLS_LARGE)
				BREAK
				CASE 15
					SETUP_NEW_BIG_MESSAGE_WITH_INT_AND_STRING(BIG_MESSAGE_FASTEST_LAP, 60300, "BM_RB", DEFAULT, DEFAULT, 20000)
				BREAK
				CASE 16
					SETUP_NEW_BIG_MESSAGE_WITH_INT_AND_STRING(BIG_MESSAGE_FASTEST_LAP, 60300, "BM_PB")
				BREAK
				CASE 17
					SETUP_NEW_BIG_MESSAGE_WITH_INT_AND_STRING(BIG_MESSAGE_FASTEST_LAP, 60300, "BM_WR")
				BREAK
				CASE 18
					SETUP_NEW_BIG_MESSAGE_WITH_INT_AND_STRING(BIG_MESSAGE_FASTEST_TIME, 60300, "BM_RB")
				BREAK
				CASE 19
					SETUP_NEW_BIG_MESSAGE_WITH_INT_AND_STRING(BIG_MESSAGE_FASTEST_TIME, 60300, "BM_PB")
				BREAK
				CASE 20
					SETUP_NEW_BIG_MESSAGE_WITH_INT_AND_STRING(BIG_MESSAGE_FASTEST_TIME, 60300, "BM_WR")
				BREAK
				CASE 21
					SETUP_NEW_BIG_MESSAGE_WITH_PLAYER(BIG_MESSAGE_SURVIVAL_TM_DIED, PLAYER_ID())
				BREAK
				CASE 22
					SETUP_NEW_BIG_MESSAGE_WITH_INT_AND_STRING(BIG_MESSAGE_RACE_POS, GET_RANDOM_INT_IN_RANGE(1, 10))
				BREAK
				CASE 23
					ADD_MISSION_SUMMARY_TITLE("SUM_TITLE")
					
					ADD_MISSION_SUMMARY_STAT_WITH_INT("SUM_INT", 50)
					ADD_MISSION_SUMMARY_STAT_PLAYER_WITH_INT(PLAYER_ID(), 30)
					
					SETUP_NEW_BIG_MESSAGE_WITH_INT_AND_STRING(BIG_MESSAGE_MISSION_PASSED, 300, "SUM_STRAP")
				BREAK
				
				CASE 24
					SETUP_NEW_BIG_MESSAGE_WITH_INT_AND_STRING(BIG_MESSAGE_GENERIC_WINNER, 300, "SUM_STRAP")
				BREAK
				CASE 25
					SETUP_NEW_BIG_MESSAGE_WITH_INT_AND_STRING(BIG_MESSAGE_GENERIC_LOSER, 300, "SUM_STRAP")
				BREAK
				CASE 26
					ADD_MISSION_SUMMARY_TITLE("SUM_TITLE")
					
					ADD_MISSION_SUMMARY_STAT_WITH_INT("SUM_INT", 50)
					ADD_MISSION_SUMMARY_STAT_PLAYER_WITH_INT(PLAYER_ID(), 30)
					
					SETUP_NEW_BIG_MESSAGE_WITH_2_INTS(BIG_MESSAGE_WAVE_COMPLETE, GET_RANDOM_INT_IN_RANGE(0, 11), GET_RANDOM_INT_IN_RANGE(500, 1000), "NUMBER")
				BREAK
				CASE 27
					SETUP_NEW_BIG_MESSAGE_WITH_INT_AND_STRING(BIG_MESSAGE_LOCAL_FASTEST_LAP, 60300, "BM_RB")
				BREAK
				CASE 28
					SETUP_NEW_BIG_MESSAGE_WITH_INT_AND_STRING(BIG_MESSAGE_LOCAL_FASTEST_TIME, 60300, "BM_RB")
				BREAK
				CASE 29
					SETUP_NEW_BIG_MESSAGE_WITH_INT_AND_STRING(BIG_MESSAGE_TEAM_WON, 300, "NUMBER")
				BREAK
				CASE 30
					SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_TEAM_LOST)
				BREAK
				CASE 31
					SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_WRONG_WAY)
				BREAK
				CASE 32
					SETUP_NEW_BIG_MESSAGE_WITH_2_INTS(BIG_MESSAGE_LAP, 4, 7)
				BREAK
				CASE 33
					SETUP_NEW_BIG_MESSAGE_WITH_INT_AND_STRING(BIG_MESSAGE_NEW_HIGH_SCORE, 300, "NUMBER")
				BREAK
				CASE 34
					SETUP_NEW_BIG_MESSAGE_WITH_PLAYER(BIG_MESSAGE_POWER_PLAY, PLAYER_ID())
				BREAK
				CASE 36
					SETUP_NEW_BIG_MESSAGE_WITH_INT_AND_STRING(BIG_MESSAGE_UNLOCK, 0, "FM_RANK40_UNLOCK")
				BREAK
				CASE 37
					SETUP_NEW_BIG_MESSAGE_WITH_INT_AND_STRING(BIG_MESSAGE_DOUBLE_DAMAGE, 300, "NUMBER", "", HUD_COLOUR_FREEMODE)
				BREAK
				CASE 38
					SETUP_NEW_BIG_MESSAGE_WITH_INT_AND_STRING(BIG_MESSAGE_QUAD_DAMAGE, 300, "NUMBER")
				BREAK
				CASE 39
					SETUP_NEW_BIG_MESSAGE_WITH_INT_AND_STRING(BIG_MESSAGE_CURRENT_PERSONAL_BEST, 4004, "STRING")
				BREAK
				CASE 40
					SETUP_NEW_BIG_MESSAGE_WITH_INT_AND_STRING(BIG_MESSAGE_GENERIC_MIDSIZED, 23, "NUMBER", "HUD_INPUT77")
				BREAK	
				CASE 41
					SETUP_NEW_BIG_MESSAGE_WITH_INT_AND_STRING(BIG_MESSAGE_GENERIC_MIDSIZED_TITLE_INT, 23, "HUD_INPUT77", "NUMBER")
				BREAK
				CASE 42
					SETUP_NEW_BIG_MESSAGE_WITH_PLAYER(BIG_MESSAGE_ONE_ON_ONE_DM, PLAYER_ID(), -1, "STRING")
				BREAK
				CASE 43
//					SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_START_OF_JOB, "HUD_INPUT77", "HUD_INPUT77")
					SETUP_NEW_BIG_MESSAGE_WITH_3_PLAYERS(BIG_MESSAGE_GENERIC_TEXT, PLAYER_ID(), PLAYER_ID(), PLAYER_ID(), DEFAULT, "MULTIPLAY_VS3", "PIM_AAF2", HUD_COLOUR_PURPLE, 5000)
				BREAK
				CASE 44
					SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_IN_STRAPLINE(BIG_MESSAGE_MISSION_FAILED, "STRING", "LITERAL STR")
				BREAK
	
				CASE 45
					SETUP_NEW_BIG_MESSAGE_WITH_INT_AND_STRING(BIG_MESSAGE_CREWXP_RANKUP, 11)
				BREAK
				
				CASE 46
					SETUP_NEW_BIG_MESSAGE_WITH_INT_AND_STRING(BIG_MESSAGE_END_OF_JOB, iDebugEndPos, "", GET_RACE_POS_ORDINAL_STRING(iDebugEndPos, TRUE), HUD_COLOUR_FREEMODE)
					
					ADD_MISSION_SUMMARY_TITLE("SUM_TITLE")
					
					ADD_MISSION_SUMMARY_STAT_WITH_INT("SUM_INT", 50)
					ADD_MISSION_SUMMARY_STAT_PLAYER_WITH_INT(PLAYER_ID(), 30)
					ADD_MISSION_THUMB_VOTES(ESMGT_RACE)
				BREAK
				
				CASE 47
					SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_PLAYLIST_WON)
				BREAK
				
				CASE 48
					SETUP_NEW_BIG_MESSAGE_WITH_PLAYER(BIG_MESSAGE_PLAYLIST_LOST, PLAYER_ID())
				BREAK
				CASE 49
					SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_LEAVING_SURVIVAL)
				BREAK
				CASE 50								
					SETUP_NEW_BIG_MESSAGE_WITH_TWO_STRINGS_TWO_INTS(BIG_MESSAGE_GENERIC_TEXT, "OVT_RM_PLA_TC", "OVT_RM_SCR", "OVT_ORA", "OVT_PRP", DEFAULT, 10, 5, HUD_COLOUR_BLACK, DEFAULT, BIG_MESSAGE_BIT_APPLY_DARKER_BG)
				BREAK

/*
                        DELIVERED
                         Showroom
---------------------------------------------------
Vehicles Delivered                                   4/4
---------------------------------------------------
Market Value Loss                                  25%
---------------------------------------------------
Commission                                    $120,000
Commission Loss                            -$50,000
Gamertag A loss                             -$10,000
Gamertag B loss                             -$10,000
Gamertag C loss                             -$10,000
Gamertag D loss                             -$20,000
---------------------------------------------------
               Payment              $70,000
*/

				CASE 51
				
					SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_BUY_END_OF_JOB_SUCCESS, "VEX_VEH_HUDc" ) //  BIG_MESSAGE_GB_END_OF_JOB_WITH_SUMMARY
					ADD_MISSION_SUMMARY_TITLE("BIGM_SHOW")
					
				//	ADD_MISSION_SUMMARY_STAT_STRING_WITH_TWO_INTS("BIGM_VDEL", 5, 5) 	// Vehicles Delivered
					ADD_MISSION_SUMMARY_STAT_STRING_AND_FRACTION("BIGM_VDEL", 5, 5)
					ADD_MISSION_SUMMARY_STAT_STRING_AND_PERCENTAGE("BIGM_MVL", 25)		// Market Value Loss
					ADD_MISSION_SUMMARY_STAT_STRING_AND_CASH("BIGM_COM", 120000)		// Commision
					ADD_MISSION_SUMMARY_STAT_STRING_AND_CASH("BIGM_COML", -50000)		// Commision loss
					ADD_MISSION_SUMMARY_STAT_PLAYER_WITH_CASH(PLAYER_ID(), -12500)		// Player 0 Loss
					ADD_MISSION_SUMMARY_STAT_STRING_AND_CASH("BIGM_COLLB", 15000)		// Collection Bonus
					ADD_MISSION_SUMMARY_END_COMPLETION_CASH("BIGM_PAYM", 70000)			// Payment
					
				//	ADD_MISSION_SUMMARY_END_COMPLETION_XP("BIGM_PAYM", 70000)
				//	ADD_MISSION_SUMMARY_STAT_STRING_AND_CASH_OF_TYPE("BIGM_PAYM", 70000, ESEF_DOLLAR_VALUE)
				//	ADD_MISSION_SUMMARY_STAT_STRING_AND_CASH(PLAYER_ID(), 30)
				BREAK
								
			ENDSWITCH
		ENDIF
	#ENDIF

ENDPROC

#IF IS_DEBUG_BUILD
//-- DaveW - Get territory takeover to compile
	
ENUM TERRITORY_MESSAGE_STATE
	TERRITORY_WON,
	TERRITORY_LOST,
	TERRITORY_RETAINED,
	TERRITORY_NONE
ENDENUM

/// PURPOSE: Called by any script to force big message detailing terriroty takeover
FUNC BOOL SETUP_NEW_BIG_MESSAGE_FOR_TERRITORY_TAKEOVER(TERRITORY_MESSAGE_STATE territoryState, STRING sTerritory, INT iTeamOwnsTerr)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("[JA@MESSAGE] SETUP_NEW_BIG_MESSAGE_FOR_TERRITORY_TAKEOVER - initialising a new territory takeover big message") NET_NL()
	#ENDIF
	
	BIG_MESSAGE newBigMessage
	INIT_BIG_MESSAGE(BIG_MESSAGE_TERRITORY, newBigMessage, iTeamOwnsTerr, sTerritory)
	
	newBigMessage.iTerritoryState = ENUM_TO_INT(territoryState)
	
	RETURN SAVE_BIG_MESSAGE(newBigMessage)
ENDFUNC

#ENDIF







