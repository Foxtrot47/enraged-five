//╔═════════════════════════════════════════════════════════════════════════════╗
//║				Thumbs.sch, Speirs 03/10/2012									║
//╚═════════════════════════════════════════════════════════════════════════════╝

USING "hud_drawing.sch"
USING "mp_scaleform_functions.sch"
USING "Transition_Crews.sch"

FUNC BOOL HAS_THUMBS_DOWN_BUTTON_BEEN_PRESSED()
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)	
	
		PRINTLN("------------------------------------------------------------------------------")
		PRINTLN("--                                                                          --")
		PRINTLN("--                   THUMBS_DOWN , HAS_THUMBS_DOWN_BUTTON_BEEN_PRESSED		 --")
		PRINTLN("--                                                                          --")
		PRINTLN("------------------------------------------------------------------------------")
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_THUMBS_UP_BUTTON_BEEN_PRESSED()

	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)	
	
		PRINTLN("------------------------------------------------------------------------------")
		PRINTLN("--                                                                          --")
		PRINTLN("--                   THUMBS_UP , HAS_THUMBS_UP_BUTTON_BEEN_PRESSED    		 --")
		PRINTLN("--                                                                          --")
		PRINTLN("------------------------------------------------------------------------------")
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC CLEANUP_THUMBS()
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("THUMB_VOTE")
		CLEAR_HELP(TRUE)
	ENDIF
ENDPROC



