
//////////////////////////////////////////////////////////////////////////////////////////
//                                                                                      //
//      SCRIPT NAME     :   AM_Common_UI.sch                                			//
//      AUTHOR          :   Brenda.Carey@RockstarNorth.com                              //
//      DESCRIPTION     :   Wrappers for event bottom right hud elements                //
//                                                                                      //
//////////////////////////////////////////////////////////////////////////////////////////

USING "shared_hud_displays.sch"
USING "freemode_events_header.sch"



ENUM DISPLAY_EVENT_UI_TYPE
	DISPLAY_EVENT_NONE = 0,
	
	DISPLAY_EVENT_1ST_2ND_3RD_CHECKPOINTS_LOCAL_TIMER,
	
	
	DISPLAY_EVENT_MAX
ENDENUM

ENUM FMEVENT_SCORETITLE

	FMEVENT_SCORETITLE_GAMERTAG = 0,
	FMEVENT_SCORETITLE_YOUR_SCORE,
	FMEVENT_SCORETITLE_YOUR_BEST,
	FMEVENT_SCORETITLE_NONE
ENDENUM

ENUM DISPLAY_EVENT_PLACEMENT
	DISPLAY_EVENT_PLACEMENT_NAMEONLY,
	
	DISPLAY_EVENT_PLACEMENT_FIRST,
	DISPLAY_EVENT_PLACEMENT_SECOND,
	DISPLAY_EVENT_PLACEMENT_THIRD,
	DISPLAY_EVENT_PLACEMENT_FOURTH,
	DISPLAY_EVENT_PLACEMENT_FIFTH,
	
	DISPLAY_EVENT_PLACEMENT_MAX
ENDENUM

FUNC TEXT_LABEL_63 PARSE_PLACEMENT_GAMERTAGS(DISPLAY_EVENT_PLACEMENT Place, STRING Gamertag)


	CONST_INT NUM_GAMERTAG_CHARACTERS 8

	TEXT_LABEL_63 ResultString
	
	
	SWITCH Place
		
		CASE DISPLAY_EVENT_PLACEMENT_NAMEONLY
			ResultString = Gamertag
			RETURN ResultString
		BREAK
		
		CASE DISPLAY_EVENT_PLACEMENT_FIRST
		
			ResultString = GET_FILENAME_FOR_AUDIO_CONVERSATION("HUD_POSFIRST")

		BREAK
		
		CASE DISPLAY_EVENT_PLACEMENT_SECOND
		
			ResultString = GET_FILENAME_FOR_AUDIO_CONVERSATION("HUD_POSSECOND")

		BREAK
		
		CASE DISPLAY_EVENT_PLACEMENT_THIRD
		
			ResultString = GET_FILENAME_FOR_AUDIO_CONVERSATION("HUD_POSTHIRD")

		BREAK
		
		CASE DISPLAY_EVENT_PLACEMENT_FOURTH
		
			ResultString = GET_FILENAME_FOR_AUDIO_CONVERSATION("HUD_POSFOURTH")

		BREAK
		
		CASE DISPLAY_EVENT_PLACEMENT_FIFTH
		
			ResultString = GET_FILENAME_FOR_AUDIO_CONVERSATION("HUD_POSFIFTH")

		BREAK
	
	ENDSWITCH
	
	ResultString += " "

//	IF GET_LENGTH_OF_LITERAL_STRING(Gamertag) > NUM_GAMERTAG_CHARACTERS
		ResultString += Gamertag
//	ELSE
//		ResultString += GET_FIRST_N_CHARACTERS_OF_LITERAL_STRING(Gamertag, NUM_GAMERTAG_CHARACTERS)
//		ResultString += GET_FILENAME_FOR_AUDIO_CONVERSATION("HUD_ELLIPSIS")
//	ENDIF
		
	RETURN ResultString

ENDFUNC





FUNC TEXT_LABEL_63 PARSE_PLACEMENT_VEHICLE_NAME(STRING VehicleName)


	TEXT_LABEL_63 ResultString
	
	ResultString = VehicleName
	ResultString += " "
	ResultString += GET_FILENAME_FOR_AUDIO_CONVERSATION("HUD_TEAM")
		
	RETURN ResultString

ENDFUNC


FUNC TEXT_LABEL_63 PARSE_PLACEMENT_VEHICLE_NAME_AND_NUMBER(STRING VehicleName, INT Number)


	TEXT_LABEL_63 ResultString
	
	ResultString = VehicleName
	ResultString += " "
	ResultString += GET_FILENAME_FOR_AUDIO_CONVERSATION("HUD_TEAM")
	ResultString += " "
	ResultString += Number
		
	RETURN ResultString

ENDFUNC

FUNC TEXT_LABEL_63 PARSE_PLACEMENT_TEAM_AND_NUMBER(INT Number)


	TEXT_LABEL_63 ResultString
	
	ResultString += GET_FILENAME_FOR_AUDIO_CONVERSATION("HUD_TEAM")
	ResultString += " "
	ResultString += Number
		
	RETURN ResultString

ENDFUNC


PROC HIDE_BOTTOM_RIGHT_CODE_UI()
	
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_DISTRICT_NAME)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_STREET_NAME)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_VEHICLE_NAME)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME)
	g_b_Private_FMeventsHidingCodeUIThisFrame = TRUE

ENDPROC

FUNC BOOL SHOULD_DISPLAY_BOTTOM_RIGHT_UI_EVENTS(BOOL bIsSpectating = FALSE)
	
	#IF IS_DEBUG_BUILD
	BOOL bprints = FALSE
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_displayEventHudDisable")
		bprints = TRUE
	ENDIF
	IF GET_FRAME_COUNT() % 400 = 0
		bprints = TRUE
	ENDIF
	#ENDIF
	
	IF IS_SPECTATOR_SHOWING_NEWS_HUD()
		#IF IS_DEBUG_BUILD
			IF bprints 
				PRINTLN("[BCEVENTTIMERS] SHOULD_DISPLAY_BOTTOM_RIGHT_UI_EVENTS = FALSE, IS_SPECTATOR_SHOWING_NEWS_HUD = TRUE ")
			ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF G_bTurnOnInGameWidgets
			RETURN TRUE
		ENDIF
	#ENDIF
	
	IF IS_PLAYER_IN_CORONA()
		#IF IS_DEBUG_BUILD
			IF bprints 
				PRINTLN("[BCEVENTTIMERS] SHOULD_DISPLAY_BOTTOM_RIGHT_UI_EVENTS = FALSE, IS_PLAYER_IN_CORONA = TRUE ")
			ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF NOT bIsSpectating
		IF IS_NET_PLAYER_OK(PLAYER_ID()) = FALSE
			#IF IS_DEBUG_BUILD
				IF bprints 
					PRINTLN("[BCEVENTTIMERS] SHOULD_DISPLAY_BOTTOM_RIGHT_UI_EVENTS = FALSE, IS_NET_PLAYER_OK = FALSE ")
				ENDIF
			#ENDIF
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC


PROC BOTTOM_RIGHT_UI_1STINT_2NDINT_3RDINT_CHECKPT_LOCALINT_TIMER(PLAYER_INDEX First_Player_Index, 
																PLAYER_INDEX Second_Player_Index, 
																PLAYER_INDEX Third_Player_Index, 
																INT First_Player_Score, 
																INT Second_Player_Score, 
																INT Third_Player_Score, 
																INT Current_Checkpoint_Display, 
																INT Max_Checkpoint_Display, 
																INT iLocal_Score_Display, 
																INT Event_Timer_Display,			
																BOOL& bForceRebuildNames, 
																HUD_COLOURS EventTimerColour = HUD_COLOUR_WHITE, 
																STRING ModeName = NULL,
																HUD_COLOURS PlayerOneFleckColour = HUD_COLOUR_PURE_WHITE,
																HUD_COLOURS PlayerTwoFleckColour = HUD_COLOUR_PURE_WHITE,
																HUD_COLOURS PlayerThreeFleckColour = HUD_COLOUR_PURE_WHITE)
						
	IF SHOULD_DISPLAY_BOTTOM_RIGHT_UI_EVENTS() = FALSE
		EXIT
	ENDIF
	
																
	HIDE_BOTTOM_RIGHT_CODE_UI()
	
	#IF IS_DEBUG_BUILD
		IF g_DisplayTimersDisplaying
			DEBUG_PRINTCALLSTACK()
			PRINTLN("[BCEVENTTIMERS] BOTTOM_RIGHT_UI_1STINT_2NDINT_3RDINT_CHECKPT_LOCALINT_TIMER - called ")
		ENDIF
	#ENDIF	
	
	STRING PlayerName								
	BOOL RebuildStrings = FALSE
	
	IF EventHudPlayerIndexLastFrame[0] != First_Player_Index
	OR EventHudPlayerIndexLastFrame[1] != Second_Player_Index
	OR EventHudPlayerIndexLastFrame[2] != Third_Player_Index
	OR bForceRebuildNames
		RebuildStrings = TRUE
	ENDIF
	
	EventHudPlayerIndexLastFrame[0] = First_Player_Index
	EventHudPlayerIndexLastFrame[1] = Second_Player_Index
	EventHudPlayerIndexLastFrame[2] = Third_Player_Index
	EventHudPlayerIndexLastFrame[3] = NULL
	EventHudPlayerIndexLastFrame[4] = NULL


		
	IF EventHudPlayerIndexLastFrame[0] != INVALID_PLAYER_INDEX()	
		IF RebuildStrings = TRUE
		
			PlayerName = GET_PLAYER_NAME(EventHudPlayerIndexLastFrame[0])
			EventHudPlayerNameLastFrame[0] = PARSE_PLACEMENT_GAMERTAGS(DISPLAY_EVENT_PLACEMENT_FIRST, PlayerName)
		ENDIF
		IF First_Player_Score > 0
			DRAW_GENERIC_SCORE(First_Player_Score, EventHudPlayerNameLastFrame[0], DEFAULT, HUD_COLOUR_GOLD, HUDORDER_SIXTHBOTTOM, TRUE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUD_COLOUR_GOLD,
								DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, PlayerOneFleckColour)		
		ENDIF
	ENDIF	
	
	
	IF EventHudPlayerIndexLastFrame[1] != INVALID_PLAYER_INDEX()
		IF RebuildStrings = TRUE
			PlayerName = GET_PLAYER_NAME(EventHudPlayerIndexLastFrame[1])
			EventHudPlayerNameLastFrame[1] = PARSE_PLACEMENT_GAMERTAGS(DISPLAY_EVENT_PLACEMENT_SECOND, PlayerName)
		ENDIF
		IF Second_Player_Score > 0
			DRAW_GENERIC_SCORE(Second_Player_Score, EventHudPlayerNameLastFrame[1], DEFAULT, HUD_COLOUR_SILVER, HUDORDER_FIFTHBOTTOM, TRUE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUD_COLOUR_SILVER,
								DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, PlayerTwoFleckColour)		
		ENDIF
	ENDIF


	IF EventHudPlayerIndexLastFrame[2] != INVALID_PLAYER_INDEX()
		IF RebuildStrings = TRUE
			PlayerName = GET_PLAYER_NAME(EventHudPlayerIndexLastFrame[2])
			EventHudPlayerNameLastFrame[2] = PARSE_PLACEMENT_GAMERTAGS(DISPLAY_EVENT_PLACEMENT_THIRD, PlayerName)
		ENDIF
		IF Third_Player_Score > 0
			DRAW_GENERIC_SCORE(Third_Player_Score, EventHudPlayerNameLastFrame[2], DEFAULT, HUD_COLOUR_BRONZE, HUDORDER_FOURTHBOTTOM, TRUE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUD_COLOUR_BRONZE,
								DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, PlayerThreeFleckColour)		
		ENDIF
	ENDIF
		
		
		

	DRAW_GENERIC_BIG_DOUBLE_NUMBER(Current_Checkpoint_Display, Max_Checkpoint_Display, "HUD_CHECKPOINTS", DEFAULT, DEFAULT, HUDORDER_THIRDBOTTOM, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
	
	
	IF FM_EVENT_IS_PLAYER_RESTRICTED_WITH_PASSIVE(PLAYER_ID()) = FALSE
		DRAW_GENERIC_SCORE(iLocal_Score_Display, "HUD_USCORE", DEFAULT, DEFAULT, HUDORDER_SECONDBOTTOM)
	ENDIF
	
	STRING LocalModelName = "HUD_COUNTDOWN"
	IF NOT IS_STRING_EMPTY_HUD(ModeName)
		LocalModelName = ModeName
	ENDIF
	DRAW_GENERIC_TIMER(Event_Timer_Display, LocalModelName, DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUDORDER_BOTTOM, DEFAULT, EventTimerColour, DEFAULT, DEFAULT, DEFAULT, EventTimerColour)
	
	bForceRebuildNames = FALSE
	
	SET_FAR_RIGHT_TITLE_POSITION_HUD_THIS_FRAME()
	
ENDPROC








PROC BOTTOM_RIGHT_UI_1STINT_2NDINT_3RDINT_ATTEMPT_LOCALINT_TIMER(PLAYER_INDEX First_Player_Index, 
																PLAYER_INDEX Second_Player_Index, 
																PLAYER_INDEX Third_Player_Index, 
																INT First_Player_Score, 
																INT Second_Player_Score, 
																INT Third_Player_Score, 
																INT Attempt_Display, 
																INT iLocal_Score_Display, 
																INT Event_Timer_Display,
																STRING UnitString,
																BOOL& bForceRebuildNames,
																HUD_COLOURS EventTimerColour = HUD_COLOUR_WHITE, 
																BOOL bDisplayAttempt = TRUE, 
																INT FlashBackgroundtimerMS = 0,
																STRING ModeName = NULL,
																FMEVENT_SCORETITLE LocalDisplayType = FMEVENT_SCORETITLE_GAMERTAG,
																HUD_COLOURS PlayerOneFleckColour = HUD_COLOUR_PURE_WHITE,
																HUD_COLOURS PlayerTwoFleckColour = HUD_COLOUR_PURE_WHITE,
																HUD_COLOURS PlayerThreeFleckColour = HUD_COLOUR_PURE_WHITE)
						
	IF SHOULD_DISPLAY_BOTTOM_RIGHT_UI_EVENTS() = FALSE
		EXIT
	ENDIF															
	HIDE_BOTTOM_RIGHT_CODE_UI()
	
	#IF IS_DEBUG_BUILD
		IF g_DisplayTimersDisplaying
			DEBUG_PRINTCALLSTACK()
			PRINTLN("[BCEVENTTIMERS] BOTTOM_RIGHT_UI_1STINT_2NDINT_3RDINT_ATTEMPT_LOCALINT_TIMER - called ")
		ENDIF
	#ENDIF	
										
	STRING PlayerName								
	BOOL RebuildStrings = FALSE
	
	IF EventHudPlayerIndexLastFrame[0] != First_Player_Index
	OR EventHudPlayerIndexLastFrame[1] != Second_Player_Index
	OR EventHudPlayerIndexLastFrame[2] != Third_Player_Index
	OR bForceRebuildNames
		RebuildStrings = TRUE
	ENDIF
	
	EventHudPlayerIndexLastFrame[0] = First_Player_Index
	EventHudPlayerIndexLastFrame[1] = Second_Player_Index
	EventHudPlayerIndexLastFrame[2] = Third_Player_Index
	EventHudPlayerIndexLastFrame[3] = NULL
	EventHudPlayerIndexLastFrame[4] = NULL


		
	IF EventHudPlayerIndexLastFrame[0] != INVALID_PLAYER_INDEX()	
		IF RebuildStrings = TRUE
		
			PlayerName = GET_PLAYER_NAME(EventHudPlayerIndexLastFrame[0])
			EventHudPlayerNameLastFrame[0] = PARSE_PLACEMENT_GAMERTAGS(DISPLAY_EVENT_PLACEMENT_FIRST, PlayerName)
		ENDIF
		IF First_Player_Score > 0
			DRAW_GENERIC_SCORE(First_Player_Score, EventHudPlayerNameLastFrame[0], DEFAULT, HUD_COLOUR_GOLD, HUDORDER_SIXTHBOTTOM, TRUE, UnitString, DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUD_COLOUR_GOLD,
								DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, PlayerOneFleckColour)		
		ENDIF
	ENDIF	
	
	
	IF EventHudPlayerIndexLastFrame[1] != INVALID_PLAYER_INDEX()
		IF RebuildStrings = TRUE
			PlayerName = GET_PLAYER_NAME(EventHudPlayerIndexLastFrame[1])
			EventHudPlayerNameLastFrame[1] = PARSE_PLACEMENT_GAMERTAGS(DISPLAY_EVENT_PLACEMENT_SECOND, PlayerName)
		ENDIF
		IF Second_Player_Score > 0
			DRAW_GENERIC_SCORE(Second_Player_Score, EventHudPlayerNameLastFrame[1], DEFAULT, HUD_COLOUR_SILVER, HUDORDER_FIFTHBOTTOM, TRUE, UnitString, DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUD_COLOUR_SILVER,
							DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, PlayerTwoFleckColour)		
		ENDIF
	ENDIF


	IF EventHudPlayerIndexLastFrame[2] != INVALID_PLAYER_INDEX()
		IF RebuildStrings = TRUE
			PlayerName = GET_PLAYER_NAME(EventHudPlayerIndexLastFrame[2])
			EventHudPlayerNameLastFrame[2] = PARSE_PLACEMENT_GAMERTAGS(DISPLAY_EVENT_PLACEMENT_THIRD, PlayerName)
		ENDIF
		IF Third_Player_Score > 0
			DRAW_GENERIC_SCORE(Third_Player_Score, EventHudPlayerNameLastFrame[2], DEFAULT, HUD_COLOUR_BRONZE, HUDORDER_FOURTHBOTTOM, TRUE, UnitString, DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUD_COLOUR_BRONZE,
							DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, PlayerThreeFleckColour)		
		ENDIF
	ENDIF
		
	IF bDisplayAttempt
		DRAW_GENERIC_SCORE(Attempt_Display, "HUD_ATTEMPTS", DEFAULT, DEFAULT, HUDORDER_THIRDBOTTOM, FALSE, UnitString, DEFAULT, DEFAULT, HUDFLASHING_FLASHWHITE,FlashBackgroundtimerMS )
	ENDIF
	
	SWITCH LocalDisplayType 
	
		CASE FMEVENT_SCORETITLE_GAMERTAG
			IF PLAYER_ID() != INVALID_PLAYER_INDEX()
				IF FM_EVENT_IS_PLAYER_RESTRICTED_WITH_PASSIVE(PLAYER_ID()) = FALSE
					DRAW_GENERIC_SCORE(iLocal_Score_Display, GET_PLAYER_NAME(PLAYER_ID()), DEFAULT, DEFAULT, HUDORDER_SECONDBOTTOM, TRUE, UnitString)
				ENDIF
			ENDIF
		BREAK
		
		CASE FMEVENT_SCORETITLE_YOUR_SCORE
			IF FM_EVENT_IS_PLAYER_RESTRICTED_WITH_PASSIVE(PLAYER_ID()) = FALSE
				DRAW_GENERIC_SCORE(iLocal_Score_Display, "HUD_USCORE", DEFAULT, DEFAULT, HUDORDER_SECONDBOTTOM, FALSE, UnitString)
			ENDIF
		BREAK
		
		CASE FMEVENT_SCORETITLE_YOUR_BEST
			IF FM_EVENT_IS_PLAYER_RESTRICTED_WITH_PASSIVE(PLAYER_ID()) = FALSE
				DRAW_GENERIC_SCORE(iLocal_Score_Display, "HUD_UBEST", DEFAULT, DEFAULT, HUDORDER_SECONDBOTTOM, FALSE, UnitString)
			ENDIF
		BREAK
		CASE FMEVENT_SCORETITLE_NONE
		//nothing
		BREAK
	ENDSWITCH
	
	
	
	STRING LocalModelName = "HUD_COUNTDOWN"
	IF NOT IS_STRING_EMPTY_HUD(ModeName)
		LocalModelName = ModeName
	ENDIF
	DRAW_GENERIC_TIMER(Event_Timer_Display, LocalModelName, DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUDORDER_BOTTOM, DEFAULT, EventTimerColour, DEFAULT, DEFAULT, DEFAULT, EventTimerColour)
		
	bForceRebuildNames = FALSE
	
	SET_FAR_RIGHT_TITLE_POSITION_HUD_THIS_FRAME()
	
ENDPROC




PROC BOTTOM_RIGHT_UI_1STFLOAT_2NDFLOAT_3RDFLOAT_ATTEMPT_LOCALFLOAT_TIMER(PLAYER_INDEX First_Player_Index, 
																PLAYER_INDEX Second_Player_Index, 
																PLAYER_INDEX Third_Player_Index, 
																FLOAT First_Player_Score, 
																FLOAT Second_Player_Score, 
																FLOAT Third_Player_Score, 
																FLOAT Attempt_Display, 
																FLOAT iLocal_Score_Display, 
																INT Event_Timer_Display,
																STRING UnitString,
																BOOL& bForceRebuildNames,
																HUD_COLOURS EventTimerColour = HUD_COLOUR_WHITE,
																BOOL bDisplayAttempt = TRUE, 
																INT FlashBackgroundtimerMS = 0,
																STRING ModeName = NULL, 
																FMEVENT_SCORETITLE LocalDisplayType = FMEVENT_SCORETITLE_GAMERTAG,
																HUD_COLOURS PlayerOneFleckColour = HUD_COLOUR_PURE_WHITE,
																HUD_COLOURS PlayerTwoFleckColour = HUD_COLOUR_PURE_WHITE,
																HUD_COLOURS PlayerThreeFleckColour = HUD_COLOUR_PURE_WHITE)
						
	IF SHOULD_DISPLAY_BOTTOM_RIGHT_UI_EVENTS() = FALSE
		EXIT
	ENDIF															
	HIDE_BOTTOM_RIGHT_CODE_UI()
	
	#IF IS_DEBUG_BUILD
		IF g_DisplayTimersDisplaying
			DEBUG_PRINTCALLSTACK()
			PRINTLN("[BCEVENTTIMERS] BOTTOM_RIGHT_UI_1STFLOAT_2NDFLOAT_3RDFLOAT_ATTEMPT_LOCALFLOAT_TIMER - called ")
		ENDIF
	#ENDIF	
										
	STRING PlayerName								
	BOOL RebuildStrings = FALSE

	
	IF EventHudPlayerIndexLastFrame[0] != First_Player_Index
	OR EventHudPlayerIndexLastFrame[1] != Second_Player_Index
	OR EventHudPlayerIndexLastFrame[2] != Third_Player_Index
	OR bForceRebuildNames
		RebuildStrings = TRUE
	ENDIF
	
	EventHudPlayerIndexLastFrame[0] = First_Player_Index
	EventHudPlayerIndexLastFrame[1] = Second_Player_Index
	EventHudPlayerIndexLastFrame[2] = Third_Player_Index
	EventHudPlayerIndexLastFrame[3] = NULL
	EventHudPlayerIndexLastFrame[4] = NULL


		
	IF EventHudPlayerIndexLastFrame[0] != INVALID_PLAYER_INDEX()	
		IF RebuildStrings = TRUE
		
			PlayerName = GET_PLAYER_NAME(EventHudPlayerIndexLastFrame[0])
			EventHudPlayerNameLastFrame[0] = PARSE_PLACEMENT_GAMERTAGS(DISPLAY_EVENT_PLACEMENT_FIRST, PlayerName)
		ENDIF
		IF First_Player_Score > 0
			DRAW_GENERIC_SCORE(-1, EventHudPlayerNameLastFrame[0], DEFAULT, HUD_COLOUR_GOLD, HUDORDER_SIXTHBOTTOM, TRUE, UnitString, TRUE, First_Player_Score, DEFAULT, DEFAULT, HUD_COLOUR_GOLD,
								DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, PlayerOneFleckColour)		
		ENDIF
	ENDIF	
	
	
	IF EventHudPlayerIndexLastFrame[1] != INVALID_PLAYER_INDEX()
		IF RebuildStrings = TRUE
			PlayerName = GET_PLAYER_NAME(EventHudPlayerIndexLastFrame[1])
			EventHudPlayerNameLastFrame[1] = PARSE_PLACEMENT_GAMERTAGS(DISPLAY_EVENT_PLACEMENT_SECOND, PlayerName)
		ENDIF
		IF Second_Player_Score > 0
			DRAW_GENERIC_SCORE(-1, EventHudPlayerNameLastFrame[1], DEFAULT, HUD_COLOUR_SILVER, HUDORDER_FIFTHBOTTOM, TRUE, UnitString, TRUE, Second_Player_Score, DEFAULT, DEFAULT, HUD_COLOUR_SILVER,
							DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, PlayerTwoFleckColour)		
		ENDIF
	ENDIF


	IF EventHudPlayerIndexLastFrame[2] != INVALID_PLAYER_INDEX()
		IF RebuildStrings = TRUE
			PlayerName = GET_PLAYER_NAME(EventHudPlayerIndexLastFrame[2])
			EventHudPlayerNameLastFrame[2] = PARSE_PLACEMENT_GAMERTAGS(DISPLAY_EVENT_PLACEMENT_THIRD, PlayerName)
		ENDIF
		IF Third_Player_Score > 0
			DRAW_GENERIC_SCORE(-1, EventHudPlayerNameLastFrame[2], DEFAULT, HUD_COLOUR_BRONZE, HUDORDER_FOURTHBOTTOM, TRUE, UnitString, TRUE, Third_Player_Score, DEFAULT, DEFAULT, HUD_COLOUR_BRONZE,
								DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, PlayerThreeFleckColour)		
		ENDIF
	ENDIF
		
	IF bDisplayAttempt
		DRAW_GENERIC_SCORE(-1, "HUD_ATTEMPTS", DEFAULT, DEFAULT, HUDORDER_THIRDBOTTOM, DEFAULT, UnitString, TRUE, Attempt_Display, HUDFLASHING_FLASHWHITE, FlashBackgroundtimerMS )
	ENDIF
	
	SWITCH LocalDisplayType 
	
		CASE FMEVENT_SCORETITLE_GAMERTAG
			IF PLAYER_ID() != INVALID_PLAYER_INDEX()
				IF FM_EVENT_IS_PLAYER_RESTRICTED_WITH_PASSIVE(PLAYER_ID()) = FALSE
					DRAW_GENERIC_SCORE(-1, GET_PLAYER_NAME(PLAYER_ID()), DEFAULT, DEFAULT, HUDORDER_SECONDBOTTOM, TRUE, UnitString, TRUE, iLocal_Score_Display)
				ENDIF
			ENDIF
		BREAK
		
		CASE FMEVENT_SCORETITLE_YOUR_SCORE
			IF FM_EVENT_IS_PLAYER_RESTRICTED_WITH_PASSIVE(PLAYER_ID()) = FALSE
				DRAW_GENERIC_SCORE(-1, "HUD_USCORE", DEFAULT, DEFAULT, HUDORDER_SECONDBOTTOM, FALSE, UnitString, TRUE, iLocal_Score_Display)
			ENDIF
		BREAK
		
		CASE FMEVENT_SCORETITLE_YOUR_BEST
			IF FM_EVENT_IS_PLAYER_RESTRICTED_WITH_PASSIVE(PLAYER_ID()) = FALSE
				DRAW_GENERIC_SCORE(-1, "HUD_UBEST", DEFAULT, DEFAULT, HUDORDER_SECONDBOTTOM, FALSE, UnitString, TRUE, iLocal_Score_Display)
			ENDIF
		BREAK
		
		CASE FMEVENT_SCORETITLE_NONE
		//nothing
		BREAK
	ENDSWITCH
	
	
	STRING LocalModelName = "HUD_COUNTDOWN"
	IF NOT IS_STRING_EMPTY_HUD(ModeName)
		LocalModelName = ModeName
	ENDIF
	IF Event_Timer_Display != -10
		DRAW_GENERIC_TIMER(Event_Timer_Display, LocalModelName, DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUDORDER_BOTTOM, DEFAULT, EventTimerColour, DEFAULT, DEFAULT, DEFAULT, EventTimerColour)
	ENDIF	
	bForceRebuildNames = FALSE
	
	SET_FAR_RIGHT_TITLE_POSITION_HUD_THIS_FRAME()
	
ENDPROC


PROC BOTTOM_RIGHT_UI_1STFLOAT_2NDFLOAT_3RDFLOAT_4THFLOAT_ATTEMPT_LOCALFLOAT_TIMER(PLAYER_INDEX First_Player_Index, 
																PLAYER_INDEX Second_Player_Index, 
																PLAYER_INDEX Third_Player_Index, 
																PLAYER_INDEX Forth_Player_Index, 
																FLOAT First_Player_Score, 
																FLOAT Second_Player_Score, 
																FLOAT Third_Player_Score, 
																FLOAT Forth_Player_Score, 
																FLOAT Attempt_Display, 
																FLOAT iLocal_Score_Display, 
																INT Event_Timer_Display,
																STRING UnitString,
																BOOL& bForceRebuildNames,
																HUD_COLOURS EventTimerColour = HUD_COLOUR_WHITE,
																BOOL bDisplayAttempt = TRUE, 
																INT FlashBackgroundtimerMS = 0,
																STRING ModeName = NULL, 
																FMEVENT_SCORETITLE LocalDisplayType = FMEVENT_SCORETITLE_GAMERTAG,
																HUD_COLOURS PlayerOneFleckColour = HUD_COLOUR_PURE_WHITE,
																HUD_COLOURS PlayerTwoFleckColour = HUD_COLOUR_PURE_WHITE,
																HUD_COLOURS PlayerThreeFleckColour = HUD_COLOUR_PURE_WHITE)
						
	IF SHOULD_DISPLAY_BOTTOM_RIGHT_UI_EVENTS() = FALSE
		EXIT
	ENDIF															
	HIDE_BOTTOM_RIGHT_CODE_UI()
	
	#IF IS_DEBUG_BUILD
		IF g_DisplayTimersDisplaying
			DEBUG_PRINTCALLSTACK()
			PRINTLN("[BCEVENTTIMERS] BOTTOM_RIGHT_UI_1STFLOAT_2NDFLOAT_3RDFLOAT_ATTEMPT_LOCALFLOAT_TIMER - called ")
		ENDIF
	#ENDIF	
										
	STRING PlayerName								
	BOOL RebuildStrings = FALSE

	
	IF EventHudPlayerIndexLastFrame[0] != First_Player_Index
	OR EventHudPlayerIndexLastFrame[1] != Second_Player_Index
	OR EventHudPlayerIndexLastFrame[2] != Third_Player_Index
	OR EventHudPlayerIndexLastFrame[4] != Forth_Player_Index
	OR bForceRebuildNames
		RebuildStrings = TRUE
	ENDIF
	
	EventHudPlayerIndexLastFrame[0] = First_Player_Index
	EventHudPlayerIndexLastFrame[1] = Second_Player_Index
	EventHudPlayerIndexLastFrame[2] = Third_Player_Index
	EventHudPlayerIndexLastFrame[3] = Forth_Player_Index
	EventHudPlayerIndexLastFrame[4] = NULL


		
	IF EventHudPlayerIndexLastFrame[0] != INVALID_PLAYER_INDEX()	
		IF RebuildStrings = TRUE
		
			PlayerName = GET_PLAYER_NAME(EventHudPlayerIndexLastFrame[0])
			EventHudPlayerNameLastFrame[0] = PARSE_PLACEMENT_GAMERTAGS(DISPLAY_EVENT_PLACEMENT_FIRST, PlayerName)
		ENDIF
		IF First_Player_Score > 0
			DRAW_GENERIC_SCORE(-1, EventHudPlayerNameLastFrame[0], DEFAULT, HUD_COLOUR_GOLD, HUDORDER_SEVENTHBOTTOM, TRUE, UnitString, TRUE, First_Player_Score, DEFAULT, DEFAULT, HUD_COLOUR_GOLD,
								DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, PlayerOneFleckColour)		
		ENDIF
	ENDIF	
	
	
	IF EventHudPlayerIndexLastFrame[1] != INVALID_PLAYER_INDEX()
		IF RebuildStrings = TRUE
			PlayerName = GET_PLAYER_NAME(EventHudPlayerIndexLastFrame[1])
			EventHudPlayerNameLastFrame[1] = PARSE_PLACEMENT_GAMERTAGS(DISPLAY_EVENT_PLACEMENT_SECOND, PlayerName)
		ENDIF
		IF Second_Player_Score > 0
			DRAW_GENERIC_SCORE(-1, EventHudPlayerNameLastFrame[1], DEFAULT, HUD_COLOUR_SILVER, HUDORDER_SIXTHBOTTOM, TRUE, UnitString, TRUE, Second_Player_Score, DEFAULT, DEFAULT, HUD_COLOUR_SILVER,
							DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, PlayerTwoFleckColour)		
		ENDIF
	ENDIF


	IF EventHudPlayerIndexLastFrame[2] != INVALID_PLAYER_INDEX()
		IF RebuildStrings = TRUE
			PlayerName = GET_PLAYER_NAME(EventHudPlayerIndexLastFrame[2])
			EventHudPlayerNameLastFrame[2] = PARSE_PLACEMENT_GAMERTAGS(DISPLAY_EVENT_PLACEMENT_THIRD, PlayerName)
		ENDIF
		IF Third_Player_Score > 0
			DRAW_GENERIC_SCORE(-1, EventHudPlayerNameLastFrame[2], DEFAULT, HUD_COLOUR_BRONZE, HUDORDER_FIFTHBOTTOM, TRUE, UnitString, TRUE, Third_Player_Score, DEFAULT, DEFAULT, HUD_COLOUR_BRONZE,
								DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, PlayerThreeFleckColour)		
		ENDIF
	ENDIF

	IF EventHudPlayerIndexLastFrame[3] != INVALID_PLAYER_INDEX()
		IF RebuildStrings = TRUE
			PlayerName = GET_PLAYER_NAME(EventHudPlayerIndexLastFrame[3])
			EventHudPlayerNameLastFrame[3] = PARSE_PLACEMENT_GAMERTAGS(DISPLAY_EVENT_PLACEMENT_FOURTH, PlayerName)
		ENDIF
		IF Forth_Player_Score > 0
			DRAW_GENERIC_SCORE(-1, EventHudPlayerNameLastFrame[3], DEFAULT, HUD_COLOUR_GREYLIGHT, HUDORDER_FOURTHBOTTOM, TRUE, UnitString, TRUE, Forth_Player_Score, DEFAULT, DEFAULT, HUD_COLOUR_GREYLIGHT,
								DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, PlayerThreeFleckColour)		
		ENDIF
	ENDIF
		
	IF bDisplayAttempt
		DRAW_GENERIC_SCORE(-1, "HUD_ATTEMPTS", DEFAULT, DEFAULT, HUDORDER_THIRDBOTTOM, DEFAULT, UnitString, TRUE, Attempt_Display, HUDFLASHING_FLASHWHITE, FlashBackgroundtimerMS )
	ENDIF
	
	SWITCH LocalDisplayType 
	
		CASE FMEVENT_SCORETITLE_GAMERTAG
			IF PLAYER_ID() != INVALID_PLAYER_INDEX()
				IF FM_EVENT_IS_PLAYER_RESTRICTED_WITH_PASSIVE(PLAYER_ID()) = FALSE
					DRAW_GENERIC_SCORE(-1, GET_PLAYER_NAME(PLAYER_ID()), DEFAULT, DEFAULT, HUDORDER_THIRDBOTTOM, TRUE, UnitString, TRUE, iLocal_Score_Display)
				ENDIF
			ENDIF
		BREAK
		
		CASE FMEVENT_SCORETITLE_YOUR_SCORE
			IF FM_EVENT_IS_PLAYER_RESTRICTED_WITH_PASSIVE(PLAYER_ID()) = FALSE
				DRAW_GENERIC_SCORE(-1, "HUD_USCORE", DEFAULT, DEFAULT, HUDORDER_THIRDBOTTOM, FALSE, UnitString, TRUE, iLocal_Score_Display)
			ENDIF
		BREAK
		
		CASE FMEVENT_SCORETITLE_YOUR_BEST
			IF FM_EVENT_IS_PLAYER_RESTRICTED_WITH_PASSIVE(PLAYER_ID()) = FALSE
				DRAW_GENERIC_SCORE(-1, "HUD_UBEST", DEFAULT, DEFAULT, HUDORDER_THIRDBOTTOM, FALSE, UnitString, TRUE, iLocal_Score_Display)
			ENDIF
		BREAK
		
		CASE FMEVENT_SCORETITLE_NONE
		//nothing
		BREAK
	ENDSWITCH
	
	
	STRING LocalModelName = "HUD_COUNTDOWN"
	IF NOT IS_STRING_EMPTY_HUD(ModeName)
		LocalModelName = ModeName
	ENDIF
	DRAW_GENERIC_TIMER(Event_Timer_Display, LocalModelName, DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUDORDER_BOTTOM, DEFAULT, EventTimerColour, DEFAULT, DEFAULT, DEFAULT, EventTimerColour)
			
	bForceRebuildNames = FALSE
	
	SET_FAR_RIGHT_TITLE_POSITION_HUD_THIS_FRAME()
	
ENDPROC


PROC BOTTOM_RIGHT_UI_1STTIME_2NDTIME_3RDTIME_ATTEMPT_LOCALTIME_TIMER(PLAYER_INDEX First_Player_Index, 
																PLAYER_INDEX Second_Player_Index, 
																PLAYER_INDEX Third_Player_Index, 
																INT First_Player_Score, 
																INT Second_Player_Score, 
																INT Third_Player_Score, 
																INT Attempt_Display, 
																INT iLocal_Score_Display, 
																INT Event_Timer_Display,
																BOOL& bForceRebuildNames,
																HUD_COLOURS EventTimerColour = HUD_COLOUR_WHITE,
																BOOL bDisplayAttempt = TRUE, 
																INT FlashBackgroundtimerMS = 0,
																STRING ModeName = NULL,
																FMEVENT_SCORETITLE LocalDisplayType = FMEVENT_SCORETITLE_YOUR_SCORE,
																HUD_COLOURS PlayerOneFleckColour = HUD_COLOUR_PURE_WHITE,
																HUD_COLOURS PlayerTwoFleckColour = HUD_COLOUR_PURE_WHITE,
																HUD_COLOURS PlayerThreeFleckColour = HUD_COLOUR_PURE_WHITE)
						
	IF SHOULD_DISPLAY_BOTTOM_RIGHT_UI_EVENTS() = FALSE
		EXIT
	ENDIF
	
	HIDE_BOTTOM_RIGHT_CODE_UI()
	
	#IF IS_DEBUG_BUILD
		IF g_DisplayTimersDisplaying
			DEBUG_PRINTCALLSTACK()
			PRINTLN("[BCEVENTTIMERS] BOTTOM_RIGHT_UI_1STTIME_2NDTIME_3RDTIME_ATTEMPT_LOCALTIME_TIMER - called ")
		ENDIF
	#ENDIF	
										
										
	STRING PlayerName								
	BOOL RebuildStrings = FALSE
	
	IF EventHudPlayerIndexLastFrame[0] != First_Player_Index
	OR EventHudPlayerIndexLastFrame[1] != Second_Player_Index
	OR EventHudPlayerIndexLastFrame[2] != Third_Player_Index
	OR bForceRebuildNames
		RebuildStrings = TRUE
	ENDIF
	
	EventHudPlayerIndexLastFrame[0] = First_Player_Index
	EventHudPlayerIndexLastFrame[1] = Second_Player_Index
	EventHudPlayerIndexLastFrame[2] = Third_Player_Index
	EventHudPlayerIndexLastFrame[3] = NULL
	EventHudPlayerIndexLastFrame[4] = NULL


		
	IF EventHudPlayerIndexLastFrame[0] != INVALID_PLAYER_INDEX()	
		IF RebuildStrings = TRUE
		
			PlayerName = GET_PLAYER_NAME(EventHudPlayerIndexLastFrame[0])
			EventHudPlayerNameLastFrame[0] = PARSE_PLACEMENT_GAMERTAGS(DISPLAY_EVENT_PLACEMENT_FIRST, PlayerName)
		ENDIF
		IF First_Player_Score > 0
			DRAW_GENERIC_TIMER(First_Player_Score, EventHudPlayerNameLastFrame[0], DEFAULT, TIMER_STYLE_DONTUSEMILLISECONDS, DEFAULT, DEFAULT, HUDORDER_SIXTHBOTTOM, TRUE, HUD_COLOUR_GOLD, DEFAULT, DEFAULT, DEFAULT, HUD_COLOUR_GOLD,
							DEFAULT, PlayerOneFleckColour)
		ENDIF
	ENDIF	
	
	
	IF EventHudPlayerIndexLastFrame[1] != INVALID_PLAYER_INDEX()
		IF RebuildStrings = TRUE
			PlayerName = GET_PLAYER_NAME(EventHudPlayerIndexLastFrame[1])
			EventHudPlayerNameLastFrame[1] = PARSE_PLACEMENT_GAMERTAGS(DISPLAY_EVENT_PLACEMENT_SECOND, PlayerName)
		ENDIF	
		IF Second_Player_Score > 0
			DRAW_GENERIC_TIMER(Second_Player_Score, EventHudPlayerNameLastFrame[1], DEFAULT, TIMER_STYLE_DONTUSEMILLISECONDS, DEFAULT, DEFAULT, HUDORDER_FIFTHBOTTOM, TRUE, HUD_COLOUR_SILVER, DEFAULT, DEFAULT, DEFAULT, HUD_COLOUR_SILVER,
							DEFAULT, PlayerTwoFleckColour)
		ENDIF
		
	ENDIF


	IF EventHudPlayerIndexLastFrame[2] != INVALID_PLAYER_INDEX()
		IF RebuildStrings = TRUE
			PlayerName = GET_PLAYER_NAME(EventHudPlayerIndexLastFrame[2])
			EventHudPlayerNameLastFrame[2] = PARSE_PLACEMENT_GAMERTAGS(DISPLAY_EVENT_PLACEMENT_THIRD, PlayerName)
		ENDIF
		IF Third_Player_Score > 0
			DRAW_GENERIC_TIMER(Third_Player_Score, EventHudPlayerNameLastFrame[2], DEFAULT, TIMER_STYLE_DONTUSEMILLISECONDS, DEFAULT, DEFAULT, HUDORDER_FOURTHBOTTOM, TRUE, HUD_COLOUR_BRONZE, DEFAULT, DEFAULT, DEFAULT, HUD_COLOUR_BRONZE ,
							DEFAULT, PlayerThreeFleckColour)
		ENDIF
	ENDIF
		
	IF bDisplayAttempt
		DRAW_GENERIC_TIMER(Attempt_Display, "HUD_ATTEMPTS", DEFAULT, TIMER_STYLE_DONTUSEMILLISECONDS, DEFAULT, DEFAULT, HUDORDER_THIRDBOTTOM, DEFAULT, DEFAULT, HUDFLASHING_FLASHWHITE, FlashBackgroundtimerMS )
	ENDIF
	

	SWITCH LocalDisplayType 
	
		CASE FMEVENT_SCORETITLE_GAMERTAG
			IF PLAYER_ID() != INVALID_PLAYER_INDEX()
				IF FM_EVENT_IS_PLAYER_RESTRICTED_WITH_PASSIVE(PLAYER_ID()) = FALSE
					DRAW_GENERIC_TIMER(iLocal_Score_Display, GET_PLAYER_NAME(PLAYER_ID()), DEFAULT, TIMER_STYLE_DONTUSEMILLISECONDS, DEFAULT, DEFAULT, HUDORDER_SECONDBOTTOM, TRUE)
				ENDIF
			ENDIF
		BREAK
		
		CASE FMEVENT_SCORETITLE_YOUR_SCORE
			IF FM_EVENT_IS_PLAYER_RESTRICTED_WITH_PASSIVE(PLAYER_ID()) = FALSE
				DRAW_GENERIC_TIMER(iLocal_Score_Display, "HUD_USCORE", DEFAULT, TIMER_STYLE_DONTUSEMILLISECONDS, DEFAULT, DEFAULT, HUDORDER_SECONDBOTTOM)
			ENDIF
		BREAK
		
		CASE FMEVENT_SCORETITLE_YOUR_BEST
			IF FM_EVENT_IS_PLAYER_RESTRICTED_WITH_PASSIVE(PLAYER_ID()) = FALSE
				DRAW_GENERIC_TIMER(iLocal_Score_Display, "HUD_UBEST", DEFAULT, TIMER_STYLE_DONTUSEMILLISECONDS, DEFAULT, DEFAULT, HUDORDER_SECONDBOTTOM)
			ENDIF
		BREAK
		
		CASE FMEVENT_SCORETITLE_NONE
		//nothing
		BREAK
	ENDSWITCH
	
	
	STRING LocalModelName = "HUD_COUNTDOWN"
	IF NOT IS_STRING_EMPTY_HUD(ModeName)
		LocalModelName = ModeName
	ENDIF
	DRAW_GENERIC_TIMER(Event_Timer_Display, LocalModelName, DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUDORDER_BOTTOM, DEFAULT, EventTimerColour, DEFAULT, DEFAULT, DEFAULT, EventTimerColour)
			
	bForceRebuildNames = FALSE
	
	SET_FAR_RIGHT_TITLE_POSITION_HUD_THIS_FRAME()
	
ENDPROC










PROC BOTTOM_RIGHT_UI_EVENT_TIMER(INT Event_Timer_Display, HUD_COLOURS EventTimerColour = HUD_COLOUR_WHITE, STRING ModeName = NULL)												
	IF SHOULD_DISPLAY_BOTTOM_RIGHT_UI_EVENTS() = FALSE
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF g_DisplayTimersDisplaying
			DEBUG_PRINTCALLSTACK()
			PRINTLN("[BCEVENTTIMERS] BOTTOM_RIGHT_UI_EVENT_TIMER - called ")
		ENDIF
	#ENDIF
	
	STRING LocalModelName = "HUD_COUNTDOWN"
	IF NOT IS_STRING_EMPTY_HUD(ModeName)
		LocalModelName = ModeName
	ENDIF
	DRAW_GENERIC_TIMER(Event_Timer_Display, LocalModelName, DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUDORDER_BOTTOM, DEFAULT, EventTimerColour, DEFAULT, DEFAULT, DEFAULT, EventTimerColour)
ENDPROC


PROC BOTTOM_RIGHT_UI_STARTING_EVENT_TIMER(INT Event_Starting_Timer_Display, STRING ModeName = NULL)			
	IF SHOULD_DISPLAY_BOTTOM_RIGHT_UI_EVENTS() = FALSE
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF g_DisplayTimersDisplaying
			DEBUG_PRINTCALLSTACK()
			PRINTLN("[BCEVENTTIMERS] BOTTOM_RIGHT_UI_STARTING_EVENT_TIMER - called ")
		ENDIF
	#ENDIF
	
	STRING LocalModelName = "HUD_STARTING"
	IF NOT IS_STRING_EMPTY_HUD(ModeName)
		LocalModelName = ModeName
	ENDIF
	DRAW_GENERIC_TIMER(Event_Starting_Timer_Display, LocalModelName, DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUDORDER_BOTTOM)
ENDPROC



//AM URBAN WAREFARE

PROC BOTTOM_RIGHT_UI_VEHTEAM_ENEMIES_TIMER(INT TeamScore, INT CurrentEnemies, INT MaxEnemies, INT EventEndTimer, HUD_COLOURS EventTimerColour = HUD_COLOUR_WHITE, BOOL displayKills = FALSE, STRING ModeName = NULL, HUD_COLOURS TeamColour = HUD_COLOUR_WHITE)
	
	IF SHOULD_DISPLAY_BOTTOM_RIGHT_UI_EVENTS() = FALSE
		EXIT
	ENDIF
	SET_FAR_RIGHT_TITLE_POSITION_HUD_THIS_FRAME()

	HIDE_BOTTOM_RIGHT_CODE_UI()
	
	#IF IS_DEBUG_BUILD
		IF g_DisplayTimersDisplaying
			DEBUG_PRINTCALLSTACK()
			PRINTLN("[BCEVENTTIMERS] BOTTOM_RIGHT_UI_VEHTEAM_ENEMIES_TIMER - called ")
		ENDIF
	#ENDIF
	
	TEXT_LABEL_63 vehFullString = "HUD_TEAM"
	IF displayKills
		DRAW_GENERIC_SCORE(TeamScore,vehFullString , DEFAULT, TeamColour, HUDORDER_THIRDBOTTOM, TRUE, "", 
						DEFAULT,DEFAULT,DEFAULT,DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)	
	ENDIF
	DRAW_GENERIC_BIG_DOUBLE_NUMBER(CurrentEnemies, MaxEnemies, "UW_KLL", DEFAULT, DEFAULT, HUDORDER_SECONDBOTTOM, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
	STRING LocalModelName = "HUD_COUNTDOWN"
	IF NOT IS_STRING_EMPTY_HUD(ModeName)
		LocalModelName = ModeName
	ENDIF
	DRAW_GENERIC_TIMER(EventEndTimer, LocalModelName, DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUDORDER_BOTTOM, DEFAULT, EventTimerColour, DEFAULT, DEFAULT, DEFAULT, EventTimerColour)
		
ENDPROC


PROC BOTTOM_RIGHT_UI_GAMERTAG_ENEMIES_TIMER(STRING Gamertag, INT TeamScore, INT CurrentEnemies, INT MaxEnemies, INT EventEndTimer, HUD_COLOURS EventTimerColour = HUD_COLOUR_WHITE, BOOL displayKills = FALSE, STRING ModeName = NULL )
	IF SHOULD_DISPLAY_BOTTOM_RIGHT_UI_EVENTS() = FALSE
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF g_DisplayTimersDisplaying
			DEBUG_PRINTCALLSTACK()
			PRINTLN("[BCEVENTTIMERS] BOTTOM_RIGHT_UI_GAMERTAG_ENEMIES_TIMER - called ")
		ENDIF
	#ENDIF
	
	HIDE_BOTTOM_RIGHT_CODE_UI()
	SET_FAR_RIGHT_TITLE_POSITION_HUD_THIS_FRAME()
	
	IF displayKills
		DRAW_GENERIC_SCORE(TeamScore,Gamertag , DEFAULT, DEFAULT, HUDORDER_THIRDBOTTOM, TRUE, "HUD_NUMKILLS")		
	ENDIF
	DRAW_GENERIC_BIG_DOUBLE_NUMBER(CurrentEnemies, MaxEnemies, "UW_KLL", DEFAULT, DEFAULT, HUDORDER_SECONDBOTTOM, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
	STRING LocalModelName = "HUD_COUNTDOWN"
	IF NOT IS_STRING_EMPTY_HUD(ModeName)
		LocalModelName = ModeName
	ENDIF
	DRAW_GENERIC_TIMER(EventEndTimer, LocalModelName, DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUDORDER_BOTTOM, DEFAULT, EventTimerColour, DEFAULT, DEFAULT, DEFAULT, EventTimerColour)
ENDPROC


//AM URBAN WAREFARE COMPETITIVE


//to hide a team, pass in -1 to the score field.
PROC BOTTOM_RIGHT_UI_VEHTEAM_COMPETITIVE_ENEMIES_TIMER(INT FirstTeamScore,INT FirstTeamNum,  INT SecondTeamScore, INT SecondTeamNum, INT ThirdTeamScore, INT ThirdTeamNum, INT FourthTeamScore, INT FourthTeamNum,  INT EventEndTimer, HUD_COLOURS EventTimerColour = HUD_COLOUR_WHITE, STRING ModeName = NULL, INT iLocalPlayerTeam = -1)
	IF SHOULD_DISPLAY_BOTTOM_RIGHT_UI_EVENTS() = FALSE
		EXIT
	ENDIF
	SET_FAR_RIGHT_TITLE_POSITION_HUD_THIS_FRAME()

	HIDE_BOTTOM_RIGHT_CODE_UI()
	
	#IF IS_DEBUG_BUILD
		IF g_DisplayTimersDisplaying
			DEBUG_PRINTCALLSTACK()
			PRINTLN("[BCEVENTTIMERS] BOTTOM_RIGHT_UI_VEHTEAM_COMPETITIVE_ENEMIES_TIMER - called ")
		ENDIF
	#ENDIF	
	
	TEXT_LABEL_63 vehFullString
	

	IF FirstTeamScore > -1
		vehFullString = PARSE_PLACEMENT_TEAM_AND_NUMBER(FirstTeamNum)
		HUD_COLOURS TitleColour = HUD_COLOUR_WHITE
		IF iLocalPlayerTeam = FirstTeamNum
			TitleColour = HUD_COLOUR_BLUE
		ENDIF
		DRAW_GENERIC_SCORE(FirstTeamScore,vehFullString , DEFAULT, TitleColour, HUDORDER_FIFTHBOTTOM, TRUE, "", 
						DEFAULT,DEFAULT,DEFAULT,DEFAULT, TitleColour, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)		
	ENDIF
	
	IF SecondTeamScore > -1
		vehFullString = PARSE_PLACEMENT_TEAM_AND_NUMBER(SecondTeamNum)
		HUD_COLOURS TitleColour = HUD_COLOUR_WHITE
		IF iLocalPlayerTeam = SecondTeamNum
			TitleColour = HUD_COLOUR_BLUE
		ENDIF
		DRAW_GENERIC_SCORE(SecondTeamScore,vehFullString , DEFAULT, TitleColour, HUDORDER_FOURTHBOTTOM, TRUE, "",
						DEFAULT,DEFAULT,DEFAULT,DEFAULT, TitleColour, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)	
	ENDIF
	
	IF ThirdTeamScore > -1
		vehFullString = PARSE_PLACEMENT_TEAM_AND_NUMBER(ThirdTeamNum)
		HUD_COLOURS TitleColour = HUD_COLOUR_WHITE
		IF iLocalPlayerTeam = ThirdTeamNum
			TitleColour = HUD_COLOUR_BLUE
		ENDIF
		DRAW_GENERIC_SCORE(ThirdTeamScore,vehFullString , DEFAULT, TitleColour, HUDORDER_THIRDBOTTOM, TRUE, "",
						DEFAULT,DEFAULT,DEFAULT,DEFAULT, TitleColour, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)	
	ENDIF
	
	IF FourthTeamScore > -1
		vehFullString = PARSE_PLACEMENT_TEAM_AND_NUMBER(FourthTeamNum)
		HUD_COLOURS TitleColour = HUD_COLOUR_WHITE
		IF iLocalPlayerTeam = FourthTeamNum
			TitleColour = HUD_COLOUR_BLUE
		ENDIF
		DRAW_GENERIC_SCORE(FourthTeamScore,vehFullString , DEFAULT, TitleColour, HUDORDER_SECONDBOTTOM, TRUE, "", 
						DEFAULT,DEFAULT,DEFAULT,DEFAULT, TitleColour, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)		
	ENDIF
	
	
	STRING LocalModelName = "HUD_COUNTDOWN"
	IF NOT IS_STRING_EMPTY_HUD(ModeName)
		LocalModelName = ModeName
	ENDIF
	DRAW_GENERIC_TIMER(EventEndTimer, LocalModelName, DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUDORDER_BOTTOM, DEFAULT, EventTimerColour, DEFAULT, DEFAULT, DEFAULT, EventTimerColour)
ENDPROC


PROC BOTTOM_RIGHT_UI_GAMERTAG_COMPETITIVE_ENEMIES_TIMER(STRING FirstGamertag,INT FirstTeamNum,
														STRING SecondGamertag, INT SecondTeamNum,
														STRING ThirdGamertag, INT ThirdTeamNum,
														STRING FourthGamertag, INT FourthTeamNum,
														INT FirstTeamScore, INT SecondTeamScore, 
														INT ThirdTeamScore, INT FourthTeamScore, 
														INT EventEndTimer, 
														HUD_COLOURS EventTimerColour = HUD_COLOUR_WHITE,
														STRING ModeName = NULL, 
														INT iLocalPlayerTeam = -1)
	IF SHOULD_DISPLAY_BOTTOM_RIGHT_UI_EVENTS() = FALSE
		EXIT
	ENDIF													
	SET_FAR_RIGHT_TITLE_POSITION_HUD_THIS_FRAME()
	
	HIDE_BOTTOM_RIGHT_CODE_UI()
		
	#IF IS_DEBUG_BUILD
		IF g_DisplayTimersDisplaying
			DEBUG_PRINTCALLSTACK()
			PRINTLN("[BCEVENTTIMERS] BOTTOM_RIGHT_UI_GAMERTAG_COMPETITIVE_ENEMIES_TIMER - called ")
		ENDIF
	#ENDIF	
		
	IF FirstTeamScore > -1
		HUD_COLOURS TitleColour = HUD_COLOUR_WHITE
		IF iLocalPlayerTeam = FirstTeamNum
			TitleColour = HUD_COLOUR_BLUE
		ENDIF
		DRAW_GENERIC_SCORE(FirstTeamScore,FirstGamertag , DEFAULT, DEFAULT, HUDORDER_FIFTHBOTTOM, TRUE, "", DEFAULT, DEFAULT, DEFAULT, DEFAULT, TitleColour)		
	ENDIF
	
	IF SecondTeamScore > -1
		HUD_COLOURS TitleColour = HUD_COLOUR_WHITE
		IF iLocalPlayerTeam = SecondTeamNum
			TitleColour = HUD_COLOUR_BLUE
		ENDIF
		DRAW_GENERIC_SCORE(SecondTeamScore,SecondGamertag , DEFAULT, DEFAULT, HUDORDER_FOURTHBOTTOM, TRUE, "", DEFAULT, DEFAULT, DEFAULT, DEFAULT, TitleColour)		
	ENDIF
	
	IF ThirdTeamScore > -1
		HUD_COLOURS TitleColour = HUD_COLOUR_WHITE
		IF iLocalPlayerTeam = ThirdTeamNum
			TitleColour = HUD_COLOUR_BLUE
		ENDIF
		DRAW_GENERIC_SCORE(ThirdTeamScore,ThirdGamertag , DEFAULT, DEFAULT, HUDORDER_THIRDBOTTOM, TRUE, "", DEFAULT, DEFAULT, DEFAULT, DEFAULT, TitleColour)		
	ENDIF
	
	IF FourthTeamScore > -1
		HUD_COLOURS TitleColour = HUD_COLOUR_WHITE
		IF iLocalPlayerTeam = FourthTeamNum
			TitleColour = HUD_COLOUR_BLUE
		ENDIF
		DRAW_GENERIC_SCORE(FourthTeamScore,FourthGamertag , DEFAULT, DEFAULT, HUDORDER_SECONDBOTTOM, TRUE, "", DEFAULT, DEFAULT, DEFAULT, DEFAULT, TitleColour)		
	ENDIF
	
	
	STRING LocalModelName = "HUD_COUNTDOWN"
	IF NOT IS_STRING_EMPTY_HUD(ModeName)
		LocalModelName = ModeName
	ENDIF
	DRAW_GENERIC_TIMER(EventEndTimer, LocalModelName, DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUDORDER_BOTTOM, DEFAULT, EventTimerColour, DEFAULT, DEFAULT, DEFAULT, EventTimerColour)
ENDPROC



//HOT PROPERTY



PROC BOTTOM_RIGHT_UI_1STTIME_2NDTIME_3RDTIME_TIMEHELD_TIMER(PLAYER_INDEX First_Player_Index, 
																PLAYER_INDEX Second_Player_Index, 
																PLAYER_INDEX Third_Player_Index, 
																INT First_Player_Score, 
																INT Second_Player_Score, 
																INT Third_Player_Score, 
																INT TimeHeld,
																INT Event_Timer_Display,			
																BOOL& bForceRebuildNames, 
																HUD_COLOURS EventTimerColour = HUD_COLOUR_WHITE,
																STRING ModeName = NULL,
																HUD_COLOURS PlayerOneFleckColour = HUD_COLOUR_PURE_WHITE,
																HUD_COLOURS PlayerTwoFleckColour = HUD_COLOUR_PURE_WHITE,
																HUD_COLOURS PlayerThreeFleckColour = HUD_COLOUR_PURE_WHITE)
						
	IF SHOULD_DISPLAY_BOTTOM_RIGHT_UI_EVENTS() = FALSE
		EXIT
	ENDIF															
	HIDE_BOTTOM_RIGHT_CODE_UI()
				
										
	#IF IS_DEBUG_BUILD
		IF g_DisplayTimersDisplaying
			DEBUG_PRINTCALLSTACK()
			PRINTLN("[BCEVENTTIMERS] BOTTOM_RIGHT_UI_1STTIME_2NDTIME_3RDTIME_TIMEHELD_TIMER - called ")
		ENDIF
	#ENDIF										
										
	STRING PlayerName								
	BOOL RebuildStrings = FALSE
	
	IF EventHudPlayerIndexLastFrame[0] != First_Player_Index
	OR EventHudPlayerIndexLastFrame[1] != Second_Player_Index
	OR EventHudPlayerIndexLastFrame[2] != Third_Player_Index
	OR bForceRebuildNames
		RebuildStrings = TRUE
	ENDIF
	
	EventHudPlayerIndexLastFrame[0] = First_Player_Index
	EventHudPlayerIndexLastFrame[1] = Second_Player_Index
	EventHudPlayerIndexLastFrame[2] = Third_Player_Index
	EventHudPlayerIndexLastFrame[3] = NULL
	EventHudPlayerIndexLastFrame[4] = NULL


		
	IF EventHudPlayerIndexLastFrame[0] != INVALID_PLAYER_INDEX()
		IF RebuildStrings = TRUE
		
			PlayerName = GET_PLAYER_NAME(EventHudPlayerIndexLastFrame[0])
			EventHudPlayerNameLastFrame[0] = PARSE_PLACEMENT_GAMERTAGS(DISPLAY_EVENT_PLACEMENT_FIRST, PlayerName)
		ENDIF
		IF First_Player_Score > 0
			DRAW_GENERIC_TIMER(First_Player_Score, EventHudPlayerNameLastFrame[0], DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUDORDER_FIFTHBOTTOM, TRUE, HUD_COLOUR_GOLD, DEFAULT, DEFAULT, DEFAULT, HUD_COLOUR_GOLD,
								DEFAULT, PlayerOneFleckColour)		
		ENDIF
	ENDIF	
	
	
	IF EventHudPlayerIndexLastFrame[1] != INVALID_PLAYER_INDEX()
		IF RebuildStrings = TRUE
			PlayerName = GET_PLAYER_NAME(EventHudPlayerIndexLastFrame[1])
			EventHudPlayerNameLastFrame[1] = PARSE_PLACEMENT_GAMERTAGS(DISPLAY_EVENT_PLACEMENT_SECOND, PlayerName)
		ENDIF
		IF Second_Player_Score > 0
			DRAW_GENERIC_TIMER(Second_Player_Score, EventHudPlayerNameLastFrame[1],DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUDORDER_FOURTHBOTTOM, TRUE, HUD_COLOUR_SILVER, DEFAULT, DEFAULT, DEFAULT, HUD_COLOUR_SILVER,
								DEFAULT, PlayerTwoFleckColour)		
		ENDIF
	ENDIF


	IF EventHudPlayerIndexLastFrame[2] != INVALID_PLAYER_INDEX()
		IF RebuildStrings = TRUE
			PlayerName = GET_PLAYER_NAME(EventHudPlayerIndexLastFrame[2])
			EventHudPlayerNameLastFrame[2] = PARSE_PLACEMENT_GAMERTAGS(DISPLAY_EVENT_PLACEMENT_THIRD, PlayerName)
		ENDIF
		IF Third_Player_Score > 0
			DRAW_GENERIC_TIMER(Third_Player_Score, EventHudPlayerNameLastFrame[2], DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUDORDER_THIRDBOTTOM, TRUE, HUD_COLOUR_BRONZE, DEFAULT, DEFAULT, DEFAULT, HUD_COLOUR_BRONZE,
								DEFAULT, PlayerThreeFleckColour)		
		ENDIF
	ENDIF
		
		
	DRAW_GENERIC_TIMER(TimeHeld, "HUD_TIMEHELD", DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUDORDER_SECONDBOTTOM)
	
	
	STRING LocalModelName = "HUD_COUNTDOWN"
	IF NOT IS_STRING_EMPTY_HUD(ModeName)
		LocalModelName = ModeName
	ENDIF
	DRAW_GENERIC_TIMER(Event_Timer_Display, LocalModelName, DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUDORDER_BOTTOM, DEFAULT, EventTimerColour, DEFAULT, DEFAULT, DEFAULT, EventTimerColour)
			
	bForceRebuildNames = FALSE
	
ENDPROC


//AM INFECTION


PROC BOTTOM_RIGHT_UI_CHECKPOINT_TIMER(INT NumberNotInfected, INT Event_Timer_Display, HUD_COLOURS EventTimerColour = HUD_COLOUR_WHITE, STRING ModeName = NULL)	
	IF SHOULD_DISPLAY_BOTTOM_RIGHT_UI_EVENTS() = FALSE
		EXIT
	ENDIF
	HIDE_BOTTOM_RIGHT_CODE_UI()
		
	#IF IS_DEBUG_BUILD
		IF g_DisplayTimersDisplaying
			DEBUG_PRINTCALLSTACK()
			PRINTLN("[BCEVENTTIMERS] BOTTOM_RIGHT_UI_CHECKPOINT_TIMER - called ")
		ENDIF
	#ENDIF		
	
	DRAW_GENERIC_SCORE(NumberNotInfected, "HUD_UNINFECTED", DEFAULT, DEFAULT, HUDORDER_DONTCARE)
	STRING LocalModelName = "HUD_COUNTDOWN"
	IF NOT IS_STRING_EMPTY_HUD(ModeName)
		LocalModelName = ModeName
	ENDIF
	DRAW_GENERIC_TIMER(Event_Timer_Display, LocalModelName, DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUDORDER_BOTTOM, DEFAULT, EventTimerColour, DEFAULT, DEFAULT, DEFAULT, EventTimerColour)


ENDPROC


//AM PENNED IN

PROC BOTTOM_RIGHT_UI_DOUBLE_NUM_TIMER(INT NumberInvolved, INT OutOfNumber, INT Event_Timer_Display, HUD_COLOURS EventTimerColour = HUD_COLOUR_WHITE, STRING ModeName = NULL)	
	IF SHOULD_DISPLAY_BOTTOM_RIGHT_UI_EVENTS() = FALSE
		EXIT
	ENDIF
	HIDE_BOTTOM_RIGHT_CODE_UI()
	
	#IF IS_DEBUG_BUILD
		IF g_DisplayTimersDisplaying
			DEBUG_PRINTCALLSTACK()
			PRINTLN("[BCEVENTTIMERS] BOTTOM_RIGHT_UI_DOUBLE_NUM_TIMER - called ")
		ENDIF
	#ENDIF		
	
	DRAW_GENERIC_BIG_DOUBLE_NUMBER(NumberInvolved, OutOfNumber, "HUD_PARITIC", DEFAULT, DEFAULT, HUDORDER_SECONDBOTTOM)
	STRING LocalModelName = "HUD_COUNTDOWN"
	IF NOT IS_STRING_EMPTY_HUD(ModeName)
		LocalModelName = ModeName
	ENDIF
	DRAW_GENERIC_TIMER(Event_Timer_Display, LocalModelName, DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUDORDER_BOTTOM, DEFAULT, EventTimerColour, DEFAULT, DEFAULT, DEFAULT, EventTimerColour)


ENDPROC


PROC BOTTOM_RIGHT_UI_SCORE_TIMER(INT NumberInvolved, STRING NumberTitle, INT Event_Timer_Display, HUD_COLOURS EventTimerColour = HUD_COLOUR_WHITE, STRING ModeName = NULL)	
	IF SHOULD_DISPLAY_BOTTOM_RIGHT_UI_EVENTS() = FALSE
		EXIT
	ENDIF
	
	HIDE_BOTTOM_RIGHT_CODE_UI()
	
	#IF IS_DEBUG_BUILD
		IF g_DisplayTimersDisplaying
			DEBUG_PRINTCALLSTACK()
			PRINTLN("[BCEVENTTIMERS] BOTTOM_RIGHT_UI_SCORE_TIMER - called ")
		ENDIF
	#ENDIF		
	
	DRAW_GENERIC_SCORE(NumberInvolved, NumberTitle, DEFAULT, DEFAULT, HUDORDER_SECONDBOTTOM)
	
	STRING LocalModelName = "HUD_COUNTDOWN"
	IF NOT IS_STRING_EMPTY_HUD(ModeName)
		LocalModelName = ModeName
	ENDIF
	DRAW_GENERIC_TIMER(Event_Timer_Display, LocalModelName, DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUDORDER_BOTTOM, DEFAULT, EventTimerColour, DEFAULT, DEFAULT, DEFAULT, EventTimerColour)


ENDPROC

//AM_CRIMINAL_DAMAGE


//THIS IS NOT TIMER NOW CL 9068938  EDIW-STILEY
PROC BOTTOM_RIGHT_UI_1STTIME_2NDTIME_3RDTIME_DAMAGE_TIMER(PLAYER_INDEX First_Player_Index, 
																PLAYER_INDEX Second_Player_Index, 
																PLAYER_INDEX Third_Player_Index, 
																INT First_Player_Score, 
																INT Second_Player_Score, 
																INT Third_Player_Score, 
																INT DamageCash,
																INT Event_Timer_Display,			
																BOOL& bForceRebuildNames, 
																HUD_COLOURS EventTimerColour = HUD_COLOUR_WHITE,
																STRING ModeName = NULL,
																STRING overrideScoreName = NULL,
																INT iCurrentCash = 0,
																BOOL bForceShowCash = FALSE,
																HUD_COLOURS PlayerOneFleckColour = HUD_COLOUR_PURE_WHITE,
																HUD_COLOURS PlayerTwoFleckColour = HUD_COLOUR_PURE_WHITE,
																HUD_COLOURS PlayerThreeFleckColour = HUD_COLOUR_PURE_WHITE)
						
	IF SHOULD_DISPLAY_BOTTOM_RIGHT_UI_EVENTS() = FALSE
		EXIT
	ENDIF															
	HIDE_BOTTOM_RIGHT_CODE_UI()
	
										
	#IF IS_DEBUG_BUILD
		IF g_DisplayTimersDisplaying
			DEBUG_PRINTCALLSTACK()
			PRINTLN("[BCEVENTTIMERS] BOTTOM_RIGHT_UI_1STTIME_2NDTIME_3RDTIME_DAMAGE_TIMER - called ")
		ENDIF
	#ENDIF											
										
	STRING PlayerName								
	BOOL RebuildStrings = FALSE
	
	IF EventHudPlayerIndexLastFrame[0] != First_Player_Index
	OR EventHudPlayerIndexLastFrame[1] != Second_Player_Index
	OR EventHudPlayerIndexLastFrame[2] != Third_Player_Index
	OR bForceRebuildNames
		RebuildStrings = TRUE
	ENDIF
	
	EventHudPlayerIndexLastFrame[0] = First_Player_Index
	EventHudPlayerIndexLastFrame[1] = Second_Player_Index
	EventHudPlayerIndexLastFrame[2] = Third_Player_Index
	EventHudPlayerIndexLastFrame[3] = NULL
	EventHudPlayerIndexLastFrame[4] = NULL


		
	IF EventHudPlayerIndexLastFrame[0] != INVALID_PLAYER_INDEX()	
		IF RebuildStrings = TRUE
		
			PlayerName = GET_PLAYER_NAME(EventHudPlayerIndexLastFrame[0])
			EventHudPlayerNameLastFrame[0] = PARSE_PLACEMENT_GAMERTAGS(DISPLAY_EVENT_PLACEMENT_FIRST, PlayerName)
		ENDIF
		IF First_Player_Score > 0
			DRAW_GENERIC_SCORE(First_Player_Score, EventHudPlayerNameLastFrame[0], DEFAULT, HUD_COLOUR_GOLD, HUDORDER_SIXTHBOTTOM, TRUE, "HUD_CASH", DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUD_COLOUR_GOLD,
							DEFAULT, DEFAULT, DEFAULT, DEFAULT,DEFAULT,DEFAULT,PlayerOneFleckColour)		
		ENDIF
	ENDIF	
	
	
	IF EventHudPlayerIndexLastFrame[1] != INVALID_PLAYER_INDEX()
		IF RebuildStrings = TRUE
			PlayerName = GET_PLAYER_NAME(EventHudPlayerIndexLastFrame[1])
			EventHudPlayerNameLastFrame[1] = PARSE_PLACEMENT_GAMERTAGS(DISPLAY_EVENT_PLACEMENT_SECOND, PlayerName)
		ENDIF
		IF Second_Player_Score > 0
			DRAW_GENERIC_SCORE(Second_Player_Score, EventHudPlayerNameLastFrame[1],DEFAULT, HUD_COLOUR_SILVER, HUDORDER_FIFTHBOTTOM, TRUE, "HUD_CASH", DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUD_COLOUR_SILVER,
							DEFAULT, DEFAULT, DEFAULT, DEFAULT,DEFAULT,DEFAULT,PlayerTwoFleckColour)	
		ENDIF
	ENDIF


	IF EventHudPlayerIndexLastFrame[2] != INVALID_PLAYER_INDEX()
		IF RebuildStrings = TRUE
			PlayerName = GET_PLAYER_NAME(EventHudPlayerIndexLastFrame[2])
			EventHudPlayerNameLastFrame[2] = PARSE_PLACEMENT_GAMERTAGS(DISPLAY_EVENT_PLACEMENT_THIRD, PlayerName)
		ENDIF
		IF Third_Player_Score > 0
			DRAW_GENERIC_SCORE(Third_Player_Score, EventHudPlayerNameLastFrame[2], DEFAULT, HUD_COLOUR_BRONZE, HUDORDER_FOURTHBOTTOM, TRUE, "HUD_CASH", DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUD_COLOUR_BRONZE,
							DEFAULT, DEFAULT, DEFAULT, DEFAULT,DEFAULT,DEFAULT,PlayerThreeFleckColour)	
		ENDIF
	ENDIF
		
	STRING thisLabel = "TIM_DAMAGE"
	IF NOT IS_STRING_NULL_OR_EMPTY(overrideScoreName)
		thisLabel = overrideScoreName
	ENDIF
	IF NOT FM_EVENT_IS_PLAYER_RESTRICTED_WITH_PASSIVE(PLAYER_ID())	
		DRAW_GENERIC_SCORE(DamageCash, thisLabel, DEFAULT, DEFAULT, HUDORDER_THIRDBOTTOM, DEFAULT, "HUD_CASH" )
	ENDIF
	
	IF iCurrentCash > 0
	OR bForceShowCash
		IF NOT FM_EVENT_IS_PLAYER_RESTRICTED_WITH_PASSIVE(PLAYER_ID())	
			DRAW_GENERIC_SCORE(iCurrentCash, "GB_SNG_HT0", DEFAULT, DEFAULT, HUDORDER_SECONDBOTTOM, DEFAULT, "HUD_CASH" )
		ENDIF
	ENDIF
	
	STRING LocalModelName = "HUD_COUNTDOWN"
	IF NOT IS_STRING_EMPTY_HUD(ModeName)
		LocalModelName = ModeName
	ENDIF
	DRAW_GENERIC_TIMER(Event_Timer_Display, LocalModelName, DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUDORDER_BOTTOM, DEFAULT, EventTimerColour, DEFAULT, DEFAULT, DEFAULT, EventTimerColour)
			
	bForceRebuildNames = FALSE
	
	SET_FAR_RIGHT_TITLE_POSITION_HUD_THIS_FRAME()	
	
ENDPROC



//AM KING OF THE CASTLE


PROC BOTTOM_RIGHT_UI_1STINT_2NDINT_3RDINT_LOCALINT_TIMER(PLAYER_INDEX First_Player_Index, 
																PLAYER_INDEX Second_Player_Index, 
																PLAYER_INDEX Third_Player_Index, 
																INT First_Player_Score, 
																INT Second_Player_Score, 
																INT Third_Player_Score, 
																INT iLocal_Score_Display, 
																INT Event_Timer_Display,			
																BOOL& bForceRebuildNames, 
																HUD_COLOURS EventTimerColour = HUD_COLOUR_WHITE,
																STRING ModeName = NULL,
																FMEVENT_SCORETITLE LocalDisplayType = FMEVENT_SCORETITLE_YOUR_SCORE,
																HUD_COLOURS PlayerOneFleckColour = HUD_COLOUR_PURE_WHITE,
																HUD_COLOURS PlayerTwoFleckColour = HUD_COLOUR_PURE_WHITE,
																HUD_COLOURS PlayerThreeFleckColour = HUD_COLOUR_PURE_WHITE)
						
	IF SHOULD_DISPLAY_BOTTOM_RIGHT_UI_EVENTS() = FALSE
		EXIT
	ENDIF															
	HIDE_BOTTOM_RIGHT_CODE_UI()
				
	#IF IS_DEBUG_BUILD
		IF g_DisplayTimersDisplaying
			DEBUG_PRINTCALLSTACK()
			PRINTLN("[BCEVENTTIMERS] BOTTOM_RIGHT_UI_1STINT_2NDINT_3RDINT_LOCALINT_TIMER - called ")
		ENDIF
	#ENDIF											
										
	STRING PlayerName								
	BOOL RebuildStrings = FALSE
	
	IF EventHudPlayerIndexLastFrame[0] != First_Player_Index
	OR EventHudPlayerIndexLastFrame[1] != Second_Player_Index
	OR EventHudPlayerIndexLastFrame[2] != Third_Player_Index
	OR bForceRebuildNames
		RebuildStrings = TRUE
	ENDIF
	
	EventHudPlayerIndexLastFrame[0] = First_Player_Index
	EventHudPlayerIndexLastFrame[1] = Second_Player_Index
	EventHudPlayerIndexLastFrame[2] = Third_Player_Index
	EventHudPlayerIndexLastFrame[3] = NULL
	EventHudPlayerIndexLastFrame[4] = NULL


		
	IF EventHudPlayerIndexLastFrame[0] != INVALID_PLAYER_INDEX()	
		IF RebuildStrings = TRUE
		
			PlayerName = GET_PLAYER_NAME(EventHudPlayerIndexLastFrame[0])
			EventHudPlayerNameLastFrame[0] = PARSE_PLACEMENT_GAMERTAGS(DISPLAY_EVENT_PLACEMENT_FIRST, PlayerName)
		ENDIF
		IF First_Player_Score > 0
			DRAW_GENERIC_SCORE(First_Player_Score, EventHudPlayerNameLastFrame[0], DEFAULT, HUD_COLOUR_GOLD, HUDORDER_SIXTHBOTTOM, TRUE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUD_COLOUR_GOLD,
							DEFAULT, DEFAULT, DEFAULT, DEFAULT,DEFAULT,DEFAULT,PlayerOneFleckColour)			
		ENDIF
	ENDIF	
	
	
	IF EventHudPlayerIndexLastFrame[1] != INVALID_PLAYER_INDEX()
		IF RebuildStrings = TRUE
			PlayerName = GET_PLAYER_NAME(EventHudPlayerIndexLastFrame[1])
			EventHudPlayerNameLastFrame[1] = PARSE_PLACEMENT_GAMERTAGS(DISPLAY_EVENT_PLACEMENT_SECOND, PlayerName)
		ENDIF
		IF Second_Player_Score > 0
			DRAW_GENERIC_SCORE(Second_Player_Score, EventHudPlayerNameLastFrame[1], DEFAULT, HUD_COLOUR_SILVER, HUDORDER_FIFTHBOTTOM, TRUE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUD_COLOUR_SILVER,
							DEFAULT, DEFAULT, DEFAULT, DEFAULT,DEFAULT,DEFAULT,PlayerTwoFleckColour)			
		ENDIF
	ENDIF


	IF EventHudPlayerIndexLastFrame[2] != INVALID_PLAYER_INDEX()
		IF RebuildStrings = TRUE
			PlayerName = GET_PLAYER_NAME(EventHudPlayerIndexLastFrame[2])
			EventHudPlayerNameLastFrame[2] = PARSE_PLACEMENT_GAMERTAGS(DISPLAY_EVENT_PLACEMENT_THIRD, PlayerName)
		ENDIF
		IF Third_Player_Score > 0
			DRAW_GENERIC_SCORE(Third_Player_Score, EventHudPlayerNameLastFrame[2], DEFAULT, HUD_COLOUR_BRONZE, HUDORDER_FOURTHBOTTOM, TRUE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUD_COLOUR_BRONZE,
							DEFAULT, DEFAULT, DEFAULT, DEFAULT,DEFAULT,DEFAULT,PlayerThreeFleckColour)			
		ENDIF
	ENDIF
		
	SWITCH LocalDisplayType 
	
		CASE FMEVENT_SCORETITLE_GAMERTAG
			IF PLAYER_ID() != INVALID_PLAYER_INDEX()
				IF FM_EVENT_IS_PLAYER_RESTRICTED_WITH_PASSIVE(PLAYER_ID()) = FALSE
					DRAW_GENERIC_SCORE(iLocal_Score_Display, GET_PLAYER_NAME(PLAYER_ID()), DEFAULT, DEFAULT, HUDORDER_THIRDBOTTOM, TRUE)
				ENDIF
			ENDIF
		BREAK
		
		CASE FMEVENT_SCORETITLE_YOUR_SCORE
			IF FM_EVENT_IS_PLAYER_RESTRICTED_WITH_PASSIVE(PLAYER_ID()) = FALSE
				DRAW_GENERIC_SCORE(iLocal_Score_Display,"HUD_USCORE", DEFAULT, DEFAULT, HUDORDER_THIRDBOTTOM)
			ENDIF
		BREAK
		
		CASE FMEVENT_SCORETITLE_YOUR_BEST
			IF FM_EVENT_IS_PLAYER_RESTRICTED_WITH_PASSIVE(PLAYER_ID()) = FALSE
				DRAW_GENERIC_SCORE(iLocal_Score_Display, "HUD_UBEST", DEFAULT, DEFAULT, HUDORDER_THIRDBOTTOM)
			ENDIF
		BREAK
		
		CASE FMEVENT_SCORETITLE_NONE
		//nothing
		BREAK
	ENDSWITCH	

	
	
	STRING LocalModelName = "HUD_COUNTDOWN"
	IF NOT IS_STRING_EMPTY_HUD(ModeName)
		LocalModelName = ModeName
	ENDIF
	DRAW_GENERIC_TIMER(Event_Timer_Display, LocalModelName, DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUDORDER_BOTTOM, DEFAULT, EventTimerColour, DEFAULT, DEFAULT, DEFAULT, EventTimerColour)
			
	bForceRebuildNames = FALSE
	
ENDPROC






//AM MULTI TARGET

PROC BOTTOM_RIGHT_UI_1STINT_2NDINT_3RDINT_KILLED_TIMER(PLAYER_INDEX First_Player_Index, 
																PLAYER_INDEX Second_Player_Index, 
																PLAYER_INDEX Third_Player_Index, 
																INT First_Player_Score, 
																INT Second_Player_Score, 
																INT Third_Player_Score, 
																INT TargetsKilled, 
																INT Event_Timer_Display,			
																BOOL& bForceRebuildNames, 
																HUD_COLOURS EventTimerColour = HUD_COLOUR_WHITE,
																STRING ModeName = NULL,
																STRING PersonalTrackerTitle = NULL,
																HUD_COLOURS PlayerOneFleckColour = HUD_COLOUR_PURE_WHITE,
																HUD_COLOURS PlayerTwoFleckColour = HUD_COLOUR_PURE_WHITE,
																HUD_COLOURS PlayerThreeFleckColour = HUD_COLOUR_PURE_WHITE)
	IF SHOULD_DISPLAY_BOTTOM_RIGHT_UI_EVENTS() = FALSE
		EXIT
	ENDIF					
	HIDE_BOTTOM_RIGHT_CODE_UI()
					
	#IF IS_DEBUG_BUILD
		IF g_DisplayTimersDisplaying
			DEBUG_PRINTCALLSTACK()
			PRINTLN("[BCEVENTTIMERS] BOTTOM_RIGHT_UI_1STINT_2NDINT_3RDINT_KILLED_TIMER - called ")
		ENDIF
	#ENDIF																	
																	
									
	STRING PlayerName								
	BOOL RebuildStrings = FALSE
	
	IF EventHudPlayerIndexLastFrame[0] != First_Player_Index
	OR EventHudPlayerIndexLastFrame[1] != Second_Player_Index
	OR EventHudPlayerIndexLastFrame[2] != Third_Player_Index
	OR bForceRebuildNames
		RebuildStrings = TRUE
	ENDIF
	
	EventHudPlayerIndexLastFrame[0] = First_Player_Index
	EventHudPlayerIndexLastFrame[1] = Second_Player_Index
	EventHudPlayerIndexLastFrame[2] = Third_Player_Index
	EventHudPlayerIndexLastFrame[3] = NULL
	EventHudPlayerIndexLastFrame[4] = NULL


		
	IF EventHudPlayerIndexLastFrame[0] != INVALID_PLAYER_INDEX()	
		IF RebuildStrings = TRUE
		
			PlayerName = GET_PLAYER_NAME(EventHudPlayerIndexLastFrame[0])
			EventHudPlayerNameLastFrame[0] = PARSE_PLACEMENT_GAMERTAGS(DISPLAY_EVENT_PLACEMENT_FIRST, PlayerName)
		ENDIF
		IF First_Player_Score > 0
			DRAW_GENERIC_SCORE(First_Player_Score, EventHudPlayerNameLastFrame[0], DEFAULT, HUD_COLOUR_GOLD, HUDORDER_FIFTHBOTTOM, TRUE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUD_COLOUR_GOLD,
							DEFAULT, DEFAULT, DEFAULT, DEFAULT,DEFAULT,DEFAULT,PlayerOneFleckColour)
		ENDIF
	ENDIF	
	
	
	IF EventHudPlayerIndexLastFrame[1] != INVALID_PLAYER_INDEX()
		IF RebuildStrings = TRUE
			PlayerName = GET_PLAYER_NAME(EventHudPlayerIndexLastFrame[1])
			EventHudPlayerNameLastFrame[1] = PARSE_PLACEMENT_GAMERTAGS(DISPLAY_EVENT_PLACEMENT_SECOND, PlayerName)
		ENDIF
		IF Second_Player_Score > 0
			DRAW_GENERIC_SCORE(Second_Player_Score, EventHudPlayerNameLastFrame[1], DEFAULT, HUD_COLOUR_SILVER, HUDORDER_FOURTHBOTTOM, TRUE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUD_COLOUR_SILVER,
							DEFAULT, DEFAULT, DEFAULT, DEFAULT,DEFAULT,DEFAULT,PlayerTwoFleckColour)
		ENDIF
	ENDIF


	IF EventHudPlayerIndexLastFrame[2] != INVALID_PLAYER_INDEX()
		IF RebuildStrings = TRUE
			PlayerName = GET_PLAYER_NAME(EventHudPlayerIndexLastFrame[2])
			EventHudPlayerNameLastFrame[2] = PARSE_PLACEMENT_GAMERTAGS(DISPLAY_EVENT_PLACEMENT_THIRD, PlayerName)
		ENDIF
		IF Third_Player_Score > 0
			DRAW_GENERIC_SCORE(Third_Player_Score, EventHudPlayerNameLastFrame[2], DEFAULT, HUD_COLOUR_BRONZE, HUDORDER_THIRDBOTTOM, TRUE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUD_COLOUR_BRONZE,
							DEFAULT, DEFAULT, DEFAULT, DEFAULT,DEFAULT,DEFAULT,PlayerThreeFleckColour)
		ENDIF
	ENDIF
	
	STRING LocalTrackerTitle = "HUD_TARKILLED"
	IF NOT IS_STRING_EMPTY_HUD(PersonalTrackerTitle)
		LocalTrackerTitle = PersonalTrackerTitle	
	ENDIF		
	DRAW_GENERIC_SCORE(TargetsKilled, LocalTrackerTitle, DEFAULT, DEFAULT, HUDORDER_SECONDBOTTOM, FALSE)
	
	
	STRING LocalModelName = "HUD_COUNTDOWN"
	IF NOT IS_STRING_EMPTY_HUD(ModeName)
		LocalModelName = ModeName
	ENDIF
	DRAW_GENERIC_TIMER(Event_Timer_Display, LocalModelName, DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUDORDER_BOTTOM, DEFAULT, EventTimerColour, DEFAULT, DEFAULT, DEFAULT, EventTimerColour)
			
	bForceRebuildNames = FALSE
	
ENDPROC



//cross the line



PROC BOTTOM_RIGHT_UI_PLAYER_STATE_TIMER(STRING Team1Member1Name, INT Team1Member1Status = -3,
										STRING Team1Member2Name = NULL, INT Team1Member2Status = -3,
										STRING Team1Member3Name = NULL, INT Team1Member3Status = -3,
										STRING Team1Member4Name = NULL, INT Team1Member4Status = -3,
										STRING Team2Member1Name = NULL, INT Team2Member1Status = -3,
										STRING Team2Member2Name = NULL, INT Team2Member2Status = -3,
										STRING Team2Member3Name = NULL, INT Team2Member3Status = -3,
										STRING Team2Member4Name = NULL, INT Team2Member4Status = -3,
										HUD_COLOURS TeamOneColour = HUD_COLOUR_BLUE,
										HUD_COLOURS TeamTwoColour = HUD_COLOUR_RED,
										INT Event_Timer_Display = 0,			
										HUD_COLOURS EventTimerColour = HUD_COLOUR_WHITE,
										STRING ModeName = NULL,
										BOOL bIsSpectating = FALSE,
										BOOL bIsTimerNotDistance = FALSE)
																
																
	IF SHOULD_DISPLAY_BOTTOM_RIGHT_UI_EVENTS(bIsSpectating) = FALSE
		EXIT
	ENDIF
	
	HIDE_BOTTOM_RIGHT_CODE_UI()
	STRING NumberString
	IF NOT bIsTimerNotDistance
		IF SHOULD_USE_METRIC_MEASUREMENTS()
			NumberString = "HUD_METNUM"
		ELSE
			NumberString = "HUD_FTNUM"
		ENDIf
	ELSE
		NumberString = "AMCH_S"
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF g_DisplayTimersDisplaying	
			DEBUG_PRINTCALLSTACK()
			PRINTLN("[BCEVENTTIMERS] BOTTOM_RIGHT_UI_PLAYER_STATE_TIMER - called ")
		ENDIF
	#ENDIF
		
	IF GET_CURRENT_LANGUAGE() = LANGUAGE_KOREAN 
	OR GET_CURRENT_LANGUAGE() = LANGUAGE_CHINESE
	OR GET_CURRENT_LANGUAGE() = LANGUAGE_CHINESE_SIMPLIFIED
		IF Team1Member1Status > 99
		OR Team1Member2Status > 99
		OR Team1Member3Status > 99
		OR Team1Member4Status > 99
		OR Team2Member1Status > 99
		OR Team2Member2Status > 99
		OR Team2Member3Status > 99
		OR Team2Member4Status > 99
			SET_FAR_RIGHT_TITLE_POSITION_HUD_THIS_FRAME()
		ENDIF
	ENDIF
		
	
	SWITCH Team1Member1Status 
		CASE -3 //NONE
			
			
		BREAK
		CASE -2 //DEAD
			DRAW_GENERIC_SCORE(Team1Member1Status, Team1Member1Name, DEFAULT, DEFAULT,  HUDORDER_NINETHBOTTOM, TRUE, NumberString, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TeamOneColour, DEFAULT, DEFAULT, DEFAULT, ACTIVITY_POWERUP_CROSS )
			
		BREAK
		CASE -1 //CROSSED
			DRAW_GENERIC_SCORE(Team1Member1Status, Team1Member1Name, DEFAULT, DEFAULT,  HUDORDER_NINETHBOTTOM, TRUE, NumberString, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TeamOneColour, DEFAULT, DEFAULT, DEFAULT, ACTIVITY_POWERUP_TICK)
		
		BREAK
		DEFAULT
			DRAW_GENERIC_SCORE(Team1Member1Status, Team1Member1Name, DEFAULT, DEFAULT,  HUDORDER_NINETHBOTTOM, TRUE, NumberString, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TeamOneColour)
		BREAK
	ENDSWITCH
	
	SWITCH Team1Member2Status 
		CASE -3 //NONE
			
			
		BREAK
		CASE -2 //DEAD
			DRAW_GENERIC_SCORE(Team1Member2Status, Team1Member2Name, DEFAULT, DEFAULT,  HUDORDER_EIGHTHBOTTOM, TRUE, NumberString, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TeamOneColour, DEFAULT, DEFAULT, DEFAULT, ACTIVITY_POWERUP_CROSS )
			
		BREAK
		CASE -1 //CROSSED
			
			DRAW_GENERIC_SCORE(Team1Member2Status, Team1Member2Name, DEFAULT, DEFAULT,  HUDORDER_EIGHTHBOTTOM, TRUE, NumberString, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TeamOneColour, DEFAULT, DEFAULT, DEFAULT, ACTIVITY_POWERUP_TICK )
		BREAK
		DEFAULT
			DRAW_GENERIC_SCORE(Team1Member2Status, Team1Member2Name, DEFAULT, DEFAULT, HUDORDER_EIGHTHBOTTOM, TRUE, NumberString, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TeamOneColour)
		BREAK
	ENDSWITCH
	
	SWITCH Team1Member3Status 
		CASE -3 //NONE
			
			
		BREAK
		CASE -2 //DEAD
			
			DRAW_GENERIC_SCORE(Team1Member3Status, Team1Member3Name, DEFAULT, DEFAULT,  HUDORDER_SEVENTHBOTTOM, TRUE, NumberString, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TeamOneColour, DEFAULT, DEFAULT, DEFAULT, ACTIVITY_POWERUP_CROSS )
		BREAK
		CASE -1 //CROSSED
			
			DRAW_GENERIC_SCORE(Team1Member3Status, Team1Member3Name, DEFAULT, DEFAULT,  HUDORDER_SEVENTHBOTTOM, TRUE, NumberString, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TeamOneColour, DEFAULT, DEFAULT, DEFAULT, ACTIVITY_POWERUP_TICK )
		BREAK
		DEFAULT
			DRAW_GENERIC_SCORE(Team1Member3Status, Team1Member3Name, DEFAULT, DEFAULT, HUDORDER_SEVENTHBOTTOM, TRUE, NumberString, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TeamOneColour)
		BREAK
	ENDSWITCH
	
	SWITCH Team1Member4Status 
		CASE -3 //NONE
			
			
		BREAK
		CASE -2 //DEAD
			
			DRAW_GENERIC_SCORE(Team1Member4Status, Team1Member4Name, DEFAULT, DEFAULT,  HUDORDER_SIXTHBOTTOM, TRUE, NumberString, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TeamOneColour, DEFAULT, DEFAULT, DEFAULT, ACTIVITY_POWERUP_CROSS )
		BREAK
		CASE -1 //CROSSED
			
			DRAW_GENERIC_SCORE(Team1Member4Status, Team1Member4Name, DEFAULT, DEFAULT,  HUDORDER_SIXTHBOTTOM, TRUE, NumberString, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TeamOneColour, DEFAULT, DEFAULT, DEFAULT, ACTIVITY_POWERUP_TICK )
		BREAK
		DEFAULT
			DRAW_GENERIC_SCORE(Team1Member4Status, Team1Member4Name, DEFAULT, DEFAULT, HUDORDER_SIXTHBOTTOM, TRUE, NumberString, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TeamOneColour)
		BREAK
	ENDSWITCH
	
	
	SWITCH Team2Member1Status 
		CASE -3 //NONE
			
			
		BREAK
		CASE -2 //DEAD
			
			DRAW_GENERIC_SCORE(Team2Member1Status, Team2Member1Name, DEFAULT, DEFAULT,  HUDORDER_FIFTHBOTTOM, TRUE, NumberString, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TeamTwoColour, DEFAULT, DEFAULT, DEFAULT, ACTIVITY_POWERUP_CROSS )
		BREAK
		CASE -1 //CROSSED
			
			DRAW_GENERIC_SCORE(Team2Member1Status, Team2Member1Name, DEFAULT, DEFAULT,  HUDORDER_FIFTHBOTTOM, TRUE, NumberString, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TeamTwoColour, DEFAULT, DEFAULT, DEFAULT, ACTIVITY_POWERUP_TICK )
		BREAK
		DEFAULT
			DRAW_GENERIC_SCORE(Team2Member1Status, Team2Member1Name, DEFAULT, DEFAULT, HUDORDER_FIFTHBOTTOM, TRUE, NumberString, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TeamTwoColour)
		BREAK
	ENDSWITCH
	
	SWITCH Team2Member2Status 
		CASE -3 //NONE
			
			
		BREAK
		CASE -2 //DEAD
			DRAW_GENERIC_SCORE(Team2Member2Status, Team2Member2Name, DEFAULT, DEFAULT,  HUDORDER_FOURTHBOTTOM, TRUE, NumberString, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TeamTwoColour, DEFAULT, DEFAULT, DEFAULT, ACTIVITY_POWERUP_CROSS )
			
		BREAK
		CASE -1 //CROSSED
			
			DRAW_GENERIC_SCORE(Team2Member2Status, Team2Member2Name, DEFAULT, DEFAULT,  HUDORDER_FOURTHBOTTOM, TRUE, NumberString, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TeamTwoColour, DEFAULT, DEFAULT, DEFAULT, ACTIVITY_POWERUP_TICK )
		BREAK
		DEFAULT
			DRAW_GENERIC_SCORE(Team2Member2Status, Team2Member2Name, DEFAULT, DEFAULT, HUDORDER_FOURTHBOTTOM, TRUE, NumberString, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TeamTwoColour)
		BREAK
	ENDSWITCH
	
	SWITCH Team2Member3Status 
		CASE -3 //NONE
			
			
		BREAK
		CASE -2 //DEAD
			DRAW_GENERIC_SCORE(Team2Member3Status, Team2Member3Name, DEFAULT, DEFAULT,  HUDORDER_THIRDBOTTOM, TRUE, NumberString, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TeamTwoColour, DEFAULT, DEFAULT, DEFAULT, ACTIVITY_POWERUP_CROSS )
			
		BREAK
		CASE -1 //CROSSED
			
			DRAW_GENERIC_SCORE(Team2Member3Status, Team2Member3Name, DEFAULT, DEFAULT,  HUDORDER_THIRDBOTTOM, TRUE, NumberString, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TeamTwoColour, DEFAULT, DEFAULT, DEFAULT, ACTIVITY_POWERUP_TICK )
		BREAK
		DEFAULT
			DRAW_GENERIC_SCORE(Team2Member3Status, Team2Member3Name, DEFAULT, DEFAULT, HUDORDER_THIRDBOTTOM, TRUE, NumberString, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TeamTwoColour)
		BREAK
	ENDSWITCH
	
	SWITCH Team2Member4Status 
		CASE -3 //NONE
			
			
		BREAK
		CASE -2 //DEAD
			
			DRAW_GENERIC_SCORE(Team2Member4Status, Team2Member4Name, DEFAULT, DEFAULT,  HUDORDER_SECONDBOTTOM, TRUE, NumberString, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TeamTwoColour, DEFAULT, DEFAULT, DEFAULT, ACTIVITY_POWERUP_CROSS )
		BREAK
		CASE -1 //CROSSED
			
			DRAW_GENERIC_SCORE(Team2Member4Status, Team2Member4Name, DEFAULT, DEFAULT,  HUDORDER_SECONDBOTTOM, TRUE, NumberString, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TeamTwoColour, DEFAULT, DEFAULT, DEFAULT, ACTIVITY_POWERUP_TICK )
		BREAK
		DEFAULT
			DRAW_GENERIC_SCORE(Team2Member4Status, Team2Member4Name, DEFAULT, DEFAULT, HUDORDER_SECONDBOTTOM, TRUE, NumberString, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TeamTwoColour)
		BREAK
	ENDSWITCH	
		
	
	
	
	STRING LocalModelName = "HUD_COUNTDOWN"
	IF NOT IS_STRING_EMPTY_HUD(ModeName)
		LocalModelName = ModeName
	ENDIF
	IF Event_Timer_Display != -1
		DRAW_GENERIC_TIMER(Event_Timer_Display, LocalModelName, DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUDORDER_BOTTOM, DEFAULT, EventTimerColour, DEFAULT, DEFAULT, DEFAULT, EventTimerColour)
	ENDIF
			
	
ENDPROC



//Smash and Grab


PROC BOTTOM_RIGHT_UI_1STCASH_2NDCASH_3RDCASH_CASH1_CASH2_TIMER(PLAYER_INDEX First_Player_Index, 
																PLAYER_INDEX Second_Player_Index, 
																PLAYER_INDEX Third_Player_Index, 
																INT First_Player_Cash, 
																INT Second_Player_Cash, 
																INT Third_Player_Cash, 
																INT Cash1,
																INT Cash2,
																INT Event_Timer_Display,			
																BOOL& bForceRebuildNames, 
																HUD_COLOURS EventTimerColour = HUD_COLOUR_WHITE,
																STRING ModeName = NULL,
																STRING Cash1Title = NULL,
																STRING Cash2Title = NULL,
																HUD_COLOURS PlayerOneFleckColour = HUD_COLOUR_PURE_WHITE,
																HUD_COLOURS PlayerTwoFleckColour = HUD_COLOUR_PURE_WHITE,
																HUD_COLOURS PlayerThreeFleckColour = HUD_COLOUR_PURE_WHITE)
	IF SHOULD_DISPLAY_BOTTOM_RIGHT_UI_EVENTS() = FALSE
		EXIT
	ENDIF					
	HIDE_BOTTOM_RIGHT_CODE_UI()
					
	#IF IS_DEBUG_BUILD
		IF g_DisplayTimersDisplaying
			DEBUG_PRINTCALLSTACK()
			PRINTLN("[BCEVENTTIMERS] BOTTOM_RIGHT_UI_1STINT_2NDINT_3RDINT_KILLED_TIMER - called ")
		ENDIF
	#ENDIF																	
																	
									
	STRING PlayerName								
	BOOL RebuildStrings = FALSE
	
	IF EventHudPlayerIndexLastFrame[0] != First_Player_Index
	OR EventHudPlayerIndexLastFrame[1] != Second_Player_Index
	OR EventHudPlayerIndexLastFrame[2] != Third_Player_Index
	OR bForceRebuildNames
		RebuildStrings = TRUE
	ENDIF
	
	EventHudPlayerIndexLastFrame[0] = First_Player_Index
	EventHudPlayerIndexLastFrame[1] = Second_Player_Index
	EventHudPlayerIndexLastFrame[2] = Third_Player_Index
	EventHudPlayerIndexLastFrame[3] = NULL
	EventHudPlayerIndexLastFrame[4] = NULL

	IF First_Player_Cash > 99999
	OR Second_Player_Cash > 99999
	OR Third_Player_Cash > 99999
	OR Cash1 > 99999
	OR Cash2 > 99999
		SET_FAR_RIGHT_TITLE_POSITION_HUD_THIS_FRAME()

	ELIF First_Player_Cash > 999
	OR Second_Player_Cash > 999
	OR Third_Player_Cash > 999
	OR Cash1 > 999
	OR Cash2 > 999
		SET_MIDDLE_TITLE_POSITION_HUD_THIS_FRAME()
	ENDIF
		
	IF EventHudPlayerIndexLastFrame[0] != INVALID_PLAYER_INDEX()	
		IF RebuildStrings = TRUE
		
			PlayerName = GET_PLAYER_NAME(EventHudPlayerIndexLastFrame[0])
			EventHudPlayerNameLastFrame[0] = PARSE_PLACEMENT_GAMERTAGS(DISPLAY_EVENT_PLACEMENT_FIRST, PlayerName)
		ENDIF
		IF First_Player_Cash > 0
			DRAW_GENERIC_SCORE(First_Player_Cash, EventHudPlayerNameLastFrame[0], DEFAULT, HUD_COLOUR_GOLD, HUDORDER_SIXTHBOTTOM, TRUE, "HUD_CASH", DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUD_COLOUR_GOLD,
								DEFAULT, DEFAULT, DEFAULT, DEFAULT,DEFAULT,DEFAULT,PlayerOneFleckColour)
		ENDIF
	ENDIF	
	
	
	IF EventHudPlayerIndexLastFrame[1] != INVALID_PLAYER_INDEX()
		IF RebuildStrings = TRUE
			PlayerName = GET_PLAYER_NAME(EventHudPlayerIndexLastFrame[1])
			EventHudPlayerNameLastFrame[1] = PARSE_PLACEMENT_GAMERTAGS(DISPLAY_EVENT_PLACEMENT_SECOND, PlayerName)
		ENDIF
		IF Second_Player_Cash > 0
			DRAW_GENERIC_SCORE(Second_Player_Cash, EventHudPlayerNameLastFrame[1], DEFAULT, HUD_COLOUR_SILVER, HUDORDER_FIFTHBOTTOM, TRUE, "HUD_CASH", DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUD_COLOUR_SILVER,
								DEFAULT, DEFAULT, DEFAULT, DEFAULT,DEFAULT,DEFAULT,PlayerTwoFleckColour)
		ENDIF
	ENDIF


	IF EventHudPlayerIndexLastFrame[2] != INVALID_PLAYER_INDEX()
		IF RebuildStrings = TRUE
			PlayerName = GET_PLAYER_NAME(EventHudPlayerIndexLastFrame[2])
			EventHudPlayerNameLastFrame[2] = PARSE_PLACEMENT_GAMERTAGS(DISPLAY_EVENT_PLACEMENT_THIRD, PlayerName)
		ENDIF
		IF Third_Player_Cash > 0
			DRAW_GENERIC_SCORE(Third_Player_Cash, EventHudPlayerNameLastFrame[2], DEFAULT, HUD_COLOUR_BRONZE, HUDORDER_FOURTHBOTTOM, TRUE, "HUD_CASH", DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUD_COLOUR_BRONZE,
								DEFAULT, DEFAULT, DEFAULT, DEFAULT,DEFAULT,DEFAULT,PlayerThreeFleckColour)
		ENDIF
	ENDIF
	
	
	STRING LocalModelName = "GB_SNG_HT1" //CASH DELIVERED
	IF NOT IS_STRING_EMPTY_HUD(Cash1Title)
		LocalModelName = Cash1Title
	ENDIF
	IF Cash1 > -1
		DRAW_GENERIC_SCORE(Cash1, LocalModelName, DEFAULT, DEFAULT, HUDORDER_THIRDBOTTOM, FALSE, "HUD_CASH")
	ENDIF
	
	LocalModelName = "GB_SNG_HT0" //CASH HELD 
	IF NOT IS_STRING_EMPTY_HUD(Cash2Title)
		LocalModelName = Cash2Title
	ENDIF
	IF Cash2 > -1
		DRAW_GENERIC_SCORE(Cash2, LocalModelName, DEFAULT, DEFAULT, HUDORDER_SECONDBOTTOM, FALSE, "HUD_CASH")
	ENDIF
	
	LocalModelName = "GB_SNG_TM0" //CHALLENGE END
	IF NOT IS_STRING_EMPTY_HUD(ModeName)
		LocalModelName = ModeName
	ENDIF
	DRAW_GENERIC_TIMER(Event_Timer_Display, LocalModelName, DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUDORDER_BOTTOM, DEFAULT, EventTimerColour, DEFAULT, DEFAULT, DEFAULT, EventTimerColour)
			
	bForceRebuildNames = FALSE
	
ENDPROC


//Carjacking

PROC BOTTOM_RIGHT_UI_1STCASH_2NDCASH_3RDCASH_4THCASH_CASH1_CASH2_TIMER(PLAYER_INDEX First_Player_Index, 
																PLAYER_INDEX Second_Player_Index, 
																PLAYER_INDEX Third_Player_Index, 
																PLAYER_INDEX Fourth_Player_Index, 
																INT First_Player_Cash, 
																INT Second_Player_Cash, 
																INT Third_Player_Cash, 
																INT Fourth_Player_Cash, 
																INT Cash1,
																INT Cash2,
																INT Event_Timer_Display,			
																BOOL& bForceRebuildNames, 
																HUD_COLOURS EventTimerColour = HUD_COLOUR_WHITE,
																STRING ModeName = NULL,
																STRING Cash1Title = NULL,
																STRING Cash2Title = NULL,
																HUD_COLOURS PlayerOneFleckColour = HUD_COLOUR_PURE_WHITE,
																HUD_COLOURS PlayerTwoFleckColour = HUD_COLOUR_PURE_WHITE,
																HUD_COLOURS PlayerThreeFleckColour = HUD_COLOUR_PURE_WHITE,
																HUD_COLOURS PlayerFourFleckColour = HUD_COLOUR_PURE_WHITE,
																BOOL bAllowZeroScores = FALSE,
																HUD_COLOURS Cash1Colour = HUD_COLOUR_WHITE,
																HUD_COLOURS Cash2Colour = HUD_COLOUR_WHITE)
	IF SHOULD_DISPLAY_BOTTOM_RIGHT_UI_EVENTS() = FALSE
		EXIT
	ENDIF					
	HIDE_BOTTOM_RIGHT_CODE_UI()
					
	#IF IS_DEBUG_BUILD
		IF g_DisplayTimersDisplaying
			DEBUG_PRINTCALLSTACK()
			PRINTLN("[BCEVENTTIMERS] BOTTOM_RIGHT_UI_1STINT_2NDINT_3RDINT_KILLED_TIMER - called ")
		ENDIF
	#ENDIF																	
																	
									
	STRING PlayerName								
	BOOL RebuildStrings = FALSE
	
	IF EventHudPlayerIndexLastFrame[0] != First_Player_Index
	OR EventHudPlayerIndexLastFrame[1] != Second_Player_Index
	OR EventHudPlayerIndexLastFrame[2] != Third_Player_Index
	OR EventHudPlayerIndexLastFrame[3] != Fourth_Player_Index
	OR bForceRebuildNames
		RebuildStrings = TRUE
	ENDIF
	
	EventHudPlayerIndexLastFrame[0] = First_Player_Index
	EventHudPlayerIndexLastFrame[1] = Second_Player_Index
	EventHudPlayerIndexLastFrame[2] = Third_Player_Index
	EventHudPlayerIndexLastFrame[3] = Fourth_Player_Index
	EventHudPlayerIndexLastFrame[4] = NULL

	IF First_Player_Cash > 99999
	OR Second_Player_Cash > 99999
	OR Third_Player_Cash > 99999
	OR Fourth_Player_Cash > 99999
	OR Cash1 > 99999
	OR Cash2 > 99999
		SET_FAR_RIGHT_TITLE_POSITION_HUD_THIS_FRAME()

	ELIF First_Player_Cash > 999
	OR Second_Player_Cash > 999
	OR Third_Player_Cash > 999
	OR Fourth_Player_Cash > 999
	OR Cash1 > 999
	OR Cash2 > 999
		SET_MIDDLE_TITLE_POSITION_HUD_THIS_FRAME()
	ENDIF
		
	IF EventHudPlayerIndexLastFrame[0] != INVALID_PLAYER_INDEX()	
		IF RebuildStrings = TRUE
		
			PlayerName = GET_PLAYER_NAME(EventHudPlayerIndexLastFrame[0])
			EventHudPlayerNameLastFrame[0] = PARSE_PLACEMENT_GAMERTAGS(DISPLAY_EVENT_PLACEMENT_FIRST, PlayerName)
		ENDIF
		IF First_Player_Cash > 0
		OR (First_Player_Cash = 0 AND bAllowZeroScores)
			DRAW_GENERIC_SCORE(First_Player_Cash, EventHudPlayerNameLastFrame[0], DEFAULT, HUD_COLOUR_GOLD, HUDORDER_SEVENTHBOTTOM, TRUE, "HUD_CASH", DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUD_COLOUR_GOLD,
								DEFAULT, DEFAULT, DEFAULT, DEFAULT,DEFAULT,DEFAULT,PlayerOneFleckColour)
		ENDIF
	ENDIF	
	
	
	IF EventHudPlayerIndexLastFrame[1] != INVALID_PLAYER_INDEX()
		IF RebuildStrings = TRUE
			PlayerName = GET_PLAYER_NAME(EventHudPlayerIndexLastFrame[1])
			EventHudPlayerNameLastFrame[1] = PARSE_PLACEMENT_GAMERTAGS(DISPLAY_EVENT_PLACEMENT_SECOND, PlayerName)
		ENDIF
		IF Second_Player_Cash > 0
		OR (Second_Player_Cash = 0 AND bAllowZeroScores)
			DRAW_GENERIC_SCORE(Second_Player_Cash, EventHudPlayerNameLastFrame[1], DEFAULT, HUD_COLOUR_SILVER, HUDORDER_SIXTHBOTTOM, TRUE, "HUD_CASH", DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUD_COLOUR_SILVER,
								DEFAULT, DEFAULT, DEFAULT, DEFAULT,DEFAULT,DEFAULT,PlayerTwoFleckColour)
		ENDIF
	ENDIF


	IF EventHudPlayerIndexLastFrame[2] != INVALID_PLAYER_INDEX()
		IF RebuildStrings = TRUE
			PlayerName = GET_PLAYER_NAME(EventHudPlayerIndexLastFrame[2])
			EventHudPlayerNameLastFrame[2] = PARSE_PLACEMENT_GAMERTAGS(DISPLAY_EVENT_PLACEMENT_THIRD, PlayerName)
		ENDIF
		IF Third_Player_Cash > 0
		OR (Third_Player_Cash = 0 AND bAllowZeroScores)
			DRAW_GENERIC_SCORE(Third_Player_Cash, EventHudPlayerNameLastFrame[2], DEFAULT, HUD_COLOUR_BRONZE, HUDORDER_FIFTHBOTTOM, TRUE, "HUD_CASH", DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUD_COLOUR_BRONZE,
								DEFAULT, DEFAULT, DEFAULT, DEFAULT,DEFAULT,DEFAULT,PlayerThreeFleckColour)
		ENDIF
	ENDIF
	
	
	IF EventHudPlayerIndexLastFrame[3] != INVALID_PLAYER_INDEX()
		IF RebuildStrings = TRUE
			PlayerName = GET_PLAYER_NAME(EventHudPlayerIndexLastFrame[3])
			EventHudPlayerNameLastFrame[3] = PARSE_PLACEMENT_GAMERTAGS(DISPLAY_EVENT_PLACEMENT_FOURTH, PlayerName)
		ENDIF
		IF Fourth_Player_Cash > 0
		OR (Fourth_Player_Cash = 0 AND bAllowZeroScores)
			DRAW_GENERIC_SCORE(Fourth_Player_Cash, EventHudPlayerNameLastFrame[3], DEFAULT, HUD_COLOUR_GREYLIGHT, HUDORDER_FOURTHBOTTOM, TRUE, "HUD_CASH", DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUD_COLOUR_GREYLIGHT,
								DEFAULT, DEFAULT, DEFAULT, DEFAULT,DEFAULT,DEFAULT,PlayerFourFleckColour)
		ENDIF
	ENDIF
	
	
	STRING LocalModelName = "GB_SNG_HT1" //CASH DELIVERED
	IF NOT IS_STRING_EMPTY_HUD(Cash1Title)
		LocalModelName = Cash1Title
	ENDIF
	IF Cash1 > -1
		DRAW_GENERIC_SCORE(Cash1, LocalModelName, DEFAULT, Cash1Colour, HUDORDER_THIRDBOTTOM, FALSE, "HUD_CASH")
	ENDIF
	
	LocalModelName = "GB_SNG_HT0" //CASH HELD 
	IF NOT IS_STRING_EMPTY_HUD(Cash2Title)
		LocalModelName = Cash2Title
	ENDIF
	IF Cash2 > -1
		DRAW_GENERIC_SCORE(Cash2, LocalModelName, DEFAULT, Cash2Colour, HUDORDER_SECONDBOTTOM, FALSE, "HUD_CASH")
	ENDIF
	
	LocalModelName = "GB_SNG_TM0" //CHALLENGE END
	IF NOT IS_STRING_EMPTY_HUD(ModeName)
		LocalModelName = ModeName
	ENDIF
	DRAW_GENERIC_TIMER(Event_Timer_Display, LocalModelName, DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUDORDER_BOTTOM, DEFAULT, EventTimerColour, DEFAULT, DEFAULT, DEFAULT, EventTimerColour)
			
	bForceRebuildNames = FALSE
	
ENDPROC




PROC BOTTOM_RIGHT_UI_1STTIME_2NDTIME_3RDTIME_4THTIME_TIMER(PLAYER_INDEX First_Player_Index, 
																PLAYER_INDEX Second_Player_Index, 
																PLAYER_INDEX Third_Player_Index, 
																PLAYER_INDEX Fourth_Player_Index,
																INT First_Player_Time, 
																INT Second_Player_Time, 
																INT Third_Player_Time, 
																INT Fourth_Player_Time, 
																INT Event_Timer_Display,			
																BOOL& bForceRebuildNames, 
																HUD_COLOURS EventTimerColour = HUD_COLOUR_WHITE,
																STRING ModeName = NULL,
																HUD_COLOURS PlayerOneFleckColour = HUD_COLOUR_PURE_WHITE,
																HUD_COLOURS PlayerTwoFleckColour = HUD_COLOUR_PURE_WHITE,
																HUD_COLOURS PlayerThreeFleckColour = HUD_COLOUR_PURE_WHITE,
																HUD_COLOURS PlayerFourFleckColour = HUD_COLOUR_PURE_WHITE)
																
						
	IF SHOULD_DISPLAY_BOTTOM_RIGHT_UI_EVENTS() = FALSE
		EXIT
	ENDIF															
	HIDE_BOTTOM_RIGHT_CODE_UI()
	
										
	#IF IS_DEBUG_BUILD
		IF g_DisplayTimersDisplaying
			DEBUG_PRINTCALLSTACK()
			PRINTLN("[BCEVENTTIMERS] BOTTOM_RIGHT_UI_1STTIME_2NDTIME_3RDTIME_DAMAGE_TIMER - called ")
		ENDIF
	#ENDIF											
										
	STRING PlayerName								
	BOOL RebuildStrings = FALSE
	
	IF EventHudPlayerIndexLastFrame[0] != First_Player_Index
	OR EventHudPlayerIndexLastFrame[1] != Second_Player_Index
	OR EventHudPlayerIndexLastFrame[2] != Third_Player_Index
	OR EventHudPlayerIndexLastFrame[3] != Fourth_Player_Index
	OR bForceRebuildNames
		RebuildStrings = TRUE
	ENDIF
	
	EventHudPlayerIndexLastFrame[0] = First_Player_Index
	EventHudPlayerIndexLastFrame[1] = Second_Player_Index
	EventHudPlayerIndexLastFrame[2] = Third_Player_Index
	EventHudPlayerIndexLastFrame[3] = Fourth_Player_Index
	EventHudPlayerIndexLastFrame[4] = NULL


		
	IF EventHudPlayerIndexLastFrame[0] != INVALID_PLAYER_INDEX()	
		IF RebuildStrings = TRUE
		
			PlayerName = GET_PLAYER_NAME(EventHudPlayerIndexLastFrame[0])
			EventHudPlayerNameLastFrame[0] = PARSE_PLACEMENT_GAMERTAGS(DISPLAY_EVENT_PLACEMENT_FIRST, PlayerName)
		ENDIF
		IF First_Player_Time > 0
			DRAW_GENERIC_TIMER(First_Player_Time, EventHudPlayerNameLastFrame[0], DEFAULT, DEFAULT,DEFAULT,DEFAULT,HUDORDER_SEVENTHBOTTOM,TRUE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT,DEFAULT, PlayerOneFleckColour)
		ENDIF
	ENDIF	
	
	
	IF EventHudPlayerIndexLastFrame[1] != INVALID_PLAYER_INDEX()
		IF RebuildStrings = TRUE
			PlayerName = GET_PLAYER_NAME(EventHudPlayerIndexLastFrame[1])
			EventHudPlayerNameLastFrame[1] = PARSE_PLACEMENT_GAMERTAGS(DISPLAY_EVENT_PLACEMENT_SECOND, PlayerName)
		ENDIF
		IF Second_Player_Time > 0
			DRAW_GENERIC_TIMER(Second_Player_Time, EventHudPlayerNameLastFrame[1], DEFAULT, DEFAULT,DEFAULT,DEFAULT,HUDORDER_SIXTHBOTTOM,TRUE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, PlayerTwoFleckColour)	
		ENDIF
	ENDIF


	IF EventHudPlayerIndexLastFrame[2] != INVALID_PLAYER_INDEX()
		IF RebuildStrings = TRUE
			PlayerName = GET_PLAYER_NAME(EventHudPlayerIndexLastFrame[2])
			EventHudPlayerNameLastFrame[2] = PARSE_PLACEMENT_GAMERTAGS(DISPLAY_EVENT_PLACEMENT_THIRD, PlayerName)
		ENDIF
		IF Third_Player_Time > 0
			DRAW_GENERIC_TIMER(Third_Player_Time, EventHudPlayerNameLastFrame[2], DEFAULT, DEFAULT,DEFAULT,DEFAULT,HUDORDER_FIFTHBOTTOM,TRUE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, PlayerThreeFleckColour)		
		ENDIF
	ENDIF
	
	IF EventHudPlayerIndexLastFrame[3] != INVALID_PLAYER_INDEX()
		IF RebuildStrings = TRUE
			PlayerName = GET_PLAYER_NAME(EventHudPlayerIndexLastFrame[3])
			EventHudPlayerNameLastFrame[3] = PARSE_PLACEMENT_GAMERTAGS(DISPLAY_EVENT_PLACEMENT_FOURTH, PlayerName)
		ENDIF
		IF Fourth_Player_Time > 0
			DRAW_GENERIC_TIMER(Fourth_Player_Time, EventHudPlayerNameLastFrame[3], DEFAULT, DEFAULT,DEFAULT,DEFAULT,HUDORDER_FOURTHBOTTOM,TRUE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, PlayerFourFleckColour)		
		ENDIF
	ENDIF
		
	
	
	STRING LocalModelName = "HUD_COUNTDOWN"
	IF NOT IS_STRING_EMPTY_HUD(ModeName)
		LocalModelName = ModeName
	ENDIF
	DRAW_GENERIC_TIMER(Event_Timer_Display, LocalModelName, DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUDORDER_BOTTOM, DEFAULT, EventTimerColour, DEFAULT, DEFAULT, DEFAULT, EventTimerColour)
			
	bForceRebuildNames = FALSE
	
	SET_FAR_RIGHT_TITLE_POSITION_HUD_THIS_FRAME()	
	
ENDPROC



//Most Wanted

PROC BOTTOM_RIGHT_UI_1TIME_2TIME_3TIME_4TIME_TIMER(PLAYER_INDEX First_Player_Index, 
																PLAYER_INDEX Second_Player_Index, 
																PLAYER_INDEX Third_Player_Index, 
																PLAYER_INDEX Fourth_Player_Index,
																INT First_Player_Time, 
																INT Second_Player_Time, 
																INT Third_Player_Time, 
																INT Fourth_Player_Time, 
																BOOL First_Player_Eliminated, 
																BOOL Second_Player_Eliminated, 
																BOOL Third_Player_Eliminated, 
																BOOL Fourth_Player_Eliminated, 
																INT Event_Timer_Display,			
																BOOL& bForceRebuildNames, 
																HUD_COLOURS EventTimerColour = HUD_COLOUR_WHITE,
																STRING ModeName = NULL,
																HUD_COLOURS PlayerOneFleckColour = HUD_COLOUR_PURE_WHITE,
																HUD_COLOURS PlayerTwoFleckColour = HUD_COLOUR_PURE_WHITE,
																HUD_COLOURS PlayerThreeFleckColour = HUD_COLOUR_PURE_WHITE,
																HUD_COLOURS PlayerFourFleckColour = HUD_COLOUR_PURE_WHITE,
																BOOL bSpectating = FALSE)
																
						
	IF SHOULD_DISPLAY_BOTTOM_RIGHT_UI_EVENTS(bSpectating) = FALSE
		EXIT
	ENDIF															
	HIDE_BOTTOM_RIGHT_CODE_UI()
	
										
	#IF IS_DEBUG_BUILD
		IF g_DisplayTimersDisplaying
			DEBUG_PRINTCALLSTACK()
			PRINTLN("[BCEVENTTIMERS] BOTTOM_RIGHT_UI_1STTIME_2NDTIME_3RDTIME_DAMAGE_TIMER - called ")
		ENDIF
	#ENDIF											
										
	STRING PlayerName								
	BOOL RebuildStrings = FALSE
	
	IF EventHudPlayerIndexLastFrame[0] != First_Player_Index
	OR EventHudPlayerIndexLastFrame[1] != Second_Player_Index
	OR EventHudPlayerIndexLastFrame[2] != Third_Player_Index
	OR EventHudPlayerIndexLastFrame[3] != Fourth_Player_Index
	OR bForceRebuildNames
		RebuildStrings = TRUE
	ENDIF
	
	EventHudPlayerIndexLastFrame[0] = First_Player_Index
	EventHudPlayerIndexLastFrame[1] = Second_Player_Index
	EventHudPlayerIndexLastFrame[2] = Third_Player_Index
	EventHudPlayerIndexLastFrame[3] = Fourth_Player_Index
	EventHudPlayerIndexLastFrame[4] = NULL

	HUD_COLOURS FirstPlayerColour = HUD_COLOUR_WHITE
	HUD_COLOURS FirstPlayerTitleColour = HUD_COLOUR_PURE_WHITE
	IF First_Player_Eliminated
		FirstPlayerColour = HUD_COLOUR_GREY
		FirstPlayerTitleColour = HUD_COLOUR_GREYDARK
	ENDIF	

		
	IF EventHudPlayerIndexLastFrame[0] != INVALID_PLAYER_INDEX()	
		IF RebuildStrings = TRUE
		
			PlayerName = GET_PLAYER_NAME(EventHudPlayerIndexLastFrame[0])
			EventHudPlayerNameLastFrame[0] = PARSE_PLACEMENT_GAMERTAGS(DISPLAY_EVENT_PLACEMENT_NAMEONLY, PlayerName)
		ENDIF
		IF First_Player_Time > 0
			DRAW_GENERIC_TIMER(First_Player_Time, EventHudPlayerNameLastFrame[0], DEFAULT, DEFAULT,DEFAULT,DEFAULT,HUDORDER_SEVENTHBOTTOM,TRUE, FirstPlayerColour, DEFAULT, DEFAULT, DEFAULT, FirstPlayerTitleColour, DEFAULT, PlayerOneFleckColour)	
		ENDIF
	ENDIF	
	
	HUD_COLOURS SecondPlayerColour = HUD_COLOUR_WHITE
	HUD_COLOURS SecondPlayerTitleColour = HUD_COLOUR_PURE_WHITE
	IF Second_Player_Eliminated
		SecondPlayerColour = HUD_COLOUR_GREY
		SecondPlayerTitleColour = HUD_COLOUR_GREYDARK
	ENDIF	
	
	IF EventHudPlayerIndexLastFrame[1] != INVALID_PLAYER_INDEX()
		IF RebuildStrings = TRUE
			PlayerName = GET_PLAYER_NAME(EventHudPlayerIndexLastFrame[1])
			EventHudPlayerNameLastFrame[1] = PARSE_PLACEMENT_GAMERTAGS(DISPLAY_EVENT_PLACEMENT_NAMEONLY, PlayerName)
		ENDIF
		IF Second_Player_Time > 0
			DRAW_GENERIC_TIMER(Second_Player_Time, EventHudPlayerNameLastFrame[1], DEFAULT, DEFAULT,DEFAULT,DEFAULT,HUDORDER_SIXTHBOTTOM,TRUE, SecondPlayerColour, DEFAULT, DEFAULT, DEFAULT, SecondPlayerTitleColour, DEFAULT, PlayerTwoFleckColour)	
		ENDIF
	ENDIF

	HUD_COLOURS ThirdPlayerColour = HUD_COLOUR_WHITE
	HUD_COLOURS ThirdPlayerTitleColour = HUD_COLOUR_PURE_WHITE
	IF Third_Player_Eliminated
		ThirdPlayerColour = HUD_COLOUR_GREY
		ThirdPlayerTitleColour = HUD_COLOUR_GREYDARK
	ENDIF

	IF EventHudPlayerIndexLastFrame[2] != INVALID_PLAYER_INDEX()
		IF RebuildStrings = TRUE
			PlayerName = GET_PLAYER_NAME(EventHudPlayerIndexLastFrame[2])
			EventHudPlayerNameLastFrame[2] = PARSE_PLACEMENT_GAMERTAGS(DISPLAY_EVENT_PLACEMENT_NAMEONLY, PlayerName)
		ENDIF
		IF Third_Player_Time > 0
			DRAW_GENERIC_TIMER(Third_Player_Time, EventHudPlayerNameLastFrame[2], DEFAULT, DEFAULT,DEFAULT,DEFAULT,HUDORDER_FIFTHBOTTOM,TRUE, ThirdPlayerColour, DEFAULT, DEFAULT, DEFAULT, ThirdPlayerTitleColour, DEFAULT, PlayerThreeFleckColour)		
		ENDIF
	ENDIF
	
	HUD_COLOURS FourthPlayerColour = HUD_COLOUR_WHITE
	HUD_COLOURS FourthPlayerTitleColour = HUD_COLOUR_PURE_WHITE
	IF Fourth_Player_Eliminated
		FourthPlayerColour = HUD_COLOUR_GREY
		FourthPlayerTitleColour = HUD_COLOUR_GREYDARK
	ENDIF
	
	IF EventHudPlayerIndexLastFrame[3] != INVALID_PLAYER_INDEX()
		IF RebuildStrings = TRUE
			PlayerName = GET_PLAYER_NAME(EventHudPlayerIndexLastFrame[3])
			EventHudPlayerNameLastFrame[3] = PARSE_PLACEMENT_GAMERTAGS(DISPLAY_EVENT_PLACEMENT_NAMEONLY, PlayerName)
		ENDIF
		IF Fourth_Player_Time > 0
			DRAW_GENERIC_TIMER(Fourth_Player_Time, EventHudPlayerNameLastFrame[3], DEFAULT, DEFAULT,DEFAULT,DEFAULT,HUDORDER_FOURTHBOTTOM,TRUE, FourthPlayerColour, DEFAULT, DEFAULT, DEFAULT, FourthPlayerTitleColour, DEFAULT, PlayerFourFleckColour)		
		ENDIF
	ENDIF
		
	
	
	STRING LocalModelName = "HUD_COUNTDOWN"
	IF NOT IS_STRING_EMPTY_HUD(ModeName)
		LocalModelName = ModeName
	ENDIF
	DRAW_GENERIC_TIMER(Event_Timer_Display, LocalModelName, DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUDORDER_BOTTOM, DEFAULT, EventTimerColour, DEFAULT, DEFAULT, DEFAULT, EventTimerColour)
			
	bForceRebuildNames = FALSE
	
	SET_FAR_RIGHT_TITLE_POSITION_HUD_THIS_FRAME()	
	
ENDPROC


//Finders Keepers

PROC BOTTOM_RIGHT_UI_1STINT_2NDINT_3RDINT_4THINT_LOCALINT_TIMER(PLAYER_INDEX First_Player_Index, 
																PLAYER_INDEX Second_Player_Index, 
																PLAYER_INDEX Third_Player_Index, 
																PLAYER_INDEX Fourth_Player_Index, 
																INT First_Player_Score, 
																INT Second_Player_Score, 
																INT Third_Player_Score,
																INT Fourth_Player_Score,
																INT iLocal_Score_Display, 
																INT Event_Timer_Display,			
																BOOL& bForceRebuildNames, 
																HUD_COLOURS EventTimerColour = HUD_COLOUR_WHITE,
																STRING ModeName = NULL,
																FMEVENT_SCORETITLE LocalDisplayType = FMEVENT_SCORETITLE_YOUR_SCORE, 
																HUD_COLOURS PlayerOneFleckColour = HUD_COLOUR_PURE_WHITE,
																HUD_COLOURS PlayerTwoFleckColour = HUD_COLOUR_PURE_WHITE,
																HUD_COLOURS PlayerThreeFleckColour = HUD_COLOUR_PURE_WHITE,
																HUD_COLOURS PlayerFourFleckColour = HUD_COLOUR_PURE_WHITE)
						
	IF SHOULD_DISPLAY_BOTTOM_RIGHT_UI_EVENTS() = FALSE
		EXIT
	ENDIF															
	HIDE_BOTTOM_RIGHT_CODE_UI()
				
	#IF IS_DEBUG_BUILD
		IF g_DisplayTimersDisplaying
			DEBUG_PRINTCALLSTACK()
			PRINTLN("[BCEVENTTIMERS] BOTTOM_RIGHT_UI_1STINT_2NDINT_3RDINT_4THINT_LOCALINT_TIMER - called ")
		ENDIF
	#ENDIF											
										
	STRING PlayerName								
	BOOL RebuildStrings = FALSE
	
	IF EventHudPlayerIndexLastFrame[0] != First_Player_Index
	OR EventHudPlayerIndexLastFrame[1] != Second_Player_Index
	OR EventHudPlayerIndexLastFrame[2] != Third_Player_Index
	OR EventHudPlayerIndexLastFrame[3] != Fourth_Player_Index
	OR bForceRebuildNames
		RebuildStrings = TRUE
	ENDIF
	
	EventHudPlayerIndexLastFrame[0] = First_Player_Index
	EventHudPlayerIndexLastFrame[1] = Second_Player_Index
	EventHudPlayerIndexLastFrame[2] = Third_Player_Index
	EventHudPlayerIndexLastFrame[3] = Fourth_Player_Index
	EventHudPlayerIndexLastFrame[4] = NULL


		
	IF EventHudPlayerIndexLastFrame[0] != INVALID_PLAYER_INDEX()	
		IF RebuildStrings = TRUE
		
			PlayerName = GET_PLAYER_NAME(EventHudPlayerIndexLastFrame[0])
			EventHudPlayerNameLastFrame[0] = PARSE_PLACEMENT_GAMERTAGS(DISPLAY_EVENT_PLACEMENT_FIRST, PlayerName)
		ENDIF
		IF First_Player_Score > 0
			DRAW_GENERIC_SCORE(First_Player_Score, EventHudPlayerNameLastFrame[0], DEFAULT, HUD_COLOUR_GOLD, HUDORDER_SIXTHBOTTOM, TRUE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUD_COLOUR_GOLD,
								DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, PlayerOneFleckColour)		
		ENDIF
	ENDIF	
	
	
	IF EventHudPlayerIndexLastFrame[1] != INVALID_PLAYER_INDEX()
		IF RebuildStrings = TRUE
			PlayerName = GET_PLAYER_NAME(EventHudPlayerIndexLastFrame[1])
			EventHudPlayerNameLastFrame[1] = PARSE_PLACEMENT_GAMERTAGS(DISPLAY_EVENT_PLACEMENT_SECOND, PlayerName)
		ENDIF
		IF Second_Player_Score > 0
			DRAW_GENERIC_SCORE(Second_Player_Score, EventHudPlayerNameLastFrame[1], DEFAULT, HUD_COLOUR_SILVER, HUDORDER_FIFTHBOTTOM, TRUE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUD_COLOUR_SILVER,
								DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, PlayerTwoFleckColour)	
		ENDIF
	ENDIF


	IF EventHudPlayerIndexLastFrame[2] != INVALID_PLAYER_INDEX()
		IF RebuildStrings = TRUE
			PlayerName = GET_PLAYER_NAME(EventHudPlayerIndexLastFrame[2])
			EventHudPlayerNameLastFrame[2] = PARSE_PLACEMENT_GAMERTAGS(DISPLAY_EVENT_PLACEMENT_THIRD, PlayerName)
		ENDIF
		IF Third_Player_Score > 0
			DRAW_GENERIC_SCORE(Third_Player_Score, EventHudPlayerNameLastFrame[2], DEFAULT, HUD_COLOUR_BRONZE, HUDORDER_FOURTHBOTTOM, TRUE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUD_COLOUR_BRONZE,
								DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, PlayerThreeFleckColour)	
		ENDIF
	ENDIF
	
	IF EventHudPlayerIndexLastFrame[3] != INVALID_PLAYER_INDEX()
		IF RebuildStrings = TRUE
			PlayerName = GET_PLAYER_NAME(EventHudPlayerIndexLastFrame[3])
			EventHudPlayerNameLastFrame[3] = PARSE_PLACEMENT_GAMERTAGS(DISPLAY_EVENT_PLACEMENT_FOURTH, PlayerName)
		ENDIF
		IF Fourth_Player_Score > 0
			DRAW_GENERIC_SCORE(Fourth_Player_Score, EventHudPlayerNameLastFrame[3], DEFAULT, HUD_COLOUR_GREYLIGHT , HUDORDER_THIRDBOTTOM, TRUE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUD_COLOUR_GREYLIGHT,
								DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, PlayerFourFleckColour)	
		ENDIF
	ENDIF
		
	SWITCH LocalDisplayType 
	
		CASE FMEVENT_SCORETITLE_GAMERTAG
			IF PLAYER_ID() != INVALID_PLAYER_INDEX()
				IF FM_EVENT_IS_PLAYER_RESTRICTED_WITH_PASSIVE(PLAYER_ID()) = FALSE
					DRAW_GENERIC_SCORE(iLocal_Score_Display, GET_PLAYER_NAME(PLAYER_ID()), DEFAULT, DEFAULT, HUDORDER_SECONDBOTTOM, TRUE)
				ENDIF
			ENDIF
		BREAK
		
		CASE FMEVENT_SCORETITLE_YOUR_SCORE
			IF FM_EVENT_IS_PLAYER_RESTRICTED_WITH_PASSIVE(PLAYER_ID()) = FALSE
				DRAW_GENERIC_SCORE(iLocal_Score_Display,"HUD_USCORE", DEFAULT, DEFAULT, HUDORDER_SECONDBOTTOM)
			ENDIF
		BREAK
		
		CASE FMEVENT_SCORETITLE_YOUR_BEST
			IF FM_EVENT_IS_PLAYER_RESTRICTED_WITH_PASSIVE(PLAYER_ID()) = FALSE
				DRAW_GENERIC_SCORE(iLocal_Score_Display, "HUD_UBEST", DEFAULT, DEFAULT, HUDORDER_SECONDBOTTOM)
			ENDIF
		BREAK
		
		CASE FMEVENT_SCORETITLE_NONE
		//nothing
		BREAK
	ENDSWITCH	

	
	
	STRING LocalModelName = "HUD_COUNTDOWN"
	IF NOT IS_STRING_EMPTY_HUD(ModeName)
		LocalModelName = ModeName
	ENDIF
	DRAW_GENERIC_TIMER(Event_Timer_Display, LocalModelName, DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUDORDER_BOTTOM, DEFAULT, EventTimerColour, DEFAULT, DEFAULT, DEFAULT, EventTimerColour)
			
	bForceRebuildNames = FALSE
	
ENDPROC







PROC BOTTOM_RIGHT_UI_1TIME_2TIME_3TIME_4TIME_MYTIME_TIMER(PLAYER_INDEX First_Player_Index, 
																PLAYER_INDEX Second_Player_Index, 
																PLAYER_INDEX Third_Player_Index, 
																PLAYER_INDEX Fourth_Player_Index,
																INT First_Player_Time, 
																INT Second_Player_Time, 
																INT Third_Player_Time, 
																INT Fourth_Player_Time, 
																INT Personal_Timer_Display,	
																INT Event_Timer_Display,			
																BOOL& bForceRebuildNames, 
																HUD_COLOURS PersonalTimerColour = HUD_COLOUR_WHITE,
																HUD_COLOURS EventTimerColour = HUD_COLOUR_WHITE,
																STRING PersonalTimerName = NULL,
																STRING ModeName = NULL,
																HUD_COLOURS PlayerOneFleckColour = HUD_COLOUR_PURE_WHITE,
																HUD_COLOURS PlayerTwoFleckColour = HUD_COLOUR_PURE_WHITE,
																HUD_COLOURS PlayerThreeFleckColour = HUD_COLOUR_PURE_WHITE,
																HUD_COLOURS PlayerFourFleckColour = HUD_COLOUR_PURE_WHITE,
																BOOL bSpectating = FALSE)
																
						
	IF SHOULD_DISPLAY_BOTTOM_RIGHT_UI_EVENTS(bSpectating) = FALSE
		EXIT
	ENDIF															
	HIDE_BOTTOM_RIGHT_CODE_UI()
	
										
	#IF IS_DEBUG_BUILD
		IF g_DisplayTimersDisplaying
			DEBUG_PRINTCALLSTACK()
			PRINTLN("[BCEVENTTIMERS] BOTTOM_RIGHT_UI_1STTIME_2NDTIME_3RDTIME_DAMAGE_TIMER - called ")
		ENDIF
	#ENDIF											
										
	STRING PlayerName								
	BOOL RebuildStrings = FALSE
	
	IF EventHudPlayerIndexLastFrame[0] != First_Player_Index
	OR EventHudPlayerIndexLastFrame[1] != Second_Player_Index
	OR EventHudPlayerIndexLastFrame[2] != Third_Player_Index
	OR EventHudPlayerIndexLastFrame[3] != Fourth_Player_Index
	OR bForceRebuildNames
		RebuildStrings = TRUE
	ENDIF
	
	EventHudPlayerIndexLastFrame[0] = First_Player_Index
	EventHudPlayerIndexLastFrame[1] = Second_Player_Index
	EventHudPlayerIndexLastFrame[2] = Third_Player_Index
	EventHudPlayerIndexLastFrame[3] = Fourth_Player_Index
	EventHudPlayerIndexLastFrame[4] = NULL

		
	IF EventHudPlayerIndexLastFrame[0] != INVALID_PLAYER_INDEX()	
		IF RebuildStrings = TRUE
		
			PlayerName = GET_PLAYER_NAME(EventHudPlayerIndexLastFrame[0])
			EventHudPlayerNameLastFrame[0] = PARSE_PLACEMENT_GAMERTAGS(DISPLAY_EVENT_PLACEMENT_FIRST, PlayerName)
		ENDIF
		IF First_Player_Time > 0
			DRAW_GENERIC_TIMER(First_Player_Time, EventHudPlayerNameLastFrame[0], DEFAULT, DEFAULT,DEFAULT,DEFAULT,HUDORDER_SIXTHBOTTOM,TRUE, HUD_COLOUR_GOLD, DEFAULT, DEFAULT, DEFAULT, HUD_COLOUR_GOLD, DEFAULT, PlayerOneFleckColour)	
		ENDIF
	ENDIF	
	
	IF EventHudPlayerIndexLastFrame[1] != INVALID_PLAYER_INDEX()
		IF RebuildStrings = TRUE
			PlayerName = GET_PLAYER_NAME(EventHudPlayerIndexLastFrame[1])
			EventHudPlayerNameLastFrame[1] = PARSE_PLACEMENT_GAMERTAGS(DISPLAY_EVENT_PLACEMENT_SECOND, PlayerName)
		ENDIF
		IF Second_Player_Time > 0
			DRAW_GENERIC_TIMER(Second_Player_Time, EventHudPlayerNameLastFrame[1], DEFAULT, DEFAULT,DEFAULT,DEFAULT,HUDORDER_FIFTHBOTTOM,TRUE, HUD_COLOUR_SILVER, DEFAULT, DEFAULT, DEFAULT, HUD_COLOUR_SILVER, DEFAULT, PlayerTwoFleckColour)	
		ENDIF
	ENDIF

	IF EventHudPlayerIndexLastFrame[2] != INVALID_PLAYER_INDEX()
		IF RebuildStrings = TRUE
			PlayerName = GET_PLAYER_NAME(EventHudPlayerIndexLastFrame[2])
			EventHudPlayerNameLastFrame[2] = PARSE_PLACEMENT_GAMERTAGS(DISPLAY_EVENT_PLACEMENT_THIRD, PlayerName)
		ENDIF
		IF Third_Player_Time > 0
			DRAW_GENERIC_TIMER(Third_Player_Time, EventHudPlayerNameLastFrame[2], DEFAULT, DEFAULT,DEFAULT,DEFAULT,HUDORDER_FOURTHBOTTOM,TRUE, HUD_COLOUR_BRONZE, DEFAULT, DEFAULT, DEFAULT, HUD_COLOUR_BRONZE, DEFAULT, PlayerThreeFleckColour)		
		ENDIF
	ENDIF
	
	
	IF EventHudPlayerIndexLastFrame[3] != INVALID_PLAYER_INDEX()
		IF RebuildStrings = TRUE
			PlayerName = GET_PLAYER_NAME(EventHudPlayerIndexLastFrame[3])
			EventHudPlayerNameLastFrame[3] = PARSE_PLACEMENT_GAMERTAGS(DISPLAY_EVENT_PLACEMENT_FOURTH, PlayerName)
		ENDIF
		IF Fourth_Player_Time > 0
			DRAW_GENERIC_TIMER(Fourth_Player_Time, EventHudPlayerNameLastFrame[3], DEFAULT, DEFAULT,DEFAULT,DEFAULT,HUDORDER_THIRDBOTTOM,TRUE, HUD_COLOUR_GREYLIGHT, DEFAULT, DEFAULT, DEFAULT, HUD_COLOUR_GREYLIGHT, DEFAULT, PlayerFourFleckColour)		
		ENDIF
	ENDIF
		
	STRING PersonalLocalModelName = "GB_TIME_SURV"
	IF NOT IS_STRING_EMPTY_HUD(PersonalTimerName)
		PersonalLocalModelName = PersonalTimerName
	ENDIF
	IF Personal_Timer_Display > -1
		DRAW_GENERIC_TIMER(Personal_Timer_Display, PersonalLocalModelName, DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUDORDER_SECONDBOTTOM, DEFAULT, PersonalTimerColour, DEFAULT, DEFAULT, DEFAULT, PersonalTimerColour)
	ENDIF	
	
	
	STRING LocalModelName = "HUD_COUNTDOWN"
	IF NOT IS_STRING_EMPTY_HUD(ModeName)
		LocalModelName = ModeName
	ENDIF
	DRAW_GENERIC_TIMER(Event_Timer_Display, LocalModelName, DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUDORDER_BOTTOM, DEFAULT, EventTimerColour, DEFAULT, DEFAULT, DEFAULT, EventTimerColour)
	
	
	bForceRebuildNames = FALSE
	
	SET_FAR_RIGHT_TITLE_POSITION_HUD_THIS_FRAME()	
	
ENDPROC



