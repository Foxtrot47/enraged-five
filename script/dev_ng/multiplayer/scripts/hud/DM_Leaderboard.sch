USING "leader_board_common.sch"
USING "freemode_events_header.sch"

PROC SET_FRIEND_DPAD(DPAD_VARS& dpadVars)
	IF NOT IS_BIT_SET(dpadVars.iDpadBitSet, ciDPAD_IS_FRIEND)
		SET_BIT(dpadVars.iDpadBitSet, ciDPAD_IS_FRIEND)
	ENDIF
ENDPROC

PROC SET_CREW_DPAD(DPAD_VARS& dpadVars)
	IF NOT IS_BIT_SET(dpadVars.iDpadBitSet, ciDPAD_IS_CREW)
		SET_BIT(dpadVars.iDpadBitSet, ciDPAD_IS_CREW)
	ENDIF
ENDPROC

PROC CLEAR_FRIEND_DPAD(DPAD_VARS& dpadVars)
	IF IS_BIT_SET(dpadVars.iDpadBitSet, ciDPAD_IS_FRIEND)
		CLEAR_BIT(dpadVars.iDpadBitSet, ciDPAD_IS_FRIEND)
	ENDIF
ENDPROC

PROC CLEAR_CREW_DPAD(DPAD_VARS& dpadVars)
	IF IS_BIT_SET(dpadVars.iDpadBitSet, ciDPAD_IS_CREW)
		CLEAR_BIT(dpadVars.iDpadBitSet, ciDPAD_IS_CREW)
	ENDIF
ENDPROC

#IF IS_DEBUG_BUILD
PROC DEBUG_DRAW_DPAD_LBD(SCALEFORM_INDEX& siMovie, LBD_SUB_MODE eSubMode, DPAD_VARS& dpadVars, FM_DPAD_LBD_STRUCT& fmDpadVars)

	PLAYER_INDEX PlayerId
	GAMER_HANDLE gamerH[NUM_NETWORK_PLAYERS]
	INT iPlayerCrew[NUM_NETWORK_PLAYERS]
	STRING strHeadshotTxd
	PEDHEADSHOT_ID pedHeadshot
	TEXT_LABEL_15 tlCrewTag
	INT iRank, iScore, iPlayer
		
	IF SHOULD_DPAD_LBD_DISPLAY(siMovie, eSubMode, dpadVars, g_i_NumDpadLbdPlayers)
	
		SET_SCRIPT_GFX_ALIGN(UI_ALIGN_LEFT, UI_ALIGN_TOP)
	
		IF dpadVars.iDrawProgress = DPAD_INIT
		
			g_i_NumDpadLbdPlayers = 0
		
			INT i
			INT iCount
			
			REPEAT NUM_NETWORK_PLAYERS i

				PlayerId = INT_TO_PLAYERINDEX(i)

				iPlayer = NATIVE_TO_INT(PlayerId)
			
				iRank = GlobalplayerBD_FM[iPlayer].scoreData.iRank
				iScore = GET_JOB_POINTS(PlayerId)
		
				// Grab crew tags
				tlCrewTag = GET_CREW_TAG_DPAD_LBD(PlayerId)
			
				gamerH[i] = GET_GAMER_HANDLE_PLAYER(playerId)
				iPlayerCrew[i] = GET_PLAYER_CREW_INT(gamerH[i])

				// Local player 
				IF PlayerId = PLAYER_ID()
					CLEAR_FRIEND_DPAD(dpadVars)
					CLEAR_CREW_DPAD(dpadVars)
				ELSE
					// Are friends or same crew?
					IF NETWORK_IS_FRIEND(gamerH[i])
						SET_FRIEND_DPAD(dpadVars)
					ELSE
						CLEAR_FRIEND_DPAD(dpadVars)
					ENDIF
					// Crew
					IF iPlayerCrew[i] = 0 // No crew returns 0
						CLEAR_CREW_DPAD(dpadVars)
					ELSE
						IF (iPlayerCrew[i] = iPlayerCrew[PARTICIPANT_ID_TO_INT()])
							SET_CREW_DPAD(dpadVars)
						ELSE
							CLEAR_CREW_DPAD(dpadVars)
						ENDIF
					ENDIF
				ENDIF
				
				dpadVars.tl63LeaderboardPlayerNames = GET_NAME_FOR_FAKE_LBD(i) 		// grab names
				fmDpadVars.aGamer[i] = GET_GAMER_HANDLE_PLAYER(playerId)				// store tags
				
				// Attempt to get player headshot
				pedHeadshot = Get_HeadshotID_For_Player(playerId)
				strHeadshotTxd = ""
				IF pedHeadshot <> NULL
					strHeadshotTxd = GET_PEDHEADSHOT_TXD_STRING(pedHeadshot)
					PRINTLN("[CS_DPAD] OK strHeadshotTxd = ", strHeadshotTxd)
				ENDIF
				
				PRINTLN("[CS_DPAD] iScore = ", iScore)
				
				g_i_NumDpadLbdPlayers++
				
				PRINTLN("[CS_DPAD] Freemode about to call POPULATE_NEW_FREEMODE_DPAD_ROWS with row = ", iCount, " player name = ", dpadVars.tl63LeaderboardPlayerNames, " iPlayer = ", iPlayer)

				POPULATE_NEW_FREEMODE_DPAD_ROWS(SUB_FREEMODE, siMovie, dpadVars, iCount, tlCrewTag, strHeadshotTxd, iRank, iScore, HUD_COLOUR_FREEMODE, DISPLAY_HEADSHOT, -1)

				iCount++
			ENDREPEAT
			
			PRINTLN("[CS_DPAD]  dpadVars.iDrawProgress = DPAD_DRAW")
		
			dpadVars.iDrawProgress = DPAD_DRAW
		ENDIF
		
		IF dpadVars.iDrawProgress = DPAD_DRAW
			
			IF NOT IS_BIT_SET(dpadVars.iDpadBitSet, ciDPAD_IS_UPDATED)
				DISPLAY_VIEW(siMovie)
				SET_BIT(dpadVars.iDpadBitSet, ciDPAD_IS_UPDATED)
			ENDIF
		
			IF HAS_SCALEFORM_MOVIE_LOADED(siMovie)
				SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_FADE)
					DRAW_SCALEFORM_MOVIE(siMovie, SDPAD_X, SDPAD_Y, SDPAD_W, SDPAD_H, 255, 255, 255, 255)
				SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
			ENDIF
		ENDIF
			
	ELSE
		dpadVars.iDrawProgress = DPAD_INIT
		RESET_DPAD_REFRESH()
	ENDIF
	
	RESET_SCRIPT_GFX_ALIGN()
ENDPROC
#ENDIF

FUNC BOOL IS_END_SHARD_ONSCREEN()

	
	IF IS_BIT_SET(MPGlobalsAmbience.iDpadBitSet, ciDPAD_BIT_HELP_DELAY_ACTIVE)
	AND IS_ANY_BIG_MESSAGE_BEING_DISPLAYED()

		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(MPGlobalsAmbience.iDpadBitSet, ciDPAD_BIT_POST_HELP_DELAY)
	AND IS_ANY_BIG_MESSAGE_BEING_DISPLAYED()

		RETURN TRUE
	ENDIF
	
	 //#IF FEATURE_NEW_AMBIENT_EVENTS
	
	RETURN FALSE
ENDFUNC

PROC CLEAR_END_SHARD()
	IF IS_BIT_SET(MPGlobalsAmbience.iDpadBitSet, ciDPAD_BIT_POST_HELP_DELAY)
		IF IS_ANY_BIG_MESSAGE_BEING_DISPLAYED()
			CLEAR_ALL_BIG_MESSAGES()
			PRINTLN("[CS_DPAD] DRAW_CHALLENGE_DPAD_LBD, ciDPAD_BIT_HELP_DELAY_ACTIVE, CLEAR_END_SHARD ")
		ENDIF
	ENDIF
ENDPROC

PROC SET_DRAWING_CHALLENGE_BOOL(BOOL bSet)
	IF bSet
		IF g_sDpadTypeToDraw.bDrawingChallenge = FALSE
			PRINTLN("[CS_DPAD] SET_DRAWING_CHALLENGE_BOOL, TRUE ")
			g_sDpadTypeToDraw.bDrawingChallenge = TRUE
		ENDIF
	ELSE
		IF g_sDpadTypeToDraw.bDrawingChallenge = TRUE
			PRINTLN("[CS_DPAD] SET_DRAWING_CHALLENGE_BOOL, FALSE ")
			g_sDpadTypeToDraw.bDrawingChallenge = FALSE
		ENDIF
	ENDIF
ENDPROC

#IF FEATURE_HEIST_ISLAND
FUNC BOOL DPAD_PASSED_ISLAND_CHECK(PLAYER_INDEX PlayerId)

	BOOL bLocalPlayerOnIsland =	IS_PLAYER_ON_HEIST_ISLAND(PLAYER_ID())
	BOOL bRemotePlayerOnIsland = IS_PLAYER_ON_HEIST_ISLAND(PlayerId)
	
	IF bLocalPlayerOnIsland AND !bRemotePlayerOnIsland
	OR !bLocalPlayerOnIsland AND bRemotePlayerOnIsland
		RETURN FALSE
	ENDIF

	RETURN TRUE
ENDFUNC
#ENDIF

FUNC BOOL PASSED_TUTORIAL_CHECK(PLAYER_INDEX PlayerId)

	IF NOT IS_NET_PLAYER_OK(PlayerId, FALSE)
		RETURN FALSE
	ENDIF
	
	IF NOT IS_NET_PLAYER_OK(PLAYER_ID(), FALSE)
		RETURN FALSE
	ENDIF
	
	IF NOT DPAD_PASSED_ISLAND_CHECK(PlayerId)
		RETURN FALSE
	ENDIF
	
	#IF FEATURE_GEN9_EXCLUSIVE
	IF IS_PLAYER_ON_MP_INTRO()
		RETURN PlayerId = PLAYER_ID()
	ENDIF
	#ENDIF
	
	IF NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PlayerId, PLAYER_ID())  
	OR (GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PlayerId) = FMMC_TYPE_FM_GANGOPS)  
	OR (GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PlayerId) = FMMC_TYPE_TUNER_ROBBERY)  
	
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL INSERT_PLAYER_INTO_PLAYER_LIST(PLAYER_INDEX PlayerId, BOOL bOnMPIntro)

	IF bOnMPIntro
		RETURN FALSE
	ENDIF
	
	#IF FEATURE_GEN9_EXCLUSIVE
	IF IS_REMOTE_PLAYER_ON_MP_INTRO(PlayerId)
		RETURN FALSE
	ENDIF
	#ENDIF
	
	RETURN ( ( GlobalplayerBD[NATIVE_TO_INT(PlayerId)].iCurrentShop <> -1 ) OR IS_THIS_PLAYER_IN_CORONA(PlayerId) )
ENDFUNC

PROC GB_FILL_GOON_ROWS(INT &iPlayerRows[], PLAYER_INDEX PlayerBoss, INT &iCurrentRow, INT &iNumGangstersTotal, BOOL bOnMPIntro)

	INT i
	PLAYER_INDEX PlayerId
	REPEAT NUM_NETWORK_PLAYERS i					
		IF IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(i), FALSE)
			PlayerId = INT_TO_PLAYERINDEX(i)
			IF NOT IS_PLAYER_SCTV(PlayerId)
				IF PASSED_TUTORIAL_CHECK(PlayerId)
				OR INSERT_PLAYER_INTO_PLAYER_LIST(PlayerId, bOnMPIntro)
				
					IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(PlayerId, PlayerBoss, FALSE)
					
						iPlayerRows[i] = iCurrentRow
						iCurrentRow++
						iNumGangstersTotal++
						
						#IF IS_DEBUG_BUILD
						PRINTLN("[CS_DPAD] GB_FILL_GOON_ROWS, PlayerId = ", GET_PLAYER_NAME(PlayerId), " iPlayerRows = ", iPlayerRows[i])
						#ENDIF
						
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
ENDPROC

//FUNC HUD_COLOURS GB_GET_DPAD_GANG_ROW_COLOUR(PLAYER_INDEX playerToGetColourFor, BOOL bIgnoreFMEvents)
//
//	HUD_COLOURS hcReturn
//	
//	hcReturn = HUD_COLOUR_FREEMODE
//
//	IF NOT bIgnoreFMEvents
//	AND FM_EVENT_IS_PLAYER_ON_ANY_FM_EVENT(playerToGetColourFor)
//	
//	#IF FEATURE_GANG_BOSS
//	AND NOT GB_IS_PLAYER_PERMANENT_PARTICIPANT(playerToGetColourFor)
//	#ENDIF
//	
//		hcReturn = GET_HUD_COLOUR_FOR_FREEMODE_EVENT_TYPE()
//	ENDIF
//
//	#IF FEATURE_GANG_BOSS
//	// check if this player is in a gang
//	INT iGangID = GET_GANG_ID_FOR_PLAYER(playerToGetColourFor)
//	
//	IF NOT (iGangID = -1)
//
//		// if not same gang then use assigned hud color for that gang. 				
//		RETURN GET_HUD_COLOUR_FOR_GANG_ID( iGangID )
//	ENDIF
//	#ENDIF
//	
//	RETURN hcReturn
//ENDFUNC

PROC DO_THE_MICS(DPAD_VARS& dpadVars, SCALEFORM_INDEX& siMovie, LBD_SUB_MODE eSubMode)

	INT i, iIcon
	PLAYER_INDEX PlayerId
	
	REPEAT NUM_NETWORK_PLAYERS i
		PlayerId = INT_TO_PLAYERINDEX(i)
		IF PlayerId <> INVALID_PLAYER_INDEX()
			IF IS_NET_PLAYER_OK(playerId, FALSE)
				IF dpadVars.sDpadMics[i].iRow <> -1
					iIcon = GET_DPAD_PLAYER_VOICE_ICON(dpadVars.sDpadMics[i].iVoiceChatState, DEFAULT, eSubMode)
					SET_PLAYER_MIC(siMovie, dpadVars.sDpadMics[i].iRow, iIcon, GlobalplayerBD_FM[i].scoreData.iRank)
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC DRAW_FREEMODE_DPAD_LBD(SCALEFORM_INDEX& siMovie, LBD_SUB_MODE eSubMode, DPAD_VARS& dpadVars, FM_DPAD_LBD_STRUCT& fmDpadVars)

	PLAYER_INDEX PlayerId
	GAMER_HANDLE gamerH[NUM_NETWORK_PLAYERS]
	INT iPlayerCrew[NUM_NETWORK_PLAYERS]
	STRING strHeadshotTxd
	PEDHEADSHOT_ID pedHeadshot
	TEXT_LABEL_15 tlCrewTag
	INT iRank, iScore, iPlayer, i, iRow
	HUD_COLOURS hcColourRow
	hcColourRow = HUD_COLOUR_FREEMODE
	
	INT iNumGangstersTotal
	#IF IS_DEBUG_BUILD
	INT iNumGangs
	#ENDIF
	INT iStoredRow[NUM_NETWORK_PLAYERS]
	BOOL bDO_BOSS_LEADERBOARD
	
	BOOL bOnMPIntro
	#IF FEATURE_GEN9_EXCLUSIVE
	bOnMPIntro = IS_PLAYER_ON_MP_INTRO()
	#ENDIF
	
	IF SHOULD_DPAD_LBD_DISPLAY(siMovie, eSubMode, dpadVars, g_i_NumDpadLbdPlayers)
	
		SET_DPAD_DRAW_TYPE(eDPAD_TYPE_FM)
		
		IF ( NOT IS_ANY_FM_EVENTS_BIG_MESSAGE_BEING_DISPLAYED()	AND NOT IS_ANY_GB_BIG_MESSAGE_BEING_DISPLAYED() )
		OR IS_BIT_SET(MPGlobalsAmbience.iDpadBitSet, ciDPAD_BIT_POST_HELP_DELAY)
	
			IF IS_END_SHARD_ONSCREEN()
			
				CLEAR_END_SHARD()
			ELSE
		
				SET_SCRIPT_GFX_ALIGN(UI_ALIGN_LEFT, UI_ALIGN_TOP)
			
				IF dpadVars.iDrawProgress = DPAD_INIT
				
					g_i_NumDpadLbdPlayers = 0
					
					PRINTLN("[CS_DPAD] [FM_TYPE]  dpadVars.iDrawProgress = DPAD_POPULATE")
					
					// Clear our current scaleform movie
					IF g_sDpadTypeToDraw.eDpadTypeToDrawLastFrame != eDPAD_TYPE_FM
					OR g_sDpadTypeToDraw.bDrawingChallenge
						PRINTLN("[CS_DPAD] [FM_TYPE]  DRAW_FREEMODE_DPAD_LBD, (g_sDpadTypeToDraw.eDpadTypeToDrawLastFrame != eDPAD_TYPE_FM): SCALEFORM_SET_DATA_SLOT_EMPTY called.")
						SCALEFORM_SET_DATA_SLOT_EMPTY(siMovie)
						SET_DRAWING_CHALLENGE_BOOL(FALSE)
					ENDIF
				
					
					INT iCurrentRow
					
					REPEAT NUM_NETWORK_PLAYERS i
						iStoredRow[i] = -1
					ENDREPEAT
					
					REPEAT NUM_NETWORK_PLAYERS i					
						IF IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(i), FALSE)
							PlayerId = INT_TO_PLAYERINDEX(i)
							IF NOT IS_PLAYER_SCTV(PlayerId)
								IF PASSED_TUTORIAL_CHECK(PlayerId)
								OR INSERT_PLAYER_INTO_PLAYER_LIST(PlayerId, bOnMPIntro)
								
									iPlayer = NATIVE_TO_INT(PlayerId)
									
									IF GB_IS_PLAYER_BOSS_OF_A_GANG(PlayerId)
									
										#IF IS_DEBUG_BUILD
										PRINTLN("[CS_DPAD] [FM_TYPE] DRAW_FREEMODE_DPAD_LBD, GB_IS_PLAYER_BOSS_OF_A_GANG = ", GET_PLAYER_NAME(PlayerId), " iCurrentRow = ", iCurrentRow)
										iNumGangs++
										#ENDIF
										
										iStoredRow[i] = iCurrentRow
										
										iCurrentRow++
										
										iNumGangstersTotal++
										
										GB_FILL_GOON_ROWS(iStoredRow, PlayerId, iCurrentRow, iNumGangstersTotal, bOnMPIntro)
									ENDIF	
								ENDIF
							ENDIF
						ENDIF
					ENDREPEAT
					
					bDO_BOSS_LEADERBOARD = ( iNumGangstersTotal > 0 )
					IF bOnMPIntro
						bDO_BOSS_LEADERBOARD = FALSE
					ENDIF
					
					
					INT iNumberOfFMPlayers
					
					#IF IS_DEBUG_BUILD
					PRINTLN("[CS_DPAD] [FM_TYPE] [magnate] iNumGangstersTotal = ", iNumGangstersTotal, " iNumGangs = ", iNumGangs)
					#ENDIF //#IF IS_DEBUG_BUILD
					
					 // #IF FEATURE_GANG_BOSS
					
					REPEAT NUM_NETWORK_PLAYERS i
						IF IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(i), FALSE)
							PlayerId = INT_TO_PLAYERINDEX(i)
							IF NOT IS_PLAYER_SCTV(PlayerId)
								IF PASSED_TUTORIAL_CHECK(PlayerId)
								OR INSERT_PLAYER_INTO_PLAYER_LIST(PlayerId, bOnMPIntro)

									iPlayer = NATIVE_TO_INT(PlayerId)
								
									iRank = GlobalplayerBD_FM[iPlayer].scoreData.iRank
									iScore = GET_JOB_POINTS(PlayerId)
									
									PRINTLN("[2226937] i = ", i, " iRank = ", iRank)
							
									// Grab crew tags
									tlCrewTag = GET_CREW_TAG_DPAD_LBD(PlayerId)
									
									gamerH[i] = GET_GAMER_HANDLE_PLAYER(playerId)
									iPlayerCrew[i] = GET_PLAYER_CREW_INT(gamerH[i])
															
		//							PRINTLN("CREW_TEST i = ", i, " iPlayerCrew = ", iPlayerCrew[i])
								
									// Local player 
									IF PlayerId = PLAYER_ID()
//										PRINTLN("[CS_DPAD] [FM_TYPE] - PlayerId = PLAYER_ID()")
										CLEAR_FRIEND_DPAD(dpadVars)
										CLEAR_CREW_DPAD(dpadVars)
										
										dpadVars.iPlayersRow = iRow
									ELSE
										// Are friends or same crew?
										IF NETWORK_IS_FRIEND(gamerH[i])
//											PRINTLN("[CS_DPAD] [FM_TYPE] - NETWORK_IS_FRIEND(TRUE)")
											SET_FRIEND_DPAD(dpadVars)
										ELSE
//											PRINTLN("[CS_DPAD] [FM_TYPE] - NETWORK_IS_FRIEND(FALSE)")
											CLEAR_FRIEND_DPAD(dpadVars)
										ENDIF
										// Crew
										IF iPlayerCrew[i] = 0 // No crew returns 0
										OR NOT IS_PLAYER_IN_ACTIVE_CLAN(gamerH[i])
//											PRINTLN("[CS_DPAD] [FM_TYPE] - iPlayerCrew[i] = 0")
											CLEAR_CREW_DPAD(dpadVars)
										ELSE
											IF (iPlayerCrew[i] = iPlayerCrew[PARTICIPANT_ID_TO_INT()])
//												PRINTLN("[CS_DPAD] [FM_TYPE] - iPlayerCrew[i] = ", iPlayerCrew[i], "; iPlayerCrew[PARTICIPANT_ID_TO_INT()] = ", iPlayerCrew[PARTICIPANT_ID_TO_INT()], " SET_CREW_DPAD")
												SET_CREW_DPAD(dpadVars)
											ELSE
//												PRINTLN("[CS_DPAD] [FM_TYPE] - iPlayerCrew[i] = ", iPlayerCrew[i], "; iPlayerCrew[PARTICIPANT_ID_TO_INT()] = ", iPlayerCrew[PARTICIPANT_ID_TO_INT()], " CLEAR_CREW_DPAD")
												CLEAR_CREW_DPAD(dpadVars)
											ENDIF
										ENDIF
									ENDIF
									
									dpadVars.tl63LeaderboardPlayerNames = GET_PLAYER_NAME(PlayerId) 		// grab names
									fmDpadVars.aGamer[i] = GET_GAMER_HANDLE_PLAYER(playerId)					// store tags
									
									PRINTLN("[CS_DPAD] [FM_TYPE] dpadVars.tl63LeaderboardPlayerNames = ", dpadVars.tl63LeaderboardPlayerNames, " i = ", i)
//									IF IS_GAMER_HANDLE_VALID(fmDpadVars.aGamer[i])
//										PRINTLN("[CS_DPAD] [FM_TYPE] IS_GAMER_HANDLE_VALID,  iPlayer = ", i)
//									ENDIF
									
									// Attempt to get player headshot
									pedHeadshot = Get_HeadshotID_For_Player(playerId)
									strHeadshotTxd = ""
									IF pedHeadshot <> NULL
										
										strHeadshotTxd = GET_PEDHEADSHOT_TXD_STRING(pedHeadshot)
										PRINTLN("[CS_DPAD] [FM_TYPE] OK strHeadshotTxd = ", strHeadshotTxd)
									ELSE
										PRINTLN("[CS_DPAD] [FM_TYPE] NULL strHeadshotTxd = ", strHeadshotTxd)
									ENDIF
									
//									PRINTLN("[CS_DPAD] [FM_TYPE] iScore = ", iScore)
									
									g_i_NumDpadLbdPlayers++
									
									PRINTLN("[CS_DPAD] [FM_TYPE] POPULATE_NEW_FREEMODE_DPAD_ROWS with row = ", iRow, " player name = ", dpadVars.tl63LeaderboardPlayerNames, " iPlayer = ", iPlayer, " g_i_NumDpadLbdPlayers = ", g_i_NumDpadLbdPlayers)

									hcColourRow = GB_GET_DPAD_GANG_ROW_COLOUR(PlayerId, TRUE)
									
									#IF IS_DEBUG_BUILD
									INT iGangID = GET_GANG_ID_FOR_PLAYER(PlayerId)
									PRINTLN("7500803, PlayerId ", GET_PLAYER_NAME(PlayerId), " hcColourRow = ", HUD_COLOUR_AS_STRING(hcColourRow), " iGangID = ", iGangID)
									#ENDIF
	
									IF bDO_BOSS_LEADERBOARD
									
										IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PlayerId)
										AND iStoredRow[i] <> -1
	
											iRow = iStoredRow[i]
										ELSE
											iRow = (iNumGangstersTotal + iNumberOfFMPlayers)
											iNumberOfFMPlayers++
										ENDIF	
									ENDIF
									
									dpadVars.sDpadMics[iPlayer].iRow = iRow
									
									#IF IS_DEBUG_BUILD
									PRINTLN("[CS_DPAD] [FM_TYPE] POPULATE_NEW_FREEMODE_DPAD_ROWS with row ADJUSTED = ", iRow, " player name = ", dpadVars.tl63LeaderboardPlayerNames, " iPlayer = ", iPlayer, " bDO_BOSS_LEADERBOARD = ", bDO_BOSS_LEADERBOARD)
									#ENDIF

									POPULATE_NEW_FREEMODE_DPAD_ROWS(SUB_FREEMODE, siMovie, dpadVars, iRow, tlCrewTag, strHeadshotTxd, iRank, iScore, hcColourRow, DISPLAY_HEADSHOT, -1, DEFAULT, DEFAULT, DEFAULT, DEFAULT, iPlayer)
									
									iRow++
								ENDIF
							ENDIF
						ENDIF
					ENDREPEAT
					
					// Speaker chat icons
					IF IS_BIT_SET(dpadVars.iDpadBitSet, ciDPAD_DISPLAY_VIEW_DONE_ONCE)
						DO_THE_MICS(dpadVars, siMovie, eSubMode)
					ENDIF
					
					PRINTLN("[CS_DPAD] [FM_TYPE]  dpadVars.iDrawProgress = DPAD_DRAW")
				
					dpadVars.iDrawProgress = DPAD_DRAW
				ENDIF
				
				IF dpadVars.iDrawProgress = DPAD_DRAW
					
					IF NOT IS_BIT_SET(dpadVars.iDpadBitSet, ciDPAD_IS_UPDATED)
						HIGHLIGHT_LOCAL_PLAYER(dpadVars, siMovie)
						DISPLAY_VIEW(siMovie)
						SET_BIT(dpadVars.iDpadBitSet, ciDPAD_IS_UPDATED)
					ENDIF
					
					HIGHLIGHT_LOCAL_PLAYER(dpadVars, siMovie)
					
					IF NOT IS_BIT_SET(dpadVars.iDpadBitSet, ciDPAD_DISPLAY_VIEW_DONE_ONCE)
						SET_BIT(dpadVars.iDpadBitSet, ciDPAD_DISPLAY_VIEW_DONE_ONCE)
					ENDIF
					
					IF MAINTAIN_DPAD_VOICE_CHAT(dpadVars)
					
						PRINTLN("[CS_DPAD] [FM_TYPE]  MAINTAIN_DPAD_VOICE_CHAT, dpadVars.iDrawProgress = DPAD_INIT")
					
						dpadVars.iDrawProgress = DPAD_INIT
					ENDIF
					
					IF g_RefreshDpad = REFRESH_DPAD_UPDATE
						PRINTLN("[CS_DPAD] [FM_TYPE]  DRAW_FREEMODE_DPAD_LBD, REFRESH_DPAD_UPDATE, dpadVars.iDrawProgress = DPAD_INIT")
						dpadVars.iDrawProgress = DPAD_INIT
					ENDIF
					
					IF g_RefreshDpad = REFRESH_DPAD_REPOPULATE
						PRINTLN("[CS_DPAD] [FM_TYPE]  DRAW_FREEMODE_DPAD_LBD, REFRESH_DPAD_REPOPULATE, dpadVars.iDrawProgress = DPAD_INIT")
						dpadVars.iDrawProgress = DPAD_INIT
					ENDIF
				ENDIF
				
				// Draw
				IF HAS_SCALEFORM_MOVIE_LOADED(siMovie)
					SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_FADE)
						DRAW_SCALEFORM_MOVIE(siMovie, SDPAD_X, SDPAD_Y, SDPAD_W, SDPAD_H, 255, 255, 255, 255)
					SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
				ENDIF
			ENDIF	
		ENDIF
	ELSE
		dpadVars.iDrawProgress = DPAD_INIT
		RESET_DPAD_REFRESH()
		
		IF IS_BIT_SET(dpadVars.iDpadBitSet, ciDPAD_IS_UPDATED)
			CLEAR_BIT(dpadVars.iDpadBitSet, ciDPAD_IS_UPDATED)
		ENDIF
		
		IF IS_BIT_SET(dpadVars.iDpadBitSet, ciDPAD_ROW_HIGHLIGHT)
			CLEAR_BIT(dpadVars.iDpadBitSet, ciDPAD_ROW_HIGHLIGHT)
		ENDIF
	ENDIF
	
	RESET_SCRIPT_GFX_ALIGN()
ENDPROC


FUNC BOOL DRAW_CHALLENGE_DPAD_FOR_DEV_SPECTATOR()	

	IF IS_PLAYER_DEV_SPECTATOR(PLAYER_ID())
	
		RETURN g_s_DevSpectateStruct.b_DevSpectateLbd
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_PRINT_LEADERBOARD_HELP()

	SWITCH FM_EVENT_GET_PLAYER_CURRENT_FM_EVENT(PLAYER_ID())
		CASE FMMC_TYPE_DEAD_DROP
//		CASE FMMC_TYPE_AIR_DROP	
		CASE FMMC_TYPE_TIME_TRIAL	
		CASE FMMC_TYPE_MOVING_TARGET
		CASE FMMC_TYPE_HOLD_THE_WHEEL	
		CASE FMMC_TYPE_HUNT_THE_BEAST	
		CASE FMMC_TYPE_RC_TIME_TRIAL

			RETURN  FALSE
	ENDSWITCH
	
	RETURN TRUE
	
ENDFUNC

PROC HANDLE_CHALLENGE_LBD_PROMPT(INT &iLocalHelpCounter)
	INT iGlobalHelpCounter = GET_PACKED_STAT_INT(PACKED_MP_AMBIENT_EVENT_LBD_HELP_COUNTER)
	STRING sLBDHelp = "AMEV_LBD_HELP"
	
	IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sLBDHelp)
	AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
	AND NOT IS_BIT_SET(MPGlobalsAmbience.iTaxiBitSet, biG_PlayerUsingTaxi)
		IF SHOULD_PRINT_LEADERBOARD_HELP()
			IF !IS_RADAR_HIDDEN()
			AND !IS_BIT_SET(MPGlobalsAmbience.iTaxiBitSet, biG_PlayerUsingTaxi)
			AND IS_NET_PLAYER_OK(PLAYER_ID(), TRUE, TRUE)
			AND !g_bBrowserVisible 
			AND !g_bInATM 
				IF iGlobalHelpCounter < 3
					IF iLocalHelpCounter = iGlobalHelpCounter
						PRINT_HELP_NO_SOUND(sLBDHelp, DEFAULT_HELP_TEXT_TIME_FM_EVENT_INITIAL)
						SET_FREEMODE_EVENT_HELP_BACKGROUND()
						iGlobalHelpCounter++
						SET_PACKED_STAT_INT(PACKED_MP_AMBIENT_EVENT_LBD_HELP_COUNTER, iGlobalHelpCounter)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

// Add challenge to here if you want a Challenge leaderboard to be drawn as opposed the generic Freemode one
FUNC BOOL SHOULD_DRAW_CHALLENGE_DPAD_LBD()

	IF DRAW_CHALLENGE_DPAD_FOR_DEV_SPECTATOR()
	
		RETURN TRUE
	ENDIF

	IF FM_EVENT_IS_PLAYER_RESTRICTED_WITH_HIDE_OPTION(PLAYER_ID())	
	
		RETURN FALSE
	ENDIF
	
	// 2400375
	IF SHOULD_FORCE_DRAW_CHALLENGE_DPAD_LBD()
	
		PRINTLN("[DM_LEADERBOARD] - SHOULD_DRAW_CHALLENGE_DPAD_LBD - SHOULD_FORCE_DRAW_CHALLENGE_DPAD_LBD = true")
		RETURN TRUE
	ENDIF
	
	IF FM_EVENT_IS_PLAYER_ON_ANY_FM_EVENT(PLAYER_ID())

		SWITCH FM_EVENT_GET_PLAYER_CURRENT_FM_EVENT(PLAYER_ID())
		
			CASE FMMC_TYPE_MOVING_TARGET						
			CASE FMMC_TYPE_CHECKPOINTS			
			CASE FMMC_TYPE_CHALLENGES				
			//CASE FMMC_TYPE_AIR_DROP	
			CASE FMMC_TYPE_PENNED_IN						
		//	CASE FMMC_TYPE_MTA								
			CASE FMMC_TYPE_HOLD_THE_WHEEL						
			CASE FMMC_TYPE_HOT_PROPERTY												
			CASE FMMC_TYPE_KING_CASTLE						
	//		CASE FMMC_TYPE_INFECTION						
			CASE FMMC_TYPE_CRIMINAL_DAMAGE			
			
			CASE FMMC_TYPE_HUNT_THE_BEAST
				PRINTLN("[DM_LEADERBOARD] - SHOULD_DRAW_CHALLENGE_DPAD_LBD - Player on ambient event = true")
				RETURN TRUE
			BREAK
			
			CASE FMMC_TYPE_DEAD_DROP
				IF NOT FM_EVENT_IS_PLAYER_WAITING_FOR_FM_EVENT_TO_START(PLAYER_ID())
					PRINTLN("[DM_LEADERBOARD] - SHOULD_DRAW_CHALLENGE_DPAD_LBD - Player not waiting for FMMC_TYPE_DEAD_DROP")
					//-- Just draw normal LB until the event starts
					RETURN TRUE
				ENDIF
			BREAK
			
			CASE FMMC_TYPE_KILL_LIST
				IF NOT FM_EVENT_IS_PLAYER_WAITING_FOR_FM_EVENT_TO_START(PLAYER_ID())
					PRINTLN("[DM_LEADERBOARD] - SHOULD_DRAW_CHALLENGE_DPAD_LBD - Player not waiting for UW")
					//-- Just draw normal LB until the event starts
					RETURN TRUE
				ENDIF
			BREAK
			
			CASE FMMC_TYPE_OFFROAD_RACE	
				IF NOT FM_EVENT_IS_PLAYER_WAITING_FOR_FM_EVENT_TO_START(PLAYER_ID())
					PRINTLN("[DM_LEADERBOARD] - SHOULD_DRAW_CHALLENGE_DPAD_LBD - Player not waiting for OFFROAD RACE")
					//-- Just draw normal LB until the event starts
					RETURN TRUE
				ENDIF
			BREAK
			
			CASE FMMC_TYPE_STOP_THE_STUNT
				RETURN TRUE
			BREAK
		ENDSWITCH
	ENDIF

	//PRINTLN("[DM_LEADERBOARD] - SHOULD_DRAW_CHALLENGE_DPAD_LBD - RETURN FALSE")
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_PLAYER_PASSED_FM_EVENT_DPAD_CHECKS(PLAYER_INDEX PlayerId, LBD_SUB_MODE eSubMode, BOOL bOnMPIntro)
	
	IF eSubMode = SUB_CHALLENGE
		IF FM_EVENT_HAS_PLAYER_HAS_BLOCKED_CURRENT_FM_EVENT(PlayerId)
		OR FM_EVENT_IS_PLAYER_BLOCKED_FROM_JOB_DUE_TO_PASSIVE(PlayerId)
		OR FM_EVENT_IS_PLAYER_RESTRICTED_WITH_HIDE_OPTION(PlayerId)
			
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF NOT HAS_PLAYER_COMPLETED_INITIAL_AMBIENT_TUTORIALS(PlayerId)
	
		RETURN FALSE
	ENDIF
		
	IF NOT PASSED_TUTORIAL_CHECK(PlayerId)
	AND NOT INSERT_PLAYER_INTO_PLAYER_LIST(PlayerId, bOnMPIntro)
	
		RETURN FALSE
	ENDIF
		
	RETURN TRUE
ENDFUNC

FUNC BOOL HAS_PLAYER_PASSED_DPAD_CHECKS(PLAYER_INDEX PlayerId)

	INT iPlayer

	IF PlayerId = INVALID_PLAYER_INDEX()
		PRINTLN("[ST / CS LBD DEBUG] - HAS_PLAYER_PASSED_DPAD_CHECKS - FALSE - Player ID is invalid. ")
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_SCTV(PlayerId)
		PRINTLN("[ST / CS LBD DEBUG] - HAS_PLAYER_PASSED_DPAD_CHECKS - FALSE - Player ", GET_PLAYER_NAME(PlayerId), " is SCTV")
		RETURN FALSE
	ENDIF
	iPlayer = NATIVE_TO_INT(PlayerId)
	
	IF iPlayer <> -1
		IF IS_BIT_SET(GlobalplayerBD_FM[iPlayer].iFmTutProgBitset, biTrigTut_PlayerRunningActivityTutorial)
			PRINTLN("[ST / CS LBD DEBUG] - HAS_PLAYER_PASSED_DPAD_CHECKS - FALSE - IS_BIT_SET(GlobalplayerBD_FM[iPlayer].iFmTutProgBitset, biTrigTut_PlayerRunningActivityTutorial) set for player ", GET_PLAYER_NAME(PlayerId))
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC


// See 2588193, we don't want two calls to the same function from Boss Work/FM events
FUNC BOOL SHOULD_BAIL_FROM_DPAD_CALL(LBD_SUB_MODE eSubMode)

	#IF FEATURE_GEN9_EXCLUSIVE
	IF IS_PLAYER_ON_MP_INTRO()
		RETURN TRUE
	ENDIF
	#ENDIF	

	IF eSubMode = SUB_BOSS	
	
		IF FM_EVENT_IS_PLAYER_ON_ANY_FM_EVENT(PLAYER_ID())
		AND NOT GB_IS_PLAYER_PERMANENT_PARTICIPANT(PLAYER_ID())
		AND NOT FM_EVENT_IS_PLAYER_PERMANENT_PARTICIPANT_IN_BUSINESS_BATTLE(PLAYER_ID())  
		
			#IF IS_DEBUG_BUILD
			IF FM_EVENT_IS_PLAYER_ON_ANY_FM_EVENT(PLAYER_ID())
				PRINTLN("MAINTAIN_DPAD_LEADERBOARD, SHOULD_BAIL_FROM_DPAD_CALL, FM_EVENT_IS_PLAYER_ON_ANY_FM_EVENT ")
			ENDIF
			IF NOT GB_IS_PLAYER_PERMANENT_PARTICIPANT(PLAYER_ID())
				PRINTLN("MAINTAIN_DPAD_LEADERBOARD, SHOULD_BAIL_FROM_DPAD_CALL, NOT GB_IS_PLAYER_PERMANENT_PARTICIPANT ")
			ENDIF
			#ENDIF
			
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF eSubMode = SUB_CHALLENGE
		IF GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(PLAYER_ID())
		AND GB_IS_PLAYER_PERMANENT_PARTICIPANT(PLAYER_ID())
		
			RETURN TRUE
		ENDIF
		
		IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) = GB_UI_LEVEL_FULL
	
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


// Draw the Challenge dpad lbd as opposed the Freemode one (DRAW_FREEMODE_DPAD_LBD)
// Black title bar missing? See GET_CHALLENGE_TITLE
PROC DRAW_CHALLENGE_DPAD_LBD(THE_LEADERBOARD_STRUCT& lbdStruct[], SCALEFORM_INDEX& siMovie, LBD_SUB_MODE eSubMode, DPAD_VARS& dpadVars, FM_DPAD_LBD_STRUCT& fmDpadVars, 
							AM_FREEMODE_CHALLENGES eChallengeType = AM_CHALLENGE_INVALID_CHALLENGE, BOOL bIsTeam = FALSE, STRING sCustomTitle = NULL)

	IF SHOULD_BAIL_FROM_DPAD_CALL(eSubMode)
	
		EXIT
	ENDIF
	
	INT iNumGangstersTotal
	#IF IS_DEBUG_BUILD
	INT iNumGangs
	#ENDIF
	INT iStoredRow[NUM_NETWORK_PLAYERS]
	BOOL bDO_BOSS_LEADERBOARD
	
	BOOL bOnMPIntro
	#IF FEATURE_GEN9_EXCLUSIVE
	bOnMPIntro = IS_PLAYER_ON_MP_INTRO()
	#ENDIF
	
	// Local vars
	PLAYER_INDEX PlayerId
	STRING strHeadshotTxd
	PEDHEADSHOT_ID pedHeadshot
	TEXT_LABEL_15 tlCrewTag
	INT iRank, iScore, iPlayer//, iIcon
	FLOAT fScore = -1.0
	INT iTime = -1
	INT iRowStatus = -1
	
	INT iTeam 
	
	// Added for bugs where we were not displaying all remaining FM players
	INT iBSPlayerIndex = 0
	
	// Display a position number or a headshot in the wee black bixes
	eHeadshotOrPositionNumber headshotOrPositionNumber = DISPLAY_HEADSHOT
	
	// Show the Job Points ?
	dpadVars.bShowJp = FALSE 
	
	HUD_COLOURS hcRowColour
	
	IF SHOULD_DRAW_CHALLENGE_DPAD_LBD()	
	OR eSubMode = SUB_BOSS
		IF SHOULD_DPAD_LBD_DISPLAY(siMovie, eSubMode, dpadVars, g_i_NumDpadLbdPlayers, DEFAULT, DRAW_CHALLENGE_DPAD_FOR_DEV_SPECTATOR(), sCustomTitle)
			
			SET_DPAD_DRAW_TYPE(eDPAD_TYPE_CHALLENGE)		
		
			IF ( NOT IS_ANY_FM_EVENTS_BIG_MESSAGE_BEING_DISPLAYED()	AND NOT IS_ANY_GB_BIG_MESSAGE_BEING_DISPLAYED() )
			OR IS_BIT_SET(MPGlobalsAmbience.iDpadBitSet, ciDPAD_BIT_POST_HELP_DELAY)
			
				IF IS_END_SHARD_ONSCREEN()
				
					CLEAR_END_SHARD()
				ELSE
			
					SET_SCRIPT_GFX_ALIGN(UI_ALIGN_LEFT, UI_ALIGN_TOP)
				
					// Populating the leaderboard with players and scores
					IF dpadVars.iDrawProgress = DPAD_INIT
					
						SET_DRAWING_CHALLENGE_BOOL(TRUE)
					
						g_i_NumDpadLbdPlayers = 0
						
						PRINTLN("[CS_DPAD] [CHLG_TYPE]  DRAW_CHALLENGE_DPAD_LBD, dpadVars.iDrawProgress = DPAD_POPULATE")
					
						INT i
						INT iCount
						INT iCurrentTeam = -1
						//INT iPosition
						
						// Clear our current scaleform movie
						IF g_RefreshDpad != REFRESH_DPAD_UPDATE
							PRINTLN("[CS_DPAD] [CHLG_TYPE]  DRAW_CHALLENGE_DPAD_LBD, g_RefreshDpad != REFRESH_DPAD_UPDATE: SCALEFORM_SET_DATA_SLOT_EMPTY called.")
							SCALEFORM_SET_DATA_SLOT_EMPTY(siMovie)
							IF IS_BIT_SET(dpadVars.iDpadBitSet, ciDPAD_IS_UPDATED)
								CLEAR_BIT(dpadVars.iDpadBitSet, ciDPAD_IS_UPDATED)
								PRINTLN("[CS_DPAD] [CHLG_TYPE]  DRAW_CHALLENGE_DPAD_LBD, CLEAR_BIT(dpadVars.iDpadBitSet, ciDPAD_IS_UPDATED) ")
							ENDIF
						ENDIF
						
						//╔═════════════════════════════════════════════════════════════════════════════╗
						//║				GROUP GANGSTERS TOGETHER										║
						
						INT iCurrentRow
						
						IF eSubMode = SUB_BOSS	
						
							REPEAT NUM_NETWORK_PLAYERS i
								iStoredRow[i] = -1
							ENDREPEAT
						
							REPEAT NUM_NETWORK_PLAYERS i														
								IF IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(i), FALSE)
									PlayerId = INT_TO_PLAYERINDEX(i)
									IF NOT IS_PLAYER_SCTV(PlayerId)
										IF PASSED_TUTORIAL_CHECK(PlayerId)
										OR INSERT_PLAYER_INTO_PLAYER_LIST(PlayerId, bOnMPIntro) 
										
											iPlayer = NATIVE_TO_INT(PlayerId)
											
											IF GB_IS_PLAYER_BOSS_OF_A_GANG(PlayerId)
												
												#IF IS_DEBUG_BUILD
												PRINTLN("[CS_DPAD] [CHLG_TYPE] DRAW_CHALLENGE_DPAD_LBD, GB_IS_PLAYER_BOSS_OF_A_GANG = ", GET_PLAYER_NAME(PlayerId), " iCurrentRow = ", iCurrentRow)
												iNumGangs++
												#ENDIF
												
												iStoredRow[iPlayer] = iCurrentRow
												
												iCurrentRow++
												
												iNumGangstersTotal++
												
												GB_FILL_GOON_ROWS(iStoredRow, PlayerId, iCurrentRow, iNumGangstersTotal, bOnMPIntro)
											ENDIF	
										ENDIF
									ENDIF
								ENDIF
							ENDREPEAT
						ENDIF
						
						IF NOT GB_IS_PLAYER_ON_ANY_GANG_BOSS_CHALLENGE(PLAYER_ID())
						AND GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) <> FMMC_TYPE_VEHICLE_EXPORT_SELL

							bDO_BOSS_LEADERBOARD = ( iNumGangstersTotal > 0 )
						ENDIF
						
						INT iNumberOfFMPlayers
						
						#IF IS_DEBUG_BUILD
						PRINTLN("[CS_DPAD] [CHLG_TYPE] [magnate] iNumGangstersTotal = ", iNumGangstersTotal, " iNumGangs = ", iNumGangs)
						#ENDIF //#IF IS_DEBUG_BUILD
					
						 // #IF FEATURE_GANG_BOSS
						
						//╚═════════════════════════════════════════════════════════════════════════════╝
						
						//╔═════════════════════════════════════════════════════════════════════════════╗
						//║				CHALLENGE PARTICIPANTS											║
						//╚═════════════════════════════════════════════════════════════════════════════╝
						REPEAT NUM_NETWORK_PLAYERS i
							
							// Just draw unsorted group of Challenge players (for modes where there is no scores etc.)
							IF SHOULD_DRAW_UNSORTED_LBD()
								IF IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(i), FALSE)
									PlayerId = INT_TO_PLAYERINDEX(i)
									PRINTLN("[CS_DPAD] [CHLG_TYPE]  DRAW_CHALLENGE_DPAD_LBD, [PLAYERS] SHOULD_DRAW_UNSORTED_LBD, PlayerId = ", NATIVE_TO_INT(PlayerId))
								ELSE
									PlayerId = INVALID_PLAYER_INDEX()
								ENDIF	
							ELSE
								// Sorted lbd players passed in from Challenge scripts
								PlayerId = lbdStruct[i].playerID
								
								#IF IS_DEBUG_BUILD
								IF PlayerId <> INVALID_PLAYER_INDEX()
									IF IS_NET_PLAYER_OK(PlayerId, FALSE)
										PRINTLN("[CS_DPAD] [CHLG_TYPE]  DRAW_CHALLENGE_DPAD_LBD, [PLAYERS] SORTED, PlayerId = ", GET_PLAYER_NAME(PlayerId))
									ENDIF
								ELSE
									PRINTLN("[CS_DPAD] [CHLG_TYPE]  DRAW_CHALLENGE_DPAD_LBD, [PLAYERS] ERROR, PlayerId = INVALID_PLAYER_INDEX ")
								ENDIF
								#ENDIF
							ENDIF
						
							IF HAS_PLAYER_PASSED_DPAD_CHECKS(PlayerId)
							AND HAS_PLAYER_PASSED_FM_EVENT_DPAD_CHECKS(PlayerId, eSubMode, bOnMPIntro)
							AND IS_NET_PLAYER_OK(PlayerId, FALSE)

								iPlayer = NATIVE_TO_INT(PlayerId)
								
//								dpadVars.sDpadMics[iPlayer].iRow = iCount
							
								iRank = GlobalplayerBD_FM[iPlayer].scoreData.iRank
								
								PRINTLN("[2226937] [2630598] DRAW_CHALLENGE_DPAD_LBD, i = ", i, " Name = ", GET_PLAYER_NAME(PlayerId), " iRank = ", iRank)
						
								// Grab crew tags
								tlCrewTag = GET_CREW_TAG_DPAD_LBD(PlayerId)
							
								// Local player 
								IF PlayerId = PLAYER_ID()								
									dpadVars.iPlayersRow = iCount
								ENDIF
								
								// grab names
								dpadVars.tl63LeaderboardPlayerNames = GET_PLAYER_NAME(PlayerId) 		
								
								// store tags
								fmDpadVars.aGamer[i] = GET_GAMER_HANDLE_PLAYER(playerId)					
								
								PRINTLN("[CS_DPAD] [CHLG_TYPE] DRAW_CHALLENGE_DPAD_LBD, dpadVars.tl63LeaderboardPlayerNames = ", dpadVars.tl63LeaderboardPlayerNames, " i = ", i)
//								IF IS_GAMER_HANDLE_VALID(fmDpadVars.aGamer[i])
//									PRINTLN("[CS_DPAD] [CHLG_TYPE] DRAW_CHALLENGE_DPAD_LBD, IS_GAMER_HANDLE_VALID,  iPlayer = ", i)
//								ENDIF
								
								// Attempt to get player headshot
								pedHeadshot = Get_HeadshotID_For_Player(playerId)
								strHeadshotTxd = ""
								IF pedHeadshot <> NULL
									
									strHeadshotTxd = GET_PEDHEADSHOT_TXD_STRING(pedHeadshot)
//									PRINTLN("[CS_DPAD] [CHLG_TYPE] DRAW_CHALLENGE_DPAD_LBD, OK strHeadshotTxd = ", strHeadshotTxd)
								ELSE
//									PRINTLN("[CS_DPAD] [CHLG_TYPE] DRAW_CHALLENGE_DPAD_LBD, NULL strHeadshotTxd = ", strHeadshotTxd)
								ENDIF
								
								g_i_NumDpadLbdPlayers++
								
								PRINTLN("[CS_DPAD] [CHLG_TYPE] DRAW_CHALLENGE_DPAD_LBD, with row = ", iCount, " player name = ", dpadVars.tl63LeaderboardPlayerNames, " iPlayer = ", iPlayer)
								
								// Score, time, whatever...
								IF lbdStruct[i].fScore <> -1
									fScore = lbdStruct[i].fScore
								ENDIF
								IF lbdStruct[i].iFinishTime <> -1
									iTime = lbdStruct[i].iFinishTime 
								ENDIF
								
								IF lbdStruct[i].iRowStatus <> -1
									iRowStatus = lbdStruct[i].iRowStatus 
								ENDIF
								
								// Score to display on lbd
								iScore = lbdStruct[i].iScore
								
								// If there is a score, show the positions instead of headshots
								IF lbdStruct[i].iScore <> -1
								OR lbdStruct[i].fScore <> -1
								OR lbdStruct[i].iFinishTime <> -1
									IF NOT SHOULD_DRAW_UNSORTED_LBD()
										headshotOrPositionNumber = DISPLAY_POSITION_NUMBER
									ENDIF
								ENDIF
								
								IF eChallengeType != AM_CHALLENGE_INVALID_CHALLENGE
									// grab f and i
									FORMAT_SCORE_FOR_LBD_DISPLAY(iScore, fScore, lbdStruct[i].iScore, eChallengeType)
									dpadVars.sDpadRowString = GET_CHALLENGE_UNITS(eChallengeType, TRUE)
								ENDIF
								
								// Draw team grouped position numbers 2412404
								IF bIsTeam
								
									iTeam = ( lbdStruct[i].iTeam + 1 ) // starts at 0
								
									IF iCurrentTeam != iTeam
										iCurrentTeam = iTeam
									ELSE
										iTeam = -2
									ENDIF
								ENDIF
				
								// ROW COLOURS
								hcRowColour = GB_GET_DPAD_GANG_ROW_COLOUR(playerID, FALSE)
								
								IF bDO_BOSS_LEADERBOARD
								
									IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PlayerId)
									AND iStoredRow[iPlayer] <> -1

										iCount = iStoredRow[iPlayer]
									ELSE
										iCount = (iNumGangstersTotal + iNumberOfFMPlayers)
										iNumberOfFMPlayers++
									ENDIF	
								ENDIF
								
								dpadVars.sDpadMics[iPlayer].iRow = iCount
								
								#IF IS_DEBUG_BUILD
								PRINTLN("[CS_DPAD] [CHLG_TYPE] DRAW_CHALLENGE_DPAD_LBD with row ADJUSTED = ", iCount, " player name = ", dpadVars.tl63LeaderboardPlayerNames, " iPlayer = ", iPlayer)
								#ENDIF
								TEXT_LABEL_15 tl15DpadVar1 
								IF lbdStruct[i].iTl15TeamCount != -1
									tl15DpadVar1 = "UW_TM"
									tl15DpadVar1 += lbdStruct[i].iTl15TeamCount
								ENDIF
								// Populating the dpad rows
								IF DOES_CHALLENGE_USE_TIME_FOR_LBD(eChallengeType)
									POPULATE_CHALLENGE_DPAD_ROWS(PlayerId, siMovie, dpadVars, iCount, tlCrewTag, strHeadshotTxd, iRank, iScore, hcRowColour, 
																	headshotOrPositionNumber, fScore, iScore, iRowStatus, tl15DpadVar1, 
																	lbdStruct[i].iDpadVarInt, iTeam, bIsTeam)
								ELSE
									POPULATE_CHALLENGE_DPAD_ROWS(PlayerId, siMovie, dpadVars, iCount, tlCrewTag, strHeadshotTxd, iRank, iScore, hcRowColour, 
																	headshotOrPositionNumber, fScore, iTime, iRowStatus, tl15DpadVar1, 
																	lbdStruct[i].iDpadVarInt, iTeam, bIsTeam)
								ENDIF
								
								// Record that we have drawn this player in the dpad leaderboard already
								SET_BIT(iBSPlayerIndex, NATIVE_TO_INT(playerID))
								
								iCount++
							ELSE
								PRINTLN("[CS_DPAD] [CHLG_TYPE] DRAW_CHALLENGE_DPAD_LBD, HAS_PLAYER_PASSED_DPAD_CHECKS, FALSE ")
							ENDIF
						ENDREPEAT
						
						//╔═════════════════════════════════════════════════════════════════════════════╗
						//║				CHALLENGE NON - PARTICIPANTS (FREEMODE)							║
						//╚═════════════════════════════════════════════════════════════════════════════╝
			
						// 2405759 Freemode Events - Event leaderboards need to show all players in session, have them listed below those players participating
						REPEAT NUM_NETWORK_PLAYERS i
							
							// Just draw unsorted
							PlayerId = INT_TO_PLAYERINDEX(i)
							IF IS_NET_PLAYER_OK(PlayerId, FALSE)
							AND NOT IS_BIT_SET(iBSPlayerIndex, NATIVE_TO_INT(PlayerId))				// Using this check now for 2410443
							//AND NOT NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
								PlayerId = INT_TO_PLAYERINDEX(i)
								PRINTLN("[CS_DPAD] [CHLG_TYPE] DRAW_CHALLENGE_DPAD_LBD, [PLAYERS], CHALLENGE NON - PARTICIPANTS, Player = ", GET_PLAYER_NAME(PlayerId))
							ELSE
								PlayerId = INVALID_PLAYER_INDEX()
							ENDIF	
							
							IF HAS_PLAYER_PASSED_DPAD_CHECKS(PlayerId)
								IF IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(i), FALSE)
								
									iPlayer = NATIVE_TO_INT(PlayerId)
									
//									dpadVars.sDpadMics[iPlayer].iRow = iCount

									PRINTLN("[2226937] [2630598] DRAW_CHALLENGE_DPAD_LBD, [PLAYERS], CHALLENGE NON - PARTICIPANTS i = ", i, " Name = ", GET_PLAYER_NAME(PlayerId), " iRank = ", iRank)
								
									iRank = GlobalplayerBD_FM[iPlayer].scoreData.iRank
							
									// Grab crew tags
									tlCrewTag = GET_CREW_TAG_DPAD_LBD(PlayerId)

									fmDpadVars.aGamer[i] = GET_GAMER_HANDLE_PLAYER(playerId)			
									
									// Attempt to get player headshot
									pedHeadshot = Get_HeadshotID_For_Player(playerId)
									strHeadshotTxd = ""
									IF pedHeadshot <> NULL
										
										strHeadshotTxd = GET_PEDHEADSHOT_TXD_STRING(pedHeadshot)
//										PRINTLN("[2405759] DRAW_CHALLENGE_DPAD_LBD, OK strHeadshotTxd = ", strHeadshotTxd)
									ELSE
//										PRINTLN("[2405759] DRAW_CHALLENGE_DPAD_LBD, NULL strHeadshotTxd = ", strHeadshotTxd)
									ENDIF
									
									g_i_NumDpadLbdPlayers++
								
									// We can use iCount directly as this was left from the mode list above
									PRINTLN("[2405759]  DRAW_CHALLENGE_DPAD_LBD, iCount =  ", iCount)
									
									// ROW COLOURS
									hcRowColour = GB_GET_DPAD_GANG_ROW_COLOUR(playerID, TRUE)
									
									IF bDO_BOSS_LEADERBOARD
									
										IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PlayerId)
	
											iCount = iStoredRow[i]
										ELSE
											iCount = (iNumGangstersTotal + iNumberOfFMPlayers)
											iNumberOfFMPlayers++
										ENDIF	
									ENDIF
									
									dpadVars.sDpadMics[iPlayer].iRow = iCount

									POPULATE_NON_CHALLENGE_PLAYERS(PlayerId, GET_PLAYER_NAME(PlayerId), siMovie, dpadVars, iCount, tlCrewTag, strHeadshotTxd, iRank, hcRowColour, headshotOrPositionNumber)
									
									iCount++
								ENDIF
							ELSE
								PRINTLN("[CS_DPAD] [CHLG_TYPE] DRAW_CHALLENGE_DPAD_LBD, HAS_PLAYER_PASSED_DPAD_CHECKS, FALSE 2 ")
							ENDIF
						ENDREPEAT	
			
						// Speaker chat icons
						IF IS_BIT_SET(dpadVars.iDpadBitSet, ciDPAD_DISPLAY_VIEW_DONE_ONCE)
							DO_THE_MICS(dpadVars, siMovie, eSubMode)
						ENDIF
						
						PRINTLN("[CS_DPAD] [CHLG_TYPE]  DRAW_CHALLENGE_DPAD_LBD, dpadVars.iDrawProgress = DPAD_DRAW")
					
						RESET_NET_TIMER(dpadVars.timerRefresh)
					
						RESET_DPAD_REFRESH()
						dpadVars.iDrawProgress = DPAD_DRAW
					ENDIF
					
					//╔═════════════════════════════════════════════════════════════════════════════╗
					//║				DRAW LBD														║
					//╚═════════════════════════════════════════════════════════════════════════════╝
					IF dpadVars.iDrawProgress = DPAD_DRAW
						
						IF NOT IS_BIT_SET(dpadVars.iDpadBitSet, ciDPAD_IS_UPDATED)
							HIGHLIGHT_LOCAL_PLAYER(dpadVars, siMovie)
							DISPLAY_VIEW(siMovie)
							SET_BIT(dpadVars.iDpadBitSet, ciDPAD_IS_UPDATED)
						ENDIF
						
						HIGHLIGHT_LOCAL_PLAYER(dpadVars, siMovie)
						
						IF NOT IS_BIT_SET(dpadVars.iDpadBitSet, ciDPAD_DISPLAY_VIEW_DONE_ONCE)
							SET_BIT(dpadVars.iDpadBitSet, ciDPAD_DISPLAY_VIEW_DONE_ONCE)
						ENDIF
						
						IF MAINTAIN_DPAD_VOICE_CHAT(dpadVars)
						
							PRINTLN("[CS_DPAD] [CHLG_TYPE]  DRAW_CHALLENGE_DPAD_LBD, MAINTAIN_DPAD_VOICE_CHAT, dpadVars.iDrawProgress = DPAD_INIT")
//							DO_THE_MICS(dpadVars, siMovie)
							g_RefreshDpad = REFRESH_DPAD_UPDATE
							//dpadVars.iDrawProgress = DPAD_INIT
						ENDIF
						
						REFRESH_DPAD_LEADERBOARD_AFTER_TIME(dpadVars)
						
						IF g_RefreshDpad = REFRESH_DPAD_UPDATE
							PRINTLN("[CS_DPAD] [CHLG_TYPE]  DRAW_CHALLENGE_DPAD_LBD, REFRESH_DPAD_UPDATE, dpadVars.iDrawProgress = DPAD_INIT")
							dpadVars.iDrawProgress = DPAD_INIT
						ENDIF
						
						IF g_RefreshDpad = REFRESH_DPAD_REPOPULATE
							PRINTLN("[CS_DPAD] [CHLG_TYPE]  DRAW_CHALLENGE_DPAD_LBD, REFRESH_DPAD_REPOPULATE, dpadVars.iDrawProgress = DPAD_INIT")
							dpadVars.iDrawProgress = DPAD_INIT
						ENDIF
					ENDIF
					
					// Draw
					IF HAS_SCALEFORM_MOVIE_LOADED(siMovie)
						SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_FADE)
							DRAW_SCALEFORM_MOVIE(siMovie, SDPAD_X, SDPAD_Y, SDPAD_W, SDPAD_H, 255, 255, 255, 255)
						SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
					ENDIF
				ENDIF
			ENDIF
		//╔═════════════════════════════════════════════════════════════════════════════╗
		//║				RESET LBD														║
		//╚═════════════════════════════════════════════════════════════════════════════╝
		ELSE
			dpadVars.iDrawProgress = DPAD_INIT
			RESET_DPAD_REFRESH()
			
			SET_DRAWING_CHALLENGE_BOOL(FALSE)
			
			IF IS_BIT_SET(dpadVars.iDpadBitSet, ciDPAD_IS_UPDATED)
				CLEAR_BIT(dpadVars.iDpadBitSet, ciDPAD_IS_UPDATED)
			ENDIF
			
			IF IS_BIT_SET(dpadVars.iDpadBitSet, ciDPAD_ROW_HIGHLIGHT)
				CLEAR_BIT(dpadVars.iDpadBitSet, ciDPAD_ROW_HIGHLIGHT)
			ENDIF
		ENDIF
	ENDIF
	
	RESET_SCRIPT_GFX_ALIGN()
ENDPROC

#IF FEATURE_COPS_N_CROOKS

FUNC ARCADE_MODE ARCADE_MODE_FOR_PLAYER_LIST(LBD_SUB_MODE eSubMode)

	ARCADE_MODE arcadeMode 
	SWITCH eSubMode
		CASE SUB_ARCADE_CNC
		
			arcadeMode =  ARC_COPS_CROOKS
		BREAK
	ENDSWITCH

	RETURN arcadeMode
ENDFUNC

PROC DRAW_ARCADE_PLAYER_LIST(AMBIENT_ACTIVITY_LEADERBOARD_STRUCT &leaderboardArray[eARCADETEAM_NUM_TYPES][NUM_PLAYERS_IN_ARCADE_TEAM], SCALEFORM_INDEX& siMovie, LBD_SUB_MODE eSubMode, DPAD_VARS& dpadVars)

	STRING strHeadshotTxd
	PEDHEADSHOT_ID pedHeadshot
	PLAYER_INDEX PlayerId
	INT iScore, iPlayer
	
	ARCADE_TEAM eArcadeTeam
	
	// Added for bugs where we were not displaying all remaining FM players
	INT iBSPlayerIndex = 0
	
	// Display a position number or a headshot in the wee black boxes
	eHeadshotOrPositionNumber headshotOrPositionNumber = DISPLAY_HEADSHOT
	
	// Show the Job Points ?
	dpadVars.bShowJp = FALSE 
	
	HUD_COLOURS hcRowColour
	
	INT iPlayerTeam = ENUM_TO_INT(GET_PLAYER_ARCADE_TEAM(PLAYER_ID()))
	
	INT NUM_ARCADE_TEAMS = ENUM_TO_INT(eARCADETEAM_NUM_TYPES)
	
	IF SHOULD_DPAD_LBD_DISPLAY(siMovie, eSubMode, dpadVars, g_i_NumDpadLbdPlayers, DEFAULT, DRAW_CHALLENGE_DPAD_FOR_DEV_SPECTATOR())
		
		SET_DPAD_DRAW_TYPE(eDPAD_TYPE_CHALLENGE)		
	
		IF ( NOT IS_ANY_FM_EVENTS_BIG_MESSAGE_BEING_DISPLAYED()	AND NOT IS_ANY_GB_BIG_MESSAGE_BEING_DISPLAYED() )
		OR IS_BIT_SET(MPGlobalsAmbience.iDpadBitSet, ciDPAD_BIT_POST_HELP_DELAY)
		
			IF IS_END_SHARD_ONSCREEN()
			
				CLEAR_END_SHARD()
			ELSE
		
				SET_SCRIPT_GFX_ALIGN(UI_ALIGN_LEFT, UI_ALIGN_TOP)
			
				// Populating the leaderboard with players and scores
				IF dpadVars.iDrawProgress = DPAD_INIT
				
					SET_DRAWING_CHALLENGE_BOOL(TRUE)
				
					g_i_NumDpadLbdPlayers = 0
					
					PRINTLN("[PLAYER_LIST], dpadVars.iDrawProgress = DPAD_POPULATE")
				
					INT iRow
					
					// Clear our current scaleform movie
					IF g_RefreshDpad != REFRESH_DPAD_UPDATE
						PRINTLN("[PLAYER_LIST], g_RefreshDpad != REFRESH_DPAD_UPDATE: SCALEFORM_SET_DATA_SLOT_EMPTY called.")
						SCALEFORM_SET_DATA_SLOT_EMPTY(siMovie)
						IF IS_BIT_SET(dpadVars.iDpadBitSet, ciDPAD_IS_UPDATED)
							CLEAR_BIT(dpadVars.iDpadBitSet, ciDPAD_IS_UPDATED)
							PRINTLN("[PLAYER_LIST], CLEAR_BIT(dpadVars.iDpadBitSet, ciDPAD_IS_UPDATED) ")
						ENDIF
					ENDIF
					
					INT iLoop
					INT iTotalTeam = COUNT_OF(leaderboardArray)
					INT iTeam
					
					FOR iLoop = 0 TO iTotalTeam - 1
					
               			iTeam = iPlayerTeam + iLoop

               			IF iTeam >= NUM_ARCADE_TEAMS
                        	iTeam = iTeam % NUM_ARCADE_TEAMS
               			ENDIF
               
               			IF iTeam = ENUM_TO_INT(eARCADETEAM_NONE)
               			OR iTeam = ENUM_TO_INT(eARCADETEAM_NUM_TYPES)
                        	RELOOP
               			ENDIF
						
						INT i
											
						// Now loop over the players
						REPEAT NUM_PLAYERS_IN_ARCADE_TEAM i
						
							// Sorted lbd players passed in from Challenge scripts
							PlayerId = leaderboardArray[iTeam][i].playerID
														
							IF HAS_PLAYER_PASSED_DPAD_CHECKS(PlayerId)
							AND HAS_PLAYER_PASSED_FM_EVENT_DPAD_CHECKS(PlayerId, eSubMode, bOnMPIntro)
							AND IS_NET_PLAYER_OK(PlayerId, FALSE)

								iPlayer = NATIVE_TO_INT(PlayerId)
							
								// Local player 
								IF PlayerId = PLAYER_ID()								
									dpadVars.iPlayersRow = iRow
								ENDIF
								
								// grab names
								dpadVars.tl63LeaderboardPlayerNames = GET_PLAYER_NAME(PlayerId) 							
								
								// Attempt to get player headshot
								pedHeadshot = Get_HeadshotID_For_Player(playerId)
								strHeadshotTxd = ""
								IF pedHeadshot <> NULL
									
									strHeadshotTxd = GET_PEDHEADSHOT_TXD_STRING(pedHeadshot)
	//								PRINTLN("[DRAW_ARCADE_PLAYER_LIST], OK strHeadshotTxd = ", strHeadshotTxd)
	//							ELSE
	//								PRINTLN("[DRAW_ARCADE_PLAYER_LIST], NULL strHeadshotTxd = ", strHeadshotTxd)
								ENDIF
								
								g_i_NumDpadLbdPlayers++
								
								// Score to display on lbd
								iScore = leaderboardArray[iTeam][i].iScore

								eArcadeTeam = GET_PLAYER_ARCADE_TEAM(PlayerId)
				
								// ROW COLOURS
								hcRowColour = GET_ARCADE_TEAM_HUD_COLOUR(ARCADE_MODE_FOR_PLAYER_LIST(eSubMode), (iTeam-1), playerID)//function uses native team as opposed arcade tea
								
								dpadVars.sDpadMics[iPlayer].iRow = iRow
								
								#IF IS_DEBUG_BUILD
								PRINTLN("[DRAW_ARCADE_PLAYER_LIST] Drawing player ", dpadVars.tl63LeaderboardPlayerNames, " at row ", iRow)
								#ENDIF
								
								IF eArcadeTeam = eARCADETEAM_COP
									g_GBLeaderboardStruct.dpadVariables.eDpadVarType = DPAD_VAR_INT 
								ELSE
									g_GBLeaderboardStruct.dpadVariables.eDpadVarType = DPAD_VAR_CASH 
								ENDIF
																	
								POPULATE_ARCADE_DPAD_ROWS(PlayerId, siMovie, dpadVars, iRow, strHeadshotTxd, iScore, hcRowColour, headshotOrPositionNumber)

								// Record that we have drawn this player in the dpad leaderboard already
								SET_BIT(iBSPlayerIndex, NATIVE_TO_INT(playerID))
								
								iRow++
								
//								iNumEnemies++
	//						ELSE
	//							PRINTLN("[DRAW_ARCADE_PLAYER_LIST], HAS_PLAYER_PASSED_DPAD_CHECKS, FALSE, i = ", i)
							ENDIF
						ENDREPEAT

					ENDFOR
					
					// Speaker chat icons
					IF IS_BIT_SET(dpadVars.iDpadBitSet, ciDPAD_DISPLAY_VIEW_DONE_ONCE)
						DO_THE_MICS(dpadVars, siMovie, eSubMode)
					ENDIF
					
					PRINTLN("[PLAYER_LIST], dpadVars.iDrawProgress = DPAD_DRAW")
				
					RESET_NET_TIMER(dpadVars.timerRefresh)
				
					RESET_DPAD_REFRESH()
					dpadVars.iDrawProgress = DPAD_DRAW
				ENDIF
				
				//╔═════════════════════════════════════════════════════════════════════════════╗
				//║				DRAW LBD														║
				//╚═════════════════════════════════════════════════════════════════════════════╝
				IF dpadVars.iDrawProgress = DPAD_DRAW
					
					IF NOT IS_BIT_SET(dpadVars.iDpadBitSet, ciDPAD_IS_UPDATED)
						HIGHLIGHT_LOCAL_PLAYER(dpadVars, siMovie)
						DISPLAY_VIEW(siMovie)
						SET_BIT(dpadVars.iDpadBitSet, ciDPAD_IS_UPDATED)
					ENDIF
					
					HIGHLIGHT_LOCAL_PLAYER(dpadVars, siMovie)
					
					IF NOT IS_BIT_SET(dpadVars.iDpadBitSet, ciDPAD_DISPLAY_VIEW_DONE_ONCE)
						SET_BIT(dpadVars.iDpadBitSet, ciDPAD_DISPLAY_VIEW_DONE_ONCE)
					ENDIF
					
					IF MAINTAIN_DPAD_VOICE_CHAT(dpadVars)
						PRINTLN("[PLAYER_LIST], MAINTAIN_DPAD_VOICE_CHAT, dpadVars.iDrawProgress = DPAD_INIT")
						g_RefreshDpad = REFRESH_DPAD_UPDATE
					ENDIF
					
					REFRESH_DPAD_LEADERBOARD_AFTER_TIME(dpadVars)
					
					IF g_RefreshDpad = REFRESH_DPAD_UPDATE
						PRINTLN("[PLAYER_LIST], REFRESH_DPAD_UPDATE, dpadVars.iDrawProgress = DPAD_INIT")
						dpadVars.iDrawProgress = DPAD_INIT
					ENDIF
					
					IF g_RefreshDpad = REFRESH_DPAD_REPOPULATE
						PRINTLN("[PLAYER_LIST], REFRESH_DPAD_REPOPULATE, dpadVars.iDrawProgress = DPAD_INIT")
						dpadVars.iDrawProgress = DPAD_INIT
					ENDIF
				ENDIF
				
				// Draw
				IF HAS_SCALEFORM_MOVIE_LOADED(siMovie)
					SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_FADE)
						DRAW_SCALEFORM_MOVIE(siMovie, SDPAD_X, SDPAD_Y, SDPAD_W, SDPAD_H, 255, 255, 255, 255)
					SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
				ENDIF
			ENDIF
		ENDIF
	//╔═════════════════════════════════════════════════════════════════════════════╗
	//║				RESET LBD														║
	//╚═════════════════════════════════════════════════════════════════════════════╝
	ELSE
		dpadVars.iDrawProgress = DPAD_INIT
		RESET_DPAD_REFRESH()
		
		SET_DRAWING_CHALLENGE_BOOL(FALSE)
		
		IF IS_BIT_SET(dpadVars.iDpadBitSet, ciDPAD_IS_UPDATED)
			CLEAR_BIT(dpadVars.iDpadBitSet, ciDPAD_IS_UPDATED)
		ENDIF
		
		IF IS_BIT_SET(dpadVars.iDpadBitSet, ciDPAD_ROW_HIGHLIGHT)
			CLEAR_BIT(dpadVars.iDpadBitSet, ciDPAD_ROW_HIGHLIGHT)
		ENDIF
	ENDIF

	RESET_SCRIPT_GFX_ALIGN()
ENDPROC

#ENDIF //#IF FEATURE_COPS_N_CROOKS

 // FEATURE_NEW_AMBIENT_EVENTS



