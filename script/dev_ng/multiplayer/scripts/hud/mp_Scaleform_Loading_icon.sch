ENUM  LOADING_ICON_SYMBOLS
	LOADING_ICON_EMPTY = 0,
	LOADING_ICON_SPINNER = 1,
	LOADING_ICON_STAR = 4,
	LOADING_ICON_LOADING = 5
ENDENUM

STRUCT SCALEFORM_LOADING_ICON
	BOOL bInitialised
	TEXT_LABEL_63 sMainStringSlot
	TEXT_LABEL_63 sPlayerName
	INT TopNumber
	INT Denominator
	BOOL bRefreshScaleformLoading
	BOOL IsTextDoubleNumber
	BOOL IsTextSingleNumber
	BOOL IsTextTwoString
	BOOL IsTextPlayerNameAndDoubleNumber
	BOOL IsNumberTime
	
	LOADING_ICON_SYMBOLS loadingIcon = LOADING_ICON_SPINNER
	
	FLOAT LoadingAlignX
	FLOAT LoadingAlignY
	FLOAT LoadingSizeX
	FLOAT LoadingSizeY

ENDSTRUCT
