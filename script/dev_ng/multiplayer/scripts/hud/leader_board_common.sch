USING "transition_common.sch"
USING "net_common_functions.sch"
USING "mp_scaleform_functions.sch"
USING "net_hud_colour_controller.sch"
USING "gunclub_shop_private.sch"
USING "net_clouds.sch"

USING "freemode_events_header_private.sch"
///    Pixels based on 1280x720 
///    1px on x-axis = 0.00078125 
///    1px on y-axis = 0.0013888

CONST_INT ONE_HOUR_MILLISECONDS						3600000
CONST_INT BET_SWAP_DELAY 							5000

CONST_INT NUM_LEADERBOARD_SLOTS						16
//CONST_INT DPAD_DOWN_TIMER 							10000
CONST_INT THUMBS_FADE_DEFAULT						300

TWEAK_INT MAX_BEFORE_SCROLL							17
TWEAK_INT MAX_ROWS_DRAWN 							16 // The number of rows drawn on any leaderboard page

CONST_INT LAMAR_RANK								99

CONST_INT WAIT_FOR_SAFE_RESTART 					0
CONST_INT WAIT_FOR_PLAY_AGAIN						1
CONST_INT WAIT_FOR_RESTART							2

CONST_INT SC_LB_TYPE_RACE							0
CONST_INT SC_LB_TYPE_RACE_CREW						1
CONST_INT SC_LB_TYPE_GTA_RACE						2
CONST_INT SC_LB_TYPE_GTA_RACE_CREW					3
CONST_INT SC_LB_TYPE_DEATHMATCH						4
CONST_INT SC_LB_TYPE_DEATHMATCH_CREW				5
CONST_INT SC_LB_TYPE_TEAM_DEATHMATCH				6
CONST_INT SC_LB_TYPE_TEAM_DEATHMATCH_CREW			7
CONST_INT SC_LB_TYPE_GOLF							8
CONST_INT SC_LB_TYPE_ARMWRESTLING					9
CONST_INT SC_LB_TYPE_DARTS							10
CONST_INT SC_LB_TYPE_TENNIS							11
CONST_INT SC_LB_TYPE_SHOOTING_RANGE					12

CONST_INT EMPTY_LB_ENTRY							-999999

// Text
CONST_INT TEXT_PLACEMENT_POSITION					0		
CONST_INT TEXT_PLACEMENT_PLAYER_NAMES				1				
CONST_INT TEXT_PLACEMENT_LB_LARGE_POS				2		
CONST_INT TEXT_PLACEMENT_TEAM_SCORE					3			
CONST_INT TEXT_PLACEMENT_COLUMN1					4
CONST_INT TEXT_PLACEMENT_COLUMN2					5
CONST_INT TEXT_PLACEMENT_COLUMN3					6
CONST_INT TEXT_PLACEMENT_COLUMN4					7
CONST_INT TEXT_PLACEMENT_COLUMN5					8
CONST_INT TEXT_PLACEMENT_COLUMN6					9				
CONST_INT TEXT_PLACEMENT_THUMB_COUNT				10
CONST_INT TEXT_PLACEMENT_TEAM_TOTALS				11
CONST_INT TEXT_PLACEMENT_TITLE						12
CONST_INT TEXT_PLACEMENT_COLUMN_TITLE				13
CONST_INT TEXT_PLACEMENT_SELECTION_COUNT			14
CONST_INT TEXT_PLACEMENT_PERCENTAGE_LIKE			15
CONST_INT TEXT_PLACEMENT_ROCKSTAR_BADGE				16
//CONST_INT TEXT_PLACEMENT_COLUMN_4					
//CONST_INT TEXT_PLACEMENT_KILLS						
//CONST_INT TEXT_PLACEMENT_COLUMN_6					
//CONST_INT TEXT_PLACEMENT_TIME						
//CONST_INT TEXT_PLACEMENT_RATIO		
//CONST_INT TEXT_PLACEMENT_TITLE		
//CONST_INT TEXT_PLACEMENT_CASH_EARNED						
//CONST_INT TEXT_PLACEMENT_XP_EARNED		
// Sprites

CONST_INT SPRITE_PLACEMENT_LB_DDS					0
CONST_INT SPRITE_PLACEMENT_CARS						1
CONST_INT SPRITE_PLACEMENT_LAP_STAR					2
CONST_INT SPRITE_PLACEMENT_VEHICLE_BLOB				3
CONST_INT SPRITE_PLACEMENT_VOICE					4
CONST_INT SPRITE_PLACEMENT_THUMB_VOTE				5
CONST_INT SPRITE_PLACEMENT_GRADIENT_HEADER			6
CONST_INT SPRITE_PLACEMENT_LBD_THUMBS				7
CONST_INT SPRITE_PLACEMENT_ARROWS					8
CONST_INT SPRITE_PLACEMENT_THUMB_PERCENT			9
CONST_INT SPRITE_PLACEMENT_HEAD_POS					10
CONST_INT SPRITE_PLACEMENT_EYE_POS					11


CONST_FLOAT LEADERBOARD_CAMERA_ANGLE 				30.0

										// BIG LEADERBOARD 
// RECTS /////////////////////////////////////////////////
CONST_INT RECT_PLACEMENT_WHITE_TITLES				0
CONST_INT RECT_PLACEMENT_HORZ						1
CONST_INT RECT_PLACEMENT_LONG_CAP					2
CONST_INT RECT_PLACEMENT_POS						3	
CONST_INT RECT_PLACEMENT_VARS						4
CONST_INT RECT_PLACEMENT_BLACK_POS					5
CONST_INT RECT_PLACEMENT_COLOUR_POS					6
CONST_INT RECT_PLACEMENT_COUNT						7
CONST_INT RECT_PLACEMENT_VOTE_COUNT					8
CONST_INT RECT_HEADER								9 //a rectangle to darken the grad sprite

//CONST_FLOAT ROCKSTAR_BADGE_X 						0.511
CONST_FLOAT ROCKSTAR_BADGE_Y 						0.1011
CONST_FLOAT ROCKSTAR_BADGE_SCALE 					0.5

CONST_FLOAT TVOTEP_H 	0.050
CONST_FLOAT TVOTEP_W 	0.022
//CONST_FLOAT TVOTEP_X 	0.573
CONST_FLOAT TVOTEP_Y 	0.1211
//CONST_FLOAT PERCENT_X	0.583

CONST_FLOAT fCrewOffset		0.000 //0.003
CONST_FLOAT fCrewScale		0.500

CONST_FLOAT f6wrap1EndX     	0.276
CONST_FLOAT f6wrap2EndX     	0.365
CONST_FLOAT f6wrap3EndX     	0.448
CONST_FLOAT f6wrap4EndX     	0.517
CONST_FLOAT f6wrap5EndX     	0.583
CONST_FLOAT f6wrap6EndX     	0.648

CONST_FLOAT f5wrap1EndX     	0.276
CONST_FLOAT f5wrap2EndX     	0.373
CONST_FLOAT f5wrap3EndX     	0.467
CONST_FLOAT f5wrap4EndX     	0.552
CONST_FLOAT f5wrap5EndX    		0.648

CONST_FLOAT f4wrap1EndX     	0.276
CONST_FLOAT f4wrap2EndX     	0.387
CONST_FLOAT f4wrap3EndX     	0.505
CONST_FLOAT f4wrap4EndX     	0.645

CONST_FLOAT f3wrap1EndX     	0.276
CONST_FLOAT f3wrap2EndX     	0.448
CONST_FLOAT f3wrap3EndX     	0.648

CONST_FLOAT plf6wrap1EndX     	0.266
CONST_FLOAT plf6wrap2EndX     	0.332
CONST_FLOAT plf6wrap3EndX     	0.400
CONST_FLOAT plf6wrap4EndX     	0.470
CONST_FLOAT plf6wrap5EndX     	0.554
CONST_FLOAT plf6wrap6EndX     	0.645

CONST_FLOAT plf5wrap1EndX     	0.276
CONST_FLOAT plf5wrap2EndX     	0.373
CONST_FLOAT plf5wrap3EndX     	0.467
CONST_FLOAT plf5wrap4EndX     	0.552
CONST_FLOAT plf5wrap5EndX     	0.645

CONST_FLOAT plf4wrap1EndX     	0.276
CONST_FLOAT plf4wrap2EndX     	0.386
CONST_FLOAT plf4wrap3EndX     	0.514
CONST_FLOAT plf4wrap4EndX     	0.645

CONST_FLOAT plf3wrap1EndX     	0.300
CONST_FLOAT plf3wrap2EndX     	0.470
CONST_FLOAT plf3wrap3EndX     	0.645

CONST_FLOAT f2wrap1EndX       	0.276
CONST_FLOAT f2wrap2EndX    		0.648

CONST_FLOAT f1wrap1EndX     	0.648

TWEAK_FLOAT fRoundsOffset    	0.0

// TEXT /////////////////////////////////////////////////

// Column variables passed in
CONST_FLOAT POSITION_POS_X							0.1704
CONST_FLOAT BIG_PLAYER_NAMES_X						0.185
//CONST_FLOAT TEAM_BIG_PLAYER_NAMES_X				0.186
//CONST_FLOAT CARS_POS_X							0.429
//CONST_FLOAT COLUMN_VARIABLE_5						0.457
//CONST_FLOAT RACE_BEST_LAP_POS						0.628 
//CONST_FLOAT GTA_BEST_LAP_POS						0.704
//CONST_FLOAT COLUMN_VARIABLE_7						0.579
//CONST_FLOAT TIME_POS_X							0.779 
//CONST_FLOAT COLUMN_VARIABLE_9						0.707
//CONST_FLOAT COLUMN_VARIABLE_10					0.757
CONST_FLOAT VARIABLE_BASE_Y							0.217
CONST_FLOAT VARIABLE_BASE_Y_NAMES					0.213
//TWEAK_FLOAT VARIABLE_BASE_Y_NAMES					0.217
CONST_FLOAT VARIABLE_BASE_CREW						0.216

TWEAK_FLOAT COUNT_LONG__RECT_X						0.27500000
TWEAK_FLOAT COUNT_RECT_Y							0.867
TWEAK_FLOAT COUNT_LONG__RECT_W						0.22900000

CONST_FLOAT THUMB_LONG__RECT_X						0.882
CONST_FLOAT THUMB_RECT_Y							-0.015
CONST_FLOAT THUMB_LONG__RECT_W						0.085

// Large 1st top right
CONST_FLOAT LARGE_POS_X 							0.744
//TWEAK_FLOAT LARGE_POS_X 							0.744
CONST_FLOAT LARGE_POS_Y 							0.096

CONST_FLOAT LARGE_TITLE_POS_X						0.1585
//TWEAK_FLOAT LARGE_TITLE_POS_X						0.1585
//CONST_FLOAT LARGE_POS_Y 							0.092

CONST_FLOAT COLUM_TITLE_X							0.152
CONST_FLOAT COLUM_TITLE_Y 							0.169
//TWEAK_FLOAT COLUM_TITLE_Y 							0.169

// Team scores top right big
CONST_FLOAT YOUR_TEAM_SCORE_X						0.422
//CONST_FLOAT ENEMY_TEAM_SCORE_X					0.772
CONST_FLOAT TEAM_SCORE_Y							0.130

// SPRITES
CONST_FLOAT RANK_BADGE_X							0.3775
CONST_FLOAT RANK_BADGE_Y 							0.1975000 //0.19811 + 0.0013888

CONST_FLOAT EYE_X									0.3580
CONST_FLOAT EYE_Y_OFFSET							0.2310
CONST_FLOAT EYE_W									0.0220
CONST_FLOAT EYE_H									0.0420

CONST_FLOAT DDS_X									0.500
CONST_FLOAT DDS_Y 									0.500
CONST_FLOAT DDS_W 									1.003
CONST_FLOAT DDS_H 									1.026

CONST_FLOAT DPAD_DDS_X								0.500
CONST_FLOAT DPAD_DDS_Y 								0.500
CONST_FLOAT DPAD_DDS_W 								1.000
CONST_FLOAT DPAD_DDS_H 								1.000
CONST_INT 	DPAD_DDS_A								200

CONST_FLOAT GRAD_X 0.4995
CONST_FLOAT GRAD_Y 0.1441
CONST_FLOAT GRAD_W 0.6785
CONST_FLOAT GRAD_H 0.1381

// RECTANGLES ///////////////////////////////////////////

// Column heading background
CONST_FLOAT COLUMN_HEADING_RECT_X					0.4995
CONST_FLOAT COLUMN_HEADING_RECT_Y					0.182
CONST_FLOAT COLUMN_HEADING_RECT_W					0.6785

CONST_FLOAT RECT_Y_OFFSET							0.03749976//0.03877776 2 pixel gap on top of RECT_HEIGHT_CONSTANT
//TWEAK_FLOAT RECT_Y_OFFSET							0.03749976//0.03877776 2 pixel gap on top of RECT_HEIGHT_CONSTANT

// Columns
TWEAK_FLOAT COLUMN_RECT_Y							0.230

CONST_FLOAT COLUMN_LONG__RECT_X						0.4995
CONST_FLOAT COLUMN_LONG__RECT_W						0.6785



TWEAK_FLOAT PLAYER_NAME_RECT_W						0.2091//0.207
TWEAK_FLOAT PLAYER_NAME_RECT_X						0.2845//0.284

CONST_FLOAT COLUMN_DM__RECT_CONST_WIDTH				0.053

// Wee red
TWEAK_FLOAT RED_X									0.1701
TWEAK_FLOAT RED_W									0.01953125// 25px	//0.017
CONST_FLOAT THIN_COLOUR_X							0.181955
CONST_FLOAT THIN_COLOUR_W							0.003125

CONST_FLOAT RECT_HEIGHT_CONSTANT					0.034722			//0.03611088 
//CONST_FLOAT TWO_PIXEL_HEIGHT						0.0015625
CONST_FLOAT TEAM_RECT_X								0.31515
CONST_FLOAT TEAM_RECT_W								0.3099
//CONST_FLOAT WHITE_TEAM_RECT_Y						0.250

TWEAK_FLOAT VAR_RECT_W								0.6579
TWEAK_FLOAT VAR_RECT_X								0.5095554 //0.51011

// CAPS
CONST_FLOAT COLUMN_CAPS_Y_ALL						0.1615
CONST_FLOAT COLUMN_CAP_H_ALL						0.0069444 // 5px
CONST_FLOAT COLUMN_CAP1_X							0.4995
CONST_FLOAT COLUMN_CAP1_W							0.6785

// SPRITES /////////////////////////////////////////////
CONST_FLOAT LBVOTE_H 								0.050
CONST_FLOAT LBVOTE_W 								0.022
CONST_FLOAT LBVOTE_X 								0.358
CONST_FLOAT LBVOTE_Y 								0.230
CONST_FLOAT LBVOTE_ROT 								180.00

CONST_INT HUNDRED_ALPHA	255
//CONST_INT NINETY_ALPHA	230
CONST_INT EIGHTY_ALPHA 	204
CONST_INT SIXTY_ALPHA 	153
CONST_INT FIFTY_ALPHA	128
CONST_INT THIRTY_ALPHA	77
CONST_INT FIFTEEN_ALPHA	39

// TEXT
//CONST_FLOAT PLAYER_CASH_EARNED_X					0.436
CONST_FLOAT PLAYER_XP_EARNED_X						0.503

CONST_FLOAT TEXT_VOTE_UP_X							0.872
CONST_FLOAT TEXT_VOTE_Y								0.285
CONST_FLOAT TEXT_VOTE_X_OFFSET						0.040

//CONST_FLOAT DPAD_POSITION_OFFSET					0.003
//CONST_FLOAT DPAD_POSITION_X							0.054 

CONST_FLOAT TEAM_TOTAL_X							0.16525
//CONST_FLOAT TEAM_TOTAL_Y							0.214

FUNC MODEL_NAMES GET_LAMAR_VEH_MODEL()
	RETURN EMPEROR
ENDFUNC

FUNC BOOL REPLACE_BETTING_COLUMN()

	IF CV2_WAS_STUNT_RACE_LAUNCHED_AS_PROFESSIONAL()
	
		RETURN TRUE
	ENDIF

	IF g_FinalRoundsLbd
	
		RETURN FALSE
	ENDIF

	RETURN g_SwapBettingColumnForVotes
ENDFUNC

FUNC BOOL IS_SCREEN_SMALL()
	INT iX, iY
	GET_ACTUAL_SCREEN_RESOLUTION(iX, iY)
	IF iX <= 1024
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

ENUM LBD_VAR_TYPE
	VAR_TYPE_INT=0,
	VAR_TYPE_INT_CASH,
	VAR_TYPE_TIME,
	VAR_TYPE_STRING,
	VAR_TYPE_FLOAT,
	VAR_TYPE_ENUM,
	VAR_TYPE_NOT_USED
ENDENUM

ENUM LBD_VOTE
	LBD_VOTE_EXIT=0,
	LBD_VOTE_REPLAY,
	LBD_VOTE_RANDOM,
	LBD_VOTE_CONTINUE,
	LBD_VOTE_RESTART,
	LBD_VOTE_RESTART_SWAP_TEAMS,
	LBD_VOTE_NOT_USED
ENDENUM

STRUCT SC_LB_RACE_STRUCT
	INT iScore
	INT iRaceTime
	INT iBestLap
	INT iKills
	INT iDeaths
	INT iVehicleID
	INT iVehicleColour
ENDSTRUCT

// 1757460
STRUCT AMMO_ARMOUR_STRUCT
	INT iBitSet
	INT iTotalCostToFillAmmo
	INT iWeaponSlot
ENDSTRUCT

// iBitSet (AMMO_ARMOUR_STRUCT)
CONST_INT LBD_BOOL_BLOCK_ARMOUR						0
CONST_INT LBD_BOOL_BLOCK_AMMO						1
CONST_INT LBD_BOOL_COUNT_AMMO_COST					2
CONST_INT LBD_BOOL_AMMO_WARNING_ON					3
CONST_INT LBD_BOOL_REFRESH_NEEDED					4
CONST_INT LBD_BOOL_WAS_USING_KEYBOARD				5

ENUM eRACE_TYPE
	eRACE_TYPE_NONE=0,
	eRACE_TYPE_LAPS,
	eRACE_TYPE_P2P
ENDENUM

STRUCT LBD_VARS
	eRACE_TYPE eRaceType = eRACE_TYPE_NONE
	TEXT_LABEL_63 tl63TeamNames//[FMMC_MAX_TEAMS]
	GAMER_HANDLE aGamer[NUM_NETWORK_PLAYERS]
	
	SHOP_INPUT_DATA_STRUCT sInputData
	AMMO_ARMOUR_STRUCT sAmmoArmour
	
//	INT iHandleBitSet
	BOOL bDrawLamar
	BOOL bLamarFinished =  FALSE
	BOOL bLamarShowBestlap = FALSE

	INT iLamarRow = -1	
	INT iLamarFinishTime 
	INT iLamarBestLap = -1
	INT iLamarPos
	INT iLamarCash
	INT iLamarXP
	STRING sLamarVeh
	
	INT iTeamPos = 1
	
	// Veh colours for blobs
	INT iVehRed
	INT iVehGreen
	INT iVehBlue
	TEXT_LABEL_15 tlRaceVeh
	
	BOOL bHideMyRow
	BOOL bHidePos
	BOOL bHideIndividual
	
	TEXT_LABEL_23 tlDisplayStr
	
	BOOL bVoted
	BOOL bVotedDown
	
//	INT iPlayerBestLap
	BOOL bDrawBestLapStar
	
	INT iPlaylistJobCount
	
	BOOL bDrawScrollbars
	BOOL bPlayerRowGrabbed
	
	
	INT iPlayerSelected
	INT iNumPlayersOnLbd
	
//	INT iLikePercent
	INT iProgressSpc
	
	INT iTeamRowsPassed
	
	INT iLBDPlayerBitSetHideVote
	
	PEDHEADSHOT_ID lamarHeadshotIndex
	TEXT_LABEL_23 sLamarHeadshotTxd
	
//	PEDHEADSHOT_ID pedHeadshot
	
	INT iPlaylistProgress = -1
ENDSTRUCT


STRUCT FM_LEADERBOARD_STRUCT
	INT iParticipant
	PLAYER_INDEX playerID 
	INT iPlayerScore
	INT iCash
	INT iKills
	INT iCustomInt
	INT iHeadshots
	INT iHighestStreak
	INT iDeaths
	INT iBadgeRank
	INT iRank
	BOOL bInitialise
ENDSTRUCT

STRUCT LEADERBOARD_PLACEMENT_TOOLS
	SPRITE_PLACEMENT SpritePlacement[MPHUD_MAX_NUMBER_OF_SPRITE_PLACEMENTS]
	TEXT_PLACEMENT TextPlacement[MPHUD_MAX_NUMBER_OF_TEXT_PLACEMENTS]
	TEXT_PLACEMENT MainTitle
	SCALEFORM_INDEX ButtonMovie[MAX_NUMBER_MPHUD_SCALEFORM_MOVIES]
	RECT RectPlacement[MPHUD_MAX_NUMBER_OF_RECTS]
//	TRANSITION_HUDCOGS ScreenPlace
	TEXTSTYLES aStyle
	
	BOOL bHudScreenInitialised
	SCALEFORM_INSTRUCTIONAL_BUTTONS LeaderboardScaleformStruct
	SCALEFORM_LOADING_ICON ScaleformLoadingStruct
	SCALEFORM_TABS ScaleformTabStruct
//	BOOL bDoOnce
ENDSTRUCT

PROC MANAGE_LBD_DEV_SPECTATE_EVENT()
	IF IS_PLAYLIST_IS_A_TOURNAMENT_PLAYLIST()
		IF IS_PLAYER_DEV_SPECTATOR(PLAYER_ID())
			PRINTLN("[DEV_SPC] MANAGE_LBD_DEV_SPECTATE_EVENT, IS_PLAYER_DEV_SPECTATOR ") 
			IF g_b_OnLeaderboard
				PRINTLN("[DEV_SPC] MANAGE_LBD_DEV_SPECTATE_EVENT, g_b_OnLeaderboard ") 
				IF HAS_DEV_CONTINUE_BUTTON_BEEN_PRESSED()
					BROADCAST_DEV_SPECTATE_LBD_CONTINUE_BUTTON()
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_MOUSE_INSIDE_PLAYER_RECT(INT i, BOOL bDrawRect)

	FLOAT fCursorX = GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_CURSOR_X)
	FLOAT fCursorY = GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_CURSOR_Y)
	FLOAT fX, fY, fXMin, fYMin, fXMax, fYMax
	
	// Coords from centre, apparently as the highlight rect matches.
	fX = ADJUST_SCREEN_X_POSITION(PLAYER_NAME_RECT_X)
	fY = ADJUST_SCREEN_Y_POSITION(COLUMN_RECT_Y + (TO_FLOAT(i) * RECT_Y_OFFSET))
	
	// Detection box, adjust to top left corner origin.
	fXMin = fX - (PLAYER_NAME_RECT_W / 2)
	fXMax = fXMin + PLAYER_NAME_RECT_W
	fYMin = fY - (RECT_HEIGHT_CONSTANT / 2)
	fYMax = fYMin + RECT_HEIGHT_CONSTANT
		
	IF fCursorX >= fXMin
	AND fCursorX <= fXMax
	AND fCursorY >= fYMin
	AND fCursorY <= fYMax
		IF bDrawRect = TRUE
			DRAW_RECT(fX, fY, PLAYER_NAME_RECT_W, RECT_HEIGHT_CONSTANT, 255, 255, 255, 32)
		ENDIF
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_PLAYER_CARD_BUTTON_BEEN_PRESSED(BOOL bSpectator = FALSE)

	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
	AND IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
	AND IS_MOUSE_INSIDE_PLAYER_RECT(g_i_ActualLeaderboardPlayerSelected, FALSE)
		RETURN TRUE
	ENDIF
	
	IF bSpectator
		IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_SELECT)		

			PRINTLN("------------------------------------------------------------------------------")
			PRINTLN("--                                                                          --")
			PRINTLN("--                   PLAYER_CARD_PRESSED, bSpectator	                     --")
			PRINTLN("--                                                                          --")
			PRINTLN("------------------------------------------------------------------------------")

			RETURN TRUE
		ENDIF
	ELSE
		IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_SELECT)		

			PRINTLN("------------------------------------------------------------------------------")
			PRINTLN("--                                                                          --")
			PRINTLN("--                   PLAYER_CARD_PRESSED				                     --")
			PRINTLN("--                                                                          --")
			PRINTLN("------------------------------------------------------------------------------")

			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

CONST_INT SPEC_INITIALISE 	0
CONST_INT SPEC_CARDS		1

PROC HANDLE_SPECTATOR_PLAYER_CARDS(LBD_VARS& lbdVars, INT& iSavedSpectatorRow[], INT iLamarRow = -1)

	INT i, iPlayer
	GAMER_HANDLE tempHandle
	PLAYER_INDEX PlayerId
	
	SWITCH lbdVars.iProgressSpc
		CASE SPEC_INITIALISE
			REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i	
				iSavedSpectatorRow[i] = -1
			ENDREPEAT
			PRINTLN("[CS_GC]  iProgressSpc = SPEC_CARDS")
			lbdVars.iProgressSpc = SPEC_CARDS
		BREAK
		CASE SPEC_CARDS
			IF HAS_PLAYER_CARD_BUTTON_BEEN_PRESSED()
				REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i		
					IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
						PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
						iPlayer = NATIVE_TO_INT(PlayerId)
						IF IS_NET_PLAYER_OK(PlayerId, FALSE)
							IF (g_i_ActualLeaderboardPlayerSelected) = iSavedSpectatorRow[iPlayer]
							AND (g_i_ActualLeaderboardPlayerSelected) <> iLamarRow
								tempHandle = GET_GAMER_HANDLE_PLAYER(PlayerId)
								PRINTLN("[CS_GC] HANDLE_SPECTATOR_PLAYER_CARDS, NETWORK_SHOW_PROFILE_UI for ", iPlayer, " g_i_ActualLeaderboardPlayerSelected = ", g_i_ActualLeaderboardPlayerSelected, " iSavedSpectatorRow = ", iSavedSpectatorRow[iPlayer])
								NETWORK_SHOW_PROFILE_UI(tempHandle)
								PLAY_SOUND_FRONTEND(-1,"SELECT","HUD_FRONTEND_DEFAULT_SOUNDSET")
							ELSE
								PRINTLN("[CS_GC]  iProgress = g_i_ActualLeaderboardPlayerSelected = ", g_i_ActualLeaderboardPlayerSelected, " iSavedSpectatorRow[iPlayer] = ", iSavedSpectatorRow[iPlayer])
							ENDIF
						ENDIF
					ENDIF
				ENDREPEAT
			ELSE
				PRINTLN("[CS_GC]  iProgress = HAS_PLAYER_CARD_BUTTON_BEEN_PRESSED")
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC HANDLE_THE_PLAYER_CARDS(THE_LEADERBOARD_STRUCT& leaderboard[], LBD_VARS& lbdVars, INT& iSavedPlayerRows[], BOOL bJobSpectating)

	INT i, iPlayer
	GAMER_HANDLE tempHandle
	IF HAS_PLAYER_CARD_BUTTON_BEEN_PRESSED(bJobSpectating)
		REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i		
			iPlayer = NATIVE_TO_INT(leaderboard[i].PlayerId)
			IF (iPlayer <> -1)
				// Add one for Lamar
				IF (lbdVars.iLamarRow <> -1)
					IF (iSavedPlayerRows[iPlayer] >= lbdVars.iLamarRow)
						iSavedPlayerRows[iPlayer] = (iSavedPlayerRows[iPlayer] + 1)
					ENDIF
				ENDIF
				IF (g_i_ActualLeaderboardPlayerSelected <> lbdVars.iLamarRow)
					IF g_i_ActualLeaderboardPlayerSelected = iSavedPlayerRows[iPlayer]
						tempHandle = lbdVars.aGamer[iPlayer]
						IF IS_GAMER_HANDLE_VALID(tempHandle)
							PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
							NETWORK_SHOW_PROFILE_UI(tempHandle)
							PRINTLN("[CS_GC] NETWORK_SHOW_PROFILE_UI for iPlayer = ", iPlayer, " g_i_ActualLeaderboardPlayerSelected = ", g_i_ActualLeaderboardPlayerSelected, " iSavedPlayerRows = ", iSavedPlayerRows[iPlayer])
						ENDIF
					ELSE
						PRINTLN("[CS_GC] FAIL (g_i_ActualLeaderboardPlayerSelected) = ", g_i_ActualLeaderboardPlayerSelected, " iSavedPlayerRows = ", iSavedPlayerRows[iPlayer], " iPlayer = ", iPlayer)
					ENDIF
				ELSE
					PRINTLN("[CS_GC] iLamarRow = ", lbdVars.iLamarRow )
				ENDIF
			ELSE
				PRINTLN("[CS_GC] INVALID_PLAYER_INDEX = ", iPlayer, " i = ", i)
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC

PROC SCROLL_UP_LIST(INT& iPlayerSelection)//, INT iRowStart)
//	IF iPLayerSelection > iRowStart
		iPlayerSelection--
//	ENDIF
ENDPROC

PROC SCROLL_DOWN_LIST(INT& iPlayerSelection)//, INT iMaxRow)
//	IF iPlayerSelection < (iMaxRow - 1)
		iPlayerSelection++
//	ENDIF
ENDPROC

PROC JUMP_TO_ROW_ZERO(INT& iPlayerSelection)
	iPlayerSelection = 0
ENDPROC

FUNC BOOL IS_THIS_A_TEAM_ROW(INT iTeamRowBit, INT iPlayerSelection)
	RETURN iPLayerSelection >= 0 
	AND iPlayerSelection <= 31
	AND IS_BIT_SET(iTeamRowBit, iPlayerSelection)
ENDFUNC

FUNC BOOL IS_THIS_A_TEAM_NAME_ROW(INT iTeamNameRowBit, INT iPlayerSelection)
	RETURN iPLayerSelection >= 0 
	AND iPlayerSelection <= 31
	AND IS_BIT_SET(iTeamNameRowBit, iPlayerSelection)
ENDFUNC

FUNC BOOL HAS_PRESSED_UP_DOWN()
	IF IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
	
		RETURN TRUE
	ENDIF
	IF IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
	
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

PROC GRAB_SPECTATOR_PLAYER_SELECTED(THE_LEADERBOARD_STRUCT& leaderboard[], INT& iSavedPlayerRows[])
	IF HAS_PRESSED_UP_DOWN()
		INT i, iPlayer
		REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i		
			iPlayer = NATIVE_TO_INT(leaderboard[i].PlayerId)
			IF iPlayer <> -1	
				IF g_i_ActualLeaderboardPlayerSelected = iSavedPlayerRows[iPlayer]
					
					g_i_SpecLeaderboardPlayerSelected = iPlayer

					PRINTLN("[CS_SPEC] g_i_SpecLeaderboardPlayerSelected = ", g_i_SpecLeaderboardPlayerSelected)
				ELSE
					PRINTLN("[CS_SPEC] g_i_ActualLeaderboardPlayerSelected = ", g_i_ActualLeaderboardPlayerSelected, " iSavedPlayerRows = ", iSavedPlayerRows[iPlayer], " iPlayer = ", iPlayer)
				ENDIF
			ELSE
				PRINTLN("[CS_SPEC] INVALID_PLAYER_INDEX = ", iPlayer)
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC

FUNC INT GET_SCROLL_DELAY(LBD_VARS &lbdVars)
	
	INT iDelay = 200
	
	GET_CONTROL_VALUE_OF_ANALOGUE_STICKS(lbdVars.sInputData.leftStickLR, lbdVars.sInputData.leftStickUD, lbdVars.sInputData.rightStickLR, lbdVars.sInputData.rightStickUD, TRUE)
	
	FLOAT fDELAY_MULTIPLIER = 1.0
	
	//left stick & dpad up-down
	IF (fDELAY_MULTIPLIER = 1.0)
		IF NOT lbdVars.sInputData.bLeftStickUDReset
		OR NOT lbdVars.sInputData.bDPADDOWNReset
		OR NOT lbdVars.sInputData.bDPADUPReset
			IF lbdVars.sInputData.iLeftStickUDTimerDelay <= 0
				lbdVars.sInputData.iLeftStickUDTimerDelay = GET_GAME_TIMER()
				IF NETWORK_IS_GAME_IN_PROGRESS()
					lbdVars.sInputData.iMP_LeftStickUDTimerDelay = GET_NETWORK_TIME()
				ENDIF
			ENDIF
			
			IF lbdVars.sInputData.iLeftStickUDTimerDelay > 0
				IF (NETWORK_IS_GAME_IN_PROGRESS() AND IS_TIME_MORE_THAN(GET_NETWORK_TIME(), GET_TIME_OFFSET(lbdVars.sInputData.iMP_LeftStickUDTimerDelay, (iDelay*3))))
				OR (NOT NETWORK_IS_GAME_IN_PROGRESS() AND (GET_GAME_TIMER() - lbdVars.sInputData.iLeftStickUDTimerDelay) > (iDelay*3))
					fDELAY_MULTIPLIER = 0.25
				ENDIF
			ENDIF
		ELSE
			lbdVars.sInputData.iLeftStickUDTimerDelay = 0
		ENDIF
	ENDIF
	
	//left stick & dpad left-right
	IF (fDELAY_MULTIPLIER = 1.0)
		IF NOT lbdVars.sInputData.bLeftStickLRReset
		OR NOT lbdVars.sInputData.bDPADLEFTReset
		OR NOT lbdVars.sInputData.bDPADRIGHTReset
			IF lbdVars.sInputData.iLeftStickLRTimerDelay <= 0
				lbdVars.sInputData.iLeftStickLRTimerDelay = GET_GAME_TIMER()
				IF NETWORK_IS_GAME_IN_PROGRESS()
					lbdVars.sInputData.iMP_LeftStickLRTimerDelay = GET_NETWORK_TIME()
				ENDIF
			ENDIF
			
			IF lbdVars.sInputData.iLeftStickLRTimerDelay > 0
				IF (NETWORK_IS_GAME_IN_PROGRESS() AND IS_TIME_MORE_THAN(GET_NETWORK_TIME(), GET_TIME_OFFSET(lbdVars.sInputData.iMP_LeftStickLRTimerDelay, (iDelay*3))))
				OR (NOT NETWORK_IS_GAME_IN_PROGRESS() AND (GET_GAME_TIMER() - lbdVars.sInputData.iLeftStickLRTimerDelay) > (iDelay*3))
					fDELAY_MULTIPLIER = 0.25
				ENDIF
			ENDIF
		ELSE
			lbdVars.sInputData.iLeftStickLRTimerDelay = 0
		ENDIF
	ENDIF
	
	//right stick up-down
	IF (fDELAY_MULTIPLIER = 1.0)
		IF NOT lbdVars.sInputData.bRightStickUDReset
			IF lbdVars.sInputData.iRightStickUDTimerDelay <= 0
				lbdVars.sInputData.iRightStickUDTimerDelay = GET_GAME_TIMER()
				IF NETWORK_IS_GAME_IN_PROGRESS()
					lbdVars.sInputData.iMP_RightStickUDTimerDelay = GET_NETWORK_TIME()
				ENDIF
			ENDIF
			
			IF lbdVars.sInputData.iRightStickUDTimerDelay > 0
				IF (NETWORK_IS_GAME_IN_PROGRESS() AND IS_TIME_MORE_THAN(GET_NETWORK_TIME(), GET_TIME_OFFSET(lbdVars.sInputData.iMP_RightStickUDTimerDelay, (iDelay*3))))
				OR (NOT NETWORK_IS_GAME_IN_PROGRESS() AND (GET_GAME_TIMER() - lbdVars.sInputData.iRightStickUDTimerDelay) > (iDelay*3))
					fDELAY_MULTIPLIER = 0.25
				ENDIF
			ENDIF
		ELSE
			lbdVars.sInputData.iRightStickUDTimerDelay = 0
		ENDIF
	ENDIF
	
	//right stick left-right
	IF (fDELAY_MULTIPLIER = 1.0)
		IF NOT lbdVars.sInputData.bRightStickLRReset
			IF lbdVars.sInputData.iRightStickLRTimerDelay <= 0
				lbdVars.sInputData.iRightStickLRTimerDelay = GET_GAME_TIMER()
				IF NETWORK_IS_GAME_IN_PROGRESS()
					lbdVars.sInputData.iMP_RightStickLRTimerDelay = GET_NETWORK_TIME()
				ENDIF
			ENDIF
			
			IF lbdVars.sInputData.iRightStickLRTimerDelay > 0
				IF (NETWORK_IS_GAME_IN_PROGRESS() AND IS_TIME_MORE_THAN(GET_NETWORK_TIME(), GET_TIME_OFFSET(lbdVars.sInputData.iMP_RightStickLRTimerDelay, (iDelay*3))))
				OR (NOT NETWORK_IS_GAME_IN_PROGRESS() AND (GET_GAME_TIMER() - lbdVars.sInputData.iRightStickLRTimerDelay) > (iDelay*3))
					fDELAY_MULTIPLIER = 0.25
				ENDIF
			ENDIF
		ELSE
			lbdVars.sInputData.iRightStickLRTimerDelay = 0
		ENDIF
	ENDIF
	
	iDelay = FLOOR(TO_FLOAT(iDelay)*fDELAY_MULTIPLIER)
	
	IF NOT lbdVars.sInputData.bLeftStickUDReset
		IF (NETWORK_IS_GAME_IN_PROGRESS() AND IS_TIME_MORE_THAN(GET_NETWORK_TIME(), GET_TIME_OFFSET(lbdVars.sInputData.iMP_LeftStickUDTimer, iDelay)))
		OR (NOT NETWORK_IS_GAME_IN_PROGRESS() AND (GET_GAME_TIMER() - lbdVars.sInputData.iLeftStickUDTimer) > iDelay)
		OR ((lbdVars.sInputData.leftStickUD < 75) AND (lbdVars.sInputData.leftStickUD > -75))
			lbdVars.sInputData.bLeftStickUDReset = TRUE
		ENDIF		
	ENDIF
	
	IF NOT lbdVars.sInputData.bLeftStickLRReset
		IF (NETWORK_IS_GAME_IN_PROGRESS() AND IS_TIME_MORE_THAN(GET_NETWORK_TIME(), GET_TIME_OFFSET(lbdVars.sInputData.iMP_LeftStickLRTimer, iDelay)))
		OR (NOT NETWORK_IS_GAME_IN_PROGRESS() AND (GET_GAME_TIMER() - lbdVars.sInputData.iLeftStickLRTimer) > iDelay)
		OR ((lbdVars.sInputData.leftStickLR < 75) AND (lbdVars.sInputData.leftStickLR > -75))
			lbdVars.sInputData.bLeftStickLRReset = TRUE
		ENDIF
	ENDIF
	
	IF NOT lbdVars.sInputData.bRightStickUDReset
		IF (NETWORK_IS_GAME_IN_PROGRESS() AND IS_TIME_MORE_THAN(GET_NETWORK_TIME(), GET_TIME_OFFSET(lbdVars.sInputData.iMP_RightStickUDTimer, iDelay)))
		OR (NOT NETWORK_IS_GAME_IN_PROGRESS() AND (GET_GAME_TIMER() - lbdVars.sInputData.iRightStickUDTimer) > iDelay)
		OR ((lbdVars.sInputData.rightStickUD < 75) AND (lbdVars.sInputData.rightStickUD > -75))
			lbdVars.sInputData.bRightStickUDReset = TRUE
		ENDIF
	ENDIF
	
	IF NOT lbdVars.sInputData.bRightStickLRReset
		IF (NETWORK_IS_GAME_IN_PROGRESS() AND IS_TIME_MORE_THAN(GET_NETWORK_TIME(), GET_TIME_OFFSET(lbdVars.sInputData.iMP_RightStickLRTimer, iDelay)))
		OR (NOT NETWORK_IS_GAME_IN_PROGRESS() AND (GET_GAME_TIMER() - lbdVars.sInputData.iRightStickLRTimer) > iDelay)
		OR ((lbdVars.sInputData.rightStickLR < 75) AND (lbdVars.sInputData.rightStickLR > -75))
			lbdVars.sInputData.bRightStickLRReset = TRUE
		ENDIF
	ENDIF
	
	
	IF NOT lbdVars.sInputData.bDPADUPReset
		IF (NETWORK_IS_GAME_IN_PROGRESS() AND IS_TIME_MORE_THAN(GET_NETWORK_TIME(), GET_TIME_OFFSET(lbdVars.sInputData.iMP_LeftStickUDTimer, iDelay)))
		OR (NOT NETWORK_IS_GAME_IN_PROGRESS() AND (GET_GAME_TIMER() - lbdVars.sInputData.iLeftStickUDTimer) > iDelay)
			lbdVars.sInputData.bDPADUPReset = TRUE
		ENDIF		
	ENDIF
	
	IF NOT lbdVars.sInputData.bDPADDOWNReset
		IF (NETWORK_IS_GAME_IN_PROGRESS() AND IS_TIME_MORE_THAN(GET_NETWORK_TIME(), GET_TIME_OFFSET(lbdVars.sInputData.iMP_LeftStickUDTimer, iDelay)))
		OR (NOT NETWORK_IS_GAME_IN_PROGRESS() AND (GET_GAME_TIMER() - lbdVars.sInputData.iLeftStickUDTimer) > iDelay)
			lbdVars.sInputData.bDPADDOWNReset = TRUE
		ENDIF
	ENDIF
	
	IF NOT lbdVars.sInputData.bDPADLEFTReset
		IF (NETWORK_IS_GAME_IN_PROGRESS() AND IS_TIME_MORE_THAN(GET_NETWORK_TIME(), GET_TIME_OFFSET(lbdVars.sInputData.iMP_LeftStickLRTimer, iDelay)))
		OR (NOT NETWORK_IS_GAME_IN_PROGRESS() AND (GET_GAME_TIMER() - lbdVars.sInputData.iLeftStickLRTimer) > iDelay)
			lbdVars.sInputData.bDPADLEFTReset = TRUE
		ENDIF
	ENDIF
	
	IF NOT lbdVars.sInputData.bDPADRIGHTReset
		IF (NETWORK_IS_GAME_IN_PROGRESS() AND IS_TIME_MORE_THAN(GET_NETWORK_TIME(), GET_TIME_OFFSET(lbdVars.sInputData.iMP_LeftStickLRTimer, iDelay)))
		OR (NOT NETWORK_IS_GAME_IN_PROGRESS() AND (GET_GAME_TIMER() - lbdVars.sInputData.iLeftStickLRTimer) > iDelay)
			lbdVars.sInputData.bDPADRIGHTReset = TRUE
		ENDIF
	ENDIF
	
	PRINTLN("GET_SCROLL_DELAY, iDelay = ", iDelay)
	
	RETURN iDelay
ENDFUNC

PROC PROCESS_LBD_SCROLL_LOGIC(LBD_VARS &lbdVars, TIME_DATATYPE& timeScrollDelay, INT& iPlayerSelection, INT iTeamRowBit, INT iTeamNameRowBit, INT iNumLeaderboardPlayers, 
								INT iNumTeams, BOOL bIsTeam, INT iNumSpectators = 0)
	
	INT iMoveUp
	BOOL bPressed
	BOOL bDoSounds
	INT iROWSTART = 0
	INT iROWEND
	
	IF iNumLeaderboardPlayers > 1
		bDoSounds = TRUE
	ENDIF
	
	// Team leaderboards
	IF bIsTeam
	AND NOT g_FinalRoundsLbd
		iROWSTART 	= 1
		iROWEND 	= (iNumLeaderboardPlayers + iNumTeams + ( iNumTeams - 1 ) + iNumSpectators) 
		PRINTLN("[SCROLL_DEBUG] PROCESS_LBD_SCROLL_LOGIC, bIsTeam, iROWEND = ", iROWEND, " iNumLeaderboardPlayers = ", iNumLeaderboardPlayers, " iNumTeams = ", iNumTeams, 
				" iNumSpectators = ", iNumSpectators)
	ELSE
		iROWEND = (iNumLeaderboardPlayers)
		PRINTLN("PROCESS_LBD_SCROLL_LOGIC, g_FinalRoundsLbd, iROWEND = ", iROWEND)
	ENDIF
	
	BOOL bOneTeamOnly = (bIsTeam AND (iNumTeams = 1))
	
	INT iScrollDelay = GET_SCROLL_DELAY(lbdVars)
	
	//DISPLAY_TEXT_WITH_NUMBER(0.1,0.1,"NUMBER",g_iMenuCursorItem)
	//DISPLAY_TEXT_WITH_NUMBER(0.1,0.2,"NUMBER",iPlayerSelection)
	
	// PC mouse select
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
	AND NOT IS_A_SPECTATOR_CAM_RUNNING()
	
		SET_MOUSE_CURSOR_THIS_FRAME()
		
		SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_CURSOR_X)
		SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_CURSOR_Y)
		SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
		
		//IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
		
			IF g_iMenuCursorItem > MENU_CURSOR_NO_ITEM
		
				IF g_iMenuCursorItem != iPlayerSelection
					iPlayerSelection = g_iMenuCursorItem
					g_iMenuCursorItem = MENU_CURSOR_NO_ITEM
					PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
				ENDIF
				
			ENDIF
				
		//ENDIF
		
	ELSE
		// CAPS
		IF iPlayerSelection < iROWSTART
			iPlayerSelection = iROWSTART
		ELIF iPlayerSelection >= iROWEND
			iPlayerSelection = (iROWEND -1)
		ENDIF
	ENDIF
	
	IF SHOULD_SELECTION_BE_INCREMENTED(timeScrollDelay, iMoveUp, bPressed, FALSE, FALSE, iScrollDelay, bDoSounds)
	
		IF iMoveUp > 0
		
			IF iPlayerSelection > iROWSTART		
			
				SCROLL_UP_LIST(iPlayerSelection)
				PRINTLN("[SCROLL_DEBUG] -  A SCROLL_UP_LIST ", iPlayerSelection, " iROWSTART = ", iROWSTART)
				
				IF bIsTeam
				AND NOT bOneTeamOnly
					IF IS_THIS_A_TEAM_ROW(iTeamRowBit, iPlayerSelection)
						lbdVars.iTeamRowsPassed--
						lbdVars.iTeamRowsPassed--
						PRINTLN("[SCROLL_DEBUG] - B SCROLL_UP_LIST - Team row hit at ", iPlayerSelection)
						SCROLL_UP_LIST(iPlayerSelection)
						SCROLL_UP_LIST(iPlayerSelection)
					ENDIF
					
					IF IS_THIS_A_TEAM_NAME_ROW(iTeamNameRowBit, iPlayerSelection)
						lbdVars.iTeamRowsPassed--
						lbdVars.iTeamRowsPassed--
						PRINTLN("[SCROLL_DEBUG] - C SCROLL_UP_LIST - Team Name hit at ", iPlayerSelection)
						SCROLL_UP_LIST(iPlayerSelection)
						SCROLL_UP_LIST(iPlayerSelection)
					ENDIF
				ENDIF
				
				// CAPS
				IF iPlayerSelection < iROWSTART
					iPlayerSelection = iROWSTART
					PRINTLN("[SCROLL_DEBUG] - D iPlayerSelection = iROWSTART ")
				ENDIF
				
			ELSE
				PRINTLN("[SCROLL_DEBUG] - E SCROLL_UP_LIST_debug iPlayerSelection = ", iPlayerSelection, " iROWSTART = ", iROWSTART)
			ENDIF	
			
		ELSE
		
			IF iPlayerSelection < (iROWEND -1)

				SCROLL_DOWN_LIST(iPlayerSelection)
				PRINTLN("[SCROLL_DEBUG] - SCROLL_DOWN_LIST ", iPlayerSelection, " iROWEND = ", iROWEND, " iNumTeams = ", iNumTeams)
				
				// Skip team bars
				IF bIsTeam
				AND NOT bOneTeamOnly
					IF IS_THIS_A_TEAM_ROW(iTeamRowBit, iPlayerSelection)
						lbdVars.iTeamRowsPassed++
						PRINTLN("[SCROLL_DEBUG] - SCROLL_DOWN_LIST - Team row hit at ", iPlayerSelection)
						SCROLL_DOWN_LIST(iPlayerSelection)
					ENDIF
					
					IF IS_THIS_A_TEAM_NAME_ROW(iTeamNameRowBit, iPlayerSelection) 
						lbdVars.iTeamRowsPassed++
						PRINTLN("[SCROLL_DEBUG] - SCROLL_DOWN_LIST - Team Name hit at ", iPlayerSelection)
						SCROLL_DOWN_LIST(iPlayerSelection)
					ENDIF
				ENDIF
				
				// CAPS
				IF iPlayerSelection >= iROWEND
					iPlayerSelection = (iROWEND -1)
				ENDIF
				
			ELSE
				PRINTLN("[SCROLL_DEBUG] - SCROLL_DOWN_LIST (FAIL) ", iPlayerSelection, " iROWEND = ", iROWEND)
			ENDIF
		ENDIF
		
		PRINTLN("[SCROLL_DEBUG] - Moved player selection to ", iPlayerSelection)
		PRINTLN("[SCROLL_DEBUG] - g_i_LeaderboardShift = ", g_i_LeaderboardShift)
		PRINTLN("[SCROLL_DEBUG] - g_i_LeaderboardRowCounter = ", g_i_LeaderboardRowCounter)	
		PRINTLN("[SCROLL_DEBUG] - g_i_ActualLeaderboardPlayerSelected = ", g_i_ActualLeaderboardPlayerSelected)
		
	ENDIF
	
	g_i_ActualLeaderboardPlayerSelected = iPlayerSelection
ENDPROC

CONST_INT THUMB_SAFETY_TIMEOUT 30000

FUNC BOOL SHOULD_ALLOW_THUMB_VOTE()

	IF NOT IS_THIS_A_MINI_GAME_TRANSITION_SESSION()
	AND NOT IS_THIS_A_MINIGAME_SCRIPT()
	AND NOT g_bOnHorde
	//AND NOT IS_THIS_TRANSITION_SESSION_SETTING_CHALLENGE_TIME()
	AND NOT IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL()
	AND NOT Is_Player_Currently_On_MP_Random_Event(PLAYER_ID())
	AND g_FMMC_STRUCT.bMissionIsPublished
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC STRING GET_TRANSPORT_CLASS_ICON_NAME(LBD_TRANSPORT_CLASS eTransportClass)

	SWITCH eTransportClass
		CASE LBD_CLASS_CARS
			RETURN "Leaderboard_Car_Colour_Icon"
		BREAK
		CASE LBD_CLASS_BOATS
			RETURN "Leaderboard_Transport_Boat_Icon"
		BREAK
		CASE LBD_CLASS_PLANES
			RETURN "Leaderboard_Transport_Plane_Icon"
		BREAK
		CASE LBD_CLASS_CHUTES
			RETURN "Leaderboard_Parachute_Colour_Icon"
		BREAK
		CASE LBD_CLASS_BIKES
			RETURN "Leaderboard_Transport_Bike_Icon"
		BREAK
		CASE LBD_CLASS_CYCLES
			RETURN "Leaderboard_Transport_Bicycle_Icon"
		BREAK
		CASE LBD_CLASS_HELI	
			RETURN "Leaderboard_Transport_Heli_Icon"
		BREAK	
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
		SCRIPT_ASSERT("GET_TRANSPORT_CLASS_ICON_NAME invalid eTransportClass ")
	#ENDIF
	
	
	RETURN ""
ENDFUNC

#IF IS_DEBUG_BUILD
	FUNC STRING GET_NAME_FOR_FAKE_LBD(INT iCounter)
		SWITCH iCounter
			CASE 0
				RETURN "Player 0"
			BREAK
			CASE 1
				RETURN "Player 1"
			BREAK
			CASE 2
				RETURN "Player 2"
			BREAK
			CASE 3
				RETURN "Player 3"
			BREAK
			CASE 4
				RETURN "Player 4"
			BREAK
			CASE 5
				RETURN "Player 5"
			BREAK
			CASE 6
				RETURN "Player 6"
			BREAK
			CASE 7
				RETURN "Player 7"
			BREAK
			CASE 8
				RETURN "Player 8"
			BREAK
			CASE 9
				RETURN "Player 9"
			BREAK
			CASE 10
				RETURN "Player 10"
			BREAK
			CASE 11
				RETURN "Player 11"
			BREAK
			CASE 12
				RETURN "Player 12"
			BREAK
			CASE 13
				RETURN "Player 13"
			BREAK
			CASE 14
				RETURN "Player 14"
			BREAK
			CASE 15
				RETURN "Player 15"
			BREAK
			CASE 16
				RETURN "Player 16"
			BREAK
			CASE 17
				RETURN "Player 17"
			BREAK
			CASE 18
				RETURN "Player 18"
			BREAK
			CASE 19
				RETURN "Player 19"
			BREAK
			CASE 20
				RETURN "Player 20"
			BREAK
			CASE 21
				RETURN "Player 21"
			BREAK
			CASE 22
				RETURN "Player 22"
			BREAK
			CASE 23
				RETURN "Player 23"
			BREAK
			CASE 24
				RETURN "Player 24"
			BREAK
			CASE 25
				RETURN "Player 25"
			BREAK
			CASE 26
				RETURN "Player 26"
			BREAK
			CASE 27
				RETURN "Player 27"
			BREAK
			CASE 28
				RETURN "Player 28"
			BREAK
			CASE 29
				RETURN "Player 29"
			BREAK
			CASE 30
				RETURN "Player 30"
			BREAK
			CASE 31
				RETURN "Player 31"
			BREAK
		ENDSWITCH
		
		RETURN "1234567891234567 "
	ENDFUNC
#ENDIF

FUNC HUD_COLOURS GET_RECT_SELECTION_COLOUR(LBD_SUB_MODE eSubMode, INT iLocalTeam, INT iEnemyTeam)

	IF IS_PLAYER_DEV_SPECTATOR(PLAYER_ID())
	OR g_FinalRoundsLbd
		RETURN HUD_COLOUR_FRIENDLY
	ENDIF
	
	INT iMaxTeams = 4
	IF eSubMode = SUB_RACE_TARGET	
		iMaxTeams = (NUM_NETWORK_PLAYERS/2)
	ENDIF
	
	IF IS_TEAM_COLOUR_OVERRIDE_FLAG_SET()
		//Use the player HUD colour as the team colour. Set bEndLeaderboard = TRUE to prevent special handling when spectating.
		RETURN GET_PLAYER_HUD_COLOUR(PLAYER_ID(), iEnemyTeam, DEFAULT, DEFAULT, TRUE)
	ELSE
		RETURN GET_HUD_COLOUR_FOR_OPPONENT_CHOSEN_TEAM(iLocalTeam, iEnemyTeam, TRUE, iMaxTeams)
	ENDIF
ENDFUNC

FUNC HUD_COLOURS GET_INACTIVE_COLOUR()

	IF IS_PLAYER_ON_A_PLAYLIST(PLAYER_ID())
		RETURN HUD_COLOUR_WHITE
	ENDIF
	
	RETURN HUD_COLOUR_GREY
ENDFUNC

FUNC BOOL SHOULD_PLAY_AGAIN()

	RETURN g_b_PlayAgain
ENDFUNC

FUNC BOOL SHOW_PLAYER_CARD_BUTTON()

	RETURN g_b_AddPlayerCard
ENDFUNC

FUNC BOOL SHOULD_DRAW_RESTART_BUTTONS()

	RETURN g_b_RestartButtons
ENDFUNC

FUNC BOOL IS_IT_OK_TO_DRAW_LEADERBOARD(BOOL bCheckReady)

	IF NOT IS_SAFE_TO_DRAW_ON_SCREEN()
		PRINTLN("[2056544] LBD DEBUG - IS_IT_OK_TO_DRAW_LEADERBOARD, IS_SAFE_TO_DRAW_ON_SCREEN ")
		RETURN FALSE
	ENDIF
	
	IF IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
		PRINTLN("[2056544] LBD DEBUG - IS_IT_OK_TO_DRAW_LEADERBOARD, HUDPART_TRANSITIONHUD ")
		RETURN FALSE
	ENDIF
	
	IF bCheckReady
		IF g_b_PopulateReady = FALSE
		
			PRINTLN("[2056544] LBD DEBUG - IS_IT_OK_TO_DRAW_LEADERBOARD, g_b_PopulateReady = FALSE ")
		
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_WARNING_MESSAGE_ACTIVE()
		PRINTLN("[2056544] LBD DEBUG - IS_IT_OK_TO_DRAW_LEADERBOARD, IS_WARNING_MESSAGE_ACTIVE ")
	
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_SPECTATING(PLAYER_ID())
		IF IS_PAUSE_MENU_ACTIVE()
			PRINTLN("[2056544] LBD DEBUG - IS_IT_OK_TO_DRAW_LEADERBOARD, (IS_PLAYER_SPECTATING), IS_PAUSE_MENU_ACTIVE ")
			RETURN FALSE
		ENDIF
	ENDIF
	
	//If we should get out because the NJVS is being skipped
	IF SHOULD_TRANSITION_SESSIONS_SKIP_LB_AND_NJVS()
		PRINTLN("[2056544] LBD DEBUG - IS_IT_OK_TO_DRAW_LEADERBOARD, SHOULD_TRANSITION_SESSIONS_SKIP_LB_AND_NJVS ")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC INIT_MPHUD_LEADERBOARD_RUNNING(LEADERBOARD_PLACEMENT_TOOLS& Placement)

	RESET_ALL_SPRITE_PLACEMENT_VALUES(Placement.SpritePlacement)
	
	// temp remove
	IF SDPAD_X = 0
	AND SDPAD_Y = 0
	AND SDPAD_H = 0
	AND SDPAD_W = 0
	ENDIF
		
	// TEXT
	
	////////////////	Large Pos
	Placement.TextPlacement[TEXT_PLACEMENT_LB_LARGE_POS].x = LARGE_POS_X
	Placement.TextPlacement[TEXT_PLACEMENT_LB_LARGE_POS].y = LARGE_POS_Y
	
	////////////////	POSITION
	Placement.TextPlacement[TEXT_PLACEMENT_POSITION].x = POSITION_POS_X
	Placement.TextPlacement[TEXT_PLACEMENT_POSITION].y = VARIABLE_BASE_Y
	
	////////////////	COLUMN 2
	Placement.TextPlacement[TEXT_PLACEMENT_PLAYER_NAMES].x = BIG_PLAYER_NAMES_X
	Placement.TextPlacement[TEXT_PLACEMENT_PLAYER_NAMES].y = VARIABLE_BASE_Y
	
//	////////////////	CARS
//	Placement.TextPlacement[TEXT_PLACEMENT_COLUMN_4].x = CARS_POS_X
//	Placement.TextPlacement[TEXT_PLACEMENT_COLUMN_4].y = VARIABLE_BASE_Y
	
//	////////////////	COLUMN 5
//	Placement.TextPlacement[TEXT_PLACEMENT_KILLS].x = COLUMN_VARIABLE_5
//	Placement.TextPlacement[TEXT_PLACEMENT_KILLS].y = VARIABLE_BASE_Y

//	////////////////	BEST LAP
//	Placement.TextPlacement[TEXT_PLACEMENT_COLUMN_6].x = RACE_BEST_LAP_POS 
//	Placement.TextPlacement[TEXT_PLACEMENT_COLUMN_6].y = VARIABLE_BASE_Y
	
//	////////////////	TOTAL TIME
//	Placement.TextPlacement[TEXT_PLACEMENT_TIME].x = TIME_POS_X
//	Placement.TextPlacement[TEXT_PLACEMENT_TIME].y = VARIABLE_BASE_Y
	
//	////////////////	K/D RATIO
//	Placement.TextPlacement[TEXT_PLACEMENT_RATIO].x = COLUMN_VARIABLE_10
//	Placement.TextPlacement[TEXT_PLACEMENT_RATIO].y = VARIABLE_BASE_Y
	
	////////////////	Your team score
	Placement.TextPlacement[TEXT_PLACEMENT_TEAM_SCORE].x = YOUR_TEAM_SCORE_X
	Placement.TextPlacement[TEXT_PLACEMENT_TEAM_SCORE].y = TEAM_SCORE_Y
	
//	////////////////	Cash earned
//	Placement.TextPlacement[TEXT_PLACEMENT_CASH_EARNED].x = PLAYER_CASH_EARNED_X
//	Placement.TextPlacement[TEXT_PLACEMENT_CASH_EARNED].y = VARIABLE_BASE_Y
	
	////////////////	XP earned
//	Placement.TextPlacement[TEXT_PLACEMENT_XP_EARNED].x = PLAYER_XP_EARNED_X
//	Placement.TextPlacement[TEXT_PLACEMENT_XP_EARNED].y = VARIABLE_BASE_Y
	
	////////////////	Column 1
	Placement.TextPlacement[TEXT_PLACEMENT_COLUMN1].x = PLAYER_XP_EARNED_X
	Placement.TextPlacement[TEXT_PLACEMENT_COLUMN1].y = VARIABLE_BASE_Y
	
	Placement.TextPlacement[TEXT_PLACEMENT_COLUMN2].x = PLAYER_XP_EARNED_X
	Placement.TextPlacement[TEXT_PLACEMENT_COLUMN2].y = VARIABLE_BASE_Y
	
	Placement.TextPlacement[TEXT_PLACEMENT_COLUMN3].x = PLAYER_XP_EARNED_X
	Placement.TextPlacement[TEXT_PLACEMENT_COLUMN3].y = VARIABLE_BASE_Y
	
	Placement.TextPlacement[TEXT_PLACEMENT_COLUMN4].x = PLAYER_XP_EARNED_X
	Placement.TextPlacement[TEXT_PLACEMENT_COLUMN4].y = VARIABLE_BASE_Y
	
	Placement.TextPlacement[TEXT_PLACEMENT_COLUMN5].x = PLAYER_XP_EARNED_X
	Placement.TextPlacement[TEXT_PLACEMENT_COLUMN5].y = VARIABLE_BASE_Y
	
	Placement.TextPlacement[TEXT_PLACEMENT_COLUMN6].x = PLAYER_XP_EARNED_X
	Placement.TextPlacement[TEXT_PLACEMENT_COLUMN6].y = VARIABLE_BASE_Y
	
//	Placement.TextPlacement[TEXT_PLACEMENT_TITLE].x = PLAYER_XP_EARNED_X
//	Placement.TextPlacement[TEXT_PLACEMENT_TITLE].y = VARIABLE_BASE_Y
	
	Placement.TextPlacement[TEXT_PLACEMENT_THUMB_COUNT].x = TEXT_VOTE_UP_X
	Placement.TextPlacement[TEXT_PLACEMENT_THUMB_COUNT].y = TEXT_VOTE_Y
	
	Placement.TextPlacement[TEXT_PLACEMENT_TEAM_TOTALS].x = TEAM_TOTAL_X
	Placement.TextPlacement[TEXT_PLACEMENT_TEAM_TOTALS].y = VARIABLE_BASE_Y

	// SPRITES
	
	// Debug draw rank badge
	Placement.SpritePlacement[SPRITE_PLACEMENT_LB_DDS].x = DDS_X
	Placement.SpritePlacement[SPRITE_PLACEMENT_LB_DDS].y = DDS_Y
	Placement.SpritePlacement[SPRITE_PLACEMENT_LB_DDS].w = DDS_W
	Placement.SpritePlacement[SPRITE_PLACEMENT_LB_DDS].h = DDS_H
	Placement.SpritePlacement[SPRITE_PLACEMENT_LB_DDS].r = 255
	Placement.SpritePlacement[SPRITE_PLACEMENT_LB_DDS].g = 255
	Placement.SpritePlacement[SPRITE_PLACEMENT_LB_DDS].b = 255
	Placement.SpritePlacement[SPRITE_PLACEMENT_LB_DDS].a = 100
	
	Placement.SpritePlacement[SPRITE_PLACEMENT_LB_DDS].x = DPAD_DDS_X
	Placement.SpritePlacement[SPRITE_PLACEMENT_LB_DDS].y = DPAD_DDS_Y
	Placement.SpritePlacement[SPRITE_PLACEMENT_LB_DDS].w = DPAD_DDS_W
	Placement.SpritePlacement[SPRITE_PLACEMENT_LB_DDS].h = DPAD_DDS_H
	Placement.SpritePlacement[SPRITE_PLACEMENT_LB_DDS].A = DPAD_DDS_A
	Placement.SpritePlacement[SPRITE_PLACEMENT_LB_DDS].r = 255
	Placement.SpritePlacement[SPRITE_PLACEMENT_LB_DDS].g = 255
	Placement.SpritePlacement[SPRITE_PLACEMENT_LB_DDS].b = 255	
	
	Placement.SpritePlacement[SPRITE_PLACEMENT_CARS].x = 0.600
	Placement.SpritePlacement[SPRITE_PLACEMENT_CARS].y = 0.467
	Placement.SpritePlacement[SPRITE_PLACEMENT_CARS].w = 1
	Placement.SpritePlacement[SPRITE_PLACEMENT_CARS].h = 1
	Placement.SpritePlacement[SPRITE_PLACEMENT_CARS].r = 0
	Placement.SpritePlacement[SPRITE_PLACEMENT_CARS].g = 0
	Placement.SpritePlacement[SPRITE_PLACEMENT_CARS].b = 0
	Placement.SpritePlacement[SPRITE_PLACEMENT_CARS].a = 255	
	
	Placement.SpritePlacement[SPRITE_PLACEMENT_LAP_STAR].x = 0.600
	Placement.SpritePlacement[SPRITE_PLACEMENT_LAP_STAR].y = 0.467
	Placement.SpritePlacement[SPRITE_PLACEMENT_LAP_STAR].w = 1
	Placement.SpritePlacement[SPRITE_PLACEMENT_LAP_STAR].h = 1
	Placement.SpritePlacement[SPRITE_PLACEMENT_LAP_STAR].r = 0
	Placement.SpritePlacement[SPRITE_PLACEMENT_LAP_STAR].g = 0
	Placement.SpritePlacement[SPRITE_PLACEMENT_LAP_STAR].b = 0
	Placement.SpritePlacement[SPRITE_PLACEMENT_LAP_STAR].a = 255
	
	Placement.SpritePlacement[SPRITE_PLACEMENT_VEHICLE_BLOB].x = 0.600
	Placement.SpritePlacement[SPRITE_PLACEMENT_VEHICLE_BLOB].y = 0.467
	Placement.SpritePlacement[SPRITE_PLACEMENT_VEHICLE_BLOB].w = 1
	Placement.SpritePlacement[SPRITE_PLACEMENT_VEHICLE_BLOB].h = 1
	Placement.SpritePlacement[SPRITE_PLACEMENT_VEHICLE_BLOB].r = 0
	Placement.SpritePlacement[SPRITE_PLACEMENT_VEHICLE_BLOB].g = 0
	Placement.SpritePlacement[SPRITE_PLACEMENT_VEHICLE_BLOB].b = 0
	Placement.SpritePlacement[SPRITE_PLACEMENT_VEHICLE_BLOB].a = 255
	
	Placement.SpritePlacement[SPRITE_PLACEMENT_GRADIENT_HEADER].x = GRAD_X
	Placement.SpritePlacement[SPRITE_PLACEMENT_GRADIENT_HEADER].y = GRAD_Y
	Placement.SpritePlacement[SPRITE_PLACEMENT_GRADIENT_HEADER].w = GRAD_W
	Placement.SpritePlacement[SPRITE_PLACEMENT_GRADIENT_HEADER].h = GRAD_H
	Placement.SpritePlacement[SPRITE_PLACEMENT_GRADIENT_HEADER].r = 255
	Placement.SpritePlacement[SPRITE_PLACEMENT_GRADIENT_HEADER].g = 255
	Placement.SpritePlacement[SPRITE_PLACEMENT_GRADIENT_HEADER].b = 255
	Placement.SpritePlacement[SPRITE_PLACEMENT_GRADIENT_HEADER].a = 100
	
//	Placement.SpritePlacement[SPRITE_PLACEMENT_LBD_THUMBS].x = LBVOTE_H
//	Placement.SpritePlacement[SPRITE_PLACEMENT_LBD_THUMBS].y = LBVOTE_W
//	Placement.SpritePlacement[SPRITE_PLACEMENT_LBD_THUMBS].w = LBVOTE_X
//	Placement.SpritePlacement[SPRITE_PLACEMENT_LBD_THUMBS].h = LBVOTE_Y
//	Placement.SpritePlacement[SPRITE_PLACEMENT_LBD_THUMBS].r = 255
//	Placement.SpritePlacement[SPRITE_PLACEMENT_LBD_THUMBS].g = 255
//	Placement.SpritePlacement[SPRITE_PLACEMENT_LBD_THUMBS].b = 255
//	Placement.SpritePlacement[SPRITE_PLACEMENT_LBD_THUMBS].a = 100
	
	// RECTS
	Placement.RectPlacement[RECT_PLACEMENT_WHITE_TITLES].x = COLUMN_HEADING_RECT_X  
	Placement.RectPlacement[RECT_PLACEMENT_WHITE_TITLES].y = COLUMN_HEADING_RECT_Y
	Placement.RectPlacement[RECT_PLACEMENT_WHITE_TITLES].w = COLUMN_HEADING_RECT_W
	Placement.RectPlacement[RECT_PLACEMENT_WHITE_TITLES].h = RECT_HEIGHT_CONSTANT
	Placement.RectPlacement[RECT_PLACEMENT_WHITE_TITLES].r = 0
	Placement.RectPlacement[RECT_PLACEMENT_WHITE_TITLES].g = 0
	Placement.RectPlacement[RECT_PLACEMENT_WHITE_TITLES].b = 0
	Placement.RectPlacement[RECT_PLACEMENT_WHITE_TITLES].a = 204
	
	// COLUMN RECTS
	
	////////////////	RECT_PLACEMENT_HORZ
	Placement.RectPlacement[RECT_PLACEMENT_HORZ].x = PLAYER_NAME_RECT_X
	Placement.RectPlacement[RECT_PLACEMENT_HORZ].y = COLUMN_RECT_Y
	Placement.RectPlacement[RECT_PLACEMENT_HORZ].w = PLAYER_NAME_RECT_W
	Placement.RectPlacement[RECT_PLACEMENT_HORZ].h = RECT_HEIGHT_CONSTANT
	Placement.RectPlacement[RECT_PLACEMENT_HORZ].r = 0
	Placement.RectPlacement[RECT_PLACEMENT_HORZ].g = 0
	Placement.RectPlacement[RECT_PLACEMENT_HORZ].b = 0
	Placement.RectPlacement[RECT_PLACEMENT_HORZ].a = 204
	
	
	////////////////	CAPS
	Placement.RectPlacement[RECT_PLACEMENT_LONG_CAP].x = COLUMN_CAP1_X	
	Placement.RectPlacement[RECT_PLACEMENT_LONG_CAP].y = COLUMN_CAPS_Y_ALL	
	Placement.RectPlacement[RECT_PLACEMENT_LONG_CAP].w = COLUMN_DM__RECT_CONST_WIDTH	
	Placement.RectPlacement[RECT_PLACEMENT_LONG_CAP].h = COLUMN_CAP_H_ALL	
	Placement.RectPlacement[RECT_PLACEMENT_LONG_CAP].r = 0
	Placement.RectPlacement[RECT_PLACEMENT_LONG_CAP].g = 0
	Placement.RectPlacement[RECT_PLACEMENT_LONG_CAP].b = 0
	Placement.RectPlacement[RECT_PLACEMENT_LONG_CAP].a = 204
	
	Placement.RectPlacement[RECT_PLACEMENT_POS].x = RED_X	
	Placement.RectPlacement[RECT_PLACEMENT_POS].y = COLUMN_RECT_Y	
	Placement.RectPlacement[RECT_PLACEMENT_POS].w = RED_W	
	Placement.RectPlacement[RECT_PLACEMENT_POS].h = RECT_HEIGHT_CONSTANT	
	Placement.RectPlacement[RECT_PLACEMENT_POS].a = 255	
	
	Placement.RectPlacement[RECT_PLACEMENT_BLACK_POS].x = RED_X	
	Placement.RectPlacement[RECT_PLACEMENT_BLACK_POS].y = COLUMN_RECT_Y	
	Placement.RectPlacement[RECT_PLACEMENT_BLACK_POS].w = RED_W	
	Placement.RectPlacement[RECT_PLACEMENT_BLACK_POS].h = RECT_HEIGHT_CONSTANT	
	Placement.RectPlacement[RECT_PLACEMENT_BLACK_POS].a = 255
	
	Placement.SpritePlacement[SPRITE_PLACEMENT_HEAD_POS].x = RED_X	
	Placement.SpritePlacement[SPRITE_PLACEMENT_HEAD_POS].y = COLUMN_RECT_Y	
	Placement.SpritePlacement[SPRITE_PLACEMENT_HEAD_POS].w = RED_W	
	Placement.SpritePlacement[SPRITE_PLACEMENT_HEAD_POS].h = RECT_HEIGHT_CONSTANT	
	Placement.SpritePlacement[SPRITE_PLACEMENT_HEAD_POS].r = 255	
	Placement.SpritePlacement[SPRITE_PLACEMENT_HEAD_POS].g = 255	
	Placement.SpritePlacement[SPRITE_PLACEMENT_HEAD_POS].b = 255	
	Placement.SpritePlacement[SPRITE_PLACEMENT_HEAD_POS].a = HUNDRED_ALPHA
	
	Placement.RectPlacement[RECT_PLACEMENT_COLOUR_POS].x = THIN_COLOUR_X	
	Placement.RectPlacement[RECT_PLACEMENT_COLOUR_POS].y = COLUMN_RECT_Y	
	Placement.RectPlacement[RECT_PLACEMENT_COLOUR_POS].w = THIN_COLOUR_W	
	Placement.RectPlacement[RECT_PLACEMENT_COLOUR_POS].h = RECT_HEIGHT_CONSTANT	
	Placement.RectPlacement[RECT_PLACEMENT_COLOUR_POS].a = 255
	
	Placement.RectPlacement[RECT_PLACEMENT_COUNT].x = COUNT_LONG__RECT_X	
	Placement.RectPlacement[RECT_PLACEMENT_COUNT].y = COLUMN_RECT_Y	
	Placement.RectPlacement[RECT_PLACEMENT_COUNT].w = COUNT_LONG__RECT_W	
	Placement.RectPlacement[RECT_PLACEMENT_COUNT].h = RECT_HEIGHT_CONSTANT	
	Placement.RectPlacement[RECT_PLACEMENT_COUNT].a = FIFTY_ALPHA	
	
	Placement.RectPlacement[RECT_PLACEMENT_VOTE_COUNT].x = COUNT_LONG__RECT_X	
	Placement.RectPlacement[RECT_PLACEMENT_VOTE_COUNT].y = COLUMN_RECT_Y	
	Placement.RectPlacement[RECT_PLACEMENT_VOTE_COUNT].w = COUNT_LONG__RECT_W	
	Placement.RectPlacement[RECT_PLACEMENT_VOTE_COUNT].h = RECT_HEIGHT_CONSTANT	
	Placement.RectPlacement[RECT_PLACEMENT_VOTE_COUNT].a = FIFTY_ALPHA	
	
	Placement.RectPlacement[RECT_PLACEMENT_VARS].x = VAR_RECT_X
	Placement.RectPlacement[RECT_PLACEMENT_VARS].y = COLUMN_RECT_Y 
	Placement.RectPlacement[RECT_PLACEMENT_VARS].w = VAR_RECT_W 
	Placement.RectPlacement[RECT_PLACEMENT_VARS].h = RECT_HEIGHT_CONSTANT 	
	Placement.RectPlacement[RECT_PLACEMENT_VARS].a = SIXTY_ALPHA
	
ENDPROC

FUNC BOOL SHOULD_DRAW_SCROLLBAR_COUNTER(LBD_VARS& lbdVars)
	RETURN FALSE // 2573261 "Drop Zone VI - Position counter in the bottom left started at "-1-16" rather than "1-16""
	RETURN lbdVars.bDrawScrollbars
ENDFUNC

PROC TRACK_LBD_ROW_FOR_SCROLL(LBD_VARS& lbdVars, INT iNumTeams, INT iNumLBDPlayers, BOOL bTeamRowAtEnd = FALSE)

	INT iMaxRow = MAX_ROWS_DRAWN
	
	IF iNumTeams + iNumLBDPlayers > iMaxRow
		lbdVars.bDrawScrollbars = TRUE	
	ELSE
		lbdVars.bDrawScrollbars = FALSE
	ENDIF
	
	IF bTeamRowAtEnd
		iMaxRow -= 1
	ENDIF
	
	IF g_i_ActualLeaderboardPlayerSelected > iMaxRow
		g_i_LeaderboardShift = (g_i_ActualLeaderboardPlayerSelected - iMaxRow)
	ELSE
		g_i_LeaderboardShift = 0
	ENDIF
	
	g_i_LeaderboardRowCounter = (g_i_ActualLeaderboardPlayerSelected - g_i_LeaderboardShift)
	
	lbdVars.iNumPlayersOnLbd = iNumLBDPlayers
	
ENDPROC



FUNC BOOL CREATE_LEADERBOARD_CAM()
	
	VECTOR vCamStartRot
	FLOAT fFov = 45.0
	INT iInterpTime = (1000/3)*2
	
	IF NOT DOES_CAM_EXIST(g_TransitionSessionNonResetVars.sPostMissionCleanupData.ciLeaderBoardCam)
		g_TransitionSessionNonResetVars.sPostMissionCleanupData.vLbCamStartCoords = GET_FINAL_RENDERED_CAM_COORD()
		vCamStartRot = GET_FINAL_RENDERED_CAM_ROT()
		fFov = GET_FINAL_RENDERED_CAM_FOV()
		NET_PRINT("[WJK] [RC] - CREATE_LEADERBOARD_CAM - vLbCamStartCoords = ")NET_PRINT_VECTOR(g_TransitionSessionNonResetVars.sPostMissionCleanupData.vLbCamStartCoords)NET_NL()
		NET_PRINT("[WJK] [RC] - CREATE_LEADERBOARD_CAM - vCamStartRot = ")NET_PRINT_VECTOR(vCamStartRot)NET_NL()
		NET_PRINT("[WJK] [RC] - CREATE_LEADERBOARD_CAM - fFov = ")NET_PRINT_FLOAT(fFov)NET_NL()
//		vCamEndRot = <<LEADERBOARD_CAMERA_ANGLE,0.0,vCamStartRot.z>>
		TOGGLE_RENDERPHASES(FALSE)
		ANIMPOSTFX_PLAY("MinigameTransitionIn", 0, TRUE)
		g_TransitionSessionNonResetVars.sPostMissionCleanupData.ciLeaderBoardCam = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA",g_TransitionSessionNonResetVars.sPostMissionCleanupData.vLbCamStartCoords,vCamStartRot)
		SET_CAM_FOV(g_TransitionSessionNonResetVars.sPostMissionCleanupData.ciLeaderBoardCam, fFov)
		SET_CAM_ACTIVE(g_TransitionSessionNonResetVars.sPostMissionCleanupData.ciLeaderBoardCam, TRUE)
		RENDER_SCRIPT_CAMS(TRUE, FALSE, iInterpTime, TRUE, TRUE)
		IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.bWantLbCamToPan
			SET_CAM_PARAMS(g_TransitionSessionNonResetVars.sPostMissionCleanupData.ciLeaderBoardCam, g_TransitionSessionNonResetVars.sPostMissionCleanupData.vLbCamStartCoords, vCamStartRot, fFov)
       		SET_CAM_PARAMS(g_TransitionSessionNonResetVars.sPostMissionCleanupData.ciLeaderBoardCam, g_TransitionSessionNonResetVars.sPostMissionCleanupData.vLbCamStartCoords+<<0.0, 0.0, 3.0>>, vCamStartRot, fFov, iInterpTime, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR) 
			NET_PRINT("[WJK] [RC] - CREATE_LEADERBOARD_CAM - bWantLbCamToPan = TRUE.")NET_NL()
			NET_PRINT("[WJK] [RC] - CREATE_LEADERBOARD_CAM - told cam to pan to coords: ")NET_PRINT_VECTOR(g_TransitionSessionNonResetVars.sPostMissionCleanupData.vLbCamStartCoords+<<0.0, 0.0, 3.0>>)NET_NL()
		ENDIF
		NETWORK_SET_IN_FREE_CAM_MODE(TRUE)
		g_TransitionSessionNonResetVars.sPostMissionCleanupData.bLeaderboardCamRenderingOrInterping = TRUE
		NET_PRINT("[WJK] [RC] - CREATE_LEADERBOARD_CAM - created leaderboard cam.")NET_NL()
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC REQUEST_LEADERBOARD_CAM(BOOL bWantPan = FALSE, BOOL bForceCreateThisFrame = FALSE, BOOL bForceNotSkyCam = FALSE)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("[WJK] - REQUEST_LEADERBOARD_CAM") NET_NL()
		NET_PRINT("[WJK] - Called by script ") NET_PRINT(GET_THIS_SCRIPT_NAME()) NET_NL()
		NET_PRINT("[WJK] - bWantLbCamToPan = ")NET_NL()
		IF bWantPan
			NET_PRINT("[WJK] - TRUE.")NET_NL()
		ELSE
			NET_PRINT("[WJK] - FALSE.")NET_NL()
		ENDIF
	#ENDIF
	
	IF ( (GET_FM_MISSION_TYPE_PLAYER_IS_ON(PLAYER_ID()) = FMMC_TYPE_MISSION) AND (NOT bForceNotSkyCam) AND NOT SHOULD_LOCAL_PLAYER_SPAWN_AT_LAMARS_HOUSE() )
		
		g_TransitionSessionNonResetVars.sPostMissionCleanupData.bWantSwoopCam = TRUE
		
	ELSE
		
		g_TransitionSessionNonResetVars.sPostMissionCleanupData.bWantLbCamToPan = bWantPan
		g_TransitionSessionNonResetVars.sPostMissionCleanupData.bRequestLeaderBoardCam = TRUE
		
		IF bForceCreateThisFrame
			NET_PRINT("[WJK] - REQUEST_LEADERBOARD_CAM - bForceCreateThisFrame = TRUE.")NET_NL()
			CREATE_LEADERBOARD_CAM()
		ENDIF
		
	ENDIF
	
ENDPROC

PROC CLEANUP_LEADERBOARD_CAM(BOOL bCleanupRequestFlag = TRUE, BOOL bResetRenderingOrInterpingFlag = TRUE, BOOL bStopRenderingAndDeleteCam = FALSE, BOOL bCleanupBlur = TRUE, BOOL doRenderFreeze = TRUE, BOOL bStopPostFx = TRUE)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("***  CLEANUP_LEADERBOARD_CAM *** ") NET_NL()
		NET_PRINT("     Called by script ") NET_PRINT(GET_THIS_SCRIPT_NAME()) NET_NL()
		DEBUG_PRINTCALLSTACK()
	#ENDIF
	
	SCRIPT_TIMER sTempTimer
	
	IF bCleanupRequestFlag
		g_TransitionSessionNonResetVars.sPostMissionCleanupData.bRequestLeaderBoardCam = FALSE
		NET_PRINT("[WJK] - CLEANUP_LEADERBOARD_CAM - bCleanupRequestFlag = TRUE, setting bRequestLeaderBoardCam = FALSE.") NET_NL()
	ENDIF
	
	g_TransitionSessionNonResetVars.sPostMissionCleanupData.bSetGroundAndHeading = FALSE
	
	IF bResetRenderingOrInterpingFlag
		g_TransitionSessionNonResetVars.sPostMissionCleanupData.bLeaderboardCamRenderingOrInterping = FALSE
		NET_PRINT("[WJK] - CLEANUP_LEADERBOARD_CAM - bLeaderboardCamRenderingOrInterping = FALSE.") NET_NL()
	ENDIF
	
	g_TransitionSessionNonResetVars.sPostMissionCleanupData.bLeaderBoardCamReady = FALSE
	g_TransitionSessionNonResetVars.sPostMissionCleanupData.bLeaderBoardCamPanned = FALSE
	
	RESET_NET_TIMER(g_TransitionSessionNonResetVars.sPostMissionCleanupData.stFilterEffectRunningTimer)
	g_TransitionSessionNonResetVars.sPostMissionCleanupData.stFilterEffectRunningTimer = sTempTimer
	
	RESET_NET_TIMER(g_TransitionSessionNonResetVars.sPostMissionCleanupData.stInterpBackToGameTimer)
	g_TransitionSessionNonResetVars.sPostMissionCleanupData.stInterpBackToGameTimer = sTempTimer
	
	RESET_NET_TIMER(g_TransitionSessionNonResetVars.sPostMissionCleanupData.stLbCamFailTimer)
	g_TransitionSessionNonResetVars.sPostMissionCleanupData.stLbCamFailTimer = sTempTimer
	
//	
//	IF NOT IS_TRANSITION_SESSION_LAUNCHING()
//		IF NOT IS_TRANSITION_ACTIVE()
//			FORCE_SKYBLUR_CLEAR_INSTANTLY()
//		ENDIF
//	ENDIF
//	
	RESET_NET_TIMER(MPglobals.tdTimefade)
	
	g_bCleanupReadyForLbCamToInterpBackToGame = FALSE
	g_TransitionSessionNonResetVars.sPostMissionCleanupData.iLeaderBoardCamStage = 0
	
	g_TransitionSessionNonResetVars.sPostMissionCleanupData.iState = 0
	
	IF NOT g_TransitionSessionNonResetVars.sPostMissionCleanupData.bJumpingToCleanupForSp
		
		IF doRenderFreeze
			IF NOT IS_CORONA_INITIALISING_A_QUICK_RESTART()
			AND NOT IS_THIS_A_ROUNDS_MISSION_FOR_CORONA()
			AND NOT g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingHeistCelebration
				NET_PRINT("[WJK] - CLEANUP_LEADERBOARD_CAM - TOGGLE_RENDERPHASES(TRUE) - SKYFREEZE_CLEAR.") NET_NL()
				TOGGLE_RENDERPHASES(TRUE)
			ENDIF
		ENDIF
		
		NETWORK_SET_IN_FREE_CAM_MODE(FALSE)
		NET_PRINT("[WJK] - CLEANUP_LEADERBOARD_CAM - called NETWORK_SET_IN_FREE_CAM_MODE(FALSE).") NET_NL()
		
		IF bStopRenderingAndDeleteCam
		
			NET_PRINT("[WJK] - CLEANUP_LEADERBOARD_CAM - bStopRenderingAndDeleteCam = TRUE.") NET_NL()
			
			IF DOES_CAM_EXIST(g_TransitionSessionNonResetVars.sPostMissionCleanupData.ciLeaderBoardCam)
				NET_PRINT("[WJK] - CLEANUP_LEADERBOARD_CAM - cam exists.") NET_NL()
//				IF NOT IS_PLAYER_SCTV(PLAYER_ID())
					RENDER_SCRIPT_CAMS(FALSE, FALSE, 1000, FALSE, TRUE)
					NET_PRINT("[WJK] - CLEANUP_LEADERBOARD_CAM - calling RENDER_SCRIPT_CAMS(FALSE).") NET_NL()
//				ENDIF
				SET_CAM_ACTIVE(g_TransitionSessionNonResetVars.sPostMissionCleanupData.ciLeaderBoardCam, FALSE)
				DESTROY_CAM(g_TransitionSessionNonResetVars.sPostMissionCleanupData.ciLeaderBoardCam, TRUE)
				NET_PRINT("[WJK] - CLEANUP_LEADERBOARD_CAM - set cam inactive and destroyed it.") NET_NL()
			ELSE
				NET_PRINT("[WJK] - CLEANUP_LEADERBOARD_CAM - cam does not exist.") NET_NL()
			ENDIF
			
		ENDIF
		
		IF bStopPostFx
			//-- Won't do this if it's just after the tutorial race, or it'll clear the screen blur
			IF NOT g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingHeistCelebration
				IF ANIMPOSTFX_IS_RUNNING("MinigameTransitionIn")
					NET_PRINT("[WJK] - CLEANUP_LEADERBOARD_CAM - bStopPostFx = TRUE.") NET_NL()
					ANIMPOSTFX_STOP("MinigameTransitionIn")
				ENDIF
			ENDIF
		ENDIF
		
		IF bCleanupBlur
			
			
			NET_PRINT("[WJK] - CLEANUP_LEADERBOARD_CAM - bCleanupBlur = TRUE.") NET_NL()
			IF (GET_CORONA_STATUS() = CORONA_STATUS_IDLE) // Fix bobby suggested for B*1392555.
				IF NOT g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingHeistCelebration
					NET_PRINT("[WJK] - FMMC_Launcher - CLEANUP_LEADERBOARD_CAM - (GET_CORONA_STATUS() = CORONA_STATUS_IDLE), calling DISABLE_SCREENBLUR_FADE().")NET_NL()
					DISABLE_SCREENBLUR_FADE()
				ENDIF
			#IF IS_DEBUG_BUILD
			ELSE
				NET_PRINT("[WJK] - FMMC_Launcher - CLEANUP_LEADERBOARD_CAM - (GET_CORONA_STATUS() != CORONA_STATUS_IDLE), not calling DISABLE_SCREENBLUR_FADE(), leaving blur on, Bobby will clean it.")NET_NL()
			#ENDIF
			ENDIF
		ENDIF
		
	ENDIF
	
	g_TransitionSessionNonResetVars.sPostMissionCleanupData.bWantSwoopCam = FALSE
	
	NET_PRINT("[WJK] [RC] - CLEANUP_LEADERBOARD_CAM - cleaned up leaderboard cam.")NET_NL()
	
ENDPROC

FUNC BOOL IS_LEADERBOARD_CAM_READY(BOOL bCheckingForEomScreenDisplay = FALSE)
	IF bCheckingForEomScreenDisplay
		RETURN TRUE
	ENDIF
	IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_BbNeedsFmCleanup)
	OR g_TransitionSessionNonResetVars.sPostMissionCleanupData.bComingOutOfPlaylist
	OR g_TransitionSessionNonResetVars.sPostMissionCleanupData.bWantSwoopCam
	OR (GET_FM_MISSION_TYPE_PLAYER_IS_ON(PLAYER_ID()) = FMMC_TYPE_MISSION)
		IF IS_SKYSWOOP_IN_SKY()
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN g_TransitionSessionNonResetVars.sPostMissionCleanupData.bLeaderBoardCamReady
ENDFUNC

FUNC BOOL IS_LEADERBOARD_CAM_REQUESTED()
	RETURN g_TransitionSessionNonResetVars.sPostMissionCleanupData.bRequestLeaderBoardCam
ENDFUNC


FUNC BOOL IS_LEADERBOARD_CAM_RENDERING_OR_INTERPING()
	RETURN g_TransitionSessionNonResetVars.sPostMissionCleanupData.bLeaderboardCamRenderingOrInterping
ENDFUNC

PROC MAINTAIN_LEADERBOARD_CAM_RENDERING_REQUESTS()
	
	IF IS_LEADERBOARD_CAM_READY()
		
		SWITCH g_TransitionSessionNonResetVars.sPostMissionCleanupData.iState
			
			// On state.
			CASE 0
				
				g_TransitionSessionNonResetVars.sPostMissionCleanupData.bLeaderboardCamRenderingOrInterping = TRUE
				
				IF IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_SCRIPT_CAMS_OFF_REQUESTED)
					IF DOES_CAM_EXIST(g_TransitionSessionNonResetVars.sPostMissionCleanupData.ciLeaderBoardCam)
						SET_CAM_ACTIVE(g_TransitionSessionNonResetVars.sPostMissionCleanupData.ciLeaderBoardCam, FALSE)
//						IF NOT IS_PLAYER_SCTV(PLAYER_ID())
							IF NOT IS_CAM_ACTIVE(g_TransitionSessionNonResetVars.sPostMissionCleanupData.ciLeaderBoardCam)
								RENDER_SCRIPT_CAMS(FALSE, FALSE, DEFAULT_INTERP_TO_FROM_GAME, TRUE, FALSE)
								TOGGLE_RENDERPHASES(TRUE)
								ANIMPOSTFX_STOP("MinigameTransitionIn")
								g_TransitionSessionNonResetVars.sPostMissionCleanupData.iState = 1
							ENDIF
//						ELSE
//							TOGGLE_RENDERPHASES(TRUE)
//							ANIMPOSTFX_STOP("MinigameTransitionIn")
//							g_TransitionSessionNonResetVars.sPostMissionCleanupData.iState = 1
//						ENDIF
					ENDIF
					
				ENDIF
				
			BREAK
			
			CASE 1
				
				g_TransitionSessionNonResetVars.sPostMissionCleanupData.bLeaderboardCamRenderingOrInterping = FALSE
				
				IF NOT IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_SCRIPT_CAMS_OFF_REQUESTED)
					IF DOES_CAM_EXIST(g_TransitionSessionNonResetVars.sPostMissionCleanupData.ciLeaderBoardCam)
						SET_CAM_ACTIVE(g_TransitionSessionNonResetVars.sPostMissionCleanupData.ciLeaderBoardCam, TRUE)
					ENDIF
					IF IS_CORONA_INITIALISING_A_QUICK_RESTART()
					OR IS_THIS_A_ROUNDS_MISSION_FOR_CORONA()
						TOGGLE_RENDERPHASES(FALSE)
					ENDIF
					IF NOT IS_PLAYER_SCTV(PLAYER_ID())
						IF IS_CAM_ACTIVE(g_TransitionSessionNonResetVars.sPostMissionCleanupData.ciLeaderBoardCam)
							RENDER_SCRIPT_CAMS(TRUE, FALSE, 0, TRUE, FALSE)
							ANIMPOSTFX_PLAY("MinigameTransitionIn", 0, TRUE)
							g_TransitionSessionNonResetVars.sPostMissionCleanupData.iState = 0
						ENDIF
					ELSE
						ANIMPOSTFX_PLAY("MinigameTransitionIn", 0, TRUE)
						g_TransitionSessionNonResetVars.sPostMissionCleanupData.iState = 0
					ENDIF
				ENDIF
				
			BREAK
			
		ENDSWITCH
		
	ENDIF
	
ENDPROC

PROC MAINTAIN_LEADERBOARD_CAM_READY_AND_BLURRED_FLAG()

	VECTOR vcampos, vcamrot
	
	IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.bRequestLeaderBoardCam
		IF DOES_CAM_EXIST(g_TransitionSessionNonResetVars.sPostMissionCleanupData.ciLeaderBoardCam)
			
			IF NOT HAS_NET_TIMER_STARTED(g_TransitionSessionNonResetVars.sPostMissionCleanupData.stFilterEffectRunningTimer)
				START_NET_TIMER(g_TransitionSessionNonResetVars.sPostMissionCleanupData.stFilterEffectRunningTimer)
			ENDIF
			
			vcampos = GET_FINAL_RENDERED_CAM_COORD()
			vcamrot = GET_FINAL_RENDERED_CAM_ROT()
			
			IF (vcamrot.x >= (LEADERBOARD_CAMERA_ANGLE-1.0)
			AND vcamrot.x <= (LEADERBOARD_CAMERA_ANGLE+1.0))
			OR ( ARE_VECTORS_EQUAL(vcampos, (g_TransitionSessionNonResetVars.sPostMissionCleanupData.vLbCamStartCoords+<<0.0, 0.0, 3.0>>)))
			OR NOT g_TransitionSessionNonResetVars.sPostMissionCleanupData.bWantLbCamToPan
				g_TransitionSessionNonResetVars.sPostMissionCleanupData.bLeaderBoardCamPanned = TRUE
			ENDIF
			
		ELSE
			g_TransitionSessionNonResetVars.sPostMissionCleanupData.bLeaderBoardCamPanned = FALSE
			g_TransitionSessionNonResetVars.sPostMissionCleanupData.bLeaderBoardCamReady = FALSE
		ENDIF
		
		IF NOT HAS_NET_TIMER_STARTED(g_TransitionSessionNonResetVars.sPostMissionCleanupData.stLbCamFailTimer)
			START_NET_TIMER(g_TransitionSessionNonResetVars.sPostMissionCleanupData.stLbCamFailTimer)
		ELSE
			IF HAS_NET_TIMER_EXPIRED(g_TransitionSessionNonResetVars.sPostMissionCleanupData.stLbCamFailTimer, 10000)
				#IF IS_DEBUG_BUILD
				IF NOT g_TransitionSessionNonResetVars.sPostMissionCleanupData.bLeaderBoardCamPanned
					NET_PRINT("[WJK] - stLbCamFailTimer timer has expired. Lb cam had been requested but failed to interp.")NET_NL()
				ENDIF
				#ENDIF
				g_TransitionSessionNonResetVars.sPostMissionCleanupData.bLeaderBoardCamPanned = TRUE
			ENDIF
		ENDIF
		
	ELSE
		g_TransitionSessionNonResetVars.sPostMissionCleanupData.bLeaderBoardCamPanned = FALSE
		g_TransitionSessionNonResetVars.sPostMissionCleanupData.bLeaderBoardCamReady = FALSE
	ENDIF
	
	IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.bLeaderBoardCamPanned
		IF HAS_NET_TIMER_STARTED(g_TransitionSessionNonResetVars.sPostMissionCleanupData.stFilterEffectRunningTimer)
			IF HAS_NET_TIMER_EXPIRED(g_TransitionSessionNonResetVars.sPostMissionCleanupData.stFilterEffectRunningTimer, 1500)
				g_TransitionSessionNonResetVars.sPostMissionCleanupData.bLeaderBoardCamReady = TRUE
			ENDIF
		ENDIF
//		IF ANIMPOSTFX_IS_RUNNING("MinigameTransitionIn")
//			g_TransitionSessionNonResetVars.sPostMissionCleanupData.bLeaderBoardCamReady = TRUE
//		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL INTERP_LEADERBOARD_CAM_AND_BLUR()
	
	NET_PRINT("[WJK] - INTERP_LEADERBOARD_CAM_AND_BLUR - being called.")NET_NL()
	
	IF CREATE_LEADERBOARD_CAM()
		NET_PRINT("[WJK] - INTERP_LEADERBOARD_CAM_AND_BLUR - CREATE_LEADERBOARD_CAM = TRUE.")NET_NL()
		IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.bLeaderBoardCamPanned
			NET_PRINT("[WJK] - INTERP_LEADERBOARD_CAM_AND_BLUR - g_TransitionSessionNonResetVars.sPostMissionCleanupData.bLeaderBoardCamPanned = TRUE.")NET_NL()
//			IF SET_SKYBLUR_BLURRY()
				NET_PRINT("[WJK] - INTERP_LEADERBOARD_CAM_AND_BLUR - SET_SKYBLUR_BLURRY = TRUE.")NET_NL()
				RETURN TRUE	
//			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL HAS_LEADERBOARD_CAM_FROZEN_RENDERPHASES() 
	IF IS_SKYSWOOP_IN_SKY()
		RETURN TRUE
	ENDIF
	RETURN (g_TransitionSessionNonResetVars.sPostMissionCleanupData.iLeaderBoardCamStage >= 1) 
ENDFUNC

FUNC VECTOR GET_SPECIAL_PLAYER_END_POSITION()
	
	IF CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_FINALE()
		INT iPart = GET_PARTICIPANT_NUMBER_IN_TEAM_PRIVATE()
		SWITCH iPart
			CASE 0
				RETURN <<771.7906, 198.7745, 82.6525>>
			BREAK
			CASE 1
				RETURN <<770.5034, 199.4262, 82.6525>>
			BREAK
			CASE 2
				RETURN <<768.3102, 200.4025, 82.6525>>
			BREAK
			CASE 3
				RETURN <<766.5592, 201.4036, 82.6525>>
			BREAK
			DEFAULT
				RETURN <<769.2659, 202.7318, 82.6525>>
			BREAK
		ENDSWITCH
	ENDIF
	
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

PROC SAVE_PLAYER_END_JOB_POSITION_FOR_POST_MISSION_CLEANUP()
	
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		
		IF (g_TransitionSessionNonResetVars.bInsideDisplacedInterior)
			g_TransitionSessionNonResetVars.sPostMissionCleanupData.vEndOfJobPosition = g_TransitionSessionNonResetVars.vDisplacedPerceivedCoords
			PRINTLN("[Post Mission Cleanup] - SAVE_PLAYER_END_JOB_POSITION_FOR_POST_MISSION_CLEANUP - grabbed DISPLACED player coords: ", g_TransitionSessionNonResetVars.sPostMissionCleanupData.vEndOfJobPosition)
		ELSE	
			g_TransitionSessionNonResetVars.sPostMissionCleanupData.vEndOfJobPosition = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
			PRINTLN("[Post Mission Cleanup] - SAVE_PLAYER_END_JOB_POSITION_FOR_POST_MISSION_CLEANUP - grabbed player coords: ", g_TransitionSessionNonResetVars.sPostMissionCleanupData.vEndOfJobPosition)		
		ENDIF
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			g_TransitionSessionNonResetVars.sPostMissionCleanupData.fEndOfJobHeading = GET_ENTITY_HEADING(PLAYER_PED_ID())
		ENDIF		
		PRINTLN("[Post Mission Cleanup] - SAVE_PLAYER_END_JOB_POSITION_FOR_POST_MISSION_CLEANUP - grabbed player heading: ", g_TransitionSessionNonResetVars.sPostMissionCleanupData.fEndOfJobHeading)
	ENDIF
	
	IF NOT IS_VECTOR_ZERO(GET_SPECIAL_PLAYER_END_POSITION())
		g_TransitionSessionNonResetVars.sPostMissionCleanupData.vEndOfJobPosition = GET_SPECIAL_PLAYER_END_POSITION()
		PRINTLN("[Post Mission Cleanup] - SAVE_PLAYER_END_JOB_POSITION_FOR_POST_MISSION_CLEANUP - grabbed special coords for player: ", g_TransitionSessionNonResetVars.sPostMissionCleanupData.vEndOfJobPosition)
	ENDIF
	
	IF IS_THIS_THE_LAST_PLAYLIST_MISSION()
		g_TransitionSessionNonResetVars.sPostMissionCleanupData.bBlockCleanupOfEndMissionPosData = TRUE
		PRINTLN("[Post Mission Cleanup] - SAVE_PLAYER_END_JOB_POSITION_FOR_POST_MISSION_CLEANUP - IS_THIS_THE_LAST_PLAYLIST_MISSION() = TRUE, SettingbBlockCleanupOfEndMissionPosData = TRUE.")			
	ENDIF
	
ENDPROC

PROC PROCESS_LEADERBOARD_CAM()
	
	IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.bWantSwoopCam
		
		g_TransitionSessionNonResetVars.sPostMissionCleanupData.bLeaderboardCamRenderingOrInterping = TRUE
		
		IF SET_SKYSWOOP_UP(TRUE, FALSE)
			// Done.
		ENDIF
		
	ELSE
		
		// If we end up in sp and the leaderboard camera is still requested, clean it up, we don;t need it and it might cause camera issues in SP. 
		IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.bRequestLeaderBoardCam
			IF (GET_CURRENT_GAMEMODE() != GAMEMODE_FM)
				#IF IS_DEBUG_BUILD PRINTLN("[WJK] - PROCESS_LEADERBOARD_CAM - not in MP anymore, gone back to SP and bWantSwoopCam = FALSE, cleaning up leaderboard camera.") #ENDIF
				CLEANUP_LEADERBOARD_CAM(TRUE, TRUE, TRUE, FALSE, FALSE, FALSE)
			ENDIF
		ENDIF
		
		MAINTAIN_LEADERBOARD_CAM_RENDERING_REQUESTS()
		MAINTAIN_LEADERBOARD_CAM_READY_AND_BLURRED_FLAG()
		
		#IF IS_DEBUG_BUILD
			IF g_bLeaderboardCamExtraDebugPrints
			NET_PRINT("[WJK] - PROCESS_LEADERBOARD_CAM - bLeaderboardCamExtraDebugPrints - iLeaderBoardCamStage = ")NET_PRINT_INT(g_TransitionSessionNonResetVars.sPostMissionCleanupData.iLeaderBoardCamStage)NET_NL()
			ENDIF
		#ENDIF
		
		SWITCH g_TransitionSessionNonResetVars.sPostMissionCleanupData.iLeaderBoardCamStage
			
			// Create the leaderboard camera.
			CASE 0
				
				IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.bRequestLeaderBoardCam
					
					#IF IS_DEBUG_BUILD
						IF g_bLeaderboardCamExtraDebugPrints
						NET_PRINT("[WJK] - PROCESS_LEADERBOARD_CAM - bLeaderboardCamExtraDebugPrints - bRequestLeaderBoardCam = TRUE.")NET_NL()
						ENDIF
					#ENDIF
		
					IF IS_SCREEN_FADED_IN()
						
						#IF IS_DEBUG_BUILD
							IF g_bLeaderboardCamExtraDebugPrints
							NET_PRINT("[WJK] - PROCESS_LEADERBOARD_CAM - bLeaderboardCamExtraDebugPrints - screen is faded in.")NET_NL()
							ENDIF
						#ENDIF
						
						IF NOT IS_A_SPECTATOR_CAM_RUNNING()
							HIDE_HUD_AND_RADAR_THIS_FRAME()
							DISABLE_FRONTEND_THIS_FRAME()
						ENDIF
						
						IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
							IF NOT IS_PED_INJURED(PLAYER_PED_ID())
								SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_CannotBeTargetedByAI, TRUE)
							ENDIF
						ENDIF
						
						NET_PRINT("[WJK] - PROCESS_LEADERBOARD_CAM - bRequestLeaderBoardCam = TRUE.")NET_NL()
						
						// Make sure the focus is clear in prep for cleanup.
						// Stop load scenes.
						IF NOT g_TransitionSessionNonResetVars.sPostMissionCleanupData.bLbCamClearedLoadsScenesAndFocus
							
							#IF IS_DEBUG_BUILD
								IF g_bLeaderboardCamExtraDebugPrints
								NET_PRINT("[WJK] - PROCESS_LEADERBOARD_CAM - bLeaderboardCamExtraDebugPrints - bLbCamClearedLoadsScenesAndFocus = FALSE")NET_NL()
								ENDIF
							#ENDIF
						
							CLEAR_FOCUS()
							NEW_LOAD_SCENE_STOP()
							NET_PRINT("[WJK] - PROCESS_LEADERBOARD_CAM - bLbCamClearedLoadsScenesAndFocus = TRUE.")NET_NL()
							
							g_TransitionSessionNonResetVars.sPostMissionCleanupData.bLbCamClearedLoadsScenesAndFocus = TRUE
							
						ENDIF
						
						IF INTERP_LEADERBOARD_CAM_AND_BLUR() // SET_SKYSWOOP_UP()
							
							#IF IS_DEBUG_BUILD
								IF g_bLeaderboardCamExtraDebugPrints
								NET_PRINT("[WJK] - PROCESS_LEADERBOARD_CAM - bLeaderboardCamExtraDebugPrints - INTERP_LEADERBOARD_CAM_AND_BLUR() = TRUE")NET_NL()
								ENDIF
							#ENDIF
							
							TOGGLE_RENDERPHASES(FALSE)
							g_TransitionSessionNonResetVars.sPostMissionCleanupData.bLbCamClearedLoadsScenesAndFocus = FALSE
							g_TransitionSessionNonResetVars.sPostMissionCleanupData.iLeaderBoardCamStage = 1
							NET_PRINT("[WJK] - PROCESS_LEADERBOARD_CAM - INTERP_LEADERBOARD_CAM_AND_BLUR = TRUE.")NET_NL()
							NET_PRINT("[WJK] - PROCESS_LEADERBOARD_CAM - iLeaderBoardCamStage = 1.")NET_NL()
							
						ENDIF
						
					ENDIF
					
				ENDIF
				
			BREAK
			
			CASE 1
				
				IF NOT IS_A_SPECTATOR_CAM_RUNNING()
					HIDE_HUD_AND_RADAR_THIS_FRAME()
					DISABLE_FRONTEND_THIS_FRAME()
				ENDIF
				
				IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_CannotBeTargetedByAI, TRUE)
					ENDIF
				ENDIF
				
		        // Put player on ground and make face the same direction as the camera once board is facing the skyline.
		        IF NOT g_TransitionSessionNonResetVars.sPostMissionCleanupData.bSetGroundAndHeading
					
					NET_PRINT("[WJK] - PROCESS_LEADERBOARD_CAM - bSetGroundAndHeading = FALSE.")NET_NL()
					
		            IF IS_LEADERBOARD_CAM_READY()
						#IF IS_DEBUG_BUILD
							IF g_bLeaderboardCamExtraDebugPrints
							NET_PRINT("[WJK] - PROCESS_LEADERBOARD_CAM - bLeaderboardCamExtraDebugPrints - IS_LEADERBOARD_CAM_READY() = TRUE")NET_NL()
							ENDIF
						#ENDIF
						g_TransitionSessionNonResetVars.sPostMissionCleanupData.bSetGroundAndHeading = TRUE	
		            ENDIF
		        
				ELSE
					
					g_TransitionSessionNonResetVars.sPostMissionCleanupData.iLeaderBoardCamStage = 2
					NET_PRINT("[WJK] - PROCESS_LEADERBOARD_CAM - iLeaderBoardCamStage = 2.")NET_NL()
					
				ENDIF
			BREAK
			
			CASE 2
			
				IF NOT IS_A_SPECTATOR_CAM_RUNNING()
					HIDE_HUD_AND_RADAR_THIS_FRAME()
					DISABLE_FRONTEND_THIS_FRAME()
				ENDIF
				
				IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_CannotBeTargetedByAI, TRUE)
					ENDIF
				ENDIF
				
				// Wait for cleanup to be called here.
				
			BREAK
			
		ENDSWITCH
	
	ENDIF
	
ENDPROC


FUNC TEXT_LABEL_15 GET_MISSION_DIFFICULTY(LBD_SUB_MODE eSubMode)

	TEXT_LABEL_15 tl15 = ""
	
	IF Is_Player_Currently_On_MP_LTS_Mission(PLAYER_ID())
	OR Is_Player_Currently_On_MP_Versus_Mission(PLAYER_ID())
		RETURN tl15
	ENDIF

	SWITCH eSubMode
		CASE SUB_MISSIONS_NORMAL
		CASE SUB_MISSIONS_TIMED
		CASE SUB_MISSIONS_NO_HUD
		CASE SUB_MISSIONS_VERSUS
		CASE SUB_MISSIONS_LTS
		CASE SUB_MISSIONS_CTF
		CASE SUB_MISSIONS_HEIST
			tl15 = "LBD_DIF_"
			tl15 += g_FMMC_STRUCT.iDifficulity
		BREAK
	ENDSWITCH
	
	IF bSVM_MISSION()
	AND NETWORK_IS_ACTIVITY_SESSION()
		tl15 = "LBD_DIF_"
		tl15 += g_FMMC_STRUCT.iDifficulity
	ENDIF
	
	RETURN tl15
ENDFUNC

FUNC STRING GET_SESSION_NAME()

	#IF FEATURE_GEN9_EXCLUSIVE
	IF IS_PLAYER_ON_MP_INTRO()
		RETURN "HUD_LBD_INT" //GTA Online (Introduction)
	ENDIF
	#ENDIF

	IF NETWORK_SESSION_IS_CLOSED_FRIENDS()
		// "GTA Online (Friend, ~1~)"
		RETURN "HUD_LBD_FMF"
	ENDIF
	
	IF NETWORK_SESSION_IS_CLOSED_CREW()// (find out limit using NETWORK_SESSION_GET_UNIQUE_CREW_LIMIT)
		// "GTA Online (Crew, ~1~)"
		RETURN "HUD_LBD_FMC"
	ENDIF
	
	IF NETWORK_SESSION_IS_SOLO()
		// "GTA Online (Solo, ~1~)" 
		RETURN "HUD_LBD_FMS"
	ENDIF
	
	IF NETWORK_SESSION_IS_PRIVATE()// (invite only)
		// "GTA Online (Invite, ~1~)"
		RETURN "HUD_LBD_FMI"
	ENDIF
	
	// "GTA Online (Public, ~1~)"
	RETURN "HUD_LBD_FMP" 
ENDFUNC


FUNC STRING GET_BOSS_DPAD_TITLE(DPAD_VARS& dpadVars)
	
	SWITCH dpadVars.eSmugglerType
		CASE eSMUGGLER_TYPE_STEAL
			RETURN "GR_DPD_E"
		BREAK
		CASE eSMUGGLER_TYPE_SELL
			RETURN "GR_DPD_F"
		BREAK
		CASE eSMUGGLER_TYPE_SETUP
			RETURN "GR_DPD_S"
		BREAK
	ENDSWITCH

	
	SWITCH dpadVars.eGunrunType
	
		CASE eGUNRUN_TYPE_SETUP
			RETURN "GR_DPD_A"
		BREAK
		CASE eGUNRUN_TYPE_STEAL
			RETURN "GR_DPD_B"
		BREAK
		CASE eGUNRUN_TYPE_SELL
			RETURN "GR_DPD_C"
		BREAK
		CASE eGUNRUN_TYPE_DEFEND
			RETURN "GR_DPD_D"
		BREAK
	
	ENDSWITCH

	 // #IF FEATURE_GUNRUNNING
	
	SWITCH GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID())
	
		CASE FMMC_TYPE_FM_GANGOPS
			RETURN "H2_DPAD_SET"
		BREAK
	
		
		CASE FMMC_TYPE_BIKER_UNLOAD_WEAPONS
			RETURN "GB_BIGUNLOAD_T"
		BREAK
		
		CASE FMMC_TYPE_BIKER_DEAL_GONE_WRONG
			RETURN "DEAL_DEALN"
		BREAK
		
		CASE FMMC_TYPE_BIKER_RIPPIN_IT_UP
			RETURN "PI_BIK_13_0"
		BREAK
		
		CASE FMMC_TYPE_BIKER_RACE_P2P
			RETURN "PI_BIK_4_1"
		BREAK
		CASE FMMC_TYPE_BIKER_DRIVEBY_ASSASSIN
			RETURN "PI_BIK_13_1"
		BREAK
		CASE FMMC_TYPE_BIKER_CRIMINAL_MISCHIEF
			RETURN "PI_BIK_13_3"
		BREAK
		
		CASE FMMC_TYPE_BIKER_CONTRACT_KILLING
			RETURN "CELL_BIKER_CK"
		BREAK
		
		CASE FMMC_TYPE_BIKER_DESTROY_VANS
			RETURN "DV_SH_TITLE"
		BREAK
		
		CASE FMMC_TYPE_BIKER_BURN_ASSETS
			RETURN "BA_SH_TITLE"
		BREAK
		
		CASE FMMC_TYPE_BIKER_SHUTTLE
			RETURN "SHU_SH_TITLE"
		BREAK
		
		CASE FMMC_TYPE_BIKER_WHEELIE_RIDER
			RETURN "PI_BIK_13_4"
		BREAK

		CASE FMMC_TYPE_BIKER_RESCUE_CONTACT
			RETURN "CELL_BIKER_RESC"
		BREAK
		
		CASE FMMC_TYPE_BIKER_SEARCH_AND_DESTROY
			RETURN "CELL_BIKER_SEAR"
		BREAK
		
		CASE FMMC_TYPE_BIKER_STAND_YOUR_GROUND
			RETURN "CELL_BIKER_STAN"
		BREAK
		
		 // #IF FEATURE_BIKER
		
		CASE FMMC_TYPE_GB_SIGHTSEER
			RETURN "PIM_MAGM210"
		BREAK
		
		CASE FMMC_TYPE_GB_CHAL_CARJACKING
			RETURN "PIM_MAGM608"
		BREAK
		
		CASE FMMC_TYPE_GB_CHAL_FINDERS_KEEPERS
			RETURN "PIM_MAGM604"
		BREAK
		
		CASE FMMC_TYPE_GB_CHAL_ROB_SHOP
			RETURN "PIM_MAGM602"
		BREAK
		
		CASE FMMC_TYPE_GB_CHAL_COLLECT_MONEY
			RETURN "PIM_MAGM603"
		BREAK
		
		CASE FMMC_TYPE_GB_BOSS_DEATHMATCH		
			IF IS_PLAYER_ON_BIKER_DM()
				RETURN "LBD_BKVBK"
			ENDIF
			
			RETURN "PIM_MAGM201"
		BREAK
		
		CASE FMMC_TYPE_GB_TERMINATE		
			IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_BIKER_GANG()
				RETURN "GB_DPAD_BMFD"
			ENDIF
			RETURN "PIM_MAGM202"
		BREAK
		
		CASE FMMC_TYPE_GB_YACHT_ROBBERY	
			RETURN "PIM_MAGM204"
		BREAK
		
		CASE FMMC_TYPE_GB_CHAL_FIVESTAR
			IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_BIKER_GANG()
				RETURN "PI_BIK_3_2"
			ENDIF
			RETURN "PIM_MAGM601"
		BREAK
		
		CASE FMMC_TYPE_GB_BELLY_BEAST	
			RETURN "PIM_MAGM207"
		BREAK
		
		CASE FMMC_TYPE_GB_ASSAULT	
			RETURN "PIM_MAGM206"
		BREAK
		
		CASE FMMC_TYPE_GB_CHAL_P2P		
			RETURN "PIM_MAGM607"
		BREAK
	
		CASE FMMC_TYPE_GB_HUNT_THE_BOSS	
			RETURN "PIM_MAGM212"
		BREAK
		
		
		CASE FMMC_TYPE_GB_HEADHUNTER						RETURN "GB_DPAD_HEAD"								
		CASE FMMC_TYPE_GB_CONTRABAND_BUY					RETURN "GB_DPAD_BUY"
		CASE FMMC_TYPE_GB_CONTRABAND_SELL					RETURN "GB_DPAD_SELL"
		CASE FMMC_TYPE_GB_CONTRABAND_DEFEND					RETURN "GB_DPAD_DEF"
		CASE FMMC_TYPE_GB_AIRFREIGHT						RETURN "GB_DPAD_AIR"
		CASE FMMC_TYPE_GB_CASHING_OUT						RETURN "GB_DPAD_CASH"
		CASE FMMC_TYPE_GB_SALVAGE							RETURN "GB_DPAD_SAL"
		CASE FMMC_TYPE_GB_FRAGILE_GOODS						RETURN "GB_DPAD_FRA"
		
		CASE FMMC_TYPE_VEHICLE_EXPORT_BUY					RETURN "VEX_TITLEa"
		CASE FMMC_TYPE_VEHICLE_EXPORT_SELL					RETURN "VEX_TITLEb"
		CASE FMMC_TYPE_IMPEXP_FORTIFIED						RETURN "FRT_MODE"
		CASE FMMC_TYPE_IMPEXP_TRANSPORTER					RETURN "FRT_TRNS"
		
		CASE FMMC_TYPE_IMPEXP_PLOUGHED						RETURN "MODE_PLW"
		CASE FMMC_TYPE_IMPEXP_FULLY_LOADED					RETURN "MODE_FUL"
		CASE FMMC_TYPE_IMPEXP_AMPHIBIOUS_ASSAULT			RETURN "MODE_AA"
															
		CASE FMMC_TYPE_IMPEXP_VELOCITY						RETURN "MODE_VEL"
		CASE FMMC_TYPE_IMPEXP_RAMPED_UP						RETURN "MODE_RMP"
		CASE FMMC_TYPE_IMPEXP_STOCKPILING					RETURN "MODE_STK"
														
		 // #IF FEATURE_IMPORT_EXPORT
		CASE FMMC_TYPE_GUNRUNNING_BUY						RETURN "GR_TITLEL"
		CASE FMMC_TYPE_GUNRUNNING_SELL						RETURN "GRS_TITLEL"
		CASE FMMC_TYPE_GUNRUNNING_DEFEND					RETURN "GRD_TITLEL"
		CASE FMMC_TYPE_BIKER_STEAL_BIKES					RETURN "GB_STEAL_T"
		CASE FMMC_TYPE_BIKER_SAFECRACKER					RETURN "SC_MENU_TITLE"
		
		CASE FMMC_TYPE_BIKER_CONTRABAND_SELL				RETURN "GB_DPAD_BSEL"
		CASE FMMC_TYPE_BIKER_CONTRABAND_DEFEND				RETURN "GB_DPAD_BDEF"

		CASE FMMC_TYPE_BIKER_LAST_RESPECTS					RETURN "GB_DPAD_GFH"
		CASE FMMC_TYPE_BIKER_FREE_PRISONER					RETURN "GB_DPAD_JB"
		
		CASE FMMC_TYPE_BIKER_JOUST 							RETURN "CELL_JOUST"
		
		CASE FMMC_TYPE_BIKER_CAGED_IN						RETURN "CAG_BLIP"
		CASE FMMC_TYPE_BIKER_BUY							
			IF IS_ILLICIT_GOODS_VARIATION_A_SETUP_MISSION(INT_TO_ENUM(ILLICIT_GOODS_VARIATION, GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.iContrabandVariation))
				RETURN "GB_DPAD_BSET"
			ENDIF
			RETURN "GB_DPAD_BBUY"
		BREAK
		
	ENDSWITCH
	
	PRINTLN("[CS_CHALLENGE_DPAD], GET_BOSS_DPAD_TITLE, ", GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()))

	RETURN ""
ENDFUNC

FUNC STRING GET_BOSS_SORT_VAR()
	
	SWITCH GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID())
			
		CASE FMMC_TYPE_GB_CHAL_CARJACKING
			RETURN "BD_SORT_1"
		BREAK
		
		CASE FMMC_TYPE_GB_CHAL_FINDERS_KEEPERS
			RETURN "BD_SORT_4"
		BREAK
		
		CASE FMMC_TYPE_GB_CHAL_ROB_SHOP
			RETURN "BD_SORT_3"
		BREAK
		
		CASE FMMC_TYPE_GB_CHAL_COLLECT_MONEY
			RETURN "BD_SORT_3"
		BREAK
	ENDSWITCH

	RETURN ""
ENDFUNC

 // #IF FEATURE_GANG_BOSS


// Name of the Challenge dpad leaderboard, ie. "Hot Property"
FUNC STRING GET_CHALLENGE_TITLE(DPAD_VARS& dpadVars)

	INT iChallengeType = FM_EVENT_GET_PLAYER_CURRENT_FM_EVENT(PLAYER_ID())
	
	// Override the title for the cached event type
	IF SHOULD_FORCE_DRAW_CHALLENGE_DPAD_LBD()
		IF dpadVars.iCachedEventType != -1
			iChallengeType = dpadVars.iCachedEventType
		ENDIF
	ENDIF
	
	SWITCH iChallengeType
	
		CASE FMMC_TYPE_MOVING_TARGET						RETURN "PIM_TA9"
		CASE FMMC_TYPE_CHECKPOINTS			
		
			IF GET_ACTIVE_CHECKPOINT_COLLECTION_TYPE() = 0
			
				// "Land & Sea Checkpoint Collection"
				RETURN "CPC_TILEL"
			ELIF GET_ACTIVE_CHECKPOINT_COLLECTION_TYPE() = 1

				// "Air Checkpoint Collection"
				RETURN "CPC_TILELA"
			ENDIF
			
			RETURN "PIM_TA0"
		BREAK
//			CASE FMMC_TYPE_AIR_DROP						RETURN "PIM_TA0"		
		CASE FMMC_TYPE_CHALLENGES				
		
			SWITCH GET_ACTIVE_FREEMODE_CHALLENGE()

				CASE AM_CHALLENGE_LONGEST_JUMP			RETURN "AMCH_0SLC"

				CASE AM_CHALLENGE_LONGEST_SKYDIVE		RETURN "AMCH_1SLC"

				CASE AM_CHALLENGE_HIGHEST_SPEED			RETURN "AMCH_2SLC"
					
				CASE AM_CHALLENGE_LONGEST_STOPPIE		RETURN "AMCH_3SLC"

				CASE AM_CHALLENGE_LONGEST_WHEELIE		RETURN "AMCH_4SLC"

				CASE AM_CHALLENGE_LONGEST_NO_CRASH		RETURN "AMCH_5SLC"

				CASE AM_CHALLENGE_LOWEST_PARACHUTE		RETURN "AMCH_6SLC"

				CASE AM_CHALLENGE_MOST_VEHICLES_STOLEN	RETURN "AMCH_7SLC"
				
				CASE AM_CHALLENGE_MOST_NEAR_MISSES		RETURN "AMCH_8SLC"

				CASE AM_CHALLENGE_REVERSE_DRIVING		RETURN "AMCH_12SLC"

				CASE AM_CHALLENGE_LONGEST_FALL			RETURN "AMCH_13SLC"

				CASE AM_CHALLENGE_LOW_FLYING			RETURN "AMCH_15SLC"

				CASE AM_CHALLENGE_LOW_FLYING_INVERTED	RETURN "AMCH_16SLC"		

				CASE AM_CHALLENGE_LONGEST_BAIL			RETURN "AMCH_23SLC"		

				CASE AM_CHALLENGE_MOST_BRIDGES			RETURN "AMCH_9SLC"	

				CASE AM_CHALLENGE_PVP_HEADSHOTS			RETURN "AMCH_19SLC"

				CASE AM_CHALLENGE_PVP_DRIVEBY			RETURN "AMCH_20SLC"

				CASE AM_CHALLENGE_PVP_MELEE				RETURN "AMCH_21SLC"

				CASE AM_CHALLENGE_PVP_LONGEST_SNIPE		RETURN "AMCH_22SLC"

			ENDSWITCH
		BREAK
		
		CASE FMMC_TYPE_PENNED_IN						RETURN "PIM_TA10"
		CASE FMMC_TYPE_HOLD_THE_WHEEL					RETURN "PIM_TA4"
		CASE FMMC_TYPE_HOT_PROPERTY						RETURN "PIM_TA5"
		CASE FMMC_TYPE_DEAD_DROP						RETURN "PIM_TA3"
		CASE FMMC_TYPE_KING_CASTLE						RETURN "PIM_TA8"
//			CASE FMMC_TYPE_INFECTION						RETURN "PIM_TA17"
		CASE FMMC_TYPE_CRIMINAL_DAMAGE					RETURN "PIM_TA2"
		CASE FMMC_TYPE_KILL_LIST		
			IF GET_CURRENT_URBAN_WARFARE_TYPE() = 1
				// "Competitive Urban Warfare"
				RETURN "FM_AE_TITL_12"
			ELSE
				// "Urban Warfare"
				RETURN "PIM_TA7"
			ENDIF
		BREAK
		CASE FMMC_TYPE_HUNT_THE_BEAST					RETURN "PIM_TA6"
		
	ENDSWITCH
	
	PRINTLN("[CS_CHALLENGE_DPAD], GET_CHALLENGE_TITLE, ", FM_EVENT_GET_PLAYER_CURRENT_FM_EVENT(PLAYER_ID()))
		
	RETURN ""
ENDFUNC



// For dpad leaderboard, the sorting variable, ie. (Score, Time) or "" for no brackets
FUNC STRING GET_CHALLENGE_SORT_VAR(DPAD_VARS& dpadVars)

	INT iChallengeType = FM_EVENT_GET_PLAYER_CURRENT_FM_EVENT(PLAYER_ID())
	
	// Override the title for the cached event type
	IF SHOULD_FORCE_DRAW_CHALLENGE_DPAD_LBD()
		IF dpadVars.iCachedEventType != -1
			iChallengeType = dpadVars.iCachedEventType
		ENDIF
	ENDIF

	SWITCH iChallengeType
	
		CASE FMMC_TYPE_MOVING_TARGET						RETURN ""				// No brackets
		CASE FMMC_TYPE_CHECKPOINTS			RETURN "FM_AE_SORT_5"
		CASE FMMC_TYPE_CHALLENGES				
		
			SWITCH GET_ACTIVE_FREEMODE_CHALLENGE()

				CASE AM_CHALLENGE_LONGEST_JUMP			RETURN "FM_AE_SORT_2"

				CASE AM_CHALLENGE_LONGEST_SKYDIVE		RETURN "FM_AE_SORT_2"

				CASE AM_CHALLENGE_HIGHEST_SPEED			RETURN "FM_AE_SORT_3"
					
				CASE AM_CHALLENGE_LONGEST_STOPPIE		RETURN "FM_AE_SORT_2"

				CASE AM_CHALLENGE_LONGEST_WHEELIE		RETURN "FM_AE_SORT_2"

				CASE AM_CHALLENGE_LONGEST_NO_CRASH		RETURN "FM_AE_SORT_2"

				CASE AM_CHALLENGE_LOWEST_PARACHUTE		RETURN "FM_AE_SORT_2"

				CASE AM_CHALLENGE_MOST_VEHICLES_STOLEN	RETURN "FM_AE_SORT_13"
				
				CASE AM_CHALLENGE_MOST_NEAR_MISSES		RETURN "FM_AE_SORT_4"

				CASE AM_CHALLENGE_REVERSE_DRIVING		RETURN "FM_AE_SORT_2"

				CASE AM_CHALLENGE_LONGEST_FALL			RETURN "FM_AE_SORT_2"

				CASE AM_CHALLENGE_LOW_FLYING			RETURN "FM_AE_SORT_2"

				CASE AM_CHALLENGE_LOW_FLYING_INVERTED	RETURN "FM_AE_SORT_2"			

				CASE AM_CHALLENGE_LONGEST_BAIL			RETURN "FM_AE_SORT_2"		

				CASE AM_CHALLENGE_MOST_BRIDGES			RETURN "FM_AE_SORT_5"	

				CASE AM_CHALLENGE_PVP_HEADSHOTS			RETURN "FM_AE_SORT_9"

				CASE AM_CHALLENGE_PVP_DRIVEBY			RETURN "FM_AE_SORT_9"

				CASE AM_CHALLENGE_PVP_MELEE				RETURN "FM_AE_SORT_9"

				CASE AM_CHALLENGE_PVP_LONGEST_SNIPE		RETURN "FM_AE_SORT_9"

			ENDSWITCH
		BREAK
		
		CASE FMMC_TYPE_PENNED_IN						RETURN ""					// No brackets
	//	CASE FMMC_TYPE_MTA								RETURN "FM_AE_SORT_9"
		CASE FMMC_TYPE_HOLD_THE_WHEEL						RETURN ""					// No brackets
		CASE FMMC_TYPE_HOT_PROPERTY						RETURN "FM_AE_SORT_10"
		CASE FMMC_TYPE_DEAD_DROP						RETURN "" 					// No brackets
		CASE FMMC_TYPE_KING_CASTLE						RETURN "FM_AE_SORT_5"
//			CASE FMMC_TYPE_INFECTION						RETURN ""					// No brackets
		CASE FMMC_TYPE_CRIMINAL_DAMAGE							RETURN "FM_AE_SORT_1"
		CASE FMMC_TYPE_KILL_LIST					RETURN "FM_AE_SORT_9"
		
	ENDSWITCH
	
	PRINTLN("[CS_CHALLENGE_DPAD], GET_CHALLENGE_SORT_VAR, ", FM_EVENT_GET_PLAYER_CURRENT_FM_EVENT(PLAYER_ID()))
		
	RETURN ""
ENDFUNC

 // #IF FEATURE_NEW_AMBIENT_EVENTS

FUNC STRING GET_LEADERBOARD_TITLE(LBD_SUB_MODE eSubMode, STRING sLbdTitle, BOOL bIsImpromptu = FALSE)

	STRING sFreemodeTitle

	IF eSubMode = SUB_FREEMODE

	AND ( NOT bSVM_MISSION() OR IS_STRING_NULL_OR_EMPTY(sLbdTitle) )
	
		sFreemodeTitle = GET_SESSION_NAME()
		
		RETURN sFreemodeTitle
		
	ELIF bIsImpromptu
	
		RETURN "HUD_LBD_IMP"	
		
	ELIF eSubMode = SUB_FINAL_ROUNDS
	
		IF g_bDrawJustText = FALSE
			g_bDrawJustText = TRUE
		ENDIF
	
		RETURN "HUD_LBD_OVR"
		
	#IF FEATURE_COPS_N_CROOKS
	
	ELIF eSubMode = SUB_ARCADE_CNC
		RETURN "HUD_LBD_CNC"
		
	#ENDIF
	
	ELSE
		IF NOT IS_STRING_NULL_OR_EMPTY(sLbdTitle)
		
			IF g_bDrawJustText = TRUE
				g_bDrawJustText = FALSE
			ENDIF
		
			RETURN sLbdTitle
		ELSE

			IF g_bDrawJustText = FALSE
				g_bDrawJustText = TRUE
			ENDIF
			
			SWITCH eSubMode
				CASE SUB_DM_FFA
				CASE SUB_ARENA_CARNAGE	
				CASE SUB_DM_UPDATE
				
					RETURN "HUD_LBD_DM"
				BREAK
							
				CASE SUB_DM_TEAM
				
					RETURN "HUD_LBD_TDM"
				BREAK
			
				CASE SUB_RACE_GTA

					RETURN "HUD_LBD_GRCE"
				BREAK
					
				CASE SUB_RACE_NORMAL
				CASE SUB_RACE_AGG_BEST
				CASE SUB_RACE_AGG_TIME
				CASE SUB_RACE_RALLY
				CASE SUB_RACE_STUNT
				CASE SUB_RACE_TARGET

					RETURN "HUD_LBD_RCE"
				BREAK
				
				CASE SUB_RACE_BASEJUMP
					RETURN "HUD_LBD_BRCE"
				BREAK

				CASE SUB_MISSIONS_HEIST
				CASE SUB_MISSIONS_NO_HUD
				CASE SUB_MISSIONS_VERSUS
				CASE SUB_MISSIONS_LTS
				CASE SUB_MISSIONS_TIMED
				CASE SUB_MISSIONS_NORMAL
				CASE SUB_MISSIONS_CTF
				CASE SUB_PLAYLIST
					RETURN "HUD_TITLEMC"
				BREAK
				
				CASE SUB_HORDE
					RETURN "HUD_LBD_HRD"
				BREAK
				
				CASE SUB_SHOOT_RANGE
				
					RETURN "HUD_LBD_SHOO"
				BREAK
			ENDSWITCH
		ENDIF
	ENDIF

	RETURN sLbdTitle
ENDFUNC

PROC RENDER_SPECTATOR_BUTTON_BAR(LEADERBOARD_PLACEMENT_TOOLS& Placement)

	SPRITE_PLACEMENT ScaleformSprite = GET_SCALEFORM_INSTRUCTIONAL_BUTTON_POSITION()
	
	// B Exit
	ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL), "HUD_INPUT34", Placement.LeaderboardScaleformStruct)
		
	// Draw
	RUN_SCALEFORM_INSTRUCTIONAL_BUTTONS	
	(Placement.ButtonMovie[GET_SCALEFORM_INDEX_FOR_HUD(HUD_SCALEFORM_INSTRUCTIONAL)], ScaleformSprite,
	Placement.LeaderboardScaleformStruct, SHOULD_REFRESH_SCALEFORM_INSTRUCTIONAL_BUTTONS(Placement.LeaderboardScaleformStruct))
ENDPROC

PROC REFRESH_SCALEFORM_BUTTON_SLOTS(LEADERBOARD_PLACEMENT_TOOLS& Placement)
	MPHUD_CLEAR_SCALEFORM_ELEMENT(Placement.ButtonMovie[GET_SCALEFORM_INDEX_FOR_HUD(HUD_SCALEFORM_INSTRUCTIONAL)], HUD_SCALEFORM_INSTRUCTIONAL)
	REFRESH_SCALEFORM_INSTRUCTIONAL_BUTTONS(Placement.LeaderboardScaleformStruct)
ENDPROC

CONST_INT LEADERBOARD_VOICE_CHANNEL 0

PROC START_LBD_AUDIO_SCENE()
	IF NOT IS_AUDIO_SCENE_ACTIVE("LEADERBOARD_SCENE")
		PRINTLN(" [CS_LBD_SCENE] START_LBD_AUDIO_SCENE")
		START_AUDIO_SCENE("LEADERBOARD_SCENE")
	ENDIF
ENDPROC

PROC STOP_LBD_AUDIO_SCENE()
	IF IS_AUDIO_SCENE_ACTIVE("LEADERBOARD_SCENE")
		PRINTLN(" [CS_LBD_SCENE] STOP_LBD_AUDIO_SCENE")
		STOP_AUDIO_SCENE("LEADERBOARD_SCENE") 
	ENDIF
ENDPROC

PROC LBD_SCENE_CLEANUP()
	IF IS_PLAYER_ON_A_PLAYLIST(PLAYER_ID())
		SET_FRONTEND_RADIO_ACTIVE(FALSE) 
		PRINTLN(" [CS_LBD_SCENE] LBD_SCENE_CLEANUP, (IS_PLAYER_ON_A_PLAYLIST) SET_FRONTEND_RADIO_ACTIVE(FALSE) ")
	ELSE
		STOP_LBD_AUDIO_SCENE()
		PRINTLN(" [CS_LBD_SCENE] LBD_SCENE_CLEANUP, STOP_LBD_AUDIO_SCENE")
	ENDIF
ENDPROC

PROC LBD_SCENE_CLEANUP_PLAYLIST_ONLY()
	STOP_LBD_AUDIO_SCENE()
	SET_FRONTEND_RADIO_ACTIVE(TRUE) 
	PRINTLN(" [CS_LBD_SCENE] LBD_SCENE_CLEANUP, LBD_SCENE_CLEANUP_PLAYLIST_ONLY")
ENDPROC

PROC PREPARE_PLAYER_FOR_LEADERBOARD()
	PRINTLN("PREPARE_PLAYER_FOR_LEADERBOARD  ")
	//NETWORK_SET_VOICE_CHANNEL(LEADERBOARD_VOICE_CHANNEL) *1091621 - Allow players to speak to each other while still on leaderboard
	Disable_MP_Comms()
	DISABLE_SELECTOR()
	STOP_CONTROL_SHAKE(PLAYER_CONTROL) // (746017)
	CLEAR_PRINTS()
	CLEAR_ANY_OBJECTIVE_TEXT()
	CLEAR_ALL_FLOATING_HELP()
	DISPLAY_RADAR(FALSE)	
	NETWORK_SET_LOCAL_PLAYER_INVINCIBLE_TIME(0)
	g_i_ActualLeaderboardPlayerSelected = -1
	PRINTLN("[SEL_LOGIC] PREPARE_PLAYER_FOR_LEADERBOARD g_i_ActualLeaderboardPlayerSelected = -1 ", g_i_ActualLeaderboardPlayerSelected)
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		SET_PLAYER_INVINCIBLE(PLAYER_ID(), TRUE)
	ENDIF
	IF IS_SKYSWOOP_AT_GROUND()
		NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE) // don't change this 1246265 // BH - Ive changed to make player visible so you dont see him deleted.
	ENDIF
	IF IS_PC_VERSION()
		CLOSE_MP_TEXT_CHAT()		// url:bugstar:2201855. ST.
	ENDIF
	SET_FRONTEND_ACTIVE(FALSE)
ENDPROC

PROC INVISIBLE_FOR_LBD()
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		PRINTLN(" INVISIBLE_FOR_LBD ")
		SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)
	ENDIF
ENDPROC

PROC CLEANUP_LEADERBOARD_PREPARATION()
	PRINTLN("CLEANUP_LEADERBOARD_PREPARATION  ")
	ENABLE_ALL_MP_HUD()
	Enable_MP_Comms()
	ENABLE_SELECTOR()
	DISPLAY_RADAR(TRUE)
ENDPROC

CONST_FLOAT fTopRightPosWrap 0.840 // 0.83188896 //0.843
//TWEAK_FLOAT fTopRightPosWrap 0.840 //0.83188896 //0.843
PROC DRAW_POSITION_TOP_RIGHT(LEADERBOARD_PLACEMENT_TOOLS& Placement, LBD_SUB_MODE eSubMode, PLAYER_INDEX playerId, INT iRank)

	// 1420189
	IF g_bOnCoopMission
	OR (IS_PLAYER_ON_A_PLAYLIST(PLAYER_ID()) AND eSubMode <> SUB_PLAYLIST)  
	
		EXIT
	ENDIF

	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_FADE)
	SET_TEXT_RIGHT_JUSTIFY(TRUE)
								
	TEXT_LABEL_23 tlLabel	
	IF iRank > 0	
		tlLabel = "LBD_PLC_"
		tlLabel += iRank	
		
		IF playerId = PLAYER_ID() 
			SET_TEXT_WRAP(0, fTopRightPosWrap) 
		
			SET_LBD_TOP_RIGHT_FONT_AND_SIZE(Placement.aStyle.TS_STANDARDSMALLHUD, DROPSTYLE_DROPSHADOWONLY)
			SET_TEXT_STYLE(Placement.aStyle.TS_STANDARDSMALLHUD)
			Placement.TextPlacement[TEXT_PLACEMENT_LB_LARGE_POS].x = (LARGE_POS_X + 0.01111104)
			Placement.TextPlacement[TEXT_PLACEMENT_LB_LARGE_POS].y = LARGE_POS_Y
			
			SET_TEXT_COLOUR_WITH_HUD_COLOUR(Placement.aStyle.TS_TITLEHUD, HUD_COLOUR_PURE_WHITE)
			
			//SET_TEXT_COLOUR_WITH_HUD_COLOUR(Placement.aStyle.TS_TITLEHUD, FRIENDLY_DM_COLOUR())
			
			DRAW_TEXT(Placement.TextPlacement[TEXT_PLACEMENT_LB_LARGE_POS], Placement.aStyle.TS_TITLEHUD, tlLabel)	
		ENDIF
	ENDIF
 ENDPROC

FUNC BOOL HAS_PLAY_AGAIN_BUTTON_BEEN_PRESSED()
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)			
	
		PRINTLN("------------------------------------------------------------------------------")
		PRINTLN("--                                                                          --")
		PRINTLN("--                   HAS_PLAY_AGAIN_BUTTON_BEEN_PRESSED				     --")
		PRINTLN("--                                                                          --")
		PRINTLN("------------------------------------------------------------------------------")
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_LEADERBOARD_RESTART_BUTTON_BEEN_PRESSED()
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)			
	
		PRINTLN("------------------------------------------------------------------------------")
		PRINTLN("--                                                                          --")
		PRINTLN("--                   RESTART_PRESSED				                         --")
		PRINTLN("--                                                                          --")
		PRINTLN("------------------------------------------------------------------------------")
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_LEADERBOARD_RANDOM_RESTART_BUTTON_BEEN_PRESSED()
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_Y)			
	
		PRINTLN("------------------------------------------------------------------------------")
		PRINTLN("--                                                                          --")
		PRINTLN("--                   RANDOM_RESTART_PRESSED				                 --")
		PRINTLN("--                                                                          --")
		PRINTLN("------------------------------------------------------------------------------")
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_LEADERBOARD_NEXT_BUTTON_BEEN_PRESSED()
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)			
	
		PRINTLN("------------------------------------------------------------------------------")
		PRINTLN("--                                                                          --")
		PRINTLN("--                         NEXT_PRESSED				                     --")
		PRINTLN("--                                                                          --")
		PRINTLN("------------------------------------------------------------------------------")
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC
FUNC BOOL HAS_LEADERBOARD_SPECTATE_BUTTON_BEEN_PRESSED()
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_Y)		
	
		PRINTLN("------------------------------------------------------------------------------")
		PRINTLN("--                                                                          --")
		PRINTLN("--                   SPECTATE_PRESSED				                     --")
		PRINTLN("--                                                                          --")
		PRINTLN("------------------------------------------------------------------------------")
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAVE_ALL_COMMON_LEADERBOARD_ELEMENTS_LOADED(LEADERBOARD_PLACEMENT_TOOLS& Placement, BOOL bNeedVehBlobs = FALSE)
	BOOL bIgnoreButtonChecks
	bIgnoreButtonChecks = bNeedVehBlobs
	IF bNeedVehBlobs
		REQUEST_STREAMED_TEXTURE_DICT("MPCarHUD")
	ENDIF
	REQUEST_STREAMED_TEXTURE_DICT("CommonMenu")
	REQUEST_STREAMED_TEXTURE_DICT("MPLeaderboard")
	
	IF NOT HAS_SCALEFORM_MOVIE_LOADED(Placement.ButtonMovie[GET_SCALEFORM_INDEX_FOR_HUD(HUD_SCALEFORM_INSTRUCTIONAL)] )
		Placement.ButtonMovie[GET_SCALEFORM_INDEX_FOR_HUD(HUD_SCALEFORM_INSTRUCTIONAL)] = REQUEST_SCALEFORM_MOVIE("instructional_buttons")
	ENDIF
	IF NOT HAS_SCALEFORM_MOVIE_LOADED(Placement.ButtonMovie[GET_SCALEFORM_INDEX_FOR_HUD(HUD_SCALEFORM_LOADING_ICON)] )	
		Placement.ButtonMovie[GET_SCALEFORM_INDEX_FOR_HUD(HUD_SCALEFORM_LOADING_ICON)] = REQUEST_SCALEFORM_MOVIE("SAVING_FOOTER")
	ENDIF
	
	IF HAS_STREAMED_TEXTURE_DICT_LOADED("MPLeaderboard")
		IF HAS_STREAMED_TEXTURE_DICT_LOADED("CommonMenu")
			IF NOT bNeedVehBlobs
			OR HAS_STREAMED_TEXTURE_DICT_LOADED("MPCarHUD")
				IF HAS_SCALEFORM_MOVIE_LOADED(Placement.ButtonMovie[GET_SCALEFORM_INDEX_FOR_HUD(HUD_SCALEFORM_INSTRUCTIONAL)] )
				OR bIgnoreButtonChecks
					IF HAS_SCALEFORM_MOVIE_LOADED(Placement.ButtonMovie[GET_SCALEFORM_INDEX_FOR_HUD(HUD_SCALEFORM_LOADING_ICON)] )	
					OR bIgnoreButtonChecks
					
						bNeedLbdCleanup = TRUE
						
						RETURN TRUE
					ELSE
						PRINTLN("HAVE_ALL_COMMON_LEADERBOARD_ELEMENTS_LOADED, (HUD_SCALEFORM_LOADING_ICON)")
					ENDIF
				ELSE
					PRINTLN("HAVE_ALL_COMMON_LEADERBOARD_ELEMENTS_LOADED, (HUD_SCALEFORM_INSTRUCTIONAL)")
				ENDIF
			ELSE
				PRINTLN("HAVE_ALL_COMMON_LEADERBOARD_ELEMENTS_LOADED, (MPCarHUD)")
			ENDIF
		ELSE
			PRINTLN("HAVE_ALL_COMMON_LEADERBOARD_ELEMENTS_LOADED, (CommonMenu)")
		ENDIF
	ELSE
		PRINTLN("HAVE_ALL_COMMON_LEADERBOARD_ELEMENTS_LOADED, (MPLeaderboard)")
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC UNLOAD_ALL_COMMON_LEADERBOARD_ELEMENTS(LEADERBOARD_PLACEMENT_TOOLS& Placement, BOOL bNeedVehBlobs = FALSE)
	IF bNeedLbdCleanup
		IF bNeedVehBlobs
			//IF HAS_STREAMED_TEXTURE_DICT_LOADED("MPCarHUD")
				SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPCarHUD")
			//ENDIF
		ENDIF
		//IF HAS_STREAMED_TEXTURE_DICT_LOADED("MPLeaderboard")
			SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPLeaderboard")
		//ENDIF
		//IF HAS_STREAMED_TEXTURE_DICT_LOADED("CommonMenu")
			SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("CommonMenu")
		//ENDIF
		
		SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("mphud")
		
		INT i
		FOR i = 0 TO (MAX_NUMBER_MPHUD_SCALEFORM_MOVIES -1)
			IF HAS_SCALEFORM_MOVIE_LOADED(Placement.ButtonMovie[GET_SCALEFORM_INDEX_FOR_HUD(INT_TO_ENUM(HUD_SCALEFORM_INDEX, i))])
				SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(Placement.ButtonMovie[GET_SCALEFORM_INDEX_FOR_HUD(INT_TO_ENUM(HUD_SCALEFORM_INDEX, i))])
			ENDIF
		ENDFOR
		PRINTLN("UNLOAD_ALL_COMMON_LEADERBOARD_ELEMENTS, YUP")
	ENDIF
	
	bNeedLbdCleanup = FALSE
	PRINTLN("UNLOAD_ALL_COMMON_LEADERBOARD_ELEMENTS")
ENDPROC

CONST_FLOAT STAR_X 			0.569
CONST_FLOAT STAR_Y 			0.231
CONST_FLOAT STAR_W 			0.019
CONST_FLOAT STAR_H 			0.031

PROC RENDER_LAP_STAR(LBD_VARS& lbdVars, LEADERBOARD_PLACEMENT_TOOLS& Placement, INT i)

	IF NOT g_b_ShowBestLap
	OR NOT lbdVars.bDrawBestLapStar
		EXIT
	ENDIF

	Placement.SpritePlacement[SPRITE_PLACEMENT_LAP_STAR].x = STAR_X
	Placement.SpritePlacement[SPRITE_PLACEMENT_LAP_STAR].y = STAR_Y + (TO_FLOAT(i) * RECT_Y_OFFSET)
	Placement.SpritePlacement[SPRITE_PLACEMENT_LAP_STAR].w = STAR_W
	Placement.SpritePlacement[SPRITE_PLACEMENT_LAP_STAR].h = STAR_H
	Placement.SpritePlacement[SPRITE_PLACEMENT_LAP_STAR].a = 255

//	IF lbdVars.iPlayerBestLap = g_i_ActualLeaderboardPlayerSelected
//		SET_SPRITE_BLACK(Placement.SpritePlacement[SPRITE_PLACEMENT_LAP_STAR])
//	ELSE
		SET_SPRITE_WHITE(Placement.SpritePlacement[SPRITE_PLACEMENT_LAP_STAR])
//	ENDIF
	DRAW_2D_SPRITE("MPLeaderboard", "Leaderboard_Star_Icon", Placement.SpritePlacement[SPRITE_PLACEMENT_LAP_STAR], FALSE, HIGHLIGHT_OPTION_NORMAL, GFX_ORDER_AFTER_FADE)
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_FADE)
ENDPROC

#IF IS_DEBUG_BUILD

FUNC STRING GET_STRING_FROM_GFX_DRAW_ORDER(GFX_DRAW_ORDER eOrder)
	
	STRING sReturn
	
	SWITCH eOrder
		CASE GFX_ORDER_BEFORE_HUD_PRIORITY_LOW		sReturn = "GFX_ORDER_BEFORE_HUD_PRIORITY_LOW"		BREAK
		CASE GFX_ORDER_BEFORE_HUD					sReturn = "GFX_ORDER_BEFORE_HUD"					BREAK
		CASE GFX_ORDER_BEFORE_HUD_PRIORITY_HIGH		sReturn = "GFX_ORDER_BEFORE_HUD_PRIORITY_HIGH"		BREAK
		CASE GFX_ORDER_AFTER_HUD_PRIORITY_LOW		sReturn = "GFX_ORDER_AFTER_HUD_PRIORITY_LOW"		BREAK
		CASE GFX_ORDER_AFTER_HUD					sReturn = "GFX_ORDER_AFTER_HUD"						BREAK
		CASE GFX_ORDER_AFTER_HUD_PRIORITY_HIGH		sReturn = "GFX_ORDER_AFTER_HUD_PRIORITY_HIGH"		BREAK
		CASE GFX_ORDER_AFTER_FADE_PRIORITY_LOW		sReturn = "GFX_ORDER_AFTER_FADE_PRIORITY_LOW"		BREAK
		CASE GFX_ORDER_AFTER_FADE					sReturn = "GFX_ORDER_AFTER_FADE"					BREAK
		CASE GFX_ORDER_AFTER_FADE_PRIORITY_HIGH		sReturn = "GFX_ORDER_AFTER_FADE_PRIORITY_HIGH"		BREAK
	ENDSWITCH
	
	RETURN sReturn
	
ENDFUNC

#ENDIF

PROC DRAW_WEE_POSITION_RECTS(LEADERBOARD_PLACEMENT_TOOLS& Placement, INT i, STRING strHeadshotTxd)

	// 100% black background
	Placement.RectPlacement[RECT_PLACEMENT_BLACK_POS].x = RED_X
	Placement.RectPlacement[RECT_PLACEMENT_BLACK_POS].y = COLUMN_RECT_Y + (TO_FLOAT(i) * RECT_Y_OFFSET)
	Placement.RectPlacement[RECT_PLACEMENT_BLACK_POS].w = RED_W
	Placement.RectPlacement[RECT_PLACEMENT_BLACK_POS].h = RECT_HEIGHT_CONSTANT
	Placement.RectPlacement[RECT_PLACEMENT_BLACK_POS].a = HUNDRED_ALPHA
	DRAW_RECTANGLE(Placement.RectPlacement[RECT_PLACEMENT_BLACK_POS])
	
	Placement.SpritePlacement[SPRITE_PLACEMENT_HEAD_POS].y = COLUMN_RECT_Y + (TO_FLOAT(i) * RECT_Y_OFFSET)
		
	IF NOT IS_STRING_NULL_OR_EMPTY(strHeadshotTxd)
		REQUEST_STREAMED_TEXTURE_DICT(strHeadshotTxd)
		IF HAS_STREAMED_TEXTURE_DICT_LOADED(strHeadshotTxd)
			DRAW_2D_SPRITE(strHeadshotTxd, strHeadshotTxd, Placement.SpritePlacement[SPRITE_PLACEMENT_HEAD_POS], FALSE, DEFAULT, GFX_ORDER_AFTER_FADE)
			SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_FADE)
		ENDIF
	ENDIF
	
ENDPROC

// TEAM NAMES WEE RECT AT THE TOP WHERE POSITION SITS
TWEAK_FLOAT tfTEAM_WEE_RECT_W	0.6785
TWEAK_FLOAT tfTEAM_WEE_RECT_X	0.4995

TWEAK_FLOAT tfTEAM_POS_X		0.171

TWEAK_FLOAT tfTEAM_NAME_X		0.185

PROC DRAW_TEAM_NAMES_BARS(LBD_VARS &lbdVars, LEADERBOARD_PLACEMENT_TOOLS& Placement, INT iTeamNamesRow, INT iTeamPos)//, INT iTeam)
	// Bars
	Placement.RectPlacement[RECT_PLACEMENT_BLACK_POS].x = tfTEAM_WEE_RECT_X
	Placement.RectPlacement[RECT_PLACEMENT_BLACK_POS].y = COLUMN_RECT_Y + (TO_FLOAT(iTeamNamesRow) * RECT_Y_OFFSET)
	Placement.RectPlacement[RECT_PLACEMENT_BLACK_POS].w = tfTEAM_WEE_RECT_W
	Placement.RectPlacement[RECT_PLACEMENT_BLACK_POS].h = RECT_HEIGHT_CONSTANT
	Placement.RectPlacement[RECT_PLACEMENT_BLACK_POS].a = EIGHTY_ALPHA
	DRAW_RECTANGLE(Placement.RectPlacement[RECT_PLACEMENT_BLACK_POS])
	
	// Positions
	Placement.TextPlacement[TEXT_PLACEMENT_TEAM_SCORE].x = tfTEAM_POS_X 
	Placement.TextPlacement[TEXT_PLACEMENT_TEAM_SCORE].y = VARIABLE_BASE_Y+(TO_FLOAT(iTeamNamesRow) * RECT_Y_OFFSET)	
	SET_STANDARD_LBD_TEXT(Placement.aStyle.TS_LBD)
	Placement.aStyle.TS_LBD.aFont = FONT_STANDARD
	SET_TEXT_STYLE(Placement.aStyle.TS_LBD)
	SET_TEXT_WHITE(Placement.aStyle.TS_LBD)
	SET_TEXT_CENTRE(TRUE)
	
	//DRAW_TEXT_WITH_NUMBER(Placement.TextPlacement[TEXT_PLACEMENT_TEAM_SCORE], Placement.aStyle.TS_LBD, "NUMBER", iTeamPos, FONT_CENTRE)
	
	// "Team ~1~"
	Placement.TextPlacement[TEXT_PLACEMENT_TEAM_TOTALS].x = tfTEAM_NAME_X
	Placement.TextPlacement[TEXT_PLACEMENT_TEAM_TOTALS].y = VARIABLE_BASE_Y+(TO_FLOAT(iTeamNamesRow) * RECT_Y_OFFSET)	
	SET_STANDARD_LBD_PLYR_NAME_TEXT(Placement.aStyle.TS_LBDPLYR)
	SET_TEXT_STYLE(Placement.aStyle.TS_LBD)
	SET_TEXT_JUSTIFICATION(FONT_LEFT)
	SET_TEXT_WHITE(Placement.aStyle.TS_LBD)	
	
	IF NOT IS_STRING_NULL_OR_EMPTY(lbdVars.tl63TeamNames)//[iTeam])
		IF DOES_TEXT_LABEL_EXIST(lbdVars.tl63TeamNames)
			DRAW_TEXT(Placement.TextPlacement[TEXT_PLACEMENT_TEAM_TOTALS], Placement.aStyle.TS_LBD, lbdVars.tl63TeamNames)//[iTeam])
		ELSE
			DRAW_TEXT_WITH_PLAYER_NAME(Placement.TextPlacement[TEXT_PLACEMENT_TEAM_TOTALS], Placement.aStyle.TS_LBD, lbdVars.tl63TeamNames)
		ENDIF
	ELSE
		// Team 1, Team 2 etc.
		DRAW_TEXT_WITH_NUMBER(Placement.TextPlacement[TEXT_PLACEMENT_TEAM_TOTALS], Placement.aStyle.TS_LBD, "TEAM_LBD_NMS", iTeamPos, FONT_LEFT)
	ENDIF

	SET_TEXT_CENTRE(TRUE)
ENDPROC

TWEAK_FLOAT tfTEAM_RECT_W	0.6785
TWEAK_FLOAT tfTEAM_RECT_X	0.4995

PROC DRAW_RECT_LONG_TEAM_BARS(LEADERBOARD_PLACEMENT_TOOLS& Placement, LBD_SUB_MODE eSubMode, INT i, INT iLocalTeam, INT iEnemyTeam)

	// 50% black
	Placement.RectPlacement[RECT_PLACEMENT_BLACK_POS].x = tfTEAM_RECT_X
	Placement.RectPlacement[RECT_PLACEMENT_BLACK_POS].y = COLUMN_RECT_Y + (TO_FLOAT(i) * RECT_Y_OFFSET)
	Placement.RectPlacement[RECT_PLACEMENT_BLACK_POS].w = tfTEAM_RECT_W
	Placement.RectPlacement[RECT_PLACEMENT_BLACK_POS].h = RECT_HEIGHT_CONSTANT
	Placement.RectPlacement[RECT_PLACEMENT_BLACK_POS].a = FIFTY_ALPHA
	DRAW_RECTANGLE(Placement.RectPlacement[RECT_PLACEMENT_BLACK_POS])
	
	// 30% colour
	SWITCH eSubMode			
		CASE SUB_DM_TEAM
		CASE SUB_RACE_RALLY
		CASE SUB_MISSIONS_NORMAL
		CASE SUB_MISSIONS_CTF
		CASE SUB_MISSIONS_TIMED
		CASE SUB_MISSIONS_NO_HUD
		CASE SUB_MISSIONS_VERSUS
		CASE SUB_MISSIONS_HEIST
		CASE SUB_MISSIONS_LTS
		CASE SUB_PLAYLIST
			SET_RECT_TO_THIS_HUD_COLOUR(Placement.RectPlacement[RECT_PLACEMENT_HORZ], GET_RECT_SELECTION_COLOUR(eSubMode, iLocalTeam, iEnemyTeam))
		BREAK
	ENDSWITCH	
ENDPROC

TWEAK_FLOAT SCROLL_X		0.18025
TWEAK_FLOAT SCROLL_Y		0.854

TWEAK_FLOAT SCROLLARR_H 0.038
TWEAK_FLOAT SCROLLARR_W 0.025
TWEAK_FLOAT SCROLLARR_X 0.376
TWEAK_FLOAT SCROLLARR_Y 0.867

FUNC INT GET_SCROLLING_NUMBER_SELECTION(LBD_VARS& lbdVars)
	RETURN (g_i_ActualLeaderboardPlayerSelected - lbdVars.iTeamRowsPassed)
ENDFUNC

PROC DRAW_SCROLLBARS(LBD_VARS& lbdVars, LEADERBOARD_PLACEMENT_TOOLS& Placement)
	IF SHOULD_DRAW_SCROLLBAR_COUNTER(lbdVars)
	
		INT iNumberToShowLeft  = GET_SCROLLING_NUMBER_SELECTION(lbdVars)
		INT iNumberToShowRight = lbdVars.iNumPlayersOnLbd
	
		// 50% black rect
		Placement.RectPlacement[RECT_PLACEMENT_COUNT].x = COUNT_LONG__RECT_X
		Placement.RectPlacement[RECT_PLACEMENT_COUNT].y = COUNT_RECT_Y 
		Placement.RectPlacement[RECT_PLACEMENT_COUNT].w = COUNT_LONG__RECT_W
		Placement.RectPlacement[RECT_PLACEMENT_COUNT].h = RECT_HEIGHT_CONSTANT
		Placement.RectPlacement[RECT_PLACEMENT_COUNT].a = FIFTY_ALPHA
		DRAW_RECTANGLE(Placement.RectPlacement[RECT_PLACEMENT_COUNT])
		
		// 1/16
		Placement.TextPlacement[TEXT_PLACEMENT_SELECTION_COUNT].x = SCROLL_X 
		Placement.TextPlacement[TEXT_PLACEMENT_SELECTION_COUNT].y = SCROLL_Y
		SET_STANDARD_LBD_TEXT(Placement.aStyle.TS_LBD)
		Placement.aStyle.TS_LBD.aFont = FONT_STANDARD
		SET_TEXT_STYLE(Placement.aStyle.TS_LBD)
		SET_TEXT_CENTRE(TRUE)	
		DRAW_TEXT_WITH_2_NUMBERS(Placement.TextPlacement[TEXT_PLACEMENT_SELECTION_COUNT], Placement.aStyle.TS_LBD, "LBD_NUM", iNumberToShowLeft, iNumberToShowRight, FONT_CENTRE)
		
		// arrows up down
		Placement.SpritePlacement[SPRITE_PLACEMENT_ARROWS].x = SCROLLARR_X  
		Placement.SpritePlacement[SPRITE_PLACEMENT_ARROWS].y = SCROLLARR_Y
		Placement.SpritePlacement[SPRITE_PLACEMENT_ARROWS].w = SCROLLARR_W
		Placement.SpritePlacement[SPRITE_PLACEMENT_ARROWS].h = SCROLLARR_H 
		Placement.SpritePlacement[SPRITE_PLACEMENT_ARROWS].a = 255
		Placement.SpritePlacement[SPRITE_PLACEMENT_ARROWS].r = 255
		Placement.SpritePlacement[SPRITE_PLACEMENT_ARROWS].g = 255
		Placement.SpritePlacement[SPRITE_PLACEMENT_ARROWS].b = 255
		Placement.SpritePlacement[SPRITE_PLACEMENT_ARROWS].fRotation = 0
		DRAW_2D_SPRITE("CommonMenu", "shop_arrows_upANDdown", Placement.SpritePlacement[SPRITE_PLACEMENT_ARROWS], FALSE, HIGHLIGHT_OPTION_NORMAL, GFX_ORDER_AFTER_FADE)
		SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_FADE)
	ENDIF
ENDPROC

CONST_FLOAT TVOTE_H 0.050
CONST_FLOAT TVOTE_W 0.022
CONST_FLOAT TVOTE_X 0.854
CONST_FLOAT TVOTE_Y 0.000
CONST_FLOAT TVOTE_X_OFF 0.040
CONST_FLOAT TVOTE_ROT 180.00

PROC DRAW_THUMB_VOTE_COUNT(LEADERBOARD_PLACEMENT_TOOLS& Placement, INT iUp, INT iDown)

	SET_SCRIPT_GFX_ALIGN(UI_ALIGN_IGNORE, UI_ALIGN_BOTTOM)
	
	SET_INSTRUCTIONAL_BUTTONS_UNDER_HUD_THIS_FRAME()
	
	FLOAT fThumbOffset

//	IF num instructional button rows = 1
//		fThumbOffset	= 0.0
//	ELSE
		fThumbOffset 	= 0.0365
//	ENDIF
	
	// 50% black Rectangle
	Placement.RectPlacement[RECT_PLACEMENT_VOTE_COUNT].x = THUMB_LONG__RECT_X
	Placement.RectPlacement[RECT_PLACEMENT_VOTE_COUNT].y = (THUMB_RECT_Y - fThumbOffset)
	Placement.RectPlacement[RECT_PLACEMENT_VOTE_COUNT].w = THUMB_LONG__RECT_W
	Placement.RectPlacement[RECT_PLACEMENT_VOTE_COUNT].h = RECT_HEIGHT_CONSTANT
	Placement.RectPlacement[RECT_PLACEMENT_VOTE_COUNT].a = FIFTY_ALPHA
	DRAW_RECTANGLE(Placement.RectPlacement[RECT_PLACEMENT_VOTE_COUNT])
	
	// Likes number
	Placement.TextPlacement[TEXT_PLACEMENT_THUMB_COUNT].x = TEXT_VOTE_UP_X
	Placement.TextPlacement[TEXT_PLACEMENT_THUMB_COUNT].y = (TEXT_VOTE_Y - fThumbOffset)
	SET_STANDARD_LBD_TEXT(Placement.aStyle.TS_LBD)
	Placement.aStyle.TS_LBD.aFont = FONT_STANDARD
	SET_TEXT_STYLE(Placement.aStyle.TS_LBD)
	SET_TEXT_CENTRE(TRUE)
	DRAW_TEXT_WITH_NUMBER(Placement.TextPlacement[TEXT_PLACEMENT_THUMB_COUNT], Placement.aStyle.TS_LBD, "NUMBER", iUp, FONT_CENTRE)
	
	// Dislikes number
	Placement.TextPlacement[TEXT_PLACEMENT_THUMB_COUNT].x = TEXT_VOTE_UP_X + TEXT_VOTE_X_OFFSET
	Placement.TextPlacement[TEXT_PLACEMENT_THUMB_COUNT].y = (TEXT_VOTE_Y - fThumbOffset)
	SET_STANDARD_LBD_TEXT(Placement.aStyle.TS_LBD)
	Placement.aStyle.TS_LBD.aFont = FONT_STANDARD
	SET_TEXT_STYLE(Placement.aStyle.TS_LBD)
	SET_TEXT_CENTRE(TRUE)
	DRAW_TEXT_WITH_NUMBER(Placement.TextPlacement[TEXT_PLACEMENT_THUMB_COUNT], Placement.aStyle.TS_LBD, "NUMBER", iDown, FONT_CENTRE)

	// Thumb Up
	Placement.SpritePlacement[SPRITE_PLACEMENT_THUMB_VOTE].x = TVOTE_X  
	Placement.SpritePlacement[SPRITE_PLACEMENT_THUMB_VOTE].y = (TVOTE_Y  - fThumbOffset)
	Placement.SpritePlacement[SPRITE_PLACEMENT_THUMB_VOTE].w = TVOTE_W
	Placement.SpritePlacement[SPRITE_PLACEMENT_THUMB_VOTE].h = TVOTE_H 
	Placement.SpritePlacement[SPRITE_PLACEMENT_THUMB_VOTE].a = 255
	Placement.SpritePlacement[SPRITE_PLACEMENT_THUMB_VOTE].r = 255
	Placement.SpritePlacement[SPRITE_PLACEMENT_THUMB_VOTE].g = 255
	Placement.SpritePlacement[SPRITE_PLACEMENT_THUMB_VOTE].b = 255
	Placement.SpritePlacement[SPRITE_PLACEMENT_THUMB_VOTE].fRotation = 0
	DRAW_2D_SPRITE("MPLeaderboard", "Leaderboard_Thumb", Placement.SpritePlacement[SPRITE_PLACEMENT_THUMB_VOTE], FALSE, HIGHLIGHT_OPTION_NORMAL, GFX_ORDER_AFTER_FADE)
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_FADE)
	
	// Thumb Down
	Placement.SpritePlacement[SPRITE_PLACEMENT_THUMB_VOTE].x = TVOTE_X + TVOTE_X_OFF
	Placement.SpritePlacement[SPRITE_PLACEMENT_THUMB_VOTE].fRotation = TVOTE_ROT
	DRAW_2D_SPRITE("MPLeaderboard", "Leaderboard_Thumb", Placement.SpritePlacement[SPRITE_PLACEMENT_THUMB_VOTE], FALSE, HIGHLIGHT_OPTION_NORMAL, GFX_ORDER_AFTER_FADE)
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_FADE)
	
	RESET_SCRIPT_GFX_ALIGN()
ENDPROC

PROC CHECK_LBD_THUMBS(LBD_VARS& lbdVars, INT iPlayerId)
	IF iPlayerId <> -1
		IF IS_BIT_SET(GlobalplayerBD_FM[iPlayerId].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_VOTED_UP)
		OR IS_BIT_SET(GlobalplayerBD_FM[iPlayerId].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_VOTED_DOWN)
			lbdVars.bVoted = TRUE
		ELSE
			lbdVars.bVoted = FALSE
		ENDIF
		IF IS_BIT_SET(GlobalplayerBD_FM[iPlayerId].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_VOTED_DOWN)
			lbdVars.bVotedDown = TRUE
		ELSE
			lbdVars.bVotedDown = FALSE
		ENDIF
	ENDIF
ENDPROC

PROC DRAW_LBD_THUMBS(LEADERBOARD_PLACEMENT_TOOLS& Placement, BOOL bVoted, BOOL bVotedDown, INT iRow)
	IF bVoted
		SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_FADE)		
		Placement.SpritePlacement[SPRITE_PLACEMENT_LBD_THUMBS].x = LBVOTE_X
		Placement.SpritePlacement[SPRITE_PLACEMENT_LBD_THUMBS].y = LBVOTE_Y+(TO_FLOAT(iRow) * RECT_Y_OFFSET) 
		Placement.SpritePlacement[SPRITE_PLACEMENT_LBD_THUMBS].w = LBVOTE_W 
		Placement.SpritePlacement[SPRITE_PLACEMENT_LBD_THUMBS].h = LBVOTE_H 
		IF bVotedDown
			Placement.SpritePlacement[SPRITE_PLACEMENT_LBD_THUMBS].fRotation = LBVOTE_ROT
		ELSE
			Placement.SpritePlacement[SPRITE_PLACEMENT_LBD_THUMBS].fRotation = 0
		ENDIF
		Placement.SpritePlacement[SPRITE_PLACEMENT_LBD_THUMBS].a = 255
		Placement.SpritePlacement[SPRITE_PLACEMENT_LBD_THUMBS].r = 255
		Placement.SpritePlacement[SPRITE_PLACEMENT_LBD_THUMBS].g = 255
		Placement.SpritePlacement[SPRITE_PLACEMENT_LBD_THUMBS].b = 255
		DRAW_2D_SPRITE("MPLeaderboard", "Leaderboard_Thumb", Placement.SpritePlacement[SPRITE_PLACEMENT_LBD_THUMBS], FALSE, HIGHLIGHT_OPTION_NORMAL, GFX_ORDER_AFTER_FADE)
	ENDIF
ENDPROC

PROC CHECK_MOUSE_INSIDE_PLAYER_RECT( INT i )

	IF NOT IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		EXIT
	ENDIF
			
	IF IS_MOUSE_INSIDE_PLAYER_RECT(i, TRUE)
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
			g_iMenuCursorItem = i
		ELSE
			g_iMenuCursorItem = MENU_CURSOR_NO_ITEM
		ENDIF
	ENDIF
	
ENDPROC

PROC DRAW_RECT_PLAYER_BARS(LEADERBOARD_PLACEMENT_TOOLS& Placement, LBD_SUB_MODE eSubMode, BOOL bSpectator, PLAYER_INDEX playerId, INT i, INT iLocalTeam = -1, INT iEnemyTeam = -1, INT iTeam = -1, INT iLBDTeam = -1)

	BOOL bSelected
	
	INT iCarriedTeam = -1
	
	IF i = g_i_LeaderboardRowCounter
	OR ( bSpectator AND ( g_i_ActualLeaderboardPlayerSelected = i ) )
		bSelected = TRUE
	ELSE
		bSelected = FALSE
	ENDIF
	
	BOOL bTargetPlayer = FALSE
	
	HIGHLIGHT_OPTION eHighlightOption = HIGHLIGHT_OPTION_NORMAL
		
	HUD_COLOURS eHudColour = HUD_COLOUR_FREEMODE

	SWITCH eSubMode
	
		CASE SUB_SHOOT_RANGE
			IF playerId = PLAYER_ID()
				eHudColour = HUD_COLOUR_ORANGE
			ELSE
				eHudColour = HUD_COLOUR_PURPLE
			ENDIF
			Placement.RectPlacement[RECT_PLACEMENT_HORZ].x = PLAYER_NAME_RECT_X
			Placement.RectPlacement[RECT_PLACEMENT_HORZ].y = COLUMN_RECT_Y + (TO_FLOAT(i) * RECT_Y_OFFSET)
			Placement.RectPlacement[RECT_PLACEMENT_HORZ].w = PLAYER_NAME_RECT_W
			Placement.RectPlacement[RECT_PLACEMENT_HORZ].h = RECT_HEIGHT_CONSTANT
			
			IF bSelected
				Placement.RectPlacement[RECT_PLACEMENT_HORZ].a = HUNDRED_ALPHA
			ELSE
				Placement.RectPlacement[RECT_PLACEMENT_HORZ].a = THIRTY_ALPHA
			ENDIF
			
			CHECK_MOUSE_INSIDE_PLAYER_RECT(i)
					
		BREAK
	
		CASE SUB_RACE_RALLY
			eHudColour = GET_RECT_SELECTION_COLOUR(eSubMode, iLocalTeam, iEnemyTeam)
			Placement.RectPlacement[RECT_PLACEMENT_HORZ].x = PLAYER_NAME_RECT_X
			Placement.RectPlacement[RECT_PLACEMENT_HORZ].y = COLUMN_RECT_Y + (TO_FLOAT(i) * RECT_Y_OFFSET)
			Placement.RectPlacement[RECT_PLACEMENT_HORZ].w = PLAYER_NAME_RECT_W
			Placement.RectPlacement[RECT_PLACEMENT_HORZ].h = RECT_HEIGHT_CONSTANT
			IF bSelected
				Placement.RectPlacement[RECT_PLACEMENT_HORZ].a = HUNDRED_ALPHA
			ELSE
				Placement.RectPlacement[RECT_PLACEMENT_HORZ].a = THIRTY_ALPHA
			ENDIF
			
			CHECK_MOUSE_INSIDE_PLAYER_RECT(i)
			
		BREAK		
		
		CASE SUB_PLAYLIST
			eHudColour = HUD_COLOUR_FRIENDLY
			Placement.RectPlacement[RECT_PLACEMENT_HORZ].x = PLAYER_NAME_RECT_X
			Placement.RectPlacement[RECT_PLACEMENT_HORZ].y = COLUMN_RECT_Y + (TO_FLOAT(i) * RECT_Y_OFFSET)
			Placement.RectPlacement[RECT_PLACEMENT_HORZ].w = PLAYER_NAME_RECT_W
			Placement.RectPlacement[RECT_PLACEMENT_HORZ].h = RECT_HEIGHT_CONSTANT
			IF bSelected
				Placement.RectPlacement[RECT_PLACEMENT_HORZ].a = HUNDRED_ALPHA
			ELSE
				IF bTargetPlayer
					Placement.RectPlacement[RECT_PLACEMENT_HORZ].a = FIFTEEN_ALPHA
				ELSE
					Placement.RectPlacement[RECT_PLACEMENT_HORZ].a = THIRTY_ALPHA
				ENDIF
			ENDIF
			
			CHECK_MOUSE_INSIDE_PLAYER_RECT(i)
		BREAK

		CASE SUB_MISSIONS_NORMAL
		CASE SUB_MISSIONS_CTF
		CASE SUB_MISSIONS_NO_HUD
		CASE SUB_MISSIONS_VERSUS
		CASE SUB_MISSIONS_HEIST
		CASE SUB_MISSIONS_LTS
		CASE SUB_MISSIONS_TIMED
		CASE SUB_FINAL_ROUNDS
		CASE SUB_DM_TEAM
		CASE SUB_DM_FFA	
		CASE SUB_DM_UPDATE
		CASE SUB_KOTH
		
//			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciTEAM_COLOUR_NAMES)
//				PRINTLN("ciTEAM_COLOUR_NAMES TRUE  ")
//			ELSE
//			
//				PRINTLN("ciTEAM_COLOUR_NAMES FALSE  ")
//			ENDIF
//			
//			IF g_bTeamColoursOverridden
//				PRINTLN("g_bTeamColoursOverridden TRUE  ")
//			ELSE
//			
//				PRINTLN("g_bTeamColoursOverridden FALSE  ")
//			ENDIF
			
			// 2376936: Consistent player teams for new lowrider modes
			IF DOES_LB_USE_CUSTOM_TEAM_COLOURS_FOR_NEW_VS()
			OR bSVM_MISSION()
			OR CONTENT_IS_USING_ARENA()
			
				IF playerId <> INVALID_PLAYER_INDEX()
				AND NOT bSpectator
					IF IS_NET_PLAYER_OK(playerId, FALSE)
					AND NETWORK_IS_PLAYER_A_PARTICIPANT(playerId)
					AND NETWORK_IS_PARTICIPANT_ACTIVE(NETWORK_GET_PARTICIPANT_INDEX(playerId))
						iCarriedTeam = GET_PLAYER_TEAM(playerId)
					ENDIF
				ENDIF
				
				#IF IS_DEBUG_BUILD
				IF GET_COMMANDLINE_PARAM_EXISTS("sc_TeamColourDebug")
					PRINTLN("[RE] [TEAMCOLOUR] - iCarriedTeam = ", iCarriedTeam)
					PRINTLN("[RE] [TEAMCOLOUR] - iTeam = ", iTeam)
					PRINTLN("[RE] [TEAMCOLOUR] - iLocalTeam = ", iLocalTeam, " - iEnemyTeam = ", iEnemyTeam)
				ENDIF
				#ENDIF
				
				IF iCarriedTeam <> -1 AND iCarriedTeam != 8 //no SCTV here or we OOB
					eHudColour = GET_HUD_COLOUR_FOR_FMMC_TEAM(iCarriedTeam, playerId) 
					PRINTLN("DOES_LB_USE_CUSTOM_TEAM_COLOURS_FOR_NEW_VS A  ", iCarriedTeam)
				ELIF iTeam <> -1
					eHudColour = GET_HUD_COLOUR_FOR_FMMC_TEAM(iTeam, playerId) 
					PRINTLN("DOES_LB_USE_CUSTOM_TEAM_COLOURS_FOR_NEW_VS B  ", iTeam)
				ELSE
					eHudColour = GET_RECT_SELECTION_COLOUR(eSubMode, iLocalTeam, iEnemyTeam)
					PRINTLN("DOES_LB_USE_CUSTOM_TEAM_COLOURS_FOR_NEW_VS C iLocalTeam = ", iLocalTeam, " iEnemyTeam = ", iEnemyTeam)
				ENDIF
			ELSE
				PRINTLN("DOES_LB_USE_CUSTOM_TEAM_COLOURS_FOR_NEW_VS D  iLocalTeam = ", iLocalTeam, " iEnemyTeam = ", iEnemyTeam)
				eHudColour = GET_RECT_SELECTION_COLOUR(eSubMode, iLocalTeam, iEnemyTeam)

			ENDIF
			
			IF eSubMode = SUB_FINAL_ROUNDS
			OR g_FinalRoundsLbd
				eHudColour = HUD_COLOUR_FRIENDLY
			ENDIF
			
			// url:bugstar:2905689 - Can we have an indication who was on the Target team at the end. 
			IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_ENTOURAGE(g_FMMC_STRUCT.iAdversaryModeType)
				IF iLBDTeam = 2
					bTargetPlayer = TRUE
				ENDIF
			ENDIF
			
			Placement.RectPlacement[RECT_PLACEMENT_HORZ].x = PLAYER_NAME_RECT_X
			Placement.RectPlacement[RECT_PLACEMENT_HORZ].y = COLUMN_RECT_Y + (TO_FLOAT(i) * RECT_Y_OFFSET)
			Placement.RectPlacement[RECT_PLACEMENT_HORZ].w = PLAYER_NAME_RECT_W
			Placement.RectPlacement[RECT_PLACEMENT_HORZ].h = RECT_HEIGHT_CONSTANT
			IF bSelected
				Placement.RectPlacement[RECT_PLACEMENT_HORZ].a = HUNDRED_ALPHA
			ELSE
				IF bTargetPlayer
					Placement.RectPlacement[RECT_PLACEMENT_HORZ].a = FIFTEEN_ALPHA
				ELSE
					Placement.RectPlacement[RECT_PLACEMENT_HORZ].a = THIRTY_ALPHA
				ENDIF
			ENDIF
			
			CHECK_MOUSE_INSIDE_PLAYER_RECT(i)
			
		BREAK	
		
		CASE SUB_ARENA_CARNAGE
		CASE SUB_RACE_NORMAL
		CASE SUB_RACE_AGG_BEST
		CASE SUB_RACE_AGG_TIME
		CASE SUB_RACE_TARGET
		CASE SUB_RACE_STUNT
		CASE SUB_RACE_BASEJUMP
		CASE SUB_RACE_GTA
		CASE SUB_FREEMODE
		CASE SUB_ARCADE_CNC
		CASE SUB_HORDE
		CASE SUB_KART
		CASE SUB_ARENA_ARCADE
		CASE SUB_ARENA_CTF
		CASE SUB_ARENA_DDERBY
		CASE SUB_ARENA_DETONATION
		CASE SUB_ARENA_FIRE
		CASE SUB_ARENA_GAMESMASTER
		CASE SUB_ARENA_MONSTER
		CASE SUB_ARENA_PASSBOMB
		CASE SUB_ARENA_TAG_TEAM
			eHudColour = GET_RECT_SELECTION_COLOUR(eSubMode, iLocalTeam, iEnemyTeam)
			Placement.RectPlacement[RECT_PLACEMENT_HORZ].x = PLAYER_NAME_RECT_X
			Placement.RectPlacement[RECT_PLACEMENT_HORZ].y = COLUMN_RECT_Y + (TO_FLOAT(i) * RECT_Y_OFFSET)
			Placement.RectPlacement[RECT_PLACEMENT_HORZ].w = PLAYER_NAME_RECT_W
			Placement.RectPlacement[RECT_PLACEMENT_HORZ].h = RECT_HEIGHT_CONSTANT

			IF bSelected
				Placement.RectPlacement[RECT_PLACEMENT_HORZ].a = HUNDRED_ALPHA
			ELSE
				Placement.RectPlacement[RECT_PLACEMENT_HORZ].a = THIRTY_ALPHA
			ENDIF
			
			CHECK_MOUSE_INSIDE_PLAYER_RECT(i)
						
		BREAK
		
		CASE SUB_SC_PRE_RACE_SUB_TITLE
			eHudColour = HUD_COLOUR_ORANGE
			Placement.RectPlacement[RECT_PLACEMENT_HORZ].x = COLUMN_LONG__RECT_X
			Placement.RectPlacement[RECT_PLACEMENT_HORZ].y = COLUMN_RECT_Y + (TO_FLOAT(i) * RECT_Y_OFFSET)
			Placement.RectPlacement[RECT_PLACEMENT_HORZ].w = COLUMN_LONG__RECT_W
			Placement.RectPlacement[RECT_PLACEMENT_HORZ].h = RECT_HEIGHT_CONSTANT
			Placement.RectPlacement[RECT_PLACEMENT_HORZ].a = 255		
		BREAK
		
		CASE SUB_SC_PRE_RACE_CONTENT
			Placement.RectPlacement[RECT_PLACEMENT_HORZ].x = COLUMN_LONG__RECT_X
			Placement.RectPlacement[RECT_PLACEMENT_HORZ].y = COLUMN_RECT_Y + (TO_FLOAT(i) * RECT_Y_OFFSET)
			Placement.RectPlacement[RECT_PLACEMENT_HORZ].w = COLUMN_LONG__RECT_W
			Placement.RectPlacement[RECT_PLACEMENT_HORZ].h = RECT_HEIGHT_CONSTANT
			eHighlightOption = HIGHLIGHT_OPTION_SELECTED_ACTIVE
		BREAK
		
	ENDSWITCH
	
	//Small 4px couloured box next to position box
	Placement.RectPlacement[RECT_PLACEMENT_COLOUR_POS].x = THIN_COLOUR_X
	Placement.RectPlacement[RECT_PLACEMENT_COLOUR_POS].y = COLUMN_RECT_Y + (TO_FLOAT(i) * RECT_Y_OFFSET)
	Placement.RectPlacement[RECT_PLACEMENT_COLOUR_POS].w = THIN_COLOUR_W
	Placement.RectPlacement[RECT_PLACEMENT_COLOUR_POS].h = RECT_HEIGHT_CONSTANT
	Placement.RectPlacement[RECT_PLACEMENT_COLOUR_POS].a = HUNDRED_ALPHA
	
	IF bSpectator
		SET_RECT_TO_NORMAL_COLOUR(Placement.RectPlacement[RECT_PLACEMENT_HORZ])
		IF bSelected = TRUE
			Placement.RectPlacement[RECT_PLACEMENT_HORZ].a = HUNDRED_ALPHA
		ELSE
			Placement.RectPlacement[RECT_PLACEMENT_HORZ].a = FIFTY_ALPHA
		ENDIF
		DRAW_RECTANGLE(Placement.RectPlacement[RECT_PLACEMENT_HORZ], eHighlightOption)
	ENDIF

	IF eSubMode = SUB_SC_PRE_RACE_CONTENT
		SET_RECT_TO_HUD_COLOUR(playerId, Placement.RectPlacement[RECT_PLACEMENT_HORZ])
		SET_RECT_TO_HUD_COLOUR(playerId, Placement.RectPlacement[RECT_PLACEMENT_COLOUR_POS])
	ELIF eSubMode = SUB_FREEMODE AND !bSelected
		SET_RECT_TO_NORMAL_COLOUR(Placement.RectPlacement[RECT_PLACEMENT_HORZ])
		SET_RECT_TO_NORMAL_COLOUR(Placement.RectPlacement[RECT_PLACEMENT_COLOUR_POS])
	ELSE
		SET_RECT_TO_THIS_HUD_COLOUR(Placement.RectPlacement[RECT_PLACEMENT_HORZ], eHudColour)
		SET_RECT_TO_THIS_HUD_COLOUR(Placement.RectPlacement[RECT_PLACEMENT_COLOUR_POS], eHudColour)
	ENDIF
	
	DRAW_RECTANGLE(Placement.RectPlacement[RECT_PLACEMENT_HORZ], eHighlightOption)
	DRAW_RECTANGLE(Placement.RectPlacement[RECT_PLACEMENT_COLOUR_POS])
	PRINTLN("[2056544] LBD DEBUG - DRAW_RECTANGLE, i = ", i)
ENDPROC

PROC DRAW_HORZ_VARIABLE_BARS(LEADERBOARD_PLACEMENT_TOOLS& Placement, LBD_SUB_MODE eSubMode, BOOL bSpectator, PLAYER_INDEX playerId, INT i, BOOL bTeamMode = FALSE, INT iLocalTeam = -1, INT iEnemyTeam = -1)

	BOOL bSelected
	
	bTeamMode = bTeamMode
	playerId = playerId
	bSpectator = bSpectator

	IF i = g_i_ActualLeaderboardPlayerSelected
		bSelected = TRUE
	ELSE
		bSelected = FALSE
	ENDIF
	
	// 50% black
	Placement.RectPlacement[RECT_PLACEMENT_BLACK_POS].x = VAR_RECT_X
	Placement.RectPlacement[RECT_PLACEMENT_BLACK_POS].y = COLUMN_RECT_Y + (TO_FLOAT(i) * RECT_Y_OFFSET)
	Placement.RectPlacement[RECT_PLACEMENT_BLACK_POS].w = VAR_RECT_W
	Placement.RectPlacement[RECT_PLACEMENT_BLACK_POS].h = RECT_HEIGHT_CONSTANT
	
	SET_RECT_TO_THIS_HUD_COLOUR(Placement.RectPlacement[RECT_PLACEMENT_BLACK_POS], HUD_COLOUR_INGAME_BG)
	
	IF i%2 = 0
		Placement.RectPlacement[RECT_PLACEMENT_BLACK_POS].a = SIXTY_ALPHA
	ELSE
		Placement.RectPlacement[RECT_PLACEMENT_BLACK_POS].a = CEIL( TO_FLOAT(SIXTY_ALPHA) * 0.9)
	ENDIF
	
	DRAW_RECTANGLE(Placement.RectPlacement[RECT_PLACEMENT_BLACK_POS])
	
	// Colours
	IF bSelected

		SWITCH eSubMode			
			CASE SUB_DM_TEAM
			CASE SUB_RACE_RALLY
				SET_RECT_TO_THIS_HUD_COLOUR(Placement.RectPlacement[RECT_PLACEMENT_VARS], GET_RECT_SELECTION_COLOUR(eSubMode, iLocalTeam, iEnemyTeam))
				Placement.RectPlacement[RECT_PLACEMENT_VARS].x = VAR_RECT_X
				Placement.RectPlacement[RECT_PLACEMENT_VARS].y = COLUMN_RECT_Y + (TO_FLOAT(i) * RECT_Y_OFFSET)
				Placement.RectPlacement[RECT_PLACEMENT_VARS].w = VAR_RECT_W
				Placement.RectPlacement[RECT_PLACEMENT_VARS].h = RECT_HEIGHT_CONSTANT
				Placement.RectPlacement[RECT_PLACEMENT_VARS].a = THIRTY_ALPHA
			BREAK
			
			CASE SUB_MISSIONS_NORMAL
			CASE SUB_MISSIONS_CTF
			CASE SUB_MISSIONS_NO_HUD
			CASE SUB_MISSIONS_VERSUS
			CASE SUB_MISSIONS_HEIST
			CASE SUB_MISSIONS_LTS			
			CASE SUB_MISSIONS_TIMED
			CASE SUB_PLAYLIST
			CASE SUB_FINAL_ROUNDS
				SET_RECT_TO_THIS_HUD_COLOUR(Placement.RectPlacement[RECT_PLACEMENT_VARS], GET_RECT_SELECTION_COLOUR(eSubMode, iLocalTeam, iEnemyTeam))
				Placement.RectPlacement[RECT_PLACEMENT_VARS].x = VAR_RECT_X
				Placement.RectPlacement[RECT_PLACEMENT_VARS].y = COLUMN_RECT_Y + (TO_FLOAT(i) * RECT_Y_OFFSET)
				Placement.RectPlacement[RECT_PLACEMENT_VARS].w = VAR_RECT_W
				Placement.RectPlacement[RECT_PLACEMENT_VARS].h = RECT_HEIGHT_CONSTANT
				Placement.RectPlacement[RECT_PLACEMENT_VARS].a = THIRTY_ALPHA		
			BREAK
			
			CASE SUB_ARENA_CARNAGE
			CASE SUB_DM_FFA	
			CASE SUB_RACE_NORMAL
			CASE SUB_RACE_STUNT
			CASE SUB_RACE_GTA
			CASE SUB_SHOOT_RANGE
			CASE SUB_RACE_AGG_BEST
			CASE SUB_RACE_AGG_TIME
			CASE SUB_RACE_TARGET
			CASE SUB_ARENA_ARCADE
			CASE SUB_ARENA_CTF
			CASE SUB_ARENA_DDERBY
			CASE SUB_ARENA_DETONATION
			CASE SUB_ARENA_FIRE
			CASE SUB_ARENA_GAMESMASTER
			CASE SUB_ARENA_MONSTER
			CASE SUB_ARENA_PASSBOMB
			CASE SUB_ARENA_TAG_TEAM
			CASE SUB_KOTH
			CASE SUB_DM_UPDATE
				SET_RECT_TO_THIS_HUD_COLOUR(Placement.RectPlacement[RECT_PLACEMENT_VARS], GET_RECT_SELECTION_COLOUR(eSubMode, iLocalTeam, iEnemyTeam))
				Placement.RectPlacement[RECT_PLACEMENT_VARS].x = VAR_RECT_X
				Placement.RectPlacement[RECT_PLACEMENT_VARS].y = COLUMN_RECT_Y + (TO_FLOAT(i) * RECT_Y_OFFSET)
				Placement.RectPlacement[RECT_PLACEMENT_VARS].w = VAR_RECT_W
				Placement.RectPlacement[RECT_PLACEMENT_VARS].h = RECT_HEIGHT_CONSTANT
				Placement.RectPlacement[RECT_PLACEMENT_VARS].a = THIRTY_ALPHA
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

PROC DRAW_HEADER(LEADERBOARD_PLACEMENT_TOOLS& Placement)

	//25% black panel added here behind the grad sprite

	Placement.SpritePlacement[SPRITE_PLACEMENT_GRADIENT_HEADER].x = GRAD_X
	Placement.SpritePlacement[SPRITE_PLACEMENT_GRADIENT_HEADER].y = GRAD_Y
	Placement.SpritePlacement[SPRITE_PLACEMENT_GRADIENT_HEADER].w = GRAD_W
	Placement.SpritePlacement[SPRITE_PLACEMENT_GRADIENT_HEADER].h = GRAD_H
	Placement.SpritePlacement[SPRITE_PLACEMENT_GRADIENT_HEADER].a = 128//255
	DRAW_2D_SPRITE("CommonMenu", "Header_Gradient_Script", Placement.SpritePlacement[SPRITE_PLACEMENT_GRADIENT_HEADER], FALSE, HIGHLIGHT_OPTION_NORMAL, GFX_ORDER_AFTER_FADE_PRIORITY_LOW)
	
	Placement.RectPlacement[RECT_HEADER].x = GRAD_X
	Placement.RectPlacement[RECT_HEADER].y = GRAD_y
	Placement.RectPlacement[RECT_HEADER].w = GRAD_W
	Placement.RectPlacement[RECT_HEADER].h = GRAD_H 	
	Placement.RectPlacement[RECT_HEADER].r = 0 	
	Placement.RectPlacement[RECT_HEADER].g = 0
	Placement.RectPlacement[RECT_HEADER].b = 0
	Placement.RectPlacement[RECT_HEADER].a = CEIL(255*0.55)
	DRAW_RECTANGLE(Placement.RectPlacement[RECT_HEADER])
	
ENDPROC

PROC DRAW_WHITE_TOP_RECT(LEADERBOARD_PLACEMENT_TOOLS& Placement)
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_FADE)
	SET_RECT_TO_THIS_HUD_COLOUR(Placement.RectPlacement[RECT_PLACEMENT_WHITE_TITLES], HUD_COLOUR_FREEMODE)
	Placement.RectPlacement[RECT_PLACEMENT_WHITE_TITLES].x = COLUMN_LONG__RECT_X
	Placement.RectPlacement[RECT_PLACEMENT_WHITE_TITLES].y = COLUMN_HEADING_RECT_Y 
	Placement.RectPlacement[RECT_PLACEMENT_WHITE_TITLES].w = COLUMN_LONG__RECT_W
	Placement.RectPlacement[RECT_PLACEMENT_WHITE_TITLES].h = RECT_HEIGHT_CONSTANT
	Placement.RectPlacement[RECT_PLACEMENT_WHITE_TITLES].a = 255	

	DRAW_RECTANGLE(Placement.RectPlacement[RECT_PLACEMENT_WHITE_TITLES])
ENDPROC

//// The thin blue line above the main white heading rect
//PROC DRAW_LONG_CAP(LEADERBOARD_PLACEMENT_TOOLS& Placement)
//
//	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_FADE)
//	SET_RECT_TO_THIS_HUD_COLOUR(Placement.RectPlacement[RECT_PLACEMENT_LONG_CAP], GET_RECT_SELECTION_COLOUR(eSubMode, 0,0))
//
//	Placement.RectPlacement[RECT_PLACEMENT_LONG_CAP].x = COLUMN_CAP1_X
//	Placement.RectPlacement[RECT_PLACEMENT_LONG_CAP].y = COLUMN_CAPS_Y_ALL 
//	Placement.RectPlacement[RECT_PLACEMENT_LONG_CAP].w = COLUMN_CAP1_W 
//	Placement.RectPlacement[RECT_PLACEMENT_LONG_CAP].h = COLUMN_CAP_H_ALL
//	Placement.RectPlacement[RECT_PLACEMENT_LONG_CAP].a = EIGHTY_ALPHA
//
//	DRAW_RECTANGLE(Placement.RectPlacement[RECT_PLACEMENT_LONG_CAP])
//ENDPROC


CONST_FLOAT VOICE_H 0.04077776
CONST_FLOAT VOICE_W 0.0225625
CONST_FLOAT VOICE_X 0.3775
CONST_FLOAT VOICE_Y 0.2299

///    1px on x-axis = 0.00078125 
///    1px on y-axis = 0.00138888

PROC DRAW_VOICE_CHAT_ICONS(LEADERBOARD_PLACEMENT_TOOLS& Placement, PLAYER_INDEX playerId, INT iRow)
	Placement.SpritePlacement[SPRITE_PLACEMENT_VOICE].x = VOICE_X  
	Placement.SpritePlacement[SPRITE_PLACEMENT_VOICE].y = VOICE_Y+(TO_FLOAT(iRow) * RECT_Y_OFFSET) 
	Placement.SpritePlacement[SPRITE_PLACEMENT_VOICE].w = VOICE_W
	Placement.SpritePlacement[SPRITE_PLACEMENT_VOICE].h = VOICE_H 
	Placement.SpritePlacement[SPRITE_PLACEMENT_VOICE].a = 255
	Placement.SpritePlacement[SPRITE_PLACEMENT_VOICE].r = 255
	Placement.SpritePlacement[SPRITE_PLACEMENT_VOICE].g = 255
	Placement.SpritePlacement[SPRITE_PLACEMENT_VOICE].b = 255
	
	IF IS_NET_PLAYER_OK(playerId, FALSE)
		IF NETWORK_PLAYER_HAS_HEADSET(playerId)
			IF NETWORK_IS_PLAYER_MUTED_BY_ME(playerId)
				DRAW_2D_SPRITE("MPLeaderboard", "Leaderboard_Audio_Mute", Placement.SpritePlacement[SPRITE_PLACEMENT_VOICE], FALSE, HIGHLIGHT_OPTION_NORMAL, GFX_ORDER_AFTER_FADE)
				SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_FADE)
			ELSE
				IF NETWORK_IS_PLAYER_TALKING(playerId)
					DRAW_2D_SPRITE("MPLeaderboard", "Leaderboard_Audio_3", Placement.SpritePlacement[SPRITE_PLACEMENT_VOICE], FALSE, HIGHLIGHT_OPTION_NORMAL, GFX_ORDER_AFTER_FADE)
					SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_FADE)
				ELSE
					DRAW_2D_SPRITE("MPLeaderboard", "Leaderboard_Audio_Inactive", Placement.SpritePlacement[SPRITE_PLACEMENT_VOICE], FALSE, HIGHLIGHT_OPTION_NORMAL, GFX_ORDER_AFTER_FADE)
					SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_FADE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC DRAW_VOICE_CHAT_ICONS_OR_RANK_BADGE(LEADERBOARD_PLACEMENT_TOOLS& Placement, PLAYER_INDEX playerId, INT iRow, INT iBadgeRank)

	Placement.SpritePlacement[SPRITE_PLACEMENT_VOICE].x = VOICE_X  
	Placement.SpritePlacement[SPRITE_PLACEMENT_VOICE].y = VOICE_Y+(TO_FLOAT(iRow) * RECT_Y_OFFSET) 
	Placement.SpritePlacement[SPRITE_PLACEMENT_VOICE].w = VOICE_W
	Placement.SpritePlacement[SPRITE_PLACEMENT_VOICE].h = VOICE_H 
	Placement.SpritePlacement[SPRITE_PLACEMENT_VOICE].a = 255
	Placement.SpritePlacement[SPRITE_PLACEMENT_VOICE].r = 255
	Placement.SpritePlacement[SPRITE_PLACEMENT_VOICE].g = 255
	Placement.SpritePlacement[SPRITE_PLACEMENT_VOICE].b = 255
	
	IF IS_NET_PLAYER_OK(playerId, FALSE)
	AND NETWORK_PLAYER_HAS_HEADSET(playerId)
	AND NOT NETWORK_IS_PLAYER_MUTED_BY_ME(playerId)
	AND NETWORK_IS_PLAYER_TALKING(playerId)
		DRAW_2D_SPRITE("MPLeaderboard", "Leaderboard_Audio_3", Placement.SpritePlacement[SPRITE_PLACEMENT_VOICE], FALSE, HIGHLIGHT_OPTION_NORMAL, GFX_ORDER_AFTER_FADE)
		SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_FADE)
	ELSE
		// 1899229
		IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
			DRAW_RANK_BADGE_FONT_LEADERBOARD(iBadgeRank, RANK_BADGE_X, RANK_BADGE_Y+(TO_FLOAT(iRow) * RECT_Y_OFFSET ), RANKDISPLAYTYPE_FULL_WITH_BOX)
		ENDIF
	ENDIF
ENDPROC

CONST_FLOAT LBD_TEXT_X 			0.278
CONST_FLOAT LBD_TEXT_Y 			0.169

FUNC STRING GET_CASH_OR_KILL_STRING(BOOL bIsCashMatch)
	IF bIsCashMatch
		RETURN "HUD_CASH"
	ELSE
		RETURN "NUMBER"
	ENDIF
	
	RETURN ""
ENDFUNC

//PROC COLOUR_LINE_OF_TEXT(LEADERBOARD_PLACEMENT_TOOLS& Placement, PLAYER_INDEX playerId, INT iParticipant)//, BOOL bDrawBlack = FALSE)
PROC COLOUR_LINE_OF_TEXT(LEADERBOARD_PLACEMENT_TOOLS& Placement, INT iParticipant)
//	IF g_i_ActualLeaderboardPlayerSelected = NATIVE_TO_INT(playerId)
////		SET_TEXT_BLACK(Placement.aStyle.TS_LBD)
//		SET_TEXT_WHITE(Placement.aStyle.TS_LBD)
//	ELSE
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
			SET_TEXT_WHITE(Placement.aStyle.TS_LBD)
//			IF bDrawBlack
//				SET_TEXT_BLACK(Placement.aStyle.TS_LBD)
//			ELSE
//				SET_TEXT_WHITE(Placement.aStyle.TS_LBD)
//			ENDIF
		ELSE
//			SET_TEXT_GREY(Placement.aStyle.TS_LBD)
//			SET_TEXT_DARK_GREY(Placement.aStyle.TS_LBD)
			SET_TEXT_COLOUR_WITH_HUD_COLOUR(Placement.aStyle.TS_LBD, GET_INACTIVE_COLOUR())
		ENDIF
//	ENDIF
ENDPROC

//FUNC HUD_COLOURS GET_PLAYER_NAME_COLOUR(PLAYER_INDEX playerId, INT iParticipant)
FUNC HUD_COLOURS GET_PLAYER_NAME_COLOUR(INT iParticipant)
//	IF g_i_ActualLeaderboardPlayerSelected = NATIVE_TO_INT(playerId)
//		RETURN HUD_COLOUR_BLACK
//	ELSE
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
			RETURN HUD_COLOUR_WHITE
		ELSE
			RETURN GET_INACTIVE_COLOUR()
		ENDIF
//	ENDIF
	
	RETURN HUD_COLOUR_WHITE
ENDFUNC

FUNC BOOL SHOULD_HIDE_POSITION_NUMBER(LBD_VARS& lbdVars)
	
	IF lbdVars.bHideMyRow 
	
		RETURN TRUE
	ENDIF
	
	IF lbdVars.bHidePos
	AND NOT g_FinalRoundsLbd
	
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_PLAYING_OR_PLANNING_HEIST(PLAYER_ID())
	
		RETURN TRUE
	ENDIF
	
		IF IS_LOADED_MISSION_LOW_RIDER_FLOW_MISSION()
		
			RETURN TRUE
		ENDIF
	 //#IF FEATURE_LOWRIDER_CONTENT
	
	RETURN FALSE
ENDFUNC

PROC DRAW_POSITIONS(LBD_VARS& lbdVars, LEADERBOARD_PLACEMENT_TOOLS& Placement, INT iParticipant, INT iRank, INT iRow, BOOL bTeamMode)

	IF NOT SHOULD_HIDE_POSITION_NUMBER(lbdVars)
	
		IF iRank <> 0
		AND NOT bTeamMode
//			Placement.TextPlacement[TEXT_PLACEMENT_POSITION] = GET_NAME_SLOT_TEXT_PLACEMENT_LB(Placement.TextPlacement[TEXT_PLACEMENT_POSITION], iRow)
			
			Placement.TextPlacement[TEXT_PLACEMENT_POSITION].x = POSITION_POS_X 
			Placement.TextPlacement[TEXT_PLACEMENT_POSITION].y = VARIABLE_BASE_Y+(TO_FLOAT(iRow) * RECT_Y_OFFSET)
			
			SET_TEXT_STYLE(Placement.aStyle.TS_LBD)
			COLOUR_LINE_OF_TEXT(Placement, iParticipant)
			
			#IF IS_DEBUG_BUILD
				IF g_b_SpamRankOutput
					PRINTNL()
					PRINTLN("[SPAMRANK] BEFORE ")
					PRINTLN("[SPAMRANK] 1407518 DRAW_POSITIONS iParticipant = ", iParticipant)
					PRINTNL()
					PRINTLN("[SPAMRANK] 1407518 DRAW_POSITIONS iRank        = ", iRank)
					PRINTNL()
					PRINTLN("[SPAMRANK] 1407518 DRAW_POSITIONS iRow         = ", iRow)
					PRINTNL()
				ENDIF
			#ENDIF
			
			Placement.RectPlacement[RECT_PLACEMENT_BLACK_POS].x = RED_X
			Placement.RectPlacement[RECT_PLACEMENT_BLACK_POS].y = COLUMN_RECT_Y + (TO_FLOAT(iRow) * RECT_Y_OFFSET)
			Placement.RectPlacement[RECT_PLACEMENT_BLACK_POS].w = RED_W
			Placement.RectPlacement[RECT_PLACEMENT_BLACK_POS].h = RECT_HEIGHT_CONSTANT
			Placement.RectPlacement[RECT_PLACEMENT_BLACK_POS].a = FIFTY_ALPHA
			DRAW_RECTANGLE(Placement.RectPlacement[RECT_PLACEMENT_BLACK_POS])
			
			DRAW_TEXT_WITH_NUMBER(Placement.TextPlacement[TEXT_PLACEMENT_POSITION], Placement.aStyle.TS_LBD, "NUMBER", iRank, FONT_CENTRE)
			
			#IF IS_DEBUG_BUILD
				IF g_b_SpamRankOutput
					PRINTNL()
					PRINTLN("[SPAMRANK] AFTER ")
					PRINTLN("[SPAMRANK] 1407518 DRAW_POSITIONS iParticipant = ", iParticipant)
					PRINTNL()
					PRINTLN("[SPAMRANK] 1407518 DRAW_POSITIONS iRank        = ", iRank)
					PRINTNL()
					PRINTLN("[SPAMRANK] 1407518 DRAW_POSITIONS iRow         = ", iRow)
					PRINTNL()
				ENDIF
			#ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC DRAW_PLAYER_NAMES(LEADERBOARD_PLACEMENT_TOOLS& Placement, LBD_SUB_MODE eSubMode, TEXT_LABEL_63& participantNames[], INT iRow, INT iParticipant, BOOL bSpectator = FALSE)

	FLOAT xPos	
	SWITCH eSubMode
		CASE SUB_DM_TEAM
		CASE SUB_RACE_RALLY
		
//			xPos = TEAM_BIG_PLAYER_NAMES_X
		BREAK
//		DEFAULT xPos = BIG_PLAYER_NAMES_X 
		BREAK
	ENDSWITCH	
	
	xPos = BIG_PLAYER_NAMES_X 
	
	SET_STANDARD_LBD_PLYR_NAME_TEXT(Placement.aStyle.TS_LBDPLYR)
	SET_TEXT_STYLE(Placement.aStyle.TS_LBDPLYR)
	Placement.TextPlacement[TEXT_PLACEMENT_PLAYER_NAMES].x = xPos
	Placement.TextPlacement[TEXT_PLACEMENT_PLAYER_NAMES].y = VARIABLE_BASE_Y_NAMES+(TO_FLOAT(iRow) * RECT_Y_OFFSET)
	
	bSpectator = bSpectator
	
	DRAW_TEXT_WITH_PLAYER_NAME(Placement.TextPlacement[TEXT_PLACEMENT_PLAYER_NAMES], Placement.aStyle.TS_LBDPLYR, participantNames[iParticipant], "STRING",	GET_PLAYER_NAME_COLOUR(iParticipant))
	
	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("2136769")
		PRINTLN("DRAW_PLAYER_NAMES, Name = ", participantNames[iParticipant], " iParticipant = ", iParticipant, " iRow = ", iRow)
	ENDIF
	#ENDIF
ENDPROC

PROC DRAW_SPEC_NAMES(LEADERBOARD_PLACEMENT_TOOLS& Placement, LBD_SUB_MODE eSubMode, STRING sNames, INT iRow, INT iParticipant, BOOL bSpectator = FALSE)

	FLOAT xPos	
	SWITCH eSubMode
		CASE SUB_DM_TEAM
		CASE SUB_RACE_RALLY
		
//			xPos = TEAM_BIG_PLAYER_NAMES_X
		BREAK
//		DEFAULT xPos = BIG_PLAYER_NAMES_X 
		BREAK
	ENDSWITCH	
	
	xPos = BIG_PLAYER_NAMES_X 
	
	SET_STANDARD_LBD_PLYR_NAME_TEXT(Placement.aStyle.TS_LBDPLYR)
	SET_TEXT_STYLE(Placement.aStyle.TS_LBDPLYR)
	Placement.TextPlacement[TEXT_PLACEMENT_PLAYER_NAMES].x = xPos
	Placement.TextPlacement[TEXT_PLACEMENT_PLAYER_NAMES].y = VARIABLE_BASE_Y_NAMES+(TO_FLOAT(iRow) * RECT_Y_OFFSET)
	
	bSpectator = bSpectator
	
	DRAW_TEXT_WITH_PLAYER_NAME(Placement.TextPlacement[TEXT_PLACEMENT_PLAYER_NAMES], Placement.aStyle.TS_LBDPLYR, sNames, "STRING",	GET_PLAYER_NAME_COLOUR(iParticipant))
	
ENDPROC

FUNC INT GET_SPECTATOR_ROW(BOOL bIsTeamMode, INT iNumLbdPlayers, INT iNumTeams, INT iSpectatorNumber)

	INT iSpectatorRow
	
	IF bIsTeamMode 
		// Skipping the team name and team score bars
		iSpectatorRow = (iNumLbdPlayers + iNumTeams + iNumTeams + iSpectatorNumber)
	ELSE
		iSpectatorRow = (iNumLbdPlayers + iSpectatorNumber)
	ENDIF
	
	iSpectatorRow = (iSpectatorRow - g_i_LeaderboardShift)
	
	PRINTLN("[SPEC_ROW] iNumLbdPlayers            = ", iNumLbdPlayers)
	PRINTLN("[SPEC_ROW] iNumTeams                 = ", iNumTeams)
	PRINTLN("[SPEC_ROW] iSpectatorNumber          = ", iSpectatorNumber)
	PRINTLN("[SPEC_ROW] g_i_LeaderboardShift      = ", g_i_LeaderboardShift)
	PRINTLN("[SPEC_ROW] iSpectatorRow             = ", iSpectatorRow)
	
	RETURN iSpectatorRow 
ENDFUNC

PROC DRAW_SPECTATOR_ROW(LBD_VARS& lbdVars, LEADERBOARD_PLACEMENT_TOOLS& Placement, LBD_SUB_MODE eSubMode, PLAYER_INDEX playerID, STRING sName, INT iBadgeRank, INT iRow, INT iParticipant)

	IF IS_PLAYER_DEV_SPECTATOR(playerID) // 1840918
	OR NOT IS_IT_OK_TO_DRAW_LEADERBOARD(FALSE)
	OR g_bCelebrationScreenIsActive
	OR SHOULD_DRAW_SCROLLBAR_COUNTER(lbdVars) // No room for spectators on scrolling leaderboards
		EXIT
	ENDIF

	STRING strHeadshotTxd
	PEDHEADSHOT_ID pedHeadshot
	pedHeadshot = Get_HeadshotID_For_Player(PlayerId)
	IF pedHeadshot != NULL
		strHeadshotTxd = GET_PEDHEADSHOT_TXD_STRING(pedHeadshot)
	ENDIF
	
	REQUEST_STREAMED_TEXTURE_DICT("mphud")
	
	IF iRow >= 0
	AND iRow < MAX_BEFORE_SCROLL -1
	
		DRAW_WEE_POSITION_RECTS(Placement, iRow, strHeadshotTxd)
		DRAW_RECT_PLAYER_BARS(Placement, eSubMode, TRUE, INVALID_PLAYER_INDEX(), iRow)
		DRAW_VOICE_CHAT_ICONS_OR_RANK_BADGE(Placement, playerId, iRow, iBadgeRank)
		DRAW_SPEC_NAMES(Placement, eSubMode, sName, iRow, iParticipant, TRUE)
		
		Placement.SpritePlacement[SPRITE_PLACEMENT_EYE_POS].x = EYE_X
		Placement.SpritePlacement[SPRITE_PLACEMENT_EYE_POS].y = EYE_Y_OFFSET+(TO_FLOAT(iRow) * RECT_Y_OFFSET)
		Placement.SpritePlacement[SPRITE_PLACEMENT_EYE_POS].w = EYE_W
		Placement.SpritePlacement[SPRITE_PLACEMENT_EYE_POS].h = EYE_H
		Placement.SpritePlacement[SPRITE_PLACEMENT_EYE_POS].r = 255
		Placement.SpritePlacement[SPRITE_PLACEMENT_EYE_POS].g = 255
		Placement.SpritePlacement[SPRITE_PLACEMENT_EYE_POS].b = 255
		Placement.SpritePlacement[SPRITE_PLACEMENT_EYE_POS].a = 255
		
		IF HAS_STREAMED_TEXTURE_DICT_LOADED("mphud")
			DRAW_2D_SPRITE("mphud", "spectating", Placement.SpritePlacement[SPRITE_PLACEMENT_EYE_POS], FALSE, HIGHLIGHT_OPTION_NORMAL, GFX_ORDER_AFTER_FADE)
		ENDIF
	ENDIF
ENDPROC

CONST_INT MAX_NUM_LBD_COLUMNS 6

FUNC BOOL IS_ROUNDS_MISSION_LBD(LBD_SUB_MODE eSubMode)
//	SWITCH eSubMode
//		CASE SUB_MISSIONS_LTS
//		CASE SUB_MISSIONS_CTF
//		CASE SUB_MISSIONS_NO_HUD
//		CASE SUB_MISSIONS_VERSUS
//		CASE SUB_FINAL_ROUNDS
//		
//			IF (g_sTransitionSessionData.sMissionRoundData.iNumberOfRounds <> 0)
//			
//				RETURN TRUE
//			ENDIF
//		BREAK
//	ENDSWITCH

	RETURN (g_sTransitionSessionData.sMissionRoundData.iNumberOfRounds > 0)
	
	UNUSED_PARAMETER(eSubMode)
	
//	PRINTLN("[CS_ROUND] IS_ROUNDS_MISSION_LBD, FALSE iNumberOfRounds = ", g_sTransitionSessionData.sMissionRoundData.iNumberOfRounds)
//	
//	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYLIST_ROUNDS_MISSION(LBD_SUB_MODE eSubMode)
	IF IS_ROUNDS_MISSION_LBD(eSubMode)
		IF IS_PLAYER_ON_A_PLAYLIST(PLAYER_ID())
			IF NOT SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD()
		
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

// The special name for the first column see 2207640
FUNC STRING CUSTOM_COLUMN_TITLE(STRING strTextLabel)
	RETURN strTextLabel
ENDFUNC

FUNC BOOL bAGG_RACE(LBD_SUB_MODE eSubMode)
	IF eSubMode = SUB_RACE_AGG_BEST
	OR eSubMode = SUB_RACE_AGG_TIME
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL bAGG_RACE_BEST(LBD_SUB_MODE eSubMode)
	RETURN eSubMode = SUB_RACE_AGG_BEST
ENDFUNC

FUNC BOOL SHOW_ASSISTS_LEADERBOARD()
	RETURN IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, cSHOW_ASSISTS_ON_LEADERBOARD)
ENDFUNC

FUNC BOOL REPLACE_SCORE_WITH_KILLS()
	RETURN IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyTwo, ciOptionsBS22_DM_SCORE_TO_KILLS_ON_LDB)
ENDFUNC

FUNC BOOL IS_DOUBLE_DOWN_LEADERBOARD()
	RETURN g_FMMC_STRUCT.iLDB_ShowEndTimeForTeam != 0
ENDFUNC

FUNC BOOL SHOW_SCORE_COLUMN_ON_LEADERBOARD()

	IF g_FinalRoundsLbd
		IF IS_DOUBLE_DOWN_LEADERBOARD()
			RETURN FALSE
		ENDIF
	ENDIF

	RETURN IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciSHOW_SCORE_COLUMN_ON_LEADERBOARD)
ENDFUNC

FUNC BOOL bIS_TURF_WARS()

		RETURN IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTen, ciENABLE_TURF_WAR_CLAIMABLE_PROPS)

	RETURN FALSE
ENDFUNC

FUNC BOOL CUSTOM_LBD_REMIX()
	RETURN IS_THIS_ROCKSTAR_MISSION_NEW_VS_RUNNING_BACK_REMIX(g_FMMC_STRUCT.iAdversaryModeType)
ENDFUNC

FUNC BOOL bIS_JUGGERNAUT()
	RETURN IS_THIS_ROCKSTAR_MISSION_NEW_VS_JUGGERNAUT(g_FMMC_STRUCT.iAdversaryModeType)
ENDFUNC

FUNC BOOL HIDE_TEAM_SCORE(LBD_SUB_MODE eSubMode)

	IF bIS_TURF_WARS()
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_TURF_WAR_SHOW_SCORE_ON_LEADERBOARD)
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_HOSTILE_TAKEOVER(g_FMMC_STRUCT.iAdversaryModeType)
		RETURN TRUE
	ENDIF
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_SHOWDOWN(g_FMMC_STRUCT.iAdversaryModeType)
		RETURN TRUE
	ENDIF
	
	IF eSubMode = SUB_RACE_TARGET
		RETURN TRUE
	ENDIF
	
	IF CONTENT_IS_USING_ARENA()
	
		IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_BOMB_FOOTBALL(g_FMMC_STRUCT.iAdversaryModeType)
 			RETURN FALSE
		ENDIF
		
		IF eSubMode = SUB_KOTH
		 	RETURN FALSE
		ENDIF
	
		RETURN TRUE

	ENDIF

	RETURN FALSE
ENDFUNC

// 2371262 need one with score, RP , Cash and vote
FUNC BOOL HIDE_KILLS_DEATHS_COLUMN_LBD()

	RETURN IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciHIDE_LEADERBOARD_KILLS_AND_DEATHS)
ENDFUNC

// 2640267 deaths, cash, RP, votes
FUNC BOOL HIDE_KILLS_SHOW_DEATHS_COLUMN_LBD()
	RETURN IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSix, cHIDE_KILLS_ON_LEADERBOARD)
ENDFUNC

// 2464135 Hunting Pack Lbd Columns, Deaths, RP, Cash. Next/Rnd
FUNC BOOL IS_NO_SCORE_NO_KILL_LBD_SET()
	
	RETURN IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciLBD_NO_SCORE_OR_KILLS)
ENDFUNC

FUNC BOOL SHOW_SLIPSTREAM_COLUMN()
	RETURN IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTen, ciLBD_SHOW_SLIPSTREAM_TIME)
ENDFUNC

FUNC BOOL SHOW_TIME_COLLECTED_INSTEAD_OF_SCORE()
	RETURN IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwenty, ciOptionsBS20_SHOW_TIME_INSTEAD_OF_SCORE)
ENDFUNC

FUNC BOOL SHOW_TIME_COLUMN()
	
	IF g_FinalRoundsLbd
		IF IS_DOUBLE_DOWN_LEADERBOARD()
			RETURN FALSE
		ENDIF
	ENDIF

	RETURN IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciLBD_SHOW_TIME_COLUMN)
ENDFUNC

FUNC BOOL IS_THREE_COLUMN_LBD()	
	IF HIDE_KILLS_DEATHS_COLUMN_LBD()
	AND IS_NO_SCORE_NO_KILL_LBD_SET()
	
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL DRAW_CUSTOM_TITLE()

	IF NOT IS_STRING_NULL_OR_EMPTY(CUSTOM_COLUMN_TITLE(g_FMMC_STRUCT.tl63PointsTitle))
	
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYLIST_LBD(LBD_SUB_MODE eSubMode)
	RETURN (eSubMode = SUB_PLAYLIST)
ENDFUNC

FUNC BOOL IS_ARENA_LBD(LBD_SUB_MODE eSubMode)
	SWITCH eSubMode
		CASE SUB_ARENA_CARNAGE	
		CASE SUB_ARENA_ARCADE
		CASE SUB_ARENA_CTF
		CASE SUB_ARENA_DDERBY
		CASE SUB_ARENA_DETONATION
		CASE SUB_ARENA_FIRE
		CASE SUB_ARENA_GAMESMASTER
		CASE SUB_ARENA_MONSTER
		CASE SUB_ARENA_PASSBOMB
		CASE SUB_ARENA_TAG_TEAM
		
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_NUM_LBD_COLUMNS(LBD_SUB_MODE eSubMode)
	
	IF NOT IS_PLAYLIST_LBD(eSubMode)
	AND eSubMode != SUB_FINAL_ROUNDS 
	AND !IS_ARENA_LBD(eSubMode)
	
		IF bIS_JUGGERNAUT()
		
			RETURN MAX_NUM_LBD_COLUMNS
		ENDIF
	
		IF bIS_TURF_WARS()
		
			RETURN MAX_NUM_LBD_COLUMNS
		ENDIF
		
		IF CUSTOM_LBD_REMIX()
			RETURN MAX_NUM_LBD_COLUMNS
		ENDIF
	
		IF SHOW_SLIPSTREAM_COLUMN()
		
			RETURN 5
		ENDIF
	
		IF SHOW_ASSISTS_LEADERBOARD()
		
			RETURN 5
		ENDIF
	
		IF HIDE_KILLS_SHOW_DEATHS_COLUMN_LBD()
		
			RETURN 4
		ENDIF
	
		IF IS_THREE_COLUMN_LBD()
		
			RETURN 3
		ENDIF
	
		IF HIDE_KILLS_DEATHS_COLUMN_LBD()
			RETURN 4
		ENDIF
		
		IF IS_NO_SCORE_NO_KILL_LBD_SET()  
		
			RETURN 4
		ENDIF
	ENDIF

	SWITCH eSubMode	
	
		CASE SUB_DM_FFA
		CASE SUB_DM_TEAM
		CASE SUB_KOTH
		CASE SUB_DM_UPDATE
			RETURN MAX_NUM_LBD_COLUMNS
			
		CASE SUB_ARENA_CARNAGE	
		CASE SUB_ARENA_ARCADE
		CASE SUB_ARENA_CTF
		CASE SUB_ARENA_DDERBY
		CASE SUB_ARENA_DETONATION
		CASE SUB_ARENA_FIRE
		CASE SUB_ARENA_GAMESMASTER
		CASE SUB_ARENA_MONSTER
		CASE SUB_ARENA_PASSBOMB
		CASE SUB_ARENA_TAG_TEAM
		
			IF IS_ROUNDS_MISSION_LBD(eSubMode)
			
				RETURN MAX_NUM_LBD_COLUMNS
			ENDIF
		
			RETURN MAX_NUM_LBD_COLUMNS -1
		
		CASE SUB_RACE_FOOT
			IF g_b_ShowBestLap
				RETURN 5
			ELSE
				RETURN 4
			ENDIF
		BREAK
		
		CASE SUB_RACE_AGG_BEST			
		CASE SUB_RACE_AGG_TIME			
		CASE SUB_RACE_TARGET			
			RETURN MAX_NUM_LBD_COLUMNS
		
		CASE SUB_RACE_NORMAL
		CASE SUB_RACE_STUNT
		CASE SUB_RACE_GTA
		CASE SUB_RACE_RALLY
		CASE SUB_RACE_BASEJUMP
			IF g_b_ShowBestLap
				RETURN MAX_NUM_LBD_COLUMNS
			ELSE
				RETURN 5
			ENDIF
		BREAK
		CASE SUB_SHOOT_RANGE
		CASE SUB_MISSIONS_NORMAL
		CASE SUB_MISSIONS_TIMED
			RETURN 4
		CASE SUB_HORDE
		CASE SUB_KART
			RETURN 3
		CASE SUB_MISSIONS_HEIST
			RETURN (4)
		CASE SUB_MISSIONS_NO_HUD
			RETURN (5)
		
		CASE SUB_MISSIONS_LTS
			RETURN (5)
		
		CASE SUB_FINAL_ROUNDS
		
			IF bIS_JUGGERNAUT()
			
				RETURN MAX_NUM_LBD_COLUMNS
			ENDIF
		
			IF SHOW_SCORE_COLUMN_ON_LEADERBOARD()
//				IF HIDE_KILLS_DEATHS_COLUMN_LBD()
//					RETURN 4
//				ELSE
					RETURN MAX_NUM_LBD_COLUMNS
//				ENDIF
			ELSE
				RETURN (5)
			ENDIF
		BREAK
		
		CASE SUB_MISSIONS_VERSUS	RETURN MAX_NUM_LBD_COLUMNS

		CASE SUB_MISSIONS_CTF
			IF SHOW_SCORE_COLUMN_ON_LEADERBOARD()
				RETURN MAX_NUM_LBD_COLUMNS
			ELSE
				RETURN (5)
			ENDIF
		BREAK
		
		CASE SUB_SC_PRE_RACE_SUB_TITLE
		CASE SUB_SC_PRE_RACE_CONTENT
		
			RETURN MAX_NUM_LBD_COLUMNS

		CASE SUB_FREEMODE			
		CASE SUB_ARCADE_CNC
			RETURN 2
			
		CASE SUB_PLAYLIST			RETURN CLAMP_INT(g_sCurrentPlayListDetails.iLength+2, 3, MAX_NUM_LBD_COLUMNS)
	ENDSWITCH
	
	RETURN MAX_NUM_LBD_COLUMNS
ENDFUNC

CONST_FLOAT SIX_COLUMN_1  	0.427
CONST_FLOAT SIX_COLUMN_2  	0.517
CONST_FLOAT SIX_COLUMN_3  	0.600
CONST_FLOAT SIX_COLUMN_4  	0.668
CONST_FLOAT SIX_COLUMN_5  	0.735
CONST_FLOAT SIX_COLUMN_6  	0.801

CONST_FLOAT FIVE_COLUMN_1 	0.427
CONST_FLOAT FIVE_COLUMN_2 	0.523
CONST_FLOAT FIVE_COLUMN_3 	0.617
CONST_FLOAT FIVE_COLUMN_4 	0.704
CONST_FLOAT FIVE_COLUMN_5 	0.801

CONST_FLOAT FOUR_COLUMN_1 	0.427
CONST_FLOAT FOUR_COLUMN_2 	0.539
CONST_FLOAT FOUR_COLUMN_3 	0.659
CONST_FLOAT FOUR_COLUMN_4 	0.801

CONST_FLOAT THREE_COLUMN_1 	0.427
CONST_FLOAT THREE_COLUMN_2 	0.602
CONST_FLOAT THREE_COLUMN_3 	0.801

CONST_FLOAT TWO_COLUMN_1 	0.427
CONST_FLOAT TWO_COLUMN_2 	0.801

CONST_FLOAT ONE_COLUMN_1 	0.801

CONST_FLOAT SIX_COLUMN_1_PLAYLIST 		0.427
CONST_FLOAT SIX_COLUMN_2_PLAYLIST 		0.494
CONST_FLOAT SIX_COLUMN_3_PLAYLIST 		0.563
CONST_FLOAT SIX_COLUMN_4_PLAYLIST 		0.634
CONST_FLOAT SIX_COLUMN_5_PLAYLIST  		0.709
CONST_FLOAT SIX_COLUMN_6_PLAYLIST  		0.798

CONST_FLOAT FIVE_COLUMN_1_PLAYLIST  	0.436
CONST_FLOAT FIVE_COLUMN_2_PLAYLIST  	0.535
CONST_FLOAT FIVE_COLUMN_3_PLAYLIST  	0.629
CONST_FLOAT FIVE_COLUMN_4_PLAYLIST  	0.704
CONST_FLOAT FIVE_COLUMN_5_PLAYLIST  	0.798

CONST_FLOAT FOUR_COLUMN_1_PLAYLIST  	0.436
CONST_FLOAT FOUR_COLUMN_2_PLAYLIST  	0.547
CONST_FLOAT FOUR_COLUMN_3_PLAYLIST  	0.666
CONST_FLOAT FOUR_COLUMN_4_PLAYLIST  	0.798

CONST_FLOAT THREE_COLUMN_1_PLAYLIST 	0.450
CONST_FLOAT THREE_COLUMN_2_PLAYLIST 	0.624
CONST_FLOAT THREE_COLUMN_3_PLAYLIST 	0.798

PROC SET_LBD_SELECTION_TO_PLAYER_POS(LBD_VARS &lbdVars, PLAYER_INDEX playerID, INT &iPlayerSelection, INT iCurrentRow, INT iNumPlayers)

	IF NOT lbdVars.bPlayerRowGrabbed
		IF playerID = PLAYER_ID()
			INT iTeam = GET_PLAYER_TEAM(PLAYER_ID())
			PRINTLN("BEFORE, SET_LBD_SELECTION_TO_PLAYER_POS, PLAYER_ID, iPlayerSelection = ", iPlayerSelection, " iCurrentRow = ", iCurrentRow, " iNumPlayers = ", iNumPlayers, " iTeam = ", iTeam)
			// 2030757
			iPlayerSelection = iCurrentRow

			lbdVars.bPlayerRowGrabbed = TRUE
			
			iTeam = (iTeam + 1)
			INT iTeamRowsPassed = ((iTeam * 2) - 1) // How many team rows have been skipped?
			
			lbdVars.iTeamRowsPassed = iTeamRowsPassed
			PRINTLN("AFTER, SET_LBD_SELECTION_TO_PLAYER_POS, PLAYER_ID, iPlayerSelection = ", iPlayerSelection, " iCurrentRow = ", iCurrentRow, " iNumPlayers = ", iNumPlayers, " iTeamRowsPassed = ", iTeamRowsPassed)
		ELSE
			IF iCurrentRow >= iNumPlayers - 1
				PRINTLN("BEFORE, SET_LBD_SELECTION_TO_PLAYER_POS , iPlayerSelection = ", iPlayerSelection, " iCurrentRow = ", iCurrentRow, " iNumPlayers = ", iNumPlayers)
				iPlayerSelection = 0
				lbdVars.bPlayerRowGrabbed = TRUE
				PRINTLN("AFTER, SET_LBD_SELECTION_TO_PLAYER_POS, iPlayerSelection = ", iPlayerSelection, " iCurrentRow = ", iCurrentRow, " iNumPlayers = ", iNumPlayers)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC


FUNC FLOAT GET_COLUMN_POS(INT iNumColumns, INT iColumnPriorityOrder, BOOL bPlaylist = FALSE)

	SWITCH iNumColumns
	 
		CASE MAX_NUM_LBD_COLUMNS
			SWITCH iColumnPriorityOrder
				CASE 1 IF bPlaylist RETURN SIX_COLUMN_1_PLAYLIST ELSE RETURN SIX_COLUMN_1 ENDIF BREAK
				CASE 2 IF bPlaylist RETURN SIX_COLUMN_2_PLAYLIST ELSE RETURN SIX_COLUMN_2 ENDIF BREAK
				CASE 3 IF bPlaylist RETURN SIX_COLUMN_3_PLAYLIST ELSE RETURN SIX_COLUMN_3 ENDIF BREAK
				CASE 4 IF bPlaylist RETURN SIX_COLUMN_4_PLAYLIST ELSE RETURN SIX_COLUMN_4 ENDIF BREAK
				CASE 5 IF bPlaylist RETURN SIX_COLUMN_5_PLAYLIST ELSE RETURN SIX_COLUMN_5 ENDIF BREAK 
				CASE 6 IF bPlaylist RETURN SIX_COLUMN_6_PLAYLIST ELSE RETURN SIX_COLUMN_6 ENDIF BREAK
			ENDSWITCH 
		BREAK
		CASE 5
			SWITCH iColumnPriorityOrder
				CASE 1 IF bPlaylist RETURN FIVE_COLUMN_1_PLAYLIST ELSE RETURN FIVE_COLUMN_1 ENDIF BREAK
				CASE 2 IF bPlaylist RETURN FIVE_COLUMN_2_PLAYLIST ELSE RETURN FIVE_COLUMN_2 ENDIF BREAK
				CASE 3 IF bPlaylist RETURN FIVE_COLUMN_3_PLAYLIST ELSE RETURN FIVE_COLUMN_3	ENDIF BREAK
				CASE 4 IF bPlaylist RETURN FIVE_COLUMN_4_PLAYLIST ELSE RETURN FIVE_COLUMN_4	ENDIF BREAK
				CASE 5 IF bPlaylist RETURN FIVE_COLUMN_5_PLAYLIST ELSE RETURN FIVE_COLUMN_5	ENDIF BREAK
			ENDSWITCH 
		BREAK
		CASE 4
			SWITCH iColumnPriorityOrder
				CASE 1 IF bPlaylist RETURN FOUR_COLUMN_1_PLAYLIST ELSE RETURN FOUR_COLUMN_1 ENDIF BREAK
				CASE 2 IF bPlaylist RETURN FOUR_COLUMN_2_PLAYLIST ELSE RETURN FOUR_COLUMN_2 ENDIF BREAK
				CASE 3 IF bPlaylist RETURN FOUR_COLUMN_3_PLAYLIST ELSE RETURN FOUR_COLUMN_3 ENDIF BREAK
				CASE 4 IF bPlaylist RETURN FOUR_COLUMN_4_PLAYLIST ELSE RETURN FOUR_COLUMN_4 ENDIF BREAK
			ENDSWITCH 
		BREAK
		CASE 3
			SWITCH iColumnPriorityOrder
				CASE 1 IF bPlaylist RETURN THREE_COLUMN_1_PLAYLIST ELSE RETURN THREE_COLUMN_1 ENDIF BREAK
				CASE 2 IF bPlaylist RETURN THREE_COLUMN_2_PLAYLIST ELSE RETURN THREE_COLUMN_2 ENDIF BREAK
				CASE 3 IF bPlaylist RETURN THREE_COLUMN_3_PLAYLIST ELSE RETURN THREE_COLUMN_3 ENDIF BREAK
			ENDSWITCH 
		BREAK
		CASE 2
			SWITCH iColumnPriorityOrder
				CASE 1 RETURN TWO_COLUMN_1
				CASE 2 RETURN TWO_COLUMN_2
			ENDSWITCH 
		BREAK
		CASE 1
			RETURN ONE_COLUMN_1
		BREAK
	ENDSWITCH
	
	RETURN ONE_COLUMN_1
ENDFUNC

FUNC STRING sTEAM_TOTALS_NAME()

	IF IS_ON_RALLY_RACE_GLOBAL_SET()
		RETURN "LBD_POS"
	ELIF bIS_TURF_WARS()
		RETURN "LBD_TRF1"
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_POWER_MAD(g_FMMC_STRUCT.iAdversaryModeType)
		RETURN "LBD_POWM"
	ENDIF
	
	RETURN "LBD_TTS"
ENDFUNC

FUNC INT GET_TEAM_SCORE_COLUMN_POSITION()

	IF IS_DOUBLE_DOWN_LEADERBOARD()
		RETURN 2
	ENDIF
	
	RETURN 1
ENDFUNC

PROC DRAW_TEAM_SCORES(LBD_SUB_MODE eSubMode, LEADERBOARD_PLACEMENT_TOOLS& Placement, BOOL bRace, INT iTeamScore, INT iRow, INT iNumColumns, INT iRank = -1)	

	IF IS_THREE_COLUMN_LBD()
	OR HIDE_TEAM_SCORE(eSubMode)
		PRINTLN("[DRAW_TEAM_SCORES] Skipping DRAW_TEAM_SCORES ")
		EXIT
	ENDIF
	
	Placement.TextPlacement[TEXT_PLACEMENT_TEAM_SCORE].x = GET_COLUMN_POS(iNumColumns, GET_TEAM_SCORE_COLUMN_POSITION()) // YOUR_TEAM_SCORE_X
	Placement.TextPlacement[TEXT_PLACEMENT_TEAM_SCORE].y = VARIABLE_BASE_Y+(TO_FLOAT(iRow) * RECT_Y_OFFSET)	
	SET_STANDARD_LBD_TEXT(Placement.aStyle.TS_LBD)
	Placement.aStyle.TS_LBD.aFont = FONT_STANDARD
	SET_TEXT_STYLE(Placement.aStyle.TS_LBD)
	SET_TEXT_WHITE(Placement.aStyle.TS_LBD)
	SET_TEXT_CENTRE(TRUE)
	
	// Actual score (number)
	IF SHOW_SLIPSTREAM_COLUMN()
//		IF NOT lbdVars.bHideMyRow 
			Placement.TextPlacement[TEXT_PLACEMENT_TEAM_SCORE].x = GET_COLUMN_POS(iNumColumns, 2) 
			IF iTeamScore > 0
				IF iTeamScore >= ONE_HOUR_MILLISECONDS								
					DRAW_TEXT_TIMER(Placement.TextPlacement[TEXT_PLACEMENT_TEAM_SCORE], Placement.aStyle.TS_LBD, iTeamScore, TIME_FORMAT_HOURS|TIME_FORMAT_MINUTES|TIME_FORMAT_SECONDS)
				ELSE		
					DRAW_TEXT_TIMER(Placement.TextPlacement[TEXT_PLACEMENT_TEAM_SCORE], Placement.aStyle.TS_LBD, iTeamScore, TIME_FORMAT_MINUTES|TIME_FORMAT_SECONDS|TIME_FORMAT_MILLISECONDS|TEXT_FORMAT_USE_DOT_FOR_MILLISECOND_DIVIDER)	
				ENDIF
			ELIF iTeamScore = -1
				DRAW_TEXT(Placement.TextPlacement[TEXT_PLACEMENT_TEAM_SCORE], Placement.aStyle.TS_LBD, "RACE_LB_DNF")
			ELSE	
				DRAW_TEXT(Placement.TextPlacement[TEXT_PLACEMENT_TEAM_SCORE], Placement.aStyle.TS_LBD, "RACE_LB_EMPTY")
			ENDIF
//		ENDIF
	ELIF NOT bRace
		DRAW_TEXT_WITH_NUMBER(Placement.TextPlacement[TEXT_PLACEMENT_TEAM_SCORE], Placement.aStyle.TS_LBD, "NUMBER", iTeamScore, FONT_CENTRE)
	ELSE
		// "1st Place"
		TEXT_LABEL_23 sTemp = "LBD_PLC_"
		IF iRank > 0
			sTemp += iRank
		ELSE
			// "DNF"
			sTemp = "RACE_LB_DNF"
		ENDIF
		DRAW_TEXT(Placement.TextPlacement[TEXT_PLACEMENT_TEAM_SCORE], Placement.aStyle.TS_LBD, sTemp)
	ENDIF
	
	// "TEAM TOTALS"
	IF iRank > 0
		Placement.TextPlacement[TEXT_PLACEMENT_TEAM_TOTALS].x = TEAM_TOTAL_X
		Placement.TextPlacement[TEXT_PLACEMENT_TEAM_TOTALS].y = VARIABLE_BASE_Y+(TO_FLOAT(iRow) * RECT_Y_OFFSET)	
		SET_STANDARD_LBD_PLYR_NAME_TEXT(Placement.aStyle.TS_LBDPLYR)
		SET_TEXT_STYLE(Placement.aStyle.TS_LBD)
		SET_TEXT_JUSTIFICATION(FONT_LEFT)
		
		SET_TEXT_WHITE(Placement.aStyle.TS_LBD)

		// "TEAM SCORE TOTAL"
		DRAW_TEXT(Placement.TextPlacement[TEXT_PLACEMENT_TEAM_TOTALS], Placement.aStyle.TS_LBD, sTEAM_TOTALS_NAME())
	ENDIF
	
	SET_TEXT_CENTRE(TRUE)
ENDPROC

FUNC INT GET_VEH_CHAR_LIMIT()
	SWITCH GET_CURRENT_LANGUAGE()
		CASE LANGUAGE_RUSSIAN
		CASE LANGUAGE_POLISH
		CASE LANGUAGE_ITALIAN
		CASE LANGUAGE_FRENCH
		CASE LANGUAGE_PORTUGUESE
		
			RETURN 6
		BREAK
		CASE LANGUAGE_JAPANESE
		CASE LANGUAGE_CHINESE
		CASE LANGUAGE_KOREAN
		CASE LANGUAGE_CHINESE_SIMPLIFIED
			RETURN 4
		BREAK
	ENDSWITCH  
	
	RETURN 11
ENDFUNC

FUNC INT GET_VOTE_CHAR_LIMIT()
	SWITCH GET_CURRENT_LANGUAGE()
		CASE LANGUAGE_JAPANESE
		CASE LANGUAGE_CHINESE
		CASE LANGUAGE_RUSSIAN
		CASE LANGUAGE_KOREAN
		CASE LANGUAGE_POLISH
		CASE LANGUAGE_ITALIAN
		CASE LANGUAGE_FRENCH
		CASE LANGUAGE_PORTUGUESE
		CASE LANGUAGE_CHINESE_SIMPLIFIED
			RETURN 6
		BREAK
		
	ENDSWITCH  
	
	RETURN 11
ENDFUNC

FUNC INT GET_LBD_TITLE_CHAR_LIMIT()

	RETURN 8
ENDFUNC

FUNC INT GET_TITLE_CHAR_LIMIT()
	SWITCH GET_CURRENT_LANGUAGE()
		CASE LANGUAGE_JAPANESE
		CASE LANGUAGE_CHINESE
		CASE LANGUAGE_KOREAN
		CASE LANGUAGE_CHINESE_SIMPLIFIED
			RETURN 16
		BREAK
		
	ENDSWITCH  
	
	RETURN 40
ENDFUNC

FUNC STRING GET_LABEL_IF_SHORTENED(BOOL bShortened)

	IF bShortened
		RETURN "VEH_NAME_DOTS"
	ENDIF
	
	RETURN "STRING"
ENDFUNC

FUNC TEXT_LABEL_63 GET_SHORTENED_TEXT_LABEL(STRING sTextToCheck, BOOL& bShortened, INT iCharLimit, BOOL bLiteral = FALSE)

	TEXT_LABEL_63 tlFullName
	TEXT_LABEL_63 tlShortenedName

	INT iOriginalStringLength 
	
	IF bLiteral = FALSE
		iOriginalStringLength = GET_LENGTH_OF_STRING_WITH_THIS_TEXT_LABEL(sTextToCheck) 
		tlFullName = GET_FILENAME_FOR_AUDIO_CONVERSATION(sTextToCheck)
	ELSE
		iOriginalStringLength = GET_LENGTH_OF_LITERAL_STRING(g_FMMC_STRUCT.tl63MissionName)
		tlFullName = g_FMMC_STRUCT.tl63MissionName
	ENDIF

	IF NOT IS_STRING_NULL_OR_EMPTY(tlFullName)
		IF iCharLimit >= iOriginalStringLength
			tlShortenedName = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME(tlFullName, 0, iOriginalStringLength)
			bShortened = FALSE
		ELSE
			tlShortenedName = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME(tlFullName, 0, iCharLimit)
			bShortened = TRUE
		ENDIF
	
		PRINTLN("GET_SHORTENED_TEXT_LABEL, iCharLimit = ", iCharLimit, " tlFullName = ", tlFullName, " iOriginalStringLength = ", iOriginalStringLength, " tlShortenedName = ", tlShortenedName)
		
		IF bShortened
			RETURN tlShortenedName
		ENDIF
	ENDIF
	
	RETURN tlFullName
ENDFUNC

PROC DRAW_A_TITLE(LBD_VARS& lbdVars, LEADERBOARD_PLACEMENT_TOOLS& Placement, INT iPriority, INT iNumColumns, STRING sColumnTitle, BOOL bNumbers = FALSE, INT iColumnNumber = 0, BOOL bPlayList = FALSE, BOOL bShowRounds = FALSE)
	
	SET_TEXT_CENTRE(TRUE)
	FLOAT fOffset
	TEXT_LABEL_63 tlTitle
	BOOL bShorten = FALSE

	SWITCH iNumColumns
		CASE 6
			SWITCH iPriority
				CASE 1 IF bPlayList fOffset = plf6wrap1EndX ELSE fOffset = f6wrap1EndX ENDIF BREAK		
				CASE 2 IF bPlayList fOffset = plf6wrap2EndX ELSE fOffset = f6wrap2EndX ENDIF BREAK		
				CASE 3 IF bPlayList fOffset = plf6wrap3EndX ELSE fOffset = f6wrap3EndX ENDIF BREAK 		
				CASE 4 IF bPlayList fOffset = plf6wrap4EndX ELSE fOffset = f6wrap4EndX ENDIF BREAK		
				CASE 5 IF bPlayList fOffset = plf6wrap5EndX ELSE fOffset = f6wrap5EndX ENDIF BREAK		
				CASE 6 IF bPlayList fOffset = plf6wrap6EndX ELSE fOffset = f6wrap6EndX ENDIF BREAK		
			ENDSWITCH																					
		BREAK																							
																										
		CASE 5																							
			SWITCH iPriority																			
				CASE 1 IF bPlayList fOffset = plf5wrap1EndX ELSE fOffset = f5wrap1EndX ENDIF BREAK		
				CASE 2 IF bPlayList fOffset = plf5wrap2EndX ELSE fOffset = f5wrap2EndX ENDIF BREAK		
				CASE 3 IF bPlayList fOffset = plf5wrap3EndX ELSE fOffset = f5wrap3EndX ENDIF BREAK		
				CASE 4 IF bPlayList fOffset = plf5wrap4EndX ELSE fOffset = f5wrap4EndX ENDIF BREAK		
				CASE 5 
					IF bPlayList 
					OR (ARE_STRINGS_EQUAL(sColumnTitle, "LBD_VOTES") AND IS_SCREEN_SMALL())
						fOffset = plf5wrap5EndX 
					ELSE 
						fOffset = f5wrap5EndX 
					ENDIF 
				BREAK		
			ENDSWITCH  																					
		BREAK																							
																										
		CASE 4																							
			SWITCH iPriority																			
				CASE 1 IF bPlayList fOffset = plf4wrap1EndX ELSE fOffset = f4wrap1EndX ENDIF BREAK
				CASE 2 IF bPlayList fOffset = plf4wrap2EndX ELSE fOffset = f4wrap2EndX ENDIF BREAK
				CASE 3 IF bPlayList fOffset = plf4wrap3EndX ELSE fOffset = f4wrap3EndX ENDIF BREAK
				CASE 4 IF bPlayList fOffset = plf4wrap4EndX ELSE fOffset = f4wrap4EndX ENDIF BREAK
			ENDSWITCH
		BREAK
		
		CASE 3
			SWITCH iPriority
				CASE 1 IF bPlayList fOffset = plf3wrap1EndX ELSE fOffset = f3wrap1EndX ENDIF BREAK
				CASE 2 IF bPlayList fOffset = plf3wrap2EndX ELSE fOffset = f3wrap2EndX ENDIF BREAK
				CASE 3 IF bPlayList fOffset = plf3wrap3EndX ELSE fOffset = f3wrap3EndX ENDIF BREAK
			ENDSWITCH
		BREAK
		
		CASE 2
			SWITCH iPriority
				CASE 1 fOffset =  f2wrap1EndX BREAK
				CASE 2 fOffset =  f2wrap2EndX BREAK
			ENDSWITCH
		BREAK
		
		CASE 1
			SWITCH iPriority
				CASE 1 fOffset = f1wrap1EndX BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
//	SET_LB_COLUMN_TITLES_FONT_AND_SIZE(Placement.aStyle.TS_LBD, DROPSTYLE_NONE)
	SET_STANDARD_LBD_TEXT(Placement.aStyle.TS_LBD)
	SET_TEXT_STYLE(Placement.aStyle.TS_LBD)	
	SET_TEXT_COLOUR_WITH_HUD_COLOUR(Placement.aStyle.TS_LBD, HUD_COLOUR_PURE_WHITE)
	
	IF bShowRounds 
		fOffset = fRoundsOffset
	ENDIF
	
	Placement.TextPlacement[TEXT_PLACEMENT_TITLE].x = (COLUM_TITLE_X + fOffset)
	Placement.TextPlacement[TEXT_PLACEMENT_TITLE].y = COLUM_TITLE_Y
	
	IF bNumbers = FALSE
		IF NOT GET_IS_WIDESCREEN()
			bShorten = TRUE
		ENDIF
		IF bShorten
			tlTitle = GET_SHORTENED_TEXT_LABEL(sColumnTitle, bShorten, GET_LBD_TITLE_CHAR_LIMIT())
			DISPLAY_TEXT_WITH_PLAYER_NAME(Placement.TextPlacement[TEXT_PLACEMENT_TITLE].x, Placement.TextPlacement[TEXT_PLACEMENT_TITLE].y, GET_LABEL_IF_SHORTENED(bShorten), tlTitle, HUD_COLOUR_PURE_WHITE)
			PRINTLN("[2195822] DRAW_A_TITLE, bShorten = TRUE,  tlTitle = ", tlTitle, " GET_LBD_TITLE_CHAR_LIMIT() = ", GET_LBD_TITLE_CHAR_LIMIT())
		ELSE
			DRAW_TEXT(Placement.TextPlacement[TEXT_PLACEMENT_TITLE], Placement.aStyle.TS_LBD, sColumnTitle)
			PRINTLN("[2195822] DRAW_A_TITLE, bShorten = FALSE,  sColumnTitle = ", sColumnTitle, " GET_LBD_TITLE_CHAR_LIMIT() = ", GET_LBD_TITLE_CHAR_LIMIT())
		ENDIF
	ELSE
//		IF bShowRounds
//			PRINTLN("[CS_ROUND] DRAW_A_TITLE, iNumberOfRounds = ", g_sTransitionSessionData.sMissionRoundData.iNumberOfRounds, " iNumColumns = ", iNumColumns)
//			DRAW_TEXT_WITH_NUMBER(Placement.TextPlacement[TEXT_PLACEMENT_TITLE], Placement.aStyle.TS_LBD, sColumnTitle, (g_sTransitionSessionData.sMissionRoundData.iNumberOfRounds + 1))
//		ELSE
			INT iPlaylistProgress
			SWITCH iColumnNumber
			
				CASE 1
					iPlaylistProgress = (CLAMP_INT(g_sCurrentPlayListDetails.iPlaylistProgress - 4, 1, 100))
				BREAK
				
				CASE 2
					iPlaylistProgress = (CLAMP_INT(g_sCurrentPlayListDetails.iPlaylistProgress - 3, 2, 100))
				BREAK
				
				CASE 3
					iPlaylistProgress = (CLAMP_INT(g_sCurrentPlayListDetails.iPlaylistProgress - 2, 3, 100))
				BREAK
				
				CASE 4
					iPlaylistProgress = (CLAMP_INT(g_sCurrentPlayListDetails.iPlaylistProgress - 1, 4, 100))
				BREAK
				
			ENDSWITCH
			
			DRAW_TEXT_WITH_2_NUMBERS(Placement.TextPlacement[TEXT_PLACEMENT_TITLE], Placement.aStyle.TS_LBD, "LBD_NUM", iPlaylistProgress, lbdVars.iPlaylistJobCount)
//		ENDIF
	ENDIF
ENDPROC

FUNC BOOL HAS_VOTED()

	IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_VOTED_UP)
	OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_VOTED_DOWN)
	OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_NO_VOTE)
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC STRING GET_LBD_COLUMN_TITLE(LBD_VARS& lbdVars, LBD_SUB_MODE eSubMode, INT iNumColumns, INT iColumnPriority)

	// These are special cases for the Mission leaderboards and are set in the Creator	
	IF bIS_JUGGERNAUT()
	AND NOT IS_PLAYLIST_LBD(eSubMode)
		SWITCH iColumnPriority
			CASE 1 RETURN "LBD_JUGG"
			CASE 2 RETURN "LBD_KIL"
			CASE 3 RETURN "LBD_DTH"
			CASE 4 RETURN "LBD_XP"
			CASE 5 RETURN "LBD_CASH"
			CASE 6 
				IF IS_ROUNDS_MISSION_LBD(eSubMode)
					IF IS_SCREEN_SMALL()
						RETURN "LBD_RND_SM"
					ELSE
						RETURN "LBD_RND"
					ENDIF
				ELSE
					IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_THUMB_VOTING_ALLOWED)	
						RETURN "LBD_VOTES"
					ELSE
						RETURN "LBD_NXT"		
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
	ELIF bIS_TURF_WARS()
	AND NOT IS_PLAYLIST_LBD(eSubMode)
		SWITCH iColumnPriority
			CASE 1 RETURN "LBD_TRF"
			CASE 2 RETURN "LBD_KIL"
			CASE 3 RETURN "LBD_DTH"
			CASE 4 RETURN "LBD_XP"
			CASE 5 RETURN "LBD_CASH"
			CASE 6 
				IF IS_ROUNDS_MISSION_LBD(eSubMode)
					IF IS_SCREEN_SMALL()
						RETURN "LBD_RND_SM"
					ELSE
						RETURN "LBD_RND"
					ENDIF
				ELSE
					IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_THUMB_VOTING_ALLOWED)	
						RETURN "LBD_VOTES"
					ELSE
						RETURN "LBD_NXT"		
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
	ELIF CUSTOM_LBD_REMIX()
	AND NOT IS_PLAYLIST_LBD(eSubMode)
		SWITCH iColumnPriority
			CASE 1 RETURN "LBD_SCO"
			CASE 2 RETURN "LBD_BEST_T"
			CASE 3 RETURN "LBD_DTH"
			CASE 4 RETURN "LBD_XP"
			CASE 5 RETURN "LBD_CASH"
			CASE 6 
				IF IS_ROUNDS_MISSION_LBD(eSubMode)
					IF IS_SCREEN_SMALL()
						RETURN "LBD_RND_SM"
					ELSE
						RETURN "LBD_RND"
					ENDIF
				ELSE
					IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_THUMB_VOTING_ALLOWED)	
						RETURN "LBD_VOTES"
					ELSE
						RETURN "LBD_NXT"		
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
	ELIF SHOW_SLIPSTREAM_COLUMN()
	AND NOT IS_PLAYLIST_LBD(eSubMode)
		SWITCH iColumnPriority
			CASE 1 
				IF lbdVars.eRaceType = eRACE_TYPE_LAPS
					PRINTLN("lbdVars.eRaceType = eRACE_TYPE_LAPS ")
					RETURN "LBD_BLT"
				ELIF lbdVars.eRaceType = eRACE_TYPE_P2P
					PRINTLN("lbdVars.eRaceType = eRACE_TYPE_P2P ")
					RETURN "LBD_CHK"
				ELSE
					PRINTLN("lbdVars.eRaceType = INVALID ")
				ENDIF
			BREAK
			CASE 2 RETURN "LBD_SLP"
			CASE 3 RETURN "LBD_XP"
			CASE 4 RETURN "LBD_CASH"
			CASE 5
				IF IS_ROUNDS_MISSION_LBD(eSubMode)
					IF IS_SCREEN_SMALL()
						RETURN "LBD_RND_SM"
					ELSE
						RETURN "LBD_RND"
					ENDIF
				ELSE
					IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_THUMB_VOTING_ALLOWED)	
						RETURN "LBD_VOTES"
					ELSE
						RETURN "LBD_NXT"		
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
	ELIF SHOW_ASSISTS_LEADERBOARD()
	AND NOT IS_PLAYLIST_LBD(eSubMode)
		SWITCH iColumnPriority
			CASE 1 RETURN "LBD_KIL"
			CASE 2 RETURN "LBD_ASS"
			CASE 3 RETURN "LBD_XP"
			CASE 4 RETURN "LBD_CASH"
			CASE 5 
				IF IS_ROUNDS_MISSION_LBD(eSubMode)
					IF IS_SCREEN_SMALL()
						RETURN "LBD_RND_SM"
					ELSE
						RETURN "LBD_RND"
					ENDIF
				ELSE
					IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_THUMB_VOTING_ALLOWED)	
						RETURN "LBD_VOTES"
					ELSE
						RETURN "LBD_NXT"		
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
	ELIF HIDE_KILLS_SHOW_DEATHS_COLUMN_LBD()
	AND NOT IS_PLAYLIST_LBD(eSubMode)
	AND !IS_ARENA_LBD(eSubMode)
		SWITCH iColumnPriority
			CASE 1 RETURN "LBD_DTH"
			CASE 2 RETURN "LBD_XP"
			CASE 3 RETURN "LBD_CASH"
			CASE 4 
				IF IS_ROUNDS_MISSION_LBD(eSubMode)
					IF IS_SCREEN_SMALL()
						RETURN "LBD_RND_SM"
					ELSE
						RETURN "LBD_RND"
					ENDIF
				ELSE
					IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_THUMB_VOTING_ALLOWED)	
						RETURN "LBD_VOTES"
					ELSE
						RETURN "LBD_NXT"		
					ENDIF
				ENDIF	
			BREAK
		ENDSWITCH
	ELIF IS_THREE_COLUMN_LBD()
	AND NOT IS_PLAYLIST_LBD(eSubMode)
		SWITCH iColumnPriority
			CASE 1 RETURN "LBD_XP"
			CASE 2 RETURN "LBD_CASH"
			CASE 3 
				IF IS_PLAYLIST_ROUNDS_MISSION(eSubMode)
					IF IS_SCREEN_SMALL()
						RETURN "LBD_RND_SM"
					ELSE
						RETURN "LBD_RND"
					ENDIF	
				ELSE
					RETURN "LBD_NXT"
				ENDIF
			BREAK
		ENDSWITCH
	ELIF IS_NO_SCORE_NO_KILL_LBD_SET()
	AND NOT IS_PLAYLIST_LBD(eSubMode)
		SWITCH iColumnPriority
			CASE 1 RETURN "LBD_DTH"
			CASE 2 RETURN "LBD_XP"
			CASE 3 RETURN "LBD_CASH"
			CASE 4 
				IF IS_ROUNDS_MISSION_LBD(eSubMode)
					IF IS_SCREEN_SMALL()
						RETURN "LBD_RND_SM"
					ELSE
						RETURN "LBD_RND"
					ENDIF
				ELSE
					IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_THUMB_VOTING_ALLOWED)	
//						IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_VOTED_UP)
//						OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_VOTED_DOWN)
//						OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_NO_VOTE)
							RETURN "LBD_VOTES"
//						ELSE
//							RETURN "LBD_BET"
//						ENDIF
					ELSE
						RETURN "LBD_NXT"		
					ENDIF
				ENDIF	
			BREAK
		ENDSWITCH
	ELIF HIDE_KILLS_DEATHS_COLUMN_LBD()
	AND NOT IS_PLAYLIST_LBD(eSubMode)
	AND eSubMode <> SUB_FINAL_ROUNDS
	AND !IS_ARENA_LBD(eSubMode)
		SWITCH iColumnPriority
			CASE 1
				IF SHOW_TIME_COLUMN()
					RETURN "LBD_TIME"
				ELSE
					RETURN "LBD_SCO"
				ENDIF
			BREAK
			CASE 2 RETURN "LBD_XP"
			CASE 3 RETURN "LBD_CASH"
			CASE 4 
				IF IS_ROUNDS_MISSION_LBD(eSubMode)
					IF IS_SCREEN_SMALL()
						RETURN "LBD_RND_SM"
					ELSE
						RETURN "LBD_RND"
					ENDIF
				ELSE
					IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_THUMB_VOTING_ALLOWED)	
//						IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_VOTED_UP)
//						OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_VOTED_DOWN)
//						OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_NO_VOTE)
							RETURN "LBD_VOTES"
//						ELSE
//							RETURN "LBD_BET"
//						ENDIF
					ELSE
						RETURN "LBD_NXT"		
					ENDIF
				ENDIF	
			BREAK
			CASE 5
				IF eSubMode = SUB_FINAL_ROUNDS
					IF IS_SCREEN_SMALL()
						RETURN "LBD_RND_SM"
					ELSE
						RETURN "LBD_RND"
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
	ELSE

		SWITCH eSubMode
		
			CASE SUB_RACE_TARGET
				SWITCH iColumnPriority
					CASE 1 RETURN "LBD_VEH"
					CASE 2 RETURN "LBD_SCO"
					CASE 3
						IF IS_SCREEN_SMALL()
							RETURN "LBD_TIME_SM"
						ELSE
							RETURN "LBD_TIME"
						ENDIF
					BREAK
					CASE 4 RETURN "LBD_XP"
					CASE 5 RETURN "LBD_CASH"
					CASE 6 
						IF REPLACE_BETTING_COLUMN()
							IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_THUMB_VOTING_ALLOWED)	
							AND HAS_VOTED()
								RETURN "LBD_VOTES"
							ELSE
								RETURN "LBD_NXT"		
							ENDIF
						ELSE
							RETURN "LBD_BET"
						ENDIF
					BREAK
				ENDSWITCH
			
			BREAK
			
			CASE SUB_RACE_AGG_BEST
				SWITCH iColumnPriority
					CASE 1 RETURN "LBD_VEH"
					CASE 2 RETURN "LBD_SCO_PCN"
					CASE 3 RETURN "LBD_BES"
					CASE 4 RETURN "LBD_XP"
					CASE 5 RETURN "LBD_CASH"
					CASE 6 
						IF REPLACE_BETTING_COLUMN()
							IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_THUMB_VOTING_ALLOWED)	
							AND HAS_VOTED()
								RETURN "LBD_VOTES"
							ELSE
								RETURN "LBD_NXT"		
							ENDIF
						ELSE
							RETURN "LBD_BET"
						ENDIF
					BREAK
				ENDSWITCH
			BREAK
			CASE SUB_RACE_AGG_TIME
				SWITCH iColumnPriority
					CASE 1 RETURN "LBD_VEH"
					CASE 2 RETURN "LBD_SCO_PCN"
					CASE 3
						IF IS_SCREEN_SMALL()
							RETURN "LBD_TIME_SM"
						ELSE
							RETURN "LBD_TIME"
						ENDIF
					BREAK
					CASE 4 RETURN "LBD_XP"
					CASE 5 RETURN "LBD_CASH"
					CASE 6 
						IF REPLACE_BETTING_COLUMN()
							IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_THUMB_VOTING_ALLOWED)	
							AND HAS_VOTED()
								RETURN "LBD_VOTES"
							ELSE
								RETURN "LBD_NXT"		
							ENDIF
						ELSE
							RETURN "LBD_BET"
						ENDIF
					BREAK
				ENDSWITCH
			BREAK
			
			CASE SUB_RACE_FOOT
				SWITCH iNumColumns
					CASE 5
						SWITCH iColumnPriority
							CASE 1 
								IF IS_SCREEN_SMALL()
									RETURN "LBD_TIME_SM"
								ELSE
									RETURN "LBD_TIME"
								ENDIF
							BREAK
							CASE 2 RETURN "LBD_BES"
							CASE 3 RETURN "LBD_XP"
							CASE 4 RETURN "LBD_CASH"
							CASE 5 
								IF REPLACE_BETTING_COLUMN()
									IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_THUMB_VOTING_ALLOWED)	
										IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_VOTED_UP)
										OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_VOTED_DOWN)
										OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_NO_VOTE)
											RETURN "LBD_VOTES"
										ELSE
											RETURN "LBD_BET"
										ENDIF
									ELSE
										RETURN "LBD_NXT"		
									ENDIF
								ELSE
									RETURN "LBD_BET"
								ENDIF	
							BREAK
						ENDSWITCH	
					BREAK
					CASE 4
						// ON FOOT
						SWITCH iColumnPriority
							CASE 1 
								IF IS_SCREEN_SMALL()
									RETURN "LBD_TIME_SM"
								ELSE
									RETURN "LBD_TIME"
								ENDIF
							BREAK
							CASE 2 RETURN "LBD_XP"
							CASE 3 RETURN "LBD_CASH"
							CASE 4 
								IF REPLACE_BETTING_COLUMN()
									IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_THUMB_VOTING_ALLOWED)	
										IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_VOTED_UP)
										OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_VOTED_DOWN)
										OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_NO_VOTE)
											RETURN "LBD_VOTES"
										ELSE
											RETURN "LBD_BET"
										ENDIF
									ELSE
										RETURN "LBD_NXT"		
									ENDIF		
								ELSE
									RETURN "LBD_BET"
								ENDIF	
							BREAK
						ENDSWITCH		  
					BREAK
				ENDSWITCH
			BREAK
			CASE SUB_RACE_NORMAL
			CASE SUB_RACE_STUNT
			CASE SUB_RACE_RALLY
			CASE SUB_RACE_GTA
				SWITCH iNumColumns
					CASE 6
						SWITCH iColumnPriority
							CASE 1 RETURN "LBD_VEH"
							CASE 2 
							
								IF DRAW_CUSTOM_TITLE() 
								
									RETURN CUSTOM_COLUMN_TITLE(g_FMMC_STRUCT.tl63PointsTitle) 
								ENDIF
							
								IF IS_SCREEN_SMALL()
									RETURN "LBD_TIME_SM"
								ELSE
									RETURN "LBD_TIME"
								ENDIF
							BREAK
							CASE 3 RETURN "LBD_BES"
							CASE 4 RETURN "LBD_XP"
							CASE 5 RETURN "LBD_CASH"
							CASE 6 
								IF REPLACE_BETTING_COLUMN()
									IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_THUMB_VOTING_ALLOWED)	
									AND HAS_VOTED()
										RETURN "LBD_VOTES"
									ELSE
										RETURN "LBD_NXT"		
									ENDIF
								ELSE
									RETURN "LBD_BET"
								ENDIF
							BREAK
						ENDSWITCH
					BREAK
					CASE 5
						SWITCH iColumnPriority
							CASE 1 RETURN "LBD_VEH"
							
							CASE 2
								IF IS_SCREEN_SMALL()
									RETURN "LBD_TIME_SM"
								ELSE
									RETURN "LBD_TIME"
								ENDIF
							BREAK
							CASE 3 RETURN "LBD_XP"
							
							CASE 4 RETURN "LBD_CASH"
							CASE 5 
								IF REPLACE_BETTING_COLUMN()
									IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_THUMB_VOTING_ALLOWED)	
									AND HAS_VOTED()
										RETURN "LBD_VOTES"
									ELSE
										RETURN "LBD_NXT"
									ENDIF	
								ELSE
									RETURN "LBD_BET"
								ENDIF	
							BREAK
						ENDSWITCH	
					BREAK
					
				ENDSWITCH
			BREAK
			CASE SUB_RACE_BASEJUMP
				SWITCH iNumColumns
					CASE 5
						SWITCH iColumnPriority
							CASE 1 RETURN "LBD_SCO"
							CASE 2 
								IF IS_SCREEN_SMALL()
									RETURN "LBD_TIME_SM"
								ELSE
									RETURN "LBD_TIME"
								ENDIF
							BREAK
							CASE 3 RETURN "LBD_XP"
							CASE 4 RETURN "LBD_CASH"
							CASE 5 
								IF REPLACE_BETTING_COLUMN()
									IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_THUMB_VOTING_ALLOWED)	
										IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_VOTED_UP)
										OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_VOTED_DOWN)
										OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_NO_VOTE)
											RETURN "LBD_VOTES"
										ELSE
											RETURN "LBD_BET"
										ENDIF
									ELSE
										RETURN "LBD_NXT"		
									ENDIF		
								ELSE
									RETURN "LBD_BET"
								ENDIF	
							BREAK
						ENDSWITCH		  
					BREAK
				ENDSWITCH
			BREAK
			CASE SUB_ARENA_CARNAGE
				SWITCH iColumnPriority
					CASE 1 RETURN "LBD_VEH"
					CASE 2 RETURN "LBD_KIL"
					CASE 3 RETURN "LBD_DD"
					CASE 4 RETURN "LBD_AP"
					CASE 5 RETURN "LBD_CASH"
					CASE 6
						IF IS_ROUNDS_MISSION_LBD(eSubMode)
							IF IS_SCREEN_SMALL()
								RETURN "LBD_RND_SM"
							ELSE
								RETURN "LBD_RND"
							ENDIF
						ELSE
							IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_THUMB_VOTING_ALLOWED)
								IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_VOTED_UP)
								OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_VOTED_DOWN)
								OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_NO_VOTE)
									RETURN "LBD_VOTES"
								ENDIF
							ENDIF		
							
							RETURN "LBD_NXT"	
						ENDIF
					BREAK
				ENDSWITCH
			BREAK
			CASE SUB_ARENA_ARCADE
				SWITCH iColumnPriority
					CASE 1 RETURN "LBD_VEH"
					CASE 2 RETURN "LBD_TS"
					CASE 3 RETURN "LBD_KIL"
					CASE 4 RETURN "LBD_APP"
					CASE 5 RETURN "LBD_CASH"
					CASE 6
						IF IS_ROUNDS_MISSION_LBD(eSubMode)
							IF IS_SCREEN_SMALL()
								RETURN "LBD_RND_SM"
							ELSE
								RETURN "LBD_RND"
							ENDIF
						ELSE
							IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_THUMB_VOTING_ALLOWED)
								IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_VOTED_UP)
								OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_VOTED_DOWN)
								OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_NO_VOTE)
									RETURN "LBD_VOTES"
								ENDIF
							ENDIF		
							
							RETURN "LBD_NXT"	
						ENDIF
					BREAK
				ENDSWITCH
			BREAK
			CASE SUB_ARENA_CTF
				SWITCH iColumnPriority
					CASE 1 RETURN "LBD_VEH"
					CASE 2 RETURN "LBD_FD"
					CASE 3 RETURN "LBD_FR"
					CASE 4 RETURN "LBD_AP"
					CASE 5 RETURN "LBD_CASH"
					CASE 6
						IF IS_ROUNDS_MISSION_LBD(eSubMode)
							IF IS_SCREEN_SMALL()
								RETURN "LBD_RND_SM"
							ELSE
								RETURN "LBD_RND"
							ENDIF
						ELSE
							IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_THUMB_VOTING_ALLOWED)
								IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_VOTED_UP)
								OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_VOTED_DOWN)
								OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_NO_VOTE)
									RETURN "LBD_VOTES"
								ENDIF
							ENDIF		
							
							RETURN "LBD_NXT"	
						ENDIF	
					BREAK
				ENDSWITCH
			BREAK
			CASE SUB_ARENA_DDERBY
				SWITCH iColumnPriority
					CASE 1 RETURN "LBD_VEH"
					CASE 2 RETURN "LBD_ATIME"  
					CASE 3 RETURN "LBD_BES"
					CASE 4 RETURN "LBD_APP"
					CASE 5 RETURN "LBD_CASH"
					CASE 6
						IF IS_ROUNDS_MISSION_LBD(eSubMode)
							IF IS_SCREEN_SMALL()
								RETURN "LBD_RND_SM"
							ELSE
								RETURN "LBD_RND"
							ENDIF
						ELSE
							IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_THUMB_VOTING_ALLOWED)
								IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_VOTED_UP)
								OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_VOTED_DOWN)
								OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_NO_VOTE)
									RETURN "LBD_VOTES"
								ENDIF
							ENDIF		
							
							RETURN "LBD_NXT"	
						ENDIF	
					BREAK
				ENDSWITCH
			BREAK
			CASE SUB_ARENA_DETONATION
				SWITCH iColumnPriority
					CASE 1 RETURN "LBD_VEH"
					CASE 2 RETURN "LBD_PNT" 
					CASE 3 RETURN "LBD_GLS"
					CASE 4 RETURN "LBD_AP"
					CASE 5 RETURN "LBD_CASH"
					CASE 6
						IF IS_ROUNDS_MISSION_LBD(eSubMode)
							IF IS_SCREEN_SMALL()
								RETURN "LBD_RND_SM"
							ELSE
								RETURN "LBD_RND"
							ENDIF
						ELSE
							IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_THUMB_VOTING_ALLOWED)
								IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_VOTED_UP)
								OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_VOTED_DOWN)
								OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_NO_VOTE)
									RETURN "LBD_VOTES"
								ENDIF
							ENDIF		
							
							RETURN "LBD_NXT"	
						ENDIF
					BREAK
				ENDSWITCH
			BREAK
			CASE SUB_ARENA_GAMESMASTER
				SWITCH iColumnPriority
					CASE 1 RETURN "LBD_VEH"
					CASE 2 RETURN "LBD_CHK" 
					CASE 3 RETURN "LBD_KIL"
					CASE 4 RETURN "LBD_AP"
					CASE 5 RETURN "LBD_CASH"
					CASE 6
						IF IS_ROUNDS_MISSION_LBD(eSubMode)
							IF IS_SCREEN_SMALL()
								RETURN "LBD_RND_SM"
							ELSE
								RETURN "LBD_RND"
							ENDIF
						ELSE
							IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_THUMB_VOTING_ALLOWED)
								IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_VOTED_UP)
								OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_VOTED_DOWN)
								OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_NO_VOTE)
									RETURN "LBD_VOTES"
								ENDIF
							ENDIF		
							
							RETURN "LBD_NXT"	
						ENDIF
					BREAK
				ENDSWITCH
			BREAK
			CASE SUB_ARENA_MONSTER
			CASE SUB_ARENA_FIRE
				SWITCH iColumnPriority
					CASE 1 RETURN "LBD_VEH"
					CASE 2 RETURN "LBD_KIL" 
					CASE 3 RETURN "LBD_DTH"
					CASE 4 RETURN "LBD_AP"
					CASE 5 RETURN "LBD_CASH"
					CASE 6
						IF IS_ROUNDS_MISSION_LBD(eSubMode)
							IF IS_SCREEN_SMALL()
								RETURN "LBD_RND_SM"
							ELSE
								RETURN "LBD_RND"
							ENDIF
						ELSE
							IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_THUMB_VOTING_ALLOWED)
								IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_VOTED_UP)
								OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_VOTED_DOWN)
								OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_NO_VOTE)
									RETURN "LBD_VOTES"
								ENDIF
							ENDIF		
							
							RETURN "LBD_NXT"	
						ENDIF
					BREAK
				ENDSWITCH
			BREAK
			CASE SUB_ARENA_PASSBOMB
				SWITCH iColumnPriority
					CASE 1 RETURN "LBD_VEH"
					CASE 2 RETURN "LBD_TS"  
					CASE 3 RETURN "LBD_BOM"
					CASE 4 RETURN "LBD_AP"
					CASE 5 RETURN "LBD_CASH"
					CASE 6
						IF IS_ROUNDS_MISSION_LBD(eSubMode)
							IF IS_SCREEN_SMALL()
								RETURN "LBD_RND_SM"
							ELSE
								RETURN "LBD_RND"
							ENDIF
						ELSE
							IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_THUMB_VOTING_ALLOWED)
								IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_VOTED_UP)
								OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_VOTED_DOWN)
								OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_NO_VOTE)
									RETURN "LBD_VOTES"
								ENDIF
							ENDIF		
							
							RETURN "LBD_NXT"	
						ENDIF	
					BREAK
				ENDSWITCH
			BREAK
			CASE SUB_ARENA_TAG_TEAM
				SWITCH iColumnPriority
					CASE 1 RETURN "LBD_VEH"
					CASE 2 RETURN "LBD_KIL" 
					CASE 3 RETURN "LBD_TAG"
					CASE 4 RETURN "LBD_AP"
					CASE 5 RETURN "LBD_CASH"
					CASE 6
						IF IS_ROUNDS_MISSION_LBD(eSubMode)
							IF IS_SCREEN_SMALL()
								RETURN "LBD_RND_SM"
							ELSE
								RETURN "LBD_RND"
							ENDIF
						ELSE
							IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_THUMB_VOTING_ALLOWED)
								IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_VOTED_UP)
								OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_VOTED_DOWN)
								OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_NO_VOTE)
									RETURN "LBD_VOTES"
								ENDIF
							ENDIF		
							
							RETURN "LBD_NXT"	
						ENDIF
					BREAK
				ENDSWITCH
			BREAK
			CASE SUB_KOTH
				SWITCH iNumColumns
					CASE 6
						SWITCH iColumnPriority
							CASE 1	RETURN "LBD_SCO"
							CASE 2	RETURN "LBD_KIL"
							CASE 3 	RETURN "LBD_DTH"
							CASE 4 
								IF CONTENT_IS_USING_ARENA()
									RETURN "LBD_AP"
								ENDIF
								
								RETURN "LBD_XP"
							
							CASE 5 RETURN "LBD_CASH"
							CASE 6
								IF REPLACE_BETTING_COLUMN()
									IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_THUMB_VOTING_ALLOWED)
										IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_VOTED_UP)
										OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_VOTED_DOWN)
										OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_NO_VOTE)
											RETURN "LBD_VOTES"
										ELSE
											RETURN "LBD_BET"
										ENDIF
									ELSE
										RETURN "LBD_NXT"		
									ENDIF		
								ELSE
									RETURN "LBD_BET"
								ENDIF	
							BREAK
						ENDSWITCH
					BREAK
				ENDSWITCH
			BREAK
			CASE SUB_DM_FFA
			CASE SUB_DM_TEAM
				SWITCH iNumColumns
					CASE 6
						SWITCH iColumnPriority
							CASE 1
								IF REPLACE_SCORE_WITH_KILLS()
									RETURN "LBD_KIL"
								ELSE
									RETURN "LBD_SCO"
								ENDIF
							BREAK
							CASE 2 RETURN "LBD_DTH"
							CASE 3 RETURN "LBD_RAT"
							CASE 4 
								IF CONTENT_IS_USING_ARENA()
									RETURN "LBD_AP"
								ENDIF
								
								RETURN "LBD_XP"
							
							CASE 5 RETURN "LBD_CASH"
							CASE 6
								IF REPLACE_BETTING_COLUMN()
									IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_THUMB_VOTING_ALLOWED)
										IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_VOTED_UP)
										OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_VOTED_DOWN)
										OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_NO_VOTE)
											RETURN "LBD_VOTES"
										ELSE
											RETURN "LBD_BET"
										ENDIF
									ELSE
										RETURN "LBD_NXT"		
									ENDIF		
								ELSE
									RETURN "LBD_BET"
								ENDIF	
							BREAK
						ENDSWITCH
					BREAK
				ENDSWITCH
			BREAK
			
			CASE SUB_DM_UPDATE
				SWITCH iColumnPriority
					CASE 1 RETURN "LBD_SCO"
					CASE 2 RETURN "LBD_KIL"
					CASE 3 RETURN "LBD_DTH"
					CASE 4 RETURN "LBD_RAT"
					CASE 5 RETURN "LBD_XP"
					CASE 6 RETURN "LBD_CASH"
				ENDSWITCH
			BREAK
			
			CASE SUB_MISSIONS_CTF
				IF SHOW_SCORE_COLUMN_ON_LEADERBOARD()
					SWITCH iNumColumns
						CASE 6
							SWITCH iColumnPriority
								CASE 1 
									IF DRAW_CUSTOM_TITLE() 
										RETURN CUSTOM_COLUMN_TITLE(g_FMMC_STRUCT.tl63PointsTitle) 
									ELSE
										RETURN "LBD_SCO"
									ENDIF
								BREAK
								CASE 2 RETURN "LBD_KIL"
								CASE 3 RETURN "LBD_DTH"
								CASE 4 RETURN "LBD_XP"
								CASE 5 RETURN "LBD_CASH"
								CASE 6
									IF IS_ROUNDS_MISSION_LBD(eSubMode)
										IF IS_SCREEN_SMALL()
											RETURN "LBD_RND_SM"
										ELSE
											RETURN "LBD_RND"
										ENDIF
									ELSE
										IF REPLACE_BETTING_COLUMN()
											IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_THUMB_VOTING_ALLOWED)
												IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_VOTED_UP)
												OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_VOTED_DOWN)
												OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_NO_VOTE)
													RETURN "LBD_VOTES"
												ELSE
													RETURN "LBD_BET"
												ENDIF
											ELSE
												RETURN "LBD_NXT"		
											ENDIF		
										ELSE
											RETURN "LBD_BET"
										ENDIF	
									ENDIF
								BREAK
							ENDSWITCH
						BREAK
					ENDSWITCH
				ELSE
					SWITCH iNumColumns
						CASE 5
							SWITCH iColumnPriority
								CASE 1 RETURN "LBD_KIL"
								CASE 2 RETURN "LBD_DTH"
								CASE 3 RETURN "LBD_XP"
								CASE 4 RETURN "LBD_CASH"
								CASE 5
									IF IS_ROUNDS_MISSION_LBD(eSubMode)
										IF IS_SCREEN_SMALL()
											RETURN "LBD_RND_SM"
										ELSE
											RETURN "LBD_RND"
										ENDIF
									ELSE
										IF REPLACE_BETTING_COLUMN()
										OR IS_PLAYER_ON_A_PLAYLIST(PLAYER_ID())
											IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_THUMB_VOTING_ALLOWED)
											AND HAS_VOTED()
												RETURN "LBD_VOTES"
											ELSE
												RETURN "LBD_NXT"
											ENDIF	
										ELSE
											RETURN "LBD_BET"
										ENDIF	
									ENDIF
								BREAK
							ENDSWITCH
						BREAK
					ENDSWITCH
				ENDIF
			BREAK
			
			CASE SUB_SHOOT_RANGE
				SWITCH iColumnPriority
					CASE 1 RETURN "LBD_SCO"
					CASE 2 RETURN "LBD_FIRE"
					CASE 3 RETURN "LBD_GHIT"
					CASE 4 RETURN "LBD_ACC"
					BREAK
				ENDSWITCH
			BREAK
			
			CASE SUB_MISSIONS_NORMAL
				SWITCH iColumnPriority
					CASE 1 RETURN "LBD_SCO"
					CASE 2 RETURN "LBD_KIL"
					CASE 3 RETURN "LBD_DTH"
					CASE 4 RETURN "LBD_RAT"
					BREAK
				ENDSWITCH
			BREAK
			
			CASE SUB_MISSIONS_TIMED
				SWITCH iColumnPriority
					CASE 1 
						IF IS_SCREEN_SMALL()
							RETURN "LBD_TIME_SM"
						ELSE
							RETURN "LBD_TIME"
						ENDIF
					BREAK
					CASE 2 RETURN "LBD_KIL"
					CASE 3 RETURN "LBD_DTH"
					CASE 4 RETURN "LBD_RAT"	
					BREAK
				ENDSWITCH
			BREAK
			
			CASE SUB_FINAL_ROUNDS
				IF SHOW_SCORE_COLUMN_ON_LEADERBOARD()
					SWITCH iColumnPriority
						CASE 1 
							IF DRAW_CUSTOM_TITLE() 
								RETURN CUSTOM_COLUMN_TITLE(g_FMMC_STRUCT.tl63PointsTitle) 
							ELSE
							
								RETURN "LBD_SCO" 
							ENDIF
						BREAK
						CASE 2 
							IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_STOCKPILE(g_FMMC_STRUCT.iAdversaryModeType)
								RETURN "LDB_DEL"
							ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_ENABLE_SHOWDOWN_POINTS)
								RETURN "LDB_SDELM"
							ELSE
								RETURN "LBD_KIL"
							ENDIF
						BREAK
						CASE 3 
							IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_STOCKPILE(g_FMMC_STRUCT.iAdversaryModeType)
								RETURN "LBD_KIL"
							ELSE
								RETURN "LBD_DTH"
							ENDIF
						BREAK
						CASE 4 RETURN "LBD_XP"
						CASE 5 RETURN "LBD_CASH"
						
						CASE 6 	
							IF IS_SCREEN_SMALL()
								RETURN "LBD_RND_SM"
							ELSE
								RETURN "LBD_RND"
							ENDIF
						BREAK
					ENDSWITCH
				ELSE
					SWITCH iColumnPriority
						CASE 1 
							IF DRAW_CUSTOM_TITLE() 
								RETURN CUSTOM_COLUMN_TITLE(g_FMMC_STRUCT.tl63PointsTitle) 
							ELSE
								IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_ENABLE_SHOWDOWN_POINTS)
									RETURN "LDB_SDELM"
								ELSE
									RETURN "LBD_KIL" 
								ENDIF
							ENDIF
						BREAK
						CASE 2 RETURN "LBD_DTH"
						CASE 3 RETURN "LBD_XP"
						CASE 4 RETURN "LBD_CASH"
						
						CASE 5 
							IF IS_SCREEN_SMALL()
								RETURN "LBD_RND_SM"
							ELSE
								RETURN "LBD_RND"
							ENDIF
						BREAK
					ENDSWITCH
				ENDIF
			BREAK
			
			CASE SUB_MISSIONS_LTS
				SWITCH iColumnPriority
					CASE 1 
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_ENABLE_SHOWDOWN_POINTS)
							RETURN "LDB_SDELM"
						ELSE
							RETURN "LBD_KIL"
						ENDIF
					BREAK
					CASE 2 RETURN "LBD_DTH"
					CASE 3 RETURN "LBD_XP"
					CASE 4 RETURN "LBD_CASH"
					CASE 5	
						IF IS_PLAYLIST_ROUNDS_MISSION(eSubMode)
							
							IF IS_SCREEN_SMALL()
								RETURN "LBD_RND_SM"
							ELSE
								RETURN "LBD_RND"
							ENDIF
							
						ELSE
							IF g_FinalRoundsLbd
								RETURN "LBD_BET"
							ELSE
								IF IS_ROUNDS_MISSION_LBD(eSubMode)
									IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_THUMB_VOTING_ALLOWED)	
										IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_VOTED_UP)
										OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_VOTED_DOWN)
										OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_NO_VOTE)
											RETURN "LBD_VOTES"
										ELSE
											RETURN "LBD_NXT"
										ENDIF
									ELSE
										RETURN "LBD_NXT"		
									ENDIF
								ELSE
									IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_THUMB_VOTING_ALLOWED)	
										IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_VOTED_UP)
										OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_VOTED_DOWN)
										OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_NO_VOTE)
											RETURN "LBD_VOTES"
										ELSE
											RETURN "LBD_BET"
										ENDIF
									ELSE
										RETURN "LBD_NXT"		
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					BREAK
					// "ROUNDS WON of ~1~"
					CASE 6 
						IF IS_SCREEN_SMALL()
							RETURN "LBD_RND_SM"
						ELSE
							RETURN "LBD_RND"
						ENDIF
					BREAK
				ENDSWITCH
			BREAK
			
			CASE SUB_MISSIONS_HEIST
				SWITCH iColumnPriority
					CASE 1 RETURN "LBD_KIL"
					CASE 2 RETURN "LBD_XP"
					CASE 3 RETURN "LBD_CASH"
					CASE 4 RETURN "LBD_NXT"
					CASE 5 
						IF IS_SCREEN_SMALL()
							RETURN "LBD_RND_SM"
						ELSE
							RETURN "LBD_RND"
						ENDIF
					BREAK
				ENDSWITCH
			BREAK
			
			CASE SUB_MISSIONS_VERSUS
				SWITCH iColumnPriority
					CASE 1 
						// Force column heading if option to display time instead of checkpoints is set, and Vs mode is Arena Trials
						IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_ARENA_TRIALS(g_FMMC_STRUCT.iAdversaryModeType) AND SHOW_TIME_COLLECTED_INSTEAD_OF_SCORE()
							RETURN "LBD_TIMEGAIN"
						ENDIF
						IF DRAW_CUSTOM_TITLE() 
							RETURN CUSTOM_COLUMN_TITLE(g_FMMC_STRUCT.tl63PointsTitle) 		
						ELSE
							IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_RESURRECTION(g_FMMC_STRUCT.iAdversaryModeType)
								RETURN "LBD_RES"
							ELIF IS_DOUBLE_DOWN_LEADERBOARD()
								RETURN "TIM_TIMER"
							ELSE
								RETURN "LBD_SCO" 
							ENDIF
						ENDIF
					BREAK			
					CASE 2 
						IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_STOCKPILE(g_FMMC_STRUCT.iAdversaryModeType)
							RETURN "LDB_DEL"
						ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_ENABLE_SHOWDOWN_POINTS)
							RETURN "LDB_SDELM"
						ELSE
							RETURN "LBD_KIL"
						ENDIF
					BREAK
					CASE 3 
						IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_STOCKPILE(g_FMMC_STRUCT.iAdversaryModeType)
							RETURN "LBD_KIL"
						ELSE
							RETURN "LBD_DTH"
						ENDIF
					BREAK
					CASE 4 RETURN "LBD_XP"
					CASE 5 RETURN "LBD_CASH"
					CASE 6 
						IF IS_ROUNDS_MISSION_LBD(eSubMode)
							IF IS_SCREEN_SMALL()
								RETURN "LBD_RND_SM"
							ELSE
								RETURN "LBD_RND"
							ENDIF
						ELSE
							IF REPLACE_BETTING_COLUMN()
							OR IS_PLAYER_ON_A_PLAYLIST(PLAYER_ID())
								IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_THUMB_VOTING_ALLOWED)
								AND HAS_VOTED()
									RETURN "LBD_VOTES"
								ELSE
									RETURN "LBD_NXT"
								ENDIF	
							ELSE
								RETURN "LBD_BET"
							ENDIF	
						ENDIF
					BREAK
				ENDSWITCH
			BREAK
			
			CASE SUB_MISSIONS_NO_HUD
				SWITCH iColumnPriority
					CASE 1 
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_ENABLE_SHOWDOWN_POINTS)
							RETURN "LDB_SDELM"
						ELSE
							RETURN "LBD_KIL"
						ENDIF
						
						BREAK
					CASE 2 RETURN "LBD_DTH"
					CASE 3 RETURN "LBD_XP"
					CASE 4 RETURN "LBD_CASH"
					CASE 5 
						IF IS_PLAYLIST_ROUNDS_MISSION(eSubMode)
						OR IS_ROUNDS_MISSION_LBD(eSubMode)
						
							IF IS_SCREEN_SMALL()
								RETURN "LBD_RND_SM"
							ELSE
								RETURN "LBD_RND"
							ENDIF	
						ELSE
							RETURN "LBD_NXT"
						ENDIF
					BREAK
					CASE 6 
						IF IS_SCREEN_SMALL()
							RETURN "LBD_RND_SM"
						ELSE
							RETURN "LBD_RND"
						ENDIF
					BREAK
				ENDSWITCH
			BREAK
			
			CASE SUB_PLAYLIST	
				SWITCH iNumColumns
					CASE 6
						SWITCH iColumnPriority
							CASE 1 RETURN "LBD_PLYL"
							CASE 2 RETURN "LBD_PLYL"
							CASE 3 RETURN "LBD_PLYL"
							CASE 4 RETURN "LBD_PLYL"
							CASE 5 RETURN "LBD_OVE"
							CASE 6 RETURN "LBD_NXT"
							BREAK
						ENDSWITCH
					BREAK
					
					CASE 5
						SWITCH iColumnPriority
							CASE 1 RETURN "LBD_PLYL"
							CASE 2 RETURN "LBD_PLYL"
							CASE 3 RETURN "LBD_PLYL"
							CASE 4 RETURN "LBD_OVE"
							CASE 5 RETURN "LBD_NXT"
							BREAK
						ENDSWITCH
					BREAK
					
					CASE 4
						SWITCH iColumnPriority
							CASE 1 RETURN "LBD_PLYL"
							CASE 2 RETURN "LBD_PLYL"
							CASE 3 RETURN "LBD_OVE"
							CASE 4 RETURN "LBD_NXT"
							BREAK
						ENDSWITCH
					BREAK
					
					CASE 3
						SWITCH iColumnPriority
							CASE 1 RETURN "LBD_PLYL"
							CASE 2 RETURN "LBD_OVE"
							CASE 3 RETURN "LBD_NXT"
							BREAK
						ENDSWITCH
					BREAK
				ENDSWITCH
			BREAK
			
			CASE SUB_FREEMODE
			CASE SUB_ARCADE_CNC
				SWITCH iColumnPriority
					CASE 1 RETURN "LBD_CASH"
				ENDSWITCH
			BREAK
			
			CASE SUB_KART
				SWITCH iColumnPriority
					CASE 1 RETURN "LBD_KIL"
					CASE 2 RETURN "LBD_DTH"
					CASE 3 RETURN "LBD_CASH"
				ENDSWITCH
			BREAK
			
			CASE SUB_HORDE
				SWITCH iColumnPriority
					CASE 1 RETURN "LBD_KIL"
					CASE 2 RETURN "LBD_DTH"
					CASE 3 RETURN "LBD_XP"
				ENDSWITCH
			BREAK
			
		ENDSWITCH
	ENDIF
	
	RETURN ""
ENDFUNC

PROC FILL_COLUMN_TITLES(LBD_VARS& lbdVars, LEADERBOARD_PLACEMENT_TOOLS& Placement, LBD_SUB_MODE eSubMode, INT iNumColumns)
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_FADE)
	
	IF eSubMode = SUB_PLAYLIST
		SWITCH iNumColumns
			CASE 6
				DRAW_A_TITLE(lbdVars, Placement,  1, iNumColumns, GET_LBD_COLUMN_TITLE(lbdVars, eSubMode, iNumColumns, 1), TRUE, 1, TRUE)
				DRAW_A_TITLE(lbdVars, Placement,  2, iNumColumns, GET_LBD_COLUMN_TITLE(lbdVars, eSubMode, iNumColumns, 2), TRUE, 2, TRUE)
				DRAW_A_TITLE(lbdVars, Placement,  3, iNumColumns, GET_LBD_COLUMN_TITLE(lbdVars, eSubMode, iNumColumns, 3), TRUE, 3, TRUE)
				DRAW_A_TITLE(lbdVars, Placement,  4, iNumColumns, GET_LBD_COLUMN_TITLE(lbdVars, eSubMode, iNumColumns, 4), TRUE, 4, TRUE)
				DRAW_A_TITLE(lbdVars, Placement,  5, iNumColumns, GET_LBD_COLUMN_TITLE(lbdVars, eSubMode, iNumColumns, 5), DEFAULT, DEFAULT, TRUE)		
				DRAW_A_TITLE(lbdVars, Placement,  6, iNumColumns, GET_LBD_COLUMN_TITLE(lbdVars, eSubMode, iNumColumns, 6), DEFAULT, DEFAULT, TRUE)		
			BREAK
			
			CASE 5
				DRAW_A_TITLE(lbdVars, Placement,  1, iNumColumns, GET_LBD_COLUMN_TITLE(lbdVars, eSubMode, iNumColumns, 1), TRUE, 1, TRUE)
				DRAW_A_TITLE(lbdVars, Placement,  2, iNumColumns, GET_LBD_COLUMN_TITLE(lbdVars, eSubMode, iNumColumns, 2), TRUE, 2, TRUE)
				DRAW_A_TITLE(lbdVars, Placement,  3, iNumColumns, GET_LBD_COLUMN_TITLE(lbdVars, eSubMode, iNumColumns, 3), TRUE, 3, TRUE)
				DRAW_A_TITLE(lbdVars, Placement,  4, iNumColumns, GET_LBD_COLUMN_TITLE(lbdVars, eSubMode, iNumColumns, 4), DEFAULT, DEFAULT, TRUE)	
				DRAW_A_TITLE(lbdVars, Placement,  5, iNumColumns, GET_LBD_COLUMN_TITLE(lbdVars, eSubMode, iNumColumns, 5), DEFAULT, DEFAULT, TRUE)
			BREAK																					 
																									 
			CASE 4
				DRAW_A_TITLE(lbdVars, Placement,  1, iNumColumns, GET_LBD_COLUMN_TITLE(lbdVars, eSubMode, iNumColumns, 1), TRUE, 1, TRUE)
				DRAW_A_TITLE(lbdVars, Placement,  2, iNumColumns, GET_LBD_COLUMN_TITLE(lbdVars, eSubMode, iNumColumns, 2), TRUE, 2, TRUE)
				DRAW_A_TITLE(lbdVars, Placement,  3, iNumColumns, GET_LBD_COLUMN_TITLE(lbdVars, eSubMode, iNumColumns, 3), DEFAULT, DEFAULT, TRUE)
				DRAW_A_TITLE(lbdVars, Placement,  4, iNumColumns, GET_LBD_COLUMN_TITLE(lbdVars, eSubMode, iNumColumns, 4), DEFAULT, DEFAULT, TRUE)
			BREAK
			
			CASE 3																					 
				DRAW_A_TITLE(lbdVars,Placement,  1, iNumColumns, GET_LBD_COLUMN_TITLE(lbdVars, eSubMode, iNumColumns, 1), TRUE, 1, TRUE)
				DRAW_A_TITLE(lbdVars,Placement,  2, iNumColumns, GET_LBD_COLUMN_TITLE(lbdVars, eSubMode, iNumColumns, 2), DEFAULT, DEFAULT, TRUE)
				DRAW_A_TITLE(lbdVars,Placement,  3, iNumColumns, GET_LBD_COLUMN_TITLE(lbdVars, eSubMode, iNumColumns, 3), DEFAULT, DEFAULT, TRUE)
			BREAK
			
		ENDSWITCH
	ELSE
		SWITCH iNumColumns
			CASE 6
				DRAW_A_TITLE(lbdVars, Placement,  1, iNumColumns, GET_LBD_COLUMN_TITLE(lbdVars, eSubMode, iNumColumns, 1))
				DRAW_A_TITLE(lbdVars, Placement,  2, iNumColumns, GET_LBD_COLUMN_TITLE(lbdVars, eSubMode, iNumColumns, 2))
				DRAW_A_TITLE(lbdVars, Placement,  3, iNumColumns, GET_LBD_COLUMN_TITLE(lbdVars, eSubMode, iNumColumns, 3))
				DRAW_A_TITLE(lbdVars, Placement,  4, iNumColumns, GET_LBD_COLUMN_TITLE(lbdVars, eSubMode, iNumColumns, 4))
				DRAW_A_TITLE(lbdVars, Placement,  5, iNumColumns, GET_LBD_COLUMN_TITLE(lbdVars, eSubMode, iNumColumns, 5))
				DRAW_A_TITLE(lbdVars, Placement,  6, iNumColumns, GET_LBD_COLUMN_TITLE(lbdVars, eSubMode, iNumColumns, 6))//, FALSE, 0, FALSE, IS_ROUNDS_MISSION_LBD(lbdVars, eSubMode))
			BREAK																					 
																									 
			CASE 5																					 
				DRAW_A_TITLE(lbdVars,Placement,  1, iNumColumns, GET_LBD_COLUMN_TITLE(lbdVars, eSubMode, iNumColumns, 1))
				DRAW_A_TITLE(lbdVars,Placement,  2, iNumColumns, GET_LBD_COLUMN_TITLE(lbdVars, eSubMode, iNumColumns, 2))
				DRAW_A_TITLE(lbdVars,Placement,  3, iNumColumns, GET_LBD_COLUMN_TITLE(lbdVars, eSubMode, iNumColumns, 3))
				DRAW_A_TITLE(lbdVars,Placement,  4, iNumColumns, GET_LBD_COLUMN_TITLE(lbdVars, eSubMode, iNumColumns, 4))
				DRAW_A_TITLE(lbdVars,Placement,  5, iNumColumns, GET_LBD_COLUMN_TITLE(lbdVars, eSubMode, iNumColumns, 5))
			BREAK																					 
																									 
			CASE 4																					 
				DRAW_A_TITLE(lbdVars,Placement,  1, iNumColumns, GET_LBD_COLUMN_TITLE(lbdVars, eSubMode, iNumColumns, 1))
				DRAW_A_TITLE(lbdVars,Placement,  2, iNumColumns, GET_LBD_COLUMN_TITLE(lbdVars, eSubMode, iNumColumns, 2))
				DRAW_A_TITLE(lbdVars,Placement,  3, iNumColumns, GET_LBD_COLUMN_TITLE(lbdVars, eSubMode, iNumColumns, 3))
				DRAW_A_TITLE(lbdVars,Placement,  4, iNumColumns, GET_LBD_COLUMN_TITLE(lbdVars, eSubMode, iNumColumns, 4))
			BREAK																					 
																									 
			CASE 3																					 
				DRAW_A_TITLE(lbdVars,Placement,  1, iNumColumns, GET_LBD_COLUMN_TITLE(lbdVars, eSubMode, iNumColumns, 1))
				DRAW_A_TITLE(lbdVars,Placement,  2, iNumColumns, GET_LBD_COLUMN_TITLE(lbdVars, eSubMode, iNumColumns, 2))
				DRAW_A_TITLE(lbdVars,Placement,  3, iNumColumns, GET_LBD_COLUMN_TITLE(lbdVars, eSubMode, iNumColumns, 3))
			BREAK																					 
																									 
			CASE 2																					 
				DRAW_A_TITLE(lbdVars,Placement,  1, iNumColumns, GET_LBD_COLUMN_TITLE(lbdVars, eSubMode, iNumColumns, 1))
				DRAW_A_TITLE(lbdVars,Placement,  2, iNumColumns, GET_LBD_COLUMN_TITLE(lbdVars, eSubMode, iNumColumns, 2))
			BREAK																					 
																									 
			CASE 1			
				DRAW_A_TITLE(lbdVars, Placement,  1, iNumColumns, GET_LBD_COLUMN_TITLE(lbdVars, eSubMode, iNumColumns, 1))
			BREAK
		ENDSWITCH
	ENDIF
	
	// POSITION
	SET_TEXT_CENTRE(TRUE)
	Placement.TextPlacement[TEXT_PLACEMENT_COLUMN_TITLE].x = (LBD_TEXT_X )
	Placement.TextPlacement[TEXT_PLACEMENT_COLUMN_TITLE].y = (LBD_TEXT_Y )
	
	IF IS_ON_RALLY_RACE_GLOBAL_SET()
		DRAW_TEXT(Placement.TextPlacement[TEXT_PLACEMENT_COLUMN_TITLE], Placement.aStyle.TS_LBD, "LBD_TEAM")
	ELSE
		DRAW_TEXT(Placement.TextPlacement[TEXT_PLACEMENT_COLUMN_TITLE], Placement.aStyle.TS_LBD, "LBD_POS")
	ENDIF
	
ENDPROC

FUNC BOOL IS_PLAYER_USING_CUSTOM_VEHICLE_NAME(INT iParticipant)

	IF CONTENT_IS_USING_ARENA()
		RETURN IS_BIT_SET(g_sMC_serverBDEndJob.iCustomVehicleNameBS,iParticipant)
	ENDIF

	RETURN FALSE

ENDFUNC

FUNC TEXT_LABEL_15 GET_VEHICLE_NAME_TEXT_LABEL_FOR_LEADERBOARD_DISPLAY(STRING sName)

	TEXT_LABEL_15 tLabel = sName
	
	IF ARE_STRINGS_EQUAL(sName, "BRUISER")			tLabel = "BRUISER_S"		ENDIF
	IF ARE_STRINGS_EQUAL(sName, "BRUTUS")			tLabel = "BRUTUS_S"			ENDIF
	IF ARE_STRINGS_EQUAL(sName, "CERBERUS")			tLabel = "CERBERUS_S"		ENDIF
	IF ARE_STRINGS_EQUAL(sName, "DEATHBIKE")		tLabel = "DEATHBIKE_S"		ENDIF
	IF ARE_STRINGS_EQUAL(sName, "DOMINATOR4")		tLabel = "DOMINATOR4_S"		ENDIF
	IF ARE_STRINGS_EQUAL(sName, "IMPALER2")			tLabel = "IMPALER2_S"		ENDIF
	IF ARE_STRINGS_EQUAL(sName, "IMPERATOR")		tLabel = "IMPERATOR_S"		ENDIF
	IF ARE_STRINGS_EQUAL(sName, "ISSI4")			tLabel = "ISSI4_S"			ENDIF
	IF ARE_STRINGS_EQUAL(sName, "MONSTER3")			tLabel = "MONSTER3_S"		ENDIF
	IF ARE_STRINGS_EQUAL(sName, "SCARAB")			tLabel = "SCARAB_S"			ENDIF
	IF ARE_STRINGS_EQUAL(sName, "SLAMVAN4")			tLabel = "SLAMVAN4_S"		ENDIF
	IF ARE_STRINGS_EQUAL(sName, "ZR380")			tLabel = "ZR380_S"			ENDIF
	
	IF ARE_STRINGS_EQUAL(sName, "BRUISER2")			tLabel = "BRUISER_S"		ENDIF
	IF ARE_STRINGS_EQUAL(sName, "BRUTUS2")			tLabel = "BRUTUS_S"			ENDIF
	IF ARE_STRINGS_EQUAL(sName, "CERBERUS2")		tLabel = "CERBERUS_S"		ENDIF
	IF ARE_STRINGS_EQUAL(sName, "DEATHBIKE2")		tLabel = "DEATHBIKE_S"		ENDIF
	IF ARE_STRINGS_EQUAL(sName, "DOMINATOR5")		tLabel = "DOMINATOR4_S"		ENDIF
	IF ARE_STRINGS_EQUAL(sName, "IMPALER3")			tLabel = "IMPALER2_S"		ENDIF
	IF ARE_STRINGS_EQUAL(sName, "IMPERATOR2")		tLabel = "IMPERATOR_S"		ENDIF
	IF ARE_STRINGS_EQUAL(sName, "ISSI5")			tLabel = "ISSI4_S"			ENDIF
	IF ARE_STRINGS_EQUAL(sName, "MONSTER4")			tLabel = "MONSTER3_S"		ENDIF
	IF ARE_STRINGS_EQUAL(sName, "SCARAB2")			tLabel = "SCARAB_S"			ENDIF
	IF ARE_STRINGS_EQUAL(sName, "SLAMVAN5")			tLabel = "SLAMVAN4_S"		ENDIF
	IF ARE_STRINGS_EQUAL(sName, "ZR3802")			tLabel = "ZR380_S"			ENDIF
	
	IF ARE_STRINGS_EQUAL(sName, "BRUISER3")			tLabel = "BRUISER_S"		ENDIF
	IF ARE_STRINGS_EQUAL(sName, "BRUTUS3")			tLabel = "BRUTUS_S"			ENDIF
	IF ARE_STRINGS_EQUAL(sName, "CERBERUS3")		tLabel = "CERBERUS_S"		ENDIF
	IF ARE_STRINGS_EQUAL(sName, "DEATHBIKE3")		tLabel = "DEATHBIKE_S"		ENDIF
	IF ARE_STRINGS_EQUAL(sName, "DOMINATOR6")		tLabel = "DOMINATOR4_S"		ENDIF
	IF ARE_STRINGS_EQUAL(sName, "IMPALER4")			tLabel = "IMPALER2_S"		ENDIF
	IF ARE_STRINGS_EQUAL(sName, "IMPERATOR3")		tLabel = "IMPERATOR_S"		ENDIF
	IF ARE_STRINGS_EQUAL(sName, "ISSI6")			tLabel = "ISSI4_S"			ENDIF
	IF ARE_STRINGS_EQUAL(sName, "MONSTER5")			tLabel = "MONSTER3_S"		ENDIF
	IF ARE_STRINGS_EQUAL(sName, "SCARAB3")			tLabel = "SCARAB_S"			ENDIF
	IF ARE_STRINGS_EQUAL(sName, "SLAMVAN6")			tLabel = "SLAMVAN4_S"		ENDIF
	IF ARE_STRINGS_EQUAL(sName, "ZR3803")			tLabel = "ZR380_S"			ENDIF
	
	RETURN tLabel

ENDFUNC

PROC DISPLAY_TEXT_WITH_VEHICLE_NAME(FLOAT PosX, FLOAT PosY, STRING TextLabel, STRING PlayerName, HUD_COLOURS Colour)
	
	BEGIN_TEXT_COMMAND_DISPLAY_TEXT(TextLabel)	
		IF NOT (Colour = HUD_COLOUR_PURE_WHITE)
			SET_COLOUR_OF_NEXT_TEXT_COMPONENT(Colour)
		ENDIF
		ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY(PlayerName)
	END_TEXT_COMMAND_DISPLAY_TEXT(PosX, PosY)
ENDPROC

FUNC BOOL LBD_DISPLAY_QUICK_RESTART(LBD_SUB_MODE eSubMode)

	IF NOT SHOULD_SHOW_QUICK_RESTART_OPTION()
		RETURN FALSE
	ENDIF	

	IF eSubMode = SUB_MISSIONS_HEIST  
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_DefaultForceQuickRestart) 
		RETURN TRUE
	ENDIF
	
	IF IS_LOADED_MISSION_LOW_RIDER_FLOW_MISSION() 
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC DRAW_A_COLUMN(LBD_SUB_MODE eSubMode, LBD_VARS& lbdVars, LEADERBOARD_PLACEMENT_TOOLS& Placement, LBD_VAR_TYPE VarType, LBD_VOTE eVote, FLOAT fVariable, INT iPriority, INT iVariable, INT iNumColumns, INT iParticipant, INT iRow, BOOL bLamarRow = FALSE, BOOL bRoundsColumn = FALSE)

	INT iPlacement
	INT iNumVehNameCharacters
	TEXT_LABEL_63 tlFullCarName
	TEXT_LABEL_63 tlShortenedCarName
	INT iEndPoint
	BOOL bShorten = FALSE
	BOOL bCustomName = FALSE
	HUD_COLOURS hcVehNameColour
	
	TEXT_LABEL_63 tl63Vote

	SWITCH iPriority
		CASE 1 iPlacement = TEXT_PLACEMENT_COLUMN1 BREAK
		CASE 2 iPlacement = TEXT_PLACEMENT_COLUMN2 BREAK
		CASE 3 iPlacement = TEXT_PLACEMENT_COLUMN3 BREAK
		CASE 4 iPlacement = TEXT_PLACEMENT_COLUMN4 BREAK
		CASE 5 iPlacement = TEXT_PLACEMENT_COLUMN5 BREAK
		CASE 6 iPlacement = TEXT_PLACEMENT_COLUMN6 BREAK
	ENDSWITCH
	
	Placement.TextPlacement[iPlacement].x = GET_COLUMN_POS(iNumColumns, iPriority, eSubMode = SUB_PLAYLIST) 
	Placement.TextPlacement[iPlacement].y = VARIABLE_BASE_Y+(TO_FLOAT(iRow) * RECT_Y_OFFSET)	
	SET_STANDARD_LBD_TEXT(Placement.aStyle.TS_LBD)
	Placement.aStyle.TS_LBD.aFont = FONT_STANDARD
	IF VarType <> VAR_TYPE_STRING
		SET_TEXT_STYLE(Placement.aStyle.TS_LBD)
	ENDIF
	IF iParticipant = -1
		SET_TEXT_WHITE(Placement.aStyle.TS_LBD)
	ELSE
		COLOUR_LINE_OF_TEXT(Placement, iParticipant)
	ENDIF
	SET_TEXT_CENTRE(TRUE)
	
	SWITCH VarType
		CASE VAR_TYPE_TIME
			// Hide row (1394095)
			IF NOT lbdVars.bHideMyRow 
				IF iVariable > 0
					IF iVariable >= ONE_HOUR_MILLISECONDS								
						DRAW_TEXT_TIMER(Placement.TextPlacement[iPlacement], Placement.aStyle.TS_LBD, iVariable, TIME_FORMAT_HOURS|TIME_FORMAT_MINUTES|TIME_FORMAT_SECONDS)
					ELSE		
						DRAW_TEXT_TIMER(Placement.TextPlacement[iPlacement], Placement.aStyle.TS_LBD, iVariable, TIME_FORMAT_MINUTES|TIME_FORMAT_SECONDS|TIME_FORMAT_MILLISECONDS|TEXT_FORMAT_USE_DOT_FOR_MILLISECOND_DIVIDER)	
					ENDIF
				ELIF iVariable = -1
					DRAW_TEXT(Placement.TextPlacement[iPlacement], Placement.aStyle.TS_LBD, "RACE_LB_DNF")
				ELIF
					iVariable = -2
					DRAW_TEXT(Placement.TextPlacement[iPlacement], Placement.aStyle.TS_LBD, "RACE_LB_WRCK")
				ELSE	
					DRAW_TEXT(Placement.TextPlacement[iPlacement], Placement.aStyle.TS_LBD, "RACE_LB_EMPTY")
				ENDIF
			ENDIF
		BREAK
		CASE VAR_TYPE_INT
			IF iVariable != EMPTY_LB_ENTRY
				IF NOT lbdVars.bHideMyRow 
					IF bRoundsColumn
						DRAW_TEXT_WITH_NUMBER(Placement.TextPlacement[iPlacement], Placement.aStyle.TS_LBD, "NUMBER", iVariable, FONT_CENTRE)
//						DRAW_TEXT_WITH_2_NUMBERS(Placement.TextPlacement[iPlacement], Placement.aStyle.TS_LBD, "LBD_PLYL", iVariable, (g_sTransitionSessionData.sMissionRoundData.iNumberOfRounds + 1), FONT_CENTRE)
					ELSE
						DRAW_TEXT_WITH_NUMBER(Placement.TextPlacement[iPlacement], Placement.aStyle.TS_LBD, "NUMBER", iVariable, FONT_CENTRE)
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE VAR_TYPE_INT_CASH	 
			IF NOT lbdVars.bHideMyRow 
			OR IS_SUBMODE_RACE(eSubMode)
				IF iVariable < 0
					SET_TEXT_RED(Placement.aStyle.TS_LBD)
				ENDIF
				DRAW_TEXT_WITH_NUMBER(Placement.TextPlacement[iPlacement], Placement.aStyle.TS_LBD, "HUD_CASH", iVariable, FONT_CENTRE)
			ENDIF
		BREAK
		CASE VAR_TYPE_STRING
			IF eSubMode = SUB_MISSIONS_HEIST
				Placement.TextPlacement[iPlacement].y = (VARIABLE_BASE_Y + (TO_FLOAT(iRow) * RECT_Y_OFFSET))
				SET_TEXT_STYLE(Placement.aStyle.TS_LBD)
				DISPLAY_TEXT(Placement.TextPlacement[iPlacement].x, Placement.TextPlacement[iPlacement].y, "LBD_LEADER")
				
			ELIF eSubMode = SUB_MISSIONS_VERSUS
				PRINTLN("[2676798] DISPLAY_TEXT = ") 
				Placement.TextPlacement[iPlacement].y = (VARIABLE_BASE_Y + (TO_FLOAT(iRow) * RECT_Y_OFFSET))
				SET_TEXT_STYLE(Placement.aStyle.TS_LBD)
				DISPLAY_TEXT(Placement.TextPlacement[iPlacement].x, Placement.TextPlacement[iPlacement].y, "LBD_DASH")
			ELIF eSubMode <> SUB_RACE_FOOT
				IF NOT lbdVars.bHideMyRow 
				
					INT iPlayerId
					iPlayerId = -1
					
					IF iParticipant != -1 
					AND NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
						iPlayerId = NATIVE_TO_INT(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant)))
					ENDIF
				
					IF CONTENT_IS_USING_ARENA()
						IF iPlayerId != -1
							PRINTLN("[1876225] Player ",iPlayerId," is playing using language ",GlobalplayerBD_FM_3[iPlayerId].sMagnateGangBossData.eLanguage) 
							IF GlobalplayerBD_FM_3[iPlayerId].sMagnateGangBossData.eLanguage = LANGUAGE_ENGLISH
								bCustomName = IS_PLAYER_USING_CUSTOM_VEHICLE_NAME(iParticipant)
								
								PRINTLN("[1876225] CONTENT_IS_USING_ARENA - bCustomName: ", bCustomName)
								
								IF IS_PLAYER_USING_CUSTOM_VEHICLE_NAME(iParticipant)
									lbdVars.tlRaceVeh = g_tlRaceVeh[iParticipant]
									PRINTLN("[1876225] CONTENT_IS_USING_ARENA - using g_tlRaceVeh") 
								ENDIF								
							ELSE
								PRINTLN("[1876225] Player ",iPlayerId," is playing in different language, not using custom vehicle name.") 
							ENDIF
						ELSE
							PRINTLN("[1876225] Player ",iPlayerId," is invalid.") 
						ENDIF
					ENDIF

					PRINTLN("[1876225] tlRaceVeh = ", lbdVars.tlRaceVeh) 
			
					IF NOT bCustomName					
						IF NOT IS_STRING_NULL_OR_EMPTY(lbdVars.tlRaceVeh)
							TEXT_LABEL_15 tlLdbVehName
							tlLdbVehName =  GET_VEHICLE_NAME_TEXT_LABEL_FOR_LEADERBOARD_DISPLAY(lbdVars.tlRaceVeh)
							tlFullCarName = GET_FILENAME_FOR_AUDIO_CONVERSATION(tlLdbVehName)
						ENDIF
					ELSE
						tlFullCarName = lbdVars.tlRaceVeh
					ENDIF
					
					PRINTLN("[1876225] tlFullCarName = ", tlFullCarName) 
					
					iEndPoint = GET_VEH_CHAR_LIMIT()
					
					iNumVehNameCharacters = GET_LENGTH_OF_LITERAL_STRING(tlFullCarName)
					IF iNumVehNameCharacters > 0
						IF iEndPoint >= iNumVehNameCharacters
							iEndPoint = iNumVehNameCharacters
						ELSE
							bShorten = TRUE
						ENDIF
						IF bCustomName
							tlShortenedCarName = GET_FIRST_N_CHARACTERS_OF_LITERAL_STRING(tlFullCarName,iEndPoint)
						ELSE
							tlShortenedCarName = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME(tlFullCarName, 0, iEndPoint)
						ENDIF
					ENDIF
					
					Placement.TextPlacement[iPlacement].y = (VARIABLE_BASE_Y + (TO_FLOAT(iRow) * RECT_Y_OFFSET))	
					
					PRINTLN("[1876225] tlShortenedCarName = ", tlShortenedCarName) 
					
					SET_TEXT_STYLE(Placement.aStyle.TS_LBD)	
					hcVehNameColour = HUD_COLOUR_WHITE
					IF NOT bLamarRow
					AND NOT NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
						hcVehNameColour = GET_INACTIVE_COLOUR()
					ENDIF
					
					IF bCustomName
						SET_TEXT_FONT(FONT_CONDENSED)
					ENDIF

					IF bShorten = TRUE
						DISPLAY_TEXT_WITH_VEHICLE_NAME(Placement.TextPlacement[iPlacement].x, Placement.TextPlacement[iPlacement].y, "VEH_NAME_DOTS", tlShortenedCarName, hcVehNameColour)
					ELSE
						DISPLAY_TEXT_WITH_VEHICLE_NAME(Placement.TextPlacement[iPlacement].x, Placement.TextPlacement[iPlacement].y, "STRING", tlShortenedCarName, hcVehNameColour)
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE VAR_TYPE_FLOAT
			IF NOT lbdVars.bHideMyRow 
				INT iDecimalSpaces
				iDecimalSpaces = 2
				IF IS_ARENA_LBD(eSubMode)
					iDecimalSpaces = 0
				ENDIF
				DRAW_TEXT_WITH_FLOAT(Placement.TextPlacement[iPlacement], Placement.aStyle.TS_LBD, "NUMBER", fVariable, iDecimalSpaces, FONT_CENTRE)
			ENDIF
		BREAK
		
		CASE VAR_TYPE_ENUM
		
			INT iPlayerId
			iPlayerId = -1
			
			IF iParticipant != -1 
			AND NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
				iPlayerId = NATIVE_TO_INT(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant)))
			ENDIF
			
			Placement.TextPlacement[iPlacement].y = (VARIABLE_BASE_Y + (TO_FLOAT(iRow) * RECT_Y_OFFSET))

			IF iPlayerID != -1
				IF IS_BIT_SET(lbdVars.iLBDPlayerBitSetHideVote, iPlayerId) 
					// 1979114
					tl63Vote = GET_SHORTENED_TEXT_LABEL("LBD_RESTART", bShorten, GET_VOTE_CHAR_LIMIT())
					DISPLAY_TEXT_WITH_PLAYER_NAME(Placement.TextPlacement[iPlacement].x, Placement.TextPlacement[iPlacement].y, GET_LABEL_IF_SHORTENED(bShorten), tl63Vote, HUD_COLOUR_PURE_WHITE) 
				ELSE
					IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_THUMB_VOTING_ALLOWED)
						IF iParticipant != -1
							IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
								IF IS_BIT_SET(GlobalplayerBD_FM[iPlayerId].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_VOTED_UP)
									PRINTLN("[VAR_TYPE_ENUM]// FMMC_LIKEC")
									tl63Vote = GET_SHORTENED_TEXT_LABEL("FMMC_LIKEC", bShorten, GET_VOTE_CHAR_LIMIT())
									DISPLAY_TEXT_WITH_PLAYER_NAME(Placement.TextPlacement[iPlacement].x, Placement.TextPlacement[iPlacement].y, GET_LABEL_IF_SHORTENED(bShorten), tl63Vote, HUD_COLOUR_PURE_WHITE)
									IF NOT g_FinalRoundsLbd
										g_SwapBettingColumnForVotes = TRUE
									ENDIF
								ELIF IS_BIT_SET(GlobalplayerBD_FM[iPlayerId].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_VOTED_DOWN)
									PRINTLN("[VAR_TYPE_ENUM]// FMMC_DISLIKEC")
									tl63Vote = GET_SHORTENED_TEXT_LABEL("FMMC_DISLIKEC", bShorten, GET_VOTE_CHAR_LIMIT())
									DISPLAY_TEXT_WITH_PLAYER_NAME(Placement.TextPlacement[iPlacement].x, Placement.TextPlacement[iPlacement].y, GET_LABEL_IF_SHORTENED(bShorten), tl63Vote, HUD_COLOUR_PURE_WHITE)
									IF NOT g_FinalRoundsLbd
										g_SwapBettingColumnForVotes = TRUE
									ENDIF
								ELIF IS_BIT_SET(GlobalplayerBD_FM[iPlayerId].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_NO_VOTE)
									PRINTLN("[VAR_TYPE_ENUM]// FMMC_NOVOTEC")
									tl63Vote = GET_SHORTENED_TEXT_LABEL("FMMC_NOVOTEC", bShorten, GET_VOTE_CHAR_LIMIT())
									DISPLAY_TEXT_WITH_PLAYER_NAME(Placement.TextPlacement[iPlacement].x, Placement.TextPlacement[iPlacement].y, GET_LABEL_IF_SHORTENED(bShorten), tl63Vote, HUD_COLOUR_PURE_WHITE)
									IF NOT g_FinalRoundsLbd
										g_SwapBettingColumnForVotes = TRUE
									ENDIF
								ENDIF
									
								IF NOT IS_BIT_SET(GlobalplayerBD_FM[iPlayerId].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_VOTED_UP)
								AND NOT IS_BIT_SET(GlobalplayerBD_FM[iPlayerId].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_VOTED_DOWN)
								AND NOT IS_BIT_SET(GlobalplayerBD_FM[iPlayerId].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_NO_VOTE)
									SWITCH eVote
										CASE LBD_VOTE_RESTART
										CASE LBD_VOTE_RESTART_SWAP_TEAMS
											PRINTLN("[VAR_TYPE_ENUM]// LBD_VOTE_RESTART")
											tl63Vote = GET_SHORTENED_TEXT_LABEL("LBD_RESTART", bShorten, GET_VOTE_CHAR_LIMIT())
											DISPLAY_TEXT_WITH_PLAYER_NAME(Placement.TextPlacement[iPlacement].x, Placement.TextPlacement[iPlacement].y, GET_LABEL_IF_SHORTENED(bShorten), tl63Vote, HUD_COLOUR_PURE_WHITE)
											SET_BIT(lbdVars.iLBDPlayerBitSetHideVote, iPlayerId)
										BREAK
									ENDSWITCH
								ENDIF
							ENDIF
						ENDIF
						
						PRINTLN("[VAR_TYPE_ENUM]// UP ")
					ELSE		
						PRINTLN("VAR_TYPE_ENUM, LBD_DISPLAY_QUICK_RESTART() = ", LBD_DISPLAY_QUICK_RESTART(eSubMode), " eVote = ", ENUM_TO_INT(eVote))
						IF LBD_DISPLAY_QUICK_RESTART(eSubMode)

							SWITCH eVote
							
								CASE LBD_VOTE_EXIT
									SET_TEXT_COLOUR_WITH_HUD_COLOUR(Placement.aStyle.TS_LBD, HUD_COLOUR_PURE_WHITE)
									tl63Vote = GET_SHORTENED_TEXT_LABEL("LBD_EXT", bShorten, GET_VOTE_CHAR_LIMIT())
									DISPLAY_TEXT_WITH_PLAYER_NAME(Placement.TextPlacement[iPlacement].x, Placement.TextPlacement[iPlacement].y, GET_LABEL_IF_SHORTENED(bShorten), tl63Vote, HUD_COLOUR_PURE_WHITE)
								BREAK
								
								CASE LBD_VOTE_RESTART
								CASE LBD_VOTE_RESTART_SWAP_TEAMS
									SET_TEXT_COLOUR_WITH_HUD_COLOUR(Placement.aStyle.TS_LBD, HUD_COLOUR_PURE_WHITE)
									tl63Vote = GET_SHORTENED_TEXT_LABEL("LBD_RESTART", bShorten, GET_VOTE_CHAR_LIMIT())
									DISPLAY_TEXT_WITH_PLAYER_NAME(Placement.TextPlacement[iPlacement].x, Placement.TextPlacement[iPlacement].y, GET_LABEL_IF_SHORTENED(bShorten), tl63Vote, HUD_COLOUR_PURE_WHITE)
								BREAK
								
								CASE LBD_VOTE_CONTINUE
									SET_TEXT_COLOUR_WITH_HUD_COLOUR(Placement.aStyle.TS_LBD, HUD_COLOUR_PURE_WHITE)
									tl63Vote = GET_SHORTENED_TEXT_LABEL("LBD_CONT", bShorten, GET_VOTE_CHAR_LIMIT())
									DISPLAY_TEXT_WITH_PLAYER_NAME(Placement.TextPlacement[iPlacement].x, Placement.TextPlacement[iPlacement].y, GET_LABEL_IF_SHORTENED(bShorten), tl63Vote, HUD_COLOUR_PURE_WHITE)
								BREAK
								
								DEFAULT
									// 2079964
									IF NOT IS_PLAYER_SPECTATING(PLAYER_ID())
									OR g_bMissionEnding
									OR g_bMissionOver
										SET_TEXT_COLOUR_WITH_HUD_COLOUR(Placement.aStyle.TS_LBD, GET_INACTIVE_COLOUR())
										tl63Vote = GET_SHORTENED_TEXT_LABEL("LBD_RESTART", bShorten, GET_VOTE_CHAR_LIMIT())
										DISPLAY_TEXT_WITH_PLAYER_NAME(Placement.TextPlacement[iPlacement].x, Placement.TextPlacement[iPlacement].y, GET_LABEL_IF_SHORTENED(bShorten), tl63Vote, GET_INACTIVE_COLOUR())
									ENDIF
								BREAK

							ENDSWITCH
						ELSE
	
							SWITCH eVote
								CASE LBD_VOTE_RANDOM
									//On a playlist players vote for next
									IF IS_PLAYER_ON_A_PLAYLIST(PLAYER_ID())
										PRINTLN("[VAR_TYPE_ENUM]// LBD_CONT")
										tl63Vote = GET_SHORTENED_TEXT_LABEL("LBD_CONT", bShorten, GET_VOTE_CHAR_LIMIT())
										DISPLAY_TEXT_WITH_PLAYER_NAME(Placement.TextPlacement[iPlacement].x, Placement.TextPlacement[iPlacement].y, GET_LABEL_IF_SHORTENED(bShorten), tl63Vote, HUD_COLOUR_PURE_WHITE)
									ELSE
										// not on a playlist players pick random. 
										PRINTLN("[VAR_TYPE_ENUM]// LBD_RAN")
										tl63Vote = GET_SHORTENED_TEXT_LABEL("LBD_RAN", bShorten, GET_VOTE_CHAR_LIMIT())
										DISPLAY_TEXT_WITH_PLAYER_NAME(Placement.TextPlacement[iPlacement].x, Placement.TextPlacement[iPlacement].y, GET_LABEL_IF_SHORTENED(bShorten), tl63Vote, HUD_COLOUR_PURE_WHITE)
									ENDIF
								BREAK
								CASE LBD_VOTE_CONTINUE
									IF NOT IS_PLAYER_SPECTATING(PLAYER_ID())
									OR g_bMissionOver
										PRINTLN("[VAR_TYPE_ENUM]// LBD_CONT")
										IF Is_Player_Currently_On_MP_Heist(PLAYER_ID()) 
										OR Is_Player_Currently_On_MP_Heist_Planning(PLAYER_ID())	
											tl63Vote = GET_SHORTENED_TEXT_LABEL("FM_NXT_GTA2", bShorten, GET_VOTE_CHAR_LIMIT())
											DISPLAY_TEXT_WITH_PLAYER_NAME(Placement.TextPlacement[iPlacement].x, Placement.TextPlacement[iPlacement].y, GET_LABEL_IF_SHORTENED(bShorten), tl63Vote, HUD_COLOUR_PURE_WHITE)
										ELSE
											tl63Vote = GET_SHORTENED_TEXT_LABEL("LBD_CONT", bShorten, GET_VOTE_CHAR_LIMIT())
											DISPLAY_TEXT_WITH_PLAYER_NAME(Placement.TextPlacement[iPlacement].x, Placement.TextPlacement[iPlacement].y, GET_LABEL_IF_SHORTENED(bShorten), tl63Vote, HUD_COLOUR_PURE_WHITE)
										ENDIF
									ENDIF
								BREAK
								CASE LBD_VOTE_REPLAY	
									PRINTLN("[VAR_TYPE_ENUM]// LBD_REP")
									tl63Vote = GET_SHORTENED_TEXT_LABEL("LBD_RESTART", bShorten, GET_VOTE_CHAR_LIMIT())
									DISPLAY_TEXT_WITH_PLAYER_NAME(Placement.TextPlacement[iPlacement].x, Placement.TextPlacement[iPlacement].y, GET_LABEL_IF_SHORTENED(bShorten), tl63Vote, HUD_COLOUR_PURE_WHITE)
								BREAK
								CASE LBD_VOTE_EXIT
									PRINTLN("[VAR_TYPE_ENUM]// LBD_EXT")
									tl63Vote = GET_SHORTENED_TEXT_LABEL("LBD_EXT", bShorten, GET_VOTE_CHAR_LIMIT())
									DISPLAY_TEXT_WITH_PLAYER_NAME(Placement.TextPlacement[iPlacement].x, Placement.TextPlacement[iPlacement].y, GET_LABEL_IF_SHORTENED(bShorten), tl63Vote, HUD_COLOUR_PURE_WHITE)
								BREAK
								CASE LBD_VOTE_RESTART
								CASE LBD_VOTE_RESTART_SWAP_TEAMS
									PRINTLN("[VAR_TYPE_ENUM]// LBD_RESTART")
									tl63Vote = GET_SHORTENED_TEXT_LABEL("LBD_RESTART", bShorten, GET_VOTE_CHAR_LIMIT())
									DISPLAY_TEXT_WITH_PLAYER_NAME(Placement.TextPlacement[iPlacement].x, Placement.TextPlacement[iPlacement].y, GET_LABEL_IF_SHORTENED(bShorten), tl63Vote, HUD_COLOUR_PURE_WHITE)
								BREAK
							ENDSWITCH

						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

CONST_FLOAT BLOB_H 			0.025
CONST_FLOAT BLOB_W 			0.014
CONST_FLOAT BLOB_X 			0.466
CONST_FLOAT BLOB_Y 			0.231

PROC DRAW_VEHICLE_BLOBS(LBD_VARS& lbdVars, LEADERBOARD_PLACEMENT_TOOLS& Placement, LBD_SUB_MODE eSubMode, INT iRow)
	IF NOT IS_ON_FOOT_RACE_GLOBAL_SET()
		IF NOT lbdVars.bHideMyRow 
			SWITCH eSubMode 
				CASE SUB_RACE_RALLY
				CASE SUB_RACE_GTA
				CASE SUB_RACE_NORMAL
				CASE SUB_RACE_STUNT
				CASE SUB_RACE_AGG_BEST
				CASE SUB_RACE_AGG_TIME
				CASE SUB_RACE_TARGET
					Placement.SpritePlacement[SPRITE_PLACEMENT_VEHICLE_BLOB].x = BLOB_X  
					Placement.SpritePlacement[SPRITE_PLACEMENT_VEHICLE_BLOB].y = BLOB_Y+(TO_FLOAT(iRow) * RECT_Y_OFFSET) 
					Placement.SpritePlacement[SPRITE_PLACEMENT_VEHICLE_BLOB].w = BLOB_W 
					Placement.SpritePlacement[SPRITE_PLACEMENT_VEHICLE_BLOB].h = BLOB_H 
					Placement.SpritePlacement[SPRITE_PLACEMENT_VEHICLE_BLOB].a = 255
					Placement.SpritePlacement[SPRITE_PLACEMENT_VEHICLE_BLOB].r = lbdVars.iVehRed
					Placement.SpritePlacement[SPRITE_PLACEMENT_VEHICLE_BLOB].g = lbdVars.iVehGreen
					Placement.SpritePlacement[SPRITE_PLACEMENT_VEHICLE_BLOB].b = lbdVars.iVehBlue 
					DRAW_2D_SPRITE("MPCarHUD", "Leaderboard_Car_Colour_Icon_SingleColour", Placement.SpritePlacement[SPRITE_PLACEMENT_VEHICLE_BLOB], FALSE, HIGHLIGHT_OPTION_NORMAL, GFX_ORDER_AFTER_FADE)
					SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_FADE)
				BREAK
			ENDSWITCH
		ENDIF
	ENDIF
ENDPROC

FUNC LBD_VOTE GET_VOTE_FOR_PLAYER(PLAYER_INDEX playerId, INT iParticipant)

	IF iParticipant = -1
//		PRINTLN("[VAR_TYPE_ENUM] LBD_VOTE_EXIT")
		RETURN LBD_VOTE_EXIT
	ENDIF
	
	IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
//		PRINTLN("[VAR_TYPE_ENUM] LBD_VOTE_EXIT")
		RETURN LBD_VOTE_EXIT
	ENDIF
	
	IF HAS_PLAYER_VOTED_TO_PLAY_NEXT_MISSION(playerID)
	OR GlobalplayerBD_FM[NATIVE_TO_INT(playerId)].iEOM_voteStatus = ciFMMC_EOM_VOTE_STATUS_RANDOM 

//		PRINTLN("[VAR_TYPE_ENUM] LBD_VOTE_RANDOM")
		RETURN LBD_VOTE_RANDOM
	ENDIF
	
	IF HAS_PLAYER_VOTED_TO_RESTART_MISSION(playerID)
	OR GlobalplayerBD_FM[NATIVE_TO_INT(playerId)].iEOM_voteStatus = ciFMMC_EOM_VOTE_STATUS_RESTART
//		PRINTLN("[VAR_TYPE_ENUM] LBD_VOTE_REPLAY")
		RETURN LBD_VOTE_REPLAY 
	ENDIF
	
	IF HAS_PLAYER_VOTED_TO_CONTINUE(playerID)
	OR GlobalplayerBD_FM[NATIVE_TO_INT(playerId)].iEOM_voteStatus = ciFMMC_EOM_VOTE_STATUS_CONTINUE
	OR IS_PLAYER_READY_FOR_JOB_VOTES(playerID)
//		PRINTLN("[VAR_TYPE_ENUM] LBD_VOTE_CONTINUE")
		RETURN LBD_VOTE_CONTINUE
	ENDIF
	
	IF GlobalplayerBD_FM[NATIVE_TO_INT(playerId)].iEOM_voteStatus = ciFMMC_EOM_VOTE_STATUS_QUICK_RESTART
//		PRINTLN("[VAR_TYPE_ENUM] LBD_VOTE_CONTINUE")
		RETURN LBD_VOTE_RESTART
	ENDIF
	
	IF GlobalplayerBD_FM[NATIVE_TO_INT(playerId)].iEOM_voteStatus = ciFMMC_EOM_VOTE_STATUS_QUICK_RESTART_SWAP
//		PRINTLN("[VAR_TYPE_ENUM] LBD_VOTE_CONTINUE")
		RETURN LBD_VOTE_RESTART_SWAP_TEAMS
	ENDIF

//	PRINTLN("[VAR_TYPE_ENUM] LBD_VOTE_NOT_USED")
	RETURN LBD_VOTE_NOT_USED
ENDFUNC

// Is variable an int or time etc.?
FUNC LBD_VAR_TYPE GET_COLUMN_VAR_TYPE(LBD_VARS& lbdVars, LBD_SUB_MODE eSubMode, INT iColumnPriority, INT iNumColumns)

	IF bIS_JUGGERNAUT()
	AND NOT IS_PLAYLIST_LBD(eSubMode)
		SWITCH iColumnPriority
			CASE 1 RETURN VAR_TYPE_INT 	
			CASE 2 RETURN VAR_TYPE_INT 	
			CASE 3 RETURN VAR_TYPE_INT 	
			CASE 4 RETURN VAR_TYPE_INT 		
			CASE 5 RETURN VAR_TYPE_INT_CASH	
			CASE 6
				IF IS_ROUNDS_MISSION_LBD(eSubMode)
					RETURN VAR_TYPE_INT
				ELSE
					RETURN VAR_TYPE_ENUM
				ENDIF
			BREAK
		ENDSWITCH
	ELIF bIS_TURF_WARS()
	AND NOT IS_PLAYLIST_LBD(eSubMode)
		SWITCH iColumnPriority
			CASE 1 RETURN VAR_TYPE_INT 	
			CASE 2 RETURN VAR_TYPE_INT 	
			CASE 3 RETURN VAR_TYPE_INT 	
			CASE 4 RETURN VAR_TYPE_INT 		
			CASE 5 RETURN VAR_TYPE_INT_CASH	
			CASE 6
				IF IS_ROUNDS_MISSION_LBD(eSubMode)
					RETURN VAR_TYPE_INT
				ELSE
					RETURN VAR_TYPE_ENUM
				ENDIF
			BREAK
		ENDSWITCH
	ELIF CUSTOM_LBD_REMIX()
	AND NOT IS_PLAYLIST_LBD(eSubMode)
		SWITCH iColumnPriority
			CASE 1 RETURN VAR_TYPE_INT 	
			CASE 2 RETURN VAR_TYPE_TIME 	
			CASE 3 RETURN VAR_TYPE_INT 	
			CASE 4 RETURN VAR_TYPE_INT 		
			CASE 5 RETURN VAR_TYPE_INT_CASH	
			CASE 6
				IF IS_ROUNDS_MISSION_LBD(eSubMode)
					RETURN VAR_TYPE_INT
				ELSE
					RETURN VAR_TYPE_ENUM
				ENDIF
			BREAK
		ENDSWITCH
	ELIF SHOW_SLIPSTREAM_COLUMN()
	AND NOT IS_PLAYLIST_LBD(eSubMode)
		SWITCH iColumnPriority
			CASE 1 
				IF lbdVars.eRaceType = eRACE_TYPE_LAPS
					RETURN VAR_TYPE_TIME 		// Best Lap/Checkpoints
				ELSE
					RETURN VAR_TYPE_INT 		// Best Lap/Checkpoints
				ENDIF
			BREAK
			CASE 2 RETURN VAR_TYPE_TIME 	// Slipstream
			CASE 3 RETURN VAR_TYPE_INT 		// xp	
			CASE 4 RETURN VAR_TYPE_INT_CASH	// cash
			CASE 5
				IF IS_ROUNDS_MISSION_LBD(eSubMode)
				
					RETURN VAR_TYPE_INT
				ELSE

					RETURN VAR_TYPE_ENUM
				ENDIF
			BREAK
		ENDSWITCH
	ELIF SHOW_ASSISTS_LEADERBOARD()
	AND NOT IS_PLAYLIST_LBD(eSubMode)
		SWITCH iColumnPriority
			CASE 1 RETURN VAR_TYPE_INT 		// Kills
			CASE 2 RETURN VAR_TYPE_INT 		// Assists
			CASE 3 RETURN VAR_TYPE_INT 		// xp	
			CASE 4 RETURN VAR_TYPE_INT_CASH	// cash
			CASE 5
				IF IS_ROUNDS_MISSION_LBD(eSubMode)
				
					RETURN VAR_TYPE_INT
				ELSE
//					IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_THUMB_VOTING_ALLOWED)

						RETURN VAR_TYPE_ENUM
//					ELSE
//						RETURN VAR_TYPE_INT	
//					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
	ELIF HIDE_KILLS_SHOW_DEATHS_COLUMN_LBD()
	AND NOT IS_PLAYLIST_LBD(eSubMode)
		SWITCH iColumnPriority
			CASE 1 RETURN VAR_TYPE_INT 		// Deaths
			CASE 2 RETURN VAR_TYPE_INT 		// xp	
			CASE 3 RETURN VAR_TYPE_INT_CASH	// cash
			CASE 4 
				IF IS_ROUNDS_MISSION_LBD(eSubMode)
				
					RETURN VAR_TYPE_INT
				ELSE
					IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_THUMB_VOTING_ALLOWED)

						RETURN VAR_TYPE_ENUM
					ELSE
						RETURN VAR_TYPE_INT	
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
	ELIF IS_THREE_COLUMN_LBD()
	AND NOT IS_PLAYLIST_LBD(eSubMode)
		SWITCH iColumnPriority
			CASE 1 RETURN VAR_TYPE_INT 			// XP
			CASE 2 RETURN VAR_TYPE_INT_CASH 	// cash	
			CASE 3 
				IF IS_PLAYLIST_ROUNDS_MISSION(eSubMode)
					RETURN VAR_TYPE_INT 
				ELSE	
					RETURN VAR_TYPE_ENUM
				ENDIF
			BREAK	
		ENDSWITCH
	ELIF IS_NO_SCORE_NO_KILL_LBD_SET()
	AND NOT IS_PLAYLIST_LBD(eSubMode)
		SWITCH iColumnPriority
			CASE 1 RETURN VAR_TYPE_INT 		// Deaths
			CASE 2 RETURN VAR_TYPE_INT 		// xp	
			CASE 3 RETURN VAR_TYPE_INT_CASH	// cash
			CASE 4 
				IF IS_ROUNDS_MISSION_LBD(eSubMode)
				
					RETURN VAR_TYPE_INT
				ELSE
					IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_THUMB_VOTING_ALLOWED)
	//					IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_VOTED_UP)
	//					OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_VOTED_DOWN)
	//					OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_NO_VOTE)
							RETURN VAR_TYPE_ENUM
	//					ELSE
	//						RETURN VAR_TYPE_INT_CASH
	//					ENDIF
					ELSE
						RETURN VAR_TYPE_INT	
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
	ELIF HIDE_KILLS_DEATHS_COLUMN_LBD()
	AND NOT IS_PLAYLIST_LBD(eSubMode)
	AND eSubMode <> SUB_FINAL_ROUNDS
	AND !IS_ARENA_LBD(eSubMode)
		SWITCH iColumnPriority
			CASE 1
				IF SHOW_TIME_COLUMN()
					RETURN VAR_TYPE_TIME
				ELSE
					RETURN VAR_TYPE_INT 		// Score
				ENDIF
			BREAK
			CASE 2 RETURN VAR_TYPE_INT 		// xp	
			CASE 3 RETURN VAR_TYPE_INT_CASH	// cash
			CASE 4 
				IF IS_ROUNDS_MISSION_LBD(eSubMode)
				
					RETURN VAR_TYPE_INT
				ELSE
					IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_THUMB_VOTING_ALLOWED)
	//					IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_VOTED_UP)
	//					OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_VOTED_DOWN)
	//					OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_NO_VOTE)
							RETURN VAR_TYPE_ENUM
	//					ELSE
	//						RETURN VAR_TYPE_INT_CASH
	//					ENDIF
					ELSE
						RETURN VAR_TYPE_INT	
					ENDIF
				ENDIF
			BREAK
			CASE 5
				IF eSubMode = SUB_FINAL_ROUNDS
					RETURN VAR_TYPE_INT 
				ENDIF
			BREAK
		ENDSWITCH
	ELSE
		SWITCH eSubMode
			CASE SUB_ARENA_ARCADE
				SWITCH iColumnPriority
					CASE 1 RETURN VAR_TYPE_STRING
					CASE 2 RETURN VAR_TYPE_TIME 
					CASE 3 RETURN VAR_TYPE_INT 
					CASE 4 RETURN VAR_TYPE_INT  
					CASE 5 RETURN VAR_TYPE_INT_CASH  
					CASE 6 
						IF IS_ROUNDS_MISSION_LBD(eSubMode)
						OR IS_PLAYLIST_ROUNDS_MISSION(eSubMode)
							RETURN VAR_TYPE_INT
						ENDIF
							
						RETURN VAR_TYPE_ENUM
				ENDSWITCH
			BREAK
			CASE SUB_ARENA_FIRE
			CASE SUB_ARENA_MONSTER
				SWITCH iColumnPriority
					CASE 1 RETURN VAR_TYPE_STRING
					CASE 2 RETURN VAR_TYPE_INT 
					CASE 3 RETURN VAR_TYPE_INT 
					CASE 4 RETURN VAR_TYPE_INT  
					CASE 5 RETURN VAR_TYPE_INT_CASH  
					CASE 6 
						IF IS_ROUNDS_MISSION_LBD(eSubMode)
						OR IS_PLAYLIST_ROUNDS_MISSION(eSubMode)
							RETURN VAR_TYPE_INT
						ENDIF
							
						RETURN VAR_TYPE_ENUM
				ENDSWITCH
			BREAK
			CASE SUB_ARENA_CTF
			CASE SUB_ARENA_DETONATION
			CASE SUB_ARENA_GAMESMASTER
			CASE SUB_ARENA_TAG_TEAM
				SWITCH iColumnPriority
					CASE 1 RETURN VAR_TYPE_STRING
					CASE 2 RETURN VAR_TYPE_INT 
					CASE 3 RETURN VAR_TYPE_INT 
					CASE 4 RETURN VAR_TYPE_INT  
					CASE 5 RETURN VAR_TYPE_INT_CASH  
					CASE 6 
						IF IS_ROUNDS_MISSION_LBD(eSubMode)
						OR IS_PLAYLIST_ROUNDS_MISSION(eSubMode)
							RETURN VAR_TYPE_INT
						ENDIF
							
						RETURN VAR_TYPE_ENUM
				ENDSWITCH
			BREAK
			CASE SUB_ARENA_CARNAGE
				SWITCH iColumnPriority
					CASE 1 RETURN VAR_TYPE_STRING
					CASE 2 RETURN VAR_TYPE_INT 
					CASE 3 RETURN VAR_TYPE_FLOAT 
					CASE 4 RETURN VAR_TYPE_INT  
					CASE 5 RETURN VAR_TYPE_INT_CASH  
					CASE 6 
						IF IS_ROUNDS_MISSION_LBD(eSubMode)
						OR IS_PLAYLIST_ROUNDS_MISSION(eSubMode)
							RETURN VAR_TYPE_INT
						ENDIF
							
						RETURN VAR_TYPE_ENUM
				ENDSWITCH
			BREAK
			CASE SUB_ARENA_DDERBY
				SWITCH iColumnPriority
					CASE 1 RETURN VAR_TYPE_STRING
					CASE 2 RETURN VAR_TYPE_TIME
					CASE 3 RETURN VAR_TYPE_TIME 
					CASE 4 RETURN VAR_TYPE_INT  
					CASE 5 RETURN VAR_TYPE_INT_CASH  
					CASE 6 
						IF IS_ROUNDS_MISSION_LBD(eSubMode)
						OR IS_PLAYLIST_ROUNDS_MISSION(eSubMode)
							RETURN VAR_TYPE_INT
						ENDIF
							
						RETURN VAR_TYPE_ENUM
				ENDSWITCH
			BREAK
			CASE SUB_ARENA_PASSBOMB
				SWITCH iColumnPriority
					CASE 1 RETURN VAR_TYPE_STRING
					CASE 2 RETURN VAR_TYPE_TIME
					CASE 3 RETURN VAR_TYPE_TIME 
					CASE 4 RETURN VAR_TYPE_INT  
					CASE 5 RETURN VAR_TYPE_INT_CASH  
					CASE 6 
						IF IS_ROUNDS_MISSION_LBD(eSubMode)
						OR IS_PLAYLIST_ROUNDS_MISSION(eSubMode)
							RETURN VAR_TYPE_INT
						ENDIF
							
						RETURN VAR_TYPE_ENUM
				ENDSWITCH
			BREAK
			CASE SUB_KOTH
				SWITCH iColumnPriority
					CASE 1 RETURN VAR_TYPE_INT // SCORE
					CASE 2 RETURN VAR_TYPE_INT // kills
					CASE 3 RETURN VAR_TYPE_INT	// deaths
					CASE 4 RETURN VAR_TYPE_INT  // xp
					CASE 5 RETURN VAR_TYPE_INT_CASH  // cash
					CASE 6 
						IF REPLACE_BETTING_COLUMN()
							IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_THUMB_VOTING_ALLOWED)
								IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_VOTED_UP)
								OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_VOTED_DOWN)
								OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_NO_VOTE)
									RETURN VAR_TYPE_ENUM
								ELSE
									RETURN VAR_TYPE_INT_CASH
								ENDIF
							ELSE
								RETURN VAR_TYPE_ENUM	
							ENDIF
						ELSE
							RETURN VAR_TYPE_INT_CASH  // bets
						ENDIF
					BREAK
				ENDSWITCH
			BREAK
			CASE SUB_DM_FFA
			CASE SUB_DM_TEAM
				SWITCH iColumnPriority
					CASE 1 RETURN VAR_TYPE_INT // kills
					CASE 2 RETURN VAR_TYPE_INT // deaths
					CASE 3 RETURN VAR_TYPE_FLOAT	// ratio
					CASE 4 RETURN VAR_TYPE_INT  // xp
					CASE 5 RETURN VAR_TYPE_INT_CASH  // cash
					CASE 6 
						IF REPLACE_BETTING_COLUMN()
							IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_THUMB_VOTING_ALLOWED)
								IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_VOTED_UP)
								OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_VOTED_DOWN)
								OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_NO_VOTE)
									RETURN VAR_TYPE_ENUM
								ELSE
									RETURN VAR_TYPE_INT_CASH
								ENDIF
							ELSE
								RETURN VAR_TYPE_ENUM	
							ENDIF
						ELSE
							RETURN VAR_TYPE_INT_CASH  // bets
						ENDIF
					BREAK
				ENDSWITCH
			BREAK
			CASE SUB_DM_UPDATE
				SWITCH iColumnPriority
					CASE 1 RETURN VAR_TYPE_INT // score
					CASE 2 RETURN VAR_TYPE_INT // kills
					CASE 3 RETURN VAR_TYPE_INT	// deaths
					CASE 4 RETURN VAR_TYPE_FLOAT  // ratio
					CASE 5 RETURN VAR_TYPE_INT  // rp
					CASE 6 RETURN VAR_TYPE_INT_CASH  // cash
				ENDSWITCH
			BREAK
			CASE SUB_SHOOT_RANGE
				SWITCH iColumnPriority
					CASE 1 RETURN VAR_TYPE_INT // SCORE
					CASE 2 RETURN VAR_TYPE_INT // fired
					CASE 3 RETURN VAR_TYPE_INT	// hit
					CASE 4 RETURN VAR_TYPE_FLOAT  // accuracy
				ENDSWITCH
			BREAK
			CASE SUB_KART
				SWITCH iColumnPriority
					CASE 1 RETURN VAR_TYPE_INT // kills
					CASE 2 RETURN VAR_TYPE_INT // deaths
					CASE 3 RETURN VAR_TYPE_INT_CASH  // cash 
				ENDSWITCH
			BREAK
			CASE SUB_HORDE
				SWITCH iColumnPriority
					CASE 1 RETURN VAR_TYPE_INT // kills
					CASE 2 RETURN VAR_TYPE_INT // deaths
					CASE 3 RETURN VAR_TYPE_FLOAT	// ratio
					CASE 4 RETURN VAR_TYPE_INT  // xp
				ENDSWITCH
			BREAK
			
			CASE SUB_RACE_AGG_BEST
			CASE SUB_RACE_AGG_TIME
			CASE SUB_RACE_TARGET
			
				SWITCH iColumnPriority
					CASE 1 RETURN VAR_TYPE_STRING // veh name
					CASE 2 RETURN VAR_TYPE_INT // score
					CASE 3 RETURN VAR_TYPE_TIME // best lap
					CASE 4 RETURN VAR_TYPE_INT  // xp
					CASE 5 RETURN VAR_TYPE_INT_CASH  // cash 
					CASE 6
						IF REPLACE_BETTING_COLUMN()
							IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_THUMB_VOTING_ALLOWED)
							AND HAS_VOTED()
								RETURN VAR_TYPE_ENUM
							ELSE
								RETURN VAR_TYPE_ENUM
							ENDIF
						ELSE
							RETURN VAR_TYPE_INT_CASH  // bets
						ENDIF
					BREAK
				ENDSWITCH
			BREAK
			
			CASE SUB_RACE_FOOT
				SWITCH iNumColumns
					CASE 5
						SWITCH iColumnPriority
							CASE 1 RETURN VAR_TYPE_TIME 
							CASE 2 RETURN VAR_TYPE_TIME 
							CASE 3 RETURN VAR_TYPE_INT 
							CASE 4 RETURN VAR_TYPE_INT_CASH  
							CASE 5 
								IF REPLACE_BETTING_COLUMN() 
									IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_THUMB_VOTING_ALLOWED)
										IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_VOTED_UP)
										OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_VOTED_DOWN)
										OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_NO_VOTE)
											RETURN VAR_TYPE_ENUM
										ELSE
											RETURN VAR_TYPE_INT_CASH
										ENDIF
									ELSE
										RETURN VAR_TYPE_ENUM	
									ENDIF
								ELSE
									RETURN VAR_TYPE_INT_CASH  // bets
								ENDIF
							BREAK
						ENDSWITCH
					BREAK
					CASE 4
						SWITCH iColumnPriority
							CASE 1 RETURN VAR_TYPE_TIME 
							CASE 2 RETURN VAR_TYPE_INT 
							CASE 3 RETURN VAR_TYPE_INT  
							CASE 4 RETURN VAR_TYPE_INT_CASH  
							CASE 5 
								IF REPLACE_BETTING_COLUMN()
									IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_THUMB_VOTING_ALLOWED)
										IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_VOTED_UP)
										OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_VOTED_DOWN)
										OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_NO_VOTE)
											RETURN VAR_TYPE_ENUM
										ELSE
											RETURN VAR_TYPE_INT_CASH
										ENDIF
									ELSE
										RETURN VAR_TYPE_ENUM	
									ENDIF
								ELSE
									RETURN VAR_TYPE_INT_CASH  // bets
								ENDIF
							BREAK
						ENDSWITCH
					BREAK
				ENDSWITCH
			BREAK
			CASE SUB_RACE_NORMAL
			CASE SUB_RACE_GTA	
			CASE SUB_RACE_RALLY
			CASE SUB_RACE_STUNT
				SWITCH iNumColumns
				
					CASE 6
						SWITCH iColumnPriority
							CASE 1 RETURN VAR_TYPE_STRING // veh name
							CASE 2 RETURN VAR_TYPE_TIME // total time
							CASE 3 RETURN VAR_TYPE_TIME // best lap
							CASE 4 RETURN VAR_TYPE_INT  // xp
							CASE 5 RETURN VAR_TYPE_INT_CASH  // cash 
							CASE 6
								IF REPLACE_BETTING_COLUMN()
									IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_THUMB_VOTING_ALLOWED)
									AND HAS_VOTED()
										RETURN VAR_TYPE_ENUM
									ELSE
										RETURN VAR_TYPE_ENUM
									ENDIF
								ELSE
									RETURN VAR_TYPE_INT_CASH  // bets
								ENDIF
							BREAK
						ENDSWITCH
					BREAK
					
					CASE 5
						SWITCH iColumnPriority
							CASE 1 RETURN VAR_TYPE_STRING // veh name
							CASE 2 RETURN VAR_TYPE_TIME // total time
							CASE 3 RETURN VAR_TYPE_INT  // xp
							CASE 4 RETURN VAR_TYPE_INT_CASH  // cash 
							CASE 5 
								IF REPLACE_BETTING_COLUMN()
									IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_THUMB_VOTING_ALLOWED)
									AND HAS_VOTED()
										RETURN VAR_TYPE_ENUM
									ELSE
										RETURN VAR_TYPE_ENUM
									ENDIF
								ELSE
									RETURN VAR_TYPE_INT_CASH  // bets
								ENDIF
							BREAK
						ENDSWITCH
					BREAK
					
					CASE 4
						SWITCH iColumnPriority
							CASE 1 RETURN VAR_TYPE_TIME // veh name
							CASE 2 RETURN VAR_TYPE_TIME // total time
							CASE 3 RETURN VAR_TYPE_INT  // xp
							CASE 4 RETURN VAR_TYPE_INT_CASH  // cash 
							CASE 5 
								IF REPLACE_BETTING_COLUMN()
									IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_THUMB_VOTING_ALLOWED)
										IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_VOTED_UP)
										OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_VOTED_DOWN)
										OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_NO_VOTE)
											RETURN VAR_TYPE_ENUM
										ELSE
											RETURN VAR_TYPE_INT_CASH
										ENDIF
									ELSE
										RETURN VAR_TYPE_ENUM	
									ENDIF
								ELSE
									RETURN VAR_TYPE_INT_CASH  // bets
								ENDIF
							BREAK
						ENDSWITCH
					BREAK
					
					CASE 1
						RETURN VAR_TYPE_INT 
					BREAK

				ENDSWITCH
			BREAK
			CASE SUB_RACE_BASEJUMP
				SWITCH iNumColumns
					CASE 5
						SWITCH iColumnPriority
							CASE 1 RETURN VAR_TYPE_INT // points
							CASE 2 RETURN VAR_TYPE_TIME // total time 
							CASE 3 RETURN VAR_TYPE_INT  // xp
							CASE 4 RETURN VAR_TYPE_INT_CASH  // cash 
							CASE 5 
								IF REPLACE_BETTING_COLUMN()
									IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_THUMB_VOTING_ALLOWED)
										IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_VOTED_UP)
										OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_VOTED_DOWN)
										OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_NO_VOTE)
											RETURN VAR_TYPE_ENUM
										ELSE
											RETURN VAR_TYPE_INT_CASH
										ENDIF
									ELSE
										RETURN VAR_TYPE_ENUM	
									ENDIF
								ELSE
									RETURN VAR_TYPE_INT_CASH  // bets
								ENDIF
							BREAK
						ENDSWITCH
					BREAK
				ENDSWITCH
			BREAK
			
			CASE SUB_MISSIONS_HEIST
				SWITCH iColumnPriority
					CASE 1 RETURN VAR_TYPE_INT 
					CASE 2 RETURN VAR_TYPE_INT 
					CASE 3 RETURN VAR_TYPE_INT_CASH 
					CASE 4 RETURN VAR_TYPE_ENUM	
					CASE 5 RETURN VAR_TYPE_INT 
				ENDSWITCH
			BREAK
			
			CASE SUB_MISSIONS_VERSUS
				SWITCH iColumnPriority
					CASE 1
						IF SHOW_TIME_COLUMN()
							RETURN VAR_TYPE_TIME 
						ENDIF
						RETURN VAR_TYPE_INT 
					CASE 2 RETURN VAR_TYPE_INT 
					CASE 3 RETURN VAR_TYPE_INT 
					CASE 4 RETURN VAR_TYPE_INT 
					CASE 5 RETURN VAR_TYPE_INT_CASH 
					CASE 6 
						IF IS_ROUNDS_MISSION_LBD(eSubMode)
						OR IS_PLAYLIST_ROUNDS_MISSION(eSubMode)
							RETURN VAR_TYPE_INT
						ELSE
							IF REPLACE_BETTING_COLUMN()
								RETURN VAR_TYPE_ENUM
							ELSE
								RETURN VAR_TYPE_INT_CASH  // bets
							ENDIF
						ENDIF
					BREAK
				ENDSWITCH
			BREAK
			
			CASE SUB_MISSIONS_NO_HUD
				SWITCH iColumnPriority
					CASE 1 RETURN VAR_TYPE_INT 
					CASE 2 RETURN VAR_TYPE_INT 
					CASE 3 RETURN VAR_TYPE_INT 
					CASE 4 RETURN VAR_TYPE_INT_CASH 
					CASE 5 
						IF IS_PLAYLIST_ROUNDS_MISSION(eSubMode)
						OR IS_ROUNDS_MISSION_LBD(eSubMode)
							RETURN VAR_TYPE_INT 
						ELSE	
							RETURN VAR_TYPE_ENUM
						ENDIF
					BREAK
					CASE 6 RETURN VAR_TYPE_INT 
				ENDSWITCH
			BREAK
			
			CASE SUB_FINAL_ROUNDS
				IF SHOW_SCORE_COLUMN_ON_LEADERBOARD()
					SWITCH iColumnPriority
						CASE 1
							IF SHOW_TIME_COLUMN()
								RETURN VAR_TYPE_TIME 
							ENDIF
							RETURN VAR_TYPE_INT 
						BREAK
						CASE 2 RETURN VAR_TYPE_INT 
						CASE 3 RETURN VAR_TYPE_INT 
						CASE 4 RETURN VAR_TYPE_INT 
						CASE 5 RETURN VAR_TYPE_INT_CASH 
						CASE 6 RETURN VAR_TYPE_INT 
						BREAK
					ENDSWITCH
				ELSE
					SWITCH iColumnPriority
						CASE 1
							IF SHOW_TIME_COLUMN()
								RETURN VAR_TYPE_TIME 
							ENDIF
							RETURN VAR_TYPE_INT 
						BREAK
						CASE 2 RETURN VAR_TYPE_INT 
						CASE 3 RETURN VAR_TYPE_INT 
						CASE 4 RETURN VAR_TYPE_INT_CASH 
						CASE 5 RETURN VAR_TYPE_INT 
						BREAK
					ENDSWITCH
				ENDIF
			BREAK
			
			CASE SUB_MISSIONS_LTS
				SWITCH iColumnPriority
					CASE 1 RETURN VAR_TYPE_INT 
					CASE 2 RETURN VAR_TYPE_INT 
					CASE 3 RETURN VAR_TYPE_INT 
					CASE 4 RETURN VAR_TYPE_INT_CASH 
					CASE 5
						IF IS_PLAYLIST_ROUNDS_MISSION(eSubMode)
							RETURN VAR_TYPE_INT 
						ELSE
							IF g_FinalRoundsLbd
								RETURN VAR_TYPE_INT_CASH
							ELSE
								IF IS_ROUNDS_MISSION_LBD(eSubMode)
									RETURN VAR_TYPE_ENUM	
								ELSE
									IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_THUMB_VOTING_ALLOWED)
										IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_VOTED_UP)
										OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_VOTED_DOWN)
										OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_NO_VOTE)
											RETURN VAR_TYPE_ENUM
										ELSE
											RETURN VAR_TYPE_INT_CASH
										ENDIF
									ELSE
										RETURN VAR_TYPE_ENUM	
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					BREAK
					CASE 6 RETURN VAR_TYPE_INT 
				ENDSWITCH
			BREAK
			
			CASE SUB_MISSIONS_CTF
				IF SHOW_SCORE_COLUMN_ON_LEADERBOARD()
					SWITCH iColumnPriority
						CASE 1 RETURN VAR_TYPE_INT // score
						CASE 2 RETURN VAR_TYPE_INT // kills
						CASE 3 RETURN VAR_TYPE_INT	// deaths
						CASE 4 RETURN VAR_TYPE_INT  // xp
						CASE 5 
							IF IS_PLAYLIST_ROUNDS_MISSION(eSubMode)
								RETURN VAR_TYPE_INT 
							ELSE
								RETURN VAR_TYPE_INT_CASH  // cash
							ENDIF
						BREAK
						CASE 6 
							IF IS_ROUNDS_MISSION_LBD(eSubMode)
								RETURN VAR_TYPE_INT
							ELSE
								IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_THUMB_VOTING_ALLOWED)
									IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_VOTED_UP)
									OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_VOTED_DOWN)
									OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_NO_VOTE)
										RETURN VAR_TYPE_ENUM
									ELSE
										RETURN VAR_TYPE_INT_CASH
									ENDIF
								ELSE
									RETURN VAR_TYPE_ENUM	
								ENDIF
							ENDIF
						BREAK
					ENDSWITCH
				ELSE
					SWITCH iColumnPriority
						CASE 1 RETURN VAR_TYPE_INT // kills
						CASE 2 RETURN VAR_TYPE_INT	// deaths
						CASE 3 RETURN VAR_TYPE_INT  // xp
						CASE 4 
							IF IS_PLAYLIST_ROUNDS_MISSION(eSubMode)
								RETURN VAR_TYPE_INT 
							ELSE
								RETURN VAR_TYPE_INT_CASH  // cash
							ENDIF
						BREAK
						CASE 5 
							IF IS_ROUNDS_MISSION_LBD(eSubMode)
								RETURN VAR_TYPE_INT
							ELSE
								IF REPLACE_BETTING_COLUMN()
								OR IS_PLAYER_ON_A_PLAYLIST(PLAYER_ID())
									RETURN VAR_TYPE_ENUM	
								ELSE
									RETURN VAR_TYPE_INT_CASH  // bets
								ENDIF
							ENDIF
						BREAK
					ENDSWITCH
				ENDIF
			BREAK
			
			CASE SUB_MISSIONS_NORMAL
				SWITCH iColumnPriority
					CASE 1 RETURN VAR_TYPE_INT 
					CASE 2 RETURN VAR_TYPE_INT 
					CASE 3 RETURN VAR_TYPE_INT 
					CASE 4 RETURN VAR_TYPE_FLOAT  
				ENDSWITCH
			BREAK
			
			CASE SUB_MISSIONS_TIMED
				SWITCH iColumnPriority
					CASE 1 RETURN VAR_TYPE_TIME 
					CASE 2 RETURN VAR_TYPE_INT 
					CASE 3 RETURN VAR_TYPE_INT 
					CASE 4 RETURN VAR_TYPE_FLOAT  
				ENDSWITCH
			BREAK
			
			CASE SUB_PLAYLIST
				SWITCH iNumColumns
					CASE 6
						SWITCH iColumnPriority
							CASE 1 RETURN VAR_TYPE_INT
							CASE 2 RETURN VAR_TYPE_INT 
							CASE 3 RETURN VAR_TYPE_INT 
							CASE 4 RETURN VAR_TYPE_INT  
							CASE 5 RETURN VAR_TYPE_INT   
							CASE 6 RETURN VAR_TYPE_ENUM  // votes
						ENDSWITCH
					BREAK
					CASE 5
						SWITCH iColumnPriority
							CASE 1 RETURN VAR_TYPE_INT
							CASE 2 RETURN VAR_TYPE_INT 
							CASE 3 RETURN VAR_TYPE_INT 
							CASE 4 RETURN VAR_TYPE_INT  
							CASE 5 RETURN VAR_TYPE_ENUM 
						ENDSWITCH
					BREAK
					
					CASE 4
						SWITCH iColumnPriority
							CASE 1 RETURN VAR_TYPE_INT
							CASE 2 RETURN VAR_TYPE_INT 
							CASE 3 RETURN VAR_TYPE_INT 
							CASE 4 RETURN VAR_TYPE_ENUM 
						ENDSWITCH
					BREAK
					
					CASE 3
						SWITCH iColumnPriority
							CASE 1 RETURN VAR_TYPE_INT
							CASE 2 RETURN VAR_TYPE_INT 
							CASE 3 RETURN VAR_TYPE_ENUM 
						ENDSWITCH
					BREAK
				ENDSWITCH
			BREAK
			
			CASE SUB_SC_PRE_RACE_SUB_TITLE
			CASE SUB_SC_PRE_RACE_CONTENT
			
				RETURN VAR_TYPE_INT
			BREAK
			
			CASE SUB_FREEMODE
			CASE SUB_ARCADE_CNC
				SWITCH iColumnPriority
					CASE 1 RETURN VAR_TYPE_INT
				ENDSWITCH
			BREAK
		ENDSWITCH
	ENDIF
	
	RETURN VAR_TYPE_NOT_USED
ENDFUNC

PROC POPULATE_COLUMNS(	LBD_VARS& lbdVars, LEADERBOARD_PLACEMENT_TOOLS& Placement, LBD_SUB_MODE eSubMode, LBD_VOTE eVote, INT iNumColumns, FLOAT fRatio, 
						INT iColumn1Var, INT iColumn2Var, INT iColumn3Var, INT iColumn4Var, INT iColumn5Var, INT iColumn6Var, INT iParticipant, INT iRow, INT iRoundScore = 0, 
						INT iCustomInt = 0, INT iteam = -1)
																					
	LBD_VAR_TYPE lbdVarType
	INT iLastColumn
	
	IF bIS_JUGGERNAUT()
	AND NOT IS_PLAYLIST_LBD(eSubMode)	
	AND eSubMode <> SUB_FINAL_ROUNDS
		PRINTLN("3923725 1 ")
		DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 1, iNumColumns), eVote,  fRatio, 1, iColumn1Var, iNumColumns, iParticipant, iRow) 
		DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 2, iNumColumns), eVote,  fRatio, 2, iColumn2Var, iNumColumns, iParticipant, iRow) 
		DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 3, iNumColumns), eVote,  fRatio, 3, iColumn3Var, iNumColumns, iParticipant, iRow) 
		DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 4, iNumColumns), eVote,  fRatio, 4, iColumn4Var, iNumColumns, iParticipant, iRow) 
		DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 5, iNumColumns), eVote,  fRatio, 5, iColumn5Var, iNumColumns, iParticipant, iRow) 
		DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 6, iNumColumns), eVote,  fRatio, 6, iRoundScore, iNumColumns, iParticipant, iRow, FALSE, IS_ROUNDS_MISSION_LBD(eSubMode))
	ELIF bIS_TURF_WARS()
	AND NOT IS_PLAYLIST_LBD(eSubMode)	
		PRINTLN("3923725 2 ")
		DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 1, iNumColumns), eVote,  fRatio, 1, iColumn1Var, iNumColumns, iParticipant, iRow) 
		DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 2, iNumColumns), eVote,  fRatio, 2, iColumn2Var, iNumColumns, iParticipant, iRow) 
		DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 3, iNumColumns), eVote,  fRatio, 3, iColumn3Var, iNumColumns, iParticipant, iRow) 
		DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 4, iNumColumns), eVote,  fRatio, 4, iColumn4Var, iNumColumns, iParticipant, iRow) 
		DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 5, iNumColumns), eVote,  fRatio, 5, iColumn5Var, iNumColumns, iParticipant, iRow) 
		DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 6, iNumColumns), eVote,  fRatio, 6, iRoundScore, iNumColumns, iParticipant, iRow, FALSE, IS_ROUNDS_MISSION_LBD(eSubMode))
	ELIF CUSTOM_LBD_REMIX()
	AND NOT IS_PLAYLIST_LBD(eSubMode)	
		PRINTLN("3923725 CUSTOM_LBD_REMIX ")
		DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 1, iNumColumns), eVote,  fRatio, 1, iColumn1Var, iNumColumns, iParticipant, iRow) 
		DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 2, iNumColumns), eVote,  fRatio, 2, iColumn2Var, iNumColumns, iParticipant, iRow) 
		DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 3, iNumColumns), eVote,  fRatio, 3, iColumn3Var, iNumColumns, iParticipant, iRow) 
		DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 4, iNumColumns), eVote,  fRatio, 4, iColumn4Var, iNumColumns, iParticipant, iRow) 
		DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 5, iNumColumns), eVote,  fRatio, 5, iColumn5Var, iNumColumns, iParticipant, iRow) 
		DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 6, iNumColumns), eVote,  fRatio, 6, iRoundScore, iNumColumns, iParticipant, iRow, FALSE, IS_ROUNDS_MISSION_LBD(eSubMode))
	ELIF SHOW_SLIPSTREAM_COLUMN()
	AND NOT IS_PLAYLIST_LBD(eSubMode)
		PRINTLN("3923725 3 ")
		IF NOT lbdVars.bHideIndividual		
			DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 1, iNumColumns), eVote,  fRatio, 1, iColumn1Var, iNumColumns, iParticipant, iRow) // Best Lap/Checkpoints
		ENDIF
		DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 2, iNumColumns), eVote,  fRatio, 2, iColumn2Var, iNumColumns, iParticipant, iRow) // Slipstream
		DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 3, iNumColumns), eVote,  fRatio, 3, iColumn4Var, iNumColumns, iParticipant, iRow) // RP
		DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 4, iNumColumns), eVote,  fRatio, 4, iColumn5Var, iNumColumns, iParticipant, iRow) // Cash
		DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 5, iNumColumns), eVote,  fRatio, 5, iRoundScore, iNumColumns, iParticipant, iRow, FALSE, IS_ROUNDS_MISSION_LBD(eSubMode))
	ELIF SHOW_ASSISTS_LEADERBOARD()
	AND NOT IS_PLAYLIST_LBD(eSubMode)
		PRINTLN("3923725 4 ")
		DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 1, iNumColumns), eVote,  fRatio, 1, iColumn2Var, iNumColumns, iParticipant, iRow) // Kills
		PRINTLN("2655789, g_iMyAssists = ", g_iMyAssists, " iCustomInt = ", iCustomInt)
		DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 2, iNumColumns), eVote,  fRatio, 2, iCustomInt, iNumColumns, iParticipant, iRow) 	// Assists
		DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 3, iNumColumns), eVote,  fRatio, 3, iColumn4Var, iNumColumns, iParticipant, iRow) // RP
		DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 4, iNumColumns), eVote,  fRatio, 4, iColumn5Var, iNumColumns, iParticipant, iRow) // Cash

		DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 5, iNumColumns), eVote,  fRatio, 5, iRoundScore, iNumColumns, iParticipant, iRow, FALSE, IS_ROUNDS_MISSION_LBD(eSubMode))
	ELIF HIDE_KILLS_SHOW_DEATHS_COLUMN_LBD()
	AND NOT IS_PLAYLIST_LBD(eSubMode)
	AND eSubMode <> SUB_FINAL_ROUNDS
	AND !IS_ARENA_LBD(eSubMode)
		PRINTLN("3923725 5 ")
		DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 1, iNumColumns), eVote,  fRatio, 1, iColumn3Var, iNumColumns, iParticipant, iRow) // Deaths
		DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 2, iNumColumns), eVote,  fRatio, 2, iColumn4Var, iNumColumns, iParticipant, iRow) // RP
		DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 3, iNumColumns), eVote,  fRatio, 3, iColumn5Var, iNumColumns, iParticipant, iRow) // Cash
		IF IS_PLAYLIST_ROUNDS_MISSION(eSubMode)
		OR IS_ROUNDS_MISSION_LBD(eSubMode)
			DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 4, iNumColumns), eVote,  fRatio, 4, iRoundScore, iNumColumns, iParticipant, iRow, FALSE, IS_ROUNDS_MISSION_LBD(eSubMode))
		ELSE
			DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 4, iNumColumns), eVote,  fRatio, 4, iColumn6Var, iNumColumns, iParticipant, iRow) // Betting
		ENDIF
	ELIF IS_THREE_COLUMN_LBD()
	AND NOT IS_PLAYLIST_LBD(eSubMode)
		PRINTLN("3923725 6 ")
		DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 1, iNumColumns), eVote,  fRatio, 1, iColumn4Var, iNumColumns, iParticipant, iRow) //RP
		DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 2, iNumColumns), eVote,  fRatio, 2, iColumn5Var, iNumColumns, iParticipant, iRow) // Cash
		IF IS_PLAYLIST_ROUNDS_MISSION(eSubMode)
			DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 3, iNumColumns), eVote,  fRatio, 3, iRoundScore, iNumColumns, iParticipant, iRow, FALSE, IS_ROUNDS_MISSION_LBD(eSubMode))
		ELSE
			DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 3, iNumColumns), eVote,  fRatio, 3, iColumn6Var, iNumColumns, iParticipant, iRow) // Betting
		ENDIF
	ELIF IS_NO_SCORE_NO_KILL_LBD_SET()
	AND NOT IS_PLAYLIST_LBD(eSubMode)
		PRINTLN("3923725 7 ")
		DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 1, iNumColumns), eVote,  fRatio, 1, iColumn3Var, iNumColumns, iParticipant, iRow) // Deaths
		DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 2, iNumColumns), eVote,  fRatio, 2, iColumn4Var, iNumColumns, iParticipant, iRow) // RP
		DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 3, iNumColumns), eVote,  fRatio, 3, iColumn5Var, iNumColumns, iParticipant, iRow) // Cash
		IF IS_PLAYLIST_ROUNDS_MISSION(eSubMode)
		OR IS_ROUNDS_MISSION_LBD(eSubMode)
			PRINTLN("2464135 POPULATE_COLUMNS, iRoundScore = ", iRoundScore)
			DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 4, iNumColumns), eVote,  fRatio, 4, iRoundScore, iNumColumns, iParticipant, iRow, FALSE, IS_ROUNDS_MISSION_LBD(eSubMode))
		ELSE
			PRINTLN("2464135 POPULATE_COLUMNS, NAHHH = ", iRoundScore)
			DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 4, iNumColumns), eVote,  fRatio, 4, iColumn6Var, iNumColumns, iParticipant, iRow) // Betting
		ENDIF
	ELIF HIDE_KILLS_DEATHS_COLUMN_LBD()
	AND NOT IS_PLAYLIST_LBD(eSubMode)
	AND eSubMode <> SUB_FINAL_ROUNDS
	AND !IS_ARENA_LBD(eSubMode)
		PRINTLN("3923725 8 ")
		DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 1, iNumColumns), eVote,  fRatio, 1, iColumn1Var, iNumColumns, iParticipant, iRow) // Score
		DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 2, iNumColumns), eVote,  fRatio, 2, iColumn4Var, iNumColumns, iParticipant, iRow) // RP
		DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 3, iNumColumns), eVote,  fRatio, 3, iColumn5Var, iNumColumns, iParticipant, iRow) // Cash
		IF IS_PLAYLIST_ROUNDS_MISSION(eSubMode)
//			IF REPLACE_BETTING_COLUMN()
				PRINTLN("3659011 1 iRoundScore = ", iRoundScore)
				DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 4, iNumColumns), eVote,  fRatio, 4, iRoundScore, iNumColumns, iParticipant, iRow, FALSE, 
								IS_ROUNDS_MISSION_LBD(eSubMode))
//			ELSE
//				PRINTLN("3659011 2 iRoundScore = ", iRoundScore)
//				DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 4, iNumColumns), eVote,  fRatio, 4, iColumn6Var, iNumColumns, iParticipant, iRow, FALSE, 
//								IS_ROUNDS_MISSION_LBD(eSubMode))
//			ENDIF
		ELIF IS_ROUNDS_MISSION_LBD(eSubMode)
//		AND IS_THIS_ROCKSTAR_MISSION_NEW_VS_OVERTIME_ANY(g_FMMC_STRUCT.iAdversaryModeType)
		AND eSubMode <> SUB_FINAL_ROUNDS
			PRINTLN("3659011 3 iRoundScore = ", iRoundScore)
			DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 4, iNumColumns), eVote,  fRatio, 4, iRoundScore, iNumColumns, iParticipant, iRow, FALSE, IS_ROUNDS_MISSION_LBD(eSubMode))
		ELSE
			DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 4, iNumColumns), eVote,  fRatio, 4, iColumn6Var, iNumColumns, iParticipant, iRow) // Betting
		ENDIF
		IF eSubMode = SUB_FINAL_ROUNDS
			DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 5, iNumColumns), eVote,  fRatio, 5, iRoundScore, iNumColumns, iParticipant, iRow, FALSE, IS_ROUNDS_MISSION_LBD(eSubMode))
		ENDIF
	ELSE
	
		PRINTLN("3923725 9 ")
	
		SWITCH eSubMode
			CASE SUB_RACE_BASEJUMP
				SWITCH iNumColumns
					CASE 5
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (1), iNumColumns),  eVote,	fRatio, (1),  iColumn1Var , iNumColumns, iParticipant, iRow)
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (2), iNumColumns),  eVote,	fRatio, (2),  iColumn2Var, iNumColumns, iParticipant, iRow)
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (3), iNumColumns),  eVote,	fRatio, (3),  iColumn3Var, iNumColumns, iParticipant, iRow)
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (4), iNumColumns),  eVote,	fRatio, (4),  iColumn4Var, iNumColumns, iParticipant, iRow)
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (5), iNumColumns),  eVote,  fRatio, (5),  iColumn5Var, iNumColumns, iParticipant, iRow)
					BREAK	
				ENDSWITCH
			BREAK
			
			CASE SUB_RACE_AGG_BEST
			CASE SUB_RACE_AGG_TIME
			CASE SUB_RACE_TARGET
				DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (1), iNumColumns), eVote,  fRatio,	(1), iColumn6Var, iNumColumns, iParticipant, iRow)
				DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (2), iNumColumns), eVote,  fRatio,	(2), iColumn1Var, iNumColumns, iParticipant, iRow)
				DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (3), iNumColumns), eVote,  fRatio,	(3), iColumn2Var, iNumColumns, iParticipant, iRow)
				DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (4), iNumColumns), eVote,  fRatio,	(4), iColumn3Var, iNumColumns, iParticipant, iRow)
				DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (5), iNumColumns), eVote,  fRatio,	(5), iColumn4Var, iNumColumns, iParticipant, iRow)
				DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (6), iNumColumns), eVote,  fRatio,	(6), iColumn5Var, iNumColumns, iParticipant, iRow)
			BREAK
			
			CASE SUB_RACE_FOOT
				SWITCH iNumColumns
					CASE 5
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (1), iNumColumns),  eVote,	fRatio, (1),  iColumn1Var , iNumColumns, iParticipant, iRow)
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (2), iNumColumns),  eVote,	fRatio, (2),  iColumn2Var, iNumColumns, iParticipant, iRow)
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (3), iNumColumns),  eVote,	fRatio, (3),  iColumn3Var, iNumColumns, iParticipant, iRow)
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (4), iNumColumns),  eVote,	fRatio, (4),  iColumn4Var, iNumColumns, iParticipant, iRow)
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (5), iNumColumns),  eVote,    fRatio, (5),  iColumn5Var, iNumColumns, iParticipant, iRow)
					BREAK
			
					CASE 4
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 1, iNumColumns), eVote,  fRatio, 1, iColumn1Var, iNumColumns, iParticipant, iRow)
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 2, iNumColumns), eVote,  fRatio, 2, iColumn3Var, iNumColumns, iParticipant, iRow)
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 3, iNumColumns), eVote,  fRatio, 3, iColumn4Var, iNumColumns, iParticipant, iRow)
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 4, iNumColumns), eVote,  fRatio, 4, iColumn5Var, iNumColumns, iParticipant, iRow)
					BREAK
				ENDSWITCH
			BREAK
			CASE SUB_RACE_GTA
			CASE SUB_RACE_NORMAL
			CASE SUB_RACE_RALLY
			CASE SUB_RACE_STUNT
				SWITCH iNumColumns
					CASE 6
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (1), iNumColumns), eVote,  fRatio,	(1), iColumn6Var, iNumColumns, iParticipant, iRow)
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (2), iNumColumns), eVote,  fRatio,	(2), iColumn1Var, iNumColumns, iParticipant, iRow)
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (3), iNumColumns), eVote,  fRatio,	(3), iColumn2Var, iNumColumns, iParticipant, iRow)
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (4), iNumColumns), eVote,  fRatio,	(4), iColumn3Var, iNumColumns, iParticipant, iRow)
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (5), iNumColumns), eVote,  fRatio,	(5), iColumn4Var, iNumColumns, iParticipant, iRow)
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (6), iNumColumns), eVote,  fRatio,	(6), iColumn5Var, iNumColumns, iParticipant, iRow)
					BREAK
					
					CASE 5
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (1), iNumColumns),  eVote, 	fRatio, (1),  iColumn6Var, iNumColumns, iParticipant, iRow)
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (2), iNumColumns),  eVote, 	fRatio, (2),  iColumn1Var, iNumColumns, iParticipant, iRow)
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (3), iNumColumns),  eVote, 	fRatio, (3),  iColumn3Var, iNumColumns, iParticipant, iRow)// no best lap
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (4), iNumColumns),  eVote, 	fRatio, (4),  iColumn4Var, iNumColumns, iParticipant, iRow)
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (5), iNumColumns),  eVote,    fRatio, (5),  iColumn5Var, iNumColumns, iParticipant, iRow)
					BREAK	
					
					CASE 4
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 1, iNumColumns), eVote,  fRatio, 1, iColumn1Var, iNumColumns, iParticipant, iRow)
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 2, iNumColumns), eVote,  fRatio, 2, iColumn2Var, iNumColumns, iParticipant, iRow)
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 3, iNumColumns), eVote,  fRatio, 3, iColumn3Var, iNumColumns, iParticipant, iRow)
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 4, iNumColumns), eVote,  fRatio, 4, iColumn4Var, iNumColumns, iParticipant, iRow)
					BREAK
						
					CASE 3
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 1, iNumColumns), eVote,fRatio, 1, iColumn1Var, iNumColumns, iParticipant, iRow)
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 2, iNumColumns),eVote, fRatio, 2, iColumn2Var, iNumColumns, iParticipant, iRow)
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 3, iNumColumns), eVote,fRatio, 3, iColumn3Var, iNumColumns, iParticipant, iRow)
					BREAK
					
					CASE 2
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 1, iNumColumns), eVote, fRatio, 1, iColumn1Var, iNumColumns, iParticipant, iRow)
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 2, iNumColumns), eVote, fRatio, 2, iColumn2Var, iNumColumns, iParticipant, iRow)
					BREAK
					
					CASE 1
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 1, iNumColumns), eVote, fRatio, 1, iColumn1Var, iNumColumns, iParticipant, iRow)
					BREAK
				ENDSWITCH
			BREAK
			CASE SUB_SHOOT_RANGE
				SWITCH iNumColumns
					CASE 4
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (1), iNumColumns), eVote,  fRatio, (1), iColumn1Var, iNumColumns, iParticipant, iRow) 
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (2), iNumColumns), eVote,  fRatio,	(2), iColumn2Var, iNumColumns, iParticipant, iRow)
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (3), iNumColumns), eVote,  fRatio,	(3), iColumn3Var, iNumColumns, iParticipant, iRow)
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (4), iNumColumns), eVote,  fRatio,	(4), iColumn6Var, iNumColumns, iParticipant, iRow)
					BREAK
				ENDSWITCH
			BREAK
			
			CASE SUB_ARENA_ARCADE
				SWITCH iNumColumns
					CASE 6
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (1), iNumColumns), eVote,  fRatio,	(1), -1, iNumColumns, iParticipant, iRow)			// Vehicle Name
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (2), iNumColumns), eVote,  fRatio,	(2), iColumn1Var, iNumColumns, iParticipant, iRow)	// TimeS
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (3), iNumColumns), eVote,  fRatio,	(3), iColumn6Var, iNumColumns, iParticipant, iRow)  // Kills
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (4), iNumColumns), eVote,  fRatio,	(4), iColumn3Var, iNumColumns, iParticipant, iRow)  // AP
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (5), iNumColumns), eVote,  fRatio,	(5), iColumn4Var, iNumColumns, iParticipant, iRow)  // Cash
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (6), iNumColumns), eVote,  fRatio,	(6), iColumn5Var, iNumColumns, iParticipant, iRow)  // Next/Rounds
					BREAK
					
					CASE 5
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (1), iNumColumns),  eVote, 	fRatio, (1),  -1, iNumColumns, iParticipant, iRow)			 // Vehicle Name
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (2), iNumColumns),  eVote, 	fRatio, (2),  iColumn1Var, iNumColumns, iParticipant, iRow)	 // TimeS
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (3), iNumColumns),  eVote, 	fRatio, (3),  iColumn6Var, iNumColumns, iParticipant, iRow)  // Kills
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (4), iNumColumns),  eVote, 	fRatio, (4),  iColumn3Var, iNumColumns, iParticipant, iRow)  // AP
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (5), iNumColumns),  eVote,   fRatio, (5),  iColumn4Var, iNumColumns, iParticipant, iRow)  // Cash
					BREAK	
				ENDSWITCH
			BREAK
			
			CASE SUB_ARENA_DDERBY
				SWITCH iNumColumns
					CASE 6
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (1), iNumColumns), eVote,  fRatio,	(1), -1, iNumColumns, iParticipant, iRow)			// Vehicle Name
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (2), iNumColumns), eVote,  fRatio,	(2), iColumn1Var, iNumColumns, iParticipant, iRow)	// TimeS
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (3), iNumColumns), eVote,  fRatio,	(3), iColumn2Var, iNumColumns, iParticipant, iRow)  // Best lap
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (4), iNumColumns), eVote,  fRatio,	(4), iColumn3Var, iNumColumns, iParticipant, iRow)  // AP
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (5), iNumColumns), eVote,  fRatio,	(5), iColumn4Var, iNumColumns, iParticipant, iRow)  // Cash
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (6), iNumColumns), eVote,  fRatio,	(6), iColumn5Var, iNumColumns, iParticipant, iRow)  // Next/Rounds
					BREAK
					
					CASE 5
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (1), iNumColumns),  eVote, 	fRatio, (1),  -1, iNumColumns, iParticipant, iRow)			 // Vehicle Name
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (2), iNumColumns),  eVote, 	fRatio, (2),  iColumn1Var, iNumColumns, iParticipant, iRow)	 // TimeS
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (3), iNumColumns),  eVote, 	fRatio, (3),  iColumn2Var, iNumColumns, iParticipant, iRow)  // Best lap
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (4), iNumColumns),  eVote, 	fRatio, (4),  iColumn3Var, iNumColumns, iParticipant, iRow)  // AP
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (5), iNumColumns),  eVote,   fRatio, (5),  iColumn4Var, iNumColumns, iParticipant, iRow)  // Cash
					BREAK	
				ENDSWITCH
			BREAK
			
			CASE SUB_ARENA_FIRE
			CASE SUB_ARENA_MONSTER
				
				PRINTLN("WRITE 5446432, iColumn1Var  = ", iColumn1Var)
				PRINTLN("WRITE 5446432, iColumn2Var  = ", iColumn2Var)
				PRINTLN("WRITE 5446432, iColumn3Var  = ", iColumn3Var)
				PRINTLN("WRITE 5446432, iColumn4Var  = ", iColumn4Var)
				PRINTLN("WRITE 5446432, iColumn5Var  = ", iColumn5Var)
			
				SWITCH iNumColumns
					CASE 6
					
						iLastColumn = iColumn6Var
						IF IS_ROUNDS_MISSION_LBD(eSubMode)
							iLastColumn = iRoundScore
						ENDIF
					
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (1), iNumColumns), eVote,  fRatio,	(1), -1, iNumColumns, iParticipant, iRow)			// Vehicle Name
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (2), iNumColumns), eVote,  fRatio,	(2), iColumn1Var, iNumColumns, iParticipant, iRow)	// Kills
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (3), iNumColumns), eVote,  fRatio,	(3), iColumn2Var, iNumColumns, iParticipant, iRow) 	// Deaths
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (4), iNumColumns), eVote,  fRatio,	(4), iColumn4Var, iNumColumns, iParticipant, iRow)  // AP
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (5), iNumColumns), eVote,  fRatio,	(5), iColumn5Var, iNumColumns, iParticipant, iRow)  // Cash
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (6), iNumColumns), eVote,  fRatio,	(6), iLastColumn, iNumColumns, iParticipant, iRow)  // Next/Rounds
					BREAK
					
					CASE 5
					
						iLastColumn = iColumn5Var
						IF IS_ROUNDS_MISSION_LBD(eSubMode)
							iLastColumn = iRoundScore
						ENDIF
					
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (1), iNumColumns),  eVote, 	fRatio, (1), -1, iNumColumns, iParticipant, iRow)			// Vehicle Name
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (2), iNumColumns),  eVote, 	fRatio, (2), iColumn1Var, iNumColumns, iParticipant, iRow) 	// Kills
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (3), iNumColumns),  eVote, 	fRatio, (3), iColumn2Var, iNumColumns, iParticipant, iRow)  // Deaths
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (4), iNumColumns),  eVote, 	fRatio, (4), iColumn4Var, iNumColumns, iParticipant, iRow)  // AP
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (5), iNumColumns),  eVote,   fRatio, (5), iLastColumn, iNumColumns, iParticipant, iRow)  // Cash
					BREAK	
				ENDSWITCH
			
			BREAK
			
			CASE SUB_ARENA_CTF
			CASE SUB_ARENA_DETONATION
			CASE SUB_ARENA_GAMESMASTER
			CASE SUB_ARENA_TAG_TEAM
			
				SWITCH iNumColumns
					CASE 6
					
						iLastColumn = iColumn6Var
						IF IS_ROUNDS_MISSION_LBD(eSubMode)
							iLastColumn = iRoundScore
						ENDIF
					
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (1), iNumColumns), eVote,  fRatio,	(1), -1, iNumColumns, iParticipant, iRow)			// Vehicle Name
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (2), iNumColumns), eVote,  fRatio,	(2), iColumn1Var, iNumColumns, iParticipant, iRow)	// Flags D / Pts / Chk / Kills
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (3), iNumColumns), eVote,  fRatio,	(3), iColumn2Var, iNumColumns, iParticipant, iRow)  // Flags R / Goals / Kills / Tags
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (4), iNumColumns), eVote,  fRatio,	(4), iColumn4Var, iNumColumns, iParticipant, iRow)  // AP
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (5), iNumColumns), eVote,  fRatio,	(5), iColumn5Var, iNumColumns, iParticipant, iRow)  // Cash
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (6), iNumColumns), eVote,  fRatio,	(6), iLastColumn, iNumColumns, iParticipant, iRow)  // Next/Rounds
					BREAK
					
					CASE 5
					
						iLastColumn = iColumn5Var
						IF IS_ROUNDS_MISSION_LBD(eSubMode)
							iLastColumn = iRoundScore
						ENDIF
					
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (1), iNumColumns),  eVote, 	fRatio, (1),  -1, iNumColumns, iParticipant, iRow)			// Vehicle Name
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (2), iNumColumns),  eVote, 	fRatio, (2),  iColumn1Var, iNumColumns, iParticipant, iRow)	// Flags D / Pts / Chk / Kills
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (3), iNumColumns),  eVote, 	fRatio, (3),  iColumn2Var, iNumColumns, iParticipant, iRow) // Flags R / Goals / Kills / Tags
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (4), iNumColumns),  eVote, 	fRatio, (4),  iColumn4Var, iNumColumns, iParticipant, iRow) // AP
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (5), iNumColumns),  eVote,   fRatio, (5),  iLastColumn, iNumColumns, iParticipant, iRow) // Cash
					BREAK																																							
				ENDSWITCH
			
			BREAK
			
			CASE SUB_ARENA_CARNAGE	
			
				SWITCH iNumColumns
					CASE 6
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (1), iNumColumns), eVote,  fRatio,	(1), -1, iNumColumns, iParticipant, iRow)			// Vehicle Name
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (2), iNumColumns), eVote,  fRatio,	(2), iColumn1Var, iNumColumns, iParticipant, iRow)	// Kills
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (3), iNumColumns), eVote,  fRatio,	(3), -1, iNumColumns, iParticipant, iRow)  			// DMG
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (4), iNumColumns), eVote,  fRatio,	(4), iColumn3Var, iNumColumns, iParticipant, iRow)  // AP
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (5), iNumColumns), eVote,  fRatio,	(5), iColumn4Var, iNumColumns, iParticipant, iRow)  // Cash
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (6), iNumColumns), eVote,  fRatio,	(6), iColumn5Var, iNumColumns, iParticipant, iRow)  // Next/Rounds
					BREAK
					
					CASE 5
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (1), iNumColumns),  eVote, 	fRatio, (1),  -1, iNumColumns, iParticipant, iRow)			 // Vehicle Name
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (2), iNumColumns),  eVote, 	fRatio, (2),  iColumn1Var, iNumColumns, iParticipant, iRow)	 // Kills
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (3), iNumColumns),  eVote, 	fRatio, (3),  -1, iNumColumns, iParticipant, iRow)  		 // DMG
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (4), iNumColumns),  eVote, 	fRatio, (4),  iColumn3Var, iNumColumns, iParticipant, iRow)  // AP
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (5), iNumColumns),  eVote,   fRatio, (5),  iColumn4Var, iNumColumns, iParticipant, iRow)  // Cash
					BREAK	
				ENDSWITCH
			
			BREAK
			
			CASE SUB_ARENA_PASSBOMB
			
				SWITCH iNumColumns
					CASE 6
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (1), iNumColumns), eVote,  fRatio,	(1), -1, iNumColumns, iParticipant, iRow)			// Vehicle Name
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (2), iNumColumns), eVote,  fRatio,	(2), iCustomInt, iNumColumns, iParticipant, iRow)	// TimeS
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (3), iNumColumns), eVote,  fRatio,	(3), iColumn1Var, iNumColumns, iParticipant, iRow)  // BombTime
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (4), iNumColumns), eVote,  fRatio,	(4), iColumn3Var, iNumColumns, iParticipant, iRow)  // AP
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (5), iNumColumns), eVote,  fRatio,	(5), iColumn4Var, iNumColumns, iParticipant, iRow)  // Cash
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (6), iNumColumns), eVote,  fRatio,	(6), iColumn5Var, iNumColumns, iParticipant, iRow)  // Next/Rounds
					BREAK
					
					CASE 5
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (1), iNumColumns),  eVote, 	fRatio, (1),  -1, iNumColumns, iParticipant, iRow)			 // Vehicle Name
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (2), iNumColumns),  eVote, 	fRatio, (2),  iCustomInt, iNumColumns, iParticipant, iRow)	 // TimeS
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (3), iNumColumns),  eVote, 	fRatio, (3),  iColumn1Var, iNumColumns, iParticipant, iRow)  // BombTime
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (4), iNumColumns),  eVote, 	fRatio, (4),  iColumn3Var, iNumColumns, iParticipant, iRow)  // AP
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (5), iNumColumns),  eVote,   fRatio, (5),  iColumn4Var, iNumColumns, iParticipant, iRow)  // Cash
					BREAK	
				ENDSWITCH
			
			BREAK
			
			CASE SUB_KOTH

				DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (1), iNumColumns), eVote, fRatio,(1), iColumn1Var, iNumColumns, iParticipant, iRow) 
				DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (2), iNumColumns), eVote, fRatio,(2), iColumn2Var, iNumColumns, iParticipant, iRow)
				DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (3), iNumColumns), eVote, fRatio,(3), iColumn3Var, iNumColumns, iParticipant, iRow)// ratio
				DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (4), iNumColumns), eVote, fRatio,(4), iColumn4Var, iNumColumns, iParticipant, iRow)
				DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (5), iNumColumns), eVote, fRatio,(5), iColumn5Var, iNumColumns, iParticipant, iRow)
				DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (6), iNumColumns), eVote, fRatio,(6), iColumn6Var, iNumColumns, iParticipant, iRow)

			BREAK
			
			CASE SUB_DM_FFA
			CASE SUB_DM_TEAM
				SWITCH iNumColumns
					CASE 6
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (1), iNumColumns), eVote, fRatio, 	(1), iColumn1Var, iNumColumns, iParticipant, iRow) // veh name in races, column 1 in others
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (2), iNumColumns), eVote, fRatio,	(2), iColumn2Var, iNumColumns, iParticipant, iRow)
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (3), iNumColumns), eVote, fRatio,	(3), iColumn6Var, iNumColumns, iParticipant, iRow)// ratio
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (4), iNumColumns), eVote, fRatio,	(4), iColumn3Var, iNumColumns, iParticipant, iRow)
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (5), iNumColumns), eVote, fRatio,	(5), iColumn4Var, iNumColumns, iParticipant, iRow)
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (6), iNumColumns), eVote, fRatio,	(6), iColumn5Var, iNumColumns, iParticipant, iRow)
					BREAK
				ENDSWITCH
			BREAK
			
			CASE SUB_DM_UPDATE
				DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (1), iNumColumns), eVote, fRatio, 	(1), iColumn1Var, iNumColumns, iParticipant, iRow)//score 
				DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (2), iNumColumns), eVote, fRatio,	(2), iColumn2Var, iNumColumns, iParticipant, iRow)//kills
				DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (3), iNumColumns), eVote, fRatio,	(3), iColumn3Var, iNumColumns, iParticipant, iRow)//deaths
				DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (4), iNumColumns), eVote, fRatio,	(4), iColumn6Var, iNumColumns, iParticipant, iRow)//ratio
				DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (5), iNumColumns), eVote, fRatio,	(5), iColumn4Var, iNumColumns, iParticipant, iRow)//RP
				DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (6), iNumColumns), eVote, fRatio,	(6), iColumn5Var, iNumColumns, iParticipant, iRow)//Cash
			BREAK
			
			CASE SUB_KART
				DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (1), iNumColumns),  eVote,	fRatio, (1), iColumn1Var, iNumColumns, iParticipant, iRow)
				DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (2), iNumColumns),  eVote,	fRatio, (2), iColumn2Var, iNumColumns, iParticipant, iRow)
				DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (3), iNumColumns),  eVote,	fRatio, (3), iColumn4Var, iNumColumns, iParticipant, iRow)
			BREAK
			
			CASE SUB_HORDE
				SWITCH iNumColumns
					CASE 6
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (1), iNumColumns), eVote, fRatio, (1), iColumn1Var, iNumColumns, iParticipant, iRow) // veh name in races, column 1 in others
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (2), iNumColumns), eVote, fRatio,	(2), iColumn2Var, iNumColumns, iParticipant, iRow)
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (3), iNumColumns), eVote, fRatio,	(3), iColumn6Var, iNumColumns, iParticipant, iRow)// ratio
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (4), iNumColumns), eVote, fRatio,	(4), iColumn3Var, iNumColumns, iParticipant, iRow)
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (5), iNumColumns), eVote, fRatio,	(5), iColumn4Var, iNumColumns, iParticipant, iRow)
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (6), iNumColumns), eVote, fRatio,	(6), iColumn5Var, iNumColumns, iParticipant, iRow)
					BREAK
					
					CASE 5
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (1), iNumColumns),  eVote,	fRatio, (1), iColumn1Var, iNumColumns, iParticipant, iRow)
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (2), iNumColumns),  eVote,	fRatio, (2), iColumn2Var, iNumColumns, iParticipant, iRow)
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (3), iNumColumns),  eVote,	fRatio, (3), iColumn6Var, iNumColumns, iParticipant, iRow)
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (4), iNumColumns),  eVote,	fRatio, (4), iColumn3Var, iNumColumns, iParticipant, iRow)
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (5), iNumColumns),  eVote,  fRatio, (5), iColumn4Var, iNumColumns, iParticipant, iRow)
					BREAK			
					
					CASE 4
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (1), iNumColumns),  eVote,	fRatio, (1), iColumn1Var, iNumColumns, iParticipant, iRow)
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (2), iNumColumns),  eVote,	fRatio, (2), iColumn2Var, iNumColumns, iParticipant, iRow)
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (3), iNumColumns),  eVote,	fRatio, (3), iColumn6Var, iNumColumns, iParticipant, iRow)
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (4), iNumColumns),  eVote,	fRatio, (4), iColumn3Var, iNumColumns, iParticipant, iRow)
					BREAK	
					
					CASE 3
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (1), iNumColumns),  eVote,	fRatio, (1), iColumn1Var, iNumColumns, iParticipant, iRow)
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (2), iNumColumns),  eVote,	fRatio, (2), iColumn2Var, iNumColumns, iParticipant, iRow)
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (4), iNumColumns),  eVote,	fRatio, (3), iColumn3Var, iNumColumns, iParticipant, iRow)
					BREAK
					
				ENDSWITCH
			BREAK
			
			CASE SUB_MISSIONS_CTF
				IF SHOW_SCORE_COLUMN_ON_LEADERBOARD()
					DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (1), iNumColumns), eVote, fRatio, (1), iColumn1Var, iNumColumns, iParticipant, iRow) // Score
					DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (2), iNumColumns), eVote, fRatio,	(2), iColumn2Var, iNumColumns, iParticipant, iRow) // Kills
					DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (3), iNumColumns), eVote, fRatio,	(3), iColumn3Var, iNumColumns, iParticipant, iRow) // Deaths
					DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (4), iNumColumns), eVote, fRatio,	(4), iColumn4Var, iNumColumns, iParticipant, iRow) // RP
					DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (5), iNumColumns), eVote, fRatio,	(5), iColumn5Var, iNumColumns, iParticipant, iRow) // Cash
					IF IS_ROUNDS_MISSION_LBD(eSubMode)
		//				IF IS_PLAYLIST_ROUNDS_MISSION(eSubMode)
							// 2349496
							DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (6), iNumColumns), eVote, fRatio,	(6), iRoundScore, iNumColumns, iParticipant, iRow, FALSE, IS_ROUNDS_MISSION_LBD(eSubMode))
		//				ENDIF
					ELSE
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (6), iNumColumns), eVote, fRatio,	(6), iColumn6Var, iNumColumns, iParticipant, iRow) // Betting
					ENDIF
				ELSE
					DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (1), iNumColumns), eVote, fRatio,	(1), iColumn2Var, iNumColumns, iParticipant, iRow) // Kills
					DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (2), iNumColumns), eVote, fRatio,	(2), iColumn3Var, iNumColumns, iParticipant, iRow) // Deaths
					DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (3), iNumColumns), eVote, fRatio,	(3), iColumn4Var, iNumColumns, iParticipant, iRow) // RP
					DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (4), iNumColumns), eVote, fRatio,	(4), iColumn5Var, iNumColumns, iParticipant, iRow) // Cash
					IF IS_ROUNDS_MISSION_LBD(eSubMode)
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (5), iNumColumns), eVote, fRatio,	(5), iRoundScore, iNumColumns, iParticipant, iRow, FALSE, IS_ROUNDS_MISSION_LBD(eSubMode))
					ELSE
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (5), iNumColumns), eVote, fRatio,	(5), iColumn6Var, iNumColumns, iParticipant, iRow) // Betting
					ENDIF
				ENDIF
			BREAK
			
			CASE SUB_MISSIONS_NORMAL
			CASE SUB_MISSIONS_TIMED 
				DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 1, iNumColumns), eVote,  fRatio, 1, iColumn1Var, iNumColumns, iParticipant, iRow)
				DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 2, iNumColumns), eVote,  fRatio, 2, iColumn2Var, iNumColumns, iParticipant, iRow)
				DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 3, iNumColumns), eVote,  fRatio, 3, iColumn3Var, iNumColumns, iParticipant, iRow)
				DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 4, iNumColumns), eVote,  fRatio, 4, iColumn6Var, iNumColumns, iParticipant, iRow)
			BREAK
			
			CASE SUB_MISSIONS_HEIST
				DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 1, iNumColumns), eVote,  fRatio, 1, iColumn2Var, iNumColumns, iParticipant, iRow)
				DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 2, iNumColumns), eVote,  fRatio, 2, iColumn4Var, iNumColumns, iParticipant, iRow)
				
				// 2228343 On the scoreboard for heist setups please can we have the leader's cash entry read LEADER rather than $0
				lbdVarType = GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 3, iNumColumns) 
				IF iParticipant = g_iHeistLeaderPlayerID
					lbdVarType = VAR_TYPE_STRING
				ENDIF
				
				DRAW_A_COLUMN(eSubMode, lbdVars, Placement, lbdVarType, eVote,  fRatio, 3, iColumn5Var, iNumColumns, iParticipant, iRow) 
				DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 4, iNumColumns), eVote,  fRatio, 4, iColumn6Var, iNumColumns, iParticipant, iRow)
				IF IS_ROUNDS_MISSION_LBD(eSubMode)
					DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 5, iNumColumns), eVote,  fRatio, 5, iRoundScore, iNumColumns, iParticipant, iRow, FALSE, IS_ROUNDS_MISSION_LBD(eSubMode))
				ENDIF
			BREAK
			
			CASE SUB_FINAL_ROUNDS
				IF SHOW_SCORE_COLUMN_ON_LEADERBOARD()
				OR bIS_JUGGERNAUT()
					PRINTLN("3923725 A ")
					DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 1, iNumColumns), eVote,  fRatio, 1, iColumn1Var, iNumColumns, iParticipant, iRow) // Points
					DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 2, iNumColumns), eVote,  fRatio, 2, iColumn2Var, iNumColumns, iParticipant, iRow) // Kills
					DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 3, iNumColumns), eVote,  fRatio, 3, iColumn3Var, iNumColumns, iParticipant, iRow) // Deaths
					DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 4, iNumColumns), eVote,  fRatio, 4, iColumn4Var, iNumColumns, iParticipant, iRow) //RP
					DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 5, iNumColumns), eVote,  fRatio, 5, iColumn5Var, iNumColumns, iParticipant, iRow) // Cash
					DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 6, iNumColumns), eVote,  fRatio, 6, iRoundScore, iNumColumns, iParticipant, iRow, FALSE, IS_ROUNDS_MISSION_LBD(eSubMode))
				ELSE
					PRINTLN("3923725 B ")
					IF SHOW_TIME_COLUMN()
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 1, iNumColumns), eVote,  fRatio, 1, iColumn1Var, iNumColumns, iParticipant, iRow) // Time
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 2, iNumColumns), eVote,  fRatio, 2, iColumn3Var, iNumColumns, iParticipant, iRow) // Deaths
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 3, iNumColumns), eVote,  fRatio, 3, iColumn4Var, iNumColumns, iParticipant, iRow) //RP
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 4, iNumColumns), eVote,  fRatio, 4, iColumn5Var, iNumColumns, iParticipant, iRow) // Cash
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 5, iNumColumns), eVote,  fRatio, 5, iRoundScore, iNumColumns, iParticipant, iRow, FALSE, IS_ROUNDS_MISSION_LBD(eSubMode))
					ELSE
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 1, iNumColumns), eVote,  fRatio, 1, iColumn2Var, iNumColumns, iParticipant, iRow) // Kills
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 2, iNumColumns), eVote,  fRatio, 2, iColumn3Var, iNumColumns, iParticipant, iRow) // Deaths
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 3, iNumColumns), eVote,  fRatio, 3, iColumn4Var, iNumColumns, iParticipant, iRow) //RP
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 4, iNumColumns), eVote,  fRatio, 4, iColumn5Var, iNumColumns, iParticipant, iRow) // Cash
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 5, iNumColumns), eVote,  fRatio, 5, iRoundScore, iNumColumns, iParticipant, iRow, FALSE, IS_ROUNDS_MISSION_LBD(eSubMode))
					ENDIF
				ENDIF
			BREAK
			
			CASE SUB_MISSIONS_VERSUS
				// 2676798
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_IN_AND_OUT(g_FMMC_STRUCT.iRootContentIDHash)
				AND iTeam = 1
					lbdVarType = VAR_TYPE_STRING
				ELSE
					lbdVarType = GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 1, iNumColumns)
				ENDIF
				DRAW_A_COLUMN(eSubMode, lbdVars, Placement, lbdVarType, eVote,  fRatio, 1, iColumn1Var, iNumColumns, iParticipant, iRow) // Score
				DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 2, iNumColumns), eVote,  fRatio, 2, iColumn2Var, iNumColumns, iParticipant, iRow) // Kills
				DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 3, iNumColumns), eVote,  fRatio, 3, iColumn3Var, iNumColumns, iParticipant, iRow) // Deaths
				DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 4, iNumColumns), eVote,  fRatio, 4, iColumn4Var, iNumColumns, iParticipant, iRow) //RP
				DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 5, iNumColumns), eVote,  fRatio, 5, iColumn5Var, iNumColumns, iParticipant, iRow) // Cash
				IF IS_PLAYLIST_ROUNDS_MISSION(eSubMode)
				OR IS_ROUNDS_MISSION_LBD(eSubMode)
					DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 6, iNumColumns), eVote,  fRatio, 6, iRoundScore, iNumColumns, iParticipant, iRow, FALSE, IS_ROUNDS_MISSION_LBD(eSubMode))
				ELSE
					DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 6, iNumColumns), eVote,  fRatio, 6, iColumn6Var, iNumColumns, iParticipant, iRow) // Betting
				ENDIF
			BREAK
			
			CASE SUB_MISSIONS_NO_HUD
			CASE SUB_MISSIONS_LTS
				DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 1, iNumColumns), eVote,  fRatio, 1, iColumn2Var, iNumColumns, iParticipant, iRow) // Kills
				DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 2, iNumColumns), eVote,  fRatio, 2, iColumn3Var, iNumColumns, iParticipant, iRow) // Deaths
				DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 3, iNumColumns), eVote,  fRatio, 3, iColumn4Var, iNumColumns, iParticipant, iRow) //RP
				DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 4, iNumColumns), eVote,  fRatio, 4, iColumn5Var, iNumColumns, iParticipant, iRow) // Cash
				IF IS_PLAYLIST_ROUNDS_MISSION(eSubMode)
				OR IS_ROUNDS_MISSION_LBD(eSubMode)
					DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 5, iNumColumns), eVote,  fRatio, 5, iRoundScore, iNumColumns, iParticipant, iRow, FALSE, IS_ROUNDS_MISSION_LBD(eSubMode))
				ELSE
					DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 5, iNumColumns), eVote,  fRatio, 5, iColumn6Var, iNumColumns, iParticipant, iRow) // Betting
				ENDIF
				IF IS_ROUNDS_MISSION_LBD(eSubMode)
				AND NOT IS_PLAYLIST_ROUNDS_MISSION(eSubMode)
	//				DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(eSubMode, 6, iNumColumns), eVote,  fRatio, 6, iRoundScore, iNumColumns, iParticipant, iRow, FALSE, IS_ROUNDS_MISSION_LBD(eSubMode)) // Rounds
				ENDIF
			BREAK
			
			CASE SUB_PLAYLIST
				SWITCH iNumColumns
					CASE 6
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (1), iNumColumns), eVote,  fRatio,	(1), iColumn2Var, iNumColumns, iParticipant, iRow)
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (2), iNumColumns), eVote,  fRatio,	(2), iColumn3Var, iNumColumns, iParticipant, iRow)
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (3), iNumColumns), eVote,  fRatio,	(3), iColumn4Var, iNumColumns, iParticipant, iRow)
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (4), iNumColumns), eVote,  fRatio,	(4), iColumn5Var, iNumColumns, iParticipant, iRow)
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (5), iNumColumns), eVote,  fRatio,	(5), iColumn6Var, iNumColumns, iParticipant, iRow)
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (6), iNumColumns), eVote,  fRatio,	(6), iColumn1Var, iNumColumns, iParticipant, iRow) // just votes
					BREAK
					CASE 5
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (1), iNumColumns), eVote,  fRatio,	(1), iColumn2Var, iNumColumns, iParticipant, iRow)
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (2), iNumColumns), eVote,  fRatio,	(2), iColumn3Var, iNumColumns, iParticipant, iRow)
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (3), iNumColumns), eVote,  fRatio,	(3), iColumn4Var, iNumColumns, iParticipant, iRow)
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (4), iNumColumns), eVote,  fRatio,	(4), iColumn6Var, iNumColumns, iParticipant, iRow)
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (5), iNumColumns), eVote,  fRatio,	(5), iColumn1Var, iNumColumns, iParticipant, iRow)
					BREAK
					CASE 4
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (1), iNumColumns), eVote,  fRatio,	(1), iColumn2Var, iNumColumns, iParticipant, iRow)
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (2), iNumColumns), eVote,  fRatio,	(2), iColumn3Var, iNumColumns, iParticipant, iRow)
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (3), iNumColumns), eVote,  fRatio,	(3), iColumn6Var, iNumColumns, iParticipant, iRow)
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (4), iNumColumns), eVote,  fRatio,	(4), iColumn1Var, iNumColumns, iParticipant, iRow)
					BREAK																											
					CASE 3
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (1), iNumColumns), eVote,  fRatio,	(1), iColumn2Var, iNumColumns, iParticipant, iRow)
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (2), iNumColumns), eVote,  fRatio,	(2), iColumn6Var, iNumColumns, iParticipant, iRow)
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (3), iNumColumns), eVote,  fRatio,	(3), iColumn1Var, iNumColumns, iParticipant, iRow)
					BREAK		
				ENDSWITCH
			BREAK
			
			CASE SUB_FREEMODE
			CASE SUB_ARCADE_CNC
				
				SWITCH iNumColumns
					CASE 1
						DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, 1, iNumColumns), eVote, fRatio, 1, iColumn1Var, iNumColumns, iParticipant, iRow)
					BREAK
				ENDSWITCH
			BREAK
		ENDSWITCH		
	ENDIF
ENDPROC

PROC DO_RESTART_HELP(LBD_SUB_MODE eSubMode)
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_FADE_PRIORITY_HIGH)
	SWITCH eSubMode
		CASE SUB_ARENA_CARNAGE
		CASE SUB_DM_FFA
		CASE SUB_DM_TEAM
		CASE SUB_DM_VEH
//		CASE SUB_HORDE
		CASE SUB_RACE_NORMAL
		CASE SUB_RACE_GTA
		CASE SUB_RACE_RALLY
		CASE SUB_RACE_BASEJUMP
		CASE SUB_RACE_FOOT
		CASE SUB_RACE_STUNT
		CASE SUB_RACE_AGG_BEST
		CASE SUB_RACE_AGG_TIME
		CASE SUB_RACE_TARGET
//		CASE SUB_MISSIONS_NORMAL
//		CASE SUB_MISSIONS_TIMED
//		CASE SUB_PLAYLIST
		CASE SUB_ARENA_ARCADE
		CASE SUB_ARENA_CTF
		CASE SUB_ARENA_DDERBY
		CASE SUB_ARENA_DETONATION
		CASE SUB_ARENA_FIRE
		CASE SUB_ARENA_GAMESMASTER
		CASE SUB_ARENA_MONSTER
		CASE SUB_ARENA_PASSBOMB
		CASE SUB_ARENA_TAG_TEAM
		CASE SUB_KOTH
			IF IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet4, BI_FM_NMH4_DO_REPLAY_HELP)
				IF NOT DONE_REPLAY_JOB_HELP()
					PRINT_HELP("FM_RETRY_INV")
					PRINTLN("DO_RESTART_HELP ")
					SET_DONE_REPLAY_JOB_HELP_HELP() 
				ENDIF
			ENDIF
		BREAK
	 ENDSWITCH
	 SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_FADE)
ENDPROC



PROC STORE_GAMER_HANDLES(LBD_VARS& lbdVars, PLAYER_INDEX playerId, INT i)
//	IF NOT IS_BIT_SET(lbdVars.iHandleBitSet, i)
		lbdVars.aGamer[i] = GET_GAMER_HANDLE_PLAYER(playerId)
//		PRINTLN("STORE_GAMER_HANDLES,  i = ", i)
//		IF IS_GAMER_HANDLE_VALID(lbdVars.aGamer[i])
//			PRINTLN("VALID,  iPlayer = ", i)
//		ENDIF
//		SET_BIT(lbdVars.iHandleBitSet, i)
//	ENDIF
ENDPROC

PROC DEFINE_LBD_NAMES_ARRAY(LBD_VARS& lbdVars, TEXT_LABEL_63& tl63_ParticipantNames[], BOOL bGamerhandles)
	INT iParticipant
	PLAYER_INDEX PlayerId
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant		
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
			PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))
			IF IS_NET_PLAYER_OK(PlayerId, FALSE)			
				IF NOT DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(PlayerId)
				AND NOT IS_PLAYER_SCTV(PlayerId)
					tl63_ParticipantNames[iparticipant] = GET_PLAYER_NAME(PlayerId)
					IF bGamerhandles
						lbdVars.aGamer[iParticipant] = GET_GAMER_HANDLE_PLAYER(playerId)
						PRINTLN(" [CS_DPAD] DEFINE_LBD_NAMES_ARRAY, GET_GAMER_HANDLE_PLAYER iparticipant = ")
					ENDIF
					PRINTLN(" [CS_DPAD] DEFINE_LBD_NAMES_ARRAY,  iparticipant = ", iparticipant, " tl63_ParticipantNames = ", tl63_ParticipantNames[iparticipant])
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC GRAB_GAMER_HANDLES(LBD_VARS& lbdVars)
	INT iParticipant
	PLAYER_INDEX PlayerId
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant		
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
			PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))
			IF IS_NET_PLAYER_OK(PlayerId, FALSE)			
				IF NOT DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(PlayerId)
				AND NOT IS_PLAYER_SCTV(PlayerId)
					lbdVars.aGamer[iParticipant] = GET_GAMER_HANDLE_PLAYER(playerId)
					PRINTLN(" [CS_DPAD] DEFINE_LBD_NAMES_ARRAY, GET_GAMER_HANDLE_PLAYER iparticipant = ")
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC SHOW_VOTE_HELP()
	IF GET_MP_INT_CHARACTER_STAT(MP_STAT_HELP_THUMB_VOTE) < 3
		IF NOT IS_SCRIPT_HUD_DISPLAYING(HUDPART_AWARDS)
			PRINT_HELP_FOREVER("THUMB_VOTE")
		ENDIF
	ENDIF
ENDPROC

PROC DRAW_MISSION_END_BOX(END_SCREEN_MP_GAME_TYPE esmgtType, INT iBetWon, BOOL bShowBets, INT iXPToShow, BOOL bShowXP, INT iTime, INT iPersonalBest, INT iDifference, BOOL bShowRaceTimes, INT iMyPos, INT iTotalPos, BOOL bShowPos, BOOL bShowWorldRec, BOOL bShowPBRec, BOOL bThumbs = FALSE, INT iScore = -1, INT iCash = -1)

	ADD_MISSION_SUMMARY_TITLE_STRING(g_FMMC_STRUCT.tl63MissionName)
	
	// RP
	IF iXPToShow = 0 
		bShowXP = FALSE
	ENDIF
	IF bShowXP
		IF iXPToShow <> 0		
			ADD_MISSION_SUMMARY_STAT_WITH_INT("MBOX_REP", iXPToShow)
		ENDIF
	ENDIF
	
	// Cash
	IF iCash > 0
		ADD_MISSION_SUMMARY_STAT_WITH_INT("MBOX_CSH", iCash)
	ENDIF
	
	// Job Points
	ADD_MISSION_SUMMARY_STAT_WITH_INT("MBOX_JOB", GET_POINTS_FOR_POSITION(iMyPos))
	
	// Score
	IF iScore <> -1
		ADD_MISSION_SUMMARY_STAT_WITH_INT("MBOX_SCO", iScore)
	ENDIF
	
	// Scores
	IF bShowWorldRec
		//New World Record!!!
		IF iTime > 0
			ADD_MISSION_SUMMARY_STAT_STRING_AND_CASH_OF_TYPE("MBOX_NEWWR", iTime, ESEF_TIME_M_S_MS)
		ENDIF
	ENDIF
	
	// Bets
	IF bShowBets
		IF iBetWon >= 0
			ADD_MISSION_SUMMARY_STAT_STRING_AND_CASH_OF_TYPE("MBOX_BET_W", iBetWon, ESEF_DOLLAR_VALUE_BLUEBACK)
		ELSE
			ADD_MISSION_SUMMARY_STAT_STRING_AND_CASH_OF_TYPE("MBOX_BET_L", iBetWon, ESEF_DOLLAR_VALUE_REDBACK)
		ENDIF
	ENDIF
	
	IF bShowPos
		ADD_MISSION_SUMMARY_END_COMPLETION_FRACTION("MBOX_POS",iMyPos,iTotalPos)
	ENDIF
	
	IF bShowPBRec
	AND NOT bShowWorldRec
		//New Personal Best!!!
		IF iTime > 0
			ADD_MISSION_SUMMARY_STAT_STRING_AND_CASH_OF_TYPE("MBOX_NEWPB", iTime, ESEF_TIME_M_S_MS)
		ENDIF
	ENDIF
	
	IF bShowWorldRec OR bShowPBRec
		bShowRaceTimes = FALSE 
	ENDIF
	
	IF bShowRaceTimes
		//Time
		IF iTime > 0
			ADD_MISSION_SUMMARY_STAT_STRING_AND_CASH_OF_TYPE("MBOX_TIME", iTime, ESEF_TIME_M_S_MS)
		ENDIF
		//Personal Best
		IF iPersonalBest > 0
			ADD_MISSION_SUMMARY_STAT_STRING_AND_CASH_OF_TYPE("MBOX_PB", iPersonalBest, ESEF_TIME_M_S_MS)
		ENDIF
		//Difference
		IF iDifference > 0
			ADD_MISSION_SUMMARY_STAT_STRING_AND_CASH_OF_TYPE("MBOX_DIFF", iDifference, ESEF_TIME_M_S_MS)
		ENDIF
	ENDIF
	
	IF bThumbs
		ADD_MISSION_THUMB_VOTES(esmgtType)
//		SHOW_VOTE_HELP()
	ENDIF
ENDPROC

PROC DRAW_CREW_TAGS(GAMER_HANDLE& gamerHandle[], LBD_SUB_MODE eSubMode, LEADERBOARD_PLACEMENT_TOOLS& Placement, TEXT_LABEL_63& participantNames[], INT iParticipant, INT iRow)
	IF eSubMode <> SUB_SHOOT_RANGE
	
		FLOAT fNameWidth

		SET_TEXT_STYLE(Placement.aStyle.TS_LBDPLYR)
		IF NOT IS_STRING_NULL_OR_EMPTY(participantNames[iParticipant])
		
			fNameWidth = GET_STRING_WIDTH_PLAYER_NAME(participantNames[iParticipant])
			
			#IF IS_DEBUG_BUILD
				IF fNameWidth = 0.0
					PRINTLN("DRAW_CREW_TAGS, fNameWidth = 0.0 ")
				ENDIF
			#ENDIF

			IF IS_GAMER_HANDLE_VALID(gamerHandle[iParticipant]) // problem drawing spectator crew tags on lbd with this check???
			
				// 2111163
				// XB1 check individual player and hide tag if we do not have privileges
				IF IS_XBOX_PLATFORM()
				#IF FEATURE_GEN9_RELEASE
				OR IS_PROSPERO_VERSION()
				#ENDIF
					IF IS_GAMER_HANDLE_VALID(gamerHandle[iParticipant])
						IF NOT NETWORK_CAN_VIEW_GAMER_USER_CONTENT(gamerHandle[iParticipant])
							EXIT
						ENDIF
					ENDIF
				ELSE
					IF NOT NETWORK_HAVE_USER_CONTENT_PRIVILEGES()
						EXIT
					ENDIF
				ENDIF
				
				DRAW_CREW_TAG_GAMER(gamerHandle[iParticipant], (BIG_PLAYER_NAMES_X + (fNameWidth + fCrewOffset)), (VARIABLE_BASE_CREW+(TO_FLOAT(iRow) * RECT_Y_OFFSET)), fCrewScale)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC DRAW_LAMAR_ROW(LBD_VARS& lbdVars, LEADERBOARD_PLACEMENT_TOOLS& Placement, LBD_SUB_MODE eSubMode, INT iNumColumns)

	BOOL bSelected
	
	IF lbdVars.bDrawLamar
	
		IF g_i_ActualLeaderboardPlayerSelected = lbdVars.iLamarRow
			bSelected = TRUE
		ELSE
			bSelected = FALSE
		ENDIF
		
		lbdVars.tlRaceVeh = lbdVars.sLamarVeh
		
		// DRAW_HORZ_VARIABLE_BARS
		// 50% black
		Placement.RectPlacement[RECT_PLACEMENT_BLACK_POS].x = VAR_RECT_X
		Placement.RectPlacement[RECT_PLACEMENT_BLACK_POS].y = COLUMN_RECT_Y + (TO_FLOAT(lbdVars.iLamarRow) * RECT_Y_OFFSET)
		Placement.RectPlacement[RECT_PLACEMENT_BLACK_POS].w = VAR_RECT_W
		Placement.RectPlacement[RECT_PLACEMENT_BLACK_POS].h = RECT_HEIGHT_CONSTANT
		
		IF lbdVars.iLamarRow%2 = 0
			Placement.RectPlacement[RECT_PLACEMENT_BLACK_POS].a = SIXTY_ALPHA
		ELSE
			Placement.RectPlacement[RECT_PLACEMENT_BLACK_POS].a = CEIL( TO_FLOAT(SIXTY_ALPHA) * 0.9)
		ENDIF
		
		DRAW_RECTANGLE(Placement.RectPlacement[RECT_PLACEMENT_BLACK_POS])
		
		// Colour
		SET_RECT_TO_THIS_HUD_COLOUR(Placement.RectPlacement[RECT_PLACEMENT_HORZ], HUD_COLOUR_NET_PLAYER1)	
		Placement.RectPlacement[RECT_PLACEMENT_HORZ].x = PLAYER_NAME_RECT_X
		Placement.RectPlacement[RECT_PLACEMENT_HORZ].y = COLUMN_RECT_Y + (TO_FLOAT(lbdVars.iLamarRow) * RECT_Y_OFFSET)
		Placement.RectPlacement[RECT_PLACEMENT_HORZ].w = PLAYER_NAME_RECT_W
		Placement.RectPlacement[RECT_PLACEMENT_HORZ].h = RECT_HEIGHT_CONSTANT
		IF bSelected
			Placement.RectPlacement[RECT_PLACEMENT_HORZ].a = HUNDRED_ALPHA
		ELSE
			Placement.RectPlacement[RECT_PLACEMENT_HORZ].a = THIRTY_ALPHA
		ENDIF
		DRAW_RECTANGLE(Placement.RectPlacement[RECT_PLACEMENT_HORZ])
		
		// DRAW_WEE_POSITION_RECTS
		// 50% black
		Placement.RectPlacement[RECT_PLACEMENT_BLACK_POS].x = RED_X
		Placement.RectPlacement[RECT_PLACEMENT_BLACK_POS].y = COLUMN_RECT_Y + (TO_FLOAT(lbdVars.iLamarRow) * RECT_Y_OFFSET)
		Placement.RectPlacement[RECT_PLACEMENT_BLACK_POS].w = RED_W
		Placement.RectPlacement[RECT_PLACEMENT_BLACK_POS].h = RECT_HEIGHT_CONSTANT
		Placement.RectPlacement[RECT_PLACEMENT_BLACK_POS].a = HUNDRED_ALPHA
		DRAW_RECTANGLE(Placement.RectPlacement[RECT_PLACEMENT_BLACK_POS])
		
		IF NOT IS_STRING_NULL_OR_EMPTY(lbdVars.sLamarHeadshotTxd)
		AND NOT ARE_STRINGS_EQUAL("NULL", lbdVars.sLamarHeadshotTxd)
			REQUEST_STREAMED_TEXTURE_DICT(lbdVars.sLamarHeadshotTxd)
			IF HAS_STREAMED_TEXTURE_DICT_LOADED(lbdVars.sLamarHeadshotTxd)
				Placement.SpritePlacement[SPRITE_PLACEMENT_HEAD_POS].y = COLUMN_RECT_Y + (TO_FLOAT(lbdVars.iLamarRow) * RECT_Y_OFFSET)
				DRAW_2D_SPRITE(lbdVars.sLamarHeadshotTxd, lbdVars.sLamarHeadshotTxd, Placement.SpritePlacement[SPRITE_PLACEMENT_HEAD_POS], FALSE, DEFAULT, GFX_ORDER_AFTER_FADE)
				SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_FADE)
			ENDIF
		ENDIF
		
		
		//little black box over portrait to darken image
		Placement.RectPlacement[RECT_PLACEMENT_BLACK_POS].x = RED_X
		Placement.RectPlacement[RECT_PLACEMENT_BLACK_POS].y = COLUMN_RECT_Y + (TO_FLOAT(lbdVars.iLamarRow) * RECT_Y_OFFSET)
		Placement.RectPlacement[RECT_PLACEMENT_BLACK_POS].w = RED_W
		Placement.RectPlacement[RECT_PLACEMENT_BLACK_POS].h = RECT_HEIGHT_CONSTANT
		Placement.RectPlacement[RECT_PLACEMENT_BLACK_POS].a = FIFTY_ALPHA
		DRAW_RECTANGLE(Placement.RectPlacement[RECT_PLACEMENT_BLACK_POS])
		
		
		//Small 4px couloured box next to position box
		Placement.RectPlacement[RECT_PLACEMENT_COLOUR_POS].x = THIN_COLOUR_X
		Placement.RectPlacement[RECT_PLACEMENT_COLOUR_POS].y = COLUMN_RECT_Y + (TO_FLOAT(lbdVars.iLamarRow) * RECT_Y_OFFSET)
		Placement.RectPlacement[RECT_PLACEMENT_COLOUR_POS].w = THIN_COLOUR_W
		Placement.RectPlacement[RECT_PLACEMENT_COLOUR_POS].h = RECT_HEIGHT_CONSTANT
		Placement.RectPlacement[RECT_PLACEMENT_COLOUR_POS].a = HUNDRED_ALPHA
		SET_RECT_TO_THIS_HUD_COLOUR(Placement.RectPlacement[RECT_PLACEMENT_COLOUR_POS], HUD_COLOUR_NET_PLAYER1)	
		DRAW_RECTANGLE(Placement.RectPlacement[RECT_PLACEMENT_COLOUR_POS])
		
		
		
		// DRAW_POSITIONS
//		Placement.TextPlacement[TEXT_PLACEMENT_POSITION] = GET_NAME_SLOT_TEXT_PLACEMENT_LB(Placement.TextPlacement[TEXT_PLACEMENT_POSITION], lbdVars.iLamarRow)
		Placement.TextPlacement[TEXT_PLACEMENT_POSITION].x = POSITION_POS_X 
		Placement.TextPlacement[TEXT_PLACEMENT_POSITION].y = VARIABLE_BASE_Y+(TO_FLOAT(lbdVars.iLamarRow) * RECT_Y_OFFSET)	
		SET_TEXT_STYLE(Placement.aStyle.TS_LBD)
		SET_TEXT_WHITE(Placement.aStyle.TS_LBD)
		DRAW_TEXT_WITH_NUMBER(Placement.TextPlacement[TEXT_PLACEMENT_POSITION], Placement.aStyle.TS_LBD, "NUMBER", lbdVars.iLamarPos, FONT_CENTRE)
		
		// DRAW_PLAYER_NAMES
		SET_STANDARD_LBD_PLYR_NAME_TEXT(Placement.aStyle.TS_LBDPLYR)
		SET_TEXT_STYLE(Placement.aStyle.TS_LBDPLYR)
		Placement.TextPlacement[TEXT_PLACEMENT_PLAYER_NAMES].x = BIG_PLAYER_NAMES_X
		Placement.TextPlacement[TEXT_PLACEMENT_PLAYER_NAMES].y = VARIABLE_BASE_Y_NAMES+(TO_FLOAT(lbdVars.iLamarRow) * RECT_Y_OFFSET)	
		lbdVars.tlDisplayStr = GET_TUTORIAL_PED_NAME()
	//	DRAW_TEXT(Placement.TextPlacement[TEXT_PLACEMENT_PLAYER_NAMES], Placement.aStyle.TS_LBDPLYR, lbdVars.tlDisplayStr)
		DRAW_TEXT_WITH_PLAYER_NAME(Placement.TextPlacement[TEXT_PLACEMENT_PLAYER_NAMES], Placement.aStyle.TS_LBDPLYR, lbdVars.tlDisplayStr, "STRING", HUD_COLOUR_WHITE)
		
		DRAW_RANK_BADGE_FONT_LEADERBOARD(LAMAR_RANK, RANK_BADGE_X, RANK_BADGE_Y+(TO_FLOAT(lbdVars.iLamarRow) * RECT_Y_OFFSET), RANKDISPLAYTYPE_FULL_WITH_BOX)
		
		// POPULATE_COLUMNS
		IF lbdVars.bLamarShowBestlap
			DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (1), iNumColumns), LBD_VOTE_NOT_USED, 0,	(1), 0, iNumColumns, -1, lbdVars.iLamarRow, TRUE)
			DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (2), iNumColumns), LBD_VOTE_NOT_USED, 0,	(2), lbdVars.iLamarFinishTime , iNumColumns, -1, lbdVars.iLamarRow, TRUE) //finish time
			DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (3), iNumColumns), LBD_VOTE_NOT_USED, 0,	(3), lbdVars.iLamarBestLap, iNumColumns, -1, lbdVars.iLamarRow, TRUE) // best lap
			DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (4), iNumColumns), LBD_VOTE_NOT_USED, 0,	(4), lbdVars.iLamarXP, iNumColumns, -1, lbdVars.iLamarRow, TRUE)
			DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (5), iNumColumns), LBD_VOTE_NOT_USED, 0,	(5), lbdVars.iLamarCash, iNumColumns, -1, lbdVars.iLamarRow, TRUE)
			DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (6), iNumColumns), LBD_VOTE_NOT_USED, 0,	(6), 0, iNumColumns, -1, lbdVars.iLamarRow, TRUE)
		ELSE
			DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (1), iNumColumns), LBD_VOTE_NOT_USED, 0,	(1), 0, iNumColumns, -1, lbdVars.iLamarRow, TRUE)
			DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (2), iNumColumns), LBD_VOTE_NOT_USED, 0,	(2), lbdVars.iLamarFinishTime , iNumColumns, -1, lbdVars.iLamarRow, TRUE) //finish time
			DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (3), iNumColumns), LBD_VOTE_NOT_USED, 0,	(3), lbdVars.iLamarXP, iNumColumns, -1, lbdVars.iLamarRow, TRUE) // RP
			DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (4), iNumColumns), LBD_VOTE_NOT_USED, 0,	(4), lbdVars.iLamarCash, iNumColumns, -1, lbdVars.iLamarRow, TRUE) // CASH
			DRAW_A_COLUMN(eSubMode, lbdVars, Placement, GET_COLUMN_VAR_TYPE(lbdVars, eSubMode, (5), iNumColumns), LBD_VOTE_NOT_USED, 0,	(5), 0, iNumColumns, -1, lbdVars.iLamarRow, TRUE) // BETS
		ENDIF
		
		// DRAW_VEHICLE_BLOBS
		Placement.SpritePlacement[SPRITE_PLACEMENT_VEHICLE_BLOB].x = BLOB_X  
		Placement.SpritePlacement[SPRITE_PLACEMENT_VEHICLE_BLOB].y = BLOB_Y+(TO_FLOAT(lbdVars.iLamarRow) * RECT_Y_OFFSET) 
		Placement.SpritePlacement[SPRITE_PLACEMENT_VEHICLE_BLOB].w = BLOB_W 
		Placement.SpritePlacement[SPRITE_PLACEMENT_VEHICLE_BLOB].h = BLOB_H 
		Placement.SpritePlacement[SPRITE_PLACEMENT_VEHICLE_BLOB].a = 255
		Placement.SpritePlacement[SPRITE_PLACEMENT_VEHICLE_BLOB].r = 8
		Placement.SpritePlacement[SPRITE_PLACEMENT_VEHICLE_BLOB].g = 8
		Placement.SpritePlacement[SPRITE_PLACEMENT_VEHICLE_BLOB].b = 8
		DRAW_2D_SPRITE("MPCarHUD", "Leaderboard_Car_Colour_Icon_SingleColour", Placement.SpritePlacement[SPRITE_PLACEMENT_VEHICLE_BLOB], FALSE, HIGHLIGHT_OPTION_NORMAL, GFX_ORDER_AFTER_FADE)
		SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_FADE)
	ENDIF
ENDPROC

PROC DRAW_TEAM_ROWS(LBD_VARS &lbdVars, LEADERBOARD_PLACEMENT_TOOLS& Placement, LBD_SUB_MODE eSubMode, INT iTeamScore, INT iNumColumns, INT iLocalTeam, INT iEnemyTeam, INT iTeamRow, INT iTeamNamesRow, BOOL& bDraw, 
					BOOL bTeammode)
	INT iNewTeamRow 		= (iTeamRow - g_i_LeaderboardShift)
	INT iNewTeamNamesRow 	= (iTeamNamesRow - g_i_LeaderboardShift)
	
	IF bTeammode
		IF NOT bDraw

			IF iNewTeamRow >= 0
			AND iNewTeamRow <= MAX_ROWS_DRAWN
				DRAW_TEAM_SCORES(eSubMode, Placement, IS_SUBMODE_RACE(eSubMode),  iTeamScore, iNewTeamRow, iNumColumns, lbdVars.iTeamPos)
				DRAW_RECT_LONG_TEAM_BARS(Placement, eSubMode, iNewTeamRow, iLocalTeam, iEnemyTeam)
			ENDIF
			
			IF iNewTeamNamesRow >= 0
			AND iNewTeamNamesRow <= MAX_ROWS_DRAWN
				DRAW_TEAM_NAMES_BARS(lbdVars, Placement, iNewTeamNamesRow, lbdVars.iTeamPos)//, iTeam)
			ENDIF
			
			lbdVars.iTeamPos++
			bDraw = TRUE
		ENDIF
	ENDIF
ENDPROC

PROC PRELOAD_HEADSHOTS()
	INT i
	REPEAT NUM_NETWORK_PLAYERS i
		IF NOT IS_STRING_NULL_OR_EMPTY(g_s_HeadshotLbd[i])
			REQUEST_STREAMED_TEXTURE_DICT(g_s_HeadshotLbd[i])
			PRINTLN("PRELOAD_HEADSHOTS, g_s_HeadshotLbd[iPlayer] = ", g_s_HeadshotLbd[i], " i = ", i)
		ENDIF
	ENDREPEAT
ENDPROC

PROC POPULATE_HEADSHOTS_LBD(PLAYER_INDEX playerId)

	PEDHEADSHOT_ID pedHeadshots[NUM_NETWORK_PLAYERS]
	
	INT iPlayer = NATIVE_TO_INT(playerId)
	
	IF iPlayer <> -1
		IF IS_STRING_NULL_OR_EMPTY(g_s_HeadshotLbd[iPlayer])

			pedHeadshots[iPlayer] = Get_HeadshotID_For_Player(PlayerId)

			IF pedHeadshots[iPlayer] <> NULL
				g_s_HeadshotLbd[iPlayer] = GET_PEDHEADSHOT_TXD_STRING(pedHeadshots[iPlayer])
				PRINTLN("POPULATE_COLUMN_LBD, POPULATE_HEADSHOTS, g_s_HeadshotLbd[iPlayer] = ", g_s_HeadshotLbd[iPlayer], " iPlayer = ", iPlayer)
				CPRINTLN(DEBUG_MISSION, "POPULATE_COLUMN_LBD, POPULATE_HEADSHOTS, g_s_HeadshotLbd[iPlayer] = ", g_s_HeadshotLbd[iPlayer], " iPlayer = ", iPlayer)
			ENDIF
			
		ENDIF
	ENDIF
ENDPROC


PROC POPULATE_COLUMN_LBD	(LBD_VARS &lbdVars, LEADERBOARD_PLACEMENT_TOOLS& Placement, LBD_SUB_MODE eSubMode, LBD_VOTE eVote, TEXT_LABEL_63& participantNames[], INT iNumColumns,
							FLOAT fRatio, INT iColumn1Var, INT iColumn2Var, INT iColumn3Var, INT iColumn4Var, INT iColumn5Var, INT iColumn6Var,
							INT iLocalTeamForColour, INT iEnemyTeamForColour,
							INT iRow, INT iRank, INT iParticipant, INT iBadgeRank, PLAYER_INDEX playerId, 
							BOOL bTeamMode, BOOL& bDrawTeamRow, INT iTeamScore, INT iTeamRow, INT iTeamRank = 0, INT iRoundScore = 0, INT iTeamNamesRow = 0, INT iTeam = -1, INT iCustomInt = -1, INT iLBDTeam = -1)
							
	IF NOT IS_IT_OK_TO_DRAW_LEADERBOARD(TRUE)
		PRINTLN("[2056544] LBD DEBUG - POPULATE_COLUMN_LBD - IS_IT_OK_TO_DRAW_LEADERBOARD = FALSE")
		EXIT
	ENDIF
	PRINTLN("[2056544] LBD DEBUG - POPULATE_COLUMN_LBD - IS_IT_OK_TO_DRAW_LEADERBOARD = TRUE ")
	
	BOOL bSpectator = FALSE
	INT iNewRow
	INT iPlayer = NATIVE_TO_INT(playerId)
	
//	TEXT_LABEL_23 strHeadshotTxd
	
	IF iParticipant <> -1 // PT Fix (1046023)
	
		IF IS_STRING_NULL_OR_EMPTY(participantNames[iParticipant])
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
				PLAYER_INDEX tempplayer
				tempplayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))
				participantNames[iParticipant] = GET_PLAYER_NAME(tempplayer)
				STORE_GAMER_HANDLES(lbdVars, tempplayer, iParticipant)
				PRINTLN("DEFINE_NAMES_ARRAY,  iparticipant = ", iParticipant, " tl63_ParticipantNames = ", participantNames[iParticipant])
			ENDIF
		ENDIF
		
		IF IS_STRING_NULL_OR_EMPTY(participantNames[iParticipant])
			participantNames[iParticipant] = "Unknown Player"
		ENDIF
		
		iNewRow = (iRow - g_i_LeaderboardShift)
		
		IF bTeamMode
			// For team modes use team position
			iRank = iTeamRank
		ELSE			
			// LAMAR, Shift everyone below Lamar down 
			IF lbdVars.bDrawLamar					
				IF iRank >= lbdVars.iLamarPos
					iNewRow = (iNewRow+1)
				ENDIF
			ELSE
				// For non team modes just draw the row as players are already in position
				iRank = (iRow+1)
			ENDIF
		ENDIF
		
		PRINTLN("[3299768] LBD DEBUG - POPULATE_COLUMN_LBD - participantNames = ", participantNames[iParticipant], " iparticipant = ", iparticipant, " iNewRow = ", iNewRow)
		
		DRAW_TEAM_ROWS(lbdVars, Placement, eSubMode, iTeamScore, iNumColumns, iLocalTeamForColour, iEnemyTeamForColour, iTeamRow, iTeamNamesRow, bDrawTeamRow, bTeamMode)//, iTeam)//, iRank)
		
		IF iNewRow >= 0
		AND iNewRow <= MAX_ROWS_DRAWN	
			DRAW_HORZ_VARIABLE_BARS(Placement, eSubMode, bSpectator, playerId, iNewRow, bTeamMode, iLocalTeamForColour, iEnemyTeamForColour)
			DRAW_RECT_PLAYER_BARS(Placement, eSubMode, bSpectator, playerId, iNewRow, iLocalTeamForColour, iEnemyTeamForColour, iTeam, iLBDTeam)
			RENDER_LAP_STAR(lbdVars, Placement, iNewRow)
			DRAW_WEE_POSITION_RECTS(Placement, iNewRow, g_s_HeadshotLbd[iPlayer])
			DRAW_POSITIONS(lbdVars, Placement, iParticipant, iRank, iNewRow, bTeamMode) 
			DRAW_PLAYER_NAMES(Placement, eSubMode, participantNames, iNewRow, iParticipant)
			DRAW_CREW_TAGS(lbdVars.aGamer, eSubMode, Placement, participantNames, iParticipant, iNewRow) 
			POPULATE_COLUMNS(lbdVars, Placement, eSubMode, eVote, iNumColumns, fRatio, iColumn1Var, iColumn2Var, iColumn3Var, iColumn4Var, iColumn5Var, iColumn6Var, iParticipant, 
								iNewRow, iRoundScore, iCustomInt, iTeam)
			DRAW_VEHICLE_BLOBS(lbdVars, Placement, eSubMode, iNewRow)
			DRAW_VOICE_CHAT_ICONS_OR_RANK_BADGE(Placement, playerId, iNewRow, iBadgeRank)
			DRAW_POSITION_TOP_RIGHT(Placement, eSubMode, playerId, iRank)
			DRAW_LBD_THUMBS(Placement, lbdVars.bVoted, lbdVars.bVotedDown, iNewRow)
			PRINTLN("[2056544] LBD DEBUG - DRAWING_ROW, iNewRow = ", iNewRow)
		ELSE
			PRINTLN("[2056544] LBD DEBUG - NOT DRAWING ROW ", iRow, " because iNewRow = ", iNewRow)
		ENDIF
	ELSE
		PRINTLN("[2056544] LBD DEBUG - NOT DRAWING ROW ", iRow, " because iParticipant = -1")
	ENDIF
ENDPROC

/// PURPOSE: Returns the correct text to pass in as the playlist type for the string:
///    [FMMC_PL_LEAD2]
///    ~a~ ~a~ - ~a~ - ~1~ of ~1~
FUNC STRING GET_PLAYLIST_TYPE_STRING() // Copy of GET_CORONA_PLAYLIST_TYPE_STRING
	//Playing a challenge
	IF IS_BIT_SET(g_TransitionSessionNonResetVars.sTransVars.iTransitionSessionLocalBitSet, ciTRANSITION_SESSIONS_PBS_THIS_IS_A_CHALLENGE)
		RETURN "FMMC_PL_TYPE1"	//Challenge
//	//Setting a challenge time
//	ELIF IS_BIT_SET(g_TransitionSessionNonResetVars.sTransVars.iTransitionSessionLocalBitSet, ciTRANSITION_SESSIONS_PBS_THIS_IS_SETTING_CHALLENGE) 
//		RETURN "FMMC_PL_TYPE2"	//Challenge
	//doing a head to head
	ELIF IS_BIT_SET(g_sCurrentPlayListDetails.iPlayListBitSet, ciPLAYLIST_DOING_HEAD_TO_HEAD)
	OR IS_BIT_SET(g_TransitionSessionNonResetVars.sTransVars.iTransitionSessionLocalBitSet, ciTRANSITION_SESSIONS_PBS_THIS_IS_HEAD_TOO_HEAD) 
		RETURN "FMMC_PL_TYPE3"	//Head 2 Head 
	ENDIF
	//bog standard playlist!
	RETURN "FMMC_PL_TYPE0" //Playlist
ENDFUNC

CONST_FLOAT fRockstarBadgeOffset 	0.0
CONST_FLOAT fThumbIconOffset		0.065
CONST_FLOAT fThumbPercentOffset		0.008

PROC DRAW_LEADERBOARD_TITLE_PLAYLIST_DETAILS(LBD_VARS& lbdVars, LEADERBOARD_PLACEMENT_TOOLS& Placement, STRING sLeaderBoardTitle)
	// "~a~ ~a~ - ~a~ ~a~ - ~1~ of ~1~"
	STRING TextLabel = "FMMC_PL_LEAD2"
	TEXT_LABEL_63 tlPlaylistName = g_sCurrentPlayListDetails.tl31PlaylistName

	IF lbdVars.iPlaylistProgress = -1
		lbdVars.iPlaylistProgress = g_sCurrentPlayListDetails.iPlaylistProgress
		
		// 2518826, on the final round, 1 was being added to iPlaylistProgress :(
		IF SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD()
			lbdVars.iPlaylistProgress = (lbdVars.iPlaylistProgress - 1)
		ENDIF
		
		PRINTLN("DRAW_LEADERBOARD_TITLE_PLAYLIST_DETAILS, iPlaylistProgress = ", lbdVars.iPlaylistProgress)
		PRINTLN("DRAW_LEADERBOARD_TITLE_PLAYLIST_DETAILS, iNumberOfRoundsPlayed = ", g_sTransitionSessionData.sMissionRoundData.iNumberOfRoundsPlayed)
		PRINTLN("DRAW_LEADERBOARD_TITLE_PLAYLIST_DETAILS, iNumberOfRounds = ", g_sTransitionSessionData.sMissionRoundData.iNumberOfRounds)
	ENDIF
	
	IF IS_THIS_TRANSITION_SESSION_A_CHALLENGE()
	//OR IS_THIS_TRANSITION_SESSION_SETTING_CHALLENGE_TIME()
		tlPlaylistName = g_sCurrentPlayListDetails.tl31ChallengeName
	ENDIF
	IF NOT IS_STRING_EMPTY_HUD(TextLabel)			
		IF IS_SAFE_TO_DRAW_ON_SCREEN()
			SET_TEXT_STYLE(Placement.aStyle.TS_TITLEHUD)	
			SET_TEXT_RIGHT_JUSTIFY(FALSE)
			SET_TEXT_CENTRE(FALSE)	
			BEGIN_TEXT_COMMAND_DISPLAY_TEXT(TextLabel)
				ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL("PM_INF_QKMT5")												// Playlist/challenge etc.
				ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY(tlPlaylistName) 										// Playlist name
				ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY(sLeaderBoardTitle) 									// Job name
				ADD_TEXT_COMPONENT_INTEGER(lbdVars.iPlaylistProgress)												// Playlist progress 
				ADD_TEXT_COMPONENT_INTEGER(g_sCurrentPlayListDetails.iLength)										// Playlist length
			END_TEXT_COMMAND_DISPLAY_TEXT(ADJUST_SCREEN_X_POSITION(Placement.MainTitle.x), ADJUST_SCREEN_Y_POSITION(Placement.MainTitle.y))
		ENDIF
	ENDIF	
ENDPROC 

PROC LEADERBOARD_TITLE_AND_TOP_BAR(LBD_VARS& lbdVars, LEADERBOARD_PLACEMENT_TOOLS& Placement, STRING sLeaderBoardTitle, LBD_SUB_MODE eSubMode, INT iLeaderBoardTitleValue = -1, BOOL bPlaylistController = FALSE)

	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_FADE)
	
	// LEADERBOARD_TITLE_AND_TOP_BAR
	SET_LBD_MAIN_TITLE_FONT_AND_SIZE(Placement.aStyle.TS_TITLEHUD, DROPSTYLE_DROPSHADOWONLY)
	SET_TEXT_STYLE(Placement.aStyle.TS_TITLEHUD)
	
	Placement.MainTitle.x = (LARGE_TITLE_POS_X)// + 0.01111104)
	Placement.MainTitle.y = LARGE_POS_Y
	
	BOOL bPlayingPlaylist 
	IF NOT IS_STRING_NULL_OR_EMPTY(g_sCurrentPlayListDetails.tl31PlaylistName)
	AND IS_PLAYER_ON_A_PLAYLIST(PLAYER_ID())
		bPlayingPlaylist = TRUE
	ENDIF
	
	INT iPlaylistProgress 
	iPlaylistProgress = (g_sCurrentPlayListDetails.iPlaylistProgress - 1)
	IF iPlaylistProgress  < 1
		iPlaylistProgress  = 1
	ELIF iPlaylistProgress > g_sCurrentPlayListDetails.iLength
		iPlaylistProgress  = g_sCurrentPlayListDetails.iLength
	ENDIF
	
	INT iRounds
	iRounds = g_sTransitionSessionData.sMissionRoundData.iNumberOfRoundsPlayed
	
//	IF IS_PLAYER_SPECTATING(PLAYER_ID())
//	AND NOT IS_PLAYER_ON_A_PLAYLIST(PLAYER_ID())
//		iRounds = (iRounds + 1)
//	ENDIF
	
	IF iRounds < 1
		iRounds = 1
	ENDIF
	
	PRINTLN("LEADERBOARD_TITLE_AND_TOP_BAR, iRounds = ", iRounds, " iNumberOfRoundsPlayed = ", g_sTransitionSessionData.sMissionRoundData.iNumberOfRoundsPlayed)
	PRINTLN(" LEADERBOARD_TITLE_AND_TOP_BAR, iPlaylistProgress = ", iPlaylistProgress, " g_sCurrentPlayListDetails.iPlaylistProgress = ", g_sCurrentPlayListDetails.iPlaylistProgress)
	
	//2257230
	BOOL bShortened
	TEXT_LABEL_63 tl63Title
	tl63Title = GET_SHORTENED_TEXT_LABEL(sLeaderBoardTitle, bShortened, GET_TITLE_CHAR_LIMIT(), TRUE)
	PRINTLN("LEADERBOARD_TITLE_AND_TOP_BAR, tl63Title = ", tl63Title)

	// 2294723 we don't want to shorten the Heist titles
	IF IS_PLAYER_PLAYING_OR_PLANNING_HEIST(PLAYER_ID())
		bShortened = FALSE
	ENDIF
	
	// Drawing the actual playlist lbd
	IF bPlaylistController
		PRINTLN(" LEADERBOARD_TITLE_AND_TOP_BAR, 1 ")
		// [FMMC_PL_LEAD] Playlist - ~a~ - ~1~/~1~
		DRAW_TEXT_WITH_KEYBOARD_DISPLAY_AND_TWO_INTS(Placement.MainTitle, Placement.aStyle.TS_TITLEHUD, "FMMC_PL_LEAD", sLeaderBoardTitle, iPlaylistProgress, g_sCurrentPlayListDetails.iLength)
	ELSE
		// Drawing the Job lbd during a playlist
		IF bPlayingPlaylist
			PRINTLN(" LEADERBOARD_TITLE_AND_TOP_BAR, 2 ")
			DRAW_LEADERBOARD_TITLE_PLAYLIST_DETAILS(lbdVars, Placement, sLeaderBoardTitle)
		ELSE
			PRINTLN(" LEADERBOARD_TITLE_AND_TOP_BAR, 3 ")
			IF IS_ROUNDS_MISSION_LBD(eSubMode)
			AND (eSubMode <> SUB_FINAL_ROUNDS)
				IF bShortened
					DRAW_TEXT_WITH_KEYBOARD_DISPLAY_AND_INT(Placement.MainTitle, Placement.aStyle.TS_TITLEHUD, GET_LABEL_IF_SHORTENED(bShortened), tl63Title, iRounds)
				ELSE
					DRAW_TEXT_WITH_KEYBOARD_DISPLAY_AND_INT(Placement.MainTitle, Placement.aStyle.TS_TITLEHUD, "FMMC_RD_LBD", sLeaderBoardTitle, iRounds)
				ENDIF
			ELSE
				// Normal Job lbds
				SWITCH eSubMode
					CASE SUB_ARENA_CARNAGE
					CASE SUB_DM_FFA
					CASE SUB_DM_TEAM
					CASE SUB_KOTH
					CASE SUB_RACE_GTA
					CASE SUB_RACE_NORMAL
					CASE SUB_RACE_STUNT
					CASE SUB_RACE_RALLY
					CASE SUB_RACE_BASEJUMP
					CASE SUB_MISSIONS_NORMAL
					CASE SUB_MISSIONS_CTF
					CASE SUB_MISSIONS_NO_HUD
					CASE SUB_MISSIONS_VERSUS
					CASE SUB_MISSIONS_HEIST
					CASE SUB_MISSIONS_LTS
					CASE SUB_MISSIONS_TIMED
					CASE SUB_PLAYLIST
					CASE SUB_FINAL_ROUNDS
					CASE SUB_RACE_AGG_BEST
					CASE SUB_RACE_AGG_TIME
					CASE SUB_RACE_TARGET
					CASE SUB_ARENA_ARCADE
					CASE SUB_ARENA_CTF
					CASE SUB_ARENA_DDERBY
					CASE SUB_ARENA_DETONATION
					CASE SUB_ARENA_FIRE
					CASE SUB_ARENA_GAMESMASTER
					CASE SUB_ARENA_MONSTER
					CASE SUB_ARENA_PASSBOMB
					CASE SUB_ARENA_TAG_TEAM
					CASE SUB_DM_UPDATE
						IF g_bDrawJustText = TRUE
							DRAW_TEXT(Placement.MainTitle, Placement.aStyle.TS_TITLEHUD, sLeaderBoardTitle)	
						ELSE
							IF bShortened
								DISPLAY_TEXT_WITH_PLAYER_NAME(Placement.MainTitle.x, Placement.MainTitle.y, GET_LABEL_IF_SHORTENED(bShortened), tl63Title, HUD_COLOUR_PURE_WHITE)
								PRINTLN("LEADERBOARD_TITLE_AND_TOP_BAR, bShortened ")
							ELSE
								DRAW_TEXT_WITH_PLAYER_NAME(Placement.MainTitle, Placement.aStyle.TS_TITLEHUD, sLeaderBoardTitle)
								PRINTLN("LEADERBOARD_TITLE_AND_TOP_BAR, NOT bShortened ")
							ENDIF
						ENDIF
					BREAK
					CASE SUB_HORDE
					CASE SUB_KART
						IF g_bDrawJustText = TRUE
							DRAW_TEXT(Placement.MainTitle, Placement.aStyle.TS_TITLEHUD, sLeaderBoardTitle)	
						ELSE
							IF iLeaderBoardTitleValue = -1
								IF bShortened
									DISPLAY_TEXT_WITH_PLAYER_NAME(Placement.MainTitle.x, Placement.MainTitle.y, GET_LABEL_IF_SHORTENED(bShortened), tl63Title, HUD_COLOUR_PURE_WHITE)
								ELSE
									DRAW_TEXT_WITH_PLAYER_NAME(Placement.MainTitle, Placement.aStyle.TS_TITLEHUD, sLeaderBoardTitle)
								ENDIF
							ELSE
								DRAW_TEXT_WITH_NUMBER(Placement.MainTitle, Placement.aStyle.TS_TITLEHUD, sLeaderBoardTitle, iLeaderBoardTitleValue)
							ENDIF
						ENDIF
					BREAK
					CASE SUB_FREEMODE
					CASE SUB_ARCADE_CNC
					CASE SUB_SHOOT_RANGE
						DRAW_TEXT(Placement.MainTitle, Placement.aStyle.TS_TITLEHUD, sLeaderBoardTitle)	
					BREAK
				ENDSWITCH
			ENDIF
		ENDIF
	ENDIF

	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_FADE)
ENDPROC

PROC HIDE_HUD_DURING_LBD()
	HIDE_HUD_AND_RADAR_THIS_FRAME()
	HIDE_SCRIPTED_HUD_COMPONENT_THIS_FRAME(HUD_MP_RANK_BAR)
ENDPROC

PROC BLOCK_JIP_INVITING_ON_LEADERBOARDS()
	IF NOT ARE_TUTORIAL_INVITES_ARE_BLOCKED()
		IF NOT IS_PLAYER_ON_A_PLAYLIST(PLAYER_ID())
			PRINTLN("[TS] [CS LBD] - DO_END_JOB_VOTING_SCREEN - NETWORK_BLOCK_INVITES(TRUE)")
			NETWORK_BLOCK_INVITES(TRUE)
			PRINTLN("[TS] [CS LBD] - DO_END_JOB_VOTING_SCREEN - NETWORK_SET_PRESENCE_SESSION_INVITES_BLOCKED(TRUE)")
			NETWORK_SET_PRESENCE_SESSION_INVITES_BLOCKED(TRUE)			
			IF NETWORK_IS_HOST()
				PRINTLN("[TS] [CS LBD] - DO_END_JOB_VOTING_SCREEN - NETWORK_IS_HOST - NETWORK_SESSION_BLOCK_JOIN_REQUESTS(TRUE)")
				NETWORK_SESSION_BLOCK_JOIN_REQUESTS(TRUE)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

#IF IS_DEBUG_BUILD
PROC PRINT_SUB_MODE(LBD_SUB_MODE eSubMode)
	SWITCH eSubMode
		CASE SUB_ARENA_CARNAGE			PRINTLN("SUB_ARENA_CARNAGE") BREAK
		CASE SUB_ARENA_ARCADE			PRINTLN("SUB_ARENA_ARCADE") BREAK
		CASE SUB_ARENA_CTF				PRINTLN("SUB_ARENA_CTF") BREAK
		CASE SUB_ARENA_DDERBY			PRINTLN("SUB_ARENA_DDERBY") BREAK
		CASE SUB_ARENA_DETONATION		PRINTLN("SUB_ARENA_DETONATION") BREAK	
		CASE SUB_ARENA_FIRE				PRINTLN("SUB_ARENA_FIRE") BREAK	
		CASE SUB_ARENA_GAMESMASTER		PRINTLN("SUB_ARENA_GAMESMASTER") BREAK	
		CASE SUB_ARENA_MONSTER			PRINTLN("SUB_ARENA_MONSTER") BREAK	
		CASE SUB_ARENA_PASSBOMB			PRINTLN("SUB_ARENA_PASSBOMB") BREAK	
		CASE SUB_ARENA_TAG_TEAM			PRINTLN("SUB_ARENA_TAG_TEAM") BREAK	
		
		CASE SUB_DM_FFA					PRINTLN("SUB_DM_FFA") BREAK
		CASE SUB_DM_TEAM				PRINTLN("SUB_DM_TEAM") BREAK
		CASE SUB_KOTH					PRINTLN("SUB_KOTH") BREAK
		CASE SUB_DM_VEH					PRINTLN("SUB_DM_VEH") BREAK
		CASE SUB_HORDE					PRINTLN("SUB_HORDE") BREAK	
		CASE SUB_KART					PRINTLN("SUB_KART") BREAK	
		CASE SUB_RACE_STUNT				PRINTLN("SUB_RACE_STUNT") BREAK	
		CASE SUB_CHALLENGE				PRINTLN("SUB_CHALLENGE") BREAK	
		CASE SUB_DM_UPDATE				PRINTLN("SUB_DM_UPDATE") BREAK	
		
		CASE SUB_RACE_AGG_BEST			PRINTLN("SUB_RACE_AGG_BEST") BREAK
		CASE SUB_RACE_AGG_TIME			PRINTLN("SUB_RACE_AGG_TIME") BREAK
		CASE SUB_RACE_TARGET			PRINTLN("SUB_RACE_TARGET") BREAK
		CASE SUB_RACE_NORMAL			PRINTLN("SUB_RACE_NORMAL") BREAK
		CASE SUB_RACE_GTA				PRINTLN("SUB_RACE_GTA") BREAK
		CASE SUB_RACE_RALLY				PRINTLN("SUB_RACE_RALLY") BREAK
		CASE SUB_RACE_BASEJUMP			PRINTLN("SUB_RACE_BASEJUMP") BREAK
		CASE SUB_RACE_FOOT				PRINTLN("SUB_RACE_FOOT") BREAK
		CASE SUB_MISSIONS_NORMAL		PRINTLN("SUB_MISSIONS_NORMAL") BREAK
		CASE SUB_MISSIONS_TIMED			PRINTLN("SUB_MISSIONS_TIMED") BREAK
		CASE SUB_MISSIONS_NO_HUD		PRINTLN("SUB_MISSIONS_NO_HUD") BREAK
		CASE SUB_MISSIONS_VERSUS		PRINTLN("SUB_MISSIONS_VERSUS") BREAK
		CASE SUB_MISSIONS_HEIST 		PRINTLN("SUB_MISSIONS_HEIST") BREAK
		CASE SUB_MISSIONS_LTS			PRINTLN("SUB_MISSIONS_LTS") BREAK
		CASE SUB_MISSIONS_CTF			PRINTLN("SUB_MISSIONS_CTF") BREAK
		CASE SUB_PLAYLIST				PRINTLN("SUB_PLAYLIST") BREAK
		CASE SUB_FREEMODE				PRINTLN("SUB_FREEMODE") BREAK
		CASE SUB_ARCADE_CNC				PRINTLN("SUB_ARCADE_CNC") BREAK
		CASE SUB_SHOOT_RANGE			PRINTLN("SUB_SHOOT_RANGE") BREAK
		CASE SUB_SC_PRE_RACE_SUB_TITLE  PRINTLN("SUB_SC_PRE_RACE_SUB_TITLE") BREAK
		CASE SUB_SC_PRE_RACE_CONTENT	PRINTLN("SUB_SC_PRE_RACE_CONTENT") BREAK
		CASE SUB_FINAL_ROUNDS			PRINTLN("SUB_FINAL_ROUNDS") BREAK
		CASE SUB_NOT_USED				PRINTLN("SUB_NOT_USED") BREAK
	ENDSWITCH
ENDPROC
#ENDIF

//â•”â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•—

FUNC BOOL HAS_END_JOB_BUY_AMMO_BEEN_PRESSED()
	IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LS)		
	
		PRINTLN(" ------------------------------------------------------------------------------")
		PRINTLN(" --                                                                          --")
		PRINTLN(" - FMMC_EOM - HAS_END_JOB_BUY_AMMO_BEEN_PRESSED                     		  --")
		PRINTLN(" --                                                                          --")
		PRINTLN(" ------------------------------------------------------------------------------")
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_END_JOB_BUY_ARMOUR_BEEN_PRESSED()
	IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RS)		
	
		PRINTLN(" ------------------------------------------------------------------------------")
		PRINTLN(" --                                                                          --")
		PRINTLN(" - FMMC_EOM - HAS_END_JOB_BUY_ARMOUR_BEEN_PRESSED                     		  --")
		PRINTLN(" --                                                                          --")
		PRINTLN(" ------------------------------------------------------------------------------")
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC TURN_ON_AMMO_WARNING(AMMO_ARMOUR_STRUCT& AmmoArmourStruct, BOOL bTurnOn)
	IF bTurnOn
		PRINTLN(" [ammo/armour] TURN_ON_AMMO_WARNING, TRUE ")
		SET_BIT(AmmoArmourStruct.iBitSet, LBD_BOOL_AMMO_WARNING_ON)
	ELSE
		PRINTLN(" [ammo/armour] TURN_ON_AMMO_WARNING, FALSE ")
		CLEAR_BIT(AmmoArmourStruct.iBitSet, LBD_BOOL_AMMO_WARNING_ON)
	ENDIF
ENDPROC

FUNC BOOL IS_AMMO_WARNING_SCREEN_ACTIVE(AMMO_ARMOUR_STRUCT& AmmoArmourStruct)

	RETURN IS_BIT_SET(AmmoArmourStruct.iBitSet, LBD_BOOL_AMMO_WARNING_ON)
ENDFUNC

FUNC BOOL WARN_ABOUT_AMMO_COST(AMMO_ARMOUR_STRUCT& AmmoArmourStruct)

	IF AmmoArmourStruct.iTotalCostToFillAmmo > 0 //>= CORONA_PURCHASE_AMMO_WARNING_LIMIT
	
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL AMMO_AVAILABLE(AMMO_ARMOUR_STRUCT& AmmoArmourStruct)

	IF IS_BIT_SET(AmmoArmourStruct.iBitSet, LBD_BOOL_BLOCK_AMMO)
	
		RETURN FALSE
	ENDIF
	
	IF g_b_BlockAmmo 			
	
		RETURN FALSE
	ENDIF
	
	IF IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL()
	
		RETURN FALSE
	ENDIF

	RETURN TRUE
ENDFUNC

FUNC BOOL ARMOUR_AVAILABLE(AMMO_ARMOUR_STRUCT& AmmoArmourStruct)

	IF IS_BIT_SET(AmmoArmourStruct.iBitSet, LBD_BOOL_BLOCK_ARMOUR)
	
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciREMOVE_ARMOUR_PURCHASE_OPTION)
	
		RETURN FALSE
	ENDIF
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_Ebc(g_FMMC_STRUCT.iRootContentIDHash)
	OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_HuntDark(g_FMMC_STRUCT.iRootContentIDHash)
	OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_SUMO(g_FMMC_STRUCT.iRootContentIDHash)

		RETURN FALSE
	ENDIF
	
	IF IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL()
	
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

//â•”â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•—
//â•‘		ARMOUR																	â•‘
//â•šâ•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•	

PROC INITIAL_ARMOUR_CHECK(AMMO_ARMOUR_STRUCT& AmmoArmourStruct)

	
	INT iArmorLevel 
	
	IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
		iArmorLevel = GET_MP_MAX_UNLOCKED_ARMOR_LEVEL()
	
	ELSE
	
		iArmorLevel = 2 //PLAYERKIT_STANDARDARMOUR
	ENDIF
	IF iArmorLevel >= 0

		GUNCLUB_WEAPON_DATA_STRUCT sWeaponData
		GET_GUNCLUB_WEAPON_DATA_FROM_WEAPON_TYPE(WEAPONTYPE_UNARMED, sWeaponData, iArmorLevel)	// fetches armor data
		
		INT iArmorToGive = FLOOR((TO_FLOAT(GET_PLAYER_MAX_ARMOUR(PLAYER_ID()))/100.0)*sWeaponData.iDefaultClipSize)

		INT iArmorCost, iDummy1
		BOOL bDummy
		IF GET_GUNSHOP_WEAPON_COST(WEAPONTYPE_UNARMED, iArmorCost, iDummy1, bDummy, iArmorLevel)
		
			IF GET_PED_ARMOUR(PLAYER_PED_ID()) >= iArmorToGive
			
				PRINTLN(" [ammo/armour] INITIAL_ARMOUR_CHECK - Unable to purchase armor, ARMOR FULL")
				SET_BIT(AmmoArmourStruct.iBitSet, LBD_BOOL_BLOCK_ARMOUR)
			
			ELIF NOT NETWORK_CAN_SPEND_MONEY(iArmorCost, FALSE, TRUE, FALSE)
			
				PRINTLN(" [ammo/armour]  INITIAL_ARMOUR_CHECK - Unable to purchase armor, CANT AFFORD IT, NETWORK_CAN_SPEND_MONEY: ", NETWORK_CAN_SPEND_MONEY(iArmorCost, FALSE, TRUE, FALSE))
				
				SET_BIT(AmmoArmourStruct.iBitSet, LBD_BOOL_BLOCK_ARMOUR)
			ENDIF
		ELSE
			PRINTLN(" [ammo/armour]  INITIAL_ARMOUR_CHECK - Unable to purchase armor, could not find cost")
		ENDIF
	ELSE
		PRINTLN(" [ammo/armour]  INITIAL_ARMOUR_CHECK - Unable to purchase armor, armor not currently unlocked")
	ENDIF
ENDPROC

PROC PROCESS_PLAYER_PURCHASING_ARMOR(AMMO_ARMOUR_STRUCT& AmmoArmourStruct, BOOL bApplyArmour = TRUE, BOOL bChargeCash = TRUE)
	
	IF USE_SERVER_TRANSACTIONS()
	AND GET_BASKET_TRANSACTION_SCRIPT_INDEX() != -1
		IF bChargeCash
			PRINTLN(" [ammo/armour] PROCESS_PLAYER_PURCHASING_ARMOR - Trying to by armour but already pending a transaction")
			EXIT
		ENDIF
	ENDIF


	INT iArmorLevel = GET_MP_MAX_UNLOCKED_ARMOR_LEVEL()

	GUNCLUB_WEAPON_DATA_STRUCT sWeaponData
	GET_GUNCLUB_WEAPON_DATA_FROM_WEAPON_TYPE(WEAPONTYPE_UNARMED, sWeaponData, iArmorLevel)	// fetches armor data
		
	INT iArmorToGive = FLOOR((TO_FLOAT(GET_PLAYER_MAX_ARMOUR(PLAYER_ID()))/100.0)*sWeaponData.iDefaultClipSize)

	INT iArmorCost = sWeaponData.iWeaponCost

	PRINTLN(" [ammo/armour]  PROCESS_PLAYER_PURCHASING_ARMOR - Player Cash $", GET_PLAYER_CASH(PLAYER_ID()), " and Armor Cost: $", iArmorCost)
	
	TEXT_LABEL_15 tlArmorName = GET_MP_ARMOR_NAME(iArmorLevel)
	
	PRINTLN(" [ammo/armour]  PROCESS_PLAYER_PURCHASING_ARMOR - tlArmorName = ", tlArmorName)
	PRINTLN(" [ammo/armour]  PROCESS_PLAYER_PURCHASING_ARMOR - iArmorLevel = ", iArmorLevel)
	PRINTLN(" [ammo/armour]  PROCESS_PLAYER_PURCHASING_ARMOR - iArmorToGive = ", iArmorToGive)
	
	IF bChargeCash
		IF USE_SERVER_TRANSACTIONS()
		
			TEXT_LABEL_63 tlCatalogueKey
			GENERATE_KEY_FOR_SHOP_CATALOGUE( tlCatalogueKey, tlArmorName, GET_ENTITY_MODEL( PLAYER_PED_ID() ), SHOP_TYPE_GUN, ENUM_TO_INT( ITEM_TYPE_ARMOUR ), 0 )
			
			PRINTLN(" [ammo/armour]  PROCESS_PLAYER_PURCHASING_ARMOR - Purchasing armour, key='", tlCatalogueKey, "'")
			
			IF NETWORK_REQUEST_BASKET_TRANSACTION(NET_SHOP_TTYPE_BASKET, CATEGORY_WEAPON_AMMO, GET_HASH_KEY( tlCatalogueKey ), NET_SHOP_ACTION_SPEND, 1, iArmorCost, 1, CATALOG_ITEM_FLAG_BANK_THEN_WALLET, DEFAULT, CTPF_AUTO_RETRY )
				g_cashTransactionData[GET_BASKET_TRANSACTION_SCRIPT_INDEX()].cashInfo.iItemHash = GET_HASH_KEY( tlCatalogueKey )
				g_cashTransactionData[GET_BASKET_TRANSACTION_SCRIPT_INDEX()].cashInfo.purchaseType = PURCHASE_ARMOR
				
				NETWORK_START_BASKET_TRANSACTION_CHECKOUT()
				
				SET_AMMO_ARMOR_CASH_TRANSACTION_ACTIVE(AMMO_ARMOR_CASH_TRANSACTION_ARMOR)
			ENDIF
		ELSE
			
			BOOL fromBank = (NETWORK_GET_VC_BANK_BALANCE() > 0) 
			BOOL fromBankAndWallet = (NETWORK_GET_VC_BANK_BALANCE() < iArmorCost)

			PRINTLN(" [ammo/armour]  PROCESS_PLAYER_PURCHASING_ARMOR - tlArmorName = ", tlArmorName)
			PRINTLN(" [ammo/armour]  PROCESS_PLAYER_PURCHASING_ARMOR - iArmorLevel = ", iArmorLevel)
			PRINTLN(" [ammo/armour]  PROCESS_PLAYER_PURCHASING_ARMOR - iArmorToGive = ", iArmorToGive)
			
			NETWORK_BUY_ITEM(iArmorCost, GET_HASH_KEY(tlArmorName), PURCHASE_ARMOR, DEFAULT, fromBank, tlArmorName, GET_HASH_KEY("SHOP_NJVS"), ENUM_TO_INT(WEAPONTYPE_UNARMED), DEFAULT, fromBankAndWallet) 
		
		ENDIF
	ENDIF
	
	IF bApplyArmour
		SET_PED_ARMOUR(PLAYER_PED_ID(), iArmorToGive)
			
		PRINTLN(" [ammo/armour]  PROCESS_PLAYER_PURCHASING_ARMOR - Purchased - Remaining Cash: $", GET_PLAYER_CASH(PLAYER_ID()))
		
		SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(2, GET_MP_ARMOR_HASH(iArmorLevel), hash("ARMOR"), 1,  GET_LOCATION_HASH_FOR_TELEMETRY() ,hash("PURCHASE"),  FALSE, hash("JOB_QUICK_RESTART"))
		// Put the armor clothing component on
		INT iStat = GET_MP_INT_CHARACTER_STAT(MP_STAT_AUTO_EQUIP_ARMOUR)
		IF iStat = 1
			PED_COMP_NAME_ENUM eBodyArmour
			eBodyArmour = GET_BODY_ARMOUR_FOR_JBIB(PLAYER_PED_ID(), GET_ENTITY_MODEL(PLAYER_PED_ID()), COMP_TYPE_JBIB, GET_PED_COMPONENT_ITEM_CURRENT_FROM_LOOKUP(PLAYER_PED_ID(), COMP_TYPE_JBIB))//, sWeaponData.iDefaultClipSize)
			IF eBodyArmour != DUMMY_PED_COMP
				IF eBodyArmour != SPECIAL2_FMM_0_0
				    eBodyArmour += INT_TO_ENUM(PED_COMP_NAME_ENUM, GET_BODY_ARMOUR_FROM_CLIP_SIZE(sWeaponData.iDefaultClipSize))
				ENDIF
				PRINTLN(" [ammo/armour]  PROCESS_PLAYER_PURCHASING_ARMOR Player bought body armour, equiping = ", eBodyArmour)
				
				// Local ped
				SET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_SPECIAL2, eBodyArmour, FALSE)
				// Local ped clone
				SET_PED_COMP_ITEM_CURRENT_MP(g_CoronaClonePeds.piPlayersPed[NATIVE_TO_INT(PLAYER_ID())].pedID, COMP_TYPE_SPECIAL2, eBodyArmour, FALSE)
			ELSE
				PRINTLN(" [ammo/armour]  PROCESS_PLAYER_PURCHASING_ARMOR Player bought body armour, but has nothing to equip")
			ENDIF
		ELSE
			PRINTLN(" [ammo/armour]  PROCESS_PLAYER_PURCHASING_ARMOR Player bought body armour, don't auto-equip ")
		ENDIF
	ENDIF
	
	IF bChargeCash
		PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
		
		SET_BIT(AmmoArmourStruct.iBitSet, LBD_BOOL_BLOCK_ARMOUR)
	//	REFRESH_BUTTONS(AmmoArmourStruct)
		SET_BIT(AmmoArmourStruct.iBitSet, LBD_BOOL_REFRESH_NEEDED)
	ENDIF
ENDPROC

PROC HANDLE_BUYING_ARMOUR(AMMO_ARMOUR_STRUCT& AmmoArmourStruct)
	
	BOOL bApplyAmmo = TRUE
	BOOL bChargeCash = TRUE
	
	// If PC then we should not actually give the ammo here, purely charge the cash and wait for the server
	IF IS_PC_VERSION()
		IF USE_SERVER_TRANSACTIONS()
			PRINTLN("[ammo/armour] PROCESS_PLAYER_PURCHASING_FULL_AMMO_FOR_CORONA - PC Version, bApplyAmmo = FALSE")
			bApplyAmmo = FALSE
		ENDIF
	ENDIF

	IF ARMOUR_AVAILABLE(AmmoArmourStruct)

		INITIAL_ARMOUR_CHECK(AmmoArmourStruct)

		IF HAS_END_JOB_BUY_ARMOUR_BEEN_PRESSED()
			IF GET_MP_MAX_UNLOCKED_ARMOR_LEVEL() > -1
		
				PROCESS_PLAYER_PURCHASING_ARMOR(AmmoArmourStruct, bApplyAmmo, bChargeCash)
			ENDIF
		ENDIF
	ENDIF
ENDPROC


//â•”â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•—
//â•‘		AMMO																	â•‘
//â•šâ•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•	
PROC COUNT_AMMO_COST(AMMO_ARMOUR_STRUCT& AmmoArmourStruct)
	
	INT iBSCheckedAmmoTypes
	
	INT iWeaponSlot
	INT iWeaponAmmoCost
	WEAPON_TYPE aWeapon
	GUNCLUB_WEAPON_DATA_STRUCT sWeaponStruct
	
	AmmoArmourStruct.iTotalCostToFillAmmo = 0
	
	// Loop over our normal weapons
	FOR iWeaponSlot = 0 TO (ENUM_TO_INT(NUM_WEAPONSLOTS)-1) STEP 1
		
		// Get the weapon type from the slot
		IF GET_PLAYER_PED_WEAPON_SLOT_FROM_INT(iWeaponSlot) = WEAPONSLOT_INVALID
			aWeapon = WEAPONTYPE_INVALID
		ELSE
			aWeapon = GET_PED_WEAPONTYPE_IN_SLOT(PLAYER_PED_ID(), GET_PLAYER_PED_WEAPON_SLOT_FROM_INT(iWeaponSlot))
		ENDIF
		
		// Is the weapon valid for the corona?
		IF IS_CORONA_WEAPON_VALID(aWeapon, g_FMMC_STRUCT.iMissionType, TRUE)
			
			//...yes, get the total cost for ammo type
			IF GET_CORONA_WEAPON_TOTAL_AMMO_COST(aWeapon, sWeaponStruct, iBSCheckedAmmoTypes, iWeaponAmmoCost)
				
				//...yes, add the total cost to overall value
				AmmoArmourStruct.iTotalCostToFillAmmo += iWeaponAmmoCost
						
				PRINTLN(" [ammo/armour] COUNT_AMMO_COST			- Current Total Cost: ", AmmoArmourStruct.iTotalCostToFillAmmo)
			ENDIF
		ENDIF
	ENDFOR
	
	// Process our DLC weapons in the same way	
	scrShopWeaponData sWeaponData
	FOR iWeaponSlot = 0 TO (GET_NUM_DLC_WEAPONS()-1) STEP 1
		
		IF GET_DLC_WEAPON_DATA(iWeaponSlot, sWeaponData)
				
			IF IS_CORONA_WEAPON_VALID(INT_TO_ENUM(WEAPON_TYPE, sWeaponData.m_nameHash), g_FMMC_STRUCT.iMissionType, TRUE)
				
				IF GET_CORONA_WEAPON_TOTAL_AMMO_COST(INT_TO_ENUM(WEAPON_TYPE, sWeaponData.m_nameHash), sWeaponStruct, iBSCheckedAmmoTypes, iWeaponAmmoCost)
				
					//...yes, add the total cost to overall value
					AmmoArmourStruct.iTotalCostToFillAmmo += iWeaponAmmoCost
							
					PRINTLN(" [ammo/armour] COUNT_AMMO_COST			- Current Total Cost: ", AmmoArmourStruct.iTotalCostToFillAmmo)
				ENDIF
					
			ENDIF
		ENDIF
	ENDFOR
	
	IF AmmoArmourStruct.iTotalCostToFillAmmo <= 0
		SET_BIT(AmmoArmourStruct.iBitSet, LBD_BOOL_BLOCK_AMMO)
		PRINTLN(" [ammo/armour] COUNT_AMMO_COST - LBD_BOOL_BLOCK_AMMO ")
	ENDIF
	
	PRINTLN(" [ammo/armour] COUNT_AMMO_COST - Total Cost: ", AmmoArmourStruct.iTotalCostToFillAmmo)
ENDPROC

FUNC BOOL PROCESS_PURCHASING_AMMO(AMMO_ARMOUR_STRUCT& AmmoArmourStruct, WEAPON_TYPE aStartingWeapon, BOOL bApplyAmmo = TRUE, BOOL bChargeCash = TRUE, BOOL bMK2WeaponLookup = FALSE)

	GUNCLUB_WEAPON_DATA_STRUCT	sWeaponStruct
			
	IF GET_GUNCLUB_WEAPON_DATA_FROM_WEAPON_TYPE(aStartingWeapon, sWeaponStruct, DEFAULT, bMK2WeaponLookup)
	
		// Make sure weapon is valid and has an ammo type
		IF sWeaponStruct.eType != ITEM_TYPE_INVALID
		AND sWeaponStruct.eWeapon != WEAPONTYPE_INVALID
		AND sWeaponStruct.eAmmoType != INT_TO_ENUM(AMMO_TYPE, 0)

			INT iAmmoCount = sWeaponStruct.iDefaultClipSize

			IF IS_CORONA_WEAPON_THROWN(aStartingWeapon)
				PRINTLN(" [ammo/armour] PROCESS_PURCHASING_AMMO - Weapon is a thrown type. Give 1 ammo")
				iAmmoCount = 1
				
			ELIF sWeaponStruct.eType = ITEM_TYPE_GUN
				PRINTLN(" [ammo/armour] PROCESS_PURCHASING_AMMO - Weapon is ITEM_TYPE_GUN. Give 2 * default clip size")
				iAmmoCount *= 2 // 2 clips for a gun
			ENDIF
			
			INT iAmmoInitialCount = iAmmoCount
			
			IF CAN_PLAYER_PURCHASE_CORONA_AMMO_FOR_WEAPON(aStartingWeapon, iAmmoCount)

				INT iWeaponCost
				INT iAmmoCost
				INT iMaxAmmo
				
				BOOL bChargedPerUnit
				GET_GUNSHOP_WEAPON_COST(aStartingWeapon, iWeaponCost, iAmmoCost, bChargedPerUnit)
				
				IF USE_SERVER_TRANSACTIONS()
					IF bChargedPerUnit
						iAmmoCost *= iAmmoCount
					ENDIF
				ENDIF

				iAmmoCost 	= ROUND(TO_FLOAT(iAmmoCost) * GET_AMMO_MULTIPLIER_FOR_FULL_AMMO(sWeaponStruct.eWeapon, iAmmoCount))
				iAmmoCount	= ROUND(TO_FLOAT(iAmmoCount) * GET_AMMO_MULTIPLIER_FOR_FULL_AMMO(sWeaponStruct.eWeapon, iAmmoCount))
				
				IF GET_MAX_AMMO(PLAYER_PED_ID(), sWeaponStruct.eWeapon, iMaxAmmo)
					IF iMaxAmmo = GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), sWeaponStruct.eWeapon)
						PRINTLN(" [ammo/armour] PROCESS_PURCHASING_AMMO - Weapon is FULL. Set iAmmoCost = 0")
						iAmmoCost = 0							
					ENDIF
				ENDIF
			
			
				IF iAmmoCost > 0

					BOOL fromBank = (NETWORK_GET_VC_BANK_BALANCE() > 0) 
					BOOL fromBankAndWallet = (NETWORK_GET_VC_BANK_BALANCE() < iAmmoCost)
				
					IF fromBankAndWallet
						PRINTLN("[ammo/armour] PROCESS_PURCHASING_AMMO - fromBankAndWallet is TRUE. NETWORK_CAN_SPEND_MONEY can only have one TRUE param. Set fromBank = FALSE")
						fromBank = FALSE
					ENDIF

					IF GET_CORONA_CASH_AVAILABLE() >= iAmmoCost
					AND NETWORK_CAN_SPEND_MONEY(iAmmoCost, fromBank, fromBankAndWallet, FALSE)

						IF bChargeCash
							IF USE_SERVER_TRANSACTIONS()
							
								TEXT_LABEL_63 tlCatalogueKey, tlCatalogueKeyExtra
								TEXT_LABEL_15 sName
								
								AMMO_TYPE eAmmoType
								GET_WEAPON_AMMO_TYPE_FROM_WEAPON_TYPE( sWeaponStruct.eWeapon, eAmmoType )
								
								IF sWeaponStruct.eWeapon = WEAPONTYPE_GRENADE
								OR sWeaponStruct.eWeapon = WEAPONTYPE_STICKYBOMB
								OR sWeaponStruct.eWeapon = WEAPONTYPE_SMOKEGRENADE
								OR sWeaponStruct.eWeapon = WEAPONTYPE_DLC_PROXMINE
									sName = GET_WEAPON_NAME( sWeaponStruct.eWeapon )
									GENERATE_KEY_FOR_SHOP_CATALOGUE( tlCatalogueKeyExtra, sName, GET_ENTITY_MODEL( PLAYER_PED_ID() ), SHOP_TYPE_GUN, ENUM_TO_INT(ITEM_TYPE_NADE), 0, GET_WEAPON_ALT_NUM_FOR_CATALOG_KEY(sWeaponStruct.eWeapon))
									GENERATE_KEY_FOR_SHOP_CATALOGUE( tlCatalogueKey, sName, GET_ENTITY_MODEL( PLAYER_PED_ID() ), SHOP_TYPE_GUN, ENUM_TO_INT(ITEM_TYPE_NADE), 1, GET_WEAPON_ALT_NUM_FOR_CATALOG_KEY(sWeaponStruct.eWeapon))
								ELSE
									GET_GUNCLUB_WEAPON_AMMO_TEXT( sWeaponStruct.eAmmoType, iAmmoCount, sName )
									GENERATE_KEY_FOR_SHOP_CATALOGUE( tlCatalogueKey, sName, GET_ENTITY_MODEL( PLAYER_PED_ID() ), SHOP_TYPE_GUN, 0, GET_WEAPON_INDEX_FROM_WEAPON_TYPE( aStartingWeapon ), GET_WEAPON_ALT_NUM_FOR_CATALOG_KEY(aStartingWeapon))
								ENDIF
							
								INT iBasketQuantity 
								IF bChargedPerUnit
									iBasketQuantity = iAmmoCount
								ELSE
									iBasketQuantity = FLOOR( GET_AMMO_MULTIPLIER_FOR_FULL_AMMO( sWeaponStruct.eWeapon, iAmmoInitialCount ) )
								ENDIF

								PRINTLN("[ammo/armour] PROCESS_PURCHASING_AMMO - Purchasing ammo, key='", tlCatalogueKey, "'")
								PRINTLN("[ammo/armour] PROCESS_PURCHASING_AMMO - iBasketQuantity = ", iBasketQuantity, ", iAmmoCost = ", iAmmoCost, ", iAmmoCount = ", iAmmoCount, ", iAmmoInitialCount = ", iAmmoInitialCount)
							
								
								
								IF NETWORK_REQUEST_BASKET_TRANSACTION( NET_SHOP_TTYPE_BASKET, CATEGORY_WEAPON_AMMO, GET_HASH_KEY( tlCatalogueKey ), NET_SHOP_ACTION_SPEND, iBasketQuantity, (iAmmoCost/iBasketQuantity), 1, CATALOG_ITEM_FLAG_BANK_THEN_WALLET, DEFAULT, CTPF_AUTO_RETRY)
									g_cashTransactionData[GET_BASKET_TRANSACTION_SCRIPT_INDEX()].cashInfo.iItemHash = ENUM_TO_INT(sWeaponStruct.eWeapon)
									g_cashTransactionData[GET_BASKET_TRANSACTION_SCRIPT_INDEX()].cashInfo.purchaseType = PURCHASE_WEAPONAMMO
									IF GET_HASH_KEY( tlCatalogueKeyExtra ) != 0
										NETWORK_REQUEST_BASKET_TRANSACTION(NET_SHOP_TTYPE_BASKET, CATEGORY_WEAPON, GET_HASH_KEY( tlCatalogueKeyExtra ), NET_SHOP_ACTION_SPEND, 1, 0, 1, CATALOG_ITEM_FLAG_BANK_THEN_WALLET, DEFAULT, CTPF_AUTO_RETRY )
										g_cashTransactionData[GET_BASKET_TRANSACTION_SCRIPT_INDEX()].cashInfo.iItemHash = ENUM_TO_INT(sWeaponStruct.eWeapon)
									ENDIF
								ENDIF
														
							ELSE
							
								PRINTLN(" [ammo/armour] PROCESS_PURCHASING_AMMO - Telemetry: 	Player Cash: 		$", GET_PLAYER_CASH(PLAYER_ID()))
								PRINTLN(" [ammo/armour] PROCESS_PURCHASING_AMMO - 				- amount: 			$", iAmmoCost)
								PRINTLN(" [ammo/armour] PROCESS_PURCHASING_AMMO - 				- itemHash:			", ENUM_TO_INT(aStartingWeapon))
								PRINTLN(" [ammo/armour] PROCESS_PURCHASING_AMMO - 				- extra1:			", iAmmoCount)
								
								PRINTLN(" [ammo/armour] PROCESS_PURCHASING_AMMO - 				- itemIdentifier:	FM_COR_AMMO_FILL")
								NETWORK_BUY_ITEM(	iAmmoCost, ENUM_TO_INT(aStartingWeapon), PURCHASE_WEAPONAMMO, iAmmoCount, fromBank, "FM_COR_AMMO_FILL", GET_HASH_KEY("SHOP_NJVS"), DEFAULT, DEFAULT, fromBankAndWallet)
							
							ENDIF
						ENDIF
						
						IF bApplyAmmo
							ADD_AMMO_TO_PED(PLAYER_PED_ID(), aStartingWeapon, iAmmoCount)
							CHECK_AND_SAVE_WEAPON(aStartingWeapon)
						ENDIF
						
						IF bChargeCash
							AmmoArmourStruct.iTotalCostToFillAmmo -= iAmmoCost
						ENDIF
						
	//						IF AmmoArmourStruct.iTotalCostToFillAmmo <= CORONA_PURCHASE_ALL_AMMO_REFRESH_THRESHOLD
	//							COUNT_AMMO_COST(AmmoArmourStruct)
	//							PRINTLN(" [ammo/armour] PROCESS_PURCHASING_AMMO - Re-calculating full ammo amount so we have accurate cost")
	//						ENDIF
						
						IF AmmoArmourStruct.iTotalCostToFillAmmo <= 0
							AmmoArmourStruct.iTotalCostToFillAmmo = 0
							SET_BIT(AmmoArmourStruct.iBitSet, LBD_BOOL_BLOCK_AMMO)
							PRINTLN(" [ammo/armour] PROCESS_PURCHASING_AMMO - Player has full ammo, remove option")
						ENDIF
						
						PRINTLN(" [ammo/armour] PROCESS_PURCHASING_AMMO - Purchased: Ammo: ", iAmmoCount, ", Remaining Cash: $", GET_PLAYER_CASH(PLAYER_ID()))
						PRINTLN(" [ammo/armour] PROCESS_PURCHASING_AMMO - Purchase all ammo is now: ", AmmoArmourStruct.iTotalCostToFillAmmo)
									
						RETURN TRUE
					
					//Can't Afford
					ELSE
						PRINTLN(" [ammo/armour] PROCESS_PURCHASING_AMMO - CHECK B - Unable to purchase ammo, CANT AFFORD IT. NETWORK_CAN_SPEND_MONEY = ", NETWORK_CAN_SPEND_MONEY(iAmmoCost, fromBank, fromBankAndWallet, FALSE))
					ENDIF
				ENDIF
			ELSE
			
				#IF IS_DEBUG_BUILD
		
					// Player has hit the real limit
					IF HAS_PLAYER_HIT_CORONA_AMMO_LIMIT(GET_CORONA_WEAPON_FROM_INT(AmmoArmourStruct.iWeaponSlot), FALSE)
					
						PRINTLN(" [ammo/armour] PROCESS_PURCHASING_AMMO - Unable to purchase ammo, AMMO MAXED")
					
					// Player has hit the locked weapon limit
					ELIF HAS_PLAYER_HIT_CORONA_AMMO_LIMIT(GET_CORONA_WEAPON_FROM_INT(AmmoArmourStruct.iWeaponSlot), TRUE)
					
						PRINTLN(" [ammo/armour] PROCESS_PURCHASING_AMMO - Unable to purchase ammo, AMMO LIMITED - this weapon is not unlocked")
					
					// Player can't afford the ammo
					ELSE
						INT iWeaponCost, iAmmoCost
						BOOL bChargedPerUnit
						GET_GUNSHOP_WEAPON_COST(GET_CORONA_WEAPON_FROM_INT(AmmoArmourStruct.iWeaponSlot), iWeaponCost, iAmmoCost, bChargedPerUnit)
						
						IF USE_SERVER_TRANSACTIONS()
							IF bChargedPerUnit
								iAmmoCost *= iAmmoCount
							ENDIF
						ENDIF
						
						IF iAmmoCost > 0
						AND (GET_CORONA_CASH_AVAILABLE() < iAmmoCost
						OR NOT NETWORK_CAN_SPEND_MONEY(iAmmoCost, FALSE, TRUE, FALSE))
							PRINTLN(" [ammo/armour] PROCESS_PURCHASING_AMMO - Unable to purchase ammo, CANT AFFORD IT: NETWORK_CAN_SPEND_MONEY = ", NETWORK_CAN_SPEND_MONEY(iAmmoCost, FALSE, TRUE, FALSE))
						ELSE
							PRINTLN(" [ammo/armour] PROCESS_PURCHASING_AMMO - Unable to purchase ammo, UNKNOWN?")
						ENDIF
					ENDIF
				
				#ENDIF
			
			ENDIF
		ENDIF
	ENDIF



	
	
//	SET_BIT(AmmoArmourStruct.iBitSet, LBD_BOOL_BLOCK_AMMO)
//	PRINTLN(" [ammo/armour] PROCESS_PURCHASING_AMMO - LBD_BOOL_BLOCK_AMMO ")
	
	RETURN FALSE
ENDFUNC

/// PURPOSE: Returns TRUE if the player can afford this purchase
FUNC BOOL CAN_PLAYER_AFFORD_FULL_AMMO(AMMO_ARMOUR_STRUCT& AmmoArmourStruct)
	// Can the player afford this max ammo option
//	IF GET_CORONA_CASH_AVAILABLE() < coronaMenuData.iTotalCostToFillAmmo
	IF NOT NETWORK_CAN_SPEND_MONEY(AmmoArmourStruct.iTotalCostToFillAmmo, FALSE, TRUE, FALSE)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL PROCESS_PURCHASING_FULL_AMMO(AMMO_ARMOUR_STRUCT& AmmoArmourStruct, BOOL bApplyAmmo = TRUE, BOOL bChargeCash = TRUE)
	
	IF USE_SERVER_TRANSACTIONS()
	AND GET_BASKET_TRANSACTION_SCRIPT_INDEX() != -1
		IF bChargeCash
			PRINTLN(" [ammo/armour] PROCESS_PURCHASING_FULL_AMMO - Trying to by ammo but already pending a transaction")
			RETURN FALSE
		ENDIF
	ENDIF	
	
	INT iBSCheckedAmmoTypes
	
	INT iWeaponSlot
	WEAPON_TYPE aWeapon

	GUNCLUB_WEAPON_DATA_STRUCT	sWeaponStruct
	
	BOOL bMadePurchase

	// Loop over our normal weapons
	FOR iWeaponSlot = 0 TO (ENUM_TO_INT(NUM_WEAPONSLOTS)-1) STEP 1
		
		// Get the weapon type from the slot
		IF GET_PLAYER_PED_WEAPON_SLOT_FROM_INT(iWeaponSlot) = WEAPONSLOT_INVALID
			aWeapon = WEAPONTYPE_INVALID
		ELSE
			aWeapon = GET_PED_WEAPONTYPE_IN_SLOT(PLAYER_PED_ID(), GET_PLAYER_PED_WEAPON_SLOT_FROM_INT(iWeaponSlot))
		ENDIF
		
		// Is the weapon valid for the corona?
		IF IS_CORONA_WEAPON_VALID(aWeapon, g_FMMC_STRUCT.iMissionType, TRUE)
			
			IF GET_GUNCLUB_WEAPON_DATA_FROM_WEAPON_TYPE(aWeapon, sWeaponStruct)
			
				IF aWeapon != WEAPONTYPE_UNARMED
					IF GET_CORONA_AMMO_TYPE_INT(sWeaponStruct.eAmmoType) != -1
					OR aWeapon = WEAPONTYPE_GRENADE
						IF aWeapon = WEAPONTYPE_GRENADE
						OR NOT IS_BIT_SET(iBSCheckedAmmoTypes, GET_CORONA_AMMO_TYPE_INT(sWeaponStruct.eAmmoType))
													
							PRINTLN(" [ammo/armour] PROCESS_PURCHASING_FULL_AMMO - Purchasing ammo for: ", GET_WEAPON_NAME(aWeapon))
							IF PROCESS_PURCHASING_AMMO(AmmoArmourStruct, aWeapon, bApplyAmmo, bChargeCash)
								
								IF aWeapon != WEAPONTYPE_GRENADE
									SET_BIT(iBSCheckedAmmoTypes, GET_CORONA_AMMO_TYPE_INT(sWeaponStruct.eAmmoType))
								ENDIF
								
								PRINTLN("[ammo/armour] PROCESS_PURCHASING_FULL_AMMO - Purchase ammo for: ", GET_WEAPON_NAME(aWeapon), " successful")
								bMadePurchase = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		ENDIF
	ENDFOR
	
	// Process our DLC weapons in the same way	
	scrShopWeaponData sWeaponData
	FOR iWeaponSlot = 0 TO (GET_NUM_DLC_WEAPONS()-1) STEP 1
		
		IF GET_DLC_WEAPON_DATA(iWeaponSlot, sWeaponData)

			aWeapon = INT_TO_ENUM(WEAPON_TYPE, sWeaponData.m_nameHash)

			IF IS_CORONA_WEAPON_VALID(aWeapon, g_FMMC_STRUCT.iMissionType, TRUE)
				
				BOOL bMK2Weapon = FALSE
				IF IS_WEAPON_MK2(aWeapon)
					bMK2Weapon = TRUE
				ENDIF
				
				// Grab our script data for the weapon
				IF GET_GUNCLUB_WEAPON_DATA_FROM_WEAPON_TYPE(aWeapon, sWeaponStruct, DEFAULT, bMK2Weapon)
					IF GET_CORONA_AMMO_TYPE_INT(sWeaponStruct.eAmmoType) != -1
					
						IF NOT IS_BIT_SET(iBSCheckedAmmoTypes, GET_CORONA_AMMO_TYPE_INT(sWeaponStruct.eAmmoType))
							
							IF PROCESS_PURCHASING_AMMO(AmmoArmourStruct, aWeapon, bApplyAmmo, bChargeCash, bMK2Weapon)
								
								SET_BIT(iBSCheckedAmmoTypes, GET_CORONA_AMMO_TYPE_INT(sWeaponStruct.eAmmoType))

								PRINTLN(" [ammo/armour] PROCESS_PURCHASING_FULL_AMMO - Purchasing ammo for: ", GET_WEAPON_NAME(aWeapon))
								bMadePurchase = TRUE
							#IF IS_DEBUG_BUILD	
							ELSE
								IF IS_WEAPON_MK2(aWeapon)
									PRINTLN(" [ammo/armour] PROCESS_PURCHASING_FULL_AMMO - PROCESS_PURCHASING_AMMO FAIL: ", GET_WEAPON_NAME(aWeapon))
								ENDIF
							#ENDIF
							ENDIF
						#IF IS_DEBUG_BUILD	
						ELSE
							IF IS_WEAPON_MK2(aWeapon)
								PRINTLN(" [ammo/armour] PROCESS_PURCHASING_FULL_AMMO - IS_BIT_SET FAIL: ", GET_WEAPON_NAME(aWeapon))
							ENDIF
						#ENDIF
						ENDIF
					#IF IS_DEBUG_BUILD	
					ELSE
						IF IS_WEAPON_MK2(aWeapon)
							PRINTLN(" [ammo/armour] PROCESS_PURCHASING_FULL_AMMO - GET_CORONA_AMMO_TYPE_INT FAIL: ", GET_WEAPON_NAME(aWeapon))
						ENDIF
					#ENDIF
					ENDIF
				#IF IS_DEBUG_BUILD	
				ELSE
					IF IS_WEAPON_MK2(aWeapon)
						PRINTLN(" [ammo/armour] PROCESS_PURCHASING_FULL_AMMO - GET_GUNCLUB_WEAPON_DATA_FROM_WEAPON_TYPE FAIL: ", GET_WEAPON_NAME(aWeapon))
					ENDIF
				#ENDIF
				ENDIF	
			#IF IS_DEBUG_BUILD	
			ELSE
				IF IS_WEAPON_MK2(aWeapon)
					PRINTLN(" [ammo/armour] PROCESS_PURCHASING_FULL_AMMO - IS_CORONA_WEAPON_VALID FAIL: ", GET_WEAPON_NAME(aWeapon))
				ENDIF
			#ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	IF USE_SERVER_TRANSACTIONS()
		IF bChargeCash
		AND bMadePurchase
			NETWORK_START_BASKET_TRANSACTION_CHECKOUT()
			
			// Set our ammom / armor cash transaction as being active
			SET_AMMO_ARMOR_CASH_TRANSACTION_ACTIVE(AMMO_ARMOR_CASH_TRANSACTION_FULL_AMMO)
		ENDIF
	ENDIF

	// Stop us from offering this to the player any more
	SET_BIT(AmmoArmourStruct.iBitSet, LBD_BOOL_BLOCK_AMMO)
	PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
//	REFRESH_BUTTONS(AmmoArmourStruct)
	SET_BIT(AmmoArmourStruct.iBitSet, LBD_BOOL_REFRESH_NEEDED)
	
	RETURN TRUE
ENDFUNC

PROC HANDLE_BUYING_AMMO(AMMO_ARMOUR_STRUCT& AmmoArmourStruct)

	BOOL bApplyAmmo = TRUE
	BOOL bChargeCash = TRUE
		
	// If PC then we should not actually give the ammo here, purely charge the cash and wait for the server
	IF IS_PC_VERSION()
		IF USE_SERVER_TRANSACTIONS()
			PRINTLN("[ammo/armour] PROCESS_PLAYER_PURCHASING_FULL_AMMO_FOR_CORONA - PC Version, bApplyAmmo = FALSE")
			bApplyAmmo = FALSE
		ENDIF
	ENDIF

	IF AMMO_AVAILABLE(AmmoArmourStruct)
	
		// Count cost of total ammo
		IF NOT IS_BIT_SET(AmmoArmourStruct.iBitSet, LBD_BOOL_COUNT_AMMO_COST)
			COUNT_AMMO_COST(AmmoArmourStruct)
			SET_BIT(AmmoArmourStruct.iBitSet, LBD_BOOL_COUNT_AMMO_COST)
		ELSE
	
			// Can the player afford this max ammo option
			IF NOT CAN_PLAYER_AFFORD_FULL_AMMO(AmmoArmourStruct)
				PRINTLN(" [ammo/armour] HANDLE_BUYING_AMMO - FALSE Total Cost: ", AmmoArmourStruct.iTotalCostToFillAmmo, ", Player Cash: ", GET_CORONA_CASH_AVAILABLE())
				SET_BIT(AmmoArmourStruct.iBitSet, LBD_BOOL_BLOCK_AMMO)
			ENDIF
			
			// Warning screen ON
			IF IS_AMMO_WARNING_SCREEN_ACTIVE(AmmoArmourStruct)

				IF HAS_YES_BUTTON_BEEN_PRESSED()
					// Buy ammo
					PROCESS_PURCHASING_FULL_AMMO(AmmoArmourStruct, bApplyAmmo, bChargeCash)
					TURN_ON_AMMO_WARNING(AmmoArmourStruct, FALSE)
					PRINTLN(" [ammo/armour] HANDLE_BUYING_AMMO - 1 ")
					// Stop us from offering this to the player any more
					SET_BIT(AmmoArmourStruct.iBitSet, LBD_BOOL_BLOCK_AMMO)
				ELSE
					// Quit out
					IF HAS_END_JOB_QUIT_BEEN_PRESSED()
//					OR HAS_VOTING_ENDED(serverVars)
						PRINTLN(" [ammo/armour] HANDLE_BUYING_AMMO - 2 ")
						TURN_ON_AMMO_WARNING(AmmoArmourStruct, FALSE)
					ENDIF	
					PRINTLN(" [ammo/armour] HANDLE_BUYING_AMMO - 3 ")
				ENDIF

				SET_WARNING_MESSAGE_WITH_HEADER_AND_SUBSTRING_FLAGS("FM_CSC_QUIT", "FM_COR_PWAR", (FE_WARNING_YES | FE_WARNING_NO), "", TRUE, AmmoArmourStruct.iTotalCostToFillAmmo)
			// Warning screen OFF
			ELSE
				PRINTLN(" [ammo/armour] HANDLE_BUYING_AMMO - 4 ")
				// Buy ammo button
				IF HAS_END_JOB_BUY_AMMO_BEEN_PRESSED()
					PRINTLN(" [ammo/armour] HANDLE_BUYING_AMMO - 5 ")
					IF WARN_ABOUT_AMMO_COST(AmmoArmourStruct)
						// Activate warning screen
						TURN_ON_AMMO_WARNING(AmmoArmourStruct, TRUE)
						PRINTLN(" [ammo/armour] HANDLE_BUYING_AMMO - 6 ")
					ELSE
						// Buy ammo
						PROCESS_PURCHASING_FULL_AMMO(AmmoArmourStruct, bApplyAmmo, bChargeCash)
						PRINTLN(" [ammo/armour] HANDLE_BUYING_AMMO - 7 ")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//â•šâ•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•â•	

PROC RENDER_LEADERBOARD(LBD_VARS& lbdVars, LEADERBOARD_PLACEMENT_TOOLS& Placement, LBD_SUB_MODE eSubMode, BOOL bThumbVote, STRING sLeaderboardTitle, INT iNumColumns, INT iLeaderboardTitleValue = -1, BOOL bDoSafeChecks = TRUE, BOOL bOnPlayList = FALSE, BOOL bBlockJIP = TRUE)
						
	IF bDoSafeChecks
		IF NOT IS_IT_OK_TO_DRAW_LEADERBOARD(FALSE)		
			EXIT
		ENDIF
	ENDIF
	bThumbVote = bThumbVote
	
	lbdVars.iTeamPos = 1
	
	HIDE_HUD_DURING_LBD()
	
	BOOL IsHQRHELPDisplaying = FALSE
		IsHQRHELPDisplaying = IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HQRHELP")
	IF IS_HELP_MESSAGE_BEING_DISPLAYED()
	AND NOT IsHQRHELPDisplaying
		HIDE_HELP_TEXT_THIS_FRAME()
	ENDIF
	
	//Turn off the pause menu
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_FRONTEND_PAUSE)
	
	// Initialise data
	IF NOT(Placement.bHudScreenInitialised)
		PRELOAD_HEADSHOTS()
		INIT_MPHUD_LEADERBOARD_RUNNING(Placement)
		STOP_CONTROL_SHAKE(PLAYER_CONTROL)
		IF bBlockJIP
			BLOCK_JIP_INVITING_ON_LEADERBOARDS()
		ENDIF

		IF IS_PLAYLIST_IS_A_TOURNAMENT_PLAYLIST()
			OVERRIDE_HUD_COLOUR(HUD_COLOUR_FREEMODE, HUD_COLOUR_T)
			OVERRIDE_HUD_COLOUR(HUD_COLOUR_FREEMODE_DARK, HUD_COLOUR_TDARK)
		ENDIF
		
		// Fade the clouds back in 2193949
		SET_CLOUD_STATE(ENUMCLOUDSTATE_FADING_OUT)
		
		#IF IS_DEBUG_BUILD
		PRINTNL()
		PRINTLN("RENDER_LEADERBOARD___________________________")	
		PRINTNL()
		PRINTLN("tl63MissionName = ", g_FMMC_STRUCT.tl63MissionName)
		PRINTLN("tl63PointsTitle = ", g_FMMC_STRUCT.tl63PointsTitle)
		PRINTLN("RENDER_LEADERBOARD bOnPlayList = ", bOnPlayList)
		PRINT_SUB_MODE(eSubMode)
		
		//		[ammo/armour]
		IF IS_BIT_SET(lbdVars.sAmmoArmour.iBitSet, LBD_BOOL_BLOCK_AMMO)
			PRINTLN("[ammo/armour] LBD_BOOL_BLOCK_AMMO set ")
		ENDIF
		IF g_b_BlockAmmo 			
			PRINTLN("[ammo/armour] g_b_BlockAmmo set ")
		ENDIF
		IF IS_BIT_SET(lbdVars.sAmmoArmour.iBitSet, LBD_BOOL_BLOCK_ARMOUR)
			PRINTLN("[ammo/armour] LBD_BOOL_BLOCK_ARMOUR set ")
		ENDIF
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciREMOVE_ARMOUR_PURCHASE_OPTION)
			PRINTLN("[ammo/armour] ciREMOVE_ARMOUR_PURCHASE_OPTION set ")
		ENDIF
		
		IF HIDE_KILLS_DEATHS_COLUMN_LBD()
			PRINTLN("HIDE_KILLS_DEATHS_COLUMN_LBD ")
		ENDIF
		IF SHOW_ASSISTS_LEADERBOARD()
			PRINTLN("SHOW_ASSISTS_LEADERBOARD ")
		ENDIF
		IF bAGG_RACE(eSubMode)
			IF bAGG_RACE_BEST(eSubMode)
				PRINTLN("bAGG_RACE, bAGG_RACE_BEST ")
			ELSE
				PRINTLN("bAGG_RACE, bAGG_RACE_TIME ")
			ENDIF
		ENDIF
		IF bIS_JUGGERNAUT()
			PRINTLN("bIS_JUGGERNAUT ")
		ENDIF
		IF CUSTOM_LBD_REMIX()
			PRINTLN("CUSTOM_LBD_REMIX ")
		ENDIF
		IF bIS_TURF_WARS()
			PRINTLN("bIS_TURF_WARS ")
		ENDIF
		IF SHOW_SLIPSTREAM_COLUMN()
			PRINTLN("SHOW_SLIPSTREAM_COLUMN ")
		ENDIF
		IF HIDE_KILLS_SHOW_DEATHS_COLUMN_LBD()
			PRINTLN("HIDE_KILLS_SHOW_DEATHS_COLUMN_LBD ")
		ENDIF
		IF IS_THREE_COLUMN_LBD()
			PRINTLN("IS_THREE_COLUMN_LBD ")
		ENDIF
		IF IS_NO_SCORE_NO_KILL_LBD_SET()
			PRINTLN("IS_NO_SCORE_NO_KILL_LBD_SET ")
		ENDIF
		IF REPLACE_SCORE_WITH_KILLS()
			PRINTLN("REPLACE_SCORE_WITH_KILLS ")
		ENDIF
		IF SHOW_SCORE_COLUMN_ON_LEADERBOARD()
			PRINTLN("SHOW_SCORE_COLUMN_ON_LEADERBOARD ")
		ENDIF
		IF g_FinalRoundsLbd
			PRINTLN("g_FinalRoundsLbd ")
		ENDIF
		IF DRAW_CUSTOM_TITLE()
			PRINTLN("DRAW_CUSTOM_TITLE ")
		ENDIF
		IF IS_PLAYLIST_ROUNDS_MISSION(eSubMode)
			PRINTLN("IS_PLAYLIST_ROUNDS_MISSION ")
		ENDIF
		IF IS_PLAYLIST_LBD(eSubMode)
			PRINTLN("IS_PLAYLIST_LBD ")
		ENDIF
		IF SHOW_TIME_COLUMN()
			PRINTLN("SHOW_TIME_COLUMN ")
		ENDIF
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTen, ciLBD_SHOW_SLIPSTREAM_TIME)
			PRINTLN("ciLBD_SHOW_SLIPSTREAM_TIME ")
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTen, ciENABLE_TURF_WAR_CLAIMABLE_PROPS)
			PRINTLN("ciENABLE_TURF_WAR_CLAIMABLE_PROPS ")
		ENDIF		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_TURF_WAR_SHOW_SCORE_ON_LEADERBOARD)
			PRINTLN("ciENABLE_TURF_WAR_SHOW_SCORE_ON_LEADERBOARD ")
		ENDIF
		
		PRINTLN("_____________________________________________")
		PRINTNL()
		#ENDIF
		
		Placement.bHudScreenInitialised = TRUE
	ENDIF
	
	// Dont update hud colours if we're on a playlist
	IF NOT IS_PLAYER_ON_A_PLAYLIST(PLAYER_ID())
	
		INT iSlot
		// Colour heist boards green
		IF Is_Player_Currently_On_MP_Heist(PLAYER_ID())
		OR Is_Player_Currently_On_MP_Heist_Planning(PLAYER_ID())
		OR CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_MISSION_FINALE()
			IF NOT IS_MP_HUD_COLOUR_OVERRIDDEN(HUD_COLOUR_FREEMODE, iSlot)
				OVERRIDE_HUD_COLOUR(HUD_COLOUR_FREEMODE, HUD_COLOUR_H)
				PRINTLN("[HUD_COLOUR] OVERRIDE_LBD_HUD_COLOUR, RENDER_LEADERBOARD")
			ENDIF
			IF NOT IS_MP_HUD_COLOUR_OVERRIDDEN(HUD_COLOUR_FRIENDLY, iSlot)
				OVERRIDE_HUD_COLOUR(HUD_COLOUR_FRIENDLY, HUD_COLOUR_H)
			ENDIF
			IF NOT IS_MP_HUD_COLOUR_OVERRIDDEN(HUD_COLOUR_FREEMODE_DARK, iSlot)
				OVERRIDE_HUD_COLOUR(HUD_COLOUR_FREEMODE_DARK, HUD_COLOUR_HDARK)
			ENDIF
		ENDIF
		
		IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
			IF NOT IS_MP_HUD_COLOUR_OVERRIDDEN(HUD_COLOUR_FREEMODE, iSlot)
				OVERRIDE_HUD_COLOUR(HUD_COLOUR_FREEMODE, HUD_COLOUR_H)
				PRINTLN("[HUD_COLOUR] OVERRIDE_LBD_HUD_COLOUR, RENDER_LEADERBOARD")
			ENDIF
			IF NOT IS_MP_HUD_COLOUR_OVERRIDDEN(HUD_COLOUR_FREEMODE_DARK, iSlot)
				OVERRIDE_HUD_COLOUR(HUD_COLOUR_FREEMODE_DARK, HUD_COLOUR_HDARK)
			ENDIF
		ENDIF
		
		// url:bugstar:2876896 - Premium Race - Are we able to colour the leaderboard yellow afterwards the same?
		IF CV2_WAS_STUNT_RACE_LAUNCHED_AS_PROFESSIONAL()
			IF NOT IS_MP_HUD_COLOUR_OVERRIDDEN(HUD_COLOUR_FREEMODE, iSlot)
				OVERRIDE_HUD_COLOUR(HUD_COLOUR_FREEMODE, HUD_COLOUR_PINKLIGHT)
			ENDIF
			IF NOT IS_MP_HUD_COLOUR_OVERRIDDEN(HUD_COLOUR_FREEMODE_DARK, iSlot)
				OVERRIDE_HUD_COLOUR(HUD_COLOUR_FREEMODE_DARK, HUD_COLOUR_PINK)
			ENDIF
		ENDIF
		BOOL bTransform = CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_TRANSFORM_SERIES(g_FMMC_STRUCT.iRootContentIDHash)
		BOOL bTargetAssault = CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_TARGET_ASSAULT_SERIES(g_FMMC_STRUCT.iRootContentIDHash)
		BOOL bHotring = CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_HOTRING_SERIES(g_FMMC_STRUCT.iRootContentIDHash)
		BOOL bArena = CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_ARENA_SERIES(g_FMMC_STRUCT.iRootContentIDHash)
		BOOL bRaceSeries = CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_RACE_SERIES(g_FMMC_STRUCT.iRootContentIDHash)
		BOOL bSurvivalSeries = CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_SURVIVAL_SERIES(g_FMMC_STRUCT.iRootContentIDHash)
		BOOL bOpenWheelSeries = CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_OPEN_WHEEL_SERIES(g_FMMC_STRUCT.iRootContentIDHash)
		BOOL bStreetRaceSeries = CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_STREET_RACE_SERIES(g_FMMC_STRUCT.iRootContentIDHash)
		BOOL bPursuitSeries = CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_PURSUIT_SERIES(g_FMMC_STRUCT.iRootContentIDHash)
		#IF FEATURE_GEN9_EXCLUSIVE
		BOOL bHSWRaceSeries = CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_HSW_RACE_SERIES(g_FMMC_STRUCT.iRootContentIDHash)
		#ENDIF
		BOOL bCommunitySeries = CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_COMMUNITY_SERIES(g_FMMC_STRUCT.iRootContentIDHash)
		BOOL bCayoPericoSeries = CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_CAYO_PERICO_SERIES(g_FMMC_STRUCT.iRootContentIDHash)
		IF CV2_IS_THIS_JOB_A_SERIES_JOB(TRUE)
		OR bTransform
		OR bTargetAssault
		OR bHotring
		OR bArena
		OR bRaceSeries
		OR bSurvivalSeries
		OR bOpenWheelSeries
		OR bStreetRaceSeries
		OR bPursuitSeries
		OR CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_FEATURED_ADVERSARY_MODE_JOB(g_FMMC_STRUCT.iRootContentIDHash)
		#IF FEATURE_GEN9_EXCLUSIVE
		OR bHSWRaceSeries
		#ENDIF
		OR bCommunitySeries
		OR bCayoPericoSeries
//			IF CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_SPECIAL_VEHICLE_RACE_SERIES(g_FMMC_STRUCT.iRootContentIDHash)
				PRINTLN("[CORONA] SETUP_CORONA_HUD_ELEMENTS - CV2_IS_THIS_JOB_A_SERIES_JOB")
				OVERRIDE_HUD_COLOUR(HUD_COLOUR_FREEMODE, HUD_COLOUR_PINKLIGHT)
				OVERRIDE_HUD_COLOUR(HUD_COLOUR_FREEMODE_DARK, HUD_COLOUR_PINK)
				
//			ELIF CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_BUNKER_SERIES(g_FMMC_STRUCT.iRootContentIDHash)
//				PRINTLN("[CORONA] SETUP_CORONA_HUD_ELEMENTS - CV2_IS_THIS_JOB_A_SERIES_JOB - CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_BUNKER_SERIES")
//				OVERRIDE_HUD_COLOUR(HUD_COLOUR_FREEMODE, HUD_COLOUR_PINKLIGHT)
//				OVERRIDE_HUD_COLOUR(HUD_COLOUR_FREEMODE_DARK, HUD_COLOUR_PINK)
//				
//			ELIF bTargetAssault
//				PRINTLN("[CORONA] SETUP_CORONA_HUD_ELEMENTS - CV2_IS_THIS_JOB_A_SERIES_JOB - bTargetAssault")
//				OVERRIDE_HUD_COLOUR(HUD_COLOUR_FREEMODE, HUD_COLOUR_PINKLIGHT)
//				OVERRIDE_HUD_COLOUR(HUD_COLOUR_FREEMODE_DARK, HUD_COLOUR_PINK)
//				
//			ELIF bHotring
//				PRINTLN("[CORONA] SETUP_CORONA_HUD_ELEMENTS - CV2_IS_THIS_JOB_A_SERIES_JOB - bHotring")
//				OVERRIDE_HUD_COLOUR(HUD_COLOUR_FREEMODE, HUD_COLOUR_PINKLIGHT)
//				OVERRIDE_HUD_COLOUR(HUD_COLOUR_FREEMODE_DARK, HUD_COLOUR_PINK)
//				
//			ELIF bArena
//				PRINTLN("[CORONA] SETUP_CORONA_HUD_ELEMENTS - CV2_IS_THIS_JOB_A_SERIES_JOB - bArena")
//				OVERRIDE_HUD_COLOUR(HUD_COLOUR_FREEMODE, HUD_COLOUR_PINKLIGHT)
//				OVERRIDE_HUD_COLOUR(HUD_COLOUR_FREEMODE_DARK, HUD_COLOUR_PINK)
//				
//			ELIF bRaceSeries
//				PRINTLN("[CORONA] SETUP_CORONA_HUD_ELEMENTS - CV2_IS_THIS_JOB_A_SERIES_JOB - bRaceSeries")
//				OVERRIDE_HUD_COLOUR(HUD_COLOUR_FREEMODE, HUD_COLOUR_PINKLIGHT)
//				OVERRIDE_HUD_COLOUR(HUD_COLOUR_FREEMODE_DARK, HUD_COLOUR_PINK)
//				
//			ELIF bSurvivalSeries
//				PRINTLN("[CORONA] SETUP_CORONA_HUD_ELEMENTS - CV2_IS_THIS_JOB_A_SERIES_JOB - bSurvivalSeries")
//				OVERRIDE_HUD_COLOUR(HUD_COLOUR_FREEMODE, HUD_COLOUR_PINKLIGHT)
//				OVERRIDE_HUD_COLOUR(HUD_COLOUR_FREEMODE_DARK, HUD_COLOUR_PINK)
//				
//			ELIF bOpenWheelSeries
//				PRINTLN("[CORONA] SETUP_CORONA_HUD_ELEMENTS - CV2_IS_THIS_JOB_A_SERIES_JOB - bOpenWheelSeries")
//				OVERRIDE_HUD_COLOUR(HUD_COLOUR_FREEMODE, HUD_COLOUR_PINKLIGHT)
//				OVERRIDE_HUD_COLOUR(HUD_COLOUR_FREEMODE_DARK, HUD_COLOUR_PINK)
//				
//			ELIF bStreetRaceSeries
//				PRINTLN("[CORONA] SETUP_CORONA_HUD_ELEMENTS - CV2_IS_THIS_JOB_A_SERIES_JOB - bStreetRaceSeries")
//				OVERRIDE_HUD_COLOUR(HUD_COLOUR_FREEMODE, HUD_COLOUR_PINKLIGHT)
//				OVERRIDE_HUD_COLOUR(HUD_COLOUR_FREEMODE_DARK, HUD_COLOUR_PINK)
//				
//			ELIF bPursuitSeries
//				PRINTLN("[CORONA] SETUP_CORONA_HUD_ELEMENTS - CV2_IS_THIS_JOB_A_SERIES_JOB - bPursuitSeries")
//				OVERRIDE_HUD_COLOUR(HUD_COLOUR_FREEMODE, HUD_COLOUR_PINKLIGHT)
//				OVERRIDE_HUD_COLOUR(HUD_COLOUR_FREEMODE_DARK, HUD_COLOUR_PINK)
//				
//			#IF FEATURE_GEN9_EXCLUSIVE
//			ELIF bHSWRaceSeries
//				PRINTLN("[CORONA] SETUP_CORONA_HUD_ELEMENTS - CV2_IS_THIS_JOB_A_SERIES_JOB - bHSWRaceSeries")
//				OVERRIDE_HUD_COLOUR(HUD_COLOUR_FREEMODE, HUD_COLOUR_PINKLIGHT)
//				OVERRIDE_HUD_COLOUR(HUD_COLOUR_FREEMODE_DARK, HUD_COLOUR_PINK)
//				
//			#ENDIF
//				
//			ELIF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_RACE
//				PRINTLN("[CORONA] SETUP_CORONA_HUD_ELEMENTS - CV2_IS_THIS_JOB_A_SERIES_JOB - FMMC_TYPE_RACE")
//				OVERRIDE_HUD_COLOUR(HUD_COLOUR_FREEMODE, HUD_COLOUR_PINKLIGHT)
//				OVERRIDE_HUD_COLOUR(HUD_COLOUR_FREEMODE_DARK, HUD_COLOUR_PINK)
//				
//			ELIF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION
//				PRINTLN("[CORONA] SETUP_CORONA_HUD_ELEMENTS - CV2_IS_THIS_JOB_A_SERIES_JOB - FMMC_TYPE_MISSION")
//				OVERRIDE_HUD_COLOUR(HUD_COLOUR_FREEMODE, HUD_COLOUR_PINKLIGHT)
//				OVERRIDE_HUD_COLOUR(HUD_COLOUR_FREEMODE_DARK, HUD_COLOUR_PINK)
//			ENDIF
		ENDIF
		
	ENDIF
	
	SET_ON_LBD_GLOBAL(TRUE)
	
//	#IF IS_DEBUG_BUILD
//		PRINT_SUB_MODE(eSubMode)
//	#ENDIF
	
	DRAW_MAIN_HUD_BACKGROUND()
	DISPLAY_HUD_WHEN_PAUSED_THIS_FRAME()
	FILL_COLUMN_TITLES(lbdVars, Placement, eSubMode, iNumColumns)	
	DRAW_WHITE_TOP_RECT(Placement)
	LEADERBOARD_TITLE_AND_TOP_BAR(lbdVars, Placement, sLeaderboardTitle, eSubMode, iLeaderboardTitleValue, bOnPlayList)
	DRAW_SCROLLBARS(lbdVars, Placement)
	DRAW_LAMAR_ROW(lbdVars, Placement, eSubMode, iNumColumns)
	g_b_PopulateReady = TRUE // this ensures all the hud elements draw together
	DISABLE_CELLPHONE_THIS_FRAME_ONLY()
	THEFEED_HIDE_THIS_FRAME()
ENDPROC 

// Gets position of the bars at the bottom of each team in team leaderboards
// "POSITION 5th Place"
FUNC INT GET_TEAM_BARS_ROW(INT iLocalTeam, INT iNumTeams, 
							INT iTeamWinCount, INT iTeam2Count, INT iTeam3Count, INT iTeam4Count, INT iTeam5Count, INT iTeam6Count, INT iTeam7Count, INT iTeam8Count,
							INT iTeamWin, INT iTeam2, INT iTeam3, INT iTeam4, INT iTeam5, INT iTeam6, INT iTeam7, INT iLosingTeam, BOOL isDpadLbd,
							
							INT iTeam9Count = 0, INT iTeam10Count = 0, INT iTeam11Count = 0, INT iTeam12Count = 0, INT iTeam13Count = 0, INT iTeam14Count = 0, INT iTeam15Count = 0,
							INT iTeam8 = 0, INT iTeam9 = 0, INT iTeam10 = 0, INT iTeam11 = 0, INT iTeam12 = 0, INT iTeam13 = 0, INT iTeam14 = 0)

	INT iRow
	
	UNUSED_PARAMETER(iLosingTeam)
	
	// 1975725
	INT iExtraRow = 1
	
	// 2167305 Team win count should never be 0 
	IF iTeamWinCount = 0
		iTeamWinCount = 1
	ENDIF
	
	BOOL bWon
	
	IF isDpadLbd
		bWon = ( iLocalTeam = iTeamWin )
	ELSE
		bWon = DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeamWin)
	ENDIF

	SWITCH iNumTeams
		CASE 15
			IF bWon		
				IF isDpadLbd
					iRow = 0
				ELSE
					iRow = iTeamWinCount + iExtraRow
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam2, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount)
				ELSE
					iRow = (iTeamWinCount + iTeam2Count + 1 + iExtraRow + 1)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam3, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count + iTeam3Count + 2 + iExtraRow + 2)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam4, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count + iTeam4Count + 3 + iExtraRow + 3)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam5, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count + iTeam5Count + 4 + iExtraRow + 4)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam6, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count + iTeam6Count + 5 + iExtraRow + 5)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam7, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Count + 6 + iExtraRow + 6)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam8, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Count + iTeam8Count + 7 + iExtraRow + 7)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam9, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Count + iTeam8Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Count + iTeam8Count + iTeam9Count + 8 + iExtraRow + 8)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam10, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Count + iTeam8Count + iTeam9Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Count + 9 + iExtraRow + 9)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam11, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Count + iTeam11Count + 10 + iExtraRow + 10)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam12, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Count + iTeam11Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Count + iTeam11Count + iTeam12Count + 11 + iExtraRow + 11)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam13, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Count + iTeam11Count + iTeam12Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Count + iTeam11Count + iTeam12Count + iTeam13Count + 12 + iExtraRow + 12)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam14, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Count + iTeam11Count + iTeam12Count + iTeam13Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Count + iTeam11Count + iTeam12Count + iTeam13Count + iTeam14Count + 13 + iExtraRow + 13)
				ENDIF
			ELSE
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Count + iTeam11Count + iTeam12Count + iTeam13Count + iTeam14Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Count + iTeam11Count + iTeam12Count + iTeam13Count + iTeam14Count + iTeam15Count + 14 + iExtraRow + 14)
				ENDIF
			ENDIF
		BREAK
		CASE 14
			IF bWon		
				IF isDpadLbd
					iRow = 0
				ELSE
					iRow = iTeamWinCount + iExtraRow
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam2, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount)
				ELSE
					iRow = (iTeamWinCount + iTeam2Count + 1 + iExtraRow + 1)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam3, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count + iTeam3Count + 2 + iExtraRow + 2)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam4, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count + iTeam4Count + 3 + iExtraRow + 3)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam5, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count + iTeam5Count + 4 + iExtraRow + 4)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam6, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count + iTeam6Count + 5 + iExtraRow + 5)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam7, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Count + 6 + iExtraRow + 6)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam8, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Count + iTeam8Count + 7 + iExtraRow + 7)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam9, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Count + iTeam8Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Count + iTeam8Count + iTeam9Count + 8 + iExtraRow + 8)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam10, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Count + iTeam8Count + iTeam9Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Count + 9 + iExtraRow + 9)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam11, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Count + iTeam11Count + 10 + iExtraRow + 10)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam12, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Count + iTeam11Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Count + iTeam11Count + iTeam12Count + 11 + iExtraRow + 11)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam13, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Count + iTeam11Count + iTeam12Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Count + iTeam11Count + iTeam12Count + iTeam13Count + 12 + iExtraRow + 12)
				ENDIF
			ELSE
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Count + iTeam11Count + iTeam12Count + iTeam13Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Count + iTeam11Count + iTeam12Count + iTeam13Count + iTeam14Count + 13 + iExtraRow + 13)
				ENDIF
			ENDIF
		BREAK
		CASE 13
			IF bWon		
				IF isDpadLbd
					iRow = 0
				ELSE
					iRow = iTeamWinCount + iExtraRow
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam2, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount)
				ELSE
					iRow = (iTeamWinCount + iTeam2Count + 1 + iExtraRow + 1)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam3, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count + iTeam3Count + 2 + iExtraRow + 2)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam4, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count + iTeam4Count + 3 + iExtraRow + 3)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam5, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count + iTeam5Count + 4 + iExtraRow + 4)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam6, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count + iTeam6Count + 5 + iExtraRow + 5)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam7, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Count + 6 + iExtraRow + 6)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam8, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Count + iTeam8Count + 7 + iExtraRow + 7)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam9, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Count + iTeam8Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Count + iTeam8Count + iTeam9Count + 8 + iExtraRow + 8)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam10, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Count + iTeam8Count + iTeam9Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Count + 9 + iExtraRow + 9)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam11, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Count + iTeam11Count + 10 + iExtraRow + 10)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam12, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Count + iTeam11Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Count + iTeam11Count + iTeam12Count + 11 + iExtraRow + 11)
				ENDIF
			ELSE
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Count + iTeam11Count + iTeam12Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Count + iTeam11Count + iTeam12Count + iTeam13Count + 12 + iExtraRow + 12)
				ENDIF
			ENDIF
		BREAK
		CASE 12
			IF bWon		
				IF isDpadLbd
					iRow = 0
				ELSE
					iRow = iTeamWinCount + iExtraRow
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam2, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount)
				ELSE
					iRow = (iTeamWinCount + iTeam2Count + 1 + iExtraRow + 1)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam3, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count + iTeam3Count + 2 + iExtraRow + 2)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam4, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count + iTeam4Count + 3 + iExtraRow + 3)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam5, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count + iTeam5Count + 4 + iExtraRow + 4)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam6, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count + iTeam6Count + 5 + iExtraRow + 5)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam7, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Count + 6 + iExtraRow + 6)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam8, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Count + iTeam8Count + 7 + iExtraRow + 7)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam9, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Count + iTeam8Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Count + iTeam8Count + iTeam9Count + 8 + iExtraRow + 8)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam10, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Count + iTeam8Count + iTeam9Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Count + 9 + iExtraRow + 9)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam11, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Count + iTeam11Count + 10 + iExtraRow + 10)
				ENDIF
			ELSE
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Count + iTeam11Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Count + iTeam11Count + iTeam12Count + 11 + iExtraRow + 11)
				ENDIF
			ENDIF
		BREAK
		CASE 11
			IF bWon		
				IF isDpadLbd
					iRow = 0
				ELSE
					iRow = iTeamWinCount + iExtraRow
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam2, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount)
				ELSE
					iRow = (iTeamWinCount + iTeam2Count + 1 + iExtraRow + 1)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam3, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count + iTeam3Count + 2 + iExtraRow + 2)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam4, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count + iTeam4Count + 3 + iExtraRow + 3)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam5, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count + iTeam5Count + 4 + iExtraRow + 4)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam6, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count + iTeam6Count + 5 + iExtraRow + 5)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam7, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Count + 6 + iExtraRow + 6)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam8, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Count + iTeam8Count + 7 + iExtraRow + 7)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam9, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Count + iTeam8Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Count + iTeam8Count + iTeam9Count + 8 + iExtraRow + 8)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam10, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Count + iTeam8Count + iTeam9Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Count + 9 + iExtraRow + 9)
				ENDIF
			ELSE
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Count + iTeam11Count + 10 + iExtraRow + 10)
				ENDIF
			ENDIF
		BREAK
		CASE 10
			IF bWon		
				IF isDpadLbd
					iRow = 0
				ELSE
					iRow = iTeamWinCount + iExtraRow
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam2, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount)
				ELSE
					iRow = (iTeamWinCount + iTeam2Count + 1 + iExtraRow + 1)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam3, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count + iTeam3Count + 2 + iExtraRow + 2)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam4, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count + iTeam4Count + 3 + iExtraRow + 3)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam5, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count + iTeam5Count + 4 + iExtraRow + 4)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam6, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count + iTeam6Count + 5 + iExtraRow + 5)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam7, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Count + 6 + iExtraRow + 6)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam8, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Count + iTeam8Count + 7 + iExtraRow + 7)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam9, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Count + iTeam8Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Count + iTeam8Count + iTeam9Count + 8 + iExtraRow + 8)
				ENDIF
			ELSE
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Count + iTeam8Count + iTeam9Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Count + 9 + iExtraRow + 9)
				ENDIF
			ENDIF
		BREAK
		CASE 9
			IF bWon		
				IF isDpadLbd
					iRow = 0
				ELSE
					iRow = iTeamWinCount + iExtraRow
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam2, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount)
				ELSE
					iRow = (iTeamWinCount + iTeam2Count + 1 + iExtraRow + 1)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam3, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count + iTeam3Count + 2 + iExtraRow + 2)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam4, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count + iTeam4Count + 3 + iExtraRow + 3)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam5, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count + iTeam5Count + 4 + iExtraRow + 4)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam6, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count + iTeam6Count + 5 + iExtraRow + 5)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam7, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Count + 6 + iExtraRow + 6)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam8, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Count + iTeam8Count + 7 + iExtraRow + 7)
				ENDIF
			ELSE
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Count + iTeam8Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Count + iTeam8Count + iTeam9Count + 8 + iExtraRow + 8)
				ENDIF
			ENDIF
		BREAK
		CASE 8
			IF bWon		
				IF isDpadLbd
					iRow = 0
				ELSE
					iRow = iTeamWinCount + iExtraRow
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam2, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount)
				ELSE
					iRow = (iTeamWinCount + iTeam2Count + 1 + iExtraRow + 1)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam3, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count + iTeam3Count + 2 + iExtraRow + 2)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam4, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count + iTeam4Count + 3 + iExtraRow + 3)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam5, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count + iTeam5Count + 4 + iExtraRow + 4)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam6, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count + iTeam6Count + 5 + iExtraRow + 5)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam7, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Count + 6 + iExtraRow + 6)
				ENDIF
			ELSE
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count + 7 + iExtraRow + 7)
				ENDIF
			ENDIF
		BREAK
		CASE 7
			IF bWon	
				IF isDpadLbd
					iRow = 0
				ELSE
					iRow = iTeamWinCount + iExtraRow
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam2, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount)
				ELSE
					iRow = (iTeamWinCount + iTeam2Count + 1 + iExtraRow + 1)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam3, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count + iTeam3Count + 2 + iExtraRow + 2)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam4, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count + iTeam4Count + 3 + iExtraRow + 3)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam5, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count + iTeam5Count+ 4 + iExtraRow + 4)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam6, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count + iTeam6Count + 5 + iExtraRow + 5)
				ENDIF
			ELSE
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Count + 6 + iExtraRow + 6)
				ENDIF
			ENDIF
		BREAK
		CASE 6
			IF bWon	
				IF isDpadLbd
					iRow = 0
				ELSE
					iRow = iTeamWinCount + iExtraRow
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam2, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount)
				ELSE
					iRow = (iTeamWinCount + iTeam2Count + 1 + iExtraRow + 1)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam3, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count + iTeam3Count + 2 + iExtraRow + 2)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam4, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count + iTeam4Count + 3 + iExtraRow + 3)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam5, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count + iTeam5Count + 4 + iExtraRow + 4)
				ENDIF
			ELSE
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count + iTeam6Count + 5 + iExtraRow + 5)
				ENDIF
			ENDIF
		BREAK
		CASE 5
			IF bWon		
				IF isDpadLbd
					iRow = 0
				ELSE
					iRow = iTeamWinCount + iExtraRow
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam2, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount)
				ELSE
					iRow = (iTeamWinCount + iTeam2Count + 1 + iExtraRow + 1)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam3, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count + iTeam3Count + 2 + iExtraRow + 2)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam4, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count + iTeam4Count + 3 + iExtraRow + 3)
				ENDIF
			ELSE
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count + iTeam5Count + 4 + iExtraRow + 4)
				ENDIF
			ENDIF
		BREAK
		CASE 4
			IF bWon	
				IF isDpadLbd
					iRow = 0
				ELSE
					iRow = iTeamWinCount + iExtraRow
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam2, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount) 
				ELSE
					iRow = (iTeamWinCount + iTeam2Count + 1 + iExtraRow + 1)  
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam3, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count + iTeam3Count + 2 + iExtraRow + 2)
				ENDIF
			ELSE
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count + iTeam4Count + 3 + iExtraRow + 3)
				ENDIF
			ENDIF
		BREAK
		CASE 3
			IF bWon	
				IF isDpadLbd
					iRow = 0
				ELSE
					iRow = iTeamWinCount + iExtraRow
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam2, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamWinCount)
				ELSE
					iRow = (iTeamWinCount + iTeam2Count + 1 + iExtraRow + 1)
				ENDIF
			ELSE
				IF isDpadLbd
					iRow = (iTeamWinCount+ iTeam2Count)
				ELSE
					iRow = (iTeamWinCount+ iTeam2Count + iTeam3Count + 2 + iExtraRow + 2)
				ENDIF
			ENDIF
		BREAK
		CASE 2
			IF bWon			
				IF isDpadLbd
					iRow = 0
				ELSE
					iRow = iTeamWinCount + iExtraRow
				ENDIF
			ELSE
				IF isDpadLbd
					iRow = (iTeamWinCount)
				ELSE
					iRow = (iTeamWinCount + iTeam2Count + 1 + iExtraRow + 1)
				ENDIF
			ENDIF
		BREAK	
		
		CASE 1
		CASE 0
			IF bWon
				IF isDpadLbd
					iRow = 0
				ELSE
					iRow = (iTeamWinCount + iExtraRow)
				ENDIF
			ELSE
				IF isDpadLbd
					iRow = 0
				ELSE
					IF iTeamWinCount >= iTeam2Count
						iRow = (iTeamWinCount + iExtraRow)
					ELSE	
						iRow = (iTeam2Count + iExtraRow)
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	PRINTLN("[CS_TEAM_LBD] [GET_TEAM_BARS_ROW]       iLocalTeam = ", iLocalTeam, " iTeamWin  = ", iTeamWin, " iRow = ", iRow, " iTeamWinCount = ", 
				iTeamWinCount, " iLosingTeam = ", iLosingTeam, " iNumTeams = ", iNumTeams, " iTeam2Count = ", iTeam2Count)
	
	RETURN iRow	
ENDFUNC

FUNC INT GET_ROW_TO_DRAW_PLAYERS(INT iLocalTeam, INT iNumTeams, INT iTeamSlotCounter, 
							INT iTeamWinCount, INT iTeam2Count, INT iTeam3Count, INT iTeam4Count, INT iTeam5Count, INT iTeam6Count, INT iTeam7Count,
							INT iTeamWin, INT iTeam2, INT iTeam3, INT iTeam4, INT iTeam5, INT iTeam6, INT iTeam7, INT iLosingTeam, BOOL isDpadLbd, BOOL bTeamLbd,
							
							INT iTeam8 = 0, INT iTeam9 = 0, INT iTeam10 = 0, INT iTeam11 = 0, INT iTeam12 = 0, INT iTeam13 = 0, INT iTeam14 = 0,
							INT iTeam8Count = 0, INT iTeam9Count = 0, INT iTeam10Count = 0, INT iTeam11Count = 0, INT iTeam12Count = 0, INT iTeam13Count = 0, INT iTeam14Count = 0)

	INT iRow
	
	UNUSED_PARAMETER(iLosingTeam)
	
	// 1975725
	INT iExtraTeamNameRow = 0
	IF bTeamLbd 
		iExtraTeamNameRow = 1
	ENDIF
	
	BOOL bWon
	
	IF isDpadLbd
		bWon = ( iLocalTeam = iTeamWin )
	ELSE
		bWon = DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeamWin)
	ENDIF

	SWITCH iNumTeams	
		CASE 15
			IF bWon	
				IF isDpadLbd
					iRow = iTeamSlotCounter 
				ELSE
					iRow = iTeamSlotCounter + iExtraTeamNameRow
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam2, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter + iTeamWinCount)
				ELSE
					iRow = (iTeamSlotCounter + iTeamWinCount + 1 + iExtraTeamNameRow + 1)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam3, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter + iTeamWinCount+ iTeam2Count)
				ELSE
					iRow = (iTeamSlotCounter + iTeamWinCount+ iTeam2Count + 2 + iExtraTeamNameRow + 2)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam4, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count)
				ELSE
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count + 3 + iExtraTeamNameRow + 3)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam5, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count)
				ELSE
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count + 4 + iExtraTeamNameRow + 4)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam6, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count)
				ELSE
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count + 5 + iExtraTeamNameRow + 5)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam7, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count)
				ELSE
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + 6 + iExtraTeamNameRow + 6)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam8, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count)
				ELSE
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + 7 + iExtraTeamNameRow + 7)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam9, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count)
				ELSE
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count + 8 + iExtraTeamNameRow + 8)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam10, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count + iTeam9Count)
				ELSE
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count + iTeam9Count + 9 + iExtraTeamNameRow + 9)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam11, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Count)
				ELSE
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Count + 10 + iExtraTeamNameRow + 10)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam12, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Count + iTeam11Count)
				ELSE
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Count + iTeam11Count + 11 + iExtraTeamNameRow + 11)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam13, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Count + iTeam11Count + iTeam12Count)
				ELSE
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Count + iTeam11Count + iTeam12Count + 12 + iExtraTeamNameRow + 12)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam14, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Count + iTeam11Count + iTeam12Count + iTeam13Count)
				ELSE
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Count + iTeam11Count + iTeam12Count + iTeam13Count + 13 + iExtraTeamNameRow + 13)
				ENDIF
			ELSE
				IF isDpadLbd
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Count + iTeam11Count + iTeam12Count + iTeam13Count + iTeam14Count)
				ELSE
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Count + iTeam11Count + iTeam12Count + iTeam13Count + iTeam14Count + 14 + iExtraTeamNameRow + 14)
				ENDIF
			ENDIF
		BREAK
		CASE 14
			IF bWon	
				IF isDpadLbd
					iRow = iTeamSlotCounter 
				ELSE
					iRow = iTeamSlotCounter + iExtraTeamNameRow
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam2, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter + iTeamWinCount)
				ELSE
					iRow = (iTeamSlotCounter + iTeamWinCount + 1 + iExtraTeamNameRow + 1)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam3, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter + iTeamWinCount+ iTeam2Count)
				ELSE
					iRow = (iTeamSlotCounter + iTeamWinCount+ iTeam2Count + 2 + iExtraTeamNameRow + 2)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam4, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count)
				ELSE
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count + 3 + iExtraTeamNameRow + 3)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam5, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count)
				ELSE
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count + 4 + iExtraTeamNameRow + 4)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam6, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count)
				ELSE
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count + 5 + iExtraTeamNameRow + 5)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam7, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count)
				ELSE
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + 6 + iExtraTeamNameRow + 6)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam8, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count)
				ELSE
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + 7 + iExtraTeamNameRow + 7)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam9, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count)
				ELSE
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count + 8 + iExtraTeamNameRow + 8)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam10, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count + iTeam9Count)
				ELSE
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count + iTeam9Count + 9 + iExtraTeamNameRow + 9)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam11, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Count)
				ELSE
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Count + 10 + iExtraTeamNameRow + 10)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam12, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Count + iTeam11Count)
				ELSE
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Count + iTeam11Count + 11 + iExtraTeamNameRow + 11)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam13, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Count + iTeam11Count + iTeam12Count)
				ELSE
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Count + iTeam11Count + iTeam12Count + 12 + iExtraTeamNameRow + 12)
				ENDIF
			ELSE
				IF isDpadLbd
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Count + iTeam11Count + iTeam12Count + iTeam13Count)
				ELSE
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Count + iTeam11Count + iTeam12Count + iTeam13Count + 13 + iExtraTeamNameRow + 13)
				ENDIF
			ENDIF
		BREAK
		CASE 13
			IF bWon	
				IF isDpadLbd
					iRow = iTeamSlotCounter 
				ELSE
					iRow = iTeamSlotCounter + iExtraTeamNameRow
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam2, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter + iTeamWinCount)
				ELSE
					iRow = (iTeamSlotCounter + iTeamWinCount + 1 + iExtraTeamNameRow + 1)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam3, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter + iTeamWinCount+ iTeam2Count)
				ELSE
					iRow = (iTeamSlotCounter + iTeamWinCount+ iTeam2Count + 2 + iExtraTeamNameRow + 2)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam4, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count)
				ELSE
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count + 3 + iExtraTeamNameRow + 3)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam5, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count)
				ELSE
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count + 4 + iExtraTeamNameRow + 4)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam6, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count)
				ELSE
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count + 5 + iExtraTeamNameRow + 5)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam7, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count)
				ELSE
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + 6 + iExtraTeamNameRow + 6)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam8, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count)
				ELSE
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + 7 + iExtraTeamNameRow + 7)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam9, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count)
				ELSE
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count + 8 + iExtraTeamNameRow + 8)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam10, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count + iTeam9Count)
				ELSE
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count + iTeam9Count + 9 + iExtraTeamNameRow + 9)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam11, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Count)
				ELSE
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Count + 10 + iExtraTeamNameRow + 10)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam12, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Count + iTeam11Count)
				ELSE
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Count + iTeam11Count + 11 + iExtraTeamNameRow + 11)
				ENDIF
			ELSE
				IF isDpadLbd
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Count + iTeam11Count + iTeam12Count)
				ELSE
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Count + iTeam11Count + iTeam12Count + 12 + iExtraTeamNameRow + 12)
				ENDIF
			ENDIF
		BREAK
		CASE 12
			IF bWon	
				IF isDpadLbd
					iRow = iTeamSlotCounter 
				ELSE
					iRow = iTeamSlotCounter + iExtraTeamNameRow
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam2, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter + iTeamWinCount)
				ELSE
					iRow = (iTeamSlotCounter + iTeamWinCount + 1 + iExtraTeamNameRow + 1)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam3, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter + iTeamWinCount+ iTeam2Count)
				ELSE
					iRow = (iTeamSlotCounter + iTeamWinCount+ iTeam2Count + 2 + iExtraTeamNameRow + 2)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam4, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count)
				ELSE
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count + 3 + iExtraTeamNameRow + 3)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam5, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count)
				ELSE
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count + 4 + iExtraTeamNameRow + 4)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam6, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count)
				ELSE
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count + 5 + iExtraTeamNameRow + 5)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam7, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count)
				ELSE
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + 6 + iExtraTeamNameRow + 6)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam8, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count)
				ELSE
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + 7 + iExtraTeamNameRow + 7)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam9, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count)
				ELSE
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count + 8 + iExtraTeamNameRow + 8)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam10, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count + iTeam9Count)
				ELSE
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count + iTeam9Count + 9 + iExtraTeamNameRow + 9)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam11, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Count)
				ELSE
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Count + 10 + iExtraTeamNameRow + 10)
				ENDIF
			ELSE
				IF isDpadLbd
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Count + iTeam11Count)
				ELSE
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Count + iTeam11Count + 11 + iExtraTeamNameRow + 11)
				ENDIF
			ENDIF
		BREAK
		CASE 11
			IF bWon	
				IF isDpadLbd
					iRow = iTeamSlotCounter 
				ELSE
					iRow = iTeamSlotCounter + iExtraTeamNameRow
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam2, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter + iTeamWinCount)
				ELSE
					iRow = (iTeamSlotCounter + iTeamWinCount + 1 + iExtraTeamNameRow + 1)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam3, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter + iTeamWinCount+ iTeam2Count)
				ELSE
					iRow = (iTeamSlotCounter + iTeamWinCount+ iTeam2Count + 2 + iExtraTeamNameRow + 2)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam4, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count)
				ELSE
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count + 3 + iExtraTeamNameRow + 3)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam5, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count)
				ELSE
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count + 4 + iExtraTeamNameRow + 4)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam6, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count)
				ELSE
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count + 5 + iExtraTeamNameRow + 5)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam7, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count)
				ELSE
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + 6 + iExtraTeamNameRow + 6)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam8, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count)
				ELSE
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + 7 + iExtraTeamNameRow + 7)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam9, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count)
				ELSE
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count + 8 + iExtraTeamNameRow + 8)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam10, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count + iTeam9Count)
				ELSE
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count + iTeam9Count + 9 + iExtraTeamNameRow + 9)
				ENDIF
			ELSE
				IF isDpadLbd
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Count)
				ELSE
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Count + 10 + iExtraTeamNameRow + 10)
				ENDIF
			ENDIF
		BREAK
		CASE 10
			IF bWon	
				IF isDpadLbd
					iRow = iTeamSlotCounter 
				ELSE
					iRow = iTeamSlotCounter + iExtraTeamNameRow
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam2, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter + iTeamWinCount)
				ELSE
					iRow = (iTeamSlotCounter + iTeamWinCount + 1 + iExtraTeamNameRow + 1)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam3, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter + iTeamWinCount+ iTeam2Count)
				ELSE
					iRow = (iTeamSlotCounter + iTeamWinCount+ iTeam2Count + 2 + iExtraTeamNameRow + 2)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam4, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count)
				ELSE
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count + 3 + iExtraTeamNameRow + 3)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam5, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count)
				ELSE
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count + 4 + iExtraTeamNameRow + 4)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam6, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count)
				ELSE
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count + 5 + iExtraTeamNameRow + 5)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam7, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count)
				ELSE
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + 6 + iExtraTeamNameRow + 6)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam8, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count)
				ELSE
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + 7 + iExtraTeamNameRow + 7)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam9, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count)
				ELSE
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count + 8 + iExtraTeamNameRow + 8)
				ENDIF
			ELSE
				IF isDpadLbd
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count + iTeam9Count)
				ELSE
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count + iTeam9Count + 9 + iExtraTeamNameRow + 9)
				ENDIF
			ENDIF
		BREAK
		CASE 9
			IF bWon	
				IF isDpadLbd
					iRow = iTeamSlotCounter 
				ELSE
					iRow = iTeamSlotCounter + iExtraTeamNameRow
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam2, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter + iTeamWinCount)
				ELSE
					iRow = (iTeamSlotCounter + iTeamWinCount + 1 + iExtraTeamNameRow + 1)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam3, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter + iTeamWinCount+ iTeam2Count)
				ELSE
					iRow = (iTeamSlotCounter + iTeamWinCount+ iTeam2Count + 2 + iExtraTeamNameRow + 2)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam4, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count)
				ELSE
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count + 3 + iExtraTeamNameRow + 3)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam5, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count)
				ELSE
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count + 4 + iExtraTeamNameRow + 4)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam6, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count)
				ELSE
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count + 5 + iExtraTeamNameRow + 5)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam7, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count)
				ELSE
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + 6 + iExtraTeamNameRow + 6)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam8, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count)
				ELSE
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + 7 + iExtraTeamNameRow + 7)
				ENDIF
			ELSE
				IF isDpadLbd
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count)
				ELSE
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count + 8 + iExtraTeamNameRow + 8)
				ENDIF
			ENDIF
		BREAK
		CASE 8
			IF bWon	
				IF isDpadLbd
					iRow = iTeamSlotCounter 
				ELSE
					iRow = iTeamSlotCounter + iExtraTeamNameRow
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam2, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter + iTeamWinCount)
				ELSE
					iRow = (iTeamSlotCounter + iTeamWinCount + 1 + iExtraTeamNameRow + 1)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam3, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter + iTeamWinCount+ iTeam2Count)
				ELSE
					iRow = (iTeamSlotCounter + iTeamWinCount+ iTeam2Count + 2 + iExtraTeamNameRow + 2)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam4, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count)
				ELSE
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count + 3 + iExtraTeamNameRow + 3)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam5, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count)
				ELSE
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count + 4 + iExtraTeamNameRow + 4)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam6, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count)
				ELSE
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count + 5 + iExtraTeamNameRow + 5)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam7, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count)
				ELSE
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + 6 + iExtraTeamNameRow + 6)
				ENDIF
			ELSE
				IF isDpadLbd
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count)
				ELSE
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + 7 + iExtraTeamNameRow + 7)
				ENDIF
			ENDIF
		BREAK
		
		CASE 7
			IF bWon		
				IF isDpadLbd
					iRow = iTeamSlotCounter 
				ELSE
					iRow = iTeamSlotCounter  + iExtraTeamNameRow
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam2, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter + iTeamWinCount)
				ELSE
					iRow = (iTeamSlotCounter + iTeamWinCount + 1 + iExtraTeamNameRow + 1)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam3, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter + iTeamWinCount+ iTeam2Count)
				ELSE
					iRow = (iTeamSlotCounter + iTeamWinCount+ iTeam2Count + 2 + iExtraTeamNameRow + 2)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam4, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count)
				ELSE
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count + 3 + iExtraTeamNameRow + 3)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam5, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count)
				ELSE
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count + 4 + iExtraTeamNameRow + 4)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam6, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count)
				ELSE
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count + 5 + iExtraTeamNameRow + 5)
				ENDIF
			ELSE
				IF isDpadLbd
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count)
				ELSE
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + 6 + iExtraTeamNameRow + 6)
				ENDIF
			ENDIF
		BREAK
		
		CASE 6
			IF bWon		
				IF isDpadLbd
					iRow = iTeamSlotCounter 
				ELSE
					iRow = iTeamSlotCounter  + iExtraTeamNameRow
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam2, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter + iTeamWinCount)
				ELSE
					iRow = (iTeamSlotCounter + iTeamWinCount + 1 + iExtraTeamNameRow + 1)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam3, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter + iTeamWinCount+ iTeam2Count)
				ELSE
					iRow = (iTeamSlotCounter + iTeamWinCount+ iTeam2Count + 2 + iExtraTeamNameRow + 2)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam4, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count)
				ELSE
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count + 3 + iExtraTeamNameRow + 3)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam5, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count)
				ELSE
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count + 4 + iExtraTeamNameRow + 4)
				ENDIF
			ELSE
				IF isDpadLbd
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count)
				ELSE
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count + 5 + iExtraTeamNameRow + 5)
				ENDIF
			ENDIF
		BREAK
	
		CASE 5
			IF bWon		
				IF isDpadLbd
					iRow = iTeamSlotCounter 
				ELSE
					iRow = iTeamSlotCounter  + iExtraTeamNameRow
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam2, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter + iTeamWinCount)
				ELSE
					iRow = (iTeamSlotCounter + iTeamWinCount + 1 + iExtraTeamNameRow + 1)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam3, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter + iTeamWinCount+ iTeam2Count)
				ELSE
					iRow = (iTeamSlotCounter + iTeamWinCount+ iTeam2Count + 2 + iExtraTeamNameRow + 2)
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam4, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count)
				ELSE
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count + 3 + iExtraTeamNameRow + 3)
				ENDIF
			ELSE
				IF isDpadLbd
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count)
				ELSE
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count + 4 + iExtraTeamNameRow + 4)
				ENDIF
			ENDIF
		BREAK

		CASE 4
			IF bWon	
				IF isDpadLbd
					iRow = iTeamSlotCounter 
				ELSE
					iRow = iTeamSlotCounter  + iExtraTeamNameRow
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam2, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter + iTeamWinCount)  
				ELSE
					// Start losing team from the bottom of the winning team
					iRow = (iTeamSlotCounter + iTeamWinCount + 1 + iExtraTeamNameRow + 1)  
					//NET_PRINT("4 teams - row for second team: ") NET_PRINT_INT(iRow) NET_NL()
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam3, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter + iTeamWinCount+ iTeam2Count)
				ELSE
					// Start losing team from the bottom of the winning team
					iRow = (iTeamSlotCounter + iTeamWinCount+ iTeam2Count + 2 + iExtraTeamNameRow + 2)
					//NET_PRINT("4 teams - row for third team: ") NET_PRINT_INT(iRow) NET_NL()
				ENDIF
			ELSE
				IF isDpadLbd
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count)
				ELSE
					// Start losing team from the bottom of the winning team
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count+ iTeam3Count + 3 + iExtraTeamNameRow + 3)
					//NET_PRINT("4 teams - row for last team: ") NET_PRINT_INT(iRow) NET_NL()
				ENDIF
			ENDIF
		BREAK
		
		CASE 3
			IF bWon			
				IF isDpadLbd
					iRow = iTeamSlotCounter 
				ELSE
					iRow = iTeamSlotCounter  + iExtraTeamNameRow
				ENDIF
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam2, isDpadLbd)
				IF isDpadLbd
					iRow = (iTeamSlotCounter + iTeamWinCount)
				ELSE
					// Start losing team from the bottom of the winning team
					iRow = (iTeamSlotCounter + iTeamWinCount + 1 + iExtraTeamNameRow + 1)
					//NET_PRINT("3 teams - row for second team: ") NET_PRINT_INT(iRow) NET_NL()
				ENDIF
			ELSE
				IF isDpadLbd
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count)
				ELSE
					// Start losing team from the bottom of the winning team
					iRow = (iTeamSlotCounter+ iTeamWinCount+ iTeam2Count + 2 + iExtraTeamNameRow + 2)
					//NET_PRINT("3 teams - row for last team: ") NET_PRINT_INT(iRow) NET_NL()
				ENDIF
			ENDIF
		BREAK
		
		CASE 2
			IF bWon			
				IF isDpadLbd
					iRow = iTeamSlotCounter 
				ELSE
					iRow = iTeamSlotCounter  + iExtraTeamNameRow
				ENDIF
			ELSE
				IF isDpadLbd
					iRow = (iTeamSlotCounter + iTeamWinCount)
				ELSE
					// Start losing team from the bottom of the winning team
					iRow = (iTeamSlotCounter + iTeamWinCount + 1 + iExtraTeamNameRow + 1)
					//NET_PRINT("2 teams - row for last team: ") NET_PRINT_INT(iRow) NET_NL()
				ENDIF				
			ENDIF
		BREAK
		
		CASE 1
		CASE 0
			IF isDpadLbd
				iRow = iTeamSlotCounter 
			ELSE
				iRow = iTeamSlotCounter  + iExtraTeamNameRow
			ENDIF
		BREAK
		
		DEFAULT
			PRINTLN("iNumTeams = ", iNumTeams)
			SCRIPT_ASSERT("GET_ROW_TO_DRAW_PLAYERS, deathmatch, Invalid team, more need set up?")
		BREAK	
	ENDSWITCH
	
	PRINTLN("[CS_TEAM_LBD] [GET_ROW_TO_DRAW_PLAYERS] iLocalTeam = ", iLocalTeam, " iTeamWin  = ", iTeamWin, " iRow = ", iRow, " iTeamWinCount = ",
				iTeamWinCount, " iLosingTeam = ", iLosingTeam, " iNumTeams = ", iNumTeams, " iTeamSlotCounter = ", iTeamSlotCounter)
	//NET_PRINT("iRow: ") NET_PRINT_INT(iRow) NET_NL()
	
	RETURN iRow	
ENDFUNC

// Mortifying Christmas tree function that does the job
// Gets position of the Team name bars at the top e.g. "1 Team 1"
FUNC INT GET_TEAM_NAME_ROWS(INT iLocalTeam, INT iNumTeams, INT iTeamSlotCounter,
							INT iTeamWinCount, INT iTeam2Count, INT iTeam3Count, INT iTeam4Count, INT iTeam5Count, INT iTeam6Count, INT iTeam7Count,
							INT iTeamWin, INT iTeam2, INT iTeam3, INT iTeam4, INT iTeam5, INT iTeam6, INT iTeam7,
							
							INT iTeam8 = 0, INT iTeam9 = 0, INT iTeam10 = 0, INT iTeam11 = 0, INT iTeam12 = 0, INT iTeam13 = 0, INT iTeam14 = 0,
							INT iTeam8Count = 0, INT iTeam9Count = 0, INT iTeam10Count = 0, INT iTeam11Count = 0, INT iTeam12Count = 0, INT iTeam13Count = 0, INT iTeam14Count = 0)

	UNUSED_PARAMETER(iTeamSlotCounter)
	INT iRow

	INT iTeam2Increment = 2
	INT iTeam3Increment = 4
	INT iTeam4Increment = 6
	INT iTeam5Increment = 8
	INT iTeam6Increment = 10
	INT iTeam7Increment = 12
	INT iTeam8Increment = 14
	
	INT iTeam9Increment  = 16
	INT iTeam10Increment = 18
	INT iTeam11Increment = 20
	INT iTeam12Increment = 22
	INT iTeam13Increment = 24
	INT iTeam14Increment = 26
	INT iTeam15Increment = 28

	SWITCH iNumTeams	
		CASE 15
			IF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeamWin)		
				iRow = 0 
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam2)

				iRow = (iTeamWinCount + iTeam2Increment)
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam3)

				iRow = (iTeamWinCount+ iTeam2Count + iTeam3Increment)
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam4)

				iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count + iTeam4Increment)
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam5)

				iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count + iTeam5Increment)
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam6)

				iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count + iTeam6Increment)
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam7)

				iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Increment)
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam8)

				iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Increment)
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam9)
			
				iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count + iTeam9Increment)
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam10)
			
				iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Increment)
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam11)
			
				iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Count + iTeam11Increment)
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam12)
			
				iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Count + iTeam11Count + iTeam12Increment)
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam13)
			
				iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Count + iTeam11Count + iTeam12Count + iTeam13Increment)
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam14)
			
				iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Count + iTeam11Count + iTeam12Count + iTeam13Count + iTeam14Increment)
			ELSE
			
				iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Count + iTeam11Count + iTeam12Count + iTeam13Count + iTeam14Count + iTeam15Increment)
			ENDIF
		BREAK
		CASE 14
			IF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeamWin)		
				iRow = 0 
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam2)

				iRow = (iTeamWinCount + iTeam2Increment)
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam3)

				iRow = (iTeamWinCount+ iTeam2Count + iTeam3Increment)
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam4)

				iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count + iTeam4Increment)
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam5)

				iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count + iTeam5Increment)
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam6)

				iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count + iTeam6Increment)
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam7)

				iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Increment)
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam8)

				iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Increment)
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam9)
			
				iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count + iTeam9Increment)
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam10)
			
				iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Increment)
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam11)
			
				iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Count + iTeam11Increment)
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam12)
			
				iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Count + iTeam11Count + iTeam12Increment)
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam13)
			
				iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Count + iTeam11Count + iTeam12Count + iTeam13Increment)
			ELSE
			
				iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Count + iTeam11Count + iTeam12Count + iTeam13Count + iTeam14Increment)
			ENDIF
		BREAK
		CASE 13
			IF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeamWin)		
				iRow = 0 
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam2)

				iRow = (iTeamWinCount + iTeam2Increment)
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam3)

				iRow = (iTeamWinCount+ iTeam2Count + iTeam3Increment)
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam4)

				iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count + iTeam4Increment)
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam5)

				iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count + iTeam5Increment)
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam6)

				iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count + iTeam6Increment)
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam7)

				iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Increment)
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam8)

				iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Increment)
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam9)
			
				iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count + iTeam9Increment)
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam10)
			
				iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Increment)
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam11)
			
				iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Count + iTeam11Increment)
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam12)
			
				iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Count + iTeam11Count + iTeam12Increment)
			ELSE
			
				iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Count + iTeam11Count + iTeam12Count + iTeam13Increment)
			ENDIF
		BREAK
		CASE 12
			IF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeamWin)		
				iRow = 0 
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam2)

				iRow = (iTeamWinCount + iTeam2Increment)
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam3)

				iRow = (iTeamWinCount+ iTeam2Count + iTeam3Increment)
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam4)

				iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count + iTeam4Increment)
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam5)

				iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count + iTeam5Increment)
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam6)

				iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count + iTeam6Increment)
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam7)

				iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Increment)
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam8)

				iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Increment)
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam9)
			
				iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count + iTeam9Increment)
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam10)
			
				iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Increment)
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam11)
			
				iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Count + iTeam11Increment)
			ELSE
			
				iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Count + iTeam11Count + iTeam12Increment)
			ENDIF
		BREAK
		CASE 11
			IF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeamWin)		
				iRow = 0 
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam2)

				iRow = (iTeamWinCount + iTeam2Increment)
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam3)

				iRow = (iTeamWinCount+ iTeam2Count + iTeam3Increment)
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam4)

				iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count + iTeam4Increment)
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam5)

				iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count + iTeam5Increment)
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam6)

				iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count + iTeam6Increment)
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam7)

				iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Increment)
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam8)

				iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Increment)
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam9)
			
				iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count + iTeam9Increment)
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam10)
			
				iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Increment)
			ELSE
			
				iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Count + iTeam11Increment)
			ENDIF
		BREAK
		CASE 10
			IF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeamWin)		
				iRow = 0 
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam2)

				iRow = (iTeamWinCount + iTeam2Increment)
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam3)

				iRow = (iTeamWinCount+ iTeam2Count + iTeam3Increment)
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam4)

				iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count + iTeam4Increment)
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam5)

				iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count + iTeam5Increment)
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam6)

				iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count + iTeam6Increment)
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam7)

				iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Increment)
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam8)

				iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Increment)
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam9)
			
				iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count + iTeam9Increment)
			ELSE
			
				iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count + iTeam9Count + iTeam10Increment)
			ENDIF
		BREAK
		CASE 9
			IF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeamWin)		
				iRow = 0 
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam2)

				iRow = (iTeamWinCount + iTeam2Increment)
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam3)

				iRow = (iTeamWinCount+ iTeam2Count + iTeam3Increment)
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam4)

				iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count + iTeam4Increment)
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam5)

				iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count + iTeam5Increment)
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam6)

				iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count + iTeam6Increment)
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam7)

				iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Increment)
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam8)

				iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Increment)
			ELSE
			
				iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Count + iTeam9Increment)
			ENDIF
		BREAK
		CASE 8
			IF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeamWin)		
				iRow = 0 
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam2)

				iRow = (iTeamWinCount + iTeam2Increment)
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam3)

				iRow = (iTeamWinCount+ iTeam2Count + iTeam3Increment)
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam4)

				iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count + iTeam4Increment)
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam5)

				iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count + iTeam5Increment)
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam6)

				iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count + iTeam6Increment)
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam7)

				iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Increment)
			ELSE

				iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count+ iTeam7Count + iTeam8Increment)
			ENDIF
		BREAK
		
		CASE 7
			IF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeamWin)		
				iRow = 0 
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam2)

				iRow = (iTeamWinCount + iTeam2Increment)
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam3)

				iRow = (iTeamWinCount+ iTeam2Count + iTeam3Increment)
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam4)

				iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count + iTeam4Increment)
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam5)

				iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count + iTeam5Increment)
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam6)

				iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count + iTeam6Increment)
			ELSE
				iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count+ iTeam6Count + iTeam7Increment)
			ENDIF
		BREAK
		
		CASE 6
			IF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeamWin)		
				iRow = 0 
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam2)

				iRow = (iTeamWinCount + iTeam2Increment)
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam3)

				iRow = (iTeamWinCount+ iTeam2Count + iTeam3Increment)
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam4)

				iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count + iTeam4Increment)
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam5)

				iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count + iTeam5Increment)
			ELSE

				iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count+ iTeam5Count + iTeam6Increment)
			ENDIF
		BREAK
	
		CASE 5
			IF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeamWin)		
				iRow = 0 
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam2)

				iRow = (iTeamWinCount + iTeam2Increment)
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam3)

				iRow = (iTeamWinCount+ iTeam2Count + iTeam3Increment)
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam4)

				iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count + iTeam4Increment)
			ELSE

				iRow = (iTeamWinCount+ iTeam2Count+ iTeam3Count+ iTeam4Count + iTeam5Increment)
			ENDIF
		BREAK

		CASE 4
			IF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeamWin)	
				iRow = 0 
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam2)

				// Start losing team from the bottom of the winning team
				iRow = (iTeamWinCount + iTeam2Increment)  
//				NET_PRINT("4 teams - row for second team: ") NET_PRINT_INT(iRow) NET_NL()
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam3)

				// Start losing team from the bottom of the winning team
				iRow = (iTeamWinCount + iTeam2Count + iTeam3Increment)
//				NET_PRINT("4 teams - row for third team: ") NET_PRINT_INT(iRow) NET_NL()
			ELSE

				// Start losing team from the bottom of the winning team
				iRow = (iTeamWinCount + iTeam2Count+ iTeam3Count + iTeam4Increment)
//				NET_PRINT("4 teams - row for last team: ") NET_PRINT_INT(iRow) NET_NL()
			ENDIF
		BREAK
		
		CASE 3
			IF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeamWin)			
				iRow = 0 
			ELIF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeam2)

				// Start losing team from the bottom of the winning team
				iRow = (iTeamWinCount + iTeam2Increment)
				//NET_PRINT("3 teams - row for second team: ") NET_PRINT_INT(iRow) NET_NL()
			ELSE

				// Start losing team from the bottom of the winning team
				iRow = (iTeamWinCount+ iTeam2Count + iTeam3Increment)
				//NET_PRINT("3 teams - row for last team: ") NET_PRINT_INT(iRow) NET_NL()
			ENDIF
		BREAK
		
		CASE 2
			IF DOES_TEAM_LIKE_TEAM(iLocalTeam, iTeamWin)			
				iRow = 0 
			ELSE

				// Start losing team from the bottom of the winning team
				iRow = (iTeamWinCount + iTeam2Increment)
				//NET_PRINT("2 teams - row for last team: ") NET_PRINT_INT(iRow) NET_NL()
			ENDIF
		BREAK
		
		CASE 1
		CASE 0
			iRow = 0 
		BREAK
		
		DEFAULT
			PRINTLN("iNumTeams = ", iNumTeams)
			SCRIPT_ASSERT("[GET_TEAM_NAME_ROWS] , deathmatch, Invalid team, more need set up?")
		BREAK	
	ENDSWITCH
	
	PRINTLN("[CS_TEAM_LBD] [GET_TEAM_NAME_ROWS]      iLocalTeam = ", iLocalTeam, " iTeamWin  = ", iTeamWin, " iRow = ", iRow, " iTeamWinCount = ",
				iTeamWinCount, " iNumTeams = ", iNumTeams)
	
	//NET_PRINT("iRow: ") NET_PRINT_INT(iRow) NET_NL()
	
	RETURN iRow	
ENDFUNC

CONST_INT DPAD_REFRESH 		0
CONST_INT DPAD_SETUP 		1
CONST_INT DPAD_RENDERING	2

PROC HIDE_HUD_FOR_LBD()
	HIDE_ALL_CONTEXT_HELP_NEXT_UPDATE()
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_VEHICLE_NAME)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_DISTRICT_NAME)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_STREET_NAME)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_FEED)
ENDPROC

FUNC BOOL LOAD_DPAD_LBD(SCALEFORM_INDEX& siMovie, LBD_SUB_MODE eSubMode, DPAD_VARS& dpadVars, BOOL bImpromptu = FALSE)

	TEXT_LABEL_15 difficultyTextLabel15
	difficultyTextLabel15 = ""
	
	BOOL bIsFreemode = FALSE
	IF USE_FREEMODE_MOVIE(eSubMode)
		bIsFreemode = TRUE
		PRINTLN("[CS_DPAD], LOAD_DPAD_LBD, USE_FREEMODE_MOVIE ")
	ENDIF

	STRING strTitle
	SWITCH dpadVars.iLoadProgress
	
		CASE DPAD_REFRESH
			// Load assets
			IF REQUEST_SCALEFORM_SUMMARY_CARD(bIsFreemode, siMovie)
				dpadVars.iLoadProgress = DPAD_SETUP
			ENDIF
		BREAK
		
		CASE DPAD_SETUP
			IF CAN_DISPLAY_DPADDOWN_PARTS()
			
				strTitle = GET_LEADERBOARD_TITLE(eSubMode, g_FMMC_STRUCT.tl63MissionName, bImpromptu)

				difficultyTextLabel15 = GET_MISSION_DIFFICULTY(eSubMode)
				SET_SCALEFORM_SUMMARY_CARD_TITLE(siMovie, strTitle, difficultyTextLabel15, TRUE)
				
				dpadVars.iLoadProgress = DPAD_RENDERING
			ENDIF
		BREAK
		
		// Draw
		CASE DPAD_RENDERING
		
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

// (1244092) no gaps between bars
/*
PROC NO_GAPS_BETWEEN_DPAD_BARS(SCALEFORM_INDEX& siMovie)
	IF HAS_SCALEFORM_MOVIE_LOADED(siMovie)
		BEGIN_SCALEFORM_MOVIE_METHOD(siMovie, "SET_ROW_SPACING")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC
*/

// 25px title + 2 px title gap + (16 players * 25px per height row) + (15 player gaps * 2 px) = 457px
// 1px on y-axis = 0.00138888
// 25 * 0.00138888


CONST_FLOAT DPAD_ROW_HEIGHT 0.034722 

FUNC BOOL SAFE_TO_DRAW_DPAD_LBD()

	// Z menu
	#IF IS_DEBUG_BUILD
	
	BOOL bSpamDebug = GET_COMMANDLINE_PARAM_EXISTS("sc_DpadSpam")
	
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_DisableDpad")
	
		IF bSpamDebug
		PRINTLN("[CS_DPAD]  SAFE_TO_DRAW_DPAD_LBD, sc_DisableDpad ")
		ENDIF
	
		RETURN FALSE
	ENDIF
	
	IF g_TransitionSessionNonResetVars.MenuVars.bMenuActive
	
		IF bSpamDebug
		PRINTLN("[CS_DPAD]  SAFE_TO_DRAW_DPAD_LBD, bMenuActive ")
		ENDIF
	
		RETURN FALSE
	ENDIF
	
	IF g_debugMenuControl.bDebugMenuOnScreen
	
		IF bSpamDebug
		PRINTLN("[CS_DPAD]  SAFE_TO_DRAW_DPAD_LBD, bDebugMenuOnScreen ")
		ENDIF
	
		RETURN FALSE
	ENDIF
	
	#ENDIF //#IF IS_DEBUG_BUILD
	
	IF IS_CUSTOM_MENU_ON_SCREEN()
	
		#IF IS_DEBUG_BUILD
		IF bSpamDebug
		PRINTLN("[CS_DPAD]  SAFE_TO_DRAW_DPAD_LBD, IS_CUSTOM_MENU_ON_SCREEN ")
		ENDIF
		#ENDIF
	
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_ON_IMPROMPTU_DM()
	
		#IF IS_DEBUG_BUILD
		IF bSpamDebug
		PRINTLN("[CS_DPAD]  SAFE_TO_DRAW_DPAD_LBD, IS_PLAYER_ON_IMPROMPTU_DM ")
		ENDIF
		#ENDIF
	
		RETURN FALSE
	ENDIF

	IF NOT IS_SAFE_TO_DRAW_ON_SCREEN()
	
		#IF IS_DEBUG_BUILD
		IF bSpamDebug
		PRINTLN("[CS_DPAD]  SAFE_TO_DRAW_DPAD_LBD, IS_SAFE_TO_DRAW_ON_SCREEN ")
		ENDIF
		#ENDIF
	
		RETURN FALSE
	ENDIF
	
	IF NOT IS_SKYSWOOP_AT_GROUND()
	
		#IF IS_DEBUG_BUILD
		IF bSpamDebug
		PRINTLN("[CS_DPAD]  SAFE_TO_DRAW_DPAD_LBD, IS_SKYSWOOP_AT_GROUND ")
		ENDIF
		#ENDIF
	
		RETURN FALSE
	ENDIF
	
	IF IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
	
		#IF IS_DEBUG_BUILD
		IF bSpamDebug
		PRINTLN("[CS_DPAD]  SAFE_TO_DRAW_DPAD_LBD, IS_SCRIPT_HUD_DISPLAYING ")
		ENDIF
		#ENDIF

		RETURN FALSE
	ENDIF
	
	IF GET_DPADDOWN_ACTIVATION_STATE() = DPADDOWN_SECOND
	
		#IF IS_DEBUG_BUILD
		IF bSpamDebug
		PRINTLN("[CS_DPAD]  SAFE_TO_DRAW_DPAD_LBD, DPADDOWN_SECOND ")
		ENDIF
		#ENDIF
	
		RETURN FALSE
	ENDIF
	
	IF MPGlobalsAmbience.bUsingTelescope
	
		#IF IS_DEBUG_BUILD
		IF bSpamDebug
		PRINTLN("[CS_DPAD]  SAFE_TO_DRAW_DPAD_LBD, bUsingTelescope ")
		ENDIF
		#ENDIF
	
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_IN_CORONA()
	
		#IF IS_DEBUG_BUILD
		IF bSpamDebug
		PRINTLN("[CS_DPAD]  IS_PLAYER_IN_CORONA ")
		ENDIF
		#ENDIF
	
		RETURN FALSE

	ELIF NOT IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID()) AND (g_sAtCoordsBandsMP[MATCB_FOCUS_MISSION].matcbNumInBand > 0) // Is_There_A_MissionsAtCoords_Focus_Mission()
		
		#IF IS_DEBUG_BUILD
		IF bSpamDebug
		PRINTLN("[CS_DPAD]  IS_PLAYER_ON_ANY_FM_MISSION ")
		ENDIF
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	IF IS_SELECTOR_ONSCREEN()
	OR IS_SELECTOR_UI_BUTTON_PRESSED()
	OR g_sSelectorUI.bDisplay
	OR g_sSelectorUI.bLoaded
		
		#IF IS_DEBUG_BUILD
		IF bSpamDebug
		PRINTLN("[CS_DPAD]  SAFE_TO_DRAW_DPAD_LBD, IS_SELECTOR_ONSCREEN ")
		ENDIF
		#ENDIF
	
		RETURN FALSE
	ENDIF
	
	IF IS_PAUSE_MENU_ACTIVE()
		
		#IF IS_DEBUG_BUILD
		IF bSpamDebug
		PRINTLN("[CS_DPAD]  SAFE_TO_DRAW_DPAD_LBD, IS_PAUSE_MENU_ACTIVE ")
		ENDIF
		#ENDIF
	
		RETURN FALSE
	ENDIF
	
	IF IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE()
	AND g_eSpecialSpectatorState = SSS_INIT
		PRINTLN("[CS_DPAD]  SAFE_TO_DRAW_DPAD_LBD, FALSE - INIT ROAMING SPECTATOR ")
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_SPECTATING(PLAYER_ID())
	AND NOT IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE()
		#IF IS_DEBUG_BUILD
		IF bSpamDebug
		PRINTLN("[CS_DPAD]  SAFE_TO_DRAW_DPAD_LBD, IS_PLAYER_SPECTATING ")
		ENDIF
		#ENDIF
	
		RETURN FALSE
	ENDIF
	
	IF g_bCelebrationScreenIsActive
	
		#IF IS_DEBUG_BUILD
		IF bSpamDebug
		PRINTLN("[CS_DPAD]  SAFE_TO_DRAW_DPAD_LBD, g_bCelebrationScreenIsActive ")
		ENDIF
		#ENDIF
	
		RETURN FALSE
	ENDIF

	IF g_bApartmentDanceIsActive
	
		#IF IS_DEBUG_BUILD
		IF bSpamDebug
		PRINTLN("[CS_DPAD]  SAFE_TO_DRAW_DPAD_LBD, g_bApartmentDanceIsActive ")
		ENDIF
		#ENDIF
	
		RETURN FALSE
	ENDIF
	
	IF GB_IS_GLOBAL_NON_BD_BIT0_SET(eGB_GLOBAL_NON_BD_BITSET_0_BLOCK_DPAD_DOWN_LB)
	
		#IF IS_DEBUG_BUILD
		IF bSpamDebug
		PRINTLN("[CS_DPAD]  SAFE_TO_DRAW_DPAD_LBD, eGB_GLOBAL_NON_BD_BITSET_0_BLOCK_DPAD_DOWN_LB ")
		ENDIF
		#ENDIF
		
		RETURN FALSE
	ENDIF

	IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_SPAWNING_IN_A_PROPERTY)
		
		#IF IS_DEBUG_BUILD
		IF bSpamDebug
		PRINTLN("[CS_DPAD]  SAFE_TO_DRAW_DPAD_LBD, PROPERTY_BROADCAST_BS_SPAWNING_IN_A_PROPERTY ")
		ENDIF
		#ENDIF
	
		RETURN FALSE
	ENDIF
	
	IF g_bUsingShootingRange
		#IF IS_DEBUG_BUILD
		IF bSpamDebug
		PRINTLN("[CS_DPAD] SAFE_TO_DRAW_DPAD_LBD, g_bUsingShootingRange")		
		ENDIF
		#ENDIF
	
		RETURN FALSE
	ENDIF
	
	IF g_GangOpsPlanning.sStateData.bInteract
	OR g_GangOpsFinale.sStateData.bInteract
	OR g_GangOpsIdle.sStateData.bInteract
		#IF IS_DEBUG_BUILD
		IF bSpamDebug
		PRINTLN("[CS_DPAD] SAFE_TO_DRAW_DPAD_LBD, g_GangOpsPlanning.sStateData.bInteract OR g_GangOpsFinale.sStateData.bInteract OR g_GangOpsIdle.sStateData.bInteract")		
		ENDIF
		#ENDIF
	
		RETURN FALSE
	ENDIF
	
	#IF FEATURE_CASINO_HEIST
	IF g_sCasinoHeistData.sSetupData.sStateData.bInteract
	OR g_sCasinoHeistData.sPrepData.sStateData.bInteract
	OR g_sCasinoHeistData.sFinaleData.sStateData.bInteract
		#IF IS_DEBUG_BUILD
		IF bSpamDebug
		PRINTLN("[CS_DPAD] SAFE_TO_DRAW_DPAD_LBD, Casino heist planning board being used")		
		ENDIF
		#ENDIF
	
		RETURN FALSE
	ENDIF
	#ENDIF
	
	IF g_sNetHeistPlanningGenericClientData.sStateData.bInteract
		#IF IS_DEBUG_BUILD
		IF bSpamDebug
		PRINTLN("[CS_DPAD] SAFE_TO_DRAW_DPAD_LBD, Net heist planning generic board/screen/table being used")		
		ENDIF
		#ENDIF
	
		RETURN FALSE
	ENDIF

	IF IS_PLAYER_USING_CASINO_LUCKY_WHEEL(PLAYER_ID())
	
		#IF IS_DEBUG_BUILD
		IF bSpamDebug
		PRINTLN("[CS_DPAD]  SAFE_TO_DRAW_DPAD_LBD, IS_PLAYER_USING_CASINO_LUCKY_WHEEL ")
		ENDIF
		#ENDIF
	
		RETURN FALSE
	ENDIF	
	
	IF g_bIsPlayerPlayingDegenatron
	OR g_bIsPlayerPlayingWizardsRuin
	OR g_bIsPlayerPlayingArcadeGame
	
		#IF IS_DEBUG_BUILD
		IF bSpamDebug
		PRINTLN("[CS_DPAD]  SAFE_TO_DRAW_DPAD_LBD, arcade game active ")
		ENDIF
		#ENDIF
	
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC CLEANUP_DPAD(SCALEFORM_INDEX& siMovie, DPAD_VARS& dpadVars, BOOL bResetTimer)
	CLEAR_BIT(dpadVars.iDpadBitSet, ciDPAD_IS_UPDATED)
	g_i_NumDpadLbdPlayers = 0
	RESET_DPAD_REFRESH()
	g_Show_Lap_Dpad = FALSE
	dpadVars.iDrawProgress = DPAD_INIT
	IF bResetTimer
		IF HAS_NET_TIMER_STARTED(dpadVars.timerDpad)
			RESET_NET_TIMER(dpadVars.timerDpad)	
			PRINTLN("[CS_DPAD] CLEANUP_DPAD, RESET_NET_TIMER")
			CLEAR_BIT(MPGlobalsAmbience.iNGAmbBitSet, iNGABI_DPAD_DOWN_LBD_DISPLAYING)
		ENDIF
	ENDIF
	IF HAS_SCALEFORM_MOVIE_LOADED(siMovie)
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(siMovie)
	ENDIF
	IF IS_BIT_SET(dpadVars.iDpadBitSet, ciDPAD_INITIALISE)
		CLEAR_BIT(dpadVars.iDpadBitSet, ciDPAD_INITIALISE)
	ENDIF
	THEFEED_SET_SCRIPTED_MENU_HEIGHT(0.0)	
ENDPROC

PROC CLEANUP_DPAD_LBD(SCALEFORM_INDEX& siMovie)
	IF HAS_SCALEFORM_MOVIE_LOADED(siMovie)
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(siMovie)
	ENDIF
ENDPROC

FUNC INT GET_JOB_ICON_DPAD(LBD_SUB_MODE eSubMode)

	INT iReturn = -1
	
	IF IS_LOADED_MISSION_LOW_RIDER_FLOW_MISSION()
	
		iReturn = 2
	ENDIF

	SWITCH eSubMode
		CASE SUB_KOTH
			iReturn = 22
		BREAK
		CASE SUB_DM_FFA
		CASE SUB_ARENA_CARNAGE	
		CASE SUB_ARENA_ARCADE
		CASE SUB_ARENA_CTF
		CASE SUB_ARENA_DDERBY
		CASE SUB_ARENA_DETONATION
		CASE SUB_ARENA_FIRE
		CASE SUB_ARENA_GAMESMASTER
		CASE SUB_ARENA_MONSTER
		CASE SUB_ARENA_PASSBOMB
		CASE SUB_ARENA_TAG_TEAM
		CASE SUB_KART	
		CASE SUB_DM_UPDATE
			iReturn = 3
		BREAK
		CASE SUB_DM_VEH
			iReturn = 3
		BREAK
		CASE SUB_DM_TEAM
			iReturn = 6
		BREAK
		CASE SUB_HORDE
			iReturn = 5
		BREAK
		CASE SUB_RACE_STUNT
			iReturn = 21
		BREAK
		
		CASE SUB_RACE_NORMAL
		CASE SUB_RACE_GTA
		CASE SUB_RACE_RALLY
		CASE SUB_RACE_FOOT
		CASE SUB_RACE_AGG_BEST
		CASE SUB_RACE_AGG_TIME
		CASE SUB_RACE_TARGET
			iReturn = 4
			
			// 14 cycle
			// 15 sea
			// 16 air
		BREAK
		CASE SUB_RACE_BASEJUMP
			iReturn = 10
		BREAK
		CASE SUB_MISSIONS_NORMAL
		CASE SUB_MISSIONS_TIMED
		CASE SUB_MISSIONS_NO_HUD
		CASE SUB_MISSIONS_VERSUS
			iReturn = 2
		BREAK
		CASE SUB_MISSIONS_LTS
			iReturn = 17
		BREAK
		CASE SUB_MISSIONS_CTF
			iReturn = 18
		BREAK
		
		CASE SUB_MISSIONS_HEIST 
		
			IF Is_Player_Currently_On_MP_Heist(PLAYER_ID()) 
			
				iReturn = 20
			ENDIF
			
			IF Is_Player_Currently_On_MP_Heist_Planning(PLAYER_ID())
			
				iReturn = 19
			ENDIF
		BREAK
	ENDSWITCH
	
	IF Is_Player_Currently_On_MP_Versus_Mission(PLAYER_ID())
	
		iReturn = 2
	ENDIF
	
	IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_FLOW() 
		iReturn = 20
	ENDIF
	
	PRINTLN("GET_JOB_ICON_DPAD, iReturn = ", iReturn)
	
	RETURN iReturn
ENDFUNC


#IF IS_DEBUG_BUILD
FUNC STRING sDEBUG_PRINT_DEBUG_LBD_TYPE(eDPAD_TYPE eDpadType)

	STRING sReturn

	SWITCH eDpadType
		CASE eDPAD_TYPE_FM 
			sReturn = "eDPAD_TYPE_FM"
		BREAK
		CASE eDPAD_TYPE_CHALLENGE
			sReturn = "eDPAD_TYPE_CHALLENGE"
		BREAK
		CASE eDPAD_TYPE_NOT_USED
		sReturn = "eDPAD_TYPE_NOT_USED"
		BREAK
	ENDSWITCH

	RETURN sReturn
ENDFUNC
#ENDIF

PROC SET_DPAD_DRAW_TYPE(eDPAD_TYPE eType)

	#IF IS_DEBUG_BUILD
	STRING sDebugName
	#ENDIF

	IF g_sDpadTypeToDraw.eDpadTypeToDrawLastFrame <> g_sDpadTypeToDraw.eDpadTypeToDraw
		g_sDpadTypeToDraw.eDpadTypeToDrawLastFrame = g_sDpadTypeToDraw.eDpadTypeToDraw
		
		#IF IS_DEBUG_BUILD
		sDebugName = sDEBUG_PRINT_DEBUG_LBD_TYPE(g_sDpadTypeToDraw.eDpadTypeToDrawLastFrame)
		PRINTLN("3534137, sDEBUG_PRINT_DEBUG_LBD_TYPE, g_sDpadTypeToDraw.eDpadTypeToDrawLastFrame = ", sDebugName)
		#ENDIF
	ENDIF
	IF g_sDpadTypeToDraw.eDpadTypeToDraw <> eType
		g_sDpadTypeToDraw.eDpadTypeToDraw = eType
		
		#IF IS_DEBUG_BUILD
		sDebugName = sDEBUG_PRINT_DEBUG_LBD_TYPE(g_sDpadTypeToDraw.eDpadTypeToDraw)
		PRINTLN("3534137, sDEBUG_PRINT_DEBUG_LBD_TYPE, g_sDpadTypeToDraw.eDpadTypeToDraw = ", sDebugName)
		#ENDIF
	ENDIF
ENDPROC
  
 // #IF FEATURE_NEW_AMBIENT_EVENTS

FUNC FLOAT GET_DPAD_FEED_HEIGHT()

	FLOAT fReturn
	FLOAT fSafeZone
	FLOAT fMaxDpadHeight
	
	fMaxDpadHeight = 0.63471816
	
	fSafeZone = 1.0 - ( 1.0 - GET_SAFE_ZONE_SIZE() )

	fReturn = 1.0 - ( fSafeZone - fMaxDpadHeight )
	
//	PRINTLN("GET_DPAD_FEED_HEIGHT, fSafeZone = ", fSafeZone)
//	PRINTLN("GET_DPAD_FEED_HEIGHT, GET_SAFE_ZONE_SIZE() = ", GET_SAFE_ZONE_SIZE())
//	PRINTLN("GET_DPAD_FEED_HEIGHT = ", fReturn)
	
	RETURN fReturn
ENDFUNC

FUNC INT GET_NUM_PLAYERS_FOR_DPAD_LEADERBOARD()

	#IF FEATURE_GEN9_EXCLUSIVE
	IF IS_PLAYER_ON_MP_INTRO()
		RETURN -1
	ENDIF
	#ENDIF

	RETURN g_i_NumPlayersForLbd
ENDFUNC

// Check for dpad down button press to draw the dpad leaderboard and also handle the timeout so it vanishes
FUNC BOOL SHOULD_DPAD_LBD_DISPLAY(SCALEFORM_INDEX& siMovie, LBD_SUB_MODE eSubMode, DPAD_VARS& dpadVars, INT iNumLBDPlayers, BOOL bImpromptu = FALSE, BOOL bDevSpectator = FALSE, STRING sCustomTitle = NULL)

	TEXT_LABEL_15 difficultyTextLabel15
	difficultyTextLabel15 = ""
	STRING strTitle
	BOOL bIsFreemode = (eSubMode = SUB_FREEMODE)
	#IF FEATURE_COPS_N_CROOKS
	BOOL bIsArcade = (eSubMode = SUB_ARCADE_CNC)
	#ENDIF //FEATURE_COPS_N_CROOKS
	BOOL bUseFreemodeMovie = USE_FREEMODE_MOVIE(eSubMode)
		
	FLOAT fFeedHeight
	fFeedHeight = GET_DPAD_FEED_HEIGHT()
	
	INT iJobIcon = -1
	
	IF eSubMode = SUB_CHALLENGE
	OR eSubMode = SUB_BOSS
		IF DOES_DPAD_LBD_NEED_SECOND_PAGE() 
			IF GET_DPADDOWN_ACTIVATION_STATE() > DPADDOWN_NONE
			AND g_dPadSecondPage
				
				PRINTLN("2469525, GET_DPADDOWN_ACTIVATION_STATE() = DPADDOWN_NONE, THEFEED_HIDE_THIS_FRAME ")
				THEFEED_HIDE_THIS_FRAME()
				THEFEED_SET_SCRIPTED_MENU_HEIGHT(fFeedHeight)
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_FEED)
				IF IS_HELP_MESSAGE_BEING_DISPLAYED()
					HIDE_HELP_TEXT_THIS_FRAME()
				ENDIF
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_HELP_TEXT)
			ENDIF
		ENDIF
	ENDIF

	IF NOT bDevSpectator
		IF NOT SAFE_TO_DRAW_DPAD_LBD()
		
			CLEANUP_DPAD(siMovie, dpadVars, TRUE)			
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(MPGlobalsAmbience.iAmbBitSet, iABI_TRIP_SKIP_IN_PROGRESS)	//IS_TRIP_SKIP_IN_PROGRESS_GLOBAL()
		CLEANUP_DPAD(siMovie, dpadVars, TRUE)
		RETURN FALSE
	ENDIF
	
	// Push dpad down
	IF NOT HAS_NET_TIMER_STARTED(dpadVars.timerDpad)	
		IF GET_DPADDOWN_ACTIVATION_STATE() = DPADDOWN_FIRST
			REQUEST_SCALEFORM_SUMMARY_CARD(bUseFreemodeMovie, siMovie)
			// Set the 10 second viewing timer
			START_NET_TIMER(dpadVars.timerDpad)
			CLEANUP_DPAD(siMovie, dpadVars, FALSE)
			SET_BIT(MPGlobalsAmbience.iNGAmbBitSet, iNGABI_DPAD_DOWN_LBD_DISPLAYING)
		ENDIF
	ENDIF
		
	IF HAS_NET_TIMER_STARTED(dpadVars.timerDpad)	
	OR bDevSpectator
	
		// Clear all help
		IF IS_HELP_MESSAGE_BEING_DISPLAYED()
			HIDE_HELP_TEXT_THIS_FRAME()
		ENDIF
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_HELP_TEXT)
	
		// Quit when time expires
		IF HAS_NET_TIMER_EXPIRED(dpadVars.timerDpad, DPAD_DOWN_TIMER)
		OR GET_DPADDOWN_ACTIVATION_STATE() = DPADDOWN_NONE
		AND NOT bDevSpectator
			
			// Reset timer when 10 secs up or dpad pressed again
			CLEANUP_DPAD(siMovie, dpadVars, TRUE)
			RETURN FALSE
		ELSE
			// Draw
			IF bIsFreemode = FALSE 
			#IF FEATURE_COPS_N_CROOKS
			AND bIsArcade = FALSE
			#ENDIF //#IF FEATURE_COPS_N_CROOKS
				HIDE_ALL_CONTEXT_HELP_NEXT_UPDATE()
				IF eSubMode = SUB_CHALLENGE
				OR eSubMode = SUB_BOSS
					PRINTLN("2469525, THEFEED_HIDE_THIS_FRAME, A ")
					THEFEED_HIDE_THIS_FRAME()
				ENDIF
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_FEED)
			ENDIF
		
			IF NOT IS_BIT_SET(dpadVars.iDpadBitSet, ciDPAD_INITIALISE)
				// 1973599
				IF bIsFreemode = FALSE 
				#IF FEATURE_COPS_N_CROOKS
				AND bIsArcade = FALSE
				#ENDIF //#IF FEATURE_COPS_N_CROOKS
					HIDE_ALL_CONTEXT_HELP_NEXT_UPDATE()
					IF eSubMode = SUB_CHALLENGE
					OR eSubMode = SUB_BOSS
						PRINTLN("2469525, THEFEED_HIDE_THIS_FRAME, B ")
						THEFEED_HIDE_THIS_FRAME()
					ENDIF
					HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_FEED)
				ENDIF
				THEFEED_SET_SCRIPTED_MENU_HEIGHT(fFeedHeight)	
				PRINTLN("2469525, THEFEED_SET_SCRIPTED_MENU_HEIGHT, fFeedHeight = ", fFeedHeight, " GET_SAFE_ZONE_SIZE() = ", GET_SAFE_ZONE_SIZE())
			
				IF REQUEST_SCALEFORM_SUMMARY_CARD(bUseFreemodeMovie, siMovie) //  we can't spam this anymore see 1391422
					SCALEFORM_SET_DATA_SLOT_EMPTY(siMovie)
				
					strTitle = GET_LEADERBOARD_TITLE(eSubMode, g_FMMC_STRUCT.tl63MissionName, bImpromptu)
					
					difficultyTextLabel15 = GET_MISSION_DIFFICULTY(eSubMode)
					
					// Dpad lbd title
					IF bImpromptu
						SET_SCALEFORM_SUMMARY_CARD_TITLE(siMovie, strTitle, "", FALSE, DEFAULT)
					
					ELIF eSubMode = SUB_CHALLENGE
						SET_CHALLENGE_SUMMARY_CARD_TITLE(siMovie, GET_CHALLENGE_TITLE(dpadVars), GET_CHALLENGE_SORT_VAR(dpadVars))
					
					ELIF eSubMode = SUB_BOSS
						PRINTLN("[SHOULD_DPAD_LBD_DISPLAY] SUB_BOSS ")
						
						STRING sTtitle = GET_BOSS_DPAD_TITLE(dpadVars)
						IF NOT IS_STRING_NULL_OR_EMPTY(sCustomTitle)
							sTtitle = sCustomTitle
							PRINTLN("[SHOULD_DPAD_LBD_DISPLAY] SUB_BOSS,sCustomTitle = ", sCustomTitle)
						ENDIF
						SET_BOSS_SUMMARY_CARD_TITLE(siMovie, sTtitle, GET_BOSS_SORT_VAR())
						
					ELIF bSVM_MISSION()
					
						PRINTLN("[SHOULD_DPAD_LBD_DISPLAY] SVM_FLOW_IS_CURRENT_MISSION_SVM_FLOW, iNumLBDPlayers = ", iNumLBDPlayers, " difficultyTextLabel15 = ", difficultyTextLabel15)
						dpadVars.iStoredPlayerCount = g_i_NumPlayersForLbd
						SET_SCALEFORM_SUMMARY_CARD_TITLE(siMovie, strTitle, difficultyTextLabel15, TRUE, DEFAULT, g_i_NumPlayersForLbd)
					
					ELIF bIsFreemode 
					#IF FEATURE_COPS_N_CROOKS
					OR bIsArcade
					#ENDIF //#IF FEATURE_COPS_N_CROOKS
						iJobIcon = GET_JOB_ICON_DPAD(eSubMode)
						PRINTLN("[SHOULD_DPAD_LBD_DISPLAY] bIsFreemode or bIsArcade, iNumLBDPlayers = ", iNumLBDPlayers, " iJobIcon = ", iJobIcon, " strTitle = ", strTitle)
						dpadVars.iStoredPlayerCount = g_i_NumPlayersForLbd
						SET_SCALEFORM_SUMMARY_CARD_TITLE(siMovie, strTitle, "", FALSE, iJobIcon, GET_NUM_PLAYERS_FOR_DPAD_LEADERBOARD())
					ELSE
						iJobIcon = GET_JOB_ICON_DPAD(eSubMode)
						SET_SCALEFORM_SUMMARY_CARD_TITLE(siMovie, strTitle, difficultyTextLabel15, TRUE, iJobIcon)
					ENDIF
					SET_BIT(dpadVars.iDpadBitSet, ciDPAD_INITIALISE)
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(dpadVars.iDpadBitSet, ciDPAD_INITIALISE)
				g_i_NumDpadLbdPlayers = iNumLBDPlayers
				g_Show_Lap_Dpad = TRUE
				THEFEED_SET_SCRIPTED_MENU_HEIGHT(fFeedHeight)	
				PRINTLN("2469525, THEFEED_SET_SCRIPTED_MENU_HEIGHT, fFeedHeight = ", fFeedHeight, " GET_SAFE_ZONE_SIZE() = ", GET_SAFE_ZONE_SIZE())
				
				// I need to clear this to update the Freemode player count because we only call (REQUEST_SCALEFORM_SUMMARY_CARD) once usually
				IF bIsFreemode 
				#IF FEATURE_COPS_N_CROOKS
				OR bIsArcade
				#ENDIF //#IF FEATURE_COPS_N_CROOKS
					IF dpadVars.iStoredPlayerCount <> g_i_NumPlayersForLbd
						CLEAR_BIT(dpadVars.iDpadBitSet, ciDPAD_INITIALISE)
						PRINTLN("[SHOULD_DPAD_LBD_DISPLAY] CLEAR_BIT(dpadVars.iDpadBitSet, ciDPAD_INITIALISE) ")
					ENDIF
				ENDIF
				RETURN TRUE
			ENDIF
			
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Clears all players from the global leaderboard struct
PROC GB_RESET_GLOBAL_LEADERBOARD_STRUCT()
	
	INT i
	REPEAT NUM_NETWORK_PLAYERS i
		g_GBLeaderboardStruct.challengeLbdStruct[i].playerID = INVALID_PLAYER_INDEX()
		g_GBLeaderboardStruct.challengeLbdStruct[i].iScore = 0
	ENDREPEAT
	
	PRINTLN("[CS_DPAD] RESET_GLOBAL_LEADERBOARD_STRUCT called...")
	DEBUG_PRINTCALLSTACK()
ENDPROC

// ********** GENERIC SORTING FUNCTION FOR STANDARD PLAYER EVENTS ************
// NOTE: Use this for FM modes where you need to sort players by a basic score (type INT)
// 		in ascending or descending order (enum).
//		It adheres to the rules of not surpassing a player above you until you beat their score


/// PURPOSE: Type of sort that is required
ENUM AMBIENT_ACTIVITY_LEADERBOARD_SORT_TYPE
	AAL_SORT_DESCENDING = 0,						// Will sort players in descending score: 10, 9, 4, 2, 1...
	AAL_SORT_ASCENDING,								// Will sort players in ascending scores: 1, 4, 6, 7, 8, 10...
	AAL_SORT_DESCENDING_REDUCING,					// Will sort players in descending scores which only reduce: 10, 9, 4, 1 (where no score will exceed 10)
	AAL_SORT_NON_UNIFORM_SCORING,					// Will sort the players regardless if scores goes up and down (evaluates then uses an above sort algorithm)
	AAL_SORT_NON_UNIFORM_SCORING_ASCENDING,
	AAL_SORT_ASCENDING_REDUCING
ENDENUM

/// PURPOSE: Struct that should be declared in the modes script
STRUCT AMBIENT_ACTIVITY_LEADERBOARD_STRUCT
	PLAYER_INDEX playerID
	INT iScore
ENDSTRUCT

#IF IS_DEBUG_BUILD
FUNC STRING GET_AMBIENT_ACTIVITY_LEADERBOARD_SORT_TYPE_NAME(AMBIENT_ACTIVITY_LEADERBOARD_SORT_TYPE eSortType)
	SWITCH eSortType
		CASE AAL_SORT_DESCENDING					RETURN "AAL_SORT_DESCENDING"
		CASE AAL_SORT_ASCENDING						RETURN "AAL_SORT_ASCENDING"
		CASE AAL_SORT_DESCENDING_REDUCING			RETURN "AAL_SORT_DESCENDING_REDUCING"
		CASE AAL_SORT_NON_UNIFORM_SCORING			RETURN "AAL_SORT_NON_UNIFORM_SCORING"
		CASE AAL_SORT_NON_UNIFORM_SCORING_ASCENDING	RETURN "AAL_SORT_NON_UNIFORM_SCORING_ASCENDING"
		CASE AAL_SORT_ASCENDING_REDUCING			RETURN "AAL_SORT_ASCENDING_REDUCING"
	ENDSWITCH
	
	RETURN "UNKNOWN"
ENDFUNC
#ENDIF

FUNC INT GET_AMBIENT_ACTIVITY_LEADERBOARD_DEFAULT_VALUE(AMBIENT_ACTIVITY_LEADERBOARD_SORT_TYPE eSortType)

	SWITCH eSortType
		CASE AAL_SORT_DESCENDING						RETURN -1
		CASE AAL_SORT_ASCENDING							RETURN HIGHEST_INT
		CASE AAL_SORT_DESCENDING_REDUCING				RETURN -1
		CASE AAL_SORT_NON_UNIFORM_SCORING				RETURN -1
		CASE AAL_SORT_NON_UNIFORM_SCORING_ASCENDING		RETURN HIGHEST_INT
		CASE AAL_SORT_ASCENDING_REDUCING				RETURN HIGHEST_INT
	ENDSWITCH
	
	CPRINTLN(DEBUG_NET_AMBIENT, "**[ALBD] GET_AMBIENT_ACTIVITY_LEADERBOARD_DEFAULT_VALUE - Sort Type: ", ENUM_TO_INT(eSortType))
	SCRIPT_ASSERT("[AAL] Unknown sort type for Ambient Activity Leaderboard sorting function. See logs for details...")
	RETURN 0
ENDFUNC

/// PURPOSE:
///    Call this function for your leaderboard struct before any scores have occurred.
///    Initialises the player ID's to INVALID_PLAYER_INDEX()
PROC INITIALISE_AMBIENT_ACTIVITY_LEADERBOARD(AMBIENT_ACTIVITY_LEADERBOARD_STRUCT &leaderboardArray[], AMBIENT_ACTIVITY_LEADERBOARD_SORT_TYPE eSortType = AAL_SORT_DESCENDING)
	
	INT iDefaultValue = GET_AMBIENT_ACTIVITY_LEADERBOARD_DEFAULT_VALUE(eSortType)
	INT iLoop
	INT iMaxLoop = COUNT_OF(leaderboardArray)
	
	CPRINTLN(DEBUG_NET_AMBIENT, "**[ALBD] INITIALISE_AMBIENT_ACTIVITY_LEADERBOARD - Sort Type: ", GET_AMBIENT_ACTIVITY_LEADERBOARD_SORT_TYPE_NAME(eSortType), ", iMaxLoop = ", iMaxLoop, ", iDefaultValue = ", iDefaultValue)
	
	REPEAT iMaxLoop iLoop
		
		leaderboardArray[iLoop].playerID = INVALID_PLAYER_INDEX()
		leaderboardArray[iLoop].iScore = iDefaultValue		
	
	ENDREPEAT	
ENDPROC

/// PURPOSE:
///    Inserts new player to our activity leaderboard
PROC INSERT_PLAYER_TO_AMBIENT_ACTIVITY_LEADERBOARD(AMBIENT_ACTIVITY_LEADERBOARD_STRUCT &leaderboardArray[], PLAYER_INDEX playerID, INT iPlayerScore)
	INT i
	INT iMaxLoop = COUNT_OF(leaderboardArray)
	REPEAT iMaxLoop i
		IF leaderboardArray[i].playerID = INVALID_PLAYER_INDEX()
		
			CPRINTLN(DEBUG_NET_AMBIENT, "**[ALBD] INSERT_PLAYER_TO_AMBIENT_ACTIVITY_LEADERBOARD - Adding: [", GET_PLAYER_NAME(playerID), "] to index: ", i)
		
			leaderboardArray[i].playerID = playerID
			leaderboardArray[i].iScore = iPlayerScore
			EXIT
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Removes an invalid player from our leaderboard
PROC REMOVE_PLAYER_FROM_AMBIENT_ACTIVITY_LEADERBOARD_INDEX(AMBIENT_ACTIVITY_LEADERBOARD_STRUCT &leaderboardArray[], INT iIndex, AMBIENT_ACTIVITY_LEADERBOARD_SORT_TYPE eSortType = AAL_SORT_DESCENDING)
	
	CPRINTLN(DEBUG_NET_AMBIENT, "**[ALBD] REMOVE_PLAYER_FROM_AMBIENT_ACTIVITY_LEADERBOARD_INDEX - Removing player from index: ", iIndex)
	
	INT i
	INT iDefaultValue = GET_AMBIENT_ACTIVITY_LEADERBOARD_DEFAULT_VALUE(eSortType)
	INT iMaxLoops = COUNT_OF(leaderboardArray)
	
	FOR i = iIndex TO (iMaxLoops-2) STEP 1
		leaderboardArray[i] = leaderboardArray[i+1]
	ENDFOR
	
	leaderboardArray[(iMaxLoops-1)].playerID = INVALID_PLAYER_INDEX()
	leaderboardArray[(iMaxLoops-1)].iScore = iDefaultValue
ENDPROC

/// PURPOSE:
///    Call this function when a player's score has changed
/// PARAMS:
///    leaderboardArray - Your sorted leaderboard array
///    playerID - The player who has changed their score
///    iPlayerScore - The new score for this player
///    eSortType - The sort type, Ascending, Descending...
PROC SORT_AMBIENT_ACTIVITY_LEADERBOARD(AMBIENT_ACTIVITY_LEADERBOARD_STRUCT &leaderboardArray[], PLAYER_INDEX playerID, INT iPlayerScore, AMBIENT_ACTIVITY_LEADERBOARD_SORT_TYPE eSortType = AAL_SORT_DESCENDING)
	
	// If we have an invalid player, exit - can't act on this
	IF playerID = INVALID_PLAYER_INDEX()
		CPRINTLN(DEBUG_NET_AMBIENT, "**[ALBD] SORT_AMBIENT_ACTIVITY_LEADERBOARD - playerID = INVALID_PLAYER_INDEX()")
		EXIT
	ENDIF
	
	CPRINTLN(DEBUG_NET_AMBIENT, "**[ALBD] SORT_AMBIENT_ACTIVITY_LEADERBOARD [", GET_AMBIENT_ACTIVITY_LEADERBOARD_SORT_TYPE_NAME(eSortType), "]")
	CPRINTLN(DEBUG_NET_AMBIENT, "**[ALBD]		Player: ", GET_PLAYER_NAME(playerID), ", ", NATIVE_TO_INT(playerID))
	CPRINTLN(DEBUG_NET_AMBIENT, "**[ALBD]		PlayerScore: ", iPlayerScore)

	INT iLoop
	BOOL bFound
	INT iMaxLoop = COUNT_OF(leaderboardArray)
	
	// Non Uniform sort requires us to evaluate the sorting algorithm first as players score may have increased or decreased
	IF eSortType = AAL_SORT_NON_UNIFORM_SCORING
	OR eSortType = AAL_SORT_NON_UNIFORM_SCORING_ASCENDING
		
		INT iTempLoop
		BOOL bTempFound
		FOR iTempLoop = 0 TO (iMaxLoop-1) STEP 1
			
			IF leaderboardArray[iTempLoop].playerID = playerID
				bTempFound = TRUE
				IF iPlayerScore > leaderboardArray[iTempLoop].iScore
					IF eSortType = AAL_SORT_NON_UNIFORM_SCORING_ASCENDING
						eSortType = AAL_SORT_ASCENDING_REDUCING
						CPRINTLN(DEBUG_NET_AMBIENT, "**[ALBD]			- Player score increased: AAL_SORT_ASCENDING_REDUCING")
					ELSE
						eSortType = AAL_SORT_DESCENDING
						CPRINTLN(DEBUG_NET_AMBIENT, "**[ALBD]			- Player score increased: AAL_SORT_DESCENDING")
					ENDIF
				ELIF iPlayerScore < leaderboardArray[iTempLoop].iScore
					IF eSortType = AAL_SORT_NON_UNIFORM_SCORING_ASCENDING
						eSortType = AAL_SORT_ASCENDING
						CPRINTLN(DEBUG_NET_AMBIENT, "**[ALBD]			- Player score decreased: AAL_SORT_ASCENDING")
					ELSE
						eSortType = AAL_SORT_DESCENDING_REDUCING
						CPRINTLN(DEBUG_NET_AMBIENT, "**[ALBD]			- Player score decreased: AAL_SORT_DESCENDING_REDUCING")
					ENDIF
				ELSE
					// Scores are equal - why are we sorting
					CPRINTLN(DEBUG_NET_AMBIENT, "**[ALBD]			- Player score remained the same. EXIT")
					EXIT
				ENDIF
				
				// Exit this loop to begin sort
				iTempLoop = iMaxLoop
			ENDIF			
		ENDFOR
		
		// If we never found our player, they must be new so we revert to descending sort
		IF NOT bTempFound
			eSortType = AAL_SORT_DESCENDING
		ENDIF
	ENDIF
	
	IF eSortType = AAL_SORT_DESCENDING_REDUCING
	OR eSortType = AAL_SORT_ASCENDING_REDUCING
		
		FOR iLoop = 0 TO (iMaxLoop-1) STEP 1
		
			IF NOT bFound
				IF leaderboardArray[iLoop].playerID = playerID
					
					leaderboardArray[iLoop].iScore = iPlayerScore
					bFound = TRUE
				ENDIF
			ELSE
				IF eSortType = AAL_SORT_DESCENDING_REDUCING
					// For descending reducing, if the player has a lower score the need to move down
					IF iPlayerScore < leaderboardArray[iLoop].iScore
						
						leaderboardArray[iLoop-1] = leaderboardArray[iLoop]
						
						leaderboardArray[iLoop].playerID = playerID
						leaderboardArray[iLoop].iScore = iPlayerScore
					ENDIF
				ELSE
					// For descending reducing, if the player has a lower score the need to move down
					IF iPlayerScore > leaderboardArray[iLoop].iScore
						
						leaderboardArray[iLoop-1] = leaderboardArray[iLoop]
						
						leaderboardArray[iLoop].playerID = playerID
						leaderboardArray[iLoop].iScore = iPlayerScore
					ENDIF
				ENDIF
			ENDIF
		ENDFOR	
	ELSE
		// Loop backwards 
		FOR iLoop =(iMaxLoop-1) TO 0 STEP -1 
			
			// Find the player that has scored
			IF NOT bFound
				IF leaderboardArray[iLoop].playerID = playerID
					
					leaderboardArray[iLoop].iScore = iPlayerScore
					bFound = TRUE
					CPRINTLN(DEBUG_NET_AMBIENT, "**[ALBD]		Found Player: ", GET_PLAYER_NAME(playerID), " in LB position ", iLoop)
				ENDIF
			ELSE
				// If the player has beaten the score (depending on sort), switch out the array position
				IF eSortType = AAL_SORT_DESCENDING
					IF iPlayerScore > leaderboardArray[iLoop].iScore 
						leaderboardArray[iLoop+1] = leaderboardArray[iLoop]
						leaderboardArray[iLoop].playerID = playerID
						leaderboardArray[iLoop].iScore = iPlayerScore
					ENDIF
				ELIF eSortType = AAL_SORT_ASCENDING
					CPRINTLN(DEBUG_NET_AMBIENT, "**[ALBD]		Comparing My PlayerScore: ", iPlayerScore, " With player ", GET_PLAYER_NAME(leaderboardArray[iLoop].playerID), " score ", leaderboardArray[iLoop].iScore)
					IF iPlayerScore < leaderboardArray[iLoop].iScore
						CPRINTLN(DEBUG_NET_AMBIENT, "**[ALBD]		My PlayerScore: ", iPlayerScore, " is better than this player's score ", GET_PLAYER_NAME(leaderboardArray[iLoop].playerID), ", ", leaderboardArray[iLoop].iScore)
						leaderboardArray[iLoop+1] = leaderboardArray[iLoop]
						leaderboardArray[iLoop].playerID = playerID
						leaderboardArray[iLoop].iScore = iPlayerScore
						
					ENDIF
				ENDIF
			ENDIF		
		ENDFOR
	ENDIF
	
	// If the player does not exist, insert them into the array
	IF NOT bFound
		INSERT_PLAYER_TO_AMBIENT_ACTIVITY_LEADERBOARD(leaderboardArray, playerID, iPlayerScore)	
	ENDIF
ENDPROC

/// PURPOSE:
///    Call this every frame for players leaving to be automatically handled for your mode.
FUNC BOOL MAINTAIN_PLAYER_LEAVING_AMBIENT_ACTIVITY_LEADERBOARD(AMBIENT_ACTIVITY_LEADERBOARD_STRUCT &leaderboardArray[], INT &iStaggeredCount, AMBIENT_ACTIVITY_LEADERBOARD_SORT_TYPE eSortType = AAL_SORT_DESCENDING)

	BOOL bRemoved
	INT iMaxLoop = COUNT_OF(leaderboardArray)
	
	IF leaderboardArray[iStaggeredCount].playerID != INVALID_PLAYER_INDEX()
		
		IF NOT IS_NET_PLAYER_OK(leaderboardArray[iStaggeredCount].playerID, FALSE)
		OR NOT NETWORK_IS_PLAYER_A_PARTICIPANT(leaderboardArray[iStaggeredCount].playerID)
		// We could add a participant of script check?
			REMOVE_PLAYER_FROM_AMBIENT_ACTIVITY_LEADERBOARD_INDEX(leaderboardArray, iStaggeredCount, eSortType)
			
			bRemoved = TRUE
		ENDIF
	ENDIF
	
	iStaggeredCount++
	IF iStaggeredCount >= iMaxLoop
		iStaggeredCount = 0
	ENDIF
	
	RETURN bRemoved
ENDFUNC






