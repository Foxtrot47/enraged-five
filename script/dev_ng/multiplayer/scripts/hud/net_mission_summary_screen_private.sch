/* ------------------------------------------------------------------
* Name: net_mission_summary_screen_private.sch
* Author: James Adwick
* Date: 20/11/2012
* Purpose: End of mission screen (common) that appears at the same
* 		   time as MISSION PASSED big message private commands.
* ------------------------------------------------------------------*/

USING "globals.sch"
USING "net_include.sch"
USING "end_screen.sch"


/// PURPOSE: Cleans up mission summary screen, available for next one
PROC CLEANUP_MISSION_SUMMARY_SCREEN()
	
	NET_PRINT("[JA@MESSAGE] CLEANUP_MISSION_SUMMARY_SCREEN - cleaning up summary screen") NET_NL()
	
	MPGlobals.g_missionSummary.summaryScreenState = MISSION_SUMMARY_NULL
	MPGlobals.g_missionSummary.bSummaryScreenDisplayed = FALSE
	
//	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("CommonMenu")
	RESET_ENDSCREEN(MPGlobals.g_missionSummary.endScreenStats)
	MPGlobals.g_missionSummary.endScreenStats.splash = NULL
	ENDSCREEN_SHUTDOWN(MPGlobals.g_missionSummary.endScreenStats, TRUE)
ENDPROC

/// PURPOSE: Main function called within big message to load and render summary screen
PROC DRAW_MISSION_SUMMARY_SCREEN(INT iOptionalXP = -1)

	SWITCH MPGlobals.g_missionSummary.summaryScreenState
		CASE MISSION_SUMMARY_INIT
		
			IF ENDSCREEN_PREPARE(MPGlobals.g_missionSummary.endScreenStats, FALSE)
		
				NET_PRINT("[JA@MESSAGE] DRAW_MISSION_SUMMARY_SCREEN - ENDSCREEN_PREPARE complete moving to MISSION_SUMMARY_DRAW") NET_NL()
		
				IF iOptionalXP != -1
					ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(MPGlobals.g_missionSummary.endScreenStats, ESEF_RAW_INTEGER, "BM_END_XP", "", iOptionalXP, 0, ESCM_NO_MARK, FALSE)
				ENDIF
		
				MPGlobals.g_missionSummary.summaryScreenState = MISSION_SUMMARY_DRAW
			ENDIF
		BREAK
		
		CASE MISSION_SUMMARY_DRAW
			MPGlobals.g_missionSummary.endScreenStats.bAlwaysShowStats = TRUE
			RENDER_ENDSCREEN(MPGlobals.g_missionSummary.endScreenStats, FALSE)
			
			
						
		BREAK
	ENDSWITCH

ENDPROC
