
// ________________________________________________________________________	//

// FILE:																	//
// X:\gta5\script\dev_ng\multiplayer\scripts\hud\Job_Intro_Overheads.sch	//
// ------------------------------------------------------------------------ //

// AUTHOR:																	//
// William.Kennedy@RockstarNorth.com										//
// ------------------------------------------------------------------------ //

// PURPOSE:																	//
// Contains functions and logic for processing job intro overheads.			//
// ------------------------------------------------------------------------ //

// ________________________________________________________________________	//

USING "Commands_Hud.sch"
USING "net_hud_activating.sch"
USING "Net_include.sch"
USING "net_common_functions.sch"
USING "Net_Above_Head_Display.sch"

#IF IS_DEBUG_BUILD
USING "Profiler.sch"
#ENDIF

CONST_INT NUM_TIMES_TO_SHOW_JOB_INTRO_OVERHEADS_BUTTON_HELP		5
CONST_INT KEEP_NAME_ON_SCREEN_FOR_TALKING_TIME					1500

ENUM eJOB_INTRO_OVERHEADS_STAGE
	eJOBINTROOVERHEADSSTAGE_INIT = 0,
	eJOBINTROOVERHEADSSTAGE_SETUP_JOB_INTRO_OVERHEADS,
	eJOBINTROOVERHEADSSTAGE_MAINTAIN_RACE_JOB_INTRO,
	eJOBINTROOVERHEADSSTAGE_END
ENDENUM

STRUCT STRUCT_JOB_INTRO_OVERHEADS_PARTICIPANT_DATA
	PARTICIPANT_INDEX participantId
	PLAYER_INDEX playerId
	PED_INDEX pedId
	INt iParticipantId
	INT iPlayerId
	VECTOR vIntroPosition
	BOOL bVisibleOnScreen
	SCRIPT_TIMER talkingTimer
	TEXT_LABEL_63 tl31Name
	FLOAT fDistanceFromCamera
ENDSTRUCT

ENUM RACE_NAME_STATE
	eRN_IDLE,
	eRN_DISPLAY
ENDENUM

STRUCT STRUCT_JOB_INTRO_OVERHEADS_DATA
	INT iTotalNumParticipants
	STRUCT_JOB_INTRO_OVERHEADS_PARTICIPANT_DATA sJobIntroOhParticipantData[NUM_NETWORK_PLAYERS]
	eJOB_INTRO_OVERHEADS_STAGE eStage
	INT iParticipantNetOkBitSet
	INT iClosestOnScreenParticipantsToCamera[NUM_NETWORK_PLAYERS]
	BOOL bOverheadsButtonToggle = TRUE
	BOOL bShownButtonHelp
	RACE_NAME_STATE eRaceNameState
	#IF IS_DEBUG_BUILD
	BOOL bDoDebugPrints
	#ENDIF
ENDSTRUCT

FUNC BOOL RACE_INTRO_NAME_TOGGLE_PRESSED()
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_Y)
	OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_Y)
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL OK_FOR_RACE_NAMES(STRUCT_JOB_INTRO_OVERHEADS_DATA &sJobIntroOhData)

	RETURN FALSE

	IF IS_CORONA_READY_TO_START_WITH_JOB()
	AND sJobIntroOhData.iTotalNumParticipants > 1
	
	AND NOT CONTENT_IS_USING_ARENA()
	AND g_mnMyRaceModel != RCBANDITO
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_JOB_INTRO_OVERHEADS_BUTTON_TOGGLE(STRUCT_JOB_INTRO_OVERHEADS_DATA &sJobIntroOhData)
	SWITCH sJobIntroOhData.eRaceNameState
		CASE eRN_IDLE
			IF OK_FOR_RACE_NAMES(sJobIntroOhData)
				IF RACE_INTRO_NAME_TOGGLE_PRESSED()	
					sJobIntroOhData.bOverheadsButtonToggle = FALSE
					PRINTLN("[JOB_INTRO_OH] - MAINTAIN_JOB_INTRO_OVERHEADS_BUTTON_TOGGLE, sJobIntroOhData.eRaceNameState = eRN_DISPLAY ")
					sJobIntroOhData.eRaceNameState = eRN_DISPLAY
				ENDIF	
			ENDIF
		BREAK
		
		CASE eRN_DISPLAY
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("JOBINTOH_1")
				CLEAR_HELP()
			ENDIF
		BREAK
	
	ENDSWITCH
ENDPROC

PROC REMOVE_GLOBAL_OVERHEADS()
	
	INT i, iTag
	
	IF IS_MP_GAMER_TAG_MOVIE_ACTIVE()
		REPEAT NUM_NETWORK_PLAYERS i
			
			// Remove code tags.
			IF IS_MP_GAMER_TAG_ACTIVE(i)
				SET_ALL_MP_GAMER_TAGS_VISIBILITY(i, FALSE)
				REMOVE_MP_GAMER_TAG(i)
				PRINTLN("[JOB_INTRO_OH] - removed mp gamer tag #", i)
			ELSE
				PRINTLN("[JOB_INTRO_OH] - mp gamer tag #", i, " already not active")
			ENDIF
			
			// Reset global script oberheads system data.
			g_sOverHead.iOverHeadPlayerNumber[i] = -1
			CLEAR_BIT(g_sOverHead.iOverHeadSetUp, i)
			PRINTLN("[JOB_INTRO_OH] - g_sOverHead.iOverHeadPlayerNumber[", i, "] = -1")
			PRINTLN("[JOB_INTRO_OH] - CLEAR_BIT(g_sOverHead.iOverHeadSetUp, ", i, ")")
			
			REPEAT eMP_TAG_BITSETS iTag
				g_sOverHead.iOverHeadItemDisplayed[iTag][i] = 0
				PRINTLN("[JOB_INTRO_OH] - g_sOverHead.iOverHeadItemDisplayed[", iTag, "][", i, "] = 0")
			ENDREPEAT
			
		ENDREPEAT
	ENDIF
	
ENDPROC

PROC REMOVE_JOB_INTRO_OVERHEADS()
	
	INT i
	
	IF IS_MP_GAMER_TAG_MOVIE_ACTIVE()
		REPEAT NUM_NETWORK_PLAYERS i
			
			// Remove code tags.
			IF IS_MP_GAMER_TAG_ACTIVE(i)
				SET_ALL_MP_GAMER_TAGS_VISIBILITY(i, FALSE)
				REMOVE_MP_GAMER_TAG(i)
				PRINTLN("[JOB_INTRO_OH] - removed mp gamer tag #", i)
			ELSE
				PRINTLN("[JOB_INTRO_OH] - mp gamer tag #", i, " already not active")
			ENDIF
			
		ENDREPEAT
	ENDIF
	
ENDPROC

PROC SET_OVERHEAD_COLOUR_AND_ALPHA(STRUCT_JOB_INTRO_OVERHEADS_DATA &sJobIntroOhData, INT iIndex #IF IS_DEBUG_BUILD , BOOL bDoDebugPrints = FALSE #ENDIF)
	
	INT iTag
	
	// Setup colours and alphas
	HUD_COLOURS	hudColourToSet = GET_PLAYER_HUD_COLOUR(sJobIntroOhData.sJobIntroOhParticipantData[iIndex].playerId) // GET_COLOUR_FOR_PLAYER_NAME(iPlayerTeam)
	#IF IS_DEBUG_BUILD
	STRING strTemp = HUD_COLOUR_AS_STRING(hudColourToSet)
	#ENDIF
	
	REPEAT COUNT_OF(eMP_TAG) iTag
	
		IF INT_TO_ENUM(eMP_TAG, iTag) = MP_TAG_GAMER_NAME
			SET_MP_GAMER_TAG_VISIBILITY(sJobIntroOhData.sJobIntroOhParticipantData[iIndex].iPlayerId, INT_TO_ENUM(eMP_TAG, iTag), TRUE)
			SET_MP_GAMER_TAG_ALPHA(sJobIntroOhData.sJobIntroOhParticipantData[iIndex].iPlayerId, INT_TO_ENUM(eMP_TAG, iTag), UNIVERSAL_PLAYER_NAME_ALPHA)
			#IF IS_DEBUG_BUILD
				IF bDoDebugPrints
					PRINTLN("[JOB_INTRO_OH] - SET_MP_GAMER_TAG_ALPHA tag #", iTag, " to ", UNIVERSAL_PLAYER_NAME_ALPHA)	
				ENDIF
			#ENDIF
		ELSE
			SET_MP_GAMER_TAG_VISIBILITY(sJobIntroOhData.sJobIntroOhParticipantData[iIndex].iPlayerId, INT_TO_ENUM(eMP_TAG, iTag), FALSE)
			SET_MP_GAMER_TAG_ALPHA(sJobIntroOhData.sJobIntroOhParticipantData[iIndex].iPlayerId, INT_TO_ENUM(eMP_TAG, iTag), 0)	
			#IF IS_DEBUG_BUILD
				IF bDoDebugPrints
					PRINTLN("[JOB_INTRO_OH] - SET_MP_GAMER_TAG_ALPHA tag #", iTag, " to 0")	
				ENDIF
			#ENDIF
		ENDIF
		
		IF INT_TO_ENUM(eMP_TAG, iTag) = MP_TAG_HEALTH_BAR
		
			SET_MP_GAMER_TAG_HEALTH_BAR_COLOUR(sJobIntroOhData.sJobIntroOhParticipantData[iIndex].iPlayerId, hudColourToSet)
			SET_MP_GAMER_TAG_ALPHA(sJobIntroOhData.sJobIntroOhParticipantData[iIndex].iPlayerId,MP_TAG_HEALTH_BAR, 255 )
			#IF IS_DEBUG_BUILD
				IF bDoDebugPrints
					PRINTLN("[JOB_INTRO_OH] - SET_MP_GAMER_TAG_HEALTH_BAR_COLOUR tag #", iTag, " to ", strTemp)	
				ENDIF
			#ENDIF
			
		ELIF INT_TO_ENUM(eMP_TAG, iTag) = MP_TAG_CREW_TAG
		
			IF ARE_STRINGS_EQUAL(g_sOverHead.ClanTag[NATIVE_TO_INT(PLAYER_ID())], g_sOverHead.ClanTag[sJobIntroOhData.sJobIntroOhParticipantData[iIndex].iPlayerId])
				SET_MP_GAMER_TAG_COLOUR(sJobIntroOhData.sJobIntroOhParticipantData[iIndex].iPlayerId, INT_TO_ENUM(eMP_TAG, iTag), HUD_COLOUR_YELLOWLIGHT)
				#IF IS_DEBUG_BUILD
					IF bDoDebugPrints
						PRINTLN("[JOB_INTRO_OH] - SET_MP_GAMER_TAG_HEALTH_BAR_COLOUR tag #", iTag, " to HUD_COLOUR_YELLOWLIGHT")	
					ENDIF
				#ENDIF
			ELSE
				SET_MP_GAMER_TAG_COLOUR(sJobIntroOhData.sJobIntroOhParticipantData[iIndex].iPlayerId, INT_TO_ENUM(eMP_TAG, iTag), HUD_COLOUR_WHITE)	
				#IF IS_DEBUG_BUILD
					IF bDoDebugPrints
						PRINTLN("[JOB_INTRO_OH] - SET_MP_GAMER_TAG_HEALTH_BAR_COLOUR tag #", iTag, " to HUD_COLOUR_WHITE")	
					ENDIF
				#ENDIF
			ENDIF
			
		ELSE
		
			SET_MP_GAMER_TAG_COLOUR(sJobIntroOhData.sJobIntroOhParticipantData[iIndex].iPlayerId, INT_TO_ENUM(eMP_TAG, iTag), hudColourToSet)
			#IF IS_DEBUG_BUILD
				IF bDoDebugPrints
					PRINTLN("[JOB_INTRO_OH] - SET_MP_GAMER_TAG_COLOUR tag #", iTag, " to ", strTemp)	
				ENDIF
			#ENDIF
			
		ENDIF
		
	ENDREPEAT
					
ENDPROC

PROC ADD_JOB_INTRO_OVERHEADS(STRUCT_JOB_INTRO_OVERHEADS_DATA &sJobIntroOhData)
	
	INT i
	
	IF IS_MP_GAMER_TAG_MOVIE_ACTIVE()
		REPEAT sJobIntroOhData.iTotalNumParticipants i
			
			IF NOT IS_MP_GAMER_TAG_ACTIVE(sJobIntroOhData.sJobIntroOhParticipantData[i].iPlayerId)
				IF IS_MP_GAMER_TAG_FREE(sJobIntroOhData.sJobIntroOhParticipantData[i].iPlayerId)
					
					// Create tag
					GAMER_HANDLE gamerHandle = GET_GAMER_HANDLE_PLAYER(sJobIntroOhData.sJobIntroOhParticipantData[i].playerId)	
					BOOL bIsPlayerInActiveClan = IS_PLAYER_IN_ACTIVE_CLAN(gamerHandle)
					NETWORK_CLAN_DESC PlayerClan
					BOOL isRockstar
					BOOL isPrivate
					
					IF bIsPlayerInActiveClan
						
						PlayerClan = GET_GAMER_CREW(gamerHandle)
						
						IF IS_PLAYER_CLAN_SYSTEM(gamerHandle)
							isRockstar = TRUE
						ELSE
							isRockstar = FALSE
						ENDIF
						
						IF IS_PLAYER_CLAN_PRIVATE(gamerHandle)
							isPrivate = TRUE
						ELSE
							isPrivate = FALSE
						ENDIF
							
					ENDIF
					
					#IF IS_DEBUG_BUILD
						PRINTLN("[JOB_INTRO_OH] - [Clan Data] - ******************** Data for player #", sJobIntroOhData.sJobIntroOhParticipantData[i].iPlayerId, " ********************")
						PRINTLN("[JOB_INTRO_OH] - [Clan Data] - player in active clan = ", bIsPlayerInActiveClan)
						PRINTLN("[JOB_INTRO_OH] - [Clan Data] - player is Rockstar = ", isRockstar)
						PRINTLN("[JOB_INTRO_OH] - [Clan Data] - player clan is private = ", isPrivate)
						PRINTLN("[JOB_INTRO_OH] - [Clan Data] - player clan id = ", PlayerClan.Id)
						PRINTLN("[JOB_INTRO_OH] - [Clan Data] - player clan name = ", PlayerClan.ClanName)
						PRINTLN("[JOB_INTRO_OH] - [Clan Data] - player clan tag = ", PlayerClan.ClanTag)
						PRINTLN("[JOB_INTRO_OH] - [Clan Data] - player clan member count = ", PlayerClan.MemberCount)
						PRINTLN("[JOB_INTRO_OH] - [Clan Data] - player clan is system clan = ", PlayerClan.IsSystemClan)
						PRINTLN("[JOB_INTRO_OH] - [Clan Data] - player clan is open clan = ", PlayerClan.IsOpenClan)
						PRINTLN("[JOB_INTRO_OH] - [Clan Data] - player clan rank name = ", PlayerClan.RankName)
						PRINTLN("[JOB_INTRO_OH] - [Clan Data] - player clan rank order = ", PlayerClan.RankOrder)
						PRINTLN("[JOB_INTRO_OH] - [Clan Data] - player clan created time = ", PlayerClan.CreatedTimePosix)
						PRINTLN("[JOB_INTRO_OH] - [Clan Data] - player clan red = ", PlayerClan.ClanColor_Red)
						PRINTLN("[JOB_INTRO_OH] - [Clan Data] - player clan green = ", PlayerClan.ClanColor_Green)
						PRINTLN("[JOB_INTRO_OH] - [Clan Data] - player clan blue = ", PlayerClan.ClanColor_Blue)
						PRINTLN("[JOB_INTRO_OH] - [Clan Data] - ")
					#ENDIF
					
					CREATE_MP_GAMER_TAG_WITH_CREW_COLOR(	sJobIntroOhData.sJobIntroOhParticipantData[i].iPlayerId,
															sJobIntroOhData.sJobIntroOhParticipantData[i].tl31Name, 
															isPrivate, isRockstar, PlayerClan.ClanTag, PlayerClan.RankOrder, g_Private_LocalPlayerCrew_Colour_Red, g_Private_LocalPlayerCrew_Colour_Green, g_Private_LocalPlayerCrew_Colour_Blue)
					PRINTLN("[JOB_INTRO_OH] - called CREATE_MP_GAMER_TAG_WITH_CREW_COLOR:")
					PRINTLN("[JOB_INTRO_OH] - iPlayerNum = ", sJobIntroOhData.sJobIntroOhParticipantData[i].iPlayerId)
					PRINTLN("[JOB_INTRO_OH] - PlayerName = ", sJobIntroOhData.sJobIntroOhParticipantData[i].tl31Name)
					PRINTLN("[JOB_INTRO_OH] - bCrewTypeIsPrivate = ", isPrivate)
					PRINTLN("[JOB_INTRO_OH] - bCrewTagContainsRockstar = ", isRockstar)
					PRINTLN("[JOB_INTRO_OH] - CrewTag = ", PlayerClan.ClanTag)
					PRINTLN("[JOB_INTRO_OH] - iCrewRank = ", PlayerClan.RankOrder)
					PRINTLN("[JOB_INTRO_OH] - crewR = (g_Private_LocalPlayerCrew_Colour_Red) ", g_Private_LocalPlayerCrew_Colour_Red)
					PRINTLN("[JOB_INTRO_OH] - crewG = (g_Private_LocalPlayerCrew_Colour_Green) ", g_Private_LocalPlayerCrew_Colour_Green)
					PRINTLN("[JOB_INTRO_OH] - crewB = (g_Private_LocalPlayerCrew_Colour_Blue) ", g_Private_LocalPlayerCrew_Colour_Blue)
					
					SET_OVERHEAD_COLOUR_AND_ALPHA(sJobIntroOhData, i #IF IS_DEBUG_BUILD , TRUE #ENDIF)
					
				ENDIF
			ENDIF
			
		ENDREPEAT
	ENDIF
	
ENDPROC

PROC RESET_JOB_INTRO_OVERHEADS_PARTICIPANT_DATA(STRUCT_JOB_INTRO_OVERHEADS_DATA &sJobIntroOhData)
	
	INT i
	TEXT_LABEL_31 tl31Temp
	
	// Reset data.
	REPEAT NUM_NETWORK_PLAYERS i
		sJobIntroOhData.sJobIntroOhParticipantData[i].participantId = INT_TO_NATIVE(PARTICIPANT_INDEX, -1)
		sJobIntroOhData.sJobIntroOhParticipantData[i].playerId = INT_TO_NATIVE(PLAYER_INDEX, -1)
		sJobIntroOhData.sJobIntroOhParticipantData[i].iParticipantId = (-1)
		sJobIntroOhData.sJobIntroOhParticipantData[i].iPlayerId = (-1)
		sJobIntroOhData.sJobIntroOhParticipantData[i].vIntroPosition = << 0.0, 0.0, 0.0 >> 
		sJobIntroOhData.sJobIntroOhParticipantData[i].bVisibleOnScreen = FALSE
		sJobIntroOhData.sJobIntroOhParticipantData[i].tl31Name = tl31Temp
		sJobIntroOhData.sJobIntroOhParticipantData[i].fDistanceFromCamera = (-1.0)
		sJobIntroOhData.iClosestOnScreenParticipantsToCamera[i] = (-1)
		PRINTLN("[JOB_INTRO_OH] - cleared sJobIntroOhData.sJobIntroOhParticipantData[", i, "] to invalid values before setup")
	ENDREPEAT
	
ENDPROC

PROC SAVE_JOB_INTRO_PLAYER_DATA(STRUCT_JOB_INTRO_OVERHEADS_DATA &sJobIntroOhData)
	
	#IF IS_DEBUG_BUILD
	PRINTLN("[JOB_INTRO_OH] - SAVE_JOB_INTRO_PLAYER_DATA called")
	DEBUG_PRINTCALLSTACK()
	#ENDIF
	
	INT iParticipant = -1
	INT iPlayer = -1
	PLAYER_INDEX playerTemp
	
	RESET_JOB_INTRO_OVERHEADS_PARTICIPANT_DATA(sJobIntroOhData)
	
	REPEAT NUM_NETWORK_PLAYERS iPlayer
		sJobIntroOhData.iClosestOnScreenParticipantsToCamera[iPlayer] = -1
	ENDREPEAT
	
	// Fill data.
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_NATIVE(PARTICIPANT_INDEX, iParticipant))
			
			playerTemp = NETWORK_GET_PLAYER_INDEX(INT_TO_NATIVE(PARTICIPANT_INDEX, iParticipant))
			iPlayer = NATIVE_TO_INT(playerTemp)
			
			IF IS_NET_PLAYER_OK(playerTemp, FALSE)
				
				IF NOT IS_PLAYER_SCTV(playerTemp)
					
					sJobIntroOhData.sJobIntroOhParticipantData[sJobIntroOhData.iTotalNumParticipants].playerId = playerTemp
					sJobIntroOhData.sJobIntroOhParticipantData[sJobIntroOhData.iTotalNumParticipants].iPlayerId = iPlayer
					sJobIntroOhData.sJobIntroOhParticipantData[sJobIntroOhData.iTotalNumParticipants].participantId = INT_TO_NATIVE(PARTICIPANT_INDEX, iParticipant)
					sJobIntroOhData.sJobIntroOhParticipantData[sJobIntroOhData.iTotalNumParticipants].iParticipantId = iParticipant
					sJobIntroOhData.sJobIntroOhParticipantData[sJobIntroOhData.iTotalNumParticipants].pedId = GET_PLAYER_PED(playerTemp)
					sJobIntroOhData.sJobIntroOhParticipantData[sJobIntroOhData.iTotalNumParticipants].tl31Name = GET_PLAYER_NAME(playerTemp)
					sJobIntroOhData.iClosestOnScreenParticipantsToCamera[sJobIntroOhData.iTotalNumParticipants] = iParticipant
					IF DOES_ENTITY_EXIST(sJobIntroOhData.sJobIntroOhParticipantData[sJobIntroOhData.iTotalNumParticipants].pedId)
						IF NOT IS_ENTITY_DEAD(sJobIntroOhData.sJobIntroOhParticipantData[sJobIntroOhData.iTotalNumParticipants].pedId)
							sJobIntroOhData.sJobIntroOhParticipantData[sJobIntroOhData.iTotalNumParticipants].vIntroPosition = GET_ENTITY_COORDS(sJobIntroOhData.sJobIntroOhParticipantData[sJobIntroOhData.iTotalNumParticipants].pedId)
						ENDIF
					ENDIF
					sJobIntroOhData.sJobIntroOhParticipantData[sJobIntroOhData.iTotalNumParticipants].fDistanceFromCamera = GET_DISTANCE_BETWEEN_COORDS(GET_FINAL_RENDERED_CAM_COORD(), sJobIntroOhData.sJobIntroOhParticipantData[sJobIntroOhData.iTotalNumParticipants].vIntroPosition)
					
					PRINTLN("[JOB_INTRO_OH] - sJobIntroOhData.sJobIntroOhParticipantData[", sJobIntroOhData.iTotalNumParticipants, "] is valid for processing. Adding to data:")
					PRINTLN("[JOB_INTRO_OH] - sJobIntroOhData.sJobIntroOhParticipantData[", sJobIntroOhData.iTotalNumParticipants, "].iPlayerId = ", sJobIntroOhData.sJobIntroOhParticipantData[sJobIntroOhData.iTotalNumParticipants].iPlayerId)
					PRINTLN("[JOB_INTRO_OH] - sJobIntroOhData.sJobIntroOhParticipantData[", sJobIntroOhData.iTotalNumParticipants, "].iParticipantId = ", sJobIntroOhData.sJobIntroOhParticipantData[sJobIntroOhData.iTotalNumParticipants].iParticipantId)
					PRINTLN("[JOB_INTRO_OH] - sJobIntroOhData.sJobIntroOhParticipantData[", sJobIntroOhData.iTotalNumParticipants, "].tl31Name = ", sJobIntroOhData.sJobIntroOhParticipantData[sJobIntroOhData.iTotalNumParticipants].tl31Name)
					PRINTLN("[JOB_INTRO_OH] - sJobIntroOhData.sJobIntroOhParticipantData[", sJobIntroOhData.iTotalNumParticipants, "].vIntroPosition = ", sJobIntroOhData.sJobIntroOhParticipantData[sJobIntroOhData.iTotalNumParticipants].vIntroPosition)
					PRINTLN("[JOB_INTRO_OH] - sJobIntroOhData.sJobIntroOhParticipantData[", sJobIntroOhData.iTotalNumParticipants, "].fDistanceFromCamera = ", sJobIntroOhData.sJobIntroOhParticipantData[sJobIntroOhData.iTotalNumParticipants].fDistanceFromCamera)
					PRINTLN("[JOB_INTRO_OH] - sJobIntroOhData.iClosestOnScreenParticipantsToCamera[", sJobIntroOhData.iTotalNumParticipants, "] = ", sJobIntroOhData.iClosestOnScreenParticipantsToCamera[sJobIntroOhData.iTotalNumParticipants])
					
					sJobIntroOhData.iTotalNumParticipants++
					
					PRINTLN("[JOB_INTRO_OH] - sJobIntroOhData.iTotalNumParticipants = ", sJobIntroOhData.iTotalNumParticipants)
					
				ELSE
					
					#IF IS_DEBUG_BUILD
					TEXT_LABEL_31 tl31TempName = GET_PLAYER_NAME(playerTemp)
					PRINTLN("[JOB_INTRO_OH] - not saving out data for participant/player ",  iParticipant, "/", iPlayer," (", tl31TempName, ") - they are SCTV.")
					#ENDIF
					
				ENDIF
				
			ELSE
					
				PRINTLN("[JOB_INTRO_OH] - not saving out data for participant/player ",  iParticipant, "/", iPlayer," - they are not net ok.")	
					
			ENDIF
			
		ENDIF
	ENDREPEAT
	
ENDPROC

PROC MAINTAIN_TIME_SINCE_STARTED_TALKING_TIMER(INT iParticipant, STRUCT_JOB_INTRO_OVERHEADS_DATA &sJobIntroOhData)
	
	IF NETWORK_IS_PLAYER_TALKING(sJobIntroOhData.sJobIntroOhParticipantData[iParticipant].playerId)
		RESET_NET_TIMER(sJobIntroOhData.sJobIntroOhParticipantData[iParticipant].talkingTimer)
		START_NET_TIMER(sJobIntroOhData.sJobIntroOhParticipantData[iParticipant].talkingTimer)
		#IF IS_DEBUG_BUILD
		IF sJobIntroOhData.bDoDebugPrints
			PRINTLN("[JOB_INTRO_OH] - NETWORK_IS_PLAYER_TALKING(sJobIntroOhData.sJobIntroOhParticipantData[", iParticipant, "].iPlayerId) = TRUE")
			PRINTLN("[JOB_INTRO_OH] - START_NET_TIMER(sJobIntroOhData.sJobIntroOhParticipantData[", iParticipant, "].talkingTimer)")
		ENDIF
		#ENDIF
	ELSE
		IF HAS_NET_TIMER_STARTED(sJobIntroOhData.sJobIntroOhParticipantData[iParticipant].talkingTimer)
			IF HAS_NET_TIMER_EXPIRED(sJobIntroOhData.sJobIntroOhParticipantData[iParticipant].talkingTimer, KEEP_NAME_ON_SCREEN_FOR_TALKING_TIME)
				RESET_NET_TIMER(sJobIntroOhData.sJobIntroOhParticipantData[iParticipant].talkingTimer)
				#IF IS_DEBUG_BUILD
				IF sJobIntroOhData.bDoDebugPrints
					PRINTLN("[JOB_INTRO_OH] - NETWORK_IS_PLAYER_TALKING(sJobIntroOhData.sJobIntroOhParticipantData[", iParticipant, "].iPlayerId) = FALSE")
					PRINTLN("[JOB_INTRO_OH] - sJobIntroOhData.sJobIntroOhParticipantData[", iParticipant, "].talkingTimer has expired, resetting timer.")
				ENDIF
				#ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC MAINTAIN_JOB_INTRO_OVERHEADS_EVERY_FRAME_DATA(STRUCT_JOB_INTRO_OVERHEADS_DATA &sJobIntroOhData)
	
	INT i
	
	REPEAT sJobIntroOhData.iTotalNumParticipants i
		IF IS_NET_PLAYER_OK(sJobIntroOhData.sJobIntroOhParticipantData[i].playerId)
			SET_BIT(sJobIntroOhData.iParticipantNetOkBitSet, i)
			sJobIntroOhData.sJobIntroOhParticipantData[i].bVisibleOnScreen = CAN_I_SEE_PLAYER(sJobIntroOhData.sJobIntroOhParticipantData[i].playerId)
			sJobIntroOhData.sJobIntroOhParticipantData[i].fDistanceFromCamera = VDIST2(GET_FINAL_RENDERED_CAM_COORD(), sJobIntroOhData.sJobIntroOhParticipantData[i].vIntroPosition)		
			MAINTAIN_TIME_SINCE_STARTED_TALKING_TIMER(i, sJobIntroOhData)
		ELSE
			CLEAR_BIT(sJobIntroOhData.iParticipantNetOkBitSet, i)
		ENDIF
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
	IF sJobIntroOhData.bDoDebugPrints
		INT iIndex
		PRINTLN("[JOB_INTRO_OH] - ")
		PRINTLN("[JOB_INTRO_OH] - ")
		PRINTLN("[JOB_INTRO_OH] - ")
		PRINTLN("[JOB_INTRO_OH] - ******************** MAINTAIN_JOB_INTRO_OVERHEADS_EVERY_FRAME_DATA - - START ********************")
		REPEAT sJobIntroOhData.iTotalNumParticipants iIndex
			PRINTLN("[JOB_INTRO_OH] - sJobIntroOhData.sJobIntroOhParticipantData[", iIndex, "].iParticipantId = ", sJobIntroOhData.sJobIntroOhParticipantData[iIndex].iParticipantId)
			PRINTLN("[JOB_INTRO_OH] - sJobIntroOhData.sJobIntroOhParticipantData[", iIndex, "].iPlayerId = ", sJobIntroOhData.sJobIntroOhParticipantData[iIndex].iPlayerId)
			PRINTLN("[JOB_INTRO_OH] - sJobIntroOhData.sJobIntroOhParticipantData[", iIndex, "].vIntroPosition = ", sJobIntroOhData.sJobIntroOhParticipantData[iIndex].vIntroPosition)
			PRINTLN("[JOB_INTRO_OH] - sJobIntroOhData.sJobIntroOhParticipantData[", iIndex, "].bVisibleOnScreen = ", sJobIntroOhData.sJobIntroOhParticipantData[iIndex].bVisibleOnScreen)
			PRINTLN("[JOB_INTRO_OH] - sJobIntroOhData.sJobIntroOhParticipantData[", iIndex, "].tl31Name = ", sJobIntroOhData.sJobIntroOhParticipantData[iIndex].tl31Name)
			PRINTLN("[JOB_INTRO_OH] - sJobIntroOhData.sJobIntroOhParticipantData[", iIndex, "].fDistanceFromCamera = ", sJobIntroOhData.sJobIntroOhParticipantData[iIndex].fDistanceFromCamera)
			IF NETWORK_IS_PLAYER_TALKING(sJobIntroOhData.sJobIntroOhParticipantData[iIndex].playerId)
				PRINTLN("[JOB_INTRO_OH] - NETWORK_IS_PLAYER_TALKING(sJobIntroOhData.sJobIntroOhParticipantData[", iIndex, "].playerId) = TRUE")
			ELSE
				PRINTLN("[JOB_INTRO_OH] - NETWORK_IS_PLAYER_TALKING(sJobIntroOhData.sJobIntroOhParticipantData[", iIndex, "].playerId) = FALSE")
			ENDIF
		ENDREPEAT
		PRINTLN("[JOB_INTRO_OH] - ******************** MAINTAIN_JOB_INTRO_OVERHEADS_EVERY_FRAME_DATA - END ********************")
	ENDIF
	#ENDIF
	
ENDPROC

PROC SORT_JOB_INTRO_OVERHEAD_PARTICIPANTS(STRUCT_JOB_INTRO_OVERHEADS_DATA &sJobIntroOhData)
	
	#IF IS_DEBUG_BUILD
	IF sJobIntroOhData.bDoDebugPrints
		PRINTLN("[JOB_INTRO_OH] - ")
		PRINTLN("[JOB_INTRO_OH] - ")
		PRINTLN("[JOB_INTRO_OH] - ")
		PRINTLN("[JOB_INTRO_OH] - ******************** SORT_JOB_INTRO_OVERHEAD_PARTICIPANTS - SORTING - START ********************")
	ENDIF
	#ENDIF
	
	INT j, i, iTempIndex, iTempIndex2
	
	#IF IS_DEBUG_BUILD
	IF sJobIntroOhData.bDoDebugPrints
		PRINTLN("[JOB_INTRO_OH] - ")
		PRINTLN("[JOB_INTRO_OH] - ------ SORT FOR VISIBILITY ------")
	ENDIF
	#ENDIF
	
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	// SORT FOR VISIBILITY.
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	REPEAT sJobIntroOhData.iTotalNumParticipants j
	
		#IF IS_DEBUG_BUILD
		IF sJobIntroOhData.bDoDebugPrints
			PRINTLN("[JOB_INTRO_OH] - ")
			PRINTLN("[JOB_INTRO_OH] - j = ", j)
		ENDIF
		#ENDIF
		
		REPEAT sJobIntroOhData.iTotalNumParticipants i
		
			#IF IS_DEBUG_BUILD
			IF sJobIntroOhData.bDoDebugPrints
				PRINTLN("[JOB_INTRO_OH] - i = ", i)
			ENDIF
			#ENDIF
			
			IF i >= 0
			AND (i+1) < sJobIntroOhData.iTotalNumParticipants
				
				iTempIndex = sJobIntroOhData.iClosestOnScreenParticipantsToCamera[i]
				iTempIndex2 = sJobIntroOhData.iClosestOnScreenParticipantsToCamera[i+1]
				
				#IF IS_DEBUG_BUILD
				IF sJobIntroOhData.bDoDebugPrints
					PRINTLN("[JOB_INTRO_OH] - iTempIndex = sJobIntroOhData.iClosestOnScreenParticipantsToCamera[", i, "] = ", iTempIndex)
					PRINTLN("[JOB_INTRO_OH] - iTempIndex2 = sJobIntroOhData.iClosestOnScreenParticipantsToCamera[", i+1, "] = ", iTempIndex2)
					PRINTLN("[JOB_INTRO_OH] - [", iTempIndex, "].bVisibleOnScreen = ", sJobIntroOhData.sJobIntroOhParticipantData[iTempIndex].bVisibleOnScreen)
					PRINTLN("[JOB_INTRO_OH] - [", iTempIndex2, "].bVisibleOnScreen = ", sJobIntroOhData.sJobIntroOhParticipantData[iTempIndex2].bVisibleOnScreen)
				ENDIF
				#ENDIF
				
				IF (sJobIntroOhData.sJobIntroOhParticipantData[iTempIndex].bVisibleOnScreen AND (NOT sJobIntroOhData.sJobIntroOhParticipantData[iTempIndex2].bVisibleOnScreen))
					#IF IS_DEBUG_BUILD
					IF sJobIntroOhData.bDoDebugPrints
						PRINTLN("[JOB_INTRO_OH] - [", iTempIndex, "].bVisibleOnScreen = TRUE and [", iTempIndex2, "].bVisibleOnScreen = FALSE - swapping data.")
					ENDIF
					#ENDIF
					iTempIndex2 = sJobIntroOhData.iClosestOnScreenParticipantsToCamera[i]
					sJobIntroOhData.iClosestOnScreenParticipantsToCamera[i] = sJobIntroOhData.iClosestOnScreenParticipantsToCamera[i+1]
					sJobIntroOhData.iClosestOnScreenParticipantsToCamera[i+1] = iTempIndex2
				ELSE
					#IF IS_DEBUG_BUILD
					IF sJobIntroOhData.bDoDebugPrints
						PRINTLN("[JOB_INTRO_OH] - [", iTempIndex, "].bVisibleOnScreen = ", sJobIntroOhData.sJobIntroOhParticipantData[iTempIndex].bVisibleOnScreen, ", [", iTempIndex2, "].bVisibleOnScreen = ", sJobIntroOhData.sJobIntroOhParticipantData[iTempIndex2].bVisibleOnScreen, ", We do not want to swap these.")
					ENDIF
					#ENDIF
				ENDIF
				
			ELSE
			
				#IF IS_DEBUG_BUILD
				IF sJobIntroOhData.bDoDebugPrints
					IF i < 0
						PRINTLN("[JOB_INTRO_OH] - i < 0, not perfporming sort on this index.")
					ENDIF
					IF (i+1) >= sJobIntroOhData.iTotalNumParticipants
						PRINTLN("[JOB_INTRO_OH] - i+1 (", i+1, ") >= sJobIntroOhData.iTotalNumParticipants (", sJobIntroOhData.iTotalNumParticipants, "), not performing sort on this index.")
					ENDIF
				ENDIF
				#ENDIF
				
			ENDIF
			
		ENDREPEAT
		
	ENDREPEAT
	
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	// SORT FOR DISTANCE FROM CAMERA.
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	#IF IS_DEBUG_BUILD
	IF sJobIntroOhData.bDoDebugPrints
		INT iIndex
		PRINTLN("[JOB_INTRO_OH] - ")
		PRINTLN("[JOB_INTRO_OH] - ")
		PRINTLN("[JOB_INTRO_OH] - ")
		PRINTLN("[JOB_INTRO_OH] - ******************** SORT_JOB_INTRO_OVERHEAD_PARTICIPANTS - SORTED DATA FOR VISIBILITY - START ********************")
		REPEAT sJobIntroOhData.iTotalNumParticipants iIndex
			PRINTLN("[JOB_INTRO_OH] - sJobIntroOhData.iClosestOnScreenParticipantsToCamera[", iIndex, "] (playerId) = ", sJobIntroOhData.iClosestOnScreenParticipantsToCamera[iIndex])
			INT iTemp = sJobIntroOhData.iClosestOnScreenParticipantsToCamera[iIndex]
			PRINTLN("[JOB_INTRO_OH] - sJobIntroOhData.sJobIntroOhParticipantData[", iTemp, "].iParticipantId = ", sJobIntroOhData.sJobIntroOhParticipantData[iTemp].iParticipantId)
			PRINTLN("[JOB_INTRO_OH] - sJobIntroOhData.sJobIntroOhParticipantData[", iTemp, "].iPlayerId = ", sJobIntroOhData.sJobIntroOhParticipantData[iTemp].iPlayerId)
			PRINTLN("[JOB_INTRO_OH] - sJobIntroOhData.sJobIntroOhParticipantData[", iTemp, "].vIntroPosition = ", sJobIntroOhData.sJobIntroOhParticipantData[iTemp].vIntroPosition)
			PRINTLN("[JOB_INTRO_OH] - sJobIntroOhData.sJobIntroOhParticipantData[", iTemp, "].bVisibleOnScreen = ", sJobIntroOhData.sJobIntroOhParticipantData[iTemp].bVisibleOnScreen)
			PRINTLN("[JOB_INTRO_OH] - sJobIntroOhData.sJobIntroOhParticipantData[", iTemp, "].tl31Name = ", sJobIntroOhData.sJobIntroOhParticipantData[iTemp].tl31Name)
			PRINTLN("[JOB_INTRO_OH] - sJobIntroOhData.sJobIntroOhParticipantData[", iTemp, "].fDistanceFromCamera = ", sJobIntroOhData.sJobIntroOhParticipantData[iTemp].fDistanceFromCamera)
		ENDREPEAT
		PRINTLN("[JOB_INTRO_OH] - ******************** SORT_JOB_INTRO_OVERHEAD_PARTICIPANTS - SORTED DATA FOR VISIBILITY - END ********************")
	ENDIF
	#ENDIF
	
	#IF IS_DEBUG_BUILD
	IF sJobIntroOhData.bDoDebugPrints
		PRINTLN("[JOB_INTRO_OH] - ")
		PRINTLN("[JOB_INTRO_OH] - ------ SORT FOR DISTANCE FROM CAMERA ------")
	ENDIF
	#ENDIF
	
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	// SORT FOR DISTANCE FROM CAMERA.
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	REPEAT sJobIntroOhData.iTotalNumParticipants j
	
		#IF IS_DEBUG_BUILD
		IF sJobIntroOhData.bDoDebugPrints
			PRINTLN("[JOB_INTRO_OH] - ")
			PRINTLN("[JOB_INTRO_OH] - j = ", j)
		ENDIF
		#ENDIF
		
		REPEAT sJobIntroOhData.iTotalNumParticipants i
		
			#IF IS_DEBUG_BUILD
			IF sJobIntroOhData.bDoDebugPrints
				PRINTLN("[JOB_INTRO_OH] - ")
				PRINTLN("[JOB_INTRO_OH] - i = ", i)
			ENDIF
			#ENDIF
			
			IF i >= 0
			AND (i+1) < sJobIntroOhData.iTotalNumParticipants
				
				iTempIndex = sJobIntroOhData.iClosestOnScreenParticipantsToCamera[i]
				iTempIndex2 = sJobIntroOhData.iClosestOnScreenParticipantsToCamera[i+1]
				
				#IF IS_DEBUG_BUILD
				IF sJobIntroOhData.bDoDebugPrints
					PRINTLN("[JOB_INTRO_OH] - iTempIndex = sJobIntroOhData.iClosestOnScreenParticipantsToCamera[", i, "] = ", iTempIndex)
					PRINTLN("[JOB_INTRO_OH] - iTempIndex2 = sJobIntroOhData.iClosestOnScreenParticipantsToCamera[", i+1, "] = ", iTempIndex2)
					PRINTLN("[JOB_INTRO_OH] - [", iTempIndex, "].fDistanceFromCamera = ", sJobIntroOhData.sJobIntroOhParticipantData[iTempIndex].fDistanceFromCamera)
					PRINTLN("[JOB_INTRO_OH] - [", iTempIndex2, "].fDistanceFromCamera = ", sJobIntroOhData.sJobIntroOhParticipantData[iTempIndex2].fDistanceFromCamera)
				ENDIF
				#ENDIF
				
				IF sJobIntroOhData.sJobIntroOhParticipantData[iTempIndex].bVisibleOnScreen
				AND sJobIntroOhData.sJobIntroOhParticipantData[iTempIndex2].bVisibleOnScreen
					
					#IF IS_DEBUG_BUILD
					IF sJobIntroOhData.bDoDebugPrints
						PRINTLN("[JOB_INTRO_OH] - [", iTempIndex, "].bVisibleOnScreen = TRUE.")
						PRINTLN("[JOB_INTRO_OH] - [", iTempIndex2, "].bVisibleOnScreen = TRUE.")
					ENDIF
					#ENDIF
					
					IF (sJobIntroOhData.sJobIntroOhParticipantData[iTempIndex].fDistanceFromCamera < sJobIntroOhData.sJobIntroOhParticipantData[iTempIndex2].fDistanceFromCamera)
						#IF IS_DEBUG_BUILD
						IF sJobIntroOhData.bDoDebugPrints
							PRINTLN("[JOB_INTRO_OH] - [", iTempIndex, "].fDistanceFromCamera = ", sJobIntroOhData.sJobIntroOhParticipantData[iTempIndex].fDistanceFromCamera, " and [", iTempIndex2, "].fDistanceFromCamera = ", sJobIntroOhData.sJobIntroOhParticipantData[iTempIndex2].fDistanceFromCamera, " - swapping data.")
						ENDIF
						#ENDIF
						iTempIndex2 = sJobIntroOhData.iClosestOnScreenParticipantsToCamera[i]
						sJobIntroOhData.iClosestOnScreenParticipantsToCamera[i] = sJobIntroOhData.iClosestOnScreenParticipantsToCamera[i+1]
						sJobIntroOhData.iClosestOnScreenParticipantsToCamera[i+1] = iTempIndex2
					ELSE
						#IF IS_DEBUG_BUILD
						IF sJobIntroOhData.bDoDebugPrints
							PRINTLN("[JOB_INTRO_OH] - [", iTempIndex, "].fDistanceFromCamera = ", sJobIntroOhData.sJobIntroOhParticipantData[iTempIndex].fDistanceFromCamera, ", [", iTempIndex2, "].fDistanceFromCamera = ", sJobIntroOhData.sJobIntroOhParticipantData[iTempIndex2].fDistanceFromCamera, ", We do not want to swap these.")
						ENDIF
						#ENDIF
					ENDIF
					
				ELSE
					
					#IF IS_DEBUG_BUILD
					IF sJobIntroOhData.bDoDebugPrints
						IF NOT sJobIntroOhData.sJobIntroOhParticipantData[iTempIndex].bVisibleOnScreen
							PRINTLN("[JOB_INTRO_OH] - [", iTempIndex, "].bVisibleOnScreen = FALSE, we do not want to move this further up the list. Not evaluating index.")
						ENDIF
						IF NOT sJobIntroOhData.sJobIntroOhParticipantData[iTempIndex2].bVisibleOnScreen
							PRINTLN("[JOB_INTRO_OH] - [", iTempIndex2, "].bVisibleOnScreen = FALSE, we do not want to move this further up the list. Not evaluating index.")
						ENDIF
					ENDIF
					#ENDIF
					
				ENDIF
				
			ELSE
			
				#IF IS_DEBUG_BUILD
				IF sJobIntroOhData.bDoDebugPrints
					IF i < 0
						PRINTLN("[JOB_INTRO_OH] - i < 0, not perfporming sort on this index.")
					ENDIF
					IF (i+1) >= sJobIntroOhData.iTotalNumParticipants
						PRINTLN("[JOB_INTRO_OH] - i+1 (", i+1, ") >= sJobIntroOhData.iTotalNumParticipants (", sJobIntroOhData.iTotalNumParticipants, "), not performing sort on this index.")
					ENDIF
				ENDIF
				#ENDIF
				
			ENDIF
			
		ENDREPEAT
		
	ENDREPEAT
	
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	// PRINT FINAL SORTED DATA To LOGS.
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	#IF IS_DEBUG_BUILD
	IF sJobIntroOhData.bDoDebugPrints
		INT iIndex
		PRINTLN("[JOB_INTRO_OH] - ")
		PRINTLN("[JOB_INTRO_OH] - ")
		PRINTLN("[JOB_INTRO_OH] - ")
		PRINTLN("[JOB_INTRO_OH] - ******************** SORT_JOB_INTRO_OVERHEAD_PARTICIPANTS - SORTED DATA - START ********************")
		REPEAT sJobIntroOhData.iTotalNumParticipants iIndex
			PRINTLN("[JOB_INTRO_OH] - sJobIntroOhData.iClosestOnScreenParticipantsToCamera[", iIndex, "] (playerId) = ", sJobIntroOhData.iClosestOnScreenParticipantsToCamera[iIndex])
			INT iTemp = sJobIntroOhData.iClosestOnScreenParticipantsToCamera[iIndex]
			PRINTLN("[JOB_INTRO_OH] - sJobIntroOhData.sJobIntroOhParticipantData[", iTemp, "].iParticipantId = ", sJobIntroOhData.sJobIntroOhParticipantData[iTemp].iParticipantId)
			PRINTLN("[JOB_INTRO_OH] - sJobIntroOhData.sJobIntroOhParticipantData[", iTemp, "].iPlayerId = ", sJobIntroOhData.sJobIntroOhParticipantData[iTemp].iPlayerId)
			PRINTLN("[JOB_INTRO_OH] - sJobIntroOhData.sJobIntroOhParticipantData[", iTemp, "].vIntroPosition = ", sJobIntroOhData.sJobIntroOhParticipantData[iTemp].vIntroPosition)
			PRINTLN("[JOB_INTRO_OH] - sJobIntroOhData.sJobIntroOhParticipantData[", iTemp, "].bVisibleOnScreen = ", sJobIntroOhData.sJobIntroOhParticipantData[iTemp].bVisibleOnScreen)
			PRINTLN("[JOB_INTRO_OH] - sJobIntroOhData.sJobIntroOhParticipantData[", iTemp, "].tl31Name = ", sJobIntroOhData.sJobIntroOhParticipantData[iTemp].tl31Name)
			PRINTLN("[JOB_INTRO_OH] - sJobIntroOhData.sJobIntroOhParticipantData[", iTemp, "].fDistanceFromCamera = ", sJobIntroOhData.sJobIntroOhParticipantData[iTemp].fDistanceFromCamera)
		ENDREPEAT
		PRINTLN("[JOB_INTRO_OH] - ******************** SORT_JOB_INTRO_OVERHEAD_PARTICIPANTS - SORTED DATA - END ********************")
	ENDIF
	#ENDIF
	
ENDPROC

PROC MAINTAIN_DRAW_JOB_INTRO_OVERHEAD_PARTICIPANTS(STRUCT_JOB_INTRO_OVERHEADS_DATA &sJobIntroOhData)
	
	INT i, iIndex, iTag
	BOOL bDisplayTag
	
	#IF IS_DEBUG_BUILD
	IF sJobIntroOhData.bDoDebugPrints
		PRINTLN("[JOB_INTRO_OH] - ")
		PRINTLN("[JOB_INTRO_OH] - ")
		PRINTLN("[JOB_INTRO_OH] - ")
		PRINTLN("[JOB_INTRO_OH] - ")
		PRINTLN("[JOB_INTRO_OH] - ")
		PRINTLN("[JOB_INTRO_OH] - ******************** MAINTAIN_DRAW_JOB_INTRO_OVERHEAD_PARTICIPANTS - START ******************** iTotalNumParticipants = ", sJobIntroOhData.iTotalNumParticipants)
	ENDIF
	#ENDIF
	
	FOR i = (sJobIntroOhData.iTotalNumParticipants - 1) TO 0 STEP -1
	//REPEAT 	sJobIntroOhData.iTotalNumParticipants i
		
		iIndex = sJobIntroOhData.iClosestOnScreenParticipantsToCamera[i]
		bDisplayTag = FALSE
		
		#IF IS_DEBUG_BUILD
		IF sJobIntroOhData.bDoDebugPrints
			PRINTLN("[JOB_INTRO_OH] - ---------- iIndex = sJobIntroOhData.iClosestOnScreenParticipantsToCamera[", i, "] = ", iIndex, " ----------")
			PRINTLN("[JOB_INTRO_OH] - sJobIntroOhData.sJobIntroOhParticipantData[", iIndex, "].iParticipantId = ", sJobIntroOhData.sJobIntroOhParticipantData[iIndex].iParticipantId)
			PRINTLN("[JOB_INTRO_OH] - sJobIntroOhData.sJobIntroOhParticipantData[", iIndex, "].iPlayerId = ", sJobIntroOhData.sJobIntroOhParticipantData[iIndex].iPlayerId)
			PRINTLN("[JOB_INTRO_OH] - sJobIntroOhData.sJobIntroOhParticipantData[", iIndex, "].vIntroPosition = ", sJobIntroOhData.sJobIntroOhParticipantData[iIndex].vIntroPosition)
			PRINTLN("[JOB_INTRO_OH] - sJobIntroOhData.sJobIntroOhParticipantData[", iIndex, "].bVisibleOnScreen = ", sJobIntroOhData.sJobIntroOhParticipantData[iIndex].bVisibleOnScreen)
			PRINTLN("[JOB_INTRO_OH] - sJobIntroOhData.sJobIntroOhParticipantData[", iIndex, "].tl31Name = ", sJobIntroOhData.sJobIntroOhParticipantData[iIndex].tl31Name)
			PRINTLN("[JOB_INTRO_OH] - sJobIntroOhData.sJobIntroOhParticipantData[", iIndex, "].fDistanceFromCamera = ", sJobIntroOhData.sJobIntroOhParticipantData[iIndex].fDistanceFromCamera)
		ENDIF
		#ENDIF
		
		IF iIndex != (-1)
			
			#IF IS_DEBUG_BUILD
			IF sJobIntroOhData.bDoDebugPrints
				PRINTLN("[JOB_INTRO_OH] - iIndex != (-1)")
			ENDIF
			#ENDIF
			
			IF sJobIntroOhData.sJobIntroOhParticipantData[iIndex].iPlayerId != (-1)
			
				#IF IS_DEBUG_BUILD
				IF sJobIntroOhData.bDoDebugPrints
					PRINTLN("[JOB_INTRO_OH] - sJobIntroOhData.iClosestOnScreenParticipantsToCamera[", i, "].iPlayerId = ", sJobIntroOhData.sJobIntroOhParticipantData[iIndex].iPlayerId)
				ENDIF
				#ENDIF
				
//				IF sJobIntroOhData.sJobIntroOhParticipantData[iIndex].iParticipantId != PARTICIPANT_ID_TO_INT()
//				
//					#IF IS_DEBUG_BUILD
//					IF sJobIntroOhData.bDoDebugPrints
//						PRINTLN("[JOB_INTRO_OH] - sJobIntroOhData.iClosestOnScreenParticipantsToCamera[", i, "].iParticipantId = ", sJobIntroOhData.sJobIntroOhParticipantData[iIndex].iParticipantId, " != PARTICIPANT_ID_TO_INT")
//					ENDIF
//					#ENDIF
					
					IF NOT sJobIntroOhData.bOverheadsButtonToggle
					OR HAS_NET_TIMER_STARTED(sJobIntroOhData.sJobIntroOhParticipantData[iIndex].talkingTimer)
					
						#IF IS_DEBUG_BUILD
						IF sJobIntroOhData.bDoDebugPrints
							IF NOT sJobIntroOhData.bOverheadsButtonToggle
								PRINTLN("[JOB_INTRO_OH] - sJobIntroOhData.bOverheadsButtonToggle = FALSE")
							ENDIF
							IF HAS_NET_TIMER_STARTED(sJobIntroOhData.sJobIntroOhParticipantData[iIndex].talkingTimer)
								PRINTLN("[JOB_INTRO_OH] - sJobIntroOhData.sJobIntroOhData.sJobIntroOhParticipantData[", iIndex, "].talkingTimer has started")
							ENDIF
						ENDIF
						#ENDIF
	
						IF IS_BIT_SET(sJobIntroOhData.iParticipantNetOkBitSet, sJobIntroOhData.sJobIntroOhParticipantData[iIndex].iPlayerId)
							
							#IF IS_DEBUG_BUILD
							IF sJobIntroOhData.bDoDebugPrints
								PRINTLN("[JOB_INTRO_OH] - sJobIntroOhData.iClosestOnScreenParticipantsToCamera[", iIndex, "].iPlayerId is net ok")
							ENDIF
							#ENDIF
	
							IF sJobIntroOhData.sJobIntroOhParticipantData[iIndex].bVisibleOnScreen
							
								#IF IS_DEBUG_BUILD
								IF sJobIntroOhData.bDoDebugPrints
									PRINTLN("[JOB_INTRO_OH] - sJobIntroOhData.iClosestOnScreenParticipantsToCamera[", iIndex, "].bVisibleOnScreen = TRUE")
								ENDIF
								#ENDIF
								
								IF i >= (sJobIntroOhData.iTotalNumParticipants - 6) // Do top 7 in the list (going from max to 0) and all those who are talking.
								OR HAS_NET_TIMER_STARTED(sJobIntroOhData.sJobIntroOhParticipantData[iIndex].talkingTimer)
									
									#IF IS_DEBUG_BUILD
									IF sJobIntroOhData.bDoDebugPrints
										INT iTemp = (sJobIntroOhData.iTotalNumParticipants - 6)
										PRINTLN("[JOB_INTRO_OH] - i = ", i, ". (sJobIntroOhData.iTotalNumParticipants - 6) = ", iTemp)
										IF HAS_NET_TIMER_STARTED(sJobIntroOhData.sJobIntroOhParticipantData[iIndex].talkingTimer)
											PRINTLN("[JOB_INTRO_OH] - HAS_NET_TIMER_STARTED(sJobIntroOhData.sJobIntroOhParticipantData[", iIndex, "].talkingTimer) = TRUE")
										ENDIF
									ENDIF
									#ENDIF
									
									bDisplayTag = TRUE
									
								ENDIF
							ENDIF
						ENDIF
//					ENDIF
				ENDIF
			
				REPEAT COUNT_OF(eMP_TAG) iTag
				
					#IF IS_DEBUG_BUILD
					IF sJobIntroOhData.bDoDebugPrints
						PRINTLN("[JOB_INTRO_OH] - iTag = ", iTag)
					ENDIF
					#ENDIF
					
					IF IS_MP_GAMER_TAG_MOVIE_ACTIVE()
					
						#IF IS_DEBUG_BUILD
						IF sJobIntroOhData.bDoDebugPrints
							PRINTLN("[JOB_INTRO_OH] - IS_MP_GAMER_TAG_MOVIE_ACTIVE = TRUE")
						ENDIF
						#ENDIF
						
						IF IS_MP_GAMER_TAG_ACTIVE(sJobIntroOhData.sJobIntroOhParticipantData[iIndex].iPlayerId)
						
							#IF IS_DEBUG_BUILD
							IF sJobIntroOhData.bDoDebugPrints
								PRINTLN("[JOB_INTRO_OH] - IS_MP_GAMER_TAG_ACTIVE(sJobIntroOhData.iClosestOnScreenParticipantsToCamera[", i, "].iPlayerId) = TRUE")
							ENDIF
							#ENDIF
							
							IF bDisplayTag
							
								#IF IS_DEBUG_BUILD
								IF sJobIntroOhData.bDoDebugPrints
									PRINTLN("[JOB_INTRO_OH] - bDisplayTag = TRUE")
								ENDIF
								#ENDIF
								
								SET_OVERHEAD_COLOUR_AND_ALPHA(sJobIntroOhData, iIndex #IF IS_DEBUG_BUILD , sJobIntroOhData.bDoDebugPrints #ENDIF)
								
								SWITCH INT_TO_ENUM(eMP_TAG, iTag)
									CASE MP_TAG_AUDIO
										#IF IS_DEBUG_BUILD
										IF sJobIntroOhData.bDoDebugPrints
											PRINTLN("[JOB_INTRO_OH] - INT_TO_ENUM(eMP_TAG, iTag) = MP_TAG_AUDIO")
										ENDIF
										#ENDIF
										IF NETWORK_IS_PLAYER_TALKING(sJobIntroOhData.sJobIntroOhParticipantData[iIndex].playerId)
											#IF IS_DEBUG_BUILD
											IF sJobIntroOhData.bDoDebugPrints
												PRINTLN("[JOB_INTRO_OH] - NETWORK_IS_PLAYER_TALKING(sJobIntroOhData.iClosestOnScreenParticipantsToCamera[", i, "].iPlayerId) = TRUE")
											ENDIF
											#ENDIF
											SET_MP_GAMER_TAG_VISIBILITY(sJobIntroOhData.sJobIntroOhParticipantData[iIndex].iPlayerId, INT_TO_ENUM(eMP_TAG, iTag), TRUE)
											SET_MP_GAMER_TAG_ALPHA(sJobIntroOhData.sJobIntroOhParticipantData[iIndex].iPlayerId, INT_TO_ENUM(eMP_TAG, iTag), UNIVERSAL_HUD_ALPHA)
										ELSE
											#IF IS_DEBUG_BUILD
											IF sJobIntroOhData.bDoDebugPrints
												PRINTLN("[JOB_INTRO_OH] - NETWORK_IS_PLAYER_TALKING(sJobIntroOhData.iClosestOnScreenParticipantsToCamera[", i, "].iPlayerId) = FALSE")
											ENDIF
											#ENDIF
										ENDIF
									BREAK
									CASE MP_TAG_DRIVER
										#IF IS_DEBUG_BUILD
										IF sJobIntroOhData.bDoDebugPrints
											PRINTLN("[JOB_INTRO_OH] - INT_TO_ENUM(eMP_TAG, iTag) = MP_TAG_DRIVER")
										ENDIF
										#ENDIF
										IF IS_ON_RALLY_RACE_GLOBAL_SET()
											#IF IS_DEBUG_BUILD	
											IF sJobIntroOhData.bDoDebugPrints
												PRINTLN("[JOB_INTRO_OH] - IS_ON_RALLY_RACE_GLOBAL_SET() = TRUE")
											ENDIF
											#ENDIF
											IF IS_RACE_DRIVER()
												#IF IS_DEBUG_BUILD
												IF sJobIntroOhData.bDoDebugPrints
													PRINTLN("[JOB_INTRO_OH] - IS_RACE_DRIVER() = TRUE")
												ENDIF
												#ENDIF
												SET_MP_GAMER_TAG_VISIBILITY(sJobIntroOhData.sJobIntroOhParticipantData[iIndex].iPlayerId, INT_TO_ENUM(eMP_TAG, iTag), TRUE)
												SET_MP_GAMER_TAG_ALPHA(sJobIntroOhData.sJobIntroOhParticipantData[iIndex].iPlayerId, INT_TO_ENUM(eMP_TAG, iTag), UNIVERSAL_HUD_ALPHA)
											ELSE
												#IF IS_DEBUG_BUILD
												IF sJobIntroOhData.bDoDebugPrints
													PRINTLN("[JOB_INTRO_OH] - IS_RACE_DRIVER() = FALSE")
												ENDIF
												#ENDIF
											ENDIF
										ENDIF
									BREAK
									CASE MP_TAG_CO_DRIVER
										#IF IS_DEBUG_BUILD
										IF sJobIntroOhData.bDoDebugPrints
											PRINTLN("[JOB_INTRO_OH] - INT_TO_ENUM(eMP_TAG, iTag) = MP_TAG_CO_DRIVER")
										ENDIF
										#ENDIF
										IF IS_ON_RALLY_RACE_GLOBAL_SET()
											#IF IS_DEBUG_BUILD
											IF sJobIntroOhData.bDoDebugPrints
												PRINTLN("[JOB_INTRO_OH] - IS_ON_RALLY_RACE_GLOBAL_SET() = TRUE")
											ENDIF
											#ENDIF
											IF IS_RACE_PASSENGER()
												#IF IS_DEBUG_BUILD
												IF sJobIntroOhData.bDoDebugPrints
													PRINTLN("[JOB_INTRO_OH] - IS_RACE_PASSENGER() = TRUE")
												ENDIF
												#ENDIF
												SET_MP_GAMER_TAG_VISIBILITY(sJobIntroOhData.sJobIntroOhParticipantData[iIndex].iPlayerId, INT_TO_ENUM(eMP_TAG, iTag), TRUE)
												SET_MP_GAMER_TAG_ALPHA(sJobIntroOhData.sJobIntroOhParticipantData[iIndex].iPlayerId, INT_TO_ENUM(eMP_TAG, iTag), UNIVERSAL_HUD_ALPHA)
											ELSE
												#IF IS_DEBUG_BUILD
												IF sJobIntroOhData.bDoDebugPrints
													PRINTLN("[JOB_INTRO_OH] - IS_RACE_PASSENGER() = FALSE")
												ENDIF
												#ENDIF
											ENDIF
										ENDIF
									BREAK
		//							CASE MP_TAG_TYPING
		//								IF IS_MP_TEXT_CHAT_TYPING()
		//									SET_MP_GAMER_TAG_VISIBILITY(sJobIntroOhData.sJobIntroOhParticipantData[iPart].iPlayerId, INT_TO_ENUM(eMP_TAG, iTag), TRUE)
		//									SET_MP_GAMER_TAG_ALPHA(sJobIntroOhData.sJobIntroOhParticipantData[iPart].iPlayerId, INT_TO_ENUM(eMP_TAG, iTag), UNIVERSAL_HUD_ALPHA)
		//								ENDIF
		//							BREAK
								ENDSWITCH
							ELSE
								SET_MP_GAMER_TAG_VISIBILITY(sJobIntroOhData.sJobIntroOhParticipantData[iIndex].iPlayerId, INT_TO_ENUM(eMP_TAG, iTag), FALSE)
								#IF IS_DEBUG_BUILD
								IF sJobIntroOhData.bDoDebugPrints
									PRINTLN("[JOB_INTRO_OH] - bDisplayTag = FALSE")
								ENDIF
								#ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDREPEAT
				
			ENDIF
		ENDIF
		
	ENDFOR
	//ENDREPEAT
	
	PRINTLN("[JOB_INTRO_OH] - ******************** MAINTAIN_DRAW_JOB_INTRO_OVERHEAD_PARTICIPANTS - END ********************")
	PRINTLN("[JOB_INTRO_OH] - ")
	PRINTLN("[JOB_INTRO_OH] - ")
	PRINTLN("[JOB_INTRO_OH] - ")
	PRINTLN("[JOB_INTRO_OH] - ")
	PRINTLN("[JOB_INTRO_OH] - ")
		
ENDPROC

PROC MAINTAIN_JOB_INTRO_OVERHEADS_HELP(STRUCT_JOB_INTRO_OVERHEADS_DATA &sJobIntroOhData)
	
	IF NOT sJobIntroOhData.bShownButtonHelp
		
		INT iNumTimesShownHelp = GET_MP_INT_CHARACTER_STAT(MP_STAT_LOWRIDER_RACE_HELP)
		
		IF iNumTimesShownHelp < NUM_TIMES_TO_SHOW_JOB_INTRO_OVERHEADS_BUTTON_HELP
			IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
			AND OK_FOR_RACE_NAMES(sJobIntroOhData)
				PRINT_HELP("JOBINTOH_1")
				sJobIntroOhData.bShownButtonHelp = TRUE
				SET_MP_INT_CHARACTER_STAT(MP_STAT_LOWRIDER_RACE_HELP, (iNumTimesShownHelp + 1))
				PRINTLN("[JOB_INTRO_OH] - shown help JOBINTOH_1")
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC SWITCH_FROM_JOB_GLOBAL_OVERHEADS_TO_INTRO_OVERHEADS(STRUCT_JOB_INTRO_OVERHEADS_DATA &sJobIntroOhData)
	
	REMOVE_GLOBAL_OVERHEADS()
	MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.bBlockGlobalOverheadsForJobIntro = TRUE
	PRINTLN("[JOB_INTRO_OH] - [Overheads] - MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.bBlockGlobalOverheadsForJobIntro = TRUE")
	sJobIntroOhData.eStage = eJOBINTROOVERHEADSSTAGE_SETUP_JOB_INTRO_OVERHEADS
	PRINTLN("[JOB_INTRO_OH] - sJobIntroOhData.eStage = eJOBINTROOVERHEADSSTAGE_SETUP_JOB_INTRO_OVERHEADS")
				
ENDPROC

PROC SWITCH_FROM_JOB_INTRO_OVERHEADS_TO_GLOBAL_OVERHEADS(STRUCT_JOB_INTRO_OVERHEADS_DATA &sJobIntroOhData)
	
	REMOVE_JOB_INTRO_OVERHEADS()
	RESET_JOB_INTRO_OVERHEADS_PARTICIPANT_DATA(sJobIntroOhData)
	MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.bBlockGlobalOverheadsForJobIntro = FALSE
	sJobIntroOhData.eRaceNameState = eRN_IDLE
	PRINTLN("[JOB_INTRO_OH] - [Overheads] - MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.bBlockGlobalOverheadsForJobIntro = FALSE")
	sJobIntroOhData.eStage = eJOBINTROOVERHEADSSTAGE_END
	PRINTLN("[JOB_INTRO_OH] - sJobIntroOhData.eStage = eJOBINTROOVERHEADSSTAGE_END")
	
ENDPROC

PROC MAINTAIN_JOB_INTRO_OVERHEADS(STRUCT_JOB_INTRO_OVERHEADS_DATA &sJobIntroOhData)
	
	#IF IS_DEBUG_BUILD
	IF NOT sJobIntroOhData.bDoDebugPrints
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_doRaceIntroOhPrints")
			sJobIntroOhData.bDoDebugPrints = TRUE
			PRINTLN("[JOB_INTRO_OH] - [Overheads] - sc_doRaceIntroOhPrints commandline exists, setting sJobIntroOhData.bDoDebugPrints = TRUE")
		ENDIF
	ENDIF
	#ENDIF
	
	SWITCH sJobIntroOhData.eStage
		
		CASE eJOBINTROOVERHEADSSTAGE_INIT
			
			IF GET_TOGGLE_PAUSED_RENDERPHASES_STATUS()
				IF IS_MP_GAMER_TAG_MOVIE_ACTIVE()
					IF g_b_OnRaceIntro
						SWITCH_FROM_JOB_GLOBAL_OVERHEADS_TO_INTRO_OVERHEADS(sJobIntroOhData)
					ELSE
						PRINTLN("[JOB_INTRO_OH] - IS_MP_GAMER_TAG_MOVIE_ACTIVE = FALSE. Cannot continue until this has loaded")
					ENDIF
				ELSE
					PRINTLN("[JOB_INTRO_OH] - g_b_OnRaceIntro = FALSE. Cannot continue until this is TRUE")
				ENDIF
			ELSE
				PRINTLN("[JOB_INTRO_OH] - GET_TOGGLE_PAUSED_RENDERPHASES_STATUS = FALSE. Cannot continue until this is TRUE")
			ENDIF
			
		BREAK
		
		CASE eJOBINTROOVERHEADSSTAGE_SETUP_JOB_INTRO_OVERHEADS
			
			IF g_b_OnRaceIntro
				SAVE_JOB_INTRO_PLAYER_DATA(sJobIntroOhData)
				MAINTAIN_JOB_INTRO_OVERHEADS_EVERY_FRAME_DATA(sJobIntroOhData)
				ADD_JOB_INTRO_OVERHEADS(sJobIntroOhData)
				SORT_JOB_INTRO_OVERHEAD_PARTICIPANTS(sJobIntroOhData)
				sJobIntroOhData.eStage = eJOBINTROOVERHEADSSTAGE_MAINTAIN_RACE_JOB_INTRO
				PRINTLN("[JOB_INTRO_OH] - sJobIntroOhData.eStage = eJOBINTROOVERHEADSSTAGE_MAINTAIN_RACE_JOB_INTRO")
			ELSE
				PRINTLN("[JOB_INTRO_OH] - g_b_OnRaceIntro = FALSE")
				SWITCH_FROM_JOB_INTRO_OVERHEADS_TO_GLOBAL_OVERHEADS(sJobIntroOhData)
			ENDIF
			
		BREAK
		
		CASE eJOBINTROOVERHEADSSTAGE_MAINTAIN_RACE_JOB_INTRO
			
			IF g_b_OnRaceIntro
				MAINTAIN_JOB_INTRO_OVERHEADS_BUTTON_TOGGLE(sJobIntroOhData)
				MAINTAIN_JOB_INTRO_OVERHEADS_EVERY_FRAME_DATA(sJobIntroOhData)
				SORT_JOB_INTRO_OVERHEAD_PARTICIPANTS(sJobIntroOhData)
				MAINTAIN_DRAW_JOB_INTRO_OVERHEAD_PARTICIPANTS(sJobIntroOhData)
				MAINTAIN_JOB_INTRO_OVERHEADS_HELP(sJobIntroOhData)
			ELSE
				PRINTLN("[JOB_INTRO_OH] - g_b_OnRaceIntro = FALSE")
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("JOBINTOH_1")
					CLEAR_HELP()
					PRINTLN("[JOB_INTRO_OH] - cleared help JOBINTOH_1")
				ENDIf
				SWITCH_FROM_JOB_INTRO_OVERHEADS_TO_GLOBAL_OVERHEADS(sJobIntroOhData)
			ENDIF
			
		BREAK
		
		CASE eJOBINTROOVERHEADSSTAGE_END
			// Done.
		BREAK
		
	ENDSWITCH
	
ENDPROC

#IF IS_DEBUG_BUILD
PROC ADD_JOB_INTRO_OVERHEADS_WIDGETS(STRUCT_JOB_INTRO_OVERHEADS_DATA &sJobIntroOhData)
	
	START_WIDGET_GROUP("Jon Intro Overheads")
		ADD_WIDGET_BOOL("DoEveryFrameDebugPrints", sJobIntroOhData.bDoDebugPrints)
	STOP_WIDGET_GROUP()
	
ENDPROC
#ENDIF

























