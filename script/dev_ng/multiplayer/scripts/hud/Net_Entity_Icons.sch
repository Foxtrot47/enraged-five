


USING "Commands_Hud.sch"
USING "net_hud_activating.sch"
USING "Net_include.sch"
USING "Transition_Controller.sch"
USING "Transition_Crews.sch"
USING "Weapons_public.sch"
USING "Net_Entity_Controller.sch"
USING "net_common_functions.sch"
USING "Net_Above_Head_Display.sch"
USING "net_blips.sch"
USING "net_betting_common.sch"
USING "fm_post_mission_cleanup_public.sch"
USING "stripclub_public.sch"

#IF IS_DEBUG_BUILD
USING "Profiler.sch"
#ENDIF

CONST_FLOAT ARROW_DISTANCE	30.0
CONST_FLOAT VEH_DM_ARROW_DISTANCE 100.0
CONST_FLOAT PENNED_IN_SPECTATOR_ARROW_DISTANCE 400.0

CONST_INT MARKER_OPTIONS_FULL				0
CONST_INT MARKER_OPTIONS_FULL_WITH_CREWTAG	1
CONST_INT MARKER_OPTIONS_ARROW_ONLY			2
CONST_INT MARKER_OPTIONS_OFF				3

FUNC BOOL SCALEFORMXML_RANKBAR_RUNNING_ICONS()


	IF IS_SCRIPTED_HUD_COMPONENT_ACTIVE(HUD_MP_RANK_BAR)
	AND HAS_SCALEFORM_SCRIPT_HUD_MOVIE_LOADED(HUD_MP_RANK_BAR)
		PRINTLN("[overheads] - RANKBAR_RUNNING_ICONS( DISPLAYING - return TRUE ")
		RETURN TRUE
	ENDIF

	IF IS_SCRIPTED_HUD_COMPONENT_HIDDEN_THIS_FRAME(HUD_MP_RANK_BAR)
	OR IS_SCRIPT_HUD_DISABLED(HUDPART_RANKBAR)
	
		IF IS_SCRIPTED_HUD_COMPONENT_HIDDEN_THIS_FRAME(HUD_MP_RANK_BAR)
			PRINTLN("[overheads] - RANKBAR_RUNNING_ICONS( HIDING COMPONENT_HIDDEN_THIS_FRAME - return TRUE ")
		ENDIF
		IF IS_SCRIPT_HUD_DISABLED(HUDPART_RANKBAR)
			PRINTLN("[overheads] - RANKBAR_RUNNING_ICONS( HIDEN HUDPART_RANKBAR - return TRUE ")
		ENDIF
		RETURN TRUE
	ENDIF

	
	RETURN TRUE
		
ENDFUNC

FUNC BOOL IS_ARENA_WAR_UGC_RACE()
	IF NOT IS_THIS_MISSION_ROCKSTAR_CREATED()
	AND  IS_ARENA_RACE()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC STRING GET_DISPLAYOVERHEAD_STRING(DISPLAYOVERHEADENUM anEnum)

	SWITCH anEnum
		CASE DISPLAYOVERHEADS_DISPLAY_NOTHING RETURN "DISPLAYOVERHEADS_DISPLAY_NOTHING"
		CASE DISPLAYOVERHEADS_DISPLAY_PLAYER_NAME RETURN "DISPLAYOVERHEADS_DISPLAY_PLAYER_NAME"
		CASE DISPLAYOVERHEADS_DISPLAY_WANTEDLEVEL RETURN "DISPLAYOVERHEADS_DISPLAY_WANTEDLEVEL"
		CASE DISPLAYOVERHEADS_DISPLAY_HEALTHBAR RETURN "DISPLAYOVERHEADS_DISPLAY_HEALTHBAR"
		CASE DISPLAYOVERHEADS_DISPLAY_CLAN RETURN "DISPLAYOVERHEADS_DISPLAY_CLAN"
		CASE DISPLAYOVERHEADS_DISPLAY_VOTED_TICK RETURN "DISPLAYOVERHEADS_DISPLAY_VOTED_TICK"
		CASE DISPLAYOVERHEADS_DISPLAY_VOTED_CROSS RETURN "DISPLAYOVERHEADS_DISPLAY_VOTED_CROSS"
		CASE DISPLAYOVERHEADS_DISPLAY_AUDIO RETURN "DISPLAYOVERHEADS_DISPLAY_AUDIO"
		CASE DISPLAYOVERHEADS_DISPLAY_INVENTORY RETURN "DISPLAYOVERHEADS_DISPLAY_INVENTORY"
		CASE DISPLAYOVERHEADS_DISPLAY_DRUGS_COKE RETURN "DISPLAYOVERHEADS_DISPLAY_DRUGS_COKE"
		CASE DISPLAYOVERHEADS_DISPLAY_DRUGS_METH RETURN "DISPLAYOVERHEADS_DISPLAY_DRUGS_METH"
		CASE DISPLAYOVERHEADS_DISPLAY_DRUGS_WEED RETURN "DISPLAYOVERHEADS_DISPLAY_DRUGS_WEED"
		CASE DISPLAYOVERHEADS_DISPLAY_DRUGS_HEROIN RETURN "DISPLAYOVERHEADS_DISPLAY_DRUGS_HEROIN"
		CASE DISPLAYOVERHEADS_DISPLAY_ISVISIBLE RETURN "DISPLAYOVERHEADS_DISPLAY_ISVISIBLE"
		CASE DISPLAYOVERHEADS_DISPLAY_CHAT_WINDOW RETURN "DISPLAYOVERHEADS_DISPLAY_CHAT_WINDOW"
		CASE DISPLAYOVERHEADS_DISPLAY_MC_ROLE	RETURN "DISPLAYOVERHEADS_DISPLAY_MC_ROLE"
		CASE DISPLAYOVERHEADS_DISPLAY_PACKAGE_TRANSMITTOR RETURN "DISPLAYOVERHEADS_DISPLAY_PACKAGE_TRANSMITTOR"
	ENDSWITCH

	RETURN ""
ENDFUNC


PROC SET_PLAYER_BETTING_STRING(INT iPlayernum)

	INT OddNum, OddDenom
	IF iPlayernum > -1
		GET_PLAYER_BETTING_ODDS(GlobalServerBD_Betting.playerBettingData[iPlayernum].iTeam , OddNum, OddDenom)	
	ENDIF	
		TEXT_LABEL_31 tlBet
		tlBet = OddNum
		tlBet += "/"
		tlBet += OddDenom
		
		PRINTLN("[overheads] - <<<<< SET_PLAYER_BETTING_STRING = ", tlBet, "  >>>>>> ")
		
		SET_MP_GAMER_TAG_BIG_TEXT(iPlayernum, tlBet)
	

ENDPROC


PROC SET_PLAYER_CUSTOM_STRING(INT iPlayernum)

	SET_MP_GAMER_TAG_BIG_TEXT(iPlayernum,g_Overhead_CustomString.tl_customString[iPlayernum] )

ENDPROC


FUNC BOOL IS_OVERHEAD_ACTIVE(DISPLAYOVERHEADENUM anEnum,  PLAYER_INDEX aPlayer)
	//IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(aPlayer)
		RETURN IS_BIT_SET(MPGlobalsHud.DisplayInfo.iOverheadDisplayBitset[GET_WEAPON_BITSET(ENUM_TO_INT(anEnum))][NATIVE_TO_INT(aPlayer)], GET_WEAPON_INDEX_BITSET(ENUM_TO_INT(anEnum)))
	//ENDIF
	RETURN FALSE
ENDFUNC

PROC SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADENUM anEnum, BOOL setBit,  PLAYER_INDEX aPlayer)		
	//IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(aPlayer)
		IF setBit
			IF NOT IS_BIT_SET(MPGlobalsHud.DisplayInfo.iOverheadDisplayBitset[GET_WEAPON_BITSET(ENUM_TO_INT(anEnum))][NATIVE_TO_INT(aPlayer)], GET_WEAPON_INDEX_BITSET(ENUM_TO_INT(anEnum)))
				#IF IS_DEBUG_BUILD
				IF g_TurnOnOverheadDebug
					IF IS_NET_PLAYER_OK(aPlayer)
						DEBUG_PRINTCALLSTACK()
						PRINTLN("[overheads] - SET_OVERHEAD_ACTIVE - SET_BIT is called with ", GET_DISPLAYOVERHEAD_STRING(anEnum))
						PRINTLN("[overheads] -  for player ", NATIVE_TO_INT(aPlayer), " - ")
						PRINTLN("[overheads] -  for player ", GET_PLAYER_NAME(aPlayer), " - ")
						PRINTLN("[overheads] - ", GET_PLAYER_NAME(aPlayer), " - called by ", GET_THIS_SCRIPT_NAME())
					ENDIF
				ENDIF
				#ENDIF
				SET_BIT(MPGlobalsHud.DisplayInfo.iOverheadDisplayBitset[GET_WEAPON_BITSET(ENUM_TO_INT(anEnum))][NATIVE_TO_INT(aPlayer)], GET_WEAPON_INDEX_BITSET(ENUM_TO_INT(anEnum)))
			ENDIF
		ELSE
			IF  IS_BIT_SET(MPGlobalsHud.DisplayInfo.iOverheadDisplayBitset[GET_WEAPON_BITSET(ENUM_TO_INT(anEnum))][NATIVE_TO_INT(aPlayer)], GET_WEAPON_INDEX_BITSET(ENUM_TO_INT(anEnum)))

				#IF IS_DEBUG_BUILD
				IF g_TurnOnOverheadDebug
					DEBUG_PRINTCALLSTACK()
					PRINTLN("[overheads] - CLEAR_BIT is called with ", GET_DISPLAYOVERHEAD_STRING(anEnum), " for player ", NATIVE_TO_INT(aPlayer), " - ", GET_PLAYER_NAME(aPlayer), " - called by ", GET_THIS_SCRIPT_NAME())
				ENDIF
				#ENDIF
				CLEAR_BIT(MPGlobalsHud.DisplayInfo.iOverheadDisplayBitset[GET_WEAPON_BITSET(ENUM_TO_INT(anEnum))][NATIVE_TO_INT(aPlayer)], GET_WEAPON_INDEX_BITSET(ENUM_TO_INT(anEnum)))
			ENDIF
		ENDIF
	//ENDIF

ENDPROC


PROC RESET_OVERHEAD_BITSET(PLAYER_INDEX aPlayer)
		
INT I

	//IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(aPlayer)
		#IF IS_DEBUG_BUILD
		IF g_TurnOnOverheadDebug
			PRINTLN("[overheads] - RESET_OVERHEAD_BITSET called on player ", NATIVE_TO_INT(aPlayer))
		ENDIF
		#ENDIF
		FOR I = 0 TO OH_BITSET_NUM-1
			 MPGlobalsHud.DisplayInfo.iOverheadDisplayBitset[I][NATIVE_TO_INT(aPlayer)] = 0	
		ENDFOR
	//ENDIF
		
ENDPROC





FUNC BOOL IS_OVERHEAD_ACTIVE_LAST_FRAME(DISPLAYOVERHEADENUM anEnum,  PLAYER_INDEX aPlayer)
	//IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(aPlayer)

		RETURN IS_BIT_SET(MPGlobalsHud.DisplayInfo.iOverheadDisplayBitset_LastFrame[GET_WEAPON_BITSET(ENUM_TO_INT(anEnum))][NATIVE_TO_INT(aPlayer)], GET_WEAPON_INDEX_BITSET(ENUM_TO_INT(anEnum)))
	//ENDIF
	RETURN FALSE
ENDFUNC

PROC SET_OVERHEAD_ACTIVE_LAST_FRAME(DISPLAYOVERHEADENUM anEnum, BOOL setBit, PLAYER_INDEX aPlayer)		
	//IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(aPlayer)

		IF setBit
			SET_BIT(MPGlobalsHud.DisplayInfo.iOverheadDisplayBitset_LastFrame[GET_WEAPON_BITSET(ENUM_TO_INT(anEnum))][NATIVE_TO_INT(aPlayer)], GET_WEAPON_INDEX_BITSET( ENUM_TO_INT(anEnum)))
		ELSE
			CLEAR_BIT(MPGlobalsHud.DisplayInfo.iOverheadDisplayBitset_LastFrame[GET_WEAPON_BITSET(ENUM_TO_INT(anEnum))][NATIVE_TO_INT(aPlayer)], GET_WEAPON_INDEX_BITSET( ENUM_TO_INT(anEnum)))
		ENDIF
	//ENDIF
	
ENDPROC

PROC UPDATE_OVERHEAD_LAST_FRAME(PLAYER_INDEX aPlayer)
	INT I
	//IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(aPlayer)

		FOR I = 0 TO OH_BITSET_NUM-1
			
			MPGlobalsHud.DisplayInfo.iOverheadDisplayBitset_LastFrame[I][NATIVE_TO_INT(aPlayer)] = MPGlobalsHud.DisplayInfo.iOverheadDisplayBitset[I][NATIVE_TO_INT(aPlayer)]
		ENDFOR
	//ENDIF
ENDPROC

FUNC BOOL HAS_OVERHEAD_ACTIVATION_CHANGED(PLAYER_INDEX aPlayer)
	INT I
	//IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(aPlayer)

		FOR I = 0 TO OH_BITSET_NUM-1
	//		PRINTLN("[overheads] - "DISPLAY FOR PLAYER ", NATIVE_TO_INT(aPlayer))
	//		PRINTLN("[overheads] - "MPGlobalsHud.DisplayInfo.iOverheadDisplayBitset_LastFrame[I][NATIVE_TO_INT(aPlayer)] = ", MPGlobalsHud.DisplayInfo.iOverheadDisplayBitset_LastFrame[I][NATIVE_TO_INT(aPlayer)], "MPGlobalsHud.DisplayInfo.iOverheadDisplayBitset[I][NATIVE_TO_INT(aPlayer)] = ", MPGlobalsHud.DisplayInfo.iOverheadDisplayBitset[I][NATIVE_TO_INT(aPlayer)])
			IF MPGlobalsHud.DisplayInfo.iOverheadDisplayBitset_LastFrame[I][NATIVE_TO_INT(aPlayer)] <> MPGlobalsHud.DisplayInfo.iOverheadDisplayBitset[I][NATIVE_TO_INT(aPlayer)]
				RETURN TRUE
			ENDIF
		ENDFOR
	//ENDIF

	RETURN FALSE
ENDFUNC

PROC REFRESH_OVERHEAD_DISPLAY(PLAYER_INDEX aPlayer)
	INT i
	FOR i = 0 TO OH_BITSET_NUM-1
		MPGlobalsHud.DisplayInfo.iOverheadDisplayBitset_LastFrame[i][NATIVE_TO_INT(aPlayer)] = 0
	ENDFOR
ENDPROC


PROC RESET_ALL_OVERHEAD_BITSETS()
	
	#IF IS_DEBUG_BUILD
	IF g_TurnOnOverheadDebugFullOn
		PRINTLN("[overheads] - RESET_ALL_OVERHEAD_BITSETS - Called")
		DEBUG_PRINTCALLSTACK()
	ENDIF
	#ENDIF
	
	INT I, J
	FOR J = 0 TO NUM_NETWORK_PLAYERS-1
		FOR I = 0 TO OH_BITSET_NUM-1
			MPGlobalsHud.DisplayInfo.iOverheadDisplayBitset_LastFrame[I][J] = 0
			MPGlobalsHud.DisplayInfo.iOverheadDisplayBitset[I][J] = 0
		ENDFOR
		FOR I = 0 TO OH_BITSET_LOGIC_NUM-1
			MPGlobalsHud.DisplayInfo.iOverheadLogicBitset_LastFrame[I][J] = 0
			MPGlobalsHud.DisplayInfo.iOverheadLogicBitset[I][J] = 0
		ENDFOR
		FOR I = 0 TO OH_BITSET_EVENT_NUM-1
			MPGlobalsHud.DisplayInfo.iOverheadEVENTBitset_LastFrame[I][J] = 0
			MPGlobalsHud.DisplayInfo.iOverheadEVENTBitset[I][J] = 0
			PRINTLN("[Overheads] - [Passive Debug] - [iOverheadEVENTBitset] - iOverheadEVENTBitset been zeroed.")
		ENDFOR
		
	ENDFOR
	
ENDPROC








FUNC BOOL IS_OVERHEAD_LOGIC_LAST_FRAME(DISPLAYOVERHEADS_LOGIC anEnum, PLAYER_INDEX aPlayer)
	IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(aPlayer)
		RETURN IS_BIT_SET(MPGlobalsHud.DisplayInfo.iOverheadLogicBitset_LastFrame[(GET_WEAPON_BITSET(ENUM_TO_INT(anEnum)))][NATIVE_TO_INT(aPlayer)], GET_WEAPON_INDEX_BITSET(ENUM_TO_INT(anEnum)))
	ENDIF
	RETURN FALSE
ENDFUNC

PROC SET_OVERHEAD_LOGIC_LAST_FRAME(DISPLAYOVERHEADS_LOGIC anEnum, BOOL setBit, PLAYER_INDEX aPlayer)
	IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(aPlayer)
		IF setBit
			SET_BIT(MPGlobalsHud.DisplayInfo.iOverheadLogicBitset_LastFrame[(GET_WEAPON_BITSET(ENUM_TO_INT(anEnum)) )][NATIVE_TO_INT(aPlayer)], GET_WEAPON_INDEX_BITSET( ENUM_TO_INT(anEnum)))
		ELSE
			CLEAR_BIT(MPGlobalsHud.DisplayInfo.iOverheadLogicBitset_LastFrame[(GET_WEAPON_BITSET(ENUM_TO_INT(anEnum)) )][NATIVE_TO_INT(aPlayer)], GET_WEAPON_INDEX_BITSET( ENUM_TO_INT(anEnum)))
		ENDIF
	ENDIF
	
ENDPROC

PROC UPDATE_OVERHEAD_LOGIC_LAST_FRAME(PLAYER_INDEX aPlayer)
	INT I
	IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(aPlayer)
		FOR I = 0 TO OH_BITSET_LOGIC_NUM-1
			MPGlobalsHud.DisplayInfo.iOverheadLogicBitset_LastFrame[I][NATIVE_TO_INT(aPlayer)] = MPGlobalsHud.DisplayInfo.iOverheadLogicBitset[I][NATIVE_TO_INT(aPlayer)]		
		ENDFOR
	ENDIF
ENDPROC

FUNC BOOL HAS_OVERHEAD_LOGIC_CHANGED(PLAYER_INDEX aPlayer)
	INT I
	IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(aPlayer)	
		FOR I = 0 TO OH_BITSET_LOGIC_NUM-1		
			IF MPGlobalsHud.DisplayInfo.iOverheadLogicBitset_LastFrame[I][NATIVE_TO_INT(aPlayer)] <> MPGlobalsHud.DisplayInfo.iOverheadLogicBitset[I][NATIVE_TO_INT(aPlayer)]
				RETURN TRUE
			ENDIF	
		ENDFOR
	ENDIF

	RETURN FALSE
ENDFUNC

PROC REFRESH_OVERHEAD_LOGIC(PLAYER_INDEX aPlayer)
	INT I
	IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(aPlayer)
		PRINTLN("[overheads] - REFRESH_OVERHEAD_LOGIC called on player ", NATIVE_TO_INT(aPlayer))
		FOR I = 0 TO OH_BITSET_LOGIC_NUM-1
			MPGlobalsHud.DisplayInfo.iOverheadLogicBitset_LastFrame[I][NATIVE_TO_INT(aPlayer)] = 0
		ENDFOR
	ENDIF
ENDPROC







FUNC BOOL IS_OVERHEAD_EVENT_LAST_FRAME_ACTIVE(DISPLAYOVERHEADS_EVENTS anEnum, PLAYER_INDEX aPlayer)
	IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(aPlayer)
		RETURN IS_BIT_SET(MPGlobalsHud.DisplayInfo.iOverheadEVENTBitset_LastFrame[GET_WEAPON_BITSET(ENUM_TO_INT(anEnum))][NATIVE_TO_INT(aPlayer)], GET_WEAPON_INDEX_BITSET(ENUM_TO_INT(anEnum)))
	ENDIF
	RETURN FALSE
ENDFUNC

PROC SET_OVERHEAD_EVENT_LAST_FRAME_ACTIVE(DISPLAYOVERHEADS_EVENTS anEnum, BOOL setBit, PLAYER_INDEX aPlayer)
	IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(aPlayer)
		IF setBit
			SET_BIT(MPGlobalsHud.DisplayInfo.iOverheadEVENTBitset_LastFrame[GET_WEAPON_BITSET(ENUM_TO_INT(anEnum))][NATIVE_TO_INT(aPlayer)], GET_WEAPON_INDEX_BITSET(ENUM_TO_INT(anEnum)))
		ELSE
			CLEAR_BIT(MPGlobalsHud.DisplayInfo.iOverheadEVENTBitset_LastFrame[GET_WEAPON_BITSET(ENUM_TO_INT(anEnum))][NATIVE_TO_INT(aPlayer)], GET_WEAPON_INDEX_BITSET(ENUM_TO_INT(anEnum)))
		ENDIF
	ENDIF
	
	
ENDPROC


PROC UPDATE_OVERHEAD_EVENT_LAST_FRAME(PLAYER_INDEX aPlayer)
	INT I
	IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(aPlayer)
		FOR I = 0 TO OH_BITSET_LOGIC_NUM-1
			MPGlobalsHud.DisplayInfo.iOverheadEVENTBitset_LastFrame[I][NATIVE_TO_INT(aPlayer)] = MPGlobalsHud.DisplayInfo.iOverheadEVENTBitset[I][NATIVE_TO_INT(aPlayer)]
		ENDFOR
	ENDIF
ENDPROC

FUNC BOOL HAS_OVERHEAD_EVENT_CHANGED(PLAYER_INDEX aPlayer)
	INT I
	IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(aPlayer)
		FOR I = 0 TO OH_BITSET_LOGIC_NUM-1
			IF MPGlobalsHud.DisplayInfo.iOverheadEVENTBitset_LastFrame[I][NATIVE_TO_INT(aPlayer)] <> MPGlobalsHud.DisplayInfo.iOverheadEVENTBitset[I][NATIVE_TO_INT(aPlayer)]
				RETURN TRUE
			ENDIF
		ENDFOR
	ENDIF

	RETURN FALSE
ENDFUNC

PROC REFRESH_OVERHEAD_EVENT(PLAYER_INDEX aPlayer)
	INT I
	
	IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(aPlayer)
		FOR I = 0 TO OH_BITSET_EVENT_NUM-1
			PRINTLN("[overheads] - REFRESH_OVERHEAD_EVENT called on player ", NATIVE_TO_INT(aPlayer))
			MPGlobalsHud.DisplayInfo.iOverheadEVENTBitset_LastFrame[I][NATIVE_TO_INT(aPlayer)] = 0
		ENDFOR
	ENDIF
ENDPROC




PROC INITIALISE_PLAYERS_OHD_EVENT_STATE( PLAYER_INDEX aPlayer)
	
	IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(aPlayer)
		IF IS_NET_PLAYER_OK(aPlayer, FALSE, FALSE)
			IF NETWORK_IS_PLAYER_IN_MP_CUTSCENE(aPlayer)
			AND is_player_browsing_items_in_any_shop() = FALSE
				SET_OVERHEAD_EVENT_ACTIVE(DISPLAYOVERHEADS_EVENTS_IN_MP_CUTSCENE, TRUE, aPlayer)
			ENDIF	
			IF IS_PLAYER_WANTED_LEVEL_GREATER(aPlayer, 0) 
				SET_OVERHEAD_EVENT_ACTIVE(DISPLAYOVERHEADS_EVENTS_IS_WANTED, TRUE, aPlayer)
			ENDIF
			IF NETWORK_IS_PLAYER_TALKING(aPlayer) = TRUE
				SET_OVERHEAD_EVENT_ACTIVE(DISPLAYOVERHEADS_EVENTS_TALKING , TRUE, aPlayer)
			ENDIF
			IF GlobalplayerBD_FM_2[NATIVE_TO_INT(aPlayer)].iVoteToUse = -1
				SET_OVERHEAD_EVENT_ACTIVE(DISPLAYOVERHEADS_EVENTS_IS_VOTING ,FALSE, aPlayer)
			ELSE
				SET_OVERHEAD_EVENT_ACTIVE(DISPLAYOVERHEADS_EVENTS_IS_VOTING ,TRUE, aPlayer)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC INITIALISE_PLAYERS_OHD_EVENT_STATE_ALL_PLAYERS()

	INT I
	FOR I = 0 TO NUM_NETWORK_PLAYERS-1
		INITIALISE_PLAYERS_OHD_EVENT_STATE(INT_TO_NATIVE(PLAYER_INDEX, I))
	ENDFOR
	
ENDPROC



ENUM ABOVE_VEHICLE_ICONS
	ABOVE_VEHICLE_ICONS_GANG,
	ABOVE_VEHICLE_ICONS_LOCKED

ENDENUM



ENUM MASKDIRENUM
	MASKDIRENUM_NONE = 0,
	MASKDIRENUM_UP,
	MASKDIRENUM_DOWN
ENDENUM

FUNC BOOL IS_VOTING_TO_DISPLAY(BOOL isVoting)

	IF isVoting
		REQUEST_STREAMED_TEXTURE_DICT("MPMissionEnd")
		IF HAS_STREAMED_TEXTURE_DICT_LOADED("MPMissionEnd")
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN NOT(isVoting)
ENDFUNC


//check to see if player are in the same vote the presumes that IS_PLAYER_VOTING has been called
FUNC BOOL ARE_PLAYERS_IN_SAME_VOTE(INT iPlayerGBD)
	IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(PLAYER_ID())
		RETURN (GlobalplayerBD_FM_2[iPlayerGBD].iVoteToUse = GlobalplayerBD_FM_2[NATIVE_TO_INT(PLAYER_ID())].iVoteToUse)
	ENDIF
	RETURN FALSE
ENDFUNC
//
//FUNC BOOL IS_PLAYER_VOTING()
//	//if the vote ID has been initilised then they are on the same vote
//	IF NATIVE_TO_INT(PLAYER_ID()) > -1
//	AND IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(PLAYER_ID())
//		RETURN (GlobalplayerBD_FM_2[NATIVE_TO_INT(PLAYER_ID())].iVoteToUse != -1)
//	ENDIF
//	RETURN FALSE
//ENDFUNC


/// PURPOSE: Checks if two players are in the same vehicle.
FUNC BOOL ARE_PEDS_IN_SAME_VEHICLE(PED_INDEX pedId1, PED_INDEX pedId2)
	
//	IF NOT IS_NET_PLAYER_OK( playerId1 )
//	OR NOT IS_NET_PLAYER_OK( playerId2 )
//		RETURN FALSE
//	ENDIF
	
	IF pedId1 = pedId2
		RETURN TRUE
	ENDIF

	IF NOT IS_PED_IN_ANY_VEHICLE( pedId1 )
	OR NOT IS_PED_IN_ANY_VEHICLE( pedId2 )
		RETURN FALSE
	ENDIF	

	RETURN GET_VEHICLE_PED_IS_IN(pedId1 ) = GET_VEHICLE_PED_IS_IN(pedId2 )
		
ENDFUNC


/// PURPOSE:
///    Not broadcast, local only. 
/// PARAMS:
///    PlayerId - 
///    aString - 
PROC DISPLAY_PLAYER_OVERHEAD_STRING(PLAYER_INDEX PlayerId, STRING aString)
	STRING aStringFile = GET_FILENAME_FOR_AUDIO_CONVERSATION(aString)
	g_Overhead_CustomString.tl_customString[NATIVE_TO_INT(PlayerId)] = aStringFile
ENDPROC

PROC RESET_PLAYER_OVERHEAD_STRING(PLAYER_INDEX PlayerId)
	g_Overhead_CustomString.tl_customString[NATIVE_TO_INT(PlayerId)] = ""
ENDPROC

FUNC BOOL DOES_PLAYER_HAVE_OVERHEAD(PLAYER_INDEX PlayerId)
	RETURN NOT IS_STRING_NULL_OR_EMPTY(g_Overhead_CustomString.tl_customString[NATIVE_TO_INT(PlayerId)])
ENDFUNC


FUNC BOOL CAN_DISPLAY_OVERHEAD_STATS(PLAYER_INDEX PlayerId)
	
	IF IS_SCRIPT_HUD_DISABLED(HUDPART_ALL_OVERHEADS)
		
		#IF IS_DEBUG_BUILD
		IF g_TurnOnOverheadDebugFullOn
			PRINTLN("[overheads] - CAN_DISPLAY_OVERHEAD_STATS: IS_SCRIPT_HUD_DISABLED(HUDPART_ALL_OVERHEADS) = TRUE for Player ", NATIVE_TO_INT(playerID))
		ENDIF
		#ENDIF
		
		RETURN FALSE
		
	ENDIF
	
	IF IS_SCRIPT_HUD_DISABLED(HUDPART_THISPLAYER_OVERHEADS, NATIVE_TO_INT(PlayerId))
	
		#IF IS_DEBUG_BUILD
			IF g_TurnOnOverheadDebugFullOn
				PRINTLN("[overheads] - CAN_DISPLAY_OVERHEAD_STATS: IS_SCRIPT_HUD_DISABLED(HUDPART_THISPLAYER_OVERHEADS, PlayerId) = TRUE for Player ", NATIVE_TO_INT(playerID))
			ENDIF 
		#ENDIF
		
		RETURN FALSE
		
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_hidempoverheads")
		#IF IS_DEBUG_BUILD
			IF g_TurnOnOverheadDebugFullOn
				PRINTLN("[overheads] - CAN_DISPLAY_OVERHEAD_STATS: GET_COMMANDLINE_PARAM_EXISTS(sc_hidempoverheads) = TRUE")
			ENDIF 
		#ENDIF
		
		RETURN FALSE
		
	ENDIF
	#ENDIF
	
//	IF IS_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_NOTHING, playerId) 
//		#IF IS_DEBUG_BUILD
//			IF g_TurnOnOverheadDebugFullOn
//				PRINTLN("[overheads] - CAN_DISPLAY_OVERHEAD_STATS: IS_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_NOTHING, playerId)  = TRUE for Player ", NATIVE_TO_INT(playerID))
//			ENDIF 
//		#ENDIF
//
//		RETURN FALSE
//
//	ENDIF
	
	RETURN TRUE
	
ENDFUNC








/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////
///    
///    Vehicle and object icon drawing

/// PURPOSE: returns the correct icon texture to use
/// PARAMS:the vehicle to add it too
/// RETURNS:the texture
FUNC STRING GET_VEHICLE_ICON_NAME(VEHICLE_INDEX objectiveveh, ABOVE_VEHICLE_ICONS WhichIcon = ABOVE_VEHICLE_ICONS_GANG)

	//Temp vars
	MODEL_NAMES mnModel 	= GET_ENTITY_MODEL(objectiveveh)		//The model
	BOOL 		bIsPlane 	= IS_THIS_MODEL_A_PLANE(mnModel)		//Is the model a plane
	BOOL 		bIsHeli	 	= IS_THIS_MODEL_A_HELI(mnModel)			//Is the model a heli
	BOOL 		bIsBike	 	= IS_THIS_MODEL_A_BIKE(mnModel)			//Is the model a Bike
	INT iTeam
	
	iTeam = TEAM_FREEMODE
	
	SWITCH WhichIcon
		CASE ABOVE_VEHICLE_ICONS_LOCKED
			SWITCH iTeam
				
				CASE TEAM_FREEMODE
					IF bIsPlane			RETURN "MP_SpecItem_PartnerIcon"
					ELIF bIsHeli		RETURN "MP_SpecItem_PartnerIcon"
					ELIF bIsBike		RETURN "MP_SpecItem_PartnerIcon"
					ENDIF				RETURN "MP_SpecItem_PartnerIcon"
			ENDSWITCH	
		BREAK

		CASE ABOVE_VEHICLE_ICONS_GANG
		
			//Switch the players team
			SWITCH iTeam
				//Cops plane, Heli, car icons
	
				CASE TEAM_FREEMODE
					IF bIsPlane			RETURN "MP_SpecItem_Plane"
					ELIF bIsHeli		RETURN "MP_SpecItem_Heli"
					ELIF bIsBike		RETURN "MP_SpecItem_Bike"
					ENDIF				RETURN "MP_SpecItem_Car"
					
			ENDSWITCH
		BREAK
		
	ENDSWITCH
	//Default
	RETURN ""
ENDFUNC		


FUNC STRING GET_VEHICLE_ICON_TITLE(VEHICLE_INDEX objectiveveh)

	//Temp vars
	MODEL_NAMES mnModel 	= GET_ENTITY_MODEL(objectiveveh)		//The model
	BOOL 		bIsPlane 	= IS_THIS_MODEL_A_PLANE(mnModel)		//Is the model a plane
	BOOL 		bIsHeli	 	= IS_THIS_MODEL_A_HELI(mnModel)			//Is the model a heli
	BOOL 		bIsBike	 	= IS_THIS_MODEL_A_BIKE(mnModel)			//Is the model a Bike


	IF bIsPlane			RETURN "OVHEAD_PLANE"
	ELIF bIsHeli		RETURN "OVHEAD_HELI"
	ELIF bIsBike		RETURN "OVHEAD_BIKE"
	ENDIF				
	//Default
	RETURN ""
ENDFUNC		



//Get the offset form the model
PROC GET_OVERHEAD_VEHICLE_ICON_OFFSET(MODEL_NAMES mnPassed, FLOAT &fX, FLOAT &fY, FLOAT &fZ)
	SWITCH mnPassed
	CASE BANSHEE     			fX = 0.0	fY = -0.17	fZ = 0.01 	BREAK	//Show Room Model
	CASE BUCCANEER   			fX = 0.0	fY = 0.21	fZ = 0.01 	BREAK	//Show Room Model	
	CASE BULLET      			fX = 0.0	fY = 0.1	fZ = 0.01 	BREAK	//Show Room Model
	CASE CAVALCADE				fX = 0.0	fY = 0.41	fZ = 0.01 	BREAK 	//Vagos Gang Car
	CASE CHEETAH     			fX = 0.0	fY = 0.1	fZ = 0.04 	BREAK	//Show Room Model
	CASE COGCABRIO   			fX = 0.0	fY = 0.1	fZ = 0.08 	BREAK	//Show Room Model
	CASE DAEMON					fX = 0.0	fY = -0.55	fZ = -0.26	BREAK 	//Lost Gang car
	CASE DOMINATOR 				fX = 0.0	fY = -0.28	fZ = 0.06 	BREAK	//Show Room Model
	CASE EMPEROR     			fX = 0.0	fY = -0.02	fZ = 0.1 	BREAK	//Show Room Model
	CASE FUSILADE    			fX = 0.0	fY = -0.25	fZ = 0.03 	BREAK	//Show Room Model
	CASE INFERNUS      			fX = 0.0	fY = 0.34	fZ = 0.02 	BREAK	//Show Room Model
	CASE INGOT       			fX = 0.0	fY = 0.03	fZ = 0.02 	BREAK	//Show Room Model
	CASE NINEF       			fX = 0.0	fY = 0.39	fZ = 0.0 	BREAK	//Show Room Model
	CASE PHOENIX     			fX = 0.0	fY = 0.22	fZ = 0.04 	BREAK	//Show Room Model
	CASE POLICE	    			fX = 0.0	fY = 0.32	fZ = -0.05 	BREAK	//Police Vehicle
	CASE REBEL2        			fX = 0.0	fY = 0.39	fZ = 0.09 	BREAK	//Show Room Model
	CASE REGINA      			fX = 0.0	fY = 0.17	fZ = 0.02 	BREAK	//Show Room Model
	CASE SENTINEL  				fX = 0.0	fY = 0.06	fZ = 0.02 	BREAK  	//Show Room Model
	CASE STANIER     			fX = 0.0	fY = 0.110	fZ = 0.02 	BREAK	//Show Room Model
	CASE TORNADO       			fX = 0.0	fY = 0.25	fZ = 0.02 	BREAK	//Show Room Model
	CASE VOLTIC        			fX = 0.0	fY = 0.09	fZ = 0.01 	BREAK	//Show Room Model
	CASE PROP_DRUG_PACKAGE		fX = 0.0	fY = 0.12	fZ = 0.304 	BREAK	//Package model
	DEFAULT						fX = 0.0	fY = 0.12	fZ = 0.02 	BREAK	//The Default
	ENDSWITCH
ENDPROC

///// PURPOSE: returns the correct icon texture to use
///// PARAMS:the icon to use
///// RETURNS:the texture
//FUNC STRING GET_OBJECT_ICON_NAME(eMP_TAG paramTag)
//
//	SWITCH paramTag
//		CASE MP_TAG_INV_VEHICLE			RETURN "MP_SpecItem_Car"
//		CASE MP_TAG_CASH_FROM_SAFE		RETURN "MP_SpecItem_Safe"
//		CASE MP_TAG_CASH_FROM_BANK		RETURN "MP_SpecItem_Cash"
//		CASE MP_TAG_ROLE				RETURN "MP_PlayerRoleIcons_3_Driver" 
//		CASE MP_TAG_KEY_CARD			RETURN "MP_SpecItem_KeyCard"
//		CASE MP_TAG_DOWNLOAD_DATA		RETURN "MP_SpecItem_Data"
//		CASE MP_TAG_COKE_DRUGS			RETURN "MP_SpecItem_Coke"
//		CASE MP_TAG_METH_DRUGS			RETURN "MP_SpecItem_Meth"
//		CASE MP_TAG_WEED				RETURN "MP_SpecItem_Weed"
//		CASE MP_TAG_HEROIN				RETURN "MP_SpecItem_Heroin"
//		CASE MP_TAG_PACKAGES			RETURN "MP_SpecItem_Package"
//		CASE MP_TAG_REMOTE_CONTROL		RETURN "MP_SpecItem_Remote"
//		CASE MP_TAG_WEAPONS_PACKAGE		RETURN "MP_SpecItem_Weapons"
//		CASE MP_TAG_RANDOM_OBJECT		RETURN "MP_SpecItem_RandomObject"
//		CASE MP_TAG_IF_PED_FOLLOWING	RETURN "MP_SpecItem_Ped"
//		CASE MP_TAG_CUFF_KEYS			RETURN "MP_SpecItem_CuffKeys"
//		CASE MP_TAG_HOLDUP_CASH			RETURN "MP_SpecItem_Cash"
//	ENDSWITCH
//
//	RETURN ""
//ENDFUNC

/// PURPOSE: returns the correct icon texture to use
/// PARAMS:the icon to use
/// RETURNS:the texture
FUNC STRING GET_OBJECT_ICON_TITLE(eMP_TAG paramTag)

	SWITCH paramTag
//		CASE MP_TAG_INV_VEHICLE			RETURN "OVHEAD_VEH"
//		CASE MP_TAG_CASH_FROM_SAFE		RETURN "OVHEAD_CASHSAFE"
//		CASE MP_TAG_CASH_FROM_BANK		RETURN "OVHEAD_CASHBANK"
//		CASE MP_TAG_ROLE				RETURN "OVHEAD_ROLE" 
//		CASE MP_TAG_KEY_CARD			RETURN "OVHEAD_KEYCARD"
//		CASE MP_TAG_DOWNLOAD_DATA		RETURN "OVHEAD_DNDATA"
//		CASE MP_TAG_COKE_DRUGS			RETURN "OVHEAD_COKE"
//		CASE MP_TAG_METH_DRUGS			RETURN "OVHEAD_METH"
//		CASE MP_TAG_WEED				RETURN "OVHEAD_WEED"
//		CASE MP_TAG_HEROIN				RETURN "OVHEAD_HERO"
		CASE MP_TAG_PACKAGES			RETURN "OVHEAD_PACKA"
//		CASE MP_TAG_REMOTE_CONTROL		RETURN "OVHEAD_RMCONT"
//		CASE MP_TAG_WEAPONS_PACKAGE		RETURN "OVHEAD_WEAPACKA"
//		CASE MP_TAG_RANDOM_OBJECT		RETURN "OVHEAD_OBJECT"
		CASE MP_TAG_IF_PED_FOLLOWING	RETURN "OVHEAD_PED"
		CASE MP_TAG_PACKAGE_LARGE		RETURN "OVHEAD_PACKA"
//		CASE MP_TAG_CUFF_KEYS			RETURN "OVHEAD_CUFFKY"
//		CASE MP_TAG_HOLDUP_CASH			RETURN "OVHEAD_HOLDCASH"
		CASE MP_TAG_TRACKIFY			RETURN "OVHEAD_TRACK"
		CASE MP_TAG_GANG_CEO			RETURN "OVHEAD_GCEO"
		CASE MP_TAG_GANG_BIKER			RETURN "OVHEAD_GBIKER"
	ENDSWITCH

	
	RETURN ""
ENDFUNC

FUNC STRING GET_CORONA_ICON_TEXTURE(eMP_TAG_SCRIPT aTag)

	SWITCH aTag
		CASE MP_TAG_SCRIPT_ARM_WRESTLING		RETURN "Arm_Wrestling"
		CASE MP_TAG_SCRIPT_BASEJUMP				RETURN "Basejump"
		CASE MP_TAG_SCRIPT_CUSTOM_MISSION		RETURN "Custom_Mission"
		CASE MP_TAG_SCRIPT_DARTS				RETURN "Darts"
		CASE MP_TAG_SCRIPT_PILOT_SCHOOL			RETURN "Race_Air"
		CASE MP_TAG_SCRIPT_DEATHMATCH			RETURN "Deathmatch"
		CASE MP_TAG_SCRIPT_DRUG_TRAFFICKING		RETURN "Drug_Trafficking"
		CASE MP_TAG_SCRIPT_GANG_ATTACK			RETURN "Gang_Attack"
		CASE MP_TAG_SCRIPT_GOLF					RETURN "Golf"
		CASE MP_TAG_SCRIPT_IN_WORLD_CIRCLES		RETURN "In_World_Circle"
		CASE MP_TAG_SCRIPT_RACE_AIR				RETURN "Race_Air"
		CASE MP_TAG_SCRIPT_RACE_BOAT			RETURN "Race_Boat"
		CASE MP_TAG_SCRIPT_RACE_LAND			RETURN "Race_Land"
		CASE MP_TAG_SCRIPT_RACE_OFFROAD			RETURN "Race_OffRoad"
		CASE MP_TAG_SCRIPT_SHOOTING_RANGE		RETURN "Shooting_Range"
		CASE MP_TAG_SCRIPT_SURVIVAL				RETURN "Survival"
		CASE MP_TAG_SCRIPT_TENNIS				RETURN "Tennis"
		CASE MP_TAG_SCRIPT_VEHICLE_DM			RETURN "Vehicle_Deathmatch"
		CASE MP_TAG_SCRIPT_TEAM_DM				RETURN "Team_Deathmatch"
		CASE MP_TAG_SCRIPT_RACE_BIKE			RETURN "Race_Bike"
		CASE MP_TAG_SCRIPT_RACE_FOOT			RETURN "Race_Foot"
		// KGM 4/12/13: From here down there are no associated TAG Icons (which became obsolete pre-release), these IDs are needed to help identify Ground Projection textures
		CASE MP_TAG_SCRIPT_CTF					RETURN "Custom_Mission"
		CASE MP_TAG_SCRIPT_LTS					RETURN "Custom_Mission"
		CASE MP_TAG_SCRIPT_STUNT_PLAYLIST		RETURN ""
		CASE MP_TAG_SCRIPT_ADVERSARY_PLAYLIST	RETURN ""
		CASE MP_TAG_SCRIPT_STUNT_PREMIUM		RETURN ""
	ENDSWITCH

	RETURN ""

ENDFUNC


//Get the offset form the model
PROC GET_OVERHEAD_OBJECT_ICON_OFFSET(MODEL_NAMES mnPassed, FLOAT &fX, FLOAT &fY, FLOAT &fZ)
	SWITCH mnPassed
	CASE Prop_Drug_package_02	fX = 0.0	fY = 0.0	fZ = 0.25 	BREAK	//Package grab model	
	DEFAULT						fX = 0.0	fY = 0.0	fZ = 0.2 	BREAK	//The default			
	ENDSWITCH
ENDPROC


// get the scalse of an icon based on its distance
FUNC FLOAT GET_SCALER_FROM_COORD(VECTOR vPos)
	FLOAT fPedRange 		= GET_DISTANCE_BETWEEN_COORDS(GET_GAMEPLAY_CAM_COORD(), vPos)
	FLOAT DefaultFov 		= 50.0
	FLOAT DefaultRange 		=  6 // OverHeadDetails.ScalerScale //4.1165
	FLOAT fScalerX 			= (GET_GAMEPLAY_CAM_FOV() * fPedRange) / (DefaultFov*DefaultRange)
	IF fScalerX > 1
		RETURN fScalerX	
	ELSE
		RETURN 1.0 
	ENDIF
ENDFUNC

// Checks to see if the object should be drawn.
//Set the draw origin of an entity based on the model size
FUNC BOOL SET_VEHICLE_AND_OBJECT_SPRITES_OVERHEAD(VECTOR tempVector, SPRITE_PLACEMENT &aSprite,VEHICLE_INDEX tempveh = NULL, OBJECT_INDEX tempobj = NULL)
	FLOAT fScreenX, fScreenY	
	FLOAT entityHeading
	FLOAT YAboveHead
	FLOAT XInCarOffset
	FLOAT ZInCarOffset	
	FLOAT XShiftStat = -0.015	
	FLOAT Scaler	
	VECTOR EntityPos
	VECTOR entityPosOffset 
	VECTOR vMin, vMax
	MODEL_NAMES mnTemp
	IF tempveh != NULL
		IF IS_VEHICLE_DRIVEABLE(tempveh)
			//Get entity pos
			entityHeading 	= GET_ENTITY_HEADING(tempveh)
			EntityPos 		= GET_ENTITY_COORDS(tempveh)
			//Get an offset
			mnTemp = GET_ENTITY_MODEL(tempveh)
			GET_MODEL_DIMENSIONS(mnTemp, vMin, vMax)
			GET_OVERHEAD_VEHICLE_ICON_OFFSET(mnTemp, XInCarOffset, YAboveHead, ZInCarOffset)			
			//Calculate that in world coordinates
			entityPosOffset = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(EntityPos, entityHeading, <<XInCarOffset, YAboveHead, ZInCarOffset+ vMax.z>>)
		ENDIF
	ELIF tempobj != NULL
		IF DOES_ENTITY_EXIST(tempobj)	
			//Get entity pos
			entityHeading 	= GET_ENTITY_HEADING(tempobj)
			EntityPos 		= GET_ENTITY_COORDS(tempobj)
			//Get an offset
			mnTemp = GET_ENTITY_MODEL(tempobj)
			GET_MODEL_DIMENSIONS(mnTemp, vMin, vMax)
			GET_OVERHEAD_VEHICLE_ICON_OFFSET(mnTemp, XInCarOffset, YAboveHead, ZInCarOffset)			
			//Calculate that in world coordinates
			entityPosOffset = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(EntityPos, entityHeading, <<XInCarOffset,YAboveHead, ZInCarOffset+ vMax.z >>)
		ENDIF
	ELIF NOT ARE_VECTORS_EQUAL(tempVector, << 0.0, 0.0, 0.0 >>)
		entityPosOffset =  (tempVector + << 0.0, 0.0, 0.5 >>)
	ENDIF
	IF GET_SCREEN_COORD_FROM_WORLD_COORD(entityPosOffset, fScreenX, fScreenY)
		fScreenX = aSprite.x
		fScreenY = aSprite.y 
		Scaler = GET_SCALER_FROM_COORD(entityPosOffset)	
		IF Scaler > 1
			//Adjust the xpos
			XShiftStat /= Scaler		
			//Set the scale of the icon
			aSprite.w = aSprite.w / Scaler
			aSprite.h = aSprite.h / Scaler
		ENDIF
		//set up the sprite
		fScreenX += XShiftStat
		//Make it so the icon is drawn in the center.
		aSprite.x = fScreenX + (aSprite.w/2)
		aSprite.y = fScreenY
		SET_DRAW_ORIGIN(entityPosOffset)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL TURN_OFF_FLYING_OVERHEADS(VEHICLE_INDEX objectiveveh)

	MODEL_NAMES mnVeh = GET_ENTITY_MODEL(objectiveveh)
	IF IS_THIS_MODEL_A_HELI(mnVeh) = FALSE
	AND IS_THIS_MODEL_A_PLANE(mnVeh) = FALSE
		RETURN FALSE
	ENDIF
	
	IF IS_VEHICLE_ON_ALL_WHEELS(objectiveveh) 
		RETURN FALSE
	ENDIF

	RETURN TRUE

ENDFUNC


/// PURPOSE: DRAWS AN ICON ABOVE AN OBJECT
/// PARAMS:
///    objectiveObj - The Vehicle Index
///    paramTag 	- The Vehicle Icon to Dra
PROC DRAW_SPRITE_ON_OBJECTIVE_VEHICLE_THIS_FRAME(VEHICLE_INDEX objectiveveh, ABOVE_VEHICLE_ICONS UseLockIcon = ABOVE_VEHICLE_ICONS_GANG, HUD_COLOURS iconColour = HUD_COLOUR_BLUELIGHT)
	//Sprite Placement details
//	SPRITE_PLACEMENT OverheadSprite
	INT R,G,B,A
	GET_HUD_COLOUR(iconColour,R,G,B,A)
//	
//	//car icon
//	OverheadSprite.x = 0
//	OverheadSprite.y = 0
//	OverheadSprite.w = 0.028
//	OverheadSprite.h = 0.045
//	OverheadSprite.r = R
//	OverheadSprite.g = G
//	OverheadSprite.b = B
//	OverheadSprite.a = UNIVERSAL_HUD_ALPHA
//	//personal car icon
//	
//	
	
	IF UseLockIcon = ABOVE_VEHICLE_ICONS_GANG
	ENDIF
	
	TRACK_VEHICLE_VISIBILITY(objectiveveh)
		
//	STRING TitleString
//	IF IS_STRING_EMPTY_HUD(VehicleName)
//		TitleString = GET_VEHICLE_ICON_TITLE(objectiveveh)
//	ELSE
		//CDM fix may not want exactly this.
//		TitleString = VehicleName
//	ENDIF
	
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF IS_VEHICLE_DRIVEABLE(objectiveveh)
			IF TURN_OFF_FLYING_OVERHEADS(objectiveveh) = FALSE
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), objectiveveh)	
					IF IS_VEHICLE_VISIBLE(objectiveveh)
						//Request the correct dictonary

	//					IF g_bUse_New_Entity_icon_System
							ADD_ENTITY_ICON(objectiveveh, "MP_Arrow" )//GET_VEHICLE_ICON_NAME(objectiveveh, UseLockIcon))
							SET_ENTITY_ICON_COLOR(objectiveveh, R,G,B,UNIVERSAL_HUD_ALPHA)
							SET_ENTITY_ICON_VISIBILITY(objectiveveh, TRUE)
							//IF g_bTurnOnSpriteText
							//CDM fix may not want exactly this.
//							SET_ENTITY_ICON_TEXT_LABEL(objectiveveh, TitleString)
							//ENDIF

	//					ELSE
	//						//If the sprite can be drawn then draw it
	//						IF SET_VEHICLE_AND_OBJECT_SPRITES_OVERHEAD(<< 0.0, 0.0, 0.0 >>, OverheadSprite, objectiveveh)
	//							
	//							
	//							
	//							DRAW_2D_SPRITE("MPInventory", GET_VEHICLE_ICON_NAME(objectiveveh, UseLockIcon), OverheadSprite, TRUE, HIGHLIGHT_OPTION_NORMAL, GFX_ORDER_BEFORE_HUD)
	//							CLEAR_DRAW_ORIGIN()
	//						ENDIF
	//					ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: DRAWS AN ICON ABOVE AN OBJECT
/// PARAMS:
///    objectiveObj - The Vehicle Index
///    paramTag 	- The Vehicle Icon to Dra
PROC TEMP_DRAW_TEXT_ON_OBJECTIVE_VEHICLE_THIS_FRAME(VEHICLE_INDEX objectiveveh, STRING iconName,STRING VehicleName,HUD_COLOURS iconColour = HUD_COLOUR_BLUELIGHT, BOOL bKeepIfInVehicle = FALSE)
	//Sprite Placement details
//	SPRITE_PLACEMENT OverheadSprite
	INT R,G,B,A
	GET_HUD_COLOUR(iconColour,R,G,B,A)
//	
//	//car icon
//	OverheadSprite.x = 0
//	OverheadSprite.y = 0
//	OverheadSprite.w = 0.028
//	OverheadSprite.h = 0.045
//	OverheadSprite.r = R
//	OverheadSprite.g = G
//	OverheadSprite.b = B
//	OverheadSprite.a = UNIVERSAL_HUD_ALPHA
//	//personal car icon
//	
//	
	
//	IF UseLockIcon = ABOVE_VEHICLE_ICONS_GANG
//	ENDIF
	
	TRACK_VEHICLE_VISIBILITY(objectiveveh)
		
	STRING TitleString
	IF IS_STRING_EMPTY_HUD(VehicleName)
		TitleString = GET_VEHICLE_ICON_TITLE(objectiveveh)
	ELSE
		//CDM fix may not want exactly this.
		TitleString = VehicleName
	ENDIF

	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF IS_VEHICLE_DRIVEABLE(objectiveveh)
			IF TURN_OFF_FLYING_OVERHEADS(objectiveveh) = FALSE
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), objectiveveh)	
				OR bKeepIfInVehicle
					IF IS_VEHICLE_VISIBLE(objectiveveh)
						//Request the correct dictonary

	//					IF g_bUse_New_Entity_icon_System
							ADD_ENTITY_ICON(objectiveveh, iconName )//GET_VEHICLE_ICON_NAME(objectiveveh, UseLockIcon))
							SET_ENTITY_ICON_COLOR(objectiveveh, R,G,B,UNIVERSAL_HUD_ALPHA)
							SET_ENTITY_ICON_VISIBILITY(objectiveveh, TRUE)
							//IF g_bTurnOnSpriteText
							//CDM fix may not want exactly this.
							SET_ENTITY_ICON_TEXT_LABEL(objectiveveh, TitleString)
							//ENDIF

	//					ELSE
	//						//If the sprite can be drawn then draw it
	//						IF SET_VEHICLE_AND_OBJECT_SPRITES_OVERHEAD(<< 0.0, 0.0, 0.0 >>, OverheadSprite, objectiveveh)
	//							
	//							
	//							
	//							DRAW_2D_SPRITE("MPInventory", GET_VEHICLE_ICON_NAME(objectiveveh, UseLockIcon), OverheadSprite, TRUE, HIGHLIGHT_OPTION_NORMAL, GFX_ORDER_BEFORE_HUD)
	//							CLEAR_DRAW_ORIGIN()
	//						ENDIF
	//					ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: DRAWS AN ICON ABOVE AN OBJECT
/// PARAMS:
///    objectiveObj - The Object index
///    paramTag 	- The object Icon to Draw 		
PROC DRAW_SPRITE_ON_OBJECTIVE_OBJECT_THIS_FRAME(OBJECT_INDEX objectiveObj, eMP_TAG paramTag = MP_TAG_GAMER_NAME/*MP_TAG_RANDOM_OBJECT*/, HUD_COLOURS iconColour = HUD_COLOUR_GREEN, STRING ObjectName = NULL)
	//Sprite Placement details
//	SPRITE_PLACEMENT OverheadSprite
	INT R,G,B,A
	GET_HUD_COLOUR(iconColour,R,G,B,A)
	
	//Set up the sprite
//	OverheadSprite.x = 0
//	OverheadSprite.y = 0
//	OverheadSprite.w = 0.028
//	OverheadSprite.h = 0.045
//	OverheadSprite.r = R
//	OverheadSprite.g = G
//	OverheadSprite.b = B
//	OverheadSprite.a = UNIVERSAL_HUD_ALPHA
	//personal car icon
	

//	IF g_bUse_New_Entity_icon_System
////		IF DOES_ENTITY_HAVE_ICON(objectiveObj)
////			REMOVE_ENTITY_ICON(objectiveObj)
////		ENDIF
//	ENDIF
	
	TRACK_OBJECT_VISIBILITY(objectiveObj)

	STRING TitleString
	IF IS_STRING_EMPTY_HUD(ObjectName)
		TitleString = GET_OBJECT_ICON_TITLE(paramTag)
	ENDIF



	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF DOES_ENTITY_EXIST(objectiveObj)
			IF IS_OBJECT_VISIBLE(objectiveObj)
				//Get the correct dictionary name
				
//				IF g_bUse_New_Entity_icon_System
					ADD_ENTITY_ICON(objectiveObj, "MP_Arrow" ) //GET_OBJECT_ICON_NAME(paramTag))
					SET_ENTITY_ICON_COLOR(objectiveObj, R,G,B,UNIVERSAL_HUD_ALPHA)
					SET_ENTITY_ICON_VISIBILITY(objectiveObj, TRUE)
					IF g_bTurnOnSpriteText
						SET_ENTITY_ICON_TEXT_LABEL(objectiveObj, TitleString)
					ENDIF
					
					
					
//				ELSE
//					//If the sprite can be drawn then draw it
//					IF SET_VEHICLE_AND_OBJECT_SPRITES_OVERHEAD(<< 0.0, 0.0, 0.0 >>, OverheadSprite, NULL, objectiveObj)
//						DRAW_2D_SPRITE(dictName, GET_OBJECT_ICON_NAME(paramTag), OverheadSprite, TRUE, HIGHLIGHT_OPTION_NORMAL, GFX_ORDER_BEFORE_HUD)
//						CLEAR_DRAW_ORIGIN()
//					ENDIF
//				ENDIF
					
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_AIMING_AT_PLAYER_THROUGH_SNIPER_SCOPE(PLAYER_INDEX aPlayer)

	IF aPlayer = PLAYER_ID()
		IF IS_AIMING_THROUGH_SNIPER_SCOPE(PLAYER_PED_ID())
			IF IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), GET_ENTITY_FROM_PED_OR_VEHICLE(GET_PLAYER_PED(aPlayer)))
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE: DRAWS AN ICON ABOVE AN OBJECT
/// PARAMS:
///    objectiveObj - The Object index
///    paramTag 	- The object Icon to Draw 		
PROC DRAW_SPRITE_ON_PLAYER_OVER_DIST_THIS_FRAME(PLAYER_INDEX ObjectivePlayer, HUD_COLOURS iconColour = HUD_COLOUR_FREEMODE)
	//Sprite Placement details
//	SPRITE_PLACEMENT OverheadSprite
//	INT R,G,B,A
//	GET_HUD_COLOUR(iconColour,R,G,B,A)
	
	PED_INDEX RemotePed = GET_PLAYER_PED(ObjectivePlayer)
	
	IF ObjectivePlayer <> PLAYER_ID()
	
//		PRINTLN("[overheads] - DRAW_SPRITE_ON_PLAYER_OVER_DIST_THIS_FRAME called on me ")
		IF (g_sTriggerMP.mtState = NO_CURRENT_MP_TRIGGER_REQUEST) 
			IF NOT IS_TRANSITION_ACTIVE()
				IF IS_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_PLAYER_NAME, ObjectivePlayer)
				
					IF CAN_I_SEE_PLAYER(ObjectivePlayer)
						
						IF IS_PED_IN_ANY_HELI(PLAYER_PED_ID()) 
						OR IS_PED_IN_ANY_PLANE(PLAYER_PED_ID()) 
						OR IS_AIMING_AT_PLAYER_THROUGH_SNIPER_SCOPE(ObjectivePlayer)
						
							VECTOR LocalPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
							VECTOR RemotePlayerPos = GET_ENTITY_COORDS(RemotePed)
			
	//						PRINTLN("[overheads] - VDIST(LocalPlayerPos,RemotePlayerPos) = ", VDIST(LocalPlayerPos,RemotePlayerPos))
	//						NET_NL()NET_PRINT("			- Player = ") NET_PRINT_INT(NATIVE_TO_INT(ObjectivePlayer))
							IF VDIST(LocalPlayerPos,RemotePlayerPos) > 70
							AND (VDIST(LocalPlayerPos,RemotePlayerPos) < 500)
							 
	//							NET_NL()NET_PRINT("DRAW ARROW! ")
								DISPLAY_ARROW_ON_PLAYER(ObjectivePlayer, iconColour)
								
	//							ADD_ENTITY_ICON(RemotePed, "MP_Arrow" ) //GET_OBJECT_ICON_NAME(paramTag))
	//							SET_ENTITY_ICON_COLOR(RemotePed, R,G,B,UNIVERSAL_HUD_ALPHA)
	//							SET_ENTITY_ICON_VISIBILITY(RemotePed, TRUE)
	//							SET_ENTITY_ICON_TEXT_LABEL(ObjectivePlayer, GET_PLAYER_NAME(ObjectivePlayer))
							ENDIF
						ENDIF
							
					ENDIF
				ENDIF	
			ENDIF
		ENDIF
	ENDIF
ENDPROC



PROC MAINTAIN_DISTANCE_ARROW_ABOVE_HEAD()

	IF IS_NET_PLAYER_OK(PLAYER_ID())
		INT IndexTagging
		FOR IndexTagging = 0 TO NUM_NETWORK_PLAYERS-1
			IF IS_NET_PLAYER_OK(INT_TO_NATIVE(PLAYER_INDEX, IndexTagging))
				DRAW_SPRITE_ON_PLAYER_OVER_DIST_THIS_FRAME(INT_TO_NATIVE(PLAYER_INDEX, IndexTagging))
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE
				ADD_SCRIPT_PROFILE_MARKER("DRAW_SPRITE_ON_PLAYER_OVER_DIST_THIS_FRAME")
				#ENDIF
				#ENDIF
			ENDIF
		ENDFOR
		
	ENDIF


ENDPROC

FUNC BOOL IS_ENTITY_UNDERWATER(ENTITY_INDEX entity)
	
	IF NOT IS_ENTITY_IN_WATER(entity)
		RETURN FALSE
	ENDIF
	
	RETURN (GET_ENTITY_SUBMERGED_LEVEL(entity) >= 1.0)
	
ENDFUNC


/// PURPOSE: DRAWS AN ICON ABOVE A COORD ALLOWING OPTIONAL FADE IN AND FADE OUT DISTANCE OVERRIDES
/// PARAMS:
///    objectiveVector - The coord
///    paramTag 	- The object Icon to Draw
/// NOTES:	default params don't fade in or out, but would fade in from 60m to 50m and fadeout from 40m to 30m (start is farther away from coords than end)
PROC DRAW_SPRITE_ON_OBJECTIVE_COORD_THIS_FRAME_WITH_FADES_IN_AND_OUT(	VECTOR			objectiveVector,
																		eMP_TAG_SCRIPT	paramTag,
																		HUD_COLOURS		iconColour			= HUD_COLOUR_FREEMODE,
																		BOOL			paramDoFadeIn			= FALSE,
																		FLOAT			paramFadeInStart		= 50.0,
																		FLOAT			paramFadeInEnd			= 40.0,
																		BOOL			paramDoFadeOut			= FALSE,
																		FLOAT			paramFadeOutStart		= 30.0,
																		FLOAT			paramFadeOutEnd			= 20.0)
	//Sprite Placement details
//	SPRITE_PLACEMENT OverheadSprite
	INT R,G,B,A
	GET_HUD_COLOUR(iconColour,R,G,B,A)

	INT iAlpha = 255
	
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF NOT IS_ENTITY_UNDERWATER(PLAYER_PED_ID())
			IF NOT IS_CELLPHONE_CAMERA_IN_USE()
		
				// KGM: using VDIST, not VDIST2 (squared)
				FLOAT	Distance		= VDIST(objectiveVector, GET_PLAYER_COORDS(PLAYER_ID()))
				FLOAT	RangeDistance	= 0.0
				FLOAT	theRange		= 0.0
			
				// Fade In?
				IF (paramDoFadeIn)
					IF (Distance > paramFadeInStart)
						iAlpha = 0
					ELIF (Distance >= paramFadeInEnd)
						theRange		= paramFadeInStart - paramFadeInEnd
						RangeDistance	= Distance - paramFadeInEnd
						iAlpha			= FLOOR(255.0 - (255.0 / theRange * RangeDistance))
					ENDIF
				ENDIF
				
				// Fade Out?
				IF (paramDoFadeOut)
					IF (Distance < paramFadeOutEnd)
						iAlpha = 0
					ELIF (Distance <= paramFadeOutStart)
						theRange		= paramFadeOutStart - paramFadeOutEnd
						RangeDistance	= Distance - paramFadeOutEnd
						iAlpha			= FLOOR(255.0 / theRange * RangeDistance)
					ENDIF
				ENDIF
				
			ENDIF
		ELSE
			iAlpha = 0
		ENDIF
	ENDIF

	ENTITY_ICON_ID anId
	//Request the correct dictonary
	anId = ADD_ENTITY_ICON_BY_VECTOR(objectiveVector.x, objectiveVector.y, objectiveVector.z, GET_CORONA_ICON_TEXTURE(paramTag))
	IF DOES_ENTITY_HAVE_ICON_ID(anId)
		SET_ENTITY_ICON_ID_COLOR(anId, R,G,B,iAlpha)
		SET_ENTITY_ICON_ID_VISIBILITY(anId, TRUE)
		SET_ENTITY_ICON_ID_BG_VISIBILITY(anId, TRUE)
		SET_ENTITY_ICON_ID_BG_COLOR(anId, R,G,B, iAlpha)
	ENDIF
						
ENDPROC




/// PURPOSE: DRAWS AN ICON ABOVE A COORD
/// PARAMS:
///    objectiveVector - The coord
///    paramTag 	- The object Icon to Draw 		
PROC DRAW_SPRITE_ON_OBJECTIVE_COORD_THIS_FRAME(VECTOR objectiveVector, eMP_TAG_SCRIPT paramTag, HUD_COLOURS iconColour = HUD_COLOUR_FREEMODE)
	//Sprite Placement details
//	SPRITE_PLACEMENT OverheadSprite
	INT R,G,B,A
	GET_HUD_COLOUR(iconColour,R,G,B,A)

	INT iAlpha = 255


	IF is_net_player_ok(PLAYER_ID())
		
		IF NOT IS_ENTITY_UNDERWATER(PLAYER_PED_ID())
			
			// KGM: using VDIST, not VDIST2 (squared)
			FLOAT Distance = VDIST(objectiveVector, GET_PLAYER_COORDS(PLAYER_ID()))
		
		
			IF Distance > gPRivate_LowerDistance
			AND Distance < gPRivate_upperDistance
				FLOAT RangeDistance = Distance - gPRivate_LowerDistance
				FLOAT m = 255.0/TO_FLOAT(gPRivate_upperDistance)
				
				iAlpha = FLOOR(m*RangeDistance)
				
			elif Distance < gPRivate_LowerDistance
			
				iAlpha = 0 
			ENDIF
		
		ELSE
			iAlpha = 0
		ENDIF
		
	ENDIF

	ENTITY_ICON_ID anId
	//Request the correct dictonary
	anId = ADD_ENTITY_ICON_BY_VECTOR(objectiveVector.x, objectiveVector.y, objectiveVector.z, GET_CORONA_ICON_TEXTURE(paramTag))
	IF DOES_ENTITY_HAVE_ICON_ID(anId)
		SET_ENTITY_ICON_ID_COLOR(anId, R,G,B,iAlpha)
		SET_ENTITY_ICON_ID_VISIBILITY(anId, TRUE)
		SET_ENTITY_ICON_ID_BG_VISIBILITY(anId, TRUE)
		SET_ENTITY_ICON_ID_BG_COLOR(anId, R,G,B, iAlpha)
	ENDIF
						
ENDPROC

FUNC BOOL IS_VALID_OVERHEAD_ITEM(eMP_TAG MpTag)

	MpTag = MpTag
//	IF MpTag = MP_TAG_HOLDUP_CASH
//		RETURN FALSE
//	ENDIF
	
	
	RETURN TRUE
	


ENDFUNC

/// PURPOSE: sets up a fake wanted rating above a ped
/// PARAMS:
///    iMpTagId -		The returned controling INT of the MP tag 
///    niPed - 			The net ID to add the Tag to
///    iWantedLevel - 	The wanted wanted level
///    iTeam - 			The team (optional defaults to white)
///    sPedName - 		The ped name (optional)
/// RETURNS:TRUE when done
FUNC BOOL CREATE_PED_OVERHEAD_ICONS(INT &iMpTagId, INT iPed, INT &iSetUpBitset[], INT &iInitialiseBitset[], NETWORK_INDEX niPed, INT iTeam = -1, STRING sPedName = NULL, eMP_TAG MpTag = MP_TAG_IF_PED_FOLLOWING, HUD_COLOURS iconColour = HUD_COLOUR_BLUELIGHT, BOOL bRemoveIfDead = TRUE)
	
	IF NOT IS_MP_GAMER_TAG_MOVIE_ACTIVE()
		PRINTLN("[overheads] - IS_MP_GAMER_TAG_MOVIE_ACTIVE, returning FALSE as cannot add gamertags until this is true.")
		RETURN FALSE
	ENDIF
	
	IF IS_VALID_OVERHEAD_ITEM(MpTag)
		IF IS_NET_PED_INJURED(niPed)
			IF IS_BIT_SET(iSetUpBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
				//Set the visibility
				///Any turned on HAVE to be turned off before removing.
				IF bRemoveIfDead
					SET_MP_GAMER_TAG_VISIBILITY(iMpTagId, MpTag, FALSE)
					CLEAR_BIT(iSetUpBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
					PRINTLN("[overheads] - CREATE_PED_OVERHEAD_ICONS - removed overhead icon because ped is dead.")
				ENDIF
			ENDIF
			IF bRemoveIfDead
				RETURN FALSE
			ENDIF
		ENDIF
		
		IF NOT IS_BIT_SET(iInitialiseBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
			IF iMpTagId != -1
				IF IS_MP_GAMER_TAG_ACTIVE(iMpTagId)
					REMOVE_MP_GAMER_TAG(iMpTagId)
					PRINTLN("[Overheads] - REMOVE_MP_GAMER_TAG call 0. Player = a ped.")
				ELIF IS_MP_GAMER_TAG_FREE(iMpTagId)
					iMpTagId = -1
					SET_BIT(iInitialiseBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
					PRINTLN("[overheads] - CREATE_PED_OVERHEAD_ICONS - initialised overhead icon id to -1.")
				ENDIF
			ENDIF
		ENDIF
		
		IF iMpTagId < 0 //OR NOT IS_MP_GAMER_TAG_ACTIVE(iMpTagId)
			
			#IF IS_DEBUG_BUILD
			IF iMpTagId < 0
				PRINTLN("[overheads] - CREATE_PED_OVERHEAD_ICONS - iMpTagId < 0 so CREATE_FAKE_MP_GAMER_TAG")
			ENDIF
			#ENDIF
			
			IF IS_STRING_NULL(sPedName)
			OR ARE_STRINGS_EQUAL(sPedName, "")
				iMpTagId = CREATE_FAKE_MP_GAMER_TAG(NET_TO_PED(niPed), "", FALSE, FALSE, "", 0)			
				PRINTLN("[overheads] - CREATE_PED_OVERHEAD_ICONS - pedName doesn't exist, created overhead icon. Id = ", iMpTagId)
			//If the ped has a name
			ELSE
				iMpTagId = CREATE_FAKE_MP_GAMER_TAG(NET_TO_PED(niPed), sPedName, FALSE, FALSE, "", 0)	
				PRINTLN("[overheads] - CREATE_PED_OVERHEAD_ICONS - pedName does exist, created overhead icon. Id = ", iMpTagId)
			ENDIF

		//If the tag exists and it hasn't been set it up then set it up
		ELIF IS_MP_GAMER_TAG_ACTIVE(iMpTagId) AND IS_BIT_SET(iInitialiseBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
			
			BOOL bRenderForVehicle = FALSE
			
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetNine, ciPed_BSNine_OverheadProtectIconAI)
				ENTITY_INDEX eiVeh
				IF IS_PED_IN_ANY_VEHICLE(NET_TO_PED(niPed))
					eiVeh = GET_VEHICLE_PED_IS_IN(NET_TO_PED(niPed))
					
					IF NOT IS_ENTITY_OCCLUDED(eiVeh)
						bRenderForVehicle = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT IS_ENTITY_OCCLUDED(NET_TO_ENT(niPed))
			OR bRenderForVehicle
				IF NOT IS_BIT_SET(iSetUpBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
					PRINTLN("[overheads] - CREATE_PED_OVERHEAD_ICONS - entity is not occluded and bSetup = FALSE. Id = ", iMpTagId)
					//Set the visibility
					///Any turned on HAVE to be turned off before removing.
					SET_MP_GAMER_TAG_VISIBILITY(iMpTagId, MpTag, TRUE)
					PRINTLN("[overheads] - CREATE_PED_OVERHEAD_ICONS - set tag visibility to TRUE. Id = ", iMpTagId)
					//Set the tem clouor if needed
					IF iTeam != -1
					AND iconColour = HUD_COLOUR_WHITE
						SET_MP_GAMER_TAG_COLOUR(iMpTagId, MpTag, GET_TEAM_HUD_COLOUR(iTeam))
						PRINTLN("[overheads] - CREATE_PED_OVERHEAD_ICONS - set colur to team colour. Id = ", iMpTagId)
					ELSE
						SET_MP_GAMER_TAG_COLOUR(iMpTagId, MpTag, iconColour)
						PRINTLN("[overheads] - CREATE_PED_OVERHEAD_ICONS - set colur to iconColour. Id = ", iMpTagId)
					ENDIF
					SET_BIT(iSetUpBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
					PRINTLN("[overheads] - CREATE_PED_OVERHEAD_ICONS - set bSetUp = TRUE. Id = ", iMpTagId)
					RETURN TRUE		
				ELSE
					RETURN TRUE		
				ENDIF
			ELSE
				IF IS_BIT_SET(iSetUpBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
					PRINTLN("[overheads] - CREATE_PED_OVERHEAD_ICONS - IF IS_MP_GAMER_TAG_ACTIVE(iMpTagId)")
					PRINTLN("[overheads] - CREATE_PED_OVERHEAD_ICONS - IF IS_ENTITY_OCCLUDED(NET_TO_ENT(niPed))")
					//Set the visibility
					///Any turned on HAVE to be turned off before removing.
					SET_MP_GAMER_TAG_VISIBILITY(iMpTagId, MpTag, FALSE)
					CLEAR_BIT(iSetUpBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
					RETURN TRUE		
				ELSE
					RETURN TRUE		
				ENDIF
			ENDIF
		ENDIF
		
		
		#IF IS_DEBUG_BUILD
		IF iMpTagId < 0
			PRINTLN("[overheads] - CREATE_PED_OVERHEAD_ICONS - iMpTagId < 0")
		ELIF NOT IS_MP_GAMER_TAG_ACTIVE(iMpTagId)
			PRINTLN("[overheads] - CREATE_PED_OVERHEAD_ICONS - IF NOT IS_MP_GAMER_TAG_ACTIVE(iMpTagId)")
		ENDIF
		#ENDIF
		
		#IF IS_DEBUG_BUILD
			PRINTLN("[overheads] - SET_APARTMENT_EXTERIOR_SPAWN_DATA - REATE_PED_WANTED_OVERHEAD TAG has **NOT** been created")
		#ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE: removes a fake wanted rating above a ped
/// PARAMS:
///    iMpTagId -		The MP tag to remove
PROC REMOVE_PED_OVERHEAD_ICONS(INT &iMpTagId, INT iPed, INT &iSetUpBitset[], INT &iInitialiseBitset[], eMP_TAG MpTag = MP_TAG_IF_PED_FOLLOWING)
	IF iMpTagId != -1 AND IS_BIT_SET(iInitialiseBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
		IF IS_VALID_OVERHEAD_ITEM(MpTag)
			IF IS_MP_GAMER_TAG_ACTIVE(iMpTagId)
				#IF IS_DEBUG_BUILD
					PRINTLN("[overheads] - REMOVE_PED_OVERHEAD_ICONS - Ped TAG has been remove with ID ", iMpTagId)
				#ENDIF
				////////////////////////////////////////////////////////////
				///       
				///       EVERY ONE SET VISIBLE NEEDS TO BE TURNED OFF HERE!!!!!!!!!!!!!!!!!!111111
				///       
				SET_MP_GAMER_TAG_VISIBILITY(iMpTagId, MpTag, FALSE)
				REMOVE_MP_GAMER_TAG(iMpTagId)
				PRINTLN("[Overheads] - REMOVE_MP_GAMER_TAG call 4. Player = a ped")
				CLEAR_BIT(iSetUpBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
				CLEAR_BIT(iInitialiseBitset[GET_LONG_BITSET_INDEX(iped)], GET_LONG_BITSET_BIT(iPed))
				//iMpTagId = -1
			ENDIF
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE: sets up a fake wanted rating above a ped
/// PARAMS:
///    iMpTagId -		The returned controling INT of the MP tag 
///    niPed - 			The net ID to add the Tag to
///    iWantedLevel - 	The wanted wanted level
///    iTeam - 			The team (optional defaults to white)
///    sPedName - 		The ped name (optional)
/// RETURNS:TRUE when done
FUNC BOOL CREATE_PED_OVERHEAD_NAME(INT &iMpTagId, BOOL &bSetUp,BOOL &bInitialise, NETWORK_INDEX niPed, INT iTeam = -1, STRING sPedName = NULL, HUD_COLOURS iconColour = HUD_COLOUR_BLUELIGHT)
	IF IS_NET_PED_INJURED(niPed)
		IF bSetUp = TRUE
			//Set the visibility
			///Any turned on HAVE to be turned off before removing.
			SET_MP_GAMER_TAG_VISIBILITY(iMpTagId, MP_TAG_GAMER_NAME, FALSE)
			bSetUp = FALSE
		ENDIF	
		RETURN FALSE
	ENDIF
	
	IF bInitialise = FALSE
		iMpTagId = -1
		bInitialise = TRUE
	ENDIF

	IF iMpTagId < 0
		
		PRINTLN("[overheads] - CREATE_PED_WANTED_OVERHEAD TAG - CREATE_PED_OVERHEAD_NAME - iMpTagId < 0")
		
		//if he is nameless
		IF IS_STRING_NULL(sPedName)
			iMpTagId = CREATE_FAKE_MP_GAMER_TAG(NET_TO_PED(niPed), "", FALSE, FALSE, "", 0)		
			PRINTLN("[overheads] - CREATE_PED_WANTED_OVERHEAD TAG - CREATE_FAKE_MP_GAMER_TAG - NAME IS NULL")
		//If the ped has a name
		ELSE
			iMpTagId = CREATE_FAKE_MP_GAMER_TAG(NET_TO_PED(niPed), sPedName, FALSE, FALSE, "", 0)	
			PRINTLN("[overheads] - CREATE_PED_WANTED_OVERHEAD TAG - CREATE_FAKE_MP_GAMER_TAG - NAME IS ")PRINTLN(sPedName)
		ENDIF
	
	//If the tag exists anf it hasn't been set it up then set it up
	ELIF IS_MP_GAMER_TAG_ACTIVE(iMpTagId)
		IF NOT IS_ENTITY_OCCLUDED(NET_TO_ENT(niPed))		
			IF bSetUp = FALSE
				PRINTLN("[overheads] - CREATE_PED_WANTED_OVERHEAD TAG - IS_MP_GAMER_TAG_ACTIVE(iMpTagId), entity is not occluded.")
				//Set the visibility
				///Any turned on HAVE to be turned off before removing.
				SET_MP_GAMER_TAG_VISIBILITY(iMpTagId, MP_TAG_GAMER_NAME, TRUE)
				//Set the tem clouor if needed
				 
				IF iTeam != -1
				AND iconColour = HUD_COLOUR_WHITE
					SET_MP_GAMER_TAG_COLOUR(iMpTagId, MP_TAG_GAMER_NAME, GET_TEAM_HUD_COLOUR(iTeam))
					SET_MP_GAMER_TAG_COLOUR(iMpTagId, MP_TAG_GAMER_NAME_NEARBY, GET_TEAM_HUD_COLOUR(iTeam))
					PRINTLN("[overheads] - CREATE_PED_WANTED_OVERHEAD TAG - SET_MP_GAMER_TAG_COLOUR CALLED")
				ELSE
					SET_MP_GAMER_TAG_COLOUR(iMpTagId, MP_TAG_GAMER_NAME, iconColour)
					SET_MP_GAMER_TAG_COLOUR(iMpTagId, MP_TAG_GAMER_NAME_NEARBY, iconColour)
				ENDIF
				bSetUp = TRUE
				RETURN TRUE		
			ELSE
				RETURN TRUE		
			ENDIF
		ELSE
			IF bSetUp = TRUE
				PRINTLN("[overheads] - CREATE_PED_WANTED_OVERHEAD TAG - IS_MP_GAMER_TAG_ACTIVE(iMpTagId), entity is occluded.")
				PRINTLN("[overheads] - CREATE_PED_WANTED_OVERHEAD TAG - IS_ENTITY_OCCLUDED(NET_TO_ENT(niPed))")
				//Set the visibility
				///Any turned on HAVE to be turned off before removing.
				SET_MP_GAMER_TAG_VISIBILITY(iMpTagId, MP_TAG_GAMER_NAME, FALSE)
				bSetUp = FALSE
				RETURN TRUE		
			ELSE
				RETURN TRUE		
			ENDIF
		ENDIF
	ENDIF
	#IF IS_DEBUG_BUILD
		PRINTLN("[overheads] - CREATE_PED_WANTED_OVERHEAD TAG - has **NOT** been created")
	#ENDIF
	RETURN FALSE
ENDFUNC


/// PURPOSE: sets up a fake wanted rating above a ped
/// PARAMS:
///    iMpTagId -		The returned controling INT of the MP tag 
///    niPed - 			The net ID to add the Tag to
///    iWantedLevel - 	The wanted wanted level
///    iTeam - 			The team (optional defaults to white)
///    sPedName - 		The ped name (optional)
/// RETURNS:TRUE when done
FUNC BOOL CREATE_PED_INDEX_OVERHEAD_NAME(INT &iMpTagId, BOOL &bSetUp,BOOL &bInitialise, PED_INDEX piPed, INT iTeam = -1, STRING sPedName = NULL, HUD_COLOURS iconColour = HUD_COLOUR_BLUELIGHT)
	IF IS_PED_INJURED(piPed)
		IF bSetUp = TRUE
			IF IS_MP_GAMER_TAG_ACTIVE(iMpTagId)
				//Set the visibility
				///Any turned on HAVE to be turned off before removing.
				///       
				SET_MP_GAMER_TAG_VISIBILITY(iMpTagId, MP_TAG_GAMER_NAME, FALSE)
			ENDIF
			bSetUp = FALSE
		ENDIF	
		RETURN FALSE
	ENDIF
	
	IF bInitialise = FALSE
		iMpTagId = -1
		bInitialise = TRUE
	ENDIF

	IF iMpTagId < 0
	
		PRINTLN("[overheads] - iMpTagId < 0")
		
		//if he is nameless
		IF IS_STRING_NULL(sPedName)
			iMpTagId = CREATE_FAKE_MP_GAMER_TAG(piPed, "", FALSE, FALSE, "", 0)		
			PRINTLN("[overheads] - CREATE_FAKE_MP_GAMER_TAG - NAME IS NULL")
		//If the ped has a name
		ELSE
			iMpTagId = CREATE_FAKE_MP_GAMER_TAG(piPed, sPedName, FALSE, FALSE, "", 0)	
			PRINTLN("[overheads] - CREATE_FAKE_MP_GAMER_TAG - name is ", sPedName, " iMpTagId = ", iMpTagId)
		ENDIF
	
	//If the tag exists anf it hasn't been set it up then set it up
	ELIF IS_MP_GAMER_TAG_ACTIVE(iMpTagId)
		IF NOT IS_ENTITY_OCCLUDED(piPed)		
			IF bSetUp = FALSE
				PRINTLN("[overheads] - CREATE_FAKE_MP_GAMER_TAG - IS_MP_GAMER_TAG_ACTIVE(iMpTagId)")
				//Set the visibility
				///Any turned on HAVE to be turned off before removing.
				SET_MP_GAMER_TAG_VISIBILITY(iMpTagId, MP_TAG_GAMER_NAME, TRUE)
				//Set the tem clouor if needed
				 
				IF iTeam != -1
				AND iconColour = HUD_COLOUR_WHITE
					SET_MP_GAMER_TAG_COLOUR(iMpTagId, MP_TAG_GAMER_NAME, GET_TEAM_HUD_COLOUR(iTeam))
					SET_MP_GAMER_TAG_COLOUR(iMpTagId, MP_TAG_GAMER_NAME_NEARBY, GET_TEAM_HUD_COLOUR(iTeam))
					PRINTLN("[overheads] - CREATE_FAKE_MP_GAMER_TAG - SET_MP_GAMER_TAG_COLOUR CALLED")
				ELSE
					SET_MP_GAMER_TAG_COLOUR(iMpTagId, MP_TAG_GAMER_NAME, iconColour)
					SET_MP_GAMER_TAG_COLOUR(iMpTagId, MP_TAG_GAMER_NAME_NEARBY, iconColour)
				ENDIF
				bSetUp = TRUE
				RETURN TRUE		
			ELSE
				RETURN TRUE		
			ENDIF
		ELSE
			IF bSetUp = TRUE
				PRINTLN("[overheads] - CREATE_FAKE_MP_GAMER_TAG - IS_MP_GAMER_TAG_ACTIVE(iMpTagId)")
				PRINTLN("[overheads] - CREATE_FAKE_MP_GAMER_TAG - IS_ENTITY_OCCLUDED(NET_TO_ENT(niPed))")
				//Set the visibility
				///Any turned on HAVE to be turned off before removing.
				SET_MP_GAMER_TAG_VISIBILITY(iMpTagId, MP_TAG_GAMER_NAME, FALSE)
				bSetUp = FALSE
				RETURN TRUE		
			ELSE
				RETURN TRUE		
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


FUNC BOOL CREATE_PED_INDEX_OVERHEAD_ICON(INT &iMpTagId, BOOL &bSetUp,BOOL &bInitialise, PED_INDEX piPed, INT iTeam = -1, STRING sPedName = NULL, HUD_COLOURS iconColour = HUD_COLOUR_BLUELIGHT, eMP_TAG iconTag = MP_TAG_DRIVER)
	IF IS_PED_INJURED(piPed)
		IF bSetUp = TRUE
			//Set the visibility
			///Any turned on HAVE to be turned off before removing.
			SET_MP_GAMER_TAG_VISIBILITY(iMpTagId, iconTag, FALSE)
			bSetUp = FALSE
		ENDIF	
		RETURN FALSE
	ENDIF
	
	IF bInitialise = FALSE
		iMpTagId = -1
		bInitialise = TRUE
	ENDIF

	IF iMpTagId < 0
	
		PRINTLN("[overheads] - iMpTagId < 0")
		
		//if he is nameless
		IF IS_STRING_EMPTY_HUD(sPedName)
			iMpTagId = CREATE_FAKE_MP_GAMER_TAG(piPed, "", FALSE, FALSE, "", 0)		
			PRINTLN("[overheads] - CREATE_PED_INDEX_OVERHEAD_ICON - NAME IS NULL")
		//If the ped has a name
		ELSE
			iMpTagId = CREATE_FAKE_MP_GAMER_TAG(piPed, sPedName, FALSE, FALSE, "", 0)	
			PRINTLN("[overheads] - CREATE_PED_INDEX_OVERHEAD_ICON - name is ", sPedName, " iMpTagId = ", iMpTagId)
		ENDIF
	
	//If the tag exists anf it hasn't been set it up then set it up
	ELIF IS_MP_GAMER_TAG_ACTIVE(iMpTagId)
		IF NOT IS_ENTITY_OCCLUDED(piPed)		
			IF bSetUp = FALSE
				PRINTLN("[overheads] - CREATE_PED_INDEX_OVERHEAD_ICON - IS_MP_GAMER_TAG_ACTIVE(iMpTagId)")
				//Set the visibility
				///Any turned on HAVE to be turned off before removing.
				SET_MP_GAMER_TAG_VISIBILITY(iMpTagId, iconTag, TRUE)
				//Set the tem clouor if needed
				 
				IF iTeam != -1
				AND iconColour = HUD_COLOUR_WHITE
					SET_MP_GAMER_TAG_COLOUR(iMpTagId, iconTag, GET_TEAM_HUD_COLOUR(iTeam))
					PRINTLN("[overheads] - CREATE_PED_INDEX_OVERHEAD_ICON - SET_MP_GAMER_TAG_COLOUR CALLED")
				ELSE
					SET_MP_GAMER_TAG_COLOUR(iMpTagId, iconTag, iconColour)
				ENDIF
				bSetUp = TRUE
				RETURN TRUE		
			ELSE
				RETURN TRUE		
			ENDIF
		ELSE
			IF bSetUp = TRUE
				PRINTLN("[overheads] - CREATE_PED_INDEX_OVERHEAD_ICON - IS_MP_GAMER_TAG_ACTIVE(iMpTagId)")
				PRINTLN("[overheads] - CREATE_PED_INDEX_OVERHEAD_ICON - IS_ENTITY_OCCLUDED(NET_TO_ENT(niPed))")
				//Set the visibility
				///Any turned on HAVE to be turned off before removing.
				SET_MP_GAMER_TAG_VISIBILITY(iMpTagId, iconTag, FALSE)
				bSetUp = FALSE
				RETURN TRUE		
			ELSE
				RETURN TRUE		
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC



/// PURPOSE: removes a fake wanted rating above a ped
/// PARAMS:
///    iMpTagId -		The MP tag to remove
PROC REMOVE_PED_OVERHEAD_NAME(INT &iMpTagId, BOOL &bSetUp, BOOL &bInitialise)
	IF iMpTagId != -1
		IF IS_MP_GAMER_TAG_ACTIVE(iMpTagId)
		
			PRINTLN("[overheads] - REMOVE_PED_OVERHEAD_ICONS - Ped TAG has been remove with ID ", iMpTagId)
				
			////////////////////////////////////////////////////////////
			///       
			///       EVERY ONE SET VISIBLE NEEDS TO BE TURNED OFF HERE!!!!!!!!!!!!!!!!!!111111
			///       
			
			SET_MP_GAMER_TAG_VISIBILITY(iMpTagId, MP_TAG_GAMER_NAME, FALSE)
			REMOVE_MP_GAMER_TAG(iMpTagId)
			PRINTLN("[Overheads] - REMOVE_MP_GAMER_TAG call 3. Player = a ped")
			bSetUp = FALSE
			bInitialise = FALSE
			
			iMpTagId = -1
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Check if the player is the driver of a vehicle
FUNC BOOL IS_PED_DRIVING_ANY_VEHICLE(PED_INDEX& aPed)
	IF IS_PED_IN_ANY_VEHICLE(aPed)
		IF GET_PED_IN_VEHICLE_SEAT(GET_VEHICLE_PED_IS_USING(aPed)) = aPed
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC 


PROC DEAL_WITH_PED_LEAVING(PED_INDEX& aPed)

	IF DO_LEFT_PLAYERS_FLY_HELIS_INTO_SUNSET() = FALSE
		EXIT
	ENDIF

	IF NOT IS_PED_INJURED(aPed)
	
		//added for class C bug 1472560
		IF NETWORK_HAS_CONTROL_OF_ENTITY(aPed)
//			SET_ALLOW_LOCKON_TO_PED_IF_FRIENDLY(aPed, TRUE) //removed for bug 1609618
			PRINTLN("[overheads] - DEAL_WITH_PED_LEAVING: SET_ALLOW_LOCKON_TO_PED_IF_FRIENDLY = TRUE ")
		ENDIF
	
		IF IS_PED_IN_ANY_HELI(aPed)
			

			
			VEHICLE_INDEX aHeli = GET_VEHICLE_PED_IS_IN(aPed)
			
			IF IS_PED_DRIVING_ANY_VEHICLE(aPed)
				IF NETWORK_HAS_CONTROL_OF_ENTITY(aHeli)

				
					
					VECTOR HeliCoords = GET_ENTITY_COORDS(aHeli)
					
					IF GET_SCRIPT_TASK_STATUS(aPed, SCRIPT_TASK_VEHICLE_MISSION) != PERFORMING_TASK
					AND GET_SCRIPT_TASK_STATUS(aPed,SCRIPT_TASK_VEHICLE_MISSION) != WAITING_TO_START_TASK
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(aPed,TRUE)
						SET_PED_COMBAT_ATTRIBUTES(aPed, CA_USE_VEHICLE, TRUE)
						TASK_VEHICLE_MISSION_COORS_TARGET(aPed,aHeli,HeliCoords,MISSION_LAND,30,DRIVINGMODE_PLOUGHTHROUGH,-1,-1)
						
					ENDIF

					SET_PED_KEEP_TASK(aPed, TRUE)
					
				ENDIF
			ENDIF
						
		ELIF IS_PED_IN_ANY_PLANE(aPed)

//			VEHICLE_INDEX aHeli = GET_VEHICLE_PED_IS_IN(aPed)
//			
//			IF IS_PED_DRIVING_ANY_VEHICLE(aPed)
//				VECTOR HeliCoords = GET_ENTITY_COORDS(aHeli)
//				TASK_PLANE_MISSION(aPed, aHeli, NULL, NULL, HeliCoords, MISSION_CRUISE, 30.0, 10.0, -1.0, 30, 30)
//			ENDIF
			
		ENDIF
	ENDIF
	
ENDPROC



PROC RUN_PED_LEFT_BEHIND_DISPLAY()
	
	IF IS_MP_GAMER_TAG_MOVIE_ACTIVE()
		
		INT NumberActive = 0
		IF g_OverheadPed_LeftBehind_Details.iOneDisplaying
			PRINTLN("[overheads] - RUN_PED_LEFT_BEHIND_DISPLAY: g_OverheadPed_LeftBehind_Details.iOneDisplaying = ", g_OverheadPed_LeftBehind_Details.iOneDisplaying)

			INT I
			FOR I = 0 TO NUM_NETWORK_PLAYERS-1
				IF IS_STRING_EMPTY_HUD(g_OverheadPed_LeftBehind_Details.tl_reason[I]) = FALSE
					NumberActive++


					PRINTLN("[overheads] - RUN_PED_LEFT_BEHIND_DISPLAY: CREATE_PED_INDEX_OVERHEAD_NAME - called for index ", I)
					CREATE_PED_INDEX_OVERHEAD_NAME(g_OverheadPed_LeftBehind_Details.iMpTag[I], g_OverheadPed_LeftBehind_Details.bSetup[I], g_OverheadPed_LeftBehind_Details.bInitialise[I], g_OverheadPed_LeftBehind_Details.g_PedIndex[I], -1, g_OverheadPed_LeftBehind_Details.tl_reason[I])
					IF NOT IS_PED_INJURED(g_OverheadPed_LeftBehind_Details.g_PedIndex[I])	
						IF NETWORK_HAS_CONTROL_OF_ENTITY(g_OverheadPed_LeftBehind_Details.g_PedIndex[I])
							FINALIZE_HEAD_BLEND(g_OverheadPed_LeftBehind_Details.g_PedIndex[I])
						ENDIF
					ENDIF
					DEAL_WITH_PED_LEAVING(g_OverheadPed_LeftBehind_Details.g_PedIndex[I])

	//				FREEZE_ENTITY_POSITION(g_OverheadPed_LeftBehind_Details.g_PedIndex[I], FALSE)
					
				ENDIF
			ENDFOR
		ENDIF
		
		
		IF NumberActive = 0
			g_OverheadPed_LeftBehind_Details.iOneDisplaying = FALSE
		ENDIF
	
	ENDIF
	
ENDPROC




















///////////////////////////////////////////////////////////////////////////////
///    
///    CNC DISPLAY FUNCTIUONS
///    
//FUNC BOOL SHOULD_CUFFS_BE_DRAWN(DISPLAYOVERHEADSTRUCT& Details)
//	IF NOT IS_STRING_NULL_OR_EMPTY(Details.PrivateStruct.sCuffedIcon)
//		IF NOT ARE_STRINGS_EQUAL(Details.PrivateStruct.sCuffedIcon, "Handcuff_Keys")
//			RETURN TRUE
//		ENDIF
//	ENDIF
//	RETURN FALSE
//ENDFUNC
#IF IS_DEBUG_BUILD

FUNC STRING GET_STRING_FROM_MP_TAG(eMP_TAG paramTag)
	SWITCH paramTag
//		CASE MP_TAG_PARTNER_ICON		RETURN "PARTNER_ICON"		
		CASE MP_TAG_GAMER_NAME			RETURN "GAMER_NAME"		
		CASE MP_TAG_GAMER_NAME_NEARBY	RETURN "GAMER_NAME_NEARBY"		
		CASE MP_TAG_CREW_TAG			RETURN "CREW_TAG"		
		CASE MP_TAG_WANTED_LEVEL		RETURN "WANTED_LEVEL"		
		CASE MP_TAG_AUDIO				RETURN "AUDIO"		
//		CASE MP_TAG_KEYS				RETURN "KEYS"		
//		CASE MP_TAG_CUFFS				RETURN "CUFFS"		
		//CASE MP_TAG_RANK_ICON			RETURN "RANK_ICON"		
		//CASE MP_TAG_RANK_NUMBER		RETURN "RANK_NUMBER"		
//		CASE MP_TAG_KEY_CARD			RETURN "KEY_CARD"		
//		CASE MP_TAG_DOWNLOAD_DATA		RETURN "DOWNLOAD_DATA"		
//		CASE MP_TAG_COKE_DRUGS			RETURN "COKE_DRUGS"		
//		CASE MP_TAG_METH_DRUGS			RETURN "METH_DRUGS"		
//		CASE MP_TAG_WEED				RETURN "WEED"		
//		CASE MP_TAG_HEROIN				RETURN "HEROIN"		
//		CASE MP_TAG_CASH_FROM_SAFE		RETURN "CASH_FROM_SAFE"		
//		CASE MP_TAG_CASH_FROM_BANK		RETURN "CASH_FROM_BANK"		
//		CASE MP_TAG_INV_VEHICLE			RETURN "INV_VEHICLE"		
		CASE MP_TAG_PACKAGES			RETURN "PACKAGES"	
	//	CASE MP_TAG_BALL				RETURN "BALL"	
//		CASE MP_TAG_REMOTE_CONTROL		RETURN "REMOTE_CONTROL"		
//		CASE MP_TAG_WEAPONS_PACKAGE		RETURN "WEAPONS_PACKAGE"		
//		CASE MP_TAG_RANDOM_OBJECT		RETURN "RANDOM_OBJECT"		
		CASE MP_TAG_IF_PED_FOLLOWING	RETURN "IF_PED_FOLLOWING"		
//		CASE MP_TAG_CUFF_KEYS			RETURN "CUFF_KEYS"		
//		CASE MP_TAG_ROLE				RETURN "ROLE"		
//		CASE MP_TAG_CUSTODY				RETURN "CUSTODY"		
//		CASE MP_TAG_NO_VOTE				RETURN "NO_VOTE"		
//		CASE MP_TAG_VOTED				RETURN "VOTED"	
//		CASE MP_TAG_HOLDUP_CASH			RETURN "HOLDUP_CASH"	
		CASE MP_TAG_HEALTH_BAR			RETURN "HEALTH_BAR"	
		CASE MP_TAG_USING_MENU			RETURN "USING_MENU"	
		CASE MP_TAG_PASSIVE_MODE		RETURN "PASSIVE_MODE"	
		CASE MP_TAG_BIG_TEXT			RETURN "MP_TAG_BIG_TEXT"	
		CASE MP_TAG_CO_DRIVER			RETURN "MP_TAG_CO_DRIVER"	
		CASE MP_TAG_DRIVER				RETURN "MP_TAG_DRIVER"	
//		CASE MP_TAG_POWERPLAY			RETURN "MP_TAG_POWERPLAY"	
		CASE MP_TAG_TAGGED				RETURN "MP_TAG_TAGGED"	
//		CASE MP_TAG_SMOKES				RETURN "MP_TAG_SMOKES"
		CASE MP_TAG_ARROW				RETURN "MP_TAG_ARROW"
		CASE MP_TAG_TYPING				RETURN "MP_TAG_TYPING"
		CASE MP_TAG_PACKAGE_LARGE		RETURN "PACKAGES"	
		CASE MP_TAG_TRACKIFY			RETURN "TRACKIFY"
		CASE MP_TAG_GANG_CEO			RETURN "GANG_CEO"
		CASE MP_TAG_GANG_BIKER			RETURN "GANG_BIKER"
		
		
		CASE	MP_TAG_MC_ROLE_PRESIDENT		RETURN "MP_TAG_MC_ROLE_PRESIDENT"
		CASE	MP_TAG_MC_ROLE_VICE_PRESIDENT	RETURN "MP_TAG_MC_ROLE_VICE_PRESIDENT"
		CASE	MP_TAG_MC_ROLE_ROAD_CAPTAIN	RETURN "MP_TAG_MC_ROLE_ROAD_CAPTAIN"
		CASE	MP_TAG_MC_ROLE_SARGEANT		RETURN "MP_TAG_MC_ROLE_SARGEANT"
		CASE	MP_TAG_MC_ROLE_ENFORCER		RETURN "MP_TAG_MC_ROLE_ENFORCER"
		CASE	MP_TAG_MC_ROLE_PROSPECT		RETURN "MP_TAG_MC_ROLE_PROSPECT"
		CASE	MP_TAG_TRANSMITTER		RETURN "MP_TAG_TRANSMITTER"
		
	ENDSWITCH
	RETURN ""
ENDFUNC
#ENDIF


PROC SET_ICON_VISABILITY(INT iPlayerNum, eMP_TAG paramTag, BOOL paramVisibility)

	IF IS_VALID_OVERHEAD_ITEM(paramTag)
		IF paramVisibility
//			IF NOT IS_BIT_SET(g_sOverHead.iOverHeadItemDisplayed[GET_WEAPON_BITSET(ENUM_TO_INT(paramTag))][iPlayernum], GET_WEAPON_INDEX_BITSET(ENUM_TO_INT(paramTag)))
	//			PRINTLN("[overheads] - SET_MP_GAMER_TAG_VISIBILITY iBitToSet = TRUE for player ", iPlayerNum)
				SET_MP_GAMER_TAG_VISIBILITY (iPlayerNum, paramTag, TRUE)
				
				#IF IS_DEBUG_BUILD
				IF g_TurnOnOverheadDebug
					PRINTLN("[overheads] - SET_ICON_VISABILITY = TRUE ", iPlayerNum)
					PRINTLN("[overheads] - Player GamerTag = ", GET_PLAYER_NAME(INT_TO_NATIVE(PLAYER_INDEX, iPlayerNum)))
					PRINTLN("[overheads] - eMP_Tag = ", GET_STRING_FROM_MP_TAG(paramTag))
				ENDIF
				IF g_bDoArrowCheckDebug
					PRINTLN("[Overheads] - [Arrow Debug] - SET_MP_GAMER_TAG_VISIBILITY = TRUE for tag ", GET_STRING_FROM_MP_TAG(paramTag), ". Player num = ", iPlayerNum)
				ENDIF
				#ENDIF
				SET_BIT(g_sOverHead.iOverHeadItemDisplayed[GET_WEAPON_BITSET(ENUM_TO_INT(paramTag))][iPlayernum], GET_WEAPON_INDEX_BITSET(ENUM_TO_INT(paramTag)))
				
				IF paramTag = MP_TAG_PACKAGE_LARGE
					PRINTLN("[Overheads] - [Arrow Debug] - SET_ICON_VISABILITY = MP_TAG_PACKAGE_LARGE SWITCH ON FLASH ")
					g_sOverHead.bmakelargepackageflash = TRUE
					IF NOT HAS_NET_TIMER_STARTED(g_sOverHead.tFlashtimer)
						START_NET_TIMER(g_sOverHead.tFlashtimer)
					ENDIF
				ENDIF
		ELSE
			
			IF IS_BIT_SET(g_sOverHead.iOverHeadItemDisplayed[GET_WEAPON_BITSET(ENUM_TO_INT(paramTag))][iPlayernum], GET_WEAPON_INDEX_BITSET(ENUM_TO_INT(paramTag)))
				SET_MP_GAMER_TAG_VISIBILITY (iPlayerNum, paramTag, FALSE)
	//			PRINTLN("[overheads] - SET_MP_GAMER_TAG_VISIBILITY iBitToSet = FALSE for player ", iPlayerNum)
				
				CLEAR_BIT(g_sOverHead.iOverHeadItemDisplayed[GET_WEAPON_BITSET(ENUM_TO_INT(paramTag))][iPlayernum], GET_WEAPON_INDEX_BITSET(ENUM_TO_INT(paramTag)))
				#IF IS_DEBUG_BUILD
				IF g_TurnOnOverheadDebug
					PRINTLN("[overheads] - SET_ICON_VISABILITY = FALSE ", iPlayerNum)
					PRINTLN("[overheads] - Player GamerTag = ", GET_PLAYER_NAME(INT_TO_NATIVE(PLAYER_INDEX, iPlayerNum)))
					PRINTLN("[overheads] - eMP_Tag = ", GET_STRING_FROM_MP_TAG(paramTag))
				ENDIF
				IF g_bDoArrowCheckDebug
					PRINTLN("[Overheads] - [Arrow Debug] - SET_MP_GAMER_TAG_VISIBILITY = FALSE for tag ", GET_STRING_FROM_MP_TAG(paramTag), ". Player num = ", iPlayerNum)
				ENDIF
				#ENDIF
				
				IF paramTag = MP_TAG_PACKAGE_LARGE
					PRINTLN("[Overheads] - [Arrow Debug] - SET_ICON_VISABILITY = MP_TAG_PACKAGE_LARGE SWITCH OFF FLASH ")
					g_sOverHead.bmakelargepackageflash = FALSE
	
				ENDIF
//			ELSE
//				#IF IS_DEBUG_BUILD
//				IF g_TurnOnOverheadDebugFullOn
//					PRINTLN("[overheads] - SET_ICON_VISABILITY FAILED TURN OFF - Already off ", iPlayerNum)
//					PRINTLN("[overheads] - Player GamerTag = ", GET_PLAYER_NAME(INT_TO_NATIVE(PLAYER_INDEX, iPlayerNum)))
//					PRINTLN("[overheads] - eMP_Tag = ", GET_STRING_FROM_MP_TAG(paramTag))
//				ENDIF
//				IF g_bDoArrowCheckDebug
//					PRINTLN("[Overheads] - [Arrow Debug] - SET_ICON_VISABILITY = FALSE for tag ", GET_STRING_FROM_MP_TAG(paramTag), ". Player num = ", iPlayerNum, ", tag is already off.")
//				ENDIF
//				#ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC
PROC CLEANUP_ABOVE_HEAD_DISPLAY( INT iPlayernum, PLAYER_INDEX aPlayer)
	
//	IF IS_BIT_SET(g_sOverHead.iOverHeadSetUp, iPlayernum)
		
		#IF IS_DEBUG_BUILD
		IF g_TurnOnOverheadDebugFullOn
			PRINTLN("[overheads] - CLEANUP_ABOVE_HEAD_DISPLAY - been called for player ", iPlayernum)
			DEBUG_PRINTCALLSTACK()
			PRINTLN("[overheads] - CLEANUP_ABOVE_HEAD_DISPLAY - IS_MP_GAMER_TAG_MOVIE_ACTIVE = ", IS_MP_GAMER_TAG_MOVIE_ACTIVE())
			PRINTLN("[overheads] - CLEANUP_ABOVE_HEAD_DISPLAY - IS_MP_GAMER_TAG_ACTIVE = ", IS_MP_GAMER_TAG_ACTIVE(iPlayernum))
			PRINTLN("[overheads] - CLEANUP_ABOVE_HEAD_DISPLAY - IS_MP_GAMER_TAG_FREE = ", IS_MP_GAMER_TAG_FREE(iPlayernum))
		ENDIF
		#ENDIF
		
		IF IS_MP_GAMER_TAG_MOVIE_ACTIVE()
			IF IS_MP_GAMER_TAG_ACTIVE(iPlayernum)
				SET_ALL_MP_GAMER_TAGS_VISIBILITY(iPlayernum, FALSE)
			ENDIF
			IF NOT IS_MP_GAMER_TAG_ACTIVE(iPlayernum)
			AND NOT IS_MP_GAMER_TAG_FREE(iPlayernum)
				//Being Deleted already
			ELSE
				IF IS_MP_GAMER_TAG_ACTIVE(iPlayernum)
					REMOVE_MP_GAMER_TAG(iPlayerNum)
					PRINTLN("[Overheads] - REMOVE_MP_GAMER_TAG call 2. Player = ", iPlayernum)
				ENDIF
			ENDIF
		ENDIF
		
		g_sOverHead.iOverHeadPlayerNumber[iPlayernum] = -1
		CLEAR_BIT(g_sOverHead.iOverHeadSetUp, iPlayernum)
		REFRESH_OVERHEAD_DISPLAY(aPlayer)
		
		INT I
		FOR I = 0 TO eMP_TAG_BITSETS-1
			g_sOverHead.iOverHeadItemDisplayed[I][iPlayernum] = 0
		ENDFOR

//	ENDIF
		
ENDPROC

PROC CLEANUP_ABOVE_HEAD_DISPLAY_JUST_SCALEFORM( INT iPlayernum)
	IF IS_BIT_SET(g_sOverHead.iOverHeadSetUp, iPlayernum)
		IF IS_MP_GAMER_TAG_ACTIVE(iPlayernum)
			#IF IS_DEBUG_BUILD
			IF g_TurnOnOverheadDebugFullOn
				PRINTLN("[overheads] - CLEANUP_ABOVE_HEAD_DISPLAY_JUST_SCALEFORM - cleaning up player ", iPlayernum)
			ENDIF
			#ENDIF
			REMOVE_MP_GAMER_TAG(iPlayerNum)
			PRINTLN("[Overheads] - REMOVE_MP_GAMER_TAG call 1. Player = ", iPlayernum)
		ENDIF
		g_sOverHead.iOverHeadPlayerNumber[iPlayernum] = -1
		CLEAR_BIT(g_sOverHead.iOverHeadSetUp, iPlayernum)
		INT I 
		FOR I = 0 TO eMP_TAG_BITSETS-1
			g_sOverHead.iOverHeadItemDisplayed[I][iPlayernum] = 0
		ENDFOR
	ELSE
		#IF IS_DEBUG_BUILD
		IF g_TurnOnOverheadDebugFullOn
			PRINTLN("[overheads] - CLEANUP_ABOVE_HEAD_DISPLAY_JUST_SCALEFORM - failed for player ", iPlayernum)
		ENDIF
		#ENDIF
	ENDIF
	
ENDPROC




PROC HIDE_ABOVE_HEAD_DISPLAY(INT iPlayerNum, PLAYER_INDEX aPlayer)

	INT i
	
//	#IF IS_DEBUG_BUILD
//	IF g_TurnOnOverheadDebug
//		DEBUG_PRINTCALLSTACK()
//		NET_NL()NET_PRINT("[overhead] HIDE_ABOVE_HEAD_DISPLAY CALLED Still need check ")NET_PRINT_INT(iPlayerNum)
//		FOR I = 0 TO eMP_TAG_BITSETS-1
//			NET_NL()NET_PRINT("		- g_sOverHead.iOverHeadItemDisplayed[I][iPlayernum] = ")NET_PRINT_INT(g_sOverHead.iOverHeadItemDisplayed[I][iPlayernum])
//		ENDFOR
//		
//	ENDIF
//	#ENDIF

	
	FOR i = 0 TO eMP_TAG_BITSETS-1
//		IF g_sOverHead.iOverHeadItemDisplayed[I][iPlayernum] != 0
		IF NOT IS_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_NOTHING, aPlayer)
			IF IS_MP_GAMER_TAG_ACTIVE(iPlayerNum)
				SET_ALL_MP_GAMER_TAGS_VISIBILITY(iPlayerNum, FALSE)
			ENDIF
		
			#IF IS_DEBUG_BUILD
			IF g_TurnOnOverheadDebug
				DEBUG_PRINTCALLSTACK()
				PRINTLN("[overheads] - HIDE_ABOVE_HEAD_DISPLAY called for Local player num ", iPlayerNum, ", Gamer tag = ", GET_PLAYER_NAME(aPlayer))
			ENDIF
			#ENDIF
			
			g_sOverHead.iOverHeadItemDisplayed[I][iPlayernum] = 0
			REFRESH_OVERHEAD_DISPLAY(aPlayer)
			SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_NOTHING, TRUE, aPlayer)
		ENDIF
//		ENDIF
	ENDFOR
	
ENDPROC


PROC HIDE_ALL_ABOVE_HEAD_DISPLAY_FOR_PLAYER_NOW(PLAYER_INDEX PlayerID, INT iPlayernum, BOOL bResetEvents = FALSE)
	
	IF g_sOverHead.bPlayerIsAlive[iPlayernum]
	
		#IF IS_DEBUG_BUILD
		IF g_TurnOnOverheadDebug
			PRINTLN("[overheads] - HIDE_ALL_ABOVE_HEAD_DISPLAY_FOR_PLAYER_NOW called for Local player num ", iPlayerNum)
			DEBUG_PRINTCALLSTACK()
		ENDIF
		#ENDIF
		
		IF IS_MP_GAMER_TAG_MOVIE_ACTIVE()
			IF IS_MP_GAMER_TAG_ACTIVE(iPlayerNum)
				SET_ALL_MP_GAMER_TAGS_VISIBILITY(iPlayerNum, FALSE)
			ENDIF
		ENDIF
		
//		INT iTag
//		FOR  iTag = 0 TO (ENUM_TO_INT(MP_TAG_VOTED))
//			SET_MP_GAMER_TAG_VISIBILITY (iPlayerNum,  INT_TO_ENUM(eMP_TAG, iTag), FALSE)
//		ENDFOR

		INT I
		FOR I = 0 TO eMP_TAG_BITSETS-1
			g_sOverHead.iOverHeadItemDisplayed[I][iPlayernum] = 0
		ENDFOR
		
		g_sOverHead.bPlayerIsAlive[iPlayernum] = FALSE
		IF bResetEvents
			RESET_OVERHEAD_EVENT_BITSET_FOR_PLAYER(PlayerID)
		ENDIF
		REFRESH_OVERHEAD_DISPLAY(PlayerID)
		
	ENDIF	
	
ENDPROC


PROC HIDE_ABOVE_HEAD_DISPLAY_ONCE(INT iPlayerNum, PLAYER_INDEX aPlayer)

	INT I

	FOR I = 0 TO eMP_TAG_BITSETS-1
		IF (g_sOverHead.iOverHeadItemDisplayed[I][iPlayernum] != 0)
		
			IF IS_MP_GAMER_TAG_ACTIVE(iPlayerNum)
				SET_ALL_MP_GAMER_TAGS_VISIBILITY(iPlayerNum, FALSE)
			ENDIF
		
			#IF IS_DEBUG_BUILD
			IF g_TurnOnOverheadDebug
				DEBUG_PRINTCALLSTACK()
				PRINTLN("[overheads] - HIDE_ABOVE_HEAD_DISPLAY_ONCE called for Local player num ", iPlayerNum, " - aPlayer = ", GET_PLAYER_NAME(aPlayer))
			ENDIF
			#ENDIF
			
			g_sOverHead.iOverHeadItemDisplayed[I][iPlayernum] = 0
			
			REFRESH_OVERHEAD_DISPLAY(aPlayer)
			
		ENDIF
	ENDFOR
	
ENDPROC

PROC PROCESS_HEAD_LARGE_PACKAGE_FLASH(INT iPlayerNum)

	IF IS_MP_GAMER_TAG_ACTIVE(iPlayerNum)
		IF g_sOverHead.bmakelargepackageflash = TRUE
			IF IS_BIT_SET(GlobalplayerBD[iPlayerNum].iMyOverHeadInventory, ENUM_TO_INT(MP_TAG_PACKAGE_LARGE))
	 
				//g_sOverHead.tFlashtimer
				//GET_NET_TIMER_OFFSET(g_sOverHead.tFlashtimer, INT Offset)
				 
				INT CURRENTTIME = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(g_sOverHead.tFlashtimer)
				//PRINTLN("[KW] PROCESS_HEAD_LARGE_PACKAGE_FLASH CURRENTTIME = ",CURRENTTIME )
			//	PRINTLN("[KW] PROCESS_HEAD_LARGE_PACKAGE_FLASH g_sOverHead.ival  = ",g_sOverHead.fval  )
			//	PRINTLN("[KW] PROCESS_HEAD_LARGE_PACKAGE_FLASH g_sOverHead.ival2  = ",g_sOverHead.fval2 )
				CURRENTTIME = CURRENTTIME +ROUND(-177.0 / 0.8) // -760 and 0.8
				IF CURRENTTIME > ROUND(UNIVERSAL_HUD_ALPHA * 4.0)  // 2
					RESET_NET_TIMER(g_sOverHead.tFlashtimer)
					START_NET_TIMER(g_sOverHead.tFlashtimer)
					CURRENTTIME = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(g_sOverHead.tFlashtimer)
					PRINTLN("[KW] PROCESS_HEAD_LARGE_PACKAGE_FLASH RESETTIMER CURRENTTIME = ",CURRENTTIME )
				ELIF CURRENTTIME > UNIVERSAL_HUD_ALPHA 
					CURRENTTIME = UNIVERSAL_HUD_ALPHA
				ELIF CURRENTTIME < 0
					CURRENTTIME  = 0
					//RESET_NET_TIMER(g_sOverHead.tFlashtimer)
				//	START_NET_TIMER(g_sOverHead.tFlashtimer)
					//CURRENTTIME = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(g_sOverHead.tFlashtimer)
					PRINTLN("[KW] PROCESS_HEAD_LARGE_PACKAGE_FLASH RESETTIMER CURRENTTIME 2 = ",CURRENTTIME )
				ENDIF
				
				
				//PRINTLN("[KW] PROCESS_HEAD_LARGE_PACKAGE_FLASH CURRENTTIME final =  ",CURRENTTIME )
				SET_MP_GAMER_TAG_ALPHA(iPlayerNum, MP_TAG_PACKAGE_LARGE, CURRENTTIME) //190 full
			ENDIF
		ENDIF
	ENDIF
ENDPROC


PROC REFRESH_ALL_OVERHEAD_DISPLAY()
	INT J
	PLAYER_INDEX PlayerId
	FOR J = 0 TO NUM_NETWORK_PLAYERS-1
		
		PlayerId = INT_TO_NATIVE(PLAYER_INDEX, J)
		IF NOT (PlayerId = INVALID_PLAYER_INDEX())
			CLEANUP_ABOVE_HEAD_DISPLAY(J,PlayerId )
		ENDIF
		
	ENDFOR
ENDPROC

PROC REFRESH_PLAYER_OVERHEAD_DISPLAY(INT iPlayerNum, PLAYER_INDEX playerId)
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[overheads] - REFRESH_PLAYER_OVERHEAD_DISPLAY called for player ", iPlayerNum)
		DEBUG_PRINTCALLSTACK()
	#ENDIF
	
	IF PlayerId != INVALID_PLAYER_INDEX()
		CLEANUP_ABOVE_HEAD_DISPLAY(iPlayerNum, PlayerId)
	ENDIF
	
ENDPROC


PROC HANDLE_COLOUR_FOR_HEISTS(PLAYER_INDEX playerID, INT iPlayernum, eMP_TAG eTag)
	
	IF Is_Player_Currently_On_MP_Heist_Planning(playerID)
	OR Is_Player_Currently_On_MP_Heist(playerID)
	OR MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.bLaunchedHeist
		SET_MP_GAMER_TAG_COLOUR(iPlayernum, eTag, HUD_COLOUR_FRIENDLY)
		PRINTLN("[overheads] - set over head for player ", iPlayernum, ", to HUD_COLOUR_FRIENDLY")
	ELSE
		SET_MP_GAMER_TAG_COLOUR(iPlayernum, eTag, GET_PLAYER_HUD_COLOUR(playerID, GET_PLAYER_TEAM(playerID)))
		PRINTLN("[overheads] - set over head for player ", iPlayernum, ", to HUD colour for team.")
	ENDIF
	
ENDPROC


PROC HANDLE_COLOUR_FOR_IMPROMPTU_DEATHMATCH(PLAYER_INDEX playerID, INT iPlayernum, eMP_TAG eTag)

	IF IS_ON_IMPROMPTU_DEATHMATCH_GLOBAL_SET()
		IF IS_BIT_SET(GlobalplayerBD_FM[iPlayernum].iXPBitset, ciGLOBAL_XP_BIT_ON_IMPROMPTU_DM)
		AND (g_sImpromptuVars.playerImpromptuRival = playerID)
			SET_MP_GAMER_TAG_COLOUR(iPlayernum, eTag, GET_PLAYER_HUD_COLOUR(playerID))
		ELSE
			SET_MP_GAMER_TAG_COLOUR(iPlayernum, eTag, HUD_COLOUR_WHITE)
		ENDIF
	ELSE
		SET_MP_GAMER_TAG_COLOUR(iPlayernum, eTag, GET_PLAYER_HUD_COLOUR(playerID, GET_PLAYER_TEAM(playerID)))
	ENDIF
	
ENDPROC

PROC HANDLE_SPECTATOR_TARGET_OVERHEAD_COLOUR(PLAYER_INDEX playerID, INT iPlayernum, eMP_TAG eTag, PED_INDEX spectatedPed)
	
	IF IS_PLAYER_SPECTATING(PLAYER_ID())
	OR (DID_I_JOIN_MISSION_AS_SPECTATOR() AND IS_A_SPECTATOR_CAM_RUNNING())
		IF DOES_ENTITY_EXIST(spectatedPed)
			PLAYER_INDEX playerBeingSpectated = NETWORK_GET_PLAYER_INDEX_FROM_PED(spectatedPed)
			IF playerBeingSpectated = playerID
				SET_MP_GAMER_TAG_COLOUR(iPlayernum, eTag, HUD_COLOUR_FRIENDLY)
			ENDIF
		ENDIF
	ELSE
		HANDLE_COLOUR_FOR_IMPROMPTU_DEATHMATCH(playerID, iPlayernum, eTag)
	ENDIF
	
ENDPROC


PROC MAINTAIN_HEIST_OH_COLOUR(PLAYER_INDEX playerID, INT iPlayerNum)
	
	INT iTag
	
	IF IS_BIT_SET(MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.iOnHeistBitset, iPlayerNum) != MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.bLaunchedHeist
		FOR iTag = 0 TO (COUNT_OF(eMP_TAG)-1)
			IF iTag != (ENUM_TO_INT(MP_TAG_CREW_TAG))
			AND IS_VALID_OVERHEAD_ITEM(INT_TO_ENUM(eMP_TAG, iTag))
				HANDLE_COLOUR_FOR_HEISTS(playerId, iPlayerNum, INT_TO_ENUM(eMP_TAG, iTag))
			ENDIF
		ENDFOR
		IF NOT MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.bLaunchedHeist
			CLEAR_BIT(MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.iOnHeistBitset, iPlayerNum)
		ELSE
			SET_BIT(MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.iOnHeistBitset, iPlayerNum)
		ENDIF
	ENDIF
	
ENDPROC


FUNC BOOL CREATE_PLAYER_OVERHEADS(PLAYER_INDEX playerID, INT iPlayernum) //, INT iPlayerTeam)

	#IF IS_DEBUG_BUILD
	IF g_TurnOnOverheadDebug
	PRINTLN("[Overheads] - [Crew Hierarchy] - CREATE_PLAYER_OVERHEADS has been called.")
	ENDIF
	#ENDIF
	IF IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_CAN_DISPLAY, playerID) = FALSE
		#IF IS_DEBUG_BUILD
			IF g_TurnOnOverheadDebug
				PRINTLN("[overheads] - CREATE_PLAYER_OVERHEADS = FALSE DISPLAYOVERHEADS_LOGIC_CAN_DISPLAY = FALSE for player = ", NATIVE_TO_INT(playerID), " - ", GET_PLAYER_NAME(playerID), " - Reason is ", CAN_DISPLAY_OVERHEAD_STATS(playerID))
			ENDIF
		#ENDIF
		HIDE_ABOVE_HEAD_DISPLAY_ONCE(iPlayernum, playerID)
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(g_sOverHead.iOverHeadSetUp, iPlayernum)
	AND IS_MP_GAMER_TAG_ACTIVE(iPlayerNum)
//		DEBUG_PRINTCALLSTACK()
//		NET_NL()NET_PRINT("[overhead] CREATE_PLAYER_OVERHEADS = TRUE for player = ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
		RETURN TRUE
	ENDIF

	IF NOT IS_MP_GAMER_TAG_ACTIVE(iPlayerNum)
		IF NOT MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.bPauseMenuActive[iPlayerNum]
			IF IS_MP_GAMER_TAG_FREE(iPlayerNum)
				#IF IS_DEBUG_BUILD
					IF g_TurnOnOverheadDebugFullOn
						PRINTLN("[overheads] - CREATE_PLAYER_OVERHEADS - setting up gamer tag for player ", iPlayernum)
					ENDIF
				#ENDIF
				GAMER_HANDLE gamerHandle = GET_GAMER_HANDLE_PLAYER(PlayerID)	
				NETWORK_CLAN_DESC PlayerClan
				BOOL isRockstar
				BOOL isPrivate
				
				
				IF IS_PLAYER_IN_ACTIVE_CLAN(gamerHandle)
					PlayerClan = GET_GAMER_CREW(gamerHandle)
					g_sOverHead.ClanTag[iPlayernum] = PlayerClan.ClanTag
	//				IF IS_PLAYER_CLAN_LEADER(gamerHandle)
	//					isLeader = 0
	//				ELSE
	//					isLeader = 1
	//				ENDIF
					IF IS_PLAYER_CLAN_SYSTEM(gamerHandle)
						isRockstar = TRUE
					ELSE
						isRockstar = FALSE
					ENDIF
					
					IF IS_PLAYER_CLAN_PRIVATE(gamerHandle)
						isPrivate = TRUE
					ELSE
						isPrivate = FALSE
					ENDIF
					
					#IF IS_DEBUG_BUILD
					IF g_TurnOnOverheadDebugFullOn
						PRINTLN("[overheads] - CREATE_PLAYER_OVERHEADS - g_sOverHead.ClanTag[", iPlayernum, "] is = ", g_sOverHead.ClanTag[iPlayernum])
					ENDIF
					#ENDIF
				ELSE
					g_sOverHead.ClanTag[iPlayernum] = ""
					#IF IS_DEBUG_BUILD
					IF g_TurnOnOverheadDebugFullOn
						PRINTLN("[overheads] - CREATE_PLAYER_OVERHEADS - NETWORK_CLAN_SERVICE_IS_VALID() = FALSE g_sOverHead.ClanTag[", iPlayernum, "] is = ", g_sOverHead.ClanTag[iPlayernum])
					ENDIF
					#ENDIF
				ENDIF
				//INT iplayerRank = GET_PLAYER_CnC_RANK(PlayerId)
				//PRINTLN("[overheads] - PLAYERS_OVERHEAD_COMMON_FUNCTIONS - iplayerRank = ", iplayerRank)
	//			CREATE_MP_GAMER_TAG(iPlayerNum, GET_PLAYER_NAME(PlayerID), isPrivate,  isRockstar, PlayerClan.ClanTag, PlayerClan.RankOrder) 
				
				#IF IS_DEBUG_BUILD
					BOOL bIsPlayerInActiveClan = IS_PLAYER_IN_ACTIVE_CLAN(gamerHandle)
					PRINTLN("[Overheads] - [Crew Hierarchy] - ********************* Player ", iPlayernum, " Clan Data Start *********************")
					PRINTLN("[Overheads] - [Crew Hierarchy] - player in active clan = ", bIsPlayerInActiveClan)
					PRINTLN("[Overheads] - [Crew Hierarchy] - player is Rockstar = ", isRockstar)
					PRINTLN("[Overheads] - [Crew Hierarchy] - player clan is private = ", isPrivate)
					PRINTLN("[Overheads] - [Crew Hierarchy] - player clan id = ", PlayerClan.Id)
					PRINTLN("[Overheads] - [Crew Hierarchy] - player clan name = ", PlayerClan.ClanName)
					PRINTLN("[Overheads] - [Crew Hierarchy] - player clan tag = ", PlayerClan.ClanTag)
					PRINTLN("[Overheads] - [Crew Hierarchy] - player clan member count = ", PlayerClan.MemberCount)
					PRINTLN("[Overheads] - [Crew Hierarchy] - player clan is system clan = ", PlayerClan.IsSystemClan)
					PRINTLN("[Overheads] - [Crew Hierarchy] - player clan is open clan = ", PlayerClan.IsOpenClan)
					PRINTLN("[Overheads] - [Crew Hierarchy] - player clan rank name = ", PlayerClan.RankName)
					PRINTLN("[Overheads] - [Crew Hierarchy] - player clan rank order = ", PlayerClan.RankOrder)
					PRINTLN("[Overheads] - [Crew Hierarchy] - player clan created time = ", PlayerClan.CreatedTimePosix)
					PRINTLN("[Overheads] - [Crew Hierarchy] - player clan red = ", PlayerClan.ClanColor_Red)
					PRINTLN("[Overheads] - [Crew Hierarchy] - player clan green = ", PlayerClan.ClanColor_Green)
					PRINTLN("[Overheads] - [Crew Hierarchy] - player clan blue = ", PlayerClan.ClanColor_Blue)
					PRINTLN("[Overheads] - [Crew Hierarchy] - ********************* Player ", iPlayernum, " Clan Data End *********************")
				#ENDIF
				CREATE_MP_GAMER_TAG_WITH_CREW_COLOR(iPlayerNum, GET_PLAYER_NAME(PlayerID), isPrivate,  isRockstar, PlayerClan.ClanTag, PlayerClan.RankOrder, g_Private_LocalPlayerCrew_Colour_Red, g_Private_LocalPlayerCrew_Colour_Green, g_Private_LocalPlayerCrew_Colour_Blue)
			
				#IF IS_DEBUG_BUILD
				IF NOT g_Private_IsCrewColourSet
					PRINTLN("SCRIPT ASSERT - CREATE_MP_GAMER_TAG_WITH_CREW_COLOR been called using g_Private_LocalPlayerCrew_Colour_Red, g_Private_LocalPlayerCrew_Colour_Green, g_Private_LocalPlayerCrew_Colour_Blue - g_Private_IsCrewColourSet = FALSE though.")
				ENDIF
				#ENDIF
				
				INT iTag
				HUD_COLOURS hudColourToSet
				INT iplayerteam = GET_PLAYER_TEAM(playerId)

				
				IF IS_PLAYER_USING_ARENA()
				AND NOT IS_ARENA_WAR_UGC_RACE()
				AND (iplayerteam >= 0 AND iplayerteam < FMMC_MAX_TEAMS)
					hudColourToSet = GET_HUD_COLOUR_FOR_FMMC_TEAM(iplayerteam, player_id(), FALSE)

				ELSE
					hudColourToSet = GET_PLAYER_HUD_COLOUR(playerID) // GET_COLOUR_FOR_PLAYER_NAME(iPlayerTeam)
				ENDIF
			
				
				#IF IS_DEBUG_BUILD
					IF g_TurnOnOverheadDebugFullOn
					PRINTLN("[overheads] - SET_MP_GAMER_TAG_COLOUR - playerID = ", NATIVE_TO_INT(playerID))
					PRINTLN("[overheads] - SET_MP_GAMER_TAG_COLOUR - COLOUR = ", ENUM_TO_INT(hudColourToSet))
					PRINTLN("[overheads] - SET_MP_GAMER_TAG_COLOUR - GET_PLAYER_TEAM = ", GET_PLAYER_TEAM(PlayerID))
					ENDIF
				#ENDIF
				FOR  iTag = 0 TO (COUNT_OF(eMP_TAG)-1)
					PRINTLN("[overheads] - CREATE_PLAYER_OVERHEADS - editing MP_TAG ", iTag)
					IF iTag != (ENUM_TO_INT(MP_TAG_CREW_TAG))
					AND IS_VALID_OVERHEAD_ITEM(INT_TO_ENUM(eMP_TAG, iTag))
						IF INT_TO_ENUM(eMP_TAG, iTag) = MP_TAG_HEALTH_BAR
							SET_MP_GAMER_TAG_HEALTH_BAR_COLOUR(iPlayerNum, hudColourToSet)
							SET_MP_GAMER_TAG_ALPHA(iPlayerNum,MP_TAG_HEALTH_BAR, 255 )
							PRINTLN("[overheads] - CREATE_PLAYER_OVERHEADS - SET_MP_GAMER_TAG_COLOUR - MP_TAG_HEALTH_BAR = ", HUD_COLOUR_AS_STRING(hudColourToSet))
						ELSE
							SET_MP_GAMER_TAG_COLOUR(iPlayerNum, INT_TO_ENUM(eMP_TAG, iTag), hudColourToSet)
							PRINTLN("[overheads] - CREATE_PLAYER_OVERHEADS - setting SET_MP_GAMER_TAG_COLOUR of MP_TAG ", iTag)
						ENDIF

						IF iTag != (ENUM_TO_INT(MP_TAG_HEALTH_BAR))
						AND iTag != (ENUM_TO_INT(MP_TAG_GAMER_NAME))
						AND iTag != (ENUM_TO_INT(MP_TAG_PACKAGE_LARGE))
						
							IF  IS_PLAYER_USING_ARENA()
							AND NOT IS_ARENA_WAR_UGC_RACE()
								SET_MP_GAMER_TAG_ALPHA(iPlayerNum, INT_TO_ENUM(eMP_TAG, iTag), UNIVERSAL_PLAYER_NAME_ALPHA)
								#IF IS_DEBUG_BUILD
								IF g_TurnOnOverheadDebug
								PRINTLN("[Overheads] - call 7 - setting gamertag alpha to UNIVERSAL_HUD_ALPHA = ", UNIVERSAL_PLAYER_NAME_ALPHA)	
								ENDIF
								#ENDIF
							ELSE
								SET_MP_GAMER_TAG_ALPHA(iPlayerNum, INT_TO_ENUM(eMP_TAG, iTag), UNIVERSAL_HUD_ALPHA)
								
								#IF IS_DEBUG_BUILD
								IF g_TurnOnOverheadDebug
									PRINTLN("[Overheads] - call 7 - setting gamertag alpha to UNIVERSAL_HUD_ALPHA = ", UNIVERSAL_HUD_ALPHA)		
								ENDIF
								#ENDIF
							ENDIF	

						ENDIF	
						
					ENDIF
				ENDFOR
				//Default the clan tag colour to white
				SET_MP_GAMER_TAG_COLOUR(iPlayerNum, MP_TAG_CREW_TAG, HUD_COLOUR_WHITE)	
				SET_MP_GAMER_TAG_ALPHA		(iPlayerNum, MP_TAG_GAMER_NAME, UNIVERSAL_PLAYER_NAME_ALPHA)
				#IF IS_DEBUG_BUILD
				IF g_TurnOnOverheadDebug
					PRINTLN("[Overheads] - call 6 - setting gamertag alpha to UNIVERSAL_PLAYER_NAME_ALPHA = ", UNIVERSAL_PLAYER_NAME_ALPHA)		
				ENDIF
				#ENDIF
				SET_MP_GAMER_TAG_ALPHA		(iPlayerNum, MP_TAG_GAMER_NAME_NEARBY, UNIVERSAL_PLAYER_NAME_ALPHA)	
				#IF IS_DEBUG_BUILD
				IF g_TurnOnOverheadDebug
					PRINTLN("[Overheads] - call 5 - setting gamertag alpha to UNIVERSAL_PLAYER_NAME_ALPHA = ", UNIVERSAL_PLAYER_NAME_ALPHA)			
				ENDIF
				#ENDIF	
				g_sOverHead.iOverHeadPlayerNumber[iPlayernum] = iPlayerNum
				CLEAR_BIT(g_sOverHead.iOverHeadSetUp, iPlayernum)
				
				#IF IS_DEBUG_BUILD
				IF g_TurnOnOverheadDebug
					PRINTLN("[overheads] - calling MAINTAIN_HEIST_OH_COLOUR at point 1")
				ENDIF
				#ENDIF
				MAINTAIN_HEIST_OH_COLOUR(playerId, iPlayerNum)
			
		//		EXIT

			ELSE
				
				#IF IS_DEBUG_BUILD
					IF g_TurnOnOverheadDebug
						PRINTLN("[overheads] - CREATE_PLAYER_OVERHEADS = FALSE IS_MP_GAMER_TAG_FREE = FALSE for player = ", NATIVE_TO_INT(playerID), " - ", GET_PLAYER_NAME(playerID))
					ENDIF
				#ENDIF
				
			ENDIF
		ENDIF
	ELIF NOT IS_BIT_SET(g_sOverHead.iOverHeadSetUp, iPlayernum)
		SET_BIT(g_sOverHead.iOverHeadSetUp, iPlayernum)
		IF  IS_MP_GAMER_TAG_ACTIVE(iPlayerNum)
			IF NOT Is_Player_Currently_On_MP_Heist(PLAYER_ID())
			OR NOT Is_Player_Currently_On_MP_Heist_Planning(PLAYER_ID())
			OR NOT GB_IS_PLAYER_ON_FM_GANGOPS_MISSION(PLAYER_ID())
				INT iTag

				HUD_COLOURS hudColourToSet
				INT iplayerteam = GET_PLAYER_TEAM(playerId)

				
				
				IF (IS_PLAYER_USING_ARENA() OR (IS_KING_OF_THE_HILL() AND USE_KOTH_TEAM_COLOURS()))
				AND NOT IS_ARENA_WAR_UGC_RACE()
				AND (iplayerteam >= 0 AND iplayerteam < FMMC_MAX_TEAMS)
					hudColourToSet = GET_HUD_COLOUR_FOR_FMMC_TEAM(iplayerteam, player_id(), FALSE)
				ELSE
					hudColourToSet = GET_PLAYER_HUD_COLOUR(playerID) // GET_COLOUR_FOR_PLAYER_NAME(iPlayerTeam)
				ENDIF
				
				
				PRINTLN("[overheads] - SET_MP_GAMER_TAG_COLOUR 2 - playerID = ", NATIVE_TO_INT(playerID))
				PRINTLN("[overheads] - SET_MP_GAMER_TAG_COLOUR 2 - COLOUR = ", ENUM_TO_INT(hudColourToSet))
				PRINTLN("[overheads] - SET_MP_GAMER_TAG_COLOUR 2 - GET_PLAYER_TEAM = ", GET_PLAYER_TEAM(PlayerID))
				
				FOR  iTag = 0 TO (COUNT_OF(eMP_TAG)-1)
					#IF IS_DEBUG_BUILD
						IF g_TurnOnOverheadDebug
							PRINTLN("[overheads] - CREATE_PLAYER_OVERHEADS - editing MP_TAG ", iTag)
						ENDIF
					#ENDIF
					IF iTag != (ENUM_TO_INT(MP_TAG_CREW_TAG))
					AND IS_VALID_OVERHEAD_ITEM(INT_TO_ENUM(eMP_TAG, iTag))
						IF IS_MP_GAMER_TAG_ACTIVE(iPlayerNum)
							IF INT_TO_ENUM(eMP_TAG, iTag) = MP_TAG_HEALTH_BAR
								
								SET_MP_GAMER_TAG_HEALTH_BAR_COLOUR(iPlayerNum, hudColourToSet)
								SET_MP_GAMER_TAG_ALPHA(iPlayerNum,MP_TAG_HEALTH_BAR, 255 )
								PRINTLN("[overheads] - CREATE_PLAYER_OVERHEADS - SET_MP_GAMER_TAG_COLOUR 2 - MP_TAG_HEALTH_BAR = ", HUD_COLOUR_AS_STRING(hudColourToSet))
							ELSE
								SET_MP_GAMER_TAG_COLOUR		(iPlayerNum, INT_TO_ENUM(eMP_TAG, iTag), hudColourToSet)
								#IF IS_DEBUG_BUILD
								IF g_TurnOnOverheadDebug
								PRINTLN("[overheads] - CREATE_PLAYER_OVERHEADS - setting SET_MP_GAMER_TAG_COLOUR of MP_TAG ", iTag)
								ENDIF
								#ENDIF
							ENDIF

							IF iTag != (ENUM_TO_INT(MP_TAG_HEALTH_BAR))
							AND iTag != (ENUM_TO_INT(MP_TAG_GAMER_NAME))
							AND iTag != (ENUM_TO_INT(MP_TAG_PACKAGE_LARGE))

														
								IF  IS_PLAYER_USING_ARENA()
								AND NOT IS_ARENA_WAR_UGC_RACE()
									SET_MP_GAMER_TAG_ALPHA(iPlayerNum, INT_TO_ENUM(eMP_TAG, iTag), UNIVERSAL_PLAYER_NAME_ALPHA)
									#IF IS_DEBUG_BUILD
									IF g_TurnOnOverheadDebug
										PRINTLN("[Overheads] - call 4 - setting gamertag alpha to UNIVERSAL_PLAYER_NAME_ALPHA = ", UNIVERSAL_PLAYER_NAME_ALPHA)	
									ENDIF
									#ENDIF
								ELSE
									SET_MP_GAMER_TAG_ALPHA(iPlayerNum, INT_TO_ENUM(eMP_TAG, iTag), UNIVERSAL_HUD_ALPHA)
									PRINTLN("[overheads] - CREATE_PLAYER_OVERHEADS - setting SET_MP_GAMER_TAG_ALPHA of MP_TAG ", iTag)
									#IF IS_DEBUG_BUILD
									IF g_TurnOnOverheadDebug
										PRINTLN("[Overheads] - call 4 - setting gamertag alpha to UNIVERSAL_HUD_ALPHA = ", UNIVERSAL_HUD_ALPHA)		
									ENDIF
									#ENDIF
								ENDIF	

							ENDIF
						ENDIF
					ENDIF
				ENDFOR

				IF ARE_STRINGS_EQUAL(g_sOverHead.ClanTag[NATIVE_TO_INT(PLAYER_ID())], g_sOverHead.ClanTag[iPlayernum] )
					SET_MP_GAMER_TAG_COLOUR(iPlayerNum, MP_TAG_CREW_TAG, HUD_COLOUR_YELLOWLIGHT)	
				ELSE
					SET_MP_GAMER_TAG_COLOUR(iPlayerNum, MP_TAG_CREW_TAG, HUD_COLOUR_WHITE)	
				ENDIF
				SET_MP_GAMER_TAG_ALPHA		(iPlayerNum, MP_TAG_GAMER_NAME, UNIVERSAL_PLAYER_NAME_ALPHA)	
				#IF IS_DEBUG_BUILD
					IF g_TurnOnOverheadDebug
						PRINTLN("[Overheads] - call 3 - setting gamertag alpha to UNIVERSAL_PLAYER_NAME_ALPHA = ", UNIVERSAL_PLAYER_NAME_ALPHA)		
					ENDIF
				#ENDIF
				SET_MP_GAMER_TAG_ALPHA		(iPlayerNum, MP_TAG_GAMER_NAME_NEARBY, UNIVERSAL_PLAYER_NAME_ALPHA)	
				#IF IS_DEBUG_BUILD
					IF g_TurnOnOverheadDebug
						PRINTLN("[Overheads] - call 2 - setting gamertag alpha to UNIVERSAL_PLAYER_NAME_ALPHA = ", UNIVERSAL_PLAYER_NAME_ALPHA)
					ENDIF
				#ENDIF
			ENDIF
		endif
		#IF IS_DEBUG_BUILD
		IF g_TurnOnOverheadDebug
			PRINTLN("[overheads] - calling MAINTAIN_HEIST_OH_COLOUR at point 2")
		ENDIF
		#ENDIF
		MAINTAIN_HEIST_OH_COLOUR(playerId, iPlayerNum)
				
	ENDIF

	
	RETURN FALSE
ENDFUNC



PROC RESET_MOTOR_MC_ROLES(INT iPlayerNum)

		SET_ICON_VISABILITY(iPlayerNum,  MP_TAG_MC_ROLE_PRESIDENT, FALSE)
		SET_ICON_VISABILITY(iPlayerNum,  MP_TAG_GANG_CEO, FALSE)
		SET_ICON_VISABILITY(iPlayerNum,  MP_TAG_MC_ROLE_PROSPECT, FALSE)
		SET_ICON_VISABILITY(iPlayerNum,  MP_TAG_MC_ROLE_VICE_PRESIDENT, FALSE)
		SET_ICON_VISABILITY(iPlayerNum,  MP_TAG_MC_ROLE_ROAD_CAPTAIN, FALSE)
		SET_ICON_VISABILITY(iPlayerNum,  MP_TAG_MC_ROLE_SARGEANT, FALSE)
		SET_ICON_VISABILITY(iPlayerNum,  MP_TAG_MC_ROLE_ENFORCER, FALSE)
ENDPROC

///////////////////////////////////////////////////////////////////////////////
///    
///    OVERHEAD display common to FM, RC, DM & CNC
///    
PROC PLAYERS_OVERHEAD_COMMON_FUNCTIONS(PLAYER_INDEX playerID, INT iPlayernum)
	////////////////////////////////////////////////
	/// Set Up the gamer ID
	
	BOOL bCalledSetGamerTagNameThisFrame
	
	IF IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_CAN_DISPLAY, playerID) = FALSE

		#IF IS_DEBUG_BUILD
			IF g_TurnOnOverheadDebug
				PRINTLN("[overheads] - PLAYERS_OVERHEAD_COMMON_FUNCTIONS = FALSE  DISPLAYOVERHEADS_LOGIC_CAN_DISPLAY = FALSE for player = ", NATIVE_TO_INT(playerID), " - ", GET_PLAYER_NAME(playerID))
			ENDIF
		#ENDIF
		HIDE_ABOVE_HEAD_DISPLAY_ONCE(iPlayernum, playerID)
		EXIT
	ENDIF
	
		////////////////////////////////////////////////
		/// NAME
	//	IF MPGlobalsHud.DisplayInfo.bDisplayPlayerName
		IF IS_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_PLAYER_NAME, playerID )	
			
			#IF IS_DEBUG_BUILD
			IF g_TurnOnOverheadDebug
				PRINTLN("[overheads] - PLAYER NAME ACTUALLY DISPLAY = true ", NATIVE_TO_INT(playerID))
				IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(playerID)
					PRINTLN("[overheads] - Player GamerTag = ", GET_PLAYER_NAME(playerID))
				ENDIF
			ENDIF
			#ENDIF
			SET_ICON_VISABILITY(iPlayerNum, MP_TAG_GAMER_NAME, TRUE)
		ELSE
			
			#IF IS_DEBUG_BUILD
			IF g_TurnOnOverheadDebug
				PRINTLN("[overheads] - PLAYER NAME ACTUALLY DISPLAY = false ", NATIVE_TO_INT(playerID))
				IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(playerID)
					PRINTLN("[overheads] - Player GamerTag = ", GET_PLAYER_NAME(playerID))
				ENDIF
			ENDIF
			#ENDIF
			SET_ICON_VISABILITY(iPlayerNum, MP_TAG_GAMER_NAME, FALSE)
		ENDIF
		
		#IF FEATURE_FREEMODE_ARCADE
			#IF FEATURE_COPS_N_CROOKS	
				IF IS_FREEMODE_ARCADE() AND GET_ARCADE_MODE() = ARC_COPS_CROOKS
					SET_ICON_VISABILITY(iPlayerNum, MP_TAG_GAMER_NAME, !IS_PLAYER_LOCAL(playerID))	
				ENDIF		
			#ENDIF
		#ENDIF		
		
		////////////////////////////////////////////////
		/// CHAT WINDOW
	//	IF MPGlobalsHud.DisplayInfo.bDisplayPlayerName
		IF IS_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_CHAT_WINDOW, playerID)	
			
			#IF IS_DEBUG_BUILD
			IF g_TurnOnOverheadDebug
				PRINTLN("[overheads] - CHAT WINDOW ACTUALLY DISPLAY = true ", NATIVE_TO_INT(playerID))
				IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(playerID)
					PRINTLN("[overheads] - Player GamerTag = ", GET_PLAYER_NAME(playerID))
				ENDIF
			ENDIF
			#ENDIF
			
			SET_ICON_VISABILITY(iPlayerNum, MP_TAG_TYPING, TRUE)
			
		ELSE
			
			#IF IS_DEBUG_BUILD
			IF g_TurnOnOverheadDebug
				PRINTLN("[overheads] - CHAT WINDOW ACTUALLY DISPLAY = false ", NATIVE_TO_INT(playerID))
				IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(playerID)
					PRINTLN("[overheads] - Player GamerTag = ", GET_PLAYER_NAME(playerID))
				ENDIF
			ENDIF
			#ENDIF
			
			SET_ICON_VISABILITY(iPlayerNum, MP_TAG_TYPING, FALSE)
			
		ENDIF
		
		////////////////////////////////////////////////
		/// WANTED RATING
	//	IF MPGlobalsHud.DisplayInfo.bDisplayWantedLevel
		IF (IS_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_WANTEDLEVEL , playerID)
		OR IS_OVERHEAD_EVENT_ACTIVE(DISPLAYOVERHEADS_EVENTS_REFRESH_WANTED, PlayerID)) //Refresh is only used if another player changes levels.
		AND ((MPGlobalsHud.DisplayInfo.iOverhead_iPlayerWantedLevel_LastFrame[NATIVE_TO_INT(playerID)] > 0) OR (MPGlobalsHud.DisplayInfo.iOverhead_iPlayerFakeWantedLevel_LastFrame[NATIVE_TO_INT(playerID)] > 0))
		
			IF IS_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_WANTEDLEVEL , playerID)	
				SET_ICON_VISABILITY(iPlayerNum,  MP_TAG_WANTED_LEVEL, TRUE)
				INT iPlayerWantedLevel = GET_PLAYER_WANTED_LEVEL(PlayerID)
				IF GET_PLAYER_FAKE_WANTED_LEVEL(PlayerID) > 0
					iPlayerWantedLevel = GET_PLAYER_FAKE_WANTED_LEVEL(PlayerID)
				ENDIF
				IF g_sOverHead.iWantedLevel[iPlayernum] != iPlayerWantedLevel
					g_sOverHead.iWantedLevel[iPlayernum] = iPlayerWantedLevel
				ENDIF
				PRINTLN("[overheads] - g_sOverHead.iWantedLevel[", iPlayernum, "] = ", g_sOverHead.iWantedLevel[iPlayernum])
				SET_MP_GAMER_TAG_WANTED_LEVEL(iPlayerNum, g_sOverHead.iWantedLevel[iPlayernum])
				SET_OVERHEAD_EVENT_ACTIVE(DISPLAYOVERHEADS_EVENTS_REFRESH_WANTED,FALSE, PlayerID)
			ELSE
				g_sOverHead.iWantedLevel[iPlayernum] = -1
				SET_ICON_VISABILITY(iPlayerNum, MP_TAG_WANTED_LEVEL, FALSE)
				SET_OVERHEAD_EVENT_ACTIVE(DISPLAYOVERHEADS_EVENTS_REFRESH_WANTED,FALSE, PlayerID)
			ENDIF
		ELSE
			g_sOverHead.iWantedLevel[iPlayernum] = -1
			SET_ICON_VISABILITY(iPlayerNum, MP_TAG_WANTED_LEVEL, FALSE)
			SET_OVERHEAD_EVENT_ACTIVE(DISPLAYOVERHEADS_EVENTS_REFRESH_WANTED,FALSE, PlayerID)
		ENDIF	
		
		//PACKAGE TRANSMITTOR
		IF (IS_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_PACKAGE_TRANSMITTOR , playerID)
		OR IS_OVERHEAD_EVENT_ACTIVE(DISPLAYOVERHEADS_EVENTS_CARRYING_TRANSMITTOR_PACKAGE, PlayerID)) //Refresh is only used if another player changes levels.
		OR g_iDawnRaidPickupCarrierPart != -1
		
		
		//OR (MPGlobalsHud.DisplayInfo.iOverhead_iPlayerLargepackages_LastFrame[iPlayerNum] !=NULL AND IS_THIS_ROCKSTAR_MISSION_NEW_VS_DAWN_RAID(g_FMMC_STRUCT.iAdversaryModeType))	
			IF IS_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_PACKAGE_TRANSMITTOR , playerID)	
				SET_ICON_VISABILITY(iPlayerNum,  MP_TAG_TRANSMITTER, TRUE)
				SET_OVERHEAD_EVENT_ACTIVE(DISPLAYOVERHEADS_EVENTS_CARRYING_TRANSMITTOR_PACKAGE,FALSE, PlayerID)
				PRINTLN(" RUN_OHD_PACKAGE_TRANSMITTORS ON")
		
			ELSE
				SET_ICON_VISABILITY(iPlayerNum, MP_TAG_TRANSMITTER, FALSE)
				PRINTLN(" RUN_OHD_PACKAGE_TRANSMITTORS OFF")
				SET_OVERHEAD_EVENT_ACTIVE(DISPLAYOVERHEADS_EVENTS_CARRYING_TRANSMITTOR_PACKAGE,FALSE, PlayerID)
			ENDIF
		ELSE

			SET_ICON_VISABILITY(iPlayerNum, MP_TAG_TRANSMITTER, FALSE)
			PRINTLN(" RUN_OHD_PACKAGE_TRANSMITTORS OFF 2")
			SET_OVERHEAD_EVENT_ACTIVE(DISPLAYOVERHEADS_EVENTS_CARRYING_TRANSMITTOR_PACKAGE,FALSE, PlayerID)
		ENDIF	
		
	////////////////////////////////////////////////
		/// GANG OVER HEAD ICONS
	//	IF MPGlobalsHud.DisplayInfo.bDisplayWantedLevel
		IF (IS_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_MC_ROLE , playerID)
		OR IS_OVERHEAD_EVENT_ACTIVE(DISPLAYOVERHEADS_EVENTS_CHANGED_GANG_ROLE, PlayerID)) //Refresh is only used if another player changes levels.
	//	AND ((MPGlobalsHud.DisplayInfo.iOverhead_iPlayerWantedLevel_LastFrame[NATIVE_TO_INT(playerID)] > 0) OR (MPGlobalsHud.DisplayInfo.iOverhead_iPlayerFakeWantedLevel_LastFrame[NATIVE_TO_INT(playerID)] > 0))
		
			IF IS_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_MC_ROLE , playerID)
				IF GB_IS_PLAYER_BOSS_OF_A_GANG(playerID)
					IF GB_IS_PLAYER_BOSS_OF_A_BIKER_GANG(playerID)
						RESET_MOTOR_MC_ROLES(iPlayerNum)
						SET_ICON_VISABILITY(iPlayerNum,  MP_TAG_MC_ROLE_PRESIDENT, TRUE)
					ELSE
						RESET_MOTOR_MC_ROLES(iPlayerNum)
						SET_ICON_VISABILITY(iPlayerNum,  MP_TAG_GANG_CEO, TRUE)
					ENDIF
					
				ELSE
					IF GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(playerID)
						IF GB_GET_PLAYER_GANG_ROLE(playerID) = GR_NONE
							RESET_MOTOR_MC_ROLES(iPlayerNum)
							SET_ICON_VISABILITY(iPlayerNum,  MP_TAG_MC_ROLE_PROSPECT, TRUE)
						ELIF GB_GET_PLAYER_GANG_ROLE(playerID) = GR_VP
							RESET_MOTOR_MC_ROLES(iPlayerNum)
							SET_ICON_VISABILITY(iPlayerNum,  MP_TAG_MC_ROLE_VICE_PRESIDENT, TRUE)
						ELIF GB_GET_PLAYER_GANG_ROLE(playerID) = GR_ROAD_CAPTAIN
							RESET_MOTOR_MC_ROLES(iPlayerNum)
							SET_ICON_VISABILITY(iPlayerNum,  MP_TAG_MC_ROLE_ROAD_CAPTAIN, TRUE)
						ELIF GB_GET_PLAYER_GANG_ROLE(playerID) = GR_SERGEANT_AT_ARMS
							RESET_MOTOR_MC_ROLES(iPlayerNum)
							SET_ICON_VISABILITY(iPlayerNum,  MP_TAG_MC_ROLE_SARGEANT, TRUE)
						//ELIF GB_GET_PLAYER_GANG_ROLE(playerID) = GR_ENFORCER
						ELSE
							RESET_MOTOR_MC_ROLES(iPlayerNum)
							SET_ICON_VISABILITY(iPlayerNum,  MP_TAG_MC_ROLE_ENFORCER, TRUE)
						ENDIF
					ENDIF
				ENDIF
			
			
				SET_OVERHEAD_EVENT_ACTIVE(DISPLAYOVERHEADS_EVENTS_CHANGED_GANG_ROLE,FALSE, PlayerID)
			ELSE
				
				RESET_MOTOR_MC_ROLES(iPlayerNum)
				SET_OVERHEAD_EVENT_ACTIVE(DISPLAYOVERHEADS_EVENTS_CHANGED_GANG_ROLE,FALSE, PlayerID)
			ENDIF
		ELSE
			
			RESET_MOTOR_MC_ROLES(iPlayerNum)
			SET_OVERHEAD_EVENT_ACTIVE(DISPLAYOVERHEADS_EVENTS_CHANGED_GANG_ROLE,FALSE, PlayerID)
		ENDIF	
				
		
		
		

		
		////////////////////////////////////////////////
		/// SPEECH ICON
		IF IS_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_AUDIO , playerID)
			SET_ICON_VISABILITY(iPlayerNum,  MP_TAG_AUDIO, TRUE)
		ELSE
			SET_ICON_VISABILITY(iPlayerNum, MP_TAG_AUDIO, FALSE)
		ENDIF
		
		////////////////////////////////////////////////
		/// HEALTH BAR
		IF IS_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_HEALTHBAR , playerID)
			#IF IS_DEBUG_BUILD
			IF g_TurnOnOverheadDebug
				PRINTLN("[overheads] - PLAYER HEALTHBAR ACTUALLY DISPLAY = true ", NATIVE_TO_INT(playerID))
				IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(playerID)
					PRINTLN("[overheads] - Player GamerTag = ", GET_PLAYER_NAME(playerID))
				ENDIF
			ENDIF
			#ENDIF
			SET_ICON_VISABILITY(iPlayerNum,  MP_TAG_HEALTH_BAR, TRUE)
		ELSE
			#IF IS_DEBUG_BUILD
			IF g_TurnOnOverheadDebug
				PRINTLN("[overheads] - PLAYER HEALTHBAR ACTUALLY DISPLAY = false ", NATIVE_TO_INT(playerID))
				IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(playerID)
					PRINTLN("[overheads] - Player GamerTag = ", GET_PLAYER_NAME(playerID))
				ENDIF
			ENDIF
			#ENDIF
			SET_ICON_VISABILITY(iPlayerNum, MP_TAG_HEALTH_BAR, FALSE)
		ENDIF
		
		////////////////////////////////////////////////
		/// TRACKIFY
		IF IS_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_INVENTORY, playerID)
			IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(playerID)].iMyOverHeadInventory, ENUM_TO_INT(MP_TAG_TRACKIFY))
				#IF IS_DEBUG_BUILD
				IF g_TurnOnOverheadDebug
					PRINTLN("[overheads] - PLAYER MP_TAG_TRACKIFY ACTUALLY DISPLAY = TRUE ", NATIVE_TO_INT(playerID))
					IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(playerID)
						PRINTLN("[overheads] - Player GamerTag = ", GET_PLAYER_NAME(playerID))
					ENDIF
				ENDIF
				#ENDIF

				//Only display Trackify overhead icon if the teams like one another
				IF DOES_TEAM_LIKE_TEAM(GET_PLAYER_TEAM(playerID),GET_PLAYER_TEAM(PLAYER_ID()))
					SET_ICON_VISABILITY(iPlayerNum, MP_TAG_TRACKIFY, TRUE)
					PRINTLN("[overheads] - SET_ICON_VISABILITY is set to true. SAME TEAM")
				ELSE
					SET_ICON_VISABILITY(iPlayerNum, MP_TAG_TRACKIFY, FALSE)
					PRINTLN("[overheads] - SET_ICON_VISABILITY is set to false. ENEMY TEAM")
				ENDIF
			ELSE
				SET_ICON_VISABILITY(iPlayerNum, MP_TAG_TRACKIFY, FALSE)
			ENDIF
		ELSE
			SET_ICON_VISABILITY(iPlayerNum, MP_TAG_TRACKIFY, FALSE)
		ENDIF
		
		
		////////////////////////////////////////////////
		/// PACKAGES
		IF IS_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_INVENTORY, playerID)
			
			////////////////////////////////////////////////
			/// A Package
			IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(playerID)].iMyOverHeadInventory, ENUM_TO_INT(MP_TAG_PACKAGES))
				#IF IS_DEBUG_BUILD
				IF g_TurnOnOverheadDebug
					PRINTLN("[overheads] - PLAYER PACKAGES ACTUALLY DISPLAY = TRUE ", NATIVE_TO_INT(playerID))
					IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(playerID)
						PRINTLN("[overheads] - Player GamerTag = ", GET_PLAYER_NAME(playerID))
					ENDIF
				ENDIF
				#ENDIF
				SET_ICON_VISABILITY(iPlayerNum, MP_TAG_PACKAGES, TRUE)
			/*ELIF  IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(playerID)].iMyOverHeadInventory, ENUM_TO_INT(MP_TAG_BALL))
				#IF IS_DEBUG_BUILD
				IF g_TurnOnOverheadDebug
					PRINTLN("[overheads] - PLAYER PACKAGES ACTUALLY DISPLAY = TRUE ", NATIVE_TO_INT(playerID))
					IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(playerID)
						PRINTLN("[overheads] - Player GamerTag = ", GET_PLAYER_NAME(playerID))
					ENDIF
				ENDIF
				#ENDIF
				SET_ICON_VISABILITY(iPlayerNum, MP_TAG_BALL, TRUE)*/
			ELIF  IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(playerID)].iMyOverHeadInventory, ENUM_TO_INT(MP_TAG_PACKAGE_LARGE))
			or (g_FMMC_STRUCT.iPTLOwnedPickUps[NATIVE_TO_INT(PlayerId)] >= 0 AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciPOINTLESS_MODE_ENABLED))
			OR  (IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_LOCAL_DPADDOWNACTIVE, PLAYER_ID()) AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciPOINTLESS_MODE_ENABLED) )
			OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciPOINTLESS_ENABLE_OVERHEAD_DISPLAY)
				#IF IS_DEBUG_BUILD
				IF g_TurnOnOverheadDebug
					PRINTLN("[overheads] - PLAYER MP_TAG_PACKAGE_LARGE ACTUALLY DISPLAY = TRUE ", NATIVE_TO_INT(playerID))
					IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(playerID)
						PRINTLN("[overheads] - Player GamerTag = ", GET_PLAYER_NAME(playerID))
					ENDIF
				ENDIF
				#ENDIF
				IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(playerID)
				AND network_is_activity_session()
				AND NOT IS_PLAYER_USING_ARENA()
					SET_ICON_VISABILITY(iPlayerNum, MP_TAG_PACKAGE_LARGE, TRUE)
					if IS_MP_GAMER_TAG_ACTIVE(NATIVE_TO_INT(PlayerId))
						SET_MP_GAMER_TAG_NUM_PACKAGES(NATIVE_TO_INT(PlayerId), g_FMMC_STRUCT.iPTLOwnedPickUps[NATIVE_TO_INT(PlayerId)])
					endif
				ENDIF
				
		
				
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwenty, ciOptionsBS20_Use_Biker_Arrow_For_Player_Carry_Overhead)
				OR (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne, ciOptionsBS21_DisplayBombCarrier) AND CAN_I_SEE_PLAYER(playerID, -1))
					PRINTLN("[overheads] - ciOptionsBS20_Use_Biker_Arrow_For_Player_Carry_Overhead is set. Overriding the overhead to Biker Arrow/Bomb.")
					SET_ICON_VISABILITY(iPlayerNum, MP_TAG_PACKAGE_LARGE, FALSE)
					
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne, ciOptionsBS21_DisplayBombCarrier)
						SET_ICON_VISABILITY(iPlayerNum, MP_TAG_BOMB, TRUE)
					ELSE
						SET_ICON_VISABILITY(iPlayerNum, MP_TAG_BIKER_ARROW, TRUE)
					ENDIF
				ENDIF
				
			//	ENDIF
			/*	#IF IS_DEBUG_BUILD
					START_WIDGET_GROUP("LARGE PACKAGE REQ") 
						//ADD_WIDGET_INT_SLIDER("ROUND(g_sMPTunables.igb_finders_keepers_rumble_radius)", g_sMPTunables.igb_finders_keepers_rumble_radius,0.0, 200.0,1.0)
						ADD_WIDGET_FLOAT_SLIDER("g_sOverHead.ival", g_sOverHead.fval,-1000.0, 100000.0,1.0)
						ADD_WIDGET_FLOAT_SLIDER("g_sOverHead.ival2", g_sOverHead.fval2,-1000.0, 100000.0,1.0)
						ADD_WIDGET_FLOAT_SLIDER("g_sOverHead.ival3", g_sOverHead.fval3,-1000.0, 100000.0,1.0)
						
						
					STOP_WIDGET_GROUP()	
				#ENDIF*/
			ELIF  IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(playerID)].iMyOverHeadInventory, ENUM_TO_INT(MP_TAG_GANG_CEO))
				SET_ICON_VISABILITY(iPlayerNum, MP_TAG_GANG_CEO, TRUE)
			ELIF  IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(playerID)].iMyOverHeadInventory, ENUM_TO_INT(MP_TAG_GANG_BIKER))
				SET_ICON_VISABILITY(iPlayerNum, MP_TAG_GANG_BIKER, TRUE)
			ELSE
				#IF IS_DEBUG_BUILD
				IF g_TurnOnOverheadDebug
					PRINTLN("[overheads] - PLAYER PACKAGES MP_TAG_PACKAGES BITSET is = FALSE ", NATIVE_TO_INT(playerID))
					IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(playerID)
						PRINTLN("[overheads] - Player GamerTag = ", GET_PLAYER_NAME(playerID))
					ENDIF
				ENDIF
				#ENDIF
				SET_ICON_VISABILITY(iPlayerNum, MP_TAG_PACKAGES, FALSE)
				SET_ICON_VISABILITY(iPlayerNum, MP_TAG_PACKAGE_LARGE, FALSE)
				SET_ICON_VISABILITY(iPlayerNum, MP_TAG_GANG_BIKER, FALSE)
				SET_ICON_VISABILITY(iPlayerNum, MP_TAG_GANG_CEO, FALSE)
				SET_ICON_VISABILITY(iPlayerNum, MP_TAG_BIKER_ARROW, FALSE)
				SET_ICON_VISABILITY(iPlayerNum, MP_TAG_BOMB, FALSE)
			ENDIF
	
		ELSE
					
			#IF IS_DEBUG_BUILD
			IF g_TurnOnOverheadDebug
				PRINTLN("[overheads] - PLAYER PACKAGES DISPLAY_INVENTORY = FALSE ", NATIVE_TO_INT(playerID))
				IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(playerID)
					PRINTLN("[overheads] - Player GamerTag = ", GET_PLAYER_NAME(playerID))
				ENDIF
			ENDIF
			#ENDIF				
							
			SET_ICON_VISABILITY(iPlayerNum, MP_TAG_PACKAGES, FALSE)
			SET_ICON_VISABILITY(iPlayerNum, MP_TAG_PACKAGE_LARGE , FALSE)
			SET_ICON_VISABILITY(iPlayerNum, MP_TAG_GANG_BIKER, FALSE)
			SET_ICON_VISABILITY(iPlayerNum, MP_TAG_GANG_CEO, FALSE)
			SET_ICON_VISABILITY(iPlayerNum, MP_TAG_BIKER_ARROW, FALSE)
			SET_ICON_VISABILITY(iPlayerNum, MP_TAG_BOMB, FALSE)
		ENDIF
		
		
		////////////////////////////////////////////////
		/// DRAW THE IF THE PLAYER HAS VOTED
//		IF IS_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_VOTED_TICK , playerID)
//			#IF IS_DEBUG_BUILD
//			IF g_TurnOnOverheadDebug
//				NET_NL()NET_PRINT("[overhead] PLAYER VOTING YES ACTUALLY DISPLAY = true ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
//				IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(playerID)
//					NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))
//				ENDIF
//			ENDIF
//			#ENDIF
//			SET_ICON_VISABILITY(iPlayerNum,  MP_TAG_VOTED, TRUE)
//		ELSE
//			SET_ICON_VISABILITY(iPlayerNum,  MP_TAG_VOTED, FALSE)
//			#IF IS_DEBUG_BUILD
//			IF g_TurnOnOverheadDebug
//				NET_NL()NET_PRINT("[overhead] PLAYER VOTING YES ACTUALLY DISPLAY = false ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
//				IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(playerID)
//					NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))
//				ENDIF
//			ENDIF
//			#ENDIF
//		ENDIF	
		
		////////////////////////////////////////////////
		/// DRAW THE IF THE PLAYER IS NOT ABLE TO VOTE
//		IF IS_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_VOTED_CROSS, playerID )
//			#IF IS_DEBUG_BUILD
//			IF g_TurnOnOverheadDebug
//				NET_NL()NET_PRINT("[overhead] PLAYER VOTING NO ACTUALLY DISPLAY = true ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
//				IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(playerID)
//					NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))
//				ENDIF
//			ENDIF
//			#ENDIF
//			SET_ICON_VISABILITY(iPlayerNum,  MP_TAG_NO_VOTE, TRUE)
//		ELSE
//			#IF IS_DEBUG_BUILD
//			IF g_TurnOnOverheadDebug
//				NET_NL()NET_PRINT("[overhead] PLAYER VOTING NO ACTUALLY DISPLAY = FALSE ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
//				IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(playerID)
//					NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))
//				ENDIF
//			ENDIF
//			#ENDIF
//			SET_ICON_VISABILITY(iPlayerNum,  MP_TAG_NO_VOTE, FALSE)
//		ENDIF	

		////////////////////////////////////////////////
		/// CLAN TAG
		IF IS_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_CLAN, playerID )
			#IF IS_DEBUG_BUILD
			IF g_TurnOnOverheadDebug
				PRINTLN("[overheads] - PLAYER CREW DISPLAY = TRUE ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
				IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(playerID)
					PRINTLN("[overheads] - Player GamerTag = ", GET_PLAYER_NAME(playerID))
				ENDIF
				
			ENDIF
			#ENDIF
			SET_ICON_VISABILITY(iPlayerNum,  MP_TAG_CREW_TAG, TRUE)
		ELSE
			#IF IS_DEBUG_BUILD
			IF g_TurnOnOverheadDebug
				PRINTLN("[overheads] - PLAYER CREW DISPLAY = FALSE ", NATIVE_TO_INT(playerID))
				IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(playerID)
					PRINTLN("[overheads] - Player GamerTag = ", GET_PLAYER_NAME(playerID))
				ENDIF
			ENDIF
			#ENDIF
			SET_ICON_VISABILITY(iPlayerNum,  MP_TAG_CREW_TAG, FALSE)
		ENDIF
		
		//		////////////////////////////////////////////////
//		/// RESPAWNING
		IF IS_OVERHEAD_EVENT_ACTIVE(DISPLAYOVERHEADS_EVENTS_REFRESH_RESPAWNING, PlayerID)
			IF IS_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_RESPAWNING, playerID )
				#IF IS_DEBUG_BUILD
				IF g_TurnOnOverheadDebug
					PRINTLN("[overheads] - DISPLAYOVERHEADS_DISPLAY_RESPAWNING = TRUE ", NATIVE_TO_INT(playerID))
					IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(playerID)
						PRINTLN("[overheads] - Player GamerTag = ", GET_PLAYER_NAME(playerID))
					ENDIF
					
				ENDIF
				#ENDIF
				IF NOT bCalledSetGamerTagNameThisFrame
					SET_MP_GAMER_TAG_NAME(iPlayernum, GET_FILENAME_FOR_AUDIO_CONVERSATION("OHD_SPAWNING"))
					bCalledSetGamerTagNameThisFrame = TRUE
				ENDIF
				SET_OVERHEAD_EVENT_ACTIVE(DISPLAYOVERHEADS_EVENTS_REFRESH_RESPAWNING,FALSE, PlayerID)
	//			SET_ICON_VISABILITY(iPlayerNum,  MP_TAG_USING_MENU, TRUE)
			ELSE
				#IF IS_DEBUG_BUILD
				IF g_TurnOnOverheadDebug
					PRINTLN("[overheads] - DISPLAYOVERHEADS_DISPLAY_RESPAWNING = FALSE ", NATIVE_TO_INT(playerID))
					IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(playerID)
						PRINTLN("[overheads] - Player GamerTag = ", GET_PLAYER_NAME(playerID))
					ENDIF
				ENDIF
				#ENDIF
				IF NOT bCalledSetGamerTagNameThisFrame
					SET_MP_GAMER_TAG_NAME(iPlayernum, GET_PLAYER_NAME(playerID))
					bCalledSetGamerTagNameThisFrame = TRUE
		//			SET_ICON_VISABILITY(iPlayerNum,  MP_TAG_USING_MENU, FALSE)
				ENDIF
				SET_OVERHEAD_EVENT_ACTIVE(DISPLAYOVERHEADS_EVENTS_REFRESH_RESPAWNING,FALSE, PlayerID)
			ENDIF
		ENDIF
		
//		////////////////////////////////////////////////
//		/// PAUSED
		IF IS_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_PAUSED, playerID )
			#IF IS_DEBUG_BUILD
			IF g_TurnOnOverheadDebug
				PRINTLN("[overheads] - PLAYER PAUSED DISPLAY = TRUE ", NATIVE_TO_INT(playerID))
				IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(playerID)
					PRINTLN("[overheads] - Player GamerTag = ", GET_PLAYER_NAME(playerID))
				ENDIF
				
			ENDIF
			#ENDIF
			IF NOT bCalledSetGamerTagNameThisFrame
			AND GET_CURRENT_LANGUAGE() != LANGUAGE_CHINESE_SIMPLIFIED 
				SET_MP_GAMER_TAG_NAME(iPlayernum, GET_FILENAME_FOR_AUDIO_CONVERSATION("PAUSED"))
				bCalledSetGamerTagNameThisFrame = TRUE
			ENDIF
//			SET_ICON_VISABILITY(iPlayerNum,  MP_TAG_USING_MENU, TRUE)
		ELSE
			#IF IS_DEBUG_BUILD
			IF g_TurnOnOverheadDebug
				PRINTLN("[overheads] - PLAYER PAUSED DISPLAY = FALSE ", NATIVE_TO_INT(playerID))
				IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(playerID)
					PRINTLN("[overheads] - Player GamerTag = ", GET_PLAYER_NAME(playerID))
				ENDIF
			ENDIF
			#ENDIF
			IF NOT bCalledSetGamerTagNameThisFrame
				SET_MP_GAMER_TAG_NAME(iPlayernum, GET_PLAYER_NAME(playerID))
				bCalledSetGamerTagNameThisFrame = TRUE
			ENDIF
//			SET_ICON_VISABILITY(iPlayerNum,  MP_TAG_USING_MENU, FALSE)
		ENDIF
		
		

		
		
		
//		////////////////////////////////////////////////
//		/// PASSIVE
		IF IS_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_PASSIVE, playerID )
			#IF IS_DEBUG_BUILD
			IF g_TurnOnOverheadDebug
				PRINTLN("[overheads] - PLAYER PASSIVE DISPLAY = TRUE ", NATIVE_TO_INT(playerID))
				IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(playerID)
					PRINTLN("[overheads] - Player GamerTag = ", GET_PLAYER_NAME(playerID))
				ENDIF
				
			ENDIF
			#ENDIF
			SET_ICON_VISABILITY(iPlayerNum,  MP_TAG_PASSIVE_MODE , TRUE)
		ELSE
			#IF IS_DEBUG_BUILD
			IF g_TurnOnOverheadDebug
				PRINTLN("[overheads] - PLAYER PASSIVE DISPLAY = FALSE ", NATIVE_TO_INT(playerID))
				IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(playerID)
					PRINTLN("[overheads] - Player GamerTag = ", GET_PLAYER_NAME(playerID))
				ENDIF
			ENDIF
			#ENDIF
			SET_ICON_VISABILITY(iPlayerNum,  MP_TAG_PASSIVE_MODE , FALSE)
		ENDIF
		
		
//		////////////////////////////////////////////////
//		/// TAGGED
		IF IS_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_TAGGED, playerID )
			#IF IS_DEBUG_BUILD
			IF g_TurnOnOverheadDebug
				PRINTLN("[overheads] - PLAYER TAGGED DISPLAY = TRUE ", NATIVE_TO_INT(playerID))
				IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(playerID)
					PRINTLN("[overheads] - Player GamerTag = ", GET_PLAYER_NAME(playerID))
				ENDIF
				
			ENDIF
			#ENDIF
			SET_ICON_VISABILITY(iPlayerNum,  MP_TAG_TAGGED , TRUE)
		ELSE
			#IF IS_DEBUG_BUILD
			IF g_TurnOnOverheadDebug
				PRINTLN("[overheads] - PLAYER TAGGED DISPLAY = FALSE ", NATIVE_TO_INT(playerID))
				IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(playerID)
					PRINTLN("[overheads] - Player GamerTag = ", GET_PLAYER_NAME(playerID))
				ENDIF
			ENDIF
			#ENDIF
			SET_ICON_VISABILITY(iPlayerNum,  MP_TAG_TAGGED , FALSE)
		ENDIF
		
		
		
//		////////////////////////////////////////////////
//		/// IS RALLY DRIVER
		IF IS_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_RALLY_DRIVER, playerID )
			#IF IS_DEBUG_BUILD
			IF g_TurnOnOverheadDebug
				PRINTLN("[overheads] - PLAYER RALLY DRIVER DISPLAY = TRUE ", NATIVE_TO_INT(playerID))
				IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(playerID)
					PRINTLN("[overheads] - Player GamerTag = ", GET_PLAYER_NAME(playerID))
				ENDIF
				
			ENDIF
			#ENDIF
			SET_ICON_VISABILITY(iPlayerNum,  MP_TAG_DRIVER , TRUE)
		ELSE
			#IF IS_DEBUG_BUILD
			IF g_TurnOnOverheadDebug
				PRINTLN("[overheads] - PLAYER RALLY DRIVER DISPLAY = FALSE ", NATIVE_TO_INT(playerID))
				IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(playerID)
					PRINTLN("[overheads] - Player GamerTag = ", GET_PLAYER_NAME(playerID))
				ENDIF
			ENDIF
			#ENDIF
			SET_ICON_VISABILITY(iPlayerNum,  MP_TAG_DRIVER , FALSE)
		ENDIF
		
		
//		////////////////////////////////////////////////
//		/// IS RALLY PASSENGER
		IF IS_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_RALLY_PASSENGER, playerID )
			#IF IS_DEBUG_BUILD
			IF g_TurnOnOverheadDebug
				PRINTLN("[overheads] - PLAYER RALLY PASSENGER DISPLAY = TRUE ", NATIVE_TO_INT(playerID))
				IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(playerID)
					PRINTLN("[overheads] - Player GamerTag = ", GET_PLAYER_NAME(playerID))
				ENDIF
				
			ENDIF
			#ENDIF
			SET_ICON_VISABILITY(iPlayerNum,  MP_TAG_CO_DRIVER , TRUE)
		ELSE
			#IF IS_DEBUG_BUILD
			IF g_TurnOnOverheadDebug
				PRINTLN("[overheads] - PLAYER RALLY PASSENGER DISPLAY = FALSE ", NATIVE_TO_INT(playerID))
				IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(playerID)
					PRINTLN("[overheads] - Player GamerTag = ", GET_PLAYER_NAME(playerID))
				ENDIF
			ENDIF
			#ENDIF
			SET_ICON_VISABILITY(iPlayerNum,  MP_TAG_CO_DRIVER , FALSE)
		ENDIF
	
	
	
//		////////////////////////////////////////////////
//		/// IS PLAYER BETTING
		IF IS_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_BETTING_ODDS, playerID )
			#IF IS_DEBUG_BUILD
			IF g_TurnOnOverheadDebug
				PRINTLN("[overheads] - PLAYER BETTING DISPLAY = TRUE ", NATIVE_TO_INT(playerID))
				IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(playerID)
					PRINTLN("[overheads] - Player GamerTag = ", GET_PLAYER_NAME(playerID))
				ENDIF
			ENDIF
			#ENDIF
			SET_PLAYER_BETTING_STRING(iPlayerNum)
			SET_ICON_VISABILITY(iPlayerNum,  MP_TAG_BIG_TEXT , TRUE)
		ELSE
			#IF IS_DEBUG_BUILD
			IF g_TurnOnOverheadDebug
				PRINTLN("[overheads] - PLAYER BETTING DISPLAY = FALSE ", NATIVE_TO_INT(playerID))
				IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(playerID)
					PRINTLN("[overheads] - Player GamerTag = ", GET_PLAYER_NAME(playerID))
				ENDIF
			ENDIF
			#ENDIF
			SET_ICON_VISABILITY(iPlayerNum,  MP_TAG_BIG_TEXT , FALSE)
		ENDIF	
		
		
		
//		
////		////////////////////////////////////////////////
////		/// POWERPLAY
//		IF IS_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_POWERPLAY, playerID )
//			#IF IS_DEBUG_BUILD
//			IF g_TurnOnOverheadDebug
//				NET_NL()NET_PRINT("[overhead] PLAYER POWER PLAY DISPLAY = TRUE ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
//				IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(playerID)
//					NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))
//				ENDIF
//				
//			ENDIF
//			#ENDIF
//			
//			SET_ICON_VISABILITY(iPlayerNum,  MP_TAG_POWERPLAY , TRUE)
//		ELSE
//			#IF IS_DEBUG_BUILD
//			IF g_TurnOnOverheadDebug
//				NET_NL()NET_PRINT("[overhead] PLAYER POWER PLAY DISPLAY = FALSE ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
//				IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(playerID)
//					NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))
//				ENDIF
//			ENDIF
//			#ENDIF
//			SET_ICON_VISABILITY(iPlayerNum,  MP_TAG_POWERPLAY , FALSE)
//		ENDIF	
//		
				
//		////////////////////////////////////////////////
//		/// CUSTOM TEXT
		IF IS_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_CUSTOM_STRING, playerID )
		 
			#IF IS_DEBUG_BUILD
			IF g_TurnOnOverheadDebug
				PRINTLN("[overheads] - PLAYER BIG_TEXT DISPLAY = TRUE ", NATIVE_TO_INT(playerID))
				IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(playerID)
					PRINTLN("[overheads] - Player GamerTag = ", GET_PLAYER_NAME(playerID))
				ENDIF
				
			ENDIF
			#ENDIF
			
			SET_PLAYER_CUSTOM_STRING(iPlayerNum)
			SET_ICON_VISABILITY(iPlayerNum,  MP_TAG_BIG_TEXT , TRUE)
		ELSE
			IF IS_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_BETTING_ODDS, playerID ) = FALSE
				#IF IS_DEBUG_BUILD
				IF g_TurnOnOverheadDebug
					PRINTLN("[overheads] - PLAYER BIG_TEXT DISPLAY = FALSE ", NATIVE_TO_INT(playerID))
					IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(playerID)
						PRINTLN("[overheads] - Player GamerTag = ", GET_PLAYER_NAME(playerID))
					ENDIF
				ENDIF
				#ENDIF
				
				SET_ICON_VISABILITY(iPlayerNum,  MP_TAG_BIG_TEXT , FALSE)
			ENDIF
		ENDIF
	#IF FEATURE_FREEMODE_ARCADE
		#IF FEATURE_COPS_N_CROOKS
			IF IS_FREEMODE_ARCADE() AND GET_ARCADE_MODE() = ARC_COPS_CROOKS
				IF DOES_PLAYER_HAVE_OVERHEAD(playerID)
					SET_ICON_VISABILITY(iPlayerNum,  MP_TAG_BIG_TEXT , TRUE)
				ENDIF
			ENDIF
		#ENDIF
	#ENDIF
		
//		////////////////////////////////////////////////
//		/// ARROW
		IF IS_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_ARROW, playerID )
			#IF IS_DEBUG_BUILD
			IF g_TurnOnOverheadDebug
				PRINTLN("[overheads] - DISPLAYOVERHEADS_DISPLAY_ARROW = TRUE ", NATIVE_TO_INT(playerID))
				IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(playerID)
					PRINTLN("[overheads] - Player GamerTag = ", GET_PLAYER_NAME(playerID))
				ENDIF
				
			ENDIF
			#ENDIF
			IF GlobalplayerBD[NATIVE_TO_INT(playerID)].bdisplayarrowoverplayersheadteamonly = TRUE
				IF DOES_TEAM_LIKE_TEAM(GET_PLAYER_TEAM(playerID),GET_PLAYER_TEAM(PLAYER_ID()))
					SET_ICON_VISABILITY(iPlayerNum,  MP_TAG_ARROW, TRUE)
				ELSE
					SET_ICON_VISABILITY(iPlayerNum,  MP_TAG_ARROW, FALSE)
				ENDIF
			ELSE
				SET_ICON_VISABILITY(iPlayerNum,  MP_TAG_ARROW, TRUE)
			ENDIF
		ELSE
			SET_ICON_VISABILITY(iPlayerNum,  MP_TAG_ARROW , FALSE)
		ENDIF
		
//		// No arrows if the arrows are turned off.
//		IF (GET_PACKED_STAT_INT(PACKED_MP_PLAYER_MARKER_DISPLAY) != MARKER_OPTIONS_ARROW_ONLY)	
//			SET_ICON_VISABILITY(iPlayerNum, MP_TAG_ARROW, FALSE)
//		ENDIF
		
ENDPROC

FUNC BOOL SHOULD_READ_TAGS_BOOL()

	IF IS_ON_DEATHMATCH_GLOBAL_SET()
		RETURN TRUE
		
	ENDIF	
	
	IF IS_ON_RACE_GLOBAL_SET()
		RETURN TRUE
	ENDIF
	

RETURN FALSE

ENDFUNC

FUNC FLOAT GET_OVERHEAD_ARROW_MARKER_DISTANCE()
	
	IF IS_ON_VEH_DEATHMATCH_GLOBAL_SET()
		RETURN VEH_DM_ARROW_DISTANCE
	ENDIF
	
	IF IS_BIT_SET(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iFmAmbientEventBitSet, ciSET_PLAYER_IS_PENNED_IN_SPECTATOR) 
		RETURN PENNED_IN_SPECTATOR_ARROW_DISTANCE
	ENDIF
	
	IF g_iDawnRaidPickupCarrierPart!= -1 AND IS_THIS_ROCKSTAR_MISSION_NEW_VS_DAWN_RAID(g_FMMC_STRUCT.iAdversaryModeType)
		RETURN PENNED_IN_SPECTATOR_ARROW_DISTANCE
	ENDIF
	
	IF IS_BIT_SET(MPGlobalsAmbience.iAmbBitSet, iABI_HEIST_SPECTATE_WHILE_DEAD_ENABLED)
    AND IS_THIS_ROCKSTAR_MISSION_NEW_VS_RESURRECTION(g_FMMC_STRUCT.iAdversaryModeType)
	AND IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iSpecInfoBitset, SPEC_INFO_BS_USING_HEIST_SPECTATE) 
		PRINTLN("GET_OVERHEAD_ARROW_MARKER_DISTANCE PENNED_IN_SPECTATOR_ARROW_DISTANCE = 400")
		RETURN PENNED_IN_SPECTATOR_ARROW_DISTANCE
	ENDIF
	
	RETURN ARROW_DISTANCE
	
ENDFUNC

//PROC RUN_ARROW_MARKER_CHECK(BOOL bProcessingLocalPlayer, BOOL bPassedPlayerVisibleToLocalPlayer, PLAYER_INDEX playerId, FLOAT fDisFromPlayer)
//	
//	VECTOR vCoords
//	INT TempColR, TempColG, TempColB, TempColA
//	BOOL bOptionOkForArrow
//	
//	IF (GET_PACKED_STAT_INT(PACKED_MP_PLAYER_MARKER_DISPLAY) = MARKER_OPTIONS_ARROW_ONLY) // Option in the pause menu for different densities of overheads.
//		bOptionOkForArrow = TRUE
//	ENDIF
//	
//	IF ((GET_PACKED_STAT_INT(PACKED_MP_PLAYER_MARKER_DISPLAY) = MARKER_OPTIONS_FULL_WITH_CREWTAG) AND (fDisFromPlayer >= GET_OVERHEAD_ARROW_MARKER_DISTANCE()*GET_OVERHEAD_ARROW_MARKER_DISTANCE())) // Option in the pause menu for different densities of overheads.
//		bOptionOkForArrow = TRUE
//	ENDIF
//	
//	IF ((GET_PACKED_STAT_INT(PACKED_MP_PLAYER_MARKER_DISPLAY) = MARKER_OPTIONS_FULL) AND (fDisFromPlayer >= GET_OVERHEAD_ARROW_MARKER_DISTANCE()*GET_OVERHEAD_ARROW_MARKER_DISTANCE()))
//		bOptionOkForArrow = TRUE
//	ENDIF
//	
//	IF bOptionOkForArrow
//		IF NOT bProcessingLocalPlayer
//			IF bPassedPlayerVisibleToLocalPlayer
//				IF NOT IS_PLAYER_SCTV(PLAYER_ID())
//					IF NOT IS_A_SPECTATOR_CAM_RUNNING()
//						IF IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_LOCAL_DPADDOWNACTIVE, PLAYER_ID())
//						OR IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_JUST_APPEARED_ON_SCREEN, playerId)
//							vCoords = GET_ENTITY_COORDS(GET_PLAYER_PED(playerId))
//							vCoords.Z += 1.00
//							GET_HUD_COLOUR(GET_PLAYER_HUD_COLOUR(playerId), TempColR, TempColG, TempColB, TempColA)
//							DRAW_MARKER(MARKER_ARROW, vCoords, <<0.0,0.0,0.0>>, <<180.0,0.0,0.0>>, <<0.3,0.3,0.3>>, TempColR, TempColG, TempColB, 150)
//						ENDIF
//					ENDIf
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF
//	
//ENDPROC

PROC RUN_OHD_PLAYER_ARROW_CHECK_FM(BOOL bProcessingLocalPlayer, BOOL bPassedPlayerVisibleToLocalPlayer, PLAYER_INDEX playerId)
	
	BOOL bShow
	
	// Whinging compiler.
	IF bProcessingLocalPlayer
		bProcessingLocalPlayer = bProcessingLocalPlayer
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF g_bDoArrowCheckDebug
		PRINTLN("[Overheads] - [Arrow Debug] - calling RUN_OHD_PLAYER_ARROW_CHECK_FM for player ", NATIVE_TO_INT(playerID))
	ENDIF
	#ENDIF
	
	// Actual checks.
	IF IS_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_NOTHING, playerId)
		#IF IS_DEBUG_BUILD
		IF g_TurnOnOverheadDebug
			PRINTLN("[overheads] - RUN_OHD_PLAYER_ARROW_CHECK_FM = FALSE DISPLAYOVERHEADS_DISPLAY_NOTHING for player  ", NATIVE_TO_INT(playerID), " Player GamerTag = ", GET_PLAYER_NAME(playerID))
		ENDIF
		IF g_bDoArrowCheckDebug
			PRINTLN("[Overheads] - [Arrow Debug] - RUN_OHD_PLAYER_ARROW_CHECK_FM - IS_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_NOTHING) = TRUE for player ", NATIVE_TO_INT(playerID))
		ENDIF
		#ENDIF
		bShow = FALSE
		
	ELIF NOT bPassedPlayerVisibleToLocalPlayer
		#IF IS_DEBUG_BUILD
		IF g_TurnOnOverheadDebug
			PRINTLN("[overheads] - RUN_OHD_PLAYER_ARROW_CHECK_FM = FALSE bPassedPlayerVisibleToLocalPlayer = FALSE for player  ", NATIVE_TO_INT(playerID), " Player GamerTag = ", GET_PLAYER_NAME(playerID))
		ENDIF
		IF g_bDoArrowCheckDebug
			PRINTLN("[Overheads] - [Arrow Debug] - RUN_OHD_PLAYER_ARROW_CHECK_FM - bPassedPlayerVisibleToLocalPlayer = FALSE for player ", NATIVE_TO_INT(playerID))
		ENDIF
		#ENDIF
		bShow = FALSE
	
	ELIF ( IS_OVERHEAD_EVENT_ACTIVE(DISPLAYOVERHEADS_EVENTS_IS_PLAYER_SPECTATING, PLAYER_ID()) OR IS_PLAYER_SPECTATING(PLAYER_ID()) )
		#IF IS_DEBUG_BUILD
		IF g_TurnOnOverheadDebug
			PRINTLN("[overheads] - RUN_OHD_PLAYER_ARROW_CHECK_FM = DISPLAYOVERHEADS_EVENTS_IS_PLAYER_SPECTATING = TRUE for local player, setting DISPLAYOVERHEADS_DISPLAY_ARROW to FALSE for player ", NATIVE_TO_INT(playerID))
		ENDIF
		IF g_bDoArrowCheckDebug
			PRINTLN("[Overheads] - [Arrow Debug] - RUN_OHD_PLAYER_ARROW_CHECK_FM - IS_OVERHEAD_EVENT_ACTIVE(DISPLAYOVERHEADS_EVENTS_IS_PLAYER_SPECTATING, PLAYER_ID()) = TRUE.")
		ENDIF
		#ENDIF
		bShow = FALSE
		
	ELIF IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_LOCAL_DPADDOWNACTIVE, PLAYER_ID())
		#IF IS_DEBUG_BUILD 
		IF g_TurnOnOverheadDebug
			PRINTLN("[overheads] - RUN_OHD_PLAYER_ARROW_CHECK_FM = DISPLAYOVERHEADS_LOGIC_IS_LOCAL_DPADDOWNACTIVE = TRUE for Local player, setting DISPLAYOVERHEADS_DISPLAY_ARROW to FALSE for player ", NATIVE_TO_INT(playerID))
		ENDIF
		IF g_bDoArrowCheckDebug
			PRINTLN("[Overheads] - [Arrow Debug] - RUN_OHD_PLAYER_ARROW_CHECK_FM - IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_LOCAL_DPADDOWNACTIVE, PLAYER_ID()) = TRUE.")
		ENDIF
		#ENDIF		
		bShow = TRUE
		
	ELIF IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_JUST_APPEARED_ON_SCREEN, playerId)
		#IF IS_DEBUG_BUILD 
		IF g_TurnOnOverheadDebug
			PRINTLN("[overheads] - RUN_OHD_PLAYER_ARROW_CHECK_FM = DISPLAYOVERHEADS_LOGIC_JUST_APPEARED_ON_SCREEN = TRUE for local player, setting DISPLAYOVERHEADS_DISPLAY_ARROW to FALSE for player ", NATIVE_TO_INT(playerID))
		ENDIF
		IF g_bDoArrowCheckDebug
			PRINTLN("[Overheads] - [Arrow Debug] - RUN_OHD_PLAYER_ARROW_CHECK_FM - IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_JUST_APPEARED_ON_SCREEN = TRUE for player ", NATIVE_TO_INT(playerID))
		ENDIF
		#ENDIF		
		bShow = TRUE
	ENDIF
	
	IF NOT IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_ARROW, playerId)
		#IF IS_DEBUG_BUILD 
		IF g_TurnOnOverheadDebug
			PRINTLN("[overheads] - RUN_OHD_PLAYER_ARROW_CHECK_FM = DISPLAYOVERHEADS_LOGIC_ARROW = FALSE for player ", NATIVE_TO_INT(playerID), " Player GamerTag = ", GET_PLAYER_NAME(playerID))
		ENDIF
		IF g_bDoArrowCheckDebug
			PRINTLN("[Overheads] - [Arrow Debug] - RUN_OHD_PLAYER_ARROW_CHECK_FM - IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_ARROW = FALSE for player ", NATIVE_TO_INT(playerID))
		ENDIF
		#ENDIF		
		bShow = FALSE
	ENDIF
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TOUR_DE_FORCE(g_fmmc_struct.iAdversaryModeType) 
			#IF IS_DEBUG_BUILD 
		IF g_TurnOnOverheadDebug
			PRINTLN("[overheads] - RUN_OHD_PLAYER_ARROW_CHECK_FM = IS_THIS_ROCKSTAR_MISSION_NEW_VS_TOUR_DE_FORCE for player ", NATIVE_TO_INT(playerID), " Player GamerTag = ", GET_PLAYER_NAME(playerID))
		ENDIF
		IF g_bDoArrowCheckDebug
			PRINTLN("[Overheads] - [Arrow Debug] - RUN_OHD_PLAYER_ARROW_CHECK_FM - IS_THIS_ROCKSTAR_MISSION_NEW_VS_TOUR_DE_FORCE ", NATIVE_TO_INT(playerID))
		ENDIF
		#ENDIF		
		bShow = FALSE
	ENDIF
	
	
	
	
	SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_ARROW, bShow, playerID)
	
ENDPROC

FUNC BOOL IS_PLAYER_ON_RC_RACE(PLAYER_INDEX iPlayerID, BOOL bcheckiflocalplayeriscurrentlyracing = FALSE)
	IF iPlayerID != PLAYER_ID()
		IF 	g_b_On_Race
		AND g_FMMC_STRUCT.iRaceClass  = FMMC_VEHICLE_CLASS_SPECIAL
		AND (g_FMMC_STRUCT.iVehicleModel = 10 OR g_FMMC_STRUCT.iVehicleModel = 10  )
			IF bcheckiflocalplayeriscurrentlyracing
				IF NOT g_bMissionEnding 
				AND IS_INSTANCED_CONTENT_COUNTDOWN_FINISHED()
					IF NOT IS_PLAYER_SPECTATING(iPlayerID)
					AND NOT IS_MP_DECORATOR_BIT_SET(iPlayerID, MP_DECORATOR_BS_INVALID_TO_SPECTATE) 
						RETURN TRUE
					ELSE
						RETURN FALSE	
					ENDIF
				ELSE 	
					RETURN FALSE
				ENDIF
			ELSE
				RETURN TRUE
			ENDIF

		ELSE
			RETURN FALSE
		ENDIF
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC



PROC RUN_OHD_PLAYER_NAME_CHECK_FM(BOOL bProcessingLocalPlayer, BOOL bPassedPlayerVisibleToLocalPlayer, PLAYER_INDEX playerId)
	IF IS_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_NOTHING, playerId)
	OR NOT bPassedPlayerVisibleToLocalPlayer
	OR IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_ARROW, playerId)
	OR ( IS_ON_DEATHMATCH_GLOBAL_SET() AND (GlobalServerBD_DM.iTags = TAGS_OFF) )

		IF NOT MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.bOnJobIntro[0]
		AND NOT IS_PLAYER_ON_RC_RACE(playerID, TRUE)	
			#IF IS_DEBUG_BUILD
			IF g_TurnOnOverheadDebug
				NET_NL()NET_PRINT("[overhead] RUN_OHD_PLAYER_NAME_CHECK_FM = FALSE DISPLAYOVERHEADS_DISPLAY_NOTHING for player  ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
				NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))
			ENDIF
			#ENDIF
			
			#IF IS_DEBUG_BUILD
			IF g_TurnOnOverheadDebug
				NET_NL()NET_PRINT("[overhead] RUN_OHD_PLAYER_NAME_CHECK_FM = FALSE bPassedPlayerVisibleToLocalPlayer = FALSE for player  ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
				NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))
			ENDIF
			#ENDIF
			
			SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_PLAYER_NAME, FALSE, playerID )
		ELSE
			
			SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_PLAYER_NAME, TRUE, playerID )	
		ENDIF
		
	ELSE
		
		IF bProcessingLocalPlayer
			IF IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_LOCAL_DPADDOWNACTIVE, PLAYER_ID())
			OR IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_INTERACTION_MENU_UP, PLAYER_ID()) 
			OR IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_JUST_APPEARED_ON_SCREEN, PLAYER_ID())
			OR IS_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_RESPAWNING, PLAYER_ID())
			OR IS_OVERHEAD_EVENT_ACTIVE(DISPLAYOVERHEADS_EVENTS_IN_IMPROMPTU_RACE_COUNTDOWN, PLAYER_ID())
			OR MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.bOnJobIntro[0]
				
				SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_PLAYER_NAME, TRUE, PLAYER_ID() )
				#IF IS_DEBUG_BUILD 
					
					IF g_TurnOnOverheadDebug
						NET_NL()NET_PRINT("[overhead] RUN_OHD_PLAYER_NAME_CHECK_FM = TRUE because of dpad down or interaction menu is up for Local player ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
						NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(PLAYER_ID()))
					ENDIF
					
				#ENDIF
				
			ELSE
				IF IS_OVERHEAD_EVENT_ACTIVE(DISPLAYOVERHEADS_EVENTS_IS_VOTING, PLAYER_ID())
					#IF IS_DEBUG_BUILD
					IF g_TurnOnOverheadDebug
						NET_NL()NET_PRINT("[overhead] RUN_OHD_PLAYER_NAME_CHECK_FM = TRUE because of Voting for Local player ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
							NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(PLAYER_ID()))
					ENDIF
					#ENDIF
					SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_PLAYER_NAME, TRUE, playerID )
				ELSE
					#IF IS_DEBUG_BUILD
					IF g_TurnOnOverheadDebug
						NET_NL()NET_PRINT("[overhead] RUN_OHD_PLAYER_NAME_CHECK_FM = FALSE Local player ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
							NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(PLAYER_ID()))
					ENDIF
					#ENDIF
					SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_PLAYER_NAME, FALSE, PLAYER_ID() )
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT bProcessingLocalPlayer
			
			IF (g_iOverheadNamesState = TAGS_ON AND SHOULD_READ_TAGS_BOOL()) //g_bFM_TAGS_ON = TRUE  //Player Names = On display remote players names always. 
			OR (MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.eOverheadOverrideState[0] = eOVERHEADOVERRIDESTATE_FORCE_NAME_ON)
			OR MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.bOnJobIntro[0]
			OR IS_PLAYER_ON_RC_RACE(playerID, TRUE)		
				#IF IS_DEBUG_BUILD
				IF g_TurnOnOverheadDebug
					IF SHOULD_READ_TAGS_BOOL()
						NET_NL()NET_PRINT("[overhead] RUN_OHD_PLAYER_NAME_CHECK_FM = TRUE because of g_bFM_TAGS_ON = TRUE Remote player ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
						NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))
					ENDIF
				ENDIF
				#ENDIF
				
				SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_PLAYER_NAME, TRUE, playerID )
				
			
			ELIF (g_iOverheadNamesState = TAGS_WHEN_TARGETED //g_bFM_TAGS_ON_WHEN_TARGETTED 
			AND SHOULD_READ_TAGS_BOOL())
			
			
				IF IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_ARE_WANTED, playerId)  
				OR IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_AIMING_AT_PLAYER, playerId) 
				OR IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_LOCAL_DPADDOWNACTIVE, playerId)
				OR IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_INTERACTION_MENU_UP, playerId) 
				OR IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_JUST_APPEARED_ON_SCREEN, playerId)
				OR IS_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_RESPAWNING, playerId)
				OR IS_OVERHEAD_EVENT_ACTIVE(DISPLAYOVERHEADS_EVENTS_IN_IMPROMPTU_RACE_COUNTDOWN, playerId)
				OR IS_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_PAUSED, playerId)
				OR MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.bOnJobIntro[0]
				OR IS_PLAYER_ON_RC_RACE(playerID, TRUE)		
				OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_VEHICLE_OVERHEAD_GAMERTAG) 
				#IF IS_DEBUG_BUILD
				OR GET_COMMANDLINE_PARAM_EXISTS("enablevehiclehealthoverhead")
				#ENDIF
				AND NOT IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_ARROW, playerId)
					#IF IS_DEBUG_BUILD
					IF g_TurnOnOverheadDebug
						NET_NL()NET_PRINT("[overhead] RUN_OHD_PLAYER_NAME_CHECK_FM = TRUE because ")
						NET_NL()NET_PRINT("		- TAGS_WHEN_TARGETED = TRUE  ")
						IF IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_LOCAL_DPADDOWNACTIVE, playerId)
							NET_NL()NET_PRINT("		- DISPLAYOVERHEADS_LOGIC_IS_LOCAL_DPADDOWNACTIVE = TRUE for Remote player ")
						ENDIF
						IF IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_AIMING_AT_PLAYER, playerId)
							NET_NL()NET_PRINT("		- DISPLAYOVERHEADS_LOGIC_IS_AIMING_AT_PLAYER = TRUE for Remote player ")
						ENDIF
						IF IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_ARE_WANTED, playerId)
							NET_NL()NET_PRINT("		- DISPLAYOVERHEADS_LOGIC_ARE_WANTED = TRUE for Remote player ")
						ENDIF
						IF IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_INTERACTION_MENU_UP, playerId)
							NET_NL()NET_PRINT("		- DISPLAYOVERHEADS_LOGIC_IS_INTERACTION_MENU_UP = TRUE for Remote player ")
						ENDIF
						IF IS_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_RESPAWNING, playerId)
							NET_NL()NET_PRINT("		- DISPLAYOVERHEADS_DISPLAY_RESPAWNING = TRUE for Remote player ")
						ENDIF
						IF IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_JUST_APPEARED_ON_SCREEN, playerId)
							NET_NL()NET_PRINT("		- DISPLAYOVERHEADS_LOGIC_JUST_APPEARED_ON_SCREEN = TRUE for Remote player ")
						ENDIF
						IF MPGlobalsAmbience.R2Pdata.bOnCountdown
						NET_NL()NET_PRINT("		- MPGlobalsAmbience.R2Pdata.bOnCountdown = TRUE for Remote player ")
						ENDIF
						
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_VEHICLE_OVERHEAD_GAMERTAG) 
							NET_NL()NET_PRINT("		- g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_VEHICLE_OVERHEAD_GAMERTAG = TRUE for Remote player ")
						ENDIF
						
						NET_NL()NET_PRINT("		- Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))	
					ENDIF
					#ENDIF
					SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_PLAYER_NAME, TRUE, playerID )
				ELSE
					#IF IS_DEBUG_BUILD
					IF g_TurnOnOverheadDebug
						NET_NL()NET_PRINT("[overhead] RUN_OHD_PLAYER_NAME_CHECK_FM = FALSE because of g_bFM_TAGS_ON_WHEN_TARGETTED = TRUE And Is Targetting = FALSE for Remote player ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
						NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))
					ENDIF
					#ENDIF
					SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_PLAYER_NAME, FALSE, playerID )
				ENDIF
				
				
			ELIF (g_iOverheadNamesState = TAGS_OFF //g_bFM_TAGS_ON = FALSE  //Player Names = off display remote players names only when dpaddown is on
			AND SHOULD_READ_TAGS_BOOL())
				
				
				IF IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_LOCAL_DPADDOWNACTIVE, playerId)
				OR IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_INTERACTION_MENU_UP, playerId) 
				OR IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_JUST_APPEARED_ON_SCREEN, playerId)
				OR IS_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_RESPAWNING, playerId)
				OR IS_OVERHEAD_EVENT_ACTIVE(DISPLAYOVERHEADS_EVENTS_IN_IMPROMPTU_RACE_COUNTDOWN, playerId)
				OR IS_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_PAUSED, playerId)
				OR MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.bOnJobIntro[0]
				OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_VEHICLE_OVERHEAD_GAMERTAG) 
				OR IS_PLAYER_ON_RC_RACE(playerID, TRUE)		
				#IF IS_DEBUG_BUILD
				OR GET_COMMANDLINE_PARAM_EXISTS("enablevehiclehealthoverhead")
				#ENDIF
				AND NOT IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_ARROW, playerId)
					
					#IF IS_DEBUG_BUILD
					IF g_TurnOnOverheadDebug
						NET_NL()NET_PRINT("[overhead] RUN_OHD_PLAYER_NAME_CHECK_FM = TRUE because ")
						NET_NL()NET_PRINT("		- TAGS_OFF = TRUE  ")
						IF IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_LOCAL_DPADDOWNACTIVE, playerId)
							NET_NL()NET_PRINT("		- DISPLAYOVERHEADS_LOGIC_IS_LOCAL_DPADDOWNACTIVE = TRUE for Remote player ")
						ENDIF
						IF IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_INTERACTION_MENU_UP, playerId)
							NET_NL()NET_PRINT("		- DISPLAYOVERHEADS_LOGIC_IS_INTERACTION_MENU_UP = TRUE for Remote player ")
						ENDIF
						IF IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_JUST_APPEARED_ON_SCREEN, playerId)
							NET_NL()NET_PRINT("		- DISPLAYOVERHEADS_LOGIC_JUST_APPEARED_ON_SCREEN = TRUE for Remote player ")
						ENDIF
						NET_NL()NET_PRINT("		- Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))	
					ENDIF
					#ENDIF

					SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_PLAYER_NAME, TRUE, playerID )
				ELSE
					#IF IS_DEBUG_BUILD
					IF g_TurnOnOverheadDebug
						NET_NL()NET_PRINT("[overhead] RUN_OHD_PLAYER_NAME_CHECK_FM = FALSE because of TAGS_OFF = TRUE And Dpad down = FALSE for Remote player ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
							NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))
					ENDIF
					#ENDIF
					SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_PLAYER_NAME, FALSE, playerID )
				ENDIF
				
				
				
			ELIF (g_iOverheadNamesState = TAGS_WHEN_INTEAM   //Player Names = off display remote players names only when dpaddown is on and in the same team
			AND SHOULD_READ_TAGS_BOOL()) 	

				
				IF IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_LOCAL_DPADDOWNACTIVE, playerId)
				OR IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_AIMING_AT_PLAYER, playerId) 
				OR IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_INTERACTION_MENU_UP, playerId)
				OR IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_JUST_APPEARED_ON_SCREEN, playerId)
				OR IS_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_RESPAWNING, playerId)
				OR IS_OVERHEAD_EVENT_ACTIVE(DISPLAYOVERHEADS_EVENTS_IN_IMPROMPTU_RACE_COUNTDOWN, playerId)
				OR IS_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_PAUSED, playerId)
				OR MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.bOnJobIntro[0]
				OR IS_PLAYER_ON_RC_RACE(playerID, TRUE)		
				AND NOT IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_ARROW, playerId)
					
					#IF IS_DEBUG_BUILD
					IF g_TurnOnOverheadDebug
						NET_NL()NET_PRINT("[overhead] RUN_OHD_PLAYER_NAME_CHECK_FM = TRUE because ")
						NET_NL()NET_PRINT("		- TAGS_WHEN_INTEAM = TRUE  ")
						IF IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_LOCAL_DPADDOWNACTIVE, playerId)
							NET_NL()NET_PRINT("		- DISPLAYOVERHEADS_LOGIC_IS_LOCAL_DPADDOWNACTIVE = TRUE for Remote player ")
						ENDIF
						IF IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_AIMING_AT_PLAYER, playerId)
							NET_NL()NET_PRINT("		- DISPLAYOVERHEADS_LOGIC_IS_AIMING_AT_PLAYER = TRUE for Remote player ")
						ENDIF
						IF IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_INTERACTION_MENU_UP, playerId)
							NET_NL()NET_PRINT("		- DISPLAYOVERHEADS_LOGIC_IS_INTERACTION_MENU_UP = TRUE for Remote player ")
						ENDIF
						IF IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_JUST_APPEARED_ON_SCREEN, playerId)
							NET_NL()NET_PRINT("		- DISPLAYOVERHEADS_LOGIC_JUST_APPEARED_ON_SCREEN = TRUE for Remote player ")
						ENDIF
						NET_NL()NET_PRINT("		- Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))	
					ENDIF
					#ENDIF
					
					SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_PLAYER_NAME, TRUE, playerID )
				ELSE
				
					IF IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_ARE_IN_SAME_TEAM, playerId) 
					OR IS_MP_PLAYER_BLIPPED(PlayerId)
						
						IF IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_ARE_IN_SAME_VEHICLE, playerId) 
							#IF IS_DEBUG_BUILD
							IF g_TurnOnOverheadDebug
								IF IS_MP_PLAYER_BLIPPED(PlayerId)
									NET_NL()NET_PRINT("[overhead] RUN_OHD_PLAYER_NAME_CHECK_FM = FALSE because of TAGS_WHEN_INTEAM = TRUE ARE_IN_SAME_TEAM = TRUE AND SAME_VEHICLE = TRUE And Dpad down = FALSE for Remote player ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
									NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))
								ENDIF
							ENDIF
							#ENDIF
							SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_PLAYER_NAME, FALSE, playerID )
							
						ELSE
							#IF IS_DEBUG_BUILD
							IF g_TurnOnOverheadDebug
								IF IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_ARE_IN_SAME_TEAM, playerId) 
									NET_NL()NET_PRINT("[overhead] RUN_OHD_PLAYER_NAME_CHECK_FM = TRUE because of TAGS_WHEN_INTEAM = TRUE ARE_IN_SAME_TEAM = TRUE AND SAME_VEHICLE = FALSE And Dpad down = FALSE for Remote player ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
									NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))
								ENDIF
							ENDIF
							#ENDIF
							
							#IF IS_DEBUG_BUILD
							IF g_TurnOnOverheadDebug
								IF IS_MP_PLAYER_BLIPPED(PlayerId)
									NET_NL()NET_PRINT("[overhead] RUN_OHD_PLAYER_NAME_CHECK_FM = TRUE because of TAGS_WHEN_INTEAM = TRUE PLAYER_BLIPPED = TRUE AND SAME_VEHICLE = FALSE And Dpad down = FALSE for Remote player ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
									NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))
								ENDIF
							ENDIF
							#ENDIF
							
							
							
							SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_PLAYER_NAME, TRUE, playerID )
						ENDIF
					
					ELSE
					
						#IF IS_DEBUG_BUILD
						IF g_TurnOnOverheadDebug
							NET_NL()NET_PRINT("[overhead] RUN_OHD_PLAYER_NAME_CHECK_FM = FALSE because of TAGS_WHEN_INTEAM = TRUE and ARE_IN_SAME_TEAM = FALSE And Dpad down = FALSE for Remote player ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
								NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))
						ENDIF
						#ENDIF
						SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_PLAYER_NAME, FALSE, playerID )
					ENDIF
				ENDIF
				
			ELSE

				IF IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_LOCAL_DPADDOWNACTIVE, playerId)
				OR IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_AIMING_AT_PLAYER, playerId)
				OR IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_INTERACTION_MENU_UP, playerId) 
				OR IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_JUST_APPEARED_ON_SCREEN, playerId)
				OR IS_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_RESPAWNING, playerId)
				OR IS_OVERHEAD_EVENT_ACTIVE(DISPLAYOVERHEADS_EVENTS_IN_IMPROMPTU_RACE_COUNTDOWN, playerId)
				OR IS_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_PAUSED, playerId)
				OR MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.bOnJobIntro[0]
				OR IS_PLAYER_ON_RC_RACE(playerID, TRUE)		
				OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_VEHICLE_OVERHEAD_GAMERTAG)
				#IF IS_DEBUG_BUILD
				OR GET_COMMANDLINE_PARAM_EXISTS("enablevehiclehealthoverhead")
				#ENDIF

				AND NOT(IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_ARROW, playerId) AND (IS_PLAYER_USING_ARENA()AND NOT IS_ARENA_WAR_UGC_RACE()))

					#IF IS_DEBUG_BUILD
					IF g_TurnOnOverheadDebug
						IF IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_LOCAL_DPADDOWNACTIVE, playerId)
							NET_NL()NET_PRINT("[overhead] RUN_OHD_PLAYER_NAME_CHECK_FM = TRUE because DPADDOWNACTIVE = TRUE for player  ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
							NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))
						ENDIF
					ENDIF
					#ENDIF
					
					#IF IS_DEBUG_BUILD
					IF g_TurnOnOverheadDebug
						IF IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_AIMING_AT_PLAYER, playerId)
							NET_NL()NET_PRINT("[overhead] RUN_OHD_PLAYER_NAME_CHECK_FM = TRUE because IS_AIMING_AT_PLAYER = TRUE for player  ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
							NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))
						ENDIF
					ENDIF
					#ENDIF
					
					#IF IS_DEBUG_BUILD
					IF g_TurnOnOverheadDebug
						IF IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_INTERACTION_MENU_UP, playerId)
							NET_NL()NET_PRINT("[overhead] RUN_OHD_PLAYER_NAME_CHECK_FM = TRUE because IS_INTERACTION_MENU_UP = TRUE for player  ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
							NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))
						ENDIF
					ENDIF
					#ENDIF
					
					#IF IS_DEBUG_BUILD
					IF g_TurnOnOverheadDebug
						IF IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_JUST_APPEARED_ON_SCREEN, playerId)
							NET_NL()NET_PRINT("[overhead] RUN_OHD_PLAYER_NAME_CHECK_FM = TRUE because JUST_APPEARED_ON_SCREEN = TRUE for player  ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
							NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))
						ENDIF
					ENDIF
					#ENDIF

					SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_PLAYER_NAME, TRUE, playerID )
				ELSE 
					#IF IS_DEBUG_BUILD
					IF g_TurnOnOverheadDebug
						NET_NL()NET_PRINT("[overhead] RUN_OHD_PLAYER_NAME_CHECK_FM = FALSE because IN Same Team but in same vehicle for player  ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
							NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))
					ENDIF
					#ENDIF
					SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_PLAYER_NAME, FALSE, playerID )
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	// For the capture team, brute force on or off gamer tags depending on command line and widget combinations.
	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("enableFarGamerTagDistance")
		SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_PLAYER_NAME, TRUE, playerID)
		IF g_bBlockLocalPlayerOverhead[0]
			SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_PLAYER_NAME, FALSE, PLAYER_ID() )
		ELSE
			SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_PLAYER_NAME, TRUE, PLAYER_ID() )
		ENDIF
	ENDIF
	#ENDIF
			
ENDPROC

PROC RUN_OHD_GANG_ICONS_FM(BOOL bProcessingLocalPlayer, BOOL bPassedPlayerVisibleToLocalPlayer, PLAYER_INDEX playerId)
	
	// If we want to display no overheads.
	IF IS_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_NOTHING, playerId)
	OR NOT bPassedPlayerVisibleToLocalPlayer
	OR NOT GB_IS_PLAYER_MEMBER_OF_A_GANG(playerId, TRUE)
	OR IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_ARROW, playerId)
	OR ( IS_ON_DEATHMATCH_GLOBAL_SET() AND (GlobalServerBD_DM.iTags = TAGS_OFF) )
	
		SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_MC_ROLE, FALSE, playerID )
	
	// If the wipe overheads flag is not set.
	ELSE
		IF GB_IS_PLAYER_MEMBER_OF_A_GANG(playerId, TRUE)
		AND (IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_LOCAL_DPADDOWNACTIVE, playerId) OR IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_AIMING_AT_PLAYER, playerId) )
					// Don't show above local player.
			IF bProcessingLocalPlayer
				SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_MC_ROLE, FALSE, PlayerId )
				
			// Check if we should show above remote player.
			ELSE
						// If tags are set to on and we care about them then turn on the overhead stars.
				IF (g_iOverheadNamesState = TAGS_ON //g_bFM_TAGS_ON = TRUE //Player Names = on display remote players level always
				AND SHOULD_READ_TAGS_BOOL())
					
					SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_MC_ROLE, TRUE, PlayerId )
				
				// If the tag for show when being targetted is on and we care about the tags.
				ELIF (g_iOverheadNamesState = TAGS_WHEN_TARGETED 
				AND SHOULD_READ_TAGS_BOOL())
					
					// If I am aiming at the player, show their stars.
					IF IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_AIMING_AT_PLAYER, playerId)
						SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_MC_ROLE, TRUE, playerID)
					ELSE
						SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_MC_ROLE, FALSE, PlayerId)
					ENDIF
				// If tags are off and we care about tags.
				ELIF (g_iOverheadNamesState = TAGS_OFF //g_bFM_TAGS_ON = FALSE //Player Names = off display remote players level only when dpaddown is on
				AND SHOULD_READ_TAGS_BOOL())
					
					// If dpad down has been pressed or the they have just appeared on screen.
					IF IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_LOCAL_DPADDOWNACTIVE, playerId)
					OR IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_JUST_APPEARED_ON_SCREEN, playerId)
					OR IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_AIMING_AT_PLAYER, playerId)
						SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_MC_ROLE, TRUE, PlayerId )
					ELSE
						SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_MC_ROLE, FALSE, PlayerId)
					ENDIF
					
				ELIF (g_iOverheadNamesState = TAGS_WHEN_INTEAM   //Player Names = off display remote players names only when dpaddown is on and in the same team
				AND SHOULD_READ_TAGS_BOOL()) 	
					
					// If dpad down has been pressed or the they have just appeared on screen.
					IF IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_LOCAL_DPADDOWNACTIVE, playerId)
					OR IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_JUST_APPEARED_ON_SCREEN, playerId)
					OR IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_AIMING_AT_PLAYER, playerId)
//						NET_NL()NET_PRINT("DISPLAYOVERHEADS_LOGIC_ARE_WANTED = TRUE 2")
						SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_MC_ROLE, TRUE, PlayerId)
					ELSE
						SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_MC_ROLE, FALSE, PlayerId)
					ENDIF
					
				ELSE
					
					IF NOT IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_ARE_IN_SAME_VOTE, playerId)
						SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_MC_ROLE, TRUE, PlayerId )
					ELSE
						SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_MC_ROLE, FALSE, PlayerId )
//						NET_NL()NET_PRINT("DISPLAYOVERHEADS_LOGIC_ARE_WANTED = TRUE 4")
					ENDIF
					
				ENDIF	
				
				
				
			ENDIF
			
		ELSE
			
			SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_MC_ROLE, FALSE, PlayerId)
			
		ENDIF

	ENDIF
		
		
ENDPROC

PROC RUN_OHD_PACKAGE_TRANSMITTORS(BOOL bProcessingLocalPlayer, BOOL bPassedPlayerVisibleToLocalPlayer, PLAYER_INDEX playerId)
	
	// If we want to display no overheads.
	//IF IS_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_NOTHING, playerId)
	//OR NOT bPassedPlayerVisibleToLocalPlayer

	//	SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_PACKAGE_TRANSMITTOR, FALSE, playerID )
	//	PRINTLN(" RUN_OHD_PACKAGE_TRANSMITTORS 1")
	// If the wipe overheads flag is not set.
	//ELSE
		IF g_iDawnRaidPickupCarrierPart = -1 
			SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_PACKAGE_TRANSMITTOR, FALSE, PlayerId )
			EXIT
		ENDIF

						// If tags are set to on and we care about them then turn on the overhead stars.
		IF (g_iOverheadNamesState = TAGS_ON //g_bFM_TAGS_ON = TRUE //Player Names = on display remote players level always
		AND SHOULD_READ_TAGS_BOOL())
		//	PRINTLN(" RUN_OHD_PACKAGE_TRANSMITTORS 2")
			SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_PACKAGE_TRANSMITTOR, TRUE, PlayerId )
		
		// If the tag for show when being targetted is on and we care about the tags.
		ELIF (g_iOverheadNamesState = TAGS_WHEN_TARGETED 
		AND SHOULD_READ_TAGS_BOOL())
			
			// If I am aiming at the player, show their stars.
			IF IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_AIMING_AT_PLAYER, playerId)
				SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_PACKAGE_TRANSMITTOR, TRUE, playerID)
			ELSE
				SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_PACKAGE_TRANSMITTOR, FALSE, PlayerId)
			ENDIF
		//	PRINTLN(" RUN_OHD_PACKAGE_TRANSMITTORS 3")
		// If tags are off and we care about tags.
		ELIF (g_iOverheadNamesState = TAGS_OFF //g_bFM_TAGS_ON = FALSE //Player Names = off display remote players level only when dpaddown is on
		AND SHOULD_READ_TAGS_BOOL())
			
			// If dpad down has been pressed or the they have just appeared on screen.
			IF IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_LOCAL_DPADDOWNACTIVE, playerId)
			OR IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_JUST_APPEARED_ON_SCREEN, playerId)
			OR IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_AIMING_AT_PLAYER, playerId)
			OR NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(g_iDawnRaidPickupCarrierPart) )= playerId
		
				SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_PACKAGE_TRANSMITTOR, TRUE, PlayerId )
			ELSE
				SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_PACKAGE_TRANSMITTOR, FALSE, PlayerId)
			ENDIF
			//PRINTLN(" RUN_OHD_PACKAGE_TRANSMITTORS 6")
		ELIF (g_iOverheadNamesState = TAGS_WHEN_INTEAM   //Player Names = off display remote players names only when dpaddown is on and in the same team
		AND SHOULD_READ_TAGS_BOOL()) 	
			
			// If dpad down has been pressed or the they have just appeared on screen.
			IF IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_LOCAL_DPADDOWNACTIVE, playerId)
			OR IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_JUST_APPEARED_ON_SCREEN, playerId)
			OR IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_AIMING_AT_PLAYER, playerId)
		
		
//				NET_NL()NET_PRINT("DISPLAYOVERHEADS_LOGIC_ARE_WANTED = TRUE 2")
				SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_PACKAGE_TRANSMITTOR, TRUE, PlayerId)
			ELSE
				SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_PACKAGE_TRANSMITTOR, FALSE, PlayerId)
			ENDIF
			//PRINTLN(" RUN_OHD_PACKAGE_TRANSMITTORS 5")
		
		ELSE
		//	PRINTLN(" RUN_OHD_PACKAGE_TRANSMITTORS 4")
		
			IF NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(g_iDawnRaidPickupCarrierPart) )= playerId
				SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_PACKAGE_TRANSMITTOR, TRUE, PlayerId )
			ELSE
				SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_PACKAGE_TRANSMITTOR, FALSE, PlayerId )
	//			NET_NL()NET_PRINT("DISPLAYOVERHEADS_LOGIC_ARE_WANTED = TRUE 4")
			ENDIF

			
		ENDIF	

	//ENDIF
		
	IF bProcessingLocalPlayer
	OR bPassedPlayerVisibleToLocalPlayer
	
	ENDIF
		
ENDPROC

PROC RUN_OHD_WANTED_LEVEL_CHECK_FM(BOOL bProcessingLocalPlayer, BOOL bPassedPlayerVisibleToLocalPlayer, PLAYER_INDEX playerId, INT iCurrentWantedLevel, INT iCurrentFakeWantedLevel)
	
	// If we want to display no overheads.
	IF IS_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_NOTHING, playerId)
	OR NOT bPassedPlayerVisibleToLocalPlayer
	OR ( (iCurrentWantedLevel <= 0) AND (iCurrentFakeWantedLevel < 0) )
	OR IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_ARROW, playerId)
	OR ( IS_ON_DEATHMATCH_GLOBAL_SET() AND (GlobalServerBD_DM.iTags = TAGS_OFF) )
	
		SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_WANTEDLEVEL, FALSE, playerID )
	
	// If the wipe overheads flag is not set.
	ELSE
	
		// Check the player is wanted.
		IF IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_ARE_WANTED, playerId)
		AND (IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_LOCAL_DPADDOWNACTIVE, playerId) OR IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_AIMING_AT_PLAYER, playerId) )
		
			// Don't show above local player.
			IF bProcessingLocalPlayer
			
				SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_WANTEDLEVEL, FALSE, PlayerId )
				
			// Check if we should show above remote player.
			ELSE
				
				// If tags are set to on and we care about them then turn on the overhead stars.
				IF (g_iOverheadNamesState = TAGS_ON //g_bFM_TAGS_ON = TRUE //Player Names = on display remote players level always
				AND SHOULD_READ_TAGS_BOOL())
					
					SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_WANTEDLEVEL, TRUE, PlayerId )
				
				// If the tag for show when being targetted is on and we care about the tags.
				ELIF (g_iOverheadNamesState = TAGS_WHEN_TARGETED 
				AND SHOULD_READ_TAGS_BOOL())
					
					// If I am aiming at the player, show their stars.
					IF IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_AIMING_AT_PLAYER, playerId)
						SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_WANTEDLEVEL, TRUE, playerID)
					ELSE
						SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_WANTEDLEVEL, FALSE, PlayerId)
					ENDIF
				
				// If tags are off and we care about tags.
				ELIF (g_iOverheadNamesState = TAGS_OFF //g_bFM_TAGS_ON = FALSE //Player Names = off display remote players level only when dpaddown is on
				AND SHOULD_READ_TAGS_BOOL())
					
					// If dpad down has been pressed or the they have just appeared on screen.
					IF IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_LOCAL_DPADDOWNACTIVE, playerId)
					OR IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_JUST_APPEARED_ON_SCREEN, playerId)
					OR IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_AIMING_AT_PLAYER, playerId)
						SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_WANTEDLEVEL, TRUE, PlayerId )
					ELSE
						SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_WANTEDLEVEL, FALSE, PlayerId)
					ENDIF
					
				ELIF (g_iOverheadNamesState = TAGS_WHEN_INTEAM   //Player Names = off display remote players names only when dpaddown is on and in the same team
				AND SHOULD_READ_TAGS_BOOL()) 	
					
					// If dpad down has been pressed or the they have just appeared on screen.
					IF IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_LOCAL_DPADDOWNACTIVE, playerId)
					OR IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_JUST_APPEARED_ON_SCREEN, playerId)
					OR IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_AIMING_AT_PLAYER, playerId)
//						NET_NL()NET_PRINT("DISPLAYOVERHEADS_LOGIC_ARE_WANTED = TRUE 2")
						SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_WANTEDLEVEL, TRUE, PlayerId)
					ELSE
						SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_WANTEDLEVEL, FALSE, PlayerId)
					ENDIF
					
				ELSE
					
					IF NOT IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_ARE_IN_SAME_VOTE, playerId)
						SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_WANTEDLEVEL, TRUE, PlayerId )
					ELSE
						SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_WANTEDLEVEL, FALSE, PlayerId )
//						NET_NL()NET_PRINT("DISPLAYOVERHEADS_LOGIC_ARE_WANTED = TRUE 4")
					ENDIF
					
				ENDIF	
				
				// For Mr. Watson's tutorial help text.
				IF NOT IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet6, BI_FM_NMH6_OVERHEAD_WANTED)
					IF IS_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_WANTEDLEVEL, PlayerId)
						SET_BIT(MPGlobalsAmbience.iFmNmhBitSet6, BI_FM_NMH6_OVERHEAD_WANTED)
					ENDIF
				ENDIF
				
			ENDIF
			
		ELSE
			
			SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_WANTEDLEVEL, FALSE, PlayerId)
			
		ENDIF

	ENDIF

ENDPROC



PROC RUN_OHD_TALKING_CHECK(BOOL bProcessingLocalPlayer, BOOL bPassedPlayerVisibleToLocalPlayer, PLAYER_INDEX playerId)
	
	IF IS_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_NOTHING, playerId)
	OR NOT bPassedPlayerVisibleToLocalPlayer
	OR IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_ARROW, playerId)
	OR ( IS_ON_DEATHMATCH_GLOBAL_SET() AND (GlobalServerBD_DM.iTags = TAGS_OFF) )
		
		IF NOT MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.bOnJobIntro[0]
			SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_AUDIO, FALSE, playerID )
		ELSE
			IF IS_OVERHEAD_EVENT_ACTIVE(DISPLAYOVERHEADS_EVENTS_TALKING, playerId)
				SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_AUDIO, IS_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_PLAYER_NAME, playerID ), playerID )
			ELSE
				SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_AUDIO, FALSE, playerID )
			ENDIF
		ENDIF
		
	ELSE
		
		IF NOT bProcessingLocalPlayer		
			//If the player is talking and we can see the player name then let us see if they are talking.
			IF IS_OVERHEAD_EVENT_ACTIVE(DISPLAYOVERHEADS_EVENTS_TALKING, playerId)
				SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_AUDIO, IS_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_PLAYER_NAME, playerID ), playerID )
			ELSE
				SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_AUDIO, FALSE, playerID )
			ENDIF
		ENDIF
		
	ENDIF

ENDPROC

PROC RUN_OHD_CHAT_WINDOW_CHECK(BOOL bProcessingLocalPlayer, BOOL bPassedPlayerVisibleToLocalPlayer, PLAYER_INDEX playerId)
	
	BOOL bDisplay = TRUE
	
	IF IS_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_NOTHING, playerId)
		#IF IS_DEBUG_BUILD
		IF g_TurnOnOverheadDebug
			PRINTLN("[overhead] - RUN_OHD_CHAT_WINDOW_CHECK - bDisplay = FALSE, DISPLAYOVERHEADS_DISPLAY_NOTHING is active.")
		ENDIF
		#ENDIF
		bDisplay = FALSE
	ENDIF
	
	IF NOT bPassedPlayerVisibleToLocalPlayer
		#IF IS_DEBUG_BUILD
		IF g_TurnOnOverheadDebug
			PRINTLN("[overhead] - RUN_OHD_CHAT_WINDOW_CHECK - bDisplay = FALSE, bPassedPlayerVisibleToLocalPlayer = FALSE.")
		ENDIF
		#ENDIF
		bDisplay = FALSE
	ENDIF
	
	IF IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_ARROW, playerId)
		#IF IS_DEBUG_BUILD
		IF g_TurnOnOverheadDebug
			PRINTLN("[overhead] - RUN_OHD_CHAT_WINDOW_CHECK - bDisplay = FALSE, DISPLAYOVERHEADS_LOGIC_ARROW is active.")
		ENDIF
		#ENDIF
		bDisplay = FALSE
	ENDIF
	
	IF IS_ON_DEATHMATCH_GLOBAL_SET() 
		IF GlobalServerBD_DM.iTags = TAGS_OFF
			#IF IS_DEBUG_BUILD
			IF g_TurnOnOverheadDebug
				PRINTLN("[overhead] - RUN_OHD_CHAT_WINDOW_CHECK - bDisplay = FALSE, IS_ON_DEATHMATCH_GLOBAL_SET = TRUE and GlobalServerBD_DM.iTags = TAGS_OFF.")
			ENDIF
			#ENDIF
			bDisplay = FALSE
		ENDIF
	ENDIF	
	
	IF bProcessingLocalPlayer
		#IF IS_DEBUG_BUILD
		IF g_TurnOnOverheadDebug
			PRINTLN("[overhead] - RUN_OHD_CHAT_WINDOW_CHECK - bDisplay = FALSE, bProcessingLocalPlayer = TRUE.")
		ENDIF
		#ENDIF
		bDisplay = FALSE
	ENDIF
	
	IF NOT IS_OVERHEAD_EVENT_ACTIVE(DISPLAYOVERHEADS_EVENTS_CHAT_WINDOW, playerId)
		#IF IS_DEBUG_BUILD
		IF g_TurnOnOverheadDebug
			PRINTLN("[overhead] - RUN_OHD_CHAT_WINDOW_CHECK - bDisplay = FALSE, DISPLAYOVERHEADS_EVENTS_CHAT_WINDOW is not active.")
		ENDIF
		#ENDIF
		bDisplay = FALSE
	ENDIF
	
	// Attached to player name, only show if that is showing.
	IF NOT IS_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_PLAYER_NAME, playerID)
		#IF IS_DEBUG_BUILD
		IF g_TurnOnOverheadDebug
			PRINTLN("[overhead] - RUN_OHD_CHAT_WINDOW_CHECK - bDisplay = FALSE, DISPLAYOVERHEADS_DISPLAY_PLAYER_NAME is not active.")
		ENDIF
		#ENDIF
		bDisplay = FALSE
	ENDIF
	
	SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_CHAT_WINDOW, bDisplay, playerID)
			
ENDPROC


FUNC BOOL CAN_DISPLAY_HEALTH_BAR(PLAYER_INDEX playerId, BOOL bPassedPlayerVisibleToLocalPlayer)


		IF IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE()
		AND NOT IS_USING_ROAMING_SPECTATOR_POWERUP()
			RETURN FALSE
		ENDIF
		
		
		// Mission Controller Check
		IF IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_DISPLAY_HEALTHBAR, playerId)
		AND IS_ON_RALLY_RACE_GLOBAL_SET()= FALSE
		AND IS_FFA_RACE_GLOBAL_SET()= FALSE
		AND IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_AIMING_AT_PLAYER, playerId) AND (playerId != PLAYER_ID()) 
		AND bPassedPlayerVisibleToLocalPlayer
			RETURN TRUE
		ENDIF
		
		
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_VEHICLE_OVERHEAD_HEALTH)
		AND IS_PED_IN_ANY_VEHICLE( GET_PLAYER_PED(playerId) )
		AND (playerId != PLAYER_ID() OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne, ciOptionsBS21_ShowVehicleHealthOverheadForLocalPlayer))
		AND NOT IS_ARENA_WAR_UGC_RACE()
		AND bPassedPlayerVisibleToLocalPlayer
			RETURN TRUE
		ENDIF
		
		
		#IF IS_DEBUG_BUILD
			IF (GET_COMMANDLINE_PARAM_EXISTS("enablevehiclehealthoverhead") AND IS_PED_IN_ANY_VEHICLE( GET_PLAYER_PED(playerId)))
				RETURN TRUE
			ENDIF
		#ENDIF 
			
			
		// For Destruction Derby (this uses the Race Controller)
		IF IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_DISPLAY_HEALTHBAR, playerId)
		AND (IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_AIMING_AT_PLAYER, playerId) AND (playerId != PLAYER_ID())) 
			RETURN TRUE
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_VEHICLE_OVERHEAD_HEALTH)
		AND IS_PED_IN_ANY_VEHICLE( GET_PLAYER_PED(playerId) )
		AND IS_PLAYER_USING_ARENA() 
		AND NOT IS_ARENA_WAR_UGC_RACE()   //this will need to be replaced with a destruction debry check
		AND NOT IS_PLAYER_RESPAWNING(playerId) 
		AND NOT IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_DISPLAY_HEALTHBAR, playerId)
		AND bPassedPlayerVisibleToLocalPlayer
			RETURN TRUE
		ENDIF
		

		
		RETURN FALSE

ENDFUNC

PROC RUN_OHD_HEALTHBAR_CHECK(BOOL bPassedPlayerVisibleToLocalPlayer, PLAYER_INDEX playerId)
	
	BOOL bShow
	
	IF IS_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_NOTHING, playerId) 
	OR NOT bPassedPlayerVisibleToLocalPlayer
	OR IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_ARROW, playerId)
	OR (IS_ON_DEATHMATCH_GLOBAL_SET() AND GlobalServerBD_DM.iHealthBar = DM_HEALTH_BARS_OFF)
	OR IS_BIT_SET(MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.iHideHealthBarsBitset[1], NATIVE_TO_INT(playerId))
		// hide the health bar here
		
		#IF IS_DEBUG_BUILD
		IF g_TurnOnOverheadDebug
			NET_NL()NET_PRINT("[overhead] RUN_OHD_HEALTHBAR_CHECK = FALSE DISPLAYOVERHEADS_DISPLAY_NOTHING for player ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
			NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))
		ENDIF
		#ENDIF
		
		#IF IS_DEBUG_BUILD
		IF g_TurnOnOverheadDebug
			NET_NL()NET_PRINT("[overhead] RUN_OHD_HEALTHBAR_CHECK = FALSE bPassedPlayerVisibleToLocalPlayer = FALSE for player ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
			NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))
		ENDIF
		#ENDIF
		
		bShow = FALSE
		
		// override the hide healthbar
		IF CAN_DISPLAY_HEALTH_BAR(playerId, bPassedPlayerVisibleToLocalPlayer)
			bShow = TRUE
			
			IF IS_PED_IN_ANY_VEHICLE( GET_PLAYER_PED(PLAYER_ID()) )
				IF GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(GET_PLAYER_PED(PLAYER_ID()))) = BOMBUSHKA
				AND GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_VEHICLE) = CAM_VIEW_MODE_FIRST_PERSON
					bShow = FALSE
				ENDIF
			ENDIF
			
			IF bShow
				IF IS_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_HEALTHBAR,  playerID)
					PRINTLN("[overheads] 2 RUN_OHD_HEALTHBAR_CHECK DISPLAYOVERHEADS_DISPLAY_HEALTHBAR Already true. Exit ,", bShow)
					EXIT
				ENDIF
			ENDIF
			
			SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_HEALTHBAR, bShow, playerID )
			PRINTLN("[overheads] 2 RUN_OHD_HEALTHBAR_CHECK DISPLAYOVERHEADS_DISPLAY_HEALTHBAR = ,", bShow)
			
			EXIT
			
		ENDIF
	ELSE
		

		//Logic to display Health bar. 
		IF CAN_DISPLAY_HEALTH_BAR(playerId, bPassedPlayerVisibleToLocalPlayer)
			IF NOT IS_ON_DEATHMATCH_GLOBAL_SET() 
				bShow = IS_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_PLAYER_NAME, playerID )
			ELSE
				bShow = TRUE
			ENDIF
			
			IF IS_PED_IN_ANY_VEHICLE( GET_PLAYER_PED(PLAYER_ID()) )
				IF GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(GET_PLAYER_PED(PLAYER_ID()))) = BOMBUSHKA
				AND GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_VEHICLE) = CAM_VIEW_MODE_FIRST_PERSON
					bShow = FALSE
				ENDIF
			ENDIF
			
			SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_HEALTHBAR, bShow, playerID )
			
			IF  IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_VEHICLE_OVERHEAD_HEALTH)
			AND IS_PED_IN_ANY_VEHICLE( GET_PLAYER_PED(playerId) )
			AND 	playerId != PLAYER_ID()
			#IF IS_DEBUG_BUILD
			OR (GET_COMMANDLINE_PARAM_EXISTS("enablevehiclehealthoverhead") AND IS_PED_IN_ANY_VEHICLE( GET_PLAYER_PED(playerId)))
			#ENDIF
				IF bShow = FALSE
					bShow = TRUE
					IF NOT IS_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_HEALTHBAR,  playerID)
						SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_HEALTHBAR, bShow, playerID )
						PRINTLN("[overheads] RUN_OHD_HEALTHBAR_CHECK DISPLAYOVERHEADS_DISPLAY_HEALTHBAR =  TRUE")
					ELSE
						PRINTLN("[overheads] RUN_OHD_HEALTHBAR_CHECK DISPLAYOVERHEADS_DISPLAY_HEALTHBAR ALREADY SHOWING!")
					ENDIF
					
				ELSE
					PRINTLN("[overheads] RUN_OHD_HEALTHBAR_CHECK DISPLAYOVERHEADS_DISPLAY_HEALTHBAR ALREADY  TRUE")
					EXIT
				ENDIF
			ENDIF
		ELSE
		
			#IF IS_DEBUG_BUILD
			IF g_TurnOnOverheadDebug
				NET_NL()NET_PRINT("[overhead] RUN_OHD_HEALTHBAR_CHECK = FALSE for player ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
				IF ((IS_ON_DEATHMATCH_GLOBAL_SET() AND GlobalServerBD_DM.iHealthBar = DM_HEALTH_BARS_OFF) = FALSE) = TRUE
					NET_NL()NET_PRINT("		- IS_ON_DEATHMATCH_GLOBAL_SET() AND GlobalServerBD_DM.iHealthBar = DM_HEALTH_BARS_OFF = TRUE")
				ENDIF
				IF IS_ON_RALLY_RACE_GLOBAL_SET()= TRUE
					NET_NL()NET_PRINT("		- IS_ON_RALLY_RACE_GLOBAL_SET()= TRUE ")
				ENDIF
				IF IS_FFA_RACE_GLOBAL_SET()= TRUE
					NET_NL()NET_PRINT("		- IS_FFA_RACE_GLOBAL_SET()= TRUE ")
				ENDIF
				NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))
			ENDIF
			#ENDIF
		
			bShow = FALSE
			
		ENDIF
	ENDIF
	
	PRINTLN("[overheads] RUN_OHD_HEALTHBAR_CHECK bShow = ", bShow)
	
	SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_HEALTHBAR, bShow, playerID )
	
ENDPROC



PROC RUN_OHD_INVENTORY_CHECK(BOOL bProcessingLocalPlayer, INT iPlayerNum, PLAYER_INDEX playerId)
	
	
//SET_BIT(GlobalplayerBD[iGbdSlot].iMyOverHeadInventory, ENUM_TO_INT(invent_sptnames))
	IF bProcessingLocalPlayer
	
	ENDIF 
	IF iPlayerNum = 0
	
	ENDIF
	IF IS_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_NOTHING, playerId) 
	OR ( IS_ON_DEATHMATCH_GLOBAL_SET() AND (GlobalServerBD_DM.iTags = TAGS_OFF) )
		#IF IS_DEBUG_BUILD
			IF g_TurnOnOverheadDebug
				NET_NL()NET_PRINT("[overhead] RUN_OHD_INVENTORY_CHECK: DISPLAYOVERHEADS_DISPLAY_NOTHING = TRUE")
			ENDIF
			
		#ENDIF
		BOOL bdisplayinventory = FALSE
		
		IF  (g_FMMC_STRUCT.iPTLOwnedPickUps[NATIVE_TO_INT(PlayerId)] >= 0 AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciPOINTLESS_MODE_ENABLED))
		OR  (IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_LOCAL_DPADDOWNACTIVE, PLAYER_ID()) AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciPOINTLESS_MODE_ENABLED) )
		OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciPOINTLESS_ENABLE_OVERHEAD_DISPLAY) 
			bdisplayinventory = TRUE
			PRINTLN("[overhead] RUN_OHD_INVENTORY_CHECK: = TRUE ")
		ENDIF
		
		
		IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(playerID)
			SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_INVENTORY, bdisplayinventory, playerID )
		ENDIF
	ELSE
		IF  (g_FMMC_STRUCT.iPTLOwnedPickUps[NATIVE_TO_INT(PlayerId)] >= 0 AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciPOINTLESS_MODE_ENABLED))
		OR GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bdisplayinventoverplayershead  
		OR  (IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_LOCAL_DPADDOWNACTIVE, PLAYER_ID()) AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciPOINTLESS_MODE_ENABLED) )
		OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciPOINTLESS_ENABLE_OVERHEAD_DISPLAY) 
		OR IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(playerID)].iMyOverHeadInventory, ENUM_TO_INT(MP_TAG_TRACKIFY)) AND IS_THIS_ROCKSTAR_MISSION_NEW_VS_DAWN_RAID(g_FMMC_STRUCT.iAdversaryModeType)   // fix 3470217 KW 26/4/17

		AND IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_ARE_IN_SAME_MISSION, playerId)
	
		
			#IF IS_DEBUG_BUILD
				IF g_TurnOnOverheadDebug
					IF  IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_ARE_IN_SAME_MISSION, playerId)
						NET_NL()NET_PRINT("[overhead] RUN_OHD_INVENTORY_CHECK: DISPLAYOVERHEADS_LOGIC_ARE_IN_SAME_MISSION = TRUE")
					ENDIF
					IF GlobalplayerBD[iPlayerNum].iMyOverHeadInventory > 0
						NET_NL()NET_PRINT("[overhead] RUN_OHD_INVENTORY_CHECK: GlobalplayerBD[iPlayerNum].iMyOverHeadInventory > 0 = TRUE")
					ENDIF
				ENDIF
				
				
			#ENDIF
			 
			IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(playerID)
				IF not IS_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_INVENTORY, playerId)
				AND IS_MP_GAMER_TAG_MOVIE_ACTIVE()
				AND IS_NET_PLAYER_OK(playerId)
					SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_INVENTORY, TRUE, playerID )
				ENDIF
			//IF g_FMMC_STRUCT.iPTLOwnedPickUps[NATIVE_TO_INT(PlayerId)] > 0	
				IF 	IS_MP_GAMER_TAG_ACTIVE(NATIVE_TO_INT(PlayerId))
					SET_MP_GAMER_TAG_NUM_PACKAGES(NATIVE_TO_INT(PlayerId), g_FMMC_STRUCT.iPTLOwnedPickUps[NATIVE_TO_INT(PlayerId)])
				endif
				PRINTLN("[overhead] 2 RUN_OHD_INVENTORY_CHECK:  g_FMMC_STRUCT.iPTLOwnedPickUps[NATIVE_TO_INT(PlayerId)] ",  g_FMMC_STRUCT.iPTLOwnedPickUps[NATIVE_TO_INT(PlayerId)])
			//ENDIF
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
				IF g_TurnOnOverheadDebug
					IF  IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_ARE_IN_SAME_MISSION, playerId)
						NET_NL()NET_PRINT("[overhead] RUN_OHD_INVENTORY_CHECK: DISPLAYOVERHEADS_LOGIC_ARE_IN_SAME_MISSION = FALSE")
					ENDIF
					IF GlobalplayerBD[iPlayerNum].iMyOverHeadInventory > 0
						NET_NL()NET_PRINT("[overhead] RUN_OHD_INVENTORY_CHECK: GlobalplayerBD[iPlayerNum].iMyOverHeadInventory > 0 = FALSE")
					ENDIF
				ENDIF
			#ENDIF
			IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(playerID)
				SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_INVENTORY, FALSE, playerID )
			ENDIF
		ENDIF
	ENDIF

ENDPROC

FUNC BOOL CREW_TAG_DPAD_DOWN_CHECK(PLAYER_INDEX playerId)
	
	IF NOT IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_LOCAL_DPADDOWNACTIVE, playerId)
	OR ( IS_ON_DEATHMATCH_GLOBAL_SET() AND (GlobalServerBD_DM.iTags = TAGS_OFF) )
		NET_PRINT("[WJK] - RUN_OHD_CREW_CHECK - CREW_TAG_DPAD_DOWN_CHECK - DISPLAYOVERHEADS_LOGIC_IS_LOCAL_DPADDOWNACTIVE not active.")NET_NL()
		RETURN FALSE
	ENDIF
	
	IF MPGlobalsHud_TitleUpdate.bGamertagsDpadDownState != DPADDOWN_SECOND
		NET_PRINT("[WJK] - RUN_OHD_CREW_CHECK - CREW_TAG_DPAD_DOWN_CHECK - MPGlobalsHud_TitleUpdate.bGamertagsDpadDownState != DPADDOWN_SECOND.")NET_NL()
		NET_PRINT("[WJK] - RUN_OHD_CREW_CHECK - CREW_TAG_DPAD_DOWN_CHECK - MPGlobalsHud_TitleUpdate.bGamertagsDpadDownState = ")
		IF MPGlobalsHud_TitleUpdate.bGamertagsDpadDownState = DPADDOWN_NONE
			NET_PRINT("DPADDOWN_NONE,")
		ENDIF
		IF MPGlobalsHud_TitleUpdate.bGamertagsDpadDownState = DPADDOWN_FIRST
			NET_PRINT("DPADDOWN_FIRST.")
		ENDIF
		NET_NL()
		RETURN FALSE
	ENDIF
	
	NET_PRINT("[WJK] - RUN_OHD_CREW_CHECK - CREW_TAG_DPAD_DOWN_CHECK - returning TRUE.")NET_NL()
	RETURN TRUE
	
ENDFUNC

PROC RUN_OHD_CREW_CHECK(BOOL bProcessingLocalPlayer, BOOL bPassedPlayerVisibleToLocalPlayer, PLAYER_INDEX playerId)

	IF IS_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_NOTHING, playerId) 
	OR NOT bPassedPlayerVisibleToLocalPlayer
	OR IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_ARROW, playerId)
	OR ( IS_ON_DEATHMATCH_GLOBAL_SET() AND (GlobalServerBD_DM.iTags = TAGS_OFF) )
	OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciPOINTLESS_MODE_ENABLED)
		SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_CLAN, FALSE, playerID )
	ELSE
		IF IS_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_PLAYER_NAME, playerId)
		AND (IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_AIMING_AT_PLAYER, playerId) AND (playerId != PLAYER_ID()))
		AND (GET_PACKED_STAT_INT(PACKED_MP_PLAYER_MARKER_DISPLAY) = MARKER_OPTIONS_FULL_WITH_CREWTAG)
		
			GAMER_HANDLE aGamerHandle = GET_GAMER_HANDLE_PLAYER(playerId) //GET_PLAYER_GAMER_HANDLE(playerId)
			IF IS_PLAYER_IN_ACTIVE_CLAN(aGamerHandle)
				IF bProcessingLocalPlayer  
//				OR IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_ARE_IN_SAME_TEAM, playerId)
					IF NOT CREW_TAG_DPAD_DOWN_CHECK(playerID)
					AND NOT IS_OVERHEAD_EVENT_ACTIVE(DISPLAYOVERHEADS_EVENTS_IS_VOTING, playerId)
//					AND NOT IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_JUST_APPEARED_ON_SCREEN, playerId)
					
						
						#IF IS_DEBUG_BUILD
						IF g_TurnOnOverheadDebug
							NET_NL()NET_PRINT("[overhead] RUN_OHD_CREW_CHECK = FALSE beacuse dpaddown = FALSE and NOT Voting for local player  ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
								NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))
						ENDIF
						#ENDIF
						


						SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_CLAN, FALSE , playerID)
					ELSE
						#IF IS_DEBUG_BUILD
						IF g_TurnOnOverheadDebug
							IF IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_LOCAL_DPADDOWNACTIVE, playerId)
								NET_NL()NET_PRINT("[overhead] RUN_OHD_CREW_CHECK = TRUE beacuse dpaddown = TRUE for local player  ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
								NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))
							ENDIF
							
							IF IS_OVERHEAD_EVENT_ACTIVE(DISPLAYOVERHEADS_EVENTS_IS_VOTING, playerId)
								NET_NL()NET_PRINT("[overhead] RUN_OHD_CREW_CHECK = TRUE beacuse is_Voting = TRUE for local player  ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
								NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))
							ENDIF
							
							IF IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_JUST_APPEARED_ON_SCREEN, playerId)
								NET_NL()NET_PRINT("[overhead] RUN_OHD_CREW_CHECK = TRUE beacuse is_Voting = TRUE for local player  ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
								NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))
							ENDIF
						ENDIF
						#ENDIF
						SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_CLAN, TRUE , playerID)
					ENDIF
				
				ELSE

					IF NOT CREW_TAG_DPAD_DOWN_CHECK(playerID)
					AND IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_ARE_IN_SAME_VOTE, playerId) = FALSE
					AND IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_ARE_IN_SAME_VEHICLE, playerId) = FALSE
					AND IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_AIMING_AT_PLAYER, playerId) = FALSE
					AND IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_INTERACTION_MENU_UP, playerId) = FALSE
					OR IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_ARROW, playerId)
//					AND IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_JUST_APPEARED_ON_SCREEN, playerId) = FALSE
						
						#IF IS_DEBUG_BUILD
						IF g_TurnOnOverheadDebug
							NET_NL()NET_PRINT("[overhead] RUN_OHD_CREW_CHECK = FALSE for remote player  ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
							IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(playerID)
								NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))
							ENDIF
						ENDIF
						#ENDIF
					
						SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_CLAN, FALSE , playerID)
					ELSE
					
					
						
						#IF IS_DEBUG_BUILD
						IF g_TurnOnOverheadDebug
							IF IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_LOCAL_DPADDOWNACTIVE, playerId) = TRUE
								NET_NL()NET_PRINT("[overhead] RUN_OHD_CREW_CHECK = TRUE because dpaddown = TRUE for remote player  ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
									NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))
							ENDIF
						
							IF IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_ARE_IN_SAME_VOTE, playerId) = TRUE
								NET_NL()NET_PRINT("[overhead] RUN_OHD_CREW_CHECK = TRUE because in Same vote = TRUE for remote player  ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
									NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))
							ENDIF
						
							IF IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_ARE_IN_SAME_VEHICLE, playerId) = TRUE
								NET_NL()NET_PRINT("[overhead] RUN_OHD_CREW_CHECK = TRUE because in Same vehicle = TRUE for remote player  ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
									NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))
							ENDIF
						
							IF IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_AIMING_AT_PLAYER, playerId) = TRUE
								NET_NL()NET_PRINT("[overhead] RUN_OHD_CREW_CHECK = TRUE because in aiming at player = TRUE for remote player  ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
									NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))
							ENDIF
						
							IF IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_JUST_APPEARED_ON_SCREEN, playerId) = TRUE
								NET_NL()NET_PRINT("[overhead] RUN_OHD_CREW_CHECK = TRUE because player just appeared on screen = TRUE for remote player  ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
									NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))
							ENDIF
							
						ENDIF
						#ENDIF
						
						
						SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_CLAN, TRUE , playerID)

					ENDIF

				
				ENDIF

			ELSE
			
				#IF IS_DEBUG_BUILD
				IF g_TurnOnOverheadDebug
					NET_NL()NET_PRINT("[overhead] RUN_OHD_CREW_CHECK = FALSE because player has no crew, player  ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
					IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(playerID)
						NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))
					ENDIF
				ENDIF
				#ENDIF
			
				SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_CLAN, FALSE , playerID)
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
			IF g_TurnOnOverheadDebug
				NET_NL()NET_PRINT("[overhead] RUN_OHD_CREW_CHECK = FALSE because player name is off player  ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
				IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(playerID)
					NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))
				ENDIF
			ENDIF
			#ENDIF
			SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_CLAN, FALSE , playerID)
		ENDIF
	ENDIF

ENDPROC



PROC RUN_OHD_PAUSEMENU_CHECK(BOOL bProcessingLocalPlayer, BOOL bPassedPlayerVisibleToLocalPlayer, PLAYER_INDEX playerId)

	IF IS_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_NOTHING, playerId) 
	OR NOT bPassedPlayerVisibleToLocalPlayer 
	OR IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_ARROW, playerId)
	OR ( IS_ON_DEATHMATCH_GLOBAL_SET() AND (GlobalServerBD_DM.iTags = TAGS_OFF) )
		SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_PAUSED, FALSE, playerID )
	ELSE
		
		IF bProcessingLocalPlayer = FALSE
			//If the player is talking and we can see the player name then let us see if they are talking.
			IF IS_OVERHEAD_EVENT_ACTIVE(DISPLAYOVERHEADS_EVENTS_IS_PLAYER_PAUSING, playerId)
				SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_PAUSED,TRUE , playerID )
				#IF IS_DEBUG_BUILD
				IF g_TurnOnOverheadDebug
					NET_NL()NET_PRINT("[overhead] RUN_OHD_PAUSEMENU_CHECK = TRUE for player ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
						NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))
				ENDIF
				#ENDIF

			ELSE
				SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_PAUSED, FALSE, playerID )
				#IF IS_DEBUG_BUILD
				IF g_TurnOnOverheadDebug
					NET_NL()NET_PRINT("[overhead] RUN_OHD_PAUSEMENU_CHECK = FALSE for player ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
						NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))
				ENDIF
				#ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

PROC RUN_OHD_RESPAWNING_CHECK(BOOL bProcessingLocalPlayer, BOOL bPassedPlayerVisibleToLocalPlayer, PLAYER_INDEX playerId)

	IF IS_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_NOTHING, playerId) 
	OR NOT bPassedPlayerVisibleToLocalPlayer 
	OR IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_ARROW, playerId)
	OR ( IS_ON_DEATHMATCH_GLOBAL_SET() AND (GlobalServerBD_DM.iTags = TAGS_OFF) )
		SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_RESPAWNING, FALSE, playerID )
	ELSE
		
		IF bProcessingLocalPlayer = FALSE
			//If the player is talking and we can see the player name then let us see if they are talking.
			IF IS_OVERHEAD_EVENT_ACTIVE(DISPLAYOVERHEADS_EVENTS_IS_RESPAWNING, playerId)
				
				SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_RESPAWNING,TRUE , playerID )
				
				#IF IS_DEBUG_BUILD
				IF g_TurnOnOverheadDebug
					NET_NL()NET_PRINT("[overhead] RUN_OHD_RESPAWNING_CHECK = TRUE for player ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
					NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))
				ENDIF
				#ENDIF

			ELSE
				
				SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_RESPAWNING, FALSE, playerID )
				
				#IF IS_DEBUG_BUILD
				IF g_TurnOnOverheadDebug
					NET_NL()NET_PRINT("[overhead] RUN_OHD_RESPAWNING_CHECK = FALSE for player ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
					NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))
				ENDIF
				#ENDIF
				
			ENDIF
		ENDIF
	ENDIF

ENDPROC


PROC RUN_OHD_PASSIVE_CHECK(BOOL bProcessingLocalPlayer, BOOL bPassedPlayerVisibleToLocalPlayer, PLAYER_INDEX playerId, BOOL bPlayerInAnyVehicle)
	
	IF bProcessingLocalPlayer
	ENDIF
	
	IF IS_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_NOTHING, playerId) 
	OR NOT bPassedPlayerVisibleToLocalPlayer
	OR IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_ARROW, playerId)
	OR ( IS_ON_DEATHMATCH_GLOBAL_SET() AND (GlobalServerBD_DM.iTags = TAGS_OFF) )
	OR bPlayerInAnyVehicle
		
		SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_PASSIVE, FALSE, playerID )
		
	ELSE
		
		//If the player is talking and we can see the player name then let us see if they are talking.
		IF ( IS_OVERHEAD_EVENT_ACTIVE(DISPLAYOVERHEADS_EVENTS_IS_PLAYER_PASSIVE, playerId) OR IS_PLAYER_PASSIVE(playerId))
		AND NOT IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_IN_CORONA, playerId)
		AND NOT IS_OVERHEAD_EVENT_ACTIVE(DISPLAYOVERHEADS_EVENTS_IS_PLAYER_SPECTATING, PLAYER_ID())//Don't show any passive icons when spectating. 
		
			SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_PASSIVE,TRUE , playerID )
			
			#IF IS_DEBUG_BUILD
			IF g_TurnOnOverheadDebug
				IF IS_OVERHEAD_EVENT_ACTIVE(DISPLAYOVERHEADS_EVENTS_IS_PLAYER_PASSIVE, playerId)
					NET_NL()NET_PRINT("[overhead] RUN_OHD_PASSIVE_CHECK = TRUE IS_PLAYER_PASSIVE = TRUE for player ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
					NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))
				ENDIF
			ENDIF
			#ENDIF
			
			#IF IS_DEBUG_BUILD
			IF g_TurnOnOverheadDebug
				IF IS_OVERHEAD_EVENT_ACTIVE(DISPLAYOVERHEADS_EVENTS_IS_PLAYER_PAUSING, playerId)
					NET_NL()NET_PRINT("[overhead] RUN_OHD_PASSIVE_CHECK = TRUE IS_PLAYER_PAUSING = TRUE for player ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
					NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))
				ENDIF
			ENDIF
			#ENDIF

		ELSE
			
			SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_PASSIVE, FALSE, playerID )
			
			#IF IS_DEBUG_BUILD
			IF g_TurnOnOverheadDebug
				NET_NL()NET_PRINT("[overhead] RUN_OHD_PASSIVE_CHECK = FALSE for player ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
				NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))
				NET_NL()NET_PRINT("		-bProcessingLocalPlayer = ")NET_PRINT_BOOL(bProcessingLocalPlayer)
				NET_NL()NET_PRINT("		-DISPLAYOVERHEADS_EVENTS_IS_PLAYER_PASSIVE = ")NET_PRINT_BOOL(IS_OVERHEAD_EVENT_ACTIVE(DISPLAYOVERHEADS_EVENTS_IS_PLAYER_PASSIVE, playerId))
				NET_NL()NET_PRINT("		-DISPLAYOVERHEADS_EVENTS_IS_PLAYER_PAUSING = ")NET_PRINT_BOOL(IS_OVERHEAD_EVENT_ACTIVE(DISPLAYOVERHEADS_EVENTS_IS_PLAYER_PAUSING, playerId))
				NET_NL()NET_PRINT("		-DISPLAYOVERHEADS_EVENTS_IS_PLAYER_SPECTATING = ")NET_PRINT_BOOL(IS_OVERHEAD_EVENT_ACTIVE(DISPLAYOVERHEADS_EVENTS_IS_PLAYER_SPECTATING, playerId))
			ENDIF
			
			IF g_TurnOnOverheadDebug
				IF IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_IN_CORONA, playerId)
					NET_NL()NET_PRINT("[overhead] RUN_OHD_PASSIVE_CHECK = FALSE as DISPLAYOVERHEADS_LOGIC_IS_IN_CORONA = TRUE for player ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
					NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))
				ENDIF
			ENDIF
			#ENDIF
			
		ENDIF
		
	ENDIF

ENDPROC





PROC RUN_OHD_IS_TAGGED_CHECK(BOOL bProcessingLocalPlayer, BOOL bPassedPlayerVisibleToLocalPlayer, PLAYER_INDEX playerId)

	IF IS_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_NOTHING, playerId) 
	OR NOT bPassedPlayerVisibleToLocalPlayer
	OR ( IS_ON_DEATHMATCH_GLOBAL_SET() AND (GlobalServerBD_DM.iTags = TAGS_OFF) )
//	OR IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_ARROW, playerId)
		SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_TAGGED, FALSE, playerID )
	ELSE
		
		IF bProcessingLocalPlayer = FALSE
		
			//If the player is talking and we can see the player name then let us see if they are talking.
			IF IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_TAGGED, playerId)
			
				SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_TAGGED,TRUE , playerID )
				
				#IF IS_DEBUG_BUILD
				IF g_TurnOnOverheadDebug
					NET_NL()NET_PRINT("[overhead] RUN_OHD_IS_TAGGED_CHECK = TRUE for player ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
						NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))
				ENDIF
				#ENDIF

			ELSE
				SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_TAGGED, FALSE, playerID )
				#IF IS_DEBUG_BUILD
				IF g_TurnOnOverheadDebug
					NET_NL()NET_PRINT("[overhead] RUN_OHD_IS_TAGGED_CHECK = FALSE for player ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
						NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))
				ENDIF
				#ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC


PROC RUN_OHD_IS_RALLY_DRIVER_CHECK(BOOL bProcessingLocalPlayer, BOOL bPassedPlayerVisibleToLocalPlayer, PLAYER_INDEX playerId)
	
	// Make the compiler be quiet.
	IF bProcessingLocalPlayer
	ENDIF
	
	// Print.
	#IF IS_DEBUG_BUILD
	IF g_TurnOnOverheadDebug
	OR g_TurnOnRallyDriverOverheadDebug
		NET_NL()NET_PRINT("[overhead] RUN_OHD_IS_RALLY_DRIVER_CHECK being called for player ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
		NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))
	ENDIF
	#ENDIF
			
	// If the dispay nothing flag is set, or the player is not visible, turn off the icon.
	IF IS_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_NOTHING, playerId) 
	OR NOT bPassedPlayerVisibleToLocalPlayer
	OR IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_ARROW, playerId)	
	OR ( IS_ON_DEATHMATCH_GLOBAL_SET() AND (GlobalServerBD_DM.iTags = TAGS_OFF) )
		
		SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_RALLY_DRIVER, FALSE, playerID )
		
		// Print.
		#IF IS_DEBUG_BUILD
		IF g_TurnOnOverheadDebug
		OR g_TurnOnRallyDriverOverheadDebug
			NET_NL()NET_PRINT("[overhead] RUN_OHD_IS_RALLY_DRIVER_CHECK = FALSE, DISPLAYOVERHEADS_DISPLAY_NOTHING is active for player: ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
			NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))
		ENDIF
		#ENDIF
	
	// If the display nothing flag is not set and the player is visble.
	ELSE
		
		// If the player is a rally driver and the player is in the same vehicle as me.
		IF ( IS_OVERHEAD_EVENT_ACTIVE(DISPLAYOVERHEADS_EVENTS_IS_RALLY_DRIVER, playerId)
		AND IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_ARE_IN_SAME_VEHICLE, playerId)
		AND (MPGlobalsHud.bShowRallyIcons[1][NATIVE_TO_INT(playerID)] OR IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_LOCAL_DPADDOWNACTIVE, playerId)) )
//		OR MPGlobalsAmbience.bInInterMenuInpromptuRaceSubMenu
			
			// Show rally driver icon.
			SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_RALLY_DRIVER, TRUE, playerID)
			
			// Print.
			#IF IS_DEBUG_BUILD
			IF g_TurnOnOverheadDebug
			OR g_TurnOnRallyDriverOverheadDebug
				NET_NL()NET_PRINT("[overhead] RUN_OHD_IS_RALLY_DRIVER_CHECK = TRUE for player ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
				NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))
			ENDIF
			#ENDIF
			
		// If the player either is not a rally driver or not in the same vehicle as me, turn off the icon.
		ELSE
			
			// Do not show the rally driver icon.
			SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_RALLY_DRIVER, FALSE, playerID )
			
			// Print why we're in here.
			#IF IS_DEBUG_BUILD
			IF g_TurnOnOverheadDebug
			OR g_TurnOnRallyDriverOverheadDebug
				IF NOT IS_OVERHEAD_EVENT_ACTIVE(DISPLAYOVERHEADS_EVENTS_IS_RALLY_DRIVER, playerId)
					NET_NL()NET_PRINT("[overhead] RUN_OHD_IS_RALLY_DRIVER_CHECK = FALSE IS_RALLY_DRIVER = FALSE for player ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
					NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))
				ENDIF
				IF NOT IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_ARE_IN_SAME_VEHICLE, playerId) 
					NET_NL()NET_PRINT("[overhead] RUN_OHD_IS_RALLY_DRIVER_CHECK = FALSE ARE_IN_SAME_VEHICLE = FALSE for player ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
					NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))
				ENDIF	
				IF NOT MPGlobalsHud.bShowRallyIcons[1][NATIVE_TO_INT(playerID)]
					NET_NL()NET_PRINT("[overhead] RUN_OHD_IS_RALLY_DRIVER_CHECK = MPGlobalsHud.bShowRallyIcons[1][NATIVE_TO_INT(playerID)] = FALSE.")
				ENDIF
//				IF NOT MPGlobalsAmbience.bInInterMenuInpromptuRaceSubMenu
//					NET_NL()NET_PRINT("[overhead] RUN_OHD_IS_RALLY_DRIVER_CHECK = MPGlobalsAmbience.bInInterMenuInpromptuRaceSubMenu = FALSE.")
//				ENDIF
			ENDIF
			#ENDIF
			
		ENDIF
		
	ENDIF

ENDPROC

PROC RUN_OHD_IS_RALLY_PASSENGER_CHECK(BOOL bProcessingLocalPlayer, BOOL bPassedPlayerVisibleToLocalPlayer, PLAYER_INDEX playerId)
	
	// Stop compiler whinging.
	IF bProcessingLocalPlayer
	ENDIF

	// If the dispay nothing flag is set, or the player is not visible, turn off the icon.
	IF IS_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_NOTHING, playerId) 
	OR NOT bPassedPlayerVisibleToLocalPlayer
	OR IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_ARROW, playerId)
	OR ( IS_ON_DEATHMATCH_GLOBAL_SET() AND (GlobalServerBD_DM.iTags = TAGS_OFF) )
		
		SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_RALLY_PASSENGER, FALSE, playerID )
	
	// If the display nothing flag is not set and the player is visble.
	ELSE
		
		// If the player is a rally passenger and the player is in the same vehicle as me.
		IF IS_OVERHEAD_EVENT_ACTIVE(DISPLAYOVERHEADS_EVENTS_IS_RALLY_PASSENGER, playerId)
		AND IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_ARE_IN_SAME_VEHICLE, playerId)
		AND (MPGlobalsHud.bShowRallyIcons[1][NATIVE_TO_INT(playerID)] OR IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_LOCAL_DPADDOWNACTIVE, playerId))
		
			// Show the rally passenger icon.
			SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_RALLY_PASSENGER,TRUE, playerID )
			
			#IF IS_DEBUG_BUILD
			IF g_TurnOnOverheadDebug
				NET_NL()NET_PRINT("[overhead] RUN_OHD_IS_RALLY_PASSENGER_CHECK = TRUE for player ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
				NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))
			ENDIF
			#ENDIF
		
		// If the player either is not a rally passenger or not in the same vehicle as me, turn off the icon.
		ELSE
			
			// Do not show rally passenger icon.
			SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_RALLY_PASSENGER, FALSE, playerID )
			
			// Print why we're in here.
			#IF IS_DEBUG_BUILD
			IF g_TurnOnOverheadDebug
				IF NOT IS_OVERHEAD_EVENT_ACTIVE(DISPLAYOVERHEADS_EVENTS_IS_RALLY_PASSENGER, playerId)
					NET_NL()NET_PRINT("[overhead] RUN_OHD_IS_RALLY_PASSENGER_CHECK = FALSE IS_RALLY_PASSENGER = FALSE for player ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
					NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))
				ENDIF
				IF NOT IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_ARE_IN_SAME_VEHICLE, playerId)
					NET_NL()NET_PRINT("[overhead] RUN_OHD_IS_RALLY_PASSENGER_CHECK = FALSE ARE_IN_SAME_VEHICLE = FALSE for player ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
					NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))
				ENDIF
				IF NOT MPGlobalsHud.bShowRallyIcons[1][NATIVE_TO_INT(playerID)]
					NET_NL()NET_PRINT("[overhead] RUN_OHD_IS_RALLY_PASSENGER_CHECK = MPGlobalsHud.bShowRallyIcons[1][NATIVE_TO_INT(playerID)] = FALSE.")
				ENDIF
			ENDIF
			#ENDIF
			
		ENDIF
		
	ENDIF

ENDPROC



PROC RUN_OHD_IS_BETTING_CHECK(BOOL bProcessingLocalPlayer, BOOL bPassedPlayerVisibleToLocalPlayer, PLAYER_INDEX playerId)
	
	IF IS_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_NOTHING, playerId) 
	OR NOT bPassedPlayerVisibleToLocalPlayer
	OR IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_ARROW, playerId)
	OR ( IS_ON_DEATHMATCH_GLOBAL_SET() AND (GlobalServerBD_DM.iTags = TAGS_OFF) )
		SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_BETTING_ODDS, FALSE, playerID )
	ELSE
		
		IF bProcessingLocalPlayer = FALSE
		ENDIF
			
			//If the player is talking and we can see the player name then let us see if they are talking.
			
			
			IF IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_BETTING, playerID)
			AND NOT IS_OVERHEAD_EVENT_ACTIVE(DISPLAYOVERHEADS_EVENTS_IS_RALLY_PASSENGER, playerID)
			
				SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_BETTING_ODDS,TRUE , playerID )
				
				#IF IS_DEBUG_BUILD
				IF g_TurnOnOverheadDebug
					NET_NL()NET_PRINT("[overhead] RUN_OHD_IS_BETTING_CHECK = TRUE for player ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
						NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))
				ENDIF
				#ENDIF

			ELSE
			
				SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_BETTING_ODDS, FALSE, playerID )
				
				#IF IS_DEBUG_BUILD
				IF g_TurnOnOverheadDebug
					IF NOT IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_BETTING, playerID)
						NET_NL()NET_PRINT("[overhead] RUN_OHD_IS_BETTING_CHECK = FALSE for player ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
						NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))
					ENDIF
					IF IS_OVERHEAD_EVENT_ACTIVE(DISPLAYOVERHEADS_EVENTS_IS_RALLY_PASSENGER, playerID)
						NET_NL()NET_PRINT("[overhead] DISPLAYOVERHEADS_EVENTS_IS_RALLY_PASSENGER = TRUE for player ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
						NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))
					ENDIF
				ENDIF
				#ENDIF
				
			ENDIF
//		ENDIF
	ENDIF

ENDPROC


PROC RUN_OHD_IS_POWERPLAY_CHECK(BOOL bProcessingLocalPlayer, BOOL bPassedPlayerVisibleToLocalPlayer, PLAYER_INDEX playerId)

	IF IS_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_NOTHING, playerId) 
	OR NOT bPassedPlayerVisibleToLocalPlayer
	OR ( IS_ON_DEATHMATCH_GLOBAL_SET() AND (GlobalServerBD_DM.iTags = TAGS_OFF) )
		SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_POWERPLAY, FALSE, playerID )
	ELSE
		
		IF bProcessingLocalPlayer = FALSE
		ENDIF
			//If the player is talking and we can see the player name then let us see if they are talking.
			
			IF IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_POWERPLAY, playerID)
				SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_POWERPLAY,TRUE , playerID )
				#IF IS_DEBUG_BUILD
				IF g_TurnOnOverheadDebug
					NET_NL()NET_PRINT("[overhead] RUN_OHD_IS_POWERPLAY_CHECK = TRUE for player ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
						NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))
				ENDIF
				#ENDIF

			ELSE
				SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_POWERPLAY, FALSE, playerID )
				#IF IS_DEBUG_BUILD
				IF g_TurnOnOverheadDebug
					NET_NL()NET_PRINT("[overhead] RUN_OHD_IS_POWERPLAY_CHECK = FALSE for player ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
					NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))
				ENDIF
				#ENDIF
			ENDIF
//		ENDIF
	ENDIF

ENDPROC

PROC RUN_OHD_IS_CUSTOM_STRING_CHECK(BOOL bProcessingLocalPlayer, BOOL bPassedPlayerVisibleToLocalPlayer, PLAYER_INDEX playerId)

	IF IS_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_NOTHING, playerId) 
	OR NOT bPassedPlayerVisibleToLocalPlayer
	OR IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_ARROW, playerId)
	OR ( IS_ON_DEATHMATCH_GLOBAL_SET() AND (GlobalServerBD_DM.iTags = TAGS_OFF) )
		SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_CUSTOM_STRING, FALSE, playerID )
	ELSE
		
		IF bProcessingLocalPlayer = FALSE
		ENDIF
			//If the player is talking and we can see the player name then let us see if they are talking.
			IF IS_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_BETTING_ODDS, playerID) = FALSE
			AND IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_CUSTOM_TEXT, playerID)
				SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_CUSTOM_STRING,TRUE , playerID )
				#IF IS_DEBUG_BUILD
				IF g_TurnOnOverheadDebug
					NET_NL()NET_PRINT("[overhead] RUN_OHD_IS_CUSTOM_STRING_CHECK = TRUE for player ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
						NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))
				ENDIF
				#ENDIF

			ELSE
				SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_CUSTOM_STRING, FALSE, playerID )
				#IF IS_DEBUG_BUILD
				IF g_TurnOnOverheadDebug
					IF IS_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_BETTING_ODDS, playerID)
						NET_NL()NET_PRINT("[overhead] RUN_OHD_IS_CUSTOM_STRING_CHECK = FALSE because BETTING_ODDS = TRUE for player ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
						NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))
					ENDIF
					IF IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_CUSTOM_TEXT, playerID) = FALSE
						NET_NL()NET_PRINT("[overhead] RUN_OHD_IS_CUSTOM_STRING_CHECK = FALSE because IS_CUSTOM_TEXT = FALSE for player ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
						NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))
					ENDIF
				ENDIF
				#ENDIF
			ENDIF
//		ENDIF
	ENDIF

ENDPROC



/// PURPOSE:
///    Gets if the local player is aiming at playerId.
/// PARAMS:
///    playerId - player to check if local player is aiming at.
/// RETURNS:
///    TRUE if aiming at playerId, FALSE if not.
FUNC BOOL IS_PLAYER_AIMING_AT_PED(PED_INDEX pedId)
	
	ENTITY_INDEX entityId
	PED_INDEX vehePedId
	
	//
	IF IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedId)
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), pedId)
		RETURN TRUE
	ENDIF
	
	IF IS_ON_VEH_DEATHMATCH_GLOBAL_SET()
		IF GET_ENTITY_PLAYER_IS_FREE_AIMING_AT(PLAYER_ID(), entityId)
			IF IS_ENTITY_A_PED(entityId)
				vehePedId = GET_PED_INDEX_FROM_ENTITY_INDEX(entityId)
				IF DOES_ENTITY_EXIST(vehePedId)
					IF NOT IS_ENTITY_DEAD(vehePedId)
						IF IS_PED_IN_ANY_VEHICLE(vehePedId)
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL IS_THIS_PLAYER_AIMING_AT_PED(PLAYER_INDEX playerAiming, PED_INDEX pedTarget)
	
	ENTITY_INDEX entityId
	PED_INDEX vehePedId
	
	//
	IF IS_PLAYER_TARGETTING_ENTITY(playerAiming, pedTarget)
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_FREE_AIMING_AT_ENTITY(playerAiming, pedTarget)
		RETURN TRUE
	ENDIF
	
	IF IS_ON_VEH_DEATHMATCH_GLOBAL_SET()
		IF GET_ENTITY_PLAYER_IS_FREE_AIMING_AT(playerAiming, entityId)
			IF IS_ENTITY_A_PED(entityId)
				vehePedId = GET_PED_INDEX_FROM_ENTITY_INDEX(entityId)
				IF DOES_ENTITY_EXIST(vehePedId)
					IF NOT IS_ENTITY_DEAD(vehePedId)
						IF IS_PED_IN_ANY_VEHICLE(vehePedId)
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL IS_PLAYER_AIMING_AT_PED_IN_VEHICLE(PED_INDEX pedId, BOOL iscontrolPressed)
	
	IF iscontrolPressed
		IF IS_PED_IN_ANY_VEHICLE(pedId)
			IF IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), GET_VEHICLE_PED_IS_USING(pedId))
				RETURN TRUE
			ENDIF
			
			IF IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), GET_VEHICLE_PED_IS_USING(pedId))
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC


PROC RUN_OHD_SAME_VEHICLE_CHECK(PLAYER_INDEX PlayerId, PED_INDEX PlayerPedId )
	
	IF  IS_ON_RALLY_RACE_GLOBAL_SET()
		
		// If the player passed is not injured.
		IF NOT IS_PED_INJURED(PlayerPedId)
			
			// If the in any vehicle logic check is false.
			IF NOT IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IN_ANY_VEHICLE, playerId)
				
				// but the player is in a vehicle.
				IF IS_PED_IN_ANY_VEHICLE(PlayerPedId)
					
					// Print info.
					#IF IS_DEBUG_BUILD
						IF g_TurnOnOverheadDebug
						OR g_TurnOnSameVehCheckDebug
							NET_NL()NET_PRINT("[overhead] RUN_OHD_SAME_VEHICLE_CHECK: IS_PED_IN_ANY_VEHICLE(PlayerPedId) = TRUE FOR PLAYER ")NET_PRINT_INT(NATIVE_TO_INT(playerId))
							NET_NL()NET_PRINT("		-gamertag ")NET_PRINT(GET_PLAYER_NAME(playerId))
						ENDIF
					#ENDIF
					
					// Request visibility tracking.
					REQUEST_PED_VEHICLE_VISIBILITY_TRACKING(GET_PLAYER_PED(PlayerID), TRUE)
					
					// Set the in any vehicle logic to true.
					SET_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IN_ANY_VEHICLE, TRUE , playerId)
				
				// If the player is not in any vehicle.
				ELSE
					
					// Print info.
					#IF IS_DEBUG_BUILD
						IF g_TurnOnOverheadDebug
						OR g_TurnOnSameVehCheckDebug
							NET_NL()NET_PRINT("[overhead] RUN_OHD_SAME_VEHICLE_CHECK: IS_PED_IN_ANY_VEHICLE(PlayerPedId) = FALSE FOR PLAYER so Skip setting this up ")NET_PRINT_INT(NATIVE_TO_INT(playerId))
							NET_NL()NET_PRINT("		-gamertag ")NET_PRINT(GET_PLAYER_NAME(playerId))
						ENDIF
					#ENDIF
					
					// Set the in any vehicle logic to false.
					SET_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IN_ANY_VEHICLE, FALSE, playerId)
					
				ENDIF
				
			// If the in any vehicle logic is true.
			ELSE
				
				// If the player is not in a vehicle.
				IF NOT IS_PED_IN_ANY_VEHICLE(PlayerPedId)
				
					// Print info.
					#IF IS_DEBUG_BUILD
						IF g_TurnOnOverheadDebug
						OR g_TurnOnSameVehCheckDebug
							NET_NL()NET_PRINT("[overhead] RUN_OHD_SAME_VEHICLE_CHECK: IS_PED_IN_ANY_VEHICLE(PlayerPedId) = FALSE FOR PLAYER ")NET_PRINT_INT(NATIVE_TO_INT(playerId))
							NET_NL()NET_PRINT("		-gamertag ")NET_PRINT(GET_PLAYER_NAME(playerId))
							NET_NL()NET_PRINT("[overhead] IS_PED_IN_ANY_VEHICLE RUN_OHD_SAME_VEHICLE_CHECK(PlayerPedId) = FALSE FOR PLAYER ")NET_PRINT_INT(NATIVE_TO_INT(playerId))
							NET_NL()NET_PRINT("		-gamertag ")NET_PRINT(GET_PLAYER_NAME(playerId))
						ENDIF
					#ENDIF
					
					// Request visible checks.
					REQUEST_PED_VEHICLE_VISIBILITY_TRACKING(GET_PLAYER_PED(PlayerID), FALSE)
					
					// Turn off same vehicle and in any vehicle logic.
					SET_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IN_ANY_VEHICLE, FALSE , playerId)
					SET_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_ARE_IN_SAME_VEHICLE, FALSE , playerId)
				
				// If the player is in a vehicle.
				ELSE
					
					// Print info.
					#IF IS_DEBUG_BUILD
						IF g_TurnOnOverheadDebug
						OR g_TurnOnSameVehCheckDebug
							NET_NL()NET_PRINT("[overhead] RUN_OHD_SAME_VEHICLE_CHECK: IS_PED_IN_ANY_VEHICLE(PlayerPedId) = TRUE FOR PLAYER ")NET_PRINT_INT(NATIVE_TO_INT(playerId))
							NET_NL()NET_PRINT("		-gamertag ")NET_PRINT(GET_PLAYER_NAME(playerId))
							NET_NL()NET_PRINT("[overhead] IS_PED_IN_ANY_VEHICLE RUN_OHD_SAME_VEHICLE_CHECK(PlayerPedId) = TRUE FOR PLAYER ")NET_PRINT_INT(NATIVE_TO_INT(playerId))
							NET_NL()NET_PRINT("		-gamertag ")NET_PRINT(GET_PLAYER_NAME(playerId))
						ENDIF
					#ENDIF
					
					// Turn on the in any vehicle logic.
					SET_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IN_ANY_VEHICLE, TRUE, playerId)
					
				ENDIF
			ENDIF
		ENDIF
		
		// If the in any vehicle logic is active.
		IF IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IN_ANY_VEHICLE, playerId) = TRUE
			
			// If both players are ok.
			IF IS_NET_PLAYER_OK(playerId)
			AND IS_NET_PLAYER_OK(PLAYER_ID())
				
				// If the peds are in the same vehicle.
				IF ARE_PEDS_IN_SAME_VEHICLE(PlayerPedId, PLAYER_PED_ID())
					
					// If the in same vehicle logic is not active.
					IF NOT IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_ARE_IN_SAME_VEHICLE, playerId)
						
						// Print info.
						#IF IS_DEBUG_BUILD
						IF g_TurnOnOverheadDebug
						OR g_TurnOnSameVehCheckDebug
							NET_NL()NET_PRINT("[overhead] RUN_OHD_SAME_VEHICLE_CHECK: ARE_PEDS_IN_SAME_VEHICLE = TRUE FOR PLAYER ")NET_PRINT_INT(NATIVE_TO_INT(playerId))
							IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(playerID)
								NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))
							ENDIF
						ENDIF
						#ENDIF
						
						// Set the in same vehicle logic active.
						SET_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_ARE_IN_SAME_VEHICLE, TRUE, playerId)
					
					// If the in same vehicle logic is active.
					ELSE
						
						// Print info.
						#IF IS_DEBUG_BUILD
						IF g_TurnOnOverheadDebug
						OR g_TurnOnSameVehCheckDebug
							NET_NL()NET_PRINT("[overhead] RUN_OHD_SAME_VEHICLE_CHECK: DISPLAYOVERHEADS_LOGIC_ARE_IN_SAME_VEHICLE = TRUE FOR PLAYER ")NET_PRINT_INT(NATIVE_TO_INT(playerId))
							IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(playerID)
								NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))
							ENDIF
						ENDIF
						#ENDIF
						
					ENDIF
				
				// If the peds are not in the same vehicle.
				ELSE
				
					// Print info.
					#IF IS_DEBUG_BUILD
						IF g_TurnOnOverheadDebug
						OR g_TurnOnSameVehCheckDebug
							NET_NL()NET_PRINT("[overhead] RUN_OHD_SAME_VEHICLE_CHECK: ARE_PEDS_IN_SAME_VEHICLE(playerId) = FALSE FOR PLAYER ")NET_PRINT_INT(NATIVE_TO_INT(playerId))
							NET_NL()NET_PRINT("		-gamertag ")NET_PRINT(GET_PLAYER_NAME(playerId))
							NET_NL()NET_PRINT("[overhead] RUN_OHD_SAME_VEHICLE_CHECK: ARE_PEDS_IN_SAME_VEHICLE(PLAYER_ID()) = FALSE FOR PLAYER ")NET_PRINT_INT(NATIVE_TO_INT(PLAYER_ID()))
							NET_NL()NET_PRINT("		-gamertag ")NET_PRINT(GET_PLAYER_NAME(PLAYER_ID()))
						ENDIF
					#ENDIF
					
					// If the in same vehicle logic is active.
					IF IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_ARE_IN_SAME_VEHICLE, playerId)
					
						// Print info.
						#IF IS_DEBUG_BUILD
						IF g_TurnOnOverheadDebug
						OR g_TurnOnSameVehCheckDebug
							NET_NL()NET_PRINT("[overhead] RUN_OHD_SAME_VEHICLE_CHECK: ARE_PEDS_IN_SAME_VEHICLE = TRUE FOR PLAYER ")NET_PRINT_INT(NATIVE_TO_INT(playerId))
							IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(playerID)
								NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))
							ENDIF
						ENDIF
						#ENDIF
					
						// Set the in same vehicle logic not active.
						SET_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_ARE_IN_SAME_VEHICLE, FALSE, playerId)
						
					
					// If the in same vehicle logic is not active.
					ELSE
						
						// Print info.
						#IF IS_DEBUG_BUILD
						IF g_TurnOnOverheadDebug
						OR g_TurnOnSameVehCheckDebug
							NET_NL()NET_PRINT("[overhead] RUN_OHD_SAME_VEHICLE_CHECK: DISPLAYOVERHEADS_LOGIC_ARE_IN_SAME_VEHICLE = FALSE FOR PLAYER ")NET_PRINT_INT(NATIVE_TO_INT(playerId))
							IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(playerID)
								NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))
							ENDIF
						ENDIF
						#ENDIF
						
					ENDIF
				ENDIF
			
			// If one of the players is not ok.
			ELSE
					
				// If the in same vehicle logic is active.
				IF IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_ARE_IN_SAME_VEHICLE, playerId)
					
					// Print info.
					#IF IS_DEBUG_BUILD
					IF g_TurnOnOverheadDebug
					OR g_TurnOnSameVehCheckDebug
						NET_NL()NET_PRINT("[overhead] RUN_OHD_SAME_VEHICLE_CHECK: DISPLAYOVERHEADS_LOGIC_ARE_IN_SAME_VEHICLE = TRUE, player is not ok FOR PLAYER ")NET_PRINT_INT(NATIVE_TO_INT(playerId))
						IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(playerID)
							NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))
						ENDIF
					ENDIF
					#ENDIF
					
					// Turn off in same vehicle logic.
					SET_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_ARE_IN_SAME_VEHICLE, FALSE, playerId)
					
				// If the in same vehicle logic is not active.
				ELSE
				
					// Print info.
					#IF IS_DEBUG_BUILD
					IF g_TurnOnOverheadDebug
					OR g_TurnOnSameVehCheckDebug
						NET_NL()NET_PRINT("[overhead] RUN_OHD_SAME_VEHICLE_CHECK: DISPLAYOVERHEADS_LOGIC_ARE_IN_SAME_VEHICLE = FALSE, player is not ok FOR PLAYER ")NET_PRINT_INT(NATIVE_TO_INT(playerId))
						IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(playerID)
							NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))
						ENDIF
					ENDIF
					#ENDIF
				
				ENDIF
			ENDIF

		ENDIF
	
	ELSE
		
		// Turn off in same vehicle logic.
		SET_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_ARE_IN_SAME_VEHICLE, FALSE, playerId)
		
	ENDIF
	
ENDPROC



FUNC BOOL ARE_IN_DIFFERENT_TUTORIAL_SESSIONS(PLAYER_INDEX PlayerId)

	IF NETWORK_IS_IN_TUTORIAL_SESSION()
		IF NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), playerId) = FALSE
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC




FUNC BOOL SHOW_PLAYER_BETTING(PLAYER_INDEX PlayerId)
//
//	#IF IS_DEBUG_BUILD
//		NET_NL()NET_PRINT("SHOW_PLAYER_BETTING HAVE CHANGED for player ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
//		NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))
//		NET_NL()NET_PRINT("		-g_b_RaceCutDone = ")NET_PRINT_BOOL(g_b_RaceCutDone)
//		NET_NL()NET_PRINT("		-IS_ON_RACE_GLOBAL_SET() = ")NET_PRINT_BOOL(IS_ON_RACE_GLOBAL_SET())
//		NET_NL()NET_PRINT("		-IS_BETTING_AVAILABLE_TO_PLAYER(playerId) = ")NET_PRINT_BOOL(IS_BETTING_AVAILABLE_TO_PLAYER(playerId))
//
//	#ENDIF

//	IF IS_ON_RACE_GLOBAL_SET()
//		IF NOT IS_ON_DEATHMATCH_GLOBAL_SET()
//			IF IS_BETTING_AVAILABLE_TO_PLAYER(playerId)
//				IF NOT IS_PLAYER_BETTING_POOL_VOID(playerId)
//					RETURN TRUE
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF
	
	IF PlayerId = PlayerId
		PlayerId = PlayerId
	ENDIF
	
	RETURN FALSE

ENDFUNC

PROC RUN_OVERHEAD_FREEMODE_STAGGERED_LOGIC_CHECKS(PLAYER_INDEX PlayerId)

	INT iPlayerTeam						= GET_PLAYER_TEAM(playerID)
	INT  iPlayerNum					= NATIVE_TO_INT(playerID)
	PED_INDEX PlayerPedId 				= GET_PLAYER_PED(PlayerId)
	BOOL AreInSameTeam 					= (GET_PLAYER_TEAM(PLAYER_ID()) = iPlayerTeam)
	BOOL bInPlaneOrHeli
	
	// Save of the player is alive or not.
	IF DOES_ENTITY_EXIST(PlayerPedId)
		IF NOT IS_ENTITY_DEAD(PlayerPedId)
			IF IS_PED_IN_ANY_PLANE(PlayerPedId)
			OR IS_PED_IN_ANY_HELI(PlayerPedId)
				bInPlaneOrHeli = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	//Needs done every staggered frame
	IF IS_A_SPECTATOR_CAM_RUNNING()
	OR IS_PLAYER_SCTV(PLAYER_ID())
	OR bInPlaneOrHeli
		SET_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_CINEMATIC_CAMERA_RUNNING, FALSE, playerId)
	ELSE
		SET_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_CINEMATIC_CAMERA_RUNNING, IS_CINEMATIC_CAM_RENDERING(), playerId)
	ENDIF
	
	RUN_OHD_SAME_VEHICLE_CHECK(PlayerId, PlayerPedId)

	SET_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_ARE_IN_SAME_TEAM, AreInSameTeam, playerId)

	
	SET_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_ARE_IN_DIFFERENT_TUTORIAL_SESSION, ARE_IN_DIFFERENT_TUTORIAL_SESSIONS(playerId), playerId)
	
	//Can update when near a voting location
	BOOL Vote = IS_OVERHEAD_EVENT_ACTIVE(DISPLAYOVERHEADS_EVENTS_IS_VOTING, PlayerId) //IS_PLAYER_VOTING()
	IF Vote
		SET_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_ARE_IN_SAME_VOTE, ARE_PLAYERS_IN_SAME_VOTE(iPlayerNum), playerId)
	ELSE
		SET_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_ARE_IN_SAME_VOTE, FALSE, playerId)
	ENDIF
	
	
	//Can update when a mission starts and ends
	SET_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_ARE_IN_SAME_MISSION, ARE_PLAYERS_ON_SAME_MP_MISSION_INSTANCE(PLAYER_ID(), playerID), playerId)

	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_VEHICLE_OVERHEAD_HEALTH)
	AND NOT IS_ARENA_WAR_UGC_RACE() 
		SET_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_DISPLAY_HEALTHBAR, TRUE, PlayerId)
	ENDIF

	SET_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_BETTING, SHOW_PLAYER_BETTING(playerId), PlayerId)
	
//	SET_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_POWERPLAY, (g_i_PowerPlayer = iPlayerNum), PlayerId)
	
	SET_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_CUSTOM_TEXT, DOES_PLAYER_HAVE_OVERHEAD(playerId) , PlayerId)
	
	

ENDPROC

FUNC BOOL MAKE_OVERHEAD_STATS_LOGIC_ACTIVE(BOOL bPlayerVoted, BOOL bPlayerVisible)
	
	IF NOT bPlayerVoted
		RETURN FALSE
	ENDIF
	
	IF g_iDawnRaidPickupCarrierPart != -1
		IF NOT bPlayerVisible
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL HIDE_ALL_OVERHEADS_ON_REFRESH_CHECKS(PLAYER_INDEX playerID, BOOL bPassedPlayerVisibleToLocalPlayer)
	
	IF MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.bOnJobIntro[0]
		#IF IS_DEBUG_BUILD
			IF g_TurnOnOverheadDebug
				NET_PRINT("[WJK] - Overheads - HIDE_ALL_OVERHEADS = returning TRUE - bOnJobIntro = TRUE for local player:  ")NET_PRINT_INT(NATIVE_TO_INT(PLAYER_ID()))
				NET_PRINT(", Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(PLAYER_ID()))NET_NL()
			ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF NOT bPassedPlayerVisibleToLocalPlayer
		#IF IS_DEBUG_BUILD
			IF g_TurnOnOverheadDebug
				NET_PRINT("[WJK] - Overheads - HIDE_ALL_OVERHEADS = returning TRUE - bPassedPlayerVisibleToLocalPlayer = FALSE for player  ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
				NET_PRINT(", Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))NET_NL()
			ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	IF IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_CINEMATIC_CAMERA_RUNNING, PlayerID  )
		#IF IS_DEBUG_BUILD
			IF g_TurnOnOverheadDebug
				NET_PRINT("[WJK] - Overheads - HIDE_ALL_OVERHEADS = returning TRUE - DISPLAYOVERHEADS_LOGIC_IS_CINEMATIC_CAMERA_RUNNING active for player  ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
				NET_PRINT(", Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))NET_NL()
			ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	IF IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_ARE_IN_DIFFERENT_TUTORIAL_SESSION, PlayerID  ) 
		#IF IS_DEBUG_BUILD
			IF g_TurnOnOverheadDebug
				NET_PRINT("[WJK] - Overheads - HIDE_ALL_OVERHEADS = returning TRUE - DISPLAYOVERHEADS_LOGIC_ARE_IN_DIFFERENT_TUTORIAL_SESSION active for player  ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
				NET_PRINT(", Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))NET_NL()
			ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	IF (IS_OVERHEAD_EVENT_ACTIVE(DISPLAYOVERHEADS_EVENTS_IN_MP_CUTSCENE, PlayerID) AND SHOW_PLAYER_BETTING(PlayerID) = FALSE)
		#IF IS_DEBUG_BUILD
			IF g_TurnOnOverheadDebug
				NET_PRINT("[WJK] - Overheads - HIDE_ALL_OVERHEADS = returning TRUE - DISPLAYOVERHEADS_EVENTS_IN_MP_CUTSCENE active and SHOW_PLAYER_BETTING = FALSE for player  ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
				NET_PRINT(", Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))NET_NL()
			ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	IF IS_OVERHEAD_EVENT_ACTIVE(DISPLAYOVERHEADS_EVENTS_INVISIBLE, PlayerID)	
		#IF IS_DEBUG_BUILD
			IF g_TurnOnOverheadDebug
				NET_PRINT("[WJK] - Overheads - HIDE_ALL_OVERHEADS = returning TRUE - DISPLAYOVERHEADS_EVENTS_INVISIBLE active for player  ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
				NET_PRINT(", Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))NET_NL()
			ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	IF IS_OVERHEAD_EVENT_ACTIVE(DISPLAYOVERHEADS_EVENTS_TELEPORTING, PlayerID)	
		#IF IS_DEBUG_BUILD
			IF g_TurnOnOverheadDebug
				NET_PRINT("[WJK] - Overheads - HIDE_ALL_OVERHEADS = returning TRUE - DISPLAYOVERHEADS_EVENTS_TELEPORTING active for player  ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
				NET_PRINT(", Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))NET_NL()
			ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	IF IS_OVERHEAD_EVENT_ACTIVE(DISPLAYOVERHEADS_EVENTS_IS_PLAYER_SPECTATING, PlayerID)	
		#IF IS_DEBUG_BUILD
			IF g_TurnOnOverheadDebug
				NET_PRINT("[WJK] - Overheads - HIDE_ALL_OVERHEADS = returning TRUE - DISPLAYOVERHEADS_EVENTS_IS_PLAYER_SPECTATING active for player  ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
				NET_PRINT(", Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))NET_NL()
			ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	IF NOT IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_CAN_DISPLAY, PlayerID)	
		#IF IS_DEBUG_BUILD
			IF g_TurnOnOverheadDebug
				NET_PRINT("[WJK] - Overheads - HIDE_ALL_OVERHEADS = returning TRUE - DISPLAYOVERHEADS_LOGIC_CAN_DISPLAY active for player  ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
				NET_PRINT(", Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))NET_NL()
			ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	IF IS_POST_MISSION_CLEANUP_REQUIRED_OR_ACTIVE(PLAYER_ID())
		#IF IS_DEBUG_BUILD
			IF g_TurnOnOverheadDebug
				NET_PRINT("[WJK] - Overheads - HIDE_ALL_OVERHEADS = returning TRUE - IS_POST_MISSION_CLEANUP_REQUIRED_OR_ACTIVE = TRUE for local player:  ")NET_PRINT_INT(NATIVE_TO_INT(PLAYER_ID()))
				NET_PRINT(", Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(PLAYER_ID()))NET_NL()
			ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_BbNeedsFmCleanup)
		#IF IS_DEBUG_BUILD
			IF g_TurnOnOverheadDebug
				NET_PRINT("[WJK] - Overheads - HIDE_ALL_OVERHEADS = returning TRUE - IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_BbNeedsFmCleanup) = TRUE for local player:  ")NET_PRINT_INT(NATIVE_TO_INT(PLAYER_ID()))
				NET_PRINT(", Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(PLAYER_ID()))NET_NL()
			ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.bLeaderboardCamRenderingOrInterping
		#IF IS_DEBUG_BUILD
			IF g_TurnOnOverheadDebug
				NET_PRINT("[WJK] - Overheads - HIDE_ALL_OVERHEADS = returning TRUE - bLeaderboardCamRenderingOrInterping = TRUE for local player:  ")NET_PRINT_INT(NATIVE_TO_INT(PLAYER_ID()))
				NET_PRINT(", Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(PLAYER_ID()))NET_NL()
			ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	IF g_b_EndCameraOn
		#IF IS_DEBUG_BUILD
			IF g_TurnOnOverheadDebug
				NET_PRINT("[WJK] - Overheads - HIDE_ALL_OVERHEADS = returning TRUE - g_b_EndCameraOn = TRUE for local player:  ")NET_PRINT_INT(NATIVE_TO_INT(PLAYER_ID()))
				NET_PRINT(", Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(PLAYER_ID()))NET_NL()
			ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	IF g_b_TransitionActive
		#IF IS_DEBUG_BUILD
			IF g_TurnOnOverheadDebug
				NET_PRINT("[WJK] - Overheads - HIDE_ALL_OVERHEADS = returning TRUE - g_b_TransitionActive = TRUE for local player:  ")NET_PRINT_INT(NATIVE_TO_INT(PLAYER_ID()))
				NET_PRINT(", Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(PLAYER_ID()))NET_NL()
			ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	IF MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.bNotInSameInterior[NATIVE_TO_INT(playerID)]
		#IF IS_DEBUG_BUILD
			IF g_TurnOnOverheadDebug
				NET_PRINT("[WJK] - Overheads - HIDE_ALL_OVERHEADS = returning TRUE - bNotInSameInterior = TRUE for local player:  ")NET_PRINT_INT(NATIVE_TO_INT(PLAYER_ID()))
				NET_PRINT(", Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(PLAYER_ID()))NET_NL()
			ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	IF MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.bOnCelebrationScreen
		#IF IS_DEBUG_BUILD
			IF g_TurnOnOverheadDebug
				NET_PRINT("[WJK] - Overheads - HIDE_ALL_OVERHEADS = returning TRUE - bOnCelebrationScreen = TRUE for local player:  ")NET_PRINT_INT(NATIVE_TO_INT(PLAYER_ID()))
				NET_PRINT(", Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(PLAYER_ID()))NET_NL()
			ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	IF MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.bOnLeaderboard
		#IF IS_DEBUG_BUILD
			IF g_TurnOnOverheadDebug
				NET_PRINT("[WJK] - Overheads - HIDE_ALL_OVERHEADS = returning TRUE - bOnLeaderboard = TRUE for local player:  ")NET_PRINT_INT(NATIVE_TO_INT(PLAYER_ID()))
				NET_PRINT(", Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(PLAYER_ID()))NET_NL()
			ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	IF MPGlobalsHud_TitleUpdate.bPlayerSwitchActive
		#IF IS_DEBUG_BUILD
			IF g_TurnOnOverheadDebug
				NET_PRINT("[WJK] - Overheads - HIDE_ALL_OVERHEADS = returning TRUE - bPlayerSwitchActive = TRUE for local player:  ")NET_PRINT_INT(NATIVE_TO_INT(PLAYER_ID()))
				NET_PRINT(", Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(PLAYER_ID()))NET_NL()
			ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	IF g_bGamerTagBlocker
		#IF IS_DEBUG_BUILD
			IF g_TurnOnOverheadDebug
				NET_PRINT("[WJK] - Overheads - HIDE_ALL_OVERHEADS = returning TRUE - g_bGamerTagBlocker = TRUE for local player:  ")NET_PRINT_INT(NATIVE_TO_INT(PLAYER_ID()))
				NET_PRINT(", Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(PLAYER_ID()))NET_NL()
			ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	IF GET_HEIST_SUPPRESS_HUD_FOR_PLANNING()
		#IF IS_DEBUG_BUILD
			IF g_TurnOnOverheadDebug
				NET_PRINT("[WJK] - Overheads - HIDE_ALL_OVERHEADS = returning TRUE - bSuppressHudThisFrame = TRUE for local player:  ")NET_PRINT_INT(NATIVE_TO_INT(PLAYER_ID()))
				NET_PRINT(", Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(PLAYER_ID()))NET_NL()
			ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

/// PURPOSE:
///    Runs checks to decide if the overheads should be hidden. If one of the checks is hit, hide_all_overheads is set to true.
/// PARAMS:
///    playerID - player running checks against.
/// RETURNS:
///    TRUE if we should hide overheads for that player, FALSE if not.
FUNC BOOL HIDE_ALL_OVERHEADS_EVERY_FRAME_CHECKS(PLAYER_INDEX playerID, FLOAT fDisFromPlayer)
	
	// Stop compiler moaning.
	IF playerID = playerID
		playerID = playerID
	ENDIF
	
	IF fDisFromPlayer = fDisFromPlayer
		fDisFromPlayer = fDisFromPlayer
	ENDIF
	
	IF (GET_PACKED_STAT_INT(PACKED_MP_PLAYER_MARKER_DISPLAY) = MARKER_OPTIONS_OFF) // Option in the pause menu for different densities of overheads.
		#IF IS_DEBUG_BUILD
			IF g_TurnOnOverheadDebug
				NET_PRINT("[WJK] - Overheads - HIDE_ALL_OVERHEADS = returning TRUE - PACKED_MP_PLAYER_MARKER_DISPLAY = MARKER_OPTIONS_OFF for local player:  ")NET_PRINT_INT(NATIVE_TO_INT(PLAYER_ID()))
				NET_PRINT(", Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(PLAYER_ID()))NET_NL()
			ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	IF (MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.eOverheadOverrideState[0] = eOVERHEADOVERRIDESTATE_FORCE_OFF)
		NET_PRINT("[WJK] - Overheads - HIDE_ALL_OVERHEADS = returning TRUE - eOVERHEADOVERRIDESTATE_FORCE_OFF = TRUE for player:  ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
		NET_PRINT(", Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))NET_NL()
		RETURN TRUE
	ENDIF
	
	IF MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.bOnJobIntro[0]
		#IF IS_DEBUG_BUILD
			IF g_TurnOnOverheadDebug
				NET_PRINT("[WJK] - Overheads - HIDE_ALL_OVERHEADS = returning TRUE - bOnJobIntro = TRUE for local player:  ")NET_PRINT_INT(NATIVE_TO_INT(PLAYER_ID()))
				NET_PRINT(", Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(PLAYER_ID()))NET_NL()
			ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_REMOTE_BROWSING_IN_SHOP(CARMOD_SHOP_01_AP)
		#IF IS_DEBUG_BUILD
			IF g_TurnOnOverheadDebug
				NET_PRINT("[WJK] - Overheads - HIDE_ALL_OVERHEADS = returning TRUE - IS_PLAYER_REMOTE_BROWSING_IN_SHOP(CARMOD_SHOP_01_AP) for local player:  ")NET_PRINT_INT(NATIVE_TO_INT(PLAYER_ID()))
				NET_PRINT(", Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(PLAYER_ID()))NET_NL()
			ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_REMOTE_BROWSING_IN_SHOP(CARMOD_SHOP_05_ID2)
		#IF IS_DEBUG_BUILD
			IF g_TurnOnOverheadDebug
				NET_PRINT("[WJK] - Overheads - HIDE_ALL_OVERHEADS = returning TRUE - IS_PLAYER_REMOTE_BROWSING_IN_SHOP(CARMOD_SHOP_05_ID2) for local player:  ")NET_PRINT_INT(NATIVE_TO_INT(PLAYER_ID()))
				NET_PRINT(", Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(PLAYER_ID()))NET_NL()
			ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_REMOTE_BROWSING_IN_SHOP(CARMOD_SHOP_06_BT1)
		#IF IS_DEBUG_BUILD
			IF g_TurnOnOverheadDebug
				NET_PRINT("[WJK] - Overheads - HIDE_ALL_OVERHEADS = returning TRUE - IS_PLAYER_REMOTE_BROWSING_IN_SHOP(CARMOD_SHOP_06_BT1) for local player:  ")NET_PRINT_INT(NATIVE_TO_INT(PLAYER_ID()))
				NET_PRINT(", Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(PLAYER_ID()))NET_NL()
			ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_REMOTE_BROWSING_IN_SHOP(CARMOD_SHOP_07_CS1)
		#IF IS_DEBUG_BUILD
			IF g_TurnOnOverheadDebug
				NET_PRINT("[WJK] - Overheads - HIDE_ALL_OVERHEADS = returning TRUE - IS_PLAYER_REMOTE_BROWSING_IN_SHOP(CARMOD_SHOP_07_CS1) for local player:  ")NET_PRINT_INT(NATIVE_TO_INT(PLAYER_ID()))
				NET_PRINT(", Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(PLAYER_ID()))NET_NL()
			ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_REMOTE_BROWSING_IN_SHOP(CARMOD_SHOP_08_CS6)
		#IF IS_DEBUG_BUILD
			IF g_TurnOnOverheadDebug
				NET_PRINT("[WJK] - Overheads - HIDE_ALL_OVERHEADS = returning TRUE - IS_PLAYER_REMOTE_BROWSING_IN_SHOP(CARMOD_SHOP_08_CS6) for local player:  ")NET_PRINT_INT(NATIVE_TO_INT(PLAYER_ID()))
				NET_PRINT(", Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(PLAYER_ID()))NET_NL()
			ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_REMOTE_BROWSING_IN_SHOP(CARMOD_SHOP_SUPERMOD)
		#IF IS_DEBUG_BUILD
			IF g_TurnOnOverheadDebug
				NET_PRINT("[WJK] - Overheads - HIDE_ALL_OVERHEADS = returning TRUE - IS_PLAYER_REMOTE_BROWSING_IN_SHOP(CARMOD_SHOP_SUPERMOD) for local player:  ")NET_PRINT_INT(NATIVE_TO_INT(PLAYER_ID()))
				NET_PRINT(", Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(PLAYER_ID()))NET_NL()
			ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF

//	
//	IF (GET_PACKED_STAT_INT(PACKED_MP_PLAYER_MARKER_DISPLAY) = MARKER_OPTIONS_ARROW_ONLY) // Option in the pause menu for different densities of overheads.
//		#IF IS_DEBUG_BUILD
//			IF g_TurnOnOverheadDebug
//				NET_PRINT("[WJK] - Overheads - HIDE_ALL_OVERHEADS = returning TRUE - PACKED_MP_PLAYER_MARKER_DISPLAY = MARKER_OPTIONS_ARROW_ONLY for local player:  ")NET_PRINT_INT(NATIVE_TO_INT(PLAYER_ID()))
//				NET_PRINT(", Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(PLAYER_ID()))NET_NL()
//			ENDIF
//		#ENDIF
//		RETURN TRUE
//	ENDIF
//	
//	IF (GET_PACKED_STAT_INT(PACKED_MP_PLAYER_MARKER_DISPLAY) = MARKER_OPTIONS_FULL)
//	OR (GET_PACKED_STAT_INT(PACKED_MP_PLAYER_MARKER_DISPLAY) = MARKER_OPTIONS_FULL_WITH_CREWTAG)
//		IF fDisFromPlayer >= GET_OVERHEAD_ARROW_MARKER_DISTANCE()*GET_OVERHEAD_ARROW_MARKER_DISTANCE()
//			#IF IS_DEBUG_BUILD
//				IF g_TurnOnOverheadDebug
//					NET_PRINT("[WJK] - Overheads - HIDE_ALL_OVERHEADS = returning TRUE - distance between players <= 100 and PACKED_MP_PLAYER_MARKER_DISPLAY = MARKER_OPTIONS_FULL OR MARKER_OPTIONS_FULL_WITH_CREWTAG for local player:  ")NET_PRINT_INT(NATIVE_TO_INT(PLAYER_ID()))
//					NET_PRINT(", Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(PLAYER_ID()))NET_NL()
//				ENDIF
//			#ENDIF
//			RETURN TRUE
//		ENDIF
//	ENDIF
	
	IF IS_PLAYER_IN_CORONA()
		//NET_PRINT("[WJK] - Overheads - HIDE_ALL_OVERHEADS = returning TRUE - IS_PLAYER_IN_CORONA = TRUE for local player.")
		//NET_PRINT(", Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))NET_NL()
		RETURN TRUE
	ENDIF
	
	IF NOT IS_SKYSWOOP_AT_GROUND() 
		//NET_PRINT("[WJK] - Overheads - HIDE_ALL_OVERHEADS = returning TRUE - IS_SKYSWOOP_AT_GROUND = FALSE for local player.")
		//NET_PRINT(", Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))NET_NL()
		RETURN TRUE
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF g_bTogglePlayerOverhead[0][NATIVE_TO_INT(playerID)]
		#IF IS_DEBUG_BUILD
			IF g_TurnOnOverheadDebug
				NET_PRINT("[WJK] - Overheads - HIDE_ALL_OVERHEADS = returning TRUE - g_bTogglePlayerOverhead[0] = TRUE for player:  ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
				NET_PRINT(", Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))NET_NL()
			ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	#ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC RUN_OVERHEAD_HIDE_ALL_CHECKS(INT iPlayerNum, PLAYER_INDEX PlayerID, BOOL bPassedPlayerVisibleToLocalPlayer, FLOAT fDisFromPlayer, BOOL bEveryFrame)
	
	BOOL bHideAll
	
	IF bEveryFrame
		IF HIDE_ALL_OVERHEADS_EVERY_FRAME_CHECKS(playerID, fDisFromPlayer)
			bHideAll = TRUE
		ENDIF
	ELSE
		IF HIDE_ALL_OVERHEADS_ON_REFRESH_CHECKS(PlayerID, bPassedPlayerVisibleToLocalPlayer)
			bHideAll = TRUE
		ENDIF
	ENDIF
	
	// If any of these checks are true, we want to hide the display. Not turn it off but just make it invisible.
	IF bHideAll
		// Logic still processes, drawing does not (so process the logic for the icons and if they should display or not but then don;t allow them to draw).
		HIDE_ABOVE_HEAD_DISPLAY(iPlayerNum, PlayerID)
	ELSE
		// Allow icons to draw.
		SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_NOTHING, FALSE, PlayerID)
	ENDIF
	
ENDPROC

PROC RUN_OVERHEAD_FREEMODE_INIT_CHECKS(PLAYER_INDEX PlayerId)
	
	BOOL bPassedPlayerVisibleToLocalPlayer
	
	IF IS_NET_PLAYER_OK(PlayerId, FALSE)
	
		PED_INDEX PlayerPedId 				= GET_PLAYER_PED(PlayerId)
		BOOL bProcessingLocalPlayer 		= (PlayerID = PLAYER_ID())
		INT PlayerGbd 						= NATIVE_TO_INT(playerID)
		INT iPlayerWantedLevel				= GET_PLAYER_WANTED_LEVEL(PlayerID)
		INT iPlayerFakeWantedLevel			= GET_PLAYER_FAKE_WANTED_LEVEL(PlayerID)
//		BOOL AreInSameTeam 					= (GET_PLAYER_TEAM(PLAYER_ID()) = GET_PLAYER_TEAM(PlayerId))
		FLOAT fDisBetweenPlayers			= GET_DISTANCE_BETWEEN_PLAYERS(PLAYER_ID(), playerId)
		INT iPixelHeight							= -1
		BOOL bInAnyVehicle					= IS_PED_IN_ANY_VEHICLE(PlayerPedId)
		
		IF IS_PLAYER_SCTV(PLAYER_ID())
			fDisBetweenPlayers = VDIST(GET_PLAYER_COORDS(playerId), GET_FINAL_RENDERED_CAM_COORD())
		ENDIF
		
		IF IS_PED_IN_ANY_HELI(GET_PLAYER_PED(PLAYER_ID()))
			iPixelHeight = 10
		ENDIF
		

		
		
		bPassedPlayerVisibleToLocalPlayer = CAN_I_SEE_PLAYER(playerID, iPixelHeight)
		IF IS_BIT_SET(MPGlobalsAmbience.iAmbBitSet, iABI_HEIST_SPECTATE_WHILE_DEAD_ENABLED)
   		AND IS_THIS_ROCKSTAR_MISSION_NEW_VS_RESURRECTION(g_FMMC_STRUCT.iAdversaryModeType)
		AND IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iSpecInfoBitset, SPEC_INFO_BS_USING_HEIST_SPECTATE) 
			bPassedPlayerVisibleToLocalPlayer = TRUE
		ENDIF
		
		IF g_iDawnRaidPickupCarrierPart != -1 AND  IS_THIS_ROCKSTAR_MISSION_NEW_VS_DAWN_RAID(g_FMMC_STRUCT.iAdversaryModeType)
			bPassedPlayerVisibleToLocalPlayer = TRUE
		ENDIF
		RUN_OVERHEAD_FREEMODE_STAGGERED_LOGIC_CHECKS(PlayerId)
		
		NET_NL()NET_PRINT("RUN_OVERHEAD_FREEMODE_INIT_CHECKS - Player Int = ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
		NET_NL()NET_PRINT(" 		-Player Gbd = ")NET_PRINT_INT(PlayerGbd)
		NET_NL()NET_PRINT(" 		- bProcessingLocalPlayer = ")NET_PRINT_BOOL(bProcessingLocalPlayer)
		
		
		// RESPAWNING.
		RUN_OHD_RESPAWNING_CHECK(bProcessingLocalPlayer, bPassedPlayerVisibleToLocalPlayer, playerID)
		
		//PAUSE MENU MARKER
		RUN_OHD_PAUSEMENU_CHECK(bProcessingLocalPlayer, bPassedPlayerVisibleToLocalPlayer, playerID)
		
		//PLAYER NAME CNC
		RUN_OHD_PLAYER_NAME_CHECK_FM(bProcessingLocalPlayer, bPassedPlayerVisibleToLocalPlayer, playerID )
		
		// OVERHEAD ARROW
		RUN_OHD_PLAYER_ARROW_CHECK_FM(bProcessingLocalPlayer, bPassedPlayerVisibleToLocalPlayer, playerID)
		
		//WANTED LEVEL on/off
		RUN_OHD_WANTED_LEVEL_CHECK_FM(bProcessingLocalPlayer, bPassedPlayerVisibleToLocalPlayer, playerID, iPlayerWantedLevel, iPlayerFakeWantedLevel)
		
		//TALKING
		RUN_OHD_TALKING_CHECK(bProcessingLocalPlayer, bPassedPlayerVisibleToLocalPlayer, playerID)
		
		//HealthBar
		RUN_OHD_HEALTHBAR_CHECK(bPassedPlayerVisibleToLocalPlayer, playerID)
		
		//HAS VOTED
//		RUN_OHD_VOTING_CHECK(bProcessingLocalPlayer, playerID)
		
		//INVENTORY
		RUN_OHD_INVENTORY_CHECK(bProcessingLocalPlayer, PlayerGbd, playerID)
		
		//PASSIVE MARKER
		RUN_OHD_PASSIVE_CHECK(bProcessingLocalPlayer, bPassedPlayerVisibleToLocalPlayer, playerID, bInAnyVehicle)
		
		//TAGGED MARKER
		RUN_OHD_IS_TAGGED_CHECK(bProcessingLocalPlayer, bPassedPlayerVisibleToLocalPlayer, playerID)
		
		//RALLY DRIVER MARKER
		RUN_OHD_IS_RALLY_DRIVER_CHECK(bProcessingLocalPlayer, bPassedPlayerVisibleToLocalPlayer, playerID)
		
		//RALLY PASSENGER MARKER
		RUN_OHD_IS_RALLY_PASSENGER_CHECK(bProcessingLocalPlayer, bPassedPlayerVisibleToLocalPlayer, playerID)
		
		RUN_OHD_IS_BETTING_CHECK(bProcessingLocalPlayer, bPassedPlayerVisibleToLocalPlayer, playerID)
		
//		RUN_OHD_IS_POWERPLAY_CHECK(bProcessingLocalPlayer, bPassedPlayerVisibleToLocalPlayer, playerID)
		
		RUN_OHD_IS_CUSTOM_STRING_CHECK(bProcessingLocalPlayer, bPassedPlayerVisibleToLocalPlayer, playerID)
		
		// Chat window.
		RUN_OHD_CHAT_WINDOW_CHECK(bProcessingLocalPlayer, bPassedPlayerVisibleToLocalPlayer, playerID)
		
		//Can display external check
		SET_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_CAN_DISPLAY, MAKE_OVERHEAD_STATS_LOGIC_ACTIVE(CAN_DISPLAY_OVERHEAD_STATS(PlayerID), bPassedPlayerVisibleToLocalPlayer), playerId)
		
		RUN_OVERHEAD_HIDE_ALL_CHECKS(PlayerGbd, playerID, bPassedPlayerVisibleToLocalPlayer, fDisBetweenPlayers, TRUE)
		RUN_OVERHEAD_HIDE_ALL_CHECKS(PlayerGbd, playerID, bPassedPlayerVisibleToLocalPlayer, fDisBetweenPlayers, FALSE)
		RUN_OHD_GANG_ICONS_FM(bProcessingLocalPlayer, bPassedPlayerVisibleToLocalPlayer, playerID)
		RUN_OHD_PACKAGE_TRANSMITTORS(bProcessingLocalPlayer, bPassedPlayerVisibleToLocalPlayer, playerID)
		
//		RUN_ARROW_MARKER_CHECK(bProcessingLocalPlayer, bPassedPlayerVisibleToLocalPlayer, playerID, fDisBetweenPlayers)
		
	ENDIF
	

ENDPROC

PROC RUN_OVERHEAD_FREEMODE_INIT_CHECKS_ALLPLAYERS()
	INT I
	FOR I = 0 TO NUM_NETWORK_PLAYERS-1
		RUN_OVERHEAD_FREEMODE_INIT_CHECKS(INT_TO_NATIVE(PLAYER_INDEX, I))
	ENDFOR
	
ENDPROC



PROC INITIALISE_OVERHEADS()
	NET_NL()NET_PRINT(" INITIALISE_OVERHEADS - Called ")
	RESET_ALL_OVERHEAD_BITSETS()
	INITIALISE_PLAYERS_OHD_EVENT_STATE_ALL_PLAYERS()
	RUN_OVERHEAD_FREEMODE_INIT_CHECKS_ALLPLAYERS()
ENDPROC

FUNC BOOL MAKE_APPEAR_ON_SCREEN_LOGIC_ACTIVE(BOOL bPassedPlayerVisibleToLocalPlayer, BOOL bProcessingLocalPlayer, PLAYER_INDEX PlayerId)
	
	IF NOT bProcessingLocalPlayer
		IF bPassedPlayerVisibleToLocalPlayer
		AND IS_NET_PLAYER_BLIPPED(PlayerId)
		AND NOT IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_ARE_IN_SAME_VEHICLE, playerId)
			IF NOT HAS_NET_TIMER_STARTED(MPGlobalsHud.timerBeenVisibleForTime[NATIVE_TO_INT(PlayerId)])
				START_NET_TIMER(MPGlobalsHud.timerBeenVisibleForTime[NATIVE_TO_INT(PlayerId)])
				NET_PRINT("[WJK] - overheads - MAKE_APPEAR_ON_SCREEN_LOGIC_ACTIVE - player ")NET_PRINT(GET_PLAYER_NAME(PlayerId))NET_PRINT(" just appeared on screen.")NET_NL()
			ELSE
				IF NOT HAS_NET_TIMER_EXPIRED(MPGlobalsHud.timerBeenVisibleForTime[NATIVE_TO_INT(PlayerId)], DPAD_DOWN_GAMERTAGS_DISPLAY_TIME)
					RETURN TRUE
				ENDIF
			ENDIF
		ELSE
			RESET_NET_TIMER(MPGlobalsHud.timerBeenVisibleForTime[NATIVE_TO_INT(PlayerId)])
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL MAKE_DPAD_DOWN_LOGIC_ACTIVE(BOOL bDPadDownActive, BOOL bPlayerVisible)
	
	IF NOT bDPadDownActive
		RETURN FALSE
	ENDIF
	
	IF MPGlobalsHud_TitleUpdate.bGamertagsDpadDownState = DPADDOWN_NONE
		RETURN FALSE
	ENDIF
	
	IF SCALEFORMXML_RANKBAR_RUNNING_ICONS() = FALSE
		RETURN FALSE
	ENDIF
	
	IF NOT bPlayerVisible
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL MAKE_INTERACTION_MENU_TARGETTING_LOGIC_ACTIVE(BOOL bPlayerBeingTargetted, BOOL bPlayerVisible)
	
	IF NOT bPlayerBeingTargetted
		RETURN FALSE
	ENDIF
	
	IF NOT bPlayerVisible
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL MAKE_AIMING_LOGIC_ACTIVE(BOOL bPlayerBeingAimedAt, BOOL bPlayerVisible)
	
	IF NOT bPlayerBeingAimedAt
		RETURN FALSE
	ENDIF
	
	IF NOT bPlayerVisible
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL MAKE_CHAT_WINDOW_LOGIC_ACTIVE(BOOL bUsingChatWindow, BOOL bPlayerVisible)
	
	IF NOT bUsingChatWindow
		RETURN FALSE
	ENDIF
	
	IF NOT bPlayerVisible
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL MAKE_WANTED_STARS_LOGIC_ACTIVE(BOOL bPlayerWanted, BOOL bPlayerVisible)
	
	IF NOT bPlayerWanted
		RETURN FALSE
	ENDIF
	
	IF NOT bPlayerVisible
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL MAKE_TALKING_LOGIC_ACTIVE(BOOL bTalking, BOOL bPlayerVisible)
	
	IF NOT bTalking
		RETURN FALSE
	ENDIF
	
	IF NOT bPlayerVisible
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL MAKE_TAGGED_LOGIC_ACTIVE(BOOL bTagged, BOOL bPlayerVisible)
	
	IF NOT bTagged
		RETURN FALSE
	ENDIF
	
	IF NOT bPlayerVisible
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL MAKE_IN_CORONA_LOGIC_ACTIVE(BOOL bInCorona, BOOL bPlayerVisible)
	
	IF NOT bInCorona
		RETURN FALSE
	ENDIF
	
	IF NOT bPlayerVisible
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL SHOULD_TAG_BE_SHOWN_FOR_TUTORIAL_FLASHING(eMP_TAG eTag, PLAYER_INDEX playerId)
	
	SWITCH eTag
		CASE MP_TAG_GAMER_NAME
			RETURN TRUE
		CASE MP_TAG_GAMER_NAME_NEARBY
			RETURN FALSE
		CASE MP_TAG_CREW_TAG
			RETURN TRUE
		CASE MP_TAG_HEALTH_BAR
			RETURN TRUE
		CASE MP_TAG_BIG_TEXT
			RETURN FALSE
		CASE MP_TAG_ARROW
			RETURN FALSE
		CASE MP_TAG_AUDIO
			RETURN TRUE
		CASE MP_TAG_PASSIVE_MODE
			RETURN IS_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_PASSIVE, playerId)
		CASE MP_TAG_USING_MENU
			RETURN FALSE
		CASE MP_TAG_TAGGED
			RETURN IS_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_TAGGED, playerId)
		CASE MP_TAG_DRIVER
			RETURN IS_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_RALLY_DRIVER, playerId)
		CASE MP_TAG_CO_DRIVER
			RETURN IS_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_RALLY_PASSENGER, playerId)
		CASE MP_TAG_IF_PED_FOLLOWING
			RETURN FALSE
		CASE MP_TAG_PACKAGES
			RETURN FALSE
		CASE MP_TAG_WANTED_LEVEL
			RETURN IS_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_WANTEDLEVEL, playerId)
		CASE MP_TAG_PACKAGE_LARGE
			RETURN FALSE
		CASE MP_TAG_TRACKIFY
			RETURN FALSE
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC

PROC FLASH_GAMERTAGS()
	
	INT iPlayerNum
	INT iTagCount
	PLAYER_INDEX playerId
	
	IF HAS_NET_TIMER_EXPIRED_ONE_FRAME(MPGlobalsHud_TitleUpdate.stFlashTimer, 500)
		REPEAT NUM_NETWORK_PLAYERS iPlayerNum
			playerId = INT_TO_NATIVE(PLAYER_INDEX, iPlayerNum)
			IF playerId != PLAYER_ID()
				IF IS_NET_PLAYER_OK(playerId)
					IF IS_MP_GAMER_TAG_ACTIVE(iPlayerNum)
						REPEAT COUNT_OF(eMP_TAG) iTagCount
							IF SHOULD_TAG_BE_SHOWN_FOR_TUTORIAL_FLASHING(INT_TO_ENUM(eMP_TAG, iTagCount), playerId)
								SET_ICON_VISABILITY(iPlayerNum, INT_TO_ENUM(eMP_TAG, iTagCount), (NOT MPGlobalsHud_TitleUpdate.bOn))
							ENDIF
						ENDREPEAT
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		MPGlobalsHud_TitleUpdate.bOn = (NOT MPGlobalsHud_TitleUpdate.bOn)	
	ENDIF
	
ENDPROC

PROC RESET_FLASH_GAMERTAGS_DATA()
	
	RESET_NET_TIMER(MPGlobalsHud_TitleUpdate.stFlashTimer)
	MPGlobalsHud_TitleUpdate.bOn = FALSE
	
ENDPROC

PROC MAINTAIN_OVERHEAD_ALPHA(INT iPlayerNum, PLAYER_INDEX playerId)
	
	INT iTagCount, iAlpha, iSpecificAlpha
	INT iSpecificTag = -1
	
	PED_INDEX pedId = GET_PLAYER_PED(playerId)
	BOOL bSittingInAnyVehicleLocalPlayer = IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
	BOOL bSittingInAnyVehicleRemotePlayer = IS_PED_SITTING_IN_ANY_VEHICLE(pedId)
	
	VEHICLE_INDEX localPlayerVehicle
	VEHICLE_INDEX remotePlayerVehicle 

	IF bSittingInAnyVehicleLocalPlayer
		IF bSittingInAnyVehicleRemotePlayer
			localPlayerVehicle = GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())
			remotePlayerVehicle = GET_VEHICLE_PED_IS_USING(pedId)
		ENDIF
	ENDIF
	
	IF IS_NET_PLAYER_OK(playerId)
		iAlpha = GET_ENTITY_ALPHA(GET_PLAYER_PED(playerId))
		
		IF bSittingInAnyVehicleLocalPlayer
		AND IS_PLAYER_USING_ARENA()
		AND NOT IS_ARENA_WAR_UGC_RACE()
			iAlpha = 255
		ENDIF
		#IF IS_DEBUG_BUILD
		IF g_TurnOnOverheadDebug
			PRINTLN("[Overheads] - IS_NET_PLAYER_OK(", iPlayerNum, ") = FALSE. Setting alpha = ", iAlpha)
		ENDIF
		#ENDIF
	ENDIF
	
	IF NOT IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_AIMING_AT_PLAYER, playerId)
		IF iAlpha > 179
		AND NOT IS_PLAYER_USING_ARENA()
			iAlpha = 179
			#IF IS_DEBUG_BUILD
			IF g_TurnOnOverheadDebug
				PRINTLN("[Overheads] - I am not aiming at player ", iPlayerNum, ". Setting alpha = ", iAlpha)
			ENDIF
			#ENDIF
		ENDIF
	ENDIF
	
	IF g_bBrowserVisible
		iAlpha = 0
		#IF IS_DEBUG_BUILD
		IF g_TurnOnOverheadDebug
			PRINTLN("[Overheads] - g_bBrowserVisible = TRUE. Setting alpha = ", iAlpha)
		ENDIF
		#ENDIF
	ENDIF
	
	IF IS_CUTSCENE_PLAYING()
		iAlpha = 0
		#IF IS_DEBUG_BUILD
		IF g_TurnOnOverheadDebug
			PRINTLN("[Overheads] - IS_CUTSCENE_PLAYING = TRUE. Setting alpha = ", iAlpha)
		ENDIF
		#ENDIF
	ENDIF

	IF g_bCelebrationScreenIsActive
		iAlpha = 0
		#IF IS_DEBUG_BUILD
		IF g_TurnOnOverheadDebug
			PRINTLN("[Overheads] - g_bCelebrationScreenIsActive = TRUE. Setting alpha = ", iAlpha)
		ENDIF
		#ENDIF
	ENDIF
	
	IF g_bGamerTagBlocker
		iAlpha = 0
		#IF IS_DEBUG_BUILD
		IF g_TurnOnOverheadDebug
			PRINTLN("[Overheads] - g_bGamerTagBlocker = TRUE. Setting alpha = ", iAlpha)
		ENDIF
		#ENDIF
	ENDIF
	
	IF IS_POST_MISSION_SCENE_ACTIVE()
		iAlpha = 0
		#IF IS_DEBUG_BUILD
		IF g_TurnOnOverheadDebug
			PRINTLN("[Overheads] - IS_POST_MISSION_SCENE_ACTIVE = TRUE. Setting alpha = ", iAlpha)
		ENDIF
		#ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT.bHideBlips
		iAlpha = 0
		#IF IS_DEBUG_BUILD
		IF g_TurnOnOverheadDebug
			PRINTLN("[Overheads] - g_FMMC_STRUCT.bHideBlips = TRUE. Setting alpha = ", iAlpha)
		ENDIF
		#ENDIF
	ENDIF
	
//	IF IS_PLAYER_BLIP_HIDDEN(playerId)
//		iAlpha = 0
//		#IF IS_DEBUG_BUILD
//		IF g_TurnOnOverheadDebug
//			PRINTLN("[Overheads] - IS_PLAYER_BLIP_HIDDEN = TRUE. Setting alpha = ", iAlpha)
//		ENDIF
//		#ENDIF
//	ENDIF
//	
//	IF NOT IS_MP_PLAYER_BLIPPED(playerId)
//		iAlpha = 0
//		#IF IS_DEBUG_BUILD
//		IF g_TurnOnOverheadDebug
//			PRINTLN("[Overheads] - IS_MP_PLAYER_BLIPPED = FALSE. Setting alpha = ", iAlpha)
//		ENDIF
//		#ENDIF
//	ENDIF

	
	
	IF SHOULD_BLOCK_PLAYER_BLIP(playerId)
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciSHOW_ONLY_FRIENDLY_GAMER_TAGS)
		AND NOT IS_LOCAL_PLAYER_IN_ARENA_SPECTATOR_BOX() 
		AND NOT IS_PLAYER_ON_RC_RACE(playerID, TRUE)		
			iAlpha = 0
			#IF IS_DEBUG_BUILD
			IF g_TurnOnOverheadDebug
				PRINTLN("[Overheads] - SHOULD_BLOCK_PLAYER_BLIP = TRUE. Setting alpha = ", iAlpha)
			ENDIF
			#ENDIF
		#IF IS_DEBUG_BUILD
		ELSE
			IF g_TurnOnOverheadDebug
				PRINTLN("[Overheads] - SHOULD_BLOCK_PLAYER_BLIP = TRUE. ciSHOW_ONLY_FRIENDLY_GAMER_TAGS is on. Ignoring player blip hidden alpha override ")
			ENDIF
		#ENDIF
		ENDIF
	ENDIF
	
	IF (GET_CAM_VIEW_MODE_FOR_CONTEXT(GET_CAM_ACTIVE_VIEW_MODE_CONTEXT()) = CAM_VIEW_MODE_FIRST_PERSON)
		IF (localPlayerVehicle = remotePlayerVehicle)
			IF DOES_ENTITY_EXIST(localPlayerVehicle)
				IF GET_ENTITY_MODEL(localPlayerVehicle) != LUXOR
				AND GET_ENTITY_MODEL(localPlayerVehicle) != LUXOR2
					IF NOT  IS_PLAYER_USING_ARENA()
						#IF IS_DEBUG_BUILD
						IF g_TurnOnOverheadDebug
							PRINTLN("[Overheads] - in 1st perosn mode, in sanme vehicle as player ", iPlayerNum, " and not in a luxor. Setting alpha = ", iAlpha)
						ENDIF
						#ENDIF
						iAlpha = 0
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.iOnHeistBitset, iPlayerNum) != MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.bLaunchedHeist
		iAlpha = 0
		#IF IS_DEBUG_BUILD
		IF g_TurnOnOverheadDebug
			PRINTLN("[Overheads] - bOnHeist[", iPlayerNum, "] != bLaunchedHeist. Setting alpha = ", iAlpha)
		ENDIF
		#ENDIF
	ENDIF
	
	// url:bugstar:2237961 - ST 17/2/15
	IF g_TransitionSessionNonResetVars.sGlobalCelebrationData.bReadyForApartmentTakeOver
		iAlpha = 0
		#IF IS_DEBUG_BUILD
		IF g_TurnOnOverheadDebug
			PRINTLN("[Overheads] - bReadyForApartmentTakeOver = TRUE. Setting alpha = ", iAlpha)
		ENDIF
		#ENDIF
	ENDIF
	
	IF IS_STRIPCLUB_POST_HEIST_SCENE_PLAYING()
		iAlpha = 0
		#IF IS_DEBUG_BUILD
		IF g_TurnOnOverheadDebug
			PRINTLN("[Overheads] - IS_STRIPCLUB_POST_HEIST_SCENE_PLAYING = TRUE. Setting alpha = ", iAlpha)
		ENDIF
		#ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSet, ciHIDE_GAMERTAGS)
	AND NETWORK_IS_ACTIVITY_SESSION()
		iAlpha = 0
		#IF IS_DEBUG_BUILD
		IF g_TurnOnOverheadDebug
			PRINTLN("[Overheads] - ciHIDE_GAMERTAGS = TRUE. Setting alpha = ", iAlpha)
		ENDIF
		#ENDIF
	ENDIF
	
	IF NETWORK_IS_ACTIVITY_SESSION()
		INT iPlayerTeam = GET_PLAYER_TEAM(playerId)
		IF iPlayerTeam < FMMC_MAX_TEAMS
		AND iPlayerTeam >= 0
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyNine, ciOptionsBS29_HideGamertagsForTeam0 + iPlayerTeam)
				iAlpha = 0
				#IF IS_DEBUG_BUILD
				IF g_TurnOnOverheadDebug
					PRINTLN("[Overheads] - ciOptionsBS29_HideGamertagsForTeam", iPlayerTeam, " = TRUE. Setting alpha = ", iAlpha)
				ENDIF
				#ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	// For specific ambient events we don't want to show the white arrow and red arrow
	// at the same time. url:bugstar:2375007.
	IF GlobalplayerBD_FM_3[iPlayerNum].iFmAmbientEventType = FMMC_TYPE_PENNED_IN
		iSpecificAlpha = 0
		iSpecificTag = ENUM_TO_INT(MP_TAG_ARROW)
		#IF IS_DEBUG_BUILD
		IF g_TurnOnOverheadDebug
			PRINTLN("[Overheads] - FMMC_TYPE_PENNED_IN = TRUE. Setting alpha = ", iAlpha, ", iSpecificAlpha = ", iSpecificAlpha)
		ENDIF
		#ENDIF
	ENDIF
	
	// Set alpha to 0 if critical to Moving Target, Dead Drop or Hold the Wheel - url:bugstar:2463281
	IF IS_FM_PLAYER_CRITICAL_TO_THIS_FM_EVENT(playerId, FMMC_TYPE_MOVING_TARGET)
	OR IS_FM_PLAYER_CRITICAL_TO_THIS_FM_EVENT(playerId, FMMC_TYPE_DEAD_DROP)
	OR IS_FM_PLAYER_CRITICAL_TO_THIS_FM_EVENT(playerId, FMMC_TYPE_HOLD_THE_WHEEL)
		iSpecificAlpha = 0
		iSpecificTag = ENUM_TO_INT(MP_TAG_ARROW)
		#IF IS_DEBUG_BUILD
		IF g_TurnOnOverheadDebug
			PRINTLN("[Overheads] - FMMC_TYPE_MOVING_TARGET, FMMC_TYPE_DEAD_DROP, FMMC_TYPE_HOLD_THE_WHEEL = TRUE. Setting alpha = ", iAlpha, ", iSpecificAlpha = ", iSpecificAlpha)
		ENDIF
		#ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciSHOW_ONLY_FRIENDLY_GAMER_TAGS)
		INT iTeam = GET_PLAYER_TEAM(PLAYER_ID())
		IF GET_PLAYER_TEAM(playerId) != iTeam
			iAlpha = 0
			#IF IS_DEBUG_BUILD
			IF g_TurnOnOverheadDebug
				PRINTLN("[Overheads] - ciSHOW_ONLY_FRIENDLY_GAMER_TAGS is on")
				PRINTLN("[Overheads] - ciSHOW_ONLY_FRIENDLY_GAMER_TAGS player isn't on the same team.")
				PRINTLN("[Overheads] - ciSHOW_ONLY_FRIENDLY_GAMER_TAGS - setting gamertag alpha to iAlpha = ", iAlpha)
			ENDIF
			#ENDIF
		ENDIF
	ENDIF
	
	IF IS_PLAYER_IN_NIGHTCLUB(playerId)
		iAlpha = 0
		#IF IS_DEBUG_BUILD
		IF g_TurnOnOverheadDebug
			PRINTLN("[Overheads] - IS_PLAYER_IN_NIGHTCLUB = TRUE. Setting alpha = ", iAlpha, ", iSpecificAlpha = ", iSpecificAlpha)
		ENDIF
		#ENDIF
	ENDIF
	
	IF IS_RC_VEHICLE_CLEANING_UP()
		iAlpha = 0
		#IF IS_DEBUG_BUILD
		IF g_TurnOnOverheadDebug
			PRINTLN("[Overheads] - IS_RC_VEHICLE_CLEANING_UP = TRUE. Setting alpha = ", iAlpha, ", iSpecificAlpha = ", iSpecificAlpha)
		ENDIF
		#ENDIF
	ENDIF
	
	IF IS_DRONE_CLEANING_UP()
		iAlpha = 0
		#IF IS_DEBUG_BUILD
		IF g_TurnOnOverheadDebug
			PRINTLN("[Overheads] - IS_DRONE_CLEANING_UP = TRUE. Setting alpha = ", iAlpha, ", iSpecificAlpha = ", iSpecificAlpha)
		ENDIF
		#ENDIF
	ENDIF
	
	IF IS_MP_GAMER_TAG_ACTIVE(iPlayerNum)
		REPEAT COUNT_OF(eMP_TAG) iTagCount
			IF iSpecificTag = iTagCount
				IF iSpecificAlpha <= 0
					SET_MP_GAMER_TAG_VISIBILITY(iPlayerNum, INT_TO_ENUM(eMP_TAG, iTagCount), FALSE)
				ENDIF
				SET_MP_GAMER_TAG_ALPHA(iPlayerNum, INT_TO_ENUM(eMP_TAG, iTagCount), iSpecificAlpha)
				#IF IS_DEBUG_BUILD
				IF g_TurnOnOverheadDebug
					PRINTLN("[Overheads] - call 1 - setting gamertag alpha to iSpecificAlpha = ", iSpecificAlpha)
				ENDIF
				#ENDIF
			ELSE
				IF iAlpha <= 0
					SET_MP_GAMER_TAG_VISIBILITY(iPlayerNum, INT_TO_ENUM(eMP_TAG, iTagCount), FALSE)
				ENDIF
				SET_MP_GAMER_TAG_ALPHA(iPlayerNum, INT_TO_ENUM(eMP_TAG, iTagCount), iAlpha)
				#IF IS_DEBUG_BUILD
				IF g_TurnOnOverheadDebug
					PRINTLN("[Overheads] - call 0 - setting gamertag alpha to iAlpha = ", iAlpha)
				ENDIF
				#ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
ENDPROC

PROC MAINTAIN_FIRST_TIME_ON_SCREEN_FLASHING()
	
	SWITCH MPGlobalsHud_TitleUpdate.iFirstTimeFlashingStage
		CASE 0
			IF IS_HELP_MESSAGE_BEING_DISPLAYED()
			AND IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM_IHELP_OTP")
			#IF IS_DEBUG_BUILD OR bTestFlashingTags #ENDIF
				MPGlobalsHud_TitleUpdate.iFirstTimeFlashingStage++
			ENDIF
		BREAK
		CASE 1
			IF IS_HELP_MESSAGE_BEING_DISPLAYED()
			AND IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM_IHELP_OTP")
			#IF IS_DEBUG_BUILD OR bTestFlashingTags #ENDIF
				FLASH_GAMERTAGS()
			ELSE
				MPGlobalsHud_TitleUpdate.iFirstTimeFlashingStage++
			ENDIF
		BREAK
		CASE 2
			REFRESH_ALL_OVERHEAD_DISPLAY()
			RESET_FLASH_GAMERTAGS_DATA()
			MPGlobalsHud_TitleUpdate.iFirstTimeFlashingStage = 0
		BREAK
	ENDSWITCH
	
ENDPROC

PROC SET_ON_JOB_INTRO(BOOL bOnIntro)

	IF MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.bOnJobIntro[0] != bOnIntro
	
		MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.bOnJobIntro[0] = bOnIntro
		
		#IF IS_DEBUG_BUILD
		IF bOnIntro
			PRINTLN("[Overheads] - SET_ON_JOB_INTRO been called. bOnIntro = TRUE.")
		ELSE
			PRINTLN("[Overheads] - SET_ON_JOB_INTRO been called. bOnIntro = FALSE.")
		ENDIF
		DEBUG_PRINTCALLSTACK()
		#ENDIF
		
	ENDIF
	
ENDPROC

FUNC BOOL SHOULD_USE_VEHICLE_HEALTH_BAR(PLAYER_INDEX playerId)
	IF IS_ON_VEH_DEATHMATCH_GLOBAL_SET()
		RETURN TRUE
	ELIF NETWORK_IS_ACTIVITY_SESSION()
		IF (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_VEHICLE_OVERHEAD_HEALTH)
		AND IS_PED_IN_ANY_VEHICLE( GET_PLAYER_PED(playerId)))
		AND (playerId != PLAYER_ID() OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne, ciOptionsBS21_ShowVehicleHealthOverheadForLocalPlayer))
			RETURN TRUE
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_VEHICLE_OVERHEAD_HEALTH) 
		AND IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE()
			RETURN TRUE		
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC	

/// PURPOSE:
///    Process freemode overheads.
/// PARAMS:
///    PlayerId - player to process.
///    iPlayernum - 
///    isAiming - if the player is aiming.
///    isDpaddownActive - if dpad down is activated.
PROC RUN_OVERALL_FREEMODE_LOGIC_CHECKS(PLAYER_INDEX PlayerId, INT iPlayernum, BOOL isDpaddownActive)
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	OPEN_SCRIPT_PROFILE_MARKER_GROUP("RUN_OVERALL_FREEMODE_LOGIC_CHECKS")
	#ENDIF
	#ENDIF
	
	IF IS_MP_GAMER_TAG_MOVIE_ACTIVE()			
		// ****************************************************************
		// Set required flags that will be used through rest of function.
		// ****************************************************************
		
		PED_INDEX PlayerPedId 						= GET_PLAYER_PED(PlayerId)
		BOOL bProcessingLocalPlayer 				= (PlayerID = PLAYER_ID())
		BOOL bPassedPlayerVisibleToLocalPlayer		= TRUE
		INT  iPlayerWantedLevel						= GET_PLAYER_WANTED_LEVEL(playerID)
		INT iPlayerFakeWantedLevel					= GET_PLAYER_FAKE_WANTED_LEVEL(PlayerID)
		FLOAT fDisFromPlayer						= VDIST2(GET_ENTITY_COORDS(PlayerPedId), GET_ENTITY_COORDS(PLAYER_PED_ID()))
		INT iTag
		INT iPixelHeight							= -1
		BOOL bInHeli								= IS_PED_IN_ANY_HELI(PlayerPedId)
		BOOL bInPlane								= IS_PED_IN_ANY_PLANE(PlayerPedId)
		BOOL bInAnyVehicle							= IS_PED_IN_ANY_VEHICLE(PlayerPedId)
		INt iLocalPlayerNum							= NATIVE_TO_INT(PLAYER_ID())
		GB_GANG_ROLE  PlayerGangrole				= GB_GET_PLAYER_GANG_ROLE(PlayerId)
		INT ipackagesheld							= g_FMMC_STRUCT.iPTLOwnedPickUps[NATIVE_TO_INT(PlayerId)]
		INT ipackage_holder_patritipant				= g_iDawnRaidPickupCarrierPart
		IF IS_PED_IN_ANY_HELI(GET_PLAYER_PED(PLAYER_ID()))
		OR MPGlobals.sFreemodeCache.bLocalPlayerSCTV
			iPixelHeight = 10
		ENDIF
		
		INT iMarkerDisplay = GET_PACKED_STAT_INT(PACKED_MP_PLAYER_MARKER_DISPLAY)
		
		IF IS_PLAYER_SCTV(PLAYER_ID())
			fDisFromPlayer = VDIST(GET_PLAYER_COORDS(playerId), GET_FINAL_RENDERED_CAM_COORD())
//			PRINTLN("[Overheads] - distance from player = ", fDisFromPlayer)
		ENDIF
		

		
		IF NOT bProcessingLocalPlayer
			bPassedPlayerVisibleToLocalPlayer = CAN_I_SEE_PLAYER(PlayerId, iPixelHeight)
			
			IF IS_BIT_SET(MPGlobalsAmbience.iAmbBitSet, iABI_HEIST_SPECTATE_WHILE_DEAD_ENABLED)
		    AND IS_THIS_ROCKSTAR_MISSION_NEW_VS_RESURRECTION(g_FMMC_STRUCT.iAdversaryModeType)
			AND IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iSpecInfoBitset, SPEC_INFO_BS_USING_HEIST_SPECTATE) 
				bPassedPlayerVisibleToLocalPlayer = TRUE
				PRINTLN("RUN_OVERALL_FREEMODE_LOGIC_CHECKS RESURRECTION MODE bPassedPlayerVisibleToLocalPlayer = TRUE")
			ENDIF		
			
			SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_ISVISIBLE, bPassedPlayerVisibleToLocalPlayer, PlayerId)
			//REFRESH_ALL_OVERHEAD_DISPLAY()
		ENDIF
		
		
		
		
		// ************************
		// Start the profiler \o/. 
		// ************************
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("RUN_OVERALL_FREEMODE_LOGIC_CHECKS INIT ")
		#ENDIF
		#ENDIF	
		
		// ***************************
		// Debug for trailer widgets.
		// ***************************
		
		#IF IS_DEBUG_BUILD
			IF g_bTogglePlayerOverhead[1][iPlayerNum] != g_bTogglePlayerOverhead[0][iPlayerNum]
				SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_NOTHING, g_bTogglePlayerOverhead[0][iPlayerNum], PlayerID)
				REFRESH_ALL_OVERHEAD_DISPLAY()
				PRINTLN("[overheads] - REFRESH_ALL_OVERHEAD_DISPLAY call 0")
				g_bTogglePlayerOverhead[1][iPlayerNum] = g_bTogglePlayerOverhead[0][iPlayerNum]
			ENDIF
		#ENDIF

	
		
		// For switch between arrow and tag depending on distance of player from local player.
		IF (iMarkerDisplay = MARKER_OPTIONS_FULL)
		OR (iMarkerDisplay = MARKER_OPTIONS_FULL_WITH_CREWTAG)
			
			IF bInHeli
			OR bInPlane
			OR IS_BIT_SET(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iFmAmbientEventBitSet, ciSET_PLAYER_IS_PENNED_IN_SPECTATOR) 
				SET_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_ARROW, FALSE, playerId)
			ELIF NOT bPassedPlayerVisibleToLocalPlayer
				SET_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_ARROW, FALSE, playerId)
			ELSE
				IF (NOT MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.bAtArrowNotTagDistance[iPlayerNum]) 
				OR iMarkerDisplay != MPGlobalsHud_TitleUpdate.iOhDensitySetting[iLocalPlayerNum]
					IF (fDisFromPlayer >= GET_OVERHEAD_ARROW_MARKER_DISTANCE()*GET_OVERHEAD_ARROW_MARKER_DISTANCE()*GET_LODSCALE())
						#IF IS_DEBUG_BUILD
							FLOAT fTemp, fTemp2, fTemp3
							fTemp = GET_OVERHEAD_ARROW_MARKER_DISTANCE()
							fTemp2 = GET_LODSCALE()
							fTemp3 = ( GET_OVERHEAD_ARROW_MARKER_DISTANCE()*GET_OVERHEAD_ARROW_MARKER_DISTANCE()*GET_LODSCALE())
							PRINTLN("[overheads] - refreshing overheads - arrow on - fDisFromPlayer = ", fDisFromPlayer)
							PRINTLN("[overheads] - refreshing overheads - arrow on - GET_OVERHEAD_ARROW_MARKER_DISTANCE = ", fTemp)
							PRINTLN("[overheads] - refreshing overheads - arrow on - GET_LODSCALE = ", fTemp2)
							PRINTLN("[overheads] - refreshing overheads - arrow on - (GET_OVERHEAD_ARROW_MARKER_DISTANCE()*GET_OVERHEAD_ARROW_MARKER_DISTANCE()*GET_LODSCALE()) = ", fTemp3)
						#ENDIF
						REFRESH_PLAYER_OVERHEAD_DISPLAY(iPlayerNum, playerId)
						PRINTLN("[overheads] - REFRESH_ALL_OVERHEAD_DISPLAY call 3")
						SET_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_ARROW, TRUE, playerId)
						MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.bAtArrowNotTagDistance[iPlayerNum] = TRUE
					ENDIF
				ENDIF
				IF (MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.bAtArrowNotTagDistance[iPlayerNum]) 
				OR (iMarkerDisplay != MPGlobalsHud_TitleUpdate.iOhDensitySetting[iLocalPlayerNum])
				AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciHARD_TARGET_ENABLE_MODE)	
					IF (fDisFromPlayer < GET_OVERHEAD_ARROW_MARKER_DISTANCE()*GET_OVERHEAD_ARROW_MARKER_DISTANCE()*GET_LODSCALE())
					
						#IF IS_DEBUG_BUILD
							FLOAT fTemp, fTemp2, fTemp3
							fTemp = GET_OVERHEAD_ARROW_MARKER_DISTANCE()
							fTemp2 = GET_LODSCALE()
							fTemp3 = ( GET_OVERHEAD_ARROW_MARKER_DISTANCE()*GET_OVERHEAD_ARROW_MARKER_DISTANCE()*GET_LODSCALE())
							PRINTLN("[overheads] - refreshing overheads - arrow off - fDisFromPlayer = ", fDisFromPlayer)
							PRINTLN("[overheads] - refreshing overheads - arrow off - GET_OVERHEAD_ARROW_MARKER_DISTANCE = ", fTemp)
							PRINTLN("[overheads] - refreshing overheads - arrow off - GET_LODSCALE = ", fTemp2)
							PRINTLN("[overheads] - refreshing overheads - arrow off - (GET_OVERHEAD_ARROW_MARKER_DISTANCE()*GET_OVERHEAD_ARROW_MARKER_DISTANCE()*GET_LODSCALE()) = ", fTemp3)
						#ENDIF
						REFRESH_PLAYER_OVERHEAD_DISPLAY(iPlayerNum, playerId)
						PRINTLN("[overheads] - REFRESH_ALL_OVERHEAD_DISPLAY call 4")
						SET_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_ARROW, FALSE, playerId)
						MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.bAtArrowNotTagDistance[iPlayerNum] = FALSE
						
					ENDIF
				ENDIF
			ENDIF
			
		ELIF (iMarkerDisplay = MARKER_OPTIONS_ARROW_ONLY)	
			IF iMarkerDisplay != MPGlobalsHud_TitleUpdate.iOhDensitySetting[iLocalPlayerNum]
				#IF IS_DEBUG_BUILD
					PRINTLN("[overheads] - refreshing overheads - arrow only")
				#ENDIF
				REFRESH_PLAYER_OVERHEAD_DISPLAY(iPlayerNum, playerId)
				SET_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_ARROW, TRUE, playerId)
				MPGlobalsHud_TitleUpdate.iOhDensitySetting[iPlayerNum] = iMarkerDisplay
			ENDIF
		ENDIF
		
		// If we have requested no gamer tag hud in the options menu, turn everything off.
		IF iMarkerDisplay != MPGlobalsHud_TitleUpdate.iOhDensitySetting[iPlayerNum]
		
			NET_PRINT("[WJK] - RUN_OVERALL_FREEMODE_LOGIC_CHECKS - marker display setting mismatch, refreshing overhead displays.")NET_NL()
			NET_PRINT("[WJK] - PACKED_MP_PLAYER_MARKER_DISPLAY = ")NET_PRINT_INT(iMarkerDisplay)NET_NL()
			NET_PRINT("[WJK] - MPGlobalsHud_TitleUpdate.iOhDensitySetting[iPlayerNum] = ")NET_PRINT_INT(MPGlobalsHud_TitleUpdate.iOhDensitySetting[iPlayerNum])NET_NL()
			
			IF iMarkerDisplay = MARKER_OPTIONS_OFF
				RESET_OVERHEAD_LOGIC_BITSET(playerID)
			ENDIF
			
			REFRESH_PLAYER_OVERHEAD_DISPLAY(iPlayerNum, playerId)
			
			MPGlobalsHud_TitleUpdate.iOhDensitySetting[iPlayerNum] = iMarkerDisplay
			
			NET_PRINT("[WJK] - MPGlobalsHud_TitleUpdate.iOhDensitySetting now = ")NET_PRINT_INT(MPGlobalsHud_TitleUpdate.iOhDensitySetting[iPlayerNum])NET_NL()
			
		ENDIF
		
		
		
		// ***********************************************************************
		// Process each type of overhead if the logic for it has been activated.
		// ***********************************************************************
		
		// Set the dpad down logic active if the player has pressed dpad down.
		SET_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_LOCAL_DPADDOWNACTIVE, MAKE_DPAD_DOWN_LOGIC_ACTIVE(isDpaddownActive, bPassedPlayerVisibleToLocalPlayer), playerId)
		
		// Check if a player has just appeared on screen. 
		SET_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_JUST_APPEARED_ON_SCREEN, MAKE_APPEAR_ON_SCREEN_LOGIC_ACTIVE(bPassedPlayerVisibleToLocalPlayer, bProcessingLocalPlayer, PlayerId), playerId)
		
		//Add checks for spectator here, fake dpaddownactive
		IF DOES_A_SPECTATOR_CAM_WANT_OVERHEAD()
		OR IS_PLAYER_SCTV(PLAYER_ID())
		OR IS_BIT_SET(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iFmAmbientEventBitSet, ciSET_PLAYER_IS_PENNED_IN_SPECTATOR) 
		OR (IS_BIT_SET(MPGlobalsAmbience.iAmbBitSet, iABI_HEIST_SPECTATE_WHILE_DEAD_ENABLED) AND IS_THIS_ROCKSTAR_MISSION_NEW_VS_RESURRECTION(g_FMMC_STRUCT.iAdversaryModeType) AND IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iSpecInfoBitset, SPEC_INFO_BS_USING_HEIST_SPECTATE) )
	
			SET_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_LOCAL_DPADDOWNACTIVE, TRUE, playerId)
//			IF IS_PLAYER_SCTV(PLAYER_ID())
//				PRINTLN("[Overheads] - setting DISPLAYOVERHEADS_LOGIC_IS_LOCAL_DPADDOWNACTIVE active for player for SCTV: ", fDisFromPlayer)
//			ENDIF
		ENDIF
		
		// Set passive event based on if in passiv emode or not.
		IF IS_OVERHEAD_EVENT_ACTIVE(DISPLAYOVERHEADS_EVENTS_IS_PLAYER_PASSIVE, playerId)
			// IF NOT IS_PLAYER_PASSIVE(playerId)
			IF NOT GlobalplayerBD_FM_3[NATIVE_TO_INT(playerId)].bInPassive
				IF HAS_NET_TIMER_STARTED(MPGlobalsHud.timerPassiveOhBitBackupTimer)
					IF HAS_NET_TIMER_EXPIRED(MPGlobalsHud.timerPassiveOhBitBackupTimer, 3000)
						SET_OVERHEAD_EVENT_ACTIVE(DISPLAYOVERHEADS_EVENTS_IS_PLAYER_PASSIVE, FALSE, playerId)
						RESET_NET_TIMER(MPGlobalsHud.timerPassiveOhBitBackupTimer)
						PRINTLN("[Overheads] - player ", iPlayernum, " is not bInPassive. Clearing bit DISPLAYOVERHEADS_EVENTS_IS_PLAYER_PASSIVE.")
					ENDIF
				ENDIF
			ENDIF
			// ENDIF
		ELSE
			// IF IS_PLAYER_PASSIVE(playerId)
			IF GlobalplayerBD_FM_3[NATIVE_TO_INT(playerId)].bInPassive
				IF HAS_NET_TIMER_STARTED(MPGlobalsHud.timerPassiveOhBitBackupTimer)
					IF HAS_NET_TIMER_EXPIRED(MPGlobalsHud.timerPassiveOhBitBackupTimer, 3000)
						SET_OVERHEAD_EVENT_ACTIVE(DISPLAYOVERHEADS_EVENTS_IS_PLAYER_PASSIVE, TRUE, playerId)
						RESET_NET_TIMER(MPGlobalsHud.timerPassiveOhBitBackupTimer)
						PRINTLN("[Overheads] - player ", iPlayernum, " is bInPassive. Setting bit DISPLAYOVERHEADS_EVENTS_IS_PLAYER_PASSIVE.")
					ENDIF
				ENDIF
			ENDIF
			// ENDIF
		ENDIF
		
		// Set interaction menu logic active if it is on.
		SET_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_INTERACTION_MENU_UP, MAKE_INTERACTION_MENU_TARGETTING_LOGIC_ACTIVE((MPGlobals.PlayerInteractionData.iTargetPlayerInt != -1), bPassedPlayerVisibleToLocalPlayer), playerId )
		
		// Set aiming at other player logic active if the player aiming at another ped.
		SET_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_AIMING_AT_PLAYER, MAKE_AIMING_LOGIC_ACTIVE(IS_PLAYER_AIMING_AT_PED(PlayerPedId), bPassedPlayerVisibleToLocalPlayer), playerId)
		
		// Set using chat window logic active if player is using chat window.
		SET_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_CHAT_WINDOW, MAKE_CHAT_WINDOW_LOGIC_ACTIVE(IS_MP_TEXT_CHAT_TYPING(), bPassedPlayerVisibleToLocalPlayer), playerId)
		
		// Set the wanted level logic active if the player has a wanted level.
		SET_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_ARE_WANTED, MAKE_WANTED_STARS_LOGIC_ACTIVE(((iPlayerWantedLevel > 0) OR (iPlayerFakeWantedLevel > 0)), bPassedPlayerVisibleToLocalPlayer), playerId)
		
		// Set vote yes logic active if player has voted yes? not sure what this means, ask Brenda about it.
		SET_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_DID_PLAYER_VOTE_YES, GlobalplayerBD_FM_2[iPlayerNum].bHasPlayerVoted, playerId)
		
		// Set diaply overhead stats logic active if can do.
		SET_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_CAN_DISPLAY, MAKE_OVERHEAD_STATS_LOGIC_ACTIVE(CAN_DISPLAY_OVERHEAD_STATS(PlayerID), bPassedPlayerVisibleToLocalPlayer), playerId)
		
		// Set talking icon logc active if player is talking. 
		SET_OVERHEAD_EVENT_ACTIVE(DISPLAYOVERHEADS_EVENTS_TALKING, MAKE_TALKING_LOGIC_ACTIVE(NETWORK_IS_PLAYER_TALKING(PlayerID), bPassedPlayerVisibleToLocalPlayer), playerId)
		
		// Set tagged logic active if player is set as tagged.
		SET_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_TAGGED, MAKE_TAGGED_LOGIC_ACTIVE(IS_BIT_SET(MPGlobals.iTaggedPlayersBitSet, NATIVE_TO_INT(PlayerId)), bPassedPlayerVisibleToLocalPlayer), playerId)
		
		// Display in corona icons if player is in a corona.
		SET_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_IN_CORONA, MAKE_IN_CORONA_LOGIC_ACTIVE(IS_THIS_PLAYER_IN_CORONA(playerId), bPassedPlayerVisibleToLocalPlayer), playerId)
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("RUN_OVERALL_FREEMODE_LOGIC_CHECKS SCRIPT LOGIC ")
		#ENDIF
		#ENDIF
		
		
		
		// ****************************************************************************************
		// Refresh logic if any properties of it have changed and update what is being displayed.
		// ****************************************************************************************
		
		IF HAS_OVERHEAD_LOGIC_CHANGED(playerID)
		OR HAS_OVERHEAD_EVENT_CHANGED(playerID)
		OR HAS_OVERHEAD_ACTIVATION_CHANGED(PlayerID)
			
			// Print a bunch of info.
			#IF IS_DEBUG_BUILD
				IF g_TurnOnOverheadDebug
					IF HAS_OVERHEAD_LOGIC_CHANGED(PlayerID)
						NET_NL()NET_PRINT("HAS_OVERHEAD_ACTIVATION_CHANGED HAVE CHANGED for player ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
						NET_NL()NET_PRINT("		-Player num = ")NET_PRINT_INT(iPlayernum)
						NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))
					ENDIF
					IF HAS_OVERHEAD_EVENT_CHANGED(PlayerID)
						NET_NL()NET_PRINT("HAS_OVERHEAD_EVENT_CHANGED HAVE CHANGED for player ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
						NET_NL()NET_PRINT("		-Player num = ")NET_PRINT_INT(iPlayernum)
						NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))
					ENDIF
					IF HAS_OVERHEAD_ACTIVATION_CHANGED(PlayerID)
						NET_NL()NET_PRINT("HAS_OVERHEAD_ACTIVATION_CHANGED HAVE CHANGED for player ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
						NET_NL()NET_PRINT("		-Player num = ")NET_PRINT_INT(iPlayernum)
						NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))
					ENDIF
				ENDIF
			#ENDIF
			
			// Pause menu.
			RUN_OHD_PAUSEMENU_CHECK(bProcessingLocalPlayer, bPassedPlayerVisibleToLocalPlayer, playerID)
			
			// Respawning.
			RUN_OHD_RESPAWNING_CHECK(bProcessingLocalPlayer, bPassedPlayerVisibleToLocalPlayer, playerID)
			
			// Check if the player name should be displayed.
			RUN_OHD_PLAYER_NAME_CHECK_FM(bProcessingLocalPlayer, bPassedPlayerVisibleToLocalPlayer, playerID )
			
			// Check if we should show the overhead arow.
			RUN_OHD_PLAYER_ARROW_CHECK_FM(bProcessingLocalPlayer, bPassedPlayerVisibleToLocalPlayer, playerID)
			
			// Check if the wanted level should display.
			RUN_OHD_WANTED_LEVEL_CHECK_FM(bProcessingLocalPlayer, bPassedPlayerVisibleToLocalPlayer, playerID, iPlayerWantedLevel, iPlayerFakeWantedLevel)
			
			// Check if the talking icon should be displayed.
			RUN_OHD_TALKING_CHECK(bProcessingLocalPlayer, bPassedPlayerVisibleToLocalPlayer, playerID)
			
			// Check if the healthbar should display.
			RUN_OHD_HEALTHBAR_CHECK(bPassedPlayerVisibleToLocalPlayer, playerID)
			
			// Check if the voting should display.
			// RUN_OHD_VOTING_CHECK(bProcessingLocalPlayer, playerID)
			
			// Check if the crew emblem should display.
			RUN_OHD_CREW_CHECK(bProcessingLocalPlayer, bPassedPlayerVisibleToLocalPlayer, playerID)
			
			// Check if the passive marker should display.
			RUN_OHD_PASSIVE_CHECK(bProcessingLocalPlayer, bPassedPlayerVisibleToLocalPlayer, playerID, bInAnyVehicle)
			
			// Check if the tagged icon should display.
			RUN_OHD_IS_TAGGED_CHECK(bProcessingLocalPlayer, bPassedPlayerVisibleToLocalPlayer, playerID)
			
			// Rally driver.
			RUN_OHD_IS_RALLY_DRIVER_CHECK(bProcessingLocalPlayer, bPassedPlayerVisibleToLocalPlayer, playerID)
			
			// Rally passenger.
			RUN_OHD_IS_RALLY_PASSENGER_CHECK(bProcessingLocalPlayer, bPassedPlayerVisibleToLocalPlayer, playerID)
			
			// Check if betting overheads should be displayed.
			RUN_OHD_IS_BETTING_CHECK(bProcessingLocalPlayer, bPassedPlayerVisibleToLocalPlayer, playerID)
			
			// Check if the powerplay (this is for Chris's deathmatches) should be displayed.
	//		RUN_OHD_IS_POWERPLAY_CHECK(bProcessingLocalPlayer, bPassedPlayerVisibleToLocalPlayer, playerID)
			
			// Check if the cutsom string overhead should be displayed.
			RUN_OHD_IS_CUSTOM_STRING_CHECK(bProcessingLocalPlayer, bPassedPlayerVisibleToLocalPlayer, playerID)
			
			// Chat window.
			RUN_OHD_CHAT_WINDOW_CHECK(bProcessingLocalPlayer, bPassedPlayerVisibleToLocalPlayer, playerID)
			
			// Do we need to hide all? Temp run this all the time to see if it sorts timing issues. 
			RUN_OVERHEAD_HIDE_ALL_CHECKS(iPlayernum, playerID, bPassedPlayerVisibleToLocalPlayer, fDisFromPlayer, FALSE)
			
			RUN_OHD_GANG_ICONS_FM(bProcessingLocalPlayer, bPassedPlayerVisibleToLocalPlayer, playerID)
			
			RUN_OHD_PACKAGE_TRANSMITTORS(bProcessingLocalPlayer, bPassedPlayerVisibleToLocalPlayer, playerID)
		ENDIF
		
		
		
		
		// ********************************************************************************************************************************
		// Some brute force checks to see if a massive refresh is needed. If so, all flags are blanked and everything is calculated again.
		// Really for if properties change about the icons that don't require the activity of them to change.
		// ********************************************************************************************************************************

		RUN_OVERHEAD_HIDE_ALL_CHECKS(iPlayernum, playerID, bPassedPlayerVisibleToLocalPlayer, fDisFromPlayer, TRUE)
		
		// For overhead arrow maker.
	//	RUN_ARROW_MARKER_CHECK(bProcessingLocalPlayer, bPassedPlayerVisibleToLocalPlayer, playerID, fDisFromPlayer)
		
		// Num wanted stars have changed.
		IF NOT bProcessingLocalPlayer
			IF MPGlobalsHud.DisplayInfo.iOverhead_iPlayerWantedLevel_LastFrame[iPlayerNum] != iPlayerWantedLevel
				IF g_sTriggerMP.mtState != NO_CURRENT_MP_TRIGGER_REQUEST
					SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CURRENT_WANTED, iPlayerWantedLevel)
	//				REQUEST_SAVE()
				ENDIF
				SET_OVERHEAD_EVENT_ACTIVE(DISPLAYOVERHEADS_EVENTS_REFRESH_WANTED, TRUE, playerID)
				RUN_OHD_WANTED_LEVEL_CHECK_FM(bProcessingLocalPlayer, bPassedPlayerVisibleToLocalPlayer, playerID, iPlayerWantedLevel, iPlayerFakeWantedLevel)
			ENDIF
			MPGlobalsHud.DisplayInfo.iOverhead_iPlayerWantedLevel_LastFrame[iPlayerNum] = iPlayerWantedLevel
		ENDIF
		
		// Num wanted stars have changed.
		IF NOT bProcessingLocalPlayer
			IF MPGlobalsHud.DisplayInfo.iOverhead_iPlayerFakeWantedLevel_LastFrame[iPlayerNum] != iPlayerFakeWantedLevel
				SET_OVERHEAD_EVENT_ACTIVE(DISPLAYOVERHEADS_EVENTS_REFRESH_WANTED, TRUE, playerID)
				RUN_OHD_WANTED_LEVEL_CHECK_FM(bProcessingLocalPlayer, bPassedPlayerVisibleToLocalPlayer, playerID, iPlayerWantedLevel, iPlayerFakeWantedLevel)
			ENDIF
			MPGlobalsHud.DisplayInfo.iOverhead_iPlayerFakeWantedLevel_LastFrame[iPlayerNum] = iPlayerFakeWantedLevel
		ENDIF
		
		// PLayer has changed gang role
		
		IF MPGlobalsHud.DisplayInfo.iOverhead_iPlayerGANG_ROLE_LastFrame[iPlayerNum] != PlayerGangrole
			SET_OVERHEAD_EVENT_ACTIVE(DISPLAYOVERHEADS_EVENTS_CHANGED_GANG_ROLE, TRUE, playerID)
			//RUN_OHD_WANTED_LEVEL_CHECK_FM(bProcessingLocalPlayer, bPassedPlayerVisibleToLocalPlayer, playerID, PlayerGangrole, iPlayerFakeWantedLevel)
			RUN_OHD_GANG_ICONS_FM(bProcessingLocalPlayer, bPassedPlayerVisibleToLocalPlayer, playerID)
		ENDIF
		MPGlobalsHud.DisplayInfo.iOverhead_iPlayerGANG_ROLE_LastFrame[iPlayerNum] = PlayerGangrole
		
		
		// if number of packages have changed
		IF MPGlobalsHud.DisplayInfo.iOverhead_iPlayerLargepackages_LastFrame[iPlayerNum] != ipackagesheld
		//OR IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_LOCAL_DPADDOWNACTIVE, PLAYER_ID())	AND  IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciPOINTLESS_ENABLE_OVERHEAD_DISPLAY)	
				SET_OVERHEAD_EVENT_ACTIVE(DISPLAYOVERHEADS_EVENTS_CARRYING_LARGE_PACKAGE, TRUE, playerID)
				RUN_OHD_INVENTORY_CHECK(bProcessingLocalPlayer, iPlayerNum, playerID)
				PRINTLN("[overhead] 1 RUN_OHD_INVENTORY_CHECK:  g_FMMC_STRUCT.iPTLOwnedPickUps[NATIVE_TO_INT(PlayerId)] ",  g_FMMC_STRUCT.iPTLOwnedPickUps[NATIVE_TO_INT(PlayerId)])
				PRINTLN("[overhead] RUN_OHD_INVENTORY_CHECK:  ipackagesheld =  ", ipackagesheld)
			
			//ENDIF
		ENDIF
		MPGlobalsHud.DisplayInfo.iOverhead_iPlayerLargepackages_LastFrame[iPlayerNum] = ipackagesheld
		
		IF MPGlobalsHud.DisplayInfo.iOverhead_iPatricipantIDPackage_transmittor_LastFrame[iPlayerNum] !=ipackage_holder_patritipant
			IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_DAWN_RAID(g_FMMC_STRUCT.iAdversaryModeType)
				SET_OVERHEAD_EVENT_ACTIVE(DISPLAYOVERHEADS_EVENTS_CARRYING_TRANSMITTOR_PACKAGE, TRUE, playerID)
				PRINTLN("[overhead] 1 g_iDawnRaidPickupCarrierPart =  ", g_iDawnRaidPickupCarrierPart)
				PRINTLN("[overhead] RUN_OHD_PACKAGE_TRANSMITTORS:  ipackagesheld =  ", ipackagesheld)
				RUN_OHD_PACKAGE_TRANSMITTORS(bProcessingLocalPlayer, bPassedPlayerVisibleToLocalPlayer, playerID)
			ENDIF
		ENDIF
		MPGlobalsHud.DisplayInfo.iOverhead_iPatricipantIDPackage_transmittor_LastFrame[iPlayerNum] =ipackage_holder_patritipant
		
		// If the show rally icons global has changed value, force a refresh.
		IF MPGlobalsHud.bShowRallyIcons[0][iPlayerNum] != MPGlobalsHud.bShowRallyIcons[1][iPlayerNum]
			RUN_OHD_IS_RALLY_DRIVER_CHECK(bProcessingLocalPlayer, bPassedPlayerVisibleToLocalPlayer, playerID)
			RUN_OHD_IS_RALLY_PASSENGER_CHECK(bProcessingLocalPlayer, bPassedPlayerVisibleToLocalPlayer, playerID)
			MPGlobalsHud.bShowRallyIcons[0][iPlayerNum] = MPGlobalsHud.bShowRallyIcons[1][iPlayerNum]
		ENDIF
		
	//	// Impromptu race status.
	//	IF MPGlobalsHud.bInInterMenuInpromptuRaceSubMenu != MPGlobalsAmbience.bInInterMenuInpromptuRaceSubMenu
	//		NET_PRINT("[overhead] MPGlobalsHud.bInInterMenuInpromptuRaceSubMenu != MPGlobalsAmbience.bInInterMenuInpromptuRaceSubMenu.")NET_NL()
	//		RUN_OHD_IS_RALLY_DRIVER_CHECK(bProcessingLocalPlayer, bPassedPlayerVisibleToLocalPlayer, playerID)
	//		MPGlobalsHud.bInInterMenuInpromptuRaceSubMenu = MPGlobalsAmbience.bInInterMenuInpromptuRaceSubMenu
	//	ENDIF
		
		// Race respawning overhead.
		IF MPGlobalsHud.bInRaceRespawnDelay[1] != MPGlobalsHud.bInRaceRespawnDelay[0]
			NET_PRINT("[overhead] MPGlobalsHud.bInRaceRespawnDelay[1] != MPGlobalsHud.bInRaceRespawnDelay[0].")NET_NL()
			MPGlobalsHud.bInRaceRespawnDelay[1] = MPGlobalsHud.bInRaceRespawnDelay[0]
			SET_OVERHEAD_EVENT_ACTIVE(DISPLAYOVERHEADS_EVENTS_REFRESH_RESPAWNING, TRUE, playerID)
		ENDIF
		
		// Inventory properties has changed.
		IF MPGlobalsHud.DisplayInfo.iOverhead_iPlayerInventory_LastFrame[iPlayerNum] <> GlobalplayerBD[iPlayerNum].iMyOverHeadInventory
		OR  IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_LOCAL_DPADDOWNACTIVE, PLAYER_ID())
			RUN_OHD_INVENTORY_CHECK(bProcessingLocalPlayer, iPlayerNum, playerID)
			println("[overhead] RUN_OHD_INVENTORY_CHECK CHANGED 1")
		ENDIF
	 	MPGlobalsHud.DisplayInfo.iOverhead_iPlayerInventory_LastFrame[iPlayerNum]	= GlobalplayerBD[iPlayerNum].iMyOverHeadInventory

		// Player team has changed.
		IF MPGlobalsHud.DisplayInfo.iOverhead_iTeamNum_LastFrame[iPlayerNum] <> GET_PLAYER_TEAM(PlayerId)
			REFRESH_OVERHEAD_DISPLAY(playerID)
			#IF IS_DEBUG_BUILD
			NET_NL()NET_PRINT("[overheads] RUN_OVERALL_FREEMODE_LOGIC_CHECKS: TEAM HAS CHANGED - REFRESH.  ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
			NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))
			#ENDIF
		ENDIF
		MPGlobalsHud.DisplayInfo.iOverhead_iTeamNum_LastFrame[iPlayerNum] = GET_PLAYER_TEAM(PlayerId) 
		
		// Turn off overheads during post mission cleanup.
		IF MPGlobalsHud.DisplayInfo.bNeedsFmCleanupCopy[iPlayerNum] != IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_BbNeedsFmCleanup)
			MPGlobalsHud.DisplayInfo.bNeedsFmCleanupCopy[iPlayerNum] = IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_BbNeedsFmCleanup)
			REFRESH_OVERHEAD_DISPLAY(playerID)
		ENDIF
		
		// Turn off overheads if the leader board camera is active.
		IF MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.bLeaderboardCamActiveCopy[iPlayerNum] != g_TransitionSessionNonResetVars.sPostMissionCleanupData.bLeaderboardCamRenderingOrInterping
			MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.bLeaderboardCamActiveCopy[iPlayerNum] = g_TransitionSessionNonResetVars.sPostMissionCleanupData.bLeaderboardCamRenderingOrInterping
			REFRESH_ALL_OVERHEAD_DISPLAY()
			PRINTLN("[overheads] - REFRESH_ALL_OVERHEAD_DISPLAY call 1")
		ENDIF
		
		// Debug for capture team.
		#IF IS_DEBUG_BUILD
			
			IF g_bBlockLocalPlayerOverhead[0]
				IF IS_DEBUG_KEY_JUST_PRESSED(KEY_L, KEYBOARD_MODIFIER_SHIFT, "Toggle Local Player Name Overhead")
					g_bBlockLocalPlayerOverhead[0] = FALSE
				ENDIF
			ELSE
				IF IS_DEBUG_KEY_JUST_PRESSED(KEY_L, KEYBOARD_MODIFIER_SHIFT, "Toggle Local Player Name Overhead")
					g_bBlockLocalPlayerOverhead[0] = TRUE
				ENDIF
			ENDIF
		
			IF GET_COMMANDLINE_PARAM_EXISTS("enableFarGamerTagDistance")
				IF g_bBlockLocalPlayerOverhead[1] != g_bBlockLocalPlayerOverhead[0]
					g_bBlockLocalPlayerOverhead[1] = g_bBlockLocalPlayerOverhead[0]
					REFRESH_ALL_OVERHEAD_DISPLAY()
					PRINTLN("[overheads] - REFRESH_ALL_OVERHEAD_DISPLAY call 2")
					RUN_OHD_PLAYER_NAME_CHECK_FM(TRUE, TRUE, PLAYER_ID())
					NET_NL()NET_PRINT("[overheads] RUN_OVERALL_FREEMODE_LOGIC_CHECKS: enableFarGamerTagDistance is on and g_bBlockLocalPlayerOverhead[1] != g_bBlockLocalPlayerOverhead[0] - REFRESH.  ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
					NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))
				ENDIF
			ENDIF
			
		#ENDIF
		
		// Force refresh for dpad down state second.
		IF (MPGlobalsHud.DisplayInfo.eDpadDownStateCopy[iPlayerNum] != MPGlobalsHud_TitleUpdate.bGamertagsDpadDownState)
			REFRESH_PLAYER_OVERHEAD_DISPLAY(iPlayerNum, playerId)
			MPGlobalsHud.DisplayInfo.eDpadDownStateCopy[iPlayerNum] = MPGlobalsHud_TitleUpdate.bGamertagsDpadDownState
		ENDIF
		
		// On dm.
		IF MPGlobalsHud.DisplayInfo.iOverhead_OnDM_LastFrame[iPlayerNum] <>  IS_ON_DEATHMATCH_GLOBAL_SET()
			REFRESH_PLAYER_OVERHEAD_DISPLAY(iPlayerNum, playerId)
			#IF IS_DEBUG_BUILD
			NET_NL()NET_PRINT("[overheads] RUN_OVERALL_FREEMODE_LOGIC_CHECKS: ON DEATHMATCH STATE CHANGED - REFRESH.  ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
			NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))
			#ENDIF
		ENDIF
		MPGlobalsHud.DisplayInfo.iOverhead_OnDM_LastFrame[iPlayerNum] = IS_ON_DEATHMATCH_GLOBAL_SET()
		
		// All overhead.
		IF MPGlobalsHud.DisplayInfo.bOverhead_AllOverheadActive_LastFrame[iPlayerNum] <> IS_SCRIPT_HUD_DISABLED(HUDPART_ALL_OVERHEADS)
			REFRESH_PLAYER_OVERHEAD_DISPLAY(iPlayerNum, playerId)
			#IF IS_DEBUG_BUILD
			NET_NL()NET_PRINT("[overheads] RUN_OVERALL_FREEMODE_LOGIC_CHECKS: HUDPART_ALL_OVERHEADS STATE CHANGED - REFRESH.  ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
			NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))
			#ENDIF
		ENDIF
		MPGlobalsHud.DisplayInfo.bOverhead_AllOverheadActive_LastFrame[iPlayerNum] = IS_SCRIPT_HUD_DISABLED(HUDPART_ALL_OVERHEADS)
		
		// This player overhead.
		IF MPGlobalsHud.DisplayInfo.bOverhead_ThisPlayerOverheadActive_LastFrame[iPlayerNum] <> IS_SCRIPT_HUD_DISABLED(HUDPART_THISPLAYER_OVERHEADS, NATIVE_TO_INT(playerID))
			REFRESH_PLAYER_OVERHEAD_DISPLAY(iPlayerNum, playerId)
			#IF IS_DEBUG_BUILD
			NET_NL()NET_PRINT("[overheads] RUN_OVERALL_FREEMODE_LOGIC_CHECKS: HUDPART_THISPLAYER_OVERHEADS STATE CHANGED - REFRESH.  ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
			NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))
			#ENDIF
		ENDIF
		MPGlobalsHud.DisplayInfo.bOverhead_ThisPlayerOverheadActive_LastFrame[iPlayerNum] = IS_SCRIPT_HUD_DISABLED(HUDPART_THISPLAYER_OVERHEADS, NATIVE_TO_INT(playerID))
		
		// On race.
		IF MPGlobalsHud.DisplayInfo.iOverhead_OnRace_LastFrame[iPlayerNum] <> IS_ON_RACE_GLOBAL_SET()
			REFRESH_PLAYER_OVERHEAD_DISPLAY(iPlayerNum, playerId)
			#IF IS_DEBUG_BUILD
			NET_NL()NET_PRINT("[overheads] RUN_OVERALL_FREEMODE_LOGIC_CHECKS: ON RACES STATE CHANGED - REFRESH.  ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
			NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))
			#ENDIF

		ENDIF
		MPGlobalsHud.DisplayInfo.iOverhead_OnRace_LastFrame[iPlayerNum] = IS_ON_RACE_GLOBAL_SET()
		
		// On mission.
		IF MPGlobalsHud.DisplayInfo.iOverhead_OnMission_LastFrame[iPlayerNum] <> g_bFM_ON_TEAM_MISSION
			REFRESH_PLAYER_OVERHEAD_DISPLAY(iPlayerNum, playerId)
			#IF IS_DEBUG_BUILD
			NET_NL()NET_PRINT("[overheads] RUN_OVERALL_FREEMODE_LOGIC_CHECKS: ON MISSION STATE CHANGED - REFRESH.  ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
			NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))
			#ENDIF
		ENDIF
		MPGlobalsHud.DisplayInfo.iOverhead_OnMission_LastFrame[iPlayerNum] = g_bFM_ON_TEAM_MISSION
		
		// No overheads during transition from corona to job.
		IF MPGlobalsHud_TitleUpdate.bCoronaToJobTransitionActive != g_b_TransitionActive
			REFRESH_PLAYER_OVERHEAD_DISPLAY(iPlayerNum, playerId)
			#IF IS_DEBUG_BUILD
			NET_NL()NET_PRINT("[overheads] RUN_OVERALL_FREEMODE_LOGIC_CHECKS: g_b_TransitionActive value changed - REFRESH.  ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
			NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))
			#ENDIF
			MPGlobalsHud_TitleUpdate.bCoronaToJobTransitionActive = g_b_TransitionActive
		ENDIF
		
		// No overheads during switch.
		IF (MPGlobalsHud_TitleUpdate.bPlayerSwitchActive != IS_PLAYER_SWITCH_IN_PROGRESS())
			REFRESH_PLAYER_OVERHEAD_DISPLAY(iPlayerNum, playerId)
			MPGlobalsHud_TitleUpdate.bPlayerSwitchActive = IS_PLAYER_SWITCH_IN_PROGRESS()
			#IF IS_DEBUG_BUILD
			NET_NL()NET_PRINT("[overheads] RUN_OVERALL_FREEMODE_LOGIC_CHECKS: bPlayerSwitchActive value changed - REFRESH.  ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
			NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))
			#ENDIF
		ENDIF
		
//		// No arrows if the arrows are turned off.
//		IF (iMarkerDisplay != MARKER_OPTIONS_ARROW_ONLY)	
//			SET_ICON_VISABILITY(iPlayerNum, MP_TAG_ARROW, FALSE)
//		ENDIF
		
		// Players mp blip is being shown or not (not really used just now, but used for icons tied to radar blip visibility).
		IF MPGlobalsHud.DisplayInfo.iOverhead_IsBlipped_LastFrame[iPlayerNum] <> IS_MP_PLAYER_BLIPPED(playerID)
			REFRESH_PLAYER_OVERHEAD_DISPLAY(iPlayerNum, playerId)
			#IF IS_DEBUG_BUILD
			NET_NL()NET_PRINT("[overheads] RUN_OVERALL_FREEMODE_LOGIC_CHECKS: IS BLIPPED STATE CHANGED - REFRESH.  ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
			NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))
			#ENDIF
		ENDIF
		MPGlobalsHud.DisplayInfo.iOverhead_IsBlipped_LastFrame[iPlayerNum] = IS_MP_PLAYER_BLIPPED(playerID)
		
		IF MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.bOnEndRaceScreen[iPlayerNum] != g_b_EndCameraOn
			REFRESH_PLAYER_OVERHEAD_DISPLAY(iPlayerNum, playerId)
			#IF IS_DEBUG_BUILD
			NET_NL()NET_PRINT("[overheads] RUN_OVERALL_FREEMODE_LOGIC_CHECKS: g_b_EndCameraOn has changed.  ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
			NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))
			#ENDIF
		ENDIF
		
		// A mystery setting that does something.
		IF 	MPGlobalsHud.DisplayInfo.iOverhead_inventory_LastFrame[iPlayerNum] <> GlobalplayerBD[NATIVE_TO_INT(playerID)].iMyOverHeadInventory
			REFRESH_OVERHEAD_DISPLAY(playerID)
			#IF IS_DEBUG_BUILD
			NET_NL()NET_PRINT("[overheads] RUN_OVERALL_FREEMODE_LOGIC_CHECKS: IS INVENTORY STATE CHANGED - REFRESH.  ")NET_PRINT_INT(NATIVE_TO_INT(playerID))
			NET_NL()NET_PRINT("		-Player GamerTag = ")NET_PRINT(GET_PLAYER_NAME(playerID))
			#ENDIF
		ENDIF
		MPGlobalsHud.DisplayInfo.iOverhead_inventory_LastFrame[iPlayerNum] = GlobalplayerBD[NATIVE_TO_INT(playerID)].iMyOverHeadInventory
		
		// B* 1614285. 
		SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_NOTHING, TRUE, PLAYER_ID())
		
		IF NOT MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.bPauseMenuActive[iPlayerNum]
			IF IS_PAUSE_MENU_ACTIVE_EX()
				MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.bPauseMenuActive[iPlayerNum] = TRUE
				REFRESH_ALL_OVERHEAD_DISPLAY()
				PRINTLN("[overheads] - REFRESH_ALL_OVERHEAD_DISPLAY call 6")
			ENDIF
		ELSE
			IF NOT IS_PAUSE_MENU_ACTIVE_EX()
				MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.bPauseMenuActive[iPlayerNum] = FALSE
				REFRESH_ALL_OVERHEAD_DISPLAY()
				PRINTLN("[overheads] - REFRESH_ALL_OVERHEAD_DISPLAY call 7")
			ENDIF
		ENDIF
		
		IF GET_HEIST_SUPPRESS_HUD_FOR_PLANNING() != MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.bSuppressHudThisFrameCopy
			REFRESH_ALL_OVERHEAD_DISPLAY()
			PRINTLN("[overheads] - REFRESH_ALL_OVERHEAD_DISPLAY call one bagillion.")
			MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.bSuppressHudThisFrameCopy = GET_HEIST_SUPPRESS_HUD_FOR_PLANNING()
		ENDIF
		
		MAINTAIN_FIRST_TIME_ON_SCREEN_FLASHING()
		
		// Must be in same interior to see overhead.
		IF NOT IS_LOCAL_PLAYER_IN_ARENA_SPECTATOR_BOX()
			IF NOT MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.bNotInSameInterior[iPlayerNum]
				IF GET_INTERIOR_FROM_ENTITY(PlayerPedId) != GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID())
					MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.bNotInSameInterior[iPlayerNum] = TRUE
					REFRESH_ALL_OVERHEAD_DISPLAY()
					PRINTLN("[overheads] - REFRESH_ALL_OVERHEAD_DISPLAY call 8")
				ENDIF
			ELSE
				IF GET_INTERIOR_FROM_ENTITY(PlayerPedId) = GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID())
					MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.bNotInSameInterior[iPlayerNum] = FALSE
					REFRESH_ALL_OVERHEAD_DISPLAY()
					PRINTLN("[overheads] - REFRESH_ALL_OVERHEAD_DISPLAY call 9")
				ELSE
					SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_NOTHING, TRUE, playerID)
					#IF IS_DEBUG_BUILD
					IF g_TurnOnOverheadDebug
						PRINTLN("[overheads] - DISPLAYOVERHEADS_DISPLAY_NOTHING call 10 TRUE")
					ENDIF
					#ENDIF
				ENDIF
			ENDIF
		ENDIF
		IF NOT MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.bOnCelebrationScreen
			IF g_bCelebrationScreenIsActive
				MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.bOnCelebrationScreen = TRUE
				REFRESH_ALL_OVERHEAD_DISPLAY()
				PRINTLN("[overheads] - REFRESH_ALL_OVERHEAD_DISPLAY call 10")
			ENDIF
		ELSE
			IF NOT g_bCelebrationScreenIsActive
				MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.bOnCelebrationScreen = FALSE
				REFRESH_ALL_OVERHEAD_DISPLAY()
				PRINTLN("[overheads] - REFRESH_ALL_OVERHEAD_DISPLAY call 11")
			ENDIF
		ENDIF
		
		IF NOT MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.bOnLeaderboard
			IF g_b_OnLeaderboard
				MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.bOnLeaderboard = TRUE
				REFRESH_ALL_OVERHEAD_DISPLAY()
				PRINTLN("[overheads] - REFRESH_ALL_OVERHEAD_DISPLAY call 12")
			ENDIF
		ELSE
			IF NOT g_b_OnLeaderboard
				MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.bOnLeaderboard = FALSE
				REFRESH_ALL_OVERHEAD_DISPLAY()
				PRINTLN("[overheads] - REFRESH_ALL_OVERHEAD_DISPLAY call 13")
			ENDIF
		ENDIF
		
		IF MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.bOnJobIntro[0] != MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.bOnJobIntro[1]
			REFRESH_ALL_OVERHEAD_DISPLAY()
			MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.bOnJobIntro[1] = MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.bOnJobIntro[0]
			PRINTLN("[overheads] - REFRESH_ALL_OVERHEAD_DISPLAY - bOnJobIntro. MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.bOnJobIntro[0] = ", MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.bOnJobIntro[0])
		ENDIF
		
		IF IS_MP_GAMER_TAG_ACTIVE(iPlayerNum)
			
			IF NOT MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.bOnImpromptuDm
				IF IS_ON_IMPROMPTU_DEATHMATCH_GLOBAL_SET()
					FOR  iTag = 0 TO (COUNT_OF(eMP_TAG)-1)
						IF iTag != (ENUM_TO_INT(MP_TAG_CREW_TAG))
						AND IS_VALID_OVERHEAD_ITEM(INT_TO_ENUM(eMP_TAG, iTag))
							HANDLE_COLOUR_FOR_IMPROMPTU_DEATHMATCH(playerId, iPlayerNum, INT_TO_ENUM(eMP_TAG, iTag))
						ENDIF
					ENDFOR
					MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.bOnImpromptuDm = TRUE
				ENDIF
			ELSE
				IF NOT IS_ON_IMPROMPTU_DEATHMATCH_GLOBAL_SET()
					FOR  iTag = 0 TO (COUNT_OF(eMP_TAG)-1)
						IF iTag != (ENUM_TO_INT(MP_TAG_CREW_TAG))
						AND IS_VALID_OVERHEAD_ITEM(INT_TO_ENUM(eMP_TAG, iTag))
							HANDLE_COLOUR_FOR_IMPROMPTU_DEATHMATCH(playerId, iPlayerNum, INT_TO_ENUM(eMP_TAG, iTag))
						ENDIF
					ENDFOR
					MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.bOnImpromptuDm = FALSE
				ENDIF
			ENDIF
			
			// Make spectated player have friendly overhead. 
			PED_INDEX specPed = GET_SPECTATOR_CURRENT_FOCUS_PED()
		
			IF DOES_ENTITY_EXIST(specPed)
				IF NOT IS_ENTITY_DEAD(specPed)
					IF IS_PED_A_PLAYER(specPed)
						IF MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.piSpectatorTarget != specPed
							FOR  iTag = 0 TO (COUNT_OF(eMP_TAG)-1)
								IF iTag != (ENUM_TO_INT(MP_TAG_CREW_TAG))
								AND IS_VALID_OVERHEAD_ITEM(INT_TO_ENUM(eMP_TAG, iTag))
									HANDLE_SPECTATOR_TARGET_OVERHEAD_COLOUR(playerId, iPlayerNum, INT_TO_ENUM(eMP_TAG, iTag), specPed)
								ENDIF
							ENDFOR
							MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.piSpectatorTarget = specPed
						ENDIF
					ELSE
						MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.piSpectatorTarget = specPed
					ENDIF
				ELSE
					MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.piSpectatorTarget = specPed
				ENDIF
			ELSE
				MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.piSpectatorTarget = specPed
			ENDIF
			
			#IF IS_DEBUG_BUILD
			IF g_TurnOnOverheadDebug
				PRINTLN("[overheads] - calling MAINTAIN_HEIST_OH_COLOUR at point 3")
			ENDIF
			#ENDIf
			MAINTAIN_HEIST_OH_COLOUR(playerId, iPlayerNum)
			
			// B* 2376934 - Can we look at an option to have a team's colours appear the same to every other team?
			IF IS_THIS_A_MISSION()
			AND MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.bTeamColourOverride
				
				INT iTeam = GET_PLAYER_TEAM(playerId)
				
				IF iTeam >= 0 AND iTeam < FMMC_MAX_TEAMS
				AND g_FMMC_STRUCT.iTeamColourOverride[iTeam] != -1
					
					FOR iTag = 0 TO (COUNT_OF(eMP_TAG)-1)
						IF iTag != (ENUM_TO_INT(MP_TAG_CREW_TAG))
						AND IS_VALID_OVERHEAD_ITEM(INT_TO_ENUM(eMP_TAG, iTag))
							
							HUD_COLOURS hudColour = GET_HUD_COLOUR_FOR_FMMC_TEAM(iTeam, player_Id(), FALSE)
							
							
							IF INT_TO_ENUM(eMP_TAG, iTag) = MP_TAG_HEALTH_BAR
								SET_MP_GAMER_TAG_HEALTH_BAR_COLOUR(iPlayernum, hudColour)
								SET_MP_GAMER_TAG_ALPHA(iPlayerNum,MP_TAG_HEALTH_BAR, 255 )
							ELSE
								SET_MP_GAMER_TAG_COLOUR(iPlayernum, INT_TO_ENUM(eMP_TAG, iTag), hudColour)
//								PRINTLN("[overheads][teamcolouroverride] - set overhead for player ", iPlayernum, " to HUD colour override ",g_FMMC_STRUCT.iTeamColourOverride[iTeam]," for team.")
							ENDIF
						ENDIF
					ENDFOR
					
				ENDIF
				
				IF NOT IS_BIT_SET(MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.iTeamColourOverride_OverheadsUpdatedBS, iPlayerNum)
					SET_BIT(MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.iTeamColourOverride_OverheadsUpdatedBS, iPlayerNum)
				ENDIF
					
			ELSE
				INT iplayerteam = GET_PLAYER_TEAM(playerId)
				INT iPlayerhudColour

				
				IF IS_PLAYER_USING_ARENA()
				AND NOT IS_ARENA_WAR_UGC_RACE()
				AND (iplayerteam >= 0 AND iplayerteam < FMMC_MAX_TEAMS)
					iPlayerhudColour = ENUM_TO_INT(GET_HUD_COLOUR_FOR_FMMC_TEAM(iplayerteam, player_id(), FALSE))
					
				ELSE
					iPlayerhudColour = ENUM_TO_INT(GET_PLAYER_HUD_COLOUR(playerID, iplayerteam))
				ENDIF
				
				
				
				IF IS_BIT_SET(MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.iTeamColourOverride_OverheadsUpdatedBS, iPlayerNum)
				OR MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.iPlayerTeam[iPlayerNum] != iplayerteam
				OR MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.iSavedOhColour[iPlayerNum] != iPlayerhudColour
					
					FOR iTag = 0 TO (COUNT_OF(eMP_TAG)-1)
						IF iTag != (ENUM_TO_INT(MP_TAG_CREW_TAG))
						AND IS_VALID_OVERHEAD_ITEM(INT_TO_ENUM(eMP_TAG, iTag))
							IF INT_TO_ENUM(eMP_TAG, iTag) = MP_TAG_HEALTH_BAR
								SET_MP_GAMER_TAG_HEALTH_BAR_COLOUR(iPlayernum, INT_TO_ENUM(HUD_COLOURS, iPlayerhudColour))
								SET_MP_GAMER_TAG_ALPHA(iPlayerNum,MP_TAG_HEALTH_BAR, 255 )	
								PRINTLN("[overheads][teamcolouroverride] - set over head health bar for player ", iPlayernum, ", back to HUD colour for team.")
							ELSE
								SET_MP_GAMER_TAG_COLOUR(iPlayernum, INT_TO_ENUM(eMP_TAG, iTag), INT_TO_ENUM(HUD_COLOURS, iPlayerhudColour))
								PRINTLN("[overheads][teamcolouroverride] - set over head for player ", iPlayernum, ", back to HUD colour for team.")
							ENDIF
						ENDIF
					ENDFOR
					
					IF IS_BIT_SET(MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.iTeamColourOverride_OverheadsUpdatedBS, iPlayerNum)
						CLEAR_BIT(MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.iTeamColourOverride_OverheadsUpdatedBS, iPlayerNum)
						PRINTLN("[overheads] - iTeamColourOverride_OverheadsUpdatedBS for player ,", iPlayerNum, " is set, clearing.")
					ENDIF
					
					IF MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.iPlayerTeam[iPlayerNum] != iplayerteam
						MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.iPlayerTeam[iPlayerNum] = iplayerteam
						PRINTLN("[overheads] - DisplayInfo_TitleUpdate.iPlayerTeam[", iPlayerNum, "] != GET_PLAYER_TEAM, setting equal.")
					ENDIF
					
					IF MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.iSavedOhColour[iPlayerNum] != iPlayerhudColour
						MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.iSavedOhColour[iPlayerNum] = iPlayerhudColour
						PRINTLN("[overheads] - DisplayInfo_TitleUpdate.iSavedOhColour[", iPlayerNum, "] != iPlayerhudColour, setting equal.")
					ENDIF
					
				ENDIF
				
			ENDIF
			
		ENDIF
		
		// Block overheads during exit apartment cutscene. 
		IF g_bGamerTagBlockerCopy != g_bGamerTagBlocker
			REFRESH_ALL_OVERHEAD_DISPLAY()
			g_bGamerTagBlockerCopy = g_bGamerTagBlocker
			PRINTLN("[overheads] - REFRESH_ALL_OVERHEAD_DISPLAY call 76")
		ENDIF
		
		// Veh deamthmatch uses vehicle health. 
		IF IS_MP_GAMER_TAG_ACTIVE(iPlayerNum)
		AND NOT IS_ENTITY_DEAD( GET_PLAYER_PED(playerId))
		AND NOT g_bDisable_vehicle_health_bar
			IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(playerId)
				IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciEXPLODE_VEHICLE_ON_ZERO_HEALTH)
					IF SHOULD_USE_VEHICLE_HEALTH_BAR(playerId)
					#IF IS_DEBUG_BUILD
					OR GET_COMMANDLINE_PARAM_EXISTS("enablevehiclehealthoverhead")				
					#ENDIF	
						IF IS_MP_GAMER_TAG_ACTIVE(iPlayerNum)
							SET_MP_GAMER_TAGS_SHOULD_USE_VEHICLE_HEALTH(iPlayerNum, TRUE )
							#IF IS_DEBUG_BUILD
							IF g_TurnOnOverheadDebug
								PRINTLN("[Overheads] - SET_MP_GAMER_TAGS_SHOULD_USE_VEHICLE_HEALTH(", iPlayerNum, ") = TRUE")
							ENDIF
							#ENDIF
						ENDIF
					ELSE
						IF IS_MP_GAMER_TAG_ACTIVE(iPlayerNum)
							SET_MP_GAMER_TAGS_SHOULD_USE_VEHICLE_HEALTH(iPlayerNum, FALSE )
							
							#IF IS_DEBUG_BUILD
							IF g_TurnOnOverheadDebug
								PRINTLN("[Overheads] - SET_MP_GAMER_TAGS_SHOULD_USE_VEHICLE_HEALTH(", iPlayerNum, ") = FALSE")
							ENDIF
							#ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		// Refresh if overhead override state changes.
		IF (MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.eOverheadOverrideState[0] != MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.eOverheadOverrideState[1])
			MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.eOverheadOverrideState[1] = MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.eOverheadOverrideState[0]
			
			PRINTLN("[overheads] - REFRESH_ALL_OVERHEAD_DISPLAY call 14")
		ENDIF
		
		//IF NOT IS_BIT_SET(MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.iHideHealthBarBitset[0], iPlayerNum)
		IF IS_BIT_SET(MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.iHideHealthBarsBitset[0], iPlayerNum)
			IF NOT IS_BIT_SET(MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.iHideHealthBarsBitset[1], iPlayerNum)
				SET_BIT(MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.iHideHealthBarsBitset[1], iPlayerNum)
				REFRESH_ALL_OVERHEAD_DISPLAY()
			ENDIF
		ELSE
			IF IS_BIT_SET(MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.iHideHealthBarsBitset[1], iPlayerNum)
				CLEAR_BIT(MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.iHideHealthBarsBitset[1], iPlayerNum)
				REFRESH_ALL_OVERHEAD_DISPLAY()
			ENDIF
		ENDIF
			
	ENDIF
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	CLOSE_SCRIPT_PROFILE_MARKER_GROUP()
	#ENDIF
	#ENDIF


ENDPROC



///////////////////////////////////////////////////////////////////////////////
///    
///    Process FM, DM & RC overhead displays
///    
PROC PROCESS_PLAYER_NAME_DISPLAY_FM_DM_RC(PLAYER_INDEX playerID, INT iPlayernum) //, BOOL bInDeathmatch = FALSE)
	
	#IF IS_DEBUG_BUILD
	bInChatWindow = IS_MP_TEXT_CHAT_TYPING()
	#ENDIF
	
	IF (HAS_OVERHEAD_ACTIVATION_CHANGED(playerID)
	OR HAS_OVERHEAD_EVENT_CHANGED(playerID)
	OR HAS_OVERHEAD_LOGIC_CHANGED(playerID))
		IF IS_MP_GAMER_TAG_MOVIE_ACTIVE()
			IF NOT IS_UPDATING_MP_GAMER_TAG_NAME_AND_CREW_DETAILS(iPlayernum)	
			
				IF CREATE_PLAYER_OVERHEADS(playerID, iPlayerNum) //, iPlayerTeam) 
					
					////////////////////////////////////////////////
					/// Call the proc that runs the common functions
					PLAYERS_OVERHEAD_COMMON_FUNCTIONS(playerID , iPlayerNum)
					
					UPDATE_OVERHEAD_LAST_FRAME( playerID)
					UPDATE_OVERHEAD_EVENT_LAST_FRAME(PlayerID)
					UPDATE_OVERHEAD_LOGIC_LAST_FRAME(PlayerID)
					
					MAINTAIN_OVERHEAD_ALPHA(iPlayerNum, playerID)
					
					////////////////////////////////////////////////
					/// INVENTORY
					////////////////////////////////////////////////
					
			//		IF IS_OVERHEAD_EVENT_ACTIVE(DISPLAYOVERHEADS_EVENTS_REFRESH_INVENTORY, playerID)
			//			
			//			IF IS_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_INVENTORY, playerID)
			//	
			//			////////////////////////////////////////////////
			//			/// A Package
			//				IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(playerID)].iMyOverHeadInventory, ENUM_TO_INT(MP_TAG_PACKAGES))
			//					NET_NL()NET_PRINT("SET_ICON_VISABILITY MP_TAG_PACKAGES = TRUE")NET_PRINT_INT(iPlayerNum)
			//					SET_ICON_VISABILITY(iPlayerNum, MP_TAG_PACKAGES, TRUE)
			//				ELSE
			//					NET_NL()NET_PRINT("SET_ICON_VISABILITY MP_TAG_PACKAGES = FALSE")NET_PRINT_INT(iPlayerNum)
			//					SET_ICON_VISABILITY(iPlayerNum, MP_TAG_PACKAGES, FALSE)
			//				ENDIF	
			//			ELSE
			//								
			//				SET_ICON_VISABILITY(iPlayerNum, MP_TAG_PACKAGES, FALSE)
			//			ENDIF
			//					
			//			SET_OVERHEAD_EVENT_ACTIVE(DISPLAYOVERHEADS_EVENTS_REFRESH_INVENTORY,FALSE, playerID)		
			//			
			//		ENDIF
		
				ENDIF
				

				
			ELSE
				PRINTLN("[overheads] - IS_UPDATING_MP_GAMER_TAG_NAME_AND_CREW_DETAILS = TRUE")
			ENDIF
		ELSE
			//PRINTLN("[overheads] - IS_MP_GAMER_TAG_MOVIE_ACTIVE = TRUE")
		ENDIF
		
	ENDIF
	
	IF g_sMPTunables.bdisableflashinglargepackagesinch = FALSE
		PROCESS_HEAD_LARGE_PACKAGE_FLASH(iPlayerNum)
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF g_bDoOverheadsPassivePrints
			PRINTLN("[Overheads] - [Passive Debug] - ************************************************")
			PRINTLN("[Overheads] - [Passive Debug] -  checking player ", iPlayernum)
			PRINTLN("[Overheads] - [Passive Debug] - ************************************************")
			IF IS_BIT_SET(MPGlobalsHud.DisplayInfo.iOverheadEVENTBitset[GET_WEAPON_BITSET(ENUM_TO_INT(DISPLAYOVERHEADS_EVENTS_IS_PLAYER_PASSIVE))][iPlayernum], GET_WEAPON_INDEX_BITSET(ENUM_TO_INT(DISPLAYOVERHEADS_EVENTS_IS_PLAYER_PASSIVE)))
				PRINTLN("[Overheads] - [Passive Debug] - DISPLAYOVERHEADS_EVENTS_IS_PLAYER_PASSIVE = TRUE", iPlayernum)
			ELSE
				PRINTLN("[Overheads] - [Passive Debug] - DISPLAYOVERHEADS_EVENTS_IS_PLAYER_PASSIVE = FALSE", iPlayernum)
			ENDIF
		ENDIF
		IF g_bDoArrowCheckDebug
			PRINTLN("[Overheads] - [Arrow Debug] - ************************************************")
			PRINTLN("[Overheads] - [Arrow Debug] -  checking player ", iPlayernum)
			PRINTLN("[Overheads] - [Arrow Debug] - ************************************************")
			SWITCH GET_DPADDOWN_ACTIVATION_STATE()
				CASE DPADDOWN_NONE		PRINTLN("[Overheads] - [Arrow Debug] - GET_DPADDOWN_ACTIVATION_STATE() = DPADDOWN_NONE", iPlayernum) BREAK
				CASE DPADDOWN_FIRST		PRINTLN("[Overheads] - [Arrow Debug] - GET_DPADDOWN_ACTIVATION_STATE() = DPADDOWN_FIRST", iPlayernum) BREAK
				CASE DPADDOWN_SECOND	PRINTLN("[Overheads] - [Arrow Debug] - GET_DPADDOWN_ACTIVATION_STATE() = DPADDOWN_SECOND", iPlayernum) BREAK
			ENDSWITCH
			
			SWITCH MPGlobalsHud_TitleUpdate.bGamertagsDpadDownState
				CASE DPADDOWN_NONE		PRINTLN("[Overheads] - [Arrow Debug] - MPGlobalsHud_TitleUpdate.bGamertagsDpadDownState = DPADDOWN_NONE", iPlayernum) BREAK
				CASE DPADDOWN_FIRST		PRINTLN("[Overheads] - [Arrow Debug] - MPGlobalsHud_TitleUpdate.bGamertagsDpadDownState = DPADDOWN_FIRST", iPlayernum) BREAK
				CASE DPADDOWN_SECOND	PRINTLN("[Overheads] - [Arrow Debug] - MPGlobalsHud_TitleUpdate.bGamertagsDpadDownState = DPADDOWN_SECOND", iPlayernum) BREAK
			ENDSWITCH
			IF HAS_NET_TIMER_STARTED(MPGlobalsHud.iOnMissionOverlayTimer)
				PRINTLN("[Overheads] - [Arrow Debug] - iOnMissionOverlayTimer has started.", iPlayernum) 
			ELSE
				PRINTLN("[Overheads] - [Arrow Debug] - iOnMissionOverlayTimer has not started.", iPlayernum) 
			ENDIF
			IF HAS_NET_TIMER_EXPIRED(MPGlobalsHud.iOnMissionOverlayTimer, DPAD_DOWN_DISPLAY_TIME)
				PRINTLN("[Overheads] - [Arrow Debug] - iOnMissionOverlayTimer has expired.", iPlayernum) 
			ELSE
				PRINTLN("[Overheads] - [Arrow Debug] - iOnMissionOverlayTimer has not expired.", iPlayernum) 
			ENDIF
			IF IS_DPADDOWN_DISABLED_THIS_FRAME()
				PRINTLN("[Overheads] - [Arrow Debug] - IS_DPADDOWN_DISABLED_THIS_FRAME() = TRUE", iPlayernum)
			ELSE
				PRINTLN("[Overheads] - [Arrow Debug] - IS_DPADDOWN_DISABLED_THIS_FRAME() = TRUE", iPlayernum)
			ENDIF
			IF IS_BIT_SET(g_sOverHead.iOverHeadItemDisplayed[GET_WEAPON_BITSET(ENUM_TO_INT(MP_TAG_ARROW))][iPlayernum], GET_WEAPON_INDEX_BITSET(ENUM_TO_INT(MP_TAG_ARROW)))
				PRINTLN("[Overheads] - [Arrow Debug] - iOverHeadItemDisplayed for MP_TAG_ARROW = TRUE", iPlayernum)
			ELSE
				PRINTLN("[Overheads] - [Arrow Debug] - iOverHeadItemDisplayed for MP_TAG_ARROW = FALSE", iPlayernum)
			ENDIF
			IF IS_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_NOTHING, playerId)
				PRINTLN("[Overheads] - [Arrow Debug] - IS_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_NOTHING) = TRUE", iPlayernum)
			ELSE
				PRINTLN("[Overheads] - [Arrow Debug] - IS_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_NOTHING) = FALSE", iPlayernum)
			ENDIF
			IF NOT IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_ARROW, playerId)
				PRINTLN("[Overheads] - [Arrow Debug] - IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_DISPLAY_NOTHING) = FALSE", iPlayernum)
			ELSE
				PRINTLN("[Overheads] - [Arrow Debug] - IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_DISPLAY_NOTHING) = TRUE", iPlayernum)
			ENDIF
			IF IS_OVERHEAD_EVENT_ACTIVE(DISPLAYOVERHEADS_EVENTS_IS_PLAYER_SPECTATING, PLAYER_ID())	
				PRINTLN("[Overheads] - [Arrow Debug] - IS_OVERHEAD_EVENT_ACTIVE(DISPLAYOVERHEADS_EVENTS_IS_PLAYER_SPECTATING, PLAYER_ID())	 = TRUE", iPlayernum)
			ELSE
				PRINTLN("[Overheads] - [Arrow Debug] - IS_OVERHEAD_EVENT_ACTIVE(DISPLAYOVERHEADS_EVENTS_IS_PLAYER_SPECTATING, PLAYER_ID())	 = FALSE", iPlayernum)
			ENDIF	
			IF IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_LOCAL_DPADDOWNACTIVE, PLAYER_ID())
				PRINTLN("[Overheads] - [Arrow Debug] - IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_LOCAL_DPADDOWNACTIVE, PLAYER_ID())	 = TRUE", iPlayernum)
			ELSE
				PRINTLN("[Overheads] - [Arrow Debug] - IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_IS_LOCAL_DPADDOWNACTIVE, PLAYER_ID())	 = FALSE", iPlayernum)
			ENDIF	
			IF IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_JUST_APPEARED_ON_SCREEN, PLAYER_ID())
				PRINTLN("[Overheads] - [Arrow Debug] - IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_JUST_APPEARED_ON_SCREEN, PLAYER_ID())	 = TRUE", iPlayernum)
			ELSE
				PRINTLN("[Overheads] - [Arrow Debug] - IS_OVERHEAD_LOGIC_ACTIVE(DISPLAYOVERHEADS_LOGIC_JUST_APPEARED_ON_SCREEN, PLAYER_ID())	 = FALSE", iPlayernum)
			ENDIF	
			
		ENDIF
	#ENDIF
	
ENDPROC



















