



USING "net_include.sch"
USING "Screen_Placements.sch"
USING "Transition_Controller.sch"
USING "PM_MissionCreator_Public.sch"
USING "Transition_Saving.sch"
USING "MP_SkyCam.sch"
USING "mp_Scaleform_Loading_icon.sch"
USING "net_hud_colour_controller.sch"
USING "menu_cursor_public.sch"

CONST_INT DPAD_INIT									0
CONST_INT DPAD_POPULATE 							1
CONST_INT DPAD_DRAW 								2

// Player talking states
CONST_INT ciVOICE_CHAT_NULL							0
CONST_INT ciVOICE_CHAT_MUTED						1
CONST_INT ciVOICE_CHAT_TALKING						2
CONST_INT ciVOICE_CHAT_NOT_TALKING					3

// (dpadVars.iDpadBitSet)
CONST_INT ciDPAD_INITIALISE 						0
CONST_INT ciDPAD_LAMARS_ROW							1
CONST_INT ciDPAD_IS_TEAM							2
CONST_INT ciDPAD_LAMAR_FINISHED						3
CONST_INT ciDPAD_FINISHED							4
CONST_INT ciDPAD_IS_FRIEND							5
CONST_INT ciDPAD_IS_CREW							6
CONST_INT ciDPAD_IS_UPDATED							7
CONST_INT ciDPAD_PLAYER_DEAD						8
CONST_INT ciDPAD_HIDE_SPLIT_TIME					9  // Set this to TRUE for the local player and his race team-mate
CONST_INT ciDPAD_ROW_HIGHLIGHT						10
CONST_INT ciDPAD_DISPLAY_VIEW_DONE_ONCE				11
CONST_INT ciDPAD_IS_A_HARD_TARGET					12
CONST_INT ciDPAD_SPEC_EYE							13

CONST_FLOAT SDPAD_X 								0.122
CONST_FLOAT	SDPAD_Y 								0.300
CONST_FLOAT SDPAD_H 								0.6  // 0.03055536
CONST_FLOAT SDPAD_W 								0.28 // careful changing these values as the x and y are linked

CONST_INT DPAD_DOWN_TIMER 							10000

STRUCT SCALEFORM_TABS_CLAN_DATA
	BOOL bIsMember
	BOOL bIsPrivate
	BOOL bIsRockstar
	STRING sCrewTag
	INT iCrewRank
ENDSTRUCT

STRUCT SCALEFORM_TABS_PLAYER_DATA
	TEXT_LABEL_63 PlayerFaceTextureName
	TEXT_LABEL_63 PlayerFaceTextureDict
	STRING GangTextureName
	STRING GangTextureDict
	STRING ClanFullName
	STRING PlayerGamertag
ENDSTRUCT

CONST_INT SELECTION_INCREMENT_TIMER_DELAY	300

//CONST_INT MAX_SCALEFORM_TABS 5
STRUCT SCALEFORM_TABS
	BOOL bInitialised
	STRING TabsHeader
	SCALEFORM_TABS_CLAN_DATA CrewData
	SCALEFORM_TABS_PLAYER_DATA PlayerData
	PED_INDEX HeadshotPed
	PEDHEADSHOT_ID HeadshotPic

	
	SCALEFORM_INPUT_EVENT ScaleformInputEventNumber
	INT iHighlightTab
	BOOL InitialiseScaleformFaces
	BOOL bRefreshScaleformFaces


	
	BOOL HasScreenChangedFaceCheck
	PED_INDEX PedHeadshotIndexOld
	BOOL PedHeadShotOldScreen
	BOOL TeamShotOldScreen

	TEXT_LABEL_63 OldGangIcon_TextureName
	TEXT_LABEL_63 OldGangIcon_TextureDict

ENDSTRUCT

CONST_INT MAX_SCALEFORM_SOCIALCLUB_ITEMS 3
STRUCT SCALEFORM_SOCIALCLUB
	BOOL bInitialised
	
	BOOL bHasValue[MAX_SCALEFORM_SOCIALCLUB_ITEMS]
	
	INT iParam1[MAX_SCALEFORM_SOCIALCLUB_ITEMS] 
	INT iParam2[MAX_SCALEFORM_SOCIALCLUB_ITEMS] 
	INT iParam3[MAX_SCALEFORM_SOCIALCLUB_ITEMS]
	TEXT_LABEL_63 sParam[MAX_SCALEFORM_SOCIALCLUB_ITEMS]
	
	BOOL bRefreshScaleformLoading
ENDSTRUCT


STRUCT SCALEFORM_ALERTSCREEN
	BOOL bInitialised

	TEXT_LABEL_63 sWarningMessage
	TEXT_LABEL_63 sSubWarningMessage
	TEXT_LABEL_63 sAlertTextureDict
	TEXT_LABEL_63 sAlertTextureName
	INT iTextureTeam
	INT iTextureR
	INT iTextureG
	INT iTextureB
	PED_INDEX aPed
	PEDHEADSHOT_ID HeadshotPic
	BOOL bRefreshScaleformLoading

ENDSTRUCT

STRUCT SCALEFORM_RANKUP_ANIMATION
	BOOL bInitialised
	INT iTeam
	INT iRankNumber
	BOOL bRefreshInstructionalButtons
ENDSTRUCT





STRUCT SCALEFORM_PLAYER_CARD
	BOOL bInitialisedVisuals
	
	BOOL bHasValue
	
	INT iTeam
	STRING iTeamName
	STRING GamerTag
	STRING CrewTag
	INT iRank
	STRING RankTitle
	INT iXP
	FLOAT iKDRatio
	FLOAT iDrivingSkill
	FLOAT iPassengerSkill
	FLOAT iBadCopSkill
	FLOAT iReliabilitySkill
	FLOAT iShootingSkill
	FLOAT iHackingSkill
	FLOAT iArrestingSkill
	
	INT iMissionType
	
	INT KDKills
	INT KDDeath
	
	INT DMWins
	INT DMLosses
	
	INT RaceWins
	INT RaceLosses
	
	INT AccuracyFired
	INT AccuracyHit
	
	STRING sCrewName
	INT iMissionsCreated
	
	BOOL bIsFreemode
	FLOAT ReliabilityLevel[2]
	STRING ReliabilityLevelString
	INT ReliabilityLevelInt
	
	HUD_COLOURS TeamColour

	BOOL bRefreshScaleformVisuals

ENDSTRUCT


CONST_INT MAX_NUM_COMPARISON_PLAYERS 2
STRUCT SCALEFORM_COMPARISON_CARD
	BOOL bInitialisedVisuals
	
	BOOL bHasValue
	
	INT iTeam[MAX_NUM_COMPARISON_PLAYERS]
	STRING GamerTag[MAX_NUM_COMPARISON_PLAYERS]
	STRING RankTitle[MAX_NUM_COMPARISON_PLAYERS]
	INT iRank[MAX_NUM_COMPARISON_PLAYERS]
	FLOAT iKDRatio[MAX_NUM_COMPARISON_PLAYERS]
	FLOAT iDrivingSkill[MAX_NUM_COMPARISON_PLAYERS]
	FLOAT iPassengerSkill[MAX_NUM_COMPARISON_PLAYERS]
	FLOAT iReliabilitySkill[MAX_NUM_COMPARISON_PLAYERS]
	FLOAT iShootingSkill[MAX_NUM_COMPARISON_PLAYERS]
	FLOAT iHackingSkill[MAX_NUM_COMPARISON_PLAYERS]
	
	BOOL bIsPartner

	BOOL bRefreshScaleformVisuals

ENDSTRUCT

/// PURPOSE:
///    Returns an index of which item in a grid is being highlighted by the mouse, and handles the highlight if necessary.
/// PARAMS:
///    iColumns - 
///    iRows - 
///    fItemWidth - 
///    fItemHeight - 
///    fItemXGap - 
///    fItemYGap - 
/// RETURNS:
///    
FUNC INT GET_CURSOR_HIGHLIGHTED_ITEM_IN_GRID_MENU( INT iColumns, INT iRows, FLOAT fXOrigin, FLOAT fYOrigin, FLOAT fItemWidth, FLOAT fItemHeight, FLOAT fItemGap, BOOL bHighlight = TRUE )

	//FLOAT fCursorX = GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_CURSOR_X)
	//FLOAT fCursorY = GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_CURSOR_Y)
		
	//INT iNumItems = iColumns * iRows
	
	INT iX, iY
	
	FLOAT fHighLightX = fXOrigin
	FLOAT FHighlightY = fYOrigin
	
	FLOAT fHighlightWidth = fItemWidth - fItemGap
	FLOAT fHighlightHeight = fItemHeight - fItemGap
	
	IF bHighlight
		// DO NOTHING
	ENDIF
	
	iY = 0
	
	WHILE iY < iRows
	
		iX = 0
		
		WHILE iX < iColumns
		
			fHighLightX = fXOrigin + (iX * fHighlightWidth)
			fHighLightY = fYOrigin + (iY * fHighlightHeight)
			
			FORCE_SCRIPTED_GFX_WHEN_FRONTEND_ACTIVE(TRUE) 
			SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
			SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
			DRAW_RECT_FROM_CORNER(fHighLightX, fHighLightY, fHighlightWidth, fHighlightHeight, 255,0,0,255)
			
			CPRINTLN( DEBUG_AMBIENT, "menu iX: ", iX, " menu iY: ",iY)
					
			++ iX
		
		ENDWHILE
				
		++ iY
		
	ENDWHILE
	
	


	RETURN -1 // Item is not being highlighted

ENDFUNC



/// PURPOSE:
///    Gets the value of decrement/increment items in a menu when moused over
/// PARAMS:
///    bIsCharacterCreator - If it's the character creator, use a different origin.
///    fIncDist - The distance from the right hand edge of the increment start
/// RETURNS:
///    The value of the increment change
///    
///    TODO: Support mouse slider style value changes.
///    
FUNC INT GET_PC_MOUSE_MENU_VALUE_CHANGE( BOOL bIsCharacterCreator = FALSE, FLOAT fIncDist = 0.075  )
		
	VECTOR vPauseMenuOrigin = GET_PAUSE_MENU_POSITION()

	FLOAT fMenuWidth = 0.225
	
	IF bIsCharacterCreator
		fMenuWidth = 0.225
	ENDIF
	

	fMenuWidth = ADJUST_X_COORD_FOR_ASPECT_RATIO(fMenuWidth)

	FLOAT fMenuXMax = vPauseMenuOrigin.x + fMenuWidth
	
	// Only have to worry about x value as this only affects the current menu item.
	FLOAT fMouseX = GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_CURSOR_X)

	// Do this as a proportion of the menu so we handle scaling correctly.
	FLOAT fMouseValueIncrement = fMenuXMax - (fMenuWidth * fIncDist)
	//FLOAT fMouseValueDecrement = fMenuXMax - MOUSE_VALUE_DECREMENT_WIDTH
	
	// Adjust for 4:3
	IF NOT GET_IS_WIDESCREEN()
		//fMenuXOrigin = ((fMenuXOrigin - 0.5) * 1.33) + 0.5
		fMenuWidth = (fMenuWidth * 1.33)
	ENDIF
	
	//SET_TEXT_SCALE(0.8, 0.8)
	//DISPLAY_TEXT_WITH_NUMBER( 0.1, 0.1, "NUMBER", iNumMenuItems )

	//DRAW_RECT_FROM_CORNER(fMouseValueIncrement, fMouseY, fIncDist , 0.01, 0, 255, 0, 255)
	//DRAW_RECT_FROM_CORNER(fMenuXOrigin, fMouseY+0.01, fMenuWidth - fIncDist, 0.01, 0, 0, 255, 255)	
	
//	SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
//	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD)
//	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
//	FLOAT fMouseY = GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_CURSOR_Y)
//	DRAW_DEBUG_LINE_2D( <<vPauseMenuOrigin.x, fMouseY, 0.0>>,<< fMouseValueIncrement, fMouseY, 0.0>>)
//	DRAW_DEBUG_LINE_2D( <<fMouseValueIncrement, fMouseY, 0.0>>,<< fMenuXMax, fMouseY, 0.0>>, 255, 0, 0, 255)
	
	// Check if mouse is in the bounding box for the menu
	IF 	fMouseX >= vPauseMenuOrigin.x		
	AND fMouseX <= fMenuXMax 
	
		// Increment value
		IF fMouseX >= fMouseValueIncrement
			//DISPLAY_TEXT_WITH_NUMBER( 0.1, 0.2, "NUMBER", 1 )
			RETURN 1

		// Decrement value
		ELSE 
			//DISPLAY_TEXT_WITH_NUMBER( 0.1, 0.2, "NUMBER", -1 )
			RETURN -1
		ENDIF
	
	ENDIF
	
	// No value change needed
	//DISPLAY_TEXT_WITH_NUMBER( 0.1, 0.2, "NUMBER", 0 )
	
	RETURN 0

ENDFUNC

/// PURPOSE: 
///    Checks if the mouse cursor is inside the menu bounds
/// PARAMS:
///    iNumMenuItems - Number of items inside the menu
/// RETURNS:
///    TRUE if mouse is inside the menu, FALSE otherwise
///    
FUNC BOOL IS_PC_MOUSE_SELECTING_CURRENT_ITEM(INT iCurrentSelection, INT iValueChange = 0, INT iCurrentColumn = 0)
	
	// This is intended only for the pause menu based UIs
	IF NOT IS_PAUSE_MENU_ACTIVE()
		RETURN FALSE
	ENDIF
	
	CONST_FLOAT MOUSE_VALUE_INCREMENT_WIDTH 0.015
	
	VECTOR vPauseMenuOrigin = GET_PAUSE_MENU_POSITION()

	FLOAT fColumnMinX
	FLOAT fColumnMaxX
	FLOAT fColumnCycleSplitX
	FLOAT fMouseX
	
	SWITCH iCurrentColumn
		CASE 0
			fColumnMinX			= 0.0
			fColumnMaxX			= 0.225
			fColumnCycleSplitX	= fColumnMaxX - MOUSE_VALUE_INCREMENT_WIDTH
		BREAK
		CASE 4
			fColumnMinX			= 0.3875 - 0.1617188
			fColumnMaxX			= 0.8383 - 0.1617188
			fColumnCycleSplitX	= fColumnMaxX - MOUSE_VALUE_INCREMENT_WIDTH
		BREAK
		DEFAULT
			SCRIPT_ASSERT("IS_PC_MOUSE_SELECTING_CURRENT_ITEM() currently only supports columns 0 and 4. Ask Steve R or Sam H if you need another adding.")
			RETURN FALSE
		BREAK
	ENDSWITCH
	
	fColumnMinX			= vPauseMenuOrigin.x + ADJUST_X_COORD_FOR_ASPECT_RATIO(fColumnMinX)
	fColumnMaxX			= vPauseMenuOrigin.x + ADJUST_X_COORD_FOR_ASPECT_RATIO(fColumnMaxX)
	fColumnCycleSplitX	= vPauseMenuOrigin.x + ADJUST_X_COORD_FOR_ASPECT_RATIO(fColumnCycleSplitX)
	
	//DISPLAY_TEXT_WITH_NUMBER(0.1, 0.0, "NUMBER", 0)
	
			
	// Mouse is off the menu
	IF PAUSE_MENU_GET_MOUSE_HOVER_UNIQUE_ID() = -1
	//	DISPLAY_TEXT_WITH_NUMBER(0.1, 0.1, "NUMBER", 1)
		RETURN FALSE
	ENDIF
	
	// Mouse is highlighting a different item
	IF iCurrentSelection !=  PAUSE_MENU_GET_MOUSE_HOVER_UNIQUE_ID()
	//	DISPLAY_TEXT_WITH_NUMBER(0.1, 0.2, "NUMBER", 2)
		RETURN FALSE
	ENDIF
	
	// No value cycle here
	IF iValueChange = 0
	//	DISPLAY_TEXT_WITH_NUMBER(0.1, 0.3, "NUMBER", 3)
		RETURN TRUE
	ENDIF
	
	SET_USE_ADJUSTED_MOUSE_COORDS(TRUE) // For Eyefinity
	
//	DRAW_RECT_FROM_CORNER(vPauseMenuOrigin.x, 0.1, fMenuWidth, 0.8, 255, 0, 0, 255)
//	DRAW_RECT_FROM_CORNER(fMouseValueIncrement, 0.1, ADJUST_X_COORD_FOR_ASPECT_RATIO(MOUSE_VALUE_INCREMENT_WIDTH) , 0.8, 0, 255, 0, 255)
//	DRAW_RECT_FROM_CORNER(vPauseMenuOrigin.x, 0.1, fMenuWidth - ADJUST_X_COORD_FOR_ASPECT_RATIO(MOUSE_VALUE_INCREMENT_WIDTH), 0.8, 0, 0, 255, 255)	
		
	fMouseX = GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_CURSOR_X)
	
	// Check if mouse is in the bounding box for the menu
	IF 	fMouseX >= fColumnMinX
	AND fMouseX <= fColumnMaxX
	
		// Increment value
		IF iValueChange = 1
		
			IF fMouseX >= fColumnCycleSplitX
				//DISPLAY_TEXT_WITH_NUMBER( 0.9, 0.1, "NUMBER", iValueChange )
				RETURN TRUE
			ELSE
				RETURN FALSE
			ENDIF
		
		// Decrement value
		ELSE iValueChange = -1
		
			IF fMouseX < fColumnCycleSplitX
				//DISPLAY_TEXT_WITH_NUMBER( 0.9, 0.1, "NUMBER", iValueChange )
				RETURN TRUE
			ELSE
				RETURN FALSE
			ENDIF
		
		ENDIF
		// No value change needed
		//DISPLAY_TEXT_WITH_NUMBER( 0.9, 0.1, "NUMBER", 99 )
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC


// Wrapper functions for PC inputs. Makes the code simpler and easier to read.

FUNC BOOL IS_PC_MOUSE_ACCEPT_JUST_RELEASED(INT iCurrentMenuItem, INT iValueChange = 0, INT iCurrentColumn = 0)

	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
	
		IF IS_WARNING_MESSAGE_ACTIVE()
		OR NETWORK_TEXT_CHAT_IS_TYPING()
			RETURN FALSE
		ENDIF

	//	DISPLAY_TEXT_WITH_NUMBER( 0.0, 0.0, "NUMBER", -1 )

		IF IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
					
			IF IS_PC_MOUSE_SELECTING_CURRENT_ITEM(iCurrentMenuItem, iValueChange, iCurrentColumn)
				//CPRINTLN( DEBUG_AMBIENT, "*** IS_PC_MOUSE_ACCEPT_JUST_RELEASED TRUE ***")
				RETURN TRUE
			ENDIF
			
		ENDIF
		
	ENDIF
	
	RETURN FALSE

ENDFUNC


FUNC BOOL IS_PC_MOUSE_ACCEPT_JUST_PRESSED(INT iCurrentMenuItem, INT iValueChange = 0, INT iCurrentColumn = 0)

	
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
	
		IF IS_WARNING_MESSAGE_ACTIVE()
		OR NETWORK_TEXT_CHAT_IS_TYPING()
			RETURN FALSE
		ENDIF
		
	//	DISPLAY_TEXT_WITH_NUMBER( 0.0, 0.1, "NUMBER", -2 )
				
		IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
		
			IF IS_PC_MOUSE_SELECTING_CURRENT_ITEM(iCurrentMenuItem, iValueChange, iCurrentColumn)
				//CPRINTLN( DEBUG_AMBIENT, "*** IS_PC_MOUSE_ACCEPT_JUST_PRESSED TRUE ***")
				RETURN TRUE
			ENDIF
					
		ENDIF
		
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC BOOL IS_PC_MOUSE_ACCEPT_PRESSED(INT iCurrentMenuItem, INT iValueChange = 0, INT iCurrentColumn = 0)

	
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
	
		//	DISPLAY_TEXT_WITH_NUMBER( 0.0, 0.3, "NUMBER", -3 )
	
		IF IS_WARNING_MESSAGE_ACTIVE()
		OR NETWORK_TEXT_CHAT_IS_TYPING()
			RETURN FALSE
		ENDIF
		
		IF IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
			IF IS_PC_MOUSE_SELECTING_CURRENT_ITEM(iCurrentMenuItem, iValueChange, iCurrentColumn)
				//CPRINTLN( DEBUG_AMBIENT, "*** IS_PC_MOUSE_ACCEPT_PRESSED TRUE ***")
				RETURN TRUE
			ENDIF
		ENDIF
		
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC BOOL IS_PC_MOUSE_CANCEL_JUST_RELEASED()

	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
	
		IF IS_WARNING_MESSAGE_ACTIVE()
		OR NETWORK_TEXT_CHAT_IS_TYPING()
			RETURN FALSE
		ENDIF
		
		IF IS_LOCAL_PLAYER_IN_ACTIVITY_TUTORIAL_CORONA()  
			RETURN FALSE
		ENDIF
	
		IF IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_CURSOR_CANCEL)
			//CPRINTLN( DEBUG_AMBIENT, "*** IS_PC_MOUSE_CANCEL_JUST_RELEASED TRUE ***")
			RETURN TRUE
		ENDIF
		
	ENDIF
	
	RETURN FALSE

ENDFUNC


FUNC BOOL IS_PC_MOUSE_CANCEL_JUST_PRESSED()

	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
	
		IF IS_WARNING_MESSAGE_ACTIVE()
		OR NETWORK_TEXT_CHAT_IS_TYPING()
			RETURN FALSE
		ENDIF
		
		IF IS_LOCAL_PLAYER_IN_ACTIVITY_TUTORIAL_CORONA()  
			RETURN FALSE
		ENDIF
		
		IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_CANCEL)
			//CPRINTLN( DEBUG_AMBIENT, "*** IS_PC_MOUSE_CANCEL_JUST_PRESSED TRUE ***" )
			RETURN TRUE
		ENDIF
		
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC BOOL IS_PC_MOUSE_CANCEL_PRESSED()

	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
	
		IF IS_WARNING_MESSAGE_ACTIVE()
		OR NETWORK_TEXT_CHAT_IS_TYPING()
			RETURN FALSE
		ENDIF
		
		IF IS_LOCAL_PLAYER_IN_ACTIVITY_TUTORIAL_CORONA()  
			RETURN FALSE
		ENDIF
		
		IF IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_CANCEL)
			//CPRINTLN( DEBUG_AMBIENT, "*** IS_PC_MOUSE_CANCEL_PRESSED TRUE ***" )
			RETURN TRUE
		ENDIF
		
	ENDIF
	
	RETURN FALSE

ENDFUNC


FUNC BOOL CHAT_SAFE_IS_DISABLED_CONTROL_JUST_PRESSED(CONTROL_TYPE ctControls, CONTROL_ACTION caInput )

	IF NETWORK_TEXT_CHAT_IS_TYPING()
		RETURN FALSE
	ENDIF
		
	IF IS_DISABLED_CONTROL_JUST_PRESSED(ctControls, caInput)
		RETURN TRUE		
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC BOOL CHAT_SAFE_IS_DISABLED_CONTROL_JUST_RELEASED(CONTROL_TYPE ctControls, CONTROL_ACTION caInput )

	IF NETWORK_TEXT_CHAT_IS_TYPING()
		RETURN FALSE
	ENDIF
		
	IF IS_DISABLED_CONTROL_JUST_RELEASED(ctControls, caInput)
		RETURN TRUE		
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC BOOL CHAT_SAFE_IS_DISABLED_CONTROL_PRESSED(CONTROL_TYPE ctControls, CONTROL_ACTION caInput )

	IF NETWORK_TEXT_CHAT_IS_TYPING()
		RETURN FALSE
	ENDIF
		
	IF IS_DISABLED_CONTROL_PRESSED(ctControls, caInput)
		RETURN TRUE		
	ENDIF
	
	RETURN FALSE

ENDFUNC


FUNC BOOL ARE_STRINGS_JUST_EQUAL(STRING aString1, STRING aString2)
	
	IF IS_STRING_NULL(aString1)
		RETURN TRUE
	ENDIF
	IF IS_STRING_NULL(aString2)
		RETURN TRUE
	ENDIF
	IF ARE_STRINGS_EQUAL(aString1, aString2)
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC SCALEFORM_TAB_NAVIGATION GET_CUSTOMISEABLE_TAB_FROM_INDEX(INT index)
	SCALEFORM_TAB_NAVIGATION result = SCALEFORM_TAB_MP_CHAR_1
	
	SWITCH Index 
		CASE 0 
			result = SCALEFORM_TAB_MP_CHAR_1
		BREAK
		CASE 1 
			result = SCALEFORM_TAB_MP_CHAR_2
		BREAK
		CASE 2 
			result = SCALEFORM_TAB_MP_CHAR_3
		BREAK
		CASE 3 
			result = SCALEFORM_TAB_MP_CHAR_4
		BREAK
		CASE 4 
			result = SCALEFORM_TAB_MP_CHAR_5
		BREAK
		
		
	ENDSWITCH
	
	RETURN result

ENDFUNC

//BOOL WIDGETDONE
//SPRITE_PLACEMENT DebugWidgetSprite
//
//FUNC SPRITE_PLACEMENT GET_SCALEFORM_TABS_POSITION()
//
//	SPRITE_PLACEMENT aSprite
//	
////	IF WIDGETDONE = FALSE
////		CREATE_A_SPRITE_PLACEMENT_WIDGET(DebugWidgetSprite, "Tabs Header")
////		WIDGETDONE = TRUE
////	ENDIF
//	
//	//Scaleform movie Bottom
//	aSprite.x = 0.5
//	aSprite.y = 0.157
//	aSprite.w = 0.68
//	aSprite.h = 0.114
//	aSprite.r = 255
//	aSprite.g = 255
//	aSprite.b = 255
//	aSprite.a = 200
//	aSprite.fRotation = 0
//	
////	aSprite.x += DebugWidgetSprite.x
////	aSprite.y += DebugWidgetSprite.y
////	aSprite.w += DebugWidgetSprite.w
////	aSprite.h += DebugWidgetSprite.h
////	aSprite.r += DebugWidgetSprite.r
////	aSprite.g += DebugWidgetSprite.g
////	aSprite.b += DebugWidgetSprite.b
////	aSprite.a += DebugWidgetSprite.a
//	
//	RETURN aSprite
//
//ENDFUNC

//PROC RESET_SCALEFORM_TABS(SCALEFORM_TABS& DisplayStruct)
//	SCALEFORM_TABS EmptyStruct
//	DisplayStruct = EmptyStruct
//	DisplayStruct.ScaleformInputEventNumber = SCALEFORM_INPUT_EVENT_INVALID
//ENDPROC




PROC SET_LOADING_ICON_SUBTITLES_OFFSET_SHIFT_THIS_FRAME(INT iRows = 1)
	#IF IS_DEBUG_BUILD
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_TraceSubtitleShiftUp")
			IF g_HasLoadingIconSubtitlesAboveButton != iRows
				DEBUG_PRINTCALLSTACK()
				NET_NL()NET_PRINT("SET_LOADING_ICON_SUBTITLES_OFFSET_SHIFT_THIS_FRAME - called - iRows = ")NET_PRINT_INT(iRows)
			ENDIF	
		ENDIF
	#ENDIF
	g_HasLoadingIconSubtitlesAboveButton = iRows
ENDPROC


PROC RUN_LOADING_ICON_OFFSET_SHIFT()


	FLOAT XPosAboveButtons = 0
	FLOAT YPosAboveButtons = -0.0375

	
	#IF IS_DEBUG_BUILD
	XPosAboveButtons += iXPosLoadingIconAboveButton
	YPosAboveButtons += iYPosLoadingIconAboveButton
	
	#ENDIF
	
	YPosAboveButtons *= g_HasLoadingIconSubtitlesAboveButton
	
	 
	IF g_HasLoadingIconAboveButton_LastFrame <> g_HasLoadingIconSubtitlesAboveButton
	OR g_HasLoadingIconActive_LastFrame <>  BUSYSPINNER_IS_ON()
		NET_NL()NET_PRINT("RUN_LOADING_ICON_OFFSET_SHIFT: -called")
		NET_NL()NET_PRINT("			- g_HasLoadingIconAboveButton = ")NET_PRINT_INT(g_HasLoadingIconSubtitlesAboveButton)
		NET_NL()NET_PRINT("			- g_HasLoadingIconAboveButton_LastFrame = ")NET_PRINT_INT(g_HasLoadingIconAboveButton_LastFrame)
		NET_NL()NET_PRINT("			- IS_HUD_COMPONENT_ACTIVE(NEW_HUD_SAVING_GAME) = ")NET_PRINT_BOOL(IS_HUD_COMPONENT_ACTIVE(NEW_HUD_SAVING_GAME))
		NET_NL()NET_PRINT("			- g_HasLoadingIconActive_LastFrame = ")NET_PRINT_BOOL(g_HasLoadingIconActive_LastFrame)
		
		RESET_NET_TIMER(g_SavingIconPositionTimer)
		g_RunLoadingIconSpam = TRUE
	ENDIF
	
	IF BUSYSPINNER_IS_ON()
		IF g_RunLoadingIconSpam
			
			IF HAS_NET_TIMER_EXPIRED(g_SavingIconPositionTimer, 2000) = FALSE
			
				IF g_HasLoadingIconSubtitlesAboveButton > 0
					SET_HUD_COMPONENT_POSITION(NEW_HUD_SAVING_GAME, XPosAboveButtons, YPosAboveButtons)	
					#IF IS_DEBUG_BUILD
					NET_NL()NET_PRINT("RUN_LOADING_ICON_OFFSET_SHIFT: SET_HUD_COMPONENT_POSITION - called")
					NET_NL()NET_PRINT("				- YPosAboveButtons = ")NET_PRINT_FLOAT(YPosAboveButtons)
					#ENDIF
				ELSE
					
					RESET_HUD_COMPONENT_VALUES(NEW_HUD_SAVING_GAME)
//					#IF IS_DEBUG_BUILD
//					NET_NL()NET_PRINT("RUN_LOADING_ICON_OFFSET_SHIFT: RESET_HUD_COMPONENT_VALUES - called")
//					#ENDIF
				ENDIF
			ELSE
				g_RunLoadingIconSpam = FALSE
				
			ENDIF
				
		ENDIF
	ENDIF

	g_HasLoadingIconActive_LastFrame = BUSYSPINNER_IS_ON()
	g_HasLoadingIconAboveButton_LastFrame = g_HasLoadingIconSubtitlesAboveButton
//	g_HasLoadingIconSubtitlesAboveButton = 0 //Is reset in the function below. 

ENDPROC




PROC RUN_SUBTITLES_OFFSET_SHIFT()


	FLOAT XPosAboveButtons = 0
	FLOAT YPosAboveButtons = -0.0375

	
	#IF IS_DEBUG_BUILD
	XPosAboveButtons += iXPosSubtitlesAboveButton
	YPosAboveButtons += iYPosSubtitlesAboveButton
	
	#ENDIF
	
	YPosAboveButtons *= g_HasLoadingIconSubtitlesAboveButton
	
	

//	NET_NL()NET_PRINT("STOP SHIFTING SUBTITLES g_B_Private_Ignore_SubtitleShiftThisFrame = ")NET_PRINT_BOOL(g_B_Private_Ignore_SubtitleShiftThisFrame)

		 
	IF g_HasSubtitlesAboveButton_LastFrame <> g_HasLoadingIconSubtitlesAboveButton
	OR g_HasSubtitlesActive_LastFrame <> IS_HUD_COMPONENT_ACTIVE(NEW_HUD_SUBTITLE_TEXT)
		NET_NL()NET_PRINT("RUN_SUBTITLES_OFFSET_SHIFT: -called")
		NET_NL()NET_PRINT("			- g_HasSubtitlesAboveButton = ")NET_PRINT_INT(g_HasLoadingIconSubtitlesAboveButton)
		NET_NL()NET_PRINT("			- g_HasSubtitlesAboveButton_LastFrame = ")NET_PRINT_INT(g_HasSubtitlesAboveButton_LastFrame)
		NET_NL()NET_PRINT("			- IS_HUD_COMPONENT_ACTIVE(NEW_HUD_SUBTITLE_TEXT) = ")NET_PRINT_BOOL(IS_HUD_COMPONENT_ACTIVE(NEW_HUD_SUBTITLE_TEXT))
		NET_NL()NET_PRINT("			- g_HasSubtitlesActive_LastFrame = ")NET_PRINT_BOOL(g_HasSubtitlesActive_LastFrame)
		
		RESET_NET_TIMER(g_SubtitlesPositionTimer)
		g_RunSubtitlesSpam = TRUE
	ENDIF
	
	IF IS_HUD_COMPONENT_ACTIVE(NEW_HUD_SUBTITLE_TEXT)
	AND NOT IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP()
//	AND NOT g_B_Private_Ignore_SubtitleShiftThisFrame

		IF g_RunSubtitlesSpam
			
			IF HAS_NET_TIMER_EXPIRED(g_SubtitlesPositionTimer, 500) = FALSE
			
				IF g_HasLoadingIconSubtitlesAboveButton > 0
					SET_HUD_COMPONENT_POSITION(NEW_HUD_SUBTITLE_TEXT, XPosAboveButtons, YPosAboveButtons)	
					#IF IS_DEBUG_BUILD
					NET_NL()NET_PRINT("RUN_SUBTITLES_OFFSET_SHIFT: SET_HUD_COMPONENT_POSITION - called")
					#ENDIF
				ELSE
					
					RESET_HUD_COMPONENT_VALUES(NEW_HUD_SUBTITLE_TEXT)
					#IF IS_DEBUG_BUILD
					NET_NL()NET_PRINT("RUN_SUBTITLES_OFFSET_SHIFT: RESET_HUD_COMPONENT_VALUES - called")
					#ENDIF
				ENDIF
			ELSE
				g_RunSubtitlesSpam = FALSE
				
			ENDIF
				
		ENDIF
	ENDIF 

	g_HasSubtitlesActive_LastFrame = IS_HUD_COMPONENT_ACTIVE(NEW_HUD_SUBTITLE_TEXT)
	g_HasSubtitlesAboveButton_LastFrame = g_HasLoadingIconSubtitlesAboveButton
	SET_LOADING_ICON_SUBTITLES_OFFSET_SHIFT_THIS_FRAME(0)
	
//	g_B_Private_Ignore_SubtitleShiftThisFrame = FALSE

ENDPROC






PROC RUN_OBJECTIVETEXT_OFFSET_SHIFT()

//	#IF IS_DEBUG_BUILD
//	IF g_b_TurnOnStreetnameMOVEMENT = FALSE
//		EXIT
//	ENDIF	
//	#ENDIF

	
	
	FLOAT XPosAboveButtonsObjective = -1
	FLOAT YPosAboveButtonsObjective = -0.0375*g_HasLoadingIconSubtitlesAboveButton
	
	

	BOOL ISBEINGUSED
	
	
	
	IF (g_HasLoadingIconSubtitlesAboveButton > 0)
		ISBEINGUSED = TRUE
	ENDIF
	
	IF g_bMPTVplayerWatchingTV
		ISBEINGUSED = TRUE
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF g_b_TurnOnStreetnameMOVEMENT
			ISBEINGUSED = g_b_StreenameWidgetUse
		ENDIF
	#ENDIF
	
	

	IF g_HasObjectiveTextActive_LastFrame != ISBEINGUSED
	OR g_HasObjectiveTextBusySpinnerActive_LastFrame != g_HasLoadingIconSubtitlesAboveButton
		IF ISBEINGUSED
			g_RunObjectiveTextSpam  = FALSE
		ELSE
			g_RunObjectiveTextSpam  = TRUE
		ENDIF
	ENDIF
	
	
	
	IF ISBEINGUSED 
	
		
		
		IF g_RunObjectiveTextSpam  = FALSE
			SET_HUD_COMPONENT_POSITION(NEW_HUD_SUBTITLE_TEXT, XPosAboveButtonsObjective, YPosAboveButtonsObjective)	

			#IF IS_DEBUG_BUILD
				NET_NL()NET_PRINT("RUN_OBJECTIVETEXT_OFFSET_SHIFT: SET_HUD_COMPONENT_POSITION - called g_HasLoadingIconSubtitlesAboveButton = ")NET_PRINT_INT(g_HasLoadingIconSubtitlesAboveButton)
				NET_PRINT(" g_bMPTVplayerWatchingTV = ")NET_PRINT_BOOL(g_bMPTVplayerWatchingTV)
			#ENDIF
				
			g_RunObjectiveTextSpam  = TRUE
		ENDIF
	ELSE
		IF g_RunObjectiveTextSpam  
			RESET_HUD_COMPONENT_VALUES(NEW_HUD_SUBTITLE_TEXT)
			#IF IS_DEBUG_BUILD
				NET_NL()NET_PRINT("RUN_OBJECTIVETEXT_OFFSET_SHIFT: RESET_HUD_COMPONENT_VALUES - called g_HasLoadingIconSubtitlesAboveButton = ")NET_PRINT_INT(g_HasLoadingIconSubtitlesAboveButton)
				NET_PRINT(" g_bMPTVplayerWatchingTV = ")NET_PRINT_BOOL(g_bMPTVplayerWatchingTV)

			#ENDIF
			g_RunObjectiveTextSpam  = FALSE
		ENDIF
	ENDIF

	g_HasObjectiveTextActive_LastFrame = ISBEINGUSED
	g_HasObjectiveTextBusySpinnerActive_LastFrame = g_HasLoadingIconSubtitlesAboveButton
	SET_LOADING_ICON_SUBTITLES_OFFSET_SHIFT_THIS_FRAME(0)



ENDPROC





PROC RUN_STREETNAMES_OFFSET_SHIFT()

//	#IF IS_DEBUG_BUILD
//	IF g_b_TurnOnStreetnameMOVEMENT = FALSE
//		EXIT
//	ENDIF	
//	#ENDIF

	
	
	FLOAT XPosAboveButtonsStreet = -1
	FLOAT YPosAboveButtonsStreet = (-MPGlobalsScoreHud.TopOfTimers-0.0075)
	
	FLOAT XPosAboveButtonsArea = -1
	FLOAT YPosAboveButtonsArea = (-MPGlobalsScoreHud.TopOfTimers)-0.040-0.0075

	FLOAT XPosAboveButtonsVehicle = -1
	FLOAT YPosAboveButtonsVehicle = (-MPGlobalsScoreHud.TopOfTimers)-0.040-0.040-0.0075
 
	YPosAboveButtonsStreet += -0.036*MPGlobalsScoreHud.bNumberOfInstructionalButtonsRowsUnderHud
	YPosAboveButtonsArea += -0.036*MPGlobalsScoreHud.bNumberOfInstructionalButtonsRowsUnderHud
	YPosAboveButtonsVehicle += -0.036*MPGlobalsScoreHud.bNumberOfInstructionalButtonsRowsUnderHud

	IF BUSYSPINNER_IS_ON()
		YPosAboveButtonsStreet += -0.040
		YPosAboveButtonsArea += -0.040
		YPosAboveButtonsVehicle += -0.040
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF g_B_TurnOffStreetNames
		SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_DISTRICT_NAME)
		SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_STREET_NAME)
		SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_VEHICLE_NAME)
		SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME)
	ENDIF
	#ENDIF
//	
	#IF IS_DEBUG_BUILD
	YPosAboveButtonsVehicle += iXPosStreetNameAboveButton
	YPosAboveButtonsStreet += iYPosStreetNameAboveButton
	#ENDIF
	
	
	
	
//	NET_NL()NET_PRINT("RUN_STREETNAMES_OFFSET_SHIFT: MPGlobalsScoreHud.TopOfTimers = ")NET_PRINT_FLOAT(MPGlobalsScoreHud.TopOfTimers)
//	NET_NL()NET_PRINT("RUN_STREETNAMES_OFFSET_SHIFT: YPosAboveButtonsArea = ")NET_PRINT_FLOAT(YPosAboveButtonsArea)
//	NET_NL()NET_PRINT("RUN_STREETNAMES_OFFSET_SHIFT: YPosAboveButtonsStreet = ")NET_PRINT_FLOAT(YPosAboveButtonsStreet)
//	NET_NL()NET_PRINT("RUN_STREETNAMES_OFFSET_SHIFT: YPosAboveButtonsVehicle = ")NET_PRINT_FLOAT(YPosAboveButtonsVehicle)

	BOOL ISBEINGUSED
	

	IF (MPGlobalsScoreHud.TopOfTimers > 0)
		ISBEINGUSED = TRUE
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF g_b_TurnOnStreetnameMOVEMENT
			ISBEINGUSED = g_b_StreenameWidgetUse
		ENDIF
	#ENDIF
	
	
	IF g_fStreetTimerHeight_LastFrame != MPGlobalsScoreHud.TopOfTimers 
	OR g_bStreetTimerBusySpinner_LastFrame != BUSYSPINNER_IS_ON()
		IF MPGlobalsScoreHud.TopOfTimers  > 0
			g_RunStreetNamesSpam  = FALSE
		ELSE
			g_RunStreetNamesSpam  = TRUE
		ENDIF
	ENDIF
	

	
	
//	NET_NL()NET_PRINT("RUN_STREETNAMES_OFFSET_SHIFT: MPGlobalsScoreHud.isSomethingDisplaying = ")NET_PRINT_BOOL(MPGlobalsScoreHud.isSomethingDisplaying)
//	NET_NL()NET_PRINT("RUN_STREETNAMES_OFFSET_SHIFT: ISBEINGUSED = ")NET_PRINT_BOOL(ISBEINGUSED)
//	NET_NL()NET_PRINT("RUN_STREETNAMES_OFFSET_SHIFT: g_RunStreetNamesSpam = ")NET_PRINT_BOOL(g_RunStreetNamesSpam)

	IF ISBEINGUSED 
		IF g_RunStreetNamesSpam  = FALSE
			SET_HUD_COMPONENT_POSITION(NEW_HUD_STREET_NAME, XPosAboveButtonsStreet, YPosAboveButtonsStreet)	
			SET_HUD_COMPONENT_POSITION(NEW_HUD_VEHICLE_NAME, XPosAboveButtonsVehicle, YPosAboveButtonsVehicle)
			SET_HUD_COMPONENT_POSITION(NEW_HUD_AREA_NAME , XPosAboveButtonsArea, YPosAboveButtonsArea)
			#IF IS_DEBUG_BUILD
//			IF g_b_TurnOnStreetnamePrints
				NET_NL()NET_PRINT("RUN_STREETNAMES_OFFSET_SHIFT: SET_HUD_COMPONENT_POSITION - called MPGlobalsScoreHud.TopOfTimers = ")NET_PRINT_FLOAT(MPGlobalsScoreHud.TopOfTimers)
//			ENDIF
			#ENDIF
				
			g_RunStreetNamesSpam  = TRUE
		ENDIF
	ELSE
		IF g_RunStreetNamesSpam  
			RESET_HUD_COMPONENT_VALUES(NEW_HUD_STREET_NAME)
			RESET_HUD_COMPONENT_VALUES(NEW_HUD_VEHICLE_NAME)
			RESET_HUD_COMPONENT_VALUES(NEW_HUD_AREA_NAME)
			#IF IS_DEBUG_BUILD
//			IF g_b_TurnOnStreetnamePrints
				NET_NL()NET_PRINT("RUN_STREETNAMES_OFFSET_SHIFT: RESET_HUD_COMPONENT_VALUES - called")
//			ENDIF
			#ENDIF
			RESET_NET_TIMER(g_StreetNamePositionTimer)
			g_RunStreetNamesSpam  = FALSE
		ENDIF
	ENDIF


	g_fStreetTimerHeight_LastFrame = MPGlobalsScoreHud.TopOfTimers
	g_bStreetTimerBusySpinner_LastFrame = BUSYSPINNER_IS_ON()
	
//	g_B_ShowingStreetName_LastFrame = IS_HUD_COMPONENT_ACTIVE(NEW_HUD_STREET_NAME)
//	g_B_ShowingAreaName_LastFrame = IS_HUD_COMPONENT_ACTIVE(NEW_HUD_AREA_NAME)
//	g_B_ShowingVehicleName_LastFrame = IS_HUD_COMPONENT_ACTIVE(NEW_HUD_VEHICLE_NAME)
//	
//	g_HasStreetNamesActive_LastFrame = ISBEINGUSED
//	g_HasStreetNamesAboveButton_LastFrame = ACTIVATE

	MPGlobalsScoreHud.TopOfTimers = 0

ENDPROC





PROC ADJUST_PEDHEADSHOT_LIGHTS(BOOL bActive)


	SET_PEDHEADSHOT_CUSTOM_LIGHTING(bActive)
	SET_PEDHEADSHOT_CUSTOM_LIGHT(0, 
								<<0,0,0>>,
								<<0,0,0>>,
								0.0,
								0.0)
	SET_PEDHEADSHOT_CUSTOM_LIGHT(1, 
								<<0,0,0>>,
								<<0,0,0>>,
								0.0,
								0.0)
	SET_PEDHEADSHOT_CUSTOM_LIGHT(2, 
								<<0,0,0>>,
								<<0,0,0>>,
								0.0,
								0.0)
	SET_PEDHEADSHOT_CUSTOM_LIGHT(3, 
								<<0,0,0>>,
								<<0,0,0>>,
								0.0,
								0.0)


ENDPROC


FUNC STRING GET_PEDHEADSHOT_STATE_STRING(PEDHEADSHOTSTATE astate)

	SWITCH astate
		CASE PHS_INVALID 	RETURN "PHS_INVALID"
		CASE PHS_QUEUED 	RETURN "PHS_QUEUED"
		CASE PHS_READY 	RETURN "PHS_READY"
		CASE PHS_WAITING_ON_TEXTURE 	RETURN "PHS_WAITING_ON_TEXTURE"
		CASE PHS_FAILED 	RETURN "PHS_FAILED"
	ENDSWITCH


	RETURN ""
ENDFUNC


/// PURPOSE: Check if a peds blend has finished
FUNC BOOL HAS_CHAR_HEADSHOT_PED_BLEND_FINISHED(PED_INDEX aPed)
	
	IF DOES_ENTITY_EXIST(aPed)
	AND NOT IS_ENTITY_DEAD(aPed)
		IF HAS_PED_HEAD_BLEND_FINISHED(aPed)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


FUNC BOOL REGISTER_PEDHEADSHOT_TAB(PED_INDEX aPed, TEXT_LABEL_63& TextureName,  PEDHEADSHOT_ID& HeadshotPic)

	#IF IS_DEBUG_BUILD
		BOOL bPrint
		IF HAS_NET_TIMER_EXPIRED_ONE_FRAME(HeadshotsDebugTimer, 6000)
			bPrint = TRUE
		ENDIF
		IF G_bTurnOnAllHUDBlockers
			bPrint = TRUE
		ENDIF
		
	#ENDIF

	TEXT_LABEL_63 TempTextureName
	
	IF DOES_ENTITY_EXIST(aPed) = FALSE
		TextureName = ""
		RETURN TRUE
	ENDIF
	
	IF HAS_CHAR_HEADSHOT_PED_BLEND_FINISHED(aPed) = FALSE
		RETURN FALSE
	ENDIF
	
	IF HeadshotPic = NULL
	      // Can REGISTER as many times
	      // Should check handle returned is valid
	      // If valid, check it is ready -> render
	      // If not ready - check handle is valid
	                                                            
		IF DOES_ENTITY_EXIST(aPed)
		                                                                
		    IF NOT IS_ENTITY_DEAD(aPed)
				ADJUST_PEDHEADSHOT_LIGHTS(TRUE)
				
		        HeadshotPic  = REGISTER_PEDHEADSHOT(aPed)
				
				NET_NL()NET_PRINT("REGISTER_PEDHEADSHOT_TAB HeadshotPic = NULL Getting Index  ")
			ELSE

				TextureName = ""
				RETURN FALSE
		    ENDIF
		ELSE

			TextureName = ""
			RETURN FALSE
		ENDIF
	                                                                  
	ELSE
	

		#IF IS_DEBUG_BUILD
			IF bPrint
				NET_NL()NET_PRINT("REGISTER_PEDHEADSHOT_TAB: HeadshotPic != NULL")
			ENDIF
		#ENDIF
		IF IS_PEDHEADSHOT_VALID(HeadshotPic)
			#IF IS_DEBUG_BUILD
				IF bPrint
					NET_NL()NET_PRINT("REGISTER_PEDHEADSHOT_TAB: IS_PEDHEADSHOT_VALID = TRUE")
				ENDIF
			#ENDIF
		    IF IS_PEDHEADSHOT_READY(HeadshotPic)
				#IF IS_DEBUG_BUILD
					IF bPrint
						NET_NL()NET_PRINT("REGISTER_PEDHEADSHOT_TAB: IS_PEDHEADSHOT_READY = TRUE")
					ENDIF
				#ENDIF
				TempTextureName = GET_PEDHEADSHOT_TXD_STRING(HeadshotPic)
				                                                          
				REQUEST_STREAMED_TEXTURE_DICT(TempTextureName)
				IF HAS_STREAMED_TEXTURE_DICT_LOADED(TempTextureName)
					TextureName = TempTextureName
					ADJUST_PEDHEADSHOT_LIGHTS(FALSE)
					RETURN TRUE
				ENDIF
			ELSE
	
				#IF IS_DEBUG_BUILD
					IF bPrint
						NET_NL()NET_PRINT("REGISTER_PEDHEADSHOT_TAB: IS_PEDHEADSHOT_READY = false GET_PEDHEADSHOT_STATE = ") NET_PRINT(GET_PEDHEADSHOT_STATE_STRING(GET_PEDHEADSHOT_STATE(HeadshotPic)))
					ENDIF
				#ENDIF
		    ENDIF
		ELSE
			NET_NL()NET_PRINT("REGISTER_PEDHEADSHOT_TAB: IS_PEDHEADSHOT_VALID = FALSE, setting HeadshotPic = NULL  ")
			HeadshotPic = NULL
		ENDIF
	ENDIF


	RETURN FALSE
ENDFUNC




PROC UNLOAD_PEDHEADSHOT(PEDHEADSHOT_ID& HeadshotPic)
	
	IF HeadshotPic <> NULL
		IF NATIVE_TO_INT(HeadshotPic) > -1
			NET_NL()NET_PRINT("UNLOAD_PEDHEADSHOT - called")
			UNREGISTER_PEDHEADSHOT(HeadshotPic) 
			HeadshotPic = NULL
		ENDIF
	ENDIF

ENDPROC

FUNC BOOL SHOULD_PEDHEAD_APPEAR()
	
	IF GET_CURRENT_HUD_STATE() = HUD_STATE_MODE_SELECTION 
	OR GET_CURRENT_HUD_STATE() = HUD_STATE_CREATE_FACE
	OR GET_CURRENT_HUD_STATE() = HUD_STATE_SELECT_CHARACTER_FM
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
	
ENDFUNC

FUNC BOOL SHOULD_TEAM_APPEAR()
	
	IF GET_CURRENT_HUD_STATE() = HUD_STATE_MODE_SELECTION 
	OR GET_CURRENT_HUD_STATE() = HUD_STATE_CREATE_FACE
	OR GET_CURRENT_HUD_STATE() = HUD_STATE_SELECT_CHARACTER_FM
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
	
ENDFUNC

FUNC BOOL HAVE_TRANSITION_CHARACTER_FACE_CHANGED(SCALEFORM_TABS& DisplayStruct)

	BOOL ShouldRefresh = FALSE
	
	IF DisplayStruct.HeadshotPed <> DisplayStruct.PedHeadshotIndexOld
	OR DisplayStruct.HasScreenChangedFaceCheck
		UNLOAD_PEDHEADSHOT(DisplayStruct.HeadshotPic) 
		NET_NL()NET_PRINT("HAVE_TRANSITION_CHARACTER_FACE_CHANGED = TRUE ")
		DisplayStruct.HeadshotPic = NULL
		ShouldRefresh = TRUE
		DisplayStruct.HasScreenChangedFaceCheck = FALSE
	ENDIF
	DisplayStruct.PedHeadshotIndexOld = DisplayStruct.HeadshotPed
	
	IF SHOULD_PEDHEAD_APPEAR() <> DisplayStruct.PedHeadShotOldScreen 
		UNLOAD_PEDHEADSHOT(DisplayStruct.HeadshotPic) 
		NET_NL()NET_PRINT("HAVE_TRANSITION_CHARACTER_FACE_CHANGED FACE SCREEN CHANGE = TRUE ")
		ShouldRefresh = TRUE
		DisplayStruct.HasScreenChangedFaceCheck = FALSE
	ENDIF
	DisplayStruct.PedHeadShotOldScreen = SHOULD_PEDHEAD_APPEAR()
	
	IF SHOULD_TEAM_APPEAR() <> DisplayStruct.TeamShotOldScreen 
		NET_NL()NET_PRINT("HAVE_TRANSITION_CHARACTER_FACE_CHANGED TEAM SCREEN CHANGE = TRUE ")
		ShouldRefresh = TRUE
		DisplayStruct.HasScreenChangedFaceCheck = FALSE
	ENDIF
	DisplayStruct.TeamShotOldScreen = SHOULD_TEAM_APPEAR()
	
	RETURN ShouldRefresh
ENDFUNC

PROC ADD_SCALEFORM_TABS_PLAYER_ICON(PED_INDEX aPed, SCALEFORM_TABS& DisplayStruct)
	DisplayStruct.HeadshotPed = aPed
ENDPROC
PROC ADD_SCALEFORM_TABS_CREW_ICON(STRING TextureName, STRING TextureDict, SCALEFORM_TABS& DisplayStruct)
	DisplayStruct.PlayerData.GangTextureName = TextureName
	DisplayStruct.PlayerData.GangTextureDict = TextureDict
ENDPROC
PROC ADD_SCALEFORM_TABS_CREW_FULL_NAME(STRING ClanFullName, SCALEFORM_TABS& DisplayStruct)
	DisplayStruct.PlayerData.ClanFullName = ClanFullName
ENDPROC
PROC ADD_SCALEFORM_TABS_GAMERTAG(STRING PlayersGamertag, SCALEFORM_TABS& DisplayStruct)
	DisplayStruct.PlayerData.PlayerGamertag = PlayersGamertag
ENDPROC
PROC ADD_SCALEFORM_TABS_TITLE(STRING TabsTitle, SCALEFORM_TABS& DisplayStruct)
	DisplayStruct.TabsHeader = TabsTitle
ENDPROC
PROC ADD_CREW_TABS_ICON(BOOL bIsPrivate, BOOL bIsRockstar, STRING sCrewTag, INT iCrewRank, SCALEFORM_TABS& DisplayStruct)
	DisplayStruct.CrewData.bIsPrivate = bIsPrivate
	DisplayStruct.CrewData.bIsRockstar = bIsRockstar 
	DisplayStruct.CrewData.sCrewTag = sCrewTag
	DisplayStruct.CrewData.iCrewRank = iCrewRank
ENDPROC
PROC ADD_SCALEFORM_CREW_IS_MEMBER(BOOL isMember, SCALEFORM_TABS& DisplayStruct)
	DisplayStruct.CrewData.bIsMember = isMember
ENDPROC



STRUCT SCALEFORM_TABS_INPUT_DATA
	STRING PlayerIconTextureName
	STRING PlayerIconTextureDict
	STRING GangIconTextureName
	STRING GangIconTextureDict

	TEXT_LABEL_63 CrewFullName
	STRING PlayersGamertag
	STRING MainTitle
	
	BOOL bIsCrewMember
	BOOL bIsCrewPrivate
	BOOL bIsCrewRockstar
	TEXT_LABEL_7 sCrewTag
	INT iCrewRank
	
ENDSTRUCT

PROC COMPILE_SCALEFORM_TABS(PED_INDEX aPed, SCALEFORM_TABS_INPUT_DATA& DataStruct, SCALEFORM_TABS& DisplayStruct)
	
//	INT WhichSlot = GET_JOINING_CHARACTER()
//	IF NOT IS_STRING_EMPTY_HUD(DataStruct.GangIconTextureName)
//	ADD_SCALEFORM_TABS_PLAYER_ICON(Placement.IdlePed[WhichSlot], DisplayStruct)
//	ENDIF

	ADD_SCALEFORM_TABS_PLAYER_ICON(aPed, DisplayStruct)
	
	ADD_SCALEFORM_TABS_CREW_ICON(DataStruct.GangIconTextureName,DataStruct.GangIconTextureDict, DisplayStruct)

	
	ADD_SCALEFORM_TABS_CREW_FULL_NAME(DataStruct.CrewFullName, DisplayStruct)

	ADD_SCALEFORM_CREW_IS_MEMBER(Datastruct.bIsCrewMember, DisplayStruct)
	IF DataStruct.bIsCrewMember
		ADD_CREW_TABS_ICON(DataStruct.bIsCrewPrivate, DataStruct.bIsCrewRockstar, DataStruct.sCrewTag, DataStruct.iCrewRank, DisplayStruct)
	ENDIF
	
	ADD_SCALEFORM_TABS_GAMERTAG(DataStruct.PlayersGamertag, DisplayStruct)
	ADD_SCALEFORM_TABS_TITLE(DataStruct.MainTitle, DisplayStruct)

ENDPROC


//PROC REFRESH_SCALEFORM_TABS_INPUT(SCALEFORM_TABS& DisplayStruct)
//	DisplayStruct.bRefreshScaleformInputs = TRUE
//ENDPROC
//PROC REFRESH_SCALEFORM_TABS_HIGHLIGHT(SCALEFORM_TABS& DisplayStruct)
//	DisplayStruct.bRefreshScaleformHighlights = TRUE
//ENDPROC
PROC REFRESH_SCALEFORM_TABS_FACES(SCALEFORM_TABS& DisplayStruct)
	DisplayStruct.bRefreshScaleformFaces = TRUE
ENDPROC

//PROC REFRESH_SCALEFORM_TABS_DISPLAY(SCALEFORM_TABS& DisplayStruct)
//	RESET_SCALEFORM_TABS(DisplayStruct)
//	DisplayStruct.bRefreshScaleformVisuals = TRUE
//ENDPROC

//FUNC BOOL SHOULD_REFRESH_SCALEFORM_TABS_INPUT(SCALEFORM_TABS& DisplayStruct)
//	RETURN DisplayStruct.bRefreshScaleformInputs
//ENDFUNC
//FUNC BOOL SHOULD_REFRESH_SCALEFORM_TABS_HIGHLIGHT(SCALEFORM_TABS& DisplayStruct)
//	RETURN DisplayStruct.bRefreshScaleformHighlights
//ENDFUNC
//FUNC BOOL SHOULD_REFRESH_SCALEFORM_TABS_DISPLAY(SCALEFORM_TABS& DisplayStruct)
//	RETURN DisplayStruct.bRefreshScaleformVisuals
//ENDFUNC
FUNC BOOL SHOULD_REFRESH_SCALEFORM_TABS_FACES(SCALEFORM_TABS& DisplayStruct)
	RETURN DisplayStruct.bRefreshScaleformFaces
ENDFUNC

//PROC RESET_REFRESH_SCALEFORM_TABS_INPUT(SCALEFORM_TABS& DisplayStruct)
//	DisplayStruct.bRefreshScaleformInputs = FALSE
//ENDPROC

//PROC RESET_REFRESH_SCALEFORM_TABS_HIGHLIGHT(SCALEFORM_TABS& DisplayStruct)
//	DisplayStruct.bRefreshScaleformHighlights = FALSE
//ENDPROC

//PROC RESET_REFRESH_SCALEFORM_TABS_DISPLAY(SCALEFORM_TABS& DisplayStruct)
//	DisplayStruct.bRefreshScaleformVisuals = FALSE
//ENDPROC
PROC RESET_REFRESH_SCALEFORM_TABS_FACES(SCALEFORM_TABS& DisplayStruct)
	DisplayStruct.bRefreshScaleformFaces = FALSE
ENDPROC
PROC HOLD_DRAWING_SCALEFORM_TABS_FACES(SCALEFORM_TABS& DisplayStruct)
	DisplayStruct.InitialiseScaleformFaces = TRUE //so it only resets when the badge resets. need because of the few frames delay

ENDPROC


//
//PROC SET_SCALEFORM_TAB_INPUT(CONTROL_ACTION WhichAction, SCALEFORM_TABS& DisplayStruct)
//	
//	SWITCH WhichAction
//		CASE INPUT_FRONTEND_UP
//			DisplayStruct.ScaleformInputEventNumber = (SCALEFORM_INPUT_EVENT_UP)
//		BREAK
//		CASE INPUT_FRONTEND_DOWN
//			DisplayStruct.ScaleformInputEventNumber = (SCALEFORM_INPUT_EVENT_DOWN)
//		BREAK
//		CASE INPUT_FRONTEND_LEFT
//			DisplayStruct.ScaleformInputEventNumber = (SCALEFORM_INPUT_EVENT_LEFT)
//		BREAK
//		CASE INPUT_FRONTEND_RIGHT
//			DisplayStruct.ScaleformInputEventNumber = (SCALEFORM_INPUT_EVENT_RIGHT)
//		BREAK
//		CASE INPUT_FRONTEND_ACCEPT
//			DisplayStruct.ScaleformInputEventNumber = (SCALEFORM_INPUT_EVENT_CROSS)
//		BREAK
//		CASE INPUT_FRONTEND_CANCEL
//			DisplayStruct.ScaleformInputEventNumber = (SCALEFORM_INPUT_EVENT_CIRCLE)
//		BREAK
//		CASE INPUT_FRONTEND_LB
//			DisplayStruct.ScaleformInputEventNumber = (SCALEFORM_INPUT_EVENT_LEFTSHOULDER1)
//		BREAK
//		CASE INPUT_FRONTEND_RB
//			DisplayStruct.ScaleformInputEventNumber = (SCALEFORM_INPUT_EVENT_RIGHTSHOULDER1)
//		BREAK
//
//	ENDSWITCH
//ENDPROC


//PROC RUN_SCALEFORM_TABS_INPUT(SCALEFORM_INDEX& ScaleformIndex,  SCALEFORM_TABS& DisplayStruct, BOOL Reset = FALSE )
//
//	IF Reset = TRUE
//		DisplayStruct.InitialiseScaleformInputs = FALSE
//		RESET_REFRESH_SCALEFORM_TABS_INPUT(DisplayStruct)
//	ENDIF
//
//	IF DisplayStruct.InitialiseScaleformInputs = FALSE
//		IF DisplayStruct.ScaleformInputEventNumber != SCALEFORM_INPUT_EVENT_INVALID
//			IF HAS_SCALEFORM_MOVIE_LOADED(ScaleformIndex)
//				NET_NL()NET_PRINT("RUN_SCALEFORM_TABS_INPUT: ENUM_TO_INT(DisplayStruct.ScaleformInputEventNumber) = ")NET_PRINT_INT(ENUM_TO_INT(DisplayStruct.ScaleformInputEventNumber))NET_NL()
//
//				BEGIN_SCALEFORM_MOVIE_METHOD(ScaleformIndex, "SET_INPUT_EVENT")
//					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(DisplayStruct.ScaleformInputEventNumber))
//				END_SCALEFORM_MOVIE_METHOD()
//				DisplayStruct.ScaleformInputEventNumber = SCALEFORM_INPUT_EVENT_INVALID
//				DisplayStruct.InitialiseScaleformInputs = TRUE
//			ENDIF
//		ENDIF
//	ENDIF
//
//
//ENDPROC


//PROC SET_SCALEFORM_TAB_HIGHLIGHT(INT WhichTab, SCALEFORM_TABS& DisplayStruct)
//	DisplayStruct.iHighlightTab = WhichTab
//ENDPROC


//PROC RUN_SCALEFORM_TABS_HIGHLIGHT(SCALEFORM_INDEX& ScaleformIndex,  SCALEFORM_TABS& DisplayStruct, BOOL Reset = FALSE )
//
//	IF Reset = TRUE
//		DisplayStruct.InitialiseScaleformHighlights = FALSE
//		RESET_REFRESH_SCALEFORM_TABS_HIGHLIGHT(DisplayStruct)
//	ENDIF
//
//	IF DisplayStruct.InitialiseScaleformHighlights = FALSE
//		IF DisplayStruct.iHighlightTab != -1
//			IF HAS_SCALEFORM_MOVIE_LOADED(ScaleformIndex)
//				NET_NL()NET_PRINT("RUN_SCALEFORM_TABS_HIGHLIGHT: DisplayStruct.iHighlightTab = ")NET_PRINT_INT(DisplayStruct.iHighlightTab)NET_NL()
//
//				BEGIN_SCALEFORM_MOVIE_METHOD(ScaleformIndex, "HIGHLIGHT_MENU")
//					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(DisplayStruct.iHighlightTab)
//				END_SCALEFORM_MOVIE_METHOD()
//				DisplayStruct.iHighlightTab = -1
//				DisplayStruct.InitialiseScaleformHighlights = TRUE
//			ENDIF
//		ENDIF
//	ENDIF
//
//ENDPROC



//FUNC BOOL HAVE_TRANSITION_CHARACTER_ICONS_CHANGED(SCALEFORM_TABS& DisplayStruct)
//
//	BOOL ShouldRefresh = FALSE
//	
//	
//	IF ARE_STRINGS_JUST_EQUAL(DisplayStruct.PlayerData.GangTextureName, G_OldGangIcon_TextureName) = FALSE
//		ShouldRefresh = TRUE
//	ENDIF
//	G_OldGangIcon_TextureName = DisplayStruct.PlayerData.GangTextureName
//	
//	IF ARE_STRINGS_JUST_EQUAL(DisplayStruct.PlayerData.GangTextureDict, G_OldGangIcon_TextureDict) = FALSE
//		ShouldRefresh = TRUE
//	ENDIF
//	G_OldGangIcon_TextureDict = DisplayStruct.PlayerData.GangTextureDict
//
//	RETURN ShouldRefresh
//
//ENDFUNC

//
//BOOL ShouldResetFace
//
//PROC RUN_SCALEFORM_TABS_FACES(SCALEFORM_INDEX& ScaleformIndex,  SCALEFORM_TABS& DisplayStruct, BOOL Reset = FALSE )
//
//	IF Reset = TRUE
//		DisplayStruct.InitialiseScaleformFaces = FALSE
//		RESET_REFRESH_SCALEFORM_TABS_FACES(DisplayStruct)
//	ENDIF
//
//	IF DisplayStruct.InitialiseScaleformFaces = FALSE
//		
//		BOOL ShouldShowImage = TRUE
//		IF IS_STRING_EMPTY_HUD(DisplayStruct.PlayerData.GangTextureDict)
//			ShouldShowImage = FALSE
//		ENDIF
//		IF IS_STRING_EMPTY_HUD(DisplayStruct.PlayerData.GangTextureName)
//			ShouldShowImage = FALSE
//		ENDIF
//		
//		IF SHOULD_PEDHEAD_APPEAR() = FALSE
//			ShouldShowImage = FALSE
//		ENDIF
//		
//		IF HAS_SCALEFORM_MOVIE_LOADED(ScaleformIndex)
//			
//			NET_NL()NET_PRINT("SET_CHAR_IMG - called with ")NET_PRINT(DisplayStruct.PlayerData.PlayerFaceTextureName)
//			BEGIN_SCALEFORM_MOVIE_METHOD(ScaleformIndex, "SET_CHAR_IMG")
//				IF ShouldShowImage = TRUE
//					SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(DisplayStruct.PlayerData.PlayerFaceTextureName)
//					SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(DisplayStruct.PlayerData.PlayerFaceTextureName)
//				ELSE
//					SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME("")
//					SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME("")
//				ENDIF	
//				SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(ShouldShowImage) //Link this with the gang badge
//			END_SCALEFORM_MOVIE_METHOD()
//			
//			DisplayStruct.InitialiseScaleformFaces = TRUE
//		ENDIF
//		
//		
//	ENDIF
//	
//ENDPROC


//PROC RUN_SCALEFORM_TABS(SCALEFORM_INDEX& ScaleformIndex, SPRITE_PLACEMENT& aSprite, SCALEFORM_TABS& DisplayStruct, BOOL Reset = FALSE )
//	
//	INT I
//	//http://rsgediwiki1/wiki/index.php/Scaleform_NEW_GTAV_Pause_Menu
//	
//	
////	HOLD_DRAWING_SCALEFORM_TABS_FACES(DisplayStruct)
//	
////	IF HAVE_TRANSITION_CHARACTER_FACE_CHANGED(DisplayStruct)
////		ShouldResetFace = TRUE
////	ENDIF
//
//	IF HAVE_TRANSITION_CHARACTER_ICONS_CHANGED(DisplayStruct)
//		Reset = TRUE
//	ENDIF
//	
////	IF ShouldResetFace = TRUE
////		IF SHOULD_PEDHEAD_APPEAR() = TRUE
////			IF REGISTER_PEDHEADSHOT_TAB(DisplayStruct.HeadshotPed , DisplayStruct.PlayerData.PlayerFaceTextureName, DisplayStruct.HeadshotPic)
////				IF IS_STRING_EMPTY_HUD(DisplayStruct.PlayerData.PlayerFaceTextureName) = FALSE
////					REFRESH_SCALEFORM_TABS_FACES(DisplayStruct)
////					ShouldResetFace = FALSE
////				ELSE
////					ShouldResetFace = FALSE
////				ENDIF
////			ENDIF
////		ELSE
////			REFRESH_SCALEFORM_TABS_FACES(DisplayStruct)
////			ShouldResetFace = FALSE
////		ENDIF
////	ENDIF
//	
//	IF Reset = TRUE
//		DisplayStruct.bInitialised = FALSE
//		
////		RESET_REFRESH_SCALEFORM_TABS_DISPLAY(DisplayStruct)
//	ENDIF
//	
//	
//	IF DisplayStruct.bInitialised = FALSE
//		
//		
//		
//		IF NOT HAS_SCALEFORM_MOVIE_LOADED(ScaleformIndex)
////			IF NOT HAS_SCALEFORM_MOVIE_LOADED(DisplayStruct.InputIndex)
//				ScaleformIndex = REQUEST_SCALEFORM_MOVIE("pause_menu_header")
////			ENDIF
//		ENDIF
//
//		IF HAS_SCALEFORM_MOVIE_LOADED(ScaleformIndex)
//		
////			BEGIN_SCALEFORM_MOVIE_METHOD(ScaleformIndex, "REMOVE_MENU")
////			END_SCALEFORM_MOVIE_METHOD()
//			
//			IF DisplayStruct.DisplayTab[0] = TRUE //At least one tab
//				BEGIN_SCALEFORM_MOVIE_METHOD(ScaleformIndex, "BUILD_MENU")
//					FOR I = 0 TO MAX_SCALEFORM_TABS-1
//						IF DisplayStruct.DisplayTab[I] = TRUE
//							NET_NL()NET_PRINT("BUILD_MENU I = ")NET_PRINT_INT(I)
//							
//							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT((ENUM_TO_INT(DisplayStruct.TabContent[I])))
//						ENDIF
//					ENDFOR	
//				END_SCALEFORM_MOVIE_METHOD()
//
//				FOR I = 0 TO MAX_SCALEFORM_TABS-1
//					IF DisplayStruct.DisplayTab[I] = TRUE
//						BEGIN_SCALEFORM_MOVIE_METHOD(ScaleformIndex, "SET_MENU_ITEM_COLOUR")
//							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(I)
//							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT((ENUM_TO_INT(DisplayStruct.TabColour[I])))
//						END_SCALEFORM_MOVIE_METHOD()	
//					ENDIF
//				ENDFOR
//				FOR I = 0 TO MAX_SCALEFORM_TABS-1
//					IF DisplayStruct.DisplayTab[I] = TRUE
//						BEGIN_SCALEFORM_MOVIE_METHOD(ScaleformIndex, "LOCK_MENU_ITEM")
//							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(I)
//							IF DisplayStruct.iTabLocked[I] = 0
//								SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
//							ELSE
//								SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
//							ENDIF
//						END_SCALEFORM_MOVIE_METHOD()	
//					ENDIF
//				ENDFOR
//				FOR I = 0 TO MAX_SCALEFORM_TABS-1
//					IF DisplayStruct.DisplayTab[I] = TRUE
//						BEGIN_SCALEFORM_MOVIE_METHOD(ScaleformIndex, "SET_MENU_HEADER_TEXT")
//						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT((ENUM_TO_INT(DisplayStruct.TabContent[I])))	
//						NET_NL()NET_PRINT("DisplayStruct.TabNames[")
//						NET_PRINT_INT(I)
//						NET_PRINT("] = ")
//						NET_PRINT(DisplayStruct.TabNames[I])
//						IF NOT IS_STRING_EMPTY_HUD(DisplayStruct.TabNames[I])
//							IF DisplayStruct.TabNameIsName[I]
//								SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(DisplayStruct.TabNames[I])
//							ELSE
//								SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(DisplayStruct.TabNames[I])
//							ENDIF
//						ELSE
//							SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")
//						ENDIF
//						END_SCALEFORM_MOVIE_METHOD()	
//					ENDIF
//				ENDFOR
//				
//			ENDIF
//				
//			IF NOT IS_STRING_EMPTY_HUD(DisplayStruct.TabsHeader)
//				BEGIN_SCALEFORM_MOVIE_METHOD(ScaleformIndex, "SET_HEADER_TITLE")
//					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(DisplayStruct.TabsHeader)
//				END_SCALEFORM_MOVIE_METHOD()
//			ENDIF
//			
//			BOOL ShouldShowImage = TRUE
//			BOOL ShouldShowTeam = TRUE
//			IF IS_STRING_EMPTY_HUD(DisplayStruct.PlayerData.GangTextureDict)
//				ShouldShowImage = FALSE
//				ShouldShowTeam = FALSE
//			ENDIF
//			IF IS_STRING_EMPTY_HUD(DisplayStruct.PlayerData.GangTextureName)
//				ShouldShowImage = FALSE
//				ShouldShowTeam = FALSE
//			ENDIF
//			
//			IF SHOULD_TEAM_APPEAR() = FALSE
//				ShouldShowTeam = FALSE
//			ENDIF
//			
//			IF ShouldShowImage
//				IF DisplayStruct.CrewData.bIsMember
//					BEGIN_SCALEFORM_MOVIE_METHOD(ScaleformIndex, "SET_CREW_TAG")
//						SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(DisplayStruct.CrewData.bIsPrivate)
//						SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(DisplayStruct.CrewData.bIsRockstar)
//						IF NOT IS_STRING_EMPTY_HUD(DisplayStruct.CrewData.sCrewTag)
//							SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(DisplayStruct.CrewData.sCrewTag)
//						ELSE
//							SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME("")
//						ENDIF
//						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(DisplayStruct.CrewData.iCrewRank)
//					END_SCALEFORM_MOVIE_METHOD()
//				ENDIF
//			ENDIF
//			
//			
//			
//			BEGIN_SCALEFORM_MOVIE_METHOD(ScaleformIndex, "SET_CREW_IMG")
//				NET_PRINT("DisplayStruct.PlayerData.GangTextureName = ")NET_PRINT(DisplayStruct.PlayerData.GangTextureName)
//				IF NOT IS_STRING_EMPTY_HUD(DisplayStruct.PlayerData.GangTextureDict)
//					SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(DisplayStruct.PlayerData.GangTextureDict)
//				ELSE
//					SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME("")
//				ENDIF
//				IF NOT IS_STRING_EMPTY_HUD(DisplayStruct.PlayerData.GangTextureName)
//					SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(DisplayStruct.PlayerData.GangTextureName)
//				ELSE
//					SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME("")
//				ENDIF
//				SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(ShouldShowTeam)
//			END_SCALEFORM_MOVIE_METHOD()
//
//			
//			
//			BEGIN_SCALEFORM_MOVIE_METHOD(ScaleformIndex, "SET_HEADING_DETAILS")
//				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")
//				IF NOT IS_STRING_EMPTY_HUD(DisplayStruct.PlayerData.ClanFullName)
//					IF ARE_STRINGS_EQUAL(DisplayStruct.PlayerData.ClanFullName, "CREW_NONE")
//						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(DisplayStruct.PlayerData.ClanFullName)
//					ELSE
//						SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(DisplayStruct.PlayerData.ClanFullName)
//					ENDIF
//				ELSE
//					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")
//				ENDIF
//				IF NOT IS_STRING_EMPTY_HUD(DisplayStruct.PlayerData.PlayerGamertag)
//					IF ARE_STRINGS_EQUAL(DisplayStruct.PlayerData.PlayerGamertag, "GAMERTAG_NONE")
//						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(DisplayStruct.PlayerData.PlayerGamertag)
//					ELSE
//						SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(DisplayStruct.PlayerData.PlayerGamertag)
//					ENDIF
//				ELSE
//					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")
//				ENDIF
//			END_SCALEFORM_MOVIE_METHOD()
//		
//		
//		
//			DisplayStruct.bInitialised = TRUE
//		ENDIF
//		
//
//		
//	ENDIF
//
//	RUN_SCALEFORM_TABS_INPUT(ScaleformIndex, DisplayStruct, SHOULD_REFRESH_SCALEFORM_TABS_INPUT(DisplayStruct))
//	RUN_SCALEFORM_TABS_HIGHLIGHT(ScaleformIndex, DisplayStruct, SHOULD_REFRESH_SCALEFORM_TABS_HIGHLIGHT(DisplayStruct))
////	IF HAS_SCALEFORM_MOVIE_LOADED(ScaleformIndex)
////		RUN_SCALEFORM_TABS_FACES(ScaleformIndex, DisplayStruct,  SHOULD_REFRESH_SCALEFORM_TABS_FACES(DisplayStruct))
////	ENDIF
//	
//	IF DisplayStruct.bInitialised = TRUE
//		DRAW_THE_SCALEFORM_MOVIE(ScaleformIndex, aSprite)
//	ENDIF
//	
//	
//ENDPROC








PROC SCALEFORM_SET_DATA_SLOT_EMPTY(SCALEFORM_INDEX& ScaleformIndex, INT iViewIndex = 0, INT iSlotIndex = 0)
	IF HAS_SCALEFORM_MOVIE_LOADED(ScaleformIndex)
		BEGIN_SCALEFORM_MOVIE_METHOD(ScaleformIndex, "SET_DATA_SLOT_EMPTY")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iViewIndex)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSlotIndex)
		END_SCALEFORM_MOVIE_METHOD()
		PRINTLN("[CS_DPAD] SCALEFORM_SET_DATA_SLOT_EMPTY, ciDPAD_SET_DATA_SLOT_EMPTY")
	ENDIF
ENDPROC





//
////BOOL WIDGETICONDONE
////SPRITE_PLACEMENT DebugICONWidgetSprite
////FLOAT WidgtetLoadingAlignX, WidgtetLoadingAlignY, WidgtetLoadingSizeX, WidgtetLoadingSizeY
//
//FUNC SPRITE_PLACEMENT GET_SCALEFORM_LOADING_ICON_POSITION()
//
//	SPRITE_PLACEMENT aSprite
//	
////	IF WIDGETICONDONE = FALSE
////		START_WIDGET_GROUP("LoadingIcon")
////			CREATE_A_SPRITE_PLACEMENT_WIDGET(DebugICONWidgetSprite, "Loading Icon")
////			ADD_WIDGET_FLOAT_SLIDER("WidgtetLoadingAlignX", WidgtetLoadingAlignX, -2, 2, 0.001)
////			ADD_WIDGET_FLOAT_SLIDER("WidgtetLoadingAlignY", WidgtetLoadingAlignY, -2, 2, 0.001)
////			ADD_WIDGET_FLOAT_SLIDER("WidgtetLoadingSizeX", WidgtetLoadingSizeX, -2, 2, 0.001)
////			ADD_WIDGET_FLOAT_SLIDER("WidgtetLoadingSizeY", WidgtetLoadingSizeY, -2, 2, 0.001)
////		STOP_WIDGET_GROUP()
////		WIDGETICONDONE = TRUE
////	ENDIF
//	
//	//Loading Icon	
//	aSprite.x = 0.387-0.108-0.003
//	aSprite.y = 0.933
//	aSprite.w = 0.450
//	aSprite.h = 0.041
//	aSprite.r = 255
//	aSprite.g = 255
//	aSprite.b = 255
//	aSprite.a = 200
//	aSprite.fRotation = 0
//	
////	aSprite.x += DebugICONWidgetSprite.x
////	aSprite.y += DebugICONWidgetSprite.y
////	aSprite.w += DebugICONWidgetSprite.w
////	aSprite.h += DebugICONWidgetSprite.h
////	aSprite.r += DebugICONWidgetSprite.r
////	aSprite.g += DebugICONWidgetSprite.g
////	aSprite.b += DebugICONWidgetSprite.b
////	aSprite.a += DebugICONWidgetSprite.a
//	
//	RETURN aSprite
//
//ENDFUNC







PROC SET_LOADING_ICON_INACTIVE(BOOL bPreloadSpinner = FALSE)
	
	#IF IS_DEBUG_BUILD			
	DEBUG_PRINTCALLSTACK()
	NET_PRINT("SET_LOADING_ICON_INACTIVE called ")
	#ENDIF
	
	BUSYSPINNER_OFF()
	
	IF bPreloadSpinner
		NET_PRINT("PRELOAD_BUSYSPINNER() called ")
		PRELOAD_BUSYSPINNER()
	ENDIF
ENDPROC

FUNC BOOL IS_LOADING_ICON_ACTIVE()
	RETURN BUSYSPINNER_IS_ON()
ENDFUNC

PROC SET_SCALEFORM_LOADING_ICON_DISPLAY(SCALEFORM_LOADING_ICON& DisplayStruct, LOADING_ICON_SYMBOLS loadingIcon)
	DisplayStruct.loadingIcon = loadingIcon
ENDPROC

PROC RESET_SCALEFORM_LOADING_ICON(SCALEFORM_LOADING_ICON& DisplayStruct)
	SCALEFORM_LOADING_ICON EmptyStruct
	DisplayStruct = EmptyStruct
ENDPROC

PROC REFRESH_SCALEFORM_LOADING_ICON(SCALEFORM_LOADING_ICON& DisplayStruct, BOOL bTurnSpinnerOff = FALSE)
	RESET_SCALEFORM_LOADING_ICON(DisplayStruct)
	
	IF bTurnSpinnerOff
		SET_LOADING_ICON_INACTIVE()
	ENDIF
	
	DisplayStruct.bRefreshScaleformLoading = TRUE
ENDPROC
FUNC BOOL SHOULD_REFRESH_SCALEFORM_LOADING_ICON(SCALEFORM_LOADING_ICON& DisplayStruct)
	RETURN DisplayStruct.bRefreshScaleformLoading
ENDFUNC
PROC RESET_REFRESH_SCALEFORM_LOADING_ICON(SCALEFORM_LOADING_ICON& DisplayStruct)
	DisplayStruct.bRefreshScaleformLoading = FALSE
ENDPROC




PROC RUN_SCALEFORM_LOADING_ICON(SCALEFORM_LOADING_ICON& DisplayStruct, BOOL Reset)

	IF Reset = TRUE
		DisplayStruct.bInitialised = FALSE
		RESET_REFRESH_SCALEFORM_LOADING_ICON(DisplayStruct)
	ENDIF

//	HAS_SCALEFORM_MOVIE_LOADED(ScaleformIndex)
		
		
		
		
		
		IF DisplayStruct.bInitialised = FALSE
			IF DisplayStruct.IsTextDoubleNumber

			
				BEGIN_TEXT_COMMAND_BUSYSPINNER_ON(DisplayStruct.sMainStringSlot)
					ADD_TEXT_COMPONENT_INTEGER(DisplayStruct.TopNumber)
					ADD_TEXT_COMPONENT_INTEGER(DisplayStruct.Denominator)
				END_TEXT_COMMAND_BUSYSPINNER_ON(ENUM_TO_INT(DisplayStruct.loadingIcon))
				
//				#IF IS_DEBUG_BUILD
//				NET_NL()NET_PRINT("RUN_SCALEFORM_LOADING_ICON: IsTextDoubleNumber ")NET_NL()
//				NET_NL()NET_PRINT("			- DisplayStruct.sMainStringSlot = ")
//				NET_NL()NET_PRINT("			- DisplayStruct.TopNumber = ")NET_PRINT_INT(DisplayStruct.TopNumber)
//				NET_NL()NET_PRINT("			- DisplayStruct.Denominator = ")NET_PRINT_INT(DisplayStruct.Denominator)
//
//
//				#ENDIF
				
				
			ELIF  DisplayStruct.IsTextSingleNumber
				BEGIN_TEXT_COMMAND_BUSYSPINNER_ON(DisplayStruct.sMainStringSlot)
					ADD_TEXT_COMPONENT_INTEGER(DisplayStruct.TopNumber)
				END_TEXT_COMMAND_BUSYSPINNER_ON(ENUM_TO_INT(DisplayStruct.loadingIcon))
				
//				#IF IS_DEBUG_BUILD
//				NET_NL()NET_PRINT("RUN_SCALEFORM_LOADING_ICON: IsTextSingleNumber ")NET_NL()
//				NET_NL()NET_PRINT("			- DisplayStruct.sMainStringSlot = ")
//				NET_NL()NET_PRINT("			- DisplayStruct.TopNumber = ")NET_PRINT_INT(DisplayStruct.TopNumber)
//				#ENDIF
				
			ELIF DisplayStruct.IsTextPlayerNameAndDoubleNumber
				
				BEGIN_TEXT_COMMAND_BUSYSPINNER_ON(DisplayStruct.sMainStringSlot)
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(DisplayStruct.sPlayerName)
					ADD_TEXT_COMPONENT_INTEGER(DisplayStruct.TopNumber)
					ADD_TEXT_COMPONENT_INTEGER(DisplayStruct.Denominator)
				END_TEXT_COMMAND_BUSYSPINNER_ON(ENUM_TO_INT(DisplayStruct.loadingIcon))
				
//				#IF IS_DEBUG_BUILD
//				NET_NL()NET_PRINT("RUN_SCALEFORM_LOADING_ICON: IsTextPlayerNameAndDoubleNumber ")NET_NL()
//				NET_NL()NET_PRINT("			- DisplayStruct.sMainStringSlot = ")
//				NET_NL()NET_PRINT("			- DisplayStruct.sPlayerName = ")NET_PRINT(DisplayStruct.sPlayerName)
//				NET_NL()NET_PRINT("			- DisplayStruct.TopNumber = ")NET_PRINT_INT(DisplayStruct.TopNumber)
//				NET_NL()NET_PRINT("			- DisplayStruct.Denominator = ")NET_PRINT_INT(DisplayStruct.Denominator)
//				#ENDIF
			
			ELIF DisplayStruct.IsTextTwoString
				BEGIN_TEXT_COMMAND_BUSYSPINNER_ON(DisplayStruct.sMainStringSlot)
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(DisplayStruct.sPlayerName)
				END_TEXT_COMMAND_BUSYSPINNER_ON(ENUM_TO_INT(DisplayStruct.loadingIcon))
				
//				#IF IS_DEBUG_BUILD
//				NET_NL()NET_PRINT("RUN_SCALEFORM_LOADING_ICON: IsTextTwoString ")NET_NL()
//				NET_NL()NET_PRINT("			- DisplayStruct.sMainStringSlot = ")
//				NET_NL()NET_PRINT("			- DisplayStruct.sPlayerName = ")NET_PRINT(DisplayStruct.sPlayerName)
//				#ENDIF
			
			ELIF DisplayStruct.IsNumberTime
			
				BEGIN_TEXT_COMMAND_BUSYSPINNER_ON(DisplayStruct.sMainStringSlot)
					ADD_TEXT_COMPONENT_SUBSTRING_TIME(DisplayStruct.TopNumber, TIME_FORMAT_MINUTES|TIME_FORMAT_SECONDS|TEXT_FORMAT_HIDE_LEADING_ZEROS_ON_LEADING_UNITS)
				END_TEXT_COMMAND_BUSYSPINNER_ON(ENUM_TO_INT(DisplayStruct.loadingIcon))
			
//				#IF IS_DEBUG_BUILD
//				NET_NL()NET_PRINT("RUN_SCALEFORM_LOADING_ICON: NUMBERTIME ")NET_NL()
//				NET_NL()NET_PRINT("			- DisplayStruct.sMainStringSlot = ")
//				
//				#ENDIF
			
			ELSE
				BEGIN_TEXT_COMMAND_BUSYSPINNER_ON(DisplayStruct.sMainStringSlot)
				END_TEXT_COMMAND_BUSYSPINNER_ON(ENUM_TO_INT(DisplayStruct.loadingIcon))
				
//				#IF IS_DEBUG_BUILD
//				NET_NL()NET_PRINT("RUN_SCALEFORM_LOADING_ICON: ELSE ")NET_NL()
//				NET_NL()NET_PRINT("			- DisplayStruct.sMainStringSlot = ")
//				
//				#ENDIF
				
			ENDIF
		
			
			
			
//			SET_LOADING_ICON_ACTIVE(aStrng, LOADING_ICON_SPINNER)
//		
			DisplayStruct.bInitialised = TRUE
		
		ENDIF
//	ELSE
//		ScaleformIndex = REQUEST_SCALEFORM_MOVIE("SAVING_FOOTER")
//	ENDIF
	
//	DisplayStruct.LoadingAlignX = -0.050 
//	DisplayStruct.LoadingAlignY = -0.916 
//	DisplayStruct.LoadingSizeX = 0 
//	DisplayStruct.LoadingSizeY = 0
	
//	DisplayStruct.LoadingAlignX += WidgtetLoadingAlignX 
//	DisplayStruct.LoadingAlignY += WidgtetLoadingAlignY 
//	DisplayStruct.LoadingSizeX += WidgtetLoadingSizeX 
//	DisplayStruct.LoadingSizeY += WidgtetLoadingSizeY
	
	IF DisplayStruct.bInitialised = TRUE
//		SET_SCRIPT_GFX_ALIGN(UI_ALIGN_LEFT, UI_ALIGN_BOTTOM)
//		SET_SCRIPT_GFX_ALIGN_PARAMS(DisplayStruct.LoadingAlignX, DisplayStruct.LoadingAlignY, DisplayStruct.LoadingSizeX, DisplayStruct.LoadingSizeY)
//		DRAW_THE_SCALEFORM_MOVIE(ScaleformIndex, aSprite)
//		RESET_SCRIPT_GFX_ALIGN()
	ENDIF

ENDPROC
















/////GALLERY FUNCTIONS


//BOOL WIDGETBUTTONDONE
//SPRITE_PLACEMENT DebugButtonWidgetSprite



FUNC BOOL HAS_ALL_GALLERY_HUD_ELEMENTS_LOADED(SCALEFORM_INDEX& ScaleformMovie)

	IF NOT HAS_SCALEFORM_MOVIE_LOADED(ScaleformMovie)
		ScaleformMovie = REQUEST_SCALEFORM_MOVIE("pause_mp_menu_gallery")
	ENDIF
	IF HAS_SCALEFORM_MOVIE_LOADED(ScaleformMovie )
		RETURN TRUE
	ENDIF
	
	IF NOT HAS_SCALEFORM_MOVIE_LOADED(ScaleformMovie)
		NET_NL()NET_PRINT("HAS_ALL_GALLERY_HUD_ELEMENTS_LOADED: HAS_SCALEFORM_MOVIE_LOADED HUD_SCALEFORM_GALLERY is FALSE")
	ENDIF

	
	RETURN FALSE
	
ENDFUNC

//
//
////BOOL WIDGETBUTTONDONEAWARD
////SPRITE_PLACEMENT DebugButtonWidgetSpriteAward
//FUNC SPRITE_PLACEMENT GET_SCALEFORM_GALLERY_POSITION()
//
//	SPRITE_PLACEMENT aSprite
//	
////	IF WIDGETBUTTONDONEAWARD = FALSE
////		CREATE_A_SPRITE_PLACEMENT_WIDGET(DebugButtonWidgetSpriteAward, "Gallery positions")
////		WIDGETBUTTONDONEAWARD = TRUE
////	ENDIF
//	
//////	Scaleform movie Bottom
//	aSprite.x = 0.613
//	aSprite.y = 0.529
//	aSprite.w = 0.454 //0.6781 
//	aSprite.h = 0.5972
//	aSprite.r = 255
//	aSprite.g = 255
//	aSprite.b = 255
//	aSprite.a = 200
//	aSprite.fRotation = 0
//	
////	aSprite.x += DebugButtonWidgetSpriteAward.x
////	aSprite.y += DebugButtonWidgetSpriteAward.y
////	aSprite.w += DebugButtonWidgetSpriteAward.w
////	aSprite.h += DebugButtonWidgetSpriteAward.h
////	aSprite.r += DebugButtonWidgetSpriteAward.r
////	aSprite.g += DebugButtonWidgetSpriteAward.g
////	aSprite.b += DebugButtonWidgetSpriteAward.b
////	aSprite.a += DebugButtonWidgetSpriteAward.a
//	
//	RETURN aSprite
//
//ENDFUNC
//
//PROC RESET_SCALEFORM_GALLERY(SCALEFORM_GALLERY& DisplayStruct)
//	DisplayStruct = g_EmptyGalleryMedalStruct
//ENDPROC
//
//PROC REFRESH_SCALEFORM_GALLERY(SCALEFORM_GALLERY& DisplayStruct)
//	RESET_SCALEFORM_GALLERY(DisplayStruct)
//	DisplayStruct.bRefreshScaleformGalleryVisuals = TRUE
//ENDPROC
//PROC REFRESH_SCALEFORM_GALLERY_INPUT(SCALEFORM_GALLERY& DisplayStruct)
//	DisplayStruct.bRefreshScaleformGalleryInputs = TRUE
//ENDPROC
//PROC REFRESH_SCALEFORM_GALLERY_HIGHLIGHT(SCALEFORM_GALLERY& DisplayStruct)
//	DisplayStruct.bRefreshScaleformGalleryHighlights = TRUE
//ENDPROC
//FUNC BOOL SHOULD_REFRESH_SCALEFORM_GALLERY(SCALEFORM_GALLERY& DisplayStruct)
//	RETURN DisplayStruct.bRefreshScaleformGalleryVisuals
//ENDFUNC
//FUNC BOOL SHOULD_REFRESH_SCALEFORM_GALLERY_INPUT(SCALEFORM_GALLERY& DisplayStruct)
//	RETURN DisplayStruct.bRefreshScaleformGalleryInputs
//ENDFUNC
//FUNC BOOL SHOULD_REFRESH_SCALEFORM_GALLERY_HIGHLIGHT(SCALEFORM_GALLERY& DisplayStruct)
//	RETURN DisplayStruct.bRefreshScaleformGalleryHighlights
//ENDFUNC
//
//PROC RESET_REFRESH_SCALEFORM_GALLERY(SCALEFORM_GALLERY& DisplayStruct)
//	DisplayStruct.bRefreshScaleformGalleryVisuals = FALSE
//ENDPROC
//PROC RESET_REFRESH_SCALEFORM_GALLERY_INPUT(SCALEFORM_GALLERY& DisplayStruct)
//	DisplayStruct.bRefreshScaleformGalleryInputs = FALSE
//ENDPROC
//PROC RESET_REFRESH_SCALEFORM_GALLERY_HIGHLIGHT(SCALEFORM_GALLERY& DisplayStruct)
//	DisplayStruct.bRefreshScaleformGalleryHighlights = FALSE
//ENDPROC
//
//
//
//PROC SET_SCALEFORM_GALLERY_INPUT(CONTROL_ACTION WhichAction, SCALEFORM_GALLERY& DisplayStruct)
//	
//	SWITCH WhichAction
//		CASE INPUT_FRONTEND_UP
//			DisplayStruct.ScaleformInputEventNumber = (SCALEFORM_INPUT_EVENT_UP)
//		BREAK
//		CASE INPUT_FRONTEND_DOWN
//			DisplayStruct.ScaleformInputEventNumber = (SCALEFORM_INPUT_EVENT_DOWN)
//		BREAK
//		CASE INPUT_FRONTEND_LEFT
//			DisplayStruct.ScaleformInputEventNumber = (SCALEFORM_INPUT_EVENT_LEFT)
//		BREAK
//		CASE INPUT_FRONTEND_RIGHT
//			DisplayStruct.ScaleformInputEventNumber = (SCALEFORM_INPUT_EVENT_RIGHT)
//		BREAK
//	ENDSWITCH
//ENDPROC
//
//
//PROC RUN_SCALEFORM_GALLERY_INPUT(SCALEFORM_INDEX& ScaleformIndex,  SCALEFORM_GALLERY& DisplayStruct, BOOL Reset = FALSE )
//
//	IF Reset = TRUE
//		DisplayStruct.InitialiseScaleformGalleryInputs = FALSE
//		RESET_REFRESH_SCALEFORM_GALLERY_INPUT(DisplayStruct)
//	ENDIF
//
//	IF DisplayStruct.InitialiseScaleformGalleryInputs = FALSE
//		IF DisplayStruct.ScaleformInputEventNumber != SCALEFORM_INPUT_EVENT_INVALID
//			IF HAS_SCALEFORM_MOVIE_LOADED(ScaleformIndex)
//				BEGIN_SCALEFORM_MOVIE_METHOD(ScaleformIndex, "SET_INPUT_EVENT")
//					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(DisplayStruct.ScaleformInputEventNumber))
//				END_SCALEFORM_MOVIE_METHOD()
//				DisplayStruct.ScaleformInputEventNumber = SCALEFORM_INPUT_EVENT_INVALID
//				DisplayStruct.InitialiseScaleformGalleryInputs = TRUE
//			ENDIF
//		ENDIF
//	ENDIF
//
//
//ENDPROC
//
//
//PROC SET_SCALEFORM_GALLERY_HIGHLIGHT(INT WhichTab, SCALEFORM_GALLERY& DisplayStruct)
//	DisplayStruct.iHighlightBlock = WhichTab
//ENDPROC
//
//
//PROC RUN_SCALEFORM_GALLERY_HIGHLIGHT(SCALEFORM_INDEX& ScaleformIndex,  SCALEFORM_GALLERY& DisplayStruct, BOOL Reset = FALSE )
//
//	IF Reset = TRUE
//		DisplayStruct.InitialiseScaleformGalleryHighlights = FALSE
//		RESET_REFRESH_SCALEFORM_GALLERY_HIGHLIGHT(DisplayStruct)
//	ENDIF
//
//	IF DisplayStruct.InitialiseScaleformGalleryHighlights = FALSE
////		IF DisplayStruct.iHighlightBlock != -1
//			IF HAS_SCALEFORM_MOVIE_LOADED(ScaleformIndex)
//				NET_NL()NET_PRINT("RUN_SCALEFORM_GALLERY_HIGHLIGHT: DisplayStruct.iHighlightBlock = ")NET_PRINT_INT(DisplayStruct.iHighlightBlock)NET_NL()
//
//				BEGIN_SCALEFORM_MOVIE_METHOD(ScaleformIndex, "SET_HIGHLIGHT")
//					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(DisplayStruct.iHighlightBlock)
//				END_SCALEFORM_MOVIE_METHOD()
//				DisplayStruct.iHighlightBlock = -1
//				DisplayStruct.InitialiseScaleformGalleryHighlights = TRUE
//			ENDIF
////		ENDIF
//	ENDIF
//
//ENDPROC
//
//
//
//FUNC HUD_COLOURS GET_HUD_COLOUR_FROM_AWARDPOSITION(AWARDPOSITIONS aPosition)
//	
//	SWITCH aPosition
//		CASE AWARDPOSITIONS_BRONZE
//			RETURN HUD_COLOUR_BRONZE
//		BREAK
//		CASE AWARDPOSITIONS_SILVER
//			RETURN HUD_COLOUR_SILVER
//		BREAK
//		CASE AWARDPOSITIONS_GOLD
//			RETURN HUD_COLOUR_GOLD
//		BREAK
//		CASE AWARDPOSITIONS_PLATINUM
//			RETURN HUD_COLOUR_PLATINUM
//		BREAK
//	 
//		DEFAULT 
//			RETURN HUD_COLOUR_PURE_WHITE
//		BREAK
//	ENDSWITCH
//
//	RETURN HUD_COLOUR_PURE_WHITE
//
//ENDFUNC
//
//
//PROC PRIVATE_ADD_SCALEFORM_GALLERY_TEXTURE(INT iSlot, STRING TextureName, STRING TextureDict, AWARDPOSITIONS aPosition, SCALEFORM_GALLERY& DisplayStruct)
//	DisplayStruct.TextureName[iSlot] = TextureName
//	DisplayStruct.TextureDict[iSlot] = TextureDict
//	DisplayStruct.TextureLevel[iSlot] = GET_HUD_COLOUR_FROM_AWARDPOSITION(aPosition)
//ENDPROC
//PROC PRIVATE_ADD_SCALEFORM_GALLERY_TEXT(INT iSlot, STRING Title, STRING Description,INT DescriptionNumber, SCALEFORM_GALLERY& DisplayStruct)
//	DisplayStruct.TitleLabel[iSlot] = Title
//	DisplayStruct.DescriptionLabel[iSlot] = Description
//	DisplayStruct.DescriptionNumber[iSlot] =DescriptionNumber
//ENDPROC
//
//PROC CLEAR_SCALEFORM_GALLERY(SCALEFORM_INDEX& ScaleformIndex)
//	IF HAS_SCALEFORM_MOVIE_LOADED(ScaleformIndex)
//		BEGIN_SCALEFORM_MOVIE_METHOD(ScaleformIndex, "CLEAR_GALLERY")
//		END_SCALEFORM_MOVIE_METHOD()
//	ENDIF
//ENDPROC
//
//PROC CLEANUP_SCALEFORM_GALLERY(SCALEFORM_INDEX& ScaleformIndex)
//	
//	CLEAR_SCALEFORM_GALLERY(ScaleformIndex)
//	SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(ScaleformIndex)
//	
//	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPAwards1")
//	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPAwards2")
//	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPAwards3")
//	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPAwards4")
//	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPAwards5")
//	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPMedals1")
//	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPMedals2")
//
//
//ENDPROC
//
//PROC ADD_SCALEFORM_GALLERY(STRING Title, STRING Description, STRING TextureName, STRING TextureDict,INT iDescriptionNumber, AWARDPOSITIONS aPostiion, SCALEFORM_GALLERY& DisplayStruct)
//	INT I
//	FOR I = 0 TO MAX_SCALEFORM_GALLERY_ITEMS-1
//		IF DisplayStruct.bHasValue[I] = FALSE
//			PRIVATE_ADD_SCALEFORM_GALLERY_TEXTURE(I, TextureName, TextureDict, aPostiion, DisplayStruct)
//			PRIVATE_ADD_SCALEFORM_GALLERY_TEXT(I, Title, Description,iDescriptionNumber, DisplayStruct)
//			DisplayStruct.bHasValue[I] = TRUE
//			I = MAX_SCALEFORM_GALLERY_ITEMS
//		ENDIF
//	ENDFOR
//ENDPROC
//
//FUNC BOOL HAVE_ALL_DISPLAYED_AWARD_TEXTURES_LOADED(SCALEFORM_GALLERY& DisplayStruct)
//
//	INT I
//	FOR I = 0 TO MAX_SCALEFORM_GALLERY_ITEMS-1
//		IF DisplayStruct.bHasValue[I] = TRUE
//			IF NOT IS_STRING_NULL_OR_EMPTY(DisplayStruct.TextureDict[I])
//				REQUEST_STREAMED_TEXTURE_DICT(DisplayStruct.TextureDict[I])	
//				IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED(DisplayStruct.TextureDict[I])
//					RETURN FALSE
//				ENDIF	
//			ENDIF
//		ENDIF
//	ENDFOR
//	
//	RETURN TRUE
//ENDFUNC
//
//
//PROC SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING_WITH_NUMBER(STRING param, INT Number)
//	BEGIN_TEXT_COMMAND_SCALEFORM_STRING(param)
//		ADD_TEXT_COMPONENT_INTEGER(Number)
//	END_TEXT_COMMAND_SCALEFORM_STRING()
//ENDPROC
//
//PROC RESET_SCALEFORM_GALLERY_ELEMENTS(SCALEFORM_GALLERY& DisplayStruct)
//	
//	INT I
//	FOR I = 0 TO MAX_SCALEFORM_GALLERY_ITEMS-1
//		DisplayStruct.bHasValue[I] = FALSE
//	ENDFOR
//
//ENDPROC
//
//PROC RUN_SCALEFORM_GALLERYS(SCALEFORM_INDEX& ScaleformIndex, SPRITE_PLACEMENT& aSprite, SCALEFORM_GALLERY& DisplayStruct, BOOL Reset = FALSE )
//	
//	// http://rsgediwiki1/wiki/index.php/Pause_mp_menu_awards
//	// http://rsgediwiki1/wiki/index.php/Scaleform_NEW_GTAV_Pause_Menu#MP_GALLERY
//	
//	INT I
//	
//	IF Reset = TRUE
//		NET_NL()NET_PRINT("RUN_SCALEFORM_GALLERYS: Reset = TRUE ")
//		DisplayStruct.bInitialisedGalleryVisuals = FALSE
//		CLEAR_SCALEFORM_GALLERY(ScaleformIndex)
//		RESET_REFRESH_SCALEFORM_GALLERY(DisplayStruct)
//	ENDIF
//	
//	
//	IF DisplayStruct.bInitialisedGalleryVisuals = FALSE
//		IF NOT HAS_SCALEFORM_MOVIE_LOADED(ScaleformIndex)
//			ScaleformIndex = REQUEST_SCALEFORM_MOVIE("pause_mp_menu_gallery")
//		ENDIF
//		
//		
//		IF HAS_SCALEFORM_MOVIE_LOADED(ScaleformIndex)
//			IF HAVE_ALL_DISPLAYED_AWARD_TEXTURES_LOADED(DisplayStruct)
//				NET_NL()NET_PRINT("RUN_SCALEFORM_GALLERYS: ADD_GALLERY_ITEM ")
//				
//				INT R, G, B, A
//				
//				BOOL SKipTint = FALSE
//				FOR I = 0 TO MAX_SCALEFORM_GALLERY_ITEMS-1
//				
//					GET_HUD_COLOUR(DisplayStruct.TextureLevel[I], R, G, B, A)
//					
//					IF DisplayStruct.TextureLevel[I] = HUD_COLOUR_PURE_WHITE
//
//						SKipTint = TRUE
//					ELSE
//						SKipTint = FALSE
//					ENDIF
//				
//					NET_NL()NET_PRINT("RUN_SCALEFORM_GALLERYS: SkipTint = ")NET_PRINT_BOOL(SKipTint)
//
//				
//					BEGIN_SCALEFORM_MOVIE_METHOD(ScaleformIndex, "ADD_GALLERY_ITEM")
//						IF DisplayStruct.bHasValue[I] = TRUE
//							NET_NL()NET_PRINT("RUN_SCALEFORM_GALLERYS: DisplayStruct.TextureDict[I] = ")NET_PRINT(DisplayStruct.TextureDict[I])
//							NET_NL()NET_PRINT("RUN_SCALEFORM_GALLERYS: DisplayStruct.TextureName[I] = ")NET_PRINT(DisplayStruct.TextureName[I])
//							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(I)
//							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
//							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
//							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
//							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
//							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
//							SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(DisplayStruct.TitleLabel[I])
//							IF DisplayStruct.DescriptionNumber[I] > -1
//								SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING_WITH_NUMBER(DisplayStruct.DescriptionLabel[I], DisplayStruct.DescriptionNumber[I])
//							ELSE
//								SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(DisplayStruct.DescriptionLabel[I])
//							ENDIF
//							SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(DisplayStruct.TextureDict[I])
//							SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(DisplayStruct.TextureName[I])	
//							IF SKipTint = FALSE
//							
//								NET_NL()NET_PRINT("RUN_SCALEFORM_GALLERYS: Red = ")NET_PRINT_INT(R)
//								NET_NL()NET_PRINT("RUN_SCALEFORM_GALLERYS: Green = ")NET_PRINT_INT(G)
//								NET_NL()NET_PRINT("RUN_SCALEFORM_GALLERYS: Blue = ")NET_PRINT_INT(B)
//
//							
//								SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(R) //R
//								SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(G) //G
//								SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(B) //B
//								SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(255) //A
//							ENDIF
//						ENDIF
//					END_SCALEFORM_MOVIE_METHOD()
//				ENDFOR
//				CALL_SCALEFORM_MOVIE_METHOD(ScaleformIndex, "DRAW_GALLERY")
//				
//				DisplayStruct.bInitialisedGalleryVisuals = TRUE
//			ENDIF
//		ENDIF
//	ENDIF
//
//	RUN_SCALEFORM_GALLERY_INPUT(ScaleformIndex, DisplayStruct, SHOULD_REFRESH_SCALEFORM_GALLERY_INPUT(DisplayStruct))
//	RUN_SCALEFORM_GALLERY_HIGHLIGHT(ScaleformIndex, DisplayStruct, SHOULD_REFRESH_SCALEFORM_GALLERY_HIGHLIGHT(DisplayStruct))
//
//
//	IF DisplayStruct.bInitialisedGalleryVisuals = TRUE
//		DRAW_THE_SCALEFORM_MOVIE(ScaleformIndex, aSprite)
//	ENDIF
//	
//ENDPROC











/////GALLERY WEAPONS
///    



//BOOL WIDGETBUTTONDONE
//SPRITE_PLACEMENT DebugButtonWidgetSprite

FUNC BOOL HAS_ALL_GALLERY_WEAPON_HUD_ELEMENTS_LOADED(SCALEFORM_INDEX& aMovie)


	IF NOT HAS_SCALEFORM_MOVIE_LOADED(aMovie)
		aMovie = REQUEST_SCALEFORM_MOVIE("PAUSE_MENU_PAGES_WEAPONS")
	ENDIF

	IF HAS_SCALEFORM_MOVIE_LOADED(aMovie)
		RETURN TRUE
	ENDIF
	
	IF NOT HAS_SCALEFORM_MOVIE_LOADED(aMovie)
		NET_NL()NET_PRINT("HAS_ALL_GALLERY_WEAPON_HUD_ELEMENTS_LOADED: HAS_SCALEFORM_MOVIE_LOADED HUD_SCALEFORM_GALLERY_WEAPON is FALSE")
	ENDIF

	
	RETURN FALSE
	
ENDFUNC


//BOOL WIDGETBUTTONDONE
//SPRITE_PLACEMENT DebugButtonWidgetSprite
FUNC SPRITE_PLACEMENT GET_SCALEFORM_GALLERY_WEAPON_POSITION()

	SPRITE_PLACEMENT aSprite
	
//	IF WIDGETBUTTONDONE = FALSE
//		CREATE_A_SPRITE_PLACEMENT_WIDGET(DebugButtonWidgetSprite, "GALLERY_WEAPON positions")
//		WIDGETBUTTONDONE = TRUE
//	ENDIF
	
	//Scaleform movie Bottom
	aSprite.x = 0.5
	aSprite.y = 0.523
	aSprite.w = 0.68
	aSprite.h = 0.597
	aSprite.r = 255
	aSprite.g = 255
	aSprite.b = 255
	aSprite.a = 200
	aSprite.fRotation = 0
	
//	aSprite.x += DebugButtonWidgetSprite.x
//	aSprite.y += DebugButtonWidgetSprite.y
//	aSprite.w += DebugButtonWidgetSprite.w
//	aSprite.h += DebugButtonWidgetSprite.h
//	aSprite.r += DebugButtonWidgetSprite.r
//	aSprite.g += DebugButtonWidgetSprite.g
//	aSprite.b += DebugButtonWidgetSprite.b
//	aSprite.a += DebugButtonWidgetSprite.a
//	
	RETURN aSprite

ENDFUNC

PROC RESET_SCALEFORM_GALLERY_WEAPON(SCALEFORM_GALLERY_WEAPON& DisplayStruct)
	DisplayStruct = g_EmptyGalleryWeaponStruct
ENDPROC

PROC REFRESH_SCALEFORM_GALLERY_WEAPON(SCALEFORM_GALLERY_WEAPON& DisplayStruct)
	RESET_SCALEFORM_GALLERY_WEAPON(DisplayStruct)
	DisplayStruct.bRefreshScaleformGALLERYVisuals = TRUE
ENDPROC
PROC REFRESH_SCALEFORM_GALLERY_WEAPON_INPUT(SCALEFORM_GALLERY_WEAPON& DisplayStruct)
	DisplayStruct.bRefreshScaleformGALLERYInputs = TRUE
ENDPROC
PROC REFRESH_SCALEFORM_GALLERY_WEAPON_HIGHLIGHT(SCALEFORM_GALLERY_WEAPON& DisplayStruct)
	DisplayStruct.bRefreshScaleformGALLERYHighlights = TRUE
ENDPROC
FUNC BOOL SHOULD_REFRESH_SCALEFORM_GALLERY_WEAPON(SCALEFORM_GALLERY_WEAPON& DisplayStruct)
	RETURN DisplayStruct.bRefreshScaleformGALLERYVisuals
ENDFUNC
FUNC BOOL SHOULD_REFRESH_SCALEFORM_GALLERY_WEAPON_INPUT(SCALEFORM_GALLERY_WEAPON& DisplayStruct)
	RETURN DisplayStruct.bRefreshScaleformGALLERYInputs
ENDFUNC
FUNC BOOL SHOULD_REFRESH_SCALEFORM_GALLERY_WEAPON_HIGHLIGHT(SCALEFORM_GALLERY_WEAPON& DisplayStruct)
	RETURN DisplayStruct.bRefreshScaleformGALLERYHighlights
ENDFUNC

PROC RESET_REFRESH_SCALEFORM_GALLERY_WEAPON(SCALEFORM_GALLERY_WEAPON& DisplayStruct)
	DisplayStruct.bRefreshScaleformGALLERYVisuals = FALSE
ENDPROC
PROC RESET_REFRESH_SCALEFORM_GALLERY_WEAPON_INPUT(SCALEFORM_GALLERY_WEAPON& DisplayStruct)
	DisplayStruct.bRefreshScaleformGALLERYInputs = FALSE
ENDPROC
PROC RESET_REFRESH_SCALEFORM_GALLERY_WEAPON_HIGHLIGHT(SCALEFORM_GALLERY_WEAPON& DisplayStruct)
	DisplayStruct.bRefreshScaleformGALLERYHighlights = FALSE
ENDPROC



PROC SET_SCALEFORM_GALLERY_WEAPON_INPUT(CONTROL_ACTION WhichAction, SCALEFORM_GALLERY_WEAPON& DisplayStruct)
	
	SWITCH WhichAction
		CASE INPUT_FRONTEND_UP
			DisplayStruct.ScaleformInputEventNumber = (SCALEFORM_INPUT_EVENT_UP)
		BREAK
		CASE INPUT_FRONTEND_DOWN
			DisplayStruct.ScaleformInputEventNumber = (SCALEFORM_INPUT_EVENT_DOWN)
		BREAK
		CASE INPUT_FRONTEND_LEFT
			DisplayStruct.ScaleformInputEventNumber = (SCALEFORM_INPUT_EVENT_LEFT)
		BREAK
		CASE INPUT_FRONTEND_RIGHT
			DisplayStruct.ScaleformInputEventNumber = (SCALEFORM_INPUT_EVENT_RIGHT)
		BREAK
	ENDSWITCH
ENDPROC


PROC RUN_SCALEFORM_GALLERY_WEAPON_INPUT(SCALEFORM_INDEX& ScaleformIndex,  SCALEFORM_GALLERY_WEAPON& DisplayStruct, BOOL Reset = FALSE )

	IF Reset = TRUE
		DisplayStruct.InitialiseScaleformGALLERYInputs = FALSE
		RESET_REFRESH_SCALEFORM_GALLERY_WEAPON_INPUT(DisplayStruct)
	ENDIF

	IF DisplayStruct.InitialiseScaleformGALLERYInputs = FALSE
		IF DisplayStruct.ScaleformInputEventNumber != SCALEFORM_INPUT_EVENT_INVALID
			IF HAS_SCALEFORM_MOVIE_LOADED(ScaleformIndex)
				BEGIN_SCALEFORM_MOVIE_METHOD(ScaleformIndex, "SET_INPUT_EVENT")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(DisplayStruct.ScaleformInputEventNumber))
				END_SCALEFORM_MOVIE_METHOD()
				DisplayStruct.ScaleformInputEventNumber = SCALEFORM_INPUT_EVENT_INVALID
				DisplayStruct.InitialiseScaleformGALLERYInputs = TRUE
			ENDIF
		ENDIF
	ENDIF


ENDPROC


PROC SET_SCALEFORM_GALLERY_WEAPON_HIGHLIGHT(INT WhichTab, SCALEFORM_GALLERY_WEAPON& DisplayStruct)
	DisplayStruct.iHighlightBlock = WhichTab
ENDPROC


PROC RUN_SCALEFORM_GALLERY_WEAPON_HIGHLIGHT(SCALEFORM_INDEX& ScaleformIndex,  SCALEFORM_GALLERY_WEAPON& DisplayStruct, BOOL Reset = FALSE )

	IF Reset = TRUE
		DisplayStruct.InitialiseScaleformGALLERYHighlights = FALSE
		RESET_REFRESH_SCALEFORM_GALLERY_WEAPON_HIGHLIGHT(DisplayStruct)
	ENDIF

	IF DisplayStruct.InitialiseScaleformGALLERYHighlights = FALSE
//		IF DisplayStruct.iHighlightBlock != -1
			IF HAS_SCALEFORM_MOVIE_LOADED(ScaleformIndex)
				NET_NL()NET_PRINT("RUN_SCALEFORM_GALLERY_WEAPON_HIGHLIGHT: DisplayStruct.iHighlightBlock = ")NET_PRINT_INT(DisplayStruct.iHighlightBlock)NET_NL()

				BEGIN_SCALEFORM_MOVIE_METHOD(ScaleformIndex, "SET_HIGHLIGHT")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(DisplayStruct.iHighlightBlock)
				END_SCALEFORM_MOVIE_METHOD()
				DisplayStruct.iHighlightBlock = -1
				DisplayStruct.InitialiseScaleformGALLERYHighlights = TRUE
			ENDIF
//		ENDIF
	ENDIF

ENDPROC






PROC PRIVATE_ADD_SCALEFORM_GALLERY_WEAPON_TEXTURE(INT iSlot, STRING TextureName, STRING TextureDict, SCALEFORM_GALLERY_WEAPON& DisplayStruct)
	DisplayStruct.TextureName[iSlot] = TextureName
	DisplayStruct.TextureDict[iSlot] = TextureDict
ENDPROC
PROC PRIVATE_ADD_SCALEFORM_GALLERY_WEAPON_TEXT(INT iSlot, STRING Title, STRING Description, SCALEFORM_GALLERY_WEAPON& DisplayStruct)
	DisplayStruct.TitleLabel[iSlot] = Title
	DisplayStruct.DescriptionLabel[iSlot] = Description
ENDPROC
PROC PRIVATE_ADD_SCALEFORM_GALLERY_WEAPON_KILL_STATS(INT iSlot, INT StatValue, SCALEFORM_GALLERY_WEAPON& DisplayStruct)
	DisplayStruct.KillsValue[iSlot] = StatValue
ENDPROC
PROC PRIVATE_ADD_SCALEFORM_GALLERY_WEAPON_DEATH_STATS(INT iSlot, INT StatValue, SCALEFORM_GALLERY_WEAPON& DisplayStruct)
	DisplayStruct.DeathsValue[iSlot] = StatValue
ENDPROC
PROC PRIVATE_ADD_SCALEFORM_GALLERY_WEAPON_RATING_STATS(INT iSlot, INT StatValue, SCALEFORM_GALLERY_WEAPON& DisplayStruct)
	DisplayStruct.RatingValue[iSlot] = StatValue
ENDPROC
PROC PRIVATE_ADD_SCALEFORM_GALLERY_WEAPON_KILLSVSDEATHS_STATS(INT iSlot, FLOAT StatValue, SCALEFORM_GALLERY_WEAPON& DisplayStruct)
	DisplayStruct.KillsVsDeathsValue[iSlot] = StatValue
ENDPROC
PROC PRIVATE_ADD_SCALEFORM_GALLERY_WEAPON_POWER_PC(INT iSlot, INT StatValue, SCALEFORM_GALLERY_WEAPON& DisplayStruct)
	DisplayStruct.PowerValue[iSlot] = StatValue
ENDPROC
PROC PRIVATE_ADD_SCALEFORM_GALLERY_WEAPON_SPEED_PC(INT iSlot, INT StatValue, SCALEFORM_GALLERY_WEAPON& DisplayStruct)
	DisplayStruct.SpeedPercent[iSlot] = StatValue
ENDPROC
PROC PRIVATE_ADD_SCALEFORM_GALLERY_WEAPON_CAPACITY_PC(INT iSlot, INT StatValue, SCALEFORM_GALLERY_WEAPON& DisplayStruct)
	DisplayStruct.CapacityPercent[iSlot] = StatValue
ENDPROC
PROC PRIVATE_ADD_SCALEFORM_GALLERY_WEAPON_ACCURACYVS_PC(INT iSlot, INT StatValue, SCALEFORM_GALLERY_WEAPON& DisplayStruct)
	DisplayStruct.AccuracyVSPercent[iSlot] = StatValue
ENDPROC
PROC PRIVATE_ADD_SCALEFORM_GALLERY_WEAPON_LOCKED(INT iSlot, BOOL isLocked, SCALEFORM_GALLERY_WEAPON& DisplayStruct)
	DisplayStruct.isLocked[iSlot] = isLocked
ENDPROC
PROC PRIVATE_ADD_SCALEFORM_GALLERY_WEAPON_HASHELD(INT iSlot, BOOL HasHeld, SCALEFORM_GALLERY_WEAPON& DisplayStruct)
	DisplayStruct.HasEverHeld[iSlot] = HasHeld
ENDPROC
PROC PRIVATE_ADD_SCALEFORM_GALLERY_WEAPON_ADDON(INT iSlot, INT AddonIndex, STRING AddonTexture, SCALEFORM_GALLERY_WEAPON& DisplayStruct)
	DisplayStruct.AddonTextureName[AddonIndex][iSlot] = AddonTexture
ENDPROC
PROC PRIVATE_ADD_SCALEFORM_GALLERY_WEAPON_SILLOUETTE(INT iSlot, STRING SillouetteTexture, SCALEFORM_GALLERY_WEAPON& DisplayStruct)
	DisplayStruct.TextureSillouetteName[iSlot] = SillouetteTexture
ENDPROC 

PROC CLEAR_SCALEFORM_GALLERY_WEAPON(SCALEFORM_INDEX& ScaleformIndex)
	IF HAS_SCALEFORM_MOVIE_LOADED(ScaleformIndex)
		BEGIN_SCALEFORM_MOVIE_METHOD(ScaleformIndex, "CLEAR_WEAPONS")
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC

PROC CLEANUP_SCALEFORM_GALLERY_WEAPON(SCALEFORM_INDEX& ScaleformIndex)
	
	CLEAR_SCALEFORM_GALLERY_WEAPON(ScaleformIndex)
	SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(ScaleformIndex)
	
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPWeaponsCommon")
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPWeaponsGang0")
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPWeaponsGang1")


ENDPROC

PROC ADD_SCALEFORM_GALLERY_WEAPON(STRING Title, STRING Description, STRING TextureName, STRING TextureDict, SCALEFORM_GALLERY_WEAPON& DisplayStruct)
	INT I
	FOR I = 0 TO MAX_SCALEFORM_GALLERY_WEAPON_ITEMS-1
		IF DisplayStruct.bHasValue[I] = FALSE
			PRIVATE_ADD_SCALEFORM_GALLERY_WEAPON_TEXTURE(I, TextureName, TextureDict, DisplayStruct)
			PRIVATE_ADD_SCALEFORM_GALLERY_WEAPON_TEXT(I, Title, Description, DisplayStruct)
			DisplayStruct.bHasValue[I] = TRUE
			I = MAX_SCALEFORM_GALLERY_WEAPON_ITEMS
		ENDIF
	ENDFOR
ENDPROC

FUNC BOOL HAVE_ALL_DISPLAYED_WEAPON_TEXTURES_LOADED(SCALEFORM_GALLERY_WEAPON& DisplayStruct)

	INT I
	FOR I = 0 TO MAX_SCALEFORM_GALLERY_WEAPON_ITEMS-1
		IF DisplayStruct.bHasValue[I] = TRUE
			IF NOT IS_STRING_NULL_OR_EMPTY(DisplayStruct.TextureDict[I])
				REQUEST_STREAMED_TEXTURE_DICT(DisplayStruct.TextureDict[I])	
				IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED(DisplayStruct.TextureDict[I])
					RETURN FALSE
				ENDIF	
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN TRUE
ENDFUNC

PROC RUN_SCALEFORM_GALLERY_WEAPONS(SCALEFORM_INDEX& ScaleformIndex, SPRITE_PLACEMENT& aSprite, SCALEFORM_GALLERY_WEAPON& DisplayStruct, BOOL Reset = FALSE )
	
	// http://rsgediwiki1/wiki/index.php/Pause_mp_menu_awards
	// http://rsgediwiki1/wiki/index.php/Scaleform_NEW_GTAV_Pause_Menu#MP_GALLERY_WEAPON
	
	INT I, J
	
	IF Reset = TRUE
		NET_NL()NET_PRINT("RUN_SCALEFORM_GALLERY_WEAPONS: Reset = TRUE ")
		DisplayStruct.bInitialisedGALLERYVisuals = FALSE
		CLEAR_SCALEFORM_GALLERY_WEAPON(ScaleformIndex)
		RESET_REFRESH_SCALEFORM_GALLERY_WEAPON(DisplayStruct)
	ENDIF
	

	IF DisplayStruct.bInitialisedGALLERYVisuals = FALSE
		IF NOT HAS_SCALEFORM_MOVIE_LOADED(ScaleformIndex)
			ScaleformIndex = REQUEST_SCALEFORM_MOVIE("PAUSE_MENU_PAGES_WEAPONS")
		ENDIF
		
		
		IF HAS_SCALEFORM_MOVIE_LOADED(ScaleformIndex)
			IF HAVE_ALL_DISPLAYED_WEAPON_TEXTURES_LOADED(DisplayStruct)
				NET_NL()NET_PRINT("RUN_SCALEFORM_GALLERY_WEAPONS: SET_DATA_SLOT ")
				FOR I = 0 TO MAX_SCALEFORM_GALLERY_WEAPON_ITEMS-1
				
					BOOL HasHeld
				
					IF DisplayStruct.HasEverHeld[I]
						HasHeld = FALSE
						NET_NL()NET_PRINT("DisplayStruct.HasEverHeld[") NET_PRINT_INT(I)NET_PRINT("] = TRUE")
					ELSE
						HasHeld = TRUE
						NET_NL()NET_PRINT("DisplayStruct.HasEverHeld[") NET_PRINT_INT(I)NET_PRINT("] = FALSE")
					ENDIF
					
					INT iLock 
					IF DisplayStruct.isLocked[I] = TRUE
						iLock = 0
					ELSE
						iLock = 1
					ENDIF
				
					BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND("SET_DATA_SLOT")
						IF DisplayStruct.bHasValue[I] = TRUE
						
							NET_NL()NET_PRINT("Weapon: DisplayStruct.TextureName[")NET_PRINT_INT(I)NET_PRINT("] = ")
							NET_PRINT_INT(I)NET_PRINT("] = ")NET_PRINT(DisplayStruct.TextureName[I])
							NET_NL()NET_PRINT("Weapon: DisplayStruct.TextureDict[")NET_PRINT_INT(I)NET_PRINT("] = ")
							NET_PRINT_INT(I)NET_PRINT("] = ")NET_PRINT(DisplayStruct.TextureDict[I])
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(I)
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iLock)
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(DisplayStruct.TitleLabel[I])
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(DisplayStruct.DescriptionLabel[I])
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(DisplayStruct.TextureDict[I])
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(DisplayStruct.TextureName[I])	
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(DisplayStruct.KillsValue[I])	
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(DisplayStruct.DeathsValue[I])
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(DisplayStruct.RatingValue[I])
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(DisplayStruct.KillsVsDeathsValue[I])
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(DisplayStruct.PowerValue[I])
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(DisplayStruct.SpeedPercent[I])
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(DisplayStruct.CapacityPercent[I])
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(DisplayStruct.AccuracyVsPercent[I])
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(HasHeld)
						ENDIF
					END_SCALEFORM_MOVIE_METHOD()
				ENDFOR
				BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND("DISPLAY_DATA_SLOT")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
				END_SCALEFORM_MOVIE_METHOD()
				
				FOR I = 0 TO MAX_SCALEFORM_GALLERY_WEAPON_ITEMS-1
					BEGIN_SCALEFORM_MOVIE_METHOD(ScaleformIndex, "ADD_ATTACHMENT")
						IF DisplayStruct.bHasValue[I] = TRUE
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(I)
							IF NOT IS_STRING_EMPTY_HUD(DisplayStruct.TextureDict[I])
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(DisplayStruct.TextureDict[I])
								NET_NL()NET_PRINT("Addon: DisplayStruct.TextureDict[")NET_PRINT_INT(I)NET_PRINT("] = ")
								NET_PRINT(DisplayStruct.TextureDict[I])
							ENDIF
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(DisplayStruct.TextureSillouetteName[I])
							FOR J = 0 TO MAX_SCALEFORM_GALLERY_WEAPON_ADDONS-1
								NET_NL()NET_PRINT("Addon: DisplayStruct.AddonTextureName[")NET_PRINT_INT(J)NET_PRINT("][")
								NET_PRINT_INT(I)NET_PRINT("] = ")NET_PRINT(DisplayStruct.AddonTextureName[J][I])
								IF NOT IS_STRING_EMPTY_HUD(DisplayStruct.AddonTextureName[J][I])
									SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(DisplayStruct.AddonTextureName[J][I])
								ENDIF
							ENDFOR
						ENDIF		
					END_SCALEFORM_MOVIE_METHOD()
				ENDFOR
				
				DisplayStruct.bInitialisedGALLERYVisuals = TRUE
			ENDIF
		ENDIF
	ENDIF

	RUN_SCALEFORM_GALLERY_WEAPON_INPUT(ScaleformIndex, DisplayStruct, SHOULD_REFRESH_SCALEFORM_GALLERY_WEAPON_INPUT(DisplayStruct))
	RUN_SCALEFORM_GALLERY_WEAPON_HIGHLIGHT(ScaleformIndex, DisplayStruct, SHOULD_REFRESH_SCALEFORM_GALLERY_WEAPON_HIGHLIGHT(DisplayStruct))


	IF DisplayStruct.bInitialisedGALLERYVisuals = TRUE
		DRAW_THE_SCALEFORM_MOVIE(ScaleformIndex, aSprite)
	ENDIF
//	
ENDPROC










/////LOCAL PLAYER CARD
///    
//

//BOOL WIDGETBUTTONPLAYERCARDDONE
//SPRITE_PLACEMENT DebugButtonWidgetSprite


FUNC BOOL HAS_ALL_PLAYER_CARD_HUD_ELEMENTS_LOADED(SCALEFORM_INDEX& aMovie)


	IF NOT HAS_SCALEFORM_MOVIE_LOADED(aMovie)
		aMovie = REQUEST_SCALEFORM_MOVIE("PAUSE_MP_MENU_PLAYER_CARD",FALSE)
	ENDIF


	IF HAS_SCALEFORM_MOVIE_LOADED(aMovie )

		RETURN TRUE
	ENDIF
	
	IF NOT HAS_SCALEFORM_MOVIE_LOADED(aMovie)
		NET_NL()NET_PRINT("HAS_ALL_PLAYER_CARD_HUD_ELEMENTS_LOADED: HAS_SCALEFORM_MOVIE_LOADED HUD_SCALEFORM_PLAYER_CARD is FALSE")
	ENDIF

	
	RETURN FALSE
	
ENDFUNC


FUNC SPRITE_PLACEMENT GET_SCALEFORM_PLAYER_CARD_POSITION()

	SPRITE_PLACEMENT aSprite
	
//	IF WIDGETBUTTONPLAYERCARDDONE = FALSE
//		CREATE_A_SPRITE_PLACEMENT_WIDGET(DebugButtonWidgetSprite, "PLAYER_CARD positions")
//		WIDGETBUTTONPLAYERCARDDONE = TRUE
//	ENDIF
	
	//Scaleform movie Bottom
	aSprite.x = 0.729
	aSprite.y = 0.528
	aSprite.w = 0.222
	aSprite.h = 0.5972
	aSprite.r = 255
	aSprite.g = 255
	aSprite.b = 255
	aSprite.a = 200
	aSprite.fRotation = 0
	
//	aSprite.x += DebugButtonWidgetSprite.x
//	aSprite.y += DebugButtonWidgetSprite.y
//	aSprite.w += DebugButtonWidgetSprite.w
//	aSprite.h += DebugButtonWidgetSprite.h
//	aSprite.r += DebugButtonWidgetSprite.r
//	aSprite.g += DebugButtonWidgetSprite.g
//	aSprite.b += DebugButtonWidgetSprite.b
//	aSprite.a += DebugButtonWidgetSprite.a
	
	RETURN aSprite

ENDFUNC

PROC RESET_SCALEFORM_PLAYER_CARD(SCALEFORM_PLAYER_CARD& DisplayStruct)
	SCALEFORM_PLAYER_CARD EmptyStruct
	DisplayStruct = EmptyStruct
ENDPROC

PROC REFRESH_SCALEFORM_PLAYER_CARD(SCALEFORM_PLAYER_CARD& DisplayStruct)
	RESET_SCALEFORM_PLAYER_CARD(DisplayStruct)
	DisplayStruct.bRefreshScaleformVisuals = TRUE
ENDPROC
FUNC BOOL SHOULD_REFRESH_SCALEFORM_PLAYER_CARD(SCALEFORM_PLAYER_CARD& DisplayStruct)
	RETURN DisplayStruct.bRefreshScaleformVisuals
ENDFUNC

PROC RESET_REFRESH_SCALEFORM_PLAYER_CARD(SCALEFORM_PLAYER_CARD& DisplayStruct)
	DisplayStruct.bRefreshScaleformVisuals = FALSE
ENDPROC



PROC PRIVATE_ADD_SCALEFORM_PLAYER_CARD_NAME(STRING GamerTag, SCALEFORM_PLAYER_CARD& DisplayStruct)
	DisplayStruct.Gamertag = GamerTag
ENDPROC
PROC PRIVATE_SET_SCALEFORM_PLAYER_CARD_HUD_COLOUR(HUD_COLOURS hcPassed, SCALEFORM_PLAYER_CARD& DisplayStruct)
	DisplayStruct.TeamColour = hcPassed
ENDPROC

PROC PRIVATE_ADD_SCALEFORM_PLAYER_CARD_TEAM_CODE(INT iTeam, SCALEFORM_PLAYER_CARD& DisplayStruct)
	DisplayStruct.TeamColour = GET_TEAM_HUD_COLOUR(iTeam)
	DisplayStruct.iTeamName = GET_TEAM_NAME(iTeam, FALSE, FALSE, TRUE, FALSE)
ENDPROC

PROC PRIVATE_ADD_SCALEFORM_PLAYER_CARD_RANK_AND_TITLE(INT iRank, STRING RankTitle, SCALEFORM_PLAYER_CARD& DisplayStruct)
	DisplayStruct.iRank = iRank
	DisplayStruct.RankTitle = RankTitle
ENDPROC
PROC PRIVATE_ADD_SCALEFORM_PLAYER_CARD_XP(INT iXP, SCALEFORM_PLAYER_CARD& DisplayStruct)
	DisplayStruct.iXP = iXP
ENDPROC
PROC PRIVATE_ADD_SCALEFORM_PLAYER_CARD_KILLSDEATH(FLOAT KillsDeathRatio, SCALEFORM_PLAYER_CARD& DisplayStruct)
	DisplayStruct.iKDRatio = KillsDeathRatio
ENDPROC

PROC PRIVATE_ADD_SCALEFORM_PLAYER_CARD_IS_FREEMODE(BOOL isFreemode, SCALEFORM_PLAYER_CARD& DisplayStruct)
	DisplayStruct.bIsFreemode = isFreemode
ENDPROC
PROC PRIVATE_ADD_SCALEFORM_PLAYER_CARD_ADD_KD_DETAILS(INT Kills,INT Deaths, SCALEFORM_PLAYER_CARD& DisplayStruct)
	DisplayStruct.KDKills = Kills
	DisplayStruct.KDDeath = Deaths
ENDPROC
PROC PRIVATE_ADD_SCALEFORM_PLAYER_CARD_ADD_DEATHMATCH_DETAILS(INT Wins,INT Losses, SCALEFORM_PLAYER_CARD& DisplayStruct)
	DisplayStruct.DMWins = Wins
	DisplayStruct.DMLosses = Losses
ENDPROC
PROC PRIVATE_ADD_SCALEFORM_PLAYER_CARD_ADD_RACE_DETAILS(INT Wins,INT Losses, SCALEFORM_PLAYER_CARD& DisplayStruct)
	DisplayStruct.RaceWins = Wins
	DisplayStruct.RaceLosses = Losses
ENDPROC	
PROC PRIVATE_ADD_SCALEFORM_PLAYER_CARD_ADD_ACCURACY_DETAILS(INT Fired,INT Hit, SCALEFORM_PLAYER_CARD& DisplayStruct)
	DisplayStruct.AccuracyFired = Fired
	DisplayStruct.AccuracyHit = Hit
ENDPROC	
PROC PRIVATE_ADD_SCALEFORM_PLAYER_CARD_ADD_MISSION_CREATOR_DETAILS(INT iNumberCreated, SCALEFORM_PLAYER_CARD& DisplayStruct)
	DisplayStruct.iMissionsCreated = iNumberCreated
ENDPROC
PROC PRIVATE_ADD_SCALEFORM_PLAYER_CARD_ADD_CREW_DETAILS(STRING sCrewName, STRING sCrewTag, SCALEFORM_PLAYER_CARD& DisplayStruct)
	DisplayStruct.sCrewName = sCrewName
	DisplayStruct.CrewTag = sCrewTag
ENDPROC
PROC PRIVATE_ADD_SCALEFORM_PLAYER_CARD_ADD_MISSION_TYPE(INT iMissionType, SCALEFORM_PLAYER_CARD& DisplayStruct)
	DisplayStruct.iMissionType = iMissionType
ENDPROC


PROC CLEAR_SCALEFORM_PLAYER_CARD(SCALEFORM_INDEX& ScaleformIndex)
	IF HAS_SCALEFORM_MOVIE_LOADED(ScaleformIndex)
//		BEGIN_SCALEFORM_MOVIE_METHOD(ScaleformIndex, "CLEAR_ALL")
//		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC

PROC CLEANUP_SCALEFORM_PLAYER_CARD(SCALEFORM_INDEX& ScaleformIndex)
	
	CLEAR_SCALEFORM_PLAYER_CARD(ScaleformIndex)
	SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(ScaleformIndex)

ENDPROC


FUNC STRING GET_MISSION_CREATOR_LEVEL(INT NumberOfMissionsCreated)
	
	IF NumberOfMissionsCreated < 1
		RETURN "MC_LEVEL1" //N/A
	ELIF NumberOfMissionsCreated < 5
		RETURN "MC_LEVEL2" //BRONZE
	ELIF NumberOfMissionsCreated < 10
		RETURN "MC_LEVEL3" //SILVER
	ELIF NumberOfMissionsCreated < 15
		RETURN "MC_LEVEL4" //GOLD
	ENDIF
	RETURN "MC_LEVEL5" //PLATINUM

ENDFUNC



PROC RUN_SCALEFORM_PLAYER_CARDS(SCALEFORM_INDEX& ScaleformIndex, SPRITE_PLACEMENT& aSprite, SCALEFORM_PLAYER_CARD& DisplayStruct, BOOL Reset = FALSE )
	
	// http://rsgediwiki1/wiki/index.php/Scaleform_NEW_GTAV_Pause_Menu
		

	IF Reset = TRUE
		NET_NL()NET_PRINT("RUN_SCALEFORM_PLAYER_CARDS: Reset = TRUE ")
		DisplayStruct.bInitialisedVisuals = FALSE
		CLEAR_SCALEFORM_PLAYER_CARD(ScaleformIndex)
		RESET_REFRESH_SCALEFORM_PLAYER_CARD(DisplayStruct)
	ENDIF
	
	
	IF DisplayStruct.bInitialisedVisuals = FALSE
		IF NOT HAS_SCALEFORM_MOVIE_LOADED(ScaleformIndex)
			ScaleformIndex = REQUEST_SCALEFORM_MOVIE("PAUSE_MP_MENU_PLAYER_CARD")
		ENDIF
		
		IF HAS_SCALEFORM_MOVIE_LOADED(ScaleformIndex)
		
			TEXT_LABEL_31 MissionCreatedDescription = GET_MISSION_CREATOR_LEVEL(DisplayStruct.iMissionsCreated)
			INT NextXPLevel = GET_XP_NEEDED_FOR_RANK(DisplayStruct.iRank+1)
							

			MP_ICONS_ENUMS Emblem 
			
			IF DisplayStruct.bIsFreemode = TRUE
					/////////////////////////////////////////////
				////////////////////FREEMODE///////////////////		
							
		
				Emblem  = ICON_RANK_FREEMODE
			

			
				BEGIN_SCALEFORM_MOVIE_METHOD(ScaleformIndex, "SET_TITLE")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(DisplayStruct.Gamertag)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME("")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(DisplayStruct.TeamColour))
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(Emblem))
				END_SCALEFORM_MOVIE_METHOD()
			
				BEGIN_SCALEFORM_MOVIE_METHOD(ScaleformIndex, "SET_DESCRIPTION")
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING(DisplayStruct.ReliabilityLevelString)
						ADD_TEXT_COMPONENT_INTEGER(DisplayStruct.ReliabilityLevelInt)
					END_TEXT_COMMAND_SCALEFORM_STRING()
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(DisplayStruct.ReliabilityLevel[0])
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(DisplayStruct.ReliabilityLevel[1])
				END_SCALEFORM_MOVIE_METHOD()
			
				//Rank stuff
				BEGIN_SCALEFORM_MOVIE_METHOD(ScaleformIndex, "SET_DATA_SLOT")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1) //FM
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
					
					IF GET_MP_CHARACTER_TITLE_SETTING() = MP_SETTING_TITLE_CREW
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(DisplayStruct.RankTitle)
					ELSE
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(DisplayStruct.RankTitle)
					ENDIF
					
					//SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(DisplayStruct.RankTitle)	//Job Title
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(ICON_EMPTY))
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(1.5)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(DisplayStruct.iRank)	
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(DisplayStruct.iXP)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(NextXPLevel)
				END_SCALEFORM_MOVIE_METHOD()
				
				//KD RATIO
				BEGIN_SCALEFORM_MOVIE_METHOD(ScaleformIndex, "SET_DATA_SLOT")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)//FM
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("HUD_OVER_KDR")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(LOBBY_KILLDEATH_RATIO))
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(DisplayStruct.iKDRatio)
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING("HUD_KDDEETS")
						ADD_TEXT_COMPONENT_INTEGER(DisplayStruct.KDKills)
						ADD_TEXT_COMPONENT_INTEGER(DisplayStruct.KDDeath)
					END_TEXT_COMMAND_SCALEFORM_STRING()
				END_SCALEFORM_MOVIE_METHOD()
				
			
				
				//DEATHMATCH WINS
				BEGIN_SCALEFORM_MOVIE_METHOD(ScaleformIndex, "SET_DATA_SLOT")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)//FM
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1) 
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("HUD_OVER_DMW")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(STATS_ACCURACY))
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT((TO_FLOAT(DisplayStruct.DMWins)/TO_FLOAT(DisplayStruct.DMLosses)))
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING("HUD_DMDEETS")
						ADD_TEXT_COMPONENT_INTEGER(DisplayStruct.DMWins)
						ADD_TEXT_COMPONENT_INTEGER(DisplayStruct.DMLosses)
					END_TEXT_COMMAND_SCALEFORM_STRING()
				END_SCALEFORM_MOVIE_METHOD()
				
				
				//RACE WINS
				BEGIN_SCALEFORM_MOVIE_METHOD(ScaleformIndex, "SET_DATA_SLOT")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(3)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)//FM
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1) 
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("HUD_OVER_RCW")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(STATS_RACE_WINS))
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT((TO_FLOAT(DisplayStruct.RaceWins)/TO_FLOAT(DisplayStruct.RaceLosses)))
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING("HUD_DMDEETS")
						ADD_TEXT_COMPONENT_INTEGER(DisplayStruct.RaceWins)
						ADD_TEXT_COMPONENT_INTEGER(DisplayStruct.RaceLosses)
					END_TEXT_COMMAND_SCALEFORM_STRING()
				END_SCALEFORM_MOVIE_METHOD()
				
				//ACCURACY
				BEGIN_SCALEFORM_MOVIE_METHOD(ScaleformIndex, "SET_DATA_SLOT")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(4)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)//FM
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1) 
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("HUD_OVER_ACC")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(STATS_ACCURACY))
					
					IF (DisplayStruct.AccuracyFired = 0)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0.0)
					ELSE
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT((TO_FLOAT(DisplayStruct.AccuracyHit)/(TO_FLOAT(DisplayStruct.AccuracyFired))))
					ENDIF
										
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING("HUD_ACDEETS")
						ADD_TEXT_COMPONENT_INTEGER(DisplayStruct.AccuracyFired)
						ADD_TEXT_COMPONENT_INTEGER(DisplayStruct.AccuracyHit)
					END_TEXT_COMMAND_SCALEFORM_STRING()
				END_SCALEFORM_MOVIE_METHOD()
				
				
				//Missions Created
				BEGIN_SCALEFORM_MOVIE_METHOD(ScaleformIndex, "SET_DATA_SLOT")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(5)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)//FM
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("HUD_OVER_MC")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(STATS_MISSIONSCREATED))
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(DisplayStruct.iMissionsCreated)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(MissionCreatedDescription)
				END_SCALEFORM_MOVIE_METHOD()
				
				//CREW DETAILS
				BEGIN_SCALEFORM_MOVIE_METHOD(ScaleformIndex, "SET_DATA_SLOT")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(6)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)//FM
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1) 
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("HUD_OVER_CREW")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(STATS_CREW))
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(DisplayStruct.sCrewName)
				END_SCALEFORM_MOVIE_METHOD()

			
			ENDIF
		
			
			

			DisplayStruct.bInitialisedVisuals = TRUE
		ENDIF
	ENDIF



	IF DisplayStruct.bInitialisedVisuals = TRUE
		DRAW_THE_SCALEFORM_MOVIE(ScaleformIndex, aSprite)
	ENDIF
	
ENDPROC


PROC RUN_MP_PLAYER_CARD(SCALEFORM_INDEX& ScaleformIndex, SPRITE_PLACEMENT& aSprite, SCALEFORM_PLAYER_CARD& DisplayStruct, BOOL Reset = FALSE )
	
	IF Reset = TRUE
		NET_NL()NET_PRINT("RUN_MP_PLAYER_CARD: Reset = TRUE ")
		DisplayStruct.bInitialisedVisuals = FALSE
		CLEAR_SCALEFORM_PLAYER_CARD(ScaleformIndex)
		RESET_REFRESH_SCALEFORM_PLAYER_CARD(DisplayStruct)
	ENDIF
	
	IF DisplayStruct.bInitialisedVisuals = FALSE
		IF NOT HAS_SCALEFORM_MOVIE_LOADED(ScaleformIndex)
			ScaleformIndex = REQUEST_SCALEFORM_MOVIE("MP_PLAYER_CARD")
		ENDIF
		
		IF HAS_SCALEFORM_MOVIE_LOADED(ScaleformIndex)

			MP_ICONS_ENUMS Emblem 
			
			IF DisplayStruct.bIsFreemode = TRUE
				/////////////////////////////////////////////
				////////////////////FREEMODE/////////////////	
		
				Emblem  = ICON_RANK_FREEMODE
				
				INT NextXPLevel = GET_XP_NEEDED_FOR_RANK(DisplayStruct.iRank+1)
				
				// Set title with player name and crew
				BEGIN_SCALEFORM_MOVIE_METHOD(ScaleformIndex, "SET_TITLE")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(DisplayStruct.Gamertag)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(DisplayStruct.sCrewName)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(DisplayStruct.TeamColour))
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(Emblem))
					
					IF GET_MP_CHARACTER_TITLE_SETTING() = MP_SETTING_TITLE_CREW
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(DisplayStruct.RankTitle)
					ELSE
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(DisplayStruct.RankTitle)
					ENDIF
					
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(DisplayStruct.CrewTag)
				END_SCALEFORM_MOVIE_METHOD()

				// Set up the rank of the player card
				BEGIN_SCALEFORM_MOVIE_METHOD(ScaleformIndex, "SET_DATA_SLOT")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("HUD_OVER_XP")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(DisplayStruct.iRank)	
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(DisplayStruct.iXP)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(NextXPLevel)
				END_SCALEFORM_MOVIE_METHOD()
				
				IF DisplayStruct.iMissionType != FMMC_TYPE_RACE
				AND DisplayStruct.iMissionType != FMMC_TYPE_RACE_TO_POINT
					// Other modes (should be broken down into individual modes)

					PRINTLN("PLAYER CARD: KDKills: ", DisplayStruct.KDKills, " KDDeath: ", DisplayStruct.KDDeath)
					PRINTLN("PLAYER CARD: DMWins: ", DisplayStruct.DMWins, " DMLosses: ", DisplayStruct.DMLosses)

					// Display KILL / DEATH ratio
					BEGIN_SCALEFORM_MOVIE_METHOD(ScaleformIndex, "SET_DATA_SLOT")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1) 
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("HUD_OVER_KDR")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(LOBBY_KILLDEATH_RATIO))
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(DisplayStruct.iKDRatio)
						BEGIN_TEXT_COMMAND_SCALEFORM_STRING("HUD_KDDEETS")
							ADD_TEXT_COMPONENT_INTEGER(DisplayStruct.KDKills)
							ADD_TEXT_COMPONENT_INTEGER(DisplayStruct.KDDeath)
						END_TEXT_COMMAND_SCALEFORM_STRING()
					END_SCALEFORM_MOVIE_METHOD()
					
					// Number stat -> Deathmatches played
					BEGIN_SCALEFORM_MOVIE_METHOD(ScaleformIndex, "SET_DATA_SLOT")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("HUD_OVER_DMP")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(ICON_DEATHMATCH))
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT((DisplayStruct.DMWins + DisplayStruct.DMLosses))
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")
					END_SCALEFORM_MOVIE_METHOD()
					
					// Percentage stat -> Deathmatches won
					BEGIN_SCALEFORM_MOVIE_METHOD(ScaleformIndex, "SET_DATA_SLOT")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(3)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("HUD_OVER_DMW")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(STATS_TROPHY))
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ROUND( (TO_FLOAT(DisplayStruct.DMWins) / TO_FLOAT(DisplayStruct.DMWins + DisplayStruct.DMLosses)) * 100 ))
					END_SCALEFORM_MOVIE_METHOD()
					
					// Percentage stat -> Accuracy
					BEGIN_SCALEFORM_MOVIE_METHOD(ScaleformIndex, "SET_DATA_SLOT")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(4)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("HUD_OVER_ACC")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(STATS_ACCURACY))
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ROUND( (TO_FLOAT(DisplayStruct.AccuracyHit) / TO_FLOAT(DisplayStruct.AccuracyFired)) * 100 ))
					END_SCALEFORM_MOVIE_METHOD()
										
				ELSE
				
					PRINTLN("PLAYER CARD: RaceWins: ", DisplayStruct.RaceWins, " RaceLosses: ", DisplayStruct.RaceLosses)
				
					// Race type
					// Number stat -> Races played
					BEGIN_SCALEFORM_MOVIE_METHOD(ScaleformIndex, "SET_DATA_SLOT")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("HUD_OVER_RCP")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(STATS_RACE_WINS))
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT((DisplayStruct.RaceWins + DisplayStruct.RaceLosses))
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")
					END_SCALEFORM_MOVIE_METHOD()
					
					// Percentage stat -> Deathmatches won
					BEGIN_SCALEFORM_MOVIE_METHOD(ScaleformIndex, "SET_DATA_SLOT")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("HUD_OVER_RCW")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(STATS_RACE_WINS))
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ROUND( (TO_FLOAT(DisplayStruct.RaceWins) / TO_FLOAT(DisplayStruct.RaceWins + DisplayStruct.RaceLosses)) * 100 ))
					END_SCALEFORM_MOVIE_METHOD()
				ENDIF
				
				DisplayStruct.bInitialisedVisuals = TRUE
			ENDIF
		ENDIF
	ENDIF


	IF DisplayStruct.bInitialisedVisuals = TRUE
		DRAW_THE_SCALEFORM_MOVIE(ScaleformIndex, aSprite)
	ENDIF
	
ENDPROC







/////LOCAL COMPARISON CARD
///    
//

//BOOL WIDGETBUTTONDONE
//SPRITE_PLACEMENT DebugButtonCOMPARISONWidgetSprite
//
//
//FUNC BOOL HAS_ALL_COMPARISON_CARD_HUD_ELEMENTS_LOADED(SCALEFORM_INDEX& aMovie)
//
//
//	IF NOT HAS_SCALEFORM_MOVIE_LOADED(aMovie)
//		aMovie = REQUEST_SCALEFORM_MOVIE("PAUSE_MP_MENU_PLAYER_COMPARISON",FALSE)
//	ENDIF
//
//
//	IF HAS_SCALEFORM_MOVIE_LOADED(aMovie )
//
//		RETURN TRUE
//	ENDIF
//	
//	IF NOT HAS_SCALEFORM_MOVIE_LOADED(aMovie)
//		NET_NL()NET_PRINT("HAS_ALL_COMPARISON_CARD_HUD_ELEMENTS_LOADED: HAS_SCALEFORM_MOVIE_LOADED HUD_SCALEFORM_COMPARISON_CARD is FALSE")
//	ENDIF
//
//	
//	RETURN FALSE
//	
//ENDFUNC
//
//
//FUNC SPRITE_PLACEMENT GET_SCALEFORM_COMPARISON_CARD_POSITION()
//
//	SPRITE_PLACEMENT aSprite
//	
////	IF WIDGETBUTTONDONE = FALSE
////		CREATE_A_SPRITE_PLACEMENT_WIDGET(DebugButtonCOMPARISONWidgetSprite, "COMPARISON_CARD positions")
////		WIDGETBUTTONDONE = TRUE
////	ENDIF
//	
//	//Scaleform movie Bottom
//	aSprite.x = 0.729
//	aSprite.y = 0.528
//	aSprite.w = 0.222
//	aSprite.h = 0.5972
//	aSprite.r = 255
//	aSprite.g = 255
//	aSprite.b = 255
//	aSprite.a = 200
//	aSprite.fRotation = 0
//	
////	aSprite.x += DebugButtonCOMPARISONWidgetSprite.x
////	aSprite.y += DebugButtonCOMPARISONWidgetSprite.y
////	aSprite.w += DebugButtonCOMPARISONWidgetSprite.w
////	aSprite.h += DebugButtonCOMPARISONWidgetSprite.h
////	aSprite.r += DebugButtonCOMPARISONWidgetSprite.r
////	aSprite.g += DebugButtonCOMPARISONWidgetSprite.g
////	aSprite.b += DebugButtonCOMPARISONWidgetSprite.b
////	aSprite.a += DebugButtonCOMPARISONWidgetSprite.a
//	
//	RETURN aSprite
//
//ENDFUNC
//
//PROC RESET_SCALEFORM_COMPARISON_CARD(SCALEFORM_COMPARISON_CARD& DisplayStruct)
//	SCALEFORM_COMPARISON_CARD EmptyStruct
//	DisplayStruct = EmptyStruct
//ENDPROC
//
//PROC REFRESH_SCALEFORM_COMPARISON_CARD(SCALEFORM_COMPARISON_CARD& DisplayStruct)
//	RESET_SCALEFORM_COMPARISON_CARD(DisplayStruct)
//	DisplayStruct.bRefreshScaleformVisuals = TRUE
//ENDPROC
//FUNC BOOL SHOULD_REFRESH_SCALEFORM_COMPARISON_CARD(SCALEFORM_COMPARISON_CARD& DisplayStruct)
//	RETURN DisplayStruct.bRefreshScaleformVisuals
//ENDFUNC
//
//PROC RESET_REFRESH_SCALEFORM_COMPARISON_CARD(SCALEFORM_COMPARISON_CARD& DisplayStruct)
//	DisplayStruct.bRefreshScaleformVisuals = FALSE
//ENDPROC
//
//
//
//PROC PRIVATE_ADD_SCALEFORM_COMPARISON_CARD_NAME(INT iSlot, STRING GamerTag, SCALEFORM_COMPARISON_CARD& DisplayStruct)
//	DisplayStruct.Gamertag[iSlot] = GamerTag
//ENDPROC
//
//PROC PRIVATE_ADD_SCALEFORM_COMPARISON_CARD_RANK_AND_TITLE(INT iSlot, INT iRank, STRING RankTitle, SCALEFORM_COMPARISON_CARD& DisplayStruct)
//	DisplayStruct.iRank[iSlot] = iRank
//	DisplayStruct.RankTitle[iSlot] = RankTitle
//ENDPROC
//PROC PRIVATE_ADD_SCALEFORM_COMPARISON_CARD_KILLSDEATH(INT iSlot, FLOAT KillsDeathRatio, SCALEFORM_COMPARISON_CARD& DisplayStruct)
//	DisplayStruct.iKDRatio[iSlot] = KillsDeathRatio
//ENDPROC
//PROC PRIVATE_ADD_SCALEFORM_COMPARISON_CARD_DRIVING(INT iSlot, FLOAT DrivingSkill, SCALEFORM_COMPARISON_CARD& DisplayStruct)
//	DisplayStruct.iDrivingSkill[iSlot] = DrivingSkill
//ENDPROC
//PROC PRIVATE_ADD_SCALEFORM_COMPARISON_CARD_DRIVEBY(INT iSlot, FLOAT DrivebySkill, SCALEFORM_COMPARISON_CARD& DisplayStruct)
//	DisplayStruct.iPassengerSkill[iSlot] = DrivebySkill
//ENDPROC
//PROC PRIVATE_ADD_SCALEFORM_COMPARISON_CARD_RELIABILITY(INT iSlot, FLOAT Reliability, SCALEFORM_COMPARISON_CARD& DisplayStruct)
//	DisplayStruct.iReliabilitySkill[iSlot] = Reliability
//ENDPROC
//PROC PRIVATE_ADD_SCALEFORM_COMPARISON_CARD_SHOOTING(INT iSlot, FLOAT ShootingSkill, SCALEFORM_COMPARISON_CARD& DisplayStruct)
//	DisplayStruct.iShootingSkill[iSlot] = ShootingSkill
//ENDPROC
//PROC PRIVATE_ADD_SCALEFORM_COMPARISON_CARD_HACKING(INT iSlot, FLOAT HackingSkill, SCALEFORM_COMPARISON_CARD& DisplayStruct)
//	DisplayStruct.iHackingSkill[iSlot] = HackingSkill
//ENDPROC
//PROC PRIVATE_ADD_SCALEFORM_COMPARISON_CARD_PARTNER(BOOL IsPartner, SCALEFORM_COMPARISON_CARD& DisplayStruct)
//	DisplayStruct.bIsPartner = IsPartner
//ENDPROC
//
//
//PROC CLEAR_SCALEFORM_COMPARISON_CARD(SCALEFORM_INDEX& ScaleformIndex)
//	IF HAS_SCALEFORM_MOVIE_LOADED(ScaleformIndex)
////		BEGIN_SCALEFORM_MOVIE_METHOD(ScaleformIndex, "CLEAR_ALL")
////		END_SCALEFORM_MOVIE_METHOD()
//	ENDIF
//ENDPROC
//
//PROC CLEANUP_SCALEFORM_COMPARISON_CARD(SCALEFORM_INDEX& ScaleformIndex)
//	
//	CLEAR_SCALEFORM_COMPARISON_CARD(ScaleformIndex)
//	SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(ScaleformIndex)
//
//ENDPROC
//
//
//
//
//PROC RUN_SCALEFORM_COMPARISON_CARDS(SCALEFORM_INDEX& ScaleformIndex, SPRITE_PLACEMENT& aSprite, SCALEFORM_COMPARISON_CARD& DisplayStruct, BOOL Reset = FALSE )
//	
//	// http://rsgediwiki1/wiki/index.php/Scaleform_NEW_GTAV_Pause_Menu
//		
//	IF Reset = TRUE
//		NET_NL()NET_PRINT("RUN_SCALEFORM_COMPARISON_CARDS: Reset = TRUE ")
//		DisplayStruct.bInitialisedVisuals = FALSE
//		CLEAR_SCALEFORM_COMPARISON_CARD(ScaleformIndex)
//		RESET_REFRESH_SCALEFORM_COMPARISON_CARD(DisplayStruct)
//	ENDIF
//	
//	INT I
//	
//	IF DisplayStruct.bInitialisedVisuals = FALSE
//		IF NOT HAS_SCALEFORM_MOVIE_LOADED(ScaleformIndex)
//			ScaleformIndex = REQUEST_SCALEFORM_MOVIE("PAUSE_MP_MENU_PLAYER_COMPARISON")
//		ENDIF
//		
//		IF HAS_SCALEFORM_MOVIE_LOADED(ScaleformIndex)
//		
//			BEGIN_SCALEFORM_MOVIE_METHOD(ScaleformIndex, "SET_PARTNER_UP")
//				SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(DisplayStruct.bIsPartner)
//			END_SCALEFORM_MOVIE_METHOD()
//		
//			FOR I = 0 TO MAX_NUM_COMPARISON_PLAYERS-1
//				BEGIN_SCALEFORM_MOVIE_METHOD(ScaleformIndex, "SET_DATA_SLOT")
//					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(I)
//					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
//					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(DisplayStruct.iTeam[I])
//					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)	
//					SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(DisplayStruct.Gamertag[I])
//					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(DisplayStruct.RankTitle[I])	 
//					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(DisplayStruct.iRank[I])	
//					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(DisplayStruct.iKDRatio[I])
//					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(DisplayStruct.iDrivingSkill[I])
//					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(DisplayStruct.iPassengerSkill[I])
//					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(DisplayStruct.iReliabilitySkill[I])
//					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(DisplayStruct.iShootingSkill[I])
//					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(DisplayStruct.iHackingSkill[I])
//				END_SCALEFORM_MOVIE_METHOD()
//			ENDFOR
//				
//			DisplayStruct.bInitialisedVisuals = TRUE
//		ENDIF
//	ENDIF
//
//
//
//	IF DisplayStruct.bInitialisedVisuals = TRUE
//		DRAW_THE_SCALEFORM_MOVIE(ScaleformIndex, aSprite)
//	ENDIF
//	
//ENDPROC




///PLAYERLIST 




//FUNC BOOL HAS_ALL_PLAYERLIST_HUD_ELEMENTS_LOADED(SCALEFORM_INDEX& aMovie)
//
//
//	IF NOT HAS_SCALEFORM_MOVIE_LOADED(aMovie)
//		aMovie = REQUEST_SCALEFORM_MOVIE("PAUSE_MP_MENU_VERTICAL_LIST")
//	ENDIF
//
//
//	IF HAS_SCALEFORM_MOVIE_LOADED(aMovie )
//
//		RETURN TRUE
//	ENDIF
//	
//	IF NOT HAS_SCALEFORM_MOVIE_LOADED(aMovie)
//		NET_NL()NET_PRINT("HAS_ALL_PLAYERLIST_WEAPON_HUD_ELEMENTS_LOADED: HAS_SCALEFORM_MOVIE_LOADED HUD_SCALEFORM_PLAYERLIST is FALSE")
//	ENDIF
//
//	
//	RETURN FALSE
//	
//ENDFUNC








//BLANK BIT IN SUMMERY

//BOOL WIDGETICONDONE
//SPRITE_PLACEMENT DebugICONWidgetSprite

//
//FUNC SPRITE_PLACEMENT GET_SCALEFORM_SOCIALCLUB_POSITION()
//
//	SPRITE_PLACEMENT aSprite
//	
////	IF WIDGETICONDONE = FALSE
////		CREATE_A_SPRITE_PLACEMENT_WIDGET(DebugICONWidgetSprite, "Loading Icon")
////		WIDGETICONDONE = TRUE
////	ENDIF
//	
//	//Loading Icon	
//	aSprite.x = 0.387
//	aSprite.y = 0.933
//	aSprite.w = 0.450
//	aSprite.h = 0.041
//	aSprite.r = 255
//	aSprite.g = 255
//	aSprite.b = 255
//	aSprite.a = 200
//	aSprite.fRotation = 0
//	
////	aSprite.x += DebugICONWidgetSprite.x
////	aSprite.y += DebugICONWidgetSprite.y
////	aSprite.w += DebugICONWidgetSprite.w
////	aSprite.h += DebugICONWidgetSprite.h
////	aSprite.r += DebugICONWidgetSprite.r
////	aSprite.g += DebugICONWidgetSprite.g
////	aSprite.b += DebugICONWidgetSprite.b
////	aSprite.a += DebugICONWidgetSprite.a
//	
//	RETURN aSprite
//
//ENDFUNC
//
//
//
//
//PROC RESET_SCALEFORM_SOCIALCLUB(SCALEFORM_SOCIALCLUB& DisplayStruct)
//	SCALEFORM_SOCIALCLUB EmptyStruct
//	DisplayStruct = EmptyStruct
//ENDPROC
//
//PROC REFRESH_SCALEFORM_SOCIALCLUB(SCALEFORM_SOCIALCLUB& DisplayStruct)
//	RESET_SCALEFORM_SOCIALCLUB(DisplayStruct)
//	DisplayStruct.bRefreshScaleformLoading = TRUE
//ENDPROC
//FUNC BOOL SHOULD_REFRESH_SCALEFORM_SOCIALCLUB(SCALEFORM_SOCIALCLUB& DisplayStruct)
//	RETURN DisplayStruct.bRefreshScaleformLoading
//ENDFUNC
//PROC RESET_REFRESH_SCALEFORM_SOCIALCLUB(SCALEFORM_SOCIALCLUB& DisplayStruct)
//	DisplayStruct.bRefreshScaleformLoading = FALSE
//ENDPROC
//
//
//PROC PRIVATE_ADD_SCALEFORM_SOCIALCLUB_DISPLAY_PARAM1(INT iSlot, INT ivalue, SCALEFORM_SOCIALCLUB& DisplayStruct)
//	DisplayStruct.iParam1[iSlot] = ivalue
//	DisplayStruct.bHasValue[iSlot] = TRUE
//ENDPROC
//PROC PRIVATE_ADD_SCALEFORM_SOCIALCLUB_DISPLAY_PARAM2(INT iSlot, INT ivalue, SCALEFORM_SOCIALCLUB& DisplayStruct)
//	DisplayStruct.iParam2[iSlot] = ivalue
//	DisplayStruct.bHasValue[iSlot] = TRUE
//ENDPROC
//PROC PRIVATE_ADD_SCALEFORM_SOCIALCLUB_DISPLAY_PARAM3(INT iSlot, INT ivalue, SCALEFORM_SOCIALCLUB& DisplayStruct)
//	DisplayStruct.iParam3[iSlot] = ivalue
//	DisplayStruct.bHasValue[iSlot] = TRUE
//ENDPROC
//PROC PRIVATE_ADD_SCALEFORM_SOCIALCLUB_DISPLAY_sPARAM(INT iSlot, TEXT_LABEL_63 svalue, SCALEFORM_SOCIALCLUB& DisplayStruct)
//	DisplayStruct.sParam[iSlot] = svalue
//	DisplayStruct.bHasValue[iSlot] = TRUE
//ENDPROC
//
//PROC RUN_SCALEFORM_SOCIALCLUB(SCALEFORM_INDEX& ScaleformIndex, SPRITE_PLACEMENT& aSprite, SCALEFORM_SOCIALCLUB& DisplayStruct, BOOL Reset)
//
//	IF Reset = TRUE
//		DisplayStruct.bInitialised = FALSE
//		RESET_REFRESH_SCALEFORM_SOCIALCLUB(DisplayStruct)
//	ENDIF
//	INT I
//	IF NOT HAS_SCALEFORM_MOVIE_LOADED(ScaleformIndex)
//		ScaleformIndex = REQUEST_SCALEFORM_MOVIE("PAUSE_MP_MENU_SIMPLE_LIST")
//	ENDIF
//	IF HAS_SCALEFORM_MOVIE_LOADED(ScaleformIndex)
//		
//		IF DisplayStruct.bInitialised = FALSE
//			FOR I = 0 TO MAX_SCALEFORM_SOCIALCLUB_ITEMS-1
//				IF DisplayStruct.bHasValue[I] = TRUE
//					BEGIN_SCALEFORM_MOVIE_METHOD(ScaleformIndex, "SET_DATA_SLOT")
//						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(I)
//						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(DisplayStruct.iParam1[I])
//						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(DisplayStruct.iParam2[I])
//						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(DisplayStruct.iParam3[I])
//						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(DisplayStruct.sParam[I])
//					END_SCALEFORM_MOVIE_METHOD()
//				ENDIF
//			ENDFOR
//			CALL_SCALEFORM_MOVIE_METHOD(ScaleformIndex, "DISPLAY_VIEW")
//		
//			DisplayStruct.bInitialised = TRUE
//		
//		ENDIF
//	ENDIF
//	
//	IF DisplayStruct.bInitialised = TRUE
//		DRAW_THE_SCALEFORM_MOVIE(ScaleformIndex, aSprite)
//	ENDIF
//
//ENDPROC
//
//
//
//
//
////ALERT CONFIRMATION SCREEN
//
////BOOL WIDGETICONDONEALERT
////SPRITE_PLACEMENT DebugICONWidgetSpriteALERT
//
//
//FUNC SPRITE_PLACEMENT GET_SCALEFORM_ALERTSCREEN_POSITION()
//
//	SPRITE_PLACEMENT aSprite
//	
////	IF WIDGETICONDONEALERT = FALSE
////		CREATE_A_SPRITE_PLACEMENT_WIDGET(DebugICONWidgetSpriteALERT, "Alert scaleform")
////		WIDGETICONDONEALERT = TRUE
////	ENDIF
//	
//	//Loading Icon	
//	aSprite.x = 0.5
//	aSprite.y = 0.5
//	aSprite.w = 1.0
//	aSprite.h = 1.0
//	aSprite.r = 255
//	aSprite.g = 255
//	aSprite.b = 255
//	aSprite.a = 100
//	aSprite.fRotation = 0
//	
////	aSprite.x += DebugICONWidgetSpriteALERT.x
////	aSprite.y += DebugICONWidgetSpriteALERT.y
////	aSprite.w += DebugICONWidgetSpriteALERT.w
////	aSprite.h += DebugICONWidgetSpriteALERT.h
////	aSprite.r += DebugICONWidgetSpriteALERT.r
////	aSprite.g += DebugICONWidgetSpriteALERT.g
////	aSprite.b += DebugICONWidgetSpriteALERT.b
////	aSprite.a += DebugICONWidgetSpriteALERT.a
//	
//	RETURN aSprite
//
//ENDFUNC
//
//PROC RESET_SCALEFORM_ALERTSCREEN(SCALEFORM_ALERTSCREEN& DisplayStruct)
//	SCALEFORM_ALERTSCREEN EmptyStruct
//	DisplayStruct = EmptyStruct
//ENDPROC
//
//PROC REFRESH_SCALEFORM_ALERTSCREEN(SCALEFORM_ALERTSCREEN& DisplayStruct)
//	RESET_SCALEFORM_ALERTSCREEN(DisplayStruct)
//	DisplayStruct.bRefreshScaleformLoading = TRUE
//ENDPROC
//FUNC BOOL SHOULD_REFRESH_SCALEFORM_ALERTSCREEN(SCALEFORM_ALERTSCREEN& DisplayStruct)
//	RETURN DisplayStruct.bRefreshScaleformLoading
//ENDFUNC
//PROC RESET_REFRESH_SCALEFORM_ALERTSCREEN(SCALEFORM_ALERTSCREEN& DisplayStruct)
//	DisplayStruct.bRefreshScaleformLoading = FALSE
//ENDPROC
//
//
//PROC PRIVATE_ADD_SCALEFORM_ALERTSCREEN_DISPLAY_WARNING(STRING svalue,STRING sSubValue, SCALEFORM_ALERTSCREEN& DisplayStruct)
//	DisplayStruct.sWarningMessage = svalue
//	DisplayStruct.sSubWarningMessage = sSubValue
//ENDPROC
//PROC PRIVATE_ADD_SCALEFORM_ALERTSCREEN_DISPLAY_TEXTURE(STRING sNameValue,STRING sDictValue,SCALEFORM_ALERTSCREEN& DisplayStruct)
//	DisplayStruct.sAlertTextureDict = sDictValue
//	DisplayStruct.sAlertTextureName = sNameValue
//ENDPROC
//PROC PRIVATE_ADD_SCALEFORM_ALERTSCREEN_DISPLAY_TEAM(INT iTeam, SCALEFORM_ALERTSCREEN& DisplayStruct)
//	DisplayStruct.iTextureTeam = iTeam
//ENDPROC
//PROC PRIVATE_ADD_SCALEFORM_ALERTSCREEN_PLAYER_ICON(PED_INDEX aPed, SCALEFORM_ALERTSCREEN& DisplayStruct)
//	DisplayStruct.aPed = aPed
//ENDPROC


// This COULD be sorted from code some day.

// Commented out as this will not work on PC due to the hard coded
// button icons. Steve R LDS - 23/June/2014


//FUNC INT GET_ADJUSTED_SORT_ORDER(TEXT_LABEL_63 ibn)
//	// Parallels the sorting table from code (for now)
//
//
//	SWITCH ibn
//		CASE GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_LB)		RETURN 1000
//		CASE GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_RB)		RETURN 1001
//		CASE GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_LT)		RETURN 1002
//		CASE GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_RT)		RETURN 1003
//		
//		CASE ICON_LSTICK_NONE	RETURN 1005 // LSTICK_CLICK in code
//		CASE ICON_RSTICK_NONE	RETURN 1006 // RSTICK_CLICK in code
//		
//		CASE GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE)	RETURN 1010
//		CASE GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_SELECT)	RETURN 1011
//		
//		CASE ICON_DPAD_UP		RETURN 1020
//		CASE ICON_DPAD_DOWN		RETURN 1021
//		CASE ICON_DPAD_LEFT		RETURN 1022
//		CASE ICON_DPAD_RIGHT	RETURN 1023
//		CASE ICON_DPAD_NONE		RETURN 1024
//		CASE ICON_DPAD_ALL		RETURN 1025
//		CASE ICON_DPAD_UPDOWN	RETURN 1026
//		CASE GET_CONTROL_GROUP_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUTGROUP_FRONTEND_DPAD_LR) RETURN 1027
//		
//		CASE ICON_GENERIC_ALL_ARROWS 	RETURN 1030
//		CASE ICON_GENERIC_LEFTRIGHT_ARROWS RETURN 1031
//		CASE GET_CONTROL_GROUP_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUTGROUP_FRONTEND_GENERIC_UD) RETURN 1032
//		
//		CASE GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_X)		RETURN 1040
//		CASE GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_Y)		RETURN 1041
//		CASE GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)		RETURN 1042
//		CASE GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)		RETURN 1043
//		CASE ICON_SPINNER		RETURN 1050		// Added for killstrip
//	ENDSWITCH
//	
//
//	
//	RETURN ENUM_TO_INT(ibn)
//ENDFUNC
//
//
//FUNC INT SORT_INSTRUCTIONAL_BUTTON(SCALEFORM_BUTTON& lhs, SCALEFORM_BUTTON& rhs)
//	// ignores the secondary button, because... why not
//	IF lhs.ButtonSlot = rhs.ButtonSlot
//		RETURN 0
//	ENDIF
//	
//	INT AdjLhs = GET_ADJUSTED_SORT_ORDER(lhs.ButtonSlot)
//	INT AdjRhs = GET_ADJUSTED_SORT_ORDER(rhs.ButtonSlot)
//	
//	IF AdjLhs < AdjRhs
//		RETURN 1//-1
//	ENDIF
//	
//	RETURN -1//1
//ENDFUNC
//
//
//PROC SORT_INSTRUCTIONAL_BUTTONS(SCALEFORM_INSTRUCTIONAL_BUTTONS& DisplayButtons)
//	INT low = 0
//	INT high = DisplayButtons.ButtonCount-1
//	INT bestSoFar, candidate
//	BOOL bHighBit, bLowBit
//	
//	// Simple short sort [fast for small sets]
//	WHILE high > low
//		bestSoFar = low
//		FOR candidate = low+1 TO high
//			// if bestSoFar is worst than candidate
//			IF SORT_INSTRUCTIONAL_BUTTON(DisplayButtons.Buttons[candidate], DisplayButtons.Buttons[bestSoFar]) > 0
//				// new best!
//				bestSoFar = candidate
//			ENDIF
//		ENDFOR
//		
//		// swap
//		SCALEFORM_BUTTON tempButton = DisplayButtons.Buttons[high]
//		DisplayButtons.Buttons[high] = DisplayButtons.Buttons[bestSoFar]
//		DisplayButtons.Buttons[bestSoFar] = tempButton
//		
//		bHighBit = IS_BIT_SET(DisplayButtons.iBS_ButtonSlotHasInt, high)
//		bLowBit = IS_BIT_SET(DisplayButtons.iBS_ButtonSlotHasInt, bestSoFar)
//		
//		CLEAR_BIT(DisplayButtons.iBS_ButtonSlotHasInt, high)
//		CLEAR_BIT(DisplayButtons.iBS_ButtonSlotHasInt, bestSoFar)
//		
//		IF bHighBit
//			SET_BIT(DisplayButtons.iBS_ButtonSlotHasInt, bestSoFar)
//		ENDIF
//		
//		IF bLowBit
//			SET_BIT(DisplayButtons.iBS_ButtonSlotHasInt, high)
//		ENDIF
//		
//		// pinch off the end, as we know it's the best in the list, so no need to check it again
//		high--
//	ENDWHILE
//
//ENDPROC



//
//PROC RUN_SCALEFORM_ALERTSCREEN(SCALEFORM_INDEX& ScaleformIndex, SPRITE_PLACEMENT& aSprite, SCALEFORM_ALERTSCREEN& DisplayStruct, BOOL Reset, SCALEFORM_INSTRUCTIONAL_BUTTONS& DisplayButtons )
//
//
//		
//	IF Reset = TRUE
////		UNLOAD_PEDHEADSHOT()
//		DisplayStruct.bInitialised = FALSE
//		RESET_REFRESH_SCALEFORM_ALERTSCREEN(DisplayStruct)
//		
//	ENDIF
//	
//	IF NOT HAS_SCALEFORM_MOVIE_LOADED(ScaleformIndex)
//		ScaleformIndex = REQUEST_SCALEFORM_MOVIE("popup_warning")
//	ENDIF
//
//	IF HAS_SCALEFORM_MOVIE_LOADED(ScaleformIndex)
//	
//		IF DisplayStruct.bInitialised = FALSE
//			
//			BEGIN_SCALEFORM_MOVIE_METHOD(ScaleformIndex, "SHOW_POPUP_WARNING")
//				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
//				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(DisplayStruct.sWarningMessage)
//				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(DisplayStruct.sSubWarningMessage)
//			END_SCALEFORM_MOVIE_METHOD()
//			
//			IF REGISTER_PEDHEADSHOT_TAB(DisplayStruct.aPed , DisplayStruct.sAlertTextureName, DisplayStruct.HeadshotPic)	
//				INT R,G,B
//				GET_TEAM_RGB_COLOUR(DisplayStruct.iTextureTeam, R, G, B)
//
//				BEGIN_SCALEFORM_MOVIE_METHOD(ScaleformIndex, "SET_ALERT_IMAGE_WITH_GANG_HIGHLIGHT")
//					NET_NL()NET_PRINT("SET_ALERT_IMAGE_WITH_GANG_HIGHLIGHT: DisplayStruct.sAlertTextureName =")
//					NET_PRINT(DisplayStruct.sAlertTextureName)
//					
//					SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(DisplayStruct.sAlertTextureName)
//					SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(DisplayStruct.sAlertTextureName)
//					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(DisplayStruct.iTextureTeam)
//					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(R)
//					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(G)
//					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(B)
//				END_SCALEFORM_MOVIE_METHOD()
//				
//				
//				
//				CALL_SCALEFORM_MOVIE_METHOD(ScaleformIndex, "CLEAR_ALL")
//				
//				SORT_INSTRUCTIONAL_BUTTONS(DisplayButtons)
//				
//				INT i
//				REPEAT DisplayButtons.ButtonCount i
//					NET_NL()NET_PRINT("RUN_SCALEFORM_ALERTSCREEN: DisplayButtons.DisplayButton[")NET_PRINT_INT(I)NET_PRINT("] = TRUE ")
//					IF BEGIN_SCALEFORM_MOVIE_METHOD(ScaleformIndex, "SET_DATA_SLOT")
//						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(I)
//						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT((ENUM_TO_INT(DisplayButtons.Buttons[I].ButtonSlot)))
//						IF DisplayButtons.Buttons[I].SecondaryButtonSlot != ICON_INVALID
//							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT((ENUM_TO_INT(DisplayButtons.Buttons[I].SecondaryButtonSlot)))
//						ENDIF
//						
//						IF IS_BIT_SET(DisplayButtons.iBS_ButtonSlotHasInt, I)
//							BEGIN_TEXT_COMMAND_SCALEFORM_STRING(DisplayButtons.Buttons[I].sButtonSlotString)
//								ADD_TEXT_COMPONENT_INTEGER(DisplayButtons.Buttons[I].iButtonSlotInt)
//							END_TEXT_COMMAND_SCALEFORM_STRING()
//						ELSE
//							SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(DisplayButtons.Buttons[I].sButtonSlotString)
//						ENDIF
//							
//						END_SCALEFORM_MOVIE_METHOD()
//					ENDIF
//				ENDREPEAT
//				
//				CALL_SCALEFORM_MOVIE_METHOD(ScaleformIndex, "DRAW_INSTRUCTIONAL_BUTTONS")
//				
//				
//				DisplayStruct.bInitialised = TRUE
//			ENDIF
//		
//		ENDIF
//	ELSE
//		NET_NL()NET_PRINT("RUN_SCALEFORM_ALERTSCREEN - HAS_SCALEFORM_MOVIE_LOADED = FALSE")
//	ENDIF
//	
//	IF HAS_SCALEFORM_MOVIE_LOADED(ScaleformIndex)
//		DRAW_THE_SCALEFORM_MOVIE_FULL_SCREEN(ScaleformIndex, aSprite)
//	ENDIF
//
//ENDPROC






//CREWTAG SCREEN

//BOOL WIDGETICONDONEALERT
//SPRITE_PLACEMENT DebugICONWidgetSpriteALERT


FUNC SPRITE_PLACEMENT GET_SCALEFORM_CREWTAG_POSITION()

	SPRITE_PLACEMENT aSprite
	
//	IF WIDGETICONDONEALERT = FALSE
//		CREATE_A_SPRITE_PLACEMENT_WIDGET(DebugICONWidgetSpriteALERT, "Alert scaleform")
//		WIDGETICONDONEALERT = TRUE
//	ENDIF
	
	//Loading Icon	
	aSprite.x = 0.5
	aSprite.y = 0.0
	aSprite.w = 1.0
	aSprite.h = 1.0
	aSprite.r = 255
	aSprite.g = 255
	aSprite.b = 255
	aSprite.a = 100
	aSprite.fRotation = 0
	
//	aSprite.x += DebugICONWidgetSpriteALERT.x
//	aSprite.y += DebugICONWidgetSpriteALERT.y
//	aSprite.w += DebugICONWidgetSpriteALERT.w
//	aSprite.h += DebugICONWidgetSpriteALERT.h
//	aSprite.r += DebugICONWidgetSpriteALERT.r
//	aSprite.g += DebugICONWidgetSpriteALERT.g
//	aSprite.b += DebugICONWidgetSpriteALERT.b
//	aSprite.a += DebugICONWidgetSpriteALERT.a
	
	RETURN aSprite

ENDFUNC

PROC RESET_SCALEFORM_CREWTAG(SCALEFORM_CREWTAG& DisplayStruct)
	SCALEFORM_CREWTAG EmptyStruct
	DisplayStruct = EmptyStruct
ENDPROC

PROC REFRESH_SCALEFORM_CREWTAG(SCALEFORM_CREWTAG& DisplayStruct)
	RESET_SCALEFORM_CREWTAG(DisplayStruct)
	DisplayStruct.bRefreshScaleformLoading = TRUE
ENDPROC
FUNC BOOL SHOULD_REFRESH_SCALEFORM_CREWTAG(SCALEFORM_CREWTAG& DisplayStruct)
	RETURN DisplayStruct.bRefreshScaleformLoading
ENDFUNC
PROC RESET_REFRESH_SCALEFORM_CREWTAG(SCALEFORM_CREWTAG& DisplayStruct)
	DisplayStruct.bRefreshScaleformLoading = FALSE
ENDPROC


PROC PRIVATE_ADD_SCALEFORM_CREWTAG_IS_PRIVATE(BOOL isPrivate, SCALEFORM_CREWTAG& DisplayStruct)
	DisplayStruct.bisPrivate = isPrivate
ENDPROC
PROC PRIVATE_ADD_SCALEFORM_CREWTAG_IS_ROCKSTAR(BOOL isRockstar, SCALEFORM_CREWTAG& DisplayStruct)
	DisplayStruct.bisRockstar = isRockstar
ENDPROC
PROC PRIVATE_ADD_SCALEFORM_CREWTAG_CREW_STRING(STRING sCrewTag, SCALEFORM_CREWTAG& DisplayStruct)
	DisplayStruct.sTagContent = sCrewTag
ENDPROC
PROC PRIVATE_ADD_SCALEFORM_CREWTAG_IS_FOUNDER(BOOL isFounder, SCALEFORM_CREWTAG& DisplayStruct)
	DisplayStruct.bisFounder = isFounder
ENDPROC


PROC RUN_SCALEFORM_CREWTAG(SCALEFORM_INDEX& ScaleformIndex, SPRITE_PLACEMENT& aSprite, SCALEFORM_CREWTAG& DisplayStruct, BOOL Reset)

	IF Reset = TRUE
		DisplayStruct.bInitialised = FALSE
		RESET_REFRESH_SCALEFORM_CREWTAG(DisplayStruct)
	ENDIF
	
	IF NOT HAS_SCALEFORM_MOVIE_LOADED(ScaleformIndex)
		ScaleformIndex = REQUEST_SCALEFORM_MOVIE("MP_CREW_TAG")
	ENDIF
	
		IF HAS_SCALEFORM_MOVIE_LOADED(ScaleformIndex)
			
			IF DisplayStruct.bInitialised = FALSE

				BEGIN_SCALEFORM_MOVIE_METHOD(ScaleformIndex, "SET_CREW_TAG")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(DisplayStruct.bisPrivate)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(DisplayStruct.bisRockstar)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(DisplayStruct.sTagContent)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(DisplayStruct.bisFounder)
				END_SCALEFORM_MOVIE_METHOD()
			
			
				DisplayStruct.bInitialised = TRUE
			
			ENDIF
		ENDIF
	
	
	IF DisplayStruct.bInitialised = TRUE
		DRAW_THE_SCALEFORM_MOVIE(ScaleformIndex, aSprite)
	ENDIF

ENDPROC





//BOOL WIDGETBUTTONDONE
//SPRITE_PLACEMENT DebugButtonWidgetSprite
//FLOAT WidgtetInstructionAlignX, WidgtetInstructionAlignY, WidgtetInstructionSizeX, WidgtetInstructionSizeY

FUNC SPRITE_PLACEMENT GET_SCALEFORM_INSTRUCTIONAL_BUTTON_POSITION()

	SPRITE_PLACEMENT aSprite
	
//	IF WIDGETBUTTONDONE = FALSE
//		START_WIDGET_GROUP("INSTRUCTIONAL_BUTTON")
//			CREATE_A_SPRITE_PLACEMENT_WIDGET(DebugButtonWidgetSprite, "Button positions")
//			ADD_WIDGET_FLOAT_SLIDER("WidgtetInstructionAlignX", WidgtetInstructionAlignX, -2, 2, 0.001)
//			ADD_WIDGET_FLOAT_SLIDER("WidgtetInstructionAlignY", WidgtetInstructionAlignY, -2, 2, 0.001)
//			ADD_WIDGET_FLOAT_SLIDER("WidgtetInstructionSizeX", WidgtetInstructionSizeX, -2, 2, 0.001)
//			ADD_WIDGET_FLOAT_SLIDER("WidgtetInstructionSizeY", WidgtetInstructionSizeY, -2, 2, 0.001)
//		STOP_WIDGET_GROUP()
//		WIDGETBUTTONDONE = TRUE
//	ENDIF
	
	//Scaleform movie Bottom
	aSprite.x = 0.5 //389 // Move to edge of safezone (910150)
	aSprite.y = 0.5
	aSprite.w = 1.0
	aSprite.h = 1.0
	aSprite.r = 255
	aSprite.g = 255
	aSprite.b = 255
	aSprite.a = 200
	aSprite.fRotation = 0
	
//	aSprite.x += DebugButtonWidgetSprite.x
//	aSprite.y += DebugButtonWidgetSprite.y
//	aSprite.w += DebugButtonWidgetSprite.w
//	aSprite.h += DebugButtonWidgetSprite.h
//	aSprite.r += DebugButtonWidgetSprite.r
//	aSprite.g += DebugButtonWidgetSprite.g
//	aSprite.b += DebugButtonWidgetSprite.b
//	aSprite.a += DebugButtonWidgetSprite.a
	
	RETURN aSprite

ENDFUNC

PROC RESET_SCALEFORM_INSTRUCTIONAL_BUTTON(SCALEFORM_INSTRUCTIONAL_BUTTONS& DisplayStruct)
	//SCALEFORM_INSTRUCTIONAL_BUTTONS EmptyStruct	old method removed by DaveyG 20130517 as this is beasting memory b*# 1393711
	//DisplayStruct = EmptyStruct

	DisplayStruct.bInitialised = FALSE
	
	INT iResetCount = 0
	REPEAT MAX_SCALEFORM_INSTRC_BUTTONS_INCREASED iResetCount
		DisplayStruct.Buttons[iResetCount].ButtonSlot = ""
		DisplayStruct.Buttons[iResetCount].SecondaryButtonSlot = ""
		DisplayStruct.Buttons[iResetCount].sButtonSlotString = ""
		DisplayStruct.Buttons[iResetCount].iButtonSlotInt = 0
		DisplayStruct.Buttons[iResetCount].iButtonSlotIntTwo = 0
		DisplayStruct.Buttons[iResetCount].tlGamerTag = ""
		
		DisplayStruct.Buttons[iResetCount].eButtonSlotControl	= FRONTEND_CONTROL
		DisplayStruct.Buttons[iResetCount].iButtonSlotInputA	= ENUM_TO_INT(MAX_INPUTS)
		DisplayStruct.Buttons[iResetCount].iButtonSlotInputB	= ENUM_TO_INT(MAX_INPUTS)
	ENDREPEAT
	
	DisplayStruct.iBS_ButtonSlotHasInt				= 0
	DisplayStruct.iBS_ButtonSlotHasIntTwo			= 0
	DisplayStruct.iBS_ButtonSlotHasPlayer			= 0
	
	DisplayStruct.iBS_ButtonSlotIsInputValid		= 0
	DisplayStruct.iBS_ButtonSlotIsInputClickable	= 0
	DisplayStruct.iBS_ButtonSlotIsInputGroup		= 0
	
	DisplayStruct.bRefreshInstructionalButtons = FALSE
	DisplayStruct.ButtonCount = 0
		
	DisplayStruct.AlignX = 0.0
	DisplayStruct.AlignY = 0.0
	DisplayStruct.SizeX = 0.0
	DisplayStruct.SizeY = 0.0
	
	DisplayStruct.fButtonWrap = 1.0
	
ENDPROC

PROC REFRESH_SCALEFORM_INSTRUCTIONAL_BUTTONS(SCALEFORM_INSTRUCTIONAL_BUTTONS& DisplayStruct)
	RESET_SCALEFORM_INSTRUCTIONAL_BUTTON(DisplayStruct)
	DisplayStruct.bRefreshInstructionalButtons = TRUE
ENDPROC
FUNC BOOL SHOULD_REFRESH_SCALEFORM_INSTRUCTIONAL_BUTTONS(SCALEFORM_INSTRUCTIONAL_BUTTONS& DisplayStruct)
	RETURN DisplayStruct.bRefreshInstructionalButtons
ENDFUNC
PROC RESET_REFRESH_SCALEFORM_INSTRUCTIONAL_BUTTONS(SCALEFORM_INSTRUCTIONAL_BUTTONS& DisplayStruct)
	DisplayStruct.bRefreshInstructionalButtons = FALSE
ENDPROC

PROC ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(STRING aButton, STRING TitleString, SCALEFORM_INSTRUCTIONAL_BUTTONS& DisplayStruct, STRING aSecondButton = NULL )
	IF DisplayStruct.ButtonCount >= MAX_SCALEFORM_INSTRC_BUTTONS_INCREASED
		EXIT
	ENDIF
	
	PRINTLN("ADD_SCALEFORM_INSTRUCTIONAL_BUTTON()")

	INT I = DisplayStruct.ButtonCount
	DisplayStruct.Buttons[I].ButtonSlot = aButton
	DisplayStruct.Buttons[I].SecondaryButtonSlot = aSecondButton
	DisplayStruct.Buttons[I].sButtonSlotString = TitleString
	DisplayStruct.ButtonCount++
ENDPROC

PROC ADD_SCALEFORM_INSTRUCTIONAL_BUTTON_WITH_INT(STRING aButton, STRING TitleString, INT iInt, SCALEFORM_INSTRUCTIONAL_BUTTONS& DisplayStruct, STRING aSecondButton = NULL)
	IF DisplayStruct.ButtonCount >= MAX_SCALEFORM_INSTRC_BUTTONS_INCREASED
		EXIT
	ENDIF
	
	PRINTLN("ADD_SCALEFORM_INSTRUCTIONAL_BUTTON_WITH_INT()")

	INT I = DisplayStruct.ButtonCount
	DisplayStruct.Buttons[I].ButtonSlot = aButton
	DisplayStruct.Buttons[I].SecondaryButtonSlot = aSecondButton
	DisplayStruct.Buttons[I].sButtonSlotString = TitleString
	DisplayStruct.Buttons[I].iButtonSlotInt = iInt
	SET_BIT(DisplayStruct.iBS_ButtonSlotHasInt, I)					// 1105424: Need to notify system that this text has a number to insert
	DisplayStruct.ButtonCount++
ENDPROC

PROC ADD_SCALEFORM_INSTRUCTIONAL_BUTTON_WITH_TWO_INTS(STRING aButton, STRING TitleString, INT iInt, INT iIntTwo, SCALEFORM_INSTRUCTIONAL_BUTTONS& DisplayStruct, STRING aSecondButton = NULL)
	IF DisplayStruct.ButtonCount >= MAX_SCALEFORM_INSTRC_BUTTONS_INCREASED
		EXIT
	ENDIF
	
	PRINTLN("ADD_SCALEFORM_INSTRUCTIONAL_BUTTON_WITH_TWO_INTS()")

	INT I = DisplayStruct.ButtonCount
	DisplayStruct.Buttons[I].ButtonSlot = aButton
	DisplayStruct.Buttons[I].SecondaryButtonSlot = aSecondButton
	DisplayStruct.Buttons[I].sButtonSlotString = TitleString
	DisplayStruct.Buttons[I].iButtonSlotInt = iInt
	DisplayStruct.Buttons[I].iButtonSlotIntTwo = iIntTwo
	SET_BIT(DisplayStruct.iBS_ButtonSlotHasInt, I)					// 1105424: Need to notify system that this text has a number to insert
	SET_BIT(DisplayStruct.iBS_ButtonSlotHasIntTwo, I)
	DisplayStruct.ButtonCount++
ENDPROC

PROC ADD_SCALEFORM_INSTRUCTIONAL_BUTTON_WITH_INT_AS_TIME(STRING aButton, STRING TitleString, INT iInt, SCALEFORM_INSTRUCTIONAL_BUTTONS& DisplayStruct, STRING aSecondButton = NULL)
	IF DisplayStruct.ButtonCount >= MAX_SCALEFORM_INSTRC_BUTTONS_INCREASED
		EXIT
	ENDIF
	
	PRINTLN("ADD_SCALEFORM_INSTRUCTIONAL_BUTTON_WITH_INT_AS_TIME()")

	INT I = DisplayStruct.ButtonCount
	DisplayStruct.Buttons[I].ButtonSlot = aButton
	DisplayStruct.Buttons[I].SecondaryButtonSlot = aSecondButton
	DisplayStruct.Buttons[I].sButtonSlotString = TitleString
	DisplayStruct.Buttons[I].iButtonSlotInt = iInt
	SET_BIT(DisplayStruct.iBS_ButtonSlotHasInt, I)					// 1105424: Need to notify system that this text has a number to insert
	DisplayStruct.iIntAsTime = I
	DisplayStruct.ButtonCount++
ENDPROC

PROC ADD_SCALEFORM_INSTRUCTIONAL_BUTTON_WITH_PLAYER(STRING aButton, STRING TitleString, STRING tlGamerTag, SCALEFORM_INSTRUCTIONAL_BUTTONS& DisplayStruct, STRING aSecondButton = NULL)
	IF DisplayStruct.ButtonCount >= MAX_SCALEFORM_INSTRC_BUTTONS_INCREASED
		EXIT
	ENDIF
	
	PRINTLN("ADD_SCALEFORM_INSTRUCTIONAL_BUTTON_WITH_PLAYER()")

	INT I = DisplayStruct.ButtonCount
	DisplayStruct.Buttons[I].ButtonSlot = aButton
	DisplayStruct.Buttons[I].SecondaryButtonSlot = aSecondButton
	DisplayStruct.Buttons[I].sButtonSlotString = TitleString
	DisplayStruct.Buttons[I].tlGamerTag = tlGamerTag
	SET_BIT(DisplayStruct.iBS_ButtonSlotHasPlayer, I)					// 1105424: Need to notify system that this text has a number to insert
	DisplayStruct.ButtonCount++
ENDPROC



PROC ADD_SCALEFORM_INSTRUCTIONAL_INPUT(CONTROL_TYPE eControlType, CONTROL_ACTION eInputA, STRING TitleString, SCALEFORM_INSTRUCTIONAL_BUTTONS& DisplayStruct, BOOL bIsClickable = FALSE, CONTROL_ACTION eInputB = MAX_INPUTS)
	IF DisplayStruct.ButtonCount >= MAX_SCALEFORM_INSTRC_BUTTONS_INCREASED
		EXIT
	ENDIF
	
	PRINTLN("ADD_SCALEFORM_INSTRUCTIONAL_INPUT()")

	INT I = DisplayStruct.ButtonCount
	DisplayStruct.Buttons[I].sButtonSlotString = TitleString
	
	SET_BIT(DisplayStruct.iBS_ButtonSlotIsInputValid, I)
	DisplayStruct.Buttons[I].eButtonSlotControl	= eControlType
	DisplayStruct.Buttons[I].iButtonSlotInputA	= ENUM_TO_INT(eInputA)
	DisplayStruct.Buttons[I].iButtonSlotInputB	= ENUM_TO_INT(eInputB)
	
	IF bIsClickable
		SET_BIT(DisplayStruct.iBS_ButtonSlotIsInputClickable, I)
		
		IF eInputB < MAX_INPUTS
			SCRIPT_ASSERT("ADD_SCALEFORM_INSTRUCTIONAL_INPUT() - If a button has more than one input, you shouldn't make it clickable. Only the first input will work properly when clicked on.")
		ENDIF
	ENDIF
	
	DisplayStruct.ButtonCount++
ENDPROC

PROC ADD_SCALEFORM_INSTRUCTIONAL_INPUT_WITH_INT(CONTROL_TYPE eControlType, CONTROL_ACTION eInputA, STRING TitleString, INT iInt, SCALEFORM_INSTRUCTIONAL_BUTTONS& DisplayStruct, BOOL bIsClickable = FALSE, CONTROL_ACTION eInputB = MAX_INPUTS)
	IF DisplayStruct.ButtonCount >= MAX_SCALEFORM_INSTRC_BUTTONS_INCREASED
		EXIT
	ENDIF
	
	PRINTLN("ADD_SCALEFORM_INSTRUCTIONAL_INPUT_WITH_INT()")
	
	INT I = DisplayStruct.ButtonCount
	DisplayStruct.Buttons[I].sButtonSlotString = TitleString
	
	SET_BIT(DisplayStruct.iBS_ButtonSlotIsInputValid, I)
	DisplayStruct.Buttons[I].eButtonSlotControl	= eControlType
	DisplayStruct.Buttons[I].iButtonSlotInputA	= ENUM_TO_INT(eInputA)
	DisplayStruct.Buttons[I].iButtonSlotInputB	= ENUM_TO_INT(eInputB)
	
	SET_BIT(DisplayStruct.iBS_ButtonSlotHasInt, I)					// 1105424: Need to notify system that this text has a number to insert
	DisplayStruct.Buttons[I].iButtonSlotInt = iInt

	IF bIsClickable
		SET_BIT(DisplayStruct.iBS_ButtonSlotIsInputClickable, I)
		
		IF eInputB < MAX_INPUTS
			SCRIPT_ASSERT("ADD_SCALEFORM_INSTRUCTIONAL_INPUT() - If a button has more than one input, you shouldn't make it clickable. Only the first input will work properly when clicked on.")
		ENDIF
	ENDIF
	
	DisplayStruct.ButtonCount++
ENDPROC

PROC ADD_SCALEFORM_INSTRUCTIONAL_INPUT_WITH_INT_AS_TIME(CONTROL_TYPE eControlType, CONTROL_ACTION eInputA, STRING TitleString, INT iTime, SCALEFORM_INSTRUCTIONAL_BUTTONS& DisplayStruct, BOOL bIsClickable = FALSE, CONTROL_ACTION eInputB = MAX_INPUTS)
	IF DisplayStruct.ButtonCount >= MAX_SCALEFORM_INSTRC_BUTTONS_INCREASED
		EXIT
	ENDIF
	
	PRINTLN("ADD_SCALEFORM_INSTRUCTIONAL_INPUT_WITH_INT_AS_TIME()")

	INT I = DisplayStruct.ButtonCount
	DisplayStruct.Buttons[I].sButtonSlotString = TitleString
	
	SET_BIT(DisplayStruct.iBS_ButtonSlotIsInputValid, I)
	DisplayStruct.Buttons[I].eButtonSlotControl	= eControlType
	DisplayStruct.Buttons[I].iButtonSlotInputA	= ENUM_TO_INT(eInputA)
	DisplayStruct.Buttons[I].iButtonSlotInputB	= ENUM_TO_INT(eInputB)
	
	SET_BIT(DisplayStruct.iBS_ButtonSlotHasInt, I)					// 1105424: Need to notify system that this text has a number to insert
	DisplayStruct.Buttons[I].iButtonSlotInt = iTime
	DisplayStruct.iIntAsTime = I

	IF bIsClickable
		SET_BIT(DisplayStruct.iBS_ButtonSlotIsInputClickable, I)
		
		IF eInputB < MAX_INPUTS
			SCRIPT_ASSERT("ADD_SCALEFORM_INSTRUCTIONAL_INPUT() - If a button has more than one input, you shouldn't make it clickable. Only the first input will work properly when clicked on.")
		ENDIF
	ENDIF
	
	DisplayStruct.ButtonCount++
ENDPROC

PROC ADD_SCALEFORM_INSTRUCTIONAL_INPUT_WITH_PLAYER(CONTROL_TYPE eControlType, CONTROL_ACTION eInputA, STRING TitleString, STRING sGamerTag, SCALEFORM_INSTRUCTIONAL_BUTTONS& DisplayStruct, BOOL bIsClickable = FALSE, CONTROL_ACTION eInputB = MAX_INPUTS)
	IF DisplayStruct.ButtonCount >= MAX_SCALEFORM_INSTRC_BUTTONS_INCREASED
		EXIT
	ENDIF
	
	PRINTLN("ADD_SCALEFORM_INSTRUCTIONAL_INPUT_WITH_PLAYER()")

	INT I = DisplayStruct.ButtonCount
	DisplayStruct.Buttons[I].sButtonSlotString = TitleString
	
	SET_BIT(DisplayStruct.iBS_ButtonSlotIsInputValid, I)
	DisplayStruct.Buttons[I].eButtonSlotControl	= eControlType
	DisplayStruct.Buttons[I].iButtonSlotInputA	= ENUM_TO_INT(eInputA)
	DisplayStruct.Buttons[I].iButtonSlotInputB	= ENUM_TO_INT(eInputB)
	
	SET_BIT(DisplayStruct.iBS_ButtonSlotHasPlayer, I)				// 1105424: Need to notify system that this text has a number to insert
	DisplayStruct.Buttons[I].tlGamerTag = sGamerTag

	IF bIsClickable
		SET_BIT(DisplayStruct.iBS_ButtonSlotIsInputClickable, I)
		
		IF eInputB < MAX_INPUTS
			SCRIPT_ASSERT("ADD_SCALEFORM_INSTRUCTIONAL_INPUT() - If a button has more than one input, you shouldn't make it clickable. Only the first input will work properly when clicked on.")
		ENDIF
	ENDIF
	
	DisplayStruct.ButtonCount++
ENDPROC



PROC ADD_SCALEFORM_INSTRUCTIONAL_INPUT_GROUP(CONTROL_TYPE eControlType, CONTROL_ACTION_GROUP eInputGroup, STRING TitleString, SCALEFORM_INSTRUCTIONAL_BUTTONS& DisplayStruct)
	IF DisplayStruct.ButtonCount >= MAX_SCALEFORM_INSTRC_BUTTONS_INCREASED
		EXIT
	ENDIF
	
	PRINTLN("ADD_SCALEFORM_INSTRUCTIONAL_GROUP()")

	INT I = DisplayStruct.ButtonCount
	DisplayStruct.Buttons[I].sButtonSlotString = TitleString
	
	SET_BIT(DisplayStruct.iBS_ButtonSlotIsInputValid, I)
	DisplayStruct.Buttons[I].eButtonSlotControl	= eControlType
	DisplayStruct.Buttons[I].iButtonSlotInputA	= ENUM_TO_INT(eInputGroup)
	
	SET_BIT(DisplayStruct.iBS_ButtonSlotIsInputGroup, I)
	
	DisplayStruct.ButtonCount++
ENDPROC

PROC ADD_SCALEFORM_INSTRUCTIONAL_INPUT_GROUP_WITH_INT(CONTROL_TYPE eControlType, CONTROL_ACTION_GROUP eInputGroup, STRING TitleString, INT iInt, SCALEFORM_INSTRUCTIONAL_BUTTONS& DisplayStruct)
	IF DisplayStruct.ButtonCount >= MAX_SCALEFORM_INSTRC_BUTTONS_INCREASED
		EXIT
	ENDIF
	
	PRINTLN("ADD_SCALEFORM_INSTRUCTIONAL_GROUP_WITH_INT()")

	INT I = DisplayStruct.ButtonCount
	DisplayStruct.Buttons[I].sButtonSlotString = TitleString
	
	SET_BIT(DisplayStruct.iBS_ButtonSlotIsInputValid, I)
	DisplayStruct.Buttons[I].eButtonSlotControl	= eControlType
	DisplayStruct.Buttons[I].iButtonSlotInputA	= ENUM_TO_INT(eInputGroup)
	
	SET_BIT(DisplayStruct.iBS_ButtonSlotHasInt, I)					// 1105424: Need to notify system that this text has a number to insert
	DisplayStruct.Buttons[I].iButtonSlotInt = iInt

	SET_BIT(DisplayStruct.iBS_ButtonSlotIsInputGroup, I)
	
	DisplayStruct.ButtonCount++
ENDPROC

PROC ADD_SCALEFORM_INSTRUCTIONAL_INPUT_GROUP_WITH_INT_AS_TIME(CONTROL_TYPE eControlType, CONTROL_ACTION_GROUP eInputGroup, STRING TitleString, INT iTime, SCALEFORM_INSTRUCTIONAL_BUTTONS& DisplayStruct)
	IF DisplayStruct.ButtonCount >= MAX_SCALEFORM_INSTRC_BUTTONS_INCREASED
		EXIT
	ENDIF
	
	PRINTLN("ADD_SCALEFORM_INSTRUCTIONAL_GROUP_WITH_INT_AS_TIME()")

	INT I = DisplayStruct.ButtonCount
	DisplayStruct.Buttons[I].sButtonSlotString = TitleString
	
	SET_BIT(DisplayStruct.iBS_ButtonSlotIsInputValid, I)
	DisplayStruct.Buttons[I].eButtonSlotControl	= eControlType
	DisplayStruct.Buttons[I].iButtonSlotInputA	= ENUM_TO_INT(eInputGroup)
	
	SET_BIT(DisplayStruct.iBS_ButtonSlotHasInt, I)					// 1105424: Need to notify system that this text has a number to insert
	DisplayStruct.Buttons[I].iButtonSlotInt = iTime
	DisplayStruct.iIntAsTime = I

	SET_BIT(DisplayStruct.iBS_ButtonSlotIsInputGroup, I)
	
	DisplayStruct.ButtonCount++
ENDPROC

PROC ADD_SCALEFORM_INSTRUCTIONAL_INPUT_GROUP_WITH_PLAYER(CONTROL_TYPE eControlType, CONTROL_ACTION_GROUP eInputGroup, STRING TitleString, STRING sGamerTag, SCALEFORM_INSTRUCTIONAL_BUTTONS& DisplayStruct)
	IF DisplayStruct.ButtonCount >= MAX_SCALEFORM_INSTRC_BUTTONS_INCREASED
		EXIT
	ENDIF
	
	PRINTLN("ADD_SCALEFORM_INSTRUCTIONAL_GROUP_WITH_PLAYER()")

	INT I = DisplayStruct.ButtonCount
	DisplayStruct.Buttons[I].sButtonSlotString = TitleString
	
	SET_BIT(DisplayStruct.iBS_ButtonSlotIsInputValid, I)
	DisplayStruct.Buttons[I].eButtonSlotControl	= eControlType
	DisplayStruct.Buttons[I].iButtonSlotInputA	= ENUM_TO_INT(eInputGroup)
	
	SET_BIT(DisplayStruct.iBS_ButtonSlotHasPlayer, I)				// 1105424: Need to notify system that this text has a number to insert
	DisplayStruct.Buttons[I].tlGamerTag = sGamerTag

	SET_BIT(DisplayStruct.iBS_ButtonSlotIsInputGroup, I)
	
	DisplayStruct.ButtonCount++
ENDPROC

FUNC BOOL IS_SCALEFORM_INSTRUCTIONAL_INPUT_ADDED(SCALEFORM_INSTRUCTIONAL_BUTTONS& DisplayStruct, CONTROL_TYPE eControlType, CONTROL_ACTION eInput)
				
	INT iButtonID
	REPEAT DisplayStruct.ButtonCount iButtonID

		IF IS_BIT_SET(DisplayStruct.iBS_ButtonSlotIsInputValid, iButtonID)
			
			IF NOT IS_BIT_SET(DisplayStruct.iBS_ButtonSlotIsInputGroup, iButtonID)
				IF  eControlType = DisplayStruct.Buttons[iButtonID].eButtonSlotControl
				AND eInput = INT_TO_ENUM(CONTROL_ACTION, DisplayStruct.Buttons[iButtonID].iButtonSlotInputA)
					RETURN TRUE
				ENDIF
			ENDIF
		
		ELSE
		
			IF NOT IS_STRING_NULL_OR_EMPTY(DisplayStruct.Buttons[iButtonID].ButtonSlot)
				IF ARE_STRINGS_EQUAL(DisplayStruct.Buttons[iButtonID].ButtonSlot, GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(eControlType, eInput))
					RETURN TRUE
				ENDIF
			ENDIF
		
		ENDIF
	
	ENDREPEAT
	
	RETURN FALSE

ENDFUNC

FUNC BOOL IS_SCALEFORM_INSTRUCTIONAL_INPUT_GROUP_ADDED(SCALEFORM_INSTRUCTIONAL_BUTTONS& DisplayStruct, CONTROL_TYPE eControlType, CONTROL_ACTION_GROUP eInputGroup)
				
	INT iButtonID
	REPEAT DisplayStruct.ButtonCount iButtonID

		IF IS_BIT_SET(DisplayStruct.iBS_ButtonSlotIsInputValid, iButtonID)
			
			IF IS_BIT_SET(DisplayStruct.iBS_ButtonSlotIsInputGroup, iButtonID)
				IF  eControlType = DisplayStruct.Buttons[iButtonID].eButtonSlotControl
				AND eInputGroup = INT_TO_ENUM(CONTROL_ACTION_GROUP, DisplayStruct.Buttons[iButtonID].iButtonSlotInputA)
					RETURN TRUE
				ENDIF
			ENDIF
		
		ELSE
		
			IF NOT IS_STRING_NULL_OR_EMPTY(DisplayStruct.Buttons[iButtonID].ButtonSlot)
				IF ARE_STRINGS_EQUAL(DisplayStruct.Buttons[iButtonID].ButtonSlot, GET_CONTROL_GROUP_INSTRUCTIONAL_BUTTONS_STRING(eControlType, eInputGroup))
					RETURN TRUE
				ENDIF
			ENDIF
		
		ENDIF
	
	ENDREPEAT
	
	RETURN FALSE

ENDFUNC

PROC SET_SCALEFORM_INSTRUCTIONAL_BUTTON_WRAP(SCALEFORM_INSTRUCTIONAL_BUTTONS& DisplayStruct, FLOAT fWrapPercent)
	DisplayStruct.fButtonWrap = fWrapPercent
ENDPROC

PROC UPDATE_SCALEFORM_INSTRUCTION_BUTTON_SPINNER(SCALEFORM_INDEX& ScaleformIndex, INT iInt, SCALEFORM_INSTRUCTIONAL_BUTTONS& DisplayStruct, BOOL bUpdateTimerOnly = FALSE)

	// no point doing anything if nothing's got ints
	IF DisplayStruct.iBS_ButtonSlotHasInt = 0
		EXIT
	ENDIF
	
	INT I
	
	IF HAS_SCALEFORM_MOVIE_LOADED(ScaleformIndex)
		REPEAT DisplayStruct.ButtonCount I
			IF IS_BIT_SET(DisplayStruct.iBS_ButtonSlotHasInt, I) //DisplayStruct.Buttons[I].iButtonSlotInt != -1
				
				IF bUpdateTimerOnly AND DisplayStruct.iIntAsTime != I
					RELOOP
				ENDIF
				
				IF BEGIN_SCALEFORM_MOVIE_METHOD(ScaleformIndex, "OVERRIDE_RESPAWN_TEXT")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(I)
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING(DisplayStruct.Buttons[I].sButtonSlotString)
					
						IF DisplayStruct.iIntAsTime = I
							ADD_TEXT_COMPONENT_SUBSTRING_TIME(iInt, TIME_FORMAT_MINUTES|TIME_FORMAT_SECONDS|TEXT_FORMAT_HIDE_LEADING_ZEROS_ON_LEADING_UNITS)
						ELSE
							ADD_TEXT_COMPONENT_INTEGER(iInt)
						ENDIF
					END_TEXT_COMMAND_SCALEFORM_STRING()
					END_SCALEFORM_MOVIE_METHOD()
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC

PROC RUN_SCALEFORM_INSTRUCTIONAL_BUTTONS(SCALEFORM_INDEX& ScaleformIndex, SPRITE_PLACEMENT& aSprite, SCALEFORM_INSTRUCTIONAL_BUTTONS& DisplayStruct, BOOL Reset = FALSE )
	
	IF Reset = TRUE OR HAVE_CONTROLS_CHANGED(FRONTEND_CONTROL)
		DisplayStruct.bInitialised = FALSE
		IF HAS_SCALEFORM_MOVIE_LOADED(ScaleformIndex)
			
			PRINTLN("RUN_SCALEFORM_INSTRUCTIONAL_BUTTONS() - Reseting buttons")
			
			IF IS_PC_VERSION()
				PRINTLN("RUN_SCALEFORM_INSTRUCTIONAL_BUTTONS() - Turning OFF mouse clickable buttons")
				BEGIN_SCALEFORM_MOVIE_METHOD(ScaleformIndex, "TOGGLE_MOUSE_BUTTONS")
		    		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
				END_SCALEFORM_MOVIE_METHOD()
			ENDIF

			BEGIN_SCALEFORM_MOVIE_METHOD(ScaleformIndex, "CLEAR_ALL")
			END_SCALEFORM_MOVIE_METHOD()
		
		ENDIF
		RESET_REFRESH_SCALEFORM_INSTRUCTIONAL_BUTTONS(DisplayStruct)
		
	ENDIF
	IF g_HasLoadingIconSubtitlesAboveButton < 2
		SET_LOADING_ICON_SUBTITLES_OFFSET_SHIFT_THIS_FRAME()
	ENDIF
	
	IF DisplayStruct.bInitialised = FALSE
	
		IF NOT HAS_SCALEFORM_MOVIE_LOADED(ScaleformIndex)
//			NET_NL()NET_PRINT("RUN_SCALEFORM_INSTRUCTIONAL_BUTTONS - REQUEST_SCALEFORM_MOVIE")
			ScaleformIndex = REQUEST_SCALEFORM_MOVIE("instructional_buttons")
		ENDIF

		IF HAS_SCALEFORM_MOVIE_LOADED(ScaleformIndex)
		
			PRINTLN("RUN_SCALEFORM_INSTRUCTIONAL_BUTTONS() - Adding buttons...")

//			NET_NL()NET_PRINT("RUN_SCALEFORM_INSTRUCTIONAL_BUTTONS - REQUEST_SCALEFORM_MOVIE HAS LOADED")
			
			//SORT_INSTRUCTIONAL_BUTTONS(DisplayStruct) - Commented out for now as this will not work on PC - Steve R 23/June/2014

			CALL_SCALEFORM_MOVIE_METHOD(ScaleformIndex, "CLEAR_ALL")
			
			// Clickable buttons for PC keyboard and mouse
			IF IS_PC_VERSION()
				PRINTLN("RUN_SCALEFORM_INSTRUCTIONAL_BUTTONS() - Turning ON mouse clickable buttons")
				BEGIN_SCALEFORM_MOVIE_METHOD(ScaleformIndex, "TOGGLE_MOUSE_BUTTONS")
    				SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
				END_SCALEFORM_MOVIE_METHOD()
			ENDIF
			
			INT I = 0
			REPEAT DisplayStruct.ButtonCount I
				
				// If button input action is stored as enum...
				IF IS_BIT_SET(DisplayStruct.iBS_ButtonSlotIsInputValid, I)
				
					BEGIN_SCALEFORM_MOVIE_METHOD(ScaleformIndex, "SET_DATA_SLOT")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(I)

						// Setup decal
						IF NOT IS_BIT_SET(DisplayStruct.iBS_ButtonSlotIsInputGroup, I)
							
							// Add input decal(s)
							CONTROL_TYPE			eControlType	= DisplayStruct.Buttons[I].eButtonSlotControl
							CONTROL_ACTION			eInputA			= INT_TO_ENUM(CONTROL_ACTION, DisplayStruct.Buttons[I].iButtonSlotInputA)
							CONTROL_ACTION			eInputB			= INT_TO_ENUM(CONTROL_ACTION, DisplayStruct.Buttons[I].iButtonSlotInputB)
							
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INSTRUCTIONAL_BUTTONS( GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(eControlType, eInputA) )
							
							IF eInputB < MAX_INPUTS
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_INSTRUCTIONAL_BUTTONS( GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(eControlType, eInputB) )
							ENDIF
						
						ELSE
						
							// Add group input decal
							CONTROL_TYPE			eControlType	= DisplayStruct.Buttons[I].eButtonSlotControl
							CONTROL_ACTION_GROUP	eInputGroup		= INT_TO_ENUM(CONTROL_ACTION_GROUP, DisplayStruct.Buttons[I].iButtonSlotInputA)

							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INSTRUCTIONAL_BUTTONS( GET_CONTROL_GROUP_INSTRUCTIONAL_BUTTONS_STRING(eControlType, eInputGroup) )
						
						ENDIF
					
						// Setup label
						IF IS_BIT_SET(DisplayStruct.iBS_ButtonSlotHasInt, I)
						
							// Add label (with int)
							BEGIN_TEXT_COMMAND_SCALEFORM_STRING(DisplayStruct.Buttons[I].sButtonSlotString)
								IF DisplayStruct.iIntAsTime = I
									ADD_TEXT_COMPONENT_SUBSTRING_TIME(DisplayStruct.Buttons[I].iButtonSlotInt, TIME_FORMAT_MINUTES|TIME_FORMAT_SECONDS|TEXT_FORMAT_HIDE_LEADING_ZEROS_ON_LEADING_UNITS)
								ELSE
									ADD_TEXT_COMPONENT_INTEGER(DisplayStruct.Buttons[I].iButtonSlotInt)
									
									IF IS_BIT_SET(DisplayStruct.iBS_ButtonSlotHasIntTwo, I)
										ADD_TEXT_COMPONENT_INTEGER(DisplayStruct.Buttons[I].iButtonSlotIntTwo)
									ENDIF
								ENDIF

							END_TEXT_COMMAND_SCALEFORM_STRING()
							
						ELIF IS_BIT_SET(DisplayStruct.iBS_ButtonSlotHasPlayer, I)
							
							// Add label (with player name)
							BEGIN_TEXT_COMMAND_SCALEFORM_STRING(DisplayStruct.Buttons[I].sButtonSlotString)
								ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(DisplayStruct.Buttons[I].tlGamerTag)
							END_TEXT_COMMAND_SCALEFORM_STRING()
							
						ELSE
							
							// Add label (no modifiers)
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(DisplayStruct.Buttons[I].sButtonSlotString)
							
						ENDIF
							
						// Setup clickable input
						IF IS_PC_VERSION()
							IF IS_BIT_SET(DisplayStruct.iBS_ButtonSlotIsInputClickable, I)
								
								// Make clickable
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(DisplayStruct.Buttons[I].iButtonSlotInputA)

							ELSE
								
								// Make unclickable
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(MAX_INPUTS))
							
							ENDIF
						ENDIF

					END_SCALEFORM_MOVIE_METHOD()
					
				// If button input action is stored as string...
				ELSE
					BEGIN_SCALEFORM_MOVIE_METHOD(ScaleformIndex, "SET_DATA_SLOT")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(I)
	//					IF DisplayStruct.Buttons[I].ButtonSlot != ICON_INVALID
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INSTRUCTIONAL_BUTTONS(DisplayStruct.Buttons[I].ButtonSlot)
	//					ENDIF
						IF NOT IS_STRING_NULL_OR_EMPTY(DisplayStruct.Buttons[I].SecondaryButtonSlot)
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INSTRUCTIONAL_BUTTONS(DisplayStruct.Buttons[I].SecondaryButtonSlot)
						ENDIF
						
						IF IS_BIT_SET(DisplayStruct.iBS_ButtonSlotHasInt, I)
						
							BEGIN_TEXT_COMMAND_SCALEFORM_STRING(DisplayStruct.Buttons[I].sButtonSlotString)
							
								IF DisplayStruct.iIntAsTime = I
									ADD_TEXT_COMPONENT_SUBSTRING_TIME(DisplayStruct.Buttons[I].iButtonSlotInt, TIME_FORMAT_MINUTES|TIME_FORMAT_SECONDS|TEXT_FORMAT_HIDE_LEADING_ZEROS_ON_LEADING_UNITS)
								ELSE
									ADD_TEXT_COMPONENT_INTEGER(DisplayStruct.Buttons[I].iButtonSlotInt)
									
									IF IS_BIT_SET(DisplayStruct.iBS_ButtonSlotHasIntTwo, I)
										ADD_TEXT_COMPONENT_INTEGER(DisplayStruct.Buttons[I].iButtonSlotIntTwo)
									ENDIF
								ENDIF
							END_TEXT_COMMAND_SCALEFORM_STRING()
						ELIF IS_BIT_SET(DisplayStruct.iBS_ButtonSlotHasPlayer, I)
							BEGIN_TEXT_COMMAND_SCALEFORM_STRING(DisplayStruct.Buttons[I].sButtonSlotString)
								ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(DisplayStruct.Buttons[I].tlGamerTag)
							END_TEXT_COMMAND_SCALEFORM_STRING()
						ELSE
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(DisplayStruct.Buttons[I].sButtonSlotString)
						ENDIF
							
						// Make unclickable
						IF IS_PC_VERSION()
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(MAX_INPUTS))
						ENDIF

					END_SCALEFORM_MOVIE_METHOD()
				ENDIF
			ENDREPEAT
			
			BEGIN_SCALEFORM_MOVIE_METHOD(ScaleformIndex, "SET_MAX_WIDTH")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(DisplayStruct.fButtonWrap)
			END_SCALEFORM_MOVIE_METHOD()
			
			BEGIN_SCALEFORM_MOVIE_METHOD(ScaleformIndex, "DRAW_INSTRUCTIONAL_BUTTONS")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
			END_SCALEFORM_MOVIE_METHOD()
			
			DisplayStruct.bInitialised = TRUE
		ENDIF
	ENDIF
	
	DisplayStruct.AlignX = 0.050 
	DisplayStruct.AlignY = 0.045 
	DisplayStruct.SizeX = 0 
	DisplayStruct.SizeY = 0
	
//	DisplayStruct.AlignX += WidgtetInstructionAlignX 
//	DisplayStruct.AlignY += WidgtetInstructionAlignY 
//	DisplayStruct.SizeX += WidgtetInstructionSizeX 
//	DisplayStruct.SizeY += WidgtetInstructionSizeY

	IF DisplayStruct.bInitialised = TRUE
	
//		SET_SCRIPT_GFX_ALIGN(UI_ALIGN_RIGHT, UI_ALIGN_BOTTOM)
//		SET_SCRIPT_GFX_ALIGN_PARAMS(DisplayStruct.AlignX, DisplayStruct.AlignY, DisplayStruct.SizeX, DisplayStruct.SizeY)
		DRAW_THE_SCALEFORM_MOVIE_FULL_SCREEN(ScaleformIndex, aSprite)
//		RESET_SCRIPT_GFX_ALIGN()
	
	ENDIF
	
	HIDE_ALL_BOTTOM_RIGHT_HUD()
ENDPROC

CONST_INT USE_JEFFS_NEW_MOVIE_FOR_FREEMODE		1

/// PURPOSE: Returns TRUE when scaleform movie has loaded.
FUNC BOOL REQUEST_SCALEFORM_SUMMARY_CARD(BOOL bUseJeffsNewFreemodeMovie, SCALEFORM_INDEX &siMovie, BOOL bUseSpectatorCard = FALSE)

	IF bUseJeffsNewFreemodeMovie
		#IF USE_JEFFS_NEW_MOVIE_FOR_FREEMODE
		siMovie = REQUEST_SCALEFORM_MOVIE("mp_mm_card_freemode")
		#ENDIF	//	USE_JEFFS_NEW_MOVIE_FOR_FREEMODE

		#IF NOT USE_JEFFS_NEW_MOVIE_FOR_FREEMODE
		siMovie = REQUEST_SCALEFORM_MOVIE("mp_matchmaking_card")
		#ENDIF	//	NOT USE_JEFFS_NEW_MOVIE_FOR_FREEMODE
	
	ELIF bUseSpectatorCard
		siMovie = REQUEST_SCALEFORM_MOVIE("MP_SPECTATOR_CARD")
	ELSE
		siMovie = REQUEST_SCALEFORM_MOVIE("mp_matchmaking_card")
	ENDIF
	
	RETURN HAS_SCALEFORM_MOVIE_LOADED(siMovie)

ENDFUNC


FUNC BOOL b_MODE_HAS_SCORES()

	SWITCH GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID())

//		CASE FMMC_TYPE_BIKER_JOUST				
//		CASE FMMC_TYPE_BIKER_UNLOAD_WEAPONS	
//		CASE FMMC_TYPE_BIKER_DEAL_GONE_WRONG	
//		CASE FMMC_TYPE_BIKER_RESCUE_CONTACT	
//		CASE FMMC_TYPE_RACE_STUNT_FOR_QM		
//		CASE FMMC_TYPE_BIKER_LAST_RESPECTS		
//		CASE FMMC_TYPE_BIKER_CONTRACT_KILLING	
//		CASE FMMC_TYPE_VEHICLE_EXPORT_SELL		
//		CASE FMMC_TYPE_BIKER_RACE_P2P			
//		CASE FMMC_TYPE_BIKER_CONTRABAND_SELL	
//		CASE FMMC_TYPE_BIKER_CONTRABAND_DEFEND	
//		CASE FMMC_TYPE_BIKER_BUY				
		CASE FMMC_TYPE_BIKER_DRIVEBY_ASSASSIN	
		CASE FMMC_TYPE_BIKER_RIPPIN_IT_UP		
//		CASE FMMC_TYPE_BIKER_STEAL_BIKES		
//		CASE FMMC_TYPE_BIKER_FREE_PRISONER		
//		CASE FMMC_TYPE_BIKER_SAFECRACKER		
		CASE FMMC_TYPE_BIKER_SEARCH_AND_DESTROY
//		CASE FMMC_TYPE_BIKER_CAGED_IN			
//		CASE FMMC_TYPE_BIKER_STAND_YOUR_GROUND	
//		CASE FMMC_TYPE_ADVERSARY_SERIES		
//		CASE FMMC_TYPE_ADVERSARY_SERIES_1		
//		CASE FMMC_TYPE_ADVERSARY_SERIES_2		
		CASE FMMC_TYPE_BIKER_CRIMINAL_MISCHIEF	
//		CASE FMMC_TYPE_FEATURED_ADVERSARY		
//		CASE FMMC_TYPE_BIKER_DESTROY_VANS		
//		CASE FMMC_TYPE_BIKER_BURN_ASSETS		
//		CASE FMMC_TYPE_BIKER_SHUTTLE			
		CASE FMMC_TYPE_BIKER_WHEELIE_RIDER		
//		CASE FMMC_TYPE_BIKER_ON_THE_RUN		
		CASE FMMC_TYPE_VEHICLE_EXPORT_SELL
			RETURN TRUE
		
	ENDSWITCH
	
	RETURN FALSE

ENDFUNC

// Just draw unsorted group of Challenge players (for modes where there is no scores etc.)
FUNC BOOL SHOULD_DRAW_UNSORTED_LBD()

	SWITCH GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID())

		CASE FMMC_TYPE_BIKER_JOUST				
		CASE FMMC_TYPE_BIKER_UNLOAD_WEAPONS	
		CASE FMMC_TYPE_BIKER_DEAL_GONE_WRONG	
		CASE FMMC_TYPE_BIKER_RESCUE_CONTACT	
		CASE FMMC_TYPE_RACE_STUNT_FOR_QM		
		CASE FMMC_TYPE_BIKER_LAST_RESPECTS		
		CASE FMMC_TYPE_BIKER_CONTRACT_KILLING	
//		CASE FMMC_TYPE_VEHICLE_EXPORT_SELL		
		CASE FMMC_TYPE_BIKER_RACE_P2P			
		CASE FMMC_TYPE_BIKER_CONTRABAND_SELL	
		CASE FMMC_TYPE_BIKER_CONTRABAND_DEFEND	
		CASE FMMC_TYPE_BIKER_BUY				
//		CASE FMMC_TYPE_BIKER_DRIVEBY_ASSASSIN	
//		CASE FMMC_TYPE_BIKER_RIPPIN_IT_UP		
		CASE FMMC_TYPE_BIKER_STEAL_BIKES		
		CASE FMMC_TYPE_BIKER_FREE_PRISONER		
		CASE FMMC_TYPE_BIKER_SAFECRACKER		
//		CASE FMMC_TYPE_BIKER_SEARCH_AND_DESTROY
		CASE FMMC_TYPE_BIKER_CAGED_IN			
		CASE FMMC_TYPE_BIKER_STAND_YOUR_GROUND	
		CASE FMMC_TYPE_ADVERSARY_SERIES		
		CASE FMMC_TYPE_ADVERSARY_SERIES_1		
		CASE FMMC_TYPE_ADVERSARY_SERIES_2		
//		CASE FMMC_TYPE_BIKER_CRIMINAL_MISCHIEF	
		CASE FMMC_TYPE_FEATURED_ADVERSARY		
		CASE FMMC_TYPE_BIKER_DESTROY_VANS		
		CASE FMMC_TYPE_BIKER_BURN_ASSETS		
		CASE FMMC_TYPE_BIKER_SHUTTLE			
//		CASE FMMC_TYPE_BIKER_WHEELIE_RIDER		
		CASE FMMC_TYPE_BIKER_ON_THE_RUN		
			RETURN TRUE
		
	ENDSWITCH

	SWITCH FM_EVENT_GET_PLAYER_CURRENT_FM_EVENT(PLAYER_ID())
		CASE FMMC_TYPE_MOVING_TARGET	
		CASE FMMC_TYPE_DEAD_DROP
		CASE FMMC_TYPE_HOLD_THE_WHEEL			
		CASE FMMC_TYPE_HUNT_THE_BEAST
		
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	
	IF GB_IS_PLAYER_PERMANENT_PARTICIPANT(PLAYER_ID())
		SWITCH GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID())
			CASE FMMC_TYPE_GB_BOSS_DEATHMATCH		
			CASE FMMC_TYPE_GB_TERMINATE		
			CASE FMMC_TYPE_GB_YACHT_ROBBERY	
			CASE FMMC_TYPE_GB_CHAL_FIVESTAR
			CASE FMMC_TYPE_GB_BELLY_BEAST	
			CASE FMMC_TYPE_GB_ASSAULT	
			CASE FMMC_TYPE_GB_CHAL_P2P		
			CASE FMMC_TYPE_GB_HUNT_THE_BOSS	
			CASE FMMC_TYPE_GB_SIGHTSEER
			
				RETURN TRUE
			BREAK
		ENDSWITCH
	ENDIF
	 // #IF FEATURE_GANG_BOSS
	
	IF FM_EVENT_IS_PLAYER_PERMANENT_PARTICIPANT_IN_BUSINESS_BATTLE(PLAYER_ID())
		RETURN TRUE
	ENDIF
	 // #IF FEATURE_BUSINESS_BATTLES

	
	RETURN FALSE
ENDFUNC

/// PURPOSE: Sets the title on the scaleform movie passed.
PROC SET_CHALLENGE_SUMMARY_CARD_TITLE(SCALEFORM_INDEX& siMovie, STRING strTitle, STRING strSort, INT iJobIcon = -1)
	PRINTLN("[CS_CHALLENGE_DPAD], MP Scaleform: SET_CHALLENGE_SUMMARY_CARD_TITLE - title: ", strTitle, " strSort = ", strSort, " iJobIcon = ", iJobIcon)

	IF HAS_SCALEFORM_MOVIE_LOADED(siMovie)
		BEGIN_SCALEFORM_MOVIE_METHOD(siMovie, "SET_TITLE")
		
			// 1. STRING Title
			IF IS_STRING_NULL_OR_EMPTY(strSort)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(strTitle)
			ELSE
				
				// With "Challenge"
				IF FM_EVENT_GET_PLAYER_CURRENT_FM_EVENT(PLAYER_ID()) = FMMC_TYPE_CHALLENGES
				
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING("FM_AE_BRACKT_C")
						ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(strTitle)
						ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(strSort)
					END_TEXT_COMMAND_SCALEFORM_STRING()
				ELSE
					// Without
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING("FM_AE_BRACKT")
						ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(strTitle)
						ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(strSort)
					END_TEXT_COMMAND_SCALEFORM_STRING()
				ENDIF
			ENDIF
			
			// 2. STRING strDifficulty
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")

			// 3. ICON			
			IF iJobIcon <> -1
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iJobIcon) 
			ENDIF
			
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC

 // #IF FEATURE_NEW_AMBIENT_EVENTS

PROC SET_BOSS_SUMMARY_CARD_TITLE(SCALEFORM_INDEX& siMovie, STRING strTitle, STRING strSort, INT iJobIcon = -1)
	PRINTLN("[CS_CHALLENGE_DPAD], MP Scaleform: SET_BOSS_SUMMARY_CARD_TITLE - title: ", strTitle, " strSort = ", strSort, " iJobIcon = ", iJobIcon)

	IF HAS_SCALEFORM_MOVIE_LOADED(siMovie)
		BEGIN_SCALEFORM_MOVIE_METHOD(siMovie, "SET_TITLE")
		
			// 1. STRING Title
			IF IS_STRING_NULL_OR_EMPTY(strSort)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(strTitle)
			ELSE
				
//				// With "Challenge"
//				IF FM_EVENT_GET_PLAYER_CURRENT_FM_EVENT(PLAYER_ID()) = FMMC_TYPE_CHALLENGES
//				
//					BEGIN_TEXT_COMMAND_SCALEFORM_STRING("FM_AE_BRACKT_C")
//						ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(strTitle)
//						ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(strSort)
//					END_TEXT_COMMAND_SCALEFORM_STRING()
//				ELSE
					// Without
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING("FM_AE_BRACKT")
						ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(strTitle)
						ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(strSort)
					END_TEXT_COMMAND_SCALEFORM_STRING()
//				ENDIF
			ENDIF
			
			// 2. STRING strDifficulty
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")

			// 3. ICON			
			IF iJobIcon <> -1
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iJobIcon) 
			ENDIF
			
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC
 // 	#IF FEATURE_GANG_BOSS

//TWEAK_INT tiICON 0

/// PURPOSE: Sets the title on the scaleform movie passed.
PROC SET_SCALEFORM_SUMMARY_CARD_TITLE(SCALEFORM_INDEX& siMovie, STRING strTitle, STRING strDifficulty, BOOL bLiteralString = TRUE, INT iJobIcon = -1, INT iFreemodeCount = -1, BOOL bUseSecondPage = TRUE)
	PRINTLN(" MP Scaleform: SET_SCALEFORM_SUMMARY - title: ", strTitle)
	INT iNumberOne 
	INT iNumberTwo 
	IF HAS_SCALEFORM_MOVIE_LOADED(siMovie)
		BEGIN_SCALEFORM_MOVIE_METHOD(siMovie, "SET_TITLE")
		
			// 1. STRING Title
			IF bLiteralString
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(strTitle)
			ELSE
				// GTA Online (Public, 14)
				IF iFreemodeCount <> -1
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING(strTitle)
//						PRINTLN("[CS_DPAD] iFreemodeCount =  ", iFreemodeCount)
						ADD_TEXT_COMPONENT_INTEGER(iFreemodeCount)
					END_TEXT_COMMAND_SCALEFORM_STRING()
				ELSE
					// GTA Online
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(strTitle)
				ENDIF
			ENDIF
			
			// 2. STRING Difficulty/Page number
			IF DOES_DPAD_LBD_NEED_SECOND_PAGE()
			AND bUseSecondPage
				IF ON_SECOND_PAGE_DPAD_LBD()
					// 2/2
					iNumberOne = 2
					iNumberTwo = 2
				ELSE
					// 1/2
					iNumberOne = 1
					iNumberTwo = 2
				ENDIF
				BEGIN_TEXT_COMMAND_SCALEFORM_STRING("LBD_DPD_CNT")
					ADD_TEXT_COMPONENT_INTEGER(iNumberOne)
					ADD_TEXT_COMPONENT_INTEGER(iNumberTwo)
				END_TEXT_COMMAND_SCALEFORM_STRING()
			ELSE
				// Mission difficulty
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(strDifficulty)
				// Dpad icons briefcase etc.
			ENDIF
			
			// 3. ICON			
			IF iJobIcon <> -1
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iJobIcon)  // tiICON

				// Colour the Heist icon
				IF IS_PLAYER_PLAYING_OR_PLANNING_HEIST(PLAYER_ID())
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(HUD_COLOUR_H))	
				ELIF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(HUD_COLOUR_H))	
				ENDIF

			ENDIF
			
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC

FUNC BOOL IS_TEAM_MODE(LBD_SUB_MODE eSubMode)

	IF g_bVSMission
	
		RETURN TRUE
	ENDIF
	
	SWITCH eSubMode
		CASE SUB_RACE_GTA
		CASE SUB_RACE_RALLY
		CASE SUB_DM_TEAM
		
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_DISPLAY_SPLIT_FOR_ROW(INT iRow)
	SWITCH iRow
		CASE 0
		CASE 2
		CASE 4
		CASE 6
		CASE 8
		CASE 10
		CASE 12
		CASE 14

			RETURN TRUE
		BREAK	
	ENDSWITCH

	RETURN FALSE
ENDFUNC

PROC RESET_DPAD_REFRESH()
	IF g_RefreshDpad <> DONT_NEED_TO_REFRESH_DPAD
		g_RefreshDpad = DONT_NEED_TO_REFRESH_DPAD
		PRINTLN("[CS_DPAD] RESET_DPAD_REFRESH")
	ENDIF
ENDPROC

PROC REFRESH_DPAD(BOOL bNumberOfPlayersHasChanged)
	IF bNumberOfPlayersHasChanged
		IF g_RefreshDpad <> REFRESH_DPAD_REPOPULATE
			PRINTLN("[CS_DPAD] REFRESH - Number of players has changed")
			g_RefreshDpad = REFRESH_DPAD_REPOPULATE
		ENDIF
	ELSE
	
		SWITCH g_RefreshDpad
			CASE DONT_NEED_TO_REFRESH_DPAD
				g_RefreshDpad = REFRESH_DPAD_UPDATE
				PRINTLN("[CS_DPAD] REFRESH - Need to update (Number of players hasn't changed)")
				BREAK
			CASE REFRESH_DPAD_UPDATE
				BREAK
			CASE REFRESH_DPAD_REPOPULATE
				BREAK
		ENDSWITCH
	ENDIF
ENDPROC

FUNC BOOL SHOULD_DPAD_REFRESH()
	RETURN (g_RefreshDpad <> DONT_NEED_TO_REFRESH_DPAD)
ENDFUNC

PROC EMPTY_DPAD_MENU_IF_REQUIRED(SCALEFORM_INDEX dpadMovie)
	UNUSED_PARAMETER(dpadMovie)
	
	// Commented out to fix 2138772 as already done in (SHOULD_DPAD_LBD_DISPLAY) with (SCALEFORM_SET_DATA_SLOT_EMPTY)
	
//	Maybe I only need to do this for REFRESH_DPAD_REPOPULATE, but for now I'll also do it for DONT_NEED_TO_REFRESH_DPAD
//	 in case that is needed when the card is first displayed after the player presses D-pad down
//	IF (g_RefreshDpad <> REFRESH_DPAD_UPDATE)
//		SCALEFORM_SET_DATA_SLOT_EMPTY(dpadMovie)
//	ENDIF
ENDPROC

TWEAK_INT DPAD_REFRESH_TIME 250

PROC REFRESH_DPAD_LEADERBOARD_AFTER_TIME(DPAD_VARS& dpadVars)
	IF NOT HAS_NET_TIMER_STARTED(dpadVars.timerRefresh)
		START_NET_TIMER(dpadVars.timerRefresh)
	ELSE
		IF HAS_NET_TIMER_EXPIRED(dpadVars.timerRefresh, DPAD_REFRESH_TIME)
			RESET_NET_TIMER(dpadVars.timerRefresh)
			REFRESH_DPAD(FALSE)
		ENDIF
	ENDIF
ENDPROC

// Call when you want to refresh dpad (kills in Deathmatch for instance)
PROC REFRESH_DPAD_WHEN_DATA_CHANGES(INT iRefresh, INT& iStoredRefresh)
	IF iStoredRefresh <> iRefresh
		REFRESH_DPAD(FALSE)
		iStoredRefresh = iRefresh
	ENDIF
ENDPROC

CONST_INT DPAD_TAGGED_NONE 			0
CONST_INT DPAD_TAGGED_FULL_BLOCK	2
CONST_INT DPAD_TAGGED 				3

FUNC BOOL IS_DPAD_FRIEND(DPAD_VARS& dpadVars)
	RETURN IS_BIT_SET(dpadVars.iDpadBitSet, ciDPAD_IS_FRIEND)
ENDFUNC

FUNC BOOL IS_DPAD_CREW(DPAD_VARS& dpadVars)
	RETURN IS_BIT_SET(dpadVars.iDpadBitSet, ciDPAD_IS_CREW)
ENDFUNC

FUNC BOOL IS_DPAD_FRIEND_AND_CREW(DPAD_VARS& dpadVars)
	IF IS_DPAD_FRIEND(dpadVars)
	AND IS_DPAD_CREW(dpadVars)
	
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC STRING GET_RACE_TIME_STRING(INT iTime, BOOL bFinished)
	IF NOT bFinished
	AND iTime > 0
	
		RETURN "SPLIT_BEHIND"
	ENDIF

	RETURN "STRING"
ENDFUNC

PROC DO_RACE_SPLIT_TIMES(DPAD_VARS& dpadVars, INT iTimes, INT iRow)
	BOOL bFinished = IS_BIT_SET(dpadVars.iDpadBitSet, ciDPAD_FINISHED)
	//LAMAR
	IF IS_BIT_SET(dpadVars.iDpadBitSet, ciDPAD_LAMARS_ROW)
		IF dpadVars.iLamarSplit <> 0
			BOOL bLamarFinished = IS_BIT_SET(dpadVars.iDpadBitSet, ciDPAD_LAMAR_FINISHED)
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING(GET_RACE_TIME_STRING(dpadVars.iLamarSplit, bLamarFinished))
				ADD_TEXT_COMPONENT_SUBSTRING_TIME(dpadVars.iLamarSplit, TIME_FORMAT_MINUTES|TIME_FORMAT_SECONDS|TIME_FORMAT_MILLISECONDS)
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")	
		ENDIF
	ELSE
		// Drivers or solo racers
		IF NOT IS_BIT_SET(dpadVars.iDpadBitSet, ciDPAD_IS_TEAM)
		OR SHOULD_DISPLAY_SPLIT_FOR_ROW(iRow) 
		
			IF iTimes <> 0
//				IF iRow <> dpadVars.iPlayersRow // do not draw on player's row
				IF NOT IS_BIT_SET(dpadVars.iDpadBitSet, ciDPAD_HIDE_SPLIT_TIME)
					SWITCH dpadVars.SplitTimeDisplayMode
						CASE SPLIT_TIME_DISPLAY_MODE_LOCAL_PLAYER_IS_NOW_AHEAD
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME_STRING("v")
							BREAK
							
						CASE SPLIT_TIME_DISPLAY_MODE_LOCAL_PLAYER_IS_NOW_BEHIND
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME_STRING("^")
							BREAK
							
						DEFAULT
							BEGIN_TEXT_COMMAND_SCALEFORM_STRING(GET_RACE_TIME_STRING(iTimes, bFinished)) 
								ADD_TEXT_COMPONENT_SUBSTRING_TIME(iTimes, TIME_FORMAT_MINUTES|TIME_FORMAT_SECONDS|TIME_FORMAT_MILLISECONDS|TEXT_FORMAT_USE_DOT_FOR_MILLISECOND_DIVIDER)
							END_TEXT_COMMAND_SCALEFORM_STRING()
							BREAK
					ENDSWITCH
				ELSE
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")	
				ENDIF
			ELSE
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")	
			ENDIF
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")	
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL DPAD_FORCE_SCORES()
	RETURN IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciDPAD_SHOW_SCORE)
ENDFUNC

PROC DO_SPECIFIC_MODE_SCORES(LBD_SUB_MODE eSubMode, DPAD_VARS& dpadVars, INT iRow, INT iScore, STRING strHeadshotTxd, STRING sJobRole, BOOL bLiteral = FALSE, INT iTeamRank = -1)
	SWITCH eSubMode
		CASE SUB_RACE_NORMAL
		CASE SUB_RACE_STUNT
		CASE SUB_RACE_GTA
		CASE SUB_RACE_RALLY 
		CASE SUB_RACE_FOOT
		CASE SUB_RACE_AGG_BEST
		CASE SUB_RACE_AGG_TIME
		CASE SUB_ARENA_ARCADE
		CASE SUB_ARENA_DDERBY
			DO_RACE_SPLIT_TIMES(dpadVars, iScore, iRow)											// Race times
		BREAK
		CASE SUB_RACE_TARGET
			IF iTeamRank = -2
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")
			ELSE
//				BEGIN_TEXT_COMMAND_SCALEFORM_STRING("TEAM_LBD_NMS")
//					ADD_TEXT_COMPONENT_INTEGER(iTeamRank)
//				END_TEXT_COMMAND_SCALEFORM_STRING()
				
				IF bLiteral
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(sJobRole)
					PRINTLN("DO_SPECIFIC_MODE_SCORES, SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME = ", sJobRole)
				ELSE
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sJobRole)	
					PRINTLN("DO_SPECIFIC_MODE_SCORES, SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING = ", sJobRole)
				ENDIF
				
				
			ENDIF
		BREAK
		CASE SUB_HORDE
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("DPAD_SURV")	
				ADD_TEXT_COMPONENT_INTEGER(iScore)
			END_TEXT_COMMAND_SCALEFORM_STRING()
		BREAK
		CASE SUB_MISSIONS_TIMED
		CASE SUB_MISSIONS_NORMAL	
		CASE SUB_MISSIONS_NO_HUD
		CASE SUB_MISSIONS_LTS
		CASE SUB_MISSIONS_CTF
		CASE SUB_MISSIONS_VERSUS
		CASE SUB_MISSIONS_HEIST 
		CASE SUB_MISSION_RACE
		
		CASE SUB_ARENA_CTF
		CASE SUB_ARENA_DETONATION
		CASE SUB_ARENA_FIRE
		CASE SUB_ARENA_GAMESMASTER
		CASE SUB_ARENA_MONSTER
		CASE SUB_ARENA_TAG_TEAM
		
			// Draw roles
			IF DPAD_FORCE_SCORES()
			
				PRINTLN("DO_SPECIFIC_MODE_SCORES, ciDPAD_SHOW_SCORE is set ")
				
				IF iScore <> -1
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING("NUMBER")
						ADD_TEXT_COMPONENT_INTEGER(iScore)
					END_TEXT_COMMAND_SCALEFORM_STRING()
				ELSE
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")		
				ENDIF
			ELIF NOT IS_STRING_NULL_OR_EMPTY(sJobRole)
			
				PRINTLN("DO_SPECIFIC_MODE_SCORES, iScore = ", iScore)
				PRINTLN("DO_SPECIFIC_MODE_SCORES, sJobRole = ", sJobRole)
			
				IF iScore != 0	
				AND NOT IS_BIT_SET(dpadVars.iDpadBitSet, ciDPAD_IS_TEAM)
				AND eSubMode = SUB_MISSION_RACE
					IF dpadVars.SplitTimeDisplayMode = SPLIT_TIME_DISPLAY_MODE_LOCAL_PLAYER_IS_NOW_AHEAD
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME_STRING("v")
					ELIF dpadVars.SplitTimeDisplayMode = SPLIT_TIME_DISPLAY_MODE_SPLIT_TIME
						BEGIN_TEXT_COMMAND_SCALEFORM_STRING(GET_RACE_TIME_STRING(iScore, FALSE)) 
							ADD_TEXT_COMPONENT_SUBSTRING_TIME(iScore, TIME_FORMAT_MINUTES|TIME_FORMAT_SECONDS|TIME_FORMAT_MILLISECONDS|TEXT_FORMAT_USE_DOT_FOR_MILLISECOND_DIVIDER)
						END_TEXT_COMMAND_SCALEFORM_STRING()
					ENDIF
				ELSE
					IF eSubMode != SUB_MISSION_RACE
						IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciHARD_TARGET_ENABLE_MODE)
							IF bLiteral
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(sJobRole)
								PRINTLN("DO_SPECIFIC_MODE_SCORES, SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME = ", sJobRole)
							ELSE
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sJobRole)	
								PRINTLN("DO_SPECIFIC_MODE_SCORES, SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING = ", sJobRole)
							ENDIF
						ELSE
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")	
						ENDIF
					ENDIF
				ENDIF
			ELSE
				PRINTLN("DO_SPECIFIC_MODE_SCORES, strHeadshotTxd ", strHeadshotTxd)
				IF IS_STRING_NULL_OR_EMPTY(strHeadshotTxd)	// If no headshots
					IF iScore <> -1
						BEGIN_TEXT_COMMAND_SCALEFORM_STRING("NUMBER")
							ADD_TEXT_COMPONENT_INTEGER(iScore)
						END_TEXT_COMMAND_SCALEFORM_STRING()
					ELSE
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")		
					ENDIF
				ELSE
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")
				ENDIF
			ENDIF
		BREAK
		CASE SUB_ARENA_CARNAGE
		CASE SUB_ARENA_PASSBOMB
		CASE SUB_KART	
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne, ciOptionsBS21_Last_Man_Standing_Style_Leaderboard_Order)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")
			ELSE
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iScore) 
			ENDIF
		BREAK
		DEFAULT
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iScore) 										// SCORES
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL IS_SUBMODE_RACE(LBD_SUB_MODE eSubMode)
	SWITCH eSubMode
		CASE SUB_RACE_NORMAL
		CASE SUB_RACE_GTA
		CASE SUB_RACE_RALLY
		CASE SUB_RACE_BASEJUMP
		CASE SUB_RACE_FOOT
		CASE SUB_RACE_STUNT
		CASE SUB_RACE_TARGET
		
			RETURN TRUE
	ENDSWITCH

	RETURN FALSE
ENDFUNC

FUNC BOOL IS_SUBMODE_DEATHMATCH(LBD_SUB_MODE eSubMode)
	SWITCH eSubMode
		CASE SUB_DM_FFA
		CASE SUB_DM_TEAM
		CASE SUB_DM_VEH
		CASE SUB_KOTH
		CASE SUB_DM_UPDATE
		
			RETURN TRUE
	ENDSWITCH

	RETURN FALSE
ENDFUNC


ENUM eHeadshotOrPositionNumber
	DISPLAY_HEADSHOT,
	DISPLAY_POSITION_NUMBER
ENDENUM

//FUNC INT GET_SPEAKER_ICON(GAMER_HANDLE ghPlayer)//, PLAYER_INDEX PlayerId)
//
//	INT iReturn = ENUM_TO_INT(ICON_RANK_FREEMODE)
//	
//	IF NETWORK_IS_GAMER_MUTED_BY_ME(ghPlayer)
//		PRINTLN("GET_SPEAKER_ICON, MUTED_HEADSET")
//		iReturn = ENUM_TO_INT(MUTED_HEADSET)
//		RETURN iReturn
//	ENDIF
//	
//	IF NETWORK_IS_GAMER_TALKING(ghPlayer)
//		PRINTLN("GET_SPEAKER_ICON, ACTIVE_HEADSET")
//		iReturn = ENUM_TO_INT(ACTIVE_HEADSET)
//		RETURN iReturn
//	ENDIF
//	
////	IF NETWORK_GAMER_HAS_HEADSET(ghPlayer)
////		PRINTLN("GET_SPEAKER_ICON, INACTIVE_HEADSET")
////		iReturn = ENUM_TO_INT(INACTIVE_HEADSET)
////		RETURN iReturn
////	ENDIF
//	
//	RETURN iReturn
//ENDFUNC
//
//FUNC BOOL IS_ICON_RANK_GLOBE(INT iIconNumber)
//	
//	IF iIconNumber = ENUM_TO_INT(ICON_RANK_FREEMODE)
//	
//		RETURN TRUE
//	ENDIF	
//
//	RETURN FALSE
//ENDFUNC
//
//PROC DRAW_MICS_OR_RANK(SCALEFORM_INDEX& siMovie, GAMER_HANDLE ghPlayer, INT iRow, INT iOnlineRank, BOOL bCanTalk)
//
//	STRING strMethodName = "SET_ICON"
//	
//	INT iIconNumber = GET_SPEAKER_ICON(ghPlayer)
//	
//	// 2047383
//	IF NOT bCanTalk
//		iIconNumber = ENUM_TO_INT(ICON_RANK_FREEMODE)
//	ENDIF
//	
//	IF BEGIN_SCALEFORM_MOVIE_METHOD(siMovie, strMethodName)
//		PRINTLN("DRAW_MICS_OR_RANK, iRow = ", iRow, " iIconNumber = ", iIconNumber, " iOnlineRank = ", iOnlineRank)
//		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iRow)				// Dpad page SHOULD THIS BE THE ROW AS OPPOSED PLAYER ID?*************
//		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iIconNumber)		// Mics or Rank globe
//		IF IS_ICON_RANK_GLOBE(iIconNumber)
//			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iOnlineRank)   // Rank number
//		ENDIF
//		END_SCALEFORM_MOVIE_METHOD()
//	ENDIF
//ENDPROC
//
//PROC DRAW_MICS(SCALEFORM_INDEX& siMovie)
//	PLAYER_INDEX PlayerId
//	GAMER_HANDLE gamerH[NUM_NETWORK_PLAYERS]
//	INT iRank, iPlayer, iRow, i
//	REPEAT NUM_NETWORK_PLAYERS i
//		IF IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(i), FALSE)
//			PlayerId = INT_TO_PLAYERINDEX(i)
//			iPlayer = NATIVE_TO_INT(PlayerId)
//			IF NOT IS_PLAYER_SCTV(PlayerId)
////				IF NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PlayerId, PLAYER_ID())
////				OR GlobalplayerBD[NATIVE_TO_INT(PlayerId)].iCurrentShop <> -1
//					gamerH[i] = GET_GAMER_HANDLE_PLAYER(playerId)
//					iRank = GlobalplayerBD_FM[iPlayer].scoreData.iRank
//					
////					#IF IS_DEBUG_BUILD
////					IF NETWORK_IS_GAMER_TALKING(gamerH[i])
////					OR NETWORK_IS_PLAYER_TALKING(PlayerId)
////						PRINTLN("[MIC_TEST] iPlayer_IS_TALKING! = ", iPlayer)
////					ELSE
////						PRINTLN("[MIC_TEST] iPlayer = ", iPlayer)
////					ENDIF
////					#ENDIF			
//					
//					DRAW_MICS_OR_RANK(siMovie, gamerH[i], iRow, iRank, NETWORK_CAN_COMMUNICATE_WITH_GAMER(gamerH[i]))
//					
//					iRow++
//				ENDIF
////			ENDIF
//		ENDIF
//	ENDREPEAT
//ENDPROC
//
//PROC DRAW_MICROPHONES(DPAD_VARS& dpadVars, SCALEFORM_INDEX& siMovie)
//	IF NOT HAS_NET_TIMER_STARTED(dpadVars.timerMic)
//		START_NET_TIMER(dpadVars.timerMic)
//		DRAW_MICS(siMovie)
//	ELSE
//		IF HAS_NET_TIMER_EXPIRED(dpadVars.timerMic, 500)
//			RESET_NET_TIMER(dpadVars.timerMic)
//		ENDIF
//	ENDIF
//ENDPROC

//╔═════════════════════════════════════════════════════════════════════════════╗
//║		NEW MICS																║

PROC SET_PLAYER_MIC(SCALEFORM_INDEX& siMovie, INT iRow, INT iIcon, INT iRank)

	IF HAS_SCALEFORM_MOVIE_LOADED(siMovie)
		IF BEGIN_SCALEFORM_MOVIE_METHOD(siMovie, "SET_ICON")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iRow)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iIcon)	// Always false
			
			IF iIcon = ENUM_TO_INT(ICON_RANK_FREEMODE)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iRank)
			ENDIF
			END_SCALEFORM_MOVIE_METHOD()
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Returns the correct icon
FUNC INT GET_DPAD_PLAYER_VOICE_ICON(INT iVoiceChatState, BOOL bCrossedOut = FALSE, LBD_SUB_MODE eSubMode = SUB_NOT_USED)

	INT iIcon = ENUM_TO_INT(ICON_RANK_FREEMODE)
	
	SWITCH eSubMode
		CASE SUB_ARCADE_CNC
			iIcon = ENUM_TO_INT(ICON_EMPTY)
		BREAK
	ENDSWITCH
	
	IF bCrossedOut
		iIcon = ENUM_TO_INT(STATUS_DEAD)
	ENDIF

	SWITCH iVoiceChatState
		CASE ciVOICE_CHAT_TALKING	RETURN ENUM_TO_INT(ACTIVE_HEADSET)
		CASE ciVOICE_CHAT_MUTED		RETURN ENUM_TO_INT(MUTED_HEADSET)
	ENDSWITCH
	
	RETURN iIcon
ENDFUNC

/// PURPOSE: Returns the voice chat state of the player in slot passes
PROC SET_DPAD_PLAYER_VOICE_STATE(DPAD_VARS& dpadVars, INT iSlot, INT iVoiceStat)
	dpadVars.sDpadMics[iSlot].iVoiceChatState = iVoiceStat
ENDPROC

/// PURPOSE: Returns the voice chat state of the player in slot passes
FUNC INT GET_DPAD_PLAYER_VOICE_STATE(DPAD_VARS& dpadVars, INT iSlot)
	RETURN dpadVars.sDpadMics[iSlot].iVoiceChatState
ENDFUNC

FUNC TEXT_LABEL_15 GET_CREW_TAG_DPAD_LBD(PLAYER_INDEX playerId)

	TEXT_LABEL_15 tlCrewTag
	
	IF IS_NET_PLAYER_OK(playerId, FALSE)
		//GAMER_HANDLE gamerHandle = GET_GAMER_HANDLE_PLAYER(playerId)
		g_GamerHandle = GET_GAMER_HANDLE_PLAYER(playerId)
		
		// 2111217
		// XB1 check individual player and hide tag if we do not have privileges
		IF IS_XBOX_PLATFORM()
		#IF FEATURE_GEN9_RELEASE
		OR IS_PROSPERO_VERSION()
		#ENDIF
			IF IS_GAMER_HANDLE_VALID(g_GamerHandle)
				IF NOT NETWORK_CAN_VIEW_GAMER_USER_CONTENT(g_GamerHandle)
					RETURN tlCrewTag
				ENDIF
			ENDIF
		ELSE
			IF NOT NETWORK_HAVE_USER_CONTENT_PRIVILEGES()
				RETURN tlCrewTag
			ENDIF
		ENDIF
		
		IF IS_PLAYER_IN_ACTIVE_CLAN(g_GamerHandle)	
			//NETWORK_CLAN_DESC crewHandle = GET_PLAYER_CREW(playerId)
			g_ClanDescription = GET_PLAYER_CREW(playerId)
			NETWORK_CLAN_GET_UI_FORMATTED_TAG_EZ(g_ClanDescription, tlCrewTag)
		ENDIF
	ENDIF
	
	RETURN tlCrewTag
ENDFUNC

FUNC INT GET_PLAYER_CREW_INT(GAMER_HANDLE &GamerHandle)
	//NETWORK_CLAN_DESC retDesc
	IF IS_GAMER_HANDLE_VALID(GamerHandle)
		NETWORK_CLAN_PLAYER_GET_DESC( g_ClanDescription, 0, GamerHandle)
	ENDIF
	RETURN g_ClanDescription.Id
ENDFUNC

FUNC BOOL MAINTAIN_DPAD_VOICE_CHAT(DPAD_VARS& dpadVars)//, INT iColumn, INT iVoteParticipant)
	
	INT iCheckIndex
	INT iVoiceChat
	GAMER_HANDLE aGamerToCheck
	PLAYER_INDEX playerId
	BOOL bUpdateRow = FALSE
	
	iCheckIndex 	= dpadVars.iDpadClientSyncCheck
	
	playerId 		= INT_TO_PLAYERINDEX(dpadVars.iDpadClientSyncCheck)
	
	IF playerId <> INVALID_PLAYER_INDEX()
	AND IS_NET_PLAYER_OK(playerId, FALSE)
	
		aGamerToCheck	= GET_GAMER_HANDLE_PLAYER(playerId)
		iVoiceChat 		= GET_DPAD_PLAYER_VOICE_STATE(dpadVars, dpadVars.iDpadClientSyncCheck)
		
		// Is the gamer we are going to check valid?
		IF IS_GAMER_HANDLE_VALID(aGamerToCheck)
		
			//...yes, udpate state of player
			SWITCH iVoiceChat
				
				CASE ciVOICE_CHAT_NULL

					// Check to see if we can communicate with this gamer?
					IF NETWORK_CAN_COMMUNICATE_WITH_GAMER(aGamerToCheck)
					
						//...yes, is this player now talking
						IF NETWORK_IS_GAMER_TALKING(aGamerToCheck)
							bUpdateRow = TRUE
							
//							PRINTLN("MAINTAIN_DPAD_VOICE_CHAT, ciVOICE_CHAT_NULL > ciVOICE_CHAT_TALKING, iCheckIndex = ", iCheckIndex, " Player = ", GET_PLAYER_NAME(playerId))
							
							SET_DPAD_PLAYER_VOICE_STATE(dpadVars, iCheckIndex, ciVOICE_CHAT_TALKING)
						ENDIF
					ELSE
						//...no, have we now muted this player
						IF NETWORK_IS_GAMER_MUTED_BY_ME(aGamerToCheck)
							bUpdateRow = TRUE
							
//							PRINTLN("MAINTAIN_DPAD_VOICE_CHAT, ciVOICE_CHAT_NULL > ciVOICE_CHAT_MUTED, iCheckIndex =  ", iCheckIndex, " Player = ", GET_PLAYER_NAME(playerId))
							
							SET_DPAD_PLAYER_VOICE_STATE(dpadVars, iCheckIndex, ciVOICE_CHAT_MUTED)
						ENDIF
					ENDIF
					
				BREAK
			
				CASE ciVOICE_CHAT_TALKING
					
					// Check to see if we can communicate with this gamer?
					IF NETWORK_CAN_COMMUNICATE_WITH_GAMER(aGamerToCheck)
					
						//...yes, is this player no longer talking
						IF NOT NETWORK_IS_GAMER_TALKING(aGamerToCheck)
							bUpdateRow = TRUE
							
//							PRINTLN("MAINTAIN_DPAD_VOICE_CHAT, ciVOICE_CHAT_TALKING > ciVOICE_CHAT_NULL 1, iCheckIndex =  ", iCheckIndex, " Player = ", GET_PLAYER_NAME(playerId))
							
							SET_DPAD_PLAYER_VOICE_STATE(dpadVars, iCheckIndex, ciVOICE_CHAT_NULL)
						ENDIF
					ELSE
						//...no, reset back to null state
						bUpdateRow = TRUE
						
//						PRINTLN("MAINTAIN_DPAD_VOICE_CHAT, ciVOICE_CHAT_TALKING > ciVOICE_CHAT_NULL 2, iCheckIndex =  ", iCheckIndex, " Player = ", GET_PLAYER_NAME(playerId))
						
						SET_DPAD_PLAYER_VOICE_STATE(dpadVars, iCheckIndex, ciVOICE_CHAT_NULL)
					ENDIF			
				BREAK
				
				CASE ciVOICE_CHAT_MUTED
					
					// Check if we can communicate with this gamer?
					IF NETWORK_CAN_COMMUNICATE_WITH_GAMER(aGamerToCheck)
						
						IF NOT NETWORK_IS_GAMER_MUTED_BY_ME(aGamerToCheck)
							bUpdateRow = TRUE
							
//							PRINTLN("MAINTAIN_DPAD_VOICE_CHAT, ciVOICE_CHAT_MUTED > ciVOICE_CHAT_NULL 1, iCheckIndex =  ", iCheckIndex, " Player = ", GET_PLAYER_NAME(playerId))

							SET_DPAD_PLAYER_VOICE_STATE(dpadVars, iCheckIndex, ciVOICE_CHAT_NULL)
						ENDIF
					ELSE
						//...no, are we no longer muted?
						IF NOT NETWORK_IS_GAMER_MUTED_BY_ME(aGamerToCheck)
							bUpdateRow = TRUE
							
//							PRINTLN("MAINTAIN_DPAD_VOICE_CHAT, ciVOICE_CHAT_MUTED > ciVOICE_CHAT_NULL 2, iCheckIndex =  ", iCheckIndex, " Player = ", GET_PLAYER_NAME(playerId))
							
							SET_DPAD_PLAYER_VOICE_STATE(dpadVars, iCheckIndex, ciVOICE_CHAT_NULL)
						ENDIF
					ENDIF
				BREAK
				
			ENDSWITCH
		ENDIF
	ENDIF
	
	dpadVars.iDpadClientSyncCheck++
	
	IF dpadVars.iDpadClientSyncCheck >= NUM_NETWORK_PLAYERS
		dpadVars.iDpadClientSyncCheck = 0
	ENDIF
	
	RETURN bUpdateRow
ENDFUNC


//╚═════════════════════════════════════════════════════════════════════════════╝	

FUNC INT GET_MIN_DPAD_ROW()
	INT iMinRow = 0
	IF g_dPadSecondPage
		iMinRow = MAX_DPAD_PLAYERS_ONE_PAGE // 16
	ENDIF
	
	RETURN iMinRow
ENDFUNC

FUNC INT GET_MAX_DPAD_ROW()
	INT iMaxRow
	IF g_dPadSecondPage
		iMaxRow = NUM_NETWORK_PLAYERS // 18
	ELSE
		iMaxRow = MAX_DPAD_PLAYERS_ONE_PAGE // 16
	ENDIF
	
	RETURN iMaxRow
ENDFUNC


FUNC BOOL bOK_TO_SHOW_GANG_ICONS()

	IF FM_EVENT_IS_PLAYER_ON_ANY_FM_EVENT(PLAYER_ID())
	OR b_MODE_HAS_SCORES()
	
//		PRINTLN("[3138369] bOK_TO_SHOW_GANG_ICONS RETURN FALSE ")


		IF NOT FM_EVENT_IS_PLAYER_PERMANENT_PARTICIPANT_IN_BUSINESS_BATTLE(PLAYER_ID())  
	
		RETURN FALSE
		
		ENDIF 
	ENDIF

	RETURN TRUE
ENDFUNC

FUNC BOOL bSVM_MISSION()

	IF NETWORK_IS_ACTIVITY_SESSION()
		IF SVM_FLOW_IS_CURRENT_MISSION_SVM_FLOW()
		OR SVM_FLOW_IS_CURRENT_MISSION_SVM_MISSION() 
		
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL bSHOW_SECUROSERV_ICON(PLAYER_INDEX playerId)

		IF bSVM_MISSION()
			IF IS_NET_PLAYER_OK(playerId, FALSE)
			
				RETURN GB_IS_PLAYER_BOSS_OF_A_GANG(playerId)
			ENDIF
		ENDIF

	IF IS_NET_PLAYER_OK(playerId, FALSE)
	AND bOK_TO_SHOW_GANG_ICONS()
	AND GB_IS_PLAYER_BOSS_OF_A_GANG_TYPE(playerId, GT_VIP)
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL bSHOW_BIKER_ICON(PLAYER_INDEX playerId)

	IF IS_NET_PLAYER_OK(playerId, FALSE)
	AND bOK_TO_SHOW_GANG_ICONS()
	AND GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(playerId, TRUE)
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC HANDLE_DPAD_ICON(DPAD_VARS& dpadVars, PLAYER_INDEX playerId)
	
	IF bSHOW_SECUROSERV_ICON(playerId)
	
		#IF IS_DEBUG_BUILD
		IF IS_NET_PLAYER_OK(playerId, FALSE)
			PRINTLN("HANDLE_DPAD_ICON, bSHOW_SECUROSERV_ICON for ", GET_PLAYER_NAME(playerId))
		ENDIF
		#ENDIF
		
//		PRINTLN("[3138369] HANDLE_DPAD_ICON 1 ")
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(STATUS_GANG_CEO)) 
		
	ELIF bSHOW_BIKER_ICON(playerId)
	
		#IF IS_DEBUG_BUILD
		IF IS_NET_PLAYER_OK(playerId, FALSE)
			PRINTLN("HANDLE_DPAD_ICON, bSHOW_BIKER_ICON ", GET_PLAYER_NAME(playerId))
		ENDIF
		#ENDIF
		
//		PRINTLN("[3138369] HANDLE_DPAD_ICON 2 ")
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(STATUS_GANG_BIKER)) 
			
	ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciHARD_TARGET_ENABLE_MODE)
	AND NATIVE_TO_INT(playerId) > -1
	AND NATIVE_TO_INT(playerId) < NUM_NETWORK_PLAYERS
	AND IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(playerId)].iMissionModeBit, ciADVERSARY_IS_A_HARD_TARGET)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(DPAD_DOWN_TARGET))				
	ELSE
	
//		PRINTLN("[3138369] HANDLE_DPAD_ICON 3 ")

		
		#IF IS_DEBUG_BUILD
		IF IS_NET_PLAYER_OK(playerId, FALSE)
			IF dpadVars.bShowJp
				PRINTLN("HANDLE_DPAD_ICON, STATUS_GANG_NONE, SHOW_JP YES ", GET_PLAYER_NAME(playerId))
			ELSE
				PRINTLN("HANDLE_DPAD_ICON, STATUS_GANG_NONE, SHOW_JP NO ", GET_PLAYER_NAME(playerId))
			ENDIF
		ENDIF
		#ENDIF
		
		IF bSVM_MISSION()
			
			dpadVars.bShowJp = FALSE
		ENDIF
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(dpadVars.bShowJp)							// 9 Show JP

	ENDIF

ENDPROC

#IF USE_JEFFS_NEW_MOVIE_FOR_FREEMODE
PROC POPULATE_NEW_FREEMODE_DPAD_ROWS_USING_JEFFS_NEW_FREEMODE_MOVIE(LBD_SUB_MODE eSubMode, SCALEFORM_INDEX& siMovie, DPAD_VARS& dpadVars, INT iRow, TEXT_LABEL_15 crewTag, STRING strHeadshotTxd, INT iOnlineRank, INT iScore, 
																	HUD_COLOURS colourRow, eHeadshotOrPositionNumber headshotOrPositionNumber, FLOAT fScore = -1.0, INT iTime = -1, INT iPlayer = -1)

	STRING strMethodName = "SET_DATA_SLOT"
	IF g_RefreshDpad = REFRESH_DPAD_UPDATE				//	If g_RefreshDpad is REFRESH_DPAD_REPOPULATE then we should do a full SET_DATA_SLOT
		PRINTLN("[CS_DPAD] USING_JEFFS_NEW_FREEMODE_MOVIE, >>> UPDATE_SLOT")
		strMethodName = "UPDATE_SLOT" // Updating
	ELSE
		PRINTLN("[CS_DPAD] USING_JEFFS_NEW_FREEMODE_MOVIE, >>> SET_DATA_SLOT")
	ENDIF
	
	PLAYER_INDEX playerId
	playerId = INT_TO_PLAYERINDEX(iPlayer)
	
//	PRINTLN("[3138369] POPULATE_NEW_FREEMODE_DPAD_ROWS_USING_JEFFS_NEW_FREEMODE_MOVIE 1 ")
	
	IF BEGIN_SCALEFORM_MOVIE_METHOD(siMovie, strMethodName)
	
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iRow)											// 1

		IF IS_BIT_SET(dpadVars.iDpadBitSet, ciDPAD_PLAYER_DEAD)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME("")
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iOnlineRank)								// 2
		ENDIF
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(dpadVars.tl63LeaderboardPlayerNames)	// 3 Player gamertag

		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(colourRow))						// 4 HudColourEnum bgColour 

		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(ICON_RANK_FREEMODE)) 				// 5 icon2

		IF headshotOrPositionNumber = DISPLAY_POSITION_NUMBER
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iRow+1)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(-1) 										// 6 String/int numberOnAvatarImg
		ENDIF

		IF iTime <> -1
		
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
				ADD_TEXT_COMPONENT_SUBSTRING_TIME(iTime, TIME_FORMAT_MINUTES | TIME_FORMAT_SECONDS)	// 7 Score
			END_TEXT_COMMAND_SCALEFORM_STRING()
			
		ELIF fScore <> - 1.0
		
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("NUMBER")
				ADD_TEXT_COMPONENT_FLOAT(fScore, 1)											// 7 Score
			END_TEXT_COMMAND_SCALEFORM_STRING()
		
		ELIF iScore <> -1
			
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iScore)									
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME("")
		ENDIF
	
		IF eSubMode = SUB_KOTH
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME("")
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(crewTag) 							// 8 String crewTag
		ENDIF

		HANDLE_DPAD_ICON(dpadVars, playerId)
		
		IF (headshotOrPositionNumber = DISPLAY_POSITION_NUMBER) 
		OR IS_STRING_NULL_OR_EMPTY(strHeadshotTxd)											// daveyg if txd is invalid
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME("")								// 10 ranking(-1 to hide)	
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME("")								// 11
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING(strHeadshotTxd)			// NEW_avatarTXD
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING(strHeadshotTxd)			// NEW_avatarTXN
		ENDIF

		// "C" or "F" is Crew/Friend
		IF IS_DPAD_FRIEND_AND_CREW(dpadVars) 
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("DPAD_FRIEND")	
		ELIF IS_DPAD_FRIEND(dpadVars)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("DPAD_FRIEND")	
		ELIF IS_DPAD_CREW(dpadVars)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("DPAD_CREW")
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")										// 12
		ENDIF

		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC


PROC POPULATE_CHALLENGE_DPAD_ROWS(PLAYER_INDEX playerID, SCALEFORM_INDEX& siMovie, DPAD_VARS& dpadVars, INT iRow, TEXT_LABEL_15 crewTag, STRING strHeadshotTxd, INT iOnlineRank, INT iScore, 
								HUD_COLOURS colourRow, eHeadshotOrPositionNumber headshotOrPositionNumber, FLOAT fScore = -1.0, INT iTime = -1, INT iInvolvedStatus = -1, 
								STRING sDpadVarString1 = NULL, INT iDpadVarInt = -1, INT iTeamPos = -1, BOOL bIsTeam = FALSE)
								

	// Draw second page logic 1806319
	IF iRow >= GET_MIN_DPAD_ROW()
	AND iRow < GET_MAX_DPAD_ROW()

		iRow = (iRow % MAX_DPAD_PLAYERS_ONE_PAGE)
		
		INT iPositionToShow 
		iPositionToShow = (iRow + 1)
		
		IF g_dPadSecondPage
			iPositionToShow = ( iPositionToShow + (NUM_NETWORK_PLAYERS / 2) )
		ENDIF
		
		IF bIsTeam
			iPositionToShow = iTeamPos
			
			IF iPositionToShow = -2
				iScore = -1				// We only show the score for the first player in a team
			ENDIF
		ENDIF
		
		STRING strMethodName = "SET_DATA_SLOT"
		IF g_RefreshDpad = REFRESH_DPAD_UPDATE				//	If g_RefreshDpad is REFRESH_DPAD_REPOPULATE then we should do a full SET_DATA_SLOT
			PRINTLN("[CS_DPAD] 2554763 POPULATE_NEW_FREEMODE_DPAD_ROWS_USING_JEFFS_NEW_FREEMODE_MOVIE, UPDATE_SLOT")
			strMethodName = "UPDATE_SLOT" // Updating
		ELSE
			PRINTLN("[CS_DPAD] 2554763 POPULATE_NEW_FREEMODE_DPAD_ROWS_USING_JEFFS_NEW_FREEMODE_MOVIE, SET_DATA_SLOT")
		ENDIF
		
		IF HAS_SCALEFORM_MOVIE_LOADED(siMovie)
			IF BEGIN_SCALEFORM_MOVIE_METHOD(siMovie, strMethodName)
			
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iRow)											// 1

				IF IS_BIT_SET(dpadVars.iDpadBitSet, ciDPAD_PLAYER_DEAD)
				OR dpadVars.eDpadVarType = DPAD_VAR_REPLACE_PLAYER_NAMES							// 2398710
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME("")
				ELSE
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iOnlineRank)								// 2 Drawing the ranking (number only)
				ENDIF
				
				IF dpadVars.eDpadVarType = DPAD_VAR_REPLACE_PLAYER_NAMES
				AND NOT IS_STRING_NULL_OR_EMPTY(sDpadVarString1)
					
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sDpadVarString1)
				ELSE
				
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(dpadVars.tl63LeaderboardPlayerNames)	// 3 Player gamertag
				ENDIF

				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(colourRow))						// 4 HudColourEnum bgColour 

				IF dpadVars.eDpadVarType = DPAD_VAR_REPLACE_PLAYER_NAMES
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME("")								// 2398710
				ELSE
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(ICON_RANK_FREEMODE)) 			// 5 icon2, the rank globe icon
				ENDIF
		
				IF headshotOrPositionNumber = DISPLAY_POSITION_NUMBER
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPositionToShow)
				ELSE
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(-1) 										// 6 String/int numberOnAvatarImg
				ENDIF
				
				IF SHOULD_DRAW_UNSORTED_LBD()
				
					PRINTLN("2554763, SHOULD_DRAW_UNSORTED_LBD ")
				
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME("")
				ELIF dpadVars.eDpadVarType = DPAD_VAR_REPLACE_PLAYER_NAMES	
				AND NOT IS_STRING_NULL_OR_EMPTY(sDpadVarString1)
					PRINTLN("2554763, B ")
					
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING("FM_AE_ONE_INT")
						ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(sDpadVarString1)
						ADD_TEXT_COMPONENT_INTEGER(iDpadVarInt)
					END_TEXT_COMMAND_SCALEFORM_STRING()
				ELIF dpadVars.eDpadVarType = DPAD_VAR_STRING_AND_INT	
				AND NOT IS_STRING_NULL_OR_EMPTY(sDpadVarString1)
					PRINTLN("2554763, C ")
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING("FM_AE_ONE_INT")
						ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(sDpadVarString1)
						ADD_TEXT_COMPONENT_INTEGER(iDpadVarInt)
					END_TEXT_COMMAND_SCALEFORM_STRING()
				ELIF dpadVars.eDpadVarType = DPAD_VAR_TWO_STRINGS_AND_INT	
				AND NOT IS_STRING_NULL_OR_EMPTY(sDpadVarString1)
				
					PRINTLN("2554763, D ")
					
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING("FM_AE_TWO_INT")
						ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(sDpadVarString1)
						ADD_TEXT_COMPONENT_INTEGER(iDpadVarInt)
					END_TEXT_COMMAND_SCALEFORM_STRING()
					
				ELIF dpadVars.eDpadVarType = DPAD_VAR_NUMBER_WITH_UNIT	
				AND NOT IS_STRING_NULL_OR_EMPTY(dpadVars.sDpadRowString)
					PRINTLN("2554763, E ")
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING("FM_AE_UNIT")
					
						IF fScore != -1				
							ADD_TEXT_COMPONENT_FLOAT(fScore, 1)	
						ENDIF
						IF iScore != -1
							ADD_TEXT_COMPONENT_INTEGER(iScore)
						ENDIF
						
						ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(dpadVars.sDpadRowString)
						
					END_TEXT_COMMAND_SCALEFORM_STRING()
				ELIF dpadVars.eDpadVarType = DPAD_VAR_CASH
					PRINTLN("2554763, F ")
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING("FM_AE_CASH")
					
						ADD_TEXT_COMPONENT_FORMATTED_INTEGER(iScore, INTEGER_FORMAT_COMMA_SEPARATORS)
												
					END_TEXT_COMMAND_SCALEFORM_STRING()
				
				ELIF dpadVars.eDpadVarType = DPAD_VAR_CASH_WITH_MINUS_FIRST
					PRINTLN("2554763, FF ")
					
					IF iScore = 0
						BEGIN_TEXT_COMMAND_SCALEFORM_STRING("FM_AE_CASH")
						
							ADD_TEXT_COMPONENT_FORMATTED_INTEGER(iScore, INTEGER_FORMAT_COMMA_SEPARATORS)
													
						END_TEXT_COMMAND_SCALEFORM_STRING()
					ELSE
						BEGIN_TEXT_COMMAND_SCALEFORM_STRING("FM_NG_CASH")
						
							ADD_TEXT_COMPONENT_FORMATTED_INTEGER(iScore, INTEGER_FORMAT_COMMA_SEPARATORS)
													
						END_TEXT_COMMAND_SCALEFORM_STRING()
					ENDIF
				
				ELIF iInvolvedStatus > -1
					PRINTLN("2554763, G ")
					IF iInvolvedStatus = 0 															// Knocked Out
					AND NOT IS_STRING_NULL_OR_EMPTY(dpadVars.sDpadRowString)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(dpadVars.sDpadRowString) 			
					ELSE
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME("")
					ENDIF
				ELSE
					IF iTime <> -1
					
						PRINTLN("2554763, iTime = ", iTime)
					
						BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
							ADD_TEXT_COMPONENT_SUBSTRING_TIME(iTime, TIME_FORMAT_MINUTES | TIME_FORMAT_SECONDS)	// 7 Score
						END_TEXT_COMMAND_SCALEFORM_STRING()
						
					ELIF fScore <> - 1.0
					
						PRINTLN("2554763, fScore = ", fScore)
					
						BEGIN_TEXT_COMMAND_SCALEFORM_STRING("NUMBER")
							ADD_TEXT_COMPONENT_FLOAT(fScore, 1)											// 7 Score
						END_TEXT_COMMAND_SCALEFORM_STRING()
					
					ELIF iScore <> -1
					
						PRINTLN("2554763, iScore = ", iScore)
						
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iScore)									
					ELSE
						PRINTLN("2554763, SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME, iScore = ", iScore)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME("")
					ENDIF		
				ENDIF
			
				IF dpadVars.eDpadVarType = DPAD_VAR_REPLACE_PLAYER_NAMES
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME("")
				ELSE
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(crewTag) 							// 8 String crewTag
				ENDIF
	
				HANDLE_DPAD_ICON(dpadVars, playerId)
				
				IF (headshotOrPositionNumber = DISPLAY_POSITION_NUMBER) 
				OR IS_STRING_NULL_OR_EMPTY(strHeadshotTxd)											// daveyg if txd is invalid
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME("")								// 10 ranking(-1 to hide)	
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME("")								// 11
				ELSE
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING(strHeadshotTxd)			// NEW_avatarTXD
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING(strHeadshotTxd)			// NEW_avatarTXN
				ENDIF

				// "C" or "F" is Crew/Friend
				IF IS_DPAD_FRIEND_AND_CREW(dpadVars) 
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("DPAD_FRIEND")	
				ELIF IS_DPAD_FRIEND(dpadVars)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("DPAD_FRIEND")	
				ELIF IS_DPAD_CREW(dpadVars)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("DPAD_CREW")
				ELSE
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")										// 12
				ENDIF

				END_SCALEFORM_MOVIE_METHOD()
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC POPULATE_NON_CHALLENGE_PLAYERS(PLAYER_INDEX playerID, STRING sName, SCALEFORM_INDEX& siMovie, DPAD_VARS& dpadVars, INT iRow, TEXT_LABEL_15 crewTag, STRING strHeadshotTxd, INT iOnlineRank, HUD_COLOURS colourRow, eHeadshotOrPositionNumber headshotOrPositionNumber)

	UNUSED_PARAMETER(headshotOrPositionNumber)

	// Draw second page logic 1806319
	IF iRow >= GET_MIN_DPAD_ROW()
	AND iRow < GET_MAX_DPAD_ROW()

		iRow = (iRow % MAX_DPAD_PLAYERS_ONE_PAGE)
		
		INT iPositionToShow 
		iPositionToShow = (iRow + 1)
		
		IF g_dPadSecondPage
			iPositionToShow = ( iPositionToShow + (NUM_NETWORK_PLAYERS / 2) )
		ENDIF
		
		STRING strMethodName = "SET_DATA_SLOT"
		IF g_RefreshDpad = REFRESH_DPAD_UPDATE				
			strMethodName = "UPDATE_SLOT" 
		ENDIF
		
		IF BEGIN_SCALEFORM_MOVIE_METHOD(siMovie, strMethodName)
		
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iRow)											// 1

			IF IS_BIT_SET(dpadVars.iDpadBitSet, ciDPAD_PLAYER_DEAD)
			OR dpadVars.eDpadVarType = DPAD_VAR_REPLACE_PLAYER_NAMES							// 2398710
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME("")
			ELSE
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iOnlineRank)								// 2 Drawing the ranking (number only)
			ENDIF
			
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(sName)	// 3 Player gamertag

			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(colourRow))						// 4 HudColourEnum bgColour 

			IF dpadVars.eDpadVarType = DPAD_VAR_REPLACE_PLAYER_NAMES
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME("")								// 2398710
			ELSE
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(ICON_RANK_FREEMODE)) 			// 5 icon2, the rank globe icon
			ENDIF
	
//			IF headshotOrPositionNumber = DISPLAY_POSITION_NUMBER
//				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPositionToShow)
//			ELSE
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(-1) 										// 6 String/int numberOnAvatarImg
//			ENDIF
			
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME("")
			
			IF dpadVars.eDpadVarType = DPAD_VAR_REPLACE_PLAYER_NAMES
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME("")
			ELSE
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(crewTag) 							// 8 String crewTag
			ENDIF

			HANDLE_DPAD_ICON(dpadVars, playerId)
			
//			IF (headshotOrPositionNumber = DISPLAY_POSITION_NUMBER) 
//			OR IS_STRING_NULL_OR_EMPTY(strHeadshotTxd)											// daveyg if txd is invalid
//				SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME("")								// 10 ranking(-1 to hide)	
//				SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME("")								// 11
//			ELSE
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING(strHeadshotTxd)			// NEW_avatarTXD
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING(strHeadshotTxd)			// NEW_avatarTXN
//			ENDIF

			// "C" or "F" is Crew/Friend
			IF IS_DPAD_FRIEND_AND_CREW(dpadVars) 
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("DPAD_FRIEND")	
			ELIF IS_DPAD_FRIEND(dpadVars)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("DPAD_FRIEND")	
			ELIF IS_DPAD_CREW(dpadVars)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("DPAD_CREW")
			ELSE
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")										// 12
			ENDIF

			END_SCALEFORM_MOVIE_METHOD()
		ENDIF
	ENDIF
ENDPROC

#IF FEATURE_COPS_N_CROOKS

PROC POPULATE_ARCADE_DPAD_ROWS(PLAYER_INDEX playerID, SCALEFORM_INDEX& siMovie, DPAD_VARS& dpadVars, INT iRow, STRING strHeadshotTxd, INT iScore, HUD_COLOURS colourRow, eHeadshotOrPositionNumber headshotOrPositionNumber)
								
	IF iRow >= GET_MIN_DPAD_ROW()
	AND iRow < GET_MAX_DPAD_ROW()

		iRow = (iRow % MAX_DPAD_PLAYERS_ONE_PAGE)
		
		STRING strMethodName = "SET_DATA_SLOT"
		IF g_RefreshDpad = REFRESH_DPAD_UPDATE														//	If g_RefreshDpad is REFRESH_DPAD_REPOPULATE then we should do a full SET_DATA_SLOT
			strMethodName = "UPDATE_SLOT" 
		ENDIF
		
		IF HAS_SCALEFORM_MOVIE_LOADED(siMovie)
			IF BEGIN_SCALEFORM_MOVIE_METHOD(siMovie, strMethodName)
			
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iRow)											// 1

				SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME("")									// 2 Drawing the ranking (number only)
				
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(dpadVars.tl63LeaderboardPlayerNames)	// 3 Player gamertag

				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(colourRow))						// 4 HudColourEnum bgColour 

				SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME("")									// 5 icon2, the rank globe icon
		
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(-1) 											// 6 String/int numberOnAvatarImg
				
				IF dpadVars.eDpadVarType = DPAD_VAR_CASH											// 7 Score
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING("FM_AE_CASH")
					
						ADD_TEXT_COMPONENT_FORMATTED_INTEGER(iScore, INTEGER_FORMAT_COMMA_SEPARATORS)
												
					END_TEXT_COMMAND_SCALEFORM_STRING()
				ELSE
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iScore)									
//				ELSE
//					SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME("")	
				ENDIF
			
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME("")									// 8 String crewTag
	
				HANDLE_DPAD_ICON(dpadVars, playerId)
				
				IF (headshotOrPositionNumber = DISPLAY_POSITION_NUMBER) 
				OR IS_STRING_NULL_OR_EMPTY(strHeadshotTxd)											// daveyg if txd is invalid
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME("")								// 10 ranking(-1 to hide)	
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME("")								// 11
				ELSE
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING(strHeadshotTxd)			// NEW_avatarTXD
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING(strHeadshotTxd)			// NEW_avatarTXN
				ENDIF

				// "C" or "F" is Crew/Friend
				IF IS_DPAD_FRIEND_AND_CREW(dpadVars) 
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("DPAD_FRIEND")	
				ELIF IS_DPAD_FRIEND(dpadVars)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("DPAD_FRIEND")	
				ELIF IS_DPAD_CREW(dpadVars)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("DPAD_CREW")
				ELSE
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")										// 12
				ENDIF

				END_SCALEFORM_MOVIE_METHOD()
			ENDIF
		ENDIF
	ENDIF
ENDPROC

#ENDIF //#IF FEATURE_COPS_N_CROOKS

 // #IF FEATURE_NEW_AMBIENT_EVENTS

#ENDIF	//	USE_JEFFS_NEW_MOVIE_FOR_FREEMODE


PROC HIGHLIGHT_LOCAL_PLAYER(DPAD_VARS& dpadVars, SCALEFORM_INDEX& siMovie)
	IF NOT IS_BIT_SET(dpadVars.iDpadBitSet, ciDPAD_ROW_HIGHLIGHT)
		BEGIN_SCALEFORM_MOVIE_METHOD(siMovie, "SET_HIGHLIGHT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(dpadVars.iPlayersRow)
			PRINTLN("[CS_DAPD] HIGHLIGHT_LOCAL_PLAYER, iRow = ", dpadVars.iPlayersRow)
		END_SCALEFORM_MOVIE_METHOD()
		SET_BIT(dpadVars.iDpadBitSet, ciDPAD_ROW_HIGHLIGHT)
	ENDIF
ENDPROC

FUNC BOOL USE_FREEMODE_MOVIE(LBD_SUB_MODE eSubMode)
	SWITCH eSubMode
		CASE SUB_FREEMODE
		CASE SUB_ARCADE_CNC
		CASE SUB_CHALLENGE
		CASE SUB_BOSS
		CASE SUB_KOTH
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE	  
ENDFUNC

PROC POPULATE_NEW_FREEMODE_DPAD_ROWS(LBD_SUB_MODE eSubMode, SCALEFORM_INDEX& siMovie, DPAD_VARS& dpadVars, INT iRow, TEXT_LABEL_15 crewTag, STRING strHeadshotTxd, INT iOnlineRank, INT iScore, HUD_COLOURS colourRow, 
									eHeadshotOrPositionNumber headshotOrPositionNumber, INT iRowIndexToDisplay, STRING sJobRole = NULL, BOOL bLiteral = FALSE, FLOAT fScore = -1.0, INT iTime = -1, INT iPlayer = -1)
	
	// url:bugstar:3215869 - [XR-046] The white text is hard to read against the yellow CEO team color making it hard to read the player's display name during the Import / Export Special Vehicle Missions.											
	IF colourRow = HUD_COLOUR_G3
		colourRow = HUD_COLOUR_YELLOW
	ENDIF
									
	// Draw second page logic 1806319
	IF iRow >= GET_MIN_DPAD_ROW()
	AND iRow < GET_MAX_DPAD_ROW()

		iRow = (iRow % MAX_DPAD_PLAYERS_ONE_PAGE)

		IF USE_FREEMODE_MOVIE(eSubMode)
			PRINTLN("[3224559] NEW_MOVIE ")
			POPULATE_NEW_FREEMODE_DPAD_ROWS_USING_JEFFS_NEW_FREEMODE_MOVIE(eSubMode, siMovie, dpadVars, iRow, crewTag, strHeadshotTxd, iOnlineRank, iScore, colourRow, headshotOrPositionNumber, fScore, iTime, iPlayer)
		ELSE
			PRINTLN("[3224559] OLD_MOVIE ")

			STRING strMethodName = "SET_DATA_SLOT"
			IF g_RefreshDpad = REFRESH_DPAD_UPDATE				//	If g_RefreshDpad is REFRESH_DPAD_REPOPULATE then we should do a full SET_DATA_SLOT
				PRINTLN("[CS_DPAD] POPULATE_NEW_FREEMODE_DPAD_ROWS, UPDATE_SLOT")
				strMethodName = "UPDATE_SLOT" // Updating
			ELSE
				PRINTLN("[CS_DPAD] POPULATE_NEW_FREEMODE_DPAD_ROWS, SET_DATA_SLOT")
			ENDIF
			
			BOOL bIsFreemode = (eSubMode = SUB_FREEMODE)
			BOOL bIsArcade = (eSubMode = SUB_ARCADE_CNC)
			BOOL bIsRace = IS_SUBMODE_RACE(eSubMode) // Optimise
			BOOL bIsDeathmatch = IS_SUBMODE_DEATHMATCH(eSubMode)
			
			IF BEGIN_SCALEFORM_MOVIE_METHOD(siMovie, strMethodName)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iRow)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)													// default
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)													// Team bars group
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)													// Layout type
				IF IS_BIT_SET(dpadVars.iDpadBitSet, ciDPAD_PLAYER_DEAD)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME("")
				ELSE
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iOnlineRank)
				ENDIF
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)													// default
				
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(dpadVars.tl63LeaderboardPlayerNames)		// Player gamertag
				IF IS_BIT_SET(dpadVars.iDpadBitSet, ciDPAD_LAMARS_ROW)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(HUD_COLOUR_NET_PLAYER1))			
				ELSE
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(colourRow))						// HudColourEnum bgColour 
				ENDIF
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(DPAD_TAGGED_FULL_BLOCK) 							// taggedType

				PRINTLN("5472445, dpadVars.mpIconActive = ", ENUM_TO_INT(dpadVars.mpIconActive))
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(dpadVars.mpIconActive)) 
				
				IF IS_BIT_SET(dpadVars.iDpadBitSet, ciDPAD_PLAYER_DEAD)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(STATUS_DEAD))
				ELSE
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(ICON_RANK_FREEMODE)) 				// icon2
				ENDIF

		//		DO_SPECIFIC_POSITION(eSubMode, iRow)													// Position number or ignore for headshots
				IF headshotOrPositionNumber = DISPLAY_POSITION_NUMBER
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iRowIndexToDisplay)
				ELSE
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(-1) 											// String/int numberOnAvatarImg
				ENDIF


				DO_SPECIFIC_MODE_SCORES(eSubMode, dpadVars, iRow, iScore, strHeadshotTxd, sJobRole, bLiteral, iRowIndexToDisplay)		// Kills, time etc.
			
				IF bIsFreemode OR bIsArcade
					IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciHARD_TARGET_ENABLE_MODE)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(crewTag) 								// String crewTag
					ELSE
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME("") 									// Nothing.
					ENDIF
				ELSE
					IF NOT bIsRace
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")	
					ENDIF
				ENDIF
				IF (NOT bIsRace) AND (NOT bIsDeathmatch)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bIsFreemode OR bIsArcade)						// Show JP
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)												// inumGroupMembers
					
					IF (headshotOrPositionNumber = DISPLAY_POSITION_NUMBER) OR IS_STRING_NULL_OR_EMPTY(strHeadshotTxd)											//daveyg if txd is invalid
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME("")								//ranking(-1 to hide)	
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME("")
					ELSE
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING(strHeadshotTxd)			// NEW_avatarTXD
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING(strHeadshotTxd)			// NEW_avatarTXN
					ENDIF
					// "C" or "F" is Crew/Friend
					IF bIsFreemode OR bIsArcade
						IF IS_DPAD_FRIEND_AND_CREW(dpadVars) 
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("DPAD_FRIEND")	
						ELIF IS_DPAD_FRIEND(dpadVars)
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("DPAD_FRIEND")	
						ELIF IS_DPAD_CREW(dpadVars)
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("DPAD_CREW")
						ELSE
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")	
						ENDIF
					ENDIF
				ENDIF
				
				END_SCALEFORM_MOVIE_METHOD()
			ENDIF

		ENDIF	

	ENDIF
ENDPROC

/// PURPOSE: Sets up the player details for the summary card
PROC SET_SCALEFORM_SUMMARY_CARD_ROW_PLAYER(	SCALEFORM_INDEX siMovie, BOOL bIsUpdating, INT iSlot, INT iRank, HUD_COLOURS highlightColour, STRING tlGamertag, 
											STRING tlCrewTag, STRING strHeadshotTxd, BOOL bSpectated = FALSE, INT iPlacing = -1, BOOL bPlayerCanBeSpectated = TRUE)

//	PRINTLN("MP Scaleform: SET_SCALEFORM_SUMMARY_CARD_ROW_PLAYER")
	
	STRING strMethodName = "SET_DATA_SLOT"
	IF bIsUpdating
		strMethodName = "UPDATE_SLOT"
	ENDIF
	
	BEGIN_SCALEFORM_MOVIE_METHOD(siMovie, strMethodName)
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSlot)									// Row value
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)										// default
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)										// default
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)										// Layout type
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iRank)									// Rank of player
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)										// default
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(tlGamertag)					// Player gamertag

		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(highlightColour))							// Blue or Red highlight
				
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)
		
		IF bSpectated
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(SPECTATOR))
		ELSE
			IF bPlayerCanBeSpectated
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
			ELSE
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(SPECTATE_DISABLED))
			ENDIF
		ENDIF

		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(ICON_RANK_FREEMODE))
		
		
		IF iPlacing > -1
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPlacing)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")
		ELSE
			IF IS_STRING_NULL_OR_EMPTY(strHeadshotTxd)	//daveyg if txd is invalid
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING("char_default")		// Outline headshot txd
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING("char_default")		// Outline headshot txn
			ELSE
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING(strHeadshotTxd)		// Headshot txd
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING(strHeadshotTxd)		// Headshot txn
			ENDIF
		ENDIF
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(tlCrewTag)						// Player crewtag
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

/// PURPOSE: Sets up the player details for the Spectator summary card
PROC SET_SCALEFORM_SPECTATOR_SUMMARY_CARD_ROW_PLAYER(	SCALEFORM_INDEX siMovie, BOOL bIsUpdating, INT iRow, INT iRank, HUD_COLOURS highlightColour, STRING tlGamertag, 
											STRING tlCrewTag, STRING strHeadshotTxd, BOOL bSpectated = FALSE, INT iPlacing = -1, BOOL bPlayerCanBeSpectated = TRUE, BOOL bPlayerDead = FALSE)
	
	STRING strMethodName = "ADD_NEW_ORDER_ITEM"
	IF bIsUpdating
		strMethodName = "UPDATE_SLOT"
		CPRINTLN(DEBUG_SPECTATOR, "=== HUD === SPEC_HUD_STAGE_FULL_LIST - UPDATE_SLOT")
	ELSE
		CPRINTLN(DEBUG_SPECTATOR, "=== HUD === SPEC_HUD_STAGE_FULL_LIST - ADD_NEW_ORDER_ITEM")
	ENDIF
	
	BEGIN_SCALEFORM_MOVIE_METHOD(siMovie, strMethodName)
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iRow)									// Row value
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)										// default
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)										// default
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)										// Layout type						
		IF bPlayerDead
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME("")
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iRank)								// Rank of player
		ENDIF
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)										// default
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(tlGamertag)					// Player gamertag

		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(highlightColour))			// Blue or Red highlight
				
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)
		
		IF bSpectated
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(SPECTATOR))
		ELSE
			IF bPlayerCanBeSpectated
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
			ELSE
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(SPECTATE_DISABLED))
			ENDIF
		ENDIF
		
		IF bPlayerDead
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(STATUS_DEAD))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(ICON_RANK_FREEMODE)) 	// icon2
		ENDIF
		
		// KGM 14/8/15 [BUG 2465753]: Numbers represent teams and should never be present on the spectator list instead of headshots, so I'm removing the option to display a number
		//		(Numbers are used on team-based leaderboards to represent winning team, etc) - this was 'probably' a copy 'n' paste from the leaderboard display routine.
		//		NOTE: This possibly disguises a bug in the calling routine where the 'team' variable should probably have been cleared somewhere but got left set and passed into here.
		UNUSED_PARAMETER(iPlacing)
		
//		IF iPlacing > -1
//			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPlacing)
//			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")
//		ELSE
			IF IS_STRING_NULL_OR_EMPTY(strHeadshotTxd)	//daveyg if txd is invalid
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING("char_default")		// Outline headshot txd
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING("char_default")		// Outline headshot txn
			ELSE
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING(strHeadshotTxd)		// Headshot txd
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING(strHeadshotTxd)		// Headshot txn
			ENDIF
//		ENDIF
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(tlCrewTag)								// Player crewtag
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC SET_SCALEFORM_SUMMARY_CARD_ROW_JUST_STRING(SCALEFORM_INDEX &siMovie, BOOL bIsUpdating, INT iSlot, HUD_COLOURS rowColour, STRING tlDisplayStr, 
												BOOL bLiteralString = TRUE, BOOL bSpectated = FALSE, INT iPlacing = -1)
	PRINTLN("MP Scaleform: SET_SCALEFORM_SUMMARY_CARD_ROW_JUST_STRING")
	
	STRING strMethodName = "SET_DATA_SLOT"
	IF bIsUpdating
		strMethodName = "UPDATE_SLOT"
	ENDIF

	BEGIN_SCALEFORM_MOVIE_METHOD(siMovie, strMethodName)
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSlot)									// Row value
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)										// default
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)										// default
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)										// Layout type
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")									// Rank of player
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)										// default
		
		IF bLiteralString
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(tlDisplayStr)				// Player gamertag
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(tlDisplayStr)					// Text label
		ENDIF
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(rowColour))							// Blue or Red highlight
				
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)			//column left of rank1
		IF bSpectated									//column left of rank2
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(SPECTATOR))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		ENDIF			
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)			//under rank symbol
		
		IF iPlacing > -1
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPlacing)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING("char_default")		// Outline headshot txd
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING("char_default")		// Outline headshot txn
		ENDIF
		
//		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)			
//		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)										
//		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)		
//		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)		
		
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC SET_SCALEFORM_SUMMARY_CARD_HIGHLIGHT(SCALEFORM_INDEX &siMovie, INT iSlot)
	BEGIN_SCALEFORM_MOVIE_METHOD(siMovie, "SET_HIGHLIGHT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSlot)
	END_SCALEFORM_MOVIE_METHOD()
	
ENDPROC

/// PURPOSE: Displays the scaleform movie passed
PROC DISPLAY_VIEW(SCALEFORM_INDEX &siMovie, BOOL bCollapseLeft = FALSE, BOOL bDoDisplayView = TRUE)
	PRINTLN("MP Scaleform: DISPLAY_VIEW")
	
	IF BEGIN_SCALEFORM_MOVIE_METHOD(siMovie, "COLLAPSE")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bCollapseLeft)
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
	
	IF bDoDisplayView = TRUE
		IF BEGIN_SCALEFORM_MOVIE_METHOD(siMovie, "DISPLAY_VIEW")
			END_SCALEFORM_MOVIE_METHOD()
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Draws the summary card
PROC GET_SCALEFORM_SUMMARY_CARD_POSITION(FLOAT& fReturnX, FLOAT& fReturnY, FLOAT& fReturnWidth, FLOAT& fReturnHeight, UI_ALIGNMENT alignX = UI_ALIGN_LEFT)

	SET_SCRIPT_GFX_ALIGN(alignX, UI_ALIGN_TOP)
	
	IF alignX = UI_ALIGN_RIGHT
		fReturnX		= 0.14
		fReturnY		= 0.3
		fReturnWidth	= 0.28
		fReturnHeight	= 0.6
	ELIF alignX = UI_ALIGN_LEFT
		fReturnX		= 0.14
		fReturnY		= 0.3
		fReturnWidth	= 0.28
		fReturnHeight	= 0.6
	ENDIF
	
	GET_SCRIPT_GFX_ALIGN_POSITION(fReturnX, fReturnY, fReturnX, fReturnY)
	RESET_SCRIPT_GFX_ALIGN()

ENDPROC

//PURPOSE: Draws the summary card
PROC DRAW_SCALEFORM_SUMMARY_CARD(SCALEFORM_INDEX &siMovie, UI_ALIGNMENT alignX = UI_ALIGN_LEFT)
	SET_SCRIPT_GFX_ALIGN(alignX, UI_ALIGN_TOP)
	IF alignX = UI_ALIGN_RIGHT
		DRAW_SCALEFORM_MOVIE(siMovie, 0.14, 0.3, 0.28, 0.6, 255, 255, 255, 255)
	ELIF alignX = UI_ALIGN_LEFT
		DRAW_SCALEFORM_MOVIE(siMovie, 0.14, 0.3, 0.28, 0.6, 255, 255, 255, 255)
	ENDIF
	RESET_SCRIPT_GFX_ALIGN()
ENDPROC

/// PURPOSE: Cleans up memory for textures
PROC SCALEFORM_SUMMARY_CARD_DESTROY(SCALEFORM_INDEX &siMovie)
	PRINTLN("MP Scaleform: SCALEFORM_SUMMARY_CARD_DESTROY")
	
	BEGIN_SCALEFORM_MOVIE_METHOD(siMovie, "ON_DESTROY")
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

/// PURPOSE: Prints a black box with white text underneath the summary card
PROC SCALEFORM_SUMMARY_CARD_PRINT_HELP(SCALEFORM_INDEX &siMovie, STRING sHelpLabel)
	PRINTLN("MP Scaleform: SCALEFORM_SUMMARY_CARD_PRINT_HELP")
	
	BEGIN_SCALEFORM_MOVIE_METHOD(siMovie, "SET_DESCRIPTION")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sHelpLabel)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

/// PURPOSE: 	Returns true when player is using DPAD or LSTICK to indicated wanting to increment the current selection. iIncrement returns
///    			1 or -1 based on direction. bHorizontal = True will deal with left/right, false will deal with up/down
FUNC BOOL SHOULD_SELECTION_BE_INCREMENTED(TIME_DATATYPE &selectionIncrementTimer, INT &iIncrement, BOOL &bButtonPressed, BOOL bHorizontal = TRUE, BOOL bSelectActive = FALSE, INT iOverrideTime = -1, BOOL bDoSounds = TRUE, INT iCurrentSelection = 0,INT iCurrentColumn = 0)

	IF IS_PC_VERSION()
	AND NETWORK_TEXT_CHAT_IS_TYPING()
		RETURN FALSE
	ENDIF

	BOOL bIncrement
	bButtonPressed = FALSE
	//INT iCursorValueChange
	INT iTimerDelay = SELECTION_INCREMENT_TIMER_DELAY

	IF iOverrideTime != -1
		iTimerDelay = iOverrideTime
	ENDIF
	
	SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
	//DISPLAY_TEXT_WITH_NUMBER( 0.0, 0.0, "NUMBER", PAUSE_MENU_GET_MOUSE_HOVER_INDEX() )
	//DISPLAY_TEXT_WITH_NUMBER( 0.0, 0.1, "NUMBER", PAUSE_MENU_GET_MOUSE_HOVER_UNIQUE_ID() )
	//DISPLAY_TEXT_WITH_NUMBER( 0.0, 0.2, "NUMBER", PAUSE_MENU_GET_MOUSE_HOVER_MENU_ITEM_ID() )
	//DISPLAY_TEXT_WITH_NUMBER( 0.0, 0.3, "NUMBER", iCurrentSelection )
	
	IF bHorizontal
		// Listen for right button press
		IF IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
		OR (GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_X) > 0.3)
		OR IS_PC_MOUSE_ACCEPT_JUST_RELEASED(iCurrentSelection, 1, iCurrentColumn)
		OR (bSelectActive AND (IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)))
		
			bButtonPressed = TRUE
		
			IF ABSI(GET_TIME_DIFFERENCE(selectionIncrementTimer, GET_NETWORK_TIME())) > iTimerDelay
			
				IF bDoSounds
					PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
				ENDIF
					
				selectionIncrementTimer = GET_NETWORK_TIME()
				
				bIncrement = TRUE
				iIncrement = 1
			ENDIF
		ENDIF
		
		// Listen for left button press
		IF IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
		OR IS_PC_MOUSE_ACCEPT_JUST_RELEASED(iCurrentSelection, -1, iCurrentColumn)
		OR (GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_X) < -0.3)
		
			bButtonPressed = TRUE
		
			IF ABSI(GET_TIME_DIFFERENCE(selectionIncrementTimer, GET_NETWORK_TIME())) > iTimerDelay
			
				IF bDoSounds
					PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
				ENDIF

				selectionIncrementTimer = GET_NETWORK_TIME()
				
				bIncrement = TRUE
				iIncrement = -1
			ENDIF
		ENDIF
		
		// Listen for arrow hold
		IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL) AND IS_CURSOR_ACCEPT_HELD()
			IF ABSI(GET_TIME_DIFFERENCE(selectionIncrementTimer, GET_NETWORK_TIME())) > iTimerDelay
				IF IS_PC_MOUSE_SELECTING_CURRENT_ITEM(iCurrentSelection, -1, iCurrentColumn)
					bButtonPressed = TRUE
					iIncrement = -1
					bIncrement = TRUE		
					selectionIncrementTimer = GET_NETWORK_TIME()
				ELIF IS_PC_MOUSE_SELECTING_CURRENT_ITEM(iCurrentSelection, 1, iCurrentColumn)
					bButtonPressed = TRUE
					iIncrement = 1
					bIncrement = TRUE		
					selectionIncrementTimer = GET_NETWORK_TIME()
				ENDIF
				
				IF bDoSounds AND bButtonPressed
					PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
				ENDIF
			ENDIF
			
			/*
			IF IS_CURSOR_ACCEPT_HELD() AND IS_PC_MOUSE_SELECTING_CURRENT_ITEM(iCurrentSelection)
				IF ABSI(GET_TIME_DIFFERENCE(selectionIncrementTimer, GET_NETWORK_TIME())) > iTimerDelay
				
					bButtonPressed = TRUE
					
					iCursorValueChange = GET_CURSOR_MENU_ITEM_VALUE_CHANGE()
					IF (iCursorValueChange = -1)
						iIncrement = -1
						bIncrement = TRUE
					ENDIF
					
					IF (iCursorValueChange = 1)
						iIncrement = 1
						bIncrement = TRUE
					ENDIF			
					
					selectionIncrementTimer = GET_NETWORK_TIME()
				ENDIF
			ENDIF
			*/
		ENDIF
	ELSE
		// Listen for up button press
		IF IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
		OR (GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y) < -0.3)
		OR IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_UP)
		OR (bSelectActive AND IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT))
		
			bButtonPressed = TRUE
			
			// Scrollwheel needs faster response
			IF IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_UP)
				iTimerDelay = 100
			ENDIF
			
			IF ABSI(GET_TIME_DIFFERENCE(selectionIncrementTimer, GET_NETWORK_TIME())) > iTimerDelay
					
				selectionIncrementTimer = GET_NETWORK_TIME()
				
				IF bDoSounds
					PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", "HUD_FRONTEND_DEFAULT_SOUNDSET")
				ENDIF
				
				bIncrement = TRUE
				iIncrement = 1
			ENDIF
		ENDIF
		
		// Listen for down button press
		IF IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
		OR (GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y) > 0.3)
		OR IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_DOWN)
		
			bButtonPressed = TRUE
			
			// Scrollwheel needs faster response
			IF IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_DOWN)
				iTimerDelay = 100
			ENDIF
		
			IF ABSI(GET_TIME_DIFFERENCE(selectionIncrementTimer, GET_NETWORK_TIME())) > iTimerDelay
				
				selectionIncrementTimer = GET_NETWORK_TIME()
				
				IF bDoSounds
					PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", "HUD_FRONTEND_DEFAULT_SOUNDSET")
				ENDIF
				
				bIncrement = TRUE
				iIncrement = -1
			ENDIF
		ENDIF
	ENDIF
	
	RETURN bIncrement
ENDFUNC

/// PURPOSE: 	Returns true when player is using DPAD or LSTICK to indicated wanting to increment the current selection. iIncrement returns
///    			1 or -1 based on direction. bHorizontal = True will deal with left/right, false will deal with up/down
FUNC BOOL SHOULD_ANALOGUE_SELECTION_BE_INCREMENTED(TIME_DATATYPE &selectionIncrementTimer, INT &iIncrement, BOOL bHorizontal = TRUE, INT iOverrideTime = -1, BOOL bDoSounds = TRUE, INT iMouseIncrement = 0)

	BOOL bIncrement

	INT iTimerDelay = SELECTION_INCREMENT_TIMER_DELAY

	IF iOverrideTime != -1
		iTimerDelay = iOverrideTime
	ENDIF

	IF bHorizontal
		// Listen for right button press
		IF (GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_X) > 0.3)
		OR iMouseIncrement = 1
		
			IF ABSI(GET_TIME_DIFFERENCE(selectionIncrementTimer, GET_NETWORK_TIME())) > iTimerDelay
			
				IF bDoSounds
					PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
				ENDIF
					
				selectionIncrementTimer = GET_NETWORK_TIME()
				
				bIncrement = TRUE
				iIncrement = 1
			ENDIF
		ENDIF
		
		// Listen for left button press
		IF (GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_X) < -0.3)
		OR iMouseIncrement = -1
		
			IF ABSI(GET_TIME_DIFFERENCE(selectionIncrementTimer, GET_NETWORK_TIME())) > iTimerDelay
			
				IF bDoSounds
					PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
				ENDIF

				selectionIncrementTimer = GET_NETWORK_TIME()
				
				bIncrement = TRUE
				iIncrement = -1
			ENDIF
		ENDIF
	ELSE
		// Listen for up button press
		IF (GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y) < -0.3)
		
			IF ABSI(GET_TIME_DIFFERENCE(selectionIncrementTimer, GET_NETWORK_TIME())) > iTimerDelay
					
				selectionIncrementTimer = GET_NETWORK_TIME()
				
				IF bDoSounds
					PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", "HUD_FRONTEND_DEFAULT_SOUNDSET")
				ENDIF
				
				bIncrement = TRUE
				iIncrement = 1
			ENDIF
		ENDIF
		
		// Listen for down button press
		IF (GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y) > 0.3)
		
			IF ABSI(GET_TIME_DIFFERENCE(selectionIncrementTimer, GET_NETWORK_TIME())) > iTimerDelay
				
				selectionIncrementTimer = GET_NETWORK_TIME()
				
				IF bDoSounds
					PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", "HUD_FRONTEND_DEFAULT_SOUNDSET")
				ENDIF
				
				bIncrement = TRUE
				iIncrement = -1
			ENDIF
		ENDIF
	ENDIF
	
	RETURN bIncrement
ENDFUNC

/// PURPOSE: 	SINGLEPLAYER VERSION - Returns true when player is using DPAD or LSTICK to indicated wanting to increment the current selection. iIncrement returns
///    			1 or -1 based on direction. bHorizontal = True will deal with left/right, false will deal with up/down
FUNC BOOL SHOULD_SELECTION_BE_INCREMENTED_SP(INT &selectionIncrementTimer, INT &iIncrement, BOOL &bButtonPressed, BOOL bHorizontal = TRUE, BOOL bSelectActive = FALSE, BOOL bPlaySound = TRUE, INT iOverrideTime = -1, INT iMouseIncrement = 0)

	BOOL bIncrement
	bButtonPressed = FALSE


	INT iTimerDelay = SELECTION_INCREMENT_TIMER_DELAY

	IF iOverrideTime != -1
		iTimerDelay = iOverrideTime
	ENDIF

	IF bHorizontal
		// Listen for right button press
		IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
		OR (GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_X) > 0.3)
		OR (bSelectActive AND IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT))
		OR iMouseIncrement = 1
		
			bButtonPressed = TRUE
			iIncrement = 1
					
			IF ABSI(TIMERA() - selectionIncrementTimer) > iTimerDelay
				GET_CURRENT_TIMEOFDAY()
				
				IF bPlaySound
					PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
				ENDIF
					
				selectionIncrementTimer = TIMERA()
				
				bIncrement = TRUE
			ENDIF
		ENDIF
		
		// Listen for left button press
		IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
		OR (GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_X) < -0.3)
		OR iMouseIncrement = -1
		
			bButtonPressed = TRUE
			iIncrement = -1
			
			IF ABSI(TIMERA() - selectionIncrementTimer) > iTimerDelay
			
				IF bPlaySound
					PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
				ENDIF

				selectionIncrementTimer = TIMERA()
				
				bIncrement = TRUE
			ENDIF
		ENDIF
	ELSE
		// Listen for up button press
		IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
		OR (GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y) < -0.3)
		OR (bSelectActive AND IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT))
		
			bButtonPressed = TRUE
			iIncrement = 1
		
			IF ABSI(TIMERA() - selectionIncrementTimer) > iTimerDelay
					
				selectionIncrementTimer = TIMERA()
				
				IF bPlaySound
					PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", "HUD_FRONTEND_DEFAULT_SOUNDSET")
				ENDIF
				
				bIncrement = TRUE
			ENDIF
		ENDIF
		
		// Listen for down button press
		IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
		OR (GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y) > 0.3)
		
			bButtonPressed = TRUE
			iIncrement = -1
		
			IF ABSI(TIMERA() - selectionIncrementTimer) > iTimerDelay
				
				selectionIncrementTimer = TIMERA()
				
				IF bPlaySound
					PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", "HUD_FRONTEND_DEFAULT_SOUNDSET")
				ENDIF
				
				bIncrement = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN bIncrement
ENDFUNC

// **************************************************************
// 				CORONA LOBBY: Scaleform Functions
// Following functions can be called to set data for frontend 
// 			screens used within the FM corona lobby.
// **************************************************************


/// PURPOSE: Sets the title on the scaleform movie passed.
PROC CORONA_SET_SCALEFORM_TITLE(SCALEFORM_INDEX &siMovie, STRING strTitle, BOOL bVerified, BOOL bLiteralString = TRUE)
	
	PRINTLN("MP Scaleform: CORONA_SET_SCALEFORM_TITLE - title: ", strTitle)
	
	BEGIN_SCALEFORM_MOVIE_METHOD(siMovie, "SET_TITLE")
		IF bLiteralString
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(strTitle)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(strTitle)
		ENDIF
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bVerified)
		
	END_SCALEFORM_MOVIE_METHOD()

ENDPROC


/// PURPOSE: Displays the scaleform movie passed
PROC CORONA_SCALEFORM_DISPLAY_VIEW(SCALEFORM_INDEX &siMovie)
	PRINTLN("MP Scaleform: CORONA_DISPLAY_VIEW")
	
	BEGIN_SCALEFORM_MOVIE_METHOD(siMovie, "DISPLAY_VIEW")
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC


PROC CORONA_ADD_SCALEFORM_ODD_STRING(MULTIPLAYER_SETTING_BETTING_ODDS oddsFormat, INT iOddN, INT iOddD, STRING strLabelPrefix = NULL)

	TEXT_LABEL_23 strLabel
	TEXT_LABEL_23 tlPrefix
	
	IF IS_STRING_NULL_OR_EMPTY(strLabelPrefix)
		tlPrefix = "MP"
	ELSE
		tlPrefix = strLabelPrefix
	ENDIF

	SWITCH oddsFormat
	
		CASE MP_SETTING_ODDS_FRACTIONAL
			strLabel = tlPrefix
			IF iOddN = iOddD
				strLabel += "_BET_EVENS"
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(strLabel)	// Evens
			ELSE
				strLabel += "_BET_ODDS"
				BEGIN_TEXT_COMMAND_SCALEFORM_STRING(strLabel)	// ~1~/~1~
					ADD_TEXT_COMPONENT_INTEGER(iOddN)
					ADD_TEXT_COMPONENT_INTEGER(iOddD)
				END_TEXT_COMMAND_SCALEFORM_STRING()									
			ENDIF
		BREAK
		
		CASE MP_SETTING_ODDS_DECIMAL
			FLOAT fOddDecimal
			fOddDecimal = (TO_FLOAT(iOddN) / TO_FLOAT(iOddD)) + 1
			
			strLabel = tlPrefix
			IF NOT IS_STRING_NULL_OR_EMPTY(strLabelPrefix)
				strLabel += "_BET_NUMBER"
			ELSE
				strLabel = "NUMBER"
			ENDIF
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING(strLabel)	// ~1~
				ADD_TEXT_COMPONENT_FLOAT(fOddDecimal, 2)
			END_TEXT_COMMAND_SCALEFORM_STRING()												
		BREAK
		
		CASE MP_SETTING_ODDS_MONEYLINE
			strLabel = tlPrefix
			IF iOddN = iOddD
				strLabel += "_BET_US_P"
				BEGIN_TEXT_COMMAND_SCALEFORM_STRING(strLabel)	// +~1~
					ADD_TEXT_COMPONENT_INTEGER(100)
				END_TEXT_COMMAND_SCALEFORM_STRING()											
			ELIF (TO_FLOAT(iOddN) / TO_FLOAT(iOddD)) > 1.0
				strLabel += "_BET_US_P"
				BEGIN_TEXT_COMMAND_SCALEFORM_STRING(strLabel)	// +~1~
					ADD_TEXT_COMPONENT_INTEGER(ROUND(100 * (TO_FLOAT(iOddN) / TO_FLOAT(iOddD))))
				END_TEXT_COMMAND_SCALEFORM_STRING()
			ELSE
				IF NOT IS_STRING_NULL_OR_EMPTY(strLabelPrefix)
					strLabel += "_BET_NUMBER"
				ELSE
					strLabel = "NUMBER"
				ENDIF
				BEGIN_TEXT_COMMAND_SCALEFORM_STRING(strLabel)	// ~1~
					ADD_TEXT_COMPONENT_INTEGER(ROUND(-100 / (TO_FLOAT(iOddN) / TO_FLOAT(iOddD))))
				END_TEXT_COMMAND_SCALEFORM_STRING()	
			ENDIF
		BREAK
		
		DEFAULT
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		BREAK
	ENDSWITCH 

ENDPROC


/// PURPOSE: Sets up the player details for the summary card
PROC CORONA_SET_SCALEFORM_PLAYER_SUMMARY(SCALEFORM_INDEX siMovie, INT iSlot, INT iRank, HUD_COLOURS highlightColour, STRING tlGamertag, STRING tlCrewTag, STRING strHeadshotTxd, MULTIPLAYER_SETTING_BETTING_ODDS oddsFormat,  INT iOddN = -1, INT iOddD = -1, BOOL bPlayerVoted = FALSE, BOOL bSpectated = FALSE)

	PRINTLN("MP Scaleform: CORONA_SET_SCALEFORM_PLAYER_SUMMARY")
	
	BOOL bOddsAvailable
	
	BEGIN_SCALEFORM_MOVIE_METHOD(siMovie, "SET_DATA_SLOT")
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSlot)									// Row value
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)										// default
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)										// default
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)										// Layout type
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iRank)									// Rank of player
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)										// default
		
		IF NOT bPlayerVoted
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(tlGamertag)					// Player gamertag
		ELSE
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("FM_BET_RDY")
				ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(tlGamertag)
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ENDIF
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(highlightColour))							// Blue or Red highlight
				
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		
		IF iOddN > 0																// Dislay odds if betting
		AND iOddD > 0
			bOddsAvailable = TRUE
			CORONA_ADD_SCALEFORM_ODD_STRING(oddsFormat, iOddN, iOddD)
		ELSE
			IF bSpectated
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(SPECTATOR))
			ELSE
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
			ENDIF
		ENDIF

		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(ICON_RANK_FREEMODE))
		
		
		IF IS_STRING_NULL_OR_EMPTY(strHeadshotTxd)	//daveyg if txd is invalid
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(-1)									//ranking(-1 to hide)	
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING(strHeadshotTxd)		// Headshot txd
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING(strHeadshotTxd)		// Headshot txn
		ENDIF
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(tlCrewTag)						// Player crewtag
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bOddsAvailable)
		
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC


/// PURPOSE: Sets up the vehicle details on summary card
PROC CORONA_SET_SCALEFORM_VEHICLE_SUMMARY(SCALEFORM_INDEX &siMovie, INT iSlot, INT iRank,  HUD_COLOURS highlightColour, STRING tlVehicleName, INT iCarRed, INT iCarGreen, INT iCarBlue)

	PRINTLN("MP Scaleform: CORONA_SET_SCALEFORM_VEHICLE_SUMMARY")
	
	BEGIN_SCALEFORM_MOVIE_METHOD(siMovie, "SET_DATA_SLOT")
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSlot)									// Row value
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)										// default
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)										// default
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)										// Layout type
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iRank)									// Rank of player
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)										// default
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(tlVehicleName)						// Vehicle name
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(highlightColour))							// Blue or Red highlight
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)									
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(ICON_RANK_FREEMODE))
				
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iCarRed)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iCarGreen)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iCarBlue)
		
	END_SCALEFORM_MOVIE_METHOD()

ENDPROC


/// PURPOSE: Sets up the vehicle details on summary card
PROC CORONA_SET_SCALEFORM_PLAYER_BETTING_SUMMARY(SCALEFORM_INDEX &siMovie, INT iSlot, INT iRank, INT iCash, MULTIPLAYER_SETTING_BETTING_ODDS oddsFormat, INT iOddN, INT iOddD, HUD_COLOURS highlightColour, STRING strHeadshotTxd)

	PRINTLN("MP Scaleform: CORONA_SET_SCALEFORM_PLAYER_BETTING_SUMMARY")
	
	BEGIN_SCALEFORM_MOVIE_METHOD(siMovie, "SET_DATA_SLOT")
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSlot)									// Row value
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)										// default
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)										// default
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)										// Layout type
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iRank)									// Rank of player
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)										// default
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("MP_BET_CASH")
			ADD_TEXT_COMPONENT_INTEGER(iCash)										// Cash
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(highlightColour))							// Blue or Red highlight
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		
		CORONA_ADD_SCALEFORM_ODD_STRING(oddsFormat, iOddN, iOddD)
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(ICON_RANK_FREEMODE))
				
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING(strHeadshotTxd)		// Headshot txd
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING(strHeadshotTxd)		// Headshot txn
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)									// Odds to display
	END_SCALEFORM_MOVIE_METHOD()

ENDPROC


/// PURPOSE: Sets the description for the passed scaleform
PROC CORONA_SET_SCALEFORM_DESCRIPTION(SCALEFORM_INDEX &siMovie, INT iCash = -1, INT iBetsPlaced = -1, INT iBetsOnYou = -1)
	
	PRINTLN("MP Scaleform: CORONA_SET_SCALEFORM_DESCRIPTION - iCash: ", iCash, " - iBetsPlaced: ", iBetsPlaced, " - iBetsOnYou: ", iBetsOnYou)
	
	BEGIN_SCALEFORM_MOVIE_METHOD(siMovie, "SET_DESCRIPTION")
	
		IF iCash = -1
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")
		ELSE
	
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("MP_BET_CASH")
				ADD_TEXT_COMPONENT_INTEGER(iCash)										// $
			END_TEXT_COMMAND_SCALEFORM_STRING()
			
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("MP_BET_CASH")
				ADD_TEXT_COMPONENT_INTEGER(iBetsPlaced)									// $
			END_TEXT_COMMAND_SCALEFORM_STRING()
			
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("MP_BET_CASH")
				ADD_TEXT_COMPONENT_INTEGER(iBetsOnYou)									// $
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ENDIF
			
	END_SCALEFORM_MOVIE_METHOD()
	
ENDPROC


PROC CORONA_DISPLAY_VEHICLE_SELECTOR(SCALEFORM_INDEX &siMovie, STRING strVehName, STRING strVehType, STRING strColour, STRING strColourType, INT iOptionalInt = -1)

	PRINTLN("MP Scaleform: CORONA_DISPLAY_VEHICLE_SELECTOR - strVehName: ", strVehName, " , strColour: ", strColour)
	
	BEGIN_SCALEFORM_MOVIE_METHOD(siMovie, "SHOW_VEHICLE_SELECTOR")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(strVehName)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(strVehType)
		
		IF iOptionalInt != -1
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING(strColour)
				ADD_TEXT_COMPONENT_INTEGER(iOptionalInt)
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(strColour)
		ENDIF
		
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(strColourType)
	END_SCALEFORM_MOVIE_METHOD()

ENDPROC

PROC CORONA_SHOW_GAMER_INFO(SCALEFORM_INDEX &siMovie, BOOL bFriendly, BOOL bInCrew, PLAYER_INDEX playerID, STRING stCrewName, STRING stCrewTag = NULL, STRING stCrewEmblem = NULL)

	PRINTLN("MP Scaleform: CORONA_SHOW_GAMER_INFO")
	
	HUD_COLOURS playerColour = HUD_COLOUR_BLUE
	
	IF NOT bFriendly
		playerColour = HUD_COLOUR_RED
	ENDIF
	
	BEGIN_SCALEFORM_MOVIE_METHOD(siMovie, "SHOW_GAMER_INFO")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(playerColour))					// Colour of players name
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(GET_PLAYER_NAME(playerID))			// Players name
		
		// Display text beneath name
		IF bInCrew
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(stCrewName)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(stCrewName)
		ENDIF
		
		// Add crew tag
		IF IS_STRING_NULL_OR_EMPTY(stCrewTag)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME("")
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(stCrewTag)
		ENDIF
		
		// Add emblem
		IF bInCrew
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING(stCrewEmblem)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING(stCrewEmblem)
		ELSE
			IF playerID = PLAYER_ID()
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING("MPHud")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING("MP_No_Crew")
			ENDIF
		ENDIF		
		
	END_SCALEFORM_MOVIE_METHOD()

ENDPROC

PROC CORONA_UPDATE_VEHICLE_NAME(SCALEFORM_INDEX &siMovie, STRING strVehName)

	PRINTLN("MP Scaleform: CORONA_UPDATE_VEHICLE_NAME - strVehName: ", strVehName)
	
	BEGIN_SCALEFORM_MOVIE_METHOD(siMovie, "SET_VEHICLE_NAME")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(strVehName)
	END_SCALEFORM_MOVIE_METHOD()

ENDPROC

PROC CORONA_UPDATE_VEHICLE_TYPE(SCALEFORM_INDEX &siMovie, INT iVehicleVariant)

	PRINTLN("MP Scaleform: CORONA_UPDATE_VEHICLE_NAME - iVehicleVariant: ", iVehicleVariant)
	
	BEGIN_SCALEFORM_MOVIE_METHOD(siMovie, "SET_VEHICLE_TYPE")
		
		IF iVehicleVariant = 0
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("FMMC_VEH_T")
		ELSE
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("FMMC_VEH_TV")
				ADD_TEXT_COMPONENT_INTEGER(iVehicleVariant)
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ENDIF
		
	END_SCALEFORM_MOVIE_METHOD()

ENDPROC

PROC CORONA_UPDATE_VEHICLE_COLOUR_NAME(SCALEFORM_INDEX &siMovie, STRING strColourName, STRING strColourType, INT iOptionalInt = -1)

	PRINTLN("MP Scaleform: CORONA_UPDATE_VEHICLE_COLOUR_NAME - strColourName: ", strColourName)
	
	BEGIN_SCALEFORM_MOVIE_METHOD(siMovie, "SET_COLOUR")
		IF iOptionalInt != -1
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING(strColourName)
				ADD_TEXT_COMPONENT_INTEGER(iOptionalInt)
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(strColourName)
		ENDIF
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(siMovie, "SET_COLOUR_TYPE")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(strColourType)
	END_SCALEFORM_MOVIE_METHOD()

ENDPROC

PROC CORONA_SHOW_QUESTION_MARK(SCALEFORM_INDEX &siMovie, BOOL bDisplay)

	PRINTLN("MP Scaleform: CORONA_SHOW_QUESTION_MARK - ", bDisplay)
	
	BEGIN_SCALEFORM_MOVIE_METHOD(siMovie, "SHOW_QUESTION_MARK")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bDisplay)
	END_SCALEFORM_MOVIE_METHOD()
	
ENDPROC

PROC CORONA_SHOW_BOTTOM_SELECTOR(SCALEFORM_INDEX &siMovie, BOOL bDisplay)
	
	PRINTLN("MP Scaleform: CORONA_SHOW_BOTTOM_SELECTOR - ", bDisplay)
	
	BEGIN_SCALEFORM_MOVIE_METHOD(siMovie, "SHOW_BOTTOM_SELECTOR")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bDisplay)
	END_SCALEFORM_MOVIE_METHOD()
	
ENDPROC


/// PURPOSE: Display the betting selector in the corona
PROC CORONA_DISPLAY_BETTING_SELECTOR(SCALEFORM_INDEX &siMovie, MULTIPLAYER_SETTING_BETTING_ODDS oddsFormat, INT iCashOnPlayer, INT iOddN, INT iOddD, INT iCurrentBet)
	
	PRINTLN("MP Scaleform: CORONA_DISPLAY_BETTING_SELECTOR")
	
	BEGIN_SCALEFORM_MOVIE_METHOD(siMovie, "SHOW_BET_SELECTOR")
	
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("MP_BET_CASH")
			ADD_TEXT_COMPONENT_INTEGER(iCashOnPlayer)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		CORONA_ADD_SCALEFORM_ODD_STRING(oddsFormat, iOddN, iOddD)
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("MP_BET_CASH")
			ADD_TEXT_COMPONENT_INTEGER(iCurrentBet)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
	END_SCALEFORM_MOVIE_METHOD()
	
ENDPROC

/// PURPOSE: Update the displayed total bets on a player
PROC CORONA_UPDATE_BETS_ON_PLAYER(SCALEFORM_INDEX &siMovie, INT iCashOnPlayer)

	PRINTLN("MP Scaleform: CORONA_UPDATE_BETS_ON_PLAYER")

	// Set total bets on selected player
	BEGIN_SCALEFORM_MOVIE_METHOD(siMovie, "SET_BETS_ON_PLAYER")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("MP_BET_CASH")
			ADD_TEXT_COMPONENT_INTEGER(iCashOnPlayer)
		END_TEXT_COMMAND_SCALEFORM_STRING()
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

/// PURPOSE: Update the displayed odds on betting screen    
PROC CORONA_UPDATE_ODDS_ON_PLAYER(SCALEFORM_INDEX &siMovie, MULTIPLAYER_SETTING_BETTING_ODDS oddsFormat, INT iOddN, INT iOddD)
	
	PRINTLN("MP Scaleform: CORONA_UPDATE_ODDS_ON_PLAYER")
	
	// Set odds on current player
	BEGIN_SCALEFORM_MOVIE_METHOD(siMovie, "SET_ODDS")
		CORONA_ADD_SCALEFORM_ODD_STRING(oddsFormat, iOddN, iOddD)
	END_SCALEFORM_MOVIE_METHOD()
		
ENDPROC

/// PURPOSE: Sets the current bet that will be placed
PROC CORONA_UPDATE_CURRENT_BET(SCALEFORM_INDEX &siMovie, INT iCurrentBet)
	
	PRINTLN("MP Scaleform: CORONA_UPDATE_CURRENT_BET")
	
	// Set odds on current player
	BEGIN_SCALEFORM_MOVIE_METHOD(siMovie, "SET_MY_BET")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("MP_BET_CASH")
			ADD_TEXT_COMPONENT_INTEGER(iCurrentBet)
		END_TEXT_COMMAND_SCALEFORM_STRING()
	END_SCALEFORM_MOVIE_METHOD()
		
ENDPROC


PROC CORONA_DISPLAY_CENTRAL_DATA(SCALEFORM_INDEX &siMovie, BOOL bDisplay)
	
	PRINTLN("MP Scaleform: CORONA_DISPLAY_CENTRAL_DATA - ", bDisplay)
	
	BEGIN_SCALEFORM_MOVIE_METHOD(siMovie, "SHOW_CENTER_CONTENT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bDisplay)
	END_SCALEFORM_MOVIE_METHOD()
	
ENDPROC


PROC CORONA_DISPLAY_LEFT_ARROW(SCALEFORM_INDEX &siMovie, BOOL bDisplay)
	
	PRINTLN("MP Scaleform: CORONA_DISPLAY_LEFT_ARROW - ", bDisplay)
	
	BEGIN_SCALEFORM_MOVIE_METHOD(siMovie, "SHOW_LEFT_ARROW")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bDisplay)
	END_SCALEFORM_MOVIE_METHOD()
	
ENDPROC

PROC CORONA_DISPLAY_RIGHT_ARROW(SCALEFORM_INDEX &siMovie, BOOL bDisplay)
	
	PRINTLN("MP Scaleform: CORONA_DISPLAY_RIGHT_ARROW - ", bDisplay)
	
	BEGIN_SCALEFORM_MOVIE_METHOD(siMovie, "SHOW_RIGHT_ARROW")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bDisplay)
	END_SCALEFORM_MOVIE_METHOD()
	
ENDPROC





FUNC BOOL HAS_HUD_CONTROL_BEEN_PRESSED_SCALEFORM(CONTROL_ACTION anAction)

	
	INT Index = ENUM_TO_INT(anAction)
	INT Bitset =  GET_WEAPON_BITSET(Index) 
	INT BitsetIndex = GET_WEAPON_INDEX_BITSET(Index)
	
	IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, anAction)
	OR IS_ANALOGUE_STICK_MOVED(anAction, MPGlobalsHud.iDelayInt, TRUE)
		
		IF NOT IS_BIT_SET(MPGlobalsHud.iHudButtonPressed[Bitset], BitsetIndex)
			SET_BIT(MPGlobalsHud.iHudButtonPressed[Bitset], BitsetIndex)
			RETURN TRUE
		ENDIF
	ELSE
		IF IS_BIT_SET(MPGlobalsHud.iHudButtonPressed[Bitset], BitsetIndex)
			CLEAR_BIT(MPGlobalsHud.iHudButtonPressed[Bitset], BitsetIndex)
		ENDIF
	ENDIF
	
	
	
	
	RETURN FALSE
ENDFUNC


FUNC BOOL IS_JOINING_GAMETIPS_FEED_DISPLAYING()
	RETURN g_B_IsOnlineJoiningFeedDisplaying
ENDFUNC

PROC SET_JOINING_GAMETIPS_FEED_DISPLAYING(BOOL bActive)

	IF bActive 
	
		IF g_B_IsOnlineJoiningFeedDisplaying = FALSE
			#IF IS_DEBUG_BUILD
			DEBUG_PRINTCALLSTACK()
			NET_NL()NET_PRINT("THEFEED_AUTO_POST_GAMETIPS_ON - called")
			#ENDIF
			SHOW_GAME_TOOL_TIPS(TRUE)
			g_B_IsOnlineJoiningFeedDisplaying = TRUE
			
		ENDIF
	ELSE
		
		IF g_B_IsOnlineJoiningFeedDisplaying = TRUE
			#IF IS_DEBUG_BUILD
			DEBUG_PRINTCALLSTACK()
			NET_NL()NET_PRINT("THEFEED_AUTO_POST_GAMETIPS_OFF - called")
			#ENDIF
			SHOW_GAME_TOOL_TIPS(FALSE)
			THEFEED_FLUSH_QUEUE()
			 g_b_ReapplyStickySaveFailedFeed = FALSE
			g_B_IsOnlineJoiningFeedDisplaying = FALSE
		
		ENDIF
	
	ENDIF
	

ENDPROC

PROC FORCE_GAMETIPS_FEED_OFF()
	g_B_HideOnlineJoiningFeed = TRUE
	SET_JOINING_GAMETIPS_FEED_DISPLAYING(FALSE)
ENDPROC
PROC RESET_FORCE_GAMETIP_FEED_OFF()
	g_B_HideOnlineJoiningFeed = FALSE
ENDPROC


/// PURPOSE:
///    Returns correct script input for accept, including auto-swap for X/O on japanese builds.
/// RETURNS:
///    True if the correct script cancel button has been pressed.    
FUNC CONTROL_ACTION GET_SCRIPT_ACCEPT_INPUT()

	#IF IS_JAPANESE_BUILD
		IF IS_PLAYSTATION_PLATFORM()
			RETURN INPUT_SCRIPT_RRIGHT
		ENDIF
	#ENDIF

	RETURN INPUT_SCRIPT_RDOWN

ENDFUNC

/// PURPOSE:
///    Returns correct script input for cancel, including auto-swap for X/O on japanese builds.
/// RETURNS:
///    Script input for cancel
FUNC CONTROL_ACTION GET_SCRIPT_CANCEL_INPUT()

	#IF IS_JAPANESE_BUILD
		IF IS_PLAYSTATION_PLATFORM()
			RETURN INPUT_SCRIPT_RDOWN
		ENDIF
	#ENDIF

	RETURN INPUT_SCRIPT_RRIGHT

ENDFUNC


//PROC REMOVE_JOINING_SPLASH()
//
//	g_bSplashScreenBigMethodCalledJoining = FALSE
//
//	IF g_IsNewsFeedDisplaying = TRUE
//		SC_TRANSITION_NEWS_END()
//		g_IsNewsFeedDisplaying = FALSE
//	ENDIF	
//
//	IF HAS_SCALEFORM_MOVIE_LOADED(g_SplashScreenMovie) 
//		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(g_SplashScreenMovie)
//	ENDIF
//	
//	SET_JOINING_GAMETIPS_FEED_DISPLAYING(FALSE)
//	
//	THEFEED_FLUSH_QUEUE()
//	THEFEED_REPORT_LOGO_OFF()
//	
//ENDPROC



/// PURPOSE:
///    
/// PARAMS:
///    iTime -  -1 cleans up, -2 is always on
/// RETURNS:
///    
//FUNC BOOL RUN_SPLASH_START_ONLINE(BOOL bDisplay)
//	
//
//	#IF IS_DEBUG_BUILD
//	IF GET_COMMANDLINE_PARAM_EXISTS("sc_noMPIntroSplash")
//		REMOVE_JOINING_SPLASH()
//		RETURN TRUE
//	ENDIF
//	
//	IF NETWORK_IS_IN_CODE_DEVELOPMENT_MODE()
//		REMOVE_JOINING_SPLASH()
//		RETURN TRUE
//	ENDIF
//	
//	#ENDIF
//	
//	
//	IF g_B_HideOnlineJoiningFeed
//	
//		SET_JOINING_GAMETIPS_FEED_DISPLAYING(FALSE)
//		g_B_HideOnlineJoiningFeed = FALSE
//	ENDIF
//	
//	
//
//	IF g_B_DisplayOnlineSplash = FALSE 
//				
//		REMOVE_JOINING_SPLASH()
//		g_bSplashScreenBigMethodCalledJoining = FALSE
//		RETURN TRUE
//	ENDIF
//	
//	IF LOBBY_AUTO_MULTIPLAYER_FREEMODE()
//		REMOVE_JOINING_SPLASH()
//		RETURN TRUE
//	ENDIF
//	
//	IF IS_SCREEN_FADED_OUT()
//		REMOVE_JOINING_SPLASH()
//		RETURN TRUE
//	ENDIF
//	
//	IF HAS_IMPORTANT_STATS_LOADED() AND ALREADY_CREATED_GANG_MEMBER_IN_SLOT_TRANSITION() = FALSE
//		REMOVE_JOINING_SPLASH()
//		RETURN TRUE
//	ENDIF
//	
//	
//	
//	
//	IF IS_PAUSE_MENU_ACTIVE()
//	AND (GET_CURRENT_HUD_STATE() = HUD_STATE_SELECT_CHARACTER_FM 
//	OR GET_CURRENT_HUD_STATE() = HUD_STATE_CREATE_FACE
//	OR GET_CURRENT_HUD_STATE() = HUD_STATE_DELETE_CHARACTER_PROMPT)
//	
//		REMOVE_JOINING_SPLASH()
//		RETURN TRUE
//	ENDIF
//	
//	
//	IF bDisplay = true
//	
//		IF g_IsNewsFeedDisplaying
//			THEFEED_HIDE_THIS_FRAME()
//		ENDIF
//	
//		IF NOT HAS_SCALEFORM_MOVIE_LOADED(g_SplashScreenMovie) 
//						
//			g_SplashScreenMovie = REQUEST_SCALEFORM_MOVIE("GTAV_ONLINE")
//			THEFEED_REPORT_LOGO_ON()
//			
//			FORCE_SCRIPTED_GFX_WHEN_FRONTEND_ACTIVE(TRUE)
//			SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
//			
//		ELSE
//		
//		
//			THEFEED_REPORT_LOGO_ON()
//			FORCE_SCRIPTED_GFX_WHEN_FRONTEND_ACTIVE(TRUE)
//			SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
//			
//			IF GET_CURRENT_TRANSITION_STATE() != TRANSITION_STATE_TESTBED_HOW_TO_TERMINATE
//			OR GET_CURRENT_TRANSITION_STATE() != TRANSITION_STATE_WAITING_FOR_EXTERNAL_TERMINATION_CALL
//			OR GET_CURRENT_TRANSITION_STATE() != TRANSITION_STATE_TERMINATE_MAINTRANSITION
//				IF IS_SKYSWOOP_IN_SKY()
//					IF g_IsNewsFeedDisplaying = FALSE
//						SET_JOINING_GAMETIPS_FEED_DISPLAYING(TRUE)
//						
//					ENDIF
//				ENDIF
//			ENDIF
//		
////				REQUEST_STREAMED_TEXTURE_DICT("GTAV_ONLINE")
////				IF HAS_STREAMED_TEXTURE_DICT_LOADED("GTAV_ONLINE")
////					SPRITE_PLACEMENT GTAOnlinePlacement 
////					GTAOnlinePlacement.x = 0.5
////					GTAOnlinePlacement.y = 0.5
////					GTAOnlinePlacement.w = 1
////					GTAOnlinePlacement.h = 1
////					GTAOnlinePlacement.r = 255
////					GTAOnlinePlacement.g = 255
////					GTAOnlinePlacement.b = 255
////					GTAOnlinePlacement.a = 255
////						
////				
////					DRAW_2D_SPRITE("GTAV_ONLINE", "gtaonline",GTAOnlinePlacement )
////				ENDIF
//			DRAW_SCALEFORM_MOVIE_FULLSCREEN(g_SplashScreenMovie, 255, 255, 255, 255)
//			
//
//			IF g_bSplashScreenBigMethodCalledJoining = FALSE
//			
//				IF IS_ACCOUNT_OVER_17() 
//					IF IS_SKYSWOOP_IN_SKY()
//						IF SC_TRANSITION_NEWS_SHOW(g_SplashScreenMovie)
//							SET_JOINING_GAMETIPS_FEED_DISPLAYING(FALSE)
//							THEFEED_FLUSH_QUEUE()
////							SC_TRANSITION_NEWS_SHOW_NEXT_ITEM()
//							g_IsNewsFeedDisplaying = TRUE
//
//				
//		//					BEGIN_SCALEFORM_MOVIE_METHOD(g_SplashScreenMovie, "SET_BIG_LOGO_VISIBLE")
//		//						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
//		//					END_SCALEFORM_MOVIE_METHOD()
//							g_bSplashScreenBigMethodCalledJoining = TRUE
//						ENDIF
//					ENDIF
//				ENDIF
//				
//			ENDIF
//			
//			IF g_bSplashScreenBigMethodCalledJoining
//				IF HAS_NET_TIMER_EXPIRED_ONE_FRAME(g_aSplashScreenTimer, 15000)
//					SC_TRANSITION_NEWS_SHOW_NEXT_ITEM()
//				ENDIF
//			ENDIF
//
//			RETURN TRUE
//		ENDIF
//	ELSE
//	
//		REMOVE_JOINING_SPLASH()
//		RETURN TRUE
//	ENDIF
//	
//	
//	
//	
//	RETURN FALSE
//	
//ENDFUNC




///// PURPOSE:
/////    Sets up a column to be shown in the PauseMenu Scaleform
/////    After all the data for a given column is set up, call PM_DISPLAY_DATA_SLOT!
///// PARAMS:
/////    iColumn - 	Which column, probably 0,1,2
/////    iMenuIndex - Which item in the list, probably 0-9
/////    iMenuId - 	Which menu this is considered
/////    iUniqueId - 	Identifier for this menuitem (can be anything, really)
/////    bActive - 	Appears grayed out or not
/////    pszLabel - 	Stringtable identifier to display.
//PROC MPLOBBY_SET_DATA_SLOT_PLAYERLIST(INT iColumn, INT iMenuIndex, INT iMenuId, INT iUniqueId, BOOL bActive, STRING pszLabel,INT iRank, HUD_COLOURS BarColour, BOOL isHost, MP_ICONS_ENUMS GameType, MP_ICONS_ENUMS GtaOrWorld, MP_ICONS_ENUMS RankIcon)
//	IF BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND("SET_DATA_SLOT")
//		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iColumn) // column index
//		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMenuIndex) // position in the array
//		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMenuId) // FRONTEND_MENU_SCREEN
//		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iUniqueId) // unique id that Jeff will tell me current location. When playerlist is moved this changes so I can update playercard data.
//		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0) // Scaleform type, leave as 0
//		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iRank) // Rank number
//		IF bActive
//			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1) // active or not Greys out
//		ELSE
//			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0) // active or not Greys out
//		ENDIF
//		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(pszLabel) // text label
//		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(BarColour)) //BarColour
//		IF isHost
//			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
//		ELSE
//			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
//		ENDIF
//		
//		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(GameType)) //Whether the friend is in DM, GTARACE, CNC
//		
//		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(GtaOrWorld)) //Whether you're playerin GTA or not. 
//  		
//		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(RankIcon)) //Whether you're playerin GTA or not. 
//
//		
//		END_SCALEFORM_MOVIE_METHOD()
//	ENDIF
//ENDPROC
//
//
///// PURPOSE:
/////    Sets up a column to be shown in the PauseMenu Scaleform
/////    After all the data for a given column is set up, call PM_DISPLAY_DATA_SLOT!
///// PARAMS:
/////    iColumn - 	Which column, probably 0,1,2
/////    iMenuIndex - Which item in the list, probably 0-9
/////    iMenuId - 	Which menu this is considered
/////    iUniqueId - 	Identifier for this menuitem (can be anything, really)
/////    bActive - 	Appears grayed out or not
/////    pszLabel - 	Stringtable identifier to display.
//PROC MPLOBBY_UPDATE_DATA_SLOT_PLAYERLIST(INT iColumn, INT iMenuIndex, INT iMenuId, INT iUniqueId, BOOL bActive, STRING pszLabel,INT iRank, HUD_COLOURS BarColour, BOOL isHost, MP_ICONS_ENUMS GameType, MP_ICONS_ENUMS GtaOrWorld, MP_ICONS_ENUMS RankIcon)
//	IF BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND("UPDATE_DATA_SLOT")
//		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iColumn) // column index
//		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMenuIndex) // position in the array
//		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMenuId) // FRONTEND_MENU_SCREEN
//		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iUniqueId) // unique id that Jeff will tell me current location. When playerlist is moved this changes so I can update playercard data.
//		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0) // Scaleform type, leave as 0
//		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iRank) // Rank number
//		IF bActive
//			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1) // active or not Greys out
//		ELSE
//			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0) // active or not Greys out
//		ENDIF
//		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(pszLabel) // text label
//				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(BarColour)) //BarColour
//		IF isHost
//			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
//		ELSE
//			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
//		ENDIF
//		
//		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(GameType)) //Whether the friend is in DM, GTARACE, CNC
//		
//		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(GtaOrWorld)) //Whether you're playerin GTA or not. 
//  		
//		
//		END_SCALEFORM_MOVIE_METHOD()
//	ENDIF
//ENDPROC



//
///// PURPOSE:
/////    Setups the column for the players rank details.
///// PARAMS:
/////    iColumn - Which column, probably 0,1,2
/////    Gamertag - Players Gamertag
/////    iTeam - Players Team
/////    Rank - Players Rank
//PROC MPLOBBY_SET_COLUMN_TITLE(INT iColumn, STRING Gamertag, STRING iTeam, HUD_COLOURS Colour )
//	IF BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND("SET_COLUMN_TITLE")
//		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iColumn) // depth
//		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(Gamertag) // text label
//		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(GET_TEAM_NAME_KEY(iTeam))
//		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(Colour)) // depth	
//		
//	
//		END_SCALEFORM_MOVIE_METHOD()
//	ENDIF
//ENDPROC
//
//
//PROC MPLOBBY_SET_DESCRIPTION(INT iColumn, STRING ReliabilityLevelString, INT ReliabilityLevel, FLOAT LowerThreshold, FLOAT UpperThreashold)
//	IF BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND("SET_COLUMN_TITLE")
//		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iColumn) // depth
//		BEGIN_TEXT_COMMAND_SCALEFORM_STRING(ReliabilityLevelString)
//			ADD_TEXT_COMPONENT_INTEGER(ReliabilityLevel)
//		END_TEXT_COMMAND_SCALEFORM_STRING()
//		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(LowerThreshold) // Where the 2 strokes are on the reliability Bar
//		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(UpperThreashold) // Where the 2 strokes are on the reliability Bar
//		
//	END_SCALEFORM_MOVIE_METHOD()
//	ENDIF

//ENDPROC

//
//
//PROC MPLOBBY_UPDATE_DATA_SLOT_PLAYERCARD_CNC(INT iColumn, INT iMenuIndex, INT iMenuId, INT iUniqueId, BOOL bActive, STRING pszLabel,INT iRank, HUD_COLOURS BarColour, BOOL isHost, MP_ICONS_ENUMS GameType, MP_ICONS_ENUMS GtaOrWorld, MP_ICONS_ENUMS RankIcon)
//	IF BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND("UPDATE_DATA_SLOT")
//		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iColumn) // column index
//		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMenuIndex) // position in the array
//		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMenuId) // FRONTEND_MENU_SCREEN
//		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iUniqueId) // unique id that Jeff will tell me current location. When playerlist is moved this changes so I can update playercard data.
//		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0) // Scaleform type, leave as 0
//		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iRank) // Rank number
//		IF bActive
//			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1) // active or not Greys out
//		ELSE
//			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0) // active or not Greys out
//		ENDIF
//		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(pszLabel) // text label
//				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(BarColour)) //BarColour
//		IF isHost
//			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
//		ELSE
//			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
//		ENDIF
//		
//		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(GameType)) //Whether the friend is in DM, GTARACE, CNC
//		
//		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(GtaOrWorld)) //Whether you're playerin GTA or not. 
//  		
//		
//		END_SCALEFORM_MOVIE_METHOD()
//	ENDIF
//ENDPROC







