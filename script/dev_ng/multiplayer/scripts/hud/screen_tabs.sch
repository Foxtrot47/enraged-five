//USING "screens_header.sch"
USING "net_events.sch"
USING "script_network.sch"
USING "Screen_placements.sch"


CONST_INT MIN_TAB_STRING_LENGTH 4

// ==== PARAMETERS TO SET ====
//INT iNumberOfTabs = 4 // default value


// only set these if necessry, otherwise just use the default values

//FLOAT fTabsCentreX = 0.499
//FLOAT fTabsCentreY = 0.206
//FLOAT fTabsTotalWidth = 0.598
//FLOAT fTabsTotalHeight = 0.217
//FLOAT fTabIndentation = 0.00
//FLOAT fTabSeperation = 0.004
//FLOAT fTabHeight = 0.018
//FLOAT fTabGangHeight = 0.007
////FLOAT fTabBorderWidth = 0.004
//FLOAT fTabTitleIndentFromTop = 0.001
//FLOAT fTabTitleIndentFromTopSelected = -0.003
//INT iTabAlpha = 220 // 110
//FLOAT TabDivingValue = 4.940
//INT LockedTabsBitState


CONST_INT LOCKED_TAB_ONE 0
CONST_INT LOCKED_TAB_TWO 1
CONST_INT LOCKED_TAB_THREE 2 
CONST_INT LOCKED_TAB_FOUR 3
CONST_INT LOCKED_TAB_FIVE 4



// ==== PARAMETERS TO CHECK ====
//INT iDefaultTab


// =============================


//RECT TabTopRect
////RECT TabCentreRect
//RECT TabRightRect
//RECT TabLeftRect
//RECT TabBottomRect 

//TEXT_PLACEMENT TabRB
//TEXT_PLACEMENT TabLB


PROC LOCK_ALL_TABS(TAB_PLACEMENT_TOOLS& Placement)
	SET_BIT(Placement.LockedTabsBitState, LOCKED_TAB_ONE)
	SET_BIT(Placement.LockedTabsBitState, LOCKED_TAB_TWO)
	SET_BIT(Placement.LockedTabsBitState, LOCKED_TAB_THREE)
	SET_BIT(Placement.LockedTabsBitState, LOCKED_TAB_FOUR)
	SET_BIT(Placement.LockedTabsBitState, LOCKED_TAB_FIVE)
ENDPROC

PROC UNLOCK_ALL_TABS(TAB_PLACEMENT_TOOLS& Placement)
	CLEAR_BIT(Placement.LockedTabsBitState, LOCKED_TAB_ONE)
	CLEAR_BIT(Placement.LockedTabsBitState, LOCKED_TAB_TWO)
	CLEAR_BIT(Placement.LockedTabsBitState, LOCKED_TAB_THREE)
	CLEAR_BIT(Placement.LockedTabsBitState, LOCKED_TAB_FOUR)
	CLEAR_BIT(Placement.LockedTabsBitState, LOCKED_TAB_FIVE)
ENDPROC

PROC UNLOCK_SELECTED_TAB(TAB_PLACEMENT_TOOLS& Placement)
	
	SWITCH Placement.iSelectedTab
		CASE 0 	
			CLEAR_BIT(Placement.LockedTabsBitState, LOCKED_TAB_ONE)
		BREAK
		CASE 1 	
			CLEAR_BIT(Placement.LockedTabsBitState, LOCKED_TAB_TWO)
		BREAK
		CASE 2 	
			CLEAR_BIT(Placement.LockedTabsBitState, LOCKED_TAB_THREE)
		BREAK
		CASE 3 	
			CLEAR_BIT(Placement.LockedTabsBitState, LOCKED_TAB_FOUR)
		BREAK
		CASE 4 	
			CLEAR_BIT(Placement.LockedTabsBitState, LOCKED_TAB_FIVE)
		BREAK
	ENDSWITCH
ENDPROC

PROC UNLOCK_A_TAB(INT WhichTab, TAB_PLACEMENT_TOOLS& Placement)
	
	SWITCH WhichTab
		CASE 0 	
			CLEAR_BIT(Placement.LockedTabsBitState, LOCKED_TAB_ONE)
		BREAK
		CASE 1 	
			CLEAR_BIT(Placement.LockedTabsBitState, LOCKED_TAB_TWO)
		BREAK
		CASE 2 	
			CLEAR_BIT(Placement.LockedTabsBitState, LOCKED_TAB_THREE)
		BREAK
		CASE 3 	
			CLEAR_BIT(Placement.LockedTabsBitState, LOCKED_TAB_FOUR)
		BREAK
		CASE 4 	
			CLEAR_BIT(Placement.LockedTabsBitState, LOCKED_TAB_FIVE)
		BREAK
	ENDSWITCH
ENDPROC

PROC LOCK_A_TAB(INT WhichTab, TAB_PLACEMENT_TOOLS& Placement)
	
	SWITCH WhichTab
		CASE 0 	
			SET_BIT(Placement.LockedTabsBitState, LOCKED_TAB_ONE)
		BREAK
		CASE 1 	
			SET_BIT(Placement.LockedTabsBitState, LOCKED_TAB_TWO)
		BREAK
		CASE 2 	
			SET_BIT(Placement.LockedTabsBitState, LOCKED_TAB_THREE)
		BREAK
		CASE 3 	
			SET_BIT(Placement.LockedTabsBitState, LOCKED_TAB_FOUR)
		BREAK
		CASE 4 	
			SET_BIT(Placement.LockedTabsBitState, LOCKED_TAB_FIVE)
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL IS_TAB_LOCKED(INT WhichTab, TAB_PLACEMENT_TOOLS& Placement)	
	SWITCH WhichTab
		CASE 0 	
			IF IS_BIT_SET(Placement.LockedTabsBitState, LOCKED_TAB_ONE)
				RETURN TRUE
			ENDIF
		BREAK
		CASE 1 	
			IF IS_BIT_SET(Placement.LockedTabsBitState, LOCKED_TAB_TWO)
				RETURN TRUE
			ENDIF
		BREAK
		CASE 2 	
			IF IS_BIT_SET(Placement.LockedTabsBitState, LOCKED_TAB_THREE)
				RETURN TRUE
			ENDIF
		BREAK
		CASE 3 	
			IF IS_BIT_SET(Placement.LockedTabsBitState, LOCKED_TAB_FOUR)
				RETURN TRUE
			ENDIF
		BREAK
		CASE 4 	
			IF IS_BIT_SET(Placement.LockedTabsBitState, LOCKED_TAB_FIVE)
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

PROC SET_NEXT_SCREEN_TAB(INT iNextScreenTab, TAB_PLACEMENT_TOOLS& Placement)//, BOOL bSetAsDefaultTab = FALSE)
//	IF (bSetAsDefaultTab)
//		iDefaultTab = iSelectedTab
//	ENDIF
	Placement.iSelectedTab = iNextScreenTab
ENDPROC

FUNC INT GET_LENGTH_OF_TAB_STRING(STRING inString)
	INT iReturn
	IF NOT IS_STRING_NULL(inString)
		iReturn = GET_LENGTH_OF_LITERAL_STRING(inString)
	ENDIF
	RETURN(iReturn + MIN_TAB_STRING_LENGTH)
ENDFUNC

FUNC INT GET_TOTAL_TABS_STRING_LENGTH(TAB_PLACEMENT_TOOLS& Placement)
	INT i
	INT iTotal
	REPEAT MAX_NUMBER_OF_TABS i
		IF (i < Placement.iNumberOfTabs)
			iTotal += GET_LENGTH_OF_TAB_STRING(Placement.TabString[i])
		ENDIF
	ENDREPEAT
	RETURN(iTotal)
ENDFUNC

FUNC FLOAT GET_TAB_CUMULATIVE_WIDTH(INT iThisTab, TAB_PLACEMENT_TOOLS& Placement)
	INT i
	FLOAT fReturn
	REPEAT MAX_NUMBER_OF_TABS i
		IF (i < iThisTab)
			fReturn += 	Placement.Tab[i].w
		ENDIF
	ENDREPEAT
	RETURN(fReturn)
ENDFUNC

PROC CALCULATE_TAB_SIZES(TAB_PLACEMENT_TOOLS& Placement, TAB_FIXEDVALUES& Values)
	INT i

	SET_STANDARD_TAB_DETAILS(Placement.TabStyles)
	
	IF NOT (Values.bTabYFixed)
		Values.fTabsCentreY = FIX_Y(Values.fTabsCentreY)
		Values.fTabsTotalHeight = FIX_Y(Values.fTabsTotalHeight)
		Values.fTabHeight = FIX_Y(Values.fTabHeight)
	 	Values.fTabTitleIndentFromTop = FIX_Y(Values.fTabTitleIndentFromTop)
		Values.bTabYFixed = TRUE
	ENDIF

	// populate the tab rect data
	REPEAT MAX_NUMBER_OF_TABS i
		IF (i < Placement.iNumberOfTabs)
			
			// calculate width
			IF NOT (Values.bUseEvenSpacing)
				Placement.Tab[i].w = (Values.fTabsTotalWidth - ((2.0 * Values.fTabIndentation) + (Values.fTabSeperation * (Placement.iNumberOfTabs-1)))) * (TO_FLOAT(GET_LENGTH_OF_TAB_STRING(Placement.TabString[i])) / TO_FLOAT(GET_TOTAL_TABS_STRING_LENGTH(Placement)))
			ELSE
				Placement.Tab[i].w = (Values.fTabsTotalWidth - ((2.0 * Values.fTabIndentation) + (Values.fTabSeperation * (Placement.iNumberOfTabs-1)))) * (1.0/Placement.iNumberOfTabs)
			ENDIF
			
			// height
			Placement.Tab[i].h = Values.fTabHeight
			
			// X
			Placement.Tab[i].x = Values.fTabsCentreX - (0.5 * Values.fTabsTotalWidth)
			Placement.Tab[i].x += Values.fTabIndentation
			Placement.Tab[i].x += (i * Values.fTabSeperation)
			Placement.Tab[i].x += (Placement.Tab[i].w * 0.5) + GET_TAB_CUMULATIVE_WIDTH(i, Placement)
			
			// Y
			Placement.Tab[i].y = Values.fTabsCentreY - (0.5 * Values.fTabsTotalHeight)			
			Placement.Tab[i].y += (Placement.Tab[i].h * 0.5)
			
			// default colour
			IF ((Placement.Tab[i].r + Placement.Tab[i].g + Placement.Tab[i].b + Placement.Tab[i].a) = 0)
				Placement.Tab[i].r = 255
				Placement.Tab[i].g = 255
				Placement.Tab[i].b = 255
				Placement.Tab[i].a = Values.iTabAlpha
			ENDIF
						 
			#IF IS_DEBUG_BUILD
				//Tab[i].strName = TabString[i]
			#endif
			
			
			Values.bUseEvenSpacing = TRUE //Keep all tabs the same size
			// calculate width
			IF NOT (Values.bUseEvenSpacing)
				Placement.TabGangColour[i].w = (Values.fTabsTotalWidth - ((2.0 * Values.fTabIndentation) + (Values.fTabSeperation * (Placement.iNumberOfTabs-1)))) * (TO_FLOAT(GET_LENGTH_OF_TAB_STRING(Placement.TabString[i])) / TO_FLOAT(GET_TOTAL_TABS_STRING_LENGTH(Placement)))
			ELSE
				Placement.TabGangColour[i].w = (Values.fTabsTotalWidth - ((2.0 * Values.fTabIndentation) + (Values.fTabSeperation * (Placement.iNumberOfTabs-1)))) * (1.0/Placement.iNumberOfTabs)
			ENDIF
			
			// height
			Placement.TabGangColour[i].h = Values.fTabGangHeight
			
			// X
			Placement.TabGangColour[i].x = Values.fTabsCentreX - (0.5 * Values.fTabsTotalWidth)
			Placement.TabGangColour[i].x += Values.fTabIndentation
			Placement.TabGangColour[i].x += (i * Values.fTabSeperation)
			Placement.TabGangColour[i].x += (Placement.TabGangColour[i].w * 0.5) + GET_TAB_CUMULATIVE_WIDTH(i, Placement)
			
			// Y
			Placement.TabGangColour[i].y = Values.fTabsCentreY - (0.5 * Values.fTabsTotalHeight)
			Placement.TabGangColour[i].y += (Placement.TabGangColour[i].h * 0.5)
			Placement.TabGangColour[i].y -= Values.fTabHeight/Values.TabDivingValue
			
			//a
			Placement.TabGangColour[i].a = Values.iTabAlpha
			
			// default colour
			IF ((Placement.TabGangColour[i].r + Placement.TabGangColour[i].g + Placement.TabGangColour[i].b + Placement.TabGangColour[i].a) = 0)
				Placement.TabGangColour[i].r = 255
				Placement.TabGangColour[i].g = 255
				Placement.TabGangColour[i].b = 255
				Placement.TabGangColour[i].a = Values.iTabAlpha
			ENDIF
						 
			#IF IS_DEBUG_BUILD
//				TabGangColour[i].strName = TabGangColourString[i]
			#endif
			
			
		ENDIF
	ENDREPEAT	

	
	REPEAT MAX_NUMBER_OF_TABS i
		Placement.TabTitle[i].x = Placement.Tab[i].x
		Placement.TabTitle[i].y = (Placement.Tab[i].y - (0.5 * Placement.Tab[i].h)) + Values.fTabTitleIndentFromTop
		#IF IS_DEBUG_BUILD
			//TabTitle[i].strName = TabString[i]
		#endif
	ENDREPEAT


	
ENDPROC


PROC INIT_TABS(TAB_PLACEMENT_TOOLS& Placement)
	

	CALCULATE_TAB_SIZES(Placement, Placement.FixedValues)
	LOCK_ALL_TABS(Placement)
	UNLOCK_SELECTED_TAB(Placement)

ENDPROC


//
//PROC RENDER_TABS(TAB_PLACEMENT_TOOLS& Placement, BOOL DisplayLockIcons = TRUE )
//
//	CALCULATE_TAB_SIZES(Placement, Placement.FixedValues) // remove this when got sizes
//
//	RECT TabToDraw
//	INT i
//	
//	// draw tabs
//	REPEAT MAX_NUMBER_OF_TABS i
//		IF (i < Placement.iNumberOfTabs)
//			
//			TabToDraw = Placement.Tab[i]
//			
//			// set colour of highlighted tab
//			IF NOT (Placement.iSelectedTab = i)
//				// colour
//				IF (Placement.FixedValues.bDullNonSelectedTabs)
////					TabToDraw.r = ROUND(TO_FLOAT(Tab[i].r)/2.0)
////					TabToDraw.g = ROUND(TO_FLOAT(Tab[i].g)/2.0)
////					TabToDraw.b = ROUND(TO_FLOAT(Tab[i].b)/2.0)	
//					TabToDraw.r = 0
//					TabToDraw.g = 0
//					TabToDraw.b = 0
//					TabToDraw.a = 150
//				ENDIF			
//			ENDIF					
//			TabToDraw.a = Placement.FixedValues.iTabAlpha // alpha is always the same	
//			DRAW_RECTANGLE(TabToDraw)
//			
//			IF (Placement.iSelectedTab = i)
//				Placement.TabTitle[i].y = (Placement.Tab[i].y - (0.5 * Placement.Tab[i].h)) + Placement.FixedValues.fTabTitleIndentFromTopSelected
//				DRAW_TEXT_WITH_ALIGNMENT(Placement.TabTitle[i], Placement.TabStyles.TS_TabSelected, Placement.TabString[i], TRUE, FALSE)
//
//			ELSE
//				Placement.TabTitle[i].y = (Placement.Tab[i].y - (0.5 * Placement.Tab[i].h)) + Placement.FixedValues.fTabTitleIndentFromTop
//				DRAW_TEXT_WITH_ALIGNMENT(Placement.TabTitle[i], Placement.TabStyles.TS_Tab, Placement.TabString[i], TRUE, FALSE)
//				
//				IF DisplayLockIcons
//					REQUEST_STREAMED_TEXTURE_DICT("MPHud")
//					IF HAS_STREAMED_TEXTURE_DICT_LOADED("MPHud")
//						IF IS_TAB_LOCKED(i, Placement)
//							Placement.TabLock[i].x = Placement.Tab[i].x+(Placement.Tab[i].w/2)-0.008
//							Placement.TabLock[i].y = Placement.Tab[i].y-0.002
//							Placement.TabLock[i].w = 0.009
//							Placement.TabLock[i].h = 0.017
//							Placement.TabLock[i].r = 255
//							Placement.TabLock[i].g = 255
//							Placement.TabLock[i].b = 255
//							Placement.TabLock[i].a = 255
//							DRAW_2D_SPRITE("MPHud", "MP_Lock_Icon", Placement.TabLock[i], FALSE)
//						ENDIF
//					ENDIF
//				ENDIF
//				
//			ENDIF
//		ENDIF
//	ENDREPEAT
//	IF Placement.iSelectedTab > -1
//		DRAW_RECTANGLE(Placement.TabGangColour[Placement.iSelectedTab])
//	ENDIF
//
//	
//ENDPROC

FUNC BOOL IS_TAB_UNLOCKED(INT TabSelected, TAB_PLACEMENT_TOOLS& Placement)

	SWITCH TabSelected
		CASE 0
			IF NOT IS_BIT_SET(Placement.LockedTabsBitState, LOCKED_TAB_ONE)
				RETURN TRUE
			ENDIF
		BREAK
		CASE 1
			IF NOT IS_BIT_SET(Placement.LockedTabsBitState, LOCKED_TAB_TWO)
				RETURN TRUE
			ENDIF
		BREAK
		CASE 2
			IF NOT IS_BIT_SET(Placement.LockedTabsBitState, LOCKED_TAB_THREE)
				RETURN TRUE
			ENDIF
		BREAK
		CASE 3
			IF NOT IS_BIT_SET(Placement.LockedTabsBitState, LOCKED_TAB_FOUR)
				RETURN TRUE
			ENDIF
		BREAK
		CASE 4
			IF NOT IS_BIT_SET(Placement.LockedTabsBitState, LOCKED_TAB_FIVE)
				RETURN TRUE
			ENDIF
		BREAK
	
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC

FUNC INT FIND_NEXT_VALID_TAB_LEFT(TAB_PLACEMENT_TOOLS& TabPlacement)
	INT I
	INT TempTab = TabPlacement.iSelectedTab
	REPEAT MAX_NUMBER_OF_TABS I
//		IF (i < iNumberOfTabs)
			IF TempTab = 0
				TempTab = TabPlacement.iNumberOfTabs
			ELSE
				TempTab--
			ENDIF
			
			IF IS_TAB_UNLOCKED(TempTab, TabPlacement)
				RETURN TempTab
			ENDIF
//		ENDIF
		
	ENDREPEAT

RETURN 0
ENDFUNC

FUNC INT FIND_NEXT_VALID_TAB_RIGHT(TAB_PLACEMENT_TOOLS& TabPlacement)
	INT I
	INT TempTab = TabPlacement.iSelectedTab
	REPEAT MAX_NUMBER_OF_TABS I
//		IF (i < iNumberOfTabs)
			IF TempTab = TabPlacement.iNumberOfTabs
				TempTab = 0
			ELSE
				TempTab++
			ENDIF

			IF IS_TAB_UNLOCKED(TempTab, TabPlacement)
				RETURN TempTab
			ENDIF
//		ENDIF
		
	ENDREPEAT

RETURN 0
ENDFUNC

/*
//	Graeme - I've commented this out just now because nothing is using it. I'm planning to make the PAD_BUTTON_NUMBER enum only available in Debug scripts.
//	If this function is needed in the future then a new enum will need to be created containing TAB_PLACEMENT_LEFT, TAB_PLACEMENT_RIGHT, TAB_PLACEMENT_LEFTSHOULDER1, TAB_PLACEMENT_RIGHTSHOULDER1
//		to replace the uses of DPADLEFT, DPADRIGHT, LEFTSHOULDER1, RIGHTSHOULDER1
PROC PROCESS_TAB_LOGIC(TAB_PLACEMENT_TOOLS& TabPlacement, BOOL EnableDPADControls, BOOL EnableShoulderControls, BOOL& HasButtonBeenPressed)
	
	IF EnableShoulderControls = TRUE
		IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LB)
			IF NOT IS_BIT_SET(TabPlacement.iTabButtonPressed, ENUM_TO_INT(LEFTSHOULDER1))
				TabPlacement.iSelectedTab = FIND_NEXT_VALID_TAB_LEFT(TabPlacement)
				HasButtonBeenPressed = TRUE
				SET_BIT(TabPlacement.iTabButtonPressed, ENUM_TO_INT(LEFTSHOULDER1))
			ENDIF
		ELSE
			IF IS_BIT_SET(TabPlacement.iTabButtonPressed, ENUM_TO_INT(LEFTSHOULDER1))
				CLEAR_BIT(TabPlacement.iTabButtonPressed, ENUM_TO_INT(LEFTSHOULDER1))
			ENDIF
		ENDIF
		
		IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RB)

			IF NOT IS_BIT_SET(TabPlacement.iTabButtonPressed, ENUM_TO_INT(RIGHTSHOULDER1))
				TabPlacement.iSelectedTab = FIND_NEXT_VALID_TAB_RIGHT(TabPlacement)
				HasButtonBeenPressed = TRUE
				SET_BIT(TabPlacement.iTabButtonPressed, ENUM_TO_INT(RIGHTSHOULDER1))
			ENDIF
		ELSE
			IF IS_BIT_SET(TabPlacement.iTabButtonPressed, ENUM_TO_INT(RIGHTSHOULDER1))
				CLEAR_BIT(TabPlacement.iTabButtonPressed, ENUM_TO_INT(RIGHTSHOULDER1))
			ENDIF
		ENDIF
	ENDIF
	
	IF EnableDPADControls = TRUE
		IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
			IF NOT IS_BIT_SET(TabPlacement.iTabButtonPressed, ENUM_TO_INT(DPADLEFT))
				TabPlacement.iSelectedTab = FIND_NEXT_VALID_TAB_LEFT(TabPlacement)
				HasButtonBeenPressed = TRUE
				SET_BIT(TabPlacement.iTabButtonPressed, ENUM_TO_INT(DPADLEFT))
			ENDIF
		ELSE
			IF IS_BIT_SET(TabPlacement.iTabButtonPressed, ENUM_TO_INT(DPADLEFT))
				CLEAR_BIT(TabPlacement.iTabButtonPressed, ENUM_TO_INT(DPADLEFT))
			ENDIF
		ENDIF
		
		IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
			IF NOT IS_BIT_SET(TabPlacement.iTabButtonPressed, ENUM_TO_INT(DPADRIGHT))
				TabPlacement.iSelectedTab = FIND_NEXT_VALID_TAB_RIGHT(TabPlacement)
				HasButtonBeenPressed = TRUE
				SET_BIT(TabPlacement.iTabButtonPressed, ENUM_TO_INT(DPADRIGHT))
			ENDIF
		ELSE
			IF IS_BIT_SET(TabPlacement.iTabButtonPressed, ENUM_TO_INT(DPADRIGHT))
				CLEAR_BIT(TabPlacement.iTabButtonPressed, ENUM_TO_INT(DPADRIGHT))
			ENDIF
		ENDIF	
	ENDIF
	
ENDPROC
*/

PROC SET_TAB_TO_TEAM_COLOUR(INT iTabNumber, INT iTeam, TAB_PLACEMENT_TOOLS& Placement)
	SET_RECT_TO_TEAM_COLOUR(Placement.TabGangColour[iTabNumber], iTeam)
ENDPROC

PROC SET_ALL_TABS_TO_TEAM_COLOUR(INT iTeam, TAB_PLACEMENT_TOOLS& Placement)
	INT i
	REPEAT MAX_NUMBER_OF_TABS i
		SET_TAB_TO_TEAM_COLOUR(i, iTeam, Placement)	
	ENDREPEAT
ENDPROC

PROC SET_SELECTED_TAB_BOX_TO_TEAM_COLOUR(INT iTeam, TAB_PLACEMENT_TOOLS& Placement)
	SET_TAB_TO_TEAM_COLOUR(Placement.iSelectedTab, iTeam, Placement)	
ENDPROC

PROC SET_TAB_TO_PLAYER_COLOUR(INT iTabNumber, TAB_PLACEMENT_TOOLS& Placement)
	SET_RECT_TO_PLAYER_COLOUR(Placement.TabGangColour[iTabNumber])
ENDPROC

PROC SET_ALL_TABS_TO_PLAYER_COLOUR(TAB_PLACEMENT_TOOLS& Placement)
	INT i
	REPEAT MAX_NUMBER_OF_TABS i
		SET_TAB_TO_PLAYER_COLOUR(i, Placement)	
	ENDREPEAT	
ENDPROC



//PROC CLEANUP_TABS()		
//
//	#IF IS_DEBUG_BUILD
//	IF DOES_WIDGET_GROUP_EXIST(TabWidget)
//		DELETE_WIDGET_GROUP(TabWidget)
//	ENDIF
//	#endif
//	
//	// set defaults
//	bUseEvenSpacing = FALSE
//	bDullNonSelectedTabs = TRUE	
//		
//	INT i
//	REPEAT MAX_NUMBER_OF_TABS i
//		Tab[i].r = 0
//		Tab[i].g = 0
//		Tab[i].b = 0
//		Tab[i].a = 0
//		TabString[i] = ""
//	ENDREPEAT
//	
//	SET_SHARED_SCREEN_SIZES(FALSE)
//	
//	NET_PRINT("Cleaning up Tabs") NET_NL()
//	
//	bTabsInitialised = FALSE
//ENDPROC




