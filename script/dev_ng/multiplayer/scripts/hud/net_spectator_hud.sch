
USING "screens_header.sch"
USING "net_events.sch"
USING "screen_tabs.sch"
USING "script_network.sch"
USING "net_common_functions.sch"
//USING "transition_common.sch"
//USING "leader_board_common.sch"
USING "net_spectator_variables.sch"
USING "net_SCTV_common.sch"
USING "decorator_initialiser.sch"
USING "mp_scaleform_functions_xml.sch"
USING "net_mission_details_hud.sch"
USING "leader_board_common.sch"

#IF IS_DEBUG_BUILD

/// PURPOSE:
///    Creates widgets for spectator hud
PROC CREATE_SPECTATOR_HUD_WIDGET()

ENDPROC


/// PURPOSE:
///    Updates widgets for spectator hud
PROC UPDATE_SPECTATOR_HUD_WIDGET()
	
ENDPROC

/// PURPOSE:
///    turns SPEC_HUD_MODE into a debug string
/// PARAMS:
///    thisMode - 
/// RETURNS:
///    thisMode as a string 
FUNC STRING GET_SPEC_HUD_MODE_NAME(SPEC_HUD_MODE thisMode)
	SWITCH thisMode
		CASE SPEC_HUD_MODE_FULL
			RETURN "SPEC_HUD_MODE_FULL"
		BREAK
		CASE SPEC_HUD_MODE_MINIMAL
			RETURN "SPEC_HUD_MODE_MINIMAL"
		BREAK
		CASE SPEC_HUD_MODE_HIDDEN
			RETURN "SPEC_HUD_MODE_HIDDEN"
		BREAK
	ENDSWITCH
	RETURN ""
ENDFUNC

#ENDIF

// Commented out as pc controls now hard wired due to B* 2249255
/// PURPOSE:
///  Sets up PC control scheme
//PROC SETUP_PC_CONTROLS(SPECTATOR_DATA_STRUCT &specData)
//	IF IS_PC_VERSION()
//	
//		IF specData.bPCCOntrolsSetup = FALSE
//			INIT_PC_SCRIPTED_CONTROLS("SPECTATOR HUD")
//			specData.bPCCOntrolsSetup = TRUE
//			CPRINTLN(DEBUG_SPECTATOR, "=== CAM === PROCESS_eSPECCAMSTATE_TURN_OFF - SETUP PC CONTROLS")
//		ENDIF
//	ENDIF
//ENDPROC
//
//PROC CLEANUP_PC_CONTROLS(SPECTATOR_DATA_STRUCT &specData)
//
//	IF IS_PC_VERSION()
//		IF specData.bPCCOntrolsSetup = TRUE
//			SHUTDOWN_PC_SCRIPTED_CONTROLS()
//			CPRINTLN(DEBUG_SPECTATOR, "=== CAM === PROCESS_eSPECCAMSTATE_TURN_OFF - SHUTDOWN PC CONTROLS")
//			specData.bPCCOntrolsSetup = FALSE
//		ENDIF
//	ENDIF
//ENDPROC

//PURPOSE: Checks if the PLayer is set to not be Spectated
FUNC BOOL DOES_PLAYER_ALLOW_SPECTATORS(INT iPlayer)
	//Rockstar Dev can Spectate Everyone
	IF IS_ROCKSTAR_DEV()
		RETURN TRUE
	ENDIF
	
	//SCTV and MPTV obey Flag
	IF IS_PLAYER_SCTV(PLAYER_ID())
	OR g_bMPTVplayerWatchingTV
		RETURN GlobalplayerBD_FM[iPlayer].scoreData.bCanSpectate
	ENDIF
	
	//Mission Spectators can view everyone
	RETURN TRUE
ENDFUNC

//PURPOSE: Sends the player from SCTV back to SP
PROC QUIT_SCTV_BACK_TO_SP()
	DO_SCREEN_FADE_OUT(0)//snap to black!
	
	NET_NL()NET_PRINT("QUIT_SCTV_BACK_TO_SP - called")
			
	SET_CAMERA_QUICK_EXIT_TO_SP(TRUE)
	SET_MP_PAUSE_MENU_SKYCAM_UP(TRUE)
	
	RESET_EVERYTHING_TRANSITION_SESSION_FOR_SP_BAIL()
	
	SET_PAUSE_MENU_REQUESTING_A_WARP()
	SET_PAUSE_MENU_WARP_TO_MODE(GAMEMODE_SP)
	
	HUD_CHANGE_STATE(HUD_STATE_QUIT_CURRENT_SESSION_PROMPT)
	PRINTLN("SCTV - MANAGE_STAGE_QUIT_YES_NO_INPUT - QUIT TO SP")
ENDPROC

//PURPOSE: Sends the player from SCTV back to FM
PROC QUIT_SCTV_BACK_TO_GTAO()
	DO_SCREEN_FADE_OUT(0)//snap to black!
			
	NET_NL()NET_PRINT("QUIT_SCTV_BACK_TO_GTAO - called")			
			
	SET_CAMERA_QUICK_EXIT_TO_SP(TRUE)
	SET_MP_PAUSE_MENU_SKYCAM_UP(TRUE)
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
	AND NOT NETWORK_IS_ACTIVITY_SESSION()
		IF NETWORK_IS_IN_TUTORIAL_SESSION()
			PRINTLN("[TS] * SCTV - NETWORK_BAIL_TRANSITION(ENUM_TO_INT(NETWORK_BAIL_SCTV_MANUAL_QUIT))")
			NETWORK_BAIL_TRANSITION(ENUM_TO_INT(NETWORK_BAIL_SCTV_MANUAL_QUIT))
		ENDIF
	ENDIF
	
	RESET_EVERYTHING_TRANSITION_SESSION_FOR_SP_BAIL()
	
	SET_PAUSE_MENU_REQUESTING_A_WARP()
	SET_PAUSE_MENU_WARP_TO_MODE(GAMEMODE_FM)
	
	SET_CURRENT_TRANSITION_FM_SESSION_MENU_CHOICE(g_Private_Players_FM_SESSION_Menu_Choice_LAST_ENTRY)
	SET_JOINING_GAMEMODE(GAMEMODE_FM)
	
	//2473835, pause the renderphase if we're in chris's special SCTV camera
	IF NATIVE_TO_INT(PLAYER_ID()) > -1
		IF IS_BIT_SET(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iFmAmbientEventBitSet, ciSET_PLAYER_IS_PENNED_IN_SPECTATOR)
			SET_SKYFREEZE_FROZEN(TRUE)
		ENDIF	
	ENDIF
	
	HUD_CHANGE_STATE(HUD_STATE_QUIT_CURRENT_SESSION_PROMPT)
	PRINTLN("SCTV - MANAGE_STAGE_QUIT_YES_NO_INPUT - QUIT TO SP")
ENDPROC

/// PURPOSE:
///    Sets the ped the spectator cam is focusing on.
/// PARAMS:
///    thisPed - ped the spectator cam is focusing on.
PROC SET_SPECTATOR_CURRENT_FOCUS_PED(SPECTATOR_DATA_STRUCT &specData, PED_INDEX thisPed)
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_SPECTATOR, "=== HUD === SET_SPECTATOR_CURRENT_FOCUS_PED")
	#ENDIF
	IF SPEC_IS_PED_ACTIVE(MPSpecGlobals.pedCurrentFocus)
		specData.pedLastCurrentFocus = MPSpecGlobals.pedCurrentFocus
		#IF IS_DEBUG_BUILD
		IF IS_PED_A_PLAYER(specData.pedLastCurrentFocus)
		AND NETWORK_IS_PLAYER_ACTIVE(NETWORK_GET_PLAYER_INDEX_FROM_PED(specData.pedLastCurrentFocus))
			CPRINTLN(DEBUG_SPECTATOR, "=== HUD === Save last current - setting pedLastCurrentFocus = ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX_FROM_PED(specData.pedLastCurrentFocus)))
		ELSE
			CPRINTLN(DEBUG_SPECTATOR, "=== HUD === Save last current - setting pedLastCurrentFocus = ", NATIVE_TO_INT(specData.pedLastCurrentFocus))
		ENDIF
		#ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_SPECTATOR, "=== HUD === Save last current - MPSpecGlobals.pedCurrentFocus is NOT active")
		#ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF SPEC_IS_PED_ACTIVE(thisPed)
		IF IS_PED_A_PLAYER(thisPed)
		AND NETWORK_IS_PLAYER_ACTIVE(NETWORK_GET_PLAYER_INDEX_FROM_PED(thisPed))
			CPRINTLN(DEBUG_SPECTATOR, "=== HUD === MPSpecGlobals.pedCurrentFocus = ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX_FROM_PED(thisPed)))
		ELSE
			CPRINTLN(DEBUG_SPECTATOR, "=== HUD === MPSpecGlobals.pedCurrentFocus = ", NATIVE_TO_INT(thisPed))
		ENDIF
	ELSE
		CPRINTLN(DEBUG_SPECTATOR, "=== HUD === SPEC_IS_PED_ACTIVE(thisPed) = FALSE")
	ENDIF
	#ENDIF
	
	MPSpecGlobals.pedCurrentFocus = thisPed
	
	IF USING_HEIST_SPECTATE()
		MPGlobalsAmbience.piHeistSpectateTarget = NETWORK_GET_PLAYER_INDEX_FROM_PED(thisPed)
		#IF IS_DEBUG_BUILD
		IF IS_PED_A_PLAYER(thisPed)
		AND NETWORK_IS_PLAYER_ACTIVE(NETWORK_GET_PLAYER_INDEX_FROM_PED(thisPed))
			CPRINTLN(DEBUG_SPECTATOR, "=== HUD === MPGlobalsAmbience.piHeistSpectateTarget = ", GET_PLAYER_NAME(MPGlobalsAmbience.piHeistSpectateTarget))
		ELSE
			CPRINTLN(DEBUG_SPECTATOR, "=== HUD === MPGlobalsAmbience.piHeistSpectateTarget Player not active")
		ENDIF
		#ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets the ped the spectator cam should focus on.
/// PARAMS:
///    thisPed - ped the spectator cam should focus on.
PROC SET_SPECTATOR_DESIRED_FOCUS_PED(SPECTATOR_DATA_STRUCT &specData, PED_INDEX thisPed)
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_SPECTATOR, "=== HUD === SET_SPECTATOR_DESIRED_FOCUS_PED")
	#ENDIF
	IF SPEC_IS_PED_ACTIVE(MPSpecGlobals.pedDesiredFocus)
		specData.pedLastDesiredFocus = MPSpecGlobals.pedDesiredFocus
		#IF IS_DEBUG_BUILD
		IF IS_PED_A_PLAYER(specData.pedLastDesiredFocus)
		AND NETWORK_IS_PLAYER_ACTIVE(NETWORK_GET_PLAYER_INDEX_FROM_PED(specData.pedLastDesiredFocus))
			CPRINTLN(DEBUG_SPECTATOR, "=== HUD === Save last current - setting pedLastDesiredFocus = ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX_FROM_PED(specData.pedLastDesiredFocus)))
		ELSE
			CPRINTLN(DEBUG_SPECTATOR, "=== HUD === Save last current - setting pedLastDesiredFocus = ", NATIVE_TO_INT(specData.pedLastDesiredFocus))
		ENDIF
		#ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_SPECTATOR, "=== HUD === Save last desired - MPSpecGlobals.pedDesiredFocus is NOT active")
		#ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF SPEC_IS_PED_ACTIVE(thisPed)
		IF IS_PED_A_PLAYER(thisPed)
		AND NETWORK_IS_PLAYER_ACTIVE(NETWORK_GET_PLAYER_INDEX_FROM_PED(thisPed))
			CPRINTLN(DEBUG_SPECTATOR, "=== HUD === MPSpecGlobals.pedDesiredFocus = ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX_FROM_PED(thisPed)))
		ELSE
			CPRINTLN(DEBUG_SPECTATOR, "=== HUD === MPSpecGlobals.pedDesiredFocus = ", NATIVE_TO_INT(thisPed))
		ENDIF
	ELSE
		CPRINTLN(DEBUG_SPECTATOR, "=== HUD === SPEC_IS_PED_ACTIVE(thisPed) = FALSE")
	ENDIF
	#ENDIF
	
	MPSpecGlobals.pedDesiredFocus = thisPed
ENDPROC

/// PURPOSE:
///    Gets the current spectator camera mode.
/// PARAMS:
///    sSpecCamData - struct of spec cam data.
/// RETURNS:
///    Spectator camera current mode.
FUNC eSPEC_CAM_MODE GET_SPEC_CAM_MODE(SPECTATOR_CAM_DATA &sSpecCamData)
	RETURN sSpecCamData.eMode
ENDFUNC

/// PURPOSE:
///    Sets the global for when the corona has set the screen effects while transitioning into a Job
/// PARAMS:
///    bActive - Value to set this to.
PROC SET_SPEC_CORONA_SCREEN_EFFECTS_ACTIVE(BOOL bActive)
	CPRINTLN(DEBUG_SPECTATOR, "=== HUD === SET_SPEC_CORONA_SCREEN_EFFECTS_ACTIVE MPSpecGlobals.SCTVData.bCoronaScreenEffectsActive = ", PICK_STRING(bActive, "TRUE", "FALSE"))
	MPSpecGlobals.SCTVData.bCoronaScreenEffectsActive = bActive
ENDPROC

/// PURPOSE:
///    Getter for above function. Says whether we believe corona effects are active or not
FUNC BOOL GET_SPEC_CORONA_SCREEN_EFFECTS_ACTIVE()
	RETURN MPSpecGlobals.SCTVData.bCoronaScreenEffectsActive
ENDFUNC

FUNC BOOL IS_INSTRUCTIONAL_BUTTON_UPDATE_REQUESTED()
	RETURN IS_BIT_SET(MPGlobalsAmbience.iAmbBitSet, iABI_UPDATE_SPEC_INSTRUCTIONAL_BUTTONS)
ENDFUNC

//PURPOSE: Sets a flag that will request a button update.
PROC REQUEST_SPEC_INSTRUCTIONAL_BUTTON_UPDATE()
	IF NOT IS_BIT_SET(MPGlobalsAmbience.iAmbBitSet, iABI_UPDATE_SPEC_INSTRUCTIONAL_BUTTONS)
		SET_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_UPDATE_SPEC_INSTRUCTIONAL_BUTTONS)
		PRINTLN(" REQUEST_SPEC_INSTRUCTIONAL_BUTTON_UPDATE - iABI_UPDATE_SPEC_INSTRUCTIONAL_BUTTONS - SET")
	ENDIF
ENDPROC

//PURPOSE: Clears a flag that will request a button update.
PROC CLEAR_SPEC_INSTRUCTIONAL_BUTTON_UPDATE_REQUEST()
	IF IS_BIT_SET(MPGlobalsAmbience.iAmbBitSet, iABI_UPDATE_SPEC_INSTRUCTIONAL_BUTTONS)
		CLEAR_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_UPDATE_SPEC_INSTRUCTIONAL_BUTTONS)
		PRINTLN(" CLEAR_SPEC_INSTRUCTIONAL_BUTTON_UPDATE_REQUEST - iABI_UPDATE_SPEC_INSTRUCTIONAL_BUTTONS - CLEARED")
	ENDIF
ENDPROC

FUNC BOOL IS_SPECTATOR_FILTER_OPTION_DISABLED()
	IF IS_BIT_SET(MPGlobalsAmbience.iAmbBitSet, iABI_DISABLE_SPECTATOR_FILTER_OPTION)
		RETURN TRUE
	ENDIF
	
	IF IS_SPECTATOR_RUNNING_CUTSCENE()
		RETURN TRUE
	ENDIF
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_DAWN_RAID(g_FMMC_STRUCT.iAdversaryModeType)
		RETURN TRUE
	ENDIF
	
	#IF FEATURE_COPS_N_CROOKS
	IF IS_FREEMODE_ARCADE()
	AND GET_ARCADE_MODE() = ARC_COPS_CROOKS
		RETURN TRUE
	ENDIF
	#ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Sets a flag to the passed in value. Blocks the option for the Spectator to change the current filter.
PROC DISABLE_SPECTATOR_FILTER_OPTION(BOOL bSet)
	IF bSet = TRUE
		IF NOT IS_BIT_SET(MPGlobalsAmbience.iAmbBitSet, iABI_DISABLE_SPECTATOR_FILTER_OPTION)
			SET_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_DISABLE_SPECTATOR_FILTER_OPTION)
			REQUEST_SPEC_INSTRUCTIONAL_BUTTON_UPDATE()
			PRINTLN(" DISABLE_SPECTATOR_FILTER_OPTION - iABI_DISABLE_SPECTATOR_FILTER_OPTION - SET")
		ENDIF
	ELSE
		IF IS_BIT_SET(MPGlobalsAmbience.iAmbBitSet, iABI_DISABLE_SPECTATOR_FILTER_OPTION)
			CLEAR_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_DISABLE_SPECTATOR_FILTER_OPTION)
			REQUEST_SPEC_INSTRUCTIONAL_BUTTON_UPDATE()
			PRINTLN(" DISABLE_SPECTATOR_FILTER_OPTION - iABI_DISABLE_SPECTATOR_FILTER_OPTION - CLEARED")
		ENDIF
	ENDIF
ENDPROC


//PURPOSE: Turns off/on Spectator Radio if they currently have it on.
PROC DISABLE_SPECTATOR_RADIO(BOOL bOn)
	IF bOn = TRUE
		IF IS_BIT_SET(MPSpecGlobals.iBailBitset, GLOBAL_SPEC_BAIL_BS_RADIO_ON)
			SET_PLAYER_SPECTATED_VEHICLE_RADIO_OVERRIDE(FALSE)
			SET_AUDIO_FLAG("AllowRadioDuringSwitch", FALSE) 
			SET_MOBILE_PHONE_RADIO_STATE(FALSE)
			SET_AUDIO_FLAG("MobileRadioInGame", FALSE)
		ENDIF
		DISABLE_SPECTATOR_FILTER_OPTION(TRUE)
		CPRINTLN(DEBUG_SPECTATOR, " DISABLE_SPECTATOR_RADIO - TURN SPECTATOR RADIO OFF")
	ELSE
		IF IS_BIT_SET(MPSpecGlobals.iBailBitset, GLOBAL_SPEC_BAIL_BS_RADIO_ON)
			SET_PLAYER_SPECTATED_VEHICLE_RADIO_OVERRIDE(TRUE)
			SET_AUDIO_FLAG("AllowRadioDuringSwitch", TRUE) 
			SET_MOBILE_PHONE_RADIO_STATE(TRUE)
			SET_AUDIO_FLAG("MobileRadioInGame", TRUE)
		ENDIF
		DISABLE_SPECTATOR_FILTER_OPTION(FALSE)
		CPRINTLN(DEBUG_SPECTATOR, " DISABLE_SPECTATOR_RADIO - TURN SPECTATOR RADIO BACK ON")
	ENDIF
	
	#IF IS_DEBUG_BUILD
	DEBUG_PRINTCALLSTACK()
	#ENDIF
ENDPROC

FUNC BOOL IS_SPECTATOR_HUD_HIDDEN_FLAG_SET()
	RETURN IS_BIT_SET(MPGlobalsAmbience.iAmbBitSet, iABI_HIDE_SPECTATOR_HUD)
ENDFUNC

//PURPOSE: Sets a flag to the passed in value. Blocks the Spectator HUD from drawing.
PROC HIDE_SPECTATOR_HUD(BOOL bSet)
	IF bSet = TRUE
		IF NOT IS_BIT_SET(MPGlobalsAmbience.iAmbBitSet, iABI_HIDE_SPECTATOR_HUD)
			SET_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_HIDE_SPECTATOR_HUD)
			PRINTLN(" HIDE_SPECTATOR_HUD - iABI_HIDE_SPECTATOR_HUD - SET")
			DEBUG_PRINTCALLSTACK()
		ENDIF
	ELSE
		IF IS_BIT_SET(MPGlobalsAmbience.iAmbBitSet, iABI_HIDE_SPECTATOR_HUD)
			CLEAR_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_HIDE_SPECTATOR_HUD)
			PRINTLN(" HIDE_SPECTATOR_HUD - iABI_HIDE_SPECTATOR_HUD - CLEARED")
			DEBUG_PRINTCALLSTACK()
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL ARE_SPECTATOR_BUTTONS_HIDDEN()
	RETURN IS_BIT_SET(MPGlobalsAmbience.iAmbBitSet, iABI_HIDE_SPECTATOR_BUTTONS)
ENDFUNC

//PURPOSE: Sets a flag to the passed in value. Blocks the Spectator controls from processing any input.
PROC DISABLE_SPECTATOR_CONTROLS(BOOL bDisable, SPECTATOR_DATA_STRUCT &specData)
	IF bDisable = TRUE
		IF NOT IS_BIT_SET(specData.specHUDData.iBitset, SPEC_CAM_DISABLE_ALL_CONTROLS)
			SET_BIT(specData.specHUDData.iBitset, SPEC_CAM_DISABLE_ALL_CONTROLS)
			PRINTLN(" DISABLE_SPECTATOR_CONTROLS - SPEC_CAM_DISABLE_ALL_CONTROLS - SET")
			DEBUG_PRINTCALLSTACK()
		ENDIF
	ELSE
		IF IS_BIT_SET(specData.specHUDData.iBitset, SPEC_CAM_DISABLE_ALL_CONTROLS)
			CLEAR_BIT(specData.specHUDData.iBitset, SPEC_CAM_DISABLE_ALL_CONTROLS)
			PRINTLN(" DISABLE_SPECTATOR_CONTROLS - SPEC_CAM_DISABLE_ALL_CONTROLS - CLEARED")
			DEBUG_PRINTCALLSTACK()
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL ARE_SPECTATOR_CONTROLS_DISABLED()
	RETURN IS_BIT_SET(MPGlobalsAmbience.iAmbBitSet, iABI_HIDE_SPECTATOR_BUTTONS)
ENDFUNC

//PURPOSE: Sets a flag to the passed in value. Blocks the Spectator buttons from drawing.
PROC HIDE_SPECTATOR_BUTTONS(BOOL bSet)
	IF bSet = TRUE
		IF NOT IS_BIT_SET(MPGlobalsAmbience.iAmbBitSet, iABI_HIDE_SPECTATOR_BUTTONS)
			SET_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_HIDE_SPECTATOR_BUTTONS)
			PRINTLN(" HIDE_SPECTATOR_BUTTONS - iABI_HIDE_SPECTATOR_BUTTONS - SET")
			DEBUG_PRINTCALLSTACK()
		ENDIF
	ELSE
		IF IS_BIT_SET(MPGlobalsAmbience.iAmbBitSet, iABI_HIDE_SPECTATOR_BUTTONS)
			CLEAR_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_HIDE_SPECTATOR_BUTTONS)
			PRINTLN(" HIDE_SPECTATOR_BUTTONS - iABI_HIDE_SPECTATOR_BUTTONS - CLEARED")
			DEBUG_PRINTCALLSTACK()
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Spectator HUD - Set iNumberOfTargetsOnAPage to how many targets to be listed at once. 
///    Set iMaximumPage to the total number of pages-1 required to fit in all the targets
///    Set iNumberOfTargetsOnCurrentPage to how many targets are currently viewable on the page the player is viewing- DG
PROC CALCULATE_NUMBER_OF_TARGETS_ON_A_PAGE(SPECTATOR_HUD_DATA &specHUDData)

	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_ExtraSpectatorDebug")
		PRINTLN("SPECTATOR - MANAGE_HIGHLIGHT_AND_SELECT_LIST - CALCULATE_NUMBER_OF_TARGETS_ON_A_PAGE - iCurrentNumberOfTargetsInAllLists = ", specHUDData.iCurrentNumberOfTargetsInAllLists)
		PRINTLN("SPECTATOR - MANAGE_HIGHLIGHT_AND_SELECT_LIST - CALCULATE_NUMBER_OF_TARGETS_ON_A_PAGE - LARGEST_PAGE_SIZE = ", LARGEST_PAGE_SIZE)
		PRINTLN("SPECTATOR - MANAGE_HIGHLIGHT_AND_SELECT_LIST - CALCULATE_NUMBER_OF_TARGETS_ON_A_PAGE - iNumberOfTargetsOnAPage = ", specHUDData.iNumberOfTargetsOnAPage)
	ENDIF
	#ENDIF
	
	IF specHUDData.iCurrentNumberOfTargetsInAllLists > LARGEST_PAGE_SIZE
		specHUDData.iNumberOfTargetsOnAPage = LARGEST_PAGE_SIZE
		
		specHUDData.iMaximumPage = 0
		INT iPageCount = specHUDData.iCurrentNumberOfTargetsInAllLists
		WHILE iPageCount > -1
			iPageCount -= specHUDData.iNumberOfTargetsOnAPage
			specHUDData.iMaximumPage++
		ENDWHILE
		specHUDData.iMaximumPage -= 1
		
		#IF IS_DEBUG_BUILD
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_ExtraSpectatorDebug")
			PRINTLN("SPECTATOR - MANAGE_HIGHLIGHT_AND_SELECT_LIST - CALCULATE_NUMBER_OF_TARGETS_ON_A_PAGE - A - iMaximumPage = ", specHUDData.iMaximumPage)
		ENDIF
		#ENDIF
	ELIF specHUDData.iCurrentNumberOfTargetsInAllLists = 0
		specHUDData.iNumberOfTargetsOnAPage = 0
		specHUDData.iMaximumPage = 0
		
		#IF IS_DEBUG_BUILD
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_ExtraSpectatorDebug")
			PRINTLN("SPECTATOR - MANAGE_HIGHLIGHT_AND_SELECT_LIST - CALCULATE_NUMBER_OF_TARGETS_ON_A_PAGE - B - iMaximumPage = ", specHUDData.iMaximumPage)
		ENDIF
		#ENDIF
	ELSE
		specHUDData.iNumberOfTargetsOnAPage = specHUDData.iCurrentNumberOfTargetsInAllLists
		specHUDData.iMaximumPage = 0
		
		#IF IS_DEBUG_BUILD
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_ExtraSpectatorDebug")
			PRINTLN("SPECTATOR - MANAGE_HIGHLIGHT_AND_SELECT_LIST - CALCULATE_NUMBER_OF_TARGETS_ON_A_PAGE - C - iMaximumPage = ", specHUDData.iMaximumPage)
		ENDIF
		#ENDIF
	ENDIF
	
	IF specHUDData.iMaximumPage > 0
		specHUDData.iNumberOfTargetsOnCurrentPage = specHUDData.iCurrentNumberOfTargetsInAllLists - (specHUDData.iCurrentPage * specHUDData.iNumberOfTargetsOnAPage)
		IF specHUDData.iNumberOfTargetsOnCurrentPage > specHUDData.iNumberOfTargetsOnAPage
			specHUDData.iNumberOfTargetsOnCurrentPage = specHUDData.iNumberOfTargetsOnAPage
		ENDIF
		
		#IF IS_DEBUG_BUILD
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_ExtraSpectatorDebug")
			PRINTLN("SPECTATOR - MANAGE_HIGHLIGHT_AND_SELECT_LIST - CALCULATE_NUMBER_OF_TARGETS_ON_A_PAGE - A - iNumberOfTargetsOnCurrentPage = ", specHUDData.iNumberOfTargetsOnCurrentPage)
		ENDIF
		#ENDIF
	ELSE
		specHUDData.iNumberOfTargetsOnCurrentPage = specHUDData.iNumberOfTargetsOnAPage
		
		#IF IS_DEBUG_BUILD
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_ExtraSpectatorDebug")
			PRINTLN("SPECTATOR - MANAGE_HIGHLIGHT_AND_SELECT_LIST - CALCULATE_NUMBER_OF_TARGETS_ON_A_PAGE - B - iNumberOfTargetsOnCurrentPage = ", specHUDData.iNumberOfTargetsOnCurrentPage)
		ENDIF
		#ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Gets the data struct of a spectator target list entry using a list index and an entry index
/// PARAMS:
///    specHUDData - 
///    iListID - entry in the array of lists
///    iEntryID - entry within the list
/// RETURNS:
///    specHUDTargetListEntry of the spectator target
FUNC specHUDTargetListEntry GET_SPECTATOR_HUD_TARGET_LIST_ENTRY(SPECTATOR_HUD_DATA &specHUDData, INT iListID, INT iEntryID)
	RETURN specHUDData.specHUDList[iListID].Entry[iEntryID]
ENDFUNC

/// PURPOSE:
///    Gets the PED_INDEX of a spectator target list entry using a list index and an entry index
/// PARAMS:
///    specHUDData - 
///    iListID - entry in the array of lists
///    iEntryID - entry within the list
/// RETURNS:
///    PED_INDEX of the spectator target
FUNC PED_INDEX GET_SPECTATOR_HUD_TARGET_LIST_ENTRY_PED_ID(SPECTATOR_HUD_DATA &specHUDData, INT iListID, INT iEntryID)
	specHUDTargetListEntry returnEntry = GET_SPECTATOR_HUD_TARGET_LIST_ENTRY(specHUDData, iListID, iEntryID)
	RETURN returnEntry.pedIndex
ENDFUNC

/// PURPOSE:
///    Gets the specific list id and entry id for a spectator target list ID
/// PARAMS:
///    specHUDData - 
///    returnList - stores the list ID of the entry
///    returnEntry - returns the entry ID within returnList's list ID
///    iAllListsEntry - the ID the function is searching for, as part of the entire spectator list
/// RETURNS:
///    true if the list entry is found
FUNC BOOL GET_SPECTATOR_HUD_TARGET_LIST_AND_ENTRY_ID(SPECTATOR_HUD_DATA &specHUDData, INT &returnList, INT &returnEntry, INT iAllListsEntry)
	INT i
	INT iProcessEntry = iAllListsEntry
	
	REPEAT NUMBER_OF_SPEC_HUD_TARGET_LISTS i
	
		SWITCH i
			CASE SPEC_HUD_TARGET_LIST_PLAYERS			//Want all list members from these lists displayed
			CASE SPEC_HUD_TARGET_LIST_SPECIAL_ENEMIES
			CASE SPEC_HUD_TARGET_LIST_SPECIAL_ALLIES
				IF iProcessEntry > specHUDData.iCurrentNumberOfTargetsInList[i]-1		//If entry is beyond this list
					iProcessEntry -= specHUDData.iCurrentNumberOfTargetsInList[i]		//take away list amount, move onto next list
				ELSE																	//If entry is in this list
					returnEntry = iProcessEntry
					returnList = i
					RETURN TRUE
				ENDIF
			BREAK
			CASE SPEC_HUD_TARGET_LIST_GENERIC_ENEMIES	//These lists should just have a single representative, if they are populated
			CASE SPEC_HUD_TARGET_LIST_GENERIC_ALLIES
				IF iProcessEntry > 0													//If entry is beyond this list
					iProcessEntry -= 1													//take away list amount, move onto next list
				ELSE																	//If entry is in this list
					returnEntry = iProcessEntry
					returnList = i
					RETURN TRUE
				ENDIF
			BREAK
		ENDSWITCH
	ENDREPEAT
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Gets the data struct of a entry in the spectator target list
/// PARAMS:
///    specHUDData - 
///    iAllListsEntry - the ID the function is searching for, as part of the entire spectator list
/// RETURNS:
///    specHUDTargetListEntry of the entry
FUNC specHUDTargetListEntry GET_SPECTATOR_HUD_TARGET_LISTS_ENTRY(SPECTATOR_HUD_DATA &specHUDData, INT iAllListsEntry)
	INT iReturnList, iReturnEntry
	GET_SPECTATOR_HUD_TARGET_LIST_AND_ENTRY_ID(specHUDData, iReturnList, iReturnEntry, iAllListsEntry)
	
	RETURN specHUDData.specHUDList[iReturnList].Entry[iReturnEntry]
ENDFUNC

/// PURPOSE:
///    Gets the ped index of a entry in the spectator target list
/// PARAMS:
///    specHUDData - 
///    iAllListsEntry - the ID the function is searching for, as part of the entire spectator list
/// RETURNS:
///    PED_iNDEX of the entry
FUNC PED_INDEX GET_SPECTATOR_HUD_TARGET_LISTS_ENTRY_PED_ID(SPECTATOR_HUD_DATA &specHUDData, INT iAllListsEntry)
	specHUDTargetListEntry returnEntry = GET_SPECTATOR_HUD_TARGET_LISTS_ENTRY(specHUDData, iAllListsEntry)
	RETURN returnEntry.pedIndex
ENDFUNC

/// PURPOSE:
///    Returns if the specified spectator target list has any entries
/// PARAMS:
///    specHUDData - 
///    iListID - ID of the desired list
/// RETURNS:
///    true/false
FUNC BOOL IS_SPECTATOR_HUD_TARGET_LIST_POPULATED(SPECTATOR_HUD_DATA &specHUDData, INT iListID)
	IF specHUDData.iCurrentNumberOfTargetsInList[iListID] > 0
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Returns true if there is at least one target in the spectator target list
/// PARAMS:
///    specHUDData - 
/// RETURNS:
///    true/false
FUNC BOOL ARE_SPECTATOR_HUD_TARGET_LISTS_POPULATED(SPECTATOR_HUD_DATA &specHUDData)
	IF specHUDData.iCurrentNumberOfTargetsInAllLists > 0
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Returns if there are at least two targets in the spectator target list
/// PARAMS:
///    specHUDData - 
/// RETURNS:
///    true/false
FUNC BOOL ARE_SPECTATOR_HUD_TARGET_LISTS_POPULATED_ENOUGH_TO_SCROLL(SPECTATOR_HUD_DATA &specHUDData)
	IF GET_SCTV_MODE() = SCTV_MODE_FREE_CAM
		RETURN FALSE
	ENDIF
	IF specHUDData.iCurrentNumberOfTargetsInAllLists > 1
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Returns if another spectator target should be found
/// PARAMS:
///    specData - 
/// RETURNS:
///    true/false
FUNC BOOL SHOULD_FIND_ANOTHER_SPECTATOR_TARGET(SPECTATOR_DATA_STRUCT &specData, BOOL bIgnoreTimer = FALSE)

	BOOL bReturn = TRUE

	IF IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_BLOCK_FIND_NEW_FOCUS)
		CPRINTLN(DEBUG_SPECTATOR, " SHOULD_FIND_ANOTHER_SPECTATOR_TARGET = FALSE - GLOBAL_SPEC_BS_BLOCK_FIND_NEW_FOCUS")
		bReturn = FALSE
	ENDIF
	
	IF IS_BIT_SET(specData.specCamData.iActivateBitset, ASCF_INDEX_ONLY_FIND_TARGET_IF_PLAYER_LIST_POPULATED)
		IF NOT IS_SPECTATOR_HUD_TARGET_LIST_POPULATED(specData.specHUDData, SPEC_HUD_TARGET_LIST_PLAYERS)
			CPRINTLN(DEBUG_SPECTATOR, " SHOULD_FIND_ANOTHER_SPECTATOR_TARGET = FALSE - NOT SPEC_HUD_TARGET_LIST_PLAYERS")
			bReturn = FALSE
		ENDIF
	ENDIF
	
	//SCTV ONLY CHECKS
	IF IS_PLAYER_SCTV(PLAYER_ID())
		//Block trying to find a new target when we are just about to end the job and see the Celebration Screen OR is on a mocap
		BOOL bTargetOnMissionEnding
		BOOL bTargetWatchingMocap
		IF MPGlobalsAmbience.piHeistSpectateTarget != INVALID_PLAYER_INDEX()
			IF IS_NET_PLAYER_OK(MPGlobalsAmbience.piHeistSpectateTarget, FALSE)
				// MP_DECORATOR_BS_MISSION_ENDING is not the overall mission ending, just that the mission is over for the player being spectated 
				bTargetOnMissionEnding = IS_MP_DECORATOR_BIT_SET(MPGlobalsAmbience.piHeistSpectateTarget, MP_DECORATOR_BS_MISSION_ENDING)
				bTargetWatchingMocap = IS_PLAYER_WATCHING_A_MOCAP(MPGlobalsAmbience.piHeistSpectateTarget)
			ENDIF
		ENDIF
		
		/*IF specData.specHUDData.bToldPlayerCantSpectateChosenPlayer = TRUE
			CPRINTLN(DEBUG_SPECTATOR, " SHOULD_FIND_ANOTHER_SPECTATOR_TARGET = FALSE - bToldPlayerCantSpectateChosenPlayer")
			bReturn = FALSE
		ENDIF*/
		
		/*REMOVED FOR BUG 2232923
		IF g_bMissionEnding = TRUE
		OR g_b_JobEnded = TRUE
			CPRINTLN(DEBUG_SPECTATOR, " SHOULD_FIND_ANOTHER_SPECTATOR_TARGET = FALSE - g_bMissionEnding or g_b_JobEnded")
			bReturn = FALSE
		ENDIF*/
		
		IF g_bMissionOver
			IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_SUMO(g_FMMC_STRUCT.iRootContentIDHash)
				CPRINTLN(DEBUG_SPECTATOR, " SHOULD_FIND_ANOTHER_SPECTATOR_TARGET = FALSE - g_bMissionOver")
				bReturn = FALSE
			ENDIF
		ENDIF
		
		// MP_DECORATOR_BS_MISSION_ENDING is not the overall mission ending, just that the mission is over for the player being spectated
		IF IS_PLAYER_PLAYING_OR_PLANNING_HEIST(PLAYER_ID())
		OR IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciROCKSTAR_CREATED_NEW_VS_MISSION_SiegeMentality)
			IF bTargetOnMissionEnding = TRUE
				CPRINTLN(DEBUG_SPECTATOR, " SHOULD_FIND_ANOTHER_SPECTATOR_TARGET = FALSE - bTargetOnMissionEnding")
				bReturn = FALSE
			ENDIF
		ENDIF
		
		IF bTargetWatchingMocap = TRUE
			CPRINTLN(DEBUG_SPECTATOR, " SHOULD_FIND_ANOTHER_SPECTATOR_TARGET = FALSE - bTargetWatchingMocap")
			bReturn = FALSE
		ENDIF
		
		/*IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].playerSCTVTarget != INVALID_PLAYER_INDEX()
			IF IS_NET_PLAYER_OK(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].playerSCTVTarget, FALSE)
				IF IS_GLOBAL_CLIENT_FINISHED_JOB_BIT_SET(NATIVE_TO_INT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].playerSCTVTarget))
					CPRINTLN(DEBUG_SPECTATOR, " SHOULD_FIND_ANOTHER_SPECTATOR_TARGET = FALSE - IS_GLOBAL_CLIENT_FINISHED_JOB_BIT_SET")
					bReturn = FALSE
				ENDIF
			ENDIF
		ENDIF*/
	
	//NON-SCTV ONLY - REMOVED FOR BUG 2235007 (SEE ROWAN AND RYAN)
	/*ELSE
		IF Is_Player_Currently_On_MP_Versus_Mission(PLAYER_ID())
			IF IS_BIT_SET(MPSpecGlobals.iBitset, GLOBAL_SPEC_BS_TEAM_VS_JOB_ENTIRE_TEAM_DEAD)
				CPRINTLN(DEBUG_SPECTATOR, " SHOULD_FIND_ANOTHER_SPECTATOR_TARGET = FALSE - ON MP VS MISSION + ENTIRE TEAM IS DEAD")	//FOR BUG 2193211 - MAY NEED TO ADD A MAX TEAMS 2 CHECK IF WE END UP WITH MISSIONS WITH MORE THAN 2 TEAMS HERE
				bReturn = FALSE
			ENDIF
		ENDIF*/
	ENDIF
	
	IF IS_PLAYLIST_BETWEEN_LEADERBOARDS()
		CPRINTLN(DEBUG_SPECTATOR, " SHOULD_FIND_ANOTHER_SPECTATOR_TARGET = FALSE - IS_PLAYLIST_BETWEEN_LEADERBOARDS")
		bReturn = FALSE
	ENDIF
	
	IF IS_PLAYLIST_LEADERBOARD_BEING_SETTING_UP()
		CPRINTLN(DEBUG_SPECTATOR, " SHOULD_FIND_ANOTHER_SPECTATOR_TARGET = FALSE - IS_PLAYLIST_LEADERBOARD_BEING_SETTING_UP")
		bReturn = FALSE
	ENDIF
	
	IF IS_SPECTATOR_RUNNING_CUTSCENE()
	OR IS_PLAYER_WATCHING_A_MOCAP(PLAYER_ID())
		CPRINTLN(DEBUG_SPECTATOR, " SHOULD_FIND_ANOTHER_SPECTATOR_TARGET = FALSE - MOCAP RUNNING")
		bReturn = FALSE
	ENDIF
	
	IF DOES_ENTITY_EXIST(GET_SPECTATOR_CURRENT_FOCUS_PED())
		IF IS_MISSION_OVER_AND_ON_LB_FOR_SCTV(NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_SPECTATOR_CURRENT_FOCUS_PED()))
			CPRINTLN(DEBUG_SPECTATOR, " SHOULD_FIND_ANOTHER_SPECTATOR_TARGET = FALSE - IS_MISSION_OVER_AND_ON_LB_FOR_SCTV = TRUE")
			bReturn = FALSE
		ENDIF
		
		IF IS_PLAYER_PLAYING_OR_PLANNING_HEIST(PLAYER_ID()) //NEEDED FOR HEISTS - BUG 2241472 - BREAKS VERSUS MISSIONS THOUGH
			IF IS_GLOBAL_CLIENT_FINISHED_JOB_BIT_SET(NATIVE_TO_INT(NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_SPECTATOR_CURRENT_FOCUS_PED())))
				CPRINTLN(DEBUG_SPECTATOR, " SHOULD_FIND_ANOTHER_SPECTATOR_TARGET = FALSE - IS_GLOBAL_CLIENT_FINISHED_JOB_BIT_SET = TRUE")
				bReturn = FALSE
			ENDIF
		ENDIF
	ENDIF
	
	IF bReturn = FALSE
		IF HAS_NET_TIMER_STARTED(specData.stSwitchTargetDelay)
			RESET_NET_TIMER(specData.stSwitchTargetDelay)
			CPRINTLN(DEBUG_SPECTATOR, " SHOULD_FIND_ANOTHER_SPECTATOR_TARGET, bReturn = FALSE, RESET_NET_TIMER() ")
		ELSE
			CPRINTLN(DEBUG_SPECTATOR, " SHOULD_FIND_ANOTHER_SPECTATOR_TARGET, bReturn = FALSE ")
		ENDIF
		
		RETURN FALSE
	ELSE
	
		IF DOES_ENTITY_EXIST(GET_SPECTATOR_CURRENT_FOCUS_PED())

			IF NOT CAN_SPECTATOR_TARGET_IN_CUTSCENE()
				IF IS_PED_A_PLAYER(GET_SPECTATOR_CURRENT_FOCUS_PED()) AND IS_PLAYER_RUNNING_A_CUTSCENE(NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_SPECTATOR_CURRENT_FOCUS_PED()))
					CPRINTLN(DEBUG_SPECTATOR, " SHOULD_FIND_ANOTHER_SPECTATOR_TARGET, current focus ped is in cutscene = TRUE, bReturn = TRUE ")
					RETURN TRUE
				ELSE
					CPRINTLN(DEBUG_SPECTATOR, " SHOULD_FIND_ANOTHER_SPECTATOR_TARGET, not running a cutscene")
				ENDIF
			ENDIF
			
			IF NOT CAN_SPECTATOR_TARGET_THE_DEAD()
				IF IS_ENTITY_DEAD(GET_SPECTATOR_CURRENT_FOCUS_PED())
					CPRINTLN(DEBUG_SPECTATOR, " SHOULD_FIND_ANOTHER_SPECTATOR_TARGET, current focus ped is dead = TRUE, bReturn = TRUE ")
					RETURN TRUE
				ENDIF
			ENDIF
		ELSE
			CPRINTLN(DEBUG_SPECTATOR, " SHOULD_FIND_ANOTHER_SPECTATOR_TARGET, current focus ped does not exist! ")
			IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_RESURRECTION(g_FMMC_STRUCT.iAdversaryModeType)
				CPRINTLN(DEBUG_SPECTATOR, " SHOULD_FIND_ANOTHER_SPECTATOR_TARGET, current focus ped does not exist! bReturn = TRUE for VS_RESURRECTION")
				RETURN TRUE
			ENDIF
		ENDIF
	
		IF bIgnoreTimer
			CPRINTLN(DEBUG_SPECTATOR, " SHOULD_FIND_ANOTHER_SPECTATOR_TARGET, bIgnoreTimer = TRUE, bReturn = TRUE ")
			RETURN TRUE
		ENDIF
		
		IF NOT HAS_NET_TIMER_STARTED(specData.stSwitchTargetDelay)
			START_NET_TIMER(specData.stSwitchTargetDelay)
			CPRINTLN(DEBUG_SPECTATOR, " SHOULD_FIND_ANOTHER_SPECTATOR_TARGET, bReturn = TRUE, START_NET_TIMER() ")
		ELSE
			INT iSwitchTargetDelay = ciSWITCH_TARGET_DELAY
			
			IF IS_PLAYER_DEV_SPECTATOR(PLAYER_ID())
			OR IS_ROCKSTAR_DEV()
				iSwitchTargetDelay = g_sMPtunables.iDevSpectatorSwitchTargetDelay
				CPRINTLN(DEBUG_SPECTATOR, " SHOULD_FIND_ANOTHER_SPECTATOR_TARGET, IS_PLAYER_DEV_SPECTATOR(PLAYER_ID()) OR IS_ROCKSTAR_DEV() = TRUE. iSwitchTargetDelay = ", g_sMPtunables.iDevSpectatorSwitchTargetDelay)
			ENDIF
			
			IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_OVERTIME_ANY(g_FMMC_STRUCT.iAdversaryModeType)
			OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_AIRSHOOTOUT(g_FMMC_STRUCT.iAdversaryModeType)
				iSwitchTargetDelay = ciSWITCH_TARGET_DELAY_OVERTIME
				CPRINTLN(DEBUG_SPECTATOR, " SHOULD_FIND_ANOTHER_SPECTATOR_TARGET, IS_THIS_ROCKSTAR_MISSION_NEW_VS_OVERTIME = TRUE. iSwitchTargetDelay = ", iSwitchTargetDelay)
			ENDIF
				
			IF HAS_NET_TIMER_EXPIRED(specData.stSwitchTargetDelay, iSwitchTargetDelay)
				CPRINTLN(DEBUG_SPECTATOR, " SHOULD_FIND_ANOTHER_SPECTATOR_TARGET, bReturn = TRUE, HAS_NET_TIMER_EXPIRED(", iSwitchTargetDelay, ")")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	CPRINTLN(DEBUG_SPECTATOR, " SHOULD_FIND_ANOTHER_SPECTATOR_TARGET = FALSE")
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Returns if the spectator target list entry contains data
/// PARAMS:
///    thieEntry - entry to check
/// RETURNS:
///    true/false
FUNC BOOL IS_SPECTATOR_HUD_TARGET_LIST_ENTRY_VALID(specHUDTargetListEntry thisEntry)
	
	IF IS_PLAYER_SCTV(PLAYER_ID())
		IF NOT IS_PLAYER_A_ROCKSTAR_DEV()
			IF DOES_ENTITY_EXIST(thisEntry.pedIndex)
				IF NETWORK_GET_PLAYER_INDEX_FROM_PED(thisEntry.pedIndex) != INVALID_PLAYER_INDEX()
					IF NOT DOES_PLAYER_ALLOW_SPECTATORS(NATIVE_TO_INT(NETWORK_GET_PLAYER_INDEX_FROM_PED(thisEntry.pedIndex)))
						#IF IS_DEBUG_BUILD
						//CPRINTLN(DEBUG_SPECTATOR, "=== HUD === IS_SPECTATOR_HUD_TARGET_LIST_ENTRY_VALID - PLAYER SET TO NOT BE SPECTATED - ", thisEntry.sName)
						#ENDIF
						RETURN FALSE
					ENDIF
				ENDIF
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
			//CPRINTLN(DEBUG_SPECTATOR, "=== HUD === IS_SPECTATOR_HUD_TARGET_LIST_ENTRY_VALID - IGNORE THIS CHECK LOCAL PLAYER IS A DEV - PLAYER SET TO NOT BE SPECTATED - ", thisEntry.sName)
			#ENDIF
		ENDIF
	ENDIF
	
	//Block rival Team for LTS (other VS don't add rival team)
	IF Is_Player_Currently_On_MP_LTS_Mission(PLAYER_ID())
	AND NOT IS_ROCKSTAR_DEV()
	AND (NOT IS_BIT_SET(MPSpecGlobals.iBitset, GLOBAL_SPEC_BS_TEAM_VS_JOB_ENTIRE_TEAM_DEAD)
	OR MPSpecGlobals.bFriendlyOtherTeamPlayerAvailableToSpectate)
		IF GET_PLAYER_TEAM(PLAYER_ID()) != -1
			/*#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SPECTATOR, "=== HUD === IS_SPECTATOR_HUD_TARGET_LIST_ENTRY_VALID - LOCAL PLAYER TEAM = ", GET_PLAYER_TEAM(PLAYER_ID()))
			CPRINTLN(DEBUG_SPECTATOR, "=== HUD === IS_SPECTATOR_HUD_TARGET_LIST_ENTRY_VALID - REMOTE PLAYER TEAM = ", GET_PLAYER_TEAM(NETWORK_GET_PLAYER_INDEX_FROM_PED(thisEntry.pedIndex)))
			#ENDIF*/
			IF DOES_ENTITY_EXIST(thisEntry.pedIndex)
				IF GET_PLAYER_TEAM(NETWORK_GET_PLAYER_INDEX_FROM_PED(thisEntry.pedIndex)) != -1
					IF GET_PLAYER_TEAM(PLAYER_ID()) <> GET_PLAYER_TEAM(NETWORK_GET_PLAYER_INDEX_FROM_PED(thisEntry.pedIndex))
					AND NOT DOES_TEAM_LIKE_TEAM(GET_PLAYER_TEAM(PLAYER_ID()), GET_PLAYER_TEAM(NETWORK_GET_PLAYER_INDEX_FROM_PED(thisEntry.pedIndex)))
						#IF IS_DEBUG_BUILD
						//CPRINTLN(DEBUG_SPECTATOR, "=== HUD === IS_SPECTATOR_HUD_TARGET_LIST_ENTRY_VALID - PLAYER IS ON RIVAL TEAM - ", thisEntry.sName)
						#ENDIF
						RETURN FALSE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(thisEntry.pedIndex)
		IF NETWORK_GET_PLAYER_INDEX_FROM_PED(thisEntry.pedIndex) != INVALID_PLAYER_INDEX()
			IF IS_PLAYER_WATCHING_A_MOCAP(NETWORK_GET_PLAYER_INDEX_FROM_PED(thisEntry.pedIndex))
				#IF IS_DEBUG_BUILD
				//CPRINTLN(DEBUG_SPECTATOR, "=== HUD === IS_SPECTATOR_HUD_TARGET_LIST_ENTRY_VALID - PLAYER IS WATCHING MOCAP - ", thisEntry.sName)
				#ENDIF
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	IF SPEC_IS_PED_ACTIVE(thisEntry.pedIndex)
		#IF IS_DEBUG_BUILD
		//CPRINTLN(DEBUG_SPECTATOR, "=== HUD === IS_SPECTATOR_HUD_TARGET_LIST_ENTRY_VALID - PLAYER IS ACTIVE - ", thisEntry.sName)
		#ENDIF
		RETURN TRUE
	ENDIF
	
	#IF IS_DEBUG_BUILD
	//CPRINTLN(DEBUG_SPECTATOR, "=== HUD === IS_SPECTATOR_HUD_TARGET_LIST_ENTRY_VALID - PLAYER NOT ACTIVE - ", thisEntry.sName)
	#ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Sets the currently focus target equal to an entry in the target list
/// PARAMS:
///    specData - 
///    iListID - list of the entry to copy
///    iEntryID - entry to copy
PROC SET_SPECTATOR_HUD_CURRENT_FOCUS_TARGET_FROM_LIST(SPECTATOR_DATA_STRUCT &specData, INT iListID, INT iEntryID)
	IF iEntryID > -1 AND iEntryID < SPEC_HUD_TARGET_LIST_SIZE
		IF IS_SPECTATOR_HUD_TARGET_LIST_ENTRY_VALID(specData.specHUDData.specHUDList[iListID].Entry[iEntryID])
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SPECTATOR, "=== HUD === SET_SPECTATOR_HUD_CURRENT_FOCUS_TARGET_FROM_LIST iListID = ", iListID, ", iEntryID = ", iEntryID)
			#ENDIF
			specData.specHUDData.specHUDCurrentFocusTarget = specData.specHUDData.specHUDList[iListID].Entry[iEntryID]
			SET_SPECTATOR_DESIRED_FOCUS_PED(specData, specData.specHUDData.specHUDList[iListID].Entry[iEntryID].pedIndex)
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_SPECTATOR, "=== HUD === SET_SPECTATOR_HUD_CURRENT_FOCUS_TARGET_FROM_LIST requested array entry outside of array iListID = ", iListID, ", iEntryID = ", iEntryID)
		#ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Set the currently highlighted entry of the on-screen list
/// PARAMS:
///    specHUDData - 
///    iValue - entry of the list to highlight
PROC SET_SPECTATOR_HUD_CURRENTLY_HIGHLIGHTED_ROW(SPECTATOR_HUD_DATA &specHUDData, INT iValue)
	specHUDData.iCurrentlyHighlightedRow = iValue
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_SPECTATOR, "=== HUD === SET_SPECTATOR_HUD_CURRENTLY_HIGHLIGHTED_ROW - NEW HIGHLIGHTED ROW VALUE ", specHUDData.iCurrentlyHighlightedRow)
	#ENDIF
ENDPROC

/// PURPOSE:
///    Adds iIncrement to the currently highlighted entry of the on-screen list
/// PARAMS:
///    specHUDData - 
///    iIncrement - value to increment by
PROC INCREMENT_SPECTATOR_HUD_CURRENTLY_HIGHLIGHTED_ROW(SPECTATOR_HUD_DATA &specHUDData, INT iIncrement)
	specHUDData.iCurrentlyHighlightedRow += iIncrement
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_SPECTATOR, "=== HUD === INCREMENT_SPECTATOR_HUD_CURRENTLY_HIGHLIGHTED_ROW - NEW HIGHLIGHTED ROW VALUE ", specHUDData.iCurrentlyHighlightedRow)
	#ENDIF
ENDPROC

/// PURPOSE:
///    Gets the currently highlighted entry of the on-screen list
/// PARAMS:
///    specHUDData - 
/// RETURNS:
///    on-screen row ID in spectator list
FUNC INT GET_SPECTATOR_HUD_CURRENTLY_HIGHLIGHTED_ROW(SPECTATOR_HUD_DATA &specHUDData)
	RETURN specHUDData.iCurrentlyHighlightedRow
ENDFUNC

/// PURPOSE:
///    Gets the specific list id and entry id for the currently highlighted spectator target
/// PARAMS:
///    specHUDData - 
///    returnList - stores the list ID of the entry
///    returnEntry - returns the entry ID within returnList's list ID
/// RETURNS:
///    true if the list entry is found
FUNC BOOL GET_SPECTATOR_HUD_CURRENTLY_HIGHLIGHTED_TARGET_LIST_AND_ENTRY_ID(SPECTATOR_HUD_DATA &specHUDData, INT &returnList, INT &returnEntry)
	INT iTarget = specHUDData.iCurrentlyHighlightedRow + (specHUDData.iCurrentPage*specHUDData.iNumberOfTargetsOnAPage)
	
	IF iTarget < 0
		iTarget = 0
	ENDIF
	
	IF GET_SPECTATOR_HUD_TARGET_LIST_AND_ENTRY_ID(specHUDData, returnList, returnEntry, iTarget)
		RETURN TRUE
	ENDIF
		
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Returns the currently highlighted entry in the target lists
/// PARAMS:
///    specHUDData - 
/// RETURNS:
///    specHUDTargetListEntry of the highlighted entry
FUNC specHUDTargetListEntry GET_SPECTATOR_HUD_CURRENTLY_HIGHLIGHTED_TARGET_LIST_ENTRY(SPECTATOR_HUD_DATA &specHUDData)
	INT iList, iEntry
	GET_SPECTATOR_HUD_CURRENTLY_HIGHLIGHTED_TARGET_LIST_AND_ENTRY_ID(specHUDData, iList, iEntry)
	IF iEntry < 0
		iEntry = 0
	ENDIF
	RETURN specHUDData.specHUDList[iList].Entry[iEntry]
ENDFUNC

/// PURPOSE:
///    Returns the ped index of the currently highlighted list entry
/// PARAMS:
///    specHUDData - 
/// RETURNS:
///    PED_INDEX of the entry
FUNC PED_INDEX GET_SPECTATOR_HUD_CURRENTLY_HIGHLIGHTED_PED_INDEX(SPECTATOR_HUD_DATA &specHUDData)
	specHUDTargetListEntry thisEntry
	thisEntry = GET_SPECTATOR_HUD_CURRENTLY_HIGHLIGHTED_TARGET_LIST_ENTRY(specHUDData)
	RETURN thisEntry.pedIndex
ENDFUNC

/// PURPOSE:
///    Compares two mission data structs, returns true if they are the same
/// PARAMS:
///    missionData0 - 
///    missionData1 - 
/// RETURNS:
///    true/false
FUNC BOOL IS_MISSION_DATA_THE_SAME(MP_MISSION_DATA missionData0, MP_MISSION_DATA missionData1)
	
	IF missionData0.mdID.idMission <> missionData1.mdID.idMission
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SPECTATOR, "=== HUD === missionData0.mission <> missionData1.mission")
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF missionData0.iInstanceId <> missionData1.iInstanceId
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SPECTATOR, 	"=== HUD === missionData0.iInstanceId = ", missionData0.iInstanceId, 
										" <> missionData1.iInstanceId = ", missionData1.iInstanceId)
		#ENDIF
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Returns the text label for a mission type
/// PARAMS:
///    iThisFMMCMissionType - FMMC type ID, as defined in mp_globals_fm.sch
/// RETURNS:
///    text label as a string
FUNC STRING GET_SPEC_CAM_TEXT_LABLE_FOR_MISSION_TYPE(INT iThisFMMCMissionType)

	SWITCH iThisFMMCMissionType
		CASE FMMC_TYPE_DEATHMATCH
			IF IS_KING_OF_THE_HILL()
				RETURN "SPC_TXT_KING"
			ELSE
				RETURN "SPC_TXT_DM"
			ENDIF
		BREAK
		CASE FMMC_TYPE_RACE
			RETURN "SPC_TXT_RACE"
		BREAK
		CASE FMMC_TYPE_SURVIVAL
			RETURN "SPC_TXT_SVVL"
		BREAK
		CASE FMMC_TYPE_MISSION
			IF g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_LTS
				RETURN "SPC_TXT_LTS"
			ELIF g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_CTF
				RETURN "SPC_TXT_CTF"
			ELIF g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_PLANNING
				RETURN "SPC_TXT_PLN"
			ELIF g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_HEIST
				RETURN "SPC_TXT_HST"
			ELIF g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_COOP
				RETURN "FMMC_RSTAR_MCP"
			ELIF g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_VERSUS
				IF IS_LOCAL_PLAYER_ON_A_NEW_AVERSAY()
					RETURN "SPC_TXT_ADV"
				ELSE
					RETURN "SPC_TXT_NVS"
				ENDIF
			ELIF g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_FLOW_MISSION
				RETURN "FMMC_RSTAR_MSL"
			ELSE
				RETURN "SPC_TXT_MIS"
			ENDIF
		BREAK
		CASE FMMC_TYPE_BASE_JUMP
			RETURN "SPC_TXT_BSE"
		BREAK
	ENDSWITCH
	
	RETURN "SPC_TXT_DFLT"
ENDFUNC

/// PURPOSE:
///    Returns if the player is in or has just left a property
/// PARAMS:
///    playerID - 
/// RETURNS:
///    true/false
FUNC BOOL IS_PLAYER_IN_OR_JUST_IN_A_PROPERTY(PLAYER_INDEX playerID)
	IF NATIVE_TO_INT(playerId) > -1
		IF GlobalplayerBD_FM[NATIVE_TO_INT(playerId)].propertyDetails.iCurrentlyInsideProperty <> -1
		OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerId)].propertyDetails.iBS,PROPERTY_BROADCAST_BS_SPEC_WAS_JUST_IN_PROPERTY)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Returns if a player is in a mod shop
/// PARAMS:
///    playerID - 
/// RETURNS:
///    true/false
FUNC BOOL IS_PLAYER_IN_A_MOD_SHOP(PLAYER_INDEX playerID)
	IF NATIVE_TO_INT(playerId) > -1
		IF GlobalplayerBD[NATIVE_TO_INT(playerId)].iCurrentShop > -1
			IF GET_SHOP_TYPE_ENUM(INT_TO_ENUM(SHOP_NAME_ENUM, GlobalplayerBD[NATIVE_TO_INT(playerId)].iCurrentShop)) = SHOP_TYPE_CARMOD
				RETURN TRUE
			ENDIF
		ENDIF
		IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(playerID)].iSpecInfoBitset, SPEC_INFO_BS_IN_MOD_SHOP_TUTORIAL)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Returns if a player is in any shop
/// PARAMS:
///    playerID - 
/// RETURNS:
///    true/false
FUNC BOOL IS_REMOTE_PLAYER_IN_ANY_SHOP(PLAYER_INDEX playerID)
	IF NATIVE_TO_INT(playerId) > -1
		IF GlobalplayerBD[NATIVE_TO_INT(playerId)].iCurrentShop > -1
			RETURN TRUE
		ENDIF
		IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(playerID)].iSpecInfoBitset, SPEC_INFO_BS_IN_MOD_SHOP_TUTORIAL)
			RETURN TRUE
		ENDIF
	ENDIF
	CPRINTLN(DEBUG_SPECTATOR, "GlobalplayerBD[NATIVE_TO_INT(playerId)].iCurrentShop = ", GlobalplayerBD[NATIVE_TO_INT(playerId)].iCurrentShop)
	RETURN FALSE
ENDFUNC

//PURPOSE: Retursn TRUE if we are allowed to SPecatte the passed in ped (currently just checks team)
FUNC BOOL IS_HIGHLIGHTED_PLAYER_VALID_TO_SPECTATE(PLAYER_INDEX playerId)
	IF g_bVSMission																		//If versus mission
		IF NOT Is_Player_Currently_On_MP_LTS_Mission(PLAYER_ID())
			IF NOT IS_PLAYER_SCTV(PLAYER_ID())												//When not on SCTV
			AND NOT DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(PLAYER_ID())						//And I've not JIPped
				IF NOT IS_BIT_SET(MPSpecGlobals.iBitset, GLOBAL_SPEC_BS_TEAM_VS_JOB_ENTIRE_TEAM_DEAD)	//If not is entire team dead
				OR MPSpecGlobals.bFriendlyOtherTeamPlayerAvailableToSpectate
					IF GET_PLAYER_TEAM(playerId) <> -1
						IF NOT IS_BIT_SET(MPSpecGlobals.SCTVData.iBitSet, SCTV_BIT_ALLOW_SPECTATING_RIVAL_TEAMS)
						AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[GET_PLAYER_TEAM(playerId)].iTeamBitset2, ciBS2_OTHER_TEAM_CAN_SPECTATE)
							IF GET_PLAYER_TEAM(PLAYER_ID()) <> GET_PLAYER_TEAM(playerId)	//If not on same team
							AND NOT DOES_TEAM_LIKE_TEAM(GET_PLAYER_TEAM(PlayerId), GET_PLAYER_TEAM(PLAYER_ID()))
								#IF IS_DEBUG_BUILD
								IF IS_KEYBOARD_KEY_PRESSED(KEY_9)
									CPRINTLN(DEBUG_SPECTATOR, " IS_HIGHLIGHTED_PLAYER_VALID_TO_SPECTATE = FALSE")
								ENDIF
								#ENDIF
								RETURN FALSE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC LBD_SUB_MODE GET_JOB_ICON()

	IF IS_KING_OF_THE_HILL()
		RETURN SUB_KOTH
	ENDIF	

	//FREEMODE
	IF NOT IS_PLAYER_ON_ANY_FM_MISSION_THAT_IS_NOT_GANG_ATTACK(PLAYER_ID())
		RETURN SUB_FREEMODE
	ENDIF
	
	//DEATHMATCH
	IF IS_PLAYER_ON_ANY_DEATHMATCH(PLAYER_ID())
		IF g_bFM_ON_TEAM_DEATHMATCH
			RETURN SUB_DM_TEAM
		ENDIF
		IF g_bFM_ON_VEH_DEATHMATCH
			RETURN SUB_DM_VEH
		ENDIF
		RETURN SUB_DM_FFA
	ENDIF
	
	//RACE
	IF IS_PLAYER_ON_ANY_RACE(PLAYER_ID())
		IF g_b_IsRallyRace
			RETURN SUB_RACE_RALLY
		ENDIF
		IF g_b_IsGTARace
			RETURN SUB_RACE_GTA
		ENDIF
		IF g_b_On_BASEJUMP_RACE
			RETURN SUB_RACE_BASEJUMP
		ENDIF
		RETURN SUB_RACE_NORMAL
	ENDIF
	
	//MISSION
	IF IS_PLAYER_PLAYING_OR_PLANNING_HEIST(PLAYER_ID())
		RETURN SUB_MISSIONS_HEIST
	ENDIF
	
	IF g_FinalRoundsLbd
		RETURN SUB_FINAL_ROUNDS
	ENDIF

	IF Is_Player_Currently_On_MP_LTS_Mission(PLAYER_ID())
		RETURN SUB_MISSIONS_LTS
	ENDIF
	
	IF Is_Player_Currently_On_MP_CTF_Mission(PLAYER_ID())
		RETURN SUB_MISSIONS_CTF
	ENDIF
	
	IF Is_Player_Currently_On_MP_Versus_Mission(PLAYER_ID())
		RETURN SUB_MISSIONS_VERSUS
	ENDIF
	
	RETURN SUB_MISSIONS_NORMAL
ENDFUNC

/// PURPOSE:
///    Gets if the ped passed is allowed to be viewed in the spectator camera based on scripts flags and alive checks
/// PARAMS:
///    ped - ped to evaluate
///    specCamMode - spectator cam mode to test
///    bSpectateLocalPlayer - true if the local player could be a spectator target
///    bTransitionBackToPlayer - true if we're transitioning back to the local player (most likely as part of cleanup)
///    bCheckTutorial - check the tutorial session of the ped as part of evaluation
/// RETURNS:
///    true/false
FUNC BOOL CHECK_PED_ALLOWED_TO_BE_VIEWED_BY_SETUP(	PED_INDEX ped, eSPEC_CAM_MODE specCamMode, BOOL bSpectateLocalPlayer, BOOL bTransitionBackToPlayer, 
													BOOL bCheckTutorial = TRUE)
	
	#IF IS_DEBUG_BUILD
	BOOL bDebugPrintFailReason = FALSE		//set this true to debug spam the reason why any player is not appearing in the list
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_CanSpecPrintFailReason")
		bDebugPrintFailReason = TRUE
	ENDIF 
	STRING sDebugPlayerName
	#ENDIF
	
	INT iCase = 0
	PLAYER_INDEX playerId
	INT iPlayerNumber 
	
	IF NOT DOES_ENTITY_EXIST(ped)
		#IF IS_DEBUG_BUILD
		IF bDebugPrintFailReason
			CPRINTLN(DEBUG_SPECTATOR, "=== CAN SPEC ", GET_THIS_SCRIPT_NAME(), " FAIL === Ped doesn't exist")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF specCamMode = SPEC_MODE_PLAYERS_STAY_DEAD_EVENT
	OR specCamMode = SPEC_MODE_NEWS
		IF IS_ENTITY_DEAD(ped)
			#IF IS_DEBUG_BUILD
			IF bDebugPrintFailReason
				CPRINTLN(DEBUG_SPECTATOR, "=== CAN SPEC ", GET_THIS_SCRIPT_NAME(), " FAIL === Ped is dead")
			ENDIF
			#ENDIF
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_PED_A_PLAYER(ped)
		iCase = 1

	ENDIF
	
	
	
	
	SWITCH iCase
	
		CASE 0
			
			RETURN TRUE
			
		BREAK
		
		CASE 1
			
			playerId = NETWORK_GET_PLAYER_INDEX_FROM_PED(ped)
			
			iPlayerNumber = NATIVE_TO_INT(playerId)
			
			//Keep this first, otherwise g_bAllPlayersSCTVProcess is skipped and incorrectly true
			//Detect what the player has flagged themselves as being
			IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(playerID)].iSpecInfoBitset, SPEC_INFO_BS_IS_SCTV)
				
				#IF IS_DEBUG_BUILD
				IF bDebugPrintFailReason
					CPRINTLN(DEBUG_SPECTATOR, "=== CAN SPEC ", GET_THIS_SCRIPT_NAME(), " FAIL === ", sDebugPlayerName, " is SCTV")
				ENDIF
				#ENDIF
				RETURN FALSE
				
			ELSE
			
				g_bAllPlayersSCTVProcess = FALSE
				
				IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(playerID)].iSpecInfoBitset, SPEC_INFO_BS_IS_PLAYER)
					#IF IS_DEBUG_BUILD
					IF bDebugPrintFailReason
						CPRINTLN(DEBUG_SPECTATOR, "=== CAN SPEC ", GET_THIS_SCRIPT_NAME(), " FAIL === ", sDebugPlayerName, " is not clarified being a player")
					ENDIF
					#ENDIF
					RETURN FALSE
				ENDIF
				
			ENDIF
			
			// If local player is watching TV, and spec focus has strippers in office
			IF g_bMPTVplayerWatchingTV
			AND IS_BIT_SET(GlobalplayerBD_FM_2[iPlayerNumber].iMissionDataBitSetTwo, ciMISSION_DATA_STRIPPER_HIDE_HUD_CUTSCENE)
				#IF IS_DEBUG_BUILD
				IF bDebugPrintFailReason
					CPRINTLN(DEBUG_SPECTATOR, "=== CAN SPEC ", GET_THIS_SCRIPT_NAME(), " FAIL === ", GET_PLAYER_NAME(playerId), " has strippers, and local player is using TV")
				ENDIF
				#ENDIF
				RETURN FALSE
			ENDIF
			
			IF IS_THIS_PLAYER_ON_JIP_WARNING(playerId)
				#IF IS_DEBUG_BUILD
				IF bDebugPrintFailReason
					CPRINTLN(DEBUG_SPECTATOR, "=== CAN SPEC ", GET_THIS_SCRIPT_NAME(), " FAIL === Player is on JIP warning.")
				ENDIF
				#ENDIF
				RETURN FALSE
			ENDIF

			IF NOT NETWORK_IS_ACTIVITY_SESSION()
				IF IS_PLAYER_IN_WAREHOUSE(playerId)
					#IF IS_DEBUG_BUILD
					IF bDebugPrintFailReason
						CPRINTLN(DEBUG_SPECTATOR, "=== CAN SPEC ", GET_THIS_SCRIPT_NAME(), " FAIL === Player is in warehouse.")
					ENDIF
					#ENDIF
					RETURN FALSE
				ENDIF
				
				IF IS_PLAYER_IN_FACTORY(playerId)
					#IF IS_DEBUG_BUILD
					IF bDebugPrintFailReason
						CPRINTLN(DEBUG_SPECTATOR, "=== CAN SPEC ", GET_THIS_SCRIPT_NAME(), " FAIL === Player is in factory.")
					ENDIF
					#ENDIF
					RETURN FALSE
				ENDIF
				
				IF IS_PLAYER_IN_IE_GARAGE(playerId)
					#IF IS_DEBUG_BUILD
					IF bDebugPrintFailReason
						CPRINTLN(DEBUG_SPECTATOR, "=== CAN SPEC ", GET_THIS_SCRIPT_NAME(), " FAIL === Player is in IE Garage.")
					ENDIF
					#ENDIF
					RETURN FALSE
				ENDIF
				
				IF IS_PLAYER_IN_HANGAR(playerId)
					#IF IS_DEBUG_BUILD
					IF bDebugPrintFailReason
						CPRINTLN(DEBUG_SPECTATOR, "=== CAN SPEC ", GET_THIS_SCRIPT_NAME(), " FAIL === Player is in hangar.")
					ENDIF
					#ENDIF
					RETURN FALSE
				ENDIF
				
				IF IS_PLAYER_IN_BUNKER(playerId)
					#IF IS_DEBUG_BUILD
					IF bDebugPrintFailReason
						CPRINTLN(DEBUG_SPECTATOR, "=== CAN SPEC ", GET_THIS_SCRIPT_NAME(), " FAIL === Player is in bunker.")
					ENDIF
					#ENDIF
					RETURN FALSE
				ENDIF
				
				IF IS_PLAYER_IN_DEFUNCT_BASE(playerId)
					#IF IS_DEBUG_BUILD
					IF bDebugPrintFailReason
						CPRINTLN(DEBUG_SPECTATOR, "=== CAN SPEC ", GET_THIS_SCRIPT_NAME(), " FAIL === Player is in facility.")
					ENDIF
					#ENDIF
					RETURN FALSE
				ENDIF
				
				IF IS_PLAYER_IN_BUSINESS_HUB(playerId)
					#IF IS_DEBUG_BUILD
					IF bDebugPrintFailReason
						CPRINTLN(DEBUG_SPECTATOR, "=== CAN SPEC ", GET_THIS_SCRIPT_NAME(), " FAIL === Player is in business hub.")
					ENDIF
					#ENDIF
					RETURN FALSE
				ENDIF
				
				IF IS_PLAYER_IN_DEFUNCT_BASE(playerId)
					#IF IS_DEBUG_BUILD
					IF bDebugPrintFailReason
						CPRINTLN(DEBUG_SPECTATOR, "=== CAN SPEC ", GET_THIS_SCRIPT_NAME(), " FAIL === Player is in defunct base .")
					ENDIF
					#ENDIF
					RETURN FALSE
				ENDIF
				
				IF IS_PLAYER_IN_CASINO(playerId)
					#IF IS_DEBUG_BUILD
					IF bDebugPrintFailReason
						CPRINTLN(DEBUG_SPECTATOR, "=== CAN SPEC ", GET_THIS_SCRIPT_NAME(), " FAIL === Player is in casino .")
					ENDIF
					#ENDIF
					RETURN FALSE
				ENDIF
				
				IF IS_PLAYER_IN_CASINO_APARTMENT(playerId)
					#IF IS_DEBUG_BUILD
					IF bDebugPrintFailReason
						CPRINTLN(DEBUG_SPECTATOR, "=== CAN SPEC ", GET_THIS_SCRIPT_NAME(), " FAIL === Player is in casino apartment .")
					ENDIF
					#ENDIF
					RETURN FALSE
				ENDIF
				
				#IF FEATURE_CASINO_HEIST
				IF IS_PLAYER_IN_ARCADE_PROPERTY(playerId)
					#IF IS_DEBUG_BUILD
					IF bDebugPrintFailReason
						CPRINTLN(DEBUG_SPECTATOR, "=== CAN SPEC ", GET_THIS_SCRIPT_NAME(), " FAIL === Player is in arcade .")
					ENDIF
					#ENDIF
					RETURN FALSE
				ENDIF
				#ENDIF
				
				IF playerId != INVALID_PLAYER_INDEX()
					IF IS_PLAYER_IN_ARMORY_TRUCK(playerId)
						IF IS_OWNERS_ARMORY_TRUCK_INSIDE_BUNKER(globalPlayerBD[NATIVE_TO_INT(playerId)].SimpleInteriorBD.propertyOwner)
							#IF IS_DEBUG_BUILD
							IF bDebugPrintFailReason
								CPRINTLN(DEBUG_SPECTATOR, "=== CAN SPEC ", GET_THIS_SCRIPT_NAME(), " FAIL === Player is truck which is parked in bunker .")
							ENDIF
							#ENDIF
							RETURN FALSE
						ENDIF
					ENDIF
					
					IF IS_PLAYER_IN_ARMORY_AIRCRAFT(playerId)
						IF IS_OWNERS_ARMORY_AIRCRAFT_INSIDE_DEFUNCT_BASE(globalPlayerBD[NATIVE_TO_INT(playerId)].SimpleInteriorBD.propertyOwner)
							#IF IS_DEBUG_BUILD
							IF bDebugPrintFailReason
								CPRINTLN(DEBUG_SPECTATOR, "=== CAN SPEC ", GET_THIS_SCRIPT_NAME(), " FAIL === Player is avenger which is parked in base .")
							ENDIF
							#ENDIF
							RETURN FALSE
						ENDIF
					ENDIF
					
					IF IS_PLAYER_IN_HACKER_TRUCK(playerId)
						IF IS_OWNERS_HACKER_TRUCK_INSIDE_BUSINESS_HUB(globalPlayerBD[NATIVE_TO_INT(playerId)].SimpleInteriorBD.propertyOwner)
							#IF IS_DEBUG_BUILD
							IF bDebugPrintFailReason
								CPRINTLN(DEBUG_SPECTATOR, "=== CAN SPEC ", GET_THIS_SCRIPT_NAME(), " FAIL === Player is hacker truck which is parked in business hub .")
							ENDIF
							#ENDIF
							RETURN FALSE
						ENDIF
					ENDIF
				ENDIF	
				
			ENDIF
			
			

			IF NOT IS_NET_PLAYER_OK(playerId, FALSE, TRUE)
				#IF IS_DEBUG_BUILD
				IF bDebugPrintFailReason
					CPRINTLN(DEBUG_SPECTATOR, "=== CAN SPEC ", GET_THIS_SCRIPT_NAME(), " FAIL === Player is not ok")
				ENDIF
				#ENDIF
				RETURN FALSE
			ELSE
				#IF IS_DEBUG_BUILD
				sDebugPlayerName = GET_PLAYER_NAME(playerId)
				#ENDIF
			ENDIF
			
			IF NATIVE_TO_INT(playerID) != -1
				IF NOT HAS_PLAYER_COMPLETED_RACE_AND_DM_TUTORIAL(playerId)
				AND NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].iFmTutProgBitset, biTrigTut_PlayerRunningActivityTutorial)
					#IF IS_DEBUG_BUILD
					IF bDebugPrintFailReason
						CPRINTLN(DEBUG_SPECTATOR, "=== CAN SPEC ", GET_THIS_SCRIPT_NAME(), " FAIL === ", sDebugPlayerName, " has not completed race and DM tutorial")
					ENDIF
					#ENDIF
					RETURN FALSE
				ENDIF
			ENDIF
					
			//Bonus safety check for SCTV
			IF IS_PLAYER_SCTV(PlayerID)
				#IF IS_DEBUG_BUILD
				IF bDebugPrintFailReason
					CPRINTLN(DEBUG_SPECTATOR, "=== CAN SPEC ", GET_THIS_SCRIPT_NAME(), " FAIL === ", sDebugPlayerName, " is IS_PLAYER_SCTV")
				ENDIF
				#ENDIF
				RETURN(FALSE)
			ENDIF
			
			IF IS_MP_DECORATOR_BIT_SET(playerId, MP_DECORATOR_BS_INVALID_TO_SPECTATE)
			AND NOT IS_MISSION_OVER_AND_ON_LB_FOR_SCTV(playerId)
				IF NOT IS_PLAYER_SCTV(PLAYER_ID())	//block this flag for SCTV
				OR IS_PLAYER_ON_ANY_FM_JOB(PLAYER_ID())//unless we are on a job
				OR GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(playerId) // Added 30/10/2015 for url:bugstar:2579559.
					#IF IS_DEBUG_BUILD
					IF bDebugPrintFailReason
						CPRINTLN(DEBUG_SPECTATOR, "=== CAN SPEC ", GET_THIS_SCRIPT_NAME(), " FAIL === ", sDebugPlayerName, " invalid to spectate decorator is set")
					ENDIF
					#ENDIF
					RETURN FALSE
				ENDIF
			ENDIF
			
			IF DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(playerId)
				#IF IS_DEBUG_BUILD
				IF bDebugPrintFailReason
					CPRINTLN(DEBUG_SPECTATOR, "=== CAN SPEC ", GET_THIS_SCRIPT_NAME(), " FAIL === ", sDebugPlayerName, " joined mission as spectator")
				ENDIF
				#ENDIF
				RETURN FALSE
			ENDIF
			
			IF IS_PLAYER_SPECTATING(playerId)
				#IF IS_DEBUG_BUILD
				IF bDebugPrintFailReason
					CPRINTLN(DEBUG_SPECTATOR, "=== CAN SPEC ", GET_THIS_SCRIPT_NAME(), " FAIL === ", sDebugPlayerName, " is spectating")
				ENDIF
				#ENDIF
				RETURN FALSE
			ENDIF
			
			IF IS_BIT_SET(GlobalplayerBD_FM_3[NATIVE_TO_INT(playerId)].iFmAmbientEventBitSet, ciSET_PLAYER_IS_PENNED_IN_SPECTATOR)
				#IF IS_DEBUG_BUILD
				IF bDebugPrintFailReason
					CPRINTLN(DEBUG_SPECTATOR, "=== CAN SPEC ", GET_THIS_SCRIPT_NAME(), " FAIL === ", sDebugPlayerName, " is spectating penned in")
				ENDIF
				#ENDIF
				RETURN FALSE
			ENDIF
			
			IF IS_BIT_SET(globalPlayerBD_FM_3[NATIVE_TO_INT(playerID)].officeHeliDockBD.iBS, BS_HELI_DOCK_GLOBAL_PLAYER_BD_DOING_CUTSCENE)
				#IF IS_DEBUG_BUILD
				IF bDebugPrintFailReason
					CPRINTLN(DEBUG_SPECTATOR, "=== CAN SPEC ", GET_THIS_SCRIPT_NAME(), " FAIL === ", sDebugPlayerName, " is BS_HELI_DOCK_GLOBAL_PLAYER_BD_DOING_CUTSCENE")
				ENDIF
				#ENDIF
				RETURN FALSE
			ENDIF
			
			IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(playerID)].iSpecInfoBitset, SPEC_INFO_BS_IS_PLAYER_BIGFOOT)
				IF NOT IS_PLAYER_DEV_SPECTATOR(PLAYER_ID())
					#IF IS_DEBUG_BUILD
					IF bDebugPrintFailReason
						CPRINTLN(DEBUG_SPECTATOR, "=== CAN SPEC ", GET_THIS_SCRIPT_NAME(), " FAIL === ", sDebugPlayerName, " Player is bigfoot, can't be spectated ")
					ENDIF
					#ENDIF
					RETURN FALSE
				ELSE
					#IF IS_DEBUG_BUILD
					IF bDebugPrintFailReason
						CPRINTLN(DEBUG_SPECTATOR, "=== CAN SPEC ", GET_THIS_SCRIPT_NAME(), " PASS === ", sDebugPlayerName, " Player is bigfoot, but spectator is dev. Allowing spectate. ")
					ENDIF
					#ENDIF
				ENDIF				
			ENDIF
			
			IF IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
				IF NOT IS_PLAYER_ON_ANY_FM_MISSION(playerID)
					#IF IS_DEBUG_BUILD
					IF bDebugPrintFailReason
						CPRINTLN(DEBUG_SPECTATOR, "=== CAN SPEC ", GET_THIS_SCRIPT_NAME(), " FAIL === ", sDebugPlayerName, " is not on the same mission.")
					ENDIF
					#ENDIF
					RETURN FALSE
				ENDIF
			ENDIF
			
			#IF FEATURE_HEIST_ISLAND
			IF IS_PLAYER_IN_WARP_TRANSITION(playerId)
				#IF IS_DEBUG_BUILD
				IF bDebugPrintFailReason
					CPRINTLN(DEBUG_SPECTATOR, "=== CAN SPEC ", GET_THIS_SCRIPT_NAME(), " FAIL === ", sDebugPlayerName, " player is in a warp transition")
				ENDIF
				#ENDIF
				RETURN FALSE
			ENDIF
			//IF HEIST_ISLAND_TRAVEL__IS_TRANSITION_IN_PROGRESS()
			
			IF NOT NETWORK_IS_ACTIVITY_SESSION()
				IF IS_PLAYER_ON_HEIST_ISLAND(PLAYER_ID())
					IF NOT IS_PLAYER_ON_HEIST_ISLAND(playerID)
						#IF IS_DEBUG_BUILD
						IF bDebugPrintFailReason
							CPRINTLN(DEBUG_SPECTATOR, "=== CAN SPEC ", GET_THIS_SCRIPT_NAME(), " FAIL === ", sDebugPlayerName, " is not in same island state 1")
						ENDIF
						#ENDIF
						RETURN FALSE
					ELSE
						IF NOT NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), playerId)
							#IF IS_DEBUG_BUILD
							IF bDebugPrintFailReason
								CPRINTLN(DEBUG_SPECTATOR, "=== CAN SPEC ", GET_THIS_SCRIPT_NAME(), " FAIL === ", sDebugPlayerName, " is not in same island state 3 - different island instances")
							ENDIF
							#ENDIF
							RETURN FALSE
						ENDIF
					ENDIF
				ELSE
					IF IS_PLAYER_ON_HEIST_ISLAND(playerID)
						#IF IS_DEBUG_BUILD
						IF bDebugPrintFailReason
							CPRINTLN(DEBUG_SPECTATOR, "=== CAN SPEC ", GET_THIS_SCRIPT_NAME(), " FAIL === ", sDebugPlayerName, " is not in same island state 2")
						ENDIF
						#ENDIF
						RETURN FALSE
					ENDIF
				ENDIF
			ENDIF
			#ENDIF
	
			
			IF specCamMode = SPEC_MODE_NEWS
			
				IF playerId = PLAYER_ID()
					#IF IS_DEBUG_BUILD
					IF bDebugPrintFailReason
						CPRINTLN(DEBUG_SPECTATOR, "=== CAN SPEC ", GET_THIS_SCRIPT_NAME(), " FAIL === Spectator channel, ", sDebugPlayerName, " is local player")
					ENDIF
					#ENDIF
					RETURN FALSE
				ENDIF

				IF GET_PLAYER_WANTED_LEVEL(playerId) < 1
					/*#IF IS_DEBUG_BUILD
					IF bDebugPrintFailReason
						CPRINTLN(DEBUG_SPECTATOR, "=== CAN SPEC ", GET_THIS_SCRIPT_NAME(), " FAIL === ", sDebugPlayerName, " wanted level is too low")
					ENDIF
					#ENDIF*/
					RETURN FALSE
				ENDIF
				
				IF GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].iCurrentMissionType = FMMC_TYPE_MISSION   
				OR GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].iCurrentMissionType = FMMC_TYPE_DEATHMATCH
				OR GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].iCurrentMissionType = FMMC_TYPE_RACE
				OR GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].iCurrentMissionType = FMMC_TYPE_SURVIVAL
				OR GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].iCurrentMissionType = FMMC_TYPE_IMPROMPTU_DM
				OR GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].iCurrentMissionType = FMMC_TYPE_BASE_JUMP
				OR GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].iCurrentMissionType = FMMC_TYPE_ARMS_SMUGGLING
				OR GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].iCurrentMissionType = FMMC_TYPE_MG_GOLF
				OR GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].iCurrentMissionType = FMMC_TYPE_MG_TENNIS
				OR GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].iCurrentMissionType = FMMC_TYPE_MG_SHOOTING_RANGE
				OR GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].iCurrentMissionType = FMMC_TYPE_MG_DARTS
				OR GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].iCurrentMissionType = FMMC_TYPE_MG_ARM_WRESTLING
				OR GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].iCurrentMissionType = FMMC_TYPE_CINEMA
				OR GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].iCurrentMissionType = FMMC_TYPE_CAR_MOD
				OR GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].iCurrentMissionType = FMMC_TYPE_CUSTOM_CAR_GARAGE
				OR GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].iCurrentMissionType = FMMC_TYPE_GB_BOSS_DEATHMATCH
					#IF IS_DEBUG_BUILD
					IF bDebugPrintFailReason
						CPRINTLN(DEBUG_SPECTATOR, "=== CAN SPEC ", GET_THIS_SCRIPT_NAME(), " FAIL === ", sDebugPlayerName, " current mission type is not supported")
					ENDIF
					#ENDIF
					RETURN FALSE
				ENDIF
				
			ELSE
				
				IF playerId = PLAYER_ID()
					IF specCamMode = SPEC_MODE_TV_CHANNEL
						#IF IS_DEBUG_BUILD
						IF bDebugPrintFailReason
							CPRINTLN(DEBUG_SPECTATOR, "=== CAN SPEC ", GET_THIS_SCRIPT_NAME(), " FAIL === Spectator channel, ", sDebugPlayerName, " is local player")
						ENDIF
						#ENDIF
						RETURN FALSE
					ENDIF
					IF specCamMode = SPEC_MODE_HEIST
						#IF IS_DEBUG_BUILD
						IF bDebugPrintFailReason
							CPRINTLN(DEBUG_SPECTATOR, "=== CAN SPEC ", GET_THIS_SCRIPT_NAME(), " FAIL === Spectator Heist, ", sDebugPlayerName, " is local player")
						ENDIF
						#ENDIF
						RETURN FALSE
					ENDIF
					IF bSpectateLocalPlayer
					OR bTransitionBackToPlayer
						#IF IS_DEBUG_BUILD
						IF bDebugPrintFailReason
							CPRINTLN(DEBUG_SPECTATOR, "=== CAN SPEC ", GET_THIS_SCRIPT_NAME(), " PASS === ", sDebugPlayerName, " is local player and we are returning to local player")
						ENDIF
						#ENDIF
						RETURN TRUE
					ELSE
						#IF IS_DEBUG_BUILD
						IF bDebugPrintFailReason
							CPRINTLN(DEBUG_SPECTATOR, "=== CAN SPEC ", GET_THIS_SCRIPT_NAME(), " FAIL === ", sDebugPlayerName, " is local player")
						ENDIF
						#ENDIF
						RETURN FALSE
					ENDIF
				ENDIF
				
				
				IF DOES_ENTITY_EXIST(ped)
				AND NOT IS_ENTITY_DEAD(ped)
					VECTOR vPedZCheck 
					
					vPedZCheck = GET_ENTITY_COORDS(ped, FALSE)
					IF vPedZCheck.z < -200.0
					AND NOT IS_THIS_PLAYER_IN_CORONA(playerID)
					AND NOT IS_PLAYER_IN_WAREHOUSE(playerID)
					AND NOT IS_PLAYER_IN_BUNKER(playerID)
					AND NOT IS_PLAYER_IN_HANGAR(playerID)
					AND NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_SLASHERS(g_FMMC_STRUCT.iAdversaryModeType)
					AND NOT IS_PLAYER_IN_DEFUNCT_BASE(playerID)
						#IF IS_DEBUG_BUILD
						IF bDebugPrintFailReason
						CPRINTLN(DEBUG_SPECTATOR, "=== CAN SPEC ", GET_THIS_SCRIPT_NAME(), " FAIL === ", sDebugPlayerName, "'s z-coordinate is at ", vPedZCheck.z, ". Camera is probably under the map too.")
						ENDIF
						#ENDIF
						RETURN FALSE
					ENDIF
				ENDIF
				
				//Beyond here if definitely not this machine's player ped
				
				IF specCamMode = SPEC_MODE_NEWS
				OR specCamMode = SPEC_MODE_TV_CHANNEL
				 OR specCamMode = SPEC_MODE_HEIST 
					IF IS_THIS_PLAYER_IN_CORONA(playerId)
						#IF IS_DEBUG_BUILD
						IF bDebugPrintFailReason
							CPRINTLN(DEBUG_SPECTATOR, "=== CAN SPEC ", GET_THIS_SCRIPT_NAME(), " FAIL === ", sDebugPlayerName, " is in corona")
						ENDIF
						#ENDIF
						RETURN FALSE
					ENDIF
				ENDIF
				
				IF bCheckTutorial
					IF NOT IS_THIS_PLAYER_IN_CORONA(playerId)
						IF NOT NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), playerId)		//Are players on different tutorial sessions
							IF IS_PLAYER_SCTV(PLAYER_ID())						//If I'm SCTV
							AND NOT NETWORK_IS_IN_TUTORIAL_SESSION() 			//And I'm not in a tutorial
							AND (IS_PLAYER_IN_OR_JUST_IN_A_PROPERTY(playerId)	//And target is in acceptable tutorial
								OR IS_PLAYER_IN_A_MOD_SHOP(playerId))
								
								//It's fine!
								
							ELSE	//Otherwise fail
								#IF IS_DEBUG_BUILD
								IF bDebugPrintFailReason
									CPRINTLN(DEBUG_SPECTATOR, "=== CAN SPEC ", GET_THIS_SCRIPT_NAME(), " FAIL === ", sDebugPlayerName, " is in different tutorial session")
								ENDIF
								#ENDIF
								RETURN FALSE
							ENDIF
						ENDIF
					ENDIF
					
					IF iPlayerNumber > -1
						
						IF IS_BIT_SET(GlobalplayerBD_FM[iPlayerNumber].iFmTutProgBitset, biTrigTut_PlayerRunningActivityTutorial) 
							#IF IS_DEBUG_BUILD
							IF bDebugPrintFailReason
								CPRINTLN(DEBUG_SPECTATOR, "=== CAN SPEC ", GET_THIS_SCRIPT_NAME(), " FAIL === ", sDebugPlayerName, " is in the tutorial")
							ENDIF
							#ENDIF
							RETURN FALSE
						ENDIF
					ENDIF
					
				ENDIF
				
			ENDIF
			
			
			
			
			#IF IS_DEBUG_BUILD
			IF bDebugPrintFailReason
				IF IS_STRING_NULL_OR_EMPTY(sDebugPlayerName)
					CPRINTLN(DEBUG_SPECTATOR, "=== CAN SPEC ", GET_THIS_SCRIPT_NAME(), " PASS === Ped is acceptable to spectate")
				ELSE
					CPRINTLN(DEBUG_SPECTATOR, "=== CAN SPEC ", GET_THIS_SCRIPT_NAME(), " PASS === ", sDebugPlayerName, " is acceptable to spectate")
				ENDIF
			ENDIF
			#ENDIF
			
			RETURN TRUE
			
		BREAK
		
	ENDSWITCH
	
	NET_SCRIPT_ASSERT("Spectator camera viewable ped checks, return value not coming from iCase switch?")NET_NL()
	RETURN FALSE
	
ENDFUNC


/// PURPOSE:
///    Gets if the ped passed is allowed to be viewed in the spectator camera based on scripts flags and alive checks.
/// PARAMS:
///    ped - ped to evaluate.
/// RETURNS:
///    true/false
FUNC BOOL IS_PED_ALLOWED_TO_BE_VIEWED_BY_SPEC_DATA(SPECTATOR_DATA_STRUCT &specData, PED_INDEX ped, BOOL bCheckTutorial = TRUE)
	IF CHECK_PED_ALLOWED_TO_BE_VIEWED_BY_SETUP(	ped, 
												specData.SpecCamData.eMode,
												IS_BIT_SET(specData.specCamData.iActivateBitset, ASCF_INDEX_SPECTATE_LOCAL),
												IS_BIT_SET(specData.specCamData.iDeactivateBitset, DSCF_INDEX_FADE_BACK_TO_PLAYER),
												bCheckTutorial)
		RETURN TRUE
	ENDIF
		
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Gets if the ped passed is allowed to be viewed in the spectator camera based on scripts flags and alive checks.
/// PARAMS:
///    ped - ped to evaluate.
/// RETURNS:
///    true/false
FUNC BOOL IS_PED_ALLOWED_TO_BE_VIEWED_BY_LOCAL_PLAYER(PED_INDEX ped, eSPEC_CAM_MODE specCamMode)
	IF CHECK_PED_ALLOWED_TO_BE_VIEWED_BY_SETUP(	ped, 
												specCamMode,
												FALSE,
												FALSE)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Expensive, burns through the entire player list to see if any of the players can be spetated by the local machine
/// RETURNS:
///    true/false
FUNC BOOL IS_VALID_SPECTATE_TARGET_IN_SESSION()
	INT iPlayer
	REPEAT NUM_NETWORK_PLAYERS iPlayer
		IF IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(iPlayer), FALSE)
			IF IS_PED_ALLOWED_TO_BE_VIEWED_BY_LOCAL_PLAYER(GET_PLAYER_PED(INT_TO_PLAYERINDEX(iPlayer)), SPEC_MODE_PLAYERS_RESPAWN_EVENT)
				#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_SPECTATOR, "=== MP_TV SERVER === found spectate channel valid target : ", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(iPlayer)))
				#ENDIF
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Returns if there is at least one player that is allowed to be viewed by the spectator cam
/// PARAMS:
///    specCamMode - 
/// RETURNS:
///    true/false
FUNC BOOL IS_ANY_PLAYER_ALLOWED_TO_BE_VIEWED_BY_LOCAL_PLAYER(eSPEC_CAM_MODE specCamMode)
	
	INT i
	
	REPEAT NUM_NETWORK_PLAYERS i
		
		IF IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(i), FALSE, FALSE)
			
			IF IS_PED_ALLOWED_TO_BE_VIEWED_BY_LOCAL_PLAYER(GET_PLAYER_PED(INT_TO_PLAYERINDEX(i)), specCamMode)
				RETURN TRUE
			ENDIF
			
		ENDIF
		
	ENDREPEAT
	
	RETURN FALSE
	
ENDFUNC

/// PURPOSE:
///    Searches through the spectate target lists for a ped index and returns the list and entry ID of the first entry found with that ped
/// PARAMS:
///    specHUDData - 
///    returnList - stores list ID
///    returnEntry - stores entry ID
///    targetPed - ped to search for
/// RETURNS:
///    true if it finds the ped
FUNC BOOL FIND_PED_IN_SPECTATOR_HUD_TARGET_LISTS(SPECTATOR_HUD_DATA &specHUDData, INT &returnList, INT &returnEntry, PED_INDEX targetPed)
	
	INT i, j
	PED_INDEX thisPed
	
	REPEAT NUMBER_OF_SPEC_HUD_TARGET_LISTS j
		REPEAT SPEC_HUD_TARGET_LIST_SIZE i
			thisPed = GET_SPECTATOR_HUD_TARGET_LIST_ENTRY_PED_ID(specHUDData, j, i)
			IF DOES_ENTITY_EXIST(thisPed)
				IF thisPed = targetPed
					returnList = j
					returnEntry = i
					RETURN TRUE
				ENDIF
			ENDIF
		ENDREPEAT
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Get a valid entry from a specfic Spectator HUD target list
/// PARAMS:
///    specData - 
///    thisList - list to search through
///    returnEntry - store entry ID
///    bDifferentFromCurrentFocus - true if the valid entry has to be different from the current spectate target
///    bDifferentFromDesiredFocus - true if the valid entry has to be different from the spectate target the camera is switching to
/// RETURNS:
///    true if an entry is found
FUNC BOOL GET_SPECTATOR_HUD_CURRENTLY_VALID_ENTRY_FROM_LIST(SPECTATOR_DATA_STRUCT &specData, INT thisList, INT &returnEntry, 
															BOOL bDifferentFromCurrentFocus = FALSE, BOOL bDifferentFromDesiredFocus = FALSE)
	
	#IF IS_DEBUG_BUILD
	BOOL bPrintDebug = FALSE
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_GetValidSpecPrintDebug")
		bPrintDebug = TRUE
	ENDIF 
	#ENDIF
	
	INT i
	
	INT iBestEntry = -1
	BOOL bBestEntryIsOnSameTeam = FALSE
	BOOL bCurrentEntryIsOnSameTeam = FALSE
	BOOL bSCTVFoundMenuTarget = FALSE
	BOOL bCurrentEntryIsOnFriendlyTeam = FALSE
	BOOL bBestEntryIsOnFriendlyTeam = FALSE
	
	PED_INDEX pedProcess
	
	#IF IS_DEBUG_BUILD
	IF bPrintDebug
	CPRINTLN(DEBUG_SPECTATOR, "=== HUD === ***SPAM*** GET_SPECTATOR_HUD_CURRENTLY_VALID_ENTRY_FROM_LIST start")
	ENDIF
	#ENDIF
	
	REPEAT SPEC_HUD_TARGET_LIST_SIZE i
	
		#IF IS_DEBUG_BUILD
		IF bPrintDebug
		CPRINTLN(DEBUG_SPECTATOR, "=== HUD === ***SPAM*** testing list entry i = ", i)
		ENDIF
		#ENDIF
	
		bCurrentEntryIsOnSameTeam = FALSE
		bCurrentEntryIsOnFriendlyTeam = FALSE
		
		IF IS_SPECTATOR_HUD_TARGET_LIST_ENTRY_VALID(specData.specHUDData.specHUDList[thisList].Entry[i])
			pedProcess = GET_SPECTATOR_HUD_TARGET_LIST_ENTRY_PED_ID(specData.specHUDData, thisList, i)
			IF IS_PED_ALLOWED_TO_BE_VIEWED_BY_SPEC_DATA(specData, pedProcess)
				IF SPEC_IS_PED_ACTIVE(pedProcess)
					
					#IF IS_DEBUG_BUILD
					IF bPrintDebug
					CPRINTLN(DEBUG_SPECTATOR, "=== HUD === ***SPAM*** i = ", i, " is valid and viable")
					ENDIF
					#ENDIF
					
					IF IS_PLAYER_SCTV(PLAYER_ID())		//if we're just starting SCTV, try and find the player we selected to spectate
						IF NOT bSCTVFoundMenuTarget
							
							#IF IS_DEBUG_BUILD
							IF bPrintDebug
							CPRINTLN(DEBUG_SPECTATOR, 	"=== HUD === ***SPAM*** local player is SCTV, not found target ",
														g_tl23SCTVTargetName)
							ENDIF
							#ENDIF
							
							IF IS_PED_A_PLAYER(pedProcess)
								IF IS_NET_PLAYER_OK(NETWORK_GET_PLAYER_INDEX_FROM_PED(pedProcess))
								
									#IF IS_DEBUG_BUILD
									IF bPrintDebug
									CPRINTLN(DEBUG_SPECTATOR, 	"=== HUD === ***SPAM*** process is called ", 
																GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX_FROM_PED(pedProcess)),
																" sctv player is called ", g_tl23SCTVTargetName)
									ENDIF
									#ENDIF
									
									IF ARE_STRINGS_EQUAL(g_tl23SCTVTargetName, GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX_FROM_PED(pedProcess)))
									
										#IF IS_DEBUG_BUILD
										IF bPrintDebug
										CPRINTLN(DEBUG_SPECTATOR, "=== HUD === ***SPAM*** this is sctv target player")
										ENDIF
										#ENDIF
										
										bSCTVFoundMenuTarget = TRUE	//this is the person we selected to spectate in the menu
										iBestEntry = i
									
									ELSE
									
										#IF IS_DEBUG_BUILD
										IF bPrintDebug
										CPRINTLN(DEBUG_SPECTATOR, "=== HUD === ***SPAM*** not sctv target player")
										ENDIF
										#ENDIF
									
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF NOT IS_PLAYER_SCTV(PLAYER_ID())	//If we're not an SCTV searching for their menu selection
					OR NOT bSCTVFoundMenuTarget
					
						#IF IS_DEBUG_BUILD
						IF bPrintDebug
						IF NOT IS_PLAYER_SCTV(PLAYER_ID())
							CPRINTLN(DEBUG_SPECTATOR, "=== HUD === ***SPAM*** local player is not SCTV")
						ENDIF
						IF NOT bSCTVFoundMenuTarget
							CPRINTLN(DEBUG_SPECTATOR, "=== HUD === ***SPAM*** local player is SCTV but not found SCTV target")
						ENDIF
						ENDIF
						#ENDIF
					
						IF ((NOT bDifferentFromCurrentFocus)	//If don't care about being different from focus, or focus is different
						OR (pedProcess <> GET_SPECTATOR_CURRENT_FOCUS_PED()))
							
							#IF IS_DEBUG_BUILD
							IF bPrintDebug
							IF NOT bDifferentFromCurrentFocus
								CPRINTLN(DEBUG_SPECTATOR, "=== HUD === ***SPAM*** don't care about current focus")
							ENDIF
							IF pedProcess <> GET_SPECTATOR_CURRENT_FOCUS_PED()
								CPRINTLN(DEBUG_SPECTATOR, "=== HUD === ***SPAM*** test target is different from current focus")
							ENDIF
							ENDIF
							#ENDIF
							
							IF ((NOT bDifferentFromDesiredFocus)	//If don't care about being different from desired, or desired is different
							OR (pedProcess <> GET_SPECTATOR_DESIRED_FOCUS_PED()))
								
								#IF IS_DEBUG_BUILD
								IF bPrintDebug
								IF NOT bDifferentFromDesiredFocus
									CPRINTLN(DEBUG_SPECTATOR, "=== HUD === ***SPAM*** don't care about desired focus")
								ENDIF
								IF pedProcess <> GET_SPECTATOR_DESIRED_FOCUS_PED()
									CPRINTLN(DEBUG_SPECTATOR, "=== HUD === ***SPAM*** test target is different from desired focus")
								ENDIF
								ENDIF
								#ENDIF
								
								IF IS_PED_A_PLAYER(pedProcess)
									IF IS_NET_PLAYER_OK(NETWORK_GET_PLAYER_INDEX_FROM_PED(pedProcess))
									
										#IF IS_DEBUG_BUILD
										IF bPrintDebug
										CPRINTLN(DEBUG_SPECTATOR, 	"=== HUD === ***SPAM*** process is called ", 
																	GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX_FROM_PED(pedProcess)))
										ENDIF
										#ENDIF
									
										IF GET_PLAYER_TEAM(NETWORK_GET_PLAYER_INDEX_FROM_PED(pedProcess)) = GET_PLAYER_TEAM(PLAYER_ID())
											bCurrentEntryIsOnSameTeam = TRUE	//take note if target is on same team
										ELIF DOES_TEAM_LIKE_TEAM(GET_PLAYER_TEAM(NETWORK_GET_PLAYER_INDEX_FROM_PED(pedProcess)), GET_PLAYER_TEAM(PLAYER_ID()))
											bCurrentEntryIsOnFriendlyTeam = TRUE
										ENDIF
									ENDIF
								ENDIF
								
								IF iBestEntry <= -1
								OR ((NOT bBestEntryIsOnSameTeam) AND bCurrentEntryIsOnSameTeam)	//tend towards targets on the same team
								OR ((NOT bBestEntryIsOnSameTeam) AND bCurrentEntryIsOnFriendlyTeam)
									iBestEntry = i
									bBestEntryIsOnSameTeam = bCurrentEntryIsOnSameTeam
									bBestEntryIsOnFriendlyTeam = bCurrentEntryIsOnFriendlyTeam
									
									// Release compiler issue. Don't want to make this debug only, may need it.
									bBestEntryIsOnFriendlyTeam = bBestEntryIsOnFriendlyTeam
									
									#IF IS_DEBUG_BUILD
									IF bPrintDebug
									CPRINTLN(DEBUG_SPECTATOR, 	"=== HUD === ***SPAM*** iBestEntry = ", iBestEntry, 
																" bBestEntryIsOnSameTeam = ", bBestEntryIsOnSameTeam,
																" bBestEntryIsOnFriendlyTeam = ", bBestEntryIsOnFriendlyTeam)
									ENDIF
									#ENDIF
									
								ENDIF
								
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF iBestEntry > -1
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_SPECTATOR, 	"=== HUD === GET_SPECTATOR_HUD_CURRENTLY_VALID_ENTRY_FROM_LIST returning entry ", iBestEntry, 
									" bBestEntryIsOnSameTeam = ", bBestEntryIsOnSameTeam,
									" bBestEntryIsOnFriendlyTeam = ", bBestEntryIsOnFriendlyTeam)
		#ENDIF
	
		returnEntry = iBestEntry
		RETURN TRUE
	/*ELSE
		//clear SCTV stored target name if they're not here
		IF IS_PLAYER_SCTV(PLAYER_ID())
			IF NOT IS_STRING_NULL_OR_EMPTY(g_tl23SCTVTargetName)
				TEXT_LABEL_23 tl23Null
				g_tl23SCTVTargetName = tl23Null
			ENDIF
		ENDIF*/
	ENDIF
	
	RETURN FALSE
ENDFUNC

//returns if we're on a LTS
FUNC BOOL IS_SPECTATING_PLAYER_ON_LTS()
	IF IS_PLAYER_SCTV(PLAYER_ID())
		IF NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_SPECTATOR_CURRENT_FOCUS_PED()) != INVALID_PLAYER_INDEX()
		AND NETWORK_IS_PLAYER_ACTIVE(NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_SPECTATOR_CURRENT_FOCUS_PED()))
			RETURN Is_Player_Currently_On_MP_LTS_Mission(NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_SPECTATOR_CURRENT_FOCUS_PED()))
		ELIF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].playerSCTVTarget != INVALID_PLAYER_INDEX()
		AND NETWORK_IS_PLAYER_ACTIVE(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].playerSCTVTarget)
			RETURN Is_Player_Currently_On_MP_LTS_Mission(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].playerSCTVTarget)
		ENDIF
	ELSE
		RETURN Is_Player_Currently_On_MP_LTS_Mission(PLAYER_ID())
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Get a valid entry from the Spectator target list
/// PARAMS:
///    specData - 
///    returnList - stores the list ID
///    returnEntry - stores the entry ID
///    bDifferentFromCurrentFocus - entry has to be different from the current spectator target
///    bDifferentFromDesiredFocus - entry has to be different from the spectator target the cam is moving to
///    bInclineTowardsCurrentList - any valid entry in the same list as the current spectator target should be chosen over other lists
/// RETURNS:
///    true if successful
FUNC BOOL GET_SPECTATOR_HUD_CURRENTLY_VALID_LISTS_ENTRY(SPECTATOR_DATA_STRUCT &specData, INT &returnList, INT &returnEntry, 
														BOOL bDifferentFromCurrentFocus = FALSE, BOOL bDifferentFromDesiredFocus = FALSE, 
														BOOL bInclineTowardsCurrentList = FALSE)
	INT i
	INT iFoundEntry
	
	INT iCurrentFocusList = -1
	INT iDesiredFocusList = -1
	
	IF bInclineTowardsCurrentList		//if we care about finding something in our current spectator target's list
		IF DOES_ENTITY_EXIST(GET_SPECTATOR_CURRENT_FOCUS_PED())
			IF FIND_PED_IN_SPECTATOR_HUD_TARGET_LISTS(specData.specHUDData, iCurrentFocusList, i, GET_SPECTATOR_CURRENT_FOCUS_PED())
				IF GET_SPECTATOR_HUD_CURRENTLY_VALID_ENTRY_FROM_LIST(specData, iCurrentFocusList, iFoundEntry, bDifferentFromCurrentFocus, bDifferentFromDesiredFocus)
					returnList = iCurrentFocusList
					returnEntry = iFoundEntry
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
		IF DOES_ENTITY_EXIST(GET_SPECTATOR_DESIRED_FOCUS_PED())
			IF FIND_PED_IN_SPECTATOR_HUD_TARGET_LISTS(specData.specHUDData, iDesiredFocusList, i, GET_SPECTATOR_DESIRED_FOCUS_PED())
				IF GET_SPECTATOR_HUD_CURRENTLY_VALID_ENTRY_FROM_LIST(specData, iDesiredFocusList, iFoundEntry, bDifferentFromCurrentFocus, bDifferentFromDesiredFocus)
					returnList = iDesiredFocusList
					returnEntry = iFoundEntry
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	REPEAT NUMBER_OF_SPEC_HUD_TARGET_LISTS i
		IF i <> iCurrentFocusList	//dont recheck the incline lists
		AND i <> iDesiredFocusList
			IF GET_SPECTATOR_HUD_CURRENTLY_VALID_ENTRY_FROM_LIST(specData, i, iFoundEntry, bDifferentFromCurrentFocus, bDifferentFromDesiredFocus)
				returnList = i
				returnEntry = iFoundEntry
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_RACE)
	OR IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_BASE_JUMP)
	OR (IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_MISSION) AND IS_SPECTATING_PLAYER_ON_LTS())
		IF NOT HAS_NET_TIMER_STARTED(specData.stRaceNoOneTimOut)
			START_NET_TIMER(specData.stRaceNoOneTimOut)
			RETURN TRUE
		ELIF HAS_NET_TIMER_EXPIRED(specData.stRaceNoOneTimOut, 5000)
			RETURN FALSE
		ELSE
			RETURN TRUE
		ENDIF	
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Sets the spectator HUD's stage and debug prints the change has happened
/// PARAMS:
///    specHUDData - 
///    thisStage - stage to change to
PROC SET_SPECTATOR_HUD_STAGE(SPECTATOR_HUD_DATA &specHUDData, eSpecHUDStageList thisStage)
	#IF IS_DEBUG_BUILD
	SWITCH thisStage
		CASE SPEC_HUD_STAGE_INIT
			CPRINTLN(DEBUG_SPECTATOR, "=== HUD === SET_SPECTATOR_HUD_STAGE = SPEC_HUD_STAGE_INIT")
		BREAK
		CASE SPEC_HUD_STAGE_FULL_LIST
			CPRINTLN(DEBUG_SPECTATOR, "=== HUD === SET_SPECTATOR_HUD_STAGE = SPEC_HUD_STAGE_FULL_LIST")
		BREAK
		CASE SPEC_HUD_STAGE_TARGET_FOCUS
			CPRINTLN(DEBUG_SPECTATOR, "=== HUD === SET_SPECTATOR_HUD_STAGE = SPEC_HUD_STAGE_TARGET_FOCUS")
		BREAK
		CASE SPEC_HUD_STAGE_LEADERBOARD
			CPRINTLN(DEBUG_SPECTATOR, "=== HUD === SET_SPECTATOR_HUD_STAGE = SPEC_HUD_STAGE_LEADERBOARD")
		BREAK
		CASE SPEC_HUD_STAGE_HELI
			CPRINTLN(DEBUG_SPECTATOR, "=== HUD === SET_SPECTATOR_HUD_STAGE = SPEC_HUD_STAGE_HELI")
		BREAK
		CASE SPEC_HUD_STAGE_QUIT_YES_NO
			CPRINTLN(DEBUG_SPECTATOR, "=== HUD === SET_SPECTATOR_HUD_STAGE = SPEC_HUD_STAGE_QUIT_YES_NO")
		BREAK
		CASE SPEC_HUD_STAGE_DEV_ONLY
			CPRINTLN(DEBUG_SPECTATOR, "=== HUD === SET_SPECTATOR_HUD_STAGE = SPEC_HUD_STAGE_DEV_ONLY")
		BREAK
		DEFAULT
			CPRINTLN(DEBUG_SPECTATOR, "=== HUD === SET_SPECTATOR_HUD_STAGE = DEFAULT")
		BREAK
	ENDSWITCH
	#ENDIF
	
	SET_BIT(specHUDData.iBitset, SPEC_HUD_BS_REQUEST_REDO_SUMMARY_CARD)		//Update HUD display of lists
	
	specHUDData.eSpecHUDStage = thisStage
ENDPROC

/// PURPOSE:
///    Gets the spectator HUD's stage
/// PARAMS:
///    specHUDData - 
/// RETURNS:
///    the eSpecHUDStageList of the stage
FUNC eSpecHUDStageList GET_SPECTATOR_HUD_STAGE(SPECTATOR_HUD_DATA &specHUDData)
	RETURN specHUDData.eSpecHUDStage
ENDFUNC

/// PURPOSE:
///    Empties a specHUDTargetListEntry
/// PARAMS:
///    thisEntry - entry to empty
PROC FLUSH_SPECTATOR_HUD_TARGET_LIST_ENTRY(specHUDTargetListEntry &thisEntry)
	thisEntry.pedIndex = NULL
	thisEntry.iRank = 0
	thisEntry.playerID = INVALID_PLAYER_INDEX()
ENDPROC

/// PURPOSE:
///    Empties a specHUDTargetList
/// PARAMS:
///    thisList - list to empty
PROC FLUSH_SPECTATOR_HUD_TARGET_LIST(specHUDTargetList &thisList)
	INT i
	REPEAT SPEC_HUD_TARGET_LIST_SIZE i
		FLUSH_SPECTATOR_HUD_TARGET_LIST_ENTRY(thisList.Entry[i])
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Clear out all the entire spectator HUD target lists
/// PARAMS:
///    specHUDData - 
PROC FLUSH_SPECTATOR_HUD_TARGET_LISTS(SPECTATOR_HUD_DATA &specHUDData)
	INT i
	
	REPEAT NUMBER_OF_SPEC_HUD_TARGET_LISTS i
		FLUSH_SPECTATOR_HUD_TARGET_LIST(specHUDData.specHUDList[i])
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Resets the variables required in the spectator HUD
/// PARAMS:
///    specHUDData - 
PROC RESET_SPECTATOR_HUD_VARIABLES(SPECTATOR_HUD_DATA &specHUDData)
	specHUDData.iCurrentPage = 0
	SET_SPECTATOR_HUD_STAGE(specHUDData, SPEC_HUD_STAGE_INIT)
	FLUSH_SPECTATOR_HUD_TARGET_LISTS(specHUDData)
	specHUDData.specHUDCurrentFocusTarget = specHUDData.specHUDList[0].Entry[0]
ENDPROC

/// PURPOSE:
///    Disables spectator hud rendering and input
/// PARAMS:
///    specHUDData - 
PROC DISABLE_SPECTATOR_HUD(SPECTATOR_HUD_DATA &specHUDData)
	IF NOT IS_BIT_SET(specHUDData.iBitset, SPEC_HUD_BS_DISABLED)
		SET_BIT(specHUDData.iBitset, SPEC_HUD_BS_DISABLED)
		CLEAR_BIT(specHUDData.iBitset, SPEC_HUD_BS_ACTIVE)
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_SPECTATOR, "=== HUD === SET_BIT(specHUDData.iBitset, SPEC_HUD_BS_DISABLED)")
		#ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Enables spectator hud rendering and input
/// PARAMS:
///    specHUDData - 
PROC ENABLE_SPECTATOR_HUD(SPECTATOR_HUD_DATA &specHUDData)
	IF IS_BIT_SET(specHUDData.iBitset, SPEC_HUD_BS_DISABLED)
		CLEAR_BIT(specHUDData.iBitset, SPEC_HUD_BS_DISABLED)
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_SPECTATOR, "=== HUD === CLEAR_BIT(specHUDData.iBitset, SPEC_HUD_BS_DISABLED)")
		#ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Populates an entry in a spectator list with a ped's information
/// PARAMS:
///    specHUDData - 
///    thisPed - ped to populate with
///    thisList - list to populate
///    thisName - string to display or text label to use when rendering the entry in the spectator list
/// RETURNS:
///    true if successful
//FUNC BOOL PRIVATE_REGISTER_PED_WITH_SPEC_LIST(SPECTATOR_HUD_DATA &specHUDData, PED_INDEX thisPed, INT thisList, STRING thisName)
//	IF DOES_ENTITY_EXIST(thisPed)
//		IF NOT IS_PED_INJURED(thisPed)
//			IF NOT IS_PED_A_PLAYER(thisPed)
//				INT i
//				SWITCH thisList
//					CASE SPEC_HUD_TARGET_LIST_GENERIC_ENEMIES
//						REPEAT SPEC_HUD_TARGET_LIST_SIZE i
//							IF specHUDData.GenericEnemyTarget.Entry[i].pedIndex = NULL
//								specHUDData.GenericEnemyTarget.Entry[i].pedIndex = thisPed
//								specHUDData.GenericEnemyTarget.Entry[i].iRank = -1
//								//specHUDData.GenericEnemyTarget.Entry[i].sName = "SPEC_ENEMY"//Enemy
//								SET_BIT(MPSpecGlobals.iBitset, GLOBAL_SPEC_BS_NEED_TO_PROCESS_LIST_INSTANTLY)//Force list to instantly populate
//								#IF IS_DEBUG_BUILD
//								CPRINTLN(DEBUG_SPECTATOR, "=== HUD === SPEC_HUD_TARGET_LIST_GENERIC_ENEMIES - Ped registered, i = ", i)
//								#ENDIF
//								RETURN TRUE
//							ENDIF
//						ENDREPEAT
//						#IF IS_DEBUG_BUILD
//						CPRINTLN(DEBUG_SPECTATOR, "=== HUD === PRIVATE_REGISTER_PED_WITH_SPEC_LIST - No space in generic enemy list")
//						#ENDIF
//					BREAK
//					CASE SPEC_HUD_TARGET_LIST_SPECIAL_ENEMIES
//						REPEAT SPEC_HUD_TARGET_LIST_SIZE i
//							IF specHUDData.SpecialEnemyTarget.Entry[i].pedIndex = NULL
//								specHUDData.SpecialEnemyTarget.Entry[i].pedIndex = thisPed
//								specHUDData.SpecialEnemyTarget.Entry[i].iRank = -1
//								//specHUDData.SpecialEnemyTarget.Entry[i].sName = thisName
//								SET_BIT(MPSpecGlobals.iBitset, GLOBAL_SPEC_BS_NEED_TO_PROCESS_LIST_INSTANTLY)//Force list to instantly populate
//								#IF IS_DEBUG_BUILD
//								CPRINTLN(DEBUG_SPECTATOR, "=== HUD === SPEC_HUD_TARGET_LIST_SPECIAL_ENEMIES - Ped registered, i = ", i)
//								#ENDIF
//								RETURN TRUE
//							ENDIF
//						ENDREPEAT
//						#IF IS_DEBUG_BUILD
//						CPRINTLN(DEBUG_SPECTATOR, "=== HUD === PRIVATE_REGISTER_PED_WITH_SPEC_LIST - No space in special enemy list")
//						#ENDIF
//					BREAK
//					CASE SPEC_HUD_TARGET_LIST_GENERIC_ALLIES
//						REPEAT SPEC_HUD_TARGET_LIST_SIZE i
//							IF specHUDData.GenericAllyTarget.Entry[i].pedIndex = NULL
//								specHUDData.GenericAllyTarget.Entry[i].pedIndex = thisPed
//								specHUDData.GenericAllyTarget.Entry[i].iRank = -1
//								//specHUDData.GenericAllyTarget.Entry[i].sName = "SPEC_ALLY"//Ally
//								SET_BIT(MPSpecGlobals.iBitset, GLOBAL_SPEC_BS_NEED_TO_PROCESS_LIST_INSTANTLY)//Force list to instantly populate
//								#IF IS_DEBUG_BUILD
//								CPRINTLN(DEBUG_SPECTATOR, "=== HUD === SPEC_HUD_TARGET_LIST_GENERIC_ALLIES - Ped registered, i = ", i)
//								#ENDIF
//								RETURN TRUE
//							ENDIF
//						ENDREPEAT
//						#IF IS_DEBUG_BUILD
//						CPRINTLN(DEBUG_SPECTATOR, "=== HUD === PRIVATE_REGISTER_PED_WITH_SPEC_LIST - No space in generic ally list")
//						#ENDIF
//					BREAK
//					CASE SPEC_HUD_TARGET_LIST_SPECIAL_ALLIES
//						REPEAT SPEC_HUD_TARGET_LIST_SIZE i
//							IF specHUDData.SpecialAllyTarget.Entry[i].pedIndex = NULL
//								specHUDData.SpecialAllyTarget.Entry[i].pedIndex = thisPed
//								specHUDData.SpecialAllyTarget.Entry[i].iRank = -1
//								//specHUDData.SpecialAllyTarget.Entry[i].sName = thisName
//								SET_BIT(MPSpecGlobals.iBitset, GLOBAL_SPEC_BS_NEED_TO_PROCESS_LIST_INSTANTLY)//Force list to instantly populate
//								#IF IS_DEBUG_BUILD
//								CPRINTLN(DEBUG_SPECTATOR, "=== HUD === SPEC_HUD_TARGET_LIST_SPECIAL_ALLIES - Ped registered, i = ", i)
//								#ENDIF
//								RETURN TRUE
//							ENDIF
//						ENDREPEAT
//						#IF IS_DEBUG_BUILD
//						CPRINTLN(DEBUG_SPECTATOR, "=== HUD === PRIVATE_REGISTER_PED_WITH_SPEC_LIST - No space in special ally list")
//						#ENDIF
//					BREAK
//				ENDSWITCH
//			ELSE
//				#IF IS_DEBUG_BUILD
//				CPRINTLN(DEBUG_SPECTATOR, "=== HUD === PRIVATE_REGISTER_PED_WITH_SPEC_LIST - Do not register players")
//				#ENDIF
//			ENDIF
//		ELSE
//			#IF IS_DEBUG_BUILD
//			CPRINTLN(DEBUG_SPECTATOR, "=== HUD === PRIVATE_REGISTER_PED_WITH_SPEC_LIST - Ped is not alive")
//			#ENDIF
//		ENDIF
//	ELSE
//		#IF IS_DEBUG_BUILD
//		CPRINTLN(DEBUG_SPECTATOR, "=== HUD === PRIVATE_REGISTER_PED_WITH_SPEC_LIST - Ped does not exist")
//		#ENDIF
//	ENDIF
//	
//	RETURN FALSE
//ENDFUNC

/// PURPOSE:
///    Registers the ped as a generic enemy to spectate
/// PARAMS:
///    specHUDData - 
///    thisPed - ped to register
/// RETURNS:
///    true if successful
//FUNC BOOL REGISTER_PED_AS_SPECTATOR_TARGET_GENERIC_ENEMY(SPECTATOR_HUD_DATA &specHUDData, PED_INDEX thisPed)
//	IF PRIVATE_REGISTER_PED_WITH_SPEC_LIST(specHUDData, thisPed, SPEC_HUD_TARGET_LIST_GENERIC_ENEMIES, "")
//		RETURN TRUE
//	ENDIF
//	RETURN FALSE
//ENDFUNC
//
///// PURPOSE:
/////    Registers the ped as a special enemy to spectate
///// PARAMS:
/////    specHUDData - 
/////    thisPed - ped to register
///// RETURNS:
/////    true if successful
//FUNC BOOL REGISTER_PED_AS_SPECTATOR_TARGET_SPECIAL_ENEMY(SPECTATOR_HUD_DATA &specHUDData, PED_INDEX thisPed, STRING thisTextLabel)
//	IF PRIVATE_REGISTER_PED_WITH_SPEC_LIST(specHUDData, thisPed, SPEC_HUD_TARGET_LIST_SPECIAL_ENEMIES, thisTextLabel)
//		RETURN TRUE
//	ENDIF
//	RETURN FALSE
//ENDFUNC
//
///// PURPOSE:
/////    Registers the ped as a generic ally to spectate
///// PARAMS:
/////    specHUDData - 
/////    thisPed - ped to register
///// RETURNS:
/////    true if successful
//FUNC BOOL REGISTER_PED_AS_SPECTATOR_TARGET_GENERIC_ALLY(SPECTATOR_HUD_DATA &specHUDData, PED_INDEX thisPed)
//	IF PRIVATE_REGISTER_PED_WITH_SPEC_LIST(specHUDData, thisPed, SPEC_HUD_TARGET_LIST_GENERIC_ALLIES, "")
//		RETURN TRUE
//	ENDIF
//	RETURN FALSE
//ENDFUNC
//
///// PURPOSE:
/////    Registers the ped as a special ally to spectate
///// PARAMS:
/////    specHUDData - 
/////    thisPed - ped to register
///// RETURNS:
/////    true if successful
//FUNC BOOL REGISTER_PED_AS_SPECTATOR_TARGET_SPECIAL_ALLY(SPECTATOR_HUD_DATA &specHUDData, PED_INDEX thisPed, STRING thisTextLabel)
//	IF PRIVATE_REGISTER_PED_WITH_SPEC_LIST(specHUDData, thisPed, SPEC_HUD_TARGET_LIST_SPECIAL_ALLIES, thisTextLabel)
//		RETURN TRUE
//	ENDIF
//	RETURN FALSE
//ENDFUNC

/// PURPOSE:
///    Finds and removes a ped from the spectator target list
/// PARAMS:
///    specHUDData - 
///    thisPed - ped to find and remove
PROC UNREGISTER_PED_WITH_SPECTATOR_TARGET_LISTS(SPECTATOR_HUD_DATA &specHUDData, PED_INDEX thisPed)
	INT i, j
	BOOL bUnregistered
	specHUDTargetListEntry nullEntry
	REPEAT NUMBER_OF_SPEC_HUD_TARGET_LISTS j
		REPEAT SPEC_HUD_TARGET_LIST_SIZE i
			SWITCH j
				CASE SPEC_HUD_TARGET_LIST_GENERIC_ENEMIES
					IF specHUDData.GenericEnemyTarget.Entry[i].pedIndex = thisPed
						specHUDData.GenericEnemyTarget.Entry[i] = nullEntry
						bUnregistered = TRUE
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_SPECTATOR, "=== HUD === thisped : ", NATIVE_TO_INT(thisPed), " unregistered with SPEC_HUD_TARGET_LIST_GENERIC_ENEMIES")
						#ENDIF
					ENDIF
				BREAK
				CASE SPEC_HUD_TARGET_LIST_SPECIAL_ENEMIES
					IF specHUDData.SpecialEnemyTarget.Entry[i].pedIndex = thisPed
						specHUDData.SpecialEnemyTarget.Entry[i] = nullEntry
						bUnregistered = TRUE
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_SPECTATOR, "=== HUD === thisped : ", NATIVE_TO_INT(thisPed), " unregistered with SPEC_HUD_TARGET_LIST_SPECIAL_ENEMIES")
						#ENDIF
					ENDIF
				BREAK
				CASE SPEC_HUD_TARGET_LIST_GENERIC_ALLIES
					IF specHUDData.GenericAllyTarget.Entry[i].pedIndex = thisPed
						specHUDData.GenericAllyTarget.Entry[i] = nullEntry
						bUnregistered = TRUE
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_SPECTATOR, "=== HUD === thisped : ", NATIVE_TO_INT(thisPed), " unregistered with SPEC_HUD_TARGET_LIST_GENERIC_ALLIES")
						#ENDIF
					ENDIF
				BREAK
				CASE SPEC_HUD_TARGET_LIST_SPECIAL_ALLIES
					IF specHUDData.SpecialAllyTarget.Entry[i].pedIndex = thisPed
						specHUDData.SpecialAllyTarget.Entry[i] = nullEntry
						bUnregistered = TRUE
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_SPECTATOR, "=== HUD === thisped : ", NATIVE_TO_INT(thisPed), " unregistered with SPEC_HUD_TARGET_LIST_SPECIAL_ALLIES")
						#ENDIF
					ENDIF
				BREAK
			ENDSWITCH
		ENDREPEAT
	ENDREPEAT
	IF bUnregistered
		SET_BIT(MPSpecGlobals.iBitset, GLOBAL_SPEC_BS_NEED_TO_PROCESS_LIST_INSTANTLY)//Force list to instantly populate
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets the spectator hud mode (how much hud is displayed) and debug prints the change
/// PARAMS:
///    specHUDData - 
///    thisMode - mode to change to
PROC SET_SPEC_HUD_MODE(SPECTATOR_HUD_DATA &specHUDData, SPEC_HUD_MODE thisMode)
	IF specHUDData.specHUDMode <> thisMode
		SWITCH thisMode
			CASE SPEC_HUD_MODE_FULL
				DISABLE_SCRIPT_HUD(HUDPART_ALL_OVERHEADS, FALSE)
			BREAK
			CASE SPEC_HUD_MODE_MINIMAL
				DISABLE_SCRIPT_HUD(HUDPART_ALL_OVERHEADS, FALSE)
			BREAK
			CASE SPEC_HUD_MODE_HIDDEN
				DISABLE_SCRIPT_HUD(HUDPART_ALL_OVERHEADS, TRUE)
			BREAK
		ENDSWITCH
		specHUDData.specHUDMode = thisMode
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_SPECTATOR, "=== HUD === specHUDData.specHUDMode = ", GET_SPEC_HUD_MODE_NAME(thisMode))
		#ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Does every frame processing to maintain the spectator hud mode, usually hiding/revealing hud elements
/// PARAMS:
///    specData - 
PROC PROCESS_SPEC_HUD_MODES(SPECTATOR_DATA_STRUCT &specData)

	IF specData.specHUDData.specHUDMode <> SPEC_HUD_MODE_HIDDEN
		IF IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_HIDDEN)
			CLEAR_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_HIDDEN)
			CPRINTLN(DEBUG_SPECTATOR, "=== HUD === CLEAR_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_HIDDEN)")
		ENDIF
	ENDIF

	SWITCH specData.specHUDData.specHUDMode
		CASE SPEC_HUD_MODE_FULL
			IF GET_SPEC_CAM_MODE(specData.specCamData) <> SPEC_MODE_NEWS
				//HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_SUBTITLE_TEXT)		//hide subtitle text as button list is huge in full mode
				IF NOT IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_OVERHEAD_REQUESTED)
					SET_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_OVERHEAD_REQUESTED)
				ENDIF
				SET_FORCE_SHOW_GPS(TRUE)
			ELSE
				IF IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_OVERHEAD_REQUESTED)
					CLEAR_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_OVERHEAD_REQUESTED)
				ENDIF
			ENDIF
			IF IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_SCTV_TOGGLE_HUD)
				IF IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_MG_GOLF)
					DISPLAY_HUD(TRUE)
					DISPLAY_RADAR(TRUE)
					CLEAR_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_SCTV_TOGGLE_HUD)
					CPRINTLN(DEBUG_SPECTATOR, "=== HUD === GLOBAL_SPEC_BS_SCTV_TOGGLE_HUD CLEARED - A")
				ENDIF
			ENDIF
		BREAK
		CASE SPEC_HUD_MODE_MINIMAL
			IF IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_OVERHEAD_REQUESTED)
				CLEAR_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_OVERHEAD_REQUESTED)
			ENDIF
			IF IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_SCTV_TOGGLE_HUD)
				IF NOT IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_MG_GOLF)
					DISPLAY_HUD(TRUE)
					DISPLAY_RADAR(TRUE)
					SET_FORCE_SHOW_GPS(TRUE)
					CLEAR_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_SCTV_TOGGLE_HUD)
					CPRINTLN(DEBUG_SPECTATOR, "=== HUD === GLOBAL_SPEC_BS_SCTV_TOGGLE_HUD CLEARED - B")
				ENDIF
			ENDIF
		BREAK
		CASE SPEC_HUD_MODE_HIDDEN
			IF IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_OVERHEAD_REQUESTED)
				CLEAR_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_OVERHEAD_REQUESTED)
			ENDIF
			IF GET_SPEC_CAM_MODE(specData.specCamData) <> SPEC_MODE_NEWS
				IF IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_HIDDEN)
					IF NOT IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_MG_GOLF)
						HIDE_HUD_AND_RADAR_THIS_FRAME()
						SET_FORCE_SHOW_GPS(FALSE)
					ELSE
						IF NOT IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_SCTV_TOGGLE_HUD)
							DISPLAY_HUD(FALSE)
							DISPLAY_RADAR(TRUE)
							SET_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_SCTV_TOGGLE_HUD)
							CPRINTLN(DEBUG_SPECTATOR, "=== HUD === GLOBAL_SPEC_BS_SCTV_TOGGLE_HUD SET")
						ENDIF
					ENDIF
					HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WANTED_STARS)
					HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_MP_MESSAGE)
					HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_SUBTITLE_TEXT)
					HIDE_HELP_TEXT_THIS_FRAME()
					THEFEED_HIDE_THIS_FRAME()
				ELSE
					IF GET_SPECTATOR_HUD_STAGE(specData.specHUDData) = SPEC_HUD_STAGE_LEADERBOARD
						SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_REQUEST_REDO_SUMMARY_CARD)		//Update HUD display of lists
						SET_SPECTATOR_HUD_STAGE(specData.specHUDData, specData.specHUDData.eSpecHUDStageVisitedBeforeLeaderboard)
					ENDIF
				
					SET_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_HIDDEN)
					CPRINTLN(DEBUG_SPECTATOR, "=== HUD === SET_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_HIDDEN)")
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Empties all the spectator lists instantly
/// PARAMS:
///    specHUDData - 
PROC CLEAR_ALL_SPECTATOR_TARGET_LISTS(SPECTATOR_HUD_DATA &specHUDData)
	INT i
	specHUDTargetListEntry nullEntry
	REPEAT SPEC_HUD_TARGET_LIST_SIZE i
		specHUDData.GenericEnemyTarget.Entry[i] = nullEntry
		specHUDData.SpecialEnemyTarget.Entry[i] = nullEntry
		specHUDData.GenericAllyTarget.Entry[i] = nullEntry
		specHUDData.SpecialAllyTarget.Entry[i] = nullEntry
	ENDREPEAT
	SET_BIT(MPSpecGlobals.iBitset, GLOBAL_SPEC_BS_NEED_TO_PROCESS_LIST_INSTANTLY)//Force list to instantly populate
ENDPROC

/// PURPOSE:
///    Starts the sound for changing the spectator filter
/// PARAMS:
///    specHUDData - 
PROC SPEC_HUD_SOUND_START_CHANGE_FILTER(SPECTATOR_HUD_DATA &specHUDData)
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_SPECTATOR, "=== HUD === SPEC_HUD_SOUND_START_CHANGE_FILTER()")
	#ENDIF
	IF specHUDData.iSoundChangeFilter = -1
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_SPECTATOR, "=== HUD === PLAY_SOUND_FRONTEND(specHUDData.iSoundChangeFilter, Change_Cam, MP_CCTV_SOUNDSET)")
		#ENDIF
		specHUDData.iSoundChangeFilter = GET_SOUND_ID()
		PLAY_SOUND_FRONTEND(specHUDData.iSoundChangeFilter, "Change_Cam", "MP_CCTV_SOUNDSET")
	ENDIF
ENDPROC

/// PURPOSE:
///    Stops the sound for changing the spectator filter
/// PARAMS:
///    specHUDData - 
PROC SPEC_HUD_SOUND_STOP_CHANGE_FILTER(SPECTATOR_HUD_DATA &specHUDData)
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_SPECTATOR, "=== HUD === SPEC_HUD_SOUND_STOP_CHANGE_FILTER()")
	#ENDIF
	IF specHUDData.iSoundChangeFilter <> -1
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_SPECTATOR, "=== HUD === STOP_SOUND(specHUDData.iSoundChangeFilter)")
		#ENDIF
		STOP_SOUND(specHUDData.iSoundChangeFilter)
		RELEASE_SOUND_ID(specHUDData.iSoundChangeFilter)
		specHUDData.iSoundChangeFilter = -1
	ENDIF
ENDPROC

//PURPOSE: Sets the Spectator Radio as active
PROC TURN_ON_SPECTATOR_RADIO(BOOL bOn)
	IF bOn = TRUE
		IF NOT IS_BIT_SET(MPSpecGlobals.iBailBitset, GLOBAL_SPEC_BAIL_BS_RADIO_ON)
			//g_iMyChosenRadioStation = GET_MP_INT_CHARACTER_STAT(MP_STAT_LAST_PLAYED_CORONA_RADIO)
			SET_PLAYER_SPECTATED_VEHICLE_RADIO_OVERRIDE(TRUE)
			SET_AUDIO_FLAG("AllowRadioDuringSwitch", TRUE) 
			SET_MOBILE_PHONE_RADIO_STATE(TRUE)
			SET_AUDIO_FLAG("MobileRadioInGame", TRUE)
			SET_BIT(MPSpecGlobals.iBailBitset, GLOBAL_SPEC_BAIL_BS_RADIO_ON)
			CPRINTLN(DEBUG_SPECTATOR, "=== HUD === TURN_ON_SPECTATOR_RADIO - GLOBAL_SPEC_BAIL_BS_RADIO_ON SET")
		ENDIF
	ELSE
		IF IS_BIT_SET(MPSpecGlobals.iBailBitset, GLOBAL_SPEC_BAIL_BS_RADIO_ON)
			SET_PLAYER_SPECTATED_VEHICLE_RADIO_OVERRIDE(FALSE)
			SET_AUDIO_FLAG("AllowRadioDuringSwitch", FALSE) 
			SET_MOBILE_PHONE_RADIO_STATE(FALSE)
			SET_AUDIO_FLAG("MobileRadioInGame", FALSE)
			CLEAR_BIT(MPSpecGlobals.iBailBitset, GLOBAL_SPEC_BAIL_BS_RADIO_ON)
			CPRINTLN(DEBUG_SPECTATOR, "=== HUD === TURN_ON_SPECTATOR_RADIO - GLOBAL_SPEC_BAIL_BS_RADIO_ON CLEARED")
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_LOCAL_PLAYER_USING_TRUCK_SCANNER2()
	IF g_hackerTruckScanner.eState = HTSS_IN_CAM 
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_REACTIVE_CASINO_APARTMENT_TCM()
	IF IS_PLAYER_IN_CASINO_APARTMENT(PLAYER_ID())
	AND NOT IS_LOCAL_PLAYER_CASINO_APARTMENT_RENOVATION_ACTIVE()
	AND NOT IS_SKYSWOOP_IN_SKY()
	AND NOT IS_SKYSWOOP_MOVING()
	AND NOT IS_LOCAL_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR()
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_REACTIVE_ARCADE_TCM()
	IF IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
	AND NOT IS_PLAYER_ARCADE_PROPERTY_RENOVATION_ACTIVE(PLAYER_ID())
	AND NOT IS_SKYSWOOP_IN_SKY()
	AND NOT IS_SKYSWOOP_MOVING()
	AND NOT IS_LOCAL_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR()
	AND NOT IS_PLAYER_USING_DRONE(PLAYER_ID())
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Cleans up the spectator cam hud
/// PARAMS:
///    specData - 
PROC CLEANUP_SPECTATOR_CAM_HUD(SPECTATOR_DATA_STRUCT &specData)
	
	Enable_MP_Comms()
	DISABLE_SCRIPT_HUD(HUDPART_RANKBAR, FALSE)
	Enable_all_mp_hud()
	
	// The corona often displays help as you leave so do not clean up this if the following is TRUE
	IF NOT IS_PLAYER_IN_CORONA()
	AND NOT SHOULD_TRANSITION_SESSIONS_CLEANUP_AFTER_CORONA_WALK_OUT()
	AND NOT IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE)
	AND NOT g_bKeepHelpTextWhenCleaningUpSpecHUD
		IF IS_HELP_MESSAGE_BEING_DISPLAYED()
			CLEAR_HELP()
		ENDIF	
	ENDIF
	g_bKeepHelpTextWhenCleaningUpSpecHUD = FALSE
	//CLEAR_PRINTS()
	
	IF IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_SCTV_TOGGLE_HUD)
		DISPLAY_HUD(TRUE)
		DISPLAY_RADAR(TRUE)
		CLEAR_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_SCTV_TOGGLE_HUD)
		CPRINTLN(DEBUG_SPECTATOR, "=== HUD === GLOBAL_SPEC_BS_SCTV_TOGGLE_HUD CLEARED - Z")
	ENDIF
	
	SET_FORCE_SHOW_GPS(FALSE)
	
	RESET_HUD_COMPONENT_VALUES(NEW_HUD_SUBTITLE_TEXT)
	
	TURN_ON_SPECTATOR_RADIO(FALSE)
	CPRINTLN(DEBUG_SPECTATOR, "=== HUD === CLEANUP_SPECTATOR_CAM_HUD - TURN_ON_SPECTATOR_RADIO(FALSE)")
	
	CLEAR_ALL_SPECTATOR_TARGET_LISTS(specData.specHUDData)
	
	IF GET_SPEC_CAM_MODE(specData.specCamData) <> SPEC_MODE_NEWS
		IF NOT IS_BIT_SET(specData.specCamData.iDeactivateBitset, DSCF_INDEX_DONT_CLEANUP_TIMECYCLES)
			//IF NOT IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE)	//REMOVED FOR BUG 2135945
			IF NOT IS_PED_DRUNK(PLAYER_PED_ID())
			AND NOT IS_LOCAL_PLAYER_USING_TRUCK_SCANNER2()
				IF GET_TIMECYCLE_MODIFIER_INDEX() != -1
					CLEAR_TIMECYCLE_MODIFIER()
					
					IF SHOULD_REACTIVE_CASINO_APARTMENT_TCM()
						IF NOT IS_STRING_NULL_OR_EMPTY(g_tlCasinoApartmentTCM)
							SET_TIMECYCLE_MODIFIER(g_tlCasinoApartmentTCM)
						ENDIF
					ELIF SHOULD_REACTIVE_ARCADE_TCM()
						IF NOT IS_STRING_NULL_OR_EMPTY(g_tlArcadeTCM)
							SET_TIMECYCLE_MODIFIER(g_tlArcadeTCM)
						ENDIF
					ENDIF
					
		//			IF IS_PAUSE_MENU_ACTIVE_EX()
		//				CLEAR_PUSHED_TIMECYCLE_MODIFIER()
		//			ENDIF
					PRINTLN("=== HUD === CLEANUP_SPECTATOR_CAM_HUD - called CLEAR_TIMECYCLE_MODIFIER.")
				ELSE
					PRINTLN("=== HUD === CLEANUP_SPECTATOR_CAM_HUD - NO TIMECYCLE MODIFIER - cannot call CLEAR_TIMECYCLE_MODIFIER.")
				ENDIF
			ELSE
				PRINTLN("=== HUD === CLEANUP_SPECTATOR_CAM_HUD - IS_PED_DRUNK is set, cannot call CLEAR_TIMECYCLE_MODIFIER.")
			ENDIF
		ELSE
			PRINTLN("=== HUD === CLEANUP_SPECTATOR_CAM_HUD - LEAVE MODIFIER - DSCF_INDEX_DONT_CLEANUP_TIMECYCLES is set, cannot call CLEAR_TIMECYCLE_MODIFIER.")
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_BLOCK_CHANGING_RADAR)
		DISPLAY_RADAR(TRUE)
	ENDIF
	SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(specData.specHUDData.sfFilter)
	SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(specData.specHUDData.sfSummaryCard)
	
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
	AND (DOES_CURRENT_PED_COMPONENT_HAVE_RESTRICTION_TAG(PLAYER_PED_ID(), PED_COMP_BERD, DLC_RESTRICTION_TAG_THERMAL_VISION)
	OR DOES_CURRENT_PED_PROP_HAVE_RESTRICTION_TAG(PLAYER_PED_ID(), ENUM_TO_INT(ANCHOR_HEAD), DLC_RESTRICTION_TAG_THERMAL_VISION))
		PRINTLN("=== HUD === CLEANUP_SPECTATOR_CAM_HUD - SET_SEETHROUGH - player wearing thermal goggles")
	ELSE
		PRINTLN("=== HUD === CLEANUP_SPECTATOR_CAM_HUD - SET_SEETHROUGH - player NOT wearing thermal goggles, call SET_SEETHROUGH(FALSE)")
		SET_SEETHROUGH(FALSE)
	ENDIF
	
	RESET_SCALEFORM_INSTRUCTIONAL_BUTTON(specData.specHUDData.scaleformInstructionalButtons)
	SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(specData.specHUDData.sfButton)

	
	#IF FEATURE_COPS_N_CROOKS
	RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_ARC1/Arc1_CnC_Weazel_News_Stinger") 
	IF ANIMPOSTFX_IS_RUNNING("CnC_SpectatorTV")
		ANIMPOSTFX_STOP("CnC_SpectatorTV")
	ENDIF
	#ENDIF
	
	specData.specHUDData.eCurrentFilter = SPEC_HUD_FILTER_SPEC_1	//SPEC_HUD_FILTER_NONE
	specData.specHUDData.eDesiredFilter = SPEC_HUD_FILTER_SPEC_1	//SPEC_HUD_FILTER_NONE
	specData.specHUDData.iFilterStage = 99
	specData.specHUDData.iFrameDelay = -1
	
	ENABLE_SPECTATOR_HUD(specData.specHUDData)
	
	SPEC_HUD_SOUND_STOP_CHANGE_FILTER(specData.specHUDData)
	
	SET_SPEC_HUD_MODE(specData.specHUDData, SPEC_HUD_MODE_FULL)
	CLEAR_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_HIDDEN)
	CLEAR_BIT(MPSpecGlobals.iBitset, GLOBAL_SPEC_BS_READY_FOR_LEADERBOARD)
	CLEAR_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_NEWS_FILTER)
	
	CLEAR_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_REQUEST_SPEC_HELP_TEXT)
	
	specData.specHUDData.iCurrentFakeWantedLevel = 0
	SET_FAKE_WANTED_LEVEL(specData.specHUDData.iCurrentFakeWantedLevel)
	CPRINTLN(DEBUG_SPECTATOR, "=== HUD === CLEANUP_SPECTATOR_CAM_HUD - SET_FAKE_WANTED_LEVEL = 0")
	specData.specHUDData.bFakeWantedLevelFlashing = FALSE
	FLASH_WANTED_DISPLAY(specData.specHUDData.bFakeWantedLevelFlashing)
	CPRINTLN(DEBUG_SPECTATOR, "=== HUD === CLEANUP_SPECTATOR_CAM_HUD - FLASH_WANTED_DISPLAY = FALSE")
								
	SET_SPECTATOR_HUD_STAGE(specData.specHUDData, SPEC_HUD_STAGE_INIT)
	
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_SPECTATOR, "=== HUD === hud cleaned up.")
	#ENDIF
	
ENDPROC

FUNC BOOL SWAP_SPECTATOR_LIST_FOR_DPAD()

	IF IS_PLAYER_SPECTATING(PLAYER_ID())

		RETURN IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciSHOW_DPAD_DOWN_IN_HEIST_SPECTATE)
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Populates the spectator target list scaleform
/// PARAMS:
///    specData - 
///    thisStage - current spectator stage
PROC MAINTAIN_SUMMARY_CARD(SPECTATOR_DATA_STRUCT &specData, eSpecHUDStageList thisStage)

	IF specData.specHUDData.sfSummaryCard = NULL
		EXIT
	ENDIF
	
	IF specData.specHUDData.sfSummaryCard = INT_TO_NATIVE(SCALEFORM_INDEX, -1)
		EXIT
	ENDIF
	
	IF NOT HAS_SCALEFORM_MOVIE_LOADED(specData.specHUDData.sfSummaryCard)
		EXIT
	ENDIF
	
	IF GET_SPEC_CAM_MODE(specData.SpecCamData) <> SPEC_MODE_NEWS
	AND (specData.specHUDData.iNumberOfTargetsOnCurrentPage > 0	OR IS_PLAYER_SCTV(PLAYER_ID()))	//and we have someone to draw
		IF IS_BIT_SET(specData.specHUDData.iBitset, SPEC_HUD_BS_REQUEST_REDO_SUMMARY_CARD)		//if we want to completely redo the target list
		OR IS_BIT_SET(specData.specHUDData.iBitset, SPEC_HUD_BS_REQUEST_UPDATE_SUMMARY_CARD)	//or just update some info
			
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SPECTATOR, "=== HUD === MAINTAIN_SUMMARY_CARD")
			#ENDIF
			
			BOOL bUpdateList = TRUE
			IF IS_BIT_SET(specData.specHUDData.iBitset, SPEC_HUD_BS_REQUEST_REDO_SUMMARY_CARD)
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_SPECTATOR, "=== HUD === REDOING SUMMARY CARD")
				#ENDIF
				bUpdateList = FALSE
			ELSE
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_SPECTATOR, "=== HUD === UPDATING SUMMARY CARD")
				#ENDIF
			ENDIF
			
			//If the mission is at the end then only update the list don't refresh (See Bug 2036563)
			IF bUpdateList = FALSE
				IF g_bMissionOver = TRUE
					bUpdateList = TRUE
					CPRINTLN(DEBUG_SPECTATOR, "=== HUD === g_bMissionOver = TRUE - JUST DO UPDATE SUMMARY CARD")
				ENDIF
			ENDIF
			
			IF NOT IS_PLAYER_SCTV(PLAYER_ID())
			OR MPSpecGlobals.SCTVData.currentMode = MPSpecGlobals.SCTVData.switchToMode
				
				/*IF NOT bUpdateList
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_SPECTATOR, "=== HUD === SCALEFORM_SET_DATA_SLOT_EMPTY")
					#ENDIF
					SCALEFORM_SET_DATA_SLOT_EMPTY(specData.specHUDData.sfSummaryCard)		//clean out list
				ENDIF*/
				
				specHUDTargetListEntry listEntry
				HUD_COLOURS hcRowColour
				INT i
				
				TEXT_LABEL_15 tlCrewTag
				STRING strHeadshotTxd
				PEDHEADSHOT_ID pedHeadshot
				PED_INDEX pedFocus
				INT iSpecList, iSpecEntry
				BOOL bFoundFocusPed, bFocusPedIsSpecial
				BOOL bEntryIsFocusPed
				INT iTeam
				INT iPlacing
				
				SWITCH thisStage
				
					CASE SPEC_HUD_STAGE_FULL_LIST
						
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_SPECTATOR, "=== HUD === MAINTAIN_SUMMARY_CARD SPEC_HUD_STAGE_FULL_LIST")
						#ENDIF
						//setup title label for the scaleform
						IF NOT bUpdateList
							IF (NOT IS_PLAYER_SCTV(PLAYER_ID())) OR (GET_SCTV_MODE()  = SCTV_MODE_FOLLOW_CAM)
								SET_SCALEFORM_SUMMARY_CARD_TITLE(specData.specHUDData.sfSummaryCard, "SPEC_HEADER", "", FALSE, GET_JOB_ICON_DPAD(GET_JOB_ICON()))
							ELSE
								IF IS_PLAYER_SCTV(PLAYER_ID())
									IF GET_SCTV_MODE()  = SCTV_MODE_FREE_CAM
										SET_SCALEFORM_SUMMARY_CARD_TITLE(specData.specHUDData.sfSummaryCard, "FREECAM_HEAD", "", FALSE, GET_JOB_ICON_DPAD(GET_JOB_ICON()))
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						//Full list draw
						IF NOT IS_PLAYER_SCTV(PLAYER_ID())
						OR GET_SCTV_MODE() = SCTV_MODE_FOLLOW_CAM
						OR GET_SCTV_MODE() = SCTV_MODE_FREE_CAM // 2353988 added this so the dpad player list (top left) is populated in free cam. Comment out if you don't want any player rows drawn.
							
							IF NOT bUpdateList
								
								bFoundFocusPed = FALSE
								bFocusPedIsSpecial = FALSE
								IF DOES_ENTITY_EXIST(GET_SPECTATOR_CURRENT_FOCUS_PED())
									IF FIND_PED_IN_SPECTATOR_HUD_TARGET_LISTS(specData.specHUDData, iSpecList, iSpecEntry, GET_SPECTATOR_CURRENT_FOCUS_PED())
										bFoundFocusPed = TRUE
										pedFocus = GET_SPECTATOR_CURRENT_FOCUS_PED()
										SWITCH iSpecList
											CASE SPEC_HUD_TARGET_LIST_PLAYERS
											CASE SPEC_HUD_TARGET_LIST_SPECIAL_ENEMIES
											CASE SPEC_HUD_TARGET_LIST_SPECIAL_ALLIES
												bFocusPedIsSpecial = TRUE	//entry is "special", as in they have a unique name (i.e. players)
											BREAK
										ENDSWITCH
									ENDIF
								ENDIF
								
								INT iListPlace
								INT iFirstListPlaceOnPage
								INT iEntryList
								INT iEntryEntry
								iFirstListPlaceOnPage = specData.specHUDData.iCurrentPage * (specData.specHUDData.iNumberOfTargetsOnAPage)
								
								BOOL bStartNewOrderDone
								IF IS_BIT_SET(specData.specHUDData.iBitset, SPEC_HUD_BS_DO_HEADSHOT_TIMED_UPDATE)
									RESET_NET_TIMER(specData.specHUDData.SCHeadshotUpdateTimer)
									CLEAR_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_DO_HEADSHOT_TIMED_UPDATE)
									CPRINTLN(DEBUG_SPECTATOR, "=== HUD === HEADSHOT - SPEC_HUD_BS_DO_HEADSHOT_TIMED_UPDATE - CLEARED")
								ENDIF
								
								REPEAT specData.specHUDData.iNumberOfTargetsOnCurrentPage i
									iListPlace = i + iFirstListPlaceOnPage
									listEntry = GET_SPECTATOR_HUD_TARGET_LISTS_ENTRY(specData.specHUDData, iListPlace)
									
									hcRowColour = HUD_COLOUR_GREY				//set colour to grey to begin with, we use this to flag if the entry has been added
									
									IF SPEC_IS_PED_ACTIVE(listEntry.pedIndex)
										
										IF IS_PED_INJURED(listEntry.pedIndex)
										ENDIF
										
										iPlacing = -1
										iTeam = -1
										
										bEntryIsFocusPed = FALSE
										IF bFoundFocusPed
											iEntryList = -1
											iEntryEntry = -1
											IF bFocusPedIsSpecial
												IF pedFocus = listEntry.pedIndex
													bEntryIsFocusPed = TRUE
												ENDIF
											ELSE					
												IF GET_SPECTATOR_HUD_TARGET_LIST_AND_ENTRY_ID(specData.specHUDData, iEntryList, iEntryEntry, iListPlace)
													IF iSpecList = iEntryList		//it's a generic enemy or generic ally!
														bEntryIsFocusPed = TRUE
														
														IF hcRowColour = HUD_COLOUR_GREY	//colour not yet defined
															IF iEntryList = SPEC_HUD_TARGET_LIST_GENERIC_ALLIES
																hcRowColour = GET_TEAM_DEATHMATCH_HUD_COLOUR(FALSE)
															ELIF iEntryList = SPEC_HUD_TARGET_LIST_GENERIC_ENEMIES
																hcRowColour = GET_TEAM_DEATHMATCH_HUD_COLOUR(TRUE)
															ENDIF
														ENDIF
													ENDIF
												ENDIF
											ENDIF
										ENDIF
										
										IF hcRowColour = HUD_COLOUR_GREY	//colour not yet defined
											IF IS_PED_A_PLAYER(listEntry.pedIndex)
											AND NATIVE_TO_INT(NETWORK_GET_PLAYER_INDEX_FROM_PED(listEntry.pedIndex)) <> -1
											AND IS_NET_PLAYER_OK(NETWORK_GET_PLAYER_INDEX_FROM_PED(listEntry.pedIndex), FALSE)
												
												//it's a player, grab their colour based on who we're watching, or the local machine
									
												IF DOES_LB_USE_CUSTOM_TEAM_COLOURS_FOR_NEW_VS()
													iTeam = GET_PLAYER_TEAM(NETWORK_GET_PLAYER_INDEX_FROM_PED(listEntry.pedIndex))
													IF iTeam != -1 AND iTeam != 8
														hcRowColour = GET_HUD_COLOUR_FOR_FMMC_TEAM(iTeam, NETWORK_GET_PLAYER_INDEX_FROM_PED(listEntry.pedIndex))
													ELSE
														hcRowColour = GET_PLAYER_HUD_COLOUR(NETWORK_GET_PLAYER_INDEX_FROM_PED(listEntry.pedIndex))
													ENDIF
													
													#IF IS_DEBUG_BUILD
													IF GET_COMMANDLINE_PARAM_EXISTS("sc_TeamColourDebug")
														PRINTLN("[RE]1 [TEAMCOLOUR] [SPEC] - iTeam = ", iTeam)
														PRINTLN("[RE]1 [TEAMCOLOUR] [SPEC] - HUD Colour = ", GET_HUD_COLOUR_STRING_FROM_INT(ENUM_TO_INT(hcRowColour)))
													ENDIF
													#ENDIF
												ELSE
													iTeam = GET_PLAYER_TEAM(NETWORK_GET_PLAYER_INDEX_FROM_PED(listEntry.pedIndex))
													IF g_b_On_Race
														hcRowColour = GET_HUD_COLOUR_FOR_OPPONENT_CHOSEN_TEAM(GET_PLAYER_TEAM(PLAYER_ID()), iTeam, TRUE, (NUM_NETWORK_PLAYERS/2))
														PRINTLN("[RE]3 [TEAMCOLOUR] [SPEC] - iTeam = ", iTeam)
														PRINTLN("[RE]3 [TEAMCOLOUR] [SPEC] - HUD Colour = ", GET_HUD_COLOUR_STRING_FROM_INT(ENUM_TO_INT(hcRowColour)))
													ELSE
														hcRowColour = GET_PLAYER_HUD_COLOUR(NETWORK_GET_PLAYER_INDEX_FROM_PED(listEntry.pedIndex))	
													ENDIF
													
													#IF IS_DEBUG_BUILD
													IF GET_COMMANDLINE_PARAM_EXISTS("sc_TeamColourDebug")	
														PRINTLN("[RE]2 [TEAMCOLOUR] [SPEC] - iTeam = ", iTeam)
														PRINTLN("[RE]2 [TEAMCOLOUR] [SPEC] - HUD Colour = ", GET_HUD_COLOUR_STRING_FROM_INT(ENUM_TO_INT(hcRowColour)))
													ENDIF
													#ENDIF
								
												ENDIF
							
											ELSE
												//it's an AI, get the generic red or blue colour
												IF iEntryList <> -1
												OR GET_SPECTATOR_HUD_TARGET_LIST_AND_ENTRY_ID(specData.specHUDData, iEntryList, iEntryEntry, iListPlace)
													SWITCH iEntryList
														CASE SPEC_HUD_TARGET_LIST_GENERIC_ALLIES
														CASE SPEC_HUD_TARGET_LIST_SPECIAL_ALLIES
															hcRowColour = GET_TEAM_DEATHMATCH_HUD_COLOUR(FALSE)
														BREAK
														CASE SPEC_HUD_TARGET_LIST_GENERIC_ENEMIES
														CASE SPEC_HUD_TARGET_LIST_SPECIAL_ENEMIES
															hcRowColour = GET_TEAM_DEATHMATCH_HUD_COLOUR(TRUE)
														BREAK
													ENDSWITCH
												ENDIF
											ENDIF
										ENDIF
										
										IF IS_PED_A_PLAYER(listEntry.pedIndex)
											
											IF bStartNewOrderDone = FALSE
											AND bUpdateList = FALSE
												BEGIN_SCALEFORM_MOVIE_METHOD(specData.specHUDData.sfSummaryCard, "START_NEW_ORDER")
												END_SCALEFORM_MOVIE_METHOD()
												bStartNewOrderDone = TRUE
												CPRINTLN(DEBUG_SPECTATOR, "=== HUD === SPEC_HUD_STAGE_FULL_LIST  - START_NEW_ORDER")
											ENDIF
											
											//set the players "placing", as in 1st, 2nd, 3rd within the event
											IF NOT g_bOnCoopMission
											AND NOT Is_Player_Currently_On_MP_LTS_Mission(PLAYER_ID())
											AND NOT Is_Player_Currently_On_MP_Versus_Mission(PLAYER_ID())
											AND GET_FM_MISSION_TYPE_PLAYER_IS_ON(PLAYER_ID()) <> FMMC_TYPE_SURVIVAL
											AND IS_LOCAL_PLAYER_ON_ANY_FM_MISSION()
											AND GET_SPEC_CAM_MODE(specData.SpecCamData) <> SPEC_MODE_TV_CHANNEL
											 AND GET_SPEC_CAM_MODE(specData.SpecCamData) <> SPEC_MODE_HEIST 
											AND NOT IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_MG_PILOT_SCHOOL)
												iTeam = GET_PLAYER_TEAM(NETWORK_GET_PLAYER_INDEX_FROM_PED(listEntry.pedIndex))
												IF iTeam > -1
													IF MPSpecGlobals.iCurrentJobTeamPositions[iTeam] > -1
														//team event, use the player's team's placing as the player placing
														iPlacing = MPSpecGlobals.iCurrentJobTeamPositions[iTeam]
														
														PRINTLN("[CS_MISS] MAINTAIN_SUMMARY_CARD, Team iPlacing = ", iPlacing)
													ELSE
														PRINTLN("[CS_MISS] MAINTAIN_SUMMARY_CARD, iTeam = -1  ")
													ENDIF
												ENDIF
												IF iPlacing = -1
													IF NATIVE_TO_INT(NETWORK_GET_PLAYER_INDEX_FROM_PED(listEntry.pedIndex)) > -1
														//use the player's placing in the event
														iPlacing = MPSpecGlobals.iCurrentJobPlayerPositions[NATIVE_TO_INT(NETWORK_GET_PLAYER_INDEX_FROM_PED(listEntry.pedIndex))]
														
														PRINTLN("[CS_MISS] MAINTAIN_SUMMARY_CARD, iPlacing = ", iPlacing)
													ENDIF
												ENDIF
											ENDIF
											
											//in certain cases we want to modify the placing as each event stores the placing different (some have 1st as 0, some 1st as 1)
											IF iPlacing <> -1
												IF GET_FM_MISSION_TYPE_PLAYER_IS_ON(PLAYER_ID()) = FMMC_TYPE_DEATHMATCH
													IF NOT g_bFM_ON_TEAM_DEATHMATCH
														iPlacing += 1
														
														PRINTLN("[CS_MISS] MAINTAIN_SUMMARY_CARD, DM, iPlacing = ", iPlacing)
													ENDIF
												ELIF GET_FM_MISSION_TYPE_PLAYER_IS_ON(PLAYER_ID()) = FMMC_TYPE_MISSION
													IF NOT Is_Player_Currently_On_MP_CTF_Mission(PLAYER_ID())
													//IF GET_FM_MISSION_TYPE_PLAYER_IS_ON(PLAYER_ID()) <> FMMC_TYPE_MISSION_CTF
													AND iTeam = -1
														iPlacing += 1
														
														PRINTLN("[CS_MISS] MAINTAIN_SUMMARY_CARD, Mission, iPlacing = ", iPlacing)
													ENDIF
												ENDIF
											ENDIF
											
											//Backup if placing is 0
											IF iPlacing = 0
												PRINTLN("[CS_MISS] MAINTAIN_SUMMARY_CARD - RESET iPlacing to -1 DUE TO INVALID POSITION - iPlacing = ", iPlacing)
												iPlacing = -1
											ENDIF
											
											//Crew Tag
											IF IS_PLAYER_ON_ANY_FM_MISSION_THAT_IS_NOT_GANG_ATTACK(PLAYER_ID())
												tlCrewTag = ""
											ELSE
												GET_PLAYER_CREW_TAG_FOR_SCALEFORM(NETWORK_GET_PLAYER_INDEX_FROM_PED(listEntry.pedIndex), tlCrewTag)
											ENDIF
											
											//Attempt to get player headshot
											pedHeadshot = Get_HeadshotID_For_Player(NETWORK_GET_PLAYER_INDEX_FROM_PED(listEntry.pedIndex))
											strHeadshotTxd = ""
											IF pedHeadshot != NULL
												strHeadshotTxd = GET_PEDHEADSHOT_TXD_STRING(pedHeadshot)
											ENDIF
											CPRINTLN(DEBUG_SPECTATOR, "=== HUD === HEADSHOT - strHeadshotTxd = ", strHeadshotTxd)
											
											IF NOT IS_BIT_SET(specData.specHUDData.iBitset, SPEC_HUD_BS_DO_HEADSHOT_TIMED_UPDATE)
												IF IS_STRING_NULL_OR_EMPTY(strHeadshotTxd)
													SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_DO_HEADSHOT_TIMED_UPDATE)
													CPRINTLN(DEBUG_SPECTATOR, "=== HUD === HEADSHOT - SPEC_HUD_BS_DO_HEADSHOT_TIMED_UPDATE - SET")
												ENDIF
											ENDIF
											
											BOOL bPlayerAllowsSpectating
											bPlayerAllowsSpectating = TRUE
											INT iPlayer 
											iPlayer = NATIVE_TO_INT(NETWORK_GET_PLAYER_INDEX_FROM_PED(listEntry.pedIndex))
											IF iPlayer != -1
												IF NOT DOES_PLAYER_ALLOW_SPECTATORS(iPlayer)
													bPlayerAllowsSpectating = FALSE
												ENDIF
											ENDIF
											
											//#IF IS_DEBUG_BUILD
											//CPRINTLN(DEBUG_SPECTATOR, "=== HUD === SET_SCALEFORM_SUMMARY_CARD_ROW_PLAYER - iListPlace = ", iListPlace, " iPlacing = ", iPlacing)
											//#ENDIF
											
											//Block Spectating rival Team
											//bPlayerAllowsSpectating
											
											IF NOT IS_BIT_SET(specData.specHUDData.iBitset, SPEC_HUD_BS_SET_INITIAL_HIGHLIGHT)
											AND bEntryIsFocusPed
												SET_SPECTATOR_HUD_CURRENTLY_HIGHLIGHTED_ROW(specData.specHUDData, i)
												SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_SET_INITIAL_HIGHLIGHT)
												CPRINTLN(DEBUG_SPECTATOR, "=== HUD === SET_SCALEFORM_SUMMARY_CARD_ROW_PLAYER - SET ROW HIGHLIGHT TO FOCUS PED - ROW = ", i)
											ENDIF
																						
														
											//CPRINTLN(DEBUG_SPECTATOR, "=== HUD === SET_SCALEFORM_SUMMARY_CARD_ROW_PLAYER - hcRowColour = ", HUD_COLOUR_AS_STRING(hcRowColour))
											
											BOOL bDead
											bDead = IS_ENTITY_DEAD(listEntry.pedIndex)
																						
											//add the info to the scaleform for this player
									
									
											IF IS_NET_PLAYER_OK(NETWORK_GET_PLAYER_INDEX_FROM_PED(listEntry.pedIndex), FALSE)
												PRINTLN("[SPC_DPAD] SET_SCALEFORM_SPECTATOR_SUMMARY_CARD_ROW_PLAYER, iRow = ", i, " playerID = ", GET_PLAYER_NAME(listEntry.playerID), " colour = ", HUD_COLOUR_AS_STRING((hcRowColour)))
												
												IF hcRowColour = HUD_COLOUR_G3
													hcRowColour = HUD_COLOUR_YELLOW
													CPRINTLN(DEBUG_SPECTATOR, "=== HUD === [SPC_DPAD] Colour is Light variation, can't read text. Swapping to HUD_COLOUR_YELLOW")
												ENDIF
												
												SET_SCALEFORM_SPECTATOR_SUMMARY_CARD_ROW_PLAYER(	specData.specHUDData.sfSummaryCard, bUpdateList, i, listEntry.iRank, 
																						hcRowColour, GET_PLAYER_NAME(listEntry.playerID), tlCrewTag, strHeadshotTxd, bEntryIsFocusPed,
																						iPlacing, bPlayerAllowsSpectating, bDead)
											ENDIF
									
									
										/*ELSE
											IF DOES_TEXT_LABEL_EXIST(listEntry.sName)
												SET_SCALEFORM_SUMMARY_CARD_ROW_JUST_STRING(	specData.specHUDData.sfSummaryCard, bUpdateList, iListPlace, 
																							hcRowColour, listEntry.sName, FALSE, bEntryIsFocusPed, iPlacing)
											ELSE
												#IF IS_DEBUG_BUILD
												CPRINTLN(DEBUG_SPECTATOR, "=== HUD === SPEC_HUD_STAGE_FULL_LIST fail as not a text label")
												#ENDIF
											ENDIF*/
										ENDIF
									/*ELSE
										IF NOT IS_STRING_NULL_OR_EMPTY(listEntry.sName)
											SET_SCALEFORM_SUMMARY_CARD_ROW_JUST_STRING(	specData.specHUDData.sfSummaryCard, bUpdateList, iListPlace, 
																						hcRowColour, listEntry.sName, TRUE, FALSE, -1)
										ENDIF*/
									ENDIF
								ENDREPEAT
								
								IF bStartNewOrderDone = TRUE
								AND bUpdateList = FALSE
									BEGIN_SCALEFORM_MOVIE_METHOD(specData.specHUDData.sfSummaryCard, "END_NEW_ORDER")
									END_SCALEFORM_MOVIE_METHOD()
									bStartNewOrderDone = FALSE
									CPRINTLN(DEBUG_SPECTATOR, "=== HUD === SPEC_HUD_STAGE_FULL_LIST  - END_NEW_ORDER")
								ENDIF
								
								DISPLAY_VIEW(specData.specHUDData.sfSummaryCard, TRUE, FALSE)
								
								//Heist Team Names
								IF IS_PLAYER_PLAYING_OR_PLANNING_HEIST(PLAYER_ID())
									REPEAT specData.specHUDData.iNumberOfTargetsOnCurrentPage i
										iListPlace = i + iFirstListPlaceOnPage
										listEntry = GET_SPECTATOR_HUD_TARGET_LISTS_ENTRY(specData.specHUDData, iListPlace)
										IF SPEC_IS_PED_ACTIVE(listEntry.pedIndex)
											IF IS_PED_INJURED(listEntry.pedIndex)
											ENDIF
											
											IF IS_PED_A_PLAYER(listEntry.pedIndex)
												INT iPlyr
												iPlyr = NATIVE_TO_INT(NETWORK_GET_PLAYER_INDEX_FROM_PED(listEntry.pedIndex))
												IF iPlyr != -1
													INT iPlyrTeam 
													iPlyrTeam = GET_PLAYER_TEAM(NETWORK_GET_PLAYER_INDEX_FROM_PED(listEntry.pedIndex))
													IF iPlyrTeam != -1
														IF NOT IS_STRING_NULL_OR_EMPTY(g_sMission_TeamName[iPlyrTeam])
														AND NOT ARE_STRINGS_EQUAL("NULL", g_sMission_TeamName[iPlyrTeam])
															IF HAS_THIS_ADDITIONAL_TEXT_LOADED("FMMC", ODDJOB_TEXT_SLOT)
																BOOL bLiteral
																IF g_FMMC_STRUCT.iTeamNames[iPlyrTeam] = ciTEAM_NAME_MAX
																AND NOT IS_BIT_SET(g_iTeamNameBitSetPlayerName, iPlyrTeam)
																	bLiteral = FALSE
																ELSE
																	bLiteral = TRUE
																ENDIF
																
																BEGIN_SCALEFORM_MOVIE_METHOD(specData.specHUDData.sfSummaryCard, "SET_ITEM_TEXT_RIGHT")
																	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iListPlace)
																	IF bLiteral = FALSE
																	AND DOES_TEXT_LABEL_EXIST(g_sMission_TeamName[iPlyrTeam])
																		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(g_sMission_TeamName[iPlyrTeam]) 
																	ELSE
																		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(g_sMission_TeamName[iPlyrTeam])
																	ENDIF
																END_SCALEFORM_MOVIE_METHOD()
																
																CPRINTLN(DEBUG_SPECTATOR, "=== HUD === SPEC_HUD_STAGE_FULL_LIST - SET_ITEM_TEXT_RIGHT - iPlayer = ", iPlyr, " iListPlace = ", iListPlace, " Literal = ", PICK_STRING(bLiteral, "TRUE", "FALSE"), " iTEAM = ", iPlyrTeam, " TEAM NAME = ", g_sMission_TeamName[iPlyrTeam])
															ELSE
																CPRINTLN(DEBUG_SPECTATOR, "=== HUD === SPEC_HUD_STAGE_FULL_LIST - SET_ITEM_TEXT_RIGHT - iPlayer = ", iPlyr, " iListPlace = ", iListPlace, " WAITING FOR TEXT TO LOAD 'FMMC' - REDO LIST (DELAYED) ")
																REQUEST_ADDITIONAL_TEXT("FMMC", ODDJOB_TEXT_SLOT)
																SET_SPECTATOR_REDO_LIST_DUE_TO_EVENT_DELAYED()
															ENDIF
														ELSE
															CPRINTLN(DEBUG_SPECTATOR, "=== HUD === SPEC_HUD_STAGE_FULL_LIST - SET_ITEM_TEXT_RIGHT - iPlayer = ", iPlyr, " iListPlace = ", iListPlace, " TEAM NAME IS EMPTY - REDO LIST (DELAYED) ")
															SET_SPECTATOR_REDO_LIST_DUE_TO_EVENT_DELAYED()
														ENDIF
													ELSE	
														CPRINTLN(DEBUG_SPECTATOR, "=== HUD === SPEC_HUD_STAGE_FULL_LIST - SET_ITEM_TEXT_RIGHT - iPlayer = ", iPlyr, " iListPlace = ", iListPlace, " iTeam = -1 - REDO LIST (DELAYED) ")
														SET_SPECTATOR_REDO_LIST_DUE_TO_EVENT_DELAYED()
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDREPEAT
								ENDIF
								
								IF IS_BIT_SET(specData.specHUDData.iBitset, SPEC_HUD_BS_REQUEST_SPEC_HELP_TEXT)		//add any help text
									IF GET_FM_MISSION_TYPE_PLAYER_IS_ON(PLAYER_ID()) = FMMC_TYPE_MISSION
										IF IS_ENTITY_DEAD(PLAYER_PED_ID())
											#IF IS_DEBUG_BUILD
											CPRINTLN(DEBUG_SPECTATOR, "=== HUD === SCALEFORM_SUMMARY_CARD_PRINT_HELP SPEC_FIRST_HELP")
											#ENDIF
											SCALEFORM_SUMMARY_CARD_PRINT_HELP(specData.specHUDData.sfSummaryCard, "SPEC_FIRST_HELP") //prints the message under spectator list
										ENDIF
									ELIF GET_FM_MISSION_TYPE_PLAYER_IS_ON(PLAYER_ID()) = FMMC_TYPE_SURVIVAL
										IF IS_ENTITY_DEAD(PLAYER_PED_ID())
											#IF IS_DEBUG_BUILD
											CPRINTLN(DEBUG_SPECTATOR, "=== HUD === SCALEFORM_SUMMARY_CARD_PRINT_HELP HRD_DEADMSG2")
											#ENDIF
											SCALEFORM_SUMMARY_CARD_PRINT_HELP(specData.specHUDData.sfSummaryCard, "HRD_DEADMSG2") //prints the message under spectator list
										ENDIF
									ENDIF
								ENDIF
								
							ENDIF
							
							IF HAS_SCALEFORM_MOVIE_LOADED(specData.specHUDData.sfSummaryCard)
							AND NATIVE_TO_INT(specData.specHUDData.sfSummaryCard) != -1
								SET_SCALEFORM_SUMMARY_CARD_HIGHLIGHT(specData.specHUDData.sfSummaryCard, GET_SPECTATOR_HUD_CURRENTLY_HIGHLIGHTED_ROW(specData.specHUDData))
							ENDIF
						ENDIF
					BREAK
					
				ENDSWITCH
				
				CLEAR_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_REQUEST_REDO_SUMMARY_CARD)
				CLEAR_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_REQUEST_UPDATE_SUMMARY_CARD)
				
				SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_REQUEST_UPDATE_INSTRUCTIONAL_BUTTONS)
			ENDIF
		
		//Check if we should do an Update
		ELSE
			IF IS_BIT_SET(specData.specHUDData.iBitset, SPEC_HUD_BS_DO_HEADSHOT_TIMED_UPDATE)
				IF HAS_NET_TIMER_EXPIRED(specData.specHUDData.SCHeadshotUpdateTimer, 2000)
					RESET_NET_TIMER(specData.specHUDData.SCHeadshotUpdateTimer)
					CLEAR_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_DO_HEADSHOT_TIMED_UPDATE)
					CPRINTLN(DEBUG_SPECTATOR, "=== HUD === HEADSHOT - UPDATE FOR HEADSHOTS - SPEC_HUD_BS_DO_HEADSHOT_TIMED_UPDATE - CLEARED")
					SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_REQUEST_UPDATE_SUMMARY_CARD)
					CPRINTLN(DEBUG_SPECTATOR, "=== HUD === HEADSHOT - UPDATE FOR HEADSHOTS - SPEC_HUD_BS_REQUEST_UPDATE_SUMMARY_CARD - SET")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks if the player is able to use a filter
/// PARAMS:
///    thisFilter - filter to check
/// RETURNS:
///    true/false
FUNC BOOL IS_SAFE_TO_USE_SPEC_HUD_FILTER(SPEC_HUD_FILTER_LIST thisFilter)
	
	SWITCH thisFilter
		//CASE SPEC_HUD_FILTER_NONE
		CASE SPEC_HUD_FILTER_SPEC_1
		CASE SPEC_HUD_FILTER_SPEC_2
		CASE SPEC_HUD_FILTER_SPEC_3
		CASE SPEC_HUD_FILTER_SPEC_4
		CASE SPEC_HUD_FILTER_SPEC_5
		CASE SPEC_HUD_FILTER_SPEC_6
		CASE SPEC_HUD_FILTER_SPEC_7
		CASE SPEC_HUD_FILTER_SPEC_8
		CASE SPEC_HUD_FILTER_SPEC_9
		CASE SPEC_HUD_FILTER_SPEC_10
		/*CASE SPEC_HUD_FILTER_SNAP_1
		CASE SPEC_HUD_FILTER_SNAP_2
		CASE SPEC_HUD_FILTER_SNAP_3
		CASE SPEC_HUD_FILTER_SNAP_4
		CASE SPEC_HUD_FILTER_SNAP_5
		CASE SPEC_HUD_FILTER_SNAP_6
		CASE SPEC_HUD_FILTER_SNAP_7
		CASE SPEC_HUD_FILTER_SNAP_8
		CASE SPEC_HUD_FILTER_SNAP_9*/
		CASE SPEC_HUD_FILTER_NEWS
			RETURN TRUE
		BREAK
		
		#IF FEATURE_COPS_N_CROOKS
		CASE SPEC_HUD_FILTER_CNC_1
			IF IS_FREEMODE_ARCADE()
			AND GET_ARCADE_MODE() = ARC_COPS_CROOKS
				RETURN TRUE
			ENDIF
		BREAK
		#ENDIF
		
		CASE SPEC_HUD_FILTER_GTAOTV
			IF IS_PLAYER_SCTV(PLAYER_ID())
				IF IS_PLAYER_A_ROCKSTAR_DEV()
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Switches to the next valid filter
/// PARAMS:
///    specHUDData - 
PROC SWITCH_TO_NEXT_SPEC_HUD_FILTER(SPECTATOR_HUD_DATA &specHUDData)
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_SPECTATOR, "=== HUD === SWITCH_TO_NEXT_SPEC_HUD_FILTER")
	#ENDIF
	IF specHUDData.eCurrentFilter = specHUDData.eDesiredFilter
		INT iProcess = ENUM_TO_INT(specHUDData.eDesiredFilter)
		BOOL bFoundFilter = FALSE
		
		WHILE NOT bFoundFilter
			iProcess += 1
			IF iProcess >= NUMBER_OF_SPEC_HUD_FILTERS
				iProcess = 0
			ENDIF
			
			IF IS_SAFE_TO_USE_SPEC_HUD_FILTER(INT_TO_ENUM(SPEC_HUD_FILTER_LIST, iProcess))
				bFoundFilter = TRUE
			ENDIF
			
			IF NOT bFoundFilter
				IF specHUDData.eCurrentFilter = INT_TO_ENUM(SPEC_HUD_FILTER_LIST, iProcess)	//back to where we started
					bFoundFilter = TRUE
				ENDIF
			ENDIF
			
		ENDWHILE
		
		specHUDData.iFrameDelay = -1
		specHUDData.iFilterStage = 0
		specHUDData.eDesiredFilter = INT_TO_ENUM(SPEC_HUD_FILTER_LIST, iProcess)
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_SPECTATOR, "=== HUD === specHUDData.eDesiredFilter = ", ENUM_TO_INT(specHUDData.eDesiredFilter))
		#ENDIF
	ENDIF
ENDPROC

//PURPOSE: Refreshes the currently selected Filter. (For use after cutscenes where the filter was cleared)
PROC REFRESH_SPEC_HUD_FILTER()
	IF NOT IS_BIT_SET(MPSpecGlobals.SCTVData.iBitSet, SCTV_BIT_REFRESH_FILTER)
		SET_BIT(MPSpecGlobals.SCTVData.iBitSet, SCTV_BIT_REFRESH_FILTER)
		CPRINTLN(DEBUG_SPECTATOR, "=== HUD === REFRESH_SPEC_HUD_FILTER - SCTV_BIT_REFRESH_FILTER SET")
	ENDIF
ENDPROC

/// PURPOSE:
///    Switches to the previous valid filter
/// PARAMS:
///    specHUDData - 
PROC SWITCH_TO_PREVIOUS_SPEC_HUD_FILTER(SPECTATOR_HUD_DATA &specHUDData)
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_SPECTATOR, "=== HUD === SWITCH_TO_PREVIOUS_SPEC_HUD_FILTER")
	#ENDIF
	IF specHUDData.eCurrentFilter = specHUDData.eDesiredFilter
		INT iProcess = ENUM_TO_INT(specHUDData.eDesiredFilter)
		BOOL bFoundFilter = FALSE
		
		WHILE NOT bFoundFilter
			iProcess -= 1
			IF iProcess < 0
				iProcess = NUMBER_OF_SPEC_HUD_FILTERS-1
			ENDIF
			
			IF IS_SAFE_TO_USE_SPEC_HUD_FILTER(INT_TO_ENUM(SPEC_HUD_FILTER_LIST, iProcess))
				bFoundFilter = TRUE
			ENDIF
			
			IF NOT bFoundFilter
				IF specHUDData.eCurrentFilter = INT_TO_ENUM(SPEC_HUD_FILTER_LIST, iProcess)	//back to where we started
					bFoundFilter = TRUE
				ENDIF
			ENDIF
			
		ENDWHILE
		
		specHUDData.iFrameDelay = -1
		specHUDData.iFilterStage = 0
		specHUDData.eDesiredFilter = INT_TO_ENUM(SPEC_HUD_FILTER_LIST, iProcess)
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_SPECTATOR, "=== HUD === specHUDData.eDesiredFilter = ", ENUM_TO_INT(specHUDData.eDesiredFilter))
		#ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets the filter for first going into spectator cam after dying in a job
/// PARAMS:
///    specHUDData - 
PROC SETUP_DEATH_INITIAL_FILTER(SPECTATOR_HUD_DATA &specHUDData)
	
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_SPECTATOR, "=== HUD === SETUP_DEATH_INITIAL_FILTER")
	#ENDIF
	
	SET_TIMECYCLE_MODIFIER("spectator1")
	specHUDData.eDesiredFilter = SPEC_HUD_FILTER_SPEC_1
	specHUDData.eCurrentFilter = SPEC_HUD_FILTER_SPEC_1
	specHUDData.iFilterStage = 99
ENDPROC

/// PURPOSE:
///    Scaleform helper function for adding a player name in a label to the scaleform param list.
/// PARAMS:
///    param - lavel to look up in stringtable
///    sPlayerName - Player name (from GET_PLAYER_NAME()) to add to scaleform
PROC SCALEFORM_MOVIE_METHOD_ADD_PARAM_LABEL_WITH_PLAYER_NAME(STRING param, STRING sPlayerName)
	BEGIN_TEXT_COMMAND_SCALEFORM_STRING(param)
		ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(sPlayerName)
	END_TEXT_COMMAND_SCALEFORM_STRING()
ENDPROC

/// PURPOSE:
///    Scaleform helper function for adding a text label 63 in a label to the scaleform param list.
/// PARAMS:
///    param - lavel to look up in stringtable
///    tl63 - text label 63 to add to scaleform
PROC SCALEFORM_MOVIE_METHOD_ADD_PARAM_LABEL_WITH_TL63(STRING param, TEXT_LABEL_63 tl63)
	BEGIN_TEXT_COMMAND_SCALEFORM_STRING(param)
		ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(tl63)
	END_TEXT_COMMAND_SCALEFORM_STRING()
ENDPROC

/// PURPOSE:
///    Updates the GTAOTV top-right live panel with relevant information
/// PARAMS:
///    specData - 
///    bOn - set the live panel on or off (true = on)
PROC SETUP_GTAOTV_HUD_LIVE_PANEL(SPECTATOR_DATA_STRUCT &specData, BOOL bOn)
	IF HAS_SCALEFORM_MOVIE_LOADED(specData.specHUDData.sfFilter)
		BEGIN_SCALEFORM_MOVIE_METHOD(specData.specHUDData.sfFilter, "SHOW_LIVE_PANEL")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bOn)										//Live panel on/off
		END_SCALEFORM_MOVIE_METHOD()
		BEGIN_SCALEFORM_MOVIE_METHOD(specData.specHUDData.sfFilter, "SET_LIVE_PANEL")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("SPEC_LIVE")//LIVE						//Live panel title
			IF MPSpecGlobals.SCTVData.currentMode = SCTV_MODE_FOLLOW_CAM
				IF IS_PED_A_PLAYER(GET_SPECTATOR_CURRENT_FOCUS_PED())
					PLAYER_INDEX thisPlayer = NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_SPECTATOR_CURRENT_FOCUS_PED())
					IF IS_NET_PLAYER_OK(thisPlayer, FALSE, FALSE)
						PEDHEADSHOT_ID pedHeadshot
						STRING strHeadshotTxd
						TEXT_LABEL_15 tlCrewTag
						
						pedHeadshot = Get_HeadshotID_For_Player(thisPlayer)
						strHeadshotTxd = ""
						IF pedHeadshot != NULL
							strHeadshotTxd = GET_PEDHEADSHOT_TXD_STRING(pedHeadshot)
							PRINTLN(" SCTV - SETUP_GTAOTV_HUD_LIVE_PANEL - GOT HEADSHOT - strHeadshotTxd = ", strHeadshotTxd)
						ENDIF
						
						GET_PLAYER_CREW_TAG_FOR_SCALEFORM(thisPlayer, tlCrewTag)
					
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("SPEC_CRWT")//Currently Watching		//Live panel subtitle
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(GET_PLAYER_NAME(thisPlayer))		//Live panel name
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(tlCrewTag)							//Live panel crewtag
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING(strHeadshotTxd)			//Live panel player txd
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING(strHeadshotTxd)			//Live panel player txn
					ENDIF
				ENDIF
			ENDIF
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC

//PURPOSE: Gets the time of day as a text label (only hours and minutes)
FUNC TEXT_LABEL_7 GET_TIME_TEXT_LABEL(TIMEOFDAY sTimeOfDay)
	TEXT_LABEL_7 tlTime = ""
	INT iTempInt = GET_TIMEOFDAY_HOUR(sTimeOfDay)
	IF iTempInt < 10
		tlTime += 0
	ENDIF
	tlTime += iTempInt
	tlTime += ":"
	
	iTempInt = GET_TIMEOFDAY_MINUTE(sTimeOfDay)
	IF iTempInt < 10
		tlTime += 0
	ENDIF
	tlTime += iTempInt
	
	RETURN tlTime
ENDFUNC

PROC ADD_GAME_LOCATION_TO_TICKER(SPECTATOR_DATA_STRUCT &specData)
	INT iHashName1, iHashName2
	GET_STREET_NAME_AT_COORD(GET_FINAL_RENDERED_CAM_COORD(), iHashname1, iHashname2)
	IF iHashname2 != 0
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRTNM2")
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL_HASH_KEY(iHashname1)
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL_HASH_KEY(iHashname2)
		END_TEXT_COMMAND_SCALEFORM_STRING()
	ELIF iHashname1 != 0
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRTNM1")
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL_HASH_KEY(iHashname1)
		END_TEXT_COMMAND_SCALEFORM_STRING()
	ELSE
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(GET_TIME_AS_STRING(GET_NETWORK_TIME()))
	ENDIF
	specData.specHUDData.TickerUpdateDelay = HUD_TICKER_FREEMODE_2
	specData.specHUDData.iTickerStage++
ENDPROC

FUNC STRING GET_LABEL_FOR_JOB_TYPE()
	SWITCH GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType
		CASE FMMC_TYPE_DEATHMATCH
			IF g_FMMC_STRUCT.iTeamDeathmatch = FMMC_DEATHMATCH_TEAM
				RETURN "DM_CUT_2"
			ELIF g_FMMC_STRUCT.iVehicleDeathmatch = 1 //VEHICLE DEATHMATCH
				RETURN "DM_CUT_4"
			ELSE
				RETURN "DM_CUT_1"
			ENDIF
		BREAK
		
		CASE FMMC_TYPE_RACE
			IF IS_ON_RALLY_RACE_GLOBAL_SET()
				IF IS_RACE_DRIVER()
					RETURN "RACE_CUT1_RALD" //Rally - Driver
				ELSE
					RETURN "RACE_CUT1_RALP" //Rally - Co-Driver
				ENDIF
			ELSE
				IF GlobalServerBD_Races.iRaceMode = 1
					RETURN "RACE_CUT1_1" //GTA - Race
				ELSE
					IF g_FMMC_STRUCT.iRaceType = FMMC_RACE_TYPE_BOAT
					OR g_FMMC_STRUCT.iRaceType = FMMC_RACE_TYPE_BOAT_P2P
						RETURN "FMMC_RSTAR_WR" //Boat Race
					ELIF g_FMMC_STRUCT.iRaceType = FMMC_RACE_TYPE_AIR
					OR g_FMMC_STRUCT.iRaceType = FMMC_RACE_TYPE_AIR_P2P
						RETURN "FMMC_RSTAR_AR" //Air Race
					ELSE
						IF g_FMMC_STRUCT.iRaceType = FMMC_RACE_TYPE_BIKE_AND_CYCLE
						OR g_FMMC_STRUCT.iRaceType = FMMC_RACE_TYPE_BIKE_AND_CYCLE_P2P
						OR IS_BIKE_RACE(GlobalServerBD_Races.iRaceType)
							RETURN "FMMC_RSTAR_BR" //Bike Race
						ELSE
							RETURN "RACE_CUT1_0" //Race
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE FMMC_TYPE_SURVIVAL
			RETURN "MH_CUT_3" //Survival
		BREAK
		
		CASE FMMC_TYPE_MISSION
			IF Is_Player_Currently_On_MP_LTS_Mission(PLAYER_ID())
				RETURN "MC_CUT_4" //Last Team Standing
			ELIF g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_CTF
				RETURN "MC_CUT_5" //Capture the Flag
			ELSE
				RETURN "MC_CUT_1" //Mission
			ENDIF
		BREAK
		
		CASE FMMC_TYPE_BASE_JUMP
			RETURN "RACE_CUT1_2" //Basejump
		BREAK
	ENDSWITCH
	
	RETURN "MC_CUT_1" //Mission
ENDFUNC

/// PURPOSE:
///    Updates the GTAOTV bottom ticker panel with relevant information
/// PARAMS:
///    specData - 
///    bOn - set the ticker panel on or off (true = on)
PROC SETUP_GTAOTV_HUD_TICKER_PANEL(SPECTATOR_DATA_STRUCT &specData, BOOL bOn)
	IF HAS_SCALEFORM_MOVIE_LOADED(specData.specHUDData.sfFilter)
		BEGIN_SCALEFORM_MOVIE_METHOD(specData.specHUDData.sfFilter, "SHOW_TICKER")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bOn)										//Ticker panel on/off
		END_SCALEFORM_MOVIE_METHOD()
		
		TEXT_LABEL_15 thisTL
		PLAYER_INDEX TargetPlayer = NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_SPECTATOR_CURRENT_FOCUS_PED())
		
		BEGIN_SCALEFORM_MOVIE_METHOD(specData.specHUDData.sfFilter, "SET_TICKER_TITLE")
			SWITCH GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType
				CASE FMMC_TYPE_DEATHMATCH
				CASE FMMC_TYPE_RACE
				CASE FMMC_TYPE_SURVIVAL
				CASE FMMC_TYPE_MISSION
				CASE FMMC_TYPE_BASE_JUMP
					//PLAYLIST
					IF IS_PLAYER_ON_A_PLAYLIST(TargetPlayer)
						IF specData.specHUDData.iTickerStage >= HUD_TICKER_JOB_STAGES
							specData.specHUDData.iTickerStage = 0
						ENDIF
					
						SWITCH specData.specHUDData.iTickerStage
							CASE 0
								thisTL = GET_LABEL_FOR_JOB_TYPE()
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(thisTL)	//GET_MISSION_SUBTYPE_STRING_FOR_PAUSEMENU(g_FMMC_STRUCT.iMissionType, g_FMMC_STRUCT.iMissionSubType))
								specData.specHUDData.TickerUpdateDelay = HUD_TICKER_PLAYLIST_1
								specData.specHUDData.iTickerStage++
							BREAK
							CASE 1
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(g_FMMC_STRUCT.tl63MissionName)
								specData.specHUDData.TickerUpdateDelay = HUD_TICKER_PLAYLIST_2
								specData.specHUDData.iTickerStage++
							BREAK
							CASE 2
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(g_sCurrentPlayListDetails.tl31PlaylistName)
								specData.specHUDData.TickerUpdateDelay = HUD_TICKER_PLAYLIST_3
								specData.specHUDData.iTickerStage++
							BREAK
							CASE 3
								ADD_GAME_LOCATION_TO_TICKER(specData)
								specData.specHUDData.TickerUpdateDelay = HUD_TICKER_PLAYLIST_4
								specData.specHUDData.iTickerStage++
							BREAK
						ENDSWITCH
					
					//JOB
					ELSE
						IF specData.specHUDData.iTickerStage >= HUD_TICKER_JOB_STAGES
							specData.specHUDData.iTickerStage = 0
						ENDIF
					
						SWITCH specData.specHUDData.iTickerStage
							CASE 0
								thisTL = GET_LABEL_FOR_JOB_TYPE()
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(thisTL)	//GET_MISSION_SUBTYPE_STRING_FOR_PAUSEMENU(g_FMMC_STRUCT.iMissionType, g_FMMC_STRUCT.iMissionSubType))
								specData.specHUDData.TickerUpdateDelay = HUD_TICKER_JOB_1
								specData.specHUDData.iTickerStage++
							BREAK
							CASE 1
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(g_FMMC_STRUCT.tl63MissionName)
								specData.specHUDData.TickerUpdateDelay = HUD_TICKER_JOB_2
								specData.specHUDData.iTickerStage++
							BREAK
							CASE 2
								ADD_GAME_LOCATION_TO_TICKER(specData)
								specData.specHUDData.TickerUpdateDelay = HUD_TICKER_JOB_3
								specData.specHUDData.iTickerStage++
							BREAK
						ENDSWITCH
					ENDIF
				BREAK
				
				//FREEMODE
				DEFAULT
					IF specData.specHUDData.iTickerStage >= HUD_TICKER_FREEMODE_STAGES
						specData.specHUDData.iTickerStage = 0
					ENDIF
					
					SWITCH specData.specHUDData.iTickerStage
						CASE 0
							thisTL = GET_TIME_TEXT_LABEL(GET_CURRENT_TIMEOFDAY())
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(thisTL)					///"SPEC_FREE")
							specData.specHUDData.TickerUpdateDelay = HUD_TICKER_FREEMODE_1
							specData.specHUDData.iTickerStage++
						BREAK
						CASE 1
							ADD_GAME_LOCATION_TO_TICKER(specData)
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
			
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC

/// PURPOSE:
///    Creates a GTAOTV ticker message detailing the player being currently watched
/// PARAMS:
///    specData - 
PROC SETUP_GTAOTV_HUD_TICKER_CURRENTLY_WATCHING(SPECTATOR_DATA_STRUCT &specData)
	IF MPSpecGlobals.SCTVData.currentMode = SCTV_MODE_FOLLOW_CAM
		IF HAS_SCALEFORM_MOVIE_LOADED(specData.specHUDData.sfFilter)
			TEXT_LABEL_15 tlCrewTag
			GET_PLAYER_CREW_TAG_FOR_SCALEFORM(NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_SPECTATOR_CURRENT_FOCUS_PED()), tlCrewTag)
		
			BEGIN_SCALEFORM_MOVIE_METHOD(specData.specHUDData.sfFilter, "ADD_TICKER_TEXT")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)	//Type text
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("SPEC_CRWT")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)	//Type gamertag
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_SPECTATOR_CURRENT_FOCUS_PED())))
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)	//Type crewtag
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_LITERAL_STRING(tlCrewTag)
			END_SCALEFORM_MOVIE_METHOD()
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Gets two randomised news headline text labels
/// PARAMS:
///    tl15Line0 - store text label 1
///    tl15Line1 - store text label 2
PROC GET_NEWS_HUD_TEXT_LABELS(TEXT_LABEL_15 &tl15Line0, TEXT_LABEL_15 &tl15Line1)
	
	tl15Line0 = "MPTV_OTICK"
	tl15Line1 = "MPTV_OTICK"
	
	INT iRandom0, iRandom1
	
	iRandom0 = GET_RANDOM_INT_IN_RANGE(0, 16)
	iRandom1 = GET_RANDOM_INT_IN_RANGE(0, 16)
	
	IF iRandom0 = iRandom1
		IF iRandom0 > 8
			iRandom1 = iRandom0-1
		ELSE
			iRandom1 = iRandom0+1
		ENDIF
	ENDIF
	
	tl15Line0 += iRandom0
	tl15Line1 += iRandom1
	
ENDPROC

#IF FEATURE_COPS_N_CROOKS
PROC REFRESH_SPEC_HUD_CNC_1_INFO(SPECTATOR_DATA_STRUCT &specData)
	BEGIN_SCALEFORM_MOVIE_METHOD(specData.specHUDData.sfFilter, "SHOW_WEAZEL_NEWS_2020_WITH_FIRST_HEADER")
	END_SCALEFORM_MOVIE_METHOD()
	
	PLAY_SOUND_FRONTEND(-1,"logo_stinger_short","arc1_cnc_ui_weazel_news_sounds") 
ENDPROC
#ENDIF

/// PURPOSE:
///    Repopulates the news filter scaleform with spectate target info, and randomised headlines
/// PARAMS:
///    specData - 
PROC REFRESH_SPEC_HUD_NEWS_INFO(SPECTATOR_DATA_STRUCT &specData)
	GAMER_HANDLE gamerPlayer
	NETWORK_CLAN_DESC crewPlayer
	TEXT_LABEL_15 tl15Line0, tl15Line1
	GET_NEWS_HUD_TEXT_LABELS(tl15Line0, tl15Line1)
	
	PLAYER_INDEX piTemp = INVALID_PLAYER_INDEX()	
	IF DOES_ENTITY_EXIST(GET_SPECTATOR_CURRENT_FOCUS_PED())	
		piTemp = NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_SPECTATOR_CURRENT_FOCUS_PED())
	ENDIF
	
	BEGIN_SCALEFORM_MOVIE_METHOD(specData.specHUDData.sfFilter, "SET_TEXT")
		SWITCH GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType
			CASE FMMC_TYPE_DEATHMATCH
			CASE FMMC_TYPE_RACE
			CASE FMMC_TYPE_SURVIVAL
			CASE FMMC_TYPE_MISSION
			//CASE FMMC_TYPE_TRIATHLON
			CASE FMMC_TYPE_BASE_JUMP
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(GET_SPEC_CAM_TEXT_LABLE_FOR_MISSION_TYPE(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType))
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(g_FMMC_STRUCT.tl63MissionName)
			BREAK
			DEFAULT
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("SPC_TXT_DFLT")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")	//SPEC_SCTV")
			BREAK
		ENDSWITCH
	END_SCALEFORM_MOVIE_METHOD()
	BEGIN_SCALEFORM_MOVIE_METHOD(specData.specHUDData.sfFilter, "CLEAR_SCROLL_TEXT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
	END_SCALEFORM_MOVIE_METHOD()
	
	IF piTemp != INVALID_PLAYER_INDEX()	
	AND NETWORK_IS_PLAYER_ACTIVE(piTemp)	
		BEGIN_SCALEFORM_MOVIE_METHOD(specData.specHUDData.sfFilter, "SET_SCROLL_TEXT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_LABEL_WITH_PLAYER_NAME("MPTV_TICK0", GET_PLAYER_NAME(piTemp))
		END_SCALEFORM_MOVIE_METHOD()
	
		BEGIN_SCALEFORM_MOVIE_METHOD(specData.specHUDData.sfFilter, "SET_SCROLL_TEXT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(1)
			gamerPlayer = GET_GAMER_HANDLE_PLAYER(NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_SPECTATOR_CURRENT_FOCUS_PED()))
			IF IS_PLAYER_IN_ACTIVE_CLAN(gamerPlayer)
				crewPlayer = GET_PLAYER_CREW(NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_SPECTATOR_CURRENT_FOCUS_PED()))
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_LABEL_WITH_TL63("MPTV_TICK1", GET_CREW_CLAN_NAME(gamerPlayer, crewPlayer))
			ELSE
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MPTV_TICK2")
			ENDIF
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
	
	BEGIN_SCALEFORM_MOVIE_METHOD(specData.specHUDData.sfFilter, "SET_SCROLL_TEXT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(2)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(tl15Line0)
	END_SCALEFORM_MOVIE_METHOD()
	BEGIN_SCALEFORM_MOVIE_METHOD(specData.specHUDData.sfFilter, "SET_SCROLL_TEXT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(3)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(tl15Line1)
	END_SCALEFORM_MOVIE_METHOD()
		
	IF NOT IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_BLOCK_CHANGING_RADAR)
		DISPLAY_RADAR(FALSE)
	ENDIF
	
	specData.specHUDData.fNewsFilterScrollEntry = 0
	
	BEGIN_SCALEFORM_MOVIE_METHOD(specData.specHUDData.sfFilter, "DISPLAY_SCROLL_TEXT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(specData.specHUDData.fNewsFilterScrollEntry)
	END_SCALEFORM_MOVIE_METHOD()
	
	specData.specHUDData.timeNewsFilterNextUpdate = GET_NETWORK_TIME()
	
	CLEAR_REDO_NEWS_HUD_DUE_TO_EVENT()
	
ENDPROC

FUNC BOOL SHOULD_DO_SECURITY_CAM_FUZZ(SPECTATOR_DATA_STRUCT &specData)

	UNUSED_PARAMETER(specData)

	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_OVERTIME_ANY(g_FMMC_STRUCT.iAdversaryModeType)
		RETURN FALSE
	ENDIF
	
	#IF FEATURE_COPS_N_CROOKS
	IF specData.specHUDData.eDesiredFilter = SPEC_HUD_FILTER_CNC_1
		RETURN FALSE
	ENDIF
	#ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Handles cleaning up and initialising filters
/// PARAMS:
///    specData - 
PROC MANAGE_SPEC_HUD_FILTERS(SPECTATOR_DATA_STRUCT &specData)
	BOOL bFinished = FALSE
	
	IF IS_BIT_SET(MPSpecGlobals.SCTVData.iBitSet, SCTV_BIT_REFRESH_FILTER)
		specData.specHUDData.iFilterStage = 0
		CPRINTLN(DEBUG_SPECTATOR, "=== HUD === MANAGE_SPEC_HUD_FILTERS - specHUDData.eDesiredFilter = ", ENUM_TO_INT(specData.specHUDData.iFilterStage))
		CLEAR_BIT(MPSpecGlobals.SCTVData.iBitSet, SCTV_BIT_REFRESH_FILTER)
		CPRINTLN(DEBUG_SPECTATOR, "=== HUD === MANAGE_SPEC_HUD_FILTERS - SCTV_BIT_REFRESH_FILTER CLEARED")
	ENDIF
	
	#IF FEATURE_COPS_N_CROOKS
	IF IS_FREEMODE_ARCADE()
	AND GET_ARCADE_MODE() = ARC_COPS_CROOKS
		specData.specHUDData.eDesiredFilter = SPEC_HUD_FILTER_CNC_1
	ENDIF
	#ENDIF
	
	/*IF specData.specHUDData.eCurrentFilter = SPEC_HUD_FILTER_SPEC_1		//SPEC_HUD_FILTER_NONE
	AND specData.specHUDData.eDesiredFilter = SPEC_HUD_FILTER_SPEC_1	//SPEC_HUD_FILTER_NONE
		specData.specHUDData.iFilterStage = 99 //switch to final stage, we've just started up
	ENDIF*/
	
	SWITCH specData.specHUDData.iFilterStage
		CASE 0	//noise on
			IF SHOULD_DO_SECURITY_CAM_FUZZ(specData)
				SET_TIMECYCLE_MODIFIER("CAMERA_secuirity_FUZZ")
				SPEC_HUD_SOUND_START_CHANGE_FILTER(specData.specHUDData)
				CPRINTLN(DEBUG_SPECTATOR, "=== HUD === MANAGE_SPEC_HUD_FILTERS - noise on, specData.specHUDData.iFilterStage++")
			ELSE
				CPRINTLN(DEBUG_SPECTATOR, "=== HUD === MANAGE_SPEC_HUD_FILTERS - skip noise for overtime, specData.specHUDData.iFilterStage++")
			ENDIF
			
			specData.specHUDData.iFilterChangeTime = GET_GAME_TIMER()
			specData.specHUDData.iFilterStage++
		BREAK
		CASE 1	//cleanup current
			SWITCH specData.specHUDData.eCurrentFilter
				//CASE SPEC_HUD_FILTER_NONE
				CASE SPEC_HUD_FILTER_SPEC_1
				CASE SPEC_HUD_FILTER_SPEC_2
				CASE SPEC_HUD_FILTER_SPEC_3
				CASE SPEC_HUD_FILTER_SPEC_4
				CASE SPEC_HUD_FILTER_SPEC_5
				CASE SPEC_HUD_FILTER_SPEC_6
				CASE SPEC_HUD_FILTER_SPEC_7
				CASE SPEC_HUD_FILTER_SPEC_8
				CASE SPEC_HUD_FILTER_SPEC_9
				CASE SPEC_HUD_FILTER_SPEC_10
				/*CASE SPEC_HUD_FILTER_SNAP_1
				CASE SPEC_HUD_FILTER_SNAP_2
				CASE SPEC_HUD_FILTER_SNAP_3
				CASE SPEC_HUD_FILTER_SNAP_4
				CASE SPEC_HUD_FILTER_SNAP_5
				CASE SPEC_HUD_FILTER_SNAP_6
				CASE SPEC_HUD_FILTER_SNAP_7
				CASE SPEC_HUD_FILTER_SNAP_8
				CASE SPEC_HUD_FILTER_SNAP_9*/
					
					bFinished = TRUE
				BREAK
				
				#IF FEATURE_COPS_N_CROOKS
				CASE SPEC_HUD_FILTER_CNC_1
					IF specData.specHUDData.iFrameDelay = (-1)
						BEGIN_SCALEFORM_MOVIE_METHOD(specData.specHUDData.sfFilter, "HIDE_WEAZEL_NEWS_2020")
						END_SCALEFORM_MOVIE_METHOD()
						specData.specHUDData.iFrameDelay = GET_FRAME_COUNT()
						bFinished = FALSE
						CPRINTLN(DEBUG_SPECTATOR, "=== HUD === MANAGE_SPEC_HUD_FILTERS - OUTRO FILTER CNC_1 - WEAZEL_NEWS_2020")
					ELIF GET_FRAME_COUNT() >= (specData.specHUDData.iFrameDelay+26)
					
						DISPLAY_RADAR(TRUE)
						SET_SCTV_TICKER_SCORE_ON()
					
						IF ANIMPOSTFX_IS_RUNNING("CnC_SpectatorTV")
							ANIMPOSTFX_STOP("CnC_SpectatorTV")
						ENDIF
						
						RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_ARC1/Arc1_CnC_Weazel_News_Stinger") 
						
						specData.specHUDData.iFrameDelay = -1
					
						SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(specData.specHUDData.sfFilter)
						CPRINTLN(DEBUG_SPECTATOR, "=== HUD === MANAGE_SPEC_HUD_FILTERS - CLEANUP FILTER CNC_1 - WEAZEL_NEWS_2020")
						bFinished = TRUE
					ENDIF
				BREAK
				#ENDIF
				
				CASE SPEC_HUD_FILTER_NEWS
					//IF NOT IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_BLOCK_CHANGING_RADAR)
						DISPLAY_RADAR(TRUE)
					//ENDIF
					SET_SCTV_TICKER_SCORE_ON()
					SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(specData.specHUDData.sfFilter)
	
					bFinished = TRUE
				BREAK
				CASE SPEC_HUD_FILTER_GTAOTV
					IF NETWORK_IS_ACTIVITY_SESSION()
						IF NOT IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_BLOCK_CHANGING_RADAR)
							DISPLAY_RADAR(TRUE)
						ENDIF
						CLEANUP_SCTV_TICKER_QUEUE_LOCAL_VARS(specData.specHUDData.sSctvQueueLocalData)
						SET_SCTV_TICKER_UI_OFF()
						bFinished = TRUE
					ELSE
						IF NOT IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_BLOCK_CHANGING_RADAR)
							DISPLAY_RADAR(TRUE)
						ENDIF
						SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(specData.specHUDData.sfFilter)
						bFinished = TRUE
					ENDIF
				BREAK
			ENDSWITCH
			IF bFinished
				#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SPECTATOR, "=== HUD === MANAGE_SPEC_HUD_FILTERS - cleanup current, specData.specHUDData.iFilterStage++")
			#ENDIF
				specData.specHUDData.iFilterStage++
			ENDIF
		BREAK
		CASE 2	//load in desired
			SWITCH specData.specHUDData.eDesiredFilter
				//CASE SPEC_HUD_FILTER_NONE
				CASE SPEC_HUD_FILTER_SPEC_1
				CASE SPEC_HUD_FILTER_SPEC_2
				CASE SPEC_HUD_FILTER_SPEC_3
				CASE SPEC_HUD_FILTER_SPEC_4
				CASE SPEC_HUD_FILTER_SPEC_5
				CASE SPEC_HUD_FILTER_SPEC_6
				CASE SPEC_HUD_FILTER_SPEC_7
				CASE SPEC_HUD_FILTER_SPEC_8
				CASE SPEC_HUD_FILTER_SPEC_9
				CASE SPEC_HUD_FILTER_SPEC_10
				/*CASE SPEC_HUD_FILTER_SNAP_1
				CASE SPEC_HUD_FILTER_SNAP_2
				CASE SPEC_HUD_FILTER_SNAP_3
				CASE SPEC_HUD_FILTER_SNAP_4
				CASE SPEC_HUD_FILTER_SNAP_5
				CASE SPEC_HUD_FILTER_SNAP_6
				CASE SPEC_HUD_FILTER_SNAP_7
				CASE SPEC_HUD_FILTER_SNAP_8
				CASE SPEC_HUD_FILTER_SNAP_9*/
					bFinished = TRUE
				BREAK
				
				#IF FEATURE_COPS_N_CROOKS
				CASE SPEC_HUD_FILTER_CNC_1
					specData.specHUDData.sfFilter = REQUEST_SCALEFORM_MOVIE("WEAZEL_NEWS_2020")

					IF HAS_SCALEFORM_MOVIE_LOADED(specData.specHUDData.sfFilter)
					AND REQUEST_SCRIPT_AUDIO_BANK("DLC_ARC1/Arc1_CnC_Weazel_News_Stinger")
						IF NOT IS_SCREEN_FADED_OUT()
						AND NOT IS_SCREEN_FADING_IN()
						AND NOT IS_SCREEN_FADING_OUT()
							IF NOT IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_BLOCK_CHANGING_RADAR)
								DISPLAY_RADAR(FALSE)
							ENDIF
						
							REFRESH_SPEC_HUD_CNC_1_INFO(specData)
							SET_SCTV_TICKER_SCORE_OFF()
							bFinished = TRUE
							CPRINTLN(DEBUG_SPECTATOR, "=== HUD === MANAGE_SPEC_HUD_FILTERS - LOADED CNC_1 - WEAZEL_NEWS_2020")
						ELSE
							CPRINTLN(DEBUG_SPECTATOR, "=== HUD === MANAGE_SPEC_HUD_FILTERS - LOADED CNC_1 - WEAZEL_NEWS_2020 - Waiting on screen fade for intro.")
						ENDIF
					ENDIF
				BREAK
				#ENDIF
				
				CASE SPEC_HUD_FILTER_NEWS
					specData.specHUDData.sfFilter = REQUEST_SCALEFORM_MOVIE("BREAKING_NEWS")
					IF HAS_SCALEFORM_MOVIE_LOADED(specData.specHUDData.sfFilter)
						IF NOT IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_BLOCK_CHANGING_RADAR)
							DISPLAY_RADAR(FALSE)
						ENDIF
						SET_SCTV_TICKER_SCORE_OFF()
						REFRESH_SPEC_HUD_NEWS_INFO(specData)
						bFinished = TRUE
					ENDIF
				BREAK
				CASE SPEC_HUD_FILTER_GTAOTV
					IF NETWORK_IS_ACTIVITY_SESSION()
						IF NOT IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_BLOCK_CHANGING_RADAR)
							DISPLAY_RADAR(FALSE)
						ENDIF
						bFinished = TRUE
					ELSE
						specData.specHUDData.sfFilter = REQUEST_SCALEFORM_MOVIE("SOCIAL_CLUB_TV")
						IF HAS_SCALEFORM_MOVIE_LOADED(specData.specHUDData.sfFilter)
						
							SETUP_GTAOTV_HUD_LIVE_PANEL(specData, TRUE)
							
							SETUP_GTAOTV_HUD_TICKER_PANEL(specData, FALSE)	//TRUE)
							
							SETUP_GTAOTV_HUD_TICKER_CURRENTLY_WATCHING(specData)
							
							IF NOT IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_BLOCK_CHANGING_RADAR)
								DISPLAY_RADAR(FALSE)
							ENDIF
							
							bFinished = TRUE
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
			IF bFinished
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_SPECTATOR, "=== HUD === MANAGE_SPEC_HUD_FILTERS - load in desired, specData.specHUDData.iFilterStage++")
				#ENDIF
				specData.specHUDData.iFilterStage++
			ENDIF
		BREAK
		CASE 3 //wait for timer
			IF GET_GAME_TIMER() - specData.specHUDData.iFilterChangeTime > SPEC_HUD_FILTER_CHANGE_TIME
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_SPECTATOR, "=== HUD === MANAGE_SPEC_HUD_FILTERS - wait for timer, specData.specHUDData.iFilterStage++")
				#ENDIF
				specData.specHUDData.iFilterStage++
			ENDIF
		BREAK
		CASE 4	//noise off
			SWITCH specData.specHUDData.eDesiredFilter
				CASE SPEC_HUD_FILTER_SPEC_1
					SET_TIMECYCLE_MODIFIER("spectator1")
				BREAK
				CASE SPEC_HUD_FILTER_SPEC_2
					SET_TIMECYCLE_MODIFIER("spectator2")
				BREAK
				CASE SPEC_HUD_FILTER_SPEC_3
					SET_TIMECYCLE_MODIFIER("spectator3")
				BREAK
				CASE SPEC_HUD_FILTER_SPEC_4
					SET_TIMECYCLE_MODIFIER("spectator4")
				BREAK
				CASE SPEC_HUD_FILTER_SPEC_5
					SET_TIMECYCLE_MODIFIER("spectator5")
				BREAK
				CASE SPEC_HUD_FILTER_SPEC_6
					SET_TIMECYCLE_MODIFIER("spectator6")
				BREAK
				CASE SPEC_HUD_FILTER_SPEC_7
					SET_TIMECYCLE_MODIFIER("spectator7")
				BREAK
				CASE SPEC_HUD_FILTER_SPEC_8
					SET_TIMECYCLE_MODIFIER("spectator8")
				BREAK
				CASE SPEC_HUD_FILTER_SPEC_9
					SET_TIMECYCLE_MODIFIER("spectator9")
				BREAK
				CASE SPEC_HUD_FILTER_SPEC_10
					SET_TIMECYCLE_MODIFIER("spectator10")
				BREAK
				
				/*CASE SPEC_HUD_FILTER_SNAP_1
					SET_TIMECYCLE_MODIFIER("phone_cam1")
				BREAK
				CASE SPEC_HUD_FILTER_SNAP_2
					SET_TIMECYCLE_MODIFIER("phone_cam2")
				BREAK
				CASE SPEC_HUD_FILTER_SNAP_3
					SET_TIMECYCLE_MODIFIER("phone_cam3")
				BREAK
				CASE SPEC_HUD_FILTER_SNAP_4
					SET_TIMECYCLE_MODIFIER("phone_cam4")
				BREAK
				CASE SPEC_HUD_FILTER_SNAP_5
					SET_TIMECYCLE_MODIFIER("phone_cam5")
				BREAK
				CASE SPEC_HUD_FILTER_SNAP_6
					SET_TIMECYCLE_MODIFIER("phone_cam6")
				BREAK
				CASE SPEC_HUD_FILTER_SNAP_7
					SET_TIMECYCLE_MODIFIER("phone_cam7")
				BREAK
				CASE SPEC_HUD_FILTER_SNAP_8
					SET_TIMECYCLE_MODIFIER("phone_cam9")
				BREAK
				CASE SPEC_HUD_FILTER_SNAP_9
					SET_TIMECYCLE_MODIFIER("phone_cam12")
				BREAK*/
				
				CASE SPEC_HUD_FILTER_NEWS
				CASE SPEC_HUD_FILTER_GTAOTV
					SET_TIMECYCLE_MODIFIER("spectator1")
				BREAK
				
				#IF FEATURE_COPS_N_CROOKS
				CASE SPEC_HUD_FILTER_CNC_1
					ANIMPOSTFX_PLAY("CnC_SpectatorTV", 0, FALSE)
					CPRINTLN(DEBUG_SPECTATOR, "=== HUD === MANAGE_SPEC_HUD_FILTERS - SETUP FILTER CNC_1 - WEAZEL_NEWS_2020")
				BREAK
				#ENDIF
				
				//CASE SPEC_HUD_FILTER_NONE
				//	CLEAR_TIMECYCLE_MODIFIER()
				//BREAK
			ENDSWITCH
			
			SPEC_HUD_SOUND_STOP_CHANGE_FILTER(specData.specHUDData)
			
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SPECTATOR, "=== HUD === MANAGE_SPEC_HUD_FILTERS - noise off, specData.specHUDData.iFilterStage++")
			#ENDIF
			specData.specHUDData.iFilterStage++
		BREAK
		CASE 5	//set current to desired
			specData.specHUDData.eCurrentFilter = specData.specHUDData.eDesiredFilter
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SPECTATOR, "=== HUD === MANAGE_SPEC_HUD_FILTERS - set current to desired, specData.specHUDData.iFilterStage = 99")
			#ENDIF
			specData.specHUDData.iFilterStage = 99
			specData.specHUDData.iFrameDelay = -1
		BREAK
		CASE 99	//Run the filter
			#IF FEATURE_COPS_N_CROOKS			
			IF specData.specHUDData.eCurrentFilter = SPEC_HUD_FILTER_CNC_1
				THEFEED_HIDE_THIS_FRAME()
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_SUBTITLE_TEXT)
				g_b_HoldOffDrawingTimersThisFrame = TRUE
			ENDIF
			#ENDIF
			
			IF specData.specHUDData.eCurrentFilter = SPEC_HUD_FILTER_NEWS
			
				THEFEED_HIDE_THIS_FRAME()
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_SUBTITLE_TEXT)
				g_b_HoldOffDrawingTimersThisFrame = TRUE
				
				
				IF NOT IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_NEWS_FILTER)
					SET_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_NEWS_FILTER)
					PRINTLN("[BCTIMERS] GLOBAL_SPEC_BS_NEWS_FILTER so calling g_b_HoldOffDrawingTimersThisFrame = TRUE ")
				ENDIF
				
				IF SHOULD_REDO_NEWS_HUD_DUE_TO_EVENT()
					REFRESH_SPEC_HUD_NEWS_INFO(specData)
				ENDIF
				
				IF NETWORK_IS_GAME_IN_PROGRESS()
					IF GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), specData.specHUDData.timeNewsFilterNextUpdate) >= SPEC_HUD_NEWS_TICKER_TIME_UPDATE
						IF NOT IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_BLOCK_CHANGING_RADAR)
							DISPLAY_RADAR(FALSE)
						ENDIF
						
						specData.specHUDData.fNewsFilterScrollEntry++
						
						IF specData.specHUDData.fNewsFilterScrollEntry >= SPEC_HUD_NUMBER_OF_NEWS_TICKER
							specData.specHUDData.fNewsFilterScrollEntry = 0
						ENDIF
						
						BEGIN_SCALEFORM_MOVIE_METHOD(specData.specHUDData.sfFilter, "DISPLAY_SCROLL_TEXT")
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0)
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(specData.specHUDData.fNewsFilterScrollEntry)
						END_SCALEFORM_MOVIE_METHOD()
						
						specData.specHUDData.timeNewsFilterNextUpdate = GET_NETWORK_TIME()
					ENDIF
				ENDIF
			ELSE
				IF IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_NEWS_FILTER)
					CLEAR_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_NEWS_FILTER)
					PRINTLN("[BCTIMERS] GLOBAL_SPEC_BS_NEWS_FILTER cleared so stop calling g_b_HoldOffDrawingTimersThisFrame ")
				ENDIF
			ENDIF
			IF specData.specHUDData.eCurrentFilter = SPEC_HUD_FILTER_GTAOTV
				//Update HUD
				IF NETWORK_IS_ACTIVITY_SESSION()
					MAINTAIN_SCTV_TICKER_QUEUE(specData.specHUDData.sSctvQueueLocalData)
				ELSE
					IF IS_BIT_SET(MPSpecGlobals.SCTVData.iBitSet, SCTV_BIT_UPDATE_HUD)

						//SETUP_GTAOTV_HUD_LIVE_PANEL(specData, TRUE)
						SETUP_GTAOTV_HUD_TICKER_PANEL(specData, FALSE)	//TRUE)
						CLEAR_BIT(MPSpecGlobals.SCTVData.iBitSet, SCTV_BIT_UPDATE_HUD)
						
					//Update on Timer
					ELSE
						IF HAS_NET_TIMER_EXPIRED(specData.specHUDData.TickerUpdateTimer, specData.specHUDData.TickerUpdateDelay)
							SET_BIT(MPSpecGlobals.SCTVData.iBitSet, SCTV_BIT_UPDATE_HUD)
							RESET_NET_TIMER(specData.specHUDData.TickerUpdateTimer)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
ENDPROC

/// PURPOSE:
///    Performs every-frame processes to render the filters
/// PARAMS:
///    specHUDData - 
PROC MANAGE_SPEC_HUD_FILTER_RENDERING(SPECTATOR_HUD_DATA &specHUDData)
	SWITCH specHUDData.eCurrentFilter
		#IF FEATURE_COPS_N_CROOKS		
		CASE SPEC_HUD_FILTER_CNC_1
			IF specHUDData.iFilterStage = 99
				IF HAS_SCALEFORM_MOVIE_LOADED(specHUDData.sfFilter)
					//SET_TEXT_RENDER_ID (specHUDData.screenRT)
					DRAW_SCALEFORM_MOVIE_FULLSCREEN(specHUDData.sfFilter, 255,255,255,0)
					IF NOT IS_RADAR_HIDDEN()
						DISPLAY_RADAR(FALSE) //Make sure the Radar is Hidden
						CPRINTLN(DEBUG_SPECTATOR, 	"=== HUD === MANAGE_SPEC_HUD_FILTER_RENDERING - DISPLAY_RADAR(FALSE)")
					ENDIF
				ENDIF
			ENDIF
		BREAK
		#ENDIF
		CASE SPEC_HUD_FILTER_NEWS
		CASE SPEC_HUD_FILTER_GTAOTV
			IF GET_SPECTATOR_HUD_STAGE(specHUDData) <> SPEC_HUD_STAGE_LEADERBOARD
			AND NOT IS_SPECTATOR_RUNNING_CUTSCENE()
			AND NOT g_bCelebrationScreenIsActive
				IF specHUDData.iFilterStage = 99
					IF HAS_SCALEFORM_MOVIE_LOADED(specHUDData.sfFilter)
						SET_TEXT_RENDER_ID (specHUDData.screenRT)
						DRAW_SCALEFORM_MOVIE_FULLSCREEN(specHUDData.sfFilter, 255,255,255,0)
						IF NOT IS_RADAR_HIDDEN()
							DISPLAY_RADAR(FALSE) //Make sure the Radar is Hidden
							CPRINTLN(DEBUG_SPECTATOR, 	"=== HUD === MANAGE_SPEC_HUD_FILTER_RENDERING - DISPLAY_RADAR(FALSE)")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Checks input for actions that can occur in all stages
/// PARAMS:
///    specData - 
PROC MANAGE_SHARED_INPUT(SPECTATOR_DATA_STRUCT &specData)

	PED_INDEX pedHighlight
	PLAYER_INDEX playerHighlight
	GAMER_HANDLE ghHighlight	

	#IF IS_DEBUG_BUILD
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_DisplaySCTVStages")
			IF GET_FRAME_COUNT() % 10 = 0
				CPRINTLN(DEBUG_SPECTATOR, 	"=== SCTVSTAGES === MANAGE_SHARED_INPUT: specData.specHUDData.specHUDMode = ", GET_SPEC_HUD_MODE_NAME(specData.specHUDData.specHUDMode))
				CPRINTLN(DEBUG_SPECTATOR, 	"=== SCTVSTAGES === MANAGE_SHARED_INPUT: IS_PLAYER_SCTV(PLAYER_ID()) = ", IS_PLAYER_SCTV(PLAYER_ID()))
				CPRINTLN(DEBUG_SPECTATOR, 	"=== SCTVSTAGES === MANAGE_SHARED_INPUT: IS_PLAYER_A_ROCKSTAR_DEV = ", IS_PLAYER_A_ROCKSTAR_DEV())
			ENDIF
		ENDIF
	#ENDIF
	
	// Don't accept any of the below inputs if the chat box is onscreen
	IF NETWORK_TEXT_CHAT_IS_TYPING()
	OR IS_BIT_SET(specData.specHUDData.iBitset, SPEC_CAM_DISABLE_ALL_CONTROLS)
		EXIT
	ENDIF
	
	//Rockstar Dev - DpadDown Map
	IF IS_PLAYER_A_ROCKSTAR_DEV()
		IF specData.specHUDData.specHUDMode = SPEC_HUD_MODE_FULL
		OR specData.specHUDData.specHUDMode = SPEC_HUD_MODE_MINIMAL
			IF IS_PLAYER_SCTV(PLAYER_ID())
				IF IS_SCREEN_FADED_IN()
					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_RS))
						IF NOT IS_BIT_SET(specData.specHUDData.iBitset, SPEC_HUD_BS_SHOW_DPAD_DOWN_MAP)
							SET_BIGMAP_ACTIVE(TRUE, FALSE)
							DISPLAY_PLAYER_NAME_TAGS_ON_BLIPS(TRUE) 
							SET_DISPLAY_PLAYER_NAME_TAGS_ON_BLIPS(TRUE)
							SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_SHOW_DPAD_DOWN_MAP)
							CPRINTLN(DEBUG_SPECTATOR, 	"=== SCTVSTAGES === MANAGE_SHARED_INPUT: SPEC_HUD_BS_SHOW_DPAD_DOWN_MAP SET")
						ELSE
							SET_BIGMAP_ACTIVE(FALSE, FALSE) 
							DISPLAY_PLAYER_NAME_TAGS_ON_BLIPS(FALSE) 
							SET_DISPLAY_PLAYER_NAME_TAGS_ON_BLIPS(FALSE)
							CLEAR_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_SHOW_DPAD_DOWN_MAP)
							CPRINTLN(DEBUG_SPECTATOR, 	"=== SCTVSTAGES === MANAGE_SHARED_INPUT: SPEC_HUD_BS_SHOW_DPAD_DOWN_MAP CLEARED")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//Show Feed/Player List
	IF specData.specHUDData.iCurrentNumberOfTargetsInAllLists > SPEC_HUD_NUM_PLAYERS_TO_HIDE_FEED
		IF specData.specHUDData.eSpecHUDStage <> SPEC_HUD_STAGE_LEADERBOARD
			IF specData.specHUDData.specHUDMode = SPEC_HUD_MODE_FULL
			OR specData.specHUDData.specHUDMode = SPEC_HUD_MODE_MINIMAL
				IF IS_SCREEN_FADED_IN()
					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_LS))
					OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_LS))
						IF NOT IS_BIT_SET(specData.specHUDData.iBitset, SPEC_HUD_BS_SHOW_FEED)
							THEFEED_FLUSH_QUEUE()
							SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_SHOW_FEED)
							CPRINTLN(DEBUG_SPECTATOR, 	"=== SCTVSTAGES === MANAGE_SHARED_INPUT: SPEC_HUD_BS_SHOW_FEED - SET - THEFEED_FLUSH_QUEUE")
							SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_REQUEST_UPDATE_INSTRUCTIONAL_BUTTONS)
						ELSE
							CLEAR_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_SHOW_FEED)
							CPRINTLN(DEBUG_SPECTATOR, 	"=== SCTVSTAGES === MANAGE_SHARED_INPUT: SPEC_HUD_BS_SHOW_FEED - CLEARED")
							SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_REQUEST_UPDATE_INSTRUCTIONAL_BUTTONS)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF IS_BIT_SET(specData.specHUDData.iBitset, SPEC_HUD_BS_SHOW_FEED)
			THEFEED_FLUSH_QUEUE()
			CLEAR_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_SHOW_FEED)
			CPRINTLN(DEBUG_SPECTATOR, 	"=== SCTVSTAGES === MANAGE_SHARED_INPUT: SPEC_HUD_BS_SHOW_FEED - CLEARED - SMALL LIST - THEFEED_FLUSH_QUEUE")
			SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_REQUEST_UPDATE_INSTRUCTIONAL_BUTTONS)
		ENDIF
	ENDIF
	
	IF specData.specHUDData.specHUDMode = SPEC_HUD_MODE_FULL
		IF IS_PLAYER_SCTV(PLAYER_ID())
			IF IS_SCREEN_FADED_IN()
				IF IS_PLAYER_A_ROCKSTAR_DEV()
				
				//Handle rockstar dev SCTV players being able to switch camera mode
				
					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_RB))
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_SPECTATOR, "=== HUD === SCTV PRESSED RB!")
						#ENDIF
						IF IS_SAFE_TO_SWITCH_SCTV_MODE()
							IF GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), specData.specHUDData.timeSwitchProtect) >= TIME_SWITCH_PROTECT_VALUE
								SWITCH GET_SCTV_MODE() 
									CASE SCTV_MODE_FOLLOW_CAM
										SWITCH_SCTV_MODE(SCTV_MODE_FREE_CAM)
										specData.specHUDData.timeSwitchProtect = GET_NETWORK_TIME()
										SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_REQUEST_UPDATE_INSTRUCTIONAL_BUTTONS)
										SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_REQUEST_REDO_SUMMARY_CARD)		//Update HUD display of lists
									BREAK
									CASE SCTV_MODE_FREE_CAM
										IF ARE_SPECTATOR_HUD_TARGET_LISTS_POPULATED(specData.specHUDData)
											SWITCH_SCTV_MODE(SCTV_MODE_FOLLOW_CAM)
											specData.specHUDData.timeSwitchProtect = GET_NETWORK_TIME()
											SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_REQUEST_UPDATE_INSTRUCTIONAL_BUTTONS)
											SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_REQUEST_REDO_SUMMARY_CARD)		//Update HUD display of lists
										ELSE
											#IF IS_DEBUG_BUILD
											CPRINTLN(DEBUG_SPECTATOR, "=== HUD === ARE_SPECTATOR_HUD_TARGET_LISTS_POPULATED = FALSE")
											#ENDIF
										ENDIF
									BREAK
								ENDSWITCH
								SET_BIT(MPSpecGlobals.SCTVData.iBitSet, SCTV_BIT_UPDATE_HUD)
							ELSE
								#IF IS_DEBUG_BUILD
								CPRINTLN(DEBUG_SPECTATOR, "=== HUD === GET_TIME_DIFFERENCE < TIME_SWITCH_PROTECT_VALUE")
								#ENDIF
							ENDIF
						ELSE
							#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_SPECTATOR, "=== HUD === IS_SAFE_TO_SWITCH_SCTV_MODE = FALSE")
							#ENDIF
						ENDIF
					ENDIF
					IF (IS_CONTROL_PRESSED(FRONTEND_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_LS)) AND IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_RS)))
					OR (IS_CONTROL_PRESSED(FRONTEND_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_RS)) AND IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_LS)))
						TRIGGER_TRANSITION_MENU_ACTIVE(TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//Handle displaying highlighted player profile
		//IF specData.specHUDData.eSpecHUDStage <> SPEC_HUD_STAGE_LEADERBOARD
		IF GET_SCTV_MODE() != SCTV_MODE_FREE_CAM
			IF (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_LB)) AND specData.specHUDData.eSpecHUDStage <> SPEC_HUD_STAGE_LEADERBOARD)
			//OR (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_SELECT) AND specData.specHUDData.eSpecHUDStage = SPEC_HUD_STAGE_LEADERBOARD)
				pedHighlight = GET_SPECTATOR_HUD_CURRENTLY_HIGHLIGHTED_PED_INDEX(specData.specHUDData)
				IF DOES_ENTITY_EXIST(pedHighlight)
					IF IS_PED_A_PLAYER(pedHighlight)
						playerHighlight = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedHighlight)
						IF IS_NET_PLAYER_OK(playerHighlight, FALSE)
							PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
							ghHighlight = GET_GAMER_HANDLE_PLAYER(playerHighlight)
							NETWORK_SHOW_PROFILE_UI(ghHighlight)
							#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_SPECTATOR, "=== HUD === Displaying Player Profile for ", GET_PLAYER_NAME(playerHighlight))
							#ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF
	
	IF GET_SPEC_CAM_MODE(specData.SpecCamData) <> SPEC_MODE_NEWS
	AND GET_SCTV_MODE() != SCTV_MODE_FREE_CAM
		IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_PAD_LEFT))
			IF NOT IS_BIT_SET(specData.specCamData.iBitset, SPEC_CAM_BS_USE_RADIO_CONTROLS)
				IF NOT IS_SPECTATOR_FILTER_OPTION_DISABLED()
					PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
					SWITCH_TO_PREVIOUS_SPEC_HUD_FILTER(specData.specHUDData)
				ENDIF
			ELSE
				TURN_ON_SPECTATOR_RADIO(TRUE)
				UPDATE_CORONA_RADIO_STATION(-1)
				SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_REQUEST_UPDATE_INSTRUCTIONAL_BUTTONS)
				CPRINTLN(DEBUG_SPECTATOR, "=== HUD === MOVE RADIO STATION RIGHT")
			ENDIF
		ENDIF
		
		IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_PAD_RIGHT))
			IF NOT IS_BIT_SET(specData.specCamData.iBitset, SPEC_CAM_BS_USE_RADIO_CONTROLS)
				IF NOT IS_SPECTATOR_FILTER_OPTION_DISABLED()
					PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
					SWITCH_TO_NEXT_SPEC_HUD_FILTER(specData.specHUDData)
				ENDIF
			ELSE
				TURN_ON_SPECTATOR_RADIO(TRUE)
				UPDATE_CORONA_RADIO_STATION(1)
				SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_REQUEST_UPDATE_INSTRUCTIONAL_BUTTONS)
				CPRINTLN(DEBUG_SPECTATOR, "=== HUD === MOVE RADIO STATION RIGHT")
			ENDIF
		ENDIF
		
		//Radio Controls
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_RB))
			PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
			IF NOT IS_BIT_SET(specData.specCamData.iBitset, SPEC_CAM_BS_USE_RADIO_CONTROLS)
				SET_BIT(specData.specCamData.iBitset, SPEC_CAM_BS_USE_RADIO_CONTROLS)
				CPRINTLN(DEBUG_SPECTATOR, "=== HUD === RADIO CONTROLS SET ON - SPEC_CAM_BS_USE_RADIO_CONTROLS SET")
			ELSE
				CLEAR_BIT(specData.specCamData.iBitset, SPEC_CAM_BS_USE_RADIO_CONTROLS)
				CPRINTLN(DEBUG_SPECTATOR, "=== HUD === RADIO CONTROLS SET OFF - SPEC_CAM_BS_USE_RADIO_CONTROLS CLEARED")
			ENDIF
			SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_REQUEST_UPDATE_INSTRUCTIONAL_BUTTONS)
		ENDIF
		
		//IF GET_SPEC_CAM_MODE(specData.SpecCamData) <> SPEC_MODE_TV_CHANNEL
		//#IF FEATURE_HEIST_PLANNING AND GET_SPEC_CAM_MODE(specData.SpecCamData) <> SPEC_MODE_HEIST #ENDIF
		IF GET_SCTV_MODE() != SCTV_MODE_FREE_CAM
		AND NOT IS_BIT_SET(specData.specHUDData.iBitset, SPEC_HUD_BS_NO_VALID_TARGET_SHOW_LEADERBOARD)
			SWITCH specData.specHUDData.specHUDMode
				CASE SPEC_HUD_MODE_FULL
					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_RUP)) // go to minimal
						IF NOT IS_BIT_SET(specData.specCamData.iBitset, SPEC_CAM_BS_USE_RADIO_CONTROLS)
							PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
							SET_SPEC_HUD_MODE(specData.specHUDData, SPEC_HUD_MODE_MINIMAL)
						ELSE
							PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
							IF NOT IS_BIT_SET(MPSpecGlobals.iBailBitset, GLOBAL_SPEC_BAIL_BS_RADIO_ON)
								TURN_ON_SPECTATOR_RADIO(TRUE)
								CHANGE_CORONA_RADIO_STATION()
							ELSE
								TURN_ON_SPECTATOR_RADIO(FALSE)
							ENDIF
						ENDIF
						SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_REQUEST_UPDATE_INSTRUCTIONAL_BUTTONS)
					ENDIF
				BREAK
				CASE SPEC_HUD_MODE_MINIMAL
					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_RUP)) 			// Go to Hidden
						PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
						SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_REQUEST_UPDATE_INSTRUCTIONAL_BUTTONS)
						SET_SPEC_HUD_MODE(specData.specHUDData, SPEC_HUD_MODE_HIDDEN)
					ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, GET_SPECTATOR_ACCEPT_CANCEL_INPUT(TRUE)) 	// Go to Full
					OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, GET_SPECTATOR_ACCEPT_CANCEL_INPUT(FALSE))
					OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_PAD_DOWN))
					OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_LB))
					OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE)
					OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_RB))
					OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, GET_SPECTATOR_ACCEPT_CANCEL_INPUT(TRUE))
					OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_RLEFT))
					OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, GET_SPECTATOR_ACCEPT_CANCEL_INPUT(FALSE))
					OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_RUP))
					OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_PAD_UP))
						//OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_Y)		// Remove for Hidden
					//OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)		// Remove for Filter
					//OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)	// Remove for Filter
						PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
						SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_REQUEST_UPDATE_INSTRUCTIONAL_BUTTONS)
						SET_SPEC_HUD_MODE(specData.specHUDData, SPEC_HUD_MODE_FULL)
					ENDIF
				BREAK
				CASE SPEC_HUD_MODE_HIDDEN
					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, GET_SPECTATOR_ACCEPT_CANCEL_INPUT(TRUE)) 	// Go to Full
					OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, GET_SPECTATOR_ACCEPT_CANCEL_INPUT(FALSE))
					OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_PAD_DOWN))
					OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_LB))
					OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE)
					OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_RB))
					OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, GET_SPECTATOR_ACCEPT_CANCEL_INPUT(TRUE))
					OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_RLEFT))
					OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, GET_SPECTATOR_ACCEPT_CANCEL_INPUT(FALSE))
					OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_RUP))
					OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_PAD_UP))
					//OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)		// Remove for Filter
					//OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)	// Remove for Filter
						PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
						SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_REQUEST_UPDATE_INSTRUCTIONAL_BUTTONS)
						SET_SPEC_HUD_MODE(specData.specHUDData, SPEC_HUD_MODE_FULL)
					ENDIF
				BREAK
			ENDSWITCH
		ENDIF
	ENDIF
ENDPROC

PROC SET_LBD_BOOL(BOOL bJobStarted)
	IF bJobStarted
		IF g_bLbReady = FALSE
			PRINTLN("SET_LBD_BOOL TRUE")
			g_bLbReady = TRUE
		ENDIF
	ELSE
		IF g_bLbReady = TRUE
			g_bLbReady = FALSE
			PRINTLN("SET_LBD_BOOL FALSE")
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks input for actions that can occur in full list stage
/// PARAMS:
///    specData - 
PROC MANAGE_STAGE_FULL_LIST_INPUT(SPECTATOR_DATA_STRUCT &specData)

	// Don't accept any of the below inputs if the chat box is onscreen
	IF NETWORK_TEXT_CHAT_IS_TYPING()
	OR IS_BIT_SET(specData.specHUDData.iBitset, SPEC_CAM_DISABLE_ALL_CONTROLS)
		EXIT
	ENDIF

	//IF specData.specHUDData.specHUDMode = SPEC_HUD_MODE_FULL
		IF GET_SPEC_CAM_MODE(specData.SpecCamData) <> SPEC_MODE_NEWS	
			
			IF specData.specHUDData.specHUDMode = SPEC_HUD_MODE_FULL
				IF GET_SPEC_CAM_MODE(specData.SpecCamData) <> SPEC_MODE_TV_CHANNEL
				 AND GET_SPEC_CAM_MODE(specData.SpecCamData) <> SPEC_MODE_HEIST 
					IF IS_BIT_SET(specData.specCamData.iActivateBitset, ASCF_INDEX_QUIT_SCREEN)
						IF NOT g_bCelebrationScreenIsActive
						AND NOT IS_CUTSCENE_PLAYING()
							IF NOT IS_WARNING_MESSAGE_ACTIVE()
								IF CAN_SPECTATOR_QUIT()
									IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, GET_SPECTATOR_ACCEPT_CANCEL_INPUT(FALSE)) //If player presses cancel - ask yes no
										IF GET_SCTV_MODE() != SCTV_MODE_FREE_CAM
											PLAY_SOUND_FRONTEND(-1, "CANCEL", "HUD_FRONTEND_DEFAULT_SOUNDSET")
										ENDIF
										SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_REQUEST_UPDATE_INSTRUCTIONAL_BUTTONS)
										SET_SPECTATOR_HUD_STAGE(specData.specHUDData, SPEC_HUD_STAGE_QUIT_YES_NO)
										PRINTLN(" MANAGE_STAGE_FULL_LIST_INPUT - QUIT PRESSED - GO TO SPEC_HUD_STAGE_QUIT_YES_NO")
									ENDIF
								ELSE
									PRINTLN(" MANAGE_STAGE_FULL_LIST_INPUT - QUIT IGNORED - CAN_SPECTATOR_QUIT() = FALSE")
								ENDIF
							ELSE
								PRINTLN(" MANAGE_STAGE_FULL_LIST_INPUT - QUIT IGNORED - A WARNING SCREEN IS ALREADY ACTIVE")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
						
				IF ARE_SPECTATOR_HUD_TARGET_LISTS_POPULATED(specData.specHUDData)
				#IF IS_DEBUG_BUILD
				OR specData.specHUDData.bDebugForceAllowLeaderboard
				#ENDIF
					IF g_bLbReady
					AND GET_SCTV_MODE() != SCTV_MODE_FREE_CAM
					AND NOT IS_BIT_SET(specData.specHUDData.iBitset, SPEC_HUD_BS_NO_VALID_TARGET_SHOW_LEADERBOARD)				
						//IF (IS_PLAYER_SCTV(PLAYER_ID()) AND NOT IS_PLAYER_ON_ANY_MP_MISSION(PLAYER_ID()))
						IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_SURVIVAL
						OR GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_MISSION
						OR GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_DEATHMATCH
						OR GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_BASE_JUMP
						OR GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_RACE
							IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_RLEFT) ) // leaderboard
								PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
								SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_REQUEST_UPDATE_INSTRUCTIONAL_BUTTONS)
								SET_SPECTATOR_HUD_STAGE(specData.specHUDData, SPEC_HUD_STAGE_LEADERBOARD)
								CLEAR_BIT(MPSpecGlobals.iBailBitset, GLOBAL_SPEC_BAIL_BS_RESET_STATE)
								PRINTLN("[2339732] SPEC_HUD_STAGE_LEADERBOARD 1) ")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_RACE
					IF HAS_SCTV_HELI_BUTTON_BEEN_PRESSED()
//						IF GET_SPECTATOR_HUD_STAGE(specData.specHUDData) = SPEC_HUD_STAGE_FULL_LIST
//							SET_SPECTATOR_HUD_STAGE(specData.specHUDData, SPEC_HUD_STAGE_HELI)
//						ENDIF
//						IF GET_SPECTATOR_HUD_STAGE(specData.specHUDData) = SPEC_HUD_STAGE_HELI
//							SET_SPECTATOR_HUD_STAGE(specData.specHUDData, SPEC_HUD_STAGE_FULL_LIST)
//						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			//If the a flag is set to say Show leaderboard then do it.
			IF IS_BIT_SET(specData.specHUDData.iBitset, SPEC_HUD_BS_NO_VALID_TARGET_SHOW_LEADERBOARD)
				IF specData.specHUDData.eSpecHUDStage != SPEC_HUD_STAGE_LEADERBOARD
					SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_REQUEST_UPDATE_INSTRUCTIONAL_BUTTONS)
					SET_SPECTATOR_HUD_STAGE(specData.specHUDData, SPEC_HUD_STAGE_LEADERBOARD)
					CPRINTLN(DEBUG_SPECTATOR, "=== HUD === MOVE TO LEADERBAORD STAGE - DUE TO SPEC_HUD_BS_NO_VALID_TARGET_SHOW_LEADERBOARD")
					PRINTLN("[2339732] SPEC_HUD_STAGE_LEADERBOARD 2) ")
				ENDIF
			ENDIF
			
			IF ARE_SPECTATOR_HUD_TARGET_LISTS_POPULATED(specData.specHUDData)

				IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, GET_SPECTATOR_ACCEPT_CANCEL_INPUT(TRUE)) //Player has selected an individual player from list
				OR IS_BIT_SET(specData.specHUDData.iBitset, SPEC_HUD_BS_RTLT_CHANGE_TARGET)
					IF DOES_ENTITY_EXIST(GET_SPECTATOR_HUD_CURRENTLY_HIGHLIGHTED_PED_INDEX(specData.specHUDData))
					AND NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_SPECTATOR_HUD_CURRENTLY_HIGHLIGHTED_PED_INDEX(specData.specHUDData)) != INVALID_PLAYER_INDEX()
						IF GET_SPECTATOR_HUD_CURRENTLY_HIGHLIGHTED_PED_INDEX(specData.specHUDData) <> GET_SPECTATOR_SELECTED_PED()
						AND GET_SCTV_MODE() != SCTV_MODE_FREE_CAM
						AND DOES_PLAYER_ALLOW_SPECTATORS(NATIVE_TO_INT(NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_SPECTATOR_HUD_CURRENTLY_HIGHLIGHTED_PED_INDEX(specData.specHUDData))))
						AND NOT IS_SPECTATOR_RUNNING_CUTSCENE()
						AND IS_HIGHLIGHTED_PLAYER_VALID_TO_SPECTATE(NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_SPECTATOR_HUD_CURRENTLY_HIGHLIGHTED_PED_INDEX(specData.specHUDData)))
							CLEAR_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_RTLT_CHANGE_TARGET)
							INT iList, iEntry
							IF GET_SPECTATOR_HUD_CURRENTLY_HIGHLIGHTED_TARGET_LIST_AND_ENTRY_ID(specData.specHUDData, iList, iEntry)
							
							BOOL bFound = FALSE
							INT iTarget = 0
							
							IF iList = SPEC_HUD_TARGET_LIST_GENERIC_ENEMIES
							OR iList = SPEC_HUD_TARGET_LIST_GENERIC_ALLIES
							
								IF specData.specHUDData.iCurrentNumberOfTargetsInList[iList] > 0
									SET_RANDOM_SEED(GET_GAME_TIMER())
									INT iRandom = GET_RANDOM_INT_IN_RANGE(1, 11)
									INT i = 0
									
									IF specData.specHUDData.iCurrentNumberOfTargetsInList[iList] > 1		//If more than one in list, find a random one
										#IF IS_DEBUG_BUILD
										CPRINTLN(DEBUG_SPECTATOR, "=== HUD === MORE THAN ONE GENERIC, STARTING WHILE LOOP OH GOD FINGERS CROSSED")
										#ENDIF
										WHILE bFound = FALSE AND i < 100
											IF IS_SPECTATOR_HUD_TARGET_LIST_ENTRY_VALID(GET_SPECTATOR_HUD_TARGET_LIST_ENTRY(specData.specHUDData, iList, i))
												IF iTarget < iRandom		//If counter is less that random number, move counter up
													iTarget++
												ELSE						//If counter has hit random value, mark as found
													iTarget = i
													bFound = TRUE
												ENDIF
											ENDIF
											i++
											IF i > SPEC_HUD_TARGET_LIST_SIZE-1
												i = 0
												CPRINTLN(DEBUG_SPECTATOR, "=== HUD === WHILE LOOP IN PROGRESS")
											ENDIF
										ENDWHILE
										#IF IS_DEBUG_BUILD
										CPRINTLN(DEBUG_SPECTATOR, "=== HUD === WHILE LOOP OVER, bFound = ", bFound, " iTarget = ", iTarget)
										#ENDIF
									ELSE
										#IF IS_DEBUG_BUILD
										CPRINTLN(DEBUG_SPECTATOR, "=== HUD === ONLY ONE GENERIC, STARTING REPEAT")
										#ENDIF
										REPEAT SPEC_HUD_TARGET_LIST_SIZE i		//if only one in list, find that one
											IF NOT bFound
												IF IS_SPECTATOR_HUD_TARGET_LIST_ENTRY_VALID(GET_SPECTATOR_HUD_TARGET_LIST_ENTRY(specData.specHUDData, iList, i))
													iTarget = i
													bFound = TRUE
												ENDIF
												ENDIF
											ENDREPEAT
											#IF IS_DEBUG_BUILD
											CPRINTLN(DEBUG_SPECTATOR, "=== HUD === REPEAT OVER, bFound = ", bFound, " iTarget = ", iTarget)
											#ENDIF
										ENDIF
										
										IF bFound
											IF GET_SPECTATOR_DESIRED_FOCUS_PED() <> GET_SPECTATOR_HUD_TARGET_LIST_ENTRY_PED_ID(specData.specHUDData, iList, iTarget)
												#IF IS_DEBUG_BUILD
												CPRINTLN(DEBUG_SPECTATOR, "=== HUD === SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_WAITING_FOR_SWITCH) 5")
												#ENDIF
												SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_WAITING_FOR_SWITCH)
											ENDIF
											
											SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_REQUEST_UPDATE_INSTRUCTIONAL_BUTTONS)
											SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_REQUEST_REDO_SUMMARY_CARD)		//Update HUD display of lists
											specData.specHUDData.bSelectionButtonHeldThisFrame = TRUE 	//flag to change colour when selecting
										
											PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
											SET_SPECTATOR_HUD_CURRENT_FOCUS_TARGET_FROM_LIST(specData, iList, iTarget)//new random target focus from generic list!
										ENDIF
									ENDIF
								ELSE
								
									INT iLoop = 0
									iTarget = iEntry
									
									#IF IS_DEBUG_BUILD
									CPRINTLN(DEBUG_SPECTATOR, "=== HUD === Find next valid target to spectate: start at iTarget = ", iTarget)
									#ENDIF
									WHILE (NOT bFound) AND (iLoop < SPEC_HUD_TARGET_LIST_SIZE)
										
										// Check out spectator at index iTarget is valid?
										IF IS_SPECTATOR_HUD_TARGET_LIST_ENTRY_VALID(GET_SPECTATOR_HUD_TARGET_LIST_ENTRY(specData.specHUDData, iList, iTarget))
											CPRINTLN(DEBUG_SPECTATOR, "=== HUD === Found valid target, VALID iTarget = ", iTarget)
											//...yes, get out
											bFound = TRUE
										ENDIF
										
										IF NOT bFound
											CPRINTLN(DEBUG_SPECTATOR, "=== HUD === Not found valid target: INVALID iTarget = ", iTarget)
											
											// Increment our index so we only ever loop around once
											iLoop++
											
											// Move to next target
											iTarget += specData.specHUDData.iPlayerListScrollDir
											
											// Cap
											IF iTarget < 0
												iTarget = (SPEC_HUD_TARGET_LIST_SIZE-1)
											ELIF iTarget >= SPEC_HUD_TARGET_LIST_SIZE
												itarget = 0
											ENDIF
											CPRINTLN(DEBUG_SPECTATOR, "=== HUD === Not found valid target: NEXT iTarget = ", iTarget)
										ENDIF
										
									ENDWHILE
									#IF IS_DEBUG_BUILD
									CPRINTLN(DEBUG_SPECTATOR, "=== HUD === Finished finding new target, bFound = ", bFound, " iTarget = ", iTarget)
									#ENDIF
									
									// If we did find someone, then switch to them
									IF bFound
										IF GET_SPECTATOR_DESIRED_FOCUS_PED() <> GET_SPECTATOR_HUD_TARGET_LIST_ENTRY_PED_ID(specData.specHUDData, iList, iTarget)
											#IF IS_DEBUG_BUILD
											CPRINTLN(DEBUG_SPECTATOR, "=== HUD === SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_WAITING_FOR_SWITCH) 1")
											#ENDIF
											SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_WAITING_FOR_SWITCH)
										ENDIF
										
										SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_REQUEST_UPDATE_INSTRUCTIONAL_BUTTONS)
										SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_REQUEST_REDO_SUMMARY_CARD)		//Update HUD display of lists
										specData.specHUDData.bSelectionButtonHeldThisFrame = TRUE 	//flag to change colour when selecting
									
										//SET_SPECTATOR_HUD_STAGE(specData.specHUDData, SPEC_HUD_STAGE_TARGET_FOCUS)
										
										IF IS_PLAYER_SCTV(PLAYER_ID())
											INCREMENT_MP_INT_CHARACTER_STAT(MP_STAT_SPECTATED_WAS_CHANGED)
											CPRINTLN(DEBUG_SPECTATOR, "=== HUD === INCREMENT MP_STAT_SPECTATED_WAS_CHANGED")
										ENDIF
										
										PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
										SET_SPECTATOR_HUD_CURRENT_FOCUS_TARGET_FROM_LIST(specData, iList, iTarget)//new target focus!
									ENDIF
								ENDIF
							ELSE
								#IF IS_DEBUG_BUILD
								CPRINTLN(DEBUG_SPECTATOR, "=== HUD === GET_SPECTATOR_HUD_CURRENTLY_HIGHLIGHTED_TARGET_LIST_AND_ENTRY_ID = FALSE 1")
								#ENDIF
							ENDIF
							/*IF IS_SAFE_TO_SWITCH_SCTV_MODE()
								IF GET_SCTV_MODE()  = SCTV_MODE_FREE_CAM		//If picked from freecam then swap to follow
									SWITCH_SCTV_MODE(SCTV_MODE_FOLLOW_CAM)
								ENDIF
							ENDIF*/
						ENDIF
					ENDIF
				ELSE
					specData.specHUDData.bSelectionButtonHeldThisFrame = FALSE
				ENDIF
					
				IF ARE_SPECTATOR_HUD_TARGET_LISTS_POPULATED_ENOUGH_TO_SCROLL(specData.specHUDData)
					INT iIncrement
					BOOL bButtonPressed
					IF specData.specHUDData.specHUDMode = SPEC_HUD_MODE_FULL
					AND SHOULD_SELECTION_BE_INCREMENTED(specData.specHUDData.timeInput, iIncrement, bButtonPressed, FALSE)
						SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_REQUEST_UPDATE_INSTRUCTIONAL_BUTTONS)
						SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_REQUEST_UPDATE_SUMMARY_CARD)		//Update HUD display of lists
						INCREMENT_SPECTATOR_HUD_CURRENTLY_HIGHLIGHTED_ROW(specData.specHUDData, -iIncrement)
					ELSE
						//Check for RT/LT to swap Target
						IF GET_SCTV_MODE() != SCTV_MODE_FREE_CAM
						AND NOT IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_MG_SHOOTING_RANGE)
						AND NOT IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_MG_DARTS)
						AND NOT IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_MG_ARM_WRESTLING)
						AND NOT IS_SPECTATOR_RUNNING_CUTSCENE()
						AND g_iFMMCCurrentStaticCam = -1
							IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_RT))
							OR IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CELLPHONE_SCROLL_FORWARD) // Mousewheel
								PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", "HUD_FRONTEND_DEFAULT_SOUNDSET")
								SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_REQUEST_UPDATE_INSTRUCTIONAL_BUTTONS)
								SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_REQUEST_UPDATE_SUMMARY_CARD)
								IF NOT IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL) // B*2227169 When using mouse and keyboard controls the player specifically must press LMB to spectate a different player, whereas on pad it automatically changes spectator when pressing RT or LT to scroll the list
									SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_RTLT_CHANGE_TARGET)
								ENDIF
								specData.specHUDData.iPlayerListScrollDir = 1
								INCREMENT_SPECTATOR_HUD_CURRENTLY_HIGHLIGHTED_ROW(specData.specHUDData, 1)
								CPRINTLN(DEBUG_SPECTATOR, "=== HUD === RT MOVE DOWN TARGET")
							ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_LT))
							OR IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CELLPHONE_SCROLL_BACKWARD) // Mousewheel
								PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", "HUD_FRONTEND_DEFAULT_SOUNDSET")
								SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_REQUEST_UPDATE_INSTRUCTIONAL_BUTTONS)
								SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_REQUEST_UPDATE_SUMMARY_CARD)
								IF NOT IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL) // B*2227169 When using mouse and keyboard controls the player specifically must press LMB to spectate a different player, whereas on pad it automatically changes spectator when pressing RT or LT to scroll the list
									SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_RTLT_CHANGE_TARGET)
								ENDIF
								specData.specHUDData.iPlayerListScrollDir = -1
								INCREMENT_SPECTATOR_HUD_CURRENTLY_HIGHLIGHTED_ROW(specData.specHUDData, -1)
								CPRINTLN(DEBUG_SPECTATOR, "=== HUD === LT MOVE UP TARGET")
							ENDIF
						ENDIF
					ENDIF
					
					/*IF specData.specHUDData.iMaximumPage > 0
						IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_LT))
							SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_REQUEST_UPDATE_INSTRUCTIONAL_BUTTONS)
							SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_REQUEST_REDO_SUMMARY_CARD)		//Update HUD display of lists
							INCREMENT_SPECTATOR_HUD_CURRENTLY_HIGHLIGHTED_ROW(specData.specHUDData, -specData.specHUDData.iNumberOfTargetsOnAPage)
						ENDIF
						
						IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_RT))
							SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_REQUEST_UPDATE_INSTRUCTIONAL_BUTTONS)
							SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_REQUEST_REDO_SUMMARY_CARD)		//Update HUD display of lists
							INCREMENT_SPECTATOR_HUD_CURRENTLY_HIGHLIGHTED_ROW(specData.specHUDData, specData.specHUDData.iNumberOfTargetsOnAPage)
						ENDIF
					ENDIF*/
				ENDIF
			ENDIF
		ENDIF
	//ENDIF
	
	MANAGE_SHARED_INPUT(specData)
ENDPROC

/// PURPOSE:
///    Checks input for actions that can occur in more info stage
/// PARAMS:
///    specData - 
PROC MANAGE_STAGE_MORE_INFO_INPUT(SPECTATOR_DATA_STRUCT &specData)

	// Don't accept any of the below inputs if the chat box is onscreen
	IF NETWORK_TEXT_CHAT_IS_TYPING()
	OR IS_BIT_SET(specData.specHUDData.iBitset, SPEC_CAM_DISABLE_ALL_CONTROLS)
		EXIT
	ENDIF

	IF specData.specHUDData.specHUDMode = SPEC_HUD_MODE_FULL
	AND GET_SCTV_MODE() != SCTV_MODE_FREE_CAM
			
		IF GET_SPEC_CAM_MODE(specData.SpecCamData) <> SPEC_MODE_NEWS
			IF ARE_SPECTATOR_HUD_TARGET_LISTS_POPULATED(specData.specHUDData)
			#IF IS_DEBUG_BUILD
			OR specData.specHUDData.bDebugForceAllowLeaderboard
			#ENDIF
				//IF IS_PLAYER_SCTV(PLAYER_ID())
				IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_SURVIVAL
				OR GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_MISSION
					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_RLEFT) ) // leaderboard
						PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
						SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_REQUEST_UPDATE_INSTRUCTIONAL_BUTTONS)
						SET_SPECTATOR_HUD_STAGE(specData.specHUDData, SPEC_HUD_STAGE_LEADERBOARD)
						PRINTLN("[2339732] SPEC_HUD_STAGE_LEADERBOARD 3) ")
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, GET_SPECTATOR_ACCEPT_CANCEL_INPUT(FALSE)) //If player presses cancel - return to full player list
				PLAY_SOUND_FRONTEND(-1, "CANCEL", "HUD_FRONTEND_DEFAULT_SOUNDSET")
				SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_REQUEST_UPDATE_INSTRUCTIONAL_BUTTONS)
				SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_REQUEST_REDO_SUMMARY_CARD)		//Update HUD display of lists
				IF NOT IS_BIT_SET(specData.specHUDData.iBitset, SPEC_HUD_BS_NO_VALID_TARGET_SHOW_LEADERBOARD)
					SET_SPECTATOR_HUD_STAGE(specData.specHUDData, SPEC_HUD_STAGE_FULL_LIST)
				ELSE
					SET_SPECTATOR_HUD_STAGE(specData.specHUDData, SPEC_HUD_STAGE_LEADERBOARD)
					PRINTLN("[2339732] SPEC_HUD_STAGE_LEADERBOARD 4) ")
				ENDIF
			ENDIF	
		ENDIF
	ENDIF
	
	MANAGE_SHARED_INPUT(specData)
ENDPROC

/// PURPOSE:
///    Checks input for actions that can occur in leaderboard stage
/// PARAMS:
///    specData - 
PROC MANAGE_STAGE_LEADERBOARD_INPUT(SPECTATOR_DATA_STRUCT &specData)

	// Don't accept any of the below inputs if the chat box is onscreen
	IF NETWORK_TEXT_CHAT_IS_TYPING()
	OR IS_BIT_SET(specData.specHUDData.iBitset, SPEC_CAM_DISABLE_ALL_CONTROLS)
		EXIT
	ENDIF

	IF specData.specHUDData.specHUDMode = SPEC_HUD_MODE_FULL
	
		IF IS_BIT_SET(specData.specCamData.iActivateBitset, ASCF_INDEX_QUIT_SCREEN)
			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, GET_SPECTATOR_ACCEPT_CANCEL_INPUT(FALSE)) //If player presses cancel - ask yes no
				IF CAN_SPECTATOR_QUIT()
					PLAY_SOUND_FRONTEND(-1, "CANCEL", "HUD_FRONTEND_DEFAULT_SOUNDSET")
					SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_REQUEST_UPDATE_INSTRUCTIONAL_BUTTONS)
					SET_SPECTATOR_HUD_STAGE(specData.specHUDData, SPEC_HUD_STAGE_QUIT_YES_NO)
				ELSE
					PRINTLN(" MANAGE_STAGE_LEADERBOARD_INPUT - QUIT IGNORED - CAN_SPECTATOR_QUIT() = FALSE")
				ENDIF
			ENDIF	
		ENDIF
	
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_RLEFT)) //If player presses cancel - return to full player list
			PLAY_SOUND_FRONTEND(-1, "CANCEL", "HUD_FRONTEND_DEFAULT_SOUNDSET")
			SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_REQUEST_UPDATE_INSTRUCTIONAL_BUTTONS)
			SET_SPECTATOR_HUD_STAGE(specData.specHUDData, specData.specHUDData.eSpecHUDStageVisitedBeforeLeaderboard)
		ENDIF
	ENDIF
	
	MANAGE_SHARED_INPUT(specData)
ENDPROC

/// PURPOSE:
///    Clean up transition session flags
PROC CLEAN_UP_TRANSITIONS_SESSION_STUFF_WHEN_QUITTING_SPECTATOR()
	CLEAR_PLAYLIST_HAS_DONE_INITIAL_TRANSITION()
	CLEAR_TRANSITION_SESSIONS_ACCEPTING_INVITE_FROM_MP()
	CLEAR_TRANSITION_SESSIONS_ACCEPTING_INVITE()
	CLEAR_TRANSITION_SESSIONS_NEED_TO_CLEANUP_FM_AFTER_INVITE()
	CLEAR_TRANSITION_SESSION_QUITING_CORONA()
	CLEAR_ENDED_MISSION_AT_LEADERBOARD_VOTE_OR_TIME_OUT()
ENDPROC

/// PURPOSE:
///    Checks input for actions that can occur in leaderboard stage
/// PARAMS:
///    specData - 
PROC MANAGE_STAGE_QUIT_YES_NO_INPUT(SPECTATOR_DATA_STRUCT &specData)
	
	IF g_bCelebrationScreenIsActive
		SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_REQUEST_UPDATE_INSTRUCTIONAL_BUTTONS)
		SET_SPECTATOR_HUD_STAGE(specData.specHUDData, specData.specHUDData.eSpecHUDStageVisitedBeforeQuitYesNo)
		PRINTLN(" MANAGE_STAGE_QUIT_YES_NO_INPUT - QUIT CANCELLED DUE TO g_bCelebrationScreenIsActive")
	ENDIF
	
	IF (NOT IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL) AND IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, GET_SPECTATOR_ACCEPT_CANCEL_INPUT(FALSE))) //If player presses cancel - ask yes no
	OR (IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL) AND IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL))
		PLAY_SOUND_FRONTEND(-1, "CANCEL", "HUD_FRONTEND_DEFAULT_SOUNDSET")
		SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_REQUEST_UPDATE_INSTRUCTIONAL_BUTTONS)
		SET_SPECTATOR_HUD_STAGE(specData.specHUDData, specData.specHUDData.eSpecHUDStageVisitedBeforeQuitYesNo)
		PRINTLN(" MANAGE_STAGE_QUIT_YES_NO_INPUT - QUIT CANCELLED")
	ENDIF
	
	IF (NOT IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL) AND IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, GET_SPECTATOR_ACCEPT_CANCEL_INPUT(TRUE))) //If player presses cancel - ask yes no
	OR (IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL) AND IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT))
		
		IF NOT IS_PLAYER_SCTV(PLAYER_ID())
			
			IF Is_Player_Currently_On_MP_CTF_Mission(PLAYER_ID())
				INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_HAS_CHAR_QUIT_SPECCAM_CTF,  1)
				PRINTLN(" MANAGE_STAGE_QUIT_YES_NO_INPUT - MP_STAT_HAS_CHAR_QUIT_SPECCAM_CTF + 1")
			ELIF Is_Player_Currently_On_MP_LTS_Mission(PLAYER_ID())
				INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_HAS_CHAR_QUIT_SPECCAM_LTS,  1)
				PRINTLN(" MANAGE_STAGE_QUIT_YES_NO_INPUT - MP_STAT_HAS_CHAR_QUIT_SPECCAM_LTS + 1")
			ELIF IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_MISSION)
				INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_HAS_CHAR_QUIT_SPECCAM_MISS,  1)
				PRINTLN(" MANAGE_STAGE_QUIT_YES_NO_INPUT - MP_STAT_HAS_CHAR_QUIT_SPECCAM_MISS + 1")
			ELIF IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_SURVIVAL)
				INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_HAS_CHAR_QUIT_SPECCAM_SUR,  1)
				PRINTLN(" MANAGE_STAGE_QUIT_YES_NO_INPUT - MP_STAT_HAS_CHAR_QUIT_SPECCAM_SUR + 1")
            ENDIF
			
			CLEAN_UP_TRANSITIONS_SESSION_STUFF_WHEN_QUITTING_SPECTATOR()
			
			SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_REQUEST_UPDATE_INSTRUCTIONAL_BUTTONS)
			SET_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_WANT_TO_LEAVE)
			SET_BIT(specData.SpecCamData.iBitset, SPEC_CAM_BS_THIS_WANT_TO_LEAVE)
			
			SET_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_WAS_LAST_DEACTIVATED_BY_QUIT_SCREEN)
						
			//For 1635115
			IF IS_PLAYER_ON_A_PLAYLIST(PLAYER_ID())
				FORCE_ABANDON_CURRENT_MP_MISSION()
			ENDIF
			
			PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
			SET_SPECTATOR_HUD_STAGE(specData.specHUDData, specData.specHUDData.eSpecHUDStageVisitedBeforeQuitYesNo)
			PRINTLN(" MANAGE_STAGE_QUIT_YES_NO_INPUT - QUIT CONFIRMED - A")
		ELSE
			
			//SET_QUIT_GAME_WITH_SPECTATOR_ACTIVE(TRUE)
			//SET_CAMERA_QUICK_EXIT_TO_SP(TRUE)
			//RESET_EVERYTHING_TRANSITION_SESSION_FOR_SP_BAIL()
			//NETWORK_BAIL()
			
			/*SET_PAUSE_MENU_REQUESTING_A_WARP() 
			SET_PAUSE_MENU_REQUESTING_A_NEW_SESSION() 
			SET_PAUSE_MENU_WARP_TO_MODE(GAMEMODE_FM) 
			SET_SOMETHING_QUITTING_MP(TRUE) 
			SET_CURRENT_TRANSITION_FM_MENU_CHOICE(FM_MENU_CHOICE_JOIN_NEW_SESSION)// – Whatever type of session you want. 

			SET_CURRENT_TRANSITION_FM_SESSION_MENU_CHOICE(g_Private_Players_FM_SESSION_Menu_Choice_LAST_ENTRY) 
			SET_JOINING_GAMEMODE(GAMEMODE_FM)*/
			
			PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
			QUIT_SCTV_BACK_TO_GTAO()
			PRINTLN(" MANAGE_STAGE_QUIT_YES_NO_INPUT - QUIT CONFIRMED - B")
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Makes sure the player has a relevant entry highlighted, changes which page the player is viewing
/// PARAMS:
///    specHUDData - 
PROC MANAGE_HIGHLIGHT_AND_SELECT_LIST(SPECTATOR_HUD_DATA &specHUDData)
	IF GET_SPECTATOR_HUD_CURRENTLY_HIGHLIGHTED_ROW(specHUDData) >= specHUDData.iNumberOfTargetsOnCurrentPage
		PRINTLN("SPECTATOR - MANAGE_HIGHLIGHT_AND_SELECT_LIST - X - iCurrentPage = ", specHUDData.iCurrentPage)
		PRINTLN("SPECTATOR - MANAGE_HIGHLIGHT_AND_SELECT_LIST - X - iMaximumPage = ", specHUDData.iMaximumPage)
		IF specHUDData.iMaximumPage > 0
			IF specHUDData.iCurrentPage = (specHUDData.iMaximumPage-1)
				specHUDData.iCurrentPage = 0
				PRINTLN("SPECTATOR - MANAGE_HIGHLIGHT_AND_SELECT_LIST - A - iCurrentPage = ", specHUDData.iCurrentPage)
			ELSE
				specHUDData.iCurrentPage++
				PRINTLN("SPECTATOR - MANAGE_HIGHLIGHT_AND_SELECT_LIST - B - iCurrentPage = ", specHUDData.iCurrentPage)
			ENDIF
		ENDIF
		CALCULATE_NUMBER_OF_TARGETS_ON_A_PAGE(specHUDData) //recalc current page population
		SET_SPECTATOR_HUD_CURRENTLY_HIGHLIGHTED_ROW(specHUDData, 0)
		SET_BIT(specHUDData.iBitset, SPEC_HUD_BS_REQUEST_UPDATE_SUMMARY_CARD)
		PRINTLN("SPECTATOR - MANAGE_HIGHLIGHT_AND_SELECT_LIST - SPEC_HUD_BS_REQUEST_UPDATE_SUMMARY_CARD - NEXT PAGE OR BACK TO START - HIGHLIGHTED_ROW = ", GET_SPECTATOR_HUD_CURRENTLY_HIGHLIGHTED_ROW(specHUDData))
	ENDIF

	IF GET_SPECTATOR_HUD_CURRENTLY_HIGHLIGHTED_ROW(specHUDData) < 0
		PRINTLN("SPECTATOR - MANAGE_HIGHLIGHT_AND_SELECT_LIST - Y - iCurrentPage = ", specHUDData.iCurrentPage)
		PRINTLN("SPECTATOR - MANAGE_HIGHLIGHT_AND_SELECT_LIST - Y - iMaximumPage = ", specHUDData.iMaximumPage)
		IF specHUDData.iMaximumPage > 0
			IF specHUDData.iCurrentPage = 0
				specHUDData.iCurrentPage = (specHUDData.iMaximumPage-1)
				PRINTLN("SPECTATOR - MANAGE_HIGHLIGHT_AND_SELECT_LIST - CC - iCurrentPage = ", specHUDData.iCurrentPage)
			ELSE
				specHUDData.iCurrentPage--
				PRINTLN("SPECTATOR - MANAGE_HIGHLIGHT_AND_SELECT_LIST - D - iCurrentPage = ", specHUDData.iCurrentPage)
			ENDIF
		ENDIF
		CALCULATE_NUMBER_OF_TARGETS_ON_A_PAGE(specHUDData) //recalc current page population
		IF specHUDData.iNumberOfTargetsOnCurrentPage > 0
			SET_SPECTATOR_HUD_CURRENTLY_HIGHLIGHTED_ROW(specHUDData, specHUDData.iNumberOfTargetsOnCurrentPage-1)
			PRINTLN("SPECTATOR - MANAGE_HIGHLIGHT_AND_SELECT_LIST - SET_SPECTATOR_HUD_CURRENTLY_HIGHLIGHTED_ROW = ", specHUDData.iCurrentlyHighlightedRow)
		ELSE
			SET_SPECTATOR_HUD_CURRENTLY_HIGHLIGHTED_ROW(specHUDData, 0)
		ENDIF
		SET_BIT(specHUDData.iBitset, SPEC_HUD_BS_REQUEST_UPDATE_SUMMARY_CARD)
		PRINTLN("SPECTATOR - MANAGE_HIGHLIGHT_AND_SELECT_LIST - SPEC_HUD_BS_REQUEST_UPDATE_SUMMARY_CARD - PREVIOUS PAGE OR BACK TO END - HIGHLIGHTED_ROW = ", GET_SPECTATOR_HUD_CURRENTLY_HIGHLIGHTED_ROW(specHUDData))
	ENDIF
ENDPROC

/// PURPOSE:
///    Populates an array entry of text labels 23 with a player name
/// PARAMS:
///    textLabelsPassed - 
///    iPlayer - player ID
PROC DEFINE_FREEMODE_NAMES_ARRAY(TEXT_LABEL_63 &textLabelsPassed[], INT iPlayer)
	IF IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(iPlayer), FALSE)
		PLAYER_INDEX PlayerId = INT_TO_PLAYERINDEX(iPlayer)
		IF NOT IS_PLAYER_SCTV(PlayerId)
			textLabelsPassed[iPlayer] = GET_PLAYER_NAME(PlayerId)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets a flag that blocks the appearance of spectator instructions that use circle, such as "back"
/// PARAMS:
///    specHUDData - 
///    bOnOff - true for on, false for off
PROC SET_SPECTATOR_HUD_BLOCK_CIRCLE_INSTRUCTION(SPECTATOR_HUD_DATA &specHUDData, BOOL bOnOff)
	IF bOnOff
		IF NOT IS_BIT_SET(specHUDData.iBitset, SPEC_HUD_BS_BLOCK_CIRCLE_INSTRUCTION)
			DEBUG_PRINTCALLSTACK()
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SPECTATOR, "=== HUD === SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_BLOCK_CIRCLE_INSTRUCTION)")
			#ENDIF
			SET_BIT(specHUDData.iBitset, SPEC_HUD_BS_BLOCK_CIRCLE_INSTRUCTION)
		ENDIF
	ELSE
		IF IS_BIT_SET(specHUDData.iBitset, SPEC_HUD_BS_BLOCK_CIRCLE_INSTRUCTION)
			DEBUG_PRINTCALLSTACK()
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SPECTATOR, "=== HUD === CLEAR_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_BLOCK_CIRCLE_INSTRUCTION)")
			#ENDIF
			CLEAR_BIT(specHUDData.iBitset, SPEC_HUD_BS_BLOCK_CIRCLE_INSTRUCTION)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Performs every-frame instructions to disable control functionality apart from what is needed to interact with the spectator cam
/// PARAMS:
///    specData - 
PROC DISABLE_SPECTATOR_CONTROL_ACTIONS_GENERAL(SPECTATOR_DATA_STRUCT &specData)
	DISABLE_ALL_CONTROL_ACTIONS(PLAYER_CONTROL)
	DISABLE_ALL_CONTROL_ACTIONS(FRONTEND_CONTROL)
	//ENABLE_ALL_CONTROL_ACTIONS(CAMERA_CONTROL)//cant do this, lets the player walk in garage tv.
		
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, GET_SPECTATOR_ACCEPT_CANCEL_INPUT(TRUE))
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, GET_SPECTATOR_ACCEPT_CANCEL_INPUT(FALSE))
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_LB)
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_LT)
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE)
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_RB)
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_RT)
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_SELECT)
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_RLEFT))
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_Y)

	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_X)
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y)
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_LT))
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_RT))
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_LS))
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_RS))
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_LB))
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_RB))
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, GET_SPECTATOR_ACCEPT_CANCEL_INPUT(TRUE))
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_RLEFT))
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_RUP))
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, GET_SPECTATOR_ACCEPT_CANCEL_INPUT(FALSE))
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_PAD_LEFT))
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_PAD_RIGHT))
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_PAD_UP))
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_PAD_DOWN))
	
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
	
	
	IF IS_PLAYER_A_ROCKSTAR_DEV()
		ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_RS))
	ENDIF
	
	IF GET_SPEC_CAM_MODE(specData.SpecCamData) <> SPEC_MODE_NEWS		//if not news channel, allow player to change camera angles
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
		IF specData.specHUDData.eSpecHUDStage <> SPEC_HUD_STAGE_LEADERBOARD
			ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
		ENDIF
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_LR)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_UD)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_UP_ONLY)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_DOWN_ONLY)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_LEFT_ONLY)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_RIGHT_ONLY)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CINEMATIC_UD)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CINEMATIC_LR)
	ENDIF
	
	IF IS_PLAYER_SCTV(PLAYER_ID())
			
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_LS))
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_RS))
		
		IF GET_SCTV_MODE() = SCTV_MODE_FREE_CAM
			ENABLE_CONTROL_ACTION(PLAYER_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_LT))
			ENABLE_CONTROL_ACTION(PLAYER_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_RT))
			ENABLE_CONTROL_ACTION(PLAYER_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_LS))
			ENABLE_CONTROL_ACTION(PLAYER_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_RS))
			ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SCRIPT_LEFT_AXIS_X)
			ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y)
			ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_X)
			ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_Y)
			ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_UD)
			ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_LR)
			ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_LR)
			ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_UD)
			ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPRINT)
			ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SCRIPTED_FLY_LR)
			ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SCRIPTED_FLY_UD)
			ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SCRIPTED_FLY_ZDOWN)
			ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SCRIPTED_FLY_ZUP)
			ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, GET_SPECTATOR_ACCEPT_CANCEL_INPUT(TRUE))
			ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_RLEFT))
			ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_RUP))
			ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, GET_SPECTATOR_ACCEPT_CANCEL_INPUT(FALSE))
			ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_PAD_LEFT))
			ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_PAD_RIGHT))
			ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_PAD_UP))
			ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_PAD_DOWN))
						
		ENDIF
	ENDIF
	
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_RADIO_WHEEL)
	DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_VEH_RADIO_WHEEL)
	DISABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_VEH_RADIO_WHEEL)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PREV_RADIO)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_NEXT_RADIO)
	
	// B* 2306124 // B* 2360890
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_NEXT_WEAPON, FALSE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_PREV_WEAPON, FALSE)
	
	IF IS_PC_VERSION()
		IF NOT IS_WARNING_MESSAGE_ACTIVE()
			PRINTLN("SPECTATOR - PC CHECK - DISABLE INPUT_FRONTEND_PAUSE_ALTERNATE ")
			DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
		ENDIF
	ENDIF
	
	IF IS_SPECTATOR_RUNNING_CUTSCENE()
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE)
	ENDIF
	
	IF GET_SPECTATOR_HUD_STAGE(specData.specHUDData) <> SPEC_HUD_STAGE_QUIT_YES_NO
		ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_MP_TEXT_CHAT_ALL)
		ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_MP_TEXT_CHAT_TEAM)
	ENDIF
	
	IF NETWORK_TEXT_CHAT_IS_TYPING()
	OR IS_BIT_SET(specData.specHUDData.iBitset, SPEC_CAM_DISABLE_ALL_CONTROLS)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Get a string for the job type a player is on
/// PARAMS:
///    thisPlayer - player to use to get the string
/// RETURNS:
///    a text label for the player's job type
FUNC STRING GET_PLAYER_INDEX_MISSION_TEXT_LABEL(PLAYER_INDEX thisPlayer)

	IF GET_CURRENT_GAMEMODE() = GAMEMODE_FM
		SWITCH GET_FM_MISSION_TYPE_PLAYER_IS_ON(thisPlayer)
		
			CASE FMMC_TYPE_DEATHMATCH
				RETURN "SPEC_DM"
			BREAK
			
			CASE FMMC_TYPE_RACE
				RETURN "SPEC_RACE"
			BREAK
			
			CASE FMMC_TYPE_SURVIVAL
				RETURN "SPEC_HORDE"
			BREAK
			
			CASE FMMC_TYPE_MISSION
				RETURN "SPEC_MISSION"
			BREAK
			
			CASE FMMC_TYPE_IMPROMPTU_DM
				RETURN "SPEC_IMPROMP"
			BREAK
			
			CASE FMMC_TYPE_GANGHIDEOUT
				RETURN "SPEC_GANG"
			BREAK
			
//			CASE FMMC_TYPE_TRIATHLON
//				RETURN "SPEC_TRI"
//			BREAK
			
			CASE FMMC_TYPE_BASE_JUMP
				RETURN "SPEC_BASE"
			BREAK
			
			CASE FMMC_TYPE_MG_GOLF
				RETURN "SPEC_GOLF"
			BREAK
			
			CASE FMMC_TYPE_MG_TENNIS
				RETURN "SPEC_TENNIS"
			BREAK
			
			CASE FMMC_TYPE_MG_SHOOTING_RANGE
				RETURN "SPEC_SHOOT"
			BREAK
			
			CASE FMMC_TYPE_MG_DARTS
				RETURN "SPEC_DARTS"
			BREAK
			
			CASE FMMC_TYPE_MG_ARM_WRESTLING
				RETURN "SPEC_ARM"
			BREAK
			
		ENDSWITCH
		
		IF IS_PLAYER_ON_MP_AMBIENT_SCRIPT(thisPlayer, MPAM_TYPE_CINEMA)
			RETURN "SPEC_CINEMA"
		ENDIF
	ENDIF
	
	RETURN "STRING"
	
ENDFUNC

FUNC BOOL SHOULD_HIDE_FOR_DEV_SPECTATE()

	IF IS_PLAYER_SCTV(PLAYER_ID())
		IF GET_SCTV_MODE() = SCTV_MODE_FREE_CAM
			IF g_b_OnLeaderboard
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_LBD_READY()
	IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_DEATHMATCH
	OR GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_BASE_JUMP
	OR GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_RACE
	OR GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_MISSION_CTF
	OR GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_MISSION_CONTACT
	OR GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_MISSION_LTS
	OR GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_MISSION_VS
	OR GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_MISSION_COOP
	OR GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_MISSION
	
		IF NOT g_bLbReady
		
			RETURN FALSE
		ENDIF
	ENDIF
	
//	PRINTLN("[CS_MISS]  iCurrentMissionType = ", GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType)

	RETURN TRUE
ENDFUNC

PROC MANAGE_SCTV_HELI_BUTTONS(SPECTATOR_DATA_STRUCT &specData)
	IF bSCTVHeliActive
	AND NOT IS_ON_AIR_RACE_GLOBAL_SET()
		IF specData.specHUDData.bRefresh = FALSE	
			SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_REQUEST_UPDATE_INSTRUCTIONAL_BUTTONS)
			specData.specHUDData.bRefresh = TRUE
			PRINTLN("[CS_HELI_SPEC] CAM RENDER_SPECTATOR_HUD, ON ")
			bShowTextSCTV = TRUE
		ENDIF
	ELSE
		IF bShowTextSCTV = TRUE
			IF specData.specHUDData.bSCTVText = FALSE
				PRINTLN("[CS_HELI_SPEC] CAM RENDER_SPECTATOR_HUD, bSCTVOnce ")
				SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_REQUEST_UPDATE_INSTRUCTIONAL_BUTTONS)
				specData.specHUDData.bSCTVText = TRUE
			ELSE
				IF specData.specHUDData.bRefresh = TRUE
					SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_REQUEST_UPDATE_INSTRUCTIONAL_BUTTONS)
					specData.specHUDData.bRefresh = FALSE
					PRINTLN("[CS_HELI_SPEC] CAM RENDER_SPECTATOR_HUD, OFF ")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_BLOCK_FOR_END_LBD()
	IF g_b_OnLeaderboard
	AND NOT IS_BIT_SET(MPSpecGlobals.iBitset, GLOBAL_SPEC_BS_READY_FOR_LEADERBOARD)
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_HIDE_SPECTATOR_HUD(SPECTATOR_DATA_STRUCT &specData)
	IF IS_SPECTATOR_HUD_HIDDEN()
		CPRINTLN(DEBUG_SPECTATOR, "=== HUD === SHOULD_HIDE_SPECTATOR_HUD - IS_SPECTATOR_HUD_HIDDEN")
		RETURN TRUE
	ENDIF
	
	IF IS_SPECTATOR_HUD_HIDDEN_FLAG_SET()
		CPRINTLN(DEBUG_SPECTATOR, "=== HUD === SHOULD_HIDE_SPECTATOR_HUD - IS_SPECTATOR_HUD_HIDDEN_FLAG_SET")
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(specData.specCamData.iActivateBitset, ASCF_INDEX_CELEBRATION_SCREEN)
		CPRINTLN(DEBUG_SPECTATOR, "=== HUD === SHOULD_HIDE_SPECTATOR_HUD - ASCF_INDEX_CELEBRATION_SCREEN")
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_MG_ARM_WRESTLING)
		CPRINTLN(DEBUG_SPECTATOR, "=== HUD === SHOULD_HIDE_SPECTATOR_HUD - FMMC_TYPE_MG_ARM_WRESTLING")
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_MG_GOLF)
		CPRINTLN(DEBUG_SPECTATOR, "=== HUD === SHOULD_HIDE_SPECTATOR_HUD - FMMC_TYPE_MG_GOLF")
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_MG_SHOOTING_RANGE)
		CPRINTLN(DEBUG_SPECTATOR, "=== HUD === SHOULD_HIDE_SPECTATOR_HUD - FMMC_TYPE_MG_SHOOTING_RANGE")
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_MG_TENNIS)
		CPRINTLN(DEBUG_SPECTATOR, "=== HUD === SHOULD_HIDE_SPECTATOR_HUD - FMMC_TYPE_MG_TENNIS")
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_MG_DARTS)
		CPRINTLN(DEBUG_SPECTATOR, "=== HUD === SHOULD_HIDE_SPECTATOR_HUD - FMMC_TYPE_MG_DARTS")
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_CINEMA) // fix bug 1802367
		CPRINTLN(DEBUG_SPECTATOR, "=== HUD === SHOULD_HIDE_SPECTATOR_HUD - FMMC_TYPE_CINEMA")
		RETURN TRUE
	ENDIF
	
	IF SHOULD_HIDE_FOR_DEV_SPECTATE()
		CPRINTLN(DEBUG_SPECTATOR, "=== HUD === SHOULD_HIDE_SPECTATOR_HUD - SHOULD_HIDE_FOR_DEV_SPECTATE")
		RETURN TRUE
	ENDIF
	
	IF NOT IS_LBD_READY()
		CPRINTLN(DEBUG_SPECTATOR, "=== HUD === SHOULD_HIDE_SPECTATOR_HUD - NOT IS_LBD_READY")
		RETURN TRUE
	ENDIF
	
	IF SHOULD_BLOCK_FOR_END_LBD()
		CPRINTLN(DEBUG_SPECTATOR, "=== HUD === SHOULD_HIDE_SPECTATOR_HUD - SHOULD_BLOCK_FOR_END_LBD")
		RETURN TRUE
	ENDIF
	
	IF g_bCelebrationScreenIsActive
		CPRINTLN(DEBUG_SPECTATOR, "=== HUD === SHOULD_HIDE_SPECTATOR_HUD - g_bCelebrationScreenIsActive")
		RETURN TRUE
	ENDIF
	
	IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.iSplashProg != 0
		CPRINTLN(DEBUG_SPECTATOR, "=== HUD === SHOULD_HIDE_SPECTATOR_HUD - iSplashProg != 0")
		RETURN TRUE
	ENDIF
	
	IF IS_SPECTATOR_RUNNING_CUTSCENE()
		CPRINTLN(DEBUG_SPECTATOR, "=== HUD === SHOULD_HIDE_SPECTATOR_HUD - IS_SPECTATOR_RUNNING_CUTSCENE")
		RETURN TRUE
	ENDIF
	
	IF specData.specCamData.fadeSwitchStage = eSPECFADESWITCHSTAGE_FADE_OUT
		CPRINTLN(DEBUG_SPECTATOR, "=== HUD === SHOULD_HIDE_SPECTATOR_HUD - eSPECFADESWITCHSTAGE_FADE_OUT")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MANAGE_STATE_RESET(SPECTATOR_DATA_STRUCT &specData)
	IF IS_BIT_SET(MPSpecGlobals.iBailBitset, GLOBAL_SPEC_BAIL_BS_RESET_STATE)
		IF GET_SPECTATOR_HUD_STAGE(specData.specHUDData) <> SPEC_HUD_STAGE_FULL_LIST
			SET_SPECTATOR_HUD_STAGE(specData.specHUDData, SPEC_HUD_STAGE_FULL_LIST)
			CLEAR_BIT(MPSpecGlobals.iBailBitset, GLOBAL_SPEC_BAIL_BS_RESET_STATE)
			CLEAR_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_NO_VALID_TARGET_SHOW_LEADERBOARD)
			PRINTLN("=== HUD === [2339732] MANAGE_STATE_RESET ")
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_HIDE_SPECTATOR_BUTTONS(SPECTATOR_DATA_STRUCT &specData)
	IF g_bCelebrationScreenIsActive
		CPRINTLN(DEBUG_SPECTATOR, "=== HUD === SHOULD_HIDE_SPECTATOR_BUTTONS - g_bCelebrationScreenIsActive")
		RETURN TRUE
	ENDIF
	
	IF IS_SPECTATOR_RUNNING_CUTSCENE()
		CPRINTLN(DEBUG_SPECTATOR, "=== HUD === SHOULD_HIDE_SPECTATOR_BUTTONS - IS_SPECTATOR_RUNNING_CUTSCENE()")
		RETURN TRUE
	ENDIF
	
	IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.iSplashProg != 0
		CPRINTLN(DEBUG_SPECTATOR, "=== HUD === SHOULD_HIDE_SPECTATOR_BUTTONS - g_TransitionSessionNonResetVars.sPostMissionCleanupData.iSplashProg != 0")
		RETURN TRUE
	ENDIF
	
	IF specData.specCamData.fadeSwitchStage = eSPECFADESWITCHSTAGE_FADE_OUT
		CPRINTLN(DEBUG_SPECTATOR, "=== HUD === SHOULD_HIDE_SPECTATOR_BUTTONS - specData.specCamData.fadeSwitchStage = eSPECFADESWITCHSTAGE_FADE_OUT")
		RETURN TRUE
	ENDIF
	
	IF ARE_SPECTATOR_BUTTONS_HIDDEN()
		CPRINTLN(DEBUG_SPECTATOR, "=== HUD === SHOULD_HIDE_SPECTATOR_BUTTONS - ARE_SPECTATOR_BUTTONS_HIDDEN()")
		RETURN TRUE
	ENDIF
	
	IF NETWORK_TEXT_CHAT_IS_TYPING()
		CPRINTLN(DEBUG_SPECTATOR, "=== HUD === SHOULD_HIDE_SPECTATOR_BUTTONS - NETWORK_TEXT_CHAT_IS_TYPING()")
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(specData.specHUDData.iBitset, SPEC_CAM_DISABLE_ALL_CONTROLS)
		CPRINTLN(DEBUG_SPECTATOR, "=== HUD === SHOULD_HIDE_SPECTATOR_BUTTONS - SPEC_CAM_DISABLE_ALL_CONTROLS")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Renders the spectator HUD, including the scaleform for the player list and the instructional buttons
/// PARAMS:
///    specData - 
///    thisStage - current HUD stage
PROC RENDER_SPECTATOR_HUD(SPECTATOR_DATA_STRUCT &specData, eSpecHUDStageList thisStage)
	
	BOOL bShouldRenderHUD = TRUE
	BOOL bShouldRenderButtons = TRUE
	PED_INDEX pedHighlight
	PLAYER_INDEX playerHighlight	
	
	IF thisStage <> SPEC_HUD_STAGE_LEADERBOARD
		IF IS_BIT_SET(MPSpecGlobals.iBitset, GLOBAL_SPEC_BS_READY_FOR_LEADERBOARD)
			CLEAR_BIT(MPSpecGlobals.iBitset, GLOBAL_SPEC_BS_READY_FOR_LEADERBOARD)
			CPRINTLN(DEBUG_SPECTATOR, " RENDER_SPECTATOR_HUD - GLOBAL_SPEC_BS_READY_FOR_LEADERBOARD - CLEARED")
		ENDIF
	ENDIF
	
	IF IS_INSTRUCTIONAL_BUTTON_UPDATE_REQUESTED()
		CLEAR_SPEC_INSTRUCTIONAL_BUTTON_UPDATE_REQUEST()
		SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_REQUEST_UPDATE_INSTRUCTIONAL_BUTTONS)
		PRINTLN("=== HUD === RENDER_SPECTATOR_HUD - IS_INSTRUCTIONAL_BUTTON_UPDATE_REQUESTED = TRUE - SPEC_HUD_BS_REQUEST_UPDATE_INSTRUCTIONAL_BUTTONS SET")
	ENDIF
	
	IF SHOULD_HIDE_SPECTATOR_HUD(specData)
		CPRINTLN(DEBUG_SPECTATOR, "=== HUD === RENDER_SPECTATOR_HUD, SHOULD_HIDE_SPECTATOR_HUD - bShouldRenderHUD = FALSE ")
		bShouldRenderHUD = FALSE	//don't render the HUD in certain situations
	ENDIF
	
	IF SHOULD_HIDE_SPECTATOR_BUTTONS(specData)
		CPRINTLN(DEBUG_SPECTATOR, "=== HUD === RENDER_SPECTATOR_HUD, SHOULD_HIDE_SPECTATOR_BUTTONS - bShouldRenderButtons = FALSE ")
		bShouldRenderButtons = FALSE	//don't render the BUTTONS in certain situations
	ENDIF
	
	// url:bugstar:3318557
	IF IS_SCREEN_FADED_OUT()
		CPRINTLN(DEBUG_SPECTATOR, "=== HUD === RENDER_SPECTATOR_HUD, IS_SCREEN_FADED_OUT = TRUE - bShouldRenderButtons = FALSE - bShouldRenderHUD = FALSE")
		bShouldRenderHUD = FALSE
		bShouldRenderButtons = FALSE	//don't render the BUTTONS in certain situations
	ENDIF
	
	MANAGE_STATE_RESET(specData)

	// SCTV heli buttons
	MANAGE_SCTV_HELI_BUTTONS(specData)
	
	IF specData.specHUDData.specHUDMode = SPEC_HUD_MODE_FULL
		IF GET_SPEC_CAM_MODE(specData.SpecCamData) <> SPEC_MODE_NEWS
			SWITCH thisStage
			
				CASE SPEC_HUD_STAGE_FULL_LIST
				CASE SPEC_HUD_STAGE_TARGET_FOCUS
				CASE SPEC_HUD_STAGE_HELI
					IF bShouldRenderHUD
						IF NOT IS_BIT_SET(specData.specHUDData.iBitset, SPEC_HUD_BS_SHOW_FEED)
							IF HAS_SCALEFORM_MOVIE_LOADED(specData.specHUDData.sfSummaryCard)
							AND NATIVE_TO_INT(specData.specHUDData.sfSummaryCard) != -1
								DRAW_SCALEFORM_SUMMARY_CARD(specData.specHUDData.sfSummaryCard)					//Draw the player list
								//THEFEED_SET_SCRIPTED_MENU_HEIGHT(0.466664)
								IF specData.specHUDData.iCurrentNumberOfTargetsInAllLists > SPEC_HUD_NUM_PLAYERS_TO_HIDE_FEED
									THEFEED_HIDE_THIS_FRAME()
									IF NOT IS_BIT_SET(specData.specHUDData.iBitset, SPEC_HUD_BS_HIDE_FEED_DUE_TO_LIST_SIZE)
										SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_HIDE_FEED_DUE_TO_LIST_SIZE)
										CPRINTLN(DEBUG_SPECTATOR, 	"SCTV: SPEC_HUD_BS_HIDE_FEED_DUE_TO_LIST_SIZE - SET")
									ENDIF
								ELSE
									IF IS_BIT_SET(specData.specHUDData.iBitset, SPEC_HUD_BS_HIDE_FEED_DUE_TO_LIST_SIZE)
										THEFEED_FLUSH_QUEUE()
										CLEAR_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_HIDE_FEED_DUE_TO_LIST_SIZE)
										CPRINTLN(DEBUG_SPECTATOR, 	"SCTV: SPEC_HUD_BS_HIDE_FEED_DUE_TO_LIST_SIZE - CLEARED - THEFEED_FLUSH_QUEUE")
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE SPEC_HUD_STAGE_LEADERBOARD					
					IF NOT IS_SELECTOR_ONSCREEN()
						IF bShouldRenderHUD
							IF NOT IS_BIT_SET(MPSpecGlobals.iBitset, GLOBAL_SPEC_BS_READY_FOR_LEADERBOARD)
								SET_BIT(MPSpecGlobals.iBitset, GLOBAL_SPEC_BS_READY_FOR_LEADERBOARD)		//set flag for spectated job script to draw it's own leaderboard
								CPRINTLN(DEBUG_SPECTATOR, "SPEC_HUD_STAGE_LEADERBOARD - GLOBAL_SPEC_BS_READY_FOR_LEADERBOARD - SET")
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE SPEC_HUD_STAGE_QUIT_YES_NO			//display "Do you want to quit?" warning message
					FE_WARNING_FLAGS iButtonBits
					iButtonBits = FE_WARNING_YESNO
					
					INT fmmcType
					fmmcType = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType
					
					IF fmmcType = FMMC_TYPE_SURVIVAL
						SET_WARNING_MESSAGE_WITH_HEADER("SPEC_LEAVE", "SPEC_SURE_HORD", iButtonBits)
					ELIF fmmcType = FMMC_TYPE_MISSION
						IF g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_LTS
							SET_WARNING_MESSAGE_WITH_HEADER("SPEC_LEAVE", "SPEC_SURE_LTS", iButtonBits)
						ELIF g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_CTF
							SET_WARNING_MESSAGE_WITH_HEADER("SPEC_LEAVE", "SPEC_SURE_CTF", iButtonBits)
						ELSE
							SET_WARNING_MESSAGE_WITH_HEADER("SPEC_LEAVE", "SPEC_SURE_MISS", iButtonBits)
						ENDIF
					ELSE
						SET_WARNING_MESSAGE_WITH_HEADER("SPEC_LEAVE", "SPEC_SURE", iButtonBits)
					ENDIF
				BREAK
				
			ENDSWITCH
		ENDIF
	ENDIF
		
	IF bShouldRenderButtons	//bShouldRenderHUD
		//Instructional buttons
		
		//INT iTotalButtons
	
		IF NOT IS_PLAYER_SCTV(PLAYER_ID())		//If we're not sctv, or we're a "Spectate Player" sctv
		OR NOT IS_PLAYER_A_ROCKSTAR_DEV()
			IF IS_BIT_SET(specData.specCamData.iBitSet, SPEC_CAM_BS_THIS_ACTIVE)
				
				IF GET_SPEC_CAM_MODE(specData.SpecCamData) <> SPEC_MODE_NEWS
				//SPEC_MODE_TV_CHANNEL
				
					IF IS_BIT_SET(specData.specHUDData.iBitset, SPEC_HUD_BS_REQUEST_UPDATE_INSTRUCTIONAL_BUTTONS)
					OR IS_BIT_SET(MPSpecGlobals.iBitSet, SCTV_BIT_GLOBAL_REFRESH_BUTTONS)
					OR HAVE_CONTROLS_CHANGED(PLAYER_CONTROL)
					
						REFRESH_SCALEFORM_INSTRUCTIONAL_BUTTONS(specData.specHUDData.scaleformInstructionalButtons)
						
						IF GET_SPECTATOR_HUD_STAGE(specData.specHUDData) = thisStage
						AND GET_SPECTATOR_HUD_STAGE(specData.specHUDData) <> SPEC_HUD_STAGE_QUIT_YES_NO
							
							//Draw the instructional buttons!
							
							//=== CROSS ===
							IF specData.specHUDData.specHUDMode = SPEC_HUD_MODE_FULL
								BOOL bAddCross = FALSE
								IF thisStage = SPEC_HUD_STAGE_FULL_LIST 
									IF NOT IS_PLAYER_SCTV(PLAYER_ID()) 
									OR GET_SCTV_MODE() = SCTV_MODE_FOLLOW_CAM
										IF DOES_ENTITY_EXIST(GET_SPECTATOR_HUD_CURRENTLY_HIGHLIGHTED_PED_INDEX(specData.specHUDData))
											IF IS_PED_A_PLAYER(GET_SPECTATOR_HUD_CURRENTLY_HIGHLIGHTED_PED_INDEX(specData.specHUDData))
												INT iPlayer 
												iPlayer = NATIVE_TO_INT(NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_SPECTATOR_HUD_CURRENTLY_HIGHLIGHTED_PED_INDEX(specData.specHUDData)))
												IF iPlayer != -1
													IF GET_SPECTATOR_HUD_CURRENTLY_HIGHLIGHTED_PED_INDEX(specData.specHUDData) <> GET_SPECTATOR_SELECTED_PED()
													AND DOES_PLAYER_ALLOW_SPECTATORS(iPlayer)
													AND IS_HIGHLIGHTED_PLAYER_VALID_TO_SPECTATE(NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_SPECTATOR_HUD_CURRENTLY_HIGHLIGHTED_PED_INDEX(specData.specHUDData)))
														bAddCross = TRUE
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								IF bAddCross
									//iTotalButtons++
									//PRINTLN(" SPECATTOR HUD - iTotalButtons = ", iTotalButtons)
									ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(	GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, GET_SPECTATOR_ACCEPT_CANCEL_INPUT(TRUE)), 
																		"HUD_INPUT28", specData.specHUDData.scaleformInstructionalButtons)//SPECTATE
								ENDIF
							ENDIF
							
							//=== CIRCLE ===
							IF NOT IS_BIT_SET(specData.specHUDData.iBitset, SPEC_HUD_BS_BLOCK_CIRCLE_INSTRUCTION)
								IF CAN_SPECTATOR_QUIT()
									IF GET_SPEC_CAM_MODE(specData.SpecCamData) = SPEC_MODE_TV_CHANNEL
									//#IF FEATURE_HEIST_PLANNING OR GET_SPEC_CAM_MODE(specData.SpecCamData) = SPEC_MODE_HEIST #ENDIF
										//iTotalButtons++
										//PRINTLN(" SPECATTOR HUD - iTotalButtons = ", iTotalButtons)
										ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(	GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, GET_SPECTATOR_ACCEPT_CANCEL_INPUT(FALSE)), 
																			"HUD_INPUT3", specData.specHUDData.scaleformInstructionalButtons)//BACK
									ELSE
										IF specData.specHUDData.specHUDMode = SPEC_HUD_MODE_FULL
											//IF thisStage <> SPEC_HUD_STAGE_TARGET_FOCUS
												IF IS_BIT_SET(specData.specCamData.iActivateBitset, ASCF_INDEX_QUIT_SCREEN)
												 OR GET_SPEC_CAM_MODE(specData.SpecCamData) = SPEC_MODE_HEIST 
													//iTotalButtons++
													//PRINTLN(" SPECATTOR HUD - iTotalButtons = ", iTotalButtons)
													ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(	GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, GET_SPECTATOR_ACCEPT_CANCEL_INPUT(FALSE)), 
																						"HUD_INPUT69", specData.specHUDData.scaleformInstructionalButtons)//LEAVE SPECTATOR MODE
												ENDIF
											//ENDIF
											
											//IF thisStage = SPEC_HUD_STAGE_TARGET_FOCUS
											//	ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(	GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, GET_SPECTATOR_ACCEPT_CANCEL_INPUT(FALSE)), 
											//										"HUD_INPUT3", specData.specHUDData.scaleformInstructionalButtons)//BACK
											//ENDIF
										ENDIF
									ENDIF
								ELSE
									#IF IS_DEBUG_BUILD
									IF (GET_FRAME_COUNT() % 30) = 0
										PRINTLN(" ADD_SCALEFORM_INSTRUCTIONAL_BUTTON - DON'T ADD QUIT BUTTON - CAN_SPECTATOR_QUIT() = FALSE")
									ENDIF
									#ENDIF
								ENDIF
							ENDIF
							
							
							//=== TRIANGLE ===
							IF GET_SPEC_CAM_MODE(specData.SpecCamData) = SPEC_MODE_TV_CHANNEL
								//iTotalButtons++
								//PRINTLN(" SPECATTOR HUD - iTotalButtons = ", iTotalButtons)
								ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(	GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_RUP)), 
																	"HUD_INPUT82", specData.specHUDData.scaleformInstructionalButtons)//TURN OFF
							ELSE
								IF thisStage = SPEC_HUD_STAGE_FULL_LIST
								OR thisStage = SPEC_HUD_STAGE_TARGET_FOCUS
								OR thisStage = SPEC_HUD_STAGE_LEADERBOARD
									IF specData.specHUDData.specHUDMode = SPEC_HUD_MODE_FULL
										IF NOT IS_BIT_SET(specData.specCamData.iBitset, SPEC_CAM_BS_USE_RADIO_CONTROLS)
											//iTotalButtons++
											//PRINTLN(" SPECATTOR HUD - iTotalButtons = ", iTotalButtons)
											ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(	GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_RUP)), 
																				"HUD_INPUT83", specData.specHUDData.scaleformInstructionalButtons)//MINIMAL HUD
										ELSE
											IF NOT IS_BIT_SET(MPSpecGlobals.iBailBitset, GLOBAL_SPEC_BAIL_BS_RADIO_ON)
												//iTotalButtons++
												//PRINTLN(" SPECATTOR HUD - iTotalButtons = ", iTotalButtons)
												ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(	GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_RUP)), 
																				"HUD_SPECRY", specData.specHUDData.scaleformInstructionalButtons)//Local Radio On
											ELSE
												//iTotalButtons++
												//PRINTLN(" SPECATTOR HUD - iTotalButtons = ", iTotalButtons)
												ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(	GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_RUP)), 
																				"HUD_SPECRN", specData.specHUDData.scaleformInstructionalButtons)//Local Radio Off
											ENDIF
										ENDIF
									ELIF specData.specHUDData.specHUDMode = SPEC_HUD_MODE_MINIMAL
										//iTotalButtons++
										//PRINTLN(" SPECATTOR HUD - iTotalButtons = ", iTotalButtons)
										ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(	GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_RUP)), 
																			"HUD_INPUT39", specData.specHUDData.scaleformInstructionalButtons)//HIDE HUD
									ENDIF
								ENDIF
							ENDIF
							
							
							//=== SQUARE ===
							IF specData.specHUDData.specHUDMode = SPEC_HUD_MODE_FULL
								//IF (IS_PLAYER_SCTV(PLAYER_ID()) AND NOT IS_PLAYER_ON_ANY_MP_MISSION(PLAYER_ID()))
								IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_SURVIVAL
								OR GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_MISSION
								OR GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_DEATHMATCH
								OR GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_BASE_JUMP
								OR GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_RACE
									IF IS_LBD_READY()
									AND NOT IS_BIT_SET(specData.specHUDData.iBitset, SPEC_HUD_BS_NO_VALID_TARGET_SHOW_LEADERBOARD)
										IF thisStage = SPEC_HUD_STAGE_FULL_LIST
										OR thisStage = SPEC_HUD_STAGE_TARGET_FOCUS
											IF ARE_SPECTATOR_HUD_TARGET_LISTS_POPULATED(specData.specHUDData)
											#IF IS_DEBUG_BUILD
											OR specData.specHUDData.bDebugForceAllowLeaderboard
											#ENDIF
												//iTotalButtons++
												//PRINTLN(" SPECATTOR HUD - iTotalButtons = ", iTotalButtons)
												ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(	GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_RLEFT)), 
																					"HUD_INPUT40", specData.specHUDData.scaleformInstructionalButtons)//SHOW LEADERBOARD
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								
								IF thisStage = SPEC_HUD_STAGE_LEADERBOARD
									//iTotalButtons++
									//PRINTLN(" SPECATTOR HUD - iTotalButtons = ", iTotalButtons)
									IF NOT IS_BIT_SET(specData.specHUDData.iBitset, SPEC_HUD_BS_NO_VALID_TARGET_SHOW_LEADERBOARD)
										ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(	GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_RLEFT)), 
																			"HUD_INPUT43", specData.specHUDData.scaleformInstructionalButtons)//HIDE LEADERBOARD
									ENDIF
								ENDIF
							ENDIF
							
							
							//=== DPAD ===
							IF specData.specHUDData.specHUDMode = SPEC_HUD_MODE_FULL
								IF thisStage = SPEC_HUD_STAGE_FULL_LIST
								OR thisStage = SPEC_HUD_STAGE_TARGET_FOCUS
								OR thisStage = SPEC_HUD_STAGE_LEADERBOARD
									IF NOT IS_BIT_SET(specData.specCamData.iBitset, SPEC_CAM_BS_USE_RADIO_CONTROLS)
										IF NOT IS_SPECTATOR_FILTER_OPTION_DISABLED()
											//iTotalButtons++
											//PRINTLN(" SPECATTOR HUD - iTotalButtons = ", iTotalButtons)
											ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(	GET_CONTROL_GROUP_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, GET_SPECTATOR_CAM_INPUT_GROUP(INPUTGROUP_SCRIPT_DPAD_LR)), 
																			"HUD_INPUT72", specData.specHUDData.scaleformInstructionalButtons)//SELECT FILTER
										ENDIF
									ELSE
										IF NOT IS_SPECTATOR_FILTER_OPTION_DISABLED()
										//iTotalButtons++
										//PRINTLN(" SPECATTOR HUD - iTotalButtons = ", iTotalButtons)
											ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(	GET_CONTROL_GROUP_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, GET_SPECTATOR_CAM_INPUT_GROUP(INPUTGROUP_SCRIPT_DPAD_LR)), 
																			"HUD_SPECSR", specData.specHUDData.scaleformInstructionalButtons)//Select Station
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
							//=== LEFT STICK ===
							IF GET_SPEC_CAM_MODE(specData.SpecCamData) = SPEC_MODE_TV_CHANNEL
								ALLOW_ALTERNATIVE_SCRIPT_CONTROLS_LAYOUT(FRONTEND_CONTROL)
								//iTotalButtons++
								//PRINTLN(" SPECATTOR HUD - iTotalButtons = ", iTotalButtons)
								ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(	GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_X), 
																	"HUD_INPUT75", specData.specHUDData.scaleformInstructionalButtons)//SELECT CHANNEL
							ENDIF
							
							//FEED/PLAYER LIST
							IF specData.specHUDData.specHUDMode = SPEC_HUD_MODE_FULL
								IF specData.specHUDData.iCurrentNumberOfTargetsInAllLists > SPEC_HUD_NUM_PLAYERS_TO_HIDE_FEED
									IF specData.specHUDData.eSpecHUDStage <> SPEC_HUD_STAGE_LEADERBOARD
										IF NOT IS_BIT_SET(specData.specHUDData.iBitset, SPEC_HUD_BS_SHOW_FEED)
											ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(	GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_LS)), 
																							"SCTV_FEED0", specData.specHUDData.scaleformInstructionalButtons)// "Show Feed"
										ELSE
											ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(	GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_LS)), 
																							"SCTV_FEED1", specData.specHUDData.scaleformInstructionalButtons)// "Show List"
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
							//=== RIGHT STICK ===
							/*IF specData.specHUDData.specHUDMode = SPEC_HUD_MODE_FULL
								IF NOT IS_BIT_SET(specData.specCamData.iBitset, SPEC_CAM_BS_USING_SPECIAL_CAMERA)
									IF NOT IS_PLAYER_SCTV(PLAYER_ID())
									OR GET_SCTV_MODE()  = SCTV_MODE_FOLLOW_CAM
										IF NOT IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_MG_ARM_WRESTLING)
										AND NOT IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_MG_GOLF)
										AND NOT IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_MG_SHOOTING_RANGE)
										AND NOT IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_MG_TENNIS)
										AND NOT IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_MG_DARTS)
										AND NOT IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_CINEMA)
										AND NOT bSCTVHeliActive
											IF NOT IS_BIT_SET(MPSpecGlobals.SCTVData.iBitSet, SCTV_BIT_SPECTATING_DIFFERENT_CAMERA)
												//iTotalButtons++
												//PRINTLN(" SPECATTOR HUD - iTotalButtons = ", iTotalButtons)
												ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(	GET_CONTROL_GROUP_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUTGROUP_LOOK), 
																					"HUD_INPUT63", specData.specHUDData.scaleformInstructionalButtons)//CAMERA ANGLE
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF*/
							
							
							//=== SELECT ===
							IF specData.specHUDData.specHUDMode = SPEC_HUD_MODE_FULL
							AND NOT IS_BIT_SET(MPSpecGlobals.SCTVData.iBitSet, SCTV_BIT_TURF_WARS_CAMER_ACTIVE)
								IF thisStage = SPEC_HUD_STAGE_LEADERBOARD
									STRING sProfile = GET_PROFILE_BUTTON_NAME()
									
									pedHighlight = GET_SPECTATOR_HUD_CURRENTLY_HIGHLIGHTED_PED_INDEX(specData.specHUDData)
									IF DOES_ENTITY_EXIST(pedHighlight)
										IF IS_PED_A_PLAYER(pedHighlight)
											playerHighlight = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedHighlight)
											IF IS_NET_PLAYER_OK(playerHighlight, FALSE)
									
									
												//iTotalButtons++
												//PRINTLN(" SPECATTOR HUD - iTotalButtons = ", iTotalButtons)
												IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
													ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(	GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_LB)),
																					sProfile, specData.specHUDData.scaleformInstructionalButtons)//GAMER CARD
												ELSE
													//Disable camera change when this is shown
													DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
													ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(	GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_NEXT_CAMERA),
																					sProfile, specData.specHUDData.scaleformInstructionalButtons)//GAMER CARD
												ENDIF
											ENDIF
										ENDIF
									ENDIF
									
								ELSE
									// === Camera === button
									IF NOT IS_BIT_SET(specData.specCamData.iBitset, SPEC_CAM_BS_USING_SPECIAL_CAMERA)
										IF NOT IS_PLAYER_SCTV(PLAYER_ID())
										OR GET_SCTV_MODE()  = SCTV_MODE_FOLLOW_CAM
											IF NOT IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_MG_ARM_WRESTLING)
											AND NOT IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_MG_GOLF)
											AND NOT IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_MG_SHOOTING_RANGE)
											AND NOT IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_MG_TENNIS)
											AND NOT IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_MG_DARTS)
											AND NOT IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_CINEMA)
											AND NOT bSCTVHeliActive
											AND g_iFMMCCurrentStaticCam = -1
											AND NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_TURF_WARZONE(g_FMMC_STRUCT.iAdversaryModeType)
											AND NOT IS_PLAYER_IN_ANY_SIMPLE_INTERIOR(PLAYER_ID())
												IF NOT IS_BIT_SET(MPSpecGlobals.SCTVData.iBitSet, SCTV_BIT_SPECTATING_DIFFERENT_CAMERA)
													//iTotalButtons++
													//PRINTLN(" SPECATTOR HUD - iTotalButtons = ", iTotalButtons)
													ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(	GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_NEXT_CAMERA),  	// "Camera"
																					"HUD_INPUT87", specData.specHUDData.scaleformInstructionalButtons)//CAMERA MODE
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIf
							
							IF specData.specHUDData.specHUDMode = SPEC_HUD_MODE_FULL
//								PRINTLN("[CS_HELI_SPEC] specData.specHUDData.specHUDMode = SPEC_HUD_MODE_FULL")
								IF thisStage != SPEC_HUD_STAGE_LEADERBOARD
//									PRINTLN("[CS_HELI_SPEC] thisStage != SPEC_HUD_STAGE_LEADERBOARD")
									IF GET_SCTV_MODE()  = SCTV_MODE_FOLLOW_CAM
									OR NOT IS_PLAYER_SCTV(PLAYER_ID())
//										PRINTLN("[CS_HELI_SPEC] GET_SCTV_MODE()  != SCTV_MODE_FOLLOW_CAM")
										IF NETWORK_IS_ACTIVITY_SESSION()
											//PRINTLN("[CS_HELI_SPEC] NETWORK_IS_ACTIVITY_SESSION()")
											IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TURF_WARS(g_FMMC_STRUCT.iAdversaryModeType)
											OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_TURF_WARZONE(g_FMMC_STRUCT.iAdversaryModeType)
											OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_TOP_DOWN_CAM_FOR_RESPAWN_SPECTATORS)
											OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_DROP_THE_BOMB(g_FMMC_STRUCT.iAdversaryModeType)
												PRINTLN("[CS_HELI_SPEC] S_THIS_ROCKSTAR_MISSION_NEW_VS_TURF_WARS(g_FMMC_STRUCT.iAdversaryModeType)")
												ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(	GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_RS)), 
																				"DEADLINE_HELP_3", specData.specHUDData.scaleformInstructionalButtons)//CAMERA MODE
											ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciFORCE_STATIC_CAMERA_LOCATIONS)
												ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(	GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_RS)), 
																				"REZ_HELP_3", specData.specHUDData.scaleformInstructionalButtons)//CAMERA MODE
											ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
												ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(	GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_RS)), 
																				"SPEC_TTDV", specData.specHUDData.scaleformInstructionalButtons)//CAMERA MODE
											ENDIF
											
											
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
							//=== DPAD_UP ===
							IF specData.specHUDData.specHUDMode = SPEC_HUD_MODE_FULL
								IF thisStage = SPEC_HUD_STAGE_FULL_LIST 
									IF NOT IS_ON_AIR_RACE_GLOBAL_SET()
										IF IS_PLAYER_SCTV(PLAYER_ID()) 
											IF bSCTVHeliActive 	
												//iTotalButtons++
												//PRINTLN(" SPECATTOR HUD - iTotalButtons = ", iTotalButtons)										
												ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(	GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_RS)), 
																				"SCTV_HELI_OFF", specData.specHUDData.scaleformInstructionalButtons)// "Online TV Heli"			
												PRINTLN("[CS_HELI_SPEC] CAM RENDER_SPECTATOR_HUD, bSCTVHeliActive ")
											ELSE
												IF bShowTextSCTV = TRUE
													//iTotalButtons++
													//PRINTLN(" SPECATTOR HUD - iTotalButtons = ", iTotalButtons)
													ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(	GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_RS)), 
																				"SCTV_HELI_ON", specData.specHUDData.scaleformInstructionalButtons)// "Online TV Heli"	
													
													PRINTLN("[CS_HELI_SPEC] CAM RENDER_SPECTATOR_HUD, bShowTextSCTV = TRUE ")
													
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ELIF thisStage = SPEC_HUD_STAGE_HELI
									
//										ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(	GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_FRONTEND_UP), 
//																			"SCTV_HELI_ON", specData.specHUDData.scaleformInstructionalButtons)// "Online TV Heli"	
																		
								ENDIF
							ENDIF
							
							//=== PAUSE ===
							IF IS_PC_VERSION()
							#IF FEATURE_COPS_N_CROOKS
							OR (IS_FREEMODE_ARCADE()
							AND GET_ARCADE_MODE() = ARC_COPS_CROOKS)
							#ENDIF
								IF specData.specHUDData.specHUDMode = SPEC_HUD_MODE_FULL
									ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(	GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE), 
																		"SPEC_PAUSE", specData.specHUDData.scaleformInstructionalButtons)	//Pause
								ENDIF
							ENDIF
							
							//=== START ===
							
							
							//=== R3 ===
							
							
							//=== L3 ===
							
							
							//=== R2 ===
							IF specData.specHUDData.specHUDMode = SPEC_HUD_MODE_FULL
								IF thisStage = SPEC_HUD_STAGE_FULL_LIST 
									IF NOT IS_PLAYER_SCTV(PLAYER_ID()) 
									OR GET_SCTV_MODE() = SCTV_MODE_FOLLOW_CAM
										IF ARE_SPECTATOR_HUD_TARGET_LISTS_POPULATED_ENOUGH_TO_SCROLL(specData.specHUDData)
										AND NOT IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_MG_SHOOTING_RANGE)
										AND NOT IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_MG_DARTS)
										AND NOT IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_MG_ARM_WRESTLING)
										AND NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_Relay(g_FMMC_STRUCT.iRootContentIDHash)
										AND g_iFMMCCurrentStaticCam = -1
											//iTotalButtons++
											//PRINTLN(" SPECATTOR HUD - iTotalButtons = ", iTotalButtons)
											ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(	GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_RT)), 
																				"HUD_SPECDN", specData.specHUDData.scaleformInstructionalButtons)//Spectate Next Player
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
							//=== L2 ===
							IF specData.specHUDData.specHUDMode = SPEC_HUD_MODE_FULL
								IF thisStage = SPEC_HUD_STAGE_FULL_LIST 
									IF NOT IS_PLAYER_SCTV(PLAYER_ID()) 
									OR GET_SCTV_MODE() = SCTV_MODE_FOLLOW_CAM
										IF ARE_SPECTATOR_HUD_TARGET_LISTS_POPULATED_ENOUGH_TO_SCROLL(specData.specHUDData)
										AND NOT IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_MG_SHOOTING_RANGE)
										AND NOT IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_MG_DARTS)
										AND NOT IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_MG_ARM_WRESTLING)
										AND NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_Relay(g_FMMC_STRUCT.iRootContentIDHash)
										AND g_iFMMCCurrentStaticCam = -1
											//iTotalButtons++
											//PRINTLN(" SPECATTOR HUD - iTotalButtons = ", iTotalButtons)
											ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(	GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_LT)), 
																				"HUD_SPECUP", specData.specHUDData.scaleformInstructionalButtons)//Spectate Previous Player
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
							//=== R1 ===
							IF IS_PLAYER_SCTV(PLAYER_ID())		//if a rockstar dev SCTV, display info for changing camera modes
							AND IS_PLAYER_A_ROCKSTAR_DEV()
								IF thisStage = SPEC_HUD_STAGE_FULL_LIST
								OR thisStage = SPEC_HUD_STAGE_TARGET_FOCUS
								OR thisStage = SPEC_HUD_STAGE_LEADERBOARD
									IF GET_SCTV_MODE()  = SCTV_MODE_FREE_CAM
										IF ARE_SPECTATOR_HUD_TARGET_LISTS_POPULATED(specData.specHUDData)
											//iTotalButtons++
											//PRINTLN(" SPECATTOR HUD - iTotalButtons = ", iTotalButtons)
											ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(	GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_RB)), 
																				"HUD_INPUT41", specData.specHUDData.scaleformInstructionalButtons)//PLAYER CAMERA
										ENDIF
									ELIF GET_SCTV_MODE()  = SCTV_MODE_FOLLOW_CAM
										//iTotalButtons++
										//PRINTLN(" SPECATTOR HUD - iTotalButtons = ", iTotalButtons)
										ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(	GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_RB)), 
																			"HUD_INPUT51", specData.specHUDData.scaleformInstructionalButtons)//FREE CAMERA
									ENDIF
								ENDIF
							ELSE
								IF specData.specHUDData.specHUDMode = SPEC_HUD_MODE_FULL
								AND NOT USING_HEIST_SPECTATE()
									IF thisStage = SPEC_HUD_STAGE_FULL_LIST
									OR thisStage = SPEC_HUD_STAGE_TARGET_FOCUS
									OR thisStage = SPEC_HUD_STAGE_LEADERBOARD
										IF NOT IS_BIT_SET(specData.specCamData.iBitset, SPEC_CAM_BS_USE_RADIO_CONTROLS)
											//iTotalButtons++
											//PRINTLN(" SPECATTOR HUD - iTotalButtons = ", iTotalButtons)
											ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(	GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_RB)), 
																				"HUD_SPECRC", specData.specHUDData.scaleformInstructionalButtons)	//Radio Controls
										ELSE
											IF NOT IS_SPECTATOR_FILTER_OPTION_DISABLED()
												//iTotalButtons++
												//PRINTLN(" SPECATTOR HUD - iTotalButtons = ", iTotalButtons)
												ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(	GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_RB)), 
																					"HUD_SPECFC", specData.specHUDData.scaleformInstructionalButtons)	//Filter Controls
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
							//=== L1 ===
							IF specData.specHUDData.specHUDMode = SPEC_HUD_MODE_FULL
								IF thisStage <> SPEC_HUD_STAGE_LEADERBOARD
									
									pedHighlight = GET_SPECTATOR_HUD_CURRENTLY_HIGHLIGHTED_PED_INDEX(specData.specHUDData)
									IF DOES_ENTITY_EXIST(pedHighlight)
										IF IS_PED_A_PLAYER(pedHighlight)
											playerHighlight = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedHighlight)
											IF IS_NET_PLAYER_OK(playerHighlight, FALSE)
												STRING sProfile = GET_PROFILE_BUTTON_NAME()
												//iTotalButtons++
												//PRINTLN(" SPECATTOR HUD - iTotalButtons = ", iTotalButtons)
												ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(	GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, GET_SPECTATOR_CAM_INPUT(INPUT_SCRIPT_LB)), 
																						sProfile, specData.specHUDData.scaleformInstructionalButtons)//GAMER CARD
											ENDIF
										ENDIF
									ENDIF
									
								ENDIF
							ENDIF
						ENDIF
							
						CLEAR_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_REQUEST_UPDATE_INSTRUCTIONAL_BUTTONS)
						CLEAR_BIT(MPSpecGlobals.SCTVData.iBitSet, SCTV_BIT_GLOBAL_REFRESH_BUTTONS)
					ENDIF
					
					IF specData.specHUDData.specHUDMode = SPEC_HUD_MODE_FULL
					OR specData.specHUDData.specHUDMode = SPEC_HUD_MODE_MINIMAL
						SPRITE_PLACEMENT thisSpritePlacement = 	GET_SCALEFORM_INSTRUCTIONAL_BUTTON_POSITION()
						INT iNumberOfRows = 2
						
						IF specData.specHUDData.specHUDMode = SPEC_HUD_MODE_FULL
							//IF NOT GET_IS_WIDESCREEN()	//REMOVED FOR BUG 2207177
							//OR GET_CURRENT_LANGUAGE() = LANGUAGE_JAPANESE
								//SET_SCALEFORM_INSTRUCTIONAL_BUTTON_WRAP(specData.specHUDData.scaleformInstructionalButtons, 0.65)
								//SET_SCALEFORM_INSTRUCTIONAL_BUTTON_WRAP(specData.specHUDData.scaleformInstructionalButtons, 0.5)
								//iNumberOfRows = 2
							//ELSE
								SET_SCALEFORM_INSTRUCTIONAL_BUTTON_WRAP(specData.specHUDData.scaleformInstructionalButtons, 0.7)
								//iNumberOfRows = 2
							//ENDIF
						ENDIF
						
						SET_INSTRUCTIONAL_BUTTONS_UNDER_HUD_THIS_FRAME(iNumberOfRows)
						SET_LOADING_ICON_SUBTITLES_OFFSET_SHIFT_THIS_FRAME(iNumberOfRows)
						#IF IS_DEBUG_BUILD IF IS_KEYBOARD_KEY_PRESSED(KEY_0) CPRINTLN(DEBUG_SPECTATOR, "=== HUD === SET_LOADING_ICON_SUBTITLES_OFFSET_SHIFT_THIS_FRAME IS BEING CALLED") ENDIF #ENDIF
						
						IF BUSYSPINNER_IS_ON()
							IF NOT DOES_CAM_EXIST(specData.specCamData.camCinematic)
							OR NOT IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_MG_PILOT_SCHOOL)
								BUSYSPINNER_OFF()
							ENDIF
						ENDIF
						
						RUN_SCALEFORM_INSTRUCTIONAL_BUTTONS(specData.specHUDData.sfButton, 
															thisSpritePlacement, 
															specData.specHUDData.scaleformInstructionalButtons, 
															SHOULD_REFRESH_SCALEFORM_INSTRUCTIONAL_BUTTONS(specData.specHUDData.scaleformInstructionalButtons))
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF
ENDPROC

/// PURPOSE:
///    Perform spectator hud initialisation stage
/// PARAMS:
///    specData - 
/// RETURNS:
///    true when complete
FUNC BOOL DO_SPEC_HUD_STAGE_INIT(SPECTATOR_DATA_STRUCT &specData)

	DISABLE_ALL_MP_HUD()
	
	DONT_RENDER_IN_GAME_UI(FALSE)
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_SPECTATOR, "=== HUD === DONT_RENDER_IN_GAME_UI(FALSE)")
	#ENDIF
	
	SET_HUD_COMPONENT_POSITION(NEW_HUD_SUBTITLE_TEXT, 0, -0.0375)
	
	IF GET_SPEC_CAM_MODE(specData.specCamData) <> SPEC_MODE_NEWS
		DISABLE_SCRIPT_HUD(HUDPART_ALL_OVERHEADS, FALSE)
	ENDIF
	
	INT iList, iEntry
	IF FIND_PED_IN_SPECTATOR_HUD_TARGET_LISTS(specData.specHUDData, iList, iEntry, GET_SPECTATOR_CURRENT_FOCUS_PED())
		SET_SPECTATOR_HUD_CURRENT_FOCUS_TARGET_FROM_LIST(specData, iList, iEntry)
	ELSE
		SET_SPECTATOR_HUD_CURRENT_FOCUS_TARGET_FROM_LIST(specData, 0, 0)
	ENDIF
	
	CALCULATE_NUMBER_OF_TARGETS_ON_A_PAGE(specData.specHUDData)
	
	specData.specHUDData.screenRT = GET_DEFAULT_SCRIPT_RENDERTARGET_RENDER_ID()
	
	IF SHOULD_TEAM_COLOUR_OVERRIDE_BE_ACTIVE()
		SET_TEAM_COLOUR_OVERRIDE_FLAG(TRUE)
	ENDIF
	
	//Help text for first time in spec cam
	IF NOT IS_PLAYER_SCTV(PLAYER_ID())
	AND NOT DID_I_JOIN_MISSION_AS_SPECTATOR()
		IF GET_SPEC_CAM_MODE(specData.specCamData) <> SPEC_MODE_NEWS
			IF GET_FM_MISSION_TYPE_PLAYER_IS_ON(PLAYER_ID()) = FMMC_TYPE_MISSION
				INT iSpecCamHelpStat = GET_MP_INT_CHARACTER_STAT(MP_STAT_SPECTATOR_CAM_HELP_TEXT)
				IF iSpecCamHelpStat < NUMBER_OF_TIMES_TO_DISPLAY_HELP
				
					SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_REQUEST_SPEC_HELP_TEXT)
				
					iSpecCamHelpStat++
					SET_MP_INT_CHARACTER_STAT(MP_STAT_SPECTATOR_CAM_HELP_TEXT, iSpecCamHelpStat)
				ENDIF
			ELIF GET_FM_MISSION_TYPE_PLAYER_IS_ON(PLAYER_ID()) = FMMC_TYPE_SURVIVAL
				IF NOT IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_ON_LAST_SURVIVAL_ROUND)
					SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_REQUEST_SPEC_HELP_TEXT)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_REQUEST_UPDATE_INSTRUCTIONAL_BUTTONS)
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Checks if spectator input is currently allowed
/// PARAMS:
///    specData - 
/// RETURNS:
///    true/false
FUNC BOOL ALLOW_SPECTATOR_INPUT(SPECTATOR_DATA_STRUCT &specData)
	IF NOT IS_SCREEN_FADED_IN()
	OR IS_BIT_SET(specData.specCamData.iActivateBitset, ASCF_INDEX_CELEBRATION_SCREEN)
	OR IS_BIT_SET(specData.specHUDData.iBitset, SPEC_HUD_BS_WAITING_FOR_SWITCH)
	OR IS_BIT_SET(specData.specHUDData.iBitset, SPEC_CAM_DISABLE_ALL_CONTROLS)
	OR IS_PAUSE_MENU_ACTIVE()
	OR NETWORK_TEXT_CHAT_IS_TYPING() 
	
		#IF IS_DEBUG_BUILD
			IF GET_COMMANDLINE_PARAM_EXISTS("sc_DisplaySCTVStages")
				IF GET_FRAME_COUNT() % 10 = 0
					CPRINTLN(DEBUG_SPECTATOR, 	"=== SCTVSTAGES === ALLOW_SPECTATOR_INPUT = FALSE: IS_SCREEN_FADED_IN() = ", IS_SCREEN_FADED_IN())
					CPRINTLN(DEBUG_SPECTATOR, 	"=== SCTVSTAGES === ALLOW_SPECTATOR_INPUT: IS_BIT_SET(specData.specCamData.iActivateBitset, ASCF_INDEX_CELEBRATION_SCREEN) = ", IS_BIT_SET(specData.specCamData.iActivateBitset, ASCF_INDEX_CELEBRATION_SCREEN))
					CPRINTLN(DEBUG_SPECTATOR, 	"=== SCTVSTAGES === ALLOW_SPECTATOR_INPUT: IS_BIT_SET(specData.specHUDData.iBitset, SPEC_HUD_BS_WAITING_FOR_SWITCH) = ", IS_BIT_SET(specData.specHUDData.iBitset, SPEC_HUD_BS_WAITING_FOR_SWITCH))
					CPRINTLN(DEBUG_SPECTATOR, 	"=== SCTVSTAGES === ALLOW_SPECTATOR_INPUT: IS_BIT_SET(specData.specHUDData.iBitset, SPEC_CAM_DISABLE_ALL_CONTROLS) = ", IS_BIT_SET(specData.specHUDData.iBitset, SPEC_CAM_DISABLE_ALL_CONTROLS))
					CPRINTLN(DEBUG_SPECTATOR, 	"=== SCTVSTAGES === ALLOW_SPECTATOR_INPUT: IS_PAUSE_MENU_ACTIVE = ", IS_PAUSE_MENU_ACTIVE())
					CPRINTLN(DEBUG_SPECTATOR, 	"=== SCTVSTAGES === ALLOW_SPECTATOR_INPUT: NETWORK_TEXT_CHAT_IS_TYPING = ", NETWORK_TEXT_CHAT_IS_TYPING())
				ENDIF
			ENDIF
		#ENDIF

	
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Performs the full list stage, where the entire player is list on screen and the spectate target can be selected
/// PARAMS:
///    specData - 
/// RETURNS:
///    true when complete
FUNC BOOL DO_SPEC_HUD_STAGE_FULL_LIST(SPECTATOR_DATA_STRUCT &specData)
	IF IS_BIT_SET(specData.specHUDData.iBitset, SPEC_HUD_BS_WAITING_FOR_SWITCH)
		/*IF GET_SPECTATOR_DESIRED_FOCUS_PED() = GET_SPECTATOR_CURRENT_FOCUS_PED()
			//CLEAR_BIT(specHUDData.iBitset, SPEC_HUD_BS_WAITING_FOR_SWITCH)
			SET_SPECTATOR_HUD_STAGE(specData.specHUDData, SPEC_HUD_STAGE_TARGET_FOCUS)
		ENDIF*/
	ELSE
		IF ALLOW_SPECTATOR_INPUT(specData)
			MANAGE_STAGE_FULL_LIST_INPUT(specData)
		ENDIF
	ENDIF

	MANAGE_HIGHLIGHT_AND_SELECT_LIST(specData.specHUDData)
		
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    performs the stage that displays mroe info on one spectator target
/// PARAMS:
///    specData - 
/// RETURNS:
///    true when complete
FUNC BOOL DO_SPEC_HUD_STAGE_TARGET_FOCUS(SPECTATOR_DATA_STRUCT &specData)
	IF ALLOW_SPECTATOR_INPUT(specData)
		MANAGE_STAGE_MORE_INFO_INPUT(specData)
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Performs the stage where the leaderboard for the current job is displayed
/// PARAMS:
///    specData - 
/// RETURNS:
///    true when complete
FUNC BOOL DO_SPEC_HUD_STAGE_LEADERBOARD(SPECTATOR_DATA_STRUCT &specData)
	IF ALLOW_SPECTATOR_INPUT(specData)
		MANAGE_STAGE_LEADERBOARD_INPUT(specData)
	ENDIF
	
	/*IF IS_ANY_BIG_MESSAGE_BEING_DISPLAYED()									//Don't display if big message is displaying
		IF IS_BIT_SET(MPSpecGlobals.iBitset, GLOBAL_SPEC_BS_READY_FOR_LEADERBOARD)			//Clear up request for leaderboard
			CLEAR_BIT(MPSpecGlobals.iBitset, GLOBAL_SPEC_BS_READY_FOR_LEADERBOARD)
		ENDIF
		SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_REQUEST_UPDATE_INSTRUCTIONAL_BUTTONS)
		SET_SPECTATOR_HUD_STAGE(specData.specHUDData, specData.specHUDData.eSpecHUDStageVisitedBeforeLeaderboard)
	ENDIF*/		
	
	IF IS_BIT_SET(specData.specHUDData.iBitset, SPEC_HUD_BS_NO_VALID_TARGET_SHOW_LEADERBOARD)
		IF IS_BIT_SET(specData.specHUDData.iBitset, SPEC_HUD_BS_NO_VALID_TARGET_SHOW_LEADERBOARD_END)
			SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_REQUEST_REDO_SUMMARY_CARD)		//Update HUD display of lists
			SET_SPECTATOR_HUD_STAGE(specData.specHUDData, specData.specHUDData.eSpecHUDStageVisitedBeforeLeaderboard)
			CLEAR_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_NO_VALID_TARGET_SHOW_LEADERBOARD)
			CLEAR_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_NO_VALID_TARGET_SHOW_LEADERBOARD_END)
			CPRINTLN(DEBUG_SPECTATOR, "=== HUD === SPEC_HUD_BS_NO_VALID_TARGET_SHOW_LEADERBOARD - CLEARED")
			CPRINTLN(DEBUG_SPECTATOR, "=== HUD === SPEC_HUD_BS_NO_VALID_TARGET_SHOW_LEADERBOARD_END - CLEARED")
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Performs the stage where the player is asked to confirm if they want to quit
/// PARAMS:
///    specData - 
/// RETURNS:
///    true when complete
FUNC BOOL DO_SPEC_HUD_STAGE_QUIT_YES_NO(SPECTATOR_DATA_STRUCT &specData)
	IF ALLOW_SPECTATOR_INPUT(specData)
		MANAGE_STAGE_QUIT_YES_NO_INPUT(specData)
	ENDIF
		
	RETURN FALSE
ENDFUNC

//PURPOSE: Control doing the Kill Strip when target dies.
PROC MANAGE_SPEC_KILL_STRIP(SPECTATOR_DATA_STRUCT &specData)
	IF GET_SPEC_CAM_MODE(specData.specCamData) <> SPEC_MODE_NEWS
	AND IS_BIT_SET(specData.specCamData.iBitSet, SPEC_CAM_BS_THIS_ACTIVE)
	AND NOT IS_CUTSCENE_PLAYING()
	AND NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
		MAINTAIN_KILL_STRIP_DEATH_EFFECT()
		
		SWITCH specData.specHUDData.iSpecKillStripStage
			CASE 0
				IF DOES_ENTITY_EXIST(GET_SPECTATOR_CURRENT_FOCUS_PED()) //b* 2120479
					PLAYER_INDEX playerID
					
					PED_INDEX ped
					ped = GET_SPECTATOR_CURRENT_FOCUS_PED()
					
					IF DOES_ENTITY_EXIST(ped)
						playerID = NETWORK_GET_PLAYER_INDEX_FROM_PED(ped)
					ENDIF
					
					IF playerID != INVALID_PLAYER_INDEX()
						IF NOT IS_NET_PLAYER_OK(playerID, TRUE, FALSE)
							IF NOT IS_SCREEN_FADED_OUT()
							AND specData.specCamData.fadeSwitchStage = eSPECFADESWITCHSTAGE_SETUP
								TRIGGER_KILL_STRIP_DEATH_EFFECT_STAGE_1()
								specData.specHUDData.iSpecKillStripStage = 1
								CPRINTLN(DEBUG_SPECTATOR, "=== HUD === MANAGE_SPEC_KILL_STRIP - TRIGGER KILL STRIP - iSpecKillStripStage = ", specData.specHUDData.iSpecKillStripStage)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE 1
				IF IS_SCREEN_FADED_OUT()
					CLEANUP_ALL_KILL_STRIP()
					REFRESH_SPEC_HUD_FILTER()
					specData.specHUDData.iSpecKillStripStage = 0
					CPRINTLN(DEBUG_SPECTATOR, "=== HUD === MANAGE_SPEC_KILL_STRIP - CLEANUP_ALL_KILL_STRIP - SCREEN FADED OUT - iSpecKillStripStage = ", specData.specHUDData.iSpecKillStripStage)
				ENDIF
				
				IF specData.specCamData.fadeSwitchStage >= eSPECFADESWITCHSTAGE_SETUP_LOAD_SCENE
					CLEANUP_ALL_KILL_STRIP()
					REFRESH_SPEC_HUD_FILTER()
					specData.specHUDData.iSpecKillStripStage = 0
					CPRINTLN(DEBUG_SPECTATOR, "=== HUD === MANAGE_SPEC_KILL_STRIP - CLEANUP_ALL_KILL_STRIP - IN eSPECFADESWITCHSTAGE_SETUP_LOAD_SCENE - SCREEN FADED OUT - iSpecKillStripStage = ", specData.specHUDData.iSpecKillStripStage)
				ENDIF
				
				PLAYER_INDEX playerID
				
				PED_INDEX ped
				ped = GET_SPECTATOR_CURRENT_FOCUS_PED()
				
				IF DOES_ENTITY_EXIST(ped)
					playerID = NETWORK_GET_PLAYER_INDEX_FROM_PED(ped)
				ENDIF
				
				IF playerID != INVALID_PLAYER_INDEX()
					IF IS_NET_PLAYER_OK(playerID, TRUE, FALSE)
					AND IS_SCREEN_FADED_IN()
					AND specData.specCamData.eState = eSPECCAMSTATE_WATCHING
						CLEANUP_ALL_KILL_STRIP()
						REFRESH_SPEC_HUD_FILTER()
						specData.specHUDData.iSpecKillStripStage = 0
						CPRINTLN(DEBUG_SPECTATOR, "=== HUD === MANAGE_SPEC_KILL_STRIP - CLEANUP_ALL_KILL_STRIP - PLAYER IS ALIVE - iSpecKillStripStage = ", specData.specHUDData.iSpecKillStripStage)
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC


// Toggle the dev spectate challenge lbd global
PROC TOGGLE_DEV_SPEC_LBD(BOOL bOn)
	IF bOn
		IF NOT g_s_DevSpectateStruct.b_DevSpectateLbd
			g_s_DevSpectateStruct.b_DevSpectateLbd = TRUE
			PRINTLN("[DEV_SPC], TOGGLE_DEV_SPEC_LBD, TRUE ")
		ENDIF
	ELSE
		IF g_s_DevSpectateStruct.b_DevSpectateLbd
			g_s_DevSpectateStruct.b_DevSpectateLbd = FALSE
			PRINTLN("[DEV_SPC], TOGGLE_DEV_SPEC_LBD, FALSE ")
		ENDIF
	ENDIF
ENDPROC

// Check that it's ok to show the challenge dev spectate lbd
FUNC BOOL OK_TO_MOVE_TO_DEV_SPECTATE_DPAD_LBD()
	IF IS_PLAYER_DEV_SPECTATOR(PLAYER_ID())
		IF DOES_ENTITY_EXIST(GET_SPECTATOR_CURRENT_FOCUS_PED()) //b* 2120479
			PLAYER_INDEX playerID
			playerID = NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_SPECTATOR_CURRENT_FOCUS_PED())
			
			IF playerID != INVALID_PLAYER_INDEX()
				IF IS_NET_PLAYER_OK(playerID, TRUE, FALSE)
					
					IF FM_EVENT_IS_PLAYER_ON_ANY_FM_EVENT(playerID)

						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL DEV_SPECTATE_CHALLENGE_LBD_TOGGLED()
	IF OK_TO_MOVE_TO_DEV_SPECTATE_DPAD_LBD()
		IF HAS_DEV_CONTINUE_BUTTON_BEEN_PRESSED()

			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

 // #IF FEATURE_NEW_AMBIENT_EVENTS

/// PURPOSE:
///    Main spectator HUD loop function
/// PARAMS:
///    specData - 
PROC PROCESS_SPECTATOR_HUD(SPECTATOR_DATA_STRUCT &specData)
	//If not doing any systems stuff
	IF NOT IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)			//Don't bother if player is on transition hud
	AND NOT IS_PAUSE_MENU_ACTIVE()									//Don't bother if pause menu is up
	AND NOT IS_PLAYER_SWITCH_IN_PROGRESS()							//Don't bother if Brenda's player switch transition thing is in progress
	AND NOT GET_SPEC_CORONA_SCREEN_EFFECTS_ACTIVE()					// Don't draw while we are frozen and transitioning into the Job
	AND NOT IS_PLAYER_IN_CORONA()
		
		#IF IS_DEBUG_BUILD
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_ExtraSpectatorDebug")
			CPRINTLN(DEBUG_SPECTATOR, "=== HUD === PROCESS_SPECTATOR_HUD - Passed initial checks")
		ENDIF
		#ENDIF
		
		IF IS_BIT_SET(specData.specHUDData.iBitset, SPEC_HUD_BS_ACTIVE)
		OR IS_PLAYER_SCTV(PLAYER_ID())
			IF specData.specCamData.eState <> eSPECCAMSTATE_TURN_OFF
				MANAGE_SPEC_HUD_FILTERS(specData)
				MANAGE_SPEC_HUD_FILTER_RENDERING(specData.specHUDData)
				MANAGE_SPEC_KILL_STRIP(specData)
			ENDIF
		ENDIF

		IF IS_BIT_SET(specData.specHUDData.iBitset, SPEC_HUD_BS_ACTIVE)
		OR IS_PLAYER_SCTV(PLAYER_ID())
		
			#IF IS_DEBUG_BUILD
			IF GET_COMMANDLINE_PARAM_EXISTS("sc_ExtraSpectatorDebug")
				CPRINTLN(DEBUG_SPECTATOR, "=== HUD === PROCESS_SPECTATOR_HUD - Spectator active")
			ENDIF
			#ENDIF
		
			#IF IS_DEBUG_BUILD
				IF specData.specHUDData.bDebugInitSpecHUDWidget = FALSE
					
				ENDIF
			#ENDIF
			
			IF NOT IS_TEAM_COLOUR_OVERRIDE_FLAG_SET()
			AND SHOULD_TEAM_COLOUR_OVERRIDE_BE_ACTIVE()
				SET_TEAM_COLOUR_OVERRIDE_FLAG(TRUE)
			ENDIF
		 	
			//Disable and hide unwanted elements
			IF NOT IS_PLAYER_A_ROCKSTAR_DEV()
				DISABLE_DPADDOWN_THIS_FRAME()
			ENDIF
			
			DISPLAY_AMMO_THIS_FRAME(FALSE)			
			HUD_FORCE_WEAPON_WHEEL(FALSE)
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_WHEEL)
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_ICON)
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_HELP_TEXT)
			DISABLE_SPECTATOR_CONTROL_ACTIONS_GENERAL(specData)
			DISABLE_SELECTOR_THIS_FRAME()
			THEFEED_SET_SCRIPTED_MENU_HEIGHT(0.466664)
			
			IF GET_SPEC_CAM_MODE(specData.specCamData) <> SPEC_MODE_NEWS
			AND GET_SPEC_CAM_MODE(specData.specCamData) <> SPEC_MODE_TV_CHANNEL
			AND GET_SPEC_CAM_MODE(specData.specCamData) <> SPEC_MODE_HEIST 
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH)
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_MP_CASH)
				
				DISABLE_CELLPHONE_THIS_FRAME_ONLY()
				
			ENDIF
			DISPLAY_HUD_WHEN_NOT_IN_STATE_OF_PLAY_THIS_FRAME()
						
			//If game currently ok for displaying spectator HUD
			IF (NOT IS_PLAYER_SCTV(PLAYER_ID())
				OR GET_SCTV_MODE() = SCTV_MODE_FOLLOW_CAM		//Run if the player is a spectator in follow target, or is not a spectator
				OR GET_SCTV_MODE()  = SCTV_MODE_FREE_CAM)
				
				#IF IS_DEBUG_BUILD
				IF GET_COMMANDLINE_PARAM_EXISTS("sc_ExtraSpectatorDebug")
					CPRINTLN(DEBUG_SPECTATOR, "=== HUD === PROCESS_SPECTATOR_HUD - GET_SCTV_MODE()")
				ENDIF
				#ENDIF
				
				IF GET_SPECTATOR_HUD_STAGE(specData.specHUDData) <> SPEC_HUD_STAGE_QUIT_YES_NO					//If not on yes no
					IF GET_SPECTATOR_HUD_STAGE(specData.specHUDData) <> SPEC_HUD_STAGE_LEADERBOARD				//If not leaderboard
						specData.specHUDData.eSpecHUDStageVisitedBeforeLeaderboard = GET_SPECTATOR_HUD_STAGE(specData.specHUDData)	//Set this stage as last decent stage for leaderboard
					ENDIF
					
					specData.specHUDData.eSpecHUDStageVisitedBeforeQuitYesNo = GET_SPECTATOR_HUD_STAGE(specData.specHUDData)			//Set tjos stage as last decent stage for yesno
				ENDIF
				
				IF GET_SPECTATOR_HUD_STAGE(specData.specHUDData) = SPEC_HUD_STAGE_INIT
				
					#IF IS_DEBUG_BUILD
					IF GET_COMMANDLINE_PARAM_EXISTS("sc_ExtraSpectatorDebug")
						CPRINTLN(DEBUG_SPECTATOR, "=== HUD === PROCESS_SPECTATOR_HUD - SPEC_HUD_STAGE_INIT")
					ENDIF
					#ENDIF
				
					IF REQUEST_SCALEFORM_SUMMARY_CARD(FALSE, specData.specHUDData.sfSummaryCard, TRUE)
						IF DO_SPEC_HUD_STAGE_INIT(specData)
							//Cap it if it's more than the max!
							IF GET_SPECTATOR_HUD_CURRENTLY_HIGHLIGHTED_ROW(specData.specHUDData) >= specData.specHUDData.iMaximumPage
							OR GET_SPECTATOR_HUD_CURRENTLY_HIGHLIGHTED_ROW(specData.specHUDData) > specData.specHUDData.iNumberOfTargetsOnAPage
								SET_SPECTATOR_HUD_CURRENTLY_HIGHLIGHTED_ROW(specData.specHUDData, 0)
							ENDIF
							
							SET_SPECTATOR_HUD_STAGE(specData.specHUDData, SPEC_HUD_STAGE_FULL_LIST)
						ELSE
							#IF IS_DEBUG_BUILD
							IF GET_COMMANDLINE_PARAM_EXISTS("sc_ExtraSpectatorDebug")
								CPRINTLN(DEBUG_SPECTATOR, "=== HUD === PROCESS_SPECTATOR_HUD - NOT DO_SPEC_HUD_STAGE_INIT")
							ENDIF
							#ENDIF
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
						IF GET_COMMANDLINE_PARAM_EXISTS("sc_ExtraSpectatorDebug")
							CPRINTLN(DEBUG_SPECTATOR, "=== HUD === PROCESS_SPECTATOR_HUD - Loading scaleform summary card.")
						ENDIF
						#ENDIF
					ENDIF
				ELSE
					// Using this Global in order to allow Assets to be loaded in the MC.
					IF g_bAppliedMissionOutfit
					OR g_fmmc_struct.iMissionType != FMMC_TYPE_MISSION
					OR NOT NETWORK_IS_ACTIVITY_SESSION()
					
						IF IS_BIT_SET(specData.specHUDData.iBitset, SPEC_HUD_BS_ACTIVE)					//Big ol' global on/off switch
						OR (IS_PLAYER_SCTV(PLAYER_ID()) AND (GET_SCTV_MODE()  = SCTV_MODE_FREE_CAM 
															OR GET_SCTV_MODE()  = SCTV_MODE_FOLLOW_CAM))
							
							IF NOT SWAP_SPECTATOR_LIST_FOR_DPAD()
								MAINTAIN_SUMMARY_CARD(specData, GET_SPECTATOR_HUD_STAGE(specData.specHUDData))
							ENDIF
							
							SWITCH GET_SPECTATOR_HUD_STAGE(specData.specHUDData)
								
								CASE SPEC_HUD_STAGE_FULL_LIST
								CASE SPEC_HUD_STAGE_HELI
								
									
									IF DEV_SPECTATE_CHALLENGE_LBD_TOGGLED()
										SET_SPECTATOR_HUD_STAGE(specData.specHUDData, SPEC_HUD_STAGE_DEV_ONLY)
									ENDIF
									
									 // #IF FEATURE_NEW_AMBIENT_EVENTS
							
									DO_SPEC_HUD_STAGE_FULL_LIST(specData)
								BREAK
								
								CASE SPEC_HUD_STAGE_TARGET_FOCUS
									DO_SPEC_HUD_STAGE_TARGET_FOCUS(specData)
								BREAK
								
								CASE SPEC_HUD_STAGE_LEADERBOARD
									DO_SPEC_HUD_STAGE_LEADERBOARD(specData)
								BREAK
								
								CASE SPEC_HUD_STAGE_QUIT_YES_NO
									DO_SPEC_HUD_STAGE_QUIT_YES_NO(specData)
								BREAK
								
								
								CASE SPEC_HUD_STAGE_DEV_ONLY		
								
									IF DEV_SPECTATE_CHALLENGE_LBD_TOGGLED()
										SET_SPECTATOR_HUD_STAGE(specData.specHUDData, SPEC_HUD_STAGE_FULL_LIST)
									ENDIF
									
									IF NOT OK_TO_MOVE_TO_DEV_SPECTATE_DPAD_LBD()
										SET_SPECTATOR_HUD_STAGE(specData.specHUDData, SPEC_HUD_STAGE_FULL_LIST)
									ENDIF
									
									PRINTLN("[DEV_SPC], SPEC_HUD_STAGE_DEV_ONLY ")
								BREAK
								
								 //#IF FEATURE_NEW_AMBIENT_EVENTS
								
							ENDSWITCH
							
							
							TOGGLE_DEV_SPEC_LBD( (GET_SPECTATOR_HUD_STAGE(specData.specHUDData) = SPEC_HUD_STAGE_DEV_ONLY) )
							
							
							IF (specData.specCamData.bFirstTransitionComplete OR specData.specCamData.fadeSwitchStage > eSPECFADESWITCHSTAGE_SETUP_LOAD_SCENE)
							AND NOT IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_SCTV_IN_MOCAP)
								RENDER_SPECTATOR_HUD(specData, GET_SPECTATOR_HUD_STAGE(specData.specHUDData))
							ENDIF
							
							IF IS_BIT_SET(specData.specHUDData.iBitset, SPEC_HUD_BS_DO_FADE_IN_ONCE_SPEC_HUD_IS_RENDERING)
								IF IS_BIT_SET(g_iArenaLoadingBS, ciARENALOADINGBS_ArenaHasLoaded)
								OR NOT CONTENT_IS_USING_ARENA()
									IF HAS_NET_TIMER_EXPIRED(specData.specHUDData.FadeInTimer, 500)
										IF NOT IS_SCREEN_FADED_IN()
										AND NOT IS_SCREEN_FADING_IN()
											IF IS_PLAYER_SCTV(PLAYER_ID())
											OR NOT CONTENT_IS_USING_ARENA()
												DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME_SHORT)
											ENDIF
											ANIMPOSTFX_STOP_ALL()
											CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_WASTED)
										ENDIF
										CLEAR_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_DO_FADE_IN_ONCE_SPEC_HUD_IS_RENDERING)
										CPRINTLN(DEBUG_SPECTATOR, "=== CAM === SPEC_HUD_BS_DO_FADE_IN_ONCE_SPEC_HUD_IS_RENDERING - CLEARED - A")
									ENDIF
								ELSE
									CPRINTLN(DEBUG_SPECTATOR, "=== CAM === Delay fadein. Arena is loading for content.")
								ENDIF
							ENDIF
							
						ELSE
							#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_SPECTATOR, "=== HUD === Spectator Hud is not active yet because we are not looking at anyone.")
							#ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				PROCESS_SPEC_HUD_MODES(specData)
				
			ENDIF
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_ExtraSpectatorDebug")
			CPRINTLN(DEBUG_SPECTATOR, "=== HUD === PROCESS_SPECTATOR_HUD - Failed initial checks")
		ENDIF
		#ENDIF
	
		IF GET_SPEC_CORONA_SCREEN_EFFECTS_ACTIVE()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
		ENDIF
		
		//Finish up filter if needed
		IF specData.specHUDData.iFilterStage > 0
		AND specData.specHUDData.iFilterStage < 99
			MANAGE_SPEC_HUD_FILTERS(specData)
		ENDIF
	ENDIF
ENDPROC
