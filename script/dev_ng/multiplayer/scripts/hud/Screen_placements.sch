

USING "hud_drawing.sch"
USING "mp_icons.sch"

//HUD SCREEN
CONST_INT SCREEN_SELECT_CHARACTER		0
CONST_INT SCREEN_SELECT_NEW_GANG		1
CONST_INT SCREEN_NEW_CHARACTER			2
CONST_INT SCREEN_JOIN_GAME				3
CONST_INT SCREEN_DELETE_CHAR			4
//CONST_INT SCREEN_FADE_OUT				5
CONST_INT SCREEN_CLEANUP				6
CONST_INT SCREEN_NOT_CONNECTED			7
//CONST_INT SCREEN_SELECT_WEAPON			8
//CONST_INT SCREEN_SELECT_KIT				9
CONST_INT SCREEN_CONFIRM_LEAVE			10
CONST_INT SCREEN_STATS_GENERAL			11
CONST_INT SCREEN_STATS_WEAPON			12
//CONST_INT SCREEN_STATS_VEHICLE			13
CONST_INT SCREEN_STATS_AWARDS			14
CONST_INT SCREEN_LEADERBOARD			15
CONST_INT SCREEN_LOADING				16
CONST_INT SCREEN_SPAWN_SELECT			17
CONST_INT SCREEN_SWAP_TEAM_CONFIRM		18
CONST_INT SCREEN_COULD_NOT_JOIN			19
CONST_INT SCREEN_SELECT_GAME_MODE		20
//CONST_INT SCREEN_FREEMODE_OPTIONS		21
//CONST_INT SCREEN_FREEMODE_PLAYER_LIST	22
CONST_INT SCREEN_CHARACTER_OVERVIEW		23
CONST_INT SCREEN_TEAM_MATE				24
CONST_INT SCREEN_MISSION_CREATOR		25
CONST_INT SCREEN_JOINING_OVERLAY		26
CONST_INT SCREEN_DEATHMATCH_LOBBY		27
CONST_INT SCREEN_DEATHMATCH_LEAVE		28
CONST_INT SCREEN_RACES_LOBBY		29
CONST_INT SCREEN_RACES_LEAVE		30
CONST_INT SCREEN_MISSION_CREATOR_SELECTOR		31
CONST_INT SCREEN_NETWORK_BAILED 	32
CONST_INT SCREEN_NEW_FACE			33


////////HUD DISPLAYS
CONST_INT MAX_NUMBER_OF_SPRITE_PLACEMENTS 			10
CONST_INT MAX_NUMBER_OF_TEXT_PLACEMENTS				20
CONST_INT MAX_NUMBER_OF_RECTS						11

////////HUD DISPLAYS
CONST_INT LARGE_MAX_NUMBER_OF_SPRITE_PLACEMENTS 	30
CONST_INT LARGE_MAX_NUMBER_OF_TEXT_PLACEMENTS		26
CONST_INT LARGE_MAX_NUMBER_OF_RECTS					20

////////HUD DISPLAYS
CONST_INT SMALL_MAX_NUMBER_OF_SPRITE_PLACEMENTS 	10
CONST_INT SMALL_MAX_NUMBER_OF_TEXT_PLACEMENTS		20
CONST_INT SMALL_MAX_NUMBER_OF_RECTS					10

////////HUD DISPLAYS
CONST_INT MEGA_MAX_NUMBER_OF_SPRITE_PLACEMENTS 	35
CONST_INT MEGA_MAX_NUMBER_OF_TEXT_PLACEMENTS	50
CONST_INT MEGA_MAX_NUMBER_OF_RECTS				40


//TAB DISPLAY
CONST_INT MAX_NUMBER_OF_TABS 5







STRUCT SCALEFORM_INGAME_UNLOCK
	BOOL bInitialisedVisuals
	
	BOOL bHasValue
	
	STRING Title
	STRING Description
	STRING TextureDictionary
	STRING TextureName
	
	BOOL DoesTextHaveSingleNumber
	INT DescriptionNumber
	
	BOOL DoesTextHaveSingleName
	STRING DescriptionName
	
	ABILITY_ICON AnAbilityIcon
	BOOL bRefreshScaleformVisuals
	BOOL bisatshirt
ENDSTRUCT

STRUCT SCALEFORM_INGAME_AWARD
	BOOL bInitialisedVisuals
	
	BOOL bHasValue
	
	STRING Title
	STRING Description
	STRING TextureDictionary
	STRING TextureName
	STRING XPaward
	BOOL DoesTextHaveSingleNumber
	INT DescriptionNumber
	
	BOOL DoesTextHaveSingleName
	STRING DescriptionName
	
	ABILITY_ICON AnAbilityIcon

	BOOL bRefreshScaleformVisuals
	BOOL bisatshirt
ENDSTRUCT


STRUCT TEXTSTYLES
	TEXT_STYLE TS_TITLE			
	TEXT_STYLE TS_TITLESMALL
//	TEXT_STYLE TS_STANDARDTINY
	TEXT_STYLE TS_STANDARDSMALL
	TEXT_STYLE TS_STANDARDMEDIUM
	TEXT_STYLE TS_LBD
	TEXT_STYLE TS_LBDPLYR
//	TEXT_STYLE TS_STANDARDBIG	//Can go when new hud is in
//	TEXT_STYLE TS_STANDARDHUGE //Can go when new hud is in
//	TEXT_STYLE TS_EXTRA1	//Can go when new hud is in
	
	TEXT_STYLE TS_TITLEHUD
	TEXT_STYLE TS_HUDNUMBER
	
	TEXT_STYLE TS_UI_TIMERNUMBER_THREESET
	TEXT_STYLE TS_UI_TIMERNUMBER_TWOSET
	TEXT_STYLE TS_UI_DASHSINGLE
	TEXT_STYLE TS_UI_DASHDOUBLE
	TEXT_STYLE TS_UI_POSITION_SYMBOL
	
	TEXT_STYLE TS_STANDARDMEDIUMHUD
	TEXT_STYLE TS_STANDARDSMALLHUD
	
	// Specific text style instances for Minigame Menus -TAYLOR
	TEXT_STYLE TS_MINIGAME_MENU_TITLE
	TEXT_STYLE TS_MINIGAME_MENU_PRIMARY
	TEXT_STYLE TS_MINIGAME_MENU_SECONDARY
	TEXT_STYLE TS_MINIGAME_MENU_TERTIARY
	TEXT_STYLE TS_MINIGAME_MENU_NOTIFICATION
	TEXT_STYLE TS_MINIGAME_MENU_GAMERTAG
ENDSTRUCT

STRUCT HUDCOGS
	INT iHudState
	INT iHudCurrentScreen
	INT iSelectedCharacter = 0
	INT iSelection = 1
	INT iSelectedGang = 0
	INT iSelectedPage = 1
	INT iNoOfSelectedPageOptions = 1
	INT iNoOfSelectionOptions = 0
	BOOL bTurnOnInfo
	INT iHUDScreenTimer
	INT DisplayWhichSubScreen = 1
	INT DisplayWhichOldSubScreen = 1
	
ENDSTRUCT




STRUCT PLACEMENT_TOOLS
	SPRITE_PLACEMENT SpritePlacement[MAX_NUMBER_OF_SPRITE_PLACEMENTS]
	TEXT_PLACEMENT TextPlacement[MAX_NUMBER_OF_TEXT_PLACEMENTS]
	RECT RectPlacement[MAX_NUMBER_OF_RECTS]
	#IF COMPILE_WIDGET_OUTPUT
		STRING txtFileName
		STRING txtFilePath
		STRING txtScreenName
		TEXT_WIDGET_ID	widScreenName
		TEXT_WIDGET_ID	widFileName
		TEXT_WIDGET_ID	widFilePath
		BOOL bOutputAllWidgets
		BOOL bOutputEnums
	#ENDIF
ENDSTRUCT

STRUCT LARGE_PLACEMENT_TOOLS
	SPRITE_PLACEMENT SpritePlacement[LARGE_MAX_NUMBER_OF_SPRITE_PLACEMENTS]
	TEXT_PLACEMENT TextPlacement[LARGE_MAX_NUMBER_OF_TEXT_PLACEMENTS]
	TEXT_PLACEMENT MainTitle
	SCALEFORM_INDEX ButtonMovie
	RECT RectPlacement[LARGE_MAX_NUMBER_OF_RECTS]
	TEXTSTYLES aStyle
	BOOL HasButtonPressed
	HUDCOGS ScreenPlace
	INT iHudButtonPressed
	BOOL bHudScreenInitialised
	PED_INDEX SelectionPed[2]
	PED_INDEX IdlePed[5]
	BOOL ScaleFormBool
	BOOL SecondScaleformBool
	BOOL ThirdScaleformBool
	#IF COMPILE_WIDGET_OUTPUT
		STRING txtFileName
		STRING txtFilePath
		STRING txtScreenName
		TEXT_WIDGET_ID	widScreenName
		TEXT_WIDGET_ID	widFileName
		TEXT_WIDGET_ID	widFilePath
		BOOL bOutputAllWidgets
		BOOL bOutputEnums
	#ENDIF
ENDSTRUCT





//STRUCT SMALL_PLACEMENT_TOOLS
//	SPRITE_PLACEMENT SpritePlacement[SMALL_MAX_NUMBER_OF_SPRITE_PLACEMENTS]
//	TEXT_PLACEMENT TextPlacement[SMALL_MAX_NUMBER_OF_TEXT_PLACEMENTS]
//	RECT RectPlacement[SMALL_MAX_NUMBER_OF_RECTS]
//	TEXT_PLACEMENT MainTitle
//	SCALEFORM_INDEX ButtonMovie
//	TEXTSTYLES aStyle
//	HUDCOGS ScreenPlace
//	INT iHudButtonPressed
//	BOOL bHudScreenInitialised
//	#IF COMPILE_WIDGET_OUTPUT
//		STRING txtFileName
//		STRING txtFilePath
//		STRING txtScreenName
//		TEXT_WIDGET_ID	widScreenName
//		TEXT_WIDGET_ID	widFileName
//		TEXT_WIDGET_ID	widFilePath
//		BOOL bOutputAllWidgets
//		BOOL bOutputEnums
//	#ENDIF
//ENDSTRUCT

STRUCT MEGA_PLACEMENT_TOOLS
	SPRITE_PLACEMENT SpritePlacement[MEGA_MAX_NUMBER_OF_SPRITE_PLACEMENTS]
	TEXT_PLACEMENT TextPlacement[MEGA_MAX_NUMBER_OF_TEXT_PLACEMENTS]
	TEXT_PLACEMENT MainTitle
	SCALEFORM_INDEX ButtonMovie
	RECT RectPlacement[MEGA_MAX_NUMBER_OF_RECTS]
	TEXTSTYLES aStyle
	BOOL HasButtonPressed
	HUDCOGS ScreenPlace
	INT iHudButtonPressed
	BOOL bHudScreenInitialised
	#IF COMPILE_WIDGET_OUTPUT
		STRING txtFileName
		STRING txtFilePath
		STRING txtScreenName
		TEXT_WIDGET_ID	widScreenName
		TEXT_WIDGET_ID	widFileName
		TEXT_WIDGET_ID	widFilePath
		BOOL bOutputAllWidgets
		BOOL bOutputEnums
		BOOL bOutputPixelValues
	#ENDIF
ENDSTRUCT
STRUCT TAB_TEXTSTYLES
	TEXT_STYLE TS_Tab
	TEXT_STYLE TS_TabSelected
ENDSTRUCT

STRUCT TAB_FIXEDVALUES
	FLOAT fTabsCentreX = 0.499
	FLOAT fTabsCentreY = 0.206
	FLOAT fTabsTotalWidth = 0.598
	FLOAT fTabsTotalHeight = 0.217
	FLOAT fTabIndentation = 0.00
	FLOAT fTabSeperation = 0.004
	FLOAT fTabHeight = 0.018
	FLOAT fTabGangHeight = 0.007
	FLOAT fTabTitleIndentFromTop = 0.001
	FLOAT fTabTitleIndentFromTopSelected = -0.003
	INT iTabAlpha = 220 // 110
	FLOAT TabDivingValue = 4.940
	BOOL bTabYFixed
	BOOL bUseEvenSpacing = FALSE
	BOOL bDullNonSelectedTabs = TRUE
ENDSTRUCT

STRUCT TAB_PLACEMENT_TOOLS
	TEXT_LABEL_63 TabString[MAX_NUMBER_OF_TABS]
	RECT TabGangColour[MAX_NUMBER_OF_TABS]
	RECT Tab[MAX_NUMBER_OF_TABS]
	TEXT_PLACEMENT TabTitle[MAX_NUMBER_OF_TABS]
	SPRITE_PLACEMENT TabLock[MAX_NUMBER_OF_TABS]
	TAB_TEXTSTYLES TabStyles
	
	INT iNumberOfTabs = 4 // default value
// only set these if necessry, otherwise just use the default values
	INT LockedTabsBitState
	INT iSelectedTab
	INT iTabButtonPressed
	TAB_FIXEDVALUES FixedValues
ENDSTRUCT

PROC UNLOAD_ALL_HUD_TEXTURES()
	
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPEntry")  
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPGangSelect")  
//	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPOverview")  

ENDPROC


PROC UPDATE_SPRITE_VALUE(SPRITE_PLACEMENT& aMainSprite, SPRITE_PLACEMENT& aOffsetSprite)
	aMainSprite.x += aOffsetSprite.x
	aMainSprite.y += aOffsetSprite.y
	aMainSprite.w += aOffsetSprite.w
	aMainSprite.h += aOffsetSprite.h
	aMainSprite.r += aOffsetSprite.r
	aMainSprite.g += aOffsetSprite.g
	aMainSprite.b += aOffsetSprite.b
	aMainSprite.a += aOffsetSprite.a
ENDPROC

PROC SET_TEXT_PLACEMENT(TEXT_PLACEMENT& aText, FLOAT XPos, FLOAT YPos, FLOAT YOffset = 0.0)
	aText.x = XPos
	aText.y = YPos+YOffset
ENDPROC

PROC SET_RECT_PLACEMENT(RECT& aRect, FLOAT XPos, FLOAT YPos, FLOAT Width, FLOAT Height, INT R, INT G, INT B, INT A, FLOAT YOffset = 0.0)
	aRect.x = XPos
	aRect.y = YPos+YOffset
	aRect.w = Width
	aRect.h = Height
	aRect.r = R
	aRect.g = G
	aRect.b = B
	aRect.a = A
ENDPROC

PROC SET_SPRITE_PLACEMENT(SPRITE_PLACEMENT& aSprite, FLOAT XPos, FLOAT YPos, FLOAT Width, FLOAT Height, INT R, INT G, INT B, INT A, FLOAT YOffset = 0.0)
	aSprite.x = XPos
	aSprite.y = YPos+YOffset
	aSprite.w = Width
	aSprite.h = Height
	aSprite.r = R
	aSprite.g = G
	aSprite.b = B
	aSprite.a = A
ENDPROC

PROC UPDATE_TEXT_PLACEMENT_WIDGET_VALUE(TEXT_PLACEMENT& aMainText, TEXT_PLACEMENT& aWidgetText)
	aMainText.x += aWidgetText.x
	aMainText.y += aWidgetText.y
ENDPROC

PROC UPDATE_RECT_WIDGET_VALUE(RECT& aMainRect, RECT& aWidgetRect)
	aMainRect.x += aWidgetRect.x
	aMainRect.y += aWidgetRect.y
	aMainRect.w += aWidgetRect.w
	aMainRect.h += aWidgetRect.h
	aMainRect.r += aWidgetRect.r
	aMainRect.g += aWidgetRect.g
	aMainRect.b += aWidgetRect.b
	aMainRect.a += aWidgetRect.a
ENDPROC

PROC UPDATE_SPRITE_WIDGET_VALUE(SPRITE_PLACEMENT& aMainRect, SPRITE_PLACEMENT& aWidgetRect)
	aMainRect.x += aWidgetRect.x
	aMainRect.y += aWidgetRect.y
	aMainRect.w += aWidgetRect.w
	aMainRect.h += aWidgetRect.h
	aMainRect.r += aWidgetRect.r
	aMainRect.g += aWidgetRect.g
	aMainRect.b += aWidgetRect.b
	aMainRect.a += aWidgetRect.a
ENDPROC

PROC UPDATE_TEXT_STYLE_WIDGET_VALUE(TEXT_STYLE& aStyle, TEXT_STYLE& aWidgetStyle)

	aStyle.XScale += aWidgetStyle.XScale
	aStyle.YScale += aWidgetStyle.YScale
	aStyle.r += aWidgetStyle.r
	aStyle.g += aWidgetStyle.g
	aStyle.b += aWidgetStyle.b
	aStyle.a += aWidgetStyle.a
	aStyle.WrapStartX += aWidgetStyle.WrapStartX
	aStyle.WrapEndX += aWidgetStyle.WrapEndX

ENDPROC


PROC SET_STANDARD_TITLE_TEXT(TEXT_STYLE& aTextStyle, DROPSTYLE WhichStyle = DROPSTYLE_NONE)
	
	aTextStyle.aFont = FONT_STANDARD
	aTextStyle.XScale = 0.3320
	aTextStyle.YScale = 1.045
	aTextStyle.r = 0
	aTextStyle.g = 0
	aTextStyle.b = 0
	aTextStyle.a = 255
	aTextStyle.drop = WhichStyle
	aTextStyle.wrapEndX = 0
	aTextStyle.wrapStartX = 0

ENDPROC

PROC SET_TITLE_TEXT(TEXT_STYLE& aTextStyle, DROPSTYLE WhichStyle = DROPSTYLE_NONE)
	aTextStyle.aFont = FONT_STANDARD
	aTextStyle.XScale = 0.202 
	aTextStyle.YScale = 0.781
	aTextStyle.r = 255
	aTextStyle.g = 255
	aTextStyle.b = 255
	aTextStyle.a = 255
	aTextStyle.drop = WhichStyle
	aTextStyle.wrapStartX = 0
	aTextStyle.wrapEndX = 0
	aTextStyle.aTextType = TEXTTYPE_TS_TITLE
ENDPROC

PROC SET_TITLE_SMALL_TEXT(TEXT_STYLE& aTextStyle, DROPSTYLE WhichStyle = DROPSTYLE_NONE)
	aTextStyle.aFont = FONT_STANDARD
	aTextStyle.XScale = 0.202 
	aTextStyle.YScale = 0.346 //0.276
	aTextStyle.r = 255
	aTextStyle.g = 255
	aTextStyle.b = 255
	aTextStyle.a = 255
	aTextStyle.drop = WhichStyle
	aTextStyle.wrapStartX = 0
	aTextStyle.wrapEndX = 0
	aTextStyle.aTextType = TEXTTYPE_TS_TITLESMALL
ENDPROC

PROC SET_TITLE_HUD_TEXT(TEXT_STYLE& aTextStyle, DROPSTYLE WhichStyle = DROPSTYLE_NONE)
	aTextStyle.aFont = FONT_STANDARD
	aTextStyle.XScale = 0.3320
	aTextStyle.YScale = 0.628
	aTextStyle.r = 255
	aTextStyle.g = 255
	aTextStyle.b = 255
	aTextStyle.a = 255
	aTextStyle.drop = WhichStyle
	aTextStyle.wrapStartX = 0
	aTextStyle.wrapEndX = 0
	aTextStyle.aTextType = TEXTTYPE_TS_TITLEHUD
ENDPROC

PROC SET_STANDARD_HUD_NUMBER_TEXT(TEXT_STYLE& aTextStyle, DROPSTYLE WhichStyle = DROPSTYLE_NONE)
	aTextStyle.aFont = FONT_STANDARD
	aTextStyle.XScale = 0.3320  
	aTextStyle.YScale = 0.655
	aTextStyle.r = 255
	aTextStyle.g = 255
	aTextStyle.b = 255
	aTextStyle.a = 255
	aTextStyle.drop = WhichStyle
	aTextStyle.wrapStartX = 0
	aTextStyle.wrapEndX = 0
	aTextStyle.aTextType = TEXTTYPE_TS_HUDNUMBER
ENDPROC

//PROC SET_STANDARD_HUGE_TEXT(TEXT_STYLE& aTextStyle, BOOL DropShadow = FALSE)
//	aTextStyle.aFont = FONT_STANDARD
//	aTextStyle.XScale = 0.202 
//	aTextStyle.YScale = 2.0 //1.460
//	aTextStyle.r = 255
//	aTextStyle.g = 255
//	aTextStyle.b = 255
//	aTextStyle.a = 255
//	IF DropShadow = FALSE
//		aTextStyle.drop = 0
//	ELSE
//		aTextStyle.drop = 1
//	ENDIF
//	aTextStyle.wrapStartX = 0
//	aTextStyle.wrapEndX = 0
//	aTextStyle.aTextType = TEXTTYPE_TS_STANDARDHUGE
//ENDPROC

//PROC SET_STANDARD_BIG_TEXT(TEXT_STYLE& aTextStyle, BOOL DropShadow = FALSE)
//	aTextStyle.aFont = FONT_STANDARD
//	aTextStyle.XScale = 0.202 
//	aTextStyle.YScale = 0.9
//	aTextStyle.r = 255
//	aTextStyle.g = 255
//	aTextStyle.b = 255
//	aTextStyle.a = 255
//	IF DropShadow = FALSE
//		aTextStyle.drop = 0
//	ELSE
//		aTextStyle.drop = 1
//	ENDIF
//	aTextStyle.wrapStartX = 0
//	aTextStyle.wrapEndX = 0
//	aTextStyle.aTextType = TEXTTYPE_TS_STANDARDBIG
//ENDPROC

//TWEAK_FLOAT tfTITLE_SCALE_1 0.288 // 0.800
//TWEAK_FLOAT tfTITLE_SCALE_2 0.350 // 0.450
//TWEAK_FLOAT tfTITLE_SCALE_3 0.628

// 1846388
PROC SET_LBD_TOP_RIGHT_FONT_AND_SIZE(TEXT_STYLE& aTextStyle, DROPSTYLE WhichStyle = DROPSTYLE_NONE)
	aTextStyle.aFont = FONT_CONDENSED_NOT_GAMERNAME
	aTextStyle.XScale = 0.202 
	aTextStyle.YScale = 0.800
	aTextStyle.r = 255
	aTextStyle.g = 255
	aTextStyle.b = 255
	aTextStyle.a = 255
	aTextStyle.drop = WhichStyle
	aTextStyle.wrapStartX = 0
	aTextStyle.wrapEndX = 0
	aTextStyle.aTextType = TEXTTYPE_TS_STANDARDSMALLHUD
ENDPROC

PROC SET_LB_COLUMN_TITLES_FONT_AND_SIZE(TEXT_STYLE& aTextStyle, DROPSTYLE WhichStyle = DROPSTYLE_NONE)
	aTextStyle.aFont = FONT_STANDARD
	aTextStyle.XScale = 0.202 
	aTextStyle.YScale = 0.450
	aTextStyle.r = 255
	aTextStyle.g = 255
	aTextStyle.b = 255
	aTextStyle.a = 255
	aTextStyle.drop = WhichStyle
	aTextStyle.wrapStartX = 0
	aTextStyle.wrapEndX = 0
	aTextStyle.aTextType = TEXTTYPE_TS_LBD
ENDPROC

PROC SET_LBD_MAIN_TITLE_FONT_AND_SIZE(TEXT_STYLE& aTextStyle, DROPSTYLE WhichStyle = DROPSTYLE_NONE)
	aTextStyle.aFont = FONT_CONDENSED_NOT_GAMERNAME
	aTextStyle.XScale = 0.332
	aTextStyle.YScale = 0.800
	aTextStyle.r = 255
	aTextStyle.g = 255
	aTextStyle.b = 255
	aTextStyle.a = 255
	aTextStyle.drop = WhichStyle
	aTextStyle.wrapStartX = 0
	aTextStyle.wrapEndX = 0
	aTextStyle.aTextType = TEXTTYPE_TS_TITLEHUD
ENDPROC
//

PROC SET_STANDARD_MEDIUM_TEXT(TEXT_STYLE& aTextStyle, DROPSTYLE WhichStyle = DROPSTYLE_NONE)
	aTextStyle.aFont = FONT_STANDARD
	aTextStyle.XScale = 0.202 
	aTextStyle.YScale = 0.409
	aTextStyle.r = 255
	aTextStyle.g = 255
	aTextStyle.b = 255
	aTextStyle.a = 255
	aTextStyle.drop = WhichStyle
	aTextStyle.wrapStartX = 0
	aTextStyle.wrapEndX = 0
	aTextStyle.aTextType = TEXTTYPE_TS_STANDARDMEDIUM
ENDPROC

PROC SET_STANDARD_LBD_TEXT(TEXT_STYLE& aTextStyle, DROPSTYLE WhichStyle = DROPSTYLE_NONE)
	aTextStyle.aFont = FONT_STANDARD
	aTextStyle.XScale = 0.202 
	aTextStyle.YScale = 0.350
	aTextStyle.r = 255
	aTextStyle.g = 255
	aTextStyle.b = 255
	aTextStyle.a = 255
	aTextStyle.drop = WhichStyle
	aTextStyle.wrapStartX = 0
	aTextStyle.wrapEndX = 0
	aTextStyle.aTextType = TEXTTYPE_TS_LBD
ENDPROC

//TWEAK_FLOAT ySIZE_PLAYERS 0.350

PROC SET_STANDARD_LBD_PLYR_NAME_TEXT(TEXT_STYLE& aTextStyle, DROPSTYLE WhichStyle = DROPSTYLE_NONE)
	aTextStyle.aFont = FONT_CONDENSED
	aTextStyle.XScale = 0.202 
	aTextStyle.YScale = 0.485
	aTextStyle.r = 255
	aTextStyle.g = 255
	aTextStyle.b = 255
	aTextStyle.a = 255
	aTextStyle.drop = WhichStyle
	aTextStyle.wrapStartX = 0
	aTextStyle.wrapEndX = 0
	aTextStyle.aTextType = TEXTTYPE_TS_LBD_PLYR_NAME
ENDPROC

PROC SET_STANDARD_TIMER_PLYR_NAME_TEXT(TEXT_STYLE& aTextStyle, DROPSTYLE WhichStyle = DROPSTYLE_NONE)
	aTextStyle.aFont = FONT_CONDENSED
	aTextStyle.XScale = 0.202
	aTextStyle.YScale = 0.355+0.092
	aTextStyle.r = 255
	aTextStyle.g = 255
	aTextStyle.b = 255
	aTextStyle.a = 255
	aTextStyle.drop = WhichStyle
	aTextStyle.wrapStartX = 0
	aTextStyle.wrapEndX = 0
	aTextStyle.aTextType = TEXTTYPE_TS_TIMER_PLYR_NAME
ENDPROC

PROC SET_STANDARD_MEDIUM_HUD_TEXT(TEXT_STYLE& aTextStyle, DROPSTYLE WhichStyle = DROPSTYLE_NONE)
	aTextStyle.aFont = FONT_STANDARD
	aTextStyle.XScale = 0.202 
	aTextStyle.YScale = 0.368
	aTextStyle.r = 255
	aTextStyle.g = 255
	aTextStyle.b = 255
	aTextStyle.a = 255
	aTextStyle.drop = WhichStyle
	aTextStyle.wrapStartX = 0
	aTextStyle.wrapEndX = 0
	aTextStyle.aTextType = TEXTTYPE_TS_STANDARDMEDIUMHUD
ENDPROC

PROC SET_STANDARD_SMALL_TEXT(TEXT_STYLE& aTextStyle, DROPSTYLE WhichStyle = DROPSTYLE_NONE)
	aTextStyle.aFont = FONT_STANDARD
	aTextStyle.XScale = 0.202 
	aTextStyle.YScale = 0.383
	aTextStyle.r = 255
	aTextStyle.g = 255
	aTextStyle.b = 255
	aTextStyle.a = 255
	aTextStyle.drop = WhichStyle
	aTextStyle.wrapStartX = 0
	aTextStyle.wrapEndX = 0
	aTextStyle.aTextType = TEXTTYPE_TS_STANDARDSMALL
ENDPROC


//PROC SET_STANDARD_TINY_TEXT(TEXT_STYLE& aTextStyle)
//	aTextStyle.aFont = FONT_STANDARD
//	aTextStyle.XScale = 0.202 
//	aTextStyle.YScale = 0.276
//	aTextStyle.r = 255
//	aTextStyle.g = 255
//	aTextStyle.b = 255
//	aTextStyle.a = 255
//	aTextStyle.drop = 0
//	aTextStyle.wrapStartX = 0
//	aTextStyle.wrapEndX = 0
//	aTextStyle.aTextType = TEXTTYPE_TS_STANDARDTINY
//ENDPROC

PROC SET_STANDARD_LARGE_RACE_HUD_TEXT(TEXT_STYLE& aTextStyle, DROPSTYLE WhichStyle = DROPSTYLE_NONE, INT iR = 255, INT iG = 255, INT iB = 255)
	aTextStyle.aFont = FONT_CONDENSED_NOT_GAMERNAME
	aTextStyle.XScale = 3.0
	aTextStyle.YScale = 3.7
	aTextStyle.r = iR
	aTextStyle.g = iG
	aTextStyle.b = iB
	aTextStyle.a = 255
	aTextStyle.drop = WhichStyle
	aTextStyle.wrapStartX = 0
	aTextStyle.wrapEndX = 0
	aTextStyle.aTextType = TEXTTYPE_TS_UI_SINGLE_NUMBER  //TEXTTYPE_TS_HUDNUMBER
ENDPROC

PROC SET_STANDARD_SMALL_HUD_TEXT(TEXT_STYLE& aTextStyle, DROPSTYLE WhichStyle = DROPSTYLE_NONE)
	aTextStyle.aFont = FONT_STANDARD
	aTextStyle.XScale = 0.202 
	aTextStyle.YScale = 0.288
	aTextStyle.r = 255
	aTextStyle.g = 255
	aTextStyle.b = 255
	aTextStyle.a = 255
	aTextStyle.drop = WhichStyle
	aTextStyle.wrapStartX = 0
	aTextStyle.wrapEndX = 0
	aTextStyle.aTextType = TEXTTYPE_TS_STANDARDSMALLHUD
ENDPROC

PROC SET_STANDARD_SMALL_HUD_TEXT_NON_ROMANIC(TEXT_STYLE& aTextStyle, DROPSTYLE WhichStyle = DROPSTYLE_NONE)
	aTextStyle.aFont = FONT_STANDARD
	aTextStyle.XScale = 0.202 
	aTextStyle.YScale = 0.416+0.089
	aTextStyle.r = 255
	aTextStyle.g = 255
	aTextStyle.b = 255
	aTextStyle.a = 255
	aTextStyle.drop = WhichStyle
	aTextStyle.wrapStartX = 0
	aTextStyle.wrapEndX = 0
	aTextStyle.aTextType = TEXTTYPE_TS_STANDARDSMALLHUD
ENDPROC

PROC SET_STANDARD_UI_TIMERNUMBER_THREESET(TEXT_STYLE& aTextStyle, DROPSTYLE WhichStyle = DROPSTYLE_NONE, TEXT_FONTS aFont = FONT_STANDARD)
	aTextStyle.aFont = aFont
	aTextStyle.XScale = 0.3320  
	aTextStyle.YScale = 0.469+0.096-0.017+0.022-0.062-0.001-0.013
	aTextStyle.r = 255
	aTextStyle.g = 255
	aTextStyle.b = 255
	aTextStyle.a = 255
	aTextStyle.drop = WhichStyle
	aTextStyle.wrapStartX = 0
	aTextStyle.wrapEndX = 0
	aTextStyle.aTextType = TEXTTYPE_TS_UI_TIMERNUMBER_THREESETS
ENDPROC

PROC SET_STANDARD_UI_TIMERNUMBER_TWOSET(TEXT_STYLE& aTextStyle, DROPSTYLE WhichStyle = DROPSTYLE_NONE, TEXT_FONTS aFont = FONT_STYLE_FIXED_WIDTH_NUMBERS)
	aTextStyle.aFont = aFont
	aTextStyle.XScale = 0.3320  
	aTextStyle.YScale = 0.889-0.010
	aTextStyle.r = 255
	aTextStyle.g = 255
	aTextStyle.b = 255
	aTextStyle.a = 255
	aTextStyle.drop = WhichStyle
	aTextStyle.wrapStartX = 0
	aTextStyle.wrapEndX = 0
	aTextStyle.aTextType = TEXTTYPE_TS_UI_TIMERNUMBER_TWOSETS
ENDPROC

PROC SET_STANDARD_UI_SCORE_NUMBER(TEXT_STYLE& aTextStyle, DROPSTYLE WhichStyle = DROPSTYLE_NONE, TEXT_FONTS aFont = FONT_STANDARD)
	aTextStyle.aFont = aFont
	aTextStyle.XScale = 0.3320  
	aTextStyle.YScale = 0.469+0.096-0.017+0.022-0.062
	aTextStyle.r = 255
	aTextStyle.g = 255
	aTextStyle.b = 255
	aTextStyle.a = 255
	aTextStyle.drop = WhichStyle
	aTextStyle.wrapStartX = 0
	aTextStyle.wrapEndX = 0
	aTextStyle.aTextType = TEXTTYPE_TS_UI_SCORE
ENDPROC

PROC SET_STANDARD_UI_SCORE_SMALL_NUMBER(TEXT_STYLE& aTextStyle, DROPSTYLE WhichStyle = DROPSTYLE_NONE, TEXT_FONTS aFont = FONT_STANDARD)
	aTextStyle.aFont = aFont
	aTextStyle.XScale = 0.3320  
	aTextStyle.YScale = 0.469+0.096-0.017+0.022-0.062-0.095-0.008-0.012
	aTextStyle.r = 255
	aTextStyle.g = 255
	aTextStyle.b = 255
	aTextStyle.a = 255
	aTextStyle.drop = WhichStyle
	aTextStyle.wrapStartX = 0
	aTextStyle.wrapEndX = 0
	aTextStyle.aTextType = TEXTTYPE_TS_UI_SCORE_SMALL
ENDPROC

PROC SET_STANDARD_UI_SCORE_SMALL_NUMBER_NON_ROMANIC(TEXT_STYLE& aTextStyle, DROPSTYLE WhichStyle = DROPSTYLE_NONE, TEXT_FONTS aFont = FONT_STANDARD)
	aTextStyle.aFont = aFont
	aTextStyle.XScale = 0.3320  
	aTextStyle.YScale = 0.469+0.096-0.017+0.022-0.062-0.095-0.008-0.012-0.060
	aTextStyle.r = 255
	aTextStyle.g = 255
	aTextStyle.b = 255
	aTextStyle.a = 255
	aTextStyle.drop = WhichStyle
	aTextStyle.wrapStartX = 0
	aTextStyle.wrapEndX = 0
	aTextStyle.aTextType = TEXTTYPE_TS_UI_SCORE_SMALL_NON_ROMANIC
ENDPROC

PROC SET_STANDARD_UI_SINGLE_NUMBER(TEXT_STYLE& aTextStyle, DROPSTYLE WhichStyle = DROPSTYLE_NONE)
	aTextStyle.aFont = FONT_STANDARD
	aTextStyle.XScale = 0.3320
	aTextStyle.YScale = 1-0.115 //1.200+0.066
	aTextStyle.r = 255
	aTextStyle.g = 255
	aTextStyle.b = 255
	aTextStyle.a = 255
	aTextStyle.drop = WhichStyle
	aTextStyle.wrapStartX = 0
	aTextStyle.wrapEndX = 0
	aTextStyle.aTextType = TEXTTYPE_TS_UI_SINGLE_NUMBER
ENDPROC

PROC SET_STANDARD_UI_POSITION_SYMBOL(TEXT_STYLE& aTextStyle, DROPSTYLE WhichStyle = DROPSTYLE_NONE)
	aTextStyle.aFont = FONT_STANDARD
	aTextStyle.XScale = 0.3320
	aTextStyle.YScale = 0.315+0.183
	aTextStyle.r = 255
	aTextStyle.g = 255
	aTextStyle.b = 255
	aTextStyle.a = 255
	aTextStyle.drop = WhichStyle
	aTextStyle.wrapStartX = 0
	aTextStyle.wrapEndX = 0
	aTextStyle.aTextType = TEXTTYPE_TS_UI_POSITION_SYMBOL
ENDPROC

PROC SET_STANDARD_UI_POSITION_SINGLE_SYMBOL(TEXT_STYLE& aTextStyle, DROPSTYLE WhichStyle = DROPSTYLE_NONE)
	aTextStyle.aFont = FONT_STANDARD
	aTextStyle.XScale = 0.3320
	aTextStyle.YScale = 0.628-0.180-0.080 //0.315+0.183-0.085
	aTextStyle.r = 255
	aTextStyle.g = 255
	aTextStyle.b = 255
	aTextStyle.a = 255
	aTextStyle.drop = WhichStyle
	aTextStyle.wrapStartX = 0
	aTextStyle.wrapEndX = 0
	aTextStyle.aTextType = TEXTTYPE_TS_UI_POSITION_SYMBOL
ENDPROC


PROC SET_STANDARD_UI_METER_BIG_TITLE(TEXT_STYLE& aTextStyle, DROPSTYLE WhichStyle = DROPSTYLE_NONE)
	aTextStyle.aFont = FONT_STANDARD
	aTextStyle.XScale = 0.3320
	aTextStyle.YScale = 0.508-0.030
	aTextStyle.r = 255
	aTextStyle.g = 255
	aTextStyle.b = 255
	aTextStyle.a = 255
	aTextStyle.drop = WhichStyle
	aTextStyle.wrapStartX = 0
	aTextStyle.wrapEndX = 0
	aTextStyle.aTextType = TEXTTYPE_TS_UI_METER_BIG_TITLE
ENDPROC


PROC SET_STANDARD_UI_METER_BIG_TITLE_WAVE(TEXT_STYLE& aTextStyle, DROPSTYLE WhichStyle = DROPSTYLE_NONE)
	aTextStyle.aFont = FONT_STANDARD
	aTextStyle.XScale = 0.3320
	aTextStyle.YScale = 0.508-0.030-0.106
	aTextStyle.r = 255
	aTextStyle.g = 255
	aTextStyle.b = 255
	aTextStyle.a = 255
	aTextStyle.drop = WhichStyle
	aTextStyle.wrapStartX = 0
	aTextStyle.wrapEndX = 0
	aTextStyle.aTextType = TEXTTYPE_TS_UI_METER_BIG_TITLE_WAVE
ENDPROC


PROC SET_STANDARD_UI_METER_BIG_TITLE_NON_ROMAN(TEXT_STYLE& aTextStyle, DROPSTYLE WhichStyle = DROPSTYLE_NONE)
	aTextStyle.aFont = FONT_STANDARD
	aTextStyle.XScale = 0.3320
	aTextStyle.YScale = 0.508-0.030+0.086
	aTextStyle.r = 255
	aTextStyle.g = 255
	aTextStyle.b = 255
	aTextStyle.a = 255
	aTextStyle.drop = WhichStyle
	aTextStyle.wrapStartX = 0
	aTextStyle.wrapEndX = 0
	aTextStyle.aTextType = TEXTTYPE_TS_UI_METER_BIG_TITLE
ENDPROC

PROC SET_STANDARD_UI_DASHSINGLE(TEXT_STYLE& aTextStyle, DROPSTYLE WhichStyle = DROPSTYLE_NONE)
	aTextStyle.aFont = FONT_STYLE_FIXED_WIDTH_NUMBERS //FONT_STANDARD
	aTextStyle.XScale = 0.3320
	aTextStyle.YScale = 0.628-0.180-0.058
	aTextStyle.r = 255
	aTextStyle.g = 255
	aTextStyle.b = 255
	aTextStyle.a = 255
	aTextStyle.drop = WhichStyle
	aTextStyle.wrapStartX = 0
	aTextStyle.wrapEndX = 0
	aTextStyle.aTextType = TEXTTYPE_TS_UI_DASHSINGLE
ENDPROC

PROC SET_STANDARD_UI_DASHDOUBLE(TEXT_STYLE& aTextStyle, DROPSTYLE WhichStyle = DROPSTYLE_NONE)
	aTextStyle.aFont = FONT_STANDARD
	aTextStyle.XScale = 0.3320
	aTextStyle.YScale = 0.500+0.004
	aTextStyle.r = 255
	aTextStyle.g = 255
	aTextStyle.b = 255
	aTextStyle.a = 255
	aTextStyle.drop = WhichStyle
	aTextStyle.wrapStartX = 0
	aTextStyle.wrapEndX = 0
	aTextStyle.aTextType = TEXTTYPE_TS_UI_DASHDOUBLE
ENDPROC

PROC SET_STANDARD_UI_DASHTRIPLE(TEXT_STYLE& aTextStyle, DROPSTYLE WhichStyle = DROPSTYLE_NONE)
	aTextStyle.aFont = FONT_STANDARD
	aTextStyle.XScale = 0.3320
	aTextStyle.YScale = 0.500+0.004-0.010
	aTextStyle.r = 255
	aTextStyle.g = 255
	aTextStyle.b = 255
	aTextStyle.a = 255
	aTextStyle.drop = WhichStyle
	aTextStyle.wrapStartX = 0
	aTextStyle.wrapEndX = 0
	aTextStyle.aTextType = TEXTTYPE_TS_UI_DASHTRIPLE
ENDPROC

PROC SET_STANDARD_UI_AMPM(TEXT_STYLE& aTextStyle, DROPSTYLE WhichStyle = DROPSTYLE_NONE)
	aTextStyle.aFont = FONT_STANDARD
	aTextStyle.XScale = 0.3320
	aTextStyle.YScale = 0.500+0.004-0.010-0.110
	aTextStyle.r = 255
	aTextStyle.g = 255
	aTextStyle.b = 255
	aTextStyle.a = 255
	aTextStyle.drop = WhichStyle
	aTextStyle.wrapStartX = 0
	aTextStyle.wrapEndX = 0
	aTextStyle.aTextType = TEXTTYPE_TS_UI_AMPM
ENDPROC

//PROC SET_STANDARD_UI_DASHDOUBLESMALL(TEXT_STYLE& aTextStyle, BOOL DropShadow = FALSE)
//	aTextStyle.aFont = FONT_STANDARD
//	aTextStyle.XScale = 0.3320
//	aTextStyle.YScale = 0.550
//	aTextStyle.r = 255
//	aTextStyle.g = 255
//	aTextStyle.b = 255
//	aTextStyle.a = 255
//	IF DropShadow = FALSE
//		aTextStyle.drop = 0
//	ELSE
//		aTextStyle.drop = 1
//	ENDIF
//	aTextStyle.wrapStartX = 0
//	aTextStyle.wrapEndX = 0
//	aTextStyle.aTextType = TEXTTYPE_TS_UI_DASHDOUBLESMALL
//ENDPROC

PROC SET_MINIGAME_MENU_TITLE(TEXT_STYLE& aTextStyle, DROPSTYLE WhichStyle = DROPSTYLE_NONE)
	aTextStyle.aFont = FONT_STANDARD
	aTextStyle.XScale = 0.6		// Not sure what XScale is used for -TAYLOR
	aTextStyle.YScale = 0.6		// 24px height / default 40px
	aTextStyle.r = 255
	aTextStyle.g = 255
	aTextStyle.b = 255
	aTextStyle.a = 255
	aTextStyle.drop = WhichStyle
	aTextStyle.wrapStartX = 0
	aTextStyle.wrapEndX = 0
	aTextStyle.aTextType = TEXTTYPE_TS_MINIGAME_MENU_TITLE
ENDPROC

PROC SET_MINIGAME_MENU_PRIMARY(TEXT_STYLE& aTextStyle, DROPSTYLE WhichStyle = DROPSTYLE_NONE)
	aTextStyle.aFont = FONT_STANDARD
	aTextStyle.XScale = 0.325
	aTextStyle.YScale = 0.325 	// 13px height / default 40px
	aTextStyle.r = 0
	aTextStyle.g = 0
	aTextStyle.b = 0
	aTextStyle.a = 255
	aTextStyle.drop = WhichStyle
	aTextStyle.wrapStartX = 0
	aTextStyle.wrapEndX = 0
	aTextStyle.aTextType = TEXTTYPE_TS_MINIGAME_MENU_PRIMARY
ENDPROC

PROC SET_MINIGAME_MENU_SECONDARY(TEXT_STYLE& aTextStyle, DROPSTYLE WhichStyle = DROPSTYLE_NONE)
	aTextStyle.aFont = FONT_STANDARD
	aTextStyle.XScale = 0.35
	aTextStyle.YScale = 0.35 	// 14px height / default 40px
	aTextStyle.r = 0
	aTextStyle.g = 0
	aTextStyle.b = 0
	aTextStyle.a = 255
	aTextStyle.drop = WhichStyle
	aTextStyle.wrapStartX = 0
	aTextStyle.wrapEndX = 0
	aTextStyle.aTextType = TEXTTYPE_TS_MINIGAME_MENU_SECONDARY
ENDPROC

PROC SET_MINIGAME_MENU_TERTIARY(TEXT_STYLE& aTextStyle, DROPSTYLE WhichStyle = DROPSTYLE_NONE)
	aTextStyle.aFont = FONT_STANDARD
	aTextStyle.XScale = 0.3375
	aTextStyle.YScale = 0.3375 	// 13.5px height / default 40px
	aTextStyle.r = 0
	aTextStyle.g = 0
	aTextStyle.b = 0
	aTextStyle.a = 255
	aTextStyle.drop = WhichStyle
	aTextStyle.wrapStartX = 0
	aTextStyle.wrapEndX = 0
	aTextStyle.aTextType = TEXTTYPE_TS_MINIGAME_MENU_TERTIARY
ENDPROC

PROC SET_MINIGAME_MENU_NOTIFICATION (TEXT_STYLE& aTextStyle, DROPSTYLE WhichStyle = DROPSTYLE_NONE)
	aTextStyle.aFont = FONT_STANDARD
	aTextStyle.XScale = 0.275
	aTextStyle.YScale = 0.275 	// 11px height / default 40px
	aTextStyle.r = 0
	aTextStyle.g = 0
	aTextStyle.b = 0
	aTextStyle.a = 255
	aTextStyle.drop = WhichStyle
	aTextStyle.wrapStartX = 0
	aTextStyle.wrapEndX = 0
	aTextStyle.aTextType = TEXTTYPE_TS_MINIGAME_MENU_NOTIFICATION
ENDPROC

PROC SET_MINIGAME_MENU_GAMERTAG (TEXT_STYLE& aTextStyle, DROPSTYLE WhichStyle = DROPSTYLE_NONE)
	aTextStyle.aFont = FONT_CONDENSED
	aTextStyle.XScale = 0.45
	aTextStyle.YScale = 0.45 	// 11px height / default 40px
	aTextStyle.r = 0
	aTextStyle.g = 0
	aTextStyle.b = 0
	aTextStyle.a = 255
	aTextStyle.drop = WhichStyle
	aTextStyle.wrapStartX = 0
	aTextStyle.wrapEndX = 0
	aTextStyle.aTextType = TEXTTYPE_TS_MINIGAME_MENU_GAMERTAG
ENDPROC

PROC SET_STANDARD_INGAME_TEXT_DETAILS(TEXTSTYLES& aTextStyle)

	SET_TITLE_TEXT(aTextStyle.TS_TITLE)
	SET_TITLE_SMALL_TEXT(aTextStyle.TS_TITLESMALL)
	SET_TITLE_HUD_TEXT(aTextStyle.TS_TITLEHUD)
	SET_STANDARD_HUD_NUMBER_TEXT(aTextStyle.TS_HUDNUMBER)
//	SET_STANDARD_HUGE_TEXT(aTextStyle.TS_STANDARDHUGE)
//	SET_STANDARD_BIG_TEXT(aTextStyle.TS_STANDARDBIG)
	SET_STANDARD_MEDIUM_TEXT(aTextStyle.TS_STANDARDMEDIUM)
	
	SET_STANDARD_LBD_TEXT(aTextStyle.TS_LBD)
	SET_STANDARD_LBD_PLYR_NAME_TEXT(aTextStyle.TS_LBDPLYR)
	SET_STANDARD_MEDIUM_HUD_TEXT(aTextStyle.TS_STANDARDMEDIUMHUD)
	SET_STANDARD_SMALL_TEXT(aTextStyle.TS_STANDARDSMALL)
//	SET_STANDARD_TINY_TEXT(aTextStyle.TS_STANDARDTINY)
	SET_STANDARD_SMALL_HUD_TEXT(aTextStyle.TS_STANDARDSMALLHUD)
	
	SET_STANDARD_UI_TIMERNUMBER_THREESET(aTextStyle.TS_UI_TIMERNUMBER_THREESET)
	SET_STANDARD_UI_TIMERNUMBER_TWOSET(aTextStyle.TS_UI_TIMERNUMBER_TWOSET)
	SET_STANDARD_UI_DASHSINGLE(aTextStyle.TS_UI_DASHSINGLE)
	SET_STANDARD_UI_DASHDOUBLE(aTextStyle.TS_UI_DASHDOUBLE)
	SET_STANDARD_UI_POSITION_SYMBOL(aTextStyle.TS_UI_POSITION_SYMBOL)

	SET_MINIGAME_MENU_TITLE(aTextStyle.TS_MINIGAME_MENU_TITLE)
	SET_MINIGAME_MENU_PRIMARY(aTextStyle.TS_MINIGAME_MENU_PRIMARY)
	SET_MINIGAME_MENU_SECONDARY(aTextStyle.TS_MINIGAME_MENU_SECONDARY)
	SET_MINIGAME_MENU_TERTIARY(aTextStyle.TS_MINIGAME_MENU_TERTIARY)
	SET_MINIGAME_MENU_NOTIFICATION(aTextStyle.TS_MINIGAME_MENU_NOTIFICATION)
	SET_MINIGAME_MENU_GAMERTAG(aTextStyle.TS_MINIGAME_MENU_GAMERTAG)
ENDPROC

FUNC VECTOR GET_HUD_TITLE_POS()
	RETURN <<0.197, 0.107, 0.0>>
ENDFUNC
PROC SET_MEGA_HUD_TEXT_DETAILS(MEGA_PLACEMENT_TOOLS& Placement)

	SET_STANDARD_INGAME_TEXT_DETAILS(Placement.aStyle)
	VECTOR TempVect = GET_HUD_TITLE_POS()
	Placement.MainTitle.x = TempVect.x
	Placement.MainTitle.y = TempVect.y
ENDPROC
PROC SET_STANDARD_HUD_TEXT_DETAILS(LARGE_PLACEMENT_TOOLS& Placement)

	SET_STANDARD_INGAME_TEXT_DETAILS(Placement.aStyle)
	VECTOR TempVect = GET_HUD_TITLE_POS()
	Placement.MainTitle.x = TempVect.x
	Placement.MainTitle.y = TempVect.y
ENDPROC




//PROC SET_STANDARD_SMALL_HUD_TEXT_DETAILS(SMALL_PLACEMENT_TOOLS& Placement)
//
//	SET_STANDARD_INGAME_TEXT_DETAILS(Placement.aStyle)
//	VECTOR TempVect = GET_HUD_TITLE_POS()
//	Placement.MainTitle.x = TempVect.x
//	Placement.MainTitle.y = TempVect.y
//ENDPROC

PROC GET_HUD_TITLE_TEXT_PLACEMENT(TEXT_PLACEMENT& aText)
	VECTOR TempVect = GET_HUD_TITLE_POS()
	aText.x = TempVect.x
	aText.y = TempVect.y
ENDPROC

PROC RESET_TEXT_WRAP(TEXT_STYLE& aTextStyle)
	aTextStyle.WrapStartX = 0.0
	aTextStyle.WrapEndX = 0.0
ENDPROC

PROC SET_GANG_BLURB_WRAP_OVERLAY(TEXT_STYLE& aTextStyle)
	aTextStyle.WrapStartX = 0.0
	aTextStyle.WrapEndX = 0.612
ENDPROC

#IF IS_DEBUG_BUILD
PROC CREATE_TEXT_PLACEMENT_WIDGETS(TEXT_PLACEMENT &TextPlacement[], STRING Name = NULL)
	INT iPlacementIndex
	TEXT_LABEL_63 strWidget
	IF IS_STRING_EMPTY_HUD(Name)
		Name = "Text Placements"
	ENDIF
	START_WIDGET_GROUP(Name)
		REPEAT COUNT_OF(TextPlacement) iPlacementIndex					
			#IF COMPILE_WIDGET_OUTPUT
				IF IS_STRING_EMPTY_HUD(TextPlacement[iPlacementIndex].strName)
					strWidget = ""
					strWidget += iPlacementIndex
					START_WIDGET_GROUP (strWidget)
				ELSE
					START_WIDGET_GROUP (TextPlacement[iPlacementIndex].strName)
				ENDIF
			#ENDIF
			#IF NOT COMPILE_WIDGET_OUTPUT
				strWidget = ""
				strWidget += iPlacementIndex
				START_WIDGET_GROUP(strWidget)	
			#ENDIF
				ADD_WIDGET_FLOAT_SLIDER("x", TextPlacement[iPlacementIndex].x, -1.0, 1.0, 0.001)	
				ADD_WIDGET_FLOAT_SLIDER("y", TextPlacement[iPlacementIndex].y, -1.0, 1.0, 0.001)
				#IF COMPILE_WIDGET_OUTPUT
					IF NOT IS_STRING_EMPTY_HUD(TextPlacement[iPlacementIndex].strName)
						TextPlacement[iPlacementIndex].widTextName = ADD_TEXT_WIDGET("Text Name")
						SET_CONTENTS_OF_TEXT_WIDGET(TextPlacement[iPlacementIndex].widTextName, TextPlacement[iPlacementIndex].strName)
					ENDIF
				#ENDIF
			STOP_WIDGET_GROUP()
		ENDREPEAT
	STOP_WIDGET_GROUP()
ENDPROC

PROC CREATE_SPRITE_PLACEMENT_WIDGETS(SPRITE_PLACEMENT &SpritePlacement[], STRING Name = NULL)
	INT iPlacementIndex
	TEXT_LABEL_63 strWidget
	START_WIDGET_GROUP(Name)
		REPEAT COUNT_OF(SpritePlacement) iPlacementIndex					
			#IF COMPILE_WIDGET_OUTPUT
				IF IS_STRING_EMPTY_HUD(SpritePlacement[iPlacementIndex].txtSpriteName)
					strWidget = ""
					strWidget += iPlacementIndex
					START_WIDGET_GROUP (strWidget)
				ELSE
					START_WIDGET_GROUP (SpritePlacement[iPlacementIndex].txtSpriteName)
				ENDIF
			#ENDIF
			#IF NOT COMPILE_WIDGET_OUTPUT
				strWidget = ""
				strWidget += iPlacementIndex
				START_WIDGET_GROUP(strWidget)
			#ENDIF
				ADD_WIDGET_FLOAT_SLIDER("x", SpritePlacement[iPlacementIndex].x, -2.0, 2.0, 0.001)
				ADD_WIDGET_FLOAT_SLIDER("y", SpritePlacement[iPlacementIndex].y, -2.0, 2.0, 0.001)
				ADD_WIDGET_FLOAT_SLIDER("w", SpritePlacement[iPlacementIndex].w, -2.0, 2.0, 0.001)
				ADD_WIDGET_FLOAT_SLIDER("h", SpritePlacement[iPlacementIndex].h, -2.0, 2.0, 0.001)
				ADD_WIDGET_INT_SLIDER("r", SpritePlacement[iPlacementIndex].r, 0, 255, 1)
				ADD_WIDGET_INT_SLIDER("g", SpritePlacement[iPlacementIndex].g, 0, 255, 1)
				ADD_WIDGET_INT_SLIDER("b", SpritePlacement[iPlacementIndex].b, 0, 255, 1)
				ADD_WIDGET_INT_SLIDER("a", SpritePlacement[iPlacementIndex].a, 0, 255, 1)
				ADD_WIDGET_FLOAT_SLIDER("rotation", SpritePlacement[iPlacementIndex].fRotation, -360, 360, 0.001)
				#IF COMPILE_WIDGET_OUTPUT
					IF NOT IS_STRING_EMPTY_HUD(SpritePlacement[iPlacementIndex].txtSpriteName)
						SpritePlacement[iPlacementIndex].widSpriteName = ADD_TEXT_WIDGET("Sprite Name")
						SET_CONTENTS_OF_TEXT_WIDGET(SpritePlacement[iPlacementIndex].widSpriteName, SpritePlacement[iPlacementIndex].txtSpriteName)
					ENDIF
				#ENDIF
			STOP_WIDGET_GROUP()
		ENDREPEAT
	STOP_WIDGET_GROUP()
ENDPROC

PROC CREATE_RECT_PLACEMENT_WIDGETS(RECT &RectPlacement[])
	INT iPlacementIndex
	TEXT_LABEL_63 strWidget
	START_WIDGET_GROUP("Rect Placements")
		REPEAT COUNT_OF(RectPlacement) iPlacementIndex				
			#IF COMPILE_WIDGET_OUTPUT
				IF IS_STRING_EMPTY_HUD(RectPlacement[iPlacementIndex].txtRectName)
					strWidget = ""
					strWidget += iPlacementIndex
					START_WIDGET_GROUP (strWidget)
				ELSE
					START_WIDGET_GROUP (RectPlacement[iPlacementIndex].txtRectName)
				ENDIF
			#ENDIF
			#IF NOT COMPILE_WIDGET_OUTPUT
				strWidget = ""
				strWidget += iPlacementIndex
				START_WIDGET_GROUP(strWidget)
			#ENDIF
				ADD_WIDGET_FLOAT_SLIDER("x", RectPlacement[iPlacementIndex].x, -2.0, 2.0, 0.001)
				ADD_WIDGET_FLOAT_SLIDER("y", RectPlacement[iPlacementIndex].y, -2.0, 2.0, 0.001)
				ADD_WIDGET_FLOAT_SLIDER("w", RectPlacement[iPlacementIndex].w, -2.0, 2.0, 0.001)
				ADD_WIDGET_FLOAT_SLIDER("h", RectPlacement[iPlacementIndex].h, -2.0, 2.0, 0.001)
				ADD_WIDGET_INT_SLIDER("r", RectPlacement[iPlacementIndex].r, 0, 255, 1)
				ADD_WIDGET_INT_SLIDER("g", RectPlacement[iPlacementIndex].g, 0, 255, 1)
				ADD_WIDGET_INT_SLIDER("b", RectPlacement[iPlacementIndex].b, 0, 255, 1)
				ADD_WIDGET_INT_SLIDER("a", RectPlacement[iPlacementIndex].a, 0, 255, 1)
				#IF COMPILE_WIDGET_OUTPUT
					IF NOT IS_STRING_EMPTY_HUD(RectPlacement[iPlacementIndex].txtRectName)
						RectPlacement[iPlacementIndex].widRectName = ADD_TEXT_WIDGET("Rect Name")
						SET_CONTENTS_OF_TEXT_WIDGET(RectPlacement[iPlacementIndex].widRectName, RectPlacement[iPlacementIndex].txtRectName)
					ENDIF
				#ENDIF
			STOP_WIDGET_GROUP()
		ENDREPEAT
	STOP_WIDGET_GROUP()
ENDPROC


//#IF IS_DEBUG_BUILD


PROC CREATE_ONLY_PLACEMENT_WIDGETS(PLACEMENT_TOOLS& Placements, WIDGET_GROUP_ID ParentWidget = NULL)

	INT i
	TEXT_LABEL_63 strWidget

	IF ParentWidget <> NULL
		SET_CURRENT_WIDGET_GROUP(ParentWidget)
	ENDIF
	START_WIDGET_GROUP("HUD Placements")
		START_WIDGET_GROUP("Text Placements")
			REPEAT MAX_NUMBER_OF_TEXT_PLACEMENTS i					
				strWidget = ""
				strWidget += i
				START_WIDGET_GROUP(strWidget)	
					ADD_WIDGET_FLOAT_SLIDER("x", Placements.TextPlacement[i].x, -1.0, 1.0, 0.001)	
					ADD_WIDGET_FLOAT_SLIDER("y", Placements.TextPlacement[i].y, -1.0, 1.0, 0.001)
	//				ADD_WIDGET_BOOL("Write To Debug", TextPlacement[i].bOutput)
	//				ADD_WIDGET_STRING(TextPlacement[i].strName)
				STOP_WIDGET_GROUP()
			ENDREPEAT
		STOP_WIDGET_GROUP()

		START_WIDGET_GROUP("Sprite Placements")
			REPEAT MAX_NUMBER_OF_SPRITE_PLACEMENTS i					
				strWidget = ""
				strWidget += i
				START_WIDGET_GROUP(strWidget)
					ADD_WIDGET_FLOAT_SLIDER("x", Placements.SpritePlacement[i].x, -2.0, 2.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("y", Placements.SpritePlacement[i].y, -2.0, 2.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("w", Placements.SpritePlacement[i].w, -2.0, 2.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("h", Placements.SpritePlacement[i].h, -2.0, 2.0, 0.001)
					ADD_WIDGET_INT_SLIDER("r", Placements.SpritePlacement[i].r, 0, 255, 1)
					ADD_WIDGET_INT_SLIDER("g", Placements.SpritePlacement[i].g, 0, 255, 1)
					ADD_WIDGET_INT_SLIDER("b", Placements.SpritePlacement[i].b, 0, 255, 1)
					ADD_WIDGET_INT_SLIDER("a", Placements.SpritePlacement[i].a, 0, 255, 1)
	//				ADD_WIDGET_BOOL("Write To Debug", SpritePlacement[i].bOutput)
	//				ADD_WIDGET_STRING(SpritePlacement[i].strName)
				STOP_WIDGET_GROUP()
			ENDREPEAT
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Rect Placements")
			REPEAT MAX_NUMBER_OF_RECTS i				
				strWidget = ""
				strWidget += i
				START_WIDGET_GROUP(strWidget)
					ADD_WIDGET_FLOAT_SLIDER("x", Placements.RectPlacement[i].x, -2.0, 2.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("y", Placements.RectPlacement[i].y, -2.0, 2.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("w", Placements.RectPlacement[i].w, -2.0, 2.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("h", Placements.RectPlacement[i].h, -2.0, 2.0, 0.001)
					ADD_WIDGET_INT_SLIDER("r", Placements.RectPlacement[i].r, 0, 255, 1)
					ADD_WIDGET_INT_SLIDER("g", Placements.RectPlacement[i].g, 0, 255, 1)
					ADD_WIDGET_INT_SLIDER("b", Placements.RectPlacement[i].b, 0, 255, 1)
					ADD_WIDGET_INT_SLIDER("a", Placements.RectPlacement[i].a, 0, 255, 1)
	//				ADD_WIDGET_BOOL("Write To Debug", RectPlacement[i].bOutput)
	//				ADD_WIDGET_STRING(RectPlacement[i].strName)
				STOP_WIDGET_GROUP()
			ENDREPEAT
		STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP()
	IF ParentWidget <> NULL
		CLEAR_CURRENT_WIDGET_GROUP(ParentWidget)
	ENDIF
ENDPROC


PROC CREATE_ONLY_LARGE_PLACEMENT_WIDGETS(LARGE_PLACEMENT_TOOLS& Placements, WIDGET_GROUP_ID ParentWidget = NULL)

	INT i
	TEXT_LABEL_63 strWidget

	IF ParentWidget <> NULL
		SET_CURRENT_WIDGET_GROUP(ParentWidget)
	ENDIF
	START_WIDGET_GROUP("HUD Placements")
		START_WIDGET_GROUP("Text Placements")
			REPEAT LARGE_MAX_NUMBER_OF_TEXT_PLACEMENTS i					
				strWidget = ""
				strWidget += i
				START_WIDGET_GROUP(strWidget)	
					ADD_WIDGET_FLOAT_SLIDER("x", Placements.TextPlacement[i].x, -1.0, 1.0, 0.001)	
					ADD_WIDGET_FLOAT_SLIDER("y", Placements.TextPlacement[i].y, -1.0, 1.0, 0.001)
	//				ADD_WIDGET_BOOL("Write To Debug", TextPlacement[i].bOutput)
	//				ADD_WIDGET_STRING(TextPlacement[i].strName)
				STOP_WIDGET_GROUP()
			ENDREPEAT
		STOP_WIDGET_GROUP()

		START_WIDGET_GROUP("Sprite Placements")
			REPEAT LARGE_MAX_NUMBER_OF_SPRITE_PLACEMENTS i					
				strWidget = ""
				strWidget += i
				START_WIDGET_GROUP(strWidget)
					ADD_WIDGET_FLOAT_SLIDER("x", Placements.SpritePlacement[i].x, -2.0, 2.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("y", Placements.SpritePlacement[i].y, -2.0, 2.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("w", Placements.SpritePlacement[i].w, -2.0, 2.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("h", Placements.SpritePlacement[i].h, -2.0, 2.0, 0.001)
					ADD_WIDGET_INT_SLIDER("r", Placements.SpritePlacement[i].r, 0, 255, 1)
					ADD_WIDGET_INT_SLIDER("g", Placements.SpritePlacement[i].g, 0, 255, 1)
					ADD_WIDGET_INT_SLIDER("b", Placements.SpritePlacement[i].b, 0, 255, 1)
					ADD_WIDGET_INT_SLIDER("a", Placements.SpritePlacement[i].a, 0, 255, 1)
	//				ADD_WIDGET_BOOL("Write To Debug", SpritePlacement[i].bOutput)
	//				ADD_WIDGET_STRING(SpritePlacement[i].strName)
				STOP_WIDGET_GROUP()
			ENDREPEAT
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Rect Placements")
			REPEAT LARGE_MAX_NUMBER_OF_RECTS i				
				strWidget = ""
				strWidget += i
				START_WIDGET_GROUP(strWidget)
					ADD_WIDGET_FLOAT_SLIDER("x", Placements.RectPlacement[i].x, -2.0, 2.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("y", Placements.RectPlacement[i].y, -2.0, 2.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("w", Placements.RectPlacement[i].w, -2.0, 2.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("h", Placements.RectPlacement[i].h, -2.0, 2.0, 0.001)
					ADD_WIDGET_INT_SLIDER("r", Placements.RectPlacement[i].r, 0, 255, 1)
					ADD_WIDGET_INT_SLIDER("g", Placements.RectPlacement[i].g, 0, 255, 1)
					ADD_WIDGET_INT_SLIDER("b", Placements.RectPlacement[i].b, 0, 255, 1)
					ADD_WIDGET_INT_SLIDER("a", Placements.RectPlacement[i].a, 0, 255, 1)
	//				ADD_WIDGET_BOOL("Write To Debug", RectPlacement[i].bOutput)
	//				ADD_WIDGET_STRING(RectPlacement[i].strName)
				STOP_WIDGET_GROUP()
			ENDREPEAT
		STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP()
	IF ParentWidget <> NULL
		CLEAR_CURRENT_WIDGET_GROUP(ParentWidget)
	ENDIF
ENDPROC


//PROC CREATE_ONLY_SMALL_PLACEMENT_WIDGETS(SMALL_PLACEMENT_TOOLS& Placements, WIDGET_GROUP_ID ParentWidget = NULL)
//
//	INT i
//	TEXT_LABEL_63 strWidget
//
//	IF ParentWidget <> NULL
//		SET_CURRENT_WIDGET_GROUP(ParentWidget)
//	ENDIF
//	START_WIDGET_GROUP("HUD Placements")
//		START_WIDGET_GROUP("Text Placements")
//			REPEAT SMALL_MAX_NUMBER_OF_TEXT_PLACEMENTS i					
//				strWidget = ""
//				strWidget += i
//				START_WIDGET_GROUP(strWidget)	
//					ADD_WIDGET_FLOAT_SLIDER("x", Placements.TextPlacement[i].x, -1.0, 1.0, 0.001)	
//					ADD_WIDGET_FLOAT_SLIDER("y", Placements.TextPlacement[i].y, -1.0, 1.0, 0.001)
//	//				ADD_WIDGET_BOOL("Write To Debug", TextPlacement[i].bOutput)
//	//				ADD_WIDGET_STRING(TextPlacement[i].strName)
//				STOP_WIDGET_GROUP()
//			ENDREPEAT
//		STOP_WIDGET_GROUP()
//
//		START_WIDGET_GROUP("Sprite Placements")
//			REPEAT SMALL_MAX_NUMBER_OF_SPRITE_PLACEMENTS i					
//				strWidget = ""
//				strWidget += i
//				START_WIDGET_GROUP(strWidget)
//					ADD_WIDGET_FLOAT_SLIDER("x", Placements.SpritePlacement[i].x, -2.0, 2.0, 0.001)
//					ADD_WIDGET_FLOAT_SLIDER("y", Placements.SpritePlacement[i].y, -2.0, 2.0, 0.001)
//					ADD_WIDGET_FLOAT_SLIDER("w", Placements.SpritePlacement[i].w, -2.0, 2.0, 0.001)
//					ADD_WIDGET_FLOAT_SLIDER("h", Placements.SpritePlacement[i].h, -2.0, 2.0, 0.001)
//					ADD_WIDGET_INT_SLIDER("r", Placements.SpritePlacement[i].r, 0, 255, 1)
//					ADD_WIDGET_INT_SLIDER("g", Placements.SpritePlacement[i].g, 0, 255, 1)
//					ADD_WIDGET_INT_SLIDER("b", Placements.SpritePlacement[i].b, 0, 255, 1)
//					ADD_WIDGET_INT_SLIDER("a", Placements.SpritePlacement[i].a, 0, 255, 1)
//	//				ADD_WIDGET_BOOL("Write To Debug", SpritePlacement[i].bOutput)
//	//				ADD_WIDGET_STRING(SpritePlacement[i].strName)
//				STOP_WIDGET_GROUP()
//			ENDREPEAT
//		STOP_WIDGET_GROUP()
//		
//		START_WIDGET_GROUP("Rect Placements")
//			REPEAT SMALL_MAX_NUMBER_OF_RECTS i				
//				strWidget = ""
//				strWidget += i
//				START_WIDGET_GROUP(strWidget)
//					ADD_WIDGET_FLOAT_SLIDER("x", Placements.RectPlacement[i].x, -2.0, 2.0, 0.001)
//					ADD_WIDGET_FLOAT_SLIDER("y", Placements.RectPlacement[i].y, -2.0, 2.0, 0.001)
//					ADD_WIDGET_FLOAT_SLIDER("w", Placements.RectPlacement[i].w, -2.0, 2.0, 0.001)
//					ADD_WIDGET_FLOAT_SLIDER("h", Placements.RectPlacement[i].h, -2.0, 2.0, 0.001)
//					ADD_WIDGET_INT_SLIDER("r", Placements.RectPlacement[i].r, 0, 255, 1)
//					ADD_WIDGET_INT_SLIDER("g", Placements.RectPlacement[i].g, 0, 255, 1)
//					ADD_WIDGET_INT_SLIDER("b", Placements.RectPlacement[i].b, 0, 255, 1)
//					ADD_WIDGET_INT_SLIDER("a", Placements.RectPlacement[i].a, 0, 255, 1)
//	//				ADD_WIDGET_BOOL("Write To Debug", RectPlacement[i].bOutput)
//	//				ADD_WIDGET_STRING(RectPlacement[i].strName)
//				STOP_WIDGET_GROUP()
//			ENDREPEAT
//		STOP_WIDGET_GROUP()
//	STOP_WIDGET_GROUP()
//	IF ParentWidget <> NULL
//		CLEAR_CURRENT_WIDGET_GROUP(ParentWidget)
//	ENDIF
//ENDPROC
//#ENDIF

		
PROC CREATE_MEGA_PLACEMENT_WIDGETS(MEGA_PLACEMENT_TOOLS& Placements, WIDGET_GROUP_ID ParentWidget = NULL)
	IF ParentWidget <> NULL
		SET_CURRENT_WIDGET_GROUP(ParentWidget)
	ENDIF

	START_WIDGET_GROUP("HUD Placements")
#IF COMPILE_WIDGET_OUTPUT
		IF IS_STRING_NULL(Placements.txtScreenName)
			Placements.txtScreenName = ""
		ENDIF
		IF IS_STRING_NULL(Placements.txtFileName)
			Placements.txtFileName = ""
		ENDIF
		IF IS_STRING_NULL(Placements.txtFilePath)
			Placements.txtFilePath = ""
		ENDIF
		ADD_WIDGET_BOOL("Write To Debug", Placements.bOutputAllWidgets)
		ADD_WIDGET_BOOL("Output Above Values As Pixels (not floats)", Placements.bOutputPixelValues)
		ADD_WIDGET_BOOL("Write Enums To Debug", Placements.bOutputEnums)
		Placements.widScreenName = ADD_TEXT_WIDGET("Screen Name")
		SET_CONTENTS_OF_TEXT_WIDGET(Placements.widScreenName, Placements.txtScreenName)
		Placements.widFileName = ADD_TEXT_WIDGET("Export File Name")
		SET_CONTENTS_OF_TEXT_WIDGET(Placements.widFileName, Placements.txtFileName)
		Placements.widFilePath = ADD_TEXT_WIDGET("Export File Path")
		SET_CONTENTS_OF_TEXT_WIDGET(Placements.widFilePath, Placements.txtFilePath)
#ENDIF
		CREATE_TEXT_PLACEMENT_WIDGETS(Placements.TextPlacement)
		CREATE_SPRITE_PLACEMENT_WIDGETS(Placements.SpritePlacement)
		CREATE_RECT_PLACEMENT_WIDGETS(Placements.RectPlacement)
	STOP_WIDGET_GROUP()
	IF ParentWidget <> NULL
		CLEAR_CURRENT_WIDGET_GROUP(ParentWidget)
	ENDIF
ENDPROC



PROC CREATE_A_TEXT_PLACEMENT_WIDGET(TEXT_PLACEMENT& TextPlacement, STRING Title)
	START_WIDGET_GROUP(Title)
		ADD_WIDGET_FLOAT_SLIDER("x", TextPlacement.x, -1.0, 1.0, 0.001)	
		ADD_WIDGET_FLOAT_SLIDER("y", TextPlacement.y, -1.0, 1.0, 0.001)
	STOP_WIDGET_GROUP()
ENDPROC

PROC CREATE_A_SPRITE_PLACEMENT_WIDGET(SPRITE_PLACEMENT& SpritePlacement, STRING Title)

	START_WIDGET_GROUP(Title)
		ADD_WIDGET_FLOAT_SLIDER("x", SpritePlacement.x, -2.0, 2.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("y", SpritePlacement.y, -2.0, 2.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("w", SpritePlacement.w, -2.0, 2.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("h", SpritePlacement.h, -2.0, 2.0, 0.001)
		ADD_WIDGET_INT_SLIDER("r", SpritePlacement.r, -255, 255, 1)
		ADD_WIDGET_INT_SLIDER("g", SpritePlacement.g, -255, 255, 1)
		ADD_WIDGET_INT_SLIDER("b", SpritePlacement.b, -255, 255, 1)
		ADD_WIDGET_INT_SLIDER("a", SpritePlacement.a, -255, 255, 1)
		ADD_WIDGET_FLOAT_SLIDER("rotation", SpritePlacement.fRotation, 0, 360, 1)
	STOP_WIDGET_GROUP()
ENDPROC

PROC CREATE_A_RECT_PLACEMENT_WIDGET(RECT& RectPlacement, STRING Title)

	START_WIDGET_GROUP(Title)
		ADD_WIDGET_FLOAT_SLIDER("x", RectPlacement.x, -2.0, 2.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("y", RectPlacement.y, -2.0, 2.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("w", RectPlacement.w, -2.0, 2.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("h", RectPlacement.h, -2.0, 2.0, 0.001)
		ADD_WIDGET_INT_SLIDER("r", RectPlacement.r, -255, 255, 1)
		ADD_WIDGET_INT_SLIDER("g", RectPlacement.g, -255, 255, 1)
		ADD_WIDGET_INT_SLIDER("b", RectPlacement.b, -255, 255, 1)
		ADD_WIDGET_INT_SLIDER("a", RectPlacement.a, -255, 255, 1)
	STOP_WIDGET_GROUP()
ENDPROC


PROC CREATE_A_TEXT_STYLE_WIGET(TEXT_STYLE& aStyle, STRING Title)

	START_WIDGET_GROUP(Title)
		ADD_WIDGET_FLOAT_SLIDER("ScaleX", aStyle.XScale, -2.0, 2.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("ScaleY", aStyle.YScale, -2.0, 2.0, 0.001)
		ADD_WIDGET_INT_SLIDER("r", aStyle.r, -255, 255, 1)
		ADD_WIDGET_INT_SLIDER("g", aStyle.g, -255, 255, 1)
		ADD_WIDGET_INT_SLIDER("b", aStyle.b, -255, 255, 1)
		ADD_WIDGET_INT_SLIDER("a", aStyle.a, -255, 255, 1)
//		ADD_WIDGET_INT_SLIDER("drop", aStyle.drop, 0, 255, 1)
		ADD_WIDGET_FLOAT_SLIDER("WrapStartX", aStyle.WrapStartX, -2.0, 2.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("WrapEndX", aStyle.WrapEndX, -2.0, 2.0, 0.001)
	STOP_WIDGET_GROUP()
ENDPROC

PROC CREATE_TEXT_STYLE_WIDGETS(TEXTSTYLES& aTextStyle, WIDGET_GROUP_ID ParentWidget = NULL)

	IF ParentWidget <> NULL
		SET_CURRENT_WIDGET_GROUP(ParentWidget)
	ENDIF
	START_WIDGET_GROUP("HUD Text Styles")
		START_WIDGET_GROUP("TS_TITLE")
			ADD_WIDGET_FLOAT_SLIDER("ScaleX", aTextStyle.TS_TITLE.XScale, -2.0, 2.0, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("ScaleY", aTextStyle.TS_TITLE.YScale, -2.0, 2.0, 0.001)
			ADD_WIDGET_INT_SLIDER("r", aTextStyle.TS_TITLE.r, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("g", aTextStyle.TS_TITLE.g, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("b", aTextStyle.TS_TITLE.b, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("a", aTextStyle.TS_TITLE.a, 0, 255, 1)
//			ADD_WIDGET_INT_SLIDER("drop", aTextStyle.TS_TITLE.drop, 0, 255, 1)
			ADD_WIDGET_FLOAT_SLIDER("WrapStartX", aTextStyle.TS_TITLE.WrapStartX, -2.0, 2.0, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("WrapEndX", aTextStyle.TS_TITLE.WrapEndX, -2.0, 2.0, 0.001)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("TS_TITLESMALL")
			ADD_WIDGET_FLOAT_SLIDER("ScaleX", aTextStyle.TS_TITLESMALL.XScale, -2.0, 2.0, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("ScaleY", aTextStyle.TS_TITLESMALL.YScale, -2.0, 2.0, 0.001)
			ADD_WIDGET_INT_SLIDER("r", aTextStyle.TS_TITLESMALL.r, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("g", aTextStyle.TS_TITLESMALL.g, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("b", aTextStyle.TS_TITLESMALL.b, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("a", aTextStyle.TS_TITLESMALL.a, 0, 255, 1)
//			ADD_WIDGET_INT_SLIDER("drop", aTextStyle.TS_TITLESMALL.drop, 0, 255, 1)
			ADD_WIDGET_FLOAT_SLIDER("WrapStartX", aTextStyle.TS_TITLESMALL.WrapStartX, -2.0, 2.0, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("WrapEndX", aTextStyle.TS_TITLESMALL.WrapEndX, -2.0, 2.0, 0.001)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("TS_TITLEHUD")
			ADD_WIDGET_FLOAT_SLIDER("ScaleX", aTextStyle.TS_TITLEHUD.XScale, -2.0, 2.0, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("ScaleY", aTextStyle.TS_TITLEHUD.YScale, -2.0, 2.0, 0.001)
			ADD_WIDGET_INT_SLIDER("r", aTextStyle.TS_TITLEHUD.r, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("g", aTextStyle.TS_TITLEHUD.g, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("b", aTextStyle.TS_TITLEHUD.b, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("a", aTextStyle.TS_TITLEHUD.a, 0, 255, 1)
//			ADD_WIDGET_INT_SLIDER("drop", aTextStyle.TS_TITLEHUD.drop, 0, 255, 1)
			ADD_WIDGET_FLOAT_SLIDER("WrapStartX", aTextStyle.TS_TITLEHUD.WrapStartX, -2.0, 2.0, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("WrapEndX", aTextStyle.TS_TITLEHUD.WrapEndX, -2.0, 2.0, 0.001)
		STOP_WIDGET_GROUP()
		
		
		
//		START_WIDGET_GROUP("TS_STANDARDTINY")
//			ADD_WIDGET_FLOAT_SLIDER("ScaleX", aTextStyle.TS_STANDARDTINY.XScale, -2.0, 2.0, 0.001)
//			ADD_WIDGET_FLOAT_SLIDER("ScaleY", aTextStyle.TS_STANDARDTINY.YScale, -2.0, 2.0, 0.001)
//			ADD_WIDGET_INT_SLIDER("r", aTextStyle.TS_STANDARDTINY.r, 0, 255, 1)
//			ADD_WIDGET_INT_SLIDER("g", aTextStyle.TS_STANDARDTINY.g, 0, 255, 1)
//			ADD_WIDGET_INT_SLIDER("b", aTextStyle.TS_STANDARDTINY.b, 0, 255, 1)
//			ADD_WIDGET_INT_SLIDER("a", aTextStyle.TS_STANDARDTINY.a, 0, 255, 1)
//			ADD_WIDGET_INT_SLIDER("drop", aTextStyle.TS_STANDARDTINY.drop, 0, 255, 1)
//			ADD_WIDGET_FLOAT_SLIDER("WrapStartX", aTextStyle.TS_STANDARDTINY.WrapStartX, -2.0, 2.0, 0.001)
//			ADD_WIDGET_FLOAT_SLIDER("WrapEndX", aTextStyle.TS_STANDARDTINY.WrapEndX, -2.0, 2.0, 0.001)
//		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("TS_STANDARDSMALL")
			ADD_WIDGET_FLOAT_SLIDER("ScaleX", aTextStyle.TS_STANDARDSMALL.XScale, -2.0, 2.0, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("ScaleY", aTextStyle.TS_STANDARDSMALL.YScale, -2.0, 2.0, 0.001)
			ADD_WIDGET_INT_SLIDER("r", aTextStyle.TS_STANDARDSMALL.r, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("g", aTextStyle.TS_STANDARDSMALL.g, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("b", aTextStyle.TS_STANDARDSMALL.b, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("a", aTextStyle.TS_STANDARDSMALL.a, 0, 255, 1)
//			ADD_WIDGET_INT_SLIDER("drop", aTextStyle.TS_STANDARDSMALL.drop, 0, 255, 1)
			ADD_WIDGET_FLOAT_SLIDER("WrapStartX", aTextStyle.TS_STANDARDSMALL.WrapStartX, -2.0, 2.0, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("WrapEndX", aTextStyle.TS_STANDARDSMALL.WrapEndX, -2.0, 2.0, 0.001)
		STOP_WIDGET_GROUP()
		
		
		START_WIDGET_GROUP("TS_STANDARDMEDIUM")
			ADD_WIDGET_FLOAT_SLIDER("ScaleX", aTextStyle.TS_STANDARDMEDIUM.XScale, -2.0, 2.0, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("ScaleY", aTextStyle.TS_STANDARDMEDIUM.YScale, -2.0, 2.0, 0.001)
			ADD_WIDGET_INT_SLIDER("r", aTextStyle.TS_STANDARDMEDIUM.r, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("g", aTextStyle.TS_STANDARDMEDIUM.g, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("b", aTextStyle.TS_STANDARDMEDIUM.b, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("a", aTextStyle.TS_STANDARDMEDIUM.a, 0, 255, 1)
//			ADD_WIDGET_INT_SLIDER("drop", aTextStyle.TS_STANDARDMEDIUM.drop, 0, 255, 1)
			ADD_WIDGET_FLOAT_SLIDER("WrapStartX", aTextStyle.TS_STANDARDMEDIUM.WrapStartX, -2.0, 2.0, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("WrapEndX", aTextStyle.TS_STANDARDMEDIUM.WrapEndX, -2.0, 2.0, 0.001)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("TS_LBD")
			ADD_WIDGET_FLOAT_SLIDER("ScaleX", aTextStyle.TS_LBD.XScale, -2.0, 2.0, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("ScaleY", aTextStyle.TS_LBD.YScale, -2.0, 2.0, 0.001)
			ADD_WIDGET_INT_SLIDER("r", aTextStyle.TS_LBD.r, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("g", aTextStyle.TS_LBD.g, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("b", aTextStyle.TS_LBD.b, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("a", aTextStyle.TS_LBD.a, 0, 255, 1)
//			ADD_WIDGET_INT_SLIDER("drop", aTextStyle.TS_LBD.drop, 0, 255, 1)
			ADD_WIDGET_FLOAT_SLIDER("WrapStartX", aTextStyle.TS_LBD.WrapStartX, -2.0, 2.0, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("WrapEndX", aTextStyle.TS_LBD.WrapEndX, -2.0, 2.0, 0.001)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("TS_LBD_PLYR")
			ADD_WIDGET_FLOAT_SLIDER("ScaleX", aTextStyle.TS_LBDPLYR.XScale, -2.0, 2.0, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("ScaleY", aTextStyle.TS_LBDPLYR.YScale, -2.0, 2.0, 0.001)
			ADD_WIDGET_INT_SLIDER("r", aTextStyle.TS_LBDPLYR.r, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("g", aTextStyle.TS_LBDPLYR.g, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("b", aTextStyle.TS_LBDPLYR.b, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("a", aTextStyle.TS_LBDPLYR.a, 0, 255, 1)
//			ADD_WIDGET_INT_SLIDER("drop", aTextStyle.TS_LBDPLYR.drop, 0, 255, 1)
			ADD_WIDGET_FLOAT_SLIDER("WrapStartX", aTextStyle.TS_LBDPLYR.WrapStartX, -2.0, 2.0, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("WrapEndX", aTextStyle.TS_LBDPLYR.WrapEndX, -2.0, 2.0, 0.001)
		STOP_WIDGET_GROUP()
		
//		START_WIDGET_GROUP("TS_STANDARDBIG")
//			ADD_WIDGET_FLOAT_SLIDER("ScaleX", aTextStyle.TS_STANDARDBIG.XScale, -2.0, 2.0, 0.001)
//			ADD_WIDGET_FLOAT_SLIDER("ScaleY", aTextStyle.TS_STANDARDBIG.YScale, -2.0, 2.0, 0.001)
//			ADD_WIDGET_INT_SLIDER("r", aTextStyle.TS_STANDARDBIG.r, 0, 255, 1)
//			ADD_WIDGET_INT_SLIDER("g", aTextStyle.TS_STANDARDBIG.g, 0, 255, 1)
//			ADD_WIDGET_INT_SLIDER("b", aTextStyle.TS_STANDARDBIG.b, 0, 255, 1)
//			ADD_WIDGET_INT_SLIDER("a", aTextStyle.TS_STANDARDBIG.a, 0, 255, 1)
//			ADD_WIDGET_INT_SLIDER("drop", aTextStyle.TS_STANDARDBIG.drop, 0, 255, 1)
//			ADD_WIDGET_FLOAT_SLIDER("WrapStartX", aTextStyle.TS_STANDARDBIG.WrapStartX, -2.0, 2.0, 0.001)
//			ADD_WIDGET_FLOAT_SLIDER("WrapEndX", aTextStyle.TS_STANDARDBIG.WrapEndX, -2.0, 2.0, 0.001)
//		STOP_WIDGET_GROUP()
//		
//		START_WIDGET_GROUP("TS_STANDARDHUGE")
//			ADD_WIDGET_FLOAT_SLIDER("ScaleX", aTextStyle.TS_STANDARDHUGE.XScale, -2.0, 2.0, 0.001)
//			ADD_WIDGET_FLOAT_SLIDER("ScaleY", aTextStyle.TS_STANDARDHUGE.YScale, -2.0, 2.0, 0.001)
//			ADD_WIDGET_INT_SLIDER("r", aTextStyle.TS_STANDARDHUGE.r, 0, 255, 1)
//			ADD_WIDGET_INT_SLIDER("g", aTextStyle.TS_STANDARDHUGE.g, 0, 255, 1)
//			ADD_WIDGET_INT_SLIDER("b", aTextStyle.TS_STANDARDHUGE.b, 0, 255, 1)
//			ADD_WIDGET_INT_SLIDER("a", aTextStyle.TS_STANDARDHUGE.a, 0, 255, 1)
//			ADD_WIDGET_INT_SLIDER("drop", aTextStyle.TS_STANDARDHUGE.drop, 0, 255, 1)
//			ADD_WIDGET_FLOAT_SLIDER("WrapStartX", aTextStyle.TS_STANDARDHUGE.WrapStartX, -2.0, 2.0, 0.001)
//			ADD_WIDGET_FLOAT_SLIDER("WrapEndX", aTextStyle.TS_STANDARDHUGE.WrapEndX, -2.0, 2.0, 0.001)
//		STOP_WIDGET_GROUP()
		
//		START_WIDGET_GROUP("TS_EXTRA1")
//			ADD_WIDGET_FLOAT_SLIDER("ScaleX", aTextStyle.TS_EXTRA1.XScale, -2.0, 2.0, 0.001)
//			ADD_WIDGET_FLOAT_SLIDER("ScaleY", aTextStyle.TS_EXTRA1.YScale, -2.0, 2.0, 0.001)
//			ADD_WIDGET_INT_SLIDER("r", aTextStyle.TS_EXTRA1.r, 0, 255, 1)
//			ADD_WIDGET_INT_SLIDER("g", aTextStyle.TS_EXTRA1.g, 0, 255, 1)
//			ADD_WIDGET_INT_SLIDER("b", aTextStyle.TS_EXTRA1.b, 0, 255, 1)
//			ADD_WIDGET_INT_SLIDER("a", aTextStyle.TS_EXTRA1.a, 0, 255, 1)
//			ADD_WIDGET_INT_SLIDER("drop", aTextStyle.TS_EXTRA1.drop, 0, 255, 1)
//			ADD_WIDGET_FLOAT_SLIDER("WrapStartX", aTextStyle.TS_EXTRA1.WrapStartX, -2.0, 2.0, 0.001)
//			ADD_WIDGET_FLOAT_SLIDER("WrapEndX", aTextStyle.TS_EXTRA1.WrapEndX, -2.0, 2.0, 0.001)
//		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("TS_HUDNUMBER")
			ADD_WIDGET_FLOAT_SLIDER("ScaleX", aTextStyle.TS_HUDNUMBER.XScale, -2.0, 2.0, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("ScaleY", aTextStyle.TS_HUDNUMBER.YScale, -2.0, 2.0, 0.001)
			ADD_WIDGET_INT_SLIDER("r", aTextStyle.TS_HUDNUMBER.r, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("g", aTextStyle.TS_HUDNUMBER.g, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("b", aTextStyle.TS_HUDNUMBER.b, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("a", aTextStyle.TS_HUDNUMBER.a, 0, 255, 1)
//			ADD_WIDGET_INT_SLIDER("drop", aTextStyle.TS_HUDNUMBER.drop, 0, 255, 1)
			ADD_WIDGET_FLOAT_SLIDER("WrapStartX", aTextStyle.TS_HUDNUMBER.WrapStartX, -2.0, 2.0, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("WrapEndX", aTextStyle.TS_HUDNUMBER.WrapEndX, -2.0, 2.0, 0.001)
		STOP_WIDGET_GROUP()
		
//		START_WIDGET_GROUP("TS_STANDARDMEDIUMHUD")
//			ADD_WIDGET_FLOAT_SLIDER("ScaleX", aTextStyle.TS_STANDARDMEDIUMHUD.XScale, -2.0, 2.0, 0.001)
//			ADD_WIDGET_FLOAT_SLIDER("ScaleY", aTextStyle.TS_STANDARDMEDIUMHUD.YScale, -2.0, 2.0, 0.001)
//			ADD_WIDGET_INT_SLIDER("r", aTextStyle.TS_STANDARDMEDIUMHUD.r, 0, 255, 1)
//			ADD_WIDGET_INT_SLIDER("g", aTextStyle.TS_STANDARDMEDIUMHUD.g, 0, 255, 1)
//			ADD_WIDGET_INT_SLIDER("b", aTextStyle.TS_STANDARDMEDIUMHUD.b, 0, 255, 1)
//			ADD_WIDGET_INT_SLIDER("a", aTextStyle.TS_STANDARDMEDIUMHUD.a, 0, 255, 1)
//			ADD_WIDGET_INT_SLIDER("drop", aTextStyle.TS_STANDARDMEDIUMHUD.drop, 0, 255, 1)
//			ADD_WIDGET_FLOAT_SLIDER("WrapStartX", aTextStyle.TS_STANDARDMEDIUMHUD.WrapStartX, -2.0, 2.0, 0.001)
//			ADD_WIDGET_FLOAT_SLIDER("WrapEndX", aTextStyle.TS_STANDARDMEDIUMHUD.WrapEndX, -2.0, 2.0, 0.001)
//		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("TS_STANDARDSMALLHUD")
			ADD_WIDGET_FLOAT_SLIDER("ScaleX", aTextStyle.TS_STANDARDSMALLHUD.XScale, -2.0, 2.0, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("ScaleY", aTextStyle.TS_STANDARDSMALLHUD.YScale, -2.0, 2.0, 0.001)
			ADD_WIDGET_INT_SLIDER("r", aTextStyle.TS_STANDARDSMALLHUD.r, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("g", aTextStyle.TS_STANDARDSMALLHUD.g, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("b", aTextStyle.TS_STANDARDSMALLHUD.b, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("a", aTextStyle.TS_STANDARDSMALLHUD.a, 0, 255, 1)
//			ADD_WIDGET_INT_SLIDER("drop", aTextStyle.TS_STANDARDSMALLHUD.drop, 0, 255, 1)
			ADD_WIDGET_FLOAT_SLIDER("WrapStartX", aTextStyle.TS_STANDARDSMALLHUD.WrapStartX, -2.0, 2.0, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("WrapEndX", aTextStyle.TS_STANDARDSMALLHUD.WrapEndX, -2.0, 2.0, 0.001)
		STOP_WIDGET_GROUP()
		
		
	STOP_WIDGET_GROUP()
	IF ParentWidget <> NULL
		CLEAR_CURRENT_WIDGET_GROUP(ParentWidget)
	ENDIF
ENDPROC

#ENDIF	//	IS_DEBUG_BUILD


///TABS
///    
///    
///    

PROC SET_STANDARD_TAB_DETAILS(TAB_TEXTSTYLES& aTabTextStyle)

	aTabTextStyle.TS_TabSelected.aFont = FONT_STANDARD
	aTabTextStyle.TS_TabSelected.XScale = 0.2020
	aTabTextStyle.TS_TabSelected.YScale = 0.4850
	aTabTextStyle.TS_TabSelected.r = 0
	aTabTextStyle.TS_TabSelected.g = 0
	aTabTextStyle.TS_TabSelected.b = 0
	aTabTextStyle.TS_TabSelected.a = 255
	aTabTextStyle.TS_TabSelected.drop = DROPSTYLE_NONE
	aTabTextStyle.TS_TabSelected.WrapStartX = 0.0000
	aTabTextStyle.TS_TabSelected.WrapEndX = 0.0000


	aTabTextStyle.TS_Tab.aFont = FONT_STANDARD
	aTabTextStyle.TS_Tab.XScale = 0.2020
	aTabTextStyle.TS_Tab.YScale = 0.4030
	aTabTextStyle.TS_Tab.r = 255
	aTabTextStyle.TS_Tab.g = 255
	aTabTextStyle.TS_Tab.b = 255
	aTabTextStyle.TS_Tab.a = 128
	aTabTextStyle.TS_Tab.drop = DROPSTYLE_NONE
	aTabTextStyle.TS_Tab.WrapStartX = 0.0000
	aTabTextStyle.TS_Tab.WrapEndX = 0.0000	

ENDPROC


#IF IS_DEBUG_BUILD

PROC CREATE_ONLY_TAB_WIDGETS(TAB_PLACEMENT_TOOLS& Placement, WIDGET_GROUP_ID ParentWidget = NULL)

	IF ParentWidget <> NULL
		SET_CURRENT_WIDGET_GROUP(ParentWidget)
	ENDIF
	START_WIDGET_GROUP("TAB Placements")
		
		ADD_WIDGET_BOOL("bUseEvenSpacing", Placement.FixedValues.bUseEvenSpacing)
		ADD_WIDGET_BOOL("bDullNonSelectedTabs", Placement.FixedValues.bDullNonSelectedTabs)
		ADD_WIDGET_INT_SLIDER("iNumberOfTabs", Placement.iNumberOfTabs, 0, MAX_NUMBER_OF_TABS, 1)
		ADD_WIDGET_FLOAT_SLIDER("fTabsCentreX", Placement.FixedValues.fTabsCentreX, -1.0, 1.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("fTabsCentreY", Placement.FixedValues.fTabsCentreY, -1.0, 1.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("fTabsTotalWidth", Placement.FixedValues.fTabsTotalWidth, -1.0, 1.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("fTabsTotalHeight", Placement.FixedValues.fTabsTotalHeight, -1.0, 1.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("fTabIndentation", Placement.FixedValues.fTabIndentation, -1.0, 1.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("fTabSeperation", Placement.FixedValues.fTabSeperation, -1.0, 1.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("fTabHeight", Placement.FixedValues.fTabHeight, -1.0, 1.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("fTabGangHeight", Placement.FixedValues.fTabGangHeight, -1.0, 1.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("fTabTitleIndentFromTop", Placement.FixedValues.fTabTitleIndentFromTop, -1.0, 1.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("fTabTitleIndentFromTopSelected", Placement.FixedValues.fTabTitleIndentFromTopSelected, -1.0, 1.0, 0.001)
		ADD_WIDGET_INT_SLIDER("iTabAlpha", Placement.FixedValues.iTabAlpha, 0, 255, 1)
		ADD_WIDGET_FLOAT_SLIDER("TabDivingValue", Placement.FixedValues.TabDivingValue, 0, 10, 0.01)

		ADD_WIDGET_FLOAT_SLIDER("x", Placement.TabGangColour[0].x, -2.0, 2.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("y", Placement.TabGangColour[0].y, -2.0, 2.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("w", Placement.TabGangColour[0].w, -2.0, 2.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("h", Placement.TabGangColour[0].h, -2.0, 2.0, 0.001)
		ADD_WIDGET_INT_SLIDER("r", Placement.TabGangColour[0].r, 0, 255, 1)
		ADD_WIDGET_INT_SLIDER("g", Placement.TabGangColour[0].g, 0, 255, 1)
		ADD_WIDGET_INT_SLIDER("b", Placement.TabGangColour[0].b, 0, 255, 1)
		ADD_WIDGET_INT_SLIDER("a", Placement.TabGangColour[0].a, 0, 255, 1)
	
		
	STOP_WIDGET_GROUP()
	IF ParentWidget <> NULL
		CLEAR_CURRENT_WIDGET_GROUP(ParentWidget)
	ENDIF
ENDPROC




PROC CREATE_TAB_TEXT_STYLE_WIDGETS(TAB_TEXTSTYLES& aTextStyle, WIDGET_GROUP_ID ParentWidget = NULL)

	IF ParentWidget <> NULL
		SET_CURRENT_WIDGET_GROUP(ParentWidget)
	ENDIF
	START_WIDGET_GROUP("HUD Text Styles")
		START_WIDGET_GROUP("TS_Tab")
			ADD_WIDGET_FLOAT_SLIDER("ScaleX", aTextStyle.TS_Tab.XScale, -2.0, 2.0, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("ScaleY", aTextStyle.TS_Tab.YScale, -2.0, 2.0, 0.001)
			ADD_WIDGET_INT_SLIDER("r", aTextStyle.TS_Tab.r, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("g", aTextStyle.TS_Tab.g, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("b", aTextStyle.TS_Tab.b, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("a", aTextStyle.TS_Tab.a, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("drop", aTextStyle.TS_Tab.drop, 0, 255, 1)
			ADD_WIDGET_FLOAT_SLIDER("WrapStartX", aTextStyle.TS_Tab.WrapStartX, -2.0, 2.0, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("WrapEndX", aTextStyle.TS_Tab.WrapEndX, -2.0, 2.0, 0.001)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("TS_TabSelected")
			ADD_WIDGET_FLOAT_SLIDER("ScaleX", aTextStyle.TS_TabSelected.XScale, -2.0, 2.0, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("ScaleY", aTextStyle.TS_TabSelected.YScale, -2.0, 2.0, 0.001)
			ADD_WIDGET_INT_SLIDER("r", aTextStyle.TS_TabSelected.r, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("g", aTextStyle.TS_TabSelected.g, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("b", aTextStyle.TS_TabSelected.b, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("a", aTextStyle.TS_TabSelected.a, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("drop", aTextStyle.TS_TabSelected.drop, 0, 255, 1)
			ADD_WIDGET_FLOAT_SLIDER("WrapStartX", aTextStyle.TS_TabSelected.WrapStartX, -2.0, 2.0, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("WrapEndX", aTextStyle.TS_TabSelected.WrapEndX, -2.0, 2.0, 0.001)
		STOP_WIDGET_GROUP()

		
	STOP_WIDGET_GROUP()
	IF ParentWidget <> NULL
		CLEAR_CURRENT_WIDGET_GROUP(ParentWidget)
	ENDIF
ENDPROC


PROC DEBUG_PRINT_HUD_PLACEMENTS(LARGE_PLACEMENT_TOOLS& Placements)
	INT iPlacementIndex

	REPEAT COUNT_OF(Placements.TextPlacement) iPlacementIndex					
		IF Placements.TextPlacement[iPlacementIndex].x != 0.0
			PRINTLN("TextPlacement[", iPlacementIndex, "] = (", Placements.TextPlacement[iPlacementIndex].x, ",", Placements.TextPlacement[iPlacementIndex].x, ")")
		ENDIF
	ENDREPEAT
	REPEAT COUNT_OF(Placements.SpritePlacement) iPlacementIndex					
		IF Placements.SpritePlacement[iPlacementIndex].a != 0
			PRINTLN("SpritePlacement[", iPlacementIndex, "] = (", Placements.SpritePlacement[iPlacementIndex].x, ",", Placements.SpritePlacement[iPlacementIndex].x, ")")
		ENDIF
	ENDREPEAT
	REPEAT COUNT_OF(Placements.RectPlacement) iPlacementIndex				
		IF Placements.RectPlacement[iPlacementIndex].w != 0.0
			PRINTLN("RectPlacement[", iPlacementIndex, "] = (", Placements.RectPlacement[iPlacementIndex].x, ",", Placements.RectPlacement[iPlacementIndex].x, ")")
		ENDIF
	ENDREPEAT
ENDPROC

#ENDIF	//	IS_DEBUG_BUILD


//PURPOSE: Gets the blip location for a Hold UP
FUNC VECTOR GET_HOLD_UP_LOCATION(INT iHoldUp)
	SWITCH INT_TO_ENUM(Crim_HOLD_UP_POINT, iHoldUp)
		//CONVENIANCE STORES - 24/7
		CASE eCRIM_HUP_SHOP247_1 	RETURN <<1394.1692, 3599.8601, 34.0121>>
		CASE eCRIM_HUP_SHOP247_2 	RETURN <<-3038.9082, 589.5187, 6.9048>>
		CASE eCRIM_HUP_SHOP247_3 	RETURN <<-3240.3169, 1004.4334, 11.8307>>
		CASE eCRIM_HUP_SHOP247_4 	RETURN <<544.2802, 2672.8113, 41.1566>>
		CASE eCRIM_HUP_SHOP247_5 	RETURN <<2564.0679, 384.9708, 107.4644>>
		CASE eCRIM_HUP_SHOP247_6 	RETURN <<2682.0034, 3282.5432, 54.2411>>
		CASE eCRIM_HUP_SHOP247_7 	RETURN <<1731.2098, 6411.4033, 34.0372>>
		CASE eCRIM_HUP_SHOP247_8 	RETURN <<1965.0542, 3740.5552, 31.3448>>
		CASE eCRIM_HUP_SHOP247_9 	RETURN <<27.0641, -1348.8127, 28.5036>>
		CASE eCRIM_HUP_SHOP247_10 	RETURN <<376.6533, 323.6471, 102.5664>>
		
		//LIQUOR STORES
		CASE eCRIM_HUP_LIQUOR_1 	RETURN <<1195.4320, 2703.1143, 37.1573>>
		CASE eCRIM_HUP_LIQUOR_2 	RETURN <<-2973.2617, 390.8184, 14.0433>>
		CASE eCRIM_HUP_LIQUOR_3 	RETURN <<-1226.4644, -902.5864, 11.2783>>
		CASE eCRIM_HUP_LIQUOR_4 	RETURN <<1141.3596, -980.8802, 45.4155>>
		CASE eCRIM_HUP_LIQUOR_5 	RETURN <<-1491.0565, -383.5728, 39.1706>>
		
		//GAS STATIONS
		CASE eCRIM_HUP_GAS_1 		RETURN <<1698.8085, 4929.1978, 41.0783>>
		CASE eCRIM_HUP_GAS_2 		RETURN <<-711.7210, -916.6965, 18.2145>>
		CASE eCRIM_HUP_GAS_3  		RETURN <<-53.1240, -1756.4054, 28.4210>>
		CASE eCRIM_HUP_GAS_4  		RETURN <<1159.5421, -326.6986, 67.9230>>
		CASE eCRIM_HUP_GAS_5 		RETURN <<-1822.2866, 788.0060, 137.1859>>
	ENDSWITCH
	
	NET_SCRIPT_ASSERT("GET_HOLD_UP_LOCATION()  -  INVALID HOLD UP LOCATION PASSED IN")
	RETURN <<0, 0, 0>>
ENDFUNC







//Checks the postion of analouge sticks and returns true if they are moved in the passed in front end direction
FUNC BOOL IS_ANALOGUE_STICK_MOVED(CONTROL_ACTION caPassed, INT &iDelayInt, BOOL UseMovementDelay = TRUE)
	//Get the postions of the analouge sticks
	//INT iLx, iLy, iRx, iRy
	//GET_CONTROL_VALUE_OF_ANALOGUE_STICKS(  iLx, iLy, iRx, iRy)
	INT iLx = GET_CONTROL_VALUE(FRONTEND_CONTROL, INPUT_FRONTEND_AXIS_X) - 127
	INT iLy = GET_CONTROL_VALUE(FRONTEND_CONTROL, INPUT_FRONTEND_AXIS_Y) - 127
	INT iRx = GET_CONTROL_VALUE(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT_AXIS_X) - 127
//	INT iRY = GET_CONTROL_VALUE(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT_AXIS_Y) - 127
	CONST_INT ciMovementThreshold 	30
	CONST_INT ciMovementDelay 		250
	
	//Check to see if the left stick is pointing in a direction and is past the movement threshold and the timer has expired
	SWITCH caPassed
		CASE INPUT_FRONTEND_LEFT
			IF iLx < -ciMovementThreshold
				IF iDelayInt < GET_GAME_TIMER()
				OR UseMovementDelay = FALSE
					iDelayInt = GET_GAME_TIMER() + ciMovementDelay
					PRINTLN("lx", iLx)
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
		CASE INPUT_FRONTEND_RIGHT
			IF iLx > ciMovementThreshold
				IF iDelayInt < GET_GAME_TIMER()
				OR UseMovementDelay = FALSE
					iDelayInt = GET_GAME_TIMER() + ciMovementDelay
					PRINTLN("lxRight", iLx)
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
		CASE INPUT_FRONTEND_UP
			IF iLy <- ciMovementThreshold
				IF iDelayInt < GET_GAME_TIMER()
				OR UseMovementDelay = FALSE
					PRINTLN("lxy Up ", iLy)
					iDelayInt = GET_GAME_TIMER() + ciMovementDelay
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
		CASE INPUT_FRONTEND_DOWN
			IF iLy > ciMovementThreshold
				IF iDelayInt < GET_GAME_TIMER()
				OR UseMovementDelay = FALSE
					PRINTLN("lxy Down ", iLy)
					iDelayInt = GET_GAME_TIMER() + ciMovementDelay
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
		CASE INPUT_FRONTEND_RRIGHT //RightStick right
			IF iRx > ciMovementThreshold
				IF iDelayInt < GET_GAME_TIMER()
				OR UseMovementDelay = FALSE
					iDelayInt = GET_GAME_TIMER() + ciMovementDelay
					PRINTLN("RxRight", iRx)
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
		CASE INPUT_FRONTEND_RLEFT
			IF iRx < -ciMovementThreshold
				IF iDelayInt < GET_GAME_TIMER()
				OR UseMovementDelay = FALSE
					iDelayInt = GET_GAME_TIMER() + ciMovementDelay
					PRINTLN("RxLeft", iRx)
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

//Checks for button presses to decrease the current selected item
FUNC BOOL DECREASE_CURRENT_SELECTED_ITEM(INT &iDelayInt)
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
	//OR IS_BUTTON_JUST_PRESSED(PAD1, DPADLEFT)	
	OR IS_ANALOGUE_STICK_MOVED(INPUT_FRONTEND_LEFT, iDelayInt)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC	
//Checks for button presses to increase the current selected item
FUNC BOOL INCREASE_CURRENT_SELECTED_ITEM(INT &iDelayInt)	
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
	//OR IS_BUTTON_JUST_PRESSED(PAD1, DPADRIGHT)
	OR IS_ANALOGUE_STICK_MOVED(INPUT_FRONTEND_RIGHT, iDelayInt)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC
//Checks for button presses to DEcrease the current ROW
FUNC BOOL DECREASE_CURRENT_ROW(INT &iDelayInt)	
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
	//OR IS_BUTTON_JUST_PRESSED(PAD1, DPADUP)	
	OR IS_ANALOGUE_STICK_MOVED(INPUT_FRONTEND_UP, iDelayInt)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC
//Checks for button presses to Increase the current ROW
FUNC BOOL INCREASE_CURRENT_ROW(INT &iDelayInt)	
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
//	OR IS_BUTTON_JUST_PRESSED(PAD1, DPADDOWN)
	OR IS_ANALOGUE_STICK_MOVED(INPUT_FRONTEND_DOWN, iDelayInt)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC









