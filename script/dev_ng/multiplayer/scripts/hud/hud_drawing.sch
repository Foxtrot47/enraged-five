USING "commands_hud.sch"
USING "commands_misc.sch"
USING "commands_debug.sch"
USING "commands_graphics.sch"
USING "net_include.sch"
USING "Net_Script_timers.sch"

// This makes it so the default option for the screen_placement/hud_drawing is OFF, and none of the extra structures or functions are used
// If you wish to have it on, define the CONST at the top of your file as 1
#IF NOT DEFINED(COMPILE_WIDGET_OUTPUT)
CONST_INT       COMPILE_WIDGET_OUTPUT   0
#ENDIF

CONST_INT MAX_COP_TIPS 32
CONST_INT MAX_CRIM_TIPS 12
CONST_INT MAX_NUMBER_OF_SPRITES 28
CONST_INT MAX_NUM_OF_RECT 5
CONST_INT MAX_NUM_OF_TEXT 30

// Used for Minigame UI Positioning
CONST_FLOAT MINIGAME_X_PADDING_LEFT 0.0047 // 6px/1280px
CONST_FLOAT MINIGAME_X_PADDING_RIGHT 0.0055 // 7px/1280px

FLOAT fHudScreenOffsetX
FLOAT fHudScreenOffsetY

ENUM OVERHEAD_OFFSETS
    OO_NONE = 0,                
    OO_DRIVER,                      
    OO_FRONT_RIGHT,                 
    OO_BACK_LEFT,                           
    OO_BACK_RIGHT,                      
    OO_EXTRA_LEFT_1,
    OO_EXTRA_RIGHT_1,
    OO_EXTRA_LEFT_2,
    OO_EXTRA_RIGHT_2,
    OO_EXTRA_LEFT_3,
    OO_EXTRA_RIGHT_3,
    OO_BIKE_DRIVER,
    OO_BIKE_PASSENGER,
    OO_PLAYER_VEHICLE,
    OO_OBJECTIVE_VEHICLE, 
    OO_OBJECTIVE_OBJECT
    
ENDENUM

ENUM STANDARD_TEXTTYPE
    TEXTTYPE_TS_TITLE   = 0,
    TEXTTYPE_TS_TITLESMALL,
//  TEXTTYPE_TS_STANDARDTINY,
    TEXTTYPE_TS_STANDARDSMALL,
    TEXTTYPE_TS_STANDARDMEDIUM,
    TEXTTYPE_TS_LBD,
    TEXTTYPE_TS_LBD_PLYR_NAME,
    
    TEXTTYPE_TS_TIMER_PLYR_NAME,
//  TEXTTYPE_TS_STANDARDBIG,
//  TEXTTYPE_TS_STANDARDHUGE,
//  TEXTTYPE_TS_EXTRA1,
    
    TEXTTYPE_TS_TITLEHUD,
    TEXTTYPE_TS_HUDNUMBER,
    
    TEXTTYPE_TS_UI_TIMERNUMBER_THREESETS,
    TEXTTYPE_TS_UI_TIMERNUMBER_TWOSETS,
    TEXTTYPE_TS_UI_SINGLE_NUMBER,
    TEXTTYPE_TS_UI_DASHSINGLE,
    TEXTTYPE_TS_UI_DASHDOUBLE,
    TEXTTYPE_TS_UI_DASHTRIPLE,
    TEXTTYPE_TS_UI_POSITION_SYMBOL,
    TEXTTYPE_TS_UI_SCORE,
    TEXTTYPE_TS_UI_SCORE_SMALL,
    TEXTTYPE_TS_UI_SCORE_SMALL_NON_ROMANIC,
    TEXTTYPE_TS_UI_AMPM,
    TEXTTYPE_TS_UI_METER_BIG_TITLE,
    TEXTTYPE_TS_UI_METER_BIG_TITLE_WAVE,

    TEXTTYPE_TS_STANDARDMEDIUMHUD,
    TEXTTYPE_TS_STANDARDSMALLHUD,
     
    TEXTTYPE_TS_MINIGAME_MENU_TITLE,
    TEXTTYPE_TS_MINIGAME_MENU_PRIMARY,
    TEXTTYPE_TS_MINIGAME_MENU_SECONDARY,
    TEXTTYPE_TS_MINIGAME_MENU_TERTIARY,
    TEXTTYPE_TS_MINIGAME_MENU_NOTIFICATION,
    TEXTTYPE_TS_MINIGAME_MENU_GAMERTAG
ENDENUM

ENUM OVERHEADSTATTYPE
    OVERHEADSTATTYPE_EMPTY = 0,
    OVERHEADSTATTYPE_BAR,
    OVERHEADSTATTYPE_ICON,
    OVERHEADSTATTYPE_CUFFEDRANK,
    OVERHEADSTATTYPE_CUFFEDROLE,
    OVERHEADSTATTYPE_FOURRANK,
    OVERHEADSTATTYPE_FOURROLE,
    OVERHEADSTATTYPE_FOURCUFFED,
    OVERHEADSTATTYPE_FOURVOTED,
    OVERHEADSTATTYPE_WANTED,  /////////Added by bobby 12/03/12
    OVERHEADSTATTYPE_CLAN,
    OVERHEADSTATTYPE_PERCENT,
    OVERHEADSTATTYPE_HEATHBAR,
    OVERHEADSTATTYPE_ARROW
ENDENUM

STRUCT TEXT_PLACEMENT
    FLOAT x, y
    #IF IS_DEBUG_BUILD
        STRING strName
        #IF COMPILE_WIDGET_OUTPUT
            BOOL bOutput
            TEXT_WIDGET_ID  widTextName
        #ENDIF
    #ENDIF  
ENDSTRUCT

STRUCT SPRITE_PLACEMENT
    FLOAT x, y
    FLOAT w, h
    INT r,g,b,a
    FLOAT fRotation
    #IF COMPILE_WIDGET_OUTPUT
        BOOL bOutput
        STRING txtSpriteName
        TEXT_WIDGET_ID  widSpriteName
    #ENDIF
    #IF IS_DEBUG_BUILD
        BOOL bDrawTextNameOnscreen
    #ENDIF
ENDSTRUCT

STRUCT RECT
    FLOAT x, y
    FLOAT w, h
    INT r,g,b,a
    #IF COMPILE_WIDGET_OUTPUT
        BOOL bOutput
        STRING txtRectName
        TEXT_WIDGET_ID  widRectName
    #ENDIF
ENDSTRUCT


//  
STRUCT OVERHEADSTRUCT
    SPRITE_PLACEMENT OverHeadSprite[MAX_NUMBER_OF_SPRITES]
    RECT OverHeadRect[MAX_NUM_OF_RECT]
    TEXT_PLACEMENT OverHeadText[MAX_NUM_OF_TEXT]
    INT SpecialtiesBitSet
    BOOL AlphaDirection
    INT AlphaLevel
    INT AlphaLevelLower
    INT Speed
//  INT NumberPlayersChecked
//  FLOAT BestNearbyDrivingSkill
//  FLOAT BestNearbyDrivebySkill
//  FLOAT BestNearbyBadCopSkill
//  FLOAT BestNearbyReliabilitySkill
//  FLOAT BestNearbyShootingSkill
//  FLOAT BestNearbyHackingSkill

//  INT BestDriver
//  INT BestDriveBy
//  INT BestBadCop
//  INT BestReliability
//  INT BestShooting
//  INT BestHacking
//  FLOAT ScalerScale
ENDSTRUCT

ENUM DROPSTYLE
    DROPSTYLE_NONE = 0,
    DROPSTYLE_ALL = 1,
    DROPSTYLE_OUTLINEONLY = 2,
    DROPSTYLE_DROPSHADOWONLY = 3
ENDENUM

STRUCT TEXT_STYLE
    TEXT_FONTS aFont
    FLOAT XScale, YScale
    INT r,g,b,a
    DROPSTYLE drop
    FLOAT WrapStartX, WrapEndX
    STANDARD_TEXTTYPE aTextType
ENDSTRUCT

ENUM HIGHLIGHT_OPTION
    HIGHLIGHT_OPTION_NORMAL = 0,
    HIGHLIGHT_OPTION_NOTSELECTED_ACTIVE,
    HIGHLIGHT_OPTION_SELECTED_ACTIVE,
    HIGHLIGHT_OPTION_SELECTED_WHITE,
    HIGHLIGHT_OPTION_SELECTED_RED,
    HIGHLIGHT_OPTION_EMPTY_SELECTED,
    HIGHLIGHT_OPTION_EMPTY_NOTSELECTED,
    HIGHLIGHT_OPTION_NUMBER_SELECTED,
    HIGHLIGHT_OPTION_NUMBER_NOTSELECTED,
    HIGHLIGHT_OPTION_SELECTED_GREY
ENDENUM

FUNC BOOL IS_STRING_EMPTY_HUD(STRING inStr)
    IF IS_STRING_NULL(inStr)
        RETURN(TRUE)
    ELSE
        IF ARE_STRINGS_EQUAL(inStr, "")
        OR ARE_STRINGS_EQUAL(inStr, "0")
            RETURN(TRUE)
        ENDIF
    ENDIF 
    RETURN(FALSE)
ENDFUNC

FUNC FLOAT FIX_Y(FLOAT fInput)
    RETURN(fInput * 16.0/9.0)   
ENDFUNC

PROC SET_HUD_DRAWING_OFFSET(FLOAT fOffsetX, FLOAT fOffsetY)
    fHudScreenOffsetX = fOffsetX
    fHudScreenOffsetY = fOffsetY
ENDPROC

FUNC FLOAT ADJUST_SCREEN_Y_POSITION(FLOAT fInputY)
    RETURN(fInputY+ fHudScreenOffsetY)
ENDFUNC

FUNC FLOAT ADJUST_SCREEN_X_POSITION(FLOAT fInputX)
    RETURN(fInputX + fHudScreenOffsetX)
ENDFUNC


PROC SET_TEXT_STYLE(TEXT_STYLE &Style, BOOL bDrawBeforeFade = FALSE)


    // choose font
    SET_TEXT_FONT(Style.aFont)
    IF NOT (Style.WrapStartX = 0.0)
    OR NOT (Style.WrapEndX = 0.0)
        SET_TEXT_WRAP(Style.WrapStartX, Style.WrapEndX)
    ENDIF
    SET_TEXT_SCALE(Style.XScale, Style.YScale)
    SET_TEXT_COLOUR(Style.r, Style.g, Style.b, Style.a)
    SWITCH Style.drop
        CASE DROPSTYLE_NONE
        BREAK
        CASE DROPSTYLE_ALL
            SET_TEXT_OUTLINE()
            SET_TEXT_DROP_SHADOW()
        BREAK
        CASE DROPSTYLE_DROPSHADOWONLY
            SET_TEXT_DROP_SHADOW()
        BREAK
        CASE DROPSTYLE_OUTLINEONLY
            SET_TEXT_OUTLINE()
        BREAK
    ENDSWITCH

    IF bDrawBeforeFade
        SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
//      SET_TEXT_DRAW_BEFORE_FADE(TRUE)
    ENDIF
    
ENDPROC

PROC DRAW_TEXT_AND_NUMBERS_WITH_ALIGNMENT(TEXT_PLACEMENT &inText, TEXT_STYLE &Style, STRING inLabel, BOOL bAlignCentre, BOOL bAlignRight, INT iFirstNumber, INT iSecondNumber)
	STRING strLabel 
    strLabel = inLabel
	
	IF NOT IS_STRING_EMPTY_HUD(strLabel)
        IF IS_SAFE_TO_DRAW_ON_SCREEN()
            
            SET_TEXT_STYLE(Style)
            SET_TEXT_RIGHT_JUSTIFY(bAlignRight)
            SET_TEXT_CENTRE(bAlignCentre)
			DISPLAY_TEXT_WITH_2_NUMBERS(ADJUST_SCREEN_X_POSITION(inText.x), ADJUST_SCREEN_Y_POSITION(inText.y), strLabel, iFirstNumber, iSecondNumber)
        ENDIF
    ENDIF
ENDPROC

PROC DRAW_TEXT_WITH_ALIGNMENT(TEXT_PLACEMENT &inText, TEXT_STYLE &Style, STRING inLabel, BOOL bAlignCentre, BOOL bAlignRight)
    STRING strLabel 
    strLabel = inLabel
//  #IF IS_DEBUG_BUILD
//      IF IS_STRING_EMPTY_HUD(inText.strName)
//          inText.strName = GET_STRING_FROM_TEXT_FILE(inLabel)
//      ENDIF
//      #IF COMPILE_WIDGET_OUTPUT
//          IF (inText.bOutput)
//              
//              OPEN_DEBUG_FILE()
//              
//              SAVE_NEWLINE_TO_DEBUG_FILE()
//              SAVE_STRING_TO_DEBUG_FILE("#IF IS_DEBUG_BUILD")
//              SAVE_NEWLINE_TO_DEBUG_FILE()
//              SAVE_STRING_TO_DEBUG_FILE(inText.strName)
//              SAVE_STRING_TO_DEBUG_FILE(".strName = \"")
//              SAVE_STRING_TO_DEBUG_FILE(inText.strName)
//              SAVE_STRING_TO_DEBUG_FILE("\"")
//              SAVE_NEWLINE_TO_DEBUG_FILE()
//              SAVE_STRING_TO_DEBUG_FILE("#endif")
//              SAVE_NEWLINE_TO_DEBUG_FILE()
//              SAVE_STRING_TO_DEBUG_FILE(inText.strName)
//              SAVE_STRING_TO_DEBUG_FILE(".x = ")
//              SAVE_FLOAT_TO_DEBUG_FILE(inText.x)
//              SAVE_NEWLINE_TO_DEBUG_FILE()
//              SAVE_STRING_TO_DEBUG_FILE(inText.strName)
//              SAVE_STRING_TO_DEBUG_FILE(".y = ")
//              SAVE_FLOAT_TO_DEBUG_FILE(inText.y)
//              SAVE_NEWLINE_TO_DEBUG_FILE()
//              
//              CLOSE_DEBUG_FILE()
//              
//              inText.bOutput = FALSE
//          ENDIF
//      #ENDIF
//  #ENDIF          
    
    IF NOT IS_STRING_EMPTY_HUD(strLabel)            
        IF IS_SAFE_TO_DRAW_ON_SCREEN()
            
            SET_TEXT_STYLE(Style)   
            SET_TEXT_RIGHT_JUSTIFY(bAlignRight)
            SET_TEXT_CENTRE(bAlignCentre)   
            DISPLAY_TEXT(ADJUST_SCREEN_X_POSITION(inText.x), ADJUST_SCREEN_Y_POSITION(inText.y), strLabel)
        ENDIF
    ENDIF
ENDPROC

PROC DRAW_TEXT(TEXT_PLACEMENT &inText, TEXT_STYLE &Style, STRING inLabel, BOOL bBeforeHUD = FALSE)
    IF bBeforeHUD
        SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD)
    ENDIF
    DRAW_TEXT_WITH_ALIGNMENT(inText, Style, inLabel, FALSE, FALSE)
ENDPROC

PROC DRAW_TEXT_WITH_KEYBOARD_DISPLAY_AND_INT(TEXT_PLACEMENT &inText, TEXT_STYLE &Style, STRING TextLabel, STRING pShortTextLabel, INT iNumber1, BOOL bBeforeHUD = FALSE)
    IF bBeforeHUD
        SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD)
    ENDIF
    IF NOT IS_STRING_EMPTY_HUD(TextLabel)           
        IF IS_SAFE_TO_DRAW_ON_SCREEN()
            SET_TEXT_STYLE(Style)   
            SET_TEXT_RIGHT_JUSTIFY(FALSE)
            SET_TEXT_CENTRE(FALSE)  
            BEGIN_TEXT_COMMAND_DISPLAY_TEXT(TextLabel)      
                ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY(pShortTextLabel)
                ADD_TEXT_COMPONENT_INTEGER(iNumber1)
            END_TEXT_COMMAND_DISPLAY_TEXT(ADJUST_SCREEN_X_POSITION(inText.x), ADJUST_SCREEN_Y_POSITION(inText.y))
        ENDIF
    ENDIF
ENDPROC

PROC DRAW_TEXT_WITH_KEYBOARD_DISPLAY_AND_TWO_INTS(TEXT_PLACEMENT &inText, TEXT_STYLE &Style, STRING TextLabel, STRING pShortTextLabel, INT iNumber1, INT iNumber2, BOOL bBeforeHUD = FALSE)
    IF bBeforeHUD
        SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD)
    ENDIF
    IF NOT IS_STRING_EMPTY_HUD(TextLabel)           
        IF IS_SAFE_TO_DRAW_ON_SCREEN()
            SET_TEXT_STYLE(Style)   
            SET_TEXT_RIGHT_JUSTIFY(FALSE)
            SET_TEXT_CENTRE(FALSE)  
            BEGIN_TEXT_COMMAND_DISPLAY_TEXT(TextLabel)      
                ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY(pShortTextLabel)
                ADD_TEXT_COMPONENT_INTEGER(iNumber1)
                ADD_TEXT_COMPONENT_INTEGER(iNumber2)
            END_TEXT_COMMAND_DISPLAY_TEXT(ADJUST_SCREEN_X_POSITION(inText.x), ADJUST_SCREEN_Y_POSITION(inText.y))
        ENDIF
    ENDIF
ENDPROC

//PROC DRAW_TEXT_WITH_PLAYER_NAME(TEXT_PLACEMENT &inText, TEXT_STYLE &Style, STRING inPlayerName, HUD_COLOURS aColour = HUD_COLOUR_WHITE, eTextJustification Justifyied = FONT_LEFT)
//
//  IF NOT IS_STRING_EMPTY_HUD(inPlayerName)            
//      IF IS_SAFE_TO_DRAW_ON_SCREEN()
//
//          SET_TEXT_STYLE(Style)   
//          SET_TEXT_JUSTIFICATION(Justifyied)
//          BEGIN_TEXT_COMMAND_DISPLAY_TEXT("STRING")
//              SET_COLOUR_OF_NEXT_TEXT_COMPONENT(aColour)
//              ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(inPlayerName)
//          END_TEXT_COMMAND_DISPLAY_TEXT(inText.x, inText.y)
//      
//      ENDIF
//  ENDIF
//
//ENDPROC

PROC DRAW_TEXT_WITH_PLAYER_NAME(TEXT_PLACEMENT &inText, TEXT_STYLE &Style,STRING inPlayerName,  STRING TextLabel = NULL, HUD_COLOURS aColour = HUD_COLOUR_WHITE, eTextJustification Justifyied = FONT_LEFT)

    IF NOT IS_STRING_EMPTY_HUD(inPlayerName)            
        IF IS_SAFE_TO_DRAW_ON_SCREEN()

            SET_TEXT_STYLE(Style)   
            SET_TEXT_JUSTIFICATION(Justifyied)
            STRING Text  
            IF IS_STRING_EMPTY_HUD(TextLabel)
                Text = "STRING"
            ELSE
                Text = TextLabel
            ENDIF
            BEGIN_TEXT_COMMAND_DISPLAY_TEXT(Text)
                SET_COLOUR_OF_NEXT_TEXT_COMPONENT(aColour)
                ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(inPlayerName)
            END_TEXT_COMMAND_DISPLAY_TEXT(ADJUST_SCREEN_X_POSITION(inText.x), ADJUST_SCREEN_Y_POSITION(inText.y))
        
        ENDIF
    ENDIF

ENDPROC

PROC DRAW_TEXT_WITH_STRING(TEXT_PLACEMENT &inText, TEXT_STYLE &Style, STRING TextLabel, STRING inString, eTextJustification Justifyied = FONT_LEFT)

    IF NOT IS_STRING_EMPTY_HUD(inString)            
        IF IS_SAFE_TO_DRAW_ON_SCREEN()

            SET_TEXT_STYLE(Style)   
            SET_TEXT_JUSTIFICATION(Justifyied)
            
            BEGIN_TEXT_COMMAND_DISPLAY_TEXT(TextLabel)
                ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(inString)
            END_TEXT_COMMAND_DISPLAY_TEXT(ADJUST_SCREEN_X_POSITION(inText.x), ADJUST_SCREEN_Y_POSITION(inText.y))
        
        ENDIF
    ENDIF

ENDPROC

PROC DRAW_TEXT_WITH_GXT_STRING(TEXT_PLACEMENT &inText, TEXT_STYLE &Style, STRING TextLabel, STRING inString, eTextJustification Justifyied = FONT_LEFT)

    IF NOT IS_STRING_EMPTY_HUD(inString)            
        IF IS_SAFE_TO_DRAW_ON_SCREEN()

            SET_TEXT_STYLE(Style)   
            SET_TEXT_JUSTIFICATION(Justifyied)
            
            BEGIN_TEXT_COMMAND_DISPLAY_TEXT(TextLabel)
                ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(inString)
            END_TEXT_COMMAND_DISPLAY_TEXT(ADJUST_SCREEN_X_POSITION(inText.x), ADJUST_SCREEN_Y_POSITION(inText.y))
        
        ENDIF
    ENDIF

ENDPROC

PROC DRAW_TEXT_WITH_STRING_AND_PLAYER_NAME(TEXT_PLACEMENT &inText, TEXT_STYLE &Style, STRING TextLabel, STRING pShortTextLabel, STRING PlayerName, HUD_COLOURS Colour1, HUD_COLOURS Colour2, eTextJustification Justifyied = FONT_LEFT)
    IF NOT IS_STRING_EMPTY_HUD(PlayerName)          
        IF IS_SAFE_TO_DRAW_ON_SCREEN()

            SET_TEXT_STYLE(Style)   
            SET_TEXT_JUSTIFICATION(Justifyied)
            BEGIN_TEXT_COMMAND_DISPLAY_TEXT(TextLabel)      
                IF NOT (Colour1 = HUD_COLOUR_PURE_WHITE)
                    SET_COLOUR_OF_NEXT_TEXT_COMPONENT(Colour1)
                ENDIF
                ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(pShortTextLabel)
                IF NOT (Colour2 = HUD_COLOUR_PURE_WHITE)
                    SET_COLOUR_OF_NEXT_TEXT_COMPONENT(Colour2)
                ENDIF
                ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(PlayerName)
            END_TEXT_COMMAND_DISPLAY_TEXT(ADJUST_SCREEN_X_POSITION(inText.x), ADJUST_SCREEN_Y_POSITION(inText.y))
        ENDIF
    ENDIF
ENDPROC

PROC DRAW_TEXT_WITH_TWO_PLAYER_NAMES(TEXT_PLACEMENT &inText, TEXT_STYLE &Style, STRING TextLabel, STRING PlayerName1, STRING PlayerName2, HUD_COLOURS aColour1 = HUD_COLOUR_WHITE,HUD_COLOURS aColour2 = HUD_COLOUR_WHITE, eTextJustification Justifyied = FONT_LEFT )

    IF NOT IS_STRING_EMPTY_HUD(TextLabel)           
        IF IS_SAFE_TO_DRAW_ON_SCREEN()

            SET_TEXT_STYLE(Style)   
            SET_TEXT_JUSTIFICATION(Justifyied)
            DISPLAY_TEXT_WITH_TWO_PLAYER_NAMES(ADJUST_SCREEN_X_POSITION(inText.x), ADJUST_SCREEN_Y_POSITION(inText.y),TextLabel,  PlayerName1,PlayerName2, aColour1, aColour2 )
        ENDIF
    ENDIF
    
ENDPROC

PROC DRAW_TEXT_WITH_PLAYER_NAME_AND_STRING(TEXT_PLACEMENT &inText, TEXT_STYLE &Style, STRING TextLabel,STRING PlayerName, STRING pShortTextLabel, HUD_COLOURS aColour1 = HUD_COLOUR_WHITE,HUD_COLOURS aColour2 = HUD_COLOUR_WHITE, eTextJustification Justifyied = FONT_LEFT)

    IF NOT IS_STRING_EMPTY_HUD(PlayerName)          
        IF IS_SAFE_TO_DRAW_ON_SCREEN()

            SET_TEXT_STYLE(Style)   
            SET_TEXT_JUSTIFICATION(Justifyied)
            BEGIN_TEXT_COMMAND_DISPLAY_TEXT(TextLabel)      
                IF NOT (aColour1 = HUD_COLOUR_PURE_WHITE)
                    SET_COLOUR_OF_NEXT_TEXT_COMPONENT(aColour1)
                ENDIF
                ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(PlayerName)        
                IF NOT (aColour2 = HUD_COLOUR_PURE_WHITE)
                    SET_COLOUR_OF_NEXT_TEXT_COMPONENT(aColour2)
                ENDIF
                ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(pShortTextLabel)
            END_TEXT_COMMAND_DISPLAY_TEXT(ADJUST_SCREEN_X_POSITION(inText.x), ADJUST_SCREEN_Y_POSITION(inText.y))
        ENDIF
    ENDIF

ENDPROC


PROC DRAW_TEXT_WITH_TEXT(TEXT_PLACEMENT &inText, TEXT_STYLE &Style, STRING inLabel, STRING inLabel2, eTextJustification Justified = FONT_LEFT)
    
    IF NOT IS_STRING_EMPTY_HUD(inLabel)         
        IF IS_SAFE_TO_DRAW_ON_SCREEN()
            SET_TEXT_STYLE(Style)   
            SET_TEXT_JUSTIFICATION(Justified)   
            DISPLAY_TEXT_WITH_STRING(ADJUST_SCREEN_X_POSITION(inText.x), ADJUST_SCREEN_Y_POSITION(inText.y), inLabel, inLabel2)
        ENDIF
    ENDIF
ENDPROC


PROC DRAW_TEXT_WITH_TWO_STRINGS(TEXT_PLACEMENT &inText, TEXT_STYLE &Style, STRING TextLabel, STRING ShortTextLabel2,STRING ShortTextLabel3, eTextJustification Justified = FONT_LEFT)

    IF NOT IS_STRING_EMPTY_HUD(TextLabel)           
        IF IS_SAFE_TO_DRAW_ON_SCREEN()
            SET_TEXT_STYLE(Style)   
            SET_TEXT_JUSTIFICATION(Justified)   
            DISPLAY_TEXT_WITH_TWO_STRINGS(ADJUST_SCREEN_X_POSITION(inText.x), ADJUST_SCREEN_Y_POSITION(inText.y), TextLabel, ShortTextLabel2,ShortTextLabel3 )
        ENDIF
    ENDIF
ENDPROC



PROC DRAW_TEXT_TIMER(TEXT_PLACEMENT &inText, TEXT_STYLE &Style, INT inMillisecondTime, TEXT_COMPONENT_TIME_FORMAT Format, STRING inLabel = NULL, BOOL bAlignCentre = FALSE, BOOL bAlignRight = FALSE)
    STRING strLabel 
//  strLabel = inLabel
    #IF IS_DEBUG_BUILD
//      IF IS_STRING_EMPTY_HUD(inText.strName)
//          inText.strName = GET_STRING_FROM_TEXT_FILE(inLabel)
//      ENDIF
        #IF COMPILE_WIDGET_OUTPUT
            IF (inText.bOutput)
                
                OPEN_DEBUG_FILE()
                
                SAVE_NEWLINE_TO_DEBUG_FILE()
                SAVE_STRING_TO_DEBUG_FILE("#IF IS_DEBUG_BUILD")
                SAVE_NEWLINE_TO_DEBUG_FILE()
                SAVE_STRING_TO_DEBUG_FILE(inText.strName)
                SAVE_STRING_TO_DEBUG_FILE(".strName = \"")
                SAVE_STRING_TO_DEBUG_FILE(inText.strName)
                SAVE_STRING_TO_DEBUG_FILE("\"")
                SAVE_NEWLINE_TO_DEBUG_FILE()
                SAVE_STRING_TO_DEBUG_FILE("#endif")
                SAVE_NEWLINE_TO_DEBUG_FILE()
                SAVE_STRING_TO_DEBUG_FILE(inText.strName)
                SAVE_STRING_TO_DEBUG_FILE(".x = ")
                SAVE_FLOAT_TO_DEBUG_FILE(inText.x)
                SAVE_NEWLINE_TO_DEBUG_FILE()
                SAVE_STRING_TO_DEBUG_FILE(inText.strName)
                SAVE_STRING_TO_DEBUG_FILE(".y = ")
                SAVE_FLOAT_TO_DEBUG_FILE(inText.y)
                SAVE_NEWLINE_TO_DEBUG_FILE()
                
                CLOSE_DEBUG_FILE()
                
                inText.bOutput = FALSE
            ENDIF
        #ENDIF
    #endif          
    
        IF IS_SAFE_TO_DRAW_ON_SCREEN()
            SET_TEXT_STYLE(Style)   
            SET_TEXT_RIGHT_JUSTIFY(bAlignRight)
            SET_TEXT_CENTRE(bAlignCentre)   
//          SET_TEXT_JUSTIFICATION()
            
            IF IS_STRING_EMPTY_HUD(inLabel)
                strLabel = "STRING"
            ELSE
                strLabel = inLabel
            ENDIF
            
            BEGIN_TEXT_COMMAND_DISPLAY_TEXT(strLabel)                      //            ~a~ in american.txt
                ADD_TEXT_COMPONENT_SUBSTRING_TIME(inMillisecondTime, Format)
            END_TEXT_COMMAND_DISPLAY_TEXT(ADJUST_SCREEN_X_POSITION(inText.x), ADJUST_SCREEN_Y_POSITION(inText.y))
    
        ENDIF
ENDPROC

PROC DRAW_TEXT_WITH_2_NUMBERS(TEXT_PLACEMENT &inText, TEXT_STYLE &Style, STRING inLabel, INT Num1, INT Num2, eTextJustification Justified = FONT_LEFT, BOOL bAlignCentre=FALSE)
    STRING strLabel 
    strLabel = inLabel
    #IF IS_DEBUG_BUILD
        IF IS_STRING_EMPTY_HUD(inText.strName)
            inText.strName = GET_STRING_FROM_TEXT_FILE(inLabel)
        ENDIF
        #IF COMPILE_WIDGET_OUTPUT
            IF (inText.bOutput)
                
                OPEN_DEBUG_FILE()
                
                SAVE_NEWLINE_TO_DEBUG_FILE()
                SAVE_STRING_TO_DEBUG_FILE("#IF IS_DEBUG_BUILD")
                SAVE_NEWLINE_TO_DEBUG_FILE()
                SAVE_STRING_TO_DEBUG_FILE(inText.strName)
                SAVE_STRING_TO_DEBUG_FILE(".strName = \"")
                SAVE_STRING_TO_DEBUG_FILE(inText.strName)
                SAVE_STRING_TO_DEBUG_FILE("\"")
                SAVE_NEWLINE_TO_DEBUG_FILE()
                SAVE_STRING_TO_DEBUG_FILE("#endif")
                SAVE_NEWLINE_TO_DEBUG_FILE()
                SAVE_STRING_TO_DEBUG_FILE(inText.strName)
                SAVE_STRING_TO_DEBUG_FILE(".x = ")
                SAVE_FLOAT_TO_DEBUG_FILE(inText.x)
                SAVE_NEWLINE_TO_DEBUG_FILE()
                SAVE_STRING_TO_DEBUG_FILE(inText.strName)
                SAVE_STRING_TO_DEBUG_FILE(".y = ")
                SAVE_FLOAT_TO_DEBUG_FILE(inText.y)
                SAVE_NEWLINE_TO_DEBUG_FILE()
                
                CLOSE_DEBUG_FILE()
                
                inText.bOutput = FALSE
            ENDIF
        #ENDIF
    #endif          
    IF NOT IS_STRING_EMPTY_HUD(strLabel)            
        IF IS_SAFE_TO_DRAW_ON_SCREEN()
            SET_TEXT_STYLE(Style)
            SET_TEXT_CENTRE(bAlignCentre)   
            SET_TEXT_JUSTIFICATION(Justified)
            DISPLAY_TEXT_WITH_2_NUMBERS(ADJUST_SCREEN_X_POSITION(inText.x), ADJUST_SCREEN_Y_POSITION(inText.y), strLabel, Num1, Num2)
        ENDIF
    ENDIF
ENDPROC

PROC DRAW_TEXT_WITH_NUMBER(TEXT_PLACEMENT &inText, TEXT_STYLE &Style, STRING inLabel, INT Num1, eTextJustification Justified = FONT_LEFT)
    STRING strLabel 
    strLabel = inLabel
    #IF IS_DEBUG_BUILD
        IF IS_STRING_EMPTY_HUD(inText.strName)
            inText.strName = GET_STRING_FROM_TEXT_FILE(inLabel)
        ENDIF
        #IF COMPILE_WIDGET_OUTPUT
            IF (inText.bOutput)
                
                OPEN_DEBUG_FILE()
                
                SAVE_NEWLINE_TO_DEBUG_FILE()
                SAVE_STRING_TO_DEBUG_FILE("#IF IS_DEBUG_BUILD")
                SAVE_NEWLINE_TO_DEBUG_FILE()
                SAVE_STRING_TO_DEBUG_FILE(inText.strName)
                SAVE_STRING_TO_DEBUG_FILE(".strName = \"")
                SAVE_STRING_TO_DEBUG_FILE(inText.strName)
                SAVE_STRING_TO_DEBUG_FILE("\"")
                SAVE_NEWLINE_TO_DEBUG_FILE()
                SAVE_STRING_TO_DEBUG_FILE("#endif")
                SAVE_NEWLINE_TO_DEBUG_FILE()
                SAVE_STRING_TO_DEBUG_FILE(inText.strName)
                SAVE_STRING_TO_DEBUG_FILE(".x = ")
                SAVE_FLOAT_TO_DEBUG_FILE(inText.x)
                SAVE_NEWLINE_TO_DEBUG_FILE()
                SAVE_STRING_TO_DEBUG_FILE(inText.strName)
                SAVE_STRING_TO_DEBUG_FILE(".y = ")
                SAVE_FLOAT_TO_DEBUG_FILE(inText.y)
                SAVE_NEWLINE_TO_DEBUG_FILE()
                
                CLOSE_DEBUG_FILE()
                
                inText.bOutput = FALSE
            ENDIF
        #ENDIF
    #endif          
    IF NOT IS_STRING_EMPTY_HUD(strLabel)            
        IF IS_SAFE_TO_DRAW_ON_SCREEN()
            SET_TEXT_STYLE(Style)
//          SET_TEXT_CENTRE(bAlignCentre)   
            SET_TEXT_JUSTIFICATION(Justified)
            DISPLAY_TEXT_WITH_NUMBER(ADJUST_SCREEN_X_POSITION(inText.x), ADJUST_SCREEN_Y_POSITION(inText.y), strLabel, Num1)
        ENDIF
    ENDIF
ENDPROC

PROC DRAW_CASH_WITH_NUMBER(TEXT_PLACEMENT &inText, TEXT_STYLE &Style, STRING inLabel, INT Num, eTextJustification Justified = FONT_LEFT)
    STRING strLabel 
    strLabel = inLabel
    #IF IS_DEBUG_BUILD
        IF IS_STRING_EMPTY_HUD(inText.strName)
            inText.strName = GET_STRING_FROM_TEXT_FILE(inLabel)
        ENDIF
        #IF COMPILE_WIDGET_OUTPUT
            IF (inText.bOutput)
                
                OPEN_DEBUG_FILE()
                
                SAVE_NEWLINE_TO_DEBUG_FILE()
                SAVE_STRING_TO_DEBUG_FILE("#IF IS_DEBUG_BUILD")
                SAVE_NEWLINE_TO_DEBUG_FILE()
                SAVE_STRING_TO_DEBUG_FILE(inText.strName)
                SAVE_STRING_TO_DEBUG_FILE(".strName = \"")
                SAVE_STRING_TO_DEBUG_FILE(inText.strName)
                SAVE_STRING_TO_DEBUG_FILE("\"")
                SAVE_NEWLINE_TO_DEBUG_FILE()
                SAVE_STRING_TO_DEBUG_FILE("#endif")
                SAVE_NEWLINE_TO_DEBUG_FILE()
                SAVE_STRING_TO_DEBUG_FILE(inText.strName)
                SAVE_STRING_TO_DEBUG_FILE(".x = ")
                SAVE_FLOAT_TO_DEBUG_FILE(inText.x)
                SAVE_NEWLINE_TO_DEBUG_FILE()
                SAVE_STRING_TO_DEBUG_FILE(inText.strName)
                SAVE_STRING_TO_DEBUG_FILE(".y = ")
                SAVE_FLOAT_TO_DEBUG_FILE(inText.y)
                SAVE_NEWLINE_TO_DEBUG_FILE()
                
                CLOSE_DEBUG_FILE()
                
                inText.bOutput = FALSE
            ENDIF
        #ENDIF
    #endif          
    

    IF NOT IS_STRING_EMPTY_HUD(strLabel)            
        IF IS_SAFE_TO_DRAW_ON_SCREEN()
            SET_TEXT_STYLE(Style)
//          SET_TEXT_CENTRE(bAlignCentre)   
            SET_TEXT_JUSTIFICATION(Justified)
            BEGIN_TEXT_COMMAND_DISPLAY_TEXT(strLabel)
                ADD_TEXT_COMPONENT_INTEGER(Num)
                ADD_TEXT_COMPONENT_FORMATTED_INTEGER(Num, INTEGER_FORMAT_COMMA_SEPARATORS)
            END_TEXT_COMMAND_DISPLAY_TEXT(ADJUST_SCREEN_X_POSITION(inText.x), ADJUST_SCREEN_Y_POSITION(inText.y))
        ENDIF
    ENDIF
ENDPROC


PROC DRAW_TEXT_WITH_FLOAT(TEXT_PLACEMENT &inText, TEXT_STYLE &Style, STRING inLabel, FLOAT Num1,INT DeclPlaces, eTextJustification Justified = FONT_LEFT)
    
    IF NOT IS_STRING_EMPTY_HUD(inLabel)         
        IF IS_SAFE_TO_DRAW_ON_SCREEN()
            SET_TEXT_STYLE(Style)
            SET_TEXT_JUSTIFICATION(Justified)
            DISPLAY_TEXT_WITH_FLOAT(ADJUST_SCREEN_X_POSITION(inText.x), ADJUST_SCREEN_Y_POSITION(inText.y), inLabel, Num1, DeclPlaces)
        ENDIF
    ENDIF
ENDPROC

PROC DRAW_TEXT_WITH_STRING_AND_TWO_INTS(TEXT_PLACEMENT &inText, TEXT_STYLE &Style, STRING TextLabel, STRING ShortTextLabel, INT Number1, INT Number2, HUD_COLOURS aColour, eTextJustification Justified = FONT_LEFT )
    IF NOT IS_STRING_EMPTY_HUD(TextLabel)           
        IF IS_SAFE_TO_DRAW_ON_SCREEN()
            SET_TEXT_STYLE(Style)
            SET_TEXT_JUSTIFICATION(Justified)
            DISPLAY_TEXT_WITH_STRING_AND_TWO_INTS(ADJUST_SCREEN_X_POSITION(inText.x), ADJUST_SCREEN_Y_POSITION(inText.y),TextLabel, ShortTextLabel,Number1, Number2, aColour  )
        ENDIF
    ENDIF
ENDPROC

PROC DRAW_TEXT_WITH_STRING_AND_INT(TEXT_PLACEMENT &inText, TEXT_STYLE &Style, STRING TextLabel, STRING ShortTextLabel, INT Number1, HUD_COLOURS aColour, eTextJustification Justified = FONT_LEFT)
    IF NOT IS_STRING_EMPTY_HUD(TextLabel)           
        IF IS_SAFE_TO_DRAW_ON_SCREEN()
            SET_TEXT_STYLE(Style)
            SET_TEXT_JUSTIFICATION(Justified)
            DISPLAY_TEXT_WITH_STRING_AND_INT(ADJUST_SCREEN_X_POSITION(inText.x), ADJUST_SCREEN_Y_POSITION(inText.y),TextLabel, ShortTextLabel,Number1, aColour  )
        ENDIF
    ENDIF
ENDPROC

PROC SET_RECTANGLE_ACTIVE_AND_SELECTED(RECT& inRect)
//IF not selected and active = darker colour, full alpha
    inRect.r = 0
    inRect.g = 0
    inRect.b = 0
    inRect.a = 150
ENDPROC

PROC SET_RECTANGLE_PLACEMENT_ACTIVE_NOT_SELECTED(RECT& inRect)
//IF not selected and active = darker colour, full alpha
    inRect.r = inRect.r-50
    inRect.g = inRect.g-50
    inRect.b = inRect.b-50
ENDPROC

PROC SET_RECTANGLE_ACTIVE_AND_SELECTED_WHITE(RECT& inRect)
    inRect.r = 255
    inRect.g = 255
    inRect.b = 255
    inRect.a = 255
ENDPROC

PROC SET_RECTANGLE_ACTIVE_AND_SELECTED_RED(RECT& inRect)
    inRect.r = 200
    inRect.g = 20
    inRect.b = 20
    inRect.a = 204
ENDPROC

PROC SET_RECTANGLE_DARK_BRONZE(RECT& inRect)
    inRect.r = 96
    inRect.g = 56
    inRect.b = 43
    inRect.a = 192
ENDPROC

PROC SET_RECTANGLE_BRONZE(RECT& inRect, BOOL bSelected = TRUE)
    IF bSelected
        inRect.r = 169
        inRect.g = 88
        inRect.b = 74
        inRect.a = 255
    ELSE
        SET_RECTANGLE_DARK_BRONZE(inRect)
    ENDIF
ENDPROC

PROC SET_RECTANGLE_DARK_SILVER(RECT& inRect)
    inRect.r = 100
    inRect.g = 100
    inRect.b = 100
    inRect.a = 192
ENDPROC

PROC SET_RECTANGLE_SILVER(RECT& inRect, BOOL bSelected = TRUE)
    IF bSelected
        inRect.r = 162
        inRect.g = 162
        inRect.b = 162
        inRect.a = 255
    ELSE
        SET_RECTANGLE_DARK_SILVER(inRect)
    ENDIF
ENDPROC

PROC SET_RECTANGLE_DARK_GOLD(RECT& inRect)
    inRect.r = 142
    inRect.g = 113
    inRect.b = 46
    inRect.a = 192
ENDPROC

PROC SET_RECTANGLE_GOLD(RECT& inRect, BOOL bSelected = TRUE)
    IF bSelected
        inRect.r = 245
        inRect.g = 183
        inRect.b = 81
        inRect.a = 255
    ELSE
        SET_RECTANGLE_DARK_GOLD(inRect)
    ENDIF
ENDPROC

PROC DRAW_RECTANGLE(RECT inRect, HIGHLIGHT_OPTION option = HIGHLIGHT_OPTION_NORMAL, BOOL doScaling = FALSE)
    
    RECT BeforeRect = inRect
    SWITCH option
        CASE HIGHLIGHT_OPTION_SELECTED_ACTIVE
            SET_RECTANGLE_ACTIVE_AND_SELECTED(BeforeRect)
        BREAK
        CASE HIGHLIGHT_OPTION_NOTSELECTED_ACTIVE
            SET_RECTANGLE_PLACEMENT_ACTIVE_NOT_SELECTED(BeforeRect)
        BREAK
        CASE HIGHLIGHT_OPTION_SELECTED_WHITE
            SET_RECTANGLE_ACTIVE_AND_SELECTED_WHITE(BeforeRect)
        BREAK
        CASE HIGHLIGHT_OPTION_SELECTED_RED
            SET_RECTANGLE_ACTIVE_AND_SELECTED_RED(BeforeRect)
        BREAK
    ENDSWITCH
    
    IF IS_SAFE_TO_DRAW_ON_SCREEN()
        IF doScaling = TRUE
            DRAW_RECT(ADJUST_SCREEN_X_POSITION(BeforeRect.x), ADJUST_SCREEN_Y_POSITION(BeforeRect.y), BeforeRect.w, BeforeRect.h, BeforeRect.r, BeforeRect.g, BeforeRect.b, BeforeRect.a)
        ELSE
            DRAW_RECT(ADJUST_SCREEN_X_POSITION(BeforeRect.x), ADJUST_SCREEN_Y_POSITION(BeforeRect.y), BeforeRect.w, BeforeRect.h, BeforeRect.r, BeforeRect.g, BeforeRect.b, BeforeRect.a)
        ENDIF
    ENDIF
    
    
ENDPROC






PROC SET_SPRITE_PLACEMENT_EMPTY_NOT_SELECTED(SPRITE_PLACEMENT& aSprite)
//If not selected and not active = Full colour, low alpha
    aSprite.r = 0
    aSprite.g = 0
    aSprite.b = 0
    aSprite.a = 70
ENDPROC

PROC SET_SPRITE_PLACEMENT_EMPTY_AND_SELECTED(SPRITE_PLACEMENT& aSprite)
//If not selected and not active = Full colour, low alpha
    aSprite.r = 0
    aSprite.g = 0
    aSprite.b = 0
    aSprite.a = 100
ENDPROC

PROC SET_SPRITE_PLACEMENT_ACTIVE_NOT_SELECTED(SPRITE_PLACEMENT& aSprite)
//IF not selected and active = darker colour, full alpha
    aSprite.r = aSprite.r-50
    aSprite.g = aSprite.g-50
    aSprite.b = aSprite.b-50
ENDPROC

PROC SET_SPRITE_PLACEMENT_ACTIVE_AND_SELECTED(SPRITE_PLACEMENT& aSprite)
//IF selected and active = Full colour, full alpha
    aSprite.r = aSprite.r
    aSprite.g = aSprite.g
    aSprite.b = aSprite.b
    aSprite.a = aSprite.a
ENDPROC

PROC SET_SPRITE_PLACEMENT_NUMBER_SELECTED(SPRITE_PLACEMENT& aSprite)
//IF selected and active = Full colour, full alpha
    aSprite.a = 5
ENDPROC

PROC SET_SPRITE_PLACEMENT_NUMBER_NOTSELECTED(SPRITE_PLACEMENT& aSprite)
//IF selected and active = Full colour, full alpha
    aSprite.a = 2
ENDPROC

PROC SET_SPRITE_PLACEMENT_HIGHLIGHTED_GREY(SPRITE_PLACEMENT& aSprite)
    aSprite.r = 128
    aSprite.g = 128
    aSprite.b = 128
ENDPROC

PROC SET_SPRITE_PLACEMENT_COLOR(SPRITE_PLACEMENT& aSprite, INT iR, INT iG, INT iB)
    aSprite.r = iR
    aSprite.g = iG
    aSprite.b = iB
ENDPROC


//BOOL InitClanWidget
//FLOAT xShiftClan = -0.03
//FLOAT YStartDetailsSprite = 0.032

FUNC FLOAT GET_SCALER_FROM_DISTANCE(OVERHEADSTRUCT& OverHeadDetails, PED_INDEX aPlayerPed,VEHICLE_INDEX tempveh = NULL, OBJECT_INDEX tempobj = NULL)
    
    VECTOR PlayerPos
    
    IF tempveh != NULL
        IF IS_VEHICLE_DRIVEABLE(tempveh)
            PlayerPos = GET_ENTITY_COORDS(tempveh)
        ENDIF
    ELIF tempobj != NULL
        IF DOES_ENTITY_EXIST(tempobj)
            PlayerPos = GET_ENTITY_COORDS(tempobj)
        ENDIF
    ELSE
        PlayerPos = GET_ENTITY_COORDS(aPlayerPed)
    ENDIF
    
    FLOAT fPedRange = GET_DISTANCE_BETWEEN_COORDS(GET_GAMEPLAY_CAM_COORD(), playerPos)
    IF OverHeadDetails.AlphaDirection = FALSE
    ENDIF
    
    FLOAT DefaultFov = 50.0
    FLOAT DefaultRange =  6 // OverHeadDetails.ScalerScale //4.1165
            
    FLOAT fScalerX = (GET_GAMEPLAY_CAM_FOV() * fPedRange) / (DefaultFov*DefaultRange)
    
    IF fScalerX > 1
        RETURN fScalerX 
    ENDIF
    
    RETURN 1.0 //Don't get bigger

ENDFUNC

//FLOAT YStartDebug
//BOOL InitClanWidget
FUNC SPRITE_PLACEMENT GET_SPRITE_NAME_OFFSET(OVERHEADSTRUCT& OverHeadDetails, PED_INDEX aPlayerPed, SPRITE_PLACEMENT aSprite, INT SlotNo, OVERHEADSTATTYPE statType = OVERHEADSTATTYPE_EMPTY, VEHICLE_INDEX tempveh = NULL, OBJECT_INDEX tempobj = NULL)
    
    SPRITE_PLACEMENT TempSprite = aSprite
    
    FLOAT fScreenX = aSprite.x
    FLOAT fScreenY = aSprite.y
        
    FLOAT SlotOffset 
    FLOAT Scaler
    
//  IF InitClanWidget = FALSE
//      
//      START_WIDGET_GROUP("SPRITE POSITIONING")
////            ADD_WIDGET_FLOAT_SLIDER("CLAN X Shift", xShiftClan, -1, 1, 0.001)
//          
//          ADD_WIDGET_FLOAT_SLIDER("YStartDebug", YStartDebug, -5, 5, 0.001)
//      STOP_WIDGET_GROUP()
//      InitClanWidget = TRUE
//  ENDIF

    IF statType <> OVERHEADSTATTYPE_HEATHBAR
        IF tempveh != NULL
            Scaler = GET_SCALER_FROM_DISTANCE(OverHeadDetails,aPlayerPed, tempveh)      
        ELIF tempobj != NULL
            Scaler = GET_SCALER_FROM_DISTANCE(OverHeadDetails,aPlayerPed, NULL, tempobj)
        ELSE
            Scaler = GET_SCALER_FROM_DISTANCE(OverHeadDetails, aPlayerPed)
        ENDIF
    ENDIF
    
    
    FLOAT NormalDistance = 4.0
    
    FLOAT Camera_Icon_Distance = GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(NETWORK_GET_PLAYER_INDEX_FROM_PED(aPlayerPed)), GET_GAMEPLAY_CAM_COORD())
    
    /////////Changed by bobby 12/03/12
    //This const int is the amount that the SPRITE should be moved down by. 
    CONST_FLOAT cfY_Movement  0.0 //0.032 
    FLOAT YStartDetailsSprite = cfY_Movement
    
    IF Camera_Icon_Distance < NormalDistance 
        FLOAT M = -16.659
        FLOAT B = NormalDistance
        YStartDetailsSprite = ((Camera_Icon_Distance-B)/M) + cfY_Movement
    ENDIF
    
    //YStartDetailsSprite 

    FLOAT YStatStep = 0.023 //0.024 //0.018
//  FLOAT YStartDetailsSprite = 0.032
    FLOAT XShiftBar = 0.015
    FLOAT XShiftStat = -0.015
    FLOAT xShiftClan = -0.03
    FLOAT XShiftCuffedRank = -0.025
    FLOAT XShiftCuffedRole = 0.025
    FLOAT XShiftFourthRank = -0.0125
    FLOAT XShiftFourthRole = 0.0125
    FLOAT XShiftFourthCuffed = 0.035
    FLOAT XShiftFourthVoted = -0.035
    FLOAT XShiftWanted      = 0.0
    FLOAT OldYStatStep = YStatStep
    FLOAT OldYStartDetails = YStartDetailsSprite
    FLOAT OldXShiftBar = XShiftBar
    FLOAT OldXShiftStat = XShiftStat

    IF Scaler > 1
        YStatStep /= Scaler
        YStartDetailsSprite /= Scaler   // -0.332
        XShiftBar /= Scaler
        XShiftStat /= Scaler
    ENDIF

    
    SlotOffset = YStartDetailsSprite - (YStatStep*SlotNo)
    
    fScreenY += SlotOffset
    
    SWITCH statType
        CASE OVERHEADSTATTYPE_BAR
            fScreenX += XShiftBar
        BREAK
        CASE OVERHEADSTATTYPE_ICON
            fScreenX += XShiftStat
//          fScreenY -= 0.001
        BREAK
        CASE OVERHEADSTATTYPE_PERCENT
            fScreenX -= XShiftStat
//          fScreenY -= 0.001
        BREAK
        
        CASE OVERHEADSTATTYPE_CUFFEDRANK
            fScreenX += XShiftCuffedRank
        BREAK
        CASE OVERHEADSTATTYPE_CUFFEDROLE
            fScreenX += XShiftCuffedRole
        BREAK
        
        CASE OVERHEADSTATTYPE_FOURRANK
            fScreenX += XShiftFourthRank
        BREAK
        CASE OVERHEADSTATTYPE_FOURROLE
            fScreenX += XShiftFourthRole
        BREAK
        CASE OVERHEADSTATTYPE_FOURCUFFED
            fScreenX += XShiftFourthCuffed
        BREAK
        CASE OVERHEADSTATTYPE_FOURVOTED
            fScreenX += XShiftFourthVoted
        BREAK
        /////////ADDED by bobby 12/03/12
        CASE OVERHEADSTATTYPE_WANTED
            fScreenX += XShiftWanted 
            fScreenY += -0.047
        BREAK
        ///////
        CASE OVERHEADSTATTYPE_CLAN
            fScreenX += xShiftClan
        BREAK
        CASE OVERHEADSTATTYPE_HEATHBAR
            fScreenY += -0.013
        BREAK
        
    ENDSWITCH

    TempSprite.x = fScreenX
    TempSprite.y = fScreenY
        
    YStatStep = OldYStatStep 
    YStartDetailsSprite = OldYStartDetails 
    XShiftBar = OldXShiftBar 
    XShiftStat = OldXShiftStat 
    
RETURN TempSprite
ENDFUNC


//USING "commands_Hud.sch"
FUNC BOOL SET_ALL_SPRITES_AND_TEXT_OVERHEAD(OVERHEADSTRUCT& OverHeadDetails, PED_INDEX aPlayerPed, OVERHEAD_OFFSETS anOffsetPosition = OO_NONE, VEHICLE_INDEX tempveh = NULL, OBJECT_INDEX tempobj = NULL)
    FLOAT fScreenX, fScreenY
    INT I
    
    FLOAT YAboveHead
//  FLOAT YInChest = 0
    
    FLOAT PlayerHeading
    
    IF anOffsetPosition !=OO_PLAYER_VEHICLE
    AND anOffsetPosition !=OO_OBJECTIVE_VEHICLE
    AND anOffsetPosition !=OO_OBJECTIVE_OBJECT
        PlayerHeading= GET_ENTITY_HEADING(aPlayerPed)
    ELSE
        IF anOffsetPosition =OO_PLAYER_VEHICLE
            IF IS_NET_VEHICLE_DRIVEABLE(PERSONAL_VEHICLE_NET_ID())
                PlayerHeading=GET_ENTITY_HEADING(PERSONAL_VEHICLE_ID())
            ENDIF
        ENDIF
        IF anOffsetPosition =OO_OBJECTIVE_VEHICLE
            IF tempveh != NULL
                IF IS_VEHICLE_DRIVEABLE(tempveh)
                    PlayerHeading=GET_ENTITY_HEADING(tempveh)
                ENDIF
            ENDIF
        ELIF anOffsetPosition = OO_OBJECTIVE_OBJECT
            IF tempobj != NULL
                IF DOES_ENTITY_EXIST(tempobj)   
                    PlayerHeading=GET_ENTITY_HEADING(tempobj)
                ENDIF
            ENDIF
        ENDIF
    ENDIF
    
    FLOAT XInCarOffset
    FLOAT ZInCarOffset
    
    VECTOR PlayerPos
    
//  FLOAT FrontSeatXOffset = 
//  FLOAT FrontSeatYOffset = 
//  FLOAT FrontSeatZOffset = 
    
    SWITCH anOffsetPosition
        CASE OO_NONE
            YAboveHead = 1.2
            XInCarOffset = 0
        BREAK
        CASE OO_DRIVER
            YAboveHead = 1.240 //OverheadOffsetsText[0] //1.540 //2
            XInCarOffset = -0.460 //OverheadOffsetsText[1] //-0.460 //-0.5
            ZInCarOffset = -0.100 //OverheadOffsetsText[2]
        BREAK
        CASE OO_FRONT_RIGHT
            YAboveHead = 1.240 //OverheadOffsetsText[3] 
            XInCarOffset = 0.460 //OverheadOffsetsText[4] 
            ZInCarOffset = -0.100 //OverheadOffsetsText[5]
        BREAK
        CASE OO_BACK_LEFT
            YAboveHead = 1.000 //OverheadOffsetsText[6] 
            XInCarOffset = -0.460 //OverheadOffsetsText[7] 
            ZInCarOffset = -1.000 //OverheadOffsetsText[8]
        BREAK
        CASE OO_BACK_RIGHT
            YAboveHead = 1.000 //OverheadOffsetsText[9] //1.5
            XInCarOffset = 0.460 //OverheadOffsetsText[10] //0.460 //1
            ZInCarOffset = -1.000 //OverheadOffsetsText[11]
        BREAK
        CASE OO_BIKE_DRIVER
            YAboveHead = 1.650 //OverheadOffsetsText[12] //2
            XInCarOffset = 0.0 //OverheadOffsetsText[13] //0
            ZInCarOffset = 0.0 //OverheadOffsetsText[14]
        BREAK
        CASE OO_BIKE_PASSENGER
            YAboveHead = 1.450 //OverheadOffsetsText[15] //1.5
            XInCarOffset = 0.0 //OverheadOffsetsText[16] //0
            ZInCarOffset = -0.500 //OverheadOffsetsText[17]
        BREAK
        
        CASE OO_PLAYER_VEHICLE
            YAboveHead = 0.85
            XInCarOffset = 0.0
            ZInCarOffset = 0.75
            IF tempveh != NULL
                IF IS_NET_VEHICLE_DRIVEABLE(PERSONAL_VEHICLE_NET_ID())
                    VECTOR vMin, vMax
                    GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(PERSONAL_VEHICLE_ID()), vMin, vMax)
                    ZInCarOffset = (vMax.z + 0.75)
                ENDIF
            ENDIF
        BREAK
        
        CASE OO_OBJECTIVE_VEHICLE
            YAboveHead = 0.85
            XInCarOffset = 0.0
            ZInCarOffset = 0.75
            IF tempveh != NULL
                IF IS_VEHICLE_DRIVEABLE(tempveh)
                    VECTOR vMin, vMax
                    GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(tempveh), vMin, vMax)
                    ZInCarOffset = (vMax.z + 0.105)
                ENDIF
            ENDIF
        BREAK
        
        CASE OO_OBJECTIVE_OBJECT
            YAboveHead = 0.0
            XInCarOffset = 0.0
            ZInCarOffset = 0.5
            IF tempobj != NULL
                IF DOES_ENTITY_EXIST(tempobj)
                    VECTOR vMin, vMax
                    GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(tempobj), vMin, vMax)
                    ZInCarOffset = (vMax.z + 0.25)
                ENDIF
            ENDIF
        BREAK
        
    ENDSWITCH 
    IF anOffsetPosition != OO_PLAYER_VEHICLE
    AND anOffsetPosition !=OO_OBJECTIVE_VEHICLE
    AND anOffsetPosition !=OO_OBJECTIVE_OBJECT
        PlayerPos = GET_ENTITY_COORDS(aPlayerPed)
    ELSE
        IF anOffsetPosition = OO_PLAYER_VEHICLE
            IF IS_NET_VEHICLE_DRIVEABLE(PERSONAL_VEHICLE_NET_ID())
                PlayerPos = GET_ENTITY_COORDS(PERSONAL_VEHICLE_ID())
            ENDIF
        ENDIF
        IF anOffsetPosition = OO_OBJECTIVE_VEHICLE
            IF IS_VEHICLE_DRIVEABLE(tempveh)
                PlayerPos = GET_ENTITY_COORDS(tempveh)
            ENDIF
        ENDIF
        IF anOffsetPosition = OO_OBJECTIVE_OBJECT
            IF DOES_ENTITY_EXIST(tempobj)
                PlayerPos = GET_ENTITY_COORDS(tempobj)
            ENDIF
        ENDIF
    ENDIF
//  playerPos.z = playerPos.z + YAboveHead
//  PlayerPos.x = PlayerPos.x+XInCarOffset
    VECTOR PlayerPosOffset 
    IF anOffsetPosition = OO_OBJECTIVE_VEHICLE
        PlayerPosOffset = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(PlayerPos, PlayerHeading, <<XInCarOffset, YAboveHead, ZInCarOffset>>)
    ELIF anOffsetPosition != OO_OBJECTIVE_OBJECT
        PlayerPosOffset = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(PlayerPos, PlayerHeading, <<XInCarOffset,ZInCarOffset, YAboveHead>>)
    ELSE
        PlayerPosOffset = << PlayerPos.x, PlayerPos.y, PlayerPos.z + ZInCarOffset>>
    ENDIF
    IF GET_SCREEN_COORD_FROM_WORLD_COORD(PlayerPosOffset, fScreenX, fScreenY)
    
        SET_DRAW_ORIGIN(PlayerPosOffset)
        
        FOR I = 0 TO 5 //The ones above players head
            OverHeadDetails.OverHeadSprite[I].x = 0.0 // fScreenX
            OverHeadDetails.OverHeadSprite[I].y = 0.0 //fScreenY
            OverHeadDetails.OverHeadText[I].x = 0.0 //fScreenX
            OverHeadDetails.OverHeadText[I].y = 0.0 //fScreenY
        ENDFOR
        RETURN TRUE
    ENDIF
    RETURN FALSE
ENDFUNC





//USING "commands_Hud.sch"
FUNC BOOL SET_ALL_SPRITES_OVERHEAD(OVERHEADSTRUCT& OverHeadDetails, PED_INDEX aPlayerPed, OVERHEAD_OFFSETS anOffsetPosition = OO_NONE)
    FLOAT fScreenX, fScreenY
    INT I
    
//  FLOAT YAboveHead = 1.2
    FLOAT YInChest
    FLOAT XInCarOffset
    FLOAT ZInCarOffset
    FLOAT PlayerHeading = GET_ENTITY_HEADING(aPlayerPed)
    
//  FLOAT FrontSeatXOffset = 
//  FLOAT FrontSeatYOffset = 
//  FLOAT FrontSeatZOffset = 
    
    SWITCH anOffsetPosition
        CASE OO_NONE
            YInChest = 0.2
            XInCarOffset = 0
        BREAK
        CASE OO_DRIVER
            YInChest = 0.640 //OverheadOffsets[0]
            XInCarOffset = -0.460 //OverheadOffsets[1]
            ZInCarOffset = -0.100 //OverheadOffsets[2]
        BREAK
        CASE OO_FRONT_RIGHT
            YInChest = 0.640 //OverheadOffsets[3] //0.940  //1.0
            XInCarOffset = 0.460 //OverheadOffsets[4] //0.460  //0.5
            ZInCarOffset = -0.100 //OverheadOffsets[5]
        BREAK
        CASE OO_BACK_LEFT
            YInChest = 0.400 //OverheadOffsets[6]
            XInCarOffset = -0.460 //OverheadOffsets[7] 
            ZInCarOffset = -1.000 //OverheadOffsets[8]
        BREAK
        CASE OO_BACK_RIGHT
            YInChest = 0.400 //OverheadOffsets[9] //0.440 //0.5
            XInCarOffset = -0.460 //OverheadOffsets[10] //0.460 //1
            ZInCarOffset = -1.000 //OverheadOffsets[11]
        BREAK
        CASE OO_BIKE_DRIVER
            YInChest =  1.100 //OverheadOffsets[12] //0.75
            XInCarOffset = 0.0 //OverheadOffsets[13] //0
            ZInCarOffset = 0.0 //OverheadOffsets[14]
        BREAK
        CASE OO_BIKE_PASSENGER
            YInChest = 0.9 //OverheadOffsets[15] //0.25
            XInCarOffset = 0.0 //OverheadOffsets[16] //0
            ZInCarOffset = -0.500 //OverheadOffsets[17]
        BREAK
        
    ENDSWITCH 
    
    VECTOR PlayerPos = GET_ENTITY_COORDS(aPlayerPed)
//  playerPos.z = playerPos.z + YInChest
//  PlayerPos.x = PlayerPos.x+XInCarOffset
    
    VECTOR PlayerPosOffset = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(PlayerPos, PlayerHeading, <<XInCarOffset,ZInCarOffset, YInChest>>)
    
    
    IF GET_SCREEN_COORD_FROM_WORLD_COORD(PlayerPosOffset, fScreenX, fScreenY)
    
        SET_DRAW_ORIGIN(PlayerPosOffset)
        
        FOR I = 0 TO 5 //The ones above players head
            OverHeadDetails.OverHeadSprite[I].x = 0.0 //fScreenX
            OverHeadDetails.OverHeadSprite[I].y = 0.0 //fScreenY
        ENDFOR
        RETURN TRUE
    ENDIF
    RETURN FALSE
ENDFUNC



FUNC SPRITE_PLACEMENT GET_SPRITE_POSITION_AND_SCALE(OVERHEADSTRUCT& OverHeadDetails , PED_INDEX aPlayerPed, SPRITE_PLACEMENT aSprite, BOOL withXOffset = FALSE,VEHICLE_INDEX tempveh = NULL, OBJECT_INDEX tempobj = NULL)
    
    SPRITE_PLACEMENT TempSprite = aSprite
    FLOAT Scaler 
    
    IF tempveh != NULL
        IF IS_VEHICLE_DRIVEABLE(tempveh)
            Scaler = GET_SCALER_FROM_DISTANCE(OverHeadDetails,aPlayerPed, tempveh)
        ENDIF
    ELIF tempobj != NULL
        IF DOES_ENTITY_EXIST(tempobj)
            Scaler = GET_SCALER_FROM_DISTANCE(OverHeadDetails,aPlayerPed, NULL, tempobj)
        ENDIF
    ELSE
        Scaler = GET_SCALER_FROM_DISTANCE(OverHeadDetails, aPlayerPed)
    ENDIF
    
    IF Scaler > 1
        TempSprite.w = TempSprite.w / Scaler
        TempSprite.h = TempSprite.h / Scaler    
        IF withXOffset 
            TempSprite.x = TempSprite.x * Scaler
        ENDIF
    ENDIF

    RETURN TempSprite

ENDFUNC
//PROC DRAW_2D_SPRITE(STRING pTextureDictionaryName, STRING pSpriteName, SPRITE_PLACEMENT &inSprite, BOOL DoScreenScaling = TRUE, BOOL bHighlight = FALSE, BOOL IsNumbers = FALSE, BOOL isActive = FALSE)
PROC DRAW_2D_SPRITE(STRING pTextureDictionaryName, STRING pSpriteName, SPRITE_PLACEMENT &inSprite, BOOL DoScreenScaling = TRUE, HIGHLIGHT_OPTION option = HIGHLIGHT_OPTION_NORMAL, GFX_DRAW_ORDER gfxDrawOrder = GFX_ORDER_AFTER_HUD, BOOL DoStereorize = FALSE)

    SPRITE_PLACEMENT BeforeSprite = inSprite
    SWITCH option
        CASE HIGHLIGHT_OPTION_NORMAL
            SET_SPRITE_PLACEMENT_ACTIVE_AND_SELECTED(BeforeSprite)
        BREAK
        CASE HIGHLIGHT_OPTION_NOTSELECTED_ACTIVE
            SET_SPRITE_PLACEMENT_ACTIVE_NOT_SELECTED(BeforeSprite)
        BREAK
        CASE HIGHLIGHT_OPTION_EMPTY_SELECTED 
            SET_SPRITE_PLACEMENT_EMPTY_AND_SELECTED(BeforeSprite)
        BREAK
        CASE HIGHLIGHT_OPTION_EMPTY_NOTSELECTED
            SET_SPRITE_PLACEMENT_EMPTY_NOT_SELECTED(BeforeSprite)
        BREAK
        CASE HIGHLIGHT_OPTION_NUMBER_SELECTED
            SET_SPRITE_PLACEMENT_NUMBER_SELECTED(BeforeSprite)
        BREAK
        CASE HIGHLIGHT_OPTION_NUMBER_NOTSELECTED
            SET_SPRITE_PLACEMENT_NUMBER_NOTSELECTED(BeforeSprite)
        BREAK
        CASE HIGHLIGHT_OPTION_SELECTED_GREY
            SET_SPRITE_PLACEMENT_HIGHLIGHTED_GREY(BeforeSprite)
        BREAK
    ENDSWITCH
	
    IF IS_SAFE_TO_DRAW_ON_SCREEN()
        SET_SCRIPT_GFX_DRAW_ORDER(gfxDrawOrder)
        IF DoScreenScaling = TRUE
            DRAW_SPRITE(pTextureDictionaryName, pSpriteName, ADJUST_SCREEN_X_POSITION(BeforeSprite.x), ADJUST_SCREEN_Y_POSITION(BeforeSprite.y), BeforeSprite.w, BeforeSprite.h, BeforeSprite.fRotation, BeforeSprite.r, BeforeSprite.g, BeforeSprite.b, BeforeSprite.a, DoStereorize)
        ELSE    
            DRAW_SPRITE(pTextureDictionaryName, pSpriteName, ADJUST_SCREEN_X_POSITION(BeforeSprite.x), ADJUST_SCREEN_Y_POSITION(BeforeSprite.y), (BeforeSprite.w), (BeforeSprite.h), BeforeSprite.fRotation, BeforeSprite.r, BeforeSprite.g, BeforeSprite.b, BeforeSprite.a, DoStereorize)
        ENDIF
        SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
    ENDIF   
    
    #IF IS_DEBUG_BUILD
        VECTOR vTL, vTR, vBL, vBR
        
        vTL = <<(inSprite.x) - (inSprite.w / 2.0), (inSprite.y) - (inSprite.h / 2.0), 0.0>>
        vTR = <<(inSprite.x) + (inSprite.w / 2.0), (inSprite.y) - (inSprite.h / 2.0), 0.0>>
        vBL = <<(inSprite.x) - (inSprite.w / 2.0), (inSprite.y) + (inSprite.h / 2.0), 0.0>>
        vBR = <<(inSprite.x) + (inSprite.w / 2.0), (inSprite.y) + (inSprite.h / 2.0), 0.0>>
            
        IF (inSprite.bDrawTextNameOnscreen)
            // Pre-pick color
            INT iR, iG, iB
            iR = ROUND(inSprite.x * 255.0)
            iG = ROUND(inSprite.y * 255.0)
            iB = 0
            
            // Draw Rect:
            DRAW_DEBUG_LINE_2D(vTL, vTR, iR, iG, iB)
            DRAW_DEBUG_LINE_2D(vTR, vBR, iR, iG, iB)
            DRAW_DEBUG_LINE_2D(vBL, vBR, iR, iG, iB)
            DRAW_DEBUG_LINE_2D(vTL, vBL, iR, iG, iB)
            
            // Draw name:
            TEXT_LABEL_63 szString = pTextureDictionaryName
            szString += " :: "
            szString += pSpriteName
            
            VECTOR vScrPos = <<(inSprite.x + 0.002) - (inSprite.w / 2.0), (inSprite.y + 0.003) - (inSprite.h / 2.0), 0>>
            IF vScrPos.x < 0.045
                vScrPos.x = 0.045
            ENDIF
            IF vScrPos.y < 0.055
                vScrPos.y = 0.055
            ENDIF
            DRAW_DEBUG_TEXT_2D(szString, vScrPos, 255, 255, 255)
        ENDIF
        
        // See if we clicked it.
        IF IS_MOUSE_BUTTON_JUST_PRESSED(MB_LEFT_BTN)
            FLOAT fMX, fMY
            GET_MOUSE_POSITION(fMX, fMY)
            
            IF (fMX > vTL.x) AND (fMX < vTR.x) AND (fMY > vTL.y) AND (fMY < vBL.y)
                PRINTLN("Mouse-click detected on sprite - X Pos: ", fMX, "  Y Pos: ", fMY)

                //Debug text appearing on screen when clicking the left mouse button was bugged in #2233826 as a B class even though this is a debug build only feature.
                //I've removed the inSprite line below. Feel free to put this back in if you need it and update the bug. Ta. Steve T 12.02.15
                //inSprite.bDrawTextNameOnscreen = (NOT inSprite.bDrawTextNameOnscreen) 
            
            ENDIF
        ENDIF
    #ENDIF
ENDPROC

PROC DRAW_THE_SCALEFORM_MOVIE(SCALEFORM_INDEX aMovie, SPRITE_PLACEMENT &inSprite)
    DRAW_SCALEFORM_MOVIE(aMovie, inSprite.x,inSprite.y,inSprite.w,inSprite.h,inSprite.r,inSprite.g,inSprite.b,inSprite.a)
ENDPROC

PROC DRAW_THE_SCALEFORM_MOVIE_FULL_SCREEN(SCALEFORM_INDEX aMovie, SPRITE_PLACEMENT &inSprite)
//  DRAW_SCALEFORM_MOVIE_FULLSCREEN(aMovie, inSprite.x,inSprite.y,inSprite.w,inSprite.h,inSprite.r,inSprite.g,inSprite.b,inSprite.a)
    DRAW_SCALEFORM_MOVIE_FULLSCREEN(aMovie, inSprite.r,inSprite.g,inSprite.b,inSprite.a)
ENDPROC

PROC SET_RECT_TO_TEAM_DM_COLOUR(PLAYER_INDEX PlayerID, RECT &inRect)
    INT r,g,b
    GET_TEAM_DEATHMATCH_RGB_COLOUR(PlayerID, r,g,b)
    inRect.r = r
    inRect.g = g
    inRect.b = b
ENDPROC

PROC SET_RECT_TO_TEAM_COLOUR(RECT &inRect, INT iTeam)
    INT r,g,b
    GET_TEAM_RGB_COLOUR(iTeam, r,g,b)
    inRect.r = r
    inRect.g = g
    inRect.b = b
ENDPROC

FUNC RECT RETURN_RECT_TO_TEAM_COLOUR(RECT inRect, INT iTeam)
    RECT TempRect = inRect
    INT r,g,b
    GET_TEAM_RGB_COLOUR(iTeam, r,g,b)
    TempRect.r = r
    TempRect.g = g
    TempRect.b = b
    RETURN TempRect
ENDFUNC

PROC SET_SPRITE_TO_TEAM_COLOUR(SPRITE_PLACEMENT &inSprite, INT iTeam)
    INT r,g,b
    GET_TEAM_RGB_COLOUR(iTeam, r,g,b)
    inSprite.r = r
    inSprite.g = g
    inSprite.b = b
ENDPROC



FUNC HUD_COLOURS GET_CONTRAST_TEAM_COLOUR(INT iTeam)

    SWITCH iTeam
        CASE TEAM_FREEMODE
            RETURN HUD_COLOUR_FREEMODE_DARK
        BREAK
    ENDSWITCH
    RETURN HUD_COLOUR_WHITE
ENDFUNC

FUNC HUD_COLOURS GET_TEAM_COLOUR(INT iTeam)

    SWITCH iTeam

        CASE TEAM_FREEMODE
            RETURN HUD_COLOUR_FREEMODE
        BREAK
    ENDSWITCH
    RETURN HUD_COLOUR_WHITE
ENDFUNC


FUNC HUD_COLOURS GET_BAR_CONTRAST_COLOUR(HUD_COLOURS MainColour, BOOL bUseBlackForContrastTeamColour = TRUE)



        IF MainColour = GET_TEAM_COLOUR(0) //This needs to change to Team Colour
            IF bUseBlackForContrastTeamColour
                RETURN HUD_COLOUR_BLACK //Replace with net player contrast
            ELSE
                RETURN HUD_COLOUR_NET_PLAYER1_DARK
            ENDIF
        ENDIF
        IF MainColour = GET_TEAM_COLOUR(1)
            IF bUseBlackForContrastTeamColour
                RETURN HUD_COLOUR_BLACK //Replace with net player contrast
            ELSE
                RETURN HUD_COLOUR_NET_PLAYER2_DARK
            ENDIF
        ENDIF
        IF MainColour = GET_TEAM_COLOUR(2)
            IF bUseBlackForContrastTeamColour
                RETURN HUD_COLOUR_BLACK //Replace with net player contrast
            ELSE
                RETURN HUD_COLOUR_NET_PLAYER3_DARK
            ENDIF
        ENDIF
        IF MainColour = HUD_COLOUR_RED
            RETURN HUD_COLOUR_BLACK
        ENDIF
        IF MainColour = HUD_COLOUR_YELLOW
            RETURN HUD_COLOUR_YELLOWDARK
        ENDIF
        IF MainColour = HUD_COLOUR_BLUE
            RETURN HUD_COLOUR_BLACK
        ENDIF
        IF MainColour = HUD_COLOUR_GREEN
            RETURN HUD_COLOUR_GREENDARK
        ENDIF
        IF MainColour = HUD_COLOUR_WHITE
            RETURN HUD_COLOUR_BLACK
        ENDIF
        IF MainColour = HUD_COLOUR_GREY
            RETURN HUD_COLOUR_BLACK
        ENDIF
        IF MainColour = HUD_COLOUR_PURE_WHITE
            RETURN HUD_COLOUR_WHITE
        ENDIF
        IF MainColour = HUD_COLOUR_ORANGE
            RETURN HUD_COLOUR_BLACK
        ENDIF
        IF MainColour = HUD_COLOUR_PURPLE
            RETURN HUD_COLOUR_PURPLEDARK
        ENDIF
        
        IF MainColour = HUD_COLOUR_NET_PLAYER1
            RETURN HUD_COLOUR_NET_PLAYER1_DARK
        ENDIF
        IF MainColour = HUD_COLOUR_NET_PLAYER2
            RETURN HUD_COLOUR_NET_PLAYER2_DARK
        ENDIF
        IF MainColour = HUD_COLOUR_NET_PLAYER3
            RETURN HUD_COLOUR_NET_PLAYER3_DARK
        ENDIF
        IF MainColour = HUD_COLOUR_NET_PLAYER4
            RETURN HUD_COLOUR_NET_PLAYER4_DARK
        ENDIF
        IF MainColour = HUD_COLOUR_NET_PLAYER5
            RETURN HUD_COLOUR_NET_PLAYER5_DARK
        ENDIF
        IF MainColour = HUD_COLOUR_NET_PLAYER6
            RETURN HUD_COLOUR_NET_PLAYER6_DARK
        ENDIF
        IF MainColour = HUD_COLOUR_NET_PLAYER7
            RETURN HUD_COLOUR_NET_PLAYER7_DARK
        ENDIF
        IF MainColour = HUD_COLOUR_NET_PLAYER8
            RETURN HUD_COLOUR_NET_PLAYER8_DARK
        ENDIF
        IF MainColour = HUD_COLOUR_NET_PLAYER9
            RETURN HUD_COLOUR_NET_PLAYER9_DARK
        ENDIF
        IF MainColour = HUD_COLOUR_NET_PLAYER10
            RETURN HUD_COLOUR_NET_PLAYER10_DARK
        ENDIF
        IF MainColour = HUD_COLOUR_NET_PLAYER11
            RETURN HUD_COLOUR_NET_PLAYER11_DARK
        ENDIF
        IF MainColour = HUD_COLOUR_NET_PLAYER12
            RETURN HUD_COLOUR_NET_PLAYER12_DARK
        ENDIF
        IF MainColour = HUD_COLOUR_NET_PLAYER13
            RETURN HUD_COLOUR_NET_PLAYER13_DARK
        ENDIF
        IF MainColour = HUD_COLOUR_NET_PLAYER14
            RETURN HUD_COLOUR_NET_PLAYER14_DARK
        ENDIF
        IF MainColour = HUD_COLOUR_NET_PLAYER15
            RETURN HUD_COLOUR_NET_PLAYER15_DARK
        ENDIF
        IF MainColour = HUD_COLOUR_NET_PLAYER16
            RETURN HUD_COLOUR_NET_PLAYER16_DARK
        ENDIF
        IF MainColour = HUD_COLOUR_NET_PLAYER17
            RETURN HUD_COLOUR_NET_PLAYER17_DARK
        ENDIF
        IF MainColour = HUD_COLOUR_NET_PLAYER18
            RETURN HUD_COLOUR_NET_PLAYER18_DARK
        ENDIF
        IF MainColour = HUD_COLOUR_NET_PLAYER19
            RETURN HUD_COLOUR_NET_PLAYER19_DARK
        ENDIF
        IF MainColour = HUD_COLOUR_NET_PLAYER20
            RETURN HUD_COLOUR_NET_PLAYER20_DARK
        ENDIF
        IF MainColour = HUD_COLOUR_NET_PLAYER21
            RETURN HUD_COLOUR_NET_PLAYER21_DARK
        ENDIF
        IF MainColour = HUD_COLOUR_NET_PLAYER22
            RETURN HUD_COLOUR_NET_PLAYER22_DARK
        ENDIF
        IF MainColour = HUD_COLOUR_NET_PLAYER23
            RETURN HUD_COLOUR_NET_PLAYER23_DARK
        ENDIF
        IF MainColour = HUD_COLOUR_NET_PLAYER24
            RETURN HUD_COLOUR_NET_PLAYER24_DARK
        ENDIF
        IF MainColour = HUD_COLOUR_NET_PLAYER25
            RETURN HUD_COLOUR_NET_PLAYER25_DARK
        ENDIF
        IF MainColour = HUD_COLOUR_NET_PLAYER26
            RETURN HUD_COLOUR_NET_PLAYER26_DARK
        ENDIF
        IF MainColour = HUD_COLOUR_NET_PLAYER27
            RETURN HUD_COLOUR_NET_PLAYER27_DARK
        ENDIF
        IF MainColour = HUD_COLOUR_NET_PLAYER28
            RETURN HUD_COLOUR_NET_PLAYER28_DARK
        ENDIF
        IF MainColour = HUD_COLOUR_NET_PLAYER29
            RETURN HUD_COLOUR_NET_PLAYER29_DARK
        ENDIF
        IF MainColour = HUD_COLOUR_NET_PLAYER30
            RETURN HUD_COLOUR_NET_PLAYER30_DARK
        ENDIF
        IF MainColour = HUD_COLOUR_NET_PLAYER31
            RETURN HUD_COLOUR_NET_PLAYER31_DARK
        ENDIF
        IF MainColour = HUD_COLOUR_NET_PLAYER32
            RETURN HUD_COLOUR_NET_PLAYER32_DARK
        ENDIF
    
    RETURN HUD_COLOUR_BLACK

ENDFUNC

//FUNC HUD_COLOURS GET_DARK_COLOUR(HUD_COLOURS aColour)
//
//  
//
//  IF aColour = HUD_COLOUR_RED
//      RETURN HUD_COLOUR_BLACK
//  ENDIF
//  IF aColour = HUD_COLOUR_BLUE
//      RETURN HUD_COLOUR_NET_PLAYER1_DARK
//  ENDIF
//  IF aColour = HUD_COLOUR_YELLOW
//      RETURN HUD_COLOUR_YELLOWDARK
//  ENDIF
//  IF aColour = HUD_COLOUR_GREEN
//      RETURN HUD_COLOUR_GREENDARK
//  ENDIF
//  IF aColour = HUD_COLOUR_WHITE
//      RETURN HUD_COLOUR_BLACK
//  ENDIF
//  IF aColour = HUD_COLOUR_PURPLE
//      RETURN HUD_COLOUR_PURPLEDARK
//  ENDIF
//  IF aColour = HUD_COLOUR_GREY
//      RETURN HUD_COLOUR_NET_PLAYER3_DARK
//  ENDIF
//  IF aColour = HUD_COLOUR_ORANGE
//      RETURN HUD_COLOUR_NET_PLAYER2_DARK
//  ENDIF 
//
//  RETURN HUD_COLOUR_BLACK
//ENDFUNC



PROC SET_SPRITE_TO_DARK_TEAM_COLOUR(SPRITE_PLACEMENT &inSprite, INT iTeam, BOOL bUseBlackForContrastTeamColour = TRUE )
    HUD_COLOURS MainColour = GET_TEAM_HUD_COLOUR(iTeam)
    HUD_COLOURS DarkColour = GET_BAR_CONTRAST_COLOUR(MainColour, bUseBlackForContrastTeamColour)
    INT r,g,b,a
    GET_HUD_COLOUR(DarkColour, r,g,b,a)
    inSprite.r = r
    inSprite.g = g
    inSprite.b = b
ENDPROC


PROC SET_SPRITE_TO_CONTRAST_TEAM_COLOUR(SPRITE_PLACEMENT &inSprite, INT iTeam)
     
    HUD_COLOURS MainColour = GET_TEAM_HUD_COLOUR(iTeam)
    HUD_COLOURS DarkColour = GET_BAR_CONTRAST_COLOUR(MainColour)
    INT r,g,b,a
    GET_HUD_COLOUR(DarkColour, r,g,b,a)
    inSprite.r = r
    inSprite.g = g
    inSprite.b = b
ENDPROC

PROC GET_TEAM_CONTRAST_COLOUR(INT iTeam, INT& R, INT& G, INT& B)
    HUD_COLOURS MainColour = GET_TEAM_HUD_COLOUR(iTeam)
    HUD_COLOURS DarkColour = GET_BAR_CONTRAST_COLOUR(MainColour)
    INT a
    GET_HUD_COLOUR(DarkColour, r,g,b,a)
ENDPROC


PROC SET_RECT_TO_PLAYER_COLOUR(RECT &inRect)
    INT r,g,b
    GET_PLAYER_CURRENT_COLOUR(PLAYER_ID(), r,g,b)
    inRect.r = r
    inRect.g = g
    inRect.b = b    
ENDPROC

PROC SET_RECT_TO_DARK_PLAYER_COLOUR(RECT &inRect, HUD_COLOURS MainColour, BOOL bUseBlackForContrastTeamColour = TRUE)
    
    //HUD_COLOURS MainColour = GET_TEAM_HUD_COLOUR(iTeam)
    HUD_COLOURS DarkColour = GET_BAR_CONTRAST_COLOUR(MainColour, bUseBlackForContrastTeamColour)
    INT r,g,b,a
    GET_HUD_COLOUR(DarkColour, r,g,b,a)
    inRect.r = r
    inRect.g = g
    inRect.b = b
ENDPROC


PROC SET_RECT_TO_NORMAL_COLOUR(RECT& aRect)
    aRect.r = 0
    aRect.g = 0
    aRect.b = 0
    aRect.a = 128
ENDPROC

PROC SET_RECT_TO_HUD_COLOUR(PLAYER_INDEX player, RECT &inRect)
    INT r,g,b
    GET_TEAM_RGB_COLOUR(NATIVE_TO_INT(player), r,g,b)  
    inRect.r = r
    inRect.g = g
    inRect.b = b    
ENDPROC

PROC SET_RECT_TO_THIS_HUD_COLOUR(RECT &inRect, HUD_COLOURS aColour, BOOL bSetAlpha = FALSE)
    INT Red, Green, Blue, alpha_value
    GET_HUD_COLOUR(aColour, Red, Green, Blue, alpha_value)
    inRect.r = Red
    inRect.g = Green
    inRect.b = Blue
    IF bSetAlpha
        inRect.a = alpha_value
    ENDIF
ENDPROC

PROC SET_TEXT_COLOUR_WITH_HUD_COLOUR(TEXT_STYLE& aTextStyle, HUD_COLOURS eColor)
    INT iR, iG, iB, iA
    GET_HUD_COLOUR(eColor, iR, iG, iB, iA)
    aTextStyle.r = iR
    aTextStyle.g = iG
    aTextStyle.b = iB
ENDPROC

PROC SET_TEXT_TO_GANG_COLOUR(TEXT_STYLE& aTextStyle, INT iTeam)
    INT r, g, b
    GET_TEAM_RGB_COLOUR(iTeam, r, g, b)
    aTextStyle.r = r
    aTextStyle.g = g
    aTextStyle.b = b
ENDPROC

PROC SET_TEXT_BLACK(TEXT_STYLE& aTextStyle) 
    aTextStyle.r = 0
    aTextStyle.g = 0
    aTextStyle.b = 0    
    SET_TEXT_COLOUR_WITH_HUD_COLOUR(aTextStyle, HUD_COLOUR_BLACK)

ENDPROC

PROC SET_TEXT_WHITE(TEXT_STYLE& aTextStyle)
//  aTextStyle.r = 225
//  aTextStyle.g = 225
//  aTextStyle.b = 225
    SET_TEXT_COLOUR_WITH_HUD_COLOUR(aTextStyle, HUD_COLOUR_WHITE)

ENDPROC

PROC SET_TEXT_GREY(TEXT_STYLE& aTextStyle)
//  aTextStyle.r = 127
//  aTextStyle.g = 127
//  aTextStyle.b = 127
    SET_TEXT_COLOUR_WITH_HUD_COLOUR(aTextStyle, HUD_COLOUR_GREY)

ENDPROC

PROC SET_TEXT_DARK_GREY(TEXT_STYLE& aTextStyle)
//  aTextStyle.r = 102
//  aTextStyle.g = 102
//  aTextStyle.b = 102
    SET_TEXT_COLOUR_WITH_HUD_COLOUR(aTextStyle, HUD_COLOUR_GREYDARK)

ENDPROC

PROC SET_TEXT_BRONZE(TEXT_STYLE& aTextStyle)
//  aTextStyle.r = 180 //BC: 02/03/2012 updated with new RGB levels. MARCH 2, 2012. (Using longhand helps us backwards Americans. :p)
//  aTextStyle.g = 130
//  aTextStyle.b = 97
    SET_TEXT_COLOUR_WITH_HUD_COLOUR(aTextStyle, HUD_COLOUR_BRONZE)
ENDPROC

PROC SET_TEXT_SILVER(TEXT_STYLE& aTextStyle)
//  aTextStyle.r = 160 //BC: 02/03/2012 updated with new RGB levels
//  aTextStyle.g = 160
//  aTextStyle.b = 160
    SET_TEXT_COLOUR_WITH_HUD_COLOUR(aTextStyle, HUD_COLOUR_SILVER)
ENDPROC

PROC SET_TEXT_GOLD(TEXT_STYLE& aTextStyle)
//  aTextStyle.r = 245 //BC: 02/03/2012 updated with new RGB levels
//  aTextStyle.g = 183
//  aTextStyle.b = 81
    SET_TEXT_COLOUR_WITH_HUD_COLOUR(aTextStyle, HUD_COLOUR_GOLD)
ENDPROC

PROC SET_TEXT_PLATINUM(TEXT_STYLE& aTextStyle)
//  aTextStyle.r = 166 //BC: 02/03/2012 updated with new RGB levels
//  aTextStyle.g = 221
//  aTextStyle.b = 190
    SET_TEXT_COLOUR_WITH_HUD_COLOUR(aTextStyle, HUD_COLOUR_PLATINUM)
ENDPROC

PROC SET_TEXT_DARK_RED(TEXT_STYLE& aTextStyle)
//  aTextStyle.r = 125
//  aTextStyle.g = 50
//  aTextStyle.b = 50
    SET_TEXT_COLOUR_WITH_HUD_COLOUR(aTextStyle, HUD_COLOUR_REDDARK)
ENDPROC


PROC SET_TEXT_GREEN(TEXT_STYLE& aTextStyle)
//  aTextStyle.r = 20
//  aTextStyle.g = 120
//  aTextStyle.b = 20
    SET_TEXT_COLOUR_WITH_HUD_COLOUR(aTextStyle, HUD_COLOUR_GREEN)

ENDPROC

PROC SET_TEXT_RED(TEXT_STYLE& aTextStyle)
//  aTextStyle.r = 120
//  aTextStyle.g = 20
//  aTextStyle.b = 20
    SET_TEXT_COLOUR_WITH_HUD_COLOUR(aTextStyle, HUD_COLOUR_RED)

ENDPROC



PROC SET_TEXT_STYLE_FONT(TEXT_STYLE& aTextStyle, TEXT_FONTS aFont)
    aTextStyle.aFont = aFont
ENDPROC

FUNC TEXT_STYLE TURN_TEXT_STYLE_GANGCOLOUR(TEXT_STYLE aTextStyle, INT iTeam)
        TEXT_STYLE NewTextStyle = aTextStyle
        INT r,g,b
        GET_TEAM_RGB_COLOUR(iTeam, r,g,b)
        NewTextStyle.r = r
        NewTextStyle.g = g
        NewTextStyle.b = b
        
    RETURN NewTextStyle
ENDFUNC

PROC SET_TEXT_STANDARD(TEXT_STYLE &aTextStyle)
    aTextStyle.aFont = FONT_STANDARD
ENDPROC


PROC SET_TEXT_CURSIVE(TEXT_STYLE &aTextStyle)
    aTextStyle.aFont = FONT_CURSIVE
ENDPROC

PROC SET_RECT_DIMMED(RECT &inRect)
    inRect.r = 0
    inRect.g = 0
    inRect.b = 0
    inRect.a = 55
ENDPROC

PROC SET_RECT_UNDIMMED(RECT &inRect)
    inRect.r = 0
    inRect.g = 0
    inRect.b = 0
    inRect.a = 128
ENDPROC

PROC SET_RECT_SELECTED_DIMMED(RECT& inRect)
//IF not selected and active = darker colour, full alpha
    inRect.r = 255
    inRect.g = 255
    inRect.b = 255
    inRect.a = 75
ENDPROC

PROC SET_SPRITE_BLACK(SPRITE_PLACEMENT &thisSprite)
    thisSprite.r = 0
    thisSprite.g = 0
    thisSprite.b = 0
    thisSprite.a = 255
ENDPROC

PROC SET_SPRITE_GREY(SPRITE_PLACEMENT &thisSprite)
    thisSprite.r = 127
    thisSprite.g = 127
    thisSprite.b = 127
    thisSprite.a = 255
ENDPROC

PROC SET_SPRITE_DARK_GREY(SPRITE_PLACEMENT &thisSprite)
    thisSprite.r = 102
    thisSprite.g = 102
    thisSprite.b = 102
    thisSprite.a = 255
ENDPROC

PROC SET_SPRITE_WHITE(SPRITE_PLACEMENT &thisSprite, BOOL SetFullAlpha = TRUE)
    thisSprite.r = 255
    thisSprite.g = 255
    thisSprite.b = 255
    IF SetFullAlpha = TRUE
        thisSprite.a = 255
    ENDIF
ENDPROC

PROC SET_SPRITE_HUD_COLOUR(SPRITE_PLACEMENT &thisSprite, HUD_COLOURS aColour)
    INT Red, Green, Blue, alpha_value
    GET_HUD_COLOUR(aColour, Red, Green, Blue, alpha_value)
    thisSprite.r = Red
    thisSprite.g = Green
    thisSprite.b = Blue
    
//  thisSprite.a = alpha_value
ENDPROC

PROC SET_SPRITE_GOLD(SPRITE_PLACEMENT &thisSprite)
//  thisSprite.r = 245
//  thisSprite.g = 183
//  thisSprite.b = 81
    SET_SPRITE_HUD_COLOUR(thisSprite, HUD_COLOUR_GOLD)
ENDPROC 

PROC SET_SPRITE_SILVER(SPRITE_PLACEMENT &thisSprite)
//  thisSprite.r = 162
//  thisSprite.g = 162
//  thisSprite.b = 162
    SET_SPRITE_HUD_COLOUR(thisSprite, HUD_COLOUR_SILVER)
ENDPROC 

PROC SET_SPRITE_BRONZE(SPRITE_PLACEMENT &thisSprite)
//  thisSprite.r = 169
//  thisSprite.g = 88
//  thisSprite.b = 74
    SET_SPRITE_HUD_COLOUR(thisSprite, HUD_COLOUR_BRONZE)
ENDPROC 

PROC SET_SPRITE_PLATINUM(SPRITE_PLACEMENT &thisSprite)
//  thisSprite.r = 166
//  thisSprite.g = 221
//  thisSprite.b = 190
    SET_SPRITE_HUD_COLOUR(thisSprite, HUD_COLOUR_PLATINUM)
    
ENDPROC 

/// PURPOSE:
///    Given an original placement and a string, returns a placement to use to put an unlock icon at the end of the string.
///    Added by Ryan Paradis
/// PARAMS:
///    sOrigPlacementInfo - copies some data from this into the returned placement (color, width, height, alpha, scale, etc)
///    sTextPlacement - The placement for the line you're trying to place the sprite after.
///    szLineBeforeSprite - the string table line you're trying to place the sprite after.
/// RETURNS:
///    The new SPRITE_PLACEMENT where you should be drawing the sprite.
FUNC SPRITE_PLACEMENT GET_SPRITE_PLACEMENT_AFTER_TEXT(SPRITE_PLACEMENT & sOrigPlacementInfo, TEXT_PLACEMENT & sTextPlacement, STRING szLineBeforeSprite)
    SPRITE_PLACEMENT sRetVal
    
    // Copy the original info so we get color, height, scaling, etc.
    sRetVal = sOrigPlacementInfo
    
    // Move the new sprite placement to the start of the text placement.
    sRetVal.x = sTextPlacement.x
    
    // Shift the sprite out by some magical formula.
    // I can't explain why the numbers below work. I used a widget to figure out that the 0.328 will put the sprite over the last letter of the
    // word, and then add 0.011 to the placement to shift it over a bit. It just works (TM).
    sRetVal.x += (GET_STRING_WIDTH(szLineBeforeSprite) * 0.328) + 0.011
    
    RETURN sRetVal
ENDFUNC

/// PURPOSE:
///    Given an original placement and a string, returns a placement to use to put an unlock icon at the start of the string.
///    Added by Rob Pearsall
/// PARAMS:
///    sOrigPlacementInfo - copies some data from this into the returned placement (color, width, height, alpha, scale, etc)
///    sTextPlacement - The placement for the line you're trying to place the sprite after.
///    fBeforeOffset - Offset for some padding before the string
/// RETURNS:
///    The new SPRITE_PLACEMENT where you should be drawing the sprite.
FUNC SPRITE_PLACEMENT GET_SPRITE_PLACEMENT_BEFORE_TEXT(SPRITE_PLACEMENT & sOrigPlacementInfo, TEXT_PLACEMENT & sTextPlacement, FLOAT fBeforeOffset)
    SPRITE_PLACEMENT sRetVal
    
    // Copy the original info so we get color, height, scaling, etc.
    sRetVal = sOrigPlacementInfo
    
    // Move the new sprite placement to the start of the text placement.
    sRetVal.x = sTextPlacement.x + fBeforeOffset
    
    RETURN sRetVal
ENDFUNC

PROC SET_TEXT_TABBED(TEXT_PLACEMENT &thisText)
    thisText.x += 0.01
ENDPROC

PROC SET_TEXT_UNTABBED(TEXT_PLACEMENT &thisText)
    thisText.x -= 0.01
ENDPROC

PROC SET_TEXT_WRAPPED_TO_RECT(TEXT_STYLE& thisTextStyle, RECT& thisRect, FLOAT fPaddingLeft = 0.0, FLOAT fPaddingRight = 0.0)
    thisTextStyle.WrapStartX = thisRect.x - (thisRect.w / 2)
    thisTextStyle.WrapEndX = thisRect.x + (thisRect.w / 2)
    
    thisTextStyle.WrapStartX += fPaddingLeft
    thisTextStyle.WrapEndX -= fPaddingRight
ENDPROC

PROC CLEAR_TEXT_WRAPPED(TEXT_STYLE& thisTextStyle)
    thisTextStyle.WrapStartX = 0
    thisTextStyle.WrapEndX = 0
ENDPROC

//PROC SET_TEXT_WRAPPED_PADDING(TEXT_STYLE& thisTextStyle, FLOAT fPaddingFactor = 1.0)
//  IF (fPaddingFactor > 0.01)
//      thisTextStyle.WrapStartX = thisTextStyle.WrapStartX + (0.0035 * fPaddingFactor)
//      thisTextStyle.WrapEndX = thisTextStyle.WrapEndX - (0.0035 * fPaddingFactor)
//  ENDIF
//ENDPROC



ENUM CLAN_TYPE
    CLAN_TYPE_EMPTY = 0,
    CLAN_TYPE_PRIVATE,
    CLAN_TYPE_PUBLIC
ENDENUM

ENUM CLAN_RANK
    CLAN_RANK_EMPTY = 0,
    CLAN_RANK_1,
    CLAN_RANK_2,
    CLAN_RANK_3,
    CLAN_RANK_4,
    CLAN_RANK_5
ENDENUM

FUNC TEXT_LABEL_63 GET_CLAN_PRIVATE_FILENAME(CLAN_TYPE ClanGameType)    
    TEXT_LABEL_63 tl_63 = ""
    SWITCH ClanGameType
        CASE CLAN_TYPE_PRIVATE
            tl_63 += "ClanTag_Private"
        BREAK
        CASE CLAN_TYPE_PUBLIC
            tl_63 += "ClanTag_Public"
        BREAK
    ENDSWITCH
    RETURN tl_63
ENDFUNC 

FUNC TEXT_LABEL_63 GET_CLAN_RANK_FILENAME(CLAN_RANK ClanRank)
    TEXT_LABEL_63 tl_63 = ""
    SWITCH ClanRank
        CASE CLAN_RANK_EMPTY
            tl_63 += "ClanTag_End"
        BREAK
        CASE CLAN_RANK_1
            tl_63 += "ClanTag_Rank1"
        BREAK
        CASE CLAN_RANK_2
            tl_63 += "ClanTag_Rank2"
        BREAK
        CASE CLAN_RANK_3
            tl_63 += "ClanTag_Rank3"
        BREAK
        CASE CLAN_RANK_4
            tl_63 += "ClanTag_Rank4"
        BREAK
        CASE CLAN_RANK_5
            tl_63 += "ClanTag_Rank5"
        BREAK
    ENDSWITCH
    RETURN tl_63


ENDFUNC



ENUM DRAWRANKBADGE
    DRAWRANKBADGE_NORMAL = 0,
    DRAWRANKBADGE_OUTLINE,
    DRAWRANKBADGE_NORMAL_100,
    DRAWRANKBADGE_OUTLINE_100
ENDENUM

ENUM RANKDISPLAYTYPE
    RANKDISPLAYTYPE_FULL = 0,
    RANKDISPLAYTYPE_OUTLINE,
    RANKDISPLAYTYPE_BOTH,
    RANKDISPLAYTYPE_FULL_WITH_BOX,
    RANKDISPLAYTYPE_JUST_NUMBER
ENDENUM



PROC DRAW_MAIN_HUD_BACKGROUND() 
    DRAW_FRONTEND_BACKGROUND_THIS_FRAME()
    SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_FADE)
ENDPROC



FUNC FLOAT GET_SPRITE_LOWEST_X_SIZE(SPRITE_PLACEMENT aUnderBarSprite)

    FLOAT Subtract = aUnderBarSprite.w/2
    FLOAT LowestXOffsetLocal = aUnderBarSprite.x
    LowestXOffsetLocal -= Subtract
    //LowestXOffsetLocal += 0.002 // XMinorXOffset
    
    
    
    
    RETURN  LowestXOffsetLocal
    
ENDFUNC

FUNC FLOAT GET_SPRITE_HIGHEST_X_SIZE(SPRITE_PLACEMENT aUnderBarSprite)

    FLOAT Addition = aUnderBarSprite.w/2
    FLOAT LowestXOffsetLocal = aUnderBarSprite.x
    LowestXOffsetLocal += Addition
    LowestXOffsetLocal -= 0.002 // XMinorXOffset
    
    RETURN  LowestXOffsetLocal
    
ENDFUNC

//bool INITI_WIDGET
//FLOAT GAHFloatValue = 1
//FLOAT GAHBValue

//FLOAT WigetM
//  FLOAT WigetX
//  FLOAT WigetBarWidth 

PROC GET_SPRITE_LEVEL_BAR(SPRITE_PLACEMENT UnderBar, FLOAT PercentFilled, SPRITE_PLACEMENT& ResultSpriteDetails, BOOL MatchHeight = TRUE, BOOL goingUp = TRUE, BOOL ZeroIsEmpty = TRUE, FLOAT fBarStartPercentage = 0.0, BOOL bCapMaxPercentage = TRUE)

    UNUSED_PARAMETER(ZeroIsEmpty)


    FLOAT WidestWidth = UnderBar.w//-GAHFloatValue
//  FLOAT LowestWidth = 0.001//0 //0.001
    FLOAT LowestWidth

//  IF ZeroIsEmpty
//      LowestWidth = 0.001/(UnderBar.w/0.001)
//  ELSE
//      LowestWidth = 0.001/(UnderBar.w/0.1) //Increase the 0.01 if there's no sliver when 0
//  ENDIF

    LowestWidth = 0.0
    
    
    FLOAT LowestXPosition = GET_SPRITE_LOWEST_X_SIZE(UnderBar)
    
    FLOAT Percentage = (PercentFilled)
    IF PercentFilled < 0
        Percentage = 0
    ENDIF
    IF PercentFilled > 100
        Percentage = 100
    ENDIF
    
	IF (bCapMaxPercentage)
	    IF PercentFilled > 95
	    AND PercentFilled < 100
	        Percentage = 96
	    ENDIF
	ENDIF
	
    FLOAT M
    FLOAT X
    FLOAT B
    FLOAT BarWidth
    FLOAT WidestXPos
    FLOAT XPosition
    
//  IF INITI_WIDGET = FALSE
//      
//      START_WIDGET_GROUP("GET_SPRITE_LEVEL_BAR")
//          ADD_WIDGET_FLOAT_SLIDER("WidestWidth", WidestWidth, -100, 100, 0.001)
//          ADD_WIDGET_FLOAT_SLIDER("LowestWidth", LowestWidth, -100, 100, 0.001)
//          ADD_WIDGET_FLOAT_SLIDER("LowestXPosition", LowestXPosition, -1, 1, 0.001)
//          ADD_WIDGET_FLOAT_SLIDER("Percentage", Percentage, -1, 1, 0.001)
//          ADD_WIDGET_FLOAT_SLIDER("M", M, -100, 100, 0.001)
//          ADD_WIDGET_FLOAT_SLIDER("X", X, -100, 100, 0.001)
//          ADD_WIDGET_FLOAT_SLIDER("B", B, -100, 100, 0.001)
//          ADD_WIDGET_FLOAT_SLIDER("BarWidth", BarWidth, -100, 100, 0.001)
//          ADD_WIDGET_FLOAT_SLIDER("WidestXPos", WidestXPos, -100, 100, 0.001)
//          ADD_WIDGET_FLOAT_SLIDER("XPosition", XPosition, -100, 100, 0.001)
//      
//      
//      STOP_WIDGET_GROUP()
//  
//      INITI_WIDGET = TRUE
//  ENDIF
    
    
    IF goingUp = FALSE
                
        M = ((LowestWidth-WidestWidth)/(100)) //Fullest Percent
        //Work out Y (BarWidth) for width Y = MX+B
        X = Percentage
        B = WidestWidth
        BarWidth = (M*X)+B
        
        
        
        LowestXPosition = UnderBar.x
        WidestXPos   = GET_SPRITE_HIGHEST_X_SIZE(UnderBar) //The Right edge
        XPosition = (WidestXPos-(BarWidth/2))


    ELSE

        M = ((WidestWidth-LowestWidth)/100) //Fullest Percent
        //Work out Y (BarWidth) for width Y = MX+B
        X = Percentage

//      IF ZeroIsEmpty
//          B = 0.001/(UnderBar.w/0.001)
//      ELSE
//          B = 0.001/(UnderBar.w/0.1) //Increase the 0.01 if there's no sliver when 0
//      ENDIF
        
        B = 0

        BarWidth = (M*X)+B
        IF PercentFilled >= 100.0
            BarWidth = UnderBar.w
        ENDIF
        
        
        WidestXPos = UnderBar.x
    //  FLOAT LowestXPos = (UnderBar.x+(BarWidth/2))-(WidestWidth/2)
        M = ((WidestXPos-LowestXPosition)/100)
        X = Percentage

        B = LowestXPosition 
        XPosition = (M*X)+B
        
        //XPosition -= 0.001
        
        
    
    ENDIF
	
	IF fBarStartPercentage > 0.0
	AND fBarStartPercentage < 100.0
		//Get 1% of the width of the full bar we are drawing on top of
		FLOAT fAdjustedXVal = (UnderBar.w * 0.01)
		//Multiply by the percentage given
		XPosition += (fBarStartPercentage * fAdjustedXVal)
	ENDIF
    
//  WigetM = M
//  WigetX = X  
//  WigetBarWidth = BarWidth
//  IF INITI_WIDGET = FALSE
//      START_WIDGET_GROUP("BRENDA FLOAT")
//          ADD_WIDGET_FLOAT_SLIDER("GAHFloatValue", GAHFloatValue, -1000, 10000, 1)
//          ADD_WIDGET_FLOAT_SLIDER("WidestWidth", WidestWidth, -1000, 10000, 0.001)
//          ADD_WIDGET_FLOAT_SLIDER("LowestWidth", LowestWidth, -1000, 10000, 0.001)
//          ADD_WIDGET_BOOL("Is Going Up", goingUp)
//          ADD_WIDGET_FLOAT_SLIDER("M = Gradient", WigetM, -1000, 10000, 0.001)
//          ADD_WIDGET_FLOAT_SLIDER("X = Percent", WigetX, -1000, 10000, 0.001)
//          ADD_WIDGET_FLOAT_SLIDER("BarWidth", WigetBarWidth, -1000, 10000, 0.001)
//          ADD_WIDGET_BOOL("Is Zero Empty", OnlyZeroIsEmpty)
//          
//      STOP_WIDGET_GROUP()
//      INITI_WIDGET = TRUE
//  ENDIF
    
    
//  IF SegmentsMax > 0
//      
//      //Work out which segment you need from InSegments and percent
//      FLOAT newPercent = (SegmentsMax*PercentFilled)
//      INT WhichSection = ROUND(newPercent/100)
//
//      
//      //// FLOAT LowX = TO_FLOAT((100/SegmentsMax)*(WhichSection-1))
//      ////FLOAT HighX = TO_FLOAT((100/SegmentsMax)*(WhichSection))
//      //// FLOAT MidX = (((LowX-HighX)/2)+LowX)
//      
//      //Empty first fill to get the lowestX
//      M = ((WidestWidth-LowestWidth)/100) //Fullest Percent
//      //Work out Y (BarWidth) for width Y = MX+B
//      X = TO_FLOAT(100/SegmentsMax)
//      B =  0 //0.001
//      BarWidth = (M*X)+B
//      
//      
//      FLOAT LeftPosition = (Underbar.x-(WidestWidth/2))   
//      FLOAT Steps = WidestWidth/(SegmentsMax)
//      WidestXPos = (LeftPosition+(Steps*(SegmentsMax)))
//      LowestXPosition = (LeftPosition+Steps)
//      
////        NET_NL()NET_PRINT("LowestXPosition = ")NET_PRINT_FLOAT(LowestXPosition)
////        M = ((WidestXPos-LowestXPosition)/100)
////        X = MidX
////        B = LowestXPosition
////        XPosition = (M*X)+B
//              
//      XPosition = LeftPosition+(WhichSection*Steps)
////        NET_NL()NET_PRINT("XPosition 2 = ")NET_PRINT_FLOAT(XPosition)
//      XPosition -= (Steps/2)
//      
////        NET_NL()NET_PRINT("XPosition LAST = ")NET_PRINT_FLOAT(XPosition)
//      
//      IF WhichSection = 1
//          BarWidth -= 0.001
//          XPosition += 0.001
//      ELIF WhichSection = SegmentsMax
//          BarWidth -= 0.001
//          XPosition -= 0.001
//      ENDIF
//      
//  ENDIF
    
        
    ResultSpriteDetails.x = XPosition //(UnderBar.x+LevelBarOffset)-(WidestWidth/2.0)
    ResultSpriteDetails.y = UnderBar.y
    ResultSpriteDetails.w = BarWidth
    IF MatchHeight = TRUE
        ResultSpriteDetails.h = UnderBar.h
    ENDIF

    ResultSpriteDetails.fRotation = UnderBar.fRotation

ENDPROC






PROC DRAW_DIVIDING_LINES(SPRITE_PLACEMENT Underbar, RECT& TheRect, INT NumCheckpoints)
    
    FLOAT Width = Underbar.w
    TheRect.y = Underbar.y
    FLOAT LeftPosition = (Underbar.x-(Width/2)) 
    FLOAT Steps = Width/(NumCheckpoints)
    FLOAT XPosition
    INT I
    FOR I = 1 TO NumCheckpoints-1
        XPosition = (LeftPosition+(Steps*I))
        TheRect.x = XPosition
        DRAW_RECTANGLE(TheRect)
    ENDFOR

ENDPROC




FUNC BOOL DO_FLASHING(INT TimeToFlashFor, SCRIPT_TIMER& MainTimer, SCRIPT_TIMER& MiniTimer)

    IF HAS_NET_TIMER_EXPIRED(MainTimer, TimeToFlashFor)
        RETURN TRUE
    ENDIF
    
//  IF HAS_NET_TIMER_EXPIRED(MiniTimer, g_FlashingTimer1)
//      IF HAS_NET_TIMER_EXPIRED(MiniTimer, g_FlashingTimer2)
	
	IF NOT HAS_NET_TIMER_STARTED(MiniTimer)
		START_NET_TIMER(MiniTimer)
		
	ELIF HAS_NET_TIMER_EXPIRED(MiniTimer, 300)
        IF HAS_NET_TIMER_EXPIRED(MiniTimer, 800)    
            RESET_NET_TIMER(MiniTimer)
            RETURN TRUE
        ELSE
            RETURN TRUE
        ENDIF
    ELSE
        RETURN FALSE
    ENDIF
    
//  
//  IF MiniTimer < (GET_THE_NETWORK_TIMER() - 600)
//      MiniTimer = (GET_THE_NETWORK_TIMER() + 500) 
//  ENDIF
//  
//  IF MiniTimer < GET_THE_NETWORK_TIMER()
//      RETURN TRUE
//  ELSE
//      RETURN FALSE
//  ENDIF
    RETURN TRUE
ENDFUNC

FUNC BOOL DO_COLOUR_FLASHING(INT TimeToFlashFor, SCRIPT_TIMER& MainTimer, SCRIPT_TIMER& MiniTimer)

    IF HAS_NET_TIMER_EXPIRED(MainTimer, TimeToFlashFor)
//      NET_NL()NET_PRINT("DO_COLOUR_FLASHING = TRUE 1 ")
        RETURN TRUE
    ENDIF

    IF HAS_NET_TIMER_EXPIRED(MiniTimer, 300)
        IF HAS_NET_TIMER_EXPIRED(MiniTimer, 800)    
            RESET_NET_TIMER(MiniTimer)
//          NET_NL()NET_PRINT("DO_COLOUR_FLASHING = TRUE 2 ")
            RETURN TRUE
        ELSE
//          NET_NL()NET_PRINT("DO_COLOUR_FLASHING = TRUE 3 ")
            RETURN TRUE
        ENDIF
    ELSE
//      NET_NL()NET_PRINT("DO_COLOUR_FLASHING = FALSE 1 ")
        RETURN FALSE
    ENDIF
//  NET_NL()NET_PRINT("DO_COLOUR_FLASHING = TRUE 4 ")
    RETURN TRUE
ENDFUNC


