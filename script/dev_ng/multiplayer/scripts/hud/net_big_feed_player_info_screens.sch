/**********************************************************
/ Name: 		net_big_feed_player_info_screens.sch
/ Author(s): 	William Kennedy / James Adwick
/ Purpose: 		Passes the relevant data to scaleform for the
/ 				big feed screen seen during GTAO transitions
***********************************************************/


USING "globals.sch"
USING "commands_socialclub.sch"
USING "transition_common.sch"
USING "fmmc_next_job_screen.sch"

// Struct we use within the sorting functions to get least played first
STRUCT BIG_FEED_JOB_DATA_ARRAY
	INT iTheStat
	FLOAT fPercent
	INT iPlayed
	INT iAvailable
ENDSTRUCT

/// PURPOSE: Based on our Big Feed enum we populate a mission type and subtype so we can loop over all stats we need to pass to scaleform
PROC GET_BIG_FEED_MISSION_TYPE_AND_SUB_TYPE_FROM_ENUM(BIG_FEED_PLAYER_JOB_STATS ePlayerJobStat, INT &iMissionType, INT &iMissionSubType)

	iMissionType = -1
	iMissionSubType = -1

	SWITCH ePlayerJobStat
		CASE BIG_FEED_RACES
			iMissionType = FMMC_TYPE_RACE
			iMissionSubType = 0
		BREAK
		
		CASE BIG_FEED_DMS
			iMissionType = FMMC_TYPE_DEATHMATCH
			iMissionSubType = 0
		BREAK
		
		CASE BIG_FEED_PARAS
			iMissionType = FMMC_TYPE_BASE_JUMP
			iMissionSubType = 0
		BREAK
		
		CASE BIG_FEED_SURVIVAL
			iMissionType = FMMC_TYPE_SURVIVAL
			iMissionSubType = 0
		BREAK
		
		CASE BIG_FEED_MISSIONS
			iMissionType = FMMC_TYPE_MISSION
			iMissionSubType = 0
		BREAK
		
		CASE BIG_FEED_CTF
			iMissionType = FMMC_TYPE_MISSION
			iMissionSubType = FMMC_MISSION_TYPE_CTF
		BREAK
		
		CASE BIG_FEED_LTS
			iMissionType = FMMC_TYPE_MISSION
			iMissionSubType = FMMC_MISSION_TYPE_LTS
		BREAK
		
		CASE BIG_FEED_VERSUS
			iMissionType = FMMC_TYPE_MISSION
			iMissionSubType = FMMC_MISSION_TYPE_VERSUS
		BREAK
		
		CASE BIG_FEED_HEIST
			iMissionType = FMMC_TYPE_MISSION
			iMissionSubType = FMMC_MISSION_TYPE_HEIST
		BREAK
		
	ENDSWITCH

ENDPROC

/// PURPOSE:
///    Pass the data we have to scaleform to be displayed on the big feed screen
/// PARAMS:
///    iSlot - The slot we are setting
///    iTotalPlayed - First int to send to the scaleform
///    iTotalAvailable - Second int to send to the scaleform
///    strJobName - The name to display over the stat
///    iIcon - The icon to display for the stat
PROC SET_BIG_FEED_DATA_SLOT_JOB_PLAYED(INT iSlot, INT iTotalPlayed, INT iTotalAvailable, STRING strJobName, INT iIcon)
	IF HAS_SCALEFORM_MOVIE_LOADED(g_TransitionSessionNonResetVars.sPostMissionCleanupData.scaleSplash)

		BEGIN_SCALEFORM_MOVIE_METHOD(g_TransitionSessionNonResetVars.sPostMissionCleanupData.scaleSplash, "SET_DATA_SLOT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSlot)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iIcon)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(strJobName)
			// Set ratio (i.e. 5/10)
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("PM_UGEN_NUM")
				ADD_TEXT_COMPONENT_INTEGER(iTotalPlayed)
				ADD_TEXT_COMPONENT_INTEGER(iTotalAvailable)
			END_TEXT_COMMAND_SCALEFORM_STRING()
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(TO_FLOAT(iTotalPlayed)/TO_FLOAT(iTotalAvailable))
		END_SCALEFORM_MOVIE_METHOD()
		
	ENDIF

ENDPROC

/// PURPOSE:
///    Pass the data we have to scaleform to be displayed on the big feed screen
/// PARAMS:
///    iSlot - The slot we are setting
///    iTotalPlayed - First int to send to the scaleform
///    iTotalAvailable - Second int to send to the scaleform
///    strContactName - The name to display over the stat
///    strHeadshotTxd - The headshot texture to display for the stat
PROC SET_BIG_FEED_DATA_SLOT_CONTACT_MISSIONS_PLAYED(INT iSlot, INT iTotalPlayed, INT iTotalAvailable, STRING strContactName, STRING strHeadshotTxd)
	IF HAS_SCALEFORM_MOVIE_LOADED(g_TransitionSessionNonResetVars.sPostMissionCleanupData.scaleSplash)
	
		BEGIN_SCALEFORM_MOVIE_METHOD(g_TransitionSessionNonResetVars.sPostMissionCleanupData.scaleSplash, "SET_DATA_SLOT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSlot)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING(strHeadshotTxd)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(strContactName)
			// Set ratio (i.e. 5/10)
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("PM_UGEN_NUM")
				ADD_TEXT_COMPONENT_INTEGER(iTotalPlayed)
				ADD_TEXT_COMPONENT_INTEGER(iTotalAvailable)
			END_TEXT_COMMAND_SCALEFORM_STRING()
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(TO_FLOAT(iTotalPlayed)/TO_FLOAT(iTotalAvailable))
		END_SCALEFORM_MOVIE_METHOD()
		
	ENDIF

ENDPROC

PROC ADD_BIG_FEED_JOB_CARD_DATA(FRONTEND_DETAILS_TYPE detailType, INT iMenuIndex, INT iMenuId, STRING tlTitle, STRING tlDetail, INT iNumber1 = -1, INT iNumber2 = -1, FLOAT fFloat = -1.0, STRING tlCrewTag = NULL)
	IF BEGIN_SCALEFORM_MOVIE_METHOD(g_TransitionSessionNonResetVars.sPostMissionCleanupData.scaleSplash, "SET_DATA_SLOT")
		ADD_JOB_CARD_DATA_TO_SCALEFORM(detailType, iMenuIndex, iMenuID, tlTitle, tlDetail, iNumber1, iNumber2, fFloat, tlCrewTag)
		END_SCALEFORM_MOVIE_METHOD()	
	ENDIF
ENDPROC

CONST_INT BITSET_UNPLAYED_JOB_BIG_FEED_GOT_MISSION 0
CONST_INT BITSET_UNPLAYED_JOB_BIG_FEED_GOT_MISSION_DESCRIPTION 1
CONST_INT BITSET_UNPLAYED_JOB_BIG_FEED_GOT_MISSION_PHOTO 2
CONST_INT BITSET_UNPLAYED_JOB_BIG_FEED_NEED_MISSION_DATA 3
CONST_INT BITSET_UNPLAYED_JOB_BIG_FEED_GOT_ALL_DATA 4

FUNC BOOL GET_ALL_DATA_NEED_FOR_JOB_CARD(STRUCT_DL_PHOTO_VARS_LITE &photoVars, CACHED_MISSION_DESCRIPTION_LOAD_VARS &sBigFeedMissionDescVars, INT &iUnplayedJobArrayIndex)
	
	BOOL bTemp
	
	IF NOT IS_BIT_SET(g_TransitionSessionNonResetVars.sDynamicBigFeedData.g_iBigFeedGrabUnplayedJobBitset, BITSET_UNPLAYED_JOB_BIG_FEED_GOT_ALL_DATA)
		
		PRINTLN("[BIGFEED] - GET_ALL_DATA_NEED_FOR_JOB_CARD - BITSET_UNPLAYED_JOB_BIG_FEED_GOT_ALL_DATA = FALSE, grabbing mission info.")
		
		IF NOT IS_BIT_SET(g_TransitionSessionNonResetVars.sDynamicBigFeedData.g_iBigFeedGrabUnplayedJobBitset, BITSET_UNPLAYED_JOB_BIG_FEED_GOT_MISSION)
			
			IF g_TransitionSessionNonResetVars.sDynamicBigFeedData.iRandomJobType = (-1)
				PRINTLN("[BIGFEED] - GET_ALL_DATA_NEED_FOR_JOB_CARD - BITSET_UNPLAYED_JOB_BIG_FEED_GOT_MISSION = FALSE, grabbing random mission type with GET_TRANSITION_SESSIONS_QUICK_MATCH_TYPE_RANDOM.")
				g_TransitionSessionNonResetVars.sDynamicBigFeedData.iRandomJobType = GET_TRANSITION_SESSIONS_QUICK_MATCH_TYPE_RANDOM()
			ELSE
				PRINTLN("[BIGFEED] - GET_ALL_DATA_NEED_FOR_JOB_CARD - BITSET_UNPLAYED_JOB_BIG_FEED_GOT_MISSION = FALSE, iRandomJobType = ", g_TransitionSessionNonResetVars.sDynamicBigFeedData.iRandomJobType)
				PRINTLN("[BIGFEED] - GET_ALL_DATA_NEED_FOR_JOB_CARD - BITSET_UNPLAYED_JOB_BIG_FEED_GOT_MISSION = FALSE, grabbing mission with GET_NEXT_FM_MISSION_TO_LOAD.")
				IF GET_NEXT_FM_MISSION_TO_LOAD(g_TransitionSessionNonResetVars.sDynamicBigFeedData.iRandomJobType, iUnplayedJobArrayIndex, bTemp, -1, 0, FALSE, 1000, TRUE)
					SET_BIT(g_TransitionSessionNonResetVars.sDynamicBigFeedData.g_iBigFeedGrabUnplayedJobBitset, BITSET_UNPLAYED_JOB_BIG_FEED_GOT_MISSION)
				ENDIF
			ENDIF
			
		ELSE
			
			PRINTLN("[BIGFEED] - GET_ALL_DATA_NEED_FOR_JOB_CARD - BITSET_UNPLAYED_JOB_BIG_FEED_GOT_MISSION = TRUE")
			
			IF NOT IS_BIT_SET(g_TransitionSessionNonResetVars.sDynamicBigFeedData.g_iBigFeedGrabUnplayedJobBitset, BITSET_UNPLAYED_JOB_BIG_FEED_GOT_MISSION_DESCRIPTION)
				
				PRINTLN("[BIGFEED] - GET_ALL_DATA_NEED_FOR_JOB_CARD - BITSET_UNPLAYED_JOB_BIG_FEED_GOT_MISSION_DESCRIPTION = FALSE, grabbing mission description.")
				
				IF NOT sBigFeedMissionDescVars.bSucess
					PRINTLN("[BIGFEED] - GET_ALL_DATA_NEED_FOR_JOB_CARD - sBigFeedMissionDescVars.bSucess = FALSE, grabbing mission description hash with REQUEST_AND_LOAD_CACHED_DESCRIPTION.")
					REQUEST_AND_LOAD_CACHED_DESCRIPTION(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iUnplayedJobArrayIndex].iMissionDecHash, sBigFeedMissionDescVars)
				ELSE
					PRINTLN("[BIGFEED] - GET_ALL_DATA_NEED_FOR_JOB_CARD - sBigFeedMissionDescVars.bSucess = FALSE, grabbed mission description hash with REQUEST_AND_LOAD_CACHED_DESCRIPTION.")
					SET_BIT(g_TransitionSessionNonResetVars.sDynamicBigFeedData.g_iBigFeedGrabUnplayedJobBitset, BITSET_UNPLAYED_JOB_BIG_FEED_GOT_MISSION_DESCRIPTION)
				ENDIF
				
			ELSE
				
				PRINTLN("[BIGFEED] - GET_ALL_DATA_NEED_FOR_JOB_CARD - BITSET_UNPLAYED_JOB_BIG_FEED_GOT_MISSION_DESCRIPTION = TRUE")
				
				IF NOT IS_BIT_SET(g_TransitionSessionNonResetVars.sDynamicBigFeedData.g_iBigFeedGrabUnplayedJobBitset, BITSET_UNPLAYED_JOB_BIG_FEED_GOT_MISSION_PHOTO)
					
					PRINTLN("[BIGFEED] - GET_ALL_DATA_NEED_FOR_JOB_CARD - BITSET_UNPLAYED_JOB_BIG_FEED_GOT_MISSION_PHOTO = FALSE, grabbing mission with DOWNLOAD_PHOTO_FOR_FMMC_LITE.")
					
					IF DOWNLOAD_PHOTO_FOR_FMMC_LITE(photoVars, g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iUnplayedJobArrayIndex].tlName, g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iUnplayedJobArrayIndex].iPhotoVersion)
						SET_BIT(g_TransitionSessionNonResetVars.sDynamicBigFeedData.g_iBigFeedGrabUnplayedJobBitset, BITSET_UNPLAYED_JOB_BIG_FEED_GOT_MISSION_PHOTO)
					ENDIF
					
				ELSE
					
					PRINTLN("[BIGFEED] - GET_ALL_DATA_NEED_FOR_JOB_CARD - BITSET_UNPLAYED_JOB_BIG_FEED_GOT_MISSION_DESCRIPTION = TRUE")
					SET_BIT(g_TransitionSessionNonResetVars.sDynamicBigFeedData.g_iBigFeedGrabUnplayedJobBitset, BITSET_UNPLAYED_JOB_BIG_FEED_GOT_ALL_DATA)
					
				ENDIF
				
			ENDIF
			
		ENDIF
	
	ENDIF
	
	RETURN IS_BIT_SET(g_TransitionSessionNonResetVars.sDynamicBigFeedData.g_iBigFeedGrabUnplayedJobBitset, BITSET_UNPLAYED_JOB_BIG_FEED_GOT_ALL_DATA)
	
ENDFUNC
//
//FUNC STRING CONVERT_FLOAT_TO_STRING(FLOAT f, INT decimal_places = 4)
//    TEXT_LABEL float_text_label
//	INT string_length
//    
//    // Handle -ve values.
//    IF f < 0
//		INT iCeil_f = CEIL(f)
//		IF iCeil_f = 0
//			float_text_label = "-"
//		ENDIF
//		
//        float_text_label += iCeil_f
//    ELSE
//        float_text_label += FLOOR(f)
//    ENDIF
//	
//    float_text_label += "."
//	
//	string_length = GET_LENGTH_OF_LITERAL_STRING(float_text_label)
//    
//    FLOAT fFloat_remainder_a = f%1.0
//	FLOAT fFloat_remainder_b = (fFloat_remainder_a)*10000.0
//	INT iFloat_remainder_c = ROUND((fFloat_remainder_b))
//	
//	INT iAddedZeros = 0
//    IF absi(iFloat_remainder_c) < 10
//        float_text_label += "000"
//		iAddedZeros = 3
//    ELIF absi(iFloat_remainder_c) < 100
//        float_text_label += "00"
//		iAddedZeros = 2
//    ELIF absi(iFloat_remainder_c) < 1000
//        float_text_label += "0"
//		iAddedZeros = 1
//    ENDIF
//	
//    INT iFloat_remainder_d = ABSI(iFloat_remainder_c)
//	
//	float_text_label += iFloat_remainder_d
//    
//	INT iToAddZeros = 0
//    IF iFloat_remainder_d < 10
//		iToAddZeros = 3
//    ELIF iFloat_remainder_d < 100
//		iToAddZeros = 2
//    ELIF iFloat_remainder_d < 1000
//		iToAddZeros = 1
//    ENDIF
//	IF iToAddZeros > iAddedZeros
//		//might need to add zeros to end of text label...
//	ENDIF
//	
//	RETURN GET_FIRST_N_CHARACTERS_OF_LITERAL_STRING(float_text_label, string_length + decimal_places)
//
//ENDFUNC

PROC SET_BIG_FEED_DATA_SLOT_UNPLAYED_JOB(STRUCT_DL_PHOTO_VARS_LITE &sBigFeedUnplayedJobPhotoData, INT &iUnplayedJobArrayIndex)
	
	PRINTLN("[BIGFEED] - SET_BIG_FEED_DATA_SLOT_UNPLAYED_JOB - used GET_NEXT_FM_MISSION_TO_LOAD to grab mission index. Index = ", iUnplayedJobArrayIndex)
	PRINTLN("[BIGFEED] - SET_BIG_FEED_DATA_SLOT_UNPLAYED_JOB - tlName = ", g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iUnplayedJobArrayIndex].tlName)
	PRINTLN("[BIGFEED] - SET_BIG_FEED_DATA_SLOT_UNPLAYED_JOB - tlOwner = ", g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iUnplayedJobArrayIndex].tlOwner)
	PRINTLN("[BIGFEED] - SET_BIG_FEED_DATA_SLOT_UNPLAYED_JOB - tlMissionName = ", g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iUnplayedJobArrayIndex].tlMissionName)
	PRINTLN("[BIGFEED] - SET_BIG_FEED_DATA_SLOT_UNPLAYED_JOB - tlMissionDec = ", g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iUnplayedJobArrayIndex].tlMissionDec)
	PRINTLN("[BIGFEED] - SET_BIG_FEED_DATA_SLOT_UNPLAYED_JOB - iMissionDecHash = ", g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iUnplayedJobArrayIndex].iMissionDecHash)
	PRINTLN("[BIGFEED] - SET_BIG_FEED_DATA_SLOT_UNPLAYED_JOB - vStartPos = ", g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iUnplayedJobArrayIndex].vStartPos)
	PRINTLN("[BIGFEED] - SET_BIG_FEED_DATA_SLOT_UNPLAYED_JOB - vCamPos = ", g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iUnplayedJobArrayIndex].vCamPos)
	PRINTLN("[BIGFEED] - SET_BIG_FEED_DATA_SLOT_UNPLAYED_JOB - vCamHead = ", g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iUnplayedJobArrayIndex].vCamHead)
	PRINTLN("[BIGFEED] - SET_BIG_FEED_DATA_SLOT_UNPLAYED_JOB - iType = ", g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iUnplayedJobArrayIndex].iType)
	PRINTLN("[BIGFEED] - SET_BIG_FEED_DATA_SLOT_UNPLAYED_JOB - iPhotoVersion = ", g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iUnplayedJobArrayIndex].iPhotoVersion)
	PRINTLN("[BIGFEED] - SET_BIG_FEED_DATA_SLOT_UNPLAYED_JOB - iSubType = ", g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iUnplayedJobArrayIndex].iSubType)
	PRINTLN("[BIGFEED] - SET_BIG_FEED_DATA_SLOT_UNPLAYED_JOB - iMinPlayers = ", g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iUnplayedJobArrayIndex].iMinPlayers)
	PRINTLN("[BIGFEED] - SET_BIG_FEED_DATA_SLOT_UNPLAYED_JOB - iRank = ", g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iUnplayedJobArrayIndex].iRank)
	PRINTLN("[BIGFEED] - SET_BIG_FEED_DATA_SLOT_UNPLAYED_JOB - iMaxPlayers = ", g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iUnplayedJobArrayIndex].iMaxPlayers)
	PRINTLN("[BIGFEED] - SET_BIG_FEED_DATA_SLOT_UNPLAYED_JOB - iMaxNumberOfTeams = ", g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iUnplayedJobArrayIndex].iMaxNumberOfTeams)
	PRINTLN("[BIGFEED] - SET_BIG_FEED_DATA_SLOT_UNPLAYED_JOB - iRating = ", g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iUnplayedJobArrayIndex].iRating)
	PRINTLN("[BIGFEED] - SET_BIG_FEED_DATA_SLOT_UNPLAYED_JOB - iLesRating = ", g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iUnplayedJobArrayIndex].iLesRating)
	PRINTLN("[BIGFEED] - SET_BIG_FEED_DATA_SLOT_UNPLAYED_JOB - iContactCharEnum = ", g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iUnplayedJobArrayIndex].iContactCharEnum)
	PRINTLN("[BIGFEED] - SET_BIG_FEED_DATA_SLOT_UNPLAYED_JOB - iBitSet = ", g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iUnplayedJobArrayIndex].iBitSet)
	PRINTLN("[BIGFEED] - SET_BIG_FEED_DATA_SLOT_UNPLAYED_JOB - iLaunchTimesBit = ", g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iUnplayedJobArrayIndex].iLaunchTimesBit)
	PRINTLN("[BIGFEED] - SET_BIG_FEED_DATA_SLOT_UNPLAYED_JOB - iTestCompleteBitSet = ", g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iUnplayedJobArrayIndex].iTestCompleteBitSet)
	PRINTLN("[BIGFEED] - SET_BIG_FEED_DATA_SLOT_UNPLAYED_JOB - nCategory = ", g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iUnplayedJobArrayIndex].nCategory)
	PRINTLN("[BIGFEED] - SET_BIG_FEED_DATA_SLOT_UNPLAYED_JOB - iContentVersion = ", g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iUnplayedJobArrayIndex].iContentVersion)
	PRINTLN("[BIGFEED] - SET_BIG_FEED_DATA_SLOT_UNPLAYED_JOB - iRootContentIdHash = ", g_FMMC_ROCKSTAR_CREATED.sDefaultCoronaOptions[iUnplayedJobArrayIndex].iRootContentIdHash)
	
	IF iUnplayedJobArrayIndex >= 0
		
		FLOAT fRpX, fCashX, fFakeCashX, fFakeRpX
//		TEXT_LABEL_7 sRpX, sCashX
		
		g_eTunablesContexts eTunAbleCon         = GET_TUNABLE_CONTEXT(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iUnplayedJobArrayIndex].iType, g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iUnplayedJobArrayIndex].iSubType)
		g_eTunablesContexts eTunAbleConSub      = GET_CONTENT_MODIFIER_FROM_JOB_HASH(g_FMMC_ROCKSTAR_CREATED.sDefaultCoronaOptions[iUnplayedJobArrayIndex].iRootContentIdHash)     
		
		Fill_MP_Float_Tunable(eTunAbleCon, eTunAbleConSub, HASH("XP_MULTIPLIER"), fRpX)
		Fill_MP_Float_Tunable(eTunAbleCon, eTunAbleConSub, HASH("FAKE_XP_MULTIPLIER"), fFakeRpX)
		Fill_MP_Float_Tunable(eTunAbleCon, eTunAbleConSub, HASH("CASH_MULTIPLIER"), fCashX)
		Fill_MP_Float_Tunable(eTunAbleCon, eTunAbleConSub, HASH("FAKE_CASH_MULTIPLIER"), fFakeCashX)
		
		PRINTLN("[BIGFEED] - SET_BIG_FEED_DATA_SLOT_UNPLAYED_JOB - fRpX after calling fill float tuneable XP_MULTIPLIER = ", fRpX)
		PRINTLN("[BIGFEED] - SET_BIG_FEED_DATA_SLOT_UNPLAYED_JOB - fFakeRpX after calling fill float tuneable FAKE_XP_MULTIPLIER = ", fFakeRpX)
		
		PRINTLN("[BIGFEED] - SET_BIG_FEED_DATA_SLOT_UNPLAYED_JOB - fCashX after calling fill float tuneable CASH_MULTIPLIER = ", fCashX)
		PRINTLN("[BIGFEED] - SET_BIG_FEED_DATA_SLOT_UNPLAYED_JOB - fFakeCashX after calling fill float tuneable FAKE_CASH_MULTIPLIER = ", fFakeCashX)
		
		IF fFakeRpX > 0.0
			fRpX = fFakeRpX
			PRINTLN("[BIGFEED] - SET_BIG_FEED_DATA_SLOT_UNPLAYED_JOB - Setting fRpX to  fFakeRpX")
		ENDIF
		IF fFakeCashX > 0.0
			fCashX = fFakeCashX
			PRINTLN("[BIGFEED] - SET_BIG_FEED_DATA_SLOT_UNPLAYED_JOB - Setting fCashX to  fFakeCashX")
		ENDIF
		
		IF fRpX < 1.0
			fRpX = 0.0
			PRINTLN("[BIGFEED] - SET_BIG_FEED_DATA_SLOT_UNPLAYED_JOB - fRpX < 1.0, capping to 0.0")
		ENDIF
		
		IF fCashX < 1.0
			fCashX = 0.0
			PRINTLN("[BIGFEED] - SET_BIG_FEED_DATA_SLOT_UNPLAYED_JOB - fCashX < 1.0, capping to 0.0")
		ENDIF
		
		
//		fRpX = 5.0
//		fCashX = 5.0
		
//		sRpX = CONVERT_FLOAT_TO_STRING(fRpX, 1)
//		sRpX += " x"
//		sCashX = CONVERT_FLOAT_TO_STRING(fCashX, 1)
//		sCashX += " x"
		
		IF BEGIN_SCALEFORM_MOVIE_METHOD(g_TransitionSessionNonResetVars.sPostMissionCleanupData.scaleSplash, "SET_BIGFEED_BODY_TEXT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(UGC_GET_CACHED_DESCRIPTION_WITH_NO_TRANSLATION_CHECK(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iUnplayedJobArrayIndex].iMissionDecHash, 500))
			END_SCALEFORM_MOVIE_METHOD()
			PRINTLN("[BIGFEED] - SET_BIG_FEED_DATA_SLOT_UNPLAYED_JOB - added multiline description info.")
		ENDIF
		
		IF BEGIN_SCALEFORM_MOVIE_METHOD(g_TransitionSessionNonResetVars.sPostMissionCleanupData.scaleSplash, "SET_TITLE")
		
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("") // 0 Title 
			PRINTLN("[BIGFEED] - SET_BIG_FEED_DATA_SLOT_UNPLAYED_JOB - added empty string for title.")
			
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iUnplayedJobArrayIndex].tlMissionName) // 1 Mission Name 
			PRINTLN("[BIGFEED] - SET_BIG_FEED_DATA_SLOT_UNPLAYED_JOB - added mission name: ", g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iUnplayedJobArrayIndex].tlMissionName)
			
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)						// 2 Verified Mission: 0 - nothing, 1 - Verified, 2 - Created
			PRINTLN("[BIGFEED] - SET_BIG_FEED_DATA_SLOT_UNPLAYED_JOB - added int 2 for mission verification.")
			
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING(TEXTURE_DOWNLOAD_GET_NAME(sBigFeedUnplayedJobPhotoData.iTextureDownloadHandle))// 3 txd
			PRINTLN("[BIGFEED] - SET_BIG_FEED_DATA_SLOT_UNPLAYED_JOB - added txd.")
			
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING(TEXTURE_DOWNLOAD_GET_NAME(sBigFeedUnplayedJobPhotoData.iTextureDownloadHandle))// 4 txn 
			PRINTLN("[BIGFEED] - SET_BIG_FEED_DATA_SLOT_UNPLAYED_JOB - added txn.")
			
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)// 5 TextureLoadType 
			PRINTLN("[BIGFEED] - SET_BIG_FEED_DATA_SLOT_UNPLAYED_JOB - added int 1 for texture load type.")
			
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)// 6 DisplayType
			PRINTLN("[BIGFEED] - SET_BIG_FEED_DATA_SLOT_UNPLAYED_JOB - added int 0 for display type.")
			
			IF NOT g_sMPTunables.bdisable_modifier_badges
				
				// 7 rpMult
				IF fRpX <= 0.0
				    SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
					PRINTLN("[BIGFEED] - SET_BIG_FEED_DATA_SLOT_UNPLAYED_JOB - fRpX <= 0.0, calling SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE).")
				ELSE
				    BEGIN_TEXT_COMMAND_SCALEFORM_STRING("PM_MULTI")
				         ADD_TEXT_COMPONENT_FLOAT(fRpX, 0)
				    END_TEXT_COMMAND_SCALEFORM_STRING()
					PRINTLN("[BIGFEED] - SET_BIG_FEED_DATA_SLOT_UNPLAYED_JOB - fRpX > 0.0, added to movie with text label PM_MULTI")
				ENDIF
				
				// 8 cashMult
				IF fCashX <= 0.0
				    SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
					PRINTLN("[BIGFEED] - SET_BIG_FEED_DATA_SLOT_UNPLAYED_JOB - fCashX <= 0.0, calling SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE).")
				ELSE
				    BEGIN_TEXT_COMMAND_SCALEFORM_STRING("PM_MULTI")
				         ADD_TEXT_COMPONENT_FLOAT(fCashX, 0)
				    END_TEXT_COMMAND_SCALEFORM_STRING()
					PRINTLN("[BIGFEED] - SET_BIG_FEED_DATA_SLOT_UNPLAYED_JOB - fCashX > 0.0, added to movie with text label PM_MULTI")
				ENDIF
				
	//			SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(sRpX)
	//			SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(sCashX)
			
			ENDIF
			
			END_SCALEFORM_MOVIE_METHOD()	
			PRINTLN("[BIGFEED] - SET_BIG_FEED_DATA_SLOT_UNPLAYED_JOB - added title info.")
		ENDIF
		
		TEXT_LABEL_15 tlCreatorCrewTag
		INT iIconColour = ENUM_TO_INT(HUD_COLOUR_WHITE)
		tlCreatorCrewTag = ""
		iIconColour = ENUM_TO_INT(HUD_COLOUR_BLUE)
		STRING strOwner = GET_ROCKSTAR_CREATED_USER_STRING()
		
		IF g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iUnplayedJobArrayIndex].iRating = FMMC_NOT_YET_RATED
			ADD_BIG_FEED_JOB_CARD_DATA(FRONTEND_DETAILS_TYPE_TEXT_LABEL, 0, 0, "FM_ISC_RAT0", "FMMC_NOTRATE")
		ELSE
			ADD_BIG_FEED_JOB_CARD_DATA(FRONTEND_DETAILS_TYPE_FLOAT, 0, 0, "FM_ISC_RAT0", "FM_ISC_RAT1", -1, -1, PICK_FLOAT((TO_FLOAT(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iUnplayedJobArrayIndex].iRating) > 0.0), TO_FLOAT(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iUnplayedJobArrayIndex].iRating), 0.0) )
		ENDIF
		PRINTLN("[BIGFEED] - SET_BIG_FEED_DATA_SLOT_UNPLAYED_JOB - added rating info.")
		
		INT iDetailIndex
		iDetailIndex = 1
		
		ADD_BIG_FEED_JOB_CARD_DATA(FRONTEND_DETAILS_TYPE_LITERAL_STRING, iDetailIndex, 0, "FM_ISC_CB", strOwner, -1, -1, -1, tlCreatorCrewTag)
		iDetailIndex++
		PRINTLN("[BIGFEED] - SET_BIG_FEED_DATA_SLOT_UNPLAYED_JOB - added owner info. Owner = ", strOwner)
		
//		IF GET_PLAYER_RANK(PLAYER_ID()) >= g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iUnplayedJobArrayIndex].iRank
			ADD_BIG_FEED_JOB_CARD_DATA(FRONTEND_DETAILS_TYPE_INT, iDetailIndex, 0, "PM_RANK", "NUMBER", g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iUnplayedJobArrayIndex].iRank)
//		ELSE
//			ADD_BIG_FEED_JOB_CARD_DATA(FRONTEND_DETAILS_TYPE_2_INTS, iDetailIndex, 0, "PM_RANK", "FM_ISC_RANKB", g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iUnplayedJobArrayIndex].iRank, GET_PLAYER_RANK(PLAYER_ID()))
//		ENDIF
		iDetailIndex++
		PRINTLN("[BIGFEED] - SET_BIG_FEED_DATA_SLOT_UNPLAYED_JOB - added rank info.")
		
		// If number of players are the same, show single number
		IF g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iUnplayedJobArrayIndex].iMinPlayers = g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iUnplayedJobArrayIndex].iMaxPlayers
			ADD_BIG_FEED_JOB_CARD_DATA(FRONTEND_DETAILS_TYPE_INT, iDetailIndex, 0, "FM_ISC_NO0", "NUMBER", g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iUnplayedJobArrayIndex].iMinPlayers)
		ELSE
			ADD_BIG_FEED_JOB_CARD_DATA(FRONTEND_DETAILS_TYPE_2_INTS, iDetailIndex, 0, "FM_ISC_NO0", "FM_ISC_NO1", g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iUnplayedJobArrayIndex].iMinPlayers, g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iUnplayedJobArrayIndex].iMaxPlayers)
		ENDIF
		iDetailIndex++
		PRINTLN("[BIGFEED] - SET_BIG_FEED_DATA_SLOT_UNPLAYED_JOB - added max and min players info.")
		
		INT iMissionIcon
		IF g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iUnplayedJobArrayIndex].iType = FMMC_TYPE_RACE
			GET_PAUSE_MENU_MISSION_ICON(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iUnplayedJobArrayIndex].iType, g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iUnplayedJobArrayIndex].iSubType, iMissionIcon , g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iUnplayedJobArrayIndex].iAdversaryModeType )
		ELSE
			GET_PAUSE_MENU_MISSION_ICON(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iUnplayedJobArrayIndex].iType, g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iUnplayedJobArrayIndex].iSubType, iMissionIcon,  g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iUnplayedJobArrayIndex].iAdversaryModeType )
		ENDIF
		
		ADD_BIG_FEED_JOB_CARD_DATA(FRONTEND_DETAILS_TYPE_MISSION_TYPE, iDetailIndex, 0, "PM_TYPE", GET_MISSION_SUBTYPE_STRING_FOR_PAUSEMENU(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iUnplayedJobArrayIndex].iType, g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iUnplayedJobArrayIndex].iSubType, g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iUnplayedJobArrayIndex].iBitSet, g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iUnplayedJobArrayIndex].iBitSetTwo), iMissionIcon, iIconColour)
		iDetailIndex++
		PRINTLN("[BIGFEED] - SET_BIG_FEED_DATA_SLOT_UNPLAYED_JOB - added sub type info.")

		IF g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iUnplayedJobArrayIndex].iType = FMMC_TYPE_MISSION
			IF g_FMMC_STRUCT.iMaxNumberOfTeams > 1
				ADD_BIG_FEED_JOB_CARD_DATA(FRONTEND_DETAILS_TYPE_INT, iDetailIndex, 0, "FM_TEAMS", "NUMBER", g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iUnplayedJobArrayIndex].iMaxNumberOfTeams)
				iDetailIndex++
				PRINTLN("[BIGFEED] - SET_BIG_FEED_DATA_SLOT_UNPLAYED_JOB - added max teams info.")
			ENDIF
		ENDIF
		
		/*
		// Fill out empty slots to fill the space under the mission card
		WHILE iDetailIndex < 10
			ADD_BIG_FEED_JOB_CARD_DATA(FRONTEND_DETAILS_TYPE_LITERAL_STRING, iDetailIndex, 0, "", "")
			iDetailIndex++
		ENDWHILE
		*/
		
		IF BEGIN_SCALEFORM_MOVIE_METHOD(g_TransitionSessionNonResetVars.sPostMissionCleanupData.scaleSplash, "DISPLAY_VIEW")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
			PRINTLN("[BIGFEED] - SET_BIG_FEED_DATA_SLOT_UNPLAYED_JOB - called DISPLAY_VIEW")
			END_SCALEFORM_MOVIE_METHOD()
		ELSE
			PRINTLN("[BIGFEED] - SET_BIG_FEED_DATA_SLOT_UNPLAYED_JOB - call to DISPLAY_VIEW failed.")
		ENDIF
		
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Grabs the neccessary data to show which jobs the player has played.
FUNC BOOL SEND_BIG_FEED_PLAYER_DATA_FOR_ALL_JOBS_PLAYED()
	// Grab stats and pass to scaleform
	
	INT iLoop
	INT iMissionType
	INT iMissionSubType
	INT iTotalPlayed
	INT iTotalAvailable
		
	BIG_FEED_PLAYER_JOB_STATS theJobStat
	MPPLY_INT_STATS theJobStatToCheck
	
	BIG_FEED_JOB_DATA_ARRAY sStatPercents[BIG_FEED_MAX_MISSIONS]
	
	// Loop over our mission types.
	FOR iLoop = 0 TO ENUM_TO_INT(BIG_FEED_MAX_MISSIONS) - 1 STEP 1
	
		theJobStat = INT_TO_ENUM(BIG_FEED_PLAYER_JOB_STATS, iLoop)
		
		GET_BIG_FEED_MISSION_TYPE_AND_SUB_TYPE_FROM_ENUM(theJobStat, iMissionType, iMissionSubType)
		
		PRINTLN("[BIGFEED] - SEND_BIG_FEED_PLAYER_DATA_FOR_CONTACT_JOBS - checking iMissionType = ", iMissionType, " and iMissionSubType = ", iMissionSubType)
		
		// Grab the total jobs played, created then verified
		theJobStatToCheck = GET_BIG_FEED_STAT_FOR_JOBS_PLAYED(iMissionType, iMissionSubType)
		iTotalPlayed = GET_MP_INT_PLAYER_STAT(theJobStatToCheck)
		
		theJobStatToCheck = GET_BIG_FEED_STAT_FOR_JOBS_PLAYED(iMissionType, iMissionSubType, TRUE)
		iTotalPlayed += GET_MP_INT_PLAYER_STAT(theJobStatToCheck)
		
		// Grab the total jobs available, created then verified
		theJobStatToCheck = GET_BIG_FEED_STAT_FOR_JOBS_AVAILABLE(iMissionType, iMissionSubType)
		iTotalAvailable = GET_MP_INT_PLAYER_STAT(theJobStatToCheck)
		
		theJobStatToCheck = GET_BIG_FEED_STAT_FOR_JOBS_AVAILABLE(iMissionType, iMissionSubType, TRUE)
		iTotalAvailable += GET_MP_INT_PLAYER_STAT(theJobStatToCheck)
		
		sStatPercents[iLoop].iTheStat = ENUM_TO_INT(theJobStat)
		sStatPercents[iLoop].iPlayed = iTotalPlayed
		sStatPercents[iLoop].iAvailable = iTotalAvailable
		sStatPercents[iLoop].fPercent = TO_FLOAT(iTotalPlayed)/TO_FLOAT(iTotalAvailable)
		
		PRINTLN("[BIGFEED] - SEND_BIG_FEED_PLAYER_DATA_FOR_ALL_JOBS_PLAYED - iTheStat = ", sStatPercents[iLoop].iTheStat)
		PRINTLN("[BIGFEED] - SEND_BIG_FEED_PLAYER_DATA_FOR_ALL_JOBS_PLAYED - fPercent = ", sStatPercents[iLoop].fPercent)
		PRINTLN("[BIGFEED] - SEND_BIG_FEED_PLAYER_DATA_FOR_ALL_JOBS_PLAYED - iPlayed = ", sStatPercents[iLoop].iPlayed)
		PRINTLN("[BIGFEED] - SEND_BIG_FEED_PLAYER_DATA_FOR_ALL_JOBS_PLAYED - iAvailable = ", sStatPercents[iLoop].iAvailable)
		
		IF sStatPercents[iLoop].iAvailable = 0
			PRINTLN("[BIGFEED] - SEND_BIG_FEED_PLAYER_DATA_FOR_ALL_JOBS_PLAYED - for some reason a job type has returned that there are no available jobs. Don't want to send incorrect data to the big message. Bailing and returning FALSE.")
			RETURN FALSE
		ENDIF
				
	ENDFOR
	

	INT iMinIndex
	INT iLoopJ
	INT iIcon
	BIG_FEED_JOB_DATA_ARRAY sTempEntry
	
	// Re-order the Job stats with least played first
	
	FOR iLoop = 0 TO ENUM_TO_INT(BIG_FEED_MAX_MISSIONS) - 1 STEP 1
		
		iMinIndex = iLoop
		
		// Find out smallest value stat
		FOR iLoopJ = iLoop+1 TO ENUM_TO_INT(BIG_FEED_MAX_MISSIONS) - 1 STEP 1
			
			IF sStatPercents[iLoopJ].fPercent < sStatPercents[iMinIndex].fPercent
				iMinIndex = iLoopJ
			ENDIF
		ENDFOR
		
		// Switch entries so we won't process them again
		sTempEntry = sStatPercents[iLoop]
		sStatPercents[iLoop] = sStatPercents[iMinIndex]
		sStatPercents[iMinIndex] = sTempEntry
		
		// Grab the neccessary data and pass over to scaleform
		GET_BIG_FEED_MISSION_TYPE_AND_SUB_TYPE_FROM_ENUM(INT_TO_ENUM(BIG_FEED_PLAYER_JOB_STATS, sStatPercents[iLoop].iTheStat), iMissionType, iMissionSubType)
		
		// If we are a Race, remove subtype since we just want a generic 'Race' category
		IF iMissionType = FMMC_TYPE_RACE
			iMissionSubType = -1
		ENDIF
		
		// Grab the icon
		GET_PAUSE_MENU_MISSION_ICON(iMissionType, iMissionSubType, iIcon)
		
		PRINTLN("[BIGFEED] SEND_BIG_FEED_PLAYER_DATA_FOR_ALL_JOBS_PLAYED - ", GET_FILENAME_FOR_AUDIO_CONVERSATION(GET_MISSION_SUBTYPE_STRING_FOR_PAUSEMENU(iMissionType, iMissionSubType, 0, 0)),
				": ", sStatPercents[iLoop].iPlayed, "/", sStatPercents[iLoop].iAvailable)
		
		// Check our counts are valid. Prevent us from sending greater than 1 ratio
		IF sStatPercents[iLoop].iPlayed > sStatPercents[iLoop].iAvailable
			SCRIPT_ASSERT("[BIGFEED] iTotalPlayed > iTotalAvailable. Player played count is greater than the total active of this Job type.")
			sStatPercents[iLoop].iPlayed = sStatPercents[iLoop].iAvailable
		ENDIF
				
		SET_BIG_FEED_DATA_SLOT_JOB_PLAYED(iLoop, sStatPercents[iLoop].iPlayed, sStatPercents[iLoop].iAvailable, GET_MISSION_SUBTYPE_STRING_FOR_PAUSEMENU(iMissionType, iMissionSubType, 0, 0, TRUE), iIcon)
		
	ENDFOR
	
	RETURN TRUE
	
ENDFUNC

FUNc BOOL GET_HAS_PLAYER_UNLOCKED_CONTACT(enumCharacterList eContact)
	
	INT iMartinStat
	BOOL bReturnValue
	
	SWITCH eContact
		
		CASE CHAR_MP_GERALD
			bReturnValue = HAS_GOTO_JOB_CUTSCENE_BEEN_DONE()
			PRINTLN("[BIGFEED] - GET_HAS_PLAYER_UNLOCKED_CONTACT - returning value for CHAR_MP_GERALD. Unlocked = ", bReturnValue)
			RETURN bReturnValue
		CASE CHAR_LAMAR
			bReturnValue = HAS_GOTO_JOB_CUTSCENE_BEEN_DONE() 
			PRINTLN("[BIGFEED] - GET_HAS_PLAYER_UNLOCKED_CONTACT - returning value for CHAR_LAMAR. Unlocked = ", bReturnValue)
			RETURN bReturnValue
		CASE CHAR_SIMEON
			bReturnValue = HAS_GOTO_JOB_CUTSCENE_BEEN_DONE() 
			PRINTLN("[BIGFEED] - GET_HAS_PLAYER_UNLOCKED_CONTACT - returning value for CHAR_SIMEON. Unlocked = ", bReturnValue)
			RETURN bReturnValue
		CASE CHAR_LESTER
			bReturnValue = HAS_LESTER_CUTSCENE_BEEN_DONE()
			PRINTLN("[BIGFEED] - GET_HAS_PLAYER_UNLOCKED_CONTACT - returning value for CHAR_LESTER. Unlocked = ", bReturnValue)
			RETURN bReturnValue
		CASE CHAR_MARTIN
			iMartinStat = GET_MP_INT_CHARACTER_STAT(MP_STAT_FM_ACT_PHN)
			bReturnValue = IS_BIT_SET(iMartinStat, FMMC_TYPE_GANGHIDEOUT)
			PRINTLN("[BIGFEED] - GET_HAS_PLAYER_UNLOCKED_CONTACT - returning value for CHAR_MARTIN. Unlocked = ", bReturnValue)
			RETURN bReturnValue
		CASE CHAR_RON
			bReturnValue = HAS_RON_AND_TREVOR_CUTSCENE_BEEN_DONE()
			PRINTLN("[BIGFEED] - GET_HAS_PLAYER_UNLOCKED_CONTACT - returning value for CHAR_RON. Unlocked = ", bReturnValue)
			RETURN bReturnValue
		CASE CHAR_TREVOR
 			bReturnValue = HAS_RON_AND_TREVOR_CUTSCENE_BEEN_DONE()
			PRINTLN("[BIGFEED] - GET_HAS_PLAYER_UNLOCKED_CONTACT - returning value for CHAR_TREVOR. Unlocked = ", bReturnValue)
			RETURN bReturnValue
		DEFAULT
			bReturnValue = FALSE
			PRINTLN("[BIGFEED] - GET_HAS_PLAYER_UNLOCKED_CONTACT - default being returned. How did this happen? There's no valid char to check. Unlocked = ", bReturnValue)
			RETURN bReturnValue
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC

/// PURPOSE:
///    Grabs the neccessary data for players contact missions and passes to scaleform
FUNC BOOL SEND_BIG_FEED_PLAYER_DATA_FOR_CONTACT_JOBS()
	
	INT iLoop
	enumCharacterList theContactEnum
	
	TEXT_LABEL strContactName
	TEXT_LABEL strHeadshotTxd
	INT iTotalPlayed
	INT iTotalAvailable
		
	g_eFMHeistContactIDs theContactStat
	MPPLY_INT_STATS theJobStatToCheck
	
	BIG_FEED_JOB_DATA_ARRAY sStatPercents[MAX_FM_HEIST_CONTACTS]
	
	INT iTotalChars = 0
	// Loop over our mission types.
	FOR iLoop = 0 TO ENUM_TO_INT(MAX_FM_HEIST_CONTACTS) - 1 STEP 1
	
		theContactStat = INT_TO_ENUM(g_eFMHeistContactIDs, iLoop)
		
		// Grab our contact character
		theContactEnum = Convert_FM_Heist_ContactID_To_CharacterListID(theContactStat)
		
		PRINTLN("[BIGFEED] - SEND_BIG_FEED_PLAYER_DATA_FOR_CONTACT_JOBS - iLoop = ", iLoop)
		PRINTLN("[BIGFEED] - SEND_BIG_FEED_PLAYER_DATA_FOR_CONTACT_JOBS - theContactStat = ", ENUM_TO_INT(theContactStat))
		PRINTLN("[BIGFEED] - SEND_BIG_FEED_PLAYER_DATA_FOR_CONTACT_JOBS - theContactEnum = ", ENUM_TO_INT(theContactEnum))
		
		// If the character is valid
		IF theContactEnum != NO_CHARACTER
			// IF HAS_PLAYER_UNLOCKED_CONTACT(PLAYER_ID(), theContactEnum)
			IF GET_HAS_PLAYER_UNLOCKED_CONTACT(theContactEnum)
				
				PRINTLN("[BIGFEED] - SEND_BIG_FEED_PLAYER_DATA_FOR_CONTACT_JOBS - HAS_PLAYER_UNLOCKED_CONTACT = FALSE - Contact: ", g_sCharacterSheetAll[theContactEnum].label, " is unlocked")
				
				theJobStatToCheck = GET_BIG_FEED_STAT_FOR_CONTACT_PLAYED(theContactEnum)
				iTotalPlayed = GET_MP_INT_PLAYER_STAT(theJobStatToCheck)
				
				theJobStatToCheck = GET_BIG_FEED_STAT_FOR_CONTACT_AVAILABLE(theContactEnum)
				iTotalAvailable = GET_MP_INT_PLAYER_STAT(theJobStatToCheck)
				
				sStatPercents[iLoop].iTheStat = ENUM_TO_INT(theContactStat)
				sStatPercents[iLoop].fPercent = TO_FLOAT(iTotalPlayed)/TO_FLOAT(iTotalAvailable)
				sStatPercents[iLoop].iPlayed = iTotalPlayed
				sStatPercents[iLoop].iAvailable = iTotalAvailable
				
				PRINTLN("[BIGFEED] - SEND_BIG_FEED_PLAYER_DATA_FOR_CONTACT_JOBS - iTheStat = ", sStatPercents[iLoop].iTheStat)
				PRINTLN("[BIGFEED] - SEND_BIG_FEED_PLAYER_DATA_FOR_CONTACT_JOBS - fPercent = ", sStatPercents[iLoop].fPercent)
				PRINTLN("[BIGFEED] - SEND_BIG_FEED_PLAYER_DATA_FOR_CONTACT_JOBS - iPlayed = ", sStatPercents[iLoop].iPlayed)
				PRINTLN("[BIGFEED] - SEND_BIG_FEED_PLAYER_DATA_FOR_CONTACT_JOBS - iAvailable = ", sStatPercents[iLoop].iAvailable)
				
				IF sStatPercents[iLoop].iAvailable = 0
					PRINTLN("[BIGFEED] - SEND_BIG_FEED_PLAYER_DATA_FOR_CONTACT_JOBS - for some reason an unlocked contact has returned that there are no available jobs. Don;t want to send incorrect data to the big message. Bailing and returning FALSE.")
					RETURN FALSE
				ENDIF
				
				iTotalChars++
				
			#IF IS_DEBUG_BUILD
			ELSE
				IF NOT HAS_PLAYER_UNLOCKED_CONTACT(PLAYER_ID(), theContactEnum)
					PRINTLN("[BIGFEED] SEND_BIG_FEED_PLAYER_DATA_FOR_CONTACT_JOBS - HAS_PLAYER_UNLOCKED_CONTACT = FALSE - Contact: ", g_sCharacterSheetAll[theContactEnum].label, " is not unlocked")
				ENDIF
			#ENDIF
			ENDIF
		#IF IS_DEBUG_BUILD
		ELSE
			IF NOT HAS_PLAYER_UNLOCKED_CONTACT(PLAYER_ID(), theContactEnum)
				PRINTLN("[BIGFEED] SEND_BIG_FEED_PLAYER_DATA_FOR_CONTACT_JOBS - NO_CHARACTER - Contact: ", g_sCharacterSheetAll[theContactEnum].label, " is not unlocked")
			ENDIF
		#ENDIF
		ENDIF
	ENDFOR
	
	
	INT iMinIndex
	INT iLoopJ
	BIG_FEED_JOB_DATA_ARRAY sTempEntry
	
	FOR iLoop = 0 TO iTotalChars - 1 STEP 1
		
		iMinIndex = iLoop
		
		// Find out smallest value stat
		FOR iLoopJ = iLoop+1 TO iTotalChars - 1 STEP 1
			
			IF sStatPercents[iLoopJ].fPercent < sStatPercents[iMinIndex].fPercent
				iMinIndex = iLoopJ
			ENDIF
		ENDFOR
		
		// Switch entries so we won't process them again
		sTempEntry = sStatPercents[iLoop]
		sStatPercents[iLoop] = sStatPercents[iMinIndex]
		sStatPercents[iMinIndex] = sTempEntry
		
		// Grab our contact character
		theContactEnum = Convert_FM_Heist_ContactID_To_CharacterListID(INT_TO_ENUM(g_eFMHeistContactIDs, sStatPercents[iLoop].iTheStat))
			
		strContactName = g_sCharacterSheetAll[theContactEnum].label
		strHeadshotTxd = GET_FILENAME_FOR_AUDIO_CONVERSATION(g_sCharacterSheetAll[theContactEnum].picture)
				
		PRINTLN("[BIGFEED] SEND_BIG_FEED_PLAYER_DATA_FOR_CONTACT_JOBS - Contact: ", GET_FILENAME_FOR_AUDIO_CONVERSATION(strContactName), ", Headshot Dict: ", strHeadshotTxd,
				": ", sStatPercents[iLoop].iPlayed, "/", sStatPercents[iLoop].iAvailable)			
			
		// Check our counts are valid. Prevent us from sending greater than 1 ratio
		IF sStatPercents[iLoop].iPlayed > sStatPercents[iLoop].iAvailable
			SCRIPT_ASSERT("[BIGFEED] iTotalPlayed > iTotalAvailable. Player contact played count is greater than the total active of this Contact Mission type.")
			sStatPercents[iLoop].iPlayed = sStatPercents[iLoop].iAvailable
		ENDIF
		
		SET_BIG_FEED_DATA_SLOT_CONTACT_MISSIONS_PLAYED(iLoop, sStatPercents[iLoop].iPlayed, sStatPercents[iLoop].iAvailable, strContactName, strHeadshotTxd)
		
	ENDFOR
	
	RETURN TRUE
	
ENDFUNC


/// PURPOSE:
///    Maintain the update of Job played stats. This is triggered from within fm_mission_info.sch
PROC MAINTAIN_BIG_FEED_STAT_UPDATES()
	
	IF g_TransitionSessionNonResetVars.sDynamicBigFeedData.g_eBigFeedUpdateStruct.iMissionType != -1
		
		PRINTLN("[BIGFEED] MAINTAIN_BIG_FEED_STAT_UPDATES - Job has been played. iMissionType = ", g_TransitionSessionNonResetVars.sDynamicBigFeedData.g_eBigFeedUpdateStruct.iMissionType, ", iMissionSubType = ", g_TransitionSessionNonResetVars.sDynamicBigFeedData.g_eBigFeedUpdateStruct.iMissionSubType, ", bVerified = ", PICK_STRING(g_TransitionSessionNonResetVars.sDynamicBigFeedData.g_eBigFeedUpdateStruct.bVerified, "TRUE", "FALSE"))
		
		// Updated our Job played stats
		UPDATE_BIG_FEED_JOB_PLAYED_PLAYER_STATS(g_TransitionSessionNonResetVars.sDynamicBigFeedData.g_eBigFeedUpdateStruct.iMissionType, g_TransitionSessionNonResetVars.sDynamicBigFeedData.g_eBigFeedUpdateStruct.iMissionSubType, TRUE, g_TransitionSessionNonResetVars.sDynamicBigFeedData.g_eBigFeedUpdateStruct.iRank, g_TransitionSessionNonResetVars.sDynamicBigFeedData.g_eBigFeedUpdateStruct.bVerified)
	
		// Update our contact missions if needed
		UPDATE_BIG_FEED_CONTACT_JOB_PLAYED_STATS(	g_TransitionSessionNonResetVars.sDynamicBigFeedData.g_eBigFeedUpdateStruct.iMissionType, g_TransitionSessionNonResetVars.sDynamicBigFeedData.g_eBigFeedUpdateStruct.iMissionSubType,
													g_TransitionSessionNonResetVars.sDynamicBigFeedData.g_eBigFeedUpdateStruct.iContactInt, g_TransitionSessionNonResetVars.sDynamicBigFeedData.g_eBigFeedUpdateStruct.iRank,
													TRUE )
		
	
		g_TransitionSessionNonResetVars.sDynamicBigFeedData.g_eBigFeedUpdateStruct.iMissionType	= -1
		g_TransitionSessionNonResetVars.sDynamicBigFeedData.g_eBigFeedUpdateStruct.iMissionSubType = -1
		g_TransitionSessionNonResetVars.sDynamicBigFeedData.g_eBigFeedUpdateStruct.bVerified = FALSE
		g_TransitionSessionNonResetVars.sDynamicBigFeedData.g_eBigFeedUpdateStruct.iRank = -1
		g_TransitionSessionNonResetVars.sDynamicBigFeedData.g_eBigFeedUpdateStruct.iContactInt = -1
	ENDIF
	
ENDPROC

PROC MAINTAIN_UNPLAYED_JOB_FEED_DATA(STRUCT_DL_PHOTO_VARS_LITE &photoVars, CACHED_MISSION_DESCRIPTION_LOAD_VARS &sBigFeedMissionDescVars, INT &iUnplayedJobArrayIndex)
	
	IF IS_BIT_SET(g_TransitionSessionNonResetVars.sDynamicBigFeedData.g_iBigFeedGrabUnplayedJobBitset, BITSET_UNPLAYED_JOB_BIG_FEED_NEED_MISSION_DATA)
		GET_ALL_DATA_NEED_FOR_JOB_CARD(photoVars, sBigFeedMissionDescVars, iUnplayedJobArrayIndex)
	ENDIF
	
ENDPROC

FUNC BOOL SEND_UNPLAYED_JOB_DATA_TO_MOVIE_ONCE_READY(STRUCT_DL_PHOTO_VARS_LITE &sBigFeedUnplayedJobPhotoData, INT iUnplayedJobArrayIndex)
	
	IF IS_BIT_SET(g_TransitionSessionNonResetVars.sDynamicBigFeedData.g_iBigFeedGrabUnplayedJobBitset, BITSET_UNPLAYED_JOB_BIG_FEED_GOT_ALL_DATA)
		SET_BIG_FEED_DATA_SLOT_UNPLAYED_JOB(sBigFeedUnplayedJobPhotoData, iUnplayedJobArrayIndex)
		CLEAR_BIT(g_TransitionSessionNonResetVars.sDynamicBigFeedData.g_iBigFeedGrabUnplayedJobBitset, BITSET_UNPLAYED_JOB_BIG_FEED_NEED_MISSION_DATA)
		CLEAR_BIT(g_TransitionSessionNonResetVars.sDynamicBigFeedData.g_iBigFeedGrabUnplayedJobBitset, BITSET_UNPLAYED_JOB_BIG_FEED_GOT_ALL_DATA)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL REQUIRED_BIG_FEED_UGC_COMPLETE(INT iBigFeedScreenToShow)
	
	IF iBigFeedScreenToShow != ciBIG_FEED_UNPLAYED_JOB
		RETURN TRUE
	ENDIF
	
	IF GET_FM_UGC_INITIAL_HAS_FINISHED()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

/// PURPOSE:
///    Maintain the display of big feed messages that require data from script.
PROC MAINTAIN_BIG_FEED_PLAYER_DATA_SCREENS(STRUCT_DL_PHOTO_VARS_LITE &sBigFeedUnplayedJobPhotoData, CACHED_MISSION_DESCRIPTION_LOAD_VARS &sBigFeedMissionDescVars, INT &iUnplayedJobArrayIndex)
	
	BOOL bSendData
	INT iBigFeedScreenToShow
	
	// Can;t use globals until they have loaded. 
	IF NOT HAS_UGC_GLOBAL_BLOCK_LOADED()
		EXIT
	ENDIF
	
	// Update the Job stats as we complete them
	MAINTAIN_BIG_FEED_STAT_UPDATES()
	MAINTAIN_UNPLAYED_JOB_FEED_DATA(sBigFeedUnplayedJobPhotoData, sBigFeedMissionDescVars, iUnplayedJobArrayIndex)
	
	// Check the feed is being displayed
	IF g_IsNewsFeedDisplaying
		
		//...yes, is a screen being shown that contains data we need to check, and we have not yet populated it
		IF SC_TRANSITION_NEWS_HAS_EXTRA_DATA_TU()
		
			//...yes, check the name from scaleform and then switch on index for which screen we are showing
			IF SC_TRANSITION_NEWS_GET_EXTRA_DATA_INT_TU("scriptDisplay", iBigFeedScreenToShow)
				
//				iBigFeedScreenToShow = ciBIG_FEED_UNPLAYED_JOB
				
				IF REQUIRED_BIG_FEED_UGC_COMPLETE(iBigFeedScreenToShow)
				
					IF iBigFeedScreenToShow != g_TransitionSessionNonResetVars.sDynamicBigFeedData.g_iLastBigFeedScreenShown
						
//						iBigFeedScreenToShow = ciBIG_FEED_UNPLAYED_JOB
						
						#IF IS_DEBUG_BUILD
							IF g_bDebugSkipBigFeedScreen
								PRINTLN("[BIGFEED] MAINTAIN_BIG_FEED_PLAYER_DATA_SCREENS - g_bDebugSkipBigFeedScreen = TRUE. Move on")
								PRINTLN("[BIGFEED] - MAINTAIN_BIG_FEED_PLAYER_DATA_SCREENS - called SC_TRANSITION_NEWS_SHOW_NEXT_ITEM.")
								IF SC_TRANSITION_NEWS_SHOW_NEXT_ITEM()
									g_TransitionSessionNonResetVars.sDynamicBigFeedData.g_iLastBigFeedScreenShown = iBigFeedScreenToShow
									PRINTLN("[BIGFEED] - MAINTAIN_BIG_FEED_PLAYER_DATA_SCREENS - call to SC_TRANSITION_NEWS_SHOW_NEXT_ITEM was successful.")
								ELSE
									PRINTLN("[BIGFEED] - MAINTAIN_BIG_FEED_PLAYER_DATA_SCREENS - call to SC_TRANSITION_NEWS_SHOW_NEXT_ITEM was not successful.")
								ENDIF
								EXIT
							ENDIF
						#ENDIF
						
						IF (iBigFeedScreenToShow != ciBIG_FEED_UNPLAYED_JOB)
							
							// We need to check stats have loaded so we have the data to pass to the scaleform
							IF NOT HAS_IMPORTANT_STATS_LOADED()
								PRINTLN("[BIGFEED] MAINTAIN_BIG_FEED_PLAYER_DATA_SCREENS - Stats have not loaded. Move on")
								PRINTLN("[BIGFEED] - MAINTAIN_BIG_FEED_PLAYER_DATA_SCREENS - called SC_TRANSITION_NEWS_SHOW_NEXT_ITEM.")
								IF SC_TRANSITION_NEWS_SHOW_NEXT_ITEM()
									g_TransitionSessionNonResetVars.sDynamicBigFeedData.g_iLastBigFeedScreenShown = iBigFeedScreenToShow
									PRINTLN("[BIGFEED] - MAINTAIN_BIG_FEED_PLAYER_DATA_SCREENS - call to SC_TRANSITION_NEWS_SHOW_NEXT_ITEM was successful.")
								ELSE
									PRINTLN("[BIGFEED] - MAINTAIN_BIG_FEED_PLAYER_DATA_SCREENS - call to SC_TRANSITION_NEWS_SHOW_NEXT_ITEM was not successful.")
								ENDIF
								EXIT
							ENDIF
							
							// We need to check the stats have been saved at all (first time through this will be FALSE)
							IF NOT GET_MP_BOOL_PLAYER_STAT(MPPLY_BIG_FEED_INIT)
								PRINTLN("[BIGFEED] MAINTAIN_BIG_FEED_PLAYER_DATA_SCREENS - Stats have not been saved. Move on")
								PRINTLN("[BIGFEED] - MAINTAIN_BIG_FEED_PLAYER_DATA_SCREENS - called SC_TRANSITION_NEWS_SHOW_NEXT_ITEM.")
								IF SC_TRANSITION_NEWS_SHOW_NEXT_ITEM()
									g_TransitionSessionNonResetVars.sDynamicBigFeedData.g_iLastBigFeedScreenShown = iBigFeedScreenToShow
									PRINTLN("[BIGFEED] - MAINTAIN_BIG_FEED_PLAYER_DATA_SCREENS - call to SC_TRANSITION_NEWS_SHOW_NEXT_ITEM was successful.")
								ELSE
									PRINTLN("[BIGFEED] - MAINTAIN_BIG_FEED_PLAYER_DATA_SCREENS - call to SC_TRANSITION_NEWS_SHOW_NEXT_ITEM was not successful.")
								ENDIF
								EXIT
							ENDIF
							
						ELSE
							
							IF NOT GET_FM_UGC_INITIAL_HAS_FINISHED()
								PRINTLN("[BIGFEED] MAINTAIN_BIG_FEED_PLAYER_DATA_SCREENS - GET_FM_UGC_INITIAL_HAS_FINISHED = FALSE. Move on")
								PRINTLN("[BIGFEED] - MAINTAIN_BIG_FEED_PLAYER_DATA_SCREENS - called SC_TRANSITION_NEWS_SHOW_NEXT_ITEM.")
								IF SC_TRANSITION_NEWS_SHOW_NEXT_ITEM()
									g_TransitionSessionNonResetVars.sDynamicBigFeedData.g_iLastBigFeedScreenShown = iBigFeedScreenToShow
									PRINTLN("[BIGFEED] - MAINTAIN_BIG_FEED_PLAYER_DATA_SCREENS - call to SC_TRANSITION_NEWS_SHOW_NEXT_ITEM was successful.")
								ELSE
									PRINTLN("[BIGFEED] - MAINTAIN_BIG_FEED_PLAYER_DATA_SCREENS - call to SC_TRANSITION_NEWS_SHOW_NEXT_ITEM was not successful.")
								ENDIF
								EXIT
							ENDIF
						
						ENDIF
						
//						iBigFeedScreenToShow = ciBIG_FEED_UNPLAYED_JOB
						
						PRINTLN("[BIGFEED] MAINTAIN_BIG_FEED_PLAYER_DATA_SCREENS: received extra data for ", iBigFeedScreenToShow)
						
						// Set last feed screen shown as the screen we are about to populate
						g_TransitionSessionNonResetVars.sDynamicBigFeedData.g_iLastBigFeedScreenShown = iBigFeedScreenToShow
						
						// Change the context of the movie to prepare for data we are about to populate it with
						CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(g_TransitionSessionNonResetVars.sPostMissionCleanupData.scaleSplash, "SET_NEWS_CONTEXT", TO_FLOAT(iBigFeedScreenToShow)) // ciBIG_FEED_UNPLAYED_JOB)
						PRINTLN("[BIGFEED] - called SET_NEWS_CONTEXT with ciBIG_FEED_UNPLAYED_JOB = ", iBigFeedScreenToShow)
						
	//					iBigFeedScreenToShow = ciBIG_FEED_UNPLAYED_JOB
						
						IF iBigFeedScreenToShow != ciBIG_FEED_UNPLAYED_JOB
							bSendData = TRUE
						ELSE
							SET_BIT(g_TransitionSessionNonResetVars.sDynamicBigFeedData.g_iBigFeedGrabUnplayedJobBitset, BITSET_UNPLAYED_JOB_BIG_FEED_NEED_MISSION_DATA) // If we are doing the unplayed job, set flag to get data for it.
						ENDIF
						
//					ELSE
//						PRINTLN("[BIGFEED] - iBigFeedScreenToShow != g_TransitionSessionNonResetVars.sDynamicBigFeedData.g_iLastBigFeedScreenShown. iBigFeedScreenToShow = ", iBigFeedScreenToShow, ", g_iLastBigFeedScreenShown = ", g_TransitionSessionNonResetVars.sDynamicBigFeedData.g_iLastBigFeedScreenShown)
					ENDIF	
				ELSE
					PRINTLN("[BIGFEED] - REQUIRED_BIG_FEED_UGC_COMPLETE = FALSE, cannot populate movie data yet.")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	// Once we have the data for the unplayed job big feed, send it to the movie.
	SEND_UNPLAYED_JOB_DATA_TO_MOVIE_ONCE_READY(sBigFeedUnplayedJobPhotoData, iUnplayedJobArrayIndex)
	
	IF bSendData
		SWITCH iBigFeedScreenToShow
			CASE ciBIG_FEED_ALL_JOBS_PLAYED
				IF NOT SEND_BIG_FEED_PLAYER_DATA_FOR_ALL_JOBS_PLAYED()
					PRINTLN("[BIGFEED] - MAINTAIN_BIG_FEED_PLAYER_DATA_SCREENS - SEND_BIG_FEED_PLAYER_DATA_FOR_ALL_JOBS_PLAYED has returned FALSE, skip displaying all jobs played screen and show something else.")
					PRINTLN("[BIGFEED] - MAINTAIN_BIG_FEED_PLAYER_DATA_SCREENS - calling SC_TRANSITION_NEWS_SHOW_NEXT_ITEM.")
					IF SC_TRANSITION_NEWS_SHOW_NEXT_ITEM()
						g_TransitionSessionNonResetVars.sDynamicBigFeedData.g_iLastBigFeedScreenShown = iBigFeedScreenToShow
						PRINTLN("[BIGFEED] - MAINTAIN_BIG_FEED_PLAYER_DATA_SCREENS - call to SC_TRANSITION_NEWS_SHOW_NEXT_ITEM was successful.")
					ELSE
						PRINTLN("[BIGFEED] - MAINTAIN_BIG_FEED_PLAYER_DATA_SCREENS - call to SC_TRANSITION_NEWS_SHOW_NEXT_ITEM was not successful.")
					ENDIF
					EXIT
				ENDIF
			BREAK
			
			CASE ciBIG_FEED_CONTACT_JOBS_PLAYED
				IF NOT SEND_BIG_FEED_PLAYER_DATA_FOR_CONTACT_JOBS()
					PRINTLN("[BIGFEED] - MAINTAIN_BIG_FEED_PLAYER_DATA_SCREENS - SEND_BIG_FEED_PLAYER_DATA_FOR_CONTACT_JOBS has returned FALSE, skip displaying contact jobs screen and show something else.")
					PRINTLN("[BIGFEED] - MAINTAIN_BIG_FEED_PLAYER_DATA_SCREENS - calling SC_TRANSITION_NEWS_SHOW_NEXT_ITEM.")
					IF SC_TRANSITION_NEWS_SHOW_NEXT_ITEM()
						g_TransitionSessionNonResetVars.sDynamicBigFeedData.g_iLastBigFeedScreenShown = iBigFeedScreenToShow
						PRINTLN("[BIGFEED] - MAINTAIN_BIG_FEED_PLAYER_DATA_SCREENS - call to SC_TRANSITION_NEWS_SHOW_NEXT_ITEM was successful.")
					ELSE
						PRINTLN("[BIGFEED] - MAINTAIN_BIG_FEED_PLAYER_DATA_SCREENS - call to SC_TRANSITION_NEWS_SHOW_NEXT_ITEM was not successful.")
					ENDIF
					EXIT
				ENDIF
			BREAK
				
			CASE ciBIG_FEED_UNPLAYED_JOB
				// Done above.
			BREAK
			
			// Add any other screen we need to handle for scaleform.
		ENDSWITCH
	ENDIF
			
	#IF IS_DEBUG_BUILD
		IF g_bDisplayBigFeedJobCounts
			
			IF HAS_IMPORTANT_STATS_LOADED()
				
				PRINTLN("[BIGFEED] Player Played stats (R* Created):")
				PRINTLN("[BIGFEED] 		Race: 		", GET_MP_INT_PLAYER_STAT(MPPLY_UNIQUE_RACES), " / ", GET_MP_INT_PLAYER_STAT(MPPLY_AVAILABLE_RACES))
				PRINTLN("[BIGFEED] 		DMs: 		", GET_MP_INT_PLAYER_STAT(MPPLY_UNIQUE_DMS), " / ", GET_MP_INT_PLAYER_STAT(MPPLY_AVAILABLE_DMS))
				PRINTLN("[BIGFEED] 		Para: 		", GET_MP_INT_PLAYER_STAT(MPPLY_UNIQUE_PARAS), " / ", GET_MP_INT_PLAYER_STAT(MPPLY_AVAILABLE_PARAS))
				PRINTLN("[BIGFEED] 		Survival: 	", GET_MP_INT_PLAYER_STAT(MPPLY_UNIQUE_SURVIVAL), " / ", GET_MP_INT_PLAYER_STAT(MPPLY_AVAILABLE_SURVIVAL))
				PRINTLN("[BIGFEED] 		Missions: 	", GET_MP_INT_PLAYER_STAT(MPPLY_UNIQUE_MISSIONS), " / ", GET_MP_INT_PLAYER_STAT(MPPLY_AVAILABLE_MISSIONS))
				PRINTLN("[BIGFEED] 		LTS: 		", GET_MP_INT_PLAYER_STAT(MPPLY_UNIQUE_LTS), " / ", GET_MP_INT_PLAYER_STAT(MPPLY_AVAILABLE_LTS))
				PRINTLN("[BIGFEED] 		CTF: 		", GET_MP_INT_PLAYER_STAT(MPPLY_UNIQUE_CTF), " / ", GET_MP_INT_PLAYER_STAT(MPPLY_AVAILABLE_CTF))
				PRINTLN("[BIGFEED] 		Versus: 	", GET_MP_INT_PLAYER_STAT(MPPLY_UNIQUE_VERSUS), " / ", GET_MP_INT_PLAYER_STAT(MPPLY_AVAILABLE_VERSUS))
				
				PRINTLN("[BIGFEED] 		Heist Planning: ", GET_MP_INT_PLAYER_STAT(MPPLY_UNIQUE_HEIST_PLANNING), " / ", GET_MP_INT_PLAYER_STAT(MPPLY_AVAILABLE_HEIST_PLAN))
				PRINTLN("[BIGFEED] 		Heist Finale: 	", GET_MP_INT_PLAYER_STAT(MPPLY_UNIQUE_HEIST_FINALE), " / ", GET_MP_INT_PLAYER_STAT(MPPLY_AVAILABLE_HEIST_FINALE))
				
				PRINTLN("[BIGFEED] Player Played stats (R* Verified):")
				PRINTLN("[BIGFEED] 		Race: 		", GET_MP_INT_PLAYER_STAT(MPPLY_UNIQUE_RACES_V), " / ", GET_MP_INT_PLAYER_STAT(MPPLY_AVAILABLE_RACES_V))
				PRINTLN("[BIGFEED] 		DMs: 		", GET_MP_INT_PLAYER_STAT(MPPLY_UNIQUE_DMS_V), " / ", GET_MP_INT_PLAYER_STAT(MPPLY_AVAILABLE_DMS_V))
				PRINTLN("[BIGFEED] 		Para: 		", GET_MP_INT_PLAYER_STAT(MPPLY_UNIQUE_PARAS_V), " / ", GET_MP_INT_PLAYER_STAT(MPPLY_AVAILABLE_PARAS_V))
				PRINTLN("[BIGFEED] 		Survival: 	", GET_MP_INT_PLAYER_STAT(MPPLY_UNIQUE_SURVIVAL_V), " / ", GET_MP_INT_PLAYER_STAT(MPPLY_AVAILABLE_SURVIVAL_V))
				PRINTLN("[BIGFEED] 		Missions: 	", GET_MP_INT_PLAYER_STAT(MPPLY_UNIQUE_MISSIONS_V), " / ", GET_MP_INT_PLAYER_STAT(MPPLY_AVAILABLE_MISSIONS_V))
				PRINTLN("[BIGFEED] 		LTS: 		", GET_MP_INT_PLAYER_STAT(MPPLY_UNIQUE_LTS_V), " / ", GET_MP_INT_PLAYER_STAT(MPPLY_AVAILABLE_LTS_V))
				PRINTLN("[BIGFEED] 		CTF: 		", GET_MP_INT_PLAYER_STAT(MPPLY_UNIQUE_CTF_V), " / ", GET_MP_INT_PLAYER_STAT(MPPLY_AVAILABLE_CTF_V))
				PRINTLN("[BIGFEED] 		Versus: 	", GET_MP_INT_PLAYER_STAT(MPPLY_UNIQUE_VERSUS_V), " / ", GET_MP_INT_PLAYER_STAT(MPPLY_AVAILABLE_VERSUS_V))
				
				PRINTLN("[BIGFEED] 		Heist Planning: 	", GET_MP_INT_PLAYER_STAT(MPPLY_UNIQUE_HEIST_PLANNING_V), " / ", GET_MP_INT_PLAYER_STAT(MPPLY_AVAILABLE_HEIST_PLANV))
				PRINTLN("[BIGFEED] 		Heist Finale: 	", GET_MP_INT_PLAYER_STAT(MPPLY_UNIQUE_HEIST_FINALE_V), " / ", GET_MP_INT_PLAYER_STAT(MPPLY_AVAILABLE_HEIST_FINALE_V))
				
				PRINTLN("[BIGFEED] Player Contact Played stats:")
				PRINTLN("[BIGFEED] 		Lester: 		", GET_MP_INT_PLAYER_STAT(MPPLY_UNIQUE_CONTACT_0), " / ", GET_MP_INT_PLAYER_STAT(MPPLY_TOTAL_CONTACT_0))
				PRINTLN("[BIGFEED] 		Lamar: 			", GET_MP_INT_PLAYER_STAT(MPPLY_UNIQUE_CONTACT_1), " / ", GET_MP_INT_PLAYER_STAT(MPPLY_TOTAL_CONTACT_1))
				PRINTLN("[BIGFEED] 		Gerald: 		", GET_MP_INT_PLAYER_STAT(MPPLY_UNIQUE_CONTACT_2), " / ", GET_MP_INT_PLAYER_STAT(MPPLY_TOTAL_CONTACT_2))
				PRINTLN("[BIGFEED] 		Ron:		 	", GET_MP_INT_PLAYER_STAT(MPPLY_UNIQUE_CONTACT_3), " / ", GET_MP_INT_PLAYER_STAT(MPPLY_TOTAL_CONTACT_3))
				PRINTLN("[BIGFEED] 		Martin: 		", GET_MP_INT_PLAYER_STAT(MPPLY_UNIQUE_CONTACT_4), " / ", GET_MP_INT_PLAYER_STAT(MPPLY_TOTAL_CONTACT_4))
				PRINTLN("[BIGFEED] 		Trevor: 		", GET_MP_INT_PLAYER_STAT(MPPLY_UNIQUE_CONTACT_5), " / ", GET_MP_INT_PLAYER_STAT(MPPLY_TOTAL_CONTACT_5))
				PRINTLN("[BIGFEED] 		Simeon: 		", GET_MP_INT_PLAYER_STAT(MPPLY_UNIQUE_CONTACT_6), " / ", GET_MP_INT_PLAYER_STAT(MPPLY_TOTAL_CONTACT_6))
				
			ENDIF
			
			g_bDisplayBigFeedJobCounts = FALSE
		ENDIF
		
		IF g_iDisplayBigFeedScriptScreens != 0
			
			SWITCH g_iDisplayBigFeedScriptScreens
			
				CASE 0
				BREAK
				
				CASE 1
					IF NOT GET_IS_LOADING_SCREEN_ACTIVE()
						IF NOT HAS_SCALEFORM_MOVIE_LOADED(g_TransitionSessionNonResetVars.sPostMissionCleanupData.scaleSplash)
							g_TransitionSessionNonResetVars.sPostMissionCleanupData.scaleSplash = REQUEST_SCALEFORM_MOVIE("GTAV_ONLINE")
							PRINTLN("[BIGFEED] - [DEBUG WIDGET] - loading scaleform movie GTAV_ONLINE.")
						ELSE
							g_iDisplayBigFeedScriptScreens++
							PRINTLN("[BIGFEED] - [DEBUG WIDGET] - scaleform movie loaded. Setting g_iDisplayBigFeedScriptScreens = ", g_iDisplayBigFeedScriptScreens)
						ENDIF
					ELSE
						PRINTLN("[BIGFEED] - [DEBUG WIDGET] - GET_IS_LOADING_SCREEN_ACTIVE() = TRUE. Holding until returns FALSE.")
					ENDIF
				BREAK
				
				CASE 2
					//DRAW_SCALEFORM_MOVIE_FULLSCREEN(g_TransitionSessionNonResetVars.sPostMissionCleanupData.scaleSplash, 255, 255, 255, 255)
					IF SC_TRANSITION_NEWS_SHOW_TIMED(g_TransitionSessionNonResetVars.sPostMissionCleanupData.scaleSplash, 0)
						g_IsNewsFeedDisplaying = TRUE
						g_iDisplayBigFeedScriptScreens++
						g_TransitionSessionNonResetVars.sDynamicBigFeedData.g_iLastBigFeedScreenShown = -1
						PRINTLN("[BIGFEED] - [DEBUG WIDGET] - SC_TRANSITION_NEWS_SHOW_TIMED(movieIndex, 0) = TRUE. Setting g_iDisplayBigFeedScriptScreens = ", g_iDisplayBigFeedScriptScreens, " and g_IsNewsFeedDisplaying = ", g_IsNewsFeedDisplaying)
					ENDIF
				BREAK
				
				CASE 3
					IF g_TransitionSessionNonResetVars.sDynamicBigFeedData.g_iLastBigFeedScreenShown = ciBIG_FEED_UNPLAYED_JOB
						PRINTLN("[BIGFEED] - [DEBUG WIDGET] - g_TransitionSessionNonResetVars.sDynamicBigFeedData.g_iLastBigFeedScreenShown = ciBIG_FEED_UNPLAYED_JOB.")
						IF SEND_UNPLAYED_JOB_DATA_TO_MOVIE_ONCE_READY(sBigFeedUnplayedJobPhotoData, iUnplayedJobArrayIndex)
							g_iDisplayBigFeedScriptScreens++
							PRINTLN("[BIGFEED] - [DEBUG WIDGET] - SEND_UNPLAYED_JOB_DATA_TO_MOVIE_ONCE_READY = TRUE. Setting g_iDisplayBigFeedScriptScreens = ", g_iDisplayBigFeedScriptScreens)
						ELSE
							PRINTLN("[BIGFEED] - [DEBUG WIDGET] - SEND_UNPLAYED_JOB_DATA_TO_MOVIE_ONCE_READY = FALSE.")
						ENDIF
					ELSE
						g_iDisplayBigFeedScriptScreens++
						PRINTLN("[BIGFEED] - [DEBUG WIDGET] - g_TransitionSessionNonResetVars.sDynamicBigFeedData.g_iLastBigFeedScreenShown != ciBIG_FEED_UNPLAYED_JOB. Setting g_iDisplayBigFeedScriptScreens = ", g_iDisplayBigFeedScriptScreens)
					ENDIF
				BREAK
				
				CASE 4
					DRAW_SCALEFORM_MOVIE_FULLSCREEN(g_TransitionSessionNonResetVars.sPostMissionCleanupData.scaleSplash, 255, 255, 255, 255)
				BREAK
				
				CASE 5
					IF HAS_SCALEFORM_MOVIE_LOADED(g_TransitionSessionNonResetVars.sPostMissionCleanupData.scaleSplash)
						SC_TRANSITION_NEWS_END()
						SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(g_TransitionSessionNonResetVars.sPostMissionCleanupData.scaleSplash)
					ENDIF
					g_IsNewsFeedDisplaying = FALSE
					RESET_STRUCT_DL_PHOTO_VARS_LITE(sBigFeedUnplayedJobPhotoData)
					IF iUnplayedJobArrayIndex != -1
						UGC_RELEASE_CACHED_DESCRIPTION(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iUnplayedJobArrayIndex].iMissionDecHash)
					ENDIF
					iUnplayedJobArrayIndex = -1
					g_TransitionSessionNonResetVars.sDynamicBigFeedData.g_iBigFeedGrabUnplayedJobBitset = 0
					g_TransitionSessionNonResetVars.sDynamicBigFeedData.iRandomJobType = -1
					STRUCT_DL_PHOTO_VARS_LITE sBigFeedUnplayedJobPhotoDataTemp
					CACHED_MISSION_DESCRIPTION_LOAD_VARS sBigFeedMissionDescVarsTemp
					sBigFeedUnplayedJobPhotoData = sBigFeedUnplayedJobPhotoDataTemp
					sBigFeedMissionDescVars = sBigFeedMissionDescVarsTemp
					g_TransitionSessionNonResetVars.sDynamicBigFeedData.g_iLastBigFeedScreenShown = -1
					PRINTLN("[BIGFEED] - [DEBUG WIDGET] - reset data.")
					g_iDisplayBigFeedScriptScreens = 0
					PRINTLN("[BIGFEED] - [DEBUG WIDGET] - Setting g_iDisplayBigFeedScriptScreens = ", g_iDisplayBigFeedScriptScreens)
				BREAK
				
			ENDSWITCH
			
		ENDIF
		
	#ENDIF	
ENDPROC

