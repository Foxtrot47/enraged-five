/**********************************************************
/ Name: 		net_hud_colour_controller.sch
/ Author(s): 	James Adwick
/ Purpose: 		Generic header containing script functions to
/				control and modify in game HUD COLOURS
***********************************************************/

USING "globals.sch"

FUNC HUD_COLOURS CV2_GET_HUD_COLOUR_PROFESSIONAL(INT iPlayList = -1)
	IF iPlayList != -1
		SWITCH g_sMPTunables.iProfesionalCoronaType[iPlayList]
			CASE FMMC_TYPE_RACE 						RETURN HUD_COLOUR_PINKLIGHT
			CASE FMMC_TYPE_MISSION_NEW_VS				RETURN HUD_COLOUR_PINKLIGHT
			CASE FMMC_TYPE_ADVERSARY_SERIES				RETURN HUD_COLOUR_PINKLIGHT
			CASE FMMC_TYPE_ADVERSARY_SERIES_1			RETURN HUD_COLOUR_PINKLIGHT
			CASE FMMC_TYPE_ADVERSARY_SERIES_2			RETURN HUD_COLOUR_PINKLIGHT
			CASE FMMC_TYPE_SPECIAL_VEHICLE_RACE_SERIES	RETURN HUD_COLOUR_PINKLIGHT
			CASE FMMC_TYPE_BUNKER_SERIES 				RETURN HUD_COLOUR_PINKLIGHT
			CASE FMMC_TYPE_TRANSFORM_SERIES 			RETURN HUD_COLOUR_PINKLIGHT
			CASE FMMC_TYPE_TARGET_ASSAULT_SERIES		RETURN HUD_COLOUR_PINKLIGHT
			CASE FMMC_TYPE_HOTRING_SERIES				RETURN HUD_COLOUR_PINKLIGHT
			CASE FMMC_TYPE_ARENA_SERIES					RETURN HUD_COLOUR_PINKLIGHT
			CASE FMMC_TYPE_RACE_SERIES					RETURN HUD_COLOUR_PINKLIGHT
			CASE FMMC_TYPE_SURVIVAL_SERIES				RETURN HUD_COLOUR_PINKLIGHT
			CASE FMMC_TYPE_OPEN_WHEEL_SERIES			RETURN HUD_COLOUR_PINKLIGHT
			CASE FMMC_TYPE_STREET_RACE_SERIES			RETURN HUD_COLOUR_PINKLIGHT
			CASE FMMC_TYPE_PURSUIT_SERIES				RETURN HUD_COLOUR_PINKLIGHT
			#IF FEATURE_GEN9_EXCLUSIVE
			CASE FMMC_TYPE_HSW_RACE_SERIES				RETURN HUD_COLOUR_PINKLIGHT
			#ENDIF
			CASE FMMC_TYPE_COMMUNITY_SERIES				RETURN HUD_COLOUR_PINKLIGHT
			CASE FMMC_TYPE_CAYO_PERICO_SERIES			RETURN HUD_COLOUR_PINKLIGHT
		ENDSWITCH
	ENDIF
	RETURN HUD_COLOUR_PINKLIGHT
ENDFUNC
 // #IF FEATURE_NEW_CORONAS

/// PURPOSE: Checks if the HUD_COLOURS passed has been overridden
FUNC BOOL IS_MP_HUD_COLOUR_OVERRIDDEN(HUD_COLOURS hcTarget, INT &iSlot)
	
	INT i
	REPEAT TOTAL_MP_HUD_COLOURS_SLOTS i
		
		IF g_MPHudColourData[i].hcColour = hcTarget
			iSlot = i
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

/// PURPOSE: Returns TRUE if our index is out of range
FUNC BOOL IS_MP_HUD_COLOUR_INDEX_OUT_OF_RANGE(INT iSlot)
	
	IF iSlot < 0
	OR iSlot >= TOTAL_MP_HUD_COLOURS_SLOTS
		RETURN TRUE
	ENDIF	
	
	RETURN FALSE
ENDFUNC

/// PURPOSE: Replaces the HUD_COLOURS hcTarget with hcOverride
/// PARAMS:
///    hcTarget - HUD_COLOURS you wish to override
///    hcOverride - HUD_COLOURS you want to override with
/// RETURNS: Returns an INT which script can save out to know which specific override to reset if they are not clearing all
FUNC INT OVERRIDE_HUD_COLOUR(HUD_COLOURS hcTarget, HUD_COLOURS hcOverride)

	// Check our slot is within our bounds
	INT iSlot
	IF IS_MP_HUD_COLOUR_OVERRIDDEN(hcTarget, iSlot)
	
		PRINTLN("[HUD_COLOUR] OVERRIDE_HUD_COLOUR - Attempting to override HUD colour that has already been overridden: iSlot: ", iSlot)
		#IF IS_DEBUG_BUILD
		IF g_MPHudColourData[iSlot].hcOverride != hcOverride
			SCRIPT_ASSERT("[HUD_COLOUR] OVERRIDE_HUD_COLOUR - Attempting to override HUD colour that has already been overridden. See logs for details.")
		ENDIF
		#ENDIF
		RETURN -1
	ENDIF

	// Default our slot to fail
	iSlot = -1
	
	// Find a valid slot to override the HUD_COLOURS to.
	INT iIndex
	REPEAT TOTAL_MP_HUD_COLOURS_SLOTS iIndex
		
		// If a slot does not have a track ID then we can save
		IF g_MPHudColourData[iIndex].iR = MP_HUD_COLOUR_INVALID
		OR g_MPHudColourData[iIndex].iG = MP_HUD_COLOUR_INVALID
		OR g_MPHudColourData[iIndex].iB = MP_HUD_COLOUR_INVALID
		OR g_MPHudColourData[iIndex].iA = MP_HUD_COLOUR_INVALID
			
			iSlot = iIndex
			iIndex = TOTAL_MP_HUD_COLOURS_SLOTS
		ENDIF		
	ENDREPEAT

	// Check if our slot is still invalid
	IF iSlot = -1
		PRINTLN("[HUD_COLOUR] OVERRIDE_HUD_COLOUR - We have no valid slot to override the HUD_COLOUR. All slots initialised.")
		RETURN -1
	ENDIF

	// Save out our HUD_COLOUR
	INT iR, iG, iB, iA

	// Get the RGB values of the HUD_COLOUR we will be replacing
	GET_HUD_COLOUR(hcTarget, iR, iG, iB, iA)

	PRINTLN("[HUD_COLOUR] OVERRIDE_HUD_COLOUR - Overriding hud colour (", iR, ", ", iG, ", ", iB, "). Using iSlot: ", iSlot)
	DEBUG_PRINTCALLSTACK()

	// Save out values into your global array
	g_MPHudColourData[iSlot].hcColour 	= hcTarget
	#IF IS_DEBUG_BUILD
	g_MPHudColourData[iSlot].hcOverride 	= hcOverride
	#ENDIF
	g_MPHudColourData[iSlot].iR 		= iR
	g_MPHudColourData[iSlot].iG 		= iG
	g_MPHudColourData[iSlot].iB 		= iB
	g_MPHudColourData[iSlot].iA 		= iA

	// Make the override
	REPLACE_HUD_COLOUR(hcTarget, hcOverride)	
	
	RETURN iSlot
ENDFUNC




