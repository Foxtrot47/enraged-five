

// ====================================================================================
// ====================================================================================
//
// Name:        fake_interiors.sc
// Description: Background script, adds high detail information to the local player's minimap when they are in proximity to certain areas
//				(warehouses, prison, Kortz centre etc)
// Written By:  David Gentles
//
// ====================================================================================
// ====================================================================================


USING "rage_builtins.sch"
USING "globals.sch"
USING "net_blips.sch"

INT iStaggerFakeInteriorTest = 0
INT iNearestFakeInterior = -1

CONST_INT DISTANCE_TO_ANGLED_AREA_CHECK_SQR	10000//100sqr

CONST_INT NUMBER_OF_FAKE_INTERIORS			5

VECTOR VECTOR_ZERO = <<0.0,0.0,0.0>>

PED_INDEX pedTest

INT iBSFakeInterior
CONST_INT BS_FAKE_INTERIOR_DISPLAY_FORT_ZANCUDO_MAP		0
CONST_INT BS_FAKE_INTERIOR_CLEAR_FORT_ZANCUDO_MAP		1

FUNC HANGAR_ID GET_PLAYERS_OWNED_HANGAR(PLAYER_INDEX piPlayer)
	IF piPlayer != INVALID_PLAYER_INDEX()
		RETURN GlobalplayerBD_FM[NATIVE_TO_INT(piPlayer)].propertyDetails.bdHangarData.eHangar
	ENDIF
	
	RETURN HANGAR_ID_INVALID
ENDFUNC

FUNC BOOL DOES_PLAYER_OWN_AN_AB_HANGAR(PLAYER_INDEX playerID)
	IF DOES_PLAYER_OWN_A_HANGER(playerID)
		HANGAR_ID eHangarID = GET_PLAYERS_OWNED_HANGAR(playerID)
		
		IF eHangarID = ZANCUDO_HANGAR_A2
		OR eHangarID = ZANCUDO_HANGAR_3497
		OR eHangarID = ZANCUDO_HANGAR_3499
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_LOCAL_PLAYER_DISPLAY_FORT_ZANCUDO_ON_PAUSE_MAP()
	IF DOES_PLAYER_OWN_AN_AB_HANGAR(PLAYER_ID())
		RETURN TRUE
	ENDIF
	
	IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID())
		PLAYER_INDEX pGangBossID = GB_GET_LOCAL_PLAYER_GANG_BOSS()
		IF pGangBossID != INVALID_PLAYER_INDEX()
			IF DOES_PLAYER_OWN_AN_AB_HANGAR(pGangBossID)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Gets the central coordinate of the fake interior
/// PARAMS:
///    iFakeInteriorID - ID of the fake interior
/// RETURNS:
///    vector of the central coordinate
FUNC VECTOR GET_FAKE_INTERIOR_CENTRE(INT iFakeInteriorID)
	SWITCH iFakeInteriorID
	
		CASE 0		RETURN <<1240.0, -2970.0, 12.2>>				BREAK	//Cargo Ship
		CASE 1		RETURN <<40.0, -2720.0, 12.0>>					BREAK	//Docks Hideout
		CASE 2		RETURN <<-2250.0, 300.0, 182.2>>				BREAK	//Kortz Centre
		CASE 3		RETURN <<1700.0, 2580.0, 80.0>>					BREAK	//Prison
		CASE 4		RETURN <<-2250.0, 3100.0, 80.0>>				BREAK	//Military Base

	ENDSWITCH
	RETURN VECTOR_ZERO
ENDFUNC

/// PURPOSE:
///    Gets the number of angle checks required to check a fake interior area
/// PARAMS:
///    iFakeInteriorID - ID of the fake interior
/// RETURNS:
///    number of angle checks
FUNC INT GET_FAKE_INTERIOR_NUMBER_OF_ANGLE_CHECKS(INT iFakeInteriorID)
	SWITCH iFakeInteriorID
		CASE 0		RETURN 1										BREAK	//Cargo Ship
		CASE 1		RETURN 4										BREAK	//Docks Hideout
		CASE 2		RETURN 11										BREAK	//Kortz Centre
		CASE 3		RETURN 1										BREAK	//Prison
		CASE 4		RETURN 1										BREAK	//Military Base
	ENDSWITCH
	RETURN 0
ENDFUNC

/// PURPOSE:
///    Gets the minimum coordinate of a specified angle check for a specified fake interior's area
/// PARAMS:
///    iFakeInteriorID - ID of the fake interior
///    iAngleCheckID - ID of the angle check
/// RETURNS:
///    vector of the minimum coordinate for the angle check of the fake interior
FUNC VECTOR GET_FAKE_INTERIOR_ANGLE_CHECK_MIN(INT iFakeInteriorID, INT iAngleCheckID)
	SWITCH iFakeInteriorID
	
		CASE 0																//Cargo Ship
			SWITCH iAngleCheckID
				CASE 0	RETURN <<1240.534546,-2880.354004,-19.964888>>	BREAK
			ENDSWITCH
		BREAK
		CASE 1																//Docks Hideout
			SWITCH iAngleCheckID
				CASE 0	RETURN <<34.153080,-2747.067383,1.137565>>		BREAK
				CASE 1	RETURN <<13.957768,-2700.625977,5.046232>>		BREAK
				CASE 2	RETURN <<55.611851,-2687.681396,5.005801>>		BREAK
				CASE 3	RETURN <<34.569260,-2759.479004,-0.030933>>		BREAK
			ENDSWITCH
		BREAK
		CASE 2																//Kortz Centre
			SWITCH iAngleCheckID
				CASE 0	RETURN <<-2317.380127,191.631882,165.403732>>	BREAK
				CASE 1	RETURN <<-2357.994873,264.029724,162.798843>>	BREAK
				CASE 2	RETURN <<-2261.432861,387.396301,154.352219>>	BREAK
				CASE 3	RETURN <<-2326.398926,408.337799,140.318222>>	BREAK
				CASE 4	RETURN <<-2304.616699,460.212677,140.214737>>	BREAK
				CASE 5	RETURN <<-2150.824951,216.416809,162.801178>>	BREAK
				CASE 6	RETURN <<-2172.765137,203.595703,167.413513>>	BREAK
				CASE 7	RETURN <<-2191.036377,305.960968,159.625015>>	BREAK
				CASE 8	RETURN <<-2227.613037,340.058716,165.135742>>	BREAK
				CASE 9	RETURN <<-2244.409668,399.576355,137.510101>>	BREAK
				CASE 10	RETURN <<-2243.261475,371.407196,137.272202>>	BREAK
			ENDSWITCH
		BREAK
		CASE 3																//Prison
			SWITCH iAngleCheckID
				CASE 0	RETURN <<3200.0,2600.0,3000.0>>					BREAK
			ENDSWITCH
		BREAK
		CASE 4																//Military Base
			SWITCH iAngleCheckID
				CASE 0	RETURN <<-2841.107422,3506.836914,1000.473663>>	BREAK
			ENDSWITCH
		BREAK
		
	ENDSWITCH
	RETURN VECTOR_ZERO
ENDFUNC

/// PURPOSE:
///    Gets the maximum coordinate of a specified angle check for a specified fake interior's area
/// PARAMS:
///    iFakeInteriorID - ID of the fake interior
///    iAngleCheckID - ID of the angle check
/// RETURNS:
///    vector of the maximum coordinate for the angle check of the fake interior
FUNC VECTOR GET_FAKE_INTERIOR_ANGLE_CHECK_MAX(INT iFakeInteriorID, INT iAngleCheckID)
	SWITCH iFakeInteriorID
	
		CASE 0																//Cargo Ship
			SWITCH iAngleCheckID
				CASE 0	RETURN <<1240.537109,-3057.288818,40.751640>>	BREAK
			ENDSWITCH
		BREAK
		CASE 1																//Docks Hideout
			SWITCH iAngleCheckID
				CASE 0	RETURN <<34.278374,-2654.243652,20.942299>>		BREAK
				CASE 1	RETURN <<13.931628,-2654.560547,14.442390>>		BREAK
				CASE 2	RETURN <<55.595722,-2667.498535,10.822453>>		BREAK
				CASE 3	RETURN <<34.586597,-2746.387451,10.950064>>		BREAK
			ENDSWITCH
		BREAK
		CASE 2																//Kortz Centre
			SWITCH iAngleCheckID
				CASE 0	RETURN <<-2169.170410,256.726410,203.408127>> 	BREAK
				CASE 1	RETURN <<-2216.393799,329.476105,201.361679>> 	BREAK
				CASE 2	RETURN <<-2345.352783,350.788177,189.652222>> 	BREAK
				CASE 3	RETURN <<-2288.097168,388.990936,200.904495>> 	BREAK
				CASE 4	RETURN <<-2310.263184,406.638000,200.904083>>	BREAK
				CASE 5	RETURN <<-2169.221436,260.567902,202.429443>> 	BREAK
				CASE 6	RETURN <<-2258.777588,166.950607,202.831772>>	BREAK
				CASE 7	RETURN <<-2236.973145,285.595764,203.039474>>	BREAK
				CASE 8	RETURN <<-2211.361816,303.674133,214.932281>> 	BREAK
				CASE 9	RETURN <<-2282.098145,383.090363,201.039505>> 	BREAK
				CASE 10	RETURN <<-2277.930176,356.444244,201.101547>> 	BREAK
			ENDSWITCH
		BREAK
		CASE 3																//Prison
			SWITCH iAngleCheckID
				CASE 0	RETURN <<200.0,2600.0,-5.0>>					BREAK
			ENDSWITCH
		BREAK
		CASE 4																//Military Base
			SWITCH iAngleCheckID
				CASE 0	RETURN <<-1451.205078,2689.440186,-37.626541>>	BREAK
			ENDSWITCH
		BREAK
		
	ENDSWITCH
	RETURN VECTOR_ZERO
ENDFUNC

/// PURPOSE:
///    Gets the width of a specified angle check for a specified fake interior's area
/// PARAMS:
///    iFakeInteriorID - ID of the fake interior
///    iAngleCheckID - ID of the angle check
/// RETURNS:
///    width of the angle check of the fake interior
FUNC FLOAT GET_FAKE_INTERIOR_ANGLE_CHECK_WIDTH(INT iFakeInteriorID, INT iAngleCheckID)
	SWITCH iFakeInteriorID
	
		CASE 0																//Cargo Ship
			SWITCH iAngleCheckID
				CASE 0	RETURN 28.125	BREAK
			ENDSWITCH
		BREAK
		CASE 1																//Docks Hideout
			SWITCH iAngleCheckID
				CASE 0	RETURN 32.6875	BREAK
				CASE 1	RETURN 13.1875	BREAK
				CASE 2	RETURN 16.25	BREAK
				CASE 3	RETURN 21.75	BREAK
			ENDSWITCH
		BREAK
		CASE 2																//Kortz Centre
			SWITCH iAngleCheckID
				CASE 0	RETURN 95.0		BREAK
				CASE 1	RETURN 78.75	BREAK
				CASE 2	RETURN 70.6875	BREAK
				CASE 3	RETURN 64.4375	BREAK
				CASE 4	RETURN 32.375	BREAK
				CASE 5	RETURN 19.0		BREAK
				CASE 6	RETURN 19.0		BREAK
				CASE 7	RETURN 19.78125	BREAK
				CASE 8	RETURN 32.0625	BREAK
				CASE 9	RETURN 35.8125	BREAK
				CASE 10	RETURN 30.5		BREAK
			ENDSWITCH
		BREAK
		CASE 3																//Prison
			SWITCH iAngleCheckID
				CASE 0	RETURN 3000.0	BREAK
			ENDSWITCH
		BREAK
		CASE 4																//Military Base
			SWITCH iAngleCheckID
				CASE 0	RETURN 1500.0	BREAK
			ENDSWITCH
		BREAK

	ENDSWITCH
	RETURN 0.0
ENDFUNC

/// PURPOSE:
///    Gets the string ID for the minimap information required for the fake interior
/// PARAMS:
///    iFakeInteriorID - ID of the fake interior
/// RETURNS:
///    string ID of the minimap information of the fake interior
FUNC STRING GET_FAKE_INTERIOR_STRING(INT iFakeInteriorID)
	SWITCH iFakeInteriorID
	
		CASE 0		RETURN "V_FakeBoatPO1SH2A"						BREAK	//Cargo Ship
		CASE 1		RETURN "V_FakeWarehousePO103"					BREAK	//Docks Hideout
		CASE 2		RETURN "V_FakeKortzCenter"						BREAK	//Kortz Centre
		CASE 3		RETURN "V_FakePrison"							BREAK	//Prison
		CASE 4		RETURN "V_FakeMilitaryBase"						BREAK	//Military Base
	ENDSWITCH
	RETURN ""
ENDFUNC

/// PURPOSE:
///    Gets the rotation of the minimap information required for the fake interior
/// PARAMS:
///    iFakeInteriorID - IF of the fake interior
/// RETURNS:
///    angle of rotation of the minimap information of the fake interior
FUNC INT GET_FAKE_INTERIOR_ROTATION(INT iFakeInteriorID)
	SWITCH iFakeInteriorID
	
		CASE 0		RETURN 0										BREAK	//Cargo Ship
		CASE 1		RETURN 0										BREAK	//Docks Hideout
		CASE 2		RETURN 0										BREAK	//Kortz Centre
		CASE 3		RETURN 0										BREAK	//Prison
		CASE 4		RETURN 0										BREAK	//Military Base
		
	ENDSWITCH
	RETURN 0
ENDFUNC

/// PURPOSE:
///    Gets the player's current level (floor of a building) of the specified fake interior ID
/// PARAMS:
///    iFakeInteriorID - ID of the fake interior
/// RETURNS:
///    level of the fake interior of the local player ped
FUNC INT GET_FAKE_INTERIOR_LEVEL(INT iFakeInteriorID)
	VECTOR vPlayerPos
	SWITCH iFakeInteriorID
	
		CASE 0		RETURN 0										BREAK	//Cargo Ship
		CASE 1																//Docks Hideout
			vPlayerPos = GET_ENTITY_COORDS(pedTest, FALSE)
			IF vPlayerPos.Z < 9.7796
				RETURN 0
			ELIF (vPlayerPos.Z > 9.7796 AND vPlayerPos.Z < 16.0)//24.317642)
				RETURN 1
			ELSE
				RETURN 2
			ENDIF
		BREAK
		CASE 2																//Kortz Centre
			vPlayerPos = GET_ENTITY_COORDS(pedTest, FALSE)
			IF vPlayerPos.Z < 178.9
				RETURN 0
			ELIF (vPlayerPos.Z > 178.9 AND vPlayerPos.Z < 188.7)
				RETURN 1
			ELSE
				RETURN 2
			ENDIF
		BREAK
		CASE 3		RETURN 0										BREAK	//Prison
		CASE 4		RETURN 0										BREAK	//Military Base
		
	ENDSWITCH
	RETURN 0
ENDFUNC

/// PURPOSE:
///    Checks if the fake inteior specified should be activated by the local player
/// PARAMS:
///    iFakeInteriorID - ID of the fake interior
/// RETURNS:
///    true/false
FUNC BOOL SHOULD_FAKE_INTERIOR_BE_USED(INT iFakeInteriorID)
	SWITCH iFakeInteriorID
	
		CASE 0														BREAK	//Cargo Ship
		CASE 1														BREAK	//Docks Hideout
		CASE 2														BREAK	//Kortz Centre
		CASE 3														BREAK	//Prison
		CASE 4														BREAK	//Military Base
		
	ENDSWITCH
	
	RETURN TRUE
	
ENDFUNC

/// PURPOSE:
///    Gets the squared value of the distance used in a proximity check for the specified fake interior
/// PARAMS:
///    iFakeInteriorID - ID of the fake interior
/// RETURNS:
///    squared distance of the proximity check of the fake interior
FUNC INT GET_FAKE_INTERIOR_DISTANCE_TO_ANGLE_CHECK_SQUARED(INT iFakeInteriorID)
	SWITCH iFakeInteriorID
	
		CASE 0		RETURN 10000/*100sqr*/							BREAK	//Cargo Ship
		CASE 1		RETURN 10000/*100sqr*/							BREAK	//Docks Hideout
		CASE 2		RETURN 250000/*500sqr*/							BREAK	//Kortz Centre
		CASE 3		RETURN 9000000/*3000sqr*/						BREAK	//Prison
		CASE 4		RETURN 2250000/*1500sqr*/						BREAK	//Military Base
		
	ENDSWITCH
	RETURN 0
ENDFUNC

/// PURPOSE:
///    Performs any extra function calls required for a fake interior once the player has activated it
/// PARAMS:
///    iFakeInteriorID - ID of the fake interior
PROC DO_EXTRA_PROCESSING_FOR_FAKE_INTERIOR(INT iFakeInteriorID)
	VECTOR vPlayerPos
	vPlayerPos = GET_ENTITY_COORDS(pedTest, FALSE)
	SET_FAKE_PAUSEMAP_PLAYER_POSITION_THIS_FRAME(vPlayerPos.x, vPlayerPos.y)
			
	SWITCH iFakeInteriorID
	
		CASE 0														BREAK	//Cargo Ship
		CASE 1														BREAK	//Docks Hideout
		CASE 2																//Kortz Centre
			vPlayerPos = GET_ENTITY_COORDS(pedTest, FALSE)
			SET_FAKE_PAUSEMAP_PLAYER_POSITION_THIS_FRAME(vPlayerPos.x, vPlayerPos.y)
		BREAK
		CASE 3																//Prison
			SET_RADAR_AS_EXTERIOR_THIS_FRAME()
		BREAK
		CASE 4																//Military Base
			SET_RADAR_AS_EXTERIOR_THIS_FRAME()
		BREAK
		
	ENDSWITCH
	
ENDPROC

/// PURPOSE:
///    Staggers through the list of fake interiors identifying the nearest fake interior
PROC PROCESS_STAGGER_FAKE_INTERIOR()

	iStaggerFakeInteriorTest++
	
	//loop the stagger back to beginning
	IF iStaggerFakeInteriorTest > NUMBER_OF_FAKE_INTERIORS-1
		iStaggerFakeInteriorTest = 0
	ENDIF
	
	//test if stagger is nearer than current
	IF iStaggerFakeInteriorTest <> iNearestFakeInterior
		IF iNearestFakeInterior = -1
			iNearestFakeInterior = iStaggerFakeInteriorTest
		ELSE
			VECTOR vPlayerCoords = GET_ENTITY_COORDS(pedTest, FALSE)
			IF VDIST2(GET_FAKE_INTERIOR_CENTRE(iStaggerFakeInteriorTest), vPlayerCoords) < VDIST2(GET_FAKE_INTERIOR_CENTRE(iNearestFakeInterior), vPlayerCoords)
				iNearestFakeInterior = iStaggerFakeInteriorTest
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_NET_AMBIENT, "=== FAKE INTERIOR === New nearest fake interior = ", GET_FAKE_INTERIOR_STRING(iNearestFakeInterior))
				#ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Checks the nearest fake interior to see if the local player is within proximity, then if the player is within the angled area to activate the fake interior on the minimap
PROC PROCESS_FAKE_INTERIORS()
	PROCESS_STAGGER_FAKE_INTERIOR()
	
	IF iNearestFakeInterior <> -1
	AND SHOULD_FAKE_INTERIOR_BE_USED(iNearestFakeInterior)
	
		VECTOR vPlayerCoords = GET_ENTITY_COORDS(pedTest, FALSE)
		VECTOR vFakeInteriorCentre = GET_FAKE_INTERIOR_CENTRE(iNearestFakeInterior)
		
		//Are we close enough for angle check?
		IF VDIST2(vFakeInteriorCentre, vPlayerCoords) < GET_FAKE_INTERIOR_DISTANCE_TO_ANGLE_CHECK_SQUARED(iNearestFakeInterior)
			
			BOOL bInAnyAngledArea = FALSE
			INT iAreaCount = 0
			
			//Angle check
			REPEAT GET_FAKE_INTERIOR_NUMBER_OF_ANGLE_CHECKS(iNearestFakeInterior) iAreaCount
				IF NOT bInAnyAngledArea
					IF IS_ENTITY_IN_ANGLED_AREA(pedTest, 	GET_FAKE_INTERIOR_ANGLE_CHECK_MIN(iNearestFakeInterior, iAreaCount), 
																	GET_FAKE_INTERIOR_ANGLE_CHECK_MAX(iNearestFakeInterior, iAreaCount), 
																	GET_FAKE_INTERIOR_ANGLE_CHECK_WIDTH(iNearestFakeInterior, iAreaCount))
						bInAnyAngledArea = TRUE
					ENDIF
				ENDIF
			ENDREPEAT
			
			IF bInAnyAngledArea
			
				//Set fake interior
				SET_RADAR_AS_INTERIOR_THIS_FRAME(GET_HASH_KEY(	GET_FAKE_INTERIOR_STRING(iNearestFakeInterior)),
																vFakeInteriorCentre.x, vFakeInteriorCentre.y,
																GET_FAKE_INTERIOR_ROTATION(iNearestFakeInterior),
																GET_FAKE_INTERIOR_LEVEL(iNearestFakeInterior))
				
				DO_EXTRA_PROCESSING_FOR_FAKE_INTERIOR(iNearestFakeInterior)
				
			ENDIF
			
		ENDIF
		
	ENDIF
	
	// Show Fort Zancudo on map if player owns an army base hangar
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF SHOULD_LOCAL_PLAYER_DISPLAY_FORT_ZANCUDO_ON_PAUSE_MAP()
			
			IF IS_BIT_SET(iBSFakeInterior, BS_FAKE_INTERIOR_CLEAR_FORT_ZANCUDO_MAP)
				CLEAR_BIT(iBSFakeInterior, BS_FAKE_INTERIOR_CLEAR_FORT_ZANCUDO_MAP)
			ENDIF
			
			IF IS_PAUSE_MENU_ACTIVE()
				IF NOT IS_BIT_SET(iBSFakeInterior, BS_FAKE_INTERIOR_DISPLAY_FORT_ZANCUDO_MAP)
					SET_MINIMAP_COMPONENT(MINIMAP_COMPONENT_KING_HILL_10, TRUE)
					SET_BIT(iBSFakeInterior, BS_FAKE_INTERIOR_DISPLAY_FORT_ZANCUDO_MAP)
				ENDIF
			ELSE
				IF IS_BIT_SET(iBSFakeInterior, BS_FAKE_INTERIOR_DISPLAY_FORT_ZANCUDO_MAP)
					SET_MINIMAP_COMPONENT(MINIMAP_COMPONENT_KING_HILL_10, FALSE)
					CLEAR_BIT(iBSFakeInterior, BS_FAKE_INTERIOR_DISPLAY_FORT_ZANCUDO_MAP)
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_BIT_SET(iBSFakeInterior, BS_FAKE_INTERIOR_CLEAR_FORT_ZANCUDO_MAP)
				SET_MINIMAP_COMPONENT(MINIMAP_COMPONENT_KING_HILL_10, FALSE)
				CLEAR_BIT(iBSFakeInterior, BS_FAKE_INTERIOR_DISPLAY_FORT_ZANCUDO_MAP)
				SET_BIT(iBSFakeInterior, BS_FAKE_INTERIOR_CLEAR_FORT_ZANCUDO_MAP)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

SCRIPT
	
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_NET_AMBIENT, "=== FAKE INTERIOR === Launching")
	#ENDIF
	
	if HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_SP_TO_MP)
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_AMBIENT, "=== FAKE INTERIOR === fake_interiors.sc TERMINATED: FORCE_CLEANUP_FLAG_SP_TO_MP ")
		#ENDIF
		TERMINATE_THIS_THREAD()
	endif
	
	// Main loop.
	WHILE TRUE
	
		WAIT(0) 
	
		// Currently only used in GTAO so kill the script if the network game isn't in progress
		IF NOT (NETWORK_IS_GAME_IN_PROGRESS())		
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_NET_AMBIENT, "=== FAKE INTERIOR === fake_interiors.sc TERMINATED")
			#ENDIF
			TERMINATE_THIS_THREAD()
		ENDIF

		IF IS_PLAYER_SPECTATING(PLAYER_ID())
		AND DOES_ENTITY_EXIST(GET_SPECTATOR_CURRENT_FOCUS_PED())
			pedTest = GET_SPECTATOR_CURRENT_FOCUS_PED()
		ELSE
			pedTest = PLAYER_PED_ID()
		ENDIF
		
		IF IS_ENTITY_DEAD(pedTest) //player dead? we don't care!
		ENDIF
		
		PROCESS_FAKE_INTERIORS()

	ENDWHILE

ENDSCRIPT
