USING "globals.sch"
USING "net_spawn.sch"
USING "net_prints.sch"
USING "net_realty_new.sch"
USING "streamed_scripts.sch"
USING "net_gang_boss.sch"
USING "net_simple_interior.sch"
USING "heist_island_travel.sch"
USING "net_island_heist_cutscenes.sch"

CONST_INT SA_LOADING		0
CONST_INT SA_UPDATE			1
CONST_INT SA_TERMINATE		2

CAMERA_INDEX mCam, mCam2

INT iSpawnActivitesStage
BOOL bEnabledCollision
BOOL bBrainsEnabled
BOOL bGivenTask

TIME_DATATYPE SpawnTimer

#IF FEATURE_HEIST_ISLAND
INT iSyncSceneID = -1
INT iSyncSceneBS = 0
#ENDIF


// **************************************************************************************************************************************************************
// **************************************************************************************************************************************************************
// **************************************************************************************************************************************************************

CONST_INT ciNIGHTCLUB_SPAWN_DANCE_MEDIUM_INTERACTION_COUNT 6
CONST_INT ciNIGHTCLUB_SPAWN_DANCE_HIGH_INTERACTION_COUNT 7

#IF FEATURE_HEIST_ISLAND_DANCES
CONST_INT ciHEIST_ISLAND_SPAWN_DANCE_MEDIUM_INTERACTION_COUNT 12
CONST_INT ciHEIST_ISLAND_SPAWN_DANCE_HIGH_INTERACTION_COUNT 13
#ENDIF

/// PURPOSE:
///    Gets the count of medium intensity dance interactions that can be used when spawning
/// RETURNS:
///    The count of medium intensity dance interactions for spawning, varys based on active dlcs
FUNC INT GET_SPAWN_DANCE_MEDIUM_INTERACTION_COUNT()
	INT iDanceInteractionCount = ciNIGHTCLUB_SPAWN_DANCE_MEDIUM_INTERACTION_COUNT
	
	#IF FEATURE_HEIST_ISLAND_DANCES
		// Set to dance count for heist island dlc
		iDanceInteractionCount = ciHEIST_ISLAND_SPAWN_DANCE_MEDIUM_INTERACTION_COUNT
	#ENDIF
	
	RETURN iDanceInteractionCount
ENDFUNC

/// PURPOSE:
///    Gets the count of high intensity dance interactions that can be used when spawning
/// RETURNS:
///    The count of high intensity dance interactions for spawning, varys based on active dlcs
FUNC INT GET_SPAWN_DANCE_HIGH_INTERACTION_COUNT()
	INT iDanceInteractionCount = ciNIGHTCLUB_SPAWN_DANCE_HIGH_INTERACTION_COUNT
	
	#IF FEATURE_HEIST_ISLAND_DANCES
		// Set to dance count for heist island dlc
		iDanceInteractionCount = ciHEIST_ISLAND_SPAWN_DANCE_HIGH_INTERACTION_COUNT
	#ENDIF
	
	RETURN iDanceInteractionCount
ENDFUNC

PROC SetAppropriateDancingInteraction()
	INT iRand
	SWITCH g_TransitionSpawnData.iSpawnActivityLocationOverride
		
		// on balcony only play the salsa
		CASE 0
		CASE 2
			g_TransitionSpawnData.iSpawnInteractionAnim = ENUM_TO_INT(PLAYER_INTERACTION_SALSA_ROLL)
		BREAK
		
		// on dance floor base off intensity
		CASE 4
			SWITCH g_eNightClubAudioIntensityLevel
				CASE AUDIO_TAG_NULL
				CASE AUDIO_TAG_LOW
					g_TransitionSpawnData.iSpawnInteractionAnim = ENUM_TO_INT(PLAYER_INTERACTION_SALSA_ROLL)
				BREAK
				CASE AUDIO_TAG_MEDIUM
					iRand = GET_RANDOM_INT_IN_RANGE(0, GET_SPAWN_DANCE_MEDIUM_INTERACTION_COUNT())
					SWITCH iRand
						CASE 0 	g_TransitionSpawnData.iSpawnInteractionAnim = ENUM_TO_INT(PLAYER_INTERACTION_BANGING_TUNES) BREAK
						CASE 1	g_TransitionSpawnData.iSpawnInteractionAnim = ENUM_TO_INT(PLAYER_INTERACTION_OH_SNAP) BREAK
						CASE 2	g_TransitionSpawnData.iSpawnInteractionAnim = ENUM_TO_INT(PLAYER_INTERACTION_CATS_CRADLE) BREAK
						CASE 3	g_TransitionSpawnData.iSpawnInteractionAnim = ENUM_TO_INT(PLAYER_INTERACTION_FIND_THE_FISH) BREAK
						CASE 4	g_TransitionSpawnData.iSpawnInteractionAnim = ENUM_TO_INT(PLAYER_INTERACTION_HEART_PUMPING) BREAK
						CASE 5	g_TransitionSpawnData.iSpawnInteractionAnim = ENUM_TO_INT(PLAYER_INTERACTION_UNCLE_DISCO) BREAK
						
						#IF FEATURE_HEIST_ISLAND_DANCES
						CASE 6 g_TransitionSpawnData.iSpawnInteractionAnim = ENUM_TO_INT(PLAYER_INTERACTION_CROWD_INVITATION) BREAK
						CASE 7 g_TransitionSpawnData.iSpawnInteractionAnim = ENUM_TO_INT(PLAYER_INTERACTION_DRIVER) BREAK
						CASE 8 g_TransitionSpawnData.iSpawnInteractionAnim = ENUM_TO_INT(PLAYER_INTERACTION_RUNNER) BREAK
						CASE 9 g_TransitionSpawnData.iSpawnInteractionAnim = ENUM_TO_INT(PLAYER_INTERACTION_SHOOTING) BREAK
						CASE 10 g_TransitionSpawnData.iSpawnInteractionAnim = ENUM_TO_INT(PLAYER_INTERACTION_SUCK_IT) BREAK
						CASE 11 g_TransitionSpawnData.iSpawnInteractionAnim = ENUM_TO_INT(PLAYER_INTERACTION_TAKE_SELFIE) BREAK
						#ENDIF
					ENDSWITCH
				BREAK
				CASE AUDIO_TAG_HIGH
				CASE AUDIO_TAG_HIGH_HANDS
					iRand = GET_RANDOM_INT_IN_RANGE(0, GET_SPAWN_DANCE_HIGH_INTERACTION_COUNT())
					SWITCH iRand
						CASE 0 	g_TransitionSpawnData.iSpawnInteractionAnim = ENUM_TO_INT(PLAYER_INTERACTION_SALSA_ROLL) BREAK
						CASE 1 	g_TransitionSpawnData.iSpawnInteractionAnim = ENUM_TO_INT(PLAYER_INTERACTION_BANGING_TUNES) BREAK
						CASE 2	g_TransitionSpawnData.iSpawnInteractionAnim = ENUM_TO_INT(PLAYER_INTERACTION_OH_SNAP) BREAK
						CASE 3	g_TransitionSpawnData.iSpawnInteractionAnim = ENUM_TO_INT(PLAYER_INTERACTION_CATS_CRADLE) BREAK
						CASE 4	g_TransitionSpawnData.iSpawnInteractionAnim = ENUM_TO_INT(PLAYER_INTERACTION_FIND_THE_FISH) BREAK
						CASE 5	g_TransitionSpawnData.iSpawnInteractionAnim = ENUM_TO_INT(PLAYER_INTERACTION_HEART_PUMPING) BREAK
						CASE 6	g_TransitionSpawnData.iSpawnInteractionAnim = ENUM_TO_INT(PLAYER_INTERACTION_UNCLE_DISCO) BREAK
						
						#IF FEATURE_HEIST_ISLAND_DANCES
						CASE 7 g_TransitionSpawnData.iSpawnInteractionAnim = ENUM_TO_INT(PLAYER_INTERACTION_CROWD_INVITATION) BREAK
						CASE 8 g_TransitionSpawnData.iSpawnInteractionAnim = ENUM_TO_INT(PLAYER_INTERACTION_DRIVER) BREAK
						CASE 9 g_TransitionSpawnData.iSpawnInteractionAnim = ENUM_TO_INT(PLAYER_INTERACTION_RUNNER) BREAK
						CASE 10 g_TransitionSpawnData.iSpawnInteractionAnim = ENUM_TO_INT(PLAYER_INTERACTION_SHOOTING) BREAK
						CASE 11 g_TransitionSpawnData.iSpawnInteractionAnim = ENUM_TO_INT(PLAYER_INTERACTION_SUCK_IT) BREAK
						CASE 12 g_TransitionSpawnData.iSpawnInteractionAnim = ENUM_TO_INT(PLAYER_INTERACTION_TAKE_SELFIE) BREAK
						#ENDIF
					ENDSWITCH			
				BREAK
			ENDSWITCH		
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL IsDancingInteraction()
	IF (g_TransitionSpawnData.iSpawnInteractionAnim = ENUM_TO_INT(INTERACTION_ANIM_TYPE_PLAYER))
		SWITCH INT_TO_ENUM(PLAYER_INTERACTIONS, g_TransitionSpawnData.iSpawnInteractionAnim)
			CASE PLAYER_INTERACTION_BANGING_TUNES	
			CASE PLAYER_INTERACTION_OH_SNAP	
			CASE PLAYER_INTERACTION_CATS_CRADLE	
			CASE PLAYER_INTERACTION_RAISE_THE_ROOF	
			CASE PLAYER_INTERACTION_FIND_THE_FISH	
			CASE PLAYER_INTERACTION_SALSA_ROLL	
			CASE PLAYER_INTERACTION_HEART_PUMPING	
			CASE PLAYER_INTERACTION_UNCLE_DISCO	
			
			#IF FEATURE_HEIST_ISLAND_DANCES
			CASE PLAYER_INTERACTION_CROWD_INVITATION
			CASE PLAYER_INTERACTION_DRIVER
			CASE PLAYER_INTERACTION_RUNNER
			CASE PLAYER_INTERACTION_SHOOTING
			CASE PLAYER_INTERACTION_SUCK_IT
			CASE PLAYER_INTERACTION_TAKE_SELFIE
			#ENDIF
			
				RETURN TRUE			
		ENDSWITCH
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL ShouldPlayFullBodyNightclubInteraction()
	SWITCH g_TransitionSpawnData.iSpawnActivityLocationOverride
		CASE 0
		CASE 2
			RETURN FALSE
		CASE 1
		CASE 3
			RETURN TRUE
		CASE 4
		
			SWITCH g_eNightClubAudioIntensityLevel 
				CASE AUDIO_TAG_HIGH
				CASE AUDIO_TAG_HIGH_HANDS
					RETURN TRUE
			ENDSWITCH
	ENDSWITCH
	RETURN FALSE
ENDFUNC

PROC UpdateSpawnInteractionAnim()
	
	IF NOT (g_TransitionSpawnData.iSpawnInteractionType > -1)
	OR NOT (g_TransitionSpawnData.iSpawnInteractionAnim > -1)
	OR (g_TransitionSpawnData.iSpawnActivity = ENUM_TO_INT(SPAWN_ACTIVITY_TOILET_SPEW))
		g_TransitionSpawnData.bSpawnInteractionReady = TRUE
		NET_PRINT("[spawning] UpdateSpawnInteractionAnim - values are -1 ") NET_NL()
	ELSE

		IF (g_TransitionSpawnData.iSpawnInteractionState = 0)
			
			IF NOT IS_STAT_DEPENDANT(g_TransitionSpawnData.iSpawnInteractionType, g_TransitionSpawnData.iSpawnInteractionAnim)
			OR (GET_MP_INT_CHARACTER_STAT(GET_DEPENDANT_STAT_FOR_INTERACTION(g_TransitionSpawnData.iSpawnInteractionType, g_TransitionSpawnData.iSpawnInteractionAnim)) > 0)
			OR (g_TransitionSpawnData.bNightClubSpawn)
			
				IF IS_STAT_DEPENDANT(g_TransitionSpawnData.iSpawnInteractionType, g_TransitionSpawnData.iSpawnInteractionAnim)
				
					IF GET_MP_INT_CHARACTER_STAT(GET_DEPENDANT_STAT_FOR_INTERACTION(g_TransitionSpawnData.iSpawnInteractionType, g_TransitionSpawnData.iSpawnInteractionAnim)) < 0
						SET_MP_INT_CHARACTER_STAT(GET_DEPENDANT_STAT_FOR_INTERACTION(g_TransitionSpawnData.iSpawnInteractionType, g_TransitionSpawnData.iSpawnInteractionAnim), 0) 					
						PRINTLN("[spawning] UpdateSpawnInteractionAnim - stat was < 0, setting to 0")
					ENDIF
				
//					PRINTLN("[spawning] UpdateSpawnInteractionAnim - incrementing stat")
//					INCREMENT_MP_INT_CHARACTER_STAT( GET_DEPENDANT_STAT_FOR_INTERACTION(g_TransitionSpawnData.iSpawnInteractionType, g_TransitionSpawnData.iSpawnInteractionAnim))
				ENDIF
				
				IF (g_TransitionSpawnData.bNightClubSpawn)
					IF IsDancingInteraction()
						PRINTLN("[spawning] UpdateSpawnInteractionAnim - IsDancingInteraction")
						SetAppropriateDancingInteraction()
					ENDIF
					START_INTERACTION_ANIM(g_TransitionSpawnData.iSpawnInteractionType, g_TransitionSpawnData.iSpawnInteractionAnim, TRUE, DEFAULT, ShouldPlayFullBodyNightclubInteraction(), TRUE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
				ELSE
					START_INTERACTION_ANIM(g_TransitionSpawnData.iSpawnInteractionType, g_TransitionSpawnData.iSpawnInteractionAnim, TRUE, DEFAULT, TRUE, TRUE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
				ENDIF
				NET_PRINT("[spawning] UpdateSpawnInteractionAnim - told to start interaction type ") NET_PRINT_INT(g_TransitionSpawnData.iSpawnInteractionType) NET_PRINT(", anim ") NET_PRINT_INT(g_TransitionSpawnData.iSpawnInteractionAnim) NET_NL()
				g_TransitionSpawnData.iSpawnInteractionState++
			
			ELSE
				g_TransitionSpawnData.bSpawnInteractionReady = TRUE
				NET_PRINT("[spawning] UpdateSpawnInteractionAnim - player owns none, iType ") NET_PRINT_INT(g_TransitionSpawnData.iSpawnInteractionType) NET_PRINT(", anim ") NET_PRINT_INT(g_TransitionSpawnData.iSpawnInteractionAnim) NET_NL()
			ENDIF

		ENDIF
		
		IF (g_TransitionSpawnData.iSpawnInteractionState = 1)
			IF IS_ANY_INTERACTION_ANIM_PLAYING()
			OR ((MPGlobalsInteractions.PlayerInteraction.iInteractionType = -1) AND (MPGlobalsInteractions.PlayerInteraction.iInteractionAnim = -1))
			OR (MPGlobalsInteractions.PlayerInteraction.bPlayInteractionAnim = FALSE)
				g_TransitionSpawnData.iSpawnInteractionState++
				g_TransitionSpawnData.bSpawnInteractionReady = TRUE
				NET_PRINT("[spawning] UpdateSpawnInteractionAnim - interaction playing or finished, iType ") NET_PRINT_INT(g_TransitionSpawnData.iSpawnInteractionType) NET_PRINT(", anim ") NET_PRINT_INT(g_TransitionSpawnData.iSpawnInteractionAnim) NET_NL()				
			ENDIF
		ENDIF
		
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF NOT (g_TransitionSpawnData.bSpawnInteractionReady)			
			NET_PRINT("[spawning] UpdateSpawnInteractionAnim - waiting on g_TransitionSpawnData.iSpawnInteractionType = ") NET_PRINT_INT(g_TransitionSpawnData.iSpawnInteractionType) 
			NET_PRINT(", g_TransitionSpawnData.iSpawnInteractionAnim = ") NET_PRINT_INT(g_TransitionSpawnData.iSpawnInteractionAnim)  NET_NL()
			NET_PRINT(", g_TransitionSpawnData.iSpawnInteractionState = ") NET_PRINT_INT(g_TransitionSpawnData.iSpawnInteractionState)  NET_NL()
		ELSE
			NET_PRINT("[spawning] UpdateSpawnInteractionAnim - returning TRUE - g_TransitionSpawnData.iSpawnInteractionType = ") NET_PRINT_INT(g_TransitionSpawnData.iSpawnInteractionType) 
			NET_PRINT(", g_TransitionSpawnData.iSpawnInteractionAnim = ") NET_PRINT_INT(g_TransitionSpawnData.iSpawnInteractionAnim)  NET_NL()
			NET_PRINT(", g_TransitionSpawnData.iSpawnInteractionState = ") NET_PRINT_INT(g_TransitionSpawnData.iSpawnInteractionState)  NET_NL()
		ENDIF
	#ENDIF	

ENDPROC



PROC SETUP_CAMERA_FOR_INTRO_PLAYER_CUT()
	
	IF DOES_CAM_EXIST(mCam)
		DESTROY_CAM(mCam)
	ENDIF
	IF DOES_CAM_EXIST(mCam2)
		DESTROY_CAM(mCam2)
	ENDIF

	VECTOR vPlayerCoords = GET_PLAYER_COORDS(PLAYER_ID())
	
	VECTOR v1 = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPlayerCoords,GET_ENTITY_HEADING(PLAYER_PED_ID()),<<fSACam1OffsetX,fSACam1OffsetY,fSACam1OffsetZ>>)
	VECTOR v2 = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPlayerCoords,GET_ENTITY_HEADING(PLAYER_PED_ID()),<<fSACam1PointOffsetX,fSACam1PointOffsetY,fSACam1PointOffsetZ>>)
	
	mCam =  CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
	SET_CAM_FOV(mCam, fSACamFOV)
	SET_CAM_COORD(mCam, v1)
	POINT_CAM_AT_COORD(mCam, v2)

	mCam2 =  CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
	SET_CAM_FOV(mCam2, fSACamFOV)
	
	ATTACH_CAM_TO_PED_BONE(mCam2,PLAYER_PED_ID(),BONETAG_HEAD, <<fSACam2OffsetX,fSACam2OffsetY,fSACam2OffsetZ>>)
	POINT_CAM_AT_PED_BONE(mCam2, PLAYER_PED_ID(),BONETAG_HEAD, <<fSACam2PointOffsetX,fSACam2PointOffsetY,fSACam2PointOffsetZ>>)
	

	SET_CAM_ACTIVE(mCam, TRUE)
	
	RENDER_SCRIPT_CAMS(TRUE, FALSE)
		
	SHAKE_CAM(mCam2,"ROAD_VIBRATION_SHAKE",fSACamShake)
	SHAKE_CAM(mCam,"ROAD_VIBRATION_SHAKE",fSACamShake)
	
	// 	SET_CAM_ACTIVE_WITH_INTERP(mCam2,mCam,4500,GRAPH_TYPE_DECEL)
	
ENDPROC

PROC SetCameraForClubSpawn(INT iLocation, CAMERA_INDEX CamId)
	SWITCH iLocation
		CASE 0 
			SET_CAM_COORD(CamId, <<-1589.0736, -3015.3533, -74.8535>>)
			SET_CAM_ROT(CamId, <<-15.8449, -0.0000, 78.0001>>)
			SET_CAM_FOV(CamId, 25.1368)
		BREAK
		CASE 2
			SET_CAM_COORD(CamId, <<-1589.8073, -3008.4263, -74.9934>>)
			SET_CAM_ROT(CamId, <<-17.1168, -0.0000, 104.7282>>)
			SET_CAM_FOV(CamId, 34.7654)	
		BREAK
		CASE 4
			SET_CAM_COORD(CamId, <<-1592.2372, -3008.1885, -78.1660>>)
			SET_CAM_ROT(CamId, <<-3.0250, -0.0000, 121.1804>>)
			SET_CAM_FOV(CamId, 35.9)	
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL DoNightClubCam()
	SWITCH g_TransitionSpawnData.iSpawnActivityLocationOverride
		CASE 0 
		CASE 2
		CASE 4 // dancing
			RETURN TRUE
	ENDSWITCH
	RETURN FALSE
ENDFUNC



PROC SETUP_CAMERA_FOR_NIGHTCLUB()

	IF (INT_TO_ENUM(PLAYER_SPAWN_ACTIVITY, g_TransitionSpawnData.iSpawnActivity) = SPAWN_ACTIVITY_NOTHING)

		IF (g_TransitionSpawnData.bNightClubSpawn)
		
			IF DoNightClubCam()
			
				IF NOT DOES_CAM_EXIST(mCam)

					IF DOES_CAM_EXIST(mCam2)
						DESTROY_CAM(mCam2)
					ENDIF

					mCam =  CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
					
					SetCameraForClubSpawn(g_TransitionSpawnData.iSpawnActivityLocationOverride, mCam)

					SHAKE_CAM(mCam, "HAND_SHAKE", 0.2)
					SET_CAM_ACTIVE(mCam, TRUE)
					RENDER_SCRIPT_CAMS(TRUE, FALSE)	
					
					PRINTLN("[spawning] SETUP_CAMERA_FOR_NIGHTCLUB - created")
					
				ENDIF
				
				
			ENDIF
			
		ENDIF
	ENDIF
		
ENDPROC


FUNC BOOL IsSwoopDownAlmostFinished()
	
	IF IS_SCREEN_FADED_OUT()
	OR IS_SCREEN_FADING_OUT()
		RETURN FALSE
	ENDIF

	IF IS_PLAYER_SWITCH_IN_PROGRESS() 
    	IF GET_PLAYER_SWITCH_TYPE() != SWITCH_TYPE_SHORT
            IF GET_PLAYER_SWITCH_STATE() > SWITCH_STATE_OUTRO_HOLD
				RETURN(TRUE)
			ENDIF
		ENDIF
	ELSE
		RETURN(TRUE)
	ENDIF
	RETURN(FALSE)
ENDFUNC

#IF FEATURE_FIXER
OBJECT_INDEX ObjectId
INT iGameTime
FUNC BOOL UpdateSittingSmoking()

	FLOAT fTemp
	VECTOR vTemp

	// load anims
	IF (g_TransitionSpawnData.iSpawnActivityState = 0)
		REQUEST_ANIM_DICT("switch@michael@smoking2")
		REQUEST_MODEL(PROP_CIGAR_01)
		IF HAS_ANIM_DICT_LOADED("switch@michael@smoking2")
		AND HAS_MODEL_LOADED(PROP_CIGAR_01)					
			g_TransitionSpawnData.iSpawnActivityState++								
			IF DOES_CAM_EXIST(mCam)
				DESTROY_CAM(mCam)
			ENDIF
			IF DOES_CAM_EXIST(mCam2)
				DESTROY_CAM(mCam2)
			ENDIF
		ENDIF
	ENDIF

	// start sync scene
	IF (g_TransitionSpawnData.iSpawnActivityState = 1)
			
		FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
		SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE) 
		
		vTemp = GET_ENTITY_COORDS(PLAYER_PED_ID())
		vTemp.z += -1.0										
			
		IF (VDIST(g_SpawnData.vSpawnCoords, vTemp) > 2.0)
			iSyncSceneID = CREATE_SYNCHRONIZED_SCENE(vTemp, GET_ENTITY_ROTATION(PLAYER_PED_ID()))
		ELSE
			iSyncSceneID = CREATE_SYNCHRONIZED_SCENE(g_SpawnData.vSpawnCoords, GET_ENTITY_ROTATION(PLAYER_PED_ID()))
		ENDIF
		
		mCam = CREATE_CAMERA(CAMTYPE_ANIMATED, TRUE)
		TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), iSyncSceneID, "switch@michael@smoking2", "exit", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
		SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iSyncSceneID, TRUE)
		SET_SYNCHRONIZED_SCENE_LOOPED(iSyncSceneID, TRUE)		
		
		PLAY_SYNCHRONIZED_CAM_ANIM(mCam, iSyncSceneID, "exit_cam", "switch@michael@smoking2")
		SET_CAM_ANIM_CURRENT_PHASE(mCam, 0.0)										
		
		ObjectId = CREATE_OBJECT(PROP_CIGAR_01, vTemp, FALSE)
		ATTACH_ENTITY_TO_ENTITY(ObjectId, PLAYER_PED_ID(),
								GET_PED_BONE_INDEX(PLAYER_PED_ID(), BONETAG_PH_R_HAND),
								<<0.0,0.0,0.0>>, <<0.0,0.0,0.0>>)
		
		SET_SYNCHRONIZED_SCENE_RATE(iSyncSceneID, 0.0)
		
		g_TransitionSpawnData.iSpawnActivityState++
		iGameTime = GET_GAME_TIMER()
		g_TransitionSpawnData.bSpawnActivityReady = TRUE
	ENDIF

	IF (g_TransitionSpawnData.iSpawnActivityState = 2)
		IF (GET_GAME_TIMER() - iGameTime) >= 1000
		
			// wait until we are good to start
			IF IS_SKYSWOOP_AT_GROUND()
		
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iSyncSceneID)									
					RENDER_SCRIPT_CAMS(TRUE, FALSE)		
				ENDIF
		
				SET_SYNCHRONIZED_SCENE_RATE(iSyncSceneID, 0.7)
				g_TransitionSpawnData.iSpawnActivityState++
				
			ELSE
				PRINTLN("UpdateSittingSmoking - waiting on IS_SKYSWOOP_AT_GROUND")
			ENDIF
		ENDIF
	ENDIF
	
	// wait until finished
	IF (g_TransitionSpawnData.iSpawnActivityState = 3)								
		IF IS_SYNCHRONIZED_SCENE_RUNNING(iSyncSceneID)
			fTemp = GET_SYNCHRONIZED_SCENE_PHASE(iSyncSceneID)
			PRINTLN("[spawning] UPDATE_SPAWN_ACTIVITIES_AFTER_JOINING - SPAWN_ACTIVITY_SITTING_SMOKING (exit) - phase = ", fTemp)
			
			// chuck cigar
			IF (fTemp >= 0.85)
				IF IS_ENTITY_ATTACHED(ObjectId)
					DETACH_ENTITY(ObjectId)
					FREEZE_ENTITY_POSITION(ObjectId, FALSE)
					SET_ENTITY_DYNAMIC(ObjectId, TRUE)
				ENDIF
			ENDIF
			
			IF (fTemp >= 0.99)
				g_TransitionSpawnData.iSpawnActivityState++	
			ENDIF
		ELSE
			g_TransitionSpawnData.iSpawnActivityState++
		ENDIF
	ENDIF
	
	// cleanup
	IF (g_TransitionSpawnData.iSpawnActivityState = 4)	
		SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
		SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)						
		CLEAR_PED_TASKS(PLAYER_PED_ID())							
		RENDER_SCRIPT_CAMS(FALSE, FALSE)		
		IF DOES_CAM_EXIST(mCam)
			DESTROY_CAM(mCam)
		ENDIF
		REMOVE_ANIM_DICT("switch@michael@smoking2")	
		SET_OBJECT_AS_NO_LONGER_NEEDED(ObjectId)
		SET_MODEL_AS_NO_LONGER_NEEDED(PROP_CIGAR_01)
		bGivenTask = FALSE
		g_TransitionSessionNonResetVars.sPostMissionCleanupData.PostMissionSpawnScenario = POST_MISSION_SPAWN_INVALID
		NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		RETURN(TRUE)					
	ENDIF
	
	RETURN FALSE
					
ENDFUNC



FUNC BOOL UpdateWakeUpOnFloor()


	// load anims
	IF (g_TransitionSpawnData.iSpawnActivityState = 0)
		REQUEST_ANIM_DICT("GET_UP@DIRECTIONAL@TRANSITION@PRONE_TO_SEATED@INJURED")
		REQUEST_ANIM_DICT("get_up@directional@movement@from_seated@standard")
		IF HAS_ANIM_DICT_LOADED("GET_UP@DIRECTIONAL@TRANSITION@PRONE_TO_SEATED@INJURED")
		AND HAS_ANIM_DICT_LOADED("get_up@directional@movement@from_seated@standard")
			g_TransitionSpawnData.iSpawnActivityState++								
			IF DOES_CAM_EXIST(mCam)
				DESTROY_CAM(mCam)
			ENDIF
			IF DOES_CAM_EXIST(mCam2)
				DESTROY_CAM(mCam2)
			ENDIF
		ENDIF
	ENDIF

	// start sync scene
	IF (g_TransitionSpawnData.iSpawnActivityState = 1)
			
		FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
		SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE) 											
											
		TASK_PLAY_ANIM(PLAYER_PED_ID(), "GET_UP@DIRECTIONAL@TRANSITION@PRONE_TO_SEATED@INJURED", "front", INSTANT_BLEND_IN, SLOW_BLEND_OUT, DEFAULT, AF_HOLD_LAST_FRAME)
												
		
		g_TransitionSpawnData.iSpawnActivityState++
		iGameTime = GET_GAME_TIMER()
		g_TransitionSpawnData.bSpawnActivityReady = TRUE
	ENDIF

	IF (g_TransitionSpawnData.iSpawnActivityState = 2)
		IF (GET_GAME_TIMER() - iGameTime) >= 1000
		
			// wait until we are good to start
			IF IS_SKYSWOOP_AT_GROUND()
		
				//Player_Takes_Alcohol_Hit(PLAYER_PED_ID(), DEFAULT, 1)
		
				IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "GET_UP@DIRECTIONAL@TRANSITION@PRONE_TO_SEATED@INJURED", "front")
					SET_ENTITY_ANIM_SPEED(PLAYER_PED_ID(), "GET_UP@DIRECTIONAL@TRANSITION@PRONE_TO_SEATED@INJURED", "front", 0.7)
				ENDIF
				
				iGameTime = GET_GAME_TIMER()
				g_TransitionSpawnData.iSpawnActivityState++
				RETURN FALSE
				
			ELSE			
				PRINTLN("UpdateWakeUpOnFloor - waiting on IS_SKYSWOOP_AT_GROUND")
			ENDIF
		ENDIF
		
		IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "GET_UP@DIRECTIONAL@TRANSITION@PRONE_TO_SEATED@INJURED", "front")
			SET_ENTITY_ANIM_SPEED(PLAYER_PED_ID(), "GET_UP@DIRECTIONAL@TRANSITION@PRONE_TO_SEATED@INJURED", "front", 0.0)
		ENDIF		
	ENDIF
	
	// wait until finished
	IF (g_TransitionSpawnData.iSpawnActivityState = 3)		
		IF (IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "GET_UP@DIRECTIONAL@TRANSITION@PRONE_TO_SEATED@INJURED", "front") AND (GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), "GET_UP@DIRECTIONAL@TRANSITION@PRONE_TO_SEATED@INJURED", "front") >= 0.85))
		OR NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "GET_UP@DIRECTIONAL@TRANSITION@PRONE_TO_SEATED@INJURED", "front") 
			TASK_PLAY_ANIM(PLAYER_PED_ID(),"get_up@directional@movement@from_seated@standard", "get_up_l_0", SLOW_BLEND_IN, SLOW_BLEND_OUT, DEFAULT, AF_HOLD_LAST_FRAME)
			g_TransitionSpawnData.iSpawnActivityState++	
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF (g_TransitionSpawnData.iSpawnActivityState = 4)		
		IF (IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "get_up@directional@movement@from_seated@standard", "get_up_l_0") AND (GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), "get_up@directional@movement@from_seated@standard", "get_up_l_0") >= 0.85))
		OR NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "get_up@directional@movement@from_seated@standard", "get_up_l_0") 						
			g_TransitionSpawnData.iSpawnActivityState++	
		ELSE
			IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "GET_UP@DIRECTIONAL@TRANSITION@PRONE_TO_SEATED@INJURED", "front")
				SET_ENTITY_ANIM_SPEED(PLAYER_PED_ID(), "GET_UP@DIRECTIONAL@TRANSITION@PRONE_TO_SEATED@INJURED", "front", 0.7)
			ENDIF		
		ENDIF
	ENDIF

	
	// cleanup
	IF (g_TransitionSpawnData.iSpawnActivityState = 5)	
		SET_GAMEPLAY_CAM_RELATIVE_HEADING(160)
		SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)					
		CLEAR_PED_TASKS(PLAYER_PED_ID())							

		REMOVE_ANIM_DICT("GET_UP@DIRECTIONAL@TRANSITION@PRONE_TO_SEATED@INJURED")
		REMOVE_ANIM_DICT("get_up@directional@movement@from_seated@standard")		
			
		
		bGivenTask = FALSE
		g_TransitionSessionNonResetVars.sPostMissionCleanupData.PostMissionSpawnScenario = POST_MISSION_SPAWN_INVALID
		NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		
		Quit_Drunk_Camera_Gradually(10000)
		RETURN(TRUE)					
	ENDIF
	
	RETURN FALSE
					
ENDFUNC
#ENDIF

PROC UpdateSpawnActivity()

//	INT i

	//NET_PRINT("[spawning] UpdateSpawnActivity - player coords = ") NET_PRINT_VECTOR(GET_PLAYER_COORDS(PLAYER_ID())) NET_NL()	
	

	IF NOT (g_TransitionSpawnData.iSpawnActivity > -1)
		
		g_TransitionSpawnData.bSpawnActivityReady = TRUE
		
	ELSE
	

		SWITCH INT_TO_ENUM(PLAYER_SPAWN_ACTIVITY, g_TransitionSpawnData.iSpawnActivity)
			CASE SPAWN_ACTIVITY_NOTHING

				
				SWITCH g_TransitionSpawnData.iSpawnActivityState
					
					CASE 0
					
						IF IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, FALSE)

							//SETUP_CAMERA_FOR_INTRO_PLAYER_CUT()
							g_TransitionSpawnData.bSpawnActivityReady = TRUE
						
						
						ENDIF
						
					
					BREAK
					
					
				ENDSWITCH				
				
				
			BREAK
			
			CASE SPAWN_ACTIVITY_DRIVING_PERSONAL_VEHICLE
				IF (g_TransitionSpawnData.iSpawnActivityState = 0)
					IF CAN_PLAYER_SPAWN_A_PERSONAL_VEHICLE()
						g_TransitionSpawnData.iSpawnInSavedVehicleState = 0		
						
						// set flags for how we want the personal vehicle to spawn.
						g_TransitionSpawnData.bWarpPlayerIntoSavedVehicle = TRUE
						g_TransitionSpawnData.bSwitchSavedVehicleEngineOn = TRUE
						g_TransitionSpawnData.bSpawnNearbySavedVehicleConsiderHighways = TRUE
						g_TransitionSpawnData.bFreezeSavedVehicle = TRUE
						
						g_TransitionSpawnData.iSpawnActivityState++
					ELSE
						NET_PRINT("[spawning] UpdateSpawnActivity - player cant spawn a personal vehicle - SPAWN_ACTIVITY_DRIVING_PERSONAL_VEHICLE")  NET_NL()
						g_TransitionSpawnData.bSpawnActivityReady = TRUE
					ENDIF
				ENDIF
				
				IF (g_TransitionSpawnData.iSpawnActivityState = 1)
					IF CAN_PLAYER_SPAWN_A_PERSONAL_VEHICLE()
						IF IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, FALSE)
							IF SPAWN_PERSONAL_VEHICLE_NEAR_PLAYER(FALSE)								
								g_TransitionSpawnData.iSpawnActivityState++
								NET_PRINT("[spawning] UpdateSpawnActivity - vehicle spawned near player")  NET_NL()
							ELSE
								NET_PRINT("[spawning] UpdateSpawnActivity - waiting on SPAWN_PERSONAL_VEHICLE_NEAR_PLAYER")  NET_NL()	
							ENDIF
						ELSE
							NET_PRINT("[spawning] UpdateSpawnActivity - player is not ok")  NET_NL()	
						ENDIF
					ELSE
						NET_PRINT("[spawning] UpdateSpawnActivity - player cant spawn a personal vehicle")  NET_NL()
						g_TransitionSpawnData.bSpawnActivityReady = TRUE	
					ENDIF
				ENDIF
				
				IF (g_TransitionSpawnData.iSpawnActivityState = 2)
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//						IF g_TransitionSpawnData.iEntryWantedLevel > 0
//							TASK_VEHICLE_DRIVE_WANDER(PLAYER_PED_ID(), GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), 35.0, DRIVINGMODE_AVOIDCARS )
//						ELSE
//							TASK_VEHICLE_DRIVE_WANDER(PLAYER_PED_ID(), GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), 20.0, DRIVINGMODE_STOPFORCARS )	
//						ENDIF			
						NET_PRINT("[spawning] UpdateSpawnActivity - given TASK_VEHICLE_DRIVE_WANDER")  NET_NL()	
						g_TransitionSpawnData.bSpawnActivityReady = TRUE
					ENDIF
				ENDIF
			BREAK
				
			CASE SPAWN_ACTIVITY_WALKING
				IF IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, FALSE)
					IF g_TransitionSpawnData.iEntryWantedLevel > 0
						SET_NEXT_DESIRED_MOVE_STATE( PEDMOVE_RUN )
						TASK_WANDER_STANDARD(PLAYER_PED_ID())
					ELSE
						TASK_WANDER_STANDARD(PLAYER_PED_ID())
					ENDIF
					bGivenTask = TRUE
					NET_PRINT("[spawning] UpdateSpawnActivity - given TASK_WANDER_STANDARD")  NET_NL()	
					g_TransitionSpawnData.bSpawnActivityReady = TRUE
				ENDIF
			BREAK
			
			CASE SPAWN_ACTIVITY_DRUNK_AWAKEN
				#IF FEATURE_HEIST_ISLAND
				IF IS_BIT_SET(g_SimpleInteriorData.iEighthBS, BS8_SIMPLE_INTERIOR_SPAWNING_OFF_ISLAND)
					IF IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, FALSE)
						IF NOT IS_BIT_SET(iSyncSceneBS, 0)
							HEIST_ISLAND_TRAVEL__LEAVE_ISLAND(HEIST_ISLAND_LEAVE_FADE_OUT)
							
							SET_BIT(iSyncSceneBS, 0)
						ELIF HEIST_ISLAND_TRAVEL__IS_TRANSITION_FINISHED()
							REQUEST_ANIM_DICT(GET_BEACH_SCENE_ANIM_DICT())
							
							IF HAS_ANIM_DICT_LOADED(GET_BEACH_SCENE_ANIM_DICT())
								VECTOR vScenePosition, vSceneRotation
								
								SWITCH g_TransitionSpawnData.iSyncScenePosition
									CASE 0
										vScenePosition = <<-1355.0016, -1673.9679, 2.1269>>
										vSceneRotation = <<0.0, 0.0, 313.8870>>
									BREAK
									
									CASE 1
										vScenePosition = <<-1409.1108, -1574.2098, 1.1427>>
										vSceneRotation = <<0.0, 0.0, 302.7025>>
									BREAK
									
									CASE 2
										vScenePosition = <<-1559.4794, -1230.7761, 0.6671>>
										vSceneRotation = <<0.0, 0.0, 302.0941>>
									BREAK
									
									CASE 3
										vScenePosition = <<-1545.4159, -1251.4567, 1.0141>>
										vSceneRotation = <<0.0, 0.0, 309.1964>>
									BREAK
									
									CASE 4
										vScenePosition = <<-1516.5511, -1326.3306, 1.0825>>
										vSceneRotation = <<0.0, 0.0, 290.5325>>
									BREAK
									
									CASE 5
										vScenePosition = <<-1500.7712, -1391.5809, 0.8837>>
										vSceneRotation = <<0.0, 0.0, 287.5468>>
									BREAK
								ENDSWITCH
								
								SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
								SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
								
								iSyncSceneID = NETWORK_CREATE_SYNCHRONISED_SCENE(vScenePosition, vSceneRotation, DEFAULT, TRUE)
								
								NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), iSyncSceneID, GET_BEACH_SCENE_ANIM_DICT(), "action", INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT, REALLY_SLOW_BLEND_IN)
								
								NETWORK_START_SYNCHRONISED_SCENE(iSyncSceneID)
								
								mCam = CREATE_CAMERA(CAMTYPE_ANIMATED, TRUE)
								
								PLAY_CAM_ANIM(mCam, "action_camera", GET_BEACH_SCENE_ANIM_DICT(), vScenePosition, vSceneRotation)
								
								RENDER_SCRIPT_CAMS(TRUE, FALSE)
								
								NET_PRINT("[spawning] UpdateSpawnActivity - Set up SPAWN_ACTIVITY_DRUNK_AWAKEN")  NET_NL()
								
								g_TransitionSpawnData.bSpawnActivityReady = TRUE
							ENDIF
						ENDIF
					ENDIF
				ELSE
				#ENDIF
					IF (g_TransitionSpawnData.bSkipDrunkenWakeupAnim)
						bGivenTask = TRUE
						
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(160)
						SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
						
						NET_PRINT("[spawning] UpdateSpawnActivity - Set up SPAWN_ACTIVITY_DRUNK_AWAKEN- skipping anim")  NET_NL()
						g_TransitionSpawnData.bSpawnActivityReady = TRUE
						g_TransitionSpawnData.bSkipDrunkenWakeupAnim = FALSE
						
					ELSE
						IF g_SpawnData.bLaunchMission
							IF NOT g_SpawnData.bMissionSetup
							AND NOT g_SpawnData.bMissionFailedToLaunch
								IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
								AND GB_GET_NUM_GOONS_IN_LOCAL_GANG() = 0
								AND GB_GET_BOSS_MISSION_UNAVAILABLE_REASON(PLAYER_ID(), FMMC_TYPE_GB_CASINO) = GB_MU_REASON_AVAILABLE
									REQUEST_LAUNCH_GB_CASINO_MISSION(CSV_PASSED_OUT)
									g_SpawnData.bMissionSetup = TRUE
								ELSE
									g_SpawnData.bMissionFailedToLaunch = TRUE
								ENDIF
							ELSE
								IF NOT g_SpawnData.bMissionFailedToLaunch
									IF GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) = FMMC_TYPE_GB_CASINO
										INT iMission
										iMission = -1
										
										SWITCH INT_TO_ENUM(GBC_SUBVARIATION, GB_GET_CONTRABAND_SUBVARIATION_PLAYER_IS_ON(PLAYER_ID()))
											CASE CSS_PO_CHILIAD				iMission = 0	BREAK
											CASE CSS_PO_VESPUCCI_BEACH		iMission = 1	BREAK
											CASE CSS_PO_HARMONY				iMission = 2	BREAK
											CASE CSS_PO_TONGVA_HILLS		iMission = 3	BREAK
										ENDSWITCH
										
										SET_CASINO_DRUNK_SPAWN_LOCATION(g_SpawnData.eDrunkSpawnLocation, iMission)
										
										Player_Takes_Alcohol_Hit(PLAYER_PED_ID(), DEFAULT, 3)
									ELIF NOT GB_IS_PLAYER_LAUNCHING_THIS_GANG_BOSS_MISSION(PLAYER_ID(), FMMC_TYPE_GB_CASINO)
										SET_CASINO_DRUNK_SPAWN_LOCATION(g_SpawnData.eDrunkSpawnLocation)
									ELSE
										EXIT
									ENDIF
								ELSE
									SET_CASINO_DRUNK_SPAWN_LOCATION(g_SpawnData.eDrunkSpawnLocation, GET_RANDOM_INT_IN_RANGE(0, 4))
								ENDIF
								
								g_SpawnData.bMissionFailedToLaunch = FALSE
								g_SpawnData.bLaunchMission = FALSE
								g_SpawnData.bMissionSetup = FALSE
							ENDIF
						ENDIF
						
						IF IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, FALSE)
						AND IS_ENTITY_ALIVE(PLAYER_PED_ID())
							REQUEST_ANIM_DICT("GET_UP@DIRECTIONAL@TRANSITION@PRONE_TO_SEATED@INJURED")
							IF HAS_ANIM_DICT_LOADED("GET_UP@DIRECTIONAL@TRANSITION@PRONE_TO_SEATED@INJURED")
								IF NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "GET_UP@DIRECTIONAL@TRANSITION@PRONE_TO_SEATED@INJURED", "front")
									TASK_PLAY_ANIM(PLAYER_PED_ID(), "GET_UP@DIRECTIONAL@TRANSITION@PRONE_TO_SEATED@INJURED", "front", INSTANT_BLEND_IN, DEFAULT, DEFAULT, AF_HOLD_LAST_FRAME, 0.1)
									bGivenTask = TRUE
								ELSE
									SET_GAMEPLAY_CAM_RELATIVE_HEADING(160)
									SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
									
									NET_PRINT("[spawning] UpdateSpawnActivity - Set up SPAWN_ACTIVITY_DRUNK_AWAKEN")  NET_NL()
									g_TransitionSpawnData.bSpawnActivityReady = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				#IF FEATURE_HEIST_ISLAND
				ENDIF
				#ENDIF
			BREAK
			
			#IF FEATURE_FIXER
			CASE SPAWN_ACTIVITY_SITTING_SMOKING
				UpdateSittingSmoking()				
			BREAK
			CASE SPAWN_ACTIVITY_WAKE_UP_ON_FLOOR_SMOKING
				UpdateWakeUpOnFloor()
			BREAK
			#ENDIF
			
			// appartment activities 
			CASE SPAWN_ACTIVITY_WATCHING_TV
			CASE SPAWN_ACTIVITY_SHOWER
			CASE SPAWN_ACTIVITY_BED
			CASE SPAWN_ACTIVITY_TOILET_SPEW
				IF IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, FALSE)
					IF (g_TransitionSpawnData.iSpawnActivityState = 0)
						REQUEST_SPAWN_ACTIVITY(INT_TO_ENUM(PLAYER_SPAWN_ACTIVITY, g_TransitionSpawnData.iSpawnActivity))
						
						IF NOT g_SpawnData.bSpawningInSimpleInterior
						AND NOT IS_LOCAL_PLAYER_IN_ANY_SIMPLE_INTERIOR()
						AND NOT SHOULD_BYPASS_BOUNDS_LAUNCHING_SAFETY_CHECKS()
							g_SpawnData.bSpawningInProperty = TRUE
							PRINTLN("UpdateSpawnActivity - g_SpawnData.bSpawningInProperty = TRUE ")
						ENDIF
						
						SpawnTimer = GET_NETWORK_TIME()
						g_TransitionSpawnData.iSpawnActivityState++
					ENDIF
					IF (g_TransitionSpawnData.iSpawnActivityState = 1)
						IF NOT HAS_SPAWN_ACTIVITY_BEEN_REQUESTED(INT_TO_ENUM(PLAYER_SPAWN_ACTIVITY, g_TransitionSpawnData.iSpawnActivity))
						OR GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), SpawnTimer) > 20000
							
							#IF IS_DEBUG_BUILD
							IF (GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), SpawnTimer) > 20000)
								NET_PRINT("UpdateSpawnActivity - hit timeout, why?") NET_NL()
								//SCRIPT_ASSERT("UpdateSpawnActivity - hit timeout, why?")
							ENDIF
							#ENDIF							
							
							g_SimpleInteriorData.eSimpleInteriorIDToSpawnInDrunk = SIMPLE_INTERIOR_INVALID
							PRINTLN("UpdateSpawnActivity - g_SimpleInteriorData.eSimpleInteriorIDToSpawnInDrunk = SIMPLE_INTERIOR_INVALID")
							
							g_TransitionSpawnData.bSpawnActivityReady = TRUE
							CLEAR_SPAWN_ACTIVITY_FLAGS()
						ELSE
							bTransitionSpawningInProperty = TRUE
							
							IF NOT g_SpawnData.bSpawningInSimpleInterior
							AND NOT IS_LOCAL_PLAYER_IN_ANY_SIMPLE_INTERIOR()
							AND NOT SHOULD_BYPASS_BOUNDS_LAUNCHING_SAFETY_CHECKS()
								MAINTAIN_LAUNCHING_MP_INTERIOR_SCRIPT()
							ENDIF
							
							IF NOT (bBrainsEnabled)
								ENABLE_SCRIPT_BRAIN_SET(SCRIPT_BRAIN_GROUP_FREEMODE)
								ENABLE_SCRIPT_BRAIN_SET(SCRIPT_BRAIN_GROUP_MULTIPLAYER)
								bBrainsEnabled = TRUE
								NET_PRINT("[spawning] UpdateSpawnActivity - enabling brains") NET_NL()
							ENDIF
							NET_PRINT("[spawning] UpdateSpawnActivity - waiting on IS_APPARTMENT_SPAWN_ACTIVITY_LOADING - g_TransitionSpawnData.iSpawnActivity=") NET_PRINT_INT(g_TransitionSpawnData.iSpawnActivity) NET_NL()
						ENDIF
					ENDIF
				ENDIF
			BREAK

			
			DEFAULT
				NET_PRINT("[spawning] UpdateSpawnActivity - unknown spawn activity - ") NET_PRINT_INT(g_TransitionSpawnData.iSpawnActivity) NET_NL()
				SCRIPT_ASSERT("UpdateSpawnActivity - unknow activity") 
			BREAK
		ENDSWITCH
			

	
	ENDIF
	
//	// give player wanted level
//	IF (g_TransitionSpawnData.bSpawnActivityReady)
//		
//		IF g_TransitionSpawnData.iEntryWantedLevel > 0
//			SET_PLAYER_WANTED_LEVEL( PLAYER_ID(), g_TransitionSpawnData.iEntryWantedLevel)
//			SET_PLAYER_WANTED_LEVEL_NOW( PLAYER_ID() )
//			g_TransitionSpawnData.iEntryWantedLevel = 0 
//		ENDIF
//	
//				
//	ENDIF
	
	
	#IF IS_DEBUG_BUILD
		IF NOT (g_TransitionSpawnData.bSpawnActivityReady)										
			NET_PRINT("[spawning] UpdateSpawnActivity - waiting on g_TransitionSpawnData.iSpawnActivity = ") NET_PRINT_INT(g_TransitionSpawnData.iSpawnActivity) 
			NET_PRINT(", g_TransitionSpawnData.iSpawnActivityState = ") NET_PRINT_INT(g_TransitionSpawnData.iSpawnActivityState)  NET_NL()
		ELSE
			NET_PRINT("[spawning] UpdateSpawnActivity - returing TRUE - g_TransitionSpawnData.iSpawnActivity = ") NET_PRINT_INT(g_TransitionSpawnData.iSpawnActivity) 
			NET_PRINT(", g_TransitionSpawnData.iSpawnActivityState = ") NET_PRINT_INT(g_TransitionSpawnData.iSpawnActivityState)  NET_NL()	
		ENDIF
	#ENDIF
	
ENDPROC


FUNC BOOL HAS_PLAYER_TRIED_TO_MOVE()
	IF IS_ANY_BUTTON_PRESSED(PLAYER_CONTROL)
		RETURN TRUE
	ENDIF
	NET_PRINT("[spawning] HAS_PLAYER_TRIED_TO_MOVE - returning FALSE ")  NET_NL()
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_PERFORMING_TASK(SCRIPT_TASK_NAME ScriptTask)
	SCRIPTTASKSTATUS TaskStatus = GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), ScriptTask)
	IF NOT (TaskStatus = FINISHED_TASK)
		RETURN(TRUE)
	ENDIF
	NET_PRINT("[spawning] IS_PLAYER_PERFORMING_TASK - returning FALSE ")  NET_NL()
	RETURN(FALSE)
ENDFUNC

FUNC BOOL HAS_SPAWN_ACTIVITY_TIMER_EXPIRED()
	IF (g_TransitionSpawnData.bStartedTimer)		
		IF ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_TransitionSpawnData.iStartTime)) > iSACamDuration
			RETURN(TRUE)
		ENDIF
	ENDIF
	NET_PRINT("[spawning] HAS_SPAWN_ACTIVITY_TIMER_EXPIRED - returning FALSE ")  NET_NL()
	RETURN(FALSE)
ENDFUNC

FUNC BOOL UPDATE_SPAWN_ACTIVITIES_AFTER_JOINING()
	
	//NET_PRINT("[spawning] UPDATE_SPAWN_ACTIVITIES_AFTER_JOINING - player coords = ") NET_PRINT_VECTOR(GET_PLAYER_COORDS(PLAYER_ID())) NET_NL()
	VEHICLE_INDEX CarID

	IF (g_TransitionSpawnData.bSpawnActivityReady)
		
		IF NOT NETWORK_IS_IN_MP_CUTSCENE()
		AND NOT IS_CUTSCENE_PLAYING()
		AND NOT IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP() 
		AND NOT IS_PLAYER_SPECTATING(PLAYER_ID())
			SET_LOCAL_PLAYER_VISIBLE_LOCALLY() // make sure player is visible as we swoop in.
		ENDIF
		
		IF IsSwoopDownAlmostFinished()
			IF NOT (bEnabledCollision)				
				IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					SET_ENTITY_COLLISION(PLAYER_PED_ID(), TRUE)
					FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
				ELSE
					CarID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					IF NOT IS_ENTITY_DEAD(CarID)
					AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					
						FREEZE_ENTITY_POSITION(CarID, FALSE)
						SET_VEHICLE_ENGINE_ON(CarID, TRUE, TRUE)
						APPLY_FORCE_TO_ENTITY(CarID, APPLY_TYPE_IMPULSE, <<0.0, 0.0,  -1.0>>, <<0.0, 0.0, 0.0>>, 0, TRUE,TRUE,TRUE)	
						#IF FEATURE_DLC_1_2022
						IF NOT IS_BIT_SET(g_sSummer2022Flow.iBitSet, ciSUM22_FLOW_BITSET_SPAWNING_IN_VEHICLE_FOR_INTRO_VEH_RADIO)
						#ENDIF
							TASK_VEHICLE_DRIVE_WANDER(PLAYER_PED_ID(), CarID, 20.0, DRIVINGMODE_STOPFORCARS )
						#IF FEATURE_DLC_1_2022	
						ENDIF
						#ENDIF
						
						bGivenTask = TRUE
					ENDIF					
				ENDIF				
				NET_PRINT("[spawning] UPDATE_SPAWN_ACTIVITIES_AFTER_JOINING - enabling player collision") NET_NL()
				bEnabledCollision = TRUE
			ENDIF
			
		ENDIF
		
		SETUP_CAMERA_FOR_NIGHTCLUB()
		
		IF IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, TRUE)
		
			// start timer
			IF NOT (g_TransitionSpawnData.bStartedTimer)					
				g_TransitionSpawnData.iStartTime = GET_NETWORK_TIME()
				g_TransitionSpawnData.bStartedTimer = TRUE
			ENDIF
		
			SWITCH INT_TO_ENUM(PLAYER_SPAWN_ACTIVITY, g_TransitionSpawnData.iSpawnActivity)
				
				CASE SPAWN_ACTIVITY_NOTHING
				
					// activate cameras
					IF NOT (g_TransitionSpawnData.bSplineCamStarted)
						//SET_CAM_ACTIVE_WITH_INTERP(mCam2,mCam,iSACamDuration,GRAPH_TYPE_DECEL)
						g_TransitionSpawnData.iStartTime = GET_NETWORK_TIME()
						g_TransitionSpawnData.bSplineCamStarted = TRUE
					ENDIF
				
					IF HAS_PLAYER_TRIED_TO_MOVE()
					OR HAS_SPAWN_ACTIVITY_TIMER_EXPIRED()													

						IF (g_TransitionSpawnData.bNightClubSpawn)
							IF DOES_CAM_EXIST(mCam)
								STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP()
							ENDIF
							IF DOES_CAM_EXIST(mCam)
								DESTROY_CAM(mCam, TRUE)
							ENDIF					
						ENDIF

						RETURN(TRUE)
					ENDIF
					
				BREAK
				CASE SPAWN_ACTIVITY_DRIVING_PERSONAL_VEHICLE	
					IF HAS_PLAYER_TRIED_TO_MOVE()
					OR NOT IS_PLAYER_PERFORMING_TASK(SCRIPT_TASK_VEHICLE_DRIVE_WANDER)
					OR HAS_SPAWN_ACTIVITY_TIMER_EXPIRED()
					
						IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							CarID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
							FREEZE_ENTITY_POSITION(CarID, FALSE)
							SET_ENTITY_CAN_BE_DAMAGED(CarID, TRUE)
							NET_PRINT("[spawning] UPDATE_SPAWN_ACTIVITIES_AFTER_JOINING - player vehicle unfrozen") NET_NL()
						ENDIF
						
						g_TransitionSpawnData.bMakeSureVehicleCanBeDamaged = TRUE
						
						NET_PRINT("[spawning] UPDATE_SPAWN_ACTIVITIES_AFTER_JOINING - finishing spawn activity driving personal vehicle") NET_NL()
						bGivenTask = FALSE
						CLEAR_PED_TASKS(PLAYER_PED_ID())
						RETURN(TRUE)
					ELSE
//						IF (g_TransitionSpawnData.iEntryWantedLevel > 0)
//							IF (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0)		
//								TASK_VEHICLE_DRIVE_WANDER(PLAYER_PED_ID(), GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), 20.0, DRIVINGMODE_STOPFORCARS )
//							ENDIF
//						ENDIF
						NET_PRINT("[spawning] UPDATE_SPAWN_ACTIVITIES_AFTER_JOINING - waiting for player to move") NET_NL()
					ENDIF
				BREAK
				CASE SPAWN_ACTIVITY_WALKING
					IF HAS_PLAYER_TRIED_TO_MOVE()
					OR NOT IS_PLAYER_PERFORMING_TASK(SCRIPT_TASK_WANDER_STANDARD)
					OR HAS_SPAWN_ACTIVITY_TIMER_EXPIRED()
						CLEAR_PED_TASKS(PLAYER_PED_ID())
						bGivenTask = FALSE
						RETURN(TRUE)
					ELSE
					
						IF (g_TransitionSpawnData.iEntryWantedLevel > 0)
							IF (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0)	
								SET_NEXT_DESIRED_MOVE_STATE( PEDMOVE_WALK )
								TASK_WANDER_STANDARD(PLAYER_PED_ID())
							ENDIF
						ENDIF	
					ENDIF
				BREAK
				
				#IF FEATURE_FIXER
				CASE SPAWN_ACTIVITY_SITTING_SMOKING
				
					RETURN UpdateSittingSmoking()
				
				BREAK
				CASE SPAWN_ACTIVITY_WAKE_UP_ON_FLOOR_SMOKING
				
					RETURN UpdateWakeUpOnFloor()
				
				BREAK
				#ENDIF 
				
				CASE SPAWN_ACTIVITY_DRUNK_AWAKEN
					#IF FEATURE_HEIST_ISLAND
					IF IS_BIT_SET(g_SimpleInteriorData.iEighthBS, BS8_SIMPLE_INTERIOR_SPAWNING_OFF_ISLAND)
						SET_DISABLE_DECAL_RENDERING_THIS_FRAME()
						HIDE_HUD_AND_RADAR_THIS_FRAME()
						HIDE_HELP_TEXT_THIS_FRAME()
						THEFEED_HIDE_THIS_FRAME()
						HIDE_SCRIPTED_HUD_COMPONENT_THIS_FRAME(HUD_MP_RANK_BAR)
						
						DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE)
						DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
						
						IF IS_ANY_BIG_MESSAGE_BEING_DISPLAYED()
							CLEAR_ALL_BIG_MESSAGES()
						ENDIF
						
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
						SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
						
						IF NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(iSyncSceneID) = -1
						OR GET_SYNCHRONIZED_SCENE_PHASE(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(iSyncSceneID)) >= 0.99
							CLEAR_PED_TASKS(PLAYER_PED_ID())
							
							RENDER_SCRIPT_CAMS(FALSE, TRUE)
							
							REMOVE_ANIM_DICT(GET_BEACH_SCENE_ANIM_DICT())
							
							bGivenTask = FALSE
							
							Quit_Drunk_Camera_Gradually(10000)
							
							IF g_SpawnData.bSkipClearingSpawn
								CLEAR_SPECIFIC_SPAWN_LOCATION()
								
								g_SpawnData.bSkipClearingSpawn = FALSE
							ENDIF
							
							RETURN(TRUE)
						ENDIF
					ELSE
					#ENDIF
						REQUEST_ANIM_DICT("GET_UP@DIRECTIONAL@TRANSITION@PRONE_TO_SEATED@INJURED")
						REQUEST_ANIM_DICT("get_up@directional@movement@from_seated@standard")
						IF HAS_ANIM_DICT_LOADED("GET_UP@DIRECTIONAL@TRANSITION@PRONE_TO_SEATED@INJURED")
						AND IS_ENTITY_ALIVE(PLAYER_PED_ID())
							IF NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "GET_UP@DIRECTIONAL@TRANSITION@PRONE_TO_SEATED@INJURED", "front")
								IF NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "get_up@directional@movement@from_seated@standard", "get_up_l_0")
									TASK_PLAY_ANIM(PLAYER_PED_ID(), "GET_UP@DIRECTIONAL@TRANSITION@PRONE_TO_SEATED@INJURED", "front", DEFAULT, DEFAULT, DEFAULT, AF_HOLD_LAST_FRAME)
								ELIF GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), "get_up@directional@movement@from_seated@standard", "get_up_l_0") > 0.85
									
									NET_PRINT("[spawning] UpdateSpawnActivity - Finished SPAWN_ACTIVITY_DRUNK_AWAKEN")  NET_NL()
									CLEAR_PED_TASKS(PLAYER_PED_ID())
									REMOVE_ANIM_DICT("GET_UP@DIRECTIONAL@TRANSITION@PRONE_TO_SEATED@INJURED")
									REMOVE_ANIM_DICT("get_up@directional@movement@from_seated@standard")
									bGivenTask = FALSE
									
									IF g_TransitionSpawnData.bFirstPersonCam
										SET_FOLLOW_PED_CAM_VIEW_MODE(CAM_VIEW_MODE_FIRST_PERSON)
										
										g_TransitionSpawnData.bFirstPersonCam = FALSE
									ENDIF
									
									Quit_Drunk_Camera_Gradually(10000)
									
									IF g_SpawnData.bSkipClearingSpawn
										CLEAR_SPECIFIC_SPAWN_LOCATION()
										
										g_SpawnData.bSkipClearingSpawn = FALSE
									ENDIF
									
									RETURN(TRUE)
								ENDIF
							ELIF GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), "GET_UP@DIRECTIONAL@TRANSITION@PRONE_TO_SEATED@INJURED", "front") > 0.85
							AND HAS_ANIM_DICT_LOADED("get_up@directional@movement@from_seated@standard")
							AND NOT IS_SCREEN_FADED_OUT()
								TASK_PLAY_ANIM(PLAYER_PED_ID(),"get_up@directional@movement@from_seated@standard", "get_up_l_0", DEFAULT, DEFAULT, DEFAULT, AF_HOLD_LAST_FRAME)
							ENDIF
						ENDIF
					#IF FEATURE_HEIST_ISLAND
					ENDIF
					#ENDIF
				BREAK
				CASE SPAWN_ACTIVITY_WATCHING_TV
					RETURN(TRUE)
				BREAK
				CASE SPAWN_ACTIVITY_SHOWER
				CASE SPAWN_ACTIVITY_BED
					IF (g_TransitionSpawnData.bSpawnActivtyObjectScriptReady)
						RETURN(TRUE)
					ENDIF
				BREAK
				DEFAULT
					RETURN(TRUE)
				BREAK
			ENDSWITCH

			
		ENDIF

	ENDIF
		
	RETURN(FALSE)

ENDFUNC



PROC PRE_SCRIPT_CLEANUP()
	MPGlobalsInteractions.PlayerInteraction.bHoldLoop = FALSE

	RESET_SPAWNING_INTERACTION_GLOBALS()

	IF DOES_CAM_EXIST(mCam)
		DESTROY_CAM(mCam)
	ENDIF
	IF DOES_CAM_EXIST(mCam2)
		DESTROY_CAM(mCam2)
	ENDIF
	
	IF (bGivenTask)
		IF IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, TRUE)
		AND IS_ENTITY_ALIVE(PLAYER_PED_ID())
			NET_PRINT("[spawning] SCRIPT_CLEANUP - bGivenTask = TRUE ") NET_NL()
			CLEAR_PED_TASKS(PLAYER_PED_ID())		
		ENDIF
	ENDIF
ENDPROC

PROC SCRIPT_CLEANUP()

	PRE_SCRIPT_CLEANUP()
	
	CLEAR_BIT(g_SimpleInteriorData.iEighthBS, BS8_SIMPLE_INTERIOR_SPAWNING_OFF_ISLAND)
	
	NET_PRINT("[spawning] SCRIPT_CLEANUP called...") NET_NL()
	DEBUG_PRINTCALLSTACK()
	TERMINATE_THIS_THREAD()
ENDPROC

// **************************************************************************************************************************************************************
// **************************************************************************************************************************************************************


SCRIPT

	
	NET_PRINT("[spawning] Spawn_Activities script started...") NET_NL()

	NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()

	#IF IS_DEBUG_BUILD
	START_WIDGET_GROUP("spawn_activities")
		ADD_WIDGET_INT_SLIDER("iSpawnActivitesStage", iSpawnActivitesStage, -1, 99, 1)
		ADD_WIDGET_BOOL("bBrainsEnabled", bBrainsEnabled)
		
	STOP_WIDGET_GROUP()
	#ENDIF
	
	SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)

	WHILE TRUE
	
		WAIT(0)
		
		IF HAS_SCRIPT_TIMEDOUT_TRANSITION()
			SCRIPT_CLEANUP()
		ENDIF

		// If we have a match end event, bail.
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			SCRIPT_CLEANUP()
		ENDIF			
		
		SWITCH iSpawnActivitesStage
			CASE SA_LOADING
			
				IF (g_TransitionSpawnData.bNightClubSpawn)
				AND (g_iNightClubAudioCurrentTime = -1)
					
					NET_PRINT("[spawning] spawn_activities waiting for nightclub audio to be setup") NET_NL()
				
				ELSE
			
					IF NOT (g_TransitionSpawnData.bSpawnActivityReady)
						UpdateSpawnActivity()
					ENDIF
					IF NOT (g_TransitionSpawnData.bSpawnInteractionReady)
						UpdateSpawnInteractionAnim()
					ENDIF
					
					// move on
					IF IS_SPAWN_ACTIVITY_READY()	
						iSpawnActivitesStage = SA_UPDATE
						NET_PRINT("[spawning] going to SA_UPDATE") NET_NL()
					ELSE
						NET_PRINT("[spawning] Spawn_Activities waiting for IS_SPAWN_ACTIVITY_READY") NET_NL()
					ENDIF
				ENDIF
			BREAK
			
			CASE SA_UPDATE
			
				IF UPDATE_SPAWN_ACTIVITIES_AFTER_JOINING()
					#IF FEATURE_SHORTEN_SKY_HANG
					IF (g_bSkySwoopDownDidFadeOut)
						SpawnTimer = GET_NETWORK_TIME()
						PRE_SCRIPT_CLEANUP()
						iSpawnActivitesStage = SA_TERMINATE
						PRINTLN("[HangingInSky]  going to SA_TERMINATE")
					ELSE
					#ENDIF
						SCRIPT_CLEANUP()
					#IF FEATURE_SHORTEN_SKY_HANG
					ENDIF
					#ENDIF
				ENDIF
			
			BREAK
			
			
			CASE SA_TERMINATE
				#IF FEATURE_SHORTEN_SKY_HANG
				IF ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), SpawnTimer)) > 2000
				
					// if hanging in the sky did a fade out.
					IF (g_bSkySwoopDownDidFadeOut)
						IF IS_SCREEN_FADED_OUT()
						AND NOT IS_SCREEN_FADING_IN()
							PRINTLN("[HangingInSky] SPAWN_ACTIVITIES - hanging in sky did fade out, fading back in.")
							DO_SCREEN_FADE_IN(500)
						ENDIF
						g_bSkySwoopDownDidFadeOut = FALSE
					ENDIF					
					
					CLEAR_BIT(g_SimpleInteriorData.iEighthBS, BS8_SIMPLE_INTERIOR_SPAWNING_OFF_ISLAND)
					
					PRINTLN("[HangingInSky] - SPAWN_ACTIVITIES terminating from SA_TERMINATE") 
					DEBUG_PRINTCALLSTACK()
					TERMINATE_THIS_THREAD()
				ELSE
					PRINTLN("[HangingInSky] - SPAWN_ACTIVITIES waiting to terminate.")	
				ENDIF
				#ENDIF
			BREAK
			
			
			
		ENDSWITCH	
		
	ENDWHILE

ENDSCRIPT
