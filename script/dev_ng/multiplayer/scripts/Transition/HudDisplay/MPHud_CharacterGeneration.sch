/* ------------------------------------------------------------------
* Name: 		MPHud_CharacterGeneration.sch
* Author: 		James Adwick
* Date: 		02/09/2014
* Purpose: 		Handles all blends / creation of peds and application
*				of overlays, features to player peds
* ------------------------------------------------------------------*/

//--- Includes

USING "MPHud_CharacterAccessors.sch"
USING "MPHud_CharacterControllerCamera.sch"
USING "net_team_info.sch"
USING "net_gang_boss.sch"

//--- Constants

// Values to change the feature grid by
TWEAK_FLOAT cfCHARACTER_CREATOR_FEATURE_SLIDE_X			10.0
TWEAK_FLOAT cfCHARACTER_CREATOR_FEATURE_SLIDE_Y			10.0

TWEAK_FLOAT cfCHARACTER_CREATOR_APPEARANCE_SLIDE_X			2.0

/// PURPOSE:
///    Basic ok check for peds
FUNC BOOL IS_CHARACTER_CONTROLLER_PED_OK(PED_INDEX &aPed)
	
	IF DOES_ENTITY_EXIST(aPed)
	AND NOT IS_ENTITY_DEAD(aPed)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Attach a board to the player ped
PROC ATTACH_CHARACTER_CREATOR_BOARD_TO_PED(PED_INDEX &aPed, MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, INT iIndex)

	IF iIndex >= 0
	AND iIndex < ciCHARACTER_SELECTOR_TOTAL_BOARDS
		
		IF IS_CHARACTER_CONTROLLER_PED_OK(aPed)
		AND DOES_ENTITY_EXIST(characterData.sPedBoard[iIndex].objBoard)
			CPRINTLN(DEBUG_NET_CHARACTER, "ATTACH_CHARACTER_CREATOR_BOARD_TO_PED - Attach board to ped: ", iIndex)

			FREEZE_ENTITY_POSITION(characterData.sPedBoard[iIndex].objBoard, FALSE)
			SET_ENTITY_VISIBLE(characterData.sPedBoard[iIndex].objBoard, TRUE)
			ATTACH_ENTITY_TO_ENTITY(characterData.sPedBoard[iIndex].objBoard, aPed, GET_PED_BONE_INDEX(aPed, BONETAG_PH_R_HAND), <<0, 0, 0>>, <<0, 0, 0>>)
			FREEZE_ENTITY_POSITION(characterData.sPedBoard[iIndex].objRenderBoard, FALSE)
			SET_ENTITY_VISIBLE(characterData.sPedBoard[iIndex].objRenderBoard, TRUE)
			ATTACH_ENTITY_TO_ENTITY(characterData.sPedBoard[iIndex].objRenderBoard, aPed, GET_PED_BONE_INDEX(aPed, BONETAG_PH_R_HAND), <<0, 0, 0>>, <<0, 0, 0>>)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Hide the selector board from view
PROC HIDE_CHARACTER_CREATOR_SELECTOR_BOARD(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, INT iSlot)

	CPRINTLN(DEBUG_NET_CHARACTER, "HIDE_CHARACTER_CREATOR_SELECTOR_BOARD - Hiding board: ", iSlot)

	DETACH_ENTITY(characterData.sPedBoard[iSlot].objBoard, FALSE)
	DETACH_ENTITY(characterData.sPedBoard[iSlot].objRenderBoard, FALSE)

	FREEZE_ENTITY_POSITION(characterData.sPedBoard[iSlot].objBoard, TRUE)
	SET_ENTITY_VISIBLE(characterData.sPedBoard[iSlot].objBoard, FALSE)
	FREEZE_ENTITY_POSITION(characterData.sPedBoard[iSlot].objRenderBoard, TRUE)
	SET_ENTITY_VISIBLE(characterData.sPedBoard[iSlot].objRenderBoard, FALSE)
ENDPROC

/// PURPOSE:
///    Returns a 10 digit string displayig the players RP
FUNC TEXT_LABEL_15 GET_CHARACTER_CREATOR_RP_VALUE(INT iSlot, BOOL bNewChar = FALSE)

	TEXT_LABEL_15 tlNumber
	
	// For new characters we just display the slot number with the 9 0's before
	IF bNewChar
		tlNumber = "000000000"
		tlNumber += (iSlot+1)
	ELSE
		INT iRP = GET_STAT_CHARACTER_XP(iSlot)
		TEXT_LABEL_15 tlTemp = ""
		tlTemp += iRP
		
		INT iLength = GET_LENGTH_OF_LITERAL_STRING(tlTemp)
		
		INT i
		FOR i = 0 TO (10 - iLength)-1 STEP 1
			
			tlNumber += "0"
		
		ENDFOR		
		
		tlNumber += iRP
	ENDIF

	RETURN tlNumber
ENDFUNC

/// PURPOSE:
///    Called immediately to see which slot our new character will be in
PROC INITIALISE_CHARACTER_CONTROLLER_PED_SLOTS(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, MPHUD_PLACEMENT_TOOLS& Placement)

	INT i
	BOOL bisSlotActive
	
	// Update our slots
	characterData.iActiveSlotOnSelector = 0
	
	REPEAT MAX_NUM_CHARACTER_SLOTS i
		bisSlotActive = IS_STAT_CHARACTER_ACTIVE(i)
		IF bisSlotActive
			SET_BIT(characterData.iActiveSlotOnSelector, i)						
		ELSE
			IF GET_CHARACTER_SELECTOR_SLOT_FOR_CREATED_PED(characterData) = -1
				SET_CHARACTER_SELECTOR_SLOT_FOR_CREATED_PED(characterData, i)
				
				// Save out the slot that the ped is standing in
				characterData.iCharacterSelectedSlot = i
			ENDIF
		ENDIF
	ENDREPEAT
	
	// If we are entering from an alteration prompt - update accordingly so we display correct info and stats
	IF HAS_ENTERED_CHAR_CREATION_FROM_ALTERATION_PROMPT()
		CPRINTLN(DEBUG_NET_CHARACTER, "INITIALISE_CHARACTER_CONTROLLER_PED_SLOTS - Entering to edit. Update our slot to the selected character: ", Placement.ScreenPlace.iSelectedCharacter)
		SET_CHARACTER_SELECTOR_SLOT_FOR_CREATED_PED(characterData, Placement.ScreenPlace.iSelectedCharacter)
	ENDIF
	
	#IF FEATURE_GEN9_STANDALONE
	IF GET_CHARACTER_SELECTOR_SLOT_FOR_CREATED_PED(characterData) = -1
	AND GET_CHOSEN_MP_CHARACTER_SLOT() >= 0
		CPRINTLN(DEBUG_NET_CHARACTER, "INITIALISE_CHARACTER_CONTROLLER_PED_SLOTS - Entering from migration. Update our slot to the selected character: ", GET_CHOSEN_MP_CHARACTER_SLOT())
		SET_CHARACTER_SELECTOR_SLOT_FOR_CREATED_PED(characterData, GET_CHOSEN_MP_CHARACTER_SLOT())
	ENDIF
	#ENDIF
	
	CPRINTLN(DEBUG_NET_CHARACTER, "INITIALISE_CHARACTER_CONTROLLER_PED_SLOTS - The new created ped is in slot: ", GET_CHARACTER_SELECTOR_SLOT_FOR_CREATED_PED(characterData))
ENDPROC


/// PURPOSE:
///    Update the scaleform boards with the correct text
PROC UPDATE_CHARACTER_SELECTOR_BOARDS(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, MPHUD_PLACEMENT_TOOLS& Placement)
	
	INT i
	STRING strName
	TEXT_LABEL_15 tlNumber = "FACE_B_NO"
	TEXT_LABEL_15 importedFrom = ""
	HUD_COLOURS hcColour 
	BOOL bActiveSlot
	BOOL bLiteralName
	BOOL bShowRP
	INT iRank
	
	CPRINTLN(DEBUG_NET_CHARACTER, "UPDATE_CHARACTER_SELECTOR_BOARDS - Created character is in slot: ", GET_CHARACTER_SELECTOR_SLOT_FOR_CREATED_PED(characterData))
	
	REPEAT ciCHARACTER_SELECTOR_TOTAL_BOARDS i
	
		bActiveSlot = FALSE
		bLiteralName = TRUE
		bShowRP = TRUE
		importedFrom = ""
		hcColour = HUD_COLOUR_FREEMODE
		strName = GET_MP_STRING_CHARACTER_STAT(MP_STAT_CHAR_NAME, i)
	
		// If this slot is our new character
		IF i = GET_CHARACTER_SELECTOR_SLOT_FOR_CREATED_PED(characterData)
		AND NOT HAS_ENTERED_CHAR_CREATION_FROM_ALTERATION_PROMPT()
			
			hcColour = HUD_COLOUR_GREENLIGHT
			bActiveSlot = TRUE
					
			IF IS_STRING_NULL_OR_EMPTY(strName)
				strName = "FACE_N_CHAR"
				bLiteralName = FALSE
			ENDIF
			
			// Default rank and RP
			iRank = 1
			tlNumber = GET_CHARACTER_CREATOR_RP_VALUE(i, TRUE)
			
			IF Placement.bSelectedRPBoost
				tlNumber = GET_CHARACTER_CREATOR_RP_VALUE(i)
				iRank = GET_FM_RANK_FROM_XP_VALUE(GET_STAT_CHARACTER_XP(i), TRUE)
			ENDIF
			
		ELSE
			IF IS_STAT_CHARACTER_ACTIVE(i)
				
				bActiveSlot = TRUE
				
				IF (GET_MP_INT_PLAYER_STAT(MPPLY_DID_SAVE_TRANS) = 1 
				OR GET_MP_BOOL_CHARACTER_STAT(MP_STAT_WAS_CHAR_TRANSFERED,0)
				OR Placement.iWasCharacterTransferred[0])
				AND i = 0
					ImportedFrom = "HUD_IMPORXB"
					hcColour = HUD_COLOUR_GREENLIGHT
					NET_NL()NET_PRINT("[SAVETRANS]	- SETTING HUD_IMPORXB as MPPLY_DID_SAVE_TRANS = 1 & iCurrentChar = 0 ")
				ELIF (GET_MP_INT_PLAYER_STAT(MPPLY_DID_SAVE_TRANS) = 2
				OR GET_MP_BOOL_CHARACTER_STAT(MP_STAT_WAS_CHAR_TRANSFERED,1)
				OR Placement.iWasCharacterTransferred[1])
				AND i = 1
					ImportedFrom = "HUD_IMPORPS"
					hcColour = HUD_COLOUR_GREENLIGHT
					NET_NL()NET_PRINT("[SAVETRANS]	- SETTING HUD_IMPORPS as MPPLY_DID_SAVE_TRANS = 2 & iCurrentChar = 1 ")
				ELIF GET_MP_INT_PLAYER_STAT(MPPLY_DID_SAVE_TRANS) = 3
					ImportedFrom = "HUD_IMPORPS"
					hcColour = HUD_COLOUR_GREENLIGHT
					NET_NL()NET_PRINT("[SAVETRANS]	- SETTING HUD_IMPORPS as MPPLY_DID_SAVE_TRANS = 3 ")
				ENDIF
				
				tlNumber = GET_CHARACTER_CREATOR_RP_VALUE(i)
				iRank = GET_FM_RANK_FROM_XP_VALUE(GET_STAT_CHARACTER_XP(i), TRUE)
			ENDIF
			
		ENDIF
		
		// Display ERROR if this slot failed and needs to be reloaded
		IF HAS_SLOT_BEEN_IGNORED_CODE(i+1)
			tlNumber = "HUD_ERROR"
			bShowRP = FALSE
		ENDIF
	
		IF bActiveSlot
			SET_MUGSHOT_BOARD(characterData.sPedBoard[i].scaleformIndex, strName, tlNumber, "FACE_B_POL", ImportedFrom, hcColour, iRank, bLiteralName, bShowRP)
		ELSE
			HIDE_CHARACTER_CREATOR_SELECTOR_BOARD(characterData, i)
		ENDIF
	ENDREPEAT
	
ENDPROC

/// PURPOSE:
///    Sets the correct ped to visible
PROC SET_CHARACTER_CREATOR_PED_VISIBLE(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData)
	
	SET_ENTITY_VISIBLE(characterData.characterCreatorPeds[CHARACTER_CREATOR_SON], FALSE)
	SET_ENTITY_VISIBLE(characterData.characterCreatorPeds[CHARACTER_CREATOR_DAUGHTER], FALSE)
	
	SET_ENTITY_VISIBLE(characterData.characterCreatorPeds[characterData.eActiveChild], TRUE)
ENDPROC

/// PURPOSE:
///    Sets ped position for the creator
PROC SET_CHARACTER_CREATOR_PED_POSITION(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData)

	VECTOR vCoords = GET_CHARACTER_CREATOR_ANIM_VECTOR()
	CPRINTLN(DEBUG_NET_CHARACTER, "SET_CHARACTER_CREATOR_PED_POSITION - Setting the position of peds: ", vCoords)
	
	IF IS_CHARACTER_CONTROLLER_PED_OK(characterData.characterCreatorPeds[CHARACTER_CREATOR_SON])
		CLEAR_PED_TASKS_IMMEDIATELY(characterData.characterCreatorPeds[CHARACTER_CREATOR_SON])
		
		FREEZE_ENTITY_POSITION(characterData.characterCreatorPeds[CHARACTER_CREATOR_SON], TRUE)
		SET_ENTITY_COLLISION(characterData.characterCreatorPeds[CHARACTER_CREATOR_SON], FALSE)
		SET_ENTITY_COORDS(characterData.characterCreatorPeds[CHARACTER_CREATOR_SON], vCoords)
		SET_ENTITY_HEADING(characterData.characterCreatorPeds[CHARACTER_CREATOR_SON], GET_CHARACTER_CREATOR_PED_HEADING())
		FORCE_PED_AI_AND_ANIMATION_UPDATE(characterData.characterCreatorPeds[CHARACTER_CREATOR_SON], TRUE)
	ENDIF
	
	IF IS_CHARACTER_CONTROLLER_PED_OK(characterData.characterCreatorPeds[CHARACTER_CREATOR_DAUGHTER])
		CLEAR_PED_TASKS_IMMEDIATELY(characterData.characterCreatorPeds[CHARACTER_CREATOR_DAUGHTER])
			
		FREEZE_ENTITY_POSITION(characterData.characterCreatorPeds[CHARACTER_CREATOR_DAUGHTER], TRUE)
		SET_ENTITY_COLLISION(characterData.characterCreatorPeds[CHARACTER_CREATOR_DAUGHTER], FALSE)
		SET_ENTITY_COORDS(characterData.characterCreatorPeds[CHARACTER_CREATOR_DAUGHTER], vCoords)
		SET_ENTITY_HEADING(characterData.characterCreatorPeds[CHARACTER_CREATOR_DAUGHTER], GET_CHARACTER_CREATOR_PED_HEADING())
		FORCE_PED_AI_AND_ANIMATION_UPDATE(characterData.characterCreatorPeds[CHARACTER_CREATOR_DAUGHTER], TRUE)
	ENDIF
	
	SET_CHARACTER_CREATOR_PED_VISIBLE(characterData)
ENDPROC

//--- Blend / Appearance functions

/// PURPOSE:
///    Applies the chosen eye colour on the ped
PROC APPLY_CHARACTER_CREATOR_EYE_COLOUR(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData)

	CHARACTER_CREATOR_APPEARANCE_VALUES appearanceValues
	GET_CHARACTER_CREATOR_APPEARANCE_VALUES(characterData, appearanceValues, ciCHARACTER_CREATOR_APPEARANCE_EYES)
	
	CPRINTLN(DEBUG_NET_CHARACTER, "APPLY_CHARACTER_CREATOR_EYE_COLOUR - Apply eye colour: ", appearanceValues.iValue)
	
	IF IS_CHARACTER_CONTROLLER_PED_OK(characterData.characterCreatorPeds[CHARACTER_CREATOR_SON])
		CPRINTLN(DEBUG_NET_CHARACTER, "APPLY_CHARACTER_CREATOR_EYE_COLOUR - Apply eye colour (son): ", appearanceValues.iValue)
		SET_HEAD_BLEND_EYE_COLOR(characterData.characterCreatorPeds[CHARACTER_CREATOR_SON], appearanceValues.iValue)
	ENDIF
	
	IF IS_CHARACTER_CONTROLLER_PED_OK(characterData.characterCreatorPeds[CHARACTER_CREATOR_DAUGHTER])
		CPRINTLN(DEBUG_NET_CHARACTER, "APPLY_CHARACTER_CREATOR_EYE_COLOUR - Apply eye colour (daughter): ", appearanceValues.iValue)
		SET_HEAD_BLEND_EYE_COLOR(characterData.characterCreatorPeds[CHARACTER_CREATOR_DAUGHTER], appearanceValues.iValue)
	ENDIF
ENDPROC

/// PURPOSE:
///    Find the accessory colour for the character creator hairstyle
FUNC INT GET_CHARACTER_CREATOR_ACCESSORY_COLOUR()
	
	BOOL bFound
	INT iInitialValue = GET_RANDOM_INT_IN_RANGE(0, GET_NUM_PED_HAIR_TINTS())
	INT iRandomColour = iInitialValue

	WHILE NOT bFound
		
		iRandomColour++
		
		IF iRandomColour >= GET_NUM_PED_HAIR_TINTS()
			iRandomColour = 0
		ENDIF
		
		IF IS_PED_ACCS_TINT_FOR_CREATOR(iRandomColour)
		
			CPRINTLN(DEBUG_NET_CHARACTER, "GET_CHARACTER_CREATOR_ACCESSORY_COLOUR - Found accessory colour = ", iRandomColour)
			bFound = TRUE
		ELSE
			IF iRandomColour = iInitialValue
			
				CPRINTLN(DEBUG_NET_CHARACTER, "GET_CHARACTER_CREATOR_ACCESSORY_COLOUR - Failed to find accessory colour as looped fully round")
			
				iRandomColour = 0
				bFound = TRUE
			ENDIF
		ENDIF
	ENDWHILE
	
	RETURN iRandomColour
ENDFUNC

/// PURPOSE:
///    Checks to see if the hair requires a colour for the accessory or contrast the selected colour
PROC UPDATE_CHARACTER_CREATOR_SECONDARY_HAIR_COLOUR(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, CHARACTER_CREATOR_PED aPedType)
	
	INT iHairRow = ciCHARACTER_CREATOR_APPEARANCE_HAIR_STYLE_F
	IF aPedType = CHARACTER_CREATOR_SON
		iHairRow = ciCHARACTER_CREATOR_APPEARANCE_HAIR_STYLE_M
	ENDIF
	
	CHARACTER_CREATOR_APPEARANCE_VALUES appearanceValues
	GET_CHARACTER_CREATOR_APPEARANCE_VALUES(characterData, appearanceValues, iHairRow)

	INT iSecondaryColour
	IF GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_IS_NG_VERSION, characterData.iCharacterSelectedSlot) < ciCHARACTER_CREATOR_CURRENT_VERSION
	OR appearanceValues.iValue != ciCHARACTER_CREATOR_EXISTING_INDEX

		iSecondaryColour = GET_DEFAULT_SECONDARY_TINT_FOR_CREATOR(appearanceValues.iColour)

		CPRINTLN(DEBUG_NET_CHARACTER, "UPDATE_CHARACTER_CREATOR_SECONDARY_HAIR_COLOUR - Primary Colour: ", appearanceValues.iColour, " with Secondary Colour: ", iSecondaryColour, ", iValue = ", appearanceValues.iValue)

		SET_CHARACTER_CREATOR_APPEARANCE_VALUES(characterData, iHairRow, appearanceValues.iValue, appearanceValues.fOpacity, appearanceValues.iColour, iSecondaryColour, appearanceValues.iColourHighlightValue)
	ELSE
		CPRINTLN(DEBUG_NET_CHARACTER, "UPDATE_CHARACTER_CREATOR_SECONDARY_HAIR_COLOUR - appearanceValues.iValue = ciCHARACTER_CREATOR_EXISTING_INDEX")
	
		IF GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_IS_NG_VERSION, characterData.iCharacterSelectedSlot) >= ciCHARACTER_CREATOR_CURRENT_VERSION
			iSecondaryColour = GET_MP_INT_CHARACTER_STAT(MP_STAT_SEC_HAIR_TINT, characterData.iCharacterSelectedSlot)
			SET_CHARACTER_CREATOR_APPEARANCE_VALUES(characterData, iHairRow, appearanceValues.iValue, appearanceValues.fOpacity, appearanceValues.iColour, iSecondaryColour, appearanceValues.iColourHighlightValue)
		
			CPRINTLN(DEBUG_NET_CHARACTER, "UPDATE_CHARACTER_CREATOR_SECONDARY_HAIR_COLOUR - appearanceValues.iValue = ciCHARACTER_CREATOR_EXISTING_INDEX iSecondaryColour = ", iSecondaryColour)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Applies the correct hair and colour on the peds
PROC APPLY_CHARACTER_CREATOR_HAIR(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, CHARACTER_CREATOR_PED aPedType, INT iSlot = -1)

	INT iPedSlot
	
	IF iSlot = -1
		iPedSlot = characterData.iCharacterSelectedSlot
	ELSE
		iPedSlot = iSlot
	ENDIF

	INT iHairRow = ciCHARACTER_CREATOR_APPEARANCE_HAIR_STYLE_F
	IF aPedType = CHARACTER_CREATOR_SON
		iHairRow = ciCHARACTER_CREATOR_APPEARANCE_HAIR_STYLE_M
	ENDIF
	
	CHARACTER_CREATOR_APPEARANCE_VALUES appearanceValues
	GET_CHARACTER_CREATOR_APPEARANCE_VALUES(characterData, appearanceValues, iHairRow)
	
	PED_COMP_NAME_ENUM theHair = GET_CHARACTER_CREATOR_HAIR_ENUM(characterData, aPedType)
	
	CPRINTLN(DEBUG_NET_CHARACTER, "APPLY_CHARACTER_CREATOR_HAIR - Applying hair to ", PICK_STRING(aPedType = CHARACTER_CREATOR_SON, "son", "daughter"), " for hair: ", appearanceValues.iValue, " Enum: ", ENUM_TO_INT(theHair), " with colours: ", appearanceValues.iColour, ", colour2: ", appearanceValues.iColour2, " slot: ", iPedSlot)
	
	// Make sure the next few commands are wrapped with the global character slot change.
	INT iCurrentSlot = g_iPedComponentSlot
	SET_CURRENT_MP_SLOT_FOR_CLOTHES(iPedSlot)
	
	SET_PED_COMP_ITEM_CURRENT_MP(characterData.characterCreatorPeds[aPedType], COMP_TYPE_HAIR, theHair, FALSE, DEFAULT, DEFAULT, DEFAULT, TRUE, appearanceValues.iColour, appearanceValues.iColour2, iSlot)
	
	// equip correct hair overlay
	TATTOO_NAME_ENUM eHairOverlay = INVALID_TATTOO
	IF aPedType = CHARACTER_CREATOR_SON
		eHairOverlay = GET_HAIR_OVERLAY(MP_M_FREEMODE_01, theHair)
	ELSE
		eHairOverlay = GET_HAIR_OVERLAY(MP_F_FREEMODE_01, theHair)
	ENDIF
	IF eHairOverlay <> INVALID_TATTOO
		// always remove previous hair overlays
		REMOVE_TATTOOS_IN_UPGRADE_GROUP_MP(GET_HASH_KEY("hairOverlay"), characterData.characterCreatorPeds[aPedType])
	//	
	//	SET_MP_TATTOO_CURRENT(eHairOverlay, TRUE, iSlot)
		UPDATE_TATOOS_MP(characterData.characterCreatorPeds[aPedType])
		
		GIVE_PED_TEMP_TATTOO_MP(characterData.characterCreatorPeds[aPedType], eHairOverlay, TRUE)
		CPRINTLN(DEBUG_NET_CHARACTER, "APPLY_CHARACTER_CREATOR_HAIR - Equipping eHairOverlay ", eHairOverlay, " for item ", theHair)
	ELSE
		CWARNINGLN(DEBUG_NET_CHARACTER, "APPLY_CHARACTER_CREATOR_HAIR - NO eHairOverlay ", eHairOverlay, " for item ", theHair)
	ENDIF
	
	// Restore our global slot to keep in sync
	SET_CURRENT_MP_SLOT_FOR_CLOTHES(iCurrentSlot)
	
	// Apply the colour tint
	//SET_PED_HAIR_TINT(characterData.characterCreatorPeds[aPedType], appearanceValues.iColour, appearanceValues.iColour2)
ENDPROC

/// PURPOSE:
///    Apply the hat to the ped
PROC APPLY_CHARACTER_CREATOR_HAT(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData)

	INT iHat = GET_CHARACTER_CREATOR_CHILD_HAT(characterData, characterData.eActiveChild)
	
	// Exit early if the ped is not safe to act on
	IF NOT IS_CHARACTER_CONTROLLER_PED_OK(characterData.characterCreatorPeds[characterData.eActiveChild])
		EXIT
	ENDIF
	
	IF iHat >= 0
		PED_COMP_NAME_ENUM theHat = GET_CHARACTER_CREATOR_HAT_ENUM(characterData.characterCreatorPeds[characterData.eActiveChild], iHat)
		
		CPRINTLN(DEBUG_NET_CHARACTER, "APPLY_CHARACTER_CREATOR_HAT - Applying hat: ", ENUM_TO_INT(theHat))
		
		SET_PED_COMP_ITEM_CURRENT_MP(characterData.characterCreatorPeds[characterData.eActiveChild], COMP_TYPE_PROPS, theHat, FALSE)
	ELIF iHat = ciCHARACTER_CREATOR_EXISTING_OVERLAY_INDEX
		
		PED_VARIATION_STRUCT sClothes
		GET_STORED_MP_PLAYER_COMPONENTS(sClothes, characterData.iCharacterSelectedSlot) 
		
		IF sClothes.iPropIndex[ANCHOR_HEAD] != -1
		AND sClothes.iPropIndex[ANCHOR_HEAD] != 255
			SET_PED_PROP_INDEX(characterData.characterCreatorPeds[characterData.eActiveChild], ANCHOR_HEAD, sClothes.iPropIndex[ANCHOR_HEAD], sClothes.iPropTexture[ANCHOR_HEAD])
		ELSE
			CLEAR_PED_PROP(characterData.characterCreatorPeds[characterData.eActiveChild], ANCHOR_HEAD)
			APPLY_CHARACTER_CREATOR_HAIR(characterData, characterData.eActiveChild)
		ENDIF
	ELSE
		CLEAR_PED_PROP(characterData.characterCreatorPeds[characterData.eActiveChild], ANCHOR_HEAD)
		
		IF NOT IS_CHARACTER_CONTROLLER_BIT_SET(ciCHARACTER_CONTROLLER_RANDOMISE_ALL)
			APPLY_CHARACTER_CREATOR_HAIR(characterData, characterData.eActiveChild)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Apply the glasses to the ped
PROC APPLY_CHARACTER_CREATOR_GLASSES(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData)
	
	INT iGlasses = GET_CHARACTER_CREATOR_CHILD_GLASSES(characterData, characterData.eActiveChild)

	// Exit early if the ped is not safe to act on
	IF NOT IS_CHARACTER_CONTROLLER_PED_OK(characterData.characterCreatorPeds[characterData.eActiveChild])
		EXIT
	ENDIF

	IF iGlasses >= 0
		PED_COMP_NAME_ENUM theGlasses = GET_CHARACTER_CREATOR_GLASSES_ENUM(characterData.characterCreatorPeds[characterData.eActiveChild], iGlasses)
		
		CPRINTLN(DEBUG_NET_CHARACTER, "APPLY_CHARACTER_CREATOR_HAT - Applying hat: ", ENUM_TO_INT(theGlasses))
		
		SET_PED_COMP_ITEM_CURRENT_MP(characterData.characterCreatorPeds[characterData.eActiveChild], COMP_TYPE_PROPS, theGlasses, FALSE)
	ELIF iGlasses = ciCHARACTER_CREATOR_EXISTING_OVERLAY_INDEX
		
		PED_VARIATION_STRUCT sClothes
		GET_STORED_MP_PLAYER_COMPONENTS(sClothes, characterData.iCharacterSelectedSlot) 
		
		IF sClothes.iPropIndex[ANCHOR_EYES] != -1
		AND sClothes.iPropIndex[ANCHOR_EYES] != 255		
			SET_PED_PROP_INDEX(characterData.characterCreatorPeds[characterData.eActiveChild], ANCHOR_EYES, sClothes.iPropIndex[ANCHOR_EYES], sClothes.iPropTexture[ANCHOR_EYES])
		ELSE	
			CLEAR_PED_PROP(characterData.characterCreatorPeds[characterData.eActiveChild], ANCHOR_EYES)
		ENDIF
	ELSE
		CLEAR_PED_PROP(characterData.characterCreatorPeds[characterData.eActiveChild], ANCHOR_EYES)
	ENDIF
ENDPROC


/// PURPOSE:
///    Update the neck feature blend for males only (remove the thin necks when looking like your mum!)
PROC UPDATE_CHARACTER_CREATOR_NECK(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData)
	
	CHARACTER_CREATOR_FEATURE_DATA featureData
	CHARACTER_CREATOR_FEATURE_VALUES featureValues
	GET_CHARACTER_CREATOR_FEATURE_DATA(featureData, ciCHARACTER_CREATOR_FEATURES_NECK_M)
	
	FLOAT fPercentage = 100 - GET_CHARACTER_CREATOR_CHILD_DOMINANCE(characterData, CHARACTER_CREATOR_SON)
	
	SET_CHARACTER_CREATOR_FEATURE_VALUES(characterData, ciCHARACTER_CREATOR_FEATURES_NECK_M, -1, fPercentage, fPercentage)
	GET_CHARACTER_CREATOR_FEATURE_VALUES(characterData, featureValues, ciCHARACTER_CREATOR_FEATURES_NECK_M)
	
	FLOAT fNormalisedBlend = GET_CHARACTER_CREATOR_NORMALISED_BLEND(featureValues.fXBlend, featureData.fXMax, featureData.fXMin)
	
	CPRINTLN(DEBUG_NET_CHARACTER, "UPDATE_CHARACTER_CREATOR_NECK - Apply neck micromorph to male: ", fNormalisedBlend)
	
	SET_PED_MICRO_MORPH(characterData.characterCreatorPeds[CHARACTER_CREATOR_SON], featureData.morphXAxis, fNormalisedBlend)
ENDPROC

/// PURPOSE:
///    Applies the head blend on the child from two heads of the parents
PROC APPLY_CHARACTER_CREATOR_HEAD_BLEND(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, CHARACTER_CREATOR_PED aPedType)
	FLOAT fDominance, fSkinToneBlend
	fDominance = GET_CHARACTER_CREATOR_PARENT_DOMINANCE(characterData, aPedType)
	fSkinToneBlend = GET_CHARACTER_CREATOR_SKIN_TONE_BLEND(characterData, aPedType)
	
	INT iHeadIndex1 = GET_CHARACTER_CREATOR_PARENT_HEAD_EXACT_VALUE(characterData, CHARACTER_CREATOR_MUM)
	INT iHeadIndex2 = GET_CHARACTER_CREATOR_PARENT_HEAD_EXACT_VALUE(characterData, CHARACTER_CREATOR_DAD)
	
	CPRINTLN(DEBUG_NET_CHARACTER, "APPLY_CHARACTER_CREATOR_HEAD_BLEND - For ped: ", PICK_STRING(aPedType = CHARACTER_CREATOR_SON, "Son", "Daughter"), " index: ", NATIVE_TO_INT(characterData.characterCreatorPeds[ENUM_TO_INT(aPedType)]))
	CPRINTLN(DEBUG_NET_CHARACTER, "APPLY_CHARACTER_CREATOR_HEAD_BLEND - Blend for Mum head: ", iHeadIndex1, " and Dad head: ", iHeadIndex2)
	CPRINTLN(DEBUG_NET_CHARACTER, "APPLY_CHARACTER_CREATOR_HEAD_BLEND - Blend (headblend (dom)): ", fDominance)
	CPRINTLN(DEBUG_NET_CHARACTER, "APPLY_CHARACTER_CREATOR_HEAD_BLEND - Blend (texblend (skin)): ", fSkinToneBlend)
	
	IF IS_CHARACTER_CONTROLLER_PED_OK(characterData.characterCreatorPeds[ENUM_TO_INT(aPedType)])
		SET_PED_HEAD_BLEND_DATA(characterData.characterCreatorPeds[ENUM_TO_INT(aPedType)], iHeadIndex1, iHeadIndex2, 0, iHeadIndex1, iHeadIndex2, 0, fDominance, fSkinToneBlend, 0, TRUE)
	ENDIF
	
	// Set up the initial eye colour
	APPLY_CHARACTER_CREATOR_EYE_COLOUR(characterData)
		
	// Update our neck for the male to keep it thicker than females
	UPDATE_CHARACTER_CREATOR_NECK(characterData)
ENDPROC

/// PURPOSE:
///    Applies a blend change, this if for both the geometry (dominance) and the texture blend (texblend)
PROC APPLY_CHARACTER_CREATOR_DOMINANCE_SKIN_CHANGE(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData)

	FLOAT fDominance, fSkinToneBlend
	CHARACTER_CREATOR_PED aPedType = characterData.eActiveChild
	fDominance = GET_CHARACTER_CREATOR_PARENT_DOMINANCE(characterData, aPedType)
	fSkinToneBlend = GET_CHARACTER_CREATOR_SKIN_TONE_BLEND(characterData, aPedType)
	
	CPRINTLN(DEBUG_NET_CHARACTER, "APPLY_CHARACTER_CREATOR_DOMINANCE - For Active ped")
	CPRINTLN(DEBUG_NET_CHARACTER, "APPLY_CHARACTER_CREATOR_DOMINANCE - For ped: ", PICK_STRING(aPedType = CHARACTER_CREATOR_SON, "Son", "Daughter"), " index: ", NATIVE_TO_INT(characterData.characterCreatorPeds[aPedType]))
	CPRINTLN(DEBUG_NET_CHARACTER, "APPLY_CHARACTER_CREATOR_DOMINANCE - Blend (headblend (dom)): ", fDominance)
	CPRINTLN(DEBUG_NET_CHARACTER, "APPLY_CHARACTER_CREATOR_DOMINANCE - Blend (texblend (skin)): ", fSkinToneBlend)
	
	// Update our first child
	IF IS_CHARACTER_CONTROLLER_PED_OK(characterData.characterCreatorPeds[aPedType])
		UPDATE_PED_HEAD_BLEND_DATA(characterData.characterCreatorPeds[aPedType], fDominance, fSkinToneBlend, 0)	
	ENDIF

	// Switch the ped type to update the non active player
	IF aPedType = CHARACTER_CREATOR_SON
		aPedType = CHARACTER_CREATOR_DAUGHTER
	ELSE
		aPedType = CHARACTER_CREATOR_SON
	ENDIF

	fDominance = GET_CHARACTER_CREATOR_PARENT_DOMINANCE(characterData, aPedType)
	fSkinToneBlend = GET_CHARACTER_CREATOR_SKIN_TONE_BLEND(characterData, aPedType)

	CPRINTLN(DEBUG_NET_CHARACTER, "APPLY_CHARACTER_CREATOR_DOMINANCE - For Non-Active ped")
	CPRINTLN(DEBUG_NET_CHARACTER, "APPLY_CHARACTER_CREATOR_DOMINANCE - For ped: ", PICK_STRING(aPedType = CHARACTER_CREATOR_SON, "Son", "Daughter"), " index: ", NATIVE_TO_INT(characterData.characterCreatorPeds[aPedType]))
	CPRINTLN(DEBUG_NET_CHARACTER, "APPLY_CHARACTER_CREATOR_DOMINANCE - Blend (headblend and texblend): ", fDominance)
	CPRINTLN(DEBUG_NET_CHARACTER, "APPLY_CHARACTER_CREATOR_DOMINANCE - Blend (texblend (skin)): ", fSkinToneBlend)
	
	// Update our first child
	IF IS_CHARACTER_CONTROLLER_PED_OK(characterData.characterCreatorPeds[aPedType])
		UPDATE_PED_HEAD_BLEND_DATA(characterData.characterCreatorPeds[aPedType], fDominance, fSkinToneBlend, 0)
	ENDIF
ENDPROC

/// PURPOSE:
///    Change the head of a parent and refresh the blends on the children
PROC APPLY_CHARACTER_CREATOR_PARENT_HEAD_CHANGE(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, BOOL bDoBlendUpdate = TRUE)
	
	IF bDoBlendUpdate
		// Update the active ped now
		CHARACTER_CREATOR_PED aPedType = characterData.eActiveChild
		APPLY_CHARACTER_CREATOR_HEAD_BLEND(characterData, aPedType)
		
		// Switch the ped type to update the non active player
		IF aPedType = CHARACTER_CREATOR_SON
			aPedType = CHARACTER_CREATOR_DAUGHTER
		ELSE
			aPedType = CHARACTER_CREATOR_SON
		ENDIF
		
		APPLY_CHARACTER_CREATOR_HEAD_BLEND(characterData, aPedType)
	ENDIF
ENDPROC

/// PURPOSE:
///    Change the head of the currently selected parent and update visuals
PROC PROCESS_CHARACTER_PARENT_CHANGE(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, INT iIncrement)

	CHARACTER_CREATOR_PED aPedType
	IF GET_CHARACTER_CREATOR_CURRENT_SELECTION(characterData) = ciCHARACTER_CREATOR_HERITAGE_DAD_NAME
		aPedType = CHARACTER_CREATOR_DAD
	ELSE
		aPedType = CHARACTER_CREATOR_MUM
	ENDIF

	INT iParentToChange = GET_CHARACTER_CREATOR_PARENT_INDEX(aPedType)
	INT iNewHead = GET_CHARACTER_CREATOR_PARENT_HEAD(characterData, iParentToChange) + iIncrement
	
	IF iNewHead < 0
		iNewHead = (characterData.parentData[iParentToChange].iTotalHeadsAvailable - 1)
	ENDIF
	
	IF iNewHead >= characterData.parentData[iParentToChange].iTotalHeadsAvailable
		iNewHead = 0
	ENDIF
	
	SET_CHARACTER_CREATOR_PARENT_HEAD(characterData, iParentToChange, iNewHead)
	
	PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
	
	APPLY_CHARACTER_CREATOR_PARENT_HEAD_CHANGE(characterData)
ENDPROC

/// PURPOSE:
///    Increments the eye colour for the player
PROC PROCESS_CHARACTER_EYE_COLOUR_CHANGE(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, INT iIncrement)
	
	CHARACTER_CREATOR_APPEARANCE_VALUES appearanceValues
	GET_CHARACTER_CREATOR_APPEARANCE_VALUES(characterData, appearanceValues, ciCHARACTER_CREATOR_APPEARANCE_EYES)
	
	INT iNewEyeColour = appearanceValues.iValue + iIncrement
	
	IF iNewEyeColour < 0
		iNewEyeColour = ciCHARACTER_CREATOR_MAX_EYE_AVAIL
	ENDIF
	
	IF iNewEyeColour > ciCHARACTER_CREATOR_MAX_EYE_AVAIL
		iNewEyeColour = 0
	ENDIF
	
	SET_CHARACTER_CREATOR_APPEARANCE_VALUES(characterData, ciCHARACTER_CREATOR_APPEARANCE_EYES, iNewEyeColour, 100, 0, 0, 0)
	
	PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
	
	// Apply the eye colour
	APPLY_CHARACTER_CREATOR_EYE_COLOUR(characterData)
ENDPROC

/// PURPOSE:
///    Randomises the character eye colour
PROC RANDOMISE_CHARACTER_EYE_COLOUR(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData)
	
	INT iRandom = GET_RANDOM_INT_IN_RANGE(0, ciCHARACTER_CREATOR_MAX_EYE_AVAIL)
	
	SET_CHARACTER_CREATOR_APPEARANCE_VALUES(characterData, ciCHARACTER_CREATOR_APPEARANCE_EYES, iRandom, 100, 0, 0, 0)
	
	IF GET_CHARACTER_CREATOR_MENU_STATE(characterData) != ciCHARACTER_CREATOR_COLUMN_MAIN_MENU
		PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
	ENDIF
	
	// Apply the eye colour
	IF NOT IS_CHARACTER_CONTROLLER_BIT_SET(ciCHARACTER_CONTROLLER_RANDOMISE_ALL)
		APPLY_CHARACTER_CREATOR_EYE_COLOUR(characterData)
	ENDIF
ENDPROC

/// PURPOSE:
///    Updates our decal on the players outfit
PROC UPDATE_CHARACTER_CREATOR_PED_DECAL(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, CHARACTER_CREATOR_PED aPedType)
	IF GET_CHARACTER_CREATOR_CREW_TSHIRT(characterData) = CREW_TSHIRT_OFF
		REMOVE_TATTOOS_IN_UPGRADE_GROUP_MP(GET_HASH_KEY("crewLogo"), characterData.characterCreatorPeds[aPedType])
		
		UPDATE_TATOOS_MP(characterData.characterCreatorPeds[aPedType])
	ELSE
		IF NOT IS_PED_INJURED(characterData.characterCreatorPeds[aPedType])
			SWITCH GET_CHARACTER_CREATOR_CREW_TSHIRT_DECAL(characterData)
				CASE CC_CREW_DECAL_NONE
					REMOVE_TATTOOS_IN_UPGRADE_GROUP_MP(GET_HASH_KEY("crewLogo"), characterData.characterCreatorPeds[aPedType])
					UPDATE_TATOOS_MP(characterData.characterCreatorPeds[aPedType])	
				BREAK
				CASE CC_CREW_DECAL_SMALL
					EQUIP_CREW_LOGO(characterData.characterCreatorPeds[aPedType], TATTOO_MP_FM_CREW_A, TRUE)
				BREAK
				CASE CC_CREW_DECAL_BIG
					IF aPedType = CHARACTER_CREATOR_SON
						EQUIP_CREW_LOGO(characterData.characterCreatorPeds[aPedType], TATTOO_MP_FM_CREW_B, TRUE)
					ELSE
						EQUIP_CREW_LOGO(characterData.characterCreatorPeds[aPedType], TATTOO_MP_FM_CREW_C, TRUE)
					ENDIF
				BREAK
				CASE CC_CREW_DECAL_BACK
					EQUIP_CREW_LOGO(characterData.characterCreatorPeds[aPedType], TATTOO_MP_FM_CREW_C, TRUE)
				BREAK
			ENDSWITCH
		ENDIF
	ENDIF
ENDPROC
	
/// PURPOSE:
///    Update the crew tshirt to display the decal
PROC APPLY_CHARACTER_CREATOR_CREW_TSHIRT_DECAL(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData)
	
	UPDATE_CHARACTER_CREATOR_PED_DECAL(characterData, CHARACTER_CREATOR_SON)
	UPDATE_CHARACTER_CREATOR_PED_DECAL(characterData, CHARACTER_CREATOR_DAUGHTER)
	
ENDPROC

/// PURPOSE:
///    make sure any changes are validated if they are shared and need to be updated for a gender change
PROC VALIDATE_CHARACTER_CREATOR_VALUES_FOR_GENDER_CHANGE(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData)

	IF NOT GET_CHARACTER_CREATOR_ACTIVE_PED_IS_MALE(characterData)
		
		IF GET_CHARACTER_CREATOR_CREW_TSHIRT(characterData) != CREW_TSHIRT_OFF
			IF GET_CHARACTER_CREATOR_CREW_TSHIRT_DECAL(characterData) = CC_CREW_DECAL_BACK
				
				CPRINTLN(DEBUG_NET_CHARACTER, "VALIDATE_CHARACTER_CREATOR_VALUES_FOR_GENDER_CHANGE - Player changed gender and we need to remove the ")
				
				SET_CHARACTER_CREATOR_CREW_TSHIRT_DECAL(characterData, CC_CREW_DECAL_NONE)
				
				APPLY_CHARACTER_CREATOR_CREW_TSHIRT_DECAL(characterData)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Change the gender of the player ped
PROC PROCESS_CHARACTER_GENDER_CHANGE(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData)
	
	IF NOT SHOULD_CHARACTER_CREATOR_DISPLAY_GENDER(characterData)
		CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_GENDER_CHANGE - SHOULD_CHARACTER_CREATOR_DISPLAY_GENDER = FALSE")
		EXIT
	ENDIF
	
	// Hide current character
	SET_ENTITY_VISIBLE(characterData.characterCreatorPeds[characterData.eActiveChild], FALSE)
	ADD_ENTITY_TO_AUDIO_MIX_GROUP(characterData.characterCreatorPeds[characterData.eActiveChild], "MUGSHOT_ROOMS_SCENE_MUGSHOT_ROOM_HIDDEN_PED_GROUP")
	
	IF characterData.eActiveChild = CHARACTER_CREATOR_SON
		characterData.eActiveChild = CHARACTER_CREATOR_DAUGHTER
		CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_GENDER_CHANGE - Changing gender to FEMALE")
	ELSE
		characterData.eActiveChild = CHARACTER_CREATOR_SON
		CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_GENDER_CHANGE - Changing gender to MALE")
	ENDIF
	
	REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(characterData.characterCreatorPeds[characterData.eActiveChild])
	PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
	
	// Check if any values need to be updated
	VALIDATE_CHARACTER_CREATOR_VALUES_FOR_GENDER_CHANGE(characterData)
	
	SET_CHARACTER_CREATOR_PED_VISIBLE(characterData)
	
	ATTACH_CHARACTER_CREATOR_BOARD_TO_PED(characterData.characterCreatorPeds[characterData.eActiveChild], characterData, GET_CHARACTER_SELECTOR_SLOT_FOR_CREATED_PED(characterData))
ENDPROC

/// PURPOSE:
///    Update the character parent dominance slider.
PROC PROCESS_CHARACTER_DOMINANCE_CHANGE(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, INT iIncrement)
	
	FLOAT fCurrentDom = GET_CHARACTER_CREATOR_CHILD_DOMINANCE(characterData, characterData.eActiveChild)
	fCurrentDom += (iIncrement*(GET_FRAME_TIME() * cfCHARACTER_CREATOR_SLIDER_MULTIPLIER))
	
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		
		FLOAT fTemp = HANDLE_MOUSE_SLIDER(MOUSE_SLIDER_TYPE_CHARACTER_CREATOR_FAMILY_RESEMBLANCE)
		
		IF fTemp != -1
			fCurrentDom = fTemp
		ENDIF
	ENDIF
	
	IF fCurrentDom < cfCHARACTER_CREATOR_MIN_SLIDER
		SET_CHARACTER_CREATOR_CHILD_DOMINANCE(characterData, characterData.eActiveChild, cfCHARACTER_CREATOR_MIN_SLIDER)
		STOP_SOUND(characterData.iSoundID)
		EXIT
	ENDIF
	
	IF fCurrentDom > cfCHARACTER_CREATOR_MAX_SLIDER
		SET_CHARACTER_CREATOR_CHILD_DOMINANCE(characterData, characterData.eActiveChild, cfCHARACTER_CREATOR_MAX_SLIDER)
		STOP_SOUND(characterData.iSoundID)
		EXIT
	ENDIF
	
	PLAY_SOUND_FRONTEND(characterData.iSoundID, "CONTINUOUS_SLIDER", "HUD_FRONTEND_DEFAULT_SOUNDSET")
	SET_CHARACTER_CONTROLLER_BIT(ciCHARACTER_CONTROLLER_CONTINUOUS_SOUND_PLAYING)
	
	CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_DOMINANCE_CHANGE - New parent dominance: ", fCurrentDom)
	
	SET_CHARACTER_CREATOR_CHILD_DOMINANCE(characterData, characterData.eActiveChild, fCurrentDom)
	
	// Apply our dominance change on the ped
	APPLY_CHARACTER_CREATOR_DOMINANCE_SKIN_CHANGE(characterData)
	
	// Update our neck for the male
	UPDATE_CHARACTER_CREATOR_NECK(characterData)
ENDPROC

/// PURPOSE:
///    Update the character parent dominance slider.
PROC PROCESS_CHARACTER_SKIN_TONE_CHANGE(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, INT iIncrement)

	FLOAT fCurrentSkinTone = GET_CHARACTER_CREATOR_CHILD_SKIN_TONE(characterData, characterData.eActiveChild)
	fCurrentSkinTone += (iIncrement*(GET_FRAME_TIME() * cfCHARACTER_CREATOR_SLIDER_MULTIPLIER))
	
		IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		
			FLOAT fTemp = HANDLE_MOUSE_SLIDER(MOUSE_SLIDER_TYPE_CHARACTER_CREATOR_SKIN_TONE)
		
			IF fTemp != -1
				fCurrentSkinTone = fTemp
			ENDIF
		
		ENDIF
	
	IF fCurrentSkinTone < cfCHARACTER_CREATOR_MIN_SLIDER
		SET_CHARACTER_CREATOR_CHILD_SKIN_TONE(characterData, characterData.eActiveChild, cfCHARACTER_CREATOR_MIN_SLIDER)
		STOP_SOUND(characterData.iSoundID)
		EXIT
	ENDIF
	
	IF fCurrentSkinTone > cfCHARACTER_CREATOR_MAX_SLIDER
		SET_CHARACTER_CREATOR_CHILD_SKIN_TONE(characterData, characterData.eActiveChild, cfCHARACTER_CREATOR_MAX_SLIDER)
		STOP_SOUND(characterData.iSoundID)
		EXIT
	ENDIF
	
	PLAY_SOUND_FRONTEND(characterData.iSoundID, "CONTINUOUS_SLIDER", "HUD_FRONTEND_DEFAULT_SOUNDSET")
	SET_CHARACTER_CONTROLLER_BIT(ciCHARACTER_CONTROLLER_CONTINUOUS_SOUND_PLAYING)
	
	CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_SKIN_TONE_CHANGE - New skin tone: ", fCurrentSkinTone)	
	
	SET_CHARACTER_CREATOR_CHILD_SKIN_TONE(characterData, characterData.eActiveChild, fCurrentSkinTone)
	
	// Apply our dominance change on the ped
	APPLY_CHARACTER_CREATOR_DOMINANCE_SKIN_CHANGE(characterData)
ENDPROC


/// PURPOSE:
///    Returns the ped model for the ped type.
FUNC MODEL_NAMES GET_CHARACTER_CREATOR_MODEL_FOR_PED(CHARACTER_CREATOR_PED pedType)
	
	SWITCH pedType
//		CASE CHARACTER_CREATOR_SON
//		CASE CHARACTER_CREATOR_DAD
//			RETURN GET_PLAYER_MODEL_FOR_TEAM_VARIATION_FOR_MENU(TEAM_FREEMODE, 0)
		
		CASE CHARACTER_CREATOR_DAUGHTER
		CASE CHARACTER_CREATOR_MUM
			RETURN GET_PLAYER_MODEL_FOR_TEAM_VARIATION_FOR_MENU(TEAM_FREEMODE, 1)
	ENDSWITCH
	
	RETURN GET_PLAYER_MODEL_FOR_TEAM_VARIATION_FOR_MENU(TEAM_FREEMODE, 0)
ENDFUNC

/// PURPOSE:
///    Returns the correct ped model for the special peds
FUNC MODEL_NAMES GET_CHARACTER_CREATOR_MODEL_FOR_SPECIAL_PED(INT iPedIndex)
	
	SWITCH iPedIndex
		CASE 0	RETURN MP_F_MISTY_01
		CASE 1	RETURN MP_M_Claude_01
		CASE 2	RETURN MP_M_NIKO_01
		CASE 3	RETURN MP_M_Marston_01
	ENDSWITCH

	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

//--- Random Functions

/// PURPOSE:
///    Picks a random head to set for the mum or dad
PROC SET_CHARACTER_CREATOR_PARENT_RANDOM_HEAD(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, CHARACTER_CREATOR_PED aPedType)
	
	// Get our index and generate random head for max total heads
	INT iIndex = GET_CHARACTER_CREATOR_PARENT_INDEX(aPedType)
	INT iRandomHead = GET_RANDOM_INT_IN_RANGE(0, characterData.parentData[iIndex].iTotalHeadsAvailable)
	
	SET_CHARACTER_CREATOR_PARENT_HEAD(characterData, iIndex, iRandomHead)
ENDPROC


/// PURPOSE:
///    Returns TRUE when all peds have been created
FUNC BOOL ARE_ALL_CHARACTER_CREATOR_PEDS_CREATED()
	RETURN IS_CHARACTER_CONTROLLER_BIT_SET(ciCHARACTER_CONTROLLER_ALL_PEDS_CREATED_BIT)
ENDFUNC

/// PURPOSE:
///    Sets up a static face for parents on the screen
PROC SET_CHARACTER_CREATOR_PED_FACE(PED_INDEX &aPed)

	IF GET_ENTITY_MODEL(aPed) = MP_M_Marston_01
		SET_FACIAL_IDLE_ANIM_OVERRIDE(aPed, "Pose_Angry_1")
	ELSE
		SET_FACIAL_IDLE_ANIM_OVERRIDE(aPed, "Pose_Happy_1")
	ENDIF
ENDPROC

/// PURPOSE:
///    Set the mood of the ped that is created
PROC SET_CHARACTER_CREATOR_PED_MOOD(PED_INDEX &aPed)
	
	// TODO: Do we need moods for the peds?
	
	STRING sMood = GET_PLAYER_MOOD_ANIM_FROM_INDEX(4)
	IF ARE_STRINGS_EQUAL(sMood, "mood_smug_1")
		sMood = "mood_Happy_1"
	ENDIF
	IF ARE_STRINGS_EQUAL(sMood, "mood_sulk_1")
		sMood = "mood_Angry_1"
	ENDIF	
	
	IF NOT IS_PED_INJURED(aPed)
		SET_FACIAL_IDLE_ANIM_OVERRIDE(aPed, sMood)
	ENDIF
ENDPROC

/// PURPOSE:
///    Remove the components from the ped which hide the players face during the creator
PROC REMOVE_CHARACTER_CREATOR_COMPONENTS_FOR_EDITING(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData)

	// Remove masks:
//	IF GET_CHARACTER_CREATOR_ACTIVE_PED_IS_MALE(characterData)
//		set_ped_comp_item_current_mp(characterData.characterCreatorPeds[CHARACTER_CREATOR_SON], COMP_TYPE_BERD, BERD_FMM_0_0, FALSE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, iSlot)
//	ELSE
//		set_ped_comp_item_current_mp(characterData.characterCreatorPeds[CHARACTER_CREATOR_DAUGHTER], COMP_TYPE_BERD, BERD_FMF_0_0, FALSE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, iSlot)
//	ENDIF
	PRINTLN("REMOVE_CHARACTER_CREATOR_COMPONENTS_FOR_EDITING - Called")
	
	// If we are wearing a helmet, remove it
	IF IS_CHARACTER_CONTROLLER_PED_OK(characterData.characterCreatorPeds[CHARACTER_CREATOR_SON])
		
		IF IS_PED_WEARING_HELMET(characterData.characterCreatorPeds[CHARACTER_CREATOR_SON])
			PRINTLN("REMOVE_CHARACTER_CREATOR_COMPONENTS_FOR_EDITING - Removing the helmet from the ped (son)")
	
			CLEAR_PED_PROP(characterData.characterCreatorPeds[CHARACTER_CREATOR_SON], ANCHOR_HEAD)
		ENDIF
		
		IF IS_PED_WEARING_PILOT_SUIT(characterData.characterCreatorPeds[CHARACTER_CREATOR_SON], PED_COMP_TEETH)
			PRINTLN("REMOVE_CHARACTER_CREATOR_COMPONENTS_FOR_EDITING - Removing the pilot suit helmet / tube (son)")
		
			CLEAR_PED_PROP(characterData.characterCreatorPeds[CHARACTER_CREATOR_SON], ANCHOR_HEAD)
				
			SET_PED_COMPONENT_VARIATION(characterData.characterCreatorPeds[CHARACTER_CREATOR_SON], PED_COMP_TEETH, 0, 0)
		ENDIF
		
		PRINTLN("REMOVE_CHARACTER_CREATOR_COMPONENTS_FOR_EDITING - Removing the chem suit hood from the ped (son)")
		
		// Make sure we reset the hood if we need to
		SWAP_CHEM_SUIT_HOOD_FOR_ALTERNATE(characterData.characterCreatorPeds[CHARACTER_CREATOR_SON])
		SET_HOODED_JACKET_STATE(characterData.characterCreatorPeds[CHARACTER_CREATOR_SON], JACKET_HOOD_DOWN)
	ENDIF
	
	IF IS_CHARACTER_CONTROLLER_PED_OK(characterData.characterCreatorPeds[CHARACTER_CREATOR_DAUGHTER])
	
		IF IS_PED_WEARING_HELMET(characterData.characterCreatorPeds[CHARACTER_CREATOR_DAUGHTER])
		
			PRINTLN("REMOVE_CHARACTER_CREATOR_COMPONENTS_FOR_EDITING - Removing the helmet from the ped (daughter)")
		
			CLEAR_PED_PROP(characterData.characterCreatorPeds[CHARACTER_CREATOR_DAUGHTER], ANCHOR_HEAD)
		ENDIF
	
		// If we are wearing the pilot suit, remove it
		IF IS_PED_WEARING_PILOT_SUIT(characterData.characterCreatorPeds[CHARACTER_CREATOR_DAUGHTER], PED_COMP_TEETH)
		
			PRINTLN("REMOVE_CHARACTER_CREATOR_COMPONENTS_FOR_EDITING - Removing the pilot suit helmet / tube (daughter)")
		
			CLEAR_PED_PROP(characterData.characterCreatorPeds[CHARACTER_CREATOR_DAUGHTER], ANCHOR_HEAD)
					
			SET_PED_COMPONENT_VARIATION(characterData.characterCreatorPeds[CHARACTER_CREATOR_DAUGHTER], PED_COMP_TEETH, 0, 0)
		ENDIF
		
		PRINTLN("REMOVE_CHARACTER_CREATOR_COMPONENTS_FOR_EDITING - Removing the chem suit hood from the ped (daughter)")
		
		SWAP_CHEM_SUIT_HOOD_FOR_ALTERNATE(characterData.characterCreatorPeds[CHARACTER_CREATOR_DAUGHTER])
		SET_HOODED_JACKET_STATE(characterData.characterCreatorPeds[CHARACTER_CREATOR_DAUGHTER], JACKET_HOOD_DOWN)
	ENDIF
ENDPROC

/// PURPOSE:
///    Grabs the outfit for the ped and applies it
PROC SET_CHARACTER_CREATOR_OUTFITS(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, CHARACTER_CREATOR_PED pedToUpdate, INT iSlot)
	
	INT iOutfit = GET_CHARACTER_CREATOR_CHILD_OUTFIT(characterData, pedToUpdate)
	INT iOutfitStyle = GET_CHARACTER_CREATOR_CHILD_STYLE(characterData, pedToUpdate)
	
	CHARACTER_CREATOR_OUTFIT spawnOutfit = GET_CHARACTER_CREATOR_OUTFIT_SELECTION(GET_CHARACTER_CREATOR_CHILD_STYLE(characterData, pedToUpdate), iOutfit)
	
	CPRINTLN(DEBUG_NET_CHARACTER, "SET_CHARACTER_CREATOR_OUTFITS - Setting outfit to: ", ENUM_TO_INT(spawnOutfit))
	
	INT iCurrentSlot = g_iPedComponentSlot
	SET_CURRENT_MP_SLOT_FOR_CLOTHES(iSlot)
	
	IF iOutfitStyle = ciCHARACTER_CREATOR_EXISTING_INDEX
		SET_MP_PLAYERS_START_CLOTHES(FALSE, TSHIRT_STATES_INGAME_USE_STORED, characterData.characterCreatorPeds[pedToUpdate], iSlot, DEFAULT, TRUE)	
				
		REMOVE_CHARACTER_CREATOR_COMPONENTS_FOR_EDITING(characterData)
		
		IF GET_CHARACTER_CREATOR_MENU_STATE(characterData) = ciCHARACTER_CREATOR_COLUMN_APPAREL
		
			CPRINTLN(DEBUG_NET_CHARACTER, "SET_CHARACTER_CREATOR_OUTFITS - Update the hair and props as adjusting outfit while in apparrel")
		
			// Apply the hair
			APPLY_CHARACTER_CREATOR_HAIR(characterData, pedToUpdate, iSlot)
		
			APPLY_CHARACTER_CREATOR_HAT(characterData)
			
			APPLY_CHARACTER_CREATOR_GLASSES(characterData)
		ENDIF
		
		IF GET_CHARACTER_CREATOR_CREW_TSHIRT(characterData) != CREW_TSHIRT_OFF
			GIVE_PLAYER_CREW_TSHIRT(characterData.characterCreatorPeds[pedToUpdate], GET_CHARACTER_CREATOR_CREW_TSHIRT(characterData))
			SET_PED_COMP_ITEM_CURRENT_MP(characterData.characterCreatorPeds[pedToUpdate], COMP_TYPE_TEETH, TEETH_FMM_0_0, FALSE)
			
			// Remove t-shirt decal if we have one (valentines/independence/hipster/elitas etc).
			INT iTattoo = GET_MP_INT_CHARACTER_STAT(MP_STAT_TSHIRT_DECAL_HASH, iSlot) 
			TATTOO_NAME_ENUM eTattooForShirt = INVALID_TATTOO
			IF iTattoo != -1 
				IF GET_ENTITY_MODEL(characterData.characterCreatorPeds[pedToUpdate]) = MP_M_FREEMODE_01 
					eTattooForShirt = GET_TATTOO_ENUM_FROM_DLC_HASH(iTattoo, TATTOO_MP_FM) 
				ELIF GET_ENTITY_MODEL(characterData.characterCreatorPeds[pedToUpdate]) = MP_F_FREEMODE_01 
					eTattooForShirt = GET_TATTOO_ENUM_FROM_DLC_HASH(iTattoo, TATTOO_MP_FM_F) 
				ENDIF 
			ENDIF
			IF eTattooForShirt != INVALID_TATTOO
				CPRINTLN(DEBUG_NET_CHARACTER, "...Clearing t-shirt decal")
				SET_MP_TATTOO_CURRENT(eTattooForShirt, FALSE, iSlot) 
			ENDIF
			
			// Equip torso decals for this outfit - do this first as it clears torsoDecal tats (possibly sCharacterOutfitData.aTattoo).
//			EQUIP_TORSO_DECALS_MP(characterData.characterCreatorPeds[pedToUpdate], COMP_TYPE_JBIB, GET_PED_COMP_ITEM_FROM_VARIATIONS(characterData.characterCreatorPeds[pedToUpdate], sCharacterOutfitData.iComponentDrawableID[PED_COMP_JBIB], sCharacterOutfitData.iComponentTextureID[PED_COMP_JBIB], COMP_TYPE_JBIB), FALSE)
//			
//			CPRINTLN(DEBUG_NET_CHARACTER, "SET_CHARACTER_CREATOR_OUTFITS - Setting tattoo: ", ENUM_TO_INT(sCharacterOutfitData.aTattoo))
//			
//			// If we have a tattoo to apply then do so.
//			IF sCharacterOutfitData.aTattoo	!= INVALID_TATTOO
//				SET_MP_TATTOO_CURRENT(sCharacterOutfitData.aTattoo, TRUE, iSlot)
//				SET_MP_INT_CHARACTER_STAT(MP_STAT_TSHIRT_DECAL_HASH, ENUM_TO_INT(sCharacterOutfitData.aTattoo), iSlot)
//			ELSE
//				SET_MP_INT_CHARACTER_STAT(MP_STAT_TSHIRT_DECAL_HASH, -1, iSlot)
//			ENDIF
		ENDIF
	ELSE
		SET_MP_CHARACTER_CREATOR_OUTFIT(characterData, characterData.characterCreatorPeds[pedToUpdate], spawnOutfit, iSlot)
		
		SET_PED_COMPONENT_VARIATION(characterData.characterCreatorPeds[pedToUpdate], PED_COMP_HAND, 0, 0)
	ENDIF
	
	// Always restore this value for the main player ped
	SET_CURRENT_MP_SLOT_FOR_CLOTHES(iCurrentSlot)
ENDPROC

/// PURPOSE:
///    Update the players appearance based on the crew tshirt they have selected
PROC APPLY_CHARACTER_CREATOR_CREW_TSHIRT(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData)

	SET_CHARACTER_CREATOR_OUTFITS(characterData, CHARACTER_CREATOR_SON, characterData.iCharacterSelectedSlot)
	SET_CHARACTER_CREATOR_OUTFITS(characterData, CHARACTER_CREATOR_DAUGHTER, characterData.iCharacterSelectedSlot)
	
ENDPROC

/// PURPOSE:
///    Apply the micromorph
PROC APPLY_CHARACTER_CREATOR_MICRO_MORPH(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, INT iFeature)

	// Get our data that we need to
	CHARACTER_CREATOR_FEATURE_DATA featureData
	GET_CHARACTER_CREATOR_FEATURE_DATA(featureData, iFeature)
	
	// Grab our values
	CHARACTER_CREATOR_FEATURE_VALUES featureValues
	GET_CHARACTER_CREATOR_FEATURE_VALUES(characterData, featureValues, iFeature)

	CPRINTLN(DEBUG_NET_CHARACTER, "APPLY_CHARACTER_CREATOR_MICRO_MORPH - Apply micromorph to feature: ", iFeature)
	CPRINTLN(DEBUG_NET_CHARACTER, "							fXBlend - ", featureValues.fXBlend)
	CPRINTLN(DEBUG_NET_CHARACTER, "							fYBlend - ", featureValues.fYBlend)
	
	// Normalise our blend percentage between the values we are allowed
	FLOAT fNormalisedXBlend = GET_CHARACTER_CREATOR_NORMALISED_BLEND(featureValues.fXBlend, featureData.fXMax, featureData.fXMin)
	FLOAT fNormalisedYBlend = GET_CHARACTER_CREATOR_NORMALISED_BLEND(featureValues.fYBlend, featureData.fYMax, featureData.fYMin)

	CPRINTLN(DEBUG_NET_CHARACTER, "							fNormalisedX - ", fNormalisedXBlend)
	CPRINTLN(DEBUG_NET_CHARACTER, "							fNormalisedY - ", fNormalisedYBlend)
	CPRINTLN(DEBUG_NET_CHARACTER, "----------------------------------------------")
	
	
	CHARACTER_CREATOR_PED eSecondaryPed = CHARACTER_CREATOR_SON
	IF characterData.eActiveChild = CHARACTER_CREATOR_SON
		eSecondaryPed = CHARACTER_CREATOR_DAUGHTER
	ENDIF
	
	IF IS_CHARACTER_CONTROLLER_PED_OK(characterData.characterCreatorPeds[characterData.eActiveChild])
		IF featureData.bXAxisActive
			SET_PED_MICRO_MORPH(characterData.characterCreatorPeds[characterData.eActiveChild], featureData.morphXAxis, fNormalisedXBlend)
		ENDIF
		IF featureData.bYAxisActive
			SET_PED_MICRO_MORPH(characterData.characterCreatorPeds[characterData.eActiveChild], featureData.morphYAxis, fNormalisedYBlend)
		ENDIF
	ENDIF
	
	// Always apply it to secondary ped after the first
	IF IS_CHARACTER_CONTROLLER_PED_OK(characterData.characterCreatorPeds[eSecondaryPed])
		IF featureData.bXAxisActive
			SET_PED_MICRO_MORPH(characterData.characterCreatorPeds[eSecondaryPed], featureData.morphXAxis, fNormalisedXBlend)
		ENDIF
		IF featureData.bYAxisActive
			SET_PED_MICRO_MORPH(characterData.characterCreatorPeds[eSecondaryPed], featureData.morphYAxis, fNormalisedYBlend)
		ENDIF	
	ENDIF
ENDPROC

/// PURPOSE:
///    Apply all our micromorphs to the peds
PROC APPLY_CHARACTER_CREATOR_ALL_MICRO_MORPH(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData)

	// Get our data that we need to
	CHARACTER_CREATOR_FEATURE_DATA featureData
	CHARACTER_CREATOR_FEATURE_VALUES featureValues
	
	FLOAT fNormalisedXBlend
	FLOAT fNormalisedYBlend
	
	INT iFeature
	
	REPEAT ciCHARACTER_CREATOR_MAX_FEATURES iFeature
		
		GET_CHARACTER_CREATOR_FEATURE_DATA(featureData, iFeature)
		GET_CHARACTER_CREATOR_FEATURE_VALUES(characterData, featureValues, iFeature)
		
		CPRINTLN(DEBUG_NET_CHARACTER, "APPLY_CHARACTER_CREATOR_ALL_MICRO_MORPH - Apply micromorph to feature: ", iFeature)
		CPRINTLN(DEBUG_NET_CHARACTER, "							fXBlend - ", featureValues.fXBlend)
		CPRINTLN(DEBUG_NET_CHARACTER, "							fYBlend - ", featureValues.fYBlend)
		
		fNormalisedXBlend = GET_CHARACTER_CREATOR_NORMALISED_BLEND(featureValues.fXBlend, featureData.fXMax, featureData.fXMin)
		fNormalisedYBlend = GET_CHARACTER_CREATOR_NORMALISED_BLEND(featureValues.fYBlend, featureData.fYMax, featureData.fYMin)
		
		IF featureData.iGenderSpecific = CHARACTER_CREATOR_GENDER_MALE
		OR featureData.iGenderSpecific = CHARACTER_CREATOR_GENDER_MALE_HIDE
		OR featureData.iGenderSpecific = CHARACTER_CREATOR_GENDER_BOTH
			IF featureData.bXAxisActive
				SET_PED_MICRO_MORPH(characterData.characterCreatorPeds[CHARACTER_CREATOR_SON], featureData.morphXAxis, fNormalisedXBlend)
			ENDIF
			IF featureData.bYAxisActive
				SET_PED_MICRO_MORPH(characterData.characterCreatorPeds[CHARACTER_CREATOR_SON], featureData.morphYAxis, fNormalisedYBlend)
			ENDIF
		ENDIF
		
		
		IF featureData.iGenderSpecific = CHARACTER_CREATOR_GENDER_FEMALE
		OR featureData.iGenderSpecific = CHARACTER_CREATOR_GENDER_FEMALE_HIDE
		OR featureData.iGenderSpecific = CHARACTER_CREATOR_GENDER_BOTH
			IF featureData.bXAxisActive
				SET_PED_MICRO_MORPH(characterData.characterCreatorPeds[CHARACTER_CREATOR_DAUGHTER], featureData.morphXAxis, fNormalisedXBlend)
			ENDIF
			IF featureData.bYAxisActive
				SET_PED_MICRO_MORPH(characterData.characterCreatorPeds[CHARACTER_CREATOR_DAUGHTER], featureData.morphYAxis, fNormalisedYBlend)
			ENDIF
		ENDIF
		
	ENDREPEAT	
ENDPROC

/// PURPOSE:
///    Apply all overlays to the ped passed through
PROC APPLY_CHARACTER_CREATOR_ALL_OVERLAYS_TO_PED(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, CHARACTER_CREATOR_PED aPedType)

	IF NOT IS_CHARACTER_CONTROLLER_PED_OK(characterData.characterCreatorPeds[aPedType])
		EXIT
	ENDIF
	
	FLOAT fNormalisedBlend
	
	CHARACTER_CREATOR_FEATURE_GENDER featureGender = CHARACTER_CREATOR_GENDER_MALE
	CHARACTER_CREATOR_FEATURE_GENDER featureSecondaryGender = CHARACTER_CREATOR_GENDER_MALE_HIDE
	
	IF aPedType = CHARACTER_CREATOR_DAUGHTER
		featureGender = CHARACTER_CREATOR_GENDER_FEMALE
		featureSecondaryGender = CHARACTER_CREATOR_GENDER_FEMALE_HIDE
	ENDIF
	
	INT iAppearance
	CHARACTER_CREATOR_APPEARANCE_DATA appearanceData
	CHARACTER_CREATOR_APPEARANCE_VALUES appearanceValues

	INT iOverlayIndex
	CPRINTLN(DEBUG_NET_CHARACTER, "Applying overlays **********************************************")
	
	STATS_PACKED overlayStat

	REPEAT ciCHARACTER_CREATOR_MAX_OVERLAYS iOverlayIndex
		iAppearance = GET_CHARACTER_CREATOR_FEATURE_FOR_OVERLAY_INDEX(iOverlayIndex, (aPedType = CHARACTER_CREATOR_SON))
		
		GET_CHARACTER_CREATOR_APPEARANCE_DATA(appearanceData, iAppearance)
		
		IF appearanceData.iGenderSpecific = CHARACTER_CREATOR_GENDER_BOTH
		OR appearanceData.iGenderSpecific = featureGender
		OR appearanceData.iGenderSpecific = featureSecondaryGender
			
			GET_CHARACTER_CREATOR_APPEARANCE_VALUES(characterData, appearanceValues, iAppearance)
		
			// If the value is current, restore our saved makeup
			IF appearanceValues.iValue = ciCHARACTER_CREATOR_EXISTING_OVERLAY_INDEX
				overlayStat = GET_CHARACTER_OVERLAY_STAT(appearanceData.overlaySlot)
				
				IF overlayStat != INT_TO_ENUM(STATS_PACKED, -1)
					appearanceValues.iValue = GET_PACKED_STAT_INT(overlayStat, characterData.iCharacterSelectedSlot)
				ENDIF
			ENDIF
		
			fNormalisedBlend = appearanceValues.fOpacity / 100
			
			// If we have a valid slot then apply the overlay to the ped
			IF appearanceData.overlaySlot != INT_TO_ENUM(HEAD_OVERLAY_SLOT, -1)
			
				CPRINTLN(DEBUG_NET_CHARACTER, "		Applying overlay: for ped: ", PICK_STRING(aPedType = CHARACTER_CREATOR_SON, "Son", "Daughter"), " (", NATIVE_TO_INT(characterData.characterCreatorPeds[aPedType]), ") Overlay: ", GET_CHARACTER_OVERLAY_TEXT(appearanceData.overlaySlot), " is now. Value = ", appearanceValues.iValue, " and blend = ", fNormalisedBlend)
			
				SET_PED_HEAD_OVERLAY(characterData.characterCreatorPeds[aPedType], appearanceData.overlaySlot, appearanceValues.iValue, fNormalisedBlend)
				
				// Apply the colour if valid
				IF appearanceData.bColourActive
					CPRINTLN(DEBUG_NET_CHARACTER, "		Applying overlay colour: ", GET_CHARACTER_OVERLAY_TEXT(appearanceData.overlaySlot), ", iColour = ", appearanceValues.iColour, ", rtType = ", ENUM_TO_INT(appearanceData.colourPalette))
					SET_PED_HEAD_OVERLAY_TINT(characterData.characterCreatorPeds[aPedType], appearanceData.overlaySlot, appearanceData.colourPalette, appearanceValues.iColour)
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	CPRINTLN(DEBUG_NET_CHARACTER, "Finished applying overlays **********************************************")

ENDPROC

/// PURPOSE:
///    Apply the overlay
PROC APPLY_CHARACTER_CREATOR_OVERLAY(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, BOOL bBoth)

	CHARACTER_CREATOR_PED aPedType = characterData.eActiveChild

	// Apply to our first ped
	APPLY_CHARACTER_CREATOR_ALL_OVERLAYS_TO_PED(characterData, aPedType)

	IF bBoth
		IF aPedType = CHARACTER_CREATOR_SON
			aPedType = CHARACTER_CREATOR_DAUGHTER
		ELSE
			aPedType = CHARACTER_CREATOR_SON
		ENDIF
		
		// Apply to our second ped
		APPLY_CHARACTER_CREATOR_ALL_OVERLAYS_TO_PED(characterData, aPedType)
	ENDIF
ENDPROC

/// PURPOSE:
///    Make sure the facial hair / eyebrows match the hair colour
PROC UPDATE_CHARACTER_CREATOR_HAIR_TO_MATCH_COLOURS(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData)
	
	IF NOT IS_CHARACTER_CONTROLLER_BIT_SET(ciCHARACTER_CONTROLLER_PLAYER_HAS_EDITED_HAIR_OVERLAYS)
	
		CHARACTER_CREATOR_APPEARANCE_VALUES appearanceValues
		GET_CHARACTER_CREATOR_APPEARANCE_VALUES(characterData, appearanceValues, ciCHARACTER_CREATOR_APPEARANCE_HAIR_STYLE_M)
		
		// Save out the male hair colour
		INT iHairColour = appearanceValues.iColour
		INT iHighlight = appearanceValues.iColourHighlightValue
		
		CPRINTLN(DEBUG_NET_CHARACTER, "UPDATE_CHARACTER_CREATOR_HAIR_TO_MATCH_COLOURS - Setting the male characters eyebrows and beard to match hair colour: ", iHairColour, " and highlight = ", appearanceValues.iColourHighlightValue)
		
		// Grab the values of the eyebrows and set these to match the hair
		GET_CHARACTER_CREATOR_APPEARANCE_VALUES(characterData, appearanceValues, ciCHARACTER_CREATOR_APPEARANCE_EYEBROWS_M)
		SET_CHARACTER_CREATOR_APPEARANCE_VALUES(characterData, ciCHARACTER_CREATOR_APPEARANCE_EYEBROWS_M, appearanceValues.iValue, appearanceValues.fOpacity, iHairColour, iHairColour, iHighlight)
		
		// Grab the values of the facial hair and set these to match the hair
		GET_CHARACTER_CREATOR_APPEARANCE_VALUES(characterData, appearanceValues, ciCHARACTER_CREATOR_APPEARANCE_FACIAL_HAIR)
		SET_CHARACTER_CREATOR_APPEARANCE_VALUES(characterData, ciCHARACTER_CREATOR_APPEARANCE_FACIAL_HAIR, appearanceValues.iValue, appearanceValues.fOpacity, iHairColour, iHairColour, iHighlight)
		
		// Save out the female hair colour
		GET_CHARACTER_CREATOR_APPEARANCE_VALUES(characterData, appearanceValues, ciCHARACTER_CREATOR_APPEARANCE_HAIR_STYLE_F)
		
		iHairColour = appearanceValues.iColour
		iHighlight = appearanceValues.iColourHighlightValue
		
		CPRINTLN(DEBUG_NET_CHARACTER, "UPDATE_CHARACTER_CREATOR_HAIR_TO_MATCH_COLOURS - Setting the female characters eyebrows : ", iHairColour, " and iHighlight = ", iHighlight)
		
		// Grab the values of the eyebrows and set these to match the hair
		GET_CHARACTER_CREATOR_APPEARANCE_VALUES(characterData, appearanceValues, ciCHARACTER_CREATOR_APPEARANCE_EYEBROWS_F)
		SET_CHARACTER_CREATOR_APPEARANCE_VALUES(characterData, ciCHARACTER_CREATOR_APPEARANCE_EYEBROWS_F, appearanceValues.iValue, appearanceValues.fOpacity, iHairColour, iHairColour, iHighlight)
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Finalises the creator ped with clothes, facial expression.
PROC COMPLETE_CHARACTER_CREATOR_PED_SETUP(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, CHARACTER_CREATOR_PED pedToCreate, INT iSlot)
	
	SWITCH pedToCreate
	
//		CASE CHARACTER_CREATOR_DAD
//			CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_CREATOR_PED_CREATION - Complete setup of Dad")
//			
//			IF NOT IS_PED_INJURED(characterData.characterCreatorPeds[ENUM_TO_INT(pedToCreate)])
//				SET_PED_COMPONENT_VARIATION(characterData.characterCreatorPeds[ENUM_TO_INT(pedToCreate)], PED_COMP_JBIB, 4, 0)
//				SET_PED_COMPONENT_VARIATION(characterData.characterCreatorPeds[ENUM_TO_INT(pedToCreate)], PED_COMP_SPECIAL, 13, 1)
//			ENDIF
//			
//			TASK_PLAY_ANIM(characterData.characterCreatorPeds[ENUM_TO_INT(pedToCreate)], "move_m@generic", "idle", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_HOLD_LAST_FRAME, 0.9, TRUE)
//			SET_CHARACTER_CREATOR_PED_FACE(characterData.characterCreatorPeds[ENUM_TO_INT(pedToCreate)])
//		BREAK
//		
//		CASE CHARACTER_CREATOR_MUM
//			CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_CREATOR_PED_CREATION - Complete setup of Mum")
//			
//			IF NOT IS_PED_INJURED(characterData.characterCreatorPeds[ENUM_TO_INT(pedToCreate)])
//				SET_PED_COMPONENT_VARIATION(characterData.characterCreatorPeds[ENUM_TO_INT(pedToCreate)], PED_COMP_JBIB, 9, 0)
//				SET_PED_COMPONENT_VARIATION(characterData.characterCreatorPeds[ENUM_TO_INT(pedToCreate)], PED_COMP_FEET, 1, 0)
//			ENDIF
//			
//			TASK_PLAY_ANIM(characterData.characterCreatorPeds[ENUM_TO_INT(pedToCreate)], "move_m@generic", "idle", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_HOLD_LAST_FRAME, 0.9, TRUE)
//			SET_CHARACTER_CREATOR_PED_FACE(characterData.characterCreatorPeds[ENUM_TO_INT(pedToCreate)])
//		BREAK
		
		CASE CHARACTER_CREATOR_SON
		CASE CHARACTER_CREATOR_DAUGHTER
		
			IF IS_CHARACTER_CONTROLLER_PED_OK(characterData.characterCreatorPeds[ENUM_TO_INT(pedToCreate)])
			
				CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_CREATOR_PED_CREATION - Complete setup of child")
				TASK_PLAY_ANIM(characterData.characterCreatorPeds[ENUM_TO_INT(pedToCreate)], "MP_HEAD_IK_OVERRIDE", "MP_CREATOR_HEADIK", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_SECONDARY|AF_LOOPING|AF_ADDITIVE)
			
				SET_MP_PLAYERS_START_CLOTHES(TRUE, TSHIRT_STATES_NEWCHARACTER, characterData.characterCreatorPeds[ENUM_TO_INT(pedToCreate)], iSlot, DEFAULT, TRUE)
			
				IF HAS_ENTERED_CHAR_CREATION_FROM_ALTERATION_PROMPT()
					SET_MP_CHARACTER_TATTOOS_AND_PATCHES(characterData.characterCreatorPeds[ENUM_TO_INT(pedToCreate)], iSlot)
				ENDIF
				
				// Update our mood!
				SET_CHARACTER_CREATOR_PED_MOOD(characterData.characterCreatorPeds[ENUM_TO_INT(pedToCreate)])
			ENDIF
		BREAK
	
	ENDSWITCH
	
ENDPROC


/// PURPOSE:
///    Create all our peds ready for creator
PROC PROCESS_CHARACTER_CREATOR_PED_CREATION(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData)
	
	VECTOR CamCoord
	FLOAT CamRot
	CamCoord = GET_CHARACTER_CREATOR_PED_POSITION()
	CamRot = GET_CHARACTER_CREATOR_PED_HEADING()
	
	CHARACTER_CREATOR_PED pedToCreate = INT_TO_ENUM(CHARACTER_CREATOR_PED, characterData.iCharacterCreatorPedToCreate)
	
	MODEL_NAMES mnModel = GET_CHARACTER_CREATOR_MODEL_FOR_PED(pedToCreate)
	
	REQUEST_MODEL(mnModel)
	REQUEST_ANIM_DICT("MP_HEAD_IK_OVERRIDE")
	REQUEST_ANIM_DICT("move_m@generic")
	
	IF HAS_MODEL_LOADED(mnModel)
	AND HAS_ANIM_DICT_LOADED("MP_HEAD_IK_OVERRIDE")
	AND HAS_ANIM_DICT_LOADED("move_m@generic")
		
		IF NOT DOES_ENTITY_EXIST(characterData.characterCreatorPeds[ENUM_TO_INT(pedToCreate)])
		
			characterData.characterCreatorPeds[ENUM_TO_INT(pedToCreate)] = CREATE_PED(PEDTYPE_SPECIAL, mnModel, CamCoord, 0.0, FALSE, FALSE)
	
			SET_ENTITY_HEADING(characterData.characterCreatorPeds[ENUM_TO_INT(pedToCreate)], CamRot)
			CLEAR_PED_TASKS_IMMEDIATELY(characterData.characterCreatorPeds[ENUM_TO_INT(pedToCreate)])
			FREEZE_ENTITY_POSITION(characterData.characterCreatorPeds[ENUM_TO_INT(pedToCreate)], FALSE)
			SET_ENTITY_COLLISION(characterData.characterCreatorPeds[ENUM_TO_INT(pedToCreate)], TRUE)
			TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(characterData.characterCreatorPeds[ENUM_TO_INT(pedToCreate)], TRUE)
			STOP_PED_SPEAKING(characterData.characterCreatorPeds[ENUM_TO_INT(pedToCreate)], TRUE)
			DISABLE_PED_PAIN_AUDIO(characterData.characterCreatorPeds[ENUM_TO_INT(pedToCreate)], TRUE)
			SET_ENTITY_VISIBLE(characterData.characterCreatorPeds[ENUM_TO_INT(pedToCreate)], FALSE)
		
			// Make sure we align everything
			FORCE_ALL_HEADING_VALUES_TO_ALIGN(characterData.characterCreatorPeds[ENUM_TO_INT(pedToCreate)])
						
			// Remove model
			SET_MODEL_AS_NO_LONGER_NEEDED(mnModel)
		
			CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_CREATOR_PED_CREATION - Ped: ", ENUM_TO_INT(pedToCreate), " has been created at ", CamCoord)
		
			//COMPLETE_CHARACTER_CREATOR_PED_SETUP(characterData, pedToCreate, iSlot)
			
			characterData.iCharacterCreatorPedToCreate++
						
			// Cap the index
			IF characterData.iCharacterCreatorPedToCreate >= ENUM_TO_INT(CHARACTER_CREATOR_MAX_PEDS)
				characterData.iCharacterCreatorPedToCreate = 0
			ENDIF
		ELSE
			characterData.iCharacterCreatorPedToCreate++
						
			// Cap the index
			IF characterData.iCharacterCreatorPedToCreate >= ENUM_TO_INT(CHARACTER_CREATOR_MAX_PEDS)
				characterData.iCharacterCreatorPedToCreate = 0
			ENDIF
		ENDIF
	#IF IS_DEBUG_BUILD
	ELSE
		CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_CREATOR_PED_CREATION - Failing on:")
		IF NOT HAS_MODEL_LOADED(mnModel)
			CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_CREATOR_PED_CREATION - HAS_MODEL_LOADED(mnModel) = FALSE")
		ENDIF
		IF NOT HAS_ANIM_DICT_LOADED("MP_HEAD_IK_OVERRIDE")
			CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_CREATOR_PED_CREATION - HAS_ANIM_DICT_LOADED(MP_HEAD_IK_OVERRIDE) = FALSE")
		ENDIF
		IF NOT HAS_ANIM_DICT_LOADED("move_m@generic")
			CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_CREATOR_PED_CREATION - HAS_ANIM_DICT_LOADED(move_m@generic) = FALSE")
		ENDIF
		
	#ENDIF
	
	ENDIF
	
	INT i
	BOOL bAllPedsLoaded = TRUE
	// Check if all normal peds have been created
	REPEAT CHARACTER_CREATOR_MAX_PEDS i
		IF NOT DOES_ENTITY_EXIST(characterData.characterCreatorPeds[i])
			
			CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_CREATOR_PED_CREATION - Ped: ", i, " has not been created")
			bAllPedsLoaded = FALSE
		ENDIF
	ENDREPEAT
		
	// If all have been created, move to the next state of blending
	IF bAllPedsLoaded
		CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_CREATOR_PED_CREATION - All peds created. Move to next state")
		CPRINTLN(DEBUG_NET_CHARACTER, "										- Son......................: ", NATIVE_TO_INT(characterData.characterCreatorPeds[CHARACTER_CREATOR_SON]))
		CPRINTLN(DEBUG_NET_CHARACTER, "										- Daughter.................: ", NATIVE_TO_INT(characterData.characterCreatorPeds[CHARACTER_CREATOR_DAUGHTER]))
				
		SET_CHARACTER_CREATOR_PED_CREATION_STATE(CHARACTER_CREATOR_PED_STATE_BLEND)
	ENDIF	
ENDPROC

/// PURPOSE:
///    Sets all the peds head to the correct value and begins a blend
PROC PROCESS_CHARACTER_CREATOR_PED_HEAD_BLENDS(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData)
	
	INT i
	CHARACTER_CREATOR_PED aPedType
	
	REPEAT CHARACTER_CREATOR_MAX_PEDS i
		
		aPedType = INT_TO_ENUM(CHARACTER_CREATOR_PED, i)
		
		SWITCH aPedType
			CASE CHARACTER_CREATOR_SON
			CASE CHARACTER_CREATOR_DAUGHTER
				APPLY_CHARACTER_CREATOR_HEAD_BLEND(characterData, aPedType)
			BREAK
		
		ENDSWITCH
	ENDREPEAT
	
	SET_CHARACTER_CREATOR_PED_CREATION_STATE(CHARACTER_CREATOR_PED_STATE_APPLY_OUTFIT)
ENDPROC

/// PURPOSE:
///    Generates all peds required for the character creator. This will occur when on the select screen so we can
///    offer the player a new character. Loads Mum / Dad, Children and special peds
PROC GENERATE_CHARACTER_CREATOR_PEDS(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, MPHUD_PLACEMENT_TOOLS& Placement)

	IF NOT ARE_ALL_CHARACTER_CREATOR_PEDS_CREATED()

		//CPRINTLN(DEBUG_NET_CHARACTER, "GENERATE_CHARACTER_CREATOR_PEDS - Placement.ScreenPlace.iSelectedCharacter = ", Placement.ScreenPlace.iSelectedCharacter)
		//CPRINTLN(DEBUG_NET_CHARACTER, "GENERATE_CHARACTER_CREATOR_PEDS - GET_CHARACTER_SELECTOR_SLOT_FOR_CREATED_PED(characterData) = ", GET_CHARACTER_SELECTOR_SLOT_FOR_CREATED_PED(characterData))

		INT iSlot
		
		IF HAS_ENTERED_CHAR_CREATION_FROM_ALTERATION_PROMPT()
			iSlot = Placement.ScreenPlace.iSelectedCharacter
			//CPRINTLN(DEBUG_NET_CHARACTER, "GENERATE_CHARACTER_CREATOR_PEDS - Placement.ScreenPlace.iSelectedCharacter")
		#IF FEATURE_GEN9_STANDALONE
		ELIF GET_CHOSEN_MP_CHARACTER_SLOT() >= 0
			iSlot = GET_CHOSEN_MP_CHARACTER_SLOT()
		#ENDIF
		ELSE
			iSlot = GET_CHARACTER_SELECTOR_SLOT_FOR_CREATED_PED(characterData)
			//CPRINTLN(DEBUG_NET_CHARACTER, "GENERATE_CHARACTER_CREATOR_PEDS - GET_CHARACTER_SELECTOR_SLOT_FOR_CREATED_PED(characterData)")
		ENDIF
		
		SWITCH GET_CHARACTER_CREATOR_PED_CREATION_STATE()
		
			CASE CHARACTER_CREATOR_PED_STATE_CREATE
				PROCESS_CHARACTER_CREATOR_PED_CREATION(characterData)
			BREAK
			
			CASE CHARACTER_CREATOR_PED_STATE_BLEND
				PROCESS_CHARACTER_CREATOR_PED_HEAD_BLENDS(characterData)
			BREAK
			
			CASE CHARACTER_CREATOR_PED_STATE_APPLY_OUTFIT
			
				// Complete the character here
				COMPLETE_CHARACTER_CREATOR_PED_SETUP(characterData, CHARACTER_CREATOR_SON, iSlot)
				COMPLETE_CHARACTER_CREATOR_PED_SETUP(characterData, CHARACTER_CREATOR_DAUGHTER, iSlot)
			
				SET_CHARACTER_CREATOR_OUTFITS(characterData, CHARACTER_CREATOR_SON, iSlot)
				SET_CHARACTER_CREATOR_OUTFITS(characterData, CHARACTER_CREATOR_DAUGHTER, iSlot)
				
				IF HAS_ENTERED_CHAR_CREATION_FROM_ALTERATION_PROMPT()
					
					SET_MP_PLAYERS_START_CLOTHES(FALSE, TSHIRT_STATES_INGAME_USE_STORED, characterData.characterCreatorPeds[characterData.eActiveChild], iSlot, DEFAULT, TRUE)

					SET_CHARACTER_CREATOR_CHILD_STYLE(characterData, characterData.eActiveChild, ciCHARACTER_CREATOR_EXISTING_INDEX)
					
					SET_CHARACTER_CREATOR_OUTFITS(characterData, CHARACTER_CREATOR_SON, iSlot)
					SET_CHARACTER_CREATOR_OUTFITS(characterData, CHARACTER_CREATOR_DAUGHTER, iSlot)
					
					CHARACTER_CREATOR_APPEARANCE_VALUES appearanceValues
					
					IF GET_CHARACTER_CREATOR_ACTIVE_PED_IS_MALE(characterData)
						GET_CHARACTER_CREATOR_APPEARANCE_VALUES(characterData, appearanceValues, ciCHARACTER_CREATOR_APPEARANCE_HAIR_STYLE_M)
						SET_CHARACTER_CREATOR_APPEARANCE_VALUES(characterData, ciCHARACTER_CREATOR_APPEARANCE_HAIR_STYLE_M, ciCHARACTER_CREATOR_EXISTING_INDEX, 100, appearanceValues.iColour, appearanceValues.iColour2, appearanceValues.iColourHighlightValue)
					ELSE
						GET_CHARACTER_CREATOR_APPEARANCE_VALUES(characterData, appearanceValues, ciCHARACTER_CREATOR_APPEARANCE_HAIR_STYLE_F)
						SET_CHARACTER_CREATOR_APPEARANCE_VALUES(characterData, ciCHARACTER_CREATOR_APPEARANCE_HAIR_STYLE_F, ciCHARACTER_CREATOR_EXISTING_INDEX, 100, appearanceValues.iColour, appearanceValues.iColour2, appearanceValues.iColourHighlightValue)
					ENDIF
					
					UPDATE_CHARACTER_CREATOR_SECONDARY_HAIR_COLOUR(characterData, characterData.eActiveChild)
				ENDIF
				
				// Apply the overlays on our peds
				APPLY_CHARACTER_CREATOR_OVERLAY(characterData, TRUE)
				APPLY_CHARACTER_CREATOR_ALL_MICRO_MORPH(characterData)
				
				REMOVE_ANIM_DICT("MP_HEAD_IK_OVERRIDE")
				REMOVE_ANIM_DICT("move_m@generic")
				
				SET_CHARACTER_CREATOR_PED_CREATION_STATE(CHARACTER_CREATOR_PED_STATE_WAIT_FOR_BLEND)
			BREAK
			
			CASE CHARACTER_CREATOR_PED_STATE_WAIT_FOR_BLEND
				IF HAS_CHARACTER_CREATOR_PED_FINISHED_BLEND(characterData, CHARACTER_CREATOR_SON)
				AND HAS_CHARACTER_CREATOR_PED_FINISHED_BLEND(characterData, CHARACTER_CREATOR_DAUGHTER)
					
					APPLY_CHARACTER_CREATOR_HAIR(characterData, CHARACTER_CREATOR_SON, iSlot)
					APPLY_CHARACTER_CREATOR_HAIR(characterData, CHARACTER_CREATOR_DAUGHTER, iSlot)
					
					CPRINTLN(DEBUG_NET_CHARACTER, "GENERATE_CHARACTER_CREATOR_PEDS - Children have completed blend, setup hair and move through")					
					SET_CHARACTER_CREATOR_PED_CREATION_STATE(CHARACTER_CREATOR_PED_STATE_COMPLETE)
					
				#IF IS_DEBUG_BUILD
				ELSE
					CPRINTLN(DEBUG_NET_CHARACTER, "GENERATE_CHARACTER_CREATOR_PEDS - Waiting on peds to finish:")
					
					IF NOT HAS_CHARACTER_CREATOR_PED_FINISHED_BLEND(characterData, CHARACTER_CREATOR_SON)
						CPRINTLN(DEBUG_NET_CHARACTER, "					 - Son still blending: ", NATIVE_TO_INT(characterData.characterCreatorPeds[CHARACTER_CREATOR_SON]))
					ENDIF
					
					IF NOT HAS_CHARACTER_CREATOR_PED_FINISHED_BLEND(characterData, CHARACTER_CREATOR_DAUGHTER)
						CPRINTLN(DEBUG_NET_CHARACTER, "					 - Daughter still blending: ", NATIVE_TO_INT(characterData.characterCreatorPeds[CHARACTER_CREATOR_DAUGHTER]))
					ENDIF					
				#ENDIF
					
				ENDIF
			BREAK
			
			
		
		ENDSWITCH
	
	#IF IS_DEBUG_BUILD
	ELSE
		CPRINTLN(DEBUG_NET_CHARACTER, "GENERATE_CHARACTER_CREATOR_PEDS - ARE_ALL_CHARACTER_CREATOR_PEDS_CREATED() = FALSE")
	#ENDIF
	
	ENDIF
ENDPROC



/// PURPOSE:
///    Returns TRUE if the special head is valid to the player
FUNC BOOL IS_CHARACTER_CREATOR_SPECIAL_PED_AVAILABLE(INT iHead, HEAD_BLEND_HEAD_TYPE aSpecialHeadType)
	
	INT iHeadIndex = iHead - GET_PED_HEAD_BLEND_FIRST_INDEX(aSpecialHeadType)
	
	IF aSpecialHeadType = HBHT_UNIQUE_MALE
		SWITCH iHeadIndex
			// Claude
			CASE 0
				IF ARE_CHARACTER_CREATOR_COLLECTOR_EDITIONS_AVAILABLE()
					RETURN TRUE
				ENDIF
			BREAK
			
			// Niko
			CASE 1
				IF ARE_CHARACTER_CREATOR_COLLECTOR_EDITIONS_AVAILABLE()
					RETURN TRUE
				ENDIF
			BREAK
			
			// John M
			CASE 2
				IF IS_GAME_LINKED_TO_SOCIAL_CLUB()
					RETURN TRUE
				ENDIF
			BREAK
		ENDSWITCH
	ELSE
		SWITCH iHeadIndex
			// Misty
			CASE 0
				IF ARE_CHARACTER_CREATOR_COLLECTOR_EDITIONS_AVAILABLE()
					RETURN TRUE
				ENDIF
			BREAK
		
		ENDSWITCH
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Shuffles an array of heads for us
PROC SHUFFLE_CHARACTER_CREATOR_HEAD_ARRAY(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, INT iIndex)
	
	INT i
	INT iRandom
	CHARACTER_CREATOR_PARENT_HEAD_DATA sTempHeadInt
	FOR i = 0 TO (characterData.parentData[iIndex].iTotalHeadsAvailable - 1) STEP 1
		
		iRandom = GET_RANDOM_INT_IN_RANGE(0, characterData.parentData[iIndex].iTotalHeadsAvailable)
		
		sTempHeadInt = characterData.parentData[iIndex].sHeadSelection[i]
		
		characterData.parentData[iIndex].sHeadSelection[i] = characterData.parentData[iIndex].sHeadSelection[iRandom]
		characterData.parentData[iIndex].sHeadSelection[iRandom] = sTempHeadInt
	ENDFOR
	
ENDPROC

/// PURPOSE:
///    Returns a hair style for a parent
FUNC INT GET_CHARACTER_CREATOR_RANDOM_PARENT_HAIR(BOOL bIsMale)

	INT iHair
	IF bIsMale
		iHair = GET_RANDOM_INT_IN_RANGE(0, 4)
		SWITCH iHair
			CASE 0		RETURN 4
			CASE 1		RETURN 7
			CASE 2		RETURN 9
			CASE 3		RETURN 10		
		ENDSWITCH
	ELSE
		iHair = GET_RANDOM_INT_IN_RANGE(0, 5)
		SWITCH iHair
			CASE 0		RETURN 9
			CASE 1		RETURN 10
			CASE 2		RETURN 11
			CASE 3		RETURN 14
			CASE 4		RETURN 15
		ENDSWITCH
	ENDIF
	
	RETURN 0
ENDFUNC

/// PURPOSE:
///    Fill in the array with all the heads available to us
PROC SETUP_CHARACTER_CREATOR_PARENT_HEAD_ARRAY(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, CHARACTER_CREATOR_PED aPedType)

	
	// Grab the relevant values based on the ped type, mum or dad
	HEAD_BLEND_HEAD_TYPE aHeadType, aSpecialHeadType
	IF aPedType = CHARACTER_CREATOR_DAD
		aHeadType = HBHT_MALE
		aSpecialHeadType = HBHT_UNIQUE_MALE
	ELSE
		aHeadType = HBHT_FEMALE
		aSpecialHeadType = HBHT_UNIQUE_FEMALE
	ENDIF
	
	INT iIndex = GET_CHARACTER_CREATOR_PARENT_INDEX(aPedType)
	INT iStartIndex = GET_PED_HEAD_BLEND_FIRST_INDEX(aHeadType)
	INT iEndIndex = iStartIndex + GET_PED_HEAD_BLEND_NUM_HEADS(aHeadType)
	
	// Loop over populating all our heads into our arrays
	INT i
	INT iTotalHeads
	FOR i = iStartIndex TO (iEndIndex - 1) STEP 1
		
		characterData.parentData[iIndex].sHeadSelection[iTotalHeads].iHead = (i - GET_PED_HEAD_BLEND_FIRST_INDEX(aHeadType))
		characterData.parentData[iIndex].sHeadSelection[iTotalHeads].iHair = GET_CHARACTER_CREATOR_RANDOM_PARENT_HAIR((aPedType = CHARACTER_CREATOR_DAD))
		characterData.parentData[iIndex].sHeadSelection[iTotalHeads].iHairColour = 5
		
		iTotalHeads++
	ENDFOR
	
	iStartIndex = GET_PED_HEAD_BLEND_FIRST_INDEX(aSpecialHeadType)
	iEndIndex = GET_PED_HEAD_BLEND_NUM_HEADS(aSpecialHeadType)

	// Loop over populating all our special heads that are available
	FOR i = 0 TO (iEndIndex - 1) STEP 1
		
		IF IS_CHARACTER_CREATOR_SPECIAL_PED_AVAILABLE(i + iStartIndex, aSpecialHeadType)
			characterData.parentData[iIndex].sHeadSelection[iTotalHeads].iHead = (i + GET_PED_HEAD_BLEND_NUM_HEADS(aHeadType))	// Index will be the max + 0, 1, 2 etc.
			characterData.parentData[iIndex].sHeadSelection[iTotalHeads].iHair = GET_CHARACTER_CREATOR_RANDOM_PARENT_HAIR((aPedType = CHARACTER_CREATOR_DAD))
			characterData.parentData[iIndex].sHeadSelection[iTotalHeads].iHairColour = 5
			
			iTotalHeads++
		ENDIF
	ENDFOR
	
	// Save out the total heads the parents can have
	characterData.parentData[iIndex].iTotalHeadsAvailable = iTotalHeads

	// Shuffle this so its always random
	SHUFFLE_CHARACTER_CREATOR_HEAD_ARRAY(characterData, iIndex)

	// Print out the details of the heads
	#IF IS_DEBUG_BUILD
		REPEAT iTotalHeads i
			CPRINTLN(DEBUG_NET_CHARACTER, "SETUP_CHARACTER_CREATOR_PARENT_HEAD_ARRAY - for parent: ", iIndex, ", [", i, "] = ", characterData.parentData[iIndex].sHeadSelection[i].iHead)
		ENDREPEAT
		
		CPRINTLN(DEBUG_NET_CHARACTER, "SETUP_CHARACTER_CREATOR_PARENT_HEAD_ARRAY - total heads: ")
	#ENDIF
ENDPROC

/// PURPOSE:
///    Falls through all the essential data setup that is required for the start of the creator
PROC POPULATE_CHARACTER_CREATOR_PARENT_DATA(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData)

	// Populate both the dad and mum head array.
	SETUP_CHARACTER_CREATOR_PARENT_HEAD_ARRAY(characterData, CHARACTER_CREATOR_MUM)
	SETUP_CHARACTER_CREATOR_PARENT_HEAD_ARRAY(characterData, CHARACTER_CREATOR_DAD)

	// Set our initial random heads
	SET_CHARACTER_CREATOR_PARENT_RANDOM_HEAD(characterData, CHARACTER_CREATOR_MUM)
	SET_CHARACTER_CREATOR_PARENT_RANDOM_HEAD(characterData, CHARACTER_CREATOR_DAD)

ENDPROC

/// PURPOSE:
///    Picks a random gender for the creator
PROC RANDOMISE_CHARACTER_CREATOR_GENDER(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData)
	// randomise gender
	IF GET_RANDOM_INT_IN_RANGE(0,2) = 0
		characterData.eActiveChild = CHARACTER_CREATOR_SON
		CPRINTLN(DEBUG_NET_CHARACTER, "RANDOMISE_CHARACTER_CREATOR_GENDER - Randomise gender to MALE")
	ELSE
		characterData.eActiveChild = CHARACTER_CREATOR_DAUGHTER
		CPRINTLN(DEBUG_NET_CHARACTER, "RANDOMISE_CHARACTER_CREATOR_GENDER - Randomise gender to FEMALE")
	ENDIF
ENDPROC

/// PURPOSE:
///    Randomises the passed child peds dominance based on gender
PROC RANDOMISE_CHARACTER_CREATOR_DOM(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, CHARACTER_CREATOR_PED aPedType, BOOL bFullRange = FALSE)
	INT iMinDom
	INT iMaxDom
	
	IF aPedType = CHARACTER_CREATOR_SON
		iMinDom = 70
		iMaxDom = 100
	ELSE
		iMinDom = 0
		iMaxDom = 30
	ENDIF
	
	IF bFullRange
		iMinDom = 0
		iMaxDom = 100
	ENDIF
	
	INT iDom = GET_RANDOM_INT_IN_RANGE(iMinDom, iMaxDom)
		
	CPRINTLN(DEBUG_NET_CHARACTER, "RANDOMISE_CHARACTER_CREATOR_DOM - Randomise parent dominance to: ", iDom)
	SET_CHARACTER_CREATOR_CHILD_DOMINANCE(characterData, aPedType, TO_FLOAT(iDom))
ENDPROC

/// PURPOSE:
///    Randomises the passed child peds skin tone blend based on gender
PROC RANDOMISE_CHARACTER_CREATOR_SKIN_TONE(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, CHARACTER_CREATOR_PED aPedType, BOOL bFullRange = FALSE)
	INT iMinDom
	INT iMaxDom
	
	IF aPedType = CHARACTER_CREATOR_SON
		iMinDom = 60
		iMaxDom = 100
	ELSE
		iMinDom = 0
		iMaxDom = 40
	ENDIF
	
	IF bFullRange
		iMinDom = 0
		iMaxDom = 100
	ENDIF
	
	INT iDom = GET_RANDOM_INT_IN_RANGE(iMinDom, iMaxDom)
	
	CPRINTLN(DEBUG_NET_CHARACTER, "RANDOMISE_CHARACTER_CREATOR_SKIN_TONE - Randomise skin tone blend to: ", iDom)
	SET_CHARACTER_CREATOR_CHILD_SKIN_TONE(characterData, aPedType, TO_FLOAT(iDom))
ENDPROC


/// PURPOSE:
///    Randomise our dominance based on the gender
PROC RANDOMISE_CHARACTER_CREATOR_DOM_AND_SKIN_TONE(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, CHARACTER_CREATOR_PED aPedType)
	RANDOMISE_CHARACTER_CREATOR_DOM(characterData, aPedType)
	RANDOMISE_CHARACTER_CREATOR_SKIN_TONE(characterData, aPedType)
ENDPROC

/// PURPOSE:
///    Populates the stats to a random amount for the beginning of the creator or if the player hits random
PROC RANDOMISE_CHARACTER_CREATOR_STATS(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, BOOL bClearOnly = FALSE)
	
	INT i
	CHARACTER_CREATOR_STATS aStat
	CHARACTER_CREATOR_STAT_DATA statData
	
	// Have to initialise all stats to their minimum values since player won't get these back
	REPEAT CHARACTER_CREATOR_MAX_STATS i
		
		aStat = INT_TO_ENUM(CHARACTER_CREATOR_STATS, i)
		
		GET_CHARACTER_CREATOR_STAT_DATA(statData, aStat)
		
		SET_CHARACTER_CREATOR_STAT_VALUE(characterData, aStat, statData.iMinValue)
	ENDREPEAT
	
	INT iStatContribution = 5
	characterData.eCharacterSharedData.iStatValuesLeftToAssign = CHARACTER_CREATOR_MAX_POINTS
	
	IF bClearOnly
		CPRINTLN(DEBUG_NET_CHARACTER, "RANDOMISE_CHARACTER_CREATOR_STATS - Clearing all stats and not applying any changes")
		EXIT
	ENDIF
	
	INT iRandomStat
	INT iNewStatValue
	// Loop until all stats are completed
	WHILE GET_CHARACTER_CREATOR_STATS_LEFT_TO_ASSIGN(characterData) > 0
		
		iRandomStat = GET_RANDOM_INT_IN_RANGE(0, ENUM_TO_INT(CHARACTER_CREATOR_MAX_STATS))
		aStat = INT_TO_ENUM(CHARACTER_CREATOR_STATS, iRandomStat)
		
		GET_CHARACTER_CREATOR_STAT_DATA(statData, aStat)
		
		// Increment if we can
		IF GET_CHARACTER_CREATOR_STAT_VALUE(characterData, aStat) < statData.iMaxValue
			iNewStatValue = GET_CHARACTER_CREATOR_STAT_VALUE(characterData, aStat) + iStatContribution
			SET_CHARACTER_CREATOR_STAT_VALUE(characterData, aStat, iNewStatValue)
			ADJUST_CHARACTER_CREATOR_STATS_LEFT_TO_ASSIGN(characterData, -iStatContribution)
		ENDIF		
	ENDWHILE
	
	#IF IS_DEBUG_BUILD
		REPEAT CHARACTER_CREATOR_MAX_STATS i
			aStat = INT_TO_ENUM(CHARACTER_CREATOR_STATS, i)
		
			GET_CHARACTER_CREATOR_STAT_DATA(statData, aStat)
			CPRINTLN(DEBUG_NET_CHARACTER, "RANDOMISE_CHARACTER_CREATOR_STATS - Stat: ", statData.tlStatTitle, " value = ", GET_CHARACTER_CREATOR_STAT_VALUE(characterData, aStat))
		ENDREPEAT
	#ENDIF	
ENDPROC


/// PURPOSE:
///    Handle a change of a feature preset (updating graph / morph)
PROC PROCESS_CHARACTER_CREATOR_FEATURE_CHANGE(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, INT iIncrement)
	
	INT iCurrentFeature = GET_CHARACTER_CREATOR_CURRENT_SELECTION(characterData)
	
	CHARACTER_CREATOR_FEATURE_VALUES 		featureValues
	CHARACTER_CREATOR_FEATURE_DATA			featureData
	CHARACTER_CREATOR_FEATURE_PRESET_DATA	featurePresetData
	
	GET_CHARACTER_CREATOR_FEATURE_VALUES(characterData, featureValues, iCurrentFeature)
	GET_CHARACTER_CREATOR_FEATURE_DATA(featureData, iCurrentFeature)
	
	// Exit early if there are no presets to scroll over
	IF featureData.iNumOfPresets <= 1
		EXIT
	ENDIF
	
	INT iNewPreset = featureValues.iPreset + iIncrement
	INT iMinPreset = 0
		
	IF iNewPreset < iMinPreset
		iNewPreset = featureData.iNumOfPresets-1	
	ENDIF
	
	IF iNewPreset >= featureData.iNumOfPresets
		iNewPreset = iMinPreset
	ENDIF
	
	// Grab the new data for this preset
	GET_CHARACTER_CREATOR_FEATURE_PRESET_DATA(featurePresetData, iCurrentFeature, iNewPreset)
	
	// Set our values.
	SET_CHARACTER_CREATOR_FEATURE_VALUES(characterData, iCurrentFeature, iNewPreset, featurePresetData.fXBlend, featurePresetData.fYBlend)
	
	// Stop continuous sound when navigating menu rows
	IF NOT HAS_SOUND_FINISHED(characterData.iSoundID)
		STOP_SOUND(characterData.iSoundID)
	ENDIF 
	
	PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
	
	APPLY_CHARACTER_CREATOR_MICRO_MORPH(characterData, iCurrentFeature)
ENDPROC

/// PURPOSE:
///    Applies the body overlay associated with the face overlay applied
PROC UPDATE_CHARACTER_CREATOR_BODY_OVERLAYS(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, INT iAppearance, CHARACTER_CREATOR_PED aPedType)

	CHARACTER_CREATOR_APPEARANCE_VALUES 		appearanceValues
	CHARACTER_CREATOR_APPEARANCE_DATA			appearanceData
	
	GET_CHARACTER_CREATOR_APPEARANCE_VALUES(characterData, appearanceValues, iAppearance)
	GET_CHARACTER_CREATOR_APPEARANCE_DATA(appearanceData, iAppearance)

	IF appearanceData.bOverlayActive
		IF DOES_CHARACTER_CREATOR_APPEARANCE_HAVE_BODY_OVERLAY(appearanceData.overlaySlot)
		
			CPRINTLN(DEBUG_NET_CHARACTER, "UPDATE_CHARACTER_CREATOR_BODY_OVERLAYS - Appearance ", GET_CHARACTER_OVERLAY_TEXT(appearanceData.overlaySlot), " has body overlays with it")
			
			INT iValue
			INT iOverlaySlot
						
			GET_CHARACTER_CREATOR_BODY_OVERLAY_FOR_APPEARANCE(appearanceData.overlaySlot, appearanceValues.iValue, (aPedType = CHARACTER_CREATOR_SON), iOverlaySlot, iValue)
			
			CPRINTLN(DEBUG_NET_CHARACTER, "UPDATE_CHARACTER_CREATOR_BODY_OVERLAYS - Setting appearance ", iOverlaySlot, " to a value of: ", iValue, " [Overlay Val = ", appearanceValues.iValue, "]")
			
			// Set the body overlay to the value returned
			SET_CHARACTER_CREATOR_APPEARANCE_VALUES(characterData, iOverlaySlot, iValue, 100, 0, 0, 0)
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE:
///    Handle a change of a appearance (updating graph / morph)
PROC PROCESS_CHARACTER_CREATOR_APPEARANCE_CHANGE(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, INT iIncrement)
	
	INT iCurrentAppearance = GET_CHARACTER_CREATOR_CURRENT_SELECTION(characterData)
	
	CHARACTER_CREATOR_APPEARANCE_VALUES 		appearanceValues
	CHARACTER_CREATOR_APPEARANCE_DATA			appearanceData
	
	GET_CHARACTER_CREATOR_APPEARANCE_VALUES(characterData, appearanceValues, iCurrentAppearance)
	GET_CHARACTER_CREATOR_APPEARANCE_DATA(appearanceData, iCurrentAppearance)
	
	INT iNewValue = appearanceValues.iValue
	INT iPreviousValue = appearanceValues.iValue
	INT iMinValue = appearanceData.iValueMin
	
	// For makeup we allow current for face paint etc.
	IF HAS_ENTERED_CHAR_CREATION_FROM_ALTERATION_PROMPT()
		IF iCurrentAppearance = ciCHARACTER_CREATOR_APPEARANCE_EYE_MAKEUP
		OR iCurrentAppearance = ciCHARACTER_CREATOR_APPEARANCE_EYE_MAKEUP_M
			iMinValue = ciCHARACTER_CREATOR_EXISTING_OVERLAY_INDEX
		ENDIF
	ENDIF
	
	BOOL bFound
	
	WHILE NOT bFound
		
		iNewValue += iIncrement
		
		IF iNewValue < iMinValue
			iNewValue = appearanceData.iValueMax-1
		ENDIF
		
		IF iNewValue >= appearanceData.iValueMax
			iNewValue = iMinValue
		ENDIF
		
		// Check if its valid
		IF IS_CHARACTER_CREATOR_OVERLAY_VALUE_VALID(appearanceData.overlaySlot, iNewValue)
		OR iNewValue = iPreviousValue
			bFound = TRUE	
		ENDIF
	ENDWHILE
	
	// Set our values.
	SET_CHARACTER_CREATOR_APPEARANCE_VALUES(characterData, iCurrentAppearance, iNewValue, appearanceValues.fOpacity, appearanceValues.iColour, appearanceValues.iColour2, appearanceValues.iColourHighlightValue)
	
	// Update our body overlays
	UPDATE_CHARACTER_CREATOR_BODY_OVERLAYS(characterData, iCurrentAppearance, characterData.eActiveChild)
	
	IF GET_CHARACTER_CREATOR_MENU_STATE(characterData) != ciCHARACTER_CREATOR_COLUMN_MAIN_MENU
		PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
	ENDIF
	
	APPLY_CHARACTER_CREATOR_OVERLAY(characterData, FALSE)
ENDPROC



/// PURPOSE:
///    Increment our colour tint for an overlay on the player character
FUNC INT PROCESS_CHARACTER_CREATOR_OVERLAY_COLOUR_CHANGE(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, INT iIncrement, INT iSelection, INT iMinVal, INT iMaxVal, BOOL bMouseSelect = FALSE)

	// Grab the players current values for the selected appearance
	CHARACTER_CREATOR_APPEARANCE_VALUES appearanceValues
	CHARACTER_CREATOR_APPEARANCE_DATA appearanceData
	GET_CHARACTER_CREATOR_APPEARANCE_DATA(appearanceData, iSelection)
	GET_CHARACTER_CREATOR_APPEARANCE_VALUES(characterData, appearanceValues, iSelection)

	INT iNewOverlayTint = appearanceValues.iColour
	INT iHighlight = 0

	
	// Mouse selecting of colours
	IF bMouseSelect

		IF PAUSE_MENU_GET_HAIR_COLOUR_INDEX() = -1
			RETURN 0
		ENDIF
	
		iNewOverlayTint = GET_TINT_FROM_INDEX(appearanceData.overlaySlot, PAUSE_MENU_GET_HAIR_COLOUR_INDEX(), appearanceData.iColourMax)
	
	// Gamepad
	ELSE
	
		BOOL bFound
		
		WHILE NOT bFound
			
			iNewOverlayTint += iIncrement
			
			// Cap our value within the bounds
			IF iNewOverlayTint < iMinVal
				iNewOverlayTint = iMaxVal-1
			ENDIF
			
			IF iNewOverlayTint >= iMaxVal
				iNewOverlayTint = iMinVal
			ENDIF
			
			IF iNewOverlayTint = appearanceValues.iColour
			OR IS_CHARACTER_CREATOR_TINT_VALID(appearanceData.overlaySlot, iNewOverlayTint)
				bFound = TRUE
			ENDIF
		ENDWHILE
		
	ENDIF
		
	iHighlight = GET_CHARACTER_CREATOR_COLOUR_INDEX(appearanceData.overlaySlot, iNewOverlayTint, appearanceData.iColourMax)

//	CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_CREATOR_OVERLAY_COLOUR_CHANGE - New Colour: ", iNewOverlayTint, " and highlight: ", iHighlight)
		
	SET_CHARACTER_CREATOR_APPEARANCE_VALUES(characterData, iSelection, appearanceValues.iValue, appearanceValues.fOpacity, iNewOverlayTint, iNewOverlayTint, iHighlight)
	
	// Do not play sound on "None" option
	IF (appearanceValues.iValue != -1) 
	OR GET_CHARACTER_CREATOR_MENU_STATE(characterData) != ciCHARACTER_CREATOR_COLUMN_MAIN_MENU
		PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
	ENDIF
	
	// Apply the overlays
	APPLY_CHARACTER_CREATOR_OVERLAY(characterData, FALSE)
		
	// If we edit our eyebrows and facial hair, stop any global colour change
	IF iSelection = ciCHARACTER_CREATOR_APPEARANCE_EYEBROWS_F
	OR iSelection = ciCHARACTER_CREATOR_APPEARANCE_EYEBROWS_M
	OR iSelection = ciCHARACTER_CREATOR_APPEARANCE_FACIAL_HAIR
		IF NOT IS_CHARACTER_CONTROLLER_BIT_SET(ciCHARACTER_CONTROLLER_PLAYER_HAS_EDITED_HAIR_OVERLAYS)
			CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_CREATOR_APPEARANCE_CHANGE - Player now edited hair overlays. No global colour change")
			SET_CHARACTER_CONTROLLER_BIT(ciCHARACTER_CONTROLLER_PLAYER_HAS_EDITED_HAIR_OVERLAYS)
		ENDIF
	ENDIF
	
	RETURN iNewOverlayTint
ENDFUNC


/// PURPOSE:
///    Handle a change to a stat
PROC PROCESS_CHARACTER_STAT_CHANGE(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, INT iIncrement)

	INT iStat = GET_CHARACTER_CREATOR_CURRENT_SELECTION(characterData)
	
	IF iIncrement > 0
	
		// If there are no points left to assign then get out
		IF GET_CHARACTER_CREATOR_STATS_LEFT_TO_ASSIGN(characterData) = 0
			STOP_SOUND(characterData.iSoundID)
			EXIT
		ENDIF
	ELSE
		
		// If we haven't got any points to assign then don't let us change anymore
		IF GET_CHARACTER_CREATOR_STATS_LEFT_TO_ASSIGN(characterData) >= CHARACTER_CREATOR_MAX_POINTS
			STOP_SOUND(characterData.iSoundID)
			EXIT
		ENDIF
	ENDIF
	
	CHARACTER_CREATOR_STAT_DATA theData
	CHARACTER_CREATOR_STATS aStat = INT_TO_ENUM(CHARACTER_CREATOR_STATS, iStat)
	
	// Ensure the stat we are adjusting is valid
	IF GET_CHARACTER_CREATOR_STAT_DATA(theData, aStat)
	
		INT iCurrentValue = GET_CHARACTER_CREATOR_STAT_VALUE(characterData, aStat)
		IF iIncrement > 0
		
			IF iCurrentValue < theData.iMaxValue
				
				CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_STAT_CHANGE - Increasing stat: ", theData.tlStatTitle, " by 1")
				
				SET_CHARACTER_CREATOR_STAT_VALUE(characterData, aStat, iCurrentValue + 1)
				ADJUST_CHARACTER_CREATOR_STATS_LEFT_TO_ASSIGN(characterData, -1)
			ENDIF
		ELSE
		
			IF iCurrentValue > theData.iMinValue
				
				CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_STAT_CHANGE - Decreasing stat: ", theData.tlStatTitle, " by -1")
				
				SET_CHARACTER_CREATOR_STAT_VALUE(characterData, aStat, iCurrentValue - 1)
				ADJUST_CHARACTER_CREATOR_STATS_LEFT_TO_ASSIGN(characterData, 1)		
			ENDIF
		ENDIF
		
		IF (iCurrentValue <= theData.iMinValue OR iCurrentValue >= theData.iMaxValue)
			STOP_SOUND(characterData.iSoundID)
		ELSE
			PLAY_SOUND_FRONTEND(characterData.iSoundID, "CONTINUOUS_SLIDER", "HUD_FRONTEND_DEFAULT_SOUNDSET")
			SET_CHARACTER_CONTROLLER_BIT(ciCHARACTER_CONTROLLER_CONTINUOUS_SOUND_PLAYING)
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE: 
///    Preloads character creator hair
FUNC BOOL PRELOAD_CHARACTER_CREATOR_HAIR(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData)
	
	PED_COMP_NAME_ENUM theHair = GET_CHARACTER_CREATOR_HAIR_ENUM(characterData, characterData.eActiveChild)  
	PED_COMP_ITEM_DATA_STRUCT theHairStruct = GET_PED_COMP_DATA_FOR_ITEM_MP(GET_ENTITY_MODEL(characterData.characterCreatorPeds[characterData.eActiveChild]), COMP_TYPE_HAIR, theHair)
	
	IF IS_CHARACTER_CONTROLLER_PED_OK(characterData.characterCreatorPeds[characterData.eActiveChild])
		IF theHairStruct.iDrawable != -1
			SET_PED_PRELOAD_VARIATION_DATA(characterData.characterCreatorPeds[characterData.eActiveChild], INT_TO_ENUM(PED_COMPONENT, COMP_TYPE_HAIR), theHairStruct.iDrawable, 0)
			
			IF HAS_PED_PRELOAD_VARIATION_DATA_FINISHED(characterData.characterCreatorPeds[characterData.eActiveChild])
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE: 
///    Preloads outfits for styles in the apparel menu
FUNC BOOL PRELOAD_CHARACTER_CREATOR_OUTFIT(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData)
	
	INT iOutfitIndex
	INT iCurrentStyle = GET_CHARACTER_CREATOR_CHILD_STYLE(characterData, characterData.eActiveChild)
	INT iMaxOutfits = GET_CHARACTER_CREATOR_MAX_OUTFITS_FOR_STYLE(iCurrentStyle, GET_CHARACTER_CREATOR_ACTIVE_PED_IS_MALE(characterData))
	INT iOutfit = GET_CHARACTER_CREATOR_CHILD_OUTFIT(characterData, characterData.eActiveChild)

	//Pass in the correct outfit index
	iOutfitIndex = (iCurrentStyle * iMaxOutfits) + iOutfit
	
	MP_CHARACTER_CREATOR_OUTFIT sCharacterOutfitData
	CHARACTER_CREATOR_OUTFIT eOutfit = INT_TO_ENUM(CHARACTER_CREATOR_OUTFIT, iOutfitIndex)
	GET_CHARACTER_CREATOR_OUTFIT(GET_ENTITY_MODEL(characterData.characterCreatorPeds[characterData.eActiveChild]), eOutfit, sCharacterOutfitData)
	
	IF IS_CHARACTER_CONTROLLER_PED_OK(characterData.characterCreatorPeds[characterData.eActiveChild])
		INT i
		REPEAT NUM_PED_COMPONENTS i
			IF sCharacterOutfitData.iComponentDrawableID[i] != -1
				SET_PED_PRELOAD_VARIATION_DATA(characterData.characterCreatorPeds[characterData.eActiveChild], INT_TO_ENUM(PED_COMPONENT, i), sCharacterOutfitData.iComponentDrawableID[i], sCharacterOutfitData.iComponentTextureID[i])
			ENDIF
		ENDREPEAT
	ENDIF
	
	IF HAS_PED_PRELOAD_VARIATION_DATA_FINISHED(characterData.characterCreatorPeds[characterData.eActiveChild])
		RETURN TRUE 
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if the style index is valid for character
FUNC BOOL IS_CHARACTER_CREATOR_STYLE_AVAILABLE(INT iStyle)

	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_UnlockAllCareerOutfits")
		RETURN TRUE
	ENDIF
	#ENDIF
	
	SWITCH iStyle
		CASE 0 // Street
		CASE 1 // Flashy
		CASE 2 // Party
		CASE 3 // Beach
		CASE 4 // Smart
		CASE 5 // Sporty
		CASE 6 // Eccentric
		CASE 7 // Casual
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	#IF FEATURE_GEN9_STANDALONE
	SWITCH iStyle
		CASE 8 // Executive
			RETURN (GET_PENDING_CHARACTER_CAREER_FOR_NEW_CHARACTER() = CHARACTER_CAREER_EXECUTIVE)
		BREAK
		CASE 9 // Gunrunner
			RETURN (GET_PENDING_CHARACTER_CAREER_FOR_NEW_CHARACTER() = CHARACTER_CAREER_GUNRUNNER)
		BREAK
		CASE 10 // Nightclub
			RETURN (GET_PENDING_CHARACTER_CAREER_FOR_NEW_CHARACTER() = CHARACTER_CAREER_NIGHTCLUB_OWNER)
		BREAK
		CASE 11 // Biker
			RETURN (GET_PENDING_CHARACTER_CAREER_FOR_NEW_CHARACTER() = CHARACTER_CAREER_BIKER)
		BREAK
	ENDSWITCH
	#ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Sets a random style for the ped
PROC RANDOMISE_CHARACTER_CREATOR_STYLE(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, INT iFixedStyle = -1)

	INT iRandomStyle
	iRandomStyle = GET_RANDOM_INT_IN_RANGE(0, ciCHARACTER_CREATOR_MAX_STYLES)
	WHILE NOT IS_CHARACTER_CREATOR_STYLE_AVAILABLE(iRandomStyle)
		iRandomStyle = GET_RANDOM_INT_IN_RANGE(0, ciCHARACTER_CREATOR_MAX_STYLES)
	ENDWHILE
	
	IF iFixedStyle != -1
		iRandomStyle = iFixedStyle
	ENDIF
	
	SET_CHARACTER_CREATOR_CHILD_STYLE(characterData, CHARACTER_CREATOR_SON, iRandomStyle)
	
	iRandomStyle = GET_RANDOM_INT_IN_RANGE(0, ciCHARACTER_CREATOR_MAX_STYLES)
	WHILE NOT IS_CHARACTER_CREATOR_STYLE_AVAILABLE(iRandomStyle)
		iRandomStyle = GET_RANDOM_INT_IN_RANGE(0, ciCHARACTER_CREATOR_MAX_STYLES)
	ENDWHILE
	
	IF iFixedStyle != -1
		iRandomStyle = iFixedStyle
	ENDIF
	
	SET_CHARACTER_CREATOR_CHILD_STYLE(characterData, CHARACTER_CREATOR_DAUGHTER, iRandomStyle)
	
	// Dont call preload on current outfit in style if randomising from main menu
	// Just set the new randomised style and let randomise outfit call preload
	IF NOT IS_CHARACTER_CONTROLLER_BIT_SET(ciCHARACTER_CONTROLLER_RANDOMISE_ALL)
	
		// Release if in middle of preload - else apply current outfit in style
		IF IS_CHARACTER_CONTROLLER_BIT_SET(ciCHARACTER_CONTROLLER_PRELOAD_OUTFIT)
			IF IS_CHARACTER_CONTROLLER_PED_OK(characterData.characterCreatorPeds[characterData.eActiveChild])
				RELEASE_PED_PRELOAD_VARIATION_DATA(characterData.characterCreatorPeds[characterData.eActiveChild])
			ENDIF
		ELSE
			SET_CHARACTER_CONTROLLER_BIT(ciCHARACTER_CONTROLLER_PRELOAD_OUTFIT)
		ENDIF
		
	ENDIF
ENDPROC

/// PURPOSE:
///    Generates a random outfit based on the style for the characters
PROC RANDOMISE_CHARACTER_CREATOR_OUTFIT(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData)
	
	INT iRandomOutfit
	iRandomOutfit = GET_RANDOM_INT_IN_RANGE(0, GET_CHARACTER_CREATOR_MAX_OUTFITS_FOR_STYLE(GET_CHARACTER_CREATOR_CHILD_STYLE(characterData, CHARACTER_CREATOR_SON), TRUE))
		
	SET_CHARACTER_CREATOR_CHILD_OUTFIT(characterData, CHARACTER_CREATOR_SON, iRandomOutfit)
	
	iRandomOutfit = GET_RANDOM_INT_IN_RANGE(0, GET_CHARACTER_CREATOR_MAX_OUTFITS_FOR_STYLE(GET_CHARACTER_CREATOR_CHILD_STYLE(characterData, CHARACTER_CREATOR_DAUGHTER), FALSE))

	SET_CHARACTER_CREATOR_CHILD_OUTFIT(characterData, CHARACTER_CREATOR_DAUGHTER, iRandomOutfit)
	
	// Release if in middle of preload - else apply outfit
	IF IS_CHARACTER_CONTROLLER_BIT_SET(ciCHARACTER_CONTROLLER_PRELOAD_OUTFIT)
		IF IS_CHARACTER_CONTROLLER_PED_OK(characterData.characterCreatorPeds[characterData.eActiveChild])
			RELEASE_PED_PRELOAD_VARIATION_DATA(characterData.characterCreatorPeds[characterData.eActiveChild])
		ENDIF
	ELSE
		SET_CHARACTER_CONTROLLER_BIT(ciCHARACTER_CONTROLLER_PRELOAD_OUTFIT)
	ENDIF

ENDPROC

/// PURPOSE:
///    Randomise our style and outfit
PROC RANDOMISE_CHARACTER_CREATOR_STYLE_AND_OUTFIT(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, INT iFixedStyle = -1)
	
	RANDOMISE_CHARACTER_CREATOR_STYLE(characterData, iFixedStyle)
	RANDOMISE_CHARACTER_CREATOR_OUTFIT(characterData)
	
ENDPROC

/// PURPOSE:
///    Change the style of the player
PROC PROCESS_CHARACTER_STYLE_CHANGE(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, INT iIncrement)
	
	INT iMinimumStyle = 0
	INT iNewStyle = GET_CHARACTER_CREATOR_CHILD_STYLE(characterData, characterData.eActiveChild) + iIncrement
	
	BOOL bMatchingGender = FALSE
	IF GET_PACKED_STAT_INT(PACKED_CHAR_PICTURE, characterData.iCharacterSelectedSlot) = 0
		IF GET_CHARACTER_CREATOR_ACTIVE_PED_IS_MALE(characterData)
			bMatchingGender = TRUE
		ENDIF
	ELSE
		IF NOT GET_CHARACTER_CREATOR_ACTIVE_PED_IS_MALE(characterData)
			bMatchingGender = TRUE
		ENDIF
	ENDIF
	
	IF HAS_ENTERED_CHAR_CREATION_FROM_ALTERATION_PROMPT()
	AND bMatchingGender
		iMinimumStyle = ciCHARACTER_CREATOR_EXISTING_INDEX
	ENDIF
	
	IF iIncrement > 0
		WHILE iNewStyle <= ciCHARACTER_CREATOR_MAX_STYLES
		AND NOT IS_CHARACTER_CREATOR_STYLE_AVAILABLE(iNewStyle)
			iNewStyle++
		ENDWHILE
	ELSE
		WHILE iNewStyle >= iMinimumStyle
		AND NOT IS_CHARACTER_CREATOR_STYLE_AVAILABLE(iNewStyle)
			iNewStyle--
		ENDWHILE
	ENDIF
	
	IF iNewStyle < iMinimumStyle
		iNewStyle = ciCHARACTER_CREATOR_MAX_STYLES-1
		WHILE NOT IS_CHARACTER_CREATOR_STYLE_AVAILABLE(iNewStyle)
			iNewStyle--
		ENDWHILE
	ENDIF
	
	IF iNewStyle >= ciCHARACTER_CREATOR_MAX_STYLES
		iNewStyle = iMinimumStyle
	ENDIF
	
	CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_STYLE_CHANGE - Changing style to ", iNewStyle, ", slot = ", characterData.iCharacterSelectedSlot, ", bMatchingGender = ", PICK_STRING(bMatchingGender, "TRUE", "FALSE"))
	
	// Set style and first outfit in style
	SET_CHARACTER_CREATOR_CHILD_STYLE(characterData, characterData.eActiveChild, iNewStyle)
	SET_CHARACTER_CREATOR_CHILD_OUTFIT(characterData, characterData.eActiveChild, 0)
	
	PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
	
	// Release if in middle of preload - else apply outfit 0 in style
	IF IS_CHARACTER_CONTROLLER_BIT_SET(ciCHARACTER_CONTROLLER_PRELOAD_OUTFIT)
		IF IS_CHARACTER_CONTROLLER_PED_OK(characterData.characterCreatorPeds[characterData.eActiveChild])
			RELEASE_PED_PRELOAD_VARIATION_DATA(characterData.characterCreatorPeds[characterData.eActiveChild])
		ENDIF
	ELSE
		SET_CHARACTER_CONTROLLER_BIT(ciCHARACTER_CONTROLLER_PRELOAD_OUTFIT)
	ENDIF
ENDPROC

/// PURPOSE:
///    Change the outfit of the player
PROC PROCESS_CHARACTER_OUTFIT_CHANGE(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, INT iIncrement)
	
	INT iNewOutfit = GET_CHARACTER_CREATOR_CHILD_OUTFIT(characterData, characterData.eActiveChild) + iIncrement
	INT iMaxOutfits = GET_CHARACTER_CREATOR_MAX_OUTFITS_FOR_STYLE(GET_CHARACTER_CREATOR_CHILD_STYLE(characterData, characterData.eActiveChild), GET_CHARACTER_CREATOR_ACTIVE_PED_IS_MALE(characterData))
	
	IF iNewOutfit < 0
		iNewOutfit = iMaxOutfits-1
	ENDIF
	
	IF iNewOutfit >= iMaxOutfits
		iNewOutfit = 0
	ENDIF
	
	CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_OUTFIT_CHANGE - Changing outfit to ", iNewOutfit)
	
	SET_CHARACTER_CREATOR_CHILD_OUTFIT(characterData, characterData.eActiveChild, iNewOutfit)
	
	// Release if in middle of preload - else apply outfit
	IF IS_CHARACTER_CONTROLLER_BIT_SET(ciCHARACTER_CONTROLLER_PRELOAD_OUTFIT)
		IF IS_CHARACTER_CONTROLLER_PED_OK(characterData.characterCreatorPeds[characterData.eActiveChild])
			RELEASE_PED_PRELOAD_VARIATION_DATA(characterData.characterCreatorPeds[characterData.eActiveChild])
		ENDIF
	ELSE
		SET_CHARACTER_CONTROLLER_BIT(ciCHARACTER_CONTROLLER_PRELOAD_OUTFIT)
	ENDIF
	
	PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
ENDPROC

/// PURPOSE:
///    Checks the peds hat/hair compatibility  
FUNC BOOL CHARACTER_CREATOR_IS_HAT_COMPATIBLE_WITH_HAIR(PED_COMP_NAME_ENUM &eCurrentHat)
	
	INT iHat = ENUM_TO_INT(eCurrentHat)
	
	// Compatible hats
	IF iHat = -1
	OR iHat = ENUM_TO_INT(PROPS_FMF_HAT_6_0)
	OR iHat = ENUM_TO_INT(PROPS_FMF_HAT_6_1)
	OR iHat = ENUM_TO_INT(PROPS_FMF_HAT_6_2)
	OR iHat = ENUM_TO_INT(PROPS_FMF_HAT_6_3)
	OR iHat = ENUM_TO_INT(PROPS_FMF_HAT_6_4)
	OR iHat = ENUM_TO_INT(PROPS_FMF_HAT_6_5)
	OR iHat = ENUM_TO_INT(PROPS_FMF_HAT_6_6)
	OR iHat = ENUM_TO_INT(PROPS_FMF_HAT_6_7)
	OR iHat = ENUM_TO_INT(PROPS_FMF_HAT_10_0)
	OR iHat = ENUM_TO_INT(PROPS_FMF_HAT_10_1)
	OR iHat = ENUM_TO_INT(PROPS_FMF_HAT_10_2)
	OR iHat = ENUM_TO_INT(PROPS_FMF_HAT_10_3)
	OR iHat = ENUM_TO_INT(PROPS_FMF_HAT_10_4)
	OR iHat = ENUM_TO_INT(PROPS_FMF_HAT_10_5)
	OR iHat = ENUM_TO_INT(PROPS_FMF_HAT_10_6)
	OR iHat = ENUM_TO_INT(PROPS_FMF_HAT_10_7)
	OR iHat = ENUM_TO_INT(PROPS_FMF_HAT_14_0)
	OR iHat = ENUM_TO_INT(PROPS_FMF_HAT_14_1)
	OR iHat = ENUM_TO_INT(PROPS_FMF_HAT_14_2)
	OR iHat = ENUM_TO_INT(PROPS_FMF_HAT_14_3)
	OR iHat = ENUM_TO_INT(PROPS_FMF_HAT_14_4)
	OR iHat = ENUM_TO_INT(PROPS_FMF_HAT_14_5)
	OR iHat = ENUM_TO_INT(PROPS_FMF_HAT_14_6)
	OR iHat = ENUM_TO_INT(PROPS_FMF_HAT_14_7)
	OR iHat = ENUM_TO_INT(PROPS_FMF_HAT_15_0)
	OR iHat = ENUM_TO_INT(PROPS_FMF_HAT_15_1)
	OR iHat = ENUM_TO_INT(PROPS_FMF_HAT_15_2)
	OR iHat = ENUM_TO_INT(PROPS_FMF_HAT_15_3)
	OR iHat = ENUM_TO_INT(PROPS_FMF_HAT_15_4)
	OR iHat = ENUM_TO_INT(PROPS_FMF_HAT_15_5)
	OR iHat = ENUM_TO_INT(PROPS_FMF_HAT_15_6)
	OR iHat = ENUM_TO_INT(PROPS_FMF_HAT_15_7)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Finds the next compatible hat to wear with restricting hair 
FUNC INT CHARACTER_CREATOR_FIND_NEXT_COMPATIBLE_HAT(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, INT &iNewHat, PED_COMP_NAME_ENUM &eCurrentHat, INT iMaxHat)
	
	INT iLoopIndex
	INT iOriginalHat
	
	// Loop down available hat index
	IF iNewHat < characterData.iHatIndex
	
		WHILE iNewHat != -1
			iNewHat--
			eCurrentHat = GET_CHARACTER_CREATOR_HAT_ENUM(characterData.characterCreatorPeds[characterData.eActiveChild], iNewHat)
			
			IF CHARACTER_CREATOR_IS_HAT_COMPATIBLE_WITH_HAIR(eCurrentHat)
				RETURN iNewHat
			ENDIF
			
			// Loop to max hat index & continue searching
			IF iNewHat = -1
				iLoopIndex = iMaxHat
				WHILE iLoopIndex != iOriginalHat
					iLoopIndex--
					eCurrentHat = GET_CHARACTER_CREATOR_HAT_ENUM(characterData.characterCreatorPeds[characterData.eActiveChild], iLoopIndex)
					
					IF CHARACTER_CREATOR_IS_HAT_COMPATIBLE_WITH_HAIR(eCurrentHat)
						RETURN iLoopIndex
					ENDIF
				ENDWHILE
			ENDIF
		ENDWHILE
	// Loop up available hat index
	ELIF iNewHat > characterData.iHatIndex
		
		WHILE iNewHat != iMaxHat
			iNewHat++
			eCurrentHat = GET_CHARACTER_CREATOR_HAT_ENUM(characterData.characterCreatorPeds[characterData.eActiveChild], iNewHat)
			
			IF CHARACTER_CREATOR_IS_HAT_COMPATIBLE_WITH_HAIR(eCurrentHat)
				RETURN iNewHat
			ENDIF
			
			// Loop to min hat index & continue searching
			IF iNewHat = iMaxHat
				iLoopIndex = -1
				WHILE iLoopIndex != iOriginalHat
					iLoopIndex++
					eCurrentHat = GET_CHARACTER_CREATOR_HAT_ENUM(characterData.characterCreatorPeds[characterData.eActiveChild], iLoopIndex)
			
					IF CHARACTER_CREATOR_IS_HAT_COMPATIBLE_WITH_HAIR(eCurrentHat)
						RETURN iLoopIndex
					ENDIF
				ENDWHILE
			ENDIF
		ENDWHILE
	ENDIF
	
	RETURN -1
ENDFUNC

/// PURPOSE:
///    Randomise our character hat
PROC RANDOMISE_CHARCTER_CREATOR_HAT(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData)
	
	INT iMaxHat = PICK_INT(GET_CHARACTER_CREATOR_ACTIVE_PED_IS_MALE(characterData), ciCHARACTER_CREATOR_MAX_HATS_M, ciCHARACTER_CREATOR_MAX_HATS_F) + 1
	INT iRandomHat = GET_RANDOM_INT_IN_RANGE(0, iMaxHat) - 1

	// Check hat/hair compatibility
//	PED_COMP_NAME_ENUM eCurrentHair = GET_CHARACTER_CREATOR_HAIR_ENUM(characterData, characterData.eActiveChild)
//	PED_COMP_NAME_ENUM eCurrentHat = GET_CHARACTER_CREATOR_HAT_ENUM(characterData.characterCreatorPeds[characterData.eActiveChild], iRandomHat)
//
//	IF (eCurrentHair >= HAIR_FMF_11_0 AND eCurrentHair <= HAIR_FMF_11_6)
//	OR (eCurrentHair >= HAIR_FMF_4_0 AND eCurrentHair <= HAIR_FMF_4_5)
//		IF NOT CHARACTER_CREATOR_IS_HAT_COMPATIBLE_WITH_HAIR(eCurrentHat)
//			iRandomHat = CHARACTER_CREATOR_FIND_NEXT_COMPATIBLE_HAT(characterData, iRandomHat, eCurrentHat, iMaxHat)
//		ENDIF
//	ENDIF
	
	characterData.iHatIndex = iRandomHat
	
	SET_CHARACTER_CREATOR_CHILD_HAT(characterData, characterData.eActiveChild, iRandomHat)
ENDPROC

/// PURPOSE:
///    Change the hat of the character
PROC PROCESS_CHARACTER_HAT_CHANGE(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, INT iIncrement)
	
	BOOL bIsMale = GET_CHARACTER_CREATOR_ACTIVE_PED_IS_MALE(characterData)
	INT iMaxHat = PICK_INT(bIsMale, ciCHARACTER_CREATOR_MAX_HATS_M, ciCHARACTER_CREATOR_MAX_HATS_F)
	INT iNewHat = GET_CHARACTER_CREATOR_CHILD_HAT(characterData, characterData.eActiveChild) + iIncrement
	
	INT iMinV = -1
	IF HAS_ENTERED_CHAR_CREATION_FROM_ALTERATION_PROMPT()
		iMinV = ciCHARACTER_CREATOR_EXISTING_OVERLAY_INDEX
	ENDIF
	
	IF iNewHat < iMinV
		iNewHat = iMaxHat-1
	ENDIF
	
	IF iNewHat >= iMaxHat
		iNewHat = iMinV
	ENDIF
	
//	// Set to None - these hair styles are incompatible with all hats 
//	PED_COMP_NAME_ENUM eCurrentHair = GET_CHARACTER_CREATOR_HAIR_ENUM(characterData, characterData.eActiveChild)
//	PED_COMP_NAME_ENUM eCurrentHat = GET_CHARACTER_CREATOR_HAT_ENUM(characterData.characterCreatorPeds[characterData.eActiveChild], iNewHat)
//
//	IF (eCurrentHair >= HAIR_FMF_11_0 AND eCurrentHair <= HAIR_FMF_11_6)
//	OR (eCurrentHair >= HAIR_FMF_4_0 AND eCurrentHair <= HAIR_FMF_4_5)
//		IF NOT CHARACTER_CREATOR_IS_HAT_COMPATIBLE_WITH_HAIR(eCurrentHat)
//			iNewHat = CHARACTER_CREATOR_FIND_NEXT_COMPATIBLE_HAT(characterData, iNewHat, eCurrentHat, iMaxHat)
//		ENDIF
//	ENDIF
	
	characterData.iHatIndex = iNewHat
	
	SET_CHARACTER_CREATOR_CHILD_HAT(characterData, characterData.eActiveChild, iNewHat)
	
	PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
	
	APPLY_CHARACTER_CREATOR_HAT(characterData)
ENDPROC

/// PURPOSE:
///    Randomise our character glasses
PROC RANDOMISE_CHARCTER_CREATOR_GLASSES(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData)

	INT iMaxGlasses = PICK_INT(GET_CHARACTER_CREATOR_ACTIVE_PED_IS_MALE(characterData), ciCHARACTER_CREATOR_MAX_GLASSES_M, ciCHARACTER_CREATOR_MAX_GLASSES_F) + 1
	INT iRandomGlasses = GET_RANDOM_INT_IN_RANGE(0, iMaxGlasses) - 1

	SET_CHARACTER_CREATOR_CHILD_GLASSES(characterData, characterData.eActiveChild, iRandomGlasses)
ENDPROC

/// PURPOSE:
///    Change the glasses of the character
PROC PROCESS_CHARACTER_GLASSES_CHANGE(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, INT iIncrement)
	
	BOOL bIsMale = GET_CHARACTER_CREATOR_ACTIVE_PED_IS_MALE(characterData)
	INT iMaxGlasses = PICK_INT(bIsMale, ciCHARACTER_CREATOR_MAX_GLASSES_M, ciCHARACTER_CREATOR_MAX_GLASSES_F)
	INT iNewGlasses = GET_CHARACTER_CREATOR_CHILD_GLASSES(characterData, characterData.eActiveChild) + iIncrement
	
	INT iMinV = -1
	IF HAS_ENTERED_CHAR_CREATION_FROM_ALTERATION_PROMPT()
		iMinV = ciCHARACTER_CREATOR_EXISTING_OVERLAY_INDEX
	ENDIF
	
	IF iNewGlasses < iMinV
		iNewGlasses = iMaxGlasses-1
	ENDIF
	
	IF iNewGlasses >= iMaxGlasses
		iNewGlasses = iMinV
	ENDIF
	
	SET_CHARACTER_CREATOR_CHILD_GLASSES(characterData, characterData.eActiveChild, iNewGlasses)
	
	PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
	
	APPLY_CHARACTER_CREATOR_GLASSES(characterData)
ENDPROC

/// PURPOSE:
///    Randomises the crew tshirt that the character is wearing
PROC RANDOMMISE_CHARACTER_CREATOR_CREW_TSHIRT(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData)
	
	INT iNewCrewOption = GET_RANDOM_INT_IN_RANGE(0, ENUM_TO_INT(CREW_TSHIRT_COLLAR_GRAY)+1)
	SET_CHARACTER_CREATOR_CREW_TSHIRT(characterData, INT_TO_ENUM(CHAR_CREATOR_CREW_TSHIRT_OPTION_ENUM, iNewCrewOption))
	
ENDPROC


/// PURPOSE:
///    Change the crew tshirt setting on the player
PROC PROCESS_CHARACTER_CREW_TSHIRT_CHANGE(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, INT iIncrement)
	
	INT iNewCrewOption = ENUM_TO_INT(GET_CHARACTER_CREATOR_CREW_TSHIRT(characterData)) + iIncrement
	
	IF iNewCrewOption < ENUM_TO_INT(CREW_TSHIRT_OFF)
		iNewCrewOption = ENUM_TO_INT(CREW_TSHIRT_COLLAR_GRAY)
	ENDIF
	
	IF iNewCrewOption > ENUM_TO_INT(CREW_TSHIRT_COLLAR_GRAY)
		iNewCrewOption = 0
	ENDIF
	
	SET_CHARACTER_CREATOR_CREW_TSHIRT(characterData, INT_TO_ENUM(CHAR_CREATOR_CREW_TSHIRT_OPTION_ENUM, iNewCrewOption))
	
	PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
	
	// Release if in middle of preload - else apply crew tshirt to outfit
	IF IS_CHARACTER_CONTROLLER_BIT_SET(ciCHARACTER_CONTROLLER_PRELOAD_OUTFIT)
		IF IS_CHARACTER_CONTROLLER_PED_OK(characterData.characterCreatorPeds[characterData.eActiveChild])
			RELEASE_PED_PRELOAD_VARIATION_DATA(characterData.characterCreatorPeds[characterData.eActiveChild])
		ENDIF
	ELSE
		SET_CHARACTER_CONTROLLER_BIT(ciCHARACTER_CONTROLLER_PRELOAD_OUTFIT)
	ENDIF
ENDPROC

/// PURPOSE:
///    Randomises the crew tshirt decal
PROC RANDOMISE_CHARACTER_CREATOR_CREW_TSHIRT_DECAL(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData)

	INT iNewCrewOption = GET_RANDOM_INT_IN_RANGE(0, ENUM_TO_INT(CC_CREW_DECAL_BACK)+1)
	SET_CHARACTER_CREATOR_CREW_TSHIRT_DECAL(characterData, INT_TO_ENUM(CHARACTER_CREATOR_CREW_TSHIRT_DECAL, iNewCrewOption))

ENDPROC

/// PURPOSE:
///    Change the decal on the crew tshirt
PROC PROCESS_CHARACTER_CREATOR_TSHIRT_DECAL_CHANGE(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, INT iIncrement)

	INT iNewCrewOption = ENUM_TO_INT(GET_CHARACTER_CREATOR_CREW_TSHIRT_DECAL(characterData)) + iIncrement
	INT iMaxVal = ENUM_TO_INT(CC_CREW_DECAL_BACK)
	
	// Females can't have the 'back' crew logo
	IF NOT GET_CHARACTER_CREATOR_ACTIVE_PED_IS_MALE(characterData)
		iMaxVal = ENUM_TO_INT(CC_CREW_DECAL_SMALL)
	ENDIF
	
	IF iNewCrewOption < ENUM_TO_INT(CC_CREW_DECAL_NONE)
		iNewCrewOption = iMaxVal
	ENDIF
	
	IF iNewCrewOption > iMaxVal
		iNewCrewOption = 0
	ENDIF
	
	SET_CHARACTER_CREATOR_CREW_TSHIRT_DECAL(characterData, INT_TO_ENUM(CHARACTER_CREATOR_CREW_TSHIRT_DECAL, iNewCrewOption))
	
	PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
	
	APPLY_CHARACTER_CREATOR_CREW_TSHIRT_DECAL(characterData)
ENDPROC

/// PURPOSE:
///    Handles a change to the hair style
PROC PROCESS_CHARACTER_HAIR_CHANGE(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, INT iIncrement)

	INT iHairIndex = ciCHARACTER_CREATOR_APPEARANCE_HAIR_STYLE_F
	IF GET_CHARACTER_CREATOR_ACTIVE_PED_IS_MALE(characterData)
		iHairIndex = ciCHARACTER_CREATOR_APPEARANCE_HAIR_STYLE_M
	ENDIF
	
	CHARACTER_CREATOR_APPEARANCE_DATA appearanceData
	GET_CHARACTER_CREATOR_APPEARANCE_DATA(appearanceData, iHairIndex)
	
	CHARACTER_CREATOR_APPEARANCE_VALUES appearanceValues
	GET_CHARACTER_CREATOR_APPEARANCE_VALUES(characterData, appearanceValues, iHairIndex)
	
	INT iNewHairStyle = appearanceValues.iValue + iIncrement
	INT iMinimumHair = 0
	
	IF HAS_ENTERED_CHAR_CREATION_FROM_ALTERATION_PROMPT()
		iMinimumHair = -1
	ENDIF	
	
	IF iNewHairStyle < iMinimumHair
		iNewHairStyle = ciCHARACTER_CREATOR_MAX_NUMBER_OF_HAIR-1
	ENDIF
	
	IF iNewHairStyle >= ciCHARACTER_CREATOR_MAX_NUMBER_OF_HAIR
		iNewHairStyle = iMinimumHair
	ENDIF
	
	// url:bugstar:2472289
	IF GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_IS_NG_VERSION, characterData.iCharacterSelectedSlot) >= ciCHARACTER_CREATOR_CURRENT_VERSION
		IF iNewHairStyle = ciCHARACTER_CREATOR_EXISTING_INDEX
			appearanceValues.iColour = GET_MP_INT_CHARACTER_STAT(MP_STAT_HAIR_TINT, characterData.iCharacterSelectedSlot)
			appearanceValues.iColour2 = GET_MP_INT_CHARACTER_STAT(MP_STAT_SEC_HAIR_TINT, characterData.iCharacterSelectedSlot)
			appearanceValues.iColourHighlightValue = GET_CHARACTER_CREATOR_COLOUR_INDEX(appearanceData.overlaySlot, appearanceValues.iColour, appearanceData.iColourMax)
		ENDIF
	ENDIF
	
	CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_HAIR_CHANGE - Applying new hair style: ", iNewHairStyle)
	SET_CHARACTER_CREATOR_APPEARANCE_VALUES(characterData, iHairIndex, iNewHairStyle, 100, appearanceValues.iColour, appearanceValues.iColour2, appearanceValues.iColourHighlightValue)
		
	UPDATE_CHARACTER_CREATOR_SECONDARY_HAIR_COLOUR(characterData, characterData.eActiveChild)
	
	IF GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_IS_NG_VERSION, characterData.iCharacterSelectedSlot) >= ciCHARACTER_CREATOR_CURRENT_VERSION
		IF iNewHairStyle = ciCHARACTER_CREATOR_EXISTING_INDEX
			UPDATE_CHARACTER_CREATOR_HAIR_TO_MATCH_COLOURS(characterData)
			APPLY_CHARACTER_CREATOR_OVERLAY(characterData, FALSE)
		ENDIF
	ENDIF
	
	// Checks hat/hair compatibility
//	PED_COMP_NAME_ENUM eCurrentHair = GET_CHARACTER_CREATOR_HAIR_ENUM(characterData, characterData.eActiveChild)
//	PED_COMP_NAME_ENUM eCurrentHat = GET_CHARACTER_CREATOR_HAT_ENUM(characterData.characterCreatorPeds[characterData.eActiveChild], characterData.iHatIndex)
//
//	IF (eCurrentHair >= HAIR_FMF_11_0 AND eCurrentHair <= HAIR_FMF_11_6)
//	OR (eCurrentHair >= HAIR_FMF_4_0 AND eCurrentHair <= HAIR_FMF_4_5)
//		IF NOT CHARACTER_CREATOR_IS_HAT_COMPATIBLE_WITH_HAIR(eCurrentHat)
//			SET_CHARACTER_CREATOR_CHILD_HAT(characterData, characterData.eActiveChild, -1)
//		ENDIF
//	ELSE
//		SET_CHARACTER_CREATOR_CHILD_HAT(characterData, characterData.eActiveChild, characterData.iHatIndex)
//	ENDIF
	
	SET_CHARACTER_CREATOR_CHILD_HAT(characterData, characterData.eActiveChild, characterData.iHatIndex)
	
	APPLY_CHARACTER_CREATOR_HAT(characterData)
	
	PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
	
	IF IS_CHARACTER_CONTROLLER_BIT_SET(ciCHARACTER_CONTROLLER_PRELOAD_HAIR)
		IF IS_CHARACTER_CONTROLLER_PED_OK(characterData.characterCreatorPeds[characterData.eActiveChild])
			RELEASE_PED_PRELOAD_VARIATION_DATA(characterData.characterCreatorPeds[characterData.eActiveChild])
		ENDIF
	ELSE
		SET_CHARACTER_CONTROLLER_BIT(ciCHARACTER_CONTROLLER_PRELOAD_HAIR)
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles a change to the hair colour
FUNC INT PROCESS_CHARACTER_HAIR_COLOUR_CHANGE(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, INT iIncrement, BOOL bMouseSelect = FALSE)
	
	INT iAppearanceIndex = ciCHARACTER_CREATOR_APPEARANCE_HAIR_STYLE_F
	IF GET_CHARACTER_CREATOR_ACTIVE_PED_IS_MALE(characterData)
		iAppearanceIndex = ciCHARACTER_CREATOR_APPEARANCE_HAIR_STYLE_M
	ENDIF
		
	CHARACTER_CREATOR_APPEARANCE_DATA appearanceData
	CHARACTER_CREATOR_APPEARANCE_VALUES appearanceValues
	GET_CHARACTER_CREATOR_APPEARANCE_VALUES(characterData, appearanceValues, iAppearanceIndex)
	GET_CHARACTER_CREATOR_APPEARANCE_DATA(appearanceData, iAppearanceIndex)
		
	INT iNewHairColour = appearanceValues.iColour
		
	IF appearanceValues.iValue != ciCHARACTER_CREATOR_EXISTING_INDEX
	
		// Mouse selecting of colours
		IF bMouseSelect
			iNewHairColour =  GET_TINT_FROM_INDEX(appearanceData.overlaySlot, PAUSE_MENU_GET_HAIR_COLOUR_INDEX(), appearanceData.iColourMax)
			IF iNewHairColour = 0
				RETURN 0
			ENDIF
				
		// Gamepad selection
		ELSE
			BOOL bFound
			WHILE NOT bFound
				
				iNewHairColour += iIncrement
				
				IF iNewHairColour < appearanceData.iColourMin
					iNewHairColour = appearanceData.iColourMax-1
				ENDIF
				
				IF iNewHairColour >= appearanceData.iColourMax
					iNewHairColour = appearanceData.iColourMin
				ENDIF
				
				IF iNewHairColour = appearanceValues.iColour
				OR IS_PED_HAIR_TINT_FOR_CREATOR(iNewHairColour)
				OR appearanceData.iColourMax = 0
					bFound = TRUE
				ENDIF
			ENDWHILE
		
		ENDIF
			
		INT iHighlight = GET_CHARACTER_CREATOR_COLOUR_INDEX(appearanceData.overlaySlot, iNewHairColour, appearanceData.iColourMax)
	
		CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_HAIR_COLOUR_CHANGE - Applying new hair colour: ", iNewHairColour, " with Highlight: ", iHighlight)
		SET_CHARACTER_CREATOR_APPEARANCE_VALUES(characterData, iAppearanceIndex, appearanceValues.iValue, appearanceValues.fOpacity, iNewHairColour, iNewHairColour, iHighlight)
		
		UPDATE_CHARACTER_CREATOR_SECONDARY_HAIR_COLOUR(characterData, characterData.eActiveChild)
		
		IF GET_CHARACTER_CREATOR_MENU_STATE(characterData) != ciCHARACTER_CREATOR_COLUMN_MAIN_MENU
			PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
		ENDIF
		
		APPLY_CHARACTER_CREATOR_HAIR(characterData, characterData.eActiveChild)
	ENDIF
	
	// Update our other hair to match the colour
	UPDATE_CHARACTER_CREATOR_HAIR_TO_MATCH_COLOURS(characterData)
	
	// Update these overlays now we have a new colour
	APPLY_CHARACTER_CREATOR_OVERLAY(characterData, FALSE)
	
	RETURN iNewHairColour
ENDFUNC

/// PURPOSE:
///    Randomises the hair style and colour of our peds
PROC RANDOMISE_CHARACTER_HAIR_AND_HAIR_COLOUR(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, BOOL bUseDefault = FALSE, BOOL bFixedGender = FALSE, CHARACTER_CREATOR_PED aPedType = CHARACTER_CREATOR_SON)

	INT iRandomHair 
	INT iRandomColour
	CHARACTER_CREATOR_APPEARANCE_DATA appearanceData
	INT iHighlight
	BOOL bFound
	BOOL bScrollToValid
	INT iCount
	INT iInitialTint
	
	IF NOT bFixedGender
	OR aPedType = CHARACTER_CREATOR_SON
		
		// Randomise the son's haircut
		iRandomHair = GET_RANDOM_INT_IN_RANGE(0, ciCHARACTER_CREATOR_MAX_NUMBER_OF_HAIR)
		
		IF bUseDefault
			iRandomHair = 1		// Buzzcut
		ENDIF
		
		GET_CHARACTER_CREATOR_APPEARANCE_DATA(appearanceData, ciCHARACTER_CREATOR_APPEARANCE_HAIR_STYLE_M)
		
		CPRINTLN(DEBUG_NET_CHARACTER, "RANDOMISE_CHARACTER_HAIR_AND_HAIR_COLOUR - Randomise Male hair: Max hair colours = ", GET_NUM_PED_HAIR_TINTS())
		
		iInitialTint = GET_RANDOM_INT_IN_RANGE(0, appearanceData.iColourMax) 
		iRandomColour = iInitialTint
		WHILE NOT bFound
			
			// Safety catch if we haven't found a valid colour in N attempts
			IF bScrollToValid
				iRandomColour++
				
				IF iRandomColour >= appearanceData.iColourMax
					iRandomColour = 0
				ENDIF
			ELSE
				// Randomly pick
				iRandomColour = GET_RANDOM_INT_IN_RANGE(0, appearanceData.iColourMax) 
			ENDIF
			
			// Check if they are valid
			IF IS_PED_HAIR_TINT_FOR_CREATOR(iRandomColour)
				bFound = TRUE
			ELSE
				// If we have completely looped then we don't have any valid colours so bail
				IF bScrollToValid
				AND iRandomColour = iInitialTint
					bFound = TRUE
					iRandomColour = 0
				ENDIF
			ENDIF
			
			iCount++
			IF NOT bScrollToValid
			AND iCount > appearanceData.iColourMax
				bScrollToValid = TRUE
				iInitialTint = iRandomColour
			ENDIF
		ENDWHILE
		
		iHighlight = GET_CHARACTER_CREATOR_COLOUR_INDEX(appearanceData.overlaySlot, iRandomColour, appearanceData.iColourMax)
		CPRINTLN(DEBUG_NET_CHARACTER, "RANDOMISE_CHARACTER_HAIR_AND_HAIR_COLOUR - Randomise Male hair: Style = ", iRandomHair, ", Colour = ", iRandomColour)
		
		SET_CHARACTER_CREATOR_APPEARANCE_VALUES(characterData, ciCHARACTER_CREATOR_APPEARANCE_HAIR_STYLE_M, iRandomHair, 100, iRandomColour, iRandomColour, iHighlight)
		UPDATE_CHARACTER_CREATOR_HAIR_TO_MATCH_COLOURS(characterData)
		
		UPDATE_CHARACTER_CREATOR_SECONDARY_HAIR_COLOUR(characterData, CHARACTER_CREATOR_SON)
		
		IF NOT IS_CHARACTER_CONTROLLER_BIT_SET(ciCHARACTER_CONTROLLER_RANDOMISE_ALL)
			APPLY_CHARACTER_CREATOR_OVERLAY(characterData, FALSE) 
		ELSE
			//Release in the middle of a preload
			IF IS_CHARACTER_CONTROLLER_BIT_SET(ciCHARACTER_CONTROLLER_PRELOAD_HAIR)
				IF IS_CHARACTER_CONTROLLER_PED_OK(characterData.characterCreatorPeds[characterData.eActiveChild])
					RELEASE_PED_PRELOAD_VARIATION_DATA(characterData.characterCreatorPeds[characterData.eActiveChild])
				ENDIF
			ELSE
				SET_CHARACTER_CONTROLLER_BIT(ciCHARACTER_CONTROLLER_PRELOAD_HAIR)
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT bFixedGender
	OR aPedType = CHARACTER_CREATOR_DAUGHTER
	
		// Do the same for the daughter
		iRandomHair = GET_RANDOM_INT_IN_RANGE(0, ciCHARACTER_CREATOR_MAX_NUMBER_OF_HAIR)
		
		IF bUseDefault
			iRandomHair = 11	// Loose tied
		ENDIF
		
		GET_CHARACTER_CREATOR_APPEARANCE_DATA(appearanceData, ciCHARACTER_CREATOR_APPEARANCE_HAIR_STYLE_F)
		
		CPRINTLN(DEBUG_NET_CHARACTER, "RANDOMISE_CHARACTER_HAIR_AND_HAIR_COLOUR - Randomise Male hair: Max hair colours = ", GET_NUM_PED_HAIR_TINTS())
		
		iCount = 0
		bFound = FALSE
		bScrollToValid = FALSE
		
		iInitialTint = GET_RANDOM_INT_IN_RANGE(0, appearanceData.iColourMax) 
		iRandomColour = iInitialTint
		WHILE NOT bFound
			
			IF bScrollToValid
				iRandomColour++
				
				IF iRandomColour >= appearanceData.iColourMax
					iRandomColour = 0
				ENDIF
			ELSE
				iRandomColour = GET_RANDOM_INT_IN_RANGE(0, appearanceData.iColourMax) 
			ENDIF
			
			IF IS_PED_HAIR_TINT_FOR_CREATOR(iRandomColour)
				bFound = TRUE
			ELSE
				IF bScrollToValid
				AND iRandomColour = iInitialTint
					bFound = TRUE
					iRandomColour = 0
				ENDIF
			ENDIF
			
			iCount++
			IF NOT bScrollToValid
			AND iCount > appearanceData.iColourMax
				bScrollToValid = TRUE
				iInitialTint = iRandomColour
			ENDIF
		ENDWHILE
		
		iHighlight = GET_CHARACTER_CREATOR_COLOUR_INDEX(appearanceData.overlaySlot, iRandomColour, appearanceData.iColourMax)
		
		SET_CHARACTER_CREATOR_APPEARANCE_VALUES(characterData, ciCHARACTER_CREATOR_APPEARANCE_HAIR_STYLE_F, iRandomHair, 100, iRandomColour, iRandomColour, iHighlight)
		
		UPDATE_CHARACTER_CREATOR_SECONDARY_HAIR_COLOUR(characterData, CHARACTER_CREATOR_DAUGHTER)
		
		CPRINTLN(DEBUG_NET_CHARACTER, "RANDOMISE_CHARACTER_HAIR_AND_HAIR_COLOUR - Randomise Female hair: Style = ", iRandomHair, ", Colour = ", iRandomColour)
		
		UPDATE_CHARACTER_CREATOR_HAIR_TO_MATCH_COLOURS(characterData)
		
		// Checks hat/hair compatibility
//		PED_COMP_NAME_ENUM eCurrentHair = GET_CHARACTER_CREATOR_HAIR_ENUM(characterData, characterData.eActiveChild)
//		PED_COMP_NAME_ENUM eCurrentHat = GET_CHARACTER_CREATOR_HAT_ENUM(characterData.characterCreatorPeds[characterData.eActiveChild], characterData.iHatIndex)
//
//		IF (eCurrentHair >= HAIR_FMF_11_0 AND eCurrentHair <= HAIR_FMF_11_6)
//		OR (eCurrentHair >= HAIR_FMF_4_0 AND eCurrentHair <= HAIR_FMF_4_5)
//			IF NOT CHARACTER_CREATOR_IS_HAT_COMPATIBLE_WITH_HAIR(eCurrentHat)
//				SET_CHARACTER_CREATOR_CHILD_HAT(characterData, characterData.eActiveChild, -1)
//			ENDIF
//		ELSE
//			SET_CHARACTER_CREATOR_CHILD_HAT(characterData, characterData.eActiveChild, characterData.iHatIndex)
//		ENDIF

		SET_CHARACTER_CREATOR_CHILD_HAT(characterData, characterData.eActiveChild, characterData.iHatIndex)

		//APPLY_CHARACTER_CREATOR_HAT(characterData)
		
		IF NOT IS_CHARACTER_CONTROLLER_BIT_SET(ciCHARACTER_CONTROLLER_RANDOMISE_ALL)
			IF NOT bUseDefault
				APPLY_CHARACTER_CREATOR_OVERLAY(characterData, FALSE)
			ENDIF
		ELSE
			//Release in the middle of a preload
			IF IS_CHARACTER_CONTROLLER_BIT_SET(ciCHARACTER_CONTROLLER_PRELOAD_HAIR)
				IF IS_CHARACTER_CONTROLLER_PED_OK(characterData.characterCreatorPeds[characterData.eActiveChild])
					RELEASE_PED_PRELOAD_VARIATION_DATA(characterData.characterCreatorPeds[characterData.eActiveChild])
				ENDIF
			ELSE
				SET_CHARACTER_CONTROLLER_BIT(ciCHARACTER_CONTROLLER_PRELOAD_HAIR)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Sets the hats to initial values
PROC INITIALISE_CHARACTER_CREATOR_HATS(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData)

	IF HAS_ENTERED_CHAR_CREATION_FROM_ALTERATION_PROMPT()
		SET_CHARACTER_CREATOR_CHILD_HAT(characterData, CHARACTER_CREATOR_DAUGHTER, -2)
		SET_CHARACTER_CREATOR_CHILD_HAT(characterData, CHARACTER_CREATOR_SON, -2)	
	ELSE
		SET_CHARACTER_CREATOR_CHILD_HAT(characterData, CHARACTER_CREATOR_DAUGHTER, -1)
		SET_CHARACTER_CREATOR_CHILD_HAT(characterData, CHARACTER_CREATOR_SON, -1)	
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets the glasses to initial values
PROC INITIALISE_CHARACTER_CREATOR_GLASSES(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData)

	IF HAS_ENTERED_CHAR_CREATION_FROM_ALTERATION_PROMPT()
		SET_CHARACTER_CREATOR_CHILD_GLASSES(characterData, CHARACTER_CREATOR_DAUGHTER, -2)
		SET_CHARACTER_CREATOR_CHILD_GLASSES(characterData, CHARACTER_CREATOR_SON, -2)
	ELSE
		SET_CHARACTER_CREATOR_CHILD_GLASSES(characterData, CHARACTER_CREATOR_DAUGHTER, -1)
		SET_CHARACTER_CREATOR_CHILD_GLASSES(characterData, CHARACTER_CREATOR_SON, -1)	
	ENDIF
ENDPROC

/// PURPOSE:
///    Hardcoded return of the eyebrows we allow characters
FUNC INT GET_CHARACTER_CREATOR_RANDOM_EYEBROW(INT i)
	IF i = ciCHARACTER_CREATOR_APPEARANCE_EYEBROWS_F
		RETURN GET_RANDOM_INT_IN_RANGE(1, 13)
	ELSE
		RETURN GET_RANDOM_INT_IN_RANGE(13, 28)
	ENDIF
ENDFUNC


/// PURPOSE:
///    Initialise the feature values
PROC INITIALISE_CHARACTER_CREATOR_FEATURES(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData)

	INT i
	CHARACTER_CREATOR_FEATURE_DATA featureData
	CHARACTER_CREATOR_FEATURE_PRESET_DATA featurePresetData
	
	MP_FLOAT_STATS overlayStatBlend
	
	INT iPreset
	
	REPEAT ciCHARACTER_CREATOR_MAX_FEATURES i
		
		GET_CHARACTER_CREATOR_FEATURE_DATA(featureData, i)
		GET_CHARACTER_CREATOR_FEATURE_PRESET_DATA(featurePresetData, i, 0)
	
		// If we have entered via alteration and the character is a new version, update all features
		IF HAS_ENTERED_CHAR_CREATION_FROM_ALTERATION_PROMPT()
		AND GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_IS_NG_VERSION, characterData.iCharacterSelectedSlot) >= ciCHARACTER_CREATOR_CURRENT_VERSION
							
			// Restore the values
			iPreset = ciCHARACTER_CREATOR_EXISTING_INDEX
			IF featureData.bXAxisActive
				overlayStatBlend = GET_CHARACTER_FEATURE_BLEND_STAT(featureData.morphXAxis)
				
				IF overlayStatBlend != INT_TO_ENUM(MP_FLOAT_STATS, -1)
					featurePresetData.fXBlend = GET_CHARACTER_CREATOR_PERCENTAGE_FROM_BLEND(GET_MP_FLOAT_CHARACTER_STAT(overlayStatBlend, characterData.iCharacterSelectedSlot), featureData.fXMax, featureData.fXMin)
				ENDIF
			ENDIF
			
			IF featureData.bYAxisActive
				overlayStatBlend = GET_CHARACTER_FEATURE_BLEND_STAT(featureData.morphYAxis)
				
				IF overlayStatBlend != INT_TO_ENUM(MP_FLOAT_STATS, -1)
					featurePresetData.fYBlend = GET_CHARACTER_CREATOR_PERCENTAGE_FROM_BLEND(GET_MP_FLOAT_CHARACTER_STAT(overlayStatBlend, characterData.iCharacterSelectedSlot), featureData.fYMax, featureData.fYMin)
				ENDIF
			ENDIF
			
			CPRINTLN(DEBUG_NET_CHARACTER, "INITIALISE_CHARACTER_CREATOR_FEATURES - From stats. Feature: ", i, ", xVal = ", featurePresetData.fXBlend, ", yVal = ", featurePresetData.fYBlend)
			SET_CHARACTER_CREATOR_FEATURE_VALUES(characterData, i, iPreset, featurePresetData.fXBlend, featurePresetData.fYBlend)
		ELSE
			SET_CHARACTER_CREATOR_FEATURE_VALUES(characterData, i, 0, featurePresetData.fXBlend, featurePresetData.fYBlend)
		ENDIF
	ENDREPEAT
	
ENDPROC

/// PURPOSE:
///    For a migrated character we have to clear some overlays as they can not be brought across
FUNC BOOL SHOULD_CHARACTER_CREATOR_OVERLAY_BE_CLEARED(INT iAppearance)

	SWITCH iAppearance
		// All makeup can't be brought across or the damage / cuts as this is now skin complexion
		CASE ciCHARACTER_CREATOR_APPEARANCE_EYE_MAKEUP
		CASE ciCHARACTER_CREATOR_APPEARANCE_EYE_MAKEUP_M
		CASE ciCHARACTER_CREATOR_APPEARANCE_LIPSTICK
		CASE ciCHARACTER_CREATOR_APPEARANCE_LIPSTICK_M
		CASE ciCHARACTER_CREATOR_APPEARANCE_BLUSHER
		CASE ciCHARACTER_CREATOR_APPEARANCE_SKIN_COMPLEXION
		CASE ciCHARACTER_CREATOR_APPEARANCE_SKIN_BLEMISHES
		CASE ciCHARACTER_CREATOR_APPEARANCE_MOLE_FRECKLES
		CASE ciCHARACTER_CREATOR_APPEARANCE_SKIN_AGING
		CASE ciCHARACTER_CREATOR_APPEARANCE_SUN_DAMAGE
			RETURN TRUE
	ENDSWITCH

	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Initialise our overlay values
PROC INITIALISE_CHARACTER_CREATOR_OVERLAYS(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData)
	
	INT i
	CHARACTER_CREATOR_APPEARANCE_DATA appearanceData
	CHARACTER_CREATOR_FEATURE_GENDER aGender = CHARACTER_CREATOR_GENDER_FEMALE
	CHARACTER_CREATOR_FEATURE_GENDER aSecondaryGender = CHARACTER_CREATOR_GENDER_FEMALE_HIDE
	
	IF GET_CHARACTER_CREATOR_ACTIVE_PED_IS_MALE(characterData)
		aGender = CHARACTER_CREATOR_GENDER_MALE
		aSecondaryGender = CHARACTER_CREATOR_GENDER_MALE_HIDE
	ENDIF
	
	STATS_PACKED overlayStat
	MP_FLOAT_STATS overlayStatBlend
	MP_INT_STATS overlayStatColour
	RAMP_TYPE rtType
	
	INT iValue
	FLOAT fOpacity
	INT iColour
	INT iHighlight
	
	REPEAT ciCHARACTER_CREATOR_MAX_APPEARANCE i
		
		GET_CHARACTER_CREATOR_APPEARANCE_DATA(appearanceData, i)
		
		// Only handle overlays
		IF appearanceData.bOverlayActive
		
			// If we have entered via alteration and the character is a new version, update all features
			IF HAS_ENTERED_CHAR_CREATION_FROM_ALTERATION_PROMPT()
			AND (appearanceData.iGenderSpecific = CHARACTER_CREATOR_GENDER_BOTH OR appearanceData.iGenderSpecific = aGender OR appearanceData.iGenderSpecific = aSecondaryGender)
						
				// Restore the values
				iValue = ciCHARACTER_CREATOR_EXISTING_INDEX
				
				overlayStat = GET_CHARACTER_OVERLAY_STAT(appearanceData.overlaySlot)
				overlayStatBlend = GET_CHARACTER_OVERLAY_BLEND_STAT(appearanceData.overlaySlot)
				
				IF overlayStat != INT_TO_ENUM(STATS_PACKED, -1)
					iValue = GET_PACKED_STAT_INT(overlayStat, characterData.iCharacterSelectedSlot)
					
					// Catch our unsigned int to default back to -1 (NONE)
					IF iValue = ciCHARACTER_CREATOR_UNSIGNED_INT_CHECK
						iValue = ciCHARACTER_CREATOR_EXISTING_INDEX
					ENDIF
				ENDIF
				
				IF overlayStatBlend != INT_TO_ENUM(MP_FLOAT_STATS, -1)
					fOpacity = GET_MP_FLOAT_CHARACTER_STAT(overlayStatBlend, characterData.iCharacterSelectedSlot) * 100
				ENDIF
				
				// If we are a migrated character, check we need to clear the overlay
				IF GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_IS_NG_VERSION, characterData.iCharacterSelectedSlot) < ciCHARACTER_CREATOR_CURRENT_VERSION
				#IF IS_DEBUG_BUILD
				OR characterData.bDebugLastGenCharacter
				#ENDIF
					IF SHOULD_CHARACTER_CREATOR_OVERLAY_BE_CLEARED(i)
						CPRINTLN(DEBUG_NET_CHARACTER, "INITIALISE_CHARACTER_CREATOR_OVERLAYS - Clear the following overlay for migrated character Overlay: ", i, ", ", GET_CHARACTER_OVERLAY_TEXT(appearanceData.overlaySlot))
						iValue = -1
						fOpacity = appearanceData.fDefaultOpacity
						
						// For the following overlays, default to current
						IF i = ciCHARACTER_CREATOR_APPEARANCE_EYE_MAKEUP
						OR i = ciCHARACTER_CREATOR_APPEARANCE_EYE_MAKEUP_M
							iValue = ciCHARACTER_CREATOR_EXISTING_OVERLAY_INDEX
						ENDIF
					ENDIF
					
					IF i = ciCHARACTER_CREATOR_APPEARANCE_FACIAL_HAIR
						CPRINTLN(DEBUG_NET_CHARACTER, "INITIALISE_CHARACTER_CREATOR_OVERLAYS - Facial Hair Overlay: ", i, ", iValue = ", iValue)

						// NOTE: Hardcoded subtraction of 7 because we removed grey beards from last to next gen.
						IF iValue >= 27
							iValue -= 7
							CPRINTLN(DEBUG_NET_CHARACTER, "INITIALISE_CHARACTER_CREATOR_OVERLAYS - Greater than the Grey Beards. Adjust to iValue = ", iValue)
						ENDIF
					ENDIF					
				ENDIF
				
				iHighlight = 0
				// Restore the colour aswell
				IF appearanceData.bColourActive
					overlayStatColour = GET_CHARACTER_OVERLAY_COLOUR_STAT(appearanceData.overlaySlot)
					IF overlayStatColour != INT_TO_ENUM(MP_INT_STATS, -1)
						// Colour and ramp type are now packed in the stat
						UNPACK_CHARACTER_OVERLAY_TINT_VALUES(GET_MP_INT_CHARACTER_STAT(overlayStatColour, characterData.iCharacterSelectedSlot), iColour, rtType)
					ELSE
						iColour = 0
					ENDIF
					
					iHighlight = GET_CHARACTER_CREATOR_COLOUR_INDEX(appearanceData.overlaySlot, iColour, appearanceData.iColourMax)
				ELSE
					iColour = 0
				ENDIF
				
				CPRINTLN(DEBUG_NET_CHARACTER, "INITIALISE_CHARACTER_CREATOR_OVERLAYS - From stats. Overlay: ", i, ", ", GET_CHARACTER_OVERLAY_TEXT(appearanceData.overlaySlot), "....iValue = ", iValue, ", opacity = ", fOpacity, ", colour = ", iColour, " iHighlight = ", iHighlight)
				
				SET_CHARACTER_CREATOR_APPEARANCE_VALUES(characterData, i, iValue, fOpacity, iColour, iColour, iHighlight)
			ELSE
				
				iValue = appearanceData.iDefaultValue
				fOpacity = appearanceData.fDefaultOpacity
				iColour = appearanceData.iDefaultColour
				iHighlight = 0
				
				IF appearanceData.colourPalette = RT_HAIR
				OR appearanceData.colourPalette = RT_MAKEUP
					iHighlight = GET_CHARACTER_CREATOR_COLOUR_INDEX(appearanceData.overlaySlot, iColour, appearanceData.iColourMax)
				ENDIF
						
				CPRINTLN(DEBUG_NET_CHARACTER, "INITIALISE_CHARACTER_CREATOR_OVERLAYS - From defaults (Gender invalid). Overlay: ", i, ", ", GET_CHARACTER_OVERLAY_TEXT(appearanceData.overlaySlot), "....iValue = ", iValue, ", opacity = ", fOpacity, ", colour = ", iColour, " iHighlight = ", iHighlight)
								
				// Default our values
				SET_CHARACTER_CREATOR_APPEARANCE_VALUES(characterData, i, iValue, fOpacity, iColour, iColour, iHighlight)
			ENDIF
		ELSE
			IF i = ciCHARACTER_CREATOR_APPEARANCE_EYES
				
				iValue = ROUND(GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_FEATURE_20, characterData.iCharacterSelectedSlot))
				
				CPRINTLN(DEBUG_NET_CHARACTER, "INITIALISE_CHARACTER_CREATOR_OVERLAYS - Default. Eye............iValue = ", iValue)

				SET_CHARACTER_CREATOR_APPEARANCE_VALUES(characterData, i, iValue, 100, 0, 0, 0)
			ENDIF
		ENDIF
	ENDREPEAT
	
	// If we have not entered from prompt, update our hair to match
	IF NOT HAS_ENTERED_CHAR_CREATION_FROM_ALTERATION_PROMPT()
		UPDATE_CHARACTER_CREATOR_HAIR_TO_MATCH_COLOURS(characterData)
	ENDIF
ENDPROC

/// PURPOSE:
///    Find the head of the parent within the shuffled array
PROC SET_CHARACTER_CREATOR_PARENT_HEAD_FROM_STAT(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, INT iHead, BOOL bSpecial, INT iIndex)
	
	CPRINTLN(DEBUG_NET_CHARACTER, "SET_CHARACTER_CREATOR_PARENT_HEAD_FROM_STAT - Getting head from: ", iHead, ", Special: ", PICK_STRING(bSpecial, "TRUE", "FALSE"), ", iIndex: ", iIndex)
	
	INT iHeadIndex
	IF iIndex = ciCHARACTER_CREATOR_MUM_INDEX
		IF NOT bSpecial
			iHeadIndex = (iHead - GET_PED_HEAD_BLEND_FIRST_INDEX(HBHT_FEMALE))
		ELSE
			iHeadIndex = (iHead - GET_PED_HEAD_BLEND_FIRST_INDEX(HBHT_UNIQUE_FEMALE)) + GET_PED_HEAD_BLEND_NUM_HEADS(HBHT_FEMALE)
		ENDIF
	ELSE
		IF NOT bSpecial
			iHeadIndex = (iHead - GET_PED_HEAD_BLEND_FIRST_INDEX(HBHT_MALE))
		ELSE
			iHeadIndex = (iHead - GET_PED_HEAD_BLEND_FIRST_INDEX(HBHT_UNIQUE_MALE)) + GET_PED_HEAD_BLEND_NUM_HEADS(HBHT_MALE)
		ENDIF
	ENDIF
	
	CPRINTLN(DEBUG_NET_CHARACTER, "SET_CHARACTER_CREATOR_PARENT_HEAD_FROM_STAT - Parent head index = ", iHeadIndex)
	
	INT i
	INT iIndexFound = 0
	REPEAT characterData.parentData[iIndex].iTotalHeadsAvailable i
		
		IF characterData.parentData[iIndex].sHeadSelection[i].iHead = iHeadIndex
			
			iIndexFound = i
			CPRINTLN(DEBUG_NET_CHARACTER, "SET_CHARACTER_CREATOR_PARENT_HEAD_FROM_STAT - Parent head found = ", iIndexFound)
			
			i = characterData.parentData[iIndex].iTotalHeadsAvailable
		ENDIF
	ENDREPEAT
	
	SET_CHARACTER_CREATOR_PARENT_HEAD(characterData, iIndex, iIndexFound)	
ENDPROC

/// PURPOSE:
///    Recalculates the parent dominance
FUNC FLOAT GET_CHARACTER_CREATOR_DOMINANCE_FROM_STATS(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, FLOAT fDom)
	
	FLOAT fDominance = fDom
	
	IF GET_CHARACTER_CREATOR_ACTIVE_PED_IS_MALE(characterData)
		fDominance = ((fDom - cfCHARACTER_CREATOR_MALE_DOM_MIN) / (cfCHARACTER_CREATOR_MALE_DOM_MAX - cfCHARACTER_CREATOR_MALE_DOM_MIN)) * 100
	ELSE
		fDominance = ((fDom - cfCHARACTER_CREATOR_FEMALE_DOM_MIN) / (cfCHARACTER_CREATOR_FEMALE_DOM_MAX - cfCHARACTER_CREATOR_FEMALE_DOM_MIN)) * 100
	ENDIF
	
	CPRINTLN(DEBUG_NET_CHARACTER, "GET_CHARACTER_CREATOR_DOMINANCE_FROM_STATS - Setting dominance to ", fDominance, " from saved dom: ", fDom)
	
	IF fDominance > 100
		SCRIPT_ASSERT("GET_CHARACTER_CREATOR_DOMINANCE_FROM_STATS - Converted dominance to an invalid value (> 100)")
		fDominance = 100
	ENDIF
	
	IF fDominance < 0
		SCRIPT_ASSERT("GET_CHARACTER_CREATOR_DOMINANCE_FROM_STATS - Converted dominance to an invalid value (< 0)")
		fDominance = 0
	ENDIF
	
	RETURN fDominance
ENDFUNC

/// PURPOSE:
/// 	Saves out the stats that this character has   
PROC POPULATE_CHARCTER_CREATOR_STATS_FROM_STATS(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData)
	
//	SET_CHARACTER_CREATOR_STAT_VALUE(characterData, CHARACTER_CREATOR_STAT_STAMINA, CALCULATE_PLAYER_STAT_VALUE(CHAR_MULTIPLAYER, PS_STAMINA, FALSE, characterData.iCharacterSelectedSlot))
//	SET_CHARACTER_CREATOR_STAT_VALUE(characterData, CHARACTER_CREATOR_STAT_SHOOTING, CALCULATE_PLAYER_STAT_VALUE(CHAR_MULTIPLAYER, PS_SHOOTING_ABILITY, FALSE, characterData.iCharacterSelectedSlot))
//	SET_CHARACTER_CREATOR_STAT_VALUE(characterData, CHARACTER_CREATOR_STAT_STRENGTH, CALCULATE_PLAYER_STAT_VALUE(CHAR_MULTIPLAYER, PS_STRENGTH, FALSE, characterData.iCharacterSelectedSlot))
//	SET_CHARACTER_CREATOR_STAT_VALUE(characterData, CHARACTER_CREATOR_STAT_STEALTH, CALCULATE_PLAYER_STAT_VALUE(CHAR_MULTIPLAYER, PS_STEALTH_ABILITY, FALSE, characterData.iCharacterSelectedSlot))
//	SET_CHARACTER_CREATOR_STAT_VALUE(characterData, CHARACTER_CREATOR_STAT_FLYING, CALCULATE_PLAYER_STAT_VALUE(CHAR_MULTIPLAYER, PS_FLYING_ABILITY, FALSE, characterData.iCharacterSelectedSlot))
//	SET_CHARACTER_CREATOR_STAT_VALUE(characterData, CHARACTER_CREATOR_STAT_DRIVING, CALCULATE_PLAYER_STAT_VALUE(CHAR_MULTIPLAYER, PS_DRIVING_ABILITY, FALSE, characterData.iCharacterSelectedSlot))
//	SET_CHARACTER_CREATOR_STAT_VALUE(characterData, CHARACTER_CREATOR_STAT_LUNG_CAPACITY, CALCULATE_PLAYER_STAT_VALUE(CHAR_MULTIPLAYER, PS_LUNG_CAPACITY, FALSE, characterData.iCharacterSelectedSlot))
	
	// url:bugstar:3308362 - [PUBLIC] [EXPLOIT] Boost stats by changing appearance
	
	SET_CHARACTER_CREATOR_STAT_VALUE(characterData, CHARACTER_CREATOR_STAT_STAMINA, GET_INITIAL_STAT_VALUE(CHAR_MULTIPLAYER, PS_STAMINA, characterData.iCharacterSelectedSlot))
	SET_CHARACTER_CREATOR_STAT_VALUE(characterData, CHARACTER_CREATOR_STAT_SHOOTING, GET_INITIAL_STAT_VALUE(CHAR_MULTIPLAYER, PS_SHOOTING_ABILITY, characterData.iCharacterSelectedSlot))
	SET_CHARACTER_CREATOR_STAT_VALUE(characterData, CHARACTER_CREATOR_STAT_STRENGTH, GET_INITIAL_STAT_VALUE(CHAR_MULTIPLAYER, PS_STRENGTH, characterData.iCharacterSelectedSlot))
	SET_CHARACTER_CREATOR_STAT_VALUE(characterData, CHARACTER_CREATOR_STAT_STEALTH, GET_INITIAL_STAT_VALUE(CHAR_MULTIPLAYER, PS_STEALTH_ABILITY, characterData.iCharacterSelectedSlot))
	SET_CHARACTER_CREATOR_STAT_VALUE(characterData, CHARACTER_CREATOR_STAT_FLYING, GET_INITIAL_STAT_VALUE(CHAR_MULTIPLAYER, PS_FLYING_ABILITY, characterData.iCharacterSelectedSlot))
	SET_CHARACTER_CREATOR_STAT_VALUE(characterData, CHARACTER_CREATOR_STAT_DRIVING, GET_INITIAL_STAT_VALUE(CHAR_MULTIPLAYER, PS_DRIVING_ABILITY, characterData.iCharacterSelectedSlot))
	SET_CHARACTER_CREATOR_STAT_VALUE(characterData, CHARACTER_CREATOR_STAT_LUNG_CAPACITY, GET_INITIAL_STAT_VALUE(CHAR_MULTIPLAYER, PS_LUNG_CAPACITY, characterData.iCharacterSelectedSlot))
		
ENDPROC

/// PURPOSE:
///    Grabs saved data and reinitialises the cretor based on these
PROC GENERATE_INITIAL_CHARACTER_CREATOR_DATA_FROM_STATS(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData)
	
	//characterData.iCharacterSelectedSlot = GET_JOINING_CHARACTER()

	INT iHeadMum 		
	INT iHeadDad 		
	INT iHeadMumVar 	
	INT iHeadDadVar 	
	FLOAT fDominance 	
	FLOAT fSkinToneBlend
	
	IF GET_PACKED_STAT_INT(PACKED_CHAR_PICTURE, characterData.iCharacterSelectedSlot) = 0
		characterData.eActiveChild = CHARACTER_CREATOR_SON
		CPRINTLN(DEBUG_NET_CHARACTER, "			 - Player character is male")
	ELSE
		characterData.eActiveChild = CHARACTER_CREATOR_DAUGHTER
		CPRINTLN(DEBUG_NET_CHARACTER, "			 - Player character is female")
	ENDIF
	
	CPRINTLN(DEBUG_NET_CHARACTER, "			 - Player character slot: ", characterData.iCharacterSelectedSlot)
	
	// Are we an old version of the character creator or new?
	IF GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_IS_NG_VERSION, characterData.iCharacterSelectedSlot) >= ciCHARACTER_CREATOR_CURRENT_VERSION
	
		CPRINTLN(DEBUG_NET_CHARACTER, "GENERATE_INITIAL_CHARACTER_CREATOR_DATA_FROM_STATS - Player character is from new character creator")

		iHeadMum 			= GET_PACKED_STAT_INT(PACKED_MP_M1_HEAD_TYPE, characterData.iCharacterSelectedSlot)
		iHeadDad 			= GET_PACKED_STAT_INT(PACKED_MP_D1_HEAD_TYPE, characterData.iCharacterSelectedSlot)
		iHeadMumVar 		= GET_PACKED_STAT_INT(PACKED_MP_M1_HEAD_VAR, characterData.iCharacterSelectedSlot)
		iHeadDadVar 		= GET_PACKED_STAT_INT(PACKED_MP_D1_HEAD_VAR, characterData.iCharacterSelectedSlot)
		fDominance 			= GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_HEADBLEND_GEOM_BLEND, characterData.iCharacterSelectedSlot)
		fSkinToneBlend 		= GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_HEADBLEND_TEX_BLEND, characterData.iCharacterSelectedSlot)
	
		iHeadMum = GET_CHARACTER_CREATOR_PARENT_HEAD_VALUE(iHeadMum, FALSE, (iHeadMumVar = 1))
		iHeadDad = GET_CHARACTER_CREATOR_PARENT_HEAD_VALUE(iHeadDad, TRUE, (iHeadDadVar = 1))
	
		SET_CHARACTER_CREATOR_PARENT_HEAD_FROM_STAT(characterData, iHeadMum, (iHeadMumVar = 1), ciCHARACTER_CREATOR_MUM_INDEX)
		SET_CHARACTER_CREATOR_PARENT_HEAD_FROM_STAT(characterData, iHeadDad, (iHeadDadVar = 1), ciCHARACTER_CREATOR_DAD_INDEX)
		
		SET_CHARACTER_CREATOR_CHILD_DOMINANCE(characterData, characterData.eActiveChild, GET_CHARACTER_CREATOR_DOMINANCE_FROM_STATS(characterData, fDominance))
		SET_CHARACTER_CREATOR_CHILD_SKIN_TONE(characterData, characterData.eActiveChild, fSkinToneBlend * 100)
		
		CPRINTLN(DEBUG_NET_CHARACTER, "			 - Parent Dominance = ", GET_CHARACTER_CREATOR_CHILD_DOMINANCE(characterData, characterData.eActiveChild) )
		CPRINTLN(DEBUG_NET_CHARACTER, "			 - Skin Tone Blend = ", GET_CHARACTER_CREATOR_CHILD_SKIN_TONE(characterData, characterData.eActiveChild))
	ELSE
	
		CPRINTLN(DEBUG_NET_CHARACTER, "GENERATE_INITIAL_CHARACTER_CREATOR_DATA_FROM_STATS - Player character is from old character creator")
		
		INT iHeadD1			= GET_PACKED_STAT_INT(PACKED_MP_D1_HEAD_TYPE, characterData.iCharacterSelectedSlot)
		INT iHeadD2			= GET_PACKED_STAT_INT(PACKED_MP_D2_HEAD_TYPE, characterData.iCharacterSelectedSlot)
		INT iHeadDV1		= GET_PACKED_STAT_INT(PACKED_MP_D1_HEAD_VAR, characterData.iCharacterSelectedSlot)
		INT iHeadDV2		= GET_PACKED_STAT_INT(PACKED_MP_D2_HEAD_VAR, characterData.iCharacterSelectedSlot)
		FLOAT fGParentDomD	= TO_FLOAT(GET_PACKED_STAT_INT(PACKED_MP_D_HEAD_DOM, characterData.iCharacterSelectedSlot)) / 100
		
		INT iHeadDadMum1, iHeadDadDad2, iHeadMumMum3, iHeadMumDad4
		GET_CHAR_CREATOR_HEAD_BLEND_VALUES_FROM_RACE_AND_VAR(TRUE, iHeadDadMum1, iHeadDadDad2, iHeadD1, iHeadDV1, iHeadD2, iHeadDV2)
				
		INT iHeadM1			= GET_PACKED_STAT_INT(PACKED_MP_M1_HEAD_TYPE, characterData.iCharacterSelectedSlot)
		INT iHeadM2			= GET_PACKED_STAT_INT(PACKED_MP_M2_HEAD_TYPE, characterData.iCharacterSelectedSlot)
		INT iHeadMV1		= GET_PACKED_STAT_INT(PACKED_MP_M1_HEAD_VAR, characterData.iCharacterSelectedSlot)
		INT iHeadMV2		= GET_PACKED_STAT_INT(PACKED_MP_M2_HEAD_VAR, characterData.iCharacterSelectedSlot)
		FLOAT fGParentDomM	= TO_FLOAT(GET_PACKED_STAT_INT(PACKED_MP_M_HEAD_DOM, characterData.iCharacterSelectedSlot)) / 100
		
		GET_CHAR_CREATOR_HEAD_BLEND_VALUES_FROM_RACE_AND_VAR(FALSE, iHeadMumMum3, iHeadMumDad4, iHeadM1, iHeadMV1, iHeadM2, iHeadMV2)
		
		CPRINTLN(DEBUG_NET_CHARACTER, "GENERATE_INITIAL_CHARACTER_CREATOR_DATA_FROM_STATS - Dad saved stats: iHeadDadMum1 = ", iHeadDadMum1, " and iHeadDadDad2 = ", iHeadDadDad2)
		CPRINTLN(DEBUG_NET_CHARACTER, "GENERATE_INITIAL_CHARACTER_CREATOR_DATA_FROM_STATS - Mum saved stats: iHeadMumMum3 = ", iHeadMumMum3, " and iHeadMumDad4 = ", iHeadMumDad4)
		CPRINTLN(DEBUG_NET_CHARACTER, "GENERATE_INITIAL_CHARACTER_CREATOR_DATA_FROM_STATS - fGParentDomD = ", fGParentDomD, " and fGParentDomM = ", fGParentDomM)
		
		// Set Dad to our closest head from mum or dad side
		IF fGParentDomD < 0.5
			CPRINTLN(DEBUG_NET_CHARACTER, "GENERATE_INITIAL_CHARACTER_CREATOR_DATA_FROM_STATS - Choosing iHeadDadMum1 = ", iHeadDadMum1, " and iHeadDV1 = ", iHeadD1)
			SET_CHARACTER_CREATOR_PARENT_HEAD_FROM_STAT(characterData, iHeadDadMum1, (iHeadD1 = ENUM_TO_INT(CHAR_CREATOR_SPECIAL_RACE)), ciCHARACTER_CREATOR_DAD_INDEX)
		ELSE
			CPRINTLN(DEBUG_NET_CHARACTER, "GENERATE_INITIAL_CHARACTER_CREATOR_DATA_FROM_STATS - Choosing iHeadDadDad2 = ", iHeadDadDad2, " and iHeadDV2 = ", iHeadD2)
			SET_CHARACTER_CREATOR_PARENT_HEAD_FROM_STAT(characterData, iHeadDadDad2, (iHeadD2 = ENUM_TO_INT(CHAR_CREATOR_SPECIAL_RACE)), ciCHARACTER_CREATOR_DAD_INDEX)
		ENDIF
		
		// Set Mum to our closest head from mum or dad side
		IF fGParentDomM < 0.5
			CPRINTLN(DEBUG_NET_CHARACTER, "GENERATE_INITIAL_CHARACTER_CREATOR_DATA_FROM_STATS - Choosing iHeadMumMum3 = ", iHeadMumMum3, " and iHeadMV1 = ", iHeadM1)
			SET_CHARACTER_CREATOR_PARENT_HEAD_FROM_STAT(characterData, iHeadMumMum3, (iHeadM1 = ENUM_TO_INT(CHAR_CREATOR_SPECIAL_RACE)), ciCHARACTER_CREATOR_MUM_INDEX)
		ELSE
			CPRINTLN(DEBUG_NET_CHARACTER, "GENERATE_INITIAL_CHARACTER_CREATOR_DATA_FROM_STATS - Choosing iHeadMumDad4 = ", iHeadMumDad4, " and iHeadMV2 = ", iHeadM2)
			SET_CHARACTER_CREATOR_PARENT_HEAD_FROM_STAT(characterData, iHeadMumDad4, (iHeadM2 = ENUM_TO_INT(CHAR_CREATOR_SPECIAL_RACE)), ciCHARACTER_CREATOR_MUM_INDEX)
		ENDIF
		
		fDominance = GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_HEADBLEND_DOM, characterData.iCharacterSelectedSlot)
		SET_CHARACTER_CREATOR_CHILD_DOMINANCE(characterData, characterData.eActiveChild, GET_CHARACTER_CREATOR_DOMINANCE_FROM_STATS(characterData, fDominance))
		
		// For these characters it matches dominance
		SET_CHARACTER_CREATOR_CHILD_SKIN_TONE(characterData, characterData.eActiveChild, GET_CHARACTER_CREATOR_DOMINANCE_FROM_STATS(characterData, fDominance))
	ENDIF
	
	// get t-shirt
	IF WAS_PLAYER_WEARING_CREW_TSHIRT(characterData.iCharacterSelectedSlot)
		SET_CHARACTER_CREATOR_CREW_TSHIRT(characterData, INT_TO_ENUM(CHAR_CREATOR_CREW_TSHIRT_OPTION_ENUM, GET_PACKED_STAT_INT(PACKED_MP_CREW_T_TYPE, characterData.iCharacterSelectedSlot)))
	ENDIF
	
	// t-shirt decals
	IF IS_MP_TATTOO_CURRENT(TATTOO_MP_FM_CREW_A, characterData.iCharacterSelectedSlot)
		SET_CHARACTER_CREATOR_CREW_TSHIRT_DECAL(characterData, CC_CREW_DECAL_SMALL)
	ELIF IS_MP_TATTOO_CURRENT(TATTOO_MP_FM_CREW_B, characterData.iCharacterSelectedSlot)
		SET_CHARACTER_CREATOR_CREW_TSHIRT_DECAL(characterData, CC_CREW_DECAL_BIG)
	ELIF IS_MP_TATTOO_CURRENT(TATTOO_MP_FM_CREW_C, characterData.iCharacterSelectedSlot)
		IF GET_CHARACTER_CREATOR_ACTIVE_PED_IS_MALE(characterData)
			SET_CHARACTER_CREATOR_CREW_TSHIRT_DECAL(characterData, CC_CREW_DECAL_BACK)
		ELSE
			SET_CHARACTER_CREATOR_CREW_TSHIRT_DECAL(characterData, CC_CREW_DECAL_BIG)	
		ENDIF
	ENDIF
	
	// Set the players hair to their current style
	INT iColour = GET_MP_INT_CHARACTER_STAT(MP_STAT_HAIR_TINT, characterData.iCharacterSelectedSlot)
	INT iColour2 = GET_MP_INT_CHARACTER_STAT(MP_STAT_SEC_HAIR_TINT, characterData.iCharacterSelectedSlot)
	
	// Override if last gen hair
	IF GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_IS_NG_VERSION, characterData.iCharacterSelectedSlot) < ciCHARACTER_CREATOR_CURRENT_VERSION
		
		PED_COMP_NAME_ENUM theHair = INT_TO_ENUM(PED_COMP_NAME_ENUM, GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_STORED_HAIRDO_SA, characterData.iCharacterSelectedSlot))		
	
		PED_COMP_ITEM_DATA_STRUCT theStruct
		MODEL_NAMES mnModel
		
		IF characterData.eActiveChild = CHARACTER_CREATOR_SON
			mnModel = MP_M_FREEMODE_01
			CPRINTLN(DEBUG_NET_CHARACTER, "GENERATE_INITIAL_CHARACTER_CREATOR_DATA_FROM_STATS - Last Gen Character. Grab correct tint for male hair style:")
		ELSE
			mnModel = MP_F_FREEMODE_01
			CPRINTLN(DEBUG_NET_CHARACTER, "GENERATE_INITIAL_CHARACTER_CREATOR_DATA_FROM_STATS - Last Gen Character. Grab correct tint for female hair style:")
		ENDIF
		
		theStruct = GET_PED_COMP_DATA_FOR_ITEM_MP(mnModel, COMP_TYPE_HAIR, theHair)
		iColour = GET_TINT_INDEX_FOR_LAST_GEN_HAIR_TEXTURE(mnModel, theStruct.iDrawable, theStruct.iTexture)
		iColour2 = iColour
		
		CPRINTLN(DEBUG_NET_CHARACTER, "GENERATE_INITIAL_CHARACTER_CREATOR_DATA_FROM_STATS - Last Gen Character. Grab correct tint for last hair style:")
		CPRINTLN(DEBUG_NET_CHARACTER, "GENERATE_INITIAL_CHARACTER_CREATOR_DATA_FROM_STATS - 		- theHair: ", ENUM_TO_INT(theHair))
		CPRINTLN(DEBUG_NET_CHARACTER, "GENERATE_INITIAL_CHARACTER_CREATOR_DATA_FROM_STATS - 		- theStruct.iDrawable: ", theStruct.iDrawable)
		CPRINTLN(DEBUG_NET_CHARACTER, "GENERATE_INITIAL_CHARACTER_CREATOR_DATA_FROM_STATS - 		- theStruct.iTexture: ", theStruct.iTexture)
	ENDIF
	
	INT iHighlight = GET_CHARACTER_CREATOR_COLOUR_INDEX(HOS_BASE_DETAIL, iColour, GET_NUM_PED_HAIR_TINTS())
	
	CPRINTLN(DEBUG_NET_CHARACTER, "GENERATE_INITIAL_CHARACTER_CREATOR_DATA_FROM_STATS - 		- Hair iColour: ", iColour)
	CPRINTLN(DEBUG_NET_CHARACTER, "GENERATE_INITIAL_CHARACTER_CREATOR_DATA_FROM_STATS - 		- Hair iColour2: ", iColour2)
	CPRINTLN(DEBUG_NET_CHARACTER, "GENERATE_INITIAL_CHARACTER_CREATOR_DATA_FROM_STATS - 		- Hair iHighlight: ", iHighlight)
	
	IF GET_CHARACTER_CREATOR_ACTIVE_PED_IS_MALE(characterData)
		SET_CHARACTER_CREATOR_APPEARANCE_VALUES(characterData, ciCHARACTER_CREATOR_APPEARANCE_HAIR_STYLE_M, ciCHARACTER_CREATOR_EXISTING_INDEX, 100, iColour, iColour2, iHighlight)
	ELSE
		SET_CHARACTER_CREATOR_APPEARANCE_VALUES(characterData, ciCHARACTER_CREATOR_APPEARANCE_HAIR_STYLE_F, ciCHARACTER_CREATOR_EXISTING_INDEX, 100, iColour, iColour2, iHighlight)
	ENDIF
	
	UPDATE_CHARACTER_CREATOR_SECONDARY_HAIR_COLOUR(characterData, characterData.eActiveChild)
	
	// Set the stats
	POPULATE_CHARCTER_CREATOR_STATS_FROM_STATS(characterData)
ENDPROC

/// PURPOSE:
///    Initialise any data in our struct for the upcoming character screen
PROC INITIALISE_CHARACTER_CREATOR_GENERATION_DATA(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData)

	INT iCount = 0
	
	INT i
	REPEAT GET_NUM_PED_HAIR_TINTS() i
	
		IF IS_PED_HAIR_TINT_FOR_CREATOR(i)
			iCount++
		ENDIF
	ENDREPEAT

	CPRINTLN(DEBUG_NET_CHARACTER, "INITIALISE_CHARACTER_CREATOR_GENERATION_DATA - Num Hair Colours available to creator (IS_PED_HAIR_TINT_FOR_CREATOR): ", iCount)
	characterData.iMaxHairColours = iCount
	
	iCount = 0
	REPEAT GET_NUM_PED_MAKEUP_TINTS() i
	
		IF IS_PED_BLUSH_TINT_FOR_CREATOR(i)
			iCount++
		ENDIF
	ENDREPEAT

	CPRINTLN(DEBUG_NET_CHARACTER, "INITIALISE_CHARACTER_CREATOR_GENERATION_DATA - Num Blusher Colours available to creator (IS_PED_BLUSH_TINT_FOR_CREATOR): ", iCount)
	characterData.iMaxBlusherColours = iCount
	
	iCount = 0
	REPEAT GET_NUM_PED_MAKEUP_TINTS() i
	
		IF IS_PED_LIPSTICK_TINT_FOR_CREATOR(i)
			iCount++
		ENDIF
	ENDREPEAT

	CPRINTLN(DEBUG_NET_CHARACTER, "INITIALISE_CHARACTER_CREATOR_GENERATION_DATA - Num Lipstick Colours available to creator (IS_PED_LIPSTICK_TINT_FOR_CREATOR): ", iCount)
	characterData.iMaxLipstickColours = iCount
		
ENDPROC

/// PURPOSE:
///    Set up all the default data we need to have the ped generated and creator initialised
PROC GENERATE_INITIAL_CHARACTER_CREATOR_DATA(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, MPHUD_PLACEMENT_TOOLS& Placement)

	CPRINTLN(DEBUG_NET_CHARACTER, "GENERATE_INITIAL_CHARACTER_CREATOR_DATA - Setting up default data, slot: ", characterData.iCharacterSelectedSlot)
	
	// Sound ID for continuous menu sounds 
	characterData.iSoundID = GET_SOUND_ID()
	
	// Cleanup any gang outfits before ped creation
	GB_CLEANUP_GANG_OUTFITS()
	
	// Remove duffel bag before ped creation B* 4707071
	IF IS_PED_WEARING_A_DUFFEL_BAG(PLAYER_PED_ID())
		SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_HAND, 0, 0)
	ENDIF
	
	IF HAS_ENTERED_CHAR_CREATION_FROM_ALTERATION_PROMPT()
		characterData.iCharacterSelectedSlot = Placement.ScreenPlace.iSelectedCharacter
		CPRINTLN(DEBUG_NET_CHARACTER, "GENERATE_INITIAL_CHARACTER_CREATOR_DATA - Using stats as entering from alteration route: Slot to load: = ", characterData.iCharacterSelectedSlot)
	ENDIF
	
	#IF FEATURE_GEN9_STANDALONE
	IF GET_CHOSEN_MP_CHARACTER_SLOT() >= 0
		characterData.iCharacterSelectedSlot = GET_CHOSEN_MP_CHARACTER_SLOT()
	ENDIF
	#ENDIF
	
	INITIALISE_CHARACTER_CREATOR_GENERATION_DATA(characterData)
	
	// Populates all the data for the parents
	POPULATE_CHARACTER_CREATOR_PARENT_DATA(characterData)
	
	// Randomise our dom based on the gender
	RANDOMISE_CHARACTER_CREATOR_DOM_AND_SKIN_TONE(characterData, CHARACTER_CREATOR_SON)
	RANDOMISE_CHARACTER_CREATOR_DOM_AND_SKIN_TONE(characterData, CHARACTER_CREATOR_DAUGHTER)

	// Randomise the hair and colours
	RANDOMISE_CHARACTER_HAIR_AND_HAIR_COLOUR(characterData, TRUE)

	IF NOT HAS_ENTERED_CHAR_CREATION_FROM_ALTERATION_PROMPT()
		// Set up the initial gender selection
		RANDOMISE_CHARACTER_CREATOR_GENDER(characterData)

		// Randomise our outfits to begin with
		RANDOMISE_CHARACTER_CREATOR_STYLE_AND_OUTFIT(characterData, 0)
		
		// Pick from one of the carrer styles if we have come from the windfall flow.
		#IF FEATURE_GEN9_STANDALONE
		CHARACTER_CAREER_TYPE eChosenCareerType = GET_PENDING_CHARACTER_CAREER_FOR_NEW_CHARACTER()
		SWITCH eChosenCareerType
			CASE CHARACTER_CAREER_EXECUTIVE
				RANDOMISE_CHARACTER_CREATOR_STYLE_AND_OUTFIT(characterData, 8)
			BREAK
			CASE CHARACTER_CAREER_GUNRUNNER
				RANDOMISE_CHARACTER_CREATOR_STYLE_AND_OUTFIT(characterData, 9)
			BREAK
			CASE CHARACTER_CAREER_NIGHTCLUB_OWNER
				RANDOMISE_CHARACTER_CREATOR_STYLE_AND_OUTFIT(characterData, 10)
			BREAK
			CASE CHARACTER_CAREER_BIKER
				RANDOMISE_CHARACTER_CREATOR_STYLE_AND_OUTFIT(characterData, 11)
			BREAK
		ENDSWITCH
		#ENDIF
		
		// Generate random stats for the player
		RANDOMISE_CHARACTER_CREATOR_STATS(characterData)
	ELSE
		CPRINTLN(DEBUG_NET_CHARACTER, "GENERATE_INITIAL_CHARACTER_CREATOR_DATA - Player entering through alteration prompt")
		
		GENERATE_INITIAL_CHARACTER_CREATOR_DATA_FROM_STATS(characterData)
	ENDIF
		
	// Initialises the features
	INITIALISE_CHARACTER_CREATOR_FEATURES(characterData)
	
	// Initialises our appearance values
	INITIALISE_CHARACTER_CREATOR_OVERLAYS(characterData)
	
	// Reset all our values to -1
	INITIALISE_CHARACTER_CREATOR_HATS(characterData)
	INITIALISE_CHARACTER_CREATOR_GLASSES(characterData)
	
ENDPROC

/// PURPOSE:
///    Force a full refresh of our creator for a ped coming from before
PROC FORCE_CHARACTER_CREATOR_FULL_REFRESH(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData)
	
	CPRINTLN(DEBUG_NET_CHARACTER, "FORCE_CHARACTER_CREATOR_FULL_REFRESH - Force a full refresh of the player appearence due to coming into creator with an old character")
	
	IF IS_CHARACTER_CONTROLLER_PED_OK(characterData.characterCreatorPeds[characterData.eActiveChild])
		#IF NOT USE_TU_CHANGES
		CLEAR_PED_DECORATIONS(characterData.characterCreatorPeds[characterData.eActiveChild])
		#ENDIF
		#IF USE_TU_CHANGES
		CLEAR_PED_DECORATIONS_LEAVE_SCARS(characterData.characterCreatorPeds[characterData.eActiveChild])
		#ENDIF
	ENDIF
	
	// Apply the clothes and hair to match the current ped
	SET_MP_PLAYERS_START_CLOTHES(FALSE, TSHIRT_STATES_INGAME_USE_STORED, characterData.characterCreatorPeds[characterData.eActiveChild], characterData.iCharacterSelectedSlot, DEFAULT, TRUE)

	// Remove any components we shouldn't have on
	REMOVE_CHARACTER_CREATOR_COMPONENTS_FOR_EDITING(characterData)
	
	SET_CHARACTER_CREATOR_CHILD_STYLE(characterData, characterData.eActiveChild, ciCHARACTER_CREATOR_EXISTING_INDEX)
	APPLY_CHARACTER_CREATOR_HAIR(characterData, characterData.eActiveChild)
	
	APPLY_CHARACTER_CREATOR_HEAD_BLEND(characterData, characterData.eActiveChild)
	
	// And the overlays
	APPLY_CHARACTER_CREATOR_OVERLAY(characterData, TRUE)
	// And apply all micro morphs
	APPLY_CHARACTER_CREATOR_ALL_MICRO_MORPH(characterData)
	
ENDPROC

