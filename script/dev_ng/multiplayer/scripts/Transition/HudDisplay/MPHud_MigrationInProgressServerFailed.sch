
USING "screens_header.sch"
USING "net_events.sch"
USING "script_network.sch"
USING "Transition_Common.sch"



PROC RENDER_MPHUD_MIGRATION_IN_PROGRESS_SERVER_FAILED(MPHUD_PLACEMENT_TOOLS& Placement)

	SET_ALL_MENUPED_VISIBLE(Placement.SelectionPed, Placement.IdlePed, FALSE )		

	STRING sStatementText = "HUD_COMPTITLEFAIL" 
	STRING sLineOneMessge = "HUD_SAVEMIGPOST3"
	STRING sLineTwoMessge = ""
	
	IF g_i_ServerFailedMigrationReason = 1 //Canceled
		sStatementText = "HUD_SERVCAN_TITLE" 
		sLineOneMessge = "HUD_SERVCAN_TRANS"
	ELIF g_i_ServerFailedMigrationReason = 2 //RolledBack
		sStatementText = "HUD_REVERT_TITLE" 
		sLineOneMessge = "HUD_REVERT_TRANS"
	ENDIF

	FE_WARNING_FLAGS buttons = FE_WARNING_YESNO
	
	SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sLineOneMessge, buttons, sLineTwoMessge)


ENDPROC

PROC LOGIC_MPHUD_MIGRATION_IN_PROGRESS_SERVER_FAILED(MPHUD_PLACEMENT_TOOLS& Placement)
	
	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
	
		NET_NL()NET_PRINT("LOGIC_MPHUD_MIGRATION_IN_PROGRESS_SERVER_FAILED: INPUT_FRONTEND_ACCEPT ")
		
		RESET_PAUSE_MENU_WARP_REQUESTS()	
		
		NET_NL()NET_PRINT("[SAVETRANS] LOGIC_MPHUD_MIGRATION_IN_PROGRESS_SERVER_FAILED: Moving POST_CHECK_SAVE_MIGRATION into the failed but ok stage.  ")
		Placement.iCheckMigrateStages = 7 //Failed but ok stage. 
		SET_JOINING_GAMEMODE(GAMEMODE_FM)
		HUD_CHANGE_STATE(HUD_STATE_PRE_HUD_CHECKS)
		TRANSITION_CHANGE_STATE(TRANSITION_STATE_PRE_HUD_CHECKS)
		g_i_ServerFailedMigrationReason = 0
		
	ENDIF
	
	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_CANCEL)
	
		IF g_i_ServerFailedMigrationReason = 2 //RolledBack

			IF STAT_ROLLBACK_SAVE_MIGRATION() 
				NET_NL()NET_PRINT("[SAVETRANS] LOGIC_MPHUD_MIGRATION_IN_PROGRESS_SERVER_FAILED: INPUT_FRONTEND_CANCEL Called STAT_ROLLBACK_SAVE_MIGRATION() = TRUE ")
			ELSE
				NET_NL()NET_PRINT("[SAVETRANS] LOGIC_MPHUD_MIGRATION_IN_PROGRESS_SERVER_FAILED: INPUT_FRONTEND_CANCEL Called STAT_ROLLBACK_SAVE_MIGRATION() = FALSE ")
			ENDIF
		ENDIF
		SET_MIGRATION_NOT_FINISHED_BLOCK_SAVE(Placement)
		NET_NL()NET_PRINT("LOGIC_MPHUD_MIGRATION_IN_PROGRESS_SERVER_FAILED: INPUT_FRONTEND_CANCEL ")
		g_i_ServerFailedMigrationReason = 0
		REQUEST_TRANSITION(TRANSITION_STATE_RETURN_TO_SINGLEPLAYER, GAMEMODE_SP, HUD_STATE_NO_DISPLAY)
	ENDIF



ENDPROC


PROC PROCESS_MPHUD_MIGRATION_IN_PROGRESS_SERVER_FAILED(MPHUD_PLACEMENT_TOOLS& Placement)
	
	// initialise data
	IF NOT (Placement.bHudScreenInitialised)
		IF IS_PAUSE_MENU_ACTIVE()
			SET_PAUSE_MENU_ACTIVE(FALSE)
			SET_SKYFREEZE_CLEAR()
		ENDIF
		
		SET_BAIL_STILL_VALID(FALSE) //Need this when signing out in a corona, it would bring this screen up twice. It would think the bail was still valid in cleanup
		RUN_MP_DISCONNECTION_CLEANUP(Placement)
		
		
		Placement.bHudScreenInitialised = TRUE
	ENDIF

	RUN_MP_DISCONNECTION_CLEANUP_EVERY_FRAME()

	
	RENDER_MPHUD_MIGRATION_IN_PROGRESS_SERVER_FAILED(Placement)
	
// logic	
	LOGIC_MPHUD_MIGRATION_IN_PROGRESS_SERVER_FAILED(Placement)
	
			
ENDPROC

