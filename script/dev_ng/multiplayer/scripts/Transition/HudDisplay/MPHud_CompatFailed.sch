
USING "screens_header.sch"
USING "net_events.sch"
USING "script_network.sch"
USING "Transition_Common.sch"

//Stages for dealing with downloading the content packs
CONST_INT ciPROCESS_MPHUD_COMPAT_FAILED_INT				0
CONST_INT ciPROCESS_MPHUD_COMPAT_FAILED_ENTER_STORE		1
CONST_INT ciPROCESS_MPHUD_COMPAT_FAILED_OUT_STORE		2
CONST_INT ciPROCESS_MPHUD_COMPAT_FAILED_WAIT_FOR_DL		3
CONST_INT ciPROCESS_MPHUD_COMPAT_FAILED_TIME_OUT		4


//render packs need downloaded alert
PROC RENDER_MPHUD_COMPAT_FAILED(MPHUD_PLACEMENT_TOOLS& Placement)
	SET_ALL_MENUPED_VISIBLE(Placement.SelectionPed, Placement.IdlePed, FALSE )						
	STRING sStatementText = "HUD_CONNPROB"
	//One or more compatibility packs is missing and must be installed to play GTA Online. Proceed to the Xbox Games Store to download?
	//One or more compatibility packs is missing and must be installed to play GTA Online. Proceed to PlayStation®Store to download?
	STRING sQuestionText = "HUD_COMBATPACK"
	STRING sSubText = ""
	SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sQuestionText, FE_WARNING_YESNO, sSubText)
ENDPROC

//render packs timed out when downloaded alert
PROC RENDER_MPHUD_COMPAT_DOWNLOAD_TIME_OUT(MPHUD_PLACEMENT_TOOLS& Placement)
	SET_ALL_MENUPED_VISIBLE(Placement.SelectionPed, Placement.IdlePed, FALSE )				
	STRING sStatementText = "HUD_CONNPROB"
	STRING sQuestionText = "HUD_COMBATPACKT" //Timed out when checking current compatibility pack configuration. Please return to Grand Theft Auto V.
	STRING sSubText = ""
	SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sQuestionText, FE_WARNING_OK, sSubText)	
ENDPROC


//Deal with the player accepting going to the store
PROC LOGIC_MPHUD_COMPAT_FAILED()
	//Yes I want to got to the store and get the new thing
	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
		//Open the store
		OPEN_COMMERCE_STORE("", "COMPAT_PACKS") 
		//Mpove stage?
		g_iForceDLCStoreSetUp = ciPROCESS_MPHUD_COMPAT_FAILED_OUT_STORE
		PRINTLN("PROCESS_MPHUD_COMPAT_FAILED - g_iForceDLCStoreSetUp = ciPROCESS_MPHUD_COMPAT_FAILED_OUT_STORE")
	ENDIF	
	//No I don't want to go to the store
	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_CANCEL)
		REQUEST_TRANSITION(TRANSITION_STATE_RETURN_TO_SINGLEPLAYER, GET_CURRENT_GAMEMODE(), HUD_STATE_LOADING)
	ENDIF
ENDPROC

//Deal with the player press accept
PROC LOGIC_MPHUD_COMPAT_DOWNLOAD_TIME_OUT()	
	//wait for the player to close
	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
		g_iForceDLCStoreSetUp = ciPROCESS_MPHUD_COMPAT_FAILED_INT
		PRINTLN("PROCESS_MPHUD_COMPAT_FAILED - g_iForceDLCStoreSetUp = ciPROCESS_MPHUD_COMPAT_FAILED_INT")
		REQUEST_TRANSITION(TRANSITION_STATE_RETURN_TO_SINGLEPLAYER, GET_CURRENT_GAMEMODE(), HUD_STATE_LOADING)
	ENDIF
ENDPROC

//Process downloading the content packs
PROC PROCESS_MPHUD_COMPAT_FAILED(MPHUD_PLACEMENT_TOOLS& Placement)
	
	//If the initial boot screen is active
	IF GET_IS_LOADING_SCREEN_ACTIVE()		
		// Clean up the current loading
		IF NOT (Placement.bHudScreenInitialised)
			RUN_MP_DISCONNECTION_CLEANUP(Placement, TRUE)	
			Placement.bHudScreenInitialised = TRUE
		ENDIF		
		//Not set up the loading screen
		IF g_iForceDLCStoreSetUp = ciPROCESS_MPHUD_COMPAT_FAILED_INT
			//Not in the sky
			IF GET_SKYSWOOP_STAGE() != SKYSWOOP_INSKYSTATIC
				SET_SKYSWOOP_UP(FALSE, FALSE, FALSE, SWITCH_TYPE_LONG, TRUE, 0,0,0,FALSE)
			 	PRINTLN("PROCESS_MPHUD_COMPAT_FAILED - GET_SKYSWOOP_STAGE() != SKYSWOOP_INSKYSTATIC")
				EXIT
			ENDIF		
			//Clean up the loading screen
			NET_NL()NET_PRINT("PROCESS_MPHUD_COMPAT_FAILED: SHUTDOWN_LOADING_SCREEN ")NET_NL()
			SHUTDOWN_LOADING_SCREEN()
			SET_JOINING_SPINNER_LOADING(FALSE)			
		 	PRINTLN("PROCESS_MPHUD_COMPAT_FAILED - GET_IS_LOADING_SCREEN_ACTIVE = TRUE")		
		ENDIF			
	ENDIF
	
	//Spam print, because I can
	PRINTLN("PROCESS_MPHUD_COMPAT_FAILED")
	
	// initialise data
	IF NOT (Placement.bHudScreenInitialised)
		RUN_MP_DISCONNECTION_CLEANUP(Placement)	
		Placement.bHudScreenInitialised = TRUE
	ENDIF
	
	//Call Brenda's clean up
	RUN_MP_DISCONNECTION_CLEANUP_EVERY_FRAME()

	//Switch the current status of the download
	SWITCH g_iForceDLCStoreSetUp
		
		//Check to see if the players presses quit or continue
		CASE ciPROCESS_MPHUD_COMPAT_FAILED_INT
			g_iForceDLCStoreSetUp = ciPROCESS_MPHUD_COMPAT_FAILED_ENTER_STORE
			PRINTLN("PROCESS_MPHUD_COMPAT_FAILED - g_iForceDLCStoreSetUp = ciPROCESS_MPHUD_COMPAT_FAILED_ENTER_STORE")	
		BREAK
		
		//Deal with the enter store logic
		CASE ciPROCESS_MPHUD_COMPAT_FAILED_ENTER_STORE
			// logic	
			LOGIC_MPHUD_COMPAT_FAILED()
			RENDER_MPHUD_COMPAT_FAILED(Placement)
		BREAK
		
		//wait for the store to close
		CASE ciPROCESS_MPHUD_COMPAT_FAILED_OUT_STORE
			IF NOT IS_COMMERCE_STORE_OPEN()
				g_iForceDLCStoreSetUp = ciPROCESS_MPHUD_COMPAT_FAILED_WAIT_FOR_DL
				PRINTLN("PROCESS_MPHUD_COMPAT_FAILED - g_iForceDLCStoreSetUp = ciPROCESS_MPHUD_COMPAT_FAILED_WAIT_FOR_DL")
			ENDIF
		BREAK
		
		//Once the store has closed wait for the data to finish downloading
		CASE ciPROCESS_MPHUD_COMPAT_FAILED_WAIT_FOR_DL
			BOOL bTimedOut
			IF HAS_CLOUD_REQUESTS_FINISHED(bTimedOut, 0)
				HUD_CHANGE_STATE(HUD_STATE_NO_DISPLAY)
				TRANSITION_CHANGE_STATE(TRANSITION_STATE_PRE_HUD_CHECKS)
				SET_CURRENT_TRANSITION_BAILING_OPTIONS(BAILING_OPTIONS_EMPTY)
				g_iForceDLCStoreSetUp =  ciPROCESS_MPHUD_COMPAT_FAILED_INT
				PRINTLN("PROCESS_MPHUD_COMPAT_FAILED - g_iForceDLCStoreSetUp = ciPROCESS_MPHUD_COMPAT_FAILED_INT")
			ENDIF
			IF bTimedOut
			#IF IS_DEBUG_BUILD
			//Or we should force the fail
			OR GET_COMMANDLINE_PARAM_EXISTS("sc_ForceClocudRequestsFailStore")
			#ENDIF
				g_iForceDLCStoreSetUp = ciPROCESS_MPHUD_COMPAT_FAILED_TIME_OUT
				PRINTLN("PROCESS_MPHUD_COMPAT_FAILED - g_iForceDLCStoreSetUp = ciPROCESS_MPHUD_COMPAT_FAILED_TIME_OUT")
			ENDIF
		BREAK
		
		//Deal with failure
		CASE ciPROCESS_MPHUD_COMPAT_FAILED_TIME_OUT
			//draw the message
			RENDER_MPHUD_COMPAT_DOWNLOAD_TIME_OUT(Placement)
			//Deal with the player press accept
			LOGIC_MPHUD_COMPAT_DOWNLOAD_TIME_OUT()
		BREAK		
	ENDSWITCH
			
ENDPROC

//Deal with the time out of downloading
PROC PROCESS_MPHUD_COMPAT_DOWLOAD_TIME_OUT(MPHUD_PLACEMENT_TOOLS& Placement)
	//If the initial boot screen is active
	IF GET_IS_LOADING_SCREEN_ACTIVE()		
		// Clean up the current loading
		IF NOT (Placement.bHudScreenInitialised)
			RUN_MP_DISCONNECTION_CLEANUP(Placement, TRUE)	
			Placement.bHudScreenInitialised = TRUE
		ENDIF		
		//Not set up the loading screen
		IF g_iForceDLCStoreSetUp = 0
			//Not in the sky
			IF GET_SKYSWOOP_STAGE() != SKYSWOOP_INSKYSTATIC
				SET_SKYSWOOP_UP(FALSE, FALSE, FALSE, SWITCH_TYPE_LONG, TRUE, 0,0,0,FALSE)
			 	PRINTLN("PROCESS_MPHUD_COMPAT_FAILED - GET_SKYSWOOP_STAGE() != SKYSWOOP_INSKYSTATIC")
				EXIT
			ENDIF		
			//Clean up the loading screen
			NET_NL()NET_PRINT("PROCESS_MPHUD_COMPAT_FAILED: SHUTDOWN_LOADING_SCREEN ")NET_NL()
			SHUTDOWN_LOADING_SCREEN()
			SET_JOINING_SPINNER_LOADING(FALSE)			
		 	PRINTLN("PROCESS_MPHUD_COMPAT_FAILED - GET_IS_LOADING_SCREEN_ACTIVE = TRUE")		
			g_iForceDLCStoreSetUp++
		ENDIF			
	ENDIF
	
	// initialise data
	IF NOT (Placement.bHudScreenInitialised)
		RUN_MP_DISCONNECTION_CLEANUP(Placement)	
		Placement.bHudScreenInitialised = TRUE
	ENDIF	
	//Call Brenda's clean up
	RUN_MP_DISCONNECTION_CLEANUP_EVERY_FRAME()
	//draw the message
	RENDER_MPHUD_COMPAT_DOWNLOAD_TIME_OUT(Placement)
	//Deal with the player press accept
	LOGIC_MPHUD_COMPAT_DOWNLOAD_TIME_OUT()
ENDPROC


