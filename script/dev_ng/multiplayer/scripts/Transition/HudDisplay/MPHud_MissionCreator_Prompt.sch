//
//
//USING "net_events.sch"
//USING "script_network.sch"
//USING "Screens_Header.sch"
////USING "net_lobby.sch"
//
//USING "Transition_Common.sch"
//
//INT MAX_NUMBER_CREATOR_PROMPT_SELECTIONS = 5
//
//TRANSITION_CREATOR_MENU_CHOICE CREATORMenuItems[5]
//BOOL isrockstarDev
//
//PROC CLEAR_CREATOR_CUSTOM_MENU_ITEMS()
//	INT i
//	REPEAT MAX_NUMBER_CREATOR_PROMPT_SELECTIONS i
//		CREATORMenuItems[i] = CREATOR_MENU_CHOICE_EMPTY	
//	ENDREPEAT
//ENDPROC
//
//FUNC INT NO_OF_CREATOR_CUSTOM_MENU_OPTIONS()
//	INT i
//	INT iCount = 0
//	REPEAT MAX_NUMBER_CREATOR_PROMPT_SELECTIONS i
//		IF NOT (CREATORMenuItems[i] = CREATOR_MENU_CHOICE_EMPTY)	
//			IF CREATORMenuItems[i] =CREATOR_MENU_CHOICE_CUSTOM_CREATOR
//		 		IF isrockstarDev  
//					iCount++
//				ENDIF
//			ELSE
//				iCount++
//			ENDIF
//			
//		ENDIF
//	ENDREPEAT
//	RETURN(iCount)
//ENDFUNC
//
//
//PROC SETUP_CREATOR_MENU_ITEMS()
//	CLEAR_CREATOR_CUSTOM_MENU_ITEMS()
//	
//
//		
////	CREATORMenuItems[0] = CREATOR_MENU_CHOICE_CREATE_MISSION
//	CREATORMenuItems[1] = CREATOR_MENU_CHOICE_CREATE_DEATHMATCH
//	CREATORMenuItems[2] = CREATOR_MENU_CHOICE_CREATE_RACE
//	IF isrockstarDev
//		CREATORMenuItems[3] = CREATOR_MENU_CHOICE_CUSTOM_CREATOR
//		CREATORMenuItems[4] = CREATOR_MENU_CHOICE_RETURN_TO_ONLINE
//	ELSE
//		CREATORMenuItems[3] = CREATOR_MENU_CHOICE_RETURN_TO_ONLINE
//	ENDIF
//	
//		
//	
//	IF (g_iMenuSelection[HUDMENU_MISSION_CREATOR] > (NO_OF_CREATOR_CUSTOM_MENU_OPTIONS() - 1))
//		g_iMenuSelection[HUDMENU_MISSION_CREATOR] = NO_OF_CREATOR_CUSTOM_MENU_OPTIONS() -1
//	ENDIF
//	IF (g_iMenuSelection[HUDMENU_MISSION_CREATOR] = -1)
//		g_iMenuSelection[HUDMENU_MISSION_CREATOR] = 0
//	ENDIF	
//ENDPROC
//
//
//PROC INIT_MPHUD_CREATOR_PROMPT(MPHUD_PLACEMENT_TOOLS& Placement)
//
//	RESET_ALL_SPRITE_PLACEMENT_VALUES(Placement.SpritePlacement)
//
//		
//	Placement.TextPlacement[0].x = 0.500
//	Placement.TextPlacement[0].y = 0.178
//	
//	Placement.TextPlacement[1].x = 0.500
//	Placement.TextPlacement[1].y = 0.209
//	
//	Placement.TextPlacement[2].x = 0.500
//	Placement.TextPlacement[2].y = 0.240
//	
//	Placement.TextPlacement[3].x = 0.500
//	Placement.TextPlacement[3].y = 0.271
//	
//	Placement.TextPlacement[4].x = 0.500
//	Placement.TextPlacement[4].y = 0.302
//	
//	
//	//Top bar
//	Placement.SpritePlacement[0].x = 0.500
//	Placement.SpritePlacement[0].y = 0.190
//	Placement.SpritePlacement[0].w = 0.675
//	Placement.SpritePlacement[0].h = 0.045
//	Placement.SpritePlacement[0].r = 255
//	Placement.SpritePlacement[0].g = 255
//	Placement.SpritePlacement[0].b = 255
//	Placement.SpritePlacement[0].a = 200
//	
//	//2nd Bar 
//	Placement.SpritePlacement[1].x = 0.5
//	Placement.SpritePlacement[1].y = 0.222
//	Placement.SpritePlacement[1].w = 0.675
//	Placement.SpritePlacement[1].h = 0.045
//	Placement.SpritePlacement[1].r = 255
//	Placement.SpritePlacement[1].g = 255
//	Placement.SpritePlacement[1].b = 255
//	Placement.SpritePlacement[1].a = 200
//	
//	//3rd Bar 
//	Placement.SpritePlacement[2].x = 0.5
//	Placement.SpritePlacement[2].y = 0.254
//	Placement.SpritePlacement[2].w = 0.675
//	Placement.SpritePlacement[2].h = 0.045
//	Placement.SpritePlacement[2].r = 255
//	Placement.SpritePlacement[2].g = 255
//	Placement.SpritePlacement[2].b = 255
//	Placement.SpritePlacement[2].a = 200
//	
//	//4th Bar 
//	Placement.SpritePlacement[3].x = 0.5
//	Placement.SpritePlacement[3].y = 0.286
//	Placement.SpritePlacement[3].w = 0.675
//	Placement.SpritePlacement[3].h = 0.045
//	Placement.SpritePlacement[3].r = 255
//	Placement.SpritePlacement[3].g = 255
//	Placement.SpritePlacement[3].b = 255
//	Placement.SpritePlacement[3].a = 200
//	
//	//4th Bar 
//	Placement.SpritePlacement[4].x = 0.5
//	Placement.SpritePlacement[4].y = 0.318
//	Placement.SpritePlacement[4].w = 0.675
//	Placement.SpritePlacement[4].h = 0.045
//	Placement.SpritePlacement[4].r = 255
//	Placement.SpritePlacement[4].g = 255
//	Placement.SpritePlacement[4].b = 255
//	Placement.SpritePlacement[4].a = 200
//	
//	
//ENDPROC
//
//PROC RENDER_MPHUD_CREATOR_PROMPT(MPHUD_PLACEMENT_TOOLS& Placement)
//
//
////	DRAW_TEXT(Placement.MainTitle, Placement.aStyle.TS_TITLEHUD, "HUD_MAINTIT")	
////	DRAW_JOIN_HUD_BACKGROUND()
//	
//	SETUP_CREATOR_MENU_ITEMS()
//	
//	VECTOR SlotOffset[5]
//	VECTOR SlotHeading[5]
//		
////	SlotOffset[0] = <<-1.352, 3.250, -0.085>>
////	SlotHeading[0] = <<331.200,7.920, 223.920>>
//	
//	SET_PED_SLOT_OFFSETS(SlotOffset, SlotHeading)
//
//		
//	VECTOR PlaceGuy, HeadGuy
//	VECTOR CamRot
//	VECTOR CamCoord
//	IF DOES_CAM_EXIST(GET_RENDERING_CAM())
//		CamRot = GET_CAM_ROT(GET_RENDERING_CAM())
//		CamCoord = GET_CAM_COORD(GET_RENDERING_CAM())
//	ENDIF
//
//	
//	PlaceGuy = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(CamCoord,CamRot.z, SlotOffset[0])
//	HeadGuy = CamRot+SlotHeading[0] 
//	INT I
//
//	IF GET_CURRENT_GAMEMODE() = GAMEMODE_FM
//	OR GET_JOINING_GAMEMODE() = GAMEMODE_FM
//	
//		
//		FOR I = 0 TO MAX_NUM_CHARACTER_SLOTS-1
//			IF IS_STAT_CHARACTER_ACTIVE(I)
//				RUN_PED_MENU(Placement.IdlePed[I], 
//						GET_PLAYER_MODEL_FOR_TEAM_VARIATION_FOR_MENU( GET_STAT_CHARACTER_TEAM(I), GET_PACKED_STAT_INT(PACKED_CHAR_PICTURE, I)) 
//						, PlaceGuy, HeadGuy, FALSE,IS_CHARACTER_MALE(I), I)	
//			ENDIF
//		ENDFOR
//
//		SET_ALL_MENUPED_VISIBLE(Placement.SelectionPed,Placement.IdlePed, FALSE)
//	ENDIF
//
//	TEXT_STYLE TS_TEXT
//
//	SET_STANDARD_MEDIUM_HUD_TEXT(TS_TEXT)
//
//	INT iCount
//	REPEAT NO_OF_CREATOR_CUSTOM_MENU_OPTIONS() i
//		IF NOT (CREATORMenuItems[i] = CREATOR_MENU_CHOICE_EMPTY)
//			SWITCH CREATORMenuItems[i]
//
////				CASE CREATOR_MENU_CHOICE_CREATE_MISSION
////					IF g_iMenuSelection[HUDMENU_MISSION_CREATOR] = iCount
////					
////						SET_TEXT_BLACK(TS_TEXT)
////						IF NETWORK_IS_CLOUD_AVAILABLE()
////							DRAW_TEXT_WITH_ALIGNMENT(Placement.TextPlacement[iCount], TS_TEXT, "HUD_TITLE11", TRUE, FALSE)
////						ELSE
////							DRAW_TEXT_WITH_GXT_STRING(Placement.TextPlacement[iCount], TS_TEXT,"HUD_NOCLOUD", "HUD_TITLE11", FONT_CENTRE)
////						ENDIF
////						SET_TEXT_WHITE(TS_TEXT)
////						DRAW_2D_SPRITE("MPEntry", "MP_ModeSelected_Gradient", Placement.SpritePlacement[iCount], FALSE)
////					
////					ELSE
////					
////						IF NETWORK_IS_CLOUD_AVAILABLE()
////							DRAW_TEXT_WITH_ALIGNMENT(Placement.TextPlacement[iCount], TS_TEXT, "HUD_TITLE11", TRUE, FALSE)
////							DRAW_2D_SPRITE("MPEntry", "MP_ModeNotSelected_Gradient", Placement.SpritePlacement[iCount], FALSE)
////						ELSE
////							SET_TEXT_GREY(TS_TEXT)
////							DRAW_TEXT_WITH_GXT_STRING(Placement.TextPlacement[iCount], TS_TEXT,"HUD_NOCLOUD", "HUD_TITLE11", FONT_CENTRE)
////							SET_TEXT_WHITE(TS_TEXT)
////							DRAW_2D_SPRITE("MPEntry", "MP_ModeNotSelected_Gradient", Placement.SpritePlacement[iCount], FALSE)
////						ENDIF
////					
////					ENDIF
////				BREAK
////				
//				CASE CREATOR_MENU_CHOICE_CREATE_DEATHMATCH
//					IF g_iMenuSelection[HUDMENU_MISSION_CREATOR] = iCount
//					
//						SET_TEXT_BLACK(TS_TEXT)
//						IF NETWORK_IS_CLOUD_AVAILABLE()
//							DRAW_TEXT_WITH_ALIGNMENT(Placement.TextPlacement[iCount], TS_TEXT, "HUD_TITLE12", TRUE, FALSE)
//						ELSE
//							DRAW_TEXT_WITH_GXT_STRING(Placement.TextPlacement[iCount], TS_TEXT,"HUD_NOCLOUD", "HUD_TITLE12", FONT_CENTRE)
//						ENDIF
//						SET_TEXT_WHITE(TS_TEXT)
//						DRAW_2D_SPRITE("MPEntry", "MP_ModeSelected_Gradient", Placement.SpritePlacement[iCount], FALSE)
//					
//					ELSE
//					
//						IF NETWORK_IS_CLOUD_AVAILABLE()
//							DRAW_TEXT_WITH_ALIGNMENT(Placement.TextPlacement[iCount], TS_TEXT, "HUD_TITLE12", TRUE, FALSE)
//							DRAW_2D_SPRITE("MPEntry", "MP_ModeNotSelected_Gradient", Placement.SpritePlacement[iCount], FALSE)
//						ELSE
//							SET_TEXT_GREY(TS_TEXT)
//							DRAW_TEXT_WITH_GXT_STRING(Placement.TextPlacement[iCount], TS_TEXT,"HUD_NOCLOUD", "HUD_TITLE12", FONT_CENTRE)
//							SET_TEXT_WHITE(TS_TEXT)
//							DRAW_2D_SPRITE("MPEntry", "MP_ModeNotSelected_Gradient", Placement.SpritePlacement[iCount], FALSE)
//						ENDIF
//					
//					ENDIF
//				BREAK
//				
//				CASE CREATOR_MENU_CHOICE_CREATE_RACE
//					IF g_iMenuSelection[HUDMENU_MISSION_CREATOR] = iCount
//					
//						SET_TEXT_BLACK(TS_TEXT)
//						IF NETWORK_IS_CLOUD_AVAILABLE()
//							DRAW_TEXT_WITH_ALIGNMENT(Placement.TextPlacement[iCount], TS_TEXT, "HUD_TITLE13", TRUE, FALSE)
//						ELSE
//							DRAW_TEXT_WITH_GXT_STRING(Placement.TextPlacement[iCount], TS_TEXT,"HUD_NOCLOUD", "HUD_TITLE13", FONT_CENTRE)
//						ENDIF
//						SET_TEXT_WHITE(TS_TEXT)
//						DRAW_2D_SPRITE("MPEntry", "MP_ModeSelected_Gradient", Placement.SpritePlacement[iCount], FALSE)
//					
//					ELSE
//					
//						IF NETWORK_IS_CLOUD_AVAILABLE()
//							DRAW_TEXT_WITH_ALIGNMENT(Placement.TextPlacement[iCount], TS_TEXT, "HUD_TITLE13", TRUE, FALSE)
//							DRAW_2D_SPRITE("MPEntry", "MP_ModeNotSelected_Gradient", Placement.SpritePlacement[iCount], FALSE)
//						ELSE
//							SET_TEXT_GREY(TS_TEXT)
//							DRAW_TEXT_WITH_GXT_STRING(Placement.TextPlacement[iCount], TS_TEXT,"HUD_NOCLOUD", "HUD_TITLE13", FONT_CENTRE)
//							SET_TEXT_WHITE(TS_TEXT)
//							DRAW_2D_SPRITE("MPEntry", "MP_ModeNotSelected_Gradient", Placement.SpritePlacement[iCount], FALSE)
//						ENDIF
//					
//					ENDIF
//				BREAK
//				
//				CASE CREATOR_MENU_CHOICE_CUSTOM_CREATOR
//					IF g_iMenuSelection[HUDMENU_MISSION_CREATOR] = iCount
//						SET_TEXT_BLACK(TS_TEXT)
//						DRAW_TEXT_WITH_ALIGNMENT(Placement.TextPlacement[iCount], TS_TEXT, "HUD_CUSCREATOR", TRUE, FALSE)
//						SET_TEXT_WHITE(TS_TEXT)
//						DRAW_2D_SPRITE("MPEntry", "MP_ModeSelected_Gradient", Placement.SpritePlacement[iCount], FALSE)
//					ELSE
//						DRAW_TEXT_WITH_ALIGNMENT(Placement.TextPlacement[iCount], TS_TEXT, "HUD_CUSCREATOR", TRUE, FALSE)
//						DRAW_2D_SPRITE("MPEntry", "MP_ModeNotSelected_Gradient", Placement.SpritePlacement[iCount], FALSE)
//					ENDIF
//				
//				BREAK
//				
//				CASE CREATOR_MENU_CHOICE_RETURN_TO_ONLINE
//					IF g_iMenuSelection[HUDMENU_MISSION_CREATOR] = iCount
//						SET_TEXT_BLACK(TS_TEXT)
//						DRAW_TEXT_WITH_ALIGNMENT(Placement.TextPlacement[iCount], TS_TEXT, "HUD_RETURNMP", TRUE, FALSE)
//						SET_TEXT_WHITE(TS_TEXT)
//						DRAW_2D_SPRITE("MPEntry", "MP_ModeSelected_Gradient", Placement.SpritePlacement[iCount], FALSE)
//					ELSE
//						DRAW_TEXT_WITH_ALIGNMENT(Placement.TextPlacement[iCount], TS_TEXT, "HUD_RETURNMP", TRUE, FALSE)
//						DRAW_2D_SPRITE("MPEntry", "MP_ModeNotSelected_Gradient", Placement.SpritePlacement[iCount], FALSE)
//					ENDIF
//				
//				BREAK
//				
//				
//				
//				
//			
//			ENDSWITCH
//			iCount++
//		ENDIF
//	ENDREPEAT
//	
//
//
//	
//	SPRITE_PLACEMENT ScaleformSprite
//	
//
////	SCALEFORM_TABS_INPUT_DATA TabsData
////	TabsData.MainTitle = "HUD_MAINTIT"
////	ScaleformSprite = GET_SCALEFORM_TABS_POSITION()
////	INT PotentialPed = GET_JOINING_CHARACTER()
////	COMPILE_SCALEFORM_TABS(Placement.IdlePed[PotentialPed], TabsData, Placement.ScaleformTabStruct )
////	RUN_SCALEFORM_TABS(Placement.ButtonMovie[GET_SCALEFORM_INDEX_FOR_HUD(HUD_SCALEFORM_TABS)], ScaleformSprite, Placement.ScaleformTabStruct)
////	
//
//	ScaleformSprite = GET_SCALEFORM_INSTRUCTIONAL_BUTTON_POSITION()
//
//	
//	ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT), "HUD_INPUT2", Placement.ScaleformStruct)
//	ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL), "HUD_INPUT3", Placement.ScaleformStruct)
//	ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_GROUP_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUTGROUP_FRONTEND_DPAD_UD), "HUD_INPUT1", Placement.ScaleformStruct)
//	
//	
//	RUN_SCALEFORM_INSTRUCTIONAL_BUTTONS(Placement.ButtonMovie[GET_SCALEFORM_INDEX_FOR_HUD(HUD_SCALEFORM_INSTRUCTIONAL)], ScaleformSprite, Placement.ScaleformStruct)
//
//
//	
//ENDPROC
//
//PROC LOGIC_MPHUD_CREATOR_PROMPT()
//
//
//	IF HAS_HUD_CONTROL_BEEN_PRESSED(INPUT_FRONTEND_ACCEPT)
//		
//		SET_CURRENT_TRANSITION_CREATOR_MENU_CHOICE(CREATORMenuItems[g_iMenuSelection[HUDMENU_MISSION_CREATOR]])
//		SWITCH CREATORMenuItems[g_iMenuSelection[HUDMENU_MISSION_CREATOR]]		
//			
////			CASE CREATOR_MENU_CHOICE_CREATE_MISSION	
////				NET_NL()NET_PRINT("LOGIC_MPHUD_CREATOR_PROMPT: CREATOR_MENU_CHOICE_CREATE_MISSION SET ")
////				HUD_CHANGE_STATE(HUD_STATE_MISSION_CREATOR_LOBBY_PROMPT)
////				g_FMMC_STRUCT.iMissionType = 0
////			BREAK
////			CASE CREATOR_MENU_CHOICE_CREATE_DEATHMATCH
////				NET_NL()NET_PRINT("LOGIC_MPHUD_CREATOR_PROMPT: CREATOR_MENU_CHOICE_CREATE_DEATHMATCH SET ")
////				HUD_CHANGE_STATE(HUD_STATE_DM_CREATOR_LOBBY_PROMPT)
////				g_FMMC_STRUCT.iMissionType = 1
////			BREAK
////			CASE CREATOR_MENU_CHOICE_CREATE_RACE
////				NET_NL()NET_PRINT("LOGIC_MPHUD_CREATOR_PROMPT: CREATOR_MENU_CHOICE_CREATE_RACE SET ")
////				HUD_CHANGE_STATE(HUD_STATE_RACE_CREATOR_LOBBY_PROMPT)
////				g_FMMC_STRUCT.iMissionType = 2
////			BREAK
//			
////			CASE CREATOR_MENU_CHOICE_RETURN_TO_MENU
////				HUD_CHANGE_STATE(HUD_STATE_MODE_SELECTION)
////			BREAK
////			
////			CASE CREATOR_MENU_CHOICE_CUSTOM_CREATOR
////				HUD_CHANGE_STATE(HUD_STATE_CUSTOM_CREATOR_PROMPT)
////			BREAK
//			
//	
//		
//		ENDSWITCH
//		
//			
//		
//	
//	ENDIF
//
//	IF HAS_HUD_CONTROL_BEEN_PRESSED(INPUT_FRONTEND_CANCEL)
//		HUD_CHANGE_STATE(HUD_STATE_MODE_SELECTION)	
//	ENDIF
//
//	IF HAS_HUD_CONTROL_BEEN_PRESSED(INPUT_FRONTEND_UP)
//		g_iMenuSelection[HUDMENU_MISSION_CREATOR] += -1	
//		IF (g_iMenuSelection[HUDMENU_MISSION_CREATOR] < 0)
//			g_iMenuSelection[HUDMENU_MISSION_CREATOR] = MAX_NUMBER_CREATOR_PROMPT_SELECTIONS-1
//		ENDIF
//	ENDIF
//	IF HAS_HUD_CONTROL_BEEN_PRESSED(INPUT_FRONTEND_DOWN)
//		g_iMenuSelection[HUDMENU_MISSION_CREATOR] += 1
//		IF (g_iMenuSelection[HUDMENU_MISSION_CREATOR] >= MAX_NUMBER_CREATOR_PROMPT_SELECTIONS)
//			g_iMenuSelection[HUDMENU_MISSION_CREATOR] = 0
//		ENDIF
//	ENDIF
//	
//	
//	
//ENDPROC
//
//PROC PROCESS_MPHUD_CREATOR_PROMPT(MPHUD_PLACEMENT_TOOLS& Placement)
//
//	isrockstarDev = IS_ROCKSTAR_DEV()
//	#IF IS_DEBUG_BUILD
//		IF g_bIsRockstarDev = TRUE
//			isrockstarDev = TRUE
//		ENDIF
//	#ENDIF
//
//	IF isrockstarDev
//		MAX_NUMBER_CREATOR_PROMPT_SELECTIONS = 5
//	ELSE
//		MAX_NUMBER_CREATOR_PROMPT_SELECTIONS = 4
//	ENDIF
//
//	// initialise data
//	IF NOT (Placement.bHudScreenInitialised)
//
//		INIT_MPHUD_CREATOR_PROMPT(Placement)	
//
//		SET_ALL_MENUPED_VISIBLE(Placement.SelectionPed,Placement.IdlePed, FALSE)
//		Placement.ScreenPlace.iSelection = g_iMenuSelection[HUDMENU_MISSION_CREATOR]
//		Placement.bHudScreenInitialised = TRUE
//
//		NET_NL()NET_PRINT("PROCESS_MPHUD_CREATOR_PROMPT: Placement.ScreenPlace.iSelectedCharacter = ")NET_PRINT_INT(Placement.ScreenPlace.iSelectedCharacter)
//
//	ENDIF
//	
//	LOGIC_MPHUD_CREATOR_PROMPT()
//	
//	IF HAS_ALL_COMMON_HUD_ELEMENTS_LOADED(Placement)	
//		RENDER_MPHUD_CREATOR_PROMPT(Placement)
//	ENDIF
//	
//	
//ENDPROC
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//PROC RENDER_XML_MPHUD_CREATOR_PROMPT(MPHUD_PLACEMENT_TOOLS& Placement)
//	
//
//	INT i 
//	
//	SETUP_CREATOR_MENU_ITEMS()
//
//
//	REPEAT NO_OF_CREATOR_CUSTOM_MENU_OPTIONS() i
//		IF NOT (CREATORMenuItems[i] = CREATOR_MENU_CHOICE_EMPTY)
//			SWITCH CREATORMenuItems[i]
//			
////				CASE CREATOR_MENU_CHOICE_CREATE_MISSION
////					
////					IF NETWORK_IS_CLOUD_AVAILABLE()
////						PRIVATE_ADD_SCALEFORMXML_TEXT_SELECTION(i, "HUD_TITLE11",  Placement.ScaleformXMLTextSelectStruct) //General Freemode
////					ELSE
////						PRIVATE_ADD_SCALEFORMXML_TEXT_SELECTION_NO_CLOUD(i, "HUD_TITLE11",  Placement.ScaleformXMLTextSelectStruct)
////					ENDIF
////				
////				BREAK
//				
//				CASE CREATOR_MENU_CHOICE_CREATE_DEATHMATCH
//
//					IF NETWORK_IS_CLOUD_AVAILABLE()
//						PRIVATE_ADD_SCALEFORMXML_TEXT_SELECTION(i, "HUD_TITLE12",  Placement.ScaleformXMLTextSelectStruct) //General Freemode
//					ELSE
//						PRIVATE_ADD_SCALEFORMXML_TEXT_SELECTION_NO_CLOUD(i, "HUD_TITLE12",  Placement.ScaleformXMLTextSelectStruct)
//					ENDIF
//				
//				BREAK
//				
//				CASE CREATOR_MENU_CHOICE_CREATE_RACE
//					
//					IF NETWORK_IS_CLOUD_AVAILABLE()
//						PRIVATE_ADD_SCALEFORMXML_TEXT_SELECTION(i, "HUD_TITLE13",  Placement.ScaleformXMLTextSelectStruct) //General Freemode
//					ELSE
//						PRIVATE_ADD_SCALEFORMXML_TEXT_SELECTION_NO_CLOUD(i, "HUD_TITLE13",  Placement.ScaleformXMLTextSelectStruct)
//					ENDIF
//				
//				BREAK
//				
//				CASE CREATOR_MENU_CHOICE_CUSTOM_CREATOR
//					
//					IF NETWORK_IS_CLOUD_AVAILABLE()
//						PRIVATE_ADD_SCALEFORMXML_TEXT_SELECTION(i, "HUD_CUSCREATOR",  Placement.ScaleformXMLTextSelectStruct) //General Freemode
//					ELSE
//						PRIVATE_ADD_SCALEFORMXML_TEXT_SELECTION_NO_CLOUD(i, "HUD_CUSCREATOR",  Placement.ScaleformXMLTextSelectStruct)
//					ENDIF
//				
//				BREAK
//				
//				CASE CREATOR_MENU_CHOICE_RETURN_TO_MENU
//				
//					PRIVATE_ADD_SCALEFORMXML_TEXT_SELECTION(i, "HUD_RETURNMP",  Placement.ScaleformXMLTextSelectStruct) //General Freemode
//				
//				
//				BREAK
//				
//				
//				
//			
//			ENDSWITCH
//
//		ENDIF
//	ENDREPEAT
//
//	
//	
//	
//	
//	RUN_SCALEFORMXML_TEXT_SELECTIONS(Placement.ScaleformXMLTextSelectStruct, SHOULD_REFRESH_SCALEFORMXML_TEXT_SELECTION(Placement.ScaleformXMLTextSelectStruct))
//	
//ENDPROC
//
//
//
//
//
//PROC LOGIC_XML_MPHUD_CREATOR_PROMPT(MPHUD_PLACEMENT_TOOLS& Placement)		
//
//
//
//	CONST_INT CANCEL 0
//	CONST_INT ACCEPT 1
//
//
//
//	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_CANCEL)
//		SET_BIT(Placement.ButtonReleasedBitset, CANCEL)
//	ENDIF
//	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
//		SET_BIT(Placement.ButtonReleasedBitset, ACCEPT)
//	ENDIF
//
//
//
//	IF (IS_BIT_SET(Placement.ButtonReleasedBitset, ACCEPT)
//	AND IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT) )
//
//		CLEAR_BIT(Placement.ButtonReleasedBitset, ACCEPT)
//		
//		SET_CURRENT_TRANSITION_CREATOR_MENU_CHOICE(CREATORMenuItems[g_iMenuSelection[HUDMENU_MISSION_CREATOR]])
//		SWITCH CREATORMenuItems[g_iMenuSelection[HUDMENU_MISSION_CREATOR]]		
//			
////			CASE CREATOR_MENU_CHOICE_CREATE_MISSION	
////				NET_NL()NET_PRINT("LOGIC_MPHUD_CREATOR_PROMPT: CREATOR_MENU_CHOICE_CREATE_MISSION SET ")
////				HUD_CHANGE_STATE(HUD_STATE_MISSION_CREATOR_LOBBY_PROMPT)
////				g_FMMC_STRUCT.iMissionType = 0
////			BREAK
//			CASE CREATOR_MENU_CHOICE_CREATE_DEATHMATCH
//				NET_NL()NET_PRINT("LOGIC_MPHUD_CREATOR_PROMPT: CREATOR_MENU_CHOICE_CREATE_DEATHMATCH SET ")
//				HUD_CHANGE_STATE(HUD_STATE_DM_CREATOR_LOBBY_PROMPT)
//				g_FMMC_STRUCT.iMissionType = 1
//			BREAK
//			CASE CREATOR_MENU_CHOICE_CREATE_RACE
//				NET_NL()NET_PRINT("LOGIC_MPHUD_CREATOR_PROMPT: CREATOR_MENU_CHOICE_CREATE_RACE SET ")
//				HUD_CHANGE_STATE(HUD_STATE_RACE_CREATOR_LOBBY_PROMPT)
//				g_FMMC_STRUCT.iMissionType = 2
//			BREAK
//			
//			CASE CREATOR_MENU_CHOICE_CUSTOM_CREATOR
//				NET_NL()NET_PRINT("LOGIC_MPHUD_CREATOR_PROMPT: HUD_STATE_CUSTOM_CREATOR_PROMPT SET ")
//				HUD_CHANGE_STATE(HUD_STATE_CUSTOM_CREATOR_PROMPT)
//				
//			BREAK
//			
//			CASE CREATOR_MENU_CHOICE_RETURN_TO_MENU
//				HUD_CHANGE_STATE(HUD_STATE_MODE_SELECTION)
//			BREAK
//			
//	
//		
//		ENDSWITCH
//
//
//	ENDIF
//	
//	
//	IF IS_BIT_SET(Placement.ButtonReleasedBitset, CANCEL)
//		IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL) 
//			CLEAR_BIT(Placement.ButtonReleasedBitset, CANCEL)
//			HUD_CHANGE_STATE(HUD_STATE_MODE_SELECTION)	
//		ENDIF
//
//	ENDIF
//
//
//
//	
//	
//	IF HAS_HUD_CONTROL_BEEN_PRESSED(INPUT_FRONTEND_CANCEL)
//		HUD_CHANGE_STATE(HUD_STATE_MODE_SELECTION)	
//	ENDIF
//
//
//	
//	
//
//ENDPROC
//
//
//PROC PROCESS_XML_MPHUD_CREATOR_PROMPT(MPHUD_PLACEMENT_TOOLS& Placement)
//	
//	isrockstarDev = IS_ROCKSTAR_DEV()
//	#IF IS_DEBUG_BUILD
//		IF g_bIsRockstarDev = TRUE
//			isrockstarDev = TRUE
//		ENDIF
//	#ENDIF
//	
//	IF IS_PAUSE_MENU_ACTIVE()
//	OR g_TurnOnNewTransitionSecondarySystem
//		// initialise data
//		IF NOT (Placement.bHudScreenInitialised)
//			Placement.bDoIHaveControl = TRUE
//			IF IS_PAUSE_MENU_ACTIVE()
//				IF IS_THIS_SCREEN_ACTIVE(FE_MENU_VERSION_TEXT_SELECTION) = FALSE
//					Placement.bDoIHaveControl = FALSE
//					RESTART_FRONTEND_MENU(FE_MENU_VERSION_TEXT_SELECTION)
//					NET_NL()NET_PRINT("PROCESS_XML_MPHUD_CREATOR_PROMPT: ACTIVATE_FRONTEND_MENU")
//				ENDIF
//
//			ELSE
//				Placement.bDoIHaveControl = FALSE
//				ACTIVATE_FRONTEND_MENU(FE_MENU_VERSION_TEXT_SELECTION, FALSE)
//				NET_NL()NET_PRINT("PROCESS_XML_MPHUD_CREATOR_PROMPT: ACTIVATE_FRONTEND_MENU")
//			ENDIF
//			SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_FADE)
//			
//			IF isrockstarDev
//				MAX_NUMBER_CREATOR_PROMPT_SELECTIONS = 5
//			ELSE
//				MAX_NUMBER_CREATOR_PROMPT_SELECTIONS = 4
//			ENDIF
//			
//	
//
//			INIT_MPHUD_CREATOR_PROMPT(Placement)	
//
//			SET_ALL_MENUPED_VISIBLE(Placement.SelectionPed,Placement.IdlePed, FALSE)
//			Placement.ScreenPlace.iSelection = g_iMenuSelection[HUDMENU_MISSION_CREATOR]
//			Placement.bHudScreenInitialised = TRUE
//
//			NET_NL()NET_PRINT("PROCESS_XML_MPHUD_CREATOR_PROMPT: Placement.ScreenPlace.iSelectedCharacter = ")NET_PRINT_INT(Placement.ScreenPlace.iSelectedCharacter)
//
//		ENDIF
//		
//			
//		IF (Placement.bHudScreenInitialised)
//		AND IS_PAUSE_MENU_ACTIVE()
//		AND NOT IS_PAUSE_MENU_RESTARTING()
//			IF IS_FRONTEND_READY_FOR_CONTROL()
//			
//				IF HAS_MENU_TRIGGER_EVENT_OCCURRED()
//					NET_NL()NET_PRINT("HAS_MENU_TRIGGER_EVENT_OCCURRED = TRUE")
//					GET_MENU_TRIGGER_EVENT_DETAILS(Placement.aNextScreen, Placement.iMenu)
//					NET_NL()NET_PRINT("		- aNextScreen = ")NET_PRINT(GET_XML_SCREEN_STRING_VALUE(Placement.aNextScreen))
//					NET_NL()NET_PRINT("		- iMenu = ")NET_PRINT_INT(Placement.iMenu)
//				ENDIF
//				
//				IF HAS_MENU_LAYOUT_CHANGED_EVENT_OCCURRED()
//	 
//					NET_NL()NET_PRINT("HAS_MENU_LAYOUT_CHANGED_EVENT_OCCURRED = TRUE")
//					GET_MENU_LAYOUT_CHANGED_EVENT_DETAILS(Placement.CurrentScreen, Placement.aNextScreen, Placement.iMenu)
//					NET_NL()NET_PRINT("		- CurrentScreen = ")NET_PRINT(GET_XML_SCREEN_STRING_VALUE(Placement.CurrentScreen))
//					NET_NL()NET_PRINT("		- aNextScreen = ")NET_PRINT(GET_XML_SCREEN_STRING_VALUE(Placement.aNextScreen))
//					NET_NL()NET_PRINT("		- iMenu = ")NET_PRINT_INT(Placement.iMenu)
//						
//					MPLOBBY_SET_TITLE("HUD_MAINTIT", Placement.bSetupTitle)
//					STRING PlayerName = GET_PLAYER_NAME(PLAYER_ID())
//					TEXT_LABEL_63 CrewName = GET_LOCAL_PLAYER_CREW_NAME()
//					MPLOBBY_SET_HEADING_DETAILS(PlayerName, CrewName, Placement.bSetupCrewAndName)
//						
//					Placement.bDoIHaveControl = TRUE
//					
//					g_iMenuSelection[HUDMENU_MISSION_CREATOR] = Placement.iMenu
//
//				ENDIF
//		
//				IF Placement.bDoIHaveControl = TRUE	
//					RENDER_XML_MPHUD_CREATOR_PROMPT(Placement)
//					LOGIC_XML_MPHUD_CREATOR_PROMPT(Placement)	
//				ENDIF
//				
//			ELSE
//				NET_NL()NET_PRINT("IS_FRONTEND_READY_FOR_CONTROL = FALSE ")
//			ENDIF
//
//			
//		ENDIF
//		
//
//	ELSE
//		NET_PRINT("PROCESS_XML_MPHUD_CREATOR_PROMPT: IS_PAUSE_MENU_ACTIVE = FALSE") NET_NL()
//	ENDIF
//
//ENDPROC
//
