
USING "screens_header.sch"
USING "net_events.sch"
USING "script_network.sch"
USING "Transition_Common.sch"



PROC RENDER_MPHUD_SERVER_DATA_MESSAGING(MPHUD_PLACEMENT_TOOLS& Placement)


	SET_ALL_MENUPED_VISIBLE(Placement.SelectionPed, Placement.IdlePed, FALSE )		

	//Wanted by Jordan Chew email: 18/08/2015 15:11
	//[HUD_CONGRAT] 
	//congratulations


	STRING sStatementText = "HUD_CONNPROB"
	STRING sQuestionText = ""

	BOOL RPSetReward = FALSE
	BOOL CashReward = FALSE
	BOOL ItemReward = FALSE
	
	INT iRPToAward = 0
	
	IF g_i_DoCashGiftMessageAmount != 0
		CashReward = TRUE
	ENDIF
	
	INT I
	FOR I = 0 TO MAX_NUM_CHARACTER_SLOTS-1
		IF g_b_DoSetRPGiftAdminMessageAmount[I] != 0
			RPSetReward = TRUE
			iRPToAward += g_b_DoSetRPGiftAdminMessageAmount[I]
		ENDIF	
	ENDFOR	
	
	INT Index = GET_NEXT_FILLED_QUICKFIX_FEED_ELEMENT_INDEX()
	IF Index > -1 
		ItemReward = TRUE
	ENDIF
	
	IF g_b_HasItemBeenGivenOnPc
		ItemReward = TRUE
	ENDIF
	
//	INT Rank

	FE_WARNING_FLAGS buttons = FE_WARNING_OK
	
	INT WhichScreen = GET_MP_INT_PLAYER_STAT(MPPLY_SCADMIN_REWARD)
	
	
	SWITCH Placement.iProcessedServerGiftsStages
		
		CASE 0
		

			IF RPSetReward = FALSE
			AND ItemReward = FALSE
			AND CashReward = TRUE
				//Cash only
				Placement.iProcessedServerGiftsStages = 1
			
			ELIF RPSetReward = FALSE
			AND ItemReward = TRUE
			AND CashReward = FALSE
				//Item only
				Placement.iProcessedServerGiftsStages = 2
		
			ELIF RPSetReward = TRUE
			AND ItemReward = FALSE
			AND CashReward = FALSE
				//RP only
				Placement.iProcessedServerGiftsStages = 3
				
			ELIF RPSetReward = FALSE
			AND ItemReward = TRUE
			AND CashReward = TRUE
				//RP only
				Placement.iProcessedServerGiftsStages = 4
				
			ELIF RPSetReward = TRUE
			AND ItemReward = FALSE
			AND CashReward = TRUE
				//RP only
				Placement.iProcessedServerGiftsStages = 5
				
			ELIF RPSetReward = TRUE
			AND ItemReward = TRUE
			AND CashReward = FALSE
				//RP only
				Placement.iProcessedServerGiftsStages = 6
				
			ELIF RPSetReward = TRUE
			AND ItemReward = TRUE
			AND CashReward = TRUE
				//RP only
				Placement.iProcessedServerGiftsStages = 7
				
			ELSE
				NET_NL()NET_PRINT("[GIFTTOOL] RENDER_MPHUD_SERVER_DATA_MESSAGING: Stat set but nothing came through ")
				Placement.iProcessedServerGiftsStages = 0 
				g_b_HasItemBeenGivenOnPc = FALSE
				g_i_SCAdminCashGiftScreenType = 0
				SET_MP_INT_PLAYER_STAT(MPPLY_SCADMIN_REWARD, 0)
				HUD_CHANGE_STATE(HUD_STATE_LOADING)
				TRANSITION_CHANGE_STATE(TRANSITION_STATE_PRE_FM_LAUNCH_SCRIPT)
				
			ENDIF
		BREAK
	

		CASE 1 //cash only
		
			SWITCH WhichScreen
				DEFAULT
					sQuestionText = "HUD_CASHREWARD" //Congratulations, you have been awarded $~1~.
				BREAK
				CASE 2
					sQuestionText = "HUD_CASHREWARD1" //We have adjusted your account by $~1~ in response to your support request.
				BREAK
				CASE 3
					sQuestionText = "HUD_CASHREWARD2" //We have adjusted your account by $~1~ as part of an account reconciliation.
				BREAK
				CASE 4
					sQuestionText = "HUD_CASHREWARD3" //We have applied a rebate of $~1~ to your account as part of your recent Shark Card purchase.
				BREAK
				CASE 5
					sQuestionText = "HUD_CASHREWARD4" //Congratulations, we have applied your rebate of $~1~ for your promotional item purchase.
				BREAK
				CASE 6
					sQuestionText = "HUD_CASHREWARD5" //Thank you for being a loyal GTA Online player! Please enjoy your exclusive $~1~ award.
				BREAK
				CASE 7
					sQuestionText = "HUD_CASHREWARD6" //For being a dedicated GTA Online Player, we are awarding you $~1~. 
				BREAK
				
				//
				CASE 9
					sQuestionText = "HUD_CASHREWARD9" //Congratulations, you have been awarded [GIFT VALUE] for participating in GTA Online promotions.
				BREAK
				CASE 10
					sQuestionText = "HUD_CASHREWARD10" //Congratulations, you have been awarded [GIFT VALUE] for participating in Rockstar promotions.
				BREAK
				
				//
				CASE 11
					sQuestionText = "HUD_CASHREWARD11" //Congratulations, your login gift of [GIFT VALUE] has been deposited to your Maze Bank account.
				BREAK
				CASE 12
					sQuestionText = "HUD_CASHREWARD12" //Congratulations, you have been awarded [GIFT VALUE] for participating in a Rockstar promotion. Stay tuned to the Rockstar Newswire for more ways to earn exclusive gifts.
				BREAK
				CASE 13
					sQuestionText = "HUD_CASHREWARD13" //Congratulations, you have been awarded [GIFT VALUE] for participating in the Rockstar livestream. Stay tuned to the Rockstar Newswire for more ways to earn exclusive gifts.
				BREAK
				CASE 14
					sQuestionText = "HUD_CASHREWARD14" //Congratulations, your rebate of [GIFT VALUE] for participating in a Rockstar promotion has been deposited to your Maze Bank account.
				BREAK
				CASE 15
					sQuestionText = "HUD_CASHREWARD15" //Congratulations, your San Andreas State-sponsored rebate of $~1~ has been deposited to your Maze Bank account.
				BREAK
				CASE 16
					sQuestionText = "HUD_CASHREWARD16" //Congratulations, your San Andreas State-sponsored gift of [GIFT VALUE] has been deposited to your Maze Bank account.
				BREAK
				CASE 17
					sQuestionText = "HUD_CASHREWARD17" //Congratulations, your holiday gift of $~1~ has been deposited to your Maze Bank account. 
				BREAK
				CASE 18
					sQuestionText = "HUD_CASHREWARD18" //Congratulations, you have been awarded $~1~ for participating in a Twitch Prime promotion. Stay tuned to the Rockstar Newswire for all the latest benefits.
				BREAK
				CASE 19
					IF IS_PLAYSTATION_PLATFORM()
						sQuestionText = "HUD_CASHREWARD19" //Congratulations, you have been awarded $~1~ for participating in a PlayStation®Plus promotion. Stay tuned to the Rockstar Newswire for all the latest benefits.
					ELSE
						sQuestionText = "HUD_CASHREWARD" //Congratulations, you have been awarded $~1~.
					ENDIF
				BREAK
			ENDSWITCH

			IF IS_PC_VERSION()
				SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sQuestionText, buttons, DEFAULT, TRUE, g_i_DoCashGiftACTUALAmount)
			ELSE
				SET_WARNING_MESSAGE_WITH_HEADER_AND_SUBSTRING_FLAGS(sStatementText, sQuestionText, buttons, "", FALSE, -1, WARNING_MESSAGE_FIRST_SUBSTRING_IS_LITERAL, g_tl_CashGiftAmount)
			ENDIF
		BREAK
		
		CASE 2 //Item only
			SWITCH WhichScreen
				DEFAULT
					sQuestionText = "HUD_ITEMREWARD" //Congratulations, you have been awarded in-game items.
				BREAK
				CASE 1
					sQuestionText = "HUD_CASHREWARD7" //We have adjusted your account’s inventory as part of an account reconciliation.
				BREAK
			ENDSWITCH

			SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sQuestionText, buttons)
		BREAK
		
		CASE 3 //RP only
			SWITCH WhichScreen
				CASE 1
					sQuestionText = "HUD_RPREWARD1" //Congratulations, you have been awarded ~1~ RP for participating in a Twitch Prime promotion. Stay tuned to the Rockstar Newswire for all the latest benefits.
				BREAK
				CASE 2
					IF IS_PLAYSTATION_PLATFORM()
						sQuestionText = "HUD_RPREWARD2" //Congratulations, you have been awarded ~1~ RP for participating in a PlayStation®Plus promotion. Stay tuned to the Rockstar Newswire for all the latest benefits.
					ELSE
						sQuestionText = "HUD_RPREWARD"  //Congratulations, you have been awarded additional RP.
						SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sQuestionText, buttons)
						EXIT
					ENDIF
				BREAK
				DEFAULT
					sQuestionText = "HUD_RPREWARD"  //Congratulations, you have been awarded additional RP.
					SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sQuestionText, buttons)
					EXIT
				BREAK
			ENDSWITCH
			
			SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sQuestionText, buttons, DEFAULT, TRUE, iRPToAward)
		BREAK
		
		CASE 4 //Cash and item 
			sQuestionText = "HUD_IC_REW"
			IF IS_PC_VERSION()
				SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sQuestionText, buttons, DEFAULT, TRUE, g_i_DoCashGiftACTUALAmount)
			ELSE
				SET_WARNING_MESSAGE_WITH_HEADER_AND_SUBSTRING_FLAGS(sStatementText, sQuestionText, buttons, "", FALSE, -1, WARNING_MESSAGE_FIRST_SUBSTRING_IS_LITERAL, g_tl_CashGiftAmount)
			ENDIF		
		BREAK
		
		CASE 5 //Cash and RP 
			SWITCH WhichScreen
				DEFAULT
					sQuestionText = "HUD_RPC_REW" //Congratulations, you have been awarded $~1~ and additional RP.
				BREAK
				CASE 1
					sQuestionText = "HUD_RPC_REW" //Congratulations, you have been awarded $~1~ and additional RP.
				BREAK
				CASE 2
					sQuestionText = "HUD_CASHREWARD8" //We have adjusted your account by ~a~ and RP has also been removed.
				BREAK
			ENDSWITCH

			
			IF IS_PC_VERSION()
				SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sQuestionText, buttons, DEFAULT, TRUE, g_i_DoCashGiftACTUALAmount)
			ELSE
				SET_WARNING_MESSAGE_WITH_HEADER_AND_SUBSTRING_FLAGS(sStatementText, sQuestionText, buttons, "", FALSE, -1, WARNING_MESSAGE_FIRST_SUBSTRING_IS_LITERAL, g_tl_CashGiftAmount)
			ENDIF		
		BREAK
		
		CASE 6 //item and RP 

			sQuestionText = "HUD_RPI_REW"
//			Rank = GET_RANK_FROM_XP_VALUE(g_b_DoSetRPGiftAdminMessageAmount[iSlotProcessed])
			SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sQuestionText, buttons)

		BREAK
		
		
		CASE 7 //item and RP and Cash
			sQuestionText = "HUD_ICRP_REW"
			IF IS_PC_VERSION()
				SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sQuestionText, buttons, DEFAULT, TRUE, g_i_DoCashGiftACTUALAmount)
			ELSE
				SET_WARNING_MESSAGE_WITH_HEADER_AND_SUBSTRING_FLAGS(sStatementText, sQuestionText, buttons, "", FALSE, -1, WARNING_MESSAGE_FIRST_SUBSTRING_IS_LITERAL, g_tl_CashGiftAmount)
			ENDIF		
		BREAK
		
		
		
	ENDSWITCH
	


	
ENDPROC



PROC LOGIC_MPHUD_SERVER_DATA_MESSAGING(MPHUD_PLACEMENT_TOOLS& Placement)

	SWITCH Placement.iProcessedServerGiftsStages
		
		
		CASE 99
			NET_NL()NET_PRINT("LOGIC_MPHUD_SERVER_DATA_MESSAGING: moving on ")
			Placement.iProcessedServerGiftsStages = 0 
			g_b_HasItemBeenGivenOnPc = FALSE
			g_i_SCAdminCashGiftScreenType = 0
			SET_MP_INT_PLAYER_STAT(MPPLY_SCADMIN_REWARD, 0)
			g_b_skipCashGiftMessage = TRUE
			HUD_CHANGE_STATE(HUD_STATE_LOADING)
			TRANSITION_CHANGE_STATE(TRANSITION_STATE_PRE_FM_LAUNCH_SCRIPT)
		BREAK
		
		DEFAULT
		
			IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
				
				NET_NL()NET_PRINT("LOGIC_MPHUD_SERVER_DATA_MESSAGING: INPUT_FRONTEND_ACCEPT ")
				Placement.iProcessedServerGiftsStages = 99
			ENDIF
		BREAK
		
		
		
	ENDSWITCH


ENDPROC

PROC PROCESS_MPHUD_SERVER_DATA_MESSAGING(MPHUD_PLACEMENT_TOOLS& Placement)
	
	// initialise data
	IF NOT (Placement.bHudScreenInitialised)
		
		Placement.bHudScreenInitialised = TRUE
		
		#IF IS_DEBUG_BUILD
		g_b_IsFakePCGiftsWidgetActive = FALSE
		#ENDIF
		PRINTLN("LOGIC_MPHUD_SERVER_DATA_MESSAGING: g_i_DoCashGiftACTUALAmount = ", g_i_DoCashGiftACTUALAmount)
		PRINTLN("LOGIC_MPHUD_SERVER_DATA_MESSAGING: g_i_SCAdminCashGiftScreenType = ", g_i_SCAdminCashGiftScreenType)
		PRINTLN("LOGIC_MPHUD_SERVER_DATA_MESSAGING: GET_MP_INT_PLAYER_STAT(MPPLY_SCADMIN_REWARD) = ", GET_MP_INT_PLAYER_STAT(MPPLY_SCADMIN_REWARD))
		PRINTLN("LOGIC_MPHUD_SERVER_DATA_MESSAGING: Placement.iProcessedServerGiftsStages = ", Placement.iProcessedServerGiftsStages)

	ENDIF


	RUN_LOADING_SCREEN_SHUTDOWN_WITH_MESSAGE()

	// logic	
	LOGIC_MPHUD_SERVER_DATA_MESSAGING(Placement)
	RENDER_MPHUD_SERVER_DATA_MESSAGING(Placement)

			
ENDPROC

