
USING "net_events.sch"
USING "net_rank_ui.sch"
USING "net_mission.sch"
USING "net_blips.sch"
USING "net_transition.sch"

USING "screen_tabs.sch"
USING "Screens_header.sch"
USING "Transition_Common.sch"
USING "stats_helper_packed.sch"




















PROC LOGIC_XML_MPHUD_SELECTED_JOINING(MPHUD_PLACEMENT_TOOLS& Placement)
	
	CONST_INT CANCEL 0

	
	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_CANCEL)
		SET_BIT(Placement.ButtonReleasedBitset, CANCEL)
	ENDIF

	
	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_CANCEL)	
		REQUEST_TRANSITION_TERMINATE_MP_SESSION()
	ENDIF
	

ENDPROC


PROC PROCESS_XML_MPHUD_SELECTED_JOINING(MPHUD_PLACEMENT_TOOLS& Placement)


		IF Placement.bHudScreenInitialised = FALSE
			IF NOT IS_PAUSE_MENU_RESTARTING()
				INT I
				FOR I = 0 TO MAX_NUM_CHARACTER_SLOTS-1			
					IF IS_STAT_CHARACTER_ACTIVE(I)
						Placement.iTeam[I] = GET_STAT_CHARACTER_TEAM(i)
					ELSE
						Placement.iTeam[I] = -1
					ENDIF
				ENDFOR	
				
				Placement.ScreenPlace.iSelection = 0
				
				
				SET_SCALEFORMXML_TAB_HIGHLIGHT(0, Placement.ScaleformXMLTabStruct)

				NET_NL()NET_PRINT("PROCESS_MPHUD_SELECTED_JOINING: Placement.ScreenPlace.iSelectedCharacter = ")NET_PRINT_INT(Placement.ScreenPlace.iSelectedCharacter)

				SET_ALL_MENUPED_VISIBLE(Placement.SelectionPed, Placement.IdlePed, FALSE )		

				IF SHOULD_ACT_AS_LOADING_SCREEN() 
//					FORCE_SCRIPTED_GFX_WHEN_FRONTEND_ACTIVE(TRUE) 
				ENDIF
				
				IF IS_PAUSE_MENU_ACTIVE()
					NET_NL()NET_PRINT("PROCESS_XML_MPHUD_SELECTED_JOINING: RESTART_FRONTEND_MENU")
					RESTART_FRONTEND_MENU(FE_MENU_VERSION_JOINING_SCREEN)
				ELSE
					NET_NL()NET_PRINT("PROCESS_XML_MPHUD_SELECTED_JOINING: ACTIVATE_FRONTEND_MENU")
					ACTIVATE_FRONTEND_MENU(FE_MENU_VERSION_JOINING_SCREEN, FALSE)
				ENDIF
				
				PAUSE_MENU_DEACTIVATE_CONTEXT( HASH("DISPLAY_CAN_JOIN"))
				PAUSE_MENU_REDRAW_INSTRUCTIONAL_BUTTONS() 
						
				Placement.bsetupTabs = FALSE			
								
		//		Placement.ScreenPlace.iHUDScreenTimer = GET_THE_NETWORK_TIMER()
	//			REFRESH_SCALEFORM_LOADING_ICON(Placement.ScaleformLoadingStruct)
				Placement.bHudScreenInitialised = TRUE
			ENDIF

		ENDIF
	
		IF IS_PAUSE_MENU_ACTIVE()
		AND NOT IS_PAUSE_MENU_RESTARTING()
		AND Placement.bHudScreenInitialised = TRUE
		
//			IF SHOULD_ACT_AS_LOADING_SCREEN() = FALSE
//				
//				
//				IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_Y) 		
//					RUN_GOING_FORWARD_SELECTED_JOINING(Placement)
//				ENDIF
//				
//			ENDIF
			
			
			
			
			IF IS_FRONTEND_READY_FOR_CONTROL()
			

				IF Placement.bturnOnScreen = FALSE
					NET_NL()NET_PRINT("MPLOBBY_DISPLAY_DATA_SLOT")
					MPLOBBY_DISPLAY_DATA_SLOT(0)
					Placement.bturnOnScreen = TRUE
				ENDIF
				
//				SPRITE_PLACEMENT ScaleformSprite = GET_SCALEFORM_LOADING_ICON_POSITION()
				Placement.ScaleformLoadingStruct.sMainStringSlot = GET_TRANSITION_STRING()
				RUN_SCALEFORM_LOADING_ICON(Placement.ScaleformLoadingStruct, SHOULD_REFRESH_SCALEFORM_LOADING_ICON(Placement.ScaleformLoadingStruct))

		
//				IF HAS_MENU_TRIGGER_EVENT_OCCURRED()
//					NET_NL()NET_PRINT("HAS_MENU_TRIGGER_EVENT_OCCURRED = TRUE")
//					GET_MENU_TRIGGER_EVENT_DETAILS(Placement.aNextScreen, Placement.iMenu)
//					NET_NL()NET_PRINT("		- aNextScreen = ")NET_PRINT(GET_XML_SCREEN_STRING_VALUE(Placement.aNextScreen))
//					NET_NL()NET_PRINT("		- iMenu = ")NET_PRINT_INT(Placement.iMenu)
//					
//					SET_JOINING_CHARACTER(Placement.ScreenPlace.iSelectedCharacter)
//					g_MPCharData.bIsCharNew[Placement.ScreenPlace.iSelectedCharacter] = TRUE
//					HUD_CHANGE_STATE(HUD_STATE_CREATE_TEAM)		
//					
//				ENDIF
//				
//				IF HAS_MENU_LAYOUT_CHANGED_EVENT_OCCURRED()
//					REFRESH_SCALEFORMXML_TABS_DISPLAY(Placement.ScaleformXMLTabStruct)
//					
//					NET_NL()NET_PRINT("HAS_MENU_LAYOUT_CHANGED_EVENT_OCCURRED = TRUE")
//					GET_MENU_LAYOUT_CHANGED_EVENT_DETAILS(Placement.CurrentScreen, Placement.aNextScreen, Placement.iMenu)
//					NET_NL()NET_PRINT("		- CurrentScreen = ")NET_PRINT(GET_XML_SCREEN_STRING_VALUE(Placement.CurrentScreen))
//					NET_NL()NET_PRINT("		- aNextScreen = ")NET_PRINT(GET_XML_SCREEN_STRING_VALUE(Placement.aNextScreen))
//					NET_NL()NET_PRINT("		- iMenu = ")NET_PRINT_INT(Placement.iMenu)
//				ENDIF
				
	//
	//			
				IF CODE_WANTS_SCRIPT_TO_TAKE_CONTROL ()
				
				
	//				IF Placement.bDoIHaveControl = FALSE
	//					NET_NL()NET_PRINT("TEAM TAKE_CONTROL_OF_FRONTEND")
	//					TAKE_CONTROL_OF_FRONTEND()	
	//					Placement.bDoIHaveControl = TRUE
	//				ENDIF
					
					IF Placement.bturnOnScreen = FALSE
						NET_NL()NET_PRINT("TEAM MPLOBBY_DISPLAY_DATA_SLOT")
						MPLOBBY_DISPLAY_DATA_SLOT(0)
						Placement.bturnOnScreen = TRUE
					ENDIF
				
				
				
	//				NET_NL()NET_PRINT("CODE_WANTS_SCRIPT_TO_TAKE_CONTROL = TRUE")
	//				Placement.ControlScreen = GET_SCREEN_CODE_WANTS_SCRIPT_TO_CONTROL()
	//				NET_NL()NET_PRINT("		-Screen to take control = ")NET_PRINT(GET_XML_SCREEN_STRING_VALUE(Placement.ControlScreen))
	//				TAKE_CONTROL_OF_FRONTEND()
	//				INT PotentialPed = GET_JOINING_CHARACTER()
	//				
	//				SWITCH Placement.ControlScreen
	//					CASE MENU_UNIQUE_ID_HEADER_MP_CHARACTER_SELECT
	//						NET_NL()NET_PRINT("Take Control of MENU_UNIQUE_ID_HEADER_MP_CHARACTER_SELECT = TRUE")
	//						RUN_CHARACTER_SELECT_CNC_TABS(Placement.IdlePed[PotentialPed], Placement.ScaleformXMLTabStruct)
	//						
	//						
	//					BREAK
	//					
	//					CASE MENU_UNIQUE_ID_MP_CHAR_1
	//						Placement.ScreenPlace.iSelectedCharacter = 0
	////						SET_SCALEFORMXML_TAB_HIGHLIGHT(Placement.ScreenPlace.iSelectedCharacter, Placement.ScaleformXMLTabStruct)
	////						REFRESH_SCALEFORMXML_TABS_HIGHLIGHT( Placement.ScaleformXMLTabStruct)
	//						NET_NL()NET_PRINT("Take Control of MENU_UNIQUE_ID_MP_CHAR_1 = TRUE")
	//						RUN_CHARACTER_SELECT_CNC_TABS(Placement.IdlePed[PotentialPed], Placement.ScaleformXMLTabStruct)
	//						
	//						
	//					BREAK
	//					
	//					CASE MENU_UNIQUE_ID_MP_CHAR_2
	//						Placement.ScreenPlace.iSelectedCharacter = 1
	////						SET_SCALEFORMXML_TAB_HIGHLIGHT(Placement.ScreenPlace.iSelectedCharacter, Placement.ScaleformXMLTabStruct)
	////						REFRESH_SCALEFORMXML_TABS_HIGHLIGHT( Placement.ScaleformXMLTabStruct)
	//						NET_NL()NET_PRINT("Take Control of MENU_UNIQUE_ID_MP_CHAR_2 = TRUE")
	//						RUN_CHARACTER_SELECT_CNC_TABS(Placement.IdlePed[PotentialPed], Placement.ScaleformXMLTabStruct)
	//						
	//						
	//					BREAK
	//					
	//					CASE MENU_UNIQUE_ID_MP_CHAR_3
	//						Placement.ScreenPlace.iSelectedCharacter = 2
	////						SET_SCALEFORMXML_TAB_HIGHLIGHT(Placement.ScreenPlace.iSelectedCharacter, Placement.ScaleformXMLTabStruct)
	////						REFRESH_SCALEFORMXML_TABS_HIGHLIGHT( Placement.ScaleformXMLTabStruct)
	//						NET_NL()NET_PRINT("Take Control of MENU_UNIQUE_ID_MP_CHAR_3 = TRUE")
	//						RUN_CHARACTER_SELECT_CNC_TABS(Placement.IdlePed[PotentialPed], Placement.ScaleformXMLTabStruct)
	//						
	//						
	//					BREAK
	//					
	//					CASE MENU_UNIQUE_ID_MP_CHAR_4
	//						Placement.ScreenPlace.iSelectedCharacter = 3
	////						SET_SCALEFORMXML_TAB_HIGHLIGHT(Placement.ScreenPlace.iSelectedCharacter, Placement.ScaleformXMLTabStruct)
	////						REFRESH_SCALEFORMXML_TABS_HIGHLIGHT( Placement.ScaleformXMLTabStruct)
	//						NET_NL()NET_PRINT("Take Control of MENU_UNIQUE_ID_MP_CHAR_4 = TRUE")
	//						RUN_CHARACTER_SELECT_CNC_TABS(Placement.IdlePed[PotentialPed], Placement.ScaleformXMLTabStruct)
	//						
	//						
	//					BREAK
	//					
	//					CASE MENU_UNIQUE_ID_MP_CHAR_5
	//						Placement.ScreenPlace.iSelectedCharacter = 4
	////						SET_SCALEFORMXML_TAB_HIGHLIGHT(Placement.ScreenPlace.iSelectedCharacter, Placement.ScaleformXMLTabStruct)
	////						REFRESH_SCALEFORMXML_TABS_HIGHLIGHT( Placement.ScaleformXMLTabStruct)
	//						NET_NL()NET_PRINT("Take Control of MENU_UNIQUE_ID_MP_CHAR_5 = TRUE")
	//						RUN_CHARACTER_SELECT_CNC_TABS(Placement.IdlePed[PotentialPed], Placement.ScaleformXMLTabStruct)
	//						
	//						
	//					BREAK
	//					
	//					CASE MENU_UNIQUE_ID_AVAILABLE
	//						
	//						NET_NL()NET_PRINT("Take Control of MENU_UNIQUE_ID_AVAILABLE = TRUE")
	//						RUN_CHARACTER_SELECT_CNC_TABS(Placement.IdlePed[PotentialPed], Placement.ScaleformXMLTabStruct)
	//						
	//						
	//					BREAK
	//				
	//				
	//				ENDSWITCH
	//				
	//				
	//				
	//				NET_NL()NET_PRINT("RELEASE_CONTROL_OF_FRONTEND - called ")
	//				RELEASE_CONTROL_OF_FRONTEND()
				
				ENDIF
			ENDIF
			
		ELSE
			NET_NL()NET_PRINT("DON@T DISPLAY SUMMERY TAB because ")
			
			IF IS_PAUSE_MENU_ACTIVE() = FALSE
				NET_PRINT("IS_PAUSE_MENU_ACTIVE() = FALSE")
			ENDIF
			
			IF Placement.bHudScreenInitialised = FALSE
				NET_PRINT("Placement.bHudScreenInitialised = FALSE")
			ENDIF
		ENDIF
		
//		PROCESS_XML_PAGES(Placement)

//	PAUSE_MENU_ACTIVATE_CONTEXT( HASH("ENTER_AS_SCTV"))
		
	g_iPrivate_CharSummery_SelectedCharacter = 	Placement.ScreenPlace.iSelectedCharacter
		
		// logic	
	LOGIC_XML_MPHUD_SELECTED_JOINING(Placement)
//		
//	IF HAS_ALL_COMMON_HUD_ELEMENTS_LOADED(Placement)
//		RENDER_MPHUD_SELECTED_JOINING(Placement)
//	ENDIF
		


ENDPROC







