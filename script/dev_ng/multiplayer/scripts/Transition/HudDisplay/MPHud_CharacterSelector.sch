/* ------------------------------------------------------------------
* Name: 		MPHud_CharacterSelector.sch
* Author: 		James Adwick
* Date: 		02/09/2014
* Purpose: 		Maintains the selection of a character
* ------------------------------------------------------------------*/

//--- Includes

USING "globals.sch"
USING "transition_common.sch"
USING "MPHud_CharacterGeneration.sch"
USING "rc_area_public.sch"

//--- Constants

TWEAK_FLOAT cfCHARACTER_CREATOR_BOARD_X		0.405
TWEAK_FLOAT cfCHARACTER_CREATOR_BOARD_Y		0.370
TWEAK_FLOAT cfCHARACTER_CREATOR_BOARD_SCALE_X	0.810
TWEAK_FLOAT cfCHARACTER_CREATOR_BOARD_SCALE_Y	0.740

TWEAK_INT ciCHARACTER_SELECTOR_WALK_OUT_TIME	10000
//--- Functions

/// PURPOSE:
///    Gets the handle to the ped in the selector slot.
/// PARAMS:
///    Placement - 		HUD placement data
///    characterData - 	Character data
///    iSlot - 			Ped slot
/// RETURNS:
///    Handle of the ped.
///    
FUNC PED_INDEX GET_CHARACTER_SELECTOR_PED_FOR_SLOT(MPHUD_PLACEMENT_TOOLS& Placement, MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, INT iSlot )

	PED_INDEX mPed
	
	IF GET_CHARACTER_SELECTOR_SLOT_FOR_CREATED_PED(characterData) = iSlot
		mPed = characterData.characterCreatorPeds[characterData.eActiveChild]
	ELSE
		mPed = Placement.IdlePed[iSlot]
	ENDIF
	
	RETURN mPed


ENDFUNC

/// PURPOSE:
///    Works out if the mouse is highlighting a ped on screen.
/// PARAMS:
///    mPed - The ped to check
/// RETURNS:
///    TRUE if the mouse is highlighting the ped.
///    
FUNC BOOL IS_CHARACTER_BEING_MOUSE_HIGHLIGHTED( PED_INDEX mPed)

	//VECTOR vPos,
	VECTOR vPos1, vPos2
	//FLOAT fX, fY
	FLOAT fX1, fY1
	FLOAT fX2, fY2
	
	FLOAT fXMin, fXMax
	FLOAT fYMin, fYMax
	
	FLOAT fHighlightHeight
	FLOAT fHighlightWidth
	
	FLOAT fMouseX = GET_DISABLED_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_CURSOR_X)
	FLOAT fMouseY = GET_DISABLED_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_CURSOR_Y)
	
	IF NOT DOES_ENTITY_EXIST(mPed)
		RETURN FALSE
	ENDIF
	
	IF IS_ENTITY_DEAD(mPed)
		RETURN FALSE
	ENDIF
		
	//SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
	//SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
	
	vPos1 = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mPed, <<0.0,0.0,1.0>>)
	vPos2 = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mPed, <<0.0,0.0,-1.0>>)
	
	//DRAW_DEBUG_SPHERE(vPos1, 0.2)
	//DRAW_DEBUG_SPHERE(vPos2, 0.2)
	
	IF NOT GET_SCREEN_COORD_FROM_WORLD_COORD(vPos1, fX1, fY1)
		RETURN FALSE
	ENDIF
	
	IF NOT GET_SCREEN_COORD_FROM_WORLD_COORD(vPos2, fX2, fY2)
		RETURN FALSE
	ENDIF
	
	fHighlightHeight = VDIST(<<fX1, fY1, 0.0>>, <<fX2, fY2, 0.0>>)
	fHighlightWidth = fHighlightHeight / 4.5

	fXMin = fX1 - (fHighlightWidth / 2)
	fXMax = fX1 + (fHighlightWidth / 2)
	fYMin = fY1
	fYMax = fY2
			
	IF 	fMouseX >= fXMin
	AND fMouseX <= fXMax 
	AND fMouseY >= fYMin
	AND fMouseY <= fYMax
		//SET_MOUSE_CURSOR_STYLE(MOUSE_CURSOR_STYLE_HAND_MIDDLE_FINGER)
		//DRAW_RECT_FROM_CORNER(fXMin, fYMin, fHighlightWidth, fHighlightHeight, 255,255,255,32)
		RETURN TRUE
	ENDIF
	
	//SET_MOUSE_CURSOR_STYLE(MOUSE_CURSOR_STYLE_ARROW)

	RETURN FALSE

ENDFUNC

/// PURPOSE:
///    Handles selection of characters using the mouse
/// PARAMS:
///    Placement - 		Placement data
///    characterData - 	Character data
/// RETURNS:
///    The index of the ped chosen, or -1 if no ped chosen.
///    
FUNC INT MOUSE_CHARACTER_SELECT( MPHUD_PLACEMENT_TOOLS &Placement, MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData )

	PED_INDEX mPed
	
	INT iSelect = -1
	
	// Hard coded to two chars for now.
	
	// Select char 0
	mPed = GET_CHARACTER_SELECTOR_PED_FOR_SLOT( Placement, characterData, 0 )
	IF IS_ENTITY_ALIVE(mPed)
		IF IS_CHARACTER_BEING_MOUSE_HIGHLIGHTED(mPed)
			iSelect = 0
		ENDIF
	ENDIF
	
	// Select char 1
	IF characterData.iActiveSlotOnSelector != 0
		IF IS_ENTITY_ALIVE(mPed)
			mPed = GET_CHARACTER_SELECTOR_PED_FOR_SLOT( Placement, characterData, 1 )
			IF IS_CHARACTER_BEING_MOUSE_HIGHLIGHTED( mPed)
				iSelect = 1
			ENDIF
		ENDIF
	ENDIF
	
	SET_MOUSE_CURSOR_STYLE(MOUSE_CURSOR_STYLE_ARROW)

	IF NOT IS_MOUSE_ROLLED_OVER_INSTRUCTIONAL_BUTTONS()
	
		// Change mouse cursor
		IF iSelect = Placement.ScreenPlace.iSelectedCharacter
			SET_MOUSE_CURSOR_STYLE(MOUSE_CURSOR_STYLE_HAND_MIDDLE_FINGER)
		ELIF iSelect = -1
			SET_MOUSE_CURSOR_STYLE(MOUSE_CURSOR_STYLE_ARROW_DIMMED)
		ENDIF
		
		IF IS_CONTROL_JUST_PRESSED( FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
			RETURN iSelect
		ENDIF
		
	ENDIF
				
	RETURN -1

ENDFUNC

/// PURPOSE:
///    Draw the boards every frame
PROC DRAW_CHARACTER_CONTROLLER_BOARD(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData)
	
	// 2280448 - Do not 
	IF NOT IS_SKYSWOOP_AT_GROUND()
	OR GET_CHARACTER_CREATOR_CONFIRM_STATE() = CHARACTER_CREATOR_CONFIRM_STATE_PUSH_TO_GAME
		CPRINTLN(DEBUG_NET_CHARACTER, "DRAW_CHARACTER_CONTROLLER_BOARD - Skycam active. Do not render boards")
		EXIT
	ENDIF
	
	IF GET_CHARACTER_SELECTOR_STATE() >= CHARACTER_SELECTOR_STATE_WALK_IN_ANIM
	OR GET_CHARACTER_CONTROLLER_SCREEN_STATE() = CHARACTER_CONTROLLER_SCREEN_CREATOR
		
		INT i
		REPEAT ciCHARACTER_SELECTOR_TOTAL_BOARDS i
			
			SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
			SET_TEXT_RENDER_ID(characterData.sPedBoard[i].iRenderTarget)
			DRAW_SCALEFORM_MOVIE(characterData.sPedBoard[i].scaleformIndex, cfCHARACTER_CREATOR_BOARD_X, cfCHARACTER_CREATOR_BOARD_Y, cfCHARACTER_CREATOR_BOARD_SCALE_X, cfCHARACTER_CREATOR_BOARD_SCALE_Y, 255, 255, 255, 255)
			SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(FALSE)
			
//			#IF IS_DEBUG_BUILD
//				IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RB)
//				AND IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LB)
//					CPRINTLN(DEBUG_NET_CHARACTER, "DRAW_CHARACTER_CONTROLLER_BOARD - Drawing the board: ", i)
//					CPRINTLN(DEBUG_NET_CHARACTER, "DRAW_CHARACTER_CONTROLLER_BOARD - Render target: ", characterData.sPedBoard[i].iRenderTarget)
//					CPRINTLN(DEBUG_NET_CHARACTER, "DRAW_CHARACTER_CONTROLLER_BOARD - Scaleform index: ", NATIVE_TO_INT(characterData.sPedBoard[i].scaleformIndex))
//				ENDIF
//			#ENDIF
		ENDREPEAT
	ENDIF
ENDPROC



/// PURPOSE:
///    Take the player out of the character selector
PROC PROCESS_CHARACTER_SELECTOR_QUIT(MPHUD_PLACEMENT_TOOLS& Placement)
	
	CANCEL_MP_JOIN(TRUE)
	RESET_PAUSE_MENU_WARP_REQUESTS()
	UNLOAD_PEDHEADSHOT(Placement.ScaleformAlertScreenStruct.HeadshotPic)
	
	RESET_ALL_EXTERNAL_TERMINATE_REASONS()
	
	//DELETE_ALL_SELECTION_MENUPED(Placement.SelectionPed)
	//DELETE_ALL_SELECTION_MENUPED(Placement.IdlePed)
	//DELETE_ALL_INACTIVE_MENUPED(Placement.SelectionPed, Placement.IdlePed)
		
	IF LOBBY_AUTO_MULTIPLAYER_FREEMODE()
	OR DID_BAIL_WHEN_ENTERING_ONLINE_FROM_BOOT()
		SET_FRONTEND_ACTIVE(FALSE)
		DO_SCREEN_FADE_OUT(0)
	ENDIF
	
	//RESET_BIRDS_ALPHA()
	RUN_TRANSITION_MENU_AUDIO(FALSE)
	UGC_CLEAR_QUERY_RESULTS_BY_OFFLINE_STATUS()
	IF GET_CONFIRM_INVITE_INTO_GAME_STATE()
		RUN_SCALEFORM_LOCK_RIGHT_MOUSE_BUTTON(FALSE)
		HUD_CHANGE_STATE(HUD_STATE_IGNORE_INVITE)
		TRANSITION_CHANGE_STATE(TRANSITION_STATE_WAIT_HUD_EXIT)
	ELSE
		RUN_SCALEFORM_LOCK_RIGHT_MOUSE_BUTTON(FALSE)
		REQUEST_TRANSITION_TERMINATE_MP_SESSION()
	ENDIF
	
	CLEAR_HELP()
ENDPROC

/// PURPOSE:
///    Process the player entering the game
PROC PROCESS_CHARACTER_SELECTOR_ENTER_GAME(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, MPHUD_PLACEMENT_TOOLS &Placement)
	IF Placement.ScreenPlace.iSelectedCharacter = -1
		Placement.ScreenPlace.iSelectedCharacter = Placement.ScaleformXMLTabStruct.iHighlightTab
		NET_NL()NET_PRINT("LOGIC_MPHUD_XML_CHARACTER_SELECT_FM: iSelectedCharacter = -1 so making it iHighlightTab = ")NET_PRINT_INT(Placement.ScreenPlace.iSelectedCharacter)
		IF Placement.ScreenPlace.iSelectedCharacter = -1	
			Placement.ScreenPlace.iSelectedCharacter = 0							
			NET_NL()NET_PRINT("LOGIC_MPHUD_XML_CHARACTER_SELECT_FM: iSelectedCharacter = -1 STILL so making it 0")
		ENDIF
	ENDIF
	
	// Release the audio banks B*5484361
	IF characterData.bAudioBanksRequested
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("Mugshot_Character_Creator")
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_GTAO/MUGSHOT_ROOM")
		characterData.bAudioBanksRequested = FALSE
	ENDIF
	
	CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_SELECTOR_ENTER_GAME - Process the player entering GTA Online")

	RUN_TRANSITION_MENU_AUDIO(FALSE)
	NETWORK_APPLY_PED_SCAR_DATA(PLAYER_PED_ID(), Placement.ScreenPlace.iSelectedCharacter)

	IF GET_ACTIVE_CHARACTER_SLOT() != Placement.ScreenPlace.iSelectedCharacter
		NET_NL()NET_PRINT("LOGIC_MPHUD_XML_CHARACTER_SELECT_FM: Player has swapped character ")
		STAT_CLEAR_PENDING_SAVES(-1)
		REMOVE_ALL_QUEUED_SAVES_DUE_TO_CHARACTER_SWAP()
		Placement.HasGamerSetupChanged = TRUE
		g_Private_LoadingNetworkShopINVENTORY.LoadingStages = 0
		SET_ACTIVE_CHARACTER_SLOT(Placement.ScreenPlace.iSelectedCharacter) 
		SET_CURRENT_MP_SLOT_FOR_CLOTHES(Placement.ScreenPlace.iSelectedCharacter)
		RESET_EVERYTHING_TRANSITION_SESSION_FOR_SP_BAIL()
		RESET_CASINO_HEIST_FLOW_DATA()	
		RESET_ISLAND_HEIST_FLOW()
		g_TransitionSessionNonResetVars.contactRequests.bResetContactReqTimeouts = TRUE
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_MP_CONTACT_REQUESTS,	"[dsw] Swapping characters, will reset contact request timeouts")
		#ENDIF
//						REQUEST_SAVE(STAT_SAVETYPE_END_GAMER_SETUP)
	ENDIF
	
	ClearInsideDisplacedInteriorFlag()
	RUN_TRANSITION_SFX(FALSE)
	RUN_TRANSITION_SFX_MENU_SCENE(TRUE)
	SET_ENTERED_CHAR_CREATION_FROM_ALTERATION_PROMPT(FALSE)
	SET_CURRENT_MP_SLOT_FOR_CLOTHES(Placement.ScreenPlace.iSelectedCharacter)
//					ANIMPOSTFX_STOP_CROSSFADE()
	SET_QUICKJOIN_ACTIVE(TRUE)
	IF g_B_DisplayOnlineSplash = FALSE
		NET_NL()NET_PRINT("LOGIC_MPHUD_XML_CHARACTER_SELECT_FM: g_B_DisplayOnlineSplash = TRUE - Swap team ")
		g_B_DisplayOnlineSplash = TRUE
	ENDIF
	g_b_Private_Tshirt_is_new_character[Placement.ScreenPlace.iSelectedCharacter] = FALSE
	SET_JOINING_CHARACTER(Placement.ScreenPlace.iSelectedCharacter)
	IF IS_STAT_CHARACTER_ACTIVE(Placement.ScreenPlace.iSelectedCharacter)
		RUN_GOING_FORWARD_CHARACTER_SUMMARY(Placement)
	ENDIF
	
	SET_SKYSWOOP_UP(DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
	SET_SKYFREEZE_FROZEN()
ENDPROC

/// PURPOSE:
///    Handle the selection of one of the characters, either into the game or into the creator
PROC PROCESS_CHARACTER_SELECTOR_SELECTION(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, MPHUD_PLACEMENT_TOOLS &Placement)

	IF HAS_SLOT_BEEN_IGNORED_SCRIPT(Placement.ScreenPlace.iSelectedCharacter) = FALSE 
	OR SCRIPT_IS_CLOUD_AVAILABLE() = FALSE
		
		//PLAY_SOUND_FRONTEND(characterData.iSoundID, "SELECT","HUD_FRONTEND_DEFAULT_SOUNDSET")
		
			// KGM 21/10/14: Some heist variables are semi-persistent as the player jumps around MP, but need cleared if the player goes to the character select screen
			PRINTLN(".KGM [ActSelect][Heist]: Request reset of semi-persistent Heist variables on MP rejoin - NG Character Select Screen")
			g_resetHeistsOnJoiningMP = TRUE

		NET_NL()NET_PRINT("LOGIC_MPHUD_XML_CHARACTER_SELECT_FM: INPUT_FRONTEND_ACCEPT with iSelectedCharacter = ")NET_PRINT_INT(Placement.ScreenPlace.iSelectedCharacter)
		UNLOAD_PEDHEADSHOT(Placement.ScaleformAlertScreenStruct.HeadshotPic)
		
		IF IS_STAT_CHARACTER_ACTIVE(Placement.ScreenPlace.iSelectedCharacter)
			IF GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_IS_NG_VERSION, Placement.ScreenPlace.iSelectedCharacter) < ciCHARACTER_CREATOR_CURRENT_VERSION
			// New check for the PC version if we are trying to enter with a character from console NG (need inventory stats updated)
			OR (IS_PC_VERSION() AND GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_IS_NGPC_VERSION, Placement.ScreenPlace.iSelectedCharacter) < ciCHARACTER_CREATOR_CURRENT_VERSION)			
			#IF IS_DEBUG_BUILD
			OR characterData.bDebugLastGenCharacter
			#ENDIF
			OR IS_CHANGED_CHARACTER_APPEARANCE() 
				
				CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_SELECTOR_SELECTION - Player has selected an old character. Push them to creator, iSlot = ", Placement.ScreenPlace.iSelectedCharacter)
			
				#IF IS_DEBUG_BUILD
				IF IS_PC_VERSION() 
				AND GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_IS_NGPC_VERSION, Placement.ScreenPlace.iSelectedCharacter) < ciCHARACTER_CREATOR_CURRENT_VERSION
					CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_SELECTOR_SELECTION - Player has selected an old character on PC: ", Placement.ScreenPlace.iSelectedCharacter)
				ENDIF
				#ENDIF
			
				// They are coming from alteration prompt
				SET_ENTERED_CHAR_CREATION_FROM_ALTERATION_PROMPT(TRUE)
				
				characterData.iCharacterSelectedSlot = Placement.ScreenPlace.iSelectedCharacter
															
				// Re-initialise our character to match the ped
				GENERATE_INITIAL_CHARACTER_CREATOR_DATA(characterData, Placement)
				
				// Set the flag to say we have come in here
				SET_CHARACTER_CONTROLLER_BIT(ciCHARACTER_CONTROLLER_EDIT_PED_FROM_OLD_DATA)
			
				// Move us into the creator
				SET_CHARACTER_CREATOR_STATE(CHARACTER_CREATOR_STATE_INIT)
				SET_CHARACTER_SELECTOR_OUTRO_ANIM_STATE(CHARACTER_SELECTOR_OUTRO_ANIM_START)
				SET_CHARACTER_SELECTOR_STATE(CHARACTER_SELECTOR_STATE_WALK_OUT_ANIM)
			ELSE
				// Flag ourselves entering the game
				SET_CHARACTER_CONTROLLER_BIT(ciCHARACTER_CONTROLLER_ENTERING_GAME)		
				
				SET_CHARACTER_SELECTOR_OUTRO_ANIM_STATE(CHARACTER_SELECTOR_OUTRO_ANIM_START)
				SET_CHARACTER_SELECTOR_STATE(CHARACTER_SELECTOR_STATE_WALK_OUT_ANIM)
			ENDIF
			
			CLEAR_HELP()
		ELSE
			
			IF NOT HAS_ENTERED_OFFLINE_SAVE_FM()
			AND IS_SAVING_HAVING_TROUBLE() = FALSE
				DELETE_SAVE_SLOT(Placement.ScreenPlace.iSelectedCharacter+1, TRUE, FALSE)
				NETWORK_CLEAR_CHARACTER_WALLET(Placement.ScreenPlace.iSelectedCharacter)
			ENDIF
			
			
			SET_NEWS_FEED_SPLASH_SCREEN_ACTIVE(FALSE)
		
			NET_NL()NET_PRINT("LOGIC_MPHUD_XML_CHARACTER_SELECT_FM: g_MPCharData.bIsCharNew[Placement.ScreenPlace.iSelectedCharacter] - Create Character. ")
		
			SET_LOADING_ICON_INACTIVE()
			g_MPCharData.bIsCharNew[Placement.ScreenPlace.iSelectedCharacter] = TRUE
			SET_JOINING_CHARACTER(Placement.ScreenPlace.iSelectedCharacter)
			//SET_CURRENT_MP_SLOT_FOR_CLOTHES(Placement.ScreenPlace.iSelectedCharacter)
			SET_ENTERED_CHAR_CREATION_FROM_ALTERATION_PROMPT(FALSE)
			RUN_SCALEFORM_LOCK_RIGHT_MOUSE_BUTTON(FALSE)	
			//HUD_CHANGE_STATE(HUD_STATE_CREATE_FACE)
			IF SCRIPT_IS_CLOUD_AVAILABLE()
				TRANSITION_CHANGE_STATE(TRANSITION_STATE_CREATION_ENTER_SESSION)
			ENDIF
			SET_PLAYERS_SELECTED_CHARACTER_TEAM(TEAM_FREEMODE)
			SET_STAT_CHARACTER_TEAM(TEAM_FREEMODE, Placement.ScreenPlace.iSelectedCharacter)
			
			characterData.iCharacterSelectedSlot = Placement.ScreenPlace.iSelectedCharacter
			
			// Reset these stats if we are creating a new character
			SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_FORCED_HAIR_ITEM, ENUM_TO_INT(DUMMY_PED_COMP), Placement.ScreenPlace.iSelectedCharacter)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_FORCED_HAIR_TYPE, ENUM_TO_INT(COMP_TYPE_HAIR), Placement.ScreenPlace.iSelectedCharacter)
						
			// Move us into the creator
			SET_CHARACTER_CREATOR_STATE(CHARACTER_CREATOR_STATE_RP_PROMPT)
			
			SET_CHARACTER_SELECTOR_OUTRO_ANIM_STATE(CHARACTER_SELECTOR_OUTRO_ANIM_START)
			SET_CHARACTER_SELECTOR_STATE(CHARACTER_SELECTOR_STATE_WALK_OUT_ANIM)
			
			CLEAR_HELP()
		ENDIF
	ELSE
		//PLAY_SOUND_FRONTEND(-1, "SELECT","HUD_FRONTEND_DEFAULT_SOUNDSET") 
		RESET_NET_TIMER(Placement.LoadFailedTimer)
		DESTROY_ALL_MENUPEDS(Placement.IdlePed, Placement.SelectionPed)
		FORCE_RUN_RETRY_SLOT(Placement.ScreenPlace.iSelectedCharacter+1)
		SET_MP_INT_PLAYER_STAT(MPPLY_LAST_MP_CHAR, Placement.ScreenPlace.iSelectedCharacter)
		DO_SCREEN_FADE_OUT(0)
		RESET_CHARACTER_CONTROLLER_DATA(characterData)
		RESET_PAUSE_MENU_WARP_REQUESTS()
	ENDIF

ENDPROC

/// PURPOSE:
///    Probably a temp until we get lineup in place 
PROC SET_CHARACTER_SELECTOR_PED_VISIBILITY(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, MPHUD_PLACEMENT_TOOLS& Placement)

	CPRINTLN(DEBUG_NET_CHARACTER, "SET_CHARACTER_SELECTOR_PED_VISIBILITY - Called to hide all peds and make selection visible")

	// Make everyone invisible
	SET_ENTITY_VISIBLE(characterData.characterCreatorPeds[CHARACTER_CREATOR_SON], FALSE)
	SET_ENTITY_VISIBLE(characterData.characterCreatorPeds[CHARACTER_CREATOR_DAUGHTER], FALSE)

	INT i
	REPEAT MAX_NUM_CHARACTER_SLOTS i
		
		IF DOES_ENTITY_EXIST(Placement.IdlePed[i])
			SET_ENTITY_VISIBLE(Placement.IdlePed[i], TRUE)
		ENDIF
		
	ENDREPEAT
	
	IF IS_CHARACTER_CONTROLLER_PED_OK(characterData.characterCreatorPeds[characterData.eActiveChild])
		SET_ENTITY_VISIBLE(characterData.characterCreatorPeds[characterData.eActiveChild], TRUE)
	ENDIF
ENDPROC

/// PURPOSE:
///    Populate the stat card for the currently selected character
PROC DISPLAY_CHARACTER_SELECTOR_STAT_CARD(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, MPHUD_PLACEMENT_TOOLS& Placement)
	
	INT iStamina = GET_CHARACTER_CREATOR_STAT_VALUE(characterData, CHARACTER_CREATOR_STAT_STAMINA)
	INT iShooting = GET_CHARACTER_CREATOR_STAT_VALUE(characterData, CHARACTER_CREATOR_STAT_SHOOTING)
	INT iStrength = GET_CHARACTER_CREATOR_STAT_VALUE(characterData, CHARACTER_CREATOR_STAT_STRENGTH)
	INT iStealth = GET_CHARACTER_CREATOR_STAT_VALUE(characterData, CHARACTER_CREATOR_STAT_STEALTH)
	INT iFlying = GET_CHARACTER_CREATOR_STAT_VALUE(characterData, CHARACTER_CREATOR_STAT_FLYING)
	INT iDriving = GET_CHARACTER_CREATOR_STAT_VALUE(characterData, CHARACTER_CREATOR_STAT_DRIVING)
	
	// Update our stats
	INT iSlot = Placement.ScreenPlace.iSelectedCharacter
	IF Placement.ScreenPlace.iSelectedCharacter = GET_CHARACTER_SELECTOR_SLOT_FOR_CREATED_PED(characterData)
		iSlot = -1
	ENDIF
	
	CPRINTLN(DEBUG_NET_CHARACTER, "DISPLAY_CHARACTER_SELECTOR_STAT_CARD - Displaying stat card for slot: ", iSlot)
	
	DISPLAY_CHARACTER_STAT_CARD(ciCHARACTER_SELECTOR_COLUMN_STAT_CARD, characterData.iCharacterMenuID, 0, characterData.tlCrewEmblem, characterData.tlCrewTag,
									iSlot, iStamina, iShooting, iStrength, iStealth, iFlying, iDriving)
	
	DISPLAY_CHARACTER_CREATOR_COLUMN(ciCHARACTER_SELECTOR_COLUMN_STAT_CARD)
	
	// Make sure we hide the new character card
	IF Placement.ScreenPlace.iSelectedCharacter = GET_CHARACTER_SELECTOR_SLOT_FOR_CREATED_PED(characterData)
		CPRINTLN(DEBUG_NET_CHARACTER, "DISPLAY_CHARACTER_SELECTOR_STAT_CARD - PRINT_HELP_FOREVER(FACE_NEW_C)")
		SET_FRONTEND_DISPLAY_COLUMN(ciCHARACTER_SELECTOR_COLUMN_STAT_CARD, FALSE)
		PRINT_HELP_FOREVER("FACE_NEW_C")
	ELSE
		CLEAR_HELP(TRUE)
	ENDIF
ENDPROC

/// PURPOSE:
///    Requests and loads the character creator boards
PROC REQUEST_CHARACTER_CONTROLLER_BOARDS(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, INT iSlot)
	
	IF iSlot < 0
	OR iSlot >= ciCHARACTER_SELECTOR_TOTAL_BOARDS
		SCRIPT_ASSERT("REQUEST_CHARACTER_CONTROLLER_BOARDS - We are trying to set up a board which is out of range for the slot we have passed")
		EXIT
	ENDIF
	
	characterData.sPedBoard[iSlot].scaleformIndex = REQUEST_SCALEFORM_MOVIE(GET_CHARACTER_CONTROLLER_BOARD_MOVIE(iSlot))
ENDPROC

/// PURPOSE:
///    Requests and loads the character creator boards
FUNC BOOL HAS_CHARACTER_CONTROLLER_BOARDS(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, INT iSlot)
	
	IF iSlot < 0
	OR iSlot >= ciCHARACTER_SELECTOR_TOTAL_BOARDS
		SCRIPT_ASSERT("HAS_CHARACTER_CONTROLLER_BOARDS - We are trying to set up a board which is out of range for the slot we have passed")
		RETURN TRUE
	ENDIF
	
	characterData.sPedBoard[iSlot].scaleformIndex = REQUEST_SCALEFORM_MOVIE(GET_CHARACTER_CONTROLLER_BOARD_MOVIE(iSlot))
	RETURN HAS_SCALEFORM_MOVIE_LOADED(characterData.sPedBoard[iSlot].scaleformIndex)
ENDFUNC

/// PURPOSE:
///    	Generates the boards we need for our screen
PROC CREATE_CHARACTER_SELECTOR_BOARDS(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData)

	// PROP_POLICE_ID_BOARD
	// ID_text_02 


	INT i
	STRING strRTText
	
	REPEAT ciCHARACTER_SELECTOR_TOTAL_BOARDS i
		IF NOT DOES_ENTITY_EXIST(characterData.sPedBoard[i].objBoard)
			characterData.sPedBoard[i].objBoard = CREATE_OBJECT(PROP_POLICE_ID_BOARD, GET_CHARACTER_SELECTOR_ANIM_VECTOR() + <<0, 0, i*1>>, FALSE, TRUE)
			characterData.sPedBoard[i].objRenderBoard = CREATE_OBJECT(GET_CHARACTER_SELECTOR_BOARD_RENDER_TARGET(i), GET_CHARACTER_SELECTOR_ANIM_VECTOR() + <<i*1, 0, 0>>, FALSE, TRUE)
			
			strRTText = GET_CHARACTER_SELECTOR_BOARD_STRING(i)
			
			IF NOT IS_NAMED_RENDERTARGET_REGISTERED(strRTText)
				REGISTER_NAMED_RENDERTARGET(strRTText)
				LINK_NAMED_RENDERTARGET(GET_CHARACTER_SELECTOR_BOARD_RENDER_TARGET(i))
				characterData.sPedBoard[i].iRenderTarget = GET_NAMED_RENDERTARGET_RENDER_ID(strRTText)
				
				CPRINTLN(DEBUG_NET_CHARACTER, "CREATE_CHARACTER_SELECTOR_BOARDS - Creating rendertarget for board: ", i, ", target = ", characterData.sPedBoard[i].iRenderTarget, " strRTText = ", strRTText, " SCRIPT ID(", GET_THIS_SCRIPT_NAME(), " - ", NATIVE_TO_INT(GET_ID_OF_THIS_THREAD()), ")")
			ENDIF			
			
			HIDE_CHARACTER_CREATOR_SELECTOR_BOARD(characterData, i)			
		ENDIF
	ENDREPEAT

ENDPROC

/// PURPOSE:
///    Switch which ped has the raised board to show who is selected
PROC PROCESS_CHARACTER_CREATOR_ANIMATE_BOARD_SELECTION(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, MPHUD_PLACEMENT_TOOLS& Placement, INT iNewSelection)

	PED_INDEX aPedToAnimate
	
	// Grab the ped we are wanting to animate
	IF iNewSelection = GET_CHARACTER_SELECTOR_SLOT_FOR_CREATED_PED(characterData)
		aPedToAnimate = characterData.characterCreatorPeds[characterData.eActiveChild]
	ELSE
		aPedToAnimate = Placement.IdlePed[iNewSelection]
	ENDIF
	
	STRING strAnimDict = GET_CHARACTER_CONTROLLER_ANIM_DICT(ciCHARACTER_CREATOR_ANIM_DICT_SELECTOR, iNewSelection, (GET_ENTITY_MODEL(aPedToAnimate) = MP_M_FREEMODE_01))
	STRING strAnim = GET_CHARACTER_SELECTOR_ANIM(ciCHARACTER_CREATOR_ANIM_SELECTOR_WALK_IN)
	
	IF IS_CHARACTER_CONTROLLER_PED_OK(aPedToAnimate)
		IF NOT IS_ENTITY_PLAYING_ANIM(aPedToAnimate, strAnimDict, strAnim)
						
			SEQUENCE_INDEX seq
			FLOAT fBlendIn = NORMAL_BLEND_IN
			FLOAT fBlendOut = NORMAL_BLEND_OUT
	
			OPEN_SEQUENCE_TASK(seq)
				IF IS_ENTITY_PLAYING_ANIM(aPedToAnimate, strAnimDict, "loop")
				AND NOT IS_ENTITY_PLAYING_ANIM(aPedToAnimate, strAnimDict, "high_to_low")
					TASK_PLAY_ANIM(NULL, strAnimDict, "low_to_high", fBlendIn, fBlendOut, DEFAULT, AF_TURN_OFF_COLLISION)
				ELSE
					fBlendIn = SLOW_BLEND_IN
					fBlendOut = SLOW_BLEND_OUT
				ENDIF
				
				TASK_PLAY_ANIM(NULL, strAnimDict, "loop_raised", fBlendIn, fBlendOut, DEFAULT, AF_LOOPING | AF_TURN_OFF_COLLISION)
			CLOSE_SEQUENCE_TASK(seq)
		
			TASK_PERFORM_SEQUENCE(aPedToAnimate, seq)
			CLEAR_SEQUENCE_TASK(seq)
						
			CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_CREATOR_ANIMATE_BOARD_SELECTION - Play raised anim on ped: ", NATIVE_TO_INT(aPedToAnimate))
			
			IF characterData.iActiveSlotOnSelector != 0
			
				// If required grab the other ped
				iNewSelection = PICK_INT(iNewSelection = 0, 1, 0)
				IF iNewSelection = GET_CHARACTER_SELECTOR_SLOT_FOR_CREATED_PED(characterData)
					aPedToAnimate = characterData.characterCreatorPeds[characterData.eActiveChild]
				ELSE
					aPedToAnimate = Placement.IdlePed[iNewSelection]
				ENDIF
				
				strAnimDict = GET_CHARACTER_CONTROLLER_ANIM_DICT(ciCHARACTER_CREATOR_ANIM_DICT_SELECTOR, iNewSelection, (GET_ENTITY_MODEL(aPedToAnimate) = MP_M_FREEMODE_01))
				strAnim = GET_CHARACTER_SELECTOR_ANIM(ciCHARACTER_CREATOR_ANIM_SELECTOR_LOOP)
				
				IF IS_CHARACTER_CONTROLLER_PED_OK(aPedToAnimate)
					
					fBlendIn = NORMAL_BLEND_IN
					fBlendOut = NORMAL_BLEND_OUT
					
					OPEN_SEQUENCE_TASK(seq)
						IF IS_ENTITY_PLAYING_ANIM(aPedToAnimate, strAnimDict, "loop_raised")
						AND NOT IS_ENTITY_PLAYING_ANIM(aPedToAnimate, strAnimDict, "low_to_high")
							TASK_PLAY_ANIM(NULL, strAnimDict, "high_to_low", fBlendIn, fBlendOut, DEFAULT, AF_TURN_OFF_COLLISION)
						ELSE
							fBlendIn = SLOW_BLEND_IN
							fBlendOut = SLOW_BLEND_OUT
						ENDIF
						
						TASK_PLAY_ANIM(NULL, strAnimDict, "loop", fBlendIn, fBlendOut, DEFAULT, AF_LOOPING | AF_TURN_OFF_COLLISION)
					CLOSE_SEQUENCE_TASK(seq)
				
					TASK_PERFORM_SEQUENCE(aPedToAnimate, seq)
					CLEAR_SEQUENCE_TASK(seq)
					
					CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_CREATOR_ANIMATE_BOARD_SELECTION - Do standard loop on ped: ", NATIVE_TO_INT(aPedToAnimate))
				ENDIF
			ENDIF	
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE:
///    Returns the eyebrow opacity value of the active ped in the selector
FUNC FLOAT GET_CHARACTER_SELECTOR_PED_EYEBROW_OPACITY(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData)
	
	// Get eyebrow opacity in the selector for next gen characters only (B* 2197771)
	CHARACTER_CREATOR_APPEARANCE_VALUES appearanceValues
	GET_CHARACTER_CREATOR_APPEARANCE_VALUES(characterData, appearanceValues, ciCHARACTER_CREATOR_APPEARANCE_EYEBROWS_M)

	IF IS_PED_FEMALE(characterData.characterCreatorPeds[characterData.eActiveChild])
		GET_CHARACTER_CREATOR_APPEARANCE_VALUES(characterData, appearanceValues, ciCHARACTER_CREATOR_APPEARANCE_EYEBROWS_F)
	ENDIF
	
	RETURN appearanceValues.fOpacity
ENDFUNC

/// PURPOSE:
///    Initialise any data we need when moving to this screen
PROC INITIALISE_CHARACTER_SELECTOR_SELECTION(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, MPHUD_PLACEMENT_TOOLS& Placement)
	
	SET_CHARACTER_SELECTOR_PED_VISIBILITY(characterData, Placement)
	
	DISPLAY_CHARACTER_SELECTOR_STAT_CARD(characterData, Placement)
		
	characterData.iSelectorSlot = Placement.ScreenPlace.iSelectedCharacter
	characterData.fEyebrowOpacity = GET_CHARACTER_SELECTOR_PED_EYEBROW_OPACITY(characterData)
	
	SET_CHARACTER_CONTROLLER_BIT(ciCHARACTER_CONTROLLER_COMING_FROM_SELECTOR)
	
	// Update our buttons
	UPDATE_CHARACTER_CONTROLLER_BUTTONS(characterData)		
ENDPROC

/// PURPOSE:
///    Handle the player returning to the selector from the creator.
FUNC BOOL UPDATE_CHARACTER_SELECTOR_RETURN_FROM_CREATOR(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, MPHUD_PLACEMENT_TOOLS& Placement)
	
	IF g_sCharacterControllerData.eCharacterControllerPreviousScreenState = CHARACTER_CONTROLLER_SCREEN_CREATOR
		
		CPRINTLN(DEBUG_NET_CHARACTER, "UPDATE_CHARACTER_SELECTOR_RETURN_FROM_CREATOR - Player has returned from creator")
		
		characterData.eCharacterControllerCameraMode = CHARACTER_CONTROLLER_CAMERA_SELECTOR_START
		SET_CHARACTER_CONTROLLER_CAMERA_ACTIVE(characterData, CHARACTER_CONTROLLER_CAMERA_SELECTOR_START)
		
		//ANIMPOSTFX_PLAY("MenuMGSelectionTintOut", 0, FALSE)
		
		PED_INDEX aPedToAnimate
			
		IF GET_CHARACTER_SELECTOR_SLOT_FOR_CREATED_PED(characterData) = Placement.ScreenPlace.iSelectedCharacter
			aPedToAnimate = characterData.characterCreatorPeds[characterData.eActiveChild]
			CPRINTLN(DEBUG_NET_CHARACTER, "UPDATE_CHARACTER_SELECTOR_RETURN_FROM_CREATOR - Ped A to animate is creator ped: ", NATIVE_TO_INT(aPedToAnimate))
		ELSE
			aPedToAnimate = Placement.IdlePed[Placement.ScreenPlace.iSelectedCharacter]
			CPRINTLN(DEBUG_NET_CHARACTER, "UPDATE_CHARACTER_SELECTOR_RETURN_FROM_CREATOR - Ped A to animate is existing ped: ", NATIVE_TO_INT(aPedToAnimate))
		ENDIF
		
		IF IS_CHARACTER_CONTROLLER_PED_OK(aPedToAnimate)
			CLEAR_PED_TASKS_IMMEDIATELY(aPedToAnimate)
			SET_ENTITY_VISIBLE(aPedToAnimate, FALSE)
			SET_ENTITY_COORDS(aPedToAnimate, GET_CHARACTER_CREATOR_PED_POSITION())
		ENDIF
//		
//		IF characterData.iActiveSlotOnSelector != 0
//		
//			iPlacementIndex = 1
//			IF iPlacementIndex = GET_CHARACTER_SELECTOR_SLOT_FOR_CREATED_PED(characterData)
//				aPedToAnimate = characterData.characterCreatorPeds[characterData.eActiveChild]
//				CPRINTLN(DEBUG_NET_CHARACTER, "UPDATE_CHARACTER_SELECTOR_RETURN_FROM_CREATOR - Ped B to animate is creator ped: ", NATIVE_TO_INT(aPedToAnimate))
//			ELSE
//				aPedToAnimate = Placement.IdlePed[iPlacementIndex]
//				CPRINTLN(DEBUG_NET_CHARACTER, "UPDATE_CHARACTER_SELECTOR_RETURN_FROM_CREATOR - Ped B to animate is existing ped: ", NATIVE_TO_INT(aPedToAnimate))
//			ENDIF
//			
//			IF IS_CHARACTER_CONTROLLER_PED_OK(aPedToAnimate)
//				CLEAR_PED_TASKS_IMMEDIATELY(aPedToAnimate)
//				SET_ENTITY_VISIBLE(aPedToAnimate, FALSE)
//				SET_ENTITY_COORDS(aPedToAnimate, GET_CHARACTER_CREATOR_PED_POSITION())
//			ENDIF
//		ENDIF
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Animate a single ped back into the selector
PROC PROCESS_CHARACTER_SELECTOR_START_ANIM_FOR_SINGLE_PED(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, MPHUD_PLACEMENT_TOOLS& Placement)
	
	PED_INDEX aPedToAnimate
	IF GET_CHARACTER_SELECTOR_SLOT_FOR_CREATED_PED(characterData) = Placement.Screenplace.iSelectedCharacter
		aPedToAnimate = characterData.characterCreatorPeds[characterData.eActiveChild]
		CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_SELECTOR_START_ANIM_FOR_SINGLE_PED - Ped A to animate is creator ped: ", NATIVE_TO_INT(aPedToAnimate))
	ELSE
		aPedToAnimate = Placement.IdlePed[Placement.Screenplace.iSelectedCharacter]
		CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_SELECTOR_START_ANIM_FOR_SINGLE_PED - Ped A to animate is existing ped: ", NATIVE_TO_INT(aPedToAnimate))
	ENDIF
	
	STRING strAnimDict = GET_CHARACTER_CONTROLLER_ANIM_DICT(ciCHARACTER_CREATOR_ANIM_DICT_SELECTOR, Placement.Screenplace.iSelectedCharacter, (GET_ENTITY_MODEL(aPedToAnimate) = MP_M_FREEMODE_01))
	
	SEQUENCE_INDEX seq
	
	OPEN_SEQUENCE_TASK(seq)
		TASK_PLAY_ANIM_ADVANCED(NULL, strAnimDict, "Intro", GET_CHARACTER_SELECTOR_ANIM_VECTOR(), GET_CHARACTER_SELECTOR_ANIM_ROTATION(), NORMAL_BLEND_IN, NORMAL_BLEND_OUT, DEFAULT, AF_TURN_OFF_COLLISION | AF_EXTRACT_INITIAL_OFFSET)
		TASK_PLAY_ANIM(NULL, strAnimDict, "Loop", NORMAL_BLEND_IN, SLOW_BLEND_OUT, DEFAULT, AF_LOOPING | AF_TURN_OFF_COLLISION)		
	CLOSE_SEQUENCE_TASK(seq)
	
	IF IS_CHARACTER_CONTROLLER_PED_OK(aPedToAnimate)
		TASK_PERFORM_SEQUENCE(aPedToAnimate, seq)
		
		FREEZE_ENTITY_POSITION(aPedToAnimate, FALSE)
		SET_ENTITY_COLLISION(aPedToAnimate, TRUE)
		SET_ENTITY_VISIBLE(aPedToAnimate, TRUE)
		ATTACH_CHARACTER_CREATOR_BOARD_TO_PED(aPedToAnimate, characterData, Placement.Screenplace.iSelectedCharacter)
	ENDIF
	
	CLEAR_SEQUENCE_TASK(seq)
	
	CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_SELECTOR_START_ANIM_FOR_SINGLE_PED - Starting the animation on the single ped, move to PUSH IN")
	
	CLEAR_CHARACTER_CONTROLLER_BIT(ciCHARACTER_CONTROLLER_TRIGGERED_SELECTOR_PUSH_IN)
	SET_CHARACTER_SELECTOR_ANIM_STATE(CHARACTER_SELECTOR_INTRO_ANIM_WAIT_TO_FINISH)
ENDPROC

/// PURPOSE:
///    Setting up the animations for the first ped
PROC PROCESS_CHARACTER_SELECTOR_START_ANIM(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, MPHUD_PLACEMENT_TOOLS& Placement)
	
	PED_INDEX aPedToAnimate
	INT iPlacementIndex = 0
	
	IF GET_CHARACTER_SELECTOR_SLOT_FOR_CREATED_PED(characterData) = iPlacementIndex
		aPedToAnimate = characterData.characterCreatorPeds[characterData.eActiveChild]
		CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_SELECTOR_START_ANIM - Ped A to animate is creator ped: ", NATIVE_TO_INT(aPedToAnimate))
	ELSE
		aPedToAnimate = Placement.IdlePed[iPlacementIndex]
		CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_SELECTOR_START_ANIM - Ped A to animate is existing ped: ", NATIVE_TO_INT(aPedToAnimate))
	ENDIF
	
	STRING strAnimDict = GET_CHARACTER_CONTROLLER_ANIM_DICT(ciCHARACTER_CREATOR_ANIM_DICT_SELECTOR, iPlacementIndex, (GET_ENTITY_MODEL(aPedToAnimate) = MP_M_FREEMODE_01))
	
	SEQUENCE_INDEX seq
	
	OPEN_SEQUENCE_TASK(seq)
		TASK_PLAY_ANIM_ADVANCED(NULL, strAnimDict, "Intro", GET_CHARACTER_SELECTOR_ANIM_VECTOR(), GET_CHARACTER_SELECTOR_ANIM_ROTATION(), NORMAL_BLEND_IN, NORMAL_BLEND_OUT, DEFAULT, AF_TURN_OFF_COLLISION | AF_EXTRACT_INITIAL_OFFSET)
		TASK_PLAY_ANIM(NULL, strAnimDict, "Loop", NORMAL_BLEND_IN, SLOW_BLEND_OUT, DEFAULT, AF_LOOPING | AF_TURN_OFF_COLLISION)		
	CLOSE_SEQUENCE_TASK(seq)
	
	IF IS_CHARACTER_CONTROLLER_PED_OK(aPedToAnimate)
		TASK_PERFORM_SEQUENCE(aPedToAnimate, seq)
		
		FREEZE_ENTITY_POSITION(aPedToAnimate, FALSE)
		SET_ENTITY_COLLISION(aPedToAnimate, TRUE)
		SET_ENTITY_VISIBLE(aPedToAnimate, TRUE)
		ATTACH_CHARACTER_CREATOR_BOARD_TO_PED(aPedToAnimate, characterData, iPlacementIndex)
	ENDIF		

	CLEAR_SEQUENCE_TASK(seq)
	
	IF characterData.iActiveSlotOnSelector != 0
		
		iPlacementIndex = 1
		IF iPlacementIndex = GET_CHARACTER_SELECTOR_SLOT_FOR_CREATED_PED(characterData)
			aPedToAnimate = characterData.characterCreatorPeds[characterData.eActiveChild]
			CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_SELECTOR_START_ANIM - Ped B to animate is creator ped: ", NATIVE_TO_INT(aPedToAnimate))
		ELSE
			aPedToAnimate = Placement.IdlePed[iPlacementIndex]
			CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_SELECTOR_START_ANIM - Ped B to animate is existing ped: ", NATIVE_TO_INT(aPedToAnimate))
		ENDIF
		
		strAnimDict = GET_CHARACTER_CONTROLLER_ANIM_DICT(ciCHARACTER_CREATOR_ANIM_DICT_SELECTOR, iPlacementIndex, (GET_ENTITY_MODEL(aPedToAnimate) = MP_M_FREEMODE_01))
								
		SEQUENCE_INDEX seq2
		OPEN_SEQUENCE_TASK(seq2)
			TASK_PLAY_ANIM_ADVANCED(NULL, strAnimDict, "Intro", GET_CHARACTER_SELECTOR_ANIM_VECTOR(), GET_CHARACTER_SELECTOR_ANIM_ROTATION(), NORMAL_BLEND_IN, NORMAL_BLEND_OUT, DEFAULT, AF_TURN_OFF_COLLISION | AF_EXTRACT_INITIAL_OFFSET)
			TASK_PLAY_ANIM(NULL, strAnimDict, "Loop", NORMAL_BLEND_IN, SLOW_BLEND_OUT, DEFAULT, AF_LOOPING | AF_TURN_OFF_COLLISION)
		CLOSE_SEQUENCE_TASK(seq2)
		
		IF IS_CHARACTER_CONTROLLER_PED_OK(aPedToAnimate)
			
			TASK_PERFORM_SEQUENCE(aPedToAnimate, seq2)
		
			FREEZE_ENTITY_POSITION(aPedToAnimate, FALSE)
			SET_ENTITY_COLLISION(aPedToAnimate, TRUE)
			SET_ENTITY_VISIBLE(aPedToAnimate, TRUE)
			ATTACH_CHARACTER_CREATOR_BOARD_TO_PED(aPedToAnimate, characterData, iPlacementIndex)
		ENDIF
		
		CLEAR_SEQUENCE_TASK(seq2)
	ENDIF
	
	CLEAR_CHARACTER_CONTROLLER_BIT(ciCHARACTER_CONTROLLER_TRIGGERED_SELECTOR_PUSH_IN)
	SET_CHARACTER_SELECTOR_ANIM_STATE(CHARACTER_SELECTOR_INTRO_ANIM_WAIT_TO_FINISH)
ENDPROC

/// PURPOSE:
///    Wait for the last ped to be idling
PROC PROCESS_CHARACTER_SELECTOR_WAIT_TO_FINISH(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, MPHUD_PLACEMENT_TOOLS& Placement)
	
	INT iPedIndex = 0
	PED_INDEX aPedToCheck
	
	IF g_sCharacterControllerData.eCharacterControllerPreviousScreenState = CHARACTER_CONTROLLER_SCREEN_CREATOR
		iPedIndex = Placement.ScreenPlace.iSelectedCharacter
	ENDIF
	
	// If we have more than one ped (ie. an already active slot) we have to wait for second ped to be idling
	IF iPedIndex = GET_CHARACTER_SELECTOR_SLOT_FOR_CREATED_PED(characterData)
		CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_SELECTOR_WAIT_TO_FINISH - Checking the creator ped to WAIT_TO_FINISH")
		aPedToCheck = characterData.characterCreatorPeds[characterData.eActiveChild]
	ELSE
		CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_SELECTOR_WAIT_TO_FINISH - Checking the creator ped to WAIT_TO_FINISH")
		aPedToCheck = Placement.IdlePed[iPedIndex]
	ENDIF
	
	// Make sure the ped is ok
	IF IS_CHARACTER_CONTROLLER_PED_OK(aPedToCheck)
		
		IF NOT IS_CHARACTER_CONTROLLER_BIT_SET(ciCHARACTER_CONTROLLER_TRIGGERED_SELECTOR_PUSH_IN)
			
			IF g_sCharacterControllerData.eCharacterControllerPreviousScreenState = CHARACTER_CONTROLLER_SCREEN_CREATOR
			OR IS_CHARACTER_CONTROLLER_BIT_SET(ciCHARACTER_CONTROLLER_RETURN_FROM_DELETE)
				CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_SELECTOR_WAIT_TO_FINISH - Pushing camera in immediately because we are returning from creator")
				
				SET_CHARACTER_CONTROLLER_BIT(ciCHARACTER_CONTROLLER_TRIGGERED_SELECTOR_PUSH_IN)
				SET_CHARACTER_CONTROLLER_CAMERA_ACTIVE(characterData, CHARACTER_CONTROLLER_CAMERA_SELECTOR_OVERVIEW)
			ELSE
				IF IS_ENTITY_PLAYING_ANIM(aPedToCheck, GET_CHARACTER_CONTROLLER_ANIM_DICT(ciCHARACTER_CREATOR_ANIM_DICT_SELECTOR, iPedIndex, (GET_ENTITY_MODEL(aPedToCheck) = MP_M_FREEMODE_01)), "Intro")
				AND GET_ENTITY_ANIM_CURRENT_TIME(aPedToCheck, GET_CHARACTER_CONTROLLER_ANIM_DICT(ciCHARACTER_CREATOR_ANIM_DICT_SELECTOR, iPedIndex, (GET_ENTITY_MODEL(aPedToCheck) = MP_M_FREEMODE_01)), "Intro") >= 0.60
					
					CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_SELECTOR_WAIT_TO_FINISH - Pushing the camera in now the players have got to end position")
					
					SET_CHARACTER_CONTROLLER_BIT(ciCHARACTER_CONTROLLER_TRIGGERED_SELECTOR_PUSH_IN)
					SET_CHARACTER_CONTROLLER_CAMERA_ACTIVE(characterData, CHARACTER_CONTROLLER_CAMERA_SELECTOR_OVERVIEW)				
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_ENTITY_PLAYING_ANIM(aPedToCheck, GET_CHARACTER_CONTROLLER_ANIM_DICT(ciCHARACTER_CREATOR_ANIM_DICT_SELECTOR, iPedIndex, (GET_ENTITY_MODEL(aPedToCheck) = MP_M_FREEMODE_01)), "Loop")
		OR NOT IS_ENTITY_PLAYING_ANIM(aPedToCheck, GET_CHARACTER_CONTROLLER_ANIM_DICT(ciCHARACTER_CREATOR_ANIM_DICT_SELECTOR, iPedIndex, (GET_ENTITY_MODEL(aPedToCheck) = MP_M_FREEMODE_01)), "Intro")
		// TODO: Add timer
			
			CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_SELECTOR_WAIT_TO_FINISH - Peds are now looping so allow player interaction and play sound")
			
			// Shift time to bright light
			SET_CHARACTER_CONTROLLER_TIME_OF_DAY(characterData, CHARACTER_CONTROLLER_TIME_OF_DAY_MIDNIGHT)
			
			// Only play light sound if first time entering line up
			IF IS_CHARACTER_CONTROLLER_BIT_SET(ciCHARACTER_CONTROLLER_PLAY_OPENING_LIGHT_SOUND)
			
				// Checks audio bank has loaded into memory before playing sound
				IF REQUEST_SCRIPT_AUDIO_BANK("DLC_GTAO/MUGSHOT_ROOM")
					CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_SELECTOR_WAIT_TO_FINISH - GTAO_MUGSHOT_ROOM_SOUNDS is in memory. Play 'Lights_On' sound")
					PLAY_SOUND_FRONTEND(-1,"Lights_On","GTAO_MUGSHOT_ROOM_SOUNDS")
					CLEAR_CHARACTER_CONTROLLER_BIT(ciCHARACTER_CONTROLLER_PLAY_OPENING_LIGHT_SOUND)
				ENDIF
			ENDIF
			
			STRING strAnimDict = GET_CHARACTER_CONTROLLER_ANIM_DICT(ciCHARACTER_CREATOR_ANIM_DICT_SELECTOR, iPedIndex, (GET_ENTITY_MODEL(aPedToCheck) = MP_M_FREEMODE_01))
			SEQUENCE_INDEX seq2
			
			OPEN_SEQUENCE_TASK(seq2)
			
				IF g_sCharacterControllerData.eCharacterControllerPreviousScreenState != CHARACTER_CONTROLLER_SCREEN_CREATOR
				AND NOT IS_CHARACTER_CONTROLLER_BIT_SET(ciCHARACTER_CONTROLLER_RETURN_FROM_DELETE)
					TASK_PLAY_ANIM(NULL, strAnimDict, "react_light", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, DEFAULT, AF_TURN_OFF_COLLISION )
					TASK_PLAY_ANIM(NULL, strAnimDict, "Loop", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, DEFAULT, AF_LOOPING | AF_TURN_OFF_COLLISION)
				ELSE
					IF Placement.ScreenPlace.iSelectedCharacter = iPedIndex
						TASK_PLAY_ANIM(NULL, strAnimDict, "low_to_high", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, DEFAULT, AF_TURN_OFF_COLLISION)
						TASK_PLAY_ANIM(NULL, strAnimDict, "Loop_raised", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, DEFAULT, AF_LOOPING | AF_TURN_OFF_COLLISION)
					ELSE
						TASK_PLAY_ANIM(NULL, strAnimDict, "Loop", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, DEFAULT, AF_LOOPING | AF_TURN_OFF_COLLISION)
					ENDIF
				ENDIF			
			CLOSE_SEQUENCE_TASK(seq2)
			
			TASK_PERFORM_SEQUENCE(aPedToCheck, seq2)
			CLEAR_SEQUENCE_TASK(seq2)
			
			characterData.vPedPositions[iPedIndex] = GET_ENTITY_COORDS(aPedToCheck)
			characterData.vPedRotations[iPedIndex] = GET_ENTITY_ROTATION(aPedToCheck)
			
			CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_SELECTOR_WAIT_TO_FINISH - Ped ", iPedIndex, " coords = ", characterData.vPedPositions[iPedIndex], " rot = ", characterData.vPedRotations[iPedIndex])
			
			IF characterData.iActiveSlotOnSelector != 0
				
				IF iPedIndex = 0
					iPedIndex = 1
				ELSE
					iPedIndex = 0
				ENDIF
				
				// If we have more than one ped (ie. an already active slot) we have to wait for second ped to be idling
				IF iPedIndex = GET_CHARACTER_SELECTOR_SLOT_FOR_CREATED_PED(characterData)
					aPedToCheck = characterData.characterCreatorPeds[characterData.eActiveChild]
				ELSE
					aPedToCheck = Placement.IdlePed[iPedIndex]
				ENDIF
				
				IF IS_CHARACTER_CONTROLLER_PED_OK(aPedToCheck)
					characterData.vPedPositions[iPedIndex] = GET_ENTITY_COORDS(aPedToCheck)
					characterData.vPedRotations[iPedIndex] = GET_ENTITY_ROTATION(aPedToCheck)
					
					strAnimDict = GET_CHARACTER_CONTROLLER_ANIM_DICT(ciCHARACTER_CREATOR_ANIM_DICT_SELECTOR, iPedIndex, (GET_ENTITY_MODEL(aPedToCheck) = MP_M_FREEMODE_01))				
					OPEN_SEQUENCE_TASK(seq2)
						
						IF g_sCharacterControllerData.eCharacterControllerPreviousScreenState != CHARACTER_CONTROLLER_SCREEN_CREATOR
						AND NOT IS_CHARACTER_CONTROLLER_BIT_SET(ciCHARACTER_CONTROLLER_RETURN_FROM_DELETE)
							TASK_PLAY_ANIM(NULL, strAnimDict, "react_light", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, DEFAULT, AF_TURN_OFF_COLLISION )
							TASK_PLAY_ANIM(NULL, strAnimDict, "Loop", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, DEFAULT, AF_LOOPING | AF_TURN_OFF_COLLISION)
						ELSE
							IF Placement.ScreenPlace.iSelectedCharacter = iPedIndex
								TASK_PLAY_ANIM(NULL, strAnimDict, "low_to_high", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, DEFAULT, AF_TURN_OFF_COLLISION)
								TASK_PLAY_ANIM(NULL, strAnimDict, "Loop_raised", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, DEFAULT, AF_LOOPING | AF_TURN_OFF_COLLISION)
							ELSE
								TASK_PLAY_ANIM(NULL, strAnimDict, "Loop", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, DEFAULT, AF_LOOPING | AF_TURN_OFF_COLLISION)
							ENDIF
						ENDIF
						
						TASK_PLAY_ANIM(NULL, strAnimDict, "Loop", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, DEFAULT, AF_LOOPING | AF_TURN_OFF_COLLISION)					
					CLOSE_SEQUENCE_TASK(seq2)
					
					TASK_PERFORM_SEQUENCE(aPedToCheck, seq2)
					CLEAR_SEQUENCE_TASK(seq2)
					
					CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_SELECTOR_WAIT_TO_FINISH - Ped ", iPedIndex, " coords = ", characterData.vPedPositions[iPedIndex], " rot = ", characterData.vPedRotations[iPedIndex])
				ENDIF
			ENDIF			
			
			IF g_sCharacterControllerData.eCharacterControllerPreviousScreenState != CHARACTER_CONTROLLER_SCREEN_CREATOR
			AND NOT IS_CHARACTER_CONTROLLER_BIT_SET(ciCHARACTER_CONTROLLER_RETURN_FROM_DELETE)
				SET_CHARACTER_SELECTOR_ANIM_STATE(CHARACTER_SELECTOR_INTRO_ANIM_FINISH_REACT)
			ELSE
				CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_SELECTOR_WAIT_TO_FINISH - We have returned from creator. Skip the react to light")

				SET_CHARACTER_SELECTOR_ANIM_STATE(CHARACTER_SELECTOR_INTRO_ANIM_COMPLETE)
				SET_CHARACTER_SELECTOR_STATE(CHARACTER_SELECTOR_STATE_RUNNING)
				CLEAR_CHARACTER_CONTROLLER_BIT(ciCHARACTER_CONTROLLER_RETURN_FROM_DELETE)
			ENDIF
			
			// Safety catch now since we push in earlier. See top of this function.
			IF NOT IS_CHARACTER_CONTROLLER_BIT_SET(ciCHARACTER_CONTROLLER_TRIGGERED_SELECTOR_PUSH_IN)
				SET_CHARACTER_CONTROLLER_CAMERA_ACTIVE(characterData, CHARACTER_CONTROLLER_CAMERA_SELECTOR_OVERVIEW)
			ENDIF
		#IF IS_DEBUG_BUILD
		ELSE
			CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_SELECTOR_WAIT_TO_FINISH - Still waiting for ped to be in a safe anim state, Ped = ", NATIVE_TO_INT(aPedToCheck))
		#ENDIF
			
		ENDIF
	#IF IS_DEBUG_BUILD
	ELSE
		CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_SELECTOR_WAIT_TO_FINISH - Still waiting for ped to be ok, Ped = ", NATIVE_TO_INT(aPedToCheck))
	#ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Finish the react to light before starting the push in
PROC PROCESS_CHARACTER_SELECTOR_FINISH_REACT_TO_LIGHT(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, MPHUD_PLACEMENT_TOOLS& Placement)

	INT iPedIndex = 0
	PED_INDEX aPedToCheck
	
	// If we have more than one ped (ie. an already active slot) we have to wait for second ped to be idling
	IF iPedIndex = GET_CHARACTER_SELECTOR_SLOT_FOR_CREATED_PED(characterData)
		aPedToCheck = characterData.characterCreatorPeds[characterData.eActiveChild]
	ELSE
		aPedToCheck = Placement.IdlePed[iPedIndex]
	ENDIF
	
	// Make sure the ped is ok
	IF IS_CHARACTER_CONTROLLER_PED_OK(aPedToCheck)
		
		IF IS_ENTITY_PLAYING_ANIM(aPedToCheck, GET_CHARACTER_CONTROLLER_ANIM_DICT(ciCHARACTER_CREATOR_ANIM_DICT_SELECTOR, 0, (GET_ENTITY_MODEL(aPedToCheck) = MP_M_FREEMODE_01)), "Loop")
		OR NOT IS_ENTITY_PLAYING_ANIM(aPedToCheck, GET_CHARACTER_CONTROLLER_ANIM_DICT(ciCHARACTER_CREATOR_ANIM_DICT_SELECTOR, 0, (GET_ENTITY_MODEL(aPedToCheck) = MP_M_FREEMODE_01)), "react_light")
		// TODO: Add timer
			
			CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_SELECTOR_FINISH_REACT_TO_LIGHT - Peds are now looping so allow player interaction")
			
			STRING strAnimDict = GET_CHARACTER_CONTROLLER_ANIM_DICT(ciCHARACTER_CREATOR_ANIM_DICT_SELECTOR, iPedIndex, (GET_ENTITY_MODEL(aPedToCheck) = MP_M_FREEMODE_01))
			SEQUENCE_INDEX seq2
			
			OPEN_SEQUENCE_TASK(seq2)
				IF Placement.ScreenPlace.iSelectedCharacter = iPedIndex
					TASK_PLAY_ANIM(NULL, strAnimDict, "low_to_high", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, DEFAULT, AF_TURN_OFF_COLLISION)
					TASK_PLAY_ANIM(NULL, strAnimDict, "Loop_raised", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, DEFAULT, AF_LOOPING | AF_TURN_OFF_COLLISION)
				ELSE
					TASK_PLAY_ANIM(NULL, strAnimDict, "Loop", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, DEFAULT, AF_LOOPING | AF_TURN_OFF_COLLISION)
				ENDIF
			CLOSE_SEQUENCE_TASK(seq2)
			
			TASK_PERFORM_SEQUENCE(aPedToCheck, seq2)
			CLEAR_SEQUENCE_TASK(seq2)
			
			CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_SELECTOR_FINISH_REACT_TO_LIGHT - Ped 0, coords = ", characterData.vPedPositions[iPedIndex], " rot = ", characterData.vPedRotations[iPedIndex])
			
			IF characterData.iActiveSlotOnSelector != 0
				
				iPedIndex = 1
				
				// If we have more than one ped (ie. an already active slot) we have to wait for second ped to be idling
				IF iPedIndex = GET_CHARACTER_SELECTOR_SLOT_FOR_CREATED_PED(characterData)
					aPedToCheck = characterData.characterCreatorPeds[characterData.eActiveChild]
				ELSE
					aPedToCheck = Placement.IdlePed[iPedIndex]
				ENDIF
				
				IF IS_CHARACTER_CONTROLLER_PED_OK(aPedToCheck)
					characterData.vPedPositions[iPedIndex] = GET_ENTITY_COORDS(aPedToCheck)
					characterData.vPedRotations[iPedIndex] = GET_ENTITY_ROTATION(aPedToCheck)
					
					strAnimDict = GET_CHARACTER_CONTROLLER_ANIM_DICT(ciCHARACTER_CREATOR_ANIM_DICT_SELECTOR, iPedIndex, (GET_ENTITY_MODEL(aPedToCheck) = MP_M_FREEMODE_01))				
					OPEN_SEQUENCE_TASK(seq2)

						IF Placement.ScreenPlace.iSelectedCharacter = iPedIndex
							TASK_PLAY_ANIM(NULL, strAnimDict, "low_to_high", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, DEFAULT, AF_TURN_OFF_COLLISION)
							TASK_PLAY_ANIM(NULL, strAnimDict, "Loop_raised", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, DEFAULT, AF_LOOPING | AF_TURN_OFF_COLLISION)
						ELSE
							TASK_PLAY_ANIM(NULL, strAnimDict, "Loop", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, DEFAULT, AF_LOOPING | AF_TURN_OFF_COLLISION)
						ENDIF
						
					CLOSE_SEQUENCE_TASK(seq2)
					
					TASK_PERFORM_SEQUENCE(aPedToCheck, seq2)
					CLEAR_SEQUENCE_TASK(seq2)
					
					
					CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_SELECTOR_FINISH_REACT_TO_LIGHT - Ped 1, coords = ", characterData.vPedPositions[iPedIndex], " rot = ", characterData.vPedRotations[iPedIndex])
				ENDIF
			ENDIF
			
			SET_CHARACTER_SELECTOR_ANIM_STATE(CHARACTER_SELECTOR_INTRO_ANIM_COMPLETE)
			SET_CHARACTER_SELECTOR_STATE(CHARACTER_SELECTOR_STATE_RUNNING)
		#IF IS_DEBUG_BUILD
		ELSE
			CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_SELECTOR_FINISH_REACT_TO_LIGHT - Still waiting for ped to be in a safe anim state, Ped = ", NATIVE_TO_INT(aPedToCheck))
		#ENDIF
			
		ENDIF
	#IF IS_DEBUG_BUILD
	ELSE
		CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_SELECTOR_FINISH_REACT_TO_LIGHT - Still waiting for ped to be ok, Ped = ", NATIVE_TO_INT(aPedToCheck))
	#ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Process the intro scene to the selector
PROC PROCESS_CHARACTER_SELECTOR_WALK_IN_ANIM(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, MPHUD_PLACEMENT_TOOLS& Placement)

	SUPPRESS_CHARACTER_MENU_THIS_FRAME()
	
	TURN_MOUSE_CURSOR_OFF()
	
	SWITCH GET_CHARACTER_SELECTOR_ANIM_STATE()
	
		CASE CHARACTER_SELECTOR_INTRO_ANIM_START
			
			// If we have returned from the creator we only need to walk one ped in
			IF g_sCharacterControllerData.eCharacterControllerPreviousScreenState = CHARACTER_CONTROLLER_SCREEN_CREATOR
				PROCESS_CHARACTER_SELECTOR_START_ANIM_FOR_SINGLE_PED(characterData, Placement)
			ELSE
				// Start our animations
				PROCESS_CHARACTER_SELECTOR_START_ANIM(characterData, Placement)
			ENDIF
		BREAK
		
		CASE CHARACTER_SELECTOR_INTRO_ANIM_WAIT_TO_FINISH
		
			IF IS_SCREEN_FADED_OUT()
			AND NOT IS_SCREEN_FADING_IN()
				CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_SELECTOR_WALK_IN_ANIM - Fading camera in (usually came back from delete charater)")
				DO_SCREEN_FADE_IN(0)
			ENDIF
		
			PROCESS_CHARACTER_SELECTOR_WAIT_TO_FINISH(characterData, Placement)
		BREAK
		
		CASE CHARACTER_SELECTOR_INTRO_ANIM_FINISH_REACT
			PROCESS_CHARACTER_SELECTOR_FINISH_REACT_TO_LIGHT(characterData, Placement)
		BREAK
		
		CASE CHARACTER_SELECTOR_INTRO_ANIM_COMPLETE
			// Don't do anything
			
		BREAK
	
	ENDSWITCH
ENDPROC

PROC SET_CHARACTER_SELECTOR_PED_POSITION(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, PED_INDEX &aPed, INT iSlot)
	
	IF IS_CHARACTER_CONTROLLER_PED_OK(aPed)
		CLEAR_PED_TASKS_IMMEDIATELY(aPed)
		
		FREEZE_ENTITY_POSITION(aPed, FALSE)
		SET_ENTITY_VISIBLE(aPed, TRUE)
		SET_ENTITY_COLLISION(aPed, TRUE)
		SET_ENTITY_COORDS_NO_OFFSET(aPed, characterData.vPedPositions[iSlot])
		SET_ENTITY_ROTATION(aPed, characterData.vPedRotations[iSlot])
	ENDIF
ENDPROC

/// PURPOSE:
///    Reposition our peds
PROC REPOSITION_CHARACTER_SELECTOR_PEDS(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, MPHUD_PLACEMENT_TOOLS& Placement)
	INT iPedIndex = 0
	PED_INDEX aPedToCheck
	
	// If we have more than one ped (ie. an already active slot) we have to wait for second ped to be idling
	IF iPedIndex = GET_CHARACTER_SELECTOR_SLOT_FOR_CREATED_PED(characterData)
		aPedToCheck = characterData.characterCreatorPeds[characterData.eActiveChild]
	ELSE
		aPedToCheck = Placement.IdlePed[iPedIndex]
	ENDIF
	
	SET_CHARACTER_SELECTOR_PED_POSITION(characterData, aPedToCheck, iPedIndex)
	
	CPRINTLN(DEBUG_NET_CHARACTER, "REPOSITION_CHARACTER_SELECTOR_PEDS - Ped 0, coords = ", characterData.vPedPositions[iPedIndex], " rot = ", characterData.vPedRotations[iPedIndex])

	IF characterData.iActiveSlotOnSelector != 0
				
		iPedIndex = 1
		
		// If we have more than one ped (ie. an already active slot) we have to wait for second ped to be idling
		IF iPedIndex = GET_CHARACTER_SELECTOR_SLOT_FOR_CREATED_PED(characterData)
			aPedToCheck = characterData.characterCreatorPeds[characterData.eActiveChild]
		ELSE
			aPedToCheck = Placement.IdlePed[iPedIndex]
		ENDIF
		
		SET_CHARACTER_SELECTOR_PED_POSITION(characterData, aPedToCheck, iPedIndex)
		
		CPRINTLN(DEBUG_NET_CHARACTER, "REPOSITION_CHARACTER_SELECTOR_PEDS - Ped 1, coords = ", characterData.vPedPositions[iPedIndex], " rot = ", characterData.vPedRotations[iPedIndex])
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Trigger the walk out animation of the ped who is being edited
PROC PROCESS_CHARACTER_SELECTOR_START_OUTRO_ANIM(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, MPHUD_PLACEMENT_TOOLS& Placement)

	PED_INDEX aPed
	
	IF Placement.ScreenPlace.iSelectedCharacter = GET_CHARACTER_SELECTOR_SLOT_FOR_CREATED_PED(characterData)
		aPed = characterData.characterCreatorPeds[characterData.eActiveChild]
	ELSE
		aPed = Placement.IdlePed[Placement.ScreenPlace.iSelectedCharacter]
	ENDIF

	IF IS_CHARACTER_CONTROLLER_PED_OK(aPed)

		STRING strAnimDict = GET_CHARACTER_CONTROLLER_ANIM_DICT(ciCHARACTER_CREATOR_ANIM_DICT_SELECTOR, Placement.ScreenPlace.iSelectedCharacter, (GET_ENTITY_MODEL(aPed) = MP_M_FREEMODE_01))
		STRING strAnim = GET_CHARACTER_SELECTOR_ANIM(ciCHARACTER_CREATOR_ANIM_SELECTOR_OUTRO)
		
		TASK_PLAY_ANIM(aPed, strAnimDict, strAnim, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, DEFAULT, AF_TURN_OFF_COLLISION)
		
		CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_SELECTOR_START_OUTRO_ANIM - Start the player walking out with anim: ", strAnimDict, ", ", strAnim, " for Ped: ", NATIVE_TO_INT(aPed))
	ENDIF
	
	characterData.selectorOutroTimer = GET_GAME_TIMER()
	SET_CHARACTER_SELECTOR_OUTRO_ANIM_STATE(CHARACTER_SELECTOR_OUTRO_ANIM_MAINTAIN)
ENDPROC

/// PURPOSE:
///    Maintain the outro anim, timer and then cut
PROC PROCESS_CHARACTER_SELECTOR_MAINTAIN_OUTRO_ANIM(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, MPHUD_PLACEMENT_TOOLS& Placement)

	PED_INDEX aPed
		
	IF Placement.ScreenPlace.iSelectedCharacter = GET_CHARACTER_SELECTOR_SLOT_FOR_CREATED_PED(characterData)
		aPed = characterData.characterCreatorPeds[characterData.eActiveChild]
	ELSE
		aPed = Placement.IdlePed[Placement.ScreenPlace.iSelectedCharacter]
	ENDIF
	
	STRING strAnimDict = GET_CHARACTER_CONTROLLER_ANIM_DICT(ciCHARACTER_CREATOR_ANIM_DICT_SELECTOR, Placement.ScreenPlace.iSelectedCharacter, (GET_ENTITY_MODEL(aPed) = MP_M_FREEMODE_01))
	STRING strAnim = GET_CHARACTER_SELECTOR_ANIM(ciCHARACTER_CREATOR_ANIM_SELECTOR_OUTRO)
	
	IF IS_CHARACTER_CONTROLLER_PED_OK(aPed)
	
		FLOAT fPhase = 0.75
		
		IF IS_CHARACTER_CONTROLLER_BIT_SET(ciCHARACTER_CONTROLLER_ENTERING_GAME)
		AND Placement.ScreenPlace.iSelectedCharacter = 0
			fPhase = 0.55
		ENDIF
		
		// Wait until either we are almost finished the animation or we've waited of 10s! 
		IF (IS_ENTITY_PLAYING_ANIM(aPed, strAnimDict, strAnim) AND GET_ENTITY_ANIM_CURRENT_TIME(aPed, strAnimDict, strAnim) >= fPhase)
		OR ABSI(GET_GAME_TIMER() - characterData.selectorOutroTimer) >= ciCHARACTER_SELECTOR_WALK_OUT_TIME
			IF IS_CHARACTER_CONTROLLER_BIT_SET(ciCHARACTER_CONTROLLER_ENTERING_GAME)
				PROCESS_CHARACTER_SELECTOR_ENTER_GAME(characterData, Placement)
				SET_CHARACTER_SELECTOR_OUTRO_ANIM_STATE(CHARACTER_SELECTOR_OUTRO_ANIM_COMPLETE)
				
				//Freeze position after to cancel ped dying/screaming *2094111
				FREEZE_ENTITY_POSITION(aPed, TRUE)
			ELSE
				// Time is up so push us through to the creator now		
				SET_CHARACTER_CREATOR_STATE(CHARACTER_CREATOR_STATE_RP_PROMPT)
				SET_CHARACTER_CONTROLLER_SCREEN_STATE(CHARACTER_CONTROLLER_SCREEN_CREATOR)
				FREEZE_ENTITY_POSITION(aPed, TRUE)
			ENDIF
		ENDIF
	ELSE
		IF IS_CHARACTER_CONTROLLER_BIT_SET(ciCHARACTER_CONTROLLER_ENTERING_GAME)
			PROCESS_CHARACTER_SELECTOR_ENTER_GAME(characterData, Placement)
			SET_CHARACTER_SELECTOR_OUTRO_ANIM_STATE(CHARACTER_SELECTOR_OUTRO_ANIM_COMPLETE)
			FREEZE_ENTITY_POSITION(aPed, TRUE)
		ELSE
			// Just move through otherwise
			SET_CHARACTER_CREATOR_STATE(CHARACTER_CREATOR_STATE_RP_PROMPT)
			SET_CHARACTER_CONTROLLER_SCREEN_STATE(CHARACTER_CONTROLLER_SCREEN_CREATOR)
			FREEZE_ENTITY_POSITION(aPed, TRUE)
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE:
///    Walk the player out of the lineup ready to be edited
PROC PROCESS_CHARACTER_SELECTOR_WALK_OUT_ANIM(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, MPHUD_PLACEMENT_TOOLS& Placement)

	TURN_MOUSE_CURSOR_OFF()
	
	SUPPRESS_CHARACTER_MENU_THIS_FRAME()
	
	SWITCH GET_CHARACTER_SELECTOR_OUTRO_ANIM_STATE()
	
		CASE CHARACTER_SELECTOR_OUTRO_ANIM_START
			PROCESS_CHARACTER_SELECTOR_START_OUTRO_ANIM(characterData, Placement)
		BREAK
		
		CASE CHARACTER_SELECTOR_OUTRO_ANIM_MAINTAIN
			PROCESS_CHARACTER_SELECTOR_MAINTAIN_OUTRO_ANIM(characterData, Placement)
		BREAK
	
	ENDSWITCH

ENDPROC

/// PURPOSE:
///    Handle the player scrolling between the available players
PROC PROCESS_CHARACTER_SELECTOR_CHARACTER_CHANGE(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, MPHUD_PLACEMENT_TOOLS& Placement, INT iIncrement, INT iPCSelect = -1)
	
	// Do not allow us to move characters if there are none
	IF characterData.iActiveSlotOnSelector = 0
		EXIT
	ENDIF
	
	// Direct PC mouse selection
	IF iPCSelect > -1
		Placement.ScreenPlace.iSelectedCharacter = iPCSelect
	ELSE
	// Gamepad/keyboard/Scrollwheel increment
		Placement.ScreenPlace.iSelectedCharacter += iIncrement
	ENDIF
	
	// Note: this is a quick stable way to handle the scrolling while we only have 2 characters
	IF Placement.ScreenPlace.iSelectedCharacter < 0
		Placement.ScreenPlace.iSelectedCharacter = 1
	ENDIF
	
	IF Placement.ScreenPlace.iSelectedCharacter >= 2
		Placement.ScreenPlace.iSelectedCharacter = 0
	ENDIF
	
	characterData.iSelectorSlot = Placement.ScreenPlace.iSelectedCharacter
	
	DISPLAY_CHARACTER_SELECTOR_STAT_CARD(characterData, Placement)
	
	// Play a sound
	PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
	
	// Update the board selection
	PROCESS_CHARACTER_CREATOR_ANIMATE_BOARD_SELECTION(characterData, Placement, Placement.ScreenPlace.iSelectedCharacter)
	
	// Update which ped is visible
	SET_CHARACTER_SELECTOR_PED_VISIBILITY(characterData, Placement)
	
	// Update our buttons
	UPDATE_CHARACTER_CONTROLLER_BUTTONS(characterData)
	
	// Move the camera
	SET_CHARACTER_CONTROLLER_CAMERA_ACTIVE(characterData, CHARACTER_CONTROLLER_CAMERA_SELECTOR_END)
ENDPROC

/// PURPOSE:
///    Handle player siwtching targets and interacting with menu
PROC PROCESS_CHARACTER_SELECTOR_INPUT(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, MPHUD_PLACEMENT_TOOLS& Placement)
		
	// Mouse selection
	INT iMouseSelect = -1
	
	IF HAVE_CONTROLS_CHANGED(FRONTEND_CONTROL)
		// Update our buttons
		UPDATE_CHARACTER_CONTROLLER_BUTTONS(characterData)
	ENDIF
	
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
	
		TURN_MOUSE_CURSOR_ON()
		
		SET_USE_ADJUSTED_MOUSE_COORDS(TRUE)

		iMouseSelect = MOUSE_CHARACTER_SELECT(Placement, characterData)
		
		IF iMouseSelect != -1
		
			IF iMouseSelect = Placement.ScreenPlace.iSelectedCharacter
				
				SET_MOUSE_CURSOR_STYLE( MOUSE_CURSOR_STYLE_HAND_MIDDLE_FINGER)
					
				CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_SELECTOR_INPUT - Player has used mouse to confirm character from the selector. Current Slot: ", Placement.ScreenPlace.iSelectedCharacter, " new ped in: ", GET_CHARACTER_SELECTOR_SLOT_FOR_CREATED_PED(characterData))
				
				PROCESS_CHARACTER_SELECTOR_SELECTION(characterData, Placement)
				
				EXIT
						
			ENDIF
			
		ENDIF
		
	ELSE
		TURN_MOUSE_CURSOR_OFF()
	ENDIF
	
	

	// Listen for the player confirming a player or new ped
	IF IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)

		CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_SELECTOR_INPUT - Player has pressed accept from the selector. Current Slot: ", Placement.ScreenPlace.iSelectedCharacter, " new ped in: ", GET_CHARACTER_SELECTOR_SLOT_FOR_CREATED_PED(characterData))
		
		PROCESS_CHARACTER_SELECTOR_SELECTION(characterData, Placement)

		EXIT
	ENDIF
	
	// Listen for the player quitting the selector
	IF IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
		
		CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_SELECTOR_INPUT - Player has quit from the selector")
		
		PROCESS_CHARACTER_SELECTOR_QUIT(Placement)
		
		// Reset state for next entry
		SET_CHARACTER_SELECTOR_STATE(CHARACTER_SELECTOR_STATE_INIT)
		EXIT
	ENDIF
	
	// Listen for the player deleting a character
	IF IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_DELETE)
		
		IF (IS_STAT_CHARACTER_ACTIVE(Placement.ScreenPlace.iSelectedCharacter)
		OR (HAS_SLOT_BEEN_IGNORED_CODE(Placement.ScreenPlace.iSelectedCharacter+1) AND SCRIPT_IS_CLOUD_AVAILABLE()) )
		AND HAS_ENTERED_OFFLINE_SAVE_FM() = FALSE
		AND IS_SAVING_HAVING_TROUBLE() = FALSE
			
			SET_MOUSE_CURSOR_STYLE(MOUSE_CURSOR_STYLE_ARROW)
			
			CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_SELECTOR_INPUT - Player has chosen to delete a character: ", Placement.ScreenPlace.iSelectedCharacter)
				
			PLAY_SOUND_FRONTEND(-1,"SELECT","HUD_FRONTEND_DEFAULT_SOUNDSET")
			RUN_SCALEFORM_LOCK_RIGHT_MOUSE_BUTTON(FALSE)	
			HUD_CHANGE_STATE(HUD_STATE_DELETE_CHARACTER_PROMPT)	
						
			// Move our selector to the deleting state so when we re-enter we refresh and come back
			SET_CHARACTER_SELECTOR_STATE(CHARACTER_SELECTOR_STATE_DELETING)
		#IF IS_DEBUG_BUILD
		ELSE
			CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_SELECTOR_INPUT - Player chose to delete but it NOT valid to do so")
		#ENDIF
			
		ENDIF
		
		EXIT
	ENDIF
	
	// Listen for the player editing a name
	IF IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_Y)
		
		IF (IS_STAT_CHARACTER_ACTIVE(Placement.ScreenPlace.iSelectedCharacter)
		AND HAS_SLOT_BEEN_IGNORED_SCRIPT(Placement.ScreenPlace.iSelectedCharacter) = FALSE)
		AND HAS_ENTERED_OFFLINE_SAVE_FM() = FALSE
		AND IS_SAVING_HAVING_TROUBLE() = FALSE
			
			CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_SELECTOR_INPUT - Player has chosen to edit the player name")
			
			PLAY_SOUND_FRONTEND(-1,"SELECT","HUD_FRONTEND_DEFAULT_SOUNDSET")
			
			// Move us through to the edit name state
			SET_CHARACTER_SELECTOR_STATE(CHARACTER_SELECTOR_STATE_CHANGE_NAME)
		ENDIF
		
		EXIT
	ENDIF
	
	// Handle change of player
	INT iIncrement
	BOOL bButtonPressed
	
	BOOL bChangeCharacter = SHOULD_SELECTION_BE_INCREMENTED_SP(characterData.iCharacterControllerTimer, iIncrement, bButtonPressed, TRUE, FALSE, FALSE)
	
	IF IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_RB)
	OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_UP)
		bChangeCharacter = TRUE
		iIncrement = 1
	ELIF IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_LB)
	OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_DOWN)
		bChangeCharacter = TRUE
		iIncrement = -1
	ENDIF
	
	IF bChangeCharacter
	OR iMouseSelect > -1
		PROCESS_CHARACTER_SELECTOR_CHARACTER_CHANGE(characterData, Placement, iIncrement, iMouseSelect)
	ENDIF
	

ENDPROC

/// PURPOSE:
///    Handle the player editing the name of the character
PROC PROCESS_CHARACTER_SELECTOR_CHANGE_NAME(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, MPHUD_PLACEMENT_TOOLS& Placement)
	
	BOOL HasQuit
	// Bring up the keyboard
	IF GET_PLAYER_TRANSITION_KEYBOARD_INPUT_NAME(Placement.Transition_oskStatus, Placement.iTransition_KeyboardStage,Placement.iTransition_ProfanityCheck,Placement.iTransition_NumberOfAttempts, HasQuit, Placement.ScreenPlace.iSelectedCharacter)
		
		CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_SELECTOR_CHANGE_NAME - Player has entered a new name: ", GET_MP_STRING_CHARACTER_STAT(MP_STAT_CHAR_NAME, Placement.ScreenPlace.iSelectedCharacter))
		
		UPDATE_CHARACTER_SELECTOR_BOARDS(characterData, Placement)
		SET_CHARACTER_SELECTOR_STATE(CHARACTER_SELECTOR_STATE_RUNNING)	
	ELSE
		IF HasQuit
		
			CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_SELECTOR_CHANGE_NAME - Player has quit the edit screen")

			SET_CHARACTER_SELECTOR_STATE(CHARACTER_SELECTOR_STATE_RUNNING)	
		ENDIF
	ENDIF	
	
ENDPROC


FUNC BOOL GET_CHARACTER_SELECTOR_CREW_INFO(INT &iState, INT &iCrewID, TEXT_LABEL_63 &tlCrewTxd, TEXT_LABEL_15 &tlCrewTag)

	NETWORK_CLAN_DESC clanDesc
	GAMER_HANDLE aLocalHandle = GET_LOCAL_GAMER_HANDLE()

	SWITCH iState
	
		CASE 0
			IF NETWORK_CLAN_PLAYER_GET_DESC(clanDesc, SIZE_OF(clanDesc), aLocalHandle)
				iState = 1
				iCrewID = clanDesc.Id
				NETWORK_CLAN_GET_UI_FORMATTED_TAG_EZ(clanDesc, tlCrewTag)
								
				CPRINTLN(DEBUG_NET_CHARACTER, "GET_CHARACTER_SELECTOR_CREW_INFO - Grabbed player clan description: ID: ", iCrewID, ", CrewTag: ", tlCrewTag)
			#IF IS_DEBUG_BUILD
			ELSE
				CPRINTLN(DEBUG_NET_CHARACTER, "GET_CHARACTER_SELECTOR_CREW_INFO - NETWORK_CLAN_PLAYER_GET_DESC = FALSE")
			#ENDIF
			ENDIF
		BREAK
		
		CASE 1
			IF NETWORK_CLAN_REQUEST_EMBLEM(iCrewID)
				iState = 2
				
				CPRINTLN(DEBUG_NET_CHARACTER, "GET_CHARACTER_SELECTOR_CREW_INFO - NETWORK_CLAN_REQUEST_EMBLEM = TRUE")
			ELSE
				iState = 3
				
				CPRINTLN(DEBUG_NET_CHARACTER, "GET_CHARACTER_SELECTOR_CREW_INFO - NETWORK_CLAN_REQUEST_EMBLEM = FALSE")
			ENDIF
		BREAK
		
		CASE 2
			IF NETWORK_CLAN_IS_EMBLEM_READY(iCrewID, tlCrewTxd)
				CPRINTLN(DEBUG_NET_CHARACTER, "GET_CHARACTER_SELECTOR_CREW_INFO - Successfully got crew texture: ", tlCrewTxd)
				
				iState = 3
				RETURN TRUE
				
			#IF IS_DEBUG_BUILD
			ELSE
				CPRINTLN(DEBUG_NET_CHARACTER, "GET_CHARACTER_SELECTOR_CREW_INFO - NETWORK_CLAN_IS_EMBLEM_READY = FALSE")
			#ENDIF
			ENDIF
		BREAK
		
		
		CASE 3
			RETURN TRUE
		BREAK
	
	ENDSWITCH

	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Maintain our character selector menu. Simple left / right / select / delete or back
PROC MAINTAIN_GTA_ONLINE_CHARACTER_SELECTOR(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, MPHUD_PLACEMENT_TOOLS& Placement)
	
	IF GET_CHARACTER_SELECTOR_STATE() >= CHARACTER_SELECTOR_STATE_RUNNING
		// Ensures any help text we want to show will be shown
		DISPLAY_HUD_WHEN_PAUSED_THIS_FRAME()
		SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_HELP_TEXT)
	ENDIF

	SWITCH GET_CHARACTER_SELECTOR_STATE()
		CASE CHARACTER_SELECTOR_STATE_INIT
					
			// Hide our menus
			SUPPRESS_CHARACTER_MENU_THIS_FRAME()
			
			// Hide the mouse pointer
			TURN_MOUSE_CURSOR_OFF(TRUE)
			
			// Grab which slot we should be looking at.
			INITIALISE_CHARACTER_SELECTOR_SELECTION(characterData, Placement)
			
			// Override the time
			IF NOT IS_CHARACTER_CONTROLLER_BIT_SET(ciCHARACTER_CONTROLLER_RETURN_FROM_DELETE)
				SET_CHARACTER_CONTROLLER_TIME_OF_DAY(characterData, CHARACTER_CONTROLLER_TIME_OF_DAY_NOON)
			ENDIF
			
			// Create the board props (currently invisible)
			CREATE_CHARACTER_SELECTOR_BOARDS(characterData)
			
			// Update the text
			UPDATE_CHARACTER_SELECTOR_BOARDS(characterData, Placement)
			
			// Process any updates coming back here (placement, animation)
			IF UPDATE_CHARACTER_SELECTOR_RETURN_FROM_CREATOR(characterData, Placement)
			
				// Update the time 
				SET_CHARACTER_CONTROLLER_TIME_OF_DAY(characterData, CHARACTER_CONTROLLER_TIME_OF_DAY_MIDNIGHT)
			
//				SET_CHARACTER_SELECTOR_ANIM_STATE(characterData, CHARACTER_SELECTOR_INTRO_ANIM_COMPLETE)
//				SET_CHARACTER_SELECTOR_STATE(characterData, CHARACTER_SELECTOR_STATE_RUNNING)
				
				SET_CHARACTER_SELECTOR_ANIM_STATE(CHARACTER_SELECTOR_INTRO_ANIM_START)
				SET_CHARACTER_SELECTOR_STATE(CHARACTER_SELECTOR_STATE_WALK_IN_ANIM)
			ELSE
				SET_CHARACTER_SELECTOR_ANIM_STATE(CHARACTER_SELECTOR_INTRO_ANIM_START)
				SET_CHARACTER_SELECTOR_STATE(CHARACTER_SELECTOR_STATE_WALK_IN_ANIM)
				SET_CHARACTER_CONTROLLER_BIT(ciCHARACTER_CONTROLLER_PLAY_OPENING_LIGHT_SOUND)
			ENDIF
			
			CLEAR_CHARACTER_CONTROLLER_BIT(ciCHARACTER_CONTROLLER_EDIT_PED_FROM_OLD_DATA)
		BREAK
		
		CASE CHARACTER_SELECTOR_STATE_WALK_IN_ANIM
			PROCESS_CHARACTER_SELECTOR_WALK_IN_ANIM(characterData, Placement)
		BREAK
		
		
		// Main running state
		CASE CHARACTER_SELECTOR_STATE_RUNNING
			PROCESS_CHARACTER_SELECTOR_INPUT(characterData, Placement)
		BREAK
		
		// Handle the player inputting a new name in the keyboard
		CASE CHARACTER_SELECTOR_STATE_CHANGE_NAME
			PROCESS_CHARACTER_SELECTOR_CHANGE_NAME(characterData, Placement)
		BREAK
		
		CASE CHARACTER_SELECTOR_STATE_WALK_OUT_ANIM
			PROCESS_CHARACTER_SELECTOR_WALK_OUT_ANIM(characterData, Placement)
		BREAK
		
		// Wait until we re-enter following a delete
		CASE CHARACTER_SELECTOR_STATE_DELETING
			
			IF NOT IS_STAT_CHARACTER_ACTIVE(Placement.ScreenPlace.iSelectedCharacter)
			
				SUPPRESS_CHARACTER_MENU_THIS_FRAME()
			
				CPRINTLN(DEBUG_NET_CHARACTER, "MAINTAIN_GTA_ONLINE_CHARACTER_SELECTOR - We have re-entered while in the delete state, refresh and get out")
				
				Placement.ScreenPlace.iSelectedCharacter = 0
				characterData.iSelectorSlot = 0
				
				SET_CHARACTER_SELECTOR_SLOT_FOR_CREATED_PED(characterData, -1)
				
				SET_CHARACTER_CONTROLLER_BIT(ciCHARACTER_CONTROLLER_RETURN_FROM_DELETE)
				
				SET_CHARACTER_SELECTOR_STATE(CHARACTER_SELECTOR_STATE_INIT)
				SET_CHARACTER_SELECTOR_OUTRO_ANIM_STATE(CHARACTER_SELECTOR_OUTRO_ANIM_START)
				SET_CHARACTER_SELECTOR_ANIM_STATE(CHARACTER_SELECTOR_INTRO_ANIM_START)
				
				characterData.eCharacterControllerCameraMode = CHARACTER_CONTROLLER_CAMERA_SELECTOR_START
				SET_CHARACTER_CONTROLLER_CAMERA_ACTIVE(characterData, CHARACTER_CONTROLLER_CAMERA_SELECTOR_START)
				
				// Update our slots again
				INITIALISE_CHARACTER_CONTROLLER_PED_SLOTS(characterData, Placement)
				
				INITIALISE_CHARACTER_SELECTOR_SELECTION(characterData, Placement)
			ELSE
				CPRINTLN(DEBUG_NET_CHARACTER, "MAINTAIN_GTA_ONLINE_CHARACTER_SELECTOR - We have re-entered while in the delete state, no delete so go back to running")
				SET_CHARACTER_SELECTOR_STATE(CHARACTER_SELECTOR_STATE_RUNNING)
			ENDIF

//			
//			// Reposition our peds
//			REPOSITION_CHARACTER_SELECTOR_PEDS(characterData, Placement)
//			
//			// Update the text
//			UPDATE_CHARACTER_SELECTOR_BOARDS(characterData)
//			
//			// Attach the board
//			ATTACH_CHARACTER_CREATOR_BOARD_TO_PED(characterData.characterCreatorPeds[characterData.eActiveChild], characterData, GET_CHARACTER_SELECTOR_SLOT_FOR_CREATED_PED(characterData))
//			
//			// Animated the boards
//			PROCESS_CHARACTER_CREATOR_ANIMATE_BOARD_SELECTION(characterData, Placement, Placement.ScreenPlace.iSelectedCharacter)
//			
//			SET_CHARACTER_CONTROLLER_CAMERA_ACTIVE(characterData, CHARACTER_CONTROLLER_CAMERA_SELECTOR_END)
//			
//			SET_CHARACTER_SELECTOR_STATE(CHARACTER_SELECTOR_STATE_RUNNING)
		BREAK
	ENDSWITCH
ENDPROC



/// PURPOSE:
///    Generate the already existing peds
PROC GENERATE_EXISTING_CHARACTERS(MPHUD_PLACEMENT_TOOLS& Placement)

	// Exit early now if we have generated our peds, they will be displayed in lineup
	IF IS_CHARACTER_CONTROLLER_BIT_SET(ciCHARACTER_CONTROLLER_EXISTING_CHARS_CREATED)
		EXIT
	ENDIF
	
	INT I
	
	VECTOR PedRot = GET_CHARACTER_SELECTOR_ANIM_ROTATION()
	VECTOR PedPos = GET_CHARACTER_SELECTOR_ANIM_VECTOR()
	
	BOOL EverythingSetup = TRUE

	FOR I = 0 TO MAX_NUM_CHARACTER_SLOTS-1
	
		BOOL bisSlotActive = IS_STAT_CHARACTER_ACTIVE(I) 
		IF bisSlotActive = TRUE

					
			IF RUN_PED_MENU(Placement.IdlePed[I], GET_PLAYER_MODEL_FOR_TEAM_VARIATION_FOR_MENU(GET_STAT_CHARACTER_TEAM(I), GET_PACKED_STAT_INT(PACKED_CHAR_PICTURE, I)), 
							PedPos, PedRot, FALSE,IS_CHARACTER_MALE(I), I, 50, FALSE, TRUE, DEFAULT, TRUE, TRUE) = FALSE	
				EverythingSetup = FALSE
			ENDIF
		ENDIF
	ENDFOR
	
	IF EverythingSetup
		CPRINTLN(DEBUG_NET_CHARACTER, "GENERATE_EXISTING_PEDS - All existing peds are now generated")
		SET_CHARACTER_CONTROLLER_BIT(ciCHARACTER_CONTROLLER_EXISTING_CHARS_CREATED)
	ENDIF
	
ENDPROC





