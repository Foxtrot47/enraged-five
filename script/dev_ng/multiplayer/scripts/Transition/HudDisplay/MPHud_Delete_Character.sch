USING "screens_header.sch"
USING "stats_tus.sch"
USING "script_network.sch"
USING "screen_tabs.sch"
USING "Net_stat_system.sch"
USING "Transition_Common.sch"

CONST_INT DELETE_CHARACTER_YES 0
CONST_INT DELETE_CHARACTER_NO 1




PROC INIT_MPHUD_DELETE_CHARACTER(MPHUD_PLACEMENT_TOOLS& Placement)

	RESET_ALL_SPRITE_PLACEMENT_VALUES(Placement.SpritePlacement)


	Placement.TextPlacement[0].x = 0.5
	Placement.TextPlacement[0].y = 0.5

	Placement.TextPlacement[1].x = 0.500
	Placement.TextPlacement[1].y = 0.178
	
	Placement.TextPlacement[2].x = 0.500
	Placement.TextPlacement[2].y = 0.209
	
	//Top bar
	Placement.SpritePlacement[0].x = 0.500
	Placement.SpritePlacement[0].y = 0.190
	Placement.SpritePlacement[0].w = 0.675
	Placement.SpritePlacement[0].h = 0.045
	Placement.SpritePlacement[0].r = 255
	Placement.SpritePlacement[0].g = 255
	Placement.SpritePlacement[0].b = 255
	Placement.SpritePlacement[0].a = 200
	
	//Bottom Bar 
	Placement.SpritePlacement[1].x = 0.5
	Placement.SpritePlacement[1].y = 0.222
	Placement.SpritePlacement[1].w = 0.675
	Placement.SpritePlacement[1].h = 0.045
	Placement.SpritePlacement[1].r = 255
	Placement.SpritePlacement[1].g = 255
	Placement.SpritePlacement[1].b = 255
	Placement.SpritePlacement[1].a = 200
	
	
	//Scaleform movie 
	Placement.SpritePlacement[2].x = 0.390
	Placement.SpritePlacement[2].y = 0.467
	Placement.SpritePlacement[2].w = 1
	Placement.SpritePlacement[2].h = 1
	Placement.SpritePlacement[2].r = 255
	Placement.SpritePlacement[2].g = 255
	Placement.SpritePlacement[2].b = 255
	Placement.SpritePlacement[2].a = 200
	
ENDPROC





PROC RENDER_MPHUD_DELETE_CHARACTER(MPHUD_PLACEMENT_TOOLS& Placement)

//	SET_ALL_MENUPED_VISIBLE(Placement.SelectionPed, Placement.IdlePed, FALSE )
	

	// main background
//	DRAW_JOIN_HUD_BACKGROUND()
	
	
//	VECTOR SlotOffset[5]
//	VECTOR SlotHeading[5]
//	
//	SET_PED_SLOT_OFFSETS(SlotOffset, SlotHeading)
	
//	VECTOR PlaceGuy, HeadGuy
//	VECTOR CamRot
//	VECTOR CamCoord
//	IF DOES_CAM_EXIST(GET_RENDERING_CAM())
//		CamRot = GET_CAM_ROT(GET_RENDERING_CAM())
//		CamCoord = GET_CAM_COORD(GET_RENDERING_CAM())
//	ENDIF
	
//	SPRITE_PLACEMENT ScaleformSprite
	
//	PlaceGuy = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(CamCoord,CamRot.z, SlotOffset[0])
//	HeadGuy = CamRot+SlotHeading[0] 
	
	
	
	
	IF GET_CURRENT_HUD_STATE() = HUD_STATE_DELETE_CHARACTER_PROMPT
		IF Placement.iButtonPressedRun = FALSE
		
		// Commented out by James for new CC
//				IF NOT HAS_SLOT_BEEN_IGNORED_SCRIPT(Placement.ScreenPlace.iSelectedCharacter)
//					RUN_PED_MENU(Placement.IdlePed[Placement.ScreenPlace.iSelectedCharacter ], 
//						GET_PLAYER_MODEL_FOR_TEAM_VARIATION_FOR_MENU( GET_STAT_CHARACTER_TEAM(Placement.ScreenPlace.iSelectedCharacter), GET_PACKED_STAT_INT(PACKED_CHAR_PICTURE,  Placement.ScreenPlace.iSelectedCharacter)) 
//						, PlaceGuy, HeadGuy, FALSE,IS_CHARACTER_MALE(Placement.ScreenPlace.iSelectedCharacter), Placement.ScreenPlace.iSelectedCharacter)	
//
//				ENDIF
				
//				SET_ALL_MENUPED_VISIBLE(Placement.SelectionPed,Placement.IdlePed, FALSE)
				
					
	//				RESET_SCALEFORM_INSTRUCTIONAL_BUTTON(Placement.ScaleformStruct)
	//				ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL), "HUD_INPUT3", Placement.ScaleformStruct)
	//				ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT), "HUD_INPUT23", Placement.ScaleformStruct)

					
					
	//				ScaleformSprite = GET_SCALEFORM_ALERTSCREEN_POSITION()
	//				
	//				
	//				
	//				PRIVATE_ADD_SCALEFORM_ALERTSCREEN_DISPLAY_WARNING("HUD_DELETEQUE" ,"HUD_DELETESURE" ,Placement.ScaleformAlertScreenStruct)
	//				PRIVATE_ADD_SCALEFORM_ALERTSCREEN_DISPLAY_TEAM(GET_STAT_CHARACTER_TEAM(Placement.ScreenPlace.iSelectedCharacter) , Placement.ScaleformAlertScreenStruct)
	//				PRIVATE_ADD_SCALEFORM_ALERTSCREEN_PLAYER_ICON(Placement.IdlePed[Placement.ScreenPlace.iSelectedCharacter] ,Placement.ScaleformAlertScreenStruct)
	//				
	//				RUN_SCALEFORM_ALERTSCREEN(Placement.ButtonMovie[GET_SCALEFORM_INDEX_FOR_HUD(HUD_SCALEFORM_ALERT)],ScaleformSprite, Placement.ScaleformAlertScreenStruct, SHOULD_REFRESH_SCALEFORM_ALERTSCREEN(Placement.ScaleformAlertScreenStruct), Placement.ScaleformStruct)
				
		
					
				STRING sStatementText = "HUD_DELETEQUE" 
				STRING sSubText = "HUD_DELEEXPL"
				STRING sLineOne =  "HUD_DELETESURE" 
				
				IF (GET_MP_INT_CHARACTER_STAT(MP_STAT_CASINO_CHIPS, Placement.ScreenPlace.iSelectedCharacter, TRUE)) > 0
					sSubText = "HUD_DELEEXPL2"
				ENDIF

				SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sLineOne, FE_WARNING_YESNO, sSubText)
		
				Placement.ScaleformLoadingStruct.sMainStringSlot = ""
				
		ELSE
		
			SWITCH Placement.iDeleteStages 
				
				CASE DELETE_CHARACTER_KEYBOARD
				
				BREAK

				CASE DELETE_CHARACTER_LOAD_FAILED_BACK_TO_SP
					SET_WARNING_MESSAGE_WITH_HEADER( "HUD_CONNPROB", "DELETELOADFAIL"	, FE_WARNING_OK, "HUD_SPRETURNTRY")
				
				BREAK

				CASE DELETE_CHARACTER_SAVE_FAILED_BACK_TO_SP 
					SET_WARNING_MESSAGE_WITH_HEADER( "HUD_CONNPROB", "DELETESAVEFAIL"	, FE_WARNING_OK, "HUD_SPRETURNTRY")
				
				BREAK

				CASE DELETE_CHARACTER_ERROR_BACK_TO_SP 
					SET_WARNING_MESSAGE_WITH_HEADER( "HUD_CONNPROB", "DELETEERRFAIL"	, FE_WARNING_OK, "HUD_SPRETURNTRY")
				
				BREAK
				
				DEFAULT
					SET_WARNING_MESSAGE_WITH_HEADER( "HUD_CONNPROB", "HUD_DELET"	, FE_WARNING_SPINNER_ONLY, "")
				BREAK
				
			
			ENDSWITCH
		
			
		ENDIF
	ENDIF
	

	


//	CHECK_LOADING_ICON_CHANGE(Placement)
//	SET_SCALEFORM_LOADING_ICON_DISPLAY(Placement.ScaleformLoadingStruct, LOADING_ICON_SPINNER)
//	
//	
//	IF NOT IS_STRING_EMPTY_HUD(Placement.ScaleformLoadingStruct.sMainStringSlot)
//		RUN_SCALEFORM_LOADING_ICON( Placement.ScaleformLoadingStruct, SHOULD_REFRESH_SCALEFORM_LOADING_ICON(Placement.ScaleformLoadingStruct))
//	ELSE
//		MPHUD_CLEAR_SCALEFORM_ELEMENT(Placement.ButtonMovie[GET_SCALEFORM_INDEX_FOR_HUD(HUD_SCALEFORM_LOADING_ICON)], HUD_SCALEFORM_LOADING_ICON)
//	ENDIF
//	
//
//	LOAD_UP_LAST_FRAME_DATA(Placement)

ENDPROC




PROC LOGIC_MPHUD_DELETE_CHARACTER(MPHUD_PLACEMENT_TOOLS& Placement)

	BOOL bHasQuit
	TEXT_LABEL_15 ResultKeyboard

	IF Placement.iButtonPressedRun = FALSE
		IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_X)
			IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
//				PLAY_SOUND_FRONTEND(-1,"SELECT","HUD_FRONTEND_DEFAULT_SOUNDSET")
//				CLEAR_MP_SAVE_STRUCT()
				

				
				Placement.iButtonPressedRun = TRUE
				
			ENDIF
		ENDIF
				
		
				
		IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_CANCEL)
			
			PLAY_SOUND_FRONTEND(-1,"CANCEL","HUD_FRONTEND_DEFAULT_SOUNDSET")
//			PLAY_SOUND_FRONTEND(-1, "CANCEL", "HUD_FRONTEND_DEFAULT_SOUNDSET")
			UNLOAD_PEDHEADSHOT(Placement.ScaleformAlertScreenStruct.HeadshotPic)
			SET_SKIP_ALL_HUD_RESETS(TRUE)
			g_bSkipFaceIconReset = TRUE
			HUD_CHANGE_STATE(Placement.PreviousScreen)
		ENDIF
		

	ELSE
	

	
		Placement.iButtonPressedRun = TRUE
		STRING StringToCompare = GET_FILENAME_FOR_AUDIO_CONVERSATION("PM_DELETE")
		BOOL bRunDeleteShop = FALSE
		SWITCH Placement.iDeleteStages
		
			CASE DELETE_CHARACTER_KEYBOARD
				NET_NL()NET_PRINT("LOGIC_MPHUD_DELETE_CHARACTER Stage: DELETE_CHARACTER_KEYBOARD ")
			
				IF GET_PLAYER_TRANSITION_KEYBOARD_INPUT_DELETE(Placement.Transition_oskStatus, Placement.iTransition_KeyboardStage, bHasQuit, ResultKeyboard)
					IF bHasQuit
						PLAY_SOUND_FRONTEND(-1,"CANCEL","HUD_FRONTEND_DEFAULT_SOUNDSET")
						UNLOAD_PEDHEADSHOT(Placement.ScaleformAlertScreenStruct.HeadshotPic)
						SET_SKIP_ALL_HUD_RESETS(TRUE)
						g_bSkipFaceIconReset = TRUE
						HUD_CHANGE_STATE(Placement.PreviousScreen)
						Placement.iButtonPressedRun = FALSE
					ELSE
					
						IF IS_XBOX_PLATFORM()
							IF GET_CURRENT_LANGUAGE() = LANGUAGE_CHINESE
							OR GET_CURRENT_LANGUAGE() = LANGUAGE_KOREAN
							OR GET_CURRENT_LANGUAGE() = LANGUAGE_RUSSIAN
							OR GET_CURRENT_LANGUAGE() = LANGUAGE_POLISH
							OR GET_CURRENT_LANGUAGE() = LANGUAGE_CHINESE_SIMPLIFIED
								StringToCompare = "delete" //Is in english, won't be translated. 
							ENDIF
						ENDIF
					
						IF COMPARE_STRINGS(ResultKeyboard, StringToCompare, FALSE, 15) = 0
	
							IF USE_SERVER_TRANSACTIONS()
								bRunDeleteShop = TRUE
							ENDIF

							Placement.iDeletedCharacterRank = GET_RANK_FROM_XP_VALUE(GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_XP_FM, Placement.ScreenPlace.iSelectedCharacter, TRUE))
							Placement.iDeletedCharacterTotalTime = GET_MP_INT_CHARACTER_STAT(MP_STAT_TOTAL_PLAYING_TIME, Placement.ScreenPlace.iSelectedCharacter, TRUE)

							IF bRunDeleteShop
								
								Placement.iDeleteStages = DELETE_CHARACTER_PRE_SAVE
							ELSE
								Placement.iDeleteStages = DELETE_CHARACTER_DELETE_SLOT
							ENDIF 
						
							
						ELSE
							Placement.iTransition_KeyboardStage = 0
						ENDIF
					ENDIF
				ENDIF
				
			BREAK
			
			
//			CASE DELETE_CHARACTER_PRE_LOAD
//				IF Placement.ScreenPlace.iSelectedCharacter != GET_ACTIVE_CHARACTER_SLOT()
//					SET_ACTIVE_CHARACTER_SLOT(Placement.ScreenPlace.iSelectedCharacter)
//				ENDIF
//			BREAK
//			
			CASE DELETE_CHARACTER_PRE_SAVE
				NET_NL()NET_PRINT("LOGIC_MPHUD_DELETE_CHARACTER Stage: DELETE_CHARACTER_PRE_SAVE ")
			
				IF HAVE_CODE_STATS_LOADED(0) 
//				AND HAVE_CODE_STATS_LOADED(Placement.ScreenPlace.iSelectedCharacter+1)
					CLEAR_ALL_SAVE_REQUESTS()
					REQUEST_SAVE(SSR_REASON_CHARACHTER_UPDATE, STAT_SAVETYPE_DELETE_CHAR, TRUE)
//					STAT_SAVE(Placement.ScreenPlace.iSelectedCharacter+1, FALSE, STAT_SAVETYPE_DELETE_CHAR)
					Placement.iDeleteStages = DELETE_CHARACTER_PRE_SAVE_STARTED
				ELSE
					IF HAVE_CODE_STATS_LOADED(0) = FALSE
						NET_NL()NET_PRINT("LOGIC_MPHUD_DELETE_CHARACTER DELETE_CHARACTER_PRE_SAVE: HAVE_CODE_STATS_LOADED(0) = FALSE waiting to call a save. ")
					ENDIF
					IF HAVE_CODE_STATS_LOADED(Placement.ScreenPlace.iSelectedCharacter+1) = FALSE
						NET_NL()NET_PRINT("LOGIC_MPHUD_DELETE_CHARACTER DELETE_CHARACTER_PRE_SAVE: HAVE_CODE_STATS_LOADED(Placement.ScreenPlace.iSelectedCharacter+1) = FALSE waiting to call a save. ")
					ENDIF
					IF HAS_IMPORTANT_STATS_LOADED() = FALSE
						NET_NL()NET_PRINT("LOGIC_MPHUD_DELETE_CHARACTER DELETE_CHARACTER_PRE_SAVE: HAS_IMPORTANT_STATS_LOADED() = FALSE waiting to call a save. ")
					ENDIF
					Placement.iDeleteStages = DELETE_CHARACTER_LOAD_FAILED_BACK_TO_SP // A Major error occured, go back to SP. 
				ENDIF
			
			BREAK
			
			
			CASE DELETE_CHARACTER_PRE_SAVE_STARTED
				NET_NL()NET_PRINT("LOGIC_MPHUD_DELETE_CHARACTER Stage: DELETE_CHARACTER_PRE_SAVE_STARTED ")
				IF STAT_SAVE_PENDING_OR_REQUESTED()
				OR STAT_CLOUD_SLOT_SAVE_FAILED(-1) 
					Placement.iDeleteStages = DELETE_CHARACTER_PRE_SAVE_FINISHED
				ELSE
					NET_NL()NET_PRINT("LOGIC_MPHUD_DELETE_CHARACTER: Waiting for Saving to start but STAT_SAVE_PENDING_OR_REQUESTED = FALSE ")
				ENDIF
				
				IF HAS_IMPORTANT_STATS_LOADED() = FALSE
					NET_NL()NET_PRINT("LOGIC_MPHUD_DELETE_CHARACTER DELETE_CHARACTER_PRE_SAVE: HAS_IMPORTANT_STATS_LOADED() = FALSE Player in this transition has already deleted the active character, so assuming saving is ok and let them delete the inactive character. Move on.  ")
					Placement.iDeleteStages = DELETE_CHARACTER_PRE_SAVE_FINISHED
				ENDIF
			
			BREAK
			
			CASE DELETE_CHARACTER_PRE_SAVE_FINISHED
				NET_NL()NET_PRINT("LOGIC_MPHUD_DELETE_CHARACTER Stage: DELETE_CHARACTER_PRE_SAVE_FINISHED ")
				
				IF STAT_CLOUD_SLOT_SAVE_FAILED(-1) 
					NET_NL()NET_PRINT("LOGIC_MPHUD_DELETE_CHARACTER: PRE_SAVE STAT_CLOUD_SLOT_SAVE_FAILED = TRUE, move to save failed.  ")

					Placement.iDeleteStages = DELETE_CHARACTER_SAVE_FAILED_BACK_TO_SP
				ELSE
				
					IF NOT STAT_SAVE_PENDING_OR_REQUESTED()
					
					
						NET_NL()NET_PRINT("LOGIC_MPHUD_DELETE_CHARACTER: PRE_SAVE SUCCESSFUL  ")
						Placement.iDeleteStages = DELETE_CHARACTER_TRIGGER_DELETE
					ELSE
						NET_NL()NET_PRINT("LOGIC_MPHUD_DELETE_CHARACTER: PRE_SAVE waiting for saving to end but STAT_SAVE_PENDING_OR_REQUESTED = TRUE ")
					ENDIF
				ENDIF
				
				
				
			
			BREAK
			
			CASE DELETE_CHARACTER_TRIGGER_DELETE
				NET_NL()NET_PRINT("LOGIC_MPHUD_DELETE_CHARACTER Stage: DELETE_CHARACTER_TRIGGER_DELETE ")
				
				NET_NL()NET_PRINT("[PCDELETE] NET_GAMESERVER_DELETE_CHARACTER called with Character = ")NET_PRINT_INT(Placement.ScreenPlace.iSelectedCharacter)
				//IF NOT NET_GAMESERVER_TRANSACTION_IN_PROGRESS() //removed if we hit here without gameserver game breaks
					IF NET_GAMESERVER_DELETE_CHARACTER(Placement.ScreenPlace.iSelectedCharacter, FALSE, ENUM_TO_INT(DELETE_REASON_CHARACTER_MENU_USER_ACTION))
						NET_NL()NET_PRINT("[PCDELETE] NET_GAMESERVER_DELETE_CHARACTER = TRUE ")
						
						Placement.iDeleteStages = DELETE_CHARACTER_DELETE_STATUS
					ELSE
						NET_NL()NET_PRINT("[PCDELETE] NET_GAMESERVER_DELETE_CHARACTER = FALSE ")
						Placement.iDeleteStages = DELETE_CHARACTER_SAVE_FAILED_BACK_TO_SP
					ENDIF
//				ELSE
//					#IF USE_FINAL_PRINTS
//					PRINTLN_FINAL("[PCDELETE] NET_GAMESERVER_DELETE_CHARACTER  waiting for NET_GAMESERVER_TRANSACTION_IN_PROGRES")
//					#ENDIF
//					PRINTLN("[PCDELETE] NET_GAMESERVER_DELETE_CHARACTER called with Character = ",Placement.ScreenPlace.iSelectedCharacter, " waiting for NET_GAMESERVER_TRANSACTION_IN_PROGRESS")
//				ENDIF
			BREAK
			
			CASE DELETE_CHARACTER_DELETE_STATUS
				NET_NL()NET_PRINT("LOGIC_MPHUD_DELETE_CHARACTER Stage: DELETE_CHARACTER_DELETE_STATUS ")
			
				SWITCH NET_GAMESERVER_DELETE_CHARACTER_GET_STATUS()
					CASE TRANSACTION_STATUS_NONE
						NET_NL()NET_PRINT("[PCDELETE] NET_GAMESERVER_DELETE_CHARACTER_GET_STATUS = TRANSACTION_STATUS_NONE  ")
					BREAK
					
					CASE TRANSACTION_STATUS_PENDING
						NET_NL()NET_PRINT("[PCDELETE] NET_GAMESERVER_DELETE_CHARACTER_GET_STATUS = TRANSACTION_STATUS_PENDING  ")
					BREAK
					
					CASE TRANSACTION_STATUS_FAILED
						NET_NL()NET_PRINT("[PCDELETE] NET_GAMESERVER_DELETE_CHARACTER_GET_STATUS = TRANSACTION_STATUS_FAILED  ")
						Placement.iDeleteStages = DELETE_CHARACTER_ERROR_BACK_TO_SP
					BREAK
					
					CASE TRANSACTION_STATUS_SUCCESSFULL
						
						IF HAS_SHOP_INVENTORY_LOADED_FOR_SLOT(Placement.ScreenPlace.iSelectedCharacter)
							NET_NL()NET_PRINT("[PCDELETE][NET_SHOP] HAS_SHOP_INVENTORY_LOADED_FOR_SLOT = TRUE, Set as false since character is deleted.  ")
							SET_SHOP_INVENTORY_LOADED(FALSE, Placement.ScreenPlace.iSelectedCharacter)
							SET_SHOP_INVENTORY_FAILED(FALSE, Placement.ScreenPlace.iSelectedCharacter)
							NETWORK_SHOP_INVENTORY_DATA g_Private_LoadingNetworkShopInventory_Empty
							g_Private_LoadingNetworkShopInventory = g_Private_LoadingNetworkShopInventory_Empty
							g_Private_Called_NETWORK_SHOP_START_SESSION_WITH_SLOT = -1
						ENDIF
						
						
						NET_NL()NET_PRINT("[PCDELETE] NET_GAMESERVER_DELETE_CHARACTER_GET_STATUS = TRANSACTION_STATUS_SUCCESSFULL  ")
						Placement.iDeleteStages = DELETE_CHARACTER_DELETE_SLOT
					BREAK
					
					CASE TRANSACTION_STATUS_CANCELED
						NET_NL()NET_PRINT("[PCDELETE] NET_GAMESERVER_DELETE_CHARACTER_GET_STATUS = TRANSACTION_STATUS_CANCELED  ")
						Placement.iDeleteStages = DELETE_CHARACTER_ERROR_BACK_TO_SP
					BREAK

				ENDSWITCH
			BREAK
		
			CASE DELETE_CHARACTER_DELETE_SLOT
			
			
				#IF IS_DEBUG_BUILD
					IF g_b_HoldUpDeleteThirdStage
						NET_NL()NET_PRINT("LOGIC_MPHUD_DELETE_CHARACTER Stage: DELETE_CHARACTER_DELETE_SLOT - g_b_HoldUpDeleteThirdStage = TRUE, break ")
						BREAK
					ENDIF
				#ENDIF	
			
				NET_NL()NET_PRINT("LOGIC_MPHUD_DELETE_CHARACTER Stage: DELETE_CHARACTER_DELETE_SLOT ")
			
				IF HAVE_CODE_STATS_LOADED(0) 
//				AND HAVE_CODE_STATS_LOADED(Placement.ScreenPlace.iSelectedCharacter+1)
					IF Delete_Character_Headshot(Placement.ScreenPlace.iSelectedCharacter)
						
						MPGlobalsAmbience.sDailyReset.iSeed = -1
						
						NETWORK_MANUAL_DELETE_CHARACTER(Placement.ScreenPlace.iSelectedCharacter) 
						DELETE_SAVE_SLOT(Placement.ScreenPlace.iSelectedCharacter+1, TRUE, TRUE)//Needs code slot
					
						SET_MP_BOOL_CHARACTER_STAT(MP_STAT_CHAR_ISACTIVE, FALSE, Placement.ScreenPlace.iSelectedCharacter)
						Reset_Delete_Character_Headshot()
						Placement.bWasCharacterDeleted = TRUE
						
						NET_NL()NET_PRINT("LOGIC_MPHUD_DELETE_CHARACTER: Placement.ScreenPlace.iSelectedCharacter = ")NET_PRINT_INT(Placement.ScreenPlace.iSelectedCharacter)
						
	//					PRIVATE_ADD_SCALEFORMXML_CHAR_SELECT_CHAR_IN_SLOT(0, FALSE, Placement.ScaleformXMLCharSelectStruct)
	//					PRIVATE_ADD_SCALEFORMXML_CHAR_SELECT_CHAR_IN_SLOT(1, FALSE, Placement.ScaleformXMLCharSelectStruct)
	//					PRIVATE_ADD_SCALEFORMXML_CHAR_SELECT_CHAR_IN_SLOT(2, FALSE, Placement.ScaleformXMLCharSelectStruct)
	//					PRIVATE_ADD_SCALEFORMXML_CHAR_SELECT_CHAR_IN_SLOT(Placement.ScreenPlace.iSelectedCharacter, FALSE, Placement.ScaleformXMLCharSelectStruct)
	//					REFRESH_SCALEFORMXML_CHAR_SELECT(Placement.ScaleformXMLCharSelectStruct)
	//					RUN_SCALEFORMXML_CHAR_SELECTS(Placement.ScaleformXMLCharSelectStruct, SHOULD_REFRESH_SCALEFORMXML_CHAR_SELECT(Placement.ScaleformXMLCharSelectStruct))
						
						IF GET_MP_INT_PLAYER_STAT(MPPLY_DID_SAVE_TRANS) = 1
							IF Placement.ScreenPlace.iSelectedCharacter = 0
								SET_MP_INT_PLAYER_STAT(MPPLY_DID_SAVE_TRANS, 0)
							ENDIF
						ELIF GET_MP_INT_PLAYER_STAT(MPPLY_DID_SAVE_TRANS) = 2
							IF Placement.ScreenPlace.iSelectedCharacter = 1
								SET_MP_INT_PLAYER_STAT(MPPLY_DID_SAVE_TRANS, 0)
							ENDIF
						ELIF GET_MP_INT_PLAYER_STAT(MPPLY_DID_SAVE_TRANS) = 3
							IF Placement.ScreenPlace.iSelectedCharacter = 1
								SET_MP_INT_PLAYER_STAT(MPPLY_DID_SAVE_TRANS, 1)
							ELIF Placement.ScreenPlace.iSelectedCharacter = 0
								SET_MP_INT_PLAYER_STAT(MPPLY_DID_SAVE_TRANS, 2)
							ENDIF
						ENDIF
						
						
						Placement.iWasCharacterTransferred[Placement.ScreenPlace.iSelectedCharacter] = FALSE
						SET_MP_BOOL_CHARACTER_STAT(MP_STAT_WAS_CHAR_TRANSFERED, FALSE, Placement.ScreenPlace.iSelectedCharacter )
						
						REFRESH_SCALEFORM_LOADING_ICON( Placement.ScaleformLoadingStruct)
						REFRESH_SCALEFORM_INSTRUCTIONAL_BUTTONS(Placement.ScaleformStruct)
						RESET_A_CHAR_PED_CREATION(Placement.ScreenPlace.iSelectedCharacter)
						SET_SLOT_BEEN_IGNORED_SCRIPT(Placement.ScreenPlace.iSelectedCharacter,  FALSE) 						
						
						
						
						Placement.iDeleteStages = DELETE_CHARACTER_DELETE_SLOT_SAVE
					ELSE
						NET_NL()NET_PRINT("LOGIC_MPHUD_DELETE_CHARACTER: Delete_Character_Headshot(")NET_PRINT_INT(Placement.ScreenPlace.iSelectedCharacter)NET_PRINT(")")
					ENDIF
				ELSE

					#IF IS_DEBUG_BUILD
					//Don't care about the other slot not loading ok. Just the one we're in here for. 
					IF HAVE_CODE_STATS_LOADED(0) = FALSE
						NET_NL()NET_PRINT("LOGIC_MPHUD_DELETE_CHARACTER: HAVE_CODE_STATS_LOADED(0) = FALSE waiting to call a save. ")
					ENDIF
					IF HAVE_CODE_STATS_LOADED(Placement.ScreenPlace.iSelectedCharacter+1) = FALSE
						NET_NL()NET_PRINT("LOGIC_MPHUD_DELETE_CHARACTER: HAVE_CODE_STATS_LOADED(Placement.ScreenPlace.iSelectedCharacter+1) = FALSE waiting to call a save. ")
					ENDIF
					NET_NL()NET_PRINT("LOGIC_MPHUD_DELETE_CHARACTER: Placement.ScreenPlace.iSelectedCharacter = ")NET_PRINT_INT(Placement.ScreenPlace.iSelectedCharacter)
					NET_NL()NET_PRINT("LOGIC_MPHUD_DELETE_CHARACTER: GET_ACTIVE_CHARACTER_SLOT = ")NET_PRINT_INT(GET_ACTIVE_CHARACTER_SLOT())
					#ENDIF
					
					
//					IF HAS_ENTERED_OFFLINE_SAVE_FM() = FALSE
//						IF STAT_CLOUD_SLOT_LOAD_FAILED(Placement.ScreenPlace.iSelectedCharacter+1)
//						OR STAT_CLOUD_SLOT_LOAD_FAILED(0)
							NET_NL()NET_PRINT("LOGIC_MPHUD_DELETE_CHARACTER: Saves have failed to load. Bomb back to SP ")							
							Placement.iDeleteStages = DELETE_CHARACTER_LOAD_FAILED_BACK_TO_SP // A Major error occured, go back to SP. 
//						ENDIF
//					ENDIF
				ENDIF
			BREAK
			
			CASE DELETE_CHARACTER_DELETE_SLOT_SAVE  //Wait for new active slot to load before saving
			
				NET_NL()NET_PRINT("LOGIC_MPHUD_DELETE_CHARACTER Stage: DELETE_CHARACTER_DELETE_SLOT_SAVE ")
			
			
				IF HAVE_CODE_STATS_LOADED(0) 
//				AND HAVE_CODE_STATS_LOADED(Placement.ScreenPlace.iSelectedCharacter+1)
					CLEAR_ALL_SAVE_REQUESTS()
//					REQUEST_SAVE(STAT_SAVETYPE_DELETE_CHAR, TRUE)
					STAT_SAVE(Placement.ScreenPlace.iSelectedCharacter+1, FALSE, STAT_SAVETYPE_DELETE_CHAR)
					Placement.iDeleteStages = DELETE_CHARACTER_DELETE_SLOT_SAVE_STARTED
				ELSE
					IF HAVE_CODE_STATS_LOADED(0) = FALSE
						NET_NL()NET_PRINT("LOGIC_MPHUD_DELETE_CHARACTER: HAVE_CODE_STATS_LOADED(0) = FALSE waiting to call a save. ")
					ENDIF
					IF HAVE_CODE_STATS_LOADED(Placement.ScreenPlace.iSelectedCharacter+1) = FALSE
						NET_NL()NET_PRINT("LOGIC_MPHUD_DELETE_CHARACTER: HAVE_CODE_STATS_LOADED(Placement.ScreenPlace.iSelectedCharacter+1) = FALSE waiting to call a save. ")
					ENDIF
					IF HAS_IMPORTANT_STATS_LOADED() = FALSE
						NET_NL()NET_PRINT("LOGIC_MPHUD_DELETE_CHARACTER: HAS_IMPORTANT_STATS_LOADED() = FALSE waiting to call a save. ")
					ENDIF
					
					NET_NL()NET_PRINT("LOGIC_MPHUD_DELETE_CHARACTER: Saves have failed to save. Bomb back to SP ")		
					Placement.iDeleteStages = DELETE_CHARACTER_SAVE_FAILED_BACK_TO_SP
				ENDIF
				
			BREAK
			
			CASE DELETE_CHARACTER_DELETE_SLOT_SAVE_STARTED
				NET_NL()NET_PRINT("LOGIC_MPHUD_DELETE_CHARACTER Stage: DELETE_CHARACTER_DELETE_SLOT_SAVE_STARTED ")
			
			
				IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
					IF STAT_SAVE_PENDING_OR_REQUESTED()
					OR STAT_CLOUD_SLOT_SAVE_FAILED(-1) 
						Placement.iDeleteStages = DELETE_CHARACTER_DELETE_SLOT_SAVE_FINISHED
					ELSE
						NET_NL()NET_PRINT("LOGIC_MPHUD_DELETE_CHARACTER: Waiting for Saving to start but STAT_SAVE_PENDING_OR_REQUESTED = FALSE ")
					ENDIF
				ENDIF
		
			BREAK
			
			CASE DELETE_CHARACTER_DELETE_SLOT_SAVE_FINISHED
			
				NET_NL()NET_PRINT("LOGIC_MPHUD_DELETE_CHARACTER Stage: DELETE_CHARACTER_DELETE_SLOT_SAVE_FINISHED ")
			
				SET_TRANSITION_STRING("HUD_DELETING")
				
				IF STAT_CLOUD_SLOT_SAVE_FAILED(-1) 
					Placement.iDeleteStages = DELETE_CHARACTER_SAVE_FAILED_BACK_TO_SP
				ELSE
				
					IF NOT STAT_SAVE_PENDING_OR_REQUESTED()
						NET_NL()NET_PRINT("LOGIC_MPHUD_DELETE_CHARACTER: SAVE SUCCESSFUL  ")
						DELETE_ALL_INACTIVE_MENUPED(Placement.selectionPed, Placement.IdlePed)
						UNLOAD_PEDHEADSHOT(Placement.ScaleformAlertScreenStruct.HeadshotPic)
						Placement.ScaleformAlertScreenStruct.sAlertTextureName = ""
						HUD_CHANGE_STATE(Placement.PreviousScreen)
						ALLOW_EXPENSIVE_UPDATE_ALL_HEAVILY_ACCESSED_STATS()
						EXPENSIVE_UPDATE_ALL_HEAVILY_ACCESSED_STATS()
						Placement.aBool[0] = FALSE
						Placement.HasGamerSetupChanged = TRUE
						Placement.iDeleteStages = DELETE_CHARACTER_KEYBOARD
						SET_TRANSITION_STRING("")
						
						// Do the fade out in preparation of returning to character selector
						IF IS_SCREEN_FADED_IN()
						AND NOT IS_SCREEN_FADING_OUT()
							DO_SCREEN_FADE_OUT(0)
						ENDIF
						
						IF GET_MP_INT_PLAYER_STAT(MPPLY_LAST_MP_CHAR) = Placement.ScreenPlace.iSelectedCharacter
							SET_MP_INT_PLAYER_STAT(MPPLY_LAST_MP_CHAR, 0)
						ENDIF
						
						Placement.iButtonPressedRun = FALSE
					ELSE
						NET_NL()NET_PRINT("LOGIC_MPHUD_DELETE_CHARACTER: waiting for saving to end but STAT_SAVE_PENDING_OR_REQUESTED = TRUE ")
					ENDIF
				ENDIF
				
				
			BREAK
			
			
			
			CASE DELETE_CHARACTER_LOAD_FAILED_BACK_TO_SP
				
				NET_NL()NET_PRINT("LOGIC_MPHUD_DELETE_CHARACTER Stage: DELETE_CHARACTER_LOAD_FAILED_BACK_TO_SP ")

				
				IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
				
					IF IS_SCREEN_FADED_OUT()
					AND NOT IS_SCREEN_FADING_IN()
						DO_SCREEN_FADE_IN(0)
					ENDIF
				
					Placement.aBool[0] = FALSE
					Placement.HasGamerSetupChanged = TRUE
					Placement.iDeleteStages = DELETE_CHARACTER_KEYBOARD
					Placement.bHasDisconnected = TRUE
					ALLOW_EXPENSIVE_UPDATE_ALL_HEAVILY_ACCESSED_STATS()
					EXPENSIVE_UPDATE_ALL_HEAVILY_ACCESSED_STATS()
					SET_TRANSITION_STRING("")	
					SET_FRONTEND_ACTIVE(FALSE)
//					HUD_CHANGE_STATE(Placement.PreviousScreen)
					REQUEST_TRANSITION(TRANSITION_STATE_RETURN_TO_SINGLEPLAYER, GET_CURRENT_GAMEMODE(), HUD_STATE_LOADING)
				ENDIF
			BREAK
			
			
			CASE DELETE_CHARACTER_SAVE_FAILED_BACK_TO_SP
				
				NET_NL()NET_PRINT("LOGIC_MPHUD_DELETE_CHARACTER Stage: DELETE_CHARACTER_SAVE_FAILED_BACK_TO_SP ")

				
				IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
				
					IF IS_SCREEN_FADED_OUT()
					AND NOT IS_SCREEN_FADING_IN()
						DO_SCREEN_FADE_IN(0)
					ENDIF
				
					Placement.aBool[0] = FALSE
					Placement.HasGamerSetupChanged = TRUE
					Placement.iDeleteStages = DELETE_CHARACTER_KEYBOARD
					Placement.bHasDisconnected = TRUE
					ALLOW_EXPENSIVE_UPDATE_ALL_HEAVILY_ACCESSED_STATS()
					EXPENSIVE_UPDATE_ALL_HEAVILY_ACCESSED_STATS()
					SET_TRANSITION_STRING("")	
					SET_FRONTEND_ACTIVE(FALSE)
//					HUD_CHANGE_STATE(Placement.PreviousScreen)
					REQUEST_TRANSITION(TRANSITION_STATE_RETURN_TO_SINGLEPLAYER, GET_CURRENT_GAMEMODE(), HUD_STATE_LOADING)
				ENDIF
			BREAK
			
			
		
			CASE DELETE_CHARACTER_ERROR_BACK_TO_SP
				
				NET_NL()NET_PRINT("LOGIC_MPHUD_DELETE_CHARACTER Stage: DELETE_CHARACTER_ERROR_BACK_TO_SP ")

				
				IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
				
					IF IS_SCREEN_FADED_OUT()
					AND NOT IS_SCREEN_FADING_IN()
						DO_SCREEN_FADE_IN(0)
					ENDIF
				
					Placement.aBool[0] = FALSE
					Placement.HasGamerSetupChanged = TRUE
					Placement.iDeleteStages = DELETE_CHARACTER_KEYBOARD
					Placement.bHasDisconnected = TRUE
					ALLOW_EXPENSIVE_UPDATE_ALL_HEAVILY_ACCESSED_STATS()
					EXPENSIVE_UPDATE_ALL_HEAVILY_ACCESSED_STATS()
					SET_TRANSITION_STRING("")	
					SET_FRONTEND_ACTIVE(FALSE)
//					HUD_CHANGE_STATE(Placement.PreviousScreen)
					REQUEST_TRANSITION(TRANSITION_STATE_RETURN_TO_SINGLEPLAYER, GET_CURRENT_GAMEMODE(), HUD_STATE_LOADING)			
				ENDIF
			BREAK
			
			
		
		ENDSWITCH
		
	ENDIF	
	
ENDPROC

PROC PROCESS_MPHUD_DELETE_CHARACTER(MPHUD_PLACEMENT_TOOLS& Placement)
	
	
	
		// initialise data
	IF NOT (Placement.bHudScreenInitialised)
		UNLOAD_ALL_COMMON_TRANSITION_TEXTURES_AND_SCALEFORM_MOVIES(Placement)
		INIT_MPHUD_DELETE_CHARACTER(Placement)
		Placement.ScreenPlace.iSelection = DELETE_CHARACTER_NO
		
		RESET_SCALEFORM_INSTRUCTIONAL_BUTTON(Placement.ScaleformStruct)
		REFRESH_SCALEFORM_INSTRUCTIONAL_BUTTONS(Placement.ScaleformStruct)
		
//		IF IS_PAUSE_MENU_ACTIVE()
//			IF IS_THIS_SCREEN_ACTIVE(FE_MENU_VERSION_EMPTY_NO_BACKGROUND) = FALSE
//				NET_NL()NET_PRINT("PROCESS_MPHUD_DELETE_CHARACTER: RESTART_FRONTEND_MENU")
//				RESTART_FRONTEND_MENU(FE_MENU_VERSION_EMPTY_NO_BACKGROUND, GET_CHAR_SELECT_TAB_INDEX(Placement.ScreenPlace.iSelectedCharacter))
//				Placement.bSetupTabs = TRUE
//			ENDIF
//		ENDIF
//		SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_FADE)
		
//		SET_PLAYERS_SELECTED_CHARACTER_TEAM(GET_STAT_CHARACTER_TEAM(Placement.ScreenPlace.iSelectedCharacter))
		

		Placement.ianInt = 0
//		UNLOAD_PEDHEADSHOT()
		placement.iButtonPressedRun = FALSE
		Placement.iDeleteStages = DELETE_CHARACTER_KEYBOARD
		Placement.aBool[0] = FALSE
		Placement.iTransition_KeyboardStage = 0
		Placement.Transition_oskStatus = OSK_PENDING
		
		SET_TRANSITION_STRING("")
		NET_PRINT("MPHUD_DELETE_CHARACTER Screen Initialised iSelectedCharacter = ")NET_PRINT_INT(Placement.ScreenPlace.iSelectedCharacter) NET_NL()
		Placement.bHudScreenInitialised = TRUE
	ENDIF
	
	FORCE_SCRIPTED_GFX_WHEN_FRONTEND_ACTIVE(TRUE)
	SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
	//DRAW_BACKGROUND_BIRDS()

	
	//RUN_XML_ALLSLOTS_PEDSHOTS(Placement.IdlePed, Placement.ScreenPlace.iSelectedCharacter, Placement.iSelectedCharacter_LastFrame)	
	
	LOGIC_MPHUD_DELETE_CHARACTER(Placement)
	
	#IF FEATURE_GEN9_STANDALONE
	IF Placement.bWasCharacterDeleted = TRUE
		//Clean up activities and request availability update
		NETWORK_POST_UDS_ACTIVITY_TERMINATE()
		g_bUDSActivityAvailabilityUpdateRequired = TRUE
	ENDIF
	#ENDIF // FEATURE_GEN9_STANDALONE
	
	IF SCRIPT_IS_CLOUD_AVAILABLE() = FALSE
	
		HUD_CHANGE_STATE(Placement.PreviousScreen)
	ENDIF

	
	
//	IF NOT HAS_SCALEFORM_MOVIE_LOADED(Placement.ButtonMovie[GET_SCALEFORM_INDEX_FOR_HUD(HUD_SCALEFORM_ALERT)] )
//		Placement.ButtonMovie[GET_SCALEFORM_INDEX_FOR_HUD(HUD_SCALEFORM_ALERT)] = REQUEST_SCALEFORM_MOVIE("popup_warning")
//		
//	ENDIF
	
//	REQUEST_STREAMED_TEXTURE_DICT("MPEntry")
//	IF HAS_STREAMED_TEXTURE_DICT_LOADED("MPEntry")
//	IF HAS_ALL_COMMON_HUD_ELEMENTS_LOADED(Placement)
//	AND HAS_SCALEFORM_MOVIE_LOADED(Placement.ButtonMovie[GET_SCALEFORM_INDEX_FOR_HUD(HUD_SCALEFORM_ALERT)] )	
		RENDER_MPHUD_DELETE_CHARACTER(Placement)
//	ENDIF
	
	
//	NET_NL()NET_PRINT("BRENDA: HAS_IMPORTANT_STATS_LOADED = ")NET_PRINT_BOOL(HAS_IMPORTANT_STATS_LOADED())
//	NET_NL()NET_PRINT("BRENDA: GET_ACTIVE_CHARACTER_SLOT() = ")NET_PRINT_INT(GET_ACTIVE_CHARACTER_SLOT())
//	NET_NL()NET_PRINT("BRENDA: HAVE_CODE_STATS_LOADED(0) = ")NET_PRINT_BOOL(HAVE_CODE_STATS_LOADED(0))
//	NET_NL()NET_PRINT("BRENDA: HAVE_CODE_STATS_LOADED(1) = ")NET_PRINT_BOOL(HAVE_CODE_STATS_LOADED(1))
//	NET_NL()NET_PRINT("BRENDA: HAVE_CODE_STATS_LOADED(2) = ")NET_PRINT_BOOL(HAVE_CODE_STATS_LOADED(2))
//
//
//	
		//Event listener
	STRUCT_INVITE_EVENT InviteEvent
	EVENT_NAMES eventType
	INT iCount = 0 
	BOOL bCachedInvite
	REPEAT GET_NUMBER_OF_EVENTS(SCRIPT_EVENT_QUEUE_NETWORK) iCount
		eventType = GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_NETWORK, iCount)      

		// HANDLE ALL EVENTS THAT THIS SCRIPT NEEDS TO KNOW ABOUT
		SWITCH (eventType)

			 CASE EVENT_NETWORK_INVITE_CONFIRMED
				IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, InviteEvent, SIZE_OF(InviteEvent))
				    PROCESS_INVITE_EVENTS(InviteEvent, bCachedInvite)
					HUD_CHANGE_STATE(HUD_STATE_PRE_HUD_CHECKS)
					TRANSITION_CHANGE_STATE(TRANSITION_STATE_PRE_HUD_CHECKS)
				ENDIF
			BREAK
			
		ENDSWITCH
	ENDREPEAT


ENDPROC

