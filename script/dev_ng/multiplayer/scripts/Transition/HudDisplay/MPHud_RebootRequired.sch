
USING "screens_header.sch"
USING "net_events.sch"
USING "script_network.sch"
USING "Transition_Common.sch"



PROC RENDER_MPHUD_REBOOT_REQUIRED(MPHUD_PLACEMENT_TOOLS& Placement)

	SET_ALL_MENUPED_VISIBLE(Placement.SelectionPed, Placement.IdlePed, FALSE )		

	STRING sStatementText = "HUD_CONNPROB"
	STRING sQuestionText = "HUD_BLOCKER"


	IF g_b_WasBailedtoReboot
		sQuestionText = "HUD_REBOOT" 
	ENDIF

	FE_WARNING_FLAGS buttons = FE_WARNING_CONTINUE

	IF IS_SAFE_TO_QUIT_BAIL_SCREEN() = FALSE
	OR STAT_SAVE_PENDING_OR_REQUESTED()

		buttons = FE_WARNING_SPINNER_ONLY
	ENDIF	


	
	SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sQuestionText, buttons)


ENDPROC

PROC LOGIC_MPHUD_REBOOT_REQUIRED()
	
	IF IS_SAFE_TO_QUIT_BAIL_SCREEN()
	AND NOT STAT_SAVE_PENDING_OR_REQUESTED()

		IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
			g_b_WasBailedtoReboot = FALSE
			PLAYSTATS_BACKGROUND_SCRIPT_ACTION("BG_SCRIPT_KICK", 5)
			
			#IF NOT FEATURE_GEN9_STANDALONE
			// gen8 behaviour: shutdown MP session and load save game (i.e. reboot to SP)
			PRINTLN("LOGIC_MPHUD_REBOOT_REQUIRED: Reboot accepted, calling SHUTDOWN_AND_LOAD_MOST_RECENT_SAVE")
			#IF USE_FINAL_PRINTS
			PRINTLN_FINAL("LOGIC_MPHUD_REBOOT_REQUIRED: Reboot accepted, calling SHUTDOWN_AND_LOAD_MOST_RECENT_SAVE")
			#ENDIF
			SHUTDOWN_AND_LOAD_MOST_RECENT_SAVE()
			#ENDIF
			
			#IF FEATURE_GEN9_STANDALONE
			// gen9 behaviour: shutdown MP session and return to landing page
			// (load will be handled naturally during re-entry to game from landing page)
			PRINTLN("LOGIC_MPHUD_REBOOT_REQUIRED: Reboot accepted, calling REQUEST_TRANSITION_TERMINATE_MP_SESSION")
			#IF USE_FINAL_PRINTS
			PRINTLN_FINAL("LOGIC_MPHUD_REBOOT_REQUIRED: Reboot accepted, calling REQUEST_TRANSITION_TERMINATE_MP_SESSION")
			#ENDIF
			REQUEST_TRANSITION_TERMINATE_MP_SESSION()
			#ENDIF
		ENDIF
	ELSe
		PRINTLN("LOGIC_MPHUD_REBOOT_REQUIRED: STAT_SAVE_PENDING_OR_REQUESTED = ", STAT_SAVE_PENDING_OR_REQUESTED(), " " )
	ENDIF


ENDPROC


PROC PROCESS_MPHUD_REBOOT_REQUIRED(MPHUD_PLACEMENT_TOOLS& Placement)
	
	// initialise data
	IF NOT (Placement.bHudScreenInitialised)
		
		RUN_MP_DISCONNECTION_CLEANUP(Placement)
		
		Placement.bHudScreenInitialised = TRUE
	ENDIF

	RUN_MP_DISCONNECTION_CLEANUP_EVERY_FRAME()

	
	RENDER_MPHUD_REBOOT_REQUIRED(Placement)
	
// logic	
	LOGIC_MPHUD_REBOOT_REQUIRED()
	
			
ENDPROC

