
USING "screens_header.sch"
USING "net_events.sch"
USING "script_network.sch"
USING "Transition_Common.sch"


ENUM FAIL_SCREENS
	FAIL_SCREENS_BACKTOSP = 0,
	FAIL_SCREENS_FILECORRUPT,
	FAIL_SCREENS_CONFIRMOVERWRITE,
	#IF IS_DEBUG_BUILD
	FAIL_SCREENS_NOT_SAFE_TO_PROGRESS,
	#ENDIF
	FAIL_SCREENS_CLEANUP_STAGE

ENDENUM

PROC RENDER_MPHUD_STATS_LOAD_FAIL(MPHUD_PLACEMENT_TOOLS& Placement)

	STRING sStatementText = "HUD_CONNPROB" 
	STRING sLineOneMsg = "HUD_STATFAIL" 
	STRING sLineTwoMsg = ""
	SET_ALL_MENUPED_VISIBLE(Placement.SelectionPed, Placement.IdlePed, FALSE )		
	
	FE_WARNING_FLAGS buttons 
		
	IF Placement.aBool[0] = FALSE
		SWITCH INT_TO_ENUM(FAIL_SCREENS, Placement.ianInt)
			
			CASE FAIL_SCREENS_BACKTOSP
				sStatementText = "HUD_CONNPROB" 
				sLineOneMsg = "HUD_CLOUDOFFLIN" //The Rockstar game services are unavailable right now.
				sLineTwoMsg = "HUD_SPRETURNTRY"
				
				IF IS_SAFE_TO_QUIT_BAIL_SCREEN() 
					buttons = FE_WARNING_RETRYCONTINUE
				ELSE
					buttons = FE_WARNING_SPINNER_ONLY
				ENDIF

				SET_WARNING_MESSAGE_WITH_HEADER(sStatementText,sLineOneMsg , buttons, sLineTwoMsg)
			
			BREAK
			
			CASE FAIL_SCREENS_FILECORRUPT
				sStatementText = "HUD_CONNPROB" 
				sLineOneMsg = "HUD_STATCORR" 
				sLineTwoMsg = ""
				
				IF IS_SAFE_TO_QUIT_BAIL_SCREEN() 
					buttons = FE_WARNING_CANCELOVERWRITERETRY
				ELSE
					buttons = FE_WARNING_SPINNER_ONLY
				ENDIF
			
				SET_WARNING_MESSAGE_WITH_HEADER(sStatementText,sLineOneMsg , buttons, sLineTwoMsg)
			
			BREAK
			
			CASE FAIL_SCREENS_CONFIRMOVERWRITE
				sStatementText = "HUD_DELETEQUE" 
				sLineOneMsg = "HUD_WIPESURE" 
				sLineTwoMsg = ""
				
				IF IS_SAFE_TO_QUIT_BAIL_SCREEN() 
					buttons = FE_WARNING_YESNO
				ELSE
					buttons = FE_WARNING_SPINNER_ONLY
				ENDIF
			
				SET_WARNING_MESSAGE_WITH_HEADER(sStatementText,sLineOneMsg , buttons, sLineTwoMsg)
			
			BREAK
			
			#IF IS_DEBUG_BUILD
			CASE FAIL_SCREENS_NOT_SAFE_TO_PROGRESS
				sStatementText = "HUD_DELETEQUE" 
				sLineOneMsg = "HUD_MPNOTSAFE" 
				sLineTwoMsg = "HUD_MPNOTSAFEDES"
				
				IF IS_SAFE_TO_QUIT_BAIL_SCREEN() 
					buttons = FE_WARNING_OK
				ELSE
					buttons = FE_WARNING_SPINNER_ONLY
				ENDIF
			
				SET_WARNING_MESSAGE_WITH_HEADER(sStatementText,sLineOneMsg ,buttons , sLineTwoMsg)
			BREAK
			#ENDIF
			
			DEFAULT
				sStatementText = "HUD_CONNPROB" 
				sLineOneMsg = "HUD_CLOUDOFFLIN" //The Rockstar game services are unavailable right now.
				sLineTwoMsg = "HUD_SPRETURNTRY"
				IF IS_SAFE_TO_QUIT_BAIL_SCREEN() 
					buttons = FE_WARNING_RETRYCONTINUE
				ELSE
					buttons = FE_WARNING_SPINNER_ONLY
				ENDIF
				SET_WARNING_MESSAGE_WITH_HEADER(sStatementText,sLineOneMsg , buttons, sLineTwoMsg)
			BREAK
			
			
	//		CASE FAIL_SCREENS_AUTOSAVEOFF
	//			sStatementText = "HUD_DELETEQUE" 
	//			sLineOneMsg = "HUD_AUSVDIS" 
	//			sLineTwoMsg = "HUD_CONTSURE"
	//			
	//			SET_WARNING_MESSAGE_WITH_HEADER(sStatementText,sLineOneMsg , FE_WARNING_YESNO, sLineTwoMsg)
	//		
	//		BREAK
			
	//		CASE FAIL_SCREENS_CREATE_TEMP_CHARACTERS
	//		
	//			sStatementText = "HUD_DELETEQUE" 
	//			sLineOneMsg = "TEMP_CHARACTER"
	//			sLineTwoMsg = "HUD_CONTSURE"
	//			
	//			SET_WARNING_MESSAGE_WITH_HEADER(sStatementText,sLineOneMsg , FE_WARNING_YESNO, sLineTwoMsg)
	//		BREAK
			
		ENDSWITCH
	
	ELSE
	
		SWITCH Placement.iPlayTutorialStages
			
			CASE 2
				sStatementText = "HUD_CONNPROB" 
				sLineOneMsg = "DELETESAVEFAIL" 
				sLineTwoMsg = "HUD_SPRETURNTRY"
				SET_WARNING_MESSAGE_WITH_HEADER(sStatementText,sLineOneMsg , FE_WARNING_OK, sLineTwoMsg)
			BREAK
			
			DEFAULT
				sStatementText = "HUD_CONNPROB" 
				sLineOneMsg = "HUD_DELET" 
				sLineTwoMsg = ""
				SET_WARNING_MESSAGE_WITH_HEADER(sStatementText,sLineOneMsg , FE_WARNING_SPINNER_ONLY, sLineTwoMsg)
			BREAK
		
		
		ENDSWITCH
	ENDIF
		

	
	
ENDPROC


PROC LOGIC_MPHUD_STATS_LOAD_FAIL(MPHUD_PLACEMENT_TOOLS& Placement)



	
//	IF NOT NETWORK_IS_SESSION_ACTIVE()  
	IF IS_SAFE_TO_QUIT_BAIL_SCREEN()

		IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_X)
				
			SWITCH INT_TO_ENUM(FAIL_SCREENS, Placement.ianInt)
		
				
				CASE FAIL_SCREENS_BACKTOSP
					
					RUN_RETRY(Placement)
					
				BREAK
				
				CASE FAIL_SCREENS_FILECORRUPT
					
					
					Placement.ianInt = ENUM_TO_INT(FAIL_SCREENS_CONFIRMOVERWRITE)
				BREAK
			
				DEFAULT
					RUN_RETRY(Placement)
				BREAK
			
			ENDSWITCH
			
			
			
		ENDIF	


	
		IF Placement.aBool[0] = FALSE
			IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
			OR Placement.ianInt = ENUM_TO_INT(FAIL_SCREENS_CLEANUP_STAGE) //once set to cleanup we don't want to wait for another input 	 bugstar://7481812		                                  

				SWITCH INT_TO_ENUM(FAIL_SCREENS, Placement.ianInt)
				
					CASE FAIL_SCREENS_BACKTOSP
				
						NET_NL()NET_PRINT("LOGIC_MPHUD_STATS_LOAD_FAIL: FAIL_SCREENS_BACKTOSP ")
						
						Placement.ianInt = ENUM_TO_INT(FAIL_SCREENS_CLEANUP_STAGE)
						
	//					Placement.ianInt = ENUM_TO_INT(FAIL_SCREENS_AUTOSAVEOFF)
						
					BREAK
					
					CASE FAIL_SCREENS_CONFIRMOVERWRITE
						Placement.aBool[0] = TRUE

						NET_NL()NET_PRINT("LOGIC_MPHUD_STATS_LOAD_FAIL: FAIL_SCREENS_CONFIRMOVERWRITE ")
						
					BREAK
					
					CASE FAIL_SCREENS_FILECORRUPT
					
						NET_NL()NET_PRINT("LOGIC_MPHUD_STATS_LOAD_FAIL: FAIL_SCREENS_FILECORRUPT ")
						RUN_RETRY(Placement)
					BREAK
					
	//				CASE FAIL_SCREENS_AUTOSAVEOFF
	//				
	//					NET_NL()NET_PRINT("LOGIC_MPHUD_STATS_LOAD_FAIL: FAIL_SCREENS_AUTOSAVEOFF ")
	//					
	//					Placement.ianInt = ENUM_TO_INT(FAIL_SCREENS_CREATE_TEMP_CHARACTERS)
	//				BREAK
	//				
	//				CASE FAIL_SCREENS_CREATE_TEMP_CHARACTERS
	//				
	//					NET_NL()NET_PRINT("LOGIC_MPHUD_STATS_LOAD_FAIL: FAIL_SCREENS_CREATE_TEMP_CHARACTERS ")
	//					RUN_CONTINUE_WITH_NO_SAVE()
	//				
	//				BREAK


					#IF IS_DEBUG_BUILD
					CASE FAIL_SCREENS_NOT_SAFE_TO_PROGRESS
						
						NET_NL()NET_PRINT("LOGIC_MPHUD_STATS_LOAD_FAIL: FAIL_SCREENS_NOT_SAFE_TO_PROGRESS ")			
						SET_TRANSITION_STRING("HUD_QUITTING")
						REQUEST_TRANSITION_TERMINATE_MP_SESSION()
					BREAK
					#ENDIF
					
					
					CASE FAIL_SCREENS_CLEANUP_STAGE
						
						NET_NL()NET_PRINT("LOGIC_MPHUD_STATS_LOAD_FAIL: FAIL_SCREENS_CLEANUP_STAGE ")
						SET_ALL_CODE_STATS_NOT_LOADED_AND_MARK_FOR_RELOAD()						
						
						SET_TRANSITION_STRING("HUD_QUITTING")
						
						REQUEST_TRANSITION_TERMINATE_MP_SESSION()
					
					BREAK
					
					DEFAULT
						NET_NL()NET_PRINT("LOGIC_MPHUD_STATS_LOAD_FAIL: DEFAULT ")
						
						Placement.ianInt = ENUM_TO_INT(FAIL_SCREENS_CLEANUP_STAGE)
					BREAK
					
				ENDSWITCH

				
			ENDIF
		ENDIF
		
//		BOOL bRunDeleteShop = FALSE
		
		IF Placement.aBool[0]
			SWITCH Placement.iPlayTutorialStages
			
				CASE 0
					

//					IF USE_SERVER_TRANSACTIONS()
//						bRunDeleteShop = TRUE
//					ENDIF
				
//					IF bRunDeleteShop
						Placement.iPlayTutorialStages++
						Placement.OverwriteData.Stages = 0
//					ELSE
//						Placement.iPlayTutorialStages++
//					ENDIF 
					
				BREAK
			
				CASE 1
				
					IF RUN_OVERWRITE_FUNCTION(Placement.OverwriteData, FALSE, DELETE_REASON_MAIN_FILE_FAILED_USER_ACTION)
						IF Placement.OverwriteData.Successful
						
						ELSE
							Placement.iPlayTutorialStages = 2
						ENDIF
					ENDIF
				BREAK
				

				CASE 2

					IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
						IF GET_CONFIRM_INVITE_INTO_GAME_STATE()
							HUD_CHANGE_STATE(HUD_STATE_IGNORE_INVITE)	
							TRANSITION_CHANGE_STATE(TRANSITION_STATE_WAIT_HUD_EXIT)
						ELSE							
							SET_TRANSITION_STRING("HUD_QUITTING")								
							REQUEST_TRANSITION_TERMINATE_MP_SESSION()
						ENDIF
					ENDIF
				BREAK
			
			ENDSWITCH
		ENDIF
		
		
		IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_CANCEL)
			
			SWITCH INT_TO_ENUM(FAIL_SCREENS, Placement.ianInt)
				CASE FAIL_SCREENS_FILECORRUPT
				
					IF GET_CONFIRM_INVITE_INTO_GAME_STATE()
						HUD_CHANGE_STATE(HUD_STATE_IGNORE_INVITE)	
						TRANSITION_CHANGE_STATE(TRANSITION_STATE_WAIT_HUD_EXIT)
					ELSE						
						SET_TRANSITION_STRING("HUD_QUITTING")						
						REQUEST_TRANSITION_TERMINATE_MP_SESSION()
					ENDIF
				
				BREAK
				CASE FAIL_SCREENS_CONFIRMOVERWRITE
					Placement.ianInt = ENUM_TO_INT(FAIL_SCREENS_FILECORRUPT)
				BREAK
//				
//				CASE FAIL_SCREENS_AUTOSAVEOFF
//					Placement.ianInt = ENUM_TO_INT(FAIL_SCREENS_RETRYCONTINUE)
//				BREAK
//				
//				CASE FAIL_SCREENS_CREATE_TEMP_CHARACTERS
//					Placement.ianInt = ENUM_TO_INT(FAIL_SCREENS_RETRYCONTINUE)
//				BREAK
//				
			ENDSWITCH
		ENDIF
		
	ELSE

		NET_NL()NET_PRINT("PROCESS_MPHUD_STATS_LOAD_FAIL: IS_SAFE_TO_QUIT_BAIL_SCREEN = FALSE ")		
	ENDIF
		

ENDPROC




PROC PROCESS_MPHUD_STATS_LOAD_FAIL(MPHUD_PLACEMENT_TOOLS& Placement)
	
	// initialise data
	IF NOT (Placement.bHudScreenInitialised)
		IF IS_PAUSE_MENU_ACTIVE()
			SET_PAUSE_MENU_ACTIVE(FALSE)
		ENDIF
		
		 Placement.ianInt = 0
		RUN_MP_DISCONNECTION_CLEANUP(Placement)
		
		Placement.LoadFailCode =  STAT_CLOUD_SLOT_LOAD_FAILED_CODE(0)

		IF Placement.LoadFailCode = LOAD_FAILED_REASON_FILE_CORRUPT
		OR Placement.LoadFailCode = LOAD_FAILED_REASON_FILE_NOT_FOUND
		
			Placement.ianInt = ENUM_TO_INT(FAIL_SCREENS_FILECORRUPT)
		ELSE
			Placement.ianInt = ENUM_TO_INT(FAIL_SCREENS_BACKTOSP)
		ENDIF
				
		#IF IS_DEBUG_BUILD
			IF STAT_GET_LOAD_SAFE_TO_PROGRESS_TO_MP_FROM_SP() = FALSE
			AND Placement.LoadFailCode = LOAD_FAILED_REASON_NONE
			AND STAT_CLOUD_SLOT_LOAD_FAILED(0) = FALSE
			AND STAT_CLOUD_SLOT_LOAD_FAILED(1) = FALSE
			AND STAT_CLOUD_SLOT_LOAD_FAILED(2) = FALSE
				Placement.ianInt = ENUM_TO_INT(FAIL_SCREENS_NOT_SAFE_TO_PROGRESS)
				NET_NL()NET_PRINT("PROCESS_MPHUD_STATS_LOAD_FAIL: Placement.LoadFailCode = STAT_GET_LOAD_SAFE_TO_PROGRESS_TO_MP_FROM_SP() = FALSE & Placement.LoadFailCode = LOAD_FAILED_REASON_NONE ")
				
			ENDIF
		#ENDIF
		
		Placement.iPlayTutorialStages = 0
		Placement.aBool[0] = FALSE
		
		NET_NL()NET_PRINT("PROCESS_MPHUD_STATS_LOAD_FAIL: Placement.LoadFailCode = ")NET_PRINT(GET_CLOUD_LOAD_FAILURE_CODES_STRING(Placement.LoadFailCode))NET_NL()

		
		Placement.bHudScreenInitialised = TRUE
	ENDIF
	
	RUN_MP_DISCONNECTION_CLEANUP_EVERY_FRAME()

	// logic	
	LOGIC_MPHUD_STATS_LOAD_FAIL(Placement)
	RENDER_MPHUD_STATS_LOAD_FAIL(Placement)

			
ENDPROC

