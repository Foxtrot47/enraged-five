//
//
//USING "net_events.sch"
//USING "script_network.sch"
//USING "Screens_Header.sch"
////USING "net_lobby.sch"
//
//USING "Transition_Common.sch"
//
//
//CONST_INT MAX_NUMBER_RACE_RACE_CREATOR_PROMPT_SELECTIONS  6
//
//TRANSITION_CREATOR_RACE_MENU_CHOICE RACECREATORMenuItems[MAX_NUMBER_RACE_RACE_CREATOR_PROMPT_SELECTIONS]
//
//
//PROC CLEAR_RACE_CREATOR_MENU_ITEMS()
//	INT i
//	REPEAT MAX_NUMBER_RACE_RACE_CREATOR_PROMPT_SELECTIONS i
//		RACECREATORMenuItems[i] = CREATOR_RACE_MENU_CHOICE_EMPTY
//	ENDREPEAT
//ENDPROC
//
//FUNC INT NO_OF_RACE_CREATOR_MENU_OPTIONS()
//	INT i
//	INT iCount = 0
//	REPEAT MAX_NUMBER_RACE_RACE_CREATOR_PROMPT_SELECTIONS i
//		IF NOT (RACECREATORMenuItems[i] = CREATOR_RACE_MENU_CHOICE_EMPTY)	
//			iCount++
//		ENDIF
//	ENDREPEAT	
//	RETURN(iCount)
//ENDFUNC
//
//PROC SETUP_RACE_CREATOR_MENU_ITEMS()
//	CLEAR_RACE_CREATOR_MENU_ITEMS()
//	
//
//	RACECREATORMenuItems[0] = CREATOR_RACE_MENU_CHOICE_TUTORIAL	
//	RACECREATORMenuItems[1] = CREATOR_RACE_MENU_CHOICE_CREATE_LAND_RACE
//	RACECREATORMenuItems[2] = CREATOR_RACE_MENU_CHOICE_CREATE_SEA_RACE
//	RACECREATORMenuItems[3] = CREATOR_RACE_MENU_CHOICE_CREATE_AIR_RACE
//	RACECREATORMenuItems[4] = CREATOR_RACE_MENU_CHOICE_RETURN_TO_MENU
//	
//		
//	
//	IF (g_iMenuSelection[HUDMENU_MISSION_CREATOR_RACE] > (NO_OF_RACE_CREATOR_MENU_OPTIONS() - 1))
//		g_iMenuSelection[HUDMENU_MISSION_CREATOR_RACE] = NO_OF_RACE_CREATOR_MENU_OPTIONS() -1
//	ENDIF
//	IF (g_iMenuSelection[HUDMENU_MISSION_CREATOR_RACE] = -1)
//		g_iMenuSelection[HUDMENU_MISSION_CREATOR_RACE] = 0
//	ENDIF	
//ENDPROC
//
//
//PROC INIT_MPHUD_RACE_CREATOR_PROMPT(MPHUD_PLACEMENT_TOOLS& Placement)
//
//	RESET_ALL_SPRITE_PLACEMENT_VALUES(Placement.SpritePlacement)
//
//		
//	Placement.TextPlacement[0].x = 0.500
//	Placement.TextPlacement[0].y = 0.178
//	
//	Placement.TextPlacement[1].x = 0.500
//	Placement.TextPlacement[1].y = 0.209
//	
//	Placement.TextPlacement[2].x = 0.500
//	Placement.TextPlacement[2].y = 0.240
//	
//	Placement.TextPlacement[3].x = 0.500
//	Placement.TextPlacement[3].y = 0.271
//	
//	Placement.TextPlacement[4].x = 0.500
//	Placement.TextPlacement[4].y = 0.302
//	
//	Placement.TextPlacement[5].x = 0.500
//	Placement.TextPlacement[5].y = 0.334
//	
//	Placement.TextPlacement[6].x = 0.500
//	Placement.TextPlacement[6].y = 0.366
//	
//	Placement.TextPlacement[7].x = 0.500
//	Placement.TextPlacement[7].y = 0.398
//	
//	Placement.TextPlacement[8].x = 0.500
//	Placement.TextPlacement[8].y = 0.43
//	
//	
//	//Top bar
//	Placement.SpritePlacement[0].x = 0.500
//	Placement.SpritePlacement[0].y = 0.190
//	Placement.SpritePlacement[0].w = 0.675
//	Placement.SpritePlacement[0].h = 0.045
//	Placement.SpritePlacement[0].r = 255
//	Placement.SpritePlacement[0].g = 255
//	Placement.SpritePlacement[0].b = 255
//	Placement.SpritePlacement[0].a = 200
//	
//	//2nd Bar 
//	Placement.SpritePlacement[1].x = 0.5
//	Placement.SpritePlacement[1].y = 0.222
//	Placement.SpritePlacement[1].w = 0.675
//	Placement.SpritePlacement[1].h = 0.045
//	Placement.SpritePlacement[1].r = 255
//	Placement.SpritePlacement[1].g = 255
//	Placement.SpritePlacement[1].b = 255
//	Placement.SpritePlacement[1].a = 200
//	
//	//3rd Bar 
//	Placement.SpritePlacement[2].x = 0.5
//	Placement.SpritePlacement[2].y = 0.254
//	Placement.SpritePlacement[2].w = 0.675
//	Placement.SpritePlacement[2].h = 0.045
//	Placement.SpritePlacement[2].r = 255
//	Placement.SpritePlacement[2].g = 255
//	Placement.SpritePlacement[2].b = 255
//	Placement.SpritePlacement[2].a = 200
//	
//	//4th Bar 
//	Placement.SpritePlacement[3].x = 0.5
//	Placement.SpritePlacement[3].y = 0.286
//	Placement.SpritePlacement[3].w = 0.675
//	Placement.SpritePlacement[3].h = 0.045
//	Placement.SpritePlacement[3].r = 255
//	Placement.SpritePlacement[3].g = 255
//	Placement.SpritePlacement[3].b = 255
//	Placement.SpritePlacement[3].a = 200
//	
//	//5th Bar 
//	Placement.SpritePlacement[4].x = 0.5
//	Placement.SpritePlacement[4].y = 0.318
//	Placement.SpritePlacement[4].w = 0.675
//	Placement.SpritePlacement[4].h = 0.045
//	Placement.SpritePlacement[4].r = 255
//	Placement.SpritePlacement[4].g = 255
//	Placement.SpritePlacement[4].b = 255
//	Placement.SpritePlacement[4].a = 200
//	
//	//6th Bar 
//	Placement.SpritePlacement[5].x = 0.5
//	Placement.SpritePlacement[5].y = 0.35
//	Placement.SpritePlacement[5].w = 0.675
//	Placement.SpritePlacement[5].h = 0.045
//	Placement.SpritePlacement[5].r = 255
//	Placement.SpritePlacement[5].g = 255
//	Placement.SpritePlacement[5].b = 255
//	Placement.SpritePlacement[5].a = 200
//	
//	//7th Bar 
//	Placement.SpritePlacement[6].x = 0.5
//	Placement.SpritePlacement[6].y = 0.382
//	Placement.SpritePlacement[6].w = 0.675
//	Placement.SpritePlacement[6].h = 0.045
//	Placement.SpritePlacement[6].r = 255
//	Placement.SpritePlacement[6].g = 255
//	Placement.SpritePlacement[6].b = 255
//	Placement.SpritePlacement[6].a = 200
//	
//	//8th Bar 
//	Placement.SpritePlacement[7].x = 0.5
//	Placement.SpritePlacement[7].y = 0.414
//	Placement.SpritePlacement[7].w = 0.675
//	Placement.SpritePlacement[7].h = 0.045
//	Placement.SpritePlacement[7].r = 255
//	Placement.SpritePlacement[7].g = 255
//	Placement.SpritePlacement[7].b = 255
//	Placement.SpritePlacement[7].a = 200
//	
//	
//ENDPROC
//
//PROC RENDER_MPHUD_RACE_CREATOR_PROMPT(MPHUD_PLACEMENT_TOOLS& Placement)
//
//
////	DRAW_TEXT(Placement.MainTitle, Placement.aStyle.TS_TITLEHUD, "HUD_MAINTIT")	
////	DRAW_JOIN_HUD_BACKGROUND()
//	
//	SETUP_RACE_CREATOR_MENU_ITEMS()
//	
//	VECTOR SlotOffset[5]
//	VECTOR SlotHeading[5]
//		
////	SlotOffset[0] = <<-1.352, 3.250, -0.085>>
////	SlotHeading[0] = <<331.200,7.920, 223.920>>
//	
//	SET_PED_SLOT_OFFSETS(SlotOffset, SlotHeading)
//
//		
//	VECTOR PlaceGuy, HeadGuy
//	VECTOR CamRot
//	VECTOR CamCoord
//	IF DOES_CAM_EXIST(GET_RENDERING_CAM())
//		CamRot = GET_CAM_ROT(GET_RENDERING_CAM())
//		CamCoord = GET_CAM_COORD(GET_RENDERING_CAM())
//	ENDIF
//
//	
//	PlaceGuy = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(CamCoord,CamRot.z, SlotOffset[0])
//	HeadGuy = CamRot+SlotHeading[0] 
//
//	
//	INT I
//	
//	
//	IF GET_CURRENT_GAMEMODE() = GAMEMODE_FM
//	OR GET_JOINING_GAMEMODE() = GAMEMODE_FM
//
//		FOR I = 0 TO MAX_NUM_CHARACTER_SLOTS-1
//			IF IS_STAT_CHARACTER_ACTIVE(I)
//				RUN_PED_MENU(Placement.IdlePed[I], 
//						GET_PLAYER_MODEL_FOR_TEAM_VARIATION_FOR_MENU( GET_STAT_CHARACTER_TEAM(I), GET_PACKED_STAT_INT(PACKED_CHAR_PICTURE, I)) 
//						, PlaceGuy, HeadGuy, FALSE,IS_CHARACTER_MALE(I), I)	
//			ENDIF
//		ENDFOR
//
//		SET_ALL_MENUPED_VISIBLE(Placement.SelectionPed,Placement.IdlePed, FALSE)
//	ENDIF
//
//	TEXT_STYLE TS_TEXT
//
//	SET_STANDARD_MEDIUM_HUD_TEXT(TS_TEXT)
//
//	INT iCount
//	REPEAT NO_OF_RACE_CREATOR_MENU_OPTIONS() i
//		IF NOT (RACECREATORMenuItems[i] = CREATOR_RACE_MENU_CHOICE_EMPTY)
//			SWITCH RACECREATORMenuItems[i]
//
//				CASE CREATOR_RACE_MENU_CHOICE_CREATE_LAND_RACE
//					IF g_iMenuSelection[HUDMENU_MISSION_CREATOR_RACE] = iCount
//						SET_TEXT_BLACK(TS_TEXT)
//						IF GET_MP_BOOL_PLAYER_STAT(MPPLY_DONE_RACE_CREATOR_TUT) 
//							DRAW_TEXT_WITH_ALIGNMENT(Placement.TextPlacement[iCount], TS_TEXT, "FMMC_T2_MT_0", TRUE, FALSE)
//						ELSE
//							DRAW_TEXT_WITH_GXT_STRING(Placement.TextPlacement[iCount], TS_TEXT,"HUD_LOCKED", "FMMC_T2_MT_0", FONT_CENTRE)
//						ENDIF
//						SET_TEXT_WHITE(TS_TEXT)
//						DRAW_2D_SPRITE("MPEntry", "MP_ModeSelected_Gradient", Placement.SpritePlacement[iCount], FALSE)
//					ELSE
//						IF GET_MP_BOOL_PLAYER_STAT(MPPLY_DONE_RACE_CREATOR_TUT) 
//							DRAW_TEXT_WITH_ALIGNMENT(Placement.TextPlacement[iCount], TS_TEXT, "FMMC_T2_MT_0", TRUE, FALSE)
//							DRAW_2D_SPRITE("MPEntry", "MP_ModeNotSelected_Gradient", Placement.SpritePlacement[iCount], FALSE)
//						ELSE
//							SET_TEXT_GREY(TS_TEXT)
//							DRAW_TEXT_WITH_GXT_STRING(Placement.TextPlacement[iCount], TS_TEXT,"HUD_LOCKED", "FMMC_T2_MT_0", FONT_CENTRE)
//							SET_TEXT_WHITE(TS_TEXT)
//							DRAW_2D_SPRITE("MPEntry", "MP_ModeNotSelected_Gradient", Placement.SpritePlacement[iCount], FALSE)
//						ENDIF
//					ENDIF
//				BREAK
//				
//				CASE CREATOR_RACE_MENU_CHOICE_CREATE_LAND_POINT2POINT
//					IF g_iMenuSelection[HUDMENU_MISSION_CREATOR_RACE] = iCount
//						SET_TEXT_BLACK(TS_TEXT)
//						IF GET_MP_BOOL_PLAYER_STAT(MPPLY_DONE_RACE_CREATOR_TUT) 
//							DRAW_TEXT_WITH_ALIGNMENT(Placement.TextPlacement[iCount], TS_TEXT, "FMMC_T2_MT_1", TRUE, FALSE)
//						ELSE
//							DRAW_TEXT_WITH_GXT_STRING(Placement.TextPlacement[iCount], TS_TEXT,"HUD_LOCKED", "FMMC_T2_MT_1", FONT_CENTRE)
//						ENDIF
//						SET_TEXT_WHITE(TS_TEXT)
//						DRAW_2D_SPRITE("MPEntry", "MP_ModeSelected_Gradient", Placement.SpritePlacement[iCount], FALSE)
//					ELSE
//						IF GET_MP_BOOL_PLAYER_STAT(MPPLY_DONE_RACE_CREATOR_TUT) 
//							DRAW_TEXT_WITH_ALIGNMENT(Placement.TextPlacement[iCount], TS_TEXT, "FMMC_T2_MT_1", TRUE, FALSE)
//							DRAW_2D_SPRITE("MPEntry", "MP_ModeNotSelected_Gradient", Placement.SpritePlacement[iCount], FALSE)
//						ELSE
//							SET_TEXT_GREY(TS_TEXT)
//							DRAW_TEXT_WITH_GXT_STRING(Placement.TextPlacement[iCount], TS_TEXT,"HUD_LOCKED", "FMMC_T2_MT_1", FONT_CENTRE)
//							SET_TEXT_WHITE(TS_TEXT)
//							DRAW_2D_SPRITE("MPEntry", "MP_ModeNotSelected_Gradient", Placement.SpritePlacement[iCount], FALSE)
//						ENDIF
//					ENDIF
//				BREAK
//				
//				CASE CREATOR_RACE_MENU_CHOICE_CREATE_SEA_RACE
//					IF g_iMenuSelection[HUDMENU_MISSION_CREATOR_RACE] = iCount
//						SET_TEXT_BLACK(TS_TEXT)
//						IF GET_MP_BOOL_PLAYER_STAT(MPPLY_DONE_RACE_CREATOR_TUT) 
//							DRAW_TEXT_WITH_ALIGNMENT(Placement.TextPlacement[iCount], TS_TEXT, "FMMC_T2_MT_2", TRUE, FALSE)
//						ELSE
//							DRAW_TEXT_WITH_GXT_STRING(Placement.TextPlacement[iCount], TS_TEXT,"HUD_LOCKED", "FMMC_T2_MT_2", FONT_CENTRE)
//						ENDIF
//						SET_TEXT_WHITE(TS_TEXT)
//						DRAW_2D_SPRITE("MPEntry", "MP_ModeSelected_Gradient", Placement.SpritePlacement[iCount], FALSE)
//					ELSE
//						IF GET_MP_BOOL_PLAYER_STAT(MPPLY_DONE_RACE_CREATOR_TUT) 
//							DRAW_TEXT_WITH_ALIGNMENT(Placement.TextPlacement[iCount], TS_TEXT, "FMMC_T2_MT_2", TRUE, FALSE)
//							DRAW_2D_SPRITE("MPEntry", "MP_ModeNotSelected_Gradient", Placement.SpritePlacement[iCount], FALSE)
//						ELSE
//							SET_TEXT_GREY(TS_TEXT)
//							DRAW_TEXT_WITH_GXT_STRING(Placement.TextPlacement[iCount], TS_TEXT,"HUD_LOCKED", "FMMC_T2_MT_2", FONT_CENTRE)
//							SET_TEXT_WHITE(TS_TEXT)
//							DRAW_2D_SPRITE("MPEntry", "MP_ModeNotSelected_Gradient", Placement.SpritePlacement[iCount], FALSE)
//						ENDIF
//					ENDIF
//				BREAK
//				
//				CASE CREATOR_RACE_MENU_CHOICE_CREATE_SEA_POINT2POINT
//					IF g_iMenuSelection[HUDMENU_MISSION_CREATOR_RACE] = iCount
//						SET_TEXT_BLACK(TS_TEXT)
//						IF GET_MP_BOOL_PLAYER_STAT(MPPLY_DONE_RACE_CREATOR_TUT) 
//							DRAW_TEXT_WITH_ALIGNMENT(Placement.TextPlacement[iCount], TS_TEXT, "FMMC_T2_MT_3", TRUE, FALSE)
//						ELSE
//							DRAW_TEXT_WITH_GXT_STRING(Placement.TextPlacement[iCount], TS_TEXT,"HUD_LOCKED", "FMMC_T2_MT_3", FONT_CENTRE)
//						ENDIF
//						SET_TEXT_WHITE(TS_TEXT)
//						DRAW_2D_SPRITE("MPEntry", "MP_ModeSelected_Gradient", Placement.SpritePlacement[iCount], FALSE)
//					ELSE
//						IF GET_MP_BOOL_PLAYER_STAT(MPPLY_DONE_RACE_CREATOR_TUT) 
//							DRAW_TEXT_WITH_ALIGNMENT(Placement.TextPlacement[iCount], TS_TEXT, "FMMC_T2_MT_3", TRUE, FALSE)
//							DRAW_2D_SPRITE("MPEntry", "MP_ModeNotSelected_Gradient", Placement.SpritePlacement[iCount], FALSE)
//						ELSE
//							SET_TEXT_GREY(TS_TEXT)
//							DRAW_TEXT_WITH_GXT_STRING(Placement.TextPlacement[iCount], TS_TEXT,"HUD_LOCKED", "FMMC_T2_MT_3", FONT_CENTRE)
//							SET_TEXT_WHITE(TS_TEXT)
//							DRAW_2D_SPRITE("MPEntry", "MP_ModeNotSelected_Gradient", Placement.SpritePlacement[iCount], FALSE)
//						ENDIF
//					ENDIF
//				
//				BREAK
//				
//				CASE CREATOR_RACE_MENU_CHOICE_CREATE_AIR_RACE
//					IF g_iMenuSelection[HUDMENU_MISSION_CREATOR_RACE] = iCount
//						SET_TEXT_BLACK(TS_TEXT)
//						IF GET_MP_BOOL_PLAYER_STAT(MPPLY_DONE_RACE_CREATOR_TUT) 
//							DRAW_TEXT_WITH_ALIGNMENT(Placement.TextPlacement[iCount], TS_TEXT, "FMMC_T2_MT_4", TRUE, FALSE)
//						ELSE
//							DRAW_TEXT_WITH_GXT_STRING(Placement.TextPlacement[iCount], TS_TEXT,"HUD_LOCKED", "FMMC_T2_MT_4", FONT_CENTRE)
//						ENDIF
//						SET_TEXT_WHITE(TS_TEXT)
//						DRAW_2D_SPRITE("MPEntry", "MP_ModeSelected_Gradient", Placement.SpritePlacement[iCount], FALSE)
//					ELSE
//						IF GET_MP_BOOL_PLAYER_STAT(MPPLY_DONE_RACE_CREATOR_TUT) 
//							DRAW_TEXT_WITH_ALIGNMENT(Placement.TextPlacement[iCount], TS_TEXT, "FMMC_T2_MT_4", TRUE, FALSE)
//							DRAW_2D_SPRITE("MPEntry", "MP_ModeNotSelected_Gradient", Placement.SpritePlacement[iCount], FALSE)
//						ELSE
//							SET_TEXT_GREY(TS_TEXT)
//							DRAW_TEXT_WITH_GXT_STRING(Placement.TextPlacement[iCount], TS_TEXT,"HUD_LOCKED", "FMMC_T2_MT_4", FONT_CENTRE)
//							SET_TEXT_WHITE(TS_TEXT)
//							DRAW_2D_SPRITE("MPEntry", "MP_ModeNotSelected_Gradient", Placement.SpritePlacement[iCount], FALSE)
//						ENDIF
//					ENDIF
//				
//				BREAK
//				
//				CASE CREATOR_RACE_MENU_CHOICE_CREATE_AIR_POINT2POINT
//					IF g_iMenuSelection[HUDMENU_MISSION_CREATOR_RACE] = iCount
//						SET_TEXT_BLACK(TS_TEXT)
//						IF GET_MP_BOOL_PLAYER_STAT(MPPLY_DONE_RACE_CREATOR_TUT) 
//							DRAW_TEXT_WITH_ALIGNMENT(Placement.TextPlacement[iCount], TS_TEXT, "FMMC_T2_MT_5", TRUE, FALSE)
//						ELSE
//							DRAW_TEXT_WITH_GXT_STRING(Placement.TextPlacement[iCount], TS_TEXT,"HUD_LOCKED", "FMMC_T2_MT_5", FONT_CENTRE)
//						ENDIF
//						SET_TEXT_WHITE(TS_TEXT)
//						DRAW_2D_SPRITE("MPEntry", "MP_ModeSelected_Gradient", Placement.SpritePlacement[iCount], FALSE)
//					ELSE
//						IF GET_MP_BOOL_PLAYER_STAT(MPPLY_DONE_RACE_CREATOR_TUT) 
//							DRAW_TEXT_WITH_ALIGNMENT(Placement.TextPlacement[iCount], TS_TEXT, "FMMC_T2_MT_5", TRUE, FALSE)
//							DRAW_2D_SPRITE("MPEntry", "MP_ModeNotSelected_Gradient", Placement.SpritePlacement[iCount], FALSE)
//						ELSE
//							SET_TEXT_GREY(TS_TEXT)
//							DRAW_TEXT_WITH_GXT_STRING(Placement.TextPlacement[iCount], TS_TEXT,"HUD_LOCKED", "FMMC_T2_MT_5", FONT_CENTRE)
//							SET_TEXT_WHITE(TS_TEXT)
//							DRAW_2D_SPRITE("MPEntry", "MP_ModeNotSelected_Gradient", Placement.SpritePlacement[iCount], FALSE)
//						ENDIF
//					ENDIF
//				
//				BREAK
//				
//				
//				
//				CASE CREATOR_RACE_MENU_CHOICE_RETURN_TO_MENU
//					IF g_iMenuSelection[HUDMENU_MISSION_CREATOR_RACE] = iCount
//						SET_TEXT_BLACK(TS_TEXT)
//						DRAW_TEXT_WITH_ALIGNMENT(Placement.TextPlacement[iCount], TS_TEXT, "HUD_RETURNMP", TRUE, FALSE)
//						SET_TEXT_WHITE(TS_TEXT)
//						DRAW_2D_SPRITE("MPEntry", "MP_ModeSelected_Gradient", Placement.SpritePlacement[iCount], FALSE)
//					ELSE
//						DRAW_TEXT_WITH_ALIGNMENT(Placement.TextPlacement[iCount], TS_TEXT, "HUD_RETURNMP", TRUE, FALSE)
//						DRAW_2D_SPRITE("MPEntry", "MP_ModeNotSelected_Gradient", Placement.SpritePlacement[iCount], FALSE)
//					ENDIF
//				
//				BREAK
//				
//				CASE CREATOR_RACE_MENU_CHOICE_TUTORIAL
//					IF g_iMenuSelection[HUDMENU_MISSION_CREATOR_RACE] = iCount
//						SET_TEXT_BLACK(TS_TEXT)
//						DRAW_TEXT_WITH_ALIGNMENT(Placement.TextPlacement[iCount], TS_TEXT, "HUD_TUTOR", TRUE, FALSE)
//						SET_TEXT_WHITE(TS_TEXT)
//						DRAW_2D_SPRITE("MPEntry", "MP_ModeSelected_Gradient", Placement.SpritePlacement[iCount], FALSE)
//					ELSE
//						DRAW_TEXT_WITH_ALIGNMENT(Placement.TextPlacement[iCount], TS_TEXT, "HUD_TUTOR", TRUE, FALSE)
//						DRAW_2D_SPRITE("MPEntry", "MP_ModeNotSelected_Gradient", Placement.SpritePlacement[iCount], FALSE)
//					ENDIF
//				
//				BREAK
//				
//			
//			ENDSWITCH
//			iCount++
//		ENDIF
//	ENDREPEAT
//	
//
//
//	
//	SPRITE_PLACEMENT ScaleformSprite
//	
//
////	SCALEFORM_TABS_INPUT_DATA TabsData
////	TabsData.MainTitle = "HUD_MAINTIT"
////	ScaleformSprite = GET_SCALEFORM_TABS_POSITION()
////	INT PotentialPed = GET_JOINING_CHARACTER()
////	COMPILE_SCALEFORM_TABS(Placement.IdlePed[PotentialPed], TabsData, Placement.ScaleformTabStruct )
////	RUN_SCALEFORM_TABS(Placement.ButtonMovie[GET_SCALEFORM_INDEX_FOR_HUD(HUD_SCALEFORM_TABS)], ScaleformSprite, Placement.ScaleformTabStruct)
//	
//
//	ScaleformSprite = GET_SCALEFORM_INSTRUCTIONAL_BUTTON_POSITION()
//
//	
//	ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT), "HUD_INPUT2", Placement.ScaleformStruct)
//	ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL), "HUD_INPUT3", Placement.ScaleformStruct)
//	ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_GROUP_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUTGROUP_FRONTEND_DPAD_UD), "HUD_INPUT1", Placement.ScaleformStruct)
//	
//	
//	RUN_SCALEFORM_INSTRUCTIONAL_BUTTONS(Placement.ButtonMovie[GET_SCALEFORM_INDEX_FOR_HUD(HUD_SCALEFORM_INSTRUCTIONAL)], ScaleformSprite, Placement.ScaleformStruct)
//
//
//	
//ENDPROC
//
//PROC LOGIC_MPHUD_RACE_CREATOR_PROMPT()
//
//
//	IF HAS_HUD_CONTROL_BEEN_PRESSED(INPUT_FRONTEND_ACCEPT)
//		
//		SET_CURRENT_TRANSITION_RACE_CREATOR_MENU_CHOICE(RACECREATORMenuItems[g_iMenuSelection[HUDMENU_MISSION_CREATOR_RACE]])
//		SWITCH RACECREATORMenuItems[g_iMenuSelection[HUDMENU_MISSION_CREATOR_RACE]]		
//			
//			CASE CREATOR_RACE_MENU_CHOICE_CREATE_LAND_RACE	
//				IF GET_MP_BOOL_PLAYER_STAT(MPPLY_DONE_RACE_CREATOR_TUT) 
//					NET_NL()NET_PRINT("LOGIC_MPHUD_RACE_CREATOR_PROMPT: CREATOR_RACE_MENU_CHOICE_CREATE_LAND_RACE SET ")
//					SET_SINGLEPLAYER_END_NOW()
//					SET_CURRENT_GAMEMODE(GAMEMODE_CREATOR)
//					TRANSITION_CHANGE_STATE(TRANSITION_STATE_START_CREATOR_LAUNCH_SCRIPT)
//					HUD_CHANGE_STATE(HUD_STATE_NO_DISPLAY)
//					SET_RACE_TYPE(0)
//				ENDIF
//			BREAK
//			CASE CREATOR_RACE_MENU_CHOICE_CREATE_LAND_POINT2POINT
//				IF GET_MP_BOOL_PLAYER_STAT(MPPLY_DONE_RACE_CREATOR_TUT) 
//					NET_NL()NET_PRINT("LOGIC_MPHUD_RACE_CREATOR_PROMPT: CREATOR_RACE_MENU_CHOICE_CREATE_LAND_POINT2POINT SET ")
//					SET_SINGLEPLAYER_END_NOW()
//					SET_CURRENT_GAMEMODE(GAMEMODE_CREATOR)
//					TRANSITION_CHANGE_STATE(TRANSITION_STATE_START_CREATOR_LAUNCH_SCRIPT)
//					HUD_CHANGE_STATE(HUD_STATE_NO_DISPLAY)
//					g_FMMC_STRUCT.iRaceType  = 1
//				ENDIF
//			BREAK
//			CASE CREATOR_RACE_MENU_CHOICE_CREATE_SEA_RACE
//				IF GET_MP_BOOL_PLAYER_STAT(MPPLY_DONE_RACE_CREATOR_TUT) 
//					NET_NL()NET_PRINT("LOGIC_MPHUD_RACE_CREATOR_PROMPT: CREATOR_RACE_MENU_CHOICE_CREATE_SEA_RACE SET ")
//					SET_SINGLEPLAYER_END_NOW()
//					SET_CURRENT_GAMEMODE(GAMEMODE_CREATOR)
//					TRANSITION_CHANGE_STATE(TRANSITION_STATE_START_CREATOR_LAUNCH_SCRIPT)
//					HUD_CHANGE_STATE(HUD_STATE_NO_DISPLAY)
//					g_FMMC_STRUCT.iRaceType  = 2
//				ENDIF
//			BREAK
//			
//			CASE CREATOR_RACE_MENU_CHOICE_CREATE_SEA_POINT2POINT
//				IF GET_MP_BOOL_PLAYER_STAT(MPPLY_DONE_RACE_CREATOR_TUT) 
//					NET_NL()NET_PRINT("LOGIC_MPHUD_RACE_CREATOR_PROMPT: CREATOR_RACE_MENU_CHOICE_CREATE_SEA_POINT2POINT SET ")
//					SET_SINGLEPLAYER_END_NOW()
//					SET_CURRENT_GAMEMODE(GAMEMODE_CREATOR)
//					TRANSITION_CHANGE_STATE(TRANSITION_STATE_START_CREATOR_LAUNCH_SCRIPT)
//					HUD_CHANGE_STATE(HUD_STATE_NO_DISPLAY)
//					g_FMMC_STRUCT.iRaceType  = 3
//				ENDIF
//			BREAK
//			
//			CASE CREATOR_RACE_MENU_CHOICE_CREATE_AIR_RACE
//				IF GET_MP_BOOL_PLAYER_STAT(MPPLY_DONE_RACE_CREATOR_TUT) 
//					NET_NL()NET_PRINT("LOGIC_MPHUD_RACE_CREATOR_PROMPT: CREATOR_RACE_MENU_CHOICE_CREATE_AIR_RACE SET ")
//					SET_SINGLEPLAYER_END_NOW()
//					SET_CURRENT_GAMEMODE(GAMEMODE_CREATOR)
//					TRANSITION_CHANGE_STATE(TRANSITION_STATE_START_CREATOR_LAUNCH_SCRIPT)
//					HUD_CHANGE_STATE(HUD_STATE_NO_DISPLAY)
//					g_FMMC_STRUCT.iRaceType  = 4
//				ENDIF
//			BREAK
//			
//			CASE CREATOR_RACE_MENU_CHOICE_CREATE_AIR_POINT2POINT
//				IF GET_MP_BOOL_PLAYER_STAT(MPPLY_DONE_RACE_CREATOR_TUT) 
//					NET_NL()NET_PRINT("LOGIC_MPHUD_RACE_CREATOR_PROMPT: CREATOR_RACE_MENU_CHOICE_CREATE_AIR_POINT2POINT SET ")
//					SET_SINGLEPLAYER_END_NOW()
//					SET_CURRENT_GAMEMODE(GAMEMODE_CREATOR)
//					TRANSITION_CHANGE_STATE(TRANSITION_STATE_START_CREATOR_LAUNCH_SCRIPT)
//					HUD_CHANGE_STATE(HUD_STATE_NO_DISPLAY)
//					g_FMMC_STRUCT.iRaceType  = 5
//				ENDIF
//			BREAK
//			
//
//			CASE CREATOR_RACE_MENU_CHOICE_TUTORIAL
//				NET_NL()NET_PRINT("LOGIC_MPHUD_RACE_CREATOR_PROMPT: CREATOR_RACE_MENU_CHOICE_TUTORIAL SET ")
//				SET_SINGLEPLAYER_END_NOW()
//				SET_CURRENT_GAMEMODE(GAMEMODE_CREATOR)
//				TRANSITION_CHANGE_STATE(TRANSITION_STATE_START_CREATOR_LAUNCH_SCRIPT)
//				HUD_CHANGE_STATE(HUD_STATE_NO_DISPLAY)
//				g_FMMC_STRUCT.iRaceType  = 0
//				g_bFMMC_TutorialSelectedFromMpSkyMenu = TRUE
//				
//				
//			BREAK
//			
//			
//			CASE CREATOR_RACE_MENU_CHOICE_RETURN_TO_MENU
//				HUD_CHANGE_STATE(HUD_STATE_MODE_SELECTION)	
//			BREAK
//			
//	
//		
//		ENDSWITCH
//		
//			
//		
//	
//	ENDIF
//
//	IF HAS_HUD_CONTROL_BEEN_PRESSED(INPUT_FRONTEND_CANCEL)
//		
//		HUD_CHANGE_STATE(HUD_STATE_CREATOR_LOBBY_PROMPT)
//	ENDIF
//
//	IF HAS_HUD_CONTROL_BEEN_PRESSED(INPUT_FRONTEND_UP)
//		g_iMenuSelection[HUDMENU_MISSION_CREATOR_RACE] += -1	
//		IF (g_iMenuSelection[HUDMENU_MISSION_CREATOR_RACE] < 0)
//			g_iMenuSelection[HUDMENU_MISSION_CREATOR_RACE] = MAX_NUMBER_RACE_RACE_CREATOR_PROMPT_SELECTIONS-1
//		ENDIF
//	ENDIF
//	IF HAS_HUD_CONTROL_BEEN_PRESSED(INPUT_FRONTEND_DOWN)
//		g_iMenuSelection[HUDMENU_MISSION_CREATOR_RACE] += 1
//		IF (g_iMenuSelection[HUDMENU_MISSION_CREATOR_RACE] >= MAX_NUMBER_RACE_RACE_CREATOR_PROMPT_SELECTIONS)
//			g_iMenuSelection[HUDMENU_MISSION_CREATOR_RACE] = 0
//		ENDIF
//	ENDIF
//	
//	
//	
//ENDPROC
//
//PROC PROCESS_MPHUD_RACE_CREATOR_PROMPT(MPHUD_PLACEMENT_TOOLS& Placement)
//
//	// initialise data
//	IF NOT (Placement.bHudScreenInitialised)
//
//		INIT_MPHUD_RACE_CREATOR_PROMPT(Placement)	
//
//		SET_ALL_MENUPED_VISIBLE(Placement.SelectionPed,Placement.IdlePed, FALSE)
//		Placement.ScreenPlace.iSelection = g_iMenuSelection[HUDMENU_MISSION_CREATOR_RACE]
//		Placement.bHudScreenInitialised = TRUE
//
//		NET_NL()NET_PRINT("PROCESS_MPHUD_RACE_CREATOR_PROMPT: Placement.ScreenPlace.iSelectedCharacter = ")NET_PRINT_INT(Placement.ScreenPlace.iSelectedCharacter)
//
//	ENDIF
//	
//	LOGIC_MPHUD_RACE_CREATOR_PROMPT()
//	
//	IF HAS_ALL_COMMON_HUD_ELEMENTS_LOADED(Placement)	
//		RENDER_MPHUD_RACE_CREATOR_PROMPT(Placement)
//	ENDIF
//	
//	
//ENDPROC
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//PROC RENDER_XML_MPHUD_RACE_CREATOR_PROMPT(MPHUD_PLACEMENT_TOOLS& Placement)
//	
//
//	INT i 
//	
//	SETUP_RACE_CREATOR_MENU_ITEMS()
//
//
//	REPEAT NO_OF_RACE_CREATOR_MENU_OPTIONS() i
//		IF NOT (RACECREATORMenuItems[i] = CREATOR_RACE_MENU_CHOICE_EMPTY)
//			SWITCH RACECREATORMenuItems[i]
//			
//				CASE CREATOR_RACE_MENU_CHOICE_CREATE_LAND_RACE
//					
//					IF GET_MP_BOOL_PLAYER_STAT(MPPLY_DONE_RACE_CREATOR_TUT) 
//						PRIVATE_ADD_SCALEFORMXML_TEXT_SELECTION(i, "FMMC_T2_MT_0",  Placement.ScaleformXMLTextSelectStruct) //General Freemode
//					ELSE
//						PRIVATE_ADD_SCALEFORMXML_TEXT_SELECTION_LOCKED(i, "FMMC_T2_MT_0",  Placement.ScaleformXMLTextSelectStruct)
//					ENDIF
//				
//				BREAK
//				
//				CASE CREATOR_RACE_MENU_CHOICE_CREATE_LAND_POINT2POINT
//
//					IF GET_MP_BOOL_PLAYER_STAT(MPPLY_DONE_RACE_CREATOR_TUT) 
//						PRIVATE_ADD_SCALEFORMXML_TEXT_SELECTION(i, "FMMC_T2_MT_1",  Placement.ScaleformXMLTextSelectStruct) //General Freemode
//					ELSE
//						PRIVATE_ADD_SCALEFORMXML_TEXT_SELECTION_LOCKED(i, "FMMC_T2_MT_1",  Placement.ScaleformXMLTextSelectStruct)
//					ENDIF
//				
//				BREAK
//				
//				CASE CREATOR_RACE_MENU_CHOICE_CREATE_SEA_RACE
//					
//					IF GET_MP_BOOL_PLAYER_STAT(MPPLY_DONE_RACE_CREATOR_TUT) 
//						PRIVATE_ADD_SCALEFORMXML_TEXT_SELECTION(i, "FMMC_T2_MT_2",  Placement.ScaleformXMLTextSelectStruct) //General Freemode
//					ELSE
//						PRIVATE_ADD_SCALEFORMXML_TEXT_SELECTION_LOCKED(i, "FMMC_T2_MT_2",  Placement.ScaleformXMLTextSelectStruct)
//					ENDIF
//				
//				BREAK
//				
//				CASE CREATOR_RACE_MENU_CHOICE_CREATE_SEA_POINT2POINT
//				
//					IF GET_MP_BOOL_PLAYER_STAT(MPPLY_DONE_RACE_CREATOR_TUT) 
//						PRIVATE_ADD_SCALEFORMXML_TEXT_SELECTION(i, "FMMC_T2_MT_3",  Placement.ScaleformXMLTextSelectStruct) //General Freemode
//					ELSE
//						PRIVATE_ADD_SCALEFORMXML_TEXT_SELECTION_LOCKED(i, "FMMC_T2_MT_3",  Placement.ScaleformXMLTextSelectStruct)
//					ENDIF
//				
//				BREAK
//				
//				CASE CREATOR_RACE_MENU_CHOICE_CREATE_AIR_RACE
//				
//					IF GET_MP_BOOL_PLAYER_STAT(MPPLY_DONE_RACE_CREATOR_TUT) 
//						PRIVATE_ADD_SCALEFORMXML_TEXT_SELECTION(i, "FMMC_T2_MT_4",  Placement.ScaleformXMLTextSelectStruct) //General Freemode
//					ELSE
//						PRIVATE_ADD_SCALEFORMXML_TEXT_SELECTION_LOCKED(i, "FMMC_T2_MT_4",  Placement.ScaleformXMLTextSelectStruct)
//					ENDIF
//				
//				
//				BREAK
//				
//				CASE CREATOR_RACE_MENU_CHOICE_CREATE_AIR_POINT2POINT
//				
//					IF GET_MP_BOOL_PLAYER_STAT(MPPLY_DONE_RACE_CREATOR_TUT) 
//						PRIVATE_ADD_SCALEFORMXML_TEXT_SELECTION(i, "FMMC_T2_MT_5",  Placement.ScaleformXMLTextSelectStruct) //General Freemode
//					ELSE
//						PRIVATE_ADD_SCALEFORMXML_TEXT_SELECTION_LOCKED(i, "FMMC_T2_MT_5",  Placement.ScaleformXMLTextSelectStruct)
//					ENDIF
//				
//				
//				BREAK
//				
//				CASE CREATOR_RACE_MENU_CHOICE_RETURN_TO_MENU
//				
//					PRIVATE_ADD_SCALEFORMXML_TEXT_SELECTION(i, "HUD_RETURNMP",  Placement.ScaleformXMLTextSelectStruct) //General Freemode
//				
//				
//				BREAK
//				
//				CASE CREATOR_RACE_MENU_CHOICE_TUTORIAL
//				
//					PRIVATE_ADD_SCALEFORMXML_TEXT_SELECTION(i, "HUD_TUTOR",  Placement.ScaleformXMLTextSelectStruct) //General Freemode
//				
//				
//				BREAK
//				
//				
//				
//			
//			ENDSWITCH
//
//		ENDIF
//	ENDREPEAT
//
//	
//	RUN_SCALEFORMXML_TEXT_SELECTIONS(Placement.ScaleformXMLTextSelectStruct, SHOULD_REFRESH_SCALEFORMXML_TEXT_SELECTION(Placement.ScaleformXMLTextSelectStruct))
//	
//ENDPROC
//
//
//
//
//
//PROC LOGIC_XML_MPHUD_RACE_CREATOR_PROMPT(MPHUD_PLACEMENT_TOOLS& Placement)		
//
//
//		CONST_INT CANCEL 0
//	CONST_INT ACCEPT 1
//
//
//
//	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_CANCEL)
//		SET_BIT(Placement.ButtonReleasedBitset, CANCEL)
//	ENDIF
//	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
//		SET_BIT(Placement.ButtonReleasedBitset, ACCEPT)
//	ENDIF
//
//
//
//	IF (IS_BIT_SET(Placement.ButtonReleasedBitset, ACCEPT)
//	AND IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT) )
//
//		CLEAR_BIT(Placement.ButtonReleasedBitset, ACCEPT)
//		SET_CURRENT_TRANSITION_CREATOR_MENU_CHOICE(CREATORMenuItems[g_iMenuSelection[HUDMENU_MISSION_CREATOR]])
//		SWITCH RACECREATORMenuItems[g_iMenuSelection[HUDMENU_MISSION_CREATOR_RACE]]		
//			
//			CASE CREATOR_RACE_MENU_CHOICE_CREATE_LAND_RACE	
//				IF GET_MP_BOOL_PLAYER_STAT(MPPLY_DONE_RACE_CREATOR_TUT) 
//					NET_NL()NET_PRINT("LOGIC_MPHUD_RACE_CREATOR_PROMPT: CREATOR_RACE_MENU_CHOICE_CREATE_LAND_RACE SET ")
//					SET_SINGLEPLAYER_END_NOW()
//					SET_CURRENT_GAMEMODE(GAMEMODE_CREATOR)
//					TRANSITION_CHANGE_STATE(TRANSITION_STATE_START_CREATOR_LAUNCH_SCRIPT)
//					HUD_CHANGE_STATE(HUD_STATE_NO_DISPLAY)
//					SET_RACE_TYPE(0)
//				ENDIF
//			BREAK
//			CASE CREATOR_RACE_MENU_CHOICE_CREATE_LAND_POINT2POINT
//				IF GET_MP_BOOL_PLAYER_STAT(MPPLY_DONE_RACE_CREATOR_TUT) 
//					NET_NL()NET_PRINT("LOGIC_MPHUD_RACE_CREATOR_PROMPT: CREATOR_RACE_MENU_CHOICE_CREATE_LAND_POINT2POINT SET ")
//					SET_SINGLEPLAYER_END_NOW()
//					SET_CURRENT_GAMEMODE(GAMEMODE_CREATOR)
//					TRANSITION_CHANGE_STATE(TRANSITION_STATE_START_CREATOR_LAUNCH_SCRIPT)
//					HUD_CHANGE_STATE(HUD_STATE_NO_DISPLAY)
//					g_FMMC_STRUCT.iRaceType  = 1
//				ENDIF
//			BREAK
//			CASE CREATOR_RACE_MENU_CHOICE_CREATE_SEA_RACE
//				IF GET_MP_BOOL_PLAYER_STAT(MPPLY_DONE_RACE_CREATOR_TUT) 
//					NET_NL()NET_PRINT("LOGIC_MPHUD_RACE_CREATOR_PROMPT: CREATOR_RACE_MENU_CHOICE_CREATE_SEA_RACE SET ")
//					SET_SINGLEPLAYER_END_NOW()
//					SET_CURRENT_GAMEMODE(GAMEMODE_CREATOR)
//					TRANSITION_CHANGE_STATE(TRANSITION_STATE_START_CREATOR_LAUNCH_SCRIPT)
//					HUD_CHANGE_STATE(HUD_STATE_NO_DISPLAY)
//					g_FMMC_STRUCT.iRaceType  = 2
//				ENDIF
//			BREAK
//			
//			CASE CREATOR_RACE_MENU_CHOICE_CREATE_SEA_POINT2POINT
//				IF GET_MP_BOOL_PLAYER_STAT(MPPLY_DONE_RACE_CREATOR_TUT) 
//					NET_NL()NET_PRINT("LOGIC_MPHUD_RACE_CREATOR_PROMPT: CREATOR_RACE_MENU_CHOICE_CREATE_SEA_POINT2POINT SET ")
//					SET_SINGLEPLAYER_END_NOW()
//					SET_CURRENT_GAMEMODE(GAMEMODE_CREATOR)
//					TRANSITION_CHANGE_STATE(TRANSITION_STATE_START_CREATOR_LAUNCH_SCRIPT)
//					HUD_CHANGE_STATE(HUD_STATE_NO_DISPLAY)
//					g_FMMC_STRUCT.iRaceType  = 3
//				ENDIF
//			BREAK
//			
//			CASE CREATOR_RACE_MENU_CHOICE_CREATE_AIR_RACE
//				IF GET_MP_BOOL_PLAYER_STAT(MPPLY_DONE_RACE_CREATOR_TUT) 
//					NET_NL()NET_PRINT("LOGIC_MPHUD_RACE_CREATOR_PROMPT: CREATOR_RACE_MENU_CHOICE_CREATE_AIR_RACE SET ")
//					SET_SINGLEPLAYER_END_NOW()
//					SET_CURRENT_GAMEMODE(GAMEMODE_CREATOR)
//					TRANSITION_CHANGE_STATE(TRANSITION_STATE_START_CREATOR_LAUNCH_SCRIPT)
//					HUD_CHANGE_STATE(HUD_STATE_NO_DISPLAY)
//					g_FMMC_STRUCT.iRaceType  = 4
//				ENDIF
//			BREAK
//			
//			CASE CREATOR_RACE_MENU_CHOICE_CREATE_AIR_POINT2POINT
//				IF GET_MP_BOOL_PLAYER_STAT(MPPLY_DONE_RACE_CREATOR_TUT) 
//					NET_NL()NET_PRINT("LOGIC_MPHUD_RACE_CREATOR_PROMPT: CREATOR_RACE_MENU_CHOICE_CREATE_AIR_POINT2POINT SET ")
//					SET_SINGLEPLAYER_END_NOW()
//					SET_CURRENT_GAMEMODE(GAMEMODE_CREATOR)
//					TRANSITION_CHANGE_STATE(TRANSITION_STATE_START_CREATOR_LAUNCH_SCRIPT)
//					HUD_CHANGE_STATE(HUD_STATE_NO_DISPLAY)
//					g_FMMC_STRUCT.iRaceType  = 5
//				ENDIF
//			BREAK
//			
//
//			CASE CREATOR_RACE_MENU_CHOICE_TUTORIAL
//				NET_NL()NET_PRINT("LOGIC_MPHUD_RACE_CREATOR_PROMPT: CREATOR_RACE_MENU_CHOICE_TUTORIAL SET ")
//				SET_SINGLEPLAYER_END_NOW()
//				SET_CURRENT_GAMEMODE(GAMEMODE_CREATOR)
//				TRANSITION_CHANGE_STATE(TRANSITION_STATE_START_CREATOR_LAUNCH_SCRIPT)
//				HUD_CHANGE_STATE(HUD_STATE_NO_DISPLAY)
//				g_FMMC_STRUCT.iRaceType  = 0
//				g_bFMMC_TutorialSelectedFromMpSkyMenu = TRUE
//				
//				
//			BREAK
//			
//			
//			CASE CREATOR_RACE_MENU_CHOICE_RETURN_TO_MENU
//				HUD_CHANGE_STATE(HUD_STATE_MODE_SELECTION)	
//			BREAK
//			
//			
//	
//		
//		ENDSWITCH
//
//	ENDIF
//	
//	
//	IF IS_BIT_SET(Placement.ButtonReleasedBitset, CANCEL)
//		IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL) 
//			CLEAR_BIT(Placement.ButtonReleasedBitset, CANCEL)
//			HUD_CHANGE_STATE(HUD_STATE_CREATOR_LOBBY_PROMPT)
//		ENDIF
//
//	ENDIF
//
//ENDPROC
//
//
//PROC PROCESS_XML_MPHUD_RACE_CREATOR_PROMPT(MPHUD_PLACEMENT_TOOLS& Placement)
//	
//	isrockstarDev = IS_ROCKSTAR_DEV()
//	#IF IS_DEBUG_BUILD
//		IF g_bIsRockstarDev = TRUE
//			isrockstarDev = TRUE
//		ENDIF
//	#ENDIF
//	
//	IF IS_PAUSE_MENU_ACTIVE()
//	OR g_TurnOnNewTransitionSecondarySystem
//		// initialise data
//		IF NOT (Placement.bHudScreenInitialised)
//			Placement.bDoIHaveControl = TRUE
//			IF IS_PAUSE_MENU_ACTIVE()
//				IF IS_THIS_SCREEN_ACTIVE(FE_MENU_VERSION_TEXT_SELECTION) = FALSE
//					Placement.bDoIHaveControl = FALSE
//					RESTART_FRONTEND_MENU(FE_MENU_VERSION_TEXT_SELECTION)
//					NET_NL()NET_PRINT("PROCESS_XML_MPHUD_RACE_CREATOR_PROMPT: ACTIVATE_FRONTEND_MENU")
//				ENDIF
//
//			ELSE
//				Placement.bDoIHaveControl = FALSE
//				ACTIVATE_FRONTEND_MENU(FE_MENU_VERSION_TEXT_SELECTION, FALSE)
//				NET_NL()NET_PRINT("PROCESS_XML_MPHUD_RACE_CREATOR_PROMPT: ACTIVATE_FRONTEND_MENU")
//			ENDIF
//			SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_FADE)
//			
//			IF isrockstarDev
//				MAX_NUMBER_CREATOR_PROMPT_SELECTIONS = 5
//			ELSE
//				MAX_NUMBER_CREATOR_PROMPT_SELECTIONS = 4
//			ENDIF
//
//			SET_ALL_MENUPED_VISIBLE(Placement.SelectionPed,Placement.IdlePed, FALSE)
//			Placement.ScreenPlace.iSelection = g_iMenuSelection[HUDMENU_MISSION_CREATOR_RACE]
//			Placement.bHudScreenInitialised = TRUE
//
//			NET_NL()NET_PRINT("PROCESS_XML_MPHUD_RACE_CREATOR_PROMPT: Placement.ScreenPlace.iSelectedCharacter = ")NET_PRINT_INT(Placement.ScreenPlace.iSelectedCharacter)
//
//		ENDIF
//		
//			
//		IF (Placement.bHudScreenInitialised)
//		AND IS_PAUSE_MENU_ACTIVE()
//		AND NOT IS_PAUSE_MENU_RESTARTING()
//			IF IS_FRONTEND_READY_FOR_CONTROL()
//			
//				IF HAS_MENU_TRIGGER_EVENT_OCCURRED()
//					NET_NL()NET_PRINT("HAS_MENU_TRIGGER_EVENT_OCCURRED = TRUE")
//					GET_MENU_TRIGGER_EVENT_DETAILS(Placement.aNextScreen, Placement.iMenu)
//					NET_NL()NET_PRINT("		- aNextScreen = ")NET_PRINT(GET_XML_SCREEN_STRING_VALUE(Placement.aNextScreen))
//					NET_NL()NET_PRINT("		- iMenu = ")NET_PRINT_INT(Placement.iMenu)
//				ENDIF
//				
//				IF HAS_MENU_LAYOUT_CHANGED_EVENT_OCCURRED()
//	 
//					NET_NL()NET_PRINT("HAS_MENU_LAYOUT_CHANGED_EVENT_OCCURRED = TRUE")
//					GET_MENU_LAYOUT_CHANGED_EVENT_DETAILS(Placement.CurrentScreen, Placement.aNextScreen, Placement.iMenu)
//					NET_NL()NET_PRINT("		- CurrentScreen = ")NET_PRINT(GET_XML_SCREEN_STRING_VALUE(Placement.CurrentScreen))
//					NET_NL()NET_PRINT("		- aNextScreen = ")NET_PRINT(GET_XML_SCREEN_STRING_VALUE(Placement.aNextScreen))
//					NET_NL()NET_PRINT("		- iMenu = ")NET_PRINT_INT(Placement.iMenu)
//						
//					MPLOBBY_SET_TITLE("HUD_MAINTIT", Placement.bSetupTitle)
//					STRING PlayerName = GET_PLAYER_NAME(PLAYER_ID())
//					TEXT_LABEL_63 CrewName = GET_LOCAL_PLAYER_CREW_NAME()
//					MPLOBBY_SET_HEADING_DETAILS(PlayerName, CrewName, Placement.bSetupCrewAndName)
//						
//					Placement.bDoIHaveControl = TRUE
//					
//					g_iMenuSelection[HUDMENU_MISSION_CREATOR] = Placement.iMenu
//
//				ENDIF
//		
//				IF Placement.bDoIHaveControl = TRUE	
//					RENDER_XML_MPHUD_RACE_CREATOR_PROMPT(Placement)
//					LOGIC_XML_MPHUD_RACE_CREATOR_PROMPT(Placement)	
//				ENDIF
//				
//			ELSE
//				NET_NL()NET_PRINT("IS_FRONTEND_READY_FOR_CONTROL = FALSE ")
//			ENDIF
//
//			
//		ENDIF
//		
//
//	ELSE
//		NET_PRINT("PROCESS_XML_MPHUD_RACE_CREATOR_PROMPT: IS_PAUSE_MENU_ACTIVE = FALSE") NET_NL()
//	ENDIF
//
//ENDPROC
//
//
//
//
