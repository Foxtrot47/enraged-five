/* ------------------------------------------------------------------
* Name: 		MPHud_CharacterCreatorMenu.sch
* Author: 		James Adwick
* Date: 		02/09/2014
* Purpose: 		Maintains the scaleform / menu interaction for the creator
* ------------------------------------------------------------------*/

//--- Includes

USING "MPHud_CharacterAccessors.sch"
USING "MPHud_CharacterGeneration.sch"
USING "mp_scaleform_functions_xml.sch"
USING "menu_cursor_public.sch"

//--- Constants

TWEAK_FLOAT cfCHARACTER_MUM_OFF_X	0.0
TWEAK_FLOAT cfCHARACTER_MUM_OFF_y	0.136
TWEAK_FLOAT cfCHARACTER_MUM_OFF_Z	-0.010
TWEAK_FLOAT cfCHARACTER_DAD_OFF_X	0.0
TWEAK_FLOAT cfCHARACTER_DAD_OFF_Y	0.134
TWEAK_FLOAT cfCHARACTER_DAD_OFF_Z	-0.004

// Put in a photo delay
TWEAK_INT ciCHARACTER_CREATOR_PHOTO_DELAY		4000
TWEAK_INT ciCHARACTER_CREATOR_PHOTO_COMPLETE	500

// Clothing animations
TWEAK_INT ciCHARACTER_CREATOR_CLOTHES_TIMER_START			5000
TWEAK_INT ciCHARACTER_CREATOR_CLOTHES_TIMER_END				8000
TWEAK_INT ciCHARACTER_CREATOR_CLOTHES_RANDOM_CHANCE			4

TWEAK_FLOAT cfCHARACTER_CREATOR_PHOTO_BORDER_ROTATION			-0.700
TWEAK_FLOAT cfCHARACTER_CREATOR_PHOTO_X							0.5
TWEAK_FLOAT cfCHARACTER_CREATOR_PHOTO_Y							0.5
TWEAK_INT	ciCHARACTER_CREATOR_PHOTO_X_SCALE					162
TWEAK_INT	ciCHARACTER_CREATOR_PHOTO_Y_SCALE					120

TWEAK_INT	ciCHARACTER_CREATOR_PHOTO_SHUTTER_SPEED				250

//--- Debug Functions

#IF IS_DEBUG_BUILD

/// PURPOSE:
///    Debug output our information for heads and their blends
PROC CHARACTER_CREATOR_DEBUG_PRINT_INIT_INFO()
	CPRINTLN(DEBUG_NET_CHARACTER, "----------------------------------- CHARACTER_CREATOR_DEBUG_PRINT_INIT_INFO ------------------------")
	CPRINTLN(DEBUG_NET_CHARACTER, "CHARACTER_CREATOR_DEBUG_PRINT_INIT_INFO GET_PED_HEAD_BLEND_FIRST_INDEX(HBHT_FEMALE)        = ", GET_PED_HEAD_BLEND_FIRST_INDEX(HBHT_FEMALE))
	CPRINTLN(DEBUG_NET_CHARACTER, "CHARACTER_CREATOR_DEBUG_PRINT_INIT_INFO GET_PED_HEAD_BLEND_FIRST_INDEX(HBHT_MALE)          = ", GET_PED_HEAD_BLEND_FIRST_INDEX(HBHT_MALE))
	CPRINTLN(DEBUG_NET_CHARACTER, "CHARACTER_CREATOR_DEBUG_PRINT_INIT_INFO GET_PED_HEAD_BLEND_FIRST_INDEX(HBHT_UNIQUE_FEMALE) = ", GET_PED_HEAD_BLEND_FIRST_INDEX(HBHT_UNIQUE_FEMALE))
	CPRINTLN(DEBUG_NET_CHARACTER, "CHARACTER_CREATOR_DEBUG_PRINT_INIT_INFO GET_PED_HEAD_BLEND_FIRST_INDEX(HBHT_UNIQUE_MALE)   = ", GET_PED_HEAD_BLEND_FIRST_INDEX(HBHT_UNIQUE_MALE))
	CPRINTLN(DEBUG_NET_CHARACTER, "CHARACTER_CREATOR_DEBUG_PRINT_INIT_INFO GET_PED_HEAD_BLEND_NUM_HEADS(HBHT_FEMALE)          = ", GET_PED_HEAD_BLEND_NUM_HEADS(HBHT_FEMALE))
	CPRINTLN(DEBUG_NET_CHARACTER, "CHARACTER_CREATOR_DEBUG_PRINT_INIT_INFO GET_PED_HEAD_BLEND_NUM_HEADS(HBHT_MALE)            = ", GET_PED_HEAD_BLEND_NUM_HEADS(HBHT_MALE))
	CPRINTLN(DEBUG_NET_CHARACTER, "CHARACTER_CREATOR_DEBUG_PRINT_INIT_INFO GET_PED_HEAD_BLEND_NUM_HEADS(HBHT_UNIQUE_FEMALE)   = ", GET_PED_HEAD_BLEND_NUM_HEADS(HBHT_UNIQUE_FEMALE))
	CPRINTLN(DEBUG_NET_CHARACTER, "CHARACTER_CREATOR_DEBUG_PRINT_INIT_INFO GET_PED_HEAD_BLEND_NUM_HEADS(HBHT_UNIQUE_MALE)     = ", GET_PED_HEAD_BLEND_NUM_HEADS(HBHT_UNIQUE_MALE))
	CPRINTLN(DEBUG_NET_CHARACTER, "CHARACTER_CREATOR_DEBUG_PRINT_INIT_INFO GET_NUM_PED_HAIR_TINTS()     					  = ", GET_NUM_PED_HAIR_TINTS())
	CPRINTLN(DEBUG_NET_CHARACTER, "CHARACTER_CREATOR_DEBUG_PRINT_INIT_INFO GET_NUM_PED_MAKEUP_TINTS()     					  = ", GET_NUM_PED_MAKEUP_TINTS())
	CPRINTLN(DEBUG_NET_CHARACTER, "CHARACTER_CREATOR_DEBUG_PRINT_INIT_INFO -----------------------------------------------------------------------------------------------------")
	CPRINTLN(DEBUG_NET_CHARACTER, "CHARACTER_CREATOR_DEBUG_PRINT_INIT_INFO -			- IS_GAME_LINKED_TO_SOCIAL_CLUB() = ", PICK_STRING(IS_GAME_LINKED_TO_SOCIAL_CLUB(), "TRUE", "FALSE"))
	CPRINTLN(DEBUG_NET_CHARACTER, "CHARACTER_CREATOR_DEBUG_PRINT_INIT_INFO -			- ARE_CHARACTER_CREATOR_COLLECTOR_EDITIONS_AVAILABLE() = ", PICK_STRING(ARE_CHARACTER_CREATOR_COLLECTOR_EDITIONS_AVAILABLE(), "TRUE", "FALSE"))
	CPRINTLN(DEBUG_NET_CHARACTER, "CHARACTER_CREATOR_DEBUG_PRINT_INIT_INFO -----------------------------------------------------------------------------------------------------")
	
	
	INT i
	CPRINTLN(DEBUG_NET_CHARACTER, "CHARACTER_CREATOR_DEBUG_PRINT_INIT_INFO - VALID HAIR TINTS")
	REPEAT GET_NUM_PED_HAIR_TINTS() i
		IF IS_PED_HAIR_TINT_FOR_CREATOR(i)
			CPRINTLN(DEBUG_NET_CHARACTER, "CHARACTER_CREATOR_DEBUG_PRINT_INIT_INFO - Hair Tint ", i, " is valid")
		ENDIF
	ENDREPEAT
	CPRINTLN(DEBUG_NET_CHARACTER, "CHARACTER_CREATOR_DEBUG_PRINT_INIT_INFO -----------------------------------------------------------------------------------------------------")
ENDPROC

#ENDIF

//--- Functions

/// PURPOSE: 
///    Populates the title of the character creator menu
PROC SETUP_CHARACTER_CREATOR_TITLES(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, INT iNumber1 = -1, INT iNumber2 = -1)
	
	// Get the correct column index
	INT iColumn = GET_CHARACTER_CONTROLLER_COLUMN_INDEX(GET_CHARACTER_CREATOR_MENU_STATE(characterData))
	
	TEXT_LABEL_15 tlSubTitle = ""
	TEXT_LABEL_15 tlRightText = ""
	
	SWITCH GET_CHARACTER_CREATOR_MENU_STATE(characterData)
		CASE ciCHARACTER_CREATOR_COLUMN_MAIN_MENU
			tlSubTitle = "FACE_MMT"
		BREAK
		
		CASE ciCHARACTER_CREATOR_COLUMN_HERITAGE
			tlSubTitle = "FACE_HERT"
		BREAK
		
		CASE ciCHARACTER_CREATOR_COLUMN_FEATURES
			tlSubTitle = "FACE_FEATT"
			tlRightText = "FACE_FEATPG"
		BREAK
		
		CASE ciCHARACTER_CREATOR_COLUMN_STATS
			tlSubTitle = "FACE_STATST"
		BREAK
		
		CASE ciCHARACTER_CREATOR_COLUMN_APPEARANCE
			tlSubTitle = "FACE_APPT"
			tlRightText = "FACE_FEATPG"
		BREAK
		
		CASE ciCHARACTER_CREATOR_COLUMN_APPAREL
			tlSubTitle = "FACE_APPAT"
		BREAK
		
	ENDSWITCH	

	SET_FRONTEND_CREATOR_TITLE_WITH_3_STRINGS(iColumn, "FACE_TITLE", tlSubTitle, tlRightText, iNumber1, iNumber2)
ENDPROC


/// PURPOSE:
///    Setup the help text displayed in the menu
PROC SET_CHARACTER_CREATOR_MENU_HELP_TEXT(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData)
	INT iColumn = GET_CHARACTER_CONTROLLER_COLUMN_INDEX(GET_CHARACTER_CREATOR_MENU_STATE(characterData))
	
	SWITCH GET_CHARACTER_CREATOR_MENU_STATE(characterData)
	
		CASE ciCHARACTER_CREATOR_COLUMN_HERITAGE
			SWITCH GET_CHARACTER_CREATOR_CURRENT_SELECTION(characterData)
					
				CASE ciCHARACTER_CREATOR_HERITAGE_MUM_NAME
					SET_FRONTEND_HELP_TEXT(iColumn, "CHARC_H_30")
				BREAK
			
				CASE ciCHARACTER_CREATOR_HERITAGE_DAD_NAME
					SET_FRONTEND_HELP_TEXT(iColumn, "CHARC_H_31")
				BREAK
			
				CASE ciCHARACTER_CREATOR_HERITAGE_DOM
					SET_FRONTEND_HELP_TEXT(iColumn, "CHARC_H_9")
				BREAK
				
				CASE ciCHARACTER_CREATOR_HERITAGE_SKIN_TONE
					SET_FRONTEND_HELP_TEXT(iColumn, "FACE_HER_ST_H")
				BREAK			
			ENDSWITCH
		BREAK
		
		CASE ciCHARACTER_CREATOR_COLUMN_APPEARANCE
			SET_FRONTEND_HELP_TEXT(iColumn, "FACE_APP_H")
		BREAK
		
		CASE ciCHARACTER_CREATOR_COLUMN_APPAREL
			SET_FRONTEND_HELP_TEXT(iColumn, "FACE_APPA_H")
		BREAK
		
		CASE ciCHARACTER_CREATOR_COLUMN_FEATURES
			SET_FRONTEND_HELP_TEXT(iColumn, "FACE_FEAT_H")		
		BREAK
		
		CASE ciCHARACTER_CREATOR_COLUMN_STATS
			IF HAS_ENTERED_CHAR_CREATION_FROM_ALTERATION_PROMPT()
				SET_FRONTEND_HELP_TEXT(iColumn, "FACE_STATS_H2")
			ELSE
				// We display specific help text for the stat we have scrolled to
				SWITCH GET_CHARACTER_CREATOR_CURRENT_SELECTION(characterData)
					CASE 0	
						SET_FRONTEND_HELP_TEXT(iColumn, "FACE_H_STA")
					BREAK
					CASE 1
						SET_FRONTEND_HELP_TEXT(iColumn, "FACE_H_SHO")
					BREAK
					CASE 2
						SET_FRONTEND_HELP_TEXT(iColumn, "FACE_H_STR")
					BREAK
					CASE 3
						SET_FRONTEND_HELP_TEXT(iColumn, "FACE_H_STE")
					BREAK
					CASE 4
						SET_FRONTEND_HELP_TEXT(iColumn, "FACE_H_FLY")
					BREAK
					CASE 5
						SET_FRONTEND_HELP_TEXT(iColumn, "FACE_H_DRI")
					BREAK
					CASE 6
						SET_FRONTEND_HELP_TEXT(iColumn, "FACE_H_LCP")
					BREAK
				ENDSWITCH
			ENDIF
		BREAK
		
		CASE ciCHARACTER_CREATOR_COLUMN_MAIN_MENU
			SWITCH GET_CHARACTER_CREATOR_CURRENT_SELECTION(characterData)
				
				CASE ciCHARACTER_CREATOR_MAIN_MENU_GENDER
					SET_FRONTEND_HELP_TEXT(iColumn, "FACE_MM_H2")		// Select the gender of your Character
				BREAK
				
				CASE ciCHARACTER_CREATOR_MAIN_MENU_HERITAGE
					SET_FRONTEND_HELP_TEXT(iColumn, "FACE_MM_H3")		// Select to choose your parents.
				BREAK
				
				CASE ciCHARACTER_CREATOR_MAIN_MENU_FEATURES
					SET_FRONTEND_HELP_TEXT(iColumn, "FACE_MM_H4")		// Select to change the Features of your face.
				BREAK
				
				CASE ciCHARACTER_CREATOR_MAIN_MENU_STATS
					SET_FRONTEND_HELP_TEXT(iColumn, "FACE_MM_H5")		// Select to assign points to your Stats.
				BREAK	
				
				CASE ciCHARACTER_CREATOR_MAIN_MENU_APPEARANCE
					SET_FRONTEND_HELP_TEXT(iColumn, "FACE_MM_H6")		// Select to change your Appearance
				BREAK
				
				CASE ciCHARACTER_CREATOR_MAIN_MENU_APPAREL
					SET_FRONTEND_HELP_TEXT(iColumn, "FACE_MM_H7")		// Select to change your Apparel.
				BREAK
				
				CASE ciCHARACTER_CREATOR_MAIN_MENU_CONTINUE
					SET_FRONTEND_HELP_TEXT(iColumn, "FACE_MM_H8")		// Ready to start playing GTA Online?
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH	
ENDPROC


/// PURPOSE:
///    Populates the main menu of the creator. This switches focus to this column and sets focus there
PROC POPULATE_CHARACTER_CREATOR_MAIN_MENU(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, BOOL bUpdate = FALSE)
	
	IF NOT bUpdate
		SET_CHARACTER_CREATOR_MENU_STATE(characterData, ciCHARACTER_CREATOR_COLUMN_MAIN_MENU)
	
		// Clear our column
		CLEAR_FRONTEND_DATA_SLOT(ciCHARACTER_CREATOR_COLUMN_MAIN_MENU)
	
		// Set up the menu title
		SETUP_CHARACTER_CREATOR_TITLES(characterData)
	ENDIF
	
	INT iIndex
	
	// Gender row
	IF SHOULD_CHARACTER_CREATOR_DISPLAY_GENDER(characterData)
		SET_FRONTEND_CHARACTER_SETTING_SLOT(ciCHARACTER_CREATOR_COLUMN_MAIN_MENU, iIndex, characterData.iCharacterMenuID, ciCHARACTER_CREATOR_MAIN_MENU_GENDER, "FACE_SEX", PICK_STRING(characterData.eActiveChild = CHARACTER_CREATOR_SON, "FACE_MALE", "FACE_FEMALE"), 0, ciCHARACTER_CREATOR_MENU_ROW_TYPE, bUpdate)
		iIndex++
	ENDIF
	
	// Heritage
	SET_FRONTEND_CHARACTER_SETTING_SLOT(ciCHARACTER_CREATOR_COLUMN_MAIN_MENU, iIndex, characterData.iCharacterMenuID, ciCHARACTER_CREATOR_MAIN_MENU_HERITAGE, "FACE_HERI", "", 1, ciCHARACTER_CREATOR_MENU_ROW_TYPE, bUpdate)
	iIndex++
	
	// Features
	SET_FRONTEND_CHARACTER_SETTING_SLOT(ciCHARACTER_CREATOR_COLUMN_MAIN_MENU, iIndex, characterData.iCharacterMenuID, ciCHARACTER_CREATOR_MAIN_MENU_FEATURES, "FACE_FEAT", "", 1, ciCHARACTER_CREATOR_MENU_ROW_TYPE, bUpdate)
	iIndex++
	
	// Appearance
	SET_FRONTEND_CHARACTER_SETTING_SLOT(ciCHARACTER_CREATOR_COLUMN_MAIN_MENU, iIndex, characterData.iCharacterMenuID, ciCHARACTER_CREATOR_MAIN_MENU_APPEARANCE, "FACE_APP", "", 1, ciCHARACTER_CREATOR_MENU_ROW_TYPE, bUpdate)
	iIndex++
	
	// Apparel
	SET_FRONTEND_CHARACTER_SETTING_SLOT(ciCHARACTER_CREATOR_COLUMN_MAIN_MENU, iIndex, characterData.iCharacterMenuID, ciCHARACTER_CREATOR_MAIN_MENU_APPAREL, "FACE_APPA", "", 1, ciCHARACTER_CREATOR_MENU_ROW_TYPE, bUpdate)	
	iIndex++
	
	// Stats
	SET_FRONTEND_CHARACTER_SETTING_SLOT(ciCHARACTER_CREATOR_COLUMN_MAIN_MENU, iIndex, characterData.iCharacterMenuID, ciCHARACTER_CREATOR_MAIN_MENU_STATS, "FACE_STATS", "", 1, ciCHARACTER_CREATOR_MENU_ROW_TYPE, bUpdate)
	iIndex++
	
	IF NOT bUpdate
		// Save & Continue
		SET_FRONTEND_CHARACTER_SETTING_SLOT(ciCHARACTER_CREATOR_COLUMN_MAIN_MENU, iIndex, characterData.iCharacterMenuID, ciCHARACTER_CREATOR_MAIN_MENU_CONTINUE, "FACE_SAVE", "", 2, 1, bUpdate)	// Save & Continue
		iIndex++
		
		// Make sure it displays
		DISPLAY_FRONTEND_DATA_SLOT(ciCHARACTER_CREATOR_COLUMN_MAIN_MENU)
		
		IF GET_CHARACTER_CREATOR_CURRENT_SELECTION(characterData) != ciCHARACTER_CREATOR_MAIN_MENU_APPAREL
			SET_FRONTEND_COLUMN_FOCUS(ciCHARACTER_CREATOR_COLUMN_MAIN_MENU, TRUE)
		ENDIF
								
		// Update our menu state and set the highlight
		IF NOT SHOULD_CHARACTER_CREATOR_DISPLAY_GENDER(characterData)
			SET_FRONTEND_HIGHLIGHT(ciCHARACTER_CREATOR_COLUMN_MAIN_MENU, GET_CHARACTER_CREATOR_CURRENT_SELECTION(characterData)-1, TRUE)
		ELSE
			SET_FRONTEND_HIGHLIGHT(ciCHARACTER_CREATOR_COLUMN_MAIN_MENU, GET_CHARACTER_CREATOR_CURRENT_SELECTION(characterData), TRUE)
		ENDIF
										
		// Lock first column so user doesn't move in and out
		SET_FRONTEND_COLUMN_AS_LOCKED(ciCHARACTER_CREATOR_COLUMN_MAIN_MENU, TRUE)
	ENDIF
	
	// Show the correct column, hiding all others
	DISPLAY_CHARACTER_CREATOR_COLUMN(ciCHARACTER_CREATOR_COLUMN_MAIN_MENU)
		
	// Update our buttons
	UPDATE_CHARACTER_CONTROLLER_BUTTONS(characterData)
ENDPROC

/// PURPOSE:
///    Populate the heritage menu and pushes player into this submenu
PROC POPULATE_CHARACTER_CREATOR_HERITAGE_MENU(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, BOOL bUpdate = FALSE)
	
	// Grab the scaleform column
	INT iColumn = GET_CHARACTER_CONTROLLER_COLUMN_INDEX(ciCHARACTER_CREATOR_COLUMN_HERITAGE)
	
	IF NOT bUpdate
		SET_CHARACTER_CREATOR_MENU_STATE(characterData, ciCHARACTER_CREATOR_COLUMN_HERITAGE)
	
		// Clear all data
		CLEAR_FRONTEND_DATA_SLOT(iColumn)
		
		// Set up the titles
		SETUP_CHARACTER_CREATOR_TITLES(characterData)
	ENDIF
	
	TEXT_LABEL_23 tlTxd = "CHAR_CREATOR_PORTRAITS"
	TEXT_LABEL_23 tlMumTxn = "Female_"
	TEXT_LABEL_23 tlDadTxn = "Male_"
	
	INT iMumIndex 			= GET_CHARACTER_CREATOR_PARENT_HEAD(characterData, ciCHARACTER_CREATOR_MUM_INDEX)
	INT iDadIndex 			= GET_CHARACTER_CREATOR_PARENT_HEAD(characterData, ciCHARACTER_CREATOR_DAD_INDEX)
	INT iMumHeadValue 		= characterData.parentData[ciCHARACTER_CREATOR_MUM_INDEX].sHeadSelection[iMumIndex].iHead
	INT iDadHeadValue 		= characterData.parentData[ciCHARACTER_CREATOR_DAD_INDEX].sHeadSelection[iDadIndex].iHead
	BOOL bMumIsSpecial
	BOOL bDadIsSpecial
	
	iMumHeadValue = ADJUST_CHARACTER_CREATOR_PARENT_HEAD(iMumHeadValue, FALSE, bMumIsSpecial)
	iDadHeadValue = ADJUST_CHARACTER_CREATOR_PARENT_HEAD(iDadHeadValue, TRUE, bDadIsSpecial)

	IF bMumIsSpecial
		tlMumTxn = "Special_Female_"
	ENDIF
	
	tlMumTxn += iMumHeadValue
	
	IF bDadIsSpecial
		tlDadTxn = "Special_Male_"
	ENDIF
	
	tlDadTxn += iDadHeadValue
	
	SET_FRONTEND_HERITAGE_SLOT(iColumn, ciCHARACTER_CREATOR_HERITAGE_MUM_AND_DAD_WINDOW, characterData.iCharacterMenuID, ciCHARACTER_CREATOR_HERITAGE_MUM_AND_DAD_WINDOW, 0, 0, "", DEFAULT, DEFAULT, FALSE, bUpdate, DEFAULT, tlMumTxn, tlDadTxn, tlTxd)
	//SET_FRONTEND_HERITAGE_SLOT(iColumn, ciCHARACTER_CREATOR_HERITAGE_DAD_WINDOW, characterData.iCharacterMenuID, ciCHARACTER_CREATOR_HERITAGE_DAD_WINDOW, 0, 0, "", DEFAULT, DEFAULT, FALSE, bUpdate)
	
	SET_FRONTEND_HERITAGE_SLOT(iColumn, ciCHARACTER_CREATOR_HERITAGE_MUM_NAME, characterData.iCharacterMenuID, ciCHARACTER_CREATOR_HERITAGE_MUM_NAME, 2, 0, "FACE_MUMS", tlMumTxn, TRUE, DEFAULT, bUpdate, GET_CHARACTER_CREATOR_PARENT_HEAD(characterData, ciCHARACTER_CREATOR_MUM_INDEX) + 1)
	SET_FRONTEND_HERITAGE_SLOT(iColumn, ciCHARACTER_CREATOR_HERITAGE_DAD_NAME, characterData.iCharacterMenuID, ciCHARACTER_CREATOR_HERITAGE_DAD_NAME, 2, 0, "FACE_DADS", tlDadTxn, TRUE, DEFAULT, bUpdate, GET_CHARACTER_CREATOR_PARENT_HEAD(characterData, ciCHARACTER_CREATOR_DAD_INDEX) + 1)

	#IF IS_DEBUG_BUILD
	// Debug handling to display the true numbers for the parents
	IF characterData.bDebugDrawParentHeadValues
		
		SET_FRONTEND_HERITAGE_SLOT(iColumn, ciCHARACTER_CREATOR_HERITAGE_MUM_NAME, characterData.iCharacterMenuID, ciCHARACTER_CREATOR_HERITAGE_MUM_NAME, 2, 0, "FACE_MUMS", tlMumTxn, TRUE, DEFAULT, bUpdate, iMumHeadValue)
		SET_FRONTEND_HERITAGE_SLOT(iColumn, ciCHARACTER_CREATOR_HERITAGE_DAD_NAME, characterData.iCharacterMenuID, ciCHARACTER_CREATOR_HERITAGE_DAD_NAME, 2, 0, "FACE_DADS", tlDadTxn, TRUE, DEFAULT, bUpdate, iDadHeadValue)
		
	ELSE
	#ENDIF
	
	SET_FRONTEND_HERITAGE_SLOT(iColumn, ciCHARACTER_CREATOR_HERITAGE_MUM_NAME, characterData.iCharacterMenuID, ciCHARACTER_CREATOR_HERITAGE_MUM_NAME, 2, 0, "FACE_MUMS", tlMumTxn, TRUE, DEFAULT, bUpdate, GET_CHARACTER_CREATOR_PARENT_HEAD(characterData, ciCHARACTER_CREATOR_MUM_INDEX) + 1)
	SET_FRONTEND_HERITAGE_SLOT(iColumn, ciCHARACTER_CREATOR_HERITAGE_DAD_NAME, characterData.iCharacterMenuID, ciCHARACTER_CREATOR_HERITAGE_DAD_NAME, 2, 0, "FACE_DADS", tlDadTxn, TRUE, DEFAULT, bUpdate, GET_CHARACTER_CREATOR_PARENT_HEAD(characterData, ciCHARACTER_CREATOR_DAD_INDEX) + 1)
	
	#IF IS_DEBUG_BUILD
	ENDIF
	#ENDIF
	
	SET_FRONTEND_HERITAGE_SLOT(iColumn, ciCHARACTER_CREATOR_HERITAGE_DOM, characterData.iCharacterMenuID, ciCHARACTER_CREATOR_HERITAGE_DOM, 1, GET_CHARACTER_CREATOR_CHILD_DOMINANCE(characterData, characterData.eActiveChild), "FACE_H_DOM", DEFAULT, DEFAULT, DEFAULT, bUpdate)
	SET_FRONTEND_HERITAGE_SLOT(iColumn, ciCHARACTER_CREATOR_HERITAGE_SKIN_TONE, characterData.iCharacterMenuID, ciCHARACTER_CREATOR_HERITAGE_SKIN_TONE, 1, GET_CHARACTER_CREATOR_CHILD_SKIN_TONE(characterData, characterData.eActiveChild), "FACE_H_STON", DEFAULT, DEFAULT, DEFAULT, bUpdate)

	IF NOT bUpdate
		// Make sure it displays
		DISPLAY_FRONTEND_DATA_SLOT(iColumn)
		SET_FRONTEND_COLUMN_FOCUS(iColumn, TRUE)
		
		// Update our menu state and highlight the selection
		SET_FRONTEND_HIGHLIGHT(iColumn, GET_CHARACTER_CREATOR_CURRENT_SELECTION(characterData), TRUE)
			
		SET_FRONTEND_COLUMN_AS_LOCKED(iColumn, TRUE)
		
		// Show the correct column, hiding all others
		DISPLAY_CHARACTER_CREATOR_COLUMN(iColumn)
	ENDIF
	
	// Update our buttons
	UPDATE_CHARACTER_CONTROLLER_BUTTONS(characterData)
ENDPROC

/// PURPOSE:
///    Update the grid that is displayed at the bottom of the features grid
PROC UPDATE_CHARACTER_CREATOR_FEATURE_GRID(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, BOOL bUpdate = FALSE)
	
	CHARACTER_CREATOR_FEATURE_DATA featureData
	CHARACTER_CREATOR_FEATURE_VALUES featureValues
	INT iCurrentSelectedFeature = GET_CHARACTER_CREATOR_CURRENT_SELECTION(characterData)
	
	GET_CHARACTER_CREATOR_FEATURE_DATA(featureData, iCurrentSelectedFeature)
	GET_CHARACTER_CREATOR_FEATURE_VALUES(characterData, featureValues, iCurrentSelectedFeature)

	INT iRowType = 0
	IF NOT featureData.bYAxisActive
		iRowType = 1
	ENDIF
	
	SET_FRONTEND_FEATURE_SLOT(ciCHARACTER_CREATOR_FEATURE_COLUMN, 0, characterData.iCharacterMenuID, 0, featureData.tlUpText, featureData.tlRightText, featureData.tlDownText, featureData.tlLeftText, featureValues.fXBlend, featureValues.fYBlend, iRowType, bUpdate, IS_CHARACTER_CONTROLLER_BIT_SET(ciCHARACTER_CONTROLLER_PULSE_FEATURE))
	CLEAR_CHARACTER_CONTROLLER_BIT(ciCHARACTER_CONTROLLER_PULSE_FEATURE)
ENDPROC

/// PURPOSE:
///    Populates the new feature column (includes two columns scaleform side)
PROC POPULATE_CHARACTER_CREATOR_FEATURES_MENU(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, BOOL bUpdate = FALSE)
	
	// Grab the scaleform column
	INT iColumn = GET_CHARACTER_CONTROLLER_COLUMN_INDEX(ciCHARACTER_CREATOR_COLUMN_FEATURES)
	
	IF NOT bUpdate
		SET_CHARACTER_CREATOR_MENU_STATE(characterData, ciCHARACTER_CREATOR_COLUMN_FEATURES)
		
		// Clear all data
		CLEAR_FRONTEND_DATA_SLOT(iColumn)
		CLEAR_FRONTEND_DATA_SLOT(ciCHARACTER_CREATOR_FEATURE_COLUMN)			// Blend chart
	ENDIF
	
	INT i
	INT iCount
	INT iHighlight = -1
	TEXT_LABEL_15 tlFeature
	CHARACTER_CREATOR_FEATURE_DATA featureData
	CHARACTER_CREATOR_FEATURE_VALUES featureValues
	CHARACTER_CREATOR_FEATURE_PRESET_DATA featurePresetData
	CHARACTER_CREATOR_FEATURE_GENDER featureGender = CHARACTER_CREATOR_GENDER_FEMALE
	
	IF GET_CHARACTER_CREATOR_ACTIVE_PED_IS_MALE(characterData)
		featureGender = CHARACTER_CREATOR_GENDER_MALE
	ENDIF
	
	BOOL bArrows
	
	REPEAT ciCHARACTER_CREATOR_MAX_FEATURES i
	
		IF GET_CHARACTER_CREATOR_FEATURE_DATA(featureData, i)
	
			IF featureData.iGenderSpecific = CHARACTER_CREATOR_GENDER_BOTH
			OR featureData.iGenderSpecific = featureGender
	
				GET_CHARACTER_CREATOR_FEATURE_VALUES(characterData, featureValues, i)
							
				tlFeature = ""
				
				// These are presets that are defined by us
				IF featureValues.iPreset = -1
					tlFeature = "FACE_F_P_CUST"
				ELSE
					GET_CHARACTER_CREATOR_FEATURE_PRESET_DATA(featurePresetData, i, featureValues.iPreset)
					tlFeature = featurePresetData.tlPresetName
				ENDIF
				
				bArrows = (featureData.iNumOfPresets > 1)
				SET_FRONTEND_CHARACTER_SETTING_SLOT(iColumn, iCount, characterData.iCharacterMenuID, i, featureData.tlFeatureName, tlFeature, PICK_INT(bArrows, 0, 1), ciCHARACTER_CREATOR_MENU_ROW_TYPE, bUpdate)
				
				IF iHighlight = -1
					IF GET_CHARACTER_CREATOR_CURRENT_SELECTION(characterData) = i
						iHighlight = iCount
					ENDIF
				ENDIF
				
				iCount++
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF iHighlight = -1
		iHighlight = 0
	ENDIF
	
	// Set up the titles
	SETUP_CHARACTER_CREATOR_TITLES(characterData, (iHighlight+1), iCount)
	
	// Grab the details of the current feature
	INT iCurrentSelectedFeature = GET_CHARACTER_CREATOR_CURRENT_SELECTION(characterData)
	IF NOT GET_CHARACTER_CREATOR_FEATURE_DATA(featureData, iCurrentSelectedFeature)
		GET_CHARACTER_CREATOR_FEATURE_DATA(featureData, 0)
	ENDIF
	
	UPDATE_CHARACTER_CREATOR_FEATURE_GRID(characterData, bUpdate)
	
	IF NOT bUpdate
		// Make sure it displays
		DISPLAY_FRONTEND_DATA_SLOT(iColumn)
		DISPLAY_FRONTEND_DATA_SLOT(ciCHARACTER_CREATOR_FEATURE_COLUMN)
		SET_FRONTEND_COLUMN_FOCUS(iColumn, TRUE)
		
		// Add the scroll bar
		ADD_SCALEFORMXML_INIT_COLUMN_SCROLL_GEN(iColumn, 1, 1, 1, 0, FALSE)
		
		// Update our menu state and highlight the selection
		SET_FRONTEND_HIGHLIGHT(iColumn, iHighlight, TRUE)
			
		SET_FRONTEND_COLUMN_AS_LOCKED(iColumn, TRUE)
		
		SET_CHARACTER_CREATOR_MENU_HELP_TEXT(characterData)
		
		// Show the correct column, hiding all others
		DISPLAY_CHARACTER_CREATOR_COLUMN(iColumn)
		SET_FRONTEND_DISPLAY_COLUMN(ciCHARACTER_CREATOR_FEATURE_COLUMN, TRUE)
	ENDIF
	
	// Update our buttons
	UPDATE_CHARACTER_CONTROLLER_BUTTONS(characterData)
ENDPROC


/// PURPOSE:
///    Populates the stats column
PROC POPULATE_CHARACTER_CREATOR_STATS_MENU(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, BOOL bUpdate = FALSE)
	
	// Grab the scaleform column
	INT iColumn = GET_CHARACTER_CONTROLLER_COLUMN_INDEX(ciCHARACTER_CREATOR_COLUMN_STATS)
	
	IF NOT bUpdate
		SET_CHARACTER_CREATOR_MENU_STATE(characterData, ciCHARACTER_CREATOR_COLUMN_STATS)
		
		// Clear all data
		CLEAR_FRONTEND_DATA_SLOT(iColumn)
		
		// Set up the titles
		SETUP_CHARACTER_CREATOR_TITLES(characterData)
	ENDIF
	
	INT i
	CHARACTER_CREATOR_STATS aStat
	CHARACTER_CREATOR_STAT_DATA statData
	
	// Loop over all the stats populating the bars
	REPEAT CHARACTER_CREATOR_MAX_STATS i
		
		aStat = INT_TO_ENUM(CHARACTER_CREATOR_STATS, i)
		GET_CHARACTER_CREATOR_STAT_DATA(statData, aStat)
		
		SET_FRONTEND_CHARACTER_STAT_SLOT(iColumn, i, characterData.iCharacterMenuID, i, 0, statData.tlStatTitle, "", HUD_COLOUR_FREEMODE, GET_CHARACTER_CREATOR_STAT_VALUE(characterData, aStat), statData.iMaxValue, DEFAULT, bUpdate)		
	ENDREPEAT
	
	// Set up the left to assign bar
	
	HUD_COLOURS hcColour = HUD_COLOUR_RED
	TEXT_LABEL_15 tlStatText = "FACE_STATPT"
	IF GET_CHARACTER_CREATOR_STATS_LEFT_TO_ASSIGN(characterData) <= 0
		hcColour = HUD_COLOUR_GREY
		tlStatText = "FACE_STATPTA"
	ENDIF
	
	SET_FRONTEND_CHARACTER_STAT_SLOT(iColumn, ENUM_TO_INT(CHARACTER_CREATOR_MAX_STATS), characterData.iCharacterMenuID, ENUM_TO_INT(CHARACTER_CREATOR_MAX_STATS), 1, tlStatText, "", hcColour, GET_CHARACTER_CREATOR_STATS_LEFT_TO_ASSIGN(characterData), 100, FALSE, bUpdate)
	
	IF NOT bUpdate
		// Make sure it displays
		DISPLAY_FRONTEND_DATA_SLOT(iColumn)
		SET_FRONTEND_COLUMN_FOCUS(iColumn, TRUE)
		
		SET_CHARACTER_CREATOR_MENU_HELP_TEXT(characterData)		
		
		// Update our menu state and highlight the selection
		SET_FRONTEND_HIGHLIGHT(iColumn, GET_CHARACTER_CREATOR_CURRENT_SELECTION(characterData), TRUE)
			
		SET_FRONTEND_COLUMN_AS_LOCKED(iColumn, TRUE)
		
		// Show the correct column, hiding all others
		DISPLAY_CHARACTER_CREATOR_COLUMN(iColumn)
	ENDIF
	
	// Update our buttons
	UPDATE_CHARACTER_CONTROLLER_BUTTONS(characterData)
ENDPROC

/// PURPOSE:
///    Set up a row in the appearance menu
PROC SET_CHARACTER_CREATOR_APPEARANCE_ROW(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, INT iColumn, INT iCount, INT iAppearance, STRING tlAppearanceName, INT iValue, BOOL bUpdate = FALSE)
	
	TEXT_LABEL_15 tlOption

	SWITCH iAppearance
		
		CASE ciCHARACTER_CREATOR_APPEARANCE_HAIR_STYLE_F
		CASE ciCHARACTER_CREATOR_APPEARANCE_HAIR_STYLE_M
			IF GET_CHARACTER_CREATOR_ACTIVE_PED_IS_MALE(characterData)
				tlOption = GET_CHARACTER_CREATOR_HAIR_STYLE_TEXT(ciCHARACTER_CREATOR_APPEARANCE_HAIR_STYLE_M, iValue)
			ELSE
				tlOption = GET_CHARACTER_CREATOR_HAIR_STYLE_TEXT(ciCHARACTER_CREATOR_APPEARANCE_HAIR_STYLE_F, iValue)
			ENDIF
			SET_FRONTEND_CHARACTER_SETTING_SLOT(iColumn, iCount, characterData.iCharacterMenuID, iAppearance, "FACE_HAIR", tlOption, 0, ciCHARACTER_CREATOR_MENU_ROW_TYPE, bUpdate)
		BREAK
		
		CASE ciCHARACTER_CREATOR_APPEARANCE_EYES
			tlOption = "FACE_E_C_"
			tlOption += iValue			
			SET_FRONTEND_CHARACTER_SETTING_SLOT(iColumn, iCount, characterData.iCharacterMenuID, iAppearance, "FACE_APP_EYE", tlOption, 0, ciCHARACTER_CREATOR_MENU_ROW_TYPE, bUpdate)
		BREAK
		
		CASE ciCHARACTER_CREATOR_APPEARANCE_EYE_MAKEUP
		CASE ciCHARACTER_CREATOR_APPEARANCE_EYE_MAKEUP_M
			tlOption = "CC_MKUP_"
			tlOption += iValue
			
			IF iValue = ciCHARACTER_CREATOR_EXISTING_INDEX  
				tlOption = "FACE_F_P_OFF"
			ENDIF
			IF iValue = ciCHARACTER_CREATOR_EXISTING_OVERLAY_INDEX
				tlOption = "FACE_CURR"
			ENDIF
			
			SET_FRONTEND_CHARACTER_SETTING_SLOT(iColumn, iCount, characterData.iCharacterMenuID, iAppearance, "FACE_F_EYEM", tlOption, 0, ciCHARACTER_CREATOR_MENU_ROW_TYPE, bUpdate)
		BREAK

		CASE ciCHARACTER_CREATOR_APPEARANCE_EYEBROWS_F
		CASE ciCHARACTER_CREATOR_APPEARANCE_EYEBROWS_M
			tlOption = "CC_EYEBRW_"
			tlOption += iValue
			
			IF iValue = -1
				tlOption = "FACE_F_P_OFF"
				iValue = -1
			ENDIF
			SET_FRONTEND_CHARACTER_SETTING_SLOT(iColumn, iCount, characterData.iCharacterMenuID, iAppearance, "FACE_F_EYEBR", tlOption, 0, ciCHARACTER_CREATOR_MENU_ROW_TYPE, bUpdate)
		BREAK
		
		CASE ciCHARACTER_CREATOR_APPEARANCE_SKIN_BLEMISHES
			tlOption = "CC_SKINBLEM_"
			tlOption += iValue
			
			IF iValue = -1
				tlOption = "FACE_F_P_OFF"
				iValue = -1
			ENDIF
			SET_FRONTEND_CHARACTER_SETTING_SLOT(iColumn, iCount, characterData.iCharacterMenuID, iAppearance, "FACE_F_SKINB", tlOption, 0, ciCHARACTER_CREATOR_MENU_ROW_TYPE, bUpdate)
		BREAK
		
		CASE ciCHARACTER_CREATOR_APPEARANCE_SKIN_AGING
			tlOption = "CC_SKINAGE_"
			tlOption += iValue
			
			IF iValue = -1
				tlOption = "FACE_F_P_OFF"
				iValue = -1
			ENDIF
			SET_FRONTEND_CHARACTER_SETTING_SLOT(iColumn, iCount, characterData.iCharacterMenuID, iAppearance, "FACE_F_SKINA", tlOption, 0, ciCHARACTER_CREATOR_MENU_ROW_TYPE, bUpdate)
		BREAK
		
		CASE ciCHARACTER_CREATOR_APPEARANCE_SKIN_COMPLEXION
			tlOption = "CC_SKINCOM_"
			tlOption += iValue
			
			IF iValue = -1
				tlOption = "FACE_F_P_OFF"
				iValue = -1
			ENDIF
			SET_FRONTEND_CHARACTER_SETTING_SLOT(iColumn, iCount, characterData.iCharacterMenuID, iAppearance, "FACE_F_SKC", tlOption, 0, ciCHARACTER_CREATOR_MENU_ROW_TYPE, bUpdate)
		BREAK
		
		CASE ciCHARACTER_CREATOR_APPEARANCE_MOLE_FRECKLES
			tlOption = "CC_MOLEFRECK_"
			tlOption += iValue
			
			IF iValue = -1
				tlOption = "FACE_F_P_OFF"
				iValue = -1
			ENDIF
			SET_FRONTEND_CHARACTER_SETTING_SLOT(iColumn, iCount, characterData.iCharacterMenuID, iAppearance, "FACE_F_MOLE", tlOption, 0, ciCHARACTER_CREATOR_MENU_ROW_TYPE, bUpdate)
		BREAK
		
		CASE ciCHARACTER_CREATOR_APPEARANCE_SUN_DAMAGE
			tlOption = "CC_SUND_"
			tlOption += iValue
			
			IF iValue = -1
				tlOption = "FACE_F_P_OFF"
				iValue = -1
			ENDIF
			SET_FRONTEND_CHARACTER_SETTING_SLOT(iColumn, iCount, characterData.iCharacterMenuID, iAppearance, "FACE_F_SUND", tlOption, 0, ciCHARACTER_CREATOR_MENU_ROW_TYPE, bUpdate)
		BREAK
		
		CASE ciCHARACTER_CREATOR_APPEARANCE_BLUSHER
			tlOption = "CC_BLUSH_"
			tlOption += iValue
			
			IF iValue = -1
				tlOption = "FACE_F_P_OFF"
				iValue = -1
			ENDIF
			SET_FRONTEND_CHARACTER_SETTING_SLOT(iColumn, iCount, characterData.iCharacterMenuID, iAppearance, "FACE_F_BLUSH", tlOption, 0, ciCHARACTER_CREATOR_MENU_ROW_TYPE, bUpdate)
		BREAK
		
		CASE ciCHARACTER_CREATOR_APPEARANCE_LIPSTICK
		CASE ciCHARACTER_CREATOR_APPEARANCE_LIPSTICK_M
			tlOption = "CC_LIPSTICK_"
			tlOption += iValue
			
			IF iValue = -1
				tlOption = "FACE_F_P_OFF"
				iValue = -1
			ENDIF
			SET_FRONTEND_CHARACTER_SETTING_SLOT(iColumn, iCount, characterData.iCharacterMenuID, iAppearance, "FACE_F_LIPST", tlOption, 0, ciCHARACTER_CREATOR_MENU_ROW_TYPE, bUpdate)
		BREAK
		
		CASE ciCHARACTER_CREATOR_APPEARANCE_FACIAL_HAIR
			tlOption = "CC_BEARD_"
			tlOption += iValue
			SET_FRONTEND_CHARACTER_SETTING_SLOT(iColumn, iCount, characterData.iCharacterMenuID, iAppearance, "FACE_F_BEARD", tlOption, 0, ciCHARACTER_CREATOR_MENU_ROW_TYPE, bUpdate)
		BREAK
		
		DEFAULT		
			tlOption = "NUMBER"
		
			IF iValue = -1
				tlOption = "FACE_F_P_OFF"
				iValue = -1
			ENDIF
			
			IF iValue = ciCHARACTER_CREATOR_EXISTING_OVERLAY_INDEX
				tlOption = "FACE_CURR"
			ENDIF
		
			SET_FRONTEND_CHARACTER_SETTING_SLOT(iColumn, iCount, characterData.iCharacterMenuID, iAppearance, tlAppearanceName, tlOption, 0, ciCHARACTER_CREATOR_MENU_ROW_TYPE, bUpdate, (iValue+1))		
		BREAK	
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Updates the opacicty values on the appearance grid
PROC SET_CHARACTER_CREATOR_APPEARANCE_GRID_DETAILS(BOOL bOpacityActive, BOOL bColourActive, INT iValue, FLOAT fOpacityVal, INT iInt1, INT iInt2)

	TEXT_LABEL_15 tlStr1 = ""
	TEXT_LABEL_15 tlStr2 = ""
	FLOAT fOpacity = -1
	BOOL bAddPCMouseArrows = FALSE

	// Only display opacity if the appearance option allows it
	IF bOpacityActive
	AND iValue != -1
		fOpacity = fOpacityVal
		tlStr1 = "FACE_OPAC"
	ENDIF
	
	// Same is true for colour
	IF bColourActive
		tlStr2 = "FACE_COLOUR"
		iInt1 += 1			// Increment this as our colour is 0 indexed
	ELSE
		iInt1 = -1
		iInt2 = -1
	ENDIF
	
	IF IS_PC_VERSION()
		bAddPCMouseArrows = TRUE
	ENDIF

	SET_FRONTEND_APPEARANCE_SLOT(ciCHARACTER_CREATOR_APPEARANCE_DETAILS_COLUMN, tlStr1, tlStr2, fOpacity, iInt1, iInt2, bAddPCMouseArrows)
ENDPROC

FUNC INT GET_CHARACTER_CREATOR_TINT_MAX_AVAILABLE(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, INT iAppearance)
	SWITCH iAppearance
		CASE ciCHARACTER_CREATOR_APPEARANCE_EYEBROWS_F
		CASE ciCHARACTER_CREATOR_APPEARANCE_EYEBROWS_M
		CASE ciCHARACTER_CREATOR_APPEARANCE_FACIAL_HAIR
		CASE ciCHARACTER_CREATOR_APPEARANCE_HAIR_STYLE_F
		CASE ciCHARACTER_CREATOR_APPEARANCE_HAIR_STYLE_M
			RETURN characterData.iMaxHairColours
	
		CASE ciCHARACTER_CREATOR_APPEARANCE_BLUSHER
			RETURN characterData.iMaxBlusherColours
			
		CASE ciCHARACTER_CREATOR_APPEARANCE_LIPSTICK
		CASE ciCHARACTER_CREATOR_APPEARANCE_LIPSTICK_M
			RETURN characterData.iMaxLipstickColours
	ENDSWITCH
	
	RETURN 1
ENDFUNC

/// PURPOSE:
///    Apply an update to the highlight for colours
PROC UPDATE_CHARACTER_CREATOR_APPEARANCE_HIGHLIGHT(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, INT iAppearance)

	CHARACTER_CREATOR_APPEARANCE_DATA appearanceData
	CHARACTER_CREATOR_APPEARANCE_VALUES appearanceValues	
	GET_CHARACTER_CREATOR_APPEARANCE_DATA(appearanceData, iAppearance)
	GET_CHARACTER_CREATOR_APPEARANCE_VALUES(characterData, appearanceValues, iAppearance)
	
	INT iHighlight = appearanceValues.iColourHighlightValue
	INT iColourMax = GET_CHARACTER_CREATOR_TINT_MAX_AVAILABLE(characterData, iAppearance)
	
	SET_CHARACTER_CREATOR_APPEARANCE_GRID_DETAILS(appearanceData.bOpacityActive, appearanceData.bColourActive, appearanceValues.iValue, appearanceValues.fOpacity, iHighlight, iColourMax)

	SET_FRONTEND_APPEARANCE_HIGHLIGHT(ciCHARACTER_CREATOR_APPEARANCE_DETAILS_COLUMN, iHighlight)
ENDPROC


/// PURPOSE:
///    Fills out the appearance grid if valid
PROC UPDATE_CHARACTER_CREATOR_APPEARANCE_GRID(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData)
	
	CHARACTER_CREATOR_APPEARANCE_DATA appearanceData
	CHARACTER_CREATOR_APPEARANCE_VALUES appearanceValues
	INT iCurrentSelectedAppearance = GET_CHARACTER_CREATOR_CURRENT_SELECTION(characterData)
	
	GET_CHARACTER_CREATOR_APPEARANCE_DATA(appearanceData, iCurrentSelectedAppearance)
	GET_CHARACTER_CREATOR_APPEARANCE_VALUES(characterData, appearanceValues, iCurrentSelectedAppearance)

	INT iHighlight = appearanceValues.iColourHighlightValue
	INT iColourMax = GET_CHARACTER_CREATOR_TINT_MAX_AVAILABLE(characterData, iCurrentSelectedAppearance)

	SET_CHARACTER_CREATOR_APPEARANCE_GRID_DETAILS(appearanceData.bOpacityActive, appearanceData.bColourActive, appearanceValues.iValue, appearanceValues.fOpacity, iHighlight, iColourMax)
	
	CLEAR_FRONTEND_DATA_SLOT(ciCHARACTER_CREATOR_APPEARANCE_DETAILS_COLUMN)
	
	// If the colour option is active, populate menu with colour cells and highlight current selection
	IF appearanceData.bColourActive
	AND appearanceValues.iValue != ciCHARACTER_CREATOR_EXISTING_INDEX
		
		#IF IS_DEBUG_BUILD
		IF appearanceData.colourPalette = RT_NONE
			SCRIPT_ASSERT("UPDATE_CHARACTER_CREATOR_APPEARANCE_GRID - We are setting up colour for an overlay that uses an INVALID palette. RT_NONE")
		ENDIF
		#ENDIF
		
		CLEAR_FRONTEND_DATA_SLOT(ciCHARACTER_CREATOR_APPEARANCE_DETAILS_COLUMN)
		
		CPRINTLN(DEBUG_NET_CHARACTER, "UPDATE_CHARACTER_CREATOR_APPEARANCE_GRID - Pass the colours to our palette for appearance: ", iCurrentSelectedAppearance, " max colours: ", appearanceData.iColourMax)
		
		INT i
		INT iCount
		INT iR, iG, iB
		BOOL bIncludeColour
		REPEAT appearanceData.iColourMax i
			
			bIncludeColour = TRUE
			
			// Get the RGB values for our paletter		
			IF IS_CHARACTER_CREATOR_TINT_VALID(appearanceData.overlaySlot, i)
			
				IF appearanceData.colourPalette = RT_HAIR
					GET_PED_HAIR_TINT_COLOR(i, iR, iG, iB)
				ELIF appearanceData.colourPalette = RT_MAKEUP
					GET_PED_MAKEUP_TINT_COLOR(i, iR, iG, iB)
				ENDIF
			ELSE
				// Ignore this colour as not valid
				bIncludeColour = FALSE
			ENDIF
			
			IF bIncludeColour
				// Pass this through to the scaleform
				SET_FRONTEND_APPEARANCE_COLOUR_SLOT(ciCHARACTER_CREATOR_APPEARANCE_DETAILS_COLUMN, iCount, iR, iG, iB)
				iCount++
			ENDIF
		ENDREPEAT
		
		DISPLAY_FRONTEND_DATA_SLOT(ciCHARACTER_CREATOR_APPEARANCE_DETAILS_COLUMN)
		
		UPDATE_CHARACTER_CREATOR_APPEARANCE_HIGHLIGHT(characterData, iCurrentSelectedAppearance)
	ENDIF
ENDPROC

/// PURPOSE:
///    Populates the new appearance column (includes two columns scaleform side)
PROC POPULATE_CHARACTER_CREATOR_APPEARANCE_MENU(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, BOOL bUpdate = FALSE)
	
	// Grab the scaleform column
	INT iColumn = GET_CHARACTER_CONTROLLER_COLUMN_INDEX(ciCHARACTER_CREATOR_COLUMN_APPEARANCE)
	
	IF NOT bUpdate
		SET_CHARACTER_CREATOR_MENU_STATE(characterData, ciCHARACTER_CREATOR_COLUMN_APPEARANCE)
		
		// Clear all data
		CLEAR_FRONTEND_DATA_SLOT(iColumn)
		CLEAR_FRONTEND_DATA_SLOT(ciCHARACTER_CREATOR_APPEARANCE_DETAILS_COLUMN)			// Blend chart
	ENDIF
	
	INT i
	INT iCount
	INT iHighlight = -1
	INT iHighestHighlight = -1

	CHARACTER_CREATOR_APPEARANCE_DATA appearanceData
	CHARACTER_CREATOR_APPEARANCE_VALUES appearanceValues
	CHARACTER_CREATOR_FEATURE_GENDER appearanceGender = CHARACTER_CREATOR_GENDER_FEMALE
	
	IF GET_CHARACTER_CREATOR_ACTIVE_PED_IS_MALE(characterData)
		appearanceGender = CHARACTER_CREATOR_GENDER_MALE
	ENDIF
	
	REPEAT ciCHARACTER_CREATOR_MAX_APPEARANCE i
	
		IF GET_CHARACTER_CREATOR_APPEARANCE_DATA(appearanceData, i)
	
			IF appearanceData.iGenderSpecific = CHARACTER_CREATOR_GENDER_BOTH
			OR appearanceData.iGenderSpecific = appearanceGender
	
				GET_CHARACTER_CREATOR_APPEARANCE_VALUES(characterData, appearanceValues, i)
				
				SET_CHARACTER_CREATOR_APPEARANCE_ROW(characterData, iColumn, iCount, i, appearanceData.tlAppearanceName, appearanceValues.iValue, bUpdate)
				
				IF iHighestHighlight = -1
					iHighestHighlight = i
				ENDIF
				
				IF iHighlight = -1
					IF GET_CHARACTER_CREATOR_CURRENT_SELECTION(characterData) = i
						iHighlight = iCount
					ENDIF
				ENDIF
				
				iCount++
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF iHighlight = -1
		iHighlight = 0
		SET_CHARACTER_CREATOR_CURRENT_SELECTION(characterData, iHighestHighlight)
	ENDIF
	
	// Set up the titles
	SETUP_CHARACTER_CREATOR_TITLES(characterData, (iHighlight+1), iCount)
	
	// Grab the details of the current feature
	INT iCurrentSelectedAppearance = GET_CHARACTER_CREATOR_CURRENT_SELECTION(characterData)
	IF NOT GET_CHARACTER_CREATOR_APPEARANCE_DATA(appearanceData, iCurrentSelectedAppearance)
		GET_CHARACTER_CREATOR_APPEARANCE_DATA(appearanceData, 0)
	ENDIF
	
	UPDATE_CHARACTER_CREATOR_APPEARANCE_GRID(characterData)
	
	IF NOT bUpdate
		// Make sure it displays
		DISPLAY_FRONTEND_DATA_SLOT(iColumn)
		SET_FRONTEND_COLUMN_FOCUS(iColumn, TRUE)
		
		// Add the scroll bar
		ADD_SCALEFORMXML_INIT_COLUMN_SCROLL_GEN(iColumn, 1, 1, 1, 0, FALSE)
		
		// Update our menu state and highlight the selection
		SET_FRONTEND_HIGHLIGHT(iColumn, iHighlight, TRUE)
			
		SET_FRONTEND_COLUMN_AS_LOCKED(iColumn, TRUE)
		
		SET_CHARACTER_CREATOR_MENU_HELP_TEXT(characterData)
		
		// Show the correct column, hiding all others
		DISPLAY_CHARACTER_CREATOR_COLUMN(iColumn)
		SET_FRONTEND_DISPLAY_COLUMN(ciCHARACTER_CREATOR_APPEARANCE_DETAILS_COLUMN, TRUE)
	ENDIF
	
	// Update our buttons
	UPDATE_CHARACTER_CONTROLLER_BUTTONS(characterData)
ENDPROC


/// PURPOSE:
///    Adds the row with the correct appearance details
PROC ADD_CHARACTER_CREATOR_APPAREL_ROW(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, INT iRow, INT iColumn, INT iIndex, BOOL bUpdate = FALSE)
	
	TEXT_LABEL_15 tlOption
	
	SWITCH iRow
	
		CASE ciCHARACTER_CREATOR_APPAREL_STYLE
			tlOption = "FACE_A_STY_"
			tlOption += GET_CHARACTER_CREATOR_CHILD_STYLE(characterData, characterData.eActiveChild)
			
			// If we are editing then show current
			IF GET_CHARACTER_CREATOR_CHILD_STYLE(characterData, characterData.eActiveChild) = ciCHARACTER_CREATOR_EXISTING_INDEX
				tlOption = "FACE_CURR"
			ENDIF
			
			SET_FRONTEND_CHARACTER_SETTING_SLOT(iColumn, iIndex, characterData.iCharacterMenuID, iRow, "FACE_APP_STY", tlOption, 0, ciCHARACTER_CREATOR_MENU_ROW_TYPE, bUpdate)
		BREAK
		
		CASE ciCHARACTER_CREATOR_APPAREL_OUTFIT
		
			INT iStyle
			CHARACTER_CREATOR_OUTFIT eOutfit
			iStyle = GET_CHARACTER_CREATOR_CHILD_STYLE(characterData, characterData.eActiveChild)
			eOutfit = GET_CHARACTER_CREATOR_OUTFIT_SELECTION(iStyle, GET_CHARACTER_CREATOR_CHILD_OUTFIT(characterData, characterData.eActiveChild))
		
			tlOption = GET_CHARACTER_CREATOR_OUTFIT_NAME(eOutfit, GET_CHARACTER_CREATOR_ACTIVE_PED_IS_MALE(characterData))
		
			SET_FRONTEND_CHARACTER_SETTING_SLOT(iColumn, iIndex, characterData.iCharacterMenuID, iRow, "FACE_APP_OUT", tlOption, 0, ciCHARACTER_CREATOR_MENU_ROW_TYPE, bUpdate)
		BREAK
		
		
		CASE ciCHARACTER_CREATOR_APPAREL_HAT
			tlOption = "FACE_OFF"
			
			IF GET_CHARACTER_CREATOR_CHILD_HAT(characterData, characterData.eActiveChild) = ciCHARACTER_CREATOR_EXISTING_OVERLAY_INDEX
				tlOption = "FACE_CURR"
			ENDIF
			
			IF GET_CHARACTER_CREATOR_CHILD_HAT(characterData, characterData.eActiveChild) >= 0
				tlOption = GET_CHARACTER_CREATOR_HAT_NAME(characterData.characterCreatorPeds[characterData.eActiveChild], GET_CHARACTER_CREATOR_CHILD_HAT(characterData, characterData.eActiveChild))
			ENDIF
			
			SET_FRONTEND_CHARACTER_SETTING_SLOT(iColumn, iIndex, characterData.iCharacterMenuID, iRow, "FACE_HAT", tlOption, 0, ciCHARACTER_CREATOR_MENU_ROW_TYPE, bUpdate)
		BREAK
		
		CASE ciCHARACTER_CREATOR_APPAREL_GLASSES
			tlOption = "FACE_OFF"
			
			IF GET_CHARACTER_CREATOR_CHILD_GLASSES(characterData, characterData.eActiveChild) = ciCHARACTER_CREATOR_EXISTING_OVERLAY_INDEX
				tlOption = "FACE_CURR"
			ENDIF
			
			IF GET_CHARACTER_CREATOR_CHILD_GLASSES(characterData, characterData.eActiveChild) >= 0
				tlOption = GET_CHARACTER_CREATOR_GLASSES_NAME(characterData.characterCreatorPeds[characterData.eActiveChild], GET_CHARACTER_CREATOR_CHILD_GLASSES(characterData, characterData.eActiveChild))
			ENDIF
		
			SET_FRONTEND_CHARACTER_SETTING_SLOT(iColumn, iIndex, characterData.iCharacterMenuID, iRow, "FACE_GLS", tlOption, 0, ciCHARACTER_CREATOR_MENU_ROW_TYPE, bUpdate)
		BREAK
		
		CASE ciCHARACTER_CREATOR_APPAREL_CREW_TSHIRT		
			SET_FRONTEND_CHARACTER_SETTING_SLOT(iColumn, iIndex, characterData.iCharacterMenuID, iRow, "FACE_CREWT", GET_CHARACTER_CREATOR_CREW_TSHIRT_TEXT(characterData), 0, ciCHARACTER_CREATOR_MENU_ROW_TYPE, bUpdate)
		BREAK
	
		CASE ciCHARACTER_CREATOR_APPAREL_CREW_STYLE
			SET_FRONTEND_CHARACTER_SETTING_SLOT(iColumn, iIndex, characterData.iCharacterMenuID, iRow, "FACE_CREWE", GET_CHARACTER_CREATOR_CREW_TSHIRT_DECAL_TEXT(characterData), 0, ciCHARACTER_CREATOR_MENU_ROW_TYPE, bUpdate)
		BREAK
	
	ENDSWITCH
	
ENDPROC


/// PURPOSE:
///    Return TRUE if the apparel row is valid and can be included
FUNC BOOL IS_CHARACTER_CREATOR_CREATOR_APPAREL_ROW_VALID(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, INT iRow)

	INT iStyle = GET_CHARACTER_CREATOR_CHILD_STYLE(characterData, characterData.eActiveChild)
	
	SWITCH iRow
		CASE ciCHARACTER_CREATOR_APPAREL_OUTFIT
			IF iStyle =  ciCHARACTER_CREATOR_EXISTING_INDEX
				RETURN FALSE
			ENDIF
		BREAK
				
		// Only crew members can pick crew tshirt
		CASE ciCHARACTER_CREATOR_APPAREL_CREW_TSHIRT
			IF NOT IS_LOCAL_PLAYER_IN_ACTIVE_CLAN()
				RETURN FALSE
			ENDIF
		BREAK
		
		// Can not see hat if you have the dunce hat on
		CASE ciCHARACTER_CREATOR_APPAREL_HAT
			IF NETWORK_PLAYER_IS_CHEATER()
			OR NETWORK_PLAYER_IS_BADSPORT()
				RETURN FALSE
			ENDIF
		BREAK
		
		// Only if they have turned on the crew tshirt should they show the option
		CASE ciCHARACTER_CREATOR_APPAREL_CREW_STYLE
			IF IS_LOCAL_PLAYER_IN_ACTIVE_CLAN()
				IF GET_CHARACTER_CREATOR_CREW_TSHIRT(characterData) = CREW_TSHIRT_OFF
					RETURN FALSE
				ENDIF
			ELSE
				RETURN FALSE
			ENDIF
		BREAK
		
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

PROC POPULATE_CHARACTER_CREATOR_APPAREL_MENU(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, BOOL bUpdate = FALSE)

	// Grab the scaleform column
	INT iColumn = GET_CHARACTER_CONTROLLER_COLUMN_INDEX(ciCHARACTER_CREATOR_COLUMN_APPAREL)
	
	IF NOT bUpdate
		SET_CHARACTER_CREATOR_MENU_STATE(characterData, ciCHARACTER_CREATOR_COLUMN_APPAREL)
		
		// Clear all data
		CLEAR_FRONTEND_DATA_SLOT(iColumn)
		
		// Set up the titles
		SETUP_CHARACTER_CREATOR_TITLES(characterData)
	ENDIF
	
	// Add the apparel rows
	INT i
	INT iHighlight = -1
	INT iCount = 0
	
	REPEAT ciCHARACTER_CREATOR_APPAREL_TOTAL_ROWS i
		
		IF IS_CHARACTER_CREATOR_CREATOR_APPAREL_ROW_VALID(characterData, i)
			ADD_CHARACTER_CREATOR_APPAREL_ROW(characterData, i, iColumn, iCount, bUpdate)
			
			IF iHighlight = -1
				IF GET_CHARACTER_CREATOR_CURRENT_SELECTION(characterData) = i
					iHighlight = iCount
				ENDIF
			ENDIF
			
			iCount++
		ENDIF
	ENDREPEAT
	

	// If our selection is no longer valid then default to top of menu
	IF iHighlight = -1
		iHighlight = 0
		SET_CHARACTER_CREATOR_CURRENT_SELECTION(characterData, 0)
	ENDIF	
	
	IF NOT bUpdate
		// Make sure it displays
		DISPLAY_FRONTEND_DATA_SLOT(iColumn)
		//SET_FRONTEND_COLUMN_FOCUS(iColumn, TRUE)
		
		SET_CHARACTER_CREATOR_MENU_HELP_TEXT(characterData)
				
		// Update our menu state and highlight the selection
		SET_FRONTEND_HIGHLIGHT(iColumn, iHighlight, TRUE)
			
		SET_FRONTEND_COLUMN_AS_LOCKED(iColumn, TRUE)
		
		// Show the correct column, hiding all others
		DISPLAY_CHARACTER_CREATOR_COLUMN(iColumn)
	ENDIF
	
	// Update our buttons
	UPDATE_CHARACTER_CONTROLLER_BUTTONS(characterData)
ENDPROC



/// PURPOSE:
///    Initialise our creator, clearing scaleform and setting up first menu
PROC INITIALISE_CHARACTER_CREATOR(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, MPHUD_PLACEMENT_TOOLS& Placement)

	// Hide our menu
	SUPPRESS_CHARACTER_MENU_THIS_FRAME()

	IF IS_CHARACTER_CONTROLLER_BIT_SET(ciCHARACTER_CONTROLLER_EDIT_PED_FROM_OLD_DATA)
		
		CPRINTLN(DEBUG_NET_CHARACTER, "INITIALISE_CHARACTER_CREATOR - We have entered creator with an old character. Force full refresh of ped")
		
		INITIALISE_CHARACTER_CONTROLLER_PED_SLOTS(characterData, Placement)
				
		// We need to force a full refresh of the peds if we have entered with an old character
		FORCE_CHARACTER_CREATOR_FULL_REFRESH(characterData)
		
		// Update the text
		UPDATE_CHARACTER_SELECTOR_BOARDS(characterData, Placement)
	ENDIF
	
	// Clear our setting for changing gender as we enter
	SET_CHARACTER_HAS_CHANGED_GENDER(FALSE)
	
	IF SHOULD_CHARACTER_CREATOR_DISPLAY_GENDER(characterData)
		SET_CHARACTER_CREATOR_CURRENT_SELECTION_FOR_MENU(characterData, ciCHARACTER_CREATOR_COLUMN_MAIN_MENU, 0)
	ELSE
		SET_CHARACTER_CREATOR_CURRENT_SELECTION_FOR_MENU(characterData, ciCHARACTER_CREATOR_COLUMN_MAIN_MENU, 1)
	ENDIF
	
	SET_CHARACTER_CREATOR_CURRENT_SELECTION_FOR_MENU(characterData, ciCHARACTER_CREATOR_COLUMN_HERITAGE, 1)
	SET_CHARACTER_CREATOR_CURRENT_SELECTION_FOR_MENU(characterData, ciCHARACTER_CREATOR_COLUMN_STATS, 0)
	SET_CHARACTER_CREATOR_CURRENT_SELECTION_FOR_MENU(characterData, ciCHARACTER_CREATOR_COLUMN_APPEARANCE, 0)
	SET_CHARACTER_CREATOR_CURRENT_SELECTION_FOR_MENU(characterData, ciCHARACTER_CREATOR_COLUMN_APPAREL, 0)
	SET_CHARACTER_CREATOR_CURRENT_SELECTION_FOR_MENU(characterData, ciCHARACTER_CREATOR_COLUMN_FEATURES, 0)
	
	// Bring the light back down
	SET_CHARACTER_CONTROLLER_TIME_OF_DAY(characterData, CHARACTER_CONTROLLER_TIME_OF_DAY_NOON)
		
	// Populate the first menu
	POPULATE_CHARACTER_CREATOR_MAIN_MENU(characterData)
	
	// Ensure eyebrow opacity is fully visible on init (B* 2197771)
	IF IS_CHARACTER_CONTROLLER_BIT_SET(ciCHARACTER_CONTROLLER_COMING_FROM_SELECTOR)
	
		INT iApperance = ciCHARACTER_CREATOR_APPEARANCE_EYEBROWS_M
		IF IS_PED_FEMALE(characterData.characterCreatorPeds[characterData.eActiveChild])
			iApperance = ciCHARACTER_CREATOR_APPEARANCE_EYEBROWS_F
		ENDIF
		
		CHARACTER_CREATOR_APPEARANCE_VALUES appearanceValues
		GET_CHARACTER_CREATOR_APPEARANCE_VALUES(characterData, appearanceValues, iApperance)
	
		IF GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_IS_NG_VERSION, characterData.iCharacterSelectedSlot) >= ciCHARACTER_CREATOR_CURRENT_VERSION
			SET_CHARACTER_CREATOR_APPEARANCE_VALUES(characterData, iApperance, appearanceValues.iValue, characterData.fEyebrowOpacity, appearanceValues.iColour, appearanceValues.iColour, appearanceValues.iColourHighlightValue)
		ELSE
			SET_CHARACTER_CREATOR_APPEARANCE_VALUES(characterData, iApperance, appearanceValues.iValue, 100.0, appearanceValues.iColour, appearanceValues.iColour, appearanceValues.iColourHighlightValue)
		ENDIF
		CLEAR_CHARACTER_CONTROLLER_BIT(ciCHARACTER_CONTROLLER_COMING_FROM_SELECTOR)
	ENDIF

	// Reapply ped hair before walk in (B* 2218930)
	APPLY_CHARACTER_CREATOR_HAIR(characterData, characterData.eActiveChild)
	
	// Move onto running state
	SET_CHARACTER_CREATOR_ANIM_STATE(CHARACTER_CREATOR_ANIM_WALK_IN)
	SET_CHARACTER_CREATOR_STATE(CHARACTER_CREATOR_STATE_WALK_IN)
ENDPROC


/// PURPOSE:
///    Make the ped turn to the right so we can see the profile
FUNC BOOL PROCESS_CHARACTER_CREATOR_TURN_RIGHT(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, BOOL bBegin, BOOL bCleanupOpp, BOOL bCleanup)
	
	SEQUENCE_INDEX seq
	STRING strAnimDict = GET_CHARACTER_CONTROLLER_ANIM_DICT(ciCHARACTER_CREATOR_ANIM_DICT_CREATOR, 0, (characterData.eActiveChild = CHARACTER_CREATOR_SON))
	
	// If the ped is entering or cleaning up an animation wait until we know we are in this flow
//	IF IS_ENTITY_PLAYING_ANIM(characterData.characterCreatorPeds[characterData.eActiveChild], strAnimDict, "Profile_R_intro")
//	OR IS_ENTITY_PLAYING_ANIM(characterData.characterCreatorPeds[characterData.eActiveChild], strAnimDict, "Profile_R_outro")
//	OR IS_ENTITY_PLAYING_ANIM(characterData.characterCreatorPeds[characterData.eActiveChild], strAnimDict, "Profile_L_intro")
//	OR IS_ENTITY_PLAYING_ANIM(characterData.characterCreatorPeds[characterData.eActiveChild], strAnimDict, "Profile_L_outro")
//		RETURN FALSE
//	ENDIF
	
	IF bCleanup
		
		// If trigger tapped
		IF IS_ENTITY_PLAYING_ANIM(characterData.characterCreatorPeds[characterData.eActiveChild], strAnimDict, "Profile_R_Intro")
		AND NOT IS_ENTITY_PLAYING_ANIM(characterData.characterCreatorPeds[characterData.eActiveChild], strAnimDict, "Profile_R_Loop")	
				
			IF (GET_ENTITY_ANIM_CURRENT_TIME(characterData.characterCreatorPeds[characterData.eActiveChild], strAnimDict, "Profile_R_Intro") >= 0.40)
				OPEN_SEQUENCE_TASK(seq)
					TASK_PLAY_ANIM(NULL, strAnimDict, "Profile_R_Outro", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, DEFAULT, AF_TURN_OFF_COLLISION)
					TASK_PLAY_ANIM(NULL, strAnimDict, "Loop", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, DEFAULT, AF_LOOPING | AF_TURN_OFF_COLLISION)
				CLOSE_SEQUENCE_TASK(seq)	
			ELSE
				RETURN FALSE
			ENDIF
			
		// If trigger held
		ELIF IS_ENTITY_PLAYING_ANIM(characterData.characterCreatorPeds[characterData.eActiveChild], strAnimDict, "Profile_R_Loop")
		AND NOT IS_ENTITY_PLAYING_ANIM(characterData.characterCreatorPeds[characterData.eActiveChild], strAnimDict, "Profile_R_Intro")			
			OPEN_SEQUENCE_TASK(seq)
				TASK_PLAY_ANIM(NULL, strAnimDict, "Profile_R_Outro", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, DEFAULT, AF_TURN_OFF_COLLISION)
				TASK_PLAY_ANIM(NULL, strAnimDict, "Loop", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, DEFAULT, AF_LOOPING | AF_TURN_OFF_COLLISION)
			CLOSE_SEQUENCE_TASK(seq)
		ENDIF
		
	ELIF bBegin
		OPEN_SEQUENCE_TASK(seq)
		
			IF bCleanupOpp
				//TASK_PLAY_ANIM(NULL, strAnimDict, "Profile_R_Outro", FAST_BLEND_IN, FAST_BLEND_OUT, DEFAULT, AF_TURN_OFF_COLLISION)
			ENDIF
			
			TASK_PLAY_ANIM(NULL, strAnimDict, "Profile_R_Intro", SLOW_BLEND_IN, SLOW_BLEND_OUT, DEFAULT, AF_TURN_OFF_COLLISION)
			TASK_PLAY_ANIM(NULL, strAnimDict, "Profile_R_Loop", SLOW_BLEND_IN, SLOW_BLEND_OUT, DEFAULT, AF_LOOPING | AF_TURN_OFF_COLLISION)
		CLOSE_SEQUENCE_TASK(seq)
	ENDIF
	
	TASK_PERFORM_SEQUENCE(characterData.characterCreatorPeds[characterData.eActiveChild], seq)
	CLEAR_SEQUENCE_TASK(seq)
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Make the ped turn to the left so we can see the profile
FUNC BOOL PROCESS_CHARACTER_CREATOR_TURN_LEFT(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, BOOL bBegin, BOOL bCleanupOpp, BOOL bCleanup)
	
	SEQUENCE_INDEX seq
	STRING strAnimDict = GET_CHARACTER_CONTROLLER_ANIM_DICT(ciCHARACTER_CREATOR_ANIM_DICT_CREATOR, 0, (characterData.eActiveChild = CHARACTER_CREATOR_SON))
	
	// If the ped is entering or cleaning up an animation wait until we know we are in this flow
//	IF IS_ENTITY_PLAYING_ANIM(characterData.characterCreatorPeds[characterData.eActiveChild], strAnimDict, "Profile_R_intro")
//	OR IS_ENTITY_PLAYING_ANIM(characterData.characterCreatorPeds[characterData.eActiveChild], strAnimDict, "Profile_R_outro")
//	OR IS_ENTITY_PLAYING_ANIM(characterData.characterCreatorPeds[characterData.eActiveChild], strAnimDict, "Profile_L_intro")
//	OR IS_ENTITY_PLAYING_ANIM(characterData.characterCreatorPeds[characterData.eActiveChild], strAnimDict, "Profile_L_outro")
//		RETURN FALSE
//	ENDIF
	
	IF bCleanup
		
		// If trigger tapped
		IF IS_ENTITY_PLAYING_ANIM(characterData.characterCreatorPeds[characterData.eActiveChild], strAnimDict, "Profile_L_Intro")
		AND NOT IS_ENTITY_PLAYING_ANIM(characterData.characterCreatorPeds[characterData.eActiveChild], strAnimDict, "Profile_L_Loop")	
				
			IF (GET_ENTITY_ANIM_CURRENT_TIME(characterData.characterCreatorPeds[characterData.eActiveChild], strAnimDict, "Profile_L_Intro") >= 0.40)
				OPEN_SEQUENCE_TASK(seq)
					TASK_PLAY_ANIM(NULL, strAnimDict, "Profile_L_Outro", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, DEFAULT, AF_TURN_OFF_COLLISION)
					TASK_PLAY_ANIM(NULL, strAnimDict, "Loop", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, DEFAULT, AF_LOOPING | AF_TURN_OFF_COLLISION)
				CLOSE_SEQUENCE_TASK(seq)	
			ELSE
				RETURN FALSE
			ENDIF
		
		// If trigger held
		ELIF IS_ENTITY_PLAYING_ANIM(characterData.characterCreatorPeds[characterData.eActiveChild], strAnimDict, "Profile_L_Loop")
		AND NOT IS_ENTITY_PLAYING_ANIM(characterData.characterCreatorPeds[characterData.eActiveChild], strAnimDict, "Profile_L_Intro")			
			OPEN_SEQUENCE_TASK(seq)
				TASK_PLAY_ANIM(NULL, strAnimDict, "Profile_L_Outro", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, DEFAULT, AF_TURN_OFF_COLLISION)
				TASK_PLAY_ANIM(NULL, strAnimDict, "Loop", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, DEFAULT, AF_LOOPING | AF_TURN_OFF_COLLISION)
			CLOSE_SEQUENCE_TASK(seq)
		ENDIF
		
	ELIF bBegin
		OPEN_SEQUENCE_TASK(seq)
		
			IF bCleanupOpp
				//TASK_PLAY_ANIM(NULL, strAnimDict, "Profile_R_Outro", FAST_BLEND_IN, FAST_BLEND_OUT, DEFAULT, AF_TURN_OFF_COLLISION)
			ENDIF
			
			TASK_PLAY_ANIM(NULL, strAnimDict, "Profile_L_Intro", SLOW_BLEND_IN, SLOW_BLEND_OUT, DEFAULT, AF_TURN_OFF_COLLISION)
			TASK_PLAY_ANIM(NULL, strAnimDict, "Profile_L_Loop", SLOW_BLEND_IN, SLOW_BLEND_OUT, DEFAULT, AF_LOOPING | AF_TURN_OFF_COLLISION)
		CLOSE_SEQUENCE_TASK(seq)
	ENDIF
	
	TASK_PERFORM_SEQUENCE(characterData.characterCreatorPeds[characterData.eActiveChild], seq)
	CLEAR_SEQUENCE_TASK(seq)
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Call to put the player into their outro animation ready for a photo to be taken
PROC SET_CHARACTER_CREATOR_PED_POSE_FOR_PHOTO(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData)
	
	IF IS_CHARACTER_CONTROLLER_PED_OK(characterData.characterCreatorPeds[characterData.eActiveChild])
	
		INT iRandomInt = GET_RANDOM_INT_IN_RANGE(0, 4)
		STRING strAnimDict = GET_CHARACTER_CONTROLLER_ANIM_DICT(ciCHARACTER_CREATOR_ANIM_DICT_CREATOR, 0, (characterData.eActiveChild = CHARACTER_CREATOR_SON))
		
		TEXT_LABEL_15 strAnim1 = "outro_"
		TEXT_LABEL_15 strAnim2 = "outro_loop_"
		SWITCH iRandomInt
			CASE 0 strAnim1 = "outro" strAnim2 = "outro_loop"	BREAK
			CASE 1 strAnim1 += "b"	strAnim2 += "b"				BREAK
			CASE 2 strAnim1 += "c"	strAnim2 += "c"				BREAK
			CASE 3 strAnim1 += "d"	strAnim2 += "d"				BREAK
		ENDSWITCH
		
		SEQUENCE_INDEX seq		
		OPEN_SEQUENCE_TASK(seq)
			TASK_PLAY_ANIM(NULL, strAnimDict, strAnim1, DEFAULT, DEFAULT, DEFAULT, AF_TURN_OFF_COLLISION)
			TASK_PLAY_ANIM(NULL, strAnimDict, strAnim2, DEFAULT, DEFAULT, DEFAULT, AF_LOOPING | AF_TURN_OFF_COLLISION)			
		CLOSE_SEQUENCE_TASK(seq)
		
		TASK_PERFORM_SEQUENCE(characterData.characterCreatorPeds[characterData.eActiveChild], seq)
		CLEAR_SEQUENCE_TASK(seq)
			
		iRandomInt = GET_RANDOM_INT_IN_RANGE(0, 2)
		STRING strMoodType
		
		SWITCH iRandomInt
			CASE 0 strMoodType = "mood_Angry_1"	BREAK
			CASE 1 strMoodType = "mood_Happy_1"	BREAK
		ENDSWITCH
		
		CPRINTLN(DEBUG_NET_CHARACTER, "SET_CHARACTER_CREATOR_PED_POSE_FOR_PHOTO - Setting the active ped to pose for photo: ", strMoodType)
		
		SET_FACIAL_IDLE_ANIM_OVERRIDE(characterData.characterCreatorPeds[characterData.eActiveChild], strMoodType)
		
		VECTOR vCameraCoords = GET_CAM_COORD(characterData.ciCharacterControllerCamera)
		TASK_LOOK_AT_COORD(characterData.characterCreatorPeds[characterData.eActiveChild], vCameraCoords, -1, SLF_USE_EYES_ONLY | SLF_WHILE_NOT_IN_FOV)
	ENDIF
ENDPROC

/// PURPOSE:
///    Clear the pose of the ped for the end photo
PROC CLEAR_CHARACTER_CREATOR_PED_POSE_FOR_PHOTO(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData)
	
	IF IS_CHARACTER_CONTROLLER_PED_OK(characterData.characterCreatorPeds[characterData.eActiveChild])
		CPRINTLN(DEBUG_NET_CHARACTER, "CLEAR_CHARACTER_CREATOR_PED_POSE_FOR_PHOTO - Clearing the ped posing for the photo")
		
		STRING strAnimDict = GET_CHARACTER_CONTROLLER_ANIM_DICT(ciCHARACTER_CREATOR_ANIM_DICT_CREATOR, 0, (characterData.eActiveChild = CHARACTER_CREATOR_SON))
		TASK_PLAY_ANIM(characterData.characterCreatorPeds[characterData.eActiveChild], strAnimDict, "Loop", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, DEFAULT, AF_LOOPING | AF_TURN_OFF_COLLISION)	
		
		TASK_CLEAR_LOOK_AT(characterData.characterCreatorPeds[characterData.eActiveChild])
		
		// Randomly set up back to a facial expression
		SET_CHARACTER_CREATOR_PED_MOOD(characterData.characterCreatorPeds[characterData.eActiveChild])
	ENDIF
ENDPROC


/// PURPOSE:
///    Start animating the board idling by the side for the clothes select screen
PROC SET_CHARACTER_CREATOR_PED_APPAREL_ANIMATION(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData)

	IF IS_CHARACTER_CONTROLLER_PED_OK(characterData.characterCreatorPeds[characterData.eActiveChild])
	
		STRING strAnimDict = GET_CHARACTER_CONTROLLER_ANIM_DICT(ciCHARACTER_CREATOR_ANIM_DICT_CREATOR, 0, (characterData.eActiveChild = CHARACTER_CREATOR_SON))
		
		SEQUENCE_INDEX seq
		
		// Play the animation loop
		OPEN_SEQUENCE_TASK(seq)
			TASK_PLAY_ANIM(NULL, strAnimDict, "DROP_INTRO", DEFAULT, DEFAULT, DEFAULT, AF_TURN_OFF_COLLISION)
			TASK_PLAY_ANIM(NULL, strAnimDict, "DROP_LOOP", DEFAULT, DEFAULT, DEFAULT, AF_LOOPING | AF_TURN_OFF_COLLISION)
		CLOSE_SEQUENCE_TASK(seq)
		
		TASK_PERFORM_SEQUENCE(characterData.characterCreatorPeds[characterData.eActiveChild], seq)
		CLEAR_SEQUENCE_TASK(seq)
		
		CLEAR_CHARACTER_CONTROLLER_BIT(ciCHARACTER_CONTROLLER_CLEAN_UP_ANIMATION)
		
		CPRINTLN(DEBUG_NET_CHARACTER, "SET_CHARACTER_CREATOR_PED_APPAREL_ANIMATION - Start ped playing animation for clothes screen")
	ENDIF
ENDPROC

/// PURPOSE:
///    Set the clothes animation to play on the current ped
PROC SET_CHARACTER_CREATOR_PED_CLOTHES_ANIM(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData)

	IF IS_CHARACTER_CONTROLLER_PED_OK(characterData.characterCreatorPeds[characterData.eActiveChild])
	
		STRING strAnimDict = GET_CHARACTER_CONTROLLER_ANIM_DICT(ciCHARACTER_CREATOR_ANIM_DICT_CREATOR, 0, (characterData.eActiveChild = CHARACTER_CREATOR_SON))
		STRING strAnim
		
		// Check which one we played last and change it up
		IF IS_STRING_NULL_OR_EMPTY(characterData.strLastPlayedClothingAnim)
			strAnim = PICK_STRING(GET_RANDOM_INT_IN_RANGE(0, 2) = 0, "DROP_CLOTHES_A", "DROP_CLOTHES_B")
		ELIF ARE_STRINGS_EQUAL(characterData.strLastPlayedClothingAnim, "DROP_CLOTHES_A")
			strAnim = PICK_STRING(GET_RANDOM_INT_IN_RANGE(0, 2) = 0, "DROP_CLOTHES_B", "DROP_CLOTHES_C")
		ELIF ARE_STRINGS_EQUAL(characterData.strLastPlayedClothingAnim, "DROP_CLOTHES_B")
			strAnim = PICK_STRING(GET_RANDOM_INT_IN_RANGE(0, 2) = 0, "DROP_CLOTHES_A", "DROP_CLOTHES_C")
		ELIF ARE_STRINGS_EQUAL(characterData.strLastPlayedClothingAnim, "DROP_CLOTHES_C")
			strAnim = PICK_STRING(GET_RANDOM_INT_IN_RANGE(0, 2) = 0, "DROP_CLOTHES_A", "DROP_CLOTHES_B")
		ENDIF
		
		characterData.strLastPlayedClothingAnim = strAnim
		
		SEQUENCE_INDEX seq
		
		OPEN_SEQUENCE_TASK(seq)
			
			TASK_PLAY_ANIM(NULL, strAnimDict, strAnim, DEFAULT, DEFAULT, DEFAULT, AF_TURN_OFF_COLLISION)
			TASK_PLAY_ANIM(NULL, strAnimDict, "DROP_LOOP", DEFAULT, DEFAULT, DEFAULT, AF_LOOPING | AF_TURN_OFF_COLLISION)
			
		CLOSE_SEQUENCE_TASK(seq)
		
		TASK_PERFORM_SEQUENCE(characterData.characterCreatorPeds[characterData.eActiveChild], seq)
		CLEAR_SEQUENCE_TASK(seq)
		
		CPRINTLN(DEBUG_NET_CHARACTER, "SET_CHARACTER_CREATOR_PED_CLOTHES_ANIM - Play the clothing animation for the ped, ", strAnim)
	ENDIF

ENDPROC


/// PURPOSE:
///    Maintain function to control the preloading of the hair and outfits
PROC MAINTAIN_CHARACTER_CREATOR_PRELOADING(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData)
	
	// Preloading - Randomised within main menu
	IF GET_CHARACTER_CREATOR_MENU_STATE(characterData) = ciCHARACTER_CREATOR_COLUMN_MAIN_MENU
		IF IS_CHARACTER_CONTROLLER_PED_OK(characterData.characterCreatorPeds[characterData.eActiveChild])
		
			// The hair has been randomised and can now be preloaded
			IF IS_CHARACTER_CONTROLLER_BIT_SET(ciCHARACTER_CONTROLLER_PRELOAD_HAIR)
				IF PRELOAD_CHARACTER_CREATOR_HAIR(characterData)
				
					IF NOT IS_CHARACTER_CONTROLLER_BIT_SET(ciCHARACTER_CONTROLLER_SET_SAFETY_TIMER)
						characterData.iSafetyTimer = GET_GAME_TIMER()
						SET_CHARACTER_CONTROLLER_BIT(ciCHARACTER_CONTROLLER_SET_SAFETY_TIMER)
					ENDIF
					
					IF HAS_PED_HEAD_BLEND_FINISHED(characterData.characterCreatorPeds[characterData.eActiveChild])
						
						CPRINTLN(DEBUG_NET_CHARACTER, "MAINTAIN_CHARACTER_CREATOR_PRELOADING - Main Menu Randomised - Head blend has finished, now applying hair/hair colour")
						SET_CHARACTER_CONTROLLER_BIT(ciCHARACTER_CONTROLLER_HAIR_HAS_PRELOADED)	
						APPLY_CHARACTER_CREATOR_HAIR(characterData, characterData.eActiveChild)
						CLEAR_CHARACTER_CONTROLLER_BIT(ciCHARACTER_CONTROLLER_PRELOAD_HAIR)
						CLEAR_CHARACTER_CONTROLLER_BIT(ciCHARACTER_CONTROLLER_SET_SAFETY_TIMER)
					ENDIF
					
					IF NOT HAS_PED_HEAD_BLEND_FINISHED(characterData.characterCreatorPeds[characterData.eActiveChild])
					AND ABSI(GET_GAME_TIMER() - characterData.iSafetyTimer) >= ciCHARACTER_CREATOR_SAFETY_HEAD_BLEND_TIMER
					
						CPRINTLN(DEBUG_NET_CHARACTER, "MAINTAIN_CHARACTER_CREATOR_PRELOADING - Main Menu Randomised - Head blend timed out, breaking out and applying hair")
						SET_CHARACTER_CONTROLLER_BIT(ciCHARACTER_CONTROLLER_HAIR_HAS_PRELOADED)	
						APPLY_CHARACTER_CREATOR_HAIR(characterData, characterData.eActiveChild)
						CLEAR_CHARACTER_CONTROLLER_BIT(ciCHARACTER_CONTROLLER_PRELOAD_HAIR)
						CLEAR_CHARACTER_CONTROLLER_BIT(ciCHARACTER_CONTROLLER_SET_SAFETY_TIMER)
					ENDIF
				ENDIF
			ENDIF				
			
			// The outfit has been randomised and can now be preloaded
			IF IS_CHARACTER_CONTROLLER_BIT_SET(ciCHARACTER_CONTROLLER_PRELOAD_OUTFIT)
				IF PRELOAD_CHARACTER_CREATOR_OUTFIT(characterData)
					
					SET_CHARACTER_CONTROLLER_BIT(ciCHARACTER_CONTROLLER_OUTFIT_HAS_PRELOADED)
					SET_CHARACTER_CONTROLLER_BIT(ciCHARACTER_CONTROLLER_IGNORE_HAIR_UNDERLAY)
					SET_CHARACTER_CREATOR_OUTFITS(characterData, characterData.eActiveChild, characterData.iCharacterSelectedSlot)
					CLEAR_CHARACTER_CONTROLLER_BIT(ciCHARACTER_CONTROLLER_PRELOAD_OUTFIT)	
				ENDIF
			ENDIF
			
			IF  IS_CHARACTER_CONTROLLER_BIT_SET(ciCHARACTER_CONTROLLER_HAIR_HAS_PRELOADED)
			AND IS_CHARACTER_CONTROLLER_BIT_SET(ciCHARACTER_CONTROLLER_OUTFIT_HAS_PRELOADED)
			
				// Release & Clear All
				RELEASE_PED_PRELOAD_VARIATION_DATA(characterData.characterCreatorPeds[characterData.eActiveChild])
				CLEAR_CHARACTER_CONTROLLER_BIT(ciCHARACTER_CONTROLLER_HAIR_HAS_PRELOADED)
				CLEAR_CHARACTER_CONTROLLER_BIT(ciCHARACTER_CONTROLLER_OUTFIT_HAS_PRELOADED)
				CLEAR_CHARACTER_CONTROLLER_BIT(ciCHARACTER_CONTROLLER_RANDOMISE_ALL)	
			ENDIF
		ENDIF
	ENDIF
	
	// Preloading - Processing changes in Apparel menu
	IF GET_CHARACTER_CREATOR_MENU_STATE(characterData) = ciCHARACTER_CREATOR_COLUMN_APPAREL
		IF IS_CHARACTER_CONTROLLER_PED_OK(characterData.characterCreatorPeds[characterData.eActiveChild])
		
			// Style & Outfit - Checks for crew tshirt as well
			IF IS_CHARACTER_CONTROLLER_BIT_SET(ciCHARACTER_CONTROLLER_PRELOAD_OUTFIT)
				IF PRELOAD_CHARACTER_CREATOR_OUTFIT(characterData)
					IF HAS_PED_HEAD_BLEND_FINISHED(characterData.characterCreatorPeds[characterData.eActiveChild])
						
						GAMER_HANDLE aGamerHandle = GET_LOCAL_GAMER_HANDLE()			
						IF IS_PLAYER_IN_ACTIVE_CLAN(aGamerHandle)
							IF GET_CHARACTER_CREATOR_CREW_TSHIRT(characterData) != CREW_TSHIRT_OFF
								SET_CHARACTER_CONTROLLER_BIT(ciCHARACTER_CONTROLLER_IGNORE_HAIR_UNDERLAY)
								APPLY_CHARACTER_CREATOR_CREW_TSHIRT(characterData)
							ELSE
								SET_CHARACTER_CONTROLLER_BIT(ciCHARACTER_CONTROLLER_IGNORE_HAIR_UNDERLAY)
								SET_CHARACTER_CREATOR_OUTFITS(characterData, characterData.eActiveChild, characterData.iCharacterSelectedSlot)
							ENDIF
						ELSE
							SET_CHARACTER_CONTROLLER_BIT(ciCHARACTER_CONTROLLER_IGNORE_HAIR_UNDERLAY)
							SET_CHARACTER_CREATOR_OUTFITS(characterData, characterData.eActiveChild, characterData.iCharacterSelectedSlot)
						ENDIF
						
						RELEASE_PED_PRELOAD_VARIATION_DATA(characterData.characterCreatorPeds[characterData.eActiveChild])
						CLEAR_CHARACTER_CONTROLLER_BIT(ciCHARACTER_CONTROLLER_PRELOAD_OUTFIT)	
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	// Spam until head blend is complete - clear and apply new decals
	IF IS_CHARACTER_CONTROLLER_BIT_SET(ciCHARACTER_CONTROLLER_CLEAR_DECAL)
		IF HAS_PED_HEAD_BLEND_FINISHED(characterData.characterCreatorPeds[characterData.eActiveChild])
		
			#IF NOT USE_TU_CHANGES
			CLEAR_PED_DECORATIONS(characterData.characterCreatorPeds[characterData.eActiveChild])
			#ENDIF
			#IF USE_TU_CHANGES
			CLEAR_PED_DECORATIONS_LEAVE_SCARS(characterData.characterCreatorPeds[characterData.eActiveChild])
			#ENDIF
			
			UPDATE_CHARACTER_CREATOR_PED_DECAL(characterData, characterData.eActiveChild)
			
			IF GET_CHARACTER_CREATOR_CREW_TSHIRT(characterData) != CREW_TSHIRT_OFF
				APPLY_CHARACTER_CREATOR_CREW_TSHIRT_DECAL(characterData)
			ENDIF
			CLEAR_CHARACTER_CONTROLLER_BIT(ciCHARACTER_CONTROLLER_CLEAR_DECAL)
		ENDIF
	ENDIF
	
	// Preloading - Processing changes in Apperance menu
	IF GET_CHARACTER_CREATOR_MENU_STATE(characterData) = ciCHARACTER_CREATOR_COLUMN_APPEARANCE
		IF IS_CHARACTER_CONTROLLER_PED_OK(characterData.characterCreatorPeds[characterData.eActiveChild])

			// Hair
			IF IS_CHARACTER_CONTROLLER_BIT_SET(ciCHARACTER_CONTROLLER_PRELOAD_HAIR)
				IF HAS_PED_HEAD_BLEND_FINISHED(characterData.characterCreatorPeds[characterData.eActiveChild])
					IF PRELOAD_CHARACTER_CREATOR_HAIR(characterData)
						APPLY_CHARACTER_CREATOR_HAIR(characterData, characterData.eActiveChild)
						RELEASE_PED_PRELOAD_VARIATION_DATA(characterData.characterCreatorPeds[characterData.eActiveChild])
						CLEAR_CHARACTER_CONTROLLER_BIT(ciCHARACTER_CONTROLLER_PRELOAD_HAIR)
					ENDIF
				ENDIF
			ENDIF
		ENDIF	
	ENDIF
	
ENDPROC


/// PURPOSE:
///    Control function to play random / ambient animations on the ped depending on what they are doing
PROC MAINTAIN_CHARACTER_CREATOR_PED_ANIMATIONS(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData)

	IF IS_CHARACTER_CONTROLLER_BIT_SET(ciCHARACTER_CONTROLLER_CLOTHES_CHANGED)
		
		IF IS_CHARACTER_CONTROLLER_PED_OK(characterData.characterCreatorPeds[characterData.eActiveChild])
		
			STRING strAnimDict = GET_CHARACTER_CONTROLLER_ANIM_DICT(ciCHARACTER_CREATOR_ANIM_DICT_CREATOR, 0, (characterData.eActiveChild = CHARACTER_CREATOR_SON))
			
			// Are we on the correct menu and haven't played one for some time already
			IF GET_CHARACTER_CREATOR_MENU_STATE(characterData) = ciCHARACTER_CREATOR_COLUMN_APPAREL
			AND GET_GAME_TIMER() >= characterData.iCharacterCreatorClothesTimer
			AND IS_ENTITY_PLAYING_ANIM(characterData.characterCreatorPeds[characterData.eActiveChild], strAnimDict, "DROP_LOOP")
					
				INT iRandomChance = GET_RANDOM_INT_IN_RANGE(0, ciCHARACTER_CREATOR_CLOTHES_RANDOM_CHANCE)
				
				IF iRandomChance = 0
					
					CPRINTLN(DEBUG_NET_CHARACTER, "MAINTAIN_CHARACTER_CREATOR_PED_ANIMATIONS - Play a clothes animation for the ped, game time: ", GET_GAME_TIMER())
					
					characterData.iCharacterCreatorClothesTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(ciCHARACTER_CREATOR_CLOTHES_TIMER_START, ciCHARACTER_CREATOR_CLOTHES_TIMER_END)
					SET_CHARACTER_CREATOR_PED_CLOTHES_ANIM(characterData)
					CLEAR_CHARACTER_CONTROLLER_BIT(ciCHARACTER_CONTROLLER_CLOTHES_CHANGED)
				ELSE
					CLEAR_CHARACTER_CONTROLLER_BIT(ciCHARACTER_CONTROLLER_CLOTHES_CHANGED)
				ENDIF
			ELSE
				CLEAR_CHARACTER_CONTROLLER_BIT(ciCHARACTER_CONTROLLER_CLOTHES_CHANGED)
			ENDIF
		ENDIF
	ENDIF
	
	// Check if we need to exit
	IF IS_CHARACTER_CONTROLLER_BIT_SET(ciCHARACTER_CONTROLLER_CLEAN_UP_ANIMATION)
	
		IF IS_CHARACTER_CONTROLLER_PED_OK(characterData.characterCreatorPeds[characterData.eActiveChild])
			STRING strAnimDict = GET_CHARACTER_CONTROLLER_ANIM_DICT(ciCHARACTER_CREATOR_ANIM_DICT_CREATOR, 0, (characterData.eActiveChild = CHARACTER_CREATOR_SON))
			IF IS_ENTITY_PLAYING_ANIM(characterData.characterCreatorPeds[characterData.eActiveChild], strAnimDict, "DROP_LOOP")
								
				SEQUENCE_INDEX seq
				
				OPEN_SEQUENCE_TASK(seq)
					// Replace with the outro and then normal loop
					TASK_PLAY_ANIM(NULL, strAnimDict, "DROP_OUTRO", DEFAULT, DEFAULT, DEFAULT, AF_TURN_OFF_COLLISION)
					TASK_PLAY_ANIM(NULL, strAnimDict, "Loop", DEFAULT, DEFAULT, DEFAULT, AF_LOOPING | AF_TURN_OFF_COLLISION)
				CLOSE_SEQUENCE_TASK(seq)
				
				TASK_PERFORM_SEQUENCE(characterData.characterCreatorPeds[characterData.eActiveChild], seq)
				CLEAR_SEQUENCE_TASK(seq)
				
				CPRINTLN(DEBUG_NET_CHARACTER, "MAINTAIN_CHARACTER_CREATOR_PED_ANIMATIONS - Force the ped out of the lowered board anim")
				
				// Clear our bit as we are out
				CLEAR_CHARACTER_CONTROLLER_BIT(ciCHARACTER_CONTROLLER_CLEAN_UP_ANIMATION)
			ENDIF
					
		ENDIF	
	ENDIF

ENDPROC




/// PURPOSE:
///    This processes the select input to go into a menu or change an option
PROC PROCESS_CHARCTER_CREATOR_SELECT(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData)
	
	SWITCH GET_CHARACTER_CREATOR_MENU_STATE(characterData)
		
		CASE ciCHARACTER_CREATOR_COLUMN_MAIN_MENU
			
			SWITCH GET_CHARACTER_CREATOR_CURRENT_SELECTION(characterData)

				CASE ciCHARACTER_CREATOR_MAIN_MENU_HERITAGE
					POPULATE_CHARACTER_CREATOR_HERITAGE_MENU(characterData)
					
					// Move our camera in
					SET_CHARACTER_CONTROLLER_CAMERA_ACTIVE(characterData, CHARACTER_CONTROLLER_CAMERA_CREATOR_MUGSHOT_ZOOMED)
				BREAK
				
				CASE ciCHARACTER_CREATOR_MAIN_MENU_FEATURES
					SET_CHARACTER_CONTROLLER_BIT(ciCHARACTER_CONTROLLER_PULSE_FEATURE)
					POPULATE_CHARACTER_CREATOR_FEATURES_MENU(characterData)
					
					// Move our camera in
					SET_CHARACTER_CONTROLLER_CAMERA_ACTIVE(characterData, CHARACTER_CONTROLLER_CAMERA_CREATOR_MUGSHOT_ZOOMED)
				BREAK
				
				CASE ciCHARACTER_CREATOR_MAIN_MENU_STATS
					POPULATE_CHARACTER_CREATOR_STATS_MENU(characterData)
				BREAK
				
				CASE ciCHARACTER_CREATOR_MAIN_MENU_APPEARANCE
					POPULATE_CHARACTER_CREATOR_APPEARANCE_MENU(characterData)
					
					// Move our camera in
					SET_CHARACTER_CONTROLLER_CAMERA_ACTIVE(characterData, CHARACTER_CONTROLLER_CAMERA_CREATOR_MUGSHOT_ZOOMED)
				BREAK
				
				CASE ciCHARACTER_CREATOR_MAIN_MENU_APPAREL
					POPULATE_CHARACTER_CREATOR_APPAREL_MENU(characterData)
					
					// Switch our animation
					SET_CHARACTER_CREATOR_PED_APPAREL_ANIMATION(characterData)				
				BREAK
				
				CASE ciCHARACTER_CREATOR_MAIN_MENU_CONTINUE
				
					IF GET_CHARACTER_CREATOR_STATS_LEFT_TO_ASSIGN(characterData) > 0
					AND NOT HAS_ENTERED_CHAR_CREATION_FROM_ALTERATION_PROMPT()
						SET_CHARACTER_CONTROLLER_BIT(ciCHARACTER_CONTROLLER_STATS_TO_ASSIGN)
						SET_CHARACTER_CREATOR_STATE(CHARACTER_CREATOR_STATE_WARNING)
					ELSE
						DISPLAY_CHARACTER_CREATOR_COLUMN(-1)
					
						SET_CHARACTER_CREATOR_STATE(CHARACTER_CREATOR_STATE_NAME_ENTRY)
						UPDATE_CHARACTER_CONTROLLER_BUTTONS(characterData)
					ENDIF
				BREAK
				
			ENDSWITCH
			
		BREAK
		
	ENDSWITCH
	
ENDPROC

/// PURPOSE:
///    This processes the cacnel input to go into a menu or change an option
PROC PROCESS_CHARCTER_CREATOR_CANCEL(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData)
	
	SWITCH GET_CHARACTER_CREATOR_MENU_STATE(characterData)
		
		CASE ciCHARACTER_CREATOR_COLUMN_HERITAGE
		CASE ciCHARACTER_CREATOR_COLUMN_FEATURES
		CASE ciCHARACTER_CREATOR_COLUMN_STATS
		CASE ciCHARACTER_CREATOR_COLUMN_APPEARANCE
			
			IF GET_CHARACTER_CREATOR_MENU_STATE(characterData) = ciCHARACTER_CREATOR_COLUMN_APPAREL
				CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARCTER_CREATOR_CANCEL - Flag that the player should exit the apparel anim")
				SET_CHARACTER_CONTROLLER_BIT(ciCHARACTER_CONTROLLER_CLEAN_UP_ANIMATION)
			ENDIF
			
			// Make sure we now update both peds with latest settings
			IF GET_CHARACTER_CREATOR_MENU_STATE(characterData) = ciCHARACTER_CREATOR_COLUMN_APPEARANCE
				APPLY_CHARACTER_CREATOR_OVERLAY(characterData, TRUE)
			ENDIF
			
			POPULATE_CHARACTER_CREATOR_MAIN_MENU(characterData)
			
			// Move our camera back
			SET_CHARACTER_CONTROLLER_CAMERA_ACTIVE(characterData, CHARACTER_CONTROLLER_CAMERA_CREATOR_OVERVIEW)
		BREAK
		
		CASE ciCHARACTER_CREATOR_COLUMN_APPAREL
			
			IF GET_CHARACTER_CREATOR_MENU_STATE(characterData) = ciCHARACTER_CREATOR_COLUMN_APPAREL
				CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARCTER_CREATOR_CANCEL - Flag that the player should exit the apparel anim")
				SET_CHARACTER_CONTROLLER_BIT(ciCHARACTER_CONTROLLER_CLEAN_UP_ANIMATION)
				
				// Ensure hidden ped is only wearing a crew tshirt is visible ped is
				IF GET_CHARACTER_CREATOR_CREW_TSHIRT(characterData) = CREW_TSHIRT_OFF
					IF characterData.eActiveChild = CHARACTER_CREATOR_SON
						SET_CHARACTER_CREATOR_OUTFITS(characterData, CHARACTER_CREATOR_DAUGHTER, characterData.iCharacterSelectedSlot)
					ELSE
						SET_CHARACTER_CREATOR_OUTFITS(characterData, CHARACTER_CREATOR_SON, characterData.iCharacterSelectedSlot)
					ENDIF
				ENDIF
			ENDIF
			
			// Make sure we now update both peds with latest settings
			IF GET_CHARACTER_CREATOR_MENU_STATE(characterData) = ciCHARACTER_CREATOR_COLUMN_APPEARANCE
				APPLY_CHARACTER_CREATOR_OVERLAY(characterData, TRUE)
			ENDIF
			
			POPULATE_CHARACTER_CREATOR_MAIN_MENU(characterData)
			
			// Move our camera back
			SET_CHARACTER_CONTROLLER_CAMERA_ACTIVE(characterData, CHARACTER_CONTROLLER_CAMERA_CREATOR_OVERVIEW)	
		BREAK
		
		CASE ciCHARACTER_CREATOR_COLUMN_MAIN_MENU
			IF HAS_ENTERED_CHAR_CREATION_FROM_ALTERATION_PROMPT()
			#IF FEATURE_GEN9_STANDALONE
			OR GET_CHOSEN_MP_CHARACTER_SLOT() >= 0
			#ENDIF
				SET_CHARACTER_CONTROLLER_BIT(ciCHARACTER_CONTROLLER_QUIT_WARNING_BIT)
				
				SET_CHARACTER_CREATOR_STATE(CHARACTER_CREATOR_STATE_WARNING)
			ELSE
				SET_CHARACTER_CREATOR_STATE(CHARACTER_CREATOR_STATE_WARNING)
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Handle the player hitting triangle on the menu
PROC PROCESS_CHARACTER_CREATOR_TRIANGLE(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData)
	
	SWITCH GET_CHARACTER_CREATOR_MENU_STATE(characterData)
	
		CASE ciCHARACTER_CREATOR_COLUMN_STATS
			
			IF NOT HAS_ENTERED_CHAR_CREATION_FROM_ALTERATION_PROMPT()
				RANDOMISE_CHARACTER_CREATOR_STATS(characterData, TRUE)
				
				PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
				
				POPULATE_CHARACTER_CREATOR_STATS_MENU(characterData, TRUE)
			ENDIF
		BREAK
	
	ENDSWITCH
	
ENDPROC

/// PURPOSE:
///    Processes mouse clicks for trigger input.
/// PARAMS:
///    characterData - 
///    iIncrement - 
/// RETURNS:
///    
FUNC BOOL PROCESS_CHARACTER_CREATOR_MOUSE_TRIGGER_INPUT(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData)
	
	IF NOT IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		RETURN FALSE
	ENDIF
	
	IF GET_CHARACTER_CREATOR_MENU_STATE(characterData) != ciCHARACTER_CREATOR_COLUMN_APPEARANCE
		RETURN FALSE
	ENDIF 
		
	INT iSelection = GET_CHARACTER_CREATOR_CURRENT_SELECTION(characterData)

	CHARACTER_CREATOR_APPEARANCE_DATA appearanceData
	CHARACTER_CREATOR_APPEARANCE_VALUES appearanceValues
	
	GET_CHARACTER_CREATOR_APPEARANCE_DATA(appearanceData, iSelection)
	GET_CHARACTER_CREATOR_APPEARANCE_VALUES(characterData, appearanceValues, iSelection)
				
	// Only update if the colour option is active
	IF NOT appearanceData.bColourActive
		RETURN FALSE
	ENDIF
	
	IF appearanceValues.iValue = ciCHARACTER_CREATOR_EXISTING_INDEX
		RETURN FALSE
	ENDIF

	// Mouse click on colour bar. 
	IF PAUSE_MENU_GET_HAIR_COLOUR_INDEX() != -1
		
		IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
			RETURN TRUE
		ENDIF
				
	ENDIF

	//  Detect the mouse in the click areas
		
	IF IS_MOUSE_IN_PED_COLOUR_BAR_AREA(TRUE, appearanceData.bOpacityActive)
		
		IF IS_CONTROL_JUST_PRESSED( FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_UP)
			SET_CONTROL_VALUE_NEXT_FRAME(FRONTEND_CONTROL, INPUT_FRONTEND_LT, 1)
			RETURN FALSE
		ENDIF

		IF IS_CONTROL_JUST_PRESSED( FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_DOWN)
			SET_CONTROL_VALUE_NEXT_FRAME(FRONTEND_CONTROL, INPUT_FRONTEND_RT, 1)
			RETURN FALSE
		ENDIF
	
	ENDIF
	
	INT iMouseIncrement = GET_MOUSE_PED_COMPONENT_COLOUR_INCREMENT(TRUE, appearanceData.bOpacityActive)

	// Click in area
	// False returned because we're faking inputs which get processed by the gamepad code.
	IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
		IF iMouseIncrement = -1
			SET_CONTROL_VALUE_NEXT_FRAME(FRONTEND_CONTROL, INPUT_FRONTEND_LT, 1)
			RETURN FALSE
		ELIF  iMouseIncrement = 1
			SET_CONTROL_VALUE_NEXT_FRAME(FRONTEND_CONTROL, INPUT_FRONTEND_RT, 1)
			RETURN FALSE
		ENDIF
	ENDIF
	
		
	RETURN FALSE
					
ENDFUNC


/// PURPOSE:
///    Listen for input on the trigger buttons
FUNC BOOL PROCESS_CHARACTER_CREATOR_TRIGGER_INPUT(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, INT &iIncrement)
	IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RT)
	OR IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_RT)
		
		IF ABSI(TIMERA() - characterData.iCharacterControllerTimer) > 300
		
			iIncrement = 1
			characterData.iCharacterControllerTimer = TIMERA()
		
			RETURN TRUE
		ENDIF
	ELIF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LT)
	OR IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_LT)
	
		IF ABSI(TIMERA() - characterData.iCharacterControllerTimer) > 300
			
			iIncrement = -1
			characterData.iCharacterControllerTimer = TIMERA()
			
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    This randomly picks N features to randomise and apply to the current ped
PROC RANDOMISE_CHARACTER_CREATOR_FEATURES(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData)
	
	CHARACTER_CREATOR_FEATURE_DATA featureData
	CHARACTER_CREATOR_FEATURE_VALUES featureValues
		
	INT i
	INT iPreset
	FLOAT fRandomX
	FLOAT fRandomY
	INT iRandomFeature
	CHARACTER_CREATOR_FEATURE_GENDER aGender = CHARACTER_CREATOR_GENDER_FEMALE
	CHARACTER_CREATOR_FEATURE_GENDER aSecondaryGender = CHARACTER_CREATOR_GENDER_FEMALE_HIDE
	
	IF GET_CHARACTER_CREATOR_ACTIVE_PED_IS_MALE(characterData)
		aGender = CHARACTER_CREATOR_GENDER_MALE
		aSecondaryGender = CHARACTER_CREATOR_GENDER_MALE_HIDE
	ENDIF
	
	CPRINTLN(DEBUG_NET_CHARACTER, "RANDOMISE_CHARACTER_CREATOR_FEATURES - Randomise features")
	
	REPEAT ciCHARACTER_CREATOR_MAX_FEATURES i
	
		iRandomFeature = GET_RANDOM_INT_IN_RANGE(0, ciCHARACTER_CREATOR_MAX_FEATURES)
		
		GET_CHARACTER_CREATOR_FEATURE_DATA(featureData, iRandomFeature)
		GET_CHARACTER_CREATOR_FEATURE_VALUES(characterData, featureValues, iRandomFeature)
	
		IF featureData.iGenderSpecific = aGender
		OR featureData.iGenderSpecific = aSecondaryGender
		OR featureData.iGenderSpecific = CHARACTER_CREATOR_GENDER_BOTH
	
			fRandomX = featureValues.fXBlend
			fRandomY = featureValues.fYBlend
			
			IF featureData.bXAxisActive
				fRandomX = GET_RANDOM_FLOAT_IN_RANGE(0.0, 100.0)
			ENDIF
			
			IF featureData.bYAxisActive
				fRandomY = GET_RANDOM_FLOAT_IN_RANGE(0.0, 100.0)
			ENDIF
			
			iPreset = -1
			SET_CHARACTER_CREATOR_FEATURE_VALUES(characterData, iRandomFeature, iPreset, fRandomX, fRandomY)
			APPLY_CHARACTER_CREATOR_MICRO_MORPH(characterData, iRandomFeature)
		ENDIF
	ENDREPEAT
ENDPROC

PROC RANDOMISE_CHARACTER_CREATOR_OVERLAY(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, INT iOverlay)
	
	CHARACTER_CREATOR_FEATURE_GENDER aGender = CHARACTER_CREATOR_GENDER_FEMALE
	CHARACTER_CREATOR_FEATURE_GENDER aSecondaryGender = CHARACTER_CREATOR_GENDER_FEMALE_HIDE
	
	IF GET_CHARACTER_CREATOR_ACTIVE_PED_IS_MALE(characterData)
		aGender = CHARACTER_CREATOR_GENDER_MALE
		aSecondaryGender = CHARACTER_CREATOR_GENDER_MALE_HIDE
	ENDIF
	
	CHARACTER_CREATOR_APPEARANCE_DATA appearanceData
	CHARACTER_CREATOR_APPEARANCE_VALUES appearanceValues
	
	GET_CHARACTER_CREATOR_APPEARANCE_DATA(appearanceData, iOverlay)
	GET_CHARACTER_CREATOR_APPEARANCE_VALUES(characterData, appearanceValues, iOverlay)
	
	IF appearanceData.iGenderSpecific = aGender
	OR appearanceData.iGenderSpecific = aSecondaryGender
	OR appearanceData.iGenderSpecific = CHARACTER_CREATOR_GENDER_BOTH
	
		IF appearanceData.bOverlayActive
			
			INT iRandomInt = GET_RANDOM_INT_IN_RANGE(appearanceData.iValueMin, appearanceData.iValueMax)
			FLOAT fRandomBlend = GET_RANDOM_FLOAT_IN_RANGE(appearanceData.fOpacityMin, appearanceData.fOpacityMax)
			
			IF IS_CHARACTER_CREATOR_OVERLAY_VALUE_VALID(appearanceData.overlaySlot, iRandomInt)
			
				INT	iInitialTint = GET_RANDOM_INT_IN_RANGE(0, appearanceData.iColourMax) 
				INT iRandomColour = iInitialTint
				
				BOOL bFound
				BOOL bScrollToValid
				INT iCount
				INT iHighlight
				
				WHILE NOT bFound
					
					// Safety catch if we haven't found a valid colour in N attempts
					IF bScrollToValid
						iRandomColour++
						
						IF iRandomColour >= appearanceData.iColourMax
							iRandomColour = 0
						ENDIF
					ELSE
						// Randomly pick
						iRandomColour = GET_RANDOM_INT_IN_RANGE(0, appearanceData.iColourMax) 
					ENDIF
					
					// Check if they are valid
					IF IS_CHARACTER_CREATOR_TINT_VALID(appearanceData.overlaySlot, iRandomColour)
						bFound = TRUE
					ELSE
						// If we have completely looped then we don't have any valid colours so bail
						IF bScrollToValid
						AND iRandomColour = iInitialTint
							bFound = TRUE
							iRandomColour = 0
						ENDIF
					ENDIF
					
					iCount++
					IF NOT bScrollToValid
					AND iCount > appearanceData.iColourMax
						bScrollToValid = TRUE
						iInitialTint = iRandomColour
					ENDIF
				ENDWHILE
				
				iHighlight = GET_CHARACTER_CREATOR_COLOUR_INDEX(appearanceData.overlaySlot, iRandomColour, appearanceData.iColourMax)
			
				SET_CHARACTER_CREATOR_APPEARANCE_VALUES(characterData, iOverlay, iRandomInt, fRandomBlend, iRandomColour, iRandomColour, iHighlight)
				
				// Set Makeup & Lipstick on males to 'None' when Main Menu randomising (B* 2159863)
				IF GET_CHARACTER_CREATOR_MENU_STATE(characterData) = ciCHARACTER_CREATOR_COLUMN_MAIN_MENU
					IF GET_CHARACTER_CREATOR_ACTIVE_PED_IS_MALE(characterData)
					
						appearanceData.overlaySlot = HOS_MAKEUP
						iOverlay = ciCHARACTER_CREATOR_APPEARANCE_EYE_MAKEUP_M
						SET_CHARACTER_CREATOR_APPEARANCE_VALUES(characterData, iOverlay, -1, 0.0, 0, 0, 0)
						
						appearanceData.overlaySlot = HOS_SKIN_DETAIL_1
						iOverlay = ciCHARACTER_CREATOR_APPEARANCE_LIPSTICK_M
						SET_CHARACTER_CREATOR_APPEARANCE_VALUES(characterData, iOverlay, -1, 0.0, 0, 0, 0)
					ENDIF
				ENDIF

				// Update our body overlays
				UPDATE_CHARACTER_CREATOR_BODY_OVERLAYS(characterData, iOverlay, characterData.eActiveChild)
			ENDIF
		ELSE
			SWITCH iOverlay	

				CASE ciCHARACTER_CREATOR_APPEARANCE_HAIR_STYLE_F
				CASE ciCHARACTER_CREATOR_APPEARANCE_HAIR_STYLE_M
					IF GET_CHARACTER_CREATOR_MENU_STATE(characterData) != ciCHARACTER_CREATOR_COLUMN_MAIN_MENU
					    // Randomise the hair and colours
					    RANDOMISE_CHARACTER_HAIR_AND_HAIR_COLOUR(characterData)
					
					    IF IS_CHARACTER_CONTROLLER_BIT_SET(ciCHARACTER_CONTROLLER_PRELOAD_HAIR)
							IF IS_CHARACTER_CONTROLLER_PED_OK(characterData.characterCreatorPeds[characterData.eActiveChild])
								RELEASE_PED_PRELOAD_VARIATION_DATA(characterData.characterCreatorPeds[characterData.eActiveChild])
							ENDIF
						ELSE
							SET_CHARACTER_CONTROLLER_BIT(ciCHARACTER_CONTROLLER_PRELOAD_HAIR)
						ENDIF
					ENDIF
				BREAK
			
				CASE ciCHARACTER_CREATOR_APPEARANCE_EYES
					RANDOMISE_CHARACTER_EYE_COLOUR(characterData)
				BREAK	
			ENDSWITCH
		ENDIF

	ENDIF
ENDPROC

/// PURPOSE:
///    Randomise N overlays on the current peds
PROC RANDOMISE_CHARACTER_CREATOR_OVERLAYS(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData)
	
	INT i
	INT iRandomOverlay
	
	CPRINTLN(DEBUG_NET_CHARACTER, "RANDOMISE_CHARACTER_CREATOR_OVERLAYS - Randomise appearances")
	
	REPEAT ciCHARACTER_CREATOR_MAX_APPEARANCE i
	
		iRandomOverlay = GET_RANDOM_INT_IN_RANGE(0, ciCHARACTER_CREATOR_MAX_APPEARANCE)
		
		RANDOMISE_CHARACTER_CREATOR_OVERLAY(characterData, iRandomOverlay)
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Randomize an element of the character creator if valid
PROC PROCESS_CHARACTER_CREATOR_RANDOMIZE(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData)

	SWITCH GET_CHARACTER_CREATOR_MENU_STATE(characterData)
	
		CASE ciCHARACTER_CREATOR_COLUMN_MAIN_MENU
			
			SET_CHARACTER_CONTROLLER_BIT(ciCHARACTER_CONTROLLER_RANDOMISE_ALL)
			
			CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_CREATOR_RANDOMIZE - Randomising everything from the main menu")
			
			// Randomise our parents
			SET_CHARACTER_CREATOR_PARENT_RANDOM_HEAD(characterData, CHARACTER_CREATOR_MUM)
			SET_CHARACTER_CREATOR_PARENT_RANDOM_HEAD(characterData, CHARACTER_CREATOR_DAD)
			APPLY_CHARACTER_CREATOR_PARENT_HEAD_CHANGE(characterData, TRUE)
			
			// Then randomise our dominance and skin blend
			RANDOMISE_CHARACTER_CREATOR_DOM(characterData, characterData.eActiveChild, FALSE)
			RANDOMISE_CHARACTER_CREATOR_SKIN_TONE(characterData, characterData.eActiveChild, FALSE)
			APPLY_CHARACTER_CREATOR_DOMINANCE_SKIN_CHANGE(characterData)
			
			// Randomise our stats
			RANDOMISE_CHARACTER_CREATOR_STATS(characterData)
			
			// Randomise some of our features
			RANDOMISE_CHARACTER_CREATOR_FEATURES(characterData)
			
			// Randomise some of our overlays
			RANDOMISE_CHARACTER_CREATOR_OVERLAYS(characterData)
			APPLY_CHARACTER_CREATOR_OVERLAY(characterData, FALSE)
			
			APPLY_CHARACTER_CREATOR_EYE_COLOUR(characterData)
			
			// Randomise our hair and hair colour
			RANDOMISE_CHARACTER_HAIR_AND_HAIR_COLOUR(characterData, FALSE, TRUE, characterData.eActiveChild)
			
			// Randomise out style and outfit
			RANDOMISE_CHARACTER_CREATOR_STYLE(characterData)
			RANDOMISE_CHARACTER_CREATOR_OUTFIT(characterData)
			
			IF GET_RANDOM_INT_IN_RANGE(0, 5) = 0
				RANDOMISE_CHARCTER_CREATOR_HAT(characterData)
			ELSE
				SET_CHARACTER_CREATOR_CHILD_HAT(characterData, characterData.eActiveChild, -1)
			ENDIF
			APPLY_CHARACTER_CREATOR_HAT(characterData)
			
			IF GET_RANDOM_INT_IN_RANGE(0, 5) = 0
				RANDOMISE_CHARCTER_CREATOR_GLASSES(characterData)
			ELSE
				SET_CHARACTER_CREATOR_CHILD_GLASSES(characterData, characterData.eActiveChild, -1)	
			ENDIF
			APPLY_CHARACTER_CREATOR_GLASSES(characterData)
			
			PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
		BREAK	
	
		CASE ciCHARACTER_CREATOR_COLUMN_HERITAGE
			
			SWITCH GET_CHARACTER_CREATOR_CURRENT_SELECTION(characterData)
				CASE ciCHARACTER_CREATOR_HERITAGE_MUM_NAME
					SET_CHARACTER_CREATOR_PARENT_RANDOM_HEAD(characterData, CHARACTER_CREATOR_MUM)
					APPLY_CHARACTER_CREATOR_PARENT_HEAD_CHANGE(characterData, TRUE)
				BREAK
				
				CASE ciCHARACTER_CREATOR_HERITAGE_DAD_NAME
					SET_CHARACTER_CREATOR_PARENT_RANDOM_HEAD(characterData, CHARACTER_CREATOR_DAD)
					APPLY_CHARACTER_CREATOR_PARENT_HEAD_CHANGE(characterData, TRUE)
					APPLY_CHARACTER_CREATOR_OVERLAY(characterData, FALSE)
				BREAK
				
				CASE ciCHARACTER_CREATOR_HERITAGE_DOM
					RANDOMISE_CHARACTER_CREATOR_DOM(characterData, characterData.eActiveChild, TRUE)
					APPLY_CHARACTER_CREATOR_DOMINANCE_SKIN_CHANGE(characterData)
				BREAK
				
				CASE ciCHARACTER_CREATOR_HERITAGE_SKIN_TONE
					RANDOMISE_CHARACTER_CREATOR_SKIN_TONE(characterData, characterData.eActiveChild, TRUE)
					APPLY_CHARACTER_CREATOR_DOMINANCE_SKIN_CHANGE(characterData)
				BREAK
			ENDSWITCH
			
			PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
			
			POPULATE_CHARACTER_CREATOR_HERITAGE_MENU(characterData, TRUE)
		BREAK
		
		CASE ciCHARACTER_CREATOR_COLUMN_STATS
			
			IF NOT HAS_ENTERED_CHAR_CREATION_FROM_ALTERATION_PROMPT()
				RANDOMISE_CHARACTER_CREATOR_STATS(characterData)
			
				PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
			
				POPULATE_CHARACTER_CREATOR_STATS_MENU(characterData, TRUE)
			ENDIF
		BREAK
		
		CASE ciCHARACTER_CREATOR_COLUMN_FEATURES
			
			// Get the data for the feature
			CHARACTER_CREATOR_FEATURE_DATA featureData
			GET_CHARACTER_CREATOR_FEATURE_DATA(featureData, GET_CHARACTER_CREATOR_CURRENT_SELECTION(characterData))
			
			CHARACTER_CREATOR_FEATURE_VALUES featureValues
			GET_CHARACTER_CREATOR_FEATURE_VALUES(characterData, featureValues, GET_CHARACTER_CREATOR_CURRENT_SELECTION(characterData))
			
			INT iPreset
			FLOAT fRandomX
			FLOAT fRandomY
			
			fRandomX = featureValues.fXBlend
			fRandomY = featureValues.fYBlend
			
			IF featureData.bXAxisActive
				fRandomX = GET_RANDOM_FLOAT_IN_RANGE(0.0, 100.0)
			ENDIF
			
			IF featureData.bYAxisActive
				fRandomY = GET_RANDOM_FLOAT_IN_RANGE(0.0, 100.0)
			ENDIF
			
			iPreset = -1
			
			CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_CREATOR_RANDOMIZE - Player has randomized feature to: iPreset = ", iPreset, ", X = ", fRandomX, ", Y = ", fRandomY)
			SET_CHARACTER_CREATOR_FEATURE_VALUES(characterData, GET_CHARACTER_CREATOR_CURRENT_SELECTION(characterData), iPreset, fRandomX, fRandomY)
			
			// Update our ped visually
			APPLY_CHARACTER_CREATOR_MICRO_MORPH(characterData, GET_CHARACTER_CREATOR_CURRENT_SELECTION(characterData))
			
			PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
			
			// Update the menu
			POPULATE_CHARACTER_CREATOR_FEATURES_MENU(characterData, TRUE)
		BREAK
		
		CASE ciCHARACTER_CREATOR_COLUMN_APPEARANCE
			
			RANDOMISE_CHARACTER_CREATOR_OVERLAY(characterData, GET_CHARACTER_CREATOR_CURRENT_SELECTION(characterData))
			
			APPLY_CHARACTER_CREATOR_OVERLAY(characterData, FALSE)
			
			POPULATE_CHARACTER_CREATOR_APPEARANCE_MENU(characterData)
		BREAK
		
		CASE ciCHARACTER_CREATOR_COLUMN_APPAREL
			
			SWITCH GET_CHARACTER_CREATOR_CURRENT_SELECTION(characterData)
				CASE ciCHARACTER_CREATOR_APPAREL_STYLE
					RANDOMISE_CHARACTER_CREATOR_STYLE_AND_OUTFIT(characterData)
					
					// Preload first outfit in style
					IF IS_CHARACTER_CONTROLLER_BIT_SET(ciCHARACTER_CONTROLLER_PRELOAD_OUTFIT)
						IF IS_CHARACTER_CONTROLLER_PED_OK(characterData.characterCreatorPeds[characterData.eActiveChild])
							RELEASE_PED_PRELOAD_VARIATION_DATA(characterData.characterCreatorPeds[characterData.eActiveChild])
						ENDIF
					ELSE
						SET_CHARACTER_CONTROLLER_BIT(ciCHARACTER_CONTROLLER_PRELOAD_OUTFIT)
					ENDIF
				BREAK
				
				CASE ciCHARACTER_CREATOR_APPAREL_OUTFIT
					RANDOMISE_CHARACTER_CREATOR_OUTFIT(characterData)
					
					// Preload outfit
					IF IS_CHARACTER_CONTROLLER_BIT_SET(ciCHARACTER_CONTROLLER_PRELOAD_OUTFIT)
						IF IS_CHARACTER_CONTROLLER_PED_OK(characterData.characterCreatorPeds[characterData.eActiveChild])
							RELEASE_PED_PRELOAD_VARIATION_DATA(characterData.characterCreatorPeds[characterData.eActiveChild])
						ENDIF
					ELSE
						SET_CHARACTER_CONTROLLER_BIT(ciCHARACTER_CONTROLLER_PRELOAD_OUTFIT)
					ENDIF
				BREAK
				
				CASE ciCHARACTER_CREATOR_APPAREL_HAT
					RANDOMISE_CHARCTER_CREATOR_HAT(characterData)
					APPLY_CHARACTER_CREATOR_HAT(characterData)
				BREAK
				
				CASE ciCHARACTER_CREATOR_APPAREL_GLASSES
					RANDOMISE_CHARCTER_CREATOR_GLASSES(characterData)
					APPLY_CHARACTER_CREATOR_GLASSES(characterData)
				BREAK
				
				CASE ciCHARACTER_CREATOR_APPAREL_CREW_TSHIRT
					RANDOMMISE_CHARACTER_CREATOR_CREW_TSHIRT(characterData)
					
					// Preload crew tshirt
					IF IS_CHARACTER_CONTROLLER_BIT_SET(ciCHARACTER_CONTROLLER_PRELOAD_OUTFIT)
						IF IS_CHARACTER_CONTROLLER_PED_OK(characterData.characterCreatorPeds[characterData.eActiveChild])
							RELEASE_PED_PRELOAD_VARIATION_DATA(characterData.characterCreatorPeds[characterData.eActiveChild])
						ENDIF
					ELSE
						SET_CHARACTER_CONTROLLER_BIT(ciCHARACTER_CONTROLLER_PRELOAD_OUTFIT)
					ENDIF
				BREAK
				
				CASE ciCHARACTER_CREATOR_APPAREL_CREW_STYLE
					RANDOMISE_CHARACTER_CREATOR_CREW_TSHIRT_DECAL(characterData)
					APPLY_CHARACTER_CREATOR_CREW_TSHIRT_DECAL(characterData)
				BREAK
			ENDSWITCH
			
			PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
			
			POPULATE_CHARACTER_CREATOR_APPAREL_MENU(characterData)
		BREAK
		
	ENDSWITCH
ENDPROC



/// PURPOSE:
///    Listen for L1 and R1 and turn the ped
PROC PROCESS_CHARACTER_CREATOR_TURN(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData)
	
	IF GET_CHARACTER_CREATOR_MENU_STATE(characterData) = ciCHARACTER_CREATOR_COLUMN_FEATURES
	OR GET_CHARACTER_CREATOR_MENU_STATE(characterData) = ciCHARACTER_CREATOR_COLUMN_HERITAGE
	OR GET_CHARACTER_CREATOR_MENU_STATE(characterData) = ciCHARACTER_CREATOR_COLUMN_APPEARANCE
			
		//CONST_INT ciCHARACTER_CONTROLLER_TURN_HEAD_R1_BIT			6
		//CONST_INT ciCHARACTER_CONTROLLER_TURN_HEAD_L1_BIT			7

		BOOL bR1Pressed
		BOOL bL1Pressed
		
		// Mouse interaction for turning the head.
		IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		AND PAUSE_MENU_GET_MOUSE_HOVER_UNIQUE_ID() = -1
		
			FLOAT fXMin = 0.3
			
			IF NOT GET_IS_WIDESCREEN()
				fXMin = 0.4
			ENDIF
		
			FLOAT fMouseX = GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_CURSOR_X)
			FLOAT fMouseY = GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_CURSOR_Y)
			
			IF fMouseY > 0.11 AND fMouseY < 0.7
				
				IF fMouseX > fXMin AND fMouseX < 0.55
					SET_MOUSE_CURSOR_STYLE(MOUSE_CURSOR_STYLE_ARROW_LEFT)
					IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
						bL1Pressed = TRUE
					ENDIF
				ENDIF
				
				IF fMouseX > 0.65 AND fMouseX < 0.9
					SET_MOUSE_CURSOR_STYLE(MOUSE_CURSOR_STYLE_ARROW_RIGHT)
					IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
						bR1Pressed = TRUE
					ENDIF
				ENDIF
				
			ENDIF
		
		ENDIF
				
		IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RB)
			bR1Pressed = TRUE
		ENDIF
		
		IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LB)
			bL1Pressed = TRUE
		ENDIF
		
//		BOOL bR1Held
//		BOOL bL1Held
//		IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_RB) AND bR1Held = FALSE
//			characterData.iHoldIndex = 0
//			bR1Held = FALSE
//			bR1Pressed = FALSE
//		ENDIF
//		IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_LB) AND bL1Held = FALSE
//			characterData.iHoldIndex = 0
//			bL1Held = FALSE
//			bL1Pressed = FALSE
//		ENDIF
//		
//		SWITCH characterData.iHoldIndex
//			CASE 0
//				SETTIMERA(0)
//				characterData.iHoldIndex++
//			BREAK
//			CASE 1
//				IF bR1Pressed
//					IF (TIMERA() > 1500)
//						bR1Held = TRUE
//						characterData.iHoldIndex = 0
//					ENDIF
//				ENDIF
//				IF bL1Pressed
//					IF (TIMERA() > 1500)
//						bL1Held = TRUE
//						characterData.iHoldIndex = 0
//					ENDIF
//				ENDIF
//				IF bR1Pressed AND bL1Pressed
//					characterData.iHoldIndex = 0
//				ENDIF
//			BREAK
//		ENDSWITCH
		
		IF NOT IS_CHARACTER_CONTROLLER_BIT_SET(ciCHARACTER_CONTROLLER_TURN_HEAD_R1_BIT)
		AND NOT IS_CHARACTER_CONTROLLER_BIT_SET(ciCHARACTER_CONTROLLER_TURN_HEAD_L1_BIT)
			 
			IF  bR1Pressed  
				
				IF PROCESS_CHARACTER_CREATOR_TURN_RIGHT(characterData, TRUE, FALSE, FALSE)
					CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_CREATOR_TURN - Beginning TURN RIGHT")
					SET_CHARACTER_CONTROLLER_BIT(ciCHARACTER_CONTROLLER_TURN_HEAD_R1_BIT)
				ENDIF
			ELIF bL1Pressed 
			
				IF PROCESS_CHARACTER_CREATOR_TURN_LEFT(characterData, TRUE, FALSE, FALSE)
					CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_CREATOR_TURN - Beginning TURN LEFT")
					SET_CHARACTER_CONTROLLER_BIT(ciCHARACTER_CONTROLLER_TURN_HEAD_L1_BIT)
				ENDIF
			ENDIF
		ELSE
		
			IF NOT bR1Pressed
			AND NOT bL1Pressed
				BOOL bSuccess
				
				IF IS_CHARACTER_CONTROLLER_BIT_SET(ciCHARACTER_CONTROLLER_TURN_HEAD_R1_BIT)
					IF PROCESS_CHARACTER_CREATOR_TURN_RIGHT(characterData, FALSE, FALSE, TRUE)
						bSuccess = TRUE
					ENDIF
				ELIF IS_CHARACTER_CONTROLLER_BIT_SET(ciCHARACTER_CONTROLLER_TURN_HEAD_L1_BIT)
					IF PROCESS_CHARACTER_CREATOR_TURN_LEFT(characterData, FALSE, FALSE, TRUE)
						bSuccess = TRUE
					ENDIF
				ENDIF
				
				IF bSuccess
					CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_CREATOR_TURN - Clean up")
					CLEAR_CHARACTER_CONTROLLER_BIT(ciCHARACTER_CONTROLLER_TURN_HEAD_R1_BIT)
					CLEAR_CHARACTER_CONTROLLER_BIT(ciCHARACTER_CONTROLLER_TURN_HEAD_L1_BIT)
				ENDIF
			ELSE
				IF IS_CHARACTER_CONTROLLER_BIT_SET(ciCHARACTER_CONTROLLER_TURN_HEAD_R1_BIT)
					IF NOT bR1Pressed
					AND bL1Pressed
						
						IF PROCESS_CHARACTER_CREATOR_TURN_LEFT(characterData, TRUE, TRUE, FALSE)
							CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_CREATOR_TURN - Turn LEFT from RIGHT")
						
							CLEAR_CHARACTER_CONTROLLER_BIT(ciCHARACTER_CONTROLLER_TURN_HEAD_R1_BIT)
							SET_CHARACTER_CONTROLLER_BIT(ciCHARACTER_CONTROLLER_TURN_HEAD_L1_BIT)
						ENDIF
					ENDIF
				ELIF IS_CHARACTER_CONTROLLER_BIT_SET(ciCHARACTER_CONTROLLER_TURN_HEAD_L1_BIT)
					IF NOT bL1Pressed
					AND bR1Pressed
					
						IF PROCESS_CHARACTER_CREATOR_TURN_RIGHT(characterData, TRUE, TRUE, FALSE)
							CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_CREATOR_TURN - Turn RIGHT from LEFT")
						
							SET_CHARACTER_CONTROLLER_BIT(ciCHARACTER_CONTROLLER_TURN_HEAD_R1_BIT)
							CLEAR_CHARACTER_CONTROLLER_BIT(ciCHARACTER_CONTROLLER_TURN_HEAD_L1_BIT)
						ENDIF
					ENDIF
				ENDIF		
			ENDIF
		ENDIF	
	ELSE
		IF IS_CHARACTER_CONTROLLER_BIT_SET(ciCHARACTER_CONTROLLER_TURN_HEAD_R1_BIT)
			IF PROCESS_CHARACTER_CREATOR_TURN_RIGHT(characterData, FALSE, FALSE, TRUE)
				CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_CREATOR_TURN - Cleaning up RIGHT")
				CLEAR_CHARACTER_CONTROLLER_BIT(ciCHARACTER_CONTROLLER_TURN_HEAD_R1_BIT)
			ENDIF
		ELIF IS_CHARACTER_CONTROLLER_BIT_SET(ciCHARACTER_CONTROLLER_TURN_HEAD_L1_BIT)
			IF PROCESS_CHARACTER_CREATOR_TURN_LEFT(characterData, FALSE, FALSE, TRUE)
				CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_CREATOR_TURN - Cleaning up LEFT")
				CLEAR_CHARACTER_CONTROLLER_BIT(ciCHARACTER_CONTROLLER_TURN_HEAD_L1_BIT)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE: Allows mouse to move the sliders in the features tweak box.
///    
/// RETURNS:
///    TRUE if the mouse is changing the values.
FUNC BOOL GET_CHARACTER_CREATOR_FEATURES_MOUSE_BOX_VALUES( FLOAT &fX, FLOAT &fY, BOOL bYaxisActive = TRUE )

	CONST_FLOAT BOX_WIDTH		0.08
	CONST_FLOAT BOX_HEIGHT		0.14
	CONST_FLOAT BOX_WIDTH_PERCENT 0.0008
	CONST_FLOAT BOX_HEIGHT_PERCENT 0.0014
	
	VECTOR vPauseMenuOrigin = GET_PAUSE_MENU_POSITION()
		
	FLOAT fWidth = BOX_WIDTH
		
	FLOAT fXMin = vPauseMenuOrigin.x + ADJUST_X_COORD_FOR_ASPECT_RATIO(0.073)
	
	//IF NOT GET_IS_WIDESCREEN()
		fWidth = ADJUST_X_COORD_FOR_ASPECT_RATIO( fWidth )
	//ENDIF
	
	FLOAT fYMin = vPauseMenuOrigin.y + 0.54
	FLOAT fXMax = fXMin + fWidth
	FLOAT fYMax = fYMin + BOX_HEIGHT
	
	FLOAT fCursorXBoxCoord
	FLOAT fCursorYBoxCoord
	
	FLOAT fXBoxValue 
	FLOAT fYBoxValue
	
	FLOAT fMouseX = GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_CURSOR_X) + 0.005 // Fudge for middle finger cursor
	FLOAT fMouseY = GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_CURSOR_Y) 
	
//	SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
//	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD)
//	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
//	
//	DISPLAY_TEXT_WITH_FLOAT( 0.7, 0.1, "NUMBER", fMouseX, 3 )
//	DISPLAY_TEXT_WITH_FLOAT( 0.7, 0.2, "NUMBER", fMouseY, 3 )

	IF NOT bYaxisActive
		//fYMin = fYmin + (BOX_HEIGHT / 2 - 0.005)
		//fYMax = fYmax + ((BOX_HEIGHT / 2 + 0.005)
	ENDIF
	
	//DEBUG!
	//DRAW_RECT_FROM_CORNER(fXMin, fYMin, fWidth, BOX_HEIGHT, 255,255,255, 128)
	
	IF 	fMouseX >= fXMin
	AND fMouseX <= fXMax 
	AND fMouseY >= fYMin
	AND fMouseY <= fYMax
	
		
		fCursorXBoxCoord = fMouseX - fXMin
		fCursorYBoxCoord = fMouseY - fYMin
		
		fXBoxValue = fCursorXBoxCoord / fWidth * 100
		fYBoxValue = fCursorYBoxCoord / BOX_HEIGHT * 100
		
		//DISPLAY_TEXT_WITH_FLOAT( 0.5, 0.1, "NUMBER", fCursorXBoxCoord, 3 )
		//DISPLAY_TEXT_WITH_FLOAT( 0.5, 0.2, "NUMBER", fCursorYBoxCoord, 3 )
		
		//DISPLAY_TEXT_WITH_FLOAT( 0.6, 0.1, "NUMBER", fXBoxValue, 3 )
		//DISPLAY_TEXT_WITH_FLOAT( 0.6, 0.2, "NUMBER", fYBoxValue, 3 )
		
		SET_MOUSE_CURSOR_STYLE(MOUSE_CURSOR_STYLE_HAND_MIDDLE_FINGER)
		
		// Translate coords to map them into the box.
		IF IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
			fX = fXBoxValue
			fY = fYBoxValue
			RETURN TRUE
		ENDIF
	
	ENDIF
	
	RETURN FALSE

ENDFUNC

/// PURPOSE:
/// 	Get a previous analogue axis position to compare against
FUNC BOOL GET_PREVIOUS_ANALOGUE_AXIS_POSITION(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, FLOAT fAxisPosition)
	IF (characterData.fTempAxisPos = fAxisPosition)
		RETURN TRUE
	ELSE
		characterData.fTempAxisPos = fAxisPosition
		RETURN FALSE
	ENDIF
ENDFUNC

/// PURPOSE:
///    Listen for analogue input and update features if player does this.
PROC PROCESS_CHARACTER_CREATOR_ANALOGUE_INPUT(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData)

	IF GET_CHARACTER_CREATOR_MENU_STATE(characterData) = ciCHARACTER_CREATOR_COLUMN_FEATURES
	OR GET_CHARACTER_CREATOR_MENU_STATE(characterData) = ciCHARACTER_CREATOR_COLUMN_APPEARANCE

		INT iRx = GET_CONTROL_VALUE(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT_AXIS_X) - 127
		INT iRy = GET_CONTROL_VALUE(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT_AXIS_Y) - 127
		INT iCurrentSelection = GET_CHARACTER_CREATOR_CURRENT_SELECTION(characterData)
		
		// Only process this if we are in the features column
		IF GET_CHARACTER_CREATOR_MENU_STATE(characterData) = ciCHARACTER_CREATOR_COLUMN_FEATURES
		
			CHARACTER_CREATOR_FEATURE_VALUES featureValues
			CHARACTER_CREATOR_FEATURE_DATA featureData
			
			GET_CHARACTER_CREATOR_FEATURE_VALUES(characterData, featureValues, iCurrentSelection)
			GET_CHARACTER_CREATOR_FEATURE_DATA(featureData, iCurrentSelection)
			
			BOOL bSaveOutValues
		
			// Mouse and keyboard support for tweaking the blend values
			IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
				
				FLOAT fTempX, fTempY
				
				IF GET_CHARACTER_CREATOR_FEATURES_MOUSE_BOX_VALUES( fTempX, fTempY, featureData.bYAxisActive )
					
					IF featureData.bXAxisActive
						featureValues.fXBlend = fTempX
					ENDIF
					
					IF featureData.bYAxisActive
						featureValues.fYBlend = fTempY
					ENDIF
					
					bSaveOutValues = TRUE
				ENDIF
			ELSE
			
				// Check we are beyond the dead zone
				IF (iRx <= -10 OR iRx >= 10)
				AND featureData.bXAxisActive
					
					FLOAT fPercentageX = TO_FLOAT(iRx) / 127
					FLOAT fToIncreaseX = cfCHARACTER_CREATOR_FEATURE_SLIDE_X * fPercentageX
					
					featureValues.fXBlend += fToIncreaseX
					
					IF featureValues.fXBlend < 0
						featureValues.fXBlend = 0
					ENDIF
					
					IF featureValues.fXBlend > 100
						featureValues.fXBlend = 100
					ENDIF
					
					bSaveOutValues = TRUE
				ENDIF

				// Check for y movement and adjust accordingly
				IF (iRy <= -10 OR iRy >= 10)
				AND featureData.bYAxisActive
					
					FLOAT fPercentageY = TO_FLOAT(iRy) / 127
					FLOAT fToIncreaseY = cfCHARACTER_CREATOR_FEATURE_SLIDE_Y * fPercentageY
					
					featureValues.fYBlend += fToIncreaseY
					
					IF featureValues.fYBlend < 0
						featureValues.fYBlend = 0
					ENDIF
					
					IF featureValues.fYBlend > 100
						featureValues.fYBlend = 100
					ENDIF
					
					bSaveOutValues = TRUE		
				ENDIF
			ENDIF
			
			// Save out our values
			IF bSaveOutValues
				
				INT iFeatureVal = -1
				BOOL bUpdateMenu = FALSE
				
				IF featureValues.iPreset != -1
					bUpdateMenu = TRUE
				ENDIF
				
				CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_CREATOR_ANALOGUE_INPUT - Player customising setting for feature ", iCurrentSelection, " to: (", featureValues.fXBlend, ",", featureValues.fYBlend, ")")
				SET_CHARACTER_CREATOR_FEATURE_VALUES(characterData, iCurrentSelection, iFeatureVal, featureValues.fXBlend, featureValues.fYBlend)
				
				// If we need to update the text in the menu, do a full refresh.
				IF bUpdateMenu
					POPULATE_CHARACTER_CREATOR_FEATURES_MENU(characterData, TRUE)
				ELSE
					UPDATE_CHARACTER_CREATOR_FEATURE_GRID(characterData, TRUE)
				ENDIF
				
				// Feature grid sound limits
				IF (featureValues.fXBlend = 0 OR featureValues.fXBlend = 100) 
					IF NOT GET_PREVIOUS_ANALOGUE_AXIS_POSITION(characterData, featureValues.fYBlend)
						PLAY_SOUND_FRONTEND(characterData.iSoundID, "CONTINUOUS_SLIDER", "HUD_FRONTEND_DEFAULT_SOUNDSET")
					ELSE
						STOP_SOUND(characterData.iSoundID)
					ENDIF
				ELIF (featureValues.fYBlend = 0 OR featureValues.fYBlend = 100)
					IF NOT GET_PREVIOUS_ANALOGUE_AXIS_POSITION(characterData, featureValues.fXBlend)
						PLAY_SOUND_FRONTEND(characterData.iSoundID, "CONTINUOUS_SLIDER", "HUD_FRONTEND_DEFAULT_SOUNDSET")
					ELSE
						STOP_SOUND(characterData.iSoundID)
					ENDIF
				ELSE
					PLAY_SOUND_FRONTEND(characterData.iSoundID, "CONTINUOUS_SLIDER", "HUD_FRONTEND_DEFAULT_SOUNDSET")
				ENDIF
				
				// Apply the correct type of morph or overlay
				APPLY_CHARACTER_CREATOR_MICRO_MORPH(characterData, iCurrentSelection)
			ELSE
				STOP_SOUND(characterData.iSoundID)
			ENDIF
		ENDIF
		
		// Handle player shifting the opacity of the appearance options
		IF GET_CHARACTER_CREATOR_MENU_STATE(characterData) = ciCHARACTER_CREATOR_COLUMN_APPEARANCE
		
			CHARACTER_CREATOR_APPEARANCE_VALUES appearanceValues
			CHARACTER_CREATOR_APPEARANCE_DATA appearanceData
			
			GET_CHARACTER_CREATOR_APPEARANCE_VALUES(characterData, appearanceValues, iCurrentSelection)
			GET_CHARACTER_CREATOR_APPEARANCE_DATA(appearanceData, iCurrentSelection)
				
			BOOL bUpdateValues
			
			IF appearanceValues.iValue != -1
			
				// Mouse and keyboard
				IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
				
					IF appearanceData.bOpacityActive
					
						appearanceValues.fOpacity = HANDLE_MOUSE_SLIDER( MOUSE_SLIDER_TYPE_CHARACTER_CREATOR_OPACITY )
											
						IF appearanceValues.fOpacity != -1
							bUpdateValues = TRUE
						ENDIF
						
					ENDIF
					
				ELSE
			
					// Check we are beyond the dead zone
					IF (iRx <= -10 OR iRx >= 10)
					AND appearanceData.bOpacityActive
						
						FLOAT fPercentageX = TO_FLOAT(iRx) / 127
						FLOAT fToIncreaseX = cfCHARACTER_CREATOR_APPEARANCE_SLIDE_X * fPercentageX
						
						appearanceValues.fOpacity += fToIncreaseX
						
						IF appearanceValues.fOpacity < 0
							appearanceValues.fOpacity = 0
						ENDIF
						
						IF appearanceValues.fOpacity > 100
							appearanceValues.fOpacity = 100
						ENDIF
						
						bUpdateValues = TRUE
					ENDIF
					
				ENDIF
				
			ENDIF
			
			// Save out our values
			IF bUpdateValues
				SET_CHARACTER_CREATOR_APPEARANCE_VALUES(characterData, iCurrentSelection, appearanceValues.iValue, appearanceValues.fOpacity, appearanceValues.iColour, appearanceValues.iColour2, appearanceValues.iColourHighlightValue)
				
				// Update our body overlays
				UPDATE_CHARACTER_CREATOR_BODY_OVERLAYS(characterData, iCurrentSelection, characterData.eActiveChild)
				
				INT iHighlight = appearanceValues.iColourHighlightValue
				INT iColourMax = GET_CHARACTER_CREATOR_TINT_MAX_AVAILABLE(characterData, iCurrentSelection)
				
				// If we need to update the text in the menu, do a full refresh.
				SET_CHARACTER_CREATOR_APPEARANCE_GRID_DETAILS(appearanceData.bOpacityActive, appearanceData.bColourActive, appearanceValues.iValue, appearanceValues.fOpacity, iHighlight, iColourMax)
				
				// Apply the correct type of morph or overlay
				APPLY_CHARACTER_CREATOR_OVERLAY(characterData, FALSE)
				
				IF appearanceValues.fOpacity = 0 OR appearanceValues.fOpacity = 100
					STOP_SOUND(characterData.iSoundID)
				ELSE
					PLAY_SOUND_FRONTEND(characterData.iSoundID, "CONTINUOUS_SLIDER", "HUD_FRONTEND_DEFAULT_SOUNDSET")
				ENDIF
			ELSE
				STOP_SOUND(characterData.iSoundID)
			ENDIF
		ENDIF
	ENDIF
ENDPROC


// PURPOSE:
///    Solely handles the scrolling of options that change every frame.
PROC PROCESS_CHARACTER_CREATOR_CONSTANT_SCROLL(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, INT iIncrement)
	
	BOOL bUpdateMenu
	SWITCH GET_CHARACTER_CREATOR_MENU_STATE(characterData)
		
		// Only process those options that have a constant scroll available to them
		CASE ciCHARACTER_CREATOR_COLUMN_HERITAGE
		
			SWITCH GET_CHARACTER_CREATOR_CURRENT_SELECTION(characterData)
				CASE ciCHARACTER_CREATOR_HERITAGE_DOM
					PROCESS_CHARACTER_DOMINANCE_CHANGE(characterData, iIncrement)
					
					bUpdateMenu = TRUE
				BREAK
				
				CASE ciCHARACTER_CREATOR_HERITAGE_SKIN_TONE
					PROCESS_CHARACTER_SKIN_TONE_CHANGE(characterData, iIncrement)
					
					bUpdateMenu = TRUE
				BREAK
			ENDSWITCH
			
			IF bUpdateMenu
				POPULATE_CHARACTER_CREATOR_HERITAGE_MENU(characterData, TRUE)	
			ENDIF
			
		BREAK
		
		CASE ciCHARACTER_CREATOR_COLUMN_STATS
			
			// Stats can only be changed if we have not come from the alteration prompt
			IF NOT HAS_ENTERED_CHAR_CREATION_FROM_ALTERATION_PROMPT()
				PROCESS_CHARACTER_STAT_CHANGE(characterData, iIncrement)
				
				POPULATE_CHARACTER_CREATOR_STATS_MENU(characterData, TRUE)
			ENDIF
		BREAK
	ENDSWITCH
	
ENDPROC

/// PURPOSE:
///    Change the menu values which will have direct impact on character
PROC PROCESS_CHARACTER_CREATOR_SCROLL(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, INT iIncrement)

	BOOL bUpdateMenu
	BOOL bDoUpdateOnlyToScaleform = TRUE
	
	// What menu are we currently in?
	SWITCH GET_CHARACTER_CREATOR_MENU_STATE(characterData)
	
		CASE ciCHARACTER_CREATOR_COLUMN_MAIN_MENU
			
			// Switch over the options in the main menu. Only certain rows will have a valid scroll option
			SWITCH GET_CHARACTER_CREATOR_CURRENT_SELECTION(characterData)
				CASE ciCHARACTER_CREATOR_MAIN_MENU_GENDER
					PROCESS_CHARACTER_GENDER_CHANGE(characterData)
					
					bUpdateMenu = TRUE
				BREAK				
			ENDSWITCH
			
			IF bUpdateMenu
				POPULATE_CHARACTER_CREATOR_MAIN_MENU(characterData, TRUE)
			ENDIF
		BREAK
		
		// Switch over the options in the heritage menu. This is the mum / dad and two dominance sliders
		CASE ciCHARACTER_CREATOR_COLUMN_HERITAGE
			
			SWITCH GET_CHARACTER_CREATOR_CURRENT_SELECTION(characterData)
				CASE ciCHARACTER_CREATOR_HERITAGE_MUM_NAME
				CASE ciCHARACTER_CREATOR_HERITAGE_DAD_NAME
					PROCESS_CHARACTER_PARENT_CHANGE(characterData, iIncrement)	
					
					// Update our overlay
					APPLY_CHARACTER_CREATOR_OVERLAY(characterData, FALSE)
					
					bUpdateMenu = TRUE
				BREAK
				
			ENDSWITCH
			
			IF bUpdateMenu
				POPULATE_CHARACTER_CREATOR_HERITAGE_MENU(characterData, TRUE)
			ENDIF
			
		BREAK
		
		// Switch over the options in the appearance menu. 
		CASE ciCHARACTER_CREATOR_COLUMN_APPEARANCE
		
			SWITCH GET_CHARACTER_CREATOR_CURRENT_SELECTION(characterData)				
				CASE ciCHARACTER_CREATOR_APPEARANCE_HAIR_STYLE_F
				CASE ciCHARACTER_CREATOR_APPEARANCE_HAIR_STYLE_M
					PROCESS_CHARACTER_HAIR_CHANGE(characterData, iIncrement)
					
					bUpdateMenu = TRUE
					bDoUpdateOnlyToScaleform = FALSE
				BREAK		
				
				CASE ciCHARACTER_CREATOR_APPEARANCE_EYES
					PROCESS_CHARACTER_EYE_COLOUR_CHANGE(characterData, iIncrement)
					
					bUpdateMenu = TRUE
				BREAK
				
				DEFAULT
					PROCESS_CHARACTER_CREATOR_APPEARANCE_CHANGE(characterData, iIncrement)
					
					bUpdateMenu = TRUE
				BREAK
								
			ENDSWITCH
			
			IF bUpdateMenu
				POPULATE_CHARACTER_CREATOR_APPEARANCE_MENU(characterData, bDoUpdateOnlyToScaleform)
			ENDIF
		BREAK
		
		CASE ciCHARACTER_CREATOR_COLUMN_APPAREL
			
			SWITCH GET_CHARACTER_CREATOR_CURRENT_SELECTION(characterData)
				CASE ciCHARACTER_CREATOR_APPAREL_STYLE
					PROCESS_CHARACTER_STYLE_CHANGE(characterData, iIncrement)
					
					SET_CHARACTER_CONTROLLER_BIT(ciCHARACTER_CONTROLLER_CLOTHES_CHANGED)
					
					bUpdateMenu = TRUE
					bDoUpdateOnlyToScaleform = FALSE
				BREAK
				
				CASE ciCHARACTER_CREATOR_APPAREL_OUTFIT
					PROCESS_CHARACTER_OUTFIT_CHANGE(characterData, iIncrement)
					
					SET_CHARACTER_CONTROLLER_BIT(ciCHARACTER_CONTROLLER_CLOTHES_CHANGED)
					
					bUpdateMenu = TRUE
				BREAK
				
				CASE ciCHARACTER_CREATOR_APPAREL_HAT
					PROCESS_CHARACTER_HAT_CHANGE(characterData, iIncrement)					
					
					bUpdateMenu = TRUE
				BREAK
				
				CASE ciCHARACTER_CREATOR_APPAREL_GLASSES
					PROCESS_CHARACTER_GLASSES_CHANGE(characterData, iIncrement)	
					
					bUpdateMenu = TRUE
				BREAK
				
				CASE ciCHARACTER_CREATOR_APPAREL_CREW_TSHIRT
					PROCESS_CHARACTER_CREW_TSHIRT_CHANGE(characterData, iIncrement)
					
					bUpdateMenu = TRUE
					bDoUpdateOnlyToScaleform = FALSE
				BREAK
				
				CASE ciCHARACTER_CREATOR_APPAREL_CREW_STYLE
					PROCESS_CHARACTER_CREATOR_TSHIRT_DECAL_CHANGE(characterData, iIncrement)
					
					bUpdateMenu = TRUE
				BREAK
				
			ENDSWITCH
			
			IF bUpdateMenu
				POPULATE_CHARACTER_CREATOR_APPAREL_MENU(characterData, bDoUpdateOnlyToScaleform)
			ENDIF
		BREAK
		
		CASE ciCHARACTER_CREATOR_COLUMN_FEATURES
			PROCESS_CHARACTER_CREATOR_FEATURE_CHANGE(characterData, iIncrement)
			
			POPULATE_CHARACTER_CREATOR_FEATURES_MENU(characterData, TRUE)
		BREAK		
	ENDSWITCH
	
	// Make sure we handle other selections when a valid scroll has occurred
	PROCESS_CHARACTER_CREATOR_CONSTANT_SCROLL(characterData, iIncrement)
ENDPROC

/// PURPOSE:
///    Handle a up / down movement to update the menu
PROC PROCESS_CHARACTER_CREATOR_MENU_ROW_CHANGE(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData)

	SWITCH GET_CHARACTER_CREATOR_MENU_STATE(characterData)
	
		CASE ciCHARACTER_CREATOR_COLUMN_MAIN_MENU
			UPDATE_CHARACTER_CONTROLLER_BUTTONS(characterData)
		BREAK
	
		CASE ciCHARACTER_CREATOR_COLUMN_FEATURES
			SET_CHARACTER_CONTROLLER_BIT(ciCHARACTER_CONTROLLER_PULSE_FEATURE)
			POPULATE_CHARACTER_CREATOR_FEATURES_MENU(characterData, TRUE)
		BREAK
		
		CASE ciCHARACTER_CREATOR_COLUMN_APPEARANCE
			POPULATE_CHARACTER_CREATOR_APPEARANCE_MENU(characterData, TRUE)
		BREAK
	ENDSWITCH
	
	SET_CHARACTER_CREATOR_MENU_HELP_TEXT(characterData)
	
ENDPROC

/// PURPOSE:
///    Handle the input from the triggers. Only used for colour in Appearance right now.
PROC PROCESS_CHARACTER_CREATOR_TRIGGERS(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, INT iIncrement, BOOL bMouseSelect = FALSE)

	// Only process this for the appearance column
	IF GET_CHARACTER_CREATOR_MENU_STATE(characterData) = ciCHARACTER_CREATOR_COLUMN_APPEARANCE
		
		INT iSelection = GET_CHARACTER_CREATOR_CURRENT_SELECTION(characterData)

		CHARACTER_CREATOR_APPEARANCE_DATA appearanceData
		GET_CHARACTER_CREATOR_APPEARANCE_DATA(appearanceData, iSelection)
				
		// Only update if the colour option is active
		IF appearanceData.bColourActive
		
			SWITCH iSelection
			
				// Handle the hair change (not overlay tinting)
				CASE ciCHARACTER_CREATOR_APPEARANCE_HAIR_STYLE_F
				CASE ciCHARACTER_CREATOR_APPEARANCE_HAIR_STYLE_M
					PROCESS_CHARACTER_HAIR_COLOUR_CHANGE(characterData, iIncrement, bMouseSelect)
				BREAK
				
				// All other active colour appearance rows should be tint overlays
				DEFAULT
					PROCESS_CHARACTER_CREATOR_OVERLAY_COLOUR_CHANGE(characterData, iIncrement, iSelection, appearanceData.iColourMin, appearanceData.iColourMax, bMouseSelect)					
				BREAK
			
			ENDSWITCH
			
			UPDATE_CHARACTER_CREATOR_APPEARANCE_HIGHLIGHT(characterData, iSelection)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Fakes an input so the mousewheel scroll can be seamlessly wired into the menu
PROC PROCESS_CHARACTER_CREATOR_MOUSEWHEEL_SCROLL()
	
	IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_UP)
	
		SET_CONTROL_VALUE_NEXT_FRAME(FRONTEND_CONTROL, INPUT_FRONTEND_UP, 1) 
	
	ELIF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_DOWN)
		
		SET_CONTROL_VALUE_NEXT_FRAME(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN, 1) 
		
	ENDIF

ENDPROC

/// PURPOSE:
///    Works out the mouse scroll value for a given menu item
/// PARAMS:
///    characterData - 
/// RETURNS:
///    The value of the increment/decrement
///    
///    TODO: Proper mouse slider controls
///    
FUNC INT GET_CHARACTER_CREATOR_MOUSE_SCROLL_VALUE(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData)

	INT iMenuItem = GET_CHARACTER_CREATOR_CURRENT_SELECTION(characterData)
	INT iValue = 0

	SWITCH GET_CHARACTER_CREATOR_MENU_STATE(characterData)
	
		CASE ciCHARACTER_CREATOR_COLUMN_HERITAGE
		
			IF iMenuItem > 3
				// Click left to decrement, right to increment
				//RETURN GET_PC_MOUSE_MENU_VALUE_CHANGE(TRUE, 0.075)
				RETURN 0
			ENDIF
				
		BREAK
			
		CASE ciCHARACTER_CREATOR_COLUMN_STATS
		
			iValue = GET_PC_MOUSE_MENU_VALUE_CHANGE(TRUE, 0.5)
			
			IF iValue = -1
				SET_MOUSE_CURSOR_STYLE(MOUSE_CURSOR_STYLE_ARROW_MINUS)
			ELIF iValue = 1
				SET_MOUSE_CURSOR_STYLE(MOUSE_CURSOR_STYLE_ARROW_PLUS)
			ENDIF
			
			// Click left to decrement, right to increment
			RETURN iValue
		BREAK

	ENDSWITCH
	
		
	// Click right arrow to increment, anywhere else to decrement.
	RETURN GET_PC_MOUSE_MENU_VALUE_CHANGE(TRUE)

ENDFUNC

// PURPOSE:
//    Lets mouse know if it's ok to press accept
// PARAMS:
//    characterData - 
// RETURNS:
//    
FUNC BOOL IS_ITEM_ADJUSTABLE(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData)

	IF GET_CHARACTER_CREATOR_MENU_STATE(characterData) = ciCHARACTER_CREATOR_COLUMN_MAIN_MENU
	
		IF GET_CHARACTER_CREATOR_CURRENT_SELECTION(characterData) > 0
			RETURN FALSE
		ENDIF
		
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

/// PURPOSE:
///    Listen for specific menu inputs (separate to rotation / zoom etc.)
PROC PROCESS_CHARACTER_CREATOR_MENU_INPUT(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData)
	
	INT iMouseItem = MENU_CURSOR_NO_ITEM
	//BOOL bMouseItemSelect = FALSE
	INT iTempMouseIncrement = 0
	INT iMouseIncrement = 0
	BOOL bMouseAccept = FALSE
	
	// ********** PC MOUSE **********
	
	IF HAVE_CONTROLS_CHANGED(FRONTEND_CONTROL)
		UPDATE_CHARACTER_CONTROLLER_BUTTONS(characterData)
	ENDIF
	
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
	
		LOCK_MOUSE_SUPPORT()
		TURN_MOUSE_CURSOR_ON()

		iMouseItem = PAUSE_MENU_GET_MOUSE_HOVER_UNIQUE_ID()
		
		//SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
		//SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD)
		//SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
		
		//DISPLAY_TEXT_WITH_NUMBER( 0.5, 0.1, "NUMBER", iMouseItem)
		//DISPLAY_TEXT_WITH_NUMBER( 0.5, 0.2, "NUMBER", characterData.currentSelection[GET_CHARACTER_CREATOR_MENU_STATE(characterData)])
						
		PROCESS_CHARACTER_CREATOR_MOUSEWHEEL_SCROLL()
			
		IF iMouseItem > MENU_CURSOR_NO_ITEM
	
			// Mouse pointer
			SET_MOUSE_CURSOR_STYLE(MOUSE_CURSOR_STYLE_ARROW)
			
			// Moved this here so we can display different pointers for the stats menu.
			IF IS_ITEM_ADJUSTABLE(characterData)
			AND iMouseItem = characterData.currentSelection[GET_CHARACTER_CREATOR_MENU_STATE(characterData)]
				iTempMouseIncrement = GET_CHARACTER_CREATOR_MOUSE_SCROLL_VALUE(characterData)
			ENDIF
			
			IF IS_DISABLED_CONTROL_JUST_RELEASED( FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
			OR (IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT) AND iTempMouseIncrement != 0)
			
				// Confirm item
				IF iMouseItem = characterData.currentSelection[GET_CHARACTER_CREATOR_MENU_STATE(characterData)]
				
					IF iTempMouseIncrement !=0
						iMouseIncrement = iTempMouseIncrement
						bMouseAccept = FALSE
					ELSE
						bMouseAccept = TRUE
					ENDIF
				// Select new item
				ELSE
					//SET_CHARACTER_CREATOR_CURRENT_SELECTION(characterData, iMouseItem)
					//SET_FRONTEND_HIGHLIGHT(GET_CHARACTER_CONTROLLER_COLUMN_INDEX(GET_CHARACTER_CREATOR_MENU_STATE(characterData)), GET_CHARACTER_CREATOR_CURRENT_SELECTION(characterData), FALSE)
					//PROCESS_CHARACTER_CREATOR_MENU_ROW_CHANGE(characterData)
					//bMouseItemSelect = TRUE
				ENDIF
				
			ENDIF

		ELIF IS_MOUSE_ROLLED_OVER_INSTRUCTIONAL_BUTTONS()
			SET_MOUSE_CURSOR_STYLE(MOUSE_CURSOR_STYLE_ARROW)
		ELIF PAUSE_MENU_GET_HAIR_COLOUR_INDEX() > -1
			//SET_MOUSE_CURSOR_STYLE(MOUSE_CURSOR_STYLE_HAND_MIDDLE_FINGER)
		ELSE
			SET_MOUSE_CURSOR_STYLE(MOUSE_CURSOR_STYLE_ARROW)
		ENDIF
	ELSE
		TURN_MOUSE_CURSOR_OFF()
	ENDIF
		
	// ********** R-STICK ***********
	
	// Listen for the player changing the look at of the player
	PROCESS_CHARACTER_CREATOR_LOOK_AT(characterData)
	
	// ********** R1 / L1 Turn ***********
	
	PROCESS_CHARACTER_CREATOR_TURN(characterData)
	
	// ********** R-STICK **********
	
	PROCESS_CHARACTER_CREATOR_ANALOGUE_INPUT(characterData)
	
	// ********** ACCEPT ***********
	
	// Listen for the player hitting SELECT
	IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
	OR bMouseAccept = TRUE
		
		CPRINTLN(DEBUG_NET_CHARACTER, "		PROCESS_CHARACTER_CREATOR_MENU_INPUT - Player has pressed SELECT")
		
		PROCESS_CHARCTER_CREATOR_SELECT(characterData)
		EXIT
	ENDIF
	
	// ********** CANCEL ***********
	
	IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
	OR IS_PC_MOUSE_CANCEL_JUST_RELEASED()
			
		CPRINTLN(DEBUG_NET_CHARACTER, "		PROCESS_CHARACTER_CREATOR_MENU_INPUT - Player has pressed CANCEL")
		
		PROCESS_CHARCTER_CREATOR_CANCEL(characterData)
		EXIT			
	ENDIF
	
	// ********** RANDOM ***********
	
	IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_X)
		
		CPRINTLN(DEBUG_NET_CHARACTER, "		PROCESS_CHARACTER_CREATOR_MENU_INPUT - Player has pressed RANDOM")
		
		PROCESS_CHARACTER_CREATOR_RANDOMIZE(characterData)
		EXIT
	ENDIF
	
	// ********** EDIT NAME ***********
	
	IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_Y)
		
		CPRINTLN(DEBUG_NET_CHARACTER, "		PROCESS_CHARACTER_CREATOR_MENU_INPUT - Player has pressed TRIANGLE")

		PROCESS_CHARACTER_CREATOR_TRIANGLE(characterData)
		
		EXIT
	ENDIF
	
	// ********** CHANGE COLOUR ***********
	
	INT iIncrement
	
	IF PROCESS_CHARACTER_CREATOR_MOUSE_TRIGGER_INPUT(characterData)
		PROCESS_CHARACTER_CREATOR_TRIGGERS(characterData, iIncrement, TRUE)
		EXIT
	ENDIF
	
	IF PROCESS_CHARACTER_CREATOR_TRIGGER_INPUT(characterData, iIncrement)
		PROCESS_CHARACTER_CREATOR_TRIGGERS(characterData, iIncrement)
		EXIT
	ENDIF
	
	
	// ********** LEFT / RIGHT SCROLL ***********
	
	BOOL bButtonPressed
	INT iResponseTime = 300
		
	BOOL bShouldIncrement
	
	// Slightly longer delay for the pictures due to the txd / txn loading
	IF GET_CHARACTER_CREATOR_MENU_STATE(characterData) = ciCHARACTER_CREATOR_COLUMN_HERITAGE
		IF GET_CHARACTER_CREATOR_CURRENT_SELECTION(characterData) = ciCHARACTER_CREATOR_HERITAGE_MUM_NAME	
		OR GET_CHARACTER_CREATOR_CURRENT_SELECTION(characterData) = ciCHARACTER_CREATOR_HERITAGE_DAD_NAME
			iResponseTime = 400
		ENDIF
	ENDIF
	
	// Prevents conflict when selecting with mouse
	//IF bMouseItemSelect = FALSE
	bShouldIncrement = SHOULD_SELECTION_BE_INCREMENTED_SP(characterData.iCharacterControllerTimer, iIncrement, bButtonPressed, TRUE, FALSE, FALSE, iResponseTime, iMouseIncrement)
	//ENDIF

	// IF the player has scrolled left or right or holding button down, process the input
	IF bShouldIncrement
		//CPRINTLN(DEBUG_NET_CHARACTER, "		PROCESS_CHARACTER_CREATOR_MENU_INPUT - Player has scrolled left / right")
		PROCESS_CHARACTER_CREATOR_SCROLL(characterData, iIncrement)
	ELSE
	
		// Override so mouse slider works for character heritage and skin tone.
		IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)

			IF iMouseItem = characterData.currentSelection[GET_CHARACTER_CREATOR_MENU_STATE(characterData)]
			
				// Bit inefficient this, but to avoid spaghetti code I'm calling the slider twice
				// Once here to do the mouse cursors, and again in the actual code that handles the normal slider functionality
				
				IF GET_CHARACTER_CREATOR_CURRENT_SELECTION_FOR_MENU(characterData, ciCHARACTER_CREATOR_COLUMN_HERITAGE) = ciCHARACTER_CREATOR_HERITAGE_DOM
					IF HANDLE_MOUSE_SLIDER(MOUSE_SLIDER_TYPE_CHARACTER_CREATOR_FAMILY_RESEMBLANCE) != -1
						IF IS_CONTROL_PRESSED( FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
							bButtonPressed = TRUE
						ENDIF
					ENDIF
				ENDIF
				
				
				IF GET_CHARACTER_CREATOR_CURRENT_SELECTION_FOR_MENU(characterData, ciCHARACTER_CREATOR_COLUMN_HERITAGE) = ciCHARACTER_CREATOR_HERITAGE_SKIN_TONE
					IF HANDLE_MOUSE_SLIDER(MOUSE_SLIDER_TYPE_CHARACTER_CREATOR_SKIN_TONE) != -1
						IF IS_CONTROL_PRESSED( FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
							bButtonPressed = TRUE
						ENDIF
					ENDIF
				ENDIF
				
			ENDIF
			
		ENDIF

		IF bButtonPressed
			PROCESS_CHARACTER_CREATOR_CONSTANT_SCROLL(characterData,iIncrement)
		ELSE
			// Stop continuous scrolling sound in dominance/tone heritage menu & stats options
			IF GET_CHARACTER_CREATOR_CURRENT_SELECTION_FOR_MENU(characterData, ciCHARACTER_CREATOR_COLUMN_HERITAGE) = ciCHARACTER_CREATOR_HERITAGE_DOM
			OR GET_CHARACTER_CREATOR_CURRENT_SELECTION_FOR_MENU(characterData, ciCHARACTER_CREATOR_COLUMN_HERITAGE) = ciCHARACTER_CREATOR_HERITAGE_SKIN_TONE
			OR GET_CHARACTER_CREATOR_MENU_STATE(characterData) = ciCHARACTER_CREATOR_COLUMN_STATS
				IF IS_CONTROL_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_LS)
				OR IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
				OR IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
					IF IS_CHARACTER_CONTROLLER_BIT_SET(ciCHARACTER_CONTROLLER_CONTINUOUS_SOUND_PLAYING)
						STOP_SOUND(characterData.iSoundID)
						CLEAR_CHARACTER_CONTROLLER_BIT(ciCHARACTER_CONTROLLER_CONTINUOUS_SOUND_PLAYING)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Maintain entry of the characters name
PROC MAINTAIN_CHARACTER_CREATOR_NAME_ENTRY(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, MPHUD_PLACEMENT_TOOLS& Placement)
	
	//SUPPRESS_CHARACTER_MENU_THIS_FRAME()
	
	BOOL bHasQuit

	IF GET_PLAYER_TRANSITION_KEYBOARD_INPUT_NAME(characterData.keyboardStatus, characterData.iKeyboardStage, characterData.iProfanityCheck, characterData.iNumOfAttempts, bHasQuit, characterData.iCharacterSelectedSlot)
		
		// We need to update the text or go to confirmation screen
		IF bHasQuit
		
			CPRINTLN(DEBUG_NET_CHARACTER, "MAINTAIN_CHARACTER_CREATOR_NAME_ENTRY - Player has entered an invalid word or profanity failed, make them try again: ", GET_MP_STRING_CHARACTER_STAT(MP_STAT_CHAR_NAME, characterData.iCharacterSelectedSlot))
			// Player quit or failed, have to go back to main state
			//Don't go back to the main menu, just bring the keyboard up again. 
//			SET_CHARACTER_CREATOR_STATE(CHARACTER_CREATOR_STATE_RUNNING)

			IF characterData.iNumOfAttempts > 20
			
				CPRINTLN(DEBUG_NET_CHARACTER, "MAINTAIN_CHARACTER_CREATOR_NAME_ENTRY - characterData.iNumOfAttempts > 20 move to cloud connection screen ")
				BUSYSPINNER_OFF()
				SET_CLOUD_CHOKED_IN_TRANSITION(TRUE)
				HUD_CHANGE_STATE(HUD_STATE_CLOUD_FAIL)
				TRANSITION_CHANGE_STATE(TRANSITION_STATE_TERMINATE_SESSION)
			
			ENDIF
			

		ELSE
			CPRINTLN(DEBUG_NET_CHARACTER, "MAINTAIN_CHARACTER_CREATOR_NAME_ENTRY - Player has entered name: ", GET_MP_STRING_CHARACTER_STAT(MP_STAT_CHAR_NAME, characterData.iCharacterSelectedSlot))
			SET_CHARACTER_CONTROLLER_BIT(ciCHARACTER_CONTROLLER_NAME_HAS_BEEN_ENTERED_BIT)
			
			IF GET_CHARACTER_CREATOR_CURRENT_SELECTION_FOR_MENU(characterData, ciCHARACTER_CREATOR_COLUMN_MAIN_MENU) = ciCHARACTER_CREATOR_MAIN_MENU_CONTINUE
				SET_CHARACTER_CREATOR_STATE(CHARACTER_CREATOR_STATE_CONFIRMATION)
				UPDATE_CHARACTER_CONTROLLER_BUTTONS(characterData)
			ELSE
				POPULATE_CHARACTER_CREATOR_MAIN_MENU(characterData, TRUE)
				SET_CHARACTER_CREATOR_STATE(CHARACTER_CREATOR_STATE_RUNNING)
			ENDIF
			
			// Update the board to reflect the name change
			UPDATE_CHARACTER_SELECTOR_BOARDS(characterData, Placement)
		ENDIF
	ELSE
		IF bHasQuit
		
			CPRINTLN(DEBUG_NET_CHARACTER, "MAINTAIN_CHARACTER_CREATOR_NAME_ENTRY - Player has quit: ", GET_MP_STRING_CHARACTER_STAT(MP_STAT_CHAR_NAME, characterData.iCharacterSelectedSlot))
		
			// Repopulate to make this appear
			POPULATE_CHARACTER_CREATOR_MAIN_MENU(characterData)
			
			// Player quit or failed, have to go back to main state
			SET_CHARACTER_CREATOR_STATE(CHARACTER_CREATOR_STATE_RUNNING)
			UPDATE_CHARACTER_CONTROLLER_BUTTONS(characterData)
		ENDIF
	ENDIF

	
ENDPROC

/// PURPOSE:
///    Draws the photo overlay on screen for the photo
PROC DRAW_CHARACTER_CREATOR_PHOTO_UI(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData)
	SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)					
	DRAW_SCALEFORM_MOVIE_FULLSCREEN(characterData.sfCameraOverlay, 255, 255, 255, 255)
	SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(FALSE)
ENDPROC



/// PURPOSE:
///    Handle the taking of the mugshot photo
PROC PROCESS_CHARACTER_CREATOR_TAKE_PHOTO(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData)

	SWITCH GET_CHARACTER_CREATOR_PHOTO_STATE()
	
		CASE CHARACTER_CREATOR_PHOTO_INIT
			DRAW_CHARACTER_CREATOR_PHOTO_UI(characterData)
			
			characterData.iCharacterCreatorPhotoTimer = GET_GAME_TIMER()
			
			// Bring on the FLASH
			SET_CHARACTER_CONTROLLER_TIME_OF_DAY(characterData, CHARACTER_CONTROLLER_TIME_OF_DAY_MIDNIGHT)
			
			PLAY_SOUND(-1, "Take_Picture", "MUGSHOT_CHARACTER_CREATION_SOUNDS")
			
			SET_CHARACTER_CREATOR_PHOTO_STATE(CHARACTER_CREATOR_PHOTO_EXECUTE)
		BREAK
		
	
		CASE CHARACTER_CREATOR_PHOTO_EXECUTE
				
			DRAW_CHARACTER_CREATOR_PHOTO_UI(characterData)
				
			IF ABSI(GET_GAME_TIMER() - characterData.iCharacterCreatorPhotoTimer) >= ciCHARACTER_CREATOR_PHOTO_SHUTTER_SPEED
							
				IF IS_PC_VERSION()
					CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_CREATOR_TAKE_PHOTO - Calling SET_CUTSCENE_MULTIHEAD_FADE(FALSE, TRUE, TRUE)")
					SET_CUTSCENE_MULTIHEAD_FADE(TRUE, TRUE, TRUE)
				ENDIF
										
				// Close the shutter
				BEGIN_SCALEFORM_MOVIE_METHOD(characterData.sfCameraOverlay, "CLOSE_SHUTTER")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ciCHARACTER_CREATOR_PHOTO_SHUTTER_SPEED)
				END_SCALEFORM_MOVIE_METHOD()
				
				characterData.iCharacterCreatorPhotoTimer = GET_GAME_TIMER()
						
				CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_CREATOR_TAKE_PHOTO - Photo begun.")
										
				SET_CHARACTER_CREATOR_PHOTO_STATE(CHARACTER_CREATOR_PHOTO_WAIT_FOR_SHUTTER)
			#IF IS_DEBUG_BUILD
			ELSE			  
				CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_CREATOR_TAKE_PHOTO - Waiting in CHARACTER_CREATOR_PHOTO_EXECUTE")
			#ENDIF
				
			ENDIF
		BREAK
		
		CASE CHARACTER_CREATOR_PHOTO_WAIT_FOR_SHUTTER
			
			DRAW_CHARACTER_CREATOR_PHOTO_UI(characterData)
			
			// Wait until our shutter is closed
			IF ABSI(GET_GAME_TIMER() - characterData.iCharacterCreatorPhotoTimer) >= ciCHARACTER_CREATOR_PHOTO_SHUTTER_SPEED
				
				// Request the taking of a photo
				IF BEGIN_TAKE_HIGH_QUALITY_PHOTO()
				
					// Update our camera position
					SET_CHARACTER_CONTROLLER_CAMERA_ACTIVE(characterData, CHARACTER_CONTROLLER_CAMERA_PHOTO)	
					
					// Move us on			
					SET_CHARACTER_CREATOR_PHOTO_STATE(CHARACTER_CREATOR_PHOTO_FREEZE_SCREEN)
				#IF IS_DEBUG_BUILD
				ELSE			  
					CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_CREATOR_TAKE_PHOTO - Waiting for BEGIN_TAKE_HIGH_QUALITY_PHOTO to return TRUE")
				#ENDIF
				ENDIF
			#IF IS_DEBUG_BUILD
			ELSE			  
				CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_CREATOR_TAKE_PHOTO - Waiting in CHARACTER_CREATOR_PHOTO_FREEZE_SCREEN")
			#ENDIF
				
			ENDIF
			
		BREAK
		
		CASE CHARACTER_CREATOR_PHOTO_FREEZE_SCREEN
			
			DRAW_CHARACTER_CREATOR_PHOTO_UI(characterData)
			
			CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_CREATOR_TAKE_PHOTO - Photo begun. SET_SKYFREEZE_FROZEN(), GET_SKYFREEZE_STAGE() = ", ENUM_TO_INT(GET_SKYFREEZE_STAGE()))

			// Freeze the renderphases for the photo
			SET_SKYFREEZE_FROZEN(TRUE)
			
			// Move us on
			SET_CHARACTER_CREATOR_PHOTO_STATE(CHARACTER_CREATOR_PHOTO_ANIMATE_SHUTTER)
		BREAK
		
		CASE CHARACTER_CREATOR_PHOTO_ANIMATE_SHUTTER
			
			DRAW_CHARACTER_CREATOR_PHOTO_UI(characterData)
			
			// Wait until the photo has been taken
			SWITCH GET_STATUS_OF_TAKE_HIGH_QUALITY_PHOTO()
				CASE PHOTO_OPERATION_SUCCEEDED
					CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_CREATOR_TAKE_PHOTO - GET_STATUS_OF_TAKE_HIGH_QUALITY_PHOTO() = PHOTO_OPERATION_SUCCEEDED, moving on to CREATING_LIST_OF_PHOTOS")
					SET_CHARACTER_CREATOR_PHOTO_STATE(CHARACTER_CREATOR_PHOTO_START_CREATING_LIST_OF_PHOTOS)
				BREAK
				
				// Wait for this to be complete
				CASE PHOTO_OPERATION_IN_PROGRESS
					CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_CREATOR_TAKE_PHOTO - GET_STATUS_OF_TAKE_HIGH_QUALITY_PHOTO() = PHOTO_OPERATION_IN_PROGRESS")	
				BREAK
				
				CASE PHOTO_OPERATION_FAILED
					CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_CREATOR_TAKE_PHOTO - GET_STATUS_OF_TAKE_HIGH_QUALITY_PHOTO() = PHOTO_OPERATION_FAILED, allow retake?")
					SET_CHARACTER_CREATOR_PHOTO_STATE(CHARACTER_CREATOR_PHOTO_COMPLETE)
				BREAK
			ENDSWITCH
			
		BREAK
		
		CASE CHARACTER_CREATOR_PHOTO_START_CREATING_LIST_OF_PHOTOS
		
			DRAW_CHARACTER_CREATOR_PHOTO_UI(characterData)

			//CLEAR_STATUS_OF_SORTED_LIST_OPERATION()

			IF QUEUE_OPERATION_TO_CREATE_SORTED_LIST_OF_PHOTOS(TRUE)
		
				// If we have had success, move to saving
				CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_CREATOR_TAKE_PHOTO - GET_STATUS_OF_TAKE_HIGH_QUALITY_PHOTO() = PHOTO_OPERATION_SUCCEEDED")
				SET_CHARACTER_CREATOR_PHOTO_STATE(CHARACTER_CREATOR_PHOTO_START_SAVE)
			ELSE
				CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_CREATOR_TAKE_PHOTO - QUEUE_OPERATION_TO_CREATE_SORTED_LIST_OF_PHOTOS() = FALSE, allow retake?")
				SET_CHARACTER_CREATOR_PHOTO_STATE(CHARACTER_CREATOR_PHOTO_COMPLETE)
			ENDIF
		
		BREAK
		
		CASE CHARACTER_CREATOR_PHOTO_START_SAVE
			
			DRAW_CHARACTER_CREATOR_PHOTO_UI(characterData)
			
			// This checks to see if we can store a photo
			SWITCH GET_STATUS_OF_SORTED_LIST_OPERATION(TRUE)
				
				CASE PHOTO_OPERATION_SUCCEEDED
					IF GET_CURRENT_NUMBER_OF_CLOUD_PHOTOS() < GET_MAXIMUM_NUMBER_OF_CLOUD_PHOTOS()
						SET_TAKEN_PHOTO_IS_MUGSHOT(TRUE)
					// Attempt saving the photo
						IF SAVE_HIGH_QUALITY_PHOTO(-1)
					
							CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_CREATOR_TAKE_PHOTO - GET_STATUS_OF_SORTED_LIST_OPERATION(TRUE) = PHOTO_OPERATION_SUCCEEDED")
							SET_CHARACTER_CREATOR_PHOTO_STATE(CHARACTER_CREATOR_PHOTO_COMPLETE_SAVE)
						ELSE
							
							CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_CREATOR_TAKE_PHOTO - SAVE_HIGH_QUALITY_PHOTO(-1) = FALSE")
							SET_CHARACTER_CREATOR_PHOTO_STATE(CHARACTER_CREATOR_PHOTO_COMPLETE)
						ENDIF
					ELSE
						CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_CREATOR_TAKE_PHOTO - NO SPACE LEFT FOR ANOTHER SNAPMATIC PHOTO")
						SET_CHARACTER_CREATOR_PHOTO_STATE(CHARACTER_CREATOR_PHOTO_COMPLETE)
					ENDIF
				BREAK
				
				// Wait for this to be complete
				CASE PHOTO_OPERATION_IN_PROGRESS
					CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_CREATOR_TAKE_PHOTO - GET_STATUS_OF_SORTED_LIST_OPERATION(TRUE) = PHOTO_OPERATION_IN_PROGRESS")
				BREAK
				
				CASE PHOTO_OPERATION_FAILED
					CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_CREATOR_TAKE_PHOTO - GET_STATUS_OF_SORTED_LIST_OPERATION(TRUE) = PHOTO_OPERATION_FAILED")
					SET_CHARACTER_CREATOR_PHOTO_STATE(CHARACTER_CREATOR_PHOTO_COMPLETE)
				BREAK
			
			ENDSWITCH
		BREAK
		
		CASE CHARACTER_CREATOR_PHOTO_COMPLETE_SAVE
			
			DRAW_CHARACTER_CREATOR_PHOTO_UI(characterData)
			
			SWITCH GET_STATUS_OF_SAVE_HIGH_QUALITY_PHOTO()
				CASE PHOTO_OPERATION_SUCCEEDED
					CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_CREATOR_TAKE_PHOTO - GET_STATUS_OF_SAVE_HIGH_QUALITY_PHOTO() = PHOTO_OPERATION_SUCCEEDED, set bit so no photos can still be taken")
					
					SET_CHARACTER_CONTROLLER_BIT(ciCHARACTER_CONTROLLER_PHOTO_TAKEN)
					SET_CHARACTER_CREATOR_PHOTO_STATE(CHARACTER_CREATOR_PHOTO_COMPLETE)
				BREAK
				
				// Wait for this to be complete
				CASE PHOTO_OPERATION_IN_PROGRESS
					CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_CREATOR_TAKE_PHOTO - GET_STATUS_OF_SAVE_HIGH_QUALITY_PHOTO() = PHOTO_OPERATION_IN_PROGRESS")
				BREAK
				
				CASE PHOTO_OPERATION_FAILED
					CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_CREATOR_TAKE_PHOTO - GET_STATUS_OF_SAVE_HIGH_QUALITY_PHOTO() = PHOTO_OPERATION_FAILED")
					SET_CHARACTER_CREATOR_PHOTO_STATE(CHARACTER_CREATOR_PHOTO_COMPLETE)
				BREAK
			ENDSWITCH
			
		BREAK
		
		CASE CHARACTER_CREATOR_PHOTO_COMPLETE
			
			// Clean up UI and free up the memory before moving back to ready screen
			DRAW_CHARACTER_CREATOR_PHOTO_UI(characterData)
			
			CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_CREATOR_TAKE_PHOTO - Calling FREE_MEMORY_FOR_HIGH_QUALITY_PHOTO() and moving back to READY screen")
			
			BEGIN_SCALEFORM_MOVIE_METHOD(characterData.sfCameraOverlay, "SHOW_PHOTO_FRAME")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
			END_SCALEFORM_MOVIE_METHOD()
						
			BEGIN_SCALEFORM_MOVIE_METHOD(characterData.sfCameraOverlay, "SHOW_PHOTO_BORDER")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(cfCHARACTER_CREATOR_PHOTO_BORDER_ROTATION)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(cfCHARACTER_CREATOR_PHOTO_X)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(cfCHARACTER_CREATOR_PHOTO_Y)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ciCHARACTER_CREATOR_PHOTO_X_SCALE)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ciCHARACTER_CREATOR_PHOTO_Y_SCALE)
			END_SCALEFORM_MOVIE_METHOD()
			
			BEGIN_SCALEFORM_MOVIE_METHOD(characterData.sfCameraOverlay, "OPEN_SHUTTER")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ciCHARACTER_CREATOR_PHOTO_SHUTTER_SPEED)
			END_SCALEFORM_MOVIE_METHOD()
			
			//SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(characterData.sfCameraOverlay)
			
			FREE_MEMORY_FOR_HIGH_QUALITY_PHOTO()
			
			characterData.iCharacterCreatorPhotoTimer = GET_GAME_TIMER()
			
			// update our confirm state
			//SET_CHARACTER_CONTROLLER_CAMERA_ACTIVE(characterData, CHARACTER_CONTROLLER_CAMERA_CREATOR_OVERVIEW)
			SET_CHARACTER_CREATOR_CONFIRM_STATE(CHARACTER_CREATOR_CONFIRM_STATE_CLEANUP_CONFIRM)
			
			// Refresh the button so they can't take another one if we were successful
			//UPDATE_CHARACTER_CONTROLLER_BUTTONS(characterData)
		BREAK
	ENDSWITCH
	SUPPRESS_FRONTEND_RENDERING_THIS_FRAME()
ENDPROC

/// PURPOSE:
///    Removes all child peds from the muted audio group
PROC PROCESS_REMOVE_CHILD_PEDS_FROM_MUTED_AUDIO_GROUP(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData)
	CHARACTER_CREATOR_PED creatorPed = characterData.eActiveChild
	
	// Remove current ped from muted audio group
	IF IS_CHARACTER_CONTROLLER_PED_OK(characterData.characterCreatorPeds[creatorPed])
		REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(characterData.characterCreatorPeds[creatorPed])
	ENDIF
	
	// Switch over the peds to the hidden one
	IF creatorPed = CHARACTER_CREATOR_SON
		creatorPed = CHARACTER_CREATOR_DAUGHTER
	ELSE
		creatorPed = CHARACTER_CREATOR_SON
	ENDIF 
	
	// Remove hidden ped from muted audio group
	IF IS_CHARACTER_CONTROLLER_PED_OK(characterData.characterCreatorPeds[creatorPed])
		REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(characterData.characterCreatorPeds[creatorPed])
	ENDIF
ENDPROC

/// PURPOSE:
///    Maintains the display of the ready screen and 
PROC MAINTAIN_CHARACTER_CREATOR_CONFIRMATION_SCREEN(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, MPHUD_PLACEMENT_TOOLS& Placement)
	
	
	SWITCH GET_CHARACTER_CREATOR_CONFIRM_STATE()
		CASE CHARACTER_CREATOR_CONFIRM_STATE_LOADING
			
			characterData.sfCameraOverlay = REQUEST_SCALEFORM_MOVIE("DIGITAL_CAMERA")
			
			IF HAS_SCALEFORM_MOVIE_LOADED(characterData.sfCameraOverlay)
				
				// Show the remaining photo cloud overlay
				BEGIN_SCALEFORM_MOVIE_METHOD(characterData.sfCameraOverlay, "SHOW_REMAINING_PHOTOS")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
				END_SCALEFORM_MOVIE_METHOD()
				
				BEGIN_SCALEFORM_MOVIE_METHOD(characterData.sfCameraOverlay, "SET_REMAINING_PHOTOS")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
				END_SCALEFORM_MOVIE_METHOD()
				
				// Display the entire UI
				BEGIN_SCALEFORM_MOVIE_METHOD(characterData.sfCameraOverlay, "SHOW_PHOTO_FRAME")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
				END_SCALEFORM_MOVIE_METHOD()
				
				// Hide the border
				BEGIN_SCALEFORM_MOVIE_METHOD(characterData.sfCameraOverlay, "SHOW_PHOTO_BORDER")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
				END_SCALEFORM_MOVIE_METHOD()
				
				// Open the shutter
				BEGIN_SCALEFORM_MOVIE_METHOD(characterData.sfCameraOverlay, "OPEN_SHUTTER")
				END_SCALEFORM_MOVIE_METHOD()
				
				CPRINTLN(DEBUG_NET_CHARACTER, "MAINTAIN_CHARACTER_CREATOR_CONFIRMATION_SCREEN - Scaleform ready, display and open shutter")
				
				// Make the ped pose
				SET_CHARACTER_CREATOR_PED_POSE_FOR_PHOTO(characterData)
				
				// Hide all our columns
				DISPLAY_CHARACTER_CREATOR_COLUMN(-1)
				
				// Clean up UI and free up the memory before moving back to ready screen
				DRAW_CHARACTER_CREATOR_PHOTO_UI(characterData)
				
				SET_CHARACTER_CONTROLLER_CAMERA_ACTIVE(characterData, CHARACTER_CONTROLLER_CAMERA_CREATOR_MUGSHOT_ZOOMED_CENTRED)
					
				SET_CHARACTER_CREATOR_CONFIRM_STATE(CHARACTER_CREATOR_CONFIRM_STATE_RENDER)
			ENDIF
		BREAK
		
		CASE CHARACTER_CREATOR_CONFIRM_STATE_RENDER
			
			// Draw the scaleform
			DRAW_CHARACTER_CREATOR_PHOTO_UI(characterData)
			
			// Listen for accept press
			IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
			OR (IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT) AND NOT IS_MOUSE_ROLLED_OVER_INSTRUCTIONAL_BUTTONS())
				CPRINTLN(DEBUG_NET_CHARACTER, "MAINTAIN_CHARACTER_CREATOR_CONFIRMATION_SCREEN - player confirmed on photo screen. Take photo and join GTA Online")
			
				PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
				
				SET_CHARACTER_CREATOR_PHOTO_STATE(CHARACTER_CREATOR_PHOTO_INIT)
				SET_CHARACTER_CREATOR_CONFIRM_STATE(CHARACTER_CREATOR_CONFIRM_STATE_TAKE_PHOTO)
			ELIF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
			OR (IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_CURSOR_CANCEL) AND NOT IS_MOUSE_ROLLED_OVER_INSTRUCTIONAL_BUTTONS())
				CPRINTLN(DEBUG_NET_CHARACTER, "MAINTAIN_CHARACTER_CREATOR_CONFIRMATION_SCREEN - player cancelled on join screen")
				
				SET_CHARACTER_CREATOR_CONFIRM_STATE(CHARACTER_CREATOR_CONFIRM_STATE_CLEANUP_REJECT)
			ENDIF
		BREAK
		
		CASE CHARACTER_CREATOR_CONFIRM_STATE_CLEANUP_REJECT
		
			// Get rid of our pose for the photo
			CLEAR_CHARACTER_CREATOR_PED_POSE_FOR_PHOTO(characterData)
		
			// Re-display the pause menu
			DISPLAY_CHARACTER_CREATOR_COLUMN(ciCHARACTER_CREATOR_COLUMN_MAIN_MENU)
		
			// Move the camera back out to the overview position
			SET_CHARACTER_CONTROLLER_CAMERA_ACTIVE(characterData, CHARACTER_CONTROLLER_CAMERA_CREATOR_OVERVIEW)
			
			// Move back to the running state of the creator
			SET_CHARACTER_CREATOR_CONFIRM_STATE(CHARACTER_CREATOR_CONFIRM_STATE_LOADING)
			SET_CHARACTER_CREATOR_STATE(CHARACTER_CREATOR_STATE_RUNNING)
			
			// Refresh our buttons again for moving back to the menu
			UPDATE_CHARACTER_CONTROLLER_BUTTONS(characterData)
		BREAK
		
		CASE CHARACTER_CREATOR_CONFIRM_STATE_TAKE_PHOTO
		
			// Stop all sounds before playing mugshot sound
			IF NOT HAS_SOUND_FINISHED(characterData.iSoundID)
				STOP_SOUND(characterData.iSoundID)
			ENDIF
			
			// Stop all other sounds
			STOP_SOUND(-1)
			
			// Maintain the taking of a photo every frame
			PROCESS_CHARACTER_CREATOR_TAKE_PHOTO(characterData)
		BREAK
		
		CASE CHARACTER_CREATOR_CONFIRM_STATE_CLEANUP_CONFIRM
			
			DRAW_CHARACTER_CREATOR_PHOTO_UI(characterData)
			
			// Ensure crew emblem correctly applied for active child B* 2269591
			UPDATE_CHARACTER_CREATOR_PED_DECAL(characterData, characterData.eActiveChild)
			
			// Wait for the delay before moving on (this will be a frozen image of the ped)
			IF ABSI(GET_GAME_TIMER() - characterData.iCharacterCreatorPhotoTimer) >= ciCHARACTER_CREATOR_PHOTO_DELAY
			
				#IF IS_DEBUG_BUILD
					// If widget is set, then loop us to the reject so we can go again
					IF characterData.bSuppressMoveToGameFromPhoto
						SET_SKYFREEZE_CLEAR()
						SET_CHARACTER_CREATOR_CONFIRM_STATE(CHARACTER_CREATOR_CONFIRM_STATE_CLEANUP_REJECT)
						EXIT
					ENDIF
				#ENDIF
				
				SET_TRANSITION_STRING("HUD_JOINING")
				CHECK_LOADING_ICON_CHANGE(Placement)
				SET_SCALEFORM_LOADING_ICON_DISPLAY(Placement.ScaleformLoadingStruct, LOADING_ICON_LOADING)
				Placement.ScaleformLoadingStruct.sMainStringSlot = GET_TRANSITION_STRING()
				RUN_SCALEFORM_LOADING_ICON( Placement.ScaleformLoadingStruct, SHOULD_REFRESH_SCALEFORM_LOADING_ICON(Placement.ScaleformLoadingStruct))
				LOAD_UP_LAST_FRAME_DATA(Placement)

				// Trigger fade out and save our character.
				DO_SCREEN_FADE_OUT(350)
				
				// Release the audio banks
				IF characterData.bAudioBanksRequested
					RELEASE_NAMED_SCRIPT_AUDIO_BANK("Mugshot_Character_Creator")
					RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_GTAO/MUGSHOT_ROOM")
					characterData.bAudioBanksRequested = FALSE
				ENDIF
					
				characterData.iCharacterCreatorPhotoTimer = GET_GAME_TIMER()
				
				#IF FEATURE_GEN9_STANDALONE
				IF NETWORK_GET_MP_WINDFALL_AVAILABLE()
					SET_LOADING_SCREEN_BLANK()
				ENDIF
				#ENDIF // FEATURE_GEN9_STANDALONE
				
				SET_CHARACTER_CREATOR_CONFIRM_STATE(CHARACTER_CREATOR_CONFIRM_STATE_PUSH_TO_GAME)
			ENDIF
		BREAK
		
		CASE CHARACTER_CREATOR_CONFIRM_STATE_PUSH_TO_GAME
			 
			// Draw the scaleform each frame
			DRAW_CHARACTER_CREATOR_PHOTO_UI(characterData)
			
			SET_TRANSITION_STRING("HUD_JOINING")
			CHECK_LOADING_ICON_CHANGE(Placement)
			SET_SCALEFORM_LOADING_ICON_DISPLAY(Placement.ScaleformLoadingStruct, LOADING_ICON_LOADING)
			Placement.ScaleformLoadingStruct.sMainStringSlot = GET_TRANSITION_STRING()
			RUN_SCALEFORM_LOADING_ICON( Placement.ScaleformLoadingStruct, SHOULD_REFRESH_SCALEFORM_LOADING_ICON(Placement.ScaleformLoadingStruct))
			LOAD_UP_LAST_FRAME_DATA(Placement)
			
			IF HAS_CHANGED_MAP_OVER_TO_MP(Placement, TRUE, TRUE)
				IF ABSI(GET_GAME_TIMER() - characterData.iCharacterCreatorPhotoTimer) >= ciCHARACTER_CREATOR_PHOTO_COMPLETE
					// Release the scaleform
					SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(characterData.sfCameraOverlay)
					
					PROCESS_CHARACTER_CREATOR_SAVE_NEW_CHARACTER(characterData, Placement)
					
					// Check we are not banned and post our character to facebook
					IF NETWORK_HAVE_ROS_SOCIAL_CLUB_PRIV()
						g_bPostCharacterFacebookUpdate = TRUE
						g_iCharacterFacebookUpdateState = MP_CHARACTER_FACEBOOK_POST_VALIDATE
						
						CPRINTLN(DEBUG_NET_CHARACTER, "MAINTAIN_CHARACTER_CREATOR_CONFIRMATION_SCREEN - NETWORK_HAVE_ROS_SOCIAL_CLUB_PRIV() = TRUE, can post to Facebook")
						
					#IF IS_DEBUG_BUILD
					ELSE
						CPRINTLN(DEBUG_NET_CHARACTER, "MAINTAIN_CHARACTER_CREATOR_CONFIRMATION_SCREEN - NETWORK_HAVE_ROS_SOCIAL_CLUB_PRIV() = FALSE, can not post to Facebook")
					#ENDIF
					ENDIF

					IF IS_PC_VERSION()
						CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_CREATOR_TAKE_PHOTO - Calling SET_CUTSCENE_MULTIHEAD_FADE(TRUE, TRUE, FALSE)")
						SET_CUTSCENE_MULTIHEAD_FADE(FALSE, TRUE, TRUE)
					ENDIF
					
					// Cleanup the pause menu
					SET_FRONTEND_ACTIVE(FALSE)
				ENDIF
			ENDIF
		BREAK
		
	ENDSWITCH	
	
	// Disable direction input while taking mugshot *2063426
	DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_LS)
	DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
	DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
	DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_AXIS_X)
	DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_AXIS_Y)
	
	// Suppress UI except from displaying user options to take photo
	IF GET_CHARACTER_CREATOR_CONFIRM_STATE() != CHARACTER_CREATOR_CONFIRM_STATE_RENDER
		SUPPRESS_FRONTEND_RENDERING_THIS_FRAME()
	ENDIF
ENDPROC


/// PURPOSE:
///    Handles the various states we can be in on the menu
PROC MAINTAIN_CHARACTER_CREATOR_MENU(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData)

	// Listen for scaleform event to indicate a move up or down in the menu
	IF HAS_MENU_LAYOUT_CHANGED_EVENT_OCCURRED()
		
		INT iMenuID
		FRONTEND_MENU_SCREEN aCurrentScreen, aNextScreen
		
		GET_MENU_LAYOUT_CHANGED_EVENT_DETAILS(aCurrentScreen, aNextScreen, iMenuID)
		
		// Set the current selection of the menu
		CPRINTLN(DEBUG_NET_CHARACTER, "MAINTAIN_CHARACTER_CREATOR_MENU - HAS_MENU_LAYOUT_CHANGED_EVENT_OCCURRED() = TRUE")
		SET_CHARACTER_CREATOR_CURRENT_SELECTION(characterData, iMenuID)
		
		PROCESS_CHARACTER_CREATOR_MENU_ROW_CHANGE(characterData)
	ELSE
		PROCESS_CHARACTER_CREATOR_MENU_INPUT(characterData)
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Handle the player quitting - should go back to select screen?
PROC MAINTAIN_CHARACTER_CREATOR_QUIT(MPHUD_PLACEMENT_TOOLS& Placement)
	
	CPRINTLN(DEBUG_NET_CHARACTER, "MAINTAIN_CHARACTER_CREATOR_QUIT - Player has quit creator")	
	
	Placement.bSelectedRPBoost = FALSE
	PRINTLN("[RPBOOST] Placement.bSelectedRPBoost = FALSE 2 ")
	g_bUsedRPBoost = FALSE
	
	IF HAS_ENTERED_CHAR_CREATION_FROM_ALTERATION_PROMPT()
	#IF FEATURE_GEN9_STANDALONE
	OR GET_CHOSEN_MP_CHARACTER_SLOT() >= 0
	#ENDIF
//		IF IS_CHARACTER_CONTROLLER_BIT_SET(characterData, ciCHARACTER_CONTROLLER_EDIT_PED_FROM_OLD_DATA)
//		
//			CPRINTLN(DEBUG_NET_CHARACTER, "MAINTAIN_CHARACTER_CREATOR_QUIT - Player was editing an OLD character. Go back to selector")	
//		
//			CLEAR_CHARACTER_CONTROLLER_BIT(characterData, ciCHARACTER_CONTROLLER_EDIT_PED_FROM_OLD_DATA)
//			
//			SET_CHARACTER_SELECTOR_STATE(characterData, CHARACTER_SELECTOR_STATE_INIT)
//			SET_CHARACTER_CONTROLLER_SCREEN_STATE(characterData, CHARACTER_CONTROLLER_SCREEN_SELECT)	
//		ELSE
			
			CPRINTLN(DEBUG_NET_CHARACTER, "MAINTAIN_CHARACTER_CREATOR_QUIT - Set Placement.HasCancelledFirstJoin = TRUE")
			SET_MIGRATION_NOT_FINISHED_BLOCK_SAVE(Placement)

			CANCEL_MP_JOIN(TRUE)
			RESET_PAUSE_MENU_WARP_REQUESTS()
			UNLOAD_PEDHEADSHOT(Placement.ScaleformAlertScreenStruct.HeadshotPic)
			
			RESET_ALL_EXTERNAL_TERMINATE_REASONS()
			
			//DELETE_ALL_SELECTION_MENUPED(Placement.SelectionPed)
			//DELETE_ALL_SELECTION_MENUPED(Placement.IdlePed)
			//DELETE_ALL_INACTIVE_MENUPED(Placement.SelectionPed, Placement.IdlePed)
				
			IF LOBBY_AUTO_MULTIPLAYER_FREEMODE()
			OR DID_BAIL_WHEN_ENTERING_ONLINE_FROM_BOOT()
				SET_FRONTEND_ACTIVE(FALSE)
				DO_SCREEN_FADE_OUT(0)
			ENDIF
			
			//RESET_BIRDS_ALPHA()
			RUN_TRANSITION_MENU_AUDIO(FALSE)
			UGC_CLEAR_QUERY_RESULTS_BY_OFFLINE_STATUS()
			IF GET_CONFIRM_INVITE_INTO_GAME_STATE()
				RUN_SCALEFORM_LOCK_RIGHT_MOUSE_BUTTON(FALSE)
				HUD_CHANGE_STATE(HUD_STATE_IGNORE_INVITE)
				TRANSITION_CHANGE_STATE(TRANSITION_STATE_WAIT_HUD_EXIT)
			ELSE
				RUN_SCALEFORM_LOCK_RIGHT_MOUSE_BUTTON(FALSE)				
				REQUEST_TRANSITION_TERMINATE_MP_SESSION()
			ENDIF
			
			CPRINTLN(DEBUG_NET_CHARACTER, "MAINTAIN_CHARACTER_CREATOR_QUIT - Player was editing, quit via Brenda's call")
			
			//RESET_CHARACTER_CONTROLLER_DATA(characterData)
			//DESTROY_ALL_CHARACTER_CREATOR_PEDS(characterData)	
//		ENDIF
	ELSE
		CPRINTLN(DEBUG_NET_CHARACTER, "MAINTAIN_CHARACTER_CREATOR_QUIT - Move back to Character Selector")
		
		SET_CHARACTER_SELECTOR_STATE(CHARACTER_SELECTOR_STATE_INIT)
		SET_CHARACTER_CONTROLLER_SCREEN_STATE(CHARACTER_CONTROLLER_SCREEN_SELECT)
		
		IF NOT IS_STAT_CHARACTER_ACTIVE(Placement.ScreenPlace.iSelectedCharacter) 
			CPRINTLN(DEBUG_NET_CHARACTER, "MAINTAIN_CHARACTER_CREATOR_QUIT - Reset our character team to 0")
			SET_STAT_CHARACTER_TEAM(0, Placement.ScreenPlace.iSelectedCharacter)
		ENDIF
	ENDIF	
	
ENDPROC

/// PURPOSE:
///    Maintain our warning screens
PROC MAINTAIN_CHARACTER_CREATOR_WARNING(MPHUD_PLACEMENT_TOOLS& Placement)

	TEXT_LABEL_15 tlWarningMsg
	TEXT_LABEL_15 tlAlert = "FACES_WARNH"
	
	// Reset PC mouse pointer while the screen is active.
	//IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
	//	SET_MOUSE_CURSOR_STYLE(MOUSE_CURSOR_STYLE_ARROW)
	//ENDIF
		
	IF IS_CHARACTER_CONTROLLER_BIT_SET(ciCHARACTER_CONTROLLER_QUIT_WARNING_BIT)
		
		IF IS_CHARACTER_CONTROLLER_BIT_SET(ciCHARACTER_CONTROLLER_EDIT_PED_FROM_OLD_DATA)
		#IF FEATURE_GEN9_STANDALONE
		OR GET_CHOSEN_MP_CHARACTER_SLOT() >= 0
		#ENDIF
			tlWarningMsg = "FACES_WARNSP"
		ELIF IS_CHANGED_CHARACTER_APPEARANCE()
			tlWarningMsg = "FACES_WARN6"
		ELSE
			tlWarningMsg = "FACES_WARN5"
			tlAlert = "FACES_WARNH2"
		ENDIF
		
		SET_WARNING_MESSAGE_WITH_HEADER(tlAlert, tlWarningMsg, (FE_WARNING_OKCANCEL))
	
		// Confirm quit
		IF IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
				
			PRINTLN("[CORONA] CONTROL_FM_MISSION_WARNING_SCREEN - cancel pressed")
			
			CLEAR_CHARACTER_CONTROLLER_BIT(ciCHARACTER_CONTROLLER_QUIT_WARNING_BIT)
			SET_CHARACTER_CREATOR_STATE(CHARACTER_CREATOR_STATE_RUNNING)
		ELSE
			IF IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
				PRINTLN("[CORONA] CONTROL_FM_MISSION_WARNING_SCREEN - accept pressed")
				
				CLEAR_CHARACTER_CONTROLLER_BIT(ciCHARACTER_CONTROLLER_QUIT_WARNING_BIT)
				SET_CHARACTER_CREATOR_STATE(CHARACTER_CREATOR_STATE_QUIT)
			ENDIF
		ENDIF		
		
	ELIF IS_CHARACTER_CONTROLLER_BIT_SET(ciCHARACTER_CONTROLLER_STATS_TO_ASSIGN)
		tlAlert = "FACES_WARNH2"
		tlWarningMsg = "FACE_W_1"
		
		SET_WARNING_MESSAGE_WITH_HEADER(tlAlert, tlWarningMsg, (FE_WARNING_OK))
	
		// Confirm quit
		IF IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
				
			PRINTLN("[CORONA] CONTROL_FM_MISSION_WARNING_SCREEN - accept pressed")
				
			CLEAR_CHARACTER_CONTROLLER_BIT(ciCHARACTER_CONTROLLER_STATS_TO_ASSIGN)
				
			SET_CHARACTER_CREATOR_STATE(CHARACTER_CREATOR_STATE_RUNNING)
		ENDIF	
	ELSE
		tlWarningMsg = "FACE_W_2"
		SET_WARNING_MESSAGE_WITH_HEADER(tlAlert, tlWarningMsg, (FE_WARNING_YESNO))
		
		// Confirm quit
		IF IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
				
			PRINTLN("[CORONA] CONTROL_FM_MISSION_WARNING_SCREEN - cancel pressed for normal exit")
			
			SET_CHARACTER_CREATOR_STATE(CHARACTER_CREATOR_STATE_RUNNING)
		ELSE
			IF IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
				PRINTLN("[CORONA] CONTROL_FM_MISSION_WARNING_SCREEN - accept pressed for normal exit")
				
				Placement.bSelectedRPBoost = FALSE
				PRINTLN("[RPBOOST] Placement.bSelectedRPBoost = FALSE 1 ")
				
				IF NOT HAS_ENTERED_CHAR_CREATION_FROM_ALTERATION_PROMPT()
					CPRINTLN(DEBUG_NET_CHARACTER, "CONTROL_FM_MISSION_WARNING_SCREEN - Move back to Character Selector")
					
					SET_CHARACTER_SELECTOR_STATE(CHARACTER_SELECTOR_STATE_INIT)
					SET_CHARACTER_CONTROLLER_SCREEN_STATE(CHARACTER_CONTROLLER_SCREEN_SELECT)
					
					IF NOT IS_STAT_CHARACTER_ACTIVE(Placement.ScreenPlace.iSelectedCharacter) 
						CPRINTLN(DEBUG_NET_CHARACTER, "CONTROL_FM_MISSION_WARNING_SCREEN - Reset our character team to 0")
						SET_STAT_CHARACTER_TEAM(0, Placement.ScreenPlace.iSelectedCharacter)
					ENDIF					
				ENDIF
				
				CLEAR_CHARACTER_CONTROLLER_BIT(ciCHARACTER_CONTROLLER_QUIT_WARNING_BIT)
				SET_CHARACTER_CREATOR_STATE(CHARACTER_CREATOR_STATE_QUIT)
			ENDIF
		ENDIF
		
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Process the walk in of the character
PROC PROCESS_CHARACTER_CREATOR_ANIM_WALK_IN(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData)
	
	CHARACTER_CREATOR_PED creatorPed = characterData.eActiveChild

	STRING strAnimDict = GET_CHARACTER_CONTROLLER_ANIM_DICT(ciCHARACTER_CREATOR_ANIM_DICT_CREATOR, 0, (creatorPed = CHARACTER_CREATOR_SON))
	SEQUENCE_INDEX seq
	
	// Apply the walk in animation to the main ped
	IF IS_CHARACTER_CONTROLLER_PED_OK(characterData.characterCreatorPeds[creatorPed])

		OPEN_SEQUENCE_TASK(seq)
			TASK_PLAY_ANIM_ADVANCED(NULL, strAnimDict, "Intro", GET_CHARACTER_CREATOR_ANIM_VECTOR(), GET_CHARACTER_CREATOR_ANIM_ROTATION(), NORMAL_BLEND_IN, NORMAL_BLEND_OUT, DEFAULT, AF_EXTRACT_INITIAL_OFFSET | AF_TURN_OFF_COLLISION)
			TASK_PLAY_ANIM(NULL, strAnimDict, "Loop", NORMAL_BLEND_IN, SLOW_BLEND_OUT, DEFAULT, AF_LOOPING | AF_TURN_OFF_COLLISION)		
		CLOSE_SEQUENCE_TASK(seq)
		
		TASK_PERFORM_SEQUENCE(characterData.characterCreatorPeds[creatorPed], seq)
		CLEAR_SEQUENCE_TASK(seq)
		
		FREEZE_ENTITY_POSITION(characterData.characterCreatorPeds[creatorPed], FALSE)
	ENDIF
	
	// Switch over the peds that we are animating
	IF creatorPed = CHARACTER_CREATOR_SON
		creatorPed = CHARACTER_CREATOR_DAUGHTER
	ELSE
		creatorPed = CHARACTER_CREATOR_SON
	ENDIF
	
	strAnimDict = GET_CHARACTER_CONTROLLER_ANIM_DICT(ciCHARACTER_CREATOR_ANIM_DICT_CREATOR, 0, (creatorPed = CHARACTER_CREATOR_SON))
	
	// Apply the walk in animation to the hidden ped so they are in the same process as the main character
	IF IS_CHARACTER_CONTROLLER_PED_OK(characterData.characterCreatorPeds[creatorPed])
		
		OPEN_SEQUENCE_TASK(seq)
			TASK_PLAY_ANIM_ADVANCED(NULL, strAnimDict, "Intro", GET_CHARACTER_CREATOR_ANIM_VECTOR(), GET_CHARACTER_CREATOR_ANIM_ROTATION(), NORMAL_BLEND_IN, NORMAL_BLEND_OUT, DEFAULT, AF_EXTRACT_INITIAL_OFFSET | AF_TURN_OFF_COLLISION)
			TASK_PLAY_ANIM(NULL, strAnimDict, "Loop", NORMAL_BLEND_IN, SLOW_BLEND_OUT, DEFAULT, AF_LOOPING | AF_TURN_OFF_COLLISION)		
		CLOSE_SEQUENCE_TASK(seq)
		
		TASK_PERFORM_SEQUENCE(characterData.characterCreatorPeds[creatorPed], seq)
		CLEAR_SEQUENCE_TASK(seq)
		
		FREEZE_ENTITY_POSITION(characterData.characterCreatorPeds[creatorPed], FALSE)
	ENDIF
	
	SET_CHARACTER_CREATOR_ANIM_STATE(CHARACTER_CREATOR_ANIM_WAIT_TO_FINISH)
ENDPROC

/// PURPOSE:
///    Process the anim to finish
PROC PROCESS_CHARACTER_CREATOR_ANIM_WAIT_TO_FINISH(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData)

	STRING strAnimDict = GET_CHARACTER_CONTROLLER_ANIM_DICT(ciCHARACTER_CREATOR_ANIM_DICT_CREATOR, 0, (characterData.eActiveChild = CHARACTER_CREATOR_SON))

	IF IS_CHARACTER_CONTROLLER_PED_OK(characterData.characterCreatorPeds[characterData.eActiveChild])
		IF NOT IS_ENTITY_PLAYING_ANIM(characterData.characterCreatorPeds[characterData.eActiveChild], strAnimDict, "Intro")
		OR IS_ENTITY_PLAYING_ANIM(characterData.characterCreatorPeds[characterData.eActiveChild], strAnimDict, "Loop")
			CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_CREATOR_ANIM_WAIT_TO_FINISH - Child has finished walk in, allow interaction")
			
			IF IS_SCREEN_FADED_OUT()
			OR IS_SCREEN_FADING_OUT()
				DO_SCREEN_FADE_IN(250)
			ENDIF
			
			SET_CHARACTER_CREATOR_STATE(CHARACTER_CREATOR_STATE_RUNNING)
		ENDIF
	ELSE
		CPRINTLN(DEBUG_NET_CHARACTER, "PROCESS_CHARACTER_CREATOR_ANIM_WAIT_TO_FINISH - Ped does not exist, move on")
			
		IF IS_SCREEN_FADED_OUT()
		OR IS_SCREEN_FADING_OUT()
			DO_SCREEN_FADE_IN(250)
		ENDIF
		
		SET_CHARACTER_CREATOR_STATE(CHARACTER_CREATOR_STATE_RUNNING)
	ENDIF
ENDPROC

/// PURPOSE:
///    Mute the hidden ped to stop sounds playing over themselves
PROC PROCESS_MUTE_HIDDEN_PED(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData)
	CHARACTER_CREATOR_PED creatorPed = characterData.eActiveChild
	
	// Switch over the peds to the hidden one
	IF creatorPed = CHARACTER_CREATOR_SON
		creatorPed = CHARACTER_CREATOR_DAUGHTER
	ELSE
		creatorPed = CHARACTER_CREATOR_SON
	ENDIF 
	
	// Add the hidden ped to the audio group to mute them
	IF IS_CHARACTER_CONTROLLER_PED_OK(characterData.characterCreatorPeds[creatorPed])
		ADD_ENTITY_TO_AUDIO_MIX_GROUP(characterData.characterCreatorPeds[creatorPed], "MUGSHOT_ROOMS_SCENE_MUGSHOT_ROOM_HIDDEN_PED_GROUP")
	ENDIF
	
	// Stop MP_MENU_SCENE to allow audio to play
	IF IS_AUDIO_SCENE_ACTIVE("MP_MENU_SCENE")
		STOP_AUDIO_SCENE("MP_MENU_SCENE")
	ENDIF
ENDPROC

/// PURPOSE:
///    Maintain our walk in animation
PROC MAINTAIN_CHARACTER_CREATOR_WALK_IN_ANIM(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData)
	
	SUPPRESS_CHARACTER_MENU_THIS_FRAME()
	
	SWITCH GET_CHARACTER_CREATOR_ANIM_STATE()
		CASE CHARACTER_CREATOR_ANIM_WALK_IN
			IF IS_SCREEN_FADED_OUT()
			OR IS_SCREEN_FADING_OUT()
				DO_SCREEN_FADE_IN(250)
			ENDIF
			PROCESS_CHARACTER_CREATOR_ANIM_WALK_IN(characterData)
		BREAK
		
		CASE CHARACTER_CREATOR_ANIM_WAIT_TO_FINISH
			PROCESS_CHARACTER_CREATOR_ANIM_WAIT_TO_FINISH(characterData)
		BREAK	
	ENDSWITCH	
ENDPROC








//--- MAIN CONTROLLER FUNCTIONS

/// PURPOSE:
///    Main control function of the character creator, initialises menus / data and listen for menu input.
///    Player input / choices directly impact character appearence through functions within MPHud_CharacterGeneration.sch
PROC MAINTAIN_GTA_ONLINE_CHARACTER_CREATOR(MPHUD_GTA_ONLINE_CHARACTER_DATA &characterData, MPHUD_PLACEMENT_TOOLS& Placement)	
	
	WARNING_SCREEN_RETURN result
	INT OriginalSlot
	INT OriginalSlotXpValue
	INT Rank
	STRING charName
	
	
	// Switch over the state we are in for the creator.
	SWITCH GET_CHARACTER_CREATOR_STATE()
	
		
		CASE CHARACTER_CREATOR_STATE_RP_PROMPT
			
			IF g_sMPtunables.bdisable_rank_copy = FALSE
			
							
				IF Placement.ScreenPlace.iSelectedCharacter  = 0
					OriginalSlot = 1
				ELIF Placement.ScreenPlace.iSelectedCharacter  = 1
					OriginalSlot = 0
				ENDIF
							
				
				IF GET_MP_BOOL_CHARACTER_STAT(MP_STAT_CHAR_ISACTIVE, Placement.ScreenPlace.iSelectedCharacter) = FALSE
				AND GET_MP_BOOL_CHARACTER_STAT(MP_STAT_CHAR_ISACTIVE,OriginalSlot) = TRUE
					
					OriginalSlotXpValue = GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_XP_FM, OriginalSlot, TRUE)
					
					IF GET_RANK_FROM_XP_VALUE(OriginalSlotXpValue) > 11
					
						IF g_sMPtunables.ifixed_rank_copy = -1
							IF OriginalSlotXpValue > g_sMPTunableArrays.iTopRankValues[120]
								OriginalSlotXpValue = g_sMPTunableArrays.iTopRankValues[120]
							ENDIF
							CPRINTLN(DEBUG_NET_CHARACTER, "[RPBOOST] asking - apply ", OriginalSlotXpValue, "RP OriginalSlot = ", OriginalSlot, " Placement.ScreenPlace.iSelectedCharacter = ", Placement.ScreenPlace.iSelectedCharacter, " "  )
						ELSE
							OriginalSlotXpValue = GET_XP_NEEDED_FOR_RANK(g_sMPtunables.ifixed_rank_copy)
							CPRINTLN(DEBUG_NET_CHARACTER, "[RPBOOST] asking - apply ", OriginalSlotXpValue, "RP Overriden by tunables OriginalSlot = ", OriginalSlot, " Placement.ScreenPlace.iSelectedCharacter = ", Placement.ScreenPlace.iSelectedCharacter, " "  )
						ENDIF
						
						
						Rank = GET_RANK_FROM_XP_VALUE(OriginalSlotXpValue)
						
						SUPPRESS_CHARACTER_MENU_THIS_FRAME()
					
						charName = GET_MP_STRING_CHARACTER_STAT(MP_STAT_CHAR_NAME, OriginalSlot)
										
						result = WARNING_SCREEN_ACCEPT_NEW_CHARACTER_RANK_BOOST(charName, OriginalSlot, Rank, g_sMPtunables.ifixed_rank_copy) 
						
						IF result = WARNING_SCREEN_RETURN_ACCEPT
						
							DO_SCREEN_FADE_OUT(0)
						
							
							
							CPRINTLN(DEBUG_NET_CHARACTER, "[RPBOOST] ACCEPTED") 
							
							
							
							g_bUsedRPBoost = TRUE
							Placement.bSelectedRPBoost = TRUE
							SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_XP_FM, OriginalSlotXpValue, Placement.ScreenPlace.iSelectedCharacter)
							SET_CHARACTER_CREATOR_STATE(CHARACTER_CREATOR_STATE_INIT)
							
							UPDATE_CHARACTER_SELECTOR_BOARDS(characterData, Placement)
						
						ELIF result = WARNING_SCREEN_RETURN_CANCEL
							DO_SCREEN_FADE_OUT(0)
							
							CPRINTLN(DEBUG_NET_CHARACTER, "[RPBOOST] Rejected ")
							Placement.bSelectedRPBoost = FALSE
							g_bUsedRPBoost = FALSE
							SET_CHARACTER_CREATOR_STATE(CHARACTER_CREATOR_STATE_INIT)
							
						ENDIF
						Placement.bRPBoostNotOffered = FALSE
					ELSE
						Placement.bSelectedRPBoost = FALSE
						g_bUsedRPBoost = FALSE
						SET_CHARACTER_CREATOR_STATE(CHARACTER_CREATOR_STATE_INIT)
						Placement.bRPBoostNotOffered = TRUE	
						CPRINTLN(DEBUG_NET_CHARACTER, "[RPBOOST] Char Rank < 12 - skip ")
					ENDIF
				ELSE
				
					Placement.bSelectedRPBoost = FALSE
					g_bUsedRPBoost = FALSE
					SET_CHARACTER_CREATOR_STATE(CHARACTER_CREATOR_STATE_INIT)
					Placement.bRPBoostNotOffered = TRUE	
					CPRINTLN(DEBUG_NET_CHARACTER, "[RPBOOST] (NewChar && hasOtherChar) = FALSE - skip ")
					
				ENDIF
			ELSE
				Placement.bSelectedRPBoost = FALSE
				g_bUsedRPBoost = FALSE
				SET_CHARACTER_CREATOR_STATE(CHARACTER_CREATOR_STATE_INIT)
				Placement.bRPBoostNotOffered = TRUE	
				
				CPRINTLN(DEBUG_NET_CHARACTER, "[RPBOOST] g_sMPtunables.bdisable_rank_copy = TRUE - skip ")
			
			ENDIF
			
		
		BREAK
	
		CASE CHARACTER_CREATOR_STATE_INIT
		
			CPRINTLN(DEBUG_NET_CHARACTER, "MAINTAIN_GTA_ONLINE_CHARACTER_CREATOR ")
		
			#IF IS_DEBUG_BUILD
				CHARACTER_CREATOR_DEBUG_PRINT_INIT_INFO()
			#ENDIF
			
			SUPPRESS_CHARACTER_MENU_THIS_FRAME()
			
			characterData.iCharacterSelectedSlot = Placement.ScreenPlace.iSelectedCharacter
					
			// Initialising the creator menu involves setting up the default view and any loading UI for peds
			INITIALISE_CHARACTER_CREATOR(characterData, Placement)
			
			// Update the camera to the new location
			SET_CHARACTER_CONTROLLER_CAMERA_ACTIVE(characterData, CHARACTER_CONTROLLER_CAMERA_CREATOR_OVERVIEW)
			
			// Set up the ped position and visibility
			SET_CHARACTER_CREATOR_PED_POSITION(characterData)
			
			// Make sure we have the board attached
			ATTACH_CHARACTER_CREATOR_BOARD_TO_PED(characterData.characterCreatorPeds[characterData.eActiveChild], characterData, GET_CHARACTER_SELECTOR_SLOT_FOR_CREATED_PED(characterData))
			
			// Update our menu state and set the highlight
			IF HAS_ENTERED_CHAR_CREATION_FROM_ALTERATION_PROMPT()
			AND NOT g_sMPTunables.bEnableCharacterGenderChange
				SET_FRONTEND_HIGHLIGHT(ciCHARACTER_CREATOR_COLUMN_MAIN_MENU, GET_CHARACTER_CREATOR_CURRENT_SELECTION(characterData)-1, TRUE)
			ELSE
				SET_FRONTEND_HIGHLIGHT(ciCHARACTER_CREATOR_COLUMN_MAIN_MENU, GET_CHARACTER_CREATOR_CURRENT_SELECTION(characterData), TRUE)
			ENDIF
			
			SET_CHARACTER_CONTROLLER_BIT(ciCHARACTER_CONTROLLER_ENTERED_CREATOR)	
			
			// Mute our hidden ped
			PROCESS_MUTE_HIDDEN_PED(characterData)
		BREAK
		
		CASE CHARACTER_CREATOR_STATE_WALK_IN
			MAINTAIN_CHARACTER_CREATOR_WALK_IN_ANIM(characterData)
		BREAK
		
		CASE CHARACTER_CREATOR_STATE_RUNNING
			MAINTAIN_CHARACTER_CREATOR_MENU(characterData)
		BREAK
		
		CASE CHARACTER_CREATOR_STATE_NAME_ENTRY
			MAINTAIN_CHARACTER_CREATOR_NAME_ENTRY(characterData, Placement)
		BREAK
		
		CASE CHARACTER_CREATOR_STATE_CONFIRMATION
			MAINTAIN_CHARACTER_CREATOR_CONFIRMATION_SCREEN(characterData, Placement)
		BREAK
		
		CASE CHARACTER_CREATOR_STATE_QUIT
			PROCESS_REMOVE_CHILD_PEDS_FROM_MUTED_AUDIO_GROUP(characterData)
			MAINTAIN_CHARACTER_CREATOR_QUIT(Placement)
		BREAK
		
		CASE CHARACTER_CREATOR_STATE_WARNING
			MAINTAIN_CHARACTER_CREATOR_WARNING(Placement)
		BREAK		
	ENDSWITCH
	
	// Maintain any animations required on the peds
	MAINTAIN_CHARACTER_CREATOR_PED_ANIMATIONS(characterData)
	MAINTAIN_CHARACTER_CREATOR_PRELOADING(characterData)
ENDPROC

