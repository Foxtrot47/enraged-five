
USING "screens_header.sch"
USING "net_events.sch"
USING "script_network.sch"
USING "Transition_Common.sch"

PROC INIT_MPHUD_NOT_CONNECTED(MPHUD_PLACEMENT_TOOLS& Placement)

	RESET_ALL_SPRITE_PLACEMENT_VALUES(Placement.SpritePlacement)


	Placement.TextPlacement[0].x = 0.308
	Placement.TextPlacement[0].y = 0.486 * 0.5626
	
	Placement.TextPlacement[1].x = 0.567
	Placement.TextPlacement[1].y = 0.707 * 0.5626	
	
	//Scaleform movie Bottom
	Placement.SpritePlacement[0].x = 0.389
	Placement.SpritePlacement[0].y = 0.467
	Placement.SpritePlacement[0].w = 1.0
	Placement.SpritePlacement[0].h = 1.0
	Placement.SpritePlacement[0].r = 255
	Placement.SpritePlacement[0].g = 255
	Placement.SpritePlacement[0].b = 255
	Placement.SpritePlacement[0].a = 200
	
ENDPROC




PROC RENDER_MPHUD_NOT_CONNECTED(MPHUD_PLACEMENT_TOOLS& Placement)

	
	SET_ALL_MENUPED_VISIBLE(Placement.SelectionPed, Placement.IdlePed, FALSE )		

	
	STRING sStatementText = "HUD_CONNPROB"
	STRING sLineTwoMes 
	STRING sLineOneMes 
	Placement.iButtonPressedRun = FALSE
	FE_WARNING_FLAGS Buttons 
	
//	#IF IS_DEBUG_BUILD
//	NET_NL()NET_PRINT("RENDER_MPHUD_NOT_CONNECTED Logic: ")
//	NET_NL()NET_PRINT("		-  PRIVALEGES_OKAY_FOR_MP() = ")NET_PRINT_BOOL( PRIVALEGES_OKAY_FOR_MP())
//	NET_NL()NET_PRINT("		-  NETWORK_IS_SIGNED_ONLINE() = ")NET_PRINT_BOOL( NETWORK_IS_SIGNED_ONLINE())
//	NET_NL()NET_PRINT("		-  NETWORK_IS_SIGNED_IN() = ")NET_PRINT_BOOL( NETWORK_IS_SIGNED_IN())
//	NET_NL()NET_PRINT("		-  NETWORK_HAVE_ONLINE_PRIVILEGES() = ")NET_PRINT_BOOL( NETWORK_HAVE_ONLINE_PRIVILEGES())
//	NET_NL()NET_PRINT("		-  IS_SYSTEM_UI_BEING_DISPLAYED() = ")NET_PRINT_BOOL( IS_SYSTEM_UI_BEING_DISPLAYED())
//	NET_NL()NET_PRINT("		-  HAS_NET_TIMER_EXPIRED_READ_ONLY(Placement.aTimer, 3000) = ")NET_PRINT_BOOL( HAS_NET_TIMER_EXPIRED_READ_ONLY(Placement.aTimer, 3000))
//	NET_NL()NET_PRINT("		-  NETWORK_IS_CLOUD_AVAILABLE() = ")NET_PRINT_BOOL( NETWORK_IS_CLOUD_AVAILABLE())
//	NET_NL()NET_PRINT("		-  HAS_NET_TIMER_STARTED(Placement.aTimer) = ")NET_PRINT_BOOL( HAS_NET_TIMER_STARTED(Placement.aTimer))
//	NET_NL()NET_PRINT("		-  DID_A_BAIL_HAPPEN = ")NET_PRINT_BOOL(DID_A_BAIL_HAPPEN())
//	NET_NL()NET_PRINT("		-  HAS_IMPORTANT_STATS_LOADED = ")NET_PRINT_BOOL(HAS_IMPORTANT_STATS_LOADED())
//
//	#ENDIF
	
	#IF IS_DEBUG_BUILD
	NET_NL()NET_PRINT("RENDER_MPHUD_NOT_CONNECTED Logic: ")
	NET_NL()NET_PRINT("		-  PRIVALEGES_OKAY_FOR_MP() = ")NET_PRINT_BOOL( PRIVALEGES_OKAY_FOR_MP())
	NET_NL()NET_PRINT("		-  NETWORK_IS_SIGNED_ONLINE() = ")NET_PRINT_BOOL( NETWORK_IS_SIGNED_ONLINE())
	NET_NL()NET_PRINT("		-  NETWORK_IS_SIGNED_IN() = ")NET_PRINT_BOOL( NETWORK_IS_SIGNED_IN())
	NET_NL()NET_PRINT("		-  g_SignInStructDetails.bIsSignedIn = ")NET_PRINT_BOOL(g_SignInStructDetails.bIsSignedIn)
	NET_NL()NET_PRINT("		-  g_SignInStructDetails.bIsDuplicateSignIn = ")NET_PRINT_BOOL(g_SignInStructDetails.bIsDuplicateSignIn)
	NET_NL()NET_PRINT("		-  g_SignInStructDetails.bWasOnline = ")NET_PRINT_BOOL(g_SignInStructDetails.bWasOnline)
	NET_NL()NET_PRINT("		-  g_SignInStructDetails.bWasSignedIn = ")NET_PRINT_BOOL(g_SignInStructDetails.bWasSignedIn)
	NET_NL()NET_PRINT("		-  g_SignInStructDetails.bIsOnline = ")NET_PRINT_BOOL(g_SignInStructDetails.bIsOnline)	
	NET_NL()NET_PRINT("		-  IS_SYSTEM_UI_BEING_DISPLAYED() = ")NET_PRINT_BOOL( IS_SYSTEM_UI_BEING_DISPLAYED())
	NET_NL()NET_PRINT("		-  HAS_NET_TIMER_EXPIRED_READ_ONLY(Placement.aTimer, 3000) = ")NET_PRINT_BOOL( HAS_NET_TIMER_EXPIRED_READ_ONLY(Placement.aTimer, 3000))
	NET_NL()NET_PRINT("		-  NETWORK_IS_CLOUD_AVAILABLE() = ")NET_PRINT_BOOL( NETWORK_IS_CLOUD_AVAILABLE())
	NET_NL()NET_PRINT("		-  HAS_NET_TIMER_STARTED(Placement.aTimer) = ")NET_PRINT_BOOL( HAS_NET_TIMER_STARTED(Placement.aTimer))
	NET_NL()NET_PRINT("		-  DID_A_BAIL_HAPPEN = ")NET_PRINT_BOOL(DID_A_BAIL_HAPPEN())
	NET_NL()NET_PRINT("		-  HAS_IMPORTANT_STATS_LOADED = ")NET_PRINT_BOOL(HAS_IMPORTANT_STATS_LOADED())
	NET_NL()NET_PRINT("		-  LOBBY_AUTO_MULTIPLAYER_FREEMODE = ")NET_PRINT_BOOL(LOBBY_AUTO_MULTIPLAYER_FREEMODE())
	NET_NL()NET_PRINT("		-  SCRIPT_IS_CLOUD_AVAILABLE = ")NET_PRINT_BOOL(SCRIPT_IS_CLOUD_AVAILABLE())
	NET_NL()NET_PRINT("		-  NETWORK_IS_CABLE_CONNECTED = ")NET_PRINT_BOOL(NETWORK_IS_CABLE_CONNECTED())
	NET_NL()NET_PRINT("		-  GET_IS_LAUNCH_FROM_LIVE_AREA = ")NET_PRINT_BOOL(GET_IS_LAUNCH_FROM_LIVE_AREA())
	NET_NL()NET_PRINT("		-  GET_IS_LIVE_AREA_LAUNCH_WITH_CONTENT = ")NET_PRINT_BOOL(GET_IS_LIVE_AREA_LAUNCH_WITH_CONTENT())
	IF IS_PLAYSTATION_PLATFORM() AND NETWORK_IS_NP_AVAILABLE() = FALSE
		NET_NL()NET_PRINT("		-  NETWORK_GET_NP_UNAVAILABLE_REASON = ")NET_PRINT_INT(ENUM_TO_INT(NETWORK_GET_NP_UNAVAILABLE_REASON()))
	ENDIF
	NET_NL()NET_PRINT("		-  ARE_PROFILE_SETTINGS_VALID = ")NET_PRINT_BOOL(ARE_PROFILE_SETTINGS_VALID())
	NET_NL()NET_PRINT("		-  Placement.bHasSignoutMessageAppeared = ")NET_PRINT_BOOL(Placement.bHasSignoutMessageAppeared)
	NET_NL()NET_PRINT("		-  NETWORK_IS_REFRESHING_ROS_CREDENTIALS = ")NET_PRINT_BOOL(NETWORK_IS_REFRESHING_ROS_CREDENTIALS())
	NET_NL()NET_PRINT("		-  IS_PLAYSTATION_PLATFORM() AND NETWORK_IS_NP_PENDING() = ")NET_PRINT_BOOL(IS_PLAYSTATION_PLATFORM() AND NETWORK_IS_NP_PENDING())
	NET_NL()NET_PRINT("		-  HAS_SYSTEM_SERVICE_TRIGGERED() = ")NET_PRINT_BOOL(HAS_SYSTEM_SERVICE_TRIGGERED())
	
	
	#ENDIF
	
	#IF USE_FINAL_PRINTS
	PRINTLN_FINAL("		-  ONLINE PS3 DEFAULT  " )
	PRINTLN_FINAL("RENDER_MPHUD_NOT_CONNECTED Logic: ")
	PRINTLN_FINAL("		-  PRIVALEGES_OKAY_FOR_MP() = ", PRIVALEGES_OKAY_FOR_MP())
	PRINTLN_FINAL("		-  NETWORK_IS_SIGNED_ONLINE() = ",  NETWORK_IS_SIGNED_ONLINE())
	PRINTLN_FINAL("		-  NETWORK_IS_SIGNED_IN() = ",  NETWORK_IS_SIGNED_IN())
	PRINTLN_FINAL("		-  g_SignInStructDetails.bIsSignedIn = ", g_SignInStructDetails.bIsSignedIn)
	PRINTLN_FINAL("		-  g_SignInStructDetails.bIsDuplicateSignIn = ", g_SignInStructDetails.bIsDuplicateSignIn)
	PRINTLN_FINAL("		-  g_SignInStructDetails.bWasOnline = ", g_SignInStructDetails.bWasOnline)
	PRINTLN_FINAL("		-  g_SignInStructDetails.bWasSignedIn = ", g_SignInStructDetails.bWasSignedIn)
	PRINTLN_FINAL("		-  g_SignInStructDetails.bIsOnline = ", g_SignInStructDetails.bIsOnline)	
	PRINTLN_FINAL("		-  IS_SYSTEM_UI_BEING_DISPLAYED() = ",  IS_SYSTEM_UI_BEING_DISPLAYED())
	PRINTLN_FINAL("		-  HAS_NET_TIMER_EXPIRED_READ_ONLY(Placement.aTimer, 3000) = ",  HAS_NET_TIMER_EXPIRED_READ_ONLY(Placement.aTimer, 3000))
	PRINTLN_FINAL("		-  NETWORK_IS_CLOUD_AVAILABLE() = ",  NETWORK_IS_CLOUD_AVAILABLE())
	PRINTLN_FINAL("		-  HAS_NET_TIMER_STARTED(Placement.aTimer) = ",  HAS_NET_TIMER_STARTED(Placement.aTimer))
	PRINTLN_FINAL("		-  DID_A_BAIL_HAPPEN = ", DID_A_BAIL_HAPPEN())
	PRINTLN_FINAL("		-  HAS_IMPORTANT_STATS_LOADED = ", HAS_IMPORTANT_STATS_LOADED())
	PRINTLN_FINAL("		-  LOBBY_AUTO_MULTIPLAYER_FREEMODE = ", LOBBY_AUTO_MULTIPLAYER_FREEMODE())
	PRINTLN_FINAL("		-  SCRIPT_IS_CLOUD_AVAILABLE = ", SCRIPT_IS_CLOUD_AVAILABLE())
	PRINTLN_FINAL("		-  NETWORK_IS_CABLE_CONNECTED = ", NETWORK_IS_CABLE_CONNECTED())
	PRINTLN_FINAL("		-  GET_IS_LAUNCH_FROM_LIVE_AREA = ", GET_IS_LAUNCH_FROM_LIVE_AREA())
	PRINTLN_FINAL("		-  GET_IS_LIVE_AREA_LAUNCH_WITH_CONTENT = ", GET_IS_LIVE_AREA_LAUNCH_WITH_CONTENT())
	IF IS_PLAYSTATION_PLATFORM() AND NETWORK_IS_NP_AVAILABLE() = FALSE
		PRINTLN_FINAL("		-  NETWORK_GET_NP_UNAVAILABLE_REASON = ", ENUM_TO_INT(NETWORK_GET_NP_UNAVAILABLE_REASON()))
	ENDIF
	PRINTLN_FINAL("		-  ARE_PROFILE_SETTINGS_VALID = ", ARE_PROFILE_SETTINGS_VALID())
	PRINTLN_FINAL("		-  Placement.bHasSignoutMessageAppeared = ", Placement.bHasSignoutMessageAppeared)
	PRINTLN_FINAL("		-  NETWORK_IS_REFRESHING_ROS_CREDENTIALS = ", NETWORK_IS_REFRESHING_ROS_CREDENTIALS())
	PRINTLN_FINAL("		-  IS_PLAYSTATION_PLATFORM() AND NETWORK_IS_NP_PENDING() = ", (IS_PLAYSTATION_PLATFORM() AND NETWORK_IS_NP_PENDING()))
	PRINTLN_FINAL("		-  HAS_SYSTEM_SERVICE_TRIGGERED = ", HAS_SYSTEM_SERVICE_TRIGGERED())
	#ENDIF
	
	
	
	IF DID_A_BAIL_HAPPEN() 	
			IF IS_XBOX360_VERSION()
			OR IS_XBOX_PLATFORM()
			
				IF g_SignInStructDetails.bIsOnline = FALSE
				AND NETWORK_IS_SIGNED_IN() = TRUE
				AND g_SignInStructDetails.bIsDuplicateSignIn = FALSE
				
					sLineOneMes = "HUD_DISCON" //The connection to Xbox LIVE has been lost.
					sLineTwoMes = "HUD_SPRETRNFRSH"
					Buttons = FE_WARNING_OK
					
					#IF IS_DEBUG_BUILD
					NET_NL()NET_PRINT("[END]		- ONLINE  XBOX360 bIsOnline = FALSE ")
					#ENDIF
					#IF USE_FINAL_PRINTS
					PRINTLN_FINAL("[END]		- ONLINE  XBOX360 bIsOnline = FALSE  " )
					#ENDIF
				
				
				ELIF (g_SignInStructDetails.bIsSignedIn = FALSE
				OR g_SignInStructDetails.bWasSignedIn = TRUE )
				AND g_SignInStructDetails.bIsDuplicateSignIn = FALSE
					sLineOneMes = "HUD_MPDISCONNECT" //Sign in to Xbox LIVE to play multiplayer.
					sLineTwoMes = "HUD_SPRETRNFRSH"
					Buttons = FE_WARNING_OK
					
					
					#IF IS_DEBUG_BUILD
					NET_NL()NET_PRINT("[END]		-  ONLINE XBOX360 NETWORK_IS_SIGNED_ONLINE() = FALSE ")
					#ENDIF
					#IF USE_FINAL_PRINTS
					PRINTLN_FINAL("[END]		-  ONLINE XBOX360 NETWORK_IS_SIGNED_ONLINE() = FALSE  " )
					#ENDIF
					
				ELIF g_SignInStructDetails.bIsDuplicateSignIn
					sLineOneMes = "HUD_ANOTHER" // Your gamer profile does not have the correct permissions to access Xbox LIVE functionality.
					sLineTwoMes = "HUD_SPRETRNFRSH"
					Buttons = FE_WARNING_OK
					
					#IF IS_DEBUG_BUILD
					NET_NL()NET_PRINT("[END]		-  ONLINE XBOX360 PRIVALEGES_OKAY_FOR_MP() = FALSE ")
					#ENDIF
					#IF USE_FINAL_PRINTS
					PRINTLN_FINAL("[END]		-  ONLINE XBOX360 PRIVALEGES_OKAY_FOR_MP() = FALSE  " )
					#ENDIF
				ELIF PRIVALEGES_OKAY_FOR_MP() = FALSE
					sLineOneMes = "HUD_PERM" // Your gamer profile does not have the correct permissions to access Xbox LIVE functionality.
					sLineTwoMes = "HUD_PROFILECHNG"
					Buttons = FE_WARNING_OK
					
					#IF IS_DEBUG_BUILD
					NET_NL()NET_PRINT("[END]		-  ONLINE XBOX360 PRIVALEGES_OKAY_FOR_MP() = FALSE ")
					#ENDIF
					#IF USE_FINAL_PRINTS
					PRINTLN_FINAL("[END]		-  ONLINE XBOX360 PRIVALEGES_OKAY_FOR_MP() = FALSE  " )
					#ENDIF
					
				ELIF (NOT g_SignInStructDetails.bWasOnline AND g_SignInStructDetails.bIsOnline)
				
					sLineOneMes = "HUD_DISCON" // The connection to Xbox Live has been lost.
					sLineTwoMes = "HUD_SPRETRNFRSH"
					Buttons = FE_WARNING_OK
					
					#IF IS_DEBUG_BUILD
					NET_NL()NET_PRINT("[END]		-  ONLINE XBOX360 NOT g_SignInStructDetails.bWasOnline AND g_SignInStructDetails.bIsOnline ")
					#ENDIF
					#IF USE_FINAL_PRINTS
					PRINTLN_FINAL("[END]		-  ONLINE XBOX360 NOT g_SignInStructDetails.bWasOnline AND g_SignInStructDetails.bIsOnline  " )
					#ENDIF
				ELSE
					
					sLineOneMes = "HUD_DISCON" //The connection to Xbox LIVE has been lost.
					sLineTwoMes = "HUD_SPRETRNFRSH"
					Buttons = FE_WARNING_OK
					
					#IF IS_DEBUG_BUILD
					NET_NL()NET_PRINT("[END]		- ONLINE  XBOX360 DEFAULT ")
					#ENDIF
					#IF USE_FINAL_PRINTS
					PRINTLN_FINAL("[END]		-  ONLINE  XBOX360 DEFAULT  " )
					#ENDIF
					
				ENDIF

			ELIF IS_PS3_VERSION()
			OR IS_PLAYSTATION_PLATFORM()
			
				IF IS_PLAYSTATION_PLATFORM() AND NETWORK_IS_NP_AVAILABLE() = FALSE AND NETWORK_GET_NP_UNAVAILABLE_REASON() = REASON_CONNECTION
				
					sLineOneMes = "HUD_PLUGPU"
					sLineTwoMes = "HUD_SPRETRNFRSH"
					Buttons = FE_WARNING_OK
					Placement.aBool[0] = TRUE
					
					
					#IF IS_DEBUG_BUILD
					NET_NL()NET_PRINT("[END]		- ONLINE PS3 REASON_CONNECTION ")
					#ENDIF
					#IF USE_FINAL_PRINTS
					PRINTLN_FINAL("[END]		-  ONLINE  PS3 REASON_CONNECTION  " )
					#ENDIF
				ELIF g_SignInStructDetails.bIsOnline = FALSE
				AND NETWORK_IS_SIGNED_IN() = TRUE
				AND g_SignInStructDetails.bIsDuplicateSignIn = FALSE
				
	
					sLineOneMes = "HUD_DISCON" //The connection to PSN has been lost.
					sLineTwoMes = "HUD_SPRETRNFRSH"
					Buttons = FE_WARNING_OK
										
					IF IS_PLAYSTATION_PLATFORM()
						IF NETWORK_IS_LOGGED_IN_TO_PSN() = FALSE
						AND NETWORK_GET_NP_UNAVAILABLE_REASON() != REASON_SIGNED_OUT
							sLineOneMes = "HUD_PLUGPU2"//You have lost connection to the internet. You will be unable to access any network features without restoring your internet connection. Please return to Grand Theft Auto V.
							sLineTwoMes = ""
							#IF USE_FINAL_PRINTS
							PRINTLN_FINAL("[END]		-  ONLINE PS3 bIsOnline = FALSE NETWORK_IS_LOGGED_IN_TO_PSN() = FALSE " )	
							#ENDIF
							#IF IS_DEBUG_BUILD
							NET_NL()NET_PRINT("[END]		- ONLINE PS3 bIsOnline = FALSE NETWORK_IS_LOGGED_IN_TO_PSN = FALSE ")
							#ENDIF
						ELIF NETWORK_IS_SIGNED_ONLINE() = FALSE
							sLineOneMes = "HUD_RESIGN_LONG" //You were signed out of PSN℠. You will be unable to access any network features without signing back in.
							#IF USE_FINAL_PRINTS
							PRINTLN_FINAL("[END]		-  ONLINE PS3 bIsOnline = FALSE NETWORK_IS_SIGNED_ONLINE() = FALSE " )
							#ENDIF
							#IF IS_DEBUG_BUILD
							NET_NL()NET_PRINT("[END]		- ONLINE PS3 bIsOnline = FALSE NETWORK_IS_SIGNED_ONLINE = FALSE ")
							#ENDIF
						ELIF NETWORK_IS_CONNETED_TO_NP_PRESENCE() = FALSE
							sLineOneMes = "HUD_PLUGPU2"//You have lost connection to the internet. You will be unable to access any network features without restoring your internet connection. Please return to Grand Theft Auto V.
							sLineTwoMes = ""
							#IF USE_FINAL_PRINTS
							PRINTLN_FINAL("[END]		-  ONLINE PS3 bIsOnline = FALSE NETWORK_IS_CONNETED_TO_NP_PRESENCE() = FALSE " )
							#ENDIF
							#IF IS_DEBUG_BUILD
							NET_NL()NET_PRINT("[END]		- ONLINE PS3 bIsOnline = FALSE NETWORK_IS_CONNETED_TO_NP_PRESENCE = FALSE ")
							#ENDIF
						ENDIF
					ENDIF
					
					IF IS_PLAYSTATION_PLATFORM()
					AND NETWORK_IS_CABLE_CONNECTED() = FALSE
						Placement.aBool[0] = TRUE
						sLineOneMes = "HUD_PLUGPU"
						#IF USE_FINAL_PRINTS
						PRINTLN_FINAL("[END]		-  ONLINE PS3 bIsOnline = FALSE NETWORK_IS_CABLE_CONNECTED() = FALSE " )
						#ENDIF
						#IF IS_DEBUG_BUILD
						NET_NL()NET_PRINT("[END]		- ONLINE PS3 bIsOnline = FALSE NETWORK_IS_CABLE_CONNECTED = FALSE ")
						#ENDIF
					ENDIF
					
					
					
					#IF IS_DEBUG_BUILD
					NET_NL()NET_PRINT("[END]		- ONLINE PS3 bIsOnline = FALSE ")
					#ENDIF
					#IF USE_FINAL_PRINTS
					PRINTLN_FINAL("[END]		-  ONLINE PS3 bIsOnline = FALSE " )
					#ENDIF
					
				ELIF (g_SignInStructDetails.bIsSignedIn = FALSE
				OR g_SignInStructDetails.bWasSignedIn = TRUE )
				AND g_SignInStructDetails.bIsDuplicateSignIn = FALSE
					sLineOneMes = "HUD_MPDISCONNECT" //Sign in to Xbox LIVE to play multiplayer.
					sLineTwoMes = "HUD_SPRETRNFRSH"
					Buttons = FE_WARNING_OK
					
					IF IS_PLAYSTATION_PLATFORM()
					AND NETWORK_IS_CABLE_CONNECTED() = FALSE
						Placement.aBool[0] = TRUE
						sLineOneMes = "HUD_PLUGPU"
					ENDIF
					
					
					#IF IS_DEBUG_BUILD
					NET_NL()NET_PRINT("[END]		-  ONLINE PS3 NETWORK_IS_SIGNED_ONLINE() = FALSE ")
					#ENDIF
					#IF USE_FINAL_PRINTS
					PRINTLN_FINAL("[END]		-  ONLINE PS3 NETWORK_IS_SIGNED_ONLINE() = FALSE " )
					#ENDIF
					
				ELIF g_SignInStructDetails.bIsDuplicateSignIn
					sLineOneMes = "HUD_ANOTHER" // Your gamer profile does not have the correct permissions to access Xbox LIVE functionality.
					sLineTwoMes = "HUD_SPRETRNFRSH"
					Buttons = FE_WARNING_OK
					
					#IF IS_DEBUG_BUILD
					NET_NL()NET_PRINT("[END]		-  ONLINE PS3 g_SignInStructDetails.bIsDuplicateSignIn ")
					#ENDIF
					#IF USE_FINAL_PRINTS
					PRINTLN_FINAL("[END]		-  ONLINE PS3 g_SignInStructDetails.bIsDuplicateSignIn " )
					#ENDIF
					
				ELIF PRIVALEGES_OKAY_FOR_MP() = FALSE
					sLineOneMes = "HUD_PERM" // Your gamer profile does not have the correct permissions to access Xbox LIVE functionality.
					sLineTwoMes = "HUD_PROFILECHNG"
					Buttons = FE_WARNING_OK
					
					#IF IS_DEBUG_BUILD
					NET_NL()NET_PRINT("[END]		-  ONLINE PS3 PRIVALEGES_OKAY_FOR_MP() = FALSE ")
					#ENDIF	
					#IF USE_FINAL_PRINTS
					PRINTLN_FINAL("[END]		-  ONLINE PS3 PRIVALEGES_OKAY_FOR_MP() = FALSE " )
					#ENDIF
					
				ELIF (NOT g_SignInStructDetails.bWasOnline AND g_SignInStructDetails.bIsOnline)
				
					sLineOneMes = "HUD_DISCON" // You were signed out of Xbox Live.
					sLineTwoMes = "HUD_SPRETRNFRSH"
					Buttons = FE_WARNING_OK
					
					#IF IS_DEBUG_BUILD
					NET_NL()NET_PRINT("[END]		-  ONLINE PS3 NOT g_SignInStructDetails.bWasOnline AND g_SignInStructDetails.bIsOnline ")
					#ENDIF
					#IF USE_FINAL_PRINTS
					PRINTLN_FINAL("[END]		-  ONLINE PS3 NOT g_SignInStructDetails.bWasOnline AND g_SignInStructDetails.bIsOnline  " )
					#ENDIF
				ELSE
					sLineOneMes = "HUD_DISCON" //The connection to Xbox LIVE has been lost.
					sLineTwoMes = "HUD_SPRETRNFRSH"
					Buttons = FE_WARNING_OK
					
					#IF IS_DEBUG_BUILD
					NET_NL()NET_PRINT("[END]		- ONLINE PS3 DEFAULT ")
					#ENDIF
					#IF USE_FINAL_PRINTS
					PRINTLN_FINAL("[END]		-  ONLINE PS3 DEFAULT  " )
					#ENDIF
				ENDIF
			
			
			ELSE
			
				IF g_SignInStructDetails.bIsOnline = FALSE
				AND g_SignInStructDetails.bIsDuplicateSignIn = FALSE

					sLineOneMes = "HUD_DISCON" //The connection to Xbox LIVE has been lost.
					sLineTwoMes = "HUD_SPRETRNFRSH"
					Buttons = FE_WARNING_OK
					
					
					
					#IF IS_DEBUG_BUILD
					NET_NL()NET_PRINT("[END]		- ONLINE PC DEFAULT ")
					#ENDIF
					#IF USE_FINAL_PRINTS
					PRINTLN_FINAL("[END]		-  ONLINE PC DEFAULT  " )
					#ENDIF
					
				ELIF (g_SignInStructDetails.bIsSignedIn = FALSE
				OR g_SignInStructDetails.bWasSignedIn = TRUE )
				AND g_SignInStructDetails.bIsDuplicateSignIn = FALSE
					sLineOneMes = "HUD_MPDISCONNECT" //Sign in to Xbox LIVE to play multiplayer.
					sLineTwoMes = "HUD_SPRETRNFRSH"
					Buttons = FE_WARNING_OK
					
					#IF IS_DEBUG_BUILD
					NET_NL()NET_PRINT("[END]		-  ONLINE PC g_SignInStructDetails.bIsSignedIn = FALSE ")
					#ENDIF
					#IF USE_FINAL_PRINTS
					PRINTLN_FINAL("[END]		-  ONLINE PC g_SignInStructDetails.bIsSignedIn = FALSE  " )
					#ENDIF
					
				ELIF g_SignInStructDetails.bIsDuplicateSignIn
					sLineOneMes = "HUD_ANOTHER" // Your gamer profile does not have the correct permissions to access Xbox LIVE functionality.
					sLineTwoMes = "HUD_SPRETRNFRSH" 
					Buttons = FE_WARNING_OK
					
					#IF IS_DEBUG_BUILD
					NET_NL()NET_PRINT("[END]		-  ONLINE PC  g_SignInStructDetails.bIsDuplicateSignIn = TRUE ")
					#ENDIF
					#IF USE_FINAL_PRINTS
					PRINTLN_FINAL("[END]		-  ONLINE PC g_SignInStructDetails.bIsDuplicateSignIn = TRUE  " )
					#ENDIF
					
						
				ELIF PRIVALEGES_OKAY_FOR_MP() = FALSE
					sLineOneMes = "HUD_PERM" // Your gamer profile does not have the correct permissions to access Xbox LIVE functionality.
					sLineTwoMes = "HUD_PROFILECHNG"
					Buttons = FE_WARNING_OK
					
					#IF IS_DEBUG_BUILD
					NET_NL()NET_PRINT("[END]		-  ONLINE PC PRIVALEGES_OKAY_FOR_MP() = FALSE ")
					#ENDIF	
					
					#IF USE_FINAL_PRINTS
					PRINTLN_FINAL("[END]		-  ONLINE PC PRIVALEGES_OKAY_FOR_MP() = FALSE  " )
					#ENDIF
					
				ELIF (NOT g_SignInStructDetails.bWasOnline AND g_SignInStructDetails.bIsOnline)
				
					sLineOneMes = "HUD_DISCON" // The connection to Xbox LIVE has been lost.
					sLineTwoMes = "HUD_SPRETRNFRSH"
					Buttons = FE_WARNING_OK
					
					#IF IS_DEBUG_BUILD
					NET_NL()NET_PRINT("[END]		-  ONLINE PC NOT g_SignInStructDetails.bWasOnline AND g_SignInStructDetails.bIsOnline ")
					#ENDIF	
					
					#IF USE_FINAL_PRINTS
					PRINTLN_FINAL("[END]		-  ONLINE PC NOT g_SignInStructDetails.bWasOnline AND g_SignInStructDetails.bIsOnline  " )
					#ENDIF
					
				ELSE
					sLineOneMes = "HUD_DISCON" //The connection to Xbox LIVE has been lost.
					sLineTwoMes = "HUD_SPRETRNFRSH"
					Buttons = FE_WARNING_OK
					
					#IF IS_DEBUG_BUILD
					NET_NL()NET_PRINT("[END]		- ONLINE PC DEFAULT ")
					#ENDIF
					
					#IF USE_FINAL_PRINTS
					PRINTLN_FINAL("[END]		-  ONLINE PC DEFAULT  " )
					#ENDIF
					
				ENDIF
			
			
			ENDIF
		
		
	ELSE
		IF IS_XBOX360_VERSION()
		OR IS_XBOX_PLATFORM()			
		
		
			IF g_SignInStructDetails.bIsOnline = FALSE
			AND NETWORK_IS_SIGNED_IN() = TRUE
				sLineOneMes = "HUD_DISCON" //The connection to Xbox LIVE has been lost.
				sLineTwoMes = "HUD_SPRETRNFRSH"
				Buttons = FE_WARNING_OK

				
				#IF IS_DEBUG_BUILD
				NET_NL()NET_PRINT("[END]		- OFFLINE  XBOX360 g_SignInStructDetails.bIsOnline = FALSE ")
				#ENDIF
				
				#IF USE_FINAL_PRINTS
				PRINTLN_FINAL("[END]		-  OFFLINE  XBOX360 g_SignInStructDetails.bIsOnline = FALSE  " )
				#ENDIF
		
			ELIF HAS_SYSTEM_SERVICE_TRIGGERED()
			
				#IF IS_DEBUG_BUILD
				NET_NL()NET_PRINT("[END]		- OFFLINE  XBOX360 HAS_SYSTEM_SERVICE_TRIGGERED ")
				#ENDIF
				
				#IF USE_FINAL_PRINTS
				PRINTLN_FINAL("[END]		-  OFFLINE  XBOX360 HAS_SYSTEM_SERVICE_TRIGGERED  " )
				#ENDIF
				
		
			ELIF (g_SignInStructDetails.bIsSignedIn = TRUE
			AND g_SignInStructDetails.bIsDuplicateSignIn = FALSE
			AND g_SignInStructDetails.bWasOnline = FALSE
			AND g_SignInStructDetails.bWasSignedIn = TRUE
			AND g_SignInStructDetails.bIsOnline = TRUE
			AND NETWORK_IS_SIGNED_ONLINE() = TRUE
			AND PRIVALEGES_OKAY_FOR_MP() = TRUE)
			
				sLineOneMes = "HUD_CONNECTION" //Connection to the game session was lost
				sLineTwoMes = "HUD_SPRETURNTRYNOW"
				Buttons = FE_WARNING_OK
				
				#IF IS_DEBUG_BUILD
				NET_NL()NET_PRINT("[END]		-  Back online XBOX360 Everything is back ")
				#ENDIF
				
				#IF USE_FINAL_PRINTS
				PRINTLN_FINAL("[END]		-  Back online XBOX360 Everything is back  " )
				#ENDIF
		
			ELIF (g_SignInStructDetails.bIsSignedIn = FALSE
			OR g_SignInStructDetails.bWasSignedIn = TRUE )
			AND g_SignInStructDetails.bIsDuplicateSignIn = FALSE
				sLineOneMes = "HUD_CONNT" //Sign in to Xbox LIVE to play multiplayer.
				sLineTwoMes = "HUD_SPRETRNFRSH"
				Buttons = FE_WARNING_OK
				
				#IF IS_DEBUG_BUILD
				NET_NL()NET_PRINT("[END]		-  OFFLINE XBOX360 NETWORK_IS_SIGNED_ONLINE() = FALSE ")
				#ENDIF
				
				#IF USE_FINAL_PRINTS
				PRINTLN_FINAL("[END]		-  OFFLINE XBOX360 NETWORK_IS_SIGNED_ONLINE() = FALSE  " )
				#ENDIF
								
			ELIF g_SignInStructDetails.bIsDuplicateSignIn
				sLineOneMes = "HUD_ANOTHER" // Your gamer profile does not have the correct permissions to access Xbox LIVE functionality.
				sLineTwoMes = "HUD_SPRETRNFRSH"
				Buttons = FE_WARNING_OK
				
				#IF IS_DEBUG_BUILD
				NET_NL()NET_PRINT("[END]		-  OFFLINE XBOX360 g_SignInStructDetails.bIsDuplicateSignIn ")
				#ENDIF
				
				#IF USE_FINAL_PRINTS
				PRINTLN_FINAL("[END]		-  OFFLINE XBOX360 g_SignInStructDetails.bIsDuplicateSignIn  " )
				#ENDIF
				
			ELIF PRIVALEGES_OKAY_FOR_MP() = FALSE
				sLineOneMes = "HUD_PERM" // Your gamer profile does not have the correct permissions to access Xbox LIVE functionality.
				sLineTwoMes = "HUD_PROFILECHNG"
				Buttons = FE_WARNING_OK
				
				#IF IS_DEBUG_BUILD
				NET_NL()NET_PRINT("[END]		-  OFFLINE XBOX360 PRIVALEGES_OKAY_FOR_MP() = FALSE ")
				#ENDIF
				
				#IF USE_FINAL_PRINTS
				PRINTLN_FINAL("[END]		-  OFFLINE XBOX360 PRIVALEGES_OKAY_FOR_MP() = FALSE  " )
				#ENDIF
			
			ELSE
				sLineOneMes = "HUD_DISCON" //The connection to Xbox LIVE has been lost.
				sLineTwoMes = "HUD_SPRETRNFRSH"
				Buttons = FE_WARNING_OK
				
				#IF IS_DEBUG_BUILD
				NET_NL()NET_PRINT("[END]		- OFFLINE  XBOX360 DEFAULT ")
				#ENDIF
				
				#IF USE_FINAL_PRINTS
				PRINTLN_FINAL("[END]		-  OFFLINE XBOX360 DEFAULT  " )
				#ENDIF
			ENDIF
		
		ELIF IS_PS3_VERSION()
		OR IS_PLAYSTATION_PLATFORM()
		
			IF g_SignInStructDetails.bIsOnline = FALSE
			AND NETWORK_IS_SIGNED_IN() = TRUE

				sLineOneMes = "HUD_DISCON" //The connection to Xbox LIVE has been lost.
				sLineTwoMes = "HUD_SPRETRNFRSH"
				Buttons = FE_WARNING_OK
				
				IF IS_PLAYSTATION_PLATFORM()
				AND NETWORK_IS_CABLE_CONNECTED() = FALSE
					Placement.aBool[0] = TRUE
					sLineOneMes = "HUD_PLUGPU"
					
					#IF IS_DEBUG_BUILD
					NET_NL()NET_PRINT("[END]		- OFFLINE PS3 g_SignInStructDetails.bIsOnline = FALSE, override HUD_PLUGPU ")
					#ENDIF
					
					#IF USE_FINAL_PRINTS
					PRINTLN_FINAL("[END]		-  OFFLINE PS3 g_SignInStructDetails.bIsOnline = FALSE, override HUD_PLUGPU  " )
					#ENDIF
					
				ENDIF
				
				IF IS_PLAYSTATION_PLATFORM() 
				AND g_SignInStructDetails.bIsSignedIn = TRUE
				AND g_SignInStructDetails.bIsDuplicateSignIn = FALSE
				AND g_SignInStructDetails.bWasOnline = TRUE
				AND g_SignInStructDetails.bWasSignedIn = TRUE 
				AND g_SignInStructDetails.bIsOnline = FALSE
				AND NETWORK_IS_CABLE_CONNECTED() 
				AND NETWORK_IS_REFRESHING_ROS_CREDENTIALS() = FALSE
				AND NETWORK_IS_NP_PENDING() = FALSE
				
					#IF IS_DEBUG_BUILD
					NET_NL()NET_PRINT("[END]		- OFFLINE PS3 g_SignInStructDetails.bIsOnline = FALSE, override 1 ")
					#ENDIF
					
					#IF USE_FINAL_PRINTS
					PRINTLN_FINAL("[END]		-  OFFLINE PS3 g_SignInStructDetails.bIsOnline = FALSE, override 1  " )
					#ENDIF
				
					sLineOneMes = "HUD_CONNTPS4SI"
					Placement.aBool[0] = FALSE
				ENDIF	
				
				IF IS_PLAYSTATION_PLATFORM() 
				AND g_SignInStructDetails.bIsSignedIn = FALSE
				AND g_SignInStructDetails.bIsDuplicateSignIn = FALSE
				AND g_SignInStructDetails.bWasOnline = FALSE
				AND g_SignInStructDetails.bWasSignedIn = FALSE 
				AND g_SignInStructDetails.bIsOnline = FALSE
				AND NETWORK_IS_SIGNED_IN() = TRUE
				AND NETWORK_IS_CABLE_CONNECTED() 
				AND NETWORK_IS_REFRESHING_ROS_CREDENTIALS() = FALSE
				AND NETWORK_IS_NP_PENDING() = FALSE
				
					#IF IS_DEBUG_BUILD
					NET_NL()NET_PRINT("[END]		- OFFLINE PS3 g_SignInStructDetails.bIsOnline = FALSE, override 2 ")
					#ENDIF
					
					#IF USE_FINAL_PRINTS
					PRINTLN_FINAL("[END]		-  OFFLINE PS3 g_SignInStructDetails.bIsOnline = FALSE, override 2  " )
					#ENDIF
				
					sLineOneMes = "HUD_CONNTPS4SI"
					Placement.aBool[0] = FALSE
				ENDIF
			
				#IF IS_DEBUG_BUILD
				NET_NL()NET_PRINT("[END]		- OFFLINE PS3 g_SignInStructDetails.bIsOnline = FALSE ")
				#ENDIF
				
				#IF USE_FINAL_PRINTS
				PRINTLN_FINAL("[END]		-  OFFLINE PS3 g_SignInStructDetails.bIsOnline = FALSE  " )
				#ENDIF
			
					
			ELIF (g_SignInStructDetails.bIsSignedIn = TRUE
			AND g_SignInStructDetails.bIsDuplicateSignIn = FALSE
			AND g_SignInStructDetails.bWasOnline = FALSE
			AND g_SignInStructDetails.bWasSignedIn = TRUE
			AND g_SignInStructDetails.bIsOnline = TRUE
			AND NETWORK_IS_SIGNED_ONLINE() = TRUE
			AND PRIVALEGES_OKAY_FOR_MP() = TRUE)
			
				sLineOneMes = "HUD_CONNECTION" //Connection to the game session was lost
				sLineTwoMes = "HUD_SPRETURNTRYNOW"
				Buttons = FE_WARNING_OK
				
				#IF IS_DEBUG_BUILD
				NET_NL()NET_PRINT("[END]		-  Back online PS3 Everything is back ")
				#ENDIF
				
				#IF USE_FINAL_PRINTS
				PRINTLN_FINAL("[END]		-  Back online PS3 Everything is back  " )
				#ENDIF
			
			ELIF (g_SignInStructDetails.bIsSignedIn = FALSE
			OR g_SignInStructDetails.bWasSignedIn = TRUE )
			AND g_SignInStructDetails.bIsDuplicateSignIn = FALSE
			
				sLineOneMes = "HUD_MPDISCONNECT" //Sign in to Xbox LIVE to play multiplayer.
				sLineTwoMes = "HUD_SPRETRNFRSH"
				Buttons = FE_WARNING_OK
				
				IF IS_PLAYSTATION_PLATFORM()
				AND NETWORK_IS_CABLE_CONNECTED() = FALSE
					Placement.aBool[0] = TRUE
					sLineOneMes = "HUD_PLUGPU"
				ENDIF
				
				#IF IS_DEBUG_BUILD
				NET_NL()NET_PRINT("[END]		-  OFFLINE PS3 NETWORK_IS_SIGNED_ONLINE() = FALSE ")
				#ENDIF
				
				#IF USE_FINAL_PRINTS
				PRINTLN_FINAL("[END]		-  OFFLINE PS3 NETWORK_IS_SIGNED_ONLINE() = FALSE " )
				#ENDIF
				
			ELIF g_SignInStructDetails.bIsDuplicateSignIn
				sLineOneMes = "HUD_ANOTHER" // Your gamer profile does not have the correct permissions to access Xbox LIVE functionality.
				sLineTwoMes = "HUD_SPRETRNFRSH"
				Buttons = FE_WARNING_OK
				
				#IF IS_DEBUG_BUILD
				NET_NL()NET_PRINT("[END]		-  OFFLINE PS3 g_SignInStructDetails.bIsDuplicateSignIn ")
				#ENDIF
				
				#IF USE_FINAL_PRINTS
				PRINTLN_FINAL("[END]		-  OFFLINE PS3 g_SignInStructDetails.bIsDuplicateSignIn " )
				#ENDIF
				
			ELIF PRIVALEGES_OKAY_FOR_MP() = FALSE
				sLineOneMes = "HUD_PERM" // Your gamer profile does not have the correct permissions to access Xbox LIVE functionality.
				sLineTwoMes = "HUD_PROFILECHNG"
				Buttons = FE_WARNING_OK
				
				#IF IS_DEBUG_BUILD
				NET_NL()NET_PRINT("[END]		-  OFFLINE PS3 PRIVALEGES_OKAY_FOR_MP() = FALSE ")
				#ENDIF	
				
				#IF USE_FINAL_PRINTS
				PRINTLN_FINAL("[END]		-  OFFLINE PS3 PRIVALEGES_OKAY_FOR_MP() = FALSE " )
				#ENDIF
				
			ELSE
				sLineOneMes = "HUD_DISCON" //The connection to Xbox LIVE has been lost.
				sLineTwoMes = "HUD_SPRETRNFRSH"
				Buttons = FE_WARNING_OK
				
				#IF IS_DEBUG_BUILD
				NET_NL()NET_PRINT("[END]		- OFFLINE PS3 DEFAULT ")
				#ENDIF
				
				#IF USE_FINAL_PRINTS
				PRINTLN_FINAL("[END]		-  OFFLINE PS3 DEFAULT " )
				#ENDIF
			ENDIF
		
		ELSE
		
		
			IF g_SignInStructDetails.bIsOnline = FALSE
			
				sLineOneMes = "HUD_DISCON" //The connection to Xbox LIVE has been lost.
				sLineTwoMes = "HUD_SPRETRNFRSH"
				Buttons = FE_WARNING_OK
				
				#IF IS_DEBUG_BUILD
				NET_NL()NET_PRINT("[END]		- OFFLINE PC g_SignInStructDetails.bIsOnline = FALSE ")
				#ENDIF
				
				#IF USE_FINAL_PRINTS
				PRINTLN_FINAL("[END]		-  OFFLINE PC g_SignInStructDetails.bIsOnline = FALSE " )
				#ENDIF

		
			ELIF (g_SignInStructDetails.bIsSignedIn = TRUE
			AND g_SignInStructDetails.bIsDuplicateSignIn = FALSE
			AND g_SignInStructDetails.bWasOnline = FALSE
			AND g_SignInStructDetails.bWasSignedIn = TRUE
			AND g_SignInStructDetails.bIsOnline = TRUE
			AND NETWORK_IS_SIGNED_ONLINE() = TRUE
			AND PRIVALEGES_OKAY_FOR_MP() = TRUE)
			
				sLineOneMes = "HUD_CONNECTION" //Connection to the game session was lost
				sLineTwoMes = "HUD_SPRETURNTRYNOW"
				Buttons = FE_WARNING_OK
				
				#IF IS_DEBUG_BUILD
				NET_NL()NET_PRINT("[END]		-  Back online PC Everything is back ")
				#ENDIF
				
				#IF USE_FINAL_PRINTS
				PRINTLN_FINAL("[END]		-  Back online PC Everything is back " )
				#ENDIF
		
			ELIF (g_SignInStructDetails.bIsSignedIn = FALSE
			OR g_SignInStructDetails.bWasSignedIn = TRUE )
			AND g_SignInStructDetails.bIsDuplicateSignIn = FALSE
				sLineOneMes = "HUD_MPDISCONNECT" //Sign in to Xbox LIVE to play multiplayer.
				sLineTwoMes = "HUD_SPRETRNFRSH"
				Buttons = FE_WARNING_OK
				
				#IF IS_DEBUG_BUILD
				NET_NL()NET_PRINT("[END]		-  OFFLINE PC g_SignInStructDetails.bIsSignedIn = FALSE OR g_SignInStructDetails.bWasSignedIn = TRUE ")
				#ENDIF
				
				#IF USE_FINAL_PRINTS
				PRINTLN_FINAL("[END]		-  OFFLINE PC g_SignInStructDetails.bIsSignedIn = FALSE OR g_SignInStructDetails.bWasSignedIn = TRUE " )
				#ENDIF
				
			ELIF g_SignInStructDetails.bIsDuplicateSignIn
				sLineOneMes = "HUD_ANOTHER" // Your gamer profile does not have the correct permissions to access Xbox LIVE functionality.
				sLineTwoMes = "HUD_SPRETRNFRSH"
				Buttons = FE_WARNING_OK
				
				#IF IS_DEBUG_BUILD
				NET_NL()NET_PRINT("[END]		-  OFFLINE PC g_SignInStructDetails.bIsDuplicateSignIn ")
				#ENDIF
				
				#IF USE_FINAL_PRINTS
				PRINTLN_FINAL("[END]		-  OFFLINE PC g_SignInStructDetails.bIsDuplicateSignIn " )
				#ENDIF
				
			ELIF PRIVALEGES_OKAY_FOR_MP() = FALSE
				sLineOneMes = "HUD_PERM" // Your gamer profile does not have the correct permissions to access Xbox LIVE functionality.
				sLineTwoMes = "HUD_PROFILECHNG"
				Buttons = FE_WARNING_OK
				
				#IF IS_DEBUG_BUILD
				NET_NL()NET_PRINT("[END]		-  OFFLINE PC PRIVALEGES_OKAY_FOR_MP() = FALSE ")
				#ENDIF	
				
				#IF USE_FINAL_PRINTS
				PRINTLN_FINAL("[END]		-  OFFLINE PC PRIVALEGES_OKAY_FOR_MP() = FALSE " )
				#ENDIF
				
			ELSE
				sLineOneMes = "HUD_DISCON" //The connection to Xbox LIVE has been lost.
				sLineTwoMes = "HUD_SPRETRNFRSH"
				Buttons = FE_WARNING_OK
				
				#IF IS_DEBUG_BUILD
				NET_NL()NET_PRINT("[END]		- OFFLINE PC DEFAULT ")
				#ENDIF
				
				#IF USE_FINAL_PRINTS
				PRINTLN_FINAL("[END]		-  OFFLINE PC DEFAULT " )
				#ENDIF
				
			ENDIF
		
		
		ENDIF
	
	ENDIF
	
	IF NOT IS_SAFE_TO_QUIT_BAIL_SCREEN()
		Buttons = FE_WARNING_SPINNER_ONLY
		#IF IS_DEBUG_BUILD
		NET_NL()NET_PRINT("		- FE_WARNING_SPINNER_ONLY - IS_SAFE_TO_QUIT_BAIL_SCREEN = FALSE ")
		#ENDIF
		#IF USE_FINAL_PRINTS
		PRINTLN_FINAL("		- FE_WARNING_SPINNER_ONLY - IS_SAFE_TO_QUIT_BAIL_SCREEN = FALSE ")
		#ENDIF	
	ENDIF
	
	IF Placement.bHasSignoutMessageAppeared = FALSE
		Buttons = FE_WARNING_SPINNER_ONLY
		#IF IS_DEBUG_BUILD
		NET_NL()NET_PRINT("		- FE_WARNING_SPINNER_ONLY - Placement.bHasSignoutMessageAppeared = FALSE ")
		#ENDIF
		#IF USE_FINAL_PRINTS
		PRINTLN_FINAL("		- FE_WARNING_SPINNER_ONLY - Placement.bHasSignoutMessageAppeared = FALSE ")
		#ENDIF		
	ENDIF
	
	IF IS_SYSTEM_UI_BEING_DISPLAYED()
		Buttons = FE_WARNING_SPINNER_ONLY
		#IF IS_DEBUG_BUILD
		NET_NL()NET_PRINT("		- FE_WARNING_SPINNER_ONLY - IS_SYSTEM_UI_BEING_DISPLAYED = TRUE ")
		#ENDIF
		#IF USE_FINAL_PRINTS
		PRINTLN_FINAL("		- FE_WARNING_SPINNER_ONLY - IS_SYSTEM_UI_BEING_DISPLAYED = TRUE ")
		#ENDIF
	ENDIF
	
//	IF 	(g_SignInStructDetails.bIsSignedIn
//	AND g_SignInStructDetails.bIsDuplicateSignIn = FALSE
//	AND NOT IS_SYSTEM_UI_BEING_DISPLAYED()) = FALSE //Brackets!!! look closer before you rip it out. 
//	
//	AND HAS_NET_TIMER_EXPIRED(Placement.aTimer, 3000)
//	
//	OR 	((g_SignInStructDetails.bWasSignedIn = TRUE AND g_SignInStructDetails.bIsOnline = FALSE)
//	AND NOT IS_SYSTEM_UI_BEING_DISPLAYED() 
//	AND HAS_NET_TIMER_EXPIRED(Placement.aTimer, 3000))
//	
//	//Signed out while booting and heading online. 
//	OR 	((g_SignInStructDetails.bIsSignedIn = FALSE 
//	AND g_SignInStructDetails.bIsDuplicateSignIn = FALSE
//	AND g_SignInStructDetails.bWasOnline = FALSE
//	AND g_SignInStructDetails.bWasSignedIn = FALSE
//	AND g_SignInStructDetails.bIsOnline = FALSE)


	IF Placement.aBool[0] 
	AND NETWORK_IS_CABLE_CONNECTED() = TRUE
		sLineOneMes = "NT_INV_CONERR"
	ENDIF

//	IF NOT IS_SYSTEM_UI_BEING_DISPLAYED() 
	IF HAS_NET_TIMER_EXPIRED(Placement.aTimer, 3000)
	
		#IF IS_DEBUG_BUILD
		NET_NL()NET_PRINT("-  SET_WARNING_MESSAGE_WITH_HEADER = TRUE ")
		NET_NL()
		#ENDIF
		
		#IF USE_FINAL_PRINTS
		PRINTLN_FINAL("-  SET_WARNING_MESSAGE_WITH_HEADER = TRUE " )
		#ENDIF
		
		DRAW_RECT(0.5, 0.5, 1.0,1.0, 0,0,0,255)
		Placement.bHasSignoutMessageAppeared = TRUE
		
		#IF FEATURE_GEN9_STANDALONE
		IF NOT g_bWasReturnToLandingPageStarted
		#ENDIF		
			SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sLineOneMes, Buttons , sLineTwoMes)	
		#IF FEATURE_GEN9_STANDALONE
		ENDIF
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_ACCEPT)
		OR (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CURSOR_ACCEPT) AND NOT IS_WARNING_MESSAGE_ACTIVE())
			g_bWasReturnToLandingPageStarted = TRUE
		ENDIF
		#ENDIF
	ELSE	
	
//		IF IS_SYSTEM_UI_BEING_DISPLAYED()
			DRAW_RECT(0.5, 0.5, 1.0,1.0, 0,0,0,255)
			#IF IS_DEBUG_BUILD
			NET_NL()NET_PRINT("		-  SET_WARNING_MESSAGE_WITH_HEADER = FALSE - DRAWING RECTANGLE ")
			NET_NL()
			#ENDIF
			#IF USE_FINAL_PRINTS
			PRINTLN_FINAL("		-  SET_WARNING_MESSAGE_WITH_HEADER = FALSE - DRAWING RECTANGLE " )
			#ENDIF
//		ENDIF
	
//		#IF IS_DEBUG_BUILD
//		NET_NL()NET_PRINT("		-  SET_WARNING_MESSAGE_WITH_HEADER = FALSE ")
//		NET_NL()
//		#ENDIF
	ENDIF
	
	
ENDPROC

PROC LOGIC_MPHUD_NOT_CONNECTED(MPHUD_PLACEMENT_TOOLS& Placement)

	CONST_INT ACCEPT 1
	
	
	
	IF Placement.aBool[1]
	
		#IF IS_DEBUG_BUILD
		NET_NL()NET_PRINT("		- CONTROL waiting on IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT) ")
		NET_NL()
		#ENDIF
		
		#IF USE_FINAL_PRINTS
		PRINTLN_FINAL("		- CONTROL waiting on IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT) ")
		#ENDIF
	
		CLEAR_BIT(Placement.ButtonReleasedBitset, ACCEPT)
		
	//	PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")

			#IF IS_DEBUG_BUILD
				NET_NL()NET_PRINT("LOGIC_MPHUD_NOT_CONNECTED: DID_A_BAIL_HAPPEN() = ")NET_PRINT_BOOL(DID_A_BAIL_HAPPEN())
				NET_NL()NET_PRINT("LOGIC_MPHUD_NOT_CONNECTED: HAS_QUIT_CURRENT_GAME() = ")NET_PRINT_BOOL(HAS_QUIT_CURRENT_GAME())
				NET_NL()NET_PRINT("LOGIC_MPHUD_NOT_CONNECTED: Model is MP_F_FREEMODE_01 = ")NET_PRINT_BOOL(GET_PLAYER_MODEL() = MP_F_FREEMODE_01)
				NET_NL()NET_PRINT("LOGIC_MPHUD_NOT_CONNECTED: Model is MP_M_FREEMODE_01 = ")NET_PRINT_BOOL(GET_PLAYER_MODEL() = MP_M_FREEMODE_01)
				NET_NL()NET_PRINT("LOGIC_MPHUD_NOT_CONNECTED: Model is U_M_M_FILMDIRECTOR = ")NET_PRINT_BOOL(GET_PLAYER_MODEL() = U_M_M_FILMDIRECTOR)
			
			#ENDIF
			
			#IF USE_FINAL_PRINTS
				PRINTLN_FINAL("LOGIC_MPHUD_NOT_CONNECTED: DID_A_BAIL_HAPPEN() = ", DID_A_BAIL_HAPPEN())
				PRINTLN_FINAL("LOGIC_MPHUD_NOT_CONNECTED: HAS_QUIT_CURRENT_GAME() = ", HAS_QUIT_CURRENT_GAME())
				PRINTLN_FINAL("LOGIC_MPHUD_NOT_CONNECTED: Model is MP_F_FREEMODE_01 = ", GET_PLAYER_MODEL() = MP_F_FREEMODE_01)
				PRINTLN_FINAL("LOGIC_MPHUD_NOT_CONNECTED: Model is MP_M_FREEMODE_01 = ", GET_PLAYER_MODEL() = MP_M_FREEMODE_01)
				PRINTLN_FINAL("LOGIC_MPHUD_NOT_CONNECTED: Model is U_M_M_FILMDIRECTOR = ", GET_PLAYER_MODEL() = U_M_M_FILMDIRECTOR)
			#ENDIF
			
			Placement.aBool[1] = FALSE		
			
			REQUEST_TRANSITION(TRANSITION_STATE_RETURN_TO_SINGLEPLAYER, GET_CURRENT_GAMEMODE(), HUD_STATE_LOADING, TRUE)
	
	ENDIF	
	
	
	
	
	IF NOT IS_SYSTEM_UI_BEING_DISPLAYED()
	AND Placement.bHasSignoutMessageAppeared = TRUE
	AND IS_SAFE_TO_QUIT_BAIL_SCREEN()
	
		#IF IS_DEBUG_BUILD
		NET_NL()NET_PRINT("		- CONTROL listening for HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT) ")
		NET_NL()
		#ENDIF
		
		#IF USE_FINAL_PRINTS
		PRINTLN_FINAL("		- CONTROL listening for HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT) ")
		#ENDIF


		IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
			#IF IS_DEBUG_BUILD
			NET_NL()NET_PRINT("		- CONTROL SET_BIT(Placement.ButtonReleasedBitset, ACCEPT)!!!!! ")
			NET_NL()
			#ENDIF
			#IF USE_FINAL_PRINTS
			PRINTLN_FINAL("		- CONTROL SET_BIT(Placement.ButtonReleasedBitset, ACCEPT)!!!!! ")
			#ENDIF

			SET_BIT(Placement.ButtonReleasedBitset, ACCEPT)
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		NET_NL()NET_PRINT("		- CONTROL NOT listening for buttons yet ")
		NET_NL()
		#ENDIF
		#IF USE_FINAL_PRINTS
		PRINTLN_FINAL("		- CONTROL NOT listening for buttons yet ")
		#ENDIF
	ENDIF

	IF IS_SAFE_TO_QUIT_BAIL_SCREEN()

		IF IS_BIT_SET(Placement.ButtonReleasedBitset, ACCEPT)
		
			Placement.aBool[1] = TRUE
			
			#IF IS_DEBUG_BUILD
			NET_NL()NET_PRINT("		- CONTROL set Placement.aBool[1] = TRUE ")
			NET_NL()
			#ENDIF
			#IF USE_FINAL_PRINTS
			PRINTLN_FINAL("		- CONTROL set Placement.aBool[1] = TRUE ")
			#ENDIF
			
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		NET_NL()NET_PRINT("		- CONTROL IS_SAFE_TO_QUIT_BAIL_SCREEN = FALSE ")
		NET_NL()
		#ENDIF
		#IF USE_FINAL_PRINTS
		PRINTLN_FINAL("		- CONTROL IS_SAFE_TO_QUIT_BAIL_SCREEN = FALSE ")
		#ENDIF
	ENDIF

	BOOL bCanSkipMessageScreen = NOT Placement.bHasSignoutMessageAppeared
	
	#IF FEATURE_GEN9_RELEASE
	IF IS_PROSPERO_VERSION()
	OR IS_SCARLETT_VERSION()
		IF bCanSkipMessageScreen
			IF g_SignInStructDetails.bWasOnline = FALSE 
			OR g_SignInStructDetails.bIsOnline = FALSE
				// R5034 : Signing in and out before the message is displayed can cause the message not to be displayed.
				bCanSkipMessageScreen = FALSE
				
				
				#IF IS_DEBUG_BUILD
				NET_NL()NET_PRINT("		- gen 9 - setting bCanSkipMessageScreen = FALSE ")
				NET_NL()
				#ENDIF
				#IF USE_FINAL_PRINTS
				PRINTLN_FINAL("		- gen 9 - setting bCanSkipMessageScreen = FALSE ")
				#ENDIF		
				
			ENDIF	
		ENDIF
	ENDIF
	#ENDIF
			
	IF bCanSkipMessageScreen
		//Removed for Class A TRC 1521865
		IF (NETWORK_IS_SIGNED_ONLINE()
		AND NETWORK_IS_SIGNED_IN()
		AND NETWORK_HAVE_ONLINE_PRIVILEGES()
		AND SCRIPT_IS_CLOUD_AVAILABLE()
		AND NOT IS_SYSTEM_UI_BEING_DISPLAYED()
		AND ARE_PROFILE_SETTINGS_VALID())
		OR HAS_SYSTEM_SERVICE_TRIGGERED()

			SETUP_RECONNECTIONS()
			
		
			IF GET_CURRENT_TRANSITION_BAILING_OPTIONS() = BAILING_OPTIONS_NOT_CONNECTED
				SET_CURRENT_TRANSITION_BAILING_OPTIONS(BAILING_OPTIONS_EMPTY)
			ENDIF
			
			IF HAS_SYSTEM_SERVICE_TRIGGERED()		
				REQUEST_TRANSITION(TRANSITION_STATE_RETURN_TO_SINGLEPLAYER, GET_CURRENT_GAMEMODE(), HUD_STATE_LOADING, TRUE)				
			ELIF GET_CURRENT_GAMEMODE() = GAMEMODE_CREATOR
			OR GET_JOINING_GAMEMODE() = GAMEMODE_CREATOR
			OR GET_PAUSE_MENU_WARP_TO_GAMEMODE() = GAMEMODE_CREATOR
				TELL_TRANSITION_BAIL_HAPPENED(FALSE)
				SET_HAS_ENTERED_GTAO_THIS_BOOT(TRUE)
				TELL_TRANSITION_BAIL_HAPPENED(FALSE)
				SET_JOINING_GAMEMODE(GAMEMODE_CREATOR)
				SET_PAUSE_MENU_WARP_TO_MODE(GAMEMODE_CREATOR)
				HUD_CHANGE_STATE(HUD_STATE_PRE_HUD_CHECKS)
				TRANSITION_CHANGE_STATE(TRANSITION_STATE_PRE_HUD_CHECKS)
				
			ELIF GET_JOINING_GAMEMODE() = GAMEMODE_SP
			
				TELL_TRANSITION_BAIL_HAPPENED(FALSE)
				SET_HAS_ENTERED_GTAO_THIS_BOOT(TRUE)
				HUD_CHANGE_STATE(HUD_STATE_LOADING)
				TRANSITION_CHANGE_STATE(TRANSITION_STATE_TERMINATE_SESSION)
				
			ELIF (GET_CURRENT_GAMEMODE() = GAMEMODE_SP
			OR GET_CURRENT_GAMEMODE() = GAMEMODE_EMPTY)	
			
			
				TELL_TRANSITION_BAIL_HAPPENED(FALSE)
				SET_HAS_ENTERED_GTAO_THIS_BOOT(TRUE)
				
				//Changing direction for this error due to exploit
				//url:bugstar:4094207 - [PUBLIC][EXPLOIT] Buy everything for free in GTA Online, after disconnecting from single player.
				//SET_JOINING_GAMEMODE(GAMEMODE_FM)
				//HUD_CHANGE_STATE(HUD_STATE_PRE_HUD_CHECKS)
				//TRANSITION_CHANGE_STATE(TRANSITION_STATE_PRE_HUD_CHECKS)				
				
				REQUEST_TRANSITION(TRANSITION_STATE_TERMINATE_SESSION, GAMEMODE_SP, HUD_STATE_LOADING, TRUE)
			ENDIF
			
			SET_SYSTEM_SERVICE_TRIGGERED(FALSE)
			
		ENDIF

	
	ENDIF

ENDPROC

PROC PROCESS_MPHUD_NOT_CONNECTED(MPHUD_PLACEMENT_TOOLS& Placement)
	
	// initialise data
	IF NOT (Placement.bHudScreenInitialised)
		INIT_MPHUD_NOT_CONNECTED(Placement)
		IF IS_PAUSE_MENU_ACTIVE()
			SET_PAUSE_MENU_ACTIVE(FALSE)
		ENDIF
		
		Placement.aBool[0] = FALSE
		Placement.aBool[1] = FALSE
		
		//Untested
//		SET_CODE_STATS_LOADED(FALSE, 0)
//		STAT_CLEAR_SLOT_FOR_RELOAD(0)
//		
//		SET_CODE_STATS_LOADED(FALSE, 1)
//		STAT_CLEAR_SLOT_FOR_RELOAD(1)
//		
//		SET_CODE_STATS_LOADED(FALSE, 2)
//		STAT_CLEAR_SLOT_FOR_RELOAD(2)
//		
//		SET_CODE_STATS_LOADED(FALSE, 3)
//		STAT_CLEAR_SLOT_FOR_RELOAD(3)
//		
//		SET_CODE_STATS_LOADED(FALSE, 4)
//		STAT_CLEAR_SLOT_FOR_RELOAD(4)
//		
//		SET_CODE_STATS_LOADED(FALSE, 5)
//		STAT_CLEAR_SLOT_FOR_RELOAD(5)
		
		RUN_MP_DISCONNECTION_CLEANUP(Placement)
		
		REINIT_NET_TIMER(Placement.aTimer)
		
		IF LOBBY_AUTO_MULTIPLAYER_FREEMODE()
			LOBBY_SET_AUTO_MULTIPLAYER(FALSE)
			SHUTDOWN_AND_LAUNCH_SINGLE_PLAYER_GAME()
			EXIT
		ENDIF
		
		IF DID_A_BAIL_HAPPEN() = FALSE AND IS_SCARLETT_VERSION() = FALSE
			DISPLAY_SYSTEM_SIGNIN_UI()
		ENDIF
		
		Placement.bHudScreenInitialised = TRUE
	ENDIF

	RUN_MP_DISCONNECTION_CLEANUP_EVERY_FRAME(TRUE, FALSE)
	// logic	
	LOGIC_MPHUD_NOT_CONNECTED(Placement)
	
	
	RENDER_MPHUD_NOT_CONNECTED(Placement)
	
ENDPROC

