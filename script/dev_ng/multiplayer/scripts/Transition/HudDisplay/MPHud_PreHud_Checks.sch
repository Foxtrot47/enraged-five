
USING "screens_header.sch"
USING "net_events.sch"
USING "script_network.sch"
USING "Transition_Common.sch"

//PROC INIT_MPHUD_PREHUD_CHECKS(MPHUD_PLACEMENT_TOOLS& Placement)
//
//	RESET_ALL_SPRITE_PLACEMENT_VALUES(Placement.SpritePlacement)
//
//		
//	Placement.TextPlacement[0].x = 0.500
//	Placement.TextPlacement[0].y = 0.218
//	
//	
//	//Scaleform movie 
//	Placement.SpritePlacement[0].x = 0.390
//	Placement.SpritePlacement[0].y = 0.467
//	Placement.SpritePlacement[0].w = 1
//	Placement.SpritePlacement[0].h = 1
//	Placement.SpritePlacement[0].r = 255
//	Placement.SpritePlacement[0].g = 255
//	Placement.SpritePlacement[0].b = 255
//	Placement.SpritePlacement[0].a = 200
//	
//		//Loading Icon	
//	Placement.SpritePlacement[1].x = 0.387
//	Placement.SpritePlacement[1].y = 0.903
//	Placement.SpritePlacement[1].w = 0.450
//	Placement.SpritePlacement[1].h = 0.041
//	Placement.SpritePlacement[1].r = 255
//	Placement.SpritePlacement[1].g = 255
//	Placement.SpritePlacement[1].b = 255
//	Placement.SpritePlacement[1].a = 200
//	
//	SETTIMERA(0)
//	
//ENDPROC
//
////WIDGET_GROUP_ID AWidget
////INT ProfileBitset
//
//PROC RENDER_MPHUD_PREHUD_CHECKS(MPHUD_PLACEMENT_TOOLS& Placement)
//
//	// 'SELECT MP CHARACTER...'
////	DRAW_TEXT(Placement.MainTitle, Placement.aStyle.TS_TITLEHUD, "HUD_MULT_TIT") // "HUD_LOADLOAD")	
//		
//	// main background
//		// main background
////	BOOL DrawTabs = FALSE
////
////		
////	// main background
////	IF DrawTabs = TRUE
////		DRAW_MAIN_HUD_BACKGROUND()
////	ENDIF
//	
//	
//	
//	VECTOR SlotOffset[5]
//	VECTOR SlotHeading[5]
//	
//
//	SET_PED_SLOT_OFFSETS(SlotOffset, SlotHeading)
//	
//	
//	VECTOR PlaceGuy, HeadGuy
//	VECTOR CamRot
//	VECTOR CamCoord
//	IF DOES_CAM_EXIST(GET_RENDERING_CAM())
//		CamRot = GET_CAM_ROT(GET_RENDERING_CAM())
//		CamCoord = GET_CAM_COORD(GET_RENDERING_CAM())
//	ENDIF
//	
//	
//	IF GET_CURRENT_GAMEMODE() = GAMEMODE_FM
//	OR GET_JOINING_GAMEMODE() = GAMEMODE_FM
//		INT I
//		FOR I = 0 TO MAX_NUM_CHARACTER_SLOTS-1
//		
//			PlaceGuy = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(CamCoord,CamRot.z, SlotOffset[I])
//			HeadGuy = CamRot+SlotHeading[I] 
//		
//			IF IS_STAT_CHARACTER_ACTIVE(I)
//				RUN_PED_MENU(Placement.IdlePed[I], GET_PLAYER_MODEL_FOR_TEAM_VARIATION_FOR_MENU( GET_STAT_CHARACTER_TEAM(I), GET_PACKED_STAT_INT(PACKED_CHAR_PICTURE, I)), PlaceGuy, HeadGuy, FALSE,IS_CHARACTER_MALE(I), I)	
//			ENDIF
//			
//		ENDFOR
//		
//		SET_ALL_MENUPED_VISIBLE(Placement.SelectionPed, Placement.IdlePed, FALSE)
//	ENDIF
//	
//	
//	
//	
////	SPRITE_PLACEMENT ScaleformSprite
////
////	SCALEFORM_TABS_INPUT_DATA TabsData
////	TabsData.MainTitle = "HUD_GNGSEL_TIT"
////	INT PotentialPed = GET_JOINING_CHARACTER()
////	COMPILE_SCALEFORM_TABS(Placement.IdlePed[PotentialPed], TabsData, Placement.ScaleformTabStruct )
////	RUN_SCALEFORM_TABS(Placement.ButtonMovie[GET_SCALEFORM_INDEX_FOR_HUD(HUD_SCALEFORM_TABS)], ScaleformSprite, Placement.ScaleformTabStruct)
//	
//
//
//	SPRITE_PLACEMENT ScaleformSprite
////	 ScaleformSprite = GET_SCALEFORM_LOADING_ICON_POSITION()
//	Placement.ScaleformLoadingStruct.sMainStringSlot = "HUD_LOADPROF"
//
//	RUN_SCALEFORM_LOADING_ICON(Placement.ScaleformLoadingStruct, SHOULD_REFRESH_SCALEFORM_LOADING_ICON(Placement.ScaleformLoadingStruct))
//
//
//	SWITCH GET_CURRENT_GAMEMODE() //Needs to be tied to SP or handle hte player skipping loading stats then kicked from the game, so they don't go "BACK" into the dead game.
//	
//		CASE GAMEMODE_EMPTY	
//		CASE GAMEMODE_SP
//			ScaleformSprite = GET_SCALEFORM_INSTRUCTIONAL_BUTTON_POSITION()
//			
//			ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL), "HUD_INPUT3", Placement.ScaleformStruct)
//			RUN_SCALEFORM_INSTRUCTIONAL_BUTTONS(Placement.ButtonMovie[GET_SCALEFORM_INDEX_FOR_HUD(HUD_SCALEFORM_INSTRUCTIONAL)], ScaleformSprite, Placement.ScaleformStruct)
//		BREAK
//	ENDSWITCH
//
//	
//ENDPROC
//
//PROC LOGIC_MPHUD_PREHUD_CHECKS()
//
//	SWITCH GET_CURRENT_GAMEMODE()
//	
//		CASE GAMEMODE_EMPTY	
//		CASE GAMEMODE_SP
//			IF HAS_HUD_CONTROL_BEEN_PRESSED(INPUT_FRONTEND_CANCEL)
//				HUD_CHANGE_STATE(HUD_STATE_NO_DISPLAY)
//				TRANSITION_CHANGE_STATE(TRANSITION_STATE_SP_SWOOP_DOWN)
//			ENDIF
//		BREAK
//	ENDSWITCH
//
//ENDPROC
//
//PROC PROCESS_MPHUD_PREHUD_CHECKS(MPHUD_PLACEMENT_TOOLS& Placement)
//	
//	// initialise data
//	IF NOT (Placement.bHudScreenInitialised)
//		INIT_MPHUD_PREHUD_CHECKS(Placement)	
//	
//		Placement.bHudScreenInitialised = TRUE
//	ENDIF
//
//	IF HAS_ALL_COMMON_HUD_ELEMENTS_LOADED(Placement)					
//		RENDER_MPHUD_PREHUD_CHECKS(Placement)
//	ENDIF
//		
//	LOGIC_MPHUD_PREHUD_CHECKS()		
//			
//ENDPROC
































PROC RENDER_XML_MPHUD_PREHUD_CHECKS(MPHUD_PLACEMENT_TOOLS& Placement)


	
//	SPRITE_PLACEMENT ScaleformSprite

	CHECK_LOADING_ICON_CHANGE(Placement)


	SET_SCALEFORM_LOADING_ICON_DISPLAY(Placement.ScaleformLoadingStruct, LOADING_ICON_LOADING)

//	ScaleformSprite = GET_SCALEFORM_LOADING_ICON_POSITION()
	Placement.ScaleformLoadingStruct.sMainStringSlot = GET_TRANSITION_STRING()
	RUN_SCALEFORM_LOADING_ICON(Placement.ScaleformLoadingStruct, SHOULD_REFRESH_SCALEFORM_LOADING_ICON(Placement.ScaleformLoadingStruct))


	LOAD_UP_LAST_FRAME_DATA(Placement)



ENDPROC






PROC LOGIC_XML_MPHUD_PREHUD_CHECKS(MPHUD_PLACEMENT_TOOLS& Placement)		


	CONST_INT CANCEL 0


	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_CANCEL)
		SET_BIT(Placement.ButtonReleasedBitset, CANCEL)
	ENDIF

	IF IS_SCREEN_FADED_IN()
	AND LOBBY_AUTO_MULTIPLAYER_FREEMODE() = FALSE
	AND GET_IS_LAUNCH_FROM_LIVE_AREA() = FALSE	
	AND GET_IS_LIVE_AREA_LAUNCH_WITH_CONTENT() = FALSE
//		NET_PRINT("RETURNSP: PREHUD_CHECKS: CAN PRESS RETURN ")
		
		IF IS_BIT_SET(Placement.ButtonReleasedBitset, CANCEL)
//			NET_PRINT("RETURNSP: PREHUD_CHECKS: HAS PRESSED RETURN ")
			IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL) 
//				NET_PRINT("RETURNSP: PREHUD_CHECKS: HAS RELEASED RETURN ")
				CLEAR_BIT(Placement.ButtonReleasedBitset, CANCEL)
				
				PLAY_SOUND_FRONTEND(-1,"CANCEL","HUD_FRONTEND_DEFAULT_SOUNDSET")
				UGC_CLEAR_QUERY_RESULTS_BY_OFFLINE_STATUS()
				SET_TRANSITION_STRING("")
				SET_MIGRATION_NOT_FINISHED_BLOCK_SAVE(Placement)
				
				IF GET_CONFIRM_INVITE_INTO_GAME_STATE()
					SWITCH GET_CURRENT_GAMEMODE()
			
						CASE GAMEMODE_EMPTY	
						CASE GAMEMODE_SP
						
							PAUSE_MENU_DEACTIVATE_CONTEXT( HASH("RETURN_TO_MENU"))
							PAUSE_MENU_REDRAW_INSTRUCTIONAL_BUTTONS() 	
							
	//						IGNORE_INVITE()
							HUD_CHANGE_STATE(HUD_STATE_IGNORE_INVITE)
							TRANSITION_CHANGE_STATE(TRANSITION_STATE_WAIT_HUD_EXIT)
						BREAK
					ENDSWITCH
					
				ELSE
				
					//as through the pause menu FORCE_CLEANUP(SP_TO_MP) is called straight away now. 
//						IF IS_PAUSE_MENU_REQUESTING_TRANSITION()
					
						
						SWITCH GET_CURRENT_GAMEMODE()
				
							CASE GAMEMODE_EMPTY	
							CASE GAMEMODE_SP
							
								PAUSE_MENU_DEACTIVATE_CONTEXT( HASH("RETURN_TO_MENU"))
								PAUSE_MENU_REDRAW_INSTRUCTIONAL_BUTTONS() 	
								CANCEL_MP_JOIN(TRUE)
								REQUEST_TRANSITION(TRANSITION_STATE_RETURN_TO_SINGLEPLAYER, GAMEMODE_EMPTY, HUD_STATE_LOADING)
							BREAK
						ENDSWITCH
					
//						ELSE
//							SWITCH GET_CURRENT_GAMEMODE()
//					
//								CASE GAMEMODE_EMPTY	
//								CASE GAMEMODE_SP
//								
//									PAUSE_MENU_DEACTIVATE_CONTEXT( HASH("RETURN_TO_MENU"))
//									PAUSE_MENU_REDRAW_INSTRUCTIONAL_BUTTONS() 	
//									CANCEL_MP_JOIN(TRUE)
//		
//									HUD_CHANGE_STATE(HUD_STATE_LOADING)
//									SET_JOINING_GAMEMODE(GAMEMODE_EMPTY)
//									TRANSITION_CHANGE_STATE(TRANSITION_STATE_SP_SWOOP_DOWN)
//								BREAK
//							ENDSWITCH
//						ENDIF
				
				ENDIF
			ENDIF
		ENDIF
	ELSE
		CLEAR_BIT(Placement.ButtonReleasedBitset, CANCEL)
	ENDIF


ENDPROC


PROC PROCESS_XML_MPHUD_PREHUD_CHECKS(MPHUD_PLACEMENT_TOOLS& Placement)
	
	IF (IS_PAUSE_MENU_ACTIVE() = FALSE
	AND Placement.PreviousScreen = HUD_STATE_SELECT_CHARACTER_FM)
	OR (Placement.PreviousScreen != HUD_STATE_SELECT_CHARACTER_FM)
	
		IF NOT IS_PAUSE_MENU_RESTARTING()
		AND GET_PAUSE_MENU_STATE() != PM_RESTARTING
		AND GET_PAUSE_MENU_STATE() != PM_SHUTTING_DOWN
		AND GET_PAUSE_MENU_STATE() != PM_STARTING_UP
			PRINTLN("PROCESS_XML_MPHUD_PREHUD_CHECKS: GET_PAUSE_MENU_STATE = ", ENUM_TO_INT(GET_PAUSE_MENU_STATE()))
			// initialise data
			IF NOT (Placement.bHudScreenInitialised)

				Placement.bDoIHaveControl = TRUE

				IF IS_PAUSE_MENU_ACTIVE()
				AND GET_PAUSE_MENU_STATE() = PM_READY
					IF IS_THIS_SCREEN_ACTIVE(FE_MENU_VERSION_EMPTY_NO_BACKGROUND) = FALSE
						PRINTLN("PROCESS_XML_MPHUD_PREHUD_CHECKS: RESTART_FRONTEND_MENU")
						RESTART_FRONTEND_MENU(FE_MENU_VERSION_EMPTY_NO_BACKGROUND)
					ENDIF
				ELSE
					PRINTLN("PROCESS_XML_MPHUD_PREHUD_CHECKS: ACTIVATE_FRONTEND_MENU")
					#IF IS_DEBUG_BUILD
					IF GET_PAUSE_MENU_STATE() = PM_READY
					AND IS_PAUSE_MENU_ACTIVE()
						PRINTLN("PROCESS_XML_MPHUD_PREHUD_CHECKS: GET_CURRENT_FRONTEND_MENU_VERSION = ", ENUM_TO_INT(GET_CURRENT_FRONTEND_MENU_VERSION()))
					ENDIF
					#ENDIF
					ACTIVATE_FRONTEND_MENU(FE_MENU_VERSION_EMPTY_NO_BACKGROUND, FALSE)
				ENDIF
				SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_FADE)
				FORCE_SCRIPTED_GFX_WHEN_FRONTEND_ACTIVE(TRUE)			
//				IF IS_SCREEN_FADED_IN()
//					SWITCH GET_CURRENT_GAMEMODE()
//						CASE GAMEMODE_EMPTY	
//						CASE GAMEMODE_SP
//							IF IS_SCREEN_FADED_IN()
//							AND LOBBY_AUTO_MULTIPLAYER_FREEMODE() = FALSE
//							AND GET_IS_LAUNCH_FROM_LIVE_AREA() = FALSE	
//							AND GET_IS_LIVE_AREA_LAUNCH_WITH_CONTENT() = FALSE
//								PAUSE_MENU_ACTIVATE_CONTEXT( HASH("RETURN_TO_MENU"))
//								PAUSE_MENU_REDRAW_INSTRUCTIONAL_BUTTONS() 
//							ELSE
//								PAUSE_MENU_DEACTIVATE_CONTEXT( HASH("RETURN_TO_MENU"))
//								PAUSE_MENU_REDRAW_INSTRUCTIONAL_BUTTONS() 
//							ENDIF
//						BREAK
//						DEFAULT
//							PAUSE_MENU_DEACTIVATE_CONTEXT( HASH("RETURN_TO_MENU"))
//							PAUSE_MENU_REDRAW_INSTRUCTIONAL_BUTTONS() 
//						BREAK
//					ENDSWITCH
//				ENDIF
				
//				START_WIDGET_GROUP("SPINNER")
//					ADD_WIDGET_FLOAT_SLIDER("iXPosLoadingIconAboveButton", iXPosLoadingIconAboveButton, -1, 1, 0.001)
//					ADD_WIDGET_FLOAT_SLIDER("iYPosLoadingIconAboveButton", iYPosLoadingIconAboveButton, -1, 1, 0.001)
//					ADD_WIDGET_FLOAT_SLIDER("iXPosLoadingIconNormal", iXPosLoadingIconNormal, -1, 1, 0.001)
//					ADD_WIDGET_FLOAT_SLIDER("iYPosLoadingIconNormal", iYPosLoadingIconNormal, -1, 1, 0.001)
//
//				STOP_WIDGET_GROUP()
				
				Placement.bSetupTabs = FALSE
				Placement.bHudScreenInitialised = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF NEED_TO_DISPLAY_Y_ON_MAIN_TRANSITION()
		IF Placement.aBool[0] = FALSE
			PAUSE_MENU_ACTIVATE_CONTEXT( HASH("ENTER_PLAYLIST"))
			PAUSE_MENU_REDRAW_INSTRUCTIONAL_BUTTONS()
			Placement.aBool[0] = TRUE
		ENDIF
	ELSE
		IF Placement.aBool[0] = TRUE
			PAUSE_MENU_DEACTIVATE_CONTEXT( HASH("ENTER_PLAYLIST"))
			PAUSE_MENU_REDRAW_INSTRUCTIONAL_BUTTONS()
			Placement.aBool[0] = FALSE
		ENDIF
	ENDIF
		
	IF (Placement.bHudScreenInitialised)
	AND IS_PAUSE_MENU_ACTIVE()
	AND IS_PAUSE_MENU_RESTARTING() = FALSE
		IF IS_FRONTEND_READY_FOR_CONTROL()
		
			IF HAS_MENU_TRIGGER_EVENT_OCCURRED()
				NET_NL()NET_PRINT("HAS_MENU_TRIGGER_EVENT_OCCURRED = TRUE")
				GET_MENU_TRIGGER_EVENT_DETAILS(Placement.aNextScreen, Placement.iMenu)
				NET_NL()NET_PRINT("		- aNextScreen = ")NET_PRINT(GET_XML_SCREEN_STRING_VALUE(Placement.aNextScreen))
				NET_NL()NET_PRINT("		- iMenu = ")NET_PRINT_INT(Placement.iMenu)
			ENDIF
			
			IF HAS_MENU_LAYOUT_CHANGED_EVENT_OCCURRED()
 
				NET_NL()NET_PRINT("HAS_MENU_LAYOUT_CHANGED_EVENT_OCCURRED = TRUE")
				GET_MENU_LAYOUT_CHANGED_EVENT_DETAILS(Placement.CurrentScreen, Placement.aNextScreen, Placement.iMenu)
				NET_NL()NET_PRINT("		- CurrentScreen = ")NET_PRINT(GET_XML_SCREEN_STRING_VALUE(Placement.CurrentScreen))
				NET_NL()NET_PRINT("		- aNextScreen = ")NET_PRINT(GET_XML_SCREEN_STRING_VALUE(Placement.aNextScreen))
				NET_NL()NET_PRINT("		- iMenu = ")NET_PRINT_INT(Placement.iMenu)
					
				TAKE_CONTROL_OF_FRONTEND()	
				
				Placement.bDoIHaveControl = TRUE
				
			ENDIF
	
			
			
		ELSE
			NET_NL()NET_PRINT("IS_FRONTEND_READY_FOR_CONTROL = FALSE ")
		ENDIF

		
//		IF Placement.bDoIHaveControl = TRUE	
//			NET_NL()NET_PRINT("LOGIC_XML_MPHUD_PREHUD_CHECKS - called ")
//			LOGIC_XML_MPHUD_PREHUD_CHECKS(Placement)	
//		ENDIF
		
	ENDIF
	
//	LOGIC_XML_MPHUD_PREHUD_CHECKS(Placement)	
	
	RENDER_XML_MPHUD_PREHUD_CHECKS(Placement)

ENDPROC




