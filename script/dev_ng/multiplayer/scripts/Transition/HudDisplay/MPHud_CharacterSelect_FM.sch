USING "screens_header.sch"
USING "screen_tabs.sch"

USING "net_events.sch"
USING "net_rank_ui.sch"
USING "net_mission.sch"
USING "net_blips.sch"
USING "net_transition.sch"

USING "Transition_Common.sch"

#IF IS_DEBUG_BUILD
USING "net_hud_debug.sch"
#ENDIF


/// PURPOSE: Displays spinner when ped heads are loading
PROC SET_CHAR_SELECT_AS_BUSY(MPHUD_PLACEMENT_TOOLS& Placement)

	IF Placement.PreviousScreen != HUD_STATE_DELETE_CHARACTER_PROMPT
		IF IS_STAT_CHARACTER_ACTIVE(0) AND HAS_SLOT_BEEN_IGNORED_CODE(1) = FALSE
			CHAR_SLOT_BUSY_SPINNER(TRUE, 0)
		ENDIF
		IF IS_STAT_CHARACTER_ACTIVE(1) AND HAS_SLOT_BEEN_IGNORED_CODE(2) = FALSE
			IF Placement.ScreenPlace.iSelectedCharacter = 1
			CHAR_SLOT_BUSY_SPINNER(TRUE, 1)
				CHAR_SLOT_BUSY_SPINNER(FALSE, 2)
			ELSE
				CHAR_SLOT_BUSY_SPINNER(TRUE, 2)
				CHAR_SLOT_BUSY_SPINNER(FALSE, 1)
			ENDIF
		ENDIF
		IF IS_STAT_CHARACTER_ACTIVE(2) AND HAS_SLOT_BEEN_IGNORED_CODE(3) = FALSE
			CHAR_SLOT_BUSY_SPINNER(TRUE, 2)
		ENDIF
		IF IS_STAT_CHARACTER_ACTIVE(3) AND HAS_SLOT_BEEN_IGNORED_CODE(4) = FALSE
			CHAR_SLOT_BUSY_SPINNER(TRUE, 3)
		ENDIF
		IF IS_STAT_CHARACTER_ACTIVE(4) AND HAS_SLOT_BEEN_IGNORED_CODE(5) = FALSE
			CHAR_SLOT_BUSY_SPINNER(TRUE, 4)
		ENDIF
	ENDIF


ENDPROC

//PROC REMOVE_ALL_QUEUED_SAVES_DUE_TO_CHARACTER_SWAP()
//	NET_NL()NET_PRINT("[BCLOAD] REMOVE_ALL_QUEUED_SAVES_DUE_TO_CHARACTER_SWAP - CALLED")
//	INT I
//	FOR I = 0 TO MAX_NUM_SAVE_QUEUE-1
//		g_bPrivate_SaveRequested[I] = STAT_SAVETYPE_DEFAULT
//	ENDFOR
//	
//	g_bPrivate_ProcessingWhichSave = -1
//	
//ENDPROC


PROC PROCESS_MPHUD_CHAR_SELECT_INFORBOX(MPHUD_PLACEMENT_TOOLS& Placement)
	
	
	PRIVATE_ADD_SCALEFORMXML_CHAR_SELECT_CURRENT_CHAR(Placement.ScreenPlace.iSelectedCharacter, Placement.ScaleformXMLCharSelectStruct)

ENDPROC


PROC RENDER_MPHUD_CHAR_SELECT_PEDS_ONLY(MPHUD_PLACEMENT_TOOLS& Placement, BOOL& EverythingSetup)

	INT I
	
	VECTOR SlotOffset[5]
	VECTOR SlotHeading[5]


	SET_PED_SLOT_OFFSETS(SlotOffset, SlotHeading)
	
	
	VECTOR PlaceGuy, HeadGuy//, LightGuy
	VECTOR CamRot
	VECTOR CamCoord
	IF DOES_CAM_EXIST(GET_RENDERING_CAM())
		CamRot = GET_CAM_ROT(GET_RENDERING_CAM())
		CamCoord = GET_CAM_COORD(GET_RENDERING_CAM())
	ENDIF
	
	EverythingSetup = TRUE

	FOR I = 0 TO MAX_NUM_CHARACTER_SLOTS-1
	
		
	
		
		BOOL bisSlotActive = IS_STAT_CHARACTER_ACTIVE(I) AND HAS_SLOT_BEEN_IGNORED_CODE(I+1) = FALSE
		IF bisSlotActive = TRUE

			PlaceGuy = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(CamCoord,CamRot.z, SlotOffset[I])
			HeadGuy = CamRot+SlotHeading[I] 
			
			
			
			IF RUN_PED_MENU(Placement.IdlePed[I], GET_PLAYER_MODEL_FOR_TEAM_VARIATION_FOR_MENU(GET_STAT_CHARACTER_TEAM(I), GET_PACKED_STAT_INT(PACKED_CHAR_PICTURE, I)), PlaceGuy, HeadGuy, FALSE,IS_CHARACTER_MALE(I), I, 50, FALSE, FALSE) = FALSE	
				EverythingSetup = FALSE
			ENDIF
			
		ENDIF
	ENDFOR
	
	IF NOT IS_PED_INJURED(Placement.IdlePed[0])
		FORCE_ALL_HEADING_VALUES_TO_ALIGN(Placement.IdlePed[0])
	ENDIF
	
	IF NOT IS_PED_INJURED(Placement.IdlePed[1])
		FORCE_ALL_HEADING_VALUES_TO_ALIGN(Placement.IdlePed[1])
	ENDIF

ENDPROC




PROC LOGIC_MPHUD_XML_CHARACTER_SELECT_FM(MPHUD_PLACEMENT_TOOLS& Placement)
	
	
	CONST_INT CANCEL 0
	CONST_INT ACCEPT 1
	CONST_INT Y 2
	CONST_INT LEFT 3
	CONST_INT RIGHT 4
	CONST_INT X 5
	CONST_INT L1 6
	CONST_INT R1 7
	CONST_INT SELECT_BUTTON 8
//	CONST_INT R3 9
//	CONST_INT L3 10

	IF IS_PAUSE_MENU_ACTIVE()
	AND NOT IS_PAUSE_MENU_RESTARTING()
	AND IS_FRONTEND_READY_FOR_CONTROL()
	AND IS_WARNING_MESSAGE_ACTIVE() = FALSE
	AND Placement.bHudScreenInitialised = TRUE
	
			IF NOT IS_BIT_SET(Placement.ButtonReleasedBitset, ACCEPT)
			AND NOT IS_BIT_SET(Placement.ButtonReleasedBitset, CANCEL)
				IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_CANCEL)
				OR IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_CURSOR_CANCEL)
					SET_BIT(Placement.ButtonReleasedBitset, CANCEL)
				ENDIF
			ENDIF
			IF NOT IS_BIT_SET(Placement.ButtonReleasedBitset, ACCEPT)
			AND NOT IS_BIT_SET(Placement.ButtonReleasedBitset, CANCEL)
				IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
					SET_BIT(Placement.ButtonReleasedBitset, ACCEPT)
				ENDIF
			ENDIF
			
			IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_Y)
				SET_BIT(Placement.ButtonReleasedBitset, Y)
			ENDIF
		
			IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_X)
				SET_BIT(Placement.ButtonReleasedBitset, X)
			ENDIF
			
			IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_LB)
				SET_BIT(Placement.ButtonReleasedBitset, L1)
			ENDIF
			
			IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_RB)
				SET_BIT(Placement.ButtonReleasedBitset, R1)
			ENDIF
			
			IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_LEFT)
				SET_BIT(Placement.ButtonReleasedBitset, Left)
			ENDIF
		
			IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_RIGHT)
				SET_BIT(Placement.ButtonReleasedBitset, Right)
			ENDIF
			
			IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_SELECT)
				SET_BIT(Placement.ButtonReleasedBitset, SELECT_BUTTON)
			ENDIF
			
//			IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_SCRIPT_RS)
//				SET_BIT(Placement.ButtonReleasedBitset, R3)
//			ENDIF
//			IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_SCRIPT_LS)
//				SET_BIT(Placement.ButtonReleasedBitset, L3)
//			ENDIF
			
	ELSE
	
	NET_NL()NET_PRINT("LOGIC_MPHUD_XML_CHARACTER_SELECT_FM: IS_PAUSE_MENU_ACTIVE = ")NET_PRINT_BOOL(IS_PAUSE_MENU_ACTIVE())
	NET_NL()NET_PRINT("LOGIC_MPHUD_XML_CHARACTER_SELECT_FM: IS_PAUSE_MENU_RESTARTING = ")NET_PRINT_BOOL(IS_PAUSE_MENU_RESTARTING())
	NET_NL()NET_PRINT("LOGIC_MPHUD_XML_CHARACTER_SELECT_FM: IS_FRONTEND_READY_FOR_CONTROL = ")NET_PRINT_BOOL(IS_FRONTEND_READY_FOR_CONTROL())
	NET_NL()NET_PRINT("LOGIC_MPHUD_XML_CHARACTER_SELECT_FM: IS_WARNING_MESSAGE_ACTIVE = ")NET_PRINT_BOOL(IS_WARNING_MESSAGE_ACTIVE())
				
				ENDIF
				


	
	
			
	// the selected character is the same as the selected tab	
//	IF IS_BIT_SET(Placement.ButtonReleasedBitset, CANCEL)
//		IF IS_SKYCAM_PAST_FIRST_CUT()
//			NET_NL()NET_PRINT("LOGIC_MPHUD_XML_CHARACTER_SELECT_FM: IS_BIT_SET INPUT_FRONTEND_CANCEL")
//			IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
//			OR IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_CURSOR_CANCEL)
//				CLEAR_BIT(Placement.ButtonReleasedBitset, CANCEL)
//				PLAY_SOUND_FRONTEND(-1,"CANCEL","HUD_FRONTEND_DEFAULT_SOUNDSET")
//	//			ANIMPOSTFX_STOP_CROSSFADE()
//				NET_NL()NET_PRINT("LOGIC_MPHUD_XML_CHARACTER_SELECT_FM: INPUT_FRONTEND_CANCEL")
//				CANCEL_MP_JOIN(TRUE)
//				RESET_PAUSE_MENU_WARP_REQUESTS()
//				UNLOAD_PEDHEADSHOT(Placement.ScaleformAlertScreenStruct.HeadshotPic)
//				
//				RESET_ALL_EXTERNAL_TERMINATE_REASONS()
//				
//				DELETE_ALL_SELECTION_MENUPED(Placement.SelectionPed)
//				DELETE_ALL_SELECTION_MENUPED(Placement.IdlePed)
//				DELETE_ALL_INACTIVE_MENUPED(Placement.SelectionPed, Placement.IdlePed)
//				
//				
//				IF LOBBY_AUTO_MULTIPLAYER_FREEMODE()
//				OR DID_BAIL_WHEN_ENTERING_ONLINE_FROM_BOOT()
//					SET_FRONTEND_ACTIVE(FALSE)
//					DO_SCREEN_FADE_OUT(0)
//				ENDIF
//				
//				RESET_BIRDS_ALPHA()
//				RUN_TRANSITION_MENU_AUDIO(FALSE)
//				UGC_CLEAR_QUERY_RESULTS_BY_OFFLINE_STATUS()
//				IF GET_CONFIRM_INVITE_INTO_GAME_STATE()
//					RUN_SCALEFORM_LOCK_RIGHT_MOUSE_BUTTON(FALSE)
//					HUD_CHANGE_STATE(HUD_STATE_IGNORE_INVITE)
//					TRANSITION_CHANGE_STATE(TRANSITION_STATE_WAIT_HUD_EXIT)
//				ELSE
////					HUD_CHANGE_STATE(HUD_STATE_LOADING)
////					TRANSITION_CHANGE_STATE(TRANSITION_STATE_RETURN_TO_SINGLEPLAYER)
//					RUN_SCALEFORM_LOCK_RIGHT_MOUSE_BUTTON(FALSE)
//					SET_JOINING_GAMEMODE(GAMEMODE_SP)
//					HUD_CHANGE_STATE(HUD_STATE_LOADING)
//					TRANSITION_CHANGE_STATE(TRANSITION_STATE_TERMINATE_SESSION)
//
//				ENDIF
//			ENDIF
//				ENDIF
//			ENDIF
	
	
//	IF IS_BIT_SET(Placement.ButtonReleasedBitset, SELECT_BUTTON)
//		IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_SELECT) 
//			CLEAR_BIT(Placement.ButtonReleasedBitset, SELECT_BUTTON)
//			
////			IF IS_STAT_CHARACTER_ACTIVE(Placement.ScreenPlace.iSelectedCharacter)
////				SET_JOINING_CHARACTER(Placement.ScreenPlace.iSelectedCharacter)
////				HUD_CHANGE_STATE(HUD_STATE_SELECTED_CHARACTER_SUMMARY)	
////				UNLOAD_PEDHEADSHOT(Placement.ScaleformAlertScreenStruct.HeadshotPic)
////				IF g_TurnOnNewTransitionSecondarySystem = FALSE
////					SET_FRONTEND_ACTIVE(FALSE)
////					SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
////				ENDIF
////			ENDIF
//		ENDIF
//	ENDIF
					

//	IF (IS_STAT_CHARACTER_ACTIVE(Placement.ScreenPlace.iSelectedCharacter)
//	OR (HAS_SLOT_BEEN_IGNORED_CODE(Placement.ScreenPlace.iSelectedCharacter+1) AND SCRIPT_IS_CLOUD_AVAILABLE()) )
//	AND HAS_ENTERED_OFFLINE_SAVE_FM() = FALSE
//	AND IS_SAVING_HAVING_TROUBLE() = FALSE
//		IF IS_BIT_SET(Placement.ButtonReleasedBitset, X)
//			IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_X)
//				CLEAR_BIT(Placement.ButtonReleasedBitset, X)
//				PLAY_SOUND_FRONTEND(-1,"SELECT","HUD_FRONTEND_DEFAULT_SOUNDSET")
//				RUN_SCALEFORM_LOCK_RIGHT_MOUSE_BUTTON(FALSE)	
//				HUD_CHANGE_STATE(HUD_STATE_DELETE_CHARACTER_PROMPT)	
//			ENDIF
//		ENDIF
//	ELSE
//		CLEAR_BIT(Placement.ButtonReleasedBitset, X)
//	ENDIF
	
//	IF (IS_STAT_CHARACTER_ACTIVE(Placement.ScreenPlace.iSelectedCharacter)
//	AND HAS_SLOT_BEEN_IGNORED_SCRIPT(Placement.ScreenPlace.iSelectedCharacter) = FALSE)
//	AND HAS_ENTERED_OFFLINE_SAVE_FM() = FALSE
//	AND IS_SAVING_HAVING_TROUBLE() = FALSE
//		IF IS_BIT_SET(Placement.ButtonReleasedBitset, Y)
//			IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_Y) 
//				CLEAR_BIT(Placement.ButtonReleasedBitset, Y)
//				PLAY_SOUND_FRONTEND(-1,"SELECT","HUD_FRONTEND_DEFAULT_SOUNDSET")
//				#IF IS_DEBUG_BUILD
//				NET_NL()NET_NL()NET_PRINT("INPUT_FRONTEND_Y - Editting Name ")
//				#ENDIF
//				Placement.iButtonPressedRun = TRUE
//			ENDIF
//		ENDIF
//	ELSE
//		CLEAR_BIT(Placement.ButtonReleasedBitset, Y)
//	ENDIF

//	IF IS_BIT_SET(Placement.ButtonReleasedBitset, L1)
//		IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_LB) 
//			CLEAR_BIT(Placement.ButtonReleasedBitset, L1)
////			PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
//
//			REFRESH_SCALEFORMXML_INSTRUCTIONAL_BUTTONS( Placement.ScaleformXMLStruct)
//			REFRESH_SCALEFORMXML_TABS_TOP_DETAIL(Placement.ScaleformXMLTabStruct)
//			Placement.bSetupTabs = TRUE
//			IF Placement.ScaleformXMLTabStruct.iHighlightTab = 0
//				Placement.ScaleformXMLTabStruct.iHighlightTab = 1
//			ELIF Placement.ScaleformXMLTabStruct.iHighlightTab = 1
//				Placement.ScaleformXMLTabStruct.iHighlightTab = 0
//			ENDIF
//			Placement.ScreenPlace.iSelectedCharacter = Placement.ScaleformXMLTabStruct.iHighlightTab
//			REFRESH_SCALEFORMXML_TABS_HIGHLIGHT(Placement.ScaleformXMLTabStruct)
//			REFRESH_SCALEFORMXML_TABS_FACES(Placement.ScaleformXMLTabStruct)
//		ENDIF
//	ENDIF
//	
//	IF IS_BIT_SET(Placement.ButtonReleasedBitset, R1)
//		IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_RB)
//			CLEAR_BIT(Placement.ButtonReleasedBitset, R1)
////			PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
//
//			REFRESH_SCALEFORMXML_INSTRUCTIONAL_BUTTONS( Placement.ScaleformXMLStruct)
//			REFRESH_SCALEFORMXML_TABS_TOP_DETAIL(Placement.ScaleformXMLTabStruct)
//			Placement.bSetupTabs = TRUE
//			IF Placement.ScaleformXMLTabStruct.iHighlightTab = 0
//				Placement.ScaleformXMLTabStruct.iHighlightTab = 1
//			ELIF Placement.ScaleformXMLTabStruct.iHighlightTab = 1
//				Placement.ScaleformXMLTabStruct.iHighlightTab = 0
//			ENDIF
//			Placement.ScreenPlace.iSelectedCharacter = Placement.ScaleformXMLTabStruct.iHighlightTab
//			
//			REFRESH_SCALEFORMXML_TABS_HIGHLIGHT(Placement.ScaleformXMLTabStruct)
//			REFRESH_SCALEFORMXML_TABS_FACES(Placement.ScaleformXMLTabStruct)
//			
//		ENDIF
//	ENDIF
	
//	IF IS_BIT_SET(Placement.ButtonReleasedBitset, Left)
//		IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT) 
//			CLEAR_BIT(Placement.ButtonReleasedBitset, Left)
//			PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
//
//			REFRESH_SCALEFORMXML_INSTRUCTIONAL_BUTTONS( Placement.ScaleformXMLStruct)
//			REFRESH_SCALEFORMXML_TABS_TOP_DETAIL(Placement.ScaleformXMLTabStruct)
//			Placement.bSetupTabs = TRUE
//			IF Placement.ScaleformXMLTabStruct.iHighlightTab = 0
//				Placement.ScaleformXMLTabStruct.iHighlightTab = 1
//			ELIF Placement.ScaleformXMLTabStruct.iHighlightTab = 1
//				Placement.ScaleformXMLTabStruct.iHighlightTab = 0
//			ENDIF
//			Placement.ScreenPlace.iSelectedCharacter = Placement.ScaleformXMLTabStruct.iHighlightTab
//			
//			REFRESH_SCALEFORMXML_TABS_HIGHLIGHT(Placement.ScaleformXMLTabStruct)
//			REFRESH_SCALEFORMXML_TABS_FACES(Placement.ScaleformXMLTabStruct)
//			
//			
//			
//		ENDIF
//	ENDIF

//	IF IS_BIT_SET(Placement.ButtonReleasedBitset, Right)
//		IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT) 
//			CLEAR_BIT(Placement.ButtonReleasedBitset, Right)
//			PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
//
//			REFRESH_SCALEFORMXML_INSTRUCTIONAL_BUTTONS( Placement.ScaleformXMLStruct)
//			REFRESH_SCALEFORMXML_TABS_TOP_DETAIL(Placement.ScaleformXMLTabStruct)
//			Placement.bSetupTabs = TRUE
//			IF Placement.ScaleformXMLTabStruct.iHighlightTab = 0
//				Placement.ScaleformXMLTabStruct.iHighlightTab = 1
//			ELIF Placement.ScaleformXMLTabStruct.iHighlightTab = 1
//				Placement.ScaleformXMLTabStruct.iHighlightTab = 0
//			ENDIF
//			Placement.ScreenPlace.iSelectedCharacter = Placement.ScaleformXMLTabStruct.iHighlightTab
//		
//			REFRESH_SCALEFORMXML_TABS_HIGHLIGHT(Placement.ScaleformXMLTabStruct)
//			REFRESH_SCALEFORMXML_TABS_FACES(Placement.ScaleformXMLTabStruct)
//		ENDIF
//	ENDIF
			
	

	IF IS_BIT_SET(Placement.ButtonReleasedBitset, ACCEPT)
//	OR (DOES_PLAYER_HAVE_ACTIVE_CHARACTER()	AND GET_CONFIRM_INVITE_INTO_GAME_STATE() AND IS_PAUSE_MENU_ACTIVE() = FALSE)
		NET_NL()NET_PRINT("LOGIC_MPHUD_XML_CHARACTER_SELECT_FM: IS_BIT_SET INPUT_FRONTEND_ACCEPT Placement.ScreenPlace.iSelectedCharacter = ")NET_PRINT_INT(Placement.ScreenPlace.iSelectedCharacter)
		IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
//		OR (DOES_PLAYER_HAVE_ACTIVE_CHARACTER()	AND GET_CONFIRM_INVITE_INTO_GAME_STATE() AND IS_PAUSE_MENU_ACTIVE() = FALSE)
		
//			STOP_AUDIO_SCENE("MP_MENU_SCENE")	
			
			
			
			IF HAS_SLOT_BEEN_IGNORED_SCRIPT(Placement.ScreenPlace.iSelectedCharacter) = FALSE 
			OR SCRIPT_IS_CLOUD_AVAILABLE() = FALSE
				
				PLAY_SOUND_FRONTEND(-1, "SELECT","HUD_FRONTEND_DEFAULT_SOUNDSET")
				
					// KGM 21/10/14: Some heist variables are semi-persistent as the player jumps around MP, but need cleared if the player goes to the character select screen
					PRINTLN(".KGM [ActSelect][Heist]: Request reset of semi-persistent Heist variables on MP rejoin - CG Character Select Screen")
					g_resetHeistsOnJoiningMP = TRUE
		
	//			PLAY_SOUND_FRONTEND(-1,"SELECT","HUD_FRONTEND_DEFAULT_SOUNDSET") //Derek plays sounds. 
				CLEAR_BIT(Placement.ButtonReleasedBitset, ACCEPT)
				NET_NL()NET_PRINT("LOGIC_MPHUD_XML_CHARACTER_SELECT_FM: INPUT_FRONTEND_ACCEPT with iSelectedCharacter = ")NET_PRINT_INT(Placement.ScreenPlace.iSelectedCharacter)
				UNLOAD_PEDHEADSHOT(Placement.ScaleformAlertScreenStruct.HeadshotPic)
				IF IS_STAT_CHARACTER_ACTIVE(Placement.ScreenPlace.iSelectedCharacter)
				
					IF Placement.ScreenPlace.iSelectedCharacter = -1
						Placement.ScreenPlace.iSelectedCharacter = Placement.ScaleformXMLTabStruct.iHighlightTab
						NET_NL()NET_PRINT("LOGIC_MPHUD_XML_CHARACTER_SELECT_FM: iSelectedCharacter = -1 so making it iHighlightTab = ")NET_PRINT_INT(Placement.ScreenPlace.iSelectedCharacter)
						IF Placement.ScreenPlace.iSelectedCharacter = -1	
							Placement.ScreenPlace.iSelectedCharacter = 0							
							NET_NL()NET_PRINT("LOGIC_MPHUD_XML_CHARACTER_SELECT_FM: iSelectedCharacter = -1 STILL so making it 0")
						ENDIF
					ENDIF
				
					
						
					
				
//					IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
//						IF IS_SRL_LOADED()
//							END_SRL()
//						ENDIF
//					ENDIF
					RUN_TRANSITION_MENU_AUDIO(FALSE)
					NETWORK_APPLY_PED_SCAR_DATA(PLAYER_PED_ID(), Placement.ScreenPlace.iSelectedCharacter)
				
					IF GET_ACTIVE_CHARACTER_SLOT() != Placement.ScreenPlace.iSelectedCharacter
						NET_NL()NET_PRINT("LOGIC_MPHUD_XML_CHARACTER_SELECT_FM: Player has swapped character ")
						STAT_CLEAR_PENDING_SAVES(-1)
						RESET_CASINO_HEIST_FLOW_DATA()	 		
						RESET_ISLAND_HEIST_FLOW()
						REMOVE_ALL_QUEUED_SAVES_DUE_TO_CHARACTER_SWAP()
						Placement.HasGamerSetupChanged = TRUE
						g_Private_LoadingNetworkShopINVENTORY.LoadingStages = 0
						SET_ACTIVE_CHARACTER_SLOT(Placement.ScreenPlace.iSelectedCharacter) 
						SET_CURRENT_MP_SLOT_FOR_CLOTHES(Placement.ScreenPlace.iSelectedCharacter)
						
						g_TransitionSessionNonResetVars.contactRequests.bResetContactReqTimeouts = TRUE						
						#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_MP_CONTACT_REQUESTS,	"[dsw] Swapping characters, will reset contact request timeouts")
						#ENDIF
//						REQUEST_SAVE(STAT_SAVETYPE_END_GAMER_SETUP)
					ENDIF
					ClearInsideDisplacedInteriorFlag()
					RUN_TRANSITION_SFX(FALSE)
					RUN_TRANSITION_SFX_MENU_SCENE(TRUE)
					SET_ENTERED_CHAR_CREATION_FROM_ALTERATION_PROMPT(FALSE)
					SET_CURRENT_MP_SLOT_FOR_CLOTHES(Placement.ScreenPlace.iSelectedCharacter)
//					ANIMPOSTFX_STOP_CROSSFADE()
					SET_QUICKJOIN_ACTIVE(TRUE)
					IF g_B_DisplayOnlineSplash = FALSE
						NET_NL()NET_PRINT("LOGIC_MPHUD_XML_CHARACTER_SELECT_FM: g_B_DisplayOnlineSplash = TRUE - Swap team ")
						g_B_DisplayOnlineSplash = TRUE
					ENDIF
					g_b_Private_Tshirt_is_new_character[Placement.ScreenPlace.iSelectedCharacter] = FALSE
					SET_JOINING_CHARACTER(Placement.ScreenPlace.iSelectedCharacter)
					IF IS_STAT_CHARACTER_ACTIVE(Placement.ScreenPlace.iSelectedCharacter)
						RUN_GOING_FORWARD_CHARACTER_SUMMARY(Placement)
					ENDIF
				ELSE
				
					IF NOT HAS_ENTERED_OFFLINE_SAVE_FM()
					AND IS_SAVING_HAVING_TROUBLE() = FALSE
						DELETE_SAVE_SLOT(Placement.ScreenPlace.iSelectedCharacter+1, TRUE, FALSE)
						NETWORK_CLEAR_CHARACTER_WALLET(Placement.ScreenPlace.iSelectedCharacter)
					ENDIF
					
					SET_NEWS_FEED_SPLASH_SCREEN_ACTIVE(FALSE)
				
					NET_NL()NET_PRINT("LOGIC_MPHUD_XML_CHARACTER_SELECT_FM: g_MPCharData.bIsCharNew[Placement.ScreenPlace.iSelectedCharacter] - Create Character. ")
				
					// Toggle the renderphases
					PRINTLN("LOGIC_MPHUD_XML_CHARACTER_SELECT_FM  - SET_SKYFREEZE_FROZEN()")
					SET_SKYFREEZE_FROZEN()
				
					SET_LOADING_ICON_INACTIVE()
					g_MPCharData.bIsCharNew[Placement.ScreenPlace.iSelectedCharacter] = TRUE
					SET_JOINING_CHARACTER(Placement.ScreenPlace.iSelectedCharacter)
					SET_CURRENT_MP_SLOT_FOR_CLOTHES(Placement.ScreenPlace.iSelectedCharacter)
					SET_ENTERED_CHAR_CREATION_FROM_ALTERATION_PROMPT(FALSE)
					RUN_SCALEFORM_LOCK_RIGHT_MOUSE_BUTTON(FALSE)	
					HUD_CHANGE_STATE(HUD_STATE_CREATE_FACE)
					IF SCRIPT_IS_CLOUD_AVAILABLE()
						TRANSITION_CHANGE_STATE(TRANSITION_STATE_CREATION_ENTER_SESSION)
					ENDIF
					SET_PLAYERS_SELECTED_CHARACTER_TEAM(TEAM_FREEMODE)
					SET_STAT_CHARACTER_TEAM(TEAM_FREEMODE, Placement.ScreenPlace.iSelectedCharacter)
					
				ENDIF
			ELSE
				PLAY_SOUND_FRONTEND(-1, "SELECT","HUD_FRONTEND_DEFAULT_SOUNDSET") 
				CLEAR_BIT(Placement.ButtonReleasedBitset, ACCEPT)

				DESTROY_ALL_MENUPEDS(Placement.IdlePed, Placement.SelectionPed)
				FORCE_RUN_RETRY_SLOT(Placement.ScreenPlace.iSelectedCharacter+1)
				
				
				
				
			ENDIF
		ENDIF
	ENDIF
	
	
	
	
	
//	IF IS_ROCKSTAR_DEV()
//		IF IS_STAT_CHARACTER_ACTIVE(Placement.ScreenPlace.iSelectedCharacter)
//			IF IS_BIT_SET(Placement.ButtonReleasedBitset, R3) 
//			AND IS_BIT_SET(Placement.ButtonReleasedBitset, L3) 
//
//				IF g_b_HasEnteredGameAsSpectator[Placement.ScreenPlace.iSelectedCharacter] = FALSE
//					g_b_HasEnteredGameAsSpectator[Placement.ScreenPlace.iSelectedCharacter] = TRUE
//					SET_QUICKJOIN_ACTIVE(TRUE)
//					SET_JOINING_CHARACTER(Placement.ScreenPlace.iSelectedCharacter)
//					RUN_GOING_FORWARD_CHARACTER_SUMMARY(Placement)
//					
//				ENDIF
//	//		
//				
//			ENDIF
//		ENDIF
//		IF IS_BIT_SET(Placement.ButtonReleasedBitset, R3) 
//			CLEAR_BIT(Placement.ButtonReleasedBitset, R3)
//		ENDIF
//		IF IS_BIT_SET(Placement.ButtonReleasedBitset, L3) 
//			CLEAR_BIT(Placement.ButtonReleasedBitset, L3)
//		ENDIF
//	ENDIF
	
	
	IF HAVE_ANY_SLOTS_FAILED()
		
		DESTROY_ALL_MENUPEDS(Placement.IdlePed, Placement.SelectionPed)
		RUN_SCALEFORM_LOCK_RIGHT_MOUSE_BUTTON(FALSE)
		TRANSITION_CHANGE_STATE(TRANSITION_STATE_WAIT_HUD_EXIT)
		HUD_CHANGE_STATE(HUD_STATE_STATS_LOAD_SLOT_FAIL)
	ENDIF



ENDPROC

PROC RUN_BUTTON_LOGIC(MPHUD_PLACEMENT_TOOLS& Placement)



	IF IS_STAT_CHARACTER_ACTIVE(Placement.ScreenPlace.iSelectedCharacter)
	AND HAS_ENTERED_OFFLINE_SAVE_FM() = FALSE
	AND IS_SAVING_HAVING_TROUBLE() = FALSE
		IF HAS_SLOT_BEEN_IGNORED_SCRIPT(Placement.ScreenPlace.iSelectedCharacter) 
			IF SCRIPT_IS_CLOUD_AVAILABLE()
				PAUSE_MENU_DEACTIVATE_CONTEXT( HASH("IS_SELECTED_PED"))
				PAUSE_MENU_ACTIVATE_CONTEXT(HASH("CAN_RETRY_LOAD"))
				PAUSE_MENU_ACTIVATE_CONTEXT( HASH("CAN_DELETE_PED"))
				PAUSE_MENU_DEACTIVATE_CONTEXT( HASH("CAN_EDIT_TAB"))
			ELSE
				PAUSE_MENU_ACTIVATE_CONTEXT( HASH("IS_SELECTED_PED"))
				PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("CAN_RETRY_LOAD"))
				PAUSE_MENU_DEACTIVATE_CONTEXT( HASH("CAN_DELETE_PED"))
				PAUSE_MENU_DEACTIVATE_CONTEXT( HASH("CAN_EDIT_TAB"))
			ENDIF
		ELSE
			PAUSE_MENU_ACTIVATE_CONTEXT( HASH("IS_SELECTED_PED"))
			PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("CAN_RETRY_LOAD"))
			PAUSE_MENU_ACTIVATE_CONTEXT( HASH("CAN_DELETE_PED"))
			PAUSE_MENU_ACTIVATE_CONTEXT( HASH("CAN_EDIT_TAB"))
		ENDIF
	ELSE
		IF HAS_SLOT_BEEN_IGNORED_SCRIPT(Placement.ScreenPlace.iSelectedCharacter) 
		AND SCRIPT_IS_CLOUD_AVAILABLE()
		AND HAS_ENTERED_OFFLINE_SAVE_FM() = FALSE
		AND IS_SAVING_HAVING_TROUBLE() = FALSE
			PAUSE_MENU_DEACTIVATE_CONTEXT( HASH("IS_SELECTED_PED"))
			PAUSE_MENU_ACTIVATE_CONTEXT(HASH("CAN_RETRY_LOAD"))
			PAUSE_MENU_ACTIVATE_CONTEXT( HASH("CAN_DELETE_PED"))
			PAUSE_MENU_DEACTIVATE_CONTEXT( HASH("CAN_EDIT_TAB"))
		ELSE
			PAUSE_MENU_ACTIVATE_CONTEXT( HASH("IS_SELECTED_PED"))
			PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("CAN_RETRY_LOAD"))
			PAUSE_MENU_DEACTIVATE_CONTEXT( HASH("CAN_DELETE_PED"))
			PAUSE_MENU_DEACTIVATE_CONTEXT( HASH("CAN_EDIT_TAB"))
		ENDIF
	ENDIF
	PAUSE_MENU_REDRAW_INSTRUCTIONAL_BUTTONS() 
				


ENDPROC

FUNC FRONTEND_MENU_SCREEN GET_CONTROL_SCREEN_FROM_HIGHLIGHT(INT TabHighlight)

	FRONTEND_MENU_SCREEN Result = MENU_UNIQUE_ID_MP_CHAR_1
	SWITCH TabHighlight
		CASE 0 
			Result = MENU_UNIQUE_ID_MP_CHAR_1
		
		BREAK
		CASE 1 
			Result = MENU_UNIQUE_ID_MP_CHAR_2 
		
		BREAK
		CASE 2 
		
			Result = MENU_UNIQUE_ID_MP_CHAR_3
		BREAK
		CASE 3 
			Result = MENU_UNIQUE_ID_MP_CHAR_4
		
		BREAK
		CASE 4 
			Result = MENU_UNIQUE_ID_MP_CHAR_5
		
		BREAK

	
	ENDSWITCH
	RETURN Result

ENDFUNC


FUNC INT GET_HIGHLIGHT_FROM_CONTROL_SCREEN(FRONTEND_MENU_SCREEN TabHighlight)

	INT Result = 0
	SWITCH TabHighlight
		CASE MENU_UNIQUE_ID_MP_CHAR_1 
			Result = 0
		
		BREAK
		CASE MENU_UNIQUE_ID_MP_CHAR_2 
			Result =  1
		
		BREAK
		CASE MENU_UNIQUE_ID_MP_CHAR_3 
		
			Result = 2
		BREAK
		CASE MENU_UNIQUE_ID_MP_CHAR_4 
			Result = 3
		
		BREAK
		CASE MENU_UNIQUE_ID_MP_CHAR_5 
			Result = 4
		
		BREAK

	
	ENDSWITCH
	RETURN Result

ENDFUNC


PROC PROCESS_XML_MPHUD_CHARACTER_SELECT_FM(MPHUD_PLACEMENT_TOOLS& Placement)

	Placement.ScreenPlace.iSelectedCharacter = Placement.ScreenPlace.iSelectedCharacter
	
//	FORCE_SCRIPTED_GFX_WHEN_FRONTEND_ACTIVE(TRUE)
//	SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
//	
//	
//	IF GET_PAUSE_MENU_STATE() = PM_READY
//		IF HAS_NET_TIMER_EXPIRED(Placement.RemoveLoadingScreenTimer, 500)
//		IF IS_SCREEN_FADED_OUT()
//		OR IS_SCREEN_FADING_OUT()
//			DO_SCREEN_FADE_IN(0)
//		ENDIF
//		ENDIF
//	ENDIF
//	
//	IF IS_LOADING_ICON_ACTIVE()
//		SET_LOADING_ICON_INACTIVE()
//	ENDIF
//	
	
		
//	IF SCRIPT_IS_CLOUD_AVAILABLE() = FALSE
//	AND STAT_SAVE_PENDING_OR_REQUESTED() = TRUE
//		NET_NL()NET_PRINT("PROCESS_XML_MPHUD_CHARACTER_SELECT_FM - SCRIPT_IS_CLOUD_AVAILABLE() = FALSE & STAT_SAVE_PENDING_OR_REQUESTED = TRUE ")
//		SET_JOINING_GAMEMODE(GAMEMODE_SP)
//		RUN_SCALEFORM_LOCK_RIGHT_MOUSE_BUTTON(FALSE)	
//		TRANSITION_CHANGE_STATE(TRANSITION_STATE_TERMINATE_SESSION)
//		HUD_CHANGE_STATE(HUD_STATE_CLOUD_FAIL)
//
//		EXIT
//	ENDIF
//	
//	IF STAT_SAVE_PENDING_OR_REQUESTED() 
//		NET_NL()NET_PRINT("PROCESS_XML_MPHUD_CHARACTER_SELECT_FM - STAT_SAVE_PENDING_OR_REQUESTED = TRUE - hold. ")
//		REMOVE_ALL_QUEUED_SAVES_DUE_TO_CHARACTER_SWAP()
//		EXIT
//	ENDIF
//	
//
//	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//		SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)
//	ENDIF
//	
//	IF NOT IS_PED_INJURED(PlayerSwitchLeftBehindPed)
//		SET_ENTITY_VISIBLE(PlayerSwitchLeftBehindPed, FALSE)
//	ENDIF
	
//	IF Placement.aBool[3] = FALSE 
//		BOOL aCloudCheckBool
//		IF RUN_JUNK_DATA_CHECK_AND_LOCAL_CLEAR(Placement, aCloudCheckBool)
//			IF aCloudCheckBool
//				NET_NL()NET_PRINT("PROCESS_XML_MPHUD_CHARACTER_SELECT_FM: ******** MoveToCloudFail = TRUE ******** ")
//				SET_JOINING_GAMEMODE(GAMEMODE_SP)
//				IF GET_CONFIRM_INVITE_INTO_GAME_STATE()
//					IGNORE_INVITE()
//				ENDIF
//				SET_CURRENT_TRANSITION_BAILING_OPTIONS(BAILING_OPTIONS_JUNKCHECK) 
//				SET_NEWS_FEED_SPLASH_SCREEN_ACTIVE(FALSE)
//				CANCEL_MP_JOIN(TRUE)
//				SET_CONFIRM_INVITE_INTO_GAME_STATE(FALSE)
//				SET_NEED_TO_JOIN_SESSION_FROM_INVITE(FALSE)
//				SET_JUNK_CHECK_BAILING_TRANSITION(TRUE)
//				RUN_SCALEFORM_LOCK_RIGHT_MOUSE_BUTTON(FALSE)	
//				TRANSITION_CHANGE_STATE(TRANSITION_STATE_TERMINATE_SESSION)
//				HUD_CHANGE_STATE(HUD_STATE_GENERAL_ERROR)
//				EXIT
//			ENDIF
//			Placement.aBool[3] = TRUE
//		ELSE
//			NET_NL()NET_PRINT("PROCESS_XML_MPHUD_CHARACTER_SELECT_FM: RUN_JUNK_DATA_CHECK_AND_LOCAL_CLEAR = FALSE ")
//			EXIT
//		ENDIF
//			ENDIF
//	DRAW_BACKGROUND_BIRDS()
//
////	NET_NL()NET_PRINT("PROCESS_XML_MPHUD_CHARACTER_SELECT_FM: SHUTDOWN_LOADING_SCREEN Called ")
//		
//		
//	IF GET_PAUSE_MENU_STATE() = PM_READY
//	AND g_AreBirdsDisplaying
//		FULLSCREEN_BIRDS_BACKGROUND()
//			SHUTDOWN_LOADING_SCREEN()
//		NET_NL()NET_PRINT("CHAR SELECT : SHUTDOWN_LOADING_SCREEN() ")
////		IF IS_SCREEN_FADED_OUT()
////		OR IS_SCREEN_FADING_OUT()
////			DO_SCREEN_FADE_IN(0)
////		ENDIF
//	ENDIF
			
//	IF LOBBY_AUTO_MULTIPLAYER_FREEMODE()
//		LOBBY_SET_AUTO_MULTIPLAYER(FALSE)
//		NET_NL()NET_PRINT("CHAR SELECT :LOBBY_AUTO_MULTIPLAYER_FREEMODE = TRUE so calling LOBBY_SET_AUTO_MULTIPLAYER(FALSE) ")
//	ENDIF
//	
//	IF LOBBY_AUTO_MULTIPLAYER_FREEMODE()
//		FULLSCREEN_BIRDS_BACKGROUND()
//	ENDIF
//	
//	
//	IF RUN_BIRDS_ALPHA() = FALSE
//		EXIT
//	ENDIF
//
//	FULLSCREEN_BIRDS_BACKGROUND()
//
//	IF NOT IS_PAUSE_MENU_RESTARTING()
//	//AND IS_SCREEN_FADED_IN()
//	AND GET_PAUSE_MENU_STATE() != PM_RESTARTING
//	AND GET_PAUSE_MENU_STATE() != PM_STARTING_UP
//	AND GET_PAUSE_MENU_STATE() != PM_SHUTTING_DOWN
//	
//	//AND ((LOBBY_AUTO_MULTIPLAYER_FREEMODE() AND HAS_NET_TIMER_EXPIRED(Placement.RemoveLoadingScreenTimer, 0))
//	//OR LOBBY_AUTO_MULTIPLAYER_FREEMODE() = FALSE)// AND Placement.bArePedsSetup)
//	
//	
//		IF Placement.bHudScreenInitialised = FALSE
//		
//			SETTING_SAVE_TRANSFER_STATS(Placement)
//	
//			SET_NEWS_FEED_SPLASH_SCREEN_ACTIVE(FALSE)
//	
//			SET_BEEN_IN_GTAO_THIS_BOOT(TRUE)
//	
//			RUN_TRANSITION_MENU_AUDIO(TRUE)
//	
//			SET_AUDIO_FLAG("LoadMPData", TRUE) 
//	
//			Placement.ScaleformXMLTabStruct.PlayerData.GangTextureName = ""
//			GAMER_HANDLE aGamer = GET_GAMER_HANDLE_PLAYER(PLAYER_ID())
//			NETWORK_CLAN_GET_EMBLEM_TXD_NAME(aGamer, Placement.ScaleformXMLTabStruct.PlayerData.GangTextureName) 
//
////			IF IS_PLAYER_SWITCH_IN_PROGRESS()
////				STOP_PLAYER_SWITCH()
////				SET_SKYSWOOP_STAGE(SKYSWOOP_NONE)
////				NET_NL()NET_PRINT("DO_TUTORIAL_PRESTREAMING CHAR SELECT : STOP_PLAYER_SWITCH called ")
////			ENDIF
////		
////		
//			IF IS_SRL_LOADED()
//				END_SRL()
//				NET_NL()NET_PRINT("DO_TUTORIAL_PRESTREAMING CHAR SELECT : IS_SRL_LOADED = TRUE calling END_SRL ")
//			ENDIF
//
//
//			RUN_TRANSITION_SFX_MENU_SCENE(FALSE)
//
//			
//			
//			NET_NL()NET_PRINT(" <<< HAS_SLOT_BEEN_IGNORED_SCRIPT(0) = ")NET_PRINT_BOOL(HAS_SLOT_BEEN_IGNORED_SCRIPT(0))NET_PRINT(" >>>")
//			NET_NL()NET_PRINT(" <<< HAS_SLOT_BEEN_IGNORED_SCRIPT(1) = ")NET_PRINT_BOOL(HAS_SLOT_BEEN_IGNORED_SCRIPT(1))NET_PRINT(" >>>")
//			NET_NL()NET_PRINT(" <<< HAS_SLOT_BEEN_IGNORED_SCRIPT(2) = ")NET_PRINT_BOOL(HAS_SLOT_BEEN_IGNORED_SCRIPT(2))NET_PRINT(" >>>")
//			NET_NL()NET_PRINT(" <<< HAS_SLOT_BEEN_IGNORED_SCRIPT(3) = ")NET_PRINT_BOOL(HAS_SLOT_BEEN_IGNORED_SCRIPT(3))NET_PRINT(" >>>")
//			NET_NL()NET_PRINT(" <<< HAS_SLOT_BEEN_IGNORED_SCRIPT(4) = ")NET_PRINT_BOOL(HAS_SLOT_BEEN_IGNORED_SCRIPT(4))NET_PRINT(" >>>")
//
//			NET_NL()NET_PRINT(" <<< IS_STAT_CHARACTER_ACTIVE(0) = ")NET_PRINT_BOOL(IS_STAT_CHARACTER_ACTIVE(0))NET_PRINT(" >>>")
//			NET_NL()NET_PRINT(" <<< IS_STAT_CHARACTER_ACTIVE(1) = ")NET_PRINT_BOOL(IS_STAT_CHARACTER_ACTIVE(1))NET_PRINT(" >>>")
//			NET_NL()NET_PRINT(" <<< IS_STAT_CHARACTER_ACTIVE(2) = ")NET_PRINT_BOOL(IS_STAT_CHARACTER_ACTIVE(2))NET_PRINT(" >>>")
//			NET_NL()NET_PRINT(" <<< IS_STAT_CHARACTER_ACTIVE(3) = ")NET_PRINT_BOOL(IS_STAT_CHARACTER_ACTIVE(3))NET_PRINT(" >>>")
//			NET_NL()NET_PRINT(" <<< IS_STAT_CHARACTER_ACTIVE(4) = ")NET_PRINT_BOOL(IS_STAT_CHARACTER_ACTIVE(4))NET_PRINT(" >>>")
//
//			NET_NL()NET_PRINT(" <<< GET_PAUSE_MENU_STATE() = ")NET_PRINT_INT(ENUM_TO_INT(GET_PAUSE_MENU_STATE()))NET_PRINT(" >>>")
//
//
//
////			NET_NL()NET_PRINT("PROCESS_XML_MPHUD_CHARACTER_SELECT_FM: SHUTDOWN_LOADING_SCREEN")NET_NL()
////			SHUTDOWN_LOADING_SCREEN()
//			
////				INIT_MPHUD_CHARACTER_SELECT_FM(Placement)
//			Placement.ScreenPlace.bTurnOnInfo = TRUE		
//			Placement.bHudScreenInitialised = TRUE
//			
//			IF Placement.PreviousScreen != HUD_STATE_DELETE_CHARACTER_PROMPT
//				CLEAR_XML_ALLSLOTS_PEDSHOTS()
//				
//			ENDIF
//			
//			
//			ALLOW_EXPENSIVE_UPDATE_ALL_HEAVILY_ACCESSED_STATS()
//			EXPENSIVE_UPDATE_ALL_HEAVILY_ACCESSED_STATS()
//			
//			
//			
//			IF Placement.PreviousScreen = HUD_STATE_MODE_SELECTION
//			OR Placement.PreviousScreen = HUD_STATE_PRE_HUD_CHECKS
//			OR Placement.PreviousScreen = HUD_STATE_LOADING
//				Placement.ScreenPlace.iSelectedCharacter = GET_LAST_OR_VALID_SLOT(Placement.ScreenPlace.iSelectedCharacter, GAMEMODE_EMPTY, FALSE)
//			ENDIF
//			
//			IF (HAS_SLOT_BEEN_IGNORED_CODE(1)
//			AND HAS_SLOT_BEEN_IGNORED_CODE(2)) = FALSE
//				
//				IF HAS_SLOT_BEEN_IGNORED_CODE(Placement.ScreenPlace.iSelectedCharacter+1)
//					IF Placement.ScreenPlace.iSelectedCharacter = 0
//						Placement.ScreenPlace.iSelectedCharacter = 1
//					ELSE
//						Placement.ScreenPlace.iSelectedCharacter = 0
//					ENDIF
//				ENDIF	
//			ENDIF
//			
//			Placement.ScaleformXMLTabStruct.iHighlightTab = Placement.ScreenPlace.iSelectedCharacter
//
//			NET_NL()NET_PRINT("PROCESS_XML_MPHUD_CHARACTER_SELECT_FM: Placement.ScreenPlace.iSelectedCharacter = ")NET_PRINT_INT(Placement.ScreenPlace.iSelectedCharacter)
////			ANIMPOSTFX_START_CROSSFADE("DefaultMPScreenFade", "SwitchOpenNeutral", 300)
//
//			IF Placement.PreviousScreen != HUD_STATE_DELETE_CHARACTER_PROMPT	
//				Placement.bSetupTabs = FALSE
//				IF GET_PAUSE_MENU_STATE() = PM_READY			
//					IF IS_THIS_SCREEN_ACTIVE(FE_MENU_VERSION_MP_CHARACTER_SELECT) = FALSE
//						NET_NL()NET_PRINT("PROCESS_XML_MPHUD_CHARACTER_SELECT_FM: RESTART_FRONTEND_MENU")
//						RESTART_FRONTEND_MENU(FE_MENU_VERSION_MP_CHARACTER_SELECT, GET_CHAR_SELECT_TAB_INDEX(Placement.ScreenPlace.iSelectedCharacter))
//						Placement.bSetupTabs = TRUE
//					ENDIF
//				ELSE
//					NET_NL()NET_PRINT("PROCESS_XML_MPHUD_CHARACTER_SELECT_FM: ACTIVATE_FRONTEND_MENU")
//					ACTIVATE_FRONTEND_MENU(FE_MENU_VERSION_MP_CHARACTER_SELECT, FALSE, GET_CHAR_SELECT_TAB_INDEX(Placement.ScreenPlace.iSelectedCharacter))
//				ENDIF
//				SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_FADE)
//			ENDIF
//			
//	
//			NET_NL()NET_PRINT("PROCESS_XML_MPHUD_CHARACTER_SELECT_FM: Placement.ScaleformXMLTabStruct.iHighlightTab = ")NET_PRINT_INT(Placement.ScaleformXMLTabStruct.iHighlightTab)
//			Placement.bSetupTabs = TRUE	
//				
//			Placement.iButtonPressedRun = FALSE
//			
//			INT I
//			FOR I = 0 TO MAX_NUM_CHARACTER_SLOTS-1	
//			
//
//				IF IS_STAT_CHARACTER_ACTIVE(I)
//					Placement.iTeam[I] = GET_STAT_CHARACTER_TEAM(i)
//				ELSE
//					Placement.iTeam[I] = -1
//				ENDIF
//			ENDFOR	
//			
//			
//			IF Placement.PreviousScreen = HUD_STATE_SELECT_CHARACTER_FM
//			OR Placement.PreviousScreen = HUD_STATE_PRE_HUD_CHECKS
//				g_iMenuSelection[HUDMENU_TEAM_SELECTION_FM] = TEAM_FREEMODE
//			ENDIF	
//			
////			REFRESH_SCALEFORMXML_TABS_FACES(Placement.ScaleformXMLTabStruct)
//			REFRESH_SCALEFORMXML_TABS_EMBLEM(Placement.ScaleformXMLTabStruct)
//			REFRESH_SCALEFORMXML_TABS_NAME(Placement.ScaleformXMLTabStruct)
//			REFRESH_SCALEFORMXML_TABS_TOP_DETAIL(Placement.ScaleformXMLTabStruct)
//			REFRESH_SCALEFORMXML_TABS_DISPLAY(Placement.ScaleformXMLTabStruct)
////			REFRESH_SCALEFORM_ALERTSCREEN(Placement.ScaleformAlertScreenStruct)
//			REFRESH_SCALEFORMXML_CHAR_SELECT(Placement.ScaleformXMLCharSelectStruct)
//			
////				SET_SCALEFORMXML_TAB_HIGHLIGHT(Placement.ScreenPlace.iSelectedCharacter, Placement.ScaleformXMLTabStruct)	
//
//
//
//			
//
//			DELETE_ALL_INACTIVE_MENUPED(Placement.SelectionPed, Placement.IdlePed)
//			
//			Placement.ScaleformXMLTabStruct.iHighlightTab = Placement.ScreenPlace.iSelectedCharacter
//			
//			SET_CHAR_SELECT_AS_BUSY(Placement)
//			Placement.bHudScreenInitialised = TRUE
//
//		ENDIF
//	ELSE
//		NET_NL()NET_PRINT("PROCESS_XML_MPHUD_CHARACTER_SELECT_FM: GET_PAUSE_MENU_STATE = ")NET_PRINT_INT(ENUM_TO_INT(GET_PAUSE_MENU_STATE()))
//	ENDIF
			
//	IF IS_PAUSE_MENU_ACTIVE()
//	AND NOT IS_PAUSE_MENU_RESTARTING()
//	AND HAS_MENU_LAYOUT_CHANGED_EVENT_OCCURRED()
//	
//		TAKE_CONTROL_OF_FRONTEND()
//		
//		RUN_SCALEFORM_LOCK_RIGHT_MOUSE_BUTTON(FALSE)
//		
//		IF Placement.aBool[2] = FALSE
//			REFRESH_SCALEFORMXML_TABS_FACES(Placement.ScaleformXMLTabStruct)
//			RUN_SCALEFORMXML_TABS_FACE(Placement.ScaleformXMLTabStruct,Placement.ScaleformAlertScreenStruct, SHOULD_REFRESH_SCALEFORMXML_TABS_FACES(Placement.ScaleformXMLTabStruct), FALSE, TRUE)
//			IF Placement.ScaleformXMLTabStruct.InitialiseSCALEFORMXMLFaces = TRUE
//				Placement.aBool[2] = TRUE
//							REFRESH_SCALEFORMXML_TABS_FACES(Placement.ScaleformXMLTabStruct)
//			ENDIF
//		ENDIF
//		
//		IF Placement.aBool[1] = FALSE
//			INT I
//			FOR I = 0 TO MAX_NUM_CHARACTER_SLOTS-1
//				PRIVATE_ADD_SCALEFORMXML_CHAR_SELECT_CHAR_IN_SLOT(I, (IS_STAT_CHARACTER_ACTIVE(I) AND HAS_SLOT_BEEN_IGNORED_CODE(I+1) = FALSE), Placement.ScaleformXMLCharSelectStruct)
//			ENDFOR
//			PRIVATE_ADD_SCALEFORMXML_CHAR_SELECT_CURRENT_CHAR(Placement.ScreenPlace.iSelectedCharacter, Placement.ScaleformXMLCharSelectStruct)
//			RUN_SCALEFORMXML_CHAR_SELECTS(Placement.ScaleformXMLCharSelectStruct, SHOULD_REFRESH_SCALEFORMXML_CHAR_SELECT(Placement.ScaleformXMLCharSelectStruct))
//			RUN_SCALEFORMXML_CHAR_SELECTS_EACH_TAB_CHANGE(Placement.ScaleformXMLCharSelectStruct, SHOULD_REFRESH_SCALEFORMXML_CHAR_SELECT_EACH_TAB_CHANGE(Placement.ScaleformXMLCharSelectStruct))
//			IF Placement.ScaleformXMLCharSelectStruct.bInitialisedVisuals = TRUE
//				Placement.aBool[1] = TRUE
//				SET_CHAR_SELECT_AS_BUSY(Placement)
//				REFRESH_SCALEFORMXML_CHAR_SELECT(Placement.ScaleformXMLCharSelectStruct)
//			ENDIF
//			Placement.ScaleformXMLTabStruct.iHighlightTab = Placement.ScreenPlace.iSelectedCharacter
//
//		ENDIF
//		
//	ENDIF
						
				
				

				
//	IF Placement.bHudScreenInitialised = TRUE
//
//		IF Placement.bSetupAudio = FALSE
//			IF HAS_NET_TIMER_EXPIRED(Placement.aAudioTimer, 500)
//				RUN_TRANSITION_SFX(TRUE)
//				 Placement.bSetupAudio = TRUE
//			ENDIF
//		ENDIF
//	
//		IF IS_PAUSE_MENU_ACTIVE()
//		AND NOT IS_PAUSE_MENU_RESTARTING()
//			IF IS_FRONTEND_READY_FOR_CONTROL()
//			AND Placement.bArePedsSetup
//				
//				
//				IF Placement.bturnOnScreen = FALSE
//				
//
//				
//					NET_NL()NET_PRINT("PROCESS_XML_MPHUD_CHARACTER_SELECT_FM: MPLOBBY_DISPLAY_DATA_SLOT")
//					SET_CHAR_SELECT_AS_BUSY(Placement)
//					REFRESH_SCALEFORMXML_CHAR_SELECT(Placement.ScaleformXMLCharSelectStruct)
//					MPLOBBY_DISPLAY_DATA_SLOT(Placement.ScreenPlace.iSelectedCharacter)
//					REFRESH_SCALEFORMXML_TABS_NAME(Placement.ScaleformXMLTabStruct)
//					REFRESH_SCALEFORMXML_TABS_TOP_DETAIL(Placement.ScaleformXMLTabStruct)
//					
//					SET_FRONTEND_TAB_LOCKED(0, FALSE)
//					SET_FRONTEND_TAB_LOCKED(1, FALSE)
//					SET_FRONTEND_TAB_LOCKED(2, TRUE)
//					SET_FRONTEND_TAB_LOCKED(3, TRUE)
//					SET_FRONTEND_TAB_LOCKED(4, TRUE)
//					
//					RUN_SCALEFORM_LOCK_RIGHT_MOUSE_BUTTON(TRUE)
//
//
//					RUN_BUTTON_LOGIC(Placement)
//				
//					TAKE_CONTROL_OF_FRONTEND()
//					
//					RUN_SCALEFORM_LOCK_RIGHT_MOUSE_BUTTON(FALSE)
//					
//					
//					Placement.bturnOnScreen = TRUE
//				ENDIF
//		
//				IF HAS_MENU_TRIGGER_EVENT_OCCURRED()
//				
//					
//				
//					NET_NL()NET_PRINT("HAS_MENU_TRIGGER_EVENT_OCCURRED = TRUE")
//					GET_MENU_TRIGGER_EVENT_DETAILS(Placement.aNextScreen, Placement.iMenu)
//					NET_NL()NET_PRINT("		- aNextScreen = ")NET_PRINT(GET_XML_SCREEN_STRING_VALUE(Placement.aNextScreen))
//					NET_NL()NET_PRINT("		- iMenu = ")NET_PRINT_INT(Placement.iMenu)
//					
//					
//				ENDIF
//				
//
//				
//			
//				IF HAS_MENU_LAYOUT_CHANGED_EVENT_OCCURRED()
//				OR Placement.bSetupTabs = TRUE
//														
//					
//					REFRESH_SCALEFORMXML_CHAR_SELECT_EACH_TAB_CHANGE(Placement.ScaleformXMLCharSelectStruct)
//					
//					SET_CHAR_SELECT_AS_BUSY(Placement)
//					NET_NL()NET_PRINT("HAS_MENU_LAYOUT_CHANGED_EVENT_OCCURRED = TRUE")
//					GET_MENU_LAYOUT_CHANGED_EVENT_DETAILS(Placement.CurrentScreen, Placement.aNextScreen, Placement.iMenu)
//					NET_NL()NET_PRINT("		- CurrentScreen = ")NET_PRINT(GET_XML_SCREEN_STRING_VALUE(Placement.CurrentScreen))
//					NET_NL()NET_PRINT("		- aNextScreen = ")NET_PRINT(GET_XML_SCREEN_STRING_VALUE(Placement.aNextScreen))
//					NET_NL()NET_PRINT("		- iMenu = ")NET_PRINT_INT(Placement.iMenu)
//					
//					
//					
//					IF g_bSkipFaceIconReset = FALSE
//						IF (IS_STAT_CHARACTER_ACTIVE(0) OR IS_STAT_CHARACTER_ACTIVE(1))
//						
////							REFRESH_SCALEFORMXML_TABS_FACES(Placement.ScaleformXMLTabStruct)
//						ENDIF
//					ENDIF
//					
//					g_bSkipFaceIconReset = FALSE
//						
//				ENDIF
//				
//				
//
//				
////				IF CODE_WANTS_SCRIPT_TO_TAKE_CONTROL ()
////				OR Placement.bSetupTabs = TRUE
//				IF Placement.bSetupTabs = TRUE
//				
////					IF CODE_WANTS_SCRIPT_TO_TAKE_CONTROL ()
////						NET_NL()NET_PRINT("CODE_WANTS_SCRIPT_TO_TAKE_CONTROL = TRUE")
////					ENDIF
//
//					IF Placement.bSetupTabs = TRUE
//						NET_NL()NET_PRINT("Placement.bSetupTabs = TRUE. ")
//
//					ENDIF
//					
//					Placement.aBool[0] = TRUE
//					
//					IF Placement.bSetupTabs = FALSE
////						Placement.ControlScreen = GET_SCREEN_CODE_WANTS_SCRIPT_TO_CONTROL()
//						Placement.ControlScreen = Placement.aNextScreen
//
//					ENDIF
//
//					Placement.ScaleformXMLTabStruct.iHighlightTab = Placement.ScreenPlace.iSelectedCharacter
//					
//					NET_NL()NET_PRINT("		- Placement.ScaleformXMLTabStruct.iHighlightTab =  ")NET_PRINT_INT(Placement.ScaleformXMLTabStruct.iHighlightTab)NET_NL()
//					NET_NL()NET_PRINT("		- Placement.ScreenPlace.iSelectedCharacter =  ")NET_PRINT_INT(Placement.ScreenPlace.iSelectedCharacter)NET_NL()
//
//					Placement.ControlScreen = GET_CONTROL_SCREEN_FROM_HIGHLIGHT(Placement.ScaleformXMLTabStruct.iHighlightTab)
//					
//					
//					NET_NL()NET_PRINT("		-Screen to take control = ")NET_PRINT(GET_XML_SCREEN_STRING_VALUE(Placement.ControlScreen))
////					TAKE_CONTROL_OF_FRONTEND()
//					
//					
//					
//					SWITCH Placement.ControlScreen
//
//						
//						CASE MENU_UNIQUE_ID_MP_CHAR_1
//						
//							
//						
//							Placement.ScreenPlace.iSelectedCharacter = 0
//							Placement.ScaleformXMLTabStruct.iHighlightTab = Placement.ScreenPlace.iSelectedCharacter
//							
//							SET_CHAR_SELECT_AS_BUSY(Placement)
//
//							NET_NL()NET_PRINT("Take Control of MENU_UNIQUE_ID_MP_CHAR_1 = TRUE")
//							
////							UNLOAD_PEDHEADSHOT(Placement.ScaleformAlertScreenStruct.HeadshotPic)
////							REGISTER_PEDHEADSHOT_TAB(Placement.IdlePed[Placement.ScreenPlace.iSelectedCharacter] , Placement.ScaleformAlertScreenStruct.sAlertTextureName, Placement.ScaleformAlertScreenStruct.HeadshotPic)	
//							Placement.bRunHeadshotTimer = TRUE
//							RESET_NET_TIMER(Placement.HeadshotTimer)
//
//
//							RUN_BUTTON_LOGIC(Placement)
////							IF IS_ROCKSTAR_DEV()
////								PAUSE_MENU_ACTIVATE_CONTEXT( HASH("IS_RSTAR_DEV"))	
////							ELSE
////								PAUSE_MENU_DEACTIVATE_CONTEXT( HASH("IS_RSTAR_DEV"))	
////							ENDIF
////							PAUSE_MENU_REDRAW_INSTRUCTIONAL_BUTTONS() 
//
//						BREAK
//						
//						CASE MENU_UNIQUE_ID_MP_CHAR_2
//							Placement.ScreenPlace.iSelectedCharacter = 1
//							Placement.ScaleformXMLTabStruct.iHighlightTab = Placement.ScreenPlace.iSelectedCharacter
//							
//							NET_NL()NET_PRINT("Take Control of MENU_UNIQUE_ID_MP_CHAR_2 = TRUE")
//
//							SET_CHAR_SELECT_AS_BUSY(Placement)
////							UNLOAD_PEDHEADSHOT(Placement.ScaleformAlertScreenStruct.HeadshotPic)
////							REGISTER_PEDHEADSHOT_TAB(Placement.IdlePed[Placement.ScreenPlace.iSelectedCharacter] , Placement.ScaleformAlertScreenStruct.sAlertTextureName, Placement.ScaleformAlertScreenStruct.HeadshotPic)	
//							Placement.bRunHeadshotTimer = TRUE
//							RESET_NET_TIMER(Placement.HeadshotTimer)
//
//							RUN_BUTTON_LOGIC(Placement)
////							IF IS_ROCKSTAR_DEV()
////								PAUSE_MENU_ACTIVATE_CONTEXT( HASH("IS_RSTAR_DEV"))	
////							ELSE
////								PAUSE_MENU_DEACTIVATE_CONTEXT( HASH("IS_RSTAR_DEV"))	
////							ENDIF
////							PAUSE_MENU_REDRAW_INSTRUCTIONAL_BUTTONS() 
//							
//						BREAK
//						
//						CASE MENU_UNIQUE_ID_MP_CHAR_3
//							
//							Placement.ScreenPlace.iSelectedCharacter = 2
//							Placement.ScaleformXMLTabStruct.iHighlightTab = Placement.ScreenPlace.iSelectedCharacter							
//							NET_NL()NET_PRINT("Take Control of MENU_UNIQUE_ID_MP_CHAR_3 = TRUE")
//							
//
//							SET_CHAR_SELECT_AS_BUSY(Placement)
////							UNLOAD_PEDHEADSHOT(Placement.ScaleformAlertScreenStruct.HeadshotPic)
////							REGISTER_PEDHEADSHOT_TAB(Placement.IdlePed[Placement.ScreenPlace.iSelectedCharacter] , Placement.ScaleformAlertScreenStruct.sAlertTextureName, Placement.ScaleformAlertScreenStruct.HeadshotPic)	
//							Placement.bRunHeadshotTimer = TRUE
//							RESET_NET_TIMER(Placement.HeadshotTimer)
//		
//							RUN_BUTTON_LOGIC(Placement)
////							IF IS_ROCKSTAR_DEV()
////								PAUSE_MENU_ACTIVATE_CONTEXT( HASH("IS_RSTAR_DEV"))	
////							ELSE
////								PAUSE_MENU_DEACTIVATE_CONTEXT( HASH("IS_RSTAR_DEV"))	
////							ENDIF
////							PAUSE_MENU_REDRAW_INSTRUCTIONAL_BUTTONS() 
//							
//						BREAK
//						
//						CASE MENU_UNIQUE_ID_MP_CHAR_4
//							Placement.ScreenPlace.iSelectedCharacter = 3
//							Placement.ScaleformXMLTabStruct.iHighlightTab = Placement.ScreenPlace.iSelectedCharacter							
//							NET_NL()NET_PRINT("Take Control of MENU_UNIQUE_ID_MP_CHAR_4 = TRUE")
//							SET_CHAR_SELECT_AS_BUSY(Placement)
////							UNLOAD_PEDHEADSHOT(Placement.ScaleformAlertScreenStruct.HeadshotPic)
////							REGISTER_PEDHEADSHOT_TAB(Placement.IdlePed[Placement.ScreenPlace.iSelectedCharacter] , Placement.ScaleformAlertScreenStruct.sAlertTextureName, Placement.ScaleformAlertScreenStruct.HeadshotPic)	
//							Placement.bRunHeadshotTimer = TRUE
//							RESET_NET_TIMER(Placement.HeadshotTimer)
//
//							RUN_BUTTON_LOGIC(Placement)
////							IF IS_ROCKSTAR_DEV()
////								PAUSE_MENU_ACTIVATE_CONTEXT( HASH("IS_RSTAR_DEV"))	
////							ELSE
////								PAUSE_MENU_DEACTIVATE_CONTEXT( HASH("IS_RSTAR_DEV"))	
////							ENDIF
////							PAUSE_MENU_REDRAW_INSTRUCTIONAL_BUTTONS() 
//						BREAK
//						
//						CASE MENU_UNIQUE_ID_MP_CHAR_5
//							Placement.ScreenPlace.iSelectedCharacter = 4
//							Placement.ScaleformXMLTabStruct.iHighlightTab = Placement.ScreenPlace.iSelectedCharacter
//							NET_NL()NET_PRINT("Take Control of MENU_UNIQUE_ID_MP_CHAR_5 = TRUE")
//							
//
//							SET_CHAR_SELECT_AS_BUSY(Placement)
////							UNLOAD_PEDHEADSHOT(Placement.ScaleformAlertScreenStruct.HeadshotPic)
////							REGISTER_PEDHEADSHOT_TAB(Placement.IdlePed[Placement.ScreenPlace.iSelectedCharacter] , Placement.ScaleformAlertScreenStruct.sAlertTextureName, Placement.ScaleformAlertScreenStruct.HeadshotPic)	
//							Placement.bRunHeadshotTimer = TRUE
//							RESET_NET_TIMER(Placement.HeadshotTimer)
//							RUN_BUTTON_LOGIC(Placement)
////							IF IS_ROCKSTAR_DEV()
////								PAUSE_MENU_ACTIVATE_CONTEXT( HASH("IS_RSTAR_DEV"))	
////							ELSE
////								PAUSE_MENU_DEACTIVATE_CONTEXT( HASH("IS_RSTAR_DEV"))	
////							ENDIF
////							PAUSE_MENU_REDRAW_INSTRUCTIONAL_BUTTONS() 
//						BREAK
//						
//
//					
//					
//					ENDSWITCH
//					
//					
//
//					
//					Placement.bSetupTabs = FALSE
//
//					
//					NET_NL()NET_PRINT("RELEASE_CONTROL_OF_FRONTEND - called ")
////					RELEASE_CONTROL_OF_FRONTEND()
//				
//				ENDIF
//				
//				
//				
//				
//					ENDIF
//			
//			IF Placement.bRunHeadshotTimer
//				IF HAS_NET_TIMER_EXPIRED(Placement.HeadshotTimer, 500, TRUE)
//							UNLOAD_PEDHEADSHOT(Placement.ScaleformAlertScreenStruct.HeadshotPic)
//							REGISTER_PEDHEADSHOT_TAB(Placement.IdlePed[Placement.ScreenPlace.iSelectedCharacter] , Placement.ScaleformAlertScreenStruct.sAlertTextureName, Placement.ScaleformAlertScreenStruct.HeadshotPic)	
//					Placement.bRunHeadshotTimer = TRUE
//				ENDIF
//			ENDIF
//			
//		ENDIF
//							ENDIF

						
							
//
//	IF (LOBBY_AUTO_MULTIPLAYER_FREEMODE() AND HAS_NET_TIMER_EXPIRED(Placement.RemoveLoadingScreenTimer, 0))
//	OR LOBBY_AUTO_MULTIPLAYER_FREEMODE() = FALSE
//	
//		IF IS_PAUSE_MENU_ACTIVE()
//		AND NOT IS_PAUSE_MENU_RESTARTING()
////			IF IS_FRONTEND_READY_FOR_CONTROL()
////				IF Placement.bHudScreenInitialised = TRUE
//				
//					IF IS_SCREEN_FADED_IN()
//						IF HAS_NET_TIMER_EXPIRED(Placement.aTimer, 0)
//
//							ELSE
////							REFRESH_SCALEFORMXML_TABS_FACES(Placement.ScaleformXMLTabStruct)
//							REFRESH_SCALEFORMXML_TABS_TOP_DETAIL(Placement.ScaleformXMLTabStruct)
//							
//						ENDIF
//							ENDIF
							
						
//					PROCESS_MPHUD_CHAR_SELECT_INFORBOX(Placement)
//					
//					INT I
//					FOR I = 0 TO MAX_NUM_CHARACTER_SLOTS-1
//						PRIVATE_ADD_SCALEFORMXML_CHAR_SELECT_CHAR_IN_SLOT(I, (IS_STAT_CHARACTER_ACTIVE(I) AND HAS_SLOT_BEEN_IGNORED_CODE(I+1) = FALSE), Placement.ScaleformXMLCharSelectStruct)
//					ENDFOR
//					
//					PRIVATE_ADD_SCALEFORMXML_CHAR_SELECT_CURRENT_CHAR(Placement.ScreenPlace.iSelectedCharacter, Placement.ScaleformXMLCharSelectStruct)
//					RUN_SCALEFORMXML_CHAR_SELECTS(Placement.ScaleformXMLCharSelectStruct, SHOULD_REFRESH_SCALEFORMXML_CHAR_SELECT(Placement.ScaleformXMLCharSelectStruct))
//					RUN_SCALEFORMXML_CHAR_SELECTS_EACH_TAB_CHANGE(Placement.ScaleformXMLCharSelectStruct, SHOULD_REFRESH_SCALEFORMXML_CHAR_SELECT_EACH_TAB_CHANGE(Placement.ScaleformXMLCharSelectStruct))
//					
//					
//					IF Placement.ScreenPlace.iSelectedCharacter > -1
//					AND Placement.bArePedsSetup
//					AND REGISTER_PEDHEADSHOT_TAB(Placement.IdlePed[Placement.ScreenPlace.iSelectedCharacter] , Placement.ScaleformAlertScreenStruct.sAlertTextureName, Placement.ScaleformAlertScreenStruct.HeadshotPic)
//						RUN_SCALEFORMXML_TABS_FACE(Placement.ScaleformXMLTabStruct,Placement.ScaleformAlertScreenStruct, SHOULD_REFRESH_SCALEFORMXML_TABS_FACES(Placement.ScaleformXMLTabStruct))
//							ELSE
////						REFRESH_SCALEFORMXML_TABS_FACES(Placement.ScaleformXMLTabStruct)
//							ENDIF
//					
//						
//					IF IS_STRING_EMPTY_HUD(Placement.ScaleformXMLTabStruct.PlayerData.GangTextureName) = FALSE
//						RUN_SCALEFORMXML_TABS_EMBLEM(Placement.ScaleformXMLTabStruct, SHOULD_REFRESH_SCALEFORMXML_TABS_EMBLEM(Placement.ScaleformXMLTabStruct) )
//							ELSE
//						GAMER_HANDLE aGamer = GET_GAMER_HANDLE_PLAYER(PLAYER_ID())
//						IF NETWORK_CLAN_GET_EMBLEM_TXD_NAME(aGamer, Placement.ScaleformXMLTabStruct.PlayerData.GangTextureName) = FALSE
//							Placement.ScaleformXMLTabStruct.PlayerData.GangTextureName = ""
//						ENDIF
//						REFRESH_SCALEFORMXML_TABS_EMBLEM(Placement.ScaleformXMLTabStruct)
//							ENDIF
//					
//					
//		
//					RUN_CHARACTER_SELECT_TABS_NAME(Placement.ScaleformXMLTabStruct)
//					
//					RUN_CHARACTER_SELECT_TABS_TOP_DETAIL(Placement.ScaleformXMLTabStruct)
//							
						

//							ENDIF
//				ENDIF
//			ENDIF
		
	
//		RENDER_MPHUD_CHAR_SELECT_PEDS_ONLY(Placement, Placement.bArePedsSetup)
//	ENDIF
	
//	RUN_XML_ALLSLOTS_PEDSHOTS(Placement.IdlePed, Placement.ScreenPlace.iSelectedCharacter, Placement.iSelectedCharacter_LastFrame)	

	//For the Delete Screen
//	IF NOT HAS_SCALEFORM_MOVIE_LOADED(Placement.ButtonMovie[GET_SCALEFORM_INDEX_FOR_HUD(HUD_SCALEFORM_ALERT)] )
//		Placement.ButtonMovie[GET_SCALEFORM_INDEX_FOR_HUD(HUD_SCALEFORM_ALERT)] = REQUEST_SCALEFORM_MOVIE("popup_warning")
//		
//	ENDIF

//	BOOL HasQuit
//	IF IS_PAUSE_MENU_ACTIVE()
//	AND NOT IS_PAUSE_MENU_RESTARTING()
//		IF IS_FRONTEND_READY_FOR_CONTROL()
//			IF Placement.iButtonPressedRun = TRUE
//				IF GET_PLAYER_TRANSITION_KEYBOARD_INPUT_NAME(Placement.Transition_oskStatus, Placement.iTransition_KeyboardStage,Placement.iTransition_ProfanityCheck,Placement.iTransition_NumberOfAttempts, HasQuit, Placement.ScreenPlace.iSelectedCharacter)
//					Placement.ScaleformXMLTabStruct.iHighlightTab = Placement.ScreenPlace.iSelectedCharacter
//					REFRESH_SCALEFORMXML_TABS_DISPLAY(Placement.ScaleformXMLTabStruct)
//					Placement.bSetupTabs = TRUE
//					Placement.bHasNameChanged = TRUE
//					UI3DSCENE_MAKE_PUSHED_PRESET_PERSISTENT(FALSE)
//					Placement.iButtonPressedRun = FALSE
//				ELSE
//					IF HasQuit
//						Placement.iButtonPressedRun = FALSE
//						UI3DSCENE_MAKE_PUSHED_PRESET_PERSISTENT(FALSE)
//					ENDIF
//					EXIT
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF

//	IF (LOBBY_AUTO_MULTIPLAYER_FREEMODE() AND HAS_NET_TIMER_EXPIRED(Placement.RemoveLoadingScreenTimer, 3000))
//	OR LOBBY_AUTO_MULTIPLAYER_FREEMODE() = FALSE
//		IF NOT IS_WARNING_MESSAGE_ACTIVE()
//			IF GET_PAUSE_MENU_STATE() = PM_READY
//				LOGIC_MPHUD_XML_CHARACTER_SELECT_FM(Placement)
//			ENDIF
//		ENDIF
//	ENDIF
//	
//	TRANSITION_CHARACTER_SCREEN_EVENT_LISTENERS()

ENDPROC








