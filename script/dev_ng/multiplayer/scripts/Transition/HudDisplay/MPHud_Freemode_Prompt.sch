

USING "net_events.sch"
USING "script_network.sch"
USING "Screens_Header.sch"
//USING "net_lobby.sch"

USING "Transition_Common.sch"

CONST_INT MAX_NUMBER_FM_PROMPT_SELECTIONS 5

TRANSITION_FM_MENU_CHOICE FMMenuItems[MAX_NUMBER_FM_PROMPT_SELECTIONS]


PROC CLEAR_FM_MENU_ITEMS()
	INT i
	REPEAT MAX_NUMBER_FM_PROMPT_SELECTIONS i
		FMMenuItems[i] = FM_MENU_CHOICE_EMPTY	
	ENDREPEAT
ENDPROC

FUNC INT NO_OF_FM_MENU_OPTIONS()
	INT i
	INT iCount = 0
	REPEAT MAX_NUMBER_FM_PROMPT_SELECTIONS i
		IF NOT (FMMenuItems[i] = FM_MENU_CHOICE_EMPTY)	
			iCount++
		ENDIF
	ENDREPEAT	
	RETURN(iCount)
ENDFUNC


PROC SETUP_FM_MENU_ITEMS()
	CLEAR_FM_MENU_ITEMS()
	
	SWITCH GET_CURRENT_GAMEMODE()

		CASE GAMEMODE_FM	
			SWITCH GET_JOINING_GAMEMODE()
				CASE GAMEMODE_FM	//Swap teams
					
					SWITCH GET_CURRENT_TRANSITION_FM_MENU_CHOICE()
						CASE FM_MENU_CHOICE_RETURN_TO_MENU
							FMMenuItems[0] = FM_MENU_CHOICE_SWAP_TEAM
//							FMMenuItems[1] = FM_MENU_CHOICE_NEW_HOST
							FMMenuItems[1] = FM_MENU_CHOICE_JOIN_NEW_SESSION
							FMMenuItems[2] = FM_MENU_CHOICE_RETURN_TO_MENU
						BREAK
						CASE FM_MENU_CHOICE_SWAP_TEAM
//							FMMenuItems[0] = FM_MENU_CHOICE_NEW_HOST
							FMMenuItems[0] = FM_MENU_CHOICE_JOIN_NEW_SESSION
							FMMenuItems[1] = FM_MENU_CHOICE_RETURN_TO_MENU
						BREAK
			
					ENDSWITCH
				BREAK
				DEFAULT
					FMMenuItems[0] = FM_MENU_CHOICE_SWAP_TEAM
//					FMMenuItems[2] = FM_MENU_CHOICE_NEW_HOST
					FMMenuItems[1] = FM_MENU_CHOICE_JOIN_NEW_SESSION
					FMMenuItems[2] = FM_MENU_CHOICE_RETURN_TO_MENU
				BREAK
			ENDSWITCH
			
		BREAK

		CASE GAMEMODE_CREATOR
			FMMenuItems[0] = FM_MENU_CHOICE_JOIN_NEW_SESSION
//			FMMenuItems[1] = FM_MENU_CHOICE_NEW_HOST
		BREAK
		CASE GAMEMODE_SP
		CASE GAMEMODE_EMPTY
			FMMenuItems[0] = FM_MENU_CHOICE_JOIN_NEW_SESSION
//			FMMenuItems[1] = FM_MENU_CHOICE_NEW_HOST

		BREAK
	ENDSWITCH


	
	IF (g_iMenuSelection[HUDMENU_FREEMODE_PROMPT] > (NO_OF_FM_MENU_OPTIONS() - 1))
		g_iMenuSelection[HUDMENU_FREEMODE_PROMPT] = NO_OF_FM_MENU_OPTIONS() -1
	ENDIF
	IF (g_iMenuSelection[HUDMENU_FREEMODE_PROMPT] = -1)
		g_iMenuSelection[HUDMENU_FREEMODE_PROMPT] = 0
	ENDIF	
ENDPROC





















PROC RENDER_XML_MPHUD_FM_PROMPT(MPHUD_PLACEMENT_TOOLS& Placement)
	

	INT i 
	
	SETUP_FM_MENU_ITEMS()


	REPEAT NO_OF_FM_MENU_OPTIONS() i
		IF NOT (FMMenuItems[i] = FM_MENU_CHOICE_EMPTY)
			SWITCH FMMenuItems[i]

				CASE FM_MENU_CHOICE_RETURN_TO_SESSION
					PRIVATE_ADD_SCALEFORMXML_TEXT_SELECTION(i, "HUD_RETURNMP",  Placement.ScaleformXMLTextSelectStruct) //General Freemode
				BREAK

//				CASE FM_MENU_CHOICE_NEW_HOST
//					PRIVATE_ADD_SCALEFORMXML_TEXT_SELECTION(i, "HUD_RETURNMP",  Placement.ScaleformXMLTextSelectStruct) //General Freemode
//				BREAK
				
				CASE FM_MENU_CHOICE_JOIN_NEW_SESSION
					PRIVATE_ADD_SCALEFORMXML_TEXT_SELECTION(i, "HUD_RETURNMP",  Placement.ScaleformXMLTextSelectStruct) //General Freemode
				BREAK
				
				CASE FM_MENU_CHOICE_RETURN_TO_MENU
					PRIVATE_ADD_SCALEFORMXML_TEXT_SELECTION(i, "HUD_RETURNMP",  Placement.ScaleformXMLTextSelectStruct) //General Freemode
				BREAK
				
				CASE FM_MENU_CHOICE_SWAP_TEAM
					PRIVATE_ADD_SCALEFORMXML_TEXT_SELECTION(i, "HUD_RETURNMP",  Placement.ScaleformXMLTextSelectStruct) //General Freemode
				BREAK
				
				CASE FM_MENU_CHOICE_REJOIN_GAME
					PRIVATE_ADD_SCALEFORMXML_TEXT_SELECTION(i, "HUD_RETURNMP",  Placement.ScaleformXMLTextSelectStruct) //General Freemode
				BREAK
			ENDSWITCH
		ENDIF
	ENDREPEAT

	
	
	
	
	RUN_SCALEFORMXML_TEXT_SELECTIONS(Placement.ScaleformXMLTextSelectStruct, SHOULD_REFRESH_SCALEFORMXML_TEXT_SELECTION(Placement.ScaleformXMLTextSelectStruct))
	
ENDPROC



PROC LOGIC_XML_MPHUD_FM_PROMPT(MPHUD_PLACEMENT_TOOLS& Placement)		


	#IF IS_DEBUG_BUILD
	SWITCH GET_QUICKLAUNCH_STATE() 
		CASE QUICKLAUNCH_SP
			NET_NL()NET_PRINT("LOGIC_XML_MPHUD_FM_PROMPT GET_QUICKLAUNCH_STATE = QUICKLAUNCH_SP")
			FMMenuItems[g_iMenuSelection[HUDMENU_FREEMODE_PROMPT]] = FM_MENU_CHOICE_RETURN_TO_MENU
			SET_TRANSITION_STRING("HUD_QUITTING")
			HUD_CHANGE_STATE(HUD_STATE_LOADING)
			TRANSITION_CHANGE_STATE(TRANSITION_STATE_TERMINATE_SESSION)
			SET_QUICKLAUNCH_STATE(QUICKLAUNCH_EMPTY)
		BREAK

	ENDSWITCH
	#ENDIF
	
	
	


	CONST_INT CANCEL 0
	CONST_INT ACCEPT 1
	CONST_INT UP 2
	CONST_INT DOWN 3


	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_CANCEL)
		SET_BIT(Placement.ButtonReleasedBitset, CANCEL)
	ENDIF
	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
		SET_BIT(Placement.ButtonReleasedBitset, ACCEPT)
	ENDIF
	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_UP)
		SET_BIT(Placement.ButtonReleasedBitset, UP)
	ENDIF
	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_DOWN)
		SET_BIT(Placement.ButtonReleasedBitset, DOWN)
	ENDIF


	IF (IS_BIT_SET(Placement.ButtonReleasedBitset, ACCEPT)
	AND IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT) )

		CLEAR_BIT(Placement.ButtonReleasedBitset, ACCEPT)

		SET_CURRENT_TRANSITION_FM_MENU_CHOICE(FMMenuItems[g_iMenuSelection[HUDMENU_FREEMODE_PROMPT]])
		SWITCH FMMenuItems[g_iMenuSelection[HUDMENU_FREEMODE_PROMPT]]		

			CASE FM_MENU_CHOICE_REJOIN_GAME
				TRANSITION_BEGIN_RESPAWN()
			BREAK

			CASE FM_MENU_CHOICE_RETURN_TO_SESSION	
			
				IF IS_PLAYER_WANTING_TO_SWAP_CHARACTERS()
					NET_NL()NET_PRINT("LOGIC_XML_MPHUD_FM_PROMPT IS_PLAYER_WANTING_TO_SWAP_CHARACTERS = TRUE")
					SET_JOINING_GAMEMODE(GAMEMODE_FM)
					HUD_CHANGE_STATE(HUD_STATE_QUIT_CURRENT_SESSION_PROMPT)
				ELSE
					NET_NL()NET_PRINT("LOGIC_XML_MPHUD_FM_PROMPT IS_PLAYER_WANTING_TO_SWAP_CHARACTERS = FALSE")
					SET_JOINING_GAMEMODE(GAMEMODE_EMPTY)
					HUD_CHANGE_STATE(HUD_STATE_NO_DISPLAY)
					TRANSITION_CHANGE_STATE(TRANSITION_STATE_FM_SWOOP_DOWN)
				ENDIF
			BREAK
			CASE FM_MENU_CHOICE_JOIN_NEW_SESSION
			
				SWITCH GET_CURRENT_GAMEMODE()

					CASE GAMEMODE_FM //Moved here, see below comment 
					
						SWITCH GET_JOINING_GAMEMODE()
							CASE GAMEMODE_FM
								NET_NL()NET_PRINT("LOGIC_XML_MPHUD_FM_PROMPT GET_CURRENT_GAMEMODE = GAMEMODE_FM & GET_JOINING_GAMEMODE = GAMEMODE_FM")
								SET_JOINING_GAMEMODE(GAMEMODE_FM)
								HUD_CHANGE_STATE(HUD_STATE_LOADING) //HUD_STATE_FM_LOBBY_LOADING)
								TRANSITION_CHANGE_STATE(TRANSITION_STATE_LOOK_TO_JOIN_ANOTHER_SESSION_FM)
							BREAK
							DEFAULT
								NET_NL()NET_PRINT("LOGIC_XML_MPHUD_FM_PROMPT GET_CURRENT_GAMEMODE = GAMEMODE_FM & GET_JOINING_GAMEMODE = DEFAULT")
							
								SET_JOINING_GAMEMODE(GAMEMODE_FM)
								HUD_CHANGE_STATE(HUD_STATE_QUIT_CURRENT_SESSION_PROMPT)
							BREAK
						ENDSWITCH
					
					BREAK
					CASE GAMEMODE_SP
					CASE GAMEMODE_EMPTY
					CASE GAMEMODE_CREATOR
//					CASE GAMEMODE_FM	//Commented out because when I pressed circle in the DM lobby and come back into here hte session is still running. 
						
						SET_JOINING_GAMEMODE(GAMEMODE_FM)
						HUD_CHANGE_STATE(HUD_STATE_LOADING) //HUD_STATE_FM_LOBBY_LOADING)
						TRANSITION_CHANGE_STATE(TRANSITION_STATE_LOOK_FOR_FRESH_JOIN_FM)
					BREAK
				ENDSWITCH
			
			BREAK
//			CASE FM_MENU_CHOICE_NEW_HOST
//			
//				SWITCH GET_CURRENT_GAMEMODE()
//
//					CASE GAMEMODE_FM
//					
//						SWITCH GET_JOINING_GAMEMODE()
//							CASE GAMEMODE_FM
//								SET_JOINING_GAMEMODE(GAMEMODE_FM)
//								HUD_CHANGE_STATE(HUD_STATE_LOADING) //HUD_STATE_FM_LOBBY_LOADING)
//								TRANSITION_CHANGE_STATE(TRANSITION_STATE_LOOK_TO_HOST_SESSION_FM)
//							BREAK
//							DEFAULT
//								SET_JOINING_GAMEMODE(GAMEMODE_FM)
//								HUD_CHANGE_STATE(HUD_STATE_QUIT_CURRENT_SESSION_PROMPT)
//							BREAK
//						ENDSWITCH
//					
//					BREAK
//					
//					CASE GAMEMODE_SP
//					CASE GAMEMODE_EMPTY
//					CASE GAMEMODE_CREATOR
//				
//						SET_JOINING_GAMEMODE(GAMEMODE_FM)
//						HUD_CHANGE_STATE(HUD_STATE_LOADING) //HUD_STATE_FM_LOBBY_LOADING)
//						TRANSITION_CHANGE_STATE(TRANSITION_STATE_LOOK_FOR_FRESH_HOST_FM)
//					BREAK
//					
//					
//					
//				ENDSWITCH
//			
//
//			BREAK
			
			CASE FM_MENU_CHOICE_RETURN_TO_MENU
				HUD_CHANGE_STATE(HUD_STATE_QUIT_CURRENT_SESSION_PROMPT)
			BREAK
			
			CASE FM_MENU_CHOICE_SWAP_TEAM
				HUD_CHANGE_STATE(HUD_STATE_QUIT_CURRENT_SESSION_PROMPT)
				SET_JOINING_GAMEMODE(GET_CURRENT_GAMEMODE())
			BREAK
		
		ENDSWITCH

	ENDIF
	
	
	IF IS_BIT_SET(Placement.ButtonReleasedBitset, CANCEL)
		IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL) 
			CLEAR_BIT(Placement.ButtonReleasedBitset, CANCEL)
			SWITCH GET_CURRENT_GAMEMODE()
			CASE GAMEMODE_FM
						
				SWITCH GET_JOINING_GAMEMODE()
					CASE GAMEMODE_FM	//Swap teams
						SWITCH GET_CURRENT_TRANSITION_FM_MENU_CHOICE()
							CASE FM_MENU_CHOICE_RETURN_TO_MENU
								HUD_CHANGE_STATE(HUD_STATE_MODE_SELECTION)
							BREAK
							CASE FM_MENU_CHOICE_SWAP_TEAM
								HUD_CHANGE_STATE(HUD_STATE_SELECT_CHARACTER_FM)
							BREAK
						ENDSWITCH
						
					BREAK
					DEFAULT
						SET_JOINING_GAMEMODE(GAMEMODE_EMPTY)
						HUD_CHANGE_STATE(HUD_STATE_NO_DISPLAY)
						TRANSITION_CHANGE_STATE(TRANSITION_STATE_MP_SWOOP_DOWN)	
					BREAK
				ENDSWITCH
			BREAK
			DEFAULT
				HUD_CHANGE_STATE(HUD_STATE_MODE_SELECTION)	
			BREAK
		ENDSWITCH	
		ENDIF

	ENDIF


ENDPROC


PROC PROCESS_XML_MPHUD_FM_PROMPT(MPHUD_PLACEMENT_TOOLS& Placement)
	
	// initialise data
	IF NOT (Placement.bHudScreenInitialised)
		IF NOT IS_PAUSE_MENU_RESTARTING()
			Placement.bDoIHaveControl = TRUE
			IF IS_PAUSE_MENU_ACTIVE()
				
				IF IS_SOMETHING_QUITTING_MP()
					IF IS_THIS_SCREEN_ACTIVE(FE_MENU_VERSION_EMPTY_NO_BACKGROUND) = FALSE
						RESTART_FRONTEND_MENU(FE_MENU_VERSION_EMPTY_NO_BACKGROUND)
						NET_NL()NET_PRINT("PROCESS_XML_MPHUD_FM_PROMPT: RESTART_FRONTEND_MENU FE_MENU_VERSION_EMPTY_NO_BACKGROUND ")
					ENDIF
				ELSE
					IF IS_THIS_SCREEN_ACTIVE(FE_MENU_VERSION_TEXT_SELECTION) = FALSE
						RESTART_FRONTEND_MENU(FE_MENU_VERSION_TEXT_SELECTION)
						NET_NL()NET_PRINT("PROCESS_XML_MPHUD_FM_PROMPT: RESTART_FRONTEND_MENU FE_MENU_VERSION_TEXT_SELECTION ")
					ENDIF
				ENDIF
					
				

			ELSE
				Placement.bDoIHaveControl = FALSE
				IF IS_SOMETHING_QUITTING_MP()
					ACTIVATE_FRONTEND_MENU(FE_MENU_VERSION_EMPTY_NO_BACKGROUND, FALSE)
					NET_NL()NET_PRINT("PROCESS_XML_MPHUD_FM_PROMPT: ACTIVATE_FRONTEND_MENU FE_MENU_VERSION_EMPTY_NO_BACKGROUND ")
				ELSE
					ACTIVATE_FRONTEND_MENU(FE_MENU_VERSION_TEXT_SELECTION, FALSE)
					NET_NL()NET_PRINT("PROCESS_XML_MPHUD_FM_PROMPT: ACTIVATE_FRONTEND_MENU FE_MENU_VERSION_TEXT_SELECTION ")
				ENDIF
			ENDIF
			SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_FADE)
			
			REFRESH_SCALEFORMXML_TEXT_SELECTION(Placement.ScaleformXMLTextSelectStruct)
			REFRESH_SCALEFORMXML_TABS_DISPLAY(Placement.ScaleformXMLTabStruct)
			
			IF Placement.PreviousScreen = HUD_STATE_SELECT_CHARACTER_FM
				g_iMenuSelection[HUDMENU_FREEMODE_PROMPT] = 0
			ENDIF
			Placement.ScreenPlace.iSelection = g_iMenuSelection[HUDMENU_FREEMODE_PROMPT]
			
			Placement.bSetupTabs = FALSE
			
			
			Placement.bHudScreenInitialised = TRUE
		ENDIF
	ENDIF
	
	
		
	IF (Placement.bHudScreenInitialised)
	AND IS_PAUSE_MENU_ACTIVE()
	AND NOT IS_PAUSE_MENU_RESTARTING()
	
		IF IS_SOMETHING_QUITTING_MP()
			NET_NL()NET_PRINT("PROCESS_XML_MPHUD_FM_PROMPT IS_SOMETHING_QUITTING_MP = TRUE")
			FMMenuItems[g_iMenuSelection[HUDMENU_FREEMODE_PROMPT]] = FM_MENU_CHOICE_RETURN_TO_MENU
			SET_TRANSITION_STRING("HUD_QUITTING")
			REQUEST_TRANSITION_TERMINATE_MP_SESSION()
		ENDIF
	
		IF IS_FRONTEND_READY_FOR_CONTROL()
		
			IF HAS_MENU_TRIGGER_EVENT_OCCURRED()
				NET_NL()NET_PRINT("HAS_MENU_TRIGGER_EVENT_OCCURRED = TRUE")
				GET_MENU_TRIGGER_EVENT_DETAILS(Placement.aNextScreen, Placement.iMenu)
				NET_NL()NET_PRINT("		- aNextScreen = ")NET_PRINT(GET_XML_SCREEN_STRING_VALUE(Placement.aNextScreen))
				NET_NL()NET_PRINT("		- iMenu = ")NET_PRINT_INT(Placement.iMenu)
			ENDIF
			
			IF HAS_MENU_LAYOUT_CHANGED_EVENT_OCCURRED()
 
				NET_NL()NET_PRINT("HAS_MENU_LAYOUT_CHANGED_EVENT_OCCURRED = TRUE")
				GET_MENU_LAYOUT_CHANGED_EVENT_DETAILS(Placement.CurrentScreen, Placement.aNextScreen, Placement.iMenu)
				NET_NL()NET_PRINT("		- CurrentScreen = ")NET_PRINT(GET_XML_SCREEN_STRING_VALUE(Placement.CurrentScreen))
				NET_NL()NET_PRINT("		- aNextScreen = ")NET_PRINT(GET_XML_SCREEN_STRING_VALUE(Placement.aNextScreen))
				NET_NL()NET_PRINT("		- iMenu = ")NET_PRINT_INT(Placement.iMenu)
					
				MPLOBBY_SET_TITLE("HUD_MAINTIT", Placement.bSetupTitle)
//				STRING PlayerName = GET_PLAYER_NAME(PLAYER_ID())
//				TEXT_LABEL_63 CrewName = GET_LOCAL_PLAYER_CREW_NAME()
//				MPLOBBY_SET_HEADING_DETAILS(PlayerName, CrewName, Placement.bSetupCrewAndName)
					
				Placement.bDoIHaveControl = TRUE
				
				g_iMenuSelection[HUDMENU_FREEMODE_PROMPT] = Placement.iMenu

			ENDIF
	
			IF Placement.bDoIHaveControl = TRUE	
				RENDER_XML_MPHUD_FM_PROMPT(Placement)
				LOGIC_XML_MPHUD_FM_PROMPT(Placement)	
			ENDIF
			
		ELSE
			NET_NL()NET_PRINT("IS_FRONTEND_READY_FOR_CONTROL = FALSE ")
		ENDIF

		
	ENDIF

ENDPROC





