
USING "screens_header.sch"
USING "net_events.sch"
USING "script_network.sch"
USING "Transition_Common.sch"









PROC RENDER_MPHUD_MIGRATION_IN_PROGRESS(MPHUD_PLACEMENT_TOOLS& Placement)

	SET_ALL_MENUPED_VISIBLE(Placement.SelectionPed, Placement.IdlePed, FALSE )		




ENDPROC

PROC LOGIC_MPHUD_MIGRATION_IN_PROGRESS(MPHUD_PLACEMENT_TOOLS& Placement)
	
	
	STRING sStatementText = "HUD_CONNPROB" 
	STRING sLineOneMessge = "HUD_SAVEMIGPOST"
	STRING sLineTwoMessge = "HUD_SPRETRNFRSH"
		
	INT I
	FE_WARNING_FLAGS buttons
	STRUCT_SAVE_MIGRATION_STATUS Status

	SWITCH Placement.ianInt
	
		CASE 0
		
			IF g_sMPTUNABLES.bDisableSaveTransfersCancel = FALSE
				sStatementText = "HUD_CONNPROB" 
				sLineOneMessge = "HUD_SAVEMIGPOST"
				sLineTwoMessge = "HUD_SAVEMIGPOST1"
				buttons = FE_WARNING_OK + FE_WARNING_CANCEL_TRANSFER
		
				SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sLineOneMessge, buttons, sLineTwoMessge)
			
				IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
					SET_MIGRATION_NOT_FINISHED_BLOCK_SAVE(Placement)
					SET_TRANSITION_STRING("HUD_QUITTING")	
					NET_NL()NET_PRINT("LOGIC_MPHUD_MIGRATION_IN_PROGRESS: INPUT_FRONTEND_ACCEPT ")					
					REQUEST_TRANSITION(TRANSITION_STATE_RETURN_TO_SINGLEPLAYER, GAMEMODE_SP, HUD_STATE_NO_DISPLAY)					
				ENDIF
				
				IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_CANCEL)
					NET_NL()NET_PRINT("LOGIC_MPHUD_MIGRATION_IN_PROGRESS: INPUT_FRONTEND_CANCEL ")
					Placement.ianInt++
					
				ENDIF
			ELSE
				sStatementText = "HUD_CONNPROB" 
				sLineOneMessge = "HUD_SAVEMIGPOST"
				sLineTwoMessge = ""
				buttons = FE_WARNING_OK 
		
				SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sLineOneMessge, buttons, sLineTwoMessge)
			
				IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
					SET_MIGRATION_NOT_FINISHED_BLOCK_SAVE(Placement)
					SET_TRANSITION_STRING("HUD_QUITTING")	
					NET_NL()NET_PRINT("LOGIC_MPHUD_MIGRATION_IN_PROGRESS: INPUT_FRONTEND_ACCEPT ")					
					REQUEST_TRANSITION(TRANSITION_STATE_RETURN_TO_SINGLEPLAYER, GAMEMODE_SP, HUD_STATE_NO_DISPLAY)
				ENDIF
				
			
			ENDIF
		
		BREAK
	
		CASE 1
		
		
			sStatementText = "HUD_CONNPROB" // "HUD_CANCEL_TRANS" 
			sLineOneMessge = "HUD_SAVECAN" //"HUD_CANCEL_SURE"
			sLineTwoMessge = "" //"HUD_CANCEL_SURE1" removed for todo 2136705
			buttons = FE_WARNING_YESNO
	
			SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sLineOneMessge, buttons, sLineTwoMessge)
			IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
				
				NET_NL()NET_PRINT("LOGIC_MPHUD_MIGRATION_IN_PROGRESS: INPUT_FRONTEND_ACCEPT ")
				RESET_NET_TIMER(placement.aTimer)
				Placement.ianInt++
				
			ENDIF
			
			
		
		
			IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_CANCEL)
				SET_MIGRATION_NOT_FINISHED_BLOCK_SAVE(Placement)
				SET_TRANSITION_STRING("HUD_QUITTING")	
				NET_NL()NET_PRINT("LOGIC_MPHUD_MIGRATION_IN_PROGRESS: INPUT_FRONTEND_CANCEL ")				
				REQUEST_TRANSITION(TRANSITION_STATE_RETURN_TO_SINGLEPLAYER, GAMEMODE_SP, HUD_STATE_NO_DISPLAY)
			ENDIF
		
		BREAK
		
		CASE 2
		
			DRAW_RECT(0.5, 0.5, 1.0, 1.0, 0,0,0, 255)
			SET_TRANSITION_STRING("HUD_TRANSCANCEL")
			
			IF STAT_SAVE_MIGRATION_CANCEL_PENDING_OPERATION()
				NET_NL()NET_PRINT("LOGIC_MPHUD_MIGRATION_IN_PROGRESS: STAT_SAVE_MIGRATION_CANCEL_PENDING_OPERATION() = TRUE, move on ")
				Placement.ianInt++
				
			ELSE
				NET_NL()NET_PRINT("LOGIC_MPHUD_MIGRATION_IN_PROGRESS: STAT_SAVE_MIGRATION_CANCEL_PENDING_OPERATION() = FALSE, wait ")
			ENDIF
			
			IF HAS_NET_TIMER_EXPIRED_ONE_FRAME(placement.aTimer, 20000)
				NET_NL()NET_PRINT("LOGIC_MPHUD_MIGRATION_IN_PROGRESS: HAS_NET_TIMER_EXPIRED_ONE_FRAME(placement.aTimer), move to failed.  ")
				Placement.ianInt = 4
			ENDIF

		BREAK
		
		CASE 3
		
					
			DRAW_RECT(0.5, 0.5, 1.0, 1.0, 0,0,0, 255)
			SET_TRANSITION_STRING("HUD_TRANSCANCEL")

		
			SWITCH STAT_GET_CANCEL_SAVE_MIGRATION_STATUS()
	
				CASE CSMS_NONE
					NET_NL()NET_PRINT("LOGIC_MPHUD_MIGRATION_IN_PROGRESS: STAT_GET_CANCEL_SAVE_MIGRATION_STATUS = CSMS_NONE ")
					
				BREAK
				
				CASE CSMS_PENDING
					NET_NL()NET_PRINT("LOGIC_MPHUD_MIGRATION_IN_PROGRESS: STAT_GET_CANCEL_SAVE_MIGRATION_STATUS = CSMS_PENDING ")
					
				BREAK
				
				CASE CSMS_FAILED
					NET_NL()NET_PRINT("LOGIC_MPHUD_MIGRATION_IN_PROGRESS: STAT_GET_CANCEL_SAVE_MIGRATION_STATUS = CSMS_FAILED ")
					Placement.ianInt = 4
				BREAK
				
				CASE CSMS_SUCCEDDED
					NET_NL()NET_PRINT("LOGIC_MPHUD_MIGRATION_IN_PROGRESS: STAT_GET_CANCEL_SAVE_MIGRATION_STATUS = CSMS_SUCCEDDED ")
					Placement.ianInt = 5
					
				BREAK
				
				CASE CSMS_CANCELED
					NET_NL()NET_PRINT("LOGIC_MPHUD_MIGRATION_IN_PROGRESS: STAT_GET_CANCEL_SAVE_MIGRATION_STATUS = CSMS_CANCELED ")
					Placement.ianInt = 4
				BREAK
			
			ENDSWITCH
			
			
		BREAK
		
		CASE 4 //Failed
		
			sStatementText = "HUD_CONNPROB" 
			sLineOneMessge = "HUD_TRANSCANCEL_F"
			sLineTwoMessge = ""
			buttons = FE_WARNING_OK
	
			SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sLineOneMessge, buttons)

			IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
				SET_MIGRATION_NOT_FINISHED_BLOCK_SAVE(Placement)
				SET_TRANSITION_STRING("HUD_QUITTING")	
				NET_NL()NET_PRINT("LOGIC_MPHUD_MIGRATION_IN_PROGRESS: INPUT_FRONTEND_ACCEPT - failed ")				
				REQUEST_TRANSITION(TRANSITION_STATE_RETURN_TO_SINGLEPLAYER, GAMEMODE_SP, HUD_STATE_NO_DISPLAY)
			ENDIF
		
		BREAK
		
		CASE 5 //Succeeded
		
			sStatementText = "HUD_CANTIT" 
			sLineOneMessge = "HUD_TRANSCANCEL_S"
			sLineTwoMessge = ""
			buttons = FE_WARNING_YESNO
	
			SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sLineOneMessge, buttons)

			


			IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
			
				STAT_GET_SAVE_MIGRATION_STATUS(Status)
				SET_SAVE_MIGRATION_TRANSACTION_ID_WARNING(Status.m_TransactionId)

				Placement.iCheckMigrateStages = 0
				SET_CHARACTER_LAST_GEN_CHECK(LAST_GEN_STATUS_NONE)
				
				NET_NL()NET_PRINT("[SAVETRANS] LOGIC_MPHUD_MIGRATION_IN_PROGRESS: Moving POST_CHECK_SAVE_MIGRATION to start again.  ")
				
				FOR I = 0 TO MAX_NUM_CHARACTER_SLOTS-1
					SET_MP_BOOL_CHARACTER_STAT(MP_STAT_WAS_CHAR_TRANSFERED, FALSE, I)
					Placement.iWasCharacterTransferred[I] = FALSE
				ENDFOR
				
				RESET_PAUSE_MENU_WARP_REQUESTS()				
								
				NET_NL()NET_PRINT("LOGIC_MPHUD_MIGRATION_IN_PROGRESS: INPUT_FRONTEND_ACCEPT - Succeeded ")
				SET_JOINING_GAMEMODE(GAMEMODE_FM)
				HUD_CHANGE_STATE(HUD_STATE_PRE_HUD_CHECKS)
				TRANSITION_CHANGE_STATE(TRANSITION_STATE_PRE_HUD_CHECKS)
			ENDIF
			
			
			IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_CANCEL)
				SET_MIGRATION_NOT_FINISHED_BLOCK_SAVE(Placement)

				Placement.iCheckMigrateStages = 0
				
				STAT_GET_SAVE_MIGRATION_STATUS(Status)
				SET_SAVE_MIGRATION_TRANSACTION_ID_WARNING(Status.m_TransactionId)

				SET_CHARACTER_LAST_GEN_CHECK(LAST_GEN_STATUS_NONE)
				SET_TRANSITION_STRING("HUD_QUITTING")	
				NET_NL()NET_PRINT("LOGIC_MPHUD_MIGRATION_IN_PROGRESS: INPUT_FRONTEND_CANCEL - Succeeded ")				
				REQUEST_TRANSITION(TRANSITION_STATE_RETURN_TO_SINGLEPLAYER, GAMEMODE_SP, HUD_STATE_NO_DISPLAY)
			ENDIF
		
			
		
		BREAK
	
	ENDSWITCH
	
	
	



ENDPROC


PROC PROCESS_MPHUD_MIGRATION_IN_PROGRESS(MPHUD_PLACEMENT_TOOLS& Placement)
	
	// initialise data
	IF NOT (Placement.bHudScreenInitialised)
		IF IS_PAUSE_MENU_ACTIVE()
			SET_PAUSE_MENU_ACTIVE(FALSE)
			SET_SKYFREEZE_CLEAR()
		ENDIF
		
		SET_BAIL_STILL_VALID(FALSE) //Need this when signing out in a corona, it would bring this screen up twice. It would think the bail was still valid in cleanup
		RUN_MP_DISCONNECTION_CLEANUP(Placement)
		
		SET_CURRENT_GAMEMODE(GAMEMODE_EMPTY)
		SET_JOINING_GAMEMODE(GAMEMODE_EMPTY)
		SET_PAUSE_MENU_MIGRATE_SAVES(FALSE)
		Placement.ianInt = 0
		Placement.bHudScreenInitialised = TRUE
	ENDIF

	RUN_MP_DISCONNECTION_CLEANUP_EVERY_FRAME()

	
	RENDER_MPHUD_MIGRATION_IN_PROGRESS(Placement)
	
// logic	
	LOGIC_MPHUD_MIGRATION_IN_PROGRESS(Placement)
	
			
ENDPROC

