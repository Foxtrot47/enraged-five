
USING "screens_header.sch"
USING "net_events.sch"
USING "script_network.sch"
USING "Transition_Common.sch"



PROC RENDER_MPHUD_FAILED_CATALOGUE(MPHUD_PLACEMENT_TOOLS& Placement)


	SET_ALL_MENUPED_VISIBLE(Placement.SelectionPed, Placement.IdlePed, FALSE )		

	STRING sStatementText = "HUD_CONNPROB"
	STRING sQuestionText = "HUD_CATAGLOG"
	STRING sSubText = "HUD_QURETSP"
	

	
	

	SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sQuestionText, FE_WARNING_OK, sSubText)
ENDPROC






PROC LOGIC_MPHUD_FAILED_CATALOGUE()

	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)		
		SET_TRANSITION_STRING("HUD_QUITTING")
		REQUEST_TRANSITION_TERMINATE_MP_SESSION()
	ENDIF	

ENDPROC

PROC PROCESS_MPHUD_FAILED_CATALOGUE(MPHUD_PLACEMENT_TOOLS& Placement)
	
	// initialise data
	IF NOT (Placement.bHudScreenInitialised)

	
		Placement.bHudScreenInitialised = TRUE
	ENDIF


	// logic	
	LOGIC_MPHUD_FAILED_CATALOGUE()
	RENDER_MPHUD_FAILED_CATALOGUE(Placement)

			
ENDPROC

