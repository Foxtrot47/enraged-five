
USING "screens_header.sch"
USING "net_events.sch"
USING "script_network.sch"
USING "Transition_Common.sch"



PROC RENDER_MPHUD_MIGRATION_IN_PROGRESS_FAILED(MPHUD_PLACEMENT_TOOLS& Placement)

	SET_ALL_MENUPED_VISIBLE(Placement.SelectionPed, Placement.IdlePed, FALSE )		
	STRING sStatementText
	STRING sLineOneMessge
	STRING sLineTwoMessge
	
	//IF it's a general error then display that the game services are unavaliable
	IF g_bFailedMigrationGeneralError = TRUE
		sStatementText = "HUD_CONNPROB" 
		sLineOneMessge = "PM_H_D1_1"
		sLineTwoMessge = "HUD_SPRETRNFRSH"
	ELSE
		sStatementText = "HUD_CONNPROB" 
		sLineOneMessge = "HUD_SAVEMIGPOST2"
		sLineTwoMessge = "HUD_SPRETRNFRSH"
	ENDIF
	FE_WARNING_FLAGS buttons = FE_WARNING_OK
	
	SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sLineOneMessge, buttons, sLineTwoMessge)


ENDPROC

PROC LOGIC_MPHUD_MIGRATION_IN_PROGRESS_FAILED()
	
	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
	
		SET_FORCE_RUN_PRE_MIGRATION_CHECK_AFTER_FAIL(TRUE)
		REQUEST_TRANSITION(TRANSITION_STATE_RETURN_TO_SINGLEPLAYER, GAMEMODE_SP, HUD_STATE_NO_DISPLAY)
		NET_NL()NET_PRINT("[SAVETRANS] PROCESS_MPHUD_MIGRATION_IN_PROGRESS_FAILED g_bFailedMigrationGeneralError = FALSE")
		g_bFailedMigrationGeneralError = FALSE
	ENDIF



ENDPROC


PROC PROCESS_MPHUD_MIGRATION_IN_PROGRESS_FAILED(MPHUD_PLACEMENT_TOOLS& Placement)
	
	// initialise data
	IF NOT (Placement.bHudScreenInitialised)
		IF IS_PAUSE_MENU_ACTIVE()
			SET_PAUSE_MENU_ACTIVE(FALSE)
			SET_SKYFREEZE_CLEAR()
		ENDIF
		
		SET_BAIL_STILL_VALID(FALSE) //Need this when signing out in a corona, it would bring this screen up twice. It would think the bail was still valid in cleanup
		RUN_MP_DISCONNECTION_CLEANUP(Placement)
		
		SET_CURRENT_GAMEMODE(GAMEMODE_EMPTY)
		SET_JOINING_GAMEMODE(GAMEMODE_EMPTY)
		Placement.bHudScreenInitialised = TRUE
	ENDIF

	RUN_MP_DISCONNECTION_CLEANUP_EVERY_FRAME()

	
	RENDER_MPHUD_MIGRATION_IN_PROGRESS_FAILED(Placement)
	
// logic	
	LOGIC_MPHUD_MIGRATION_IN_PROGRESS_FAILED()
	
			
ENDPROC

