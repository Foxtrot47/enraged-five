USING "screens_header.sch"
USING "net_include.sch"
USING "script_network.sch"
USING "screen_tabs.sch"
USING "net_mission.sch"
USING "net_transition.sch"
USING "Transition_Common.sch"

//CONST_INT MAX_NUMBER_GAMEMODE_OPTIONS 3





FUNC BOOL SELECTING_DIFFERENT_INVITED_MODE()
	
	IF GET_CONFIRM_INVITE_INTO_GAME_STATE() = TRUE
		SWITCH g_iMenuSelection[HUDMENU_SELECT_SCREEN] 
			CASE 1 // Freemode
				IF GET_JOINING_GAMEMODE() <> GAMEMODE_FM
					RETURN TRUE
				ENDIF
			BREAK

		ENDSWITCH
	ENDIF
	
	RETURN FALSE
ENDFUNC









PROC RENDER_XML_MPHUD_MODE_SELECT(MPHUD_PLACEMENT_TOOLS& Placement)
	
	
	
//	SCALEFORMXML_TABS_INPUT_DATA TabsXMLData
//
//
//	TabsXMLData.MainTitle = "HUD_MULT_TIT"
//	COMPILE_SCALEFORMXML_TABS(PLAYER_PED_ID(), TabsXMLData, Placement.ScaleformXMLTabStruct )
//	RUN_SCALEFORMXML_TABS(Placement.ScaleformXMLTabStruct, SHOULD_REFRESH_SCALEFORMXML_TABS_DISPLAY(Placement.ScaleformXMLTabStruct))
//
//	
//	BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND_HEADER

	
	
	
	
	INT iCount = 0
	
	
	PRIVATE_ADD_SCALEFORMXML_TEXT_SELECTION(iCount, "HUD_TITLE2",  Placement.ScaleformXMLTextSelectStruct) //Freemode
	iCount++
	PRIVATE_ADD_SCALEFORMXML_TEXT_SELECTION(iCount, "HUD_TITLE4",  Placement.ScaleformXMLTextSelectStruct) //Creator
	iCount++
	PRIVATE_ADD_SCALEFORMXML_TEXT_SELECTION(iCount, "HUD_TITLE6",  Placement.ScaleformXMLTextSelectStruct) //Singleplayer
	iCount++
	
	
	
	RUN_SCALEFORMXML_TEXT_SELECTIONS(Placement.ScaleformXMLTextSelectStruct, SHOULD_REFRESH_SCALEFORMXML_TEXT_SELECTION(Placement.ScaleformXMLTextSelectStruct))
	
ENDPROC













PROC LOGIC_XML_MPHUD_MODE_SELECT(MPHUD_PLACEMENT_TOOLS& Placement)		


	CONST_INT CANCEL 0
	CONST_INT ACCEPT 1
	CONST_INT UP 2
	CONST_INT DOWN 3


	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_CANCEL)
		SET_BIT(Placement.ButtonReleasedBitset, CANCEL)
	ENDIF
	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
		SET_BIT(Placement.ButtonReleasedBitset, ACCEPT)
	ENDIF
	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_UP)
		SET_BIT(Placement.ButtonReleasedBitset, UP)
	ENDIF
	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_DOWN)
		SET_BIT(Placement.ButtonReleasedBitset, DOWN)
	ENDIF

			





	IF (IS_BIT_SET(Placement.ButtonReleasedBitset, ACCEPT)
	AND IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT) )
	OR Placement.SelectedGamemode <> -1

		CLEAR_BIT(Placement.ButtonReleasedBitset, ACCEPT)


		IF SELECTING_DIFFERENT_INVITED_MODE() = TRUE
			Placement.SelectedGamemode = g_iMenuSelection[HUDMENU_SELECT_SCREEN]
			HUD_CHANGE_STATE(HUD_STATE_IGNORE_INVITE)
		ELSE
			IF Placement.SelectedGamemode <> -1
				g_iMenuSelection[HUDMENU_SELECT_SCREEN] = Placement.SelectedGamemode 
				Placement.SelectedGamemode = -1
			ENDIF
			SWITCH g_iMenuSelection[HUDMENU_SELECT_SCREEN] 

				CASE 0
					IF SCRIPT_IS_CLOUD_AVAILABLE()
						HUD_CHANGE_STATE(HUD_STATE_FM_SESSION_PROMPT)
					ENDIF
					

				BREAK

				CASE 1
					
					SET_SINGLEPLAYER_END_NOW()
					SET_CURRENT_GAMEMODE(GAMEMODE_CREATOR)
					TRANSITION_CHANGE_STATE(TRANSITION_STATE_START_CREATOR_LAUNCH_SCRIPT)
					HUD_CHANGE_STATE(HUD_STATE_NO_DISPLAY)
					

					

				BREAK

				CASE 2 //Singleplayer
					IF IS_THIS_SCREEN_ACTIVE(FE_MENU_VERSION_EMPTY_NO_BACKGROUND) = FALSE
						RESTART_FRONTEND_MENU(FE_MENU_VERSION_EMPTY_NO_BACKGROUND)
					ENDIF
					SWITCH GET_CURRENT_GAMEMODE() 

						CASE GAMEMODE_FM
							REQUEST_TRANSITION(GET_CURRENT_TRANSITION_STATE(), GAMEMODE_SP, HUD_STATE_QUIT_CURRENT_SESSION_PROMPT)
						BREAK
						CASE GAMEMODE_CREATOR

							SET_SINGLEPLAYER_RUNNING_NOW()
							REQUEST_TRANSITION(TRANSITION_STATE_RETURN_TO_SINGLEPLAYER, GAMEMODE_EMPTY, HUD_STATE_NO_DISPLAY)
						BREAK
					
						CASE GAMEMODE_SP
						CASE GAMEMODE_EMPTY											
							REQUEST_TRANSITION(TRANSITION_STATE_RETURN_TO_SINGLEPLAYER, GET_CURRENT_GAMEMODE(), HUD_STATE_NO_DISPLAY) //This is going through this transition so I can check if Kenneth is taking the camera or not.	
						BREAK

					ENDSWITCH		
				BREAK
			ENDSWITCH
		ENDIF


	ENDIF
	

	
	IF IS_BIT_SET(Placement.ButtonReleasedBitset, CANCEL)
		IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL) 
			CLEAR_BIT(Placement.ButtonReleasedBitset, CANCEL)
		
			SWITCH GET_CURRENT_GAMEMODE()
				CASE GAMEMODE_EMPTY
					
				BREAK
				CASE GAMEMODE_SP
					IF Placement.PreviousScreen != HUD_STATE_PRE_HUD_CHECKS
						HUD_CHANGE_STATE(Placement.PreviousScreen)
					ELSE
						HUD_CHANGE_STATE(HUD_STATE_NO_DISPLAY) //This is fine normally
						TRANSITION_CHANGE_STATE(TRANSITION_STATE_SP_SWOOP_DOWN)
					ENDIF
				BREAK

//				CASE GAMEMODE_DM
//					HUD_CHANGE_STATE(HUD_STATE_DM_LOBBY_RUNNING)
//				BREAK
				CASE GAMEMODE_FM
					SWITCH GET_CURRENT_TRANSITION_FM_MENU_CHOICE() 
						CASE FM_MENU_CHOICE_SWAP_TEAM 	//If are in CNC, say yes to quitting after saying to swap teams then back out of creating a new character to this point. 
							
						BREAK
						DEFAULT
							IF HAS_QUIT_CURRENT_GAME() = FALSE
								HUD_CHANGE_STATE(HUD_STATE_NO_DISPLAY) //This is fine normally
								TRANSITION_CHANGE_STATE(TRANSITION_STATE_MP_SWOOP_DOWN)
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		ENDIF

	ENDIF

	

ENDPROC


PROC PROCESS_XML_MPHUD_MODE_SELECT(MPHUD_PLACEMENT_TOOLS& Placement)
	
	IF (IS_PAUSE_MENU_ACTIVE() = FALSE
	AND Placement.PreviousScreen = HUD_STATE_SELECT_CHARACTER_FM)
	OR (Placement.PreviousScreen != HUD_STATE_SELECT_CHARACTER_FM)
	
		IF NOT IS_PAUSE_MENU_RESTARTING()
			// initialise data
			IF NOT (Placement.bHudScreenInitialised)

				Placement.bDoIHaveControl = TRUE

				IF IS_PAUSE_MENU_ACTIVE()
					IF IS_THIS_SCREEN_ACTIVE(FE_MENU_VERSION_TEXT_SELECTION) = FALSE
						Placement.bDoIHaveControl = FALSE
						RESTART_FRONTEND_MENU(FE_MENU_VERSION_TEXT_SELECTION)
						
						NET_NL()NET_PRINT("PROCESS_XML_MPHUD_MODE_SELECT: RESTART_FRONTEND_MENU")
					ENDIF

				ELSE
					Placement.bDoIHaveControl = FALSE

					ACTIVATE_FRONTEND_MENU(FE_MENU_VERSION_TEXT_SELECTION, FALSE)
					NET_NL()NET_PRINT("PROCESS_XML_MPHUD_MODE_SELECT: ACTIVATE_FRONTEND_MENU")
				ENDIF
				SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_FADE)
				
				REFRESH_SCALEFORMXML_TABS_DISPLAY(Placement.ScaleformXMLTabStruct)
				REFRESH_SCALEFORMXML_TEXT_SELECTION(Placement.ScaleformXMLTextSelectStruct)
				
				IF Placement.PreviousScreen = HUD_STATE_SELECT_CHARACTER_FM
					g_iMenuSelection[HUDMENU_SELECT_SCREEN] = 0
				ENDIF
				Placement.ScreenPlace.iSelection = g_iMenuSelection[HUDMENU_SELECT_SCREEN]
				
				Placement.bSetupTabs = FALSE
				Placement.bHudScreenInitialised = TRUE
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
				NET_NL()NET_PRINT("PROCESS_XML_MPHUD_MODE_SELECT: IS_PAUSE_MENU_RESTARTING = TRUE")
			#ENDIF
	
		ENDIF
	ENDIF
		
	IF (Placement.bHudScreenInitialised)
	AND IS_PAUSE_MENU_ACTIVE()
	AND NOT IS_PAUSE_MENU_RESTARTING()
	
		IF IS_FRONTEND_READY_FOR_CONTROL()
		
			IF HAS_MENU_TRIGGER_EVENT_OCCURRED()
				NET_NL()NET_PRINT("HAS_MENU_TRIGGER_EVENT_OCCURRED = TRUE")
				GET_MENU_TRIGGER_EVENT_DETAILS(Placement.aNextScreen, Placement.iMenu)
				NET_NL()NET_PRINT("		- aNextScreen = ")NET_PRINT(GET_XML_SCREEN_STRING_VALUE(Placement.aNextScreen))
				NET_NL()NET_PRINT("		- iMenu = ")NET_PRINT_INT(Placement.iMenu)
			ENDIF
			
			IF HAS_MENU_LAYOUT_CHANGED_EVENT_OCCURRED()
 
				NET_NL()NET_PRINT("HAS_MENU_LAYOUT_CHANGED_EVENT_OCCURRED = TRUE")
				GET_MENU_LAYOUT_CHANGED_EVENT_DETAILS(Placement.CurrentScreen, Placement.aNextScreen, Placement.iMenu)
				NET_NL()NET_PRINT("		- CurrentScreen = ")NET_PRINT(GET_XML_SCREEN_STRING_VALUE(Placement.CurrentScreen))
				NET_NL()NET_PRINT("		- aNextScreen = ")NET_PRINT(GET_XML_SCREEN_STRING_VALUE(Placement.aNextScreen))
				NET_NL()NET_PRINT("		- iMenu = ")NET_PRINT_INT(Placement.iMenu)
					
				MPLOBBY_SET_TITLE("HUD_MAINTIT", Placement.bSetupTitle)
//				STRING PlayerName = GET_PLAYER_NAME(PLAYER_ID())
//				TEXT_LABEL_63 CrewName = GET_LOCAL_PLAYER_CREW_NAME()
//				MPLOBBY_SET_HEADING_DETAILS(PlayerName, CrewName, Placement.bSetupCrewAndName)
					
				Placement.bDoIHaveControl = TRUE
				
				g_iMenuSelection[HUDMENU_SELECT_SCREEN] = Placement.iMenu

			ENDIF
	
			IF Placement.bDoIHaveControl = TRUE	
//				RENDER_XML_MPHUD_MODE_SELECT(Placement)
				LOGIC_XML_MPHUD_MODE_SELECT(Placement)	
			ENDIF
			
		ELSE
			NET_NL()NET_PRINT("IS_FRONTEND_READY_FOR_CONTROL = FALSE ")
		ENDIF

		
	ENDIF

ENDPROC

