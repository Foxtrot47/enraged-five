
USING "screens_header.sch"
USING "net_events.sch"
USING "script_network.sch"
USING "Transition_Common.sch"

PROC INIT_MPHUD_TEAMFULL(MPHUD_PLACEMENT_TOOLS& Placement)

	RESET_ALL_SPRITE_PLACEMENT_VALUES(Placement.SpritePlacement)


	Placement.TextPlacement[0].x = 0.308
	Placement.TextPlacement[0].y = 0.486 * 0.5626
	
	Placement.TextPlacement[1].x = 0.567
	Placement.TextPlacement[1].y = 0.707 * 0.5626	
	
	//Scaleform movie Bottom
	Placement.SpritePlacement[0].x = 0.389
	Placement.SpritePlacement[0].y = 0.467
	Placement.SpritePlacement[0].w = 1.0
	Placement.SpritePlacement[0].h = 1.0
	Placement.SpritePlacement[0].r = 255
	Placement.SpritePlacement[0].g = 255
	Placement.SpritePlacement[0].b = 255
	Placement.SpritePlacement[0].a = 200
	
ENDPROC

PROC RENDER_MPHUD_TEAMFULL(MPHUD_PLACEMENT_TOOLS& Placement)


	SET_ALL_MENUPED_VISIBLE(Placement.SelectionPed, Placement.IdlePed, FALSE )		

	
	STRING sStatementText

	
	IF GET_CURRENT_GAMEMODE() = GAMEMODE_FM OR GET_JOINING_GAMEMODE() = GAMEMODE_FM
		SWITCH GET_STAT_CHARACTER_TEAM(Placement.ScreenPlace.iSelectedCharacter)
			CASE TEAM_FREEMODE
				sStatementText = "HUD_FMFULL"
			BREAK
			CASE TEAM_SCTV
				sStatementText = "HUD_SPECFULL"
			BREAK
		ENDSWITCH
	
	ENDIF

	SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, "HUD_ERSELECT", FE_WARNING_RETRY | FE_WARNING_BACK)
ENDPROC

PROC LOGIC_MPHUD_TEAMFULL()

	IF NETWORK_IS_SESSION_ACTIVE() = FALSE
		IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_CANCEL)
			PLAY_SOUND_FRONTEND(-1, "CANCEL", "HUD_FRONTEND_DEFAULT_SOUNDSET")
			SET_CURRENT_GAMEMODE(GAMEMODE_EMPTY)
			SET_JOINING_GAMEMODE(GAMEMODE_FM) 
			HUD_CHANGE_STATE(HUD_STATE_SELECT_CHARACTER_FM)
			TRANSITION_CHANGE_STATE(TRANSITION_STATE_WAIT_HUD_EXIT)
		ENDIF

	ELSE
		NET_NL()NET_PRINT("LOGIC_MPHUD_TEAMFULL: NETWORK_IS_SESSION_ACTIVE() = TRUE")
	ENDIF

ENDPROC

PROC PROCESS_MPHUD_TEAMFULL(MPHUD_PLACEMENT_TOOLS& Placement)
	
	// initialise data
	IF NOT (Placement.bHudScreenInitialised)
		INIT_MPHUD_TEAMFULL(Placement)
		Placement.bHudScreenInitialised = TRUE
	ENDIF

	// logic	
	LOGIC_MPHUD_TEAMFULL()
	
	IF HAS_ALL_COMMON_HUD_ELEMENTS_LOADED(Placement)
		RENDER_MPHUD_TEAMFULL(Placement)
	ENDIF

			
ENDPROC

