/* ------------------------------------------------------------------
* Name: MPHud_Ped_Generation.sch
* Author: James Adwick
* Date: 30/04/2013
* Purpose: Header to support Character Creator. Handles lifestyle
* calculations and head blends
* ------------------------------------------------------------------*/

USING "screens_header.sch"
USING "Transition_Common.sch"
USING "stats_helper_packed.sch"
USING "Transition_controller.sch"

// ***********************************
// 		CHARACTER CREATOR CONST
// ***********************************


CONST_INT CHAR_CREATOR_CANCEL						0
CONST_INT CHAR_CREATOR_ACCEPT						1
CONST_INT CHAR_CREATOR_SQUARE						2
CONST_INT CHAR_CREATOR_TRIANGLE						3

// Bitsets
CONST_INT CHAR_CREATOR_SKIP_WAIT_FOR_FRONTEND		0
CONST_INT CHAR_CREATOR_ALL_PEDS_LOADED				1
CONST_INT CHAR_CREATOR_UPDATE_CHILD_BLEND			2
CONST_INT CHAR_CREATOR_CHILD_SPINNER_DISPLAYED		3
CONST_INT CHAR_CREATOR_PARENTS_SPINNER_DISPLAYED	4
CONST_INT CHAR_CREATOR_CHILD_PERSIST_PRESET			5
CONST_INT CHAR_CREATOR_CALC_STAT_UPGRADES			6
CONST_INT CHAR_CREATOR_DOMINANCE_CHANGING			7
CONST_INT CHAR_CREATOR_WAIT_TO_BACK_OUT_OF_CONFIRM	8
CONST_INT CHAR_CREATOR_WAIT_TO_CLEAR_SPINNERS		9
CONST_INT CHAR_CREATOR_DO_RANDOMISE					10
CONST_INT CHAR_CREATOR_DO_CHILD_BUSY_SPINNER		11
CONST_INT CHAR_CREATOR_UPDATE_MUM_BLEND				12
CONST_INT CHAR_CREATOR_UPDATE_DAD_BLEND				13
CONST_INT CHAR_CREATOR_WAIT_TO_ENTER_GAME			14
CONST_INT CHAR_CREATOR_WAIT_TO_BACK_OUT_OF_CONFIRM_FRAME 15
CONST_INT CHAR_CREATOR_FORCE_HERITAGE_UPDATE 16
CONST_INT CHAR_CREATOR_FORCE_DRAW_MALE 17
CONST_INT CHAR_CREATOR_FORCE_DRAW_FEMALE 18
CONST_INT CHAR_CREATOR_DONT_UPDATE_GRANDPARENTS 19
CONST_INT CHAR_CREATOR_COPY_FROM_SON_DUMMY 20
CONST_INT CHAR_CREATOR_COPY_FROM_DAUGHTER_DUMMY 21
CONST_INT CHAR_CREATOR_MAINTAIN_SPECIAL_DAD 22
CONST_INT CHAR_CREATOR_MAINTAIN_SPECIAL_MUM 23
//CONST_INT CHAR_CREATOR_APPLY_CREW_DECALS 24
CONST_INT CHAR_CREATOR_FORCE_CREW_SHIRT_ZOOM 25
//CONST_INT CHAR_CREATOR_RESET_PALETTE 26
CONST_INT CHAR_CREATOR_DO_DELAYED_CREW_SHIRT_ZOOM 27
CONST_INT CHAR_CREATOR_READY_DELAYED_CREW_SHIRT_ZOOM 28

CONST_INT CHAR_CREATOR_RACE_INDEX_ONE	0
CONST_INT CHAR_CREATOR_RACE_INDEX_TWO	1

CONST_INT MAX_MALE_GRANDPARENT			18
CONST_INT MAX_FEMALE_GRANDPARENT		18

// Lifestyle max values
CONST_INT ciCHAR_SLEEP_MAX		24 //10
CONST_INT ciCHAR_FAMILY_MAX		8  //4
CONST_INT ciCHAR_SPORT_MAX		16 //8
CONST_INT ciCHAR_LEGAL_MAX		20 //10
CONST_INT ciCHAR_TV_MAX			16 //8
CONST_INT ciCHAR_PARTY_MAX		16 //8
CONST_INT ciCHAR_ILLEGAL_MAX	20 //10

// Lifestyle min values
CONST_INT ciCHAR_SLEEP_MIN		8 //4
CONST_INT ciCHAR_FAMILY_MIN		0
CONST_INT ciCHAR_SPORT_MIN		0
CONST_INT ciCHAR_LEGAL_MIN		0
CONST_INT ciCHAR_TV_MIN			0
CONST_INT ciCHAR_PARTY_MIN		0
CONST_INT ciCHAR_ILLEGAL_MIN	0

// Menu Column Indexes
CONST_INT CHAR_CREATOR_COL_MAIN_MENU	0	// Left menu
CONST_INT CHAR_CREATOR_COL_APP			1	// Middle menu (appearance)
CONST_INT CHAR_CREATOR_COL_HERITAGE		2	// Heritage (parents)
CONST_INT CHAR_CREATOR_COL_LIFESTYLE	3	// Lifestyle (hours)
CONST_INT CHAR_CREATOR_COL_PLAYER		4	// Right menu (character)

CONST_INT CHAR_CREATOR_MAX_HAIR_M		16//4
CONST_INT CHAR_CREATOR_MAX_HAIR_F		16//4
CONST_INT CHAR_CREATOR_MAX_HAIR_COL_M	5
CONST_INT CHAR_CREATOR_MAX_HAIR_COL_F	5

CONST_FLOAT HEIGHT_MOD_MAX 0.008
CONST_FLOAT SIDE_MOD_MAX 0.005
CONST_FLOAT SIZE_MOD_MAX 0.010

CONST_INT CHAR_CREATOR_PED_ROT	5

CONST_INT CHAR_CREATOR_MAX_HOURS 48

CONST_INT CHAR_CREATOR_MIN_AGE			21
CONST_INT CHAR_CREATOR_MAX_AGE			55
CONST_INT CHAR_CREATOR_DEFAULT_AGE		25

CONST_INT CHAR_CREATOR_MAX_BEARDS		19

CONST_INT CHAR_CREATOR_MAX_MAKEUP		16

CONST_INT DOMINANCE_UPDATE_MULTIPLIER   45

CONST_INT MAX_GRANDMOTHER 14
CONST_INT MAX_GRANDFATHER 14

CONST_INT MAX_PARENT_OPTIONS 25
CONST_INT SPECIAL_PED_THRESHOLD 5
CONST_INT UGLY_DAD_THRESHOLD 6
CONST_INT UGLY_MUM_THRESHOLD 9

CONST_INT MIN_NEXT_LOOK_AROUND_TIME		7000
CONST_INT MAX_NEXT_LOOK_AROUND_TIME		10000

CONST_INT CHAR_CREATOR_UNPAUSE_TIME 	700

TWEAK_FLOAT CHAR_CREATOR_M_DOM_MIN 0.15	//0.30
TWEAK_FLOAT CHAR_CREATOR_M_DOM_MAX 0.9	//0.85
TWEAK_FLOAT CHAR_CREATOR_F_DOM_MIN 0.1  //0.15
TWEAK_FLOAT CHAR_CREATOR_F_DOM_MAX 0.85 //0.70

TWEAK_FLOAT CHAR_CREATOR_ZOOM_MAX_X -0.01
TWEAK_FLOAT CHAR_CREATOR_ZOOM_MAX_Y 0.30
TWEAK_FLOAT CHAR_CREATOR_ZOOM_MAX_Z -0.11

TWEAK_FLOAT CHAR_CREATOR_ZOOM_MIN_X 0.0
TWEAK_FLOAT CHAR_CREATOR_ZOOM_MIN_Y -1.8
TWEAK_FLOAT CHAR_CREATOR_ZOOM_MIN_Z 0.65

TWEAK_FLOAT CHAR_CREATOR_CONFIRM_ZOOM_MAX_X 0.01
TWEAK_FLOAT CHAR_CREATOR_CONFIRM_ZOOM_MAX_Y 0.518//0.500
TWEAK_FLOAT CHAR_CREATOR_CONFIRM_ZOOM_MAX_Z -0.05//-0.145 // -0.140 

TWEAK_FLOAT CHAR_CREATOR_CONFIRM_ZOOM_MIN_X 0.01
TWEAK_FLOAT CHAR_CREATOR_CONFIRM_ZOOM_MIN_Y -0.650
TWEAK_FLOAT CHAR_CREATOR_CONFIRM_ZOOM_MIN_Z 0.355 // 0.360

TWEAK_FLOAT CHAR_CREATOR_ZOOM_SPEED 0.02

TWEAK_FLOAT CHAR_CREATOR_HEEL_OFFSET 0.087

CONST_FLOAT LIFESTYLE_ZOOM 0.49
CONST_FLOAT GENERAL_CLOSEST_ZOOM 0.11
CONST_FLOAT CREW_SHIRT_ZOOM 0.27

// ***********************************
// 		CHARACTER CREATOR ENUMS
// ***********************************

ENUM CHAR_CREATOR_GLOBAL_STATE
	CHARACTER_CREATOR_INIT,
	CHARACTER_CREATOR_WAIT_FOR_FRONTEND,
	CHARACTER_CREATOR_RUN,
	CHARACTER_CREATOR_INPUT_NAME,
	CHARACTER_CREATOR_JOINING_GTA_ONLINE,
	CHARACTER_CREATOR_WARNING_MESSAGE
ENDENUM

ENUM CHAR_CREATOR_GLOBAL_SUB_STATE
	CHARACTER_CREATOR_QUITING,
	CHARACTER_CREATOR_CONFIRMING,
	CHARACTER_CREATOR_HOUR_WARNING
ENDENUM

ENUM CHAR_CREATOR_CONFIRM_STATE
	CHARACTER_CREATOR_CONFIRM_LOADING_UI,
	CHARACTER_CREATOR_CONFIRM_DISPLAY_UI
ENDENUM

//ENUM CHAR_CREATOR_PED_CREATION_STATE
//	CHARACTER_CREATOR_PED_CREATE,
//	CHARACTER_CREATOR_PED_BLEND,
//	CHARACTER_CREATOR_PED_WAIT
//ENDENUM

// Screen column
ENUM CHAR_CREATOR_COLUMN
	MAIN_MENU_COLUMN,
	APPEARANCE_COLUMN,
	HERITAGE_COLUMN,
	LIFESTYLE_COLUMN,
	PLAYER_COLUMN,
	MAX_CHAR_COLUMNS
ENDENUM

// Main menu rows
ENUM CHAR_CREATOR_MAIN_MENU
	LIFESTYLE_CAT,
	HERITAGE_CAT,
	APPEARANCE_CAT,
	CONFIRM_CAT,
	MAX_MAIN_MENU_OPTIONS
ENDENUM

// Heritage rows
ENUM CHAR_CREATOR_HERITAGE
	HERITAGE_GENDER = 2,
	HERITAGE_MUM,
	HERITAGE_MUM2,
	HERITAGE_MUM_DOMINANCE,
	HERITAGE_DAD,
	HERITAGE_DAD2,
	HERITAGE_DAD_DOMINANCE,
	HERITAGE_DOMINANCE,
	MAX_HERITAGE
ENDENUM

// Lifestyle rows
ENUM CHAR_CREATOR_LIFESTYLE
	LIFESTYLE_SLEEP,
	LIFESTYLE_FAMILY,
	LIFESTYLE_SPORT,
	LIFESTYLE_LEGAL,
	LIFESTYLE_TV,
	LIFESTYLE_PARTY,
	LIFESTYLE_ILLEGAL,
	MAX_LIFESTYLE
ENDENUM

ENUM CHAR_CREATOR_APPEARANCE
	APPEARANCE_AGE,
	APPEARANCE_HAIR,
	APPEARANCE_HAIR_COLOUR,
	APPEARANCE_BEARD,
	APPEARANCE_MAKEUP,
	//APPEARANCE_ACCESSORIES,
	APPEARANCE_HAT,
	APPEARANCE_GLASSES,
	APPEARANCE_CREW_TSHIRT,
	APPEARANCE_CREW_DECALS,
	MAX_APPEARANCE
ENDENUM

// Archetype
ENUM CHAR_CREATOR_ARCHETYPE_ENUM
	ARCHETYPE_SKIP_LIFESTYLE = 0,
	ARCHETYPE_RANDOM,
	ARCHETYPE_GOOD,
	ARCHETYPE_BAD,
	ARCHETYPE_LAZY,
	ARCHETYPE_ACTIVE,
	ARCHETYPE_PARTY,
	ARCHETYPE_UGLY,
	ARCHETYPE_CUSTOM
ENDENUM

// Custom Archetype
ENUM CHAR_CREATOR_CUSTOM_ARCHETYPE_ENUM
	// female
	CUSTOM_ARCHETYPE_FEMALE_BUSINESS_1 = 0,
	CUSTOM_ARCHETYPE_FEMALE_BUSINESS_2,
	CUSTOM_ARCHETYPE_FEMALE_CASUAL_1,
	CUSTOM_ARCHETYPE_FEMALE_PARTY_SOUTH_AMERICAN,
	CUSTOM_ARCHETYPE_FEMALE_PARTY_AFRICAN,
	CUSTOM_ARCHETYPE_FEMALE_PARTY_PACIFIC_ISLANDER,
	CUSTOM_ARCHETYPE_FEMALE_COUCH_POTATO_MIXED_1,
	CUSTOM_ARCHETYPE_FEMALE_BUSINESS_3,
	
	CUSTOM_ARCHETYPE_FEMALE_CHOLA,
	CUSTOM_ARCHETYPE_FEMALE_CASUAL_2,
	CUSTOM_ARCHETYPE_FEMALE_CASUAL_3,
	CUSTOM_ARCHETYPE_FEMALE_PARTY_EUROPEAN,
	CUSTOM_ARCHETYPE_FEMALE_PARTY_EAST_ASIAN,
	CUSTOM_ARCHETYPE_FEMALE_COUCH_POTATO_EUROPEAN,
	CUSTOM_ARCHETYPE_FEMALE_COUCH_POTATO_MIXED_2,
	CUSTOM_ARCHETYPE_FEMALE_COUCH_POTATO_MIXED_3,
	CUSTOM_ARCHETYPE_FEMALE_CHOLA_MIXED,
	CUSTOM_ARCHETYPE_FEMALE_PARTY_HARD,
	
	// male	
	CUSTOM_ARCHETYPE_MALE_BUSINESS_1,
	CUSTOM_ARCHETYPE_MALE_CASUAL_ASIAN,
	CUSTOM_ARCHETYPE_MALE_CASUAL_1,
	
	CUSTOM_ARCHETYPE_MALE_SLOB_EUROPEAN,
	CUSTOM_ARCHETYPE_MALE_DIRTY_PARTY
ENDENUM


// hair styles
ENUM CHAR_CREATOR_HAIRSTYLE_ENUM
	HAIRSTYLE_UNIVERSAL_1 = 0,
	HAIRSTYLE_UNIVERSAL_2,
	HAIRSTYLE_UNIVERSAL_3,
	HAIRSTYLE_UNIVERSAL_4,
	HAIRSTYLE_UNIVERSAL_5,
	HAIRSTYLE_UNIVERSAL_6,
	HAIRSTYLE_CASUAL_1,
	HAIRSTYLE_CASUAL_2,
	HAIRSTYLE_CASUAL_3,
	HAIRSTYLE_ILLEGAL_1,
	HAIRSTYLE_ILLEGAL_2,
	HAIRSTYLE_ILLEGAL_3,
	HAIRSTYLE_ILLEGAL_4,
	HAIRSTYLE_LEGAL_1,
	HAIRSTYLE_LEGAL_2,
	HAIRSTYLE_LEGAL_3,
	HAIRSTYLE_LEGAL_4,
	HAIRSTYLE_PARTY_1,
	HAIRSTYLE_PARTY_2,
	HAIRSTYLE_PARTY_3,
	HAIRSTYLE_PARTY_4,
	HAIRSTYLE_SPORTY_1,
	HAIRSTYLE_SPORTY_2,
	HAIRSTYLE_SPORTY_3,
	HAIRSTYLE_LAZY_1,
	HAIRSTYLE_LAZY_2,
	HAIRSTYLE_LAZY_3
ENDENUM

ENUM CHAR_CREATOR_COMPLEXION_ENUM
	COMPLEXION_DARK = 0,
	COMPLEXION_MEDIUM,
	COMPLEXION_FAIR
ENDENUM

// Player stats
ENUM CHAR_CREATOR_STATS
	CHAR_CREATOR_STAT_STAMINA = 0,
	CHAR_CREATOR_STAT_SHOOTING,
	CHAR_CREATOR_STAT_STRENGTH,
	CHAR_CREATOR_STAT_STEALTH,
	CHAR_CREATOR_STAT_FLYING,
	CHAR_CREATOR_STAT_BIKE,
	CHAR_CREATOR_STAT_LUNG,
	
	CHAR_CREATOR_STAT_TOTAL
ENDENUM

// Peds used on character creator
ENUM CHAR_CREATOR_PED
	CHAR_CREATOR_PED_SON,
	CHAR_CREATOR_PED_DAUGHTER,
	CHAR_CREATOR_PED_DAD,
	CHAR_CREATOR_PED_MUM,
	CHAR_CREATOR_PED_GRANDMA_M,
	CHAR_CREATOR_PED_GRANDDAD_M,
	CHAR_CREATOR_PED_GRANDMA_D,
	CHAR_CREATOR_PED_GRANDDAD_D,
	CHAR_CREATOR_PED_SON_DUMMY,
	CHAR_CREATOR_PED_DAUGHTER_DUMMY,
	
	CHAR_CREATOR_PED_MAX
ENDENUM

CONST_INT CHAR_CREATOR_SPECIAL_PEDS_MAX	4

// Peds gender
ENUM CHAR_CREATOR_GENDER
	CHAR_CREATOR_GENDER_MALE,
	CHAR_CREATOR_GENDER_FEMALE
ENDENUM

ENUM CHAR_CREATOR_OVERLAY
	CHAR_CREATOR_BLEMISH = 0,
	CHAR_CREATOR_BEARD,
	CHAR_CREATOR_EYEBROW,
	CHAR_CREATOR_WRINKLES,
	CHAR_CREATOR_MAKEUP,
	CHAR_CREATOR_DAMAGE,
	CHAR_CREATOR_BASE_BLEMISH,
	CHAR_CREATOR_MAX_OVERLAYS
ENDENUM

ENUM CHAR_CREATOR_MOOD
	CHAR_CREATOR_HAPPY = 2,
	CHAR_CREATOR_NORMAL = 4,
	CHAR_CREATOR_SMUG = 6,
	CHAR_CREATOR_SAD = 7,
	CHAR_CREATOR_ANGRY = 1
ENDENUM

ENUM CHAR_CREATOR_CREW_DECAL_OPTION_ENUM
	CREW_DECAL_NONE = 0,
	CREW_DECAL_BIG,
	CREW_DECAL_SMALL,
	CREW_DECAL_BACK
ENDENUM

ENUM LAST_RACE_DIRECTION_ENUM
	LAST_RACE_DIRECTION_NONE = 0,
	LAST_RACE_DIRECTION_LEFT,
	LAST_RACE_DIRECTION_RIGHT
ENDENUM

// ***********************************
// 		CHARACTER CREATOR STRUCTS
// ***********************************

STRUCT CHAR_CREATOR_OVERLAY_INFO
	INT		iOverlayVal = -1
	FLOAT 	fOverlayBlend = 0.0
ENDSTRUCT

STRUCT CHAR_CREATOR_PARENT_INFO
	CHAR_CREATOR_RACE	charDadRace			// Race of Dad
	CHAR_CREATOR_RACE	charMumRace			// Race of Mum
	
	CHAR_CREATOR_RACE	charDadRace2		// Race of Dad secondary head
	CHAR_CREATOR_RACE	charMumRace2		// Race of Mum secondary head
	
	INT					iDadSpecialHeadIndex	// Special Dad Head
	INT					iMumSpecialHeadIndex	// Special Mum Head
	
	INT					iMumHair	// Hair of Mum	
	INT					iDadHair	// Hair of Dad
	
	INT					iDadHairColour
	INT					iMumHairColour
	
	FLOAT				fMumHeightMod
	FLOAT 				fMumSizeMod
	FLOAT 				fMumSideMod
	
	FLOAT				fDadHeightMod
	FLOAT				fDadSizeMod
	FLOAT				fDadSideMod
	
	INT					iDadHeadVariation
	INT					iDadHeadVariation2
	INT					iMumHeadVariation
	INT					iMumHeadVariation2			
	
	INT 				iDadBeard
	INT					iMumMakeup
ENDSTRUCT

STRUCT CHAR_CREATOR_SHARED_PED_DATA
	CHAR_CREATOR_GENDER			charGender										// Current gender of ped
	CHAR_CREATOR_ARCHETYPE_ENUM	archetype										// Last generated archetype
	CHAR_CREATOR_CUSTOM_ARCHETYPE_ENUM customArchetype							// Last generated custom archetype
	
	FLOAT 						fParentDominance = 50.0							// Parental Dominance
	FLOAT 						fMumDominance = 50.0							// Parental Dominance - mum's side
	FLOAT 						fDadDominance = 50.0							// Parental Dominance - dad's side
	
	FLOAT						fParentUgly										// How Ugly are we in range [0,1]
	
	INT							iCharAge = CHAR_CREATOR_MIN_AGE					// Age of child
	
	CHAR_CREATOR_OVERLAY_INFO	charOverlays[CHAR_CREATOR_MAX_OVERLAYS]			// Our overlay values
	INT							iSonEyebrow
	INT							iDaughterEyebrow

	CHAR_CREATOR_PARENT_INFO	charCreatorParentInfo
	
	CHAR_CREATOR_MOOD			charMood										// Mood of the player
	
	INT							iSonHair										// Hair style of son
	INT							iSonHairColour									// Colour of son's hair
	INT							iDaughterHair									// Hair style of daughter
	INT							iDaughterHairColour								// Colour of daughter's hair
	
	INT							iHoursLeftToAssign								// The hours we have assigned (24)
	INT							charLifestyleHours[MAX_LIFESTYLE]				// The current hours for each lifestyle
	INT							charLifestyleStats[CHAR_CREATOR_STAT_TOTAL]		// The current values of stats
	
	INT							iMumRotation									// Rotation amount for peds
	INT							iDadRotation
	INT							iChildRotation
	
	FLOAT 						fCharTurnValueX									// Turn head value
	FLOAT 						fCharTurnValueY
	
	INT							iCurrentStatUpgrades[CHAR_CREATOR_STAT_TOTAL]	// Keeps record of our updated stats 
	
	MP_SPAWN_OUTFIT_TYPE		charOutfit										// Outfit to dress in
	CHAR_CREATOR_HAIRSTYLE_ENUM	lastFemaleHair
ENDSTRUCT


// Main STRUCT for Character Creator
STRUCT MPHUD_CHARACTER_CREATOR_DATA
	CHAR_CREATOR_GLOBAL_STATE 			charCreatorState						// Global state of screen
	CHAR_CREATOR_PED_CREATION_STATE		charCreatorPedState						// State to load and create initial peds
	CHAR_CREATOR_GLOBAL_SUB_STATE		charCreatorSubState						// Substate to know if we are progressing or quitting
	CHAR_CREATOR_CONFIRM_STATE			charCreatorConfirmState					// State of confirmation screen
	
	INT									iCharacterSelectSlot
	
	INT									iCharacterPedCreationIndex				// Index for creating peds at beginning
	
	CHAR_CREATOR_COLUMN					columnIndex								// Index of column player is in
	CHAR_CREATOR_MAIN_MENU				prevMainMenuIndex						// Index for the main menu on the left
	CHAR_CREATOR_MAIN_MENU				mainMenuIndex							// Index for the main menu on the left
	CHAR_CREATOR_HERITAGE				heritageIndex							// Index for Heritage column
	CHAR_CREATOR_LIFESTYLE				lifestyleIndex							// Index for Lifestyle column
	CHAR_CREATOR_APPEARANCE				appearanceIndex							// Index for appearance column
	
	//CHAR_CREATOR_CACHED_PARENT			cachedMum[MAX_PARENT_OPTIONS]
	//CHAR_CREATOR_CACHED_PARENT			cachedDad[MAX_PARENT_OPTIONS]
	
	CHAR_CREATOR_HERITAGE				cachedMumHeritageIndex							// Index for Heritage column - back up last one accessed
	CHAR_CREATOR_HERITAGE				cachedDadHeritageIndex							// Index for Heritage column - back up last one accessed
	
	CHAR_CREATOR_CREW_TSHIRT_OPTION_ENUM crewTShirtOption
	CHAR_CREATOR_CREW_DECAL_OPTION_ENUM	 crewDecalOption
	
	PED_INDEX							charCreatorPeds[CHAR_CREATOR_PED_MAX]	// Array of all peds used on creator
	
	PED_INDEX							charCreatorSpecialPeds[CHAR_CREATOR_SPECIAL_PEDS_MAX]	// Array of our special peds
	
	CHAR_CREATOR_SHARED_PED_DATA		pedData									// Common ped data for child
	
	PED_COMP_NAME_ENUM					savedJbib								// save jbib entering for 2nd chance
	PED_COMP_NAME_ENUM					savedSpecial							// save special entering for 2nd chance
	BOOL								bEnteredAsMale							// did we enter as male for 2nd chance
	TATTOO_NAME_ENUM					enteredCrewDecalOption					// did we have a crew decal on the way in
	
	PED_COMP_OUTFIT_DATA_STRUCT			cacheMaleOutfitData[6]							// used to backup outfit
	PED_COMP_OUTFIT_DATA_STRUCT			cacheFemaleOutfitData[6]						// used to backup outfit
	
	INT 								iCacheMaleHatOption[6]
	INT									iCacheMaleGlassesOption[6]
	INT 								iCacheFemaleHatOption[6]
	INT									iCacheFemaleGlassesOption[6]	
	
	INT									iCacheMaleHair[6]
	INT									iCacheMaleHairColour[6]
	INT									iCacheFemaleHair[6]
	INT									iCacheFemaleHairColour[6]
	
	INT									iCacheMaleBeard[6]
	INT									iCacheFemaleMakeup[6]
	
	BOOL								bLockedMaleLifestyle[6]
	BOOL								bLockedFemaleLifestyle[6]
	
	INT 								iHeadForDadRace
	INT 								iHeadForDadRace2
	INT									iHeadForMumRace
	INT									iHeadForMumRace2
	
	INT iCacheGPSil1 
	INT iCacheGPSil2 
	INT iCacheGPSil3
	INT iCacheGPSil4
	INT iCacheGPHighlight 
	
	INT									charCreatorTimer						// Timer used for input
	INT									iWaitForSpinnersCounter					// count frames to get rid of spinners
	INT									iNextLookAroundTime						// the next time to have the ped look around on his own
	INT									iUnpauseTime
	
	INT									iCharCreatorBitSet						// Bitset used for several checks
	INT									iCharCreatorButtons						// Button bitset to handle player input
	INT									iCharCreatorStatBitSet					// BitSet of our lates stat updates (those coloured yellow)
	
	INT									CHAR_COL1_ID							// DEFAULT IDS NEEDED FOR SCALEFORM
	INT									CHAR_COL2_ID
	INT									CHAR_COL3_ID
	
	FLOAT								fCharZoom								// Allow you to zoom in and out of ped
	FLOAT 								fCacheCharZoom
	VECTOR								vCharOffset
	
	BOOL 								bInitialisedOutfit						// if not set then force an update on the outfit soon as the child is created
	BOOL								bForcingLookAround
	BOOL								bHasMadeAlteration
	BOOL								bSonLockedToOldOutfit
	BOOL								bDaughterLockedToOldOutfit
	BOOL								bEnteredWithCrewTShirt
	BOOL								bCreatedFirstPrettyChild
	
	CHAR_CREATOR_RACE					backupMumRace
	CHAR_CREATOR_RACE					backupDadRace
	
	INT									iSetSonDummyTime
	INT									iSetDaughterDummyTime
	
	INT									iSoundID
	BOOL								bZoomingSound
	
	SCALEFORM_INDEX						sfForeground
	SCALEFORM_INDEX						sfBackground
	SCALEFORM_INDEX						sfCelebration
	
	INT 								iCacheDadHair[MAX_GRANDFATHER]
	INT 								iCacheDadHairColour[MAX_GRANDFATHER]
	FLOAT 								fCacheDadSizeMod[MAX_GRANDFATHER]
	FLOAT 								fCacheDadSideMod[MAX_GRANDFATHER]
	FLOAT 								fCacheDadHeightMod[MAX_GRANDFATHER]
	INT 								iCacheDadBeard[MAX_GRANDFATHER]
	
	INT 								iCacheMumHair[MAX_GRANDMOTHER]
	INT 								iCacheMumHairColour[MAX_GRANDMOTHER]
	FLOAT 								fCacheMumSizeMod[MAX_GRANDMOTHER]
	FLOAT 								fCacheMumSideMod[MAX_GRANDMOTHER]
	FLOAT 								fCacheMumHeightMod[MAX_GRANDMOTHER]
	INT 								iCacheMumMakeup[MAX_GRANDMOTHER]
	
	// cache last random stuff
	CHAR_CREATOR_HAIRSTYLE_ENUM 		lastMaleHair
	CHAR_CREATOR_HAIRSTYLE_ENUM 		lastFemaleHair
	CHAR_CREATOR_CUSTOM_ARCHETYPE_ENUM	lastMaleCustomArchetype
	CHAR_CREATOR_CUSTOM_ARCHETYPE_ENUM	lastFemaleCustomArchetype
	INT									iLastRandomMaleHairColour
	INT									iLastRandomFemaleHairColour
	INT 								iLastRandomBeard
	INT 								iLastRandomMakeup
	INT									iLastRandomMum1
	INT									iLastRandomMum2
	INT									iLastRandomDad1
	INT									iLastRandomDad2
	INT									iLastRandomLifestyle
	INT									iLastRandomMumHair
	INT									iLastRandomDadHair
	
	INT									iNumberSinceUglyDadGenerated
	INT									iNumberSinceSpecialDadGenerated
	
	INT									iNumberSinceUglyMumGenerated
	INT									iNumberSinceSpecialMumGenerated
	
	BOOL								bDisplayedGrandparents
	
	
	#IF IS_DEBUG_BUILD
	BOOL 						bDebugInfo
	INT							iWrinklesPercent
	INT							iDamagePercent
	INT							iBasePercent
	
	INT 						iSonLoadN
	INT							iDaughterLoadN
	INT							iMumLoadN
	INT							iDadLoadN
	
	BOOL						bSonLoadingN
	BOOL						bDaughterLoadingN
	BOOL						bMumLoadingN
	BOOL						bDadLoadingN
	
	WIDGET_GROUP_ID				widgetGroup
	BOOL 						bZoomOut
	BOOL 						bDontDrawPeds
	
	BOOL						bRemovePedsHair
	BOOL						bCurrentHairStatus
	
	FLOAT						fDebugHeadBlend
	BOOL						bTriggerBlend
	
	BOOL						bFakeCollectorEdition
	BOOL						bFakeSocialClub

	VECTOR						vMumOffset
	VECTOR						vDadOffset
	VECTOR						vMumRot
	VECTOR						vDadRot

	#ENDIF
ENDSTRUCT


// ************************************
// 		CHARACTER CREATOR COMMANDS
// ************************************

/// PURPOSE: Checks to see if the special heads associated with collectors / special editions are available to player
FUNC BOOL ARE_CHAR_CREATOR_COLLECTOR_EDITIONS_AVAILABLE()

	IF IS_COLLECTORS_EDITION_GAME()
	OR IS_JAPANESE_SPECIAL_EDITION_GAME()
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL IS_CHAR_CREATOR_PED_OK(PED_INDEX aPed)

	IF DOES_ENTITY_EXIST(aPed)
	AND NOT IS_ENTITY_DEAD(aPed)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC CLEAR_CACHED_PARENTS(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData, BOOL bMale)
	INT i
	
	IF NOT bMale
		REPEAT MAX_GRANDMOTHER i 
			charCreatorData.iCacheMumHair[i] = -1
			charCreatorData.iCacheMumHairColour[i] = -1
			charCreatorData.fCacheMumSizeMod[i] = -1
			charCreatorData.fCacheMumHeightMod[i] = -1
			charCreatorData.iCacheMumMakeup[i] = -1
		ENDREPEAT
	ELSE
		REPEAT MAX_GRANDFATHER i 
			charCreatorData.iCacheDadHair[i] = -1
			charCreatorData.iCacheDadHairColour[i] = -1
			charCreatorData.fCacheDadSizeMod[i] = -1
			charCreatorData.fCacheDadHeightMod[i] = -1
			charCreatorData.iCacheDadBeard[i] = -1
		ENDREPEAT
	ENDIF
ENDPROC

/*
// fill cached parents
PROC FILL_CACHED_PARENTS(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData)
	INT iSlot 

	// mum
	iSlot = charCreatorData.iMumOption
	IF NOT charCreatorData.cachedMum[iSlot].bFilled
		charCreatorData.cachedMum[iSlot].race = charCreatorData.pedData.charCreatorParentInfo.charMumRace
		charCreatorData.cachedMum[iSlot].race2 = charCreatorData.pedData.charCreatorParentInfo.charMumRace2
		charCreatorData.cachedMum[iSlot].iSpecialHeadIndex = charCreatorData.pedData.charCreatorParentInfo.iMumSpecialHeadIndex
		charCreatorData.cachedMum[iSlot].fDominance = charCreatorData.pedData.fMumDominance
		charCreatorData.cachedMum[iSlot].iHair = charCreatorData.pedData.charCreatorParentInfo.iMumHair 
		charCreatorData.cachedMum[iSlot].iRaceHead = charCreatorData.pedData.charCreatorParentInfo.iMumHeadVariation
		charCreatorData.cachedMum[iSlot].iRaceHead2 = charCreatorData.pedData.charCreatorParentInfo.iMumHeadVariation2
		charCreatorData.cachedMum[iSlot].bFilled = TRUE
	ENDIF
	
	// dad
	iSlot = charCreatorData.iDadOption
	IF NOT charCreatorData.cachedDad[iSlot].bFilled
		charCreatorData.cachedDad[iSlot].race = charCreatorData.pedData.charCreatorParentInfo.charDadRace
		charCreatorData.cachedDad[iSlot].race2 = charCreatorData.pedData.charCreatorParentInfo.charDadRace2
		charCreatorData.cachedDad[iSlot].iSpecialHeadIndex = charCreatorData.pedData.charCreatorParentInfo.iDadSpecialHeadIndex
		charCreatorData.cachedDad[iSlot].fDominance = charCreatorData.pedData.fDadDominance	
		charCreatorData.cachedDad[iSlot].iHair = charCreatorData.pedData.charCreatorParentInfo.iDadHair 
		charCreatorData.cachedDad[iSlot].iFacial = charCreatorData.pedData.charCreatorParentInfo.iDadBeard
		charCreatorData.cachedDad[iSlot].iRaceHead = charCreatorData.pedData.charCreatorParentInfo.iDadHeadVariation
		charCreatorData.cachedDad[iSlot].iRaceHead2 = charCreatorData.pedData.charCreatorParentInfo.iDadHeadVariation2
		charCreatorData.cachedDad[iSlot].bFilled = TRUE
	ENDIF
ENDPROC
*/

/// PURPOSE: Returns 0 if MALE otherwise 1
FUNC CHAR_CREATOR_GENDER GET_CHAR_CREATOR_GENDER_OF_PED(CHAR_CREATOR_PED aPed)
	SWITCH aPed
		CASE CHAR_CREATOR_PED_SON
		CASE CHAR_CREATOR_PED_SON_DUMMY
		CASE CHAR_CREATOR_PED_DAD
		CASE CHAR_CREATOR_PED_GRANDDAD_M
		CASE CHAR_CREATOR_PED_GRANDDAD_D
			RETURN CHAR_CREATOR_GENDER_MALE
		CASE CHAR_CREATOR_PED_DAUGHTER
		CASE CHAR_CREATOR_PED_DAUGHTER_DUMMY
		CASE CHAR_CREATOR_PED_MUM
		CASE CHAR_CREATOR_PED_GRANDMA_M
		CASE CHAR_CREATOR_PED_GRANDMA_D
			RETURN CHAR_CREATOR_GENDER_FEMALE
	ENDSWITCH
	
	RETURN CHAR_CREATOR_GENDER_MALE
ENDFUNC

/// PURPOSE: Sets the hours for a lifestyle
PROC SET_CHAR_CREATOR_LIFESTYLE_HOURS(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData, CHAR_CREATOR_LIFESTYLE creatorLifestyle, INT iHours)
	charCreatorData.pedData.charLifestyleHours[ENUM_TO_INT(creatorLifestyle)] = iHours
ENDPROC


/// PURPOSE: Returns the number of hours for passed lifestyle
FUNC INT GET_CHAR_CREATOR_LIFESTYLE_HOURS(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData, CHAR_CREATOR_LIFESTYLE creatorLifestyle)
	RETURN charCreatorData.pedData.charLifestyleHours[ENUM_TO_INT(creatorLifestyle)]
ENDFUNC


/// PURPOSE: Sets the current gender of the creator screen
PROC SET_CHAR_CREATOR_GENDER(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData, CHAR_CREATOR_GENDER aGender)
	charCreatorData.pedData.charGender = aGender
ENDPROC

/// PURPOSE: Sets the current gender of the creator screen
FUNC CHAR_CREATOR_GENDER GET_CHAR_CREATOR_GENDER(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData)
	RETURN charCreatorData.pedData.charGender
ENDFUNC

/// PURPOSE: Returns TRUE if the ped we are creating is MALE
FUNC BOOL IS_CHAR_CREATOR_PED_MALE(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData)
	RETURN (charCreatorData.pedData.charGender = CHAR_CREATOR_GENDER_MALE)
ENDFUNC

/// PURPOSE: Sets the current gender of the creator screen
PROC SET_CHAR_CREATOR_AGE(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData, INT iAge)
	 charCreatorData.pedData.iCharAge = iAge
ENDPROC

/// PURPOSE: Returns TRUE if the ped we are creating is MALE
FUNC INT GET_CHAR_CREATOR_AGE(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData)
	RETURN charCreatorData.pedData.iCharAge
ENDFUNC


FUNC CHAR_CREATOR_LIFESTYLE GET_MAX_LIFESTYLE(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData)
	INT i
	INT iMaxHours = -1
	CHAR_CREATOR_LIFESTYLE maxLifestyle
	
	REPEAT MAX_LIFESTYLE i
		IF INT_TO_ENUM(CHAR_CREATOR_LIFESTYLE, i) != LIFESTYLE_SLEEP
			IF iMaxHours < GET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, INT_TO_ENUM(CHAR_CREATOR_LIFESTYLE, i))
				
				iMaxHours = GET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, INT_TO_ENUM(CHAR_CREATOR_LIFESTYLE, i))
				maxLifestyle = INT_TO_ENUM(CHAR_CREATOR_LIFESTYLE, i)
				
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN maxLifestyle
ENDFUNC


/// PURPOSE: Sets the hair of the children on character creator
PROC SET_CHAR_CREATOR_HAIR(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData, CHAR_CREATOR_GENDER aGender, INT iHair)
	CHAR_CREATOR_LIFESTYLE maxLifestyle = GET_MAX_LIFESTYLE(charCreatorData)
	INT iLockLifestyleSlot = ENUM_TO_INT(maxLifestyle) - 1			
	
	IF aGender = CHAR_CREATOR_GENDER_MALE
		charCreatorData.pedData.iSonHair = iHair
		charCreatorData.iCacheMaleHair[iLockLifestyleSlot] = iHair
	ELSE
		charCreatorData.pedData.iDaughterHair = iHair
		charCreatorData.iCacheFemaleHair[iLockLifestyleSlot] = iHair
	ENDIF
ENDPROC

/// PURPOSE: Sets the hair of the children on character creator
PROC SET_CHAR_CREATOR_HAIR_COLOUR(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData, CHAR_CREATOR_GENDER aGender, INT iHairColour)
	CHAR_CREATOR_LIFESTYLE maxLifestyle = GET_MAX_LIFESTYLE(charCreatorData)
	INT iLockLifestyleSlot = ENUM_TO_INT(maxLifestyle) - 1		
	
	IF aGender = CHAR_CREATOR_GENDER_MALE
		charCreatorData.pedData.iSonHairColour = iHairColour
		charCreatorData.iCacheMaleHairColour[iLockLifestyleSlot] = iHairColour
	ELSE
		charCreatorData.pedData.iDaughterHairColour = iHairColour
		charCreatorData.iCacheFemaleHairColour[iLockLifestyleSlot] = iHairColour
	ENDIF
ENDPROC

/// PURPOSE: Randomises our parent dominance
PROC SET_CHAR_CREATOR_RANDOM_PARENT_DOM(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData, BOOL bRandomiseGrandparents = TRUE, BOOL bFromMumSlider = FALSE, BOOL bFromDadSlider = FALSE)
	IF bRandomiseGrandparents
		charCreatorData.pedData.fMumDominance = GET_RANDOM_FLOAT_IN_RANGE(0, 101)
		charCreatorData.pedData.fDadDominance = GET_RANDOM_FLOAT_IN_RANGE(0, 101)
		
		// archetype specific!
		IF charCreatorData.pedData.archetype = ARCHETYPE_CUSTOM
		AND NOT bFromMumSlider
		AND NOT bFromDadSlider
			SWITCH charCreatorData.pedData.customArchetype
				// female
				CASE CUSTOM_ARCHETYPE_FEMALE_CHOLA
					charCreatorData.pedData.fMumDominance = 46
					charCreatorData.pedData.fDadDominance = 65
				BREAK
				CASE CUSTOM_ARCHETYPE_FEMALE_BUSINESS_1
					charCreatorData.pedData.fMumDominance = 72
					charCreatorData.pedData.fDadDominance = 64
				BREAK
				CASE CUSTOM_ARCHETYPE_FEMALE_BUSINESS_2
					charCreatorData.pedData.fMumDominance = 25
					charCreatorData.pedData.fDadDominance = 29
				BREAK
				CASE CUSTOM_ARCHETYPE_FEMALE_CASUAL_1
					charCreatorData.pedData.fMumDominance = 100
					charCreatorData.pedData.fDadDominance = 100
				BREAK	
				CASE CUSTOM_ARCHETYPE_FEMALE_CASUAL_2
					charCreatorData.pedData.fMumDominance = 100
					charCreatorData.pedData.fDadDominance = 0
				BREAK	
				CASE CUSTOM_ARCHETYPE_FEMALE_PARTY_SOUTH_AMERICAN
					charCreatorData.pedData.fMumDominance = 0
					charCreatorData.pedData.fDadDominance = 0
				BREAK	
				CASE CUSTOM_ARCHETYPE_FEMALE_CASUAL_3
					charCreatorData.pedData.fMumDominance = 100
					charCreatorData.pedData.fDadDominance = 60
				BREAK	
				CASE CUSTOM_ARCHETYPE_FEMALE_PARTY_EUROPEAN
					charCreatorData.pedData.fMumDominance = 0
					charCreatorData.pedData.fDadDominance = 0
				BREAK	
				CASE CUSTOM_ARCHETYPE_FEMALE_PARTY_EAST_ASIAN
					charCreatorData.pedData.fMumDominance = 0
					charCreatorData.pedData.fDadDominance = 0
				BREAK	
				CASE CUSTOM_ARCHETYPE_FEMALE_PARTY_AFRICAN
					charCreatorData.pedData.fMumDominance = 0
					charCreatorData.pedData.fDadDominance = 0
				BREAK
				CASE CUSTOM_ARCHETYPE_FEMALE_PARTY_PACIFIC_ISLANDER
					charCreatorData.pedData.fMumDominance = 0
					charCreatorData.pedData.fDadDominance = 0
				BREAK	
				CASE CUSTOM_ARCHETYPE_FEMALE_COUCH_POTATO_EUROPEAN
					charCreatorData.pedData.fMumDominance = 0
					charCreatorData.pedData.fDadDominance = 0
				BREAK	
				CASE CUSTOM_ARCHETYPE_FEMALE_COUCH_POTATO_MIXED_1
					charCreatorData.pedData.fMumDominance = 65
					charCreatorData.pedData.fDadDominance = 0
				BREAK	
				CASE CUSTOM_ARCHETYPE_FEMALE_COUCH_POTATO_MIXED_2
					charCreatorData.pedData.fMumDominance = 40
					charCreatorData.pedData.fDadDominance = 40
				BREAK	
				CASE CUSTOM_ARCHETYPE_FEMALE_COUCH_POTATO_MIXED_3
					charCreatorData.pedData.fMumDominance = 40
					charCreatorData.pedData.fDadDominance = 40
				BREAK		
				CASE CUSTOM_ARCHETYPE_FEMALE_CHOLA_MIXED
					charCreatorData.pedData.fMumDominance = 57
					charCreatorData.pedData.fDadDominance = 80
				BREAK		
				CASE CUSTOM_ARCHETYPE_FEMALE_BUSINESS_3
					charCreatorData.pedData.fMumDominance = 23
					charCreatorData.pedData.fDadDominance = 15
				BREAK		
				CASE CUSTOM_ARCHETYPE_FEMALE_PARTY_HARD
					charCreatorData.pedData.fMumDominance = 75
					charCreatorData.pedData.fDadDominance = 12
				BREAK	
				
				// male
				CASE CUSTOM_ARCHETYPE_MALE_BUSINESS_1
					charCreatorData.pedData.fMumDominance = 50
					charCreatorData.pedData.fDadDominance = 41
				BREAK
				CASE CUSTOM_ARCHETYPE_MALE_CASUAL_ASIAN
					charCreatorData.pedData.fMumDominance = 100
					charCreatorData.pedData.fDadDominance = 100
				BREAK
				CASE CUSTOM_ARCHETYPE_MALE_SLOB_EUROPEAN
					charCreatorData.pedData.fMumDominance = 0
					charCreatorData.pedData.fDadDominance = 100
				BREAK
				CASE CUSTOM_ARCHETYPE_MALE_CASUAL_1
					charCreatorData.pedData.fMumDominance = 15
					charCreatorData.pedData.fDadDominance = 100
				BREAK
				CASE CUSTOM_ARCHETYPE_MALE_DIRTY_PARTY
					charCreatorData.pedData.fMumDominance = 30
					charCreatorData.pedData.fDadDominance = 8
				BREAK
			ENDSWITCH
		ENDIF
	ENDIF

	IF NOT bFromMumSlider
	AND NOT bFromDadSlider
		IF IS_CHAR_CREATOR_PED_MALE(charCreatorData)
			charCreatorData.pedData.fParentDominance = GET_RANDOM_FLOAT_IN_RANGE(75, 95)
			
			IF charCreatorData.pedData.archetype = ARCHETYPE_CUSTOM
				SWITCH charCreatorData.pedData.customArchetype
					// male
					CASE CUSTOM_ARCHETYPE_MALE_BUSINESS_1
						charCreatorData.pedData.fParentDominance = 100
					BREAK
					CASE CUSTOM_ARCHETYPE_MALE_CASUAL_ASIAN
						charCreatorData.pedData.fParentDominance = 100
					BREAK
					CASE CUSTOM_ARCHETYPE_MALE_SLOB_EUROPEAN
						charCreatorData.pedData.fParentDominance = 100
					BREAK
					CASE CUSTOM_ARCHETYPE_MALE_CASUAL_1
						charCreatorData.pedData.fParentDominance = 100
					BREAK
					CASE CUSTOM_ARCHETYPE_MALE_DIRTY_PARTY
						charCreatorData.pedData.fParentDominance = 100
					BREAK
				ENDSWITCH
			ENDIF			
		ELSE
			charCreatorData.pedData.fParentDominance = GET_RANDOM_FLOAT_IN_RANGE(5, 26)
			
			IF charCreatorData.pedData.archetype = ARCHETYPE_CUSTOM
				SWITCH charCreatorData.pedData.customArchetype
					// female
					CASE CUSTOM_ARCHETYPE_FEMALE_CHOLA
						charCreatorData.pedData.fParentDominance = 0
					BREAK
					CASE CUSTOM_ARCHETYPE_FEMALE_BUSINESS_1
						charCreatorData.pedData.fParentDominance = 0
					BREAK
					CASE CUSTOM_ARCHETYPE_FEMALE_BUSINESS_2
						charCreatorData.pedData.fParentDominance = 0
					BREAK
					CASE CUSTOM_ARCHETYPE_FEMALE_CASUAL_1
						charCreatorData.pedData.fParentDominance = 0
					BREAK	
					CASE CUSTOM_ARCHETYPE_FEMALE_CASUAL_2
						charCreatorData.pedData.fParentDominance = 0
					BREAK	
					CASE CUSTOM_ARCHETYPE_FEMALE_PARTY_SOUTH_AMERICAN
						charCreatorData.pedData.fParentDominance = 0
					BREAK	
					CASE CUSTOM_ARCHETYPE_FEMALE_CASUAL_3
						charCreatorData.pedData.fParentDominance = 30
					BREAK	
					CASE CUSTOM_ARCHETYPE_FEMALE_PARTY_EUROPEAN
						charCreatorData.pedData.fParentDominance = 0
					BREAK
					CASE CUSTOM_ARCHETYPE_FEMALE_PARTY_EAST_ASIAN
						charCreatorData.pedData.fParentDominance = 0
					BREAK
					CASE CUSTOM_ARCHETYPE_FEMALE_PARTY_AFRICAN
						charCreatorData.pedData.fParentDominance = 0
					BREAK
					CASE CUSTOM_ARCHETYPE_FEMALE_PARTY_PACIFIC_ISLANDER
						charCreatorData.pedData.fParentDominance = 0
					BREAK
					CASE CUSTOM_ARCHETYPE_FEMALE_COUCH_POTATO_EUROPEAN
						charCreatorData.pedData.fParentDominance = 0
					BREAK
					CASE CUSTOM_ARCHETYPE_FEMALE_COUCH_POTATO_MIXED_1
						charCreatorData.pedData.fParentDominance = 10
					BREAK
					CASE CUSTOM_ARCHETYPE_FEMALE_COUCH_POTATO_MIXED_2
						charCreatorData.pedData.fParentDominance = 0
					BREAK
					CASE CUSTOM_ARCHETYPE_FEMALE_COUCH_POTATO_MIXED_3
						charCreatorData.pedData.fParentDominance = 14
					BREAK
					CASE CUSTOM_ARCHETYPE_FEMALE_CHOLA_MIXED
						charCreatorData.pedData.fParentDominance = 53
					BREAK
					CASE CUSTOM_ARCHETYPE_FEMALE_BUSINESS_3
						charCreatorData.pedData.fParentDominance = 0
					BREAK
					CASE CUSTOM_ARCHETYPE_FEMALE_PARTY_HARD
						charCreatorData.pedData.fParentDominance = 0
					BREAK
				ENDSWITCH
			ENDIF
		ENDIF
	ENDIF	

ENDPROC

/// PURPOSE: Sets the hair of the children on character creator
FUNC INT GET_CHAR_CREATOR_HAIR(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData, CHAR_CREATOR_GENDER aGender)
	IF aGender = CHAR_CREATOR_GENDER_MALE
		RETURN charCreatorData.pedData.iSonHair
	ELSE
		RETURN charCreatorData.pedData.iDaughterHair
	ENDIF
ENDFUNC

/// PURPOSE: Sets the hair of the children on character creator
FUNC INT GET_CHAR_CREATOR_HAIR_COLOUR(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData, CHAR_CREATOR_GENDER aGender)
	IF aGender = CHAR_CREATOR_GENDER_MALE
		RETURN charCreatorData.pedData.iSonHairColour
	ELSE
		RETURN charCreatorData.pedData.iDaughterHairColour
	ENDIF
ENDFUNC

FUNC INT GET_CHAR_CREATOR_PARENT_SPECIAL_HEAD(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData, CHAR_CREATOR_PED aChar)
	IF aChar = CHAR_CREATOR_PED_DAD
		RETURN GET_PED_HEAD_BLEND_FIRST_INDEX(HBHT_UNIQUE_MALE) + charCreatorData.pedData.charCreatorParentInfo.iDadSpecialHeadIndex
	ELSE
		RETURN GET_PED_HEAD_BLEND_FIRST_INDEX(HBHT_UNIQUE_FEMALE) + charCreatorData.pedData.charCreatorParentInfo.iMumSpecialHeadIndex
	ENDIF
ENDFUNC

FUNC INT GET_CHAR_CREATOR_PARENT_SPECIAL_HEAD_INDEX(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData, CHAR_CREATOR_PED aChar)
	IF aChar = CHAR_CREATOR_PED_DAD
		RETURN charCreatorData.pedData.charCreatorParentInfo.iDadSpecialHeadIndex
	ELSE
		RETURN charCreatorData.pedData.charCreatorParentInfo.iMumSpecialHeadIndex
	ENDIF
ENDFUNC

PROC SET_CHAR_CREATOR_PARENT_SPECIAL_HEAD(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData, CHAR_CREATOR_PED aChar, INT iSpecialHead)
	IF aChar = CHAR_CREATOR_PED_DAD
		charCreatorData.pedData.charCreatorParentInfo.iDadSpecialHeadIndex = iSpecialHead
	ELSE
		charCreatorData.pedData.charCreatorParentInfo.iMumSpecialHeadIndex = iSpecialHead
	ENDIF
ENDPROC

FUNC BOOL IS_CHAR_CREATOR_PARENT_SPECIAL_HEAD(CHAR_CREATOR_RACE aCharRace)
	RETURN (aCharRace = CHAR_CREATOR_SPECIAL_RACE)
ENDFUNC

/// PURPOSE: Returns the race of the parent
FUNC CHAR_CREATOR_RACE GET_CHAR_CREATOR_PARENT_RACE(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData, CHAR_CREATOR_PED aChar, INT iRaceIndex)
	IF aChar = CHAR_CREATOR_PED_DAD
		IF iRaceIndex = CHAR_CREATOR_RACE_INDEX_ONE
			RETURN charCreatorData.pedData.charCreatorParentInfo.charDadRace
		ELSE
			RETURN charCreatorData.pedData.charCreatorParentInfo.charDadRace2
		ENDIF
	ELSE
		IF iRaceIndex = CHAR_CREATOR_RACE_INDEX_ONE
			RETURN charCreatorData.pedData.charCreatorParentInfo.charMumRace
		ELSE
			RETURN charCreatorData.pedData.charCreatorParentInfo.charMumRace2
		ENDIF
	ENDIF
ENDFUNC

FUNC TEXT_LABEL_15 GET_CHAR_CREATOR_RACE_STRING(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData, CHAR_CREATOR_PED aCharPed, INT iRaceIndex)
	TEXT_LABEL_15 tlRaceStr //= "FACE_RACE"
	CHAR_CREATOR_RACE aRace = GET_CHAR_CREATOR_PARENT_RACE(charCreatorData, aCharPed, iRaceIndex)
		
	IF aRace = CHAR_CREATOR_SPECIAL_RACE
		INT iSpecialHead = GET_CHAR_CREATOR_PARENT_SPECIAL_HEAD_INDEX(charCreatorData, aCharPed)
		tlRaceStr = "FACE_RC_"
		
		IF aCharPed = CHAR_CREATOR_PED_MUM
			tlRaceStr += "M_"
		ELSE
			tlRaceStr += "D_"
		ENDIF
				
		tlRaceStr += iSpecialHead
	ELSE
		IF aCharPed = CHAR_CREATOR_PED_DAD
			IF iRaceIndex = CHAR_CREATOR_RACE_INDEX_ONE
				tlRaceStr = "FACE_GR3"
				//tlRaceStr += (charCreatorData.iHeadForDadRace + 1)
			ELSE
				tlRaceStr = "FACE_GR4"
				//tlRaceStr += (charCreatorData.iHeadForDadRace2 + 1)
			ENDIF
		ELIF aCharPed = CHAR_CREATOR_PED_MUM
			IF iRaceIndex = CHAR_CREATOR_RACE_INDEX_ONE
				tlRaceStr = "FACE_GR1"
				//tlRaceStr += (charCreatorData.iHeadForMumRace + 1)
			ELSE
				tlRaceStr = "FACE_GR2"
				//tlRaceStr += (charCreatorData.iHeadForMumRace2 + 1)
			ENDIF
		ENDIF
	ENDIF
	
	RETURN tlRaceStr
ENDFUNC

/// PURPOSE: Sets the race of a parent
PROC SET_CHAR_CREATOR_PARENT_RACE(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData, CHAR_CREATOR_PED aChar, CHAR_CREATOR_RACE aRace, INT iRaceIndex)
	IF aChar = CHAR_CREATOR_PED_DAD
		IF iRaceIndex = CHAR_CREATOR_RACE_INDEX_ONE
			PRINTLN("[CHARCREATOR] SET_CHAR_CREATOR_PARENT_RACE - Setting Dads race to: ", ENUM_TO_INT(aRace))
			charCreatorData.pedData.charCreatorParentInfo.charDadRace = aRace
		ELSE
			PRINTLN("[CHARCREATOR] SET_CHAR_CREATOR_PARENT_RACE - Setting Dads race (2) to: ", ENUM_TO_INT(aRace))
			charCreatorData.pedData.charCreatorParentInfo.charDadRace2 = aRace
		ENDIF
	ELIF aChar = CHAR_CREATOR_PED_MUM
		IF iRaceIndex = CHAR_CREATOR_RACE_INDEX_ONE
			PRINTLN("[CHARCREATOR] SET_CHAR_CREATOR_PARENT_RACE - Setting Mums race to: ", ENUM_TO_INT(aRace))
			charCreatorData.pedData.charCreatorParentInfo.charMumRace = aRace
		ELSE
			PRINTLN("[CHARCREATOR] SET_CHAR_CREATOR_PARENT_RACE - Setting Mums race (2) to: ", ENUM_TO_INT(aRace))
			charCreatorData.pedData.charCreatorParentInfo.charMumRace2 = aRace
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Sets the two heads to the correct values based on race and gender
PROC GET_CHAR_CREATOR_HEADS_FOR_RACE(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData, CHAR_CREATOR_PED aPed, INT &iHeadValue, INT iHeadVar, INT iRaceIndex, BOOL bForceDadHead = FALSE, BOOL bForceMumHead = FALSE)

	CHAR_CREATOR_RACE aRace = GET_CHAR_CREATOR_PARENT_RACE(charCreatorData, aPed, iRaceIndex)
	CHAR_CREATOR_RACE aPrimaryRace = GET_CHAR_CREATOR_PARENT_RACE(charCreatorData, aPed, CHAR_CREATOR_RACE_INDEX_ONE)

	// If our primary head is a special head then always use special head
	IF aPrimaryRace = CHAR_CREATOR_SPECIAL_RACE
		iHeadValue = GET_CHAR_CREATOR_PARENT_SPECIAL_HEAD(charCreatorData, aPed)
	ELSE
		IF (aPed = CHAR_CREATOR_PED_DAD OR bForceDadHead)
		AND NOT bForceMumHead
			IF iHeadVar = MP_HEAD_PRETTY
				iHeadValue = GET_PED_HEAD_BLEND_FIRST_INDEX(HBHT_MALE) + (ENUM_TO_INT(aRace) * 2)
			ELIF iHeadVar = MP_HEAD_UGLY
				iHeadValue = GET_PED_HEAD_BLEND_FIRST_INDEX(HBHT_MALE) + (ENUM_TO_INT(aRace) * 2) + 1
			ELSE
				IF aRace = CHAR_CREATOR_WHITE
					iHeadValue = GET_PED_HEAD_BLEND_FIRST_INDEX(HBHT_MALE) + CHAR_CREATOR_D_WHITE_OFFSET
				ELIF aRACE = CHAR_CREATOR_CHINESE
					iHeadValue = GET_PED_HEAD_BLEND_FIRST_INDEX(HBHT_MALE) + CHAR_CREATOR_D_CHINESE_OFFSET
				ELSE
					// fallback ugly
					iHeadValue = GET_PED_HEAD_BLEND_FIRST_INDEX(HBHT_MALE) + (ENUM_TO_INT(aRace) * 2) + 1
				ENDIF
			ENDIF
		ELSE
			IF iHeadVar = MP_HEAD_PRETTY
				iHeadValue = GET_PED_HEAD_BLEND_FIRST_INDEX(HBHT_FEMALE) + (ENUM_TO_INT(aRace) * 2)
			ELIF iHeadVar = MP_HEAD_UGLY
				iHeadValue 	= GET_PED_HEAD_BLEND_FIRST_INDEX(HBHT_FEMALE) + (ENUM_TO_INT(aRace) * 2) + 1
			ELSE
				IF aRace = CHAR_CREATOR_LATIN
					iHeadValue = GET_PED_HEAD_BLEND_FIRST_INDEX(HBHT_FEMALE) + CHAR_CREATOR_M_LATINO_OFFSET
				ELIF aRACE = CHAR_CREATOR_BLACK
					iHeadValue = GET_PED_HEAD_BLEND_FIRST_INDEX(HBHT_FEMALE) + CHAR_CREATOR_M_BLACK_OFFSET
				ELSE
					// fallback ugly
					iHeadValue 	= GET_PED_HEAD_BLEND_FIRST_INDEX(HBHT_FEMALE) + (ENUM_TO_INT(aRace) * 2) + 1
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	PRINTLN("[CHARCREATOR] GET_CHAR_CREATOR_HEADS_FOR_RACE - Parents head: iHeadValue ", iHeadValue, " and iHeadVar ", iHeadVar)

ENDPROC

FUNC INT GET_CHAR_CREATOR_SPECIAL_HEAD_INDEX(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData, BOOL bMale)
	
	IF bMale
		
		SWITCH GET_CHAR_CREATOR_PARENT_SPECIAL_HEAD_INDEX(charCreatorData, CHAR_CREATOR_PED_DAD)
			CASE 0		RETURN 1
			CASE 1		RETURN 2
			CASE 2		RETURN 3
		ENDSWITCH
		
		PRINTLN("[CHARCREATOR] GET_CHAR_CREATOR_SPECIAL_HEAD_INDEX - Unknown dad head: ", GET_CHAR_CREATOR_PARENT_SPECIAL_HEAD_INDEX(charCreatorData, CHAR_CREATOR_PED_DAD))
		SCRIPT_ASSERT("Character Creator - Unknown DAD head for special ped models")
	ELSE
		SWITCH GET_CHAR_CREATOR_PARENT_SPECIAL_HEAD_INDEX(charCreatorData, CHAR_CREATOR_PED_MUM)
			CASE 0			RETURN 0
		ENDSWITCH
		
		PRINTLN("[CHARCREATOR] GET_CHAR_CREATOR_SPECIAL_HEAD_INDEX - Unknown mum head: ", GET_CHAR_CREATOR_PARENT_SPECIAL_HEAD_INDEX(charCreatorData, CHAR_CREATOR_PED_MUM))
		SCRIPT_ASSERT("Character Creator - Unknown MUM head for special ped models")
	ENDIF
	
	RETURN 0
ENDFUNC

FUNC CHAR_CREATOR_RACE GET_CHAR_CREATOR_RACE_FROM_HEAD(INT iHeadPassed, CHAR_CREATOR_GENDER aGender)

	INT iHead
	
	IF aGender = CHAR_CREATOR_GENDER_FEMALE
		iHead = ((iHeadPassed - 12) % 12)
	ELSE
		iHead = (iHeadPassed % 12)
	ENDIF
	
	SWITCH iHead
		CASE 0
		CASE 1
			RETURN CHAR_CREATOR_WHITE
		CASE 2
		CASE 3
			RETURN CHAR_CREATOR_BLACK
		CASE 4
		CASE 5
			RETURN CHAR_CREATOR_LATIN
		CASE 6
		CASE 7
			RETURN CHAR_CREATOR_CHINESE
		CASE 8
		CASE 9
			RETURN CHAR_CREATOR_INDIAN
		CASE 10
		CASE 11
			RETURN CHAR_CREATOR_POLYNESIAN
		DEFAULT
			RETURN CHAR_CREATOR_SPECIAL_RACE
	ENDSWITCH

	RETURN CHAR_CREATOR_WHITE
ENDFUNC


/// PURPOSE: Allows you to get the hours for a specific lifestyle
/// PARAMS:
///    creatorLifestyle - Lifestyle type
///    bMaximum - TRUE if you want the maximum value, FALSE for minimum
FUNC INT GET_CHAR_CREATOR_HOURS_BOUNDS_FOR_LIFESTYLE(CHAR_CREATOR_LIFESTYLE creatorLifestyle, BOOL bMaximum = TRUE)
	
	IF bMaximum
		SWITCH creatorLifestyle
			CASE LIFESTYLE_SLEEP		RETURN ciCHAR_SLEEP_MAX
			CASE LIFESTYLE_FAMILY		RETURN ciCHAR_FAMILY_MAX
			CASE LIFESTYLE_SPORT		RETURN ciCHAR_SPORT_MAX
			CASE LIFESTYLE_LEGAL		RETURN ciCHAR_LEGAL_MAX
			CASE LIFESTYLE_TV			RETURN ciCHAR_TV_MAX
			CASE LIFESTYLE_PARTY		RETURN ciCHAR_PARTY_MAX
			CASE LIFESTYLE_ILLEGAL		RETURN ciCHAR_ILLEGAL_MAX
		ENDSWITCH
		
		RETURN 1
	ELSE
		SWITCH creatorLifestyle
			CASE LIFESTYLE_SLEEP		RETURN ciCHAR_SLEEP_MIN
			CASE LIFESTYLE_FAMILY		RETURN ciCHAR_FAMILY_MIN
			CASE LIFESTYLE_SPORT		RETURN ciCHAR_SPORT_MIN
			CASE LIFESTYLE_LEGAL		RETURN ciCHAR_LEGAL_MIN
			CASE LIFESTYLE_TV			RETURN ciCHAR_TV_MIN
			CASE LIFESTYLE_PARTY		RETURN ciCHAR_PARTY_MIN
			CASE LIFESTYLE_ILLEGAL		RETURN ciCHAR_ILLEGAL_MIN
		ENDSWITCH
		
		RETURN 0
	ENDIF
	
ENDFUNC

/// PURPOSE: Returns our text label for lifestyle
FUNC STRING GET_CHAR_CREATOR_LIFESTYLE_TEXT_LABEL(CHAR_CREATOR_LIFESTYLE aLifestyle)

	SWITCH aLifeStyle
		CASE LIFESTYLE_SLEEP		RETURN "FACES_BAR7"
		CASE LIFESTYLE_FAMILY		RETURN "FACES_BAR5"
		CASE LIFESTYLE_SPORT		RETURN "FACES_BAR6"
		CASE LIFESTYLE_LEGAL		RETURN "FACES_BAR4"
		CASE LIFESTYLE_TV			RETURN "FACES_BAR2"
		CASE LIFESTYLE_PARTY		RETURN "FACES_BAR8"
		CASE LIFESTYLE_ILLEGAL		RETURN "FACES_BAR3"
	ENDSWITCH

	PRINTLN("[CHARCREATOR] GET_CHAR_CREATOR_LIFESTYLE_TEXT_LABEL - Unknown lifestyle: ", ENUM_TO_INT(aLifestyle))
	SCRIPT_ASSERT("GET_CHAR_CREATOR_LIFESTYLE_TEXT_LABEL - Unknown character creator lifestyle. See logs for details")
	RETURN ""
ENDFUNC


/// PURPOSE: Returns the percentage for the passed lifestyle
FUNC FLOAT GET_CHAR_CREATOR_LIFESTYLE_PERCENTAGE(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData, CHAR_CREATOR_LIFESTYLE aLifestyle)
	RETURN (TO_FLOAT(charCreatorData.pedData.charLifestyleHours[aLifestyle]) / TO_FLOAT(GET_CHAR_CREATOR_HOURS_BOUNDS_FOR_LIFESTYLE(aLifestyle, TRUE)))
ENDFUNC

/// PURPOSE: Returns how many hours we have left to assign
FUNC INT GET_CHAR_CREATOR_HOURS_LEFT(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData)
	RETURN charCreatorData.pedData.iHoursLeftToAssign
ENDFUNC

#IF IS_DEBUG_BUILD
	FUNC STRING GET_CHAR_CREATOR_OVERLAY_STRING(CHAR_CREATOR_OVERLAY aOverlay)
		SWITCH aOverlay
			CASE CHAR_CREATOR_BLEMISH		RETURN "CHAR_CREATOR_BLEMISH"
			CASE CHAR_CREATOR_BEARD			RETURN "CHAR_CREATOR_BEARD"
			CASE CHAR_CREATOR_EYEBROW		RETURN "CHAR_CREATOR_EYEBROW"
			CASE CHAR_CREATOR_WRINKLES		RETURN "CHAR_CREATOR_WRINKLES"
			CASE CHAR_CREATOR_MAKEUP		RETURN "CHAR_CREATOR_MAKEUP"
			CASE CHAR_CREATOR_DAMAGE		RETURN "CHAR_CREATOR_DAMAGE"
			CASE CHAR_CREATOR_BASE_BLEMISH	RETURN "CHAR_CREATOR_BASE_BLEMISH"
		ENDSWITCH
		
		RETURN "OVERLAY UNKNOWN"
	ENDFUNC
	
	FUNC STRING GET_CHAR_CREATOR_DEBUG_OUTFIT_TEXT(MP_SPAWN_OUTFIT_TYPE anOutfit)
	
		SWITCH anOutfit
			CASE MPSO_CASUAL			RETURN "MPSO_CASUAL"
			CASE MPSO_COUCH_POTATO		RETURN "MPSO_COUCH_POTATO"
			CASE MPSO_ILLEGAL_WORK		RETURN "MPSO_ILLEGAL_WORK"
			CASE MPSO_LEGAL_WORK		RETURN "MPSO_LEGAL_WORK"
			CASE MPSO_PARTYING			RETURN "MPSO_PARTYING"
			CASE MPSO_SPORT				RETURN "MPSO_SPORT"
		ENDSWITCH
	
		RETURN "UNKNOWN OUTFIT"
	ENDFUNC
#ENDIF

/// PURPOSE: Returns the correct outfit based on lifestyle passed
FUNC MP_SPAWN_OUTFIT_TYPE GET_CHAR_CREATOR_OUTFIT_FOR_LIFESTYLE(CHAR_CREATOR_LIFESTYLE aLifestyle)
	SWITCH aLifestyle
		CASE LIFESTYLE_FAMILY		RETURN MPSO_CASUAL
		CASE LIFESTYLE_TV			RETURN MPSO_COUCH_POTATO
		CASE LIFESTYLE_ILLEGAL		RETURN MPSO_ILLEGAL_WORK
		CASE LIFESTYLE_LEGAL		RETURN MPSO_LEGAL_WORK
		CASE LIFESTYLE_PARTY		RETURN MPSO_PARTYING
		CASE LIFESTYLE_SPORT		RETURN MPSO_SPORT
	ENDSWITCH
	
	RETURN MPSO_CASUAL
ENDFUNC


FUNC BOOL IS_PED_HERITAGE_PREDOMINANT(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData, CHAR_CREATOR_RACE checkHeritage, BOOL bIsOpposite, BOOL bJustMum = FALSE, BOOL bJustDad = FALSE)
	BOOL bMumPredominant
	BOOL bDadPredominant
	BOOL bChildPredominant
	
	CHAR_CREATOR_RACE mumsMumRace = charCreatorData.pedData.charCreatorParentInfo.charMumRace
	CHAR_CREATOR_RACE mumsDadRace = charCreatorData.pedData.charCreatorParentInfo.charMumRace2
	CHAR_CREATOR_RACE dadsMumRace = charCreatorData.pedData.charCreatorParentInfo.charDadRace
	CHAR_CREATOR_RACE dadsDadRace = charCreatorData.pedData.charCreatorParentInfo.charDadRace2

	// mum
	IF mumsMumRace = checkHeritage
	AND mumsDadRace = checkHeritage
		bMumPredominant = TRUE
	ELIF mumsMumRace = checkHeritage
		IF charCreatorData.pedData.fMumDominance <= 50
			bMumPredominant = TRUE
		ENDIF
	ELIF mumsDadRace = checkHeritage
		IF charCreatorData.pedData.fMumDominance >= 50
			bMumPredominant = TRUE
		ENDIF
	ENDIF
	
	IF bMumPredominant
		IF bJustMum
			RETURN TRUE
		ENDIF
	ENDIF

	// dad
	IF dadsMumRace = checkHeritage
	AND dadsDadRace = checkHeritage
		bDadPredominant = TRUE
	ELIF dadsMumRace = checkHeritage
		IF charCreatorData.pedData.fDadDominance <= 50
			bDadPredominant = TRUE
		ENDIF
	ELIF dadsDadRace = checkHeritage
		IF charCreatorData.pedData.fDadDominance >= 50
			bDadPredominant = TRUE
		ENDIF
	ENDIF
	
	IF bDadPredominant
		IF bJustDad
			RETURN TRUE
		ENDIF
	ENDIF	
	
	IF bJustDad OR bJustMum
		RETURN FALSE
	ENDIF
	
	// special parents
	IF checkHeritage = CHAR_CREATOR_SPECIAL_RACE
		PRINTSTRING("check special race") PRINTNL()
		IF mumsMumRace = checkHeritage
		OR mumsDadRace = checkHeritage
			PRINTSTRING("mum - special ped is dominant") PRINTNL()
			bMumPredominant = TRUE
		ENDIF
		
		IF dadsMumRace = checkHeritage
		OR dadsDadRace = checkHeritage
			PRINTSTRING("dad - special ped is dominant") PRINTNL()
			bDadPredominant = TRUE
		ENDIF		
	ENDIF
		
	// child
	IF bMumPredominant
	AND bDadPredominant
		bChildPredominant = TRUE
	ELIF bMumPredominant
		IF NOT bIsOpposite
			IF charCreatorData.pedData.fParentDominance <= 40
				bChildPredominant = TRUE
			ENDIF
		ELSE
			IF charCreatorData.pedData.fParentDominance >= 60
				bChildPredominant = TRUE
			ENDIF			
		ENDIF
	ELIF bDadPredominant
		IF NOT bIsOpposite
			IF charCreatorData.pedData.fParentDominance >= 60
				bChildPredominant = TRUE
			ENDIF
		ELSE
			IF charCreatorData.pedData.fParentDominance <= 40
				bChildPredominant = TRUE
			ENDIF
		ENDIF
	ENDIF

	RETURN bChildPredominant
ENDFUNC


/// PURPOSE: Populates variables passed to those for the overlay
PROC GET_CHAR_CREATOR_OVERLAY_VALUES(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData, CHAR_CREATOR_OVERLAY aOverlay, INT &iValue, FLOAT &fBlend)
	iValue = charCreatorData.pedData.charOverlays[aOverlay].iOverlayVal
	fBlend = charCreatorData.pedData.charOverlays[aOverlay].fOverlayBlend
ENDPROC


/// PURPOSE: Apply the overlay for beard
PROC APPLY_CHAR_CREATOR_BEARD_OVERLAY(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData)

	INT iValue
	FLOAT fBlend
	
	GET_CHAR_CREATOR_OVERLAY_VALUES(charCreatorData, CHAR_CREATOR_BEARD, iValue, fBlend)
	PRINTLN("[CHARCREATOR] APPLY_CHAR_CREATOR_BEARD_OVERLAY - BEARD: ", iValue, " Blend: ", fBlend)
	
	IF NOT IS_PED_INJURED(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON])
		SET_PED_HEAD_OVERLAY(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON], HOS_FACIAL_HAIR, iValue, fBlend)
	ENDIF
	
	IF NOT IS_PED_INJURED(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER])
		SET_PED_HEAD_OVERLAY(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER], ENUM_TO_INT(CHAR_CREATOR_BEARD), 0, 0.0)
	ENDIF
ENDPROC


/// PURPOSE: Generates random hair for us
PROC GENERATE_CHAR_CREATOR_RANDOM_HAIR(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData, MP_SPAWN_OUTFIT_TYPE newOutfit, BOOL bFromOutfitChange = FALSE, BOOL bAcceptChangeToUniversal = TRUE, BOOL bAllowDup = FALSE, BOOL bOnlySon = FALSE, BOOL bOnlyDaughter = FALSE)
	
	bFromOutfitChange=bFromOutfitChange
	//ELSE
		BOOL bPredominantlyAfricanForMom
		BOOL bPredominantlyAfricanForDad
		BOOL bPredominantlyEuropeanForMom
		BOOL bPredominantlyEuropeanForDad
		
		BOOL bCurrentlyMale = IS_CHAR_CREATOR_PED_MALE(charCreatorData)
		
		INT iAttempts = 0		
		BOOL bFoundHair = FALSE
			
		INT iRandomHair
		INT iRandomHairColour
	
		INT iMaxHairColours = 1
		INT iHairColourSelection[5]		
	
		bPredominantlyAfricanForMom = IS_PED_HERITAGE_PREDOMINANT(charCreatorData, CHAR_CREATOR_BLACK, bCurrentlyMale)
		bPredominantlyEuropeanForMom = IS_PED_HERITAGE_PREDOMINANT(charCreatorData, CHAR_CREATOR_WHITE, bCurrentlyMale) OR IS_PED_HERITAGE_PREDOMINANT(charCreatorData, CHAR_CREATOR_SPECIAL_RACE, bCurrentlyMale)
		
		bPredominantlyAfricanForDad = IS_PED_HERITAGE_PREDOMINANT(charCreatorData, CHAR_CREATOR_BLACK, NOT bCurrentlyMale)
		bPredominantlyEuropeanForDad = IS_PED_HERITAGE_PREDOMINANT(charCreatorData, CHAR_CREATOR_WHITE, NOT bCurrentlyMale) OR IS_PED_HERITAGE_PREDOMINANT(charCreatorData, CHAR_CREATOR_SPECIAL_RACE, NOT bCurrentlyMale)
		//BOOL bPredominantlySouthAmerican = IS_PED_HERITAGE_PREDOMINANT(charCreatorData, CHAR_CREATOR_LATIN)
		//BOOL bPredominantlyEastAsian = IS_PED_HERITAGE_PREDOMINANT(charCreatorData, CHAR_CREATOR_CHINESE)
		//BOOL bPredominantlySouthAsian = IS_PED_HERITAGE_PREDOMINANT(charCreatorData, CHAR_CREATOR_INDIAN)
		//BOOL bPredominantlyPacificIslander = IS_PED_HERITAGE_PREDOMINANT(charCreatorData, CHAR_CREATOR_POLYNESIAN)
		
		CHAR_CREATOR_HAIRSTYLE_ENUM hairstyleSelection[12]
		CHAR_CREATOR_HAIRSTYLE_ENUM randomHair 
		INT iMaxHairstyles = 4
		CHAR_CREATOR_COMPLEXION_ENUM daughterComplexion
		CHAR_CREATOR_COMPLEXION_ENUM sonComplexion
		IF bPredominantlyAfricanForMom
			PRINTSTRING("daughter complexion is dark") PRINTNL()
			daughterComplexion = COMPLEXION_DARK
		ELIF bPredominantlyEuropeanForMom
			PRINTSTRING("daughter complexion is fair") PRINTNL()
			daughterComplexion = COMPLEXION_FAIR
		ELSE
			PRINTSTRING("daughter complexion is medium") PRINTNL()
			daughterComplexion = COMPLEXION_MEDIUM
		ENDIF
		
		IF bPredominantlyAfricanForDad
			PRINTSTRING("son complexion is dark") PRINTNL()
			sonComplexion = COMPLEXION_DARK
		ELIF bPredominantlyEuropeanForDad
			PRINTSTRING("son complexion is fair") PRINTNL()
			sonComplexion = COMPLEXION_FAIR
		ELSE
			PRINTSTRING("son complexion is medium") PRINTNL()
			sonComplexion = COMPLEXION_MEDIUM
		ENDIF		
		
		// male hair
		IF NOT bOnlyDaughter
			SWITCH sonComplexion
				CASE COMPLEXION_DARK
					hairstyleSelection[0] = HAIRSTYLE_UNIVERSAL_1
					//hairstyleSelection[1] = HAIRSTYLE_UNIVERSAL_2
					hairstyleSelection[1] = HAIRSTYLE_UNIVERSAL_3		
					iMaxHairstyles = 2
				BREAK
				
				CASE COMPLEXION_MEDIUM
					hairstyleSelection[0] = HAIRSTYLE_UNIVERSAL_1
					hairstyleSelection[1] = HAIRSTYLE_UNIVERSAL_3
					hairstyleSelection[2] = HAIRSTYLE_UNIVERSAL_4	
					hairstyleSelection[3] = HAIRSTYLE_UNIVERSAL_5	
					hairstyleSelection[4] = HAIRSTYLE_UNIVERSAL_6	
					iMaxHairstyles = 5
				BREAK		
				
				CASE COMPLEXION_FAIR
					hairstyleSelection[0] = HAIRSTYLE_UNIVERSAL_3
					hairstyleSelection[1] = HAIRSTYLE_UNIVERSAL_4
					hairstyleSelection[2] = HAIRSTYLE_UNIVERSAL_5	
					hairstyleSelection[3] = HAIRSTYLE_UNIVERSAL_6	
					iMaxHairstyles = 4
				BREAK			
			ENDSWITCH
			
			//IF bFromOutfitChange // COMMENT OUTFIT CHANGE
				SWITCH newOutfit
					CASE MPSO_SPORT
						SWITCH sonComplexion	
							CASE COMPLEXION_DARK
								hairstyleSelection[3] = HAIRSTYLE_SPORTY_1
								hairstyleSelection[4] = HAIRSTYLE_SPORTY_2
								hairstyleSelection[5] = HAIRSTYLE_SPORTY_1
								hairstyleSelection[6] = HAIRSTYLE_SPORTY_2						
								iMaxHairstyles = 7
							BREAK
							CASE COMPLEXION_MEDIUM
								hairstyleSelection[5] = HAIRSTYLE_SPORTY_1
								hairstyleSelection[6] = HAIRSTYLE_SPORTY_2
								hairstyleSelection[7] = HAIRSTYLE_SPORTY_3		
								hairstyleSelection[8] = HAIRSTYLE_SPORTY_1
								hairstyleSelection[9] = HAIRSTYLE_SPORTY_2
								hairstyleSelection[10] = HAIRSTYLE_SPORTY_3			
								iMaxHairstyles = 11
							BREAK			
							CASE COMPLEXION_FAIR
								hairstyleSelection[4] = HAIRSTYLE_SPORTY_1
								hairstyleSelection[5] = HAIRSTYLE_SPORTY_3
								hairstyleSelection[6] = HAIRSTYLE_SPORTY_1		
								hairstyleSelection[7] = HAIRSTYLE_SPORTY_3		
								iMaxHairstyles = 8
							BREAK						
						ENDSWITCH
					BREAK			
				
					CASE MPSO_PARTYING
						SWITCH sonComplexion	
							CASE COMPLEXION_DARK
								hairstyleSelection[3] = HAIRSTYLE_PARTY_1
								hairstyleSelection[4] = HAIRSTYLE_PARTY_2
								hairstyleSelection[5] = HAIRSTYLE_PARTY_3
								hairstyleSelection[6] = HAIRSTYLE_PARTY_4		
								hairstyleSelection[7] = HAIRSTYLE_PARTY_1
								hairstyleSelection[8] = HAIRSTYLE_PARTY_2
								hairstyleSelection[9] = HAIRSTYLE_PARTY_3
								hairstyleSelection[10] = HAIRSTYLE_PARTY_4							
								iMaxHairstyles = 11
							BREAK
							CASE COMPLEXION_MEDIUM
								hairstyleSelection[5] = HAIRSTYLE_PARTY_1
								hairstyleSelection[6] = HAIRSTYLE_PARTY_2
								hairstyleSelection[7] = HAIRSTYLE_PARTY_3		
								hairstyleSelection[8] = HAIRSTYLE_PARTY_1
								hairstyleSelection[9] = HAIRSTYLE_PARTY_2
								hairstyleSelection[10] = HAIRSTYLE_PARTY_3			
								iMaxHairstyles = 11
							BREAK			
							CASE COMPLEXION_FAIR
								hairstyleSelection[4] = HAIRSTYLE_PARTY_1
								hairstyleSelection[5] = HAIRSTYLE_PARTY_2
								hairstyleSelection[6] = HAIRSTYLE_PARTY_3		
								hairstyleSelection[7] = HAIRSTYLE_PARTY_1
								hairstyleSelection[8] = HAIRSTYLE_PARTY_2
								hairstyleSelection[9] = HAIRSTYLE_PARTY_3			
								iMaxHairstyles = 10
							BREAK						
						ENDSWITCH
					BREAK		
				
					CASE MPSO_ILLEGAL_WORK
						SWITCH sonComplexion	
							CASE COMPLEXION_DARK
								hairstyleSelection[3] = HAIRSTYLE_ILLEGAL_1
								hairstyleSelection[4] = HAIRSTYLE_ILLEGAL_2
								hairstyleSelection[5] = HAIRSTYLE_ILLEGAL_1
								hairstyleSelection[6] = HAIRSTYLE_ILLEGAL_2				
								iMaxHairstyles = 7
							BREAK
							CASE COMPLEXION_MEDIUM
								hairstyleSelection[5] = HAIRSTYLE_ILLEGAL_1
								hairstyleSelection[6] = HAIRSTYLE_ILLEGAL_2
								hairstyleSelection[7] = HAIRSTYLE_ILLEGAL_3
								hairstyleSelection[8] = HAIRSTYLE_ILLEGAL_1
								hairstyleSelection[9] = HAIRSTYLE_ILLEGAL_2
								hairstyleSelection[10] = HAIRSTYLE_ILLEGAL_3				
								iMaxHairstyles = 11
							BREAK			
							CASE COMPLEXION_FAIR
								hairstyleSelection[4] = HAIRSTYLE_ILLEGAL_4
								hairstyleSelection[5] = HAIRSTYLE_ILLEGAL_2
								hairstyleSelection[6] = HAIRSTYLE_ILLEGAL_3		
								hairstyleSelection[7] = HAIRSTYLE_ILLEGAL_4
								hairstyleSelection[8] = HAIRSTYLE_ILLEGAL_2
								hairstyleSelection[9] = HAIRSTYLE_ILLEGAL_3							
								iMaxHairstyles = 10
							BREAK						
						ENDSWITCH
					BREAK		
				
					CASE MPSO_COUCH_POTATO
						SWITCH sonComplexion	
							CASE COMPLEXION_DARK
								hairstyleSelection[3] = HAIRSTYLE_LAZY_1
								hairstyleSelection[4] = HAIRSTYLE_LAZY_2
								hairstyleSelection[5] = HAIRSTYLE_LAZY_1
								hairstyleSelection[6] = HAIRSTYLE_LAZY_2				
								iMaxHairstyles = 7
							BREAK
							CASE COMPLEXION_MEDIUM
								hairstyleSelection[5] = HAIRSTYLE_LAZY_2
								hairstyleSelection[6] = HAIRSTYLE_LAZY_3
								hairstyleSelection[7] = HAIRSTYLE_LAZY_2
								hairstyleSelection[8] = HAIRSTYLE_LAZY_3					
								iMaxHairstyles = 9
							BREAK			
							CASE COMPLEXION_FAIR
								hairstyleSelection[4] = HAIRSTYLE_LAZY_2
								hairstyleSelection[5] = HAIRSTYLE_LAZY_3
								hairstyleSelection[6] = HAIRSTYLE_LAZY_2
								hairstyleSelection[7] = HAIRSTYLE_LAZY_3			
								iMaxHairstyles = 8
							BREAK						
						ENDSWITCH
					BREAK
				ENDSWITCH
			//ENDIF

			WHILE iAttempts < 5
			AND NOT bFoundHair
				randomHair = hairstyleSelection[GET_RANDOM_INT_IN_RANGE(0,iMaxHairstyles)]
				IF bAllowDup
					bFoundHair = TRUE
				ELSE
					IF randomHair <> charCreatorData.lastMaleHair
						bFoundHair = TRUE
					ELSE
						PRINTSTRING("same as last time male - try again") PRINTNL()				
					ENDIF
				ENDIF
				iAttempts++
			ENDWHILE	
			charCreatorData.lastMaleHair = randomHair
			

			// get int for hair
			SWITCH randomHair
				CASE HAIRSTYLE_UNIVERSAL_1
					iRandomHair = 1
				BREAK
				CASE HAIRSTYLE_UNIVERSAL_2
					iRandomHair = 0
				BREAK
				CASE HAIRSTYLE_UNIVERSAL_3
					iRandomHair = 4
				BREAK
				CASE HAIRSTYLE_UNIVERSAL_4
					iRandomHair = 5
				BREAK
				CASE HAIRSTYLE_UNIVERSAL_5
					iRandomHair = 9
				BREAK
				CASE HAIRSTYLE_UNIVERSAL_6
					iRandomHair = 10
				BREAK
				CASE HAIRSTYLE_LAZY_1
					iRandomHair = 8
				BREAK
				CASE HAIRSTYLE_LAZY_2
					iRandomHair = 14
				BREAK
				CASE HAIRSTYLE_LAZY_3
					iRandomHair = 7
				BREAK
				CASE HAIRSTYLE_ILLEGAL_1
					iRandomHair = 8
				BREAK
				CASE HAIRSTYLE_ILLEGAL_2
					iRandomHair = 3
				BREAK
				CASE HAIRSTYLE_ILLEGAL_3
					iRandomHair = 7
				BREAK		
				CASE HAIRSTYLE_ILLEGAL_4
					iRandomHair = 1
				BREAK
				CASE HAIRSTYLE_PARTY_1
					iRandomHair = 6
				BREAK
				CASE HAIRSTYLE_PARTY_2
					iRandomHair = 3
				BREAK
				CASE HAIRSTYLE_PARTY_3
					iRandomHair = 2
				BREAK		
				CASE HAIRSTYLE_PARTY_4
					iRandomHair = 1
				BREAK	
				CASE HAIRSTYLE_SPORTY_1
					iRandomHair = 1
				BREAK	
				CASE HAIRSTYLE_SPORTY_2
					iRandomHair = 8
				BREAK
				CASE HAIRSTYLE_SPORTY_3
					iRandomHair = 11
				BREAK
			ENDSWITCH	
			
			// custom hair
			IF charCreatorData.pedData.archetype = ARCHETYPE_CUSTOM
				SWITCH charCreatorData.pedData.customArchetype
					CASE CUSTOM_ARCHETYPE_MALE_BUSINESS_1	
						iRandomHair = 1 // buzz
					BREAK
					CASE CUSTOM_ARCHETYPE_MALE_CASUAL_ASIAN	
						iRandomHair = 1 // buzz
					BREAK
					CASE CUSTOM_ARCHETYPE_MALE_SLOB_EUROPEAN	
						iRandomHair = 7 // ponytail
					BREAK
					CASE CUSTOM_ARCHETYPE_MALE_CASUAL_1
						iRandomHair = 1 // buzz
					BREAK
					CASE CUSTOM_ARCHETYPE_MALE_DIRTY_PARTY
						iRandomHair = 5 // shorter cut
					BREAK
				ENDSWITCH	
			ENDIF

			PRINTLN("[CHARCREATOR] GENERATE_CHAR_CREATOR_RANDOM_HAIR - setting sons hair to: ", iRandomHair)	
			SET_CHAR_CREATOR_HAIR(charCreatorData, CHAR_CREATOR_GENDER_MALE, iRandomHair)
			
			// male hair colour
			SWITCH randomHair
				// UNIVERSAL
				CASE HAIRSTYLE_UNIVERSAL_1
					SWITCH sonComplexion	
						CASE COMPLEXION_DARK
							iHairColourSelection[0] = 4
							iMaxHairColours = 1
						BREAK
						CASE COMPLEXION_MEDIUM
							iHairColourSelection[0] = 0
							iHairColourSelection[1] = 4
							iHairColourSelection[2] = 1
							iMaxHairColours = 3
						BREAK							
					ENDSWITCH
				BREAK	
				
				CASE HAIRSTYLE_UNIVERSAL_2
					SWITCH sonComplexion	
						CASE COMPLEXION_DARK
							iHairColourSelection[0] = 4
							iMaxHairColours = 1
						BREAK						
					ENDSWITCH
				BREAK		
				
				CASE HAIRSTYLE_UNIVERSAL_3
					SWITCH sonComplexion	
						CASE COMPLEXION_DARK
							iHairColourSelection[0] = 4
							iMaxHairColours = 1
						BREAK	
						CASE COMPLEXION_MEDIUM
							iHairColourSelection[0] = 0
							iHairColourSelection[1] = 4
							iHairColourSelection[2] = 1
							iMaxHairColours = 3
						BREAK	
						CASE COMPLEXION_FAIR
							iHairColourSelection[0] = 0
							iHairColourSelection[1] = 1
							iHairColourSelection[2] = 2
							iHairColourSelection[3] = 3
							iMaxHairColours = 4
						BREAK						
					ENDSWITCH
				BREAK		
				
				CASE HAIRSTYLE_UNIVERSAL_4
					SWITCH sonComplexion	
						CASE COMPLEXION_MEDIUM
							iHairColourSelection[0] = 0
							iHairColourSelection[1] = 4
							iHairColourSelection[2] = 1
							iMaxHairColours = 3
						BREAK	
						CASE COMPLEXION_FAIR
							iHairColourSelection[0] = 0
							iHairColourSelection[1] = 1
							iHairColourSelection[2] = 2
							iHairColourSelection[3] = 3
							iMaxHairColours = 4
						BREAK				
					ENDSWITCH
				BREAK	
				
				CASE HAIRSTYLE_UNIVERSAL_5
					SWITCH sonComplexion	
						CASE COMPLEXION_MEDIUM
							iHairColourSelection[0] = 0
							iHairColourSelection[1] = 4
							iMaxHairColours = 2
						BREAK	
						CASE COMPLEXION_FAIR
							iHairColourSelection[0] = 0
							iHairColourSelection[1] = 1
							iHairColourSelection[2] = 2
							iHairColourSelection[3] = 3
							iMaxHairColours = 4
						BREAK				
					ENDSWITCH
				BREAK		
				
				CASE HAIRSTYLE_UNIVERSAL_6
					SWITCH sonComplexion	
						CASE COMPLEXION_MEDIUM
							iHairColourSelection[0] = 0
							iHairColourSelection[1] = 4
							iMaxHairColours = 2
						BREAK	
						CASE COMPLEXION_FAIR
							iHairColourSelection[0] = 0
							iHairColourSelection[1] = 1
							iHairColourSelection[2] = 2
							iHairColourSelection[3] = 3
							iMaxHairColours = 4
						BREAK				
					ENDSWITCH
				BREAK		
				
				CASE HAIRSTYLE_LAZY_1
					SWITCH sonComplexion	
						CASE COMPLEXION_DARK
							iHairColourSelection[0] = 4
							iMaxHairColours = 1
						BREAK			
					ENDSWITCH
				BREAK	
				
				CASE HAIRSTYLE_LAZY_2
					SWITCH sonComplexion	
						CASE COMPLEXION_DARK
							iHairColourSelection[0] = 4
							iMaxHairColours = 1
						BREAK	
						CASE COMPLEXION_MEDIUM
							iHairColourSelection[0] = 0
							iHairColourSelection[1] = 1
							iMaxHairColours = 2
						BREAK	
						CASE COMPLEXION_FAIR
							iHairColourSelection[0] = 1
							iMaxHairColours = 1
						BREAK			
					ENDSWITCH
				BREAK	
				
				CASE HAIRSTYLE_LAZY_3
					SWITCH sonComplexion	
						CASE COMPLEXION_MEDIUM
							iHairColourSelection[0] = 0
							iHairColourSelection[1] = 4
							iMaxHairColours = 2
						BREAK	
						CASE COMPLEXION_FAIR
							iHairColourSelection[0] = 0
							iHairColourSelection[1] = 1
							iMaxHairColours = 2
						BREAK			
					ENDSWITCH
				BREAK	
				
				CASE HAIRSTYLE_ILLEGAL_1
					SWITCH sonComplexion	
						CASE COMPLEXION_DARK
							iHairColourSelection[0] = 4
							iMaxHairColours = 1
						BREAK			
					ENDSWITCH
				BREAK	
				
				CASE HAIRSTYLE_ILLEGAL_2
					SWITCH sonComplexion	
						CASE COMPLEXION_DARK
							iHairColourSelection[0] = 4
							iMaxHairColours = 1
						BREAK			
						CASE COMPLEXION_MEDIUM
							iHairColourSelection[0] = 4
							iHairColourSelection[1] = 2
							iHairColourSelection[2] = 1
							iHairColourSelection[3] = 0
							iMaxHairColours = 4
						BREAK	
						CASE COMPLEXION_FAIR
							iHairColourSelection[0] = 0
							iHairColourSelection[1] = 1
							iHairColourSelection[2] = 3
							iMaxHairColours = 3
						BREAK						
					ENDSWITCH
				BREAK		
				
				CASE HAIRSTYLE_ILLEGAL_3
					SWITCH sonComplexion			
						CASE COMPLEXION_MEDIUM
							iHairColourSelection[0] = 0
							iHairColourSelection[1] = 4
							iMaxHairColours = 2
						BREAK	
						CASE COMPLEXION_FAIR
							iHairColourSelection[0] = 0
							iHairColourSelection[1] = 1
							iMaxHairColours = 2
						BREAK						
					ENDSWITCH
				BREAK	
				
				CASE HAIRSTYLE_ILLEGAL_4
					SWITCH sonComplexion			
						CASE COMPLEXION_FAIR
							iHairColourSelection[0] = 0
							iHairColourSelection[1] = 1
							iHairColourSelection[2] = 2
							iHairColourSelection[3] = 3
							iMaxHairColours = 4
						BREAK						
					ENDSWITCH
				BREAK	
				
				CASE HAIRSTYLE_PARTY_1
					SWITCH sonComplexion	
						CASE COMPLEXION_DARK
							iHairColourSelection[0] = 0
							iHairColourSelection[1] = 1
							iHairColourSelection[2] = 3
							iHairColourSelection[3] = 4
							iMaxHairColours = 4
						BREAK			
						CASE COMPLEXION_MEDIUM
							iHairColourSelection[0] = 0
							iHairColourSelection[1] = 1
							iHairColourSelection[2] = 2
							iHairColourSelection[3] = 3
							iHairColourSelection[4] = 4
							iMaxHairColours = 5
						BREAK	
						CASE COMPLEXION_FAIR
							iHairColourSelection[0] = 0
							iHairColourSelection[1] = 1
							iHairColourSelection[2] = 2
							iHairColourSelection[3] = 3
							iMaxHairColours = 4
						BREAK						
					ENDSWITCH
				BREAK			
				
				CASE HAIRSTYLE_PARTY_2
					SWITCH sonComplexion	
						CASE COMPLEXION_DARK
							iHairColourSelection[0] = 0
							iHairColourSelection[1] = 1
							iHairColourSelection[2] = 3
							iHairColourSelection[3] = 4
							iMaxHairColours = 4
						BREAK			
						CASE COMPLEXION_MEDIUM
							iHairColourSelection[0] = 0
							iHairColourSelection[1] = 1
							iHairColourSelection[2] = 2
							iHairColourSelection[3] = 4
							iMaxHairColours = 4
						BREAK	
						CASE COMPLEXION_FAIR
							iHairColourSelection[0] = 0
							iHairColourSelection[1] = 1
							iHairColourSelection[2] = 2
							iHairColourSelection[3] = 3
							iMaxHairColours = 4
						BREAK						
					ENDSWITCH
				BREAK	
				
				CASE HAIRSTYLE_PARTY_3
					SWITCH sonComplexion	
						CASE COMPLEXION_DARK
							iHairColourSelection[0] = 4
							iMaxHairColours = 1
						BREAK			
						CASE COMPLEXION_MEDIUM
							iHairColourSelection[0] = 4
							iHairColourSelection[1] = 0
							iHairColourSelection[2] = 1
							iMaxHairColours = 3
						BREAK	
						CASE COMPLEXION_FAIR
							iHairColourSelection[0] = 0
							iHairColourSelection[1] = 1
							iHairColourSelection[2] = 2
							iHairColourSelection[3] = 3
							iMaxHairColours = 4
						BREAK						
					ENDSWITCH
				BREAK		
				
				CASE HAIRSTYLE_PARTY_4
					SWITCH sonComplexion	
						CASE COMPLEXION_DARK
							iHairColourSelection[0] = 3
							iHairColourSelection[1] = 2
							iHairColourSelection[2] = 1
							iMaxHairColours = 3
						BREAK								
					ENDSWITCH
				BREAK	
				
				CASE HAIRSTYLE_SPORTY_1
					SWITCH sonComplexion	
						CASE COMPLEXION_DARK
							iHairColourSelection[0] = 4
							iMaxHairColours = 1
						BREAK			
						CASE COMPLEXION_MEDIUM
							iHairColourSelection[0] = 2
							iHairColourSelection[1] = 1
							iMaxHairColours = 2
						BREAK	
						CASE COMPLEXION_FAIR
							iHairColourSelection[0] = 0
							iHairColourSelection[1] = 1
							iHairColourSelection[2] = 2
							iHairColourSelection[3] = 3
							iMaxHairColours = 4
						BREAK						
					ENDSWITCH
				BREAK	

				CASE HAIRSTYLE_SPORTY_2
					SWITCH sonComplexion	
						CASE COMPLEXION_DARK
							iHairColourSelection[0] = 4
							iMaxHairColours = 1
						BREAK			
						CASE COMPLEXION_MEDIUM
							iHairColourSelection[0] = 0
							iMaxHairColours = 1
						BREAK					
					ENDSWITCH
				BREAK	
				
				CASE HAIRSTYLE_SPORTY_3
					SWITCH sonComplexion	
						CASE COMPLEXION_MEDIUM
							iHairColourSelection[0] = 0
							iHairColourSelection[1] = 1
							iHairColourSelection[2] = 2
							iHairColourSelection[3] = 4
							iMaxHairColours = 4
						BREAK	
						CASE COMPLEXION_FAIR
							iHairColourSelection[0] = 0
							iHairColourSelection[1] = 1
							iHairColourSelection[2] = 2
							iHairColourSelection[3] = 3
							iMaxHairColours = 4
						BREAK				
					ENDSWITCH
				BREAK	
			ENDSWITCH
			
			iAttempts = 0
			bFoundHair = FALSE
			
			WHILE iAttempts < 5
			AND NOT bFoundHair
				iRandomHairColour = iHairColourSelection[GET_RANDOM_INT_IN_RANGE(0, iMaxHairColours)]
				IF bAllowDup
					bFoundHair = TRUE
				ELSE
					IF iRandomHairColour <> charCreatorData.iLastRandomMaleHairColour
						bFoundHair = TRUE
					ELSE
						PRINTSTRING("same as last time male colour - try again") PRINTNL()				
					ENDIF
				ENDIF
				iAttempts++
			ENDWHILE
			
			// custom hair colour
			IF charCreatorData.pedData.archetype = ARCHETYPE_CUSTOM
				SWITCH charCreatorData.pedData.customArchetype
					CASE CUSTOM_ARCHETYPE_MALE_BUSINESS_1	
						iRandomHairColour = 4 
					BREAK
					CASE CUSTOM_ARCHETYPE_MALE_CASUAL_ASIAN	
						iRandomHairColour = 0
					BREAK
					CASE CUSTOM_ARCHETYPE_MALE_SLOB_EUROPEAN	
						iRandomHairColour = 0 
					BREAK
					CASE CUSTOM_ARCHETYPE_MALE_CASUAL_1
						iRandomHairColour = 4
					BREAK
					CASE CUSTOM_ARCHETYPE_MALE_DIRTY_PARTY
						iRandomHairColour = 1
					BREAK
				ENDSWITCH	
			ENDIF		
			
			charCreatorData.iLastRandomMaleHairColour = iRandomHairColour	
			
			SET_CHAR_CREATOR_HAIR_COLOUR(charCreatorData, CHAR_CREATOR_GENDER_MALE, iRandomHairColour)		
			
			// random beard
			INT iRandomBeard = -1
			IF GET_RANDOM_INT_IN_RANGE(0,2) = 0
			OR randomHair = HAIRSTYLE_UNIVERSAL_2 // close shave
			OR charCreatorData.pedData.archetype = ARCHETYPE_CUSTOM
				
				INT iMaxBeard = 3
				INT iBeardSelection[10]
				iBeardSelection[0] = 18
				iBeardSelection[1] = 2
				iBeardSelection[2] = 0
				
				//IF bFromOutfitChange // COMMENT OUTFIT CHANGE
					SWITCH newOutfit
						CASE MPSO_CASUAL
							iBeardSelection[3] = 7
							iBeardSelection[4] = 13
							iBeardSelection[5] = 7
							iBeardSelection[6] = 13
							iMaxBeard += 4
						BREAK
						CASE MPSO_COUCH_POTATO
							iBeardSelection[3] = 7
							iBeardSelection[4] = 9
							iBeardSelection[5] = 7
							iBeardSelection[6] = 9
							iMaxBeard += 4
						BREAK
						CASE MPSO_ILLEGAL_WORK
							iBeardSelection[3] = 15
							iBeardSelection[4] = 12
							iBeardSelection[5] = 5
							iBeardSelection[6] = 15
							iBeardSelection[7] = 12
							iBeardSelection[8] = 5
							iMaxBeard += 6
						BREAK
						CASE MPSO_LEGAL_WORK
							iBeardSelection[3] = 9
							iBeardSelection[4] = 12
							iBeardSelection[5] = 9
							iBeardSelection[6] = 12
							iMaxBeard +=4
						BREAK
						CASE MPSO_PARTYING
							iBeardSelection[3] = 14
							iBeardSelection[4] = 9
							iBeardSelection[5] = 15
							iBeardSelection[6] = 14
							iBeardSelection[7] = 9
							iBeardSelection[8] = 15
							iMaxBeard += 6
						BREAK	
						CASE MPSO_SPORT
							iBeardSelection[3] = 13
							iBeardSelection[4] = 8
							iBeardSelection[5] = 13
							iBeardSelection[6] = 8
							iMaxBeard += 4
						BREAK
					ENDSWITCH
				//ENDIF
			
				iAttempts = 0
				bFoundHair = FALSE
			
				WHILE iAttempts < 5
				AND NOT bFoundHair
					iRandomBeard = iBeardSelection[GET_RANDOM_INT_IN_RANGE(0, iMaxBeard)]
					IF bAllowDup
						bFoundHair = TRUE
					ELSE
						IF iRandomBeard <> charCreatorData.iLastRandomBeard
							bFoundHair = TRUE
						ELSE
							PRINTSTRING("same as last time beard - try again") PRINTNL()				
						ENDIF
					ENDIF
					iAttempts++
				ENDWHILE
				
				// custom beard
				IF IS_CHAR_CREATOR_PED_MALE(charCreatorData)
					IF charCreatorData.pedData.archetype = ARCHETYPE_CUSTOM
						SWITCH charCreatorData.pedData.customArchetype
							CASE CUSTOM_ARCHETYPE_MALE_BUSINESS_1	
								iRandomBeard = 9
							BREAK
							CASE CUSTOM_ARCHETYPE_MALE_CASUAL_ASIAN	
								iRandomBeard = 7
							BREAK
							CASE CUSTOM_ARCHETYPE_MALE_SLOB_EUROPEAN	
								iRandomBeard = 2
							BREAK
							CASE CUSTOM_ARCHETYPE_MALE_CASUAL_1
								iRandomBeard = 18
							BREAK
							CASE CUSTOM_ARCHETYPE_MALE_DIRTY_PARTY
								iRandomBeard = 0
							BREAK
						ENDSWITCH	
					ENDIF				
				ENDIF
				
				charCreatorData.iLastRandomBeard = iRandomBeard
			ENDIF
			
			charCreatorData.pedData.charOverlays[ENUM_TO_INT(CHAR_CREATOR_BEARD)].iOverlayVal 		= iRandomBeard
			charCreatorData.pedData.charOverlays[ENUM_TO_INT(CHAR_CREATOR_BEARD)].fOverlayBlend 	= 1.0
			
			// store beard
			CHAR_CREATOR_LIFESTYLE maxLifestyle = GET_MAX_LIFESTYLE(charCreatorData)
			INT iLockLifestyleSlot = ENUM_TO_INT(maxLifestyle) - 1
			charCreatorData.iCacheMaleBeard[iLockLifestyleSlot] = iRandomBeard
			
			//APPLY_CHAR_CREATOR_BEARD_OVERLAY(charCreatorData)
		ENDIF
		
		IF NOT bOnlySon
			// female hair
			hairstyleSelection[0] = HAIRSTYLE_UNIVERSAL_1
			hairstyleSelection[1] = HAIRSTYLE_UNIVERSAL_2
			hairstyleSelection[2] = HAIRSTYLE_UNIVERSAL_3
			hairstyleSelection[3] = HAIRSTYLE_UNIVERSAL_4
			
			iMaxHairstyles = 4
			
			//IF bFromOutfitChange  COMMENT OUTFIT CHANGE
				SWITCH newOutfit
					CASE MPSO_CASUAL
						SWITCH daughterComplexion	
							CASE COMPLEXION_DARK
								hairstyleSelection[4] = HAIRSTYLE_CASUAL_1
								hairstyleSelection[5] = HAIRSTYLE_CASUAL_2
								hairstyleSelection[6] = HAIRSTYLE_CASUAL_3
								hairstyleSelection[7] = HAIRSTYLE_CASUAL_1
								hairstyleSelection[8] = HAIRSTYLE_CASUAL_2
								hairstyleSelection[9] = HAIRSTYLE_CASUAL_3						
								iMaxHairstyles = 10
							BREAK
							CASE COMPLEXION_MEDIUM
								hairstyleSelection[4] = HAIRSTYLE_CASUAL_1
								hairstyleSelection[5] = HAIRSTYLE_CASUAL_2
								hairstyleSelection[6] = HAIRSTYLE_CASUAL_1
								hairstyleSelection[7] = HAIRSTYLE_CASUAL_2						
								iMaxHairstyles = 8
							BREAK			
							CASE COMPLEXION_FAIR
								hairstyleSelection[4] = HAIRSTYLE_CASUAL_1
								hairstyleSelection[5] = HAIRSTYLE_CASUAL_1
								iMaxHairstyles = 6
							BREAK						
						ENDSWITCH
					BREAK
					CASE MPSO_ILLEGAL_WORK
						SWITCH daughterComplexion	
							CASE COMPLEXION_DARK
								hairstyleSelection[4] = HAIRSTYLE_ILLEGAL_1
								hairstyleSelection[5] = HAIRSTYLE_ILLEGAL_2
								hairstyleSelection[6] = HAIRSTYLE_ILLEGAL_1
								hairstyleSelection[7] = HAIRSTYLE_ILLEGAL_2
								iMaxHairstyles = 8
							BREAK
							CASE COMPLEXION_MEDIUM
								hairstyleSelection[4] = HAIRSTYLE_ILLEGAL_3
								hairstyleSelection[5] = HAIRSTYLE_ILLEGAL_3
								iMaxHairstyles = 6
							BREAK			
							CASE COMPLEXION_FAIR
								hairstyleSelection[4] = HAIRSTYLE_ILLEGAL_3
								hairstyleSelection[5] = HAIRSTYLE_ILLEGAL_3
								iMaxHairstyles = 6
							BREAK						
						ENDSWITCH
					BREAK	
					CASE MPSO_LEGAL_WORK
						SWITCH daughterComplexion	
							CASE COMPLEXION_DARK
								hairstyleSelection[4] = HAIRSTYLE_LEGAL_1
								hairstyleSelection[5] = HAIRSTYLE_LEGAL_2
								hairstyleSelection[6] = HAIRSTYLE_LEGAL_3
								hairstyleSelection[7] = HAIRSTYLE_LEGAL_4
								hairstyleSelection[8] = HAIRSTYLE_LEGAL_1
								hairstyleSelection[9] = HAIRSTYLE_LEGAL_2
								hairstyleSelection[10] = HAIRSTYLE_LEGAL_3
								hairstyleSelection[11] = HAIRSTYLE_LEGAL_4						
								iMaxHairstyles = 12
							BREAK
							CASE COMPLEXION_MEDIUM
								hairstyleSelection[4] = HAIRSTYLE_LEGAL_1
								hairstyleSelection[5] = HAIRSTYLE_LEGAL_2
								hairstyleSelection[6] = HAIRSTYLE_LEGAL_4
								hairstyleSelection[7] = HAIRSTYLE_LEGAL_1
								hairstyleSelection[8] = HAIRSTYLE_LEGAL_2
								hairstyleSelection[9] = HAIRSTYLE_LEGAL_4
								iMaxHairstyles = 10
							BREAK			
							CASE COMPLEXION_FAIR
								hairstyleSelection[4] = HAIRSTYLE_LEGAL_1
								hairstyleSelection[5] = HAIRSTYLE_LEGAL_2
								hairstyleSelection[6] = HAIRSTYLE_LEGAL_4
								hairstyleSelection[7] = HAIRSTYLE_LEGAL_1
								hairstyleSelection[8] = HAIRSTYLE_LEGAL_2
								hairstyleSelection[9] = HAIRSTYLE_LEGAL_4
								iMaxHairstyles = 10
							BREAK						
						ENDSWITCH			
					BREAK
					CASE MPSO_PARTYING
						SWITCH daughterComplexion	
							CASE COMPLEXION_DARK
								hairstyleSelection[4] = HAIRSTYLE_PARTY_1
								hairstyleSelection[5] = HAIRSTYLE_PARTY_2
								hairstyleSelection[6] = HAIRSTYLE_PARTY_3		
								hairstyleSelection[7] = HAIRSTYLE_PARTY_1
								hairstyleSelection[8] = HAIRSTYLE_PARTY_2
								hairstyleSelection[9] = HAIRSTYLE_PARTY_3		
								iMaxHairstyles = 10
							BREAK
							CASE COMPLEXION_MEDIUM
								hairstyleSelection[4] = HAIRSTYLE_PARTY_1
								hairstyleSelection[5] = HAIRSTYLE_PARTY_2
								hairstyleSelection[6] = HAIRSTYLE_PARTY_4		
								hairstyleSelection[7] = HAIRSTYLE_PARTY_1
								hairstyleSelection[8] = HAIRSTYLE_PARTY_2
								hairstyleSelection[9] = HAIRSTYLE_PARTY_4	
								iMaxHairstyles = 10
							BREAK			
							CASE COMPLEXION_FAIR
								hairstyleSelection[4] = HAIRSTYLE_PARTY_1
								hairstyleSelection[5] = HAIRSTYLE_PARTY_2
								hairstyleSelection[6] = HAIRSTYLE_PARTY_4		
								hairstyleSelection[7] = HAIRSTYLE_PARTY_1
								hairstyleSelection[8] = HAIRSTYLE_PARTY_2
								hairstyleSelection[9] = HAIRSTYLE_PARTY_4
								iMaxHairstyles = 10
							BREAK						
						ENDSWITCH	
					BREAK
					CASE MPSO_SPORT
						SWITCH daughterComplexion	
							CASE COMPLEXION_DARK
								hairstyleSelection[4] = HAIRSTYLE_SPORTY_1
								hairstyleSelection[5] = HAIRSTYLE_SPORTY_2
								hairstyleSelection[6] = HAIRSTYLE_SPORTY_3
								hairstyleSelection[7] = HAIRSTYLE_SPORTY_1
								hairstyleSelection[8] = HAIRSTYLE_SPORTY_2
								hairstyleSelection[9] = HAIRSTYLE_SPORTY_3
								iMaxHairstyles = 10
							BREAK
							CASE COMPLEXION_MEDIUM
								hairstyleSelection[4] = HAIRSTYLE_SPORTY_3
								hairstyleSelection[5] = HAIRSTYLE_SPORTY_3
								iMaxHairstyles = 6
							BREAK			
							CASE COMPLEXION_FAIR
								hairstyleSelection[4] = HAIRSTYLE_SPORTY_3
								hairstyleSelection[5] = HAIRSTYLE_SPORTY_3
								iMaxHairstyles = 6
							BREAK						
						ENDSWITCH	
					BREAK
				ENDSWITCH
			//ENDIF
			
			iAttempts = 0
			bFoundHair = FALSE
			
			WHILE iAttempts < 5
			AND NOT bFoundHair
				randomHair = hairstyleSelection[GET_RANDOM_INT_IN_RANGE(0,iMaxHairstyles)]
				IF bAllowDup
					bFoundHair = TRUE
				ELSE
					IF randomHair <> charCreatorData.lastFemaleHair
						bFoundHair = TRUE
					ELSE
						PRINTSTRING("same as last time female - try again") PRINTNL()
					ENDIF
				ENDIF
				iAttempts++
			ENDWHILE	

			charCreatorData.lastFemaleHair = randomHair

			// get int for hair
			SWITCH randomHair
				CASE HAIRSTYLE_UNIVERSAL_1
					iRandomHair = 11
				BREAK
				CASE HAIRSTYLE_UNIVERSAL_2
					iRandomHair = 10
				BREAK
				CASE HAIRSTYLE_UNIVERSAL_3
					iRandomHair = 1
				BREAK
				CASE HAIRSTYLE_UNIVERSAL_4
					iRandomHair = 4
				BREAK
				CASE HAIRSTYLE_CASUAL_1
					iRandomHair = 15
				BREAK
				CASE HAIRSTYLE_CASUAL_2
					iRandomHair = 14
				BREAK
				CASE HAIRSTYLE_CASUAL_3
					iRandomHair = 5
				BREAK
				CASE HAIRSTYLE_ILLEGAL_1
					iRandomHair = 6
				BREAK
				CASE HAIRSTYLE_ILLEGAL_2
					iRandomHair = 10
				BREAK
				CASE HAIRSTYLE_ILLEGAL_3
					iRandomHair = 3
				BREAK
				CASE HAIRSTYLE_LEGAL_1
					iRandomHair = 7
				BREAK
				CASE HAIRSTYLE_LEGAL_2
					iRandomHair = 15
				BREAK
				CASE HAIRSTYLE_LEGAL_3
					iRandomHair = 14
				BREAK
				CASE HAIRSTYLE_LEGAL_4
					iRandomHair = 12
				BREAK
				CASE HAIRSTYLE_PARTY_1
					iRandomHair = 13
				BREAK
				CASE HAIRSTYLE_PARTY_2
					iRandomHair = 8
				BREAK
				CASE HAIRSTYLE_PARTY_3
					iRandomHair = 5
				BREAK
				CASE HAIRSTYLE_PARTY_4
					iRandomHair = 15
				BREAK
				CASE HAIRSTYLE_SPORTY_1
					iRandomHair = 6
				BREAK
				CASE HAIRSTYLE_SPORTY_2
					iRandomHair = 5
				BREAK
				CASE HAIRSTYLE_SPORTY_3
					iRandomHair = 3
				BREAK
			ENDSWITCH
			
			//IF bAcceptChangeToUniversal
			//OR randomHair > HAIRSTYLE_UNIVERSAL_4
			//OR charCreatorData.pedData.lastFemaleHair > HAIRSTYLE_UNIVERSAL_4
			
			// custom hair
			IF charCreatorData.pedData.archetype = ARCHETYPE_CUSTOM
				SWITCH charCreatorData.pedData.customArchetype
					CASE CUSTOM_ARCHETYPE_FEMALE_CHOLA	
						iRandomHair = 3
					BREAK
					CASE CUSTOM_ARCHETYPE_FEMALE_BUSINESS_1	
						iRandomHair = 11
					BREAK
					CASE CUSTOM_ARCHETYPE_FEMALE_BUSINESS_2	
						iRandomHair = 1
					BREAK
					CASE CUSTOM_ARCHETYPE_FEMALE_CASUAL_1
						iRandomHair = 11
					BREAK
					CASE CUSTOM_ARCHETYPE_FEMALE_CASUAL_2
						iRandomHair = 11
					BREAK
					CASE CUSTOM_ARCHETYPE_FEMALE_PARTY_SOUTH_AMERICAN
						iRandomHair = 2
					BREAK
					CASE CUSTOM_ARCHETYPE_FEMALE_CASUAL_3
						iRandomHair = 3
					BREAK
					CASE CUSTOM_ARCHETYPE_FEMALE_PARTY_EUROPEAN
						iRandomHair = 11
					BREAK
					CASE CUSTOM_ARCHETYPE_FEMALE_PARTY_EAST_ASIAN
						iRandomHair = 11
					BREAK
					CASE CUSTOM_ARCHETYPE_FEMALE_PARTY_AFRICAN
						SWITCH GET_RANDOM_INT_IN_RANGE(0,3)
							CASE 0
								iRandomHair = 5
							BREAK
							CASE 1
								iRandomHair = 12
							BREAK
							CASE 2
								iRandomHair = 6
							BREAK
						ENDSWITCH
					BREAK
					CASE CUSTOM_ARCHETYPE_FEMALE_PARTY_PACIFIC_ISLANDER
						iRandomHair = 11
					BREAK
					CASE CUSTOM_ARCHETYPE_FEMALE_COUCH_POTATO_EUROPEAN
						iRandomHair = 1
					BREAK
					CASE CUSTOM_ARCHETYPE_FEMALE_COUCH_POTATO_MIXED_1
						iRandomHair = 11
					BREAK
					CASE CUSTOM_ARCHETYPE_FEMALE_COUCH_POTATO_MIXED_2
						iRandomHair = 11
					BREAK
					CASE CUSTOM_ARCHETYPE_FEMALE_COUCH_POTATO_MIXED_3
						iRandomHair = 11
					BREAK
					CASE CUSTOM_ARCHETYPE_FEMALE_CHOLA_MIXED
						iRandomHair = 11
					BREAK
					CASE CUSTOM_ARCHETYPE_FEMALE_BUSINESS_3
						iRandomHair = 11
					BREAK
					CASE CUSTOM_ARCHETYPE_FEMALE_PARTY_HARD
						iRandomHair = 1
					BREAK
				ENDSWITCH	
			ENDIF	
		
			bAcceptChangeToUniversal=bAcceptChangeToUniversal
				PRINTLN("[CHARCREATOR] GENERATE_CHAR_CREATOR_RANDOM_HAIR - setting daughters hair to: ", iRandomHair)
				SET_CHAR_CREATOR_HAIR(charCreatorData, CHAR_CREATOR_GENDER_FEMALE, iRandomHair)
				charCreatorData.pedData.lastFemaleHair = randomHair
				
				// female hair colour
				SWITCH randomHair
					// UNIVERSAL
					CASE HAIRSTYLE_UNIVERSAL_1
						SWITCH daughterComplexion	
							CASE COMPLEXION_DARK
								iHairColourSelection[0] = 3
								iHairColourSelection[1] = 4
								iMaxHairColours = 2
							BREAK
							CASE COMPLEXION_MEDIUM
								iHairColourSelection[0] = 3
								iHairColourSelection[1] = 4
								iHairColourSelection[2] = 0
								iMaxHairColours = 3
							BREAK			
							CASE COMPLEXION_FAIR
								iHairColourSelection[0] = 0
								iHairColourSelection[1] = 1
								iHairColourSelection[2] = 2
								iHairColourSelection[3] = 3
								iHairColourSelection[4] = 4
								iMaxHairColours = 5
							BREAK						
						ENDSWITCH
					BREAK
					CASE HAIRSTYLE_UNIVERSAL_2
						SWITCH daughterComplexion	
							CASE COMPLEXION_DARK
								iHairColourSelection[0] = 3
								iMaxHairColours = 1
							BREAK
							CASE COMPLEXION_MEDIUM
								iHairColourSelection[0] = 0
								iHairColourSelection[1] = 2
								iHairColourSelection[2] = 3
								iHairColourSelection[3] = 4
								iMaxHairColours = 4
							BREAK			
							CASE COMPLEXION_FAIR
								iHairColourSelection[0] = 0
								iHairColourSelection[1] = 1
								iHairColourSelection[2] = 3
								iHairColourSelection[3] = 4
								iMaxHairColours = 4
							BREAK						
						ENDSWITCH
					BREAK
					CASE HAIRSTYLE_UNIVERSAL_3
						SWITCH daughterComplexion	
							CASE COMPLEXION_DARK
								iHairColourSelection[0] = 3
								iHairColourSelection[1] = 4
								iHairColourSelection[2] = 0
								iMaxHairColours = 3
							BREAK
							CASE COMPLEXION_MEDIUM
								iHairColourSelection[0] = 3
								iHairColourSelection[1] = 4
								iHairColourSelection[2] = 0
								iMaxHairColours = 3
							BREAK			
							CASE COMPLEXION_FAIR
								iHairColourSelection[0] = 0
								iHairColourSelection[1] = 1
								iHairColourSelection[2] = 2
								iMaxHairColours = 3
							BREAK						
						ENDSWITCH
					BREAK
					CASE HAIRSTYLE_UNIVERSAL_4
						SWITCH daughterComplexion	
							CASE COMPLEXION_DARK
								iHairColourSelection[0] = 3
								iMaxHairColours = 1
							BREAK
							CASE COMPLEXION_MEDIUM
								iHairColourSelection[0] = 3
								iHairColourSelection[1] = 4
								iHairColourSelection[2] = 0
								iHairColourSelection[3] = 2
								iMaxHairColours = 4
							BREAK			
							CASE COMPLEXION_FAIR
								iHairColourSelection[0] = 0
								iHairColourSelection[1] = 1
								iHairColourSelection[2] = 2
								iHairColourSelection[3] = 3
								iHairColourSelection[4] = 4
								iMaxHairColours = 5
							BREAK						
						ENDSWITCH
					BREAK	
					
					// CASUAL
					CASE HAIRSTYLE_CASUAL_1
						SWITCH daughterComplexion	
							CASE COMPLEXION_DARK
								iHairColourSelection[0] = 3
								iMaxHairColours = 1
							BREAK
							CASE COMPLEXION_MEDIUM
								iHairColourSelection[0] = 3
								iHairColourSelection[1] = 4
								iHairColourSelection[2] = 0
								iMaxHairColours = 3
							BREAK			
							CASE COMPLEXION_FAIR
								iHairColourSelection[0] = 4
								iHairColourSelection[1] = 2
								iHairColourSelection[2] = 1
								iMaxHairColours = 3
							BREAK						
						ENDSWITCH
					BREAK	
					CASE HAIRSTYLE_CASUAL_2
						SWITCH daughterComplexion	
							CASE COMPLEXION_DARK
								iHairColourSelection[0] = 3
								iHairColourSelection[1] = 4
								iHairColourSelection[2] = 0
								iMaxHairColours = 3
							BREAK
							CASE COMPLEXION_MEDIUM
								iHairColourSelection[0] = 3
								iHairColourSelection[1] = 4
								iHairColourSelection[2] = 0
								iMaxHairColours = 3
							BREAK							
						ENDSWITCH
					BREAK			
					CASE HAIRSTYLE_CASUAL_3
						SWITCH daughterComplexion	
							CASE COMPLEXION_DARK
								iHairColourSelection[0] = 3
								iHairColourSelection[1] = 0
								iMaxHairColours = 2
							BREAK						
						ENDSWITCH
					BREAK		
					
					// ILLEGAL
					CASE HAIRSTYLE_ILLEGAL_1
						SWITCH daughterComplexion	
							CASE COMPLEXION_DARK
								iHairColourSelection[0] = 3
								iHairColourSelection[1] = 0
								iMaxHairColours = 2
							BREAK					
						ENDSWITCH
					BREAK	
					CASE HAIRSTYLE_ILLEGAL_2
						SWITCH daughterComplexion	
							CASE COMPLEXION_DARK
								iHairColourSelection[0] = 0
								iMaxHairColours = 1
							BREAK					
						ENDSWITCH
					BREAK		
					CASE HAIRSTYLE_ILLEGAL_3
						SWITCH daughterComplexion	
							CASE COMPLEXION_MEDIUM
								iHairColourSelection[0] = 3
								iHairColourSelection[1] = 0
								iHairColourSelection[2] = 4
								iMaxHairColours = 3
							BREAK	
							CASE COMPLEXION_FAIR
								iHairColourSelection[0] = 3
								iHairColourSelection[1] = 2
								iHairColourSelection[2] = 1
								iMaxHairColours = 3
							BREAK						
						ENDSWITCH
					BREAK	
					
					// LEGAL
					CASE HAIRSTYLE_LEGAL_1
						SWITCH daughterComplexion	
							CASE COMPLEXION_DARK
								iHairColourSelection[0] = 3
								iMaxHairColours = 1
							BREAK				
							CASE COMPLEXION_MEDIUM
								iHairColourSelection[0] = 3
								iHairColourSelection[1] = 0
								iHairColourSelection[2] = 4
								iMaxHairColours = 3
							BREAK	
							CASE COMPLEXION_FAIR
								iHairColourSelection[0] = 0
								iHairColourSelection[1] = 1
								iHairColourSelection[2] = 2
								iHairColourSelection[3] = 3
								iHairColourSelection[4] = 4
								iMaxHairColours = 5
							BREAK						
						ENDSWITCH
					BREAK		
					
					CASE HAIRSTYLE_LEGAL_2
						SWITCH daughterComplexion	
							CASE COMPLEXION_DARK
								iHairColourSelection[0] = 3
								iMaxHairColours = 1
							BREAK				
							CASE COMPLEXION_MEDIUM
								iHairColourSelection[0] = 3
								iHairColourSelection[1] = 0
								iHairColourSelection[2] = 4
								iMaxHairColours = 3
							BREAK	
							CASE COMPLEXION_FAIR
								iHairColourSelection[0] = 4
								iHairColourSelection[1] = 2
								iHairColourSelection[2] = 1
								iMaxHairColours = 3
							BREAK						
						ENDSWITCH
					BREAK	
					
					CASE HAIRSTYLE_LEGAL_3
						SWITCH daughterComplexion	
							CASE COMPLEXION_DARK
								iHairColourSelection[0] = 3
								iHairColourSelection[1] = 4
								iMaxHairColours = 2
							BREAK								
						ENDSWITCH
					BREAK	
					
					CASE HAIRSTYLE_LEGAL_4
						SWITCH daughterComplexion	
							CASE COMPLEXION_DARK
								iHairColourSelection[0] = 3
								iMaxHairColours = 1
							BREAK				
							CASE COMPLEXION_MEDIUM
								iHairColourSelection[0] = 0
								iMaxHairColours = 1
							BREAK	
							CASE COMPLEXION_FAIR
								iHairColourSelection[0] = 1
								iHairColourSelection[1] = 0
								iMaxHairColours = 2
							BREAK							
						ENDSWITCH
					BREAK	
					
					// PARTY
					CASE HAIRSTYLE_PARTY_1
						SWITCH daughterComplexion	
							CASE COMPLEXION_DARK
								iHairColourSelection[0] = 3
								iHairColourSelection[1] = 1
								iHairColourSelection[2] = 0
								iHairColourSelection[3] = 4
								iMaxHairColours = 4
							BREAK				
							CASE COMPLEXION_MEDIUM
								iHairColourSelection[0] = 3
								iHairColourSelection[1] = 4
								iMaxHairColours = 2
							BREAK	
							CASE COMPLEXION_FAIR
								iHairColourSelection[0] = 3
								iHairColourSelection[1] = 2
								iHairColourSelection[2] = 1
								iMaxHairColours = 3
							BREAK							
						ENDSWITCH
					BREAK	
					
					CASE HAIRSTYLE_PARTY_2
						SWITCH daughterComplexion	
							CASE COMPLEXION_DARK
								iHairColourSelection[0] = 1
								iHairColourSelection[1] = 0
								iHairColourSelection[2] = 4
								iMaxHairColours = 3
							BREAK				
							CASE COMPLEXION_MEDIUM
								iHairColourSelection[0] = 3
								iHairColourSelection[1] = 4
								iHairColourSelection[2] = 0
								iMaxHairColours = 3
							BREAK	
							CASE COMPLEXION_FAIR
								iHairColourSelection[0] = 0
								iHairColourSelection[1] = 2
								iHairColourSelection[2] = 1
								iMaxHairColours = 3
							BREAK							
						ENDSWITCH
					BREAK
					
					CASE HAIRSTYLE_PARTY_3
						SWITCH daughterComplexion	
							CASE COMPLEXION_DARK
								iHairColourSelection[0] = 3
								iHairColourSelection[1] = 2
								iHairColourSelection[2] = 0
								iHairColourSelection[3] = 4
								iMaxHairColours = 4
							BREAK										
						ENDSWITCH
					BREAK	
					
					CASE HAIRSTYLE_PARTY_4
						SWITCH daughterComplexion	
							CASE COMPLEXION_MEDIUM
								iHairColourSelection[0] = 3
								iHairColourSelection[1] = 0
								iHairColourSelection[2] = 4
								iMaxHairColours = 3
							BREAK	
							CASE COMPLEXION_FAIR
								iHairColourSelection[0] = 4
								iHairColourSelection[1] = 2
								iHairColourSelection[2] = 1
								iMaxHairColours = 3
							BREAK									
						ENDSWITCH
					BREAK	
					
					// SPORTY
					CASE HAIRSTYLE_SPORTY_1
						SWITCH daughterComplexion	
							CASE COMPLEXION_DARK
								iHairColourSelection[0] = 3
								iMaxHairColours = 1
							BREAK									
						ENDSWITCH
					BREAK	
					
					CASE HAIRSTYLE_SPORTY_2
						SWITCH daughterComplexion	
							CASE COMPLEXION_DARK
								iHairColourSelection[0] = 3
								iMaxHairColours = 1
							BREAK									
						ENDSWITCH
					BREAK	
					
					CASE HAIRSTYLE_SPORTY_3
						SWITCH daughterComplexion	
							CASE COMPLEXION_MEDIUM
								iHairColourSelection[0] = 3
								iHairColourSelection[1] = 0
								iHairColourSelection[2] = 4
								iMaxHairColours = 3
							BREAK	
							CASE COMPLEXION_FAIR
								iHairColourSelection[0] = 1
								iHairColourSelection[1] = 2
								iMaxHairColours = 2
							BREAK								
						ENDSWITCH
					BREAK	
				ENDSWITCH
				
				iAttempts = 0
				bFoundHair = FALSE
				
				WHILE iAttempts < 5
				AND NOT bFoundHair
					iRandomHairColour = iHairColourSelection[GET_RANDOM_INT_IN_RANGE(0, iMaxHairColours)]
					IF bAllowDup
						bFoundHair = TRUE
					ELSE
						IF iRandomHairColour <> charCreatorData.iLastRandomFemaleHairColour
							bFoundHair = TRUE
						ELSE
							PRINTSTRING("same as last time female colour - try again") PRINTNL()				
						ENDIF
					ENDIF
					iAttempts++
				ENDWHILE		
				
				// custom hair colour
				IF charCreatorData.pedData.archetype = ARCHETYPE_CUSTOM
					SWITCH charCreatorData.pedData.customArchetype
						CASE CUSTOM_ARCHETYPE_FEMALE_CHOLA	
							iRandomHairColour = 3
						BREAK
						CASE CUSTOM_ARCHETYPE_FEMALE_BUSINESS_1	
							iRandomHairColour = 0
						BREAK
						CASE CUSTOM_ARCHETYPE_FEMALE_BUSINESS_2	
							iRandomHairColour = 3
						BREAK
						CASE CUSTOM_ARCHETYPE_FEMALE_CASUAL_1
							iRandomHairColour = 0
						BREAK
						CASE CUSTOM_ARCHETYPE_FEMALE_CASUAL_2
							iRandomHairColour = 4
						BREAK
						CASE CUSTOM_ARCHETYPE_FEMALE_PARTY_SOUTH_AMERICAN
							iRandomHairColour = 3
						BREAK
						CASE CUSTOM_ARCHETYPE_FEMALE_CASUAL_3
							iRandomHairColour = 3
						BREAK
						CASE CUSTOM_ARCHETYPE_FEMALE_PARTY_EUROPEAN
							iRandomHairColour = 1
						BREAK
						CASE CUSTOM_ARCHETYPE_FEMALE_PARTY_EAST_ASIAN
							iRandomHairColour = 4
						BREAK
						CASE CUSTOM_ARCHETYPE_FEMALE_PARTY_AFRICAN
							iRandomHairColour = 3
						BREAK
						CASE CUSTOM_ARCHETYPE_FEMALE_PARTY_PACIFIC_ISLANDER
							iRandomHairColour = 0
						BREAK
						CASE CUSTOM_ARCHETYPE_FEMALE_COUCH_POTATO_EUROPEAN
							iRandomHairColour = 1
						BREAK
						CASE CUSTOM_ARCHETYPE_FEMALE_COUCH_POTATO_MIXED_1
							iRandomHairColour = 2
						BREAK
						CASE CUSTOM_ARCHETYPE_FEMALE_COUCH_POTATO_MIXED_2
							iRandomHairColour = 3
						BREAK
						CASE CUSTOM_ARCHETYPE_FEMALE_COUCH_POTATO_MIXED_3
							iRandomHairColour = 3
						BREAK
						CASE CUSTOM_ARCHETYPE_FEMALE_CHOLA_MIXED
							iRandomHairColour = 3
						BREAK
						CASE CUSTOM_ARCHETYPE_FEMALE_BUSINESS_3
							iRandomHairColour = 3
						BREAK
						CASE CUSTOM_ARCHETYPE_FEMALE_PARTY_HARD
							iRandomHairColour = 0
						BREAK
					ENDSWITCH	
				ENDIF				
				
				charCreatorData.iLastRandomFemaleHairColour = iRandomHairColour
				
				SET_CHAR_CREATOR_HAIR_COLOUR(charCreatorData, CHAR_CREATOR_GENDER_FEMALE, iRandomHairColour)	
			ENDIF
		//ENDIF
	//ENDIF
ENDPROC

PROC GIVE_CHAR_CREATOR_PED_HAIR(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData, CHAR_CREATOR_PED aPedType, BOOL bDoOverlays = TRUE)
	
	CHAR_CREATOR_GENDER aGender = GET_CHAR_CREATOR_GENDER_OF_PED(aPedType)
	INT iHairCut = GET_CHAR_CREATOR_HAIR(charCreatorData, aGender)
	INT iHairColour = GET_CHAR_CREATOR_HAIR_COLOUR(charCreatorData, aGender)
	
	BOOL bIsMale = (aGender = CHAR_CREATOR_GENDER_MALE)
	PED_COMP_NAME_ENUM pedHair = GET_CHAR_CREATOR_HAIRCUT(bIsMale, iHairCut, iHairColour)
	
	PRINTLN("[CHARCREATOR] GIVE_CHAR_CREATOR_PED_HAIR - Giving ped hair")
	
	// don't do the hair overlays for the parents
	BOOL bSkipHairOverlays = FALSE

	IF aPedType = CHAR_CREATOR_PED_DAD
	OR aPedType = CHAR_CREATOR_PED_MUM
	OR NOT bDoOverlays
		bSkipHairOverlays = TRUE
	ENDIF
	SET_PED_COMP_ITEM_CURRENT_MP(charCreatorData.charCreatorPeds[aPedType], COMP_TYPE_HAIR, pedHair, FALSE, DEFAULT, DEFAULT, DEFAULT, bSkipHairOverlays)
ENDPROC

PROC APPLY_CHAR_CREATOR_MAKEUP_OVERLAY(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData)

	INT iValue
	FLOAT fBlend
	
	GET_CHAR_CREATOR_OVERLAY_VALUES(charCreatorData, CHAR_CREATOR_MAKEUP, iValue, fBlend)
	PRINTLN("[CHARCREATOR] APPLY_CHAR_CREATOR_MAKEUP_OVERLAY - MAKEUP: ", iValue, " Blend: ", fBlend)
	IF NOT IS_PED_INJURED(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER])
		SET_PED_HEAD_OVERLAY(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER], HOS_MAKEUP, iValue, fBlend)
	ENDIF
ENDPROC

PROC GENERATE_CHAR_CREATOR_RANDOM_MAKEUP(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData, MP_SPAWN_OUTFIT_TYPE newOutfit, BOOL bFromOutfitChange = FALSE, BOOL bAllowDup = FALSE)
	INT iCurrentMakeup
	bFromOutfitChange=bFromOutfitChange
	
	IF charCreatorData.pedData.archetype = ARCHETYPE_CUSTOM
		SWITCH charCreatorData.pedData.customArchetype
			// female
			CASE CUSTOM_ARCHETYPE_FEMALE_CHOLA	
				iCurrentMakeup = 6 // chola
			BREAK
			CASE CUSTOM_ARCHETYPE_FEMALE_BUSINESS_1	
				iCurrentMakeup = 0 // smoky black
			BREAK
			CASE CUSTOM_ARCHETYPE_FEMALE_BUSINESS_2	
				iCurrentMakeup = 0 // smoky black
			BREAK
			CASE CUSTOM_ARCHETYPE_FEMALE_CASUAL_1
				iCurrentMakeup = 0
			BREAK
			CASE CUSTOM_ARCHETYPE_FEMALE_CASUAL_2
				iCurrentMakeup = 0
			BREAK
			CASE CUSTOM_ARCHETYPE_FEMALE_PARTY_SOUTH_AMERICAN
				iCurrentMakeup = 0
			BREAK
			CASE CUSTOM_ARCHETYPE_FEMALE_CASUAL_3
				iCurrentMakeup = 4
			BREAK
			CASE CUSTOM_ARCHETYPE_FEMALE_PARTY_EUROPEAN
				iCurrentMakeup = 0
			BREAK
			CASE CUSTOM_ARCHETYPE_FEMALE_PARTY_EAST_ASIAN
				iCurrentMakeup = 0
			BREAK
			CASE CUSTOM_ARCHETYPE_FEMALE_PARTY_AFRICAN
				iCurrentMakeup = 0
			BREAK
			CASE CUSTOM_ARCHETYPE_FEMALE_PARTY_PACIFIC_ISLANDER
				iCurrentMakeup = 0
			BREAK
			CASE CUSTOM_ARCHETYPE_FEMALE_COUCH_POTATO_EUROPEAN
				iCurrentMakeup = 0
			BREAK
			CASE CUSTOM_ARCHETYPE_FEMALE_COUCH_POTATO_MIXED_1
				iCurrentMakeup = 0
			BREAK
			CASE CUSTOM_ARCHETYPE_FEMALE_COUCH_POTATO_MIXED_2
				iCurrentMakeup = 0
			BREAK
			CASE CUSTOM_ARCHETYPE_FEMALE_COUCH_POTATO_MIXED_3
				iCurrentMakeup = 0
			BREAK
			CASE CUSTOM_ARCHETYPE_FEMALE_CHOLA_MIXED
				iCurrentMakeup = 6
			BREAK
			CASE CUSTOM_ARCHETYPE_FEMALE_BUSINESS_3
				iCurrentMakeup = 0
			BREAK
			CASE CUSTOM_ARCHETYPE_FEMALE_PARTY_HARD
				iCurrentMakeup = 13
			BREAK
		ENDSWITCH
	ELSE
		// not custom archetype
		INT iMaxMakeup = 6
		INT iMakeupSelection[6]
		iMakeupSelection[0] = 0
		iMakeupSelection[1] = 4
		
		//IF bFromOutfitChange // COMMENT OUTFIT CHANGE
			SWITCH newOutfit
				CASE MPSO_CASUAL
					iMakeupSelection[2] = 8
					iMakeupSelection[3] = 2
					iMakeupSelection[4] = 8
					iMakeupSelection[5] = 2					
				BREAK
				CASE MPSO_ILLEGAL_WORK
					iMakeupSelection[2] = 6
					iMakeupSelection[3] = 11
					iMakeupSelection[4] = 6
					iMakeupSelection[5] = 11
				BREAK	
				CASE MPSO_LEGAL_WORK
					iMakeupSelection[2] = 7
					iMakeupSelection[3] = 14
					iMakeupSelection[4] = 7
					iMakeupSelection[5] = 14
				BREAK	
				CASE MPSO_PARTYING
					iMakeupSelection[2] = 13
					iMakeupSelection[3] = 3
					iMakeupSelection[4] = 13
					iMakeupSelection[5] = 3
				BREAK	
				CASE MPSO_SPORT
					iMakeupSelection[2] = 5
					iMakeupSelection[3] = 10
					iMakeupSelection[4] = 5
					iMakeupSelection[5] = 10
				BREAK	
			ENDSWITCH
		//ENDIF
		
		INT iAttempts = 0
		BOOL bFoundMakeup = FALSE
		
		
		WHILE iAttempts < 5
		AND NOT bFoundMakeup
			iCurrentMakeup  = iMakeupSelection[GET_RANDOM_INT_IN_RANGE(0, iMaxMakeup)]
			IF bAllowDup
				bFoundMakeup = TRUE
			ELSE
				IF iCurrentMakeup <> charCreatorData.iLastRandomMakeup
					bFoundMakeup = TRUE
				ELSE
					PRINTSTRING("same as last time makeup - try again") PRINTNL()				
				ENDIF
			ENDIF
			iAttempts++
		ENDWHILE
		charCreatorData.iLastRandomMakeup = iCurrentMakeup
	ENDIF	
		
	charCreatorData.pedData.charOverlays[ENUM_TO_INT(CHAR_CREATOR_MAKEUP)].iOverlayVal 		= iCurrentMakeup
	charCreatorData.pedData.charOverlays[ENUM_TO_INT(CHAR_CREATOR_MAKEUP)].fOverlayBlend 	= 1.0
	
	// store makeup
	CHAR_CREATOR_LIFESTYLE maxLifestyle = GET_MAX_LIFESTYLE(charCreatorData)
	INT iLockLifestyleSlot = ENUM_TO_INT(maxLifestyle) - 1
	charCreatorData.iCacheFemaleMakeup[iLockLifestyleSlot] = iCurrentMakeup	
	
	PRINTLN("[CHARCREATOR] GENERATE_CHAR_CREATOR_RANDOM_MAKEUP - iCurrentMakeup: ", iCurrentMakeup)
	
	//APPLY_CHAR_CREATOR_MAKEUP_OVERLAY(charCreatorData)

ENDPROC

PROC APPLY_CREW_TSHIRT_DECALS_TO_CURRENT_PED(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData, BOOL bFromChangeShirt = FALSE)
	PED_INDEX pedToApply

	IF IS_CHAR_CREATOR_PED_MALE(charCreatorData)
		pedToApply = charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON]
	ELSE
		pedToApply = charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER]
	ENDIF
	
	IF charCreatorData.crewTshirtOption = CREW_TSHIRT_OFF
		REMOVE_TATTOOS_IN_UPGRADE_GROUP_MP(GET_HASH_KEY("crewLogo"), pedToApply)
		IF NOT bFromChangeShirt
			UPDATE_TATOOS_MP(pedToApply)	
		ENDIF
	ELSE
		IF NOT IS_PED_INJURED(pedToApply)
			SWITCH charCreatorData.crewDecalOption	
				CASE CREW_DECAL_NONE
					REMOVE_TATTOOS_IN_UPGRADE_GROUP_MP(GET_HASH_KEY("crewLogo"), pedToApply)
					UPDATE_TATOOS_MP(pedToApply)	
				BREAK
				CASE CREW_DECAL_SMALL
					EQUIP_CREW_LOGO(pedToApply, TATTOO_MP_FM_CREW_A)
				BREAK
				CASE CREW_DECAL_BIG
					IF IS_CHAR_CREATOR_PED_MALE(charCreatorData)
						EQUIP_CREW_LOGO(pedToApply, TATTOO_MP_FM_CREW_B)
					ELSE
						EQUIP_CREW_LOGO(pedToApply, TATTOO_MP_FM_CREW_C)
					ENDIF
				BREAK
				CASE CREW_DECAL_BACK
					EQUIP_CREW_LOGO(pedToApply, TATTOO_MP_FM_CREW_C)
				BREAK
			ENDSWITCH
		ENDIF
	ENDIF
ENDPROC

// get hat
FUNC INT GET_CURRENT_CHAR_CREATOR_HAT_OPTION(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData, BOOL bIsMale, BOOL bUseDummy = FALSE)
	PED_INDEX checkPed
	
	IF bIsMale
		IF NOT bUseDummy
			checkPed = charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON]
		ELSE
			checkPed = charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON_DUMMY]
		ENDIF
	ELSE
		IF NOT bUseDummy
			checkPed = charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER]
		ELSE
			checkPed = charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER_DUMMY]
		ENDIF
	ENDIF
	
	IF NOT IS_PED_INJURED(checkPed)
		INT i
		
		PED_COMP_NAME_ENUM thisComp = GET_PED_COMP_ITEM_CURRENT_MP(checkPed, COMP_TYPE_PROPS, ENUM_TO_INT(ANCHOR_HEAD))
		REPEAT GET_NUM_OF_HAT_OPTIONS_FOR_SPAWN_OUTFIT(bIsMale, charCreatorData.pedData.charOutfit) i
			IF thisComp = GET_HAT_OPTION_FOR_SPAWN_OUTFIT(bIsMale, charCreatorData.pedData.charOutfit, i)
				RETURN i
			ENDIF
		ENDREPEAT
	ENDIF

	RETURN -1
ENDFUNC

// get glasses
FUNC INT GET_CURRENT_CHAR_CREATOR_GLASSES_OPTION(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData, BOOL bIsMale, BOOL bUseDummy = FALSE)
	PED_INDEX checkPed
	
	IF bIsMale
		IF NOT bUseDummy
			checkPed = charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON]
		ELSE
			checkPed = charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON_DUMMY]
		ENDIF
	ELSE
		IF NOT bUseDummy
			checkPed = charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER]
		ELSE
			checkPed = charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER_DUMMY]
		ENDIF
	ENDIF
	
	IF NOT IS_PED_INJURED(checkPed)
		INT i
		
		PED_COMP_NAME_ENUM thisComp = GET_PED_COMP_ITEM_CURRENT_MP(checkPed, COMP_TYPE_PROPS, ENUM_TO_INT(ANCHOR_EYES))
		REPEAT GET_NUM_OF_GLASSES_OPTIONS_FOR_SPAWN_OUTFIT(bIsMale, charCreatorData.pedData.charOutfit) i
			IF thisComp = GET_GLASSES_OPTION_FOR_SPAWN_OUTFIT(bIsMale, charCreatorData.pedData.charOutfit, i)
				RETURN i
			ENDIF
		ENDREPEAT
	ENDIF

	RETURN -1
ENDFUNC

		
PROC SET_CHAR_CREATOR_CREW_TSHIRT(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData, BOOL bOnlyUpdateDaughter, BOOL bOnlyUpdateSon)
	IF charCreatorData.crewTshirtOption <> CREW_TSHIRT_OFF
		IF DOES_ENTITY_EXIST(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON])
		AND NOT bOnlyUpdateDaughter
		
			PRINTLN("[CHARCREATOR X] SET_CHAR_CREATOR_CREW_TSHIRT - Applying to SON: ", NATIVE_TO_INT(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON]), " tshirt: ", charCreatorData.crewTShirtOption)
			PRINTLN("[CHARCREATOR X] SET_CHAR_CREATOR_CREW_TSHIRT - Applying to SON DUMMY: ", NATIVE_TO_INT(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON_DUMMY]), " tshirt: ", charCreatorData.crewTShirtOption)
		
			GIVE_PLAYER_CREW_TSHIRT(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON], charCreatorData.crewTShirtOption)
			GIVE_PLAYER_CREW_TSHIRT(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON_DUMMY], charCreatorData.crewTShirtOption)

			SET_PED_COMP_ITEM_CURRENT_MP(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON], COMP_TYPE_TEETH, TEETH_FMM_0_0, FALSE)
			SET_PED_COMP_ITEM_CURRENT_MP(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON_DUMMY], COMP_TYPE_TEETH, TEETH_FMM_0_0, FALSE)
		ENDIF
		IF DOES_ENTITY_EXIST(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER])
		AND NOT bOnlyUpdateSon
		
			PRINTLN("[CHARCREATOR X] SET_CHAR_CREATOR_CREW_TSHIRT - Applying to DAUGHTER: ", NATIVE_TO_INT(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER]), " tshirt: ", charCreatorData.crewTShirtOption)
			PRINTLN("[CHARCREATOR X] SET_CHAR_CREATOR_CREW_TSHIRT - Applying to DAUGHTER DUMMY: ", NATIVE_TO_INT(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER_DUMMY]), " tshirt: ", charCreatorData.crewTShirtOption)
		
			GIVE_PLAYER_CREW_TSHIRT(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER], charCreatorData.crewTShirtOption)
			GIVE_PLAYER_CREW_TSHIRT(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER_DUMMY], charCreatorData.crewTShirtOption)
		ENDIF
	ENDIF
ENDPROC
		

/// PURPOSE: Loops through hours assigned to lifestyles and dresses accordingly
PROC SET_CHAR_CREATOR_OUTFIT(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData, BOOL bRestoreOldClothes = FALSE, BOOL bForce = FALSE, BOOL bOnlyUpdateSon = FALSE, BOOL bOnlyUpdateDaughter = FALSE, BOOL bFromChangingTShirt = FALSE, BOOL bFromLifestyleChange = FALSE, BOOL bLifestyleChangeRandom = FALSE, BOOL bSetFromTShirtCancel = FALSE)
	bFromLifestyleChange=bFromLifestyleChange
	PRINTLN("[CHARCREATOR] SET_CHAR_CREATOR_OUTFIT - Current outfit: ", GET_CHAR_CREATOR_DEBUG_OUTFIT_TEXT(charCreatorData.pedData.charOutfit))
	
	SET_CURRENT_MP_SLOT_FOR_CLOTHES(charCreatorData.iCharacterSelectSlot)

	CHAR_CREATOR_LIFESTYLE maxLifestyle = GET_MAX_LIFESTYLE(charCreatorData)
	INT iLockLifestyleSlot = ENUM_TO_INT(maxLifestyle) - 1

	MP_SPAWN_OUTFIT_TYPE newOutfit = GET_CHAR_CREATOR_OUTFIT_FOR_LIFESTYLE(maxLifestyle)
	
	// Check to see if we have changed our outfits
	IF newOutfit != charCreatorData.pedData.charOutfit
	OR bForce
	OR NOT charCreatorData.bInitialisedOutfit
		// Check we can apply the new clothes
		PRINTLN("[CHARCREATOR] SET_CHAR_CREATOR_OUTFIT - New outfit for lifestyle: ", GET_CHAR_CREATOR_LIFESTYLE_TEXT_LABEL(maxLifestyle), " type: ", GET_CHAR_CREATOR_DEBUG_OUTFIT_TEXT(newOutfit))
		charCreatorData.pedData.charOutfit = newOutfit

		IF ((bRestoreOldClothes
		AND charCreatorData.charCreatorState > CHARACTER_CREATOR_WAIT_FOR_FRONTEND)
		OR ((charCreatorData.bLockedMaleLifestyle[iLockLifestyleSlot] AND NOT bOnlyUpdateDaughter)
		AND (charCreatorData.bLockedFemaleLifestyle[iLockLifestyleSlot] AND NOT bOnlyUpdateSon)
		AND charCreatorData.lifestyleIndex <> maxLifestyle))
		AND NOT bSetFromTShirtCancel
			IF DOES_ENTITY_EXIST(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON_DUMMY])
			AND NOT bOnlyUpdateDaughter
				
				
				// restore backed up outfit and props
				IF NOT charCreatorData.bSonLockedToOldOutfit
					g_sTempOutfitData = charCreatorData.cacheMaleOutfitData[iLockLifestyleSlot]
					
					IF IS_CHAR_CREATOR_PED_MALE(charCreatorData)
						REMOVE_TATTOOS_IN_UPGRADE_GROUP_MP(GET_HASH_KEY("torsoDecal"), charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON_DUMMY])
					ENDIF
					SET_PED_COMP_ITEM_CURRENT_MP(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON_DUMMY], COMP_TYPE_OUTFIT, OUTFIT_DEFAULT, FALSE, -1, TRUE)
					IF IS_CHAR_CREATOR_PED_MALE(charCreatorData)
						EQUIP_TORSO_DECALS_MP(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON_DUMMY], COMP_TYPE_JBIB, GET_PED_COMP_ITEM_CURRENT_MP(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON_DUMMY], COMP_TYPE_JBIB), FALSE)	// re-equip decals
					ENDIF
				ENDIF

				CLEAR_ALL_PED_PROPS(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON_DUMMY])
				
				IF charCreatorData.iCacheMaleHatOption[iLockLifestyleSlot] >= 0
					SET_PED_COMP_ITEM_CURRENT_MP(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON_DUMMY], COMP_TYPE_PROPS, GET_HAT_OPTION_FOR_SPAWN_OUTFIT(TRUE, charCreatorData.pedData.charOutfit, charCreatorData.iCacheMaleHatOption[iLockLifestyleSlot]), FALSE)
				ENDIF
				IF charCreatorData.iCacheMaleGlassesOption[iLockLifestyleSlot] >= 0	
					SET_PED_COMP_ITEM_CURRENT_MP(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON_DUMMY], COMP_TYPE_PROPS, GET_GLASSES_OPTION_FOR_SPAWN_OUTFIT(TRUE, charCreatorData.pedData.charOutfit, charCreatorData.iCacheMaleGlassesOption[iLockLifestyleSlot]), FALSE)
				ENDIF
				
				// restore hair
				SET_CHAR_CREATOR_HAIR(charCreatorData, CHAR_CREATOR_GENDER_MALE, charCreatorData.iCacheMaleHair[iLockLifestyleSlot])
				SET_CHAR_CREATOR_HAIR_COLOUR(charCreatorData, CHAR_CREATOR_GENDER_MALE, charCreatorData.iCacheMaleHairColour[iLockLifestyleSlot])
				GIVE_CHAR_CREATOR_PED_HAIR(charCreatorData, CHAR_CREATOR_PED_SON_DUMMY, FALSE)
				
				charCreatorData.pedData.charOverlays[ENUM_TO_INT(CHAR_CREATOR_BEARD)].iOverlayVal = charCreatorData.iCacheMaleBeard[iLockLifestyleSlot]
				charCreatorData.pedData.charOverlays[ENUM_TO_INT(CHAR_CREATOR_BEARD)].fOverlayBlend = 1.0
				
				SET_BIT(charCreatorData.iCharCreatorBitset, CHAR_CREATOR_COPY_FROM_SON_DUMMY)					
				charCreatorData.iSetSonDummyTime = GET_GAME_TIMER()		
				
			ENDIF					
			
			IF DOES_ENTITY_EXIST(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER_DUMMY])
			AND NOT bOnlyUpdateSon
				// restore backed up outfit and props

				IF NOT charCreatorData.bDaughterLockedToOldOutfit
					g_sTempOutfitData = charCreatorData.cacheFemaleOutfitData[iLockLifestyleSlot]
					
					IF NOT IS_CHAR_CREATOR_PED_MALE(charCreatorData)
						REMOVE_TATTOOS_IN_UPGRADE_GROUP_MP(GET_HASH_KEY("torsoDecal"), charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER_DUMMY])
					ENDIF
					SET_PED_COMP_ITEM_CURRENT_MP(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER_DUMMY], COMP_TYPE_OUTFIT, OUTFIT_DEFAULT, FALSE, -1, TRUE)
					
					IF NOT IS_CHAR_CREATOR_PED_MALE(charCreatorData)
						EQUIP_TORSO_DECALS_MP(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER_DUMMY], COMP_TYPE_JBIB, GET_PED_COMP_ITEM_CURRENT_MP(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER_DUMMY], COMP_TYPE_JBIB), FALSE)
					ENDIF
				ENDIF
				//UPDATE_TATOOS_MP(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER_DUMMY])
				CLEAR_ALL_PED_PROPS(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER_DUMMY])
				
				IF charCreatorData.iCacheFemaleHatOption[iLockLifestyleSlot] >= 0
					SET_PED_COMP_ITEM_CURRENT_MP(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER_DUMMY], COMP_TYPE_PROPS, GET_HAT_OPTION_FOR_SPAWN_OUTFIT(FALSE, charCreatorData.pedData.charOutfit, charCreatorData.iCacheFemaleHatOption[iLockLifestyleSlot]), FALSE)
				ENDIF
				IF charCreatorData.iCacheFemaleGlassesOption[iLockLifestyleSlot] >= 0	
					SET_PED_COMP_ITEM_CURRENT_MP(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER_DUMMY], COMP_TYPE_PROPS, GET_GLASSES_OPTION_FOR_SPAWN_OUTFIT(FALSE, charCreatorData.pedData.charOutfit, charCreatorData.iCacheFemaleGlassesOption[iLockLifestyleSlot]), FALSE)
				ENDIF		
				
				// restore hair
				SET_CHAR_CREATOR_HAIR(charCreatorData, CHAR_CREATOR_GENDER_FEMALE, charCreatorData.iCacheFemaleHair[iLockLifestyleSlot])
				SET_CHAR_CREATOR_HAIR_COLOUR(charCreatorData, CHAR_CREATOR_GENDER_FEMALE, charCreatorData.iCacheFemaleHairColour[iLockLifestyleSlot])
				GIVE_CHAR_CREATOR_PED_HAIR(charCreatorData, CHAR_CREATOR_PED_DAUGHTER_DUMMY, FALSE)
				
				charCreatorData.pedData.charOverlays[ENUM_TO_INT(CHAR_CREATOR_MAKEUP)].iOverlayVal = charCreatorData.iCacheFemaleMakeup[iLockLifestyleSlot]
				charCreatorData.pedData.charOverlays[ENUM_TO_INT(CHAR_CREATOR_MAKEUP)].fOverlayBlend = 1.0

				SET_BIT(charCreatorData.iCharCreatorBitset, CHAR_CREATOR_COPY_FROM_DAUGHTER_DUMMY)					
				charCreatorData.iSetDaughterDummyTime = GET_GAME_TIMER()				
			ENDIF	
			
			IF maxLifestyle = charCreatorData.lifestyleIndex
				charCreatorData.bLockedMaleLifestyle[iLockLifestyleSlot] = FALSE
				charCreatorData.bLockedFemaleLifestyle[iLockLifestyleSlot] = FALSE
			ENDIF
		ELSE
			// random hair and makeup
			IF NOT bSetFromTShirtCancel
				GENERATE_CHAR_CREATOR_RANDOM_HAIR(charCreatorData, newOutfit, NOT bLifestyleChangeRandom, FALSE, FALSE, bOnlyUpdateSon, bOnlyUpdateDaughter)
				
				IF NOT bOnlyUpdateSon
					GENERATE_CHAR_CREATOR_RANDOM_MAKEUP(charCreatorData, newOutfit, NOT bLifestyleChangeRandom)	
				ENDIF
			ENDIF
		
			IF bSetFromTShirtCancel
			OR NOT bFromChangingTShirt
				IF DOES_ENTITY_EXIST(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON_DUMMY])
				AND NOT bOnlyUpdateDaughter
					IF NOT bSetFromTShirtCancel
						GIVE_CHAR_CREATOR_PED_HAIR(charCreatorData, CHAR_CREATOR_PED_SON_DUMMY, FALSE)
					ENDIF
					SETUP_MP_SPAWN_OUTFIT_MALE(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON_DUMMY], newOutfit, DEFAULT, DEFAULT, FALSE)//IS_CHAR_CREATOR_PED_MALE(charCreatorData))
					charCreatorData.cacheMaleOutfitData[iLockLifestyleSlot] = g_sTempOutfitData 
					charCreatorData.bLockedMaleLifestyle[iLockLifestyleSlot] = TRUE
					
					// cache hair and glasses
					charCreatorData.iCacheMaleHatOption[iLockLifestyleSlot] = GET_CURRENT_CHAR_CREATOR_HAT_OPTION(charCreatorData, TRUE, TRUE)
					charCreatorData.iCacheMaleGlassesOption[iLockLifestyleSlot] = GET_CURRENT_CHAR_CREATOR_GLASSES_OPTION(charCreatorData, TRUE, TRUE)
					
					GIVE_CHAR_CREATOR_PED_HAIR(charCreatorData, CHAR_CREATOR_PED_SON_DUMMY, FALSE)
					
					SET_BIT(charCreatorData.iCharCreatorBitset, CHAR_CREATOR_COPY_FROM_SON_DUMMY)					
					charCreatorData.iSetSonDummyTime = GET_GAME_TIMER()
					charCreatorData.bSonLockedToOldOutfit = FALSE
				ENDIF
				IF DOES_ENTITY_EXIST(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER_DUMMY])
				AND NOT bOnlyUpdateSon
					IF NOT bSetFromTShirtCancel
						GIVE_CHAR_CREATOR_PED_HAIR(charCreatorData, CHAR_CREATOR_PED_DAUGHTER_DUMMY, FALSE)
					ENDIF
					SETUP_MP_SPAWN_OUTFIT_FEMALE(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER_DUMMY], newOutfit, DEFAULT, FALSE)//!IS_CHAR_CREATOR_PED_MALE(charCreatorData))
					charCreatorData.cacheFemaleOutfitData[iLockLifestyleSlot] = g_sTempOutfitData 
					charCreatorData.bLockedFemaleLifestyle[iLockLifestyleSlot] = TRUE	
					
					// cache hair and glasses
					charCreatorData.iCacheFemaleHatOption[iLockLifestyleSlot] = GET_CURRENT_CHAR_CREATOR_HAT_OPTION(charCreatorData, FALSE, TRUE)
					charCreatorData.iCacheFemaleGlassesOption[iLockLifestyleSlot] = GET_CURRENT_CHAR_CREATOR_GLASSES_OPTION(charCreatorData, FALSE, TRUE)	
					
					GIVE_CHAR_CREATOR_PED_HAIR(charCreatorData, CHAR_CREATOR_PED_DAUGHTER_DUMMY, FALSE)
					
					SET_BIT(charCreatorData.iCharCreatorBitset, CHAR_CREATOR_COPY_FROM_DAUGHTER_DUMMY)	
					charCreatorData.iSetDaughterDummyTime = GET_GAME_TIMER()
					
					charCreatorData.bDaughterLockedToOldOutfit = FALSE
				ENDIF
			ENDIF
		ENDIF
		
		SET_CHAR_CREATOR_CREW_TSHIRT(charCreatorData, bOnlyUpdateDaughter, bOnlyUpdateSon)

		IF charCreatorData.mainMenuIndex = HERITAGE_CAT
			CLEAR_ALL_PED_PROPS(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON])
			CLEAR_ALL_PED_PROPS(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER])
			CLEAR_ALL_PED_PROPS(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON_DUMMY])
			CLEAR_ALL_PED_PROPS(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER_DUMMY])	
		ENDIF
		
		charCreatorData.bInitialisedOutfit = TRUE
	ENDIF
ENDPROC

/// PURPOSE: Based on lifestyle / outfit the player receives some standard supplies for their inventory 
PROC UPDATE_CHAR_CREATOR_PLAYER_INVENTORY(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData, INT iSlot)

	BOOL bIsMale = IS_CHAR_CREATOR_PED_MALE(charCreatorData)
	//MP_SPAWN_OUTFIT_TYPE outfit = charCreatorData.pedData.charOutfit

	/*
	SWITCH outfit
		CASE MPSO_CASUAL
			IF bIsMale
				enumGlasses = PROPS_FMM_GLASSES_7_0
				enumHats = PROPS_FMM_HAT_5_0
			ELSE
				enumGlasses = PROPS_FMF_GLASSES_2_0
				enumHats = PROPS_FMF_HAT_11_1
			ENDIF
		BREAK
		
		CASE MPSO_COUCH_POTATO
			IF bIsMale
				enumGlasses = PROPS_FMM_GLASSES_2_0
				enumHats = PROPS_FMM_HAT_9_7
			ELSE
				enumGlasses = PROPS_FMF_GLASSES_7_0
				enumHats = PROPS_FMF_HAT_5_0
			ENDIF
		BREAK
		
		CASE MPSO_ILLEGAL_WORK
			IF bIsMale
				enumGlasses = PROPS_FMM_GLASSES_5_0
				enumHats = PROPS_FMM_HAT_14_1
			ELSE
				enumGlasses = PROPS_FMF_GLASSES_11_0
				enumHats = PROPS_FMF_HAT_4_0
			ENDIF
		BREAK
		
		CASE MPSO_LEGAL_WORK
			IF bIsMale
				enumGlasses = PROPS_FMM_GLASSES_10_0
				enumHats = PROPS_FMM_HAT_12_2
			ELSE
				enumGlasses = PROPS_FMF_GLASSES_14_0
				enumHats = PROPS_FMF_HAT_14_0
			ENDIF
		BREAK
		
		CASE MPSO_PARTYING
			IF bIsMale
				enumGlasses = PROPS_FMM_GLASSES_12_7
				enumHats = PROPS_FMM_HAT_4_0
			ELSE
				enumGlasses = PROPS_FMF_GLASSES_6_0
				enumHats = PROPS_FMF_HAT_13_6
			ENDIF
		BREAK
		
		CASE MPSO_SPORT
			IF bIsMale
				enumGlasses = PROPS_FMM_GLASSES_15_0
				enumHats = PROPS_FMM_HAT_10_5
			ELSE
				enumGlasses = PROPS_FMF_GLASSES_9_0
				enumHats = PROPS_FMF_HAT_9_0
			ENDIF
		BREAK
		
		DEFAULT
			EXIT
		BREAK
		
	ENDSWITCH
	*/
	
	MODEL_NAMES mnModel = GET_PLAYER_MODEL_FOR_TEAM_VARIATION_FOR_MENU(GET_STAT_CHARACTER_TEAM(iSlot), PICK_INT(bIsMale, 0, 1))

	CHAR_CREATOR_LIFESTYLE maxLifestyle = GET_MAX_LIFESTYLE(charCreatorData)
	INT iLockLifestyleSlot = ENUM_TO_INT(maxLifestyle) - 1	
	
	PED_COMP_NAME_ENUM equipHat
	PED_COMP_NAME_ENUM equipGlasses
	IF IS_CHAR_CREATOR_PED_MALE(charCreatorData) 
		IF charCreatorData.iCacheMaleHatOption[iLockLifestyleSlot] >= 0
			equipHat = GET_HAT_OPTION_FOR_SPAWN_OUTFIT(TRUE, charCreatorData.pedData.charOutfit, charCreatorData.iCacheMaleHatOption[iLockLifestyleSlot])
			SET_PED_COMP_ITEM_AVAILABLE_MP(mnModel, COMP_TYPE_PROPS, equipHat, TRUE)
			SET_PED_COMP_ITEM_ACQUIRED_MP(mnModel, COMP_TYPE_PROPS, equipHat, TRUE)
		ENDIF
		
		IF charCreatorData.iCacheMaleGlassesOption[iLockLifestyleSlot] >= 0
			equipGlasses = GET_GLASSES_OPTION_FOR_SPAWN_OUTFIT(TRUE, charCreatorData.pedData.charOutfit, charCreatorData.iCacheMaleGlassesOption[iLockLifestyleSlot])
			SET_PED_COMP_ITEM_AVAILABLE_MP(mnModel, COMP_TYPE_PROPS, equipGlasses, TRUE)
			SET_PED_COMP_ITEM_ACQUIRED_MP(mnModel, COMP_TYPE_PROPS, equipGlasses, TRUE)
		ENDIF
	ELSE
		IF charCreatorData.iCacheFemaleHatOption[iLockLifestyleSlot] >= 0
			equipHat = GET_HAT_OPTION_FOR_SPAWN_OUTFIT(FALSE, charCreatorData.pedData.charOutfit, charCreatorData.iCacheFemaleHatOption[iLockLifestyleSlot])
			SET_PED_COMP_ITEM_AVAILABLE_MP(mnModel, COMP_TYPE_PROPS, equipHat, TRUE)
			SET_PED_COMP_ITEM_ACQUIRED_MP(mnModel, COMP_TYPE_PROPS, equipHat, TRUE)
		ENDIF
		
		IF charCreatorData.iCacheFemaleGlassesOption[iLockLifestyleSlot] >= 0
			equipGlasses = GET_GLASSES_OPTION_FOR_SPAWN_OUTFIT(FALSE, charCreatorData.pedData.charOutfit, charCreatorData.iCacheFemaleGlassesOption[iLockLifestyleSlot])
			SET_PED_COMP_ITEM_AVAILABLE_MP(mnModel, COMP_TYPE_PROPS, equipGlasses, TRUE)
			SET_PED_COMP_ITEM_ACQUIRED_MP(mnModel, COMP_TYPE_PROPS, equipGlasses, TRUE)
		ENDIF	
	ENDIF
	
	// Calculate if we should give the smokes to the player
	
	INT iTotalSmokePos 
	iTotalSmokePos += GET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_PARTY)
	iTotalSmokePos += GET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_TV)
	iTotalSmokePos += GET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_ILLEGAL)
	
	INT iTotalSmokeNeg 
	iTotalSmokePos += GET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_SPORT)
	iTotalSmokePos += GET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_LEGAL)
	iTotalSmokePos += GET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_FAMILY)
	
	// Only give smoke if the bad lifestyle outweigh the good
	IF iTotalSmokePos > iTotalSmokeNeg
		
		// If we heavily choose bad lifestyle to good give more smokes
		IF iTotalSmokePos > (2*iTotalSmokeNeg)
			PRINTLN("[CHARCREATOR] UPDATE_CHAR_CREATOR_PLAYER_INVENTORY - Giving player 10 smokes, iTotalSmokePos = ", iTotalSmokePos, ", iTotalSmokeNeg = ", iTotalSmokeNeg)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_CIGARETTES_BOUGHT, 10, iSlot)
		ELSE
			PRINTLN("[CHARCREATOR] UPDATE_CHAR_CREATOR_PLAYER_INVENTORY - Giving player 5 smokes, iTotalSmokePos = ", iTotalSmokePos, ", iTotalSmokeNeg = ", iTotalSmokeNeg)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_CIGARETTES_BOUGHT, 5, iSlot)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Calculates the overlay percentage
PROC SET_CHAR_CREATOR_OVERLAY_VALUES(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData, FLOAT &fPercent, CHAR_CREATOR_OVERLAY aOverlay)
	
	IF aOverlay = CHAR_CREATOR_EYEBROW
	
		charCreatorData.pedData.charOverlays[aOverlay].iOverlayVal =1
		charCreatorData.pedData.charOverlays[aOverlay].fOverlayBlend = 1.0
		
		charCreatorData.pedData.iDaughterEyebrow = GET_RANDOM_INT_IN_RANGE(1, 12)
		charCreatorData.pedData.iSonEyebrow = GET_RANDOM_INT_IN_RANGE(12, 20)
		
	
		PRINTLN("[CHARCREATOR] SET_CHAR_CREATOR_OVERLAY_VALUES - daughter eyebrow: ", charCreatorData.pedData.iDaughterEyebrow)
		PRINTLN("[CHARCREATOR] SET_CHAR_CREATOR_OVERLAY_VALUES - son eyebrow: ", charCreatorData.pedData.iSonEyebrow)
	
		EXIT
	ENDIF
	
	IF fPercent <= 0
		fPercent = 0.0
	
		charCreatorData.pedData.charOverlays[aOverlay].iOverlayVal 		= -1
		charCreatorData.pedData.charOverlays[aOverlay].fOverlayBlend 	= 0.0
		
		PRINTLN("[CHARCREATOR] SET_CHAR_CREATOR_OVERLAY_VALUES - cleared overlay: ", GET_CHAR_CREATOR_OVERLAY_STRING(aOverlay))
		
		EXIT
	ENDIF
		
	INT iMaxVal = GET_PED_HEAD_OVERLAY_NUM(ENUM_TO_INT(aOverlay))
	
	IF aOverlay = CHAR_CREATOR_MAKEUP
		iMaxVal = 15
	ENDIF
	
	IF fPercent >= 1.0
		fPercent = 1.0
	
		charCreatorData.pedData.charOverlays[aOverlay].iOverlayVal 		= (iMaxVal-1)
		charCreatorData.pedData.charOverlays[aOverlay].fOverlayBlend 	= 1.0//0.8
		
		PRINTLN("[CHARCREATOR] SET_CHAR_CREATOR_OVERLAY_VALUES - set max overlay: ", GET_CHAR_CREATOR_OVERLAY_STRING(aOverlay))
	
		EXIT
	ENDIF
	
	// Set the correct overlay amount
	charCreatorData.pedData.charOverlays[aOverlay].iOverlayVal 		= FLOOR((iMaxVal+1) * fPercent)
	charCreatorData.pedData.charOverlays[aOverlay].fOverlayBlend	= 1.0//(0.5 + (0.3 * fPercent))
	
	// specific overlay vals as requested
	
	IF aOverlay = CHAR_CREATOR_WRINKLES
		IF fPercent < 0.40 // 20-35
			charCreatorData.pedData.charOverlays[aOverlay].iOverlayVal = 0
		ELIF fPercent < 0.50// 35-45
			charCreatorData.pedData.charOverlays[aOverlay].iOverlayVal = 1
		ELIF fPercent < 0.60// 55-60
			charCreatorData.pedData.charOverlays[aOverlay].iOverlayVal = 2
		ELIF fPercent < 0.70 // 55-60
			charCreatorData.pedData.charOverlays[aOverlay].iOverlayVal = 3
		ELIF fPercent < 0.80 // 55-60
			charCreatorData.pedData.charOverlays[aOverlay].iOverlayVal = 4
		ELIF fPercent < 0.90 // 55-60
			charCreatorData.pedData.charOverlays[aOverlay].iOverlayVal = 5
		ELSE // 55-60
			charCreatorData.pedData.charOverlays[aOverlay].iOverlayVal = 6
		ENDIF
	ENDIF
	
	IF aOverlay = CHAR_CREATOR_DAMAGE
		IF fPercent < 0.40 
			charCreatorData.pedData.charOverlays[aOverlay].iOverlayVal = 0
		ELIF fPercent < 0.60
			charCreatorData.pedData.charOverlays[aOverlay].iOverlayVal = 1
		ELIF fPercent < 0.80 
			charCreatorData.pedData.charOverlays[aOverlay].iOverlayVal = 2
		ELSE
			charCreatorData.pedData.charOverlays[aOverlay].iOverlayVal = 3
		ENDIF
	ENDIF
	
	IF aOverlay = CHAR_CREATOR_BLEMISH
		charCreatorData.pedData.charOverlays[aOverlay].fOverlayBlend	= 1.0//0.5
	ENDIF
	
	// get it in correct range
	charCreatorData.pedData.charOverlays[aOverlay].iOverlayVal--
	
	PRINTLN("[CHARCREATOR] SET_CHAR_CREATOR_OVERLAY_VALUES - set overlay: ", GET_CHAR_CREATOR_OVERLAY_STRING(aOverlay), " to: ", 
				charCreatorData.pedData.charOverlays[aOverlay].iOverlayVal, " and Fade: ", charCreatorData.pedData.charOverlays[aOverlay].fOverlayBlend)

ENDPROC

/// PURPOSE: Clear all our hour data
PROC RESET_CHAR_CREATOR_LIFESTYLE(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData)
	
	PRINTLN("[CHARCREATOR] RESET_CHAR_CREATOR_LIFESTYLE - Resetting our Lifestyle hours")
	
	INT i
	REPEAT MAX_LIFESTYLE i
		charCreatorData.pedData.charLifestyleHours[i] = 0
	ENDREPEAT
	
	charCreatorData.pedData.iHoursLeftToAssign = CHAR_CREATOR_MAX_HOURS	
ENDPROC


FUNC BOOL IS_CHAR_CREATOR_LIFESTYLE_VALID(CHAR_CREATOR_LIFESTYLE lifestyle, CHAR_CREATOR_ARCHETYPE_ENUM archetype)

	SWITCH archetype
		CASE ARCHETYPE_GOOD	// Good character
			SWITCH lifestyle
				CASE LIFESTYLE_ILLEGAL
				CASE LIFESTYLE_TV
				CASE LIFESTYLE_PARTY
					RETURN FALSE
			ENDSWITCH
		BREAK
	
		CASE ARCHETYPE_BAD	// Bad Character
			SWITCH lifestyle
				CASE LIFESTYLE_LEGAL
				CASE LIFESTYLE_SPORT
				CASE LIFESTYLE_FAMILY
					RETURN FALSE
			ENDSWITCH
		BREAK
		
		CASE ARCHETYPE_LAZY	// Lazy Character
			SWITCH lifestyle
				CASE LIFESTYLE_SPORT
				CASE LIFESTYLE_LEGAL
				CASE LIFESTYLE_ILLEGAL
					RETURN FALSE
			ENDSWITCH
		BREAK
		
		CASE ARCHETYPE_ACTIVE	// Active Character
			SWITCH lifestyle
				CASE LIFESTYLE_TV
				CASE LIFESTYLE_SLEEP
					RETURN FALSE
			ENDSWITCH
		BREAK
		
		CASE ARCHETYPE_PARTY	// Party Character
			SWITCH lifestyle
				CASE LIFESTYLE_SPORT
				CASE LIFESTYLE_SLEEP
				CASE LIFESTYLE_TV
					RETURN FALSE
			ENDSWITCH
		BREAK		
		
		CASE ARCHETYPE_UGLY	// Ugly Character
			SWITCH lifestyle
				CASE LIFESTYLE_SPORT
				CASE LIFESTYLE_LEGAL
				CASE LIFESTYLE_SLEEP
				CASE LIFESTYLE_FAMILY
					RETURN FALSE
			ENDSWITCH
		BREAK					
	ENDSWITCH

	RETURN TRUE
ENDFUNC

/// PURPOSE: Apply the overlay for the wrinkles calculated from age
PROC APPLY_CHAR_CREATOR_WRINKLES_OVERLAY(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData, BOOL bOnlySon = FALSE, BOOL bOnlyDaughter = FALSE)

	INT iValue
	FLOAT fBlend
	
	GET_CHAR_CREATOR_OVERLAY_VALUES(charCreatorData, CHAR_CREATOR_WRINKLES, iValue, fBlend)
	PRINTLN("[CHARCREATOR] SET_CHAR_CREATOR_WRINKLES_OVERLAY - WRINKLES: ", iValue, " Blend: ", fBlend)
	IF NOT IS_PED_INJURED(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON])
	AND NOT IS_PED_INJURED(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER])
		IF NOT bOnlyDaughter
			SET_PED_HEAD_OVERLAY(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON], HOS_AGING, iValue, fBlend)
		ENDIF
		IF NOT bOnlySon
			SET_PED_HEAD_OVERLAY(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER], HOS_AGING, iValue, fBlend)
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE: Refresh the value of our wrinkle percentage based on age
PROC CALC_CHAR_CREATOR_AGE_OVERLAY(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData)
	
	INT iTempAge = GET_CHAR_CREATOR_AGE(charCreatorData) - CHAR_CREATOR_MIN_AGE
	FLOAT fPercentage = TO_FLOAT(iTempAge) / TO_FLOAT(CHAR_CREATOR_MAX_AGE - CHAR_CREATOR_MIN_AGE)
	
	PRINTLN("[CHARCREATOR] CALC_CHAR_CREATOR_AGE_OVERLAY - fPercentage: ", fPercentage)
	
	#IF IS_DEBUG_BUILD
		charCreatorData.iWrinklesPercent = ROUND(fPercentage * 100)
	#ENDIF
	
	SET_CHAR_CREATOR_OVERLAY_VALUES(charCreatorData, fPercentage, CHAR_CREATOR_WRINKLES)
	
	APPLY_CHAR_CREATOR_WRINKLES_OVERLAY(charCreatorData)
ENDPROC

/// PURPOSE: Generates a random distribution of hours for lifestyle hours
PROC GENERATE_CHAR_CREATOR_RANDOM_LIFESTYLE(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData, BOOL bAllowDup = FALSE, BOOL bInitSetup = FALSE)

	PRINTLN("[CHARCREATOR] GENERATE_CHAR_CREATOR_RANDOM_LIFESTYLE - ")
	
	charCreatorData.pedData.iHoursLeftToAssign = (CHAR_CREATOR_MAX_HOURS - GET_CHAR_CREATOR_HOURS_BOUNDS_FOR_LIFESTYLE(LIFESTYLE_SLEEP, FALSE))
	CHAR_CREATOR_LIFESTYLE lifestyleToChange
	
	// Set a random age
	IF bInitSetup
		PRINTLN("[CHARCREATOR] GENERATE_CHAR_CREATOR_RANDOM_LIFESTYLE - Set age to default")
		SET_CHAR_CREATOR_AGE(charCreatorData, CHAR_CREATOR_DEFAULT_AGE)
	ELSE
		PRINTLN("[CHARCREATOR] GENERATE_CHAR_CREATOR_RANDOM_LIFESTYLE - Randomis age")
		SET_CHAR_CREATOR_AGE(charCreatorData, GET_RANDOM_INT_IN_RANGE(CHAR_CREATOR_MIN_AGE, (CHAR_CREATOR_MAX_AGE+1)))
	ENDIF
	
	CALC_CHAR_CREATOR_AGE_OVERLAY(charCreatorData)
	
	PRINTLN("[CHARCREATOR] GENERATE_CHAR_CREATOR_RANDOM_LIFESTYLE - STARTING AGE: ", GET_CHAR_CREATOR_AGE(charCreatorData))
	
	// Default our sleep to the minimum
	SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_SLEEP, GET_CHAR_CREATOR_HOURS_BOUNDS_FOR_LIFESTYLE(LIFESTYLE_SLEEP, FALSE))
	
	CHAR_CREATOR_LIFESTYLE lifestyleToSkip = MAX_LIFESTYLE
	
	//IF GET_RANDOM_INT_IN_RANGE(0, 3) = 0
	//	PRINTLN("[CHARCREATOR] GENERATE_CHAR_CREATOR_RANDOM_LIFESTYLE - PICKING A LIFESTYLE TO SKIP")
	//	lifestyleToSkip = INT_TO_ENUM(CHAR_CREATOR_LIFESTYLE, GET_RANDOM_INT_IN_RANGE(0, ENUM_TO_INT(MAX_LIFESTYLE)))
	//ENDIF
	
	INT iRandomLifestyle
	INT iAttempts = 0
	BOOL bFoundLifestyle = FALSE
	
	// If we are hitting this the first time, then limit the archetypes
	IF bInitSetup
		iRandomLifeStyle = GET_RANDOM_INT_IN_RANGE(0, 3)
		
		IF iRandomLifeStyle = 0
			PRINTSTRING("[CHARCREATOR] Initial setup - Set lifestyle to GOOD") PRINTNL()	
			iRandomLifeStyle = 2
		ELIF iRandomLifeStyle = 1
			PRINTSTRING("[CHARCREATOR] Initial setup - Set lifestyle to ACTIVE") PRINTNL()
			iRandomLifeStyle = 5
		ELSE
			PRINTSTRING("[CHARCREATOR] Initial setup - Set lifestyle to CUSTOM") PRINTNL()
			iRandomLifeStyle = 8
		ENDIF
		
		bFoundLifestyle = TRUE
	ENDIF
	
	WHILE iAttempts < 5
	AND NOT bFoundLifestyle
		iRandomLifestyle = GET_RANDOM_INT_IN_RANGE(0,9)
		IF bAllowDup
			bFoundLifestyle = TRUE
		ELSE
			IF iRandomLifestyle <> charCreatorData.iLastRandomLifestyle
				bFoundLifestyle = TRUE
			ELSE
				PRINTSTRING("same as last time lifestyle - try again") PRINTNL()				
			ENDIF
		ENDIF
		iAttempts++
	ENDWHILE
	charCreatorData.iLastRandomLifestyle = iRandomLifestyle		
	
	SWITCH iRandomLifestyle
		CASE 0
			// skip a lifestyle
			charCreatorData.pedData.archetype = ARCHETYPE_SKIP_LIFESTYLE
			PRINTLN("[CHARCREATOR] GENERATE_CHAR_CREATOR_RANDOM_LIFESTYLE - archetype = SKIP LIFESTYLE")	
			lifestyleToSkip = INT_TO_ENUM(CHAR_CREATOR_LIFESTYLE, GET_RANDOM_INT_IN_RANGE(0, ENUM_TO_INT(MAX_LIFESTYLE)))
		BREAK
		CASE 1
			// totally random
			charCreatorData.pedData.archetype = ARCHETYPE_RANDOM
			PRINTLN("[CHARCREATOR] GENERATE_CHAR_CREATOR_RANDOM_LIFESTYLE - archetype = RANDOM")	
		BREAK
		CASE 2
			// good
			charCreatorData.pedData.archetype = ARCHETYPE_GOOD
			PRINTLN("[CHARCREATOR] GENERATE_CHAR_CREATOR_RANDOM_LIFESTYLE - archetype = GOOD")	
		BREAK
		CASE 3
			// bad
			charCreatorData.pedData.archetype = ARCHETYPE_BAD
			PRINTLN("[CHARCREATOR] GENERATE_CHAR_CREATOR_RANDOM_LIFESTYLE - archetype = BAD")	
		BREAK
		CASE 4
			// lazy
			charCreatorData.pedData.archetype = ARCHETYPE_LAZY
			PRINTLN("[CHARCREATOR] GENERATE_CHAR_CREATOR_RANDOM_LIFESTYLE - archetype = LAZY")	
		BREAK
		CASE 5
			// active
			charCreatorData.pedData.archetype = ARCHETYPE_ACTIVE
			PRINTLN("[CHARCREATOR] GENERATE_CHAR_CREATOR_RANDOM_LIFESTYLE - archetype = ACTIVE")	
		BREAK
		CASE 6
			// party
			charCreatorData.pedData.archetype = ARCHETYPE_PARTY
			PRINTLN("[CHARCREATOR] GENERATE_CHAR_CREATOR_RANDOM_LIFESTYLE - archetype = PARTY")	
		BREAK
		CASE 7
			BOOL bIsUgly
			IF IS_CHAR_CREATOR_PED_MALE(charCreatorData) 
				bIsUgly = TRUE
			ELSE
				IF GET_RANDOM_INT_IN_RANGE(0,2) = 0
					bIsUgly = TRUE
				ELSE
					charCreatorData.pedData.archetype = ARCHETYPE_RANDOM
					PRINTLN("[CHARCREATOR] GENERATE_CHAR_CREATOR_RANDOM_LIFESTYLE - archetype = RANDOM from ugly")	
					bIsUgly = FALSE
				ENDIF
			ENDIF
			
			IF bIsUgly
				// ugly
				charCreatorData.pedData.archetype = ARCHETYPE_UGLY
				PRINTLN("[CHARCREATOR] GENERATE_CHAR_CREATOR_RANDOM_LIFESTYLE - archetype = UGLY")	

				// dump some hours into ugly stats if ugly
				SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_TV, 6)
				SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_PARTY, 10)
				SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_ILLEGAL, 10)
				charCreatorData.pedData.iHoursLeftToAssign = 14		
			ENDIF
		BREAK
		CASE 8
			// custom archetype
			charCreatorData.pedData.archetype = ARCHETYPE_CUSTOM
			PRINTLN("[CHARCREATOR] GENERATE_CHAR_CREATOR_RANDOM_LIFESTYLE - archetype = CUSTOM")
			
			iAttempts = 0
			BOOL bFoundArchetype 
			bFoundArchetype = FALSE
			
			IF IS_CHAR_CREATOR_PED_MALE(charCreatorData)
				WHILE iAttempts < 5
				AND NOT bFoundArchetype
					charCreatorData.pedData.customArchetype = INT_TO_ENUM(CHAR_CREATOR_CUSTOM_ARCHETYPE_ENUM, GET_RANDOM_INT_IN_RANGE(ENUM_TO_INT(CUSTOM_ARCHETYPE_MALE_BUSINESS_1), ENUM_TO_INT(CUSTOM_ARCHETYPE_MALE_DIRTY_PARTY) + 1))
					
					IF bInitSetup
						charCreatorData.pedData.customArchetype = INT_TO_ENUM(CHAR_CREATOR_CUSTOM_ARCHETYPE_ENUM, GET_RANDOM_INT_IN_RANGE(ENUM_TO_INT(CUSTOM_ARCHETYPE_MALE_BUSINESS_1), ENUM_TO_INT(CUSTOM_ARCHETYPE_MALE_CASUAL_1) + 1))
					ENDIF					
					
					IF bAllowDup
						bFoundArchetype = TRUE
					ELSE
						IF charCreatorData.pedData.customArchetype <> charCreatorData.lastMaleCustomArchetype
							bFoundArchetype = TRUE
						ELSE
							PRINTSTRING("same as last time male custom archetype - try again") PRINTNL()				
						ENDIF
					ENDIF
					iAttempts++
				ENDWHILE		
				
				charCreatorData.lastMaleCustomArchetype = charCreatorData.pedData.customArchetype
			ELSE
				WHILE iAttempts < 5
				AND NOT bFoundArchetype
					charCreatorData.pedData.customArchetype = INT_TO_ENUM(CHAR_CREATOR_CUSTOM_ARCHETYPE_ENUM, GET_RANDOM_INT_IN_RANGE(ENUM_TO_INT(CUSTOM_ARCHETYPE_FEMALE_BUSINESS_1), ENUM_TO_INT(CUSTOM_ARCHETYPE_FEMALE_PARTY_HARD) + 1))
					
					IF bInitSetup
						charCreatorData.pedData.customArchetype = INT_TO_ENUM(CHAR_CREATOR_CUSTOM_ARCHETYPE_ENUM, GET_RANDOM_INT_IN_RANGE(ENUM_TO_INT(CUSTOM_ARCHETYPE_FEMALE_BUSINESS_1), ENUM_TO_INT(CUSTOM_ARCHETYPE_FEMALE_BUSINESS_3) + 1))
					ENDIF
					
					IF bAllowDup
						bFoundArchetype = TRUE
					ELSE
						IF charCreatorData.pedData.customArchetype <> charCreatorData.lastFemaleCustomArchetype
							bFoundArchetype = TRUE
						ELSE
							PRINTSTRING("same as last time female custom archetype - try again") PRINTNL()				
						ENDIF
					ENDIF
					iAttempts++
				ENDWHILE	
				
				charCreatorData.lastFemaleCustomArchetype = charCreatorData.pedData.customArchetype
			ENDIF
			
			// sort out the hrs left to assign
			SWITCH charCreatorData.pedData.customArchetype
				// female
				CASE CUSTOM_ARCHETYPE_FEMALE_CHOLA
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_SLEEP, 16)
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_FAMILY, 10)
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_SPORT, 8)
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_ILLEGAL, 14)
				BREAK
				CASE CUSTOM_ARCHETYPE_FEMALE_BUSINESS_1
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_SLEEP, 13)
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_FAMILY, 8)
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_SPORT, 12)
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_LEGAL, 15)
				BREAK
				CASE CUSTOM_ARCHETYPE_FEMALE_BUSINESS_2
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_SLEEP, 18)
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_FAMILY, 8)
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_SPORT, 4)
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_LEGAL, 12)
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_PARTY, 6)
				BREAK
				CASE CUSTOM_ARCHETYPE_FEMALE_CASUAL_1
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_SLEEP, 20)
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_FAMILY, 8)
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_SPORT, 7)
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_LEGAL, 7)
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_PARTY, 6)
				BREAK
				CASE CUSTOM_ARCHETYPE_FEMALE_CASUAL_2
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_SLEEP, 24)
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_FAMILY, 8)
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_SPORT, 8)
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_LEGAL, 8)
				BREAK
				CASE CUSTOM_ARCHETYPE_FEMALE_PARTY_SOUTH_AMERICAN
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_SLEEP, 24)
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_FAMILY, 8)
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_PARTY, 11)
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_ILLEGAL, 5)
				BREAK
				CASE CUSTOM_ARCHETYPE_FEMALE_CASUAL_3
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_SLEEP, 24)
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_FAMILY, 8)
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_SPORT, 8)
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_LEGAL, 8)
				BREAK
				CASE CUSTOM_ARCHETYPE_FEMALE_PARTY_EUROPEAN
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_SLEEP, 22)
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_FAMILY, 8)
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_LEGAL, 6)
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_PARTY, 12)
				BREAK
				CASE CUSTOM_ARCHETYPE_FEMALE_PARTY_EAST_ASIAN
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_SLEEP, 22)
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_FAMILY, 6)
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_LEGAL, 1)
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_TV, 3)
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_PARTY, 16)
				BREAK
				CASE CUSTOM_ARCHETYPE_FEMALE_PARTY_AFRICAN
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_SLEEP, 24)
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_FAMILY, 8)
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_LEGAL, 7)
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_PARTY, 9)
				BREAK
				CASE CUSTOM_ARCHETYPE_FEMALE_PARTY_PACIFIC_ISLANDER
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_SLEEP, 24)
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_FAMILY, 6)
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_LEGAL, 8)
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_PARTY, 10)
				BREAK
				CASE CUSTOM_ARCHETYPE_FEMALE_COUCH_POTATO_EUROPEAN
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_SLEEP, 22)
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_FAMILY, 8)
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_LEGAL, 6)
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_TV, 12)
				BREAK
				CASE CUSTOM_ARCHETYPE_FEMALE_COUCH_POTATO_MIXED_1
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_SLEEP, 22)
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_FAMILY, 2)
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_LEGAL, 11)
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_TV, 13)
				BREAK
				CASE CUSTOM_ARCHETYPE_FEMALE_COUCH_POTATO_MIXED_2
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_SLEEP, 24)
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_FAMILY, 7)
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_LEGAL, 7)
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_TV, 10)
				BREAK
				CASE CUSTOM_ARCHETYPE_FEMALE_COUCH_POTATO_MIXED_3
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_SLEEP, 24)
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_FAMILY, 8)
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_LEGAL, 7)
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_TV, 9)
				BREAK
				CASE CUSTOM_ARCHETYPE_FEMALE_CHOLA_MIXED
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_SLEEP, 24)
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_FAMILY, 6)
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_LEGAL, 3)
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_TV, 5)
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_ILLEGAL, 10)
				BREAK
				CASE CUSTOM_ARCHETYPE_FEMALE_BUSINESS_3
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_SLEEP, 18)
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_FAMILY, 8)
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_LEGAL, 11)
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_ILLEGAL, 11)
				BREAK
				CASE CUSTOM_ARCHETYPE_FEMALE_PARTY_HARD
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_SLEEP, 9)
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_FAMILY, 4)
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_TV, 15)
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_PARTY, 16)
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_ILLEGAL, 4)
				BREAK
			BREAK
				
				// male
				CASE CUSTOM_ARCHETYPE_MALE_BUSINESS_1
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_SLEEP, 16)
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_FAMILY, 8)
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_SPORT, 7)
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_LEGAL, 17)
				BREAK
				CASE CUSTOM_ARCHETYPE_MALE_CASUAL_ASIAN
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_SLEEP, 24)
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_FAMILY, 11)
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_LEGAL, 8)
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_PARTY, 5)
				BREAK
				CASE CUSTOM_ARCHETYPE_MALE_SLOB_EUROPEAN
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_SLEEP, 8)
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_FAMILY, 5)
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_TV, 5)
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_PARTY, 14)
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_ILLEGAL, 16)
				BREAK
				CASE CUSTOM_ARCHETYPE_MALE_CASUAL_1
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_SLEEP, 17)
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_FAMILY, 8)
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_SPORT, 3)
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_LEGAL, 6)
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_PARTY, 6)
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_ILLEGAL, 8)
				BREAK
				CASE CUSTOM_ARCHETYPE_MALE_DIRTY_PARTY
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_SLEEP, 8)
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_TV, 13)
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_PARTY, 14)
					SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_ILLEGAL, 13)
				BREAK
			ENDSWITCH
			
			charCreatorData.pedData.iHoursLeftToAssign = 0
		BREAK
	ENDSWITCH
	

	// Random assign hours elsewhere
	WHILE charCreatorData.pedData.iHoursLeftToAssign > 0
	
		lifestyleToChange = INT_TO_ENUM(CHAR_CREATOR_LIFESTYLE, GET_RANDOM_INT_IN_RANGE(0, ENUM_TO_INT(MAX_LIFESTYLE)))
	
		IF lifestyleToSkip != lifestyleToChange
		AND IS_CHAR_CREATOR_LIFESTYLE_VALID(lifestyleToChange, charCreatorData.pedData.archetype)
	
			IF GET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, lifestyleToChange) < GET_CHAR_CREATOR_HOURS_BOUNDS_FOR_LIFESTYLE(lifestyleToChange)
				
				SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, lifestyleToChange, (GET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, lifestyleToChange) + 1))
				charCreatorData.pedData.iHoursLeftToAssign--
			ENDIF
		ENDIF
	ENDWHILE
	
	// Randomise our gender
	//IF bInitGender
	//	SET_CHAR_CREATOR_GENDER(charCreatorData, CHAR_CREATOR_GENDER_MALE)
	//ENDIF
	
	// Update our overlays
	FLOAT fPercent = 1.0
	SET_CHAR_CREATOR_OVERLAY_VALUES(charCreatorData, fPercent, CHAR_CREATOR_EYEBROW)

	#IF IS_DEBUG_BUILD
		INT i
		FOR i = 0 TO (ENUM_TO_INT(MAX_LIFESTYLE)-1) STEP 1
			PRINTLN("[CHARCREATOR]		- ", GET_CHAR_CREATOR_LIFESTYLE_TEXT_LABEL(INT_TO_ENUM(CHAR_CREATOR_LIFESTYLE, i)), ": ", GET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, INT_TO_ENUM(CHAR_CREATOR_LIFESTYLE, i)), " hrs")
		ENDFOR
	#ENDIF
ENDPROC

/// PURPOSE: Returns a hair for latest Character Creator parents 
PROC GET_CHAR_CREATOR_PARENT_HAIRCUT(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData,  CHAR_CREATOR_PED aPed, INT &iReturnHair, INT &iReturnColour)
	iReturnHair = 1
	iReturnColour = 0
	
	CHAR_CREATOR_COMPLEXION_ENUM thisComplexion = COMPLEXION_MEDIUM
	
	IF aPed = CHAR_CREATOR_PED_DAD
		IF IS_PED_HERITAGE_PREDOMINANT(charCreatorData, CHAR_CREATOR_WHITE, FALSE, FALSE, TRUE)
			thisComplexion = COMPLEXION_FAIR
		ELIF IS_PED_HERITAGE_PREDOMINANT(charCreatorData, CHAR_CREATOR_BLACK, FALSE, FALSE, TRUE)
			thisComplexion = COMPLEXION_DARK
		ENDIF	
	
		SWITCH thisComplexion
			CASE COMPLEXION_FAIR
				SWITCH GET_RANDOM_INT_IN_RANGE(0,5)
					CASE 0
						iReturnHair = 5
					BREAK
					CASE 1
						iReturnHair = 1
					BREAK
					CASE 2
						iReturnHair = 9
					BREAK
					CASE 3
						iReturnHair = 4
					BREAK
					CASE 4
						iReturnHair = 10
					BREAK
				ENDSWITCH
				
				SWITCH iReturnHair
					CASE 2
					CASE 9
					CASE 4
					CASE 10
					CASE 11
						iReturnColour = GET_RANDOM_INT_IN_RANGE(0,5)
					BREAK
					CASE 15
					CASE 1
						SWITCH GET_RANDOM_INT_IN_RANGE(0,3)
							CASE 0
								iReturnColour = 0
							BREAK
							CASE 1
								iReturnColour = 1
							BREAK
							CASE 2
								iReturnColour = 4
							BREAK
						ENDSWITCH 
					BREAK
					CASE 5
						SWITCH GET_RANDOM_INT_IN_RANGE(0,4)
							CASE 0
								iReturnColour = 0
							BREAK
							CASE 1
								iReturnColour = 1
							BREAK
							CASE 2
								iReturnColour = 2
							BREAK
							CASE 3
								iReturnColour = 2
							BREAK
						ENDSWITCH 
					BREAK
				ENDSWITCH
			BREAK
			CASE COMPLEXION_MEDIUM
				SWITCH GET_RANDOM_INT_IN_RANGE(0,5)
					CASE 0
						iReturnHair = 1
					BREAK
					CASE 1
						iReturnHair = 10
					BREAK
					CASE 2
						iReturnHair = 4
					BREAK
					CASE 3
						iReturnHair = 9
					BREAK
					CASE 4
						iReturnHair = 5
					BREAK
				ENDSWITCH
				
				SWITCH iReturnHair
					CASE 1
					CASE 10
						SWITCH GET_RANDOM_INT_IN_RANGE(0,2)
							CASE 0
								iReturnColour = 0
							BREAK
							CASE 1
								iReturnColour = 4
							BREAK
						ENDSWITCH
					BREAK
					CASE 2
						SWITCH GET_RANDOM_INT_IN_RANGE(0,4)
							CASE 0
								iReturnColour = 0
							BREAK
							CASE 1
								iReturnColour = 1
							BREAK
							CASE 2
								iReturnColour = 2
							BREAK
							CASE 3
								iReturnColour = 4
							BREAK
						ENDSWITCH
					BREAK
					CASE 4
					CASE 9
					CASE 5
						SWITCH GET_RANDOM_INT_IN_RANGE(0,3)
							CASE 0
								iReturnColour = 0
							BREAK
							CASE 1
								iReturnColour = 1
							BREAK
							CASE 2
								iReturnColour = 4
							BREAK
						ENDSWITCH
					BREAK
				ENDSWITCH				
			BREAK
			CASE COMPLEXION_DARK
				SWITCH GET_RANDOM_INT_IN_RANGE(0,5)
					CASE 0
						iReturnHair = 1
					BREAK
					CASE 1
						iReturnHair = 4
					BREAK
					CASE 2
						iReturnHair = 10
					BREAK
					CASE 3
						iReturnHair = 9
					BREAK
					CASE 4
						iReturnHair = 5
					BREAK
				ENDSWITCH		
				
				SWITCH iReturnHair
					CASE 9
					CASE 10
						iReturnColour = 4
					BREAK
					DEFAULT
						IF GET_RANDOM_INT_IN_RANGE(0,2) = 0
							iReturnColour = 0
						ELSE
							iReturnColour = 4
						ENDIF
					BREAK
				ENDSWITCH								
			BREAK
		ENDSWITCH
	ELSE
		IF IS_PED_HERITAGE_PREDOMINANT(charCreatorData, CHAR_CREATOR_WHITE, FALSE, TRUE, FALSE)
			thisComplexion = COMPLEXION_FAIR
		ELIF IS_PED_HERITAGE_PREDOMINANT(charCreatorData, CHAR_CREATOR_BLACK, FALSE, TRUE, FALSE)
			thisComplexion = COMPLEXION_DARK
		ENDIF	
		
		SWITCH thisComplexion
			CASE COMPLEXION_FAIR
				SWITCH GET_RANDOM_INT_IN_RANGE(0,8)
					CASE 0
						iReturnHair = 2
					BREAK
					CASE 1
						iReturnHair = 1
					BREAK
					CASE 2
						iReturnHair = 7
					BREAK
					CASE 3
						iReturnHair = 10
					BREAK
					CASE 4
						iReturnHair = 14
					BREAK
					CASE 5
						iReturnHair = 4
					BREAK
					CASE 6
						iReturnHair = 11
					BREAK
					CASE 7
						iReturnHair = 12
					BREAK
				ENDSWITCH
				
				SWITCH iReturnHair
					CASE 14
					CASE 4
					CASE 11
					CASE 12
					CASE 2
					CASE 1
						iReturnColour = GET_RANDOM_INT_IN_RANGE(0,5)
					BREAK
					CASE 7
						iReturnColour = GET_RANDOM_INT_IN_RANGE(0,4)
					BREAK
					CASE 10
						SWITCH GET_RANDOM_INT_IN_RANGE(0,2)
							CASE 0 
								iReturnColour = 0
							BREAK
							CASE 1
								iReturnColour = 1
							BREAK
						ENDSWITCH
					BREAK
				ENDSWITCH
			BREAK	
			
			CASE COMPLEXION_MEDIUM
				SWITCH GET_RANDOM_INT_IN_RANGE(0,8)
					CASE 0
						iReturnHair = 2
					BREAK
					CASE 1
						iReturnHair = 1
					BREAK
					CASE 2
						iReturnHair = 7
					BREAK
					CASE 3
						iReturnHair = 10
					BREAK
					CASE 4
						iReturnHair = 14
					BREAK
					CASE 5
						iReturnHair = 4
					BREAK
					CASE 6
						iReturnHair = 11
					BREAK
					CASE 7
						iReturnHair = 12
					BREAK
				ENDSWITCH
				
				SWITCH iReturnHair
					CASE 14
					CASE 4
					CASE 11
					CASE 12
					CASE 2
					CASE 1
						SWITCH GET_RANDOM_INT_IN_RANGE(0,4)
							CASE 0
								iReturnColour = 0
							BREAK
							CASE 1
								iReturnColour = 2
							BREAK
							CASE 2
								iReturnColour = 3
							BREAK
							CASE 3
								iReturnColour = 4
							BREAK
						ENDSWITCH
					BREAK
					CASE 7
						SWITCH GET_RANDOM_INT_IN_RANGE(0,2)
							CASE 0 
								iReturnColour = 0
							BREAK
							CASE 1
								iReturnColour = 3
							BREAK
						ENDSWITCH
					BREAK
					CASE 10
						iReturnColour = 0
					BREAK
				ENDSWITCH
			BREAK
			
			CASE COMPLEXION_DARK
				SWITCH GET_RANDOM_INT_IN_RANGE(0,8)
					CASE 0
						iReturnHair = 2
					BREAK
					CASE 1
						iReturnHair = 15
					BREAK
					CASE 2
						iReturnHair = 6
					BREAK
					CASE 3
						iReturnHair = 4
					BREAK
					CASE 4
						iReturnHair = 14
					BREAK
					CASE 5
						iReturnHair = 12
					BREAK
					CASE 6
						iReturnHair = 11
					BREAK
					CASE 7
						iReturnHair = 10
					BREAK
				ENDSWITCH
				
				iReturnColour = 3
			BREAK				
		ENDSWITCH
	ENDIF
ENDPROC

/// PURPOSE: Populates our data with random hair for parents
PROC GENERATE_CHAR_CREATOR_RANDOM_PARENTS_HAIR(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData, CHAR_CREATOR_PED aPed, BOOL bFromMumSlider = FALSE, BOOL bFromDadSlider = FALSE)
	IF (aPed = CHAR_CREATOR_PED_DAD AND (NOT bFromDadSlider)) //OR NOT charCreatorData.cachedDad[charCreatorData.iDadOption].bFilled))
	OR (aPed = CHAR_CREATOR_PED_MUM AND (NOT bFromMumSlider)) // OR NOT charCreatorData.cachedMum[charCreatorData.iMumOption].bFilled))
		// generate new hair
		INT iRandom
		INT iRandomColour

		// Assign random hair for Parent
		INT iAttempts = 0
		BOOL bFoundHair = FALSE
		
		WHILE iAttempts < 5
		AND NOT bFoundHair
			GET_CHAR_CREATOR_PARENT_HAIRCUT(charCreatorData, aPed, iRandom, iRandomColour)
			IF aPed = CHAR_CREATOR_PED_DAD
				IF iRandom <> charCreatorData.iLastRandomDadHair
					bFoundHair = TRUE
				ELSE
					PRINTSTRING("same as last time dad hair - try again") PRINTNL()				
				ENDIF
			ELSE
				IF iRandom <> charCreatorData.iLastRandomMumHair
					bFoundHair = TRUE
				ELSE
					PRINTSTRING("same as last time mum hair - try again") PRINTNL()				
				ENDIF
			ENDIF
			iAttempts++
		ENDWHILE	
		
		IF aPed = CHAR_CREATOR_PED_DAD
			charCreatorData.iLastRandomDadHair = iRandom
		ELSE
			charCreatorData.iLastRandomMumHair = iRandom
		ENDIF
			
		PRINTLN("[CHARCREATOR] GENERATE_CHAR_CREATOR_RANDOM_HAIR - PARENT random hair: ", iRandom)
		
		INT iPosition
		BOOL bUseCache = FALSE
		SWITCH charCreatorData.heritageIndex
			CASE HERITAGE_MUM
				iPosition = charCreatorData.iHeadForMumRace
				IF aPed = CHAR_CREATOR_PED_MUM
				OR charCreatorData.cachedMumHeritageIndex = HERITAGE_GENDER
					bUseCache = TRUE
				ENDIF
			BREAK
			CASE HERITAGE_MUM2
				iPosition = charCreatorData.iHeadForMumRace2
				IF aPed = CHAR_CREATOR_PED_MUM
				OR charCreatorData.cachedMumHeritageIndex = HERITAGE_GENDER
					bUseCache = TRUE
				ENDIF				
			BREAK
			CASE HERITAGE_DAD
				iPosition = charCreatorData.iHeadForDadRace
				IF aPed = CHAR_CREATOR_PED_DAD
				OR charCreatorData.cachedDadHeritageIndex = HERITAGE_GENDER
					bUseCache = TRUE
				ENDIF					
			BREAK
			CASE HERITAGE_DAD2
				iPosition = charCreatorData.iHeadForDadRace2
				IF aPed = CHAR_CREATOR_PED_DAD
				OR charCreatorData.cachedDadHeritageIndex = HERITAGE_GENDER
					bUseCache = TRUE
				ENDIF					
			BREAK
		ENDSWITCH		
	
		IF aPed = CHAR_CREATOR_PED_DAD
			// beard
			IF charCreatorData.iCacheDadBeard[iPosition] < 0
			OR NOT bUseCache
				charCreatorData.pedData.charCreatorParentInfo.iDadBeard = GET_RANDOM_INT_IN_RANGE(0, GET_PED_HEAD_OVERLAY_NUM(ENUM_TO_INT(CHAR_CREATOR_BEARD)))
				IF bUseCache
					charCreatorData.iCacheDadBeard[iPosition] = charCreatorData.pedData.charCreatorParentInfo.iDadBeard
				ENDIF
			ELSE
				charCreatorData.pedData.charCreatorParentInfo.iDadBeard = charCreatorData.iCacheDadBeard[iPosition]
			ENDIF
			
			IF charCreatorData.iCacheDadHair[iPosition] < 0
			OR NOT bUseCache
				charCreatorData.pedData.charCreatorParentInfo.iDadHair = iRandom
				IF bUseCache
					charCreatorData.iCacheDadHair[iPosition] = charCreatorData.pedData.charCreatorParentInfo.iDadHair
				ENDIF
			ELSE
				charCreatorData.pedData.charCreatorParentInfo.iDadHair = charCreatorData.iCacheDadHair[iPosition]
			ENDIF
			
			IF charCreatorData.iCacheDadHairColour[iPosition] < 0
			OR NOT bUseCache
				charCreatorData.pedData.charCreatorParentInfo.iDadHairColour = iRandomColour
				IF bUseCache
					charCreatorData.iCacheDadHairColour[iPosition] = charCreatorData.pedData.charCreatorParentInfo.iDadHairColour
				ENDIF
			ELSE
				charCreatorData.pedData.charCreatorParentInfo.iDadHairColour = charCreatorData.iCacheDadHairColour[iPosition]
			ENDIF
			
			// hair
			IF charCreatorData.fCacheDadSizeMod[iPosition] <= -1
			OR NOT bUseCache
				charCreatorData.pedData.charCreatorParentInfo.fDadHeightMod = GET_RANDOM_FLOAT_IN_RANGE(0, HEIGHT_MOD_MAX)
				IF GET_RANDOM_INT_IN_RANGE(0,2) = 0
					charCreatorData.pedData.charCreatorParentInfo.fDadHeightMod *= -1
				ENDIF
				charCreatorData.pedData.charCreatorParentInfo.fDadSizeMod = GET_RANDOM_FLOAT_IN_RANGE(0, SIZE_MOD_MAX)
				IF GET_RANDOM_INT_IN_RANGE(0,2) = 0
					charCreatorData.pedData.charCreatorParentInfo.fDadSizeMod *= -1
				ENDIF
				charCreatorData.pedData.charCreatorParentInfo.fDadSideMod = GET_RANDOM_FLOAT_IN_RANGE(0, SIDE_MOD_MAX)
				IF GET_RANDOM_INT_IN_RANGE(0,2) = 0
					charCreatorData.pedData.charCreatorParentInfo.fDadSideMod *= -1
				ENDIF				
				
				IF bUseCache
					charCreatorData.fCacheDadHeightMod[iPosition] = charCreatorData.pedData.charCreatorParentInfo.fDadHeightMod
					charCreatorData.fCacheDadSizeMod[iPosition] = charCreatorData.pedData.charCreatorParentInfo.fDadSizeMod
					charCreatorData.fCacheDadSideMod[iPosition] = charCreatorData.pedData.charCreatorParentInfo.fDadSideMod
				ENDIF
			ELSE
				charCreatorData.pedData.charCreatorParentInfo.fDadHeightMod = charCreatorData.fCacheDadHeightMod[iPosition]
				charCreatorData.pedData.charCreatorParentInfo.fDadSizeMod = charCreatorData.fCacheDadSizeMod[iPosition]
				charCreatorData.pedData.charCreatorParentInfo.fDadSideMod = charCreatorData.fCacheDadSideMod[iPosition]
			ENDIF
		ELSE
			//charCreatorData.pedData.charCreatorParentInfo.iMumMakeup = GET_RANDOM_INT_IN_RANGE(0, GET_PED_HEAD_OVERLAY_NUM(ENUM_TO_INT(CHAR_CREATOR_MAKEUP)))
			IF charCreatorData.iCacheMumHair[iPosition] < 0
			OR NOT bUseCache
				charCreatorData.pedData.charCreatorParentInfo.iMumHair = iRandom
				IF bUseCache
					charCreatorData.iCacheMumHair[iPosition] = charCreatorData.pedData.charCreatorParentInfo.iMumHair
				ENDIF
			ELSE
				charCreatorData.pedData.charCreatorParentInfo.iMumHair = charCreatorData.iCacheMumHair[iPosition]
			ENDIF
			
			IF charCreatorData.iCacheMumHairColour[iPosition] < 0
			OR NOT bUseCache
				charCreatorData.pedData.charCreatorParentInfo.iMumHairColour = iRandomColour
				IF bUseCache
					charCreatorData.iCacheMumHairColour[iPosition] = charCreatorData.pedData.charCreatorParentInfo.iMumHairColour
				ENDIF
			ELSE
				charCreatorData.pedData.charCreatorParentInfo.iMumHairColour = charCreatorData.iCacheMumHairColour[iPosition]
			ENDIF
			
			printstring("obtained random mum hair ") PRINTINT(charCreatorData.pedData.charCreatorParentInfo.iMumHair) PRINTSTRING(" ") PRINTINT(charCreatorData.pedData.charCreatorParentInfo.iMumHairColour) PRINTNL()
			
			INT iRandomMakeup = GET_RANDOM_INT_IN_RANGE(0,7)
			SWITCH iRandomMakeup
				CASE 0
					charCreatorData.pedData.charCreatorParentInfo.iMumMakeup = 7 // vamp
				BREAK
				CASE 1
					charCreatorData.pedData.charCreatorParentInfo.iMumMakeup = 8 // vinewood glamour
				BREAK
				CASE 2
					charCreatorData.pedData.charCreatorParentInfo.iMumMakeup = 10 // aqua dream
				BREAK
				CASE 3
					charCreatorData.pedData.charCreatorParentInfo.iMumMakeup = 14 // ruby
				BREAK
				CASE 4
					charCreatorData.pedData.charCreatorParentInfo.iMumMakeup = 12 // purple passion
				BREAK
				CASE 5
					charCreatorData.pedData.charCreatorParentInfo.iMumMakeup = 4 // natural look
				BREAK
				CASE 6
					charCreatorData.pedData.charCreatorParentInfo.iMumMakeup = 5 // cat eyes
				BREAK
			ENDSWITCH
			
			IF charCreatorData.iCacheMumMakeup[iPosition] < 0
			OR NOT bUseCache
				charCreatorData.pedData.charCreatorParentInfo.iMumMakeup = iRandom
				IF bUseCache
					charCreatorData.iCacheMumMakeup[iPosition] = charCreatorData.pedData.charCreatorParentInfo.iMumMakeup 
				ENDIF
			ELSE
				charCreatorData.pedData.charCreatorParentInfo.iMumMakeup = charCreatorData.iCacheMumMakeup[iPosition]
			ENDIF
			
			IF charCreatorData.fCacheMumSizeMod[iPosition] <= -1
			OR NOT bUseCache
				charCreatorData.pedData.charCreatorParentInfo.fMumHeightMod = GET_RANDOM_FLOAT_IN_RANGE(0, HEIGHT_MOD_MAX)
				IF GET_RANDOM_INT_IN_RANGE(0,2) = 0
					charCreatorData.pedData.charCreatorParentInfo.fMumHeightMod *= -1
				ENDIF
				charCreatorData.pedData.charCreatorParentInfo.fMumSizeMod = GET_RANDOM_FLOAT_IN_RANGE(0, SIZE_MOD_MAX)
				IF GET_RANDOM_INT_IN_RANGE(0,2) = 0
					charCreatorData.pedData.charCreatorParentInfo.fMumSizeMod *= -1
				ENDIF	
				charCreatorData.pedData.charCreatorParentInfo.fMumSideMod = GET_RANDOM_FLOAT_IN_RANGE(0, SIDE_MOD_MAX)
				IF GET_RANDOM_INT_IN_RANGE(0,2) = 0
					charCreatorData.pedData.charCreatorParentInfo.fMumSideMod *= -1
				ENDIF					
				
				IF bUseCache
					charCreatorData.fCacheMumHeightMod[iPosition] = charCreatorData.pedData.charCreatorParentInfo.fMumHeightMod
					charCreatorData.fCacheMumSizeMod[iPosition] = charCreatorData.pedData.charCreatorParentInfo.fMumSizeMod
					charCreatorData.fCacheMumSideMod[iPosition] = charCreatorData.pedData.charCreatorParentInfo.fMumSideMod
				ENDIF
			ELSE
				charCreatorData.pedData.charCreatorParentInfo.fMumHeightMod = charCreatorData.fCacheMumHeightMod[iPosition]
				charCreatorData.pedData.charCreatorParentInfo.fMumSizeMod = charCreatorData.fCacheMumSizeMod[iPosition]
				charCreatorData.pedData.charCreatorParentInfo.fMumSideMod = charCreatorData.fCacheMumSideMod[iPosition]
			ENDIF
		ENDIF
	ELSE
		// restore old hair
		//IF aPed = CHAR_CREATOR_PED_DAD
		//	charCreatorData.pedData.charCreatorParentInfo.iDadBeard = charCreatorData.cachedDad[charCreatorData.iDadOption].iFacial
		//	charCreatorData.pedData.charCreatorParentInfo.iDadHair = charCreatorData.cachedDad[charCreatorData.iDadOption].iHair
		//ELSE
		//	charCreatorData.pedData.charCreatorParentInfo.iMumHair = charCreatorData.cachedMum[charCreatorData.iMumOption].iHair
		//ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Returns TRUE if the player has access to these special heads!  
FUNC BOOL IS_CHAR_CREATOR_SPECIAL_HEAD_VALID(INT iHead, CHAR_CREATOR_PED aPed)
	
	IF aPed = CHAR_CREATOR_PED_DAD
		
		SWITCH iHead
			// Claude
			CASE 0
				IF ARE_CHAR_CREATOR_COLLECTOR_EDITIONS_AVAILABLE()
					RETURN TRUE
				ENDIF
			BREAK
			
			// Niko
			CASE 1
				IF ARE_CHAR_CREATOR_COLLECTOR_EDITIONS_AVAILABLE()
					RETURN TRUE
				ENDIF
			BREAK
			
			// John M
			CASE 2
				IF IS_GAME_LINKED_TO_SOCIAL_CLUB()
					RETURN TRUE
				ENDIF
			BREAK			
		ENDSWITCH
		
	ELSE
	
		SWITCH iHead
		
			// Misty
			CASE 0
				IF ARE_CHAR_CREATOR_COLLECTOR_EDITIONS_AVAILABLE()
					RETURN TRUE
				ENDIF
			BREAK
		
		ENDSWITCH
	
	ENDIF

	RETURN FALSE
ENDFUNC

/// PURPOSE: Get the max number of special heads we have for parents
FUNC INT GET_CHAR_CREATOR_SPECIAL_HEAD_MAX(CHAR_CREATOR_PED aPed)

	IF aPed = CHAR_CREATOR_PED_MUM
		RETURN GET_PED_HEAD_BLEND_NUM_HEADS(HBHT_UNIQUE_FEMALE)
	ELSE
		RETURN GET_PED_HEAD_BLEND_NUM_HEADS(HBHT_UNIQUE_MALE)
	ENDIF
ENDFUNC

// new functions added by neilf for NT 1822636
FUNC BOOL IS_CHAR_CREATOR_RANDOM_SPECIAL_HEAD_AVAILABLE(CHAR_CREATOR_PED aPed)
	INT iMaxIndex = GET_CHAR_CREATOR_SPECIAL_HEAD_MAX(aPed)
	INT iHeadIndex
	REPEAT iMaxIndex iHeadIndex
		IF IS_CHAR_CREATOR_SPECIAL_HEAD_VALID(iHeadIndex, aPed)	
			RETURN(TRUE)
		ENDIF
	ENDREPEAT
	RETURN(FALSE)
ENDFUNC

/// PURPOSE: Get a random special head index that is valid for the player
FUNC INT GET_CHAR_CREATOR_RANDOM_SPECIAL_HEAD_INDEX(CHAR_CREATOR_PED aPed)

	INT iMaxIndex = GET_CHAR_CREATOR_SPECIAL_HEAD_MAX(aPed)
	INT iHeadIndex

	BOOL bFound
	
	WHILE (NOT bFound)
	
		iHeadIndex = GET_RANDOM_INT_IN_RANGE(0, iMaxIndex)
		
		IF IS_CHAR_CREATOR_SPECIAL_HEAD_VALID(iHeadIndex, aPed)
			bFound = TRUE
		ENDIF
	ENDWHILE

	RETURN iHeadIndex
ENDFUNC


FUNC BOOL DOES_CHAR_CREATOR_PARENT_RACE_HAVE_EXTRA_HEAD(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData, CHAR_CREATOR_PED aPed, INT iRaceIndex)
	
	CHAR_CREATOR_RACE aRace = GET_CHAR_CREATOR_PARENT_RACE(charCreatorData, aPed, iRaceIndex)
	
	IF aPed = CHAR_CREATOR_PED_MUM
		IF aRace = CHAR_CREATOR_LATIN
		OR aRace = CHAR_CREATOR_BLACK
			RETURN TRUE
		ENDIF
	ELSE
		IF aRace = CHAR_CREATOR_WHITE
		OR aRace = CHAR_CREATOR_CHINESE
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

// grandparent variations
PROC SET_GRANDPARENT_HEAD_VARIATION(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData, INT &iHead, CHAR_CREATOR_PED aPed, INT iRace)
	// get ugliness
	INT iUglyChance = GET_RANDOM_INT_IN_RANGE(0, 101)
	INT iUglyThreshold = 10
	INT iMediumThreshold
	
	IF aPed = CHAR_CREATOR_PED_DAD
		IF charCreatorData.iNumberSinceUglyDadGenerated > UGLY_DAD_THRESHOLD
			iUglyThreshold = 25
		ENDIF
	ELSE
		IF charCreatorData.iNumberSinceUglyMumGenerated > UGLY_MUM_THRESHOLD
			iUglyThreshold = 25
		ENDIF
	ENDIF
	
	iMediumThreshold = iUglyThreshold + 17
	
	IF NOT charCreatorData.bCreatedFirstPrettyChild
		iUglyThreshold = -1
		iMediumThreshold = -1
	ENDIF
	
	IF DOES_CHAR_CREATOR_PARENT_RACE_HAVE_EXTRA_HEAD(charCreatorData, aPed, iRace)
		IF iUglyChance <= iUglyThreshold
			iHead = MP_HEAD_UGLY
		#IF NOT	IS_JAPANESE_BUILD
		ELIF iUglyChance <= iMediumThreshold
			iHead = MP_HEAD_EXTRA1
		#ENDIF
		ENDIF				
	ELSE
		IF iUglyChance <= iUglyThreshold
			iHead = MP_HEAD_UGLY
		ELIF iUglyChance <= iMediumThreshold
			IF GET_RANDOM_INT_IN_RANGE(0,2) = 0
				iHead = MP_HEAD_UGLY
			ENDIF
		ENDIF			
	ENDIF
	
	IF iHead = MP_HEAD_UGLY
		IF aPed = CHAR_CREATOR_PED_DAD
			charCreatorData.iNumberSinceUglyDadGenerated = 0
		ELSE
			charCreatorData.iNumberSinceUglyMumGenerated = 0
		ENDIF
	ENDIF
ENDPROC

// grandad head
PROC UPDATE_GRANDAD_HEAD(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData, BOOL bForMum)
	PED_INDEX ped
	IF bForMum
		ped = charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_GRANDDAD_M]
	ELSE
		ped = charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_GRANDDAD_D]
	ENDIF

	IF NOT IS_PED_INJURED(ped)
		INT iHead
		
		IF bForMum
			GET_CHAR_CREATOR_HEADS_FOR_RACE(charCreatorData, CHAR_CREATOR_PED_MUM, iHead, charCreatorData.pedData.charCreatorParentInfo.iMumHeadVariation2, CHAR_CREATOR_RACE_INDEX_TWO, TRUE)
		ELSE
			GET_CHAR_CREATOR_HEADS_FOR_RACE(charCreatorData, CHAR_CREATOR_PED_DAD, iHead, charCreatorData.pedData.charCreatorParentInfo.iDadHeadVariation2, CHAR_CREATOR_RACE_INDEX_TWO)
		ENDIF
		
		SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HEAD, iHead, 0)
		SET_PED_COMPONENT_VARIATION(ped, PED_COMP_JBIB, 4, 0)
		SET_PED_COMPONENT_VARIATION(ped, PED_COMP_SPECIAL, 13, 1)
		
		CHAR_CREATOR_RACE thisRace

		INT iHair
		INT iHairTex
		IF bForMum
			thisRace = GET_CHAR_CREATOR_PARENT_RACE(charCreatorData, CHAR_CREATOR_PED_MUM, CHAR_CREATOR_RACE_INDEX_TWO)
			SWITCH thisRace
				CASE CHAR_CREATOR_WHITE
					IF charCreatorData.pedData.charCreatorParentInfo.iMumHeadVariation2 = MP_HEAD_PRETTY
						iHair = 10
						iHairTex = 5
					ELIF charCreatorData.pedData.charCreatorParentInfo.iMumHeadVariation2 = MP_HEAD_UGLY
						iHair = 12
						iHairTex = 4
					ELSE
						iHair = 4
						iHairTex = 3
					ENDIF
				BREAK
				CASE CHAR_CREATOR_BLACK
					IF charCreatorData.pedData.charCreatorParentInfo.iMumHeadVariation2 = MP_HEAD_PRETTY
						iHair = 1
						iHairTex = 4
					ELIF charCreatorData.pedData.charCreatorParentInfo.iMumHeadVariation2 = MP_HEAD_UGLY
					#IF NOT	IS_JAPANESE_BUILD
						OR charCreatorData.pedData.charCreatorParentInfo.iMumHeadVariation2 = MP_HEAD_EXTRA1
					#ENDIF
						iHair = 10
						iHairTex = 5
					ENDIF
				BREAK
				CASE CHAR_CREATOR_INDIAN
					IF charCreatorData.pedData.charCreatorParentInfo.iMumHeadVariation2 = MP_HEAD_PRETTY
						iHair = 9
						iHairTex = 4
					ELIF charCreatorData.pedData.charCreatorParentInfo.iMumHeadVariation2 = MP_HEAD_UGLY
						iHair = 9
						iHairTex = 5
					ENDIF						
				BREAK
				CASE CHAR_CREATOR_CHINESE
					IF charCreatorData.pedData.charCreatorParentInfo.iMumHeadVariation2 = MP_HEAD_PRETTY
						iHair = 4
						iHairTex = 5
					ELIF charCreatorData.pedData.charCreatorParentInfo.iMumHeadVariation2 = MP_HEAD_UGLY
						iHair = 12
						iHairTex = 4
					ELSE
						iHair = 12
						iHairTex = 4						
					ENDIF				
				BREAK
				CASE CHAR_CREATOR_LATIN
					IF charCreatorData.pedData.charCreatorParentInfo.iMumHeadVariation2 = MP_HEAD_PRETTY
						iHair = 15
						iHairTex = 0
					ELIF charCreatorData.pedData.charCreatorParentInfo.iMumHeadVariation2 = MP_HEAD_UGLY
					#IF NOT	IS_JAPANESE_BUILD
						OR charCreatorData.pedData.charCreatorParentInfo.iMumHeadVariation2 = MP_HEAD_EXTRA1
					#ENDIF
						iHair = 1
						iHairTex = 0
					ENDIF
				BREAK
				CASE CHAR_CREATOR_POLYNESIAN
					IF charCreatorData.pedData.charCreatorParentInfo.iMumHeadVariation2 = MP_HEAD_PRETTY
						iHair = 5
						iHairTex = 1
					ELIF charCreatorData.pedData.charCreatorParentInfo.iMumHeadVariation2 = MP_HEAD_UGLY
						iHair = 10
						iHairTex = 4
					ENDIF
				BREAK			
			ENDSWITCH
		ELSE
			thisRace = GET_CHAR_CREATOR_PARENT_RACE(charCreatorData, CHAR_CREATOR_PED_DAD, CHAR_CREATOR_RACE_INDEX_TWO)
			SWITCH thisRace
				CASE CHAR_CREATOR_WHITE
					IF charCreatorData.pedData.charCreatorParentInfo.iDadHeadVariation2 = MP_HEAD_PRETTY
						iHair = 9
						iHairTex = 1
					ELIF charCreatorData.pedData.charCreatorParentInfo.iDadHeadVariation2 = MP_HEAD_UGLY
						iHair = 1
						iHairTex = 0
					ELSE
						iHair = 9
						iHairTex = 3
					ENDIF
				BREAK
				CASE CHAR_CREATOR_BLACK
					IF charCreatorData.pedData.charCreatorParentInfo.iDadHeadVariation2 = MP_HEAD_PRETTY
						iHair = 1
						iHairTex = 0
					ELIF charCreatorData.pedData.charCreatorParentInfo.iDadHeadVariation2 = MP_HEAD_UGLY
					#IF NOT	IS_JAPANESE_BUILD
						OR charCreatorData.pedData.charCreatorParentInfo.iDadHeadVariation2 = MP_HEAD_EXTRA1
					#ENDIF
						iHair = 1
						iHairTex = 4
					ENDIF
				BREAK
				CASE CHAR_CREATOR_INDIAN
					IF charCreatorData.pedData.charCreatorParentInfo.iDadHeadVariation2 = MP_HEAD_PRETTY
						iHair = 5
						iHairTex = 4
					ELIF charCreatorData.pedData.charCreatorParentInfo.iDadHeadVariation2 = MP_HEAD_UGLY
						iHair = 10
						iHairTex = 0
					ENDIF						
				BREAK
				CASE CHAR_CREATOR_CHINESE
					IF charCreatorData.pedData.charCreatorParentInfo.iDadHeadVariation2 = MP_HEAD_PRETTY
						iHair = 7
						iHairTex = 4
					ELIF charCreatorData.pedData.charCreatorParentInfo.iDadHeadVariation2 = MP_HEAD_UGLY
						iHair = 4
						iHairTex = 5
					ELSE
						iHair = 15
						iHairTex = 0
					ENDIF				
				BREAK
				CASE CHAR_CREATOR_LATIN
					IF charCreatorData.pedData.charCreatorParentInfo.iDadHeadVariation2 = MP_HEAD_PRETTY
						iHair = 4
						iHairTex = 5
					ELIF charCreatorData.pedData.charCreatorParentInfo.iDadHeadVariation2 = MP_HEAD_UGLY
					#IF NOT	IS_JAPANESE_BUILD
						OR charCreatorData.pedData.charCreatorParentInfo.iDadHeadVariation2 = MP_HEAD_EXTRA1
					#ENDIF
						iHair = 10
						iHairTex = 1
					ENDIF
				BREAK
				CASE CHAR_CREATOR_POLYNESIAN
					IF charCreatorData.pedData.charCreatorParentInfo.iDadHeadVariation2 = MP_HEAD_PRETTY
						iHair = 4
						iHairTex = 0
					ELIF charCreatorData.pedData.charCreatorParentInfo.iDadHeadVariation2 = MP_HEAD_UGLY
						iHair = 12
						iHairTex = 4
					ENDIF
				BREAK			
			ENDSWITCH
		ENDIF
		
		
		SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HAIR, iHair, iHairTex)	
		
	ENDIF
ENDPROC

PROC UPDATE_GRANDMA_HEAD(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData, BOOL bForMum)
	PED_INDEX ped
	IF bForMum
		ped = charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_GRANDMA_M]
	ELSE
		ped = charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_GRANDMA_D]
	ENDIF

	IF NOT IS_PED_INJURED(ped)
		INT iHead
		
		IF NOT bForMum
			GET_CHAR_CREATOR_HEADS_FOR_RACE(charCreatorData, CHAR_CREATOR_PED_DAD, iHead, charCreatorData.pedData.charCreatorParentInfo.iDadHeadVariation, CHAR_CREATOR_RACE_INDEX_ONE, FALSE, TRUE)
		ELSE
			GET_CHAR_CREATOR_HEADS_FOR_RACE(charCreatorData, CHAR_CREATOR_PED_MUM, iHead, charCreatorData.pedData.charCreatorParentInfo.iMumHeadVariation, CHAR_CREATOR_RACE_INDEX_ONE)
		ENDIF

		SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HEAD, iHead, 0)	
		SET_PED_COMPONENT_VARIATION(ped, PED_COMP_JBIB, 3, 2)
		
		CHAR_CREATOR_RACE thisRace

		INT iHair
		INT iHairTex
		IF bForMum
			thisRace = GET_CHAR_CREATOR_PARENT_RACE(charCreatorData, CHAR_CREATOR_PED_MUM, CHAR_CREATOR_RACE_INDEX_ONE)
			SWITCH thisRace
				CASE CHAR_CREATOR_WHITE
					IF charCreatorData.pedData.charCreatorParentInfo.iMumHeadVariation = MP_HEAD_PRETTY
						iHair = 10
						iHairTex = 1
					ELIF charCreatorData.pedData.charCreatorParentInfo.iMumHeadVariation = MP_HEAD_UGLY
					#IF NOT	IS_JAPANESE_BUILD
						OR charCreatorData.pedData.charCreatorParentInfo.iMumHeadVariation = MP_HEAD_EXTRA1
					#ENDIF
						iHair = 11
						iHairTex = 1
					ENDIF
				BREAK
				CASE CHAR_CREATOR_BLACK
					IF charCreatorData.pedData.charCreatorParentInfo.iMumHeadVariation = MP_HEAD_PRETTY
						iHair = 11
						iHairTex = 3
					ELIF charCreatorData.pedData.charCreatorParentInfo.iMumHeadVariation = MP_HEAD_UGLY
						iHair = 11
						iHairTex = 4
					ELSE
						iHair = 7
						iHairTex = 3
					ENDIF
				BREAK
				CASE CHAR_CREATOR_INDIAN
					IF charCreatorData.pedData.charCreatorParentInfo.iMumHeadVariation = MP_HEAD_PRETTY
						iHair = 11
						iHairTex = 3
					ELIF charCreatorData.pedData.charCreatorParentInfo.iMumHeadVariation = MP_HEAD_UGLY
						iHair = 14
						iHairTex = 5
					ENDIF						
				BREAK
				CASE CHAR_CREATOR_CHINESE
					IF charCreatorData.pedData.charCreatorParentInfo.iMumHeadVariation = MP_HEAD_PRETTY
						iHair = 10
						iHairTex = 3
					ELIF charCreatorData.pedData.charCreatorParentInfo.iMumHeadVariation = MP_HEAD_UGLY
					#IF NOT	IS_JAPANESE_BUILD
						OR charCreatorData.pedData.charCreatorParentInfo.iMumHeadVariation = MP_HEAD_EXTRA1
					#ENDIF
						iHair = 11
						iHairTex = 3
					ENDIF				
				BREAK
				CASE CHAR_CREATOR_LATIN
					IF charCreatorData.pedData.charCreatorParentInfo.iMumHeadVariation = MP_HEAD_PRETTY
						iHair = 14
						iHairTex = 0
					ELIF charCreatorData.pedData.charCreatorParentInfo.iMumHeadVariation = MP_HEAD_UGLY
						iHair = 15
						iHairTex = 5
					ELSE
						iHair = 15
						iHairTex = 0
					ENDIF
				BREAK
				CASE CHAR_CREATOR_POLYNESIAN
					IF charCreatorData.pedData.charCreatorParentInfo.iMumHeadVariation = MP_HEAD_PRETTY
						iHair = 15
						iHairTex = 4
					ELIF charCreatorData.pedData.charCreatorParentInfo.iMumHeadVariation = MP_HEAD_UGLY
						iHair = 10
						iHairTex = 5
					ENDIF
				BREAK			
			ENDSWITCH
		ELSE
			thisRace = GET_CHAR_CREATOR_PARENT_RACE(charCreatorData, CHAR_CREATOR_PED_DAD, CHAR_CREATOR_RACE_INDEX_ONE)
			SWITCH thisRace
				CASE CHAR_CREATOR_WHITE
					IF charCreatorData.pedData.charCreatorParentInfo.iDadHeadVariation = MP_HEAD_PRETTY
						iHair = 4
						iHairTex = 1
					ELIF charCreatorData.pedData.charCreatorParentInfo.iDadHeadVariation = MP_HEAD_UGLY
					#IF NOT	IS_JAPANESE_BUILD
						OR charCreatorData.pedData.charCreatorParentInfo.iDadHeadVariation = MP_HEAD_EXTRA1
					#ENDIF
						iHair = 11
						iHairTex = 5
					ENDIF
				BREAK
				CASE CHAR_CREATOR_BLACK
					IF charCreatorData.pedData.charCreatorParentInfo.iDadHeadVariation = MP_HEAD_PRETTY
						iHair = 10
						iHairTex = 3
					ELIF charCreatorData.pedData.charCreatorParentInfo.iDadHeadVariation = MP_HEAD_UGLY
						iHair = 12
						iHairTex = 3
					ELSE
						iHair = 11
						iHairTex = 0
					ENDIF
				BREAK
				CASE CHAR_CREATOR_INDIAN
					IF charCreatorData.pedData.charCreatorParentInfo.iDadHeadVariation = MP_HEAD_PRETTY
						iHair = 4
						iHairTex = 3
					ELIF charCreatorData.pedData.charCreatorParentInfo.iDadHeadVariation = MP_HEAD_UGLY
						iHair = 15
						iHairTex = 3
					ENDIF						
				BREAK
				CASE CHAR_CREATOR_CHINESE
					IF charCreatorData.pedData.charCreatorParentInfo.iDadHeadVariation = MP_HEAD_PRETTY
						iHair = 7
						iHairTex = 3
					ELIF charCreatorData.pedData.charCreatorParentInfo.iDadHeadVariation = MP_HEAD_UGLY
					#IF NOT	IS_JAPANESE_BUILD
						OR charCreatorData.pedData.charCreatorParentInfo.iDadHeadVariation = MP_HEAD_EXTRA1
					#ENDIF
						iHair = 14
						iHairTex = 5
					ENDIF				
				BREAK
				CASE CHAR_CREATOR_LATIN
					IF charCreatorData.pedData.charCreatorParentInfo.iDadHeadVariation = MP_HEAD_PRETTY
						iHair = 4
						iHairTex = 3
					ELIF charCreatorData.pedData.charCreatorParentInfo.iDadHeadVariation = MP_HEAD_UGLY
						iHair = 3
						iHairTex = 3
					ELSE
						iHair = 10
						iHairTex = 0
					ENDIF
				BREAK
				CASE CHAR_CREATOR_POLYNESIAN
					IF charCreatorData.pedData.charCreatorParentInfo.iDadHeadVariation = MP_HEAD_PRETTY
						iHair = 11
						iHairTex = 0
					ELIF charCreatorData.pedData.charCreatorParentInfo.iDadHeadVariation = MP_HEAD_UGLY
						iHair = 12
						iHairTex = 3
					ENDIF
				BREAK			
			ENDSWITCH
		ENDIF		
		
		SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HAIR, iHair, iHairTex)		
		
		//IF bForMum
		//	SET_PED_HEAD_OVERLAY(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_GRANDMA], ENUM_TO_INT(CHAR_CREATOR_MAKEUP), 0, 1.0)	
		//ELSE
		//	SET_PED_HEAD_OVERLAY(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_GRANDMA], ENUM_TO_INT(CHAR_CREATOR_MAKEUP), 4, 1.0)	
		//ENDIF
		//SET_PED_HEAD_OVERLAY(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_GRANDMA], ENUM_TO_INT(CHAR_CREATOR_EYEBROW), 1, 0.0)			
			
	ENDIF
ENDPROC

/// PURPOSE: Generates random parent
PROC GENERATE_CHAR_CREATOR_RANDOM_PARENTS(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData, BOOL bAllowDup = FALSE, BOOL bFromMumSlider = FALSE, BOOL bFromDadSlider = FALSE)
	
	charCreatorData.pedData.charCreatorParentInfo.iMumHeadVariation = MP_HEAD_PRETTY
	charCreatorData.pedData.charCreatorParentInfo.iMumHeadVariation2 = MP_HEAD_PRETTY
	charCreatorData.pedData.charCreatorParentInfo.iDadHeadVariation = MP_HEAD_PRETTY
	charCreatorData.pedData.charCreatorParentInfo.iDadHeadVariation2 = MP_HEAD_PRETTY			
	
	// archetype specific!
	IF charCreatorData.pedData.archetype = ARCHETYPE_CUSTOM
	AND NOT bFromMumSlider
	AND NOT bFromDadSlider
		CHAR_CREATOR_RACE mumRace1
		CHAR_CREATOR_RACE mumRace2
		CHAR_CREATOR_RACE dadRace1
		CHAR_CREATOR_RACE dadRace2

		SWITCH charCreatorData.pedData.customArchetype
			// female
			CASE CUSTOM_ARCHETYPE_FEMALE_CHOLA
				mumRace1 = CHAR_CREATOR_WHITE
				mumRace2 = CHAR_CREATOR_WHITE
				dadRace1 = CHAR_CREATOR_INDIAN
				dadRace2 = CHAR_CREATOR_INDIAN
			BREAK
			CASE CUSTOM_ARCHETYPE_FEMALE_BUSINESS_1
				mumRace1 = CHAR_CREATOR_LATIN
				mumRace2 = CHAR_CREATOR_CHINESE
				dadRace1 = CHAR_CREATOR_CHINESE
				dadRace2 = CHAR_CREATOR_CHINESE
			BREAK
			CASE CUSTOM_ARCHETYPE_FEMALE_BUSINESS_2
				mumRace1 = CHAR_CREATOR_CHINESE
				mumRace2 = CHAR_CREATOR_CHINESE
				dadRace1 = CHAR_CREATOR_BLACK
				dadRace2 = CHAR_CREATOR_BLACK
			BREAK
			CASE CUSTOM_ARCHETYPE_FEMALE_CASUAL_1
				mumRace1 = CHAR_CREATOR_POLYNESIAN
				mumRace2 = CHAR_CREATOR_POLYNESIAN
				dadRace1 = CHAR_CREATOR_POLYNESIAN
				dadRace2 = CHAR_CREATOR_CHINESE
			BREAK
			CASE CUSTOM_ARCHETYPE_FEMALE_CASUAL_2
				mumRace1 = CHAR_CREATOR_LATIN
				mumRace2 = CHAR_CREATOR_LATIN
				dadRace1 = CHAR_CREATOR_WHITE
				dadRace2 = CHAR_CREATOR_INDIAN
			BREAK	
			CASE CUSTOM_ARCHETYPE_FEMALE_PARTY_SOUTH_AMERICAN
				mumRace1 = CHAR_CREATOR_LATIN
				mumRace2 = CHAR_CREATOR_POLYNESIAN
				dadRace1 = CHAR_CREATOR_WHITE
				dadRace2 = CHAR_CREATOR_BLACK
			BREAK	
			CASE CUSTOM_ARCHETYPE_FEMALE_CASUAL_3
				mumRace1 = CHAR_CREATOR_CHINESE
				mumRace2 = CHAR_CREATOR_BLACK
				dadRace1 = CHAR_CREATOR_BLACK
				dadRace2 = CHAR_CREATOR_WHITE
			BREAK
			CASE CUSTOM_ARCHETYPE_FEMALE_PARTY_EUROPEAN
				mumRace1 = CHAR_CREATOR_WHITE
				mumRace2 = CHAR_CREATOR_WHITE
				dadRace1 = CHAR_CREATOR_WHITE
				dadRace2 = CHAR_CREATOR_BLACK
			BREAK	
			CASE CUSTOM_ARCHETYPE_FEMALE_PARTY_EAST_ASIAN
				mumRace1 = CHAR_CREATOR_CHINESE
				mumRace2 = CHAR_CREATOR_CHINESE
				dadRace1 = CHAR_CREATOR_WHITE
				dadRace2 = CHAR_CREATOR_LATIN
			BREAK	
			CASE CUSTOM_ARCHETYPE_FEMALE_PARTY_AFRICAN
				mumRace1 = CHAR_CREATOR_BLACK
				mumRace2 = CHAR_CREATOR_BLACK
				dadRace1 = CHAR_CREATOR_BLACK
				dadRace2 = CHAR_CREATOR_BLACK
			BREAK	
			CASE CUSTOM_ARCHETYPE_FEMALE_PARTY_PACIFIC_ISLANDER
				mumRace1 = CHAR_CREATOR_POLYNESIAN
				mumRace2 = CHAR_CREATOR_POLYNESIAN
				dadRace1 = CHAR_CREATOR_POLYNESIAN
				dadRace2 = CHAR_CREATOR_POLYNESIAN
			BREAK	
			CASE CUSTOM_ARCHETYPE_FEMALE_COUCH_POTATO_EUROPEAN
				mumRace1 = CHAR_CREATOR_WHITE
				mumRace2 = CHAR_CREATOR_WHITE
				dadRace1 = CHAR_CREATOR_WHITE
				dadRace2 = CHAR_CREATOR_WHITE
			BREAK	
			CASE CUSTOM_ARCHETYPE_FEMALE_COUCH_POTATO_MIXED_1
				mumRace1 = CHAR_CREATOR_POLYNESIAN
				mumRace2 = CHAR_CREATOR_WHITE
				dadRace1 = CHAR_CREATOR_CHINESE
				dadRace2 = CHAR_CREATOR_POLYNESIAN
			BREAK	
			CASE CUSTOM_ARCHETYPE_FEMALE_COUCH_POTATO_MIXED_2
				mumRace1 = CHAR_CREATOR_CHINESE
				mumRace2 = CHAR_CREATOR_CHINESE
				dadRace1 = CHAR_CREATOR_LATIN
				dadRace2 = CHAR_CREATOR_CHINESE
			BREAK	
			CASE CUSTOM_ARCHETYPE_FEMALE_COUCH_POTATO_MIXED_3
				mumRace1 = CHAR_CREATOR_LATIN
				mumRace2 = CHAR_CREATOR_LATIN
				dadRace1 = CHAR_CREATOR_LATIN
				dadRace2 = CHAR_CREATOR_CHINESE
			BREAK	
			CASE CUSTOM_ARCHETYPE_FEMALE_CHOLA_MIXED
				mumRace1 = CHAR_CREATOR_WHITE
				mumRace2 = CHAR_CREATOR_BLACK
				dadRace1 = CHAR_CREATOR_BLACK
				dadRace2 = CHAR_CREATOR_WHITE
			BREAK
			CASE CUSTOM_ARCHETYPE_FEMALE_BUSINESS_3
				mumRace1 = CHAR_CREATOR_BLACK
				mumRace2 = CHAR_CREATOR_LATIN
				dadRace1 = CHAR_CREATOR_BLACK
				dadRace2 = CHAR_CREATOR_BLACK
			BREAK
			CASE CUSTOM_ARCHETYPE_FEMALE_PARTY_HARD
				mumRace1 = CHAR_CREATOR_WHITE
				mumRace2 = CHAR_CREATOR_WHITE
				dadRace1 = CHAR_CREATOR_LATIN
				dadRace2 = CHAR_CREATOR_POLYNESIAN
				
				IF charCreatorData.bCreatedFirstPrettyChild
					charCreatorData.pedData.charCreatorParentInfo.iMumHeadVariation = MP_HEAD_UGLY
					charCreatorData.pedData.charCreatorParentInfo.iMumHeadVariation2 = MP_HEAD_UGLY
					charCreatorData.pedData.charCreatorParentInfo.iDadHeadVariation = MP_HEAD_UGLY
					charCreatorData.pedData.charCreatorParentInfo.iDadHeadVariation2 = MP_HEAD_UGLY	
				ENDIF
			BREAK
			
			// male
			CASE CUSTOM_ARCHETYPE_MALE_BUSINESS_1
				mumRace1 = CHAR_CREATOR_BLACK
				mumRace2 = CHAR_CREATOR_BLACK
				dadRace1 = CHAR_CREATOR_BLACK
				dadRace2 = CHAR_CREATOR_BLACK
			BREAK
			CASE CUSTOM_ARCHETYPE_MALE_CASUAL_ASIAN
				mumRace1 = CHAR_CREATOR_CHINESE
				mumRace2 = CHAR_CREATOR_CHINESE
				dadRace1 = CHAR_CREATOR_CHINESE
				dadRace2 = CHAR_CREATOR_CHINESE
			BREAK
			CASE CUSTOM_ARCHETYPE_MALE_SLOB_EUROPEAN
				mumRace1 = CHAR_CREATOR_WHITE
				mumRace2 = CHAR_CREATOR_WHITE
				dadRace1 = CHAR_CREATOR_WHITE
				dadRace2 = CHAR_CREATOR_WHITE
				
				IF charCreatorData.bCreatedFirstPrettyChild
					charCreatorData.pedData.charCreatorParentInfo.iMumHeadVariation = MP_HEAD_UGLY
					charCreatorData.pedData.charCreatorParentInfo.iMumHeadVariation2 = MP_HEAD_UGLY
					charCreatorData.pedData.charCreatorParentInfo.iDadHeadVariation = MP_HEAD_UGLY
					charCreatorData.pedData.charCreatorParentInfo.iDadHeadVariation2 = MP_HEAD_UGLY		
				ENDIF
			BREAK
			CASE CUSTOM_ARCHETYPE_MALE_CASUAL_1
				mumRace1 = CHAR_CREATOR_BLACK
				mumRace2 = CHAR_CREATOR_BLACK
				dadRace1 = CHAR_CREATOR_BLACK
				dadRace2 = CHAR_CREATOR_BLACK
			BREAK
			CASE CUSTOM_ARCHETYPE_MALE_DIRTY_PARTY
				mumRace1 = CHAR_CREATOR_WHITE
				mumRace2 = CHAR_CREATOR_BLACK
				dadRace1 = CHAR_CREATOR_LATIN
				dadRace2 = CHAR_CREATOR_BLACK
				
				IF charCreatorData.bCreatedFirstPrettyChild
					charCreatorData.pedData.charCreatorParentInfo.iMumHeadVariation = MP_HEAD_UGLY
					charCreatorData.pedData.charCreatorParentInfo.iMumHeadVariation2 = MP_HEAD_UGLY
					charCreatorData.pedData.charCreatorParentInfo.iDadHeadVariation = MP_HEAD_UGLY
					charCreatorData.pedData.charCreatorParentInfo.iDadHeadVariation2 = MP_HEAD_UGLY			
				ENDIF
			BREAK
		ENDSWITCH
		
		SET_CHAR_CREATOR_PARENT_RACE(charCreatorData, CHAR_CREATOR_PED_MUM, mumRace1, CHAR_CREATOR_RACE_INDEX_ONE)
		SET_CHAR_CREATOR_PARENT_RACE(charCreatorData, CHAR_CREATOR_PED_MUM, mumRace2, CHAR_CREATOR_RACE_INDEX_TWO)
		SET_CHAR_CREATOR_PARENT_RACE(charCreatorData, CHAR_CREATOR_PED_DAD, dadRace1, CHAR_CREATOR_RACE_INDEX_ONE)
		SET_CHAR_CREATOR_PARENT_RACE(charCreatorData, CHAR_CREATOR_PED_DAD, dadRace2, CHAR_CREATOR_RACE_INDEX_TWO)		
	ELSE
		// not archetype specific
		INT iMaxRaces = ENUM_TO_INT(CHAR_CREATOR_MAX_RACES)
		INT iRandom 
		INT iAttempts = 0
		BOOL bFoundParent = FALSE		
		//INT iSlot = charCreatorData.iDadOption
		
		IF NOT bFromMumSlider
			//IF bFromDadSlider
			//AND charCreatorData.cachedDad[iSlot].bFilled
				// restore cached dad
			//	charCreatorData.pedData.charCreatorParentInfo.charDadRace = charCreatorData.cachedDad[iSlot].race
			//	charCreatorData.pedData.charCreatorParentInfo.charDadRace2 = charCreatorData.cachedDad[iSlot].race2 
			//	charCreatorData.pedData.charCreatorParentInfo.iDadSpecialHeadIndex = charCreatorData.cachedDad[iSlot].iSpecialHeadIndex
			//	charCreatorData.pedData.fDadDominance = charCreatorData.cachedDad[iSlot].fDominance 
			//	charCreatorData.pedData.charCreatorParentInfo.iDadHeadVariation = charCreatorData.cachedDad[iSlot].iRaceHead 
			//	charCreatorData.pedData.charCreatorParentInfo.iDadHeadVariation2 = charCreatorData.cachedDad[iSlot].iRaceHead2 
			//ELSE
				IF (ARE_CHAR_CREATOR_COLLECTOR_EDITIONS_AVAILABLE() OR IS_GAME_LINKED_TO_SOCIAL_CLUB()
				AND charCreatorData.iNumberSinceSpecialDadGenerated >= SPECIAL_PED_THRESHOLD)
				#IF IS_DEBUG_BUILD
					OR charCreatorData.bFakeCollectorEdition
					OR charCreatorData.bFakeSocialClub
				#ENDIF
					iMaxRaces = ENUM_TO_INT(CHAR_CREATOR_MAX_RACES) + 1
				ENDIF

				// Set up the players first Dad head race
				WHILE iAttempts < 5
				AND NOT bFoundParent
					iRandom = GET_RANDOM_INT_IN_RANGE(0, iMaxRaces)
					IF bAllowDup
						bFoundParent = TRUE
					ELSE
						IF iRandom <> charCreatorData.iLastRandomDad1
							bFoundParent = TRUE
						ELSE
							PRINTSTRING("same as last time dad1 - try again") PRINTNL()				
						ENDIF
					ENDIF
					iAttempts++
				ENDWHILE
				charCreatorData.iLastRandomDad1 = iRandom	
				
				IF iRandom = ENUM_TO_INT(CHAR_CREATOR_MAX_RACES)
				
					IF IS_CHAR_CREATOR_RANDOM_SPECIAL_HEAD_AVAILABLE(CHAR_CREATOR_PED_DAD)
						iRandom = ENUM_TO_INT(CHAR_CREATOR_SPECIAL_RACE)
						SET_CHAR_CREATOR_PARENT_SPECIAL_HEAD(charCreatorData, CHAR_CREATOR_PED_DAD, GET_CHAR_CREATOR_RANDOM_SPECIAL_HEAD_INDEX(CHAR_CREATOR_PED_DAD))
						charCreatorData.iNumberSinceSpecialDadGenerated = 0
					ELSE
						iRandom = GET_RANDOM_INT_IN_RANGE(0, ENUM_TO_INT(CHAR_CREATOR_MAX_RACES))
					ENDIF
					
				ENDIF
				
				// Set the race of the first Dad head
				SET_CHAR_CREATOR_PARENT_RACE(charCreatorData, CHAR_CREATOR_PED_DAD, INT_TO_ENUM(CHAR_CREATOR_RACE, iRandom), CHAR_CREATOR_RACE_INDEX_ONE)
				
				// Set the race of the second Dad head
				iAttempts = 0
				bFoundParent = FALSE
				
				WHILE iAttempts < 5
				AND NOT bFoundParent
					iRandom = GET_RANDOM_INT_IN_RANGE(0, ENUM_TO_INT(CHAR_CREATOR_MAX_RACES))
					IF bAllowDup
						bFoundParent = TRUE
					ELSE
						IF iRandom <> charCreatorData.iLastRandomDad2
							bFoundParent = TRUE
						ELSE
							PRINTSTRING("same as last time dad2 - try again") PRINTNL()				
						ENDIF
					ENDIF
					iAttempts++
				ENDWHILE
				charCreatorData.iLastRandomDad2 = iRandom		
				
				SET_CHAR_CREATOR_PARENT_RACE(charCreatorData, CHAR_CREATOR_PED_DAD, INT_TO_ENUM(CHAR_CREATOR_RACE, iRandom), CHAR_CREATOR_RACE_INDEX_TWO)
			
				// ugliness
				SET_GRANDPARENT_HEAD_VARIATION(charCreatorData, charCreatorData.pedData.charCreatorParentInfo.iDadHeadVariation, CHAR_CREATOR_PED_DAD, CHAR_CREATOR_RACE_INDEX_ONE)
				SET_GRANDPARENT_HEAD_VARIATION(charCreatorData, charCreatorData.pedData.charCreatorParentInfo.iDadHeadVariation2, CHAR_CREATOR_PED_DAD, CHAR_CREATOR_RACE_INDEX_TWO)
			//ENDIF
		ENDIF
		
		//iSlot = charCreatorData.iMumOption
		IF NOT bFromDadSlider
			//IF bFromMumSlider
			//AND charCreatorData.cachedMum[iSlot].bFilled
				// restore cached mum
			//	charCreatorData.pedData.charCreatorParentInfo.charMumRace = charCreatorData.cachedMum[iSlot].race
			//	charCreatorData.pedData.charCreatorParentInfo.charMumRace2 = charCreatorData.cachedMum[iSlot].race2 
			//	charCreatorData.pedData.charCreatorParentInfo.iMumSpecialHeadIndex = charCreatorData.cachedMum[iSlot].iSpecialHeadIndex
			//	charCreatorData.pedData.fMumDominance = charCreatorData.cachedMum[iSlot].fDominance 
			//	charCreatorData.pedData.charCreatorParentInfo.iMumHeadVariation = charCreatorData.cachedMum[iSlot].iRaceHead 
			//	charCreatorData.pedData.charCreatorParentInfo.iMumHeadVariation2 = charCreatorData.cachedMum[iSlot].iRaceHead2 
			//ELSE		
				iMaxRaces = ENUM_TO_INT(CHAR_CREATOR_MAX_RACES)
				
				IF (ARE_CHAR_CREATOR_COLLECTOR_EDITIONS_AVAILABLE()
				AND charCreatorData.iNumberSinceSpecialMumGenerated >= SPECIAL_PED_THRESHOLD)
				#IF IS_DEBUG_BUILD
					OR charCreatorData.bFakeCollectorEdition
				#ENDIF
					iMaxRaces = ENUM_TO_INT(CHAR_CREATOR_MAX_RACES) + 1
				ENDIF
				
				iAttempts = 0
				bFoundParent = FALSE
				
				WHILE iAttempts < 5
				AND NOT bFoundParent
					iRandom = GET_RANDOM_INT_IN_RANGE(0, iMaxRaces)
					IF bAllowDup
						bFoundParent = TRUE
					ELSE
						IF iRandom <> charCreatorData.iLastRandomMum1
							bFoundParent = TRUE
						ELSE
							PRINTSTRING("same as last time mum1 - try again") PRINTNL()				
						ENDIF
					ENDIF
					iAttempts++
				ENDWHILE
				charCreatorData.iLastRandomMum1 = iRandom

				IF iRandom = ENUM_TO_INT(CHAR_CREATOR_MAX_RACES)
				
					IF IS_CHAR_CREATOR_RANDOM_SPECIAL_HEAD_AVAILABLE(CHAR_CREATOR_PED_MUM)
						iRandom = ENUM_TO_INT(CHAR_CREATOR_SPECIAL_RACE)				
						SET_CHAR_CREATOR_PARENT_SPECIAL_HEAD(charCreatorData, CHAR_CREATOR_PED_MUM, GET_CHAR_CREATOR_RANDOM_SPECIAL_HEAD_INDEX(CHAR_CREATOR_PED_MUM))				
						charCreatorData.iNumberSinceSpecialMumGenerated = 0
					ELSE
						iRandom = GET_RANDOM_INT_IN_RANGE(0, ENUM_TO_INT(CHAR_CREATOR_MAX_RACES))
					ENDIF
				ENDIF

				// Set the race of the first Mum head
				SET_CHAR_CREATOR_PARENT_RACE(charCreatorData, CHAR_CREATOR_PED_MUM, INT_TO_ENUM(CHAR_CREATOR_RACE, iRandom), CHAR_CREATOR_RACE_INDEX_ONE)
				
				// Set the race of the second Mum head
				iAttempts = 0
				bFoundParent = FALSE
				
				WHILE iAttempts < 5
				AND NOT bFoundParent
					iRandom = GET_RANDOM_INT_IN_RANGE(0, ENUM_TO_INT(CHAR_CREATOR_MAX_RACES))
					IF bAllowDup
						bFoundParent = TRUE
					ELSE
						IF iRandom <> charCreatorData.iLastRandomMum2
							bFoundParent = TRUE
						ELSE
							PRINTSTRING("same as last time mum2 - try again") PRINTNL()				
						ENDIF
					ENDIF
					iAttempts++
				ENDWHILE
				charCreatorData.iLastRandomMum2 = iRandom			
				
				SET_CHAR_CREATOR_PARENT_RACE(charCreatorData, CHAR_CREATOR_PED_MUM, INT_TO_ENUM(CHAR_CREATOR_RACE, iRandom), CHAR_CREATOR_RACE_INDEX_TWO)
			
				// ugliness
				SET_GRANDPARENT_HEAD_VARIATION(charCreatorData, charCreatorData.pedData.charCreatorParentInfo.iMumHeadVariation, CHAR_CREATOR_PED_MUM, CHAR_CREATOR_RACE_INDEX_ONE)
				SET_GRANDPARENT_HEAD_VARIATION(charCreatorData, charCreatorData.pedData.charCreatorParentInfo.iMumHeadVariation2, CHAR_CREATOR_PED_MUM, CHAR_CREATOR_RACE_INDEX_TWO)
							
			//ENDIF
			
			
		ENDIF
	ENDIF
	
	UPDATE_GRANDAD_HEAD(charCreatorData, TRUE)
	UPDATE_GRANDMA_HEAD(charCreatorData, TRUE)	
	UPDATE_GRANDAD_HEAD(charCreatorData, FALSE)
	UPDATE_GRANDMA_HEAD(charCreatorData, FALSE)	
	
	SET_BIT(charCreatorData.iCharCreatorBitset, CHAR_CREATOR_FORCE_HERITAGE_UPDATE)
ENDPROC




/* ********************************************
*			CHARACTER APPEARENCE
***********************************************/

/// PURPOSE: Calculates the exact headblend based on dominance and gender
FUNC FLOAT GET_CHAR_CREATOR_PARENT_DOMINANCE(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData, CHAR_CREATOR_GENDER aGender)
	
	FLOAT fDomMin
	FLOAT fDomMax
	
	IF aGender = CHAR_CREATOR_GENDER_MALE
		fDomMin = CHAR_CREATOR_M_DOM_MIN //0.30
		fDomMax = CHAR_CREATOR_M_DOM_MAX //0.85
	ELSE
		fDomMin = CHAR_CREATOR_F_DOM_MIN //0.15
		fDomMax = CHAR_CREATOR_F_DOM_MAX //0.70
	ENDIF
	
	//PRINTLN("[CHARCREATOR] GET_CHAR_CREATOR_PARENT_DOMINANCE - Dom is: ", (fDomMin + ((fDomMax - fDomMin) * (charCreatorData.pedData.fParentDominance * 0.01))))
	RETURN (fDomMin + ((fDomMax - fDomMin) * (charCreatorData.pedData.fParentDominance * 0.01)))
ENDFUNC

FUNC FLOAT GET_CHAR_CREATOR_PARENT_DOMINANCE_FROM_STAT(CHAR_CREATOR_GENDER aGender, FLOAT fDomStat)
	
	FLOAT fDomMin
	FLOAT fDomMax

	IF aGender = CHAR_CREATOR_GENDER_MALE
		fDomMin = CHAR_CREATOR_M_DOM_MIN 	//0.30
		fDomMax = CHAR_CREATOR_M_DOM_MAX 	//0.85
	ELSE
		fDomMin = CHAR_CREATOR_F_DOM_MIN   //0.15
		fDomMax = CHAR_CREATOR_F_DOM_MAX  //0.70	
	ENDIF
	
	RETURN ((fDomStat - fDomMin) / (fDomMax - fDomMin)) * 100
ENDFUNC

/// PURPOSE: Calculates ugly percentage of parents
FUNC BOOL CALC_CHAR_CREATOR_UGLY_VALUE(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData)
	
	PRINTLN("[CHARCREATOR] CALC_CHAR_CREATOR_UGLY_VALUE - Calculate UGLY value")
	
	BOOL bUglyUpdated = FALSE
	
	INT iSleepHrs 		= (GET_CHAR_CREATOR_HOURS_BOUNDS_FOR_LIFESTYLE(LIFESTYLE_SLEEP, TRUE) - GET_CHAR_CREATOR_HOURS_BOUNDS_FOR_LIFESTYLE(LIFESTYLE_SLEEP, FALSE))

	FLOAT fUglyTotal = 0.0
	
	// Do Party
	FLOAT fPartyPercentage = GET_CHAR_CREATOR_LIFESTYLE_PERCENTAGE(charCreatorData, LIFESTYLE_PARTY)
	IF fPartyPercentage >= 0.75
		PRINTSTRING("party percentage = ") PRINTFLOAT(fPartyPercentage) PRINTNL()
		fUglyTotal += (fPartyPercentage * 35.0)
	ENDIF
	
	PRINTLN("		- Calculate after PARTY: ", fUglyTotal)
	
	// Do Couch
	fUglyTotal += (GET_CHAR_CREATOR_LIFESTYLE_PERCENTAGE(charCreatorData, LIFESTYLE_TV) * 25.0)
	
	PRINTLN("		- Calculate after COUCH: ", fUglyTotal)
	
	// Do Illegal
	FLOAT fIllegalPercentage = GET_CHAR_CREATOR_LIFESTYLE_PERCENTAGE(charCreatorData, LIFESTYLE_ILLEGAL)
	IF fIllegalPercentage >= 0.6
		PRINTSTRING("illegal percentage = ") PRINTFLOAT(fIllegalPercentage) PRINTNL()
		fUglyTotal += (fIllegalPercentage * 25.0)
	ENDIF
	
	PRINTLN("		- Calculate after ILLEGAL: ", fUglyTotal)
	
	// Do Sleep	
	FLOAT fSleepAdjustedPercent = (TO_FLOAT(charCreatorData.pedData.charLifestyleHours[LIFESTYLE_SLEEP] - GET_CHAR_CREATOR_HOURS_BOUNDS_FOR_LIFESTYLE(LIFESTYLE_SLEEP, FALSE)) / iSleepHrs)
	IF fSleepAdjustedPercent <= 0.0625
		PRINTSTRING("sleep adjusted percent = ") PRINTFLOAT(fSleepAdjustedPercent) PRINTNL()
		fUglyTotal += ((1 - fSleepAdjustedPercent) * 35)
	ENDIF
	
	PRINTLN("		- Calculate after SLEEP: ", fUglyTotal)
	
	IF charCreatorData.pedData.fParentUgly != (fUglyTotal) / 100.0
		PRINTLN("[CHARCREATOR] CALC_CHAR_CREATOR_UGLY_VALUE - Ugly value has changed")
		bUglyUpdated = TRUE
	ENDIF
	
	charCreatorData.pedData.fParentUgly = (fUglyTotal) / 100.0
	
	PRINTLN("[CHARCREATOR] CALC_CHAR_CREATOR_UGLY_VALUE - New UGLY Val: ", charCreatorData.pedData.fParentUgly)
	
	IF charCreatorData.pedData.fParentUgly < 0.0
		PRINTLN("[CHARCREATOR] CALC_CHAR_CREATOR_UGLY_VALUE - UGLY is less than 0")
		charCreatorData.pedData.fParentUgly = 0.0
	ENDIF
	
	IF charCreatorData.pedData.fParentUgly >= 1.0
		PRINTLN("[CHARCREATOR] CALC_CHAR_CREATOR_UGLY_VALUE - UGLY is more than 1")
		charCreatorData.pedData.fParentUgly = 1.0
	ENDIF
	
	RETURN bUglyUpdated
ENDFUNC

PROC SET_CHAR_CREATOR_MOOD(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData)
	STRING sMood = GET_PLAYER_MOOD_ANIM_FROM_INDEX(ENUM_TO_INT(charCreatorData.pedData.charMood))
	IF ARE_STRINGS_EQUAL(sMood, "mood_smug_1")
		sMood = "mood_Happy_1"
	ENDIF
	IF ARE_STRINGS_EQUAL(sMood, "mood_sulk_1")
		sMood = "mood_Angry_1"
	ENDIF	
	
	IF IS_CHAR_CREATOR_PED_OK(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON])
		SET_FACIAL_IDLE_ANIM_OVERRIDE(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON], sMood)
	ENDIF
	
	IF IS_CHAR_CREATOR_PED_OK(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER])
		SET_FACIAL_IDLE_ANIM_OVERRIDE(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER], sMood)
	ENDIF
ENDPROC

PROC SET_PARENT_STATIC_FACE(PED_INDEX &aPed)

	IF GET_ENTITY_MODEL(aPed) = MP_M_Marston_01
		SET_FACIAL_IDLE_ANIM_OVERRIDE(aPed, "Pose_Angry_1")
	ELSE
		SET_FACIAL_IDLE_ANIM_OVERRIDE(aPed, "Pose_Happy_1")
	ENDIF
ENDPROC

/// PURPOSE: Set the correct mood of the player
PROC SET_CHAR_CREATOR_MOOD_FROM_VALUE(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData, FLOAT fMoodPercentage)
	
	IF fMoodPercentage  < 0
		fMoodPercentage = 0
	ELIF fMoodPercentage > 1.0
		fMoodPercentage = 1.0
	ENDIF
	
	IF fMoodPercentage < 0.25
		charCreatorData.pedData.charMood = CHAR_CREATOR_HAPPY
	ELIF fMoodPercentage < 0.50
		charCreatorData.pedData.charMood = CHAR_CREATOR_SMUG
	ELIF fMoodPercentage < 0.70
		charCreatorData.pedData.charMood = CHAR_CREATOR_NORMAL
	ELIF fMoodPercentage < 0.85
		charCreatorData.pedData.charMood = CHAR_CREATOR_SAD
	ELSE
		charCreatorData.pedData.charMood = CHAR_CREATOR_ANGRY
	ENDIF
	
	PRINTLN("[CHARCREATOR] SET_CHAR_CREATOR_MOOD - Mood of characters: ", ENUM_TO_INT(charCreatorData.pedData.charMood), " fMood: ", fMoodPercentage)
	
	SET_CHAR_CREATOR_MOOD(charCreatorData)
ENDPROC


/// PURPOSE: Calculates the values for all overlays
PROC CALC_CHAR_CREATOR_OVERLAY_VALUES(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData)

	FLOAT fPartyVal 	= GET_CHAR_CREATOR_LIFESTYLE_PERCENTAGE(charCreatorData, LIFESTYLE_PARTY)
	FLOAT fCouchVal 	= GET_CHAR_CREATOR_LIFESTYLE_PERCENTAGE(charCreatorData, LIFESTYLE_TV)
	FLOAT fIllegalVal	= GET_CHAR_CREATOR_LIFESTYLE_PERCENTAGE(charCreatorData, LIFESTYLE_ILLEGAL)
	FLOAT fSleepVal		= GET_CHAR_CREATOR_LIFESTYLE_PERCENTAGE(charCreatorData, LIFESTYLE_SLEEP)
	FLOAT fFamilyVal	= GET_CHAR_CREATOR_LIFESTYLE_PERCENTAGE(charCreatorData, LIFESTYLE_FAMILY)
	FLOAT fSportVal		= GET_CHAR_CREATOR_LIFESTYLE_PERCENTAGE(charCreatorData, LIFESTYLE_SPORT)
	FLOAT fLegalVal		= GET_CHAR_CREATOR_LIFESTYLE_PERCENTAGE(charCreatorData, LIFESTYLE_LEGAL)
	
	// Calculate our Damage:
	FLOAT fOverlayPer = (fIllegalVal * 90.0) + (fSportVal * 10.0)
	fOverlayPer = (fOverlayPer * 0.01)

	SET_CHAR_CREATOR_OVERLAY_VALUES(charCreatorData, fOverlayPer, CHAR_CREATOR_DAMAGE)

	#IF IS_DEBUG_BUILD
		charCreatorData.iDamagePercent = ROUND(fOverlayPer * 100)
	#ENDIF	
	
//	fOverlayPer = (fPartyVal * 50.0) + (fFamilyVal * 50.0)
//	fOverlayPer = (fOverlayPer * 0.01)
//	SET_CHAR_CREATOR_OVERLAY_VALUES(charCreatorData, fOverlayPer, CHAR_CREATOR_MAKEUP)
	
	// Calculate our Blemish:
	fOverlayPer = (fPartyVal * 40.0) + (fCouchVal * 25.0) + (fIllegalVal * 60.0) + (fLegalVal * 40.0) - (fFamilyVal * 50.0) - (fSportVal * 40.0)
	
	IF fSleepVal <= 0.6
		fOverlayPer += (fSleepVal / 0.6) * 50.0
	ELSE
		fOverlayPer += 50.0
	ENDIF
	fOverlayPer = (fOverlayPer * 0.01)
	
	SET_CHAR_CREATOR_OVERLAY_VALUES(charCreatorData, fOverlayPer, CHAR_CREATOR_BLEMISH)
	
	fOverlayPer = (fIllegalVal * 50.0) + (fPartyVal * 30.0) + (fCouchVal * 30.0) - (fLegalVal * 50.0) - (fSportVal * 30.0) - (fFamilyVal * 30.0)
	fOverlayPer = (fOverlayPer * 0.01)
	SET_CHAR_CREATOR_MOOD_FROM_VALUE(charCreatorData, fOverlayPer)
	
	// Calculate our Blemish
	INT iSleepHrs = (GET_CHAR_CREATOR_HOURS_BOUNDS_FOR_LIFESTYLE(LIFESTYLE_SLEEP, TRUE) - GET_CHAR_CREATOR_HOURS_BOUNDS_FOR_LIFESTYLE(LIFESTYLE_SLEEP, FALSE))
	FLOAT fSleepAdjustedPercent = (TO_FLOAT(charCreatorData.pedData.charLifestyleHours[LIFESTYLE_SLEEP] - GET_CHAR_CREATOR_HOURS_BOUNDS_FOR_LIFESTYLE(LIFESTYLE_SLEEP, FALSE)) / iSleepHrs)
	fOverlayPer = (fPartyVal * 50.0) + (fSportVal * 40.0) + (fCouchVal * 40.0) - (fSleepAdjustedPercent * 25.0)
	fOverlayPer = (fOverlayPer * 0.01)
	
	SET_CHAR_CREATOR_OVERLAY_VALUES(charCreatorData, fOverlayPer, CHAR_CREATOR_BASE_BLEMISH)
	
	#IF IS_DEBUG_BUILD
		charCreatorData.iBasePercent = ROUND(fOverlayPer * 100)
	#ENDIF
	
ENDPROC

FUNC INT GET_CHAR_CREATOR_EYEBROW_VALUE(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData, CHAR_CREATOR_GENDER aGender)

	IF aGender = CHAR_CREATOR_GENDER_MALE
		RETURN charCreatorData.pedData.iSonEyebrow
	ELSE
		RETURN charCreatorData.pedData.iDaughterEyebrow
	ENDIF
ENDFUNC

/// PURPOSE: Sets the overlays on our children
PROC PROCESS_CHAR_CREATOR_CHILD_OVERLAYS(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData, BOOL bOnlySon = FALSE, BOOL bOnlyDaughter = FALSE, BOOL bDontDoCosmetics = FALSE)

	IF NOT IS_BIT_SET(charCreatorData.iCharCreatorBitset, CHAR_CREATOR_ALL_PEDS_LOADED)
		PRINTLN("[CHARCREATOR] PROCESS_CHAR_CREATOR_CHILD_OVERLAYS - We have not completed initial blend")
		EXIT
	ENDIF

	PRINTLN("[CHARCREATOR] PROCESS_CHAR_CREATOR_CHILD_OVERLAYS - Applying overlays")

	INT iValue
	FLOAT fBlend

	IF IS_CHAR_CREATOR_PED_OK(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON])
	AND IS_CHAR_CREATOR_PED_OK(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER])

		// Apply overlays to Son
		GET_CHAR_CREATOR_OVERLAY_VALUES(charCreatorData, CHAR_CREATOR_DAMAGE, iValue, fBlend)
		
		PRINTLN("[CHARCREATOR] PROCESS_CHAR_CREATOR_CHILD_OVERLAYS - DAMAGE: ", iValue, " Blend: ", fBlend)
		
		IF NOT bOnlyDaughter
			SET_PED_HEAD_OVERLAY(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON], HOS_DAMAGE, iValue, fBlend)
		ENDIF	
			
		IF NOT bOnlySon
			SET_PED_HEAD_OVERLAY(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER], HOS_DAMAGE, iValue, fBlend)
		ENDIF
		
		// Apply our wrinkles
		APPLY_CHAR_CREATOR_WRINKLES_OVERLAY(charCreatorData, bOnlySon, bOnlyDaughter)
		
		IF NOT bDontDoCosmetics
			// Apply our beard
			APPLY_CHAR_CREATOR_BEARD_OVERLAY(charCreatorData)
			
			// Apply makeup
			IF NOT bOnlySon
				APPLY_CHAR_CREATOR_MAKEUP_OVERLAY(charCreatorData)
			ENDIF
		ENDIF
		
		GET_CHAR_CREATOR_OVERLAY_VALUES(charCreatorData, CHAR_CREATOR_BLEMISH, iValue, fBlend)
		PRINTLN("[CHARCREATOR] PROCESS_CHAR_CREATOR_CHILD_OVERLAYS - BLEMISH: ", iValue, " Blend: ", fBlend)
		
		IF NOT bOnlyDaughter
			SET_PED_HEAD_OVERLAY(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON], HOS_BLEMISHES, iValue, fBlend)
		ENDIF
		IF NOT bOnlySon
			SET_PED_HEAD_OVERLAY(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER], HOS_BLEMISHES, iValue, fBlend)
		ENDIF
		
		GET_CHAR_CREATOR_OVERLAY_VALUES(charCreatorData, CHAR_CREATOR_BASE_BLEMISH, iValue, fBlend)
		PRINTLN("[CHARCREATOR] PROCESS_CHAR_CREATOR_CHILD_OVERLAYS - BASE_BLEMISH: ", iValue, " Blend: ", fBlend)
		IF NOT bOnlyDaughter
			SET_PED_HEAD_OVERLAY(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON], HOS_BASE_DETAIL, iValue, fBlend)
		ENDIF
		IF NOT bOnlySon
			SET_PED_HEAD_OVERLAY(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER], HOS_BASE_DETAIL, iValue, fBlend)
		ENDIF
		
		IF NOT bDontDoCosmetics
			GET_CHAR_CREATOR_OVERLAY_VALUES(charCreatorData, CHAR_CREATOR_MAKEUP, iValue, fBlend)
			
			IF NOT bOnlyDaughter
				PRINTLN("[CHARCREATOR] PROCESS_CHAR_CREATOR_CHILD_OVERLAYS - Male - Applying eyebrows: ", GET_CHAR_CREATOR_EYEBROW_VALUE(charCreatorData, CHAR_CREATOR_GENDER_MALE))
				SET_PED_HEAD_OVERLAY(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON], HOS_EYEBROW, GET_CHAR_CREATOR_EYEBROW_VALUE(charCreatorData, CHAR_CREATOR_GENDER_MALE), 1.0)
			ENDIF
			
			IF NOT bOnlySon
				IF iValue < 0
					PRINTLN("[CHARCREATOR] PROCESS_CHAR_CREATOR_CHILD_OVERLAYS - Female - applying eyebrows since we do not have makeup")
					SET_PED_HEAD_OVERLAY(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER], HOS_EYEBROW, GET_CHAR_CREATOR_EYEBROW_VALUE(charCreatorData, CHAR_CREATOR_GENDER_FEMALE), 1.0)
				ELSE
					PRINTLN("[CHARCREATOR] PROCESS_CHAR_CREATOR_CHILD_OVERLAYS - Female - not applying eyebrows since we have makeup")
					SET_PED_HEAD_OVERLAY(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER], HOS_EYEBROW, GET_CHAR_CREATOR_EYEBROW_VALUE(charCreatorData, CHAR_CREATOR_GENDER_FEMALE), 0.0)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE: Begins an update to parents when blend has changed and note child as needing update (Rob, this currently isn't used since we have a fixed 50% for grandparents now, may want to open this up though)
PROC PROCESS_CHAR_CREATOR_PARENT_BLEND_UPDATE(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData)

	IF IS_CHAR_CREATOR_PED_OK(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAD])
		PRINTLN("[CHARCREATOR] PROCESS_CHAR_CREATOR_PARENT_UPDATE - Triggering an update to the dad: ", NATIVE_TO_INT(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAD]) ," -> child, Blend %: ", charCreatorData.pedData.fParentUgly)
		UPDATE_PED_HEAD_BLEND_DATA(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAD], charCreatorData.pedData.fParentUgly, 0.0, 0.0)
	ENDIF
	
	IF IS_CHAR_CREATOR_PED_OK(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_MUM])
		PRINTLN("[CHARCREATOR] PROCESS_CHAR_CREATOR_PARENT_UPDATE - Triggering an update to the mum: ", NATIVE_TO_INT(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_MUM]) ," -> child, Blend %: ", charCreatorData.pedData.fParentUgly)
		UPDATE_PED_HEAD_BLEND_DATA(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_MUM], charCreatorData.pedData.fParentUgly, 0.0, 0.0)
	ENDIF
	
	// Now need to mark the child as needing an update
	PRINTLN("[CHARCREATOR] PROCESS_CHAR_CREATOR_PARENT_UPDATE - SET_BIT(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_UPDATE_CHILD_BLEND)")
	SET_BIT(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_UPDATE_CHILD_BLEND)
ENDPROC

/*
FUNC INT GET_CHAR_CREATOR_PARENT_HEAD_VARIATION(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData, CHAR_CREATOR_PED aPed, INT iRaceIndex)

	FLOAT fUglyPercent = charCreatorData.pedData.fParentUgly

	BOOL bExtraHead = DOES_CHAR_CREATOR_PARENT_RACE_HAVE_EXTRA_HEAD(charCreatorData, aPed, iRaceIndex)
	
	// Initial splits over the heads, may need tweaking
	IF bExtraHead
		IF fUglyPercent <= 0.75
			RETURN MP_HEAD_PRETTY
		ELIF fUglyPercent <= 0.90
			RETURN MP_HEAD_EXTRA1
		ELSE
			RETURN MP_HEAD_UGLY
		ENDIF
	ELSE
		IF fUglyPercent <= 0.75
			RETURN MP_HEAD_PRETTY
		ELIF fUglyPercent <= 0.90
			IF iRaceIndex = CHAR_CREATOR_RACE_INDEX_ONE
				RETURN MP_HEAD_UGLY
			ELSE
				RETURN MP_HEAD_PRETTY
			ENDIF
		ELSE
			RETURN MP_HEAD_UGLY
		ENDIF
	ENDIF
ENDFUNC
*/

/// PURPOSE: Updates our dominance effect on the children
PROC UPDATE_CHAR_CREATOR_CHILD_PARENT_DOM(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData)//, BOOL bDoOppositeGender = FALSE)
	FLOAT fDominance
	
	IF IS_CHAR_CREATOR_PED_OK(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON])
		fDominance = GET_CHAR_CREATOR_PARENT_DOMINANCE(charCreatorData, CHAR_CREATOR_GENDER_MALE)
		PRINTLN("[CHARCREATOR] Update son dominance: T = ", GET_CLOUD_TIME_AS_INT(), "D = ", fDominance)
	
		SET_NEW_PARENT_BLEND_VALUES(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON], fDominance)
	ENDIF
	IF IS_CHAR_CREATOR_PED_OK(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER])
		fDominance = GET_CHAR_CREATOR_PARENT_DOMINANCE(charCreatorData, CHAR_CREATOR_GENDER_FEMALE)
		PRINTLN("[CHARCREATOR] Update daughter dominance: T = ", GET_CLOUD_TIME_AS_INT(), "D = ", fDominance)
	
		SET_NEW_PARENT_BLEND_VALUES(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER], fDominance)
	ENDIF	
	
	SET_BIT(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_UPDATE_CHILD_BLEND)
	
	/*
	// Make sure the update is in the corrent order.
	IF IS_CHAR_CREATOR_PED_MALE(charCreatorData)
	
		IF NOT bDoOppositeGender
			IF IS_CHAR_CREATOR_PED_OK(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON])
				fDominance = GET_CHAR_CREATOR_PARENT_DOMINANCE(charCreatorData, CHAR_CREATOR_GENDER_MALE)
				PRINTLN("[CHARCREATOR] Update son dominance: T = ", GET_CLOUD_TIME_AS_INT(), "D = ", fDominance)
			
				SET_NEW_PARENT_BLEND_VALUES(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON], fDominance)
			ENDIF
		ELSE
			IF IS_CHAR_CREATOR_PED_OK(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER])
				fDominance = GET_CHAR_CREATOR_PARENT_DOMINANCE(charCreatorData, CHAR_CREATOR_GENDER_FEMALE)
				PRINTLN("[CHARCREATOR] Update daughter dominance: T = ", GET_CLOUD_TIME_AS_INT(), "D = ", fDominance)
			
				SET_NEW_PARENT_BLEND_VALUES(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER], fDominance)
			ENDIF
		ENDIF
	ELSE	
		IF NOT bDoOppositeGender
			IF IS_CHAR_CREATOR_PED_OK(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER])
				fDominance = GET_CHAR_CREATOR_PARENT_DOMINANCE(charCreatorData, CHAR_CREATOR_GENDER_FEMALE)
				PRINTLN("[CHARCREATOR] Update daughter2 dominance: T = ", GET_CLOUD_TIME_AS_INT(), "D = ", fDominance)
			
				SET_NEW_PARENT_BLEND_VALUES(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER], fDominance)
			ENDIF
		ELSE
			IF IS_CHAR_CREATOR_PED_OK(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON])
				fDominance = GET_CHAR_CREATOR_PARENT_DOMINANCE(charCreatorData, CHAR_CREATOR_GENDER_MALE)
				PRINTLN("[CHARCREATOR] Update son2 dominance: T = ", GET_CLOUD_TIME_AS_INT(), "D = ", fDominance)
				
				SET_NEW_PARENT_BLEND_VALUES(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON], fDominance)
			ENDIF
		ENDIF
	ENDIF
	*/
ENDPROC


/// PURPOSE: Handles the player zooming in and out of the character ped
FUNC BOOL PROCESS_CHAR_CREATOR_ZOOM(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData, INT iIncrement)
	BOOL bAtLimit = FALSE
	
	charCreatorData.fCharZoom += (CHAR_CREATOR_ZOOM_SPEED * iIncrement)

	// Keep it within bounds
	IF charCreatorData.fCharZoom < GENERAL_CLOSEST_ZOOM
		charCreatorData.fCharZoom = GENERAL_CLOSEST_ZOOM
		bAtLimit = TRUE
	ELIF charCreatorData.fCharZoom > 1.0
		charCreatorData.fCharZoom = 1.0
		bAtLimit = TRUE
	ENDIF
	
	IF IS_BIT_SET(charCreatorData.iCharCreatorBitset, CHAR_CREATOR_FORCE_CREW_SHIRT_ZOOM)
		IF charCreatorData.crewTShirtOption <> CREW_TSHIRT_OFF
			IF iIncrement < 1
				IF charCreatorData.fCharZoom < CREW_SHIRT_ZOOM
					charCreatorData.fCharZoom = CREW_SHIRT_ZOOM
					bAtLimit = TRUE
				ENDIF	
			ENDIF
		ENDIF
	ENDIF
	
	/*
	IF charCreatorData.mainMenuIndex = LIFESTYLE_CAT
		IF iIncrement < 1
			IF charCreatorData.fCharZoom < LIFESTYLE_FURTHEST_ZOOM	
				charCreatorData.fCharZoom = LIFESTYLE_FURTHEST_ZOOM
				bAtLimit = TRUE
			ENDIF
		ENDIF
	ENDIF
	*/
	
	FLOAT fModifiedZoom = charCreatorData.fCharZoom
	IF NOT IS_CHAR_CREATOR_PED_MALE(charCreatorData)
		fModifiedZoom += 0.015
	ENDIF
	
	//printstring("zoom = ") printfloat(charCreatorData.fCharZoom) PRINTNL()
	
	// Calculate offset in UI3D preset
	charCreatorData.vCharOffset.x = CHAR_CREATOR_ZOOM_MAX_X + ((CHAR_CREATOR_ZOOM_MIN_X - CHAR_CREATOR_ZOOM_MAX_X) * fModifiedZoom)
	charCreatorData.vCharOffset.y = CHAR_CREATOR_ZOOM_MAX_Y + ((CHAR_CREATOR_ZOOM_MIN_Y - CHAR_CREATOR_ZOOM_MAX_Y) * fModifiedZoom)
	charCreatorData.vCharOffset.z = CHAR_CREATOR_ZOOM_MAX_Z + ((CHAR_CREATOR_ZOOM_MIN_Z - CHAR_CREATOR_ZOOM_MAX_Z) * fModifiedZoom)
	
	RETURN NOT bAtLimit
ENDFUNC


/// PURPOSE: Changes the Gender of the current character
PROC PROCESS_CHAR_CREATOR_GENDER_CHANGE(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData)
	
	PED_INDEX mPed
	
	IF IS_CHAR_CREATOR_PED_MALE(charCreatorData)
		SET_CHAR_CREATOR_GENDER(charCreatorData, CHAR_CREATOR_GENDER_FEMALE)
		mPed = charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER] 
	ELSE
		SET_CHAR_CREATOR_GENDER(charCreatorData, CHAR_CREATOR_GENDER_MALE)
		 mPed = charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON]
	ENDIF
	SET_CHAR_CREATOR_RANDOM_PARENT_DOM(charCreatorData, FALSE)
	
	PROCESS_CHAR_CREATOR_ZOOM(charCreatorData, 0)
	
	UPDATE_CHAR_CREATOR_CHILD_PARENT_DOM(charCreatorData)
	
	// re-apply the tshirt decals
	PED_COMP_NAME_ENUM eCurrentJbib
	eCurrentJbib = GET_PED_COMPONENT_ITEM_CURRENT_FROM_LOOKUP(mPed, COMP_TYPE_JBIB)
	EQUIP_TORSO_DECALS_MP(mPed, COMP_TYPE_JBIB, eCurrentJbib, FALSE)	
	
	// crew T
	IF charCreatorData.crewDecalOption = CREW_DECAL_BACK
		IF NOT IS_CHAR_CREATOR_PED_MALE(charCreatorData)
			charCreatorData.crewDecalOption = CREW_DECAL_NONE
		ENDIF
	ENDIF
	
	// hair
	IF IS_CHAR_CREATOR_PED_MALE(charCreatorData)
		GIVE_CHAR_CREATOR_PED_HAIR(charCreatorData, CHAR_CREATOR_PED_SON, TRUE)
	ELSE
		GIVE_CHAR_CREATOR_PED_HAIR(charCreatorData, CHAR_CREATOR_PED_DAUGHTER, TRUE)
	ENDIF	
	APPLY_CREW_TSHIRT_DECALS_TO_CURRENT_PED(charCreatorData)
	

	
	//UPDATE_TATOOS_MP(mPed)
ENDPROC

CONST_FLOAT GRANDPARENT_DOM_MIN 0.25
CONST_FLOAT GRANDPARENT_DOM_MAX 0.75

PROC CONVERT_BASE_GRANDPARENT_BLEND_TO_CAPPED(FLOAT &fBaseBlend)
	FLOAT fDomMin = GRANDPARENT_DOM_MIN
	FLOAT fDomMax = GRANDPARENT_DOM_MAX
	
	//PRINTLN("[CHARCREATOR] GET_CHAR_CREATOR_PARENT_DOMINANCE - Dom is: ", (fDomMin + ((fDomMax - fDomMin) * (charCreatorData.pedData.fParentDominance * 0.01))))
	fBaseBlend = (fDomMin * 100) + ((fDomMax - fDomMin) * (fBaseBlend))
ENDPROC

PROC CONVERT_CAPPED_GRANDPARENT_BLEND(FLOAT &fCappedBlend)
	FLOAT fDomMin = GRANDPARENT_DOM_MIN * 100
	FLOAT fDomMax = GRANDPARENT_DOM_MAX * 100

	fCappedBlend = ((fCappedBlend - fDomMin) / (fDomMax - fDomMin)) * 100
ENDPROC

/// PURPOSE: Triggers an update to the parents heads and make child update
PROC PROCESS_CHAR_CREATOR_PARENT_UPDATE(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData, CHAR_CREATOR_PED aPed, BOOL bUpdateFeatures = TRUE, BOOL bRequireBlendToBeFinished = FALSE)
	
	INT iHeadValue1
	INT iHeadValue2
	
	INT iHeadVar1
	INT iHeadVar2
	
	IF aPed = CHAR_CREATOR_PED_MUM
		iHeadVar1 = charCreatorData.pedData.charCreatorParentInfo.iMumHeadVariation
	ELSE
		iHeadVar1 = charCreatorData.pedData.charCreatorParentInfo.iDadHeadVariation
	ENDIF
	
	IF aPed = CHAR_CREATOR_PED_MUM
		iHeadVar2 = charCreatorData.pedData.charCreatorParentInfo.iMumHeadVariation2
	ELSE
		iHeadVar2 = charCreatorData.pedData.charCreatorParentInfo.iDadHeadVariation2
	ENDIF	
	
	GET_CHAR_CREATOR_HEADS_FOR_RACE(charCreatorData, aPed, iHeadValue1, iHeadVar1, CHAR_CREATOR_RACE_INDEX_ONE)
	GET_CHAR_CREATOR_HEADS_FOR_RACE(charCreatorData, aPed, iHeadValue2, iHeadVar2, CHAR_CREATOR_RACE_INDEX_TWO)	
	
	PRINTLN("[CHARCREATOR] PROCESS_CHAR_CREATOR_PARENT_UPDATE creating blend on parent: iHeadValue1 = ", iHeadValue1, " , iHeadValue2 = ", iHeadValue2, " Ugly %: ", charCreatorData.pedData.fParentUgly)
	
	IF DOES_ENTITY_EXIST(charCreatorData.charCreatorPeds[aPed])
	AND NOT IS_ENTITY_DEAD(charCreatorData.charCreatorPeds[aPed])
		IF (HAS_CHAR_CREATOR_PED_BLEND_FINISHED(charCreatorData.charCreatorPeds[aPed])
		AND HAS_CHAR_CREATOR_PED_BLEND_FINISHED(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON])
		AND HAS_CHAR_CREATOR_PED_BLEND_FINISHED(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER])
		AND NOT IS_BIT_SET(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_UPDATE_CHILD_BLEND))
		OR NOT bRequireBlendToBeFinished	
			FLOAT headBlend = 0.5
			FLOAT texBlend = 0.5
			
			FLOAT fBaseBlend
			IF aPed = CHAR_CREATOR_PED_MUM
				fBaseBlend = charCreatorData.pedData.fMumDominance
				CONVERT_BASE_GRANDPARENT_BLEND_TO_CAPPED(fBaseBlend)
			
				headBlend = fBaseBlend / 100 // fMumDominance
				texBlend = fBaseBlend / 100 // fMumDominance
				CLEAR_BIT(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_UPDATE_MUM_BLEND)
			ELIF aPed = CHAR_CREATOR_PED_DAD
				fBaseBlend = charCreatorData.pedData.fDadDominance
				CONVERT_BASE_GRANDPARENT_BLEND_TO_CAPPED(fBaseBlend)
				
				headBlend = fBaseBlend / 100 // fDadDominance
				texBlend = fBaseBlend / 100 // fDadDominance
				CLEAR_BIT(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_UPDATE_DAD_BLEND)
			ENDIF
			
			IF NOT bRequireBlendToBeFinished
				PRINTLN("[CHARCREATOR X] PROCESS_CHAR_CREATOR_PARENT_UPDATE - calling SET_PED_HEAD_BLEND_DATA for ped: ", NATIVE_TO_INT(charCreatorData.charCreatorPeds[aPed]), ": iHeadValue1 = ", iHeadValue1, " , iHeadValue2 = ", iHeadValue2, " headBlend: ", headBlend)
				SET_PED_HEAD_BLEND_DATA(charCreatorData.charCreatorPeds[aPed], iHeadValue1, iHeadValue2, 0, iHeadValue1, iHeadValue2, 0, headBlend, texBlend, 0.0, TRUE)
			ELSE
				PRINTLN("[CHARCREATOR X] PROCESS_CHAR_CREATOR_PARENT_UPDATE - calling UPDATE_PED_HEAD_BLEND_DATA for ped: ", NATIVE_TO_INT(charCreatorData.charCreatorPeds[aPed]), ", headBlend: ", headBlend)
				UPDATE_PED_HEAD_BLEND_DATA(charCreatorData.charCreatorPeds[aPed], headBlend, texBlend, 0)
			ENDIF
			
			UPDATE_CHAR_CREATOR_CHILD_PARENT_DOM(charCreatorData)
			
			IF bUpdateFeatures
				IF NOT IS_CHAR_CREATOR_PARENT_SPECIAL_HEAD(GET_CHAR_CREATOR_PARENT_RACE(charCreatorData, aPed, CHAR_CREATOR_RACE_INDEX_ONE))
					
					IF aPed = CHAR_CREATOR_PED_DAD
						//SET_PED_COMP_ITEM_CURRENT_MP(charCreatorData.charCreatorPeds[aPed], COMP_TYPE_HAIR, INT_TO_ENUM(PED_COMP_NAME_ENUM, charCreatorData.pedData.charCreatorParentInfo.iDadHair), FALSE, DEFAULT, DEFAULT, DEFAULT, TRUE) // True here to skip hair overlays for parents
						SET_PED_COMPONENT_VARIATION(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAD], PED_COMP_HAIR, charCreatorData.pedData.charCreatorParentInfo.iDadHair, charCreatorData.pedData.charCreatorParentInfo.iDadHairColour)	
					ELSE
						//SET_PED_COMP_ITEM_CURRENT_MP(charCreatorData.charCreatorPeds[aPed], COMP_TYPE_HAIR, INT_TO_ENUM(PED_COMP_NAME_ENUM, charCreatorData.pedData.charCreatorParentInfo.iMumHair), FALSE, DEFAULT, DEFAULT, DEFAULT, TRUE) // True here to skip hair overlays for parents
						printstring("setting mum with head ") PRINTINT(charCreatorData.pedData.charCreatorParentInfo.iMumHair) PRINTSTRING(" ") PRINTINT(charCreatorData.pedData.charCreatorParentInfo.iMumHairColour) PRINTNL()
						SET_PED_COMPONENT_VARIATION(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_MUM], PED_COMP_HAIR, charCreatorData.pedData.charCreatorParentInfo.iMumHair, charCreatorData.pedData.charCreatorParentInfo.iMumHairColour)	
					ENDIF
				
					SET_PED_HEAD_OVERLAY(charCreatorData.charCreatorPeds[aPed], HOS_AGING, GET_RANDOM_INT_IN_RANGE(0, GET_PED_HEAD_OVERLAY_NUM(ENUM_TO_INT(CHAR_CREATOR_WRINKLES))), 1.0)
					SET_PED_HEAD_OVERLAY(charCreatorData.charCreatorPeds[aPed], HOS_BLEMISHES, GET_RANDOM_INT_IN_RANGE(0, GET_PED_HEAD_OVERLAY_NUM(ENUM_TO_INT(CHAR_CREATOR_BLEMISH))), 1.0)
					SET_PED_HEAD_OVERLAY(charCreatorData.charCreatorPeds[aPed], HOS_BASE_DETAIL, GET_RANDOM_INT_IN_RANGE(0, GET_PED_HEAD_OVERLAY_NUM(ENUM_TO_INT(CHAR_CREATOR_BASE_BLEMISH))), 1.0)

					//IF NOT HAS_ENTERED_CHAR_CREATION_FROM_ALTERATION_PROMPT()
						INT iEyebrow
						INT iBeard 
						INT iMakeup
						IF GET_CHAR_CREATOR_GENDER_OF_PED(aPed) = CHAR_CREATOR_GENDER_MALE
							iBeard = charCreatorData.pedData.charCreatorParentInfo.iDadBeard
							
							//iEyebrow = (charCreatorData.iDadOption % 9) + 12
							
							iEyebrow = GET_RANDOM_INT_IN_RANGE(12, 21)	// don't allow number 21 monobrow
						
							SET_PED_HEAD_OVERLAY(charCreatorData.charCreatorPeds[aPed], HOS_FACIAL_HAIR, iBeard, 1.0)
							SET_PED_HEAD_OVERLAY(charCreatorData.charCreatorPeds[aPed], HOS_EYEBROW, iEyebrow, 1.0)
							
							SET_MP_INT_CHARACTER_STAT(MP_STAT_HEADBLEND_DAD_EYEB, iEyebrow, charCreatorData.iCharacterSelectSlot)
							SET_MP_INT_CHARACTER_STAT(MP_STAT_HEADBLEND_DAD_BEARD, iBeard, charCreatorData.iCharacterSelectSlot)
						ELSE
							//iEyebrow = (charCreatorData.iDadOption % 11) + 1
							iMakeup = charCreatorData.pedData.charCreatorParentInfo.iMumMakeup
							
							iEyebrow = GET_RANDOM_INT_IN_RANGE(0, 12)
							
							SET_PED_HEAD_OVERLAY(charCreatorData.charCreatorPeds[aPed], HOS_MAKEUP, iMakeup, 1.0)	
							SET_PED_HEAD_OVERLAY(charCreatorData.charCreatorPeds[aPed], HOS_EYEBROW, iEyebrow, 0.0)
							
							SET_MP_INT_CHARACTER_STAT(MP_STAT_HEADBLEND_MUM_MAKEUP, iMakeup, charCreatorData.iCharacterSelectSlot)
							SET_MP_INT_CHARACTER_STAT(MP_STAT_HEADBLEND_MUM_EYEB, iEyebrow, charCreatorData.iCharacterSelectSlot)
						ENDIF
					//ENDIF
				ENDIF
			ENDIF
		ELSE
			IF aPed = CHAR_CREATOR_PED_MUM
				SET_BIT(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_UPDATE_MUM_BLEND)
			ELIF aPed = CHAR_CREATOR_PED_DAD
				SET_BIT(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_UPDATE_DAD_BLEND)
			ENDIF
		ENDIF
	ENDIF
	
	PRINTLN("[CHARCREATOR] PROCESS_CHAR_CREATOR_PARENT_UPDATE - SET_BIT(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_UPDATE_CHILD_BLEND)")
	IF NOT IS_BIT_SET(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_UPDATE_MUM_BLEND)
	AND NOT IS_BIT_SET(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_UPDATE_DAD_BLEND)
		SET_BIT(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_UPDATE_CHILD_BLEND)
	ENDIF
	
ENDPROC


/// PURPOSE: Loops over all special heads to see if they are valid 
FUNC BOOL GET_NEXT_CHAR_CREATOR_SPECIAL_HEAD(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData, CHAR_CREATOR_PED aPed, INT iIncrement)

	INT iNumberOfHeads
	INT iCurrentHeadIndex
	
	// Get how many heads to loop to
	IF aPed = CHAR_CREATOR_PED_DAD
		iNumberOfHeads = GET_PED_HEAD_BLEND_NUM_HEADS(HBHT_UNIQUE_MALE)
	ELSE
		iNumberOfHeads = GET_PED_HEAD_BLEND_NUM_HEADS(HBHT_UNIQUE_FEMALE)
	ENDIF
	
	PRINTLN("[CHARCREATOR] GET_NEXT_CHAR_CREATOR_SPECIAL_HEAD - iNumberofHeads = ", iNumberOfHeads)
	
	// Get our current index
	iCurrentHeadIndex = GET_CHAR_CREATOR_PARENT_SPECIAL_HEAD_INDEX(charCreatorData, aPed)
		
	PRINTLN("[CHARCREATOR] GET_NEXT_CHAR_CREATOR_SPECIAL_HEAD - iCurrentHeadIndex = ", iCurrentHeadIndex)
		
	BOOL bFound
	WHILE (NOT bFound)
	
		iCurrentHeadIndex += iIncrement
		
		IF iCurrentHeadIndex < 0
			iCurrentHeadIndex = iNumberOfHeads - 1
		ELIF iCurrentHeadIndex >= iNumberOfHeads
			iCurrentHeadIndex = 0
		ENDIF
		
		IF IS_CHAR_CREATOR_SPECIAL_HEAD_VALID(iCurrentHeadIndex, aPed)
			bFound = TRUE
			
			SET_CHAR_CREATOR_PARENT_SPECIAL_HEAD(charCreatorData, aPed, iCurrentHeadIndex)
			RETURN TRUE
		ENDIF
	ENDWHILE

	RETURN FALSE
ENDFUNC


FUNC CHAR_CREATOR_RACE GET_RACE_FROM_GRANDPARENT(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData, CHAR_CREATOR_PED aPed, INT iRaceIndex)
	IF aPed = CHAR_CREATOR_PED_DAD
		IF iRaceIndex = CHAR_CREATOR_RACE_INDEX_ONE
			SWITCH charCreatorData.iHeadForDadRace
				CASE 0
					RETURN CHAR_CREATOR_WHITE
				BREAK
				CASE 1
					RETURN CHAR_CREATOR_BLACK
				BREAK
				CASE 2
					RETURN CHAR_CREATOR_LATIN
				BREAK
				CASE 3
					RETURN CHAR_CREATOR_CHINESE
				BREAK
				CASE 4
					RETURN CHAR_CREATOR_INDIAN
				BREAK
				CASE 5
					RETURN CHAR_CREATOR_POLYNESIAN
				BREAK
				CASE 6
					RETURN CHAR_CREATOR_WHITE
				BREAK
				CASE 7
					RETURN CHAR_CREATOR_BLACK
				BREAK
				CASE 8
					RETURN CHAR_CREATOR_LATIN
				BREAK
				CASE 9
					RETURN CHAR_CREATOR_CHINESE
				BREAK
				CASE 10
					RETURN CHAR_CREATOR_INDIAN
				BREAK
				CASE 11
					RETURN CHAR_CREATOR_POLYNESIAN
				BREAK
				CASE 12
					RETURN CHAR_CREATOR_WHITE
				BREAK
				CASE 13
					RETURN CHAR_CREATOR_CHINESE
				BREAK
			ENDSWITCH
		ELSE
			SWITCH charCreatorData.iHeadForDadRace2
				CASE 0
					RETURN CHAR_CREATOR_WHITE
				BREAK
				CASE 1
					RETURN CHAR_CREATOR_BLACK
				BREAK
				CASE 2
					RETURN CHAR_CREATOR_LATIN
				BREAK
				CASE 3
					RETURN CHAR_CREATOR_CHINESE
				BREAK
				CASE 4
					RETURN CHAR_CREATOR_INDIAN
				BREAK
				CASE 5
					RETURN CHAR_CREATOR_POLYNESIAN
				BREAK
				CASE 6
					RETURN CHAR_CREATOR_WHITE
				BREAK
				CASE 7
					RETURN CHAR_CREATOR_BLACK
				BREAK
				CASE 8
					RETURN CHAR_CREATOR_LATIN
				BREAK
				CASE 9
					RETURN CHAR_CREATOR_CHINESE
				BREAK
				CASE 10
					RETURN CHAR_CREATOR_INDIAN
				BREAK
				CASE 11
					RETURN CHAR_CREATOR_POLYNESIAN
				BREAK
				CASE 12
					RETURN CHAR_CREATOR_WHITE
				BREAK
				CASE 13
					RETURN CHAR_CREATOR_CHINESE
				BREAK
			ENDSWITCH
		ENDIF
	ELIF aPed = CHAR_CREATOR_PED_MUM
		IF iRaceIndex = CHAR_CREATOR_RACE_INDEX_ONE
			SWITCH charCreatorData.iHeadForMumRace
				CASE 0
					RETURN CHAR_CREATOR_WHITE
				BREAK
				CASE 1
					RETURN CHAR_CREATOR_BLACK
				BREAK
				CASE 2
					RETURN CHAR_CREATOR_LATIN
				BREAK
				CASE 3
					RETURN CHAR_CREATOR_CHINESE
				BREAK
				CASE 4
					RETURN CHAR_CREATOR_INDIAN
				BREAK
				CASE 5
					RETURN CHAR_CREATOR_POLYNESIAN
				BREAK
				CASE 6
					RETURN CHAR_CREATOR_WHITE
				BREAK
				CASE 7
					RETURN CHAR_CREATOR_BLACK
				BREAK
				CASE 8
					RETURN CHAR_CREATOR_LATIN
				BREAK
				CASE 9
					RETURN CHAR_CREATOR_CHINESE
				BREAK
				CASE 10
					RETURN CHAR_CREATOR_INDIAN
				BREAK
				CASE 11
					RETURN CHAR_CREATOR_POLYNESIAN
				BREAK
				CASE 12
					RETURN CHAR_CREATOR_LATIN
				BREAK
				CASE 13
					RETURN CHAR_CREATOR_BLACK
				BREAK
			ENDSWITCH
		ELSE
			SWITCH charCreatorData.iHeadForMumRace2
				CASE 0
					RETURN CHAR_CREATOR_WHITE
				BREAK
				CASE 1
					RETURN CHAR_CREATOR_BLACK
				BREAK
				CASE 2
					RETURN CHAR_CREATOR_LATIN
				BREAK
				CASE 3
					RETURN CHAR_CREATOR_CHINESE
				BREAK
				CASE 4
					RETURN CHAR_CREATOR_INDIAN
				BREAK
				CASE 5
					RETURN CHAR_CREATOR_POLYNESIAN
				BREAK
				CASE 6
					RETURN CHAR_CREATOR_WHITE
				BREAK
				CASE 7
					RETURN CHAR_CREATOR_BLACK
				BREAK
				CASE 8
					RETURN CHAR_CREATOR_LATIN
				BREAK
				CASE 9
					RETURN CHAR_CREATOR_CHINESE
				BREAK
				CASE 10
					RETURN CHAR_CREATOR_INDIAN
				BREAK
				CASE 11
					RETURN CHAR_CREATOR_POLYNESIAN
				BREAK
				CASE 12
					RETURN CHAR_CREATOR_LATIN
				BREAK
				CASE 13
					RETURN CHAR_CREATOR_BLACK
				BREAK
			ENDSWITCH
		ENDIF
	ENDIF
	
	RETURN CHAR_CREATOR_WHITE
ENDFUNC


FUNC INT GET_HEAD_FROM_GRANDPARENT(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData, CHAR_CREATOR_PED aPed, INT iRaceIndex)
	IF aPed = CHAR_CREATOR_PED_DAD
		IF iRaceIndex = CHAR_CREATOR_RACE_INDEX_ONE
			SWITCH charCreatorData.iHeadForDadRace
				CASE 0
				CASE 1
				CASE 2
				CASE 3
				CASE 4
				CASE 5
					RETURN MP_HEAD_PRETTY
				BREAK
				CASE 6
				CASE 7
				CASE 8
				CASE 9
				CASE 10
				CASE 11
					RETURN MP_HEAD_UGLY
				BREAK				
				CASE 12
				CASE 13
					#IF NOT	IS_JAPANESE_BUILD
						RETURN MP_HEAD_EXTRA1
					#ENDIF
					RETURN MP_HEAD_UGLY
				BREAK					
			ENDSWITCH
		ELSE
			SWITCH charCreatorData.iHeadForDadRace2
				CASE 0
				CASE 1
				CASE 2
				CASE 3
				CASE 4
				CASE 5
					RETURN MP_HEAD_PRETTY
				BREAK
				CASE 6
				CASE 7
				CASE 8
				CASE 9
				CASE 10
				CASE 11
					RETURN MP_HEAD_UGLY
				BREAK				
				CASE 12
				CASE 13
					#IF NOT	IS_JAPANESE_BUILD
						RETURN MP_HEAD_EXTRA1
					#ENDIF
					RETURN MP_HEAD_UGLY
				BREAK		
			ENDSWITCH
		ENDIF
	ELIF aPed = CHAR_CREATOR_PED_MUM
		IF iRaceIndex = CHAR_CREATOR_RACE_INDEX_ONE
			SWITCH charCreatorData.iHeadForMumRace
				CASE 0
				CASE 1
				CASE 2
				CASE 3
				CASE 4
				CASE 5
					RETURN MP_HEAD_PRETTY
				BREAK
				CASE 6
				CASE 7
				CASE 8
				CASE 9
				CASE 10
				CASE 11
					RETURN MP_HEAD_UGLY
				BREAK				
				CASE 12
				CASE 13
					#IF NOT	IS_JAPANESE_BUILD
						RETURN MP_HEAD_EXTRA1
					#ENDIF
					RETURN MP_HEAD_UGLY
				BREAK		
			ENDSWITCH
		ELSE
			SWITCH charCreatorData.iHeadForMumRace2
				CASE 0
				CASE 1
				CASE 2
				CASE 3
				CASE 4
				CASE 5
					RETURN MP_HEAD_PRETTY
				BREAK
				CASE 6
				CASE 7
				CASE 8
				CASE 9
				CASE 10
				CASE 11
					RETURN MP_HEAD_UGLY
				BREAK				
				CASE 12
				CASE 13
					#IF NOT	IS_JAPANESE_BUILD
						RETURN MP_HEAD_EXTRA1
					#ENDIF
					RETURN MP_HEAD_UGLY
				BREAK		
			ENDSWITCH
		ENDIF
	ENDIF
	
	RETURN MP_HEAD_PRETTY
ENDFUNC

FUNC INT GET_GRANDPARENT_FROM_HEAD_AND_RACE(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData, CHAR_CREATOR_PED aPed, INT iRaceIndex)
	CHAR_CREATOR_RACE thisRace = GET_CHAR_CREATOR_PARENT_RACE(charCreatorData, aPed, iRaceIndex)
	INT iThisHead
	
	IF aPed = CHAR_CREATOR_PED_DAD
		IF iRaceIndex = CHAR_CREATOR_RACE_INDEX_ONE
			iThisHead = charCreatorData.pedData.charCreatorParentInfo.iDadHeadVariation
		ELIF iRaceIndex = CHAR_CREATOR_RACE_INDEX_TWO
			iThisHead = charCreatorData.pedData.charCreatorParentInfo.iDadHeadVariation2
		ENDIF
		
		SWITCH thisRace
			CASE CHAR_CREATOR_WHITE
				SWITCH iThisHead
					CASE MP_HEAD_PRETTY
						RETURN 0
					BREAK
					CASE MP_HEAD_UGLY
						RETURN 6
					BREAK
					#IF NOT	IS_JAPANESE_BUILD
						CASE MP_HEAD_EXTRA1
							RETURN 12
						BREAK
					#ENDIF
				ENDSWITCH
			BREAK
			CASE CHAR_CREATOR_BLACK
				SWITCH iThisHead
					CASE MP_HEAD_PRETTY
						RETURN 1
					BREAK
					CASE MP_HEAD_UGLY
						RETURN 7
					BREAK
				ENDSWITCH
			BREAK
			CASE CHAR_CREATOR_LATIN
				SWITCH iThisHead
					CASE MP_HEAD_PRETTY
						RETURN 2
					BREAK
					CASE MP_HEAD_UGLY
						RETURN 8
					BREAK
				ENDSWITCH
			BREAK
			CASE CHAR_CREATOR_CHINESE
				SWITCH iThisHead
					CASE MP_HEAD_PRETTY
						RETURN 3
					BREAK
					CASE MP_HEAD_UGLY
						RETURN 9
					BREAK
					#IF NOT	IS_JAPANESE_BUILD
						CASE MP_HEAD_EXTRA1
							RETURN 13
						BREAK
					#ENDIF
				ENDSWITCH
			BREAK
			CASE CHAR_CREATOR_INDIAN
				SWITCH iThisHead
					CASE MP_HEAD_PRETTY
						RETURN 4
					BREAK
					CASE MP_HEAD_UGLY
						RETURN 10
					BREAK
				ENDSWITCH
			BREAK
			CASE CHAR_CREATOR_POLYNESIAN
				SWITCH iThisHead
					CASE MP_HEAD_PRETTY
						RETURN 5
					BREAK
					CASE MP_HEAD_UGLY
						RETURN 11
					BREAK
				ENDSWITCH
			BREAK
		ENDSWITCH
	ELSE
		IF iRaceIndex = CHAR_CREATOR_RACE_INDEX_ONE
			iThisHead = charCreatorData.pedData.charCreatorParentInfo.iMumHeadVariation
		ELIF iRaceIndex = CHAR_CREATOR_RACE_INDEX_TWO
			iThisHead = charCreatorData.pedData.charCreatorParentInfo.iMumHeadVariation2
		ENDIF
		
		SWITCH thisRace
			CASE CHAR_CREATOR_WHITE
				SWITCH iThisHead
					CASE MP_HEAD_PRETTY
						RETURN 0
					BREAK
					CASE MP_HEAD_UGLY
						RETURN 6
					BREAK
				ENDSWITCH
			BREAK
			CASE CHAR_CREATOR_BLACK
				SWITCH iThisHead
					CASE MP_HEAD_PRETTY
						RETURN 1
					BREAK
					CASE MP_HEAD_UGLY
						RETURN 7
					BREAK
					#IF NOT	IS_JAPANESE_BUILD
						CASE MP_HEAD_EXTRA1
							RETURN 13
						BREAK
					#ENDIF
				ENDSWITCH
			BREAK
			CASE CHAR_CREATOR_LATIN
				SWITCH iThisHead
					CASE MP_HEAD_PRETTY
						RETURN 2
					BREAK
					CASE MP_HEAD_UGLY
						RETURN 8
					BREAK
					#IF NOT	IS_JAPANESE_BUILD
						CASE MP_HEAD_EXTRA1
							RETURN 12
						BREAK
					#ENDIF
				ENDSWITCH
			BREAK
			CASE CHAR_CREATOR_CHINESE
				SWITCH iThisHead
					CASE MP_HEAD_PRETTY
						RETURN 3
					BREAK
					CASE MP_HEAD_UGLY
						RETURN 9
					BREAK
				ENDSWITCH
			BREAK
			CASE CHAR_CREATOR_INDIAN
				SWITCH iThisHead
					CASE MP_HEAD_PRETTY
						RETURN 4
					BREAK
					CASE MP_HEAD_UGLY
						RETURN 10
					BREAK
				ENDSWITCH
			BREAK
			CASE CHAR_CREATOR_POLYNESIAN
				SWITCH iThisHead
					CASE MP_HEAD_PRETTY
						RETURN 5
					BREAK
					CASE MP_HEAD_UGLY
						RETURN 11
					BREAK
				ENDSWITCH
			BREAK
		ENDSWITCH
	ENDIF
	
	RETURN 0
ENDFUNC


//// PURPOSE: Change the parent RACE and update our blends including child
PROC PROCESS_CHANGE_PARENT_RACE(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData, CHAR_CREATOR_PED aPed, INT iIncrement, INT iRaceIndex)

	BOOL bFound

	INT iNewRace = ENUM_TO_INT(GET_CHAR_CREATOR_PARENT_RACE(charCreatorData, aPed, iRaceIndex))
	
	IF aPed = CHAR_CREATOR_PED_DAD
		IF charCreatorData.heritageIndex <> charCreatorData.cachedDadHeritageIndex
			IF charCreatorData.cachedDadHeritageIndex <> HERITAGE_GENDER
				CLEAR_CACHED_PARENTS(charCreatorData, TRUE)
			ENDIF
			charCreatorData.cachedDadHeritageIndex = charCreatorData.heritageIndex
		ENDIF	
	ELSE
		IF charCreatorData.heritageIndex <> charCreatorData.cachedMumHeritageIndex
			IF charCreatorData.cachedMumHeritageIndex <> HERITAGE_GENDER
				CLEAR_CACHED_PARENTS(charCreatorData, FALSE)
			ENDIF
			charCreatorData.cachedMumHeritageIndex = charCreatorData.heritageIndex
		ENDIF	
	ENDIF
	
	PRINTLN("[CHARCREATOR] PROCESS_CHANGE_PARENT_RACE - Race to begin: ", iNewRace)

	WHILE (NOT bFound)
	
		// Are we currently in the special race
		IF iNewRace = ENUM_TO_INT(CHAR_CREATOR_SPECIAL_RACE)
			
			PRINTLN("[CHARCREATOR] PROCESS_CHANGE_PARENT_RACE - Race is SPECIAL")
			
			// Can we move to another special head
			IF iRaceIndex = CHAR_CREATOR_RACE_INDEX_ONE
			AND GET_NEXT_CHAR_CREATOR_SPECIAL_HEAD(charCreatorData, aPed, iIncrement)

			
			
				PRINTLN("[CHARCREATOR] PROCESS_CHANGE_PARENT_RACE - Found next special head index")
			
				//...yes, we are finished
				iNewRace = ENUM_TO_INT(CHAR_CREATOR_SPECIAL_RACE)

				bFound = TRUE
			ELSE
			/*
			ELSE
				PRINTLN("[CHARCREATOR] PROCESS_CHANGE_PARENT_RACE - Loop to normal races")
			
				IF iIncrement > 0
					iNewRace = 0
				ELSE
					iNewRace = ENUM_TO_INT(CHAR_CREATOR_MAX_RACES)-1
				ENDIF
				
				bFound = TRUE
			*/
			ENDIF
		ELSE
			INT iCurrent
			INT iMaxRace
			IF aPed = CHAR_CREATOR_PED_DAD
				IF iRaceIndex = CHAR_CREATOR_RACE_INDEX_ONE
					iCurrent = charCreatorData.iHeadForDadRace
				ELSE
					iCurrent = charCreatorData.iHeadForDadRace2
				ENDIF
				iMaxRace = MAX_GRANDFATHER
			ELIF aPed = CHAR_CREATOR_PED_MUM
				IF iRaceIndex = CHAR_CREATOR_RACE_INDEX_ONE
					iCurrent = charCreatorData.iHeadForMumRace
				ELSE
					iCurrent = charCreatorData.iHeadForMumRace2
				ENDIF
				iMaxRace = MAX_GRANDMOTHER
			ENDIF
			
			iCurrent += iIncrement
			
			IF iCurrent >= iMaxRace
				iCurrent = 0
			ELIF iCurrent < 0
				iCurrent = iMaxRace - 1
			ENDIF
			
			IF aPed = CHAR_CREATOR_PED_DAD
				IF iRaceIndex = CHAR_CREATOR_RACE_INDEX_ONE
					charCreatorData.iHeadForDadRace = iCurrent 
				ELSE
					 charCreatorData.iHeadForDadRace2 = iCurrent
				ENDIF
			ELIF aPed = CHAR_CREATOR_PED_MUM
				IF iRaceIndex = CHAR_CREATOR_RACE_INDEX_ONE
					charCreatorData.iHeadForMumRace = iCurrent 
				ELSE
					charCreatorData.iHeadForMumRace2 = iCurrent 
				ENDIF
			ENDIF			
			
			PRINTLN("[CHARCREATOR] PROCESS_CHANGE_PARENT_RACE - Next race is: ", iNewRace)
			
			// If we have moved off the standard parent races, loop to special to see if player is entitled to unique head
			IF iNewRace < 0
			
				iNewRace = ENUM_TO_INT(CHAR_CREATOR_SPECIAL_RACE)
				SET_CHAR_CREATOR_PARENT_SPECIAL_HEAD(charCreatorData, aPed, GET_CHAR_CREATOR_SPECIAL_HEAD_MAX(aPed))
				
				PRINTLN("[CHARCREATOR] PROCESS_CHANGE_PARENT_RACE - Go to SPECIAL with index: ", GET_CHAR_CREATOR_PARENT_SPECIAL_HEAD_INDEX(charCreatorData, aPed))
				
			ELIF iNewRace >= ENUM_TO_INT(CHAR_CREATOR_MAX_RACES)
				iNewRace = ENUM_TO_INT(CHAR_CREATOR_SPECIAL_RACE)
				SET_CHAR_CREATOR_PARENT_SPECIAL_HEAD(charCreatorData, aPed, -1)				
				
				PRINTLN("[CHARCREATOR] PROCESS_CHANGE_PARENT_RACE - Go to SPECIAL with index: -1")
			ELSE
				bFound = TRUE
			ENDIF
		ENDIF
	ENDWHILE
	

	
	// set ugliness of new race
	IF iNewRace <> ENUM_TO_INT(CHAR_CREATOR_SPECIAL_RACE)
		CHAR_CREATOR_RACE changeRace = GET_RACE_FROM_GRANDPARENT(charCreatorData, aPed, iRaceIndex)
		INT iChangeHead = GET_HEAD_FROM_GRANDPARENT(charCreatorData, aPed, iRaceIndex)
		// Set the new race of the parent
		SET_CHAR_CREATOR_PARENT_RACE(charCreatorData, aPed, changeRace, iRaceIndex)	
	
		IF aPed = CHAR_CREATOR_PED_DAD
			IF iRaceIndex = CHAR_CREATOR_RACE_INDEX_ONE
				charCreatorData.pedData.charCreatorParentInfo.iDadHeadVariation = iChangeHead
			ELSE
				charCreatorData.pedData.charCreatorParentInfo.iDadHeadVariation2 = iChangeHead
			ENDIF
		ELSE
			IF iRaceIndex = CHAR_CREATOR_RACE_INDEX_ONE
				charCreatorData.pedData.charCreatorParentInfo.iMumHeadVariation = iChangeHead
			ELSE
				charCreatorData.pedData.charCreatorParentInfo.iMumHeadVariation2 = iChangeHead
			ENDIF		
		ENDIF	
	ENDIF
	
	#IF IS_DEBUG_BUILD
	TEXT_LABEL_15 tlRaceString = GET_CHAR_CREATOR_RACE_STRING(charCreatorData, aPed, iRaceIndex)
	PRINTLN("[CHARCREATOR] PROCESS_CHANGE_PARENT_RACE - Setting new race to: ", tlRaceString)
	#ENDIF
	
	UPDATE_GRANDAD_HEAD(charCreatorData, TRUE)
	UPDATE_GRANDMA_HEAD(charCreatorData, TRUE)
	UPDATE_GRANDAD_HEAD(charCreatorData, FALSE)
	UPDATE_GRANDMA_HEAD(charCreatorData, FALSE)
	
	SET_BIT(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_FORCE_HERITAGE_UPDATE)
	
 	UPDATE_CHAR_CREATOR_CHILD_PARENT_DOM(charCreatorData)

	PROCESS_CHAR_CREATOR_PARENT_UPDATE(charCreatorData, aPed)


ENDPROC



/* **********************************************
*				LOADING OF PEDS
*************************************************/

/// PURPOSE: Displays spinner when ped heads are loading
PROC SET_CHAR_CREATOR_CHILD_AS_BUSY(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData, BOOL bShow = TRUE)

	PRINTLN("[CHARCREATOR] SET_CHAR_CREATOR_CHILD_AS_BUSY - called, bShow ", PICK_STRING(bShow, "TRUE", "FALSE"))
	PAUSE_MENU_SET_BUSY_SPINNER(bShow, 9, 1)
	
	IF bShow
		SET_BIT(charCreatorData.iCharCreatorBitset, CHAR_CREATOR_CHILD_SPINNER_DISPLAYED)
	ELSE
		CLEAR_BIT(charCreatorData.iCharCreatorBitset, CHAR_CREATOR_CHILD_SPINNER_DISPLAYED)
	ENDIF	
ENDPROC

/// PURPOSE: Display busy spinner for parents
PROC SET_CHAR_CREATOR_PARENTS_AS_BUSY(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData, BOOL bShow = TRUE)
	IF (charCreatorData.mainMenuIndex = HERITAGE_CAT OR (charCreatorData.mainMenuIndex = CONFIRM_CAT AND charCreatorData.prevMainMenuIndex = HERITAGE_CAT))
	OR NOT bShow
		PRINTLN("[CHARCREATOR] SET_CHAR_CREATOR_PARENTS_AS_BUSY - called, bShow ", PICK_STRING(bShow, "TRUE", "FALSE"))
		PAUSE_MENU_SET_BUSY_SPINNER(bShow, 7, 0)
		
		IF bShow
			SET_BIT(charCreatorData.iCharCreatorBitset, CHAR_CREATOR_PARENTS_SPINNER_DISPLAYED)
		ELSE
			CLEAR_BIT(charCreatorData.iCharCreatorBitset, CHAR_CREATOR_PARENTS_SPINNER_DISPLAYED)
		ENDIF	
	ENDIF
ENDPROC

/// PURPOSE: Returns if the child is updating and we are showing the spinner
FUNC BOOL IS_CHAR_CREATOR_CHILD_SPINNER_DISPLAYED(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData)
	RETURN IS_BIT_SET(charCreatorData.iCharCreatorBitset, CHAR_CREATOR_CHILD_SPINNER_DISPLAYED)	
ENDFUNC

/// PURPOSE: Returns if the parents is updating and we are showing the spinner
FUNC BOOL IS_CHAR_CREATOR_PARENTS_SPINNER_DISPLAYED(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData)
	RETURN IS_BIT_SET(charCreatorData.iCharCreatorBitset, CHAR_CREATOR_PARENTS_SPINNER_DISPLAYED)	
ENDFUNC

/// PURPOSE: Set the game to wait to clear the loading spinners
PROC SET_WAIT_TO_CLEAR_SPINNERS(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData)
	IF NOT IS_BIT_SET(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_WAIT_TO_CLEAR_SPINNERS)
		SET_BIT(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_WAIT_TO_CLEAR_SPINNERS)
		charCreatorData.iWaitForSpinnersCounter = 0
	ENDIF
ENDPROC

/// PURPOSE: Check to see if child blends need updating
PROC MAINTAIN_CHAR_CREATOR_CHILD_BLENDS(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData)
	
	IF IS_BIT_SET(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_UPDATE_CHILD_BLEND)
		IF HAS_CHAR_CREATOR_PED_BLEND_FINISHED(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_MUM]) 
		AND HAS_CHAR_CREATOR_PED_BLEND_FINISHED(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAD])
		AND HAS_CHAR_CREATOR_PED_BLEND_FINISHED(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON])
		AND HAS_CHAR_CREATOR_PED_BLEND_FINISHED(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER])
		AND NOT IS_BIT_SET(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_DO_RANDOMISE)
			
			PRINTLN("[CHARCREATOR X] MAINTAIN_CHAR_CREATOR_CHILD_BLENDS - Parents have finished blending, UPDATE_PED_CHILD_BLEND, son: ", NATIVE_TO_INT(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON]), " and daughter: ", NATIVE_TO_INT(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER]))
			
			UPDATE_PED_CHILD_BLEND(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON])
			UPDATE_PED_CHILD_BLEND(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER])
			
			SET_WAIT_TO_CLEAR_SPINNERS(charCreatorData)
			
			PRINTLN("[CHARCREATOR] PROCESS_CHAR_CREATOR_PARENT_UPDATE - CLEAR_BIT(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_UPDATE_CHILD_BLEND)")
			CLEAR_BIT(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_UPDATE_CHILD_BLEND)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Return TRUE if all the peds have loaded and are ready
FUNC BOOL ARE_ALL_CHAR_CREATOR_PEDS_LOADED(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData)
	RETURN IS_BIT_SET(charCreatorData.iCharCreatorBitset, CHAR_CREATOR_ALL_PEDS_LOADED)
ENDFUNC

/// PURPOSE: Performs an exist and dead check on all peds
FUNC BOOL ARE_ALL_CHAR_CREATOR_PEDS_OK(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData)

	INT i
	REPEAT CHAR_CREATOR_PED_MAX i
		
		IF DOES_ENTITY_EXIST(charCreatorData.charCreatorPeds[i])
			IF IS_ENTITY_DEAD(charCreatorData.charCreatorPeds[i])
				RETURN FALSE
			ENDIF
		ELSE
			RETURN FALSE
		ENDIF
		
	ENDREPEAT

	REPEAT CHAR_CREATOR_SPECIAL_PEDS_MAX i
		
		IF DOES_ENTITY_EXIST(charCreatorData.charCreatorSpecialPeds[i])
			IF IS_ENTITY_DEAD(charCreatorData.charCreatorSpecialPeds[i])
				RETURN FALSE
			ENDIF
		ELSE
			RETURN FALSE
		ENDIF
		
	ENDREPEAT

	RETURN TRUE
ENDFUNC



PROC PROCESS_CHAR_CREATOR_CHANGE_HAIR(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData, INT iIncrement)

	INT iNewHair = GET_CHAR_CREATOR_HAIR(charCreatorData, GET_CHAR_CREATOR_GENDER(charCreatorData)) + iIncrement
	INT iMaxHair
	
	IF IS_CHAR_CREATOR_PED_MALE(charCreatorData)
		iMaxHair = CHAR_CREATOR_MAX_HAIR_M
	ELSE
		iMaxHair = CHAR_CREATOR_MAX_HAIR_F
	ENDIF
	
	INT iMinHair
	IF HAS_ENTERED_CHAR_CREATION_FROM_ALTERATION_PROMPT()
		iMinHair = 0
	ELSE
		iMinHair = 1
	ENDIF
	
	
	IF iNewhair < iMinHair
		iNewHair = (iMaxHair-1)
	ENDIF
	
	IF iNewHair >= iMaxHair
		iNewHair = iMinHair
	ENDIF
	
	PRINTLN("[CHARCREATOR] PROCESS_CHAR_CREATOR_CHANGE_HAIR - Changing haircut to: ", iNewHair)
	
	SET_CHAR_CREATOR_HAIR(charCreatorData, GET_CHAR_CREATOR_GENDER(charCreatorData), iNewHair)
		
	IF IS_CHAR_CREATOR_PED_MALE(charCreatorData)
		GIVE_CHAR_CREATOR_PED_HAIR(charCreatorData, CHAR_CREATOR_PED_SON)
	ELSE
		GIVE_CHAR_CREATOR_PED_HAIR(charCreatorData, CHAR_CREATOR_PED_DAUGHTER)
	ENDIF

ENDPROC

FUNC BOOL DOES_CHAR_CREATOR_HAIR_HAVE_COLOURS(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData)
	IF IS_CHAR_CREATOR_PED_MALE(charCreatorData)
		IF GET_CHAR_CREATOR_HAIR(charCreatorData, CHAR_CREATOR_GENDER_MALE) = 0
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC PROCESS_CHAR_CREATOR_CHANGE_HAIR_COLOUR(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData, INT iIncrement)

	INT iNewHair = GET_CHAR_CREATOR_HAIR_COLOUR(charCreatorData, GET_CHAR_CREATOR_GENDER(charCreatorData)) + iIncrement
	INT iMaxHair
	
	IF IS_CHAR_CREATOR_PED_MALE(charCreatorData)
		iMaxHair = CHAR_CREATOR_MAX_HAIR_COL_M
	ELSE
		iMaxHair = CHAR_CREATOR_MAX_HAIR_COL_F
	ENDIF
	
	IF iNewhair < 0
		iNewHair = (iMaxHair-1)
	ENDIF
	
	IF iNewHair >= iMaxHair
		iNewHair = 0
	ENDIF
	
	PRINTLN("[CHARCREATOR] PROCESS_CHAR_CREATOR_CHANGE_HAIR_COLOUR - Changing hair colour to: ", iNewHair)
	
	SET_CHAR_CREATOR_HAIR_COLOUR(charCreatorData, GET_CHAR_CREATOR_GENDER(charCreatorData), iNewHair)
		
	IF IS_CHAR_CREATOR_PED_MALE(charCreatorData)
		GIVE_CHAR_CREATOR_PED_HAIR(charCreatorData, CHAR_CREATOR_PED_SON)
	ELSE
		GIVE_CHAR_CREATOR_PED_HAIR(charCreatorData, CHAR_CREATOR_PED_DAUGHTER)
	ENDIF

ENDPROC

/// PURPOSE: Change the character's age in direction pressed
PROC PROCESS_CHAR_CREATOR_CHANGE_AGE(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData, INT iIncrement)
	
	INT iCurrentAge = GET_CHAR_CREATOR_AGE(charCreatorData) + iIncrement
	
	IF iCurrentAge < CHAR_CREATOR_MIN_AGE
		iCurrentAge = CHAR_CREATOR_MAX_AGE
	ENDIF
	
	IF iCurrentAge > CHAR_CREATOR_MAX_AGE
		iCurrentAge = CHAR_CREATOR_MIN_AGE
	ENDIF
	
	// Set our age to a new value
	SET_CHAR_CREATOR_AGE(charCreatorData, iCurrentAge)
		
	// Update our wrinkles overlay
	CALC_CHAR_CREATOR_AGE_OVERLAY(charCreatorData)
ENDPROC


FUNC BOOL IS_BEARD_DISALLOWED_IN_CREATOR(INT iBeard)
	IF iBeard = 17 // mutton chops
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC


/// PURPOSE: Change the character's beard in the direction from input
PROC PROCESS_CHAR_CREATOR_CHANGE_BEARD(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData, INT iIncrement)
	
	INT iCurrentBeard
	FLOAT fBeardBlend
	GET_CHAR_CREATOR_OVERLAY_VALUES(charCreatorData, CHAR_CREATOR_BEARD, iCurrentBeard, fBeardBlend)
	
	iCurrentBeard += iIncrement
	
	WHILE IS_BEARD_DISALLOWED_IN_CREATOR(iCurrentBeard)
	AND iCurrentBeard >= -1
	AND iCurrentBeard <= CHAR_CREATOR_MAX_BEARDS
		iCurrentBeard += iIncrement
	ENDWHILE	
	
	// Cap and wrap around our beards
	IF iCurrentBeard < -1
		iCurrentBeard = (CHAR_CREATOR_MAX_BEARDS-1)
	ENDIF
	
	IF iCurrentBeard >= CHAR_CREATOR_MAX_BEARDS
		iCurrentBeard = -1
	ENDIF
	
	charCreatorData.pedData.charOverlays[ENUM_TO_INT(CHAR_CREATOR_BEARD)].iOverlayVal 		= iCurrentBeard
	charCreatorData.pedData.charOverlays[ENUM_TO_INT(CHAR_CREATOR_BEARD)].fOverlayBlend 	= 1.0
	
	CHAR_CREATOR_LIFESTYLE maxLifestyle = GET_MAX_LIFESTYLE(charCreatorData)
	INT iLockLifestyleSlot = ENUM_TO_INT(maxLifestyle) - 1
	charCreatorData.iCacheMaleBeard[iLockLifestyleSlot] = iCurrentBeard		
	
	PRINTLN("[CHARCREATOR] PROCESS_CHAR_CREATOR_CHANGE_BEARD - iNewBeard: ", iCurrentBeard)
	
	APPLY_CHAR_CREATOR_BEARD_OVERLAY(charCreatorData)
ENDPROC

/*
FUNC BOOL IS_MAKEUP_DISALLOWED_IN_CREATOR(INT iMakeup)
	IF iMakeup = 2 // gothic 
	OR iMakeup = 3 // rocker 
	OR iMakeup = 6 // trailer park princess
	OR iMakeup = 9 // seriously cerise
	OR iMakeup = 10 // disco disaster
	OR iMakeup = 11 // beauty spot
	OR iMakeup = 12 // toned down
	OR iMakeup = 13 // cyan swiped
	OR iMakeup = 14 // morning after
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC
*/

/// PURPOSE: Change the character's beard in the direction from input
PROC PROCESS_CHAR_CREATOR_CHANGE_MAKEUP(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData, INT iIncrement)
	
	INT iCurrentMakeup
	FLOAT fMakeupBlend
	GET_CHAR_CREATOR_OVERLAY_VALUES(charCreatorData, CHAR_CREATOR_MAKEUP, iCurrentMakeup, fMakeupBlend)
	
	iCurrentMakeup += iIncrement
	
	//WHILE IS_MAKEUP_DISALLOWED_IN_CREATOR(iCurrentMakeup)
	//WHILE iCurrentMakeup >= -1
	//AND iCurrentMakeup <= CHAR_CREATOR_MAX_MAKEUP
	//ENDWHILE
	
	// Cap and wrap around our makeup
	IF iCurrentMakeup < -1
		iCurrentMakeup = (CHAR_CREATOR_MAX_MAKEUP-1)
	ENDIF
	
	IF iCurrentMakeup >= CHAR_CREATOR_MAX_MAKEUP
		iCurrentMakeup = -1
	ENDIF

	
	charCreatorData.pedData.charOverlays[ENUM_TO_INT(CHAR_CREATOR_MAKEUP)].iOverlayVal 		= iCurrentMakeup
	charCreatorData.pedData.charOverlays[ENUM_TO_INT(CHAR_CREATOR_MAKEUP)].fOverlayBlend 	= 1.0
	
	CHAR_CREATOR_LIFESTYLE maxLifestyle = GET_MAX_LIFESTYLE(charCreatorData)
	INT iLockLifestyleSlot = ENUM_TO_INT(maxLifestyle) - 1
	charCreatorData.iCacheFemaleMakeup[iLockLifestyleSlot] = iCurrentMakeup		
	
	PRINTLN("[CHARCREATOR] PROCESS_CHAR_CREATOR_CHANGE_MAKEUP - iCurrentMakeup: ", iCurrentMakeup)
	
	APPLY_CHAR_CREATOR_MAKEUP_OVERLAY(charCreatorData)
ENDPROC

PROC APPLY_CREW_TSHIRT_ZOOM(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData)
	IF charCreatorData.fCharZoom < CREW_SHIRT_ZOOM
		charCreatorData.fCharZoom = CREW_SHIRT_ZOOM
		PROCESS_CHAR_CREATOR_ZOOM(charCreatorData, 0)
	ENDIF
	SET_BIT(charCreatorData.iCharCreatorBitset, CHAR_CREATOR_FORCE_CREW_SHIRT_ZOOM)
ENDPROC

/// PURPOSE: Change the character's crew tshirt in the direction from input
PROC PROCESS_CHAR_CREATOR_CHANGE_CREW_TSHIRT(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData, INT iIncrement)
	INT iLastOption = ENUM_TO_INT(charCreatorData.crewTshirtOption)

	INT iOption = iLastOption + iIncrement
	
	//INT iMaxOption
	//IF NOT IS_CHAR_CREATOR_PED_MALE(charCreatorData)
	//	iMaxOption = 3
	//ELSE
	//	iMaxOption = 4
	//ENDIF
	IF iOption > 10
		iOption = 0
	ELIF iOption < 0
		iOption = 10
	ENDIF
	
	charCreatorData.crewTshirtOption = INT_TO_ENUM(CHAR_CREATOR_CREW_TSHIRT_OPTION_enum, iOption)
	
	BOOL bCancelShirt = FALSE
	IF charCreatorData.bEnteredWithCrewTShirt
		IF charCreatorData.crewTshirtOption = CREW_TSHIRT_OFF
			charCreatorData.bEnteredWithCrewTShirt = FALSE
			bCancelShirt = TRUE
		ENDIF	
	ENDIF
	
	SET_CHAR_CREATOR_OUTFIT(charCreatorData, TRUE, TRUE, FALSE, FALSE, TRUE, FALSE, FALSE, bCancelShirt)

	APPLY_CREW_TSHIRT_DECALS_TO_CURRENT_PED(charCreatorData, TRUE)
	
	IF iLastOption = 0
		SET_BIT(charCreatorData.iCharCreatorBitset, CHAR_CREATOR_DO_DELAYED_CREW_SHIRT_ZOOM)
	ELIF iLastOption <> 0
	AND iOption = 0
		SET_BIT(charCreatorData.iCharCreatorBitset, CHAR_CREATOR_READY_DELAYED_CREW_SHIRT_ZOOM)
	ELSE
		APPLY_CREW_TSHIRT_ZOOM(charCreatorData)
	ENDIF
	
	charCreatorData.bInitialisedOutfit = FALSE
ENDPROC

/// PURPOSE: Change the character's crew decals in the direction from input
PROC PROCESS_CHAR_CREATOR_CHANGE_CREW_DECALS(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData, INT iIncrement)
	INT iOption = ENUM_TO_INT(charCreatorData.crewDecalOption) + iIncrement
	
	INT iMaxOption
	IF NOT IS_CHAR_CREATOR_PED_MALE(charCreatorData)
		iMaxOption = 2
	ELSE
		iMaxOption = 3
	ENDIF
	IF iOption > iMaxOption
		iOption = 0
	ELIF iOption < 0
		iOption = iMaxOption
	ENDIF
	
	charCreatorData.crewDecalOption = INT_TO_ENUM(CHAR_CREATOR_CREW_DECAL_OPTION_ENUM, iOption)
	
	SET_CHAR_CREATOR_OUTFIT(charCreatorData, TRUE, TRUE, FALSE, FALSE, TRUE)
	APPLY_CREW_TSHIRT_DECALS_TO_CURRENT_PED(charCreatorData)
	APPLY_CREW_TSHIRT_ZOOM(charCreatorData)
	
	charCreatorData.bInitialisedOutfit = FALSE
ENDPROC


/// PURPOSE: Change the character's accessories in the direction from input
PROC PROCESS_CHAR_CREATOR_CHANGE_HAT(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData, INT iIncrement)
	CHAR_CREATOR_LIFESTYLE maxLifestyle = GET_MAX_LIFESTYLE(charCreatorData)
	INT iLockLifestyleSlot = ENUM_TO_INT(maxLifestyle) - 1	
	
	INT iCurrentHat 
	INT iMaxHats
	IF IS_CHAR_CREATOR_PED_MALE(charCreatorData)
		iCurrentHat = charCreatorData.iCacheMaleHatOption[iLockLifestyleSlot]
		iMaxHats = GET_NUM_OF_HAT_OPTIONS_FOR_SPAWN_OUTFIT(TRUE, charCreatorData.pedData.charOutfit)
	ELSE	
		iCurrentHat = charCreatorData.iCacheFemaleHatOption[iLockLifestyleSlot]	
		iMaxHats = GET_NUM_OF_HAT_OPTIONS_FOR_SPAWN_OUTFIT(FALSE, charCreatorData.pedData.charOutfit)
	ENDIF
	
	iCurrentHat += iIncrement
	
	IF iCurrentHat >= iMaxHats
		iCurrentHat = -1
	ELIF iCurrentHat < -1
		iCurrentHat = iMaxHats - 1
	ENDIF
	
	IF IS_CHAR_CREATOR_PED_MALE(charCreatorData)
		charCreatorData.iCacheMaleHatOption[iLockLifestyleSlot] = iCurrentHat 
		IF iCurrentHat >= 0
			SET_PED_COMP_ITEM_CURRENT_MP(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON], COMP_TYPE_PROPS, GET_HAT_OPTION_FOR_SPAWN_OUTFIT(TRUE, charCreatorData.pedData.charOutfit, iCurrentHat), FALSE)
		ELSE
			CLEAR_PED_PROP(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON], ANCHOR_HEAD)
		ENDIF
	ELSE	
		charCreatorData.iCacheFemaleHatOption[iLockLifestyleSlot] = iCurrentHat 
		IF iCurrentHat >= 0
			SET_PED_COMP_ITEM_CURRENT_MP(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER], COMP_TYPE_PROPS, GET_HAT_OPTION_FOR_SPAWN_OUTFIT(FALSE, charCreatorData.pedData.charOutfit, iCurrentHat), FALSE)
		ELSE
			CLEAR_PED_PROP(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER], ANCHOR_HEAD)
		ENDIF		
	ENDIF
ENDPROC

/// PURPOSE: Change the character's accessories in the direction from input
PROC PROCESS_CHAR_CREATOR_CHANGE_GLASSES(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData, INT iIncrement)
	CHAR_CREATOR_LIFESTYLE maxLifestyle = GET_MAX_LIFESTYLE(charCreatorData)
	INT iLockLifestyleSlot = ENUM_TO_INT(maxLifestyle) - 1	
	
	INT iCurrentGlasses 
	INT iMaxGlassess
	IF IS_CHAR_CREATOR_PED_MALE(charCreatorData)
		iCurrentGlasses = charCreatorData.iCacheMaleGlassesOption[iLockLifestyleSlot]
		iMaxGlassess = GET_NUM_OF_GLASSES_OPTIONS_FOR_SPAWN_OUTFIT(TRUE, charCreatorData.pedData.charOutfit)
	ELSE	
		iCurrentGlasses = charCreatorData.iCacheFemaleGlassesOption[iLockLifestyleSlot]	
		iMaxGlassess = GET_NUM_OF_GLASSES_OPTIONS_FOR_SPAWN_OUTFIT(FALSE, charCreatorData.pedData.charOutfit)
	ENDIF
	
	iCurrentGlasses += iIncrement
	
	IF iCurrentGlasses >= iMaxGlassess
		iCurrentGlasses = -1
	ELIF iCurrentGlasses < -1
		iCurrentGlasses = iMaxGlassess - 1
	ENDIF
	
	IF IS_CHAR_CREATOR_PED_MALE(charCreatorData)
		charCreatorData.iCacheMaleGlassesOption[iLockLifestyleSlot] = iCurrentGlasses 
		IF iCurrentGlasses >= 0
			SET_PED_COMP_ITEM_CURRENT_MP(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON], COMP_TYPE_PROPS, GET_GLASSES_OPTION_FOR_SPAWN_OUTFIT(TRUE, charCreatorData.pedData.charOutfit, iCurrentGlasses), FALSE)
		ELSE
			CLEAR_PED_PROP(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON], ANCHOR_EYES)
		ENDIF
	ELSE	
		charCreatorData.iCacheFemaleGlassesOption[iLockLifestyleSlot] = iCurrentGlasses 
		IF iCurrentGlasses >= 0
			SET_PED_COMP_ITEM_CURRENT_MP(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER], COMP_TYPE_PROPS, GET_GLASSES_OPTION_FOR_SPAWN_OUTFIT(FALSE, charCreatorData.pedData.charOutfit, iCurrentGlasses), FALSE)
		ELSE
			CLEAR_PED_PROP(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER], ANCHOR_EYES)
		ENDIF		
	ENDIF
ENDPROC


/// PURPOSE: Update our appearance value we have changed
PROC PROCESS_CHAR_CREATOR_APPEARANCE_CHANGE(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData, INT iIncrement)

	IF NOT ARE_ALL_CHAR_CREATOR_PEDS_LOADED(charCreatorData)
		EXIT
	ENDIF

	SWITCH charCreatorData.appearanceIndex
		CASE APPEARANCE_AGE
			PROCESS_CHAR_CREATOR_CHANGE_AGE(charCreatorData, iIncrement)
		BREAK
		CASE APPEARANCE_HAIR
			PROCESS_CHAR_CREATOR_CHANGE_HAIR(charCreatorData, iIncrement)
		BREAK
		CASE APPEARANCE_HAIR_COLOUR
			PROCESS_CHAR_CREATOR_CHANGE_HAIR_COLOUR(charCreatorData, iIncrement)
		BREAK
		CASE APPEARANCE_BEARD
			PROCESS_CHAR_CREATOR_CHANGE_BEARD(charCreatorData, iIncrement)
		BREAK
		CASE APPEARANCE_MAKEUP
			PROCESS_CHAR_CREATOR_CHANGE_MAKEUP(charCreatorData, iIncrement)
		BREAK	
		//CASE APPEARANCE_ACCESSORIES
		//	PROCESS_CHAR_CREATOR_CHANGE_ACCESSORIES(charCreatorData)
		//BREAK	
		CASE APPEARANCE_HAT
			PROCESS_CHAR_CREATOR_CHANGE_HAT(charCreatorData, iIncrement)
		BREAK
		CASE APPEARANCE_GLASSES
			PROCESS_CHAR_CREATOR_CHANGE_GLASSES(charCreatorData, iIncrement)
		BREAK		
		CASE APPEARANCE_CREW_TSHIRT
			PROCESS_CHAR_CREATOR_CHANGE_CREW_TSHIRT(charCreatorData, iIncrement)
		BREAK	
		CASE APPEARANCE_CREW_DECALS
			PROCESS_CHAR_CREATOR_CHANGE_CREW_DECALS(charCreatorData, iIncrement)
		BREAK	
	ENDSWITCH

	PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
ENDPROC

FUNC MODEL_NAMES GET_CHAR_CREATOR_SPECIAL_PED_MODEL_NAME(INT iPedIndex)
	
	SWITCH iPedIndex
		CASE 0	RETURN MP_F_MISTY_01
		CASE 1	RETURN MP_M_Claude_01
		CASE 2	RETURN MP_M_NIKO_01
		CASE 3	RETURN MP_M_Marston_01
	ENDSWITCH

	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

/// PURPOSE: Sets up the special peds for the character creator
FUNC BOOL CHAR_CREATOR_CREATE_SPECIAL_PED(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData, VECTOR vPosition, INT iPedIndex)

	MODEL_NAMES mName = GET_CHAR_CREATOR_SPECIAL_PED_MODEL_NAME(iPedIndex)

	IF mName != DUMMY_MODEL_FOR_SCRIPT
		REQUEST_MODEL(mName)
		REQUEST_ANIM_DICT("move_m@generic")

		IF HAS_MODEL_LOADED(mName)
		AND HAS_ANIM_DICT_LOADED("move_m@generic")
			
			IF NOT DOES_ENTITY_EXIST(charCreatorData.charCreatorSpecialPeds[iPedIndex])
			
				charCreatorData.charCreatorSpecialPeds[iPedIndex] = CREATE_PED(PEDTYPE_SPECIAL, mName, vPosition, 0.0, FALSE, FALSE)
			
				#IF IS_DEBUG_BUILD
					TEXT_LABEL_15 tlPedName = "Special_ped_"
					tlPedName += iPedIndex
					SET_PED_NAME_DEBUG(charCreatorData.charCreatorSpecialPeds[iPedIndex], tlPedName)
				#ENDIF
			
				CLEAR_PED_TASKS_IMMEDIATELY(charCreatorData.charCreatorSpecialPeds[iPedIndex])
				FREEZE_ENTITY_POSITION(charCreatorData.charCreatorSpecialPeds[iPedIndex], TRUE)
				SET_ENTITY_COLLISION(charCreatorData.charCreatorSpecialPeds[iPedIndex], FALSE)
				TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(charCreatorData.charCreatorSpecialPeds[iPedIndex], TRUE)
				STOP_PED_SPEAKING(charCreatorData.charCreatorSpecialPeds[iPedIndex], TRUE)
				DISABLE_PED_PAIN_AUDIO(charCreatorData.charCreatorSpecialPeds[iPedIndex], TRUE)
			
				FORCE_ALL_HEADING_VALUES_TO_ALIGN(charCreatorData.charCreatorSpecialPeds[iPedIndex])
	
				// Remove model
				SET_MODEL_AS_NO_LONGER_NEEDED(mName)
				
				TASK_PLAY_ANIM(charCreatorData.charCreatorSpecialPeds[iPedIndex], "move_m@generic", "idle", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_HOLD_LAST_FRAME, 0.9, TRUE)
				SET_PARENT_STATIC_FACE(charCreatorData.charCreatorSpecialPeds[iPedIndex])
				REMOVE_ANIM_DICT("move_m@generic")
			ENDIF
			
			RETURN TRUE
		ENDIF
		
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE: Generates a ped for the character creator
/// PARAMS:
///    charCreatorData - Data we use in creator
///    aPed - ped index to create the ped
///    charPedType - type of ped (dad, mum etc)
///    mnModel - Model name
///    vPosition - Position to create ped
///    iTeam - Team of the player
/// RETURNS:
///    TRUE when the player has been successfully set up
FUNC BOOL CHAR_CREATOR_CREATE_PED(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData, PED_INDEX &aPed, CHAR_CREATOR_PED charPedType, MODEL_NAMES mnModel, VECTOR vPosition, INT iTeam)
iteam = iteam
	REQUEST_MODEL(mnModel)

	REQUEST_ANIM_DICT("MP_HEAD_IK_OVERRIDE")
	REQUEST_ANIM_DICT("move_m@generic")
	
	IF HAS_MODEL_LOADED(mnModel)
	AND HAS_ANIM_DICT_LOADED("MP_HEAD_IK_OVERRIDE")
	AND HAS_ANIM_DICT_LOADED("move_m@generic")
		
		IF NOT DOES_ENTITY_EXIST(aPed)
		
			aPed = CREATE_PED(PEDTYPE_SPECIAL, mnModel, vPosition, 0.0, FALSE, FALSE)
			
			#IF IS_DEBUG_BUILD
				TEXT_LABEL_15 tlPedName = "Standard_ped_"
				tlPedName += ENUM_TO_INT(charPedType)
				SET_PED_NAME_DEBUG(aPed, tlPedName)
			#ENDIF
			
			CLEAR_PED_TASKS_IMMEDIATELY(aPed)
			FREEZE_ENTITY_POSITION(aPed, TRUE)
			SET_ENTITY_COLLISION(aPed, FALSE)
			TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(aPed, TRUE)
			STOP_PED_SPEAKING(aPed, TRUE)
			DISABLE_PED_PAIN_AUDIO(aPed, TRUE)
			
//			FLOAT fHeading
//			IF DOES_ENTITY_EXIST(aPed)			   	
//				fHeading = GET_ENTITY_HEADING(aPed)
//				SET_PED_DESIRED_HEADING(aPed, fHeading)
//				SET_ENTITY_HEADING(aPed, fHeading)
//				SET_ENTITY_ROTATION(aPed, <<0.0, 0.0, fHeading>>)
//			ENDIF
			
			// Make sure we align everything
			FORCE_ALL_HEADING_VALUES_TO_ALIGN(aPed)
						
			// Remove model
			SET_MODEL_AS_NO_LONGER_NEEDED(mnModel)

			IF charPedType = CHAR_CREATOR_PED_DAD			
				PRINTLN("[CHARCREATOR] CHAR_CREATOR_CREATE_PED creating blend on dad")
				
				PROCESS_CHAR_CREATOR_PARENT_UPDATE(charCreatorData, charPedType)
			
				//SET_MP_PARENTS_RANDOM_OUTFIT(iTeam, aPed)
				IF NOT IS_PED_INJURED(aPed)
					SET_PED_COMPONENT_VARIATION(aPed, PED_COMP_JBIB, 4, 0)
					SET_PED_COMPONENT_VARIATION(aPed, PED_COMP_SPECIAL, 13, 1)
				ENDIF
					
				IF NOT IS_CHAR_CREATOR_PARENT_SPECIAL_HEAD(GET_CHAR_CREATOR_PARENT_RACE(charCreatorData, charPedType, CHAR_CREATOR_RACE_INDEX_ONE))
									
					IF HAS_ENTERED_CHAR_CREATION_FROM_ALTERATION_PROMPT()
						
						// Update beard and eyebrow
						PRINTLN("[CHARCREATOR] CHAR_CREATOR_CREATE_PED - Setting dad beard: ", GET_MP_INT_CHARACTER_STAT(MP_STAT_HEADBLEND_DAD_BEARD, charCreatorData.iCharacterSelectSlot))
						PRINTLN("[CHARCREATOR] CHAR_CREATOR_CREATE_PED - Setting dad eyebrow: ", GET_MP_INT_CHARACTER_STAT(MP_STAT_HEADBLEND_DAD_EYEB, charCreatorData.iCharacterSelectSlot))
						
						SET_PED_HEAD_OVERLAY(aPed, HOS_FACIAL_HAIR, GET_MP_INT_CHARACTER_STAT(MP_STAT_HEADBLEND_DAD_BEARD, charCreatorData.iCharacterSelectSlot), 1.0)
						SET_PED_HEAD_OVERLAY(aPed, HOS_EYEBROW, GET_MP_INT_CHARACTER_STAT(MP_STAT_HEADBLEND_DAD_EYEB, charCreatorData.iCharacterSelectSlot), 1.0)
					ENDIF
				ENDIF
				
				// Give them the animation
				TASK_PLAY_ANIM(aPed, "move_m@generic", "idle", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_HOLD_LAST_FRAME, 0.9, TRUE)
				SET_PARENT_STATIC_FACE(aPed)
			
			ELIF charPedType = CHAR_CREATOR_PED_MUM					
				PROCESS_CHAR_CREATOR_PARENT_UPDATE(charCreatorData, charPedType)
				
				//SET_MP_PARENTS_RANDOM_OUTFIT(iTeam, aPed)
				IF NOT IS_PED_INJURED(aPed)
					SET_PED_COMPONENT_VARIATION(aPed, PED_COMP_JBIB, 9, 0)
					//SET_PED_COMPONENT_VARIATION(aPed, PED_COMP_TORSO, 2, 0)
					SET_PED_COMPONENT_VARIATION(aPed, PED_COMP_FEET, 1, 0)
				ENDIF		
				
				IF NOT IS_CHAR_CREATOR_PARENT_SPECIAL_HEAD(GET_CHAR_CREATOR_PARENT_RACE(charCreatorData, charPedType, CHAR_CREATOR_RACE_INDEX_ONE))
					
					IF HAS_ENTERED_CHAR_CREATION_FROM_ALTERATION_PROMPT()
	
						// Update eyebrow
						PRINTLN("[CHARCREATOR] CHAR_CREATOR_CREATE_PED - Setting dad eyebrow: ", GET_MP_INT_CHARACTER_STAT(MP_STAT_HEADBLEND_MUM_EYEB, charCreatorData.iCharacterSelectSlot))
						SET_PED_HEAD_OVERLAY(aPed, HOS_EYEBROW, GET_MP_INT_CHARACTER_STAT(MP_STAT_HEADBLEND_MUM_EYEB, charCreatorData.iCharacterSelectSlot), 1.0)
					ENDIF
				ENDIF
				
				TASK_PLAY_ANIM(aPed, "move_m@generic", "idle", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_HOLD_LAST_FRAME, 0.9, TRUE)
				SET_PARENT_STATIC_FACE(aPed)
			ELIF charPedType = CHAR_CREATOR_PED_GRANDMA_M
			OR charPedType = CHAR_CREATOR_PED_GRANDMA_D
			OR charPedType = CHAR_CREATOR_PED_GRANDDAD_M
			OR charPedType = CHAR_CREATOR_PED_GRANDDAD_D
				TASK_PLAY_ANIM(aPed, "move_m@generic", "idle", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_HOLD_LAST_FRAME, 0.9, TRUE)
				SET_PARENT_STATIC_FACE(aPed)
				SET_PED_COMPONENT_VARIATION(aPed, PED_COMP_FEET, 0, 0)
			ELIF charPedType = CHAR_CREATOR_PED_SON
			OR charPedType = CHAR_CREATOR_PED_SON_DUMMY
			OR charPedType = CHAR_CREATOR_PED_DAUGHTER
			OR charPedType = CHAR_CREATOR_PED_DAUGHTER_DUMMY
				
				PRINTLN("[CHARCREATOR] CHAR_CREATOR_CREATE_PED - Setting childs clothes and tattoos, slot: ", charCreatorData.iCharacterSelectSlot)
				
				// For MARK TENNANT. Plays animation on children. Where we want to use an untagged anim so the look at coords is not restricted
				TASK_PLAY_ANIM(aPed, "MP_HEAD_IK_OVERRIDE", "MP_CREATOR_HEADIK", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_SECONDARY|AF_LOOPING|AF_ADDITIVE)
				
				GIVE_CHAR_CREATOR_PED_HAIR(charCreatorData, charPedType)

				SET_MP_PLAYERS_START_CLOTHES(TRUE, TSHIRT_STATES_NEWCHARACTER, aPed, charCreatorData.iCharacterSelectSlot)
				SET_MP_CHARACTER_TATTOOS_AND_PATCHES(aPed, charCreatorData.iCharacterSelectSlot)

				// Update our mood!
				SET_CHAR_CREATOR_MOOD(charCreatorData)
			ENDIF
			
			REMOVE_ANIM_DICT("MP_HEAD_IK_OVERRIDE")
			REMOVE_ANIM_DICT("move_m@generic")
		ENDIF
		
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

/// PURPOSE: Deletes the peds when leaving the screen
PROC DELETE_CHAR_CREATOR_PEDS(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData)
	
	PRINTLN("[CHARCREATOR] DELETE_CHAR_CREATOR_PEDS called...")
	
	INT i
	REPEAT CHAR_CREATOR_PED_MAX i
		
		//[BCCreator] Don't delete these, you'll delete the pedindex linked in IdlePed as well. Just mark this variable as null since I don't know if you went to Son or Daughter here. 
		//charCreatorData.charCreatorPeds[i] = NULL
		
		IF DOES_ENTITY_EXIST(charCreatorData.charCreatorPeds[i])
			PRINTLN("[CHARCREATOR]		ped ", i)
			DELETE_PED(charCreatorData.charCreatorPeds[i])
		ENDIF
		
	ENDREPEAT
	
	// Clean up our special peds
	REPEAT CHAR_CREATOR_SPECIAL_PEDS_MAX i

		IF DOES_ENTITY_EXIST(charCreatorData.charCreatorSpecialPeds[i])
			PRINTLN("[CHARCREATOR]		Special ped ", i)
			DELETE_PED(charCreatorData.charCreatorSpecialPeds[i])
		ENDIF
		
	ENDREPEAT
	
ENDPROC

/// PURPOSE: Sets the desired heading every frame
PROC CHAR_CREATOR_SET_DESIRED_HEADING(PED_INDEX &aPed)
	FORCE_ALL_HEADING_VALUES_TO_ALIGN(aPed)
	
	#IF IS_DEBUG_BUILD
	IF g_UseForceAllHeadingValuesToAlign
		EXIT
	ENDIF
	#ENDIF
	
	
//	FLOAT fHeading = GET_ENTITY_HEADING(aPed)
//	SET_PED_DESIRED_HEADING(aPed, fHeading)
//	SET_ENTITY_HEADING(aPed, fHeading)
//	SET_ENTITY_ROTATION(aPed, <<0.0, 0.0, fHeading>>)
ENDPROC

/// PURPOSE: Main function to maintain the loading and creation of the peds
PROC CHAR_CREATOR_LOAD_PEDS(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData, MPHUD_PLACEMENT_TOOLS& Placement, BOOL &bResetAppearanceMenu)
	bResetAppearanceMenu = FALSE
	IF charCreatorData.charCreatorState > CHARACTER_CREATOR_WAIT_FOR_FRONTEND
	AND (GET_SKYSWOOP_STAGE() = SKYSWOOP_NONE OR GET_SKYSWOOP_STAGE() = SKYSWOOP_INSKYSTATIC)
		IF NOT ARE_ALL_CHAR_CREATOR_PEDS_LOADED(charCreatorData)
			SWITCH charCreatorData.charCreatorPedState
				CASE CHARACTER_CREATOR_PED_CREATE
					
					VECTOR CamCoord
					VECTOR CamRot
					CamCoord = GET_FINAL_RENDERED_CAM_COORD()
					CamRot = GET_FINAL_RENDERED_CAM_ROT()
					
					PRINTSTRING("camcoord = ") PRINTVECTOR(CamCoord) PRINTNL()
					PRINTSTRING("camrot = ") PRINTVECTOR(CamRot) PRINTNL()
					
					CamCoord = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(CamCoord, CamRot.z, <<0.0, -5.0, 0.0>>)
				
					INT iTeam
					MODEL_NAMES mnName
					BOOL bAllPedsLoaded
					bAllPedsLoaded = TRUE
				
					iTeam = GET_STAT_CHARACTER_TEAM(Placement.ScreenPlace.iSelectedCharacter)
					mnName = GET_PLAYER_MODEL_FOR_TEAM_VARIATION_FOR_MENU(iTeam, ENUM_TO_INT(GET_CHAR_CREATOR_GENDER_OF_PED(INT_TO_ENUM(CHAR_CREATOR_PED, charCreatorData.iCharacterPedCreationIndex))))
					
					IF CHAR_CREATOR_CREATE_PED(charCreatorData, charCreatorData.charCreatorPeds[charCreatorData.iCharacterPedCreationIndex], INT_TO_ENUM(CHAR_CREATOR_PED, charCreatorData.iCharacterPedCreationIndex), mnName, CamCoord, iTeam)
							
						// Anything to do here? set up clothes / face?
						charCreatorData.iCharacterPedCreationIndex++
						
						// Cap the index
						IF charCreatorData.iCharacterPedCreationIndex >= ENUM_TO_INT(CHAR_CREATOR_PED_MAX)
							charCreatorData.iCharacterPedCreationIndex = 0
						ENDIF
							
						PRINTLN("[CHARCREATOR] CHAR_CREATOR_LOAD_PEDS - Moving onto next creator ped: ", charCreatorData.iCharacterPedCreationIndex)
					ENDIF
					
					INT i
					REPEAT CHAR_CREATOR_PED_MAX i
						IF NOT DOES_ENTITY_EXIST(charCreatorData.charCreatorPeds[i])
							
							PRINTLN("[CHARCREATOR] CHAR_CREATOR_LOAD_PEDS - Ped: ", i, " has not been created")
						
							bAllPedsLoaded = FALSE
						ENDIF
					ENDREPEAT
						
					REPEAT CHAR_CREATOR_SPECIAL_PEDS_MAX i
						
						IF CHAR_CREATOR_CREATE_SPECIAL_PED(charCreatorData, CamCoord, i)
							// Nothing to do at the moment
						ELSE
							bAllPedsLoaded = FALSE
						ENDIF
						
					ENDREPEAT
					
					IF bAllPedsLoaded
						PRINTLN("[CHARCREATOR] CHAR_CREATOR_LOAD_PEDS - All peds have loaded. goto: CHARACTER_CREATOR_PED_BLEND")

						charCreatorData.charCreatorPedState = CHARACTER_CREATOR_PED_BLEND
					ENDIF				
				BREAK
				
				CASE CHARACTER_CREATOR_PED_BLEND
				
					IF (HAS_CHAR_CREATOR_PED_BLEND_FINISHED(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_MUM]) 
					AND HAS_CHAR_CREATOR_PED_BLEND_FINISHED(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAD]))
					AND NOT IS_ENTITY_DEAD(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON])
					AND NOT IS_ENTITY_DEAD(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER])
					
						SET_PED_BLEND_FROM_PARENTS(	charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON], 
													charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_MUM], charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAD],
													GET_CHAR_CREATOR_PARENT_DOMINANCE(charCreatorData, CHAR_CREATOR_GENDER_MALE))
													
						SET_PED_BLEND_FROM_PARENTS(	charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER], 
													charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_MUM], charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAD],
													GET_CHAR_CREATOR_PARENT_DOMINANCE(charCreatorData, CHAR_CREATOR_GENDER_FEMALE))
						
						// Update our age values
						CALC_CHAR_CREATOR_AGE_OVERLAY(charCreatorData)
						
						// Update our overlays on the peds
						PROCESS_CHAR_CREATOR_CHILD_OVERLAYS(charCreatorData)
						
						PRINTLN("[CHARCREATOR] CHAR_CREATOR_LOAD_PEDS - Start blends of children. goto: CHARACTER_CREATOR_PED_WAIT")
						PRINTLN("[CHARCREATOR X] CHAR_CREATOR_LOAD_PEDS - Start blends of son: ", NATIVE_TO_INT(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON]), " with dom: ", GET_CHAR_CREATOR_PARENT_DOMINANCE(charCreatorData, CHAR_CREATOR_GENDER_MALE))
						PRINTLN("[CHARCREATOR X] CHAR_CREATOR_LOAD_PEDS - Start blends of daughter: ", NATIVE_TO_INT(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER]), " with dom: ", GET_CHAR_CREATOR_PARENT_DOMINANCE(charCreatorData, CHAR_CREATOR_GENDER_FEMALE))
						charCreatorData.charCreatorPedState = CHARACTER_CREATOR_PED_APPLY_OUTFIT
					
					#IF IS_DEBUG_BUILD
					ELSE
						IF (GET_GAME_TIMER() % 1000 < 75)
							PRINTLN("[CHARCREATOR] CHAR_CREATOR_LOAD_PEDS - Waiting for parents blend to finish")
						ENDIF
					#ENDIF				
					ENDIF
					
				BREAK
				
				CASE CHARACTER_CREATOR_PED_APPLY_OUTFIT
					// outfits
					IF NOT HAS_ENTERED_CHAR_CREATION_FROM_ALTERATION_PROMPT()
						IF IS_CHAR_CREATOR_PED_MALE(charCreatorData)
							SET_CHAR_CREATOR_OUTFIT(charCreatorData, DEFAULT, TRUE, FALSE, TRUE, DEFAULT, TRUE, TRUE)
							SET_CHAR_CREATOR_OUTFIT(charCreatorData, DEFAULT, TRUE, TRUE, FALSE, DEFAULT, TRUE, TRUE)
						ELSE
							SET_CHAR_CREATOR_OUTFIT(charCreatorData, DEFAULT, TRUE, TRUE, FALSE, DEFAULT, TRUE, TRUE)
							SET_CHAR_CREATOR_OUTFIT(charCreatorData, DEFAULT, TRUE, FALSE, TRUE, DEFAULT, TRUE, TRUE)
						ENDIF
						CLEAR_ALL_PED_PROPS(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON])
						CLEAR_ALL_PED_PROPS(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER])						
					ELSE
						INT iCheckDecal
						BOOL bHasCrewDecal
						
						CHAR_CREATOR_LIFESTYLE maxLifestyle 
						maxLifestyle = GET_MAX_LIFESTYLE(charCreatorData)
						INT iLockLifestyleSlot 
						iLockLifestyleSlot = ENUM_TO_INT(maxLifestyle) - 1								
						
						// Set up players outfit
						IF IS_CHAR_CREATOR_PED_MALE(charCreatorData)
							SET_MP_PLAYERS_START_CLOTHES(FALSE, TSHIRT_STATES_INGAME_USE_STORED, charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON_DUMMY], Placement.ScreenPlace.iSelectedCharacter )
							
							// backup crew decal
							bHasCrewDecal = FALSE
							FOR iCheckDecal=TATTOO_MP_FM_CREW_A TO TATTOO_MP_FM_CREW_D
								IF IS_MP_TATTOO_CURRENT(INT_TO_ENUM(TATTOO_NAME_ENUM, iCheckDecal), Placement.ScreenPlace.iSelectedCharacter)
									charCreatorData.enteredCrewDecalOption = INT_TO_ENUM(TATTOO_NAME_ENUM, iCheckDecal)
									bHasCrewDecal = TRUE
								ENDIF
							ENDFOR							
							
							SET_CHAR_CREATOR_OUTFIT(charCreatorData, FALSE, TRUE, FALSE, TRUE)
							
							SET_CHAR_CREATOR_CREW_TSHIRT(charCreatorData, FALSE, FALSE)
							
							charCreatorData.iCacheMaleHatOption[iLockLifestyleSlot] = GET_CURRENT_CHAR_CREATOR_HAT_OPTION(charCreatorData, TRUE, TRUE)
							charCreatorData.iCacheMaleGlassesOption[iLockLifestyleSlot] = GET_CURRENT_CHAR_CREATOR_GLASSES_OPTION(charCreatorData, TRUE, TRUE)
							charCreatorData.iCacheMaleBeard[iLockLifestyleSlot] = charCreatorData.pedData.charOverlays[CHAR_CREATOR_BEARD].iOverlayVal
							
							SET_BIT(charCreatorData.iCharCreatorBitset, CHAR_CREATOR_COPY_FROM_SON_DUMMY)	
							charCreatorData.iSetSonDummyTime = 0				
							
							IF bHasCrewDecal
								EQUIP_CREW_LOGO(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON_DUMMY], charCreatorData.enteredCrewDecalOption)
							ENDIF
							
							charCreatorData.bEnteredAsMale = TRUE
							charCreatorData.savedJbib = GET_PED_COMP_ITEM_CURRENT_MP(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON_DUMMY], COMP_TYPE_JBIB)	
							charCreatorData.savedSpecial = GET_PED_COMP_ITEM_CURRENT_MP(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON_DUMMY], COMP_TYPE_SPECIAL)	
						ELSE
							SET_MP_PLAYERS_START_CLOTHES(FALSE, TSHIRT_STATES_INGAME_USE_STORED, charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER_DUMMY], Placement.ScreenPlace.iSelectedCharacter )
	
							// backup crew decal
							bHasCrewDecal = FALSE
							FOR iCheckDecal=TATTOO_MP_FM_CREW_A TO TATTOO_MP_FM_CREW_D
								IF IS_MP_TATTOO_CURRENT(INT_TO_ENUM(TATTOO_NAME_ENUM, iCheckDecal), Placement.ScreenPlace.iSelectedCharacter)
									charCreatorData.enteredCrewDecalOption = INT_TO_ENUM(TATTOO_NAME_ENUM, iCheckDecal)
									bHasCrewDecal = TRUE
								ENDIF
							ENDFOR										
							
							SET_CHAR_CREATOR_OUTFIT(charCreatorData, FALSE, TRUE, TRUE, FALSE)
							
							SET_CHAR_CREATOR_CREW_TSHIRT(charCreatorData, FALSE, FALSE)
							
							charCreatorData.iCacheFemaleHatOption[iLockLifestyleSlot] = GET_CURRENT_CHAR_CREATOR_HAT_OPTION(charCreatorData, FALSE, TRUE)
							charCreatorData.iCacheFemaleGlassesOption[iLockLifestyleSlot] = GET_CURRENT_CHAR_CREATOR_GLASSES_OPTION(charCreatorData, FALSE, TRUE)
							charCreatorData.iCacheFemaleMakeup[iLockLifestyleSlot] = charCreatorData.pedData.charOverlays[CHAR_CREATOR_MAKEUP].iOverlayVal
							
							SET_BIT(charCreatorData.iCharCreatorBitset, CHAR_CREATOR_COPY_FROM_DAUGHTER_DUMMY)	
							charCreatorData.iSetDaughterDummyTime = 0									
							
							IF bHasCrewDecal
								EQUIP_CREW_LOGO(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER_DUMMY], charCreatorData.enteredCrewDecalOption)
							ENDIF							
							
							charCreatorData.bEnteredAsMale = FALSE
							charCreatorData.savedJbib = GET_PED_COMP_ITEM_CURRENT_MP(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER_DUMMY], COMP_TYPE_JBIB)
							charCreatorData.savedSpecial = GET_PED_COMP_ITEM_CURRENT_MP(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER_DUMMY], COMP_TYPE_SPECIAL)	
						ENDIF
					ENDIF

					charCreatorData.charCreatorPedState = CHARACTER_CREATOR_PED_WAIT
					bResetAppearanceMenu = TRUE
				BREAK
				
				CASE CHARACTER_CREATOR_PED_WAIT
				
					IF HAS_CHAR_CREATOR_PED_BLEND_FINISHED(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON]) 
					AND HAS_CHAR_CREATOR_PED_BLEND_FINISHED(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER])
				
						PRINTLN("[CHARCREATOR] CHAR_CREATOR_LOAD_PEDS - All peds have been loaded and created")
						
						// Extra debug for Flavius (1754906)
						PRINTLN("[CHARCREATOR X] CHAR_CREATOR_LOAD_PEDS - Son......................: ", NATIVE_TO_INT(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON]))
						PRINTLN("[CHARCREATOR X] CHAR_CREATOR_LOAD_PEDS - Daughter.................: ", NATIVE_TO_INT(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER]))
						PRINTLN("[CHARCREATOR X] CHAR_CREATOR_LOAD_PEDS - Dad......................: ", NATIVE_TO_INT(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAD]))
						PRINTLN("[CHARCREATOR X] CHAR_CREATOR_LOAD_PEDS - Mum......................: ", NATIVE_TO_INT(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_MUM]))
						PRINTLN("[CHARCREATOR X] CHAR_CREATOR_LOAD_PEDS - Grandma (Mum)............: ", NATIVE_TO_INT(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_GRANDMA_M]))
						PRINTLN("[CHARCREATOR X] CHAR_CREATOR_LOAD_PEDS - Grandad (Mum)............: ", NATIVE_TO_INT(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_GRANDDAD_M]))
						PRINTLN("[CHARCREATOR X] CHAR_CREATOR_LOAD_PEDS - Grandma (Dad)............: ", NATIVE_TO_INT(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_GRANDMA_D]))
						PRINTLN("[CHARCREATOR X] CHAR_CREATOR_LOAD_PEDS - Grandad (Dad)............: ", NATIVE_TO_INT(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_GRANDDAD_D]))
						PRINTLN("[CHARCREATOR X] CHAR_CREATOR_LOAD_PEDS - Son Dummy................: ", NATIVE_TO_INT(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON_DUMMY]))
						PRINTLN("[CHARCREATOR X] CHAR_CREATOR_LOAD_PEDS - Daughter Dummy...........: ", NATIVE_TO_INT(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER_DUMMY]))
						
						
						GIVE_CHAR_CREATOR_PED_HAIR(charCreatorData, CHAR_CREATOR_PED_SON_DUMMY, IS_CHAR_CREATOR_PED_MALE(charCreatorData))
						GIVE_CHAR_CREATOR_PED_HAIR(charCreatorData, CHAR_CREATOR_PED_DAUGHTER_DUMMY, NOT IS_CHAR_CREATOR_PED_MALE(charCreatorData))	
						
						// initial parent hair
						//SET_PED_COMP_ITEM_CURRENT_MP(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAD], COMP_TYPE_HAIR, INT_TO_ENUM(PED_COMP_NAME_ENUM, charCreatorData.pedData.charCreatorParentInfo.iDadHair), FALSE, DEFAULT, DEFAULT, DEFAULT, TRUE) // True here to skip hair overlays for parents
						//SET_PED_COMP_ITEM_CURRENT_MP(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_MUM], COMP_TYPE_HAIR, INT_TO_ENUM(PED_COMP_NAME_ENUM, charCreatorData.pedData.charCreatorParentInfo.iMumHair), FALSE, DEFAULT, DEFAULT, DEFAULT, TRUE) // True here to skip hair overlays for parents				
						
						PROCESS_CHAR_CREATOR_CHILD_OVERLAYS(charCreatorData)
						
						SET_WAIT_TO_CLEAR_SPINNERS(charCreatorData)		
						SET_BIT(charCreatorData.iCharCreatorBitset, CHAR_CREATOR_ALL_PEDS_LOADED)
				
					#IF IS_DEBUG_BUILD
					ELSE
						IF (GET_GAME_TIMER() % 1000 < 75)
							PRINTLN("[CHARCREATOR] CHAR_CREATOR_LOAD_PEDS - Waiting for child blends to finish")
						ENDIF
					#ENDIF	
				
					ENDIF
				
				BREAK
			ENDSWITCH
		ENDIF
	ENDIF

ENDPROC


/* **********************************************
*				CHARACTER STATS
*************************************************/

#IF IS_DEBUG_BUILD
	FUNC STRING GET_CHAR_CREATOR_STAT_DESC(CHAR_CREATOR_STATS aStat)
		SWITCH aStat
			CASE CHAR_CREATOR_STAT_STAMINA		RETURN "CHAR_CREATOR_STAT_STAMINA"
			CASE CHAR_CREATOR_STAT_SHOOTING		RETURN "CHAR_CREATOR_STAT_SHOOTING"
			CASE CHAR_CREATOR_STAT_STRENGTH		RETURN "CHAR_CREATOR_STAT_STRENGTH"
			CASE CHAR_CREATOR_STAT_STEALTH		RETURN "CHAR_CREATOR_STAT_STEALTH"
			CASE CHAR_CREATOR_STAT_FLYING		RETURN "CHAR_CREATOR_STAT_FLYING"
			CASE CHAR_CREATOR_STAT_BIKE			RETURN "CHAR_CREATOR_STAT_BIKE"
			CASE CHAR_CREATOR_STAT_LUNG			RETURN "CHAR_CREATOR_STAT_LUNG"
		ENDSWITCH
		
		RETURN "NONE"
	ENDFUNC
#ENDIF

// Saves the stats of created character
PROC CHAR_CREATOR_SAVE_PLAYER_STATS(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData)
	
	PRINTLN("[CHARCREATOR] Saving final stats for character creation")
	PRINTLN("[CHARCREATOR] CHAR_CREATOR_STAT_STAMINA: ", charCreatorData.pedData.charLifestyleStats[CHAR_CREATOR_STAT_STAMINA])
	PRINTLN("[CHARCREATOR] CHAR_CREATOR_STAT_SHOOTING: ", charCreatorData.pedData.charLifestyleStats[CHAR_CREATOR_STAT_SHOOTING])
	PRINTLN("[CHARCREATOR] CHAR_CREATOR_STAT_STRENGTH: ", charCreatorData.pedData.charLifestyleStats[CHAR_CREATOR_STAT_STRENGTH])
	PRINTLN("[CHARCREATOR] CHAR_CREATOR_STAT_STEALTH: ", charCreatorData.pedData.charLifestyleStats[CHAR_CREATOR_STAT_STEALTH])
	PRINTLN("[CHARCREATOR] CHAR_CREATOR_STAT_FLYING: ", charCreatorData.pedData.charLifestyleStats[CHAR_CREATOR_STAT_FLYING])
	PRINTLN("[CHARCREATOR] CHAR_CREATOR_STAT_BIKE: ", charCreatorData.pedData.charLifestyleStats[CHAR_CREATOR_STAT_BIKE])
	PRINTLN("[CHARCREATOR] CHAR_CREATOR_STAT_LUNG: ", charCreatorData.pedData.charLifestyleStats[CHAR_CREATOR_STAT_LUNG])
		
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CREATOR_STAMINA, charCreatorData.pedData.charLifestyleStats[CHAR_CREATOR_STAT_STAMINA], charCreatorData.iCharacterSelectSlot)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CREATOR_SHOOTING, charCreatorData.pedData.charLifestyleStats[CHAR_CREATOR_STAT_SHOOTING], charCreatorData.iCharacterSelectSlot)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CREATOR_STRENGTH, charCreatorData.pedData.charLifestyleStats[CHAR_CREATOR_STAT_STRENGTH], charCreatorData.iCharacterSelectSlot)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CREATOR_STEALTH, charCreatorData.pedData.charLifestyleStats[CHAR_CREATOR_STAT_STEALTH], charCreatorData.iCharacterSelectSlot)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CREATOR_FLYING, charCreatorData.pedData.charLifestyleStats[CHAR_CREATOR_STAT_FLYING], charCreatorData.iCharacterSelectSlot)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CREATOR_BIKE, charCreatorData.pedData.charLifestyleStats[CHAR_CREATOR_STAT_BIKE], charCreatorData.iCharacterSelectSlot)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CREATOR_LUNG, charCreatorData.pedData.charLifestyleStats[CHAR_CREATOR_STAT_LUNG], charCreatorData.iCharacterSelectSlot)

ENDPROC

/// PURPOSE: This gets the player stat upgrades so they can be applied to an already saved character
PROC CHAR_CREATOR_GET_PLAYER_STAT_UPGRADES(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData)
	
	charCreatorData.pedData.iCurrentStatUpgrades[CHAR_CREATOR_STAT_STAMINA] 	= CALCULATE_PLAYER_STAT_VALUE(CHAR_MULTIPLAYER, PS_STAMINA, FALSE, charCreatorData.iCharacterSelectSlot)
	charCreatorData.pedData.iCurrentStatUpgrades[CHAR_CREATOR_STAT_SHOOTING] 	= CALCULATE_PLAYER_STAT_VALUE(CHAR_MULTIPLAYER, PS_SHOOTING_ABILITY, FALSE, charCreatorData.iCharacterSelectSlot)
	charCreatorData.pedData.iCurrentStatUpgrades[CHAR_CREATOR_STAT_STRENGTH] 	= CALCULATE_PLAYER_STAT_VALUE(CHAR_MULTIPLAYER, PS_STRENGTH, FALSE, charCreatorData.iCharacterSelectSlot)
	charCreatorData.pedData.iCurrentStatUpgrades[CHAR_CREATOR_STAT_STEALTH] 	= CALCULATE_PLAYER_STAT_VALUE(CHAR_MULTIPLAYER, PS_STEALTH_ABILITY, FALSE, charCreatorData.iCharacterSelectSlot)
	charCreatorData.pedData.iCurrentStatUpgrades[CHAR_CREATOR_STAT_FLYING] 		= CALCULATE_PLAYER_STAT_VALUE(CHAR_MULTIPLAYER, PS_FLYING_ABILITY, FALSE, charCreatorData.iCharacterSelectSlot)
	charCreatorData.pedData.iCurrentStatUpgrades[CHAR_CREATOR_STAT_BIKE] 		= CALCULATE_PLAYER_STAT_VALUE(CHAR_MULTIPLAYER, PS_DRIVING_ABILITY, FALSE, charCreatorData.iCharacterSelectSlot)
	charCreatorData.pedData.iCurrentStatUpgrades[CHAR_CREATOR_STAT_LUNG] 		= CALCULATE_PLAYER_STAT_VALUE(CHAR_MULTIPLAYER, PS_LUNG_CAPACITY, FALSE, charCreatorData.iCharacterSelectSlot)

	SET_BIT(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_CALC_STAT_UPGRADES)

	#IF IS_DEBUG_BUILD
	// Print out the upgrades
	INT i
	REPEAT CHAR_CREATOR_STAT_TOTAL i
		PRINTLN("[CHARCREATOR] CHAR_CREATOR_GET_PLAYER_STAT_UPGRADES: ", GET_CHAR_CREATOR_STAT_DESC(INT_TO_ENUM(CHAR_CREATOR_STATS, i)), " = ", charCreatorData.pedData.iCurrentStatUpgrades[i])
	ENDREPEAT
	#ENDIF
ENDPROC

PROC APPLY_CHAR_CREATOR_STAT_UPGRADES(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData)

	// First iteration we need to calculate the actual upgrades
//	IF IS_BIT_SET(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_CALC_STAT_UPGRADES)
//		PRINTLN("[CHARCREATOR] APPLY_CHAR_CREATOR_STAT_UPGRADES - ")
//		CLEAR_BIT(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_CALC_STAT_UPGRADES)
//		
//		INT i
//		REPEAT CHAR_CREATOR_STAT_TOTAL i
//		
//			PRINTLN("[CHARCREATOR] Stat ", GET_CHAR_CREATOR_STAT_DESC(INT_TO_ENUM(CHAR_CREATOR_STATS, i)), "  value before: ", charCreatorData.pedData.iCurrentStatUpgrades[i])
//			PRINTLN("[CHARCREATOR] Stat ", GET_CHAR_CREATOR_STAT_DESC(INT_TO_ENUM(CHAR_CREATOR_STATS, i)), "  value in creator: ", charCreatorData.pedData.charLifestyleStats[i])
//		
//			charCreatorData.pedData.iCurrentStatUpgrades[i] = (charCreatorData.pedData.iCurrentStatUpgrades[i] - charCreatorData.pedData.charLifestyleStats[i])
//			PRINTLN("[CHARCREATOR] Stat ", GET_CHAR_CREATOR_STAT_DESC(INT_TO_ENUM(CHAR_CREATOR_STATS, i)), " = ", charCreatorData.pedData.iCurrentStatUpgrades[i])
//		ENDREPEAT
//					
//	ENDIF

	// Apply these updgrades to the stats (limiting to bounds)
	INT i
	REPEAT CHAR_CREATOR_STAT_TOTAL i
		// We now lock out the stats of a player to what they currently have
		IF HAS_ENTERED_CHAR_CREATION_FROM_ALTERATION_PROMPT()
			PRINTLN("[CHARCREATOR] Correct Stat (editing appearance): ", GET_CHAR_CREATOR_STAT_DESC(INT_TO_ENUM(CHAR_CREATOR_STATS, i)))
			charCreatorData.pedData.charLifestyleStats[i] = charCreatorData.pedData.iCurrentStatUpgrades[i]
		ENDIF
		
		IF charCreatorData.pedData.charLifestyleStats[i] < 0
			charCreatorData.pedData.charLifestyleStats[i] = 0
		ENDIF
		
		IF charCreatorData.pedData.charLifestyleStats[i] > 100
			charCreatorData.pedData.charLifestyleStats[i] = 100
		ENDIF
		
		PRINTLN("[CHARCREATOR] Corrected Stat ", GET_CHAR_CREATOR_STAT_DESC(INT_TO_ENUM(CHAR_CREATOR_STATS, i)), " = ", charCreatorData.pedData.charLifestyleStats[i])
	ENDREPEAT
	
ENDPROC

/// PURPOSE: Returns the maximum percentage for a character stat
FUNC INT GET_CHAR_CREATOR_MAX_STAT_VALUE(CHAR_CREATOR_STATS aCharacterStat)

	SWITCH aCharacterStat
		CASE CHAR_CREATOR_STAT_STAMINA		RETURN 30
		CASE CHAR_CREATOR_STAT_SHOOTING		RETURN 20
		CASE CHAR_CREATOR_STAT_STRENGTH		RETURN 40
		CASE CHAR_CREATOR_STAT_STEALTH		RETURN 25
		CASE CHAR_CREATOR_STAT_FLYING		RETURN 60
		CASE CHAR_CREATOR_STAT_BIKE			RETURN 50		// bug 1045884
		CASE CHAR_CREATOR_STAT_LUNG			RETURN 40
	ENDSWITCH

	RETURN 0
ENDFUNC

/// PURPOSE: Returns the minimum value the stat can have
FUNC INT GET_CHAR_CREATOR_MIN_STAT_VALUE(CHAR_CREATOR_STATS aCharacterStat)

	SWITCH aCharacterStat
		
		CASE CHAR_CREATOR_STAT_SHOOTING		RETURN 10
	
	ENDSWITCH

	RETURN 5
ENDFUNC

/// PURPOSE: Returns TRUE if the stat is affected by the lifestyle hour
FUNC BOOL IS_CHAR_CREATOR_STAT_CHANGED_BY_LIFESTYLE(CHAR_CREATOR_LIFESTYLE aLifestyle, CHAR_CREATOR_STATS aCharStat, BOOL &bIncreaseVal)  //BARVALUEINDEX lifestyle, MP_CHARACTER_CREATOR_STATS aStat, BOOL &bIncreaseVal)

	SWITCH aLifestyle
		
		CASE LIFESTYLE_SLEEP
			SWITCH aCharStat
				CASE CHAR_CREATOR_STAT_STAMINA				
				CASE CHAR_CREATOR_STAT_STRENGTH
				CASE CHAR_CREATOR_STAT_STEALTH
				CASE CHAR_CREATOR_STAT_LUNG
					bIncreaseVal = TRUE
					RETURN TRUE
			ENDSWITCH
		BREAK
		
		CASE LIFESTYLE_FAMILY
			SWITCH aCharStat
				CASE CHAR_CREATOR_STAT_BIKE
				CASE CHAR_CREATOR_STAT_STAMINA
					bIncreaseVal = TRUE
					RETURN TRUE
				CASE CHAR_CREATOR_STAT_SHOOTING
					bIncreaseVal = FALSE
					RETURN TRUE
			ENDSWITCH
		BREAK
	
		CASE LIFESTYLE_SPORT
			SWITCH aCharStat
				CASE CHAR_CREATOR_STAT_STRENGTH
				CASE CHAR_CREATOR_STAT_STAMINA
				CASE CHAR_CREATOR_STAT_LUNG
					bIncreaseVal = TRUE
					RETURN TRUE
				CASE CHAR_CREATOR_STAT_BIKE
				CASE CHAR_CREATOR_STAT_SHOOTING
					bIncreaseVal = FALSE
					RETURN TRUE
			ENDSWITCH
		BREAK
	
		CASE LIFESTYLE_LEGAL
			SWITCH aCharStat
				CASE CHAR_CREATOR_STAT_STEALTH
				CASE CHAR_CREATOR_STAT_FLYING
				CASE CHAR_CREATOR_STAT_STAMINA
					bIncreaseVal = TRUE
					RETURN TRUE
				CASE CHAR_CREATOR_STAT_BIKE
					bIncreaseVal = FALSE
					RETURN TRUE
			ENDSWITCH
		BREAK
		
		CASE LIFESTYLE_TV
			SWITCH aCharStat
				CASE CHAR_CREATOR_STAT_FLYING
				CASE CHAR_CREATOR_STAT_BIKE
					bIncreaseVal = TRUE
					RETURN TRUE
				CASE CHAR_CREATOR_STAT_STRENGTH
				CASE CHAR_CREATOR_STAT_LUNG
				CASE CHAR_CREATOR_STAT_STAMINA
					bIncreaseVal = FALSE
					RETURN TRUE
			ENDSWITCH
		BREAK
		
		CASE LIFESTYLE_PARTY
			SWITCH aCharStat
				CASE CHAR_CREATOR_STAT_SHOOTING
				CASE CHAR_CREATOR_STAT_STEALTH
					bIncreaseVal = TRUE
					RETURN TRUE
				CASE CHAR_CREATOR_STAT_LUNG
				CASE CHAR_CREATOR_STAT_STAMINA
				CASE CHAR_CREATOR_STAT_STRENGTH
				CASE CHAR_CREATOR_STAT_FLYING
					bIncreaseVal = FALSE
					RETURN TRUE
			ENDSWITCH
		BREAK
		
		CASE LIFESTYLE_ILLEGAL
			SWITCH aCharStat
				CASE CHAR_CREATOR_STAT_LUNG
					bIncreaseVal = FALSE
					RETURN TRUE
				CASE CHAR_CREATOR_STAT_SHOOTING
				CASE CHAR_CREATOR_STAT_BIKE
					bIncreaseVal = TRUE
					RETURN TRUE
			ENDSWITCH
		BREAK
	
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

// Get total number of hours 
FUNC INT GET_CHAR_CREATOR_TOTAL_HOURS_FOR_STAT(CHAR_CREATOR_STATS aStat)
	SWITCH aStat
		CASE CHAR_CREATOR_STAT_STAMINA		RETURN (ciCHAR_PARTY_MAX + ciCHAR_SLEEP_MAX + ciCHAR_LEGAL_MAX + ciCHAR_FAMILY_MAX + ciCHAR_SPORT_MAX + ciCHAR_TV_MAX)
		CASE CHAR_CREATOR_STAT_SHOOTING		RETURN (ciCHAR_ILLEGAL_MAX + ciCHAR_FAMILY_MAX + ciCHAR_SPORT_MAX + ciCHAR_PARTY_MAX)
		CASE CHAR_CREATOR_STAT_STRENGTH		RETURN (ciCHAR_PARTY_MAX + ciCHAR_SLEEP_MAX + ciCHAR_SPORT_MAX + ciCHAR_TV_MAX)
		CASE CHAR_CREATOR_STAT_STEALTH		RETURN (ciCHAR_SLEEP_MAX + ciCHAR_LEGAL_MAX + ciCHAR_PARTY_MAX)
		CASE CHAR_CREATOR_STAT_FLYING		RETURN (ciCHAR_PARTY_MAX + ciCHAR_LEGAL_MAX + ciCHAR_TV_MAX)
		CASE CHAR_CREATOR_STAT_BIKE			RETURN (ciCHAR_ILLEGAL_MAX + ciCHAR_FAMILY_MAX + ciCHAR_TV_MAX + ciCHAR_LEGAL_MAX)
		CASE CHAR_CREATOR_STAT_LUNG			RETURN (ciCHAR_PARTY_MAX + ciCHAR_SLEEP_MAX + ciCHAR_ILLEGAL_MAX + ciCHAR_SPORT_MAX + ciCHAR_TV_MAX)
	ENDSWITCH
	
	RETURN 0
ENDFUNC

/// PURPOSE: Passes stat data to scaleform
PROC SET_CHAR_CREATOR_STAT_BARS(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData, INT iBitSet)

	IF BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND("SET_DESCRIPTION")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(CHAR_CREATOR_COL_PLAYER)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iBitSet)
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(TO_FLOAT(charCreatorData.pedData.charLifestyleStats[0]) / 100)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(TO_FLOAT(charCreatorData.pedData.charLifestyleStats[1]) / 100)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(TO_FLOAT(charCreatorData.pedData.charLifestyleStats[2]) / 100)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(TO_FLOAT(charCreatorData.pedData.charLifestyleStats[3]) / 100)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(TO_FLOAT(charCreatorData.pedData.charLifestyleStats[4]) / 100)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(TO_FLOAT(charCreatorData.pedData.charLifestyleStats[5]) / 100)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(TO_FLOAT(charCreatorData.pedData.charLifestyleStats[6]) / 100)
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0.0)
		
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC

PROC HIDE_CHAR_CREATOR_STAT_BARS()

	IF BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND("SET_DESCRIPTION")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(CHAR_CREATOR_COL_PLAYER)		
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC

// Loops over each character stat and updates value
PROC SET_CHAR_CREATOR_STATS(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData, BOOL bUpdate = FALSE)
	
	INT iStat
	INT iLifestyle
	INT iBitSetBars
	FLOAT fStatSplit
	FLOAT fStatTotal
	BOOL bStatIncreases
	
	CHAR_CREATOR_STATS statToUpdate
	CHAR_CREATOR_LIFESTYLE aLifestyle
		
	PRINTLN("[CHARCREATOR] SET_CHAR_CREATOR_STATS - bUpdate: ", PICK_STRING(bUpdate, "TRUE", "FALSE"))
	
	REPEAT CHAR_CREATOR_STAT_TOTAL iStat
		
		statToUpdate = INT_TO_ENUM(CHAR_CREATOR_STATS, iStat)
		fStatTotal = 0
		
		REPEAT MAX_LIFESTYLE iLifestyle
		
			aLifestyle = INT_TO_ENUM(CHAR_CREATOR_LIFESTYLE, iLifestyle)
		
			IF IS_CHAR_CREATOR_STAT_CHANGED_BY_LIFESTYLE(aLifestyle, statToUpdate, bStatIncreases)
		
				fStatSplit = (TO_FLOAT(GET_CHAR_CREATOR_MAX_STAT_VALUE(statToUpdate) - GET_CHAR_CREATOR_MIN_STAT_VALUE(statToUpdate)) / TO_FLOAT(GET_CHAR_CREATOR_TOTAL_HOURS_FOR_STAT(statToUpdate)))
				fStatSplit = (fStatSplit * GET_CHAR_CREATOR_HOURS_BOUNDS_FOR_LIFESTYLE(aLifestyle, TRUE))
				
				PRINTLN("					Lifestyle: ", GET_STRING_FROM_TEXT_FILE(GET_CHAR_CREATOR_LIFESTYLE_TEXT_LABEL(aLifestyle)), " Value: ", GET_CHAR_CREATOR_LIFESTYLE_PERCENTAGE(charCreatorData, aLifestyle))
				
				IF bStatIncreases
					fStatTotal += (GET_CHAR_CREATOR_LIFESTYLE_PERCENTAGE(charCreatorData, aLifestyle) * fStatSplit)
				ELSE
					fStatTotal += ((1 - GET_CHAR_CREATOR_LIFESTYLE_PERCENTAGE(charCreatorData, aLifestyle)) * fStatSplit)
				ENDIF		
						
				IF aLifestyle = charCreatorData.lifestyleIndex
					SET_BIT(iBitSetBars, iStat)
				ENDIF
			ENDIF
		
		ENDREPEAT
		
		charCreatorData.pedData.charLifestyleStats[iStat] = GET_CHAR_CREATOR_MIN_STAT_VALUE(statToUpdate) + ROUND(fStatTotal)
				
		PRINTLN("					Stat ", GET_CHAR_CREATOR_STAT_DESC(statToUpdate), " = ", charCreatorData.pedData.charLifestyleStats[iStat])
	ENDREPEAT
	
	PRINTLN("					iBitSetBars ", iBitSetBars)
	
	APPLY_CHAR_CREATOR_STAT_UPGRADES(charCreatorData)
	
	charCreatorData.iCharCreatorStatBitSet = iBitSetBars
	
	IF charCreatorData.mainMenuIndex = LIFESTYLE_CAT
		IF bUpdate
			SET_CHAR_CREATOR_STAT_BARS(charCreatorData, charCreatorData.iCharCreatorStatBitSet)
		ELSE
			SET_CHAR_CREATOR_STAT_BARS(charCreatorData, 0)
		ENDIF
	ENDIF
	
ENDPROC

/* **********************************************
*			   CONFIRMATION SCREEN
*************************************************/

/// PURPOSE: Updates the scaleform middle column based on category selected on left
PROC SET_CHAR_CREATOR_COLUMNS_TO_SELECTION(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData)

	SWITCH charCreatorData.mainMenuIndex
		CASE HERITAGE_CAT
			SET_FRONTEND_DISPLAY_COLUMN(CHAR_CREATOR_COL_HERITAGE, TRUE)
			SET_FRONTEND_DISPLAY_COLUMN(CHAR_CREATOR_COL_LIFESTYLE, FALSE)
			SET_FRONTEND_DISPLAY_COLUMN(CHAR_CREATOR_COL_APP, FALSE)
			
			IF NOT ARE_ALL_CHAR_CREATOR_PEDS_LOADED(charCreatorData)
				SET_CHAR_CREATOR_PARENTS_AS_BUSY(charCreatorData, TRUE)
			ENDIF
			
			HIDE_CHAR_CREATOR_STAT_BARS()
		BREAK
		
		CASE LIFESTYLE_CAT
		
			// Remove our parents
			UI3DSCENE_CLEAR_PATCHED_DATA()
		
			SET_FRONTEND_DISPLAY_COLUMN(CHAR_CREATOR_COL_HERITAGE, FALSE)
			SET_FRONTEND_DISPLAY_COLUMN(CHAR_CREATOR_COL_LIFESTYLE, TRUE)
			SET_FRONTEND_DISPLAY_COLUMN(CHAR_CREATOR_COL_APP, FALSE)
			
			SET_CHAR_CREATOR_PARENTS_AS_BUSY(charCreatorData, FALSE)
			
			SET_CHAR_CREATOR_STAT_BARS(charCreatorData, charCreatorData.iCharCreatorStatBitSet)
		BREAK
		
		CASE APPEARANCE_CAT
		
			// Remove our parents
			UI3DSCENE_CLEAR_PATCHED_DATA()
		
			SET_FRONTEND_DISPLAY_COLUMN(CHAR_CREATOR_COL_HERITAGE, FALSE)
			SET_FRONTEND_DISPLAY_COLUMN(CHAR_CREATOR_COL_LIFESTYLE, FALSE)
			SET_FRONTEND_DISPLAY_COLUMN(CHAR_CREATOR_COL_APP, TRUE)
			
			SET_CHAR_CREATOR_PARENTS_AS_BUSY(charCreatorData, FALSE)
			
			HIDE_CHAR_CREATOR_STAT_BARS()
		BREAK
				
		DEFAULT		
			IF charCreatorData.prevMainMenuIndex = LIFESTYLE_CAT
				SET_FRONTEND_DISPLAY_COLUMN(CHAR_CREATOR_COL_LIFESTYLE, TRUE)
			ELSE
				SET_FRONTEND_DISPLAY_COLUMN(CHAR_CREATOR_COL_APP, TRUE)
			ENDIF
			
			HIDE_CHAR_CREATOR_STAT_BARS()
		BREAK
				
	ENDSWITCH

ENDPROC

PROC DRAW_CHAR_CREATOR_CONFIRM_SCALEFORM(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData)
	//SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
	//SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_FADE)
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD)
	DRAW_SCALEFORM_MOVIE_FULLSCREEN_MASKED(charCreatorData.sfForeground, charCreatorData.sfBackground, 255, 255, 255, 255)
	
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_FADE)
	DRAW_SCALEFORM_MOVIE_FULLSCREEN(charCreatorData.sfCelebration, 255, 255, 255, 255)
	
	//SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(FALSE)
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
ENDPROC

PROC UNPAUSE_CHAR_CREATOR_SCALEFORM(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData)

	IF HAS_SCALEFORM_MOVIE_LOADED(charCreatorData.sfCelebration)
	
		BEGIN_SCALEFORM_MOVIE_METHOD(charCreatorData.sfCelebration, "UNPAUSE")
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
				ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("intro")
			END_TEXT_COMMAND_SCALEFORM_STRING()
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
	
	IF HAS_SCALEFORM_MOVIE_LOADED(charCreatorData.sfForeground)
		BEGIN_SCALEFORM_MOVIE_METHOD(charCreatorData.sfForeground, "UNPAUSE")
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
				ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("intro")
			END_TEXT_COMMAND_SCALEFORM_STRING()
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
	
	IF HAS_SCALEFORM_MOVIE_LOADED(charCreatorData.sfBackground)
		BEGIN_SCALEFORM_MOVIE_METHOD(charCreatorData.sfBackground, "UNPAUSE")
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
				ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("intro")
			END_TEXT_COMMAND_SCALEFORM_STRING()
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC

FUNC BOOL MAINTAIN_CHAR_CREATOR_CONFIRM_SCREEN(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData, MPHUD_PLACEMENT_TOOLS& Placement)
	IF NOT IS_BIT_SET(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_WAIT_TO_ENTER_GAME)
		IF NOT IS_BIT_SET(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_WAIT_TO_BACK_OUT_OF_CONFIRM)
			// Listen for the player moving into GTA
			IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)

				IF NOT IS_BIT_SET(charCreatorData.iCharCreatorButtons, CHAR_CREATOR_ACCEPT)
				
					SET_BIT(charCreatorData.iCharCreatorButtons, CHAR_CREATOR_ACCEPT)
					
					PRINTLN("[CHARCREATOR] MAINTAIN_CHAR_CREATOR_CONFIRM_SCREEN - player confirmed on join screen: Joining GTA Online")
				
					PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
					
					UNPAUSE_CHAR_CREATOR_SCALEFORM(charCreatorData)
					
					charCreatorData.iUnpauseTime = GET_GAME_TIMER()
					SET_BIT(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_WAIT_TO_ENTER_GAME)

				ENDIF
			ELSE
				IF IS_BIT_SET(charCreatorData.iCharCreatorButtons, CHAR_CREATOR_ACCEPT)
					CLEAR_BIT(charCreatorData.iCharCreatorButtons, CHAR_CREATOR_ACCEPT)
				ENDIF
			ENDIF
		
		
			// Listen for the player moving back to creator
			IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
				IF NOT IS_BIT_SET(charCreatorData.iCharCreatorButtons, CHAR_CREATOR_CANCEL)
				AND NOT IS_BIT_SET(charCreatorData.iCharCreatorButtons, CHAR_CREATOR_ACCEPT)
				
					SET_BIT(charCreatorData.iCharCreatorButtons, CHAR_CREATOR_CANCEL)
				
					PRINTLN("[CHARCREATOR] MAINTAIN_CHAR_CREATOR_CONFIRM_SCREEN - player cancelled on join screen")
				
					PLAY_SOUND_FRONTEND(-1, "BACK", "HUD_FRONTEND_DEFAULT_SOUNDSET")

					PRINTLN("[CHARCREATOR] PROCESS_XML_MPHUD_CHARACTER_CREATOR - Returning to character creator")
					
					UNPAUSE_CHAR_CREATOR_SCALEFORM(charCreatorData)
					
					charCreatorData.iUnpauseTime = GET_GAME_TIMER()
					
					SET_BIT(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_WAIT_TO_BACK_OUT_OF_CONFIRM)
					CLEAR_BIT(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_WAIT_TO_BACK_OUT_OF_CONFIRM_FRAME)
				ENDIF
			ELSE
				IF IS_BIT_SET(charCreatorData.iCharCreatorButtons, CHAR_CREATOR_CANCEL)
					CLEAR_BIT(charCreatorData.iCharCreatorButtons, CHAR_CREATOR_CANCEL)
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_BIT_SET(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_WAIT_TO_BACK_OUT_OF_CONFIRM_FRAME)
				IF GET_GAME_TIMER() >= charCreatorData.iUnpauseTime + CHAR_CREATOR_UNPAUSE_TIME
					SET_BIT(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_WAIT_TO_BACK_OUT_OF_CONFIRM_FRAME)
					
					charCreatorData.fCharZoom = charCreatorData.fCacheCharZoom
					PROCESS_CHAR_CREATOR_ZOOM(charCreatorData, 0)					
				ENDIF
			ELSE
				
				charCreatorData.charCreatorState = CHARACTER_CREATOR_RUN
				charCreatorData.charCreatorConfirmState = CHARACTER_CREATOR_CONFIRM_LOADING_UI

				UI3DSCENE_CLEAR_PATCHED_DATA()
				
				SET_CHAR_CREATOR_COLUMNS_TO_SELECTION(charCreatorData)
				SET_FRONTEND_DISPLAY_COLUMN(CHAR_CREATOR_COL_MAIN_MENU, TRUE)
				SET_FRONTEND_DISPLAY_COLUMN(CHAR_CREATOR_COL_PLAYER, TRUE)
				
				IF BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND_HEADER("SHOW_MENU")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
					END_SCALEFORM_MOVIE_METHOD()
				ENDIF
				
				IF BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND_HEADER("SHOW_HEADING_DETAILS")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
					END_SCALEFORM_MOVIE_METHOD()
				ENDIF
				
				IF BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND_HEADER("SET_HEADER_TITLE")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("HUD_MAINTIT")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")
					END_SCALEFORM_MOVIE_METHOD()
				ENDIF
				
				PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("CONFIRM_PED"))
				PAUSE_MENU_ACTIVATE_CONTEXT(HASH("CONTINUE_OPTION"))
				PAUSE_MENU_REDRAW_INSTRUCTIONAL_BUTTONS()
				
				IF HAS_SCALEFORM_MOVIE_LOADED(charCreatorData.sfForeground)
					SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(charCreatorData.sfForeground)
				ENDIF
				IF HAS_SCALEFORM_MOVIE_LOADED(charCreatorData.sfBackground)
					SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(charCreatorData.sfBackground)
				ENDIF
				IF HAS_SCALEFORM_MOVIE_LOADED(charCreatorData.sfCelebration)
					SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(charCreatorData.sfCelebration)
				ENDIF
				
				CLEAR_BIT(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_WAIT_TO_BACK_OUT_OF_CONFIRM)
				CLEAR_BIT(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_WAIT_TO_BACK_OUT_OF_CONFIRM_FRAME)
				
				RETURN FALSE

			ENDIF
		ENDIF
	ELSE
//		IF IS_SCREEN_FADED_IN()
//			DO_SCREEN_FADE_OUT(0)
//		ENDIF
		
		IF GET_GAME_TIMER() >= charCreatorData.iUnpauseTime + CHAR_CREATOR_UNPAUSE_TIME
			IF HAS_CHANGED_MAP_OVER_TO_MP(Placement, TRUE)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	// Handle the loading of our scaleform
	SWITCH charCreatorData.charCreatorConfirmState
		CASE CHARACTER_CREATOR_CONFIRM_LOADING_UI
			
			charCreatorData.sfForeground = REQUEST_SCALEFORM_MOVIE("MP_CELEBRATION_BG")
			charCreatorData.sfBackground = REQUEST_SCALEFORM_MOVIE("MP_CELEBRATION_FG")
			charCreatorData.sfCelebration = REQUEST_SCALEFORM_MOVIE("MP_CELEBRATION")
			
			IF HAS_SCALEFORM_MOVIE_LOADED(charCreatorData.sfForeground)
			AND HAS_SCALEFORM_MOVIE_LOADED(charCreatorData.sfBackground)
			AND HAS_SCALEFORM_MOVIE_LOADED(charCreatorData.sfCelebration)
				
					BEGIN_SCALEFORM_MOVIE_METHOD(charCreatorData.sfForeground, "CREATE_STAT_WALL")
						BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
							ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("intro")
						END_TEXT_COMMAND_SCALEFORM_STRING()
						
						BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
							ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("HUD_COLOUR_BLACK")
						END_TEXT_COMMAND_SCALEFORM_STRING()						
					END_SCALEFORM_MOVIE_METHOD()
					
					BEGIN_SCALEFORM_MOVIE_METHOD(charCreatorData.sfBackground, "CREATE_STAT_WALL")
						BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
							ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("intro")
						END_TEXT_COMMAND_SCALEFORM_STRING()
						
						BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
							ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("HUD_COLOUR_BLACK")
						END_TEXT_COMMAND_SCALEFORM_STRING()			
					END_SCALEFORM_MOVIE_METHOD()
					
					BEGIN_SCALEFORM_MOVIE_METHOD(charCreatorData.sfCelebration, "CREATE_STAT_WALL")
						BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
							ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("intro")
						END_TEXT_COMMAND_SCALEFORM_STRING()
						
						BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
							ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("HUD_COLOUR_BLACK")
						END_TEXT_COMMAND_SCALEFORM_STRING()			
					END_SCALEFORM_MOVIE_METHOD()
					
					BEGIN_SCALEFORM_MOVIE_METHOD(charCreatorData.sfForeground, "ADD_READY_TO_WALL")
						BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
							ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("intro")
						END_TEXT_COMMAND_SCALEFORM_STRING()
						
						BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
							ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("CELEB_READY")
						END_TEXT_COMMAND_SCALEFORM_STRING()		
					END_SCALEFORM_MOVIE_METHOD()
					
					BEGIN_SCALEFORM_MOVIE_METHOD(charCreatorData.sfBackground, "ADD_READY_TO_WALL")
						BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
							ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("intro")
						END_TEXT_COMMAND_SCALEFORM_STRING()
						
						BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
							ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("CELEB_READY")
						END_TEXT_COMMAND_SCALEFORM_STRING()								
					END_SCALEFORM_MOVIE_METHOD()
					
					BEGIN_SCALEFORM_MOVIE_METHOD(charCreatorData.sfCelebration, "ADD_READY_TO_WALL")
						BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
							ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("intro")
						END_TEXT_COMMAND_SCALEFORM_STRING()
						
						BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
							ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("CELEB_READY")
						END_TEXT_COMMAND_SCALEFORM_STRING()						
					END_SCALEFORM_MOVIE_METHOD()
					
					BEGIN_SCALEFORM_MOVIE_METHOD(charCreatorData.sfForeground, "ADD_BACKGROUND_TO_WALL")
						BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
							ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("intro")
						END_TEXT_COMMAND_SCALEFORM_STRING()
					END_SCALEFORM_MOVIE_METHOD()
					
					BEGIN_SCALEFORM_MOVIE_METHOD(charCreatorData.sfBackground, "ADD_BACKGROUND_TO_WALL")
						BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
							ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("intro")
						END_TEXT_COMMAND_SCALEFORM_STRING()
					END_SCALEFORM_MOVIE_METHOD()
					
					BEGIN_SCALEFORM_MOVIE_METHOD(charCreatorData.sfCelebration, "ADD_BACKGROUND_TO_WALL")
						BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
							ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("intro")
						END_TEXT_COMMAND_SCALEFORM_STRING()
					END_SCALEFORM_MOVIE_METHOD()				
					
					BEGIN_SCALEFORM_MOVIE_METHOD(charCreatorData.sfForeground, "SHOW_STAT_WALL")
						BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
							ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("intro")
						END_TEXT_COMMAND_SCALEFORM_STRING()
					END_SCALEFORM_MOVIE_METHOD()
					
					BEGIN_SCALEFORM_MOVIE_METHOD(charCreatorData.sfBackground, "SHOW_STAT_WALL")
						BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
							ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("intro")
						END_TEXT_COMMAND_SCALEFORM_STRING()
					END_SCALEFORM_MOVIE_METHOD()
					
					BEGIN_SCALEFORM_MOVIE_METHOD(charCreatorData.sfCelebration, "SHOW_STAT_WALL")
						BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
							ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("intro")
						END_TEXT_COMMAND_SCALEFORM_STRING()
					END_SCALEFORM_MOVIE_METHOD()						
								
				
				DRAW_CHAR_CREATOR_CONFIRM_SCALEFORM(charCreatorData)
	
				CharCreatorData.charCreatorConfirmState = CHARACTER_CREATOR_CONFIRM_DISPLAY_UI
			ENDIF
		BREAK
		
		CASE CHARACTER_CREATOR_CONFIRM_DISPLAY_UI
			DRAW_CHAR_CREATOR_CONFIRM_SCALEFORM(charCreatorData)
		BREAK
	ENDSWITCH
	
	RETURN FALSE	
ENDFUNC


#IF IS_DEBUG_BUILD

TWEAK_FLOAT fMumDetX2 0.045
TWEAK_FLOAT fMumDetY2 0.820
TWEAK_FLOAT fDadDetX2 0.045
TWEAK_FLOAT fDadDetY2 0.880

TWEAK_FLOAT fUglyX2 0.045
TWEAK_FLOAT fUglyY2 0.575
TWEAK_FLOAT fWrinklesX2 0.045
TWEAK_FLOAT fWrinklesY2 0.630
TWEAK_FLOAT fDamageX2 0.045
TWEAK_FLOAT fDamageY2 0.685
TWEAK_FLOAT fMakeupX2 0.045
TWEAK_FLOAT fMakeupY2 0.720

TWEAK_FLOAT fBlemishX2 0.045
TWEAK_FLOAT fBlemishY2 0.755

TWEAK_FLOAT fWrinklesPerX 0.045
TWEAK_FLOAT fWrinklesPerY 0.605
TWEAK_FLOAT fDamagePerX 0.045
TWEAK_FLOAT fDamagePerY 0.660

TWEAK_FLOAT fGeomX2 0.045
TWEAK_FLOAT fGeomY2 0.515
TWEAK_FLOAT fGeomX3 0.045
TWEAK_FLOAT fGeomY3 0.540

TWEAK_FLOAT fSonX 0.045
TWEAK_FLOAT fSonY 0.440
TWEAK_FLOAT fSonTimeX 0.130
TWEAK_FLOAT fSonTimeY 0.440

TWEAK_FLOAT fDaughterX 0.045
TWEAK_FLOAT fDaughterY 0.410
TWEAK_FLOAT fDaughterTimeX 0.155
TWEAK_FLOAT fDaughterTimeY 0.410

TWEAK_FLOAT fDadX 0.045
TWEAK_FLOAT fDadY 0.380
TWEAK_FLOAT fDadTimeX 0.130
TWEAK_FLOAT fDadTimeY 0.380

TWEAK_FLOAT fMumX 0.045
TWEAK_FLOAT fMumY 0.350
TWEAK_FLOAT fMumTimeX 0.130
TWEAK_FLOAT fMumTimeY 0.350

TWEAK_FLOAT fMumHeadPX 0.075
TWEAK_FLOAT fMumHeadPY 0.845
TWEAK_FLOAT fMumHeadUX 0.100
TWEAK_FLOAT fMumHeadUY 0.845

TWEAK_FLOAT fDadHeadPX 0.075
TWEAK_FLOAT fDadHeadPY 0.903
TWEAK_FLOAT fDadHeadUX 0.100
TWEAK_FLOAT fDadHeadUY 0.903

TWEAK_FLOAT fLifestyleX 0.045
TWEAK_FLOAT fLifestyleY 0.470


PROC CREATE_CHAR_CREATOR_WIDGETS(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData)
	charCreatorData.widgetGroup = START_WIDGET_GROUP("Character Creation")

		ADD_WIDGET_FLOAT_SLIDER("vMumOffset Offset X", charCreatorData.vMumOffset.x, -10.0, 10.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("vMumOffset Offset Y", charCreatorData.vMumOffset.y, -10.0, 10.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("vMumOffset Offset Z", charCreatorData.vMumOffset.z, -10.0, 10.0, 0.001)
		
		ADD_WIDGET_FLOAT_SLIDER("vDadOffset Offset X", charCreatorData.vDadOffset.x, -10.0, 10.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("vDadOffset Offset Y", charCreatorData.vDadOffset.y, -10.0, 10.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("vDadOffset Offset Z", charCreatorData.vDadOffset.z, -10.0, 10.0, 0.001)
		
		ADD_WIDGET_FLOAT_SLIDER("vMumRot Offset X", charCreatorData.vMumRot.x, -180.0, 180.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("vMumRot Offset Y", charCreatorData.vMumRot.y, -180.0, 180.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("vMumRot Offset Z", charCreatorData.vMumRot.z, -180.0, 180.0, 0.001)
		
		ADD_WIDGET_FLOAT_SLIDER("vDadRot Offset X", charCreatorData.vDadRot.x, -180.0, 180.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("vDadRot Offset Y", charCreatorData.vDadRot.y, -180.0, 180.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("vDadRot Offset Z", charCreatorData.vDadRot.z, -180.0, 180.0, 0.001)

		ADD_WIDGET_FLOAT_SLIDER("CHAR_CREATOR_LOOK_AT_X", CHAR_CREATOR_LOOK_AT_X, -5.0, 5.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("CHAR_CREATOR_LOOK_AT_Y", CHAR_CREATOR_LOOK_AT_Y, -5.0, 5.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("CHAR_CREATOR_LOOK_AT_Z", CHAR_CREATOR_LOOK_AT_Z, -5.0, 5.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("CHAR_CREATOR_LOOK_AT_Z_OFFSET", CHAR_CREATOR_LOOK_AT_Z_OFFSET, -5.0, 5.0, 0.001)

		ADD_WIDGET_BOOL("bFakeCollectorEdition", charCreatorData.bFakeCollectorEdition)		
		ADD_WIDGET_BOOL("bFakeSocialClub", charCreatorData.bFakeSocialClub)

		ADD_WIDGET_FLOAT_SLIDER("Male Parent Dom MIN", CHAR_CREATOR_M_DOM_MIN, 0.0, 1.0, 0.01)
		ADD_WIDGET_FLOAT_SLIDER("Male Parent Dom MAX", CHAR_CREATOR_M_DOM_MAX, 0.0, 1.0, 0.01)
		ADD_WIDGET_FLOAT_SLIDER("Female Parent Dom MIN", CHAR_CREATOR_F_DOM_MIN, 0.0, 1.0, 0.01)
		ADD_WIDGET_FLOAT_SLIDER("Female Parent Dom MAX", CHAR_CREATOR_F_DOM_MAX, 0.0, 1.0, 0.01)
		
		
		ADD_WIDGET_FLOAT_SLIDER("CHAR_CREATOR_HEEL_OFFSET", CHAR_CREATOR_HEEL_OFFSET, -5.0, 5.0, 0.01)
		

		ADD_WIDGET_BOOL("Zoom char out", charCreatorData.bZoomOut)		
		ADD_WIDGET_BOOL("Don't draw peds", charCreatorData.bDontDrawPeds)
		
		ADD_WIDGET_BOOL("Toggle Peds Hair", charCreatorData.bRemovePedsHair)
	
		ADD_WIDGET_BOOL("Debug Info: ", charCreatorData.bDebugInfo)
		ADD_WIDGET_INT_READ_ONLY("Char Bitset: ", charCreatorData.iCharCreatorBitset)
		
		ADD_WIDGET_FLOAT_SLIDER("Child Offset X", charCreatorData.vCharOffset.x, -10.0, 10.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("Child Offset Y", charCreatorData.vCharOffset.y, -10.0, 10.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("Child Offset Z", charCreatorData.vCharOffset.z, -10.0, 10.0, 0.001)
		
		ADD_WIDGET_FLOAT_SLIDER("CHAR_CREATOR_ZOOM_SPEED", CHAR_CREATOR_ZOOM_SPEED, -10.0, 10.0, 0.001)
		
		ADD_WIDGET_FLOAT_SLIDER("CHAR_CREATOR_ZOOM_MAX_X", CHAR_CREATOR_ZOOM_MAX_X, -10.0, 10.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("CHAR_CREATOR_ZOOM_MAX_Y", CHAR_CREATOR_ZOOM_MAX_Y, -10.0, 10.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("CHAR_CREATOR_ZOOM_MAX_Z", CHAR_CREATOR_ZOOM_MAX_Z, -10.0, 10.0, 0.001)
		
		ADD_WIDGET_FLOAT_SLIDER("CHAR_CREATOR_ZOOM_MIN_X", CHAR_CREATOR_ZOOM_MIN_X, -10.0, 10.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("CHAR_CREATOR_ZOOM_MIN_Y", CHAR_CREATOR_ZOOM_MIN_Y, -10.0, 10.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("CHAR_CREATOR_ZOOM_MIN_Z", CHAR_CREATOR_ZOOM_MIN_Z, -10.0, 10.0, 0.001)
		
		ADD_WIDGET_FLOAT_SLIDER("CHAR_CREATOR_CONFIRM_ZOOM_MAX_X", CHAR_CREATOR_CONFIRM_ZOOM_MAX_X, -10.0, 10.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("CHAR_CREATOR_CONFIRM_ZOOM_MAX_Y", CHAR_CREATOR_CONFIRM_ZOOM_MAX_Y, -10.0, 10.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("CHAR_CREATOR_CONFIRM_ZOOM_MAX_Z", CHAR_CREATOR_CONFIRM_ZOOM_MAX_Z, -10.0, 10.0, 0.001)
		
		ADD_WIDGET_FLOAT_SLIDER("CHAR_CREATOR_CONFIRM_ZOOM_MIN_X", CHAR_CREATOR_CONFIRM_ZOOM_MIN_X, -10.0, 10.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("CHAR_CREATOR_CONFIRM_ZOOM_MIN_Y", CHAR_CREATOR_CONFIRM_ZOOM_MIN_Y, -10.0, 10.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("CHAR_CREATOR_CONFIRM_ZOOM_MIN_Z", CHAR_CREATOR_CONFIRM_ZOOM_MIN_Z, -10.0, 10.0, 0.001)		

		START_WIDGET_GROUP("Ped Data")
			ADD_WIDGET_BOOL("Refresh the Parent Head Blend", charCreatorData.bTriggerBlend)
			 ADD_WIDGET_FLOAT_SLIDER("Parent Debug Head Blend", charCreatorData.fDebugHeadBlend, 0.0, 1.0, 0.001)
			ADD_WIDGET_FLOAT_READ_ONLY("Parental Dominance: ", charCreatorData.pedData.fParentDominance)
			ADD_WIDGET_FLOAT_READ_ONLY("Mum Dominance: ", charCreatorData.pedData.fMumDominance)
			ADD_WIDGET_FLOAT_READ_ONLY("Dad Dominance: ", charCreatorData.pedData.fDadDominance)
			ADD_WIDGET_FLOAT_READ_ONLY("Ugly Percentage: ", charCreatorData.pedData.fParentUgly)
			ADD_WIDGET_INT_READ_ONLY("Hours Left to Assign: ", charCreatorData.pedData.iHoursLeftToAssign)
		STOP_WIDGET_GROUP()
		
		ADD_WIDGET_FLOAT_SLIDER("fMumDetX2", fMumDetX2, -10.0, 10.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("fMumDetY2", fMumDetY2, -10.0, 10.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("fDadDetX2", fDadDetX2, -10.0, 10.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("fDadDetY2", fDadDetY2, -10.0, 10.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("fUglyX2", fUglyX2, -10.0, 10.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("fUglyY2", fUglyY2, -10.0, 10.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("fWrinklesX2", fWrinklesX2, -10.0, 10.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("fWrinklesY2", fWrinklesY2, -10.0, 10.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("fDamageX2", fDamageX2, -10.0, 10.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("fDamageY2", fDamageY2, -10.0, 10.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("fMakeupX2", fMakeupX2, -10.0, 10.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("fMakeupY2", fMakeupY2, -10.0, 10.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("fGeomX2", fGeomX2, -10.0, 10.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("fGeomY2", fGeomY2, -10.0, 10.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("fGeomX3", fGeomX3, -10.0, 10.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("fGeomY3", fGeomY3, -10.0, 10.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("fWrinklesPerX", fWrinklesPerX, -10.0, 10.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("fWrinklesPerY", fWrinklesPerY, -10.0, 10.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("fDamagePerX", fDamagePerX, -10.0, 10.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("fDamagePerY", fDamagePerY, -10.0, 10.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("fBlemishX2", fBlemishX2, -10.0, 10.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("fBlemishY2", fBlemishY2, -10.0, 10.0, 0.001)
		
		ADD_WIDGET_FLOAT_SLIDER("fSonX", fSonX, -10.0, 10.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("fSonY", fSonY, -10.0, 10.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("fSonTimeX", fSonTimeX, -10.0, 10.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("fSonTimeY", fSonTimeY, -10.0, 10.0, 0.001)
		
		ADD_WIDGET_FLOAT_SLIDER("fDaughterX", fDaughterX, -10.0, 10.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("fDaughterY", fDaughterY, -10.0, 10.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("fDaughterTimeX", fDaughterTimeX, -10.0, 10.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("fDaughterTimeY", fDaughterTimeY, -10.0, 10.0, 0.001)
		
		ADD_WIDGET_FLOAT_SLIDER("fDadX", fDadX, -10.0, 10.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("fDadY", fDadY, -10.0, 10.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("fDadTimeX", fDadTimeX, -10.0, 10.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("fDadTimeY", fDadTimeY, -10.0, 10.0, 0.001)
		
		ADD_WIDGET_FLOAT_SLIDER("fMumX", fMumX, -10.0, 10.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("fMumY", fMumY, -10.0, 10.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("fMumTimeX", fMumTimeX, -10.0, 10.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("fMumTimeY", fMumTimeY, -10.0, 10.0, 0.001)
		
		ADD_WIDGET_FLOAT_SLIDER("fMumHeadPX", fMumHeadPX, -10.0, 10.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("fMumHeadPY", fMumHeadPY, -10.0, 10.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("fMumHeadUX", fMumHeadUX, -10.0, 10.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("fMumHeadUY", fMumHeadUY, -10.0, 10.0, 0.001)
		
		ADD_WIDGET_FLOAT_SLIDER("fDadHeadPX", fDadHeadPX, -10.0, 10.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("fDadHeadPY", fDadHeadPY, -10.0, 10.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("fDadHeadUX", fDadHeadUX, -10.0, 10.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("fDadHeadUY", fDadHeadUY, -10.0, 10.0, 0.001)

	STOP_WIDGET_GROUP()
ENDPROC

PROC SET_CHAR_CREATOR_TEXT_STYLE()
	SET_TEXT_FONT(FONT_STANDARD)
	SET_TEXT_SCALE(0.202, 0.368)
	SET_TEXT_COLOUR(255, 255, 255, 255)
ENDPROC

PROC DISPLAY_CHAR_CREATOR_DEBUG_TEXT(FLOAT DisplayAtX, FLOAT DisplayAtY, STRING pTextLabel)
	SET_CHAR_CREATOR_TEXT_STYLE()
	
	BEGIN_TEXT_COMMAND_DISPLAY_TEXT(pTextLabel)
	END_TEXT_COMMAND_DISPLAY_DEBUG_TEXT(DisplayAtX, DisplayAtY)
ENDPROC

PROC DISPLAY_CHAR_CREATOR_DEBUG_TEXT_WITH_LITERAL_STRING(FLOAT DisplayAtX, FLOAT DisplayAtY, STRING pTextLabel, STRING pLiteralString)
	SET_CHAR_CREATOR_TEXT_STYLE()
	
	BEGIN_TEXT_COMMAND_DISPLAY_TEXT(pTextLabel)
		ADD_TEXT_COMPONENT_SUBSTRING_LITERAL_STRING(pLiteralString)
	END_TEXT_COMMAND_DISPLAY_DEBUG_TEXT(DisplayAtX, DisplayAtY)
ENDPROC

PROC DISPLAY_CHAR_CREATOR_DEBUG_TEXT_WITH_NUMBER(FLOAT DisplayAtX, FLOAT DisplayAtY, STRING pTextLabel, INT NumberToDisplay)
	SET_CHAR_CREATOR_TEXT_STYLE()
	
	BEGIN_TEXT_COMMAND_DISPLAY_TEXT(pTextLabel)
		ADD_TEXT_COMPONENT_INTEGER(NumberToDisplay)
	END_TEXT_COMMAND_DISPLAY_DEBUG_TEXT(DisplayAtX, DisplayAtY)
ENDPROC

PROC DISPLAY_CHAR_CREATOR_DEBUG_TEXT_WITH_2_STRINGS(FLOAT DisplayAtX, FLOAT DisplayAtY, STRING pTextLabel, STRING pStr1, STRING pStr2)
	SET_CHAR_CREATOR_TEXT_STYLE()
	
	BEGIN_TEXT_COMMAND_DISPLAY_TEXT(pTextLabel)
		ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(pStr1)
		ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(pStr2)
	END_TEXT_COMMAND_DISPLAY_DEBUG_TEXT(DisplayAtX, DisplayAtY)
ENDPROC

PROC DISPLAY_CHAR_CREATOR_DEBUG_TEXT_WITH_FLOAT(FLOAT DisplayAtX, FLOAT DisplayAtY, STRING pTextLabel, FLOAT NumberToDisplay)
	SET_CHAR_CREATOR_TEXT_STYLE()
	
	BEGIN_TEXT_COMMAND_DISPLAY_TEXT(pTextLabel)
		ADD_TEXT_COMPONENT_FLOAT(NumberToDisplay, 2)
	END_TEXT_COMMAND_DISPLAY_DEBUG_TEXT(DisplayAtX, DisplayAtY)
ENDPROC

PROC CHAR_CREATOR_DEBUG_INFO(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData)

	SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_FADE)
	
	TEXT_LABEL_15 tlRaceString
	
	tlRaceString = GET_CHAR_CREATOR_RACE_STRING(charCreatorData, CHAR_CREATOR_PED_MUM, CHAR_CREATOR_RACE_INDEX_ONE)
	DISPLAY_CHAR_CREATOR_DEBUG_TEXT_WITH_LITERAL_STRING(fMumDetX2, fMumDetY2, "FACE_MUMDET", tlRaceString)
	
	tlRaceString = GET_CHAR_CREATOR_RACE_STRING(charCreatorData, CHAR_CREATOR_PED_DAD, CHAR_CREATOR_RACE_INDEX_ONE)
	DISPLAY_CHAR_CREATOR_DEBUG_TEXT_WITH_LITERAL_STRING(fDadDetX2, fDadDetY2, "FACE_DADDET", tlRaceString)

	DISPLAY_CHAR_CREATOR_DEBUG_TEXT_WITH_FLOAT(fUglyX2, fUglyY2, "FACE_UGLY", charCreatorData.pedData.fParentUgly) 
	
	INT iValue
	FLOAT fBlend
	GET_CHAR_CREATOR_OVERLAY_VALUES(charCreatorData, CHAR_CREATOR_WRINKLES, iValue, fBlend)
	DISPLAY_CHAR_CREATOR_DEBUG_TEXT_WITH_NUMBER(fWrinklesX2, fWrinklesY2 ,"FACE_WRINKLES", iValue)
	GET_CHAR_CREATOR_OVERLAY_VALUES(charCreatorData, CHAR_CREATOR_BLEMISH, iValue, fBlend)
	DISPLAY_CHAR_CREATOR_DEBUG_TEXT_WITH_NUMBER(fBlemishX2, fBlemishY2 ,"FACE_Blemish", iValue)
	GET_CHAR_CREATOR_OVERLAY_VALUES(charCreatorData, CHAR_CREATOR_DAMAGE, iValue, fBlend)
	DISPLAY_CHAR_CREATOR_DEBUG_TEXT_WITH_NUMBER(fDamageX2, fDamageY2 ,"FACE_DAMAGE",iValue)
	GET_CHAR_CREATOR_OVERLAY_VALUES(charCreatorData, CHAR_CREATOR_MAKEUP, iValue, fBlend)
	DISPLAY_CHAR_CREATOR_DEBUG_TEXT_WITH_NUMBER(fMakeupX2, fMakeupY2,"FACE_MAKEUPP", iValue)
	
	DISPLAY_CHAR_CREATOR_DEBUG_TEXT_WITH_NUMBER(fWrinklesPerX, fWrinklesPerY, "FACE_WPER", charCreatorData.iWrinklesPercent)
	DISPLAY_CHAR_CREATOR_DEBUG_TEXT_WITH_NUMBER(fDamagePerX, fDamagePerY, "FACE_DPER", charCreatorData.iDamagePercent)
	
	DISPLAY_CHAR_CREATOR_DEBUG_TEXT_WITH_NUMBER(0.045, 0.775, "FACE_BBL", charCreatorData.iBasePercent)
	GET_CHAR_CREATOR_OVERLAY_VALUES(charCreatorData, CHAR_CREATOR_BASE_BLEMISH, iValue, fBlend)
	DISPLAY_CHAR_CREATOR_DEBUG_TEXT_WITH_NUMBER(0.045, 0.795,"FACE_BBLV", iValue)	
	
	DISPLAY_CHAR_CREATOR_DEBUG_TEXT_WITH_FLOAT(fGeomX2, fGeomY2,"FACE_GeomBlend", GET_CHAR_CREATOR_PARENT_DOMINANCE(charCreatorData, CHAR_CREATOR_GENDER_MALE)) 
	DISPLAY_CHAR_CREATOR_DEBUG_TEXT_WITH_FLOAT(fGeomX3, fGeomY3,"FACE_TexBlend", GET_CHAR_CREATOR_PARENT_DOMINANCE(charCreatorData, CHAR_CREATOR_GENDER_FEMALE))
	
	INT iHead1 
	INT iHead2
	
	GET_CHAR_CREATOR_HEADS_FOR_RACE(charCreatorData, CHAR_CREATOR_PED_MUM, iHead1, charCreatorData.pedData.charCreatorParentInfo.iMumHeadVariation, CHAR_CREATOR_RACE_INDEX_ONE)
	GET_CHAR_CREATOR_HEADS_FOR_RACE(charCreatorData, CHAR_CREATOR_PED_MUM, iHead2, charCreatorData.pedData.charCreatorParentInfo.iMumHeadVariation2, CHAR_CREATOR_RACE_INDEX_TWO)
	
	DISPLAY_CHAR_CREATOR_DEBUG_TEXT_WITH_NUMBER(fMumHeadPX, fMumHeadPY, "NUMBER", iHead1)
	DISPLAY_CHAR_CREATOR_DEBUG_TEXT_WITH_NUMBER(fMumHeadUX, fMumHeadUY, "NUMBER", iHead2)
	
	GET_CHAR_CREATOR_HEADS_FOR_RACE(charCreatorData, CHAR_CREATOR_PED_DAD, iHead1, charCreatorData.pedData.charCreatorParentInfo.iDadHeadVariation, CHAR_CREATOR_RACE_INDEX_ONE)
	GET_CHAR_CREATOR_HEADS_FOR_RACE(charCreatorData, CHAR_CREATOR_PED_DAD, iHead2, charCreatorData.pedData.charCreatorParentInfo.iDadHeadVariation2, CHAR_CREATOR_RACE_INDEX_TWO)
	
	DISPLAY_CHAR_CREATOR_DEBUG_TEXT_WITH_NUMBER(fDadHeadPX, fDadHeadPY, "NUMBER", iHead1)
	DISPLAY_CHAR_CREATOR_DEBUG_TEXT_WITH_NUMBER(fDadHeadUX, fDadHeadUY, "NUMBER", iHead2)
	
	SWITCH charCreatorData.pedData.archetype
		CASE ARCHETYPE_SKIP_LIFESTYLE
			tlRaceString = "SKIP LIFESTYLE"
		BREAK
		CASE ARCHETYPE_RANDOM
			tlRaceString = "RANDOM"
		BREAK
		CASE ARCHETYPE_GOOD
			tlRaceString = "GOOD"
		BREAK
		CASE ARCHETYPE_BAD
			tlRaceString = "BAD"
		BREAK	
		CASE ARCHETYPE_LAZY
			tlRaceString = "LAZY"
		BREAK		
		CASE ARCHETYPE_ACTIVE
			tlRaceString = "ACTIVE"
		BREAK	
		CASE ARCHETYPE_PARTY
			tlRaceString = "PARTY"
		BREAK	
		CASE ARCHETYPE_UGLY
			tlRaceString = "UGLY"
		BREAK	
		CASE ARCHETYPE_CUSTOM
			SWITCH charCreatorData.pedData.customArchetype
				CASE CUSTOM_ARCHETYPE_FEMALE_CHOLA
					tlRaceString = "F CHOLA"
				BREAK
				CASE CUSTOM_ARCHETYPE_FEMALE_BUSINESS_1
					tlRaceString = "F BUSINESS 1"
				BREAK				
				CASE CUSTOM_ARCHETYPE_FEMALE_BUSINESS_2
					tlRaceString = "F BUSINESS 2"
				BREAK		
				CASE CUSTOM_ARCHETYPE_FEMALE_CASUAL_1
					tlRaceString = "F CASUAL 1"
				BREAK		
				CASE CUSTOM_ARCHETYPE_FEMALE_CASUAL_2
					tlRaceString = "F CASUAL 2"
				BREAK	
				CASE CUSTOM_ARCHETYPE_FEMALE_PARTY_SOUTH_AMERICAN
					tlRaceString = "F PARTY SAM"
				BREAK	
				CASE CUSTOM_ARCHETYPE_FEMALE_CASUAL_3
					tlRaceString = "F CASUAL 3"
				BREAK	
				CASE CUSTOM_ARCHETYPE_FEMALE_PARTY_EUROPEAN
					tlRaceString = "F PARTY E"
				BREAK	
				CASE CUSTOM_ARCHETYPE_FEMALE_PARTY_EAST_ASIAN
					tlRaceString = "F PARTY EA"
				BREAK
				CASE CUSTOM_ARCHETYPE_FEMALE_PARTY_AFRICAN
					tlRaceString = "F PARTY AF"
				BREAK	
				CASE CUSTOM_ARCHETYPE_FEMALE_PARTY_PACIFIC_ISLANDER
					tlRaceString = "F PARTY PA"
				BREAK	
				CASE CUSTOM_ARCHETYPE_FEMALE_COUCH_POTATO_EUROPEAN
					tlRaceString = "F COUCH E"
				BREAK	
				CASE CUSTOM_ARCHETYPE_FEMALE_COUCH_POTATO_MIXED_1
					tlRaceString = "F COUCH M1"
				BREAK	
				CASE CUSTOM_ARCHETYPE_FEMALE_COUCH_POTATO_MIXED_2
					tlRaceString = "F COUCH M2"
				BREAK	
				CASE CUSTOM_ARCHETYPE_FEMALE_COUCH_POTATO_MIXED_3
					tlRaceString = "F COUCH M3"
				BREAK	
				CASE CUSTOM_ARCHETYPE_FEMALE_CHOLA_MIXED
					tlRaceString = "F CHOLA MIXED"
				BREAK	
				CASE CUSTOM_ARCHETYPE_FEMALE_BUSINESS_3
					tlRaceString = "F BUSINESS 3"
				BREAK	
				CASE CUSTOM_ARCHETYPE_FEMALE_PARTY_HARD
					tlRaceString = "F PARTY HARD"
				BREAK	
				CASE CUSTOM_ARCHETYPE_MALE_BUSINESS_1
					tlRaceString = "M BUSINESS 1"
				BREAK						
				CASE CUSTOM_ARCHETYPE_MALE_CASUAL_ASIAN
					tlRaceString = "M CASUAL ASIAN"
				BREAK	
				CASE CUSTOM_ARCHETYPE_MALE_SLOB_EUROPEAN
					tlRaceString = "M SLOB EUROPEAN"
				BREAK
				CASE CUSTOM_ARCHETYPE_MALE_CASUAL_1
					tlRaceString = "M CASUAL 1"
				BREAK	
				CASE CUSTOM_ARCHETYPE_MALE_DIRTY_PARTY
					tlRaceString = "M DIRTY PARTY"
				BREAK	
			ENDSWITCH
		BREAK
	ENDSWITCH
	DISPLAY_CHAR_CREATOR_DEBUG_TEXT_WITH_LITERAL_STRING(fLifestyleX, fLifestyleY, "STRING", tlRaceString)
	
	INT iTimePassed
	IF NOT HAS_CHAR_CREATOR_PED_BLEND_FINISHED(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON])
	
		IF NOT charCreatorData.bSonLoadingN
			charCreatorData.bSonLoadingN = TRUE
			charCreatorData.iSonLoadN = TIMERB()
		ENDIF
	
		DISPLAY_CHAR_CREATOR_DEBUG_TEXT(fSonX, fSonY, "FACE_SLOAD")
		
		iTimePassed = FLOOR((TO_FLOAT(TIMERB()) - charCreatorData.iSonLoadN) / 1000)
		DISPLAY_CHAR_CREATOR_DEBUG_TEXT_WITH_NUMBER(fSonTimeX, fSonTimeY, "FACE_LOADT", iTimePassed)
	ELSE
		IF charCreatorData.bSonLoadingN
			charCreatorData.bSonLoadingN = FALSE
			charCreatorData.iSonLoadN = 0
		ENDIF
	ENDIF
	
	IF NOT HAS_CHAR_CREATOR_PED_BLEND_FINISHED(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER])
	
		IF NOT charCreatorData.bDaughterLoadingN
			charCreatorData.bDaughterLoadingN = TRUE
			charCreatorData.iDaughterLoadN = TIMERB()
		ENDIF
	
		DISPLAY_CHAR_CREATOR_DEBUG_TEXT(fDaughterX, fDaughterY, "FACE_DALOAD")
		
		iTimePassed = FLOOR((TO_FLOAT(TIMERB()) - charCreatorData.iDaughterLoadN) / 1000)
		DISPLAY_CHAR_CREATOR_DEBUG_TEXT_WITH_NUMBER(fDaughterTimeX, fDaughterTimeY, "FACE_LOADT", iTimePassed)
	ELSE
		IF charCreatorData.bDaughterLoadingN
			charCreatorData.bDaughterLoadingN = FALSE
			charCreatorData.iDaughterLoadN = 0
		ENDIF
	ENDIF
	
	IF NOT HAS_CHAR_CREATOR_PED_BLEND_FINISHED(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAD])
	
		IF NOT charCreatorData.bDadLoadingN
			charCreatorData.bDadLoadingN = TRUE
			charCreatorData.iDadLoadN = TIMERB()
		ENDIF
	
		DISPLAY_CHAR_CREATOR_DEBUG_TEXT(fDadX, fDadY, "FACE_DLOAD")
		
		iTimePassed = FLOOR((TO_FLOAT(TIMERB()) - charCreatorData.iDadLoadN) / 1000)
		DISPLAY_CHAR_CREATOR_DEBUG_TEXT_WITH_NUMBER(fDadTimeX, fDadTimeY, "FACE_LOADT", iTimePassed)
	ELSE
		IF charCreatorData.bDadLoadingN
			charCreatorData.bDadLoadingN = FALSE
			charCreatorData.iDadLoadN = 0
		ENDIF
	ENDIF
	
	IF NOT HAS_CHAR_CREATOR_PED_BLEND_FINISHED(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_MUM])
	
		IF NOT charCreatorData.bMumLoadingN
			charCreatorData.bMumLoadingN = TRUE
			charCreatorData.iMumLoadN = TIMERB()
		ENDIF
	
		DISPLAY_CHAR_CREATOR_DEBUG_TEXT(fMumX, fMumY, "FACE_MLOAD")
		
		iTimePassed = FLOOR((TO_FLOAT(TIMERB()) - charCreatorData.iMumLoadN) / 1000)
		DISPLAY_CHAR_CREATOR_DEBUG_TEXT_WITH_NUMBER(fMumTimeX, fMumTimeY, "FACE_LOADT", iTimePassed)
	ELSE
		IF charCreatorData.bMumLoadingN
			charCreatorData.bMumLoadingN = FALSE
			charCreatorData.iMumLoadN = 0
		ENDIF
	ENDIF


	SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(FALSE)
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)

ENDPROC


PROC MAINTAIN_CHAR_CREATOR_WIDGETS(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData)
	
	IF charCreatorData.bRemovePedsHair != charCreatorData.bCurrentHairStatus
	
		charCreatorData.bCurrentHairStatus = charCreatorData.bRemovePedsHair
	
		IF charCreatorData.bCurrentHairStatus
		
			PRINTLN("[CHARCREATOR] Hiding Hair")
		
			SET_PED_COMP_ITEM_CURRENT_MP(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_MUM], COMP_TYPE_HAIR, HAIR_FMF_0_0, FALSE)
			SET_PED_COMP_ITEM_CURRENT_MP(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAD], COMP_TYPE_HAIR, HAIR_FMM_0_0, FALSE)
			SET_PED_COMP_ITEM_CURRENT_MP(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER], COMP_TYPE_HAIR, HAIR_FMF_0_0, FALSE)
			SET_PED_COMP_ITEM_CURRENT_MP(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON], COMP_TYPE_HAIR, HAIR_FMM_0_0, FALSE)
		ELSE
			PRINTLN("[CHARCREATOR] Showing Hair")
		
			GIVE_CHAR_CREATOR_PED_HAIR(charCreatorData, CHAR_CREATOR_PED_MUM)
			GIVE_CHAR_CREATOR_PED_HAIR(charCreatorData, CHAR_CREATOR_PED_DAD)
			GIVE_CHAR_CREATOR_PED_HAIR(charCreatorData, CHAR_CREATOR_PED_DAUGHTER)
			GIVE_CHAR_CREATOR_PED_HAIR(charCreatorData, CHAR_CREATOR_PED_SON)
		ENDIF
	
	ENDIF
	
	IF charCreatorData.bTriggerBlend
		charCreatorData.bTriggerBlend = FALSE
		
		PRINTLN("[CHARCREATOR] Debug Our Parents Blend to: ", charCreatorData.fDebugHeadBlend)
		
		IF IS_CHAR_CREATOR_PED_OK(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAD])
			UPDATE_PED_HEAD_BLEND_DATA(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAD], charCreatorData.fDebugHeadBlend, 0.0, 0.0)
		ENDIF
		
		IF IS_CHAR_CREATOR_PED_OK(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_MUM])
			UPDATE_PED_HEAD_BLEND_DATA(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_MUM], charCreatorData.fDebugHeadBlend, 0.0, 0.0)
		ENDIF
	ENDIF
	
ENDPROC
#ENDIF

/// PURPOSE: Handles the display of help text on the left hand column as you navigate around the menu
PROC SET_CHAR_CREATOR_HELP_TEXT(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData)
	
	TEXT_LABEL_15 tlHelpText = "CHARC_"

	SWITCH charCreatorData.columnIndex
	
		CASE MAIN_MENU_COLUMN
			tlHelpText += "M_"
			tlHelpText += ENUM_TO_INT(charCreatorData.mainMenuIndex)
			
			IF charCreatorData.mainMenuIndex = CONFIRM_CAT
			OR charCreatorData.mainMenuIndex = HERITAGE_CAT
			OR charCreatorData.mainMenuIndex = LIFESTYLE_CAT
				IF NOT HAS_ENTERED_GAME_WITH_REDUCED_CONTENT_DUE_TO_CLOUD()
				AND NOT HAS_ENTERED_OFFLINE_SAVE_FM()
					tlHelpText += "_A"
				ELSE
					tlHelpText += "_B"
				ENDIF
			ENDIF
		BREAK
		
		CASE LIFESTYLE_COLUMN
			tlHelpText += "L_"
			tlHelpText += ENUM_TO_INT(charCreatorData.lifestyleIndex)
			
			// Use condensed version of help text if we are editing our appearance since stats are not changed
			IF HAS_ENTERED_CHAR_CREATION_FROM_ALTERATION_PROMPT()
				tlHelpText += "A"
			ENDIF
		BREAK
		
		CASE APPEARANCE_COLUMN
			tlHelpText += "A_"
			tlHelpText += ENUM_TO_INT(charCreatorData.appearanceIndex)
		BREAK
		
		CASE HERITAGE_COLUMN
			tlHelpText += "H_"
			
			IF charCreatorData.heritageIndex = HERITAGE_MUM
				
				IF GET_CHAR_CREATOR_PARENT_RACE(charCreatorData, CHAR_CREATOR_PED_MUM, CHAR_CREATOR_RACE_INDEX_ONE) = CHAR_CREATOR_SPECIAL_RACE
					tlHelpText += "30"
				ELSE
					tlHelpText += ENUM_TO_INT(charCreatorData.heritageIndex)
				ENDIF
			ELIF charCreatorData.heritageIndex = HERITAGE_DAD
				IF GET_CHAR_CREATOR_PARENT_RACE(charCreatorData, CHAR_CREATOR_PED_DAD, CHAR_CREATOR_RACE_INDEX_ONE) = CHAR_CREATOR_SPECIAL_RACE
					tlHelpText += "31"
				ELSE
					tlHelpText += ENUM_TO_INT(charCreatorData.heritageIndex)
				ENDIF
			ELSE
				tlHelpText += ENUM_TO_INT(charCreatorData.heritageIndex)
			ENDIF
		BREAK
	
	ENDSWITCH
	
	SET_FRONTEND_HELP_TEXT(CHAR_CREATOR_COL_MAIN_MENU, tlHelpText)
ENDPROC


