
USING "screens_header.sch"
USING "net_events.sch"
USING "script_network.sch"

USING "Transition_Common.sch"

CONST_INT MAX_NUMBER_IGNORE_INVITE_SELECTIONS 2

//PROC INIT_MPHUD_IGNORE_INVITE(MPHUD_PLACEMENT_TOOLS& Placement)
//
//	RESET_ALL_SPRITE_PLACEMENT_VALUES(Placement.SpritePlacement)
//
//
//	Placement.TextPlacement[0].x = 0.5
//	Placement.TextPlacement[0].y = 0.5
//
//	Placement.TextPlacement[1].x = 0.500
//	Placement.TextPlacement[1].y = 0.178
//	
//	Placement.TextPlacement[2].x = 0.500
//	Placement.TextPlacement[2].y = 0.209
//	
//	//Top bar
//	Placement.SpritePlacement[0].x = 0.500
//	Placement.SpritePlacement[0].y = 0.190
//	Placement.SpritePlacement[0].w = 0.675
//	Placement.SpritePlacement[0].h = 0.045
//	Placement.SpritePlacement[0].r = 255
//	Placement.SpritePlacement[0].g = 255
//	Placement.SpritePlacement[0].b = 255
//	Placement.SpritePlacement[0].a = 200
//	
//	//Bottom Bar 
//	Placement.SpritePlacement[1].x = 0.5
//	Placement.SpritePlacement[1].y = 0.222
//	Placement.SpritePlacement[1].w = 0.675
//	Placement.SpritePlacement[1].h = 0.045
//	Placement.SpritePlacement[1].r = 255
//	Placement.SpritePlacement[1].g = 255
//	Placement.SpritePlacement[1].b = 255
//	Placement.SpritePlacement[1].a = 200
//	
//	
//	//Scaleform movie 
//	Placement.SpritePlacement[2].x = 0.390
//	Placement.SpritePlacement[2].y = 0.467
//	Placement.SpritePlacement[2].w = 1
//	Placement.SpritePlacement[2].h = 1
//	Placement.SpritePlacement[2].r = 255
//	Placement.SpritePlacement[2].g = 255
//	Placement.SpritePlacement[2].b = 255
//	Placement.SpritePlacement[2].a = 200
//	
//ENDPROC

PROC RENDER_MPHUD_IGNORE_INVITE()

	// 'SELECT MP CHARACTER...'
//	DRAW_TEXT(Placement.MainTitle, Placement.aStyle.TS_TITLEHUD, "HUD_LEAVETIT")	
		
	// main background
//	DRAW_JOIN_HUD_BACKGROUND()
//	
//	STRING YesString = "HUD_YES"
//	STRING NoString = "HUD_NO"
//	STRING ScreenTitle = "HUD_IGNOREIN"
//	
//	SET_STANDARD_MEDIUM_HUD_TEXT(Placement.TS_STANDARDMEDIUMHUD)
//	
//	// draw selector icon
//	IF Placement.ScreenPlace.iSelection = 0
//		SET_TEXT_BLACK(Placement.TS_STANDARDMEDIUMHUD)
//		DRAW_TEXT_WITH_ALIGNMENT(Placement.TextPlacement[1], Placement.TS_STANDARDMEDIUMHUD, YesString, TRUE, FALSE)
//		SET_TEXT_WHITE(Placement.TS_STANDARDMEDIUMHUD)
//		DRAW_TEXT_WITH_ALIGNMENT(Placement.TextPlacement[2], Placement.TS_STANDARDMEDIUMHUD, NoString, TRUE, FALSE)
//		DRAW_2D_SPRITE("MPEntry", "MP_ModeSelected_Gradient", Placement.SpritePlacement[0], FALSE)
//		DRAW_2D_SPRITE("MPEntry", "MP_ModeNotSelected_Gradient", Placement.SpritePlacement[1], FALSE)
//	ELSE
//		DRAW_TEXT_WITH_ALIGNMENT(Placement.TextPlacement[1], Placement.TS_STANDARDMEDIUMHUD, YesString, TRUE, FALSE)
//		SET_TEXT_BLACK(Placement.TS_STANDARDMEDIUMHUD)
//		DRAW_TEXT_WITH_ALIGNMENT(Placement.TextPlacement[2], Placement.TS_STANDARDMEDIUMHUD, NoString, TRUE, FALSE)
//		SET_TEXT_WHITE(Placement.TS_STANDARDMEDIUMHUD)
//		DRAW_2D_SPRITE("MPEntry", "MP_ModeSelected_Gradient", Placement.SpritePlacement[1], FALSE)
//		DRAW_2D_SPRITE("MPEntry", "MP_ModeNotSelected_Gradient", Placement.SpritePlacement[0], FALSE)
//	ENDIF
//	
//	
//	SPRITE_PLACEMENT ScaleformSprite
//	
//
//	SCALEFORM_TABS_INPUT_DATA TabsData
//	TabsData.MainTitle = ScreenTitle
//	ScaleformSprite = GET_SCALEFORM_TABS_POSITION()
//	INT PotentialPed = GET_JOINING_CHARACTER()
//	COMPILE_SCALEFORM_TABS(Placement.IdlePed[PotentialPed], TabsData, Placement.ScaleformTabStruct )
//	RUN_SCALEFORM_TABS(Placement.ButtonMovie[GET_SCALEFORM_INDEX_FOR_HUD(HUD_SCALEFORM_TABS)], ScaleformSprite, Placement.ScaleformTabStruct)
//	
//	
//	ScaleformSprite = GET_SCALEFORM_INSTRUCTIONAL_BUTTON_POSITION()
//	
//	ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_GROUP_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUTGROUP_FRONTEND_DPAD_UD), "HUD_INPUT1", Placement.ScaleformStruct)
//	ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT), "HUD_INPUT2", Placement.ScaleformStruct)
//	ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL), "HUD_INPUT3", Placement.ScaleformStruct)
//	
//	RUN_SCALEFORM_INSTRUCTIONAL_BUTTONS(Placement.ButtonMovie[GET_SCALEFORM_INDEX_FOR_HUD(HUD_SCALEFORM_INSTRUCTIONAL)], ScaleformSprite, Placement.ScaleformStruct)



	STRING sStatementText = "HUD_QUIT"
	STRING sLineOneMesg = "HUD_INVPROG"
	STRING sLineTwoMesg = "HUD_INGINV"
//	SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sLineOneMesg, FE_WARNING_ENTERMPSPCANCEL, sLineTwoMesg)
	SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sLineOneMesg, FE_WARNING_YESNO, sLineTwoMesg)
	


ENDPROC

PROC LOGIC_MPHUD_IGNORE_INVITE(MPHUD_PLACEMENT_TOOLS& Placement)
	

//	IF NOT NETWORK_IS_SESSION_BUSY()
//		IF GET_CURRENT_TRANSITION_STATE() = TRANSITION_STATE_WAIT_HUD_EXIT

			IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_CANCEL)
			
				#IF IS_DEBUG_BUILD
					NET_NL()NET_PRINT("LOGIC_MPHUD_IGNORE_INVITE: INPUT_FRONTEND_CANCEL ")
				#ENDIF
			
				IF Placement.PreviousScreen = HUD_STATE_PRE_HUD_CHECKS
					HUD_CHANGE_STATE(HUD_STATE_PRE_HUD_CHECKS)
					TRANSITION_CHANGE_STATE(TRANSITION_STATE_PRE_HUD_CHECKS)
				ELIF Placement.PreviousScreen = HUD_STATE_SELECT_CHARACTER_FM
					HUD_CHANGE_STATE(HUD_STATE_SELECT_CHARACTER_FM)
				ELSE
					HUD_CHANGE_STATE(HUD_STATE_DIFF_SETTINGS)	
				ENDIF
				
			ENDIF
			
//			IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
//				#IF IS_DEBUG_BUILD
//					NET_NL()NET_PRINT("LOGIC_MPHUD_IGNORE_INVITE: INPUT_FRONTEND_ACCEPT ")
//				#ENDIF
//				IGNORE_INVITE()
//
//				SET_JOINING_GAMEMODE(GAMEMODE_FM)
//				TELL_TRANSITION_BAIL_HAPPENED(FALSE) //La la la, pretend bail never happened to re-enter a normal freemode session. 
//				SET_CURRENT_TRANSITION_FM_SESSION_MENU_CHOICE(FM_SESSION_MENU_CHOICE_JOIN_PUBLIC_SESSION)
//				SET_CURRENT_TRANSITION_FM_MENU_CHOICE(FM_MENU_CHOICE_JOIN_NEW_SESSION)
//				HUD_CHANGE_STATE(HUD_STATE_LOADING)
//				TRANSITION_CHANGE_STATE(TRANSITION_STATE_TERMINATE_SESSION)
//    			REQUEST_TRANSITION(TRANSITION_STATE_RETURN_TO_SINGLEPLAYER, GAMEMODE_SP, HUD_STATE_LOADING)	
//			ENDIF
			
			
			IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
				#IF IS_DEBUG_BUILD
					NET_NL()NET_PRINT("LOGIC_MPHUD_IGNORE_INVITE: INPUT_FRONTEND_ACCEPT ")
				#ENDIF
				IGNORE_INVITE()
				TELL_TRANSITION_BAIL_HAPPENED(FALSE) //La la la, pretend bail never happened to re-enter a normal freemode session. 
				REQUEST_TRANSITION(TRANSITION_STATE_RETURN_TO_SINGLEPLAYER, GAMEMODE_SP, HUD_STATE_LOADING)	
			ENDIF
			
//		ENDIF
//	ENDIF
	
ENDPROC

PROC PROCESS_MPHUD_IGNORE_INVITE(MPHUD_PLACEMENT_TOOLS& Placement)
	
	// initialise data
	IF NOT (Placement.bHudScreenInitialised)
//		INIT_MPHUD_IGNORE_INVITE(Placement)
		Placement.ScreenPlace.iSelection = 1
		Placement.bHudScreenInitialised = TRUE
	ENDIF
	
	IF HAS_QUIT_CURRENT_GAME() = FALSE
		RENDER_MPHUD_IGNORE_INVITE()
	ENDIF
	
	LOGIC_MPHUD_IGNORE_INVITE(Placement)

	

ENDPROC

