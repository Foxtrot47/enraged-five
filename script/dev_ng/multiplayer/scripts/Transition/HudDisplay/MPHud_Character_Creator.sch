/* ------------------------------------------------------------------
* Name: MPHud_Character_Creator.sch
* Author: James Adwick
* Date: 30/04/2013
* Purpose: Character Creator screen accessed when entering MP.
* Players choose their parents and lifestyle and generate their
* character before entering GTA Online.
*
*	NOTE: Key stats for parents head that makeup child:
*	
*	PACKED_MP_M1_HEAD_TYPE,		// Type one head race
*	PACKED_MP_M1_HEAD_VAR,		// The variation of this head (normal 0 = pretty, 1 = ugly, 2 = extra variation)
*	PACKED_MP_M2_HEAD_TYPE,
*	PACKED_MP_M2_HEAD_VAR,
*	PACKED_MP_D1_HEAD_TYPE,
*	PACKED_MP_D1_HEAD_VAR,
*	PACKED_MP_D2_HEAD_TYPE,
*	PACKED_MP_D2_HEAD_VAR,
*
* ------------------------------------------------------------------*/

USING "MPHud_Ped_Generation.sch"

// ***********************************
// 	  CHARACTER CREATOR CONST_INTS
// ***********************************

CONST_INT CHAR_CREATOR_MAX_DOMINENCE				100


// ***********************************
// 		CHARACTER CREATOR SCALEFORM
// ***********************************


// Return the correct text label based on if we are showing Male / Female
FUNC STRING GET_CHAR_CREATOR_GENDER_LABEL(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData)
	IF IS_CHAR_CREATOR_PED_MALE(charCreatorData)
		RETURN "FACE_MALE"
	ELSE
		RETURN "FACE_FEMALE"
	ENDIF
ENDFUNC

// Return the colour of the screen depending on team of player
FUNC INT GET_CHAR_CREATOR_COLOUR()
	RETURN ENUM_TO_INT(HUD_COLOUR_FREEMODE)
ENDFUNC

/// PURPOSE: Populates the scaleform on the left hand side (static)
PROC SETUP_CHAR_CREATOR_MAIN_MENU(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData)
	
	PRINTLN("[CHARCREATOR] SETUP_CHAR_CREATOR_MAIN_MENU")
	
	// Set up the 4 rows that players can access to edit their character
	SET_FRONTEND_DATA_SLOT(CHAR_CREATOR_COL_MAIN_MENU, ENUM_TO_INT(LIFESTYLE_CAT), 	charCreatorData.CHAR_COL1_ID, ENUM_TO_INT(LIFESTYLE_CAT), 	"FACE_LIFE")			// Lifestyle
	SET_FRONTEND_DATA_SLOT(CHAR_CREATOR_COL_MAIN_MENU, ENUM_TO_INT(HERITAGE_CAT), 	charCreatorData.CHAR_COL1_ID, ENUM_TO_INT(HERITAGE_CAT), 	"FACE_HERI")			// Heritage
	SET_FRONTEND_DATA_SLOT(CHAR_CREATOR_COL_MAIN_MENU, ENUM_TO_INT(APPEARANCE_CAT), charCreatorData.CHAR_COL1_ID, ENUM_TO_INT(APPEARANCE_CAT), 	"FACE_APP")				// Appearance
	
	IF HAS_ENTERED_GAME_WITH_REDUCED_CONTENT_DUE_TO_CLOUD()
	OR HAS_ENTERED_OFFLINE_SAVE_FM()
		SET_FRONTEND_DATA_SLOT(CHAR_CREATOR_COL_MAIN_MENU, ENUM_TO_INT(CONFIRM_CAT), 	charCreatorData.CHAR_COL1_ID, ENUM_TO_INT(CONFIRM_CAT), 	"HUD_CONTINUE", FALSE, 2)	// Continue
	ELSE
		SET_FRONTEND_DATA_SLOT(CHAR_CREATOR_COL_MAIN_MENU, ENUM_TO_INT(CONFIRM_CAT), 	charCreatorData.CHAR_COL1_ID, ENUM_TO_INT(CONFIRM_CAT), 	"FACE_SAVE", FALSE, 2)	// Save & Continue
	ENDIF
	
	DISPLAY_FRONTEND_DATA_SLOT(CHAR_CREATOR_COL_MAIN_MENU)
ENDPROC


FUNC INT GET_HIGHLIGHT_ROW_FOR_APPEARANCE_INDEX(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData)
	BOOL bHasColour = DOES_CHAR_CREATOR_HAIR_HAVE_COLOURS(charCreatorData)

	SWITCH charCreatorData.appearanceIndex
		CASE APPEARANCE_AGE
			RETURN 0
		BREAK
		
		CASE APPEARANCE_HAIR
			RETURN 1
		BREAK
		
		CASE APPEARANCE_HAIR_COLOUR
			RETURN 2
		BREAK
		
		CASE APPEARANCE_BEARD
		CASE APPEARANCE_MAKEUP
			IF bHasColour
				RETURN 3
			ELSE
				RETURN 2
			ENDIF
		BREAK
		
		//CASE APPEARANCE_ACCESSORIES
		//	IF bHasColour
		//		RETURN 4
		//	ELSE
		//		RETURN 3
		//	ENDIF
		//BREAK		
		
		CASE APPEARANCE_HAT
			IF bHasColour
				RETURN 4
			ELSE
				RETURN 3
			ENDIF
		BREAK	
		
		CASE APPEARANCE_GLASSES
			IF bHasColour
				RETURN 5
			ELSE
				RETURN 4
			ENDIF
		BREAK					
		
		CASE APPEARANCE_CREW_TSHIRT
			IF bHasColour
				RETURN 6
			ELSE
				RETURN 5
			ENDIF
		BREAK
		
		CASE APPEARANCE_CREW_DECALS
			IF bHasColour
				RETURN 7
			ELSE
				RETURN 6
			ENDIF
		BREAK		
	ENDSWITCH

	RETURN 0
ENDFUNC


/// PURPOSE: Populates the scaleform for the middle column when Appearance is selected
PROC SETUP_CHAR_CREATOR_APPEARANCE(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData, BOOL bUpdate = FALSE)

	PRINTLN("[CHARCREATOR] SETUP_CHAR_CREATOR_APPEARANCE")

	// Set up the players hair
	BOOL bIsMale = IS_CHAR_CREATOR_PED_MALE(charCreatorData)
	INT iHair = GET_CHAR_CREATOR_HAIR(charCreatorData, GET_CHAR_CREATOR_GENDER(charCreatorData))
	INT iHairColour = GET_CHAR_CREATOR_HAIR_COLOUR(charCreatorData, GET_CHAR_CREATOR_GENDER(charCreatorData))

	TEXT_LABEL_15 tlHair = "CC_"
	TEXT_LABEL_15 tlHairColour = "CC_"
	
	IF bIsMale
		tlHair += "M_HS_"
		tlHairColour += "M_H_"
	ELSE
		tlHair += "F_HS_"
		tlHairColour += "F_H_"
	ENDIF
	
	tlHair += iHair
	tlHairColour += iHairColour

	CLEAR_FRONTEND_DATA_SLOT(CHAR_CREATOR_COL_APP)

	// Populate our appearance rows
	
	// 1. Character's Age
	SET_FRONTEND_SETTING_DATA_SLOT(CHAR_CREATOR_COL_APP, ENUM_TO_INT(APPEARANCE_AGE), charCreatorData.CHAR_COL2_ID, ENUM_TO_INT(APPEARANCE_AGE), "FACE_AGE", "NUMBER", TRUE, TRUE, bUpdate, GET_CHAR_CREATOR_AGE(charCreatorData))	// Hair

	// 2. Character's Hair Style
	SET_FRONTEND_SETTING_DATA_SLOT(CHAR_CREATOR_COL_APP, ENUM_TO_INT(APPEARANCE_HAIR), charCreatorData.CHAR_COL2_ID, ENUM_TO_INT(APPEARANCE_HAIR), "FACE_HAIR", tlHair, TRUE, TRUE, bUpdate)	// Hair
	
	INT iIndex = 2
	
	// 3. Character's Hair Colour (if valid)
	IF DOES_CHAR_CREATOR_HAIR_HAVE_COLOURS(charCreatorData)
		SET_FRONTEND_SETTING_DATA_SLOT(CHAR_CREATOR_COL_APP, iIndex, charCreatorData.CHAR_COL2_ID, ENUM_TO_INT(APPEARANCE_HAIR_COLOUR), "FACE_HAIRC", tlHairColour, TRUE, TRUE, bUpdate)	// Hair	
		iIndex++
	ENDIF
	
	// 4. Character's beard or makeup (based on gender)
	IF IS_CHAR_CREATOR_PED_MALE(charCreatorData)
		TEXT_LABEL_15 tlBeard = "HAIR_BEARD"
		INT iValue
		FLOAT fBlend
		GET_CHAR_CREATOR_OVERLAY_VALUES(charCreatorData, CHAR_CREATOR_BEARD, iValue, fBlend)

		tlBeard += (iValue+1)

		SET_FRONTEND_SETTING_DATA_SLOT(CHAR_CREATOR_COL_APP, iIndex, charCreatorData.CHAR_COL2_ID, ENUM_TO_INT(APPEARANCE_BEARD), "FACE_BRD", tlBeard, TRUE)
	ELSE
		TEXT_LABEL_15 tlMakeup = "HAIR_MKUP"
		INT iValue
		FLOAT fBlend
		GET_CHAR_CREATOR_OVERLAY_VALUES(charCreatorData, CHAR_CREATOR_MAKEUP, iValue, fBlend)

		tlMakeup += (iValue+1)
		SET_FRONTEND_SETTING_DATA_SLOT(CHAR_CREATOR_COL_APP, iIndex, charCreatorData.CHAR_COL2_ID, ENUM_TO_INT(APPEARANCE_MAKEUP), "FACE_MKP", tlMakeup, TRUE)
	ENDIF
	iIndex++

	// Hat
	CHAR_CREATOR_LIFESTYLE maxLifestyle = GET_MAX_LIFESTYLE(charCreatorData)
	INT iLockLifestyleSlot = ENUM_TO_INT(maxLifestyle) - 1				
	
	TEXT_LABEL_15 tlHat = "CHARC_HAT"
	INT iHat  
	IF IS_CHAR_CREATOR_PED_MALE(charCreatorData)
		iHat = charCreatorData.iCacheMaleHatOption[iLockLifestyleSlot]
	ELSE
		iHat = charCreatorData.iCacheFemaleHatOption[iLockLifestyleSlot]
	ENDIF
	tlHat += (iHat+2)
	
	SET_FRONTEND_SETTING_DATA_SLOT(CHAR_CREATOR_COL_APP, iIndex, charCreatorData.CHAR_COL2_ID, ENUM_TO_INT(APPEARANCE_HAT), "FACE_HAT", tlHat, TRUE, TRUE, bUpdate)
	iIndex++	
	
	
	// Glasses
	TEXT_LABEL_15 tlGlasses = "CHARC_GLS"
	INT iGlasses  
	IF IS_CHAR_CREATOR_PED_MALE(charCreatorData)
		iGlasses = charCreatorData.iCacheMaleGlassesOption[iLockLifestyleSlot]
	ELSE
		iGlasses = charCreatorData.iCacheFemaleGlassesOption[iLockLifestyleSlot]
	ENDIF
	tlGlasses += (iGlasses+2)
	
	SET_FRONTEND_SETTING_DATA_SLOT(CHAR_CREATOR_COL_APP, iIndex, charCreatorData.CHAR_COL2_ID, ENUM_TO_INT(APPEARANCE_GLASSES), "FACE_GLS", tlGlasses, TRUE, TRUE, bUpdate)
	iIndex++		
	
	
	// 6. Option to use crew tshirt or not
	IF IS_LOCAL_PLAYER_IN_ACTIVE_CLAN()
		TEXT_LABEL_15 tlCrewT
		SWITCH charCreatorData.crewTShirtOption
			CASE CREW_TSHIRT_OFF
				tlCrewT = "CHARC_CRT1"
			BREAK
			CASE CREW_TSHIRT_SOLID_CREW_COLOR
				tlCrewT = "CHARC_CRT2"
			BREAK		
			CASE CREW_TSHIRT_SOLID_BLACK
				tlCrewT = "CHARC_CRT3"
			BREAK	
			CASE CREW_TSHIRT_SOLID_WHITE
				tlCrewT = "CHARC_CRT4"
			BREAK	
			CASE CREW_TSHIRT_SOLID_GRAY
				tlCrewT = "CHARC_CRT5"
			BREAK	
			CASE CREW_TSHIRT_SLEEVES_BLACK
				tlCrewT = "CHARC_CRT6"
			BREAK	
			CASE CREW_TSHIRT_SLEEVES_WHITE
				tlCrewT = "CHARC_CRT7"
			BREAK		
			CASE CREW_TSHIRT_SLEEVES_GRAY
				tlCrewT = "CHARC_CRT8"
			BREAK	
			CASE CREW_TSHIRT_COLLAR_BLACK
				tlCrewT = "CHARC_CRT9"
			BREAK	
			CASE CREW_TSHIRT_COLLAR_WHITE
				tlCrewT = "CHARC_CRT10"
			BREAK	
			CASE CREW_TSHIRT_COLLAR_GRAY
				tlCrewT = "CHARC_CRT11"
			BREAK				
		ENDSWITCH

		SET_FRONTEND_SETTING_DATA_SLOT(CHAR_CREATOR_COL_APP, iIndex, charCreatorData.CHAR_COL2_ID, ENUM_TO_INT(APPEARANCE_CREW_TSHIRT), "FACE_CREWT", tlCrewT, TRUE, TRUE, bUpdate)
		iIndex++
	
		IF charCreatorData.crewTShirtOption <> CREW_TSHIRT_OFF
			// 7 - crew decals
			SWITCH charCreatorData.crewDecalOption
				CASE CREW_DECAL_NONE
					tlCrewT = "CHARC_CRD1"
				BREAK
				CASE CREW_DECAL_BIG
					tlCrewT = "CHARC_CRD2"
				BREAK		
				CASE CREW_DECAL_SMALL
					tlCrewT = "CHARC_CRD3"
				BREAK	
				CASE CREW_DECAL_BACK
					tlCrewT = "CHARC_CRD4"
				BREAK				
			ENDSWITCH	
			
			SET_FRONTEND_SETTING_DATA_SLOT(CHAR_CREATOR_COL_APP, iIndex, charCreatorData.CHAR_COL2_ID, ENUM_TO_INT(APPEARANCE_CREW_DECALS), "FACE_CREWE", tlCrewT, TRUE, TRUE, bUpdate)
		ENDIF
	ENDIF
	
	DISPLAY_FRONTEND_DATA_SLOT(CHAR_CREATOR_COL_APP)
	
	IF charCreatorData.columnIndex = APPEARANCE_COLUMN
		SET_FRONTEND_HIGHLIGHT(ENUM_TO_INT(APPEARANCE_COLUMN), GET_HIGHLIGHT_ROW_FOR_APPEARANCE_INDEX(charCreatorData), TRUE)
	ENDIF
	
ENDPROC

FUNC INT GET_HIGHLIGHT_ROW_FOR_HERITAGE_INDEX(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData)
	BOOL bMumSpecial	
	IF GET_CHAR_CREATOR_PARENT_RACE(charCreatorData, CHAR_CREATOR_PED_MUM, CHAR_CREATOR_RACE_INDEX_ONE) = CHAR_CREATOR_SPECIAL_RACE
		bMumSpecial = TRUE
	ELSE
		bMumSpecial = FALSE
	ENDIF
	
	BOOL bDadSpecial	
	IF GET_CHAR_CREATOR_PARENT_RACE(charCreatorData, CHAR_CREATOR_PED_DAD, CHAR_CREATOR_RACE_INDEX_ONE) = CHAR_CREATOR_SPECIAL_RACE
		bDadSpecial = TRUE
	ELSE
		bDadSpecial = FALSE
	ENDIF	
	
	SWITCH charCreatorData.heritageIndex

		CASE HERITAGE_GENDER
			RETURN 2
		BREAK	

		CASE HERITAGE_MUM
			RETURN 3
		BREAK
		
		CASE HERITAGE_MUM2
			RETURN 4
		BREAK
		
		CASE HERITAGE_MUM_DOMINANCE
			RETURN 5
		BREAK
		
		CASE HERITAGE_DAD
			IF bMumSpecial
				RETURN 4
			ELSE
				RETURN 6
			ENDIF
		BREAK
		
		CASE HERITAGE_DAD2
			IF bMumSpecial
				RETURN 5
			ELSE
				RETURN 7
			ENDIF
		BREAK
		
		CASE HERITAGE_DAD_DOMINANCE
			IF bMumSpecial
				RETURN 6
			ELSE
				RETURN 8
			ENDIF
		BREAK			
		
		CASE HERITAGE_DOMINANCE
			IF bMumSpecial
				IF bDadSpecial
					RETURN 5
				ELSE
					RETURN 7
				ENDIF
			ELSE
				IF bDadSpecial
					RETURN 7
				ELSE
					RETURN 9
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH

	RETURN 0
ENDFUNC

/// PURPOSE: Populates the scaleform for the middle column when Heritage is selected
PROC SETUP_CHAR_CREATOR_HERITAGE(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData, BOOL bUpdate = FALSE, BOOL bForceCancel = FALSE, BOOL bUpdateGrandparents = FALSE)
	
	PRINTLN("[CHARCREATOR] SETUP_CHAR_CREATOR_HERITAGE")
	
	INT iTeam = GET_CHAR_CREATOR_COLOUR()
	TEXT_LABEL_15 tlParentRace
	CHAR_CREATOR_RACE aRace
	INT iIndex = 0
	//INT iHighlight = 0
	
	IF NOT bUpdate
		CLEAR_FRONTEND_DATA_SLOT(CHAR_CREATOR_COL_HERITAGE)
	ENDIF
	
	// work out what to highlight
	INT iGPSil1 = 3
	INT iGPSil2 = 2
	INT iGPSil3 = 3
	INT iGPSil4 = 2
	INT iGPHighlight = 0
	
	IF bUpdateGrandparents
	AND NOT IS_BIT_SET(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_DONT_UPDATE_GRANDPARENTS)
	
		IF charCreatorData.columnIndex = HERITAGE_COLUMN
		AND NOT bForceCancel
			SWITCH charCreatorData.heritageIndex
				CASE HERITAGE_MUM
					iGPSil1 = 0
					iGPSil2 = 0
					iGPHighlight = 1
				BREAK
				CASE HERITAGE_MUM2
					iGPSil1 = 0
					iGPSil2 = 0
					iGPHighlight = 2
				BREAK
				CASE HERITAGE_MUM_DOMINANCE
					iGPSil1 = 0
					iGPSil2 = 0
				BREAK
				CASE HERITAGE_DAD
					iGPSil3 = 0
					iGPSil4 = 0
					iGPHighlight = 3
				BREAK
				CASE HERITAGE_DAD2
					iGPSil3 = 0
					iGPSil4 = 0
					iGPHighlight = 4
				BREAK
				CASE HERITAGE_DAD_DOMINANCE
					iGPSil3 = 0
					iGPSil4 = 0
				BREAK
			ENDSWITCH
		ENDIF
		
		IF GET_CHAR_CREATOR_PARENT_RACE(charCreatorData, CHAR_CREATOR_PED_MUM, CHAR_CREATOR_RACE_INDEX_ONE) = CHAR_CREATOR_SPECIAL_RACE
		OR IS_BIT_SET(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_MAINTAIN_SPECIAL_MUM)
			iGPSil1 = 3
			iGPSil2 = 2
			IF iGPHighlight=1
			OR iGPHighlight=2
				iGPHighlight=0
			ENDIF
		ENDIF
		
		IF GET_CHAR_CREATOR_PARENT_RACE(charCreatorData, CHAR_CREATOR_PED_DAD, CHAR_CREATOR_RACE_INDEX_ONE) = CHAR_CREATOR_SPECIAL_RACE
		OR IS_BIT_SET(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_MAINTAIN_SPECIAL_DAD)
			iGPSil3 = 3
			iGPSil4 = 2
			IF iGPHighlight=3
			OR iGPHighlight=4
				iGPHighlight=0
			ENDIF
		ENDIF		
		
		charCreatorData.iCacheGPSil1 = iGPSil1
		charCreatorData.iCacheGPSil2 = iGPSil2
		charCreatorData.iCacheGPSil3 = iGPSil3 
		charCreatorData.iCacheGPSil4 = iGPSil4 
		charCreatorData.iCacheGPHighlight = iGPHighlight 
	ELSE
		iGPSil1 = charCreatorData.iCacheGPSil1
		iGPSil2 = charCreatorData.iCacheGPSil2
		iGPSil3 = charCreatorData.iCacheGPSil3
		iGPSil4 = charCreatorData.iCacheGPSil4
		iGPHighlight = charCreatorData.iCacheGPHighlight
	ENDIF
	
	MPLOBBY_SET_DATA_SLOT_FACE(CHAR_CREATOR_COL_HERITAGE, bUpdate, iIndex, charCreatorData.CHAR_COL2_ID, 0, 3, 0.0, "", "CHARC_GRN", -1, iTeam, -1, -1, FALSE, -1, TRUE, iGPSil1, iGPSil2, iGPSil3, iGPSil4, iGPHighlight)
	
	
	iIndex++
	
	// Default picture frame of parents
	BOOL bHighlightMomAndDad = FALSE
	IF charCreatorData.columnIndex = HERITAGE_COLUMN
		IF charCreatorData.heritageIndex = HERITAGE_DAD_DOMINANCE
		OR charCreatorData.heritageIndex = HERITAGE_MUM_DOMINANCE
			bHighlightMomAndDad = TRUE
		ENDIF
	ENDIF	
	MPLOBBY_SET_DATA_SLOT_FACE(CHAR_CREATOR_COL_HERITAGE, bUpdate, iIndex, charCreatorData.CHAR_COL2_ID, 1, 0, 0.0, "", "CHARC_MAD", -1, iTeam, -1, -1, FALSE, -1,	DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, bHighlightMomAndDad)
	
	iIndex++
	
	
	MPLOBBY_SET_DATA_SLOT_FACE(CHAR_CREATOR_COL_HERITAGE, bUpdate, iIndex, 	charCreatorData.CHAR_COL2_ID, ENUM_TO_INT(HERITAGE_GENDER), 2, 0.0, "FACES_GEN", GET_CHAR_CREATOR_GENDER_LABEL(charCreatorData), -1, iTeam)

	iIndex++
	
	aRace = GET_CHAR_CREATOR_PARENT_RACE(charCreatorData, CHAR_CREATOR_PED_MUM, CHAR_CREATOR_RACE_INDEX_ONE)
	tlParentRace = GET_CHAR_CREATOR_RACE_STRING(charCreatorData, CHAR_CREATOR_PED_MUM, CHAR_CREATOR_RACE_INDEX_ONE)
	
	BOOL bArrows = TRUE
	TEXT_LABEL_15 tlParent = "FACE_MUM"
	IF aRace = CHAR_CREATOR_SPECIAL_RACE
		 tlParent = "FACE_MUMS"
		 bArrows = FALSE
	ENDIF

	MPLOBBY_SET_DATA_SLOT_FACE(CHAR_CREATOR_COL_HERITAGE, bUpdate, iIndex, charCreatorData.CHAR_COL2_ID, ENUM_TO_INT(HERITAGE_MUM), 2, 0.0, tlParent, tlParentRace, -1, iTeam, -1, -1, FALSE, charCreatorData.iHeadForMumRace + 1, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, bArrows)
	
	//IF charCreatorData.heritageIndex = HERITAGE_MUM
	//	iHighlight = iIndex
	//ENDIF
	iIndex++
	
	IF aRace != CHAR_CREATOR_SPECIAL_RACE
		tlParentRace = GET_CHAR_CREATOR_RACE_STRING(charCreatorData, CHAR_CREATOR_PED_MUM, CHAR_CREATOR_RACE_INDEX_TWO)
		MPLOBBY_SET_DATA_SLOT_FACE(CHAR_CREATOR_COL_HERITAGE, bUpdate, iIndex, charCreatorData.CHAR_COL2_ID, ENUM_TO_INT(HERITAGE_MUM2), 2, 0.0, "FACE_GMUM", tlParentRace, -1, iTeam, -1, -1, FALSE, charCreatorData.iHeadForMumRace2 + 1)
		
		//IF charCreatorData.heritageIndex = HERITAGE_MUM2
		//	iHighlight = iIndex
		//ENDIF
			
		iIndex++
		
		MPLOBBY_SET_DATA_SLOT_FACE(CHAR_CREATOR_COL_HERITAGE, bUpdate, iIndex, charCreatorData.CHAR_COL2_ID, ENUM_TO_INT(HERITAGE_MUM_DOMINANCE), 1, charCreatorData.pedData.fMumDominance, "FACES_DOM_M", "FACES_DOM_M", -1, iTeam, -1, -1, TRUE)
		
		
		//IF charCreatorData.heritageIndex = HERITAGE_MUM_DOMINANCE
		//	iHighlight = iIndex
		//ENDIF
		iIndex++	
	ELSE
		IF charCreatorData.heritageIndex = HERITAGE_MUM2
		OR charCreatorData.heritageIndex = HERITAGE_MUM_DOMINANCE
			charCreatorData.heritageIndex = HERITAGE_MUM
		ENDIF
	ENDIF
	
	aRace = GET_CHAR_CREATOR_PARENT_RACE(charCreatorData, CHAR_CREATOR_PED_DAD, CHAR_CREATOR_RACE_INDEX_ONE)
	tlParentRace = GET_CHAR_CREATOR_RACE_STRING(charCreatorData, CHAR_CREATOR_PED_DAD, CHAR_CREATOR_RACE_INDEX_ONE)
	
	bArrows = TRUE
	tlParent = "FACE_DAD"
	IF aRace = CHAR_CREATOR_SPECIAL_RACE
		 tlParent = "FACE_DADS"
		 
		 IF NOT ARE_CHAR_CREATOR_COLLECTOR_EDITIONS_AVAILABLE()
		 	bArrows = FALSE
		 ENDIF		 
	ENDIF
	
	
	MPLOBBY_SET_DATA_SLOT_FACE(CHAR_CREATOR_COL_HERITAGE, bUpdate, iIndex, 		charCreatorData.CHAR_COL2_ID, ENUM_TO_INT(HERITAGE_DAD), 2, 0.0, tlParent, tlParentRace, -1, iTeam, -1, -1, FALSE, charCreatorData.iHeadForDadRace + 1, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, bArrows)

	//IF charCreatorData.heritageIndex = HERITAGE_DAD
	//	iHighlight = iIndex
	//ENDIF
	iIndex++

	IF aRace != CHAR_CREATOR_SPECIAL_RACE
		tlParentRace = GET_CHAR_CREATOR_RACE_STRING(charCreatorData, CHAR_CREATOR_PED_DAD, CHAR_CREATOR_RACE_INDEX_TWO)
		MPLOBBY_SET_DATA_SLOT_FACE(CHAR_CREATOR_COL_HERITAGE, bUpdate, iIndex, 		charCreatorData.CHAR_COL2_ID, ENUM_TO_INT(HERITAGE_DAD2), 2, 0.0, "FACE_GDAD", tlParentRace, -1, iTeam, -1, -1, FALSE, charCreatorData.iHeadForDadRace2 + 1)

		//IF charCreatorData.heritageIndex = HERITAGE_DAD2
		//	iHighlight = iIndex
		//ENDIF
		
		iIndex++
		
		MPLOBBY_SET_DATA_SLOT_FACE(CHAR_CREATOR_COL_HERITAGE, bUpdate, iIndex, charCreatorData.CHAR_COL2_ID, ENUM_TO_INT(HERITAGE_DAD_DOMINANCE), 1, charCreatorData.pedData.fDadDominance, "FACES_DOM_D", "FACES_DOM_D", -1, iTeam, -1, -1, TRUE)
		//IF charCreatorData.heritageIndex = HERITAGE_DAD_DOMINANCE
		//	iHighlight = iIndex
		//ENDIF
		iIndex++	
	ELSE
		IF charCreatorData.heritageIndex = HERITAGE_DAD2
		OR charCreatorData.heritageIndex = HERITAGE_DAD_DOMINANCE
			charCreatorData.heritageIndex = HERITAGE_DAD
		ENDIF
	ENDIF		
	
	MPLOBBY_SET_DATA_SLOT_FACE(CHAR_CREATOR_COL_HERITAGE, bUpdate, iIndex, charCreatorData.CHAR_COL2_ID, ENUM_TO_INT(HERITAGE_DOMINANCE), 1, charCreatorData.pedData.fParentDominance, "FACES_DOM", "FACES_DOM", -1, iTeam, -1, -1, TRUE)
	//IF charCreatorData.heritageIndex = HERITAGE_DOMINANCE
	//	iHighlight = iIndex
	//ENDIF
	iIndex++

	IF NOT bUpdate
		DISPLAY_FRONTEND_DATA_SLOT(CHAR_CREATOR_COL_HERITAGE)
		IF charCreatorData.columnIndex = HERITAGE_COLUMN
			SET_FRONTEND_HIGHLIGHT(CHAR_CREATOR_COL_HERITAGE, GET_HIGHLIGHT_ROW_FOR_HERITAGE_INDEX(charCreatorData), TRUE)
			//SET_FRONTEND_HIGHLIGHT(CHAR_CREATOR_COL_HERITAGE, iHighlight, TRUE)
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_DONT_UPDATE_GRANDPARENTS)
		IF bUpdateGrandparents
			PRINTSTRING("updating grandparents 2") PRINTNL()
			UPDATE_GRANDAD_HEAD(charCreatorData, TRUE)
			UPDATE_GRANDMA_HEAD(charCreatorData, TRUE)
			UPDATE_GRANDAD_HEAD(charCreatorData, FALSE)
			UPDATE_GRANDMA_HEAD(charCreatorData, FALSE)
		ENDIF
	ENDIF
	
	CLEAR_BIT(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_DONT_UPDATE_GRANDPARENTS)
ENDPROC

/// PURPOSE: Populates the scaleform for the middle column when Lifestyle is selected
PROC SETUP_CHAR_CREATOR_LIFESTYLE(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData, BOOL bUpdate = FALSE)

	PRINTLN("[CHARCREATOR] SETUP_CHAR_CREATOR_LIFESTYLE")

	INT iTeam = GET_CHAR_CREATOR_COLOUR()

	INT i
	INT iHours
	CHAR_CREATOR_LIFESTYLE aLifestyle
	TEXT_LABEL_15 tlHourLabel
	
	// Loop through our lifestyle bars
	REPEAT MAX_LIFESTYLE i
	
		tlHourLabel = "FACES_HOURS"
		aLifestyle = INT_TO_ENUM(CHAR_CREATOR_LIFESTYLE, i)
		iHours = GET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, aLifestyle)
		
		// If hour is not plural, change text label
		IF (iHours * 0.5) = 1
			tlHourLabel = "FACES_HOUR"
		ENDIF
		
		MPLOBBY_SET_DATA_SLOT_FACE(	CHAR_CREATOR_COL_LIFESTYLE, bUpdate, i, charCreatorData.CHAR_COL2_ID, i, 0, TO_FLOAT(iHours), GET_CHAR_CREATOR_LIFESTYLE_TEXT_LABEL(aLifestyle), tlHourLabel, 
									(iHours * 0.5), iTeam, GET_CHAR_CREATOR_HOURS_BOUNDS_FOR_LIFESTYLE(aLifestyle, FALSE), GET_CHAR_CREATOR_HOURS_BOUNDS_FOR_LIFESTYLE(aLifestyle, TRUE))
	
	ENDREPEAT

	MPLOBBY_SET_DATA_SLOT_FACE(	CHAR_CREATOR_COL_LIFESTYLE, bUpdate, ENUM_TO_INT(MAX_LIFESTYLE), charCreatorData.CHAR_COL2_ID, 0, 1,
								(GET_CHAR_CREATOR_HOURS_LEFT(charCreatorData) * 0.5), "FACE_ASSIGN", "FACES_HOURS", (GET_CHAR_CREATOR_HOURS_LEFT(charCreatorData) * 0.5), iTeam, 0, 24, FALSE)


	IF NOT bUpdate
		DISPLAY_FRONTEND_DATA_SLOT(CHAR_CREATOR_COL_LIFESTYLE)
		charCreatorData.lifestyleIndex = LIFESTYLE_SLEEP
	ENDIF

ENDPROC

/// PURPOSE: Populates the scaleform for the player column on the right hand side
PROC SETUP_CHAR_CREATOR_PLAYER_DETAILS(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData, BOOL bUpdate = FALSE, BOOL bAllTrue = FALSE)//, BOOL bHighlightStamina = TRUE, BOOL bHighlightShooting = TRUE, BOOL bHighlightStrength = TRUE, BOOL bHighlightStealth = TRUE, BOOL bHighlightFlying = TRUE, BOOL bHighlightDriving = TRUE, BOOL bHighlightLung = TRUE)
	
	PRINTLN("[CHARCREATOR] SETUP_CHAR_CREATOR_PLAYER_DETAILS")

	INT iTeam = GET_CHAR_CREATOR_COLOUR()
	
	// highlight particular stats?
	BOOL bIncreaseVal
	BOOL bHighlightStamina = TRUE
	BOOL bHighlightShooting = TRUE
	BOOL bHighlightStrength = TRUE
	BOOL bHighlightStealth = TRUE
	BOOL bHighlightFlying = TRUE
	BOOL bHighlightDriving = TRUE
	BOOL bHighlightLung = TRUE
	
	IF NOT bAllTrue
		bHighlightStamina = IS_CHAR_CREATOR_STAT_CHANGED_BY_LIFESTYLE(charCreatorData.lifestyleIndex, CHAR_CREATOR_STAT_STAMINA, bIncreaseVal)
		bHighlightShooting = IS_CHAR_CREATOR_STAT_CHANGED_BY_LIFESTYLE(charCreatorData.lifestyleIndex, CHAR_CREATOR_STAT_SHOOTING, bIncreaseVal)
		bHighlightStrength = IS_CHAR_CREATOR_STAT_CHANGED_BY_LIFESTYLE(charCreatorData.lifestyleIndex, CHAR_CREATOR_STAT_STRENGTH, bIncreaseVal)
		bHighlightStealth = IS_CHAR_CREATOR_STAT_CHANGED_BY_LIFESTYLE(charCreatorData.lifestyleIndex, CHAR_CREATOR_STAT_STEALTH, bIncreaseVal)
		bHighlightFlying = IS_CHAR_CREATOR_STAT_CHANGED_BY_LIFESTYLE(charCreatorData.lifestyleIndex, CHAR_CREATOR_STAT_FLYING, bIncreaseVal)
		bHighlightDriving = IS_CHAR_CREATOR_STAT_CHANGED_BY_LIFESTYLE(charCreatorData.lifestyleIndex, CHAR_CREATOR_STAT_BIKE, bIncreaseVal)
		bHighlightLung = IS_CHAR_CREATOR_STAT_CHANGED_BY_LIFESTYLE(charCreatorData.lifestyleIndex, CHAR_CREATOR_STAT_LUNG, bIncreaseVal)	
	ENDIF
	
	//IF NOT bUpdate
		MPLOBBY_SET_COLUMN_TITLE_FACE(CHAR_CREATOR_COL_PLAYER, GET_PLAYER_NAME(PLAYER_ID()), iTeam, bHighlightStamina, bHighlightShooting, bHighlightStrength, bHighlightStealth, bHighlightFlying, bHighlightDriving, bHighlightLung)
	//ENDIF
		
	IF NOT bUpdate
		DISPLAY_FRONTEND_DATA_SLOT(CHAR_CREATOR_COL_PLAYER)
	ENDIF

ENDPROC

/// PURPOSE: Clears look at task for ped
PROC CLEAR_CHAR_CREATOR_LOOK_AT(PED_INDEX &aPed)

	IF NOT IS_PED_INJURED(aPed)
		PRINTLN("[CHARCREATOR] CLEAR_CHILD_LOOK_AT")
	
		TASK_CLEAR_LOOK_AT(aPed)
	ENDIF
ENDPROC

/// PURPOSE: Cleans up everybodies look at task
PROC CLEAR_ALL_CHAR_CREATOR_LOOK_AT(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData)
	CLEAR_CHAR_CREATOR_LOOK_AT(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON])
	CLEAR_CHAR_CREATOR_LOOK_AT(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER])
	CLEAR_CHAR_CREATOR_LOOK_AT(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAD])
	CLEAR_CHAR_CREATOR_LOOK_AT(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_MUM])
	
	INT i
	REPEAT CHAR_CREATOR_SPECIAL_PEDS_MAX i
		CLEAR_CHAR_CREATOR_LOOK_AT(charCreatorData.charCreatorSpecialPeds[i])
	ENDREPEAT
		
	charCreatorData.pedData.fCharTurnValueX = 0.0
	charCreatorData.pedData.fCharTurnValueY = 0.0
ENDPROC

/// PURPOSE: Check our value is between allowed bounds
FUNC BOOL IS_CHAR_CREATOR_SELECTION_WITHIN_BOUNDS(INT iSelection, INT minValue, INT maxValue)
	RETURN (iSelection >= minValue) AND (iSelection < maxValue)
ENDFUNC


FUNC BOOL IS_CHAR_CREATOR_HERITAGE_ROW_VALID(INT iRow, MPHUD_CHARACTER_CREATOR_DATA &charCreatorData)

	IF iRow = ENUM_TO_INT(HERITAGE_MUM2)
	OR iRow = ENUM_TO_INT(HERITAGE_MUM_DOMINANCE)
		IF GET_CHAR_CREATOR_PARENT_RACE(charCreatorData, CHAR_CREATOR_PED_MUM, CHAR_CREATOR_RACE_INDEX_ONE) = CHAR_CREATOR_SPECIAL_RACE
			RETURN FALSE	
		ENDIF
	ENDIF

	IF iRow = ENUM_TO_INT(HERITAGE_DAD2)
	OR iRow = ENUM_TO_INT(HERITAGE_DAD_DOMINANCE)
		IF GET_CHAR_CREATOR_PARENT_RACE(charCreatorData, CHAR_CREATOR_PED_DAD, CHAR_CREATOR_RACE_INDEX_ONE) = CHAR_CREATOR_SPECIAL_RACE
			RETURN FALSE	
		ENDIF
	ENDIF

	RETURN TRUE
ENDFUNC



// update special parents instructional buttons
PROC UPDATE_SPECIAL_PARENTS_INSTRUCTIONAL_BUTTONS(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData)
	IF charCreatorData.columnIndex = HERITAGE_COLUMN
		IF (charCreatorData.heritageIndex = HERITAGE_MUM
		OR charCreatorData.heritageIndex = HERITAGE_MUM2
		OR charCreatorData.heritageIndex = HERITAGE_MUM_DOMINANCE)
		AND ARE_CHAR_CREATOR_COLLECTOR_EDITIONS_AVAILABLE()
			IF GET_CHAR_CREATOR_PARENT_RACE(charCreatorData, CHAR_CREATOR_PED_MUM, CHAR_CREATOR_RACE_INDEX_ONE) = CHAR_CREATOR_SPECIAL_RACE
				PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("SPECIAL_PROMPT_M"))
				PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("SPECIAL_PROMPT_D"))
				PAUSE_MENU_ACTIVATE_CONTEXT(HASH("CUSTOM_PROMPT_M"))
				PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("CUSTOM_PROMPT_D"))
			ELSE
				PAUSE_MENU_ACTIVATE_CONTEXT(HASH("SPECIAL_PROMPT_M"))
				PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("SPECIAL_PROMPT_D"))
				PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("CUSTOM_PROMPT_M"))
				PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("CUSTOM_PROMPT_D"))
			ENDIF
		ELIF (charCreatorData.heritageIndex = HERITAGE_DAD
		OR charCreatorData.heritageIndex = HERITAGE_DAD2
		OR charCreatorData.heritageIndex = HERITAGE_DAD_DOMINANCE)
		AND (ARE_CHAR_CREATOR_COLLECTOR_EDITIONS_AVAILABLE() OR IS_GAME_LINKED_TO_SOCIAL_CLUB())
			IF GET_CHAR_CREATOR_PARENT_RACE(charCreatorData, CHAR_CREATOR_PED_DAD, CHAR_CREATOR_RACE_INDEX_ONE) = CHAR_CREATOR_SPECIAL_RACE
				PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("SPECIAL_PROMPT_M"))
				PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("SPECIAL_PROMPT_D"))
				PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("CUSTOM_PROMPT_M"))
				PAUSE_MENU_ACTIVATE_CONTEXT(HASH("CUSTOM_PROMPT_D"))
			ELSE
				PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("SPECIAL_PROMPT_M"))
				PAUSE_MENU_ACTIVATE_CONTEXT(HASH("SPECIAL_PROMPT_D"))
				PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("CUSTOM_PROMPT_M"))
				PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("CUSTOM_PROMPT_D"))
			ENDIF	
		ELSE
			PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("SPECIAL_PROMPT_M"))
			PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("SPECIAL_PROMPT_D"))
			PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("CUSTOM_PROMPT_M"))
			PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("CUSTOM_PROMPT_D"))
		ENDIF
	ELSE
		PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("SPECIAL_PROMPT_M"))
		PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("SPECIAL_PROMPT_D"))
		PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("CUSTOM_PROMPT_M"))
		PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("CUSTOM_PROMPT_D"))
	ENDIF
ENDPROC


/// PURPOSE: Process a player moving up and down within a column
PROC PROCESS_CHAR_CREATOR_UPDOWN(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData, INT iDirection)
	
	INT iNewRow
	SWITCH charCreatorData.columnIndex
	
		CASE MAIN_MENU_COLUMN
			
			iNewRow = ENUM_TO_INT(charCreatorData.mainMenuIndex) + (-1*iDirection)
			
			IF iNewRow < 0
				iNewRow = ENUM_TO_INT(MAX_MAIN_MENU_OPTIONS)-1
			ENDIF
			
			IF iNewRow >= ENUM_TO_INT(MAX_MAIN_MENU_OPTIONS)
				iNewRow = 0
			ENDIF
			
			charCreatorData.prevMainMenuIndex = charCreatorData.mainMenuIndex
			charCreatorData.mainMenuIndex = INT_TO_ENUM(CHAR_CREATOR_MAIN_MENU, iNewRow)
			
			PRINTLN("[CHARCREATOR] PROCESS_CHAR_CREATOR_UPDOWN - moved in MAIN MENU: ", iNewRow)
			
			// Simulate the player input up / down
			MPLOBBY_SET_INPUT_EVENT(iDirection)
			
			// Update the visuals of the middle column
			SET_CHAR_CREATOR_COLUMNS_TO_SELECTION(charCreatorData)
			
			// Highlight our selection on the left
			SET_FRONTEND_HIGHLIGHT(ENUM_TO_INT(MAIN_MENU_COLUMN), ENUM_TO_INT(charCreatorData.mainMenuIndex), TRUE)
			
			// Update our help text
			SET_CHAR_CREATOR_HELP_TEXT(charCreatorData)
						
			// Remove our look at of peds
			//CLEAR_ALL_CHAR_CREATOR_LOOK_AT(charCreatorData)
			PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", "HUD_FRONTEND_DEFAULT_SOUNDSET")
			
			// lifestyle zoom out
			IF charCreatorData.mainMenuIndex = LIFESTYLE_CAT
				IF charCreatorData.fCharZoom < LIFESTYLE_ZOOM
					charCreatorData.fCharZoom = LIFESTYLE_ZOOM

					PROCESS_CHAR_CREATOR_ZOOM(charCreatorData, 0)
				ENDIF
			ELIF charCreatorData.mainMenuIndex = HERITAGE_CAT
			OR charCreatorData.mainMenuIndex = APPEARANCE_CAT
				charCreatorData.fCharZoom = GENERAL_CLOSEST_ZOOM
				
				PROCESS_CHAR_CREATOR_ZOOM(charCreatorData, 0)
			ENDIF
			
			// random option 
			IF charCreatorData.mainMenuIndex = HERITAGE_CAT
				PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("CONTINUE_OPTION"))
			ELSE
				PAUSE_MENU_ACTIVATE_CONTEXT(HASH("CONTINUE_OPTION"))
			ENDIF
			
			// accessories
			IF charCreatorData.mainMenuIndex = HERITAGE_CAT
				IF DOES_ENTITY_EXIST(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON])
					CLEAR_ALL_PED_PROPS(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON])
				ENDIF
				IF DOES_ENTITY_EXIST(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON_DUMMY])
					CLEAR_ALL_PED_PROPS(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON_DUMMY])
				ENDIF
				IF DOES_ENTITY_EXIST(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER])
					CLEAR_ALL_PED_PROPS(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER])
				ENDIF
				IF DOES_ENTITY_EXIST(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER_DUMMY])
					CLEAR_ALL_PED_PROPS(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER_DUMMY])
				ENDIF
			ELSE
				IF IS_BIT_SET(charCreatorData.iCharCreatorBitset, CHAR_CREATOR_ALL_PEDS_LOADED)
					SET_CHAR_CREATOR_OUTFIT(charCreatorData, TRUE, TRUE, FALSE, FALSE, TRUE)
					charCreatorData.iSetSonDummyTime = 0		
					charCreatorData.iSetDaughterDummyTime = 0		
				ENDIF
			ENDIF
		BREAK	

		// If we are in heritage column...
		CASE HERITAGE_COLUMN
			
			BOOL bHerFound
			iNewRow = ENUM_TO_INT(charCreatorData.heritageIndex)
			
			WHILE (NOT bHerFound)
			
				iNewRow = iNewRow + (-1*iDirection)
			
				IF iNewRow < 2
					iNewRow = ENUM_TO_INT(MAX_HERITAGE)-1
				ENDIF
				
				IF iNewRow >= ENUM_TO_INT(MAX_HERITAGE)
					iNewRow = 2
				ENDIF
			
				IF IS_CHAR_CREATOR_HERITAGE_ROW_VALID(iNewRow, charCreatorData)
					bHerFound = TRUE
				ENDIF
			ENDWHILE
						
			charCreatorData.heritageIndex = INT_TO_ENUM(CHAR_CREATOR_HERITAGE, iNewRow)
			
			PRINTLN("[CHARCREATOR] PROCESS_CHAR_CREATOR_UPDOWN - moved in HERITAGE: ", iNewRow)
			
			//SETUP_CHAR_CREATOR_HERITAGE(charCreatorData)
			
	
			IF IS_BIT_SET(charCreatorData.iCharCreatorBitset, CHAR_CREATOR_ALL_PEDS_LOADED)
				SET_BIT(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_FORCE_HERITAGE_UPDATE)
			ELSE
				SETUP_CHAR_CREATOR_HERITAGE(charCreatorData, FALSE, FALSE, TRUE)
			ENDIF
			
			
			UPDATE_SPECIAL_PARENTS_INSTRUCTIONAL_BUTTONS(charCreatorData)
			PAUSE_MENU_REDRAW_INSTRUCTIONAL_BUTTONS()			
			
			// Update our help text
			SET_CHAR_CREATOR_HELP_TEXT(charCreatorData)
			
			//CLEAR_ALL_CHAR_CREATOR_LOOK_AT(charCreatorData)
			PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", "HUD_FRONTEND_DEFAULT_SOUNDSET")

		BREAK
	
		// If we are in lifestyle column...
		CASE LIFESTYLE_COLUMN
			iNewRow = ENUM_TO_INT(charCreatorData.lifestyleIndex) + (-1*iDirection)
						
			IF iNewRow < 0
				iNewRow = ENUM_TO_INT(MAX_LIFESTYLE)-1
			ENDIF
			
			IF iNewRow >= ENUM_TO_INT(MAX_LIFESTYLE)
				iNewRow = 0
			ENDIF
			
			charCreatorData.lifestyleIndex = INT_TO_ENUM(CHAR_CREATOR_LIFESTYLE, iNewRow)
			
			PRINTLN("[CHARCREATOR] PROCESS_CHAR_CREATOR_UPDOWN - moved in LIFESTYLE: ", iNewRow)
			
			MPLOBBY_SET_INPUT_EVENT(iDirection)
			SET_FRONTEND_HIGHLIGHT(ENUM_TO_INT(LIFESTYLE_COLUMN), ENUM_TO_INT(charCreatorData.lifestyleIndex), TRUE)
			
			// Update our help text
			SET_CHAR_CREATOR_HELP_TEXT(charCreatorData)
		
			SETUP_CHAR_CREATOR_PLAYER_DETAILS(charCreatorData, TRUE)//, bHighlightStamina, bHighlightShooting, bHighlightStrength, bHighlightStealth, bHighlightFlying, bHighlightDriving, bHighlightLung)
			
			PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", "HUD_FRONTEND_DEFAULT_SOUNDSET")
		BREAK
	
		CASE APPEARANCE_COLUMN
			iNewRow = ENUM_TO_INT(charCreatorData.appearanceIndex) + (-1*iDirection)

			BOOL bFoundRow
			WHILE NOT bFoundRow
				
				IF iNewRow < 0
					iNewRow = ENUM_TO_INT(MAX_APPEARANCE)-1
				ENDIF
				
				IF iNewRow >= ENUM_TO_INT(MAX_APPEARANCE)
					iNewRow = 0
				ENDIF
				
				bFoundRow = TRUE
				
				IF iNewRow  = ENUM_TO_INT(APPEARANCE_HAIR_COLOUR)
				AND NOT DOES_CHAR_CREATOR_HAIR_HAVE_COLOURS(charCreatorData)
					bFoundRow = FALSE
				ELIF iNewRow  = ENUM_TO_INT(APPEARANCE_BEARD)
				AND NOT IS_CHAR_CREATOR_PED_MALE(charCreatorData)
					bFoundRow = FALSE
				ELIF iNewRow  = ENUM_TO_INT(APPEARANCE_MAKEUP)
				AND IS_CHAR_CREATOR_PED_MALE(charCreatorData)
					bFoundRow = FALSE
				ELIF (iNewRow = ENUM_TO_INT(APPEARANCE_CREW_TSHIRT) OR iNewRow = ENUM_TO_INT(APPEARANCE_CREW_DECALS))
				AND NOT IS_LOCAL_PLAYER_IN_ACTIVE_CLAN()
					bFoundRow = FALSE
				ELIF (iNewRow = ENUM_TO_INT(APPEARANCE_CREW_DECALS) AND charCreatorData.crewTShirtOption = CREW_TSHIRT_OFF)
					bFoundRow = FALSE
				ENDIF
				
				IF NOT bFoundRow
					iNewRow = iNewRow + (-1*iDirection)
				ENDIF
			ENDWHILE			
					
			charCreatorData.appearanceIndex = INT_TO_ENUM(CHAR_CREATOR_APPEARANCE, iNewRow)
			
			
			IF charCreatorData.appearanceIndex <> APPEARANCE_CREW_TSHIRT
			AND charCreatorData.appearanceIndex <> APPEARANCE_CREW_DECALS
				CLEAR_BIT(charCreatorData.iCharCreatorBitset, CHAR_CREATOR_FORCE_CREW_SHIRT_ZOOM)
			ENDIF			
			
			PRINTLN("[CHARCREATOR] PROCESS_CHAR_CREATOR_UPDOWN - moved in APPEARANCE: ", iNewRow)
			
			MPLOBBY_SET_INPUT_EVENT(iDirection)

			SET_FRONTEND_HIGHLIGHT(ENUM_TO_INT(APPEARANCE_COLUMN), GET_HIGHLIGHT_ROW_FOR_APPEARANCE_INDEX(charCreatorData), TRUE)
			
			// Update our help text
			SET_CHAR_CREATOR_HELP_TEXT(charCreatorData)
			
			//CLEAR_ALL_CHAR_CREATOR_LOOK_AT(charCreatorData)
			PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", "HUD_FRONTEND_DEFAULT_SOUNDSET")
		BREAK
	
	ENDSWITCH
ENDPROC

/// PURPOSE: Handles player pressing X button (if valid state)
PROC PROCESS_CHAR_CREATOR_SELECT(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData)
	
	// Only process this if we are on the main menu
	IF charCreatorData.columnIndex != MAIN_MENU_COLUMN
		EXIT
	ENDIF
	
	INT iRowIndex
	BOOL bShiftColumn
	
	SWITCH charCreatorData.mainMenuIndex
		CASE HERITAGE_CAT
				
			// Process moving into heritage
			charCreatorData.columnIndex = HERITAGE_COLUMN
			iRowIndex = ENUM_TO_INT(charCreatorData.heritageIndex)
			
			PRINTLN("[CHARCREATOR] PROCESS_CHAR_CREATOR_SELECT - Player moving to HERITAGE column, iRow: ", iRowIndex)
			
			//SETUP_CHAR_CREATOR_HERITAGE(charCreatorData)
			SET_BIT(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_FORCE_HERITAGE_UPDATE)
			
			bShiftColumn = TRUE
		BREAK
		
		CASE LIFESTYLE_CAT
			
			// Process moving to lifestyle
			charCreatorData.columnIndex = LIFESTYLE_COLUMN
			iRowIndex = ENUM_TO_INT(charCreatorData.lifestyleIndex)
			
			SETUP_CHAR_CREATOR_PLAYER_DETAILS(charCreatorData, TRUE)
			
			PRINTLN("[CHARCREATOR] PROCESS_CHAR_CREATOR_SELECT - Player moving to LIFESTYLE column, iRow: ", iRowIndex)
			
			bShiftColumn = TRUE
		BREAK
		
		CASE APPEARANCE_CAT
		
			// Process moving to appearance
			charCreatorData.columnIndex = APPEARANCE_COLUMN
			iRowIndex = ENUM_TO_INT(charCreatorData.appearanceIndex)
			
			PRINTLN("[CHARCREATOR] PROCESS_CHAR_CREATOR_SELECT - Player moving to APPEARANCE column, iRow: ", iRowIndex)
			
			bShiftColumn = TRUE
		BREAK
		
		// Handle player attempting to continue into GTA online
		CASE CONFIRM_CAT
		
			IF NOT ARE_ALL_CHAR_CREATOR_PEDS_LOADED(charCreatorData)
				EXIT
			ENDIF		
		
			IF GET_CHAR_CREATOR_HOURS_LEFT(charCreatorData) > 0
			
				PRINTLN("[CHARCREATOR] PROCESS_CHAR_CREATOR_SELECT - Player pressed Join Game on screen but HOURS left to assign")
			
				SET_BIT(charCreatorData.iCharCreatorButtons, CHAR_CREATOR_ACCEPT)

				charCreatorData.charCreatorSubState = CHARACTER_CREATOR_HOUR_WARNING
				charCreatorData.charCreatorState = CHARACTER_CREATOR_WARNING_MESSAGE

				PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
				EXIT
			
			ELSE		
				PRINTLN("[CHARCREATOR] PROCESS_CHAR_CREATOR_SELECT - Player pressed Join Game on screen")
			
				SET_BIT(charCreatorData.iCharCreatorButtons, CHAR_CREATOR_ACCEPT)
				
				charCreatorData.sfForeground = REQUEST_SCALEFORM_MOVIE("MP_CELEBRATION_BG")
				charCreatorData.sfBackground = REQUEST_SCALEFORM_MOVIE("MP_CELEBRATION_FG")
				charCreatorData.sfCelebration = REQUEST_SCALEFORM_MOVIE("MP_CELEBRATION")
				
				charCreatorData.charCreatorState = CHARACTER_CREATOR_INPUT_NAME

				PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
			ENDIF
		BREAK
	ENDSWITCH
	
	IF bShiftColumn
		MPLOBBY_HIGHLIGHT_COLUMN(1)
		SET_FRONTEND_COLUMN_FOCUS(ENUM_TO_INT(charCreatorData.columnIndex), TRUE, TRUE)
		
		INT iHighlightIndex = iRowIndex
		IF charCreatorData.columnIndex = APPEARANCE_COLUMN
			iHighlightIndex = GET_HIGHLIGHT_ROW_FOR_APPEARANCE_INDEX(charCreatorData)
		ELIF charCreatorData.columnIndex = HERITAGE_COLUMN
			iHighlightIndex = GET_HIGHLIGHT_ROW_FOR_HERITAGE_INDEX(charCreatorData)
		ENDIF
		
		SET_FRONTEND_HIGHLIGHT(ENUM_TO_INT(charCreatorData.columnIndex), iHighlightIndex, TRUE)

		// Update our help text
		SET_CHAR_CREATOR_HELP_TEXT(charCreatorData)
		
		PAUSE_MENU_ACTIVATE_CONTEXT(HASH("CHAR_BACK"))
		UPDATE_SPECIAL_PARENTS_INSTRUCTIONAL_BUTTONS(charCreatorData)
		PAUSE_MENU_REDRAW_INSTRUCTIONAL_BUTTONS()
		
		//PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
	ENDIF
ENDPROC

/// PURPOSE: Process player having confirmed name and moving through to confirmation screen
PROC PROCESS_CHAR_CREATOR_NAME_ENTERED(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData)
	charCreatorData.charCreatorSubState = CHARACTER_CREATOR_CONFIRMING
	charCreatorData.charCreatorState = CHARACTER_CREATOR_JOINING_GTA_ONLINE
	charCreatorData.fCacheCharZoom = charCreatorData.fCharZoom
	charCreatorData.fCharZoom = GENERAL_CLOSEST_ZOOM
	charCreatorData.bHasMadeAlteration = TRUE

	UI3DSCENE_CLEAR_PATCHED_DATA()

	SET_FRONTEND_DISPLAY_COLUMN(CHAR_CREATOR_COL_MAIN_MENU, FALSE)
	SET_FRONTEND_DISPLAY_COLUMN(CHAR_CREATOR_COL_HERITAGE, FALSE)
	SET_FRONTEND_DISPLAY_COLUMN(CHAR_CREATOR_COL_LIFESTYLE, FALSE)
	SET_FRONTEND_DISPLAY_COLUMN(CHAR_CREATOR_COL_APP, FALSE)
	SET_FRONTEND_DISPLAY_COLUMN(CHAR_CREATOR_COL_PLAYER, FALSE)
	
	CLEAR_ALL_CHAR_CREATOR_LOOK_AT(charCreatorData)
	
	IF BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND_HEADER("SHOW_MENU")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
	
	IF BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND_HEADER("SHOW_HEADING_DETAILS")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
	
	IF BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND_HEADER("SET_HEADER_TITLE")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
	
	PAUSE_MENU_ACTIVATE_CONTEXT(HASH("CONFIRM_PED"))
	PAUSE_MENU_REDRAW_INSTRUCTIONAL_BUTTONS()
ENDPROC


/// PURPOSE: Reset our local variables for character creator screen
PROC RESET_CHARACTER_CREATOR_DATA(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData)

	PRINTLN("[CHARCREATOR] RESET_CHARACTER_CREATOR_DATA called...")

	#IF IS_DEBUG_BUILD
		IF DOES_WIDGET_GROUP_EXIST(charCreatorData.widgetGroup)
			DELETE_WIDGET_GROUP(charCreatorData.widgetGroup)
		ENDIF
	#ENDIF

	charCreatorData.charCreatorState 			= CHARACTER_CREATOR_INIT
	charCreatorData.charCreatorPedState			= CHARACTER_CREATOR_PED_CREATE
	charCreatorData.charCreatorSubState 		= CHARACTER_CREATOR_QUITING
	charCreatorData.charCreatorConfirmState		= CHARACTER_CREATOR_CONFIRM_LOADING_UI
	charCreatorData.columnIndex					= MAIN_MENU_COLUMN
	charCreatorData.prevMainMenuIndex			= INT_TO_ENUM(CHAR_CREATOR_MAIN_MENU, 0)
	charCreatorData.mainMenuIndex				= INT_TO_ENUM(CHAR_CREATOR_MAIN_MENU, 0)
	charCreatorData.heritageIndex 				= INT_TO_ENUM(CHAR_CREATOR_HERITAGE, 2)
	charCreatorData.lifestyleIndex				= LIFESTYLE_SLEEP
	charCreatorData.appearanceIndex				= APPEARANCE_AGE
	charCreatorData.iCharCreatorBitSet			= 0
	charCreatorData.iCharCreatorButtons 		= 0
	charCreatorData.fCharZoom					= LIFESTYLE_ZOOM
	charCreatorData.vCharOffset					= <<CHAR_CREATOR_ZOOM_MAX_X, CHAR_CREATOR_ZOOM_MAX_Y, CHAR_CREATOR_ZOOM_MAX_Z>>
	charCreatorData.pedData.iMumRotation		= 0
	charCreatorData.pedData.iDadRotation		= 0
	charCreatorData.pedData.iChildRotation		= 0
	charCreatorData.pedData.fCharTurnValueX		= 0.0
	charCreatorData.pedData.fCharTurnValueY		= 0.0
	charCreatorData.pedData.fParentDominance	= 50.0
	charCreatorData.pedData.fMumDominance		= 50.0
	charCreatorData.pedData.fDadDominance		= 50.0
	charCreatorData.pedData.charMood			= CHAR_CREATOR_NORMAL
	charCreatorData.pedData.iCharAge			= CHAR_CREATOR_MIN_AGE
	charCreatorData.bInitialisedOutfit			= FALSE
	charCreatorData.crewTShirtOption			= CREW_TSHIRT_OFF
	charCreatorData.crewDecalOption				= CREW_DECAL_NONE
	charCreatorData.iNextLookAroundTime			= GET_GAME_TIMER() + 10000
	charCreatorData.bForcingLookAround			= FALSE
	charCreatorData.bZoomingSound				= FALSE
	charCreatorData.bHasMadeAlteration			= FALSE
	charCreatorData.iNumberSinceSpecialDadGenerated = SPECIAL_PED_THRESHOLD
	charCreatorData.iNumberSinceSpecialMumGenerated = SPECIAL_PED_THRESHOLD
	charCreatorData.iNumberSinceUglyDadGenerated = UGLY_DAD_THRESHOLD
	charCreatorData.iNumberSinceUglyMumGenerated = UGLY_MUM_THRESHOLD	
	charCreatorData.bSonLockedToOldOutfit			= FALSE
	charCreatorData.bDaughterLockedToOldOutfit			= FALSE
	charCreatorData.bEnteredWithCrewTShirt		= FALSE
	charCreatorData.bCreatedFirstPrettyChild	= FALSE
	charCreatorData.bDisplayedGrandparents  	= FALSE
	
	charCreatorData.iCacheGPSil1 = 0
	charCreatorData.iCacheGPSil2 = 0
	charCreatorData.iCacheGPSil3 = 0
	charCreatorData.iCacheGPSil4 = 0
	charCreatorData.iCacheGPHighlight = 0
	
	charCreatorData.iCharacterPedCreationIndex = 0
	
	INT i
	REPEAT CHAR_CREATOR_STAT_TOTAL i
		charCreatorData.pedData.iCurrentStatUpgrades[i] = 0
	ENDREPEAT
	
	REPEAT CHAR_CREATOR_MAX_OVERLAYS i
		charCreatorData.pedData.charOverlays[i].iOverlayVal = -1
		charCreatorData.pedData.charOverlays[i].fOverlayBlend = 0.0
	ENDREPEAT
	
	charCreatorData.cachedMumHeritageIndex = HERITAGE_GENDER
	charCreatorData.cachedDadHeritageIndex = HERITAGE_GENDER
	CLEAR_CACHED_PARENTS(charCreatorData, TRUE)
	CLEAR_CACHED_PARENTS(charCreatorData, FALSE)

	REPEAT 6 i
		charCreatorData.bLockedMaleLifestyle[i] = FALSE
		charCreatorData.bLockedFemaleLifestyle[i] = FALSE
	ENDREPEAT
	
	REPEAT CHAR_CREATOR_MAX_RACES i 
		charCreatorData.iHeadForDadRace = 0
		charCreatorData.iHeadForDadRace2 = 0
		charCreatorData.iHeadForMumRace = 0
		charCreatorData.iHeadForMumRace2 = 0
	ENDREPEAT
	
	PROCESS_CHAR_CREATOR_ZOOM(charCreatorData, 0)
	RESET_CHAR_CREATOR_LIFESTYLE(charCreatorData)
	DELETE_CHAR_CREATOR_PEDS(charCreatorData)
ENDPROC


PROC PROCESS_CHAR_CREATOR_COMMON_CLEANUP(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData)
		
	
		
	// Clean up busy spinners
	SET_CHAR_CREATOR_CHILD_AS_BUSY(charCreatorData, FALSE)
	SET_CHAR_CREATOR_PARENTS_AS_BUSY(charCreatorData, FALSE)
	
	IF NOT HAS_SOUND_FINISHED(charCreatorData.iSoundID)
		STOP_SOUND(charCreatorData.iSoundID)
	ENDIF
	RELEASE_SOUND_ID(charCreatorData.iSoundID)
	RELEASE_NAMED_SCRIPT_AUDIO_BANK("HUD_321_GO")

	CLEAR_ADDITIONAL_TEXT(MENU_TEXT_SLOT, FALSE)

	RELEASE_CONTROL_OF_FRONTEND()
	
	PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("CONTINUE_OPTION"))
	PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("CHAR_BACK"))

//	ANIMPOSTFX_STOP_CROSSFADE()
	SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(FALSE)
	
	UI3DSCENE_MAKE_PUSHED_PRESET_PERSISTENT(FALSE)
	
	PRINTLN("[CHARCREATOR] - PROCESS_CHAR_CREATOR_COMMON_CLEANUP  - SET_SKYFREEZE_CLEAR()")
	SET_SKYFREEZE_CLEAR()	
ENDPROC


// Hacked version of this command in order to confine nastiness to char creator... need to use the saved special not the current special
PROC EQUIP_TORSO_DECALS_MP_FOR_CHAR_CREATOR(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData, PED_INDEX mPed, PED_COMP_TYPE_ENUM eType, PED_COMP_NAME_ENUM eItem, BOOL bPreviewing)
	IF NOT IS_PED_INJURED(mPed)
		IF eType = COMP_TYPE_JBIB
		OR eType = COMP_TYPE_SPECIAL
			MODEL_NAMES ePedModel = GET_ENTITY_MODEL(mPed)
			
			IF eType = COMP_TYPE_JBIB
				// if jacket jbib do the checks on the special item equipped.
				IF IS_JBIB_COMPONENT_A_JACKET(ePedModel, eItem)
					eType = COMP_TYPE_SPECIAL
					eItem = charCreatorData.savedSpecial
					CPRINTLN(DEBUG_PED_COMP, "EQUIP_TORSO_DECALS_MP: jacket jbib, so using special item")
				ELSE
					CPRINTLN(DEBUG_PED_COMP, "EQUIP_TORSO_DECALS_MP: normal jbib")
				ENDIF
				
			ELIF eType = COMP_TYPE_SPECIAL 
				// only do the torso decal stuff for specials if the jbib is a jacket
				PED_COMP_NAME_ENUM eJbib = GET_PED_COMPONENT_ITEM_CURRENT_FROM_LOOKUP(mPed, COMP_TYPE_JBIB)
				IF NOT IS_JBIB_COMPONENT_A_JACKET(ePedModel, eJbib)
					CPRINTLN(DEBUG_PED_COMP, "EQUIP_TORSO_DECALS_MP: early out. Equipping special, and the jbib isn't a jacket")
					EXIT
				ENDIF
			ENDIF	

			// always remove previous jbib / special decals
			REMOVE_TATTOOS_IN_UPGRADE_GROUP_MP(GET_HASH_KEY("torsoDecal"), mPed)
			
			// do jbib / special decals
			TATTOO_NAME_ENUM eTorsoDecalTattoo
			PRINTSTRING("checking tattoos now:") PRINTNL()
			eTorsoDecalTattoo = GET_TORSO_DECAL_TATTOO_MP(ePedModel, eType, eItem)
			IF eTorsoDecalTattoo <> INVALID_TATTOO
				// found a torso decal tattoo
				IF bPreviewing = TRUE
					GIVE_PED_TEMP_TATTOO_MP(mPed, eTorsoDecalTattoo)
				ELSE
					SET_MP_TATTOO_CURRENT(eTorsoDecalTattoo, TRUE, g_iPedComponentSlot)
				ENDIF
				CPRINTLN(DEBUG_PED_COMP, "Equipping torso decal ", eTorsoDecalTattoo, " bPreviewing= ", bPreviewing)
			ENDIF
		ENDIF
	ENDIF
ENDPROC



/// PURPOSE: Handle player leaving the character creator
PROC PROCESS_PLAYER_QUITING_CHARACTER_CREATOR(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData)
	
	PRINTLN("[CHARCREATOR] PROCESS_PLAYER_QUITING_CHARACTER_CREATOR - Player has left the creator")
	
	// if came in from alteration prompt, then restore the decals from the old jbib
	IF HAS_ENTERED_CHAR_CREATION_FROM_ALTERATION_PROMPT() 
		PED_INDEX pedToApply 
		IF charCreatorData.BenteredAsMale
			IF NOT IS_PED_INJURED(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON])
				EQUIP_TORSO_DECALS_MP_FOR_CHAR_CREATOR(charCreatorData, charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON], COMP_TYPE_JBIB, charCreatorData.savedJbib, FALSE)
				pedToApply = charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON]
			ENDIF
		ELSE
			IF NOT IS_PED_INJURED(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER])
				EQUIP_TORSO_DECALS_MP_FOR_CHAR_CREATOR(charCreatorData, charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER], COMP_TYPE_JBIB, charCreatorData.savedJbib, FALSE)
				pedToApply = charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER]
			ENDIF
		ENDIF
		
		// clear crew logo if didn't enter wearing one
		REMOVE_TATTOOS_IN_UPGRADE_GROUP_MP(GET_HASH_KEY("crewLogo"), pedToApply)	
		IF charCreatorData.enteredCrewDecalOption >= TATTOO_MP_FM_CREW_A
		AND charCreatorData.enteredCrewDecalOption <= TATTOO_MP_FM_CREW_D
			EQUIP_CREW_LOGO(pedToApply, charCreatorData.enteredCrewDecalOption)
		ENDIF		
	ENDIF
	
	RESET_CHARACTER_CREATOR_DATA(charCreatorData)
	
	SET_SRL_TUTORIAL_ACTIVE(FALSE)
	
	HUD_CHANGE_STATE(HUD_STATE_SELECT_CHARACTER_FM)
	
	SET_CURRENT_GAMEMODE(GAMEMODE_EMPTY)					
	SET_JOINING_GAMEMODE(GAMEMODE_FM)				
	TRANSITION_CHANGE_STATE(TRANSITION_STATE_TERMINATE_SESSION_AND_MOVE_INTO_HOLDING_STATE)			
					
	PROCESS_CHAR_CREATOR_COMMON_CLEANUP(charCreatorData)
	
ENDPROC

/// PURPOSE: Handle player moving back from the middle column or quitting character creator
PROC PROCESS_CHAR_CREATOR_CANCEL(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData)
	
	SWITCH charCreatorData.columnIndex
	
		CASE HERITAGE_COLUMN
		CASE LIFESTYLE_COLUMN
		CASE APPEARANCE_COLUMN
			
			IF charCreatorData.columnIndex = HERITAGE_COLUMN
				//SETUP_CHAR_CREATOR_HERITAGE(charCreatorData, DEFAULT, TRUE)
				SET_BIT(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_FORCE_HERITAGE_UPDATE)
			ENDIF
			
				
			CLEAR_BIT(charCreatorData.iCharCreatorBitset, CHAR_CREATOR_FORCE_CREW_SHIRT_ZOOM)
		
					
			charCreatorData.columnIndex = MAIN_MENU_COLUMN
			
			MPLOBBY_HIGHLIGHT_COLUMN(0)
			SET_FRONTEND_COLUMN_FOCUS(ENUM_TO_INT(charCreatorData.columnIndex), TRUE, FALSE)
			SET_FRONTEND_HIGHLIGHT(ENUM_TO_INT(charCreatorData.columnIndex), ENUM_TO_INT(charCreatorData.mainMenuIndex), TRUE)
			
			// Update our help text
			SET_CHAR_CREATOR_HELP_TEXT(charCreatorData)
			
			PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("CHAR_BACK"))
			UPDATE_SPECIAL_PARENTS_INSTRUCTIONAL_BUTTONS(charCreatorData)
			PAUSE_MENU_REDRAW_INSTRUCTIONAL_BUTTONS()

			SETUP_CHAR_CREATOR_PLAYER_DETAILS(charCreatorData, TRUE, TRUE)
			
			//PLAY_SOUND_FRONTEND(-1, "BACK", "HUD_FRONTEND_DEFAULT_SOUNDSET")
		BREAK
	
		CASE MAIN_MENU_COLUMN
		
			IF charCreatorData.bHasMadeAlteration
				PRINTLN("[CHARCREATOR] PROCESS_CHAR_CREATOR_CANCEL - Player pressed cancel on creator screen")
			
				SET_BIT(charCreatorData.iCharCreatorButtons, CHAR_CREATOR_CANCEL)
				
				IF charCreatorData.charCreatorState <> CHARACTER_CREATOR_WARNING_MESSAGE
					//PLAY_SOUND_FRONTEND(-1, "BACK", "HUD_FRONTEND_DEFAULT_SOUNDSET")
				ENDIF
				
				charCreatorData.charCreatorSubState = CHARACTER_CREATOR_QUITING
				charCreatorData.charCreatorState = CHARACTER_CREATOR_WARNING_MESSAGE
			ELSE
				// reset the stat to offer the player another chance, if came in from alteration prompt
				IF HAS_ENTERED_CHAR_CREATION_FROM_ALTERATION_PROMPT() 
					SET_MP_BOOL_CHARACTER_STAT(MP_STAT_FM_CHANGECHAR_ASKED, FALSE, charCreatorData.iCharacterSelectSlot)
				ENDIF
			
				// skip the prompt about quitting if haven't changed anything
				PROCESS_PLAYER_QUITING_CHARACTER_CREATOR(charCreatorData)
				//PLAY_SOUND_FRONTEND(-1, "BACK", "HUD_FRONTEND_DEFAULT_SOUNDSET")
			ENDIF
			
		BREAK	
	ENDSWITCH
	
	
ENDPROC

PROC DECREMENT_DOMINANCE(FLOAT &fValue)
	fValue -= (GET_FRAME_TIME() * DOMINANCE_UPDATE_MULTIPLIER)
	IF fValue < 0
		fValue = 0
	ENDIF
ENDPROC

PROC INCREMENT_DOMINANCE(FLOAT &fValue)
	fValue += (GET_FRAME_TIME() * DOMINANCE_UPDATE_MULTIPLIER)
	IF fValue > CHAR_CREATOR_MAX_DOMINENCE
		fValue = TO_FLOAT(CHAR_CREATOR_MAX_DOMINENCE)
	ENDIF
ENDPROC


/// PURPOSE: Processes the player changing a heritage setting
FUNC BOOL PROCESS_CHAR_CREATOR_HERITAGE_CHANGE(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData, INT iIncrement)
	IF NOT ARE_ALL_CHAR_CREATOR_PEDS_LOADED(charCreatorData)
		RETURN FALSE
	ENDIF
	
	
	SWITCH charCreatorData.heritageIndex
		CASE HERITAGE_MUM
			PRINTLN("[CHARCREATOR] PROCESS_CHAR_CREATOR_HERITAGE_CHANGE - Changing MUMs head")
			PROCESS_CHANGE_PARENT_RACE(charCreatorData, CHAR_CREATOR_PED_MUM, iIncrement, CHAR_CREATOR_RACE_INDEX_ONE)
			GENERATE_CHAR_CREATOR_RANDOM_PARENTS_HAIR(charCreatorData, CHAR_CREATOR_PED_MUM, FALSE, TRUE)
			PROCESS_CHAR_CREATOR_PARENT_UPDATE(charCreatorData, CHAR_CREATOR_PED_MUM)
			PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
		BREAK
		
		CASE HERITAGE_MUM2
			PRINTLN("[CHARCREATOR] PROCESS_CHAR_CREATOR_HERITAGE_CHANGE - Changing MUMs head")
			PROCESS_CHANGE_PARENT_RACE(charCreatorData, CHAR_CREATOR_PED_MUM, iIncrement, CHAR_CREATOR_RACE_INDEX_TWO)
			GENERATE_CHAR_CREATOR_RANDOM_PARENTS_HAIR(charCreatorData, CHAR_CREATOR_PED_MUM, FALSE, TRUE)
			PROCESS_CHAR_CREATOR_PARENT_UPDATE(charCreatorData, CHAR_CREATOR_PED_MUM)
			PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
		BREAK
		
		CASE HERITAGE_DAD
		
			PRINTLN("[CHARCREATOR] PROCESS_CHAR_CREATOR_HERITAGE_CHANGE - Changing DADs head")
			PROCESS_CHANGE_PARENT_RACE(charCreatorData, CHAR_CREATOR_PED_DAD, iIncrement, CHAR_CREATOR_RACE_INDEX_ONE)
			GENERATE_CHAR_CREATOR_RANDOM_PARENTS_HAIR(charCreatorData, CHAR_CREATOR_PED_DAD, TRUE, FALSE)
			PROCESS_CHAR_CREATOR_PARENT_UPDATE(charCreatorData, CHAR_CREATOR_PED_DAD)
			PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
		BREAK
		
		CASE HERITAGE_DAD2
			PRINTLN("[CHARCREATOR] PROCESS_CHAR_CREATOR_HERITAGE_CHANGE - Changing DADs head")
			PROCESS_CHANGE_PARENT_RACE(charCreatorData, CHAR_CREATOR_PED_DAD, iIncrement, CHAR_CREATOR_RACE_INDEX_TWO)
			GENERATE_CHAR_CREATOR_RANDOM_PARENTS_HAIR(charCreatorData, CHAR_CREATOR_PED_DAD, TRUE, FALSE)
			PROCESS_CHAR_CREATOR_PARENT_UPDATE(charCreatorData, CHAR_CREATOR_PED_DAD)
			PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
		BREAK
		
		CASE HERITAGE_DOMINANCE
		CASE HERITAGE_MUM_DOMINANCE
		CASE HERITAGE_DAD_DOMINANCE
			FLOAT fValue
			SWITCH charCreatorData.heritageIndex
				CASE HERITAGE_DOMINANCE
					fValue = charCreatorData.pedData.fParentDominance
				BREAK
				CASE HERITAGE_MUM_DOMINANCE
					fValue = charCreatorData.pedData.fMumDominance
				BREAK
				CASE HERITAGE_DAD_DOMINANCE
					fValue = charCreatorData.pedData.fDadDominance
				BREAK
			ENDSWITCH
		
			IF iIncrement < 0
				IF fValue > 0
					PRINTLN("[CHARCREATOR] PROCESS_CHAR_CREATOR_HERITAGE_CHANGE - Changing dom down")
					
					SWITCH charCreatorData.heritageIndex
						CASE HERITAGE_DOMINANCE
							DECREMENT_DOMINANCE(charCreatorData.pedData.fParentDominance)
							UPDATE_CHAR_CREATOR_CHILD_PARENT_DOM(charCreatorData)
						BREAK
						CASE HERITAGE_MUM_DOMINANCE
							DECREMENT_DOMINANCE(charCreatorData.pedData.fMumDominance)
							PROCESS_CHAR_CREATOR_PARENT_UPDATE(charCreatorData, CHAR_CREATOR_PED_MUM, FALSE, TRUE)
							//SET_BIT(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_DO_CHILD_BUSY_SPINNER)
						BREAK
						CASE HERITAGE_DAD_DOMINANCE
							DECREMENT_DOMINANCE(charCreatorData.pedData.fDadDominance)
							PROCESS_CHAR_CREATOR_PARENT_UPDATE(charCreatorData, CHAR_CREATOR_PED_DAD, FALSE, TRUE)	
							//SET_BIT(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_DO_CHILD_BUSY_SPINNER)
						BREAK
					ENDSWITCH

					PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
				ELSE
					RETURN FALSE
				ENDIF				
			ELSE
				IF fValue < CHAR_CREATOR_MAX_DOMINENCE
					PRINTLN("[CHARCREATOR] PROCESS_CHAR_CREATOR_HERITAGE_CHANGE - Changing dom up")
					
					SWITCH charCreatorData.heritageIndex
						CASE HERITAGE_DOMINANCE
							INCREMENT_DOMINANCE(charCreatorData.pedData.fParentDominance)
							UPDATE_CHAR_CREATOR_CHILD_PARENT_DOM(charCreatorData)
						BREAK
						CASE HERITAGE_MUM_DOMINANCE
							INCREMENT_DOMINANCE(charCreatorData.pedData.fMumDominance)
							PROCESS_CHAR_CREATOR_PARENT_UPDATE(charCreatorData, CHAR_CREATOR_PED_MUM, FALSE, TRUE)
							//SET_BIT(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_DO_CHILD_BUSY_SPINNER)
						BREAK
						CASE HERITAGE_DAD_DOMINANCE
							INCREMENT_DOMINANCE(charCreatorData.pedData.fDadDominance)
							PROCESS_CHAR_CREATOR_PARENT_UPDATE(charCreatorData, CHAR_CREATOR_PED_DAD, FALSE, TRUE)	
							//SET_BIT(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_DO_CHILD_BUSY_SPINNER)
						BREAK
					ENDSWITCH
					
					PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
				ELSE
					RETURN FALSE
				ENDIF
			ENDIF
			
			SET_BIT(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_DONT_UPDATE_GRANDPARENTS)
		BREAK
		
		CASE HERITAGE_GENDER
			
			PRINTLN("[CHARCREATOR] PROCESS_CHAR_CREATOR_HERITAGE_CHANGE - Changing gender")
			
			//IF IS_CHAR_CREATOR_PED_MALE(charCreatorData)
			//	SET_BIT(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_FORCE_DRAW_MALE)
			//ELSE
			//	SET_BIT(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_FORCE_DRAW_FEMALE)
			//ENDIF
			
			PROCESS_CHAR_CREATOR_GENDER_CHANGE(charCreatorData)
			
			PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
			
			SET_BIT(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_DONT_UPDATE_GRANDPARENTS)
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC


/// PURPOSE: Processes the player changing the hour of a lifestyle selection 
PROC PROCESS_CHAR_CREATOR_LIFESTYLE_CHANGE(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData, INT iIncrement)
	IF NOT ARE_ALL_CHAR_CREATOR_PEDS_LOADED(charCreatorData)
		EXIT
	ENDIF
	
	INT iLifestyleCurrent 	= GET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, charCreatorData.lifestyleIndex)
	INT iLifestyleMin 		= GET_CHAR_CREATOR_HOURS_BOUNDS_FOR_LIFESTYLE(charCreatorData.lifestyleIndex, FALSE)
	INT iLifestyleMax 		= GET_CHAR_CREATOR_HOURS_BOUNDS_FOR_LIFESTYLE(charCreatorData.lifestyleIndex, TRUE)
	
	// Are we wanting to reduce the hours
	IF iIncrement < 0
		
		IF iLifestyleCurrent > iLifestyleMin
			SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, charCreatorData.lifestyleIndex, (iLifestyleCurrent-1))
			charCreatorData.pedData.iHoursLeftToAssign++
			
			PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
		ENDIF
		
	// Are we wanting to increase the hours
	ELSE
		
		IF GET_CHAR_CREATOR_HOURS_LEFT(charCreatorData) > 0
			
			IF iLifestyleCurrent < iLifestyleMax
				SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, charCreatorData.lifestyleIndex, (iLifestyleCurrent+1))
				charCreatorData.pedData.iHoursLeftToAssign--
				
				PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
			ENDIF
		ENDIF
	ENDIF		
	
ENDPROC

PROC RUN_CHAR_CREATOR_TABS(MPHUD_PLACEMENT_TOOLS& Placement)
	IF IS_PAUSE_MENU_ACTIVE_EX()

		PRINTLN("[CHARCREATOR] RUN_CHAR_CREATOR_TABS - running tabs for character creator")

		GATHER_TAB_STRINGS(Placement.ScaleformXMLTabStruct)
		Placement.ScaleformXMLTabStruct.TabsHeader = "HUD_SELE_TIT"

		IF REGISTER_PEDHEADSHOT_TAB(Placement.IdlePed[Placement.ScreenPlace.iSelectedCharacter] , Placement.ScaleformAlertScreenStruct.sAlertTextureName, Placement.ScaleformAlertScreenStruct.HeadshotPic)
			RUN_SCALEFORMXML_TABS_FACE(Placement.ScaleformXMLTabStruct,Placement.ScaleformAlertScreenStruct, SHOULD_REFRESH_SCALEFORMXML_TABS_FACES(Placement.ScaleformXMLTabStruct))
		ELSE
			REFRESH_SCALEFORMXML_TABS_FACES(Placement.ScaleformXMLTabStruct)
		ENDIF		
			
		IF IS_STRING_EMPTY_HUD(Placement.ScaleformXMLTabStruct.PlayerData.GangTextureName) = FALSE
			RUN_SCALEFORMXML_TABS_EMBLEM(Placement.ScaleformXMLTabStruct, SHOULD_REFRESH_SCALEFORMXML_TABS_EMBLEM(Placement.ScaleformXMLTabStruct) )
		ELSE
			GAMER_HANDLE aGamer = GET_GAMER_HANDLE_PLAYER(PLAYER_ID())
			IF NETWORK_CLAN_GET_EMBLEM_TXD_NAME(aGamer, Placement.ScaleformXMLTabStruct.PlayerData.GangTextureName) = FALSE
				Placement.ScaleformXMLTabStruct.PlayerData.GangTextureName = ""
			ENDIF
			REFRESH_SCALEFORMXML_TABS_EMBLEM(Placement.ScaleformXMLTabStruct)
		ENDIF	

		RUN_CHARACTER_SELECT_TABS_NAME(Placement.ScaleformXMLTabStruct)

		RUN_SCALEFORMXML_TABS_NAME(Placement.ScaleformXMLTabStruct, SHOULD_REFRESH_SCALEFORMXML_TABS_DISPLAY(Placement.ScaleformXMLTabStruct))
	ENDIF
ENDPROC

// ***********************************
// 		CHARACTER CREATOR COMMANDS
// ***********************************


PROC DO_TUTORIAL_PRESTREAMING(INT iSlot)

	IF IS_TUTORIAL_SKIPPED_THIS_TIME(iSlot) = FALSE
	
		DEBUG_PRINTCALLSTACK()
		NET_NL()NET_PRINT(" DO_TUTORIAL_PRESTREAMING - Called ")


		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			SET_ENTITY_COORDS(PLAYER_PED_ID(), g_vFreemodeCutStartPos)
			//SET_ENTITY_ROTATION(PLAYER_PED_ID(), <<-0.37, -0.89, -0.25>>)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), 147.9126) 
			
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(180.0) 
			FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
		ENDIF

		REQUEST_MODEL(FROGGER)

		IF IS_PLAYER_SWITCH_IN_PROGRESS()
			STOP_PLAYER_SWITCH()
			SET_SKYSWOOP_STAGE(SKYSWOOP_NONE)
			NET_NL()NET_PRINT("DO_TUTORIAL_PRESTREAMING: STOP_PLAYER_SWITCH called ")
		ENDIF
	//
	//	IF IS_SRL_LOADED()
	//		END_SRL()
	//		NET_NL()NET_PRINT("DO_TUTORIAL_PRESTREAMING: IS_SRL_LOADED = TRUE calling END_SRL ")
	//	ENDIF

	//			IF IS_SRL_LOADED() = FALSE
	//		NET_NL()NET_PRINT("DO_TUTORIAL_PRESTREAMING: PREFETCH_SRL called GTAO_INTRO_MALE ")
	//		PREFETCH_SRL("GTAO_INTRO_MALE")
	//		SET_SRL_LONG_JUMP_MODE(TRUE)
	//			ELSE
	//				NET_NL()NET_PRINT("DO_TUTORIAL_PRESTREAMING: IS_SRL_LOADED() = TRUE skip PREFETCH_SRL(GTAO_INTRO_MALE) ")
	//			ENDIF

		

		SET_SRL_TUTORIAL_ACTIVE(FALSE)
		SET_SRL_TUTORIAL_ACTIVE(TRUE)
		
	
	ELSE
		NET_NL()NET_PRINT(" DO_TUTORIAL_PRESTREAMING - Called but IS_TUTORIAL_SKIPPED_THIS_TIME = TRUE so skipping ")
	ENDIF
	
	
	
	
	

ENDPROC

/// PURPOSE: Save out all our creator data to stats so we can recreate player's ped
PROC PROCESS_PLAYER_JOINING_GTA_ONLINE(MPHUD_PLACEMENT_TOOLS& Placement, MPHUD_CHARACTER_CREATOR_DATA &charCreatorData)
	
	SET_ACTIVE_CHARACTER_SLOT(Placement.ScreenPlace.iSelectedCharacter)
	

	
	// ********* LIFESTYLE **********
	
	// Save our hours:
	INT iLifestyleHours = GET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_SLEEP)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CREATOR_SLEEP, iLifestyleHours, Placement.ScreenPlace.iSelectedCharacter)
	PRINTLN("[CHARCREATOR] PROCESS_PLAYER_JOINING_GTA_ONLINE - Setting MP_STAT_CHAR_CREATOR_SLEEP = ", iLifestyleHours)
	
	iLifestyleHours = GET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_FAMILY)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CREATOR_FAMILY, iLifestyleHours, Placement.ScreenPlace.iSelectedCharacter)
	PRINTLN("[CHARCREATOR] PROCESS_PLAYER_JOINING_GTA_ONLINE - Setting MP_STAT_CHAR_CREATOR_FAMILY = ", iLifestyleHours)
	
	iLifestyleHours = GET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_SPORT)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CREATOR_SPORT, iLifestyleHours, Placement.ScreenPlace.iSelectedCharacter)
	PRINTLN("[CHARCREATOR] PROCESS_PLAYER_JOINING_GTA_ONLINE - Setting MP_STAT_CHAR_CREATOR_SPORT = ", iLifestyleHours)
	
	iLifestyleHours = GET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_LEGAL)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CREATOR_LEGAL, iLifestyleHours, Placement.ScreenPlace.iSelectedCharacter)
	PRINTLN("[CHARCREATOR] PROCESS_PLAYER_JOINING_GTA_ONLINE - Setting MP_STAT_CHAR_CREATOR_LEGAL = ", iLifestyleHours)
	
	iLifestyleHours = GET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_TV)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CREATOR_TV, iLifestyleHours, Placement.ScreenPlace.iSelectedCharacter)
	PRINTLN("[CHARCREATOR] PROCESS_PLAYER_JOINING_GTA_ONLINE - Setting MP_STAT_CHAR_CREATOR_TV = ", iLifestyleHours)
	
	iLifestyleHours = GET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_PARTY)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CREATOR_PARTY, iLifestyleHours, Placement.ScreenPlace.iSelectedCharacter)
	PRINTLN("[CHARCREATOR] PROCESS_PLAYER_JOINING_GTA_ONLINE - Setting MP_STAT_CHAR_CREATOR_PARTY = ", iLifestyleHours)
	
	iLifestyleHours = GET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_ILLEGAL)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CREATOR_ILLEGAL, iLifestyleHours, Placement.ScreenPlace.iSelectedCharacter)
	PRINTLN("[CHARCREATOR] PROCESS_PLAYER_JOINING_GTA_ONLINE - Setting MP_STAT_CHAR_CREATOR_ILLEGAL = ", iLifestyleHours)
	
	// ********* SAVE OUT DETAILS FOR ALWYN
	
	scrPedHeadBlendData headBlendData
	
	#IF NOT IS_NEXTGEN_BUILD
	BOOL bHASGRANDPARENT
	#ENDIF
	IF IS_CHAR_CREATOR_PED_MALE(charCreatorData)
		GET_PED_HEAD_BLEND_DATA(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON], headBlendData)
		#IF NOT IS_NEXTGEN_BUILD
		bHASGRANDPARENT = GET_PED_HEAD_BLEND_HAS_GRANDPARENTS(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON])
		#ENDIF
	ELSE
		GET_PED_HEAD_BLEND_DATA(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER], headBlendData)
		#IF NOT IS_NEXTGEN_BUILD
		bHASGRANDPARENT = GET_PED_HEAD_BLEND_HAS_GRANDPARENTS(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER])
		#ENDIF
	ENDIF
	
	PRINTLN("[CHARCREATOR] PROCESS_PLAYER_JOINING_GTA_ONLINE - GET_PED_HEAD_BLEND_DATA m_head0 		= ", headBlendData.m_head0)
	PRINTLN("[CHARCREATOR] PROCESS_PLAYER_JOINING_GTA_ONLINE - GET_PED_HEAD_BLEND_DATA m_head1 		= ", headBlendData.m_head1)
	PRINTLN("[CHARCREATOR] PROCESS_PLAYER_JOINING_GTA_ONLINE - GET_PED_HEAD_BLEND_DATA m_head2 		= ", headBlendData.m_head2)
	PRINTLN("[CHARCREATOR] PROCESS_PLAYER_JOINING_GTA_ONLINE - GET_PED_HEAD_BLEND_DATA m_tex0 		= ", headBlendData.m_tex0)
	PRINTLN("[CHARCREATOR] PROCESS_PLAYER_JOINING_GTA_ONLINE - GET_PED_HEAD_BLEND_DATA m_tex1 		= ", headBlendData.m_tex1)
	PRINTLN("[CHARCREATOR] PROCESS_PLAYER_JOINING_GTA_ONLINE - GET_PED_HEAD_BLEND_DATA m_tex2 		= ", headBlendData.m_tex2)
	PRINTLN("[CHARCREATOR] PROCESS_PLAYER_JOINING_GTA_ONLINE - GET_PED_HEAD_BLEND_DATA m_headBlend 	= ", headBlendData.m_headBlend)
	PRINTLN("[CHARCREATOR] PROCESS_PLAYER_JOINING_GTA_ONLINE - GET_PED_HEAD_BLEND_DATA m_texBlend 	= ", headBlendData.m_texBlend)
	PRINTLN("[CHARCREATOR] PROCESS_PLAYER_JOINING_GTA_ONLINE - GET_PED_HEAD_BLEND_DATA m_varBlend 	= ", headBlendData.m_varBlend)
	PRINTLN("[CHARCREATOR] PROCESS_PLAYER_JOINING_GTA_ONLINE - GET_PED_HEAD_BLEND_DATA m_isParent 	= ", headBlendData.m_isParent)
	#IF NOT IS_NEXTGEN_BUILD
	PRINTLN("[CHARCREATOR] PROCESS_PLAYER_JOINING_GTA_ONLINE - GET_PED_HEAD_BLEND_HAS_GRANDPARENTS 	= ", bHASGRANDPARENT)
	#ENDIF
	
	SET_MP_INT_CHARACTER_STAT(MP_STAT_MESH_HEAD0, headBlendData.m_head0, Placement.ScreenPlace.iSelectedCharacter)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_MESH_HEAD1, headBlendData.m_head1, Placement.ScreenPlace.iSelectedCharacter)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_MESH_HEAD2, headBlendData.m_head2, Placement.ScreenPlace.iSelectedCharacter)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_MESH_TEX0, headBlendData.m_tex0, Placement.ScreenPlace.iSelectedCharacter)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_MESH_TEX1, headBlendData.m_tex1, Placement.ScreenPlace.iSelectedCharacter)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_MESH_TEX2, headBlendData.m_tex2, Placement.ScreenPlace.iSelectedCharacter)
	SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_MESH_HEADBLEND, headBlendData.m_headBlend, Placement.ScreenPlace.iSelectedCharacter)
	SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_MESH_TEXBLEND, headBlendData.m_texBlend, Placement.ScreenPlace.iSelectedCharacter)
	SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_MESH_VARBLEND, headBlendData.m_varBlend, Placement.ScreenPlace.iSelectedCharacter)
	SET_MP_BOOL_CHARACTER_STAT(MP_STAT_MESH_ISPARENT, headBlendData.m_isParent, Placement.ScreenPlace.iSelectedCharacter)
	#IF NOT IS_NEXTGEN_BUILD
	SET_MP_BOOL_CHARACTER_STAT(MP_STAT_MESH_HASGRANDPARENT, bHASGRANDPARENT, Placement.ScreenPlace.iSelectedCharacter)
	#ENDIF
	// ********* PARENTS **********
	
	CHAR_CREATOR_RACE aRace1, aRace2		// Race of the parent
	
	// ***** MUM *****
	
	// Grab the data of the first race that contributes to our mum
	aRace1 = GET_CHAR_CREATOR_PARENT_RACE(charCreatorData, CHAR_CREATOR_PED_MUM, CHAR_CREATOR_RACE_INDEX_ONE)
	
	SET_PACKED_STAT_INT(PACKED_MP_M1_HEAD_TYPE, ENUM_TO_INT(aRace1), Placement.ScreenPlace.iSelectedCharacter)
	SET_PACKED_STAT_INT(PACKED_MP_M1_HEAD_VAR, charCreatorData.pedData.charCreatorParentInfo.iMumHeadVariation, Placement.ScreenPlace.iSelectedCharacter)
	
	PRINTLN("[CHARCREATOR] PROCESS_PLAYER_JOINING_GTA_ONLINE - MUM: Race -> ", ENUM_TO_INT(aRace1), ", iHeadVariation -> ", charCreatorData.pedData.charCreatorParentInfo.iMumHeadVariation)
	
	// Grab the data of the second race that contributes to your mum
	aRace2 = GET_CHAR_CREATOR_PARENT_RACE(charCreatorData, CHAR_CREATOR_PED_MUM, CHAR_CREATOR_RACE_INDEX_TWO)
	
	SET_PACKED_STAT_INT(PACKED_MP_M2_HEAD_TYPE, ENUM_TO_INT(aRace2), Placement.ScreenPlace.iSelectedCharacter)
	SET_PACKED_STAT_INT(PACKED_MP_M2_HEAD_VAR, charCreatorData.pedData.charCreatorParentInfo.iMumHeadVariation2, Placement.ScreenPlace.iSelectedCharacter)

	PRINTLN("[CHARCREATOR] PROCESS_PLAYER_JOINING_GTA_ONLINE - MUM: (2) Race -> ", ENUM_TO_INT(aRace2), ", iHeadVariation -> ", charCreatorData.pedData.charCreatorParentInfo.iMumHeadVariation2)
	
	IF aRace1 = CHAR_CREATOR_SPECIAL_RACE
		SET_PACKED_STAT_INT(PACKED_MP_M2_HEAD_TYPE, ENUM_TO_INT(aRace1), Placement.ScreenPlace.iSelectedCharacter)
		SET_PACKED_STAT_INT(PACKED_MP_M1_HEAD_VAR, GET_CHAR_CREATOR_PARENT_SPECIAL_HEAD_INDEX(charCreatorData, CHAR_CREATOR_PED_MUM), Placement.ScreenPlace.iSelectedCharacter)
		SET_PACKED_STAT_INT(PACKED_MP_M2_HEAD_VAR, GET_CHAR_CREATOR_PARENT_SPECIAL_HEAD_INDEX(charCreatorData, CHAR_CREATOR_PED_MUM), Placement.ScreenPlace.iSelectedCharacter)
		
		PRINTLN("[CHARCREATOR] PROCESS_PLAYER_JOINING_GTA_ONLINE - MUM: Special save index: ", GET_CHAR_CREATOR_PARENT_SPECIAL_HEAD_INDEX(charCreatorData, CHAR_CREATOR_PED_MUM))
	ENDIF
	
	// save out mom dominance
	FLOAT fMumDom = charCreatorData.pedData.fMumDominance
	CONVERT_BASE_GRANDPARENT_BLEND_TO_CAPPED(fMumDom)
	SET_PACKED_STAT_INT(PACKED_MP_M_HEAD_DOM, FLOOR(fMumDom), Placement.ScreenPlace.iSelectedCharacter)
	
	// ***** DAD *****
	
	// Grab the data of the first race that contributes to our dad
	aRace1 = GET_CHAR_CREATOR_PARENT_RACE(charCreatorData, CHAR_CREATOR_PED_DAD, CHAR_CREATOR_RACE_INDEX_ONE)

	SET_PACKED_STAT_INT(PACKED_MP_D1_HEAD_TYPE, ENUM_TO_INT(aRace1), Placement.ScreenPlace.iSelectedCharacter)
	SET_PACKED_STAT_INT(PACKED_MP_D1_HEAD_VAR, charCreatorData.pedData.charCreatorParentInfo.iDadHeadVariation, Placement.ScreenPlace.iSelectedCharacter)
	
	PRINTLN("[CHARCREATOR] PROCESS_PLAYER_JOINING_GTA_ONLINE - DAD: Race -> ", ENUM_TO_INT(aRace1), ", iHeadVariation -> ", charCreatorData.pedData.charCreatorParentInfo.iDadHeadVariation)
	
	// Grab the data of the second race that contributes to your dad
	aRace2 = GET_CHAR_CREATOR_PARENT_RACE(charCreatorData, CHAR_CREATOR_PED_DAD, CHAR_CREATOR_RACE_INDEX_TWO)
	
	SET_PACKED_STAT_INT(PACKED_MP_D2_HEAD_TYPE, ENUM_TO_INT(aRace2), Placement.ScreenPlace.iSelectedCharacter)
	SET_PACKED_STAT_INT(PACKED_MP_D2_HEAD_VAR, charCreatorData.pedData.charCreatorParentInfo.iDadHeadVariation2, Placement.ScreenPlace.iSelectedCharacter)
	
	PRINTLN("[CHARCREATOR] PROCESS_PLAYER_JOINING_GTA_ONLINE - DAD: (2) Race -> ", ENUM_TO_INT(aRace2), ", iHeadVariation -> ", charCreatorData.pedData.charCreatorParentInfo.iDadHeadVariation2)
	
	IF aRace1 = CHAR_CREATOR_SPECIAL_RACE
		SET_PACKED_STAT_INT(PACKED_MP_D2_HEAD_TYPE, ENUM_TO_INT(aRace1), Placement.ScreenPlace.iSelectedCharacter)
		SET_PACKED_STAT_INT(PACKED_MP_D1_HEAD_VAR, GET_CHAR_CREATOR_PARENT_SPECIAL_HEAD_INDEX(charCreatorData, CHAR_CREATOR_PED_DAD), Placement.ScreenPlace.iSelectedCharacter)
		SET_PACKED_STAT_INT(PACKED_MP_D2_HEAD_VAR, GET_CHAR_CREATOR_PARENT_SPECIAL_HEAD_INDEX(charCreatorData, CHAR_CREATOR_PED_DAD), Placement.ScreenPlace.iSelectedCharacter)
		
		PRINTLN("[CHARCREATOR] PROCESS_PLAYER_JOINING_GTA_ONLINE - DAD: Special save index: ", GET_CHAR_CREATOR_PARENT_SPECIAL_HEAD_INDEX(charCreatorData, CHAR_CREATOR_PED_DAD))
	ENDIF
	
	// save out dad dominance
	FLOAT fDadDom = charCreatorData.pedData.fDadDominance
	CONVERT_BASE_GRANDPARENT_BLEND_TO_CAPPED(fDadDom)
	SET_PACKED_STAT_INT(PACKED_MP_D_HEAD_DOM, FLOOR(fDadDom), Placement.ScreenPlace.iSelectedCharacter)	
	
	PRINTLN("[CHARCREATOR] PROCESS_PLAYER_JOINING_GTA_ONLINE - Setting all TEX to 0 (PACKED_MP_HEADBLEND_TEX_0, 1, 2)")
	
	// These are ignored and can be 0
	SET_PACKED_STAT_INT(PACKED_MP_HEADBLEND_TEX_0, 0, Placement.ScreenPlace.iSelectedCharacter)
	SET_PACKED_STAT_INT(PACKED_MP_HEADBLEND_TEX_1, 0, Placement.ScreenPlace.iSelectedCharacter)
	SET_PACKED_STAT_INT(PACKED_MP_HEADBLEND_TEX_2, 0, Placement.ScreenPlace.iSelectedCharacter)
	
	PRINTLN("[CHARCREATOR] PROCESS_PLAYER_JOINING_GTA_ONLINE - Setting MP_STAT_HEADBLEND_GEOM_BLEND: ", charCreatorData.pedData.fParentUgly)
	SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_HEADBLEND_GEOM_BLEND, charCreatorData.pedData.fParentUgly, Placement.ScreenPlace.iSelectedCharacter)
	SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_HEADBLEND_TEX_BLEND, charCreatorData.pedData.fParentUgly, Placement.ScreenPlace.iSelectedCharacter)
	
	FLOAT fDominance
	IF IS_CHAR_CREATOR_PED_MALE(charCreatorData)
		fDominance = GET_CHAR_CREATOR_PARENT_DOMINANCE(charCreatorData, CHAR_CREATOR_GENDER_MALE)
	ELSE
		fDominance = GET_CHAR_CREATOR_PARENT_DOMINANCE(charCreatorData, CHAR_CREATOR_GENDER_FEMALE)
	ENDIF
	
	// parent hair
	SET_MP_INT_CHARACTER_STAT(MP_STAT_HEADBLEND_DAD_HAIR, charCreatorData.pedData.charCreatorParentInfo.iDadHair, Placement.ScreenPlace.iSelectedCharacter)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_HEADBLEND_MUM_HAIR, charCreatorData.pedData.charCreatorParentInfo.iMumHair, Placement.ScreenPlace.iSelectedCharacter)
	
	// parent hair colour
	SET_PACKED_STAT_INT(PACKED_MP_DAD_HAIR_COLOUR, charCreatorData.pedData.charCreatorParentInfo.iDadHairColour, Placement.ScreenPlace.iSelectedCharacter)
	SET_PACKED_STAT_INT(PACKED_MP_MUM_HAIR_COLOUR, charCreatorData.pedData.charCreatorParentInfo.iMumHairColour, Placement.ScreenPlace.iSelectedCharacter)
	
	// parent features
	SET_PACKED_STAT_INT(PACKED_MP_DAD_BEARD, charCreatorData.pedData.charCreatorParentInfo.iDadbeard, Placement.ScreenPlace.iSelectedCharacter)
	SET_PACKED_STAT_INT(PACKED_MP_MUM_MAKEUP, charCreatorData.pedData.charCreatorParentInfo.iMumMakeup, Placement.ScreenPlace.iSelectedCharacter)
	
	
	
	// ********* CHARACTER **********
	
	PRINTLN("[CHARCREATOR] PROCESS_PLAYER_JOINING_GTA_ONLINE - Setting MP_STAT_HEADBLEND_DOM: ", fDominance)
	SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_HEADBLEND_DOM, fDominance, Placement.ScreenPlace.iSelectedCharacter)
	
	PRINTLN("[CHARCREATOR] PROCESS_PLAYER_JOINING_GTA_ONLINE - Setting MP_STAT_CHAR_CREATOR_AGE: ", GET_CHAR_CREATOR_AGE(charCreatorData))
	
	SET_PACKED_STAT_INT(PACKED_MP_HEADBLEND_AGE, GET_CHAR_CREATOR_AGE(charCreatorData), Placement.ScreenPlace.iSelectedCharacter)
	
	// Set our hair
	IF IS_CHAR_CREATOR_PED_MALE(charCreatorData)	
		SET_PACKED_STAT_INT(PACKED_CHAR_CURRENT_HAIRCUT, ENUM_TO_INT(GET_CHAR_CREATOR_HAIRCUT(TRUE, charCreatorData.pedData.iSonHair, charCreatorData.pedData.iSonHairColour)), Placement.ScreenPlace.iSelectedCharacter)
		SET_STORED_MP_PLAYER_HAIR_ONLY(ENUM_TO_INT(GET_CHAR_CREATOR_HAIRCUT(TRUE, charCreatorData.pedData.iSonHair, charCreatorData.pedData.iSonHairColour)), Placement.ScreenPlace.iSelectedCharacter)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_STORED_HAIRDO_SA,ENUM_TO_INT(GET_CHAR_CREATOR_HAIRCUT(TRUE, charCreatorData.pedData.iSonHair, charCreatorData.pedData.iSonHairColour)), Placement.ScreenPlace.iSelectedCharacter)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_STORED_HAIRDO, ENUM_TO_INT(GET_CHAR_CREATOR_HAIRCUT(TRUE, charCreatorData.pedData.iSonHair, charCreatorData.pedData.iSonHairColour)), Placement.ScreenPlace.iSelectedCharacter)
		
		PRINTLN("[CHARCREATOR] PROCESS_PLAYER_JOINING_GTA_ONLINE - Setting PACKED_CHAR_CURRENT_HAIRCUT: ", ENUM_TO_INT(GET_CHAR_CREATOR_HAIRCUT(TRUE, charCreatorData.pedData.iSonHair, charCreatorData.pedData.iSonHairColour)))
	ELSE
		SET_PACKED_STAT_INT(PACKED_CHAR_CURRENT_HAIRCUT, ENUM_TO_INT(GET_CHAR_CREATOR_HAIRCUT(FALSE, charCreatorData.pedData.iDaughterHair, charCreatorData.pedData.iDaughterHairColour)), Placement.ScreenPlace.iSelectedCharacter)
		SET_STORED_MP_PLAYER_HAIR_ONLY(ENUM_TO_INT(GET_CHAR_CREATOR_HAIRCUT(TRUE, charCreatorData.pedData.iDaughterHair, charCreatorData.pedData.iDaughterHairColour)), Placement.ScreenPlace.iSelectedCharacter)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_STORED_HAIRDO_SA,ENUM_TO_INT(GET_CHAR_CREATOR_HAIRCUT(FALSE, charCreatorData.pedData.iDaughterHair, charCreatorData.pedData.iDaughterHairColour)), Placement.ScreenPlace.iSelectedCharacter)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_STORED_HAIRDO, ENUM_TO_INT(GET_CHAR_CREATOR_HAIRCUT(FALSE, charCreatorData.pedData.iDaughterHair, charCreatorData.pedData.iDaughterHairColour)), Placement.ScreenPlace.iSelectedCharacter)
		PRINTLN("[CHARCREATOR] PROCESS_PLAYER_JOINING_GTA_ONLINE - Setting PACKED_CHAR_CURRENT_HAIRCUT: ", ENUM_TO_INT(GET_CHAR_CREATOR_HAIRCUT(FALSE, charCreatorData.pedData.iDaughterHair, charCreatorData.pedData.iDaughterHairColour)))
	ENDIF
	
	INT iValue
	FLOAT fBlend
	// Save our blemish
	GET_CHAR_CREATOR_OVERLAY_VALUES(charCreatorData, CHAR_CREATOR_BLEMISH, iValue, fBlend)

	PRINTLN("[CHARCREATOR] PROCESS_PLAYER_JOINING_GTA_ONLINE - Setting PACKED_MP_HEADBLEND_OVERLAY_BLEMISH: ", iValue)
	PRINTLN("[CHARCREATOR] PROCESS_PLAYER_JOINING_GTA_ONLINE - Setting MP_STAT_HEADBLEND_OVER_BLEMISH_PC: ", fBlend)
	SET_PACKED_STAT_INT(PACKED_MP_HEADBLEND_OVERLAY_BLEMISH, iValue, Placement.ScreenPlace.iSelectedCharacter)
	SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_HEADBLEND_OVER_BLEMISH_PC, fBlend, Placement.ScreenPlace.iSelectedCharacter)
	
	// Save our wrinkles
	GET_CHAR_CREATOR_OVERLAY_VALUES(charCreatorData, CHAR_CREATOR_WRINKLES, iValue, fBlend)

	PRINTLN("[CHARCREATOR] PROCESS_PLAYER_JOINING_GTA_ONLINE - Setting PACKED_MP_HEADBLEND_OVERLAY_WETHR: ", iValue)
	PRINTLN("[CHARCREATOR] PROCESS_PLAYER_JOINING_GTA_ONLINE - Setting MP_STAT_HEADBLEND_OVERLAY_WETHR_PC: ", fBlend)
	SET_PACKED_STAT_INT(PACKED_MP_HEADBLEND_OVERLAY_WETHR, iValue, Placement.ScreenPlace.iSelectedCharacter)
	SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_HEADBLEND_OVERLAY_WETHR_PC, fBlend, Placement.ScreenPlace.iSelectedCharacter)
	
	// Save our wrinkles
	GET_CHAR_CREATOR_OVERLAY_VALUES(charCreatorData, CHAR_CREATOR_BASE_BLEMISH, iValue, fBlend)

	PRINTLN("[CHARCREATOR] PROCESS_PLAYER_JOINING_GTA_ONLINE - Setting PACKED_MP_HEADBLEND_OVERLAY_BASE: ", iValue)
	PRINTLN("[CHARCREATOR] PROCESS_PLAYER_JOINING_GTA_ONLINE - Setting MP_STAT_HEADBLEND_OVERLAY_BASE_PC: ", fBlend)
	SET_PACKED_STAT_INT(PACKED_MP_HEADBLEND_OVERLAY_BASE, iValue, Placement.ScreenPlace.iSelectedCharacter)
	SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_HEADBLEND_OVERLAY_BASE_PC, fBlend, Placement.ScreenPlace.iSelectedCharacter)
	
	// Save our damage
	GET_CHAR_CREATOR_OVERLAY_VALUES(charCreatorData, CHAR_CREATOR_DAMAGE, iValue, fBlend)

	PRINTLN("[CHARCREATOR] PROCESS_PLAYER_JOINING_GTA_ONLINE - Setting PACKED_MP_HEADBLEND_OVERLAY_DAMAGE: ", iValue)
	PRINTLN("[CHARCREATOR] PROCESS_PLAYER_JOINING_GTA_ONLINE - Setting MP_STAT_HEADBLEND_OVERLAY_DAMAGE_PC: ", fBlend)
	SET_PACKED_STAT_INT(PACKED_MP_HEADBLEND_OVERLAY_DAMAGE, iValue, Placement.ScreenPlace.iSelectedCharacter)
	SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_HEADBLEND_OVERLAY_DAMAGE_PC, fBlend, Placement.ScreenPlace.iSelectedCharacter)
	
	//BOOL bIsCharMale = FALSE
	
	IF IS_CHAR_CREATOR_PED_MALE(charCreatorData)
		//bIsCharMale = TRUE
		PRINTLN("[CHARCREATOR] PROCESS_PLAYER_JOINING_GTA_ONLINE (MALE) - Setting PACKED_MP_HEADBLEND_OVERLAY_MAKEUP: ", -1)
		PRINTLN("[CHARCREATOR] PROCESS_PLAYER_JOINING_GTA_ONLINE (MALE) - Setting MP_STAT_HEADBLEND_OVERLAY_MAKEUP_PC: ", 0)
		SET_PACKED_STAT_INT(PACKED_MP_HEADBLEND_OVERLAY_MAKEUP, -1, Placement.ScreenPlace.iSelectedCharacter)
		SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_HEADBLEND_OVERLAY_MAKEUP_PC, 0.0, Placement.ScreenPlace.iSelectedCharacter)
		
		GET_CHAR_CREATOR_OVERLAY_VALUES(charCreatorData, CHAR_CREATOR_BEARD, iValue, fBlend)
		PRINTLN("[CHARCREATOR] PROCESS_PLAYER_JOINING_GTA_ONLINE - Setting PACKED_MP_HEADBLEND_OVERLAY_BEARD: ", iValue)
		PRINTLN("[CHARCREATOR] PROCESS_PLAYER_JOINING_GTA_ONLINE - Setting MP_STAT_HEADBLEND_OVERLAY_BEARD_PC: ", fBlend)
		SET_PACKED_STAT_INT(PACKED_MP_HEADBLEND_OVERLAY_BEARD, iValue, Placement.ScreenPlace.iSelectedCharacter)
		SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_HEADBLEND_OVERLAY_BEARD_PC, fBlend, Placement.ScreenPlace.iSelectedCharacter)
		
		PRINTLN("[CHARCREATOR] PROCESS_PLAYER_JOINING_GTA_ONLINE (MALE) - Setting PACKED_MP_HEADBLEND_OVERLAY_EYEBRW: ", GET_CHAR_CREATOR_EYEBROW_VALUE(charCreatorData, CHAR_CREATOR_GENDER_MALE))
		PRINTLN("[CHARCREATOR] PROCESS_PLAYER_JOINING_GTA_ONLINE (MALE) - Setting MP_STAT_HEADBLEND_OVERLAY_EYEBRW_PC: 1.0")
		SET_PACKED_STAT_INT(PACKED_MP_HEADBLEND_OVERLAY_EYEBRW, GET_CHAR_CREATOR_EYEBROW_VALUE(charCreatorData, CHAR_CREATOR_GENDER_MALE), Placement.ScreenPlace.iSelectedCharacter)
		SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_HEADBLEND_OVERLAY_EYEBRW_PC, 1.0, Placement.ScreenPlace.iSelectedCharacter)
	ELSE
		GET_CHAR_CREATOR_OVERLAY_VALUES(charCreatorData, CHAR_CREATOR_MAKEUP, iValue, fBlend)

		PRINTLN("[CHARCREATOR] PROCESS_PLAYER_JOINING_GTA_ONLINE (FEMALE) - Setting PACKED_MP_HEADBLEND_OVERLAY_MAKEUP: ", iValue)
		PRINTLN("[CHARCREATOR] PROCESS_PLAYER_JOINING_GTA_ONLINE (FEMALE) - Setting MP_STAT_HEADBLEND_OVERLAY_MAKEUP_PC: ", fBlend)
		SET_PACKED_STAT_INT(PACKED_MP_HEADBLEND_OVERLAY_MAKEUP, iValue, Placement.ScreenPlace.iSelectedCharacter)
		SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_HEADBLEND_OVERLAY_MAKEUP_PC, fBlend, Placement.ScreenPlace.iSelectedCharacter)
		
		SET_PACKED_STAT_INT(PACKED_MP_HEADBLEND_OVERLAY_BEARD, 0, Placement.ScreenPlace.iSelectedCharacter)
		SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_HEADBLEND_OVERLAY_BEARD_PC, 0, Placement.ScreenPlace.iSelectedCharacter)
		
		IF iValue < 0
			PRINTLN("[CHARCREATOR] PROCESS_PLAYER_JOINING_GTA_ONLINE (FEMALE) - Setting PACKED_MP_HEADBLEND_OVERLAY_EYEBRW: ", GET_CHAR_CREATOR_EYEBROW_VALUE(charCreatorData, CHAR_CREATOR_GENDER_FEMALE))
			PRINTLN("[CHARCREATOR] PROCESS_PLAYER_JOINING_GTA_ONLINE (FEMALE) - Setting MP_STAT_HEADBLEND_OVERLAY_EYEBRW_PC: 1.0")
			SET_PACKED_STAT_INT(PACKED_MP_HEADBLEND_OVERLAY_EYEBRW, GET_CHAR_CREATOR_EYEBROW_VALUE(charCreatorData, CHAR_CREATOR_GENDER_FEMALE), Placement.ScreenPlace.iSelectedCharacter)
			SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_HEADBLEND_OVERLAY_EYEBRW_PC, 1.0, Placement.ScreenPlace.iSelectedCharacter)
		ELSE
			PRINTLN("[CHARCREATOR] PROCESS_PLAYER_JOINING_GTA_ONLINE (FEMALE) - Setting PACKED_MP_HEADBLEND_OVERLAY_EYEBRW: ", GET_CHAR_CREATOR_EYEBROW_VALUE(charCreatorData, CHAR_CREATOR_GENDER_FEMALE))
			PRINTLN("[CHARCREATOR] PROCESS_PLAYER_JOINING_GTA_ONLINE (FEMALE) - Setting MP_STAT_HEADBLEND_OVERLAY_EYEBRW_PC: 0.0")
			SET_PACKED_STAT_INT(PACKED_MP_HEADBLEND_OVERLAY_EYEBRW, GET_CHAR_CREATOR_EYEBROW_VALUE(charCreatorData, CHAR_CREATOR_GENDER_FEMALE), Placement.ScreenPlace.iSelectedCharacter)
			SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_HEADBLEND_OVERLAY_EYEBRW_PC, 0.0, Placement.ScreenPlace.iSelectedCharacter)
		ENDIF
	ENDIF
	
	// Save out the ped we have created to the IDLE PED
	IF IS_CHAR_CREATOR_PED_MALE(charCreatorData)
		PRINTLN("[CHARCREATOR] PROCESS_PLAYER_JOINING_GTA_ONLINE - Saving SON to idleped")
		
		SET_PACKED_STAT_INT(PACKED_CHAR_PICTURE, 0, Placement.ScreenPlace.iSelectedCharacter)
		
		// 1425160: Clone ped to idle ped so we can safely destroy local peds here
		IF IS_CHAR_CREATOR_PED_OK(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON])
			Placement.IdlePed[Placement.ScreenPlace.iSelectedCharacter] = CLONE_PED(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON], FALSE)
		ENDIF		
	ELSE
		PRINTLN("[CHARCREATOR] PROCESS_PLAYER_JOINING_GTA_ONLINE - Saving DAUGHTER to idleped")
		
		SET_PACKED_STAT_INT(PACKED_CHAR_PICTURE, 1, Placement.ScreenPlace.iSelectedCharacter)
		
		// 1425160: Clone ped to idle ped so we can safely destroy local peds here
		IF IS_CHAR_CREATOR_PED_OK(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER])
			Placement.IdlePed[Placement.ScreenPlace.iSelectedCharacter] = CLONE_PED(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER], FALSE)
		ENDIF
	ENDIF
	
	

						
						
	
	
	// set the player's spawn outfit as owned
	//SET_START_CLOTHES_AS_ACQUIRED_AVAILABLE_IN_STORE(Placement.IdlePed[Placement.ScreenPlace.iSelectedCharacter])
	//GIVE_PLAYER_A_HELMET_AT_START_OF_GAME(IS_CHAR_CREATOR_PED_MALE(charCreatorData))
	PED_VARIATION_STRUCT pedStructure
	GET_PED_VARIATIONS(Placement.IdlePed[Placement.ScreenPlace.iSelectedCharacter], pedStructure)
	SET_STORED_MP_PLAYER_COMPONENTS(Placement.IdlePed[Placement.ScreenPlace.iSelectedCharacter], pedStructure, Placement.ScreenPlace.iSelectedCharacter)
	
	// Dress our player
	DRESS_FREEMODE_PLAYER_AT_START_TORSO(Placement.IdlePed[Placement.ScreenPlace.iSelectedCharacter], TSHIRT_STATES_NEWCHARACTER, Placement.ScreenPlace.iSelectedCharacter, FALSE)
	
	// Save out
	IF charCreatorData.crewTShirtOption = CREW_TSHIRT_OFF
		charCreatorData.crewTShirtOption = CREW_TSHIRT_SOLID_CREW_COLOR
	ENDIF
	
	SET_PACKED_STAT_INT(PACKED_MP_CREW_T_TYPE, ENUM_TO_INT(charCreatorData.crewTShirtOption), Placement.ScreenPlace.iSelectedCharacter)
	
	
	// Set up our inventory
	SET_CURRENT_MP_SLOT_FOR_CLOTHES(Placement.ScreenPlace.iSelectedCharacter)
	UPDATE_CHAR_CREATOR_PLAYER_INVENTORY(charCreatorData, Placement.ScreenPlace.iSelectedCharacter)
	
	g_b_Private_Tshirt_is_new_character[Placement.ScreenPlace.iSelectedCharacter] = TRUE
	
	// Save out the players mood
	PRINTLN("[CHARCREATOR] PROCESS_PLAYER_JOINING_GTA_ONLINE - Save all the players moods: ", ENUM_TO_INT(charCreatorData.pedData.charMood))
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_MOOD_DM, PLAYER_MOOD_NORMAL)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_MOOD_RACES, PLAYER_MOOD_NORMAL)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_MOOD_NORMAL, ENUM_TO_INT(charCreatorData.pedData.charMood))
	
	// Finalise our peds head blend
	FINALIZE_HEAD_BLEND(Placement.IdlePed[Placement.ScreenPlace.iSelectedCharacter])
	
	// Save out our character stats
	CHAR_CREATOR_SAVE_PLAYER_STATS(charCreatorData)
	
	// Clear our data
	RESET_CHARACTER_CREATOR_DATA(charCreatorData)
	
	// Clean up frontend
	PROCESS_CHAR_CREATOR_COMMON_CLEANUP(charCreatorData)
	
	// Move into game
	SET_QUICKJOIN_ACTIVE(TRUE)
	SET_JOINING_CHARACTER(Placement.ScreenPlace.iSelectedCharacter)
	
	GIVE_CHARACTER_STARTING_WEAPONS(Placement.ScreenPlace.iSelectedCharacter)
	
	RUN_GOING_FORWARD_CHARACTER_SUMMARY(Placement)
	
	// Begin prestreaming if required. (moved to point when we are committed to Online: bug 2068483).
	DO_TUTORIAL_PRESTREAMING(Placement.ScreenPlace.iSelectedCharacter)

	//Added by Brenda 20/05/13. don't fade out if this is the case. The tutorial will also be skipped. 
	
	SAVE_PLAYERS_CLOTHES(Placement.IdlePed[Placement.ScreenPlace.iSelectedCharacter]  ,Placement.ScreenPlace.iSelectedCharacter)
	
	Placement.bHasNewCharacterBeenMade = TRUE
	
//	Placement.HasGamerSetupChanged = TRUE
	
	REFRESH_TUTORIAL_DONE_STAGES(Placement.ScreenPlace.iSelectedCharacter)
	
	
	IF NOT IS_PED_INJURED(Placement.IdlePed[Placement.ScreenPlace.iSelectedCharacter ])
		FREEZE_ENTITY_POSITION(Placement.IdlePed[Placement.ScreenPlace.iSelectedCharacter ], TRUE)
	ENDIF
	
	IF NOT IS_PED_INJURED(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER])
		FREEZE_ENTITY_POSITION(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER], TRUE)
	ENDIF
	
	IF NOT IS_PED_INJURED(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON])
		FREEZE_ENTITY_POSITION(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON], TRUE)
	ENDIF
	
	
//	IF GET_MP_BOOL_PLAYER_STAT(MPPLY_NO_MORE_TUTORIALS)
//		NET_NL()NET_PRINT("PROCESS_PLAYER_JOINING_GTA_ONLINE: GET_MP_BOOL_PLAYER_STAT(MPPLY_NO_MORE_TUTORIALS) = TRUE ")
//		g_b_HasSkippedTutorialDueToNoMoreTutorials = TRUE
//		
//		SET_TUTORIALS_AS_DONE()
//	ENDIF
	
	
	FLOAT XPos = GET_RANDOM_FLOAT_IN_RANGE(-1200, 3500)
	FLOAT YPos = GET_RANDOM_FLOAT_IN_RANGE(-2100, 100) 
	VECTOR StoredDefaultStartLocation = <<XPos, YPos, 0>>
	
	SET_MP_VECTOR_CHARACTER_STAT(MP_STAT_FM_SPAWN_POSITION, StoredDefaultStartLocation, Placement.ScreenPlace.iSelectedCharacter)
	RUN_TRANSITION_MENU_AUDIO(FALSE)
	RUN_TRANSITION_SFX(FALSE)
	RUN_TRANSITION_SFX_MENU_SCENE(TRUE)

	
	CLEAR_ALL_RPBOOST_MESSAGE_ENTRIES()
	
	TIMESTAMP_CHARACTER_CREATION(Placement.ScreenPlace.iSelectedCharacter)
	
	
	IF HAS_FM_INTRO_MISSION_BEEN_DONE() = FALSE
	AND IS_TUTORIAL_SKIPPED_THIS_TIME(Placement.ScreenPlace.iSelectedCharacter) = FALSE
	AND HAS_ENTERED_OFFLINE_SAVE_FM() = FALSE
	AND HAS_ENTERED_CHAR_CREATION_FROM_ALTERATION_PROMPT() = FALSE
		DO_SCREEN_FADE_OUT(0)

		SET_TUTORIAL_CUTSCENE_IS_GOING_TO_RUN(TRUE)
		
		SET_RADIO_TO_STATION_NAME("OFF") 
		
		SET_CHARACTER_NEW_FOR_UNLOCKS(TRUE)
		
//		REQUEST_MODEL(FROGGER)
//		
//		IF IS_PLAYER_SWITCH_IN_PROGRESS()
//			STOP_PLAYER_SWITCH()
//			SET_SKYSWOOP_STAGE(SKYSWOOP_NONE)
//			NET_NL()NET_PRINT("PROCESS_PLAYER_JOINING_GTA_ONLINE: STOP_PLAYER_SWITCH called ")
//		ENDIF
//		
//		NET_NL()NET_PRINT("PROCESS_PLAYER_JOINING_GTA_ONLINE: PREFETCH_SRL called GTAO_INTRO_MALE ")
//		PREFETCH_SRL("GTAO_INTRO_MALE")
//		SET_SRL_LONG_JUMP_MODE(TRUE)
		
	ELSE
	
		NET_NL()NET_PRINT("PROCESS_PLAYER_JOINING_GTA_ONLINE: ")
		NET_NL()NET_PRINT("		- HAS_FM_INTRO_MISSION_BEEN_DONE = ")NET_NL()NET_PRINT_BOOL(HAS_FM_INTRO_MISSION_BEEN_DONE())
		NET_NL()NET_PRINT("		- IS_TUTORIAL_SKIPPED_THIS_TIME = ")NET_NL()NET_PRINT_BOOL(IS_TUTORIAL_SKIPPED_THIS_TIME(Placement.ScreenPlace.iSelectedCharacter))
		NET_NL()NET_PRINT("		- HAS_ENTERED_OFFLINE_SAVE_FM = ")NET_NL()NET_PRINT_BOOL(HAS_ENTERED_OFFLINE_SAVE_FM())
		NET_NL()NET_PRINT("		- HAS_ENTERED_CHAR_CREATION_FROM_ALTERATION_PROMPT = ")NET_NL()NET_PRINT_BOOL(HAS_ENTERED_CHAR_CREATION_FROM_ALTERATION_PROMPT())
	
	ENDIF

ENDPROC


PROC GENERATE_CHAR_CREATOR_SETUP_FROM_STATS(MPHUD_PLACEMENT_TOOLS &Placement, MPHUD_CHARACTER_CREATOR_DATA &charCreatorData)
	
	// Make sure we are using correct char slot [BC]: Changed to GET_JOINING_CHARACTER rather than GET_ACTIVE_CHARACTER - Fixes 1425850
	Placement.ScreenPlace.iSelectedCharacter = GET_JOINING_CHARACTER()
	charCreatorData.iCharacterSelectSlot = Placement.ScreenPlace.iSelectedCharacter
	
	// Grab our stat upgrades
	CHAR_CREATOR_GET_PLAYER_STAT_UPGRADES(charCreatorData)
	
	PRINTLN("[CHARCREATOR] GENERATE_CHAR_CREATOR_SETUP_FROM_STATS - Grabbing saved data for character")
	
	// SET UP PLAYER HOURS
	SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_SLEEP, 		GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CREATOR_SLEEP, 	Placement.ScreenPlace.iSelectedCharacter))
	SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_FAMILY, 	GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CREATOR_FAMILY, 	Placement.ScreenPlace.iSelectedCharacter))
	SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_SPORT, 		GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CREATOR_SPORT, 	Placement.ScreenPlace.iSelectedCharacter))
	SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_LEGAL, 		GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CREATOR_LEGAL, 	Placement.ScreenPlace.iSelectedCharacter))
	SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_TV, 		GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CREATOR_TV, 		Placement.ScreenPlace.iSelectedCharacter))
	SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_PARTY, 		GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CREATOR_PARTY, 	Placement.ScreenPlace.iSelectedCharacter))
	SET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, LIFESTYLE_ILLEGAL, 	GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CREATOR_ILLEGAL, Placement.ScreenPlace.iSelectedCharacter))
	charCreatorData.pedData.iHoursLeftToAssign = 0
	
	#IF IS_DEBUG_BUILD
		INT i
		FOR i = 0 TO (ENUM_TO_INT(MAX_LIFESTYLE)-1) STEP 1
			PRINTLN("[CHARCREATOR]		- ", GET_CHAR_CREATOR_LIFESTYLE_TEXT_LABEL(INT_TO_ENUM(CHAR_CREATOR_LIFESTYLE, i)), ": ", GET_CHAR_CREATOR_LIFESTYLE_HOURS(charCreatorData, INT_TO_ENUM(CHAR_CREATOR_LIFESTYLE, i)), " hrs")
		ENDFOR
	#ENDIF
	
	// Default eyebrows
	FLOAT fPercent
	SET_CHAR_CREATOR_OVERLAY_VALUES(charCreatorData, fPercent, CHAR_CREATOR_EYEBROW)	
	
	// SET UP GENDER
	FLOAT fDominance = GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_HEADBLEND_DOM, Placement.ScreenPlace.iSelectedCharacter)
	PRINTLN("[CHARCREATOR] GENERATE_CHAR_CREATOR_SETUP_FROM_STATS - fDominance = ", fDominance)	
	IF GET_PACKED_STAT_INT(PACKED_CHAR_PICTURE, Placement.ScreenPlace.iSelectedCharacter) = 0
		PRINTLN("[CHARCREATOR] GENERATE_CHAR_CREATOR_SETUP_FROM_STATS - Setting ped as MALE")
		SET_CHAR_CREATOR_GENDER(charCreatorData, CHAR_CREATOR_GENDER_MALE)
		charCreatorData.pedData.fParentDominance = GET_CHAR_CREATOR_PARENT_DOMINANCE_FROM_STAT(CHAR_CREATOR_GENDER_MALE, fDominance)
		charCreatorData.pedData.iSonEyebrow = GET_PACKED_STAT_INT(PACKED_MP_HEADBLEND_OVERLAY_EYEBRW, Placement.ScreenPlace.iSelectedCharacter)
	ELSE
		PRINTLN("[CHARCREATOR] GENERATE_CHAR_CREATOR_SETUP_FROM_STATS - Setting ped as FEMALE")
		SET_CHAR_CREATOR_GENDER(charCreatorData, CHAR_CREATOR_GENDER_FEMALE)
		charCreatorData.pedData.fParentDominance = GET_CHAR_CREATOR_PARENT_DOMINANCE_FROM_STAT(CHAR_CREATOR_GENDER_FEMALE, fDominance)
		charCreatorData.pedData.iDaughterEyebrow = GET_PACKED_STAT_INT(PACKED_MP_HEADBLEND_OVERLAY_EYEBRW, Placement.ScreenPlace.iSelectedCharacter)
	ENDIF	

	INT iAge = GET_PACKED_STAT_INT(PACKED_MP_HEADBLEND_AGE, Placement.ScreenPlace.iSelectedCharacter)
	PRINTLN("[CHARCREATOR] GENERATE_CHAR_CREATOR_SETUP_FROM_STATS - iAge = ", iAge)
	SET_CHAR_CREATOR_AGE(charCreatorData, iAge)

	//ELSE
	//	SET_CHAR_CREATOR_OUTFIT(charCreatorData, FALSE, TRUE, TRUE, FALSE)
	//	SET_CHAR_CREATOR_OUTFIT(charCreatorData, FALSE, TRUE, FALSE, TRUE)
	//ENDIF
	
	// get t-shirt
	IF WAS_PLAYER_WEARING_CREW_TSHIRT(Placement.ScreenPlace.iSelectedCharacter)
		charCreatorData.crewTShirtOption = INT_TO_ENUM(CHAR_CREATOR_CREW_TSHIRT_OPTION_ENUM, GET_PACKED_STAT_INT(PACKED_MP_CREW_T_TYPE, Placement.ScreenPlace.iSelectedCharacter))
		charCreatorData.bEnteredWithCrewTShirt = TRUE
	ENDIF
	
	// t-shirt decals
	IF IS_MP_TATTOO_CURRENT(TATTOO_MP_FM_CREW_A, Placement.ScreenPlace.iSelectedCharacter)
		charCreatorData.crewDecalOption = CREW_DECAL_SMALL
	ELIF IS_MP_TATTOO_CURRENT(TATTOO_MP_FM_CREW_B, Placement.ScreenPlace.iSelectedCharacter)
		charCreatorData.crewDecalOption = CREW_DECAL_BIG
	ELIF IS_MP_TATTOO_CURRENT(TATTOO_MP_FM_CREW_C, Placement.ScreenPlace.iSelectedCharacter)
		IF IS_CHAR_CREATOR_PED_MALE(charCreatorData)
			charCreatorData.crewDecalOption = CREW_DECAL_BACK
		ELSE
			charCreatorData.crewDecalOption = CREW_DECAL_BIG
		ENDIF
	ENDIF

	INT iHead1		= GET_PACKED_STAT_INT(PACKED_MP_M1_HEAD_TYPE, Placement.ScreenPlace.iSelectedCharacter)
	INT iHead2		= GET_PACKED_STAT_INT(PACKED_MP_M2_HEAD_TYPE, Placement.ScreenPlace.iSelectedCharacter)
	INT iHeadVar1	= GET_PACKED_STAT_INT(PACKED_MP_M1_HEAD_VAR, Placement.ScreenPlace.iSelectedCharacter)
	
	// get back ugliness variation head 
	charCreatorData.pedData.charCreatorParentInfo.iMumHeadVariation = GET_PACKED_STAT_INT(PACKED_MP_M1_HEAD_VAR, Placement.ScreenPlace.iSelectedCharacter)
	charCreatorData.pedData.charCreatorParentInfo.iMumHeadVariation2 = GET_PACKED_STAT_INT(PACKED_MP_M2_HEAD_VAR, Placement.ScreenPlace.iSelectedCharacter)
	
	// parental dominance
	charCreatorData.pedData.fMumDominance = TO_FLOAT(GET_PACKED_STAT_INT(PACKED_MP_M_HEAD_DOM, Placement.ScreenPlace.iSelectedCharacter))
	CONVERT_CAPPED_GRANDPARENT_BLEND(charCreatorData.pedData.fMumDominance)
	charCreatorData.pedData.fDadDominance = TO_FLOAT(GET_PACKED_STAT_INT(PACKED_MP_D_HEAD_DOM, Placement.ScreenPlace.iSelectedCharacter))
	CONVERT_CAPPED_GRANDPARENT_BLEND(charCreatorData.pedData.fDadDominance)
	
	SET_CHAR_CREATOR_PARENT_RACE(charCreatorData, CHAR_CREATOR_PED_MUM, INT_TO_ENUM(CHAR_CREATOR_RACE, iHead1), CHAR_CREATOR_RACE_INDEX_ONE)
	SET_CHAR_CREATOR_PARENT_RACE(charCreatorData, CHAR_CREATOR_PED_MUM, INT_TO_ENUM(CHAR_CREATOR_RACE, iHead2), CHAR_CREATOR_RACE_INDEX_TWO)
	
	IF IS_CHAR_CREATOR_PARENT_SPECIAL_HEAD(INT_TO_ENUM(CHAR_CREATOR_RACE, iHead1))
		SET_CHAR_CREATOR_PARENT_SPECIAL_HEAD(charCreatorData, CHAR_CREATOR_PED_MUM, iHeadVar1)
		charCreatorData.pedData.charCreatorParentInfo.iMumHeadVariation2 = MP_HEAD_PRETTY
		SET_CHAR_CREATOR_PARENT_RACE(charCreatorData, CHAR_CREATOR_PED_MUM, CHAR_CREATOR_WHITE, CHAR_CREATOR_RACE_INDEX_TWO)			
	ENDIF
	
	PRINTLN("[CHARCREATOR] GENERATE_CHAR_CREATOR_SETUP_FROM_STATS - Mums Race: ", iHead1, " Head2: ", iHead2, ", Var: ", iHeadVar1)
	
	iHead1		= GET_PACKED_STAT_INT(PACKED_MP_D1_HEAD_TYPE, Placement.ScreenPlace.iSelectedCharacter)
	iHead2		= GET_PACKED_STAT_INT(PACKED_MP_D2_HEAD_TYPE, Placement.ScreenPlace.iSelectedCharacter)
	iHeadVar1	= GET_PACKED_STAT_INT(PACKED_MP_D1_HEAD_VAR, Placement.ScreenPlace.iSelectedCharacter)
	
	// get back ugliness variation head 
	charCreatorData.pedData.charCreatorParentInfo.iDadHeadVariation = GET_PACKED_STAT_INT(PACKED_MP_D1_HEAD_VAR, Placement.ScreenPlace.iSelectedCharacter)
	charCreatorData.pedData.charCreatorParentInfo.iDadHeadVariation2 = GET_PACKED_STAT_INT(PACKED_MP_D2_HEAD_VAR, Placement.ScreenPlace.iSelectedCharacter)	
	
	SET_CHAR_CREATOR_PARENT_RACE(charCreatorData, CHAR_CREATOR_PED_DAD, INT_TO_ENUM(CHAR_CREATOR_RACE, iHead1), CHAR_CREATOR_RACE_INDEX_ONE)
	SET_CHAR_CREATOR_PARENT_RACE(charCreatorData, CHAR_CREATOR_PED_DAD, INT_TO_ENUM(CHAR_CREATOR_RACE, iHead2), CHAR_CREATOR_RACE_INDEX_TWO)
	
	IF IS_CHAR_CREATOR_PARENT_SPECIAL_HEAD(INT_TO_ENUM(CHAR_CREATOR_RACE, iHead1))
		SET_CHAR_CREATOR_PARENT_SPECIAL_HEAD(charCreatorData, CHAR_CREATOR_PED_DAD, iHeadVar1)
		charCreatorData.pedData.charCreatorParentInfo.iDadHeadVariation2 = MP_HEAD_PRETTY
		SET_CHAR_CREATOR_PARENT_RACE(charCreatorData, CHAR_CREATOR_PED_DAD, CHAR_CREATOR_WHITE, CHAR_CREATOR_RACE_INDEX_TWO)				
	ENDIF
	
	PRINTLN("[CHARCREATOR] GENERATE_CHAR_CREATOR_SETUP_FROM_STATS - Dads Head: ", iHead1, " Head2: ", iHead2, " Race: ", iHeadVar1)
	
	// Set our random hair
	GENERATE_CHAR_CREATOR_RANDOM_HAIR(charCreatorData, MPSO_CASUAL)
	
	// Then override our saved char:
	INT iHairVal
	INT iHairColour
	GET_CHAR_CREATOR_HAIR_FROM_ENUM(IS_CHAR_CREATOR_PED_MALE(charCreatorData), GET_PACKED_STAT_INT(PACKED_CHAR_CURRENT_HAIRCUT, Placement.ScreenPlace.iSelectedCharacter), iHairVal, iHairColour)
	
	PRINTLN("[CHARCREATOR] GENERATE_CHAR_CREATOR_SETUP_FROM_STATS - Player's hair: ", iHairVal, " and colour: ", iHairColour)
	SET_CHAR_CREATOR_HAIR(charCreatorData, GET_CHAR_CREATOR_GENDER(charCreatorData), iHairVal)
	SET_CHAR_CREATOR_HAIR_COLOUR(charCreatorData, GET_CHAR_CREATOR_GENDER(charCreatorData), iHairColour)
	
	// makeup
	charCreatorData.pedData.charOverlays[ENUM_TO_INT(CHAR_CREATOR_MAKEUP)].iOverlayVal 		= GET_PACKED_STAT_INT(PACKED_MP_HEADBLEND_OVERLAY_MAKEUP, Placement.ScreenPlace.iSelectedCharacter)
	charCreatorData.pedData.charOverlays[ENUM_TO_INT(CHAR_CREATOR_MAKEUP)].fOverlayBlend 	= GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_HEADBLEND_OVERLAY_MAKEUP_PC, Placement.ScreenPlace.iSelectedCharacter)	
	
	IF charCreatorData.pedData.charOverlays[ENUM_TO_INT(CHAR_CREATOR_MAKEUP)].iOverlayVal >= 255
		PRINTLN("[CHARCREATOR] GENERATE_CHAR_CREATOR_SETUP_FROM_STATS - Adjust makeup to -1 as stat is unsigned")
		charCreatorData.pedData.charOverlays[ENUM_TO_INT(CHAR_CREATOR_MAKEUP)].iOverlayVal = -1
	ENDIF
	
	// beard
	IF IS_CHAR_CREATOR_PED_MALE(charCreatorData)
		charCreatorData.pedData.charOverlays[ENUM_TO_INT(CHAR_CREATOR_BEARD)].iOverlayVal 		= GET_PACKED_STAT_INT(PACKED_MP_HEADBLEND_OVERLAY_BEARD, Placement.ScreenPlace.iSelectedCharacter)
		charCreatorData.pedData.charOverlays[ENUM_TO_INT(CHAR_CREATOR_BEARD)].fOverlayBlend 	= GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_HEADBLEND_OVERLAY_BEARD_PC, Placement.ScreenPlace.iSelectedCharacter)		
		IF charCreatorData.pedData.charOverlays[ENUM_TO_INT(CHAR_CREATOR_BEARD)].iOverlayVal > CHAR_CREATOR_MAX_BEARDS
			charCreatorData.pedData.charOverlays[ENUM_TO_INT(CHAR_CREATOR_BEARD)].iOverlayVal  = -1
		ENDIF
	ENDIF
	
	// parents hair
	charCreatorData.pedData.charCreatorParentInfo.iDadHair = GET_MP_INT_CHARACTER_STAT(MP_STAT_HEADBLEND_DAD_HAIR, Placement.ScreenPlace.iSelectedCharacter)
	charCreatorData.pedData.charCreatorParentInfo.iMumHair = GET_MP_INT_CHARACTER_STAT(MP_STAT_HEADBLEND_MUM_HAIR, Placement.ScreenPlace.iSelectedCharacter)
	
	// parent hair colour
	charCreatorData.pedData.charCreatorParentInfo.iDadHairColour =  GET_PACKED_STAT_INT(PACKED_MP_DAD_HAIR_COLOUR, Placement.ScreenPlace.iSelectedCharacter)
	charCreatorData.pedData.charCreatorParentInfo.iMumHairColour =  GET_PACKED_STAT_INT(PACKED_MP_MUM_HAIR_COLOUR, Placement.ScreenPlace.iSelectedCharacter)

	// parent features
	charCreatorData.pedData.charCreatorParentInfo.iDadbeard = GET_PACKED_STAT_INT(PACKED_MP_DAD_BEARD, Placement.ScreenPlace.iSelectedCharacter)
	charCreatorData.pedData.charCreatorParentInfo.iMumMakeup = GET_PACKED_STAT_INT(PACKED_MP_MUM_MAKEUP, Placement.ScreenPlace.iSelectedCharacter)
	
	
	charCreatorData.iHeadForDadRace = GET_GRANDPARENT_FROM_HEAD_AND_RACE(charCreatorData, CHAR_CREATOR_PED_DAD, CHAR_CREATOR_RACE_INDEX_ONE)
	charCreatorData.iHeadForDadRace2 = GET_GRANDPARENT_FROM_HEAD_AND_RACE(charCreatorData, CHAR_CREATOR_PED_DAD, CHAR_CREATOR_RACE_INDEX_TWO)
	charCreatorData.iHeadForMumRace = GET_GRANDPARENT_FROM_HEAD_AND_RACE(charCreatorData, CHAR_CREATOR_PED_MUM, CHAR_CREATOR_RACE_INDEX_ONE)
	charCreatorData.iHeadForMumRace2 = GET_GRANDPARENT_FROM_HEAD_AND_RACE(charCreatorData, CHAR_CREATOR_PED_MUM, CHAR_CREATOR_RACE_INDEX_TWO)	
	
	// cache parent's hair
	charCreatorData.iCacheDadHair[charCreatorData.iHeadForDadRace] = charCreatorData.pedData.charCreatorParentInfo.iDadHair
	charCreatorData.iCacheDadHair[charCreatorData.iHeadForDadRace2] = charCreatorData.pedData.charCreatorParentInfo.iDadHair
	charCreatorData.iCacheMumHair[charCreatorData.iHeadForMumRace] = charCreatorData.pedData.charCreatorParentInfo.iMumHair
	charCreatorData.iCacheMumHair[charCreatorData.iHeadForMumRace2] = charCreatorData.pedData.charCreatorParentInfo.iMumHair
	
	charCreatorData.iCacheDadHairColour[charCreatorData.iHeadForDadRace] = charCreatorData.pedData.charCreatorParentInfo.iDadHairColour
	charCreatorData.iCacheDadHairColour[charCreatorData.iHeadForDadRace2] = charCreatorData.pedData.charCreatorParentInfo.iDadHairColour
	charCreatorData.iCacheMumHairColour[charCreatorData.iHeadForMumRace] = charCreatorData.pedData.charCreatorParentInfo.iMumHairColour
	charCreatorData.iCacheMumHairColour[charCreatorData.iHeadForMumRace2] = charCreatorData.pedData.charCreatorParentInfo.iMumHairColour	
	
	charCreatorData.iCacheDadBeard[charCreatorData.iHeadForDadRace] = charCreatorData.pedData.charCreatorParentInfo.iDadBeard
	charCreatorData.iCacheDadBeard[charCreatorData.iHeadForDadRace2] = charCreatorData.pedData.charCreatorParentInfo.iDadBeard
	charCreatorData.iCacheMumMakeup[charCreatorData.iHeadForMumRace] = charCreatorData.pedData.charCreatorParentInfo.iMumMakeup
	charCreatorData.iCacheMumMakeup[charCreatorData.iHeadForMumRace2] = charCreatorData.pedData.charCreatorParentInfo.iMumMakeup		
	
	//FILL_CACHED_PARENTS(charCreatorData)
ENDPROC


FUNC FLOAT GET_CHAR_CREATOR_LOOK_AT_COORD_FROM_ANALOGUES(INT iAnalogueVal)

	IF iAnalogueVal > 0
		RETURN -(TO_FLOAT(iAnalogueVal)/ 127.0)
	ELSE
		RETURN (TO_FLOAT(iAnalogueVal*-1) / 127.0)
	ENDIF

ENDFUNC

PROC CHAR_CREATOR_MAKE_PED_LOOK(PED_INDEX &pedToTurn, FLOAT fXDir, FLOAT fYDir)

	FLOAT fHeading = GET_ENTITY_HEADING(pedToTurn)

	VECTOR LookatCoors = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(pedToTurn), fHeading, <<fXDir, CHAR_CREATOR_LOOK_AT_Y, CHAR_CREATOR_LOOK_AT_Z_OFFSET + fYDir>>)

	TASK_LOOK_AT_COORD(pedToTurn, LookatCoors, -1, SLF_USE_TORSO | SLF_WHILE_NOT_IN_FOV)

ENDPROC

/// PURPOSE: Handle player turning the characters head
PROC PROCESS_CHAR_CREATOR_LOOK_AT(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData)

	FLOAT fXDirection
	FLOAT fYDirection
	
	INT iRx = GET_CONTROL_VALUE(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT_AXIS_X) - 127
	fXDirection = GET_CHAR_CREATOR_LOOK_AT_COORD_FROM_ANALOGUES(iRx)

	fXDirection = (fXDirection * CHAR_CREATOR_LOOK_AT_X)
	fYDirection = (fYDirection * CHAR_CREATOR_LOOK_AT_Y)

	INT iRy = GET_CONTROL_VALUE(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT_AXIS_Y) - 127
	fYDirection = GET_CHAR_CREATOR_LOOK_AT_COORD_FROM_ANALOGUES(iRy)

	IF iRy < 15 AND iRy > -15 AND iRx < 15 AND iRx > -15
		fXDirection = 0.0
		fYDirection = 0.0
	ENDIF	
		
	// force a look around?
	BOOL bForceLookAround = FALSE
	IF NOT charCreatorData.bForcingLookAround
		IF fXDirection = 0
		AND fYDirection = 0
			IF GET_GAME_TIMER() >= charCreatorData.iNextLookAroundTime
				bForceLookAround = TRUE
			ENDIF
		ELSE
			charCreatorData.iNextLookAroundTime = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(MIN_NEXT_LOOK_AROUND_TIME, MAX_NEXT_LOOK_AROUND_TIME)
		ENDIF
	ELSE
		IF fXDirection = 0
		AND fYDirection = 0
			fXDirection = charCreatorData.pedData.fCharTurnValueX
			fYDirection = charCreatorData.pedData.fCharTurnValueY
		ENDIF
	
		IF GET_GAME_TIMER() >= charCreatorData.iNextLookAroundTime
			charCreatorData.iNextLookAroundTime = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(MIN_NEXT_LOOK_AROUND_TIME, MAX_NEXT_LOOK_AROUND_TIME)
			charCreatorData.bForcingLookAround = FALSE
		ENDIF
	ENDIF
	
	// Only update our look at if we have moved stick
	IF charCreatorData.pedData.fCharTurnValueX != fXDirection
	OR charCreatorData.pedData.fCharTurnValueY != fYDirection
	OR bForceLookAround
		IF NOT bForceLookAround
			charCreatorData.bForcingLookAround = FALSE
		ELSE
			SWITCH GET_RANDOM_INT_IN_RANGE(0,10)
				CASE 0
					fXDirection = -0.67
					fYDirection = 0.43
				BREAK
				CASE 1
					fXDirection = -0.52
					fYDirection = -0.0
				BREAK
				CASE 2
					fXDirection = -0.25
					fYDirection = -0.21
				BREAK
				CASE 3
					fXDirection = -0.16
					fYDirection = -0.40
				BREAK	
				CASE 4
					fXDirection = 0.25
					fYDirection = -0.48
				BREAK	
				CASE 5
					fXDirection = 0.41
					fYDirection = -0.44
				BREAK	
				CASE 6
					fXDirection = 0.41
					fYDirection = -0.44
				BREAK	
				CASE 7
					fXDirection = 0.6
					fYDirection = -0.0
				BREAK	
				CASE 8
					fXDirection = 0.5
					fYDirection = 0.43
				BREAK	
				CASE 9
					fXDirection = 0.0
					fYDirection = 0.75
				BREAK	
			ENDSWITCH

			charCreatorData.iNextLookAroundTime = GET_GAME_TIMER() + 3000
			charCreatorData.bForcingLookAround = TRUE
		ENDIF
		
		charCreatorData.pedData.fCharTurnValueX = fXDirection
		charCreatorData.pedData.fCharTurnValueY = fYDirection			
	
		PRINTLN("[CHARCREATOR] Set look at: X: ", fXDirection, ", Y: ", fYDirection)
			
		IF NOT IS_PED_INJURED(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON])
			CHAR_CREATOR_MAKE_PED_LOOK(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON], fXDirection, fYDirection)
		ENDIF
		
		IF NOT IS_PED_INJURED(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER])
			CHAR_CREATOR_MAKE_PED_LOOK(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER], fXDirection, fYDirection)
		ENDIF	
	ENDIF
ENDPROC

PROC RANDOMISE_SPECIAL_HEAD(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData)
	IF ARE_CHAR_CREATOR_COLLECTOR_EDITIONS_AVAILABLE()
		IF IS_CHAR_CREATOR_RANDOM_SPECIAL_HEAD_AVAILABLE(CHAR_CREATOR_PED_MUM)
			SET_CHAR_CREATOR_PARENT_SPECIAL_HEAD(charCreatorData, CHAR_CREATOR_PED_MUM, GET_CHAR_CREATOR_RANDOM_SPECIAL_HEAD_INDEX(CHAR_CREATOR_PED_MUM))
		ENDIF
	ENDIF
	IF IS_GAME_LINKED_TO_SOCIAL_CLUB()	
	OR ARE_CHAR_CREATOR_COLLECTOR_EDITIONS_AVAILABLE()
		IF IS_CHAR_CREATOR_RANDOM_SPECIAL_HEAD_AVAILABLE(CHAR_CREATOR_PED_DAD)
			SET_CHAR_CREATOR_PARENT_SPECIAL_HEAD(charCreatorData, CHAR_CREATOR_PED_DAD, GET_CHAR_CREATOR_RANDOM_SPECIAL_HEAD_INDEX(CHAR_CREATOR_PED_DAD))
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Randomise the entire creator
PROC DO_CHAR_CREATOR_RANDOMISE(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData)
	// Clean up busy spinners
	SET_CHAR_CREATOR_CHILD_AS_BUSY(charCreatorData, TRUE)
	SET_CHAR_CREATOR_PARENTS_AS_BUSY(charCreatorData, TRUE)

	RESET_CHAR_CREATOR_LIFESTYLE(charCreatorData)
	
	charCreatorData.bSonLockedToOldOutfit = FALSE
	charCreatorData.bDaughterLockedToOldOutfit = FALSE
	
	charCreatorData.cachedMumHeritageIndex = HERITAGE_GENDER
	charCreatorData.cachedDadHeritageIndex = HERITAGE_GENDER	
	CLEAR_CACHED_PARENTS(charCreatorData, TRUE)
	CLEAR_CACHED_PARENTS(charCreatorData, FALSE)


	// randomise gender
	//IF GET_RANDOM_INT_IN_RANGE(0,2) = 0 // remove as per Leslie request
	//	charCreatorData.pedData.charGender = CHAR_CREATOR_GENDER_MALE
	//ELSE
	//	charCreatorData.pedData.charGender = CHAR_CREATOR_GENDER_FEMALE
	//ENDIF	
	
	INT i
	REPEAT 6 i
		charCreatorData.bLockedMaleLifestyle[i] = FALSE
		charCreatorData.bLockedFemaleLifestyle[i] = FALSE
	ENDREPEAT	
	
	//REPEAT MAX_PARENT_OPTIONS i 
	//	charCreatorData.cachedMum[i].bFilled = FALSE
	//	charCreatorData.cachedDad[i].bFilled = FALSE
	//ENDREPEAT
	charCreatorData.iNumberSinceSpecialDadGenerated = SPECIAL_PED_THRESHOLD
	charCreatorData.iNumberSinceSpecialMumGenerated = SPECIAL_PED_THRESHOLD
	charCreatorData.iNumberSinceUglyDadGenerated = UGLY_DAD_THRESHOLD
	charCreatorData.iNumberSinceUglyMumGenerated = UGLY_MUM_THRESHOLD	
	
	GENERATE_CHAR_CREATOR_RANDOM_LIFESTYLE(charCreatorData, FALSE)
	GENERATE_CHAR_CREATOR_RANDOM_PARENTS(charCreatorData, FALSE)
	SET_CHAR_CREATOR_RANDOM_PARENT_DOM(charCreatorData)
	
	charCreatorData.iHeadForDadRace = GET_GRANDPARENT_FROM_HEAD_AND_RACE(charCreatorData, CHAR_CREATOR_PED_DAD, CHAR_CREATOR_RACE_INDEX_ONE)
	charCreatorData.iHeadForDadRace2 = GET_GRANDPARENT_FROM_HEAD_AND_RACE(charCreatorData, CHAR_CREATOR_PED_DAD, CHAR_CREATOR_RACE_INDEX_TWO)
	charCreatorData.iHeadForMumRace = GET_GRANDPARENT_FROM_HEAD_AND_RACE(charCreatorData, CHAR_CREATOR_PED_MUM, CHAR_CREATOR_RACE_INDEX_ONE)
	charCreatorData.iHeadForMumRace2 = GET_GRANDPARENT_FROM_HEAD_AND_RACE(charCreatorData, CHAR_CREATOR_PED_MUM, CHAR_CREATOR_RACE_INDEX_TWO)	
	
	
	GENERATE_CHAR_CREATOR_RANDOM_PARENTS_HAIR(charCreatorData, CHAR_CREATOR_PED_MUM)
	GENERATE_CHAR_CREATOR_RANDOM_PARENTS_HAIR(charCreatorData, CHAR_CREATOR_PED_DAD)
	
	RANDOMISE_SPECIAL_HEAD(charCreatorData)
	
	//FILL_CACHED_PARENTS(charCreatorData)
	SET_CHAR_CREATOR_OUTFIT(charCreatorData, FALSE, TRUE, FALSE, FALSE, FALSE, TRUE, TRUE)	
	CLEAR_ALL_PED_PROPS(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON])
	CLEAR_ALL_PED_PROPS(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER])	
	CLEAR_ALL_PED_PROPS(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON_DUMMY])
	CLEAR_ALL_PED_PROPS(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER_DUMMY])	

	//GENERATE_CHAR_CREATOR_RANDOM_HAIR(charCreatorData, FALSE, TRUE, FALSE)
	//GENERATE_CHAR_CREATOR_RANDOM_MAKEUP(charCreatorData, FALSE, FALSE)
	

	PROCESS_CHAR_CREATOR_ZOOM(charCreatorData, 0)
	
	CALC_CHAR_CREATOR_UGLY_VALUE(charCreatorData)
	CALC_CHAR_CREATOR_OVERLAY_VALUES(charCreatorData)
	
	GIVE_CHAR_CREATOR_PED_HAIR(charCreatorData, CHAR_CREATOR_PED_SON)
	GIVE_CHAR_CREATOR_PED_HAIR(charCreatorData, CHAR_CREATOR_PED_DAUGHTER)
	
	UPDATE_CHAR_CREATOR_CHILD_PARENT_DOM(charCreatorData)
	
	PROCESS_CHAR_CREATOR_PARENT_UPDATE(charCreatorData, CHAR_CREATOR_PED_MUM)
	PROCESS_CHAR_CREATOR_PARENT_UPDATE(charCreatorData, CHAR_CREATOR_PED_DAD)
	
	PROCESS_CHAR_CREATOR_CHILD_OVERLAYS(charCreatorData)
	
	SETUP_CHAR_CREATOR_APPEARANCE(charCreatorData)
	//SETUP_CHAR_CREATOR_HERITAGE(charCreatorData)
	SETUP_CHAR_CREATOR_LIFESTYLE(charCreatorData, TRUE)
	SETUP_CHAR_CREATOR_PLAYER_DETAILS(charCreatorData, TRUE, charCreatorData.columnIndex <> LIFESTYLE_COLUMN)
	


	SET_CHAR_CREATOR_STATS(charCreatorData)
	
	UPDATE_SPECIAL_PARENTS_INSTRUCTIONAL_BUTTONS(charCreatorData)
	PAUSE_MENU_REDRAW_INSTRUCTIONAL_BUTTONS()			
	
	charCreatorData.bHasMadeAlteration = TRUE
ENDPROC

/// PURPOSE: Randomise the entire creator
PROC PROCESS_CHAR_CREATOR_RANDOMISING(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData)
	IF charCreatorData.mainMenuIndex = HERITAGE_CAT
		IF ARE_ALL_CHAR_CREATOR_PEDS_LOADED(charCreatorData)	
			SET_BIT(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_DO_RANDOMISE)
			PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_CHAR_CREATOR_TOGGLE_SPECIAL_PARENTS(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData)
	IF NOT ARE_ALL_CHAR_CREATOR_PEDS_LOADED(charCreatorData)
		EXIT
	ENDIF

	BOOL bToggled = FALSE
	 CHAR_CREATOR_PED aPed
	
	IF charCreatorData.columnIndex = HERITAGE_COLUMN
		aPed = CHAR_CREATOR_PED_MUM
		IF charCreatorData.heritageIndex = HERITAGE_MUM
		OR charCreatorData.heritageIndex = HERITAGE_MUM2
		OR charCreatorData.heritageIndex = HERITAGE_MUM_DOMINANCE
			CHAR_CREATOR_RACE aRace = GET_CHAR_CREATOR_PARENT_RACE(charCreatorData, CHAR_CREATOR_PED_MUM, CHAR_CREATOR_RACE_INDEX_ONE)
			IF aRace = CHAR_CREATOR_SPECIAL_RACE
				SET_CHAR_CREATOR_PARENT_RACE(charCreatorData, CHAR_CREATOR_PED_MUM, charCreatorData.backupMumRace, CHAR_CREATOR_RACE_INDEX_ONE)
				SET_BIT(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_MAINTAIN_SPECIAL_MUM)
				bToggled = TRUE
			ELSE
				IF ARE_CHAR_CREATOR_COLLECTOR_EDITIONS_AVAILABLE()
					charCreatorData.backupMumRace = aRace
					SET_CHAR_CREATOR_PARENT_RACE(charCreatorData, CHAR_CREATOR_PED_MUM, CHAR_CREATOR_SPECIAL_RACE, CHAR_CREATOR_RACE_INDEX_ONE)
					bToggled = TRUE
				ENDIF
			ENDIF
			
			SETUP_CHAR_CREATOR_HERITAGE(charCreatorData)
			PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")

		ELIF charCreatorData.heritageIndex = HERITAGE_DAD
		OR charCreatorData.heritageIndex = HERITAGE_DAD2
		OR charCreatorData.heritageIndex = HERITAGE_DAD_DOMINANCE
			aPed = CHAR_CREATOR_PED_DAD
			CHAR_CREATOR_RACE aRace = GET_CHAR_CREATOR_PARENT_RACE(charCreatorData, CHAR_CREATOR_PED_DAD, CHAR_CREATOR_RACE_INDEX_ONE)
			IF aRace = CHAR_CREATOR_SPECIAL_RACE
				SET_CHAR_CREATOR_PARENT_RACE(charCreatorData, CHAR_CREATOR_PED_DAD, charCreatorData.backupDadRace, CHAR_CREATOR_RACE_INDEX_ONE)
				SET_BIT(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_MAINTAIN_SPECIAL_DAD)
				bToggled = TRUE
			ELSE
				IF ARE_CHAR_CREATOR_COLLECTOR_EDITIONS_AVAILABLE()
				OR IS_GAME_LINKED_TO_SOCIAL_CLUB()
					charCreatorData.backupDadRace = aRace
					SET_CHAR_CREATOR_PARENT_RACE(charCreatorData, CHAR_CREATOR_PED_DAD, CHAR_CREATOR_SPECIAL_RACE, CHAR_CREATOR_RACE_INDEX_ONE)
					bToggled = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF bToggled
		UPDATE_GRANDAD_HEAD(charCreatorData, TRUE)
		UPDATE_GRANDMA_HEAD(charCreatorData, TRUE)
		UPDATE_GRANDAD_HEAD(charCreatorData, FALSE)
		UPDATE_GRANDMA_HEAD(charCreatorData, FALSE)
		
		SET_BIT(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_FORCE_HERITAGE_UPDATE)
		
	 	UPDATE_CHAR_CREATOR_CHILD_PARENT_DOM(charCreatorData)

		PROCESS_CHAR_CREATOR_PARENT_UPDATE(charCreatorData, aPed)	
		PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
		
		UPDATE_SPECIAL_PARENTS_INSTRUCTIONAL_BUTTONS(charCreatorData)
		PAUSE_MENU_REDRAW_INSTRUCTIONAL_BUTTONS()
		
		SET_CHAR_CREATOR_HELP_TEXT(charCreatorData)
	ENDIF
ENDPROC

PROC MAINTAIN_CHAR_CREATOR_PLAYER_ZOOM(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData)
	BOOL bDoSound = FALSE
	IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RT)
		
		
		IF PROCESS_CHAR_CREATOR_ZOOM(charCreatorData, -1)
			bDoSound = TRUE
		ENDIF
	
	ELIF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LT)
	
		IF PROCESS_CHAR_CREATOR_ZOOM(charCreatorData, 1)
			bDoSound = TRUE
		ENDIF
	ENDIF
	
	IF bDoSound
		IF NOT charCreatorData.bZoomingSound
			IF NOT HAS_SOUND_FINISHED(charCreatorData.iSoundID)
				STOP_SOUND(charCreatorData.iSoundID)
			ENDIF
			PLAY_SOUND_FRONTEND(charCreatorData.iSoundID, "MAP_ZOOM","HUD_FRONTEND_DEFAULT_SOUNDSET")
			charCreatorData.bZoomingSound = TRUE
		ENDIF
	ELSE
		IF charCreatorData.bZoomingSound
			IF NOT HAS_SOUND_FINISHED(charCreatorData.iSoundID)
				STOP_SOUND(charCreatorData.iSoundID)
			ENDIF		
			charCreatorData.bZoomingSound = FALSE
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Process the player's input on character creator screen
PROC PROCESS_CHARACTER_CREATOR_CONTROL(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData)
	
	INT iIncrement
	BOOL bButtonPressed
	
	// ***********************************************************
	//		Handle player moving characters head left / right
	// ***********************************************************
	
	PROCESS_CHAR_CREATOR_LOOK_AT(charCreatorData)

	// ***********************************************************
	//		Handle player moving up and down within columns
	// ***********************************************************
	
	IF SHOULD_SELECTION_BE_INCREMENTED_SP(charCreatorData.charCreatorTimer, iIncrement, bButtonPressed, FALSE, FALSE, FALSE, 250)
	
		PROCESS_CHAR_CREATOR_UPDOWN(charCreatorData, iIncrement)
				
		EXIT
	ENDIF
		
	// *******************************************************
	//		Handle player exiting the character creator
	// *******************************************************
	
	IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
	OR IS_PC_MOUSE_CANCEL_JUST_RELEASED()
		IF NOT IS_BIT_SET(charCreatorData.iCharCreatorButtons, CHAR_CREATOR_CANCEL)
		
			PROCESS_CHAR_CREATOR_CANCEL(charCreatorData)

			EXIT
		ENDIF
	ELSE
		IF IS_BIT_SET(charCreatorData.iCharCreatorButtons, CHAR_CREATOR_CANCEL)
			CLEAR_BIT(charCreatorData.iCharCreatorButtons, CHAR_CREATOR_CANCEL)
		ENDIF
	ENDIF
	
	
	// *******************************************************
	//		Handle player continuing from the creator
	// *******************************************************
	
	//IF GET_CHAR_CREATOR_HOURS_LEFT(charCreatorData) = 0
	IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
	OR IS_PC_MOUSE_ACCEPT_JUST_RELEASED()
	
		IF NOT IS_BIT_SET(charCreatorData.iCharCreatorButtons, CHAR_CREATOR_ACCEPT)
		
			PROCESS_CHAR_CREATOR_SELECT(charCreatorData)
			
			EXIT
		ENDIF
	ELSE
		IF IS_BIT_SET(charCreatorData.iCharCreatorButtons, CHAR_CREATOR_ACCEPT)
			CLEAR_BIT(charCreatorData.iCharCreatorButtons, CHAR_CREATOR_ACCEPT)
		ENDIF
	ENDIF
				
	//ENDIF
	
	// *******************************************************
	//		Handle player randomising screen in creator
	// *******************************************************
	
	IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_X)
		IF NOT IS_BIT_SET(charCreatorData.iCharCreatorButtons, CHAR_CREATOR_SQUARE)
			
			PRINTLN("[CHARCREATOR] PROCESS_CHARACTER_CREATOR_CONTROL - Player pressed RANDOM")
		
			SET_BIT(charCreatorData.iCharCreatorButtons, CHAR_CREATOR_SQUARE)

			PROCESS_CHAR_CREATOR_RANDOMISING(charCreatorData)
	
			EXIT
		ENDIF
	ELSE
		IF IS_BIT_SET(charCreatorData.iCharCreatorButtons, CHAR_CREATOR_SQUARE)
			CLEAR_BIT(charCreatorData.iCharCreatorButtons, CHAR_CREATOR_SQUARE)
		ENDIF
	ENDIF
	
	IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_Y)
		IF NOT IS_BIT_SET(charCreatorData.iCharCreatorButtons, CHAR_CREATOR_TRIANGLE)
			PROCESS_CHAR_CREATOR_TOGGLE_SPECIAL_PARENTS(charCreatorData)
			SET_BIT(charCreatorData.iCharCreatorButtons, CHAR_CREATOR_TRIANGLE)
			EXIT
		ENDIF
	ELSE
		IF IS_BIT_SET(charCreatorData.iCharCreatorButtons, CHAR_CREATOR_TRIANGLE)
			CLEAR_BIT(charCreatorData.iCharCreatorButtons, CHAR_CREATOR_TRIANGLE)
		ENDIF		
	ENDIF
	
	// *******************************************************
	//		Handle player changing value of selected item
	// *******************************************************
	
	INT iResponseTime = 300
	IF charCreatorData.columnIndex = LIFESTYLE_COLUMN
		iResponseTime = 100
	ENDIF
	
	IF charCreatorData.columnIndex = APPEARANCE_COLUMN
	AND charCreatorData.appearanceIndex = APPEARANCE_AGE
		iResponseTime = 100
	ENDIF
	
	BOOL bShouldIncrement = SHOULD_SELECTION_BE_INCREMENTED_SP(charCreatorData.charCreatorTimer, iIncrement, bButtonPressed, TRUE, FALSE, FALSE, iResponseTime)
		
	IF charCreatorData.columnIndex <> HERITAGE_COLUMN
	OR (charCreatorData.heritageIndex <> HERITAGE_DOMINANCE AND charCreatorData.heritageIndex <> HERITAGE_MUM_DOMINANCE AND charCreatorData.heritageIndex <> HERITAGE_DAD_DOMINANCE)
		IF bShouldIncrement	
			SWITCH charCreatorData.columnIndex
				
				CASE HERITAGE_COLUMN
					
					PROCESS_CHAR_CREATOR_HERITAGE_CHANGE(charCreatorData, iIncrement)
					
					//SETUP_CHAR_CREATOR_HERITAGE(charCreatorData)
					SET_BIT(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_FORCE_HERITAGE_UPDATE)
					SETUP_CHAR_CREATOR_PLAYER_DETAILS(charCreatorData, TRUE, TRUE)
					SETUP_CHAR_CREATOR_APPEARANCE(charCreatorData)
					
					charCreatorData.bHasMadeAlteration = TRUE
				BREAK
				
				CASE LIFESTYLE_COLUMN
				
					PROCESS_CHAR_CREATOR_LIFESTYLE_CHANGE(charCreatorData, iIncrement)
					
					// Update our hour visuals
					SETUP_CHAR_CREATOR_LIFESTYLE(charCreatorData, TRUE)
					
					// Update our stats
					SET_CHAR_CREATOR_STATS(charCreatorData, TRUE)
				
					// Calculates the UGLY percentage on new values
					IF CALC_CHAR_CREATOR_UGLY_VALUE(charCreatorData)
					//	PROCESS_CHAR_CREATOR_PARENT_UPDATE(charCreatorData, CHAR_CREATOR_PED_MUM)
					//	PROCESS_CHAR_CREATOR_PARENT_UPDATE(charCreatorData, CHAR_CREATOR_PED_DAD)
					ENDIF
					
					// Calculates the overlays of our peds
					CALC_CHAR_CREATOR_OVERLAY_VALUES(charCreatorData)
							
					// Begin a blend update of parents and then child
					//PROCESS_CHAR_CREATOR_PARENT_BLEND_UPDATE(charCreatorData)

					// Update our childs overlays
					PROCESS_CHAR_CREATOR_CHILD_OVERLAYS(charCreatorData, FALSE, FALSE, TRUE)
					
					// Update our outfit
					SET_CHAR_CREATOR_OUTFIT(charCreatorData, FALSE, FALSE, FALSE, FALSE, FALSE, TRUE)
					
					SETUP_CHAR_CREATOR_APPEARANCE(charCreatorData)
					
					charCreatorData.bHasMadeAlteration = TRUE
				BREAK			
				
				CASE APPEARANCE_COLUMN
					
					// Process our update to our appearance change
					PROCESS_CHAR_CREATOR_APPEARANCE_CHANGE(charCreatorData, iIncrement)
					
					// Refresh scaleform visuals
					SETUP_CHAR_CREATOR_APPEARANCE(charCreatorData)
					
					// Update our childs overlays
					IF charCreatorData.appearanceIndex = APPEARANCE_MAKEUP
						PROCESS_CHAR_CREATOR_CHILD_OVERLAYS(charCreatorData)
					ENDIF
					
					charCreatorData.bHasMadeAlteration = TRUE
				BREAK
				
			ENDSWITCH
		ENDIF
	ELSE
		// We always want to update the dominance if button is pressed
		IF bButtonPressed
			IF PROCESS_CHAR_CREATOR_HERITAGE_CHANGE(charCreatorData, iIncrement)
				IF NOT IS_BIT_SET(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_DOMINANCE_CHANGING)
					SET_BIT(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_DOMINANCE_CHANGING)
				ENDIF			
				SET_BIT(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_DONT_UPDATE_GRANDPARENTS)
				SETUP_CHAR_CREATOR_HERITAGE(charCreatorData)
			ENDIF
			charCreatorData.bHasMadeAlteration = TRUE
		ELSE
			IF IS_BIT_SET(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_DOMINANCE_CHANGING)
				UPDATE_CHAR_CREATOR_CHILD_PARENT_DOM(charCreatorData)
				CLEAR_BIT(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_DOMINANCE_CHANGING)
			ENDIF
		ENDIF
	ENDIF
	
	// *******************************************************
	//		Handle player being zoomed in and out
	// *******************************************************
	
	MAINTAIN_CHAR_CREATOR_PLAYER_ZOOM(charCreatorData)
	
	
	// *******************************************************
	//		Handle player being rotated with triggers
	// *******************************************************
	
	IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LB)
		charCreatorData.pedData.iChildRotation -= CHAR_CREATOR_PED_ROT
		EXIT
	ENDIF
	
	IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RB)
		charCreatorData.pedData.iChildRotation += CHAR_CREATOR_PED_ROT
		EXIT
	ENDIF
	
	
	
ENDPROC

/// PURPOSE: Maintain player speaking and play animation to mouth of children
PROC MAINTAIN_CHAR_CREATOR_PLAYER_TALKING(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData)
	
	IF NETWORK_IS_LOCAL_TALKING()
		SET_PED_RESET_FLAG(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON], PRF_EnableVoiceDrivenMouthMovement, TRUE)
		SET_PED_RESET_FLAG(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER], PRF_EnableVoiceDrivenMouthMovement, TRUE)
	ENDIF
	
ENDPROC

/// PURPOSE: Initial Visual display to character creator screen
PROC INITIALISE_CHAR_CREATOR_DISPLAY(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData)

	PRINTLN("[CHARCREATOR] INITIALISE_CHAR_CREATOR_DISPLAY - Initialise our data")

	BOOL bSetup
	MPLOBBY_SET_TITLE("HUD_CCREATION", bSetup)

	// Setup our 3 columns
	SETUP_CHAR_CREATOR_MAIN_MENU(charCreatorData)
	SETUP_CHAR_CREATOR_APPEARANCE(charCreatorData)
	SETUP_CHAR_CREATOR_HERITAGE(charCreatorData)
	SETUP_CHAR_CREATOR_LIFESTYLE(charCreatorData)
	SETUP_CHAR_CREATOR_PLAYER_DETAILS(charCreatorData, FALSE, TRUE)
	
	// Make the default columns visible when we enter this menu
	SET_FRONTEND_DISPLAY_COLUMN(CHAR_CREATOR_COL_MAIN_MENU, TRUE)
	SET_FRONTEND_DISPLAY_COLUMN(CHAR_CREATOR_COL_HERITAGE, FALSE)
	SET_FRONTEND_DISPLAY_COLUMN(CHAR_CREATOR_COL_PLAYER, TRUE)
	SET_FRONTEND_DISPLAY_COLUMN(CHAR_CREATOR_COL_LIFESTYLE, TRUE)
	SET_FRONTEND_DISPLAY_COLUMN(CHAR_CREATOR_COL_APP, FALSE)
	
	// Setup our colours on the tabs
	MPLOBBY_SET_COLUMN_COLOUR(0, ENUM_TO_INT(GET_CHAR_CREATOR_COLOUR()))
	MPLOBBY_SET_COLUMN_COLOUR(1, ENUM_TO_INT(GET_CHAR_CREATOR_COLOUR()))
	MPLOBBY_SET_COLUMN_COLOUR(2, ENUM_TO_INT(GET_CHAR_CREATOR_COLOUR()))
	
	IF charCreatorData.mainMenuIndex = HERITAGE_CAT
		PAUSE_MENU_DEACTIVATE_CONTEXT(HASH("CONTINUE_OPTION"))
	ELSE
		PAUSE_MENU_ACTIVATE_CONTEXT(HASH("CONTINUE_OPTION"))
	ENDIF
	
	PAUSE_MENU_REDRAW_INSTRUCTIONAL_BUTTONS()

ENDPROC


/// PURPOSE: Ensures we have loaded codes PRESETS and render peds at positions
PROC RENDER_CHAR_CREATOR_PEDS(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData)
	
	IF charCreatorData.charCreatorState > CHARACTER_CREATOR_WAIT_FOR_FRONTEND
		IF ARE_ALL_CHAR_CREATOR_PEDS_LOADED(charCreatorData)
					
			IF ARE_ALL_CHAR_CREATOR_PEDS_OK(charCreatorData)
		
			#IF IS_DEBUG_BUILD
			AND NOT	charCreatorData.bDontDrawPeds
			#ENDIF	
				
				IF UI3DSCENE_IS_AVAILABLE()
						
					#IF IS_DEBUG_BUILD
						IF charCreatorData.bZoomOut
							charCreatorData.vCharOffset = <<-0.8, -2.3, 0.515>>
						ENDIF
					#ENDIF
					
					// TODO: look at keeping script rendering
					IF NOT IS_BIT_SET(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_CHILD_PERSIST_PRESET)
						PRINTLN("[CHARCREATOR] RENDER_CHAR_CREATOR_PEDS - UI3DSCENE_MAKE_PUSHED_PRESET_PERSISTENT(TRUE)")
					
						//UI3DSCENE_MAKE_PUSHED_PRESET_PERSISTENT(TRUE)
						SET_BIT(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_CHILD_PERSIST_PRESET)
					ENDIF
					
					
					// ***********************************************
					//				 CHILD DRAWING LOGIC
					// ***********************************************
					
					// Position of child
					VECTOR vChildPosition = charCreatorData.vCharOffset
					VECTOR vMumPosition
					VECTOR vDadPosition
					
					IF charCreatorData.charCreatorState = CHARACTER_CREATOR_JOINING_GTA_ONLINE
					AND NOT IS_BIT_SET(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_WAIT_TO_BACK_OUT_OF_CONFIRM_FRAME)
						vChildPosition.x = CHAR_CREATOR_CONFIRM_ZOOM_MAX_X + ((CHAR_CREATOR_CONFIRM_ZOOM_MIN_X - CHAR_CREATOR_CONFIRM_ZOOM_MAX_X) * charCreatorData.fCharZoom)
						vChildPosition.y = CHAR_CREATOR_CONFIRM_ZOOM_MAX_Y + ((CHAR_CREATOR_CONFIRM_ZOOM_MIN_Y - CHAR_CREATOR_CONFIRM_ZOOM_MAX_Y) * charCreatorData.fCharZoom)
						vChildPosition.z = CHAR_CREATOR_CONFIRM_ZOOM_MAX_Z + ((CHAR_CREATOR_CONFIRM_ZOOM_MIN_Z - CHAR_CREATOR_CONFIRM_ZOOM_MAX_Z) * charCreatorData.fCharZoom)
					ENDIF
					
					IF IS_CHAR_CREATOR_CHILD_SPINNER_DISPLAYED(charCreatorData)
						vChildPosition = <<0.0, 2.0, 0.0>>
					ENDIF
					
					IF IS_CHAR_CREATOR_PARENTS_SPINNER_DISPLAYED(charCreatorData)
						vMumPosition = <<0.0, 3.0, 0.0>>
						vDadPosition = <<0.0, 3.0, 0.0>>
					ENDIF
																	
					IF NOT IS_CHAR_CREATOR_PED_MALE(charCreatorData)
						vChildPosition.z -= 0.02
						//IF IS_PED_WEARING_HIGH_HEELS(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER])
						IF GET_PED_CONFIG_FLAG(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER], INT_TO_ENUM(PED_CONFIG_FLAGS, 322))
							vChildPosition.z -= CHAR_CREATOR_HEEL_OFFSET
						ENDIF
					ENDIF
								
					IF charCreatorData.charCreatorState = CHARACTER_CREATOR_JOINING_GTA_ONLINE
					AND NOT IS_BIT_SET(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_WAIT_TO_BACK_OUT_OF_CONFIRM_FRAME)
						IF UI3DSCENE_PUSH_PRESET("FACE_CREATION_CONFIRM")
						
							IF IS_CHAR_CREATOR_PED_MALE(charCreatorData)
								UI3DSCENE_ASSIGN_PED_TO_SLOT_WITH_ROTATION("FACE_CREATION_PRESET", charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON], 0, vChildPosition, <<0, 0, 0>>)
							ELSE
								UI3DSCENE_ASSIGN_PED_TO_SLOT_WITH_ROTATION("FACE_CREATION_PRESET", charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER], 0, vChildPosition, <<0, 0, 0>>)
							ENDIF
						#IF IS_DEBUG_BUILD
						ELSE
							IF GET_GAME_TIMER() % 1000 < 75
								PRINTLN("[CHARCREATOR] RENDER_CHAR_CREATOR_PEDS - UI3DSCENE_PUSH_PRESET(FACE_CREATION_CONFIRM) = FALSE")
							ENDIF
						#ENDIF
						
						ENDIF
					ELSE
						IF UI3DSCENE_PUSH_PRESET("FACE_CREATION_PRESET")
						
							IF (IS_CHAR_CREATOR_PED_MALE(charCreatorData)
							OR IS_BIT_SET(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_FORCE_DRAW_MALE))
							AND NOT IS_BIT_SET(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_FORCE_DRAW_FEMALE)
								UI3DSCENE_ASSIGN_PED_TO_SLOT_WITH_ROTATION("FACE_CREATION_PRESET", charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON], 0, vChildPosition, <<0, 0, charCreatorData.pedData.iChildRotation>>)
							ELSE
								UI3DSCENE_ASSIGN_PED_TO_SLOT_WITH_ROTATION("FACE_CREATION_PRESET", charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER], 0, vChildPosition, <<0, 0, charCreatorData.pedData.iChildRotation>>)
							ENDIF
							
							CLEAR_BIT(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_FORCE_DRAW_MALE)
							CLEAR_BIT(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_FORCE_DRAW_FEMALE)
							
							// ***********************************************
							//				PARENT DRAWING LOGIC
							// ***********************************************
											
							IF charCreatorData.charCreatorState != CHARACTER_CREATOR_JOINING_GTA_ONLINE
							AND NOT IS_BIT_SET(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_WAIT_TO_BACK_OUT_OF_CONFIRM_FRAME)
							AND (charCreatorData.mainMenuIndex = HERITAGE_CAT OR (charCreatorData.mainMenuIndex = CONFIRM_CAT AND charCreatorData.prevMainMenuIndex = HERITAGE_CAT))
							
								INT iSpecialHeadIndex
							
								IF GET_CHAR_CREATOR_PARENT_RACE(charCreatorData, CHAR_CREATOR_PED_MUM, CHAR_CREATOR_RACE_INDEX_ONE) != CHAR_CREATOR_SPECIAL_RACE
								AND NOT IS_BIT_SET(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_MAINTAIN_SPECIAL_MUM)
									UI3DSCENE_ASSIGN_PED_TO_SLOT_WITH_ROTATION("FACE_CREATION_PRESET", charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_MUM], 2, vMumPosition + <<0,0,0.02>> + <<charCreatorData.pedData.charCreatorParentInfo.fMumSideMod,charCreatorData.pedData.charCreatorParentInfo.fMumSizeMod,charCreatorData.pedData.charCreatorParentInfo.fMumHeightMod + 0.09>>, <<0.0, 0.0, charCreatorData.pedData.iMumRotation>>)
								ELSE
									iSpecialHeadIndex = GET_CHAR_CREATOR_SPECIAL_HEAD_INDEX(charCreatorData, FALSE)
									
									IF NOT IS_PED_INJURED(charCreatorData.charCreatorSpecialPeds[iSpecialHeadIndex])
										IF HAVE_ALL_STREAMING_REQUESTS_COMPLETED(charCreatorData.charCreatorSpecialPeds[iSpecialHeadIndex])
											UI3DSCENE_ASSIGN_PED_TO_SLOT_WITH_ROTATION("FACE_CREATION_PRESET", charCreatorData.charCreatorSpecialPeds[iSpecialHeadIndex], 2, <<vMumPosition.x, vMumPosition.y, vMumPosition.z + 0.09>> + <<0,0,0.02>>, <<0.0, 0.0, charCreatorData.pedData.iMumRotation>>)
										ENDIF
									ENDIF
				
								ENDIF
								
								IF GET_CHAR_CREATOR_PARENT_RACE(charCreatorData, CHAR_CREATOR_PED_DAD, CHAR_CREATOR_RACE_INDEX_ONE) != CHAR_CREATOR_SPECIAL_RACE
								AND NOT IS_BIT_SET(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_MAINTAIN_SPECIAL_DAD)
									UI3DSCENE_ASSIGN_PED_TO_SLOT_WITH_ROTATION("FACE_CREATION_PRESET", charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAD], 3, vDadPosition + <<0,0,0.02>> + <<charCreatorData.pedData.charCreatorParentInfo.fDadSideMod,charCreatorData.pedData.charCreatorParentInfo.fDadSizeMod, charCreatorData.pedData.charCreatorParentInfo.fDadHeightMod>>, <<0.0, 0.0, charCreatorData.pedData.iDadRotation>>)
								ELSE
									iSpecialHeadIndex = GET_CHAR_CREATOR_SPECIAL_HEAD_INDEX(charCreatorData, TRUE)
									
									IF NOT IS_PED_INJURED(charCreatorData.charCreatorSpecialPeds[iSpecialHeadIndex])
										IF HAVE_ALL_STREAMING_REQUESTS_COMPLETED(charCreatorData.charCreatorSpecialPeds[iSpecialHeadIndex])
											UI3DSCENE_ASSIGN_PED_TO_SLOT_WITH_ROTATION("FACE_CREATION_PRESET", charCreatorData.charCreatorSpecialPeds[iSpecialHeadIndex], 3, vDadPosition + <<0,0,0.02>>, <<0.0, 0.0, charCreatorData.pedData.iDadRotation>>)
										ENDIF
									ENDIF									
								ENDIF
							ENDIF
							
							
							BOOL bDisplayGrandma
							BOOL bDisplayGranddad
							
							//FLOAT fGrandmaX
							//FLOAT fGrandDadX
							
							//CONST_FLOAT MUM_GRANDMA_X 0.6
							//CONST_FLOAT MUM_GRANDDAD_X 0.2
							
							//CONST_FLOAT DAD_GRANDMA_X -0.2
							//CONST_FLOAT DAD_GRANDDAD_X -0.6
							VECTOR vGrandma = <<0.0, 0.0, -10.0>>
							VECTOR vGrandad = <<0.0, 0.0, -10.0>>
							
							PED_INDEX grandmaPed = charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_GRANDMA_M]
							PED_INDEX granddadPed = charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_GRANDDAD_M]
							
							BOOL bAllowGrandparents = FALSE
							IF charCreatorData.bDisplayedGrandparents		
								bAllowGrandparents = TRUE
							ELSE
								IF NOT IS_CHAR_CREATOR_CHILD_SPINNER_DISPLAYED(charCreatorData)
								AND NOT IS_CHAR_CREATOR_PARENTS_SPINNER_DISPLAYED(charCreatorData)
								AND NOT IS_BIT_SET(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_UPDATE_CHILD_BLEND)
								AND NOT IS_BIT_SET(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_DO_RANDOMISE)										
								AND HAS_CHAR_CREATOR_PED_BLEND_FINISHED(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON])
								AND HAS_CHAR_CREATOR_PED_BLEND_FINISHED(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER])
								AND HAS_CHAR_CREATOR_PED_BLEND_FINISHED(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_MUM])
								AND HAS_CHAR_CREATOR_PED_BLEND_FINISHED(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAD])
									bAllowGrandparents = TRUE
								ENDIF
							ENDIF
							
							IF bAllowGrandparents
							AND (charCreatorData.mainMenuIndex = HERITAGE_CAT OR (charCreatorData.mainMenuIndex = CONFIRM_CAT AND charCreatorData.prevMainMenuIndex = HERITAGE_CAT))
								IF charCreatorData.columnIndex = HERITAGE_COLUMN
									SWITCH charCreatorData.heritageIndex			
										CASE HERITAGE_MUM
										CASE HERITAGE_MUM2
										CASE HERITAGE_MUM_DOMINANCE
											IF GET_CHAR_CREATOR_PARENT_RACE(charCreatorData, CHAR_CREATOR_PED_MUM, CHAR_CREATOR_RACE_INDEX_ONE) <> CHAR_CREATOR_SPECIAL_RACE
											AND NOT IS_BIT_SET(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_MAINTAIN_SPECIAL_DAD)
											AND NOT IS_BIT_SET(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_MAINTAIN_SPECIAL_MUM)										
												bDisplayGrandma = TRUE
												bDisplayGrandDad = TRUE
												
//												vGrandma = <<0.450, 0.950, -0.062>>
//												vGrandad = <<0.300, 0.970, -0.065>>
												
												vGrandma = <<0.6, 0.0, 0.0>>
												vGrandad = <<0.2, 0.0, 0.0>>

												grandmaPed = charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_GRANDMA_M]
												granddadPed = charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_GRANDDAD_M]
												charCreatorData.bDisplayedGrandparents = TRUE
											ENDIF
										BREAK	

										CASE HERITAGE_DAD
										CASE HERITAGE_DAD2
										CASE HERITAGE_DAD_DOMINANCE
											IF GET_CHAR_CREATOR_PARENT_RACE(charCreatorData, CHAR_CREATOR_PED_DAD, CHAR_CREATOR_RACE_INDEX_ONE) <> CHAR_CREATOR_SPECIAL_RACE				
											AND NOT IS_BIT_SET(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_MAINTAIN_SPECIAL_DAD)
											AND NOT IS_BIT_SET(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_MAINTAIN_SPECIAL_MUM)
												bDisplayGrandma = TRUE
												bDisplayGrandDad = TRUE
												
												vGrandma = <<-0.2, 0.0, 0.0>>
												vGrandad = <<-0.6, 0.0, 0.0>>

												grandmaPed = charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_GRANDMA_D]
												granddadPed = charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_GRANDDAD_D]
												charCreatorData.bDisplayedGrandparents = TRUE
											ENDIF
										BREAK		
									ENDSWITCH
								ENDIF
							
								IF bDisplayGrandma
									UI3DSCENE_ASSIGN_PED_TO_SLOT_WITH_ROTATION("FACE_CREATION_PRESET", grandmaPed, 1, vGrandma, <<0.0, 0.0, 0.0>>)
								ELSE
									UI3DSCENE_ASSIGN_PED_TO_SLOT_WITH_ROTATION("FACE_CREATION_PRESET", grandmaPed, 1, vGrandma, <<0.0, 0.0, 0.0>>)
								ENDIF
								IF bDisplayGranddad
									UI3DSCENE_ASSIGN_PED_TO_SLOT_WITH_ROTATION("FACE_CREATION_PRESET", granddadPed, 4, vGrandad, <<0.0, 0.0, 0.0>>)
								ELSE
									UI3DSCENE_ASSIGN_PED_TO_SLOT_WITH_ROTATION("FACE_CREATION_PRESET", granddadPed, 4, vGrandad, <<0.0, 0.0, 0.0>>)
								ENDIF
							ENDIF
							
						#IF IS_DEBUG_BUILD
						ELSE
							IF GET_GAME_TIMER() % 1000 < 75
								PRINTLN("[CHARCREATOR] RENDER_CHAR_CREATOR_PEDS - UI3DSCENE_PUSH_PRESET(FACE_CREATION_PRESET) = FALSE")
							ENDIF
						#ENDIF			
						
						ENDIF
					ENDIF
											
					IF IS_CHAR_CREATOR_CHILD_SPINNER_DISPLAYED(charCreatorData)
					OR IS_CHAR_CREATOR_PARENTS_SPINNER_DISPLAYED(charCreatorData)
						IF HAS_CHAR_CREATOR_PED_BLEND_FINISHED(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON])
						AND HAS_CHAR_CREATOR_PED_BLEND_FINISHED(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER])
						AND HAS_CHAR_CREATOR_PED_BLEND_FINISHED(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_MUM])
						AND HAS_CHAR_CREATOR_PED_BLEND_FINISHED(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAD])
						AND NOT IS_BIT_SET(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_UPDATE_CHILD_BLEND)
						AND NOT IS_BIT_SET(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_DO_RANDOMISE)
							
							// We have finished waiting for all peds to finish blending
							SET_WAIT_TO_CLEAR_SPINNERS(charCreatorData)		
							
							//SET_CHAR_CREATOR_CHILD_AS_BUSY(charCreatorData, FALSE)
							//SET_CHAR_CREATOR_PARENTS_AS_BUSY(charCreatorData, FALSE)
							
						#IF IS_DEBUG_BUILD
						ELSE
							IF GET_GAME_TIMER() % 500 < 75
								PRINTLN("[CHARCREATOR] RENDER_CHAR_CREATOR_PEDS - Peds are till blending from Random: ")
								
								IF NOT HAS_CHAR_CREATOR_PED_BLEND_FINISHED(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON])
									PRINTLN("[CHARCREATOR] 			- NOT HAS_CHAR_CREATOR_PED_BLEND_FINISHED(CHAR_CREATOR_PED_SON)")
								ENDIF
								IF NOT HAS_CHAR_CREATOR_PED_BLEND_FINISHED(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER])
									PRINTLN("[CHARCREATOR] 			- NOT HAS_CHAR_CREATOR_PED_BLEND_FINISHED(CHAR_CREATOR_PED_DAUGHTER)")
								ENDIF
								IF NOT HAS_CHAR_CREATOR_PED_BLEND_FINISHED(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_MUM])
									PRINTLN("[CHARCREATOR] 			- NOT HAS_CHAR_CREATOR_PED_BLEND_FINISHED(CHAR_CREATOR_PED_MUM)")
								ENDIF
								IF NOT HAS_CHAR_CREATOR_PED_BLEND_FINISHED(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAD])
									PRINTLN("[CHARCREATOR] 			- NOT HAS_CHAR_CREATOR_PED_BLEND_FINISHED(CHAR_CREATOR_PED_DAD)")
								ENDIF
								
							ENDIF
						#ENDIF
							
						ENDIF
					ENDIF
				
				#IF IS_DEBUG_BUILD
				ELSE
					IF GET_GAME_TIMER() % 1000 < 75
						PRINTLN("[CHARCREATOR] RENDER_CHAR_CREATOR_PEDS - UI3DSCENE_IS_AVAILABLE() = FALSE")
					ENDIF
				#ENDIF
				ENDIF
				
				// Maintain our player speaking animation when we have loaded / created them
				MAINTAIN_CHAR_CREATOR_PLAYER_TALKING(charCreatorData)				
			ENDIF
		ENDIF
	ENDIF
	
	INT i
	// Every frame possible correct our peds
	REPEAT CHAR_CREATOR_PED_MAX i
		
		IF DOES_ENTITY_EXIST(charCreatorData.charCreatorPeds[i])
		AND NOT IS_ENTITY_DEAD(charCreatorData.charCreatorPeds[i])
			CHAR_CREATOR_SET_DESIRED_HEADING(charCreatorData.charCreatorPeds[i])
		ENDIF
	ENDREPEAT
	
	REPEAT CHAR_CREATOR_SPECIAL_PEDS_MAX i
		
		IF DOES_ENTITY_EXIST(charCreatorData.charCreatorSpecialPeds[i])
		AND NOT IS_ENTITY_DEAD(charCreatorData.charCreatorSpecialPeds[i])
			CHAR_CREATOR_SET_DESIRED_HEADING(charCreatorData.charCreatorSpecialPeds[i])
		ENDIF
	ENDREPEAT
	
ENDPROC

/// PURPOSE: True when player confirms option on warning screen
FUNC BOOL PROCESS_CHAR_CREATOR_WARNING(MPHUD_CHARACTER_CREATOR_DATA &charCreatorData, BOOL &bQuitScreen)
	
	TEXT_LABEL_15 tlMainMsg = "FACES_WARN"
	TEXT_LABEL_15 tlExtraMsg = ""
	FE_WARNING_FLAGS charCreatorButtons = FE_WARNING_YESNO

	SWITCH charCreatorData.charCreatorSubState
		CASE CHARACTER_CREATOR_QUITING
			IF NOT HAS_ENTERED_CHAR_CREATION_FROM_ALTERATION_PROMPT()
				tlExtraMsg = "FACES_WARN3"
			ELIF IS_CHANGED_CHARACTER_APPEARANCE()
				tlExtraMsg = "FACES_WARN6"
			ELSE
				tlExtraMsg = "FACES_WARN5"
			ENDIF
		BREAK
		CASE CHARACTER_CREATOR_HOUR_WARNING
			tlMainMsg = "FACES_WARNH"
			tlExtraMsg = "FACES_WARN4"
			charCreatorButtons = FE_WARNING_OK
		BREAK
	ENDSWITCH
	
	SET_WARNING_MESSAGE_WITH_HEADER(tlMainMsg, tlExtraMsg, charCreatorButtons)
	
									
	IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
	
		IF NOT IS_BIT_SET(charCreatorData.iCharCreatorButtons, CHAR_CREATOR_ACCEPT)
		
			SET_BIT(charCreatorData.iCharCreatorButtons, CHAR_CREATOR_ACCEPT)
			
			PRINTLN("[CHARCREATOR] PROCESS_CHAR_CREATOR_WARNING - player confirmed on warning screen")
		
			//PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
			
			bQuitScreen = TRUE
			RETURN TRUE
		ENDIF
	ELSE
		IF IS_BIT_SET(charCreatorData.iCharCreatorButtons, CHAR_CREATOR_ACCEPT)
			CLEAR_BIT(charCreatorData.iCharCreatorButtons, CHAR_CREATOR_ACCEPT)
		ENDIF
	ENDIF
	
	IF charCreatorData.charCreatorSubState != CHARACTER_CREATOR_HOUR_WARNING
		IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
			IF NOT IS_BIT_SET(charCreatorData.iCharCreatorButtons, CHAR_CREATOR_CANCEL)
			
				SET_BIT(charCreatorData.iCharCreatorButtons, CHAR_CREATOR_CANCEL)
			
				PRINTLN("[CHARCREATOR] PROCESS_CHAR_CREATOR_WARNING - player cancelled on warning screen")
			
				//PLAY_SOUND_FRONTEND(-1, "BACK", "HUD_FRONTEND_DEFAULT_SOUNDSET")
			
				bQuitScreen = FALSE
				RETURN TRUE
			ENDIF
		ELSE
			IF IS_BIT_SET(charCreatorData.iCharCreatorButtons, CHAR_CREATOR_CANCEL)
				CLEAR_BIT(charCreatorData.iCharCreatorButtons, CHAR_CREATOR_CANCEL)
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//INT iComponent
/// PURPOSE: Sets the peds component variations and props - for the char creator hacked
PROC SET_PED_VARIATIONS_CHAR_CREATOR(PED_INDEX pedID, PED_VARIATION_STRUCT &sVariations)
	IF NOT IS_PED_INJURED(pedID)
	
		//enumCharacterList eChar = GET_PLAYER_PED_ENUM(pedID)
		//CPRINTLN(DEBUG_PED_COMP, "SET_PED_VARIATIONS for ", eChar, " called by...", GET_THIS_SCRIPT_NAME())
		
		INT i
		REPEAT NUM_PED_COMPONENTS i
			SET_PED_COMPONENT_VARIATION(pedID, INT_TO_ENUM(PED_COMPONENT, i), sVariations.iDrawableVariation[i], sVariations.iTextureVariation[i], sVariations.iPaletteVariation[i])
		
			//CPRINTLN(DEBUG_PED_COMP, GET_COMP_VARIATION_TYPE_STRING(INT_TO_ENUM(PED_COMPONENT, i)), "...component variation [",sVariations.iDrawableVariation[i], ",", sVariations.iTextureVariation[i], "]")
		ENDREPEAT
		
		REPEAT NUM_PLAYER_PED_PROPS i
			IF sVariations.iPropIndex[i] != -1
			AND sVariations.iPropIndex[i] != 255
				//PRINTSTRING("...prop [")PRINTINT(sVariations.iPropIndex[i])PRINTSTRING(",")PRINTINT(sVariations.iPropTexture[i])PRINTSTRING("]")PRINTNL()
				SET_PED_PROP_INDEX(pedID, INT_TO_ENUM(PED_PROP_POSITION, i), sVariations.iPropIndex[i], sVariations.iPropTexture[i])
			ELSE
				CLEAR_PED_PROP(pedID, INT_TO_ENUM(PED_PROP_POSITION, i))
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC



// ******************************************************************
// 		MAIN SCREEN COMMAND: Manages loading / running of screen
// ******************************************************************

PROC PROCESS_XML_MPHUD_CHARACTER_CREATOR(MPHUD_PLACEMENT_TOOLS& Placement, MPHUD_CHARACTER_CREATOR_DATA &charCreatorData)
	// Do Everything	
		
	BOOL bHasQuit
		
	FORCE_SCRIPTED_GFX_WHEN_FRONTEND_ACTIVE(TRUE)
	SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)

	IF charCreatorData.charCreatorState <> CHARACTER_CREATOR_JOINING_GTA_ONLINE
	OR IS_BIT_SET(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_WAIT_TO_BACK_OUT_OF_CONFIRM_FRAME)
		DRAW_BACKGROUND_BIRDS(GFX_ORDER_BEFORE_HUD_PRIORITY_LOW)	
	ELSE
		RECT aRect
		aRect.x = 0.5
		aRect.y = 0.5
		aRect.w = 1.0
		aRect.h = 1.0
		aRect.r = 255
		aRect.g = 255
		aRect.b = 255
		aRect.a = 255
		SET_RECT_TO_THIS_HUD_COLOUR(aRect, HUD_COLOUR_FREEMODE)	
		SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD_PRIORITY_LOW)
		DRAW_RECTANGLE(aRect)
		SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)		
	ENDIF
	
	// set spinner on if have requested to do so
	IF IS_BIT_SET(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_DO_CHILD_BUSY_SPINNER)
		SET_CHAR_CREATOR_CHILD_AS_BUSY(charCreatorData, TRUE)
		CLEAR_BIT(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_DO_CHILD_BUSY_SPINNER)
	ENDIF
	
	// randomise if we have requested to do so
	IF IS_BIT_SET(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_DO_RANDOMISE)
		IF ARE_ALL_CHAR_CREATOR_PEDS_LOADED(charCreatorData)
			DO_CHAR_CREATOR_RANDOMISE(charCreatorData)
		ENDIF	
		
		CLEAR_BIT(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_DO_RANDOMISE)
	ENDIF	
	
	// Attempt to render first (fix bug where characters were STATIC if this and function below were switched)
	RENDER_CHAR_CREATOR_PEDS(charCreatorData)	
	
	IF IS_BIT_SET(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_MAINTAIN_SPECIAL_DAD)
		IF HAS_CHAR_CREATOR_PED_BLEND_FINISHED(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAD])
			CLEAR_BIT(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_MAINTAIN_SPECIAL_DAD)
			SETUP_CHAR_CREATOR_HERITAGE(charCreatorData, FALSE, FALSE, TRUE)
		ENDIF
	ENDIF
	IF IS_BIT_SET(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_MAINTAIN_SPECIAL_MUM)
		IF HAS_CHAR_CREATOR_PED_BLEND_FINISHED(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_MUM])
			CLEAR_BIT(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_MAINTAIN_SPECIAL_MUM)
			SETUP_CHAR_CREATOR_HERITAGE(charCreatorData, FALSE, FALSE, TRUE)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(charCreatorData.iCharCreatorBitset, CHAR_CREATOR_DO_DELAYED_CREW_SHIRT_ZOOM)
		APPLY_CREW_TSHIRT_ZOOM(charCreatorData)
		CLEAR_BIT(charCreatorData.iCharCreatorBitset, CHAR_CREATOR_DO_DELAYED_CREW_SHIRT_ZOOM)
	ENDIF
	
	IF IS_BIT_SET(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_FORCE_HERITAGE_UPDATE)
		IF IS_BIT_SET(charCreatorData.iCharCreatorBitset, CHAR_CREATOR_ALL_PEDS_LOADED)
			SETUP_CHAR_CREATOR_HERITAGE(charCreatorData, FALSE, FALSE, TRUE)
			CLEAR_BIT(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_FORCE_HERITAGE_UPDATE)
		ENDIF
	ENDIF
	
	SWITCH charCreatorData.charCreatorState
	
		// ********************************************************
		// 					CHARACTER_CREATOR_INIT
		// First frame initialisation of Character creation screen
		// ********************************************************

		CASE CHARACTER_CREATOR_INIT
		
			IF NOT IS_PAUSE_MENU_RESTARTING()
			AND GET_PAUSE_MENU_STATE() != PM_RESTARTING
			AND GET_PAUSE_MENU_STATE() != PM_STARTING_UP
			AND GET_PAUSE_MENU_STATE() != PM_SHUTTING_DOWN
			
				PRINTLN("[CHARCREATOR]")
				PRINTLN("[CHARCREATOR] *****************************************************")
				PRINTLN("[CHARCREATOR]				CHARACTER CREATOR ENTERED				")
				PRINTLN("[CHARCREATOR] *****************************************************")
				PRINTLN("[CHARCREATOR]")
				PRINTLN("[CHARCREATOR] GET_PED_HEAD_BLEND_FIRST_INDEX(HBHT_FEMALE)        = ", GET_PED_HEAD_BLEND_FIRST_INDEX(HBHT_FEMALE))
				PRINTLN("[CHARCREATOR] GET_PED_HEAD_BLEND_FIRST_INDEX(HBHT_MALE)          = ", GET_PED_HEAD_BLEND_FIRST_INDEX(HBHT_MALE))
				PRINTLN("[CHARCREATOR] GET_PED_HEAD_BLEND_FIRST_INDEX(HBHT_UNIQUE_FEMALE) = ", GET_PED_HEAD_BLEND_FIRST_INDEX(HBHT_UNIQUE_FEMALE))
				PRINTLN("[CHARCREATOR] GET_PED_HEAD_BLEND_FIRST_INDEX(HBHT_UNIQUE_MALE)   = ", GET_PED_HEAD_BLEND_FIRST_INDEX(HBHT_UNIQUE_MALE))
				PRINTLN("[CHARCREATOR] *****************************************************")
				PRINTLN("[CHARCREATOR]")
				
				PRINTSTRING("Initialising Character Creator") PRINTNL()
				#IF NOT	IS_JAPANESE_BUILD
					PRINTSTRING("Not Japanese Build") PRINTNL()
				#ENDIF
				#IF IS_JAPANESE_BUILD
					PRINTSTRING("With Japanese Build") PRINTNL()
				#ENDIF

				// Always reset our variables coming into screen
				RESET_CHARACTER_CREATOR_DATA(charCreatorData)

				// Clear out our presets
				UI3DSCENE_CLEAR_PATCHED_DATA()
				
				charCreatorData.iSoundID = GET_SOUND_ID()
				REQUEST_SCRIPT_AUDIO_BANK("HUD_321_GO")

				charCreatorData.iCharacterSelectSlot = Placement.ScreenPlace.iSelectedCharacter
			
				IF IS_PAUSE_MENU_ACTIVE_EX()
				
					//...yes, check if our menu is already being displayed
					IF GET_CURRENT_FRONTEND_MENU_VERSION() != FE_MENU_VERSION_MP_CHARACTER_CREATION
						PRINTLN("[CHARCREATOR] PROCESS_XML_MPHUD_CHARACTER_CREATOR - RESTART_FRONTEND_MENU(FE_MENU_VERSION_MP_CHARACTER_CREATION)")
					
						//...no, restart the frontend with correct menu
						RESTART_FRONTEND_MENU(FE_MENU_VERSION_MP_CHARACTER_CREATION)
						
					ELSE
						PRINTLN("[CHARCREATOR] PROCESS_XML_MPHUD_CHARACTER_CREATOR - GET_CURRENT_FRONTEND_MENU_VERSION() = FE_MENU_VERSION_MP_CHARACTER_CREATION")
						
						PRINTLN("[CHARCREATOR] PROCESS_XML_MPHUD_CHARACTER_CREATOR - SET_BIT(charCreatorData.iCharCreatorBitSet, CORONA_SKIP_WAIT_FOR_FRONTEND)")					
						SET_BIT(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_SKIP_WAIT_FOR_FRONTEND)
					ENDIF	
				ELSE
					PRINTLN("[CHARCREATOR] PROCESS_XML_MPHUD_CHARACTER_CREATOR - ACTIVATE_FRONTEND_MENU(FE_MENU_VERSION_MP_CHARACTER_CREATION, FALSE)")
				
					ACTIVATE_FRONTEND_MENU(FE_MENU_VERSION_MP_CHARACTER_CREATION, FALSE)
				ENDIF

				// Initialise our values and set up
				IF HAS_ENTERED_CHAR_CREATION_FROM_ALTERATION_PROMPT()
					PRINTLN("[CHARCREATOR] ENTERING FROM ALTERATION PROMPT")
					GENERATE_CHAR_CREATOR_SETUP_FROM_STATS(Placement, charCreatorData)
					IF IS_CHAR_CREATOR_PED_MALE(charCreatorData)
						charCreatorData.bSonLockedToOldOutfit = TRUE
						charCreatorData.bDaughterLockedToOldOutfit = FALSE
						
						//set_ped_comp_item_current_mp(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON], COMP_TYPE_BERD, BERD_FMM_0_0, FALSE)
						SET_PED_COMPONENT_VARIATION(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON], PED_COMP_BERD, 0, 0)
					ELSE
						charCreatorData.bSonLockedToOldOutfit = FALSE
						charCreatorData.bDaughterLockedToOldOutfit = TRUE
						
						//set_ped_comp_item_current_mp(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER], COMP_TYPE_BERD, BERD_FMF_0_0, FALSE)
						SET_PED_COMPONENT_VARIATION(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER], PED_COMP_BERD, 0, 0)
					ENDIF
					SET_BIT(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_FORCE_HERITAGE_UPDATE)
				ELSE
					PRINTLN("[CHARCREATOR] ENTERING FROM NEW CHARACTER")
					charCreatorData.bSonLockedToOldOutfit = FALSE
					charCreatorData.bDaughterLockedToOldOutfit = FALSE
					// override overrides for hair left over from an old character, if starting a new one
					SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_FORCED_HAIR_ITEM, ENUM_TO_INT(DUMMY_PED_COMP), charCreatorData.iCharacterSelectSlot)
					SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_FORCED_HAIR_TYPE, ENUM_TO_INT(COMP_TYPE_HAIR), charCreatorData.iCharacterSelectSlot)
					
					PRINTLN("ENTERING SLOT ", charCreatorData.iCharacterSelectSlot)
					
					SET_CURRENT_MP_SLOT_FOR_CLOTHES(charCreatorData.iCharacterSelectSlot)
					
					// randomise gender
					IF GET_RANDOM_INT_IN_RANGE(0,2) = 0
						charCreatorData.pedData.charGender = CHAR_CREATOR_GENDER_MALE
					ELSE
						charCreatorData.pedData.charGender = CHAR_CREATOR_GENDER_FEMALE
					ENDIF
					
					
					//INT i
					//REPEAT MAX_PARENT_OPTIONS i 
					//	charCreatorData.cachedMum[i].bFilled = FALSE
					//	charCreatorData.cachedDad[i].bFilled = FALSE
					//ENDREPEAT					
					
					GENERATE_CHAR_CREATOR_RANDOM_LIFESTYLE(charCreatorData, DEFAULT, TRUE)
					GENERATE_CHAR_CREATOR_RANDOM_PARENTS(charCreatorData, TRUE)
					SET_CHAR_CREATOR_RANDOM_PARENT_DOM(charCreatorData)
					
				
					charCreatorData.iHeadForDadRace = GET_GRANDPARENT_FROM_HEAD_AND_RACE(charCreatorData, CHAR_CREATOR_PED_DAD, CHAR_CREATOR_RACE_INDEX_ONE)
					charCreatorData.iHeadForDadRace2 = GET_GRANDPARENT_FROM_HEAD_AND_RACE(charCreatorData, CHAR_CREATOR_PED_DAD, CHAR_CREATOR_RACE_INDEX_TWO)
					charCreatorData.iHeadForMumRace = GET_GRANDPARENT_FROM_HEAD_AND_RACE(charCreatorData, CHAR_CREATOR_PED_MUM, CHAR_CREATOR_RACE_INDEX_ONE)
					charCreatorData.iHeadForMumRace2 = GET_GRANDPARENT_FROM_HEAD_AND_RACE(charCreatorData, CHAR_CREATOR_PED_MUM, CHAR_CREATOR_RACE_INDEX_TWO)					
										
					
					GENERATE_CHAR_CREATOR_RANDOM_PARENTS_HAIR(charCreatorData, CHAR_CREATOR_PED_MUM)
					GENERATE_CHAR_CREATOR_RANDOM_PARENTS_HAIR(charCreatorData, CHAR_CREATOR_PED_DAD)		
					
					charCreatorData.bCreatedFirstPrettyChild = TRUE
				//	FILL_CACHED_PARENTS(charCreatorData)

				//	GENERATE_CHAR_CREATOR_RANDOM_HAIR(charCreatorData)
				//	GENERATE_CHAR_CREATOR_RANDOM_MAKEUP(charCreatorData)
				ENDIF
				
				RANDOMISE_SPECIAL_HEAD(charCreatorData)
				
				PROCESS_CHAR_CREATOR_ZOOM(charCreatorData, 0)

				charCreatorData.iCharacterSelectSlot = Placement.ScreenPlace.iSelectedCharacter
							
				CALC_CHAR_CREATOR_UGLY_VALUE(charCreatorData)
				CALC_CHAR_CREATOR_OVERLAY_VALUES(charCreatorData)
				
				REFRESH_SCALEFORMXML_TABS_FACES(Placement.ScaleformXMLTabStruct)
				REFRESH_SCALEFORMXML_TABS_EMBLEM(Placement.ScaleformXMLTabStruct)
							
				PRINTLN("[CHARCREATOR] PROCESS_XML_MPHUD_CHARACTER_CREATOR - charCreatorData.charCreatorState = CHARACTER_CREATOR_WAIT_FOR_FRONTEND")
				charCreatorData.charCreatorState = CHARACTER_CREATOR_WAIT_FOR_FRONTEND
				
				#IF IS_DEBUG_BUILD
					CREATE_CHAR_CREATOR_WIDGETS(charCreatorData)
				#ENDIF
				
			ELSE
			
				#IF IS_DEBUG_BUILD
				PRINTNL()
				PRINTLN("[CHARCREATOR] *********************************************")
				PRINTLN("[CHARCREATOR]				CHARACTER CREATOR 				")
				PRINTLN("[CHARCREATOR] *********************************************")
				PRINTNL()
			
				
				IF IS_PAUSE_MENU_RESTARTING()
					PRINTLN("	 -  IS_PAUSE_MENU_RESTARTING() = TRUE ")
				ENDIF
				IF GET_PAUSE_MENU_STATE() = PM_RESTARTING
					PRINTLN("	 -  GET_PAUSE_MENU_STATE() = PM_RESTARTING ")
				ENDIF
				IF GET_PAUSE_MENU_STATE() = PM_STARTING_UP
					PRINTLN("	 -  GET_PAUSE_MENU_STATE() = PM_STARTING_UP ")
				ENDIF
				IF GET_PAUSE_MENU_STATE() = PM_SHUTTING_DOWN
					PRINTLN("	 -  GET_PAUSE_MENU_STATE() = PM_SHUTTING_DOWN ")
				ENDIF
				#ENDIF
			ENDIF
		BREAK
		
		
		// ********************************************************
		// 			CHARACTER_CREATOR_WAIT_FOR_FRONTEND
		// Wait until scaleform has loaded and pause menu is active
		// ********************************************************

		CASE CHARACTER_CREATOR_WAIT_FOR_FRONTEND
			
			REQUEST_ADDITIONAL_TEXT("HAR_MNU", MENU_TEXT_SLOT)
			
			IF HAS_ADDITIONAL_TEXT_LOADED(MENU_TEXT_SLOT)
			
				IF IS_PAUSE_MENU_ACTIVE()
				AND NOT IS_PAUSE_MENU_RESTARTING()
				
					IF IS_FRONTEND_READY_FOR_CONTROL()
						IF CODE_WANTS_SCRIPT_TO_TAKE_CONTROL()
					
							IF HAS_MENU_LAYOUT_CHANGED_EVENT_OCCURRED()
												
								GET_MENU_LAYOUT_CHANGED_EVENT_DETAILS(Placement.CurrentScreen, Placement.aNextScreen, Placement.iMenu)
								
								PRINTLN("[CHARCREATOR] PROCESS_XML_MPHUD_CHARACTER_CREATOR - HAS_MENU_LAYOUT_CHANGED_EVENT_OCCURRED() returned TRUE")
								PRINTLN("		- CurrentScreen = ", GET_XML_SCREEN_STRING_VALUE(Placement.CurrentScreen))
								PRINTLN("		- aNextScreen = ", GET_XML_SCREEN_STRING_VALUE(Placement.aNextScreen))
								PRINTLN("		- iMenu = ", Placement.iMenu)
								
								IF Placement.aNextScreen = MENU_UNIQUE_ID_CREATION_HERITAGE
								OR IS_BIT_SET(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_SKIP_WAIT_FOR_FRONTEND)
									// Take control of screen and don't give back each frame
									TAKE_CONTROL_OF_FRONTEND()
									
									// Prevent scaleform from handing certain mouse events as they're handled by script.
									LOCK_MOUSE_SUPPORT()
									
									// Set our draw order
									SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_FADE)
									
									SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
									
	//								ANIMPOSTFX_START_CROSSFADE("DefaultMPScreenFade", "SwitchOpenNeutral", 300)
																	
									// Initialise our scaleform data
									charCreatorData.CHAR_COL1_ID = ENUM_TO_INT(MENU_UNIQUE_ID_CREATION_HERITAGE_LIST) + 1000
									charCreatorData.CHAR_COL2_ID = ENUM_TO_INT(MENU_UNIQUE_ID_CREATION_LIFESTYLE_LIST) + 1000
									charCreatorData.CHAR_COL3_ID = ENUM_TO_INT(MENU_UNIQUE_ID_CREATION_YOU) + 1000
									
									// Setting columns as busy
									SET_CHAR_CREATOR_CHILD_AS_BUSY(charCreatorData, TRUE)
									SET_CHAR_CREATOR_PARENTS_AS_BUSY(charCreatorData, TRUE)	

									// Do any setup of visuals
									INITIALISE_CHAR_CREATOR_DISPLAY(charCreatorData)
									
									// Initialise the stat data and view
									SET_CHAR_CREATOR_STATS(charCreatorData, FALSE)
									
									// Move our focus into the first column
									SET_FRONTEND_COLUMN_FOCUS(CHAR_CREATOR_COL_MAIN_MENU, TRUE, FALSE)
									SET_FRONTEND_HIGHLIGHT(CHAR_CREATOR_COL_MAIN_MENU, 0, TRUE)
									
									// Set the help text for the selected option
									SET_CHAR_CREATOR_HELP_TEXT(charCreatorData)
									
									// Setup our tabs
									RUN_CHAR_CREATOR_TABS(Placement)
													
									// Update our childs overlays
									PROCESS_CHAR_CREATOR_CHILD_OVERLAYS(charCreatorData)
									
									PRINTLN("[CHARCREATOR] PROCESS_XML_MPHUD_CHARACTER_CREATOR - charCreatorData.charCreatorState = CHARACTER_CREATOR_RUN")
									
									
									charCreatorData.charCreatorState = CHARACTER_CREATOR_RUN
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				
				#IF IS_DEBUG_BUILD
				ELSE
				
					PRINTLN("[CHARCREATOR] PROCESS_XML_MPHUD_CHARACTER_CREATOR - WAITING FOR FRONTEND...")
					
					IF NOT IS_PAUSE_MENU_ACTIVE()
						PRINTLN("	 - IS_PAUSE_MENU_ACTIVE() = FALSE")
					ENDIF
					
					IF IS_PAUSE_MENU_RESTARTING()
						PRINTLN("	 - IS_PAUSE_MENU_RESTARTING() = FALSE")
					ENDIF
				
				#ENDIF
				
				ENDIF
			ENDIF
			
		BREAK
		
		
		// *******************************************************
		// 					CHARACTER_CREATOR_RUN
		// Maintain the creation screen now everything is running
		// *******************************************************
		
		CASE CHARACTER_CREATOR_RUN
			// If in running state - handle the creator control
			PROCESS_CHARACTER_CREATOR_CONTROL(charCreatorData)
		BREAK
		
			
		// *******************************************************
		// 				CHARACTER_CREATOR_INPUT_NAME
		//   Maintain the creation screen for player name input
		// *******************************************************
		
		CASE CHARACTER_CREATOR_INPUT_NAME
			
			IF GET_PLAYER_TRANSITION_KEYBOARD_INPUT_NAME(Placement.Transition_oskStatus, Placement.iTransition_KeyboardStage, Placement.iTransition_ProfanityCheck, Placement.iTransition_NumberOfAttempts, bHasQuit, Placement.ScreenPlace.iSelectedCharacter)
				
				PRINTLN("[CHARCREATOR] PROCESS_XML_MPHUD_CHARACTER_CREATOR - CHARACTER_CREATOR_INPUT_NAME New name has been entered")

				PROCESS_CHAR_CREATOR_NAME_ENTERED(charCreatorData)				
			ELSE
				IF bHasQuit
				
					IF HAS_SCALEFORM_MOVIE_LOADED(charCreatorData.sfBackground)
						SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(charCreatorData.sfBackground)
					ENDIF		
					IF HAS_SCALEFORM_MOVIE_LOADED(charCreatorData.sfForeground)
						SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(charCreatorData.sfForeground)
					ENDIF	
					IF HAS_SCALEFORM_MOVIE_LOADED(charCreatorData.sfCelebration)
						SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(charCreatorData.sfCelebration)
					ENDIF	
				
					PRINTLN("[CHARCREATOR] PROCESS_XML_MPHUD_CHARACTER_CREATOR - CHARACTER_CREATOR_INPUT_NAME Player has quit this screen")
					
					charCreatorData.charCreatorState = CHARACTER_CREATOR_RUN
				ENDIF
			ENDIF
			
		BREAK		
		
		// *******************************************************
		// 			CHARACTER_CREATOR_WARNING_MESSAGE
		// 	   Maintain the creation screen warning screen
		// *******************************************************
		
		CASE CHARACTER_CREATOR_JOINING_GTA_ONLINE
		
			IF MAINTAIN_CHAR_CREATOR_CONFIRM_SCREEN(charCreatorData, Placement)
			
				// release scaleform
				IF HAS_SCALEFORM_MOVIE_LOADED(charCreatorData.sfForeground)
					SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(charCreatorData.sfForeground)
				ENDIF
				IF HAS_SCALEFORM_MOVIE_LOADED(charCreatorData.sfBackground)
					SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(charCreatorData.sfBackground)
				ENDIF
				IF HAS_SCALEFORM_MOVIE_LOADED(charCreatorData.sfCelebration)
					SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(charCreatorData.sfCelebration)
				ENDIF			
					
				PROCESS_PLAYER_JOINING_GTA_ONLINE(Placement, charCreatorData) 

				// Update our globals so we post to facebook if possible
				IF NETWORK_HAVE_ROS_SOCIAL_CLUB_PRIV()
					g_bPostCharacterFacebookUpdate = TRUE
					g_iCharacterFacebookUpdateState = 0
				ENDIF

				// Kill the pause menu
				SET_FRONTEND_ACTIVE(FALSE)
			ENDIF
		BREAK
		
		CASE CHARACTER_CREATOR_WARNING_MESSAGE
		
			// Process the confirmation screen
			BOOL bQuit
			IF PROCESS_CHAR_CREATOR_WARNING(charCreatorData, bQuit)
				IF bQuit
					IF charCreatorData.charCreatorSubState = CHARACTER_CREATOR_QUITING
						PRINTLN("[CHARCREATOR] PROCESS_XML_MPHUD_CHARACTER_CREATOR - Quitting character creator")
						PROCESS_PLAYER_QUITING_CHARACTER_CREATOR(charCreatorData)
					ELSE
						PRINTLN("[CHARCREATOR] PROCESS_XML_MPHUD_CHARACTER_CREATOR - Hour warning, return to character creator")
						charCreatorData.charCreatorState = CHARACTER_CREATOR_RUN
					ENDIF
				ELSE
					PRINTLN("[CHARCREATOR] PROCESS_XML_MPHUD_CHARACTER_CREATOR - Returning to character creator")
					charCreatorData.charCreatorState = CHARACTER_CREATOR_RUN
				ENDIF
			ENDIF
		
		BREAK
	
	ENDSWITCH
	
	// Attempt to render first (fix bug where characters were STATIC if this and function below were switched)
	//RENDER_CHAR_CREATOR_PEDS(charCreatorData)
		
	// Ensure everything is loaded
	BOOL bResetAppearanceMenu
	CHAR_CREATOR_LOAD_PEDS(charCreatorData, Placement, bResetAppearanceMenu)
	IF bResetAppearanceMenu
		SETUP_CHAR_CREATOR_APPEARANCE(charCreatorData)
	ENDIF
	
	// Parent ped blends
	IF IS_BIT_SET(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_UPDATE_MUM_BLEND)
		PROCESS_CHAR_CREATOR_PARENT_UPDATE(charCreatorData, CHAR_CREATOR_PED_MUM, FALSE, TRUE)
	ENDIF
	IF IS_BIT_SET(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_UPDATE_DAD_BLEND)
		PROCESS_CHAR_CREATOR_PARENT_UPDATE(charCreatorData, CHAR_CREATOR_PED_DAD, FALSE, TRUE)
	ENDIF	
	
	// Maintain our child ped blends
	MAINTAIN_CHAR_CREATOR_CHILD_BLENDS(charCreatorData)
	
	BOOL bSetTatsThisFrame = FALSE
	// Copy outfits across from dummy if required
	IF IS_BIT_SET(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_COPY_FROM_SON_DUMMY)
		IF GET_GAME_TIMER() >= charCreatorData.iSetSonDummyTime + 300
		AND HAS_CHAR_CREATOR_PED_BLEND_FINISHED(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON])
		AND HAS_CHAR_CREATOR_PED_BLEND_FINISHED(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER])

			PED_VARIATION_STRUCT sonVariations
			GET_PED_VARIATIONS(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON_DUMMY], sonVariations)
			SET_PED_VARIATIONS_CHAR_CREATOR(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON], sonVariations)
			
			IF IS_CHAR_CREATOR_PED_MALE(charCreatorData)
				EQUIP_TORSO_DECALS_MP(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON], COMP_TYPE_JBIB, GET_PED_COMP_ITEM_CURRENT_MP(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON], COMP_TYPE_JBIB), FALSE)	// re-equip decals
				GIVE_CHAR_CREATOR_PED_HAIR(charCreatorData, CHAR_CREATOR_PED_SON, TRUE)
				APPLY_CREW_TSHIRT_DECALS_TO_CURRENT_PED(charCreatorData)
			ENDIF
			
			IF IS_CHAR_CREATOR_PED_MALE(charCreatorData)
				IF IS_BIT_SET(charCreatorData.iCharCreatorBitset, CHAR_CREATOR_READY_DELAYED_CREW_SHIRT_ZOOM)
					APPLY_CREW_TSHIRT_ZOOM(charCreatorData)
					CLEAR_BIT(charCreatorData.iCharCreatorBitset, CHAR_CREATOR_READY_DELAYED_CREW_SHIRT_ZOOM)
					SET_BIT(charCreatorData.iCharCreatorBitset, CHAR_CREATOR_DO_DELAYED_CREW_SHIRT_ZOOM)
				ENDIF		
			ENDIF
			
			bSetTatsThisFrame = TRUE
			
			IF HAS_ENTERED_CHAR_CREATION_FROM_ALTERATION_PROMPT()
				PRINTLN("[CHARCREATOR] Clearing any masks (male)")
	
				//set_ped_comp_item_current_mp(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON], COMP_TYPE_BERD, BERD_FMM_0_0, FALSE)
				SET_PED_COMPONENT_VARIATION(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON], PED_COMP_BERD, 0, 0)
			ENDIF
			
			PROCESS_CHAR_CREATOR_CHILD_OVERLAYS(charCreatorData, TRUE, FALSE)
			
			CLEAR_BIT(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_COPY_FROM_SON_DUMMY)
		ENDIF
	ENDIF

	IF NOT bSetTatsThisFrame
		IF IS_BIT_SET(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_COPY_FROM_DAUGHTER_DUMMY)
			IF GET_GAME_TIMER() >= charCreatorData.iSetDaughterDummyTime + 300
			AND HAS_CHAR_CREATOR_PED_BLEND_FINISHED(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER])
			AND HAS_CHAR_CREATOR_PED_BLEND_FINISHED(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON])

				PED_VARIATION_STRUCT daughterVariations
				GET_PED_VARIATIONS(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER_DUMMY], daughterVariations)
				SET_PED_VARIATIONS_CHAR_CREATOR(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER], daughterVariations)
				
				IF NOT IS_CHAR_CREATOR_PED_MALE(charCreatorData)
					EQUIP_TORSO_DECALS_MP(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER], COMP_TYPE_JBIB, GET_PED_COMP_ITEM_CURRENT_MP(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER], COMP_TYPE_JBIB), FALSE)
					GIVE_CHAR_CREATOR_PED_HAIR(charCreatorData, CHAR_CREATOR_PED_DAUGHTER, TRUE)	
					APPLY_CREW_TSHIRT_DECALS_TO_CURRENT_PED(charCreatorData)
				ENDIF
				
				IF NOT IS_CHAR_CREATOR_PED_MALE(charCreatorData)
					IF IS_BIT_SET(charCreatorData.iCharCreatorBitset, CHAR_CREATOR_READY_DELAYED_CREW_SHIRT_ZOOM)
						APPLY_CREW_TSHIRT_ZOOM(charCreatorData)
						CLEAR_BIT(charCreatorData.iCharCreatorBitset, CHAR_CREATOR_READY_DELAYED_CREW_SHIRT_ZOOM)
						SET_BIT(charCreatorData.iCharCreatorBitset, CHAR_CREATOR_DO_DELAYED_CREW_SHIRT_ZOOM)
					ENDIF		
				ENDIF		

				IF HAS_ENTERED_CHAR_CREATION_FROM_ALTERATION_PROMPT()
					PRINTLN("[CHARCREATOR] Clearing any masks (female)")
		
					//set_ped_comp_item_current_mp(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER], COMP_TYPE_BERD, BERD_FMF_0_0, FALSE)
					SET_PED_COMPONENT_VARIATION(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER], PED_COMP_BERD, 0, 0)
				ENDIF
				
				PROCESS_CHAR_CREATOR_CHILD_OVERLAYS(charCreatorData, FALSE, TRUE)
		
				CLEAR_BIT(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_COPY_FROM_DAUGHTER_DUMMY)
			ENDIF
		ENDIF	
	ENDIF

	TRANSITION_CHARACTER_SCREEN_EVENT_LISTENERS()
	
	IF IS_BIT_SET(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_WAIT_TO_CLEAR_SPINNERS)
		IF NOT HAS_CHAR_CREATOR_PED_BLEND_FINISHED(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_SON])
		OR NOT HAS_CHAR_CREATOR_PED_BLEND_FINISHED(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAUGHTER])
		OR NOT HAS_CHAR_CREATOR_PED_BLEND_FINISHED(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_MUM])
		OR NOT HAS_CHAR_CREATOR_PED_BLEND_FINISHED(charCreatorData.charCreatorPeds[CHAR_CREATOR_PED_DAD])
		OR IS_BIT_SET(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_UPDATE_CHILD_BLEND)
		OR IS_BIT_SET(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_DO_RANDOMISE)
			charCreatorData.iWaitForSpinnersCounter = 0
		ELSE
			IF charCreatorData.iWaitForSpinnersCounter > 2
				SET_CHAR_CREATOR_CHILD_AS_BUSY(charCreatorData, FALSE)
				SET_CHAR_CREATOR_PARENTS_AS_BUSY(charCreatorData, FALSE)
				CLEAR_BIT(charCreatorData.iCharCreatorBitSet, CHAR_CREATOR_WAIT_TO_CLEAR_SPINNERS)
			ELSE
				charCreatorData.iWaitForSpinnersCounter++
			ENDIF
		ENDIF
	ENDIF	

	#IF IS_DEBUG_BUILD
	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_EQUALS, KEYBOARD_MODIFIER_SHIFT, "Char Creator Debug")
		PRINTLN("JA: Debug info toggle ", charCreatorData.bDebugInfo)
		charCreatorData.bDebugInfo = !charCreatorData.bDebugInfo
			
	ENDIF
	
	MAINTAIN_CHAR_CREATOR_WIDGETS(charCreatorData)
		
	IF charCreatorData.bDebugInfo
		CHAR_CREATOR_DEBUG_INFO(charCreatorData)
	ENDIF
	#ENDIF
	
ENDPROC



