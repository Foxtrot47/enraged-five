
USING "screens_header.sch"
USING "net_events.sch"
USING "script_network.sch"
USING "Transition_Common.sch"

FUNC STRING TEXTLABEL_TO_STRING_MPHUD_DIRTY_LOAD_CONFIRM(STRING strInput)
	RETURN strInput
ENDFUNC


PROC RENDER_MPHUD_DIRTY_LOAD_CONFIRM(MPHUD_PLACEMENT_TOOLS& Placement)

	SET_ALL_MENUPED_VISIBLE(Placement.SelectionPed, Placement.IdlePed, FALSE )		

	STRING sStatementText = "HUD_CONNPROB"

//	TEXT_LABEL_63 strDate
//	
//	INT iYear, iMonth, iDay, iHour, iMinute, iSecond
//	STAT_GET_PROFILE_STATS_SERVER_SAVE_TIME(iYear, iMonth, iDay, iHour, iMinute, iSecond)
//	
//	// international date format
//	strDate = ""
//	strDate += iYear
//	strDate += "-"
//	IF (iMonth < 10)
//		strDate += "0"
//	ENDIF
//	strDate += iMonth
//	strDate += "-"
//	IF (iDay < 10)
//		strDate += "0"
//	ENDIF
//	strDate += iDay	
//	strDate += " "
//	
//	IF (iHour < 10)
//		strDate += "0"
//	ENDIF
//	strDate += iHour
//	strDate += ":"
//	IF (iMinute < 10)
//		strDate += "0"
//	ENDIF
//	strDate += iMinute
////	strDate += ":"
////	IF (iSecond < 10)
////		strDate += "0"
////	ENDIF
////	strDate += iSecond
//	
//	STRING sSubText
//	
//	sSubText = TEXTLABEL_TO_STRING_MPHUD_DIRTY_LOAD_CONFIRM(strDate)
	
	FE_WARNING_FLAGS buttons = FE_WARNING_CONTINUE
	
//	SET_WARNING_MESSAGE_WITH_HEADER_AND_SUBSTRING_FLAGS(sStatementText, "HUD_STATDIRTY2", buttons, "", FALSE, -1, WARNING_MESSAGE_FIRST_SUBSTRING_IS_LITERAL, sSubText)

	SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, "HUD_STATDIRTY3", buttons)


ENDPROC

PROC LOGIC_MPHUD_DIRTY_LOAD_CONFIRM()
	
	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)	
        SET_TRANSITION_STRING("HUD_QUITTING")      
		REQUEST_TRANSITION(TRANSITION_STATE_TERMINATE_SESSION, GAMEMODE_SP, HUD_STATE_LOADING)
		SET_RETURN_TO_SP_RELOAD(TRUE)
		RELOAD_EVERYTHING_NOW()
	ENDIF

//	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
//		SET_JOINING_GAMEMODE(GAMEMODE_FM)
//		TRANSITION_CHANGE_STATE(TRANSITION_STATE_PRE_HUD_CHECKS) 
//        HUD_CHANGE_STATE(HUD_STATE_PRE_HUD_CHECKS) 
//		STAT_CLEAR_DIRTY_READ_DETECTED()
//	ENDIF

ENDPROC


PROC PROCESS_MPHUD_DIRTY_LOAD_CONFIRM(MPHUD_PLACEMENT_TOOLS& Placement)
	
	// initialise data
	IF NOT (Placement.bHudScreenInitialised)
		IF IS_PAUSE_MENU_ACTIVE()
			SET_PAUSE_MENU_ACTIVE(FALSE)
//			TOGGLE_RENDERPHASES(TRUE)	//Need this when signing out in a corona
//			TOGGLE_RENDERPHASES(TRUE)	//Need this when signing out in a corona
			SET_SKYFREEZE_CLEAR()
		ENDIF
		
		SET_BAIL_STILL_VALID(FALSE) //Need this when signing out in a corona, it would bring this screen up twice. It would think the bail was still valid in cleanup
		RUN_MP_DISCONNECTION_CLEANUP(Placement)
		
		SET_CURRENT_GAMEMODE(GAMEMODE_EMPTY)
		SET_JOINING_GAMEMODE(GAMEMODE_EMPTY)
		Placement.bHudScreenInitialised = TRUE
	ENDIF

	RUN_MP_DISCONNECTION_CLEANUP_EVERY_FRAME()

	
	RENDER_MPHUD_DIRTY_LOAD_CONFIRM(Placement)
	
// logic	
	LOGIC_MPHUD_DIRTY_LOAD_CONFIRM()
	
			
ENDPROC

