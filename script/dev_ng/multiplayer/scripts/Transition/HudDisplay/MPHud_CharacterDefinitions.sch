/* ------------------------------------------------------------------
* Name: 		MPHud_CharacterDefinitions.sch
* Author: 		James Adwick
* Date: 		02/09/2014
* Purpose: 		Header containing structs / constants and enums for character
*				select and creation
* ------------------------------------------------------------------*/

//--- Includes

USING "globals.sch"
USING "screens_header.sch"

//--- CONSTS



CONST_INT ciCHARACTER_CREATOR_COLUMN_MAIN_MENU				0
CONST_INT ciCHARACTER_CREATOR_COLUMN_HERITAGE				1
CONST_INT ciCHARACTER_CREATOR_COLUMN_FEATURES				2
CONST_INT ciCHARACTER_CREATOR_COLUMN_STATS					3
CONST_INT ciCHARACTER_CREATOR_COLUMN_APPEARANCE				4
CONST_INT ciCHARACTER_CREATOR_COLUMN_APPAREL				5
CONST_INT ciCHARACTER_CREATOR_COLUMN_MAX					6

// Selector card
CONST_INT ciCHARACTER_SELECTOR_COLUMN_STAT_CARD				5

// Number of normal and special peds required for creator
CONST_INT ciCHARACTER_CREATOR_NUM_SPECIAL_PEDS				4

// Number of heads for male and female
CONST_INT ciCHARACTER_CREATOR_PARENTS_TOTAL_HEADS			40

// Safety timer for head blend processing
CONST_INT ciCHARACTER_CREATOR_SAFETY_HEAD_BLEND_TIMER		10000

// Indexes for parent creator data
CONST_INT ciCHARACTER_CREATOR_MUM_INDEX						0
CONST_INT ciCHARACTER_CREATOR_DAD_INDEX						1

CONST_INT ciCHARACTER_CREATOR_SON_INDEX						0
CONST_INT ciCHARACTER_CREATOR_DAUGHTER_INDEX				1

// Dominance ranges
TWEAK_FLOAT cfCHARACTER_CREATOR_MALE_DOM_MIN				0.30
TWEAK_FLOAT cfCHARACTER_CREATOR_FEMALE_DOM_MIN				0.05
TWEAK_FLOAT cfCHARACTER_CREATOR_MALE_DOM_MAX				0.95
TWEAK_FLOAT cfCHARACTER_CREATOR_FEMALE_DOM_MAX				0.70

// Skin Tone ranges
TWEAK_FLOAT cfCHARACTER_CREATOR_TONE_MIN				0.0
TWEAK_FLOAT cfCHARACTER_CREATOR_TONE_MAX				1.0

// UI Slider values to scroll across
CONST_FLOAT cfCHARACTER_CREATOR_MIN_SLIDER				0.0
CONST_FLOAT cfCHARACTER_CREATOR_MAX_SLIDER				100.0
CONST_FLOAT cfCHARACTER_CREATOR_SLIDER_MULTIPLIER		45.0

TWEAK_FLOAT cfCHARACTER_CREATOR_PED_X		399.9
TWEAK_FLOAT cfCHARACTER_CREATOR_PED_Y		-998.7
TWEAK_FLOAT cfCHARACTER_CREATOR_PED_Z		-100.000
TWEAK_FLOAT cfCHARACTER_CREATOR_PED_ROT		176.698

// Position of animation
TWEAK_FLOAT cfCHARACTER_SELECTOR_ANIM_X		409.020
TWEAK_FLOAT cfCHARACTER_SELECTOR_ANIM_Y		-1000.800 //-1001.700 //-1003.013
TWEAK_FLOAT cfCHARACTER_SELECTOR_ANIM_Z		-98.859 //-99.0041

TWEAK_FLOAT cfCHARACTER_SELECTOR_ANIM_ROT_X		0.0
TWEAK_FLOAT cfCHARACTER_SELECTOR_ANIM_ROT_Y		0.0
TWEAK_FLOAT cfCHARACTER_SELECTOR_ANIM_ROT_Z		0.0

// Position of creator animation 
TWEAK_FLOAT cfCHARACTER_CREATOR_ANIM_X		404.834
TWEAK_FLOAT cfCHARACTER_CREATOR_ANIM_Y		-997.838 //-1003.013
TWEAK_FLOAT cfCHARACTER_CREATOR_ANIM_Z		-98.841

TWEAK_FLOAT cfCHARACTER_CREATOR_ANIM_ROT_X		0.0
TWEAK_FLOAT cfCHARACTER_CREATOR_ANIM_ROT_Y		0.0
TWEAK_FLOAT cfCHARACTER_CREATOR_ANIM_ROT_Z		-40.000

CONST_FLOAT cfCHARACTER_CREATOR_BASE_DETAIL_M_MIN	90.0
CONST_FLOAT cfCHARACTER_CREATOR_BASE_DETAIL_M_MAX	100.0
CONST_FLOAT cfCHARACTER_CREATOR_BASE_DETAIL_F_MIN	80.0
CONST_FLOAT cfCHARACTER_CREATOR_BASE_DETAIL_F_MAX	90.0

// Age ranges
CONST_INT CHARACTER_CREATOR_MIN_AGE				21
CONST_INT CHARACTER_CREATOR_MAX_AGE				55
CONST_INT CHARACTER_CREATOR_DEFAULT_AGE			25
CONST_INT CHARACTER_CREATOR_AGE_FOR_WRINKLES	30

// Amount of points to assign
CONST_INT CHARACTER_CREATOR_MAX_POINTS		100

// Style selection
#IF NOT FEATURE_GEN9_STANDALONE
CONST_INT ciCHARACTER_CREATOR_MAX_STYLES	8
#ENDIF
#IF FEATURE_GEN9_STANDALONE
CONST_INT ciCHARACTER_CREATOR_MAX_STYLES		12
#ENDIF

CONST_INT ciCHARACTER_CREATOR_MAX_EYE_AVAIL 8

// Look at constants
CONST_INT ciCHARACTER_CREATOR_MIN_LOOK_AROUND_TIME		7000
CONST_INT ciCHARACTER_CREATOR_MAX_LOOK_AROUND_TIME		10000

// Hair
CONST_INT ciCHARACTER_CREATOR_MAX_NUMBER_OF_HAIR			22
CONST_INT ciCHARACTER_CREATOR_MAX_NUMBER_OF_HAIR_COLOURS	5

// Menu States
// -- Top Menu
CONST_INT ciCHARACTER_CREATOR_MAIN_MENU_GENDER		0
CONST_INT ciCHARACTER_CREATOR_MAIN_MENU_HERITAGE	1
CONST_INT ciCHARACTER_CREATOR_MAIN_MENU_FEATURES	2
CONST_INT ciCHARACTER_CREATOR_MAIN_MENU_APPEARANCE	3
CONST_INT ciCHARACTER_CREATOR_MAIN_MENU_APPAREL		4
CONST_INT ciCHARACTER_CREATOR_MAIN_MENU_STATS		5
CONST_INT ciCHARACTER_CREATOR_MAIN_MENU_CONTINUE	6

// -- Heritage men
CONST_INT ciCHARACTER_CREATOR_HERITAGE_MUM_AND_DAD_WINDOW	0
CONST_INT ciCHARACTER_CREATOR_HERITAGE_MUM_NAME				1
CONST_INT ciCHARACTER_CREATOR_HERITAGE_DAD_NAME				2
CONST_INT ciCHARACTER_CREATOR_HERITAGE_DOM					3
CONST_INT ciCHARACTER_CREATOR_HERITAGE_SKIN_TONE			4

// -- Apparel Menu
CONST_INT ciCHARACTER_CREATOR_APPAREL_STYLE				0
CONST_INT ciCHARACTER_CREATOR_APPAREL_OUTFIT			1
CONST_INT ciCHARACTER_CREATOR_APPAREL_HAT				2
CONST_INT ciCHARACTER_CREATOR_APPAREL_GLASSES			3
CONST_INT ciCHARACTER_CREATOR_APPAREL_CREW_TSHIRT		4
CONST_INT ciCHARACTER_CREATOR_APPAREL_CREW_STYLE		5
CONST_INT ciCHARACTER_CREATOR_APPAREL_TOTAL_ROWS		6

// -- Features Menu
CONST_INT ciCHARACTER_CREATOR_FEATURES_BROW				0
CONST_INT ciCHARACTER_CREATOR_FEATURES_EYES				1
CONST_INT ciCHARACTER_CREATOR_FEATURES_NOSE				2
CONST_INT ciCHARACTER_CREATOR_FEATURES_NOSE_PROFILE		3
CONST_INT ciCHARACTER_CREATOR_FEATURES_NOSE_TIP			4
CONST_INT ciCHARACTER_CREATOR_FEATURES_CHEEK			5
CONST_INT ciCHARACTER_CREATOR_FEATURES_CHEEK_SHAPE		6
CONST_INT ciCHARACTER_CREATOR_FEATURES_LIPS				7
CONST_INT ciCHARACTER_CREATOR_FEATURES_JAW				8
CONST_INT ciCHARACTER_CREATOR_FEATURES_CHIN				9
CONST_INT ciCHARACTER_CREATOR_FEATURES_CHIN_SHAPE		10
CONST_INT ciCHARACTER_CREATOR_FEATURES_NECK_F			11
CONST_INT ciCHARACTER_CREATOR_FEATURES_NECK_M			12

CONST_INT ciCHARACTER_CREATOR_MAX_FEATURES				13

// -- Appearance Menu
CONST_INT ciCHARACTER_CREATOR_APPEARANCE_HAIR_STYLE_F		0
CONST_INT ciCHARACTER_CREATOR_APPEARANCE_HAIR_STYLE_M		1
CONST_INT ciCHARACTER_CREATOR_APPEARANCE_EYEBROWS_F			2
CONST_INT ciCHARACTER_CREATOR_APPEARANCE_EYEBROWS_M			3
CONST_INT ciCHARACTER_CREATOR_APPEARANCE_FACIAL_HAIR		4
CONST_INT ciCHARACTER_CREATOR_APPEARANCE_SKIN_BLEMISHES		5
CONST_INT ciCHARACTER_CREATOR_APPEARANCE_SKIN_AGING			6
CONST_INT ciCHARACTER_CREATOR_APPEARANCE_SKIN_COMPLEXION	7
CONST_INT ciCHARACTER_CREATOR_APPEARANCE_MOLE_FRECKLES		8
CONST_INT ciCHARACTER_CREATOR_APPEARANCE_SUN_DAMAGE			9
CONST_INT ciCHARACTER_CREATOR_APPEARANCE_EYES				10
CONST_INT ciCHARACTER_CREATOR_APPEARANCE_EYE_MAKEUP			11
CONST_INT ciCHARACTER_CREATOR_APPEARANCE_BLUSHER			12
CONST_INT ciCHARACTER_CREATOR_APPEARANCE_LIPSTICK			13
CONST_INT ciCHARACTER_CREATOR_APPEARANCE_EYE_MAKEUP_M		14
CONST_INT ciCHARACTER_CREATOR_APPEARANCE_LIPSTICK_M			15
CONST_INT ciCHARACTER_CREATOR_APPEARANCE_BODY_OVERLAY_2_F	16
CONST_INT ciCHARACTER_CREATOR_APPEARANCE_BODY_OVERLAY_3_F	17
CONST_INT ciCHARACTER_CREATOR_APPEARANCE_BODY_OVERLAY_1_M	18
CONST_INT ciCHARACTER_CREATOR_APPEARANCE_BODY_OVERLAY_2_M	19
CONST_INT ciCHARACTER_CREATOR_APPEARANCE_BODY_OVERLAY_3_M	20

CONST_INT ciCHARACTER_CREATOR_MAX_APPEARANCE				21


// Additional columns in the scaleform that we bring in and out depending on selection
CONST_INT ciCHARACTER_CREATOR_FEATURE_COLUMN				3	
CONST_INT ciCHARACTER_CREATOR_APPEARANCE_DETAILS_COLUMN		7

CONST_INT ciCHARACTER_CREATOR_EXISTING_INDEX			-1
CONST_INT ciCHARACTER_CREATOR_EXISTING_OVERLAY_INDEX	-2
CONST_INT ciCHARACTER_CREATOR_UNSIGNED_INT_CHECK	255

CONST_INT ciCHARACTER_CREATOR_MENU_ROW_TYPE		4

// Prop_Police_ID_Text
CONST_INT ciCHARACTER_SELECTOR_TOTAL_BOARDS		2

CONST_INT ciCHARACTER_CREATOR_MAX_HATS_F		45
CONST_INT ciCHARACTER_CREATOR_MAX_HATS_M		45

CONST_INT ciCHARACTER_CREATOR_MAX_GLASSES_F		48
CONST_INT ciCHARACTER_CREATOR_MAX_GLASSES_M		48

//--- ENUMS

// Index of our standard peds
ENUM CHARACTER_CREATOR_PED
	CHARACTER_CREATOR_SON,
	CHARACTER_CREATOR_DAUGHTER,
	CHARACTER_CREATOR_MAX_PEDS,
	CHARACTER_CREATOR_MUM,
	CHARACTER_CREATOR_DAD
ENDENUM

// Stats displayed on the character creator
ENUM CHARACTER_CREATOR_STATS
	CHARACTER_CREATOR_STAT_STAMINA,
	CHARACTER_CREATOR_STAT_SHOOTING,
	CHARACTER_CREATOR_STAT_STRENGTH,
	CHARACTER_CREATOR_STAT_STEALTH,
	CHARACTER_CREATOR_STAT_FLYING,
	CHARACTER_CREATOR_STAT_DRIVING,
	CHARACTER_CREATOR_STAT_LUNG_CAPACITY,
	CHARACTER_CREATOR_MAX_STATS
ENDENUM

// Various states of the tshirt decal
ENUM CHARACTER_CREATOR_CREW_TSHIRT_DECAL
	CC_CREW_DECAL_NONE = 0,
	CC_CREW_DECAL_BIG,
	CC_CREW_DECAL_SMALL,
	CC_CREW_DECAL_BACK
ENDENUM



ENUM CHARACTER_CREATOR_FEATURE_GENDER
	CHARACTER_CREATOR_GENDER_BOTH = 0,
	CHARACTER_CREATOR_GENDER_FEMALE,
	CHARACTER_CREATOR_GENDER_MALE,
	CHARACTER_CREATOR_GENDER_FEMALE_HIDE,
	CHARACTER_CREATOR_GENDER_MALE_HIDE
ENDENUM

// Enum for various camera modes we can be in
ENUM CHARACTER_CONTROLLER_CAMERA_MODE
	CHARACTER_CONTROLLER_CAMERA_SELECTOR_START,
	CHARACTER_CONTROLLER_CAMERA_SELECTOR_OVERVIEW,
	CHARACTER_CONTROLLER_CAMERA_SELECTOR_END,
	CHARACTER_CONTROLLER_CAMERA_CREATOR_START,
	CHARACTER_CONTROLLER_CAMERA_CREATOR_OVERVIEW,
	CHARACTER_CONTROLLER_CAMERA_CREATOR_MUGSHOT_ZOOMED,
	CHARACTER_CONTROLLER_CAMERA_CREATOR_MUGSHOT_ZOOMED_CENTRED,
	CHARACTER_CONTROLLER_CAMERA_PHOTO
ENDENUM

// The time of day in the creator
ENUM CHARACTER_CONTROLLER_TIME_OF_DAY
	CHARACTER_CONTROLLER_TIME_OF_DAY_DEFAULT,
	CHARACTER_CONTROLLER_TIME_OF_DAY_NOON,
	CHARACTER_CONTROLLER_TIME_OF_DAY_MIDNIGHT
ENDENUM


//--- STRUCTS

// Struct that is populated with stat info for creator
STRUCT CHARACTER_CREATOR_STAT_DATA
	TEXT_LABEL_15 tlStatTitle
	INT iMinValue
	INT iMaxValue
ENDSTRUCT

// Struct containing essential data for a feature
STRUCT CHARACTER_CREATOR_FEATURE_DATA
	TEXT_LABEL_15 		tlFeatureName			// Name of feature
	BOOL 				bXAxisActive			// If X axis slider is active
	BOOL 				bYAxisActive			// If Y axis slider is active
	TEXT_LABEL_15 		tlUpText				// Text for top of grid
	TEXT_LABEL_15 		tlRightText				// Text for right of grid
	TEXT_LABEL_15 		tlDownText				// Text for bottom of grid
	TEXT_LABEL_15 		tlLeftText				// Text for left of grid
	INT 				iNumOfPresets			// Max number of options the player can get
	
	MICRO_MORPH_TYPE	morphYAxis				// Which morph slot should be used for y movement
	MICRO_MORPH_TYPE	morphXAxis				// Which morph slot should be used for x movement
	
	FLOAT				fYMax					// The value if we move to top of grid
	FLOAT				fYMin					// The value if we move to bottom of grid
	FLOAT				fXMax					// The value if we move to the right of grid
	FLOAT				fXMin					// The value if we move to the left of grid
	
	CHARACTER_CREATOR_FEATURE_GENDER iGenderSpecific			// 0 - both, 1 - female, 2 - male,
	
	BOOL				bIsValid				// If this feature is valid
ENDSTRUCT

// Struct containing essential data for an appearance
STRUCT CHARACTER_CREATOR_APPEARANCE_DATA
	TEXT_LABEL_15 		tlAppearanceName				// Name of appearance
	BOOL				bOverlayActive
	HEAD_OVERLAY_SLOT	overlaySlot						// Which overlay slot to use
	
	BOOL				bOpacityActive					// Is the opacity active
	BOOL				bColourActive					// Is the colour active
	
	RAMP_TYPE			colourPalette					// RAMP_TYPE, which subset (palette) of colours we use
	
	INT					iDefaultValue					// Default value for the appearance
	FLOAT				fDefaultOpacity					// Default opacity value
	INT					iDefaultColour					// Default colour value
	
	INT					iValueMin						// Min value this appearance can have
	INT					iValueMax						// Max value this appearance can have
	FLOAT				fOpacityMin						// Min opacity that can be chosen
	FLOAT				fOpacityMax						// Max opacity that can be chosen
	INT					iColourMin						// Min colour value
	INT					iColourMax						// Max colour value
	
	CHARACTER_CREATOR_FEATURE_GENDER iGenderSpecific	// 0 - both, 1 - female, 2 - male,
	
	BOOL				bIsValid						// If this apperance is valid
ENDSTRUCT

// Struct containing details about the features we have chosen
STRUCT CHARACTER_CREATOR_FEATURE_VALUES
	INT 	iPreset
	FLOAT	fXBlend
	FLOAT 	fYBlend
ENDSTRUCT

// Struct containing details about the appearance we have chosen
STRUCT CHARACTER_CREATOR_APPEARANCE_VALUES
	INT 	iValue
	FLOAT	fOpacity
	INT		iColour
	INT		iColour2
	INT		iColourHighlightValue
ENDSTRUCT

// Struct containing data about presets
STRUCT CHARACTER_CREATOR_FEATURE_PRESET_DATA
	FLOAT			fXBlend
	FLOAT			fYBlend
	TEXT_LABEL_15	tlPresetName
ENDSTRUCT

STRUCT CHARACTER_CREATOR_PARENT_HEAD_DATA
	INT iHead
	INT iHair
	INT iHairColour
ENDSTRUCT

// Sub struct that contains key parent details
STRUCT MPHUD_PLAYER_PARENT_DETAILS
	
	INT iTotalHeadsAvailable
	CHARACTER_CREATOR_PARENT_HEAD_DATA sHeadSelection[ciCHARACTER_CREATOR_PARENTS_TOTAL_HEADS]		// List of randomly selected heads to loop over
	
	INT iCurrentlySelectedHead										// Currently selected head
	
ENDSTRUCT

// Struct containing the details for the child peds which are shared
STRUCT MPHUD_CHARACTER_CHILD_SHARED_DATA	
	INT iStatValuesLeftToAssign = CHARACTER_CREATOR_MAX_POINTS			// Pot left to assign to stats
	INT iPlayerStats[CHARACTER_CREATOR_MAX_STATS]						// The stats of the player
	
	CHAR_CREATOR_CREW_TSHIRT_OPTION_ENUM crewTShirtStyle				// Option for crew tshirt
	CHARACTER_CREATOR_CREW_TSHIRT_DECAL crewTShirtDecal					// If the decal should be displayed
	
	FLOAT fCharacterXTurnVal
	FLOAT fCharacterYTurnVal
	
	CHARACTER_CREATOR_FEATURE_VALUES featureValues[ciCHARACTER_CREATOR_MAX_FEATURES]		// List of your values for features
	CHARACTER_CREATOR_APPEARANCE_VALUES	appearanceValues[ciCHARACTER_CREATOR_MAX_APPEARANCE]	// List of your values for appearance
ENDSTRUCT

// Struct containing the details for the child peds which are unique
STRUCT MPHUD_CHARACTER_CHILD_DATA
	FLOAT									fParentDominance = 50.0													// Blend of geometry between parents
	FLOAT									fSkinToneBlend = 50.0													// Blend of skin tone between parents

	INT iSelectedStyle						// The style selected for ped
	INT iSelectedOutfit						// The selected outfit for the ped (probably array?)

	INT iHat			// Store of hats for styles
	INT iGlasses		// Store of glasses for styles
ENDSTRUCT

// Board prop
STRUCT CHARACTER_CONTROLLER_BOARD
	OBJECT_INDEX objBoard
	OBJECT_INDEX objRenderBoard
	SCALEFORM_INDEX scaleformIndex
	INT iRenderTarget
ENDSTRUCT

// Main struct containing all relevant data to character select and creation for GTA Online
STRUCT MPHUD_GTA_ONLINE_CHARACTER_DATA

	INT										iSelectorSlot															// The slot that is chosen in the selector

	// Character Controller States
	// These have been moved to the global struct: CHARACTER_CONTROLLER_GLOBALS

	INT										iActiveSlotOnSelector													// Bitset of active slots
	INT										iNewCharacterAtSlot = -1												// The slot that the new ped will be in
	
	INT										selectorOutroTimer														// Timer before we cut to creator
	
	VECTOR									vPedPositions[2]														// Store the position of the peds on this screen
	VECTOR									vPedRotations[2]														// Store the rotations of the peds on this screen
	
	CHARACTER_CONTROLLER_BOARD				sPedBoard[2]															// Board for each of the peds
	
	CHARACTER_CONTROLLER_TIME_OF_DAY		eCharacterControllerTOD													// TIme of day of our creator
		
	INT										iCharacterCreatorCurrentScreen											// Which screen of the creator are we on
	INT										iCharacterCreatorCameraZoomTimer										// Handle the zoom effects
	FLOAT									fCharacterControllerCameraDOF											// The DOF we are currently at on the camera
	INT										iCharacterCreatorPhotoTimer												// Timer for the photo taking in the creator
	INT										iCharacterSelectorCameraWaitTimer										// Timer for the camera at the start of selector
	INT										iCharacterCreatorClothesTimer											// Timer to delay playing

	STRING									strLastPlayedClothingAnim

	INT										currentSelection[ciCHARACTER_CREATOR_COLUMN_MAX]						// Current select on each column
	
	//Ped Info
	PED_INDEX								characterCreatorPeds[CHARACTER_CREATOR_MAX_PEDS]						// Array of our standard peds

	// Parent data
	MPHUD_PLAYER_PARENT_DETAILS				parentData[2]															// Data for each of the parents

	// Child shared data
	MPHUD_CHARACTER_CHILD_SHARED_DATA		eCharacterSharedData													// Data shared for both child peds
	MPHUD_CHARACTER_CHILD_DATA				eCharacterUniqueData[2]													// Data unique to each individual child

	// Camera work
	CHARACTER_CONTROLLER_CAMERA_MODE		eCharacterControllerCameraMode = CHARACTER_CONTROLLER_CAMERA_SELECTOR_START	// The mode the camera is in
	CAMERA_INDEX							ciCharacterControllerInterpCam											// Camera we use during interps
	CAMERA_INDEX							ciCharacterControllerCamera												// Camera that is used throughout 

	// Global values
	CHARACTER_CREATOR_PED					eActiveChild															// This the the active child we have chosen

	INT 									iCharacterSelectedSlot													// The slot the player has selected
	INT										iCharacterCreatorPedToCreate											// Staggered loop to create peds
	
	BOOL									bForcingLookAround
	INT 									iCharacterNextLookAtTime
	INT										iCharacterControllerTimer												// Timer used primarily for scrolling options
	
	INT										iSafetyTimer															// Timer that is used to timeout throughout
	
	INT										iCrewEmblemState														// State we are in retrieving the crew emblem
	INT										iCrewID
	TEXT_LABEL_63							tlCrewEmblem
	TEXT_LABEL_15							tlCrewTag
	
	// Name entering variables
	OSK_STATUS 								keyboardStatus
	INT										iKeyboardStage
	INT										iProfanityCheck
	INT										iNumOfAttempts
	
	INT										iSoundID						= -1									// For character menu sounds	
	INT 									iHatIndex 						= -1									// Ensures conflicting hair/hat combo are not shown
	FLOAT									fTempAxisPos															// For analogue stick feature grid sound limits
	
	FLOAT 									fEyebrowOpacity															// Gets eyebrow opacity from selector menu to compare with creator
	
	// Essentially a constant
	INT										iCharacterMenuID														// Default ID to populate scaleform columns
	
	// Interior data
	INTERIOR_INSTANCE_INDEX					interiorIndex															// Interior index

	SCALEFORM_INDEX							sfCameraOverlay															// Scaleform for the camera screenshot
	
	// Hair limits
	
	INT										iMaxHairColours															// The max hair colours available to the ped.
	INT										iMaxBlusherColours
	INT										iMaxLipstickColours
	
	// Audio Banks
	BOOL 									bAudioBanksRequested = FALSE											// Query if audio banks have been requested
	
	#IF IS_DEBUG_BUILD
		BOOL 		bDebugChangePositionAndCamera
		BOOL		bRestartWalkOnAnimation
		BOOL		bRestartWalkInCreatorAnim
		BOOL		bRestartSelectorCameraAnim
		BOOL		bCreatorOverviewCam
		BOOL		bCreatorCameraAnimToMugshot
		BOOL		bCreatorCameraAnimToOverview
		BOOL		bCreatorCameraAnimFromStart
		BOOL		bSelectorRestartIntro
		BOOL		bCleanupPhoto
		BOOL		bSuppressMoveToGameFromPhoto
		BOOL		bDebugLastGenCharacter
		BOOL		bDebugPhotoBorder
		BOOL		bDebugDrawParentHeadValues
		BOOL		bDisplayValentinesHair
		BOOL		bDebugEyeZero
		BOOL		bDebugEyeInvalid
	#ENDIF
ENDSTRUCT

