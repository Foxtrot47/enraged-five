
USING "screens_header.sch"
USING "net_events.sch"
USING "script_network.sch"

USING "Transition_Common.sch"

CONST_INT MAX_NUMBER_CONFIRM_LEAVE_SELECTIONS 2

PROC INIT_MPHUD_CONFIRM_LEAVE(MPHUD_PLACEMENT_TOOLS& Placement)

	RESET_ALL_SPRITE_PLACEMENT_VALUES(Placement.SpritePlacement)


	Placement.TextPlacement[0].x = 0.5
	Placement.TextPlacement[0].y = 0.5

	Placement.TextPlacement[1].x = 0.500
	Placement.TextPlacement[1].y = 0.178
	
	Placement.TextPlacement[2].x = 0.500
	Placement.TextPlacement[2].y = 0.209
	
	//Top bar
	Placement.SpritePlacement[0].x = 0.500
	Placement.SpritePlacement[0].y = 0.190
	Placement.SpritePlacement[0].w = 0.675
	Placement.SpritePlacement[0].h = 0.045
	Placement.SpritePlacement[0].r = 255
	Placement.SpritePlacement[0].g = 255
	Placement.SpritePlacement[0].b = 255
	Placement.SpritePlacement[0].a = 200
	
	//Bottom Bar 
	Placement.SpritePlacement[1].x = 0.5
	Placement.SpritePlacement[1].y = 0.222
	Placement.SpritePlacement[1].w = 0.675
	Placement.SpritePlacement[1].h = 0.045
	Placement.SpritePlacement[1].r = 255
	Placement.SpritePlacement[1].g = 255
	Placement.SpritePlacement[1].b = 255
	Placement.SpritePlacement[1].a = 200
	
	
	//Scaleform movie 
	Placement.SpritePlacement[2].x = 0.390
	Placement.SpritePlacement[2].y = 0.467
	Placement.SpritePlacement[2].w = 1
	Placement.SpritePlacement[2].h = 1
	Placement.SpritePlacement[2].r = 255
	Placement.SpritePlacement[2].g = 255
	Placement.SpritePlacement[2].b = 255
	Placement.SpritePlacement[2].a = 200
	
ENDPROC

PROC RENDER_MPHUD_CONFIRM_LEAVE(MPHUD_PLACEMENT_TOOLS& Placement)

	// 'SELECT MP CHARACTER...'
//	DRAW_TEXT(Placement.MainTitle, Placement.aStyle.TS_TITLEHUD, "HUD_LEAVETIT")	
		
	// main background
//	DRAW_JOIN_HUD_BACKGROUND()
	
	
	
		
	VECTOR SlotOffset[5]
	VECTOR SlotHeading[5]
	

	SET_PED_SLOT_OFFSETS(SlotOffset, SlotHeading)
	
	
	VECTOR PlaceGuy, HeadGuy
	VECTOR CamRot
	VECTOR CamCoord
	IF DOES_CAM_EXIST(GET_RENDERING_CAM())
		CamRot = GET_CAM_ROT(GET_RENDERING_CAM())
		CamCoord = GET_CAM_COORD(GET_RENDERING_CAM())
	ENDIF
	
	
	INT I
	FOR I = 0 TO MAX_NUM_CHARACTER_SLOTS-1
	
		PlaceGuy = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(CamCoord,CamRot.z, SlotOffset[I])
		HeadGuy = CamRot+SlotHeading[I] 
	
		IF IS_STAT_CHARACTER_ACTIVE(I)
			RUN_PED_MENU(Placement.IdlePed[I], GET_PLAYER_MODEL_FOR_TEAM_VARIATION_FOR_MENU( GET_STAT_CHARACTER_TEAM(I), GET_PACKED_STAT_INT(PACKED_CHAR_PICTURE, I)), PlaceGuy, HeadGuy, FALSE,IS_CHARACTER_MALE(I), I)	
		ENDIF
		
	ENDFOR
	
	SET_ALL_MENUPED_VISIBLE(Placement.SelectionPed, Placement.IdlePed, FALSE)
	
	
	

	STRING sStatementText = "HUD_LEAVETIT"
	STRING sSubText = "HUD_UNSAVE"
	STRING sQuestionText = "HUD_QUITSESS"


	
	SWITCH GET_CURRENT_GAMEMODE()

		CASE GAMEMODE_FM
			IF IS_ON_DEATHMATCH_GLOBAL_SET()
				sQuestionText = "HUD_QUITDM"
			ElIF IS_ON_RACE_GLOBAL_SET()
				sQuestionText = "HUD_QUITRACE"
			ELIF IS_PLAYER_ON_ANY_MP_MISSION(PLAYER_ID())
				sQuestionText = "HUD_QUITMISS"
			ENDIF
			
		BREAK
	ENDSWITCH

	
	SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sQuestionText, FE_WARNING_YESNO, sSubText)




ENDPROC

PROC LOGIC_MPHUD_CONFIRM_LEAVE(MPHUD_PLACEMENT_TOOLS& Placement)
	
	BOOL QuickLaunchIntoTheGame

	IF HAS_QUIT_CURRENT_GAME()
		Placement.ScreenPlace.iSelection = 0 // yes
		QuickLaunchIntoTheGame = TRUE
	ENDIF

	#IF IS_DEBUG_BUILD
	IF QuickLaunchIntoTheGame = FALSE
		SWITCH GET_QUICKLAUNCH_STATE() 
			CASE QUICKLAUNCH_SP
				Placement.ScreenPlace.iSelection = 0 // yes
				QuickLaunchIntoTheGame = TRUE
				SET_QUICKLAUNCH_STATE(QUICKLAUNCH_EMPTY) //Stop the selector kicking up the transition script again. 
			BREAK
		ENDSWITCH
	ENDIF
	#ENDIF
	
	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_CANCEL)
	
	
		IF IS_PAUSE_MENU_REQUESTING_TRANSITION()
				
			PRINTLN("[JA@PAUSEMENU] LOGIC_MPHUD_CONFIRM_LEAVE - not leaving so swoop back down")
		
			HUD_CHANGE_STATE(HUD_STATE_NO_DISPLAY)
			TRANSITION_CHANGE_STATE(TRANSITION_STATE_MP_SWOOP_DOWN)
			RESET_PAUSE_MENU_WARP_REQUESTS()
		ELSE
		
			NET_NL()NET_PRINT("LOGIC_MPHUD_CONFIRM_LEAVE: INPUT_FRONTEND_CANCEL")

		
			HUD_CHANGE_STATE(Placement.PreviousScreen)	
			SET_JOINING_GAMEMODE(GAMEMODE_EMPTY)
			SET_CURRENT_TRANSITION_MENU_CHOICE(MENU_CHOICE_EMPTY)
			SET_CURRENT_TRANSITION_DM_MENU_CHOICE(DM_MENU_CHOICE_EMPTY)
			SET_CURRENT_TRANSITION_FM_MENU_CHOICE(FM_MENU_CHOICE_EMPTY)
		ENDIF
	ENDIF
	
	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
	OR QuickLaunchIntoTheGame
	OR HAS_QUIT_CURRENT_GAME() = TRUE
	OR IS_PAUSE_MENU_REQUESTING_TRANSITION()
	
		NET_NL()NET_PRINT("LOGIC_MPHUD_CONFIRM_LEAVE: INPUT_FRONTEND_ACCEPT")
		
		TOGGLE_RENDERPHASES(FALSE)

		
		#IF IS_DEBUG_BUILD
		IF GET_JOINING_GAMEMODE() = GAMEMODE_FM
			NET_NL()NET_PRINT("GET_JOINING_GAMEMODE() = GAMEMODE_FM")
		ENDIF

		IF GET_JOINING_GAMEMODE() = GAMEMODE_SP
			NET_NL()NET_PRINT("GET_JOINING_GAMEMODE() = GAMEMODE_SP")
		ENDIF
		IF GET_CURRENT_GAMEMODE() = GAMEMODE_FM
			NET_NL()NET_PRINT("GET_CURRENT_GAMEMODE() = GAMEMODE_FM")
		ENDIF
		IF GET_CURRENT_GAMEMODE() = GAMEMODE_SP
			NET_NL()NET_PRINT("GET_CURRENT_GAMEMODE() = GAMEMODE_SP")
		ENDIF
		#ENDIF
	
			SET_QUIT_CURRENT_GAME(TRUE)
			SWITCH GET_JOINING_GAMEMODE() 
				CASE GAMEMODE_FM
					SWITCH GET_CURRENT_GAMEMODE() 
						CASE GAMEMODE_FM
							SWITCH GET_CURRENT_TRANSITION_FM_MENU_CHOICE() //If you're swapping Teams
								CASE FM_MENU_CHOICE_JOIN_NEW_SESSION
									SET_ACTIVE_CHARACTER_SLOT(Placement.ScreenPlace.iSelectedCharacter)
									SET_PLAYERS_SELECTED_CHARACTER_TEAM(GET_STAT_CHARACTER_TEAM(Placement.ScreenPlace.iSelectedCharacter))
									SET_ALL_MENUPED_VISIBLE(Placement.SelectionPed,Placement.IdlePed, FALSE)
									SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)
									SET_JOINING_GAMEMODE(GAMEMODE_FM)
									HUD_CHANGE_STATE(HUD_STATE_LOADING) //HUD_STATE_FM_LOBBY_LOADING)
									TRANSITION_CHANGE_STATE(TRANSITION_STATE_LOOK_TO_JOIN_ANOTHER_SESSION_FM)
								BREAK
//								CASE FM_MENU_CHOICE_NEW_HOST
//									
//									SET_ACTIVE_CHARACTER_SLOT(Placement.ScreenPlace.iSelectedCharacter)
//									SET_PLAYERS_SELECTED_CHARACTER_TEAM(GET_STAT_CHARACTER_TEAM(Placement.ScreenPlace.iSelectedCharacter))
//									SET_ALL_MENUPED_VISIBLE(Placement.SelectionPed,Placement.IdlePed, FALSE)
//									SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)
//									SET_JOINING_GAMEMODE(GAMEMODE_FM)								
//									HUD_CHANGE_STATE(HUD_STATE_LOADING) //HUD_STATE_FM_LOBBY_LOADING)
//									TRANSITION_CHANGE_STATE(TRANSITION_STATE_LOOK_TO_HOST_SESSION_FM)
//								BREAK
								
								CASE FM_MENU_CHOICE_SWAP_TEAM
									HUD_CHANGE_STATE(HUD_STATE_LOADING)	
									TRANSITION_CHANGE_STATE(TRANSITION_STATE_TERMINATE_SESSION)
								BREAK
								
								DEFAULT //If you press Back while in the lobby
									
									SET_JOINING_GAMEMODE(GAMEMODE_EMPTY)
									HUD_CHANGE_STATE(HUD_STATE_LOADING) //HUD_STATE_FM_LOBBY_LOADING)	
									TRANSITION_CHANGE_STATE(TRANSITION_STATE_TERMINATE_SESSION)
									
								
								BREAK
								
							ENDSWITCH
						BREAK
						CASE GAMEMODE_CREATOR
						BREAK
						CASE GAMEMODE_SP
						CASE GAMEMODE_EMPTY
						BREAK
					ENDSWITCH
				BREAK
				CASE GAMEMODE_CREATOR
					SWITCH GET_CURRENT_GAMEMODE() 
						CASE GAMEMODE_FM
							HUD_CHANGE_STATE(HUD_STATE_LOADING)
							SET_JOINING_GAMEMODE(GAMEMODE_CREATOR)
							TRANSITION_CHANGE_STATE(TRANSITION_STATE_TERMINATE_SESSION)
						BREAK
						CASE GAMEMODE_CREATOR
	
						BREAK
						CASE GAMEMODE_SP
						CASE GAMEMODE_EMPTY
	
						BREAK
					ENDSWITCH
				BREAK
				CASE GAMEMODE_SP
					SWITCH GET_CURRENT_GAMEMODE() 
						CASE GAMEMODE_FM							
							REQUEST_TRANSITION(TRANSITION_STATE_TERMINATE_SESSION, GAMEMODE_SP, HUD_STATE_LOADING)
						BREAK
						CASE GAMEMODE_CREATOR							
							REQUEST_TRANSITION(TRANSITION_STATE_TERMINATE_SESSION, GAMEMODE_SP, HUD_STATE_LOADING)
						BREAK
						CASE GAMEMODE_SP
						CASE GAMEMODE_EMPTY
							
						BREAK
					ENDSWITCH
				BREAK
				
				CASE GAMEMODE_EMPTY //Menu choose, confirm before you leave then set joining state.
					SWITCH GET_CURRENT_GAMEMODE() 

						CASE GAMEMODE_FM
							SWITCH GET_CURRENT_TRANSITION_FM_MENU_CHOICE()
			
								CASE FM_MENU_CHOICE_JOIN_NEW_SESSION
//								CASE FM_MENU_CHOICE_NEW_HOST
									SET_JOINING_GAMEMODE(GAMEMODE_FM)
									HUD_CHANGE_STATE(HUD_STATE_LOADING)	
									TRANSITION_CHANGE_STATE(TRANSITION_STATE_TERMINATE_SESSION)
								BREAK
								
								DEFAULT //when you press back in the lobby
									HUD_CHANGE_STATE(HUD_STATE_LOADING)	
									TRANSITION_CHANGE_STATE(TRANSITION_STATE_TERMINATE_SESSION)
								BREAK
								
							ENDSWITCH
						BREAK
						CASE GAMEMODE_CREATOR
						BREAK
						CASE GAMEMODE_SP
						CASE GAMEMODE_EMPTY
						BREAK
					ENDSWITCH
				BREAK	
				
			ENDSWITCH
				
			

//		ENDIF
	
	ENDIF
	
	
//	IF HAS_HUD_CONTROL_BEEN_PRESSED(INPUT_FRONTEND_UP)
//		Placement.ScreenPlace.iSelection += -1	
//		IF (Placement.ScreenPlace.iSelection < 0)
//			Placement.ScreenPlace.iSelection = MAX_NUMBER_CONFIRM_LEAVE_SELECTIONS-1
//		ENDIF
//	ENDIF
//	
//	IF HAS_HUD_CONTROL_BEEN_PRESSED(INPUT_FRONTEND_DOWN)
//		Placement.ScreenPlace.iSelection += 1
//		IF (Placement.ScreenPlace.iSelection > MAX_NUMBER_CONFIRM_LEAVE_SELECTIONS-1)
//			Placement.ScreenPlace.iSelection = 0
//		ENDIF
//	ENDIF
	
		
	
ENDPROC

PROC PROCESS_MPHUD_CONFIRM_LEAVE(MPHUD_PLACEMENT_TOOLS& Placement)
	
	// initialise data
	IF NOT (Placement.bHudScreenInitialised)
		Placement.ScreenPlace.iSelection = 1
		Placement.bHudScreenInitialised = TRUE
	ENDIF
	
	IF HAS_QUIT_CURRENT_GAME() = FALSE
		IF HAS_ALL_COMMON_HUD_ELEMENTS_LOADED(Placement)
			RENDER_MPHUD_CONFIRM_LEAVE(Placement)
		ENDIF
	ENDIF
	
	LOGIC_MPHUD_CONFIRM_LEAVE(Placement)

	

ENDPROC

