
USING "screens_header.sch"
USING "net_events.sch"
USING "script_network.sch"
USING "Transition_Common.sch"

//PROC INIT_MPHUD_LOADING(MPHUD_PLACEMENT_TOOLS& Placement)
//
//	RESET_ALL_SPRITE_PLACEMENT_VALUES(Placement.SpritePlacement)
//
//		
//	Placement.TextPlacement[0].x = 0.500
//	Placement.TextPlacement[0].y = 0.218
//	
//	
//	//Scaleform movie 
//	Placement.SpritePlacement[0].x = 0.390
//	Placement.SpritePlacement[0].y = 0.467
//	Placement.SpritePlacement[0].w = 1
//	Placement.SpritePlacement[0].h = 1
//	Placement.SpritePlacement[0].r = 255
//	Placement.SpritePlacement[0].g = 255
//	Placement.SpritePlacement[0].b = 255
//	Placement.SpritePlacement[0].a = 200
//	
//		//Loading Icon	
//	Placement.SpritePlacement[1].x = 0.387
//	Placement.SpritePlacement[1].y = 0.903
//	Placement.SpritePlacement[1].w = 0.450
//	Placement.SpritePlacement[1].h = 0.041
//	Placement.SpritePlacement[1].r = 255
//	Placement.SpritePlacement[1].g = 255
//	Placement.SpritePlacement[1].b = 255
//	Placement.SpritePlacement[1].a = 200
//	
//	SETTIMERA(0)
//	
//ENDPROC
//
//
//
////WIDGET_GROUP_ID AWidget
////INT ProfileBitset
//
//PROC RENDER_MPHUD_LOADING(MPHUD_PLACEMENT_TOOLS& Placement)
//
//	// 'SELECT MP CHARACTER...'
////	DRAW_TEXT(Placement.MainTitle, Placement.aStyle.TS_TITLEHUD, "HUD_MULT_TIT") // "HUD_LOADLOAD")	
//		
////	// main background
////	BOOL DrawTabs = TRUE
////
////	IF g_TurnOnNewTransitionSystem 
////		DrawTabs = FALSE
////	ENDIF
////		
////	// main background
////	IF DrawTabs = TRUE
////		DRAW_MAIN_HUD_BACKGROUND()
////	ENDIF
//	
//	
//	VECTOR SlotOffset[5]
//	VECTOR SlotHeading[5]
//	
//	
////	SlotOffset[0] = <<-1.352, 3.250, -0.085>>
////	SlotHeading[0] = <<331.200,7.920, 223.920>>
//	
//	SET_PED_SLOT_OFFSETS(SlotOffset, SlotHeading)
//
//
//	
//	VECTOR PlaceGuy, HeadGuy
//	VECTOR CamRot
//	VECTOR CamCoord
//	IF DOES_CAM_EXIST(GET_RENDERING_CAM())
//		CamRot = GET_CAM_ROT(GET_RENDERING_CAM())
//		CamCoord = GET_CAM_COORD(GET_RENDERING_CAM())
//	ENDIF
//	
//	
//	IF GET_CURRENT_TRANSITION_STATE() != TRANSITION_STATE_CONFIRM_FM_SESSION_JOINING //For shifting into CNC from Freemode. Alting will delete character, this was creating it before I was ready
//		IF GET_CURRENT_TRANSITION_STATE() != TRANSITION_STATE_WAIT_HUD_EXIT //For alting into CNC 
//			IF GET_CURRENT_TRANSITION_STATE() !=TRANSITION_STATE_RETURN_TO_SINGLEPLAYER
//				IF GET_CURRENT_TRANSITION_STATE() != TRANSITION_STATE_WAIT_FOR_SINGLEPLAYER_TO_START
//					IF GET_CURRENT_TRANSITION_STATE() != TRANSITION_STATE_TERMINATE_MAINTRANSITION
//						IF GET_CURRENT_TRANSITION_STATE() != TRANSITION_STATE_PRE_HUD_CHECKS
//					
//					
//							
//							
//							INT I
//							FOR I = 0 TO MAX_NUM_CHARACTER_SLOTS-1
//								PlaceGuy = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(CamCoord,CamRot.z, SlotOffset[I])
//								HeadGuy = CamRot+SlotHeading[I] 
//							
//								IF IS_STAT_CHARACTER_ACTIVE(I)
//									
//									RUN_PED_MENU(Placement.IdlePed[I], GET_PLAYER_MODEL_FOR_TEAM_VARIATION_FOR_MENU( GET_STAT_CHARACTER_TEAM(I), GET_PACKED_STAT_INT(PACKED_CHAR_PICTURE, I)), PlaceGuy, HeadGuy, FALSE,IS_CHARACTER_MALE(I), I)	
//								ENDIF
//								
//							ENDFOR
//							
//							SET_ALL_MENUPED_VISIBLE(Placement.SelectionPed,Placement.IdlePed, FALSE)
//
//						ENDIF
//						
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF
//	
//	
////	RUN_PED_MENU(Placement.IdlePed[Placement.ScreenPlace.iSelectedCharacter ], 
////				GET_PLAYER_MODEL_FOR_TEAM_VARIATION_FOR_MENU( GET_STAT_CHARACTER_TEAM(Placement.ScreenPlace.iSelectedCharacter), GET_PACKED_STAT_INT(PACKED_CHAR_PICTURE,  Placement.ScreenPlace.iSelectedCharacter)) 
////				, PlaceGuy, HeadGuy, FALSE,IS_CHARACTER_MALE(Placement.ScreenPlace.iSelectedCharacter), Placement.ScreenPlace.iSelectedCharacter)	
////
//
//	
////	SPRITE_PLACEMENT ScaleformSprite
//
//	CHECK_LOADING_ICON_CHANGE(Placement)
//
////
////	SCALEFORM_TABS_INPUT_DATA TabsData
////	TabsData.MainTitle = GET_LOADING_TITLE()
////	INT PotentialPed = GET_JOINING_CHARACTER()
////	COMPILE_SCALEFORM_TABS(Placement.IdlePed[PotentialPed], TabsData, Placement.ScaleformTabStruct )
////	RUN_SCALEFORM_TABS(Placement.ButtonMovie[GET_SCALEFORM_INDEX_FOR_HUD(HUD_SCALEFORM_TABS)], ScaleformSprite, Placement.ScaleformTabStruct)
//	
//	
//
////	ScaleformSprite = GET_SCALEFORM_LOADING_ICON_POSITION()
//	Placement.ScaleformLoadingStruct.sMainStringSlot = GET_TRANSITION_STRING()
//	RUN_SCALEFORM_LOADING_ICON( Placement.ScaleformLoadingStruct, SHOULD_REFRESH_SCALEFORM_LOADING_ICON(Placement.ScaleformLoadingStruct))
//
//
//
////	ScaleformSprite = GET_SCALEFORM_INSTRUCTIONAL_BUTTON_POSITION()
////	
////	ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL), "HUD_INPUT3", Placement.ScaleformStruct)
////	RUN_SCALEFORM_INSTRUCTIONAL_BUTTONS(Placement.ButtonMovie[GET_SCALEFORM_INDEX_FOR_HUD(HUD_SCALEFORM_INSTRUCTIONAL)], ScaleformSprite, Placement.ScaleformStruct)
//
//	LOAD_UP_LAST_FRAME_DATA(Placement)
//	
//ENDPROC
//
////PROC LOGIC_MPHUD_LOADING(MPHUD_PLACEMENT_TOOLS& Placement)
//
////	IF HAS_HUD_CONTROL_BEEN_PRESSED(INPUT_FRONTEND_CANCEL, Placement.iHudButtonPressed)
////		HUD_CHANGE_STATE(HUD_STATE_NO_DISPLAY)
////		TRANSITION_CHANGE_STATE(TRANSITION_STATE_SP_SWOOP_DOWN)
////	ENDIF
//
////ENDPROC
//
//PROC PROCESS_MPHUD_LOADING(MPHUD_PLACEMENT_TOOLS& Placement)
//	
//	// initialise data
//	IF NOT (Placement.bHudScreenInitialised)
//		UNLOAD_ALL_COMMON_TRANSITION_TEXTURES_AND_SCALEFORM_MOVIES(Placement)
//		INIT_MPHUD_LOADING(Placement)	
//		
//		IF IS_PAUSE_MENU_ACTIVE()
//			SET_FRONTEND_ACTIVE(FALSE)
//			SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
//		ENDIF
//		
//		SET_ALL_MENUPED_VISIBLE(Placement.SelectionPed,Placement.IdlePed, FALSE)
//		REFRESH_SCALEFORM_LOADING_ICON(Placement.ScaleformLoadingStruct)
//		Placement.bHudScreenInitialised = TRUE
//	ENDIF
//
//	IF HAS_ALL_COMMON_HUD_ELEMENTS_LOADED(Placement)					
//		RENDER_MPHUD_LOADING(Placement)
//	ENDIF
//		
////	LOGIC_MPHUD_LOADING(Placement)		
//			
//ENDPROC





















PROC RENDER_XML_MPHUD_LOADING(MPHUD_PLACEMENT_TOOLS& Placement)


	
//	SPRITE_PLACEMENT ScaleformSprite

	CHECK_LOADING_ICON_CHANGE(Placement)
	SET_SCALEFORM_LOADING_ICON_DISPLAY(Placement.ScaleformLoadingStruct, LOADING_ICON_LOADING)

//	ScaleformSprite = GET_SCALEFORM_LOADING_ICON_POSITION()
	Placement.ScaleformLoadingStruct.sMainStringSlot = GET_TRANSITION_STRING()
	RUN_SCALEFORM_LOADING_ICON(Placement.ScaleformLoadingStruct, SHOULD_REFRESH_SCALEFORM_LOADING_ICON(Placement.ScaleformLoadingStruct))

	LOAD_UP_LAST_FRAME_DATA(Placement)

ENDPROC





PROC PROCESS_XML_MPHUD_LOADING(MPHUD_PLACEMENT_TOOLS& Placement)
	
//	IF (IS_PAUSE_MENU_ACTIVE() = FALSE
//	AND Placement.PreviousScreen = HUD_STATE_SELECT_CHARACTER_FM)
//	OR (Placement.PreviousScreen != HUD_STATE_SELECT_CHARACTER_FM)
//	OR g_TurnOnNewTransitionSecondarySystem
	
		IF NOT IS_PAUSE_MENU_RESTARTING()
		AND GET_PAUSE_MENU_STATE() != PM_RESTARTING
		AND GET_PAUSE_MENU_STATE() != PM_SHUTTING_DOWN
		AND GET_PAUSE_MENU_STATE() != PM_STARTING_UP
			// initialise data
			IF NOT (Placement.bHudScreenInitialised)
				PRINTLN("PROCESS_XML_MPHUD_LOADING: GET_PAUSE_MENU_STATE = ", ENUM_TO_INT(GET_PAUSE_MENU_STATE()))
		
				Placement.bDoIHaveControl = TRUE

				IF IS_PAUSE_MENU_ACTIVE()
				AND GET_PAUSE_MENU_STATE() = PM_READY
					IF IS_THIS_SCREEN_ACTIVE(FE_MENU_VERSION_EMPTY_NO_BACKGROUND) = FALSE
						PRINTLN("PROCESS_XML_MPHUD_LOADING: RESTART_FRONTEND_MENU")
						RESTART_FRONTEND_MENU(FE_MENU_VERSION_EMPTY_NO_BACKGROUND)
					ELSE
						NET_NL()NET_PRINT("PROCESS_XML_MPHUD_LOADING: Pause menu already setup")
					ENDIF
				ELSE
					//If it's orbis and we've not got a sub scription and we've going bakck to sp
					IF IS_PLAYSTATION_PLATFORM() AND NETWORK_HAVE_PLATFORM_SUBSCRIPTION() = FALSE
					AND GET_CURRENT_TRANSITION_STATE() = TRANSITION_STATE_RETURN_TO_SINGLEPLAYER
						PRINTLN("PROCESS_XML_MPHUD_LOADING: nothings")
						//Do nothing
					ELSE
						PRINTLN("PROCESS_XML_MPHUD_LOADING: ACTIVATE_FRONTEND_MENU")
						ACTIVATE_FRONTEND_MENU(FE_MENU_VERSION_EMPTY_NO_BACKGROUND, FALSE)
					ENDIF
				ENDIF
				SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_FADE)
				FORCE_SCRIPTED_GFX_WHEN_FRONTEND_ACTIVE(TRUE)			
				
				
				PAUSE_MENU_DEACTIVATE_CONTEXT( HASH("RETURN_TO_MENU"))
				PAUSE_MENU_REDRAW_INSTRUCTIONAL_BUTTONS() 
				
				Placement.bSetupTabs = FALSE
				Placement.bHudScreenInitialised = TRUE
			ENDIF
		ENDIF
//	ENDIF

	IF NEED_TO_DISPLAY_Y_ON_MAIN_TRANSITION()
		IF Placement.aBool[0] = FALSE
			PAUSE_MENU_ACTIVATE_CONTEXT( HASH("ENTER_PLAYLIST"))
			PAUSE_MENU_REDRAW_INSTRUCTIONAL_BUTTONS()
			Placement.aBool[0] = TRUE
		ENDIF
	ELSE
		IF Placement.aBool[0] = TRUE
			PAUSE_MENU_DEACTIVATE_CONTEXT( HASH("ENTER_PLAYLIST"))
			PAUSE_MENU_REDRAW_INSTRUCTIONAL_BUTTONS()
			Placement.aBool[0] = FALSE
		ENDIF
	ENDIF
		

		
	IF (Placement.bHudScreenInitialised)
	AND IS_PAUSE_MENU_ACTIVE()
	AND NOT IS_PAUSE_MENU_RESTARTING()
		IF IS_FRONTEND_READY_FOR_CONTROL()
		
			IF HAS_MENU_TRIGGER_EVENT_OCCURRED()
				NET_NL()NET_PRINT("HAS_MENU_TRIGGER_EVENT_OCCURRED = TRUE")
				GET_MENU_TRIGGER_EVENT_DETAILS(Placement.aNextScreen, Placement.iMenu)
				NET_NL()NET_PRINT("		- aNextScreen = ")NET_PRINT(GET_XML_SCREEN_STRING_VALUE(Placement.aNextScreen))
				NET_NL()NET_PRINT("		- iMenu = ")NET_PRINT_INT(Placement.iMenu)
			ENDIF
			
			IF HAS_MENU_LAYOUT_CHANGED_EVENT_OCCURRED()
 
				NET_NL()NET_PRINT("HAS_MENU_LAYOUT_CHANGED_EVENT_OCCURRED = TRUE")
				GET_MENU_LAYOUT_CHANGED_EVENT_DETAILS(Placement.CurrentScreen, Placement.aNextScreen, Placement.iMenu)
				NET_NL()NET_PRINT("		- CurrentScreen = ")NET_PRINT(GET_XML_SCREEN_STRING_VALUE(Placement.CurrentScreen))
				NET_NL()NET_PRINT("		- aNextScreen = ")NET_PRINT(GET_XML_SCREEN_STRING_VALUE(Placement.aNextScreen))
				NET_NL()NET_PRINT("		- iMenu = ")NET_PRINT_INT(Placement.iMenu)
					
				TAKE_CONTROL_OF_FRONTEND()	
				
				Placement.bDoIHaveControl = TRUE
				
			ENDIF
	
			
			
		ELSE
			NET_NL()NET_PRINT("IS_FRONTEND_READY_FOR_CONTROL = FALSE ")
		ENDIF

//		IF Placement.bDoIHaveControl = TRUE	
//			RENDER_XML_MPHUD_LOADING(Placement)
//		ENDIF
		
	ENDIF
	IF GET_CURRENT_TRANSITION_STATE() != TRANSITION_STATE_DLC_INTRO_BINK
		RENDER_XML_MPHUD_LOADING(Placement)
	ENDIF

ENDPROC



