
USING "screens_header.sch"
USING "net_events.sch"
USING "script_network.sch"
USING "Transition_Common.sch"



PROC RENDER_MPHUD_CLOUD_BACK_UP(MPHUD_PLACEMENT_TOOLS& Placement)



	SET_ALL_MENUPED_VISIBLE(Placement.SelectionPed, Placement.IdlePed, FALSE )		

	
	STRING sStatementText = "HUD_CONNPROB" 
	STRING sSubText = "HUD_CLOUDONLIN" 
		

	SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sSubText, FE_WARNING_OK, "HUD_SPRETRNFRSH")
	
	
ENDPROC

PROC LOGIC_MPHUD_CLOUD_BACK_UP()


	IF NOT NETWORK_IS_SESSION_BUSY()
		IF GET_CURRENT_TRANSITION_STATE() = TRANSITION_STATE_WAIT_HUD_EXIT
			IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
				PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
					REQUEST_TRANSITION(TRANSITION_STATE_RETURN_TO_SINGLEPLAYER, GET_CURRENT_GAMEMODE(), HUD_STATE_NO_DISPLAY)
			ENDIF
		ENDIF
	ENDIF


ENDPROC

PROC PROCESS_MPHUD_CLOUD_BACK_UP(MPHUD_PLACEMENT_TOOLS& Placement)
	
	// initialise data
	IF NOT (Placement.bHudScreenInitialised)
		IF IS_PAUSE_MENU_ACTIVE()
			SET_PAUSE_MENU_ACTIVE(FALSE)
			SET_SKYFREEZE_CLEAR()
		ENDIF
		
		SET_BAIL_STILL_VALID(FALSE) //Need this when signing out in a corona, it would bring this screen up twice. It would think the bail was still valid in cleanup
		RUN_MP_DISCONNECTION_CLEANUP(Placement)
		
		SET_CURRENT_GAMEMODE(GAMEMODE_EMPTY)
		SET_JOINING_GAMEMODE(GAMEMODE_EMPTY)
		Placement.bHudScreenInitialised = TRUE
	ENDIF

	RUN_MP_DISCONNECTION_CLEANUP_EVERY_FRAME()

	// logic	
	LOGIC_MPHUD_CLOUD_BACK_UP()
	
	RENDER_MPHUD_CLOUD_BACK_UP(Placement)

			
ENDPROC

