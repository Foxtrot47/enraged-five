
USING "screens_header.sch"
USING "net_events.sch"
USING "script_network.sch"
USING "Transition_Common.sch"



PROC RENDER_MPHUD_SERVER_DATA_CORRECTION(MPHUD_PLACEMENT_TOOLS& Placement)


	SET_ALL_MENUPED_VISIBLE(Placement.SelectionPed, Placement.IdlePed, FALSE )		

	STRING sStatementText = "HUD_CONNPROB"
	STRING sQuestionText = ""

	BOOL RPSetCorrection = FALSE
	BOOL RPDeltaCorrection = FALSE
	BOOL CashCorrection = FALSE
	INT iSlotProcessed = 0
	
	IF g_i_DoCashGiftMessageAmount != 0
		CashCorrection = TRUE
	ENDIF

	IF g_b_DoRPGiftMessageAmount != 0
		RPDeltaCorrection = TRUE
	ENDIF
	
	INT I
	FOR I = 0 TO MAX_NUM_CHARACTER_SLOTS-1
		IF g_b_DoSetRPGiftAdminMessageAmount[I] != 0
			RPSetCorrection = TRUE
			iSlotProcessed = I
			I = MAX_NUM_CHARACTER_SLOTS
		ENDIF	
	ENDFOR
	

	FE_WARNING_FLAGS buttons = FE_WARNING_OK
	
	SWITCH Placement.iProcessedServerGiftsStages
		
		CASE 0
		
			IF RPSetCorrection = FALSE
			AND RPDeltaCorrection = FALSE
			
				Placement.iProcessedServerGiftsStages = 1
			
			ELSE
				IF RPSetCorrection = TRUE
				AND RPDeltaCorrection = FALSE
					sQuestionText = "HUD_RPGIFT"
					SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sQuestionText, buttons,  DEFAULT, TRUE, g_b_DoSetRPGiftAdminMessageAmount[iSlotProcessed])
				ENDIF
				
				IF RPSetCorrection = TRUE
				AND RPDeltaCorrection = TRUE
					sQuestionText = "HUD_RPGIFT"
					SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sQuestionText, buttons,  DEFAULT, TRUE, g_b_DoSetRPGiftAdminMessageAmount[iSlotProcessed])
				ENDIF
				
				IF RPSetCorrection = FALSE
				AND RPDeltaCorrection = TRUE
					sQuestionText = "HUD_RPGIFTDP"
					SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sQuestionText, buttons,  DEFAULT, TRUE, g_b_DoRPGiftMessageAmount)
					
				ENDIF
			ENDIF
			
		BREAK
		
		CASE 1
		
			IF DOES_TEXT_LABEL_EXIST(g_tl_CashGiftServerString)
				NET_NL()NET_PRINT("[GIFTTOOL] RENDER_MPHUD_SERVER_DATA_CORRECTION: g_tl_CashGiftServerString ")NET_PRINT(g_tl_CashGiftServerString)
				NET_PRINT(" = ")NET_PRINT(GET_FILENAME_FOR_AUDIO_CONVERSATION(g_tl_CashGiftServerString))NET_NL()
				SET_WARNING_MESSAGE_WITH_HEADER_AND_SUBSTRING_FLAGS(sStatementText, g_tl_CashGiftServerString, buttons)
			ELIF CashCorrection 
				IF IS_PC_VERSION() = FALSE
					sQuestionText = "HUD_CASHGIFT"
					SET_WARNING_MESSAGE_WITH_HEADER_AND_SUBSTRING_FLAGS(sStatementText, sQuestionText, buttons, "", FALSE, -1, WARNING_MESSAGE_FIRST_SUBSTRING_IS_LITERAL, g_tl_CashGiftAmount)
				ELSE
					sQuestionText = "HUD_1_CASHGIFT"
					SET_WARNING_MESSAGE_WITH_HEADER_AND_SUBSTRING_FLAGS(sStatementText, sQuestionText, buttons, "", TRUE, g_i_DoCashGiftACTUALAmount)
				ENDIF
			ELSE
				Placement.iProcessedServerGiftsStages = 2
			ENDIF
			
		
		BREAK
		
		CASE 2
		
		BREAK
		
	ENDSWITCH
	


	
ENDPROC



PROC LOGIC_MPHUD_SERVER_DATA_CORRECTION(MPHUD_PLACEMENT_TOOLS& Placement)

	SWITCH Placement.iProcessedServerGiftsStages
		
		CASE 0
			IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
				Placement.iProcessedServerGiftsStages = 1
				
			ENDIF
		
		
		BREAK
		
		
		CASE 1
		
			IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
				
				NET_NL()NET_PRINT("LOGIC_MPHUD_SERVER_DATA_CORRECTION: INPUT_FRONTEND_ACCEPT ")
				Placement.iProcessedServerGiftsStages = 2
			ENDIF
		BREAK
		
		CASE 2
			HUD_CHANGE_STATE(HUD_STATE_LOADING)
			TRANSITION_CHANGE_STATE(TRANSITION_STATE_PRE_FM_LAUNCH_SCRIPT)
		BREAK
		
	ENDSWITCH


ENDPROC

PROC PROCESS_MPHUD_SERVER_DATA_CORRECTION(MPHUD_PLACEMENT_TOOLS& Placement)
	
	// initialise data
	IF NOT (Placement.bHudScreenInitialised)
		
		#IF IS_DEBUG_BUILD
		g_b_IsFakePCGiftsWidgetActive = FALSE
		#ENDIF
		
		Placement.bHudScreenInitialised = TRUE
	ENDIF


	RUN_LOADING_SCREEN_SHUTDOWN_WITH_MESSAGE()

	// logic	
	LOGIC_MPHUD_SERVER_DATA_CORRECTION(Placement)
	RENDER_MPHUD_SERVER_DATA_CORRECTION(Placement)

			
ENDPROC

