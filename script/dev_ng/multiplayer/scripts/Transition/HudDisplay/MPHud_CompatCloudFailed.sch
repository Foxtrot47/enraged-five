
USING "screens_header.sch"
USING "net_events.sch"
USING "script_network.sch"
USING "Transition_Common.sch"





PROC RENDER_MPHUD_COMPAT_CLOUD_FAILED(MPHUD_PLACEMENT_TOOLS& Placement)


	SET_ALL_MENUPED_VISIBLE(Placement.SelectionPed, Placement.IdlePed, FALSE )		

	
	

	STRING sStatementText = "HUD_CONNPROB"
	STRING sQuestionText = "HUD_COMPATCLOD"
	STRING sSubText = "HUD_SPRETURNTRY"

	SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sQuestionText, FE_WARNING_OK, sSubText)
	
					
	
	
	
ENDPROC






PROC LOGIC_MPHUD_COMPAT_CLOUD_FAILED()


	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
	
		REQUEST_TRANSITION(TRANSITION_STATE_RETURN_TO_SINGLEPLAYER, GET_CURRENT_GAMEMODE(), HUD_STATE_LOADING)
	
	ENDIF
	

ENDPROC

PROC PROCESS_MPHUD_COMPAT_CLOUD_FAILED(MPHUD_PLACEMENT_TOOLS& Placement)
	
	// initialise data
	IF NOT (Placement.bHudScreenInitialised)
	

		RUN_MP_DISCONNECTION_CLEANUP(Placement)
	
		Placement.bHudScreenInitialised = TRUE
	ENDIF

	RUN_MP_DISCONNECTION_CLEANUP_EVERY_FRAME()


	// logic	
	LOGIC_MPHUD_COMPAT_CLOUD_FAILED()
	RENDER_MPHUD_COMPAT_CLOUD_FAILED(Placement)

			
ENDPROC

