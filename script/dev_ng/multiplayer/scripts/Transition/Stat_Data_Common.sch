USING "globals.sch"
USING "Transition_Controller.sch"



// -------------------- Accessors to StatDataPlayerBD ----------------


/// PURPOSE: Return the rank of the player stored in Stat BD (0 if not in array)
FUNC INT GET_PLAYER_GLOBAL_FM_RANK(PLAYER_INDEX playerID)

	RETURN GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].scoreData.iRank
ENDFUNC

FUNC INT GET_PLAYER_GLOBAL_RANK(PLAYER_INDEX playerID)
	RETURN GET_PLAYER_GLOBAL_FM_RANK(playerID)
ENDFUNC

/// PURPOSE: Return the team of the player stored in Stat BD (-1 if not in array)
FUNC INT GET_PLAYER_GLOBAL_TEAM(PLAYER_INDEX playerID)
//	IF IS_PLAYER_IN_GBDLOBBY_ARRAY(playerID)
	RETURN GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].scoreData.iTeam

ENDFUNC


