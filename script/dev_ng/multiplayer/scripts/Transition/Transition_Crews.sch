



USING "Commands_Network.sch"
USING "Stat_Data_Common.sch"
USING "hud_drawing.sch"

FUNC BOOL IS_GAMER_LOCAL(GAMER_HANDLE& GamerHandle)
	
	GAMER_HANDLE LocalGamer = GET_LOCAL_GAMER_HANDLE()
 	IF NETWORK_ARE_HANDLES_THE_SAME(GamerHandle, LocalGamer)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_IN_ACTIVE_CLAN()

	GAMER_HANDLE GamerHandle
	
	IF IS_PLAYER_ONLINE()

		IF NETWORK_IS_SIGNED_ONLINE()
		AND NETWORK_IS_SIGNED_IN()
		AND NETWORK_HAS_SOCIAL_CLUB_ACCOUNT()
		
			GamerHandle = GET_LOCAL_GAMER_HANDLE()
			
			IF NETWORK_CLAN_SERVICE_IS_VALID() AND NETWORK_CLAN_PLAYER_IS_ACTIVE(GamerHandle) 
				RETURN TRUE
			ENDIF
			
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC NETWORK_CLAN_DESC GET_LOCAL_PLAYER_CREW_DATA()
	GAMER_HANDLE GamerHandle = GET_LOCAL_GAMER_HANDLE()
	NETWORK_CLAN_DESC retDesc
		IF IS_PLAYER_ONLINE()
			NETWORK_CLAN_PLAYER_GET_DESC( retDesc, 0, GamerHandle)
		ENDIF
		
		RETURN retDesc

//	RETURN g_Private_PlayerCrewData[NATIVE_TO_INT(PLAYER_ID())]
ENDFUNC




FUNC INT GET_MP_PLAYER_CLAN_ID(GAMER_HANDLE& gamerHandle)

      NETWORK_CLAN_DESC PlayerClan  
      IF IS_PLAYER_ONLINE()
            IF NETWORK_CLAN_SERVICE_IS_VALID()  
				IF NETWORK_CLAN_PLAYER_IS_ACTIVE(gamerHandle)
	                  NETWORK_CLAN_PLAYER_GET_DESC(PlayerClan, SIZE_OF(PlayerClan), gamerHandle)
	                  RETURN PlayerClan.Id
				ENDIF
            ENDIF
      ENDIF

      RETURN -1
ENDFUNC

FUNC INT GET_LOCAL_PLAYER_CLAN_ID()
	GAMER_HANDLE GamerHandle = GET_LOCAL_GAMER_HANDLE()
    RETURN GET_MP_PLAYER_CLAN_ID(GamerHandle)

ENDFUNC

FUNC TEXT_LABEL_63 GET_LOCAL_PLAYER_CREW_NAME()
	GAMER_HANDLE GamerHandle = GET_LOCAL_GAMER_HANDLE()
	NETWORK_CLAN_DESC retDesc
	NETWORK_CLAN_PLAYER_GET_DESC( retDesc, 0, GamerHandle)
	RETURN retDesc.ClanName
//	RETURN g_Private_PlayerCrewData[NATIVE_TO_INT(PLAYER_ID())].ClanName
ENDFUNC

FUNC NETWORK_CLAN_DESC GET_PLAYER_CREW(PLAYER_INDEX aPlayer)

	IF IS_PLAYER_LOCAL(aPlayer)
		RETURN g_Private_PlayerCrewData[NATIVE_TO_INT(PLAYER_ID())]
	ENDIF
	
	
	GAMER_HANDLE GamerHandle = GET_GAMER_HANDLE_PLAYER(aPlayer)
	NETWORK_CLAN_DESC retDesc
	
	NETWORK_CLAN_PLAYER_GET_DESC( retDesc, SIZE_OF(retDesc), GamerHandle)
	
	RETURN retDesc
ENDFUNC

FUNC NETWORK_CLAN_DESC GET_GAMER_CREW(GAMER_HANDLE& GamerHandle)

	IF IS_GAMER_LOCAL(GamerHandle)
		RETURN g_Private_PlayerCrewData[NATIVE_TO_INT(PLAYER_ID())]
	ENDIF

	NETWORK_CLAN_DESC retDesc

	NETWORK_CLAN_PLAYER_GET_DESC( retDesc, SIZE_OF(retDesc), GamerHandle)
	
	RETURN retDesc
ENDFUNC

FUNC BOOL IS_PLAYER_IN_ACTIVE_CLAN(GAMER_HANDLE& gamerHandle)

//	IF IS_GAMER_LOCAL(GamerHandle)
//		IF IS_LOCAL_PLAYER_IN_ACTIVE_CLAN()
//			RETURN TRUE
//		ENDIF
//	ENDIF


	IF IS_PLAYER_ONLINE()
		IF NETWORK_CLAN_SERVICE_IS_VALID() AND NETWORK_CLAN_PLAYER_IS_ACTIVE(gamerHandle)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC BOOL IS_PLAYER_ID_IN_ACTIVE_CLAN(PLAYER_INDEX player)
	IF player = INVALID_PLAYER_INDEX()
		RETURN FALSE
	ENDIF
	
	GAMER_HANDLE playerHandle = GET_GAMER_HANDLE_PLAYER(PLAYER_ID())
	
	RETURN IS_PLAYER_IN_ACTIVE_CLAN(playerHandle)
ENDFUNC

FUNC BOOL IS_PLAYER_CLAN_PRIVATE(GAMER_HANDLE& gamerHandle)

//	IF IS_GAMER_LOCAL(GamerHandle)
//		IF IS_LOCAL_PLAYER_IN_ACTIVE_CLAN()
//			IF g_Private_PlayerCrewData[NATIVE_TO_INT(PLAYER_ID())].IsOpenClan = 0
//				RETURN TRUE
//			ENDIF
//		ENDIF
//	ENDIF


	NETWORK_CLAN_DESC PlayerClan	
	IF IS_PLAYER_ONLINE()
		IF NETWORK_CLAN_SERVICE_IS_VALID() 
			IF NETWORK_CLAN_PLAYER_IS_ACTIVE(gamerHandle)
				NETWORK_CLAN_PLAYER_GET_DESC(PlayerClan, SIZE_OF(PlayerClan), gamerHandle)
				IF PlayerClan.IsOpenClan = 0
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	//NET_PRINT(" IS_PLAYER_CLAN_PRIVATE - CHECK F - FALSE") NET_NL()	
	RETURN FALSE

ENDFUNC

FUNC BOOL IS_PLAYER_CLAN_LEADER(GAMER_HANDLE& gamerHandle)
	
//	IF IS_GAMER_LOCAL(GamerHandle)
//		IF IS_LOCAL_PLAYER_IN_ACTIVE_CLAN()
//			IF ARE_STRINGS_EQUAL(g_Private_PlayerCrewData[NATIVE_TO_INT(PLAYER_ID())].RankName, "Leader")
//			AND g_Private_PlayerCrewData[NATIVE_TO_INT(PLAYER_ID())].RankOrder = 0
//				RETURN TRUE
//			ENDIF
//		ENDIF
//	ENDIF
	
	NETWORK_CLAN_DESC PlayerClan	
	IF IS_PLAYER_ONLINE()
		IF NETWORK_CLAN_SERVICE_IS_VALID() AND NETWORK_CLAN_PLAYER_IS_ACTIVE(gamerHandle)
			NETWORK_CLAN_PLAYER_GET_DESC(PlayerClan, SIZE_OF(PlayerClan), gamerHandle)
			IF ARE_STRINGS_EQUAL(PlayerClan.RankName, "Leader")
			AND PlayerClan.RankOrder = 0
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC



FUNC BOOL IS_PLAYER_CLAN_FOUNDER(GAMER_HANDLE& gamerHandle)

//	IF IS_GAMER_LOCAL(GamerHandle)
//		IF IS_LOCAL_PLAYER_IN_ACTIVE_CLAN()
//			IF ARE_STRINGS_EQUAL(g_Private_PlayerCrewData[NATIVE_TO_INT(PLAYER_ID())].RankName, "Leader")
//			AND g_Private_PlayerCrewData[NATIVE_TO_INT(PLAYER_ID())].RankOrder = 0
//				RETURN TRUE
//			ENDIF
//		ENDIF
//	ENDIF

	//Currently the same as Leader, need some SD functionality here. 
	NETWORK_CLAN_DESC PlayerClan
	IF IS_PLAYER_ONLINE()
		IF NETWORK_CLAN_SERVICE_IS_VALID() AND NETWORK_CLAN_PLAYER_IS_ACTIVE(gamerHandle)
			NETWORK_CLAN_PLAYER_GET_DESC(PlayerClan, SIZE_OF(PlayerClan), gamerHandle)
			IF ARE_STRINGS_EQUAL(PlayerClan.RankName, "Leader")
			AND PlayerClan.RankOrder = 0
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC


FUNC INT GET_PLAYER_CLAN_RANK_LEVEL(GAMER_HANDLE& gamerHandle)



	//Currently the same as Leader, need some SD functionality here. 
	NETWORK_CLAN_DESC PlayerClan
	IF IS_PLAYER_ONLINE()
		IF NETWORK_CLAN_SERVICE_IS_VALID() AND NETWORK_CLAN_PLAYER_IS_ACTIVE(gamerHandle)
			NETWORK_CLAN_PLAYER_GET_DESC(PlayerClan, SIZE_OF(PlayerClan), gamerHandle)
			RETURN PlayerClan.RankOrder
		ENDIF
	ENDIF
	
	RETURN -1

ENDFUNC


FUNC BOOL IS_PLAYER_A_ROCKSTAR(PLAYER_INDEX aPlayer)

	IF IS_PLAYER_ONLINE()
		RETURN NETWORK_PLAYER_IS_ROCKSTAR_DEV(aPlayer) 
	ENDIF
	RETURN FALSE

ENDFUNC

FUNC BOOL IS_CREW_A_ROCKSTAR_CREW(NETWORK_CLAN_DESC& aCrew)

	IF IS_PLAYER_ONLINE()
		RETURN NETWORK_CLAN_IS_ROCKSTAR_CLAN(aCrew, SIZE_OF(aCrew)) 
	ENDIF
	RETURN FALSE

ENDFUNC


FUNC BOOL IS_PLAYER_IN_A_ROCKSTAR_CREW(GAMER_HANDLE& gamerHandle)

	IF IS_PLAYER_ONLINE()
		IF IS_PLAYER_IN_ACTIVE_CLAN(gamerHandle)
			NETWORK_CLAN_DESC acrew = GET_GAMER_CREW(gamerHandle)
			RETURN NETWORK_CLAN_IS_ROCKSTAR_CLAN(acrew, SIZE_OF(acrew)) 
		ENDIF
	ENDIF
	RETURN FALSE

ENDFUNC

FUNC BOOL IS_PLAYER_CLAN_SYSTEM(GAMER_HANDLE& gamerHandle)
	
//	IF IS_GAMER_LOCAL(GamerHandle)
//		IF IS_LOCAL_PLAYER_IN_ACTIVE_CLAN()
//			IF g_Private_PlayerCrewData[NATIVE_TO_INT(PLAYER_ID())].IsSystemClan = 1
//				RETURN TRUE
//			ENDIF
//		ENDIF
//	ENDIF
	
	NETWORK_CLAN_DESC PlayerClan
	IF IS_PLAYER_ONLINE()
		IF NETWORK_CLAN_SERVICE_IS_VALID() AND NETWORK_CLAN_PLAYER_IS_ACTIVE(gamerHandle)
			NETWORK_CLAN_PLAYER_GET_DESC(PlayerClan, SIZE_OF(PlayerClan), gamerHandle)
			IF PlayerClan.IsSystemClan = 1
				RETURN TRUE
			ENDIF
		
		ENDIF
	ENDIF
	RETURN FALSE

ENDFUNC

FUNC TEXT_LABEL_63 GET_PLAYER_CLAN_NAME(GAMER_HANDLE& gamerHandle)
//	
//	IF IS_GAMER_LOCAL(GamerHandle)
//		IF IS_LOCAL_PLAYER_IN_ACTIVE_CLAN()
//			RETURN g_Private_PlayerCrewData[NATIVE_TO_INT(PLAYER_ID())].ClanName 
//		ENDIF
//	ENDIF
	
	NETWORK_CLAN_DESC PlayerClan
	IF IS_PLAYER_ONLINE()
		IF NETWORK_CLAN_SERVICE_IS_VALID() AND NETWORK_CLAN_PLAYER_IS_ACTIVE(gamerHandle)
			NETWORK_CLAN_PLAYER_GET_DESC(PlayerClan, SIZE_OF(PlayerClan), gamerHandle)
			RETURN PlayerClan.ClanName
		ENDIF
	ENDIF
	
	TEXT_LABEL_63 tlResult =  GET_FILENAME_FOR_AUDIO_CONVERSATION("CREW_NONE") //"CLAN_TT"
	RETURN tlResult
ENDFUNC


FUNC TEXT_LABEL_7 GET_PLAYER_CLAN_TAG(GAMER_HANDLE& gamerHandle)
	
//	IF IS_GAMER_LOCAL(GamerHandle)
//		IF IS_LOCAL_PLAYER_IN_ACTIVE_CLAN()
//			RETURN g_Private_PlayerCrewData[NATIVE_TO_INT(PLAYER_ID())].ClanTag 
//		ENDIF
//	ENDIF
	
	TEXT_LABEL_7 result = ""
	NETWORK_CLAN_DESC PlayerClan
	IF IS_PLAYER_ONLINE()
		IF NETWORK_CLAN_SERVICE_IS_VALID() AND NETWORK_CLAN_PLAYER_IS_ACTIVE(gamerHandle)
			NETWORK_CLAN_PLAYER_GET_DESC(PlayerClan, SIZE_OF(PlayerClan), gamerHandle)
			RETURN PlayerClan.ClanTag
		ENDIF
	ENDIF
	RETURN result
ENDFUNC

FUNC INT GET_PLAYER_CLAN_MEMBER_COUNT(GAMER_HANDLE& gamerHandle)

//	IF IS_GAMER_LOCAL(GamerHandle)
//		IF IS_LOCAL_PLAYER_IN_ACTIVE_CLAN()
//			RETURN g_Private_PlayerCrewData[NATIVE_TO_INT(PLAYER_ID())].MemberCount 
//		ENDIF
//	ENDIF

	NETWORK_CLAN_DESC PlayerClan	
	IF IS_PLAYER_ONLINE()
		IF NETWORK_CLAN_SERVICE_IS_VALID() AND NETWORK_CLAN_PLAYER_IS_ACTIVE(gamerHandle)
			NETWORK_CLAN_PLAYER_GET_DESC(PlayerClan, SIZE_OF(PlayerClan), gamerHandle)
			RETURN PlayerClan.MemberCount
		ENDIF
	ENDIF

	RETURN 0
ENDFUNC


FUNC INT GET_PLAYER_CLAN_ID(GAMER_HANDLE& gamerHandle)

//	IF IS_GAMER_LOCAL(GamerHandle)
//		IF IS_LOCAL_PLAYER_IN_ACTIVE_CLAN()
//			RETURN g_Private_PlayerCrewData[NATIVE_TO_INT(PLAYER_ID())].Id 
//		ENDIF
//	ENDIF

	NETWORK_CLAN_DESC PlayerClan	
	IF IS_PLAYER_ONLINE()
		IF NETWORK_CLAN_SERVICE_IS_VALID() AND NETWORK_CLAN_PLAYER_IS_ACTIVE(gamerHandle)
			NETWORK_CLAN_PLAYER_GET_DESC(PlayerClan, SIZE_OF(PlayerClan), gamerHandle)
			RETURN PlayerClan.Id
		ENDIF
	ENDIF

	RETURN -1
ENDFUNC




FUNC BOOL GET_GAMER_CLAN_COLOUR(GAMER_HANDLE& gamerHandle, INT& RED, INT& GREEN, INT& BLUE)

	NETWORK_CLAN_DESC PlayerClan	
	IF IS_PLAYER_ONLINE()
		IF NETWORK_CLAN_SERVICE_IS_VALID() AND NETWORK_CLAN_PLAYER_IS_ACTIVE(gamerHandle)
			NETWORK_CLAN_PLAYER_GET_DESC(PlayerClan, SIZE_OF(PlayerClan), gamerHandle)
			RED = PlayerClan.ClanColor_Red
			GREEN = PlayerClan.ClanColor_Green
			BLUE = PlayerClan.ClanColor_Blue
  			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL GET_PLAYER_CLAN_COLOUR(PLAYER_INDEX aPlayer, INT& RED, INT& GREEN, INT& BLUE)

	
	
	NETWORK_CLAN_DESC PlayerClan
	IF IS_PLAYER_ONLINE()
	AND NETWORK_IS_SIGNED_ONLINE()
	
		GAMER_HANDLE GamerHandle = GET_GAMER_HANDLE_PLAYER(aPlayer)
	
		IF NETWORK_CLAN_SERVICE_IS_VALID() AND NETWORK_CLAN_PLAYER_IS_ACTIVE(gamerHandle)
			NETWORK_CLAN_PLAYER_GET_DESC(PlayerClan, SIZE_OF(PlayerClan), gamerHandle)
			RED = PlayerClan.ClanColor_Red
			GREEN = PlayerClan.ClanColor_Green
			BLUE = PlayerClan.ClanColor_Blue
  			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC


FUNC BOOL IS_CREW_CLAN_PRIVATE(GAMER_HANDLE& gamerHandle,NETWORK_CLAN_DESC& CrewHandle)

//	IF IS_GAMER_LOCAL(GamerHandle)
//		IF IS_LOCAL_PLAYER_IN_ACTIVE_CLAN()
//			IF g_Private_PlayerCrewData[NATIVE_TO_INT(PLAYER_ID())].IsOpenClan = 0
//				RETURN TRUE
//			ENDIF
//		ENDIF
//	ENDIF

	IF IS_PLAYER_ONLINE()
	AND NETWORK_CLAN_SERVICE_IS_VALID()
	AND NETWORK_CLAN_PLAYER_IS_ACTIVE(gamerHandle)
		IF CrewHandle.IsOpenClan = 0
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE

ENDFUNC





PROC GET_PLAYER_CREW_TAG_FOR_SCALEFORM(PLAYER_INDEX playerID, TEXT_LABEL_15 &tlCrewTag)
		
	tlCrewTag = ""
	GAMER_HANDLE aGamerHandle = GET_GAMER_HANDLE_PLAYER(playerID)
	
	// XB1 check individual player and hide tag if we do not have privileges
	IF IS_XBOX_PLATFORM()
	#IF FEATURE_GEN9_RELEASE
	OR IS_PROSPERO_VERSION()
	#ENDIF
		IF IS_GAMER_HANDLE_VALID(aGamerHandle)
			IF NOT NETWORK_CAN_VIEW_GAMER_USER_CONTENT(aGamerHandle)
				EXIT
			ENDIF
		ENDIF
	ELSE
		IF NOT NETWORK_HAVE_USER_CONTENT_PRIVILEGES()
			EXIT
		ENDIF
	ENDIF
	
	IF IS_PLAYER_IN_ACTIVE_CLAN(aGamerHandle)
		NETWORK_CLAN_DESC crewDesc = GET_PLAYER_CREW(playerID)
		
		NETWORK_CLAN_GET_UI_FORMATTED_TAG_EZ(crewDesc, tlCrewTag)		
	ENDIF
ENDPROC


PROC GET_GAMER_HANDLE_CREW_TAG_FOR_SCALEFORM(GAMER_HANDLE aGamerHandle, TEXT_LABEL_15 &tlCrewTag)
	
	tlCrewTag = ""
	
	//IF IS_PLAYER_IN_ACTIVE_CLAN(aGamerHandle)
		NETWORK_CLAN_DESC crewDesc
		NETWORK_CLAN_PLAYER_GET_DESC(crewDesc, SIZE_OF(crewDesc), aGamerHandle)
		NETWORK_CLAN_GET_UI_FORMATTED_TAG_EZ(crewDesc, tlCrewTag)		
	//ENDIF
ENDPROC

//FUNC BOOL IS_CREW_CLAN_LEADER(GAMER_HANDLE& gamerHandle,NETWORK_CLAN_DESC& CrewHandle)
//	
//	IF IS_PLAYER_ONLINE()
//	AND NETWORK_CLAN_SERVICE_IS_VALID()
//	AND NETWORK_CLAN_PLAYER_IS_ACTIVE(gamerHandle)
//		IF ARE_STRINGS_EQUAL(CrewHandle.RankName, "Leader")
//		AND CrewHandle.RankOrder = 0
//			RETURN TRUE
//		ENDIF
//	ENDIF
//	
//	RETURN FALSE
//
//ENDFUNC

//FUNC BOOL IS_CREW_CLAN_FOUNDER(GAMER_HANDLE& gamerHandle,NETWORK_CLAN_DESC& CrewHandle)
//
//	//Currently the same as Leader, need some SD functionality here. 
//	IF IS_PLAYER_ONLINE()
//	AND NETWORK_CLAN_SERVICE_IS_VALID()
//	AND NETWORK_CLAN_PLAYER_IS_ACTIVE(gamerHandle)
//		IF ARE_STRINGS_EQUAL(CrewHandle.RankName, "Leader")
//		AND CrewHandle.RankOrder = 0
//			RETURN TRUE
//		ENDIF		
//	ENDIF
//	
//	RETURN FALSE
//
//ENDFUNC

FUNC BOOL IS_CREW_CLAN_ROCKSTAR(GAMER_HANDLE& gamerHandle,NETWORK_CLAN_DESC& CrewHandle)
	
	IF IS_PLAYER_ONLINE()
	AND NETWORK_CLAN_SERVICE_IS_VALID()
	AND NETWORK_CLAN_PLAYER_IS_ACTIVE(gamerHandle)
		IF CrewHandle.IsSystemClan = 1
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE

ENDFUNC

FUNC TEXT_LABEL_63 GET_CREW_CLAN_NAME(GAMER_HANDLE& gamerHandle,NETWORK_CLAN_DESC& CrewHandle)
	
	IF IS_PLAYER_ONLINE()
	AND NETWORK_CLAN_SERVICE_IS_VALID()
	AND NETWORK_CLAN_PLAYER_IS_ACTIVE(gamerHandle)
		RETURN CrewHandle.ClanName
	ENDIF
	
	TEXT_LABEL_63 tlName = "CLAN_TT"
	RETURN tlName
ENDFUNC

FUNC TEXT_LABEL_7 GET_CREW_CLAN_TAG(GAMER_HANDLE& gamerHandle,NETWORK_CLAN_DESC& CrewHandle)
	
	TEXT_LABEL_7 result = ""
	IF IS_PLAYER_ONLINE()
	AND NETWORK_CLAN_SERVICE_IS_VALID()
	AND NETWORK_CLAN_PLAYER_IS_ACTIVE(gamerHandle)
		RETURN CrewHandle.ClanTag
		
	ENDIF
	RETURN result
ENDFUNC

FUNC INT GET_CREW_MEMBER_COUNT(GAMER_HANDLE& gamerHandle,NETWORK_CLAN_DESC& CrewHandle)


	IF IS_PLAYER_ONLINE()
	AND NETWORK_CLAN_SERVICE_IS_VALID()
	AND NETWORK_CLAN_PLAYER_IS_ACTIVE(gamerHandle)
		RETURN CrewHandle.MemberCount
	ENDIF

	RETURN 0
ENDFUNC


FUNC BOOL HAS_LOCAL_PLAYER_CHANGED_ACTIVE_CREW()
	
	
	#IF IS_DEBUG_BUILD
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_noCrewChangeKick")
			RETURN FALSE
		ENDIF
	#ENDIF
	
	
	RETURN g_Private_Primary_Clan_Changed
	
//	GAMER_HANDLE GamerHandle = GET_GAMER_HANDLE_PLAYER(PLAYER_ID())
//		
//	
//		
//	IF g_Private_PlayerCrewData[NATIVE_TO_INT(PLAYER_ID())].Id <> GET_PLAYER_CLAN_ID(GamerHandle)
//		NET_NL()NET_PRINT("HAS_LOCAL_PLAYER_CHANGED_ACTIVE_CREW: g_Private_PlayerCrewData[NATIVE_TO_INT(PLAYER_ID())].Id = ")NET_PRINT_INT(g_Private_PlayerCrewData[NATIVE_TO_INT(PLAYER_ID())].Id)
//		NET_NL()NET_PRINT("HAS_LOCAL_PLAYER_CHANGED_ACTIVE_CREW: GET_PLAYER_CLAN_ID(GamerHandle) = ")NET_PRINT_INT(GET_PLAYER_CLAN_ID(GamerHandle))
//
//		RETURN TRUE
//	ENDIF
	
	RETURN FALSE
ENDFUNC	

/// PURPOSE: Updates RGB values passed to crew colour. Default to black if player is not in a crew.
PROC GET_PLAYER_CREW_COLOUR(INT &iRed, INT &iGreen, INT &iBlue)

	// Default colour to black
	iRed = 0
	iGreen = 0
	iBlue = 0

	IF IS_LOCAL_PLAYER_IN_ACTIVE_CLAN()
		iRed = g_Private_LocalPlayerCrew_Colour_Red
		iGreen = g_Private_LocalPlayerCrew_Colour_Green
		iBlue = g_Private_LocalPlayerCrew_Colour_Blue
	ENDIF

ENDPROC
/// PURPOSE: Updates RGB values passed to crew colour. Default to black if player is not in a crew.
///  url:bugstar:4472000
PROC GET_PLAYERS_CREW_COLOUR(PLAYER_INDEX pIndex, BOOL bLocal, INT &iRed, INT &iGreen, INT &iBlue)

	// Default colour to black
	iRed = 0
	iGreen = 0
	iBlue = 0

	IF bLocal
		GET_PLAYER_CREW_COLOUR(iRed, iGreen, iBlue)
	ELSE
		IF GlobalplayerBD[NATIVE_TO_INT(pIndex)].CrewData.ID != -1 
			iRed = GlobalplayerBD[NATIVE_TO_INT(pIndex)].CrewData.iCrewColour_Red 
			iGreen = GlobalplayerBD[NATIVE_TO_INT(pIndex)].CrewData.iCrewColour_Blue
			iBlue = GlobalplayerBD[NATIVE_TO_INT(pIndex)].CrewData.iCrewColour_Green
		ENDIF
	ENDIF
ENDPROC


PROC SETUP_CREW_DATA()

	

	GAMER_HANDLE GamerHandle = GET_LOCAL_GAMER_HANDLE()
	
	
	
	IF IS_PLAYER_ONLINE()
		IF NETWORK_CLAN_SERVICE_IS_VALID() AND NETWORK_CLAN_PLAYER_IS_ACTIVE(GamerHandle)
						
			
			g_Private_PlayerCrewData[NATIVE_TO_INT(PLAYER_ID())] = GET_LOCAL_PLAYER_CREW_DATA()
			
			NET_NL()NET_PRINT("SETUP_CREW_DATA: g_Private_PlayerCrewData[NATIVE_TO_INT(PLAYER_ID())].Id = ")NET_PRINT_INT(g_Private_PlayerCrewData[NATIVE_TO_INT(PLAYER_ID())].Id)
			NET_NL()NET_PRINT("SETUP_CREW_DATA: GET_PLAYER_CLAN_ID(GamerHandle) = ")NET_PRINT_INT(GET_PLAYER_CLAN_ID(GamerHandle))
			NET_NL()NET_PRINT("SETUP_CREW_DATA: g_Private_PlayerCrewData[NATIVE_TO_INT(PLAYER_ID())].ClanName = ")NET_PRINT(g_Private_PlayerCrewData[NATIVE_TO_INT(PLAYER_ID())].ClanName)
			NET_NL()NET_PRINT("SETUP_CREW_DATA: g_Private_PlayerCrewData[NATIVE_TO_INT(PLAYER_ID())].ClanTag = ")NET_PRINT(g_Private_PlayerCrewData[NATIVE_TO_INT(PLAYER_ID())].ClanTag)
			
			IF IS_LOCAL_PLAYER_IN_ACTIVE_CLAN()	
				
				IF GET_PLAYER_CLAN_COLOUR(PLAYER_ID(), g_Private_LocalPlayerCrew_Colour_Red, 	g_Private_LocalPlayerCrew_Colour_Green, g_Private_LocalPlayerCrew_Colour_Blue)
					g_Private_IsCrewColourSet = TRUE
				ELSE
					g_Private_IsCrewColourSet = FALSE
				ENDIF
				
				NET_NL()NET_PRINT("SETUP_CREW_DATA Running for Crew Colour - ")
				NET_NL()NET_PRINT("		- g_Private_IsCrewColourSet = ") NET_PRINT_BOOL(g_Private_IsCrewColourSet)
				NET_NL()NET_PRINT("		- R = ") NET_PRINT_INT(g_Private_LocalPlayerCrew_Colour_Red)
				NET_NL()NET_PRINT("		- G = ") NET_PRINT_INT(g_Private_LocalPlayerCrew_Colour_Green)
				NET_NL()NET_PRINT("		- B = ") NET_PRINT_INT(g_Private_LocalPlayerCrew_Colour_Blue)
	
			ENDIF
		ELSE
			NET_NL()NET_PRINT("SETUP_CREW_DATA: g_Private_PlayerCrewData[NATIVE_TO_INT(PLAYER_ID())].Id = -1 ")
			g_Private_PlayerCrewData[NATIVE_TO_INT(PLAYER_ID())].Id = -1
			g_Private_IsCrewColourSet = FALSE
		ENDIF
	ENDIF
	
	


ENDPROC

PROC UPDATE_CREW_METADATA()

	g_Private_TriggerCrewMetaDataChange = TRUE
ENDPROC

FUNC BOOL IS_CREW_METADATA_UPDATING()
	RETURN g_Private_TriggerCrewMetaDataChange
ENDFUNC


FUNC BOOL SHOULD_REFRESH_TITLES_AS_CREW_TITLES()

	IF g_b_HaveStatsInitialised
		IF HAS_IMPORTANT_STATS_LOADED()
			IF (GET_MP_CHARACTER_TITLE_SETTING() != g_Private_CrewTitleSettings_LastFrame) 
				IF (GET_MP_CHARACTER_TITLE_SETTING() = MP_SETTING_TITLE_CREW)
					RETURN TRUE
				ENDIF
			ENDIF
		
		ENDIF
	ENDIF
	
	
	RETURN FALSE

ENDFUNC

PROC SETUP_CREW_METADATA()

	IF (g_Private_TriggerCrewMetaDataChange OR SHOULD_REFRESH_TITLES_AS_CREW_TITLES())
	AND NETWORK_CLAN_HAS_CREWINFO_METADATA_BEEN_RECEIVED()
	AND IS_LOCAL_PLAYER_IN_ACTIVE_CLAN() 
		
		BOOL GotCrewColours = FALSE
			
		IF GET_PLAYER_CLAN_COLOUR(PLAYER_ID(), g_Private_LocalPlayerCrew_Colour_Red, 	g_Private_LocalPlayerCrew_Colour_Green, g_Private_LocalPlayerCrew_Colour_Blue)
			g_Private_IsCrewColourSet = TRUE
		ELSE
			g_Private_IsCrewColourSet = FALSE
		ENDIF
		

		
		NET_NL()NET_PRINT("SETUP_CREW_METADATA Running for Crew Colour - ")
		NET_NL()NET_PRINT("		- g_Private_IsCrewColourSet = ") NET_PRINT_BOOL(g_Private_IsCrewColourSet)
		NET_NL()NET_PRINT("		- R = ") NET_PRINT_INT(g_Private_LocalPlayerCrew_Colour_Red)
		NET_NL()NET_PRINT("		- G = ") NET_PRINT_INT(g_Private_LocalPlayerCrew_Colour_Green)
		NET_NL()NET_PRINT("		- B = ") NET_PRINT_INT(g_Private_LocalPlayerCrew_Colour_Blue)
		NET_NL()NET_PRINT("		- NETWORK_CLAN_HAS_CREWINFO_METADATA_BEEN_RECEIVED = ") NET_PRINT_BOOL(NETWORK_CLAN_HAS_CREWINFO_METADATA_BEEN_RECEIVED())
		GotCrewColours = TRUE
		



		BOOL GotAllRankTitles = TRUE
		INT I
		INT WhichRank
		FOR I = 1 TO 11
			WhichRank = (I*10)
			IF WhichRank > 100
				WhichRank = 100 
			ENDIF
			NETWORK_CLAN_CREWINFO_GET_CREWRANKTITLE(WhichRank, g_Private_LocalPlayerCrew_RankTitle[I-1])
			NET_NL()NET_PRINT("SETUP_CREW_METADATA Running for rank Title - ")
			NET_NL()NET_PRINT("		- Rank = ")NET_PRINT_INT(WhichRank)
			NET_NL()NET_PRINT("		- index = ")NET_PRINT_INT(I-1)
			NET_NL()NET_PRINT("		- Rank Title = ")NET_PRINT(g_Private_LocalPlayerCrew_RankTitle[I-1])
		
			IF IS_STRING_EMPTY_HUD(g_Private_LocalPlayerCrew_RankTitle[I-1])
				g_Private_LocalPlayerCrew_RankTitle[I-1] = GET_FILENAME_FOR_AUDIO_CONVERSATION("HUD_NOCREWTIT")
			ENDIF
		
		ENDFOR
		
		IF GotCrewColours
		AND GotAllRankTitles 
		
			g_Private_TriggerCrewMetaDataChange = FALSE
		
		ELSE
		
			g_Private_TriggerCrewMetaDataChange = FALSE
		
			NET_NL()NET_PRINT("SETUP_CREW_METADATA Returned False - ")
			IF GotCrewColours = FALSE
				NET_NL()NET_PRINT("			- GotCrewColours = FALSE ")
			ENDIF
			IF GotAllRankTitles = FALSE
				NET_NL()NET_PRINT("			- GotAllRankTitles = FALSE ")
			ENDIF
		
			
		ENDIF
		IF SHOULD_REFRESH_TITLES_AS_CREW_TITLES()
			g_Private_CrewTitleSettings_LastFrame = GET_MP_CHARACTER_TITLE_SETTING()
		ENDIF
		

		
		
	    NETWORK_CLAN_DESC PlayerClan 
		
     	IF IS_PLAYER_ONLINE()
           IF NETWORK_CLAN_SERVICE_IS_VALID()  
		   		GAMER_HANDLE gamerHandle = GET_GAMER_HANDLE_PLAYER(PLAYER_ID())
				IF NETWORK_CLAN_PLAYER_IS_ACTIVE(gamerHandle)
			
	                 NETWORK_CLAN_PLAYER_GET_DESC(PlayerClan, SIZE_OF(PlayerClan), gamerHandle)
	                 g_Private_Players_Current_crew_ID = PlayerClan.Id
					 NET_NL()NET_PRINT("g_Private_Players_Current_crew_ID = ")
					 NET_PRINT_INT(PlayerClan.Id)
				ENDIF
           ENDIF
     	ENDIF
		
		
		
	ENDIF

	
	
	
ENDPROC


PROC DELETE_CLAN_DATA(int clanid)
//NETWORK_CLAN_DESC PlayerClan
	IF clanid = g_MPPLY_CREW_0_ID
		NET_NL()NET_PRINT("DELETE_CLAN_DATA = 0 ")
		SET_HEAVILY_ACCESSED_MP_INT_PLAYER_STAT(MPPLY_CREW_0_ID, 0)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_CREW_LOCAL_TIME_0, 0)
		SET_HEAVILY_ACCESSED_MP_INT_PLAYER_STAT(MPPLY_CREW_LOCAL_XP_0, 0)
		SET_MP_INT_PLAYER_STAT(MPPLY_CREW_NO_HEISTS_0, 0)
		SET_MP_INT_PLAYER_STAT(MPPLY_CREW_HEIST_CASH_0, 0)
	ELIF clanid  = g_MPPLY_CREW_1_ID
		NET_NL()NET_PRINT("DELETE_CLAN_DATA = 1 ")
		SET_HEAVILY_ACCESSED_MP_INT_PLAYER_STAT(MPPLY_CREW_1_ID, 0)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_CREW_LOCAL_TIME_1, 0)
		SET_HEAVILY_ACCESSED_MP_INT_PLAYER_STAT(MPPLY_CREW_LOCAL_XP_1, 0)
		SET_MP_INT_PLAYER_STAT(MPPLY_CREW_NO_HEISTS_1, 0)
		SET_MP_INT_PLAYER_STAT(MPPLY_CREW_HEIST_CASH_1, 0)
	ELIF clanid  = g_MPPLY_CREW_2_ID
		NET_NL()NET_PRINT("DELETE_CLAN_DATA = 2 ")
		SET_HEAVILY_ACCESSED_MP_INT_PLAYER_STAT(MPPLY_CREW_2_ID, 0)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_CREW_LOCAL_TIME_2, 0)
		SET_HEAVILY_ACCESSED_MP_INT_PLAYER_STAT(MPPLY_CREW_LOCAL_XP_2, 0)
		SET_MP_INT_PLAYER_STAT(MPPLY_CREW_NO_HEISTS_2, 0)
		SET_MP_INT_PLAYER_STAT(MPPLY_CREW_HEIST_CASH_2, 0)
	ELIF clanid  = g_MPPLY_CREW_3_ID
		NET_NL()NET_PRINT("DELETE_CLAN_DATA = 3 ")
		SET_HEAVILY_ACCESSED_MP_INT_PLAYER_STAT(MPPLY_CREW_3_ID, 0)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_CREW_LOCAL_TIME_3, 0)
		SET_HEAVILY_ACCESSED_MP_INT_PLAYER_STAT(MPPLY_CREW_LOCAL_XP_3, 0)
		SET_MP_INT_PLAYER_STAT(MPPLY_CREW_NO_HEISTS_3, 0)
		SET_MP_INT_PLAYER_STAT(MPPLY_CREW_HEIST_CASH_3, 0)
	ELIF clanid  = g_MPPLY_CREW_4_ID
		NET_NL()NET_PRINT("DELETE_CLAN_DATA = 4 ")
		SET_HEAVILY_ACCESSED_MP_INT_PLAYER_STAT(MPPLY_CREW_4_ID, 0)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_CREW_LOCAL_TIME_4, 0)
		SET_HEAVILY_ACCESSED_MP_INT_PLAYER_STAT(MPPLY_CREW_LOCAL_XP_4, 0)
		SET_MP_INT_PLAYER_STAT(MPPLY_CREW_NO_HEISTS_4, 0)
		SET_MP_INT_PLAYER_STAT(MPPLY_CREW_HEIST_CASH_4, 0)
		
		
	ENDIF
	g_bcheckstartingcrewxp = FALSE
ENDPROC


// checks to see if player is in crew. If not, clear the local stat
/*
PROC HAS_PLAYER_LEFT_CREW()
	//GAMER_HANDLE player_handle 
	NETWORK_CLAN_DESC PlayerClan0	
	NETWORK_CLAN_DESC PlayerClan1	
	NETWORK_CLAN_DESC PlayerClan2	
	NETWORK_CLAN_DESC PlayerClan3	
	NETWORK_CLAN_DESC PlayerClan4
	
	PlayerClan0.Id = GET_MP_INT_PLAYER_STAT( MPPLY_CREW_0_ID)
	PlayerClan1.Id = GET_MP_INT_PLAYER_STAT( MPPLY_CREW_1_ID)
	PlayerClan2.Id = GET_MP_INT_PLAYER_STAT( MPPLY_CREW_2_ID)
	PlayerClan3.Id = GET_MP_INT_PLAYER_STAT( MPPLY_CREW_3_ID)
	PlayerClan4.Id = GET_MP_INT_PLAYER_STAT( MPPLY_CREW_4_ID)
	
	
	
	//player_handle = GET_GAMER_HANDLE_PLAYER(PLAYER_ID())

	
	IF IS_PLAYER_ONLINE()
	    IF NETWORK_CLAN_SERVICE_IS_VALID()  
			//number_of_members_in_clan = NETWORK_CLAN_GET_LOCAL_MEMBERSHIPS_COUNT()
			//INT i
			//IF number_of_members_in_clan > 0
				//REPEAT number_of_members_in_clan i
					IF PlayerClan0.Id != 0
						IF NOT IS_PLAYER_ALREADY_IN_THIS_CLAN(PlayerClan0.Id) //IS_PLAYER_IN_PLAYERS_ACTIVE_CLAN
							// reset clan data
							DELETE_CLAN_DATA(PlayerClan0.Id)
							
						ENDIF
					ELSE
						NET_NL()NET_PRINT("HAS_PLAYER_LEFT_CREW PlayerClan0.Id = 0 ")
					ENDIF
					
					
					IF PlayerClan1.Id != 0
						IF NOT IS_PLAYER_ALREADY_IN_THIS_CLAN(PlayerClan1.Id) //IS_PLAYER_IN_PLAYERS_ACTIVE_CLAN
							// reset clan data
							DELETE_CLAN_DATA(PlayerClan1.Id)
							
						ENDIF
					ELSE
						NET_NL()NET_PRINT("HAS_PLAYER_LEFT_CREW PlayerClan1.Id = 0 ")
					ENDIF
					
					IF  PlayerClan2.Id != 0
						IF NOT IS_PLAYER_ALREADY_IN_THIS_CLAN(PlayerClan2.Id) //IS_PLAYER_IN_PLAYERS_ACTIVE_CLAN
							// reset clan data
							DELETE_CLAN_DATA(PlayerClan2.Id)
							
						ENDIF
					ELSE
						NET_NL()NET_PRINT("HAS_PLAYER_LEFT_CREW PlayerClan2.Id = 0 ")
					ENDIF
					
					IF  PlayerClan3.Id != 0
						IF NOT IS_PLAYER_ALREADY_IN_THIS_CLAN(PlayerClan3.Id) //IS_PLAYER_IN_PLAYERS_ACTIVE_CLAN
							// reset clan data
							DELETE_CLAN_DATA(PlayerClan3.Id)
							
						ENDIF
					ELSE
						NET_NL()NET_PRINT("HAS_PLAYER_LEFT_CREW PlayerClan3.Id = 0 ")
					ENDIF
					
										
					IF  PlayerClan4.Id != 0
						IF NOT IS_PLAYER_ALREADY_IN_THIS_CLAN(PlayerClan4.Id) //IS_PLAYER_IN_PLAYERS_ACTIVE_CLAN
							// reset clan data
							DELETE_CLAN_DATA(PlayerClan4.Id)
							
						ENDIF
					ELSE
						NET_NL()NET_PRINT("HAS_PLAYER_LEFT_CREW PlayerClan4.Id = 0 ")
					ENDIF
					
				//ENDREPEAT
			//ENDIF
		ENDIF
	ENDIF


ENDPROC

*/


// checks to see if player is in crew. If not, clear the local stat
PROC PROCESS_PLAYER_LEFT_CREW_EVENT()
	//GAMER_HANDLE player_handle 
	NETWORK_CLAN_DESC PlayerClan0	
	NETWORK_CLAN_DESC PlayerClan1	
	NETWORK_CLAN_DESC PlayerClan2	
	NETWORK_CLAN_DESC PlayerClan3	
	NETWORK_CLAN_DESC PlayerClan4
	
	PlayerClan0.Id = g_MPPLY_CREW_0_ID
	PlayerClan1.Id = g_MPPLY_CREW_1_ID
	PlayerClan2.Id = g_MPPLY_CREW_2_ID
	PlayerClan3.Id = g_MPPLY_CREW_3_ID
	PlayerClan4.Id = g_MPPLY_CREW_4_ID
	
	
	//player_handle = GET_GAMER_HANDLE_PLAYER(PLAYER_ID())
	

	
	
	//EVENT_NAMES ThisScriptEvent
	//STRUCT_PED_LEFT_BEHIND Data
	
	//STRUCT_ENTITY_DAMAGE_EVENT damage_event_data

//		ThisScriptEvent = GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_NETWORK, iEventID)
		
//		IF ThisScriptEvent = EVENT_NETWORK_CLAN_LEFT	

    IF IS_PLAYER_ONLINE()
        IF NETWORK_CLAN_SERVICE_IS_VALID()  
			//number_of_members_in_clan = NETWORK_CLAN_GET_LOCAL_MEMBERSHIPS_COUNT()
			//INT i
			//IF number_of_members_in_clan > 0
    			//REPEAT number_of_members_in_clan i
					IF  PlayerClan0.Id != 0
						IF NOT IS_PLAYER_ALREADY_IN_THIS_CLAN(PlayerClan0.Id) //IS_PLAYER_IN_PLAYERS_ACTIVE_CLAN
							// reset clan data
							DELETE_CLAN_DATA(PlayerClan0.Id)
							
						ENDIF
					ELSE
						NET_NL()NET_PRINT("HAS_PLAYER_LEFT_CREW PlayerClan0.Id = 0 ")
					ENDIF
					
					
					IF  PlayerClan1.Id != 0
						IF NOT IS_PLAYER_ALREADY_IN_THIS_CLAN(PlayerClan1.Id) //IS_PLAYER_IN_PLAYERS_ACTIVE_CLAN
							// reset clan data
							DELETE_CLAN_DATA(PlayerClan1.Id)
							
						ENDIF
					ELSE
						NET_NL()NET_PRINT("HAS_PLAYER_LEFT_CREW PlayerClan1.Id = 0 ")
					ENDIF
					
					IF  PlayerClan2.Id != 0
						IF NOT IS_PLAYER_ALREADY_IN_THIS_CLAN(PlayerClan2.Id) //IS_PLAYER_IN_PLAYERS_ACTIVE_CLAN
							// reset clan data
							DELETE_CLAN_DATA(PlayerClan2.Id)
							
						ENDIF
					ELSE
						NET_NL()NET_PRINT("HAS_PLAYER_LEFT_CREW PlayerClan2.Id = 0 ")
					ENDIF
					
					IF  PlayerClan3.Id != 0
						IF NOT IS_PLAYER_ALREADY_IN_THIS_CLAN(PlayerClan3.Id) //IS_PLAYER_IN_PLAYERS_ACTIVE_CLAN
							// reset clan data
							DELETE_CLAN_DATA(PlayerClan3.Id)
							
						ENDIF
					ELSE
						NET_NL()NET_PRINT("HAS_PLAYER_LEFT_CREW PlayerClan3.Id = 0 ")
					ENDIF
					
										
					IF  PlayerClan4.Id != 0
						IF NOT IS_PLAYER_ALREADY_IN_THIS_CLAN(PlayerClan4.Id) //IS_PLAYER_IN_PLAYERS_ACTIVE_CLAN
							// reset clan data
							DELETE_CLAN_DATA(PlayerClan4.Id)
							
						ENDIF
					ELSE
						NET_NL()NET_PRINT("HAS_PLAYER_LEFT_CREW PlayerClan4.Id = 0 ")
					ENDIF
					
				//ENDREPEAT
			//ENDIF
		ENDIF
	ENDIF
//		ENDIF

ENDPROC



PROC GIVE_CREW_XP()
//Use g_Private_PlayerCrewData so the player can't change just before being awarded XP



ENDPROC

PROC RESET_GET_FRIENDS_CREWS(FRIEND_CREW_DATA &CrewData[],FRIEND_CREW_DATA_CONTROL &control)
	INT i
	REPEAT FRIEND_CREW_DATA_MAX_CREWS i
		CrewData[i].iID = 0
		CrewData[i].Name = ""
		CrewData[i].Tag = ""
	ENDREPEAT
	CLEAR_GAMER_HANDLE_STRUCT(control.currentHandle)
	control.iStage = 0
	control.iCounter = 0
	control.bFinished = FALSE
	control.bUpdate = FALSE
	control.iCurrentFriendIndex = 0
	PRINTLN("RESET_GET_FRIENDS_CREWS: called from ", GET_THIS_SCRIPT_NAME())
ENDPROC

FUNC BOOL IS_CREW_ALREADY_IN_ARRAY(INT iCrewID)
	INT i
	REPEAT FRIEND_CREW_DATA_MAX_CREWS i
		IF globalFriendCrewData[i].iID = iCrewID
			RETURN TRUE
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

//FRIEND_CREW_DATA allFriendCrewData[NETWORK_GET_MAX_FRIENDS()]
PROC GET_FRIENDS_CREWS(FRIEND_CREW_DATA &CrewData[],FRIEND_CREW_DATA_CONTROL &control)
	NETWORK_CLAN_DESC clanDesc
	IF NOT control.bFinished
		IF IS_PLAYER_ONLINE()
			IF NETWORK_CLAN_SERVICE_IS_VALID()
				SWITCH control.iStage
					CASE 0
						control.currentHandle = GET_GAMER_HANDLE_FRIEND(control.iCurrentFriendIndex)
						IF IS_GAMER_HANDLE_VALID(control.currentHandle)
							IF NOT NETWORK_IS_GAMER_IN_MY_SESSION(control.currentHandle)
								NETWORK_CLAN_DOWNLOAD_MEMBERSHIP(control.currentHandle)
								PRINTLN("GET_FRIENDS_CREWS:  NETWORK_CLAN_DOWNLOAD_MEMBERSHIP for friend: ", NETWORK_GET_FRIEND_NAME(control.iCurrentFriendIndex))
								control.iStage = 1
								PRINTLN("GET_FRIENDS_CREWS: moving to stage 1")
							ELSE
								PRINTLN("GET_FRIENDS_CREWS: player is in same session grabbing data for friend: ", NETWORK_GET_FRIEND_NAME(control.iCurrentFriendIndex)) 
								NETWORK_CLAN_PLAYER_GET_DESC(clanDesc,SIZE_OF(clanDesc),control.currentHandle)
								IF NOT IS_CREW_ALREADY_IN_ARRAY(clanDesc.Id)
									IF control.iCounter < FRIEND_CREW_DATA_MAX_CREWS
										CrewData[control.iCounter].iID = clanDesc.Id
										CrewData[control.iCounter].Name = clanDesc.ClanName
										CrewData[control.iCounter].Tag = clanDesc.ClanTag
										
										PRINTLN("CrewData[",control.iCounter,"].iID = ", CrewData[control.iCounter].iID)
										PRINTLN("CrewData[",control.iCounter,"].Name = ", CrewData[control.iCounter].Name)
										PRINTLN("CrewData[",control.iCounter,"].Tag = ", CrewData[control.iCounter].Tag)
										control.iCounter++
										control.bUpdate = TRUE
									ENDIF
								ELSE
									PRINTLN("GET_FRIENDS_CREWS: crew ID ",clanDesc.Id, " is already stored in the array" )
								ENDIF
								PRINTLN("GET_FRIENDS_CREWS: moving to stage 2")
								control.iStage = 2
								
							ENDIF
						ELSE
							PRINTLN("GET_FRIENDS_CREWS:  IS_GAMER_HANDLE_VALID = False for friend: ", NETWORK_GET_FRIEND_NAME(control.iCurrentFriendIndex))	
							PRINTLN("GET_FRIENDS_CREWS: moving to stage 2")
							control.iStage = 2
						ENDIF
					BREAK
					
					CASE 1
						IF NOT NETWORK_CLAN_DOWNLOAD_MEMBERSHIP_PENDING(control.currentHandle)
							IF NETWORK_CLAN_GET_MEMBERSHIP_COUNT(control.currentHandle) > 0
								IF NETWORK_CLAN_GET_MEMBERSHIP_VALID(control.currentHandle, 0)
									IF NETWORK_CLAN_GET_MEMBERSHIP(control.currentHandle, clanDesc, -1)
										PRINTLN("GET_FRIENDS_CREWS: got membership data for friend: ", NETWORK_GET_FRIEND_NAME(control.iCurrentFriendIndex))
										IF NOT IS_CREW_ALREADY_IN_ARRAY(clanDesc.Id)
											IF control.iCounter < FRIEND_CREW_DATA_MAX_CREWS
												
												CrewData[control.iCounter].iID = clanDesc.Id
												CrewData[control.iCounter].Name = clanDesc.ClanName
												CrewData[control.iCounter].Tag = clanDesc.ClanTag
												
												PRINTLN("CrewData[",control.iCounter,"].iID = ", CrewData[control.iCounter].iID)
												PRINTLN("CrewData[",control.iCounter,"].Name = ", CrewData[control.iCounter].Name)
												PRINTLN("CrewData[",control.iCounter,"].Tag = ", CrewData[control.iCounter].Tag)
												control.iCounter++
												control.bUpdate = TRUE
											ENDIF
										ELSE
											PRINTLN("GET_FRIENDS_CREWS: crew ID ",clanDesc.Id, " is already stored in the array" )
										ENDIF
										PRINTLN("GET_FRIENDS_CREWS: moving to stage 2")
										
										
										control.iStage = 2
									ELSE
										PRINTLN("GET_FRIENDS_CREWS: NETWORK_CLAN_GET_MEMBERSHIP() failed for friend: ", NETWORK_GET_FRIEND_NAME(control.iCurrentFriendIndex))
										PRINTLN("GET_FRIENDS_CREWS: moving to stage 2")
										control.iStage = 2
									ENDIF
								ELSE
									PRINTLN("GET_FRIENDS_CREWS: NETWORK_CLAN_GET_MEMBERSHIP_VALID failed for friend: ", NETWORK_GET_FRIEND_NAME(control.iCurrentFriendIndex))
									PRINTLN("GET_FRIENDS_CREWS: moving to stage 2")
									control.iStage = 2
								ENDIF
							ELSE
								PRINTLN("GET_FRIENDS_CREWS: NETWORK_CLAN_GET_MEMBERSHIP_COUNT is 0 for friend: ", NETWORK_GET_FRIEND_NAME(control.iCurrentFriendIndex))
								PRINTLN("GET_FRIENDS_CREWS: moving to stage 2")
								control.iStage = 2
							ENDIF
						ELSE
							PRINTLN("GET_FRIENDS_CREWS: waiting for NETWORK_CLAN_DOWNLOAD_MEMBERSHIP_PENDING for friend: ", NETWORK_GET_FRIEND_NAME(control.iCurrentFriendIndex))
//							PRINTLN("GET_FRIENDS_CREWS: moving to stage 2")
//							control.iStage = 2
						ENDIF
					BREAK
					
					CASE 2
						control.iCurrentFriendIndex++
						IF control.iCurrentFriendIndex >= NETWORK_GET_FRIEND_COUNT()
						OR control.iCounter >= FRIEND_CREW_DATA_MAX_CREWS
							control.bFinished = TRUE
							control.bUpdate = TRUE
							PRINTLN("GET_FRIENDS_CREWS: finished")
							//control.iCounter = 0
						ELSE
							control.iStage = 0
						ENDIF
					BREAK
				ENDSWITCH
			ELSE
				PRINTLN("GET_FRIENDS_CREWS: NETWORK_CLAN_SERVICE_IS_VALID() = FALSE - failed")
				control.bFinished = TRUE
				control.bUpdate = TRUE
			ENDIF
		ELSE
			PRINTLN("GET_FRIENDS_CREWS: IS_PLAYER_ONLINE() = FALSE - failed")
			control.bFinished = TRUE
			control.bUpdate = TRUE
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL GET_CREW_TAG_FOR_GAMER(INT &iLoadStage,GAMER_HANDLE &gamerHandle, TEXT_LABEL_15 &Tag)
	NETWORK_CLAN_DESC memberInfo
	GAMER_HANDLE localHandle
	SWITCH iLoadStage
		CASE 0
			IF IS_GAMER_HANDLE_VALID(gamerHandle)
				localHandle = GET_GAMER_HANDLE_PLAYER(PLAYER_ID())
				IF NETWORK_ARE_HANDLES_THE_SAME(gamerHandle,localHandle)
					GET_GAMER_HANDLE_CREW_TAG_FOR_SCALEFORM(gamerHandle,Tag)
					iLoadStage = 2
					PRINTLN("GET_CREW_TAG_FOR_GAMER: LOCAL PLAYER got crew tag: ",Tag)
				ELSE
					IF NOT NETWORK_IS_GAMER_IN_MY_SESSION(gamerHandle)
						IF NOT NETWORK_CLAN_ANY_DOWNLOAD_MEMBERSHIP_PENDING()
							IF NETWORK_CLAN_REMOTE_MEMBERSHIPS_ARE_IN_CACHE(gamerHandle)
								PRINTLN("GET_CREW_TAG_FOR_GAMER: crew data is in cache ")
								IF NETWORK_CLAN_GET_MEMBERSHIP_COUNT(gamerHandle) > 0
									IF NETWORK_CLAN_GET_MEMBERSHIP_VALID(gamerHandle,0)
										IF NETWORK_CLAN_GET_MEMBERSHIP(gamerHandle, memberInfo, -1)
											NETWORK_CLAN_GET_UI_FORMATTED_TAG_EZ(memberInfo, Tag)
											PRINTLN("GET_CREW_TAG_FOR_GAMER: got crew tag: ",Tag)
										ENDIF
									ENDIF
								ENDIF
								iLoadStage = 2
								RETURN TRUE
							ELSE
								IF NETWORK_CLAN_DOWNLOAD_MEMBERSHIP(gamerHandle)
									iLoadStage = 1
									PRINTLN("GET_CREW_TAG_FOR_GAMER: downloading crew data ")
								ENDIF
							ENDIF
						ENDIF
					ELSE
						GET_GAMER_HANDLE_CREW_TAG_FOR_SCALEFORM(gamerHandle,Tag)
						iLoadStage = 2
						PRINTLN("GET_CREW_TAG_FOR_GAMER: IN SESSION got crew tag: ",Tag)
					ENDIF
				ENDIF
			ELSE
				iLoadStage = 2
				PRINTLN("GET_CREW_TAG_FOR_GAMER: gamer not valid: ",Tag)
			ENDIF
		BREAK

		CASE 1
			IF NOT NETWORK_IS_GAMER_IN_MY_SESSION(gamerHandle)
				IF NOT NETWORK_CLAN_DOWNLOAD_MEMBERSHIP_PENDING(gamerHandle)
					IF NETWORK_CLAN_GET_MEMBERSHIP_COUNT(gamerHandle) > 0
						IF NETWORK_CLAN_GET_MEMBERSHIP_VALID(gamerHandle,0)
							IF NETWORK_CLAN_GET_MEMBERSHIP(gamerHandle, memberInfo, -1)
								NETWORK_CLAN_GET_UI_FORMATTED_TAG_EZ(memberInfo, Tag)
								PRINTLN("GET_CREW_TAG_FOR_GAMER: got crew tag: ",Tag)
							ENDIF
						ENDIF
					ENDIF
					iLoadStage = 2
					RETURN TRUE
				ELSE
					PRINTLN("GET_CREW_TAG_FOR_GAMER: download membership pending")
				ENDIF
			ELSE
				GET_GAMER_HANDLE_CREW_TAG_FOR_SCALEFORM(gamerHandle,Tag)
				iLoadStage = 2
				PRINTLN("GET_CREW_TAG_FOR_GAMER: IN SESSION got crew tag: ",Tag)
			ENDIF
		BREAK
		
		
		CASE 2
			RETURN TRUE
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Gets crew tags for a group of gamer handles at a time
/// PARAMS:
///    iLoadStage - stage of getting data
///    iNumGamers - number of gamers 
///    gamerHandles - array of gamer handles
///    returnData - the data returned includes crew tag
/// RETURNS:
///    TRUE is returned when the process is finished. Note returns true if the query fails!
FUNC BOOL GET_CREW_TAGS_FOR_GAMERS(INT &iLoadStage,INT &iNumGamers,GAMERS_HANDLES &gamerHandles[],TEXT_LABEL_15 &Tags[])
	NETWORK_CLAN_DESC memberInfo
	//GAMER_HANDLE localHandle
	//#IF IS_DEBUG_BUILD
	INT i

	//#ENDIF
	SWITCH iLoadStage
		CASE 0
			IF NETWORK_GET_PRIMARY_CLAN_DATA_PENDING()
				PRINTLN("GET_CREW_TAGS_FOR_GAMERS: waiting for another pending request to be completed??")
			ELSE
				NETWORK_GET_PRIMARY_CLAN_DATA_CLEAR()
				NETWORK_GET_PRIMARY_CLAN_DATA_START(gamerHandles,iNumGamers)
				iLoadStage = 1
				PRINTLN("GET_CREW_TAGS_FOR_GAMERS: NETWORK_GET_PRIMARY_CLAN_DATA_START called from ",GET_THIS_SCRIPT_NAME(),". Moving to stage 1")
			ENDIF
		BREAK

		CASE 1
			IF NOT NETWORK_GET_PRIMARY_CLAN_DATA_PENDING()
				IF NETWORK_GET_PRIMARY_CLAN_DATA_SUCCESS()
					iLoadStage = 2
					PRINTLN("GET_CREW_TAGS_FOR_GAMERS: NETWORK_GET_PRIMARY_CLAN_DATA_SUCCESS succeeded moving to stage 2")
				ELSE
					iLoadStage = 3
					PRINTLN("GET_CREW_TAGS_FOR_GAMERS: NETWORK_GET_PRIMARY_CLAN_DATA_SUCCESS FALSE!")
				ENDIF
			ELSE
			
			ENDIF
		BREAK
		
		CASE 2
			REPEAT iNumGamers i
				IF NETWORK_GET_PRIMARY_CLAN_DATA_NEW(gamerHandles[i].m_GamerHandle,memberInfo)
					PRINTLN("GET_CREW_TAGS_FOR_GAMERS: NETWORK_GET_PRIMARY_CLAN_DATA succeeded")
					PRINTLN("memberInfo.m_ClanID = ",memberInfo.Id)
					PRINTLN("memberInfo.m_ClanName = ",memberInfo.ClanName)
					PRINTLN("memberInfo.m_ClanTag = ",memberInfo.ClanTag)
					NETWORK_CLAN_GET_UI_FORMATTED_TAG_EZ(memberInfo,Tags[i])
				ELSE
					PRINTLN("GET_CREW_TAGS_FOR_GAMERS: NETWORK_GET_PRIMARY_CLAN_DATA failed!")
				ENDIF
			ENDREPEAT
			IF NETWORK_GET_PRIMARY_CLAN_DATA_PENDING()
				NETWORK_GET_PRIMARY_CLAN_DATA_CANCEL()
			ELSE
				NETWORK_GET_PRIMARY_CLAN_DATA_CLEAR()
			ENDIF
			iLoadStage = 3
		BREAK
		
		CASE 3
			RETURN TRUE
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL GET_GAMER_NAME_FROM_USER_ID(INT &iLoadStage,TEXT_LABEL_63 UserID, TEXT_LABEL_63 &PlayerName)
	GAMER_HANDLE gamerHandle 
	IF iLoadStage = 2
		RETURN TRUE
	ELIF IS_STRING_NULL_OR_EMPTY(UserID)
		PRINTLN("GET_GAMER_NAME_FROM_USER_ID: null user ID aborting")
		iLoadStage = 2
		RETURN TRUE
	ENDIF
	NETWORK_HANDLE_FROM_USER_ID(UserID,gamerHandle,SIZE_OF(gamerHandle))
	#IF IS_DEBUG_BUILD
	DEBUG_PRINT_GAMER_HANDLE(gamerHandle)
	#ENDIF
	SWITCH iLoadStage
		CASE 0
			IF IS_GAMER_HANDLE_VALID(gamerHandle)
				PRINTLN("GET_GAMER_NAME_FROM_USER_ID: gamer is valid. User ID: ",UserID)
				
				IF NOT NETWORK_IS_GAMER_IN_MY_SESSION(gamerHandle)
					IF IS_XBOX360_VERSION()
					OR IS_XBOX_PLATFORM()
					OR IS_PLAYSTATION_PLATFORM()
						IF NETWORK_GAMERTAG_FROM_HANDLE_START(gamerHandle)
							iLoadStage = 1
							PRINTLN("GET_GAMER_NAME_FROM_USER_ID: downloading player name User ID: ",UserID)
						ENDIF
					ELSE
						PlayerName = NETWORK_GET_GAMERTAG_FROM_HANDLE(gamerHandle)
						IF IS_PS3_VERSION()
							PRINTLN("GET_GAMER_NAME_FROM_USER_ID: PS3 got name directly : ",PlayerName)
						ELIF IS_PLAYSTATION_PLATFORM()
							PRINTLN("GET_GAMER_NAME_FROM_USER_ID: PS4 got name directly : ",PlayerName)
						ELIF IS_PC_VERSION()
							PRINTLN("GET_GAMER_NAME_FROM_USER_ID: PC got name directly : ",PlayerName)
						ENDIF
						iLoadStage = 2
					ENDIF
				ELSE
					PlayerName = GET_PLAYER_NAME(NETWORK_GET_PLAYER_FROM_GAMER_HANDLE(gamerHandle) )
					PRINTLN("GET_GAMER_NAME_FROM_USER_ID: IN SESSION got name: ",PlayerName)
					iLoadStage = 2
				ENDIF	
			ELSE
				iLoadStage = 2
				PRINTLN("GET_GAMER_NAME_FROM_USER_ID: gamer not valid: User ID: ",UserID)
			ENDIF
		BREAK

		CASE 1
			IF NOT NETWORK_IS_GAMER_IN_MY_SESSION(gamerHandle)
				IF NOT NETWORK_GAMERTAG_FROM_HANDLE_PENDING()
					IF NETWORK_GAMERTAG_FROM_HANDLE_SUCCEEDED()
						PlayerName = NETWORK_GET_GAMERTAG_FROM_HANDLE(gamerHandle)
						PRINTLN("GET_GAMER_NAME_FROM_USER_ID: got player name: ",PlayerName)
					ELSE
						PRINTLN("GET_GAMER_NAME_FROM_USER_ID: get gamer tag from handle failed User ID: ",UserID)
					ENDIF
					iLoadStage = 2
					RETURN TRUE
				ENDIF
			ELSE
				PlayerName = GET_PLAYER_NAME(NETWORK_GET_PLAYER_FROM_GAMER_HANDLE(gamerHandle) )
				PRINTLN("GET_GAMER_NAME_FROM_USER_ID: IN SESSION got name: ",PlayerName)
				iLoadStage = 2
			ENDIF
		BREAK
		
		
		CASE 2
			RETURN TRUE
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL GET_GAMER_NAME_FROM_HANDLE(INT &iLoadStage,GAMER_HANDLE &gamerHandle, TEXT_LABEL_63 &PlayerName)
	IF iLoadStage = 2
		RETURN TRUE
	ENDIF
	SWITCH iLoadStage
		CASE 0
			IF IS_GAMER_HANDLE_VALID(gamerHandle)
				PRINTLN("GET_GAMER_NAME_FROM_HANDLE: gamer is valid. ")
				#IF IS_DEBUG_BUILD
				DEBUG_PRINT_GAMER_HANDLE(gamerHandle)
				#ENDIF
				IF NOT NETWORK_IS_GAMER_IN_MY_SESSION(gamerHandle)
					IF IS_XBOX_PLATFORM()
					OR IS_PLAYSTATION_PLATFORM()
						IF NETWORK_GAMERTAG_FROM_HANDLE_START(gamerHandle)
							iLoadStage = 1
							PRINTLN("GET_GAMER_NAME_FROM_HANDLE: downloading player name ")
						ENDIF
					ELIF IS_XBOX360_VERSION()
						IF NETWORK_GAMERTAG_FROM_HANDLE_START(gamerHandle)
							iLoadStage = 1
							PRINTLN("GET_GAMER_NAME_FROM_HANDLE: downloading player name ")
						ENDIF
					
					ELSE
						PlayerName = NETWORK_GET_GAMERTAG_FROM_HANDLE(gamerHandle)
						IF IS_PS3_VERSION()
							PRINTLN("GET_GAMER_NAME_FROM_HANDLE: PS3 got name directly : ",PlayerName)
						ELIF IS_PLAYSTATION_PLATFORM()
							PRINTLN("GET_GAMER_NAME_FROM_HANDLE: PS4 got name directly : ",PlayerName)
						ELIF IS_PC_VERSION()
							PRINTLN("GET_GAMER_NAME_FROM_HANDLE: PC got name directly : ",PlayerName)
						ENDIF
						iLoadStage = 2
					ENDIF
				ELSE
					PlayerName = GET_PLAYER_NAME(NETWORK_GET_PLAYER_FROM_GAMER_HANDLE(gamerHandle) )
					PRINTLN("GET_GAMER_NAME_FROM_HANDLE: IN SESSION got name: ",PlayerName)
					iLoadStage = 2
				ENDIF	
			ELSE
				iLoadStage = 2
				PRINTLN("GET_GAMER_NAME_FROM_HANDLE: gamer not valid: ")
			ENDIF
		BREAK

		CASE 1
			IF NOT NETWORK_IS_GAMER_IN_MY_SESSION(gamerHandle)
				IF NOT NETWORK_GAMERTAG_FROM_HANDLE_PENDING()
					IF NETWORK_GAMERTAG_FROM_HANDLE_SUCCEEDED()
						PlayerName = NETWORK_GET_GAMERTAG_FROM_HANDLE(gamerHandle)
						PRINTLN("GET_GAMER_NAME_FROM_HANDLE: got player name: ",PlayerName)
					ELSE
						PRINTLN("GET_GAMER_NAME_FROM_HANDLE: get gamer tag from handle")
					ENDIF
					iLoadStage = 2
					RETURN TRUE
				ENDIF
			ELSE
				PlayerName = GET_PLAYER_NAME(NETWORK_GET_PLAYER_FROM_GAMER_HANDLE(gamerHandle) )
				PRINTLN("GET_GAMER_NAME_FROM_HANDLE: IN SESSION got name: ",PlayerName)
				iLoadStage = 2
			ENDIF
		BREAK
		
		
		CASE 2
			RETURN TRUE
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Durango Only: gets the display name from handle
FUNC BOOL GET_DISPLAY_NAME_FROM_HANDLE(INT &iLoadStage,GAMER_HANDLE &gamerHandle, TEXT_LABEL_63 &PlayerName, TEXT_LABEL_63& tempNameArray[],INT &iRequestID)
	IF NOT IS_XBOX_PLATFORM()
	AND NOT IS_PLAYSTATION_PLATFORM()
		SCRIPT_ASSERT("GET_DISPLAY_NAME_FROM_HANDLE- is only supported on Durango- See Conor")
		DEBUG_PRINTCALLSTACK()
		PRINTLN("GET_DISPLAY_NAME_FROM_HANDLE- is only supported on Durango")
		RETURN TRUE
	ENDIF
	IF iLoadStage = 2
		RETURN TRUE
	ENDIF
	INT iResult
	GAMER_HANDLE gamerHandleArray[1]
	gamerHandleArray[0] = gamerHandle
	GAMER_HANDLE localHandle 
	SWITCH iLoadStage
		CASE 0
			tempNameArray[0] = ""
			IF IS_GAMER_HANDLE_VALID(gamerHandle)
				localHandle = GET_GAMER_HANDLE_PLAYER(PLAYER_ID())
				IF NETWORK_ARE_HANDLES_THE_SAME(localHandle,gamerHandle)
					PlayerName = GET_PLAYER_NAME(PLAYER_ID())
					PRINTLN("GET_DISPLAY_NAME_FROM_HANDLE: local player got name: ",PlayerName)
					iLoadStage = 2	
				ELIF NOT NETWORK_IS_GAMER_IN_MY_SESSION(gamerHandle)
					PRINTLN("GET_DISPLAY_NAME_FROM_HANDLE: gamer is valid. ")
					#IF IS_DEBUG_BUILD
					DEBUG_PRINT_GAMER_HANDLE(gamerHandle)
					#ENDIF

					iRequestID = NETWORK_DISPLAYNAMES_FROM_HANDLES_START(gamerHandleArray,1)	
					iLoadStage = 1
				ELSE
					PlayerName = GET_PLAYER_NAME(NETWORK_GET_PLAYER_FROM_GAMER_HANDLE(gamerHandle) )
					PRINTLN("GET_DISPLAY_NAME_FROM_HANDLE: IN SESSION got name: ",PlayerName)
					iLoadStage = 2
				ENDIF
			ELSE
				iLoadStage = 2
				PRINTLN("GET_DISPLAY_NAME_FROM_HANDLE: gamer not valid: ")
			ENDIF
		BREAK

		CASE 1
			iResult = NETWORK_GET_DISPLAYNAMES_FROM_HANDLES(iRequestID,tempNameArray,1)
			PRINTLN("GET_DISPLAY_NAME_FROM_HANDLE playerNameArray[0] = ",tempNameArray[0])
			IF iResult = 0
				PRINTLN("GET_DISPLAY_NAME_FROM_HANDLE- SUCCEEDED!")
				PlayerName = tempNameArray[0]
				PRINTLN("GET_DISPLAY_NAME_FROM_HANDLE PlayerName = ",PlayerName)
				iLoadStage = 2
			ELIF iResult = -1
				PRINTLN("GET_DISPLAY_NAME_FROM_HANDLE- FAILED!")
				iLoadStage = 2
			ELSE
				 PRINTLN("GET_DISPLAY_NAME_FROM_HANDLE: waiting for display name request: iResult = ",iResult)
				 RETURN FALSE
			ENDIF
		BREAK

		CASE 2
			iRequestID = -1
			RETURN TRUE
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Durango Only: gets the display name from userID
FUNC BOOL GET_DISPLAY_NAME_FROM_USERID(INT &iLoadStage,TEXT_LABEL_63 &UserID, TEXT_LABEL_63 &PlayerName, TEXT_LABEL_63& tempNameArray[],INT &iRequestID)
	IF NOT IS_XBOX_PLATFORM()
	AND NOT IS_PLAYSTATION_PLATFORM()
		SCRIPT_ASSERT("GET_DISPLAY_NAME_FROM_USERID- is only supported on Durango- See Conor")
		DEBUG_PRINTCALLSTACK()
		PRINTLN("GET_DISPLAY_NAME_FROM_USERID- is only supported on Durango")
		RETURN TRUE
	ENDIF
	IF iLoadStage = 2
		RETURN TRUE
	ELIF IS_STRING_NULL_OR_EMPTY(UserID)
		PRINTLN("GET_DISPLAY_NAME_FROM_USERID: null user ID aborting")
		iLoadStage = 2
		RETURN TRUE
	ENDIF
	INT iResult
	GAMER_HANDLE gamerHandleArray[1]
	NETWORK_HANDLE_FROM_USER_ID(UserID,gamerHandleArray[0],SIZE_OF(gamerHandleArray[0]))
	SWITCH iLoadStage
		CASE 0
			tempNameArray[0] = ""
			IF IS_GAMER_HANDLE_VALID(gamerHandleArray[0])
				IF NOT NETWORK_IS_GAMER_IN_MY_SESSION(gamerHandleArray[0])
					PRINTLN("GET_DISPLAY_NAME_FROM_USERID: gamer is valid. ")
					#IF IS_DEBUG_BUILD
					DEBUG_PRINT_GAMER_HANDLE(gamerHandleArray[0])
					#ENDIF

					iRequestID = NETWORK_DISPLAYNAMES_FROM_HANDLES_START(gamerHandleArray,1)	
					iLoadStage = 1
				ELSE
					PlayerName = GET_PLAYER_NAME(NETWORK_GET_PLAYER_FROM_GAMER_HANDLE(gamerHandleArray[0]) )
					PRINTLN("GET_DISPLAY_NAME_FROM_USERID: IN SESSION got name: ",PlayerName)
					iLoadStage = 2
				ENDIF
			ELSE
				iLoadStage = 2
				PRINTLN("GET_DISPLAY_NAME_FROM_USERID: gamer not valid: ")
			ENDIF
		BREAK

		CASE 1
			iResult = NETWORK_GET_DISPLAYNAMES_FROM_HANDLES(iRequestID,tempNameArray,1)
			PRINTLN("GET_DISPLAY_NAME_FROM_USERID playerNameArray[0] = ",tempNameArray[0])
			IF iResult = 0
				PRINTLN("GET_DISPLAY_NAME_FROM_USERID- SUCCEEDED!")
				PlayerName = tempNameArray[0]
				PRINTLN("GET_DISPLAY_NAME_FROM_USERID PlayerName = ",PlayerName)
				iLoadStage = 2
			ELIF iResult = -1
				PRINTLN("GET_DISPLAY_NAME_FROM_USERID- FAILED!")
				iLoadStage = 2
			ELSE
				 PRINTLN("GET_DISPLAY_NAME_FROM_USERID: waiting for display name request: iResult = ",iResult)
				 RETURN FALSE
			ENDIF
		BREAK

		CASE 2
			iRequestID = -1
			RETURN TRUE
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL GET_CREW_INFO_FOR_GAMER(INT &iLoadStage, GAMER_HANDLE &gamerHandle, BOOL &bInCrew, TEXT_LABEL_15 &tlCrewTag, INT &iCrewColRed, INT &iCrewColGreen, INT &iCrewColBlue)
	
	NETWORK_CLAN_DESC clanDesc
	SWITCH iLoadStage
		CASE 0
			
			IF IS_GAMER_HANDLE_VALID(gamerHandle)
				
				IF NOT NETWORK_IS_GAMER_IN_MY_SESSION(gamerHandle)
					IF NOT NETWORK_CLAN_ANY_DOWNLOAD_MEMBERSHIP_PENDING()
					
						IF NETWORK_CLAN_REMOTE_MEMBERSHIPS_ARE_IN_CACHE(gamerHandle)
							iLoadStage = 1
							PRINTLN("GET_CREW_INFO_FOR_GAMER: crew data is in cache ")
						ELSE
							IF NETWORK_CLAN_DOWNLOAD_MEMBERSHIP(gamerHandle)
								iLoadStage = 1
								PRINTLN("GET_CREW_INFO_FOR_GAMER: downloading crew data ")
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF IS_PLAYER_IN_ACTIVE_CLAN(gamerHandle)
						NETWORK_CLAN_PLAYER_GET_DESC(clanDesc, SIZE_OF(clanDesc), gamerHandle)
						PRINTLN("GET_CREW_INFO_FOR_GAMER: IN SESSION got crew desc")
						
						NETWORK_CLAN_GET_UI_FORMATTED_TAG_EZ(clanDesc, tlCrewTag)
						iCrewColRed 	= clanDesc.ClanColor_Red
						iCrewColGreen 	= clanDesc.ClanColor_Green
						iCrewColBlue 	= clanDesc.ClanColor_Blue
						
						bInCrew = TRUE
					ELSE
						PRINTLN("GET_CREW_INFO_FOR_GAMER: IN SESSION not in crew")
						
						tlCrewTag = ""
						
						bInCrew = FALSE
					ENDIF
					
					iLoadStage = 2
					
				ENDIF
			ELSE
				tlCrewTag = ""
			
				iLoadStage = 2
				PRINTLN("GET_CREW_INFO_FOR_GAMER: gamer not valid")
			ENDIF
		BREAK

		CASE 1		
			IF IS_GAMER_HANDLE_VALID(gamerHandle)
			
				IF NETWORK_IS_GAMER_IN_MY_SESSION(gamerHandle)
					PRINTLN("GET_CREW_INFO_FOR_GAMER: gamer now in our session, move back to original state")
					iLoadStage = 0
				ELSE
			
					IF NOT NETWORK_CLAN_DOWNLOAD_MEMBERSHIP_PENDING(gamerHandle)
						IF NETWORK_CLAN_GET_MEMBERSHIP_COUNT(gamerHandle) > 0
							IF NETWORK_CLAN_GET_MEMBERSHIP_VALID(gamerHandle,0)
								IF NETWORK_CLAN_GET_MEMBERSHIP(gamerHandle, clanDesc, -1)
									PRINTLN("GET_CREW_INFO_FOR_GAMER: got crew desc")
									
									NETWORK_CLAN_GET_UI_FORMATTED_TAG_EZ(clanDesc, tlCrewTag)
									iCrewColRed 	= clanDesc.ClanColor_Red
									iCrewColGreen 	= clanDesc.ClanColor_Green
									iCrewColBlue 	= clanDesc.ClanColor_Blue
									
									bInCrew = TRUE
								ENDIF
							ENDIF
						ELSE
							bInCrew = FALSE
						ENDIF
						iLoadStage = 2
					ENDIF
				ENDIF
			ELSE
				tlCrewTag = ""
			
				iLoadStage = 2
				PRINTLN("GET_CREW_INFO_FOR_GAMER: gamer not valid (stage 1)")
			ENDIF
		BREAK
				
		CASE 2
			RETURN TRUE
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC


FUNC BOOL RUN_FRIEND_IN_GAME_FEED(BOOL& DisplayedFriendInSessionFeed)

	CONST_INT MAX_NUM_FRIENDS_IN_FEED 9

	IF DisplayedFriendInSessionFeed = FALSE
		IF NOT IS_TRANSITION_ACTIVE()
	
			GAMER_HANDLE GamerHandle

			INT I
			INT iNumberOfFriendsFound = 0
			PLAYER_INDEX aPlayer
			TEXT_LABEL_63 FriendList[NUM_NETWORK_PLAYERS] 
			BOOL UseLargeVersion = FALSE
			
			FOR I = 0 TO NUM_NETWORK_PLAYERS-1
				aPlayer = INT_TO_NATIVE(PLAYER_INDEX, I)
			
				IF IS_NET_PLAYER_OK(aPlayer, FALSE)
					GamerHandle = GET_GAMER_HANDLE_PLAYER(aPlayer)
					IF NETWORK_IS_FRIEND(GamerHandle)
					AND NOT IS_PLAYER_SCTV(aPlayer)
						iNumberOfFriendsFound++
						
						FriendList[I] += GET_PLAYER_NAME(aPlayer)
						
					ELSE
						FriendList[I] += ""	
					ENDIF
					
					IF iNumberOfFriendsFound >= MAX_NUM_FRIENDS_IN_FEED
						UseLargeVersion = TRUE
					
					ENDIF
					
				ENDIF
			ENDFOR
			
			g_i_NumberOfFriendsInSessionFeed = iNumberOfFriendsFound
			
			IF iNumberOfFriendsFound > 0

				IF g_bHasEnteredFreshGameFromSP
					REQUEST_SYSTEM_ACTIVITY_TYPE_PLAYING_WITH_FRIENDS()
					g_bHasEnteredFreshGameFromSP = FALSE
				ENDIF
				
				IF UseLargeVersion = FALSE
					TEXT_LABEL_31 StringToUse = "TICK_FRIENDMET_"
					StringToUse += iNumberOfFriendsFound

					BEGIN_TEXT_COMMAND_THEFEED_POST(StringToUse)
					
						FOR I = 0 TO NUM_NETWORK_PLAYERS-1
						
							NET_NL()NET_PRINT("RUN_FRIEND_IN_GAME_FEED: FriendList = ")NET_PRINT(FriendList[I])
						
							IF NOT IS_STRING_NULL_OR_EMPTY(FriendList[I])
								ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(APPEND_TAGS_TO_PLAYER_NAME(FriendList[I]))
							ENDIF
						ENDFOR
					
						
					
					END_TEXT_COMMAND_THEFEED_POST_TICKER(TRUE)	
					
					NET_NL()NET_PRINT("RUN_FRIEND_IN_GAME_FEED: iNumberOfFriendsFound = ")NET_PRINT_INT(iNumberOfFriendsFound)

					
				ELSE
				NET_NL()NET_PRINT("RUN_FRIEND_IN_GAME_FEED: Using the large version iNumberOfFriendsFound ")NET_PRINT_INT(iNumberOfFriendsFound)
					BEGIN_TEXT_COMMAND_THEFEED_POST("TICK_FRIENDMET_L")
					END_TEXT_COMMAND_THEFEED_POST_TICKER(TRUE)	
				
				ENDIF
			ELSE
				NET_NL()NET_PRINT("RUN_FRIEND_IN_GAME_FEED: calling nothing as iNumberOfFriendsFound = ")NET_PRINT_INT(iNumberOfFriendsFound)

			ENDIF
			
			 
			DisplayedFriendInSessionFeed  =TRUE
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC


FUNC BOOL RUN_FRIEND_IN_ONLINE_WORLD_FEED(BOOL& DisplayedFriendInOnlineFeed)



	IF DisplayedFriendInOnlineFeed = FALSE
	AND g_b_RunFriendsInSessionFeed = TRUE
	
		IF NOT IS_TRANSITION_ACTIVE()
	
			INT I
			INT iNumberOfFriendsFound = 0
			TEXT_LABEL_63 FriendName 
			INT NumberOfFriends = NETWORK_GET_FRIEND_COUNT()
						
			IF NumberOfFriends > 200
				NumberOfFriends = 200
			
			ELIF NumberOfFriends < 0
				NumberOfFriends = 0 
				
			ENDIF
			
			FOR I = 0 TO NumberOfFriends-1
				FriendName = NETWORK_GET_FRIEND_NAME(I)
				
				IF NETWORK_IS_FRIEND_IN_MULTIPLAYER(FriendName)
					NET_NL()NET_PRINT("RUN_FRIEND_IN_ONLINE_WORLD_FEED: Friend found in MP = ")NET_PRINT(FriendName)

					iNumberOfFriendsFound++
				ENDIF
			ENDFOR
			
			
			iNumberOfFriendsFound -= g_i_NumberOfFriendsInSessionFeed
			NET_NL()NET_PRINT("RUN_FRIEND_IN_ONLINE_WORLD_FEED: Friend found in session = ")NET_PRINT_INT(g_i_NumberOfFriendsInSessionFeed)

			
			IF iNumberOfFriendsFound > 0
			
				TEXT_LABEL_31 StringToUse
				
				IF iNumberOfFriendsFound = 1
					StringToUse = "TICK_FRIENDON1"
					BEGIN_TEXT_COMMAND_THEFEED_POST(StringToUse)
					END_TEXT_COMMAND_THEFEED_POST_TICKER(TRUE)	
					
				ELIF iNumberOfFriendsFound = 101
					StringToUse = "TICK_FRIEND101"
					BEGIN_TEXT_COMMAND_THEFEED_POST(StringToUse)
					END_TEXT_COMMAND_THEFEED_POST_TICKER(TRUE)
					
				ELSE
					StringToUse = "TICK_FRIENDON"
					BEGIN_TEXT_COMMAND_THEFEED_POST(StringToUse)
						ADD_TEXT_COMPONENT_INTEGER(iNumberOfFriendsFound)
					END_TEXT_COMMAND_THEFEED_POST_TICKER(TRUE)	
				ENDIF

				
			ENDIF
			
			NET_NL()NET_PRINT("RUN_FRIEND_IN_ONLINE_WORLD_FEED: iNumberOfFriendsFound = ")NET_PRINT_INT(iNumberOfFriendsFound)
			 
			g_i_NumberOfFriendsInSessionFeed = 0 
			 
			DisplayedFriendInOnlineFeed  =TRUE
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC



FUNC BOOL RUN_CHAT_RESTRICTION_MESSAGE(SCRIPT_TIMER& Atime)


	IF IS_PLAYSTATION_PLATFORM()
		IF g_b_Check_Chat_Restrictions_For_Message = FALSE
			IF NOT IS_TRANSITION_ACTIVE()
			AND NOT IS_PLAYER_SWITCH_IN_PROGRESS()
				IF HAS_NET_TIMER_EXPIRED_ONE_FRAME(Atime, 2000)
		
		     		// I have privileges but someone else who is signed in does not. Make the magic happen. 
					//Commented out due to 2021766
					//IF (NETWORK_CHECK_COMMUNICATION_PRIVILEGES() AND NETWORK_CHECK_COMMUNICATION_PRIVILEGES(PC_EVERYONE, GAMER_INDEX_ANYONE, FALSE))
					IF NETWORK_CHECK_COMMUNICATION_PRIVILEGES() = FALSE //OR I don't have chat turned on.
					#IF IS_DEBUG_BUILD
					OR GET_COMMANDLINE_PARAM_EXISTS("sc_RunChatMessage")
					#ENDIF
					
//						INT PlayerNum = NATIVE_TO_INT(PLAYER_ID())
						
						NET_NL()NET_PRINT("RUN_CHAT_RESTRICTION_MESSAGE: NETWORK_SHOW_CHAT_RESTRICTION_MSC called PLAYER_ID() = ")NET_PRINT(GET_PLAYER_NAME(PLAYER_ID()))
						NET_NL()NET_PRINT("RUN_CHAT_RESTRICTION_MESSAGE: NETWORK_CHECK_COMMUNICATION_PRIVILEGES(PC_EVERYONE, GAMER_INDEX_ANYONE, FALSE)  = ")NET_PRINT_BOOL(NETWORK_CHECK_COMMUNICATION_PRIVILEGES(PC_EVERYONE, GAMER_INDEX_ANYONE, FALSE) )
						NET_NL()NET_PRINT("RUN_CHAT_RESTRICTION_MESSAGE: NETWORK_CHECK_COMMUNICATION_PRIVILEGES()  = ")NET_PRINT_BOOL(NETWORK_CHECK_COMMUNICATION_PRIVILEGES())
						
						NETWORK_SHOW_CHAT_RESTRICTION_MSC(PLAYER_ID())
					ELSE
					
						NET_NL()NET_PRINT("RUN_CHAT_RESTRICTION_MESSAGE: DO NOT DISPLAY NETWORK_SHOW_CHAT_RESTRICTION_MSC called PLAYER_ID() = ")NET_PRINT(GET_PLAYER_NAME(PLAYER_ID()))
						NET_NL()NET_PRINT("RUN_CHAT_RESTRICTION_MESSAGE: DO NOT DISPLAY NETWORK_CHECK_COMMUNICATION_PRIVILEGES(PC_EVERYONE, GAMER_INDEX_ANYONE, FALSE)  = ")NET_PRINT_BOOL(NETWORK_CHECK_COMMUNICATION_PRIVILEGES(PC_EVERYONE, GAMER_INDEX_ANYONE, FALSE) )
						NET_NL()NET_PRINT("RUN_CHAT_RESTRICTION_MESSAGE: DO NOT DISPLAY NETWORK_CHECK_COMMUNICATION_PRIVILEGES()  = ")NET_PRINT_BOOL(NETWORK_CHECK_COMMUNICATION_PRIVILEGES())

					ENDIF
					
					 
					g_b_Check_Chat_Restrictions_For_Message  =TRUE
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ELSE	
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL RUN_UGC_RESTRICTION_MESSAGE(SCRIPT_TIMER& Atime)


	IF IS_PLAYSTATION_PLATFORM()
		IF g_b_Check_UGC_Restrictions_For_Message = FALSE
			IF NOT IS_TRANSITION_ACTIVE()
			AND NOT IS_PLAYER_SWITCH_IN_PROGRESS()
				IF HAS_NET_TIMER_EXPIRED_ONE_FRAME(Atime, 3000)
		
		     		// I have privileges but someone else who is signed in does not. Make the magic happen. 
					//IF (NETWORK_CHECK_USER_CONTENT_PRIVILEGES() AND NETWORK_CHECK_USER_CONTENT_PRIVILEGES(PC_EVERYONE, GAMER_INDEX_ANYONE, FALSE))
					//Commented out due to 2021766
					IF NETWORK_CHECK_USER_CONTENT_PRIVILEGES() = FALSE //OR I don't have UGC turned on.
					#IF IS_DEBUG_BUILD
					OR GET_COMMANDLINE_PARAM_EXISTS("sc_RunUGCMessage")
					#ENDIF
					
//						INT PlayerNum = NATIVE_TO_INT(PLAYER_ID())
						
						NET_NL()NET_PRINT("RUN_UGC_RESTRICTION_MESSAGE: NETWORK_SHOW_PSN_UGC_RESTRICTION called PLAYER_ID() = ")NET_PRINT(GET_PLAYER_NAME(PLAYER_ID()))
						NET_NL()NET_PRINT("RUN_UGC_RESTRICTION_MESSAGE: NETWORK_CHECK_COMMUNICATION_PRIVILEGES(PC_EVERYONE, GAMER_INDEX_ANYONE, FALSE)  = ")NET_PRINT_BOOL(NETWORK_CHECK_COMMUNICATION_PRIVILEGES(PC_EVERYONE, GAMER_INDEX_ANYONE, FALSE) )
						NET_NL()NET_PRINT("RUN_UGC_RESTRICTION_MESSAGE: NETWORK_CHECK_COMMUNICATION_PRIVILEGES()  = ")NET_PRINT_BOOL(NETWORK_CHECK_COMMUNICATION_PRIVILEGES())
						
						NETWORK_SHOW_PSN_UGC_RESTRICTION()
					ELSE
					
						NET_NL()NET_PRINT("RUN_UGC_RESTRICTION_MESSAGE: DO NOT DISPLAY NETWORK_SHOW_CHAT_RESTRICTION_MSC called PLAYER_ID() = ")NET_PRINT(GET_PLAYER_NAME(PLAYER_ID()))
						NET_NL()NET_PRINT("RUN_UGC_RESTRICTION_MESSAGE: DO NOT DISPLAY NETWORK_CHECK_COMMUNICATION_PRIVILEGES(PC_EVERYONE, GAMER_INDEX_ANYONE, FALSE)  = ")NET_PRINT_BOOL(NETWORK_CHECK_COMMUNICATION_PRIVILEGES(PC_EVERYONE, GAMER_INDEX_ANYONE, FALSE) )
						NET_NL()NET_PRINT("RUN_UGC_RESTRICTION_MESSAGE: DO NOT DISPLAY NETWORK_CHECK_COMMUNICATION_PRIVILEGES()  = ")NET_PRINT_BOOL(NETWORK_CHECK_COMMUNICATION_PRIVILEGES())

					ENDIF
					
					 
					g_b_Check_UGC_Restrictions_For_Message  =TRUE
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ELSE	
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC



