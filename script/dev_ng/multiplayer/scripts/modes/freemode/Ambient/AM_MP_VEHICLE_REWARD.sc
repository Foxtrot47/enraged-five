//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Name:        AM_MP_VEHICLE_REWARD.sc																				
///																													
/// Description:  Maintain player's vehicle reward. Checks if player should rewarded a vehicel. Spawn and change 	
///              the vehicle to PV.   																				
///	USAGE:
///    	1. Add new reward to VEHICLE_REWARD enum in "net_vehicle_reward.sch"
///     2. Add vehicle awarded flag/stat to GET_IS_VEHICLE_REWARDED and SET_VEHICLE_REWARDED
///     3. Add vehicle model to GET_REWARD_VEHICLE_MODEL
///     4. Add spawn coords to GET_ACTUAL_REWARD_VEHICLE_COORD_AND_HEADING and update GET_MAX_SPAWN_POINTS_FOR_REWARD_VEHICLE
///     5. Add unlock conditions to SHOULD_REWARD_SPECIFIC_VEHICLE
///    	6. Add blip setup to SETUP_REWARD_VEHICLE_HIDDEN_BLIP and GET_REWARD_VEHICLE_BLIP_COLOUR
///   	7. Add help text to PRINT_VEHICLE_REWARDED_HELP_TEXT
///     8. (optional) Any phonecall logic can be run from MAINTAIN_UNLOCK_CALLS if required
///																													
/// Written by:  Ata, Tom Turner																									
/// Date:  		20/05/2019																							
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "net_include.sch"
USING "net_events.sch"
USING "net_spawn.sch"
USING "net_ambience.sch"
USING "net_realty_new.sch"
USING "charsheet_global_definitions.sch"
USING "net_purchase_vehicle.sch"

#IF FEATURE_CASINO

USING "net_vehicle_reward.sch"

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ STRUCTS ╞════════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

STRUCT VEH_REWARD_SERVER_DATA	
	INT iBS
ENDSTRUCT

STRUCT VEH_REWARD_PLAYER_DATA											
	INT iBS		
	NETWORK_INDEX rewardVeh
ENDSTRUCT

#IF IS_DEBUG_BUILD
STRUCT VEH_REWARD_DEBUG_DATA
	BOOL bSetSelectedVehicleAsPV, bSetVehPedIsInAsPV, bAddModValue
	BOOL bEnableVehReward[COUNT_OF(VEHICLE_REWARD)]
	BOOL bClearRewardStat[COUNT_OF(VEHICLE_REWARD)]
ENDSTRUCT
#ENDIF

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════════════╡ VARIABLES  ╞══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

VEH_REWARD_SERVER_DATA serverBD
VEH_REWARD_PLAYER_DATA playerBD[NUM_NETWORK_PLAYERS]
VEHICLE_REWARD_DATA vehicleRewardData
REPLACE_MP_VEH_OR_PROP_MENU sReplaceMPSVData

#IF IS_DEBUG_BUILD
VEH_REWARD_DEBUG_DATA vehRewardDebugData
VEHICLE_PURCHASE_STRUCT vehPurchaseStruct
#ENDIF

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════════════╡ LOOK UPS   ╞══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

FUNC BOOL GET_IS_VEHICLE_REWARDED(VEHICLE_REWARD eReward)
	SWITCH eReward
		CASE VEHICLE_REWARD_CASINO	RETURN GET_MP_BOOL_CHARACTER_STAT(MP_STAT_CAS_VEHICLE_REWARD)
		#IF FEATURE_HEIST_ISLAND
		CASE VEHICLE_REWARD_KEINEMUSIK	RETURN GET_PACKED_STAT_BOOL(PACKED_MP_STAT_KEINEMUSIK_VEHICLE_REWARD)
		#ENDIF
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC SET_VEHICLE_REWARDED(VEHICLE_REWARD eReward, BOOL bRewarded)
	SWITCH eReward
		CASE VEHICLE_REWARD_CASINO	
			SET_MP_BOOL_CHARACTER_STAT(MP_STAT_CAS_VEHICLE_REWARD, bRewarded)
		BREAK
		#IF FEATURE_HEIST_ISLAND
		CASE VEHICLE_REWARD_KEINEMUSIK	
			SET_PACKED_STAT_BOOL(PACKED_MP_STAT_KEINEMUSIK_VEHICLE_REWARD, bRewarded)
		BREAK
		#ENDIF
	ENDSWITCH
ENDPROC

FUNC HUD_COLOURS GET_REWARD_VEHICLE_BLIP_COLOUR(VEHICLE_REWARD eReward, BOOL bGetApproachColor)
	UNUSED_PARAMETER(bGetApproachColor)
	// It looks like design doesn't want this any more url:bugstar:6809069
	SWITCH eReward
		CASE VEHICLE_REWARD_CASINO
			RETURN HUD_COLOUR_YELLOW
		BREAK		
		#IF FEATURE_HEIST_ISLAND
		CASE VEHICLE_REWARD_KEINEMUSIK
			IF bGetApproachColor
				RETURN HUD_COLOUR_YELLOW
			ENDIF
			
			RETURN HUD_COLOUR_YELLOW
		BREAK
		#ENDIF
	ENDSWITCH
	
	RETURN HUD_COLOUR_WHITE
ENDFUNC

FUNC MODEL_NAMES GET_REWARD_VEHICLE_MODEL(VEHICLE_REWARD eReward)
	SWITCH eReward
		CASE VEHICLE_REWARD_CASINO		RETURN PARAGON2
		#IF FEATURE_HEIST_ISLAND
		CASE VEHICLE_REWARD_KEINEMUSIK	RETURN WEEVIL
		#ENDIF
	ENDSWITCH
	
	ASSERTLN("[AM_MP_VEH_REWARD] - GET_REWARD_VEHICLE_MODEL - None assigned to ", DEBUG_GET_VEHICLE_REWARD_AS_STRING(eReward))
	RETURN FAGGIO
ENDFUNC

PROC SETUP_REWARD_VEHICLE_HIDDEN_BLIP(VEHICLE_REWARD eReward, BLIP_INDEX &biBlip)
	SWITCH eReward
		CASE VEHICLE_REWARD_CASINO
			SET_BLIP_FLASH_TIMER(biBlip, 5000)
			SET_BLIP_SPRITE(biBlip, RADAR_TRACE_CRATEDROP)
			SET_BLIP_NAME_FROM_TEXT_FILE(biBlip, "CAS_VEH_PN")
			SET_BLIP_DISPLAY(biBlip, DISPLAY_BOTH)
		BREAK
		#IF FEATURE_HEIST_ISLAND
		CASE VEHICLE_REWARD_KEINEMUSIK
			SET_BLIP_FLASH_TIMER(biBlip, 5000)
			SET_BLIP_SPRITE(biBlip, RADAR_TRACE_GANG_VEHICLE)
			SET_BLIP_COLOUR_FROM_HUD_COLOUR(biBlip, HUD_COLOUR_YELLOW)
			SET_BLIP_NAME_FROM_TEXT_FILE(biBlip, "CAS_VEH_PN")
			SET_BLIP_DISPLAY(biBlip, DISPLAY_BOTH)
		BREAK
		#ENDIF
	ENDSWITCH
ENDPROC

PROC PRINT_VEHICLE_REWARDED_HELP_TEXT(VEHICLE_REWARD eReward)
	SWITCH eReward
		CASE VEHICLE_REWARD_CASINO
			PRINT_HELP_WITH_STRING("CAS_VEH_REW", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(GET_REWARD_VEHICLE_MODEL(vehicleRewardData.eReward)))
		BREAK
		#IF FEATURE_HEIST_ISLAND
		CASE VEHICLE_REWARD_KEINEMUSIK
			PRINT_HELP_WITH_COLOURED_STRING("KEINE_AWARD", "KEINE_BLIP", HUD_COLOUR_YELLOW)
		BREAK
		#ENDIF
	ENDSWITCH
ENDPROC

PROC GET_ACTUAL_REWARD_VEHICLE_COORD_AND_HEADING(VEHICLE_REWARD eReward, VECTOR &vSpawnPoint, FLOAT &fSpawnHeading, INT iSpawnIndex)
	SWITCH eReward
		CASE VEHICLE_REWARD_CASINO
			SWITCH iSpawnIndex
				CASE 0			vSpawnPoint = <<866.0536, -2988.4448, 4.9008>> fSpawnHeading = 270.7996				BREAK
				CASE 1			vSpawnPoint = <<811.0694, -3021.5625, 4.8178>> fSpawnHeading = 160.1995             BREAK
				CASE 2			vSpawnPoint = <<1216.3718, -2947.2966, 4.8661>> fSpawnHeading = 178.1987            BREAK
				CASE 3			vSpawnPoint = <<1216.6270, -3040.2419, 4.8684>> fSpawnHeading = 178.1987            BREAK
				CASE 4			vSpawnPoint = <<1169.2684, -2969.4812, 4.9021>> fSpawnHeading = 269.7983            BREAK
				CASE 5			vSpawnPoint = <<1090.6786, -2969.8232, 4.9012>> fSpawnHeading = 359.1981            BREAK
				CASE 6			vSpawnPoint = <<1055.6046, -3039.2747, 4.9011>> fSpawnHeading = 284.1977            BREAK
				CASE 7			vSpawnPoint = <<951.2667, -3240.2932, 4.8951>> fSpawnHeading = 0.9969               BREAK
				CASE 8			vSpawnPoint = <<1013.8441, -3241.3367, 4.8949>> fSpawnHeading = 9.1969              BREAK
				CASE 9			vSpawnPoint = <<1067.4049, -3243.4578, 4.8946>> fSpawnHeading = 84.1968             BREAK
				CASE 10			vSpawnPoint = <<1096.4803, -3193.9319, 4.9008>> fSpawnHeading = 88.7966             BREAK
				CASE 11			vSpawnPoint = <<1111.1062, -3097.2893, 4.8677>> fSpawnHeading = 182.3965            BREAK
				CASE 12			vSpawnPoint = <<1089.5281, -3022.8242, 4.9010>> fSpawnHeading = 359.5967            BREAK
				CASE 13			vSpawnPoint = <<1182.7196, -3034.2178, 4.9021>> fSpawnHeading = 271.3965            BREAK
				CASE 14			vSpawnPoint = <<988.5466, -3029.0774, 4.9008>> fSpawnHeading = 359.7962             BREAK
				CASE 15			vSpawnPoint = <<988.7137, -3082.3730, 4.9010>> fSpawnHeading = 359.7962             BREAK
				CASE 16			vSpawnPoint = <<892.3446, -3076.7109, 4.9008>> fSpawnHeading = 359.7962             BREAK
				CASE 17			vSpawnPoint = <<861.8322, -3129.8245, 4.9008>> fSpawnHeading = 270.7960             BREAK
				CASE 18			vSpawnPoint = <<852.6052, -2943.1174, 4.9008>> fSpawnHeading = 270.7960             BREAK
				CASE 19			vSpawnPoint = <<988.0678, -2971.6128, 4.9008>> fSpawnHeading = 0.1957               BREAK
				CASE 20			vSpawnPoint = <<892.4056, -3019.5847, 4.9020>> fSpawnHeading = 0.1957               BREAK
				CASE 21			vSpawnPoint = <<1191.7548, -3073.6831, 4.8238>> fSpawnHeading = 90.1956             BREAK
				CASE 22			vSpawnPoint = <<809.9000, -3134.4895, 4.9009>> fSpawnHeading = 4.3955               BREAK
				CASE 23			vSpawnPoint = <<891.2492, -3241.0149, 4.8959>> fSpawnHeading = 0.1955               BREAK
				CASE 24			vSpawnPoint = <<1094.7687, -3100.6785, 4.8943>> fSpawnHeading = 90.1954             BREAK
				CASE 25			vSpawnPoint = <<1194.5701, -2942.8162, 4.9021>> fSpawnHeading = 90.1954             BREAK
				CASE 26			vSpawnPoint = <<852.6579, -2927.3237, 4.9008>> fSpawnHeading = 270.7951             BREAK
				CASE 27			vSpawnPoint = <<1066.7445, -3073.2300, 4.9010>> fSpawnHeading = -0.0050             BREAK
				CASE 28			vSpawnPoint = <<844.2502, -3050.1536, 4.7422>> fSpawnHeading = 91.7947              BREAK
				CASE 29			vSpawnPoint = <<1115.4115, -3184.9666, 4.9008>> fSpawnHeading = 360.7945            BREAK
				CASE 30			vSpawnPoint = <<1139.7178, -3185.6023, 4.9008>> fSpawnHeading = 360.7945            BREAK
				CASE 31			vSpawnPoint = <<1117.8394, -3201.7903, 4.9008>> fSpawnHeading = 179.7964            BREAK
			ENDSWITCH
		BREAK
		
		#IF FEATURE_HEIST_ISLAND
		CASE VEHICLE_REWARD_KEINEMUSIK
			SWITCH iSpawnIndex
				CASE 0	vSpawnPoint = <<-902.7407, -2607.9775, 35.6050>>	fSpawnHeading = 149.9998 BREAK
				CASE 1	vSpawnPoint = <<-920.7925, -2597.6157, 35.6050>>	fSpawnHeading = 149.9998 BREAK
				CASE 2	vSpawnPoint = <<-938.3965, -2613.4663, 35.1893>>	fSpawnHeading = 238.7997 BREAK
				CASE 3	vSpawnPoint = <<-943.8753, -2623.3833, 33.7924>>	fSpawnHeading = 238.7997 BREAK
				CASE 4	vSpawnPoint = <<-962.2972, -2654.8831, 34.7399>>	fSpawnHeading = 238.7997 BREAK
				CASE 5	vSpawnPoint = <<-971.8165, -2671.6914, 35.6050>>	fSpawnHeading = 238.9997 BREAK
				CASE 6	vSpawnPoint = <<-961.0366, -2685.9768, 35.6049>>	fSpawnHeading = 329.1993 BREAK
				CASE 7	vSpawnPoint = <<-951.3126, -2691.6216, 35.6049>>	fSpawnHeading = 329.1993 BREAK
				CASE 8	vSpawnPoint = <<-936.5653, -2692.3538, 35.6050>>	fSpawnHeading = 59.5993 BREAK
				CASE 9	vSpawnPoint = <<-925.1686, -2672.3108, 34.2890>>	fSpawnHeading = 59.5993 BREAK
				CASE 10	vSpawnPoint = <<-903.4186, -2633.8564, 35.1724>>	fSpawnHeading = 59.5993 BREAK
				CASE 11	vSpawnPoint = <<-924.6265, -2643.6985, 38.0411>>	fSpawnHeading = 59.5993 BREAK
				CASE 12	vSpawnPoint = <<-939.4594, -2668.1248, 36.4641>>	fSpawnHeading = 59.5993 BREAK
				CASE 13	vSpawnPoint = <<-928.2064, -2623.8469, 36.4646>>	fSpawnHeading = 239.1989 BREAK
				CASE 14	vSpawnPoint = <<-942.5833, -2648.6030, 38.0394>>	fSpawnHeading = 239.7989 BREAK
				CASE 15	vSpawnPoint = <<-1011.2230, -2580.2222, 36.9491>>	fSpawnHeading = 239.7989 BREAK
				CASE 16	vSpawnPoint = <<-1027.8301, -2608.4644, 37.4228>>	fSpawnHeading = 239.7989 BREAK
				CASE 17	vSpawnPoint = <<-1051.5420, -2621.0100, 35.6051>>	fSpawnHeading = 239.7989 BREAK
				CASE 18	vSpawnPoint = <<-1021.8864, -2569.7368, 34.7484>>	fSpawnHeading = 239.7989 BREAK
				CASE 19	vSpawnPoint = <<-1012.0209, -2553.0249, 35.6049>>	fSpawnHeading = 239.7989 BREAK
				CASE 20	vSpawnPoint = <<-1051.0011, -2634.0518, 35.6050>>	fSpawnHeading = 330.7986 BREAK
				CASE 21	vSpawnPoint = <<-1032.9354, -2644.7397, 35.6050>>	fSpawnHeading = 330.7986 BREAK
				CASE 22	vSpawnPoint = <<-994.2746, -2555.4246, 35.6049>>	fSpawnHeading = 149.7981 BREAK
				CASE 23	vSpawnPoint = <<-979.4405, -2564.2031, 35.6049>>	fSpawnHeading = 149.7981 BREAK
				CASE 24	vSpawnPoint = <<-983.0535, -2583.5767, 35.4786>>	fSpawnHeading = 59.9978 BREAK
				CASE 25	vSpawnPoint = <<-1008.7263, -2628.3379, 34.7685>>	fSpawnHeading = 58.7977 BREAK
				CASE 26	vSpawnPoint = <<-1018.4267, -2644.9009, 35.6050>>	fSpawnHeading = 58.7977 BREAK
				CASE 27	vSpawnPoint = <<-997.2888, -2579.6943, 36.0407>>	fSpawnHeading = 58.7977 BREAK
				CASE 28	vSpawnPoint = <<-1008.9539, -2600.4475, 38.1050>>	fSpawnHeading = 58.7977 BREAK
				CASE 29	vSpawnPoint = <<-1024.7745, -2627.7798, 35.7154>>	fSpawnHeading = 58.7977 BREAK
			ENDSWITCH
		BREAK
		#ENDIF
	ENDSWITCH
ENDPROC

FUNC INT GET_MAX_SPAWN_POINTS_FOR_REWARD_VEHICLE(VEHICLE_REWARD eReward) 
	SWITCH eReward
		CASE VEHICLE_REWARD_CASINO	RETURN 32
		#IF FEATURE_HEIST_ISLAND
		CASE VEHICLE_REWARD_KEINEMUSIK	RETURN 29
		#ENDIF
	ENDSWITCH
	
	RETURN 0
ENDFUNC

FUNC BOOL SHOULD_REWARD_SPECIFIC_VEHICLE(VEHICLE_REWARD eReward)
	INT iCasStat = GET_MP_INT_CHARACTER_STAT(MP_STAT_CASINO_NOTIFICATIONS)
	
	SWITCH eReward
		CASE VEHICLE_REWARD_CASINO
			IF NOT GET_IS_VEHICLE_REWARDED(VEHICLE_REWARD_CASINO)
				IF IS_BIT_SET(iCasStat, biFM_CASINO_COMPLETED_STORY_AS_LEADER_TEXT_SENT)
				AND GET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_LEADER)
					PRINTLN("[AM_MP_VEH_REWARD] - SHOULD_REWARD_VEHICLE all casino missions are done")
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
		
		#IF FEATURE_HEIST_ISLAND
		CASE VEHICLE_REWARD_KEINEMUSIK
			IF GET_IS_VEHICLE_REWARDED(VEHICLE_REWARD_KEINEMUSIK)
				RETURN FALSE	
			ENDIF
			
			IF GET_PACKED_STAT_BOOL(PACKED_MP_STAT_RECIVED_KEINEMUSIK_REWARD_CALL)
				PRINTLN("[AM_MP_VEH_REWARD] - SHOULD_REWARD_VEHICLE all keinemusik request missions are done")
				RETURN TRUE
			ENDIF
		BREAK
		#ENDIF
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════════╡ CLEANUP  ╞═══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛
PROC SCRIPT_CLEANUP()
	PRINTLN("[AM_MP_VEH_REWARD] - SCRIPT_CLEANUP!")
	INT iPlayer = NATIVE_TO_INT(PLAYER_ID())
	VEHICLE_INDEX rewardVehicle
	IF iPlayer != -1
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(playerBD[iPlayer].rewardVeh)
			rewardVehicle = NET_TO_VEH(playerBD[iPlayer].rewardVeh)

			IF DOES_ENTITY_EXIST(rewardVehicle)
			AND NOT IS_ENTITY_DEAD(rewardVehicle)
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), rewardVehicle)
					TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
				ENDIF
			ENDIF
		ENDIF	
	ENDIF
	g_bCleanUpVehRewardScript = FALSE
	
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC


//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════════╡ DEBUG  ╞═════════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

#IF IS_DEBUG_BUILD

PROC ADD_REWARD_VEHICLE_STAT_CLEAR_WIDGETS()
	INT iRewardVehicle
	INT iMaxRewards = COUNT_OF(VEHICLE_REWARD)
	VEHICLE_REWARD eReward
	TEXT_LABEL_63 tlName
	
	REPEAT iMaxRewards iRewardVehicle
		eReward = INT_TO_ENUM(VEHICLE_REWARD, iRewardVehicle)
		tlName = "Clear "
		tlName += DEBUG_GET_VEHICLE_REWARD_AS_STRING(eReward)
		tlName += " recieve stat"
		ADD_WIDGET_BOOL(tlName, vehRewardDebugData.bClearRewardStat[eReward])
	ENDREPEAT
ENDPROC

PROC ADD_REWARD_VEHICLE_ENABLE_WIDGETS()
	INT iRewardVehicle
	INT iMaxRewards = COUNT_OF(VEHICLE_REWARD)
	VEHICLE_REWARD eReward
	TEXT_LABEL_63 tlName
	
	REPEAT iMaxRewards iRewardVehicle
		eReward = INT_TO_ENUM(VEHICLE_REWARD, iRewardVehicle)
		tlName = "Enable "
		tlName += DEBUG_GET_VEHICLE_REWARD_AS_STRING(eReward)
		tlName += " reward"
		ADD_WIDGET_BOOL(tlName, vehRewardDebugData.bEnableVehReward[eReward])
	ENDREPEAT
ENDPROC

PROC GET_VEHICLE_PURCHASE_TRIGGER_CONTROL(CONTROL_TYPE &control, CONTROL_ACTION &action)
	control = FRONTEND_CONTROL
	action = INPUT_FRONTEND_X
ENDPROC

FUNC VEHICLE_INDEX GET_SELECTED_VEHICLE_INDEX_FOR_PURCHASE()
	VEHICLE_INDEX selectedVehicle
	ENTITY_INDEX focusEntity = GET_FOCUS_ENTITY_INDEX()
	
	IF DOES_ENTITY_EXIST(focusEntity)
	AND IS_ENTITY_A_VEHICLE(focusEntity)
		selectedVehicle = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(focusEntity)
	ENDIF
	
	RETURN selectedVehicle
ENDFUNC

FUNC BOOL PURCHASE_VEH_ADD_MOD_PRICE()
	RETURN TRUE
ENDFUNC

PROC CREATE_WIDGETS()
	START_WIDGET_GROUP("AM_MP_VEHICLE_REWARD")
		START_WIDGET_GROUP("SPAWN")
			ADD_WIDGET_BOOL("Set Selected Vehicle as PV", vehRewardDebugData.bSetSelectedVehicleAsPV)
			ADD_WIDGET_STRING("Press square once player is inside the vehicle to set as PV")
			ADD_WIDGET_BOOL("Set Vehicle Ped is in as PV", vehRewardDebugData.bSetVehPedIsInAsPV)
			ADD_WIDGET_BOOL("Add mod value", vehRewardDebugData.bAddModValue)
			ADD_REWARD_VEHICLE_ENABLE_WIDGETS()
			ADD_REWARD_VEHICLE_STAT_CLEAR_WIDGETS()
		STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP()
	
	VEHICLE_PURCHASE_INIT(vehPurchaseStruct, &GET_VEHICLE_PURCHASE_TRIGGER_CONTROL)
ENDPROC

PROC UPDATE_DEBUG_WIDGETS()
	IF vehRewardDebugData.bAddModValue
		SET_PURCHASE_VEHICLE_ADD_MOD_PRICE_TO_VEHICLE_PRICE(vehPurchaseStruct, &PURCHASE_VEH_ADD_MOD_PRICE)
	ENDIF
	
	IF vehRewardDebugData.bSetVehPedIsInAsPV
		IF NOT IS_VEHICLE_PURCHASE_RESULT_READY(vehPurchaseStruct)
			PURCHASE_VEHICLE_PED_IS_IN(vehPurchaseStruct)
		ELSE
			CLEANUP_PURCHASE_VEHICLE_DATA(vehPurchaseStruct)
			vehRewardDebugData.bSetVehPedIsInAsPV = FALSE
		ENDIF	
	ELIF vehRewardDebugData.bSetSelectedVehicleAsPV
		SET_SELECTED_PURCHASE_VEHICLE_INDEX(vehPurchaseStruct, &GET_SELECTED_VEHICLE_INDEX_FOR_PURCHASE)
		
		IF NOT IS_VEHICLE_PURCHASE_RESULT_READY(vehPurchaseStruct)
			PURCHASE_SELECTED_VEHICLE(vehPurchaseStruct)
		ELSE
			CLEANUP_PURCHASE_VEHICLE_DATA(vehPurchaseStruct)
			vehRewardDebugData.bSetSelectedVehicleAsPV = FALSE
		ENDIF	
	ENDIF
	
	// Only run every other frame for performance
	IF GET_FRAME_COUNT() % 2 = 0
		EXIT
	ENDIF
	
	INT iRewardVehicle
	INT iMaxRewards = COUNT_OF(VEHICLE_REWARD)
	VEHICLE_REWARD eReward
	TEXT_LABEL_63 tlDebugMessage

	REPEAT iMaxRewards iRewardVehicle	
		IF NOT vehRewardDebugData.bClearRewardStat[iRewardVehicle]
			RELOOP
		ENDIF
		
		vehRewardDebugData.bClearRewardStat[iRewardVehicle] = FALSE	
		eReward = INT_TO_ENUM(VEHICLE_REWARD, iRewardVehicle)
		SET_VEHICLE_REWARDED(eReward, FALSE)
		tlDebugMessage = "Reward Vehicle: cleared "
		tlDebugMessage += DEBUG_GET_VEHICLE_REWARD_AS_STRING(eReward)
		tlDebugMessage += " recieved stat"
		PRINT_DEBUG_TICKER(tlDebugMessage)	
	ENDREPEAT
ENDPROC
#ENDIF

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════╡ FUNCTIONS / PROC  ╞═══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC SCRIPT_INITIALISE()
	PRINTLN("[AM_MP_VEH_REWARD] - SCRIPT_INITIALISE - Initialising")
	
	NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE)
	
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD, SIZE_OF(playerBD))
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD))
	
	IF NOT Wait_For_First_Network_Broadcast()
		PRINTLN("[AM_MP_VEH_REWARD] - SCRIPT_INITIALISE - Failed to receive initial network broadcast. Terminating script.")
		SCRIPT_CLEANUP()
	ENDIF

	#IF IS_DEBUG_BUILD
	CREATE_WIDGETS()
	#ENDIF

	PRINTLN("[AM_MP_VEH_REWARD] - SCRIPT_INITIALISE - Complete")
ENDPROC

FUNC BOOL IS_A_VEHICLE_REWARD_AVAILABLE(VEHICLE_REWARD &eVehicleToRewardOut)
	INT iRewardVehicle
	INT iMaxRewards = COUNT_OF(VEHICLE_REWARD)
	VEHICLE_REWARD eReward
	
	#IF IS_DEBUG_BUILD
	TEXT_LABEL_63 tlDebugTicker
	#ENDIF
	
	REPEAT iMaxRewards iRewardVehicle
		eReward = INT_TO_ENUM(VEHICLE_REWARD, iRewardVehicle)
		
		IF SHOULD_REWARD_SPECIFIC_VEHICLE(eReward)
			eVehicleToRewardOut = eReward
			RETURN TRUE
		ENDIF
		
		#IF IS_DEBUG_BUILD 
		IF vehRewardDebugData.bEnableVehReward[eReward] 
			tlDebugTicker = "Awarding vehicle"
			tlDebugTicker += DEBUG_GET_VEHICLE_REWARD_AS_STRING(eReward)
			PRINT_DEBUG_TICKER(tlDebugTicker)
			eVehicleToRewardOut = eReward
			RETURN TRUE
		ENDIF
		#ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_REWARD_VEHICLE(VEHICLE_REWARD &eVehicleToRewardOut)
	
	IF GET_FRAME_COUNT() % 30 != 0
		RETURN FALSE
	ENDIF	
	
	IF GET_CURRENT_GAMEMODE() != GAMEMODE_FM
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE, TRUE)
		PRINTLN("[AM_MP_VEH_REWARD] - SHOULD_REWARD_VEHICLE false: IS_PLAYER_IN_PROPERTY")
		RETURN FALSE
	ENDIF	
	
	IF IS_PLAYER_CRITICAL_TO_ANY_EVENT(PLAYER_ID())
		PRINTLN("[AM_MP_VEH_REWARD] - SHOULD_REWARD_VEHICLE - false: player is critical to FM event.")
		RETURN FALSE
	ENDIF
	
	IF IS_LOCAL_PLAYER_DELIVERING_BOUNTY()
		PRINTLN("[AM_MP_VEH_REWARD] - SHOULD_REWARD_VEHICLE - false: IS_LOCAL_PLAYER_DELIVERING_BOUNTY")
		RETURN FALSE
	ENDIF
	
	IF NOT IS_A_VEHICLE_REWARD_AVAILABLE(eVehicleToRewardOut)
		PRINTLN("[AM_MP_VEH_REWARD] - SHOULD_REWARD_VEHICLE - false: IS_A_VEHICLE_REWARD_AVAILABLE, ", DEBUG_GET_VEHICLE_REWARD_AS_STRING(eVehicleToRewardOut))
		RETURN FALSE
	ENDIF
	
	PRINTLN("[AM_MP_VEH_REWARD] - SHOULD_REWARD_VEHICLE - true: Should award: ", DEBUG_GET_VEHICLE_REWARD_AS_STRING(eVehicleToRewardOut))
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_BLOCK_REWARD_VEHICLE()
	IF MPGlobals.VehicleData.bAssignToMainScript
		PRINTLN("[AM_MP_VEH_REWARD] - SHOULD_BLOCK_REWARD_VEHICLE bAssignToMainScript true")
		RETURN TRUE
	ENDIF
	
	IF g_bBlockVehRewardSpawn
		PRINTLN("[AM_MP_VEH_REWARD] - SHOULD_BLOCK_REWARD_VEHICLE g_bBlockVehRewardSpawn true")
		RETURN TRUE
	ENDIF
	
	IF !IS_SKYSWOOP_AT_GROUND()
		PRINTLN("[AM_MP_VEH_REWARD] - SHOULD_BLOCK_REWARD_VEHICLE IS_SKYSWOOP_AT_GROUND false")
		RETURN TRUE
	ENDIF
	
	IF !IS_NET_PLAYER_OK(PLAYER_ID())
		PRINTLN("[AM_MP_VEH_REWARD] - SHOULD_BLOCK_REWARD_VEHICLE IS_NET_PLAYER_OK false")
		RETURN TRUE
	ENDIF
	
	IF NETWORK_IS_IN_MP_CUTSCENE()
		PRINTLN("[AM_MP_VEH_REWARD] - SHOULD_BLOCK_REWARD_VEHICLE NETWORK_IS_IN_MP_CUTSCENE true")
		RETURN TRUE
	ENDIF	
	
	IF !IS_PLAYER_CONTROL_ON(PLAYER_ID())
		PRINTLN("[AM_MP_VEH_REWARD] - SHOULD_BLOCK_REWARD_VEHICLE IS_PLAYER_CONTROL_ON false")
		RETURN TRUE
	ENDIF
	
	IF NETWORK_IS_ACTIVITY_SESSION()
		PRINTLN("[AM_MP_VEH_REWARD] - SHOULD_BLOCK_REWARD_VEHICLE NETWORK_IS_ACTIVITY_SESSION true")
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_CRITICAL_TO_ANY_EVENT(PLAYER_ID())
		PRINTLN("[AM_MP_VEH_REWARD] - SHOULD_BLOCK_REWARD_VEHICLE IS_PLAYER_CRITICAL_TO_ANY_EVENT true")
		RETURN TRUE
	ENDIF
	
	PRINTLN("[AM_MP_VEH_REWARD] - SHOULD_BLOCK_REWARD_VEHICLE Not blocked")
	RETURN FALSE
ENDFUNC 

FUNC BOOL GET_SAFE_REWARD_VEHICLE_COORD(VEHICLE_REWARD eReward, VECTOR &vSpawnCoord, FLOAT &fSpawnHeading)
	VEHICLE_SPAWN_LOCATION_PARAMS Params
	Params.fMaxDistance = 10.0
	Params.bConsiderHighways = TRUE
	Params.bCheckEntityArea = TRUE
	Params.bCheckOwnVisibility = FALSE
	Params.bIsForPV = TRUE
	Params.bIgnoreCustomNodesForArea = TRUE	
	Params.bUseExactCoordsIfPossible = TRUE
	VECTOR vActualSpawnPoint
	FLOAT fActualSpawnPoint
	GET_ACTUAL_REWARD_VEHICLE_COORD_AND_HEADING(eReward, vActualSpawnPoint, fActualSpawnPoint, vehicleRewardData.iSpawnIndex)
	IF HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS(vActualSpawnPoint, <<0, 0, fActualSpawnPoint>>, GET_REWARD_VEHICLE_MODEL(eReward), FALSE, vSpawnCoord, fSpawnHeading, Params)
		RETURN TRUE
	ELSE
		PRINTLN("[AM_MP_VEH_REWARD] - GET_SAFE_REWARD_VEHICLE_COORD waiting on HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS")	
	ENDIF
	
	RETURN FALSE
ENDFUNC 

PROC CONFIGURE_STRUCT_FOR_REWARD_VEHICLE(MODEL_NAMES vehModel, VEHICLE_SETUP_STRUCT_MP &sData)
	SWITCH vehModel
		CASE PARAGON2
			sData.VehicleSetup.iModIndex[MOD_ARMOUR] = 4
		BREAK
		#IF FEATURE_HEIST_ISLAND
		CASE WEEVIL
			sData.VehicleSetup.iColour1 = 111
			sData.VehicleSetup.iColour2 = 111
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 0
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 1
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_0)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_1)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_2)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_3)
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 15
		BREAK
		#ENDIF
	ENDSWITCH
ENDPROC

PROC MAINTAIN_VEHICLE_REWARD_SPAWN_VEHICLE()
	INT iPlayer = NATIVE_TO_INT(PLAYER_ID())
	IF iPlayer = -1
		EXIT
	ENDIF
	
	IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(playerBD[iPlayer].rewardVeh)
		IF REQUEST_LOAD_MODEL(GET_REWARD_VEHICLE_MODEL(vehicleRewardData.eReward))
			IF NOT IS_BIT_SET(vehicleRewardData.iLocalBS, VEHICLE_REWARD_LOCAL_BS_RESERVE_VEHICLE)
				IF CAN_RESERVE_NETWORK_VEHICLES_FOR_THIS_SCRIPT(GET_NUM_RESERVED_MISSION_VEHICLES()+1, FALSE, TRUE)
					#IF IS_DEBUG_BUILD
					PRINTLN("[AM_MP_VEH_REWARD] - MAINTAIN_VEHICLE_REWARD_SPAWN_VEHICLE VEHICLE_REWARD_LOCAL_BS_RESERVE_VEHICLE true Num Reserved: ", GET_NUM_RESERVED_MISSION_VEHICLES() + 1,
					" Num reserved mission vehicle: ", GET_NUM_CREATED_MISSION_VEHICLES(FALSE))
					#ENDIF
					RESERVE_NETWORK_MISSION_VEHICLES(GET_NUM_RESERVED_MISSION_VEHICLES() + 1)
					SET_BIT(vehicleRewardData.iLocalBS, VEHICLE_REWARD_LOCAL_BS_RESERVE_VEHICLE)
				ELSE
					PRINTLN("[AM_MP_VEH_REWARD] - MAINTAIN_VEHICLE_REWARD_SPAWN_VEHICLE CAN_RESERVE_NETWORK_VEHICLES_FOR_THIS_SCRIPT false")
				ENDIF
			ELSE	
				VECTOR vSpawnCoord
				FLOAT fSpawnHeading
				IF NOT IS_BIT_SET(vehicleRewardData.iLocalBS, VEHICLE_REWARD_LOCAL_BS_PICK_RANDOM_SPAWN_POINT)
					vehicleRewardData.iSpawnIndex = GET_RANDOM_INT_IN_RANGE(0, GET_MAX_SPAWN_POINTS_FOR_REWARD_VEHICLE(vehicleRewardData.eReward))
					SET_BIT(vehicleRewardData.iLocalBS, VEHICLE_REWARD_LOCAL_BS_PICK_RANDOM_SPAWN_POINT)
				ELSE
					IF GET_SAFE_REWARD_VEHICLE_COORD(vehicleRewardData.eReward, vSpawnCoord, fSpawnHeading)
						IF CREATE_NET_VEHICLE(playerBD[iPlayer].rewardVeh, GET_REWARD_VEHICLE_MODEL(vehicleRewardData.eReward), vSpawnCoord, fSpawnHeading, FALSE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
							GET_ACTUAL_REWARD_VEHICLE_COORD_AND_HEADING(vehicleRewardData.eReward, vSpawnCoord, fSpawnHeading, vehicleRewardData.iSpawnIndex)
							VEHICLE_INDEX rewardVehicle = NET_TO_VEH(playerBD[iPlayer].rewardVeh)
							
							SET_ENTITY_HEADING(rewardVehicle, fSpawnHeading)
							SET_ENTITY_INVINCIBLE(rewardVehicle, TRUE)
							SET_VEHICLE_DOORS_LOCKED(rewardVehicle, VEHICLELOCK_UNLOCKED)
							SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(rewardVehicle, TRUE)
						    SET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(rewardVehicle, PLAYER_ID(), FALSE)
							SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(rewardVehicle, TRUE)
							SET_VEHICLE_RESPECTS_LOCKS_WHEN_HAS_DRIVER(rewardVehicle, TRUE)
							
							IF GET_NUM_MOD_KITS(rewardVehicle) > 0
								SET_VEHICLE_MOD_KIT(rewardVehicle, 0 )
							ENDIF
							
							VEHICLE_SETUP_STRUCT_MP sVehicleSetup
							CONFIGURE_STRUCT_FOR_REWARD_VEHICLE(GET_REWARD_VEHICLE_MODEL(vehicleRewardData.eReward), sVehicleSetup)
							SET_VEHICLE_SETUP_MP(rewardVehicle, sVehicleSetup)
							SET_VEHICLE_REWARD_STAGE(vehicleRewardData, VEH_REWARD_INIT_STAGE)
							CLEAR_BIT(vehicleRewardData.iLocalBS, VEHICLE_REWARD_LOCAL_BS_PICK_RANDOM_SPAWN_POINT)
							SET_MODEL_AS_NO_LONGER_NEEDED(GET_REWARD_VEHICLE_MODEL(vehicleRewardData.eReward))
							PRINTLN("[AM_MP_VEH_REWARD] - MAINTAIN_VEHICLE_REWARD_SPAWN_VEHICLE vehicle created")
						ENDIF
					ENDIF
				ENDIF	
			ENDIF	
		ENDIF
	ELSE
		VEHICLE_INDEX rewardVehicle = NET_TO_VEH(playerBD[iPlayer].rewardVeh)
	
		IF IS_ENTITY_DEAD(rewardVehicle)
		OR IS_ENTITY_IN_WATER(rewardVehicle)
			VEHICLE_INDEX pedVeh = rewardVehicle
			IF NETWORK_HAS_CONTROL_OF_ENTITY(pedVeh)
				IF CAN_EDIT_THIS_ENTITY(pedVeh, vehicleRewardData.bSetAsMissionEntity)
					NETWORK_FADE_OUT_ENTITY(pedVeh, FALSE, TRUE)
					DELETE_VEHICLE(pedVeh)
					PRINTLN("[AM_MP_VEH_REWARD] - MAINTAIN_VEHICLE_REWARD_SPAWN_VEHICLE - vehicle is dead lets delete it")	
				ENDIF	
			ENDIF
		ELSE
			PRINTLN("[AM_MP_VEH_REWARD] - MAINTAIN_VEHICLE_REWARD_SPAWN_VEHICLE - vehicle already exist and all good")	
			SET_VEHICLE_REWARD_STAGE(vehicleRewardData, VEH_REWARD_INIT_STAGE)
		ENDIF	
	ENDIF	
ENDPROC

PROC MAINTAIN_VEHICLE_REWARD_INIT()
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(playerBD[NATIVE_TO_INT(PLAYER_ID())].rewardVeh)
		IF !DOES_BLIP_EXIST(vehicleRewardData.rewardVehBlip)
			IF IS_SAFE_TO_DISPLAY_HELP_AND_BLIP()
				vehicleRewardData.rewardVehBlip = CREATE_BLIP_FOR_ENTITY(NET_TO_ENT(playerBD[NATIVE_TO_INT(PLAYER_ID())].rewardVeh))
				SETUP_REWARD_VEHICLE_HIDDEN_BLIP(vehicleRewardData.eReward, vehicleRewardData.rewardVehBlip)
				PRINT_VEHICLE_REWARDED_HELP_TEXT(vehicleRewardData.eReward)
				SET_VEHICLE_REWARD_STAGE(vehicleRewardData, VEH_REWARD_WAIT_FOR_PLAYER_ENTER_STAGE)
			ELSE
				PRINTLN("[AM_MP_VEH_REWARD] - MAINTAIN_VEHICLE_REWARD_INIT IS_SAFE_TO_DISPLAY_HELP_AND_BLIP FALSE")
			ENDIF	
		ELSE
			PRINTLN("[AM_MP_VEH_REWARD] - MAINTAIN_VEHICLE_REWARD_INIT - blip already exist and all good")	
			SET_VEHICLE_REWARD_STAGE(vehicleRewardData, VEH_REWARD_WAIT_FOR_PLAYER_ENTER_STAGE)
		ENDIF
	ENDIF	
ENDPROC

PROC MAINTAIN_VEHICLE_REWARD_WAIT_FOR_PLAYER_TO_ENTER()
	VEHICLE_INDEX playerVehicle
	INT iPlayer = NATIVE_TO_INT(PLAYER_ID())
	IF iPlayer != -1
		playerVehicle = NET_TO_VEH(playerBD[iPlayer].rewardVeh)
	ENDIF
	
	// Maintain blip approach color
	IF NOT IS_ENTITY_DEAD(playerVehicle) 
	AND IS_ENTITY_ALIVE(PLAYER_PED_ID())
		IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), playerVehicle, << 10.0, 10.0, 10.0>>)
			SET_BLIP_COLOUR_FROM_HUD_COLOUR(vehicleRewardData.rewardVehBlip, GET_REWARD_VEHICLE_BLIP_COLOUR(vehicleRewardData.eReward, TRUE))
		ELSE
			SET_BLIP_COLOUR_FROM_HUD_COLOUR(vehicleRewardData.rewardVehBlip, GET_REWARD_VEHICLE_BLIP_COLOUR(vehicleRewardData.eReward, FALSE))
		ENDIF
	ENDIF

	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(playerBD[NATIVE_TO_INT(PLAYER_ID())].rewardVeh)
		IF NOT IS_ENTITY_DEAD(playerVehicle)
		AND NOT IS_ENTITY_IN_WATER(playerVehicle)
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), playerVehicle)
			AND NOT IS_PED_TRYING_TO_ENTER_OR_EXIT_VEHICLE(PLAYER_PED_ID())
			AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) != PERFORMING_TASK
			AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) != WAITING_TO_START_TASK
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_LEAVE_CAMERA_CONTROL_ON)
				SET_VEHICLE_REWARD_STAGE(vehicleRewardData, VEH_REWARD_CHANGE_TO_PV_STAGE)
			ENDIF
		ELSE
			IF IS_ENTITY_ALIVE(playerVehicle)
				IF IS_ENTITY_IN_WATER(playerVehicle)
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),  playerVehicle)
						TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
						NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					ENDIF
				ENDIF	
			ENDIF	
			SET_VEHICLE_REWARD_STAGE(vehicleRewardData, VEH_REWARD_CLEANUP_STAGE)
		ENDIF
	ENDIF
ENDPROC

FUNC INT GET_PERSONAL_VEHICLE_INDEX()
	RETURN g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iLastSavedCarUsed
ENDFUNC

FUNC BOOL IS_VEHICLE_PERSONAL_MP_VEHICLE(VEHICLE_INDEX tempveh)
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF DECOR_IS_REGISTERED_AS_TYPE("Player_Vehicle", DECOR_TYPE_INT)
			IF DECOR_EXIST_ON(tempveh,"Player_Vehicle")
				IF DECOR_GET_INT(tempveh, "Player_Vehicle") = NETWORK_HASH_FROM_PLAYER_HANDLE(PLAYER_ID())
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_VEHICLE_MODEL_MATCHES_SAVE_DATA_VEH_MODEL()
	IF PLAYER_ID() != INVALID_PLAYER_INDEX()
		IF NOT HAS_OFFICE_PERSONAL_CAR_MOD_SWAP_PV_TO_SUPERMOD(PLAYER_ID())
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF IS_VEHICLE_MY_CURRENT_PERSONAL_VEHICLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
					IF GET_PERSONAL_VEHICLE_INDEX() != -1 
					AND GET_PERSONAL_VEHICLE_INDEX() < MAX_MP_SAVED_VEHICLES
						IF GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) != g_MpSavedVehicles[GET_PERSONAL_VEHICLE_INDEX()].vehicleSetupMP.VehicleSetup.eModel
							PRINTLN("IS_VEHICLE_MODEL_MATCHES_SAVE_DATA_VEH_MODEL - FALSE ")
							RETURN FALSE
						ENDIF
					ENDIF	
				ENDIF	
			ENDIF
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC MAINTAIN_VEHICLE_REWARD_CHANGE_TO_PV()

	VEHICLE_INDEX playerVeh
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		playerVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
	ENDIF
	
	IF !DOES_ENTITY_EXIST(playerVeh)
		SET_VEHICLE_REWARD_STAGE(vehicleRewardData, VEH_REWARD_CLEANUP_STAGE)
		EXIT
	ENDIF
	
	IF playerVeh != NET_TO_VEH(playerBD[NATIVE_TO_INT(PLAYER_ID())].rewardVeh)
		EXIT
	ENDIF
	
	IF IS_ENTITY_ALIVE(playerVeh)
	AND IS_ENTITY_IN_WATER(playerVeh)
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), playerVeh)
			TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
			NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		ENDIF
		SET_VEHICLE_REWARD_STAGE(vehicleRewardData, VEH_REWARD_CLEANUP_STAGE)
		EXIT
	ENDIF
	
	IF RUN_WIN_VEHICLE(playerVeh, sReplaceMPSVData, vehicleRewardData.iTransactionResult, vehicleRewardData.iResultSlot, vehicleRewardData.iDisplaySlot, vehicleRewardData.iControlState, FALSE, TRUE, FALSE)
		IF vehicleRewardData.iControlState = 3
			TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
			SET_VEHICLE_REWARD_STAGE(vehicleRewardData, VEH_REWARD_CLEANUP_STAGE)
		ELSE
			SET_PV_ASSIGN_TO_MAIN_SCRIPT(TRUE)
			SET_BIT(vehicleRewardData.iLocalBS, VEHICLE_REWARD_LOCAL_BS_REWARD_VEHICLE_CHANGED_TO_PV)
			SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(playerVeh, FALSE)
			SET_VEHICLE_REWARDED(vehicleRewardData.eReward, TRUE)
			REQUEST_SAVE(SSR_REASON_CASINO_VEH_REWARD, STAT_SAVETYPE_END_SHOPPING)
			SET_VEHICLE_REWARD_STAGE(vehicleRewardData, VEH_REWARD_CLEANUP_STAGE)
			PRINTLN("[AM_MP_VEH_REWARD] - MAINTAIN_VEHICLE_REWARD_CHANGE_TO_PV Done!")
		ENDIF	
	ENDIF
ENDPROC

PROC CLEAN_UP_WARNING_VARIABLES()
	vehicleRewardData.iMP_ReplaceGarageDisplaySlot = 0
	vehicleRewardData.iMP_ReplaceGarageVehSlot = 0
	sReplaceMPSVData.iBS = 0
	sReplaceMPSVData.iButtonBS = 0
	sReplaceMPSVData.iCurrentVertSel = 0
	sReplaceMPSVData.iCurrentVertSelSecLevel = 0
	sReplaceMPSVData.iEmptyPositionBS = 0
	sReplaceMPSVData.iFirstLevelSelectedSlotID = 0
	sReplaceMPSVData.iLevel = 0
	sReplaceMPSVData.iMaxVertSel = 0
	INT i
	FOR i = 0 TO MAX_MP_SAVED_VEHICLES - 1
		sReplaceMPSVData.iSlotIDS[i] = 0
	ENDFOR
	sReplaceMPSVData.iStage = 0
	RESET_NET_TIMER(sReplaceMPSVData.menuNavigationDelay)
ENDPROC

PROC MAINTAIN_VEHICLE_REWARD_CLEAN_UP()

	IF IS_BIT_SET(vehicleRewardData.iLocalBS, VEHICLE_REWARD_LOCAL_BS_REWARD_VEHICLE_CHANGED_TO_PV)
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			VEHICLE_INDEX playerVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			IF IS_VEHICLE_PERSONAL_MP_VEHICLE(playerVeh)
				IF TAKE_CONTROL_OF_ENTITY(playerVeh)
					SET_ENTITY_INVINCIBLE(playerVeh, FALSE)
					NET_SET_PLAYER_CONTROL(PLAYER_ID() , TRUE)
					IF DOES_BLIP_EXIST(vehicleRewardData.rewardVehBlip)
						REMOVE_BLIP(vehicleRewardData.rewardVehBlip)
					ENDIF
					vehicleRewardData.iLocalBS = 0
					RESERVE_NETWORK_MISSION_VEHICLES(GET_NUM_RESERVED_MISSION_VEHICLES()-1)
					CLEAN_UP_WARNING_VARIABLES()
				ELSE
					PRINTLN("[AM_MP_VEH_REWARD] - MAINTAIN_VEHICLE_REWARD_CLEAN_UP trying to grab entity control")
					EXIT
				ENDIF
			ELSE
				PRINTLN("[AM_MP_VEH_REWARD] - MAINTAIN_VEHICLE_REWARD_CLEAN_UP waiting for IS_VEHICLE_PERSONAL_MP_VEHICLE")
				EXIT
			ENDIF
		ELSE
			PRINTLN("[AM_MP_VEH_REWARD] - MAINTAIN_VEHICLE_REWARD_CLEAN_UP player not in vehicle what happened ?")
		ENDIF
	ELSE
		INT iPlayer = NATIVE_TO_INT(PLAYER_ID())
		IF iPlayer != -1
		AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(playerBD[iPlayer].rewardVeh)
			VEHICLE_INDEX rewardVehicle = NET_TO_VEH(playerBD[iPlayer].rewardVeh)
			IF IS_ENTITY_DEAD(rewardVehicle)
			OR IS_ENTITY_IN_WATER(rewardVehicle)
				IF DOES_BLIP_EXIST(vehicleRewardData.rewardVehBlip)
					REMOVE_BLIP(vehicleRewardData.rewardVehBlip)
				ENDIF
			ENDIF
		ENDIF	
		vehicleRewardData.iLocalBS = 0
		NET_SET_PLAYER_CONTROL(PLAYER_ID() , TRUE)
		CLEAN_UP_WARNING_VARIABLES()
		PRINTLN("[AM_MP_VEH_REWARD] - MAINTAIN_VEHICLE_REWARD_CLEAN_UP PV change is failed wait for player to get back to vehicle")
	ENDIF
	
	IF USE_SERVER_TRANSACTIONS()
		IF vehicleRewardData.iTransactionResult != 0
			IF vehicleRewardData.iTransactionResult = GARAGE_VEHICLE_TRANSACTION_STATE_PENDING
				DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
			ENDIF
			
			vehicleRewardData.iTransactionResult = 0
		ENDIF
	ENDIF
	
	SET_VEHICLE_REWARD_STAGE(vehicleRewardData, VEH_REWARD_IDLE_STAGE)
	vehicleRewardData.iResultSlot = 0
	vehicleRewardData.iDisplaySlot = 0
	vehicleRewardData.iControlState = 0
ENDPROC

PROC ASSIGN_REMOTE_REWARD_VEHICLE()
	IF vehicleRewardData.iStaggerPlayer != -1
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(playerBD[vehicleRewardData.iStaggerPlayer].rewardVeh)
			VEHICLE_INDEX rewardVehicle = NET_TO_VEH(playerBD[vehicleRewardData.iStaggerPlayer].rewardVeh)
			IF !IS_ENTITY_DEAD(rewardVehicle)
				vehicleRewardData.remoteRewardVeh[vehicleRewardData.iStaggerPlayer] = rewardVehicle
			ENDIF	
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_REMOTE_PLAYER_REMOVE_REWARD_VEHICLE()
	IF NOT IS_NET_PLAYER_OK(INT_TO_NATIVE(PLAYER_INDEX, vehicleRewardData.iStaggerPlayer), FALSE)	
		RETURN TRUE
	ENDIF
	
	IF IS_ENTITY_ALIVE(vehicleRewardData.remoteRewardVeh[vehicleRewardData.iStaggerPlayer])
	AND IS_ENTITY_IN_WATER(vehicleRewardData.remoteRewardVeh[vehicleRewardData.iStaggerPlayer])
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC REMOVE_REMOTE_REWARD_VEHICLE()
	IF vehicleRewardData.iStaggerPlayer != -1
		IF SHOULD_REMOTE_PLAYER_REMOVE_REWARD_VEHICLE()
			IF DOES_ENTITY_EXIST(vehicleRewardData.remoteRewardVeh[vehicleRewardData.iStaggerPlayer])
			AND (!IS_ENTITY_DEAD(vehicleRewardData.remoteRewardVeh[vehicleRewardData.iStaggerPlayer]) OR IS_ENTITY_IN_WATER(vehicleRewardData.remoteRewardVeh[vehicleRewardData.iStaggerPlayer]))
			AND IS_VEHICLE_EMPTY(vehicleRewardData.remoteRewardVeh[vehicleRewardData.iStaggerPlayer], TRUE)
			AND NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehicleRewardData.remoteRewardVeh[vehicleRewardData.iStaggerPlayer])
				VEHICLE_INDEX remoteVeh = vehicleRewardData.remoteRewardVeh[vehicleRewardData.iStaggerPlayer]
				IF NETWORK_HAS_CONTROL_OF_ENTITY(remoteVeh)
					IF CAN_EDIT_THIS_ENTITY(remoteVeh, vehicleRewardData.bSetAsMissionEntity)
						IF IS_VEHICLE_DRIVEABLE(remoteVeh)
							SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(remoteVeh, TRUE)
							LOCK_DOORS_WHEN_NO_LONGER_NEEDED(remoteVeh)
						ENDIF
						NETWORK_FADE_OUT_ENTITY(remoteVeh, FALSE, TRUE)
						DELETE_VEHICLE(remoteVeh)
						PRINTLN("[AM_MP_VEH_REWARD] - REMOVE_REMOTE_REWARD_VEHICLE - fade out vehicle vehicleRewardData.iStaggerPlayer: ", vehicleRewardData.iStaggerPlayer)	
					ENDIF	
				ENDIF
			ENDIF
		ENDIF	
	ENDIF
ENDPROC

PROC MAINTAIN_REMOTE_REWARD_VEHICLE()
	ASSIGN_REMOTE_REWARD_VEHICLE()
	REMOVE_REMOTE_REWARD_VEHICLE()
	
	IF vehicleRewardData.iStaggerPlayer >= NUM_NETWORK_PLAYERS
		vehicleRewardData.iStaggerPlayer = 0
	ENDIF
ENDPROC

PROC MAINTAIN_VEHICLE_ATTACH_STATE()
	INT iPlayer = NATIVE_TO_INT(PLAYER_ID())
	IF iPlayer = -1
		EXIT
	ENDIF	
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(playerBD[iPlayer].rewardVeh)	
		VEHICLE_INDEX rewardVehicle = NET_TO_VEH(playerBD[iPlayer].rewardVeh)
	
		IF DOES_ENTITY_EXIST(rewardVehicle)
		AND NOT IS_ENTITY_DEAD(rewardVehicle)
			IF NOT IS_VEHICLE_MY_PERSONAL_VEHICLE(rewardVehicle)
				IF IS_VEHICLE_ATTACHED_TO_TRAILER(rewardVehicle)
					IF NETWORK_HAS_CONTROL_OF_ENTITY(rewardVehicle)
						PRINTLN("NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID - detaching from trailer")
						DETACH_VEHICLE_FROM_TRAILER(rewardVehicle)
					ELSE
						NETWORK_REQUEST_CONTROL_OF_ENTITY(rewardVehicle)
					ENDIF
				ELIF IS_VEHICLE_ATTACHED_TO_ANY_CARGOBOB(rewardVehicle)
					IF NETWORK_HAS_CONTROL_OF_ENTITY(rewardVehicle)
						DETACH_VEHICLE_FROM_ANY_CARGOBOB(rewardVehicle)
						PRINTLN("[NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID - detaching from cargobob")
					ELSE
						NETWORK_REQUEST_CONTROL_OF_ENTITY(rewardVehicle)
					ENDIF	
				ELIF IS_ENTITY_ATTACHED_TO_ANY_VEHICLE(rewardVehicle)
					IF NETWORK_HAS_CONTROL_OF_ENTITY(rewardVehicle)
						DETACH_ENTITY(rewardVehicle)
						PRINTLN("[NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID - detaching from entity")
					ELSE
						NETWORK_REQUEST_CONTROL_OF_ENTITY(rewardVehicle)
					ENDIF
				ENDIF	
			ENDIF	
		ENDIF	
	ENDIF
ENDPROC

PROC RUN_MAIN_CLIENT_LOGIC()
	SWITCH vehicleRewardData.vehRewardStage
		CASE VEH_REWARD_IDLE_STAGE
			IF SHOULD_REWARD_VEHICLE(vehicleRewardData.eReward)	AND NOT SHOULD_BLOCK_REWARD_VEHICLE()
				SET_VEHICLE_REWARD_STAGE(vehicleRewardData, VEH_REWARD_SPAWN_VEHICLE_STAGE)
			ENDIF
		BREAK
		CASE VEH_REWARD_SPAWN_VEHICLE_STAGE
			MAINTAIN_VEHICLE_REWARD_SPAWN_VEHICLE()
		BREAK
		CASE VEH_REWARD_INIT_STAGE
			MAINTAIN_VEHICLE_REWARD_INIT()
		BREAK
		CASE VEH_REWARD_WAIT_FOR_PLAYER_ENTER_STAGE
			MAINTAIN_VEHICLE_REWARD_WAIT_FOR_PLAYER_TO_ENTER()
		BREAK
		CASE VEH_REWARD_CHANGE_TO_PV_STAGE
			MAINTAIN_VEHICLE_REWARD_CHANGE_TO_PV()
		BREAK
		CASE VEH_REWARD_CLEANUP_STAGE
			MAINTAIN_VEHICLE_REWARD_CLEAN_UP()
		BREAK
	ENDSWITCH
		
	MAINTAIN_REMOTE_REWARD_VEHICLE()
	MAINTAIN_VEHICLE_ATTACH_STATE()
ENDPROC

FUNC BOOL SHOULD_CLEANUP_VEHICLE_REWARDS_SCRIPT()
	IF !IS_SKYSWOOP_AT_GROUND()
		PRINTLN("[AM_MP_VEH_REWARD] - SHOULD_CLEANUP_VEHICLE_REWARDS_SCRIPT - IS_SKYSWOOP_AT_GROUND false = TRUE")
		RETURN TRUE
	ENDIF
	
	IF g_bCleanUpVehRewardScript 
		PRINTLN("[AM_MP_VEH_REWARD] - SHOULD_CLEANUP_VEHICLE_REWARDS_SCRIPT - g_bCleanUpVehRewardScript = TRUE")
		RETURN TRUE
	ENDIF
	
	IF NOT g_bEnableVehRewardScriptInIsland
		IF IS_PLAYER_ON_HEIST_ISLAND(PLAYER_ID())
			PRINTLN("[AM_MP_VEH_REWARD] - SHOULD_CLEANUP_VEHICLE_REWARDS_SCRIPT - IS_PLAYER_ON_HEIST_ISLAND = TRUE")
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC 

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ MAIN LOOP  ╞═════════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛
SCRIPT
	IF NETWORK_IS_GAME_IN_PROGRESS() 
		SCRIPT_INITIALISE()	
	ELSE
		SCRIPT_CLEANUP()
	ENDIF

	// Main loop
	WHILE TRUE
			
		// One wait to rule them all. This can be the ONLY wait from here on in.....
		MP_LOOP_WAIT_ZERO()
				
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			PRINTLN("[AM_MP_VEH_REWARD] - SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE = TRUE")
			SCRIPT_CLEANUP()
		ENDIF
		
		IF SHOULD_CLEANUP_VEHICLE_REWARDS_SCRIPT()
			PRINTLN("[AM_MP_VEH_REWARD] - SHOULD_CLEANUP_VEHICLE_REWARDS_SCRIPT = TRUE")
			SCRIPT_CLEANUP()
		ENDIF
		
		RUN_MAIN_CLIENT_LOGIC()
	
		#IF IS_DEBUG_BUILD
			UPDATE_DEBUG_WIDGETS()
		#ENDIF
		
	ENDWHILE
ENDSCRIPT
#ENDIF


