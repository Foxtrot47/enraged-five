// ====================================================================================
// ====================================================================================
//
// Name:        AM_MP_ORBITAL_CANNON.sc
// Description: Script to control Orbital Cannon use during missions
// Written By:  Alex Murphy
//
// ====================================================================================
// ====================================================================================

USING "globals.sch"
USING "net_include.sch"
USING "net_simple_interior.sch"

USING "net_mission_orbital_cannon.sch"

///////////////////////////////////////////////////////////////////////////////////////
//                                     VARIABLES                                     //
///////////////////////////////////////////////////////////////////////////////////////

MISSION_ORBITAL_CANNON_LOCAL_DATA_STRUCT sOrbitalCannonLocal

///////////////////////////////////////////////////////////////////////////////////////
//                                   SCRIPT CLEANUP                                  //
///////////////////////////////////////////////////////////////////////////////////////

PROC SCRIPT_CLEANUP()
	PRINTLN("[AM_MP_ORBITAL_CANNON] - SCRIPT_CLEANUP")
	
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC

///////////////////////////////////////////////////////////////////////////////////////
//                                     MAIN LOOP                                     //
///////////////////////////////////////////////////////////////////////////////////////

PROC PROCESS_PRE_GAME()
	PRINTLN("[AM_MP_ORBITAL_CANNON] - PROCESS_PRE_GAME - Initialising")
	
	NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE)
	
	HANDLE_NET_SCRIPT_INITIALISATION()
ENDPROC

FUNC BOOL SHOULD_TERMINATE_MISSION_ORBITAL_CANNON()
	IF (NOT g_bMissionPlacedOrbitalCannon)	
	OR (NOT NETWORK_IS_GAME_IN_PROGRESS() AND NOT IS_FAKE_MULTIPLAYER_MODE_SET())
	OR (NOT NETWORK_IS_SESSION_ACTIVE() AND NOT IS_FAKE_MULTIPLAYER_MODE_SET())
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

SCRIPT
	PROCESS_PRE_GAME()
	
	WHILE TRUE
		WAIT(0)
		
		MAINTAIN_MISSION_ORBITAL_CANNON(sOrbitalCannonLocal)
		
		IF IS_SKYSWOOP_MOVING()
		OR IS_SKYSWOOP_IN_SKY()
		OR IS_TRANSITION_ACTIVE()
		OR IS_TRANSITION_RUNNING()
			IF sOrbitalCannonLocal.eStage = ORBITAL_CANNON_CLIENT_STAGE_ACTIVATED
				CLEANUP_MISSION_ORBITAL_CANNON(sOrbitalCannonLocal, FALSE)
			ENDIF
		ELSE
			MAINTAIN_MISSION_ORBITAL_CANNON(sOrbitalCannonLocal)
		ENDIF
		
		IF SHOULD_TERMINATE_MISSION_ORBITAL_CANNON()
			CLEANUP_MISSION_ORBITAL_CANNON(sOrbitalCannonLocal, TRUE)
			
			SCRIPT_CLEANUP()
		ENDIF
	ENDWHILE
ENDSCRIPT
