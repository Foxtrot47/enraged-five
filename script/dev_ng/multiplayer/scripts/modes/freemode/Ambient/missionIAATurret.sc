CONST_INT DOUBLE_MAX_NUM_OF_APART_ACTIVITIES_VARIABLE_SIZE 0
USING "net_activity_creator_activities.sch"

STRUCT ServerBroadcastData
	// Factory Ped serverBD Data
	SERVER_CREATOR_ACTIVITY_PEDS activityPeds[iMaxCreatorActivities]
	SERVER_CREATOR_ACTIVITY_PROPS activityProps[iMaxCreatorActivities]
ENDSTRUCT

ServerBroadcastData serverBD
ACTIVITY_INTERIOR_STRUCT interiorStruct


CHECK_CREATOR_ACTIVITIES_STRUCT activityCheck[iMaxCreatorActivities]
ACTIVITY_CONTROLLER_STRUCT activityControllerStruct
PED_VAR_ID_STRUCT pedLocalVariationsStruct[iMaxCreatorActivities]
BUNKER_FACTORY_ACTIVITY_STRUCT bunkerFactoryActivityStruct

PROC MAINTAIN_IAA_BASE_ACTIVITIES()
//	interiorStruct.pFactoryOwner 	= thisBase.pOwner
	interiorStruct.vInteriorPos 	= <<2061.5842, 2993.7402, -68.7022>>
	
	// Runs activity creator logic

//	CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "MAINTAIN_IAA_BASE_ACTIVITIES")
	    
	INIT_ACTIVITY_LAUNCH_SCRIPT_CHECK_WITH_AI_PEDS_IAA_BASE(activityControllerStruct, activityCheck, serverBD.activityProps, serverBD.activityPeds, pedLocalVariationsStruct, interiorStruct, bunkerFactoryActivityStruct)//, serverBD.serverInteriorStruct)  //, playerBD[NATIVE_TO_INT(PLAYER_ID())].bDataReset)
	MAINTAIN_APARTMENT_ACTIVITIES_CREATOR(activityControllerStruct, activityCheck, serverBD.activityProps, serverBD.activityPeds)

ENDPROC



PROC RUN_MAINTAIN_TURRET()
//	CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "RUN_MAINTAIN_TURRET")
	MAINTAIN_IAA_BASE_ACTIVITIES()
ENDPROC

/// PURPOSE: Do necessary pre game start ini
PROC PROCESS_PRE_GAME() //
	
	NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE)
	HANDLE_NET_SCRIPT_INITIALISATION()
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD))
//	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD, SIZE_OF(playerBD))
ENDPROC

FUNC BOOL SHOULD_SCRIPT_SHUT_DOWN()
	IF NOT IS_THIS_ROCKSTAR_MISSION_GANGOPS_MISSION_OF_TYPE(ciGANGOPS_FLOW_MISSION_IAABASE_FINALE)
	OR NOT NETWORK_IS_GAME_IN_PROGRESS()
	OR NOT NETWORK_IS_SESSION_ACTIVE()
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC


// Mission Script -----------------------------------------//
SCRIPT	
	
	PROCESS_PRE_GAME()
	
	WHILE TRUE
		WAIT(0)
		RUN_MAINTAIN_TURRET()
		
		IF SHOULD_SCRIPT_SHUT_DOWN()
			
			TERMINATE_THIS_THREAD()
		ENDIF
	ENDWHILE
ENDSCRIPT