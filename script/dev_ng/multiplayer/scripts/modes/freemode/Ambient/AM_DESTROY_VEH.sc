//////////////////////////////////////////////////////////////////////////////////////////
// Name:        AM_DESTROY_VEH.sc														//
// Description: Controls a Vehicle that needs to be Destroyed by the players.			//
// Written by:  Ryan Baker																//
// Date: 22/08/2014																		//
//////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"

// Game Headers
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_path.sch"
USING "commands_event.sch"

// Network Headers
USING "net_include.sch"
USING "net_events.sch"
USING "net_spawn.sch"

USING "net_mission.sch"
//
USING "net_scoring_common.sch"
USING "net_ambience.sch"

//USING "help_at_location.sch"
USING "commands_object.sch"
USING "net_xp_animation.sch"

USING "net_wait_zero.sch"

#IF IS_DEBUG_BUILD
USING "net_debug_log.sch"
#ENDIF

USING "net_gang_boss.sch"

// Game States. 
CONST_INT GAME_STATE_INI 				0
CONST_INT GAME_STATE_RUNNING			1
CONST_INT GAME_STATE_LEAVE				2
CONST_INT GAME_STATE_TERMINATE_DELAY	3
CONST_INT GAME_STATE_END				4

CONST_INT MAX_NUM_EXTRA_PEDS	3

ENUM DESTROY_VEH_STAGE_ENUM
	//eAD_IDLE,
	eAD_DESTROY,
	eAD_CLEANUP
ENDENUM

CONST_INT VTT_WANDER	0
CONST_INT VTT_PARKED	1
CONST_INT VTT_DRIVE_TO	2
CONST_INT VTT_MAX		3

CONST_INT MAX_NUM_DESTROY_VEH_LOCATIONS	5

CONST_INT DB_EXPLODE	0
CONST_INT DB_SINK		1
CONST_INT DB_FIRE		2
CONST_INT DB_SMASH		3
CONST_INT DB_SECLUDED	4
CONST_INT DB_MAX		5

CONST_INT MAX_GANGS		8

//Server Bitset Data
CONST_INT biS_MakePedsHate		0
CONST_INT biS_DriverHateSet		1
CONST_INT biS_Extra0HateSet		2
CONST_INT biS_Extra1HateSet		3
CONST_INT biS_Extra2HateSet		4

// The server broadcast data.
// Everyone can read this data, only the server can update it
STRUCT ServerBroadcastData
	INT iServerGameState
		
	INT iServerBitSet
	
	NETWORK_INDEX niVeh
	NETWORK_INDEX niDriver
	NETWORK_INDEX niExtras[MAX_NUM_EXTRA_PEDS]
	
	PLAYER_INDEX piDestroyer
	
	INT iGang 				= 0
	MODEL_NAMES VehModel 	= BALLER
	MODEL_NAMES PedModel 	= G_M_Y_BALLAORIG_01
	
	INT iVehicleTaskType	= VTT_WANDER
	INT iDestroyBonus		= DB_EXPLODE
	
	INT iNumExtras			= MAX_NUM_EXTRA_PEDS
	
	INT iRandomLocation
	VECTOR vStartLocation
	FLOAT fStartHeading
	VECTOR vEndLocation
	FLOAT fEndHeading
	
	VECTOR vDriverOnFoot
	FLOAT fDriverOnFootHeading
	VECTOR vExtraOnFoot[MAX_NUM_EXTRA_PEDS]
	FLOAT fExtraOnFootHeading[MAX_NUM_EXTRA_PEDS]
	
	DESTROY_VEH_STAGE_ENUM eDestroyStage = eAD_DESTROY
	SCRIPT_TIMER ForceCleanup
	SCRIPT_TIMER MissionTerminateDelayTimer
	SCRIPT_TIMER DestroyCheckDelayTimer
ENDSTRUCT
ServerBroadcastData serverBD


CONST_INT biP_DestroyedVeh		0
CONST_INT biP_InVeh				1
CONST_INT biP_DamagedVeh		2
CONST_INT biP_SetDriverHate		3
CONST_INT biP_SetExtra0Hate		4
CONST_INT biP_SetExtra1Hate		5
CONST_INT biP_SetExtra2Hate		6

// The client broadcast data.
// Everyone can read this data, only the local player only the server can update it
STRUCT PlayerBroadcastData
	INT iGameState
	
	INT iPlayerBitSet
	
	DESTROY_VEH_STAGE_ENUM eDestroyStage = eAD_DESTROY
ENDSTRUCT
PlayerBroadcastData playerBD[NUM_NETWORK_PLAYERS]

///// MISSION VARIABLES /////
INT iBoolsBitSet
CONST_INT biDrivingAwardDone			0

//CONST_INT START_NODE		20
//INT iSpawnNodeCount = START_NODE
//INT iCheckCount

SEQUENCE_INDEX seqDriveTo

INT iStaggeredParticipant
//BOOL bDoEndStaggeredLoopChecks

BLIP_INDEX VehBlip

CONST_FLOAT VEH_LOW_SPEED			7.0
CONST_FLOAT VEH_MED_SPEED			12.0
CONST_FLOAT VEH_HIGH_SPEED			18.0

DRIVINGMODE DrivingModeStandard = DF_SteerAroundStationaryCars|DF_StopForPeds|DF_StopAtLights|DF_ChangeLanesAroundObstructions|DF_StopForCars|DF_ForceJoinInRoadDirection|DF_SteerAroundObjects|DF_UseSwitchedOffNodes|DF_AvoidRestrictedAreas|DF_UseWanderFallbackInsteadOfStraightLine
//DRIVINGMODE DrivingModeHurry = DF_SteerAroundStationaryCars|DF_DontSteerAroundPlayerPed|DF_UseSwitchedOffNodes|DF_AvoidRestrictedAreas|DF_UseWanderFallbackInsteadOfStraightLine

CONST_INT START_NODE		20
INT iSpawnNodeCount = START_NODE
INT iCheckCount

AI_BLIP_STRUCT DriverBlipData
AI_BLIP_STRUCT ExtraPedBlipData[MAX_NUM_EXTRA_PEDS]

SCRIPT_TIMER stDrivingAwardTimer
CONST_INT DRIVING_AWARD_TIME		300000

// Widgets & Debug.
#IF IS_DEBUG_BUILD
	BOOL bHostEndMissionNow
	BOOL bWarpToVeh
#ENDIF

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                  MAIN PROCS                            ////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//Helper function to get the servers game/mission state
FUNC INT GET_SERVER_MISSION_STATE()
	RETURN serverBD.iServerGameState
ENDFUNC

FUNC BOOL SHOULD_HIDE_LOCAL_UI()
// Removed Content, force hiding
//	IF HANDLE_DEPRECATED_HIDE_BLIP(ciPI_HIDE_MENU_DEPRECATED_AMB_DESTROY_VEH)
//		RETURN TRUE
//	ENDIF

	
	RETURN TRUE
ENDFUNC

//PURPOSE: Create all the sequences needed for this script
PROC CREATE_SEQUENCES()
	IF serverBD.iVehicleTaskType = VTT_DRIVE_TO
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh)
			IF IS_VEHICLE_DRIVEABLE(NET_TO_VEH(serverBD.niVeh))
				OPEN_SEQUENCE_TASK(seqDriveTo)
					TASK_VEHICLE_DRIVE_TO_COORD_LONGRANGE(NULL, NET_TO_VEH(serverBD.niVeh), serverBD.vEndLocation, VEH_LOW_SPEED, DrivingModeStandard, 3.0)
					TASK_LEAVE_ANY_VEHICLE(NULL)
					TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, serverBD.vDriverOnFoot, 1.0, -1, DEFAULT, DEFAULT, serverBD.fDriverOnFootHeading)
				CLOSE_SEQUENCE_TASK(seqDriveTo)
				NET_PRINT_TIME() NET_PRINT("     ---------->     DESTROY VEH -  CREATE_SEQUENCES   <----------     ") NET_NL()
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Clears all the sequences needed for this script
PROC CLEAR_SEQUENCES()
	IF serverBD.iVehicleTaskType = VTT_DRIVE_TO
		CLEAR_SEQUENCE_TASK(seqDriveTo)
		NET_PRINT_TIME() NET_PRINT("     ---------->     DESTROY VEH -  CLEAR_SEQUENCES   <----------     ") NET_NL()
	ENDIF
ENDPROC

//Cleanup Mission Data
PROC SCRIPT_CLEANUP()
	
	IF DOES_BLIP_EXIST(VehBlip)
		REMOVE_BLIP(VehBlip)
	ENDIF
	
	CLEAR_SEQUENCES()
	
	IF GET_SERVER_MISSION_STATE() = GAME_STATE_END
	AND serverBD.VehModel != DUMMY_MODEL_FOR_SCRIPT
		SET_VEHICLE_MODEL_IS_SUPPRESSED(serverBD.VehModel, FALSE)
		NET_PRINT_TIME() NET_PRINT("     ---------->     DESTROY VEH -  SET_VEHICLE_MODEL_IS_SUPPRESSED - FALSE      <----------     ") NET_NL()
	ENDIF
	
	CLEAR_BIT(MPGlobalsAmbience.iNGAmbBitSet, iNGABI_PLAYER_USING_DESTROY_VEH)
	
	SET_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_DESTROY_VEH, FALSE)
	
	NET_PRINT_TIME() NET_PRINT("     ---------->     DESTROY VEH -  CLEANUP MISSION      <----------     ") NET_NL()
	#IF IS_DEBUG_BUILD NET_LOG("SCRIPT_CLEANUP")	#ENDIF
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC


//PURPOSE: Get Random Location for Driver to drive to or for Veh to be Parked at.
PROC GET_RANDOM_PARKED_UP_LOCATION(VECTOR &vLocation, FLOAT &fHeading)
	SWITCH serverBD.iRandomLocation
		CASE 0		vLocation = <<-2192.3901, -419.3334, 12.0959>>	fHeading = 71.2404		BREAK	//BEACH DINER
		CASE 1		vLocation = <<-977.0366, -1482.8658, 4.0099>>	fHeading = 124.7807		BREAK	//CANALS POOL
		CASE 2		vLocation = <<702.5064, -1370.4773, 25.0573>>	fHeading = 279.9129		BREAK	//WAREHOUSE
		CASE 3		vLocation = <<-253.3582, 299.4982, 90.9592>>	fHeading = 184.8169		BREAK	//SPITROASTERS	V-252.5364, 302.0207, 91.0400		H-209.7617
		CASE 4		vLocation = <<868.8133, 2868.4390, 55.9186>>	fHeading = 200.5778		BREAK	//TRAILERS + BOAT - COUNTRY
	ENDSWITCH
	PRINTLN("     ---------->     DESTROY VEH - GET_RANDOM_PARKED_UP_LOCATION - vLocation = ", vLocation, " fHeading = ", fHeading)
ENDPROC

PROC GET_ON_FOOT_PED_LOCATION()
	SWITCH serverBD.iRandomLocation
		//BEACH DINER
		CASE 0		
			serverBD.vDriverOnFoot = <<-2195.7825, -420.2502, 12.0819>>		serverBD.fDriverOnFootHeading = 117.3927		//Driver
			serverBD.vExtraOnFoot[0] = <<-2189.9753, -422.8502, 12.0709>>	serverBD.fExtraOnFootHeading[0] = 152.6699		//Extra 1
			serverBD.vExtraOnFoot[1] = <<-2184.0315, -400.9480, 12.2027>>	serverBD.fExtraOnFootHeading[1] = 337.9892		//Extra 1
			serverBD.vExtraOnFoot[2] = <<-2175.2817, -426.3756, 12.1280>>	serverBD.fExtraOnFootHeading[2] = 12.2947		//Extra 1
		BREAK	
		
		//CANALS POOL
		CASE 1		
			serverBD.vDriverOnFoot = <<-981.5774, -1487.3193, 4.5867>>		serverBD.fDriverOnFootHeading = 300.0815		//Driver
			serverBD.vExtraOnFoot[0] = <<-977.8269, -1494.5681, 4.5867>>	serverBD.fExtraOnFootHeading[0] = 0.7310		//Extra 1
			serverBD.vExtraOnFoot[1] = <<-977.7324, -1491.7174, 4.5867>>	serverBD.fExtraOnFootHeading[1] = 173.7717		//Extra 1
			serverBD.vExtraOnFoot[2] = <<-954.6693, -1486.7126, 4.1600>>	serverBD.fExtraOnFootHeading[2] = 304.8721		//Extra 1
		BREAK	
		
		//WAREHOUSE
		CASE 2		
			serverBD.vDriverOnFoot = <<697.3923, -1375.2698, 25.1960>>		serverBD.fDriverOnFootHeading = 111.1953		//Driver
			serverBD.vExtraOnFoot[0] = <<717.8624, -1360.4915, 25.0461>>	serverBD.fExtraOnFootHeading[0] = 55.1674		//Extra 1
			serverBD.vExtraOnFoot[1] = <<703.9843, -1361.0935, 24.6728>>	serverBD.fExtraOnFootHeading[1] = 306.7133		//Extra 1
			serverBD.vExtraOnFoot[2] = <<707.1707, -1388.7064, 25.2759>>	serverBD.fExtraOnFootHeading[2] = 204.5666		//Extra 1
		BREAK	
		
		//SPITROASTERS
		CASE 3		
			serverBD.vDriverOnFoot = <<-258.5791, 302.4799, 91.0086>>		serverBD.fDriverOnFootHeading = 180.4109		//Driver
			serverBD.vExtraOnFoot[0] = <<-244.9193, 299.3712, 91.0424>>		serverBD.fExtraOnFootHeading[0] = 43.0354		//Extra 1
			serverBD.vExtraOnFoot[1] = <<-248.7361, 305.6155, 91.1444>>		serverBD.fExtraOnFootHeading[1] = 191.8736		//Extra 1
			serverBD.vExtraOnFoot[2] = <<-232.4524, 299.4706, 91.2088>>		serverBD.fExtraOnFootHeading[2] = 199.8353		//Extra 1	
		BREAK	
		
		//TRAILERS + BOAT - COUNTRY
		CASE 4		
			serverBD.vDriverOnFoot = <<862.7026, 2875.1482, 56.9868>>		serverBD.fDriverOnFootHeading = 206.7217		//Driver
			serverBD.vExtraOnFoot[0] = <<880.2893, 2862.0740, 55.6697>>		serverBD.fExtraOnFootHeading[0] = 191.0122		//Extra 1
			serverBD.vExtraOnFoot[1] = <<865.2399, 2853.5356, 56.2583>>		serverBD.fExtraOnFootHeading[1] = 305.8056		//Extra 1
			serverBD.vExtraOnFoot[2] = <<867.6559, 2841.1548, 56.9440>>		serverBD.fExtraOnFootHeading[2] = 235.4465		//Extra 1	
		BREAK	
	ENDSWITCH
ENDPROC

PROC GET_RANDOM_GANG()	
	INT iGang = GET_RANDOM_INT_IN_RANGE(0, MAX_GANGS)
	IF g_sMPTunables.iDestroyVehGang >= 0
	AND g_sMPTunables.iDestroyVehGang < MAX_GANGS
		iGang = g_sMPTunables.iDestroyVehGang
		PRINTLN("     ---------->     DESTROY VEH - GET_RANDOM_GANG - USE TUNABLE iDestroyVehGang = ", g_sMPTunables.iDestroyVehGang)
	ENDIF
	
	SWITCH iGang
		CASE 0	//BALLAS
			IF GET_RANDOM_BOOL()	serverBD.VehModel = BALLER				ELSE	serverBD.VehModel = ZION					ENDIF
			IF GET_RANDOM_BOOL()	serverBD.PedModel = G_M_Y_BALLAORIG_01	ELSE	serverBD.PedModel = G_M_Y_BALLAEAST_01		ENDIF
		BREAK
		CASE 1	//VAGOS
			IF GET_RANDOM_BOOL()	serverBD.VehModel = EMPEROR				ELSE	serverBD.VehModel = CAVALCADE2				ENDIF
			IF GET_RANDOM_BOOL()	serverBD.PedModel = G_M_Y_MEXGOON_02	ELSE	serverBD.PedModel = G_F_Y_VAGOS_01			ENDIF
		BREAK
		CASE 2	//LOST
			IF GET_RANDOM_BOOL()	serverBD.VehModel = DAEMON				ELSE	serverBD.VehModel = GBURRITO				ENDIF
			IF GET_RANDOM_BOOL()	serverBD.PedModel = G_M_Y_LOST_01		ELSE	serverBD.PedModel = G_F_Y_LOST_01			ENDIF
		BREAK
		CASE 3	//CHINESE
			IF GET_RANDOM_BOOL()	serverBD.VehModel = PCJ					ELSE	serverBD.VehModel = BJXL					ENDIF
			IF GET_RANDOM_BOOL()	serverBD.PedModel = G_M_M_CHIGOON_02	ELSE	serverBD.PedModel = G_M_M_CHIGOON_01		ENDIF
		BREAK
		CASE 4	//ARMENIAN
			IF GET_RANDOM_BOOL()	serverBD.VehModel = ROCOTO				ELSE	serverBD.VehModel = EMPEROR					ENDIF
			IF GET_RANDOM_BOOL()	serverBD.PedModel = G_M_M_ARMGOON_01	ELSE	serverBD.PedModel = G_M_Y_ARMGOON_02		ENDIF
		BREAK
		CASE 5	//HILLBILLY
			IF GET_RANDOM_BOOL()	serverBD.VehModel = JOURNEY				ELSE	serverBD.VehModel = SANDKING				ENDIF
			IF GET_RANDOM_BOOL()	serverBD.PedModel = A_M_M_HILLBILLY_02	ELSE	serverBD.PedModel = A_M_M_HILLBILLY_01		ENDIF
		BREAK
		CASE 6	//SALVA
			IF GET_RANDOM_BOOL()	serverBD.VehModel = HABANERO			ELSE	serverBD.VehModel = SANCHEZ2				ENDIF
			IF GET_RANDOM_BOOL()	serverBD.PedModel = G_M_Y_SALVAGOON_01	ELSE	serverBD.PedModel = G_M_Y_SALVAGOON_02		ENDIF
		BREAK
		CASE 7	//STREET PUNK
			IF GET_RANDOM_BOOL()	serverBD.VehModel = BUFFALO2			ELSE	serverBD.VehModel = DUBSTA					ENDIF
			IF GET_RANDOM_BOOL()	serverBD.PedModel = G_M_Y_STRPUNK_01	ELSE	serverBD.PedModel = G_M_Y_STRPUNK_02		ENDIF
		BREAK
	ENDSWITCH
	PRINTLN("     ---------->     DESTROY VEH - GET_RANDOM_GANG - iGang = ", serverBD.iGang)
	PRINTLN("     ---------->     DESTROY VEH - GET_RANDOM_GANG - PedModel = ", GET_MODEL_NAME_FOR_DEBUG(serverBD.PedModel))
	PRINTLN("     ---------->     DESTROY VEH - GET_RANDOM_GANG - VehModel = ", GET_MODEL_NAME_FOR_DEBUG(serverBD.VehModel))
ENDPROC

//PURPOSE: Gives the passed in a ped a random Gang Weapon
FUNC WEAPON_TYPE GET_RANDOM_GANG_WEAPON()
	SWITCH serverBD.PedModel
	
		//BALLA
		CASE G_M_Y_BALLAORIG_01
		CASE G_M_Y_BALLAEAST_01
			SWITCH GET_RANDOM_INT_IN_RANGE(0, 5)
				CASE 0	RETURN WEAPONTYPE_COMBATPISTOL
				CASE 1	RETURN WEAPONTYPE_PUMPSHOTGUN
				CASE 2	RETURN WEAPONTYPE_MICROSMG
				CASE 3	RETURN WEAPONTYPE_MG
				CASE 4	RETURN WEAPONTYPE_ASSAULTRIFLE
			ENDSWITCH
		BREAK
		
		//VAGOS
		CASE G_M_Y_MEXGOON_02
		CASE G_F_Y_VAGOS_01
			SWITCH GET_RANDOM_INT_IN_RANGE(0, 5)
				CASE 0		RETURN WEAPONTYPE_PISTOL
				CASE 1		RETURN WEAPONTYPE_PUMPSHOTGUN
				CASE 2		RETURN WEAPONTYPE_MICROSMG
				CASE 3		RETURN WEAPONTYPE_MG
				CASE 4		RETURN WEAPONTYPE_ASSAULTRIFLE
			ENDSWITCH
		BREAK
	
		//LOST
		CASE G_M_Y_LOST_01
		CASE G_F_Y_LOST_01
			SWITCH GET_RANDOM_INT_IN_RANGE(0, 5)
				CASE 0	RETURN WEAPONTYPE_COMBATPISTOL
				CASE 1	RETURN WEAPONTYPE_SAWNOFFSHOTGUN
				CASE 2	RETURN WEAPONTYPE_SMG
				CASE 3	RETURN WEAPONTYPE_MG
				CASE 4	RETURN WEAPONTYPE_ASSAULTRIFLE
			ENDSWITCH
		BREAK
		
		//CHINESE
		CASE G_M_M_CHIGOON_02
		CASE G_M_M_CHIGOON_01
			SWITCH GET_RANDOM_INT_IN_RANGE(0, 5)
				CASE 0	RETURN WEAPONTYPE_PISTOL
				CASE 1	RETURN WEAPONTYPE_PUMPSHOTGUN
				CASE 2	RETURN WEAPONTYPE_ADVANCEDRIFLE
				CASE 3	RETURN WEAPONTYPE_MG
				CASE 4	RETURN WEAPONTYPE_CARBINERIFLE
			ENDSWITCH
		BREAK
	
		//ARMENIAN
		CASE G_M_M_ARMGOON_01
		CASE G_M_Y_ARMGOON_02
			SWITCH GET_RANDOM_INT_IN_RANGE(0, 5)
				CASE 0	RETURN WEAPONTYPE_PISTOL
				CASE 1	RETURN WEAPONTYPE_SAWNOFFSHOTGUN
				CASE 2	RETURN WEAPONTYPE_SMG
				CASE 3	RETURN WEAPONTYPE_MG
				CASE 4	RETURN WEAPONTYPE_CARBINERIFLE
			ENDSWITCH
		BREAK
		
		//HILLBILLY
		CASE A_M_M_HILLBILLY_02
		CASE A_M_M_HILLBILLY_01
			SWITCH GET_RANDOM_INT_IN_RANGE(0, 5)
				CASE 0	RETURN WEAPONTYPE_COMBATPISTOL
				CASE 1	RETURN WEAPONTYPE_SAWNOFFSHOTGUN
				CASE 2	RETURN WEAPONTYPE_MICROSMG
				CASE 3	RETURN WEAPONTYPE_MG
				CASE 4	RETURN WEAPONTYPE_COMBATMG
			ENDSWITCH
		BREAK
	
		//SALVA
		CASE G_M_Y_SALVAGOON_01
		CASE G_M_Y_SALVAGOON_02
			SWITCH GET_RANDOM_INT_IN_RANGE(0, 5)
				CASE 0	RETURN WEAPONTYPE_PISTOL
				CASE 1	RETURN WEAPONTYPE_PUMPSHOTGUN
				CASE 2	RETURN WEAPONTYPE_MICROSMG
				CASE 3	RETURN WEAPONTYPE_MG
				CASE 4	RETURN WEAPONTYPE_ASSAULTRIFLE
			ENDSWITCH
		BREAK
		
		//STREET PUNK
		CASE G_M_Y_STRPUNK_01
		CASE G_M_Y_STRPUNK_02
			SWITCH GET_RANDOM_INT_IN_RANGE(0, 5)
				CASE 0	RETURN WEAPONTYPE_PISTOL
				CASE 1	RETURN WEAPONTYPE_SAWNOFFSHOTGUN
				CASE 2	RETURN WEAPONTYPE_MICROSMG
				CASE 3	RETURN WEAPONTYPE_MG
				CASE 4	RETURN WEAPONTYPE_ASSAULTRIFLE
			ENDSWITCH
		BREAK
		
		DEFAULT
			SWITCH GET_RANDOM_INT_IN_RANGE(0, 5)
				CASE 0	RETURN WEAPONTYPE_PISTOL
				CASE 1	RETURN WEAPONTYPE_PUMPSHOTGUN
				CASE 2	RETURN WEAPONTYPE_SMG
				CASE 3	RETURN WEAPONTYPE_COMBATMG
				CASE 4	RETURN WEAPONTYPE_CARBINERIFLE
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN WEAPONTYPE_PISTOL
ENDFUNC

//PURPOSE: Sets the data for this instance of Destroy Veh
PROC GET_RANDOM_SETUP_DETAILS()
	GET_RANDOM_GANG()
		
	serverBD.iVehicleTaskType = GET_RANDOM_INT_IN_RANGE(0, VTT_MAX)
	PRINTLN("     ---------->     DESTROY VEH - GET_RANDOM_SETUP_DETAILS - iVehicleTaskType = ", serverBD.iVehicleTaskType)
	
	//serverBD.iVehicleTaskType = VTT_PARKED  //TEST DEBUG
	
	IF serverBD.iVehicleTaskType = VTT_PARKED
	OR serverBD.iVehicleTaskType = VTT_DRIVE_TO
		serverBD.iRandomLocation = GET_RANDOM_INT_IN_RANGE(0, MAX_NUM_DESTROY_VEH_LOCATIONS)
		IF serverBD.iVehicleTaskType = VTT_PARKED
			GET_RANDOM_PARKED_UP_LOCATION(serverBD.vStartLocation, serverBD.fStartHeading)
		ELSE
			GET_RANDOM_PARKED_UP_LOCATION(serverBD.vEndLocation, serverBD.fEndHEading)
		ENDIF
		GET_ON_FOOT_PED_LOCATION()
	ENDIF
	
	serverBD.iNumExtras = GET_RANDOM_INT_IN_RANGE(0, MAX_NUM_EXTRA_PEDS)
	PRINTLN("     ---------->     DESTROY VEH - GET_RANDOM_SETUP_DETAILS - iNumExtras = ", serverBD.iNumExtras)
	
	IF g_sMPTunables.iDestroyVehBonus >= 0
	AND g_sMPTunables.iDestroyVehBonus < DB_MAX
		serverBD.iDestroyBonus = g_sMPTunables.iDestroyVehBonus
		PRINTLN("     ---------->     DESTROY VEH - GET_RANDOM_SETUP_DETAILS - USE TUNABLE iDestroyVehBonus = ", g_sMPTunables.iDestroyVehBonus)
	ELSE
		serverBD.iDestroyBonus = GET_RANDOM_INT_IN_RANGE(0, DB_MAX)
	ENDIF
	PRINTLN("     ---------->     DESTROY VEH - GET_RANDOM_SETUP_DETAILS - iDestroyBonus = ", serverBD.iDestroyBonus)
ENDPROC

//Do necessary pre game start ini.
PROC PROCESS_PRE_GAME( MP_MISSION_DATA missionScriptArgs )
	
	// This marks the script as a net script, and handles any instancing setup. 
	SETUP_MP_MISSION_FOR_NETWORK(GET_MAX_NUM_PARTICIPANTS_FOR_MP_MISSION(missionScriptArgs.mdID.idMission) ,  missionScriptArgs)	//NUM_NETWORK_PLAYERS, missionScriptArgs)	//
	//NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE, PARTICIPANT_ID_TO_INT())  
	
	//Reserve Entities
	RESERVE_NETWORK_MISSION_PEDS(4)
	RESERVE_NETWORK_MISSION_VEHICLES(1)

	// This makes sure the net script is active, waits untill it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD, SIZE_OF(playerBD))
	
	// KGM: Wait for the first network broadcast before moving on
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		#IF IS_DEBUG_BUILD NET_LOG("DESTROY VEH -  FAILED TO RECEIVE INITIAL NETWORK BROADCAST")	#ENDIF
		NET_PRINT_TIME() NET_PRINT("     ---------->     DESTROY VEH -  MISSION END - FAILED TO RECEIVE INITIAL NETWORK BROADCAST - SCRIPT CLEANUP D     <----------     ") NET_NL()
		SCRIPT_CLEANUP()
	ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		// This script will not be paused if another script calls PAUSE_GAME
		SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			GET_RANDOM_SETUP_DETAILS()
			serverBD.piDestroyer = INVALID_PLAYER_INDEX()
		ENDIF
		
		SET_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_DESTROY_VEH, TRUE)
		
		playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_INI
		NET_PRINT_TIME() NET_PRINT("     ---------->     DESTROY VEH -  PRE_GAME DONE     <----------     ") NET_NL()
	ELSE
		#IF IS_DEBUG_BUILD NET_LOG("DESTROY VEH -  NETWORK_IS_GAME_IN_PROGRESS = FALSE")	#ENDIF
		NET_PRINT_TIME() NET_PRINT("     ---------->     DESTROY VEH -  MISSION END - NETWORK_IS_GAME_IN_PROGRESS = FALSE - SCRIPT CLEANUP Q     <----------     ") NET_NL()
		SCRIPT_CLEANUP()
	ENDIF
ENDPROC

FUNC BOOL MISSION_END_CHECK()
	//Veh Crashed
	IF serverBD.eDestroyStage = eAD_DESTROY
		IF serverBD.iServerGameState != GAME_STATE_END
			IF IS_ENTITY_DEAD(NET_TO_VEH(serverBD.niVeh))
				IF HAS_NET_TIMER_EXPIRED(serverBD.DestroyCheckDelayTimer, 3000)	//6000
					//Send Ticker
					SCRIPT_EVENT_DATA_TICKER_MESSAGE TickerEventData
					IF serverBD.piDestroyer = INVALID_PLAYER_INDEX()
						TickerEventData.TickerEvent = TICKER_EVENT_DV_VEH_DESTROYED
						BROADCAST_TICKER_EVENT(TickerEventData, ALL_PLAYERS_ON_SCRIPT())
					ELSE
						TickerEventData.TickerEvent = TICKER_EVENT_DV_VEH_DESTROYED_BY_PLAYER
						TickerEventData.playerID = serverBD.piDestroyer
						BROADCAST_TICKER_EVENT(TickerEventData, ALL_PLAYERS_ON_SCRIPT())
					ENDIF
					
					NET_PRINT_TIME() NET_PRINT("     ---------->     DESTROY VEH -  MISSION END - VEH DESTROYED     <----------     ") NET_NL()
					RETURN TRUE
				ENDIF
			ENDIF
			IF HAS_NET_TIMER_EXPIRED(serverBD.ForceCleanup, g_sMPTunables.iDestroy_Vehicle_Expiry_Time)
				SCRIPT_EVENT_DATA_TICKER_MESSAGE TickerEventData
				TickerEventData.TickerEvent = TICKER_EVENT_DV_EXPIRY_TIME
				BROADCAST_TICKER_EVENT(TickerEventData, ALL_PLAYERS_ON_SCRIPT())
				NET_PRINT_TIME() NET_PRINT("     ---------->     DESTROY VEH -  MISSION END - HAS_NET_TIMER_EXPIRED     <----------     ") NET_NL()
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//Server only function that checks to see if the game mode should now end. For this test script, it if time has passed.
FUNC BOOL HAVE_MISSION_END_CONDITIONS_BEEN_MET()
	
	#IF IS_DEBUG_BUILD
		IF bHostEndMissionNow
			RETURN TRUE
		ENDIF
	#ENDIF	
	
	IF MISSION_END_CHECK()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


//Helper function to get a clients game/mission state
FUNC INT GET_CLIENT_MISSION_STATE(INT iPlayer)
	RETURN playerBD[iPlayer].iGameState
ENDFUNC

#IF IS_DEBUG_BUILD
PROC CREATE_WIDGETS()
	INT iPlayer
	TEXT_LABEL_15 tl63

	START_WIDGET_GROUP("DESTROY VEH")  
		ADD_WIDGET_BOOL("Warp to Veh", bWarpToVeh)
				
		// Gameplay debug, sever only.
		START_WIDGET_GROUP("Server Only Gameplay") 
			ADD_WIDGET_BOOL("End mission", bHostEndMissionNow)			
		STOP_WIDGET_GROUP()		
		
		// Data about the server
		START_WIDGET_GROUP("Server BD") 
			ADD_WIDGET_INT_SLIDER("S. Game state", serverBD.iServerGameState,-1, HIGHEST_INT,1)
		STOP_WIDGET_GROUP()	

		// Data about the clients. * = You.
		START_WIDGET_GROUP("Client BD")  				
			REPEAT NUM_NETWORK_PLAYERS iPlayer
				tl63 = "Player "
				tl63 += iPlayer
				IF iPlayer = PARTICIPANT_ID_TO_INT()
					tl63 += "*"
				ENDIF
				START_WIDGET_GROUP(tl63)
					ADD_WIDGET_INT_SLIDER("Game state", playerBD[iPlayer].iGameState,-1, HIGHEST_INT,1)
				STOP_WIDGET_GROUP()
			ENDREPEAT				
		STOP_WIDGET_GROUP()	
		
	STOP_WIDGET_GROUP()
ENDPROC		

PROC UPDATE_WIDGETS()
	//Warp player to Veh
	IF bWarpToVeh = TRUE
		IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niVeh)
			IF NET_WARP_TO_COORD(GET_ENTITY_COORDS(NET_TO_VEH(serverBD.niVeh))+<<0, 0, 3>>, 0, TRUE, FALSE)
				NET_PRINT_TIME() NET_PRINT("     ---------->     DESTROY VEH -  UPDATE_WIDGETS - bWarpToVeh DONE    <----------     ") NET_NL()
				bWarpToVeh = FALSE
			ELSE
				NET_PRINT_TIME() NET_PRINT("     ---------->     DESTROY VEH -  UPDATE_WIDGETS - bWarpToVeh IN PROGRESS    <----------     ") NET_NL()
			ENDIF
		ENDIF
	ENDIF
ENDPROC

#ENDIF

//PURPOSE: Adds a blip to the Veh
PROC ADD_VEH_BLIP()
	IF DOES_BLIP_EXIST(VehBlip)
		IF SHOULD_HIDE_LOCAL_UI()
		OR ARE_TEMP_HIDDEN_BLIPS_ACTIVE()
			SET_BLIP_DISPLAY(VehBlip, DISPLAY_NOTHING)
		ELSE
			SET_BLIP_DISPLAY(VehBlip, DISPLAY_BOTH)
		ENDIF
	ELSE
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niVeh)
			IF NOT IS_ENTITY_DEAD(NET_TO_VEH(serverBD.niVeh))
				VehBlip = ADD_BLIP_FOR_ENTITY(NET_TO_VEH(serverBD.niVeh))
				IF NOT IS_THIS_MODEL_A_BIKE(serverBD.VehModel)
					SET_BLIP_SPRITE(VehBlip, RADAR_TRACE_GANG_VEHICLE)
				ELSE
					SET_BLIP_SPRITE(VehBlip, RADAR_TRACE_GANG_BIKE)
				ENDIF
				SET_BLIP_COLOUR(VehBlip, BLIP_COLOUR_RED)
				//SHOW_HEIGHT_ON_BLIP(VehBlip, TRUE)
				SET_BLIP_FLASH_TIMER(VehBlip, 7000)
				SET_BLIP_PRIORITY(VehBlip, BLIPPRIORITY_MED_HIGH)
				SET_BLIP_NAME_FROM_TEXT_FILE(VehBlip, "DSV_BLIP")
				IF SHOULD_HIDE_LOCAL_UI()
					SET_BLIP_DISPLAY(VehBlip, DISPLAY_NOTHING)
				ENDIF
				NET_PRINT_TIME() NET_PRINT("     ---------->     DESTROY VEH -  VEH BLIP ADDED ") NET_NL()
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Sets common attributes for the peds.
PROC SET_PED_ATTRIBUTES(NETWORK_INDEX &thisPed)
	INT iRand
	//SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NET_TO_PED(serverBD.niDriver), TRUE)
	SET_PED_RELATIONSHIP_GROUP_HASH(NET_TO_PED(thisPed), rgFM_AiDislike)
	SET_PED_RANDOM_COMPONENT_VARIATION(NET_TO_PED(thisPed))
	SET_PED_KEEP_TASK(NET_TO_PED(thisPed), TRUE)
	
	GIVE_DELAYED_WEAPON_TO_PED(NET_TO_PED(thisPed), GET_RANDOM_GANG_WEAPON(), 25000, TRUE)
	
	//Combat Ability
	iRand = GET_RANDOM_INT_IN_RANGE(0, 10)
	IF iRand = 0
	OR iRand = 1
		SET_PED_COMBAT_ABILITY(NET_TO_PED(thisPed), CAL_POOR)
		PRINTLN("     ---------->     DESTROY VEH - SET_PED_ATTRIBUTES - COMBAT ABBILITY = CAL_POOR")
	ELIF iRand = 2
	OR iRand = 3
	OR iRand = 4
		SET_PED_COMBAT_ABILITY(NET_TO_PED(thisPed), CAL_PROFESSIONAL)
		PRINTLN("     ---------->     DESTROY VEH - SET_PED_ATTRIBUTES - COMBAT ABBILITY = CAL_PROFESSIONAL")
	ELSE
		SET_PED_COMBAT_ABILITY(NET_TO_PED(thisPed), CAL_AVERAGE)
		PRINTLN("     ---------->     DESTROY VEH - SET_PED_ATTRIBUTES - COMBAT ABBILITY = CAL_AVERAGE")
	ENDIF

	//Combat Attributes
	iRand = GET_RANDOM_INT_IN_RANGE(0, 10)
	IF iRand = 0
	OR iRand = 1
	OR iRand = 2
		SET_PED_COMBAT_ATTRIBUTES(NET_TO_PED(thisPed), CA_ALWAYS_FIGHT, TRUE)
		PRINTLN("     ---------->     DESTROY VEH - SET_PED_ATTRIBUTES - COMBAT ATTRIBUTE = CA_ALWAYS_FIGHT")
	ELIF iRand = 3
		SET_PED_COMBAT_ATTRIBUTES(NET_TO_PED(thisPed), CA_ALWAYS_FLEE, TRUE)
		PRINTLN("     ---------->     DESTROY VEH - SET_PED_ATTRIBUTES - COMBAT ATTRIBUTE = CA_ALWAYS_FLEE")
	ELIF iRand = 4
	OR iRand = 5
	OR iRand = 6
		SET_PED_COMBAT_ATTRIBUTES(NET_TO_PED(thisPed), CA_AGGRESSIVE, TRUE)
		PRINTLN("     ---------->     DESTROY VEH - SET_PED_ATTRIBUTES - COMBAT ATTRIBUTE = CA_AGGRESSIVE")
	ENDIF
	IF GET_RANDOM_BOOL()
		SET_PED_COMBAT_ATTRIBUTES(NET_TO_PED(thisPed), CA_CAN_TAUNT_IN_VEHICLE, TRUE)
		PRINTLN("     ---------->     DESTROY VEH - SET_PED_ATTRIBUTES - CA_CAN_TAUNT_IN_VEHICLE = TRUE")
	ENDIF
	
	SET_PED_COMBAT_ATTRIBUTES(NET_TO_PED(thisPed), CA_DO_DRIVEBYS, TRUE)

	//Combat Movement
	iRand = GET_RANDOM_INT_IN_RANGE(0, 4)
	IF iRand = 0
		SET_PED_COMBAT_MOVEMENT(NET_TO_PED(thisPed), CM_WILLADVANCE)
		PRINTLN("     ---------->     DESTROY VEH - SET_PED_ATTRIBUTES - COMBAT MOVEMENT = CM_WILLADVANCE")
	ELIF iRand = 1
		SET_PED_COMBAT_MOVEMENT(NET_TO_PED(thisPed), CM_DEFENSIVE)
		PRINTLN("     ---------->     DESTROY VEH - SET_PED_ATTRIBUTES - COMBAT MOVEMENT = CM_DEFENSIVE")
	ELIF iRand = 2
		SET_PED_COMBAT_MOVEMENT(NET_TO_PED(thisPed), CM_WILLRETREAT)
		PRINTLN("     ---------->     DESTROY VEH - SET_PED_ATTRIBUTES - COMBAT MOVEMENT = CM_WILLRETREAT")
	ENDIF
	
	SET_PED_ALLOW_MINOR_REACTIONS_AS_MISSION_PED(NET_TO_PED(thisPed), TRUE)
	SET_PED_GET_OUT_UPSIDE_DOWN_VEHICLE(NET_TO_PED(thisPed), TRUE)
	SET_PED_CONFIG_FLAG(NET_TO_PED(thisPed), PCF_GetOutUndriveableVehicle, TRUE)
	SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(NET_TO_PED(thisPed), KNOCKOFFVEHICLE_HARD)

	SET_ENTITY_HEALTH(NET_TO_PED(thisPed), ROUND(200*g_sMPTunables.fAiHealthModifier))
	PRINTLN("     ---------->     DESTROY VEH - SET_PED_ATTRIBUTES - HEALTH = ", ROUND(200*g_sMPTunables.fAiHealthModifier))
ENDPROC

PROC GIVE_DRIVER_INITIAL_TASK()
	IF serverBD.iVehicleTaskType = VTT_WANDER
		IF GET_RANDOM_BOOL()
			TASK_VEHICLE_DRIVE_WANDER(NET_TO_PED(serverBD.niDriver), NET_TO_VEH(serverBD.niVeh), GET_RANDOM_FLOAT_IN_RANGE(7.0, 18.0), DRIVINGMODE_AVOIDCARS_RECKLESS)
			PRINTLN("     ---------->     DESTROY VEH - GIVE_DRIVER_INITIAL_TASK - GIVEN WANDER - RECKLESS")
		ELSE
			TASK_VEHICLE_DRIVE_WANDER(NET_TO_PED(serverBD.niDriver), NET_TO_VEH(serverBD.niVeh), GET_RANDOM_FLOAT_IN_RANGE(7.0, 18.0), DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS)
			PRINTLN("     ---------->     DESTROY VEH - GIVE_DRIVER_INITIAL_TASK - GIVEN WANDER - WELL BEHAVED")
		ENDIF
		
	ELIF serverBD.iVehicleTaskType = VTT_DRIVE_TO
		CREATE_SEQUENCES()
		TASK_PERFORM_SEQUENCE(NET_TO_PED(serverBD.niDriver), seqDriveTo)
		PRINTLN("     ---------->     DESTROY VEH - GIVE_DRIVER_INITIAL_TASK - GIVEN DRIVE TO SEQUENCE")
		
	ELSE
		PRINTLN("     ---------->     DESTROY VEH - GIVE_DRIVER_INITIAL_TASK - NOTHING")
	ENDIF
ENDPROC


/// PURPOSE:
///    Finds coords to spawn the Veh at.
/// RETURNS:
///    BOOL - TRUE if found cords, FALSE if not. 
FUNC BOOL GOT_SAFE_CREATION_POINT(VECTOR &vPos, FLOAT &fHeading)
	
	BOOL bFoundSuitableNode
	INT iNumLanes
	VECTOR resultLinkDir
	VECTOR vCentre = GET_PLAYER_COORDS(PLAYER_ID())
	
	//Already Done
	IF serverBD.iVehicleTaskType = VTT_PARKED
		RETURN TRUE
	ENDIF
	
	IF NOT bFoundSuitableNode
		//IF GET_NTH_CLOSEST_VEHICLE_NODE_WITH_HEADING(GET_PLAYER_COORDS(NETWORK_GET_PLAYER_INDEX(PARTICIPANT_ID())), iSpawnNodeCount, vPos, fHeading, iNumLanes, NF_NONE)
		IF GENERATE_VEHICLE_CREATION_POS_FROM_PATHS(vCentre, vPos, resultLinkDir, 0, 180, 150)
			GET_NTH_CLOSEST_VEHICLE_NODE_WITH_HEADING(vPos, 1, vPos, fHeading, iNumLanes, NF_IGNORE_SLIPLANES)
			
			IF (iNumLanes >= 1)	//2)
			//OR (GET_HASH_OF_MAP_AREA_AT_COORDS(vPos) = MAP_AREA_CITY)
				IF GET_DISTANCE_BETWEEN_COORDS(<<-1367.5571, -3220.5977, 12.9448>>, vCentre) >= 600.0 // Don't spawn in aeroplane hangars or runways.
				AND GET_DISTANCE_BETWEEN_COORDS(<<750, -3200, 6>>, vCentre) >= 700.0
					//IF GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(PLAYER_ID()), vPos) <= (LEAVE_RANGE - 50.0)
						IF iCheckCount <= 5
							IF IS_POINT_OK_FOR_NET_ENTITY_CREATION(vPos)
								NET_PRINT("     ---------->     DESTROY VEH - GOT_SAFE_CREATION_POINT - found suitable node to spawn veh - iSpawnNodeCount = ")NET_PRINT_INT(iSpawnNodeCount)NET_NL()
								bFoundSuitableNode = TRUE
							ENDIF
						ELSE
							NET_PRINT("     ---------->     DESTROY VEH - GOT_SAFE_CREATION_POINT - could not find node that is ok for net entity creation after 100 checks - CLEANING UP SCRIPT")NET_NL()
							serverBD.iServerGameState = GAME_STATE_END
							NET_PRINT_TIME() NET_PRINT("     ---------->     DESTROY VEH - serverBD.iServerGameState = GAME_STATE_END 5    <----------     ") NET_NL()
							#IF IS_DEBUG_BUILD NET_LOG_Host("iServerGameState = GAME_STATE_END 5")	#ENDIF
							//bFoundSuitableNode = TRUE
						ENDIF
					//ELSE
					//	NET_PRINT("     ---------->     DESTROY VEH - GOT_SAFE_CREATION_POINT - spawn coords > 200.0m away - node cannot be used.")NET_NL()
					//ENDIF
				ELSE
					NET_PRINT("     ---------->     DESTROY VEH - GOT_SAFE_CREATION_POINT - spawn coords <= 600.0m from airport/docks.")NET_NL()
				ENDIF
			ELSE
				NET_PRINT("     ---------->     DESTROY VEH - GOT_SAFE_CREATION_POINT - iNumLanes < 2, node cannot be used.")NET_NL()
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT bFoundSuitableNode
		iSpawnNodeCount += 4
		//NET_PRINT("     ---------->     DESTROY VEH - GOT_SAFE_CREATION_POINT - not found suitable node, iSpawnNodeCount = ") NET_PRINT_INT(iSpawnNodeCount) NET_NL()
		IF iSpawnNodeCount >= 80
			iSpawnNodeCount = START_NODE
			iCheckCount++
			NET_PRINT("     ---------->     DESTROY VEH - GOT_SAFE_CREATION_POINT - iCheckCount = ") NET_PRINT_INT(iCheckCount) NET_NL()
		ENDIF
	ENDIF
	
	RETURN bFoundSuitableNode
ENDFUNC

//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                  ENTITY CREATION                  /////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////
///    		VEH AND DRIVER    			///
///////////////////////////////////////////
//PURPOSE: Create the Veh
FUNC BOOL CREATE_VEH()
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niVeh)
		IF REQUEST_LOAD_MODEL(serverBD.VehModel)
			IF GOT_SAFE_CREATION_POINT(serverBD.vStartLocation, serverBD.fStartHeading)
				IF CREATE_NET_VEHICLE(serverBD.niVeh, serverBD.VehModel, serverBD.vStartLocation, serverBD.fStartHeading, DEFAULT, DEFAULT, DEFAULT, TRUE, FALSE)	
					//SET_ENTITY_HEALTH(NET_TO_VEH(serverBD.niVeh), serverBD.CrateDropData.VehData.Veh.iHealth)
					//SET_VEHICLE_ENGINE_HEALTH(NET_TO_VEH(serverBD.niVeh), TO_FLOAT(serverBD.CrateDropData.VehData.Veh.iHealth))
					//SET_VEHICLE_PETROL_TANK_HEALTH(NET_TO_VEH(serverBD.niVeh), TO_FLOAT(serverBD.CrateDropData.VehData.Veh.iHealth))
					//SET_VEHICLE_DOORS_LOCKED(NET_TO_VEH(serverBD.niVeh), VEHICLELOCK_LOCKED_BUT_CAN_BE_DAMAGED)
					
					FREEZE_ENTITY_POSITION(NET_TO_VEH(serverBD.niVeh), FALSE)
					SET_ENTITY_DYNAMIC(NET_TO_VEH(serverBD.niVeh), TRUE)
					ACTIVATE_PHYSICS(NET_TO_VEH(serverBD.niVeh))
					SET_VEHICLE_ENGINE_ON(NET_TO_VEH(serverBD.niVeh), TRUE, TRUE)
					SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(NET_TO_VEH(serverBD.niVeh), FALSE)
					
					IF DECOR_IS_REGISTERED_AS_TYPE("Not_Allow_As_Saved_Veh", DECOR_TYPE_INT)
                        DECOR_SET_INT(NET_TO_VEH(serverBD.niVeh), "Not_Allow_As_Saved_Veh", MP_NOT_ALLOWED_AS_SAVED_VEHICLE_DEC_SIMEON)
						PRINTLN("     ---------->     DESTROY VEH - DECORATOR 'Not_Allow_As_Saved_Veh' SET")
      				ENDIF
					
					//Prevents the vehicle from being used for activities that use passive mode - MJM 
					IF DECOR_IS_REGISTERED_AS_TYPE("MPBitset", DECOR_TYPE_INT)
						INT iDecoratorValue
						IF DECOR_EXIST_ON(NET_TO_VEH(serverBD.niVeh), "MPBitset")
							iDecoratorValue = DECOR_GET_INT(NET_TO_VEH(serverBD.niVeh), "MPBitset")
						ENDIF
						SET_BIT(iDecoratorValue, MP_DECORATOR_BS_NON_PASSIVE_MODE_VEHICLE)
						DECOR_SET_INT(NET_TO_VEH(serverBD.niVeh), "MPBitset", iDecoratorValue)
					ENDIF

					//Check we have enough Seats for Ped Extras
					IF serverBD.iVehicleTaskType = VTT_WANDER
						IF GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(NET_TO_VEH(serverBD.niVeh)) < serverBD.iNumExtras
							serverBD.iNumExtras = GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(NET_TO_VEH(serverBD.niVeh))
							PRINTLN("     ---------->     DESTROY VEH - NOT ENOUGH SEATS FOR EXTRAS - CAP NUM EXTRAS - iNumExtras = ", serverBD.iNumExtras)
						ENDIF
					ENDIF
					
					NET_PRINT_TIME() NET_PRINT("     ---------->     DESTROY VEH -  CREATED VEH - ") NET_PRINT_VECTOR(serverBD.vStartLocation) NET_NL()
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//Check we have created the vehicle
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niVeh)
		//NET_PRINT_TIME() NET_PRINT("     ---------->     DESTROY VEH -  FAILING ON VEH CREATION") NET_NL()
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

//PURPOSE: Create the Driver
FUNC BOOL CREATE_DRIVER()
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niDriver)
	AND REQUEST_LOAD_MODEL(serverBD.PedModel)
	AND NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niVeh)
		IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niVeh)
			IF serverBD.iVehicleTaskType = VTT_PARKED
				IF CREATE_NET_PED(serverBD.niDriver, PEDTYPE_CRIMINAL, serverBD.PedModel, serverBD.vDriverOnFoot, serverBD.fDriverOnFootHeading)
					SET_PED_ATTRIBUTES(serverBD.niDriver)
					NET_PRINT_TIME() NET_PRINT("     ---------->     DESTROY VEH - CREATED DRIVER ON FOOT") NET_NL()
					GIVE_DRIVER_INITIAL_TASK()
				ENDIF
			ELSE
				IF CREATE_NET_PED_IN_VEHICLE(serverBD.niDriver, serverBD.niVeh, PEDTYPE_CRIMINAL, serverBD.PedModel, VS_DRIVER)
					SET_PED_ATTRIBUTES(serverBD.niDriver)
					NET_PRINT_TIME() NET_PRINT("     ---------->     DESTROY VEH - CREATED DRIVER IN VEH") NET_NL()
					GIVE_DRIVER_INITIAL_TASK()
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//Check we have created the ped
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niDriver)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

//PURPOSE: Creates the Extra Peds
FUNC BOOL CREATE_EXTRA_PEDS()
	INT i
	REPEAT serverBD.iNumExtras i
		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niExtras[i])
		AND REQUEST_LOAD_MODEL(serverBD.PedModel)
		AND NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niVeh)
			IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niVeh)
				IF serverBD.iVehicleTaskType = VTT_WANDER
					IF CREATE_NET_PED_IN_VEHICLE(serverBD.niExtras[i], serverBD.niVeh, PEDTYPE_CRIMINAL, serverBD.PedModel, INT_TO_ENUM(VEHICLE_SEAT, i))
						SET_PED_ATTRIBUTES(serverBD.niExtras[i])
						PRINTLN("     ---------->     DESTROY VEH - CREATED EXTRA PED ", i, " IN VEH")
					ENDIF
				ELSE
					IF CREATE_NET_PED(serverBD.niExtras[i], PEDTYPE_CRIMINAL, serverBD.PedModel, serverBD.vExtraOnFoot[i], serverBD.fExtraOnFootHeading[i])
						SET_PED_ATTRIBUTES(serverBD.niExtras[i])
						PRINTLN("     ---------->     DESTROY VEH - CREATED EXTRA PED ", i, " ON FOOT")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	//Check we have created the peds
	REPEAT serverBD.iNumExtras i
		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niExtras[i])
			RETURN FALSE
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

//PURPOSE: Creates the Veh and Driver that will deliver the Vehicle
FUNC BOOL CREATE_VEH_AND_DRIVER()
	IF REQUEST_LOAD_MODEL(serverBD.VehModel)
	AND REQUEST_LOAD_MODEL(serverBD.PedModel)
		IF CREATE_VEH()
		AND CREATE_DRIVER()
		AND CREATE_EXTRA_PEDS()
			SET_MODEL_AS_NO_LONGER_NEEDED(serverBD.VehModel)
			SET_MODEL_AS_NO_LONGER_NEEDED(serverBD.PedModel)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Runs through the max num participents and does relevant checks
PROC MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER()
//	INT iParticipant
//	
//	//Initialise Staggered Loop
//	IF iStaggeredParticipant = 0
//		//bDoEndStaggeredLoopChecks = FALSE
//		//NET_PRINT_TIME() NET_PRINT(" ---> DESTROY VEH -  MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER - iStaggeredParticipant RESET") NET_NL()
//	ENDIF
	
	SCRIPT_EVENT_DATA_TICKER_MESSAGE TickerEventData
	
	//For now we only need to do this check once the Veh is destroyed.
	//IF NOT IS_VEHICLE_DRIVEABLE(NET_TO_VEH(serverBD.niVeh))	//IF NOT IS_NET_VEHICLE_DRIVEABLE(serverBD.niVeh)
		IF serverBD.iServerGameState != GAME_STATE_END
			REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iStaggeredParticipant
				IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iStaggeredParticipant))
					PLAYER_INDEX PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iStaggeredParticipant))
					//PED_INDEX PlayerPedId = GET_PLAYER_PED(PlayerId)
					
					//** CHECKS THAT DON'T NEED THE PLAYER TO BE ALIVE **//
					//Check if anybody has destroyed the Veh
					IF IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_DestroyedVeh)
						
						//Send Ticker
						TickerEventData.TickerEvent = TICKER_EVENT_DV_VEH_DESTROYED_BY_PLAYER
						TickerEventData.playerID = PlayerId
						serverBD.piDestroyer = PlayerId
						BROADCAST_TICKER_EVENT(TickerEventData, ALL_PLAYERS_ON_SCRIPT())
						
						serverBD.iServerGameState = GAME_STATE_END
						PRINTLN("     ---------->     DESTROY VEH - serverBD.iServerGameState = GAME_STATE_END 999 - ", GET_PLAYER_NAME(PlayerId), " DESTROYED THE VEH")
						EXIT
					ENDIF
					
					//Start Hate
					IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_MakePedsHate)
						IF IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_InVeh)
							SET_BIT(serverBD.iServerBitSet, biS_MakePedsHate)
							PRINTLN("     ---------->     DESTROY VEH - biS_MakePedsHate SET - ", GET_PLAYER_NAME(PlayerId), " IN THE VEH")
						ENDIF
						IF IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_DamagedVeh)
							SET_BIT(serverBD.iServerBitSet, biS_MakePedsHate)
							PRINTLN("     ---------->     DESTROY VEH - biS_MakePedsHate SET - ", GET_PLAYER_NAME(PlayerId), " DAMAGED THE VEH")
						ENDIF
					
					//Ped Hate
					ELSE
						IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_DriverHateSet)
							IF IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_SetDriverHate)
								SET_BIT(serverBD.iServerBitSet, biS_DriverHateSet)
								PRINTLN("     ---------->     DESTROY VEH - biS_DriverHateSet SET - ", GET_PLAYER_NAME(PlayerId), " SET THIS PEDS HATE")
							ENDIF
						ENDIF
						IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_Extra0HateSet)
							IF IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_SetExtra0Hate)
								SET_BIT(serverBD.iServerBitSet, biS_Extra0HateSet)
								PRINTLN("     ---------->     DESTROY VEH - biS_Extra0HateSet SET - ", GET_PLAYER_NAME(PlayerId), " SET THIS PEDS HATE")
							ENDIF
						ENDIF
						IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_Extra1HateSet)
							IF IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_SetExtra1Hate)
								SET_BIT(serverBD.iServerBitSet, biS_Extra1HateSet)
								PRINTLN("     ---------->     DESTROY VEH - biS_Extra1HateSet SET - ", GET_PLAYER_NAME(PlayerId), " SET THIS PEDS HATE")
							ENDIF
						ENDIF
						IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_Extra2HateSet)
							IF IS_BIT_SET(playerBD[iStaggeredParticipant].iPlayerBitSet, biP_SetExtra2Hate)
								SET_BIT(serverBD.iServerBitSet, biS_Extra2HateSet)
								PRINTLN("     ---------->     DESTROY VEH - biS_Extra2HateSet SET - ", GET_PLAYER_NAME(PlayerId), " SET THIS PEDS HATE")
							ENDIF
						ENDIF
					ENDIF
					
					//** CHECKS THAT REQUIRE THE PLAYER TO BE ALIVE **//
					//IF IS_NET_PLAYER_OK(PlayerId)
						
					//ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
	//ENDIF
	
//	iStaggeredParticipant++
//	
//	//Reset Staggered Loop
//	IF iStaggeredParticipant = NETWORK_GET_MAX_NUM_PARTICIPANTS()
//		iStaggeredParticipant = 0
//		//bDoEndStaggeredLoopChecks = TRUE
//		//NET_PRINT_TIME() NET_PRINT(" ---> DESTROY VEH -  MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER - iStaggeredParticipant RESET") NET_NL()
//	ENDIF
ENDPROC

//PURPOSE: Returns TRUE if the Veh is stuck or not int the air
/*FUNC BOOL IS_VEH_STUCK()
	IF NOT IS_ENTITY_IN_AIR(NET_TO_VEH(serverBD.niVeh))
		NET_PRINT_TIME() NET_PRINT("     ---------->     DESTROY VEH -  IS_VEH_STUCK - TRUE - NOT IN AIR ") NET_NL()
		RETURN TRUE
	ENDIF
	
	IF IS_ENTITY_IN_WATER(NET_TO_VEH(serverBD.niVeh))
		NET_PRINT_TIME() NET_PRINT("     ---------->     DESTROY VEH -  IS_VEH_STUCK - TRUE - IN WATER ") NET_NL()
		RETURN TRUE
	ENDIF
	
	IF IS_VEHICLE_STUCK_TIMER_UP(NET_TO_VEH(serverBD.niVeh), VEH_STUCK_ON_ROOF, ROOF_TIME)
		NET_PRINT_TIME() NET_PRINT("     ---------->     DESTROY VEH -  IS_VEH_STUCK - TRUE - VEH_STUCK_ON_ROOF ") NET_NL()
		RETURN TRUE
	ENDIF
	
	IF IS_VEHICLE_STUCK_TIMER_UP(NET_TO_VEH(serverBD.niVeh), VEH_STUCK_ON_SIDE, SIDE_TIME)
		NET_PRINT_TIME() NET_PRINT("     ---------->     DESTROY VEH -  IS_VEH_STUCK - TRUE - VEH_STUCK_ON_SIDE ") NET_NL()
		RETURN TRUE
	ENDIF
	
	IF IS_VEHICLE_STUCK_TIMER_UP(NET_TO_VEH(serverBD.niVeh), VEH_STUCK_JAMMED, JAMMED_TIME)
		NET_PRINT_TIME() NET_PRINT("     ---------->     DESTROY VEH -  IS_VEH_STUCK - TRUE - VEH_STUCK_JAMMED ") NET_NL()
		RETURN TRUE
	ENDIF
	
	IF IS_VEHICLE_STUCK_TIMER_UP(NET_TO_VEH(serverBD.niVeh), VEH_STUCK_HUNG_UP, HUNG_UP_TIME)
		NET_PRINT_TIME() NET_PRINT("     ---------->     DESTROY VEH -  IS_VEH_STUCK - TRUE -  ") NET_NL()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC*/

//PURPOSE: Controls tracking the Award "Drive around in Target Veh for 5mins"
PROC MAINTAIN_DRIVING_AWARD_CHECK()
	IF NOT IS_BIT_SET(iBoolsBitSet, biDrivingAwardDone)
		IF GET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_DRIVELESTERCAR5MINS) = FALSE
			IF IS_NET_PLAYER_OK(PLAYER_ID())
				IF IS_PED_IN_THIS_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niVeh))
					IF HAS_NET_TIMER_EXPIRED(stDrivingAwardTimer, DRIVING_AWARD_TIME)
						SET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_DRIVELESTERCAR5MINS, TRUE)
						SET_BIT(iBoolsBitSet, biDrivingAwardDone)
						PRINTLN("     ---------->     DESTROY VEH - MAINTAIN_DRIVING_AWARD_CHECK - biDrivingAwardDone SET - MP_AWARD_DRIVELESTERCAR5MINS = TRUE")
					ENDIF
				ELSE
					IF HAS_NET_TIMER_STARTED(stDrivingAwardTimer)
						RESET_NET_TIMER(stDrivingAwardTimer)
						PRINTLN("     ---------->     DESTROY VEH - MAINTAIN_DRIVING_AWARD_CHECK - stDrivingAwardTimer RESET - PLAYER NOT IN VEHICLE")
					ENDIF
				ENDIF
			ELSE
				IF HAS_NET_TIMER_STARTED(stDrivingAwardTimer)
					RESET_NET_TIMER(stDrivingAwardTimer)
					PRINTLN("     ---------->     DESTROY VEH - MAINTAIN_DRIVING_AWARD_CHECK - stDrivingAwardTimer RESET - PLAYER DEAD")
				ENDIF
			ENDIF
		ELSE
			SET_BIT(iBoolsBitSet, biDrivingAwardDone)
			PRINTLN("     ---------->     DESTROY VEH - MAINTAIN_DRIVING_AWARD_CHECK - biDrivingAwardDone SET - ALREADY GOT AWARD")
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Checks for the Veh (destroyed, stuck, etc)
PROC CONTROL_VEH_BODY()
	IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niVeh)
		IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DestroyedVeh)
			
			// this check doesn't return false when the vehicle is on fire which was causing bug 1151056
			//IF NOT IS_NET_VEHICLE_DRIVEABLE(serverBD.niVeh)	
			
			IF NOT IS_VEHICLE_DRIVEABLE(NET_TO_VEH(serverBD.niVeh))
				PRINTLN("     ---------->     DESTROY VEH -  CONTROL_VEH_BODY - NOT IS_VEHICLE_DRIVEABLE")
			ENDIF
			IF IS_ENTITY_DEAD(NET_TO_VEH(serverBD.niVeh))
				PRINTLN("     ---------->     DESTROY VEH -  CONTROL_VEH_BODY - IS_ENTITY_DEAD")
			ENDIF
			
			//IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niVeh)
			//AND DOES_ENTITY_EXIST(NET_TO_VEH(serverBD.niVeh))
			IF IS_ENTITY_DEAD(NET_TO_VEH(serverBD.niVeh))
				//Check if player was Killer
				WEAPON_TYPE TempWeapon
				IF PLAYER_ID() = NETWORK_GET_DESTROYER_OF_NETWORK_ID(serverBD.niVeh, TempWeapon)
					INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_TIMES_COMPLETE_DESTROYVEH, 1)
					INCREMENT_BY_MP_INT_CHARACTER_AWARD(MP_AWARD_LESTERDELIVERVEHICLES, 1)
					GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD, PLAYER_PED_ID(), "XPT_KAIE", XPTYPE_SKILL, XPCATEGORY_COMPLETE_DESTROY_VEH, g_sMPTunables.iDestroyVehXP, 1)
										
					//Text Message
					IF GET_RANDOM_BOOL()
						Send_MP_Txtmsg_From_CharSheetID(CHAR_MP_GERALD, "DSV_PASS0")
					ELSE
						Send_MP_Txtmsg_From_CharSheetID(CHAR_MP_GERALD, "DSV_PASS1")
					ENDIF
					
					INT iCash = g_sMPTunables.iDestroyVehCash
					g_i_cashForEndEventShard = iCash
					
					PRINTLN("     ---------->     DESTROY VEH -  CONTROL_VEH_BODY - [MAGNATE_GANG_BOSS] - Cash before boss cut: $", iCash)
					GB_HANDLE_GANG_BOSS_CUT(iCash)
					PRINTLN("     ---------->     DESTROY VEH -  CONTROL_VEH_BODY - [MAGNATE_GANG_BOSS] - Cash after boss cut: $", iCash)
										
					IF iCash > 0
						GIVE_LOCAL_PLAYER_CASH_END_OF_MISSION(iCash)
						
						IF USE_SERVER_TRANSACTIONS()
							INT iTransactionID
							TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_AMBIENT_JOB_DESTROY_VEH, iCash, iTransactionID, FALSE, FALSE)
						ELSE
							AMBIENT_JOB_DATA AJdata
							NETWORK_EARN_FROM_AMBIENT_JOB(iCash, "AM_DESTROY_VEH", AJdata)
						ENDIF
					ENDIF
					
					SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DestroyedVeh)
					NET_PRINT_TIME() NET_PRINT("     ---------->     DESTROY VEH -  CONTROL_VEH_BODY - I DESTROYED THE VEH!") NET_NL()
				ENDIF
			ELSE
				//Track Aggressive Acts
				IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_MakePedsHate)
					//Got in Veh
					IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InVeh)
						IF IS_PED_IN_THIS_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niVeh), TRUE)
							SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InVeh)
							PRINTLN("     ---------->     DESTROY VEH - biP_InVeh SET")
						ENDIF
					ENDIF
					//Damaged Veh
					IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DamagedVeh)
						IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(NET_TO_VEH(serverBD.niVeh), PLAYER_PED_ID())
							SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DamagedVeh)
							PRINTLN("     ---------->     DESTROY VEH - biP_DamagedVeh SET")
						ENDIF
					ENDIF
				ENDIF
				
				//Set Disable Mod Shop Flag
				IF IS_NET_PLAYER_OK(PLAYER_ID())
				AND IS_PED_IN_THIS_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niVeh))
					IF NOT IS_BIT_SET(MPGlobalsAmbience.iNGAmbBitSet, iNGABI_PLAYER_USING_DESTROY_VEH)
						SET_BIT(MPGlobalsAmbience.iNGAmbBitSet, iNGABI_PLAYER_USING_DESTROY_VEH)
						PRINTLN("     ---------->     DESTROY VEH - iNGABI_PLAYER_USING_DESTROY_VEH SET")
					ENDIF
				ELSE
					IF IS_BIT_SET(MPGlobalsAmbience.iNGAmbBitSet, iNGABI_PLAYER_USING_DESTROY_VEH)
						CLEAR_BIT(MPGlobalsAmbience.iNGAmbBitSet, iNGABI_PLAYER_USING_DESTROY_VEH)
						PRINTLN("     ---------->     DESTROY VEH - iNGABI_PLAYER_USING_DESTROY_VEH CLEARED")
					ENDIF
				ENDIF
				
				MAINTAIN_DRIVING_AWARD_CHECK()
			ENDIF
		ENDIF
	ENDIF
ENDPROC


PROC CONTROL_PEDS()
	//Blips
	UPDATE_ENEMY_NET_PED_BLIP( serverBD.niDriver, DriverBlipData )
	INT i
	REPEAT serverBD.iNumExtras i
		UPDATE_ENEMY_NET_PED_BLIP( serverBD.niExtras[i], ExtraPedBlipData[i] )
	ENDREPEAT
	
	//Do the Hate
	IF IS_BIT_SET(serverBD.iServerBitSet, biS_MakePedsHate)
		//Control the Driver Hate
		IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_DriverHateSet)
		AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_SetDriverHate)
			IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niDriver)
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niDriver)
				OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.niDriver) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
					SET_PED_RELATIONSHIP_GROUP_HASH(NET_TO_PED(serverBD.niDriver), rgFM_AiHate)
					SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_SetDriverHate)
					PRINTLN("     ---------->     DESTROY VEH - biP_SetDriverHate SET")
				ENDIF
			ENDIF
		ENDIF
		
		//Control the Extra Hate
		REPEAT MAX_NUM_EXTRA_PEDS i
			IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_Extra0HateSet+i)
			AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_SetExtra0Hate+i)
				IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niExtras[i])
					IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niExtras[i])
					OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.niExtras[i]) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
						SET_PED_RELATIONSHIP_GROUP_HASH(NET_TO_PED(serverBD.niExtras[i]), rgFM_AiHate)
						IF NOT IS_PED_INJURED(NET_TO_PED(serverBD.niExtras[i]))
							TASK_COMBAT_HATED_TARGETS_AROUND_PED(NET_TO_PED(serverBD.niExtras[i]), 100.0)
						ENDIF
						SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_SetExtra0Hate+i)
						PRINTLN("     ---------->     DESTROY VEH - biP_SetExtra", i, "Hate SET")
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                  MISSION PROCS                  //////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

//PURPOSE: Process the JOYRIDER stages for the Client
PROC PROCESS_DESTROY_VEH_CLIENT()
	SWITCH playerBD[PARTICIPANT_ID_TO_INT()].eDestroyStage		
		CASE eAD_DESTROY
			ADD_VEH_BLIP()
			CONTROL_VEH_BODY()
			CONTROL_PEDS()
			IF serverBD.eDestroyStage > eAD_DESTROY
				playerBD[PARTICIPANT_ID_TO_INT()].eDestroyStage = eAD_CLEANUP
				NET_PRINT_TIME() NET_PRINT("     ---------->     DESTROY VEH -  PLAYER STAGE = eAD_CLEANUP    <----------     ") NET_NL()
			ENDIF
		BREAK
		
		CASE eAD_CLEANUP
			NET_PRINT_TIME() NET_PRINT("     ---------->     DESTROY VEH -  SCRIPT CLEANUP B     <----------     ") NET_NL()
			SCRIPT_CLEANUP()
		BREAK
	ENDSWITCH
ENDPROC

//PURPOSE: Process the JOYRIDER stages for the Server
PROC PROCESS_DESTROY_VEH_SERVER()
	SWITCH serverBD.eDestroyStage		
		CASE eAD_DESTROY
			//CONTROL VEHICLE BRAIN
		BREAK
						
		CASE eAD_CLEANUP
			
		BREAK
	ENDSWITCH
ENDPROC

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                      Main Loop                      //////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
SCRIPT(MP_MISSION_DATA missionScriptArgs)
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
	AND IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, TRUE)
		#IF IS_DEBUG_BUILD	
			NET_PRINT_TIME() NET_PRINT("MP: Starting mission ") NET_NL()
		#ENDIF
		
		// Vehry out all the initial game starting duties. 
		PROCESS_PRE_GAME(missionScriptArgs)	
		
		// Setup widgets. Make sure this is done after PROCESS_PRE_GAME()
		#IF IS_DEBUG_BUILD
			CREATE_WIDGETS()
		#ENDIF
	ELSE
		#IF IS_DEBUG_BUILD NET_LOG("DESTROY VEH -  NETWORK_IS_GAME_IN_PROGRESS = FALSE")	#ENDIF
		NET_PRINT_TIME() NET_PRINT("     ---------->     DESTROY VEH -  MISSION END - NETWORK_IS_GAME_IN_PROGRESS = FALSE - SCRIPT CLEANUP P     <----------     ") NET_NL()
		SCRIPT_CLEANUP()
	ENDIF
	
	// Main loop
	WHILE TRUE

		// One wait to rule them all. This can be the ONLY wait from here on in.....
		MP_LOOP_WAIT_ZERO()
				
		// If we have a match end event, bail
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
		OR SHOULD_MP_AMBIENT_SCRIPT_TERMINATE(MPAM_TYPE_DESTROY_VEH)
			#IF IS_DEBUG_BUILD NET_LOG("SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE")	#ENDIF
			NET_PRINT_TIME() NET_PRINT("     ---------->     DESTROY VEH -  MISSION END - SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE - SCRIPT CLEANUP B     <----------     ") NET_NL()
			SCRIPT_CLEANUP()
		ENDIF
		
		//Check if player is in a tutorial session and end
		IF NETWORK_IS_IN_TUTORIAL_SESSION()
			#IF IS_DEBUG_BUILD NET_LOG("PLAYER IN TUT SESSION")	#ENDIF
			NET_PRINT_TIME() NET_PRINT("     ---------->     DESTROY VEH -  MISSION END - PLAYER IN TUTORIAL SESSION - SCRIPT CLEANUP C     <----------     ") NET_NL()
			SCRIPT_CLEANUP()
		ENDIF
		
		// Deal with the debug
		#IF IS_DEBUG_BUILD		
			UPDATE_WIDGETS()
		#ENDIF		
	
		// If we need to leave the mission, make sure we just straight to the end stage and terminate the script - DON'T NEED FOR AMBIENT MISSION
		/*IF SHOULD_PLAYER_LEAVE_MP_MISSION() 
			#IF IS_DEBUG_BUILD
			NET_PRINT_TIME() NET_PRINT( GET_MP_MISSION_NAME(thisMission), " Leave mission = TRUE \n")
			#ENDIF
			playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_LEAVE
			#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_LEAVE")	#ENDIF
		ENDIF*/
		
		// -----------------------------------
		// Process your game logic.....
		SWITCH GET_CLIENT_MISSION_STATE(PARTICIPANT_ID_TO_INT())
			
			// Wait untill the server gives the all go before moving on
			CASE GAME_STATE_INI
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING
					CREATE_SEQUENCES()
					ADD_VEH_BLIP()
					
					IF HAS_PLAYER_BEEN_IN_SESSION_THIS_LONG(CRATE_DROP_DISPLAY_DELAY)
						IF !SHOULD_HIDE_LOCAL_UI()
							IF NOT IS_THIS_MODEL_A_BIKE(serverBD.VehModel)
								PRINT_HELP("DSV_HELP1")	//A ~r~target vehicle~s~ ~r~~BLIP_GANG_VEHICLE~ ~s~has been located in Los Santos. Destroy it.
							ELSE
								PRINT_HELP("DSV_HELP2")	//A ~r~target vehicle~s~ ~r~~BLIP_GANG_BIKE~ ~s~has been located in Los Santos. Destroy it.
							ENDIF
						ENDIF
						NET_PRINT_TIME() NET_PRINT("     ---------->     DESTROY VEH -  HELP TEXT DONE    <----------     ") NET_NL()
					ENDIF
					
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_RUNNING
					NET_PRINT_TIME() NET_PRINT("     ---------->     DESTROY VEH -  playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_RUNNING    <----------     ") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_RUNNING")	#ENDIF
				ELIF GET_SERVER_MISSION_STATE() = GAME_STATE_END
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY
					NET_PRINT_TIME() NET_PRINT("     ---------->     DESTROY VEH -  playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY 1    <----------     ") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_TERMINATE_DELAY")	#ENDIF
				ENDIF
			BREAK
			
			// Main gameplay state.
			CASE GAME_STATE_RUNNING
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_RUNNING
					PROCESS_DESTROY_VEH_CLIENT()
				ELIF GET_SERVER_MISSION_STATE() = GAME_STATE_END
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY
					NET_PRINT_TIME() NET_PRINT("     ---------->     DESTROY VEH -  playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY 2   <----------     ") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_TERMINATE_DELAY")	#ENDIF
				ENDIF
			BREAK
			
			CASE GAME_STATE_TERMINATE_DELAY 
				SET_MISSION_FINISHED(serverBD.MissionTerminateDelayTimer)
		      	IF IS_MISSION_READY_TO_CLEANUP(serverBD.MissionTerminateDelayTimer)
		            playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END
					NET_PRINT_TIME() NET_PRINT("     ---------->     DESTROY VEH -  playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END 3    <----------     ") NET_NL()
					#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_END 3")	#ENDIF
		      	ENDIF
			BREAK
			
			// The game stage the local player is placed when we want him to leave a mission
			CASE GAME_STATE_LEAVE
				//LEAVE_MY_MP_MISSION(thisMission)
				playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END
				NET_PRINT_TIME() NET_PRINT("     ---------->     DESTROY VEH -  playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END 2    <----------     ") NET_NL()
				#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_END 2")	#ENDIF
			FALLTHRU
			
			//Cleans up then terminates the mission
			CASE GAME_STATE_END
				NET_PRINT_TIME() NET_PRINT("     ---------->     DESTROY VEH -  SCRIPT CLEANUP A     <----------     ") NET_NL()
				SCRIPT_CLEANUP()
			BREAK

		ENDSWITCH
		
		// -----------------------------------
		// Process server game logic		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			
			SWITCH GET_SERVER_MISSION_STATE()
				
				// For CnC group missions, we may want to wait for all players to have launched the script
				CASE GAME_STATE_INI
					IF CREATE_VEH_AND_DRIVER()
						CREATE_SEQUENCES()
						START_NET_TIMER(serverBD.ForceCleanup)
						serverBD.iServerGameState = GAME_STATE_RUNNING
						NET_PRINT_TIME() NET_PRINT("     ---------->     DESTROY VEH -  serverBD.iServerGameState = GAME_STATE_RUNNING    <----------     ") NET_NL()
						#IF IS_DEBUG_BUILD NET_LOG_Host("iServerGameState = GAME_STATE_RUNNING")	#ENDIF
					ENDIF
				BREAK
				
				// Look for game end conditions
				CASE GAME_STATE_RUNNING
					
					MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER()
					PROCESS_DESTROY_VEH_SERVER()
					
					#IF IS_DEBUG_BUILD
						IF bHostEndMissionNow
						OR MPGlobalsAmbience.bKillActiveEvent
						MPGlobalsAmbience.bKillActiveEvent = FALSE
							serverBD.iServerGameState = GAME_STATE_END
							NET_PRINT_TIME() NET_PRINT("     ---------->     DESTROY VEH -  serverBD.iServerGameState = GAME_STATE_END 1    <----------     ") NET_NL()
							#IF IS_DEBUG_BUILD NET_LOG_Host("iServerGameState = GAME_STATE_END 1")	#ENDIF
						ENDIF
					#ENDIF	
					
					IF HAVE_MISSION_END_CONDITIONS_BEEN_MET()
						serverBD.iServerGameState = GAME_STATE_END
						NET_PRINT_TIME() NET_PRINT("     ---------->     DESTROY VEH -  serverBD.iServerGameState = GAME_STATE_END 4    <----------     ") NET_NL()
						#IF IS_DEBUG_BUILD NET_LOG_Host("iServerGameState = GAME_STATE_END 4")	#ENDIF
					ENDIF
					
				BREAK
				
				CASE GAME_STATE_END
				BREAK
				
			ENDSWITCH
			
		ENDIF
		
	ENDWHILE
	
ENDSCRIPT
