/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        AM_MP_ARCADE_CLAW_CRANE.sc																				//
// Description: This is the implementation of claw crane arcade game. This script will mange the claw crane gameplay 	// 
// 				functionality. This script will wait for ready flag from arcade manager script (AM_MP_ARC_CAB_MANAGER)  //
//				to start the game.							 															//
// Written by:  Online Technical Design: Ata Tabrizi																	//
// Date:  		28/08/2019																								//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_event.sch"
USING "net_include.sch"
USING "net_events.sch"
USING "net_wait_zero.sch"
USING "net_common_functions.sch"
USING "net_simple_interior.sch"

#IF IS_DEBUG_BUILD
USING "net_debug_log.sch"
#ENDIF

#IF FEATURE_CASINO_HEIST
USING "net_arcade_cabinet.sch"
USING "arcade_games_help_text_flow.sch"

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ DATA STRUCTURES ╞═════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛
ENUM CLAW_CRANE_SUB_PROPS
	CLAW_SUB_RAIL_X = 0,
	CLAW_SUB_RAIL_Y,
	CLAW_SUB_CLAW,
	CLAW_SUB_CLAW_COLLISION,
	CLAW_SUB_PRIZE_1,
	CLAW_SUB_PRIZE_2,
	CLAW_SUB_PRIZE_3,
	CLAW_SUB_PRIZE_4,
	CLAW_SUB_PRIZE_5,
	CLAW_SUB_PRIZE_6,
	CLAW_SUB_PRIZE_7,
	CLAW_SUB_PRIZE_8,
	CLAW_SUB_PRIZE_9,
	CLAW_SUB_PRIZE_10,
	
	CLAW_SUB_TOTAL
ENDENUM

ENUM CLAW_CRANE_GAME_STATE
	CLAW_GAME_IDLE_STATE = 0,
	CLAW_GAME_INIT_STATE,
	CLAW_GAME_MOVE_FORWARD_STATE,
	CLAW_GAME_MOVE_RIGHT_STATE,
	CLAW_GAME_MOVE_DOWN_TO_PICK_UP_STATE,
	CLAW_GAME_MOVE_UP_TO_PICK_UP_STATE,
	CLAW_GAME_ATTACH_PRIZE_STATE,
	CLAW_GAME_WAIT_TO_TRIGGER_PICK_UP_STATE,
	CLAW_GAME_MOVE_LEFT_STATE,
	CLAW_GAME_MOVE_BACKWARD_STATE,
	CLAW_GAME_DROP_PRIZE_STATE,
	CLAW_GAME_DROP_PRIZE_CLEANUP_STATE,
	CLAW_GAME_CLEANUP_STATE	
ENDENUM

ENUM CLAW_CRANE_STICK_DIRECTION
	CLAW_CRANE_RIGHT_X = 0,
	CLAW_CRANE_RIGHT_Y,
	CLAW_CRANE_LEFT_X,
	CLAW_CRANE_LEFT_Y,
	
	CLAW_CRANE_STRCK_TOTAL
ENDENUM

STRUCT ServerBroadcastData
	INT iServerBS
	INT iClawSyncSceneID = -1
	INT iAttachedPrizeIndex
	
	FLOAT fPrizeInitialHeading
	VECTOR vPrizeInitialCoords
	
	PLAYER_INDEX plUsingClawCrane
	
	CLAW_CRANE_GAME_STATE eGameState
	SCRIPT_TIMER prizeVisibilityTimer
ENDSTRUCT

STRUCT PlayerBroadcastData
	INT iLocalBS
	INT iAttachedPrizeIndex = -1
	ENTITY_INDEX collidedPrize
	CLAW_CRANE_GAME_STATE eGameState
ENDSTRUCT

STRUCT CLAW_CRANE_DATA
	INT iBS
	INT iStickPosition[CLAW_CRANE_STRCK_TOTAL]
	INT iPlayerStagger
	INT iClawCollisionShapeTest, iClawCollisionHitSomthing
	INT iMoveSoundID = -1
	INT iRandomWinRate = -1
	INT iPrizeSyncSceneID = -1
	
	FLOAT fMoveSoundVolume
	FLOAT fPrizeInitialHeading
	
	VECTOR vMovingDownCoord
	VECTOR vPrizeCollideCoord 
	VECTOR vPrizeInitialCoords
	
	STRING sPrizeName
	
	OBJECT_INDEX cabinetObj
	OBJECT_INDEX clawObj[CLAW_SUB_TOTAL]
	CAMERA_INDEX clawCamera
	SHAPETEST_INDEX clawCollisionShapeTest
	
	ARCADE_CABINET_CHILD_SCRIPT_CLIENT_STATE eState
	CLAW_CRANE_GAME_STATE eGameState
	ARCADE_CABINETS eArcadeCabinetLocateType
	ARCADE_CABINET_ANIM_EVENT_STRUCT sArcadeCabinetAnimEvent
	ARCADE_CABINET_AI_CONTEXT_STRUCT sArcadeCabinetAIContext
	ARCADE_GAME_TROPHY trophyType 
	SCRIPT_CONTROL_HOLD_TIMER sControlTimer
	SCRIPT_TIMER tHypeAIContextTimer
	SCRIPT_TIMER tAttractAIContextTimer
	SCRIPT_TIMER tPlayDurationTimer
ENDSTRUCT

#IF IS_DEBUG_BUILD
STRUCT CLAW_CRANE_DEBUG_DATA
	VECTOR vSubPropOffset[CLAW_SUB_TOTAL]
	VECTOR vAttachOffset
	BOOL bRemoveRanRate
	BOOL bForceWin
	BOOL bDisplayDistanceInfo
ENDSTRUCT
#ENDIF


//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡    CONSTS    ╞════════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛
// serverBD.iServerBS
CONST_INT SERVER_BD_BS_ASSIGNED_GAME_USER								0
CONST_INT SERVER_BD_BS_START_CLAW_PRESS_DOWN_ANIM						1

// sData.iBS
CONST_INT LOCAL_BS_CLAW_MOVE_FORWARD_COMPLETE							0
CONST_INT LOCAL_BS_CLAW_MOVE_RIGHT_COMPLETE								1
CONST_INT LOCAL_BS_CLAW_MOVE_DOWN_COMPLETE								2
CONST_INT LOCAL_BS_CLAW_DONT_UPDATE_GAME_STATE_BD						3
CONST_INT LOCAL_BS_CLAW_HIT_PRIZE			 							4
CONST_INT LOCAL_BS_GRAB_CLAW_MOVING_DOWN_COORDS							5
CONST_INT LOCAL_BS_ATTACHED_PRIZE_TO_CLAW								6
CONST_INT LOCAL_BS_STOP_CLAW_ANIM										7
CONST_INT LOCAL_BS_STOP_PRIZE_ANIM										8
CONST_INT LOCAL_BS_PLAY_IMPACT_SOUND									9
CONST_INT LOCAL_BS_MOVE_BACK_PRIZE_TO_CABINET							10
CONST_INT LOCAL_BS_PLAY_ASCEND_SOUND									11
CONST_INT LOCAL_BS_GET_RAN_NUM_FOR_ATTACHMENT							12
CONST_INT LOCAL_BS_SWITCH_TO_TPP_CAMERA									13
CONST_INT LOCAL_BS_DETACH_BEFORE_ANIMATION								14

// playerBD.iLocalBS
CONST_INT PLAYER_BD_BS_PLAYING_GAME										0
CONST_INT PLAYER_BD_BS_START_CLAW_PRESS_DOWN_ANIM						1

// Shape Test Stages
CONST_INT CLAW_SHAPETEST_INIT											0
CONST_INT CLAW_SHAPETEST_PROCESS										1

// Area Check Stages
CONST_INT CLAW_COLLISION_CHECK_INIT										0
CONST_INT CLAW_COLLISION_CHECK_NOT_HIT_ANYTHING							1
CONST_INT CLAW_COLLISION_CHECK_HIT_SOMETHING							2

CONST_INT MAX_NUM_CLAW_PRIZE_TYPE										9

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡    VARIABLES    ╞═════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

PlayerBroadcastData playerBD[NUM_NETWORK_PLAYERS]
ServerBroadcastData serverBD
CLAW_CRANE_DATA sData

#IF IS_DEBUG_BUILD
CLAW_CRANE_DEBUG_DATA sClawCraneDebugData
#ENDIF

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════════╡     DEBUG     ╞══════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛
#IF IS_DEBUG_BUILD
	FUNC STRING GET_BIT_SET_NAME(INT iBit)
	SWITCH iBit
		CASE LOCAL_BS_CLAW_MOVE_FORWARD_COMPLETE			RETURN "LOCAL_BS_CLAW_MOVE_FORWARD_COMPLETE"
		CASE LOCAL_BS_CLAW_MOVE_RIGHT_COMPLETE				RETURN "LOCAL_BS_CLAW_MOVE_RIGHT_COMPLETE"
		CASE LOCAL_BS_CLAW_MOVE_DOWN_COMPLETE				RETURN "LOCAL_BS_CLAW_MOVE_DOWN_COMPLETE"
		CASE LOCAL_BS_CLAW_DONT_UPDATE_GAME_STATE_BD		RETURN "LOCAL_BS_CLAW_DONT_UPDATE_GAME_STATE_BD"
		CASE LOCAL_BS_CLAW_HIT_PRIZE						RETURN "LOCAL_BS_CLAW_HIT_PRIZE"
		CASE LOCAL_BS_GRAB_CLAW_MOVING_DOWN_COORDS			RETURN "LOCAL_BS_GRAB_CLAW_MOVING_DOWN_COORDS"
		CASE LOCAL_BS_ATTACHED_PRIZE_TO_CLAW				RETURN "LOCAL_BS_ATTACHED_PRIZE_TO_CLAW"
		CASE LOCAL_BS_STOP_CLAW_ANIM						RETURN "LOCAL_BS_STOP_CLAW_ANIM	"
		CASE LOCAL_BS_STOP_PRIZE_ANIM						RETURN "LOCAL_BS_STOP_PRIZE_ANIM"	
		CASE LOCAL_BS_PLAY_IMPACT_SOUND						RETURN "LOCAL_BS_PLAY_IMPACT_SOUND"
		CASE LOCAL_BS_MOVE_BACK_PRIZE_TO_CABINET			RETURN "LOCAL_BS_MOVE_BACK_PRIZE_TO_CABINET"
		CASE LOCAL_BS_PLAY_ASCEND_SOUND						RETURN "LOCAL_BS_PLAY_ASCEND_SOUND"
		CASE LOCAL_BS_GET_RAN_NUM_FOR_ATTACHMENT			RETURN "LOCAL_BS_GET_RAN_NUM_FOR_ATTACHMENT"
		CASE LOCAL_BS_SWITCH_TO_TPP_CAMERA					RETURN "LOCAL_BS_SWITCH_TO_TPP_CAMERA"
		CASE LOCAL_BS_DETACH_BEFORE_ANIMATION				RETURN "LOCAL_BS_DETACH_BEFORE_ANIMATION"
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC STRING GET_CLAW_CRANE_GAME_STATE(CLAW_CRANE_GAME_STATE eState)
	SWITCH eState
		CASE CLAW_GAME_IDLE_STATE						RETURN "CLAW_GAME_IDLE_STATE"
		CASE CLAW_GAME_INIT_STATE						RETURN "CLAW_GAME_INIT_STATE"
		CASE CLAW_GAME_MOVE_FORWARD_STATE				RETURN "CLAW_GAME_MOVE_FORWARD_STATE"
		CASE CLAW_GAME_MOVE_RIGHT_STATE					RETURN "CLAW_GAME_MOVE_RIGHT_STATE"
		CASE CLAW_GAME_WAIT_TO_TRIGGER_PICK_UP_STATE	RETURN "CLAW_GAME_WAIT_TO_TRIGGER_PICK_UP_STATE"
		CASE CLAW_GAME_MOVE_UP_TO_PICK_UP_STATE			RETURN "CLAW_GAME_MOVE_UP_TO_PICK_UP_STATE"
		CASE CLAW_GAME_ATTACH_PRIZE_STATE				RETURN "CLAW_GAME_ATTACH_PRIZE_STATE"
		CASE CLAW_GAME_MOVE_DOWN_TO_PICK_UP_STATE		RETURN "CLAW_GAME_MOVE_DOWN_TO_PICK_UP_STATE"
		CASE CLAW_GAME_MOVE_LEFT_STATE					RETURN "CLAW_GAME_MOVE_LEFT_STATE"
		CASE CLAW_GAME_MOVE_BACKWARD_STATE				RETURN "CLAW_GAME_MOVE_BACKWARD_STATE"
		CASE CLAW_GAME_DROP_PRIZE_STATE					RETURN "CLAW_GAME_DROP_PRIZE_STATE"
		CASE CLAW_GAME_DROP_PRIZE_CLEANUP_STATE			RETURN "CLAW_GAME_DROP_PRIZE_CLEANUP_STATE"
		CASE CLAW_GAME_CLEANUP_STATE					RETURN "CLAW_GAME_CLEANUP_STATE"
	ENDSWITCH 
	RETURN "INVALID STATE"
ENDFUNC

PROC CREATE_DEBUG_WIDGET()
	START_WIDGET_GROUP("AM_MP_ARCADE_CLAW_CRANE")
		START_WIDGET_GROUP("Sub prop offset")
			ADD_WIDGET_VECTOR_SLIDER("Prop CLAW_SUB_RAIL_X", sClawCraneDebugData.vSubPropOffset[0],  -99999.9, 99999.9, 0.01)
			ADD_WIDGET_VECTOR_SLIDER("Prop CLAW_SUB_RAIL_Y", sClawCraneDebugData.vSubPropOffset[1],  -99999.9, 99999.9, 0.01)
			ADD_WIDGET_VECTOR_SLIDER("Prop CLAW_SUB_CLAW", sClawCraneDebugData.vSubPropOffset[2],  -99999.9, 99999.9, 0.01)
			ADD_WIDGET_VECTOR_SLIDER("Prop CLAW_SUB_CLAW_COLLISION", sClawCraneDebugData.vSubPropOffset[3],  -99999.9, 99999.9, 0.01)
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("prize offset")
			ADD_WIDGET_VECTOR_SLIDER("Pirze 1", sClawCraneDebugData.vSubPropOffset[4],  -99999.9, 99999.9, 0.01)
			ADD_WIDGET_VECTOR_SLIDER("Pirze 2", sClawCraneDebugData.vSubPropOffset[5],  -99999.9, 99999.9, 0.01)
			ADD_WIDGET_VECTOR_SLIDER("Pirze 3", sClawCraneDebugData.vSubPropOffset[6],  -99999.9, 99999.9, 0.01)
			ADD_WIDGET_VECTOR_SLIDER("Pirze 4", sClawCraneDebugData.vSubPropOffset[7],  -99999.9, 99999.9, 0.01)
			ADD_WIDGET_VECTOR_SLIDER("Pirze 5", sClawCraneDebugData.vSubPropOffset[8],  -99999.9, 99999.9, 0.01)
			ADD_WIDGET_VECTOR_SLIDER("Pirze 6", sClawCraneDebugData.vSubPropOffset[9],  -99999.9, 99999.9, 0.01)
			ADD_WIDGET_VECTOR_SLIDER("Pirze 7", sClawCraneDebugData.vSubPropOffset[10],  -99999.9, 99999.9, 0.01)
			ADD_WIDGET_VECTOR_SLIDER("Pirze 8", sClawCraneDebugData.vSubPropOffset[11],  -99999.9, 99999.9, 0.01)
			ADD_WIDGET_VECTOR_SLIDER("Pirze 9", sClawCraneDebugData.vSubPropOffset[12],  -99999.9, 99999.9, 0.01)
			ADD_WIDGET_VECTOR_SLIDER("Pirze 10", sClawCraneDebugData.vSubPropOffset[13],  -99999.9, 99999.9, 0.01)
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Attach offset")
			ADD_WIDGET_VECTOR_SLIDER("attach offset", sClawCraneDebugData.vAttachOffset,  -99999.9, 99999.9, 0.01)
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Win Rate")
			ADD_WIDGET_BOOL("Force Win", sClawCraneDebugData.bForceWin)
			ADD_WIDGET_BOOL("Remove random rate", sClawCraneDebugData.bRemoveRanRate)
			ADD_WIDGET_BOOL("Display distance info", sClawCraneDebugData.bDisplayDistanceInfo)
		STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP()
ENDPROC
#ENDIF

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡     CLEANUP     ╞══════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛
PROC CLEANUP_CLAW_CAMERA()
	IF DOES_CAM_EXIST(sData.clawCamera)
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
		DESTROY_CAM(sData.clawCamera)
	ENDIF
ENDPROC

PROC STOP_CLAW_CRANE_MOVE_SOUND()
	IF sData.iMoveSoundID != -1
		STOP_SOUND(sData.iMoveSoundID)
		RELEASE_SOUND_ID(sData.iMoveSoundID)
		sData.iMoveSoundID = -1
		sData.fMoveSoundVolume = 0
	ENDIF
ENDPROC

PROC CLEAR_CLAW_CRANE_HELP_TEXTS()
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("ARC_CLAW_MO_F")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("ARC_CLAW_MO_R")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("ARC_CLAW_MO_D")
		CLEAR_HELP()
	ENDIF
ENDPROC

PROC SCRIPT_CLEANUP()
	PRINTLN("[AM_ARCADE_CLAW_CRANE] - SCRIPT_CLEANUP called")
	STOP_CLAW_CRANE_MOVE_SOUND()
	CLEANUP_CLAW_CAMERA()
	CLEAR_CLAW_CRANE_HELP_TEXTS()
	SET_TERMINATE_PHYSICAL_ARCADE_GAME_FLAG(ARCADE_CABINET_CH_CLAW_CRANE, FALSE)
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════════╡     INITIALISE     ╞════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

PROC INITIALISE(ACM_CHILD_SCRIPT_LAUNCHER_STRUCT missionScriptArgs)
	INT i
	FOR i = 0 TO ENUM_TO_INT(CLAW_SUB_TOTAL) - 1
		IF DOES_ENTITY_EXIST(missionScriptArgs.cabinetSubObj[i])
			sData.clawObj[i] = missionScriptArgs.cabinetSubObj[i]
		ELSE
			PRINTLN("[AM_ARCADE_CLAW_CRANE] - INITIALISE - sub object: ", i, " doesn't exist")
		ENDIF	
	ENDFOR	
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		IF NOT HAS_NET_TIMER_STARTED(serverBD.prizeVisibilityTimer)
			START_NET_TIMER(serverBD.prizeVisibilityTimer)
		ENDIF	

		IF NOT IS_BIT_SET(serverBD.iServerBS, SERVER_BD_BS_ASSIGNED_GAME_USER)
			IF serverBD.plUsingClawCrane != INVALID_PLAYER_INDEX()
				serverBD.plUsingClawCrane = INVALID_PLAYER_INDEX()
				PRINTLN("[AM_ARCADE_CLAW_CRANE] - INITIALISE - set serverBD.plUsingClawCrane to invalid ")
			ENDIF	
		ENDIF
	ENDIF
	
	SET_TERMINATE_PHYSICAL_ARCADE_GAME_FLAG(ARCADE_CABINET_CH_CLAW_CRANE, FALSE)
ENDPROC

PROC SCRIPT_INITIALISE(ACM_CHILD_SCRIPT_LAUNCHER_STRUCT missionScriptArgs)
	#IF IS_DEBUG_BUILD
	PRINTLN("[AM_ARCADE_CLAW_CRANE] - SCRIPT_INITIALISE - Launching instance: ", missionScriptArgs.iInstanceId)
	#ENDIF
		
	NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE, missionScriptArgs.iInstanceId)
	
	// This makes sure the net script is active, waits untill it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD, SIZE_OF(playerBD))

	// KGM: Wait for the first network broadcast before moving on
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		PRINTLN("[AM_ARCADE_CLAW_CRANE] - SCRIPT_INITIALISE FAILED TO RECEIVE INITIAL NETWORK BROADCAST - SCRIPT CLEANUP")
		SCRIPT_CLEANUP()
	ENDIF

	sData.cabinetObj = missionScriptArgs.cabinetObj
	sData.eArcadeCabinetLocateType = missionScriptArgs.eArcadeCabinetLocateType
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		PRINTLN("[AM_ARCADE_CLAW_CRANE] INITIALISED arcade cabinet manager script")
		
		// This script will not be paused if another script calls PAUSE_GAME
		PRINTLN("[AM_ARCADE_CLAW_CRANE] - INITIALISED")
	ELSE
		PRINTLN("[AM_ARCADE_CLAW_CRANE] - INITIALISED NETWORK_IS_GAME_IN_PROGRESS = FALSE - SCRIPT CLEANUP")
		SCRIPT_CLEANUP()
	ENDIF
	
	INITIALISE(missionScriptArgs)
	
	#IF IS_DEBUG_BUILD
	CREATE_DEBUG_WIDGET()
	#ENDIF
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ QUERY BITS ╞══════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

PROC SET_ARCADE_CLAW_CRANE_BIT(INT iBit)
	IF NOT IS_BIT_SET(sData.iBS, iBit)
		SET_BIT(sData.iBS, iBit)
		PRINTLN("[AM_ARCADE_CLAW_CRANE] SET_ARCADE_CLAW_CRANE_BIT ", GET_BIT_SET_NAME(iBit) , " TRUE")
	ENDIF
ENDPROC

FUNC BOOL IS_ARCADE_CLAW_CRANE_BIT_SET(INT iBit)
	RETURN IS_BIT_SET(sData.iBS, iBit)
ENDFUNC

PROC CLEAR_ARCADE_CLAW_CRANE_BIT(INT iBit)
	IF IS_BIT_SET(sData.iBS, iBit)
		CLEAR_BIT(sData.iBS, iBit)
		PRINTLN("[AM_ARCADE_CLAW_CRANE] CLEAR_ARCADE_CLAW_CRANE_BIT ", GET_BIT_SET_NAME(iBit) , " FALSE")
	ENDIF
ENDPROC

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════════╡     FUNCS & PROC     ╞══════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

FUNC STRING GET_CLAW_ANIN_CLIP_NAMES(ARCADE_CABINET_ANIM_CLIPS eClip)
	SWITCH eClip
		CASE ARCADE_CABINET_ANIM_CLIP_CLAW_CRANE_PRESS_GRAB
			RETURN "prop_claw_grab"
		BREAK
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC ENTITY_INDEX GET_COLLIDED_PRIZE_INDEX(BOOL bHost)
	IF !bHost
		RETURN playerBD[NATIVE_TO_INT(PLAYER_ID())].collidedPrize
	ELSE
		IF serverBD.iAttachedPrizeIndex != -1
			RETURN GET_ENTITY_FROM_PED_OR_VEHICLE(sData.clawObj[serverBD.iAttachedPrizeIndex])
		ENDIF	
	ENDIF
	
	RETURN INT_TO_NATIVE(ENTITY_INDEX, -1)
ENDFUNC

PROC START_CLAW_SYNC_SCENE(ARCADE_CABINET_ANIM_CLIPS eClip, BOOL bHoldLastFrame = FALSE, BOOL bLoopAnim = FALSE, FLOAT blendInDelta = REALLY_SLOW_BLEND_IN, FLOAT blendOutDelta = REALLY_SLOW_BLEND_OUT)
	
	STRING sAnimClipName = GET_CLAW_ANIN_CLIP_NAMES(eClip)
	STRING sAnimDic
	IF !IS_PLAYER_FEMALE()
		sAnimDic = "anim_heist@arcade@claw@male@"
	ELSE
		sAnimDic = "anim_heist@arcade@claw@female@"
	ENDIF
	
	VECTOR vAnimCoords = GET_ENTITY_COORDS(sData.clawObj[CLAW_SUB_CLAW])
	FLOAT fClawHeading = GET_ENTITY_HEADING(sData.clawObj[CLAW_SUB_CLAW])
	
	
	IF eClip = ARCADE_CABINET_ANIM_CLIP_CLAW_CRANE_PRIZE_WIN
		vAnimCoords = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sData.cabinetObj, <<CLAW_CRAN_MIN_X_AXIS, CLAW_CRAN_MIN_Y_AXIS, CLAW_CRAN_MAX_Z_AXIS>>)
	ENDIF
	
	IF !IS_STRING_NULL_OR_EMPTY(sAnimClipName)
		serverBD.iClawSyncSceneID = NETWORK_CREATE_SYNCHRONISED_SCENE(vAnimCoords, <<0, 0, fClawHeading>>, DEFAULT, bHoldLastFrame, bLoopAnim)
		NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(sData.clawObj[CLAW_SUB_CLAW], serverBD.iClawSyncSceneID, sAnimDic, sAnimClipName, blendInDelta, blendOutDelta, SYNCED_SCENE_BLOCK_MOVER_UPDATE)
		NETWORK_START_SYNCHRONISED_SCENE(serverBD.iClawSyncSceneID)
		PRINTLN("[AM_ARC_CAB] - START_CLAW_SYNC_SCENE ", serverBD.iClawSyncSceneID, " \"", sAnimClipName, "\", \"", sAnimDic, "\"")
	ELSE
		PRINTLN("[AM_ARC_CAB] - START_CLAW_SYNC_SCENE - sAnimClipName is invalid")
	ENDIF
ENDPROC

FUNC STRING GET_PRIZE_ANIN_CLIP_NAMES(ARCADE_CABINET_ANIM_CLIPS eClip)
	SWITCH eClip
		CASE ARCADE_CABINET_ANIM_CLIP_CLAW_CRANE_PRESS_GRAB
			RETURN "prop_plush_grab"
		BREAK
	ENDSWITCH
	
	RETURN ""
ENDFUNC

PROC START_PRIZE_SYNC_SCENE(ARCADE_CABINET_ANIM_CLIPS eClip, BOOL bHoldLastFrame = FALSE, BOOL bLoopAnim = FALSE, FLOAT blendInDelta = REALLY_SLOW_BLEND_IN, FLOAT blendOutDelta = REALLY_SLOW_BLEND_OUT)
	
	STRING sAnimClipName = GET_PRIZE_ANIN_CLIP_NAMES(eClip)
	STRING sAnimDic
	IF !IS_PLAYER_FEMALE()
		sAnimDic = "anim_heist@arcade@claw@male@"
	ELSE
		sAnimDic = "anim_heist@arcade@claw@female@"
	ENDIF
	
	VECTOR vAnimCoords = GET_ENTITY_COORDS(GET_COLLIDED_PRIZE_INDEX(FALSE))
	FLOAT fPrizeHeading = GET_ENTITY_HEADING(GET_COLLIDED_PRIZE_INDEX(FALSE))
	
	IF eClip = ARCADE_CABINET_ANIM_CLIP_CLAW_CRANE_PRIZE_WIN 
		vAnimCoords = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sData.cabinetObj, <<-0.29, -0.10, 1.41>>)
	ENDIF
	
	IF !IS_STRING_NULL_OR_EMPTY(sAnimClipName)
		sData.iPrizeSyncSceneID = NETWORK_CREATE_SYNCHRONISED_SCENE(vAnimCoords, <<0, 0, fPrizeHeading>>, DEFAULT, bHoldLastFrame, bLoopAnim)
		NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(GET_COLLIDED_PRIZE_INDEX(FALSE), sData.iPrizeSyncSceneID, sAnimDic, sAnimClipName,	blendInDelta, blendOutDelta)
		NETWORK_START_SYNCHRONISED_SCENE(sData.iPrizeSyncSceneID)
		PRINTLN("[AM_ARC_CAB] - START_PRIZE_SYNC_SCENE ", sData.iPrizeSyncSceneID, " \"", sAnimClipName, "\", \"", sAnimDic, "\"")
	ELSE
		PRINTLN("[AM_ARC_CAB] - START_PRIZE_SYNC_SCENE - sAnimClipName is invalid")
	ENDIF
ENDPROC

PROC SET_CLAW_CRANE_GAME_STATE(CLAW_CRANE_GAME_STATE eState, BOOL bSetBroadcastData = TRUE)
	IF sData.eGameState != eState
		PRINTLN("[AM_ARCADE_CLAW_CRANE] SET_CLAW_CRANE_GAME_STATE ", GET_CLAW_CRANE_GAME_STATE(sData.eGameState) , " TO: ", GET_CLAW_CRANE_GAME_STATE(eState))
		sData.eGameState = eState
		IF PLAYER_ID() != INVALID_PLAYER_INDEX()
		AND bSetBroadcastData
			playerBD[NATIVE_TO_INT(PLAYER_ID())].eGameState = eState
		ENDIF	
	ENDIF	
ENDPROC

PROC SET_CLAW_CRANE_SERVER_GAME_STATE(CLAW_CRANE_GAME_STATE eState)
	IF serverBD.eGameState != eState
		PRINTLN("[AM_ARCADE_CLAW_CRANE] SET_CLAW_CRANE_SERVER_GAME_STATE ", GET_CLAW_CRANE_GAME_STATE(serverBD.eGameState) , " TO: ", GET_CLAW_CRANE_GAME_STATE(eState))
		serverBD.eGameState = eState	
	ENDIF	
ENDPROC

FUNC BOOL SHOULD_KILL_THIS_SCRIPT()
	IF SHOULD_KILL_ACM_SCRIPT()
		PRINTLN("[AM_ARCADE_CLAW_CRANE] - SHOULD_KILL_THIS_SCRIPT - SHOULD_KILL_DRONE_SCRIPT TRUE")
		RETURN TRUE
	ENDIF
	
	IF NOT IS_SKYSWOOP_AT_GROUND()
		PRINTLN("[AM_ARCADE_CLAW_CRANE] - SHOULD_KILL_THIS_SCRIPT - IS_SKYSWOOP_AT_GROUND is false - TRUE")
		RETURN TRUE
	ENDIF

	IF IS_PLAYER_WALKING_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
		PRINTLN("[AM_ARCADE_CLAW_CRANE] - SHOULD_KILL_THIS_SCRIPT - IS_PLAYER_WALKING_OUT_OF_SIMPLE_INTERIOR is true - TRUE")
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_ENTERING_OR_EXITING_PROPERTY(PLAYER_ID())
		PRINTLN("[AM_ARCADE_CLAW_CRANE] - SHOULD_KILL_THIS_SCRIPT - IS_PLAYER_ENTERING_OR_EXITING_PROPERTY is true - TRUE")
		RETURN TRUE
	ENDIF
	
	IF SHOULD_TERMINATE_PHYSICAL_ARDCADE_GAME(ARCADE_CABINET_CH_CLAW_CRANE)
		PRINTLN("[AM_ARCADE_CLAW_CRANE] - SHOULD_KILL_THIS_SCRIPT - force cleanup is true - TRUE")
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

/// PURPOSE:
///     Check if we can move to idle state of script to start the game
FUNC BOOL SHOULD_MOVE_TO_IDLE_STATE()
	IF serverBD.eGameState != CLAW_GAME_IDLE_STATE
		PRINTLN("[AM_ARCADE_CLAW_CRANE] - SHOULD_MOVE_TO_IDLE_STATE - serverBD.eGameState is ", GET_CLAW_CRANE_GAME_STATE(serverBD.eGameState))
		RETURN FALSE
	ENDIF
	
	IF sData.eGameState != CLAW_GAME_IDLE_STATE
		PRINTLN("[AM_ARCADE_CLAW_CRANE] - SHOULD_MOVE_TO_IDLE_STATE - Data.eGameState is ", GET_CLAW_CRANE_GAME_STATE(sData.eGameState))
		RETURN FALSE
	ENDIF
	
	IF !IS_PLAYER_READY_TO_PLAY_ARCADE_GAME(PLAYER_ID())
		#IF IS_DEBUG_BUILD
		IF GET_FRAME_COUNT() % 60 = 0
			PRINTLN("[AM_ARCADE_CLAW_CRANE] - SHOULD_MOVE_TO_IDLE_STATE - IS_PLAYER_READY_TO_PLAY_ARCADE_GAME is false")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF !IS_PLAYER_USING_ARCADE_CABINET(PLAYER_ID())
		PRINTLN("[AM_ARCADE_CLAW_CRANE] - SHOULD_MOVE_TO_IDLE_STATE - IS_PLAYER_USING_ARCADE_CABINET is false")
		RETURN FALSE
	ENDIF

	IF GET_ARCADE_CABINET_LOCATE_PLAYER_IS_IN(PLAYER_ID()) != sData.eArcadeCabinetLocateType
		#IF IS_DEBUG_BUILD
		IF GET_FRAME_COUNT() % 60 = 0
			PRINTLN("[AM_ARCADE_CLAW_CRANE] - SHOULD_MOVE_TO_IDLE_STATE - player not in correct locate false")
		ENDIF	
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_PLAYING_THIS_ARCADE_CABINET_ANIMATION_CLIP(ARCADE_CABINET_ANIM_CLIP_CLAW_CRANE_PRIZE_WIN)
	OR IS_PLAYER_PLAYING_THIS_ARCADE_CABINET_ANIMATION_CLIP(ARCADE_CABINET_ANIM_CLIP_CLAW_CRANE_MISS)
		PRINTLN("[AM_ARCADE_CLAW_CRANE] - SHOULD_MOVE_TO_IDLE_STATE - playing win or miss clips is false")
		RETURN FALSE
	ENDIF
	
	IF DOES_ENTITY_EXIST(sData.clawObj[CLAW_SUB_CLAW])
		IF IS_PLAYER_PLAYING_THIS_ARCADE_CABINET_ANIMATION_CLIP(ARCADE_CABINET_ANIM_CLIP_CLAW_CRANE_MISS)
		OR IS_PLAYER_PLAYING_THIS_ARCADE_CABINET_ANIMATION_CLIP(ARCADE_CABINET_ANIM_CLIP_CLAW_CRANE_PRIZE_WIN)
			PRINTLN("[AM_ARCADE_CLAW_CRANE] SHOULD_MOVE_TO_IDLE_STATE player playing miss or win anim")
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF !IS_PLAYER_PLAYING_THIS_ARCADE_CABINET_ANIMATION_CLIP(ARCADE_CABINET_ANIM_CLIP_IDLE)
		PRINTLN("[AM_ARCADE_CLAW_CRANE] SHOULD_MOVE_TO_IDLE_STATE idle anim is not running")
		RETURN FALSE
	ENDIF
	
	IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) = WAITING_TO_START_TASK
	OR GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) = PERFORMING_TASK
		PRINTLN("[AM_ARCADE_CLAW_CRANE] SHOULD_MOVE_TO_IDLE_STATE tasking player to move to coords")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC MAINTAIN_CLAW_CRANE_ATTRACT_AI_CONTEXT()
	IF NOT HAS_NET_TIMER_STARTED(sData.tAttractAIContextTimer)
		INT iDisplaySlot = GET_ARCADE_CABINET_LOCATE_DISPLAY_SLOT_PLAYER_IS_IN(PLAYER_ID())
		sData.sArcadeCabinetAIContext.eArcadeCabinet = ARCADE_CABINET_CH_CLAW_CRANE
		sData.sArcadeCabinetAIContext.sCabinetSlot = INT_TO_ENUM(ARCADE_CAB_MANAGER_SLOT, iDisplaySlot)
		sData.sArcadeCabinetAIContext.stContext = "CLAW_ATTRACT"
		PLAY_ARCADE_CABINET_AI_CONTEXT(sData.sArcadeCabinetAIContext, FALSE)			
		IF sData.sArcadeCabinetAIContext.bEventSent
			START_NET_TIMER(sData.tAttractAIContextTimer)
			PRINTLN("[AM_ARCADE_CLAW_CRANE] - MAINTAIN_CLAW_CRANE_ATTRACT_AI_CONTEXT START_NET_TIMER(sData.tAttractAIContextTimer)")
		ENDIF
	ELSE
		IF HAS_NET_TIMER_EXPIRED(sData.tAttractAIContextTimer, 40000)
			RESET_NET_TIMER(sData.tAttractAIContextTimer)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Maintain claw crane main logic loading state
PROC MAINTAIN_CLAW_CRANE_LOADING_STATE()
	IF SHOULD_MOVE_TO_IDLE_STATE()
		SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iLocalBS, PLAYER_BD_BS_PLAYING_GAME)
		PRINTLN("[AM_ARCADE_CLAW_CRANE] - MAINTAIN_CLAW_CRANE_LOADING_STATE PLAYER_BD_BS_PLAYING_GAME TRUE")
		RESET_NET_TIMER(sData.tHypeAIContextTimer)
		g_ArcadeCabinetManagerData.iClawPrizeIndex = -1 
		PLAY_SOUND_FROM_COORD(-1, "start", GET_ENTITY_COORDS(sData.cabinetObj), "dlc_ch_claw_crane_sounds", TRUE, 10)
		SET_ARCADE_CABINET_CHILD_SCRIPT_CLIENT_STATE(sData.eState, ARCADE_CABINET_STATE_IDLE)
		SET_CLAW_CRANE_GAME_STATE(CLAW_GAME_INIT_STATE)
	ELSE
		IF GET_ARCADE_CABINET_LOCATE_PLAYER_IS_IN(PLAYER_ID()) = sData.eArcadeCabinetLocateType
		AND sData.eArcadeCabinetLocateType = ARCADE_CABINET_CH_CLAW_CRANE
			MAINTAIN_CLAW_CRANE_ATTRACT_AI_CONTEXT()
		ELSE
			IF HAS_NET_TIMER_STARTED(sData.tAttractAIContextTimer)
				IF HAS_NET_TIMER_EXPIRED(sData.tAttractAIContextTimer, 10000)
					RESET_NET_TIMER(sData.tAttractAIContextTimer)
				ENDIF	
			ENDIF			
		ENDIF
	ENDIF	
ENDPROC

FUNC MODEL_NAMES GET_CLAW_CRANE_SUB_PROP_MODEL_NAMES(CLAW_CRANE_SUB_PROPS eClawCraneSubProps)
	SWITCH eClawCraneSubProps
		CASE CLAW_SUB_RAIL_X			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Arcade_Claw_01a_R2"))
		CASE CLAW_SUB_RAIL_Y			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Arcade_Claw_01a_R1")) 
		CASE CLAW_SUB_CLAW				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Arcade_Claw_01a_C"))
	ENDSWITCH
	
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

FUNC VECTOR GET_CLAW_CRANE_SUB_PROP_INITIAL_COORDS(CLAW_CRANE_SUB_PROPS eClawCraneSubProps)
	SWITCH eClawCraneSubProps
		CASE CLAW_SUB_RAIL_X			RETURN GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sData.cabinetObj, <<CLAW_CRAN_XRAIL_MIN_X_AXIS, CLAW_CRAN_MIN_Y_AXIS, CLAW_CRAN_XRAIL_MAX_Z_AXIS>>)
		CASE CLAW_SUB_RAIL_Y			RETURN GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sData.cabinetObj, <<CLAW_CRAN_YRAIL_MIN_X_AXIS, CLAW_CRAN_YRAIL_MIN_Y_AXIS, CLAW_CRAN_YRAIL_MAX_Z_AXIS>>)
		CASE CLAW_SUB_CLAW				RETURN GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sData.cabinetObj, <<CLAW_CRAN_MIN_X_AXIS, CLAW_CRAN_MIN_Y_AXIS, CLAW_CRAN_MAX_Z_AXIS>>)
	ENDSWITCH
	RETURN <<0, 0, 0>>
ENDFUNC

FUNC VECTOR GET_CLAW_CRANE_SUB_PROP_INITIAL_ROTATION(CLAW_CRANE_SUB_PROPS eClawCraneSubProps)
	SWITCH eClawCraneSubProps
		CASE CLAW_SUB_RAIL_X			RETURN <<0, 0, GET_ENTITY_HEADING(sData.cabinetObj)>>
		CASE CLAW_SUB_RAIL_Y			RETURN <<0, 0, GET_ENTITY_HEADING(sData.cabinetObj)>>
		CASE CLAW_SUB_CLAW				RETURN <<0, 0, GET_ENTITY_HEADING(sData.cabinetObj)>>
	ENDSWITCH
	RETURN <<0, 0, 0>>
ENDFUNC

/// PURPOSE:
///    Catch player controls for right/left sticks
PROC MAINTAIN_CLAW_CRANE_CONTROLS()
	INT fMouseSensitivity = 1

	ALLOW_ALTERNATIVE_SCRIPT_CONTROLS_LAYOUT(FRONTEND_CONTROL)
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
	
	//Get analogue stick positions.
	GET_CONTROL_VALUE_OF_ANALOGUE_STICKS_UNBOUND(sData.iStickPosition[CLAW_CRANE_LEFT_X],
									 	 sData.iStickPosition[CLAW_CRANE_LEFT_Y],
									 	 sData.iStickPosition[CLAW_CRANE_RIGHT_X],
									 	 sData.iStickPosition[CLAW_CRANE_RIGHT_Y])
	
	IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
		fMouseSensitivity = 5
		sData.iStickPosition[CLAW_CRANE_RIGHT_X] = sData.iStickPosition[CLAW_CRANE_RIGHT_X] * fMouseSensitivity
		sData.iStickPosition[CLAW_CRANE_RIGHT_Y] = sData.iStickPosition[CLAW_CRANE_RIGHT_Y] * fMouseSensitivity
	ENDIF									 															 
ENDPROC

PROC PLAY_CLAW_CRANE_MOVE_SOUND()
	IF sData.iMoveSoundID = -1
		sData.iMoveSoundID = GET_SOUND_ID()
		PLAY_SOUND_FROM_COORD(sData.iMoveSoundID, "move", GET_ENTITY_COORDS(sData.cabinetObj), "dlc_ch_claw_crane_sounds", TRUE, 10)
	ENDIF
ENDPROC

FUNC BOOL SHOULD_PAUSE_CLAW_CRANE()
	IF IS_PAUSE_MENU_ACTIVE()
	OR IS_COMMERCE_STORE_OPEN()
		PRINTLN("[AM_ARCADE_CLAW_CRANE] - SHOULD_PAUSE_CLAW_CRANE - pause or store is open")
		RETURN TRUE
	ENDIF
	
	IF  NETWORK_TEXT_CHAT_IS_TYPING()
		PRINTLN("[AM_ARCADE_CLAW_CRANE] - SHOULD_PAUSE_CLAW_CRANE - NETWORK_TEXT_CHAT_IS_TYPING is true")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Move claw in Y Axis. This will move with player control
PROC MOVE_CLAW_FORWARD()
	
	IF SHOULD_PAUSE_CLAW_CRANE()
		EXIT
	ENDIF	
	
	FLOAT fInterpSpeed = 0.005
	VECTOR vClawCurrentCoord, vXRailCurrentCoord, vYRailCurrentCoord
	FLOAT fFrameRateModifier = 30.0 * TIMESTEP()
	
	vXRailCurrentCoord = GET_ENTITY_COORDS(sData.clawObj[CLAW_SUB_RAIL_X])
	vYRailCurrentCoord = GET_ENTITY_COORDS(sData.clawObj[CLAW_SUB_RAIL_Y])
	vClawCurrentCoord = GET_ENTITY_COORDS(sData.clawObj[CLAW_SUB_CLAW])
	
	VECTOR vOffsetClaw = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(sData.cabinetObj, vClawCurrentCoord)
	VECTOR vOffsetXRail = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(sData.cabinetObj, vXRailCurrentCoord)
	VECTOR vOffsetYRail = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(sData.cabinetObj, vYRailCurrentCoord)
	
	FLOAT fRoundUpClowTwoDec = ROUND(vOffsetClaw.y * 100.0) / 100.0
	FLOAT fRoundUpXRailTwoDec = ROUND(vOffsetXRail.y * 100.0) / 100.0
	FLOAT fRoundUpYRailTwoDec = ROUND(vOffsetYRail.y * 100.0) / 100.0
	
	IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("ARC_CLAW_MO_F")
		PRINT_HELP_FOREVER("ARC_CLAW_MO_F")
	ENDIF
	
	PLAY_CLAW_CRANE_MOVE_SOUND()
	
	IF fRoundUpClowTwoDec <= CLAW_CRAN_MAX_Y_AXIS
	AND fRoundUpXRailTwoDec <= CLAW_CRAN_MAX_Y_AXIS
	AND fRoundUpYRailTwoDec <= CLAW_CRAN_YRAIL_MAX_Y_AXIS
	AND sData.iStickPosition[CLAW_CRANE_LEFT_Y] < 0
	
		// This is to calculate values for Claw
		FLOAT fNormalYStick, fYMovement
		VECTOR vTargetPos, vDir, vNewCoords 
		
		// This is to calculate values for XRail
		FLOAT fYMovementXRail
		VECTOR vTargetPosXRail, vDirXRail, vNewCoordsXRail
		
		// This is to calculate values for YRail
		FLOAT fYMovementYRail
		VECTOR vTargetPosYRail, vDirYRail, vNewCoordsYRail
		
		fNormalYStick = -1.0
		
		// Backward
		IF sData.iStickPosition[CLAW_CRANE_LEFT_Y] > 0
			sData.fMoveSoundVolume = 1
			vTargetPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sData.cabinetObj, <<CLAW_CRAN_MIN_X_AXIS, CLAW_CRAN_MIN_Y_AXIS, CLAW_CRAN_MAX_Z_AXIS>>)
			fYMovement = (fNormalYStick * fFrameRateModifier * fInterpSpeed)
			
			vTargetPosXRail = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sData.cabinetObj, <<CLAW_CRAN_XRAIL_MIN_X_AXIS, CLAW_CRAN_MIN_Y_AXIS, CLAW_CRAN_XRAIL_MAX_Z_AXIS>>)
			fYMovementXRail = (fNormalYStick * fFrameRateModifier * fInterpSpeed)
			
			vTargetPosYRail = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sData.cabinetObj, <<CLAW_CRAN_YRAIL_MIN_X_AXIS, CLAW_CRAN_YRAIL_MIN_Y_AXIS, CLAW_CRAN_YRAIL_MAX_Z_AXIS>>)
			fYMovementYRail = (fNormalYStick * fFrameRateModifier * fInterpSpeed)
		// Forward
		ELSE
			IF sData.iStickPosition[CLAW_CRANE_LEFT_Y] != 0
				sData.fMoveSoundVolume = 1
			ENDIF	
			
			vTargetPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sData.cabinetObj, <<CLAW_CRAN_MIN_X_AXIS, CLAW_CRAN_MAX_Y_AXIS, CLAW_CRAN_MAX_Z_AXIS>>)
			fYMovement = -(fNormalYStick * fFrameRateModifier * fInterpSpeed)
			
			vTargetPosXRail = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sData.cabinetObj, <<CLAW_CRAN_XRAIL_MIN_X_AXIS, CLAW_CRAN_MAX_Y_AXIS, CLAW_CRAN_XRAIL_MAX_Z_AXIS>>)
			fYMovementXRail = -(fNormalYStick * fFrameRateModifier * fInterpSpeed)
			
			vTargetPosYRail = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sData.cabinetObj, <<CLAW_CRAN_YRAIL_MIN_X_AXIS, CLAW_CRAN_YRAIL_MAX_Y_AXIS, CLAW_CRAN_YRAIL_MAX_Z_AXIS>>)
			fYMovementYRail = -(fNormalYStick * fFrameRateModifier * fInterpSpeed)
			
			SET_ARCADE_CLAW_CRANE_BIT(LOCAL_BS_CLAW_MOVE_FORWARD_COMPLETE)
			
			IF sData.sArcadeCabinetAnimEvent.ePreviousAnimClip != ARCADE_CABINET_ANIM_CLIP_CLAW_CRANE_PRESS_UP
				sData.sArcadeCabinetAIContext.stContext = "CLAW_MOVING_ARM"
				PLAY_ARCADE_CABINET_AI_CONTEXT(sData.sArcadeCabinetAIContext, TRUE)
				
				sData.sArcadeCabinetAnimEvent.bHoldLastFrame = FALSE
				sData.sArcadeCabinetAnimEvent.bLoopAnim = TRUE
				PLAY_ARCADE_CABINET_ANIMATION(sData.sArcadeCabinetAnimEvent, ARCADE_CABINET_ANIM_CLIP_CLAW_CRANE_PRESS_UP)
			ENDIF	
		ENDIF

		vDir = NORMALISE_VECTOR(vTargetPos - vClawCurrentCoord)
		vNewCoords = (vDir * fYMovement) + vClawCurrentCoord
		
		vDirXRail = NORMALISE_VECTOR(vTargetPosXRail - vXRailCurrentCoord)
		vNewCoordsXRail = (vDirXRail * fYMovementXRail) + vXRailCurrentCoord
		
		vDirYRail = NORMALISE_VECTOR(vTargetPosYRail - vYRailCurrentCoord)
		vNewCoordsYRail = (vDirYRail * fYMovementYRail) + vYRailCurrentCoord
		
		IF TAKE_CONTROL_OF_ENTITY(sData.clawObj[CLAW_SUB_RAIL_X])
			SET_ENTITY_COORDS(sData.clawObj[CLAW_SUB_RAIL_X], vNewCoordsXRail)
		ENDIF
		
		IF TAKE_CONTROL_OF_ENTITY(sData.clawObj[CLAW_SUB_RAIL_y])
			SET_ENTITY_COORDS(sData.clawObj[CLAW_SUB_RAIL_Y], vNewCoordsYRail)
		ENDIF
		
		IF TAKE_CONTROL_OF_ENTITY(sData.clawObj[CLAW_SUB_CLAW])
			SET_ENTITY_COORDS(sData.clawObj[CLAW_SUB_CLAW], vNewCoords)
		ENDIF
	ELSE
		IF IS_ARCADE_CLAW_CRANE_BIT_SET(LOCAL_BS_CLAW_MOVE_FORWARD_COMPLETE)
			sData.fMoveSoundVolume = 0
			SET_CLAW_CRANE_GAME_STATE(CLAW_GAME_MOVE_RIGHT_STATE)
		ENDIF
	ENDIF	
ENDPROC

FUNC BOOL SHOULD_SERVER_DROP_THE_PRIZE()
	// *** if user hasn't won return false 
	IF !IS_ARCADE_CLAW_CRANE_BIT_SET(LOCAL_BS_ATTACHED_PRIZE_TO_CLAW)
		RETURN FALSE
	ENDIF	
	
	IF serverBD.plUsingClawCrane != INVALID_PLAYER_INDEX()
	AND !NETWORK_IS_PLAYER_A_PARTICIPANT(serverBD.plUsingClawCrane) 
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///   Move claw in Y Axis. This will move without player control
PROC MOVE_CLAW_BACKWARD(BOOL bHostCleanUp = FALSE)
	FLOAT fInterpSpeed = 0.005
	VECTOR vClawCurrentCoord, vXRailCurrentCoord, vYRailCurrentCoord
	FLOAT fFrameRateModifier = 30.0 * TIMESTEP()

	vXRailCurrentCoord = GET_ENTITY_COORDS(sData.clawObj[CLAW_SUB_RAIL_X])
	vYRailCurrentCoord = GET_ENTITY_COORDS(sData.clawObj[CLAW_SUB_RAIL_Y])
	vClawCurrentCoord = GET_ENTITY_COORDS(sData.clawObj[CLAW_SUB_CLAW])
	
	FLOAT fNormalYStick, fYMovement
	VECTOR vTargetPosClaw, vDir, vNewClawCoords 
	fNormalYStick = 1.0
	
	// This is to calculate values for XRail
	FLOAT fYMovementXRail
	VECTOR vTargetPosXRail, vDirXRail, vNewCoordsXRail
	
	// This is to calculate values for YRail
	FLOAT fYMovementYRail
	VECTOR vTargetPosYRail, vDirYRail, vNewCoordsYRail

	// Claw
	vTargetPosClaw = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sData.cabinetObj, <<CLAW_CRAN_MIN_X_AXIS, CLAW_CRAN_MIN_Y_AXIS, CLAW_CRAN_MAX_Z_AXIS>>)
	fYMovement = (fNormalYStick * fFrameRateModifier * fInterpSpeed)

	vDir = NORMALISE_VECTOR(vTargetPosClaw - vClawCurrentCoord)
	vNewClawCoords = (vDir * fYMovement) + vClawCurrentCoord
	
	// XRail
	vTargetPosXRail = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sData.cabinetObj, <<CLAW_CRAN_XRAIL_MIN_X_AXIS, CLAW_CRAN_MIN_Y_AXIS, CLAW_CRAN_XRAIL_MAX_Z_AXIS>>)
	fYMovementXRail = (fNormalYStick * fFrameRateModifier * fInterpSpeed)

	vDirXRail = NORMALISE_VECTOR(vTargetPosXRail - vXRailCurrentCoord)
	vNewCoordsXRail = (vDirXRail * fYMovementXRail) + vXRailCurrentCoord
	
	/// YRail
	vTargetPosYRail = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sData.cabinetObj, <<CLAW_CRAN_YRAIL_MIN_X_AXIS, CLAW_CRAN_YRAIL_MIN_Y_AXIS, CLAW_CRAN_YRAIL_MAX_Z_AXIS>>)
	fYMovementYRail = (fNormalYStick * fFrameRateModifier * fInterpSpeed)

	vDirYRail = NORMALISE_VECTOR(vTargetPosYRail - vYRailCurrentCoord)
	vNewCoordsYRail = (vDirYRail * fYMovementYRail) + vYRailCurrentCoord

	PLAY_CLAW_CRANE_MOVE_SOUND()
	sData.fMoveSoundVolume = 1
	
	VECTOR iInitialClawCoord = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sData.cabinetObj, <<CLAW_CRAN_MIN_X_AXIS, CLAW_CRAN_MIN_Y_AXIS, CLAW_CRAN_MAX_Z_AXIS>>)
	VECTOR iInitialXRailCoord = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sData.cabinetObj, <<CLAW_CRAN_XRAIL_MIN_X_AXIS, CLAW_CRAN_MIN_Y_AXIS, CLAW_CRAN_XRAIL_MAX_Z_AXIS>>)
	VECTOR iInitialYRailCoord = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sData.cabinetObj, <<CLAW_CRAN_YRAIL_MIN_X_AXIS, CLAW_CRAN_YRAIL_MIN_Y_AXIS, CLAW_CRAN_YRAIL_MAX_Z_AXIS>>)
	
	IF TAKE_CONTROL_OF_ENTITY(sData.clawObj[CLAW_SUB_RAIL_X])
	AND !ARE_VECTORS_ALMOST_EQUAL(vXRailCurrentCoord, iInitialXRailCoord, 0.001)
		SET_ENTITY_COORDS(sData.clawObj[CLAW_SUB_RAIL_X], vNewCoordsXRail)
	ENDIF
	
	IF TAKE_CONTROL_OF_ENTITY(sData.clawObj[CLAW_SUB_RAIL_y])
	AND !ARE_VECTORS_ALMOST_EQUAL(vYRailCurrentCoord, iInitialYRailCoord, 0.001)
		SET_ENTITY_COORDS(sData.clawObj[CLAW_SUB_RAIL_Y], vNewCoordsYRail)
	ENDIF
	
	IF TAKE_CONTROL_OF_ENTITY(sData.clawObj[CLAW_SUB_CLAW])
	AND !ARE_VECTORS_ALMOST_EQUAL(vClawCurrentCoord, iInitialClawCoord, 0.001)
		SET_ENTITY_COORDS(sData.clawObj[CLAW_SUB_CLAW], vNewClawCoords)
	ENDIF
	
	IF ARE_VECTORS_ALMOST_EQUAL(vXRailCurrentCoord, iInitialXRailCoord, 0.01)
	AND ARE_VECTORS_ALMOST_EQUAL(vYRailCurrentCoord, iInitialYRailCoord, 0.01)
	AND ARE_VECTORS_ALMOST_EQUAL(vClawCurrentCoord, iInitialClawCoord, 0.01)
		IF !bHostCleanUp
			IF IS_ARCADE_CLAW_CRANE_BIT_SET(LOCAL_BS_ATTACHED_PRIZE_TO_CLAW)
				SET_CLAW_CRANE_GAME_STATE(CLAW_GAME_DROP_PRIZE_STATE)
				PLAY_SOUND_FROM_COORD(-1, "ascend_win", GET_ENTITY_COORDS(sData.cabinetObj), "dlc_ch_claw_crane_sounds", TRUE, 10)
			ELSE
				PLAY_SOUND_FROM_COORD(-1, "ascend_lose", GET_ENTITY_COORDS(sData.cabinetObj), "dlc_ch_claw_crane_sounds", TRUE, 10)
				SET_CLAW_CRANE_GAME_STATE(CLAW_GAME_CLEANUP_STATE)
				sData.sArcadeCabinetAIContext.stContext = "CLAW_FAIL"
				PLAY_SOUND_FROM_COORD(-1, "lose", GET_ENTITY_COORDS(sData.cabinetObj), "dlc_ch_claw_crane_sounds", TRUE, 10)
				PLAY_ARCADE_CABINET_AI_CONTEXT(sData.sArcadeCabinetAIContext, TRUE)
			ENDIF
		ELSE
			IF SHOULD_SERVER_DROP_THE_PRIZE()
				SET_CLAW_CRANE_SERVER_GAME_STATE(CLAW_GAME_DROP_PRIZE_STATE)
			ELSE	
				SET_CLAW_CRANE_SERVER_GAME_STATE(CLAW_GAME_CLEANUP_STATE)
			ENDIF	
		ENDIF	
	ENDIF
ENDPROC

/// PURPOSE:
///    Move claw in X Axis (right) this will check for player control
PROC MOVE_CLAW_RIGHT()

	FLOAT fInterpSpeed = 0.005
	VECTOR currentRailXCoord, currentClawCoord
	FLOAT fFrameRateModifier = 30.0 * TIMESTEP()
	
	currentRailXCoord = GET_ENTITY_COORDS(sData.clawObj[CLAW_SUB_RAIL_X])
	currentClawCoord = GET_ENTITY_COORDS(sData.clawObj[CLAW_SUB_CLAW])
	
	VECTOR vOffset = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(sData.cabinetObj, currentRailXCoord)
	VECTOR vYRailOffset = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(sData.cabinetObj, currentClawCoord)
	
	FLOAT fRoundUpClowTwoDec = ROUND(vOffset.x * 100.0) / 100.0
	FLOAT fRoundUpXRailTwoDec = ROUND(vYRailOffset.x * 100.0) / 100.0
	
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("ARC_CLAW_MO_F")
		CLEAR_HELP()
	ENDIF
	
	IF SHOULD_PAUSE_CLAW_CRANE()
		EXIT
	ENDIF	
	
	IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("ARC_CLAW_MO_R")
		PRINT_HELP_FOREVER("ARC_CLAW_MO_R")
	ENDIF
	
	IF fRoundUpClowTwoDec <= CLAW_CRAN_MAX_X_AXIS
	AND fRoundUpXRailTwoDec <= CLAW_CRAN_MAX_X_AXIS
	AND sData.iStickPosition[CLAW_CRANE_LEFT_X] > 0
		FLOAT fNormalYStick, fYMovement
		VECTOR vTargetPos, vDir, vNewCoords 
		fNormalYStick = 1.0
		
		FLOAT fXRailMovement
		VECTOR vXRailTargetPos, vXRailDir, vXRailNewCoords 

		// Right
		IF sData.iStickPosition[CLAW_CRANE_LEFT_X] > 0
			sData.fMoveSoundVolume = 1 

			vTargetPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sData.cabinetObj, <<CLAW_CRAN_MAX_X_AXIS, vOffset.y, CLAW_CRAN_MAX_Z_AXIS>>)
			fYMovement = (fNormalYStick * fFrameRateModifier * fInterpSpeed)
			
			vXRailTargetPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sData.cabinetObj, <<CLAW_CRAN_XRAIL_MAX_Z_AXIS, vYRailOffset.y, CLAW_CRAN_XRAIL_MAX_Z_AXIS>>)
			fXRailMovement = (fNormalYStick * fFrameRateModifier * fInterpSpeed)
			
			SET_ARCADE_CLAW_CRANE_BIT(LOCAL_BS_CLAW_MOVE_RIGHT_COMPLETE)
			IF sData.sArcadeCabinetAnimEvent.ePreviousAnimClip != ARCADE_CABINET_ANIM_CLIP_CLAW_CRANE_PRESS_RIGHT
				sData.sArcadeCabinetAIContext.stContext = "CLAW_MOVING_ARM"
				PLAY_ARCADE_CABINET_AI_CONTEXT(sData.sArcadeCabinetAIContext, TRUE)
				
				sData.sArcadeCabinetAnimEvent.bHoldLastFrame = FALSE
				sData.sArcadeCabinetAnimEvent.bLoopAnim = TRUE
				PLAY_ARCADE_CABINET_ANIMATION(sData.sArcadeCabinetAnimEvent, ARCADE_CABINET_ANIM_CLIP_CLAW_CRANE_PRESS_RIGHT)
			ENDIF	
		ENDIF

		vDir = NORMALISE_VECTOR(vTargetPos - currentClawCoord)
		vNewCoords = (vDir * fYMovement) + currentClawCoord
		
		vXRailDir = NORMALISE_VECTOR(vXRailTargetPos - currentRailXCoord)
		vXRailNewCoords = (vXRailDir * fXRailMovement) + currentRailXCoord
		
		IF TAKE_CONTROL_OF_ENTITY(sData.clawObj[CLAW_SUB_RAIL_X])
			SET_ENTITY_COORDS(sData.clawObj[CLAW_SUB_RAIL_X], vXRailNewCoords)
		ENDIF
		
		IF TAKE_CONTROL_OF_ENTITY(sData.clawObj[CLAW_SUB_CLAW])
			SET_ENTITY_COORDS(sData.clawObj[CLAW_SUB_CLAW], vNewCoords)
		ENDIF	
	ELSE
		IF IS_ARCADE_CLAW_CRANE_BIT_SET(LOCAL_BS_CLAW_MOVE_RIGHT_COMPLETE)
			sData.fMoveSoundVolume = 0
			SET_CLAW_CRANE_GAME_STATE(CLAW_GAME_WAIT_TO_TRIGGER_PICK_UP_STATE)
		ELSE
			IF IS_PLAYER_PLAYING_THIS_ARCADE_CABINET_ANIMATION_CLIP(ARCADE_CABINET_ANIM_CLIP_CLAW_CRANE_PRESS_UP)
			AND HAS_ARCADE_CABINET_ANIMATION_CLIP_FINISHED(ARCADE_CABINET_ANIM_CLIP_CLAW_CRANE_PRESS_UP)
				IF sData.sArcadeCabinetAnimEvent.ePreviousAnimClip != ARCADE_CABINET_ANIM_CLIP_CLAW_CRANE_PRESS_RIGHT
				AND sData.sArcadeCabinetAnimEvent.ePreviousAnimClip != ARCADE_CABINET_ANIM_CLIP_IDLE
					sData.sArcadeCabinetAnimEvent.bHoldLastFrame = FALSE
					sData.sArcadeCabinetAnimEvent.bLoopAnim = TRUE
					PLAY_ARCADE_CABINET_ANIMATION(sData.sArcadeCabinetAnimEvent, ARCADE_CABINET_ANIM_CLIP_IDLE)
				ENDIF	
			ENDIF
		ENDIF
	ENDIF	
ENDPROC

/// PURPOSE:
///    Move claw in X Axis (left) moves without player control
PROC MOVE_CLAW_LEFT(BOOL bHostCleanUp = FALSE)
	FLOAT fInterpSpeed = 0.005
	VECTOR vCurrentRailXCoord, vClawCurrentCoord
	FLOAT fFrameRateModifier = 30.0 * TIMESTEP()

	vCurrentRailXCoord = GET_ENTITY_COORDS(sData.clawObj[CLAW_SUB_RAIL_X])
	vClawCurrentCoord = GET_ENTITY_COORDS(sData.clawObj[CLAW_SUB_CLAW])
	
	FLOAT fNormalYStick, fYMovement
	VECTOR vRailXTargetPos, vClawTargetPos, vRailXDir, vRailXNewCoords, vClawNewCoords, vClawDir
	fNormalYStick = -1.0
	
	VECTOR vRailXOffset = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(sData.cabinetObj, vCurrentRailXCoord)
	VECTOR vClawOffset = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(sData.cabinetObj, vClawCurrentCoord)

	vRailXTargetPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sData.cabinetObj, <<CLAW_CRAN_XRAIL_MIN_X_AXIS, vRailXOffset.y, CLAW_CRAN_XRAIL_MAX_Z_AXIS>>)
	vClawTargetPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sData.cabinetObj, <<CLAW_CRAN_MIN_X_AXIS, vClawOffset.y, CLAW_CRAN_MAX_Z_AXIS>>)
	
	fYMovement = -(fNormalYStick * fFrameRateModifier * fInterpSpeed)
	
	vRailXDir = NORMALISE_VECTOR(vRailXTargetPos - vCurrentRailXCoord)
	vRailXNewCoords = (vRailXDir * fYMovement) + vCurrentRailXCoord
	
	vClawDir = NORMALISE_VECTOR(vClawTargetPos - vClawCurrentCoord)
	vClawNewCoords = (vClawDir * fYMovement) + vClawCurrentCoord
	
	FLOAT fRoundUpTwoDec = ROUND(vRailXOffset.x * 100.0) / 100.0
	FLOAT fRoundUpClawTwoDec = ROUND(vClawOffset.x * 100.0) / 100.0
	
	IF TAKE_CONTROL_OF_ENTITY(sData.clawObj[CLAW_SUB_RAIL_X])
	AND fRoundUpTwoDec > CLAW_CRAN_XRAIL_MIN_X_AXIS
		IF IS_ENTITY_ATTACHED(sData.clawObj[CLAW_SUB_RAIL_X])
			DETACH_ENTITY(sData.clawObj[CLAW_SUB_RAIL_X])
		ENDIF	
		SET_ENTITY_COMPLETELY_DISABLE_COLLISION(sData.clawObj[CLAW_SUB_RAIL_X], TRUE)
		SET_ENTITY_COLLISION(sData.clawObj[CLAW_SUB_RAIL_X], TRUE)
		FREEZE_ENTITY_POSITION(sData.clawObj[CLAW_SUB_RAIL_X], FALSE)
		SET_ENTITY_COORDS(sData.clawObj[CLAW_SUB_RAIL_X], vRailXNewCoords)
	ENDIF
	
	IF TAKE_CONTROL_OF_ENTITY(sData.clawObj[CLAW_SUB_CLAW])
	AND fRoundUpClawTwoDec > CLAW_CRAN_MIN_X_AXIS
		FREEZE_ENTITY_POSITION(sData.clawObj[CLAW_SUB_CLAW], FALSE)
		SET_ENTITY_COORDS(sData.clawObj[CLAW_SUB_CLAW], vClawNewCoords)
	ENDIF
	
	PLAY_CLAW_CRANE_MOVE_SOUND()	
	sData.fMoveSoundVolume = 1
	
	IF fRoundUpTwoDec <= CLAW_CRAN_XRAIL_MIN_X_AXIS
	AND fRoundUpClawTwoDec <= CLAW_CRAN_MIN_X_AXIS
		IF !bHostCleanUp
			SET_CLAW_CRANE_GAME_STATE(CLAW_GAME_MOVE_BACKWARD_STATE)
		ELSE
			SET_CLAW_CRANE_SERVER_GAME_STATE(CLAW_GAME_MOVE_BACKWARD_STATE)
		ENDIF	
	ENDIF	
ENDPROC

PROC GET_VECTOR_FROM_ROTATION(VECTOR &InVec)
      
    FLOAT CosAngle
    FLOAT SinAngle
    VECTOR ReturnVec

    // Rotation about the x axis 
    CosAngle = COS(0)
    SinAngle = SIN(0)
    ReturnVec.x = InVec.x
    ReturnVec.y = (CosAngle * InVec.y) - (SinAngle * InVec.z)
    ReturnVec.z = (SinAngle * InVec.y) + (CosAngle * InVec.z)
    InVec = ReturnVec

    // Rotation about the y axis
    CosAngle = COS(0)
    SinAngle = SIN(0)
    ReturnVec.x = (CosAngle * InVec.x) + (SinAngle * InVec.z)
    ReturnVec.y = InVec.y
    ReturnVec.z = (CosAngle * InVec.z) - (SinAngle * InVec.x) 
    InVec = ReturnVec
    
    // Rotation about the z axis 
    CosAngle = COS(0)
    SinAngle = SIN(0)
    ReturnVec.x = (CosAngle * InVec.x) - (SinAngle * InVec.y)
    ReturnVec.y = (SinAngle * InVec.x) + (CosAngle * InVec.y)
    ReturnVec.z = InVec.z
    InVec = ReturnVec
ENDPROC

FUNC VECTOR GET_CAMERA_CLAW_FORWARD_VECTOR()
	VECTOR vEnd
	//end vector - pointing north
	vEnd	= <<0.0, 1.0, 0.0>>
	GET_VECTOR_FROM_ROTATION(vEnd)
	RETURN vEnd
ENDFUNC

FUNC BOOL IS_THIS_RARE_PRIZE(ENTITY_INDEX entityToCheck)
	IF DOES_ENTITY_EXIST(entityToCheck)
	AND NOT IS_ENTITY_DEAD(entityToCheck)
		MODEL_NAMES prizeModel = GET_ENTITY_MODEL(entityToCheck)
		IF prizeModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_princess_robo_Plush_07a"))
		OR prizeModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_shiny_wasabi_Plush_08a"))
		OR prizeModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_master_09a"))
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC STRING GET_CLAW_CRANE_PRIZE_NAME(ENTITY_INDEX entityToCheck)
	IF DOES_ENTITY_EXIST(entityToCheck)
	AND NOT IS_ENTITY_DEAD(entityToCheck)
		MODEL_NAMES prizeModel = GET_ENTITY_MODEL(entityToCheck)
		IF prizeModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_arcade_claw_plush_05a"))
			RETURN "ARCCAB_CLAW_P4"
		ELIF prizeModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_arcade_claw_plush_03a"))
			RETURN "ARCCAB_CLAW_P1"
		ELIF prizeModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_arcade_claw_plush_01a"))
			RETURN "ARCCAB_CLAW_P2"
		ELIF prizeModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_arcade_claw_plush_02a"))
			RETURN "ARCCAB_CLAW_P3"
		ELIF prizeModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_arcade_claw_plush_06a"))
			RETURN "ARCCAB_CLAW_P5"
		ELIF prizeModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_arcade_claw_plush_04a"))
			RETURN "ARCCAB_CLAW_P6"
		ELIF prizeModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_princess_robo_Plush_07a"))
			RETURN "ARCCAB_CLAW_P7"	
		ELIF prizeModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_shiny_wasabi_Plush_08a"))
			RETURN "ARCCAB_CLAW_P8"	
		ELIF prizeModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_master_09a"))
			RETURN "ARCCAB_CLAW_P9"		
		ENDIF
	ENDIF
	RETURN ""
ENDFUNC		

PROC SET_CLAW_CRANE_TROPHY_STATS()
	IF DOES_ENTITY_EXIST(GET_COLLIDED_PRIZE_INDEX(FALSE))
	AND NOT IS_ENTITY_DEAD(GET_COLLIDED_PRIZE_INDEX(FALSE))
		INT iClawTrophy = GET_MP_INT_CHARACTER_STAT(MP_STAT_CH_ARC_CAB_CLAW_TROPHY)
		MODEL_NAMES prizeModel = GET_ENTITY_MODEL(GET_COLLIDED_PRIZE_INDEX(FALSE))
		
		IF prizeModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_arcade_claw_plush_05a"))
			SET_BIT(iClawTrophy, CLAW_CRANE_TROPHY_BS_PRIZE_5_WON)
			sData.trophyType = ARCADE_GAME_TROPHY_CLAW_CRANE_SMOKEY_PLUSH
			IF !GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_ARCADE_CLAW_TSHIRT_YELLOW_PLUSH)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_ARCADE_CLAW_TSHIRT_YELLOW_PLUSH, TRUE)
				ARCADE_CABINET_FLOW_NOTIFICATIONS_SET_SHOW_TROPHY_HELP(sData.trophyType, FALSE)
				ARCADE_CABINET_FLOW_NOTIFICATIONS_SET_SHOW_TSHIRT_HELP(ARCADE_GAME_TSHIRTS_CLAW_CRANE_SMOKEY_PLUSH)
				PRINTLN("[AM_ARCADE_CLAW_CRANE] - SET_CLAW_CRANE_TROPHY_STATS CLAW_CRANE_TROPHY_BS_PRIZE_5_WON TRUE")
			ELSE
				NEXT_RP_ADDITION_SHOW_RANKBAR()
				GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD, PLAYER_PED_ID(), "CLAW GAME RP250", XPTYPE_AWARDS, XPCATEGORY_RP_CLAW_CRANE, g_sMptunables.iARCADE_CLAW_WIN_XP_COMMON_PLUSH)
			ENDIF	
		ELIF prizeModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_arcade_claw_plush_03a"))
			SET_BIT(iClawTrophy, CLAW_CRANE_TROPHY_BS_PRIZE_3_WON)
			sData.trophyType = ARCADE_GAME_TROPHY_CLAW_CRANE_SAKI_PLUSH
			IF !GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_ARCADE_CLAW_TSHIRT_BLUE_PLUSH)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_ARCADE_CLAW_TSHIRT_BLUE_PLUSH, TRUE)
				ARCADE_CABINET_FLOW_NOTIFICATIONS_SET_SHOW_TROPHY_HELP(sData.trophyType, FALSE)
				ARCADE_CABINET_FLOW_NOTIFICATIONS_SET_SHOW_TSHIRT_HELP(ARCADE_GAME_TSHIRTS_CLAW_CRANE_SAKI_PLUSH)
				PRINTLN("[AM_ARCADE_CLAW_CRANE] - SET_CLAW_CRANE_TROPHY_STATS CLAW_CRANE_TROPHY_BS_PRIZE_3_WON TRUE")
			ELSE
				NEXT_RP_ADDITION_SHOW_RANKBAR()
				GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD, PLAYER_PED_ID(), "CLAW GAME RP250", XPTYPE_AWARDS, XPCATEGORY_RP_CLAW_CRANE, g_sMptunables.iARCADE_CLAW_WIN_XP_COMMON_PLUSH)
			ENDIF	
		ELIF prizeModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_arcade_claw_plush_01a"))
			SET_BIT(iClawTrophy, CLAW_CRANE_TROPHY_BS_PRIZE_1_WON)
			sData.trophyType = ARCADE_GAME_TROPHY_CLAW_CRANE_HUMPY_PLUSH
			IF !GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_ARCADE_CLAW_TSHIRT_PURPLE_PLUSH)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_ARCADE_CLAW_TSHIRT_PURPLE_PLUSH, TRUE)
				ARCADE_CABINET_FLOW_NOTIFICATIONS_SET_SHOW_TROPHY_HELP(sData.trophyType, FALSE)
				ARCADE_CABINET_FLOW_NOTIFICATIONS_SET_SHOW_TSHIRT_HELP(ARCADE_GAME_TSHIRTS_CLAW_CRANE_HUMPY_PLUSH)
				PRINTLN("[AM_ARCADE_CLAW_CRANE] - SET_CLAW_CRANE_TROPHY_STATS CLAW_CRANE_TROPHY_BS_PRIZE_1_WON TRUE")
			ELSE
				NEXT_RP_ADDITION_SHOW_RANKBAR()
				GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD, PLAYER_PED_ID(), "CLAW GAME RP250", XPTYPE_AWARDS, XPCATEGORY_RP_CLAW_CRANE, g_sMptunables.iARCADE_CLAW_WIN_XP_COMMON_PLUSH)
			ENDIF	
		ELIF prizeModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_arcade_claw_plush_02a"))
			SET_BIT(iClawTrophy, CLAW_CRANE_TROPHY_BS_PRIZE_2_WON)
			sData.trophyType = ARCADE_GAME_TROPHY_CLAW_CRANE_METHY_PLUSH
			IF !GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_ARCADE_CLAW_TSHIRT_GREEN_PLUSH)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_ARCADE_CLAW_TSHIRT_GREEN_PLUSH, TRUE)
				ARCADE_CABINET_FLOW_NOTIFICATIONS_SET_SHOW_TROPHY_HELP(sData.trophyType, FALSE)
				ARCADE_CABINET_FLOW_NOTIFICATIONS_SET_SHOW_TSHIRT_HELP(ARCADE_GAME_TSHIRTS_CLAW_CRANE_METHY_PLUSH)
				PRINTLN("[AM_ARCADE_CLAW_CRANE] - SET_CLAW_CRANE_TROPHY_STATS CLAW_CRANE_TROPHY_BS_PRIZE_2_WON TRUE")
			ELSE
				NEXT_RP_ADDITION_SHOW_RANKBAR()
				GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD, PLAYER_PED_ID(), "CLAW GAME RP250", XPTYPE_AWARDS, XPCATEGORY_RP_CLAW_CRANE, g_sMptunables.iARCADE_CLAW_WIN_XP_COMMON_PLUSH)
			ENDIF	
		ELIF prizeModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_arcade_claw_plush_06a"))
			SET_BIT(iClawTrophy, CLAW_CRANE_TROPHY_BS_PRIZE_6_WON)
			sData.trophyType = ARCADE_GAME_TROPHY_CLAW_CRANE_GRINDY_PLUSH
			IF !GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_ARCADE_CLAW_TSHIRT_RED_PLUSH)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_ARCADE_CLAW_TSHIRT_RED_PLUSH, TRUE)
				ARCADE_CABINET_FLOW_NOTIFICATIONS_SET_SHOW_TSHIRT_HELP(ARCADE_GAME_TSHIRTS_CLAW_CRANE_GRINDY_PLUSH)
				PRINTLN("[AM_ARCADE_CLAW_CRANE] - SET_CLAW_CRANE_TROPHY_STATS CLAW_CRANE_TROPHY_BS_PRIZE_6_WON TRUE")
			ELSE
				NEXT_RP_ADDITION_SHOW_RANKBAR()
				GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD, PLAYER_PED_ID(), "CLAW GAME RP250", XPTYPE_AWARDS, XPCATEGORY_RP_CLAW_CRANE, g_sMptunables.iARCADE_CLAW_WIN_XP_COMMON_PLUSH)
			ENDIF
		ELIF prizeModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_arcade_claw_plush_04a"))
			sData.trophyType = ARCADE_GAME_TROPHY_CLAW_CRANE_POOPY_PLUSH
			SET_BIT(iClawTrophy, CLAW_CRANE_TROPHY_BS_PRIZE_4_WON)
			IF !GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_ARCADE_CLAW_TSHIRT_BROWN_PLUSH)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_ARCADE_CLAW_TSHIRT_BROWN_PLUSH, TRUE)
				ARCADE_CABINET_FLOW_NOTIFICATIONS_SET_SHOW_TROPHY_HELP(sData.trophyType, FALSE)
				ARCADE_CABINET_FLOW_NOTIFICATIONS_SET_SHOW_TSHIRT_HELP(ARCADE_GAME_TSHIRTS_CLAW_CRANE_POOPY_PLUSH)
				PRINTLN("[AM_ARCADE_CLAW_CRANE] - SET_CLAW_CRANE_TROPHY_STATS CLAW_CRANE_TROPHY_BS_PRIZE_4_WON TRUE")
			ELSE
				NEXT_RP_ADDITION_SHOW_RANKBAR()
				GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD, PLAYER_PED_ID(), "CLAW GAME RP250", XPTYPE_AWARDS, XPCATEGORY_RP_CLAW_CRANE, g_sMptunables.iARCADE_CLAW_WIN_XP_COMMON_PLUSH)
			ENDIF	
		ELIF prizeModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_shiny_wasabi_Plush_08a"))
			SET_BIT(iClawTrophy, CLAW_CRANE_TROPHY_BS_PRIZE_7_WON)
			sData.trophyType = ARCADE_GAME_TROPHY_CLAW_CRANE_WASABI_PLUSH
			IF !GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_ARCADE_CLAW_TSHIRT_SHINY_WASABI_PLUSH)
				ARCADE_CABINET_FLOW_NOTIFICATIONS_SET_SHOW_TROPHY_HELP(sData.trophyType, FALSE)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_ARCADE_CLAW_TSHIRT_SHINY_WASABI_PLUSH, TRUE)
				ARCADE_CABINET_FLOW_NOTIFICATIONS_SET_SHOW_TSHIRT_HELP(ARCADE_GAME_TSHIRTS_CLAW_CRANE_WASABI_PLUSH)
				PRINTLN("[AM_ARCADE_CLAW_CRANE] - SET_CLAW_CRANE_TROPHY_STATS CLAW_CRANE_TROPHY_BS_PRIZE_7_WON TRUE")
			ELSE
				NEXT_RP_ADDITION_SHOW_RANKBAR()
				GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD, PLAYER_PED_ID(), "CLAW GAME RP500", XPTYPE_AWARDS, XPCATEGORY_RP_CLAW_CRANE, g_sMptunables.iARCADE_CLAW_WIN_XP_WASABI_PLUSH)
			ENDIF	
		ELIF prizeModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_princess_robo_Plush_07a"))
			SET_BIT(iClawTrophy, CLAW_CRANE_TROPHY_BS_PRIZE_8_WON)
			sData.trophyType = ARCADE_GAME_TROPHY_CLAW_CRANE_PRINCESS_PLUSH
			IF !GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_ARCADE_CLAW_TSHIRT_PRINCESS_ROBOT_PLUSH)
				ARCADE_CABINET_FLOW_NOTIFICATIONS_SET_SHOW_TROPHY_HELP(sData.trophyType, FALSE)
				ARCADE_CABINET_FLOW_NOTIFICATIONS_SET_SHOW_TSHIRT_HELP(ARCADE_GAME_TSHIRTS_CLAW_CRANE_PRINCESS_PLUSH)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_ARCADE_CLAW_TSHIRT_PRINCESS_ROBOT_PLUSH, TRUE)
				PRINTLN("[AM_ARCADE_CLAW_CRANE] - SET_CLAW_CRANE_TROPHY_STATS CLAW_CRANE_TROPHY_BS_PRIZE_8_WON TRUE")
			ELSE
				NEXT_RP_ADDITION_SHOW_RANKBAR()
				GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD, PLAYER_PED_ID(), "CLAW GAME RP750", XPTYPE_AWARDS, XPCATEGORY_RP_CLAW_CRANE, g_sMptunables.iARCADE_CLAW_WIN_XP_PRINCESS_PLUSH)
			ENDIF	
		ELIF prizeModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_master_09a"))
			SET_BIT(iClawTrophy, CLAW_CRANE_TROPHY_BS_PRIZE_9_WON)
			sData.trophyType = ARCADE_GAME_TROPHY_CLAW_CRANE_MASTER_PLUSH
			IF !GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_ARCADE_CLAW_TSHIRT_MASTER_PLUSH)
				ARCADE_CABINET_FLOW_NOTIFICATIONS_SET_SHOW_TROPHY_HELP(sData.trophyType, FALSE)
				ARCADE_CABINET_FLOW_NOTIFICATIONS_SET_SHOW_TSHIRT_HELP(ARCADE_GAME_TSHIRTS_CLAW_CRANE_MASTER_PLUSH)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_ARCADE_CLAW_TSHIRT_MASTER_PLUSH, TRUE)
				PRINTLN("[AM_ARCADE_CLAW_CRANE] - SET_CLAW_CRANE_TROPHY_STATS CLAW_CRANE_TROPHY_BS_PRIZE_9_WON TRUE")	
			ELSE
				NEXT_RP_ADDITION_SHOW_RANKBAR()
				GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD, PLAYER_PED_ID(), "CLAW GAME RP1000", XPTYPE_AWARDS, XPCATEGORY_RP_CLAW_CRANE, g_sMptunables.iARCADE_CLAW_WIN_XP_MASTER_PLUSH)
			ENDIF	
		ENDIF
		
		INT i, iNumPrize 
		FOR i = 0 TO CLAW_CRANE_TROPHY_BS_PRIZE_9_WON
			IF IS_BIT_SET(iClawTrophy, i)
				iNumPrize++
			ENDIF
		ENDFOR	
		
		IF iNumPrize = MAX_NUM_CLAW_PRIZE_TYPE
			IF NOT IS_BIT_SET(iClawTrophy, CLAW_CRANE_TROPHY_BS_PRIZE_WON_ALL)
				SET_BIT(iClawTrophy, CLAW_CRANE_TROPHY_BS_PRIZE_WON_ALL)
				SET_ARCADE_CLAW_CRANE_TROPHY_COMPLETE(TRUE)
				ARCADE_CABINET_FLOW_NOTIFICATIONS_SET_SHOW_TROPHY_HELP(ARCADE_GAME_TROPHY_CLAW_CRANE_UNLOCK_ALL_PLUSH, FALSE)
				PRINTLN("[AM_ARCADE_CLAW_CRANE] - SET_CLAW_CRANE_TROPHY_STATS CLAW_CRANE_TROPHY_BS_PRIZE_WON_ALL TRUE")
			ELSE
				PRINTLN("[AM_ARCADE_CLAW_CRANE] - SET_CLAW_CRANE_TROPHY_STATS CLAW_CRANE_TROPHY_BS_PRIZE_WON_ALL TRUE already won this trophy")
			ENDIF
		ELSE
			PRINTLN("[AM_ARCADE_CLAW_CRANE] - SET_CLAW_CRANE_TROPHY_STATS number of prize won: ", iNumPrize)
		ENDIF
		
		SET_MP_INT_CHARACTER_STAT(MP_STAT_CH_ARC_CAB_CLAW_TROPHY, iClawTrophy)
	ENDIF	
ENDPROC

FUNC BOOL IS_CLAW_ANIM_MOVING_UP()

	STRING sAnimClipName = GET_CLAW_ANIN_CLIP_NAMES(ARCADE_CABINET_ANIM_CLIP_CLAW_CRANE_PRESS_GRAB)
	STRING sAnimDic
	IF !IS_PLAYER_FEMALE()
		sAnimDic = "anim_heist@arcade@claw@male@"
	ELSE
		sAnimDic = "anim_heist@arcade@claw@female@"
	ENDIF

	FLOAT fExitAnimPhase = 0.486
	INT iLocalSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(serverBD.iClawSyncSceneID)
	
	IF NOT IS_STRING_NULL_OR_EMPTY(sAnimClipName)
		IF IS_ENTITY_PLAYING_ANIM(sData.clawObj[CLAW_SUB_CLAW], sAnimDic, sAnimClipName)	
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalSceneID)
				IF GET_SYNCHRONIZED_SCENE_PHASE(iLocalSceneID) >= fExitAnimPhase
					RETURN TRUE
				ENDIF
			ELSE
				RETURN TRUE
			ENDIF	
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_CLAW_ANIMATION_CLIP_FINISHED()
	FLOAT fExitAnimPhase = 0.96
	
	INT iLocalSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(serverBD.iClawSyncSceneID)
	IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalSceneID)
		IF GET_SYNCHRONIZED_SCENE_PHASE(iLocalSceneID) >= fExitAnimPhase
			PRINTLN("[AM_ARCADE_CLAW_CRANE] HAS_CLAW_ANIMATION_CLIP_FINISHED GET_SYNCHRONIZED_SCENE_PHASE true ")
			RETURN TRUE
		ENDIF
	ENDIF	

	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_PRIZE_ANIMATION_CLIP_FINISHED()
	FLOAT fExitAnimPhase = 0.96
	
	INT iLocalSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sData.iPrizeSyncSceneID)
	IF IS_ENTITY_PLAYING_ANIM(GET_COLLIDED_PRIZE_INDEX(FALSE), g_ArcadeCabinetManagerData.strArcCabinetAnimDic, "prop_plush_grab")
		IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalSceneID)
			IF GET_SYNCHRONIZED_SCENE_PHASE(iLocalSceneID) >= fExitAnimPhase
				PRINTLN("[AM_ARCADE_CLAW_CRANE] HAS_PRIZE_ANIMATION_CLIP_FINISHED GET_SYNCHRONIZED_SCENE_PHASE true ")
				RETURN TRUE
			ENDIF
		ENDIF	
	ELSE
		PRINTLN("[AM_ARCADE_CLAW_CRANE] HAS_PRIZE_ANIMATION_CLIP_FINISHED IS_ENTITY_PLAYING_ANIM false - true ")
		RETURN TRUE
	ENDIF	
	
	RETURN FALSE
ENDFUNC

PROC STOP_CLAW_CRANE_CLAW_SYNC_SCENE()
	INT iLocalSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(serverBD.iClawSyncSceneID)
	IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalSceneID)
		IF serverBD.iClawSyncSceneID != -1
			NETWORK_STOP_SYNCHRONISED_SCENE(serverBD.iClawSyncSceneID)
			serverBD.iClawSyncSceneID = -1
			PRINTLN("[AM_ARCADE_CLAW_CRANE] STOP_CLAW_CRANE_CLAW_SYNC_SCENE")
		ENDIF
	ELSE
		IF serverBD.iClawSyncSceneID != -1
			serverBD.iClawSyncSceneID = -1
			PRINTLN("[AM_ARCADE_CLAW_CRANE] STOP_CLAW_CRANE_CLAW_SYNC_SCENE - Forcing -1")
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL FREEZE_ALL_PRIZE_ENTITIES()
	INT i
	FOR i = 0 TO ENUM_TO_INT(CLAW_SUB_TOTAL) - 1
		IF DOES_ENTITY_EXIST(sData.clawObj[i])
			IF IS_THIS_ENTITY_A_PRIZE(sData.clawObj[i])
			AND NOT IS_ENTITY_ATTACHED(sData.clawObj[i])
				IF TAKE_CONTROL_OF_ENTITY(sData.clawObj[i])
					FREEZE_ENTITY_POSITION(sData.clawObj[i], TRUE)
				ELSE
					RETURN FALSE
				ENDIF
			ENDIF	
		ENDIF	
	ENDFOR
	RETURN TRUE
ENDFUNC

PROC STOP_CLAW_CRANE_PRIZE_SYNC_SCENE()
	INT iLocalSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sData.iPrizeSyncSceneID)
	IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalSceneID)
		FREEZE_ALL_PRIZE_ENTITIES()
		IF sData.iPrizeSyncSceneID != -1 
			NETWORK_STOP_SYNCHRONISED_SCENE(sData.iPrizeSyncSceneID)
			sData.iPrizeSyncSceneID = -1
			PRINTLN("[AM_ARCADE_CLAW_CRANE] STOP_CLAW_CRANE_PRIZE_SYNC_SCENE")
		ENDIF	
	ELSE
		IF sData.iPrizeSyncSceneID != -1
			sData.iPrizeSyncSceneID = -1
			PRINTLN("[AM_ARCADE_CLAW_CRANE] STOP_CLAW_CRANE_PRIZE_SYNC_SCENE forcing -1")
		ENDIF
		
		FREEZE_ALL_PRIZE_ENTITIES()
		PRINTLN("[AM_ARCADE_CLAW_CRANE] STOP_CLAW_CRANE_PRIZE_SYNC_SCENE already serverBD.iPrizeSyncSceneID = -1")
	ENDIF
ENDPROC

FUNC BOOL IS_CLAW_PRIZE_SYNCHRONIZED_SCENE_RUNNING()
	INT iLocalSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sData.iPrizeSyncSceneID)
	RETURN IS_SYNCHRONIZED_SCENE_RUNNING(iLocalSceneID)
ENDFUNC

/// PURPOSE:
///    Move claw in Z Axis 
///    bPrizeDrop = true if claw moving up and down to drop the prize in box for player
///    bPrizeDrop = false if claw is moving up and down to pick up the prize after player control press
PROC MOVE_CLAW_IN_Z_AXIS(BOOL bMoveUp, BOOL bPrizeDrop, BOOL bHostCleanUp = FALSE)
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("ARC_CLAW_MO_D")
		CLEAR_HELP()
	ENDIF

	sData.fMoveSoundVolume = 0
	
	// Up
	IF bMoveUp

		IF HAS_ARCADE_CABINET_ANIMATION_CLIP_FINISHED(ARCADE_CABINET_ANIM_CLIP_CLAW_CRANE_PRESS_GRAB)
			IF !bPrizeDrop
				IF HAS_CLAW_ANIMATION_CLIP_FINISHED()
				AND IS_BIT_SET(serverBD.iServerBS, SERVER_BD_BS_START_CLAW_PRESS_DOWN_ANIM)	
					IF TAKE_CONTROL_OF_ENTITY(sData.clawObj[CLAW_SUB_CLAW_COLLISION])
						IF NOT IS_ENTITY_ATTACHED(sData.clawObj[CLAW_SUB_CLAW_COLLISION])
							FREEZE_ENTITY_POSITION(sData.clawObj[CLAW_SUB_CLAW_COLLISION], TRUE)
						ENDIF	
					ENDIF	

					IF TAKE_CONTROL_OF_ENTITY(sData.clawObj[CLAW_SUB_RAIL_X])
						FREEZE_ENTITY_POSITION(sData.clawObj[CLAW_SUB_RAIL_X], TRUE)
					ENDIF	
					
					IF !bHostCleanUp
						IF !IS_ARCADE_CLAW_CRANE_BIT_SET(LOCAL_BS_ATTACHED_PRIZE_TO_CLAW)
							STOP_CURRENT_ARCADE_CABINET_SYNCHED_SCENE()
							CLEANUP_CLAW_CAMERA()
							sData.sArcadeCabinetAnimEvent.bHoldLastFrame = FALSE
							sData.sArcadeCabinetAnimEvent.bLoopAnim = FALSE
							PLAY_ARCADE_CABINET_ANIMATION(sData.sArcadeCabinetAnimEvent, ARCADE_CABINET_ANIM_CLIP_CLAW_CRANE_MISS)
						ENDIF
						
						IF NOT IS_ARCADE_CLAW_CRANE_BIT_SET(LOCAL_BS_STOP_PRIZE_ANIM)
							STOP_CLAW_CRANE_PRIZE_SYNC_SCENE()
							SET_ARCADE_CLAW_CRANE_BIT(LOCAL_BS_STOP_PRIZE_ANIM)
						ENDIF	
						
						SET_CLAW_CRANE_GAME_STATE(CLAW_GAME_MOVE_LEFT_STATE)
					ELSE
						SET_CLAW_CRANE_SERVER_GAME_STATE(CLAW_GAME_MOVE_LEFT_STATE)
					ENDIF
				ELSE
					PRINTLN("[AM_ARCADE_CLAW_CRANE] MOVE_CLAW_IN_Z_AXIS IS_SYNCHRONIZED_SCENE_RUNNING false")
					IF HAS_CLAW_ANIMATION_CLIP_FINISHED()
					AND IS_BIT_SET(serverBD.iServerBS, SERVER_BD_BS_START_CLAW_PRESS_DOWN_ANIM)
						IF NOT IS_ARCADE_CLAW_CRANE_BIT_SET(LOCAL_BS_STOP_CLAW_ANIM)
							STOP_CLAW_CRANE_PROP_ANIM(FALSE, FALSE, <<0,0,0>>, 0.0)
							SET_ARCADE_CLAW_CRANE_BIT(LOCAL_BS_STOP_CLAW_ANIM)
						ENDIF	
					ENDIF	
				ENDIF		
			ENDIF
			
			CLEAR_ARCADE_CLAW_CRANE_BIT(LOCAL_BS_GRAB_CLAW_MOVING_DOWN_COORDS)
		ELSE
			PRINTLN("[AM_ARCADE_CLAW_CRANE] MOVE_CLAW_IN_Z_AXIS HAS_ARCADE_CABINET_ANIMATION_CLIP_FINISHED false")
		ENDIF
	// Down
	ELSE
		IF NOT IS_ARCADE_CLAW_CRANE_BIT_SET(LOCAL_BS_GRAB_CLAW_MOVING_DOWN_COORDS)
			sData.vMovingDownCoord = GET_ENTITY_COORDS(sData.clawObj[CLAW_SUB_CLAW])
			SET_ARCADE_CLAW_CRANE_BIT(LOCAL_BS_GRAB_CLAW_MOVING_DOWN_COORDS)
		ENDIF

		IF !bPrizeDrop
			IF IS_PLAYER_PLAYING_THIS_ARCADE_CABINET_ANIMATION_CLIP(ARCADE_CABINET_ANIM_CLIP_CLAW_CRANE_PRESS_GRAB) AND HAS_ARCADE_CABINET_ANIMATION_CLIP_FINISHED(ARCADE_CABINET_ANIM_CLIP_CLAW_CRANE_PRESS_GRAB)
				IF sData.sArcadeCabinetAnimEvent.ePreviousAnimClip != ARCADE_CABINET_ANIM_CLIP_CLAW_CRANE_WIN_LOOP
					sData.sArcadeCabinetAnimEvent.bLoopAnim = TRUE
					sData.sArcadeCabinetAnimEvent.bHoldLastFrame = FALSE
					PLAY_ARCADE_CABINET_ANIMATION(sData.sArcadeCabinetAnimEvent, ARCADE_CABINET_ANIM_CLIP_CLAW_CRANE_WIN_LOOP)
				ENDIF	
			ENDIF
			
			IF IS_CLAW_ANIM_MOVING_UP()
			OR IS_ARCADE_CLAW_CRANE_BIT_SET(LOCAL_BS_CLAW_HIT_PRIZE)
				PRINTLN("[AM_ARCADE_CLAW_CRANE] MOVE_CLAW_IN_Z_AXIS IS_CLAW_ANIM_MOVING_UP true")
				IF IS_ARCADE_CLAW_CRANE_BIT_SET(LOCAL_BS_CLAW_HIT_PRIZE)
					STOP_CLAW_CRANE_PRIZE_SYNC_SCENE()
					SET_CLAW_CRANE_GAME_STATE(CLAW_GAME_ATTACH_PRIZE_STATE)
				ELSE
					IF !IS_ARCADE_CLAW_CRANE_BIT_SET(LOCAL_BS_PLAY_ASCEND_SOUND)
						PLAY_SOUND_FROM_COORD(-1, "ascend", GET_ENTITY_COORDS(sData.cabinetObj), "dlc_ch_claw_crane_sounds", TRUE, 10)
						SET_ARCADE_CLAW_CRANE_BIT(LOCAL_BS_PLAY_ASCEND_SOUND)
					ENDIF	
				ENDIF	
				
				IF !IS_ARCADE_CLAW_CRANE_BIT_SET(LOCAL_BS_PLAY_IMPACT_SOUND)
					PLAY_SOUND_FROM_COORD(-1, "descend_impact", GET_ENTITY_COORDS(sData.cabinetObj), "dlc_ch_claw_crane_sounds", TRUE, 10)
					SET_ARCADE_CLAW_CRANE_BIT(LOCAL_BS_PLAY_IMPACT_SOUND)
				ENDIF	
			ENDIF	
			
			IF HAS_CLAW_ANIMATION_CLIP_FINISHED()
			AND !IS_ARCADE_CLAW_CRANE_BIT_SET(LOCAL_BS_CLAW_HIT_PRIZE)
			AND IS_BIT_SET(serverBD.iServerBS, SERVER_BD_BS_START_CLAW_PRESS_DOWN_ANIM)
				SET_CLAW_CRANE_GAME_STATE(CLAW_GAME_MOVE_UP_TO_PICK_UP_STATE)
			ENDIF
		ELSE
			IF DOES_ENTITY_EXIST(GET_COLLIDED_PRIZE_INDEX(bHostCleanUp))
				IF IS_ENTITY_ATTACHED_TO_ENTITY(GET_COLLIDED_PRIZE_INDEX(bHostCleanUp), sData.clawObj[CLAW_SUB_CLAW_COLLISION])
					SET_ENTITY_COLLISION(GET_COLLIDED_PRIZE_INDEX(bHostCleanUp), FALSE)
					DETACH_ENTITY(GET_COLLIDED_PRIZE_INDEX(bHostCleanUp), FALSE)
					//DETACH_ENTITY(sData.clawObj[CLAW_SUB_CLAW_COLLISION], FALSE)
					SET_ARCADE_CLAW_CRANE_BIT(LOCAL_BS_DETACH_BEFORE_ANIMATION)
				ELSE	
					IF IS_ARCADE_CLAW_CRANE_BIT_SET(LOCAL_BS_DETACH_BEFORE_ANIMATION)
						IF !bHostCleanUp
							CLEANUP_CLAW_CAMERA()
							sData.sArcadeCabinetAnimEvent.bHoldLastFrame = FALSE
							sData.sArcadeCabinetAnimEvent.bLoopAnim = FALSE
							IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
								IF !IS_ARCADE_CLAW_CRANE_BIT_SET(LOCAL_BS_SWITCH_TO_TPP_CAMERA)
									SET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT, CAM_VIEW_MODE_THIRD_PERSON_MEDIUM)
									SET_ARCADE_CLAW_CRANE_BIT(LOCAL_BS_SWITCH_TO_TPP_CAMERA)
								ENDIF	
							ENDIF
							PLAY_ARCADE_CABINET_ANIMATION(sData.sArcadeCabinetAnimEvent, ARCADE_CABINET_ANIM_CLIP_CLAW_CRANE_PRIZE_WIN)
						ELSE
							FREEZE_ENTITY_POSITION(GET_COLLIDED_PRIZE_INDEX(bHostCleanUp), TRUE)
							SET_ENTITY_VISIBLE(GET_COLLIDED_PRIZE_INDEX(bHostCleanUp), FALSE)
							SET_ENTITY_COORDS_NO_OFFSET(GET_COLLIDED_PRIZE_INDEX(bHostCleanUp), serverBD.vPrizeInitialCoords)
							SET_ENTITY_HEADING(GET_COLLIDED_PRIZE_INDEX(bHostCleanUp), serverBD.fPrizeInitialHeading)
						ENDIF
						
						PLAY_SOUND_FROM_COORD(-1, "release_toy", GET_ENTITY_COORDS(sData.cabinetObj), "dlc_ch_claw_crane_sounds", TRUE, 10)
						
						IF !bHostCleanUp
							IF IS_THIS_RARE_PRIZE(GET_COLLIDED_PRIZE_INDEX(FALSE))
								sData.sArcadeCabinetAIContext.stContext = "CLAW_RAREWIN"
								PLAY_ARCADE_CABINET_AI_CONTEXT(sData.sArcadeCabinetAIContext, TRUE)
								PLAY_SOUND_FROM_COORD(-1, "rare_win", GET_ENTITY_COORDS(sData.cabinetObj), "dlc_ch_claw_crane_sounds", TRUE, 10)
							ELSE
								sData.sArcadeCabinetAIContext.stContext = "CLAW_WIN"
								PLAY_ARCADE_CABINET_AI_CONTEXT(sData.sArcadeCabinetAIContext, TRUE)
								PLAY_SOUND_FROM_COORD(-1, "win", GET_ENTITY_COORDS(sData.cabinetObj), "dlc_ch_claw_crane_sounds", TRUE, 10)
							ENDIF
						
							SET_CLAW_CRANE_TROPHY_STATS()

						ENDIF
						
						CLEAR_ARCADE_CLAW_CRANE_BIT(LOCAL_BS_DETACH_BEFORE_ANIMATION)
					ELSE	
						sData.sArcadeCabinetAIContext.stContext = "CLAW_FAIL"
						PLAY_SOUND_FROM_COORD(-1, "lose", GET_ENTITY_COORDS(sData.cabinetObj), "dlc_ch_claw_crane_sounds", TRUE, 10)
						PLAY_ARCADE_CABINET_AI_CONTEXT(sData.sArcadeCabinetAIContext, TRUE)
					ENDIF
				ENDIF	
			ELSE
				PLAY_SOUND_FROM_COORD(-1, "lose", GET_ENTITY_COORDS(sData.cabinetObj), "dlc_ch_claw_crane_sounds", TRUE, 10)
				sData.sArcadeCabinetAIContext.stContext = "CLAW_FAIL"
				PLAY_ARCADE_CABINET_AI_CONTEXT(sData.sArcadeCabinetAIContext, TRUE)
			ENDIF
			
			IF !bHostCleanUp
			
				NEXT_RP_ADDITION_SHOW_RANKBAR()
				GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD, PLAYER_PED_ID(), "CLAW GAME RP5", XPTYPE_AWARDS, XPCATEGORY_RP_CLAW_CRANE, 5)
			
				IF IS_ARCADE_CLAW_CRANE_BIT_SET(LOCAL_BS_ATTACHED_PRIZE_TO_CLAW)	
					IF !IS_ARCADE_CLAW_CRANE_BIT_SET(LOCAL_BS_DETACH_BEFORE_ANIMATION)
						SET_CLAW_CRANE_GAME_STATE(CLAW_GAME_DROP_PRIZE_CLEANUP_STATE)
					ENDIF	
				ELSE
					SET_CLAW_CRANE_GAME_STATE(CLAW_GAME_CLEANUP_STATE)
				ENDIF
			ELSE
				SET_CLAW_CRANE_SERVER_GAME_STATE(CLAW_GAME_CLEANUP_STATE)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC INT GET_ATTACHED_PRIZE_ARRAY_INDEX(ENTITY_INDEX prizeToCheck)
	IF DOES_ENTITY_EXIST(prizeToCheck)
		INT i
		FOR i = 0 TO ENUM_TO_INT(CLAW_SUB_TOTAL) - 1
			IF GET_OBJECT_INDEX_FROM_ENTITY_INDEX(prizeToCheck) = sData.clawObj[i]
				RETURN i
			ENDIF
		ENDFOR
	ENDIF
	RETURN -1
ENDFUNC

FUNC VECTOR GET_PRIZE_GRAB_OFFSET(ENTITY_INDEX prizeToCheck)
	IF DOES_ENTITY_EXIST(prizeToCheck)
	AND NOT IS_ENTITY_DEAD(prizeToCheck)
		MODEL_NAMES prizeModel = GET_ENTITY_MODEL(prizeToCheck)
		IF prizeModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_princess_robo_Plush_07a"))
			RETURN <<0.0, 0.00628, -0.23194>>
		ELIF prizeModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_shiny_wasabi_Plush_08a"))
			RETURN <<0.0, -0.03463, -0.26425>>	
		ELIF prizeModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_master_09a"))
			RETURN <<0.0, -0.00726, -0.26668>>	
		ELIF prizeModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_arcade_claw_plush_01a"))
			RETURN <<0.0, 0.01904, -0.26626>>
		ELIF prizeModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_arcade_claw_plush_02a"))
			RETURN <<0.0, 0.01785, -0.26389>>			
		ELIF prizeModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_arcade_claw_plush_03a"))
			RETURN <<0.0, 0.0248, -0.24472>>			
		ELIF prizeModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_arcade_claw_plush_04a"))
			RETURN <<0.0, 0.02957,-0.25813>>			
		ELIF prizeModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_arcade_claw_plush_05a"))
			RETURN <<0.0, -0.005, -0.27151>>			
		ELIF prizeModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_arcade_claw_plush_06a"))
			RETURN <<0.0, 0.02655, -0.24757>>			
		ENDIF
	ENDIF 
	RETURN <<0,0,0>>
ENDFUNC 

FUNC INT GET_PRIZE_WIN_RATE(ENTITY_INDEX prizeToCheck)
	IF DOES_ENTITY_EXIST(prizeToCheck)
	AND NOT IS_ENTITY_DEAD(prizeToCheck)
		MODEL_NAMES prizeModel = GET_ENTITY_MODEL(prizeToCheck)
		IF prizeModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_princess_robo_Plush_07a"))
			RETURN g_sMptunables.iARCADE_CLAW_WIN_RATE_PRINCESS_PLUSH
		ELIF prizeModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_shiny_wasabi_Plush_08a"))
			RETURN g_sMptunables.iARCADE_CLAW_WIN_RATE_WASABI_PLUSH
		ELIF prizeModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_master_09a"))
			RETURN g_sMptunables.iARCADE_CLAW_WIN_RATE_MASTER_PLUSH
		ELIF prizeModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_arcade_claw_plush_01a"))
		OR prizeModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_arcade_claw_plush_02a"))		
		OR prizeModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_arcade_claw_plush_03a"))			
		OR prizeModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_arcade_claw_plush_04a"))		
		OR prizeModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_arcade_claw_plush_05a"))		
		OR prizeModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_arcade_claw_plush_06a"))	
			RETURN g_sMptunables.iARCADE_CLAW_WIN_RATE_COMMON_PLUSH
		ENDIF
	ENDIF 
	RETURN -1
ENDFUNC

/// PURPOSE:
///    Process claw collision shape test to detect collision between claw and prize objects
PROC PROCESS_CLAW_COLLISION_SHAPETEST()

	IF DOES_ENTITY_EXIST(sData.clawObj[CLAW_SUB_CLAW_COLLISION])
	AND NOT IS_ENTITY_DEAD(sData.clawObj[CLAW_SUB_CLAW_COLLISION])

		IF TAKE_CONTROL_OF_ENTITY(sData.clawObj[CLAW_SUB_CLAW_COLLISION])
			SWITCH sData.iClawCollisionShapeTest
		        CASE CLAW_SHAPETEST_INIT
		           
					// See if we can collide with anything around drone
					FLOAT fRadius 
					VECTOR vStartCoord
					
					fRadius = GET_MODEL_HEIGHT(GET_ENTITY_MODEL(sData.clawObj[CLAW_SUB_CLAW_COLLISION])) / 3

					//vStartCoord = GET_ENTITY_COORDS(sData.clawObj[CLAW_SUB_CLAW])
					
					vStartCoord = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sData.clawObj[CLAW_SUB_CLAW_COLLISION], <<0,0, -0.2>>)
					
					sData.clawCollisionShapeTest = START_SHAPE_TEST_CAPSULE(vStartCoord, vStartCoord, fRadius, SCRIPT_INCLUDE_OBJECT, sData.clawObj[CLAW_SUB_CLAW_COLLISION])
					
					#IF IS_DEBUG_BUILD
					//	DRAW_DEBUG_SPHERE(vStartCoord, fRadius, 0, 255, 0, 255)
					#ENDIF
					
					IF NATIVE_TO_INT(sData.clawCollisionShapeTest) != 0
						sData.iClawCollisionShapeTest 		= CLAW_SHAPETEST_PROCESS
					//	PRINTLN("[AM_ARCADE_CLAW_CRANE] - PROCESS_CLAW_COLLISION_SHAPETEST sData.iClawCollisionShapeTest INIT to PROCESS ")
					ENDIF	
						
		        BREAK
		        CASE CLAW_SHAPETEST_PROCESS
		        
		            INT iHitSomething
		            VECTOR vNormalAtPosHit
					ENTITY_INDEX collidedIndex 
		           	SHAPETEST_STATUS shapetestStatus 
					shapetestStatus = GET_SHAPE_TEST_RESULT(sData.clawCollisionShapeTest, iHitSomething, sData.vPrizeCollideCoord, vNormalAtPosHit, collidedIndex)
					
					// result is ready
		            IF shapetestStatus = SHAPETEST_STATUS_RESULTS_READY
		                #IF IS_DEBUG_BUILD
						PRINTLN("[AM_ARCADE_CLAW_CRANE] - PROCESS_CLAW_COLLISION_SHAPETEST - We got shapetest results!")
						#ENDIF
						
						IF iHitSomething = 0
							
							sData.iClawCollisionHitSomthing = CLAW_COLLISION_CHECK_NOT_HIT_ANYTHING
							CLEAR_ARCADE_CLAW_CRANE_BIT(LOCAL_BS_CLAW_HIT_PRIZE)
							
							//PRINTLN("[AM_ARCADE_CLAW_CRANE] - PROCESS_CLAW_COLLISION_SHAPETEST CLAW_COLLISION_CHECK_NOT_HIT_ANYTHING ")
							sData.vPrizeCollideCoord = <<0,0,0>> 
							#IF IS_DEBUG_BUILD
						//	DRAW_DEBUG_TEXT_2D("Left area hit somthing = FALSE", <<0.1,0.1,0.5>>,0,255,0)
						//	DRAW_DEBUG_TEXT_ABOVE_ENTITY(sData.clawObj[CLAW_SUB_CLAW], "Drone collision hit somthing = FALSE", 1,0,255,0)		
							#ENDIF
							
		                ELSE
							IF DOES_ENTITY_EXIST(collidedIndex)
								IF IS_ENTITY_AN_OBJECT(collidedIndex)
									IF !IS_ENTITY_DEAD(collidedIndex)
										IF IS_THIS_ENTITY_A_PRIZE(collidedIndex)
										AND IS_ENTITY_VISIBLE(collidedIndex)
											VECTOR vPrizeOffsetCoords, vClawOffsetCoords 
											FLOAT fDistance
											playerBD[NATIVE_TO_INT(PLAYER_ID())].collidedPrize = collidedIndex
											
											vPrizeOffsetCoords = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(GET_COLLIDED_PRIZE_INDEX(FALSE)), GET_ENTITY_HEADING(GET_COLLIDED_PRIZE_INDEX(FALSE)), <<0.0, 0.000, 0.000>>)
											vClawOffsetCoords = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(sData.clawObj[CLAW_SUB_CLAW_COLLISION]), GET_ENTITY_HEADING(sData.clawObj[CLAW_SUB_CLAW_COLLISION]), GET_PRIZE_GRAB_OFFSET(collidedIndex))
											fDistance = GET_DISTANCE_BETWEEN_COORDS(vPrizeOffsetCoords, vClawOffsetCoords)

											playerBD[NATIVE_TO_INT(PLAYER_ID())].iAttachedPrizeIndex = GET_ATTACHED_PRIZE_ARRAY_INDEX(collidedIndex)
											
											IF sData.iPrizeSyncSceneID = -1
												IF NOT IS_ARCADE_CLAW_CRANE_BIT_SET(LOCAL_BS_CLAW_HIT_PRIZE)
													START_PRIZE_SYNC_SCENE(ARCADE_CABINET_ANIM_CLIP_CLAW_CRANE_PRESS_GRAB)
												ENDIF	
											ENDIF
											
											IF fDistance < 0.035
											#IF IS_DEBUG_BUILD
											OR sClawCraneDebugData.bForceWin
											#ENDIF
												#IF IS_DEBUG_BUILD
												IF sClawCraneDebugData.bDisplayDistanceInfo
												DRAW_DEBUG_TEXT_2D("Within safe distance ", <<0.5,0.5,0.5>>,0 , 255, 0)
												ENDIF
												#ENDIF
											
												IF NOT IS_ARCADE_CLAW_CRANE_BIT_SET(LOCAL_BS_GET_RAN_NUM_FOR_ATTACHMENT)
													sData.iRandomWinRate = GET_RANDOM_INT_IN_RANGE(0, GET_PRIZE_WIN_RATE(collidedIndex))
													SET_ARCADE_CLAW_CRANE_BIT(LOCAL_BS_GET_RAN_NUM_FOR_ATTACHMENT)
												ENDIF	
												
												IF sData.iRandomWinRate = 0
												#IF IS_DEBUG_BUILD
												OR sClawCraneDebugData.bRemoveRanRate
												OR sClawCraneDebugData.bForceWin
												#ENDIF
													IF NOT IS_ARCADE_CLAW_CRANE_BIT_SET(LOCAL_BS_STOP_PRIZE_ANIM)
														STOP_CLAW_CRANE_PRIZE_SYNC_SCENE()
														SET_ARCADE_CLAW_CRANE_BIT(LOCAL_BS_STOP_PRIZE_ANIM)
													ENDIF
													SET_ARCADE_CLAW_CRANE_BIT(LOCAL_BS_CLAW_HIT_PRIZE)
													#IF IS_DEBUG_BUILD
													PRINTLN("[AM_ARCADE_CLAW_CRANE] - PROCESS_CLAW_COLLISION_SHAPETEST prize collision coords: ", sData.vPrizeCollideCoord, " fDistance: ", fDistance
															, " force win ", sClawCraneDebugData.bForceWin)
													#ENDIF	
												ELSE
													PRINTLN("[AM_ARCADE_CLAW_CRANE] - PROCESS_CLAW_COLLISION_SHAPETEST iRandomNum: ", sData.iRandomWinRate)
												ENDIF
											ELSE	
												#IF IS_DEBUG_BUILD
												IF sClawCraneDebugData.bDisplayDistanceInfo
													DRAW_DEBUG_TEXT_2D("Claw is far ", <<0.5,0.5,0.5>>, 255, 0, 0)
												ENDIF	
												PRINTLN("[AM_ARCADE_CLAW_CRANE] - PROCESS_CLAW_COLLISION_SHAPETEST prize is far from claw fDistance: ", fDistance)
												#ENDIF
											ENDIF
											SET_CONTROL_SHAKE(PLAYER_CONTROL, 300, 50)
										ELSE
											CLEAR_ARCADE_CLAW_CRANE_BIT(LOCAL_BS_CLAW_HIT_PRIZE)
											IF GET_ENTITY_MODEL(collidedIndex) != DUMMY_MODEL_FOR_SCRIPT
												PRINTLN("[AM_ARCADE_CLAW_CRANE] - PROCESS_CLAW_COLLISION_SHAPETEST we are hitting: " , GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(collidedIndex)))
											ENDIF
										ENDIF	
									ELSE
										CLEAR_ARCADE_CLAW_CRANE_BIT(LOCAL_BS_CLAW_HIT_PRIZE)
										PRINTLN("[AM_ARCADE_CLAW_CRANE] - PROCESS_CLAW_COLLISION_SHAPETEST we are hitting dead entity")
									ENDIF
								ELSE
									CLEAR_ARCADE_CLAW_CRANE_BIT(LOCAL_BS_CLAW_HIT_PRIZE)
								ENDIF	
							ELSE	
								CLEAR_ARCADE_CLAW_CRANE_BIT(LOCAL_BS_CLAW_HIT_PRIZE)
							ENDIF	
							
		                    sData.iClawCollisionHitSomthing = CLAW_COLLISION_CHECK_HIT_SOMETHING
							
							#IF IS_DEBUG_BUILD
								//DRAW_DEBUG_TEXT_ABOVE_ENTITY(sData.clawObj[CLAW_SUB_CLAW_COLLISION], "Drone collision hit somthing = TRUE", 1)
								PRINTLN("[AM_ARCADE_CLAW_CRANE] - PROCESS_CLAW_COLLISION_SHAPETEST  CLAW_COLLISION_CHECK_HIT_SOMETHING ")
								//DRAW_DEBUG_TEXT_2D("Left area hit somthing = TRUE", <<0.1,0.1,0.5>>,255,0,0)
							#ENDIF
							sData.clawCollisionShapeTest = INT_TO_NATIVE(SHAPETEST_INDEX, 0)
							sData.iClawCollisionShapeTest 	= CLAW_SHAPETEST_INIT  
		                ENDIF
						 
					ELIF shapetestStatus	= SHAPETEST_STATUS_NONEXISTENT
						sData.iClawCollisionShapeTest 	= CLAW_SHAPETEST_INIT    
		            ENDIF

		        BREAK
		    ENDSWITCH
		ENDIF	
    ENDIF
ENDPROC

FUNC BOOL UNFREEZE_AND_ENABLE_ALL_PRIZE_ENTITIES()
	INT i
	FOR i = 0 TO ENUM_TO_INT(CLAW_SUB_TOTAL) - 1
		IF DOES_ENTITY_EXIST(sData.clawObj[i])
			IF IS_THIS_ENTITY_A_PRIZE(sData.clawObj[i])
			AND GET_ENTITY_FROM_PED_OR_VEHICLE(sData.clawObj[i]) !=  GET_COLLIDED_PRIZE_INDEX(FALSE)
				IF TAKE_CONTROL_OF_ENTITY(sData.clawObj[i])
					SET_ENTITY_COLLISION(sData.clawObj[i], TRUE)
					FREEZE_ENTITY_POSITION(sData.clawObj[i], FALSE)
				ELSE
					RETURN FALSE
				ENDIF
			ENDIF	
		ENDIF	
	ENDFOR
	RETURN TRUE
ENDFUNC

PROC CREATE_CLAW_CRANE_CAMERA()
	IF NOT DOES_CAM_EXIST(sData.clawCamera)
		sData.clawCamera = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
		SET_CAM_ACTIVE(sData.clawCamera, TRUE)
		SHAKE_CAM(sData.clawCamera, "HAND_SHAKE", 0.1)
		
		INT iDuration = 0
		IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
			SET_CAM_PARAMS(sData.clawCamera, GET_GAMEPLAY_CAM_COORD(), GET_GAMEPLAY_CAM_ROT(), GET_GAMEPLAY_CAM_FOV())
			iDuration = 1000
		ENDIF
		
		VECTOR vCameraCoord = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(sData.cabinetObj), GET_ENTITY_HEADING(sData.cabinetObj), << -0.000732422, -0.862213, 1.571 >>)
		SET_CAM_PARAMS(sData.clawCamera, vCameraCoord, <<-18.6932, 0.0000, GET_ENTITY_HEADING(sData.cabinetObj)>>, 50.6788, iDuration)
		
		RENDER_SCRIPT_CAMS(TRUE, FALSE)
	ENDIF
ENDPROC

FUNC BOOL IS_ENTITY_VALID_FOR_ATTACHMENT(ENTITY_INDEX entityToCheck)
	IF !IS_ENTITY_ALIVE(entityToCheck)
		RETURN FALSE
	ENDIF
	
	IF !DOES_ENTITY_HAVE_PHYSICS(entityToCheck)
		RETURN FALSE
	ENDIF	
	
	IF !DOES_ENTITY_HAVE_DRAWABLE(entityToCheck)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Initialise claw crane game state
PROC INITIALISE_CLAW_CRANE_GAME_STATE(BOOL bHostCleanUp = FALSE)
	IF !bHostCleanUp
		
		BOOL bRenovateActive = FALSE
		IF IS_PLAYER_ARCADE_PROPERTY_RENOVATION_ACTIVE(PLAYER_ID())
		OR (GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner != INVALID_PLAYER_INDEX() 
			AND IS_PLAYER_ARCADE_PROPERTY_RENOVATION_ACTIVE(GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner))
			bRenovateActive = TRUE
			PRINTLN("[AM_ARCADE_CLAW_CRANE] - INITIALISE_CLAW_CRANE_GAME_STATE bRenovateActive = TRUE")
		ENDIF
		
		IF TAKE_CONTROL_OF_ENTITY(sData.clawObj[CLAW_SUB_CLAW_COLLISION])
		AND TAKE_CONTROL_OF_ENTITY(sData.clawObj[CLAW_SUB_CLAW])
		AND TAKE_CONTROL_OF_ENTITY(sData.clawObj[CLAW_SUB_RAIL_X])
		AND TAKE_CONTROL_OF_ENTITY(sData.clawObj[CLAW_SUB_RAIL_Y])
			INT iDisplaySlot = GET_ARCADE_CABINET_LOCATE_DISPLAY_SLOT_PLAYER_IS_IN(PLAYER_ID())
			sData.sArcadeCabinetAIContext.stContext = "CLAW_START_PLAY"
			sData.sArcadeCabinetAIContext.eArcadeCabinet = ARCADE_CABINET_CH_CLAW_CRANE
			sData.sArcadeCabinetAIContext.sCabinetSlot = INT_TO_ENUM(ARCADE_CAB_MANAGER_SLOT, iDisplaySlot)
			
			IF IS_ENTITY_VALID_FOR_ATTACHMENT(sData.clawObj[CLAW_SUB_CLAW])
			AND IS_ENTITY_VALID_FOR_ATTACHMENT(sData.clawObj[CLAW_SUB_CLAW_COLLISION])
			AND !bRenovateActive
			
				INT iBoneIndex = GET_ENTITY_BONE_INDEX_BY_NAME(sData.clawObj[CLAW_SUB_CLAW], "claw_02")
				ATTACH_ENTITY_BONE_TO_ENTITY_BONE(sData.clawObj[CLAW_SUB_CLAW_COLLISION], sData.clawObj[CLAW_SUB_CLAW], iBoneIndex, iBoneIndex, TRUE)
				SET_ENTITY_COLLISION(sData.clawObj[CLAW_SUB_CLAW_COLLISION], TRUE)
				SET_ENTITY_COLLISION(sData.clawObj[CLAW_SUB_CLAW], FALSE)
				SET_ENTITY_VISIBLE(sData.clawObj[CLAW_SUB_CLAW_COLLISION], FALSE)
			ELSE
				SET_CLAW_CRANE_GAME_STATE(CLAW_GAME_CLEANUP_STATE)
				PRINTLN("[AM_ARCADE_CLAW_CRANE] - INITIALISE_CLAW_CRANE_GAME_STATE claw or claw collision entities doesn't exist")
			ENDIF
			
			FREEZE_ENTITY_POSITION(sData.clawObj[CLAW_SUB_RAIL_X], FALSE)
			FREEZE_ENTITY_POSITION(sData.clawObj[CLAW_SUB_RAIL_Y], FALSE)
			
			CREATE_CLAW_CRANE_CAMERA()
			PLAY_ARCADE_CABINET_AI_CONTEXT(sData.sArcadeCabinetAIContext, TRUE)
			START_NET_TIMER(sData.tPlayDurationTimer)
			SET_CLAW_CRANE_GAME_STATE(CLAW_GAME_MOVE_FORWARD_STATE)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Check if pick up prize control is pressed 
FUNC BOOL HAS_PICK_UP_CONTROL_PRESSED()
	IF (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
	OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT))
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC	

/// PURPOSE:
///    Claw is in position and waiting for player control to go down and pick up the prize
PROC MAINTAIN_WAIT_FOR_PLAYER_CONTROL_TO_DROP_CLAW()
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("ARC_CLAW_MO_R")
		CLEAR_HELP()
	ENDIF

	IF SHOULD_PAUSE_CLAW_CRANE()
		EXIT
	ENDIF	

	IF !IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("ARC_CLAW_MO_D")
		PRINT_HELP_FOREVER("ARC_CLAW_MO_D")
	ENDIF

	IF HAS_PICK_UP_CONTROL_PRESSED()
		sData.sArcadeCabinetAnimEvent.bHoldLastFrame = TRUE
		sData.sArcadeCabinetAnimEvent.bLoopAnim = FALSE
		sData.sArcadeCabinetAnimEvent.fBlendInDelta = SLOW_BLEND_IN
		sData.sArcadeCabinetAnimEvent.fBlendOutDelta = SLOW_BLEND_OUT
		PLAY_ARCADE_CABINET_ANIMATION(sData.sArcadeCabinetAnimEvent, ARCADE_CABINET_ANIM_CLIP_CLAW_CRANE_PRESS_GRAB)
		
		IF NOT IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iLocalBS, PLAYER_BD_BS_START_CLAW_PRESS_DOWN_ANIM)
			SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iLocalBS, PLAYER_BD_BS_START_CLAW_PRESS_DOWN_ANIM)
		ENDIF	
		
		PLAY_SOUND_FROM_COORD(-1, "descend", GET_ENTITY_COORDS(sData.cabinetObj), "dlc_ch_claw_crane_sounds", TRUE, 10)
		sData.sArcadeCabinetAIContext.stContext = "CLAW_DESCENDING_ARM"
		PLAY_ARCADE_CABINET_AI_CONTEXT(sData.sArcadeCabinetAIContext, TRUE)
		
		FREEZE_ENTITY_POSITION(sData.clawObj[CLAW_SUB_RAIL_X], TRUE)
		FREEZE_ENTITY_POSITION(sData.clawObj[CLAW_SUB_RAIL_Y], TRUE)
		
		SET_CLAW_CRANE_GAME_STATE(CLAW_GAME_MOVE_DOWN_TO_PICK_UP_STATE)
		
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("ARC_CLAW_MO_D")
			CLEAR_HELP()
		ENDIF
	ELSE
		IF IS_PLAYER_PLAYING_THIS_ARCADE_CABINET_ANIMATION_CLIP(ARCADE_CABINET_ANIM_CLIP_CLAW_CRANE_PRESS_RIGHT)
		AND HAS_ARCADE_CABINET_ANIMATION_CLIP_FINISHED(ARCADE_CABINET_ANIM_CLIP_CLAW_CRANE_PRESS_RIGHT)
			IF sData.sArcadeCabinetAnimEvent.ePreviousAnimClip != ARCADE_CABINET_ANIM_CLIP_IDLE
				sData.sArcadeCabinetAnimEvent.bHoldLastFrame = FALSE
				sData.sArcadeCabinetAnimEvent.bLoopAnim = TRUE
				PLAY_ARCADE_CABINET_ANIMATION(sData.sArcadeCabinetAnimEvent, ARCADE_CABINET_ANIM_CLIP_IDLE)
			ENDIF	
		ENDIF
	ENDIF
ENDPROC

PROC SEND_CLAW_CRANE_TELEMTRY()
	//STRING sPrizeName
	STRUCT_ARCADE_CABINET sArcadeCabinetTelemetry
	sArcadeCabinetTelemetry.reward = ARCADE_GAME_GET_TROPHY_ID_FOR_TELEMETRY(sData.trophyType)
	sArcadeCabinetTelemetry.gameType = HASH("ARCADE_CABINET_CH_CLAW_CRANE")
	sArcadeCabinetTelemetry.numPlayers = 1
	sArcadeCabinetTelemetry.timePlayed =  ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), sData.tPlayDurationTimer.Timer))
	
	IF HAS_PLAYER_RECIEVED_CLAW_CRANE_TROPHY(PLAYER_ID())
		sArcadeCabinetTelemetry.challenges = 1
	ENDIF
	
	IF PLAYER_ID() != INVALID_PLAYER_INDEX()
		IF IS_PLAYER_IN_AUTO_SHOP(PLAYER_ID())
			sArcadeCabinetTelemetry.location = HASH("SIMPLE_INTERIOR_TYPE_AUTO_SHOP")
		ELIF IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
			sArcadeCabinetTelemetry.location = HASH("SIMPLE_INTERIOR_TYPE_ARCADE")
		ELSE
			CDEBUG1LN(DEBUG_MINIGAME, "[AM_ARCADE_CLAW_CRANE] [SEND_CLAW_CRANE_TELEMTRY] not in auto shop or arcade, setting sRoadArcadeTelemetry.location = 0")
			sArcadeCabinetTelemetry.location = 0
		ENDIF
	ELSE
		CDEBUG1LN(DEBUG_MINIGAME, "[AM_ARCADE_CLAW_CRANE] [SEND_CLAW_CRANE_TELEMTRY] Invalid Player ID, setting sArcadeCabinetTelemetry.location = 0")
		sArcadeCabinetTelemetry.location = 0
	ENDIF
	
	PLAYSTATS_ARCADE_CABINET(sArcadeCabinetTelemetry)
	PRINTLN("[AM_ARCADE_CLAW_CRANE] SEND_CLAW_CRANE_TELEMTRY trophy: ", sData.trophyType, " timePlayed: ", sArcadeCabinetTelemetry.timePlayed)
	PRINTLN("[AM_ARCADE_CLAW_CRANE] SEND_CLAW_CRANE_TELEMTRY trophy: ", sData.trophyType, " timePlayed: ", sArcadeCabinetTelemetry.location)
ENDPROC


PROC MAINTAIN_CLAW_CRANE_PRIZE_DROP_CLEANUP()
	IF IS_PLAYER_PLAYING_THIS_ARCADE_CABINET_ANIMATION_CLIP(ARCADE_CABINET_ANIM_CLIP_CLAW_CRANE_PRIZE_WIN)
	AND HAS_ARCADE_CABINET_ANIMATION_CLIP_FINISHED(ARCADE_CABINET_ANIM_CLIP_CLAW_CRANE_PRIZE_WIN)
	AND HAS_ARCADE_CABINET_ANIMATION_CLIP_FINISHED(ARCADE_CABINET_ANIM_CLIP_CLAW_CRANE_PRIZE_WIN, TRUE, FALSE, TRUE)
	AND NOT IS_ARCADE_CLAW_CRANE_BIT_SET(LOCAL_BS_MOVE_BACK_PRIZE_TO_CABINET)
		IF NOT IS_ARCADE_CLAW_CRANE_BIT_SET(LOCAL_BS_MOVE_BACK_PRIZE_TO_CABINET)
			STOP_CLAW_CRANE_PRIZE_SYNC_SCENE()
			SET_ARCADE_CLAW_CRANE_BIT(LOCAL_BS_MOVE_BACK_PRIZE_TO_CABINET)
		ENDIF	
	ELIF  IS_ARCADE_CLAW_CRANE_BIT_SET(LOCAL_BS_MOVE_BACK_PRIZE_TO_CABINET)
		IF !ARE_VECTORS_ALMOST_EQUAL(GET_ENTITY_COORDS(GET_COLLIDED_PRIZE_INDEX(FALSE)), sData.vPrizeInitialCoords, 0.05)
			IF TAKE_CONTROL_OF_ENTITY(GET_COLLIDED_PRIZE_INDEX(FALSE))
				STOP_CURRENT_ARCADE_CABINET_PROP_SYNCHED_SCENE()
				IF IS_ENTITY_ATTACHED(GET_COLLIDED_PRIZE_INDEX(FALSE))
					DETACH_ENTITY(GET_COLLIDED_PRIZE_INDEX(FALSE))
				ENDIF	
				FREEZE_ENTITY_POSITION(GET_COLLIDED_PRIZE_INDEX(FALSE), TRUE)
				SET_ENTITY_VISIBLE(GET_COLLIDED_PRIZE_INDEX(FALSE), FALSE)
				SET_ENTITY_COORDS_NO_OFFSET(GET_COLLIDED_PRIZE_INDEX(FALSE), sData.vPrizeInitialCoords)
				SET_ENTITY_HEADING(GET_COLLIDED_PRIZE_INDEX(FALSE), sData.fPrizeInitialHeading)
				SET_ENTITY_VISIBLE(GET_COLLIDED_PRIZE_INDEX(FALSE), FALSE)
			ENDIF
		ELSE
			IF TAKE_CONTROL_OF_ENTITY(sData.clawObj[CLAW_SUB_CLAW])
			AND TAKE_CONTROL_OF_ENTITY(sData.clawObj[CLAW_SUB_CLAW_COLLISION])
				SET_ENTITY_COORDS_NO_OFFSET(sData.clawObj[CLAW_SUB_CLAW], GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sData.cabinetObj, <<CLAW_CRAN_MIN_X_AXIS, CLAW_CRAN_MIN_Y_AXIS, CLAW_CRAN_MAX_Z_AXIS>>))
				SET_PLAYER_IN_ARCADE_CABINET_LOCATE_TYPE(ARCADE_CABINET_INVALID)
				FREEZE_ALL_PRIZE_ENTITIES()
				SET_CLAW_CRANE_GAME_STATE(CLAW_GAME_CLEANUP_STATE)
			ENDIF		
		ENDIF
	ENDIF
ENDPROC

PROC SERVER_CLEANUP_PRIZE()
	IF DOES_ENTITY_EXIST(GET_COLLIDED_PRIZE_INDEX(TRUE))
		IF IS_ENTITY_ATTACHED_TO_ENTITY(GET_COLLIDED_PRIZE_INDEX(TRUE), sData.clawObj[CLAW_SUB_CLAW_COLLISION])
			IF TAKE_CONTROL_OF_ENTITY(GET_COLLIDED_PRIZE_INDEX(TRUE))
				SET_ENTITY_COLLISION(GET_COLLIDED_PRIZE_INDEX(TRUE), FALSE)
				IF IS_ENTITY_ATTACHED(GET_COLLIDED_PRIZE_INDEX(TRUE))
					DETACH_ENTITY(GET_COLLIDED_PRIZE_INDEX(TRUE), FALSE)
				ENDIF	
				FREEZE_ENTITY_POSITION(GET_COLLIDED_PRIZE_INDEX(TRUE), TRUE)
				SET_ENTITY_VISIBLE(GET_COLLIDED_PRIZE_INDEX(TRUE), FALSE)
				SET_ENTITY_COORDS_NO_OFFSET(GET_COLLIDED_PRIZE_INDEX(TRUE), serverBD.vPrizeInitialCoords)
				SET_ENTITY_HEADING(GET_COLLIDED_PRIZE_INDEX(TRUE), serverBD.fPrizeInitialHeading)
				PRINTLN("[AM_ARCADE_CLAW_CRANE] SERVER_CLEANUP_PRIZE")
			ENDIF	
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///     Maintain claw crane game cleanup state
PROC MAINTAIN_CLAW_CRANE_GAME_CLEANUP_STATE(BOOL bHostCleanUp = FALSE)
	IF !bHostCleanUp
		SEND_CLAW_CRANE_TELEMTRY()
		playerBD[NATIVE_TO_INT(PLAYER_ID())].iAttachedPrizeIndex = -1
		playerBD[NATIVE_TO_INT(PLAYER_ID())].iLocalBS = 0
		SET_CLAW_CRANE_GAME_STATE(CLAW_GAME_IDLE_STATE, !IS_ARCADE_CLAW_CRANE_BIT_SET(LOCAL_BS_CLAW_DONT_UPDATE_GAME_STATE_BD))
		STOP_CLAW_CRANE_PRIZE_SYNC_SCENE()
		STOP_CLAW_CRANE_PROP_ANIM(TRUE, TRUE, <<0,0,0>>, 0.0)
		PRINTLN("[AM_ARCADE_CLAW_CRANE] - MAINTAIN_CLAW_CRANE_LOADING_STATE PLAYER_BD_BS_PLAYING_GAME FALSE")
	ELSE
		IF DOES_ENTITY_EXIST(GET_COLLIDED_PRIZE_INDEX(TRUE))
		AND !TAKE_CONTROL_OF_ENTITY(GET_COLLIDED_PRIZE_INDEX(TRUE))
			EXIT
		ENDIF	
		SERVER_CLEANUP_PRIZE()
		CLEAR_BIT(serverBD.iServerBS, SERVER_BD_BS_START_CLAW_PRESS_DOWN_ANIM)
		SET_CLAW_CRANE_SERVER_GAME_STATE(CLAW_GAME_IDLE_STATE)
		serverBD.iAttachedPrizeIndex = -1
		serverBD.vPrizeInitialCoords = <<0,0,0>>
		serverBD.fPrizeInitialHeading = 0.0
	ENDIF
	
	SET_ARCADE_CABINET_CHILD_SCRIPT_CLIENT_STATE(sData.eState, ARCADE_CABINET_STATE_LOADING)	
	
	IF TAKE_CONTROL_OF_ENTITY(sData.clawObj[CLAW_SUB_RAIL_X])
		FREEZE_ENTITY_POSITION(sData.clawObj[CLAW_SUB_RAIL_X], TRUE)
	ENDIF
	IF TAKE_CONTROL_OF_ENTITY(sData.clawObj[CLAW_SUB_RAIL_Y])
		FREEZE_ENTITY_POSITION(sData.clawObj[CLAW_SUB_RAIL_Y], TRUE)
	ENDIF	
	IF TAKE_CONTROL_OF_ENTITY(sData.clawObj[CLAW_SUB_CLAW])
		FREEZE_ENTITY_POSITION(sData.clawObj[CLAW_SUB_CLAW], TRUE)
	ENDIF	
	IF TAKE_CONTROL_OF_ENTITY(sData.clawObj[CLAW_SUB_CLAW_COLLISION])
		SET_ENTITY_VISIBLE(sData.clawObj[CLAW_SUB_CLAW_COLLISION], FALSE)
	ENDIF	
	
	IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) != CAM_VIEW_MODE_FIRST_PERSON
		IF IS_ARCADE_CLAW_CRANE_BIT_SET(LOCAL_BS_SWITCH_TO_TPP_CAMERA)
			SET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT, CAM_VIEW_MODE_FIRST_PERSON)
		ENDIF	
	ENDIF
	
	sData.iClawCollisionShapeTest 	= CLAW_SHAPETEST_INIT   
	
	STOP_CLAW_CRANE_MOVE_SOUND()
	CLEAR_CLAW_CRANE_HELP_TEXTS()
	CLEANUP_CLAW_CAMERA()
	RESET_NET_TIMER(sData.tHypeAIContextTimer)
	RESET_NET_TIMER(sData.tPlayDurationTimer)
	
	ARCADE_CABINET_AI_CONTEXT_STRUCT sArcadeCabinetAIContext
	sData.sArcadeCabinetAIContext = sArcadeCabinetAIContext
	
	sData.iRandomWinRate = -1
	sData.sArcadeCabinetAnimEvent.fBlendInDelta = REALLY_SLOW_BLEND_IN
	sData.sArcadeCabinetAnimEvent.fBlendOutDelta = REALLY_SLOW_BLEND_OUT
	sData.iClawCollisionHitSomthing = CLAW_COLLISION_CHECK_INIT
	sData.sPrizeName = ""
	sData.iBS = 0
	sData.trophyType = ARCADE_GAME_TROPHY_NULL
ENDPROC

FUNC BOOL SHOULD_EXIT_EARLY_FROM_CLAW_CRANE_GAME()
	sData.sControlTimer.control = FRONTEND_CONTROL
	sData.sControlTimer.action = INPUT_FRONTEND_CANCEL
	IF IS_CONTROL_HELD(sData.sControlTimer)
		IF sData.eGameState = CLAW_GAME_IDLE_STATE
		OR sData.eGameState = CLAW_GAME_INIT_STATE
		OR sData.eGameState = CLAW_GAME_MOVE_FORWARD_STATE
		OR sData.eGameState = CLAW_GAME_MOVE_RIGHT_STATE
			sData.sArcadeCabinetAIContext.stContext = "CLAW_LEAVE"
			PLAY_ARCADE_CABINET_AI_CONTEXT(sData.sArcadeCabinetAIContext, TRUE)
			RETURN TRUE
		ENDIF	
	ENDIF	
	
	IF SHOULD_KICK_PLAYER_FROM_ANY_ARCADE_GAME()
		PRINTLN("[AM_ARCADE_CLAW_CRANE] SHOULD_EXIT_EARLY_FROM_CLAW_CRANE_GAME SHOULD_KICK_PLAYER_FROM_ANY_ARCADE_GAME")
		RETURN TRUE
	ENDIF	
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_ATTACH_PRIZE_TO_CLAW()
	IF DOES_ENTITY_EXIST(GET_COLLIDED_PRIZE_INDEX(FALSE))
		
		IF NOT IS_ARCADE_CLAW_CRANE_BIT_SET(LOCAL_BS_STOP_PRIZE_ANIM)
			STOP_CLAW_CRANE_PRIZE_SYNC_SCENE()
			SET_ARCADE_CLAW_CRANE_BIT(LOCAL_BS_STOP_PRIZE_ANIM)
		ENDIF	
		
		ENTITY_INDEX prizeEntity = GET_COLLIDED_PRIZE_INDEX(FALSE)
		
		IF IS_THIS_ENTITY_A_PRIZE(prizeEntity)
			IF IS_ENTITY_ALIVE(prizeEntity)
			AND IS_ENTITY_ALIVE(sData.clawObj[CLAW_SUB_CLAW_COLLISION])
				IF TAKE_CONTROL_OF_ENTITY(prizeEntity)
				AND TAKE_CONTROL_OF_ENTITY(sData.clawObj[CLAW_SUB_CLAW_COLLISION])
					IF NOT IS_ENTITY_ATTACHED_TO_ENTITY(prizeEntity, sData.clawObj[CLAW_SUB_CLAW_COLLISION])
					AND !IS_CLAW_PRIZE_SYNCHRONIZED_SCENE_RUNNING()
						IF TAKE_CONTROL_OF_ENTITY(prizeEntity)
							FREEZE_ENTITY_POSITION(prizeEntity, FALSE)
						ENDIF	
						sData.vPrizeInitialCoords = GET_ENTITY_COORDS(prizeEntity)
						sData.fPrizeInitialHeading = GET_ENTITY_HEADING(prizeEntity)
						STOP_CLAW_CRANE_PROP_ANIM(FALSE, FALSE, sData.vPrizeInitialCoords, sData.fPrizeInitialHeading)
						
						INT iBoneIndex = GET_ENTITY_BONE_INDEX_BY_NAME(sData.clawObj[CLAW_SUB_CLAW_COLLISION], "attach")
						INT iBoneIndexTwo = GET_ENTITY_BONE_INDEX_BY_NAME(prizeEntity, "attach")
						SET_ENTITY_COLLISION(prizeEntity, FALSE)

						ATTACH_ENTITY_BONE_TO_ENTITY_BONE(prizeEntity, sData.clawObj[CLAW_SUB_CLAW_COLLISION], iBoneIndexTwo, iBoneIndex, TRUE)
							
						SET_ENTITY_VISIBLE(prizeEntity, TRUE)
						playerBD[NATIVE_TO_INT(PLAYER_ID())].iAttachedPrizeIndex = GET_ATTACHED_PRIZE_ARRAY_INDEX(prizeEntity)
						g_ArcadeCabinetManagerData.iClawPrizeIndex = playerBD[NATIVE_TO_INT(PLAYER_ID())].iAttachedPrizeIndex
						SET_ARCADE_CLAW_CRANE_BIT(LOCAL_BS_ATTACHED_PRIZE_TO_CLAW)
					ENDIF	
				ENDIF	
			ENDIF	
		ELSE
			SET_CLAW_CRANE_GAME_STATE(CLAW_GAME_MOVE_UP_TO_PICK_UP_STATE)
		ENDIF
	ELSE
		IF !IS_ARCADE_CLAW_CRANE_BIT_SET(LOCAL_BS_PLAY_ASCEND_SOUND)
			PLAY_SOUND_FROM_COORD(-1, "ascend", GET_ENTITY_COORDS(sData.cabinetObj), "dlc_ch_claw_crane_sounds", TRUE, 10)
			SET_ARCADE_CLAW_CRANE_BIT(LOCAL_BS_PLAY_ASCEND_SOUND)
		ENDIF	
		SET_CLAW_CRANE_GAME_STATE(CLAW_GAME_MOVE_UP_TO_PICK_UP_STATE)		
	ENDIF
	
	IF !IS_CLAW_PRIZE_SYNCHRONIZED_SCENE_RUNNING()
	AND IS_ARCADE_CLAW_CRANE_BIT_SET(LOCAL_BS_ATTACHED_PRIZE_TO_CLAW)
		IF !IS_ARCADE_CLAW_CRANE_BIT_SET(LOCAL_BS_PLAY_ASCEND_SOUND)
			PLAY_SOUND_FROM_COORD(-1, "ascend", GET_ENTITY_COORDS(sData.cabinetObj), "dlc_ch_claw_crane_sounds", TRUE, 10)
			SET_ARCADE_CLAW_CRANE_BIT(LOCAL_BS_PLAY_ASCEND_SOUND)
		ENDIF	
		SET_CLAW_CRANE_GAME_STATE(CLAW_GAME_MOVE_UP_TO_PICK_UP_STATE)		
	ENDIF	
ENDPROC

PROC MAINTAIN_CLAW_CRANE_HYPE_AI_CONTEXT()
	IF sData.eGameState > CLAW_GAME_MOVE_FORWARD_STATE
	AND sData.eGameState < CLAW_GAME_DROP_PRIZE_STATE
		IF NOT HAS_NET_TIMER_STARTED(sData.tHypeAIContextTimer)
			sData.sArcadeCabinetAIContext.stContext = "CLAW_HYPE"
			PLAY_ARCADE_CABINET_AI_CONTEXT(sData.sArcadeCabinetAIContext, TRUE)			
			IF sData.sArcadeCabinetAIContext.bEventSent
				START_NET_TIMER(sData.tHypeAIContextTimer)
				PRINTLN("[AM_ARCADE_CLAW_CRANE] - MAINTAIN_CLAW_CRANE_HYPE_AI_CONTEXT START_NET_TIMER(sData.tHypeAIContextTimer)")
			ENDIF
		ELSE
			IF HAS_NET_TIMER_EXPIRED(sData.tHypeAIContextTimer, 25000)
				RESET_NET_TIMER(sData.tHypeAIContextTimer)
			ENDIF
		ENDIF
	ENDIF	
ENDPROC

PROC MAINTAIN_CLAW_CRANE_MOVE_SOUND_VARIABLE()
	IF !HAS_SOUND_FINISHED(sData.iMoveSoundID)
		SET_VARIABLE_ON_SOUND(sData.iMoveSoundID, "claw_speed", sData.fMoveSoundVolume)
	ENDIF	
ENDPROC

PROC PROCESS_EVENT_STOP_CLAW_ANIM(INT iCount)
	SCRIPT_EVENT_DATA_CLAW_CRANE_ANIM sEventData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, sEventData, SIZE_OF(sEventData))
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			IF !IS_VECTOR_ZERO(sEventData.vPrizeInitialCoords)
				serverBD.vPrizeInitialCoords = sEventData.vPrizeInitialCoords
				serverBD.fPrizeInitialHeading = sEventData.fPrizeInitialHeading
			ELSE
				IF sEventData.bBoth
					STOP_CLAW_CRANE_CLAW_SYNC_SCENE()
				ELSE
					IF !sEventData.bPrizeAnim
						STOP_CLAW_CRANE_CLAW_SYNC_SCENE()
					ENDIF
				ENDIF	
			ENDIF
			PRINTLN("[AM_ARCADE_CLAW_CRANE] - PROCESS_EVENT_STOP_CLAW_ANIM event recieved both:, ", sEventData.bBoth, " prize: ", sEventData.bPrizeAnim, " vPrizeInitialCoords: ", sEventData.vPrizeInitialCoords)
		ENDIF	 
	ELSE
		CDEBUG1LN(DEBUG_MINIGAME, "[AM_ARCADE_CLAW_CRANE] PROCESS_EVENT_STOP_CLAW_ANIM")
		SCRIPT_ASSERT("[AM_ARCADE_CLAW_CRANE] - PROCESS_EVENT_STOP_CLAW_ANIM - could not retrieve data.")
	ENDIF
ENDPROC

PROC MAINTAIN_CLAW_CRANE_EVENTS()
	INT iCount
	EVENT_NAMES ThisScriptEvent
	STRUCT_EVENT_COMMON_DETAILS Details
	
	REPEAT GET_NUMBER_OF_EVENTS(SCRIPT_EVENT_QUEUE_NETWORK) iCount

	    ThisScriptEvent = GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_NETWORK, iCount)
	   	
	    SWITCH ThisScriptEvent
			CASE EVENT_NETWORK_SCRIPT_EVENT
				
				GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, Details, SIZE_OF(Details))
				
				SWITCH Details.Type
					CASE SCRIPT_EVENT_STOP_CLAW_ANIM
						IF g_sBlockedEvents.bSCRIPT_EVENT_STOP_CLAW_ANIM
							EXIT
						ENDIF
						PROCESS_EVENT_STOP_CLAW_ANIM(iCount)
					BREAK
				ENDSWITCH	
			BREAK
	    ENDSWITCH
    ENDREPEAT
ENDPROC


/// PURPOSE:
///    Maintain claw crane game state 
PROC MAINTAIN_CLAW_CRANE_GAME_STATE()
	MAINTAIN_CLAW_CRANE_CONTROLS()
	
	SWITCH sData.eGameState
		CASE CLAW_GAME_INIT_STATE
			INITIALISE_CLAW_CRANE_GAME_STATE()
		BREAK
		CASE CLAW_GAME_MOVE_FORWARD_STATE	
			MOVE_CLAW_FORWARD()
		BREAK
		CASE CLAW_GAME_MOVE_RIGHT_STATE
			MOVE_CLAW_RIGHT()
		BREAK
		CASE CLAW_GAME_WAIT_TO_TRIGGER_PICK_UP_STATE
			MAINTAIN_WAIT_FOR_PLAYER_CONTROL_TO_DROP_CLAW()
		BREAK
		CASE CLAW_GAME_MOVE_DOWN_TO_PICK_UP_STATE
			MOVE_CLAW_IN_Z_AXIS(FALSE, FALSE)
			PROCESS_CLAW_COLLISION_SHAPETEST()
		BREAK
		CASE CLAW_GAME_ATTACH_PRIZE_STATE
			MAINTAIN_ATTACH_PRIZE_TO_CLAW()
		BREAK
		CASE CLAW_GAME_MOVE_UP_TO_PICK_UP_STATE
			MOVE_CLAW_IN_Z_AXIS(TRUE, FALSE)
		BREAK
		CASE CLAW_GAME_MOVE_LEFT_STATE
			MOVE_CLAW_LEFT()
		BREAK
		CASE CLAW_GAME_MOVE_BACKWARD_STATE
			MOVE_CLAW_BACKWARD()
		BREAK
		CASE CLAW_GAME_DROP_PRIZE_STATE
			MOVE_CLAW_IN_Z_AXIS(FALSE, TRUE)
		BREAK
		CASE CLAW_GAME_DROP_PRIZE_CLEANUP_STATE
			MAINTAIN_CLAW_CRANE_PRIZE_DROP_CLEANUP()
		BREAK
		CASE CLAW_GAME_CLEANUP_STATE	
			MAINTAIN_CLAW_CRANE_GAME_CLEANUP_STATE()
		BREAK
	ENDSWITCH
	
	IF SHOULD_EXIT_EARLY_FROM_CLAW_CRANE_GAME()
		SET_ARCADE_CLAW_CRANE_BIT(LOCAL_BS_CLAW_DONT_UPDATE_GAME_STATE_BD)
		sData.sArcadeCabinetAnimEvent.bHoldLastFrame = FALSE
		sData.sArcadeCabinetAnimEvent.bLoopAnim = FALSE
		PLAY_ARCADE_CABINET_ANIMATION(sData.sArcadeCabinetAnimEvent, ARCADE_CABINET_ANIM_CLIP_EXIT)
		SET_CLAW_CRANE_GAME_STATE(CLAW_GAME_CLEANUP_STATE, FALSE)
	ENDIF

	MAINTAIN_CLAW_CRANE_HYPE_AI_CONTEXT()
	MAINTAIN_CLAW_CRANE_MOVE_SOUND_VARIABLE()
ENDPROC

/// PURPOSE:
///     Maintain claw crane main client logic idle state 
PROC MAINTAIN_CLAW_CRANE_IDLE_STATE()
	MAINTAIN_CLAW_CRANE_GAME_STATE()
ENDPROC

/// PURPOSE:
///     Maintain claw crane main client logic cleanup 
PROC MAINTAIN_CLAW_CRANE_CLEANUP_STATE()

ENDPROC

/// PURPOSE:
///    Update claw crane player stagger variable
PROC UPDATE_CLAW_CRANE_PLAYER_STAGGER()
	sData.iPlayerStagger = (sData.iPlayerStagger + 1) % NUM_NETWORK_PLAYERS
ENDPROC

PROC SET_COLLISION_CLAW_TO_INVISIBLE_THIS_FRAME()
	IF DOES_ENTITY_EXIST(sData.clawObj[CLAW_SUB_CLAW_COLLISION])
		SET_ENTITY_LOCALLY_INVISIBLE(sData.clawObj[CLAW_SUB_CLAW_COLLISION])
	ENDIF
ENDPROC

FUNC BOOL SHOULD_HIDE_PLAYER_ON_FP_CAM()
	IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
		IF !IS_ARCADE_CLAW_CRANE_BIT_SET(LOCAL_BS_SWITCH_TO_TPP_CAMERA)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC HIDE_PLAYER_PED_ON_FP_CAM_FOR_CLAW_GAM()
	IF IS_PLAYER_USING_ARCADE_CABINET(PLAYER_ID())
	OR IS_PLAYER_PLAYING_THIS_ARCADE_CABINET_ANIMATION_CLIP(ARCADE_CABINET_ANIM_CLIP_CLAW_CRANE_MISS)
		IF GET_ARCADE_CABINET_LOCATE_PLAYER_IS_IN(PLAYER_ID()) = ARCADE_CABINET_CH_CLAW_CRANE
			IF DOES_CAM_EXIST(sData.clawCamera)
			OR SHOULD_HIDE_PLAYER_ON_FP_CAM()
				IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
					SET_ENTITY_LOCALLY_INVISIBLE(PLAYER_PED_ID())
				ENDIF	
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Maintain claw crane main client logic
PROC RUN_MAIN_CLIENT_LOGIC()
	SWITCH sData.eState
		CASE ARCADE_CABINET_STATE_LOADING
			MAINTAIN_CLAW_CRANE_LOADING_STATE()
		BREAK
		CASE ARCADE_CABINET_STATE_IDLE
			MAINTAIN_CLAW_CRANE_IDLE_STATE()
		BREAK
		CASE ARCADE_CABINET_STATE_CLEANUP
			MAINTAIN_CLAW_CRANE_CLEANUP_STATE()
		BREAK
	ENDSWITCH
	
	MAINTAIN_CLAW_CRANE_EVENTS()
	SET_COLLISION_CLAW_TO_INVISIBLE_THIS_FRAME()
	HIDE_PLAYER_PED_ON_FP_CAM_FOR_CLAW_GAM()
	UPDATE_CLAW_CRANE_PLAYER_STAGGER()
ENDPROC

/// PURPOSE:
///    This will check if any player is using the claw crane and sets server variables
PROC MAINTAIN_SERVER_USAGE_VARIABLES()
	PLAYER_INDEX piPlayer = INT_TO_NATIVE(PLAYER_INDEX, sData.iPlayerStagger)
		
	IF NETWORK_IS_PLAYER_ACTIVE(piPlayer)
	AND NETWORK_IS_PLAYER_A_PARTICIPANT(piPlayer)
		
		IF IS_PLAYER_USING_ARCADE_CABINET(piPlayer)
		AND GET_ARCADE_CABINET_LOCATE_PLAYER_IS_IN(piPlayer) = sData.eArcadeCabinetLocateType
			IF serverBD.plUsingClawCrane != piPlayer
				serverBD.plUsingClawCrane = piPlayer
				SET_BIT(serverBD.iServerBS, SERVER_BD_BS_ASSIGNED_GAME_USER)
				PRINTLN("[AM_ARCADE_CLAW_CRANE] - MAINTAIN_SERVER_USAGE_VARIABLES serverBD.plUsingClawCrane ", GET_PLAYER_NAME(serverBD.plUsingClawCrane))	
			ENDIF	
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(serverBD.iServerBS, SERVER_BD_BS_ASSIGNED_GAME_USER)
		IF serverBD.plUsingClawCrane != INVALID_PLAYER_INDEX()
			IF !IS_PLAYER_USING_ARCADE_CABINET(serverBD.plUsingClawCrane)
			OR GET_ARCADE_CABINET_LOCATE_PLAYER_IS_IN(serverBD.plUsingClawCrane) != sData.eArcadeCabinetLocateType
				IF serverBD.eGameState = CLAW_GAME_IDLE_STATE
					CLEAR_BIT(serverBD.iServerBS, SERVER_BD_BS_ASSIGNED_GAME_USER)
					PRINTLN("[AM_ARCADE_CLAW_CRANE] - MAINTAIN_SERVER_USAGE_VARIABLES reseting plUsingClawCrane player stop using ", GET_PLAYER_NAME(serverBD.plUsingClawCrane))	
					serverBD.plUsingClawCrane = INVALID_PLAYER_INDEX()
				ENDIF
			ENDIF
		ENDIF
	ENDIF	
ENDPROC

PROC MAINTAIN_SERVER_COLLIDED_PRIZE_INDEX()
	IF serverBD.plUsingClawCrane != INVALID_PLAYER_INDEX()
	AND NETWORK_IS_PLAYER_A_PARTICIPANT(serverBD.plUsingClawCrane)
		IF playerBD[NATIVE_TO_INT(serverBD.plUsingClawCrane)].iAttachedPrizeIndex != -1
			IF serverBD.iAttachedPrizeIndex != playerBD[NATIVE_TO_INT(serverBD.plUsingClawCrane)].iAttachedPrizeIndex
				serverBD.iAttachedPrizeIndex = playerBD[NATIVE_TO_INT(serverBD.plUsingClawCrane)].iAttachedPrizeIndex
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Host will check if any one is using the claw crane and stores the game logic in serverBD
PROC MAINTAIN_SERVER_GAME_LOGIC_STATE()
	IF serverBD.plUsingClawCrane != INVALID_PLAYER_INDEX()
	AND NETWORK_IS_PLAYER_A_PARTICIPANT(serverBD.plUsingClawCrane)
	AND IS_BIT_SET(playerBD[NATIVE_TO_INT(serverBD.plUsingClawCrane)].iLocalBS, PLAYER_BD_BS_PLAYING_GAME)
		SET_CLAW_CRANE_SERVER_GAME_STATE(playerBD[NATIVE_TO_INT(serverBD.plUsingClawCrane)].eGameState)
	ENDIF
ENDPROC

PROC MAINTAIN_SERVER_CLAW_CRANE_GAME_STATE_RESET()
	SWITCH serverBD.eGameState
		CASE CLAW_GAME_INIT_STATE
			SET_CLAW_CRANE_SERVER_GAME_STATE(CLAW_GAME_IDLE_STATE)
			serverBD.plUsingClawCrane = INVALID_PLAYER_INDEX()
			CLEAR_BIT(serverBD.iServerBS, SERVER_BD_BS_ASSIGNED_GAME_USER)
			PRINTLN("[AM_ARCADE_CLAW_CRANE] - MAINTAIN_SERVER_CLAW_CRANE_GAME_STATE_RESET server game state stuck in init we are safe to reset all variables")	
		BREAK
		CASE CLAW_GAME_MOVE_FORWARD_STATE
			SET_CLAW_CRANE_SERVER_GAME_STATE(CLAW_GAME_MOVE_BACKWARD_STATE)
		BREAK
		CASE CLAW_GAME_MOVE_RIGHT_STATE
		CASE CLAW_GAME_WAIT_TO_TRIGGER_PICK_UP_STATE
			SET_CLAW_CRANE_SERVER_GAME_STATE(CLAW_GAME_MOVE_LEFT_STATE)
		BREAK
		CASE CLAW_GAME_MOVE_LEFT_STATE
			MOVE_CLAW_LEFT(TRUE)
		BREAK
		CASE CLAW_GAME_MOVE_BACKWARD_STATE
			MOVE_CLAW_BACKWARD(TRUE)
		BREAK
		CASE CLAW_GAME_MOVE_DOWN_TO_PICK_UP_STATE
			SET_CLAW_CRANE_SERVER_GAME_STATE(CLAW_GAME_MOVE_UP_TO_PICK_UP_STATE)
		BREAK
		CASE CLAW_GAME_ATTACH_PRIZE_STATE
		CASE CLAW_GAME_MOVE_UP_TO_PICK_UP_STATE
			MOVE_CLAW_IN_Z_AXIS(TRUE, FALSE, TRUE)
		BREAK
		CASE CLAW_GAME_DROP_PRIZE_STATE
			MOVE_CLAW_IN_Z_AXIS(FALSE, TRUE, TRUE)
		BREAK
		CASE CLAW_GAME_CLEANUP_STATE
		CASE CLAW_GAME_DROP_PRIZE_CLEANUP_STATE
			MAINTAIN_CLAW_CRANE_GAME_CLEANUP_STATE(TRUE)
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL SHOULD_SERVER_RESET_CLAW_CRANE_GAME_STATE()
	IF serverBD.plUsingClawCrane = INVALID_PLAYER_INDEX()
		RETURN TRUE
	ENDIF	
	
	IF !NETWORK_IS_PLAYER_A_PARTICIPANT(serverBD.plUsingClawCrane) 
		RETURN TRUE
	ENDIF
	
	IF !IS_BIT_SET(playerBD[NATIVE_TO_INT(serverBD.plUsingClawCrane)].iLocalBS, PLAYER_BD_BS_PLAYING_GAME)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Check if claw crane user bail from game and reset the claw crane props to inital position
PROC  MAINTAIN_RESET_CLAW_CRANE_GAME_STATE()
	IF SHOULD_SERVER_RESET_CLAW_CRANE_GAME_STATE()
		IF serverBD.eGameState != CLAW_GAME_IDLE_STATE
			MAINTAIN_SERVER_CLAW_CRANE_GAME_STATE_RESET()
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_ANY_PLAYER_NEAR_CLAW_CRANE_CABINET()
	IF DOES_ENTITY_EXIST(sData.cabinetObj)
		VECTOR vCabinetCoords = GET_ENTITY_COORDS(sData.cabinetObj)
		INT i
		REPEAT NUM_NETWORK_PLAYERS i
			PLAYER_INDEX playerID = INT_TO_PLAYERINDEX(i)
			
			IF IS_NET_PLAYER_OK(playerID)
			AND NOT NETWORK_IS_PLAYER_CONCEALED(playerID)
				PED_INDEX pPed = GET_PLAYER_PED(playerID)
				
				IF DOES_ENTITY_EXIST(pPed)
				AND IS_ENTITY_AT_COORD(pPed, vCabinetCoords, <<5 ,5 ,2 >>)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Check if we need to set prizes to visible every 2 and half min 
PROC MAINTAIN_SERVER_PRIZE_VISIBILITY_TIMER()
	IF HAS_NET_TIMER_STARTED(serverBD.prizeVisibilityTimer)
		IF HAS_NET_TIMER_EXPIRED(serverBD.prizeVisibilityTimer, 150000)
			IF serverBD.plUsingClawCrane = INVALID_PLAYER_INDEX()
				IF !IS_ANY_PLAYER_NEAR_CLAW_CRANE_CABINET()
					INT i, iNumInvisibleProp
					FOR i = ENUM_TO_INT(CLAW_SUB_PRIZE_1) TO ENUM_TO_INT(CLAW_SUB_PRIZE_10)
						IF DOES_ENTITY_EXIST(sData.clawObj[i])
							IF NOT IS_ENTITY_VISIBLE(sData.clawObj[i])
							AND TAKE_CONTROL_OF_ENTITY(sData.clawObj[i])
								iNumInvisibleProp++
								PRINTLN("[AM_ARCADE_CLAW_CRANE] - MAINTAIN_SERVER_PRIZE_VISIBILITY_TIMER = iNumInvisibleProp: ", iNumInvisibleProp)
							ENDIF
						ENDIF
					ENDFOR
					
					IF iNumInvisibleProp > 4
						FOR i = ENUM_TO_INT(CLAW_SUB_PRIZE_1) TO ENUM_TO_INT(CLAW_SUB_PRIZE_10)
							IF DOES_ENTITY_EXIST(sData.clawObj[i])
								IF NOT IS_ENTITY_VISIBLE(sData.clawObj[i])
								AND TAKE_CONTROL_OF_ENTITY(sData.clawObj[i])
									SET_ENTITY_VISIBLE(sData.clawObj[i], TRUE)
								ENDIF
							ENDIF
						ENDFOR
					ENDIF
					
					REINIT_NET_TIMER(serverBD.prizeVisibilityTimer)
					PRINTLN("[AM_ARCADE_CLAW_CRANE] - MAINTAIN_SERVER_PRIZE_VISIBILITY_TIMER = DONE")
				ELSE
					#IF IS_DEBUG_BUILD
					IF GET_FRAME_COUNT() % 120 = 0
						PRINTLN("[AM_ARCADE_CLAW_CRANE] - MAINTAIN_SERVER_PRIZE_VISIBILITY_TIMER someone near the cabinet")
					ENDIF	
					#ENDIF	
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
				IF GET_FRAME_COUNT() % 120 = 0
					PRINTLN("[AM_ARCADE_CLAW_CRANE] - MAINTAIN_SERVER_PRIZE_VISIBILITY_TIMER claw cabinet in use")
				ENDIF	
				#ENDIF	
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_CLAW_PROP_ANIMS()
	IF serverBD.plUsingClawCrane != INVALID_PLAYER_INDEX()
	AND NETWORK_IS_PLAYER_A_PARTICIPANT(serverBD.plUsingClawCrane)
		IF !IS_BIT_SET(serverBD.iServerBS, SERVER_BD_BS_START_CLAW_PRESS_DOWN_ANIM)
			IF IS_BIT_SET(playerBD[NATIVE_TO_INT(serverBD.plUsingClawCrane)].iLocalBS, PLAYER_BD_BS_START_CLAW_PRESS_DOWN_ANIM)
				START_CLAW_SYNC_SCENE(ARCADE_CABINET_ANIM_CLIP_CLAW_CRANE_PRESS_GRAB, TRUE, DEFAULT, SLOW_BLEND_IN, SLOW_BLEND_OUT)
				SET_BIT(serverBD.iServerBS, SERVER_BD_BS_START_CLAW_PRESS_DOWN_ANIM)
				PRINTLN("[AM_ARCADE_CLAW_CRANE] - MAINTAIN_CLAW_PROP_ANIMS SERVER_BD_BS_START_CLAW_PRESS_DOWN_ANIM TRUE")
			ENDIF
		ENDIF	
	ENDIF	
ENDPROC

/// PURPOSE:
///    Maintain claw crane main server logic
PROC RUN_MAIN_SERVER_LOGIC()
	MAINTAIN_SERVER_USAGE_VARIABLES()
	MAINTAIN_SERVER_GAME_LOGIC_STATE()
	MAINTAIN_SERVER_PRIZE_VISIBILITY_TIMER()
	MAINTAIN_SERVER_COLLIDED_PRIZE_INDEX()
	MAINTAIN_RESET_CLAW_CRANE_GAME_STATE()
	MAINTAIN_CLAW_PROP_ANIMS()
ENDPROC

#IF IS_DEBUG_BUILD
PROC UPDATE_CLAW_CRANE_DEBUG_OFFSET_VALUES()
	INT i 
	FOR i = 0 TO ENUM_TO_INT(CLAW_SUB_TOTAL) - 1
		IF DOES_ENTITY_EXIST(sData.clawObj[i])
			IF !IS_VECTOR_ZERO(sClawCraneDebugData.vSubPropOffset[i])
				SET_ENTITY_COORDS(sData.clawObj[i], GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sData.cabinetObj, sClawCraneDebugData.vSubPropOffset[i]))
			ENDIF
		ENDIF
	ENDFOR	
	
//	VECTOR vOffset = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(sData.cabinetObj, <<2731.3523, -372.5332, -48.4290>>)
//	PRINTLN("[UPDATE_CLAW_CRANE_DEBUG_OFFSET_VALUES] offset: ", vOffset)
ENDPROC

PROC UPDATE_DEBUG_WIDGETS()
	UPDATE_CLAW_CRANE_DEBUG_OFFSET_VALUES()
	
	#IF IS_DEBUG_BUILD
	IF sClawCraneDebugData.bDisplayDistanceInfo
	IF IS_ARCADE_CLAW_CRANE_BIT_SET(LOCAL_BS_ATTACHED_PRIZE_TO_CLAW)
		IF sData.eGameState < CLAW_GAME_DROP_PRIZE_STATE
			DRAW_DEBUG_TEXT_2D("Within safe distance ", <<0.5,0.5,0.5>>,0 , 255, 0)
		ENDIF	
	ENDIF
	ENDIF
	#ENDIF
ENDPROC
#ENDIF

SCRIPT (ACM_CHILD_SCRIPT_LAUNCHER_STRUCT missionScriptArgs)

	IF NETWORK_IS_GAME_IN_PROGRESS() 
		SCRIPT_INITIALISE(missionScriptArgs)	
	ELSE
		SCRIPT_CLEANUP()
	ENDIF

	WHILE TRUE
		MP_LOOP_WAIT_ZERO()
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		SCRIPT_PROFILER_START_OF_FRAME() 
		#ENDIF
		#ENDIF	
			
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			PRINTLN("[AM_ARCADE_CLAW_CRANE] - SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE = TRUE")
			SCRIPT_CLEANUP()
		ENDIF
		
		IF SHOULD_KILL_THIS_SCRIPT()
			PRINTLN("[AM_ARCADE_CLAW_CRANE] - SHOULD_KILL_THIS_SCRIPT = TRUE")
			SCRIPT_CLEANUP()
		ENDIF
		
		RUN_MAIN_CLIENT_LOGIC()

		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			RUN_MAIN_SERVER_LOGIC()
		ENDIF
		
		#IF IS_DEBUG_BUILD
		UPDATE_DEBUG_WIDGETS()
		
		#IF SCRIPT_PROFILER_ACTIVE
		SCRIPT_PROFILER_END_OF_FRAME() 
		#ENDIF
		#ENDIF

	ENDWHILE

#ENDIF	
	
#IF NOT FEATURE_CASINO_HEIST
SCRIPT
#ENDIF	
	
ENDSCRIPT 


