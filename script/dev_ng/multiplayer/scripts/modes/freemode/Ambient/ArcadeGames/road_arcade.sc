USING "globals.sch"
USING "net_events.sch"
USING "net_wait_zero.sch"
USING "net_mission.sch"
USING "arcade_road/arcade_road_prototype.sch"
USING "degenatron_games_movie.sch"

#IF IS_DEBUG_BUILD
	WIDGET_GROUP_ID widgetMinigame
#ENDIF

PROC CLEANUP_SCRIPT()

	ROAD_CLEANUP_AND_EXIT_CLIENT()
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
	
ENDPROC

/// PURPOSE:
///   Do necessary pre game start initialization.
PROC PROCESS_PRE_GAME(MP_MISSION_DATA missionScriptArgs)

	// This marks the script as a net script, and handles any instancing setup. 
	SETUP_MP_MISSION_FOR_NETWORK(GET_MAX_NUM_PARTICIPANTS_FOR_MP_MISSION(missionScriptArgs.mdID.idMission) , missionScriptArgs)
	
	// This makes sure the net script is active, waits until it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(roadArcadeServerBd, SIZE_OF(roadArcadeServerBd))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(roadArcadePlayerBd, SIZE_OF(roadArcadePlayerBd))
	
	NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()
	
	// This script will not be paused if another script calls PAUSE_GAME
	SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)

	iPlayerSeatLocalID = GET_ARCADE_CABINET_LOCATE_PLAYER_SLOT_PLAYER_IS_IN(PLAYER_ID())

// Consider that we may have launched formt the M Menu:
	IF GET_ARCADE_CABINET_LOCATE_PLAYER_IS_IN(PLAYER_ID()) = ARCADE_CABINET_INVALID
#IF IS_DEBUG_BUILD

		CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugRoadArcade, "PROCESS_PRE_GAME - Setting up this frame")
			
		IF iPlayerSeatLocalID = -1
			iPlayerSeatLocalID = MPGlobalsAmbience.iRoadArcade_SeatOverride
		ENDIF

		IF MPGlobalsAmbience.iRoadArcade_GameTypeOverride = ENUM_TO_INT(RA_GT_CAR)
			eRoadArcade_ActiveGameType = RA_GT_CAR
			
			// Initialise Post FX
			ARCADE_GAMES_POSTFX_INIT(CASINO_ARCADE_GAME_RNC_STREET_LEGAL)

		ENDIF
		
		IF MPGlobalsAmbience.iRoadArcade_GameTypeOverride = ENUM_TO_INT(RA_GT_BIKE)
			eRoadArcade_ActiveGameType = RA_GT_BIKE
			
			// Initialise Post FX
			ARCADE_GAMES_POSTFX_INIT(CASINO_ARCADE_GAME_RNC_CROTCH_ROCKETS)
			
		ENDIF
		
		IF MPGlobalsAmbience.iRoadArcade_GameTypeOverride = ENUM_TO_INT(RA_GT_TRUCK)
			eRoadArcade_ActiveGameType = RA_GT_TRUCK
			
			// Initialise Post FX
			ARCADE_GAMES_POSTFX_INIT(CASINO_ARCADE_GAME_RNC_GET_TRUCKIN)
			
		ENDIF

#ENDIF
	ENDIF
	
	// If still invalid set to 0.
	IF iPlayerSeatLocalID = -1
		iPlayerSeatLocalID = 0
	ENDIF
	
#IF IS_DEBUG_BUILD				

	//iPlayerSeatLocalID = 1
	IF IS_KEYBOARD_KEY_PRESSED(KEY_H)
		IF iPlayerSeatLocalID = 1
			iPlayerSeatLocalID = 0
		ELSE
			iPlayerSeatLocalID = 1
		ENDIF
	ENDIF
#ENDIF
	
	// Initialise common Colour structs
	ROAD_ARCADE_TOD_COLOUR_STRUCTS()

	IF NOT Wait_For_First_Network_Broadcast() 
		CLEANUP_SCRIPT()
	ENDIF

	// Initialise hold actions
	schtQuitGame.control = FRONTEND_CONTROL
	schtQuitGame.action  = INPUT_SCRIPT_RUP
		
	schtQuitGamePC.control = FRONTEND_CONTROL
	schtQuitGamePC.action = INPUT_FRONTEND_DELETE
	
	bCloseGame = FALSE
	
	CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugRoadArcade, "PROCESS_PRE_GAME - Setting up this frame mdUniqueID: ", missionScriptArgs.mdUniqueID)
		
	// Select game type
	IF GET_ARCADE_CABINET_LOCATE_PLAYER_IS_IN(PLAYER_ID()) != ARCADE_CABINET_INVALID
		IF GET_ARCADE_CABINET_LOCATE_PLAYER_IS_IN(PLAYER_ID()) = ARCADE_CABINET_CH_NIGHT_DRIVE_CAR
			eRoadArcade_ActiveGameType = RA_GT_CAR
			
			// Initialise Post FX
			ARCADE_GAMES_POSTFX_INIT(CASINO_ARCADE_GAME_RNC_STREET_LEGAL)
			
		ENDIF
		
		IF GET_ARCADE_CABINET_LOCATE_PLAYER_IS_IN(PLAYER_ID()) = ARCADE_CABINET_CH_NIGHT_DRIVE_BIKE
			eRoadArcade_ActiveGameType = RA_GT_BIKE
			
			// Initialise Post FX
			ARCADE_GAMES_POSTFX_INIT(CASINO_ARCADE_GAME_RNC_CROTCH_ROCKETS)
						
		ENDIF
		
		IF GET_ARCADE_CABINET_LOCATE_PLAYER_IS_IN(PLAYER_ID()) = ARCADE_CABINET_CH_NIGHT_DRIVE_TRUCK
			eRoadArcade_ActiveGameType = RA_GT_TRUCK
			
			// Initialise Post FX
			ARCADE_GAMES_POSTFX_INIT(CASINO_ARCADE_GAME_RNC_GET_TRUCKIN)
			
		ENDIF
		
//		IF GET_ARCADE_CABINET_LOCATE_PLAYER_IS_IN(PLAYER_ID()) = ARCADE_CABINET_HAL_NIGHT_DRIVE_MUTANT
//			eRoadArcade_ActiveGameType = RA_GT_MUTANT
//						
//			// Initialise Post FX
//			ARCADE_GAMES_POSTFX_INIT(CASINO_ARCADE_GAME_RNC_STREET_LEGAL)
//			
//		ENDIF
	ELSE
		CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugRoadArcade, "PROCESS_PRE_GAME - returning ARCADE_CABINET_INVALID")
	ENDIF
	
	IF eRoadArcade_ActiveGameType = RA_GT_CAR
		CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugRoadArcade, "PROCESS_PRE_GAME - Setting up the Active Game Type RA_GT_CAR")
	ENDIF
	
	IF eRoadArcade_ActiveGameType = RA_GT_BIKE
		CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugRoadArcade, "PROCESS_PRE_GAME - Setting up the Active Game Type RA_GT_BIKE")
	ENDIF
	
	IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
		CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugRoadArcade, "PROCESS_PRE_GAME - Setting up the Active Game Type RA_GT_TRUCK")
	ENDIF
				
	// Race n Chase player one colour
	IF eRoadArcade_ActiveGameType = RA_GT_CAR
		IF iPlayerSeatLocalID = 0
			INIT_RGBA_STRUCT(rgbaFrameColour, 255, 158, 234)
		ENDIF
		
		IF iPlayerSeatLocalID = 1
			INIT_RGBA_STRUCT(rgbaFrameColour, 251, 206, 25)
		ENDIF
	ENDIF
	
	IF eRoadArcade_ActiveGameType = RA_GT_BIKE
		IF iPlayerSeatLocalID = 0
			INIT_RGBA_STRUCT(rgbaFrameColour, 200, 46, 14)
		ENDIF
		
		IF iPlayerSeatLocalID = 1
			INIT_RGBA_STRUCT(rgbaFrameColour, 0, 194, 137)
		ENDIF
	ENDIF
	
	IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
		IF iPlayerSeatLocalID = 0
			INIT_RGBA_STRUCT(rgbaFrameColour, 222, 8, 8)
		ENDIF
		
		IF iPlayerSeatLocalID = 1
			INIT_RGBA_STRUCT(rgbaFrameColour, 30, 31, 126)
		ENDIF
	ENDIF
	
	// Intialise the game game
	gameCurrentState = GS_INIT
	ROAD_ARCADE_NET_SET_SERVER_STATE(MULTI_SERVER_INIT)
		
	// Reset closing variables
	bFinishedRace = FALSE
	bCloseGame = FALSE
	bGameOVER = FALSE
	
	// Initialise structures
	ROAD_ARCADE_RESET_WARNING_OBJ()
	ROAD_ARCADE_RESET_TICKER_OBJ()
	
	// Initialise overall run timer.
	fRunTimer = 0.0
	
	REQUEST_STREAMED_TEXTURE_DICT(sDICT_HUDGeneric) // Used throughout cloading early.

ENDPROC

SCRIPT(MP_MISSION_DATA missionScriptArgs)
	
	missionScriptArgs.mdID.idMission = eAM_ROAD_ARCADE_CABINET
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		PROCESS_PRE_GAME(missionScriptArgs)
	ENDIF
	
	WHILE TRUE
		
		MP_LOOP_WAIT_ZERO()
		
		IF NETWORK_IS_GAME_IN_PROGRESS()
		
	#IF IS_DEBUG_BUILD
			IF NOT DOES_WIDGET_GROUP_EXIST(widgetMinigame)
				widgetMinigame = START_WIDGET_GROUP("Scrolling Arcade Cabinet Minigame")
				STOP_WIDGET_GROUP()
				ROAD_DEBUG_SET_PARENT_WIDGET_GROUP(widgetMinigame)
			ENDIF
				
			IF IS_KEYBOARD_KEY_PRESSED(KEY_F)
				CLEANUP_SCRIPT()
			ENDIF
	#ENDIF

			ROAD_ARCADE_LOAD_LEADERBOARDS()
			ARCADE_GAMES_LEADERBOARD_PROCESS_EVENTS()
			ARCADE_GAMES_HELP_TEXT_PRE_UPDATE()

			ARCADE_CABINET_COMMON_EVERY_FRAME_PROCESSING(gameCurrentState != GS_STREAM_WARNINGS)
				
	#IF IS_DEBUG_BUILD
			ROAD_DEBUG_PROCESSING()
	#ENDIF

			// Determines whether to switch to the next State in some cases
			BOOL bContinue = TRUE
			
			IF gameCurrentState != GS_INIT
			AND gameCurrentState != GS_STREAM_OVERLAYS
			AND HAS_STREAMED_TEXTURE_DICT_LOADED(sDICT_ScreenOverlay)
				ARCADE_DRAW_PIXELSPACE_RECT(INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2.0, cfBASE_SCREEN_HEIGHT/2.0), INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH, cfBASE_SCREEN_HEIGHT), rgbaAkedo) // 'Akedo' blue
			ENDIF
			
			// Lost connection to MP state
			IF NOT ROAD_ARCADE_NET_IS_MULTIPLAYER_GAME_ACTIVE()
			AND IS_BITMASK_AS_ENUM_SET(roadArcadePlayerBd[PARTICIPANT_ID_TO_INT()].iNetBitSet, RA_NET_GAME_INITIALISED)
			AND gameCurrentState != GS_MULTIPLAYER_REMATCH_DECLINED
				
				CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugRoadArcade, "STOPPING MP GAME for Player: ", PARTICIPANT_ID_TO_INT(), " GAME STATE FOR PLAYER: ", ROAD_ARCADE_GET_GAME_STATE_AS_STRING(gameCurrentState))
				
				IF gameCurrentState = GS_MAIN_GAME_UPDATE
					RELEASE_INGAME_ASSETS()
				ELIF gameCurrentState = GS_MULTIPLAYER_CHALLENGER_SCREEN
					RELEASE_TITLE_ASSETS(ROAD_ARCADE_GET_STAGE_TEXTURE_DICT(stgCurrentStage))
				ELSE
					RELEASE_MENU_ASSETS()
				ENDIF
			
				ROAD_ARCADE_RELEASE_ALL_SOUNDS()
			
				gameCurrentState = GS_STREAM_MULTIPLAYER_LOST_CONNECTION_ASSETS
				CLEAR_BITMASK_AS_ENUM(roadArcadePlayerBd[PARTICIPANT_ID_TO_INT()].iNetBitSet, RA_NET_GAME_INITIALISED)
				CLEAR_BITMASK_AS_ENUM(roadArcadePlayerBd[PARTICIPANT_ID_TO_INT()].iNetBitSet, RA_NET_PLAYER_CHALLENGE)
			ENDIF
			
			// Change game into MP state
			IF ROAD_ARCADE_NET_IS_MULTIPLAYER_GAME_ACTIVE()
			AND NOT IS_BITMASK_AS_ENUM_SET(roadArcadePlayerBd[PARTICIPANT_ID_TO_INT()].iNetBitSet, RA_NET_GAME_INITIALISED)
			
				CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugRoadArcade, "INITIALISING MP GAME for Player: ", PARTICIPANT_ID_TO_INT(), " GAME STATE FOR PLAYER: ", ROAD_ARCADE_GET_GAME_STATE_AS_STRING(gameCurrentState))
			
				IF gameCurrentState = GS_MAIN_GAME_UPDATE
					RELEASE_INGAME_ASSETS()
				ELIF gameCurrentState = GS_TITLE_SCREEN
					RELEASE_TITLE_ASSETS(ROAD_ARCADE_GET_STAGE_TEXTURE_DICT(stgCurrentStage))
				ELSE
					RELEASE_MENU_ASSETS()
				ENDIF
			
				ROAD_ARCADE_TITLE_SETUP(TRUE)
				ROAD_ARCADE_RELEASE_ALL_SOUNDS()
				gameCurrentState = GS_MULTIPLAYER_STREAM_CHALLENGER_ASSETS
				SET_BITMASK_AS_ENUM(roadArcadePlayerBd[PARTICIPANT_ID_TO_INT()].iNetBitSet, RA_NET_GAME_INITIALISED)
			ENDIF
									
			// Interpret the Initials string into a Hash value
			SWITCH gameCurrentState
			
				CASE GS_INIT
					bContinue = FALSE
					gameCurrentState = GS_STREAM_OVERLAYS
				BREAK
			
				CASE GS_STREAM_OVERLAYS
					REQUEST_STREAMED_TEXTURE_DICT(sDICT_ScreenOverlay)
					REQUEST_STREAMED_TEXTURE_DICT("MPArcadeGamesFX00")
					REQUEST_STREAMED_TEXTURE_DICT("MPArcadeGamesFX01")
					REQUEST_STREAMED_TEXTURE_DICT("MPArcadeGamesFX02")
					REQUEST_STREAMED_TEXTURE_DICT("MPArcadeGamesFX03")
					REQUEST_STREAMED_TEXTURE_DICT("MPArcadeGamesFX04")
					REQUEST_STREAMED_TEXTURE_DICT("MPArcadeGamesFX05")
					gameCurrentState = GS_STREAM_WARNINGS
				BREAK
			
				CASE GS_STREAM_WARNINGS
				
					REQUEST_STREAMED_TEXTURE_DICT("MPRoadArcade_Warnings")
				
					IF NOT ROAD_ARCADE_STREAM_AUDIO()
						bContinue = FALSE
						CDEBUG1LN(DEBUG_MINIGAME, "GS_STREAM_WARNINGS - Waiting on audio to stream")
					ENDIF
				
					IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED("MPRoadArcade_Warnings")
						bContinue = FALSE
					ENDIF
				
					IF bContinue
						SETTIMERA(0)
						ARCADE_GAMES_SOUND_INIT(CASINO_ARCADE_GAME_RNC_STREET_LEGAL)
						ROAD_ARCADE_AUDIO_START_AUDIO_SCENE(ROAD_ARCADE_AUDIO_SCENE_MACHINE)
						gameCurrentState = GS_SOBERUP_SCREEN
					ENDIF
				BREAK
			
				CASE GS_FIB_SCREEN
				
					ARCADE_DRAW_PIXELSPACE_SPRITE("MPRoadArcade_Warnings", 
						"ARCADE_FIB", 
						INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2, cfBASE_SCREEN_HEIGHT/2), 
						INIT_VECTOR_2D(832, 760), 0, rgbaOriginal)
					
					// Change game mode from here
					#IF IS_DEBUG_BUILD	
					
						IF IS_KEYBOARD_KEY_PRESSED(KEY_J)
							eRoadArcade_ActiveGameType = RA_GT_BIKE

							IF iPlayerSeatLocalID = 0
								INIT_RGBA_STRUCT(rgbaFrameColour, 200, 46, 14)
							ENDIF
							
							IF iPlayerSeatLocalID = 1
								INIT_RGBA_STRUCT(rgbaFrameColour, 0, 194, 137)
							ENDIF
							
						ENDIF
						
						IF IS_KEYBOARD_KEY_PRESSED(KEY_K)
							eRoadArcade_ActiveGameType = RA_GT_TRUCK
							
							IF iPlayerSeatLocalID = 0
								INIT_RGBA_STRUCT(rgbaFrameColour, 222, 8, 8)
							ENDIF
							
							IF iPlayerSeatLocalID = 1
								INIT_RGBA_STRUCT(rgbaFrameColour, 30, 31, 126)
							ENDIF
							
						ENDIF
					#ENDIF
					
					IF RA_INPUT_IS_ACCEL_JUST_PRESSED()
					OR TIMERA() >= 5000
						SETTIMERA(0)
						gameCurrentState = GS_SOBERUP_SCREEN
					ENDIF
					
				BREAK
			
				CASE GS_SOBERUP_SCREEN
				
					// Black backdrop
					ARCADE_DRAW_PIXELSPACE_RECT(INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2.0, cfBASE_SCREEN_HEIGHT/2.0), INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH, cfBASE_SCREEN_HEIGHT), rgbaBlackOut)
				
					IF eRoadArcade_ActiveGameType = RA_GT_CAR
						ARCADE_DRAW_PIXELSPACE_SPRITE("MPRoadArcade_Warnings", 
							"ARCADE_247_CAR", 
							INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2, cfBASE_SCREEN_HEIGHT/2), 
							INIT_VECTOR_2D(832, 760), 0, rgbaOriginal)
					ENDIF
					
					IF eRoadArcade_ActiveGameType = RA_GT_BIKE
						ARCADE_DRAW_PIXELSPACE_SPRITE("MPRoadArcade_Warnings", 
							"ARCADE_247_BIKE", 
							INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2, cfBASE_SCREEN_HEIGHT/2), 
							INIT_VECTOR_2D(832, 760), 0, rgbaOriginal)
					ENDIF
					
					IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
						ARCADE_DRAW_PIXELSPACE_SPRITE("MPRoadArcade_Warnings", 
							"ARCADE_247_TRUCK", 
							INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2, cfBASE_SCREEN_HEIGHT/2), 
							INIT_VECTOR_2D(832, 760), 0, rgbaOriginal)
					ENDIF
					
					
					
					// Override game mode from here
					#IF IS_DEBUG_BUILD	
					
						IF IS_KEYBOARD_KEY_PRESSED(KEY_J)
							eRoadArcade_ActiveGameType = RA_GT_BIKE

							IF iPlayerSeatLocalID = 0
								INIT_RGBA_STRUCT(rgbaFrameColour, 200, 46, 14)
							ENDIF
							
							IF iPlayerSeatLocalID = 1
								INIT_RGBA_STRUCT(rgbaFrameColour, 0, 194, 137)
							ENDIF
							
						ENDIF
						
						IF IS_KEYBOARD_KEY_PRESSED(KEY_K)
							eRoadArcade_ActiveGameType = RA_GT_TRUCK
							
							IF iPlayerSeatLocalID = 0
								INIT_RGBA_STRUCT(rgbaFrameColour, 222, 8, 8)
							ENDIF
							
							IF iPlayerSeatLocalID = 1
								INIT_RGBA_STRUCT(rgbaFrameColour, 30, 31, 126)
							ENDIF
							
						ENDIF
					#ENDIF
					
					IF RA_INPUT_IS_ACCEL_JUST_PRESSED()
					OR TIMERA() >= 5000
						SETTIMERA(0)
						
						SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPRoadArcade_Warnings")
												
						// Set up the Bink vid
						IF eRoadArcade_ActiveGameType = RA_GT_CAR
							DG_START_MOVIE(splashBink, "_Racing_StreetLegal")  // _1992_DegenatronLogo_720_auto 
						ENDIF
						
						IF eRoadArcade_ActiveGameType = RA_GT_BIKE
							DG_START_MOVIE(splashBink, "_Racing_CrotchRockets")  // _1992_DegenatronLogo_720_auto 
						ENDIF
						
						IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
							DG_START_MOVIE(splashBink, "_Racing_GetTruckin")  // _1992_DegenatronLogo_720_auto 
						ENDIF
						
						gameCurrentState = GS_OPENING_BINK
					ENDIF
				BREAK
				
				CASE GS_OPENING_BINK

					bContinue = DG_DRAW_MOVIE(splashBink, 
						1.0 * GET_ASPECT_RATIO_MODIFIER_FROM_ASPECT_RATIO(), 1.0)
					
					IF bContinue
					OR RA_INPUT_IS_ACCEL_JUST_PRESSED()	
						// Intialise Intro components	
						ROAD_ARCADE_TITLE_SETUP()
						DG_CLEAN_UP_MOVIE(splashBink)
						gameCurrentState = GS_STREAM_TITLE_ASSETS
					ENDIF
					
				BREAK
				
				CASE GS_STREAM_TITLE_ASSETS
					
					
					IF NOT ROAD_ARCADE_HAVE_TITLE_ASSETS_LOADED(stgCurrentStage, pvPlayer.sSpriteDict)
						bContinue = FALSE
					ENDIF
					
					IF bContinue
																		
						gameCurrentState = GS_TITLE_SCREEN
						
						bContinue = FALSE
					ENDIF
				
				BREAK
			
			
				CASE GS_TITLE_SCREEN
				
					ARCADE_GAMES_HELP_TEXT_PRINT_FOREVER(ARCADE_GAMES_HELP_TEXT_ENUM_RNC_HELP_LEADERBOARD_VIEW)
				
					ROAD_ARCADE_TITLE_UPDATE()
						
					// View the leaderboard
					IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
						RELEASE_TITLE_ASSETS(ROAD_ARCADE_GET_STAGE_TEXTURE_DICT(stgCurrentStage))
						
						ROAD_ARCADE_RELEASE_ALL_SOUNDS()
							
						SETTIMERA(0)
						SETTIMERB(0)
						
						REQUEST_STREAMED_TEXTURE_DICT(ROAD_ARCADE_GET_LEADERBOARD_DICT())
						
						gameCurrentState = GS_LEADERBOARD_DISPLAY
					ENDIF
					
					IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
						IF IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SPRINT)
						OR IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_ENTER)
						OR IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_RELOAD)
						OR IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_AIM)
							RELEASE_TITLE_ASSETS(ROAD_ARCADE_GET_STAGE_TEXTURE_DICT(stgCurrentStage))
						
							ROAD_ARCADE_RELEASE_ALL_SOUNDS()
								
							SETTIMERA(0)
							SETTIMERB(0)
							
							REQUEST_STREAMED_TEXTURE_DICT(ROAD_ARCADE_GET_LEADERBOARD_DICT())
							
							gameCurrentState = GS_LEADERBOARD_DISPLAY
						ENDIF
					ENDIF
																										
					// Hit accel to progress
					IF RA_INPUT_IS_ACCEL_JUST_PRESSED()
						TRIGGER_MUSIC_EVENT("ARCADE_RACE_N_CHASE_STOP_MUSIC")
						ARCADE_GAMES_HELP_TEXT_CLEAR()
						gameCurrentState = GS_STREAM_MENU_ASSETS
					ENDIF
					
				BREAK
				
				CASE GS_STREAM_MENU_ASSETS
					
					// Do not process rest of game if the assets have not loaded
					IF bContinue
						IF NOT HAVE_MENU_ASSETS_LOADED()
							bContinue = FALSE
						ENDIF
					ENDIF
						
					IF bContinue
						// Intialise Intro components	
						IF iPlayerSeatLocalID = 0 // Animaals only present in player 1 game
							IF eRoadArcade_ActiveGameType = RA_GT_CAR
							
								RA_INIT_DOG_MENU()
							ENDIF
							
							IF eRoadArcade_ActiveGameType = RA_GT_BIKE
								RA_INIT_CAT_MENU()
							ENDIF
							
							IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
								RA_INIT_DOG_MENU_TRUCK()
							ENDIF
						ENDIF
						
						IF ROAD_ARCADE_NET_IS_MULTIPLAYER_GAME_ACTIVE()
						
							// Safely clear bitmasks
							CLEAR_BITMASK_AS_ENUM(roadArcadePlayerBd[PARTICIPANT_ID_TO_INT()].iNetBitSet, RA_NET_PLAYER_TIME_OVER)
							roadArcadePlayerBd[PARTICIPANT_ID_TO_INT()].bFinished = FALSE
						
						ENDIF
						
						ARCADE_GAMES_SOUND_PLAY_LOOP(ROAD_ARCADE_AUDIO_EFFECT_MENU_AMB_SEAGUL)
						ROAD_ARCADE_AUDIO_START_AUDIO_SCENE(ROAD_ARCADE_AUDIO_SCENE_MENU)
						gameCurrentState = GS_MUSIC_SELECT
						bContinue = FALSE
					ENDIF
					
				BREAK
				
				CASE GS_MUSIC_SELECT
				
					IF eRoadArcade_ActiveGameType != RA_GT_TRUCK
						ARCADE_GAMES_SOUND_PLAY_LOOP(ROAD_ARCADE_AUDIO_EFFECT_MENU_AMB_WAVE)
					ENDIF
									
					RA_UPDATE_SELECT(muMenuSelection.iSongSelection, ROAD_ARCADE_AUDIO_EFFECT_MENU_RADIO_NAV)
					
					IF eRoadArcade_ActiveGameType = RA_GT_CAR
						RA_DRAW_SONG_SCREEN()
					ENDIF
					
					IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
						RA_DRAW_SONG_SCREEN_TRUCK()
					ENDIF
					
					IF eRoadArcade_ActiveGameType = RA_GT_BIKE
						RA_DRAW_SONG_SCREEN_BIKE()
					ENDIF
					
					ARCADE_GAMES_HELP_TEXT_PRINT(ARCADE_GAMES_HELP_TEXT_ENUM_RNC_TOP_MENU, 15000)
										
					IF RA_INPUT_IS_ACCEL_JUST_PRESSED()
						
						gameCurrentState = GS_TRACK_SELECT
						
						IF ROAD_ARCADE_NET_IS_MULTIPLAYER_GAME_ACTIVE()
							IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
							
								roadArcadePlayerBd[PARTICIPANT_ID_TO_INT()].sPlayerID = "" //url:bugstar:6201154
								
								RESET_lbCurrentEntry()
							
								gameCurrentState = GS_MUTLIPLAYER_ENTER_TAG
							ENDIF
						ENDIF
						
						IF eRoadArcade_ActiveGameType = RA_GT_CAR
						OR eRoadArcade_ActiveGameType = RA_GT_TRUCK
							IF iPlayerSeatLocalID = 0
								ARCADE_GAMES_SOUND_PLAY(ROAD_ARCADE_AUDIO_EFFECT_MENU_AMB_DOG)
							ENDIF
						ENDIF
					ENDIF
					
				BREAK
				
				CASE GS_TRACK_SELECT
					RA_UPDATE_SELECT(muMenuSelection.iTrackSelection)
						
					IF eRoadArcade_ActiveGameType = RA_GT_CAR
						RA_DRAW_TRACK_SCREEN()
					ENDIF
					
					IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
						RA_DRAW_TRACK_SCREEN_TRUCK()
					ENDIF
					
					IF eRoadArcade_ActiveGameType = RA_GT_BIKE
						RA_DRAW_TRACK_SCREEN_BIKE()
					ENDIF
										
					IF ROAD_ARCADE_NET_IS_MULTIPLAYER_GAME_ACTIVE()
					
						IF roadArcadeServerBd.stageToStart != STAGE_INVALID
							roadArcadePlayerBd[PARTICIPANT_ID_TO_INT()].sPlayerID = "" //url:bugstar:6201154
							
							RESET_lbCurrentEntry()
							
							gameCurrentState = GS_MUTLIPLAYER_ENTER_TAG
						ENDIF
																	
						IF RA_INPUT_IS_ACCEL_JUST_PRESSED()
						AND roadArcadeServerBd.stageToStart = STAGE_INVALID
							roadArcadeServerBd.stageToStart = ROAD_ARCADE_GET_FIRST_STAGE_FROM_SELECT()
							roadArcadePlayerBd[PARTICIPANT_ID_TO_INT()].sPlayerID = "" //url:bugstar:6201154
							
							RESET_lbCurrentEntry()
							
							gameCurrentState = GS_MUTLIPLAYER_ENTER_TAG
						ENDIF
					
					ELSE // Non-multiplayer game
						IF RA_INPUT_IS_ACCEL_JUST_PRESSED(ROAD_ARCADE_AUDIO_EFFECT_ENTER_GAME)
							ARCADE_GAMES_HELP_TEXT_CLEAR()
							gameCurrentState = GS_STREAM_RACE_ASSETS
							RELEASE_MENU_ASSETS()
						ENDIF
					ENDIF
			
				BREAK
				
				CASE GS_MULTIPLAYER_STREAM_CHALLENGER_ASSETS
					
					IF NOT ROAD_ARCADE_HAVE_TITLE_ASSETS_LOADED(stgCurrentStage, pvPlayer.sSpriteDict, TRUE)
						bContinue = FALSE
					ENDIF
					
					CDEBUG1LN(DEBUG_MINIGAME, "GS_MULTIPLAYER_STREAM_CHALLENGER_ASSETS - bContinue: ", bContinue)
					
					IF bContinue
						IF eRoadArcade_ActiveGameType = RA_GT_CAR
							ARCADE_GAMES_SOUND_PLAY(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_START_CAR)
						ENDIF
						
						IF eRoadArcade_ActiveGameType = RA_GT_BIKE
							ARCADE_GAMES_SOUND_PLAY(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_START_BIKE)
						ENDIF
						
						IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
							ARCADE_GAMES_SOUND_PLAY(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_START_TRUCK)
						ENDIF
						
						gameCurrentState = GS_MULTIPLAYER_CHALLENGER_SCREEN
						
						bContinue = FALSE
					ENDIF
				
				BREAK
				
				CASE GS_MULTIPLAYER_CHALLENGER_SCREEN
				
					ROAD_ARCADE_TITLE_UPDATE(TRUE)
																										
					// Hit accel to progress
					IF RA_INPUT_IS_ACCEL_JUST_PRESSED()
						TRIGGER_MUSIC_EVENT("ARCADE_RACE_N_CHASE_STOP_MUSIC")
							
						ROAD_ARCADE_RELEASE_ALL_SOUNDS()
																													
						gameCurrentState = GS_STREAM_MENU_ASSETS
					ENDIF
				
				BREAK
				
				CASE GS_MUTLIPLAYER_ENTER_TAG
									
					IF eRoadArcade_ActiveGameType = RA_GT_CAR
						ROAD_ARCADE_NET_DRAW_TAG_ENTRY()
					ENDIF
					
					IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
						ROAD_ARCADE_NET_DRAW_TAG_ENTRY_TRUCK()
					ENDIF
					
					IF eRoadArcade_ActiveGameType = RA_GT_BIKE
						ROAD_ARCADE_NET_DRAW_TAG_ENTRY_BIKE()
					ENDIF
											
					IF ROAD_ARCADE_NET_TAG_ENTRY(lbCurrentEntry, PARTICIPANT_ID_TO_INT())
					
						gameCurrentState = GS_MULTIPLAYER_WAIT_FOR_NETWORK
						
					ENDIF
									
				BREAK
				
				CASE GS_MULTIPLAYER_WAIT_FOR_NETWORK
				
					IF eRoadArcade_ActiveGameType = RA_GT_CAR
						RA_DRAW_TRACK_SCREEN(true)
					ENDIF
					
					IF eRoadArcade_ActiveGameType = RA_GT_BIKE
						RA_DRAW_TRACK_SCREEN_BIKE(true)
					ENDIF
					
					IF eRoadArcade_ActiveGameType = RA_GT_TRUCK
						RA_DRAW_TRACK_SCREEN_TRUCK(true)
					ENDIF
					
					IF ROAD_ARCADE_NET_GET_SERVER_STATE() = MULTI_SERVER_INGAME
						RELEASE_MENU_ASSETS()
						ARCADE_GAMES_HELP_TEXT_CLEAR()
						gameCurrentState = GS_STREAM_RACE_ASSETS
					ENDIF
															
				BREAK
			
				CASE GS_STREAM_RACE_ASSETS						

					IF NOT HAVE_INGAME_ASSETS_LOADED(ROAD_ARCADE_GET_FIRST_STAGE_FROM_SELECT(), pvPlayer)
						CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugRoadArcade, "Waiting on IN-GAME assets to Load")
						bContinue = FALSE
					ENDIF
					
					IF bContinue
						gameCurrentState = GS_RACE_SETUP
						ARCADE_GAMES_HELP_TEXT_PRINT(ARCADE_GAMES_HELP_TEXT_ENUM_RNC_DRIVING_CONTROLS, 10000)
						// Start up anim
						SET_ROAD_ARCADE_ANIM_STATE(pvPlayer, RA_PLAYER_PED_ANIM_START_GAME)
					ENDIF
				BREAK
							
				CASE GS_RACE_SETUP
					ROAD_PROTOTYPE_RACE_SETUP()
					ROAD_PROTOTYPE_MAIN_UPDATE()
					ROAD_ARCADE_AUDIO_START_AUDIO_SCENE(ROAD_ARCADE_AUDIO_SCENE_MAIN_GAME)
					gameCurrentState = GS_MAIN_GAME_UPDATE
				BREAK
				
				CASE GS_MAIN_GAME_UPDATE
					ROAD_PROTOTYPE_MAIN_UPDATE()
					
	#IF IS_DEBUG_BUILD
					// Debug quit to the leadboard entry stage!
					IF IS_KEYBOARD_KEY_PRESSED(KEY_K)
						RELEASE_INGAME_ASSETS()
						
						ROAD_ARCADE_RELEASE_ALL_SOUNDS()
						
						ROAD_ARCADE_GO_TO_LEADERBOARD(pvPlayer)
					ENDIF
					
					IF IS_KEYBOARD_KEY_PRESSED(KEY_J)
						stgFinalStage = stgCurrentStage
					ENDIF
					
					IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_H)
						fGameTimerElapsed -= 100000
					ENDIF
	#ENDIF
					
				BREAK
				
				
				CASE GS_CLEANUP_GAME_ASSETS
								
					RELEASE_INGAME_ASSETS()
					
					REQUEST_STREAMED_TEXTURE_DICT(ROAD_ARCADE_GET_LEADERBOARD_DICT())
																
					IF NOT ROAD_ARCADE_NET_IS_MULTIPLAYER_GAME_ACTIVE()
						
						ROAD_ARCADE_ASIGN_FINAL_TELEMETRY_DATA(pvPlayer)				
						gameCurrentState = GS_LEADERBOARD_NAME_ENTRY
						
					ELSE
						gameCurrentState = GS_MULTIPLAYER_RESULTS_SCREEN
					ENDIF
									
				BREAK
				
					
				CASE GS_LEADERBOARD_NAME_ENTRY
					
					IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED(ROAD_ARCADE_GET_LEADERBOARD_DICT())
						bContinue = false
					ENDIF
				
					IF bContinue
					
						bContinue = (pvPlayer.iPlayerScore = 0 | sAGLeaderboardData.sLeaderboard[ciCASINO_ARCADE_LEADERBOARD_POSITIONS-1].iScore > pvPlayer.iPlayerScore)
							
						IF NOT bContinue
							ARCADE_GAMES_LEADERBOARD_PROCESS_EVENTS()
						ENDIF
											
						// Draw the background images
						RA_DRAW_SCORE_ENTRY_BACKDROP(bContinue)
						
						// Out of ranking screen.
						IF (bContinue)

							ROAD_ARCADE_DRAW_SCORE(pvPlayer.iPlayerScore, INIT_VECTOR_2D(816,892))
	
							RA_WRITE_STRING_USING_TEXT_SPRITES("OUT OF RANKING", INIT_VECTOR_2D(752,540), 1, TRUE)	
							RA_WRITE_STRING_USING_TEXT_SPRITES("HARD LUCK", INIT_VECTOR_2D(838,680), 1, TRUE)	
							
							IF RA_INPUT_IS_ACCEL_JUST_PRESSED()
								ARCADE_GAMES_HELP_TEXT_CLEAR()
																
								gameCurrentState = GS_LEADERBOARD_DISPLAY
							ENDIF
														
						ELSE // Score is valid!
						
							ARCADE_GAMES_LEADERBOARD_PROCESS_ENTRY(pvPlayer.iPlayerScore)
							ARCADE_GAMES_LEADERBOARD_DRAW(iPlayerSeatLocalID = 1)
							
							IF !sAGLeaderboardData.bEditing
								ARCADE_GAMES_HELP_TEXT_CLEAR()

								gameCurrentState = GS_LEADERBOARD_DISPLAY
							ENDIF
						ENDIF						
					ENDIF
				BREAK
				
				CASE GS_LEADERBOARD_DISPLAY
					IF RA_DRAW_SCORE_TABLE()
						SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(ROAD_ARCADE_GET_LEADERBOARD_DICT())
						ARCADE_GAMES_HELP_TEXT_CLEAR()
						gameCurrentState = GS_CLEANUP_TO_TITLE
					ENDIF
				BREAK
												
				// MP exclusive stages
				CASE GS_MULTIPLAYER_RESULTS_SCREEN 
				
					IF ROAD_ARCADE_NET_DID_LOCAL_PLAYER_WIN(PARTICIPANT_ID_TO_INT())
						ARCADE_GAMES_SOUND_PLAY_ONCE(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_WIN)
					ELSE
						ARCADE_GAMES_SOUND_PLAY_ONCE(ROAD_ARCADE_AUDIO_EFFECT_PLAYER_LOSE)
					ENDIF
				
					IF ROAD_ARCADE_NET_DRAW_MULTIPLAYER_RESULTS()
						
						// Clear so decision can be made again
						CLEAR_BITMASK_AS_ENUM(roadArcadePlayerBd[PARTICIPANT_ID_TO_INT()].iNetBitSet, RA_NET_PLAYER_REMATCH_AGREED)
						CLEAR_BITMASK_AS_ENUM(roadArcadePlayerBd[PARTICIPANT_ID_TO_INT()].iNetBitSet, RA_NET_PLAYER_REMATCH_DECLINED)
						CLEAR_BITMASK_AS_ENUM(roadArcadePlayerBd[PARTICIPANT_ID_TO_INT()].iNetBitSet, RA_NET_PLAYER_TIME_OVER)
							
						// Iterate the award counter
						IF ROAD_ARCADE_NET_DID_LOCAL_PLAYER_WIN(PARTICIPANT_ID_TO_INT())
										
							CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugRoadArcade, "LOCAL PLAYER WON! Increment award counter by 1.")
														
							// If player won iterate the victory counter
							INCREMENT_BY_MP_INT_CHARACTER_AWARD(MP_AWARD_RACECHAMP, 1)
							
							// Dave W - url:bugstar:6206616 - Don't need to give xp in this script as its handled automatically by the award system
							// Ryan E - url:bugstar:6206781 - Need to set bit for each award tier, not just platinum
							IF GET_MP_INT_CHARACTER_AWARD(MP_AWARD_RACECHAMP) = g_sMPTunables.iARCADE_RACECHAMP_BRONZE_SCORE
								SET_BITMASK_AS_ENUM(sRoadArcadeTelemetry.challenges, RA_CHALLENGE_RACECHAMP_BRONZE)
								//GIVE_LOCAL_PLAYER_XP(eXPTYPE_STANDARD, "", XPTYPE_AWARDS, XPCATEGORY_BRONZE_AWARD, g_sMPtunables.iARCADE_RACE_CHAMP_BRONZE_RP)
							ENDIF
							
							IF GET_MP_INT_CHARACTER_AWARD(MP_AWARD_RACECHAMP) = g_sMPTunables.iARCADE_RACECHAMP_SILVER_SCORE 
								SET_BITMASK_AS_ENUM(sRoadArcadeTelemetry.challenges, RA_CHALLENGE_RACECHAMP_SILVER)
								//GIVE_LOCAL_PLAYER_XP(eXPTYPE_STANDARD, "", XPTYPE_AWARDS, XPCATEGORY_SILVER_AWARD, g_sMPtunables.iARCADE_RACE_CHAMP_SILVER_RP)
							ENDIF
							
							IF GET_MP_INT_CHARACTER_AWARD(MP_AWARD_RACECHAMP) = g_sMPTunables.iARCADE_RACECHAMP_GOLD_SCORE
								SET_BITMASK_AS_ENUM(sRoadArcadeTelemetry.challenges, RA_CHALLENGE_RACECHAMP_GOLD)
								//GIVE_LOCAL_PLAYER_XP(eXPTYPE_STANDARD, "", XPTYPE_AWARDS, XPCATEGORY_GOLD_AWARD, g_sMPtunables.iARCADE_RACE_CHAMP_GOLD_RP)
							ENDIF

							IF GET_MP_INT_CHARACTER_AWARD(MP_AWARD_RACECHAMP) = g_sMPTunables.iARCADE_RACECHAMP_PLATINUM_SCORE
								SET_BITMASK_AS_ENUM(sRoadArcadeTelemetry.challenges, RA_CHALLENGE_RACECHAMP_PLAT)
							//	GIVE_LOCAL_PLAYER_XP(eXPTYPE_STANDARD, "", XPTYPE_AWARDS, XPCATEGORY_PLATINUM_AWARD, g_sMPtunables.iARCADE_RACE_CHAMP_PLATINUM_RP)
								ARCADE_CABINET_FLOW_NOTIFICATIONS_SET_SHOW_TSHIRT_HELP(ARCADE_GAME_TSHIRTS_ROAD_ARCADE_RACECHAMP)
							ENDIF
							
						ENDIF
											
						ROAD_ARCADE_ASIGN_FINAL_TELEMETRY_DATA(pvPlayer)
											
						gameCurrentState = GS_MULTIPLAYER_REMATCH
					ENDIF
				BREAK
				
				CASE GS_MULTIPLAYER_REMATCH
					ARCADE_GAMES_HELP_TEXT_PRINT_FOREVER(ARCADE_GAMES_HELP_TEXT_ENUM_RNC_NET_REMATCH)
					ROAD_ARCADE_NET_DRAW_REMATCH_SCREEN(PARTICIPANT_ID_TO_INT())
				
					IF RA_INPUT_IS_ACCEL_JUST_PRESSED()
						SET_BITMASK_AS_ENUM(roadArcadePlayerBd[PARTICIPANT_ID_TO_INT()].iNetBitSet, RA_NET_PLAYER_REMATCH_AGREED)
						gameCurrentState = GS_MULTIPLAYER_AWAITING_REMATCH
					ENDIF
						
					IF RA_INPUT_IS_BRAKE_JUST_PRESSED()
						SET_BITMASK_AS_ENUM(roadArcadePlayerBd[PARTICIPANT_ID_TO_INT()].iNetBitSet, RA_NET_PLAYER_REMATCH_DECLINED)
						CLEAR_BITMASK_AS_ENUM(roadArcadePlayerBd[PARTICIPANT_ID_TO_INT()].iNetBitSet, RA_NET_PLAYER_REMATCH_AGREED) // Prevent double register.
					ENDIF
						
					IF ROAD_ARCADE_NET_DID_PLAYER_DROP_OUT()
					OR ROAD_ARCADE_NET_REMATCH_DECLINED()
						CLEAR_BITMASK_AS_ENUM(roadArcadePlayerBd[PARTICIPANT_ID_TO_INT()].iNetBitSet, RA_NET_GAME_INITIALISED)
						CLEAR_BITMASK_AS_ENUM(roadArcadePlayerBd[PARTICIPANT_ID_TO_INT()].iNetBitSet, RA_NET_PLAYER_CHALLENGE)
						SETTIMERA(0)
						gameCurrentState = GS_MULTIPLAYER_REMATCH_DECLINED
					ENDIF
									
				BREAK
				
				CASE GS_MULTIPLAYER_AWAITING_REMATCH
				
					ROAD_ARCADE_NET_DRAW_REMATCH_SCREEN(PARTICIPANT_ID_TO_INT(), TRUE) // Condition controls if it's awaiting version
				
					IF ROAD_ARCADE_NET_DID_PLAYER_DROP_OUT()
					OR ROAD_ARCADE_NET_REMATCH_DECLINED()
					OR ROAD_ARCADE_NET_GET_SERVER_STATE() = MULTI_SERVER_SOLO
						SETTIMERA(0)
						gameCurrentState = GS_MULTIPLAYER_REMATCH_DECLINED
					ENDIF
				
					IF ROAD_ARCADE_NET_HAVE_PLAYERS_CONFIRMED_REMATCH()
						REQUEST_STREAMED_TEXTURE_DICT(sDICT_SelectMenu)
						
						ROAD_ARCADE_RELEASE_ALL_SOUNDS()
						TRIGGER_MUSIC_EVENT("ARCADE_RACE_N_CHASE_STOP_MUSIC")
												
						gameCurrentState = GS_STREAM_MENU_ASSETS
					ENDIF
									
				BREAK
											
				CASE GS_MULTIPLAYER_REMATCH_DECLINED
					IF ROAD_ARCADE_NET_DRAW_REMATCH_DENIED_SCREEN(ROAD_ARCADE_NET_DID_LOCAL_PLAYER_WIN(PARTICIPANT_ID_TO_INT()))
											
						CLEAR_BITMASK_AS_ENUM(roadArcadePlayerBd[PARTICIPANT_ID_TO_INT()].iNetBitSet, RA_NET_GAME_INITIALISED)
						CLEAR_BITMASK_AS_ENUM(roadArcadePlayerBd[PARTICIPANT_ID_TO_INT()].iNetBitSet, RA_NET_PLAYER_CHALLENGE)
								
						TRIGGER_MUSIC_EVENT("ARCADE_RACE_N_CHASE_STOP_MUSIC")
											
						gameCurrentState = GS_CLEANUP_TO_TITLE
					ENDIF
				BREAK
				
				CASE GS_STREAM_MULTIPLAYER_LOST_CONNECTION_ASSETS
					REQUEST_STREAMED_TEXTURE_DICT(ROAD_ARCADE_GET_LEADERBOARD_DICT())
					SETTIMERA(0) // Reset the timer.
					gameCurrentState = GS_MULTIPLAYER_LOST_CONNECTION
				BREAK
				
				CASE GS_MULTIPLAYER_LOST_CONNECTION
				
					IF HAS_STREAMED_TEXTURE_DICT_LOADED(ROAD_ARCADE_GET_LEADERBOARD_DICT())
					AND ROAD_ARCADE_NET_LOST_CONNECTION_SCREEN()
					
						TRIGGER_MUSIC_EVENT("ARCADE_RACE_N_CHASE_STOP_MUSIC")
					
						gameCurrentState = GS_CLEANUP_TO_TITLE
					ENDIF
				
				BREAK
			
				CASE GS_CLEANUP_TO_TITLE
					ROAD_ARCADE_NET_RESET_LOCAL_PLAYER_RACE_VARIABLES()
					ROAD_ARCADE_TITLE_SETUP()
					gameCurrentState = GS_STREAM_TITLE_ASSETS
				BREAK
			
			ENDSWITCH
			
			ROAD_ARCADE_NET_SET_PARTICIPANT_GAME_STATE(PARTICIPANT_ID_TO_INT(), gameCurrentState)
			
			// Challenger icon
			IF ROAD_ARCADE_NET_IS_MULTIPLAYER_AVAILABLE()
			AND NOT ROAD_ARCADE_NET_IS_MULTIPLAYER_GAME_ACTIVE()
			AND ROAD_ARCADE_NET_GET_SERVER_STATE() = MULTI_SERVER_SOLO
			AND gameCurrentState = GS_MAIN_GAME_UPDATE
				
				// Display an on-screen icon if a challenger is present.
				ROAD_ARCADE_NET_DRAW_CHALLENGER_ICON()
					
			ENDIF
			
			// Facade and Arcade borders
			IF gameCurrentState != GS_INIT
			AND gameCurrentState != GS_STREAM_OVERLAYS // Asserts if we don't use state checks
			AND HAS_STREAMED_TEXTURE_DICT_LOADED(sDICT_ScreenOverlay)
			
				IF HAS_STREAMED_TEXTURE_DICT_LOADED("MPArcadeGamesFX00")
				AND HAS_STREAMED_TEXTURE_DICT_LOADED("MPArcadeGamesFX01")
				AND HAS_STREAMED_TEXTURE_DICT_LOADED("MPArcadeGamesFX02")
				AND HAS_STREAMED_TEXTURE_DICT_LOADED("MPArcadeGamesFX03")
				AND HAS_STREAMED_TEXTURE_DICT_LOADED("MPArcadeGamesFX04")
				AND HAS_STREAMED_TEXTURE_DICT_LOADED("MPArcadeGamesFX05")
					ARCADE_GAMES_POSTFX_DRAW()
				ENDIF
				
				ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_ScreenOverlay, "RA_INNER_FRAME_CLEAR", INIT_VECTOR_2D((cfBASE_SCREEN_WIDTH/2.0)+2, (cfBASE_SCREEN_HEIGHT/2.0)-3), INIT_VECTOR_2D(1352, 1012), 0.0, rgbaOriginal)
				ARCADE_DRAW_PIXELSPACE_SPRITE(sDICT_ScreenOverlay, "RA_OUTER_FRAME", INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2.0, cfBASE_SCREEN_HEIGHT/2.0), INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH, cfBASE_SCREEN_HEIGHT), 0.0, rgbaFrameColour)
			ENDIF

			IF ROAD_ARCADE_IS_QUIT_BEING_HELD()
				IF !bCloseGame
					pvPlayer.ae_PedAnimData.bHoldLastFrame = FALSE
					pvPlayer.ae_PedAnimData.bLoopAnim = FALSE
					
					IF pvPlayer.ae_PedAnimData.ePreviousAnimClip = ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_RACING_A
					OR pvPlayer.ae_PedAnimData.ePreviousAnimClip = ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_RACING_B
					OR pvPlayer.ae_PedAnimData.ePreviousAnimClip = ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_RACING_C
					OR pvPlayer.ae_PedAnimData.ePreviousAnimClip = ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_RACING_D
					OR pvPlayer.ae_PedAnimData.ePreviousAnimClip = ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_RACING_E
					OR pvPlayer.ae_PedAnimData.ePreviousAnimClip = ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_RACING_F
					OR pvPlayer.ae_PedAnimData.ePreviousAnimClip = ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_RACING_G
					OR pvPlayer.ae_PedAnimData.ePreviousAnimClip = ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_RACING_H
					OR pvPlayer.ae_PedAnimData.ePreviousAnimClip = ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_RACING
					OR pvPlayer.ae_PedAnimData.ePreviousAnimClip = ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_BIKE_RACING_V2
					OR pvPlayer.ae_PedAnimData.ePreviousAnimClip = ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_BIKE_RACING_V3
						pvPlayer.ae_PedAnimData.bHoldLastFrame = TRUE
						PLAY_ARCADE_CABINET_ANIMATION(pvPlayer.ae_PedAnimData, ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_RACE_TO_IDLE)
					ELSE
						PLAY_ARCADE_CABINET_ANIMATION(pvPlayer.ae_PedAnimData, ARCADE_CABINET_ANIM_CLIP_EXIT)	
					ENDIF
					bCloseGame = TRUE
				ENDIF	
			ELIF IS_LOCAL_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR()
			OR SHOULD_KICK_PLAYER_FROM_ANY_ARCADE_GAME()
				bCloseGame = TRUE
			ELSE
				// PRocess player ped animations
				ROAD_ARCADE_PROCESS_PLAYER_PED_ANIMS(pvPlayer)
			ENDIF
							
		ENDIF
		
		IF NOT NETWORK_IS_GAME_IN_PROGRESS()
			CLEANUP_SCRIPT()
		ENDIF
		
		IF bCloseGame
			CLEANUP_SCRIPT()
		ENDIF

		SWITCH ROAD_ARCADE_NET_GET_SERVER_STATE()
			// Wait untill the server gives the all go before moving on
			CASE MULTI_SERVER_INIT
				ROAD_ARCADE_NET_SET_SERVER_STATE(MULTI_SERVER_SOLO)
			BREAK
			
			CASE MULTI_SERVER_SOLO
				// Waiting for 2nd cabinet to be entered.
				
				IF ROAD_ARCADE_NET_IS_MULTIPLAYER_AVAILABLE()

					IF NOT ARCADE_GAMES_HELP_TEXT_IS_THIS_BEING_DISPLAYED(ARCADE_GAMES_HELP_TEXT_ENUM_RNC_DRIVING_CONTROLS)
						IF NOT IS_BITMASK_AS_ENUM_SET(roadArcadePlayerBd[PARTICIPANT_ID_TO_INT()].iNetBitSet, RA_NET_PLAYER_CHALLENGE)
							ARCADE_GAMES_HELP_TEXT_PRINT(ARCADE_GAMES_HELP_TEXT_ENUM_RNC_NET_CHALLENGE_READY, 7500)
							
							// If we have been challenged by a Rival display this help instead.
							IF IS_BITMASK_AS_ENUM_SET(roadArcadePlayerBd[ROAD_ARCADE_NET_GET_RIVAL_PARTICIPANT_ID(PARTICIPANT_ID_TO_INT())].iNetBitSet, RA_NET_PLAYER_CHALLENGE)
							
								IF ARCADE_GAMES_HELP_TEXT_HAS_BEEN_DISPLAYED(ARCADE_GAMES_HELP_TEXT_ENUM_RNC_NET_CHALLENGE_READY)
									ARCADE_GAMES_HELP_TEXT_PRINT(ARCADE_GAMES_HELP_TEXT_ENUM_RNC_NET_CHALLENGE, 9000)		
								ENDIF
							
							ENDIF
							
						ENDIF
						
					ENDIF
					
					// Prompt to start the race
					ROAD_ARCADE_HANDLE_SCALEFORM_BUTTONS()
						
					IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE)
						
						IF NOT IS_BITMASK_AS_ENUM_SET(roadArcadePlayerBd[PARTICIPANT_ID_TO_INT()].iNetBitSet, RA_NET_PLAYER_CHALLENGE)
							CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugRoadArcade, "ROAD_ARCADE_NET_IS_MULTIPLAYER_GAME_ACTIVE - Setting Player ", PARTICIPANT_ID_TO_INT()," Challenger bitmask.")
							ARCADE_GAMES_HELP_TEXT_CLEAR()
							ARCADE_GAMES_HELP_TEXT_PRINT(ARCADE_GAMES_HELP_TEXT_ENUM_RNC_NET_CHALLENGE_MADE, 7500)
							SET_BITMASK_AS_ENUM(roadArcadePlayerBd[PARTICIPANT_ID_TO_INT()].iNetBitSet, RA_NET_PLAYER_CHALLENGE)
						ENDIF
													
					ENDIF
																	
					IF IS_BITMASK_AS_ENUM_SET(roadArcadePlayerBd[PARTICIPANT_ID_TO_INT()].iNetBitSet, RA_NET_PLAYER_CHALLENGE)						
					AND IS_BITMASK_AS_ENUM_SET(roadArcadePlayerBd[ROAD_ARCADE_NET_GET_RIVAL_PARTICIPANT_ID(PARTICIPANT_ID_TO_INT())].iNetBitSet, RA_NET_PLAYER_CHALLENGE)
						ARCADE_GAMES_HELP_TEXT_CLEAR()
						
						// Set up the Match ID.
						IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
							roadArcadeServerBd.iMatchId = GET_CLOUD_TIME_AS_INT()
						ENDIF
						
						iBroadcastFrameCounter = 0
						ROAD_ARCADE_NET_SET_SERVER_STATE(MULTI_SERVER_CHALLENGER)
					ENDIF
					
				ENDIF
			BREAK
			
			CASE MULTI_SERVER_CHALLENGER
				
				IF ROAD_ARCADE_NET_ARE_PLAYERS_IN_WAIT_STATE()	
					ROAD_ARCADE_NET_SET_SERVER_STATE(MULTI_SERVER_INGAME)
				ENDIF

				IF ROAD_ARCADE_NET_DID_PLAYER_DROP_OUT()
					ROAD_ARCADE_NET_SET_SERVER_STATE(MULTI_SERVER_LOST_CONNECTION)
				ENDIF
			BREAK
			
			// The game stage the local player is placed when we want him to leave a mission
			CASE MULTI_SERVER_INGAME

				CDEBUG1LN(DEBUG_MINIGAME, sPrefixDebugRoadArcade, "MULTI_SERVER_INGAME running")

				ROAD_ARCADE_NET_SET_PARTICIPANT_PROGRESS(pvPlayer, PARTICIPANT_ID_TO_INT())
				ROAD_ARCADE_NET_SET_PARTICIPANT_STAGE(PARTICIPANT_ID_TO_INT(), stgCurrentStage)
				//roadArcadePlayerBd[PARTICIPANT_ID_TO_INT()].sCurrentStage = stgCurrentStage
							
				// Update the Rival object progress
				rpvRival.fZDrawAt = roadArcadePlayerBd[ROAD_ARCADE_NET_GET_RIVAL_PARTICIPANT_ID(PARTICIPANT_ID_TO_INT())].fPlayerProgress
			
				// Host player wins by time
				IF ROAD_ARCADE_NET_IS_MULTIPLAYER_RACE_OVER()
					ROAD_ARCADE_NET_SET_SERVER_STATE(MULTI_SERVER_REMATCH)
				ENDIF
				
				IF ROAD_ARCADE_NET_DID_PLAYER_DROP_OUT()
					ROAD_ARCADE_NET_SET_SERVER_STATE(MULTI_SERVER_LOST_CONNECTION)
				ENDIF
				
			BREAK
			
			CASE MULTI_SERVER_REMATCH
				IF ROAD_ARCADE_NET_HAVE_PLAYERS_CONFIRMED_REMATCH()
					roadArcadeServerBd.stageToStart = STAGE_INVALID // Set stage to start invalid so it can be selected again.
					ROAD_ARCADE_NET_SET_SERVER_STATE(MULTI_SERVER_CHALLENGER)
				ENDIF
					
				IF ROAD_ARCADE_NET_DID_PLAYER_DROP_OUT()
				OR ROAD_ARCADE_NET_REMATCH_DECLINED()					
					ROAD_ARCADE_NET_SET_SERVER_STATE(MULTI_SERVER_SOLO)
				ENDIF
			BREAK
			
			//Cleans up then terminates the mission
			CASE MULTI_SERVER_LOST_CONNECTION				
				ROAD_ARCADE_NET_SET_SERVER_STATE(MULTI_SERVER_SOLO)
			BREAK
			
		ENDSWITCH
				
		IF NOT ARCADE_GAMES_HELP_TEXT_SHOULD_ALLOW_THIS_FRAME()
			HIDE_HELP_TEXT_THIS_FRAME()
		ENDIF
		
		// Gameplay time tracker.
		fRunTimer += (0.0+@1000.0)
				
	ENDWHILE
	
ENDSCRIPT


