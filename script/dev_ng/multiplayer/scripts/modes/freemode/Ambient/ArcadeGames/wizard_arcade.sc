USING "globals.sch"
USING "net_events.sch"
USING "net_wait_zero.sch"
USING "net_mission.sch"
USING "wizard_arcade_main.sch"

#IF IS_DEBUG_BUILD
	WIDGET_GROUP_ID widgetMinigame
#ENDIF

/// PURPOSE:
///    Do necessary pre game start initialization.
PROC PROCESS_PRE_GAME(MP_MISSION_DATA missionScriptArgs)
	
	missionScriptArgs = missionScriptArgs
	NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()
	
ENDPROC

SCRIPT(MP_MISSION_DATA missionScriptArgs)
	
	missionScriptArgs.mdID.idMission = eAM_TWS_ARCADE_CABINET
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		PROCESS_PRE_GAME(missionScriptArgs)
	ENDIF
		
	WHILE TRUE
		MP_LOOP_WAIT_ZERO()
		
		IF NETWORK_IS_GAME_IN_PROGRESS()
		
#IF IS_DEBUG_BUILD
			IF NOT DOES_WIDGET_GROUP_EXIST(widgetMinigame)
				widgetMinigame = START_WIDGET_GROUP("The Wizards Sleeve Arcade Cabinet Minigame")
				STOP_WIDGET_GROUP()
				TWS_DEBUG_SET_PARENT_WIDGET_GROUP(widgetMinigame)
			ENDIF
#ENDIF
		
			PROCESS_TWS_ARCADE()
		ELSE
			TWS_SET_CLIENT_STATE(TWS_ARCADE_CLIENT_STATE_CLEANUP)
		ENDIF
		
		IF IS_LOCAL_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR()
		OR SHOULD_KICK_PLAYER_FROM_ANY_ARCADE_GAME()

			sTWSData.sWizardAnimEvent.bHoldLastFrame = FALSE
			sTWSData.sWizardAnimEvent.bLoopAnim = FALSE
			PLAY_ARCADE_CABINET_ANIMATION(sTWSData.sWizardAnimEvent, ARCADE_CABINET_ANIM_CLIP_EXIT)
			TWS_SET_CLIENT_STATE(TWS_ARCADE_CLIENT_STATE_CLEANUP)
		ENDIF
		
		#IF IS_DEBUG_BUILD
			IF IS_KEYBOARD_KEY_PRESSED(KEY_F)
				TWS_SET_CLIENT_STATE(TWS_ARCADE_CLIENT_STATE_CLEANUP)
			ENDIF
		#ENDIF
	ENDWHILE

ENDSCRIPT