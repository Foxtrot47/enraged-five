//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        AM_MP_ARCADE_STRENGTH_TEST.sc																				//
// Description: This is the implementation of strength test arcade game. This script will mange the strength test gameplay 	// 
// 				functionality. This script will wait for ready flag from arcade manager script (AM_MP_ARC_CAB_MANAGER)  	//
//				to start the game.							 																//
// Written by:  Online Technical Design: Ata Tabrizi, Scott Ranken															//
// Date:  		28/08/2019																									//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_event.sch"
USING "net_include.sch"
USING "net_events.sch"
USING "net_wait_zero.sch"
USING "net_common_functions.sch"
USING "net_simple_interior.sch"
USING "net_render_targets.sch"

#IF IS_DEBUG_BUILD
USING "net_debug_log.sch"
#ENDIF

#IF FEATURE_SUMMER_2020
USING "net_arcade_cabinet.sch"
USING "arcade_games_help_text_flow.sch"

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ CONSTANTS ╞══════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

// Score
CONST_INT ciST_COUNTDOWN_TIMER_MS							5000
CONST_INT ciST_MAX_PROP_SCORE								1100

// High Score
CONST_INT ciST_SET_HIGHSCORE_TIMER_MS						1000
CONST_INT ciST_MAX_HIGHSCORE_FLASHES						6
CONST_INT ciST_HIGHSCORE_FLASH_TIMER_MS						500
CONST_INT ciST_ELEVEN_ELEVEN_REWARD_TOTAL					11
CONST_INT ciST_PERFECT_SCORE_LIMIT							825
CONST_INT ciST_GOOD_SCORE_LIMIT								550
CONST_INT ciST_AVERAGE_SCORE_LIMIT							275
CONST_INT ciST_VALIDATE_HIGHSCORE_TIME_MS					4000
CONST_INT ciST_DISPLAY_HIGHSCORE_DELAY_TIME_MS				1100
CONST_FLOAT cfST_DISPLAY_SCORE_TIME_SECONDS					2.0
CONST_FLOAT cfST_DISPLAY_HIGHSCORE_TIME_SECONDS				3.35

// Animation
CONST_INT ciST_MAX_BASHING_STAGES							4
CONST_FLOAT cfST_PED_CAPSULE_SIZE							2.2
CONST_FLOAT cfST_BASHING_STAGE_4							0.75
CONST_FLOAT cfST_BASHING_STAGE_3							0.5
CONST_FLOAT cfST_BASHING_STAGE_2							0.25
CONST_FLOAT cfST_IMPACT_PAD_SHAKE_MAX						150.0
CONST_FLOAT cfST_IMPACT_PAD_SHAKE_MAX_DURATION_MS			400.0
CONST_FLOAT cfST_IMPACT_PAD_SHAKE_MIN_DURATION_MS			250.0

// HUD Meter
CONST_INT ciST_MAX_HUD_METER_VALUE							50
CONST_INT ciST_MAX_HUD_METER_RESISTANCE_TIME_MS				5000
TWEAK_FLOAT tfST_HUB_METER_INCREASE_INCREMENTS				2.30
TWEAK_FLOAT tfST_SWING_ANIM_WEIGHT_INCREASE					0.003
TWEAK_FLOAT tfST_SWING_ANIM_WEIGHT_DECREASE					0.003

// Catch HUD Mechanic
CONST_INT ciST_SWING_CATCH_MAX_WAIT_TIMER_MS				5001
CONST_INT ciST_SWING_CATCH_MIN_WAIT_TIMER_MS				2000
CONST_INT ciST_MAX_HUD_METER_CATCH_TIME_MS					900
CONST_INT ciST_MIN_HUD_METER_CATCH_TIME_MS					500
CONST_INT ciST_SWING_DELAY_MS								250

// Speech
CONST_INT ciST_MAX_ATTRACT_TIME_MS							40000
CONST_INT ciST_MAX_EVENT_SPAM_TIMER_MS						1001
CONST_INT ciST_MAX_HYPE_SPEECH_TIMER_MS						1500
CONST_INT ciST_MAX_LEAVE_SPEECH_TIMER_MS					1000

// Audio
CONST_INT ciST_MAX_RESULT_SOUNDS							5
CONST_INT ciST_MAX_HIT_SOUNDS								3
CONST_FLOAT cfST_RESULT_SOUND_OFFSET_PHASE					0.02
CONST_FLOAT cfST_MAX_BASHING_SOUND_STAGES					9.0

// Render Target Score
CONST_FLOAT cfST_MAX_RT_SCORE_POSITION						0.5909
CONST_FLOAT cfST_MIN_RT_SCORE_POSITION						1.4101
CONST_FLOAT cfST_STARTING_Y_AXIS							1.5
CONST_FLOAT cfST_SCORE_Y_SEGMENT							0.0909

// Anim Weights & Phases
CONST_INT ciST_ANIM_BLEND_SAFETY_TIME_MS					5000
CONST_FLOAT cfST_ATTACH_HAMMER_ANIM_PHASE_TIME				0.26
CONST_FLOAT cfST_ANIM_EXIT_PHASE							0.99
CONST_FLOAT cfST_MAX_SWING_WEIGHT							0.99
CONST_FLOAT cfST_MIN_SWING_WEIGHT							0.01
CONST_FLOAT cfST_FINAL_ANIM_WEIGHT_BUFFER					0.08
CONST_FLOAT cfST_CAMERA_SWING_LERP_BUFFER					0.11

// Camera
TWEAK_INT tiST_CAMERA_FP_INVISIBLE_TIME_MS					225
TWEAK_INT tiST_CAMERA_STARTING_LERP_TIME_MS					2500
TWEAK_INT tiST_CAMERA_ENDING_LERP_TIME_MS					2000
TWEAK_INT tiST_CAMERA_MAX_SHAKE_INTERVAL_TIME_MS			116
TWEAK_INT tiST_CAMERA_MIN_SHAKE_INTERVAL_TIME_MS			916
TWEAK_INT tiST_ANIM_FINISH_BLEND_TIME_MS_STAGE_1			2000
TWEAK_INT tiST_ANIM_FINISH_BLEND_TIME_MS_STAGE_2			2250
TWEAK_INT tiST_ANIM_FINISH_BLEND_TIME_MS_STAGE_3			2500
TWEAK_INT tiST_ANIM_FINISH_BLEND_TIME_MS_STAGE_4			2750
TWEAK_INT tiST_ANIM_FINISH_BLEND_TIME_MS_STAGE_5			3250
TWEAK_FLOAT tfST_CAMERA_MAX_SHAKE							0.01
TWEAK_FLOAT tfST_CAMERA_MAX_IMPACT_SHAKE					0.15
TWEAK_FLOAT tfST_CAMERA_ANIM_WEIGHT_OFFSET_STAGE_1			0.4
TWEAK_FLOAT tfST_CAMERA_ANIM_WEIGHT_OFFSET_STAGE_2			0.75
TWEAK_FLOAT tfST_CAMERA_ANIM_WEIGHT_OFFSET_STAGE_3			1.0
TWEAK_FLOAT tfST_CAMERA_ANIM_WEIGHT_OFFSET_STAGE_4			1.25
TWEAK_FLOAT tfST_CAMERA_ANIM_WEIGHT_OFFSET_STAGE_5			1.5
TWEAK_FLOAT tfST_CAMERA_MAX_LERP_ALPHA						0.98
TWEAK_FLOAT tfST_CAMERA_HIDE_PLAYER_LERP_ALPHA				0.65

// Scaleform
TWEAK_FLOAT tfST_SWING_METER_SCREEN_X						0.190
TWEAK_FLOAT tfST_SWING_METER_SCREEN_Y						0.500
TWEAK_FLOAT tfST_SWING_METER_MIN_CATCH_VALUE				0.05
TWEAK_FLOAT tfST_SWING_METER_MAX_MULTIPLIER_NORM_START		0.300
TWEAK_FLOAT tfST_SWING_METER_MAX_MULTIPLIER_NORM_END		0.550
TWEAK_FLOAT tfST_SWING_METER_TOP_MULTIPLIER_NORM_START		0.551
TWEAK_FLOAT tfST_SWING_METER_TOP_MULTIPLIER_NORM_END		0.850
TWEAK_FLOAT tfST_SWING_METER_BOTTOM_MULTIPLIER_NORM_START	0.150
TWEAK_FLOAT tfST_SWING_METER_BOTTOM_MULTIPLIER_NORM_END		0.299
TWEAK_FLOAT tfST_SWING_METER_MAX_MULTIPLIER					2.0

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ BIT SETS ╞═══════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

// iBS
CONST_INT BS_ST_SET_HUD_METER_TIMER							0
CONST_INT BS_ST_SET_HUD_METER_PROGRESS						1
CONST_INT BS_ST_PERFORM_SCORE_SCALEFORM_MOVIE				2
CONST_INT BS_ST_HUD_METER_REACHED_MAX_LIMIT					3
CONST_INT BS_ST_SET_FINAL_HUD_METER_VALUE					4
CONST_INT BS_ST_PLAY_LEAVE_SPEECH							5
CONST_INT BS_ST_PLAY_HYPE_SPEECH							6
CONST_INT BS_ST_PLAY_POWERING_UP_SOUND						7
CONST_INT BS_ST_PLAY_BASHING_STAGE_SOUND					8
CONST_INT BS_ST_PLAY_RESULT_SOUND							9
CONST_INT BS_ST_BASHING_AUDIO_STAGE_0						10
CONST_INT BS_ST_BASHING_AUDIO_STAGE_1						11
CONST_INT BS_ST_BASHING_AUDIO_STAGE_2						12
CONST_INT BS_ST_BASHING_AUDIO_STAGE_3						13
CONST_INT BS_ST_BASHING_AUDIO_STAGE_4						14
CONST_INT BS_ST_BASHING_AUDIO_STAGE_5						15
CONST_INT BS_ST_BASHING_AUDIO_STAGE_6						16
CONST_INT BS_ST_BASHING_AUDIO_STAGE_7						17
CONST_INT BS_ST_SET_BLEND_SAFETY_TIMER						18
CONST_INT BS_ST_SET_CAMERA_LERP_VALUES						19
CONST_INT BS_ST_SET_CAMERA_HAMMER_IMPACT_VALUES				20
CONST_INT BS_ST_PLAY_START_BASHING_SOUND					21
CONST_INT BS_ST_PLAY_POWER_BAR_DROP_SOUND					22
CONST_INT BS_ST_LERP_DURING_CATCH_STAGE						23
CONST_INT BS_ST_SET_CAMERA_CATCH_STAGE_LERP_VALUES			24
CONST_INT BS_ST_HIGHSCORE_LIGHT_ON							25
CONST_INT BS_ST_HIGHSCORE_EVENT_SENT						26
CONST_INT BS_ST_SWING_METER_HIDE_BUTTON						27
CONST_INT BS_ST_STOP_POWER_LOOP_SOUND						28
CONST_INT BS_ST_SWING_DELAY_FINISHED						29
CONST_INT BS_ST_MOVE_NETWORK_INITIALISED					30
CONST_INT BS_ST_TRIGGER_MOVE_NETWORK_SWING_RESULT			31

// iBS2
CONST_INT BS2_ST_MOVE_NETWORK_CLIP_SET						0
CONST_INT BS2_ST_MOVE_NETWORK_ENABLE_COLLISION				1
CONST_INT BS2_ST_PLAY_STAGE_LOOP_1_FACIAL_ANIM				2
CONST_INT BS2_ST_PLAY_STAGE_LOOP_2_FACIAL_ANIM				3
CONST_INT BS2_ST_PLAY_STAGE_LOOP_3_FACIAL_ANIM				4
CONST_INT BS2_ST_PLAY_STAGE_LOOP_4_FACIAL_ANIM				5
CONST_INT BS2_ST_SET_ARCADE_PLAYER_CAPSULE_SIZE				6
CONST_INT BS2_ST_SET_CAMERA_EXIT_LERP						7
CONST_INT BS2_ST_TASK_MOVE_PLAYER_AWAY_FROM_MACHINE			8

//╒══════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════════╡ ENUMS ╞═════════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

ENUM STRENGTH_TEST_GAME_STATE
	ST_GAME_STATE_INVALID 								= -1,
	ST_GAME_STATE_INITIALISE 							= 0,
	ST_GAME_STATE_REQUEST_ASSETS,
	ST_GAME_STATE_ENTRY,
	ST_GAME_STATE_SWING_BASH,
	ST_GAME_STATE_SWING_CATCH,
	ST_GAME_STATE_PERFORM_SWING,
	ST_GAME_STATE_TOTAL
ENDENUM

ENUM STRENGTH_TEST_RENDER_TARGET
	ST_RT_INVALID 										= -1,
	ST_RT_HEADBOARD 									= 0,
	ST_RT_NECK,
	ST_RT_TOTAL
ENDENUM

ENUM STRENGTH_TEST_RENDER_TARGET_STATE
	ST_RT_STATE_INVALID 								= -1,
	ST_RT_STATE_REQUEST_HIGHSCORE 						= 0,
	ST_RT_STATE_LINK_RT,
	ST_RT_STATE_REQUEST_ASSETS,
	ST_RT_STATE_INIT_SCORES,
	ST_RT_STATE_SET_TIMER,
	ST_RT_STATE_START_TIMER,
	ST_RT_STATE_PERFORM_SCORE,
	ST_RT_STATE_SET_HIGHSCORE,
	ST_RT_STATE_DRAW,
	ST_RT_STATE_TOTAL
ENDENUM

ENUM STRENGTH_TEST_RENDER_TARGET_SCORE_STATE
	ST_RT_SCORE_STATE_INVALID = -1,
	ST_RT_SCORE_STATE_ASCEND = 0,
	ST_RT_SCORE_STATE_HOLD,
	ST_RT_SCORE_STATE_DESCEND,
	ST_RT_SCORE_STATE_TOTAL
ENDENUM

ENUM STRENGTH_TEST_CAMERAS
	ST_CAMERA_INVALID = -1,
	ST_CAMERA_STARTING,
	ST_CAMERA_STAGE_ONE,
	ST_CAMERA_STAGE_TWO,
	ST_CAMERA_STAGE_THREE,
	ST_CAMERA_STAGE_FOUR,
	ST_CAMERA_STAGE_FIVE,
	ST_CAMERA_MAX
ENDENUM

ENUM STRENGTH_TEST_CHALLENGE_BIT
	STRENGTH_TEST_CHALLENGE_BIT_NONE 			= 0,
	STRENGTH_TEST_CHALLENGE_BIT_CRANK_TO_ELEVEN	= BIT0,
	STRENGTH_TEST_CHALLENGE_BIT_ELEVEN_ELEVEN	= BIT1
ENDENUM

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ STRUCTS ╞════════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

STRUCT ServerBroadcastData
	INT iServerBS
ENDSTRUCT

STRUCT PlayerBroadcastData
	INT iLocalBS
ENDSTRUCT

STRUCT RENDER_TARGET_DATA
	INT iNeckRT											= -1
	INT iHeadboardRT									= -1
	INT iScore											= 0
	FLOAT fTargetY										= 1.5
	FLOAT fNeckScoreY									= 1.5
	FLOAT fHoldTimeSecs									= 0.0
	FLOAT fCountUpTimeSecs								= 0.0
	FLOAT fCountDownTimeSecs							= 0.0
	BOOL bSetScoreTimer									= FALSE
	SCALEFORM_INDEX scoreboardScaleformID				= NULL
	TIME_DATATYPE tdScoreTimer
	SCRIPT_TIMER stScoreHoldTimer
	STRENGTH_TEST_RENDER_TARGET_SCORE_STATE	eScoreState
ENDSTRUCT

STRUCT ANIMATION_DATA
	INT iStartTimeMS									= 0
	INT iAnimDurationMS									= 0
	FLOAT fSwingWeight									= 0.0
	ARCADE_CABINET_ANIM_DETAIL Details
	ARCADE_CABINET_ANIM_CLIPS eSwingClip				= ARCADE_CABINET_ANIM_CLIP_INVALID
ENDSTRUCT

STRUCT MOVE_NETWORK_DATA
	PLAYER_INDEX arcadePlayerID
	INT iClipSetHash									= 0
	INT iClipSetVariableHash							= 0
ENDSTRUCT

STRUCT HUD_DATA
	FLOAT fNormMeterFillValue							= 0.0
	FLOAT fNormMeterBarValue							= 1.0
	FLOAT fHUDMeterProgress								= 0.0
	FLOAT fCurrentHUDMeterProgress						= 0.0
	BOOL bDisplayHUD									= FALSE
	BOOL bEnableInput									= FALSE
	SCRIPT_TIMER stSwingDelay
	TIME_DATATYPE tdHUDMeterTimer
	SCALEFORM_INDEX siSwingMeter
ENDSTRUCT

STRUCT HIGHSCORE_DATA
	INT iHighScore										= -1
	INT iHighScoreFlashes								= 0
	BOOL bRenderHighScoreFlash							= TRUE
	BOOL bFlashHighscoreLight							= TRUE
	BOOL bHostChangedDuringGame							= FALSE
	SCRIPT_TIMER stHighScoreTimer
	SCRIPT_TIMER stValidateHighscoreTimer
	PLAYER_INDEX arcadeHost
ENDSTRUCT

STRUCT GAMEPLAY_DATA
	INT iPropScore										= 0
	INT iCatchTimeMS									= 0
	FLOAT fScore										= 0.0
	BOOL bCrankChallUnlockedThisFrame					= FALSE
	BOOL bElevenElevenChallUnlockedThisFrame			= FALSE
	SCRIPT_TIMER stCountdownTimer
	SCRIPT_TIMER stBlendSafetyTimer
ENDSTRUCT

STRUCT ARCADE_SPEECH_DATA
	INT iDisplaySlot									= -1
	INT iCachedDisplaySlot								= -1
	SCRIPT_TIMER stLeaveTimer
	SCRIPT_TIMER stAttractTimer
	ARCADE_CABINET_AI_CONTEXT_STRUCT eAIContext
ENDSTRUCT

STRUCT AUDIO_DATA
	INT iSoundID 										= -1
	INT iStageSoundID 									= -1
	INT iPowerloopSoundID 								= -1
	INT iAltSoundID										= -1
	INT iAudioBashingStage								= 0
ENDSTRUCT

STRUCT CAMERA_DATA
	INT iShakeIntervalTimeMS 							= 400
	INT iTimeUntilSwingMS								= 0
	INT iInterpolationType								= 9
	FLOAT fStartingFOV									= 50.0
	FLOAT fEndingFOV									= 50.0
	FLOAT fFPCamFOV										= 50.0
	FLOAT fFPCamPitch									= 0.0
	FLOAT fFPCamHeading									= 0.0
	BOOL bFPCamera										= FALSE
	BOOL bFPCameraEntryLerpSet							= FALSE
	BOOL bFPCameraExitLerpSet							= FALSE
	VECTOR vFPCamPos
	VECTOR vFPCamRot
	VECTOR vStartingCoords
	VECTOR vEndingCoords
	VECTOR vStartingRotation
	VECTOR vEndingRotation
	SCRIPT_TIMER stShakeTimer
	SCRIPT_TIMER stFPCameraLerpTimer
	TIME_DATATYPE tdCatchLerpTimer
	CAMERA_INDEX ciMainCamera
ENDSTRUCT

STRUCT STRENGTH_TEST_DATA
	OBJECT_INDEX cabinetObj								= NULL
	
	INT iBS
	INT iBS2
	INT iTempScore										= 0
	FLOAT fCabinetHeading
	VECTOR vCabinetCoords
	
	SCRIPT_TIMER stPlayDurationTimer
	
	HUD_DATA HUDData
	AUDIO_DATA AudioData
	CAMERA_DATA CameraData
	ARCADE_SPEECH_DATA SpeechData
	ANIMATION_DATA AnimData
	RENDER_TARGET_DATA RTData
	GAMEPLAY_DATA GameplayData
	HIGHSCORE_DATA HighScoreData
	MOVE_NETWORK_DATA MoveNetworkData
	
	STRENGTH_TEST_GAME_STATE eGameState
	STRENGTH_TEST_RENDER_TARGET_STATE eRTState 
	ARCADE_CABINETS eArcadeCabinetLocateType
	ARCADE_CABINET_CHILD_SCRIPT_CLIENT_STATE eState
ENDSTRUCT

#IF IS_DEBUG_BUILD
STRUCT DEBUG_CAMERA_DATA
	BOOL bRenderCam										= FALSE
	FLOAT fFOV											= 50.0
	VECTOR vCoords
	VECTOR vRotation
ENDSTRUCT

STRUCT STRENGTH_TEST_DEBUG_CAMERA_DATA
	INT iShakeType 										= 0
	INT iImpactShakeType								= 0
	BOOL bTestShake										= FALSE
	BOOL bTestImpactShake								= FALSE
	BOOL bDisableShake									= FALSE
	BOOL bUpdateDebugValuesInFlow						= FALSE
	BOOL bOutputData									= FALSE
	DEBUG_CAMERA_DATA StartingCamData
	DEBUG_CAMERA_DATA StageOneCamData
	DEBUG_CAMERA_DATA StageTwoCamData
	DEBUG_CAMERA_DATA StageThreeCamData
	DEBUG_CAMERA_DATA StageFourCamData
	DEBUG_CAMERA_DATA StageFiveCamData
ENDSTRUCT

STRUCT STRENGTH_TEST_DEBUG_UI_DATA
	FLOAT fBarValue										= 1.0
	FLOAT fFillValue 									= 0.0
	FLOAT fSwingMultiplier								= 1.0
	BOOL fShowBarMeter									= FALSE
ENDSTRUCT

STRUCT STRENGTH_TEST_DEBUG_DATA
	INT iDebugScore										= -1
	INT iAnimationPercentage							= -1
	BOOL bEnableWidgets									= FALSE
	BOOL bHoldOnBashStage								= FALSE
	BOOL bMutePlayerInputSound							= FALSE
	BOOL bMuteBashingStageSound							= FALSE
	STRENGTH_TEST_DEBUG_UI_DATA UIDebug
	STRENGTH_TEST_DEBUG_CAMERA_DATA CameraDebug
ENDSTRUCT
#ENDIF

//╒══════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ VARIABLES ╞═══════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

PlayerBroadcastData playerBD[NUM_NETWORK_PLAYERS]
ServerBroadcastData serverBD
STRENGTH_TEST_DATA Data

#IF IS_DEBUG_BUILD
STRENGTH_TEST_DEBUG_DATA DebugData
#ENDIF

//╒══════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ FUNCTIONS ╞═══════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

#IF IS_DEBUG_BUILD
/// PURPOSE:
///    Debug function that gets the names of a Strength Test game state.
/// PARAMS:
///    eState - The Strength Test game state to get the name of.
/// RETURNS: The name of the Strength Test game state.
FUNC STRING GET_STRENGTH_TEST_GAME_STATE_NAME(STRENGTH_TEST_GAME_STATE eState)
	SWITCH eState
		CASE ST_GAME_STATE_INVALID				RETURN "ST_GAME_STATE_INVALID"
		CASE ST_GAME_STATE_INITIALISE			RETURN "ST_GAME_STATE_INITIALISE"
		CASE ST_GAME_STATE_REQUEST_ASSETS		RETURN "ST_GAME_STATE_REQUEST_ASSETS"
		CASE ST_GAME_STATE_ENTRY				RETURN "ST_GAME_STATE_ENTRY"
		CASE ST_GAME_STATE_SWING_BASH			RETURN "ST_GAME_STATE_SWING_BASH"
		CASE ST_GAME_STATE_SWING_CATCH			RETURN "ST_GAME_STATE_SWING_CATCH"
		CASE ST_GAME_STATE_PERFORM_SWING		RETURN "ST_GAME_STATE_PERFORM_SWING"
		CASE ST_GAME_STATE_TOTAL				RETURN "ST_GAME_STATE_TOTAL"
	ENDSWITCH
	RETURN "NULL"
ENDFUNC

/// PURPOSE:
///    Debug function that gets the names of a Strength Test render target state.
/// PARAMS:
///    eState - The Strength Test render target state to get the name of.
/// RETURNS: The name of the Strength Test render target state.
FUNC STRING GET_STRENGTH_TEST_RENDER_TARGET_STATE_NAME(STRENGTH_TEST_RENDER_TARGET_STATE eState)
	SWITCH eState
		CASE ST_RT_STATE_INVALID				RETURN "ST_RT_STATE_INVALID"
		CASE ST_RT_STATE_REQUEST_HIGHSCORE		RETURN "ST_RT_STATE_REQUEST_HIGHSCORE"
		CASE ST_RT_STATE_LINK_RT				RETURN "ST_RT_STATE_LINK_RT"
		CASE ST_RT_STATE_REQUEST_ASSETS			RETURN "ST_RT_STATE_REQUEST_ASSETS"
		CASE ST_RT_STATE_INIT_SCORES			RETURN "ST_RT_STATE_INIT_SCORES"
		CASE ST_RT_STATE_SET_TIMER				RETURN "ST_RT_STATE_SET_TIMER"
		CASE ST_RT_STATE_START_TIMER			RETURN "ST_RT_STATE_START_TIMER"
		CASE ST_RT_STATE_PERFORM_SCORE			RETURN "ST_RT_STATE_PERFORM_SCORE"
		CASE ST_RT_STATE_SET_HIGHSCORE			RETURN "ST_RT_STATE_SET_HIGHSCORE"
		CASE ST_RT_STATE_DRAW					RETURN "ST_RT_STATE_DRAW"
		CASE ST_RT_STATE_TOTAL					RETURN "ST_RT_STATE_TOTAL"
	ENDSWITCH
	RETURN "NULL"
ENDFUNC

/// PURPOSE:
///    Debug function that gets the names of a Strength Test render target score state.
/// PARAMS:
///    eState - The Strength Test render target score state to get the name of.
/// RETURNS: The name of the Strength Test render target score state. 
FUNC STRING GET_STRENGTH_TEST_RENDER_TARGET_SCORE_STATE_NAME(STRENGTH_TEST_RENDER_TARGET_SCORE_STATE eState)
	SWITCH eState
		CASE ST_RT_SCORE_STATE_INVALID			RETURN "ST_RT_SCORE_STATE_INVALID"
		CASE ST_RT_SCORE_STATE_ASCEND			RETURN "ST_RT_SCORE_STATE_ASCEND"
		CASE ST_RT_SCORE_STATE_HOLD				RETURN "ST_RT_SCORE_STATE_HOLD"
		CASE ST_RT_SCORE_STATE_DESCEND			RETURN "ST_RT_SCORE_STATE_DESCEND"
		CASE ST_RT_SCORE_STATE_TOTAL			RETURN "ST_RT_SCORE_STATE_TOTAL"
	ENDSWITCH
	RETURN "NULL"
ENDFUNC
#ENDIF

/// PURPOSE:
///    Sets the Strength Test game state.
/// PARAMS:
///    eState - State to set.
PROC SET_STRENGTH_TEST_GAME_STATE(STRENGTH_TEST_GAME_STATE eState)
	IF (Data.eGameState != eState)
		Data.eGameState = eState
		#IF IS_DEBUG_BUILD
		PRINTLN("[AM_MP_ARCADE_STRENGTH_TEST] SET_STRENGTH_TEST_GAME_STATE - Setting state: ", GET_STRENGTH_TEST_GAME_STATE_NAME(eState))
		#ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets the Strength Test render state.
/// PARAMS:
///    eState - State to set.
PROC SET_STRENGTH_TEST_RENDER_TARGET_STATE(STRENGTH_TEST_RENDER_TARGET_STATE eState)
	IF (Data.eRTState != eState)
		Data.eRTState = eState
		#IF IS_DEBUG_BUILD
		PRINTLN("[AM_MP_ARCADE_STRENGTH_TEST] SET_STRENGTH_TEST_RENDER_TARGET_STATE - Setting state: ", GET_STRENGTH_TEST_RENDER_TARGET_STATE_NAME(eState))
		#ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets the Strength Test render target score state.
/// PARAMS:
///    eState - State to set.
PROC SET_STRENGTH_TEST_RENDER_TARGET_SCORE_STATE(STRENGTH_TEST_RENDER_TARGET_SCORE_STATE eState)
	IF (Data.RTData.eScoreState != eState)
		Data.RTData.eScoreState = eState
		#IF IS_DEBUG_BUILD
		PRINTLN("[AM_MP_ARCADE_STRENGTH_TEST] Setting Render Target Score State: ", GET_STRENGTH_TEST_RENDER_TARGET_SCORE_STATE_NAME(eState))
		#ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Gets the Strength Test render target name based on the STRENGTH_TEST_RENDER_TARGET enum passed in.
/// PARAMS:
///    eRenderTargetType - Type to render target to get the name of.
/// RETURNS: The render target name.
FUNC STRING GET_STRENGTH_TEST_RENDER_TARGET_NAME(STRENGTH_TEST_RENDER_TARGET eRenderTargetType)
	STRING sRTName = ""
	SWITCH eRenderTargetType
		CASE ST_RT_HEADBOARD	sRTName = "arc_strenght_01a"	BREAK
		CASE ST_RT_NECK			sRTName = "arc_strenght_02a"	BREAK
	ENDSWITCH
	RETURN sRTName
ENDFUNC

/// PURPOSE:
///    Gets the model of the Strength Test machine.
/// RETURNS: The Strength Test machine model.
FUNC MODEL_NAMES GET_STRENGTH_TEST_RENDER_MODEL(STRENGTH_TEST_RENDER_TARGET eRenderTargetType)
	MODEL_NAMES eModel = DUMMY_MODEL_FOR_SCRIPT
	SWITCH eRenderTargetType
		CASE ST_RT_HEADBOARD	eModel = (INT_TO_ENUM(MODEL_NAMES, HASH("sum_prop_arcade_strength_01a")))	BREAK
		CASE ST_RT_NECK			eModel = (INT_TO_ENUM(MODEL_NAMES, HASH("sum_prop_arcade_str_bar_01a")))	BREAK
	ENDSWITCH
	RETURN eModel
ENDFUNC

/// PURPOSE:
///    Gets the Strength Test scaleform movie name.
/// RETURNS: The Strength Test scaleform movie name.
FUNC STRING GET_STRENGTH_TEST_SCALEFORM_MOVIE_NAME()
	RETURN "STRENGTH_TEST_SCORE"
ENDFUNC

/// PURPOSE:
///    Gets the Strength Test texture dictionary.
/// RETURNS: The Strength Test texture dictionary name as a string.
FUNC STRING GET_STRENGTH_TEST_TEXTURE_DICTIONARY()
	RETURN "ArcadeUI_Axe_of_Fury"
ENDFUNC

/// PURPOSE:
///    Triggers speech events.
/// PARAMS:
///    tlContext - Dialogue name.
///    bOverNetwork - Should the event be broadcast to everyone inside the property.
PROC TRIGGER_STRENGTH_TEST_DIALOGUE(TEXT_LABEL_63 tlContext, BOOL bOverNetwork)
	Data.SpeechData.iDisplaySlot 				= GET_ARCADE_CABINET_LOCATE_DISPLAY_SLOT_PLAYER_IS_IN(PLAYER_ID())
	
	IF (Data.SpeechData.iDisplaySlot = -1 AND Data.SpeechData.iCachedDisplaySlot != -1)
		Data.SpeechData.iDisplaySlot = Data.SpeechData.iCachedDisplaySlot
	ENDIF
	
	Data.SpeechData.eAIContext.eArcadeCabinet 	= ARCADE_CABINET_SUM_STRENGTH_TEST
	Data.SpeechData.eAIContext.sCabinetSlot 	= INT_TO_ENUM(ARCADE_CAB_MANAGER_SLOT, Data.SpeechData.iDisplaySlot)
	Data.SpeechData.eAIContext.stContext 		= tlContext
	Data.SpeechData.eAIContext.bUseConvo 		= FALSE
	Data.SpeechData.eAIContext.spParams			= SPEECH_PARAMS_FORCE
	PLAY_ARCADE_CABINET_AI_CONTEXT(Data.SpeechData.eAIContext, bOverNetwork)
ENDPROC

/// PURPOSE:
///    Triggers a Strength Test sound.
/// PARAMS:
///    sSoundName 	- Name of sound to trigger.
///    bNetworked 	- Whether the sound is networked or not.
PROC TRIGGER_STRENGTH_TEST_SOUND(STRING sSoundName, INT &iSoundID, BOOL bNetworked = TRUE)
	IF NOT IS_STRING_NULL_OR_EMPTY(sSoundName)
		IF (iSoundID = -1)
			iSoundID = GET_SOUND_ID()
		ENDIF
		STOP_SOUND(iSoundID)
		PLAY_SOUND_FROM_COORD(iSoundID, sSoundName, Data.vCabinetCoords, "sum20_am_Axe_of_Fury_sounds", bNetworked)
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets a param value on a Strength Test sound.
/// PARAMS:
///    sParamName 	- Name of param.
///    fSoundValue 	- Param value.
PROC SET_STRENGTH_TEST_SOUND_PARAM(STRING sParamName, INT iSoundID, FLOAT fSoundValue)
	IF IS_STRING_NULL_OR_EMPTY(sParamName)
		EXIT
	ENDIF
	
	IF (fSoundValue < 0.0)
		fSoundValue = 0.0
	ELIF (fSoundValue > 1.0)
		fSoundValue = 1.0
	ENDIF
	
	SET_VARIABLE_ON_SOUND(iSoundID, sParamName, fSoundValue)
ENDPROC

/// PURPOSE:
///    Sets the state of the Strength Test highscore light.
/// PARAMS:
///    missionScriptArgs - Child script arcade data.
///    bTurnLightOn - Light state.
PROC SET_STRENGTH_TEST_HIGHSCORE_LIGHT_STATE(ACM_CHILD_SCRIPT_LAUNCHER_STRUCT missionScriptArgs, BOOL bTurnLightOn)
	
	// Light state is already set
	IF (bTurnLightOn) AND IS_BIT_SET(Data.iBS, BS_ST_HIGHSCORE_LIGHT_ON)
	OR (NOT bTurnLightOn AND NOT IS_BIT_SET(Data.iBS, BS_ST_HIGHSCORE_LIGHT_ON))
		EXIT
	ENDIF
	
	// Ensure light objects exist and player has control of them
	IF NOT DOES_ENTITY_EXIST(missionScriptArgs.cabinetSubObj[ARCADE_CAB_OBJECT_2])	// Light On
	OR NOT DOES_ENTITY_EXIST(missionScriptArgs.cabinetSubObj[ARCADE_CAB_OBJECT_3])	// Light Off
		EXIT
	ENDIF
	
	IF (bTurnLightOn)
		REMOVE_MODEL_HIDE(GET_ENTITY_COORDS(missionScriptArgs.cabinetSubObj[ARCADE_CAB_OBJECT_2]), 3.0, GET_ENTITY_MODEL(missionScriptArgs.cabinetSubObj[ARCADE_CAB_OBJECT_2]))
		CREATE_MODEL_HIDE(GET_ENTITY_COORDS(missionScriptArgs.cabinetSubObj[ARCADE_CAB_OBJECT_3]), 3.0, GET_ENTITY_MODEL(missionScriptArgs.cabinetSubObj[ARCADE_CAB_OBJECT_3]), TRUE)
		IF NOT IS_BIT_SET(Data.iBS, BS_ST_HIGHSCORE_LIGHT_ON)
			SET_BIT(Data.iBS, BS_ST_HIGHSCORE_LIGHT_ON)
		ENDIF
	ELSE
		REMOVE_MODEL_HIDE(GET_ENTITY_COORDS(missionScriptArgs.cabinetSubObj[ARCADE_CAB_OBJECT_3]), 3.0, GET_ENTITY_MODEL(missionScriptArgs.cabinetSubObj[ARCADE_CAB_OBJECT_3]))
		CREATE_MODEL_HIDE(GET_ENTITY_COORDS(missionScriptArgs.cabinetSubObj[ARCADE_CAB_OBJECT_2]), 3.0, GET_ENTITY_MODEL(missionScriptArgs.cabinetSubObj[ARCADE_CAB_OBJECT_2]), TRUE)
		IF IS_BIT_SET(Data.iBS, BS_ST_HIGHSCORE_LIGHT_ON)
			CLEAR_BIT(Data.iBS, BS_ST_HIGHSCORE_LIGHT_ON)
		ENDIF
	ENDIF
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════════╡ MoVE Network ╞═════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    Gets the MoVE Network clip set name.
/// RETURNS: The clip set name as a STRING.
FUNC STRING GET_STRENGTH_TEST_MOVE_NETWORK_CLIP_SET_NAME()
	STRING sClipSet = "clipset@anim_heist@arcade@strength@male"
	IF IS_PLAYER_FEMALE()
	AND IS_PED_WEARING_HIGH_HEELS(PLAYER_PED_ID())
		sClipSet = "clipset@anim_heist@arcade@strength@female"
	ENDIF
	RETURN sClipSet
ENDFUNC

/// PURPOSE:
///    Gets the MoVE Netowrk clip set hash.
/// RETURNS: Hash of the clip set as an INT.
FUNC INT GET_STRENGTH_TEST_MOVE_NETWORK_CLIP_SET_HASH()
	INT iClipSetHash = HASH("clipset@anim_heist@arcade@strength@male")
	IF IS_PLAYER_FEMALE()
	AND IS_PED_WEARING_HIGH_HEELS(PLAYER_PED_ID())
		iClipSetHash = HASH("clipset@anim_heist@arcade@strength@female")
	ENDIF
	RETURN iClipSetHash
ENDFUNC

/// PURPOSE:
///    Gets the MoVE Netowrk clip set variable hash.
/// RETURNS: Variable hash of the clip set as an INT.
FUNC INT GET_STRENGTH_TEST_MOVE_NETWORK_CLIP_SET_VARIABLE_HASH()
	RETURN HASH("DEFAULT")
ENDFUNC

/// PURPOSE:
///    Requests and loads the Strength Test MoVE Network clipset.
/// RETURNS: TRUE when the clipset has loaded. FALSE otherwise.
FUNC BOOL REQUEST_AND_LOAD_STRENGTH_TEST_CLIP_SET()
	STRING sClipSet = GET_STRENGTH_TEST_MOVE_NETWORK_CLIP_SET_NAME()
	REQUEST_CLIP_SET(sClipSet)
	RETURN HAS_CLIP_SET_LOADED(sClipSet)
ENDFUNC

/// PURPOSE:
///    Removes the Strength Test clip set from memeory.
PROC REMOVE_STRENGTH_TEST_CLIP_SET()
	STRING sClipSet = GET_STRENGTH_TEST_MOVE_NETWORK_CLIP_SET_NAME()
	REMOVE_CLIP_SET(sClipSet)
ENDPROC

/// PURPOSE:
///    Everyone needs to run this function, both the local player playing the arcade game and all remote players inside the Arcade.
///    The native SET_TASK_MOVE_NETWORK_ANIM_SET isn't networked, but all players have to call it for the move network to work in their eyes.
///    Therefore we need to send an event to all players with the clipset details and have everyone call SET_TASK_MOVE_NETWORK_ANIM_SET when available.
PROC MAINTAIN_STRENGTH_TEST_MOVE_NETWORK()
	IF IS_BIT_SET(Data.iBS, BS_ST_MOVE_NETWORK_INITIALISED)
		IF IS_ENTITY_ALIVE(GET_PLAYER_PED(Data.MoveNetworkData.arcadePlayerID))
		AND IS_TASK_MOVE_NETWORK_ACTIVE(GET_PLAYER_PED(Data.MoveNetworkData.arcadePlayerID))
			CLEAR_BIT(Data.iBS, BS_ST_MOVE_NETWORK_INITIALISED)
			SET_BIT(Data.iBS2, BS2_ST_SET_ARCADE_PLAYER_CAPSULE_SIZE)
			SET_TASK_MOVE_NETWORK_ANIM_SET(GET_PLAYER_PED(Data.MoveNetworkData.arcadePlayerID), Data.MoveNetworkData.iClipSetHash, Data.MoveNetworkData.iClipSetVariableHash)
			PRINTLN("[AM_MP_ARCADE_STRENGTH_TEST] MAINTAIN_STRENGTH_TEST_MOVE_NETWORK - Calling SET_TASK_MOVE_NETWORK_ANIM_SET on Player: ", GET_PLAYER_NAME(Data.MoveNetworkData.arcadePlayerID), " Player ID: ", NATIVE_TO_INT(Data.MoveNetworkData.arcadePlayerID), " Clipset Hash: ", Data.MoveNetworkData.iClipSetHash, " Clipset Variable Hash: ", Data.MoveNetworkData.iClipSetVariableHash)
			IF (PLAYER_ID() = Data.MoveNetworkData.arcadePlayerID)
				SET_BIT(Data.iBS2, BS2_ST_MOVE_NETWORK_CLIP_SET)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ ANIMATIONS ╞══════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    Requests and loads the Strength Test animation dictionary.
/// RETURNS: TRUE when the animation dictionary has loaded.
FUNC BOOL REQUEST_AND_LOAD_STRENGTH_TEST_ANIM_DICT()
	GET_STRENGTH_TEST_ARCADE_CABINET_ANIMATION_DETAIL(Data.eArcadeCabinetLocateType, ARCADE_CABINET_ANIM_CLIP_ENTRY, 0, Data.AnimData.Details)
	REQUEST_ANIM_DICT(Data.AnimData.Details.strArcCabinetAnimDic)
	RETURN HAS_ANIM_DICT_LOADED(Data.AnimData.Details.strArcCabinetAnimDic)
ENDFUNC

/// PURPOSE:
///    Gets the Strength Test animation dictionary.
/// RETURNS: The Strength Test animation dictionary as a STRING.
FUNC STRING GET_STRENGTH_TEST_ANIM_DICT()
	STRING sAnimDict = "ANIM_HEIST@ARCADE@STRENGTH@MALE@"
	IF IS_PLAYER_FEMALE()
	AND IS_PED_WEARING_HIGH_HEELS(PLAYER_PED_ID())
		sAnimDict = "ANIM_HEIST@ARCADE@STRENGTH@FEMALE@"
	ENDIF
	RETURN sAnimDict
ENDFUNC

/// PURPOSE:
///    Gets the phase of the currently playing animation.
/// RETURNS: Anim phase as a FLOAT.
FUNC FLOAT GET_CURRENT_ANIM_PHASE()
	INT iDifference		 = (GET_GAME_TIMER()-Data.AnimData.iStartTimeMS)
	INT iIncrease		 = (Data.AnimData.iAnimDurationMS-iDifference)
	FLOAT fPercentage	 = (TO_FLOAT(iIncrease)/TO_FLOAT(Data.AnimData.iAnimDurationMS))*100.0
	FLOAT fPhase		 = (100.0-fPercentage)
	RETURN (fPhase/100)
ENDFUNC

/// PURPOSE:
///    Checks if the currently playing anim has surpassed a phase time.
/// PARAMS:
///    fPhase - Anim phase to check.
/// RETURNS: TRUE if the current animation has surpassed the phase. FALSE otherwise.
FUNC BOOL HAS_CURRENT_ANIM_PAST_PHASE(FLOAT fPhase)
	RETURN (GET_CURRENT_ANIM_PHASE() >= fPhase)
ENDFUNC

/// PURPOSE:
///    Initialises the Strength Test anim data.
PROC INITIALISE_STRENGTH_TEST_ANIM_DATA()
	Data.MoveNetworkData.iClipSetHash			= GET_STRENGTH_TEST_MOVE_NETWORK_CLIP_SET_HASH()
	Data.MoveNetworkData.iClipSetVariableHash	= GET_STRENGTH_TEST_MOVE_NETWORK_CLIP_SET_VARIABLE_HASH()
	BROADCAST_STRENGTH_TEST_EVENT_MOVE_NETWORK_DATA(PLAYER_ID(), Data.MoveNetworkData.iClipSetHash, Data.MoveNetworkData.iClipSetVariableHash)
	
	Data.GameplayData.iCatchTimeMS = GET_RANDOM_INT_IN_RANGE(ciST_SWING_CATCH_MIN_WAIT_TIMER_MS, ciST_SWING_CATCH_MAX_WAIT_TIMER_MS)
	START_NET_TIMER(Data.stPlayDurationTimer)
ENDPROC

/// PURPOSE:
///    Gets the Strength Test animation clip enum from the passed in bashing stage.
/// PARAMS:
///    iStage - Current bashing stage.
/// RETURNS: The animation clip enum of the passed in bashing stage.
FUNC ARCADE_CABINET_ANIM_CLIPS GET_STRENGTH_TEST_ANIM_CLIP_FROM_BASHING_STAGE(INT iStage)
	ARCADE_CABINET_ANIM_CLIPS eAnimClip = ARCADE_CABINET_ANIM_CLIP_INVALID
	SWITCH iStage
		CASE 0	eAnimClip = ARCADE_CABINET_ANIM_CLIP_STRENGTH_TEST_STAGE_1_LOOP	BREAK
		CASE 1	eAnimClip = ARCADE_CABINET_ANIM_CLIP_STRENGTH_TEST_STAGE_2_LOOP	BREAK
		CASE 2	eAnimClip = ARCADE_CABINET_ANIM_CLIP_STRENGTH_TEST_STAGE_3_LOOP	BREAK
		CASE 3	eAnimClip = ARCADE_CABINET_ANIM_CLIP_STRENGTH_TEST_STAGE_4_LOOP	BREAK
	ENDSWITCH
	RETURN eAnimClip
ENDFUNC

/// PURPOSE:
///    Gets the corresponding facial animation name based on the body anim clip passed in.
/// PARAMS:
///    eAnim - Body animation being played.
/// RETURNS: Name of the corresponding facial animation.
FUNC STRING GET_STRENGTH_TEST_FACIAL_ANIM_CLIP_NAME(ARCADE_CABINET_ANIM_CLIPS eAnim)
	STRING sFacialAnimName = ""
	SWITCH eAnim
		CASE ARCADE_CABINET_ANIM_CLIP_ENTRY								sFacialAnimName =  "ENTER_FACIAL"			BREAK
		CASE ARCADE_CABINET_ANIM_CLIP_EXIT								sFacialAnimName =  "EXIT_FACIAL"			BREAK
		CASE ARCADE_CABINET_ANIM_CLIP_STRENGTH_TEST_STAGE_1_LOOP		sFacialAnimName =  "STAGE_1_LOOP_FACIAL"	BREAK
		CASE ARCADE_CABINET_ANIM_CLIP_STRENGTH_TEST_STAGE_2_LOOP		sFacialAnimName =  "STAGE_2_LOOP_FACIAL"	BREAK
		CASE ARCADE_CABINET_ANIM_CLIP_STRENGTH_TEST_STAGE_3_LOOP		sFacialAnimName =  "STAGE_3_LOOP_FACIAL"	BREAK
		CASE ARCADE_CABINET_ANIM_CLIP_STRENGTH_TEST_STAGE_4_LOOP		sFacialAnimName =  "STAGE_4_LOOP_FACIAL"	BREAK
		CASE ARCADE_CABINET_ANIM_CLIP_STRENGTH_TEST_RESULT_BAD			sFacialAnimName =  "RESULT_BAD_FACIAL"		BREAK
		CASE ARCADE_CABINET_ANIM_CLIP_STRENGTH_TEST_RESULT_AVERAGE		sFacialAnimName =  "RESULT_AVERAGE_FACIAL"	BREAK
		CASE ARCADE_CABINET_ANIM_CLIP_STRENGTH_TEST_RESULT_GOOD			sFacialAnimName =  "RESULT_GOOD_FACIAL"		BREAK
		CASE ARCADE_CABINET_ANIM_CLIP_STRENGTH_TEST_RESULT_PERFECT		sFacialAnimName =  "RESULT_PERFECT_FACIAL"	BREAK
	ENDSWITCH
	RETURN sFacialAnimName
ENDFUNC

/// PURPOSE:
///    Plays the corresponding Strength Test facial animation based on the anim clip passed in.
/// PARAMS:
///    eAnim - Body animation being played.
PROC PLAY_STRENGTH_TEST_FACIAL_ANIM(ARCADE_CABINET_ANIM_CLIPS eAnim)
	STRING sFacialAnimClip = GET_STRENGTH_TEST_FACIAL_ANIM_CLIP_NAME(eAnim)
	IF NOT IS_STRING_NULL_OR_EMPTY(sFacialAnimClip)
		IF NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), g_ArcadeCabinetManagerData.strArcCabinetAnimDic, sFacialAnimClip)
			PLAY_FACIAL_ANIM(PLAYER_PED_ID(), sFacialAnimClip, g_ArcadeCabinetManagerData.strArcCabinetAnimDic)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Maintains the Strength Test bashing stage animations.
/// PARAMS:
///    fSwingWeight - The current weight of the bashing animation from 0-1.
PROC MAINTAIN_STRENGTH_TEST_BASHING_STAGE_FACIAL_ANIMS(FLOAT fSwingWeight)
	IF (fSwingWeight >= cfST_BASHING_STAGE_4)
		IF NOT IS_BIT_SET(Data.iBS2, BS2_ST_PLAY_STAGE_LOOP_4_FACIAL_ANIM)
			SET_BIT(Data.iBS2, BS2_ST_PLAY_STAGE_LOOP_4_FACIAL_ANIM)
			CLEAR_BIT(Data.iBS2, BS2_ST_PLAY_STAGE_LOOP_1_FACIAL_ANIM)
			CLEAR_BIT(Data.iBS2, BS2_ST_PLAY_STAGE_LOOP_2_FACIAL_ANIM)
			CLEAR_BIT(Data.iBS2, BS2_ST_PLAY_STAGE_LOOP_3_FACIAL_ANIM)
			PLAY_STRENGTH_TEST_FACIAL_ANIM(ARCADE_CABINET_ANIM_CLIP_STRENGTH_TEST_STAGE_4_LOOP)
		ENDIF
	ELIF (fSwingWeight >= cfST_BASHING_STAGE_3 AND fSwingWeight < cfST_BASHING_STAGE_4)
		IF NOT IS_BIT_SET(Data.iBS2, BS2_ST_PLAY_STAGE_LOOP_3_FACIAL_ANIM)
			SET_BIT(Data.iBS2, BS2_ST_PLAY_STAGE_LOOP_3_FACIAL_ANIM)
			CLEAR_BIT(Data.iBS2, BS2_ST_PLAY_STAGE_LOOP_1_FACIAL_ANIM)
			CLEAR_BIT(Data.iBS2, BS2_ST_PLAY_STAGE_LOOP_2_FACIAL_ANIM)
			CLEAR_BIT(Data.iBS2, BS2_ST_PLAY_STAGE_LOOP_4_FACIAL_ANIM)
			PLAY_STRENGTH_TEST_FACIAL_ANIM(ARCADE_CABINET_ANIM_CLIP_STRENGTH_TEST_STAGE_3_LOOP)
		ENDIF
	ELIF (fSwingWeight >= cfST_BASHING_STAGE_2 AND fSwingWeight < cfST_BASHING_STAGE_3)
		IF NOT IS_BIT_SET(Data.iBS2, BS2_ST_PLAY_STAGE_LOOP_2_FACIAL_ANIM)
			SET_BIT(Data.iBS2, BS2_ST_PLAY_STAGE_LOOP_2_FACIAL_ANIM)
			CLEAR_BIT(Data.iBS2, BS2_ST_PLAY_STAGE_LOOP_1_FACIAL_ANIM)
			CLEAR_BIT(Data.iBS2, BS2_ST_PLAY_STAGE_LOOP_3_FACIAL_ANIM)
			CLEAR_BIT(Data.iBS2, BS2_ST_PLAY_STAGE_LOOP_4_FACIAL_ANIM)
			PLAY_STRENGTH_TEST_FACIAL_ANIM(ARCADE_CABINET_ANIM_CLIP_STRENGTH_TEST_STAGE_2_LOOP)
		ENDIF
	ELIF (fSwingWeight < cfST_BASHING_STAGE_2)
		IF NOT IS_BIT_SET(Data.iBS2, BS2_ST_PLAY_STAGE_LOOP_1_FACIAL_ANIM)
			SET_BIT(Data.iBS2, BS2_ST_PLAY_STAGE_LOOP_1_FACIAL_ANIM)
			CLEAR_BIT(Data.iBS2, BS2_ST_PLAY_STAGE_LOOP_2_FACIAL_ANIM)
			CLEAR_BIT(Data.iBS2, BS2_ST_PLAY_STAGE_LOOP_3_FACIAL_ANIM)
			CLEAR_BIT(Data.iBS2, BS2_ST_PLAY_STAGE_LOOP_4_FACIAL_ANIM)
			PLAY_STRENGTH_TEST_FACIAL_ANIM(ARCADE_CABINET_ANIM_CLIP_STRENGTH_TEST_STAGE_1_LOOP)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Attaches the Strength Test hammer prop to the local player.
/// PARAMS:
///    missionScriptArgs - Prop data.
/// RETURNS: TRUE if the hammer has successfully attached to the local player, FALSE otherwise.
FUNC BOOL ATTACH_STRENGTH_TEST_HAMMER_TO_LOCAL_PLAYER(ACM_CHILD_SCRIPT_LAUNCHER_STRUCT missionScriptArgs)
	IF DOES_ENTITY_EXIST(missionScriptArgs.cabinetSubObj[ARCADE_CAB_OBJECT_1])
		IF NOT IS_ENTITY_ATTACHED_TO_ANY_PED(missionScriptArgs.cabinetSubObj[ARCADE_CAB_OBJECT_1])
			NETWORK_ALLOW_REMOTE_ATTACHMENT_MODIFICATION(missionScriptArgs.cabinetSubObj[ARCADE_CAB_OBJECT_1], TRUE)
			ATTACH_ENTITY_TO_ENTITY(missionScriptArgs.cabinetSubObj[ARCADE_CAB_OBJECT_1], PLAYER_PED_ID(), GET_PED_BONE_INDEX(PLAYER_PED_ID(), BONETAG_PH_L_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
			FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID(), DEFAULT, TRUE)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Deattaches the Strength Test hammer from the local player.
/// PARAMS:
///    missionScriptArgs - Prop data.
/// RETURNS: TRUE if the hammer has successfully detached from the local player, FALSE otherwise.
FUNC BOOL DETACH_STRENGTH_TEST_HAMMER_FROM_LOCAL_PLAYER(ACM_CHILD_SCRIPT_LAUNCHER_STRUCT missionScriptArgs)
	IF DOES_ENTITY_EXIST(missionScriptArgs.cabinetObj)
	AND DOES_ENTITY_EXIST(missionScriptArgs.cabinetSubObj[ARCADE_CAB_OBJECT_1])
	AND NETWORK_HAS_CONTROL_OF_ENTITY(missionScriptArgs.cabinetSubObj[ARCADE_CAB_OBJECT_1])
		IF IS_ENTITY_ATTACHED_TO_ANY_PED(missionScriptArgs.cabinetSubObj[ARCADE_CAB_OBJECT_1])
			ACM_SUB_PROP_DETAILS eHammerDetails
			GET_STRENGTH_TEST_ARCADE_CABINET_SUB_PROP_DETAIL(ARCADE_CABINET_SUM_STRENGTH_TEST, eHammerDetails)
			DETACH_ENTITY(missionScriptArgs.cabinetSubObj[ARCADE_CAB_OBJECT_1])
			SET_ENTITY_COORDS_NO_OFFSET(missionScriptArgs.cabinetSubObj[ARCADE_CAB_OBJECT_1], GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(missionScriptArgs.cabinetObj, eHammerDetails.vOffset[ARCADE_CAB_OBJECT_1]))
			eHammerDetails.vRotation[ARCADE_CAB_OBJECT_1].z = GET_ENTITY_HEADING(Data.cabinetObj)
			SET_ENTITY_ROTATION(missionScriptArgs.cabinetSubObj[ARCADE_CAB_OBJECT_1], eHammerDetails.vRotation[ARCADE_CAB_OBJECT_1])
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ TROPHIES ╞═══════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    Process the Strength Test trophies.
PROC PROCESS_STRENGTH_TEST_TROPHIES()
	IF (Data.GameplayData.iPropScore = ciST_MAX_PROP_SCORE)
		
		// Crank it to 11 tshirt
		IF NOT GET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_GOFOR11TH)
			SET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_GOFOR11TH, TRUE)
			SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_ARCADE_AXE_OF_FURY_CRANK_IT_TO_ELEVEN_TSHIRT, TRUE)
			ARCADE_CABINET_FLOW_NOTIFICATIONS_SET_SHOW_TSHIRT_HELP(ARCADE_GAME_TSHIRTS_STRENGTH_TEST_CRANK_IT_TO_ELEVEN)
			SET_XP_BAR_ACTIVE(TRUE)
			GIVE_LOCAL_PLAYER_FM_XP_WITH_ANIMATION(eXPTYPE_STANDARD, PLAYER_PED_ID(), "", XPTYPE_AWARDS, XPCATEGORY_RP_STRENGTH_TEST, 800)
			SET_XP_BAR_ACTIVE(FALSE)
			Data.GameplayData.bCrankChallUnlockedThisFrame = TRUE
			PRINTLN("[AM_MP_ARCADE_STRENGTH_TEST] - PROCESS_STRENGTH_TEST_TROPHIES - Player has unlocked Pause Menu Award: CRANK IT TO 11!")
		ENDIF
		
		INT iElevenCounter = GET_PACKED_STAT_INT(PACKED_MP_INT_ARCADE_AXE_OF_FURY_ELEVEN_COUNTER)+1
		PRINTLN("[AM_MP_ARCADE_STRENGTH_TEST] - PROCESS_STRENGTH_TEST_TROPHIES - iElevenCounter: ", iElevenCounter)
		
		// Player has hit 11, 11 times
		IF (iElevenCounter = ciST_ELEVEN_ELEVEN_REWARD_TOTAL)
			SET_PACKED_STAT_INT(PACKED_MP_INT_ARCADE_AXE_OF_FURY_ELEVEN_COUNTER, iElevenCounter)
			IF NOT GET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_ELEVENELEVEN)
				SET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_ELEVENELEVEN, TRUE)
				SET_ARCADE_STRENGTH_TEST_TROPHY_COMPLETE(TRUE)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_ARCADE_AXE_OF_FURY_ELEVEN_ELEVEN_TSHIRT, TRUE)
				ARCADE_CABINET_FLOW_NOTIFICATIONS_SET_SHOW_TSHIRT_HELP(ARCADE_GAME_TSHIRTS_STRENGTH_TEST_ELEVEN_ELEVEN)
				ARCADE_CABINET_FLOW_NOTIFICATIONS_SET_SHOW_TSHIRT_HELP(ARCADE_GAME_TSHIRTS_STRENGTH_TEST_AXE_MASTER)
				PRINT_HELP("ARD_CAB_AOF_TROP1")
				SET_XP_BAR_ACTIVE(TRUE)
				GIVE_LOCAL_PLAYER_FM_XP_WITH_ANIMATION(eXPTYPE_STANDARD, PLAYER_PED_ID(), "", XPTYPE_AWARDS, XPCATEGORY_RP_STRENGTH_TEST, 800)
				SET_XP_BAR_ACTIVE(FALSE)
				Data.GameplayData.bElevenElevenChallUnlockedThisFrame = TRUE
				PRINTLN("[AM_MP_ARCADE_STRENGTH_TEST] - PROCESS_STRENGTH_TEST_TROPHIES - Player has unlocked Pause Menu Award: 11 11!")
				PRINTLN("[AM_MP_ARCADE_STRENGTH_TEST] - PROCESS_STRENGTH_TEST_TROPHIES - Player has unlocked the Axe of Fury Trophy!")
			ENDIF
			
		// Increment number of times player has hit 11
		ELIF (iElevenCounter < ciST_ELEVEN_ELEVEN_REWARD_TOTAL)
			SET_PACKED_STAT_INT(PACKED_MP_INT_ARCADE_AXE_OF_FURY_ELEVEN_COUNTER, iElevenCounter)
			PRINTLN("[AM_MP_ARCADE_STRENGTH_TEST] - PROCESS_STRENGTH_TEST_TROPHIES - Setting counter stat PACKED_MP_INT_ARCADE_AXE_OF_FURY_ELEVEN_COUNTER to: ", iElevenCounter)
		ENDIF
		
	ENDIF
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ TELEMETRY ╞═══════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    Sends the Strength Test metric for telemetry.
PROC SEND_STRENGTH_TEST_METRIC()
	STRUCT_ARCADE_CABINET telemetryData
	telemetryData.score 		= Data.GameplayData.iPropScore
	telemetryData.gameType 	= HASH("ARCADE_CABINET_SUM_STRENGTH_TEST")
	telemetryData.numPlayers = 1
	telemetryData.timePlayed = ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), Data.stPlayDurationTimer.Timer))
	telemetryData.challenges = ENUM_TO_INT(STRENGTH_TEST_CHALLENGE_BIT_NONE)
	
	IF (Data.GameplayData.bCrankChallUnlockedThisFrame)
		telemetryData.challenges = ENUM_TO_INT(STRENGTH_TEST_CHALLENGE_BIT_CRANK_TO_ELEVEN)
	ELIF (Data.GameplayData.bElevenElevenChallUnlockedThisFrame)
		telemetryData.challenges = ENUM_TO_INT(STRENGTH_TEST_CHALLENGE_BIT_ELEVEN_ELEVEN)
		telemetryData.reward = ARCADE_GAME_GET_TROPHY_ID_FOR_TELEMETRY(ARCADE_GAME_TROPHY_STRENGTH_TEST)
	ENDIF
	
	IF PLAYER_ID() != INVALID_PLAYER_INDEX()
		IF IS_PLAYER_IN_AUTO_SHOP(PLAYER_ID())
			telemetryData.location = HASH("SIMPLE_INTERIOR_TYPE_AUTO_SHOP")
		ELIF IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
			telemetryData.location = HASH("SIMPLE_INTERIOR_TYPE_ARCADE")
		ELSE
			CDEBUG1LN(DEBUG_MINIGAME, "[AM_MP_ARCADE_STRENGTH_TEST] [SEND_STRENGTH_TEST_METRIC] not in auto shop or arcade, setting sRoadArcadeTelemetry.location = 0")
			telemetryData.location = 0
		ENDIF
	ELSE
		CDEBUG1LN(DEBUG_MINIGAME, "[AM_MP_ARCADE_STRENGTH_TEST] [SEND_STRENGTH_TEST_METRIC] Invalid Player ID, setting telemetryData.location = 0")
		telemetryData.location = 0
	ENDIF
	
	PLAYSTATS_ARCADE_CABINET(telemetryData)
	PRINTLN("[AM_MP_ARCADE_STRENGTH_TEST] SEND_STRENGTH_TEST_METRIC - Sending metric")
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ CAMERAS ╞════════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

#IF IS_DEBUG_BUILD
FUNC STRING DEBUG_GET_STRENGTH_TEST_CAMERA_TYPE(INT iShakeType)
	STRING sShakeType = ""
	SWITCH iShakeType
		CASE 0	sShakeType = "SMALL_EXPLOSION_SHAKE"	BREAK
		CASE 1	sShakeType = "MEDIUM_EXPLOSION_SHAKE"	BREAK
		CASE 2	sShakeType = "LARGE_EXPLOSION_SHAKE"	BREAK
		CASE 3	sShakeType = "HAND_SHAKE"				BREAK
		CASE 4	sShakeType = "JOLT_SHAKE"				BREAK
		CASE 5	sShakeType = "VIBRATE_SHAKE"			BREAK
		CASE 6	sShakeType = "WOBBLY_SHAKE"				BREAK
		CASE 7	sShakeType = "DRUNK_SHAKE"				BREAK
	ENDSWITCH
	RETURN sShakeType
ENDFUNC
#ENDIF

/// PURPOSE:
///    Normalise value between scale
/// PARAMS:
///    fCurrentValue 	- Current value to normalise
///    fMinValue 		- Minimum value
///    fMaxValue 		- Maximum value
/// RETURNS: Normalised value.
FUNC FLOAT NORMALISE_VALUE(FLOAT fCurrentValue, FLOAT fMinValue, FLOAT fMaxValue)
	RETURN (fCurrentValue-fMinValue)/(fMaxValue-fMinValue)
ENDFUNC

/// PURPOSE:
///    Gets the Stregnth Test camera type based on the current bashing stage.
/// PARAMS:
///    iCurrentStage - Current bashing stage.
/// RETURNS: The camera type based on the current bashing stage.
FUNC STRENGTH_TEST_CAMERAS GET_STRENGTH_TEST_STAGE_CAMERA_TYPE(INT iCurrentStage)
	STRENGTH_TEST_CAMERAS eCamType = ST_CAMERA_INVALID
	SWITCH iCurrentStage
		CASE 0	eCamType = ST_CAMERA_STARTING		BREAK
		CASE 1	eCamType = ST_CAMERA_STAGE_ONE		BREAK
		CASE 2	eCamType = ST_CAMERA_STAGE_TWO		BREAK
		CASE 3	eCamType = ST_CAMERA_STAGE_THREE	BREAK
		CASE 4	eCamType = ST_CAMERA_STAGE_FOUR		BREAK
		CASE 5	eCamType = ST_CAMERA_STAGE_FIVE		BREAK
	ENDSWITCH
	RETURN eCamType
ENDFUNC

/// PURPOSE:
///    Gets the Strength Test data based on the camera stage.
/// PARAMS:
///    eCamType - The stage camera type.
///    vCoords - Coords for the camera stage.
///    vRotation - Rotation for the camera stage.
///    fFOV - FOV for the camera stage.
///    bUseWorldCoords - Return coords in world space or local space relative to the arcade cabinet machine.
PROC GET_STRENGTH_TEST_CAMERA_DATA(STRENGTH_TEST_CAMERAS eCamType, VECTOR &vCoords, VECTOR &vRotation, FLOAT &fFOV, BOOL bUseWorldCoords = TRUE #IF IS_DEBUG_BUILD, BOOL bUseDebugValues = TRUE #ENDIF)
	SWITCH eCamType
		CASE ST_CAMERA_STARTING
			vCoords 	= <<1.6000, -2.1800, 0.8800>>
			vRotation 	= <<13.2800, 0.0000, 40.5800>>
			fFOV 		= 50.0
			#IF IS_DEBUG_BUILD
			IF (bUseDebugValues)
				vCoords 	= debugData.CameraDebug.StartingCamData.vCoords
				vRotation 	= debugData.CameraDebug.StartingCamData.vRotation
				fFOV 		= debugData.CameraDebug.StartingCamData.fFOV
			ENDIF
			#ENDIF
		BREAK
		CASE ST_CAMERA_STAGE_ONE
			vCoords 	= <<1.6000, -2.1800, 0.8800>>
			vRotation 	= <<15.3800, 0.0000, 44.0800>>
			fFOV 		= 45.0
			#IF IS_DEBUG_BUILD
			IF (bUseDebugValues)
				vCoords 	= debugData.CameraDebug.StageOneCamData.vCoords
				vRotation 	= debugData.CameraDebug.StageOneCamData.vRotation
				fFOV 		= debugData.CameraDebug.StageOneCamData.fFOV
			ENDIF
			#ENDIF
		BREAK
		CASE ST_CAMERA_STAGE_TWO
			vCoords 	= <<1.6000, -2.1800, 1.0800>>
			vRotation 	= <<14.4000, 0.0000, 51.5300>>
			fFOV 		= 40.0
			#IF IS_DEBUG_BUILD
			IF (bUseDebugValues)
				vCoords 	= debugData.CameraDebug.StageTwoCamData.vCoords
				vRotation 	= debugData.CameraDebug.StageTwoCamData.vRotation
				fFOV 		= debugData.CameraDebug.StageTwoCamData.fFOV
			ENDIF
			#ENDIF
		BREAK
		CASE ST_CAMERA_STAGE_THREE
			vCoords 	= <<1.6000, -2.1800, 1.0800>>
			vRotation 	= <<10.8000, 0.0000, 51.1200>>
			fFOV 		= 38.0
			#IF IS_DEBUG_BUILD
			IF (bUseDebugValues)
				vCoords 	= debugData.CameraDebug.StageThreeCamData.vCoords
				vRotation 	= debugData.CameraDebug.StageThreeCamData.vRotation
				fFOV 		= debugData.CameraDebug.StageThreeCamData.fFOV
			ENDIF
			#ENDIF
		BREAK
		CASE ST_CAMERA_STAGE_FOUR
			vCoords 	= <<1.6000, -2.1800, 1.1800>>
			vRotation 	= <<7.9400, 5.0200, 48.3100>>
			fFOV 		= 35.0
			#IF IS_DEBUG_BUILD
			IF (bUseDebugValues)
				vCoords 	= debugData.CameraDebug.StageFourCamData.vCoords
				vRotation 	= debugData.CameraDebug.StageFourCamData.vRotation
				fFOV 		= debugData.CameraDebug.StageFourCamData.fFOV
			ENDIF
			#ENDIF
		BREAK
		CASE ST_CAMERA_STAGE_FIVE
			vCoords 	= <<1.4000, -2.0000, 1.1800>>
			vRotation 	= <<10.8000, 8.3200, 49.9200>>
			fFOV 		= 32.0
			#IF IS_DEBUG_BUILD
			IF (bUseDebugValues)
				vCoords 	= debugData.CameraDebug.StageFiveCamData.vCoords
				vRotation 	= debugData.CameraDebug.StageFiveCamData.vRotation
				fFOV 		= debugData.CameraDebug.StageFiveCamData.fFOV
			ENDIF
			#ENDIF
		BREAK
	ENDSWITCH
	
	IF (bUseWorldCoords)
		vCoords = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(Data.cabinetObj, vCoords)
		vRotation.z	+= GET_ENTITY_HEADING(Data.cabinetObj)
	ENDIF
ENDPROC

/// PURPOSE:
///    Gets the camera data based on the anim weight passed in.
/// PARAMS:
///    eCamType - The camera type the current anim weight is on.
///    fAnimWeight - The anim weight between 0-1.
///    vCoords - Coords for the camera stage.
///    vRotation - Rotation for the camera stage.
///    fFOV - FOV for the camera stage.
///    bUseWorldCoords - Return coords in world space or local space relative to the arcade cabinet machine.
PROC GET_STRENGTH_TEST_CAMERA_DATA_FROM_ANIM_WEIGHT(STRENGTH_TEST_CAMERAS eCamType, FLOAT fAnimWeight, VECTOR &vCoords, VECTOR &vRotation, FLOAT &fFOV, BOOL bUseWorldCoords = TRUE #IF IS_DEBUG_BUILD, BOOL bUseDebugValues = TRUE #ENDIF)
	
	FLOAT fStartingFOV
	VECTOR vStartingCoords
	VECTOR vStartingRotation
	STRENGTH_TEST_CAMERAS eStartingCam = eCamType
	
	FLOAT fEndingFOV
	VECTOR vEndingCoords
	VECTOR vEndingRotation
	STRENGTH_TEST_CAMERAS eEndingCam = GET_STRENGTH_TEST_STAGE_CAMERA_TYPE(ENUM_TO_INT(eCamType)+1)
	
	GET_STRENGTH_TEST_CAMERA_DATA(eStartingCam, vStartingCoords, vStartingRotation, fStartingFOV, bUseWorldCoords #IF IS_DEBUG_BUILD, bUseDebugValues #ENDIF)
	GET_STRENGTH_TEST_CAMERA_DATA(eEndingCam, vEndingCoords, vEndingRotation, fEndingFOV, bUseWorldCoords #IF IS_DEBUG_BUILD, bUseDebugValues #ENDIF)
	
	fFOV 		= INTERP_FLOAT(fStartingFOV, fEndingFOV, fAnimWeight, INT_TO_ENUM(INTERPOLATION_TYPE, Data.CameraData.iInterpolationType))
	vCoords		= INTERP_VECTOR(vStartingCoords, vEndingCoords, fAnimWeight, INT_TO_ENUM(INTERPOLATION_TYPE, Data.CameraData.iInterpolationType))
	vRotation	= INTERP_VECTOR(vStartingRotation, vEndingRotation, fAnimWeight, INT_TO_ENUM(INTERPOLATION_TYPE, Data.CameraData.iInterpolationType))
	
ENDPROC

/// PURPOSE:
///    Sets the Strength Test camera active state.
/// PARAMS:
///    camera - Cameras active state to set.
///    bActive - Active state.
PROC SET_STRENGTH_TEST_CAMERA_ACTIVE_STATE(CAMERA_INDEX &camera, BOOL bActive)
	IF DOES_CAM_EXIST(camera)
		SET_CAM_ACTIVE(camera, bActive)
	ENDIF
ENDPROC

/// PURPOSE:
///    Creates the Strength Test scripted camera. 
/// PARAMS:
///    eCamType - The camera type. Used to look up the camera type data and lerp the main camera to its position.
///    iInterpTimeMS - Lerp time to and from the gameplay camera.
///    bSetActiveOnCreation - Whether the camera should be set to active on creation.
///    bRenderOnCreation - Render the scripted camera on creation.
///    bPerformCameraInterp - Whether we should lerp to the scripted camera on creation.
PROC CREATE_STRENGTH_TEST_CAMERA(STRENGTH_TEST_CAMERAS eCamType, INT iInterpTimeMS = DEFAULT_INTERP_TO_FROM_GAME, BOOL bSetActiveOnCreation = TRUE, BOOL bRenderOnCreation = TRUE, BOOL bPerformCameraInterp = TRUE)
	FLOAT fFOV
	VECTOR vCoords
	VECTOR vRotation
	GET_STRENGTH_TEST_CAMERA_DATA(eCamType, vCoords, vRotation, fFOV)
	
	IF NOT DOES_CAM_EXIST(Data.CameraData.ciMainCamera)
		Data.CameraData.ciMainCamera = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, vCoords, vRotation, fFOV, bSetActiveOnCreation)
		SET_CAM_ACTIVE(Data.CameraData.ciMainCamera, TRUE)
		RENDER_SCRIPT_CAMS(bRenderOnCreation, bPerformCameraInterp, iInterpTimeMS)
		
		Data.CameraData.bFPCamera = FALSE
		IF (GET_CAM_VIEW_MODE_FOR_CONTEXT(GET_CAM_ACTIVE_VIEW_MODE_CONTEXT()) = CAM_VIEW_MODE_FIRST_PERSON)
			Data.CameraData.bFPCamera = TRUE
		ENDIF
		
		Data.CameraData.vFPCamPos 		= GET_FINAL_RENDERED_CAM_COORD()
		Data.CameraData.vFPCamRot 		= GET_FINAL_RENDERED_CAM_ROT()
		Data.CameraData.fFPCamFOV 		= GET_FINAL_RENDERED_CAM_FOV()
		Data.CameraData.fFPCamPitch 	= GET_GAMEPLAY_CAM_RELATIVE_PITCH()
		Data.CameraData.fFPCamHeading	= GET_ENTITY_HEADING(PLAYER_PED_ID())
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets the main Strength Test scripted camera as inactive and destroys it.
PROC CLEANUP_STRENGTH_TEST_CAMERA()
	IF DOES_CAM_EXIST(Data.CameraData.ciMainCamera)
		IF IS_CAM_ACTIVE(Data.CameraData.ciMainCamera)
			SET_CAM_ACTIVE(Data.CameraData.ciMainCamera, FALSE)
		ENDIF
		DESTROY_CAM(Data.CameraData.ciMainCamera)
	ENDIF
ENDPROC

/// PURPOSE:
///    Updates the amount of shake on the Strength Test scripted camera.
///    Shake gets stronger and closer together the further the power HUD progresses. 
/// PARAMS:
///    camera - Strength Test scripted camera.
///    fSwingWeight - The animation blended weight.
///    bShakeInstantly - TRUE: Shakes the camera instantly. FALSE: Shakes camera over time.
///    bImpactShake - Changes the shake type to SMALL_EXPLOSTION_SHAKE and triggers instantly.
PROC UPDATE_STRENGTH_TEST_CAMERA_SHAKE(CAMERA_INDEX &camera, FLOAT fSwingWeight, BOOL bShakeInstantly = TRUE, BOOL bImpactShake = FALSE)
	
	IF NOT DOES_CAM_EXIST(camera)
	#IF IS_DEBUG_BUILD
	OR (debugData.CameraDebug.bDisableShake)
	#ENDIF
		EXIT
	ENDIF
	
	FLOAT fMaxShake 	= tfST_CAMERA_MAX_SHAKE
	STRING sShakeType 	= "SMALL_EXPLOSION_SHAKE"
	
	#IF IS_DEBUG_BUILD
	sShakeType = DEBUG_GET_STRENGTH_TEST_CAMERA_TYPE(DebugData.CameraDebug.iShakeType)
	#ENDIF
	
	IF (bImpactShake)
		fMaxShake		= tfST_CAMERA_MAX_IMPACT_SHAKE
		sShakeType 		= "SMALL_EXPLOSION_SHAKE"
		fSwingWeight 	= NORMALISE_VALUE(TO_FLOAT(Data.GameplayData.iPropScore), 0.0, TO_FLOAT(ciST_MAX_PROP_SCORE))
		
		#IF IS_DEBUG_BUILD
		sShakeType = DEBUG_GET_STRENGTH_TEST_CAMERA_TYPE(DebugData.CameraDebug.iImpactShakeType)
		#ENDIF
	ENDIF
	
	IF (bShakeInstantly)
		// Shake on player input as HUD bar increasing
		SHAKE_CAM(camera, sShakeType, LERP_FLOAT(0.0, fMaxShake, fSwingWeight))
		RESET_NET_TIMER(Data.CameraData.stShakeTimer)
	ELSE
		// Shake over time when HUD bar decreasing
		IF NOT HAS_NET_TIMER_STARTED(Data.CameraData.stShakeTimer)
			START_NET_TIMER(Data.CameraData.stShakeTimer)
			Data.CameraData.iShakeIntervalTimeMS = LERP_INT(tiST_CAMERA_MAX_SHAKE_INTERVAL_TIME_MS, tiST_CAMERA_MIN_SHAKE_INTERVAL_TIME_MS, fSwingWeight)
		ELIF HAS_NET_TIMER_EXPIRED(Data.CameraData.stShakeTimer, Data.CameraData.iShakeIntervalTimeMS)
			RESET_NET_TIMER(Data.CameraData.stShakeTimer)
			SHAKE_CAM(camera, sShakeType, LERP_FLOAT(0.0, fMaxShake, fSwingWeight))
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Updates the Strength Test camera during the bashing stage.
/// PARAMS:
///    camera - Strength Test scripted camera.
///    fSwingWeight - The animation blended weight.
PROC UPDATE_STRENGTH_TEST_STAGE_CAMERA(CAMERA_INDEX &camera, FLOAT fSwingWeight)
	IF DOES_CAM_EXIST(camera)
		SET_CAM_COORD(camera, INTERP_VECTOR(data.CameraData.vStartingCoords, data.CameraData.vEndingCoords, fSwingWeight, INT_TO_ENUM(INTERPOLATION_TYPE, Data.CameraData.iInterpolationType)))
		SET_CAM_ROT(camera, INTERP_VECTOR(data.CameraData.vStartingRotation, data.CameraData.vEndingRotation, fSwingWeight, INT_TO_ENUM(INTERPOLATION_TYPE, Data.CameraData.iInterpolationType)))
		SET_CAM_FOV(camera, INTERP_FLOAT(data.CameraData.fStartingFOV, data.CameraData.fEndingFOV, fSwingWeight, INT_TO_ENUM(INTERPOLATION_TYPE, Data.CameraData.iInterpolationType)))
	ENDIF
ENDPROC

/// PURPOSE:
///    Maintains the Strength Test scripted camera during the bashing stage.
/// PARAMS:
///    fSwingWeight - The animation blended weight.
///    fAnimWeight - The animation weight based on the HUD progress. This is the raw value, fSwingWeight is the blended smooth animation value.
PROC MAINTAIN_STRENGTH_TEST_BASHING_STAGE_CAMERAS(FLOAT fSwingWeight, FLOAT fAnimWeight)
	
	// Stop updating if the catch stage as taken over camera control
	IF IS_BIT_SET(Data.iBS, BS_ST_LERP_DURING_CATCH_STAGE)
		EXIT
	ENDIF
	
	// Set lerp values from the cam start to end position
	IF NOT IS_BIT_SET(data.iBS, BS_ST_SET_CAMERA_LERP_VALUES)
		SET_BIT(data.iBS, BS_ST_SET_CAMERA_LERP_VALUES)
		GET_STRENGTH_TEST_CAMERA_DATA(ST_CAMERA_STARTING, data.CameraData.vStartingCoords, data.CameraData.vStartingRotation, data.CameraData.fStartingFOV)
		GET_STRENGTH_TEST_CAMERA_DATA(ST_CAMERA_STAGE_FOUR, data.CameraData.vEndingCoords, data.CameraData.vEndingRotation, data.CameraData.fEndingFOV)
	ENDIF
	
	// Stop the camera juddering when the animation weight is at zero
	IF (fSwingWeight <= cfST_MIN_SWING_WEIGHT AND fAnimWeight = cfST_MIN_SWING_WEIGHT)
		fSwingWeight = 0.0
	ENDIF
	
	// Lerp between stage cameras
	UPDATE_STRENGTH_TEST_STAGE_CAMERA(data.CameraData.ciMainCamera, fSwingWeight)
	
ENDPROC

/// PURPOSE:
///    Gets the anim blend time for the a bashing stage.
/// PARAMS:
///    iStage - Animation bashing stage.
/// RETURNS: The anim blend time in milliseconds.
FUNC INT GET_STRENGTH_TEST_ANIM_BLEND_TIME_MS(INT iStage)
	INT iOffset
	SWITCH iStage
		CASE 0	iOffset = tiST_ANIM_FINISH_BLEND_TIME_MS_STAGE_1	BREAK
		CASE 1	iOffset = tiST_ANIM_FINISH_BLEND_TIME_MS_STAGE_2	BREAK
		CASE 2	iOffset = tiST_ANIM_FINISH_BLEND_TIME_MS_STAGE_3	BREAK
		CASE 3	iOffset = tiST_ANIM_FINISH_BLEND_TIME_MS_STAGE_4	BREAK
		CASE 4	iOffset = tiST_ANIM_FINISH_BLEND_TIME_MS_STAGE_5	BREAK
		CASE 5	iOffset = tiST_ANIM_FINISH_BLEND_TIME_MS_STAGE_5	BREAK
	ENDSWITCH
	RETURN iOffset
ENDFUNC

/// PURPOSE:
///    Gets the camera anim weight offset value. Used during the Catch stage to offset the end camera lerp.
/// PARAMS:
///    iSwingStage - The swing stage achieved by the player during the bashing stage.
/// RETURNS: THE camera anim weight offset as a FLOAT.
FUNC FLOAT GET_STRENGTH_TEST_CATCH_CAMERA_ANIM_WEIGHT_OFFSET(INT iSwingStage)
	FLOAT fAnimOffset = tfST_CAMERA_ANIM_WEIGHT_OFFSET_STAGE_1
	SWITCH iSwingStage
		CASE 0	fAnimOffset = tfST_CAMERA_ANIM_WEIGHT_OFFSET_STAGE_1	BREAK
		CASE 1	fAnimOffset = tfST_CAMERA_ANIM_WEIGHT_OFFSET_STAGE_2	BREAK
		CASE 2	fAnimOffset = tfST_CAMERA_ANIM_WEIGHT_OFFSET_STAGE_3	BREAK
		CASE 3	fAnimOffset = tfST_CAMERA_ANIM_WEIGHT_OFFSET_STAGE_4	BREAK
		CASE 4	fAnimOffset = tfST_CAMERA_ANIM_WEIGHT_OFFSET_STAGE_5	BREAK
		CASE 5	fAnimOffset = tfST_CAMERA_ANIM_WEIGHT_OFFSET_STAGE_5	BREAK
	ENDSWITCH
	RETURN fAnimOffset
ENDFUNC

/// PURPOSE:
///    Validates a rotation angle and ensures it is between 0-360.
/// PARAMS:
///    fRotationAxis - Rotation axiz to validation.
PROC VALIDATE_ROTATION_ANGLE(FLOAT &fRotationAxis)
	fRotationAxis = NORMALIZE_ANGLE(fRotationAxis)
	IF (fRotationAxis < 0)
		fRotationAxis += 360.0
	ENDIF
ENDPROC

/// PURPOSE:
///    Maintain the camera during the catch stage.
PROC MAINTAIN_STRENGTH_TEST_CATCH_STAGE_CAMERA()
	
	IF NOT IS_BIT_SET(Data.iBS, BS_ST_LERP_DURING_CATCH_STAGE)
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(Data.iBS, BS_ST_SET_CAMERA_CATCH_STAGE_LERP_VALUES)
		SET_BIT(Data.iBS, BS_ST_SET_CAMERA_CATCH_STAGE_LERP_VALUES)
		
		data.CameraData.vStartingCoords 	= GET_CAM_COORD(Data.CameraData.ciMainCamera)
		data.CameraData.vStartingRotation 	= GET_CAM_ROT(Data.CameraData.ciMainCamera)
		data.CameraData.fStartingFOV 		= GET_CAM_FOV(Data.CameraData.ciMainCamera)
		VALIDATE_ROTATION_ANGLE(data.CameraData.vStartingRotation.z)
		
		// Get the swing weight between 0-4 (4 anims)
		FLOAT fSwingWeight 					= ((Data.HUDData.fHUDMeterProgress/TO_FLOAT(ciST_MAX_HUD_METER_VALUE))*TO_FLOAT(ciST_MAX_BASHING_STAGES))
		FLOAT fCameraAnimWeightOffset		= GET_STRENGTH_TEST_CATCH_CAMERA_ANIM_WEIGHT_OFFSET(FLOOR(fSwingWeight))
		fSwingWeight += fCameraAnimWeightOffset
		
		IF (fSwingWeight > (TO_FLOAT(ciST_MAX_BASHING_STAGES)+cfST_MAX_SWING_WEIGHT))
			fSwingWeight = (TO_FLOAT(ciST_MAX_BASHING_STAGES)+cfST_MAX_SWING_WEIGHT)
		ENDIF
		
		// Floor weight to get stage
		INT iSwingStage 					= FLOOR(fSwingWeight)
		
		// Get weight value between 0-1
		FLOAT fAnimWeight 					= (fSwingWeight - TO_FLOAT(iSwingStage))
		
		// Get the ending cam position based on the anim weight
		GET_STRENGTH_TEST_CAMERA_DATA_FROM_ANIM_WEIGHT(GET_STRENGTH_TEST_STAGE_CAMERA_TYPE(iSwingStage), fAnimWeight, data.CameraData.vEndingCoords, data.CameraData.vEndingRotation, data.CameraData.fEndingFOV)
		
		// Calculate the time (MS) until the swing animation plays (animation finish blend time + catch time)
		Data.CameraData.iTimeUntilSwingMS	= (GET_STRENGTH_TEST_ANIM_BLEND_TIME_MS(iSwingStage) + Data.GameplayData.iCatchTimeMS)
		Data.CameraData.tdCatchLerpTimer 	= GET_TIME_OFFSET(GET_NETWORK_TIME(), Data.CameraData.iTimeUntilSwingMS)
	ENDIF
	
	// Lerp the camera to the stage above during the catch stage
	IF IS_TIME_LESS_THAN(GET_NETWORK_TIME(), Data.CameraData.tdCatchLerpTimer)
		INT iTimeDifference 				= ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), Data.CameraData.tdCatchLerpTimer))
		FLOAT fAlpha						= 1.0-(TO_FLOAT(iTimeDifference)/Data.CameraData.iTimeUntilSwingMS)
		UPDATE_STRENGTH_TEST_STAGE_CAMERA(Data.CameraData.ciMainCamera, fAlpha)
		UPDATE_STRENGTH_TEST_CAMERA_SHAKE(Data.CameraData.ciMainCamera, Data.AnimData.fSwingWeight, FALSE)
	ENDIF
	
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ CLEANUP ╞════════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    Displays the Strength Test HUD bottom right.
/// PARAMS:
///    bDisplay - TRUE to display HUD.
PROC DISPLAY_STRENGTH_TEST_HUD(BOOL bDisplay)
	IF (bDisplay)
		IF (Data.HUDData.bDisplayHUD != TRUE)
			Data.HUDData.bDisplayHUD = TRUE
			PRINTLN("[AM_MP_ARCADE_STRENGTH_TEST] DISPLAY_STRENGTH_TEST_HUD - Turning HUD ON")
		ENDIF
	ELSE
		IF (Data.HUDData.bDisplayHUD != FALSE)
			Data.HUDData.bDisplayHUD = FALSE
			PRINTLN("[AM_MP_ARCADE_STRENGTH_TEST] DISPLAY_STRENGTH_TEST_HUD - Turning HUD OFF")
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Enabled the user input to affect the Strength Test HUD.
/// PARAMS:
///    bEnable - TRUE to enable user input.
PROC ENABLE_STRENGTH_TEST_HUD_INPUT(BOOL bEnable)
	IF (bEnable)
		IF (Data.HUDData.bEnableInput != TRUE)
			Data.HUDData.bEnableInput = TRUE
			PRINTLN("[AM_MP_ARCADE_STRENGTH_TEST] DISPLAY_STRENGTH_TEST_HUD - Enabling HUD Input")
		ENDIF
	ELSE
		IF (Data.HUDData.bEnableInput != FALSE)
			Data.HUDData.bEnableInput = FALSE
			PRINTLN("[AM_MP_ARCADE_STRENGTH_TEST] DISPLAY_STRENGTH_TEST_HUD - Disabling HUD Input")
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Clears all text labels associated with the Strength Test arcade game if any are displaying.
PROC CLEAR_STRENGTH_TEST_HELP_TEXT()
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("ARC_CAB_TRI_STRET")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("ARC_CAB_TRI_RSTRET")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("ARC_CAB_AOF_POW")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("ARC_CAB_AOF_POW_ALT")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("ARC_CAB_AOF_MLTPLR")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("ARC_CAB_AOF_MLTPLR_ALT")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("ARC_CAB_AOF_HUD_POW")
		CLEAR_HELP()
	ENDIF
ENDPROC

/// PURPOSE:
///    Reset Strength Test timers.
PROC CLEANUP_STRENGTH_TEST_TIMERS()
	RESET_NET_TIMER(Data.GameplayData.stCountdownTimer)
ENDPROC

/// PURPOSE:
///    Stops a strength test sound based on its sound ID.
/// PARAMS:
///    iSoundID - Sound to stop.
PROC STOP_STRENGTH_TEST_SOUND(INT &iSoundID)
	IF (iSoundID != -1)
		STOP_SOUND(iSoundID)
		RELEASE_SOUND_ID(iSoundID)
		iSoundID = -1
	ENDIF
ENDPROC

/// PURPOSE:
///    Stops any Strength Test audio and clears sounds IDs.
PROC CLEANUP_STRENGTH_TEST_AUDIO()
//	STOP_STRENGTH_TEST_SOUND(Data.AudioData.iSoundID)
	STOP_STRENGTH_TEST_SOUND(Data.AudioData.iAltSoundID)
	STOP_STRENGTH_TEST_SOUND(Data.AudioData.iStageSoundID)
	STOP_STRENGTH_TEST_SOUND(Data.AudioData.iPowerloopSoundID)
ENDPROC

/// PURPOSE:
///    Cleans up the Strength Test data.
///    Note: This does not clean up the render target data - that only gets cleaned up on property exit.
PROC CLEAR_STRENGTH_TEST_DATA()
	HUD_DATA 			blankHUDData
	ANIMATION_DATA 		blankAnimData
	AUDIO_DATA			blankAudioData
	GAMEPLAY_DATA		blankGameplayData
	
	Data.HUDData 		= blankHUDData
	Data.AnimData 		= blankAnimData
	Data.AudioData		= blankAudioData
	Data.GameplayData 	= blankGameplayData
	Data.iBS			= 0
	
	RESET_NET_TIMER(Data.stPlayDurationTimer)
ENDPROC

/// PURPOSE:
///    Releases the Strength Test UI scaleform movie.
PROC RELEASE_STRENGTH_TEST_UI_SCALEFORM_MOVIE()
	IF HAS_SCALEFORM_MOVIE_LOADED(Data.HUDData.siSwingMeter)
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(Data.HUDData.siSwingMeter)
	ENDIF
ENDPROC

/// PURPOSE:
///    Releases the Strength Test scoreboard scaleform movie.
PROC RELEASE_STRENGTH_TEST_SCOREBOARD_SCALEFORM_MOVIE()
	IF HAS_SCALEFORM_MOVIE_LOADED(Data.RTData.scoreboardScaleformID)
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(Data.RTData.scoreboardScaleformID)
	ENDIF
ENDPROC

/// PURPOSE:
///    Ensures the hammer prop is in the correct position.
PROC VALIDATE_STRENGTH_TEST_HAMMER_PROP_POSITION(ACM_CHILD_SCRIPT_LAUNCHER_STRUCT missionScriptArgs)
	IF DOES_ENTITY_EXIST(missionScriptArgs.cabinetObj)
	AND DOES_ENTITY_EXIST(missionScriptArgs.cabinetSubObj[ARCADE_CAB_OBJECT_1])
	AND NETWORK_HAS_CONTROL_OF_ENTITY(missionScriptArgs.cabinetSubObj[ARCADE_CAB_OBJECT_1])
		ACM_SUB_PROP_DETAILS eHammerDetails
		GET_STRENGTH_TEST_ARCADE_CABINET_SUB_PROP_DETAIL(ARCADE_CABINET_SUM_STRENGTH_TEST, eHammerDetails)
		eHammerDetails.vRotation[ARCADE_CAB_OBJECT_1].z = GET_ENTITY_HEADING(Data.cabinetObj)
		
		VECTOR vTargetHammerCoords		= GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(missionScriptArgs.cabinetObj, eHammerDetails.vOffset[ARCADE_CAB_OBJECT_1])
		VECTOR vTargetHammerRotation	= eHammerDetails.vRotation[ARCADE_CAB_OBJECT_1]
		
		SET_ENTITY_COORDS_NO_OFFSET(missionScriptArgs.cabinetSubObj[ARCADE_CAB_OBJECT_1], vTargetHammerCoords)
		SET_ENTITY_ROTATION(missionScriptArgs.cabinetSubObj[ARCADE_CAB_OBJECT_1], vTargetHammerRotation)
	ENDIF
ENDPROC

/// PURPOSE:
///    Cleans up the settings of the MoVE Network.
PROC CLEANUP_STRENGTH_TEST_MOVE_NETWORK()
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
		FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
		SET_USE_KINEMATIC_PHYSICS(PLAYER_PED_ID(), FALSE)
	ENDIF
	IF IS_TASK_MOVE_NETWORK_ACTIVE(PLAYER_PED_ID())
		SET_TASK_MOVE_NETWORK_ENABLE_COLLISION_ON_NETWORK_CLONE_WHEN_FIXED(PLAYER_PED_ID(), FALSE)
	ENDIF
ENDPROC

/// PURPOSE:
///    Cleanup for after using the Strength Test arcade game.
PROC CLEANUP_STRENGTH_TEST_GAME()
	DISPLAY_STRENGTH_TEST_HUD(FALSE)
	ENABLE_STRENGTH_TEST_HUD_INPUT(FALSE)
	RELEASE_STRENGTH_TEST_UI_SCALEFORM_MOVIE()
	REMOVE_STRENGTH_TEST_CLIP_SET()
	
	CLEANUP_STRENGTH_TEST_TIMERS()
	CLEANUP_STRENGTH_TEST_AUDIO()
	CLEAR_STRENGTH_TEST_HELP_TEXT()
	CLEAR_STRENGTH_TEST_DATA()
ENDPROC

/// PURPOSE:
///    Cleans up all Strength Test render targets and associated data.
PROC CLEANUP_RENDER_TARGETS()
	RELEASE_STRENGTH_TEST_SCOREBOARD_SCALEFORM_MOVIE()
	INT iRenderTarget
	REPEAT ST_RT_TOTAL iRenderTarget
		RELEASE_REGISTERED_RENDER_TARGET(GET_STRENGTH_TEST_RENDER_TARGET_NAME(INT_TO_ENUM(STRENGTH_TEST_RENDER_TARGET, iRenderTarget)))
	ENDREPEAT
	Data.RTData.iNeckRT 		= -1
	Data.RTData.iHeadboardRT 	= -1
	SET_STRENGTH_TEST_RENDER_TARGET_STATE(ST_RT_STATE_INVALID)
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(GET_STRENGTH_TEST_TEXTURE_DICTIONARY())
ENDPROC

/// PURPOSE:
///    Main clean up of script.
PROC SCRIPT_CLEANUP(ACM_CHILD_SCRIPT_LAUNCHER_STRUCT missionScriptArgs)
	PRINTLN("[AM_MP_ARCADE_STRENGTH_TEST] - SCRIPT_CLEANUP called")
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		SET_ENTITY_ALPHA(PLAYER_PED_ID(), 255, FALSE)
	ENDIF
	SET_LOCAL_PLAYER_FINISHED_STRENGTH_TEST_ARCADE_GAME(FALSE)
	CLEANUP_STRENGTH_TEST_GAME()
	CLEANUP_STRENGTH_TEST_CAMERA()
	CLEANUP_RENDER_TARGETS()
	DETACH_STRENGTH_TEST_HAMMER_FROM_LOCAL_PLAYER(missionScriptArgs)
	VALIDATE_STRENGTH_TEST_HAMMER_PROP_POSITION(missionScriptArgs)
	SET_TERMINATE_PHYSICAL_ARCADE_GAME_FLAG(ARCADE_CABINET_SUM_STRENGTH_TEST, FALSE)
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ INITIALISE ╞══════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    Initialise any Strength Test data. Called once on script launch.
/// PARAMS:
///    missionScriptArgs - Child script arcade data.
PROC INITIALISE(ACM_CHILD_SCRIPT_LAUNCHER_STRUCT missionScriptArgs)
	SET_TERMINATE_PHYSICAL_ARCADE_GAME_FLAG(ARCADE_CABINET_SUM_STRENGTH_TEST, FALSE)
	
	// Hide highscore "on" light
	SET_BIT(Data.iBS, BS_ST_HIGHSCORE_LIGHT_ON)	// Setting bit as light is on by default
	SET_STRENGTH_TEST_HIGHSCORE_LIGHT_STATE(missionScriptArgs, FALSE)
ENDPROC

/// PURPOSE:
///    Script initialise.
/// PARAMS:
///    missionScriptArgs - Child script arcade data.
PROC SCRIPT_INITIALISE(ACM_CHILD_SCRIPT_LAUNCHER_STRUCT missionScriptArgs)
	
	PRINTLN("[AM_MP_ARCADE_STRENGTH_TEST] - SCRIPT_INITIALISE - Launching instance: ", missionScriptArgs.iInstanceId)
	NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE, missionScriptArgs.iInstanceId)
	
	// This makes sure the net script is active, waits untill it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD, SIZE_OF(playerBD))

	// KGM: Wait for the first network broadcast before moving on
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		PRINTLN("[AM_MP_ARCADE_STRENGTH_TEST] - SCRIPT_INITIALISE FAILED TO RECEIVE INITIAL NETWORK BROADCAST - SCRIPT CLEANUP")
		SCRIPT_CLEANUP(missionScriptArgs)
	ENDIF
	
	Data.cabinetObj = missionScriptArgs.cabinetObj
	Data.eArcadeCabinetLocateType = missionScriptArgs.eArcadeCabinetLocateType
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		PRINTLN("[AM_MP_ARCADE_STRENGTH_TEST] INITIALISED arcade cabinet manager script")
		
		// This script will not be paused if another script calls PAUSE_GAME
		PRINTLN("[AM_MP_ARCADE_STRENGTH_TEST] - INITIALISED")
	ELSE
		PRINTLN("[AM_MP_ARCADE_STRENGTH_TEST] - INITIALISED NETWORK_IS_GAME_IN_PROGRESS = FALSE - SCRIPT CLEANUP")
		SCRIPT_CLEANUP(missionScriptArgs)
	ENDIF
	
	INITIALISE(missionScriptArgs)
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ SCALEFORM ╞═══════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    Sets the scaleform movie for the Strength Test score.
/// PARAMS:
///    scaleformID 				- Scaleform index.
///    iScore 					- Score to display.
///    fCountUpTimeSeconds 		- Time for score to count up in seconds.
///    fHoldTimeSeconds 		- Time for score to display for.
///    fCountDownTimeSeconds 	- Time for score to count down in seconds.
PROC SET_SCORE_SCALEFORM_MOVIE(SCALEFORM_INDEX &scaleformID, INT iScore, FLOAT fCountUpTimeSeconds, FLOAT fHoldTimeSeconds, FLOAT fCountDownTimeSeconds)
	BEGIN_SCALEFORM_MOVIE_METHOD(scaleformID, "SET_SCORE")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iScore)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fCountUpTimeSeconds)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fHoldTimeSeconds)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fCountDownTimeSeconds)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

/// PURPOSE:
///    Sets the scaleform movie for the Strength Test score instantly (without counting up etc).
/// PARAMS:
///    scaleformID 				- Scaleform index.
///    iScore 					- Score to display.
PROC SET_INSTANT_SCORE_SCALEFORM_MOVIE(SCALEFORM_INDEX &scaleformID, INT iScore)
	BEGIN_SCALEFORM_MOVIE_METHOD(scaleformID, "SET_INSTANT_SCORE")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iScore)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

/// PURPOSE:
///    Sets the scaleform movie for the Strength Test highscore.
/// PARAMS:
///    scaleformID 				- Scaleform index.
///    iScore 					- Highscore to display.
///    fCountUpTimeSeconds		- Time for highscore to count up in seconds.
///    fHoldTimeSeconds 		- Time for highscore to display for.
///    fCountDownTimeSeconds 	- Time for highscore to count down in seconds.
PROC SET_HIGHSCORE_SCALEFORM_MOVIE(SCALEFORM_INDEX &scaleformID, INT iScore, FLOAT fCountUpTimeSeconds, FLOAT fHoldTimeSeconds, FLOAT fCountDownTimeSeconds)
	BEGIN_SCALEFORM_MOVIE_METHOD(scaleformID, "SET_HIGH_SCORE")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iScore)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fCountUpTimeSeconds)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fHoldTimeSeconds)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fCountDownTimeSeconds)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

/// PURPOSE:
///    Sets the scaleform movie for the Strength Test highscore instantly (without counting up etc).
/// PARAMS:
///    scaleformID 				- Scaleform index.
///    iScore 					- Highscore to display.
PROC SET_INSTANT_HIGHSCORE_SCALEFORM_MOVIE(SCALEFORM_INDEX &scaleformID, INT iScore)
	BEGIN_SCALEFORM_MOVIE_METHOD(scaleformID, "SET_INSTANT_HIGH_SCORE")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iScore)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

/// PURPOSE:
///    Requests and loads the Strength Test swing meter UI scaleform movie.
/// RETURNS: TRUE when the scaleform movie has been requested and loaded.
FUNC BOOL REQUEST_AND_LOAD_STRENGTH_TEST_UI_SCALEFORM_MOVIE()
	Data.HUDData.siSwingMeter = REQUEST_SCALEFORM_MOVIE("AXE_OF_FURY")
	RETURN HAS_SCALEFORM_MOVIE_LOADED(Data.HUDData.siSwingMeter)
ENDFUNC

/// PURPOSE:
///    Draws the Strength Test swing meter UI on screen.
/// PARAMS:
///    bDraw - Active state. If the UI should draw this frame.
PROC DRAW_STRENGTH_TEST_SWING_METER(BOOL bDraw = TRUE)
	IF (bDraw)
		SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
		SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
		DRAW_SCALEFORM_MOVIE_FULLSCREEN(Data.HUDData.siSwingMeter, 255, 255, 255, 255)
		SET_TEXT_RENDER_ID(GET_DEFAULT_SCRIPT_RENDERTARGET_RENDER_ID())
	ENDIF
ENDPROC

/// PURPOSE:
///    Fades in the Strength Test UI swing meter.
PROC FADE_IN_STRENGTH_TEST_SWING_METER()
	BEGIN_SCALEFORM_MOVIE_METHOD(Data.HUDData.siSwingMeter, "SHOW")
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

/// PURPOSE:
///    Fades out the Strength Test UI swing meter.
PROC FADE_OUT_STRENGTH_TEST_SWING_METER()
	BEGIN_SCALEFORM_MOVIE_METHOD(Data.HUDData.siSwingMeter, "HIDE")
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

/// PURPOSE:
///    Validates the screen position to ensure it is between 0-1. Safety net for the various aspect ratios.
/// PARAMS:
///    fScreenAxis - X or Y Axis.
PROC VALIDATE_SWING_METER_SCREEN_POSITION(FLOAT &fScreenAxis)
	IF (fScreenAxis < 0.0)
		fScreenAxis = 0.0
	ELIF (fScreenAxis > 1.0)
		fScreenAxis = 1.0
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets the Strength Test swing meter UI on the screen.
/// PARAMS:
///    fScreenX - X axis.
///    fScreenY - Y axis.
PROC SET_STRENGTH_TEST_SWING_METER_SCREEN_POSITION(FLOAT fScreenX, FLOAT fScreenY)
	VALIDATE_SWING_METER_SCREEN_POSITION(fScreenX)
	VALIDATE_SWING_METER_SCREEN_POSITION(fScreenY)
	BEGIN_SCALEFORM_MOVIE_METHOD(Data.HUDData.siSwingMeter, "SET_SCREEN_POSITION")
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fScreenX)  
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fScreenY)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

/// PURPOSE:
///    Determines the button type on the Strength Test swing meter UI.
/// PARAMS:
///    bIsKeyboard - If the player is using mouse and keyboard.
PROC SET_STRENGTH_TEST_SWING_METER_BUTTON_TYPE(BOOL bIsKeyboard)
	BEGIN_SCALEFORM_MOVIE_METHOD(Data.HUDData.siSwingMeter, "SET_IS_KEYBOARD_CONTROL")
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bIsKeyboard)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

/// PURPOSE:
///    Sets the visible state of the Strength Test swing meter UI button.
/// PARAMS:
///    bVisible - Visble state. Defaulted to TRUE.
PROC SET_STRENGTH_TEST_SWING_METER_BUTTON_VISIBLE(BOOL bVisible = TRUE)
	BEGIN_SCALEFORM_MOVIE_METHOD(Data.HUDData.siSwingMeter, "SET_BUTTON_VISIBLE")
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bVisible)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

/// PURPOSE:
///    Sets the fill value of the Strength Test swing meter UI. This is the fill value that increases when the player button bashes.
/// PARAMS:
///    fNormalisedBarValue - The normalised fill value.
PROC SET_STRENGTH_TEST_SWING_METER_FILL_VALUE(FLOAT fNormalisedFillValue)
	BEGIN_SCALEFORM_MOVIE_METHOD(Data.HUDData.siSwingMeter, "SET_METER_FILL_VALUE")
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fNormalisedFillValue)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

/// PURPOSE:
///    Sets the bar value of the Strength Test swing meter UI. This is the catch bar that drops after the bashing stage.
/// PARAMS:
///    fNormalisedBarValue - The normalised bar value.
PROC SET_STRENGTH_TEST_SWING_METER_BAR_VALUE(FLOAT fNormalisedBarValue)
	BEGIN_SCALEFORM_MOVIE_METHOD(Data.HUDData.siSwingMeter, "SET_METER_BAR_VALUE")
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fNormalisedBarValue)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

/// PURPOSE:
///    Flashes the Strength Test swing meter UI. For when the player presses input during the catch stage.
PROC FLASH_STRENGTH_TEST_SWING_METER_BAR()
	BEGIN_SCALEFORM_MOVIE_METHOD(Data.HUDData.siSwingMeter, "FLASH_METER")
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

/// PURPOSE:
///    Sets the Strength Test swing meter UI catch bar line visible.
/// PARAMS:
///    bVisible - Visible state. Defaulted to TRUE.
PROC SET_STRENGTH_TEST_SWING_METER_BAR_LINE_VISIBLE(BOOL bVisible = TRUE)
	BEGIN_SCALEFORM_MOVIE_METHOD(Data.HUDData.siSwingMeter, "SET_METER_BAR_LINE_VISIBLE")
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bVisible)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

/// PURPOSE:
///    Sets the Strength Test swing meter UI catch bar button visible.
/// PARAMS:
///    bVisible - Visible state. Defaulted to TRUE.
PROC SET_STRENGTH_TEST_SWING_METER_BAR_BUTTON_VISIBLE(BOOL bVisible = TRUE)
	BEGIN_SCALEFORM_MOVIE_METHOD(Data.HUDData.siSwingMeter, "SET_METER_BAR_ICON_VISIBLE")
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bVisible)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

/// PURPOSE:
///    Flashes the Stregnth Test swing meter UI bashing button. Flashes on player input when bashing.
PROC FLASH_STRENGTH_TEST_SWING_METER_BUTTON()
	BEGIN_SCALEFORM_MOVIE_METHOD(Data.HUDData.siSwingMeter, "FLASH_BUTTON")
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════════╡ UI ╞══════════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    Scales a value between a new maximum and minimum scale.
/// PARAMS:
///    fMaxScaleValue 	- The maximum value to scale between
///    fMinScaleValue 	- The minimum value to scale between
///    fMaxValue 		- The max value of the current scaling. E.g Scaling 0-50 to 1000-5000. This value would be 50.
///    fMinValue 		- The max value of the current scaling. E.g Scaling 0-50 to 1000-5000. This value would be 0.
///    fCurrentValue 	- The current value we want scaled. This value is between the fMinValue and fMaxValue.
/// RETURNS: Scaled value.
FUNC FLOAT SCALE_BETWEEN_TWO_VALUES(FLOAT fMaxScaleValue, FLOAT fMinScaleValue, FLOAT fMaxValue, FLOAT fMinValue, FLOAT fCurrentValue)
	RETURN ((fMaxScaleValue-fMinScaleValue)*(fMaxValue-fCurrentValue))/(fMaxValue-fMinValue)
ENDFUNC

/// PURPOSE:
///    For the bashing segment - the resistance force needs to be the same force no matter how far up the HUD the player is.
/// RETURNS: Resistance force for the HUD.
FUNC FLOAT GET_STRENGTH_METER_HUD_RESISTANCE_TIME_MS()
	FLOAT fPercent = (Data.HUDData.fCurrentHUDMeterProgress/TO_FLOAT(ciST_MAX_HUD_METER_VALUE))*100.0
	RETURN (TO_FLOAT(ciST_MAX_HUD_METER_RESISTANCE_TIME_MS)/100.0)*fPercent
ENDFUNC

/// PURPOSE:
///    Caches the current HUD meter progress during the bashing stage.
/// PARAMS:
///    fProgress - Progress to cache.
PROC SET_STRENGTH_TEST_HUD_METER_PROGRESS(FLOAT fProgress)
	IF NOT IS_BIT_SET(Data.iBS, BS_ST_SET_HUD_METER_PROGRESS)
		SET_BIT(Data.iBS, BS_ST_SET_HUD_METER_PROGRESS)
		Data.HUDData.fCurrentHUDMeterProgress = fProgress
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets the duration timer for the bashing segment.
/// PARAMS:
///    iOffsetMS - Time for bashing segment.
PROC SET_STRENGTH_TEST_HUD_METER_TIMER(INT iOffsetMS)
	IF NOT IS_BIT_SET(Data.iBS, BS_ST_SET_HUD_METER_TIMER)
		SET_BIT(Data.iBS, BS_ST_SET_HUD_METER_TIMER)
		Data.HUDData.tdHUDMeterTimer = GET_TIME_OFFSET(GET_NETWORK_TIME(), iOffsetMS)
	ENDIF
ENDPROC

/// PURPOSE:
///    Maintains the HUD meter during the bashing stage.
/// PARAMS:
///    iTimeOffsetMS 		- Total time for bashing segment.
///    fBlendToValue 		- This is the players current HUD value. Need to know so we can blend down from it (resistance).
///    bSetFinalValue 		- Sets the final HUD value once the total bashing time has expired.
/// RETURNS: TRUE when the bashing time has expired and bashing segment is over, FALSE otherwise.
FUNC BOOL BLEND_STRENGTH_TEST_HUD_METER(INT iTimeOffsetMS, FLOAT fBlendToValue, BOOL bSetFinalValue = TRUE)
	SET_STRENGTH_TEST_HUD_METER_TIMER(iTimeOffsetMS)
	
	IF IS_TIME_LESS_THAN(GET_NETWORK_TIME(), Data.HUDData.tdHUDMeterTimer)
		INT iTimeDifference = ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), Data.HUDData.tdHUDMeterTimer))
		
		FLOAT fProgress = (TO_FLOAT(iTimeDifference)/iTimeOffsetMS)*fBlendToValue
		IF (fBlendToValue < Data.HUDData.fHUDMeterProgress)
			fProgress = fBlendToValue-((TO_FLOAT(iTimeDifference)/iTimeOffsetMS)*fBlendToValue)
		ENDIF
		
		Data.HUDData.fHUDMeterProgress = fProgress
	ELSE
		IF (bSetFinalValue)
			Data.HUDData.fHUDMeterProgress = fBlendToValue
		ENDIF
		
		CLEAR_BIT(Data.iBS, BS_ST_SET_HUD_METER_TIMER)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Drops the meter bar for the player to catch.
/// PARAMS:
///    iDropTimeMS - The drop time in milliseconds.
/// RETURNS: TRUE when the drop has finished.
FUNC BOOL PERFORM_STRENGTH_TEST_SWING_METER_DROP(INT iDropTimeMS)
	SET_STRENGTH_TEST_HUD_METER_TIMER(iDropTimeMS)
	
	IF IS_TIME_LESS_THAN(GET_NETWORK_TIME(), Data.HUDData.tdHUDMeterTimer)
		INT iTimeDifference = ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), Data.HUDData.tdHUDMeterTimer))
		Data.HUDData.fNormMeterBarValue = (TO_FLOAT(iTimeDifference)/iDropTimeMS)
		SET_STRENGTH_TEST_SWING_METER_BAR_VALUE(Data.HUDData.fNormMeterBarValue)
	ELSE
		Data.HUDData.fNormMeterBarValue = 0.0
		SET_STRENGTH_TEST_SWING_METER_BAR_VALUE(Data.HUDData.fNormMeterBarValue)
		CLEAR_BIT(Data.iBS, BS_ST_SET_HUD_METER_TIMER)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Maintains the Strength Test HUD resistance during the bashing stage.
PROC PROCESS_STRENGTH_TEST_HUD_METER_RESISTANCE()
	#IF IS_DEBUG_BUILD
	IF (DebugData.iAnimationPercentage != -1)
		EXIT
	ENDIF
	#ENDIF
	SET_STRENGTH_TEST_HUD_METER_PROGRESS(Data.HUDData.fHUDMeterProgress)
	IF BLEND_STRENGTH_TEST_HUD_METER(ROUND(GET_STRENGTH_METER_HUD_RESISTANCE_TIME_MS()), Data.HUDData.fCurrentHUDMeterProgress)
		CLEAR_BIT(Data.iBS, BS_ST_SET_HUD_METER_PROGRESS)
		Data.HUDData.fHUDMeterProgress = 0.0
	ENDIF
ENDPROC

/// PURPOSE:
///    The Swing Meter UI is always the English accept button.
///    PS4 = 'X'
///    Xbox = 'A'
///    This presents an issue between consoles for the Japanese version.
///    On the Japanese PS4 version the 'X' and 'O' buttons are remapped with each other.
///    On the Japanese Xbox version the 'A' and 'B' are NOT remapped with each other.
///    This functions ensures the correct button has been pressed based on the game version and console type.
/// RETURNS:
///    TRUE if the correct swing meter button has just been pressed. FALSE otherwise.
FUNC BOOL IS_SWING_METER_BUTTON_JUST_PRESSED()
	
	IF IS_JAPANESE_SPECIAL_EDITION_GAME()
		IF IS_XBOX_PLATFORM()
			RETURN IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
		ELSE
			RETURN IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
		ENDIF
	ELSE
		RETURN IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Sets the exclusive input for the swing meter button.
///    Based on the game version and console type.
///    See notes for function IS_SWING_METER_BUTTON_JUST_PRESSED for more info.
PROC SET_SWING_METER_INPUT_EXCLUSIVE()
	
	IF IS_JAPANESE_SPECIAL_EDITION_GAME()
		IF IS_XBOX_PLATFORM()
			SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
		ELSE
			SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
		ENDIF
	ELSE
		SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Prints the swing meter button prompt help text.
///    Based on the game version and console type.
///    See notes for function IS_SWING_METER_BUTTON_JUST_PRESSED for more info.   
PROC PRINT_SWING_METER_BUTTON_PROMPT_HELP_TEXT()
	
	IF IS_JAPANESE_SPECIAL_EDITION_GAME()
		IF IS_XBOX_PLATFORM()
			PRINT_HELP_FOREVER("ARC_CAB_AOF_POW")
		ELSE
			PRINT_HELP_FOREVER("ARC_CAB_AOF_POW_ALT")
		ENDIF
	ELSE
		PRINT_HELP_FOREVER("ARC_CAB_AOF_POW")
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Draws the Strength Test MUD meters and handles user input.
PROC MAINTAIN_STRENGTH_TEST_HUD()
	IF NOT Data.HUDData.bDisplayHUD
		EXIT
	ENDIF
	
	IF (Data.HUDData.bEnableInput AND NOT IS_PAUSE_MENU_ACTIVE())
		IF IS_SWING_METER_BUTTON_JUST_PRESSED()
			FLASH_STRENGTH_TEST_SWING_METER_BUTTON()
			Data.HUDData.fHUDMeterProgress += tfST_HUB_METER_INCREASE_INCREMENTS
			IF (Data.HUDData.fHUDMeterProgress >= TO_FLOAT(ciST_MAX_HUD_METER_VALUE))
				Data.HUDData.fHUDMeterProgress = TO_FLOAT(ciST_MAX_HUD_METER_VALUE)
			ENDIF
			CLEAR_BIT(Data.iBS, BS_ST_SET_HUD_METER_PROGRESS)
			CLEAR_BIT(Data.iBS, BS_ST_SET_HUD_METER_TIMER)
			UPDATE_STRENGTH_TEST_CAMERA_SHAKE(Data.CameraData.ciMainCamera, Data.AnimData.fSwingWeight)
			
			#IF IS_DEBUG_BUILD
			IF (NOT DebugData.bMutePlayerInputSound)
			#ENDIF
				TRIGGER_STRENGTH_TEST_SOUND("Button_Mash_Hit", Data.AudioData.iSoundID)
			#IF IS_DEBUG_BUILD
			ENDIF
			#ENDIF
		ELSE
			PROCESS_STRENGTH_TEST_HUD_METER_RESISTANCE()
			UPDATE_STRENGTH_TEST_CAMERA_SHAKE(Data.CameraData.ciMainCamera, Data.AnimData.fSwingWeight, FALSE)
		ENDIF
	ENDIF
	
	Data.HUDData.fNormMeterFillValue = NORMALISE_VALUE(Data.HUDData.fHUDMeterProgress, 0.0, TO_FLOAT(ciST_MAX_HUD_METER_VALUE))
	SET_STRENGTH_TEST_SWING_METER_FILL_VALUE(Data.HUDData.fNormMeterFillValue)
	DRAW_STRENGTH_TEST_SWING_METER(Data.HUDData.bDisplayHUD)
ENDPROC

/// PURPOSE:
///    Sets exclusive controls to use for the Strength Test arcade game.
PROC MAINTAIN_STRENGTH_TEST_CONTROLS()
	DISABLE_DPADDOWN_THIS_FRAME()
	DISABLE_SELECTOR_THIS_FRAME()
	SET_SWING_METER_INPUT_EXCLUSIVE()
	SET_FRONTEND_ACTIVE(FALSE)
	DISABLE_FRONTEND_THIS_FRAME()
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════╡ RENDER TARGET STATES ╞═════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    Draws the Strength Test headboard scaleform movie/background texture to display the score and highscore.
/// PARAMS:
///    bDrawScaleformScore - Draws the scaleform movie that contains the scores. Defaulted to TRUE.
PROC DRAW_STRENGTH_TEST_HEADBOARD(BOOL bDrawScaleformMovieScores = TRUE)
	SET_TEXT_RENDER_ID(Data.RTData.iHeadboardRT)
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
	SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
	DRAW_SPRITE_NAMED_RENDERTARGET(GET_STRENGTH_TEST_TEXTURE_DICTIONARY(), "SCORE_DISPLAY_AXE_OF_FURY", 0.5, 0.5, 1.0, 1.0, 0.0, 255, 255, 255, 255)
	IF (bDrawScaleformMovieScores)
		DRAW_SCALEFORM_MOVIE(Data.RTData.scoreboardScaleformID, 0.400, 0.375, 0.525, 0.575, 255, 255, 255, 255)
	ENDIF
	SET_TEXT_RENDER_ID(GET_DEFAULT_SCRIPT_RENDERTARGET_RENDER_ID())
ENDPROC

/// PURPOSE:
///    Draws a rectangle to the Strength Test neck based on player score.
PROC DRAW_STRENGTH_TEST_SCORE_NECK()
	SET_TEXT_RENDER_ID(Data.RTData.iNeckRT)
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
	SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
	DRAW_RECT(0.5, Data.RTData.fNeckScoreY, 1.0, 1.0, 255, 0, 0, 255)
	SET_TEXT_RENDER_ID(GET_DEFAULT_SCRIPT_RENDERTARGET_RENDER_ID())
ENDPROC

/// PURPOSE:
///    Wrapper function to draw to both the headboard and neck render targets.
PROC PERFORM_RT_STATE_DRAW()
	DRAW_STRENGTH_TEST_HEADBOARD()
	DRAW_STRENGTH_TEST_SCORE_NECK()
ENDPROC

/// PURPOSE:
///    Links and registers the Strength Test render targets.
///    NOTE: The neck render target is a seperate object as the code limit is one render target to one prop.
/// RETURNS: TRUE once the render targets have been linked and registered, FALSE if it is still trying to do so.
FUNC BOOL PERFORM_RT_STATE_LINK_RT()
	
	INT iRenderTarget = 0
	INT iRenderTargetsLinked = 0
	
	REPEAT ST_RT_TOTAL iRenderTarget
		IF REGISTER_AND_LINK_RENDER_TARGET(GET_STRENGTH_TEST_RENDER_TARGET_NAME(INT_TO_ENUM(STRENGTH_TEST_RENDER_TARGET, iRenderTarget)), GET_STRENGTH_TEST_RENDER_MODEL(INT_TO_ENUM(STRENGTH_TEST_RENDER_TARGET, iRenderTarget)))
			iRenderTargetsLinked++
		ENDIF
	ENDREPEAT
	
	IF (iRenderTargetsLinked = ENUM_TO_INT(ST_RT_TOTAL))
		Data.RTData.iNeckRT			= GET_RENDER_TARGET_ID(GET_STRENGTH_TEST_RENDER_TARGET_NAME(ST_RT_NECK))
		Data.RTData.iHeadboardRT	= GET_RENDER_TARGET_ID(GET_STRENGTH_TEST_RENDER_TARGET_NAME(ST_RT_HEADBOARD))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Request all assets associated with the Strength Test arcade game.
/// RETURNS: TRUE when all assets have been requested, FALSE otherwise.
FUNC BOOL PERFORM_RT_STATE_REQUEST_ASSETS()
	REQUEST_STREAMED_TEXTURE_DICT(GET_STRENGTH_TEST_TEXTURE_DICTIONARY())
	Data.RTData.scoreboardScaleformID = REQUEST_SCALEFORM_MOVIE(GET_STRENGTH_TEST_SCALEFORM_MOVIE_NAME())
	RETURN (HAS_SCALEFORM_MOVIE_LOADED(Data.RTData.scoreboardScaleformID) AND HAS_STREAMED_TEXTURE_DICT_LOADED(GET_STRENGTH_TEST_TEXTURE_DICTIONARY()))
ENDFUNC

/// PURPOSE:
///    Sets the highscore and clears the score back to 0000 in preparation for playing the arcade game.
/// RETURNS: TRUE once scores have been set, FALSE otherwise.
FUNC BOOL PERFORM_RT_STATE_INIT_SCORES()
	IF (Data.HighScoreData.iHighScore != -1)
		SET_INSTANT_HIGHSCORE_SCALEFORM_MOVIE(Data.RTData.scoreboardScaleformID, Data.HighScoreData.iHighScore)
		SET_INSTANT_SCORE_SCALEFORM_MOVIE(Data.RTData.scoreboardScaleformID, 0000)
		RETURN TRUE
	ENDIF
	PERFORM_RT_STATE_DRAW()
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Resets the timer on the headbaord back to zero.
PROC PERFORM_RT_STATE_RESET_TIMER()
	SET_INSTANT_SCORE_SCALEFORM_MOVIE(Data.RTData.scoreboardScaleformID, 0)
	PERFORM_RT_STATE_DRAW()
ENDPROC

/// PURPOSE:
///    Sets the timer on the headboard to the amount of seconds the player has to button bash.
PROC PERFORM_RT_STATE_SET_TIMER()
	SET_INSTANT_SCORE_SCALEFORM_MOVIE(Data.RTData.scoreboardScaleformID, ciST_COUNTDOWN_TIMER_MS)
	PERFORM_RT_STATE_DRAW()
ENDPROC

/// PURPOSE:
///    Starts the timer on the headboard to count down to zero.
PROC PERFORM_RT_STATE_START_TIMER()
	SET_SCORE_SCALEFORM_MOVIE(Data.RTData.scoreboardScaleformID, 0000, 5.0, 1.0, 5.0)
	PERFORM_RT_STATE_DRAW()
ENDPROC

/// PURPOSE:
///    Starts the score progressing upwards as the hammer hits the arcade machine.
PROC PERFORM_RT_STATE_PERFORM_SCORE()
	SET_SCORE_SCALEFORM_MOVIE(Data.RTData.scoreboardScaleformID, Data.RTData.iScore, Data.RTData.fCountUpTimeSecs, Data.RTData.fHoldTimeSecs, Data.RTData.fCountDownTimeSecs)
	PERFORM_RT_STATE_DRAW()
ENDPROC

/// PURPOSE:
///    Flash the new highscore three times.
/// PARAMS:
///    missionScriptArgs - Child script arcade data.
/// RETURNS: TRUE once flashing has finished, FALSE otherwise.
FUNC BOOL PERFORM_RT_STATE_SET_HIGHSCORE(ACM_CHILD_SCRIPT_LAUNCHER_STRUCT missionScriptArgs)
	
	DRAW_STRENGTH_TEST_SCORE_NECK()
	DRAW_STRENGTH_TEST_HEADBOARD(Data.HighScoreData.bRenderHighScoreFlash)
	SET_STRENGTH_TEST_HIGHSCORE_LIGHT_STATE(missionScriptArgs, Data.HighScoreData.bFlashHighscoreLight)
	
	IF HAS_NET_TIMER_EXPIRED(Data.HighScoreData.stHighScoreTimer, ciST_HIGHSCORE_FLASH_TIMER_MS)
		REINIT_NET_TIMER(Data.HighScoreData.stHighScoreTimer)
		Data.HighScoreData.bFlashHighscoreLight 		= !Data.HighScoreData.bFlashHighscoreLight
		Data.HighScoreData.bRenderHighScoreFlash 		= !Data.HighScoreData.bRenderHighScoreFlash
		Data.HighScoreData.iHighScoreFlashes++
		
		IF (NOT Data.HighScoreData.bRenderHighScoreFlash)
			SET_INSTANT_HIGHSCORE_SCALEFORM_MOVIE(Data.RTData.scoreboardScaleformID, Data.HighScoreData.iHighScore)
		ENDIF
		
		IF (Data.HighScoreData.iHighScoreFlashes > ciST_MAX_HIGHSCORE_FLASHES)
			RESET_NET_TIMER(Data.HighScoreData.stHighScoreTimer)
			Data.HighScoreData.bFlashHighscoreLight 	= TRUE
			Data.HighScoreData.bRenderHighScoreFlash	= TRUE
			Data.HighScoreData.iHighScoreFlashes 		= 0
			SET_STRENGTH_TEST_HIGHSCORE_LIGHT_STATE(missionScriptArgs, FALSE)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Calculate the Y axis target on the prop neck on where the meter should shoot up to.
/// PARAMS:
///    iOffsetMS 	- Time in milliseconds for the meter to ascend/descend.
///    bAscending 	- Is the meter ascending or descending.
PROC SET_STRENGTH_TEST_SCORE_TIMER_AND_PERFORM_SCORE_CALCULATION(INT iOffsetMS, BOOL bAscending)
	IF (NOT Data.RTData.bSetScoreTimer)
		Data.RTData.bSetScoreTimer = TRUE
		Data.RTData.tdScoreTimer = GET_TIME_OFFSET(GET_NETWORK_TIME(), iOffsetMS)
		IF (bAscending)
			Data.RTData.fTargetY = (((cfST_MAX_RT_SCORE_POSITION-cfST_MIN_RT_SCORE_POSITION)*TO_FLOAT(Data.RTData.iScore))/TO_FLOAT(ciST_MAX_PROP_SCORE))+cfST_MIN_RT_SCORE_POSITION
			IF (Data.RTData.iScore = ciST_MAX_PROP_SCORE)
				Data.RTData.fTargetY -= cfST_SCORE_Y_SEGMENT
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Performs the score shooting up the neck of the arcade machine prop
PROC MAINTAIN_STRENGTH_TEST_RENDER_TARGET_NECK()
	
	FLOAT fLerp
	FLOAT fDistanceToLerp
	INT iTimeDifference
	
	SWITCH Data.RTData.eScoreState
		CASE ST_RT_SCORE_STATE_ASCEND
			// New data as just been recieved via events and score is now greater than zero
			IF (Data.RTData.iScore > 0)
				
				// Sets timer and performs calculation on where render target should stop based on score. Wrapped in bit set so only does once.
				SET_STRENGTH_TEST_SCORE_TIMER_AND_PERFORM_SCORE_CALCULATION(ROUND(Data.RTData.fCountUpTimeSecs*1000), TRUE)
				
				// Perform lerp
				IF IS_TIME_LESS_THAN(GET_NETWORK_TIME(), Data.RTData.tdScoreTimer)
					iTimeDifference = ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), Data.RTData.tdScoreTimer))
					fDistanceToLerp = cfST_STARTING_Y_AXIS-Data.RTData.fTargetY
					fLerp = fDistanceToLerp-((TO_FLOAT(iTimeDifference)/(Data.RTData.fCountUpTimeSecs*1000))*fDistanceToLerp)
					Data.RTData.fNeckScoreY = cfST_STARTING_Y_AXIS-fLerp
				ELSE
					// Lerp has finished
					Data.RTData.bSetScoreTimer 	= FALSE
					Data.RTData.fNeckScoreY 	= Data.RTData.fTargetY
					SET_STRENGTH_TEST_RENDER_TARGET_SCORE_STATE(ST_RT_SCORE_STATE_HOLD)
				ENDIF
			ENDIF
		BREAK
		CASE ST_RT_SCORE_STATE_HOLD
			// Hold for the amount of time score is displayed
			IF HAS_NET_TIMER_EXPIRED(Data.RTData.stScoreHoldTimer, ROUND(Data.RTData.fHoldTimeSecs*1000.0))
				RESET_NET_TIMER(Data.RTData.stScoreHoldTimer)
				SET_STRENGTH_TEST_RENDER_TARGET_SCORE_STATE(ST_RT_SCORE_STATE_DESCEND)
			ENDIF
		BREAK
		CASE ST_RT_SCORE_STATE_DESCEND
			
			// Set the timer for descending the score
			SET_STRENGTH_TEST_SCORE_TIMER_AND_PERFORM_SCORE_CALCULATION(ROUND(Data.RTData.fCountDownTimeSecs*1000), FALSE)
			
			// Perform lerp
			IF IS_TIME_LESS_THAN(GET_NETWORK_TIME(), Data.RTData.tdScoreTimer)
				iTimeDifference = ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), Data.RTData.tdScoreTimer))
				fDistanceToLerp = cfST_STARTING_Y_AXIS-Data.RTData.fTargetY
				fLerp = (TO_FLOAT(iTimeDifference)/(Data.RTData.fCountDownTimeSecs*1000))*fDistanceToLerp
				Data.RTData.fNeckScoreY = cfST_STARTING_Y_AXIS-fLerp
			ELSE
				// Lerp has finished
				Data.RTData.bSetScoreTimer 	= FALSE
				Data.RTData.fNeckScoreY 	= cfST_STARTING_Y_AXIS
				Data.RTData.fTargetY 		= cfST_STARTING_Y_AXIS
				Data.RTData.iScore			= 0
				SET_STRENGTH_TEST_RENDER_TARGET_SCORE_STATE(ST_RT_SCORE_STATE_ASCEND)
			ENDIF
		BREAK
	ENDSWITCH
	
ENDPROC

/// PURPOSE:
///    Every player must register and link the Axe of Fury render targets on arcade entrance
///    in order to see the visual changes when someone is playing. Maintains the scaleform movies.
/// PARAMS:
///    missionScriptArgs - Child script arcade data.
PROC MAINTAIN_STRENGTH_TEST_RENDER_TARGET(ACM_CHILD_SCRIPT_LAUNCHER_STRUCT missionScriptArgs)
	SWITCH Data.eRTState
		CASE ST_RT_STATE_REQUEST_HIGHSCORE
			BROADCAST_STRENGTH_TEST_EVENT_REQUEST_HIGHSCORE()
			SET_STRENGTH_TEST_RENDER_TARGET_STATE(ST_RT_STATE_LINK_RT)
		BREAK
		CASE ST_RT_STATE_LINK_RT
			IF PERFORM_RT_STATE_LINK_RT()
				SET_STRENGTH_TEST_RENDER_TARGET_STATE(ST_RT_STATE_REQUEST_ASSETS)
			ENDIF
		BREAK
		CASE ST_RT_STATE_REQUEST_ASSETS
			IF PERFORM_RT_STATE_REQUEST_ASSETS()
				SET_STRENGTH_TEST_RENDER_TARGET_STATE(ST_RT_STATE_INIT_SCORES)
			ENDIF
		BREAK
		CASE ST_RT_STATE_INIT_SCORES
			IF PERFORM_RT_STATE_INIT_SCORES()
				SET_STRENGTH_TEST_RENDER_TARGET_STATE(ST_RT_STATE_DRAW)
			ENDIF
		BREAK
		CASE ST_RT_STATE_SET_TIMER
			PERFORM_RT_STATE_SET_TIMER()
			SET_STRENGTH_TEST_RENDER_TARGET_STATE(ST_RT_STATE_DRAW)
		BREAK
		CASE ST_RT_STATE_START_TIMER
			PERFORM_RT_STATE_START_TIMER()
			SET_STRENGTH_TEST_RENDER_TARGET_STATE(ST_RT_STATE_DRAW)
		BREAK
		CASE ST_RT_STATE_PERFORM_SCORE
			PERFORM_RT_STATE_PERFORM_SCORE()
			SET_STRENGTH_TEST_RENDER_TARGET_STATE(ST_RT_STATE_DRAW)
		BREAK
		CASE ST_RT_STATE_SET_HIGHSCORE
			IF PERFORM_RT_STATE_SET_HIGHSCORE(missionScriptArgs)
				SET_STRENGTH_TEST_RENDER_TARGET_STATE(ST_RT_STATE_DRAW)
			ENDIF
		BREAK
		CASE ST_RT_STATE_DRAW
			PERFORM_RT_STATE_DRAW()
		BREAK
	ENDSWITCH
	
	MAINTAIN_STRENGTH_TEST_RENDER_TARGET_NECK()
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════╡ PROCESS EVENTS ╞════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    Process the Strength Test events.
/// PARAMS:
///    eEventType 	- Event type to process. 
///    iEventID 	- ID of event.
PROC PROCESS_STRENGTH_TEST_EVENTS(SCRIPTED_EVENT_TYPES eEventType, INT iEventID)
	
	SWITCH eEventType
		CASE SCRIPT_EVENT_STRENGTH_TEST_SET_COUNTDOWN_TIMER
			SCRIPT_EVENT_DATA_STRENGTH_TEST_SET_COUNTDOWN_TIMER setCountdownEvent
			IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, setCountdownEvent, SIZE_OF(setCountdownEvent))
				IF NETWORK_IS_PLAYER_A_PARTICIPANT(setCountdownEvent.Details.FromPlayerIndex)
					SET_STRENGTH_TEST_RENDER_TARGET_STATE(ST_RT_STATE_SET_TIMER)
				ENDIF
			ENDIF
		BREAK
		CASE SCRIPT_EVENT_STRENGTH_TEST_START_COUNTDOWN_TIMER
			SCRIPT_EVENT_DATA_STRENGTH_TEST_START_COUNTDOWN_TIMER startCountdownEvent
			IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, startCountdownEvent, SIZE_OF(startCountdownEvent))
				IF NETWORK_IS_PLAYER_A_PARTICIPANT(startCountdownEvent.Details.FromPlayerIndex)
					SET_STRENGTH_TEST_RENDER_TARGET_STATE(ST_RT_STATE_START_TIMER)
				ENDIF
			ENDIF
		BREAK
		CASE SCRIPT_EVENT_STRENGTH_TEST_SEND_HIGHSCORE
			SCRIPT_EVENT_DATA_STRENGTH_TEST_SEND_HIGHSCORE sendHighscoreEvent
			IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, sendHighscoreEvent, SIZE_OF(sendHighscoreEvent))
				IF NETWORK_IS_PLAYER_A_PARTICIPANT(sendHighscoreEvent.Details.FromPlayerIndex)
					IF (Data.HighScoreData.iHighScore != sendHighscoreEvent.iHighscore)
						Data.HighScoreData.iHighScore = sendHighscoreEvent.iHighscore
						PRINTLN("[AM_MP_ARCADE_STRENGTH_TEST] PROCESS_STRENGTH_TEST_EVENTS - SCRIPT_EVENT_DATA_STRENGTH_TEST_SEND_HIGHSCORE - Received Event - Highscore: ", sendHighscoreEvent.iHighscore)
						IF (sendHighscoreEvent.bValidateHighscore)
							SET_INSTANT_HIGHSCORE_SCALEFORM_MOVIE(Data.RTData.scoreboardScaleformID, Data.HighScoreData.iHighScore)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE SCRIPT_EVENT_STRENGTH_TEST_SET_HIGHSCORE
			SCRIPT_EVENT_DATA_STRENGTH_TEST_SET_HIGHSCORE highscoreEvent
			IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, highscoreEvent, SIZE_OF(highscoreEvent))
				IF NETWORK_IS_PLAYER_A_PARTICIPANT(highscoreEvent.Details.FromPlayerIndex)
					Data.HighScoreData.iHighScore = highscoreEvent.iHighScore
					SET_STRENGTH_TEST_RENDER_TARGET_STATE(ST_RT_STATE_SET_HIGHSCORE)
				ENDIF
			ENDIF
		BREAK
		CASE SCRIPT_EVENT_STRENGTH_TEST_PERFORM_SCORE
			SCRIPT_EVENT_DATA_STRENGTH_TEST_PERFORM_SCORE scoreEvent
			IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, scoreEvent, SIZE_OF(scoreEvent))
				IF NETWORK_IS_PLAYER_A_PARTICIPANT(scoreEvent.Details.FromPlayerIndex)
					Data.RTData.iScore 							= scoreEvent.iScore
					Data.RTData.fHoldTimeSecs 					= scoreEvent.fHoldTimeSecs
					Data.RTData.fCountUpTimeSecs 				= scoreEvent.fCountUpTimeSecs
					Data.RTData.fCountDownTimeSecs 				= scoreEvent.fCountDownTimeSecs
					SET_STRENGTH_TEST_RENDER_TARGET_STATE(ST_RT_STATE_PERFORM_SCORE)
				ENDIF
			ENDIF
		BREAK
		CASE SCRIPT_EVENT_STRENGTH_TEST_MOVE_NETWORK_DATA
			SCRIPT_EVENT_DATA_STRENGTH_TEST_MOVE_NETWORK_DATA moveNetworkEvent
			IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, moveNetworkEvent, SIZE_OF(moveNetworkEvent))
				IF NETWORK_IS_PLAYER_A_PARTICIPANT(moveNetworkEvent.Details.FromPlayerIndex)
					Data.MoveNetworkData.arcadePlayerID			= moveNetworkEvent.playerID
					Data.MoveNetworkData.iClipSetHash			= moveNetworkEvent.iClipsetHash
					Data.MoveNetworkData.iClipSetVariableHash	= moveNetworkEvent.iClipsetVariableHash
					SET_BIT(Data.iBS, BS_ST_MOVE_NETWORK_INITIALISED)
					SET_BIT(Data.iBS2, BS2_ST_TASK_MOVE_PLAYER_AWAY_FROM_MACHINE)
					PRINTLN("[AM_MP_ARCADE_STRENGTH_TEST] PROCESS_STRENGTH_TEST_EVENTS - SCRIPT_EVENT_STRENGTH_TEST_MOVE_NETWORK_DATA - Received Event - Player: ", GET_PLAYER_NAME(moveNetworkEvent.playerID), " Player ID: ", NATIVE_TO_INT(moveNetworkEvent.playerID), " Clipset Hash: ", moveNetworkEvent.iClipsetHash, " Clipset Variable Hash: ", moveNetworkEvent.iClipsetVariableHash)
				ENDIF
			ENDIF
		BREAK
		CASE SCRIPT_EVENT_STRENGTH_TEST_RESET_PED_CAPSULE
			SCRIPT_EVENT_DATA_STRENGTH_TEST_RESET_PED_CAPSULE capsuleEvent
			IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, capsuleEvent, SIZE_OF(capsuleEvent))
				IF NETWORK_IS_PLAYER_A_PARTICIPANT(capsuleEvent.Details.FromPlayerIndex)
					CLEAR_BIT(Data.iBS2, BS2_ST_SET_ARCADE_PLAYER_CAPSULE_SIZE)
					PRINTLN("[AM_MP_ARCADE_STRENGTH_TEST] PROCESS_STRENGTH_TEST_EVENTS - SCRIPT_EVENT_STRENGTH_TEST_RESET_PED_CAPSULE - Received Event")
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
ENDPROC

/// PURPOSE:
///    Listens for any Strength Test events being received.
PROC PROCESS_EVENTS()
	
	INT iEventID
	EVENT_NAMES ThisScriptEvent
	STRUCT_EVENT_COMMON_DETAILS Details
	
	REPEAT GET_NUMBER_OF_EVENTS(SCRIPT_EVENT_QUEUE_NETWORK) iEventID
	    ThisScriptEvent = GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_NETWORK, iEventID)
	   	
	    SWITCH ThisScriptEvent
			CASE EVENT_NETWORK_SCRIPT_EVENT
				GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Details, SIZE_OF(Details))
				SWITCH Details.Type
					CASE SCRIPT_EVENT_STRENGTH_TEST_SET_COUNTDOWN_TIMER
						IF g_sBlockedEvents.bSCRIPT_EVENT_STRENGTH_TEST_SET_COUNTDOWN_TIMER
							EXIT
						ENDIF
						PROCESS_STRENGTH_TEST_EVENTS(Details.Type, iEventID)
					BREAK
					CASE SCRIPT_EVENT_STRENGTH_TEST_START_COUNTDOWN_TIMER
						IF g_sBlockedEvents.bSCRIPT_EVENT_STRENGTH_TEST_START_COUNTDOWN_TIMER
							EXIT
						ENDIF
						PROCESS_STRENGTH_TEST_EVENTS(Details.Type, iEventID)
					BREAK
					CASE SCRIPT_EVENT_STRENGTH_TEST_SEND_HIGHSCORE
						IF g_sBlockedEvents.bSCRIPT_EVENT_STRENGTH_TEST_SEND_HIGHSCORE
							EXIT
						ENDIF
						PROCESS_STRENGTH_TEST_EVENTS(Details.Type, iEventID)
					BREAK
					CASE SCRIPT_EVENT_STRENGTH_TEST_SET_HIGHSCORE
						IF g_sBlockedEvents.bSCRIPT_EVENT_STRENGTH_TEST_SET_HIGHSCORE
							EXIT
						ENDIF
						PROCESS_STRENGTH_TEST_EVENTS(Details.Type, iEventID)
					BREAK
					CASE SCRIPT_EVENT_STRENGTH_TEST_PERFORM_SCORE
						IF g_sBlockedEvents.bSCRIPT_EVENT_STRENGTH_TEST_PERFORM_SCORE
							EXIT
						ENDIF
						PROCESS_STRENGTH_TEST_EVENTS(Details.Type, iEventID)
					BREAK
					CASE SCRIPT_EVENT_STRENGTH_TEST_MOVE_NETWORK_DATA
						IF g_sBlockedEvents.bSCRIPT_EVENT_STRENGTH_TEST_MOVE_NETWORK_DATA
							EXIT
						ENDIF
						PROCESS_STRENGTH_TEST_EVENTS(Details.Type, iEventID)
					BREAK
					CASE SCRIPT_EVENT_STRENGTH_TEST_RESET_PED_CAPSULE
						IF g_sBlockedEvents.bSCRIPT_EVENT_STRENGTH_TEST_RESET_PED_CAPSULE
							EXIT
						ENDIF
						PROCESS_STRENGTH_TEST_EVENTS(Details.Type, iEventID)
					BREAK
				ENDSWITCH
			BREAK
		ENDSWITCH
	ENDREPEAT
	
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════════╡ GAME STATES ╞══════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    Determines if the local player is playing a Strength Test exit anim.
/// RETURNS: TRUE if an exit anim is playing, FALSE otherwise.
FUNC BOOL IS_LOCAL_PLAYER_PLAYING_ARCADE_EXIT_ANIM()
	IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), g_ArcadeCabinetManagerData.strArcCabinetAnimDic, "RESULT_PERFECT")
	OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), g_ArcadeCabinetManagerData.strArcCabinetAnimDic, "RESULT_GOOD")
	OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), g_ArcadeCabinetManagerData.strArcCabinetAnimDic, "RESULT_AVERAGE")
	OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), g_ArcadeCabinetManagerData.strArcCabinetAnimDic, "RESULT_BAD")
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Determines if the Strength Test arcade game should move into the idle state (game has been triggered) from the loading state.
/// RETURNS: TRUE if the state should change into Idle.
FUNC BOOL SHOULD_MOVE_TO_IDLE_STATE()
	IF !IS_PLAYER_USING_ARCADE_CABINET(PLAYER_ID())
		#IF IS_DEBUG_BUILD
		IF GET_FRAME_COUNT() % 60 = 0
			PRINTLN("[AM_MP_ARCADE_STRENGTH_TEST] - SHOULD_MOVE_TO_IDLE_STATE - IS_PLAYER_USING_ARCADE_CABINET is false")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF !IS_PLAYER_READY_TO_PLAY_ARCADE_GAME(PLAYER_ID())
		#IF IS_DEBUG_BUILD
		IF GET_FRAME_COUNT() % 60 = 0
			PRINTLN("[AM_MP_ARCADE_STRENGTH_TEST] - SHOULD_MOVE_TO_IDLE_STATE - IS_PLAYER_READY_TO_PLAY_ARCADE_GAME is false")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF GET_ARCADE_CABINET_LOCATE_PLAYER_IS_IN(PLAYER_ID()) != Data.eArcadeCabinetLocateType
		#IF IS_DEBUG_BUILD
		IF GET_FRAME_COUNT() % 60 = 0
			PRINTLN("[AM_MP_ARCADE_STRENGTH_TEST] - SHOULD_MOVE_TO_IDLE_STATE - player not in correct locate false")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_LOCAL_PLAYER_PLAYING_ARCADE_EXIT_ANIM()
		#IF IS_DEBUG_BUILD
		IF GET_FRAME_COUNT() % 60 = 0
			PRINTLN("[AM_MP_ARCADE_STRENGTH_TEST] - SHOULD_MOVE_TO_IDLE_STATE - player is playing exit anim")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	RETURN TRUE 
ENDFUNC

/// PURPOSE:
///    Get the name of score audio to play based on the players score.
/// PARAMS:
///    iScore - Players score.
/// RETURNS: Name of score audio as a text label.
FUNC TEXT_LABEL_63 GET_STRENGTH_TEST_SCORE_SPEECH(INT iScore)
	TEXT_LABEL_63 tlSpeech
	IF (iScore >= 1000 AND iScore <= 1100)
		tlSpeech = "STRENGTH_SCORE_1000_1100"
	ELIF (iScore >= 700 AND iScore <= 999)
		tlSpeech = "STRENGTH_SCORE_700_999"
	ELIF (iScore >= 500 AND iScore <= 699)
		tlSpeech = "STRENGTH_SCORE_500_699"
	ELIF (iScore >= 300 AND iScore <= 499)
		tlSpeech = "STRENGTH_SCORE_300_499"
	ELIF (iScore >= 100 AND iScore <= 299)
		tlSpeech = "STRENGTH_SCORE_100_299"
	ELIF (iScore >= 1 AND iScore <= 99)
		tlSpeech = "STRENGTH_SCORE_0_99"
	ELIF (iScore = 0)
		tlSpeech = "STRENGTH_SCORE_0"
	ENDIF
	RETURN tlSpeech
ENDFUNC

/// PURPOSE:
///    Gets the Strength Test leave speech time.
/// RETURNS: The Strength Test leave speech as a text label.
FUNC TEXT_LABEL_63 GET_STRENGTH_TEST_LEAVE_SPEECH()
	TEXT_LABEL_63 tlSpeech = "STRENGTH_TRY_AGAIN"
	IF (Data.iTempScore = ciST_MAX_PROP_SCORE)
		tlSpeech = GET_STRENGTH_TEST_SCORE_SPEECH(Data.iTempScore)
	ENDIF
	RETURN tlSpeech
ENDFUNC

/// PURPOSE:
///    Maintains the leave speech to trigger after playing the Strength Test arcade game.
PROC MAINTAIN_STRENGTH_TEST_LEAVE_SPEECH()
	IF HAS_NET_TIMER_STARTED_AND_EXPIRED(Data.SpeechData.stLeaveTimer, ciST_MAX_LEAVE_SPEECH_TIMER_MS)
		TEXT_LABEL_63 tlSpeech = GET_STRENGTH_TEST_LEAVE_SPEECH()
		TRIGGER_STRENGTH_TEST_DIALOGUE(tlSpeech, FALSE)
		IF (Data.SpeechData.eAIContext.bEventSent)
			CLEAR_BIT(Data.iBS, BS_ST_PLAY_LEAVE_SPEECH)
			RESET_NET_TIMER(Data.SpeechData.stLeaveTimer)
			RESET_NET_TIMER(Data.SpeechData.stAttractTimer)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Maintains the attract speech context for the Strength Test arcade game.
///    Triggers attract speech when inside Strength Test locate.
PROC MAINTAIN_STRENGTH_TEST_ATTRACT_SPEECH()
	IF NOT HAS_NET_TIMER_STARTED(Data.SpeechData.stAttractTimer)
		TEXT_LABEL_63 tlSpeech = "STRENGTH_ATTRACT"
		TRIGGER_STRENGTH_TEST_DIALOGUE(tlSpeech, FALSE)
		IF (Data.SpeechData.eAIContext.bEventSent)
			REINIT_NET_TIMER(Data.SpeechData.stAttractTimer)
		ENDIF
	ELSE
		IF HAS_NET_TIMER_EXPIRED(Data.SpeechData.stAttractTimer, ciST_MAX_ATTRACT_TIME_MS)
			RESET_NET_TIMER(Data.SpeechData.stAttractTimer)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Maintains the loading state of the Strength Test arcade game.
PROC MAINTAIN_STRENGTH_TEST_LOADING_STATE()
	IF SHOULD_MOVE_TO_IDLE_STATE()
		SET_LOCAL_PLAYER_FINISHED_STRENGTH_TEST_ARCADE_GAME(FALSE)
		SET_STRENGTH_TEST_GAME_STATE(ST_GAME_STATE_INITIALISE)
		SET_ARCADE_CABINET_CHILD_SCRIPT_CLIENT_STATE(Data.eState, ARCADE_CABINET_STATE_IDLE)
		RESET_NET_TIMER(Data.SpeechData.stLeaveTimer)
	ELSE
		// Leave Speech
		IF IS_BIT_SET(Data.iBS, BS_ST_PLAY_LEAVE_SPEECH)
			MAINTAIN_STRENGTH_TEST_LEAVE_SPEECH()
		ELSE
			// Attract Speech
			IF GET_ARCADE_CABINET_LOCATE_PLAYER_IS_IN(PLAYER_ID()) = ARCADE_CABINET_SUM_STRENGTH_TEST
				MAINTAIN_STRENGTH_TEST_ATTRACT_SPEECH()
			ELSE
				IF HAS_NET_TIMER_STARTED(Data.SpeechData.stAttractTimer)
					RESET_NET_TIMER(Data.SpeechData.stAttractTimer)
				ENDIF			
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Maintains the first person camera entry for the Strength Test arcade game.
/// PARAMS:
///    bSetPlayerInvisible - First person camera is clipping with the players head on camera lerp. 
///    						 Need to set the players alpha to zero for the first few frames of the camera lerp.
PROC MAINTAIN_STRENGTH_TEST_FIRST_PERSON_CAMERA_ENTRY(BOOL bSetPlayerInvisible = FALSE)
	IF (NOT Data.CameraData.bFPCamera)
		EXIT
	ENDIF
	
	IF (bSetPlayerInvisible)
		IF NOT HAS_NET_TIMER_STARTED(Data.CameraData.stFPCameraLerpTimer)
			START_NET_TIMER(Data.CameraData.stFPCameraLerpTimer)
			Data.CameraData.bFPCameraEntryLerpSet = TRUE
			
			IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
				SET_ENTITY_ALPHA(PLAYER_PED_ID(), 0, FALSE)
			ENDIF
		ENDIF
	ELSE
		IF (Data.CameraData.bFPCameraEntryLerpSet)
			IF HAS_NET_TIMER_STARTED_AND_EXPIRED(Data.CameraData.stFPCameraLerpTimer, tiST_CAMERA_FP_INVISIBLE_TIME_MS)
				RESET_NET_TIMER(Data.CameraData.stFPCameraLerpTimer)
				Data.CameraData.bFPCameraEntryLerpSet = FALSE
				
				IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
					SET_ENTITY_ALPHA(PLAYER_PED_ID(), 255, FALSE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Maintains the first person camera exit for the Strength Test arcade game.
///    Manually interps the camera back inside the players head. 
///    Sets the local players alpha to 0 as the camera approaches the head to avoid clipping. 
///    Sets local players alpha back to 255 when interp has completed.
/// PARAMS:
///    bBeginCameraLerp - Sets the interp values and begins the lerp.
PROC MAINTAIN_STRENGTH_TEST_FIRST_PERSON_CAMERA_EXIT(BOOL bBeginCameraLerp = FALSE)
	IF (NOT Data.CameraData.bFPCamera)
		EXIT
	ENDIF
	
	IF (bBeginCameraLerp)
		IF (NOT Data.CameraData.bFPCameraExitLerpSet)
			Data.CameraData.bFPCameraExitLerpSet = TRUE
			
			Data.CameraData.vStartingCoords 	= GET_CAM_COORD(Data.CameraData.ciMainCamera)
			Data.CameraData.vStartingRotation 	= GET_CAM_ROT(Data.CameraData.ciMainCamera)
			Data.CameraData.fStartingFOV 		= GET_CAM_FOV(Data.CameraData.ciMainCamera)
			VALIDATE_ROTATION_ANGLE(data.CameraData.vStartingRotation.z)
			
			Data.CameraData.vEndingCoords		= Data.CameraData.vFPCamPos
			Data.CameraData.vEndingRotation		= Data.CameraData.vFPCamRot
			Data.CameraData.fEndingFOV			= Data.CameraData.fFPCamFOV
			
			Data.CameraData.tdCatchLerpTimer 	= GET_TIME_OFFSET(GET_NETWORK_TIME(), tiST_CAMERA_ENDING_LERP_TIME_MS)
			Data.CameraData.iInterpolationType	= ENUM_TO_INT(INTERPTYPE_SMOOTHSTEP)
		ENDIF
	ELSE
		IF (Data.CameraData.bFPCameraExitLerpSet)
			
			INT iTimeDifference 				= ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), Data.CameraData.tdCatchLerpTimer))
			FLOAT fAlpha						= 1.0-(TO_FLOAT(iTimeDifference)/tiST_CAMERA_ENDING_LERP_TIME_MS)
			UPDATE_STRENGTH_TEST_STAGE_CAMERA(Data.CameraData.ciMainCamera, fAlpha)
			
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(Data.CameraData.fFPCamHeading)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(Data.CameraData.fFPCamPitch)
			FORCE_CAMERA_RELATIVE_HEADING_AND_PITCH(Data.CameraData.fFPCamPitch, Data.CameraData.fFPCamPitch)
			
			IF (fAlpha >= tfST_CAMERA_MAX_LERP_ALPHA)
				Data.CameraData.iInterpolationType		= ENUM_TO_INT(INTERPTYPE_SPHERICAL_SMOOTHSTEP)
				Data.CameraData.bFPCameraExitLerpSet	= FALSE
				
				RENDER_SCRIPT_CAMS(FALSE, FALSE)
				CLEANUP_STRENGTH_TEST_CAMERA()
				
				IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
					IF (GET_ENTITY_ALPHA(PLAYER_PED_ID()) != 255)
						SET_ENTITY_ALPHA(PLAYER_PED_ID(), 255, FALSE)
					ENDIF
				ENDIF
				
			ELIF (fAlpha >= tfST_CAMERA_HIDE_PLAYER_LERP_ALPHA AND fAlpha < tfST_CAMERA_MAX_LERP_ALPHA)
				IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
					IF (GET_ENTITY_ALPHA(PLAYER_PED_ID()) != 0)
						SET_ENTITY_ALPHA(PLAYER_PED_ID(), 0, FALSE)
					ENDIF
				ENDIF
			ENDIF
			
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Initialise function called after the game has been triggered.
PROC INITIALISE_STRENGTH_TEST()
	IF DOES_ENTITY_EXIST(Data.cabinetObj)
		Data.vCabinetCoords = GET_ENTITY_COORDS(Data.cabinetObj)
		Data.fCabinetHeading = GET_ENTITY_HEADING(Data.cabinetObj)
	ENDIF
	INITIALISE_STRENGTH_TEST_ANIM_DATA()
	TRIGGER_STRENGTH_TEST_SOUND("Start_Game", Data.AudioData.iSoundID)
	CREATE_STRENGTH_TEST_CAMERA(ST_CAMERA_STARTING, tiST_CAMERA_STARTING_LERP_TIME_MS)
	MAINTAIN_STRENGTH_TEST_FIRST_PERSON_CAMERA_ENTRY(TRUE)
	Data.SpeechData.iCachedDisplaySlot = GET_ARCADE_CABINET_LOCATE_DISPLAY_SLOT_PLAYER_IS_IN(PLAYER_ID())
	Data.HighscoreData.arcadeHost = NETWORK_GET_HOST_OF_SCRIPT("AM_MP_ARCADE", GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iInstance)
ENDPROC

/// PURPOSE:
///    Request control of the Strength Test hammer prop.
/// RETURNS: TRUE when the local player has control of the hammer prop.
FUNC BOOL DOES_LOCAL_PLAYER_HAVE_CONTROL_OF_ARCADE_PROP(ACM_CHILD_SCRIPT_LAUNCHER_STRUCT missionScriptArgs)
	IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(missionScriptArgs.cabinetSubObj[ARCADE_CAB_OBJECT_1])
		NETWORK_REQUEST_CONTROL_OF_ENTITY(missionScriptArgs.cabinetSubObj[ARCADE_CAB_OBJECT_1])
	ELSE
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Requests any assets needed to play the Strength Test arcade game, e.g. animations.
/// RETURNS: TRUE when assets have loaded.
FUNC BOOL PERFORM_STRENGTH_TEST_REQUEST_ASSETS(ACM_CHILD_SCRIPT_LAUNCHER_STRUCT missionScriptArgs)
	IF REQUEST_AND_LOAD_STRENGTH_TEST_ANIM_DICT()
	AND REQUEST_AND_LOAD_STRENGTH_TEST_CLIP_SET()
	AND REQUEST_AND_LOAD_STRENGTH_TEST_UI_SCALEFORM_MOVIE()
	AND DOES_LOCAL_PLAYER_HAVE_CONTROL_OF_ARCADE_PROP(missionScriptArgs)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    This function handles the entry procedure for the strength test minigame:
///    -Plays entry anim
///    -Attaches hammer prop to player ped
///    -Displays instructional help text
///    -Displays HUD metre for bashing mechanic
///    -Triggers the stage 1 looping anim once the entry anim has completed
///    -Triggers countdown timer
/// PARAMS:
///    missionScriptArgs - Need to reference the server BD props.
/// RETURNS: TRUE when the entry state has completed.
FUNC BOOL PERFORM_STRENGTH_TEST_ENTRY(ACM_CHILD_SCRIPT_LAUNCHER_STRUCT missionScriptArgs)
	
	// Trigger move network
	IF NOT IS_TASK_MOVE_NETWORK_ACTIVE(PLAYER_PED_ID())
		VECTOR vOffset[ARCADE_CAB_MAX_NUM_PLAYERS]
		GET_ARCADE_CABINET_TRIGGER_LOCATE_DATA(ARCADE_CABINET_SUM_STRENGTH_TEST, vOffset)
		
		STRING sAnimDict 				= GET_STRENGTH_TEST_ANIM_DICT()
		VECTOR vRotation 				= <<0.0, 0.0, GET_ENTITY_HEADING(Data.cabinetObj)>>
		VECTOR vCoords 					= GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(Data.cabinetObj, vOffset[ARCADE_CAB_PLAYER_1])
		
		MOVE_INITIAL_PARAMETERS initParams
		initParams.clipSetHash0			= GET_STRENGTH_TEST_MOVE_NETWORK_CLIP_SET_HASH()
		initParams.clipSetVariableHash0	= GET_STRENGTH_TEST_MOVE_NETWORK_CLIP_SET_VARIABLE_HASH()
		
		TASK_MOVE_NETWORK_ADVANCED_BY_NAME_WITH_INIT_PARAMS(PLAYER_PED_ID(), "Heist_Arcade_Strength_Hammer", initParams, vCoords, vRotation, DEFAULT, 0.5, FALSE, sAnimDict, MOVE_USE_KINEMATIC_PHYSICS)
		PLAY_FACIAL_ANIM(PLAYER_PED_ID(), GET_STRENGTH_TEST_FACIAL_ANIM_CLIP_NAME(ARCADE_CABINET_ANIM_CLIP_ENTRY), sAnimDict)
		
		// Freezing the player and hammer fixes issues of the hammer not lining up correctly in remote players eyes
		FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
		SET_USE_KINEMATIC_PHYSICS(PLAYER_PED_ID(), TRUE)
		
		IF DOES_ENTITY_EXIST(missionScriptArgs.cabinetSubObj[ARCADE_CAB_OBJECT_1])
		AND NETWORK_HAS_CONTROL_OF_ENTITY(missionScriptArgs.cabinetSubObj[ARCADE_CAB_OBJECT_1])
			FREEZE_ENTITY_POSITION(missionScriptArgs.cabinetSubObj[ARCADE_CAB_OBJECT_1], TRUE)
		ENDIF
		
		FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID(), DEFAULT, TRUE) 
		
		Data.AnimData.iStartTimeMS 		= GET_GAME_TIMER()
		Data.AnimData.iAnimDurationMS	= FLOOR(GET_ANIM_DURATION(GET_STRENGTH_TEST_ANIM_DICT(), "ENTER")*1000)
	ELSE
		
		// Set move network collision
		IF NOT IS_BIT_SET(Data.iBS2, BS2_ST_MOVE_NETWORK_ENABLE_COLLISION)
			SET_BIT(Data.iBS2, BS2_ST_MOVE_NETWORK_ENABLE_COLLISION)
			SET_TASK_MOVE_NETWORK_ENABLE_COLLISION_ON_NETWORK_CLONE_WHEN_FIXED(PLAYER_PED_ID(), TRUE)
		ENDIF
		
		// Wait until the move network clip set has been set
		IF IS_BIT_SET(Data.iBS2, BS2_ST_MOVE_NETWORK_CLIP_SET)
			
			// Transition to the stage loops after enter anim has finished.
			IF IS_TASK_MOVE_NETWORK_READY_FOR_TRANSITION(PLAYER_PED_ID())
				REQUEST_TASK_MOVE_NETWORK_STATE_TRANSITION(PLAYER_PED_ID(), "EnterFinish")
				SET_TASK_MOVE_NETWORK_SIGNAL_FLOAT(PLAYER_PED_ID(), "Weight", cfST_MIN_SWING_WEIGHT)
				PLAY_STRENGTH_TEST_FACIAL_ANIM(ARCADE_CABINET_ANIM_CLIP_STRENGTH_TEST_STAGE_1_LOOP)
				ENABLE_STRENGTH_TEST_HUD_INPUT(TRUE)
				BROADCAST_STRENGTH_TEST_EVENT_START_COUNTDOWN_TIMER()
				START_NET_TIMER(Data.GameplayData.stCountdownTimer)
				CLEAR_BIT(Data.iBS2, BS2_ST_MOVE_NETWORK_CLIP_SET)
				CLEAR_BIT(Data.iBS2, BS2_ST_MOVE_NETWORK_ENABLE_COLLISION)
				RETURN TRUE
			ENDIF
			
			// Attach hammer
			IF HAS_CURRENT_ANIM_PAST_PHASE(cfST_ATTACH_HAMMER_ANIM_PHASE_TIME)
				IF ATTACH_STRENGTH_TEST_HAMMER_TO_LOCAL_PLAYER(missionScriptArgs)
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("ARC_CAB_TRI_STRET")
						CLEAR_HELP()
					ENDIF
					
					// Display swing meter
					DISPLAY_STRENGTH_TEST_HUD(TRUE)
					SET_STRENGTH_TEST_SWING_METER_SCREEN_POSITION(ADJUST_X_COORD_FOR_ASPECT_RATIO(tfST_SWING_METER_SCREEN_X), tfST_SWING_METER_SCREEN_Y)
					DRAW_STRENGTH_TEST_SWING_METER()
					FADE_IN_STRENGTH_TEST_SWING_METER()
					SET_STRENGTH_TEST_SWING_METER_BAR_VALUE(Data.HUDData.fNormMeterBarValue)
					SET_STRENGTH_TEST_SWING_METER_BAR_LINE_VISIBLE()
					
					// Set swing meter button type
					SET_STRENGTH_TEST_SWING_METER_BUTTON_TYPE(IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL))
					SET_STRENGTH_TEST_SWING_METER_BUTTON_VISIBLE()
					PRINT_SWING_METER_BUTTON_PROMPT_HELP_TEXT()
					BROADCAST_STRENGTH_TEST_EVENT_SET_COUNTDOWN_TIMER()
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Instead of setting anim weight directly to fAnimWeight (bashing value), adjust the
///    fSwingWeight variable to match fAnimWeight by tiny increments for smooth blending.
/// PARAMS:
///    fAnimWeight - Anim weight to set.
PROC SET_STRENGTH_TEST_HAMMER_ANIM_WEIGHT(FLOAT fAnimWeight)
	IF (Data.AnimData.fSwingWeight <= cfST_MIN_SWING_WEIGHT AND fAnimWeight = cfST_MIN_SWING_WEIGHT)
		Data.AnimData.fSwingWeight = 0.0
	ELIF (Data.AnimData.fSwingWeight < fAnimWeight)
		Data.AnimData.fSwingWeight += tfST_SWING_ANIM_WEIGHT_INCREASE
	ELIF (Data.AnimData.fSwingWeight > fAnimWeight)
		Data.AnimData.fSwingWeight -= tfST_SWING_ANIM_WEIGHT_DECREASE
	ENDIF
	
	// Update MoVE Network 0-1. Can't spam every frame
	IF IS_TASK_MOVE_NETWORK_ACTIVE(PLAYER_PED_ID())
		SET_TASK_MOVE_NETWORK_SIGNAL_FLOAT(PLAYER_PED_ID(), "Weight", Data.AnimData.fSwingWeight)
	ENDIF
ENDPROC

/// PURPOSE:
///    Gets the current bashing stage sound.
/// PARAMS:
///    iSound - Sound segment to play.
/// RETURNS: Bashing stage sound clip name.
FUNC STRING GET_STRENGTH_TEST_BASHING_STAGE_SOUND(INT iSound)
	// Note: TEXT_LABEL_TO_STRING was returning the string with garbage attached.
	STRING sSound = ""
	SWITCH iSound
		CASE 0	sSound = "Stage_00"	BREAK
		CASE 1	sSound = "Stage_01"	BREAK
		CASE 2	sSound = "Stage_02"	BREAK
		CASE 3	sSound = "Stage_03"	BREAK
		CASE 4	sSound = "Stage_04"	BREAK
		CASE 5	sSound = "Stage_05"	BREAK
		CASE 6	sSound = "Stage_06"	BREAK
		CASE 7	sSound = "Stage_07"	BREAK
	ENDSWITCH
	RETURN sSound
ENDFUNC

/// PURPOSE:
///    Gets the audio bit set for the bashing stage.
/// PARAMS:
///    iStage - Audio bashing stage.
/// RETURNS: The correct bit set for the audio bashing stage.
FUNC INT GET_STRENGTH_TEST_BASHING_AUDIO_BIT_SET(INT iStage)
	INT iBitSet = BS_ST_BASHING_AUDIO_STAGE_0
	SWITCH iStage
		CASE 0	iBitSet = BS_ST_BASHING_AUDIO_STAGE_0	BREAK
		CASE 1	iBitSet = BS_ST_BASHING_AUDIO_STAGE_1	BREAK
		CASE 2	iBitSet = BS_ST_BASHING_AUDIO_STAGE_2	BREAK
		CASE 3	iBitSet = BS_ST_BASHING_AUDIO_STAGE_3	BREAK
		CASE 4	iBitSet = BS_ST_BASHING_AUDIO_STAGE_4	BREAK
		CASE 5	iBitSet = BS_ST_BASHING_AUDIO_STAGE_5	BREAK
		CASE 6	iBitSet = BS_ST_BASHING_AUDIO_STAGE_6	BREAK
		CASE 7	iBitSet = BS_ST_BASHING_AUDIO_STAGE_7	BREAK
	ENDSWITCH
	RETURN iBitSet
ENDFUNC

/// PURPOSE:
///    Plays a bashing stage sound based on players input in the progression bar.
/// PARAMS:
///    iSoundID - Which sound ID should play these sounds.
PROC MAINTAIN_STRENGTH_TEST_BASHING_STAGE_SOUNDS(INT &iSoundID)
	
	FLOAT fSegment = TO_FLOAT(ciST_MAX_HUD_METER_VALUE)/cfST_MAX_BASHING_SOUND_STAGES
	
	IF (Data.HUDData.fHUDMeterProgress >= fSegment*(Data.AudioData.iAudioBashingStage+1) AND NOT IS_BIT_SET(Data.iBS, GET_STRENGTH_TEST_BASHING_AUDIO_BIT_SET(Data.AudioData.iAudioBashingStage)))
		TRIGGER_STRENGTH_TEST_SOUND(GET_STRENGTH_TEST_BASHING_STAGE_SOUND(Data.AudioData.iAudioBashingStage), iSoundID)
		SET_BIT(Data.iBS, GET_STRENGTH_TEST_BASHING_AUDIO_BIT_SET(Data.AudioData.iAudioBashingStage))
		Data.AudioData.iAudioBashingStage += 1
		IF (Data.AudioData.iAudioBashingStage > ROUND(cfST_MAX_BASHING_SOUND_STAGES)-1)
			Data.AudioData.iAudioBashingStage = ROUND(cfST_MAX_BASHING_SOUND_STAGES)-1
		ENDIF
		
	ELIF (Data.HUDData.fHUDMeterProgress < fSegment*(Data.AudioData.iAudioBashingStage) AND IS_BIT_SET(Data.iBS, GET_STRENGTH_TEST_BASHING_AUDIO_BIT_SET(Data.AudioData.iAudioBashingStage-1)))
		Data.AudioData.iAudioBashingStage -= 1
		IF (Data.AudioData.iAudioBashingStage < 0)
			Data.AudioData.iAudioBashingStage = 0
		ENDIF
		TRIGGER_STRENGTH_TEST_SOUND(GET_STRENGTH_TEST_BASHING_STAGE_SOUND(Data.AudioData.iAudioBashingStage), iSoundID)
		CLEAR_BIT(Data.iBS, GET_STRENGTH_TEST_BASHING_AUDIO_BIT_SET(Data.AudioData.iAudioBashingStage))
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Blends up the move network from 0-1.
///    Does the reverse on the way down.
/// RETURNS: TRUE once the bashing segment timer has expired.
FUNC BOOL PERFORM_STRENGTH_TEST_SWING_BASH()
	
	// Get the swing weight between 0-1
	FLOAT fSwingWeight 	= (Data.HUDData.fHUDMeterProgress/TO_FLOAT(ciST_MAX_HUD_METER_VALUE))
	
	// Anim phase must be greater than 0 to play
	IF (fSwingWeight <= 0.0)
		fSwingWeight = cfST_MIN_SWING_WEIGHT
	ENDIF
	
	// Play one off Start Bashing sound
	IF NOT IS_BIT_SET(Data.iBS, BS_ST_PLAY_START_BASHING_SOUND)
		SET_BIT(Data.iBS, BS_ST_PLAY_START_BASHING_SOUND)
		TRIGGER_STRENGTH_TEST_SOUND("Start_Mashing", Data.AudioData.iAltSoundID)
	ENDIF
	
	// Changing the weight blends the anims up and down
	SET_STRENGTH_TEST_HAMMER_ANIM_WEIGHT(fSwingWeight)
	
	// Maintain cameras for bashing stage
	MAINTAIN_STRENGTH_TEST_BASHING_STAGE_CAMERAS(Data.AnimData.fSwingWeight, fSwingWeight)
	
	// Maintain facial animations during the bashing stage
	MAINTAIN_STRENGTH_TEST_BASHING_STAGE_FACIAL_ANIMS(fSwingWeight)
	
	// Check if the max HUD limit has been reached
	IF (Data.HUDData.fHUDMeterProgress = TO_FLOAT(ciST_MAX_HUD_METER_VALUE))
		IF NOT IS_BIT_SET(Data.iBS, BS_ST_HUD_METER_REACHED_MAX_LIMIT)
			SET_BIT(Data.iBS, BS_ST_HUD_METER_REACHED_MAX_LIMIT)
		ENDIF
	ENDIF
	
	// Audio for powering up/down
	IF NOT IS_BIT_SET(Data.iBS, BS_ST_PLAY_POWERING_UP_SOUND)
		SET_BIT(Data.iBS, BS_ST_PLAY_POWERING_UP_SOUND)
		TRIGGER_STRENGTH_TEST_SOUND("Power_Level_Loop", Data.AudioData.iPowerloopSoundID)
	ELSE
		// Param value between 0-1
		SET_STRENGTH_TEST_SOUND_PARAM("Power_Level", Data.AudioData.iPowerloopSoundID, fSwingWeight)
	ENDIF
	
	// Maintain stage audio
	#IF IS_DEBUG_BUILD
	IF (NOT DebugData.bMuteBashingStageSound)
	#ENDIF
		MAINTAIN_STRENGTH_TEST_BASHING_STAGE_SOUNDS(Data.AudioData.iStageSoundID)
	#IF IS_DEBUG_BUILD
	ENDIF
	#ENDIF
	
	// Debug hold on this stage
	#IF IS_DEBUG_BUILD
	IF (DebugData.bHoldOnBashStage)
		RETURN FALSE
	ENDIF
	#ENDIF
	
	// Countdown timer has expired
	IF HAS_NET_TIMER_EXPIRED(Data.GameplayData.stCountdownTimer, ciST_COUNTDOWN_TIMER_MS)
		ENABLE_STRENGTH_TEST_HUD_INPUT(FALSE)
		
		// Clear current help text
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("ARC_CAB_AOF_POW")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("ARC_CAB_AOF_POW_ALT")
			CLEAR_HELP()
		ENDIF
		
		// Display swing meter catch bar and show help text
		IF IS_JAPANESE_SPECIAL_EDITION_GAME()
			IF IS_XBOX_PLATFORM()
				IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("ARC_CAB_AOF_MLTPLR")
				AND (Data.HUDData.fNormMeterFillValue > tfST_SWING_METER_MIN_CATCH_VALUE)
					SET_STRENGTH_TEST_SWING_METER_BAR_BUTTON_VISIBLE()
					PRINT_HELP_FOREVER("ARC_CAB_AOF_MLTPLR")
				ENDIF
			ELSE
				IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("ARC_CAB_AOF_MLTPLR_ALT")
				AND (Data.HUDData.fNormMeterFillValue > tfST_SWING_METER_MIN_CATCH_VALUE)
					SET_STRENGTH_TEST_SWING_METER_BAR_BUTTON_VISIBLE()
					PRINT_HELP_FOREVER("ARC_CAB_AOF_MLTPLR_ALT")
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("ARC_CAB_AOF_MLTPLR")
			AND (Data.HUDData.fNormMeterFillValue > tfST_SWING_METER_MIN_CATCH_VALUE)
				SET_STRENGTH_TEST_SWING_METER_BAR_BUTTON_VISIBLE()
				PRINT_HELP_FOREVER("ARC_CAB_AOF_MLTPLR")
			ENDIF
		ENDIF
		
		// Hide swing meter button
		IF NOT IS_BIT_SET(Data.iBS, BS_ST_SWING_METER_HIDE_BUTTON)
			SET_BIT(Data.iBS, BS_ST_SWING_METER_HIDE_BUTTON)
			SET_STRENGTH_TEST_SWING_METER_BUTTON_VISIBLE(FALSE)
		ENDIF
			
		// Make sure progress is max if at end of HUD meter
		IF NOT IS_BIT_SET(Data.iBS, BS_ST_SET_FINAL_HUD_METER_VALUE)
			SET_BIT(Data.iBS, BS_ST_SET_FINAL_HUD_METER_VALUE)
			IF (Data.HUDData.fHUDMeterProgress > TO_FLOAT(ciST_MAX_HUD_METER_VALUE-FLOOR(tfST_HUB_METER_INCREASE_INCREMENTS)))
			AND IS_BIT_SET(Data.iBS, BS_ST_HUD_METER_REACHED_MAX_LIMIT)
				Data.HUDData.fHUDMeterProgress = TO_FLOAT(ciST_MAX_HUD_METER_VALUE)
			ENDIF
		ENDIF
		
		// Start safety timer as backup in case anim blend takes too long
		IF NOT IS_BIT_SET(Data.iBS, BS_ST_SET_BLEND_SAFETY_TIMER)
			SET_BIT(Data.iBS, BS_ST_SET_BLEND_SAFETY_TIMER)
			START_NET_TIMER(Data.GameplayData.stBlendSafetyTimer)
		ENDIF
		
		// Continue to lerp camera during the ST_GAME_STATE_SWING_CATCH stage
		IF (Data.HUDData.fNormMeterFillValue > tfST_SWING_METER_MIN_CATCH_VALUE)
		AND NOT IS_BIT_SET(Data.iBS, BS_ST_LERP_DURING_CATCH_STAGE)
			SET_BIT(Data.iBS, BS_ST_LERP_DURING_CATCH_STAGE)
		ENDIF
		
		// Ensure hammer has finished blending before continuing
		IF IS_FLOAT_IN_RANGE(Data.AnimData.fSwingWeight, (fSwingWeight-cfST_FINAL_ANIM_WEIGHT_BUFFER), (fSwingWeight+cfST_FINAL_ANIM_WEIGHT_BUFFER))
		OR HAS_NET_TIMER_STARTED_AND_EXPIRED(Data.GameplayData.stBlendSafetyTimer, ciST_ANIM_BLEND_SAFETY_TIME_MS)
			RESET_NET_TIMER(Data.GameplayData.stCountdownTimer)
			RESET_NET_TIMER(Data.GameplayData.stBlendSafetyTimer)
			CLEAR_BIT(Data.iBS, BS_ST_SET_BLEND_SAFETY_TIMER)
			CLEAR_BIT(Data.iBS, BS_ST_SET_HUD_METER_PROGRESS)
			CLEAR_BIT(Data.iBS, BS_ST_SET_HUD_METER_TIMER)
			Data.GameplayData.fScore = Data.HUDData.fHUDMeterProgress
			
			CLEAR_BIT(Data.iBS, BS_ST_PLAY_START_BASHING_SOUND)
			CLEAR_BIT(Data.iBS, BS_ST_HUD_METER_REACHED_MAX_LIMIT)
			CLEAR_BIT(Data.iBS, BS_ST_SET_FINAL_HUD_METER_VALUE)
			CLEAR_BIT(Data.iBS, BS_ST_SWING_METER_HIDE_BUTTON)
			RETURN TRUE
		ENDIF
	
	// Trigger hype speech
	ELIF HAS_NET_TIMER_EXPIRED(Data.GameplayData.stCountdownTimer, ciST_MAX_HYPE_SPEECH_TIMER_MS)
		IF NOT IS_BIT_SET(Data.iBS, BS_ST_PLAY_HYPE_SPEECH)
			SET_BIT(Data.iBS, BS_ST_PLAY_HYPE_SPEECH)
			TEXT_LABEL_63 tlSpeech = "STRENGTH_HYPE"
			TRIGGER_STRENGTH_TEST_DIALOGUE(tlSpeech, TRUE)
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Determines the catch time; how fast the HUD meter should drop.
/// RETURNS: Catch time in milliseconds.
FUNC INT CALCULATE_STRENGTH_METER_HUD_CATCH_TIME_MS()
	RETURN ROUND(SCALE_BETWEEN_TWO_VALUES(TO_FLOAT(ciST_MAX_HUD_METER_CATCH_TIME_MS), TO_FLOAT(ciST_MIN_HUD_METER_CATCH_TIME_MS), TO_FLOAT(ciST_MAX_HUD_METER_VALUE), 0.0, Data.HUDData.fHUDMeterProgress))+ciST_MIN_HUD_METER_CATCH_TIME_MS
ENDFUNC

/// PURPOSE:
///    Calculates the Strength Test swing multiplier.
///    Visual representation of multipler on UI:
///    
///    	     /-----\			x2 Multiplier
///    _____/	    \_____		x1 Multiplier
///    
///    The bar increases from x1-x2 multiplier as the UI fades from faded red to red.
///    For the main red section the multiplier stays at x2.
///    The multiplier decreases from x2-x1 as the bar goes from red back to faded red.
///    The white sections of the UI do not give multipliers.
/// PARAMS:
///    fFillValue - The swing meter fill value. This is the amount the player has button bashed up to.
///    fBarValue - The catch bar value.
/// RETURNS: The swing multiplier as a FLOAT.
FUNC FLOAT GET_STRENGTH_TEST_SWING_MULTIPLIER(FLOAT fFillValue, FLOAT fBarValue)
	FLOAT fMaxMultiplierStart = SCALE_BETWEEN_TWO_VALUES(tfST_SWING_METER_MAX_MULTIPLIER_NORM_START, 0.0, 0.0, 1.0, fFillValue)
	FLOAT fMaxMultiplierEnd = SCALE_BETWEEN_TWO_VALUES(tfST_SWING_METER_MAX_MULTIPLIER_NORM_END, 0.0, 0.0, 1.0, fFillValue)
	
	FLOAT fTopMultiplierStart = SCALE_BETWEEN_TWO_VALUES(tfST_SWING_METER_TOP_MULTIPLIER_NORM_START, 0.0, 0.0, 1.0, fFillValue)
	FLOAT fTopMultiplierEnd = SCALE_BETWEEN_TWO_VALUES(tfST_SWING_METER_TOP_MULTIPLIER_NORM_END, 0.0, 0.0, 1.0, fFillValue)
	
	FLOAT fBottomMultiplierStart = SCALE_BETWEEN_TWO_VALUES(tfST_SWING_METER_BOTTOM_MULTIPLIER_NORM_START, 0.0, 0.0, 1.0, fFillValue)
	FLOAT fBottomMultiplierEnd = SCALE_BETWEEN_TWO_VALUES(tfST_SWING_METER_BOTTOM_MULTIPLIER_NORM_END, 0.0, 0.0, 1.0, fFillValue)
	
	IF (fBarValue >= fMaxMultiplierStart AND fBarValue <= fMaxMultiplierEnd)
		RETURN tfST_SWING_METER_MAX_MULTIPLIER
	ELIF (fBarValue >= fTopMultiplierStart AND fBarValue <= fTopMultiplierEnd)
		RETURN SCALE_BETWEEN_TWO_VALUES(1.0, 0.0, fTopMultiplierEnd, fTopMultiplierStart, fBarValue)+1.0
	ELIF (fBarValue >= fBottomMultiplierStart AND fBarValue <= fBottomMultiplierEnd)
		RETURN SCALE_BETWEEN_TWO_VALUES(0.0, 1.0, fBottomMultiplierEnd, fBottomMultiplierStart, fBarValue)+1.0
	ENDIF
	
	RETURN 1.0
ENDFUNC

/// PURPOSE:
///    Perforces the catch segment of the Strength Test flow.
///    -Players have to press 'O' within the safe zone represented by the second HUD meter.
/// RETURNS: TRUE once which segment has finished.
FUNC BOOL PERFORM_STRENGTH_TEST_SWING_CATCH()
	
	// Stop the power loop sound
	IF NOT IS_BIT_SET(Data.iBS, BS_ST_STOP_POWER_LOOP_SOUND)
		SET_BIT(Data.iBS, BS_ST_STOP_POWER_LOOP_SOUND)
		STOP_STRENGTH_TEST_SOUND(Data.AudioData.iPowerloopSoundID)
	ENDIF
	
	// Randomise timer to drop the swing bar
	IF HAS_NET_TIMER_EXPIRED(Data.GameplayData.stCountdownTimer, Data.GameplayData.iCatchTimeMS)
		
		// Play one off Bar Drop sound
		IF NOT IS_BIT_SET(Data.iBS, BS_ST_PLAY_POWER_BAR_DROP_SOUND)
			SET_BIT(Data.iBS, BS_ST_PLAY_POWER_BAR_DROP_SOUND)
			TRIGGER_STRENGTH_TEST_SOUND("Bar_Drop", Data.AudioData.iAltSoundID)
		ENDIF
		
		IF IS_SWING_METER_BUTTON_JUST_PRESSED()														// Player has stopped the meter
		OR PERFORM_STRENGTH_TEST_SWING_METER_DROP(CALCULATE_STRENGTH_METER_HUD_CATCH_TIME_MS())		// Drop the swing meter. Returns true if meter reaches the bottom
			CLEAR_BIT(Data.iBS, BS_ST_SET_HUD_METER_PROGRESS)
			CLEAR_BIT(Data.iBS, BS_ST_SET_HUD_METER_TIMER)
			CLEAR_BIT(Data.iBS, BS_ST_PLAY_POWER_BAR_DROP_SOUND)
			CLEAR_BIT(Data.iBS, BS_ST_STOP_POWER_LOOP_SOUND)
			RESET_NET_TIMER(Data.GameplayData.stCountdownTimer)
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("ARC_CAB_AOF_MLTPLR")
			OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("ARC_CAB_AOF_MLTPLR_ALT")
				CLEAR_HELP()
			ENDIF
			
			// Flash swing meter bar and calculate multiplier
			IF (Data.HUDData.fNormMeterBarValue != 0.0)
				FLASH_STRENGTH_TEST_SWING_METER_BAR()
				Data.GameplayData.fScore *= GET_STRENGTH_TEST_SWING_MULTIPLIER(Data.HUDData.fNormMeterFillValue, Data.HUDData.fNormMeterBarValue)
			ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Determines which swing animation to play based on the players score.
/// PARAMS:
///    iTotalScore - Players score.
/// RETURNS: Swing animation clip to play.
FUNC ARCADE_CABINET_ANIM_CLIPS GET_STRENGTH_TEST_SWING_ANIM(INT iTotalScore)
	ARCADE_CABINET_ANIM_CLIPS eClip = ARCADE_CABINET_ANIM_CLIP_INVALID
	IF (iTotalScore > ciST_PERFECT_SCORE_LIMIT)
		eClip = ARCADE_CABINET_ANIM_CLIP_STRENGTH_TEST_RESULT_PERFECT
	ELIF (iTotalScore > ciST_GOOD_SCORE_LIMIT AND iTotalScore <= ciST_PERFECT_SCORE_LIMIT)
		eClip = ARCADE_CABINET_ANIM_CLIP_STRENGTH_TEST_RESULT_GOOD
	ELIF (iTotalScore > ciST_AVERAGE_SCORE_LIMIT AND iTotalScore <= ciST_GOOD_SCORE_LIMIT)
		eClip = ARCADE_CABINET_ANIM_CLIP_STRENGTH_TEST_RESULT_AVERAGE
	ELIF (iTotalScore <= ciST_AVERAGE_SCORE_LIMIT)
		eClip = ARCADE_CABINET_ANIM_CLIP_STRENGTH_TEST_RESULT_BAD
	ENDIF
	RETURN eClip
ENDFUNC

/// PURPOSE:
///    Gets the Strength Test MoVE Network swing result transition name based on the players total score.
/// PARAMS:
///    iTotalScore - The total prop score the player has achieved.
/// RETURNS: The swing result MoVE Network transition name as a STRING.
FUNC STRING GET_STRENGTH_TEST_SWING_ANIM_MOVE_NETWORK_TRANSITION_NAME(INT iTotalScore)
	STRING sTransitionName = ""
	IF (iTotalScore > ciST_PERFECT_SCORE_LIMIT)
		sTransitionName = "PerfectResult"
	ELIF (iTotalScore > ciST_GOOD_SCORE_LIMIT AND iTotalScore <= ciST_PERFECT_SCORE_LIMIT)
		sTransitionName = "GoodResult"
	ELIF (iTotalScore > ciST_AVERAGE_SCORE_LIMIT AND iTotalScore <= ciST_GOOD_SCORE_LIMIT)
		sTransitionName = "AverageResult"
	ELIF (iTotalScore <= ciST_AVERAGE_SCORE_LIMIT)
		sTransitionName = "BadResult"
	ENDIF
	RETURN sTransitionName
ENDFUNC

/// PURPOSE:
///    Gets the Strength Test swing animation name based on the players total score.
/// PARAMS:
///    iTotalScore - The total prop score the player has achieved.
/// RETURNS: The swing result MoVE Network transition name as a STRING.
FUNC STRING GET_STRENGTH_TEST_SWING_ANIM_NAME(INT iTotalScore)
	STRING sAnimName = ""
	IF (iTotalScore > ciST_PERFECT_SCORE_LIMIT)
		sAnimName = "RESULT_PERFECT"
	ELIF (iTotalScore > ciST_GOOD_SCORE_LIMIT AND iTotalScore <= ciST_PERFECT_SCORE_LIMIT)
		sAnimName = "RESULT_GOOD"
	ELIF (iTotalScore > ciST_AVERAGE_SCORE_LIMIT AND iTotalScore <= ciST_GOOD_SCORE_LIMIT)
		sAnimName = "RESULT_AVERAGE"
	ELIF (iTotalScore <= ciST_AVERAGE_SCORE_LIMIT)
		sAnimName = "RESULT_BAD"
	ENDIF
	RETURN sAnimName
ENDFUNC

/// PURPOSE:
///    Gets the phase time the hammer prop impacts with the arcade machine prop.
/// PARAMS:
///    eSwingClip - Which swing animation is the player playing.
/// RETURNS: Phase the hammer prop impacts with the arcade machine prop.
FUNC FLOAT GET_STRENGTH_TEST_HAMMER_IMPACT_PHASE(ARCADE_CABINET_ANIM_CLIPS eSwingClip)
	FLOAT fPhase = 0.0
	SWITCH eSwingClip
		CASE ARCADE_CABINET_ANIM_CLIP_STRENGTH_TEST_RESULT_PERFECT		fPhase = 0.233	BREAK
		CASE ARCADE_CABINET_ANIM_CLIP_STRENGTH_TEST_RESULT_GOOD			fPhase = 0.225	BREAK
		CASE ARCADE_CABINET_ANIM_CLIP_STRENGTH_TEST_RESULT_AVERAGE		fPhase = 0.178	BREAK
		CASE ARCADE_CABINET_ANIM_CLIP_STRENGTH_TEST_RESULT_BAD			fPhase = 0.187	BREAK
	ENDSWITCH
	RETURN fPhase
ENDFUNC

/// PURPOSE:
///    Gets the phase time to detach the hammer prop depending on the swing animation.
/// PARAMS:
///    eSwingClip - Which swing animation is the player playing.
/// RETURNS: Phase to detach the hammer prop.
FUNC FLOAT GET_STRENGTH_TEST_DETACH_HAMMER_PHASE(ARCADE_CABINET_ANIM_CLIPS eSwingClip)
	FLOAT fPhase = 0.0
	SWITCH eSwingClip
		CASE ARCADE_CABINET_ANIM_CLIP_STRENGTH_TEST_RESULT_PERFECT		fPhase = 0.683	BREAK
		CASE ARCADE_CABINET_ANIM_CLIP_STRENGTH_TEST_RESULT_GOOD			fPhase = 0.701	BREAK
		CASE ARCADE_CABINET_ANIM_CLIP_STRENGTH_TEST_RESULT_AVERAGE		fPhase = 0.775	BREAK
		CASE ARCADE_CABINET_ANIM_CLIP_STRENGTH_TEST_RESULT_BAD			fPhase = 0.681	BREAK
	ENDSWITCH
	RETURN fPhase
ENDFUNC

/// PURPOSE:
///    Determines how long for the score to count up in the scaleform movie.
/// PARAMS:
///    eSwingClip - Which swing animation is the player playing.
/// RETURNS: The score count up duration in seconds.
FUNC FLOAT GET_STRENGTH_TEST_SCORE_COUNT_UP_SPEED_SECONDS(ARCADE_CABINET_ANIM_CLIPS eSwingClip)
	FLOAT fSeconds = 0.0
	SWITCH eSwingClip
		CASE ARCADE_CABINET_ANIM_CLIP_STRENGTH_TEST_RESULT_PERFECT		fSeconds = 0.500	BREAK
		CASE ARCADE_CABINET_ANIM_CLIP_STRENGTH_TEST_RESULT_GOOD			fSeconds = 0.700	BREAK
		CASE ARCADE_CABINET_ANIM_CLIP_STRENGTH_TEST_RESULT_AVERAGE		fSeconds = 0.900	BREAK
		CASE ARCADE_CABINET_ANIM_CLIP_STRENGTH_TEST_RESULT_BAD			fSeconds = 1.100	BREAK
	ENDSWITCH
	RETURN fSeconds
ENDFUNC

/// PURPOSE:
///    Determines how long to display the players score on the scoreboard.
/// RETURNS: The score hold duration in seconds.
FUNC FLOAT GET_STRENGTH_TEST_SCORE_HOLD_DURATION_SECONDS()
	IF (Data.GameplayData.iPropScore > Data.HighScoreData.iHighScore)
	OR (Data.GameplayData.iPropScore = ciST_MAX_PROP_SCORE)
		RETURN cfST_DISPLAY_HIGHSCORE_TIME_SECONDS
	ENDIF
	RETURN cfST_DISPLAY_SCORE_TIME_SECONDS
ENDFUNC

/// PURPOSE:
///    Determines which result sound clip to play based on player score.
/// PARAMS:
///    iScore - Players total score.
/// RETURNS: Result sound clip name.
FUNC STRING GET_STRENGTH_TEST_RESULT_SOUND(INT iScore)
	
	INT iSound		= 0
	STRING sSound 	= ""
	FLOAT fScore	= TO_FLOAT(iScore)
	FLOAT fSegment 	= TO_FLOAT(ciST_MAX_PROP_SCORE)/TO_FLOAT(ciST_MAX_RESULT_SOUNDS)
	
	REPEAT ciST_MAX_RESULT_SOUNDS iSound
		IF (fScore >= fSegment*TO_FLOAT(iSound)) AND (fScore < fSegment*TO_FLOAT(iSound+1))
			BREAKLOOP
		ENDIF
	ENDREPEAT
	
	IF (iScore = ciST_MAX_PROP_SCORE)
		iSound = 5
	ENDIF
	
	SWITCH iSound
		CASE 0	sSound = "Result_00"	BREAK
		CASE 1	sSound = "Result_01"	BREAK
		CASE 2	sSound = "Result_02"	BREAK
		CASE 3	sSound = "Result_03"	BREAK
		CASE 4	sSound = "Result_04"	BREAK
		CASE 5	sSound = "Result_05"	BREAK
	ENDSWITCH
	
	RETURN sSound
ENDFUNC

/// PURPOSE:
///    Determines which hit sound to play when the hammer impacts with the arcade machine.
/// PARAMS:
///    iScore - Players total score.
/// RETURNS: Hit sound clip name.
FUNC STRING GET_STRENGTH_TEST_HIT_SOUND(INT iScore)
	
	INT iSound		= 0
	STRING sSound 	= ""
	FLOAT fScore	= TO_FLOAT(iScore)
	FLOAT fSegment 	= TO_FLOAT(ciST_MAX_PROP_SCORE)/TO_FLOAT(ciST_MAX_HIT_SOUNDS)
	
	REPEAT ciST_MAX_HIT_SOUNDS iSound
		IF (fScore > fSegment*TO_FLOAT(iSound)) AND (fScore <= fSegment*TO_FLOAT(iSound+1))
			BREAKLOOP
		ENDIF
	ENDREPEAT
	
	IF (iScore = 0)
		iSound = 0
	ENDIF
	
	SWITCH iSound
		CASE 0	sSound = "Hit_00"	BREAK
		CASE 1	sSound = "Hit_01"	BREAK
		CASE 2	sSound = "Hit_02"	BREAK
	ENDSWITCH
	
	RETURN sSound
ENDFUNC

/// PURPOSE:
///    Plays the correct swing animation based on the players score.
/// PARAMS:
///    missionScriptArgs - Prop data.
/// RETURNS: TRUE once the animation has finished playing.
FUNC BOOL PERFORM_STRENGTH_TEST_SWING(ACM_CHILD_SCRIPT_LAUNCHER_STRUCT missionScriptArgs)
	
	// Added delay so the swing doesn't trigger immediately after the catch bar.
	// Need to give the player a slight delay so they can see where they stopped the catch bar.
	IF NOT IS_BIT_SET(Data.iBS, BS_ST_SWING_DELAY_FINISHED)
		IF HAS_NET_TIMER_EXPIRED(Data.HUDData.stSwingDelay, ciST_SWING_DELAY_MS)
			RESET_NET_TIMER(Data.HUDData.stSwingDelay)
			SET_BIT(Data.iBS, BS_ST_SWING_DELAY_FINISHED)
		ENDIF
		RETURN FALSE
	ENDIF
	
	// Trigger swing result
	IF NOT IS_BIT_SET(Data.iBS, BS_ST_TRIGGER_MOVE_NETWORK_SWING_RESULT)
		FLOAT fTotalScore = (ciST_MAX_PROP_SCORE - SCALE_BETWEEN_TWO_VALUES(ciST_MAX_PROP_SCORE, 0, (ciST_MAX_HUD_METER_VALUE*2), 0, Data.GameplayData.fScore))
		
		#IF IS_DEBUG_BUILD
		IF (DebugData.iDebugScore > -1)
			fTotalScore = TO_FLOAT(DebugData.iDebugScore)
		ENDIF
		#ENDIF
		
		CLEAR_BIT(Data.iBS, BS_ST_LERP_DURING_CATCH_STAGE)
		CLEAR_BIT(Data.iBS, BS_ST_SET_CAMERA_CATCH_STAGE_LERP_VALUES)
		
		Data.GameplayData.iPropScore = ROUND(fTotalScore)
		
		IF IS_TASK_MOVE_NETWORK_ACTIVE(PLAYER_PED_ID())
			IF IS_TASK_MOVE_NETWORK_READY_FOR_TRANSITION(PLAYER_PED_ID())
				REQUEST_TASK_MOVE_NETWORK_STATE_TRANSITION(PLAYER_PED_ID(), GET_STRENGTH_TEST_SWING_ANIM_MOVE_NETWORK_TRANSITION_NAME(Data.GameplayData.iPropScore))
				STOP_STRENGTH_TEST_SOUND(Data.AudioData.iPowerloopSoundID)
				
				data.CameraData.vStartingCoords 	= GET_CAM_COORD(Data.CameraData.ciMainCamera)
				data.CameraData.vStartingRotation 	= GET_CAM_ROT(Data.CameraData.ciMainCamera)
				data.CameraData.fStartingFOV 		= GET_CAM_FOV(Data.CameraData.ciMainCamera)
				VALIDATE_ROTATION_ANGLE(data.CameraData.vStartingRotation.z)
				GET_STRENGTH_TEST_CAMERA_DATA(ST_CAMERA_STARTING, data.CameraData.vEndingCoords, data.CameraData.vEndingRotation, data.CameraData.fEndingFOV)
				
				Data.AnimData.iStartTimeMS 			= GET_GAME_TIMER()
				Data.AnimData.iAnimDurationMS		= FLOOR(GET_ANIM_DURATION(GET_STRENGTH_TEST_ANIM_DICT(), GET_STRENGTH_TEST_SWING_ANIM_NAME(Data.GameplayData.iPropScore))*1000)
				Data.AnimData.eSwingClip			= GET_STRENGTH_TEST_SWING_ANIM(Data.GameplayData.iPropScore)
				
				PLAY_STRENGTH_TEST_FACIAL_ANIM(Data.AnimData.eSwingClip)
				SET_BIT(Data.iBS, BS_ST_TRIGGER_MOVE_NETWORK_SWING_RESULT)
				
				// Broadcast event to tell remote players to stop resizing capsule - takes a while to resize to normal
				BROADCAST_STRENGTH_TEST_EVENT_RESET_PED_CAPSULE()
			ENDIF
		ENDIF
	ENDIF
	
	// Anim finished
	IF HAS_CURRENT_ANIM_PAST_PHASE(cfST_ANIM_EXIT_PHASE)
		SET_LOCAL_PLAYER_FINISHED_STRENGTH_TEST_ARCADE_GAME(TRUE)
		CLEAR_BIT(Data.iBS, BS_ST_SET_CAMERA_HAMMER_IMPACT_VALUES)
		CLEAR_BIT(Data.iBS, BS_ST_HIGHSCORE_EVENT_SENT)
		CLEAR_BIT(Data.iBS, BS_ST_SWING_DELAY_FINISHED)
		CLEAR_BIT(Data.iBS, BS_ST_TRIGGER_MOVE_NETWORK_SWING_RESULT)
		CLEAR_BIT(Data.iBS2, BS2_ST_SET_CAMERA_EXIT_LERP)
		CLEANUP_STRENGTH_TEST_MOVE_NETWORK()
		RETURN TRUE
		
	// Detach the hammer from the player
	ELIF HAS_CURRENT_ANIM_PAST_PHASE(GET_STRENGTH_TEST_DETACH_HAMMER_PHASE(Data.AnimData.eSwingClip))
		DETACH_STRENGTH_TEST_HAMMER_FROM_LOCAL_PLAYER(missionScriptArgs)
		
		// Lerp back to gameplay camera
		IF NOT IS_BIT_SET(Data.iBS2, BS2_ST_SET_CAMERA_EXIT_LERP)
			SET_BIT(Data.iBS2, BS2_ST_SET_CAMERA_EXIT_LERP)
			
			IF (Data.CameraData.bFPCamera)
				MAINTAIN_STRENGTH_TEST_FIRST_PERSON_CAMERA_EXIT(TRUE)
			ELSE
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10.0)
				SET_STRENGTH_TEST_CAMERA_ACTIVE_STATE(Data.CameraData.ciMainCamera, FALSE)
				RENDER_SCRIPT_CAMS(FALSE, TRUE, tiST_CAMERA_ENDING_LERP_TIME_MS)
			ENDIF
		ENDIF
		
	// Play result sound
	ELIF HAS_CURRENT_ANIM_PAST_PHASE(GET_STRENGTH_TEST_HAMMER_IMPACT_PHASE(Data.AnimData.eSwingClip)+cfST_RESULT_SOUND_OFFSET_PHASE)
		IF NOT IS_BIT_SET(Data.iBS, BS_ST_PLAY_RESULT_SOUND)
			SET_BIT(Data.iBS, BS_ST_PLAY_RESULT_SOUND)
			TRIGGER_STRENGTH_TEST_SOUND(GET_STRENGTH_TEST_RESULT_SOUND(Data.GameplayData.iPropScore), Data.AudioData.iSoundID)
			FADE_OUT_STRENGTH_TEST_SWING_METER()
		ENDIF
		
	// Start the score scaleform movie when the hammer hits the machine
	ELIF HAS_CURRENT_ANIM_PAST_PHASE(GET_STRENGTH_TEST_HAMMER_IMPACT_PHASE(Data.AnimData.eSwingClip))
		IF NOT IS_BIT_SET(Data.iBS, BS_ST_PERFORM_SCORE_SCALEFORM_MOVIE)
			SET_BIT(Data.iBS, BS_ST_PERFORM_SCORE_SCALEFORM_MOVIE)
			BROADCAST_STRENGTH_TEST_EVENT_PERFORM_SCORE(Data.GameplayData.iPropScore, GET_STRENGTH_TEST_SCORE_COUNT_UP_SPEED_SECONDS(Data.AnimData.eSwingClip), GET_STRENGTH_TEST_SCORE_HOLD_DURATION_SECONDS(), GET_STRENGTH_TEST_SCORE_COUNT_UP_SPEED_SECONDS(Data.AnimData.eSwingClip))
			START_NET_TIMER(Data.HighScoreData.stHighScoreTimer)
			TRIGGER_STRENGTH_TEST_SOUND(GET_STRENGTH_TEST_HIT_SOUND(Data.GameplayData.iPropScore), Data.AudioData.iSoundID)
			UPDATE_STRENGTH_TEST_CAMERA_SHAKE(Data.CameraData.ciMainCamera, Data.AnimData.fSwingWeight, TRUE, TRUE)
			
			// Shake Pad
			FLOAT fPadShake = SCALE_BETWEEN_TWO_VALUES(cfST_IMPACT_PAD_SHAKE_MAX, 0.0, TO_FLOAT(ciST_MAX_PROP_SCORE), 0.0, TO_FLOAT(Data.GameplayData.iPropScore))
			FLOAT fPadShakeDuration = SCALE_BETWEEN_TWO_VALUES(cfST_IMPACT_PAD_SHAKE_MAX_DURATION_MS, cfST_IMPACT_PAD_SHAKE_MIN_DURATION_MS, TO_FLOAT(ciST_MAX_PROP_SCORE), 0.0, TO_FLOAT(Data.GameplayData.iPropScore))
			SET_CONTROL_SHAKE(PLAYER_CONTROL, ROUND(cfST_IMPACT_PAD_SHAKE_MAX_DURATION_MS)-ROUND(fPadShakeDuration), ROUND(cfST_IMPACT_PAD_SHAKE_MAX)-ROUND(fPadShake))
		ENDIF
		
	// Lerp the camera to follow the swing down until it impacts with the arcade machine
	ELIF HAS_CURRENT_ANIM_PAST_PHASE(GET_STRENGTH_TEST_HAMMER_IMPACT_PHASE(Data.AnimData.eSwingClip)-cfST_CAMERA_SWING_LERP_BUFFER)
		IF (Data.HUDData.fNormMeterFillValue > tfST_SWING_METER_MIN_CATCH_VALUE)
			FLOAT fStartingPhase	= GET_STRENGTH_TEST_HAMMER_IMPACT_PHASE(Data.AnimData.eSwingClip)-cfST_CAMERA_SWING_LERP_BUFFER		// Starting camera lerp phase
			FLOAT fEndingPhase 		= GET_STRENGTH_TEST_HAMMER_IMPACT_PHASE(Data.AnimData.eSwingClip)-0.001								// Ending camera lerp phase which is just before hammer impact
			FLOAT fAlpha 			= NORMALISE_VALUE(GET_CURRENT_ANIM_PHASE(), fStartingPhase, fEndingPhase)
			UPDATE_STRENGTH_TEST_STAGE_CAMERA(Data.CameraData.ciMainCamera, fAlpha)
		ENDIF
	ENDIF
	
	// Timer is started when hammer impacts with the machine. Delay is needed to give the machine time to ascend up to the score.
	IF HAS_NET_TIMER_STARTED_AND_EXPIRED(Data.HighScoreData.stHighScoreTimer, ciST_DISPLAY_HIGHSCORE_DELAY_TIME_MS)
		RESET_NET_TIMER(Data.HighScoreData.stHighScoreTimer)
		
		// Send event with new highscore
		IF (Data.GameplayData.iPropScore > Data.HighScoreData.iHighScore)
		OR (Data.GameplayData.iPropScore = ciST_MAX_PROP_SCORE)
			IF NOT IS_BIT_SET(Data.iBS, BS_ST_HIGHSCORE_EVENT_SENT)
				// If host has changed, the highscore event will not be received by the host as they are loading into the arcade.
				// Wait a small delay and then send the event to ensure the new host receives the highscore.
				IF (Data.HighscoreData.arcadeHost != NETWORK_GET_HOST_OF_SCRIPT("AM_MP_ARCADE", GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iInstance))
					Data.HighscoreData.bHostChangedDuringGame = TRUE
				ENDIF
				BROADCAST_STRENGTH_TEST_EVENT_SET_HIGHSCORE(Data.GameplayData.iPropScore)
				SET_BIT(Data.iBS, BS_ST_HIGHSCORE_EVENT_SENT)
			ENDIF
		ENDIF
		
		// Ring bell
		TEXT_LABEL_63 tlSpeech = GET_STRENGTH_TEST_SCORE_SPEECH(Data.GameplayData.iPropScore)
		IF (Data.GameplayData.iPropScore = ciST_MAX_PROP_SCORE)
			tlSpeech = "STRENGTH_RING_BELL"
		ENDIF
		
		TRIGGER_STRENGTH_TEST_DIALOGUE(tlSpeech, TRUE)
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Maintains the idle state of the Strength Test arcade game.
///    This runs the flow and main logic of the arcade game.
/// PARAMS:
///    missionScriptArgs - Child script launch data.
PROC MAINTAIN_STRENGTH_TEST_IDLE_STATE(ACM_CHILD_SCRIPT_LAUNCHER_STRUCT missionScriptArgs)
	SWITCH Data.eGameState
		CASE ST_GAME_STATE_INITIALISE
			INITIALISE_STRENGTH_TEST()
			SET_STRENGTH_TEST_GAME_STATE(ST_GAME_STATE_REQUEST_ASSETS)
		BREAK
		CASE ST_GAME_STATE_REQUEST_ASSETS
			IF PERFORM_STRENGTH_TEST_REQUEST_ASSETS(missionScriptArgs)
				SET_STRENGTH_TEST_GAME_STATE(ST_GAME_STATE_ENTRY)
			ENDIF
		BREAK
		CASE ST_GAME_STATE_ENTRY
			IF PERFORM_STRENGTH_TEST_ENTRY(missionScriptArgs)
				SET_STRENGTH_TEST_GAME_STATE(ST_GAME_STATE_SWING_BASH)
			ENDIF
		BREAK
		CASE ST_GAME_STATE_SWING_BASH
			IF PERFORM_STRENGTH_TEST_SWING_BASH()
				IF (Data.HUDData.fNormMeterFillValue <= tfST_SWING_METER_MIN_CATCH_VALUE)
					SET_STRENGTH_TEST_GAME_STATE(ST_GAME_STATE_PERFORM_SWING)
				ELSE
					SET_STRENGTH_TEST_GAME_STATE(ST_GAME_STATE_SWING_CATCH)
				ENDIF
			ENDIF
		BREAK
		CASE ST_GAME_STATE_SWING_CATCH
			IF PERFORM_STRENGTH_TEST_SWING_CATCH()
				SET_STRENGTH_TEST_GAME_STATE(ST_GAME_STATE_PERFORM_SWING)
			ENDIF
		BREAK
		CASE ST_GAME_STATE_PERFORM_SWING
			IF PERFORM_STRENGTH_TEST_SWING(missionScriptArgs)
				SET_ARCADE_CABINET_CHILD_SCRIPT_CLIENT_STATE(Data.eState, ARCADE_CABINET_STATE_CLEANUP)
			ENDIF
		BREAK
	ENDSWITCH
	
	MAINTAIN_STRENGTH_TEST_HUD()
	MAINTAIN_STRENGTH_TEST_CONTROLS()
	MAINTAIN_STRENGTH_TEST_FIRST_PERSON_CAMERA_ENTRY()
	MAINTAIN_STRENGTH_TEST_CATCH_STAGE_CAMERA()	// The camera updates across the end of stage ST_GAME_STATE_SWING_BASH & stage ST_GAME_STATE_SWING_CATCH therefore needs to be maintained outside of the flow.
ENDPROC

/// PURPOSE:
///    Maintains the cleanup state of the Strength Test arcade game.
PROC MAINTAIN_STRENGTH_TEST_CLEANUP_STATE(ACM_CHILD_SCRIPT_LAUNCHER_STRUCT missionScriptArgs)
	PROCESS_STRENGTH_TEST_TROPHIES()
	SEND_STRENGTH_TEST_METRIC()
	
	Data.iTempScore = Data.GameplayData.iPropScore
	TRIGGER_STRENGTH_TEST_SOUND("End_Game", Data.AudioData.iSoundID)
	
	CLEANUP_STRENGTH_TEST_GAME()
	IF (NOT Data.CameraData.bFPCamera)
		CLEANUP_STRENGTH_TEST_CAMERA()
	ENDIF
	
	SET_BIT(Data.iBS, BS_ST_PLAY_LEAVE_SPEECH)
	START_NET_TIMER(Data.SpeechData.stLeaveTimer)
	
	VALIDATE_STRENGTH_TEST_HAMMER_PROP_POSITION(missionScriptArgs)
	SET_ARCADE_CABINET_CHILD_SCRIPT_CLIENT_STATE(Data.eState, ARCADE_CABINET_STATE_LOADING)
ENDPROC

/// PURPOSE:
///    Increases the arcade players capsule size while they're playing.
/// PARAMS:
///    missionScriptArgs - Child script launch data.
PROC MAINTAIN_STRENGTH_TEST_PED_CAPSULE(ACM_CHILD_SCRIPT_LAUNCHER_STRUCT missionScriptArgs)
	IF IS_BIT_SET(Data.iBS2, BS2_ST_SET_ARCADE_PLAYER_CAPSULE_SIZE)
		
		// Expand the arcade game players capsule size to stop remote players blocking the camera.
		IF (Data.MoveNetworkData.arcadePlayerID != INVALID_PLAYER_INDEX())
		AND IS_ENTITY_ALIVE(GET_PLAYER_PED(Data.MoveNetworkData.arcadePlayerID))
		AND NETWORK_IS_PLAYER_A_PARTICIPANT(Data.MoveNetworkData.arcadePlayerID)
			SET_PED_CAPSULE(GET_PLAYER_PED(Data.MoveNetworkData.arcadePlayerID), cfST_PED_CAPSULE_SIZE)
		ELSE
			IF (Data.MoveNetworkData.arcadePlayerID != INVALID_PLAYER_INDEX())
				
				// Request control of hammer to place is back locally.
				IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(missionScriptArgs.cabinetSubObj[ARCADE_CAB_OBJECT_1])
					NETWORK_REQUEST_CONTROL_OF_ENTITY(missionScriptArgs.cabinetSubObj[ARCADE_CAB_OBJECT_1])
				ELSE
					DETACH_STRENGTH_TEST_HAMMER_FROM_LOCAL_PLAYER(missionScriptArgs)
					VALIDATE_STRENGTH_TEST_HAMMER_PROP_POSITION(missionScriptArgs)
					PERFORM_RT_STATE_RESET_TIMER()
					SET_STRENGTH_TEST_RENDER_TARGET_STATE(ST_RT_STATE_DRAW)
					
					Data.MoveNetworkData.arcadePlayerID = INVALID_PLAYER_INDEX()
					CLEAR_BIT(Data.iBS2, BS2_ST_SET_ARCADE_PLAYER_CAPSULE_SIZE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Moves any remote players away from the arcade game player if they get too close while playing.
///    Only does this once, after which the arcade game players ped capsule will have increased to keep players out.
PROC MAINTAIN_STRENGTH_TEST_CLEAR_PED_AREA()
	IF IS_BIT_SET(Data.iBS2, BS2_ST_TASK_MOVE_PLAYER_AWAY_FROM_MACHINE)
		
		// Don't move local player away if any of these conditions are met.
		IF (Data.MoveNetworkData.arcadePlayerID = PLAYER_ID())
		OR (Data.MoveNetworkData.arcadePlayerID = INVALID_PLAYER_INDEX())
		OR NOT IS_ENTITY_ALIVE(GET_PLAYER_PED(Data.MoveNetworkData.arcadePlayerID))
		OR NOT NETWORK_IS_PLAYER_A_PARTICIPANT(Data.MoveNetworkData.arcadePlayerID)
		OR NOT IS_PLAYER_USING_ARCADE_CABINET(Data.MoveNetworkData.arcadePlayerID)
			PRINTLN("[AM_MP_ARCADE_STRENGTH_TEST] MAINTAIN_STRENGTH_TEST_CLEAR_PED_AREA - Clearing bit.")
			CLEAR_BIT(Data.iBS2, BS2_ST_TASK_MOVE_PLAYER_AWAY_FROM_MACHINE)
			EXIT
		ENDIF
		
		// The local player is too close to the arcade game player.
		// Move the local player away and set bit.
		IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
		AND IS_ENTITY_ALIVE(GET_PLAYER_PED(Data.MoveNetworkData.arcadePlayerID))
			IF NOT IS_PLAYER_USING_ARCADE_CABINET(PLAYER_ID()) 
			AND (GET_DISTANCE_BETWEEN_ENTITIES(GET_PLAYER_PED(Data.MoveNetworkData.arcadePlayerID), PLAYER_PED_ID()) < cfST_PED_CAPSULE_SIZE)
				
				FLOAT fTargetY
				VECTOR vTargetCoords
				fTargetY = -(cfST_PED_CAPSULE_SIZE+1.0)
				vTargetCoords = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(GET_PLAYER_PED(Data.MoveNetworkData.arcadePlayerID), <<0.0, fTargetY, 0.0>>)
				TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), vTargetCoords, PEDMOVEBLENDRATIO_WALK, 7000)
				CLEAR_BIT(Data.iBS2, BS2_ST_TASK_MOVE_PLAYER_AWAY_FROM_MACHINE)
				PRINTLN("[AM_MP_ARCADE_STRENGTH_TEST] MAINTAIN_STRENGTH_TEST_CLEAR_PED_AREA - Moving out of way of arcade player.")
				
			ENDIF
		ENDIF
		
	ENDIF
ENDPROC

/// PURPOSE:
///    If the host of AM_MP_ARCADE.sc has changed while playing Axe of Fury, and a new highscore has been achieved,
///    we must assume that the highscore event was not received by the host, as they are loading into the Arcade etc.
///    Wait a small delay and send the highscore event again to ensure the new host receives the latest highscore.
PROC VALIDATE_HIGHSCORE()
	IF (Data.HighscoreData.bHostChangedDuringGame)
		IF HAS_NET_TIMER_EXPIRED(Data.HighscoreData.stValidateHighscoreTimer, ciST_VALIDATE_HIGHSCORE_TIME_MS)
			BROADCAST_STRENGTH_TEST_EVENT_VALIDATE_HIGHSCORE(Data.HighScoreData.iHighScore)
			RESET_NET_TIMER(Data.HighscoreData.stValidateHighscoreTimer)
			Data.HighscoreData.bHostChangedDuringGame = FALSE
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Logic that every client runs.
/// PARAMS:
///    missionScriptArgs - Child script launch data.
PROC RUN_MAIN_CLIENT_LOGIC(ACM_CHILD_SCRIPT_LAUNCHER_STRUCT missionScriptArgs)
	SWITCH Data.eState
		CASE ARCADE_CABINET_STATE_LOADING
			MAINTAIN_STRENGTH_TEST_LOADING_STATE()
		BREAK
		CASE ARCADE_CABINET_STATE_IDLE
			MAINTAIN_STRENGTH_TEST_IDLE_STATE(missionScriptArgs)
		BREAK
		CASE ARCADE_CABINET_STATE_CLEANUP
			MAINTAIN_STRENGTH_TEST_CLEANUP_STATE(missionScriptArgs)
		BREAK
	ENDSWITCH
	
	PROCESS_EVENTS()
	VALIDATE_HIGHSCORE()
	MAINTAIN_STRENGTH_TEST_PED_CAPSULE(missionScriptArgs)
	MAINTAIN_STRENGTH_TEST_CLEAR_PED_AREA()
	MAINTAIN_STRENGTH_TEST_MOVE_NETWORK()
	MAINTAIN_STRENGTH_TEST_RENDER_TARGET(missionScriptArgs)
	MAINTAIN_STRENGTH_TEST_FIRST_PERSON_CAMERA_EXIT()
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════════╡ SERVER LOGIC ╞═════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

PROC RUN_MAIN_SERVER_LOGIC()

ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════════╡ DEBUG ╞═════════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

#IF IS_DEBUG_BUILD
PROC INITIALISE_DEBUG_WIDGET_DATA(STRENGTH_TEST_CAMERAS eCamType, VECTOR vCoords, VECTOR vRotation, FLOAT fFOV)
	SWITCH eCamType
		CASE ST_CAMERA_STARTING		
			debugData.CameraDebug.StartingCamData.vCoords 		= vCoords
			debugData.CameraDebug.StartingCamData.vRotation 	= vRotation
			debugData.CameraDebug.StartingCamData.fFOV 			= fFOV
		BREAK
		CASE ST_CAMERA_STAGE_ONE	
			debugData.CameraDebug.StageOneCamData.vCoords		= vCoords
			debugData.CameraDebug.StageOneCamData.vRotation		= vRotation
			debugData.CameraDebug.StageOneCamData.fFOV			= fFOV
		BREAK
		CASE ST_CAMERA_STAGE_TWO	
			debugData.CameraDebug.StageTwoCamData.vCoords 		= vCoords
			debugData.CameraDebug.StageTwoCamData.vRotation		= vRotation
			debugData.CameraDebug.StageTwoCamData.fFOV			= fFOV
		BREAK
		CASE ST_CAMERA_STAGE_THREE	
			debugData.CameraDebug.StageThreeCamData.vCoords		= vCoords
			debugData.CameraDebug.StageThreeCamData.vRotation	= vRotation
			debugData.CameraDebug.StageThreeCamData.fFOV		= fFOV
		BREAK
		CASE ST_CAMERA_STAGE_FOUR	
			debugData.CameraDebug.StageFourCamData.vCoords		= vCoords
			debugData.CameraDebug.StageFourCamData.vRotation	= vRotation
			debugData.CameraDebug.StageFourCamData.fFOV			= fFOV
		BREAK
		CASE ST_CAMERA_STAGE_FIVE
			debugData.CameraDebug.StageFiveCamData.vCoords		= vCoords
			debugData.CameraDebug.StageFiveCamData.vRotation	= vRotation
			debugData.CameraDebug.StageFiveCamData.fFOV			= fFOV
		BREAK
	ENDSWITCH
ENDPROC

PROC INITIALISE_DEBUG_WIDGETS()
	INT iCam
	FLOAT fFOV
	VECTOR vCoords
	VECTOR vRotation
	REPEAT ST_CAMERA_MAX iCam
		GET_STRENGTH_TEST_CAMERA_DATA(INT_TO_ENUM(STRENGTH_TEST_CAMERAS, iCam), vCoords, vRotation, fFOV, FALSE, FALSE)
		INITIALISE_DEBUG_WIDGET_DATA(INT_TO_ENUM(STRENGTH_TEST_CAMERAS, iCam), vCoords, vRotation, fFOV)
	ENDREPEAT
ENDPROC

FUNC STRING DEBUG_GET_CAMERA_WIDGET_NAME(STRENGTH_TEST_CAMERAS eCamType)
	STRING sWidgetName = ""
	SWITCH eCamType
		CASE ST_CAMERA_STARTING		sWidgetName = "Starting Cam"		BREAK
		CASE ST_CAMERA_STAGE_ONE	sWidgetName = "Stage One Cam"		BREAK
		CASE ST_CAMERA_STAGE_TWO	sWidgetName = "Stage Two Cam"		BREAK
		CASE ST_CAMERA_STAGE_THREE	sWidgetName = "Stage Three Cam"		BREAK
		CASE ST_CAMERA_STAGE_FOUR	sWidgetName = "Stage Four Cam"		BREAK
		CASE ST_CAMERA_STAGE_FIVE	sWidgetName = "Stage Five Cam"		BREAK
	ENDSWITCH
	RETURN sWidgetName
ENDFUNC

PROC DEBUG_ADD_CAMERA_WIDGETS(STRENGTH_TEST_CAMERAS eCamType, DEBUG_CAMERA_DATA &cameraData)
	ADD_WIDGET_STRING("")
	START_WIDGET_GROUP(DEBUG_GET_CAMERA_WIDGET_NAME(eCamType))
		START_WIDGET_GROUP("Coords")
			ADD_WIDGET_VECTOR_SLIDER("Coords", cameraData.vCoords, -20.0, 20.0, 0.1)
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Rotation")
			ADD_WIDGET_VECTOR_SLIDER("Rotation", cameraData.vRotation, -360.0, 360.0, 0.1)
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("FOV")
			ADD_WIDGET_FLOAT_SLIDER("FOV", cameraData.fFOV, 0.0, 130.0, 0.01)
		STOP_WIDGET_GROUP()
		IF (eCamType = ST_CAMERA_STARTING)
			ADD_WIDGET_INT_SLIDER("Starting Lerp Time MS", tiST_CAMERA_STARTING_LERP_TIME_MS, 0, 10000, 1)
			ADD_WIDGET_INT_SLIDER("Ending Lerp Time MS", tiST_CAMERA_ENDING_LERP_TIME_MS, 0, 10000, 1)
		ENDIF
		ADD_WIDGET_BOOL("Render", cameraData.bRenderCam)
		ADD_WIDGET_BOOL("Update Debug Values in Flow", debugData.CameraDebug.bUpdateDebugValuesInFlow)
	STOP_WIDGET_GROUP()
ENDPROC

PROC DEBUG_ADD_CAMERA_SHAKE_TYPE_WIDGET(STRING sComboBoxHeader, INT &iShakeType)
	START_NEW_WIDGET_COMBO()
		ADD_TO_WIDGET_COMBO("SMALL_EXPLOSION_SHAKE")
		ADD_TO_WIDGET_COMBO("MEDIUM_EXPLOSION_SHAKE")
		ADD_TO_WIDGET_COMBO("LARGE_EXPLOSION_SHAKE")
		ADD_TO_WIDGET_COMBO("HAND_SHAKE")
		ADD_TO_WIDGET_COMBO("JOLT_SHAKE")
		ADD_TO_WIDGET_COMBO("VIBRATE_SHAKE")
		ADD_TO_WIDGET_COMBO("WOBBLY_SHAKE")
		ADD_TO_WIDGET_COMBO("DRUNK_SHAKE")
	STOP_WIDGET_COMBO(sComboBoxHeader, iShakeType)
ENDPROC

PROC CREATE_DEBUG_WIDGETS()
	START_WIDGET_GROUP("AM_MP_ARCADE_STRENGTH_TEST")
		ADD_WIDGET_BOOL("Enable Widgets", DebugData.bEnableWidgets)
		ADD_WIDGET_STRING("")
		
		START_WIDGET_GROUP("Audio")
			ADD_WIDGET_BOOL("Mute Button_Mash_Hit Sound", DebugData.bMutePlayerInputSound)
			ADD_WIDGET_BOOL("Mute Stage_0x Sound", DebugData.bMuteBashingStageSound)
		STOP_WIDGET_GROUP()
		
		ADD_WIDGET_STRING("")
		START_WIDGET_GROUP("Cameras")
			DEBUG_ADD_CAMERA_WIDGETS(ST_CAMERA_STARTING, debugData.CameraDebug.StartingCamData)
			DEBUG_ADD_CAMERA_WIDGETS(ST_CAMERA_STAGE_ONE, debugData.CameraDebug.StageOneCamData)
			DEBUG_ADD_CAMERA_WIDGETS(ST_CAMERA_STAGE_TWO, debugData.CameraDebug.StageTwoCamData)
			DEBUG_ADD_CAMERA_WIDGETS(ST_CAMERA_STAGE_THREE, debugData.CameraDebug.StageThreeCamData)
			DEBUG_ADD_CAMERA_WIDGETS(ST_CAMERA_STAGE_FOUR, debugData.CameraDebug.StageFourCamData)
			DEBUG_ADD_CAMERA_WIDGETS(ST_CAMERA_STAGE_FIVE, debugData.CameraDebug.StageFiveCamData)
			
			ADD_WIDGET_STRING("")
			START_WIDGET_GROUP("Catch Stage")
				ADD_WIDGET_FLOAT_SLIDER("Lerp Amount Stage 1", tfST_CAMERA_ANIM_WEIGHT_OFFSET_STAGE_1, 0.0, 3.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Lerp Amount Stage 2", tfST_CAMERA_ANIM_WEIGHT_OFFSET_STAGE_2, 0.0, 3.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Lerp Amount Stage 3", tfST_CAMERA_ANIM_WEIGHT_OFFSET_STAGE_3, 0.0, 3.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Lerp Amount Stage 4", tfST_CAMERA_ANIM_WEIGHT_OFFSET_STAGE_4, 0.0, 3.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Lerp Amount Stage 5", tfST_CAMERA_ANIM_WEIGHT_OFFSET_STAGE_5, 0.0, 3.0, 0.01)
				ADD_WIDGET_STRING("")
				ADD_WIDGET_INT_SLIDER("Stage 1 Lerp Time MS", tiST_ANIM_FINISH_BLEND_TIME_MS_STAGE_1, 0, 5000, 1)
				ADD_WIDGET_INT_SLIDER("Stage 2 Lerp Time MS", tiST_ANIM_FINISH_BLEND_TIME_MS_STAGE_2, 0, 5000, 1)
				ADD_WIDGET_INT_SLIDER("Stage 3 Lerp Time MS", tiST_ANIM_FINISH_BLEND_TIME_MS_STAGE_3, 0, 5000, 1)
				ADD_WIDGET_INT_SLIDER("Stage 4 Lerp Time MS", tiST_ANIM_FINISH_BLEND_TIME_MS_STAGE_4, 0, 5000, 1)
				ADD_WIDGET_INT_SLIDER("Stage 5 Lerp Time MS", tiST_ANIM_FINISH_BLEND_TIME_MS_STAGE_5, 0, 5000, 1)
			STOP_WIDGET_GROUP()
			
			ADD_WIDGET_STRING("")
			START_WIDGET_GROUP("Interpolation")
				START_NEW_WIDGET_COMBO()
					ADD_TO_WIDGET_COMBO("INTERPTYPE_LINEAR")
					ADD_TO_WIDGET_COMBO("INTERPTYPE_ACCEL")
					ADD_TO_WIDGET_COMBO("INTERPTYPE_DECEL")
					ADD_TO_WIDGET_COMBO("INTERPTYPE_COSINE")
					ADD_TO_WIDGET_COMBO("INTERPTYPE_SMOOTHSTEP")
					ADD_TO_WIDGET_COMBO("INTERPTYPE_SPHERICAL_LINEAR")
					ADD_TO_WIDGET_COMBO("INTERPTYPE_SPHERICAL_ACCEL")
					ADD_TO_WIDGET_COMBO("INTERPTYPE_SPHERICAL_DECEL")
					ADD_TO_WIDGET_COMBO("INTERPTYPE_SPHERICAL_COSINE")
					ADD_TO_WIDGET_COMBO("INTERPTYPE_SPHERICAL_SMOOTHSTEP")
				STOP_WIDGET_COMBO("Type", Data.CameraData.iInterpolationType)
			STOP_WIDGET_GROUP()
			
			ADD_WIDGET_STRING("")
			START_WIDGET_GROUP("Shake")
				ADD_WIDGET_STRING("Power Bar Increasing: Shake occurs on button press.")
				ADD_WIDGET_STRING("Power Bar Decreasing: Shake occurs between max and min interval times.")
				ADD_WIDGET_INT_SLIDER("Min Shake Interval Time MS", tiST_CAMERA_MIN_SHAKE_INTERVAL_TIME_MS, 0, 1000, 1)
				ADD_WIDGET_INT_SLIDER("Max Shake Interval Time MS", tiST_CAMERA_MAX_SHAKE_INTERVAL_TIME_MS, 0, 1000, 1)
				DEBUG_ADD_CAMERA_SHAKE_TYPE_WIDGET("Shake Type", debugData.CameraDebug.iShakeType)
				ADD_WIDGET_FLOAT_SLIDER("Max Shake", tfST_CAMERA_MAX_SHAKE, 0.0, 10.0, 0.01)
				ADD_WIDGET_BOOL("Test", debugData.CameraDebug.bTestShake)
				
				ADD_WIDGET_STRING("")
				ADD_WIDGET_STRING("Impact Shake: Occurs on swing when hammer collides with arcade machine.")
				DEBUG_ADD_CAMERA_SHAKE_TYPE_WIDGET("Impact Shake Type", debugData.CameraDebug.iImpactShakeType)
				ADD_WIDGET_FLOAT_SLIDER("Max Impact Shake", tfST_CAMERA_MAX_IMPACT_SHAKE, 0.0, 10.0, 0.01)
				ADD_WIDGET_BOOL("Test", debugData.CameraDebug.bTestImpactShake)
				
				ADD_WIDGET_STRING("")
				ADD_WIDGET_BOOL("Disable Shake in Flow", debugData.CameraDebug.bDisableShake)
			STOP_WIDGET_GROUP()
			
			ADD_WIDGET_STRING("")
			START_WIDGET_GROUP("Animation Stages")
				ADD_WIDGET_STRING("Starting Cam = Animation 0 Percent")
				ADD_WIDGET_STRING("Stage One = Animation 25 Percent")
				ADD_WIDGET_STRING("Stage Two = Animation 50 Percent")
				ADD_WIDGET_STRING("Stage Three = Animation 75 Percent")
				ADD_WIDGET_STRING("Stage Four = Animation 100 Percent")
				ADD_WIDGET_INT_SLIDER("Animation Percentage", DebugData.iAnimationPercentage, -1, 100, 1)
				ADD_WIDGET_BOOL("Hold On Bash Stage", DebugData.bHoldOnBashStage)
			STOP_WIDGET_GROUP()
			
			ADD_WIDGET_STRING("")
			ADD_WIDGET_BOOL("Output Data", debugData.CameraDebug.bOutputData)
		STOP_WIDGET_GROUP()
		
		ADD_WIDGET_STRING("")
		START_WIDGET_GROUP("Swing")
			ADD_WIDGET_FLOAT_SLIDER("Bar Increase Increments", tfST_HUB_METER_INCREASE_INCREMENTS, 0.0, 5.0, 0.001)
			ADD_WIDGET_STRING("")
			ADD_WIDGET_FLOAT_SLIDER("Swing Anim Weight Increase", tfST_SWING_ANIM_WEIGHT_INCREASE, 0.0, 5.0, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("Swing Anim Weight Decrease", tfST_SWING_ANIM_WEIGHT_DECREASE, 0.0, 5.0, 0.001)
			ADD_WIDGET_STRING("")
			ADD_WIDGET_BOOL("Hold On Bash Stage", DebugData.bHoldOnBashStage)
			ADD_WIDGET_STRING("")
			ADD_WIDGET_INT_SLIDER("Set Score", DebugData.iDebugScore, -1, 1100, 1)
			ADD_WIDGET_STRING("")
			ADD_WIDGET_FLOAT_SLIDER("Fill Value", DebugData.UIDebug.fFillValue, 0.0, 1.0, 0.001)
			ADD_WIDGET_BOOL("Show Bar", DebugData.UIDebug.fShowBarMeter)
			ADD_WIDGET_FLOAT_SLIDER("Bar Value", DebugData.UIDebug.fBarValue, 0.0, 1.0, 0.001)
			ADD_WIDGET_FLOAT_READ_ONLY("Multiplier", DebugData.UIDebug.fSwingMultiplier)
		STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP()
ENDPROC

PROC DEBUG_STOP_RENDERING_ALL_SCRIPTED_CAMERAS()
	debugData.CameraDebug.StartingCamData.bRenderCam	= FALSE
	debugData.CameraDebug.StageOneCamData.bRenderCam 	= FALSE
	debugData.CameraDebug.StageTwoCamData.bRenderCam 	= FALSE
	debugData.CameraDebug.StageThreeCamData.bRenderCam 	= FALSE
	debugData.CameraDebug.StageFourCamData.bRenderCam 	= FALSE
	debugData.CameraDebug.StageFiveCamData.bRenderCam 	= FALSE
ENDPROC

FUNC BOOL DEBUG_ARE_ANY_SCRIPTED_CAMERAS_RENDERING()
	IF debugData.CameraDebug.StartingCamData.bRenderCam
	OR debugData.CameraDebug.StageOneCamData.bRenderCam
	OR debugData.CameraDebug.StageTwoCamData.bRenderCam
	OR debugData.CameraDebug.StageThreeCamData.bRenderCam
	OR debugData.CameraDebug.StageFourCamData.bRenderCam
	OR debugData.CameraDebug.StageFiveCamData.bRenderCam
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC DEBUG_UPDATE_CAMERA_WIDGETS(DEBUG_CAMERA_DATA &debugCameraData)
	
	IF (debugCameraData.bRenderCam)
		DEBUG_STOP_RENDERING_ALL_SCRIPTED_CAMERAS()
		debugCameraData.bRenderCam = TRUE
		
		IF NOT DOES_CAM_EXIST(Data.CameraData.ciMainCamera)
			Data.CameraData.ciMainCamera = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, debugCameraData.vCoords, debugCameraData.vRotation, debugCameraData.fFOV)
		ELSE
			IF NOT IS_CAM_ACTIVE(Data.CameraData.ciMainCamera)
				SET_CAM_ACTIVE(Data.CameraData.ciMainCamera, TRUE)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
			ELSE
				SET_CAM_COORD(Data.CameraData.ciMainCamera, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(Data.cabinetObj, debugCameraData.vCoords))
				SET_CAM_ROT(Data.CameraData.ciMainCamera, debugCameraData.vRotation)
				SET_CAM_FOV(Data.CameraData.ciMainCamera, debugCameraData.fFOV)
			ENDIF
		ENDIF
	ELSE
		IF NOT DEBUG_ARE_ANY_SCRIPTED_CAMERAS_RENDERING()
		AND (Data.eState = ARCADE_CABINET_STATE_LOADING)
			IF DOES_CAM_EXIST(Data.CameraData.ciMainCamera)
				SET_CAM_ACTIVE(Data.CameraData.ciMainCamera, FALSE)
				RENDER_SCRIPT_CAMS(FALSE, FALSE)
				DESTROY_CAM(Data.CameraData.ciMainCamera)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC DEBUG_SHAKE_CAMERA(BOOL bImpactShake = FALSE)
	
	CAMERA_INDEX activeCam 	= GET_RENDERING_CAM()
	FLOAT fMaxShake			= tfST_CAMERA_MAX_SHAKE
	STRING sShakeType 		= DEBUG_GET_STRENGTH_TEST_CAMERA_TYPE(DebugData.CameraDebug.iShakeType)
	
	IF (bImpactShake)
		fMaxShake			= tfST_CAMERA_MAX_IMPACT_SHAKE
		sShakeType			= DEBUG_GET_STRENGTH_TEST_CAMERA_TYPE(DebugData.CameraDebug.iImpactShakeType)
	ENDIF
	
	// Check if one of the script cams are active
	IF DOES_CAM_EXIST(activeCam)
	AND IS_CAM_ACTIVE(activeCam)
		SHAKE_CAM(activeCam, sShakeType, fMaxShake)
	ELSE
		// No script cam is active so use gameplay cam
		SHAKE_GAMEPLAY_CAM(sShakeType, fMaxShake)
	ENDIF
ENDPROC

PROC DEBUG_UPDATE_CAMERA_SHAKE_WIDGETS()
	IF (debugData.CameraDebug.bTestShake)
		debugData.CameraDebug.bTestShake = FALSE
		DEBUG_SHAKE_CAMERA()
	ENDIF
	IF (debugData.CameraDebug.bTestImpactShake)
		debugData.CameraDebug.bTestImpactShake = FALSE
		DEBUG_SHAKE_CAMERA(TRUE)
	ENDIF
ENDPROC

PROC DEBUG_WRITE_CAMERA_DATA_TO_FILE(DEBUG_CAMERA_DATA &debugCameraData, STRING sCamHeading, STRING sPath, TEXT_LABEL_63 tlFile, STRENGTH_TEST_CAMERAS eCamType)
	SAVE_STRING_TO_NAMED_DEBUG_FILE(sCamHeading, sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("Coords: ", sPath, tlFile)
	SAVE_VECTOR_TO_NAMED_DEBUG_FILE(debugCameraData.vCoords, sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("Rotation: ", sPath, tlFile)
	SAVE_VECTOR_TO_NAMED_DEBUG_FILE(debugCameraData.vRotation, sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("FOV: ", sPath, tlFile)
	SAVE_FLOAT_TO_NAMED_DEBUG_FILE(debugCameraData.fFOV, sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	IF (eCamType = ST_CAMERA_STARTING)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("Starting Interp Time: ", sPath, tlFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE(tiST_CAMERA_STARTING_LERP_TIME_MS, sPath, tlFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("Ending Interp Time: ", sPath, tlFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE(tiST_CAMERA_ENDING_LERP_TIME_MS, sPath, tlFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	ENDIF
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
ENDPROC

PROC DEBUG_WRITE_CAMERA_CATCH_DATA_TO_FILE(STRING sPath, TEXT_LABEL_63 tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("Catch Stage Data", sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("Lerp Amount Stage 1: ", sPath, tlFile)
	SAVE_FLOAT_TO_NAMED_DEBUG_FILE(tfST_CAMERA_ANIM_WEIGHT_OFFSET_STAGE_1, sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("Lerp Amount Stage 2: ", sPath, tlFile)
	SAVE_FLOAT_TO_NAMED_DEBUG_FILE(tfST_CAMERA_ANIM_WEIGHT_OFFSET_STAGE_2, sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("Lerp Amount Stage 3: ", sPath, tlFile)
	SAVE_FLOAT_TO_NAMED_DEBUG_FILE(tfST_CAMERA_ANIM_WEIGHT_OFFSET_STAGE_3, sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("Lerp Amount Stage 4: ", sPath, tlFile)
	SAVE_FLOAT_TO_NAMED_DEBUG_FILE(tfST_CAMERA_ANIM_WEIGHT_OFFSET_STAGE_4, sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("Lerp Amount Stage 5: ", sPath, tlFile)
	SAVE_FLOAT_TO_NAMED_DEBUG_FILE(tfST_CAMERA_ANIM_WEIGHT_OFFSET_STAGE_5, sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("Stage 1 Lerp Time MS: ", sPath, tlFile)
	SAVE_INT_TO_NAMED_DEBUG_FILE(tiST_ANIM_FINISH_BLEND_TIME_MS_STAGE_1, sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("Stage 2 Lerp Time MS: ", sPath, tlFile)
	SAVE_INT_TO_NAMED_DEBUG_FILE(tiST_ANIM_FINISH_BLEND_TIME_MS_STAGE_2, sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("Stage 3 Lerp Time MS: ", sPath, tlFile)
	SAVE_INT_TO_NAMED_DEBUG_FILE(tiST_ANIM_FINISH_BLEND_TIME_MS_STAGE_3, sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("Stage 4 Lerp Time MS: ", sPath, tlFile)
	SAVE_INT_TO_NAMED_DEBUG_FILE(tiST_ANIM_FINISH_BLEND_TIME_MS_STAGE_4, sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("Stage 5 Lerp Time MS: ", sPath, tlFile)
	SAVE_INT_TO_NAMED_DEBUG_FILE(tiST_ANIM_FINISH_BLEND_TIME_MS_STAGE_5, sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
ENDPROC

FUNC STRING DEBUG_GET_STRENGTH_TEST_INTERPOLATION_TYPE_NAME(INT iInterpolationType)
	STRING sInterpolationTypeName = ""
	SWITCH iInterpolationType
		CASE 0	sInterpolationTypeName = "INTERPTYPE_LINEAR"				BREAK
		CASE 1	sInterpolationTypeName = "INTERPTYPE_ACCEL"					BREAK
		CASE 2	sInterpolationTypeName = "INTERPTYPE_DECEL"					BREAK
		CASE 3	sInterpolationTypeName = "INTERPTYPE_COSINE"				BREAK
		CASE 4	sInterpolationTypeName = "INTERPTYPE_SMOOTHSTEP"			BREAK
		CASE 5	sInterpolationTypeName = "INTERPTYPE_SPHERICAL_LINEAR"		BREAK
		CASE 6	sInterpolationTypeName = "INTERPTYPE_SPHERICAL_ACCEL"		BREAK
		CASE 7	sInterpolationTypeName = "INTERPTYPE_SPHERICAL_DECEL"		BREAK
		CASE 8	sInterpolationTypeName = "INTERPTYPE_SPHERICAL_COSINE"		BREAK
		CASE 9	sInterpolationTypeName = "INTERPTYPE_SPHERICAL_SMOOTHSTEP"	BREAK
	ENDSWITCH
	RETURN sInterpolationTypeName
ENDFUNC

PROC DEBUG_WRITE_CAMERA_INTERPOLATION_DATA_TO_FILE(STRING sPath, TEXT_LABEL_63 tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("Interpolation Data", sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("Interpolation Type: ", sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE(DEBUG_GET_STRENGTH_TEST_INTERPOLATION_TYPE_NAME(Data.CameraData.iInterpolationType), sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
ENDPROC

PROC DEBUG_WRITE_CAMERA_SHAKE_DATA_TO_FILE(STRING sPath, TEXT_LABEL_63 tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("Cam Shake Data", sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("Min Shake Interval Time MS: ", sPath, tlFile)
	SAVE_INT_TO_NAMED_DEBUG_FILE(tiST_CAMERA_MIN_SHAKE_INTERVAL_TIME_MS, sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("Max Shake Interval Time MS: ", sPath, tlFile)
	SAVE_INT_TO_NAMED_DEBUG_FILE(tiST_CAMERA_MAX_SHAKE_INTERVAL_TIME_MS, sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("Shake Type: ", sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE(DEBUG_GET_STRENGTH_TEST_CAMERA_TYPE(DebugData.CameraDebug.iShakeType), sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("Max Shake: ", sPath, tlFile)
	SAVE_FLOAT_TO_NAMED_DEBUG_FILE(tfST_CAMERA_MAX_SHAKE, sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("Cam Impact Shake Data", sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("Impact Shake Type: ", sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE(DEBUG_GET_STRENGTH_TEST_CAMERA_TYPE(DebugData.CameraDebug.iImpactShakeType), sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("Max Impact Shake: ", sPath, tlFile)
	SAVE_FLOAT_TO_NAMED_DEBUG_FILE(tfST_CAMERA_MAX_IMPACT_SHAKE, sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
ENDPROC

PROC DEBUG_OUTPUT_CAMERA_DATA()
	STRING sPath = "X:/gta5/titleupdate/dev_ng/"
	
	// File Name
	TEXT_LABEL_63 tlFile = "Axe_of_Fury_Cam_Data"
	INT iYear, iMonth, iDay, iHour, iMins, iSec
	
	tlFile += "/"
	tlFile += "Cam_Data_"
	
	GET_LOCAL_TIME(iYear, iMonth, iDay, iHour, iMins, iSec)
	
	IF (iHour < 10)
		tlFile += 0
	ENDIF
	tlFile += iHour
	tlFile += "."
	IF (iMins < 10)
		tlFile += 0
	ENDIF
	tlFile += iMins
	tlFile += "."
	IF (iSec < 10)
		tlFile += 0
	ENDIF
	tlFile += iSec
	tlFile += "_"
	
	tlFile += iDay
	tlFile += "."
	tlFile += iMonth
	tlFile += "."
	tlFile += iYear
	tlFile += ".txt"
	
	CLEAR_NAMED_DEBUG_FILE(sPath, tlFile)
	OPEN_NAMED_DEBUG_FILE(sPath, tlFile)
		
		DEBUG_WRITE_CAMERA_DATA_TO_FILE(debugData.CameraDebug.StartingCamData, "Starting Cam Data", sPath, tlFile, ST_CAMERA_STARTING)
		DEBUG_WRITE_CAMERA_DATA_TO_FILE(debugData.CameraDebug.StageOneCamData, "Stage One Cam Data", sPath, tlFile, ST_CAMERA_STAGE_ONE)
		DEBUG_WRITE_CAMERA_DATA_TO_FILE(debugData.CameraDebug.StageTwoCamData, "Stage Two Cam Data", sPath, tlFile, ST_CAMERA_STAGE_TWO)
		DEBUG_WRITE_CAMERA_DATA_TO_FILE(debugData.CameraDebug.StageThreeCamData, "Stage Three Cam Data", sPath, tlFile, ST_CAMERA_STAGE_THREE)
		DEBUG_WRITE_CAMERA_DATA_TO_FILE(debugData.CameraDebug.StageFourCamData, "Stage Four Cam Data", sPath, tlFile, ST_CAMERA_STAGE_FOUR)
		DEBUG_WRITE_CAMERA_DATA_TO_FILE(debugData.CameraDebug.StageFiveCamData, "Stage Five Cam Data", sPath, tlFile, ST_CAMERA_STAGE_FIVE)
		DEBUG_WRITE_CAMERA_CATCH_DATA_TO_FILE(sPath, tlFile)
		DEBUG_WRITE_CAMERA_INTERPOLATION_DATA_TO_FILE(sPath, tlFile)
		DEBUG_WRITE_CAMERA_SHAKE_DATA_TO_FILE(sPath, tlFile)
		
	CLOSE_DEBUG_FILE()
ENDPROC

PROC UPDATE_DEBUG_WIDGETS()
	
	IF (NOT DebugData.bEnableWidgets)
		EXIT
	ENDIF
	
	// Camera widgets
	DEBUG_UPDATE_CAMERA_WIDGETS(debugData.CameraDebug.StartingCamData)
	DEBUG_UPDATE_CAMERA_WIDGETS(debugData.CameraDebug.StageOneCamData)
	DEBUG_UPDATE_CAMERA_WIDGETS(debugData.CameraDebug.StageTwoCamData)
	DEBUG_UPDATE_CAMERA_WIDGETS(debugData.CameraDebug.StageThreeCamData)
	DEBUG_UPDATE_CAMERA_WIDGETS(debugData.CameraDebug.StageFourCamData)
	DEBUG_UPDATE_CAMERA_WIDGETS(debugData.CameraDebug.StageFiveCamData)
	DEBUG_UPDATE_CAMERA_SHAKE_WIDGETS()
	
	IF (debugData.CameraDebug.bOutputData)
		debugData.CameraDebug.bOutputData = FALSE
		DEBUG_OUTPUT_CAMERA_DATA()
	ENDIF
	
	IF (debugData.CameraDebug.bUpdateDebugValuesInFlow)
		debugData.CameraDebug.bUpdateDebugValuesInFlow = FALSE
		CLEAR_BIT(data.iBS, BS_ST_SET_CAMERA_LERP_VALUES)
	ENDIF
	
	// Animation Percentage
	IF (DebugData.iAnimationPercentage != -1)
		Data.HUDData.fHUDMeterProgress = TO_FLOAT(DebugData.iAnimationPercentage/2)
	ENDIF
	
	// UI Debug
	IF (DebugData.UIDebug.fFillValue != 0.0)
		SET_STRENGTH_TEST_SWING_METER_FILL_VALUE(DebugData.UIDebug.fFillValue)
	ENDIF
	
	IF (DebugData.UIDebug.fShowBarMeter)
		DebugData.UIDebug.fShowBarMeter = FALSE
		SET_STRENGTH_TEST_SWING_METER_BAR_LINE_VISIBLE()
	ENDIF
	
	IF (DebugData.UIDebug.fBarValue != 1.0)
		SET_STRENGTH_TEST_SWING_METER_BAR_VALUE(DebugData.UIDebug.fBarValue)
	ENDIF
	
	// Swing Multiplier
	// Use debug values if they have changed
	IF (DebugData.UIDebug.fBarValue != 1.0)
		IF (DebugData.UIDebug.fFillValue != 0.0)
			DebugData.UIDebug.fSwingMultiplier = GET_STRENGTH_TEST_SWING_MULTIPLIER(DebugData.UIDebug.fFillValue, DebugData.UIDebug.fBarValue)
		ELSE
			DebugData.UIDebug.fSwingMultiplier = GET_STRENGTH_TEST_SWING_MULTIPLIER(Data.HUDData.fNormMeterFillValue, DebugData.UIDebug.fBarValue)
		ENDIF
	ELSE
		IF (DebugData.UIDebug.fFillValue != 0.0)
			DebugData.UIDebug.fSwingMultiplier = GET_STRENGTH_TEST_SWING_MULTIPLIER(DebugData.UIDebug.fFillValue, Data.HUDData.fNormMeterBarValue)
		ELSE
			DebugData.UIDebug.fSwingMultiplier = GET_STRENGTH_TEST_SWING_MULTIPLIER(Data.HUDData.fNormMeterFillValue, Data.HUDData.fNormMeterBarValue)
		ENDIF
	ENDIF
	
ENDPROC
#ENDIF

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════╡ SCRIPT MAINTAIN ╞════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    Conditions for if this child script should be killed.
/// RETURNS: TRUE if the script should be killed.
FUNC BOOL SHOULD_KILL_THIS_SCRIPT()
	IF SHOULD_KILL_ACM_SCRIPT()
		PRINTLN("[AM_MP_ARCADE_STRENGTH_TEST] - SHOULD_KILL_THIS_SCRIPT - SHOULD_KILL_DRONE_SCRIPT TRUE")
		RETURN TRUE
	ENDIF
	
	IF NOT IS_SKYSWOOP_AT_GROUND()
		PRINTLN("[AM_MP_ARCADE_STRENGTH_TEST] - SHOULD_KILL_THIS_SCRIPT - IS_SKYSWOOP_AT_GROUND is false - TRUE")
		RETURN TRUE
	ENDIF

	IF IS_PLAYER_WALKING_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
		PRINTLN("[AM_MP_ARCADE_STRENGTH_TEST] - SHOULD_KILL_THIS_SCRIPT - IS_PLAYER_WALKING_OUT_OF_SIMPLE_INTERIOR is true - TRUE")
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_ENTERING_OR_EXITING_PROPERTY(PLAYER_ID())
		PRINTLN("[AM_MP_ARCADE_STRENGTH_TEST] - SHOULD_KILL_THIS_SCRIPT - IS_PLAYER_ENTERING_OR_EXITING_PROPERTY is true - TRUE")
		RETURN TRUE
	ENDIF
	
	IF SHOULD_TERMINATE_PHYSICAL_ARDCADE_GAME(ARCADE_CABINET_SUM_STRENGTH_TEST)
		PRINTLN("[AM_MP_ARCADE_STRENGTH_TEST] - SHOULD_KILL_THIS_SCRIPT - force cleanup is true - TRUE")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

SCRIPT (ACM_CHILD_SCRIPT_LAUNCHER_STRUCT missionScriptArgs)

	IF NETWORK_IS_GAME_IN_PROGRESS() 
		SCRIPT_INITIALISE(missionScriptArgs)	
	ELSE
		SCRIPT_CLEANUP(missionScriptArgs)
	ENDIF
	
	#IF IS_DEBUG_BUILD
	INITIALISE_DEBUG_WIDGETS()
	CREATE_DEBUG_WIDGETS()
	#ENDIF
	
	WHILE TRUE
		MP_LOOP_WAIT_ZERO()
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		SCRIPT_PROFILER_START_OF_FRAME() 
		#ENDIF
		#ENDIF	
		
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			PRINTLN("[AM_MP_ARCADE_STRENGTH_TEST] - SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE = TRUE")
			SCRIPT_CLEANUP(missionScriptArgs)
		ENDIF
		
		IF SHOULD_KILL_THIS_SCRIPT()
			PRINTLN("[AM_MP_ARCADE_STRENGTH_TEST] - SHOULD_KILL_THIS_SCRIPT = TRUE")
			SCRIPT_CLEANUP(missionScriptArgs)
		ENDIF
		
		RUN_MAIN_CLIENT_LOGIC(missionScriptArgs)
		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			RUN_MAIN_SERVER_LOGIC()
		ENDIF
		
		#IF IS_DEBUG_BUILD
		UPDATE_DEBUG_WIDGETS()
		
		#IF SCRIPT_PROFILER_ACTIVE
		SCRIPT_PROFILER_END_OF_FRAME() 
		#ENDIF
		#ENDIF
	ENDWHILE
#ENDIF	
	
#IF NOT FEATURE_SUMMER_2020
SCRIPT
#ENDIF	
	
ENDSCRIPT 
