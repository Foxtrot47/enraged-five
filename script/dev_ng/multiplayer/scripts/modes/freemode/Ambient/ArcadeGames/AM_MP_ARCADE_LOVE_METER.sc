//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        AM_MP_ARCADE_LOVE_METER.sc																				//
// Description: This is the implementation of love meter arcade game. This script will mange the love meter gameplay    // 
// 				functionality. This script will wait for ready flag from arcade manager script (AM_MP_ARC_CAB_MANAGER)  //
//				to start the game.							 															//
// Written by:  Online Technical Design: Ata Tabrizi																	//
// Date:  		28/08/2019																								//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_event.sch"
USING "net_include.sch"
USING "net_events.sch"
USING "net_wait_zero.sch"
USING "net_common_functions.sch"
USING "net_simple_interior.sch"

#IF IS_DEBUG_BUILD
USING "net_debug_log.sch"
#ENDIF

#IF FEATURE_CASINO_HEIST
USING "net_arcade_cabinet.sch"
USING "arcade_games_help_text_flow.sch"


//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════════════════╡ ENUMS ╞═════════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

ENUM LOVE_METER_GAME_STATE
	LOVE_METER_WAIT_FOR_OTHER_PLAYER_STATE,
	LOVE_METER_CALCULATE_TOTAL_STATS,
	LOVE_METER_CALCULATE_ASCII,
	LOVE_METER_COMPARE_FIRST_VALUES,
	LOVE_METER_CALCULATE_FINAL_VALUES,
	LOVE_METER_PLAY_ANIM,
	LOVE_METER_CLEANUP_ANIM,
	LOVE_METER_GAME_CLEANUP
ENDENUM

ENUM LOVE_METER_PATTERNS
	PATTERN_OFF = -1,
	PATTERN_CYCLE,
	PATTERN_FLASH,
	PATTERN_CALCULATE,
	PATTERN_MATCHING,
	PATTERN_END_MATCH,
	PATTERN_END_LOVE,
	PATTERN_END_NEMESIS
ENDENUM

ENUM LOVE_METER_LIGHT_MANAGEMENT_STATE
	LOVE_METER_LIGHT_IDLE_STATE,
	LOVE_METER_LIGHT_CALCULATE_STATE,
	LOVE_METER_LIGHT_MATCHING_STATE,
	LOVE_METER_LIGHT_END_MATCHING_STATE
ENDENUM

//lights
CONST_FLOAT LOVE_METER_BAR_Y_0			1.5
CONST_FLOAT LOVE_METER_BAR_Y_1			1.44
CONST_FLOAT LOVE_METER_BAR_Y_2			1.370
CONST_FLOAT LOVE_METER_BAR_Y_3			1.299
CONST_FLOAT LOVE_METER_BAR_Y_4			1.234
CONST_FLOAT LOVE_METER_BAR_Y_5			1.167
CONST_FLOAT LOVE_METER_BAR_Y_6			1.099
CONST_FLOAT LOVE_METER_BAR_Y_7			1.03
CONST_FLOAT LOVE_METER_BAR_Y_8			0.960
CONST_FLOAT LOVE_METER_BAR_Y_9			0.893	
CONST_FLOAT LOVE_METER_BAR_Y_10			0.824
CONST_FLOAT LOVE_METER_BAR_Y_11			0.5

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ DATA STRUCTURES ╞═════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

STRUCT ServerBroadcastData
	INT iServerBS
	INT iMatchID
	PLAYER_INDEX playerOne, playerTwo
	SCRIPT_TIMER tCalculationTimer
ENDSTRUCT

STRUCT PlayerBroadcastData
	INT iLocalBS
	INT iTotalStats
	INT iSumNameASCII
	INT iFirstValue, iFinalScore
	INT iRandomScore
	INT iPlayerNameLength
ENDSTRUCT

STRUCT LOVE_METER_LIGHTS
	OBJECT_INDEX Objs[20]
	INT iRenderTargetID = -1
	BOOL bRegisteredRenderTarget
	FLOAT fMeterRectLoc = LOVE_METER_BAR_Y_0
	FLOAT fMeterSpeed = 0.125
	INT iMeterColours[3]
	INT iMeterAlpha = 255
	INT playerScore[2]
	
	SCRIPT_TIMER patternTimer
	INT iPatternID
	INT iPatternLightID
	INT iLightFlashTime = 250
	
	INT iLoopsComplete
	
	BOOL bPatternFinished

	//SCRIPT_TIMER generalTimer
	
	//FLOAT fMeterRextPositions[12]
ENDSTRUCT

STRUCT LOVE_METER_DATA
	INT iBS
	INT iTotalFirstValues
	INT iPlayerSlot
	INT iCalSoundID = -1
	TEXT_LABEL_63 sInitialAnimDic
	
	OBJECT_INDEX cabinetObj
	PLAYER_INDEX secondPlayer
	
	ARCADE_CABINET_CHILD_SCRIPT_CLIENT_STATE eState
	ARCADE_CABINET_ANIM_EVENT_STRUCT sArcadeCabinetAnimEvent
	ARCADE_CABINET_ANIM_CLIPS eAnimClip
	ARCADE_CABINETS eArcadeCabinetLocateType
	LOVE_METER_GAME_STATE eGameState
	LOVE_METER_LIGHT_MANAGEMENT_STATE eLightState
	SCRIPT_CONTROL_HOLD_TIMER sControlTimer
	ARCADE_CABINET_AI_CONTEXT_STRUCT sArcadeCabinetAIContext
	
	SCRIPT_TIMER tHypeAIContextTimer
	SCRIPT_TIMER tNoPartnerContextTimer
	
	LOVE_METER_LIGHTS lights
ENDSTRUCT

#IF IS_DEBUG_BUILD
STRUCT LOVE_METER_DEBUG_DATA
	INT iDebugFinalValue = -1
	BOOL bDisplayScores
	BOOL bMatchRandomScore, bSoloGame
	INT iStatDriving, iStatFlying, iStatShooting, iStatStamina
	INT iStatStealth, iStatStrength, iLapDances, iSexActs
	
	SCRIPT_TIMER stTestTimer
	BOOL db_bVisTest
	BOOL db_bTrueLove
	BOOL db_bNemesis
	INT db_Score[2]
	INT idb_LoveMeterPattern = -1
	INT iOverrideFlashSpeed
ENDSTRUCT
#ENDIF

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡    CONSTS    ╞════════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

// PlayerBroadcastData.iLocalBS
CONST_INT LOVE_METER_PLAYER_BD_FIRST_TOTAL_CALCULATED						0
CONST_INT LOVE_METER_PLAYER_BD_FINAL_SCORE_CALCULATED						1
CONST_INT LOVE_METER_PLAYER_BD_FINAL_ANIM_STARTED							2
CONST_INT LOVE_METER_PLAYER_BD_FINAL_ANIM_ENDED								3
CONST_INT LOVE_METER_PLAYER_BD_USING_LOVE_METER								4

// sData.iBS

// serverBroadcastData.iServerBS
CONST_INT LOVE_METER_SERVER_BD_GET_USING_PLAYERS							0
CONST_INT LOVE_METER_SERVER_BD_CLEANUP_PLAYERS_GAME_STATE					1



//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡    VARIABLES    ╞═════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

PlayerBroadcastData playerBD[NUM_NETWORK_PLAYERS]
ServerBroadcastData serverBD
LOVE_METER_DATA sData

#IF IS_DEBUG_BUILD
LOVE_METER_DEBUG_DATA sDebugData
#ENDIF

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════════╡     DEBUG     ╞══════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛
#IF IS_DEBUG_BUILD
	FUNC STRING GET_BIT_SET_NAME(INT iBit)
		SWITCH iBit
		ENDSWITCH
	RETURN ""
ENDFUNC

FUNC STRING GET_LOVE_METER_STATE_NAME(LOVE_METER_GAME_STATE eGameState)
	SWITCH eGameState
		CASE LOVE_METER_WAIT_FOR_OTHER_PLAYER_STATE 	RETURN "LOVE_METER_WAIT_FOR_OTHER_PLAYER_STATE"
		CASE LOVE_METER_CALCULATE_ASCII					RETURN "LOVE_METER_CALCULATE_ASCII"
		CASE LOVE_METER_CALCULATE_TOTAL_STATS			RETURN "LOVE_METER_CALCULATE_TOTAL_STATS"
		CASE LOVE_METER_COMPARE_FIRST_VALUES			RETURN "LOVE_METER_COMPARE_FIRST_VALUES"
		CASE LOVE_METER_CALCULATE_FINAL_VALUES			RETURN "LOVE_METER_CALCULATE_FINAL_VALUES"
		CASE LOVE_METER_GAME_CLEANUP					RETURN "LOVE_METER_GAME_CLEANUP"
		CASE LOVE_METER_PLAY_ANIM						RETURN "LOVE_METER_PLAY_ANIM"
		CASE LOVE_METER_CLEANUP_ANIM					RETURN "LOVE_METER_CLEANUP_ANIM"
	ENDSWITCH
	RETURN "INVALID"
ENDFUNC

FUNC STRING GET_LOVE_METER_LIGHT_STATE_NAME(LOVE_METER_LIGHT_MANAGEMENT_STATE eGameState)
	SWITCH eGameState
		CASE LOVE_METER_LIGHT_IDLE_STATE 			RETURN "LOVE_METER_LIGHT_IDLE_STATE"
		CASE LOVE_METER_LIGHT_CALCULATE_STATE		RETURN "LOVE_METER_LIGHT_CALCULATE_STATE"
		CASE LOVE_METER_LIGHT_MATCHING_STATE		RETURN "LOVE_METER_LIGHT_MATCHING_STATE"
		CASE LOVE_METER_LIGHT_END_MATCHING_STATE	RETURN "LOVE_METER_LIGHT_END_MATCHING_STATE"
	ENDSWITCH
	RETURN "INVALID"
ENDFUNC

PROC CREATE_DEBUG_WIDGET()
	START_WIDGET_GROUP("AM_MP_ARCADE_LOVE_METER")
		START_WIDGET_GROUP("Final Value")
			ADD_WIDGET_INT_SLIDER("final value", sDebugData.iDebugFinalValue,  -1, 10, 1)
			ADD_WIDGET_BOOL("Match random score", sDebugData.bMatchRandomScore)
			ADD_WIDGET_BOOL("Solo Game", sDebugData.bSoloGame)
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Player Stats")
			ADD_WIDGET_INT_SLIDER("Driving ", sDebugData.iStatDriving,  -1, 1000000000, 1)
			ADD_WIDGET_INT_SLIDER("Flying ", sDebugData.iStatFlying,  -1, 1000000000, 1)
			ADD_WIDGET_INT_SLIDER("Shooting ", sDebugData.iStatShooting,  -1, 1000000000, 1)
			ADD_WIDGET_INT_SLIDER("Stamina ", sDebugData.iStatStamina,  -1, 1000000000, 1)
			ADD_WIDGET_INT_SLIDER("Stealth ", sDebugData.iStatStealth,  -1, 1000000000, 1)
			ADD_WIDGET_INT_SLIDER("Strength ", sDebugData.iStatStrength,  -1, 1000000000, 1)
			ADD_WIDGET_INT_SLIDER("Lap Dance ", sDebugData.iLapDances,  -1, 1000000000, 1)
			ADD_WIDGET_INT_SLIDER("Sex ", sDebugData.iSexActs,  -1, 1000000000, 1)
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Display scores")
			ADD_WIDGET_BOOL("Display", sDebugData.bDisplayScores)
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Lights")
			ADD_WIDGET_BOOL("db_bVisTest", sDebugData.db_bVisTest)
			ADD_WIDGET_INT_SLIDER("Player1 Score",sDebugData.db_Score[0],0,11,1)
			ADD_WIDGET_INT_SLIDER("Player2 Score",sDebugData.db_Score[1],0,11,1)
			ADD_WIDGET_BOOL("db_bTrueLove",sDebugData.db_bTrueLove)
			ADD_WIDGET_BOOL("db_bNemesis",sDebugData.db_bNemesis)
			ADD_WIDGET_FLOAT_SLIDER("Love bar",sData.lights.fMeterRectLoc,-10,10,0.001)
			ADD_WIDGET_FLOAT_SLIDER("Love Bar speed",sData.lights.fMeterSpeed,-1,1,0.1)
			ADD_WIDGET_INT_SLIDER("Love Bar R",sData.lights.iMeterColours[0],0,255,1)
			ADD_WIDGET_INT_SLIDER("Love Bar G",sData.lights.iMeterColours[1],0,255,1)
			ADD_WIDGET_INT_SLIDER("Love Bar B",sData.lights.iMeterColours[2],0,255,1)
			ADD_WIDGET_INT_SLIDER("Light Pattern",sDebugData.idb_LoveMeterPattern,-1,ENUM_TO_INT(PATTERN_END_NEMESIS),1)
			ADD_WIDGET_INT_SLIDER("Light Speed",sDebugData.iOverrideFlashSpeed,0,1000,1)
		STOP_WIDGET_GROUP()
			
	STOP_WIDGET_GROUP()
ENDPROC
#ENDIF

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════╡     GETTER & SETTER     ╞═════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

PROC SET_LOVE_METER_GAME_STATE(LOVE_METER_GAME_STATE eGameState)
	IF sData.eGameState !=  eGameState
		PRINTLN("[AM_ARCADE_LOVE_METER] - SET_LOVE_METER_GAME_STATE FROM: ", GET_LOVE_METER_STATE_NAME(sData.eGameState), " TO: ", GET_LOVE_METER_STATE_NAME(eGameState))
		sData.eGameState = eGameState
	ENDIF
ENDPROC

PROC SET_LOVE_METER_LIGHT_STATE(LOVE_METER_LIGHT_MANAGEMENT_STATE eLightState)
	IF sData.eLightState !=  eLightState
		PRINTLN("[AM_ARCADE_LOVE_METER] - SET_LOVE_METER_LIGHT_STATE FROM: ", GET_LOVE_METER_LIGHT_STATE_NAME(sData.eLightState), " TO: ", GET_LOVE_METER_LIGHT_STATE_NAME(eLightState))
		sData.eLightState = eLightState
	ENDIF
ENDPROC

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡     CLEANUP     ╞══════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

PROC SCRIPT_CLEANUP()
	PRINTLN("[AM_ARCADE_LOVE_METER] - SCRIPT_CLEANUP called")
	CLEAR_BIT(globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].sArcadeManagerGlobalPlayerBD.iArcadeCabinetBS, ARCADE_CABINET_GLOBAL_BD_PLAYER_SWITCHED_LOVE_DIC)
	INT i
	REPEAT 20 i
		IF DOES_ENTITY_EXIST(sData.lights.Objs[i])
			DELETE_OBJECT(sData.lights.Objs[i])
		ENDIF
	ENDREPEAT
	
	IF sData.lights.iRenderTargetID != -1
		IF IS_NAMED_RENDERTARGET_REGISTERED("Arc_Love_01a")   
			RELEASE_NAMED_RENDERTARGET("Arc_Love_01a")
		ENDIF
		sData.lights.iRenderTargetID = -1
	ENDIF

	SET_TERMINATE_PHYSICAL_ARCADE_GAME_FLAG(ARCADE_CABINET_CH_LOVE_METER, FALSE)
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════════╡     INITIALISE     ╞════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

PROC INITIALISE()
	#IF IS_DEBUG_BUILD
	CREATE_DEBUG_WIDGET()
	sDebugData.iStatDriving = -1
	sDebugData.iStatFlying = -1
	sDebugData.iStatShooting = -1
	sDebugData.iStatStamina = -1
	sDebugData.iStatStealth = -1
	sDebugData.iStatStrength = -1
	sDebugData.iLapDances = -1
	sDebugData.iSexActs = -1
	#ENDIF
	
	sData.lights.iMeterColours[0] = 247
	sData.lights.iMeterColours[1] = 112
	sData.lights.iMeterColours[2] = 164

	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		serverBD.playerOne = INVALID_PLAYER_INDEX()
		serverBD.playerTwo = INVALID_PLAYER_INDEX()
	ENDIF
	
	SET_TERMINATE_PHYSICAL_ARCADE_GAME_FLAG(ARCADE_CABINET_CH_LOVE_METER, FALSE)
ENDPROC

PROC SCRIPT_INITIALISE(ACM_CHILD_SCRIPT_LAUNCHER_STRUCT missionScriptArgs)
	#IF IS_DEBUG_BUILD
	PRINTLN("[AM_ARCADE_LOVE_METER] - SCRIPT_INITIALISE - Launching instance: ", missionScriptArgs.iInstanceId)
	#ENDIF
		
	NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE, missionScriptArgs.iInstanceId)
	
	// This makes sure the net script is active, waits untill it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD, SIZE_OF(playerBD))

	// KGM: Wait for the first network broadcast before moving on
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		PRINTLN("[AM_ARCADE_LOVE_METER] - SCRIPT_INITIALISE FAILED TO RECEIVE INITIAL NETWORK BROADCAST - SCRIPT CLEANUP")
		SCRIPT_CLEANUP()
	ENDIF
	
	sData.cabinetObj = missionScriptArgs.cabinetObj
	sData.eArcadeCabinetLocateType = missionScriptArgs.eArcadeCabinetLocateType
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		PRINTLN("[AM_ARCADE_LOVE_METER] INITIALISED arcade cabinet manager script")
		
		// This script will not be paused if another script calls PAUSE_GAME
		PRINTLN("[AM_ARCADE_LOVE_METER] - INITIALISED")
	ELSE
		PRINTLN("[AM_ARCADE_LOVE_METER] - INITIALISED NETWORK_IS_GAME_IN_PROGRESS = FALSE - SCRIPT CLEANUP")
		SCRIPT_CLEANUP()
	ENDIF
	
	INITIALISE()
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ QUERY BITS ╞══════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

PROC SET_ARCADE_LOVE_METER_BIT(INT iBit)
	IF NOT IS_BIT_SET(sData.iBS, iBit)
		SET_BIT(sData.iBS, iBit)
		PRINTLN("[AM_ARCADE_LOVE_METER] SET_ARCADE_LOVE_METER_BIT ", GET_BIT_SET_NAME(iBit) , " TRUE")
	ENDIF
ENDPROC

FUNC BOOL IS_ARCADE_LOVE_METER_BIT_SET(INT iBit)
	RETURN IS_BIT_SET(sData.iBS, iBit)
ENDFUNC

PROC CLEAR_ARCADE_LOVE_METER_BIT(INT iBit)
	IF IS_BIT_SET(sData.iBS, iBit)
		CLEAR_BIT(sData.iBS, iBit)
		PRINTLN("[AM_ARCADE_LOVE_METER] CLEAR_ARCADE_LOVE_METER_BIT ", GET_BIT_SET_NAME(iBit) , " FALSE")
	ENDIF
ENDPROC


//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════════╡     FUNCS & PROC     ╞══════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

FUNC BOOL SHOULD_KILL_THIS_SCRIPT()
	IF SHOULD_KILL_ACM_SCRIPT()
		PRINTLN("[AM_ARCADE_LOVE_METER] - SHOULD_KILL_THIS_SCRIPT - SHOULD_KILL_DRONE_SCRIPT TRUE")
		RETURN TRUE
	ENDIF
	
	IF NOT IS_SKYSWOOP_AT_GROUND()
		PRINTLN("[AM_ARCADE_LOVE_METER] - SHOULD_KILL_THIS_SCRIPT - IS_SKYSWOOP_AT_GROUND is false - TRUE")
		RETURN TRUE
	ENDIF

	IF IS_PLAYER_WALKING_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
		PRINTLN("[AM_ARCADE_LOVE_METER] - SHOULD_KILL_THIS_SCRIPT - IS_PLAYER_WALKING_OUT_OF_SIMPLE_INTERIOR is true - TRUE")
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_ENTERING_OR_EXITING_PROPERTY(PLAYER_ID())
		PRINTLN("[AM_ARCADE_LOVE_METER] - SHOULD_KILL_THIS_SCRIPT - IS_PLAYER_ENTERING_OR_EXITING_PROPERTY is true - TRUE")
		RETURN TRUE
	ENDIF
	
	IF SHOULD_TERMINATE_PHYSICAL_ARDCADE_GAME(ARCADE_CABINET_CH_LOVE_METER)
		PRINTLN("[AM_ARCADE_LOVE_METER] - SHOULD_KILL_THIS_SCRIPT - force cleanup is true - TRUE")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_MOVE_TO_IDLE_STATE()
	IF !IS_PLAYER_USING_ARCADE_CABINET(PLAYER_ID())
		//PRINTLN("[AM_ARCADE_LOVE_METER] - SHOULD_MOVE_TO_IDLE_STATE - IS_PLAYER_USING_ARCADE_CABINET is false")
		RETURN FALSE
	ENDIF
	
	IF !IS_PLAYER_READY_TO_PLAY_ARCADE_GAME(PLAYER_ID())
		//PRINTLN("[AM_ARCADE_LOVE_METER] - SHOULD_MOVE_TO_IDLE_STATE - IS_PLAYER_READY_TO_PLAY_ARCADE_GAME is fals")
		RETURN FALSE
	ENDIF
	
	IF GET_ARCADE_CABINET_LOCATE_PLAYER_IS_IN(PLAYER_ID()) != sData.eArcadeCabinetLocateType
		IF GET_FRAME_COUNT() % 60 = 0
			PRINTLN("[AM_ARCADE_LOVE_METER] - SHOULD_MOVE_TO_IDLE_STATE - player not in correct locate false")
		ENDIF	
		RETURN FALSE
	ENDIF
	
	RETURN TRUE 
ENDFUNC 

PROC MAINTAIN_LOVE_METER_ATTRACT_AI_CONTEXT()
	IF NOT HAS_NET_TIMER_STARTED(sData.tHypeAIContextTimer)
		INT iDisplaySlot = GET_ARCADE_CABINET_LOCATE_DISPLAY_SLOT_PLAYER_IS_IN(PLAYER_ID())
		sData.sArcadeCabinetAIContext.eArcadeCabinet = ARCADE_CABINET_CH_LOVE_METER
		sData.sArcadeCabinetAIContext.sCabinetSlot = INT_TO_ENUM(ARCADE_CAB_MANAGER_SLOT, iDisplaySlot)
		sData.sArcadeCabinetAIContext.stContext = "LOVETEST_ATTRACT"
		PLAY_ARCADE_CABINET_AI_CONTEXT(sData.sArcadeCabinetAIContext, FALSE)			
		IF sData.sArcadeCabinetAIContext.bEventSent
			START_NET_TIMER(sData.tHypeAIContextTimer)
		ENDIF
	ELSE
		IF HAS_NET_TIMER_EXPIRED(sData.tHypeAIContextTimer, 40000)
			RESET_NET_TIMER(sData.tHypeAIContextTimer)
		ENDIF
	ENDIF
ENDPROC

PROC INITIALISE_AI_CONTEXT_VARIABLES()
	INT iDisplaySlot = GET_ARCADE_CABINET_LOCATE_DISPLAY_SLOT_PLAYER_IS_IN(PLAYER_ID())
	sData.sArcadeCabinetAIContext.eArcadeCabinet = ARCADE_CABINET_CH_LOVE_METER
	sData.sArcadeCabinetAIContext.sCabinetSlot = INT_TO_ENUM(ARCADE_CAB_MANAGER_SLOT, iDisplaySlot)
ENDPROC

PROC MAINTAIN_LOVE_METER_LOADING_STATE()
	IF SHOULD_MOVE_TO_IDLE_STATE()
		RESET_NET_TIMER(sData.tHypeAIContextTimer)
		START_NET_TIMER(sData.tNoPartnerContextTimer)
		INITIALISE_AI_CONTEXT_VARIABLES()
		sData.sArcadeCabinetAIContext.stContext = "LOVETEST_START_PLAY"
		PLAY_ARCADE_CABINET_AI_CONTEXT(sData.sArcadeCabinetAIContext, TRUE)
		SET_ARCADE_CABINET_CHILD_SCRIPT_CLIENT_STATE(sData.eState, ARCADE_CABINET_STATE_IDLE)
	ELSE
		IF GET_ARCADE_CABINET_LOCATE_PLAYER_IS_IN(PLAYER_ID()) = sData.eArcadeCabinetLocateType
		AND sData.eArcadeCabinetLocateType = ARCADE_CABINET_CH_LOVE_METER
			MAINTAIN_LOVE_METER_ATTRACT_AI_CONTEXT()
		ELSE
			IF HAS_NET_TIMER_STARTED(sData.tHypeAIContextTimer)
				RESET_NET_TIMER(sData.tHypeAIContextTimer)
			ENDIF			
		ENDIF
		
		RESET_NET_TIMER(sData.tNoPartnerContextTimer)
	ENDIF	
ENDPROC

FUNC INT GET_LOVE_METER_SECOND_PLAYER()
	PLAYER_INDEX secondPlayer
	INT i
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
			secondPlayer = INT_TO_NATIVE(PLAYER_INDEX, i)
			IF PLAYER_ID() != secondPlayer
				IF GET_ARCADE_CABINET_LOCATE_DISPLAY_SLOT_PLAYER_IS_IN(PLAYER_ID()) = GET_ARCADE_CABINET_LOCATE_DISPLAY_SLOT_PLAYER_IS_IN(secondPlayer)
					IF IS_PLAYER_USING_ARCADE_CABINET(secondPlayer)	
						PRINTLN("[AM_ARCADE_LOVE_METER] GET_LOVE_METER_SECOND_PLAYER ", GET_PLAYER_NAME(secondPlayer))
						RETURN i
					ENDIF	
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT	
	RETURN -1
ENDFUNC

FUNC BOOL SHOULD_BLOCK_LOVE_METER()
	IF IS_LOCAL_PLAYER_ARCADE_LOVE_METER_ANIM_PLAYING()
		PRINTLN("[AM_ARCADE_LOVE_METER] SHOULD_BLOCK_LOVE_METER IS_LOCAL_PLAYER_ARCADE_LOVE_METER_ANIM_PLAYING TRUE")
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(serverBD.iServerBS, LOVE_METER_SERVER_BD_CLEANUP_PLAYERS_GAME_STATE)
		PRINTLN("[AM_ARCADE_LOVE_METER] SHOULD_BLOCK_LOVE_METER LOVE_METER_SERVER_BD_CLEANUP_PLAYERS_GAME_STATE TRUE")
		RETURN TRUE
	ENDIF

	IF IS_BIT_SET(serverBD.iServerBS, LOVE_METER_SERVER_BD_GET_USING_PLAYERS)
		PRINTLN("[AM_ARCADE_LOVE_METER] SHOULD_BLOCK_LOVE_METER LOVE_METER_SERVER_BD_GET_USING_PLAYERS TRUE")	
		RETURN TRUE
	ENDIF

	IF !IS_PLAYER_USING_ARCADE_CABINET(PLAYER_ID())	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_BAIL_LOVE_METER_FROM_WAIT_FOR_OTHER_PLAYER_STATE()
	IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
		PRINTLN("[AM_ARCADE_LOVE_METER] SHOULD_BAIL_LOVE_METER_FROM_WAIT_FOR_OTHER_PLAYER_STATE player controls are on TRUE")	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_LOVE_METER_WAIT_FOR_OTHER_PLAYER_STATE()
	IF !SHOULD_BLOCK_LOVE_METER()
		IF GET_LOVE_METER_SECOND_PLAYER() = -1
		#IF IS_DEBUG_BUILD
		AND !sDebugData.bSoloGame
		#ENDIF
			IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
			AND NOT IS_HELP_MESSAGE_ON_SCREEN()
				PRINT_HELP("ARC_CAB_LOVE_WAIT")
			ENDIF	
			
			sData.sControlTimer.control = FRONTEND_CONTROL
			sData.sControlTimer.action = INPUT_FRONTEND_CANCEL
			IF IS_CONTROL_HELD(sData.sControlTimer)
			OR SHOULD_BAIL_LOVE_METER_FROM_WAIT_FOR_OTHER_PLAYER_STATE()
				// AI context
				sData.sArcadeCabinetAIContext.stContext = "LOVETEST_LEAVE"
				PLAY_ARCADE_CABINET_AI_CONTEXT(sData.sArcadeCabinetAIContext, TRUE)
				
				// Anims
				sData.sArcadeCabinetAnimEvent.bHoldLastFrame = FALSE
				sData.sArcadeCabinetAnimEvent.bLoopAnim = FALSE
				PLAY_ARCADE_CABINET_ANIMATION(sData.sArcadeCabinetAnimEvent, ARCADE_CABINET_ANIM_CLIP_EXIT)
				SET_LOVE_METER_GAME_STATE(LOVE_METER_GAME_CLEANUP)
			ENDIF
			
			IF HAS_NET_TIMER_STARTED(sData.tNoPartnerContextTimer)
				IF HAS_NET_TIMER_EXPIRED(sData.tNoPartnerContextTimer, 10000)
					sData.sArcadeCabinetAIContext.stContext = "LOVETEST_NOPARTNER"
					PLAY_ARCADE_CABINET_AI_CONTEXT(sData.sArcadeCabinetAIContext, TRUE)
					REINIT_NET_TIMER(sData.tNoPartnerContextTimer)
				ENDIF
			ENDIF
			
		ELSE
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("ARC_CAB_LOVE_WAIT")
				CLEAR_HELP()
			ENDIF
			
			IF NOT IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iLocalBS, LOVE_METER_PLAYER_BD_USING_LOVE_METER)
				SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iLocalBS, LOVE_METER_PLAYER_BD_USING_LOVE_METER)
				PRINTLN("[AM_ARCADE_LOVE_METER] MAINTAIN_LOVE_METER_WAIT_FOR_OTHER_PLAYER_STATE LOVE_METER_PLAYER_BD_USING_LOVE_METER TRUE")
			ENDIF
			
			sData.iPlayerSlot = GET_ARCADE_CABINET_LOCATE_PLAYER_SLOT_PLAYER_IS_IN(PLAYER_ID())
			
			IF sData.iPlayerSlot = ENUM_TO_INT(ARCADE_CAB_PLAYER_1)
				sData.sArcadeCabinetAIContext.stContext = "LOVETEST_READY"
				PLAY_ARCADE_CABINET_AI_CONTEXT(sData.sArcadeCabinetAIContext, TRUE)
			ENDIF
			
			sData.secondPlayer = INT_TO_PLAYERINDEX(GET_LOVE_METER_SECOND_PLAYER())
			
			#IF IS_DEBUG_BUILD
			IF sDebugData.bSoloGame
				sData.secondPlayer = PLAYER_ID()
				PRINTLN("[AM_ARCADE_LOVE_METER] MAINTAIN_LOVE_METER_WAIT_FOR_OTHER_PLAYER_STATE solo Game")	
			ENDIF
			#ENDIF
			
			SET_LOVE_METER_GAME_STATE(LOVE_METER_CALCULATE_TOTAL_STATS)
		ENDIF
	ELSE
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("ARC_CAB_LOVE_WAIT")
			CLEAR_HELP()
		ENDIF
	ENDIF	
ENDPROC

PROC MAINTAIN_CALCULATE_STATS()
	IF IS_NET_PLAYER_OK(PLAYER_ID())
	AND IS_NET_PLAYER_OK(sData.secondPlayer)
		IF IS_BIT_SET(serverBD.iServerBS, LOVE_METER_SERVER_BD_GET_USING_PLAYERS)
			IF sData.iPlayerSlot = ENUM_TO_INT(ARCADE_CAB_PLAYER_1)
				IF sData.iCalSoundID = -1
					sData.iCalSoundID = GET_SOUND_ID()
					PLAY_SOUND_FROM_COORD(sData.iCalSoundID, "Calculate_Outcome", GET_ENTITY_COORDS(sData.cabinetObj), "DLC_H3_LoveMachine_Sounds", TRUE, 20)
					SET_VARIABLE_ON_SOUND(sData.iCalSoundID, "Time", 8)
				ENDIF	
				sData.sArcadeCabinetAIContext.stContext = "LOVETEST_GUAGE_RISING"
				PLAY_ARCADE_CABINET_AI_CONTEXT(sData.sArcadeCabinetAIContext, TRUE)
			ENDIF
			
			INT iTotalStats = 0
			 
			#IF IS_DEBUG_BUILD 
			IF sDebugData.iStatDriving != -1
				iTotalStats += sDebugData.iStatDriving
				PRINTLN("[AM_ARCADE_LOVE_METER] MAINTAIN_CALCULATE_STATS DEBUG driving stat: ", sDebugData.iStatDriving)
			ELSE
			#ENDIF
				iTotalStats += CALCULATE_PLAYER_STAT_VALUE(CHAR_MULTIPLAYER, PS_DRIVING_ABILITY, FALSE)
				#IF IS_DEBUG_BUILD 
				PRINTLN("[AM_ARCADE_LOVE_METER] MAINTAIN_CALCULATE_STATS driving stat: ", CALCULATE_PLAYER_STAT_VALUE(CHAR_MULTIPLAYER, PS_DRIVING_ABILITY, FALSE))
				#ENDIF
			#IF IS_DEBUG_BUILD 
			ENDIF
			#ENDIF
			
			#IF IS_DEBUG_BUILD 
			IF sDebugData.iStatFlying != -1
			 	iTotalStats += sDebugData.iStatFlying
				PRINTLN("[AM_ARCADE_LOVE_METER] MAINTAIN_CALCULATE_STATS DEBUG flying stat: ", sDebugData.iStatFlying)
			ELSE
			#ENDIF
				iTotalStats += CALCULATE_PLAYER_STAT_VALUE(CHAR_MULTIPLAYER, PS_FLYING_ABILITY, FALSE)
				#IF IS_DEBUG_BUILD 
				PRINTLN("[AM_ARCADE_LOVE_METER] MAINTAIN_CALCULATE_STATS flying stat: ", CALCULATE_PLAYER_STAT_VALUE(CHAR_MULTIPLAYER, PS_FLYING_ABILITY, FALSE))
				#ENDIF
			#IF IS_DEBUG_BUILD 
			ENDIF
			#ENDIF
			
			#IF IS_DEBUG_BUILD 
			IF sDebugData.iStatShooting != -1
				iTotalStats += sDebugData.iStatShooting
				PRINTLN("[AM_ARCADE_LOVE_METER] MAINTAIN_CALCULATE_STATS DEBUG shooting stat: ", sDebugData.iStatShooting)
			ELSE
			#ENDIF
				iTotalStats += CALCULATE_PLAYER_STAT_VALUE(CHAR_MULTIPLAYER, PS_SHOOTING_ABILITY, FALSE)
				#IF IS_DEBUG_BUILD 
				PRINTLN("[AM_ARCADE_LOVE_METER] MAINTAIN_CALCULATE_STATS shooting stat: ", CALCULATE_PLAYER_STAT_VALUE(CHAR_MULTIPLAYER, PS_SHOOTING_ABILITY, FALSE))
				#ENDIF
			#IF IS_DEBUG_BUILD
			ENDIF
			#ENDIF
			
			#IF IS_DEBUG_BUILD 
			IF sDebugData.iStatStamina != -1
			 	iTotalStats += sDebugData.iStatStamina  
				PRINTLN("[AM_ARCADE_LOVE_METER] MAINTAIN_CALCULATE_STATS DEBUG stamina stat: ", sDebugData.iStatStamina)
			ELSE
			#ENDIF
				iTotalStats += CALCULATE_PLAYER_STAT_VALUE(CHAR_MULTIPLAYER, PS_STAMINA, FALSE)
				#IF IS_DEBUG_BUILD 
				PRINTLN("[AM_ARCADE_LOVE_METER] MAINTAIN_CALCULATE_STATS stamina stat: ", CALCULATE_PLAYER_STAT_VALUE(CHAR_MULTIPLAYER, PS_STAMINA, FALSE))
				#ENDIF
			#IF IS_DEBUG_BUILD 
			ENDIF
			#ENDIF
			
			#IF IS_DEBUG_BUILD 
			IF sDebugData.iStatStealth != -1
			 	iTotalStats += sDebugData.iStatStealth  
				PRINTLN("[AM_ARCADE_LOVE_METER] MAINTAIN_CALCULATE_STATS DEBUG stealth stat: ", sDebugData.iStatStealth)
			ELSE
			#ENDIF
				iTotalStats += CALCULATE_PLAYER_STAT_VALUE(CHAR_MULTIPLAYER, PS_STEALTH_ABILITY, FALSE)
				#IF IS_DEBUG_BUILD 
				PRINTLN("[AM_ARCADE_LOVE_METER] MAINTAIN_CALCULATE_STATS stealth stat: ", CALCULATE_PLAYER_STAT_VALUE(CHAR_MULTIPLAYER, PS_STEALTH_ABILITY, FALSE))
				#ENDIF
			#IF IS_DEBUG_BUILD 
			ENDIF
			#ENDIF
			
			#IF IS_DEBUG_BUILD 
			IF sDebugData.iStatStrength != -1
			 	iTotalStats += sDebugData.iStatStrength
				PRINTLN("[AM_ARCADE_LOVE_METER] MAINTAIN_CALCULATE_STATS DEBUG strength stat: ", sDebugData.iStatStrength)
			ELSE
			#ENDIF
				iTotalStats += CALCULATE_PLAYER_STAT_VALUE(CHAR_MULTIPLAYER, PS_STRENGTH, FALSE)
				#IF IS_DEBUG_BUILD 
				PRINTLN("[AM_ARCADE_LOVE_METER] MAINTAIN_CALCULATE_STATS strength stat: ", CALCULATE_PLAYER_STAT_VALUE(CHAR_MULTIPLAYER, PS_STRENGTH, FALSE))
				#ENDIF
			#IF IS_DEBUG_BUILD 
			ENDIF
			#ENDIF
			
			#IF IS_DEBUG_BUILD 
			IF sDebugData.iLapDances != -1
				iTotalStats += sDebugData.iLapDances  
				PRINTLN("[AM_ARCADE_LOVE_METER] MAINTAIN_CALCULATE_STATS DEBUG dance stat: ", sDebugData.iLapDances)
			ELSE
			#ENDIF
				iTotalStats += GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].scoreData.iLapDances
				#IF IS_DEBUG_BUILD 
				PRINTLN("[AM_ARCADE_LOVE_METER] MAINTAIN_CALCULATE_STATS dance stat: ", GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].scoreData.iLapDances)
				#ENDIF	
			#IF IS_DEBUG_BUILD
			ENDIF
			#ENDIF
			
			#IF IS_DEBUG_BUILD 
			IF sDebugData.iSexActs != -1
				iTotalStats += sDebugData.iSexActs
				PRINTLN("[AM_ARCADE_LOVE_METER] MAINTAIN_CALCULATE_STATS DEBUG sex stat: ", sDebugData.iSexActs)
			ELSE
			#ENDIF
				iTotalStats += GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].scoreData.iSexActs
				#IF IS_DEBUG_BUILD 
				PRINTLN("[AM_ARCADE_LOVE_METER] MAINTAIN_CALCULATE_STATS sex stat: ", GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].scoreData.iSexActs)
				#ENDIF	
			#IF IS_DEBUG_BUILD 
			ENDIF
			#ENDIF

			playerBD[NATIVE_TO_INT(PLAYER_ID())].iTotalStats = iTotalStats
			PRINTLN("[AM_ARCADE_LOVE_METER] MAINTAIN_CALCULATE_STATS iTotalStats: ", iTotalStats)
			SET_LOVE_METER_GAME_STATE(LOVE_METER_CALCULATE_ASCII)
		ENDIF	
	ELSE
		sData.sArcadeCabinetAnimEvent.bHoldLastFrame = FALSE
		sData.sArcadeCabinetAnimEvent.bLoopAnim = FALSE
		PLAY_ARCADE_CABINET_ANIMATION(sData.sArcadeCabinetAnimEvent, ARCADE_CABINET_ANIM_CLIP_EXIT)
		SET_LOVE_METER_GAME_STATE(LOVE_METER_GAME_CLEANUP)
	ENDIF
ENDPROC

PROC MAINTAIN_CALCULATE_NAME_ASCII()
	IF IS_NET_PLAYER_OK(PLAYER_ID())
	AND IS_NET_PLAYER_OK(sData.secondPlayer)
		INT iSumNameASCII = GET_SUM_ASCII(GET_PLAYER_NAME(PLAYER_ID()))
		
		playerBD[NATIVE_TO_INT(PLAYER_ID())].iPlayerNameLength = GET_LENGTH_OF_LITERAL_STRING(GET_PLAYER_NAME(PLAYER_ID()))
		playerBD[NATIVE_TO_INT(PLAYER_ID())].iSumNameASCII = iSumNameASCII
		PRINTLN("[AM_ARCADE_LOVE_METER] MAINTAIN_CALCULATE_STATS iSumNameASCII: ", iSumNameASCII, " name length: ", playerBD[NATIVE_TO_INT(PLAYER_ID())].iPlayerNameLength)
		
		// Sum Stat and nameASCII 
		INT iTotal = playerBD[NATIVE_TO_INT(PLAYER_ID())].iSumNameASCII + playerBD[NATIVE_TO_INT(PLAYER_ID())].iTotalStats
		PRINTLN("[AM_ARCADE_LOVE_METER] MAINTAIN_CALCULATE_STATS iTotal: ", iTotal)
		
		// Conver to single digit
		WHILE iTotal > 9
			iTotal = GET_SUM_OF_INT_DIGITS(iTotal)
		ENDWHILE
		
		playerBD[NATIVE_TO_INT(PLAYER_ID())].iFirstValue = iTotal
	
		PRINTLN("[AM_ARCADE_LOVE_METER] MAINTAIN_CALCULATE_STATS iFirstValue: ", iTotal)
		
		SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iLocalBS, LOVE_METER_PLAYER_BD_FIRST_TOTAL_CALCULATED)
		SET_LOVE_METER_GAME_STATE(LOVE_METER_COMPARE_FIRST_VALUES)
	ELSE
		sData.sArcadeCabinetAnimEvent.bHoldLastFrame = FALSE
		sData.sArcadeCabinetAnimEvent.bLoopAnim = FALSE
		PLAY_ARCADE_CABINET_ANIMATION(sData.sArcadeCabinetAnimEvent, ARCADE_CABINET_ANIM_CLIP_EXIT)
		SET_LOVE_METER_GAME_STATE(LOVE_METER_GAME_CLEANUP)
	ENDIF
ENDPROC

PROC MAINTAIN_COMPARE_FIRST_VALUES()
	IF IS_NET_PLAYER_OK(PLAYER_ID())
	AND IS_NET_PLAYER_OK(sData.secondPlayer)
		IF IS_BIT_SET(playerBD[NATIVE_TO_INT(sData.secondPlayer)].iLocalBS, LOVE_METER_PLAYER_BD_FIRST_TOTAL_CALCULATED)
			sData.iTotalFirstValues = playerBD[NATIVE_TO_INT(PLAYER_ID())].iFirstValue + playerBD[NATIVE_TO_INT(sData.secondPlayer)].iFirstValue
			PRINTLN("[AM_ARCADE_LOVE_METER] MAINTAIN_COMPARE_FIRST_VALUES local first value: ", playerBD[NATIVE_TO_INT(PLAYER_ID())].iFirstValue, 
					" second player's total value: ", playerBD[NATIVE_TO_INT(sData.secondPlayer)].iFirstValue)
			
			// Convert to single digit
			WHILE sData.iTotalFirstValues > 9
				sData.iTotalFirstValues = GET_SUM_OF_INT_DIGITS(sData.iTotalFirstValues)
			ENDWHILE
			
			PRINTLN("[AM_ARCADE_LOVE_METER] MAINTAIN_COMPARE_FIRST_VALUES sData.iTotalFirstValues: ", sData.iTotalFirstValues)
			SET_LOVE_METER_GAME_STATE(LOVE_METER_CALCULATE_FINAL_VALUES)
		ELSE
			PRINTLN("[AM_ARCADE_LOVE_METER] MAINTAIN_COMPARE_FIRST_VALUES waiting for second player to get the first value")
		ENDIF
	ELSE
		sData.sArcadeCabinetAnimEvent.bHoldLastFrame = FALSE
		sData.sArcadeCabinetAnimEvent.bLoopAnim = FALSE
		PLAY_ARCADE_CABINET_ANIMATION(sData.sArcadeCabinetAnimEvent, ARCADE_CABINET_ANIM_CLIP_EXIT)
		SET_LOVE_METER_GAME_STATE(LOVE_METER_GAME_CLEANUP)
	ENDIF	
ENDPROC

/// PURPOSE:
///     calculate the final score 
PROC MAINTAIN_LOVE_METER_FINAL_VALUE()
	IF IS_NET_PLAYER_OK(PLAYER_ID())
	AND IS_NET_PLAYER_OK(sData.secondPlayer)
		IF HAS_NET_TIMER_EXPIRED(serverBD.tCalculationTimer, 5000)
			INT iExtraPoint = 0
			INT iTotalNameLength = playerBD[NATIVE_TO_INT(PLAYER_ID())].iPlayerNameLength + playerBD[NATIVE_TO_INT(sData.secondPlayer)].iPlayerNameLength
			
			IF IS_INT_EVEN(iTotalNameLength)
				iExtraPoint = 1
				PRINTLN("[AM_ARCADE_LOVE_METER] MAINTAIN_LOVE_METER_FINAL_VALUE iTotalNameLength: ", iTotalNameLength, " is even add extra point")
			ELSE
				PRINTLN("[AM_ARCADE_LOVE_METER] MAINTAIN_LOVE_METER_FINAL_VALUE iTotalNameLength: ", iTotalNameLength, " is odd don't add extra point")
			ENDIF
			
			sData.iTotalFirstValues += iExtraPoint
			WHILE sData.iTotalFirstValues > 10
				sData.iTotalFirstValues = GET_SUM_OF_INT_DIGITS(sData.iTotalFirstValues)
			ENDWHILE
						
			IF playerBD[NATIVE_TO_INT(PLAYER_ID())].iFirstValue < playerBD[NATIVE_TO_INT(sData.secondPlayer)].iFirstValue
				playerBD[NATIVE_TO_INT(PLAYER_ID())].iFinalScore = sData.iTotalFirstValues - (playerBD[NATIVE_TO_INT(sData.secondPlayer)].iFirstValue - playerBD[NATIVE_TO_INT(PLAYER_ID())].iFirstValue)
				PRINTLN("[AM_ARCADE_LOVE_METER] MAINTAIN_LOVE_METER_FINAL_VALUE first values are less than second player iExtraPoint: ", iExtraPoint, " iFinalScore: ", playerBD[NATIVE_TO_INT(PLAYER_ID())].iFinalScore)
				IF playerBD[NATIVE_TO_INT(PLAYER_ID())].iFinalScore < 0 
					playerBD[NATIVE_TO_INT(PLAYER_ID())].iFinalScore = 0
					PRINTLN("[AM_ARCADE_LOVE_METER] MAINTAIN_LOVE_METER_FINAL_VALUE first values are less than second player iFinalScore less than 0 so lets set it to 0") 
				ENDIF	
			ELSE
				playerBD[NATIVE_TO_INT(PLAYER_ID())].iFinalScore = sData.iTotalFirstValues
				PRINTLN("[AM_ARCADE_LOVE_METER] MAINTAIN_LOVE_METER_FINAL_VALUE first values are equal or more than second player iExtraPoint: ", iExtraPoint, " sData.iTotalFirstValues: ", sData.iTotalFirstValues)
			ENDIF
			
			#IF IS_DEBUG_BUILD
			IF sDebugData.iDebugFinalValue != -1	
				playerBD[NATIVE_TO_INT(PLAYER_ID())].iFinalScore = sDebugData.iDebugFinalValue
				PRINTLN("[AM_ARCADE_LOVE_METER] MAINTAIN_LOVE_METER_FINAL_VALUE DEBUG force final score: ", sDebugData.iDebugFinalValue)
			ENDIF
			#ENDIF
			
			playerBD[NATIVE_TO_INT(PLAYER_ID())].iRandomScore = GET_RANDOM_INT_IN_RANGE(0, g_sMPTunables.iCH_LOVE_METER_PERFECT_MATCH_CHANCE + 1)
			PRINTLN("[AM_ARCADE_LOVE_METER] MAINTAIN_LOVE_METER_FINAL_VALUE random score: ", playerBD[NATIVE_TO_INT(PLAYER_ID())].iRandomScore)

			SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iLocalBS, LOVE_METER_PLAYER_BD_FINAL_SCORE_CALCULATED)
			SET_LOVE_METER_GAME_STATE(LOVE_METER_PLAY_ANIM)
		ELSE
			PRINTLN("[AM_ARCADE_LOVE_METER] MAINTAIN_LOVE_METER_FINAL_VALUE waiting for server timer")
		ENDIF
	ELSE	
		sData.sArcadeCabinetAnimEvent.bHoldLastFrame = FALSE
		sData.sArcadeCabinetAnimEvent.bLoopAnim = FALSE
		PLAY_ARCADE_CABINET_ANIMATION(sData.sArcadeCabinetAnimEvent, ARCADE_CABINET_ANIM_CLIP_EXIT)
		SET_LOVE_METER_GAME_STATE(LOVE_METER_GAME_CLEANUP)
	ENDIF	
ENDPROC

PROC SEND_LOVE_METER_TELEMTRY(INT iFinalScore, INT iFinalSecondPlayerScore)
	GAMER_HANDLE gameHandler = GET_GAMER_HANDLE_PLAYER(sData.secondPlayer)
	STRUCT_ARCADE_LOVE_MATCH sStatData
	sStatData.matchId1 = serverBD.iMatchID
	sStatData.matchId2 = serverBD.iMatchID
	sStatData.score = iFinalScore
	
	sStatData.flying = CALCULATE_PLAYER_STAT_VALUE(CHAR_MULTIPLAYER, PS_FLYING_ABILITY, FALSE)
	sStatData.stamina = CALCULATE_PLAYER_STAT_VALUE(CHAR_MULTIPLAYER, PS_STAMINA, FALSE)
	sStatData.shooting = CALCULATE_PLAYER_STAT_VALUE(CHAR_MULTIPLAYER, PS_SHOOTING_ABILITY, FALSE)
	sStatData.driving = CALCULATE_PLAYER_STAT_VALUE(CHAR_MULTIPLAYER, PS_DRIVING_ABILITY, FALSE)
	sStatData.stealth = CALCULATE_PLAYER_STAT_VALUE(CHAR_MULTIPLAYER, PS_STEALTH_ABILITY, FALSE)
	sStatData.maxHealth = CALCULATE_PLAYER_STAT_VALUE(CHAR_MULTIPLAYER, PS_STRENGTH, FALSE)
	
	IF iFinalScore = iFinalSecondPlayerScore
		IF iFinalScore = 1
		OR iFinalScore = 0
			sStatData.nemesis2 = 1
		ENDIF
		IF iFinalScore = 10
			sStatData.trueLove = 1
		ENDIF
	ENDIF
	
	PRINTLN("[AM_ARCADE_LOVE_METER] - SEND_LOVE_METER_TELEMTRY match ID: ", serverBD.iMatchID, " flying: ", sStatData.flying, " stamina: ", sStatData.stamina,
			" shooting: ", sStatData.shooting, " driving: ", sStatData.driving, " stealth: ", sStatData.stealth, " maxHealth: ", sStatData.maxHealth)
	PLAYSTATS_ARCADE_LOVE_MATCH(gameHandler, sStatData)	
ENDPROC

FUNC BOOL SHOULD_FORCE_TRUE_LOVE_FOR_PLAYER()
	IF IS_NET_PLAYER_OK(PLAYER_ID())
	AND IS_NET_PLAYER_OK(sData.secondPlayer)
		IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSFive, BS_SIMPLE_INTERIOR_GLOBAL_BS_FIVE_TRUE_LOVE_FORTUNE)
		OR IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(sData.secondPlayer)].SimpleInteriorBD.iBSFive, BS_SIMPLE_INTERIOR_GLOBAL_BS_FIVE_TRUE_LOVE_FORTUNE)
			RETURN TRUE
		ENDIF
	ENDIF	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Play final animation 
PROC MAINTAIN_LOVE_METER_PLAY_ANIM()
	IF IS_NET_PLAYER_OK(PLAYER_ID())
	AND IS_NET_PLAYER_OK(sData.secondPlayer)
		IF IS_BIT_SET(playerBD[NATIVE_TO_INT(sData.secondPlayer)].iLocalBS, LOVE_METER_PLAYER_BD_FINAL_SCORE_CALCULATED)
		AND sData.eLightState > LOVE_METER_LIGHT_MATCHING_STATE
			IF SHOULD_FORCE_TRUE_LOVE_FOR_PLAYER()
				playerBD[NATIVE_TO_INT(PLAYER_ID())].iFinalScore = 10
			ENDIF
			
			INT iFinalScore = playerBD[NATIVE_TO_INT(PLAYER_ID())].iFinalScore
			INT iFinalSecondPlayerScore = playerBD[NATIVE_TO_INT(sData.secondPlayer)].iFinalScore
			
			IF SHOULD_FORCE_TRUE_LOVE_FOR_PLAYER()
				iFinalSecondPlayerScore = 10
			ENDIF
			
			PRINTLN("[AM_ARCADE_LOVE_METER] MAINTAIN_LOVE_METER_PLAY_ANIM iFinalScore: ", iFinalScore, "iFinalSecondPlayerScore: ", iFinalSecondPlayerScore
					, " force true love ", GET_STRING_FROM_BOOL(SHOULD_FORCE_TRUE_LOVE_FOR_PLAYER()))
			
			STRING strScore

			sData.sArcadeCabinetAnimEvent.bHoldLastFrame = FALSE
			sData.sArcadeCabinetAnimEvent.bLoopAnim = FALSE
			sData.sArcadeCabinetAnimEvent.ePreviousAnimClip = ARCADE_CABINET_ANIM_CLIP_INVALID
			REINIT_NET_TIMER(sData.sArcadeCabinetAnimEvent.sAnimEventSpamTimer)
			
			// Telemetry
			SEND_LOVE_METER_TELEMTRY(iFinalScore, iFinalSecondPlayerScore)
			
			INT iLoveTrophy = GET_MP_INT_CHARACTER_STAT(MP_STAT_CH_ARC_CAB_LOVE_TROPHY)
			
			IF iFinalScore = iFinalSecondPlayerScore
				SWITCH iFinalScore
					CASE 0
					CASE 1	
						IF playerBD[NATIVE_TO_INT(PLAYER_ID())].iRandomScore = playerBD[NATIVE_TO_INT(sData.secondPlayer)].iRandomScore
						#IF IS_DEBUG_BUILD
						OR sDebugData.bMatchRandomScore
						#ENDIF
							IF sData.iPlayerSlot = ENUM_TO_INT(ARCADE_CAB_PLAYER_1)
								sData.sArcadeCabinetAIContext.stContext = "LOVETEST_NEMESIS"
								PLAY_SOUND_FROM_COORD(-1, "Outcome_Nemesis", GET_ENTITY_COORDS(sData.cabinetObj), "DLC_H3_LoveMachine_Sounds", TRUE, 20)
							ENDIF
							
							sData.eAnimClip = ARCADE_CABINET_ANIM_CLIP_LOVE_METER_MUTUAL_WORSE
							strScore = "ARCCAB_LOVE_P1"			
							INCREMENT_MP_INT_CHARACTER_STAT(MP_STAT_CH_ARC_LOVE_LEVEL0)
							
							SET_BIT(iLoveTrophy, LOVE_METER_TROPHY_BS_NEMESIS_WON)
							NEXT_RP_ADDITION_SHOW_RANKBAR()
							GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD, PLAYER_PED_ID(), "LOVE METER NEMESIS RP800", XPTYPE_AWARDS, XPCATEGORY_RP_LOVE_METER, 800)
							ARCADE_CABINET_FLOW_NOTIFICATIONS_SET_SHOW_TSHIRT_HELP(ARCADE_GAME_TSHIRTS_LOVE_METER_NEMESIS)
							SET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_NEMESIS, TRUE)
							
							#IF IS_DEBUG_BUILD
							PRINTLN("[AM_ARCADE_LOVE_METER] NEMESIS sDebugData.bMatchRandomScore: ", sDebugData.bMatchRandomScore)
							#ENDIF
						ELSE
							strScore = "ARCCAB_LOVE_P2"			
							INCREMENT_MP_INT_CHARACTER_STAT(MP_STAT_CH_ARC_LOVE_LEVEL1)
							sData.eAnimClip = ARCADE_CABINET_ANIM_CLIP_LOVE_METER_MUTUAL_WORSE
							
							IF sData.iPlayerSlot = ENUM_TO_INT(ARCADE_CAB_PLAYER_1)
								sData.sArcadeCabinetAIContext.stContext = "LOVETEST_ICE_COLD"
								PLAY_SOUND_FROM_COORD(-1, "Outcome_Chillin", GET_ENTITY_COORDS(sData.cabinetObj), "DLC_H3_LoveMachine_Sounds", TRUE, 20)
							ENDIF	
						ENDIF
					BREAK
					CASE 2
						IF sData.iPlayerSlot = ENUM_TO_INT(ARCADE_CAB_PLAYER_1)
							sData.sArcadeCabinetAIContext.stContext = "LOVETEST_COLD_SHOULDER"
							PLAY_SOUND_FROM_COORD(-1, "Outcome_BuzzKill", GET_ENTITY_COORDS(sData.cabinetObj), "DLC_H3_LoveMachine_Sounds", TRUE, 20)
						ENDIF	
						strScore = "ARCCAB_LOVE_P3"
						INCREMENT_MP_INT_CHARACTER_STAT(MP_STAT_CH_ARC_LOVE_LEVEL2)
						sData.eAnimClip = ARCADE_CABINET_ANIM_CLIP_LOVE_METER_MUTUAL_WORSE
					BREAK
					CASE 3
						IF sData.iPlayerSlot = ENUM_TO_INT(ARCADE_CAB_PLAYER_1)
							sData.sArcadeCabinetAIContext.stContext = "LOVETEST_THAW_OUT"
							PLAY_SOUND_FROM_COORD(-1, "Outcome_Not", GET_ENTITY_COORDS(sData.cabinetObj), "DLC_H3_LoveMachine_Sounds", TRUE, 20)
						ENDIF	
						strScore = "ARCCAB_LOVE_P4"
						INCREMENT_MP_INT_CHARACTER_STAT(MP_STAT_CH_ARC_LOVE_LEVEL3)
						sData.eAnimClip =  ARCADE_CABINET_ANIM_CLIP_LOVE_METER_MUTUAL_BAD
					BREAK
					CASE 4
						IF sData.iPlayerSlot = ENUM_TO_INT(ARCADE_CAB_PLAYER_1)
							sData.sArcadeCabinetAIContext.stContext = "LOVETEST_CLAMMY"
							PLAY_SOUND_FROM_COORD(-1, "Outcome_Schwing", GET_ENTITY_COORDS(sData.cabinetObj), "DLC_H3_LoveMachine_Sounds", TRUE, 20)
						ENDIF
						sData.eAnimClip =  ARCADE_CABINET_ANIM_CLIP_LOVE_METER_MUTUAL_BAD	
						strScore = "ARCCAB_LOVE_P5"
						INCREMENT_MP_INT_CHARACTER_STAT(MP_STAT_CH_ARC_LOVE_LEVEL4)
					BREAK
					CASE 5
						IF sData.iPlayerSlot = ENUM_TO_INT(ARCADE_CAB_PLAYER_1)
							sData.sArcadeCabinetAIContext.stContext = "LOVETEST_WARMER"
							PLAY_SOUND_FROM_COORD(-1, "Outcome_Pimpin", GET_ENTITY_COORDS(sData.cabinetObj), "DLC_H3_LoveMachine_Sounds", TRUE, 20)
						ENDIF
						sData.eAnimClip = ARCADE_CABINET_ANIM_CLIP_LOVE_METER_MUTUAL_AVERAGE	
						strScore = "ARCCAB_LOVE_P6"
						INCREMENT_MP_INT_CHARACTER_STAT(MP_STAT_CH_ARC_LOVE_LEVEL5)
					BREAK
					CASE 6
						IF sData.iPlayerSlot = ENUM_TO_INT(ARCADE_CAB_PLAYER_1)
							sData.sArcadeCabinetAIContext.stContext = "LOVETEST_FLUSHED"
							PLAY_SOUND_FROM_COORD(-1, "Outcome_Hype", GET_ENTITY_COORDS(sData.cabinetObj), "DLC_H3_LoveMachine_Sounds", TRUE, 20)
						ENDIF	
						sData.eAnimClip = ARCADE_CABINET_ANIM_CLIP_LOVE_METER_MUTUAL_AVERAGE
						strScore = "ARCCAB_LOVE_P7"
						INCREMENT_MP_INT_CHARACTER_STAT(MP_STAT_CH_ARC_LOVE_LEVEL6)
					BREAK
					CASE 7
						IF sData.iPlayerSlot = ENUM_TO_INT(ARCADE_CAB_PLAYER_1)
							sData.sArcadeCabinetAIContext.stContext = "LOVETEST_GETTING_STEAMY"
							PLAY_SOUND_FROM_COORD(-1, "Outcome_Sweet", GET_ENTITY_COORDS(sData.cabinetObj), "DLC_H3_LoveMachine_Sounds", TRUE, 20)
						ENDIF
						sData.eAnimClip = ARCADE_CABINET_ANIM_CLIP_LOVE_METER_MUTUAL_GOOD
						strScore = "ARCCAB_LOVE_P8"
						INCREMENT_MP_INT_CHARACTER_STAT(MP_STAT_CH_ARC_LOVE_LEVEL7)
					BREAK
					CASE 8
						IF sData.iPlayerSlot = ENUM_TO_INT(ARCADE_CAB_PLAYER_1)
							sData.sArcadeCabinetAIContext.stContext = "LOVETEST_BURNING_LOINS"
							PLAY_SOUND_FROM_COORD(-1, "Outcome_LoveSick", GET_ENTITY_COORDS(sData.cabinetObj), "DLC_H3_LoveMachine_Sounds", TRUE, 20)
						ENDIF
						sData.eAnimClip = ARCADE_CABINET_ANIM_CLIP_LOVE_METER_MUTUAL_GOOD	
						strScore = "ARCCAB_LOVE_P9"
						INCREMENT_MP_INT_CHARACTER_STAT(MP_STAT_CH_ARC_LOVE_LEVEL8)
					BREAK
					CASE 9
						IF sData.iPlayerSlot = ENUM_TO_INT(ARCADE_CAB_PLAYER_1)
							sData.sArcadeCabinetAIContext.stContext = "LOVETEST_HOT_N_HEAVY"
							PLAY_SOUND_FROM_COORD(-1, "Outcome_SoFine", GET_ENTITY_COORDS(sData.cabinetObj), "DLC_H3_LoveMachine_Sounds", TRUE, 20)
						ENDIF	
						sData.eAnimClip = ARCADE_CABINET_ANIM_CLIP_LOVE_METER_MUTUAL_PERFECT
						strScore = "ARCCAB_LOVE_P10"
						INCREMENT_MP_INT_CHARACTER_STAT(MP_STAT_CH_ARC_LOVE_LEVEL9)						
					BREAK
					CASE 10
						IF playerBD[NATIVE_TO_INT(PLAYER_ID())].iRandomScore = playerBD[NATIVE_TO_INT(sData.secondPlayer)].iRandomScore
						OR SHOULD_FORCE_TRUE_LOVE_FOR_PLAYER()
						#IF IS_DEBUG_BUILD
						OR sDebugData.bMatchRandomScore
						#ENDIF
							sData.eAnimClip = ARCADE_CABINET_ANIM_CLIP_LOVE_METER_MUTUAL_PERFECT
							IF sData.iPlayerSlot = ENUM_TO_INT(ARCADE_CAB_PLAYER_1)
								sData.sArcadeCabinetAIContext.stContext = "LOVETEST_TRUELOVE"
								PLAY_SOUND_FROM_COORD(-1, "Outcome_PerfectMatch", GET_ENTITY_COORDS(sData.cabinetObj), "DLC_H3_LoveMachine_Sounds", TRUE, 20)
							ENDIF
							
							SET_BIT(iLoveTrophy, LOVE_METER_TROPHY_BS_TRUE_LOVE_WON)
							NEXT_RP_ADDITION_SHOW_RANKBAR()
							GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD, PLAYER_PED_ID(), "LOVE METER TRUELOVE RP800", XPTYPE_AWARDS, XPCATEGORY_RP_LOVE_METER, 800)
							
							IF IS_PLAYER_PED_FEMALE(sData.secondPlayer)
								ARCADE_CABINET_FLOW_NOTIFICATIONS_SET_SHOW_TSHIRT_HELP(ARCADE_GAME_TSHIRTS_LOVE_METER_TRUE_LOVE_HER)
							ELSE
								ARCADE_CABINET_FLOW_NOTIFICATIONS_SET_SHOW_TSHIRT_HELP(ARCADE_GAME_TSHIRTS_LOVE_METER_TRUE_LOVE_HIS)
							ENDIF	
							SET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_TRUELOVE, TRUE)
							
							strScore = "ARCCAB_LOVE_P12"
							INCREMENT_MP_INT_CHARACTER_STAT(MP_STAT_CH_ARC_LOVE_LEVEL11)
							
							#IF IS_DEBUG_BUILD
							PRINTLN("[AM_ARCADE_LOVE_METER] PERFECT MATCH sDebugData.bMatchRandomScore: ", sDebugData.bMatchRandomScore)
							#ENDIF
						ELSE
							sData.eAnimClip = ARCADE_CABINET_ANIM_CLIP_LOVE_METER_MUTUAL_PERFECT
							IF sData.iPlayerSlot = ENUM_TO_INT(ARCADE_CAB_PLAYER_1)
								sData.sArcadeCabinetAIContext.stContext = "LOVETEST_SIZZLIN"
								PLAY_SOUND_FROM_COORD(-1, "Outcome_PerfectMatch", GET_ENTITY_COORDS(sData.cabinetObj), "DLC_H3_LoveMachine_Sounds", TRUE, 20)
							ENDIF
							strScore = "ARCCAB_LOVE_P11"
							INCREMENT_MP_INT_CHARACTER_STAT(MP_STAT_CH_ARC_LOVE_LEVEL10)
						ENDIF	
					BREAK
				ENDSWITCH
			ELSE
				
				INT iDiff = ABSI(iFinalScore - iFinalSecondPlayerScore)
				PRINTLN("[AM_ARCADE_LOVE_METER] MAINTAIN_LOVE_METER_PLAY_ANIM iDiff", iDiff)
				
				IF sData.iPlayerSlot = ENUM_TO_INT(ARCADE_CAB_PLAYER_1)		
					IF iDiff <= 2
						sData.sArcadeCabinetAIContext.stContext = "LOVETEST_SLIGHT"
					ELIF iDiff >= g_ArcadeCabinetManagerData.iLoveMeterFriendZoneDiff 
						sData.sArcadeCabinetAIContext.stContext = "LOVETEST_FRIENDZONE"
					ELSE	
						sData.sArcadeCabinetAIContext.stContext = "LOVETEST_UNEVEN"
					ENDIF	
					PLAY_ARCADE_CABINET_AI_CONTEXT(sData.sArcadeCabinetAIContext, TRUE)
					PLAY_SOUND_FROM_COORD(-1, "Outcome_BuzzKill", GET_ENTITY_COORDS(sData.cabinetObj), "DLC_H3_LoveMachine_Sounds", TRUE, 20)
				ENDIF
				
				IF iDiff >= g_ArcadeCabinetManagerData.iLoveMeterFriendZoneDiff  
					SET_BIT(iLoveTrophy, LOVE_METER_TROPHY_BS_FRIENDZONED_WON)
					NEXT_RP_ADDITION_SHOW_RANKBAR()
					ARCADE_CABINET_FLOW_NOTIFICATIONS_SET_SHOW_TSHIRT_HELP(ARCADE_GAME_TSHIRTS_LOVE_METER_FRIENDZONE)
					SET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_FRIENDZONED, TRUE)
					GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD, PLAYER_PED_ID(), "LOVE METER FRIENDZ RP800", XPTYPE_AWARDS, XPCATEGORY_RP_LOVE_METER, 800)
					PRINTLN("[AM_ARCADE_LOVE_METER] MAINTAIN_LOVE_METER_PLAY_ANIM LOVE_METER_TROPHY_BS_FRIENDZONED_WON TRUE")
				ENDIF	
			
				SWITCH iFinalScore
					CASE 0
					CASE 1
						SWITCH iFinalSecondPlayerScore 
							CASE 10
							CASE 9
								sData.eAnimClip = ARCADE_CABINET_ANIM_CLIP_LOVE_METER_FRIENDZONE_WORSE
							BREAK
							DEFAULT	
								sData.eAnimClip = ARCADE_CABINET_ANIM_CLIP_LOVE_METER_WORSE_WORSE
							BREAK
						ENDSWITCH
						
						strScore = "ARCCAB_LOVE_P2"
						INCREMENT_MP_INT_CHARACTER_STAT(MP_STAT_CH_ARC_LOVE_LEVEL1)
					BREAK	
					CASE 2
						SWITCH iFinalSecondPlayerScore
							CASE 0
							CASE 1
								sData.eAnimClip = ARCADE_CABINET_ANIM_CLIP_LOVE_METER_BAD_BETTER
							BREAK
							DEFAULT
								sData.eAnimClip = ARCADE_CABINET_ANIM_CLIP_LOVE_METER_WORSE_WORSE
							BREAK
						ENDSWITCH	
						
						strScore = "ARCCAB_LOVE_P3"
						INCREMENT_MP_INT_CHARACTER_STAT(MP_STAT_CH_ARC_LOVE_LEVEL2)
					BREAK
					CASE 3
						SWITCH iFinalSecondPlayerScore
							CASE 0
							CASE 1
							CASE 2
								sData.eAnimClip = ARCADE_CABINET_ANIM_CLIP_LOVE_METER_BAD_BETTER
							BREAK
							DEFAULT
								sData.eAnimClip = ARCADE_CABINET_ANIM_CLIP_LOVE_METER_BAD_WORSE
							BREAK
						ENDSWITCH	
						
						strScore = "ARCCAB_LOVE_P4"
						INCREMENT_MP_INT_CHARACTER_STAT(MP_STAT_CH_ARC_LOVE_LEVEL3)
					BREAK
					CASE 4
						SWITCH iFinalSecondPlayerScore
							CASE 0
							CASE 1
							CASE 2
							CASE 3
								sData.eAnimClip = ARCADE_CABINET_ANIM_CLIP_LOVE_METER_BAD_BETTER
							BREAK
							DEFAULT
								sData.eAnimClip = ARCADE_CABINET_ANIM_CLIP_LOVE_METER_BAD_WORSE
							BREAK
						ENDSWITCH
						
						strScore = "ARCCAB_LOVE_P5"
						INCREMENT_MP_INT_CHARACTER_STAT(MP_STAT_CH_ARC_LOVE_LEVEL4)
					BREAK
					CASE 5
						SWITCH iFinalSecondPlayerScore
							CASE 0
							CASE 1
							CASE 2
							CASE 3
							CASE 4
								sData.eAnimClip = ARCADE_CABINET_ANIM_CLIP_LOVE_METER_AVERAGE_BETTER
							BREAK
							DEFAULT
								sData.eAnimClip = ARCADE_CABINET_ANIM_CLIP_LOVE_METER_AVERAGE_WORSE
							BREAK
						ENDSWITCH	
						
						strScore = "ARCCAB_LOVE_P6"
						INCREMENT_MP_INT_CHARACTER_STAT(MP_STAT_CH_ARC_LOVE_LEVEL5)
					BREAK
					CASE 6
						SWITCH iFinalSecondPlayerScore
							CASE 0
							CASE 1
							CASE 2
							CASE 3
							CASE 4
							CASE 5
								sData.eAnimClip = ARCADE_CABINET_ANIM_CLIP_LOVE_METER_AVERAGE_BETTER
							BREAK
							DEFAULT
								sData.eAnimClip = ARCADE_CABINET_ANIM_CLIP_LOVE_METER_AVERAGE_WORSE
							BREAK
						ENDSWITCH
						
						strScore = "ARCCAB_LOVE_P7"
						INCREMENT_MP_INT_CHARACTER_STAT(MP_STAT_CH_ARC_LOVE_LEVEL6)
					BREAK
					CASE 7
						SWITCH iFinalSecondPlayerScore
							CASE 0
							CASE 1
							CASE 2
							CASE 3
							CASE 4
							CASE 5
							CASE 6							
								sData.eAnimClip = ARCADE_CABINET_ANIM_CLIP_LOVE_METER_GOOD_BETTER
							BREAK
							DEFAULT
								sData.eAnimClip = ARCADE_CABINET_ANIM_CLIP_LOVE_METER_GOOD_WORSE
							BREAK
						ENDSWITCH	
						
						strScore = "ARCCAB_LOVE_P8"
						INCREMENT_MP_INT_CHARACTER_STAT(MP_STAT_CH_ARC_LOVE_LEVEL7)
					BREAK
					CASE 8
						SWITCH iFinalSecondPlayerScore
							CASE 9
							CASE 10
								sData.eAnimClip = ARCADE_CABINET_ANIM_CLIP_LOVE_METER_GOOD_WORSE
								
							BREAK
							DEFAULT
								sData.eAnimClip = ARCADE_CABINET_ANIM_CLIP_LOVE_METER_GOOD_BETTER
							BREAK
						ENDSWITCH	
						
						strScore = "ARCCAB_LOVE_P9"
						INCREMENT_MP_INT_CHARACTER_STAT(MP_STAT_CH_ARC_LOVE_LEVEL8)
					BREAK
					CASE 9
						SWITCH iFinalSecondPlayerScore
							CASE 10
								sData.eAnimClip = ARCADE_CABINET_ANIM_CLIP_LOVE_METER_GOOD_WORSE
							BREAK
							DEFAULT
								sData.eAnimClip = ARCADE_CABINET_ANIM_CLIP_LOVE_METER_PERFECT_BETTER
							BREAK
						ENDSWITCH	
						
						strScore = "ARCCAB_LOVE_P10"
						INCREMENT_MP_INT_CHARACTER_STAT(MP_STAT_CH_ARC_LOVE_LEVEL9)
					BREAK
					CASE 10
						SWITCH iFinalSecondPlayerScore
							CASE 0
							CASE 1
							CASE 2
								sData.eAnimClip = ARCADE_CABINET_ANIM_CLIP_LOVE_METER_FRIENDZONE_PERFECT
							BREAK
							DEFAULT
								sData.eAnimClip = ARCADE_CABINET_ANIM_CLIP_LOVE_METER_PERFECT_BETTER
							BREAK
						ENDSWITCH	
						
						strScore = "ARCCAB_LOVE_P11"
						INCREMENT_MP_INT_CHARACTER_STAT(MP_STAT_CH_ARC_LOVE_LEVEL10)
					BREAK
				ENDSWITCH
			ENDIF
			
			IF sData.iPlayerSlot = ENUM_TO_INT(ARCADE_CAB_PLAYER_1)		
				PLAY_ARCADE_CABINET_AI_CONTEXT(sData.sArcadeCabinetAIContext, TRUE)
			ENDIF
			
			IF IS_BIT_SET(iLoveTrophy, LOVE_METER_TROPHY_BS_TRUE_LOVE_WON)
			AND IS_BIT_SET(iLoveTrophy, LOVE_METER_TROPHY_BS_NEMESIS_WON)
			AND IS_BIT_SET(iLoveTrophy, LOVE_METER_TROPHY_BS_FRIENDZONED_WON)
				IF NOT IS_BIT_SET(iLoveTrophy, LOVE_METER_TROPHY_BS_WON_ALL)
					SET_BIT(iLoveTrophy, LOVE_METER_TROPHY_BS_WON_ALL)
					SET_ARCADE_LOVE_METER_TROPHY_COMPLETE(TRUE)
					ARCADE_CABINET_FLOW_NOTIFICATIONS_SET_SHOW_TSHIRT_HELP(ARCADE_GAME_TSHIRTS_LOVE_METER_UNLOCK_ALL_SECRETS)
					ARCADE_CABINET_FLOW_NOTIFICATIONS_SET_SHOW_TROPHY_HELP(ARCADE_GAME_TROPHY_LOVE_METER, DOES_PLAYER_OWN_A_CASINO_APARTMENT(PLAYER_ID()))
					PRINTLN("[AM_ARCADE_LOVE_METER] MAINTAIN_LOVE_METER_PLAY_ANIM LOVE_METER_TROPHY_BS_WON_ALL TRUE")
				ELSE
					PRINTLN("[AM_ARCADE_LOVE_METER] MAINTAIN_LOVE_METER_PLAY_ANIM LOVE_METER_TROPHY_BS_WON_ALL TRUE we already won this trophy")
				ENDIF
			ENDIF	
			
			PRINTLN("[AM_ARCADE_LOVE_METER] MAINTAIN_LOVE_METER_PLAY_ANIM strScore: ", strScore)
			PRINT_TICKER_WITH_TWO_PLAYER_NAMES_AND_TEXT_LABEL("ARCCAB_LOVE_T1", PLAYER_ID(), sData.secondPlayer, strScore)
			
			SET_MP_INT_CHARACTER_STAT(MP_STAT_CH_ARC_CAB_LOVE_TROPHY, iLoveTrophy)
			PLAY_ARCADE_CABINET_ANIMATION(sData.sArcadeCabinetAnimEvent, sData.eAnimClip)
			SET_LOVE_METER_GAME_STATE(LOVE_METER_CLEANUP_ANIM)
			SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iLocalBS, LOVE_METER_PLAYER_BD_FINAL_ANIM_STARTED)
		ELSE
			PRINTLN("[AM_ARCADE_LOVE_METER] MAINTAIN_LOVE_METER_PLAY_ANIM waiting for second player")
		ENDIF
	ELSE
		sData.sArcadeCabinetAnimEvent.bHoldLastFrame = FALSE
		sData.sArcadeCabinetAnimEvent.bLoopAnim = FALSE
		PLAY_ARCADE_CABINET_ANIMATION(sData.sArcadeCabinetAnimEvent, ARCADE_CABINET_ANIM_CLIP_EXIT)
		SET_LOVE_METER_GAME_STATE(LOVE_METER_GAME_CLEANUP)
	ENDIF	
ENDPROC

PROC MAINTAIN_LOVE_METER_GAME_CLEANUP()
	#IF IS_DEBUG_BUILD
	sDebugData.iDebugFinalValue = -1	
	sDebugData.bDisplayScores = FALSE
	#ENDIF
	
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("ARC_CAB_LOVE_WAIT")
		CLEAR_HELP()
	ENDIF
	
	IF HAS_SOUND_FINISHED(sData.iCalSoundID)
		STOP_SOUND(sData.iCalSoundID)
		RELEASE_SOUND_ID(sData.iCalSoundID)
	ENDIF
	
	sData.iCalSoundID = -1
	
	RESET_NET_TIMER(sData.tHypeAIContextTimer)
	RESET_NET_TIMER(sData.tNoPartnerContextTimer)
	
	// Reset anim event struct
	ARCADE_CABINET_ANIM_EVENT_STRUCT cabinetAnimEvent
	sData.sArcadeCabinetAnimEvent = cabinetAnimEvent
	
	playerBD[NATIVE_TO_INT(PLAYER_ID())].iLocalBS = 0
	sData.iBS = 0
	sData.secondPlayer = INVALID_PLAYER_INDEX()
	
	IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSFive, BS_SIMPLE_INTERIOR_GLOBAL_BS_FIVE_TRUE_LOVE_FORTUNE)
		CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSFive, BS_SIMPLE_INTERIOR_GLOBAL_BS_FIVE_TRUE_LOVE_FORTUNE)
		PRINTLN("[AM_ARCADE_LOVE_METER] MAINTAIN_LOVE_METER_GAME_CLEANUP BS_SIMPLE_INTERIOR_GLOBAL_BS_FIVE_TRUE_LOVE_FORTUNE false")
	ENDIF	
	
	CLEAR_BIT(globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].sArcadeManagerGlobalPlayerBD.iArcadeCabinetBS, ARCADE_CABINET_GLOBAL_BD_PLAYER_SWITCHED_LOVE_DIC)
	
	sData.sInitialAnimDic = ""
	SET_PLAYER_USING_ARCADE_CABINET(FALSE)
	NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	SET_ARCADE_CABINET_CHILD_SCRIPT_CLIENT_STATE(sData.eState, ARCADE_CABINET_STATE_CLEANUP)
	SET_LOVE_METER_GAME_STATE(LOVE_METER_WAIT_FOR_OTHER_PLAYER_STATE)
ENDPROC

PROC MAINTAIN_CLEANUP_ANIM()
	IF IS_NET_PLAYER_OK(PLAYER_ID())
	AND IS_NET_PLAYER_OK(sData.secondPlayer)
		IF IS_BIT_SET(playerBD[NATIVE_TO_INT(sData.secondPlayer)].iLocalBS, LOVE_METER_PLAYER_BD_FINAL_ANIM_STARTED)
		AND !IS_BIT_SET(serverBD.iServerBS, LOVE_METER_SERVER_BD_CLEANUP_PLAYERS_GAME_STATE)
			IF HAS_ARCADE_CABINET_ANIMATION_CLIP_FINISHED(sData.eAnimClip)
				IF NOT IS_BIT_SET(playerBD[NATIVE_TO_INT(PLAYER_ID())].iLocalBS, LOVE_METER_PLAYER_BD_FINAL_ANIM_ENDED)
					SET_BIT(playerBD[NATIVE_TO_INT(PLAYER_ID())].iLocalBS, LOVE_METER_PLAYER_BD_FINAL_ANIM_ENDED)
					PRINTLN("[AM_ARCADE_LOVE_METER] MAINTAIN_CLEANUP_ANIM set LOVE_METER_PLAYER_BD_FINAL_ANIM_ENDED")
				ENDIF	
			ELSE
				PRINTLN("[AM_ARCADE_LOVE_METER] MAINTAIN_CLEANUP_ANIM waiting for HAS_ARCADE_CABINET_ANIMATION_CLIP_FINISHED")
			ENDIF
		ELIF IS_BIT_SET(serverBD.iServerBS, LOVE_METER_SERVER_BD_CLEANUP_PLAYERS_GAME_STATE)
			SET_LOVE_METER_GAME_STATE(LOVE_METER_GAME_CLEANUP)
		ELSE
			PRINTLN("[AM_ARCADE_LOVE_METER] MAINTAIN_CLEANUP_ANIM waiting for other player")
		ENDIF
	ELSE
		PRINTLN("[AM_ARCADE_LOVE_METER] MAINTAIN_CLEANUP_ANIM player not valid")
		SET_LOVE_METER_GAME_STATE(LOVE_METER_GAME_CLEANUP)
	ENDIF	
ENDPROC

PROC MAINTAIN_LOVE_METER_HYPE_AI_CONTEXT()
	IF sData.eGameState > LOVE_METER_WAIT_FOR_OTHER_PLAYER_STATE
	AND sData.eGameState < LOVE_METER_CALCULATE_FINAL_VALUES
		IF NOT HAS_NET_TIMER_STARTED(sData.tHypeAIContextTimer)
			sData.sArcadeCabinetAIContext.stContext = "LOVETEST_HYPE"
			PLAY_ARCADE_CABINET_AI_CONTEXT(sData.sArcadeCabinetAIContext, TRUE)			
			IF sData.sArcadeCabinetAIContext.bEventSent
				START_NET_TIMER(sData.tHypeAIContextTimer)
			ENDIF
		ELSE
			IF HAS_NET_TIMER_EXPIRED(sData.tHypeAIContextTimer, 20000)
				RESET_NET_TIMER(sData.tHypeAIContextTimer)
			ENDIF
		ENDIF
	ENDIF	
ENDPROC

PROC MAINTAIN_LOVE_METER_IDLE_STATE()
	SWITCH sData.eGameState
		CASE LOVE_METER_WAIT_FOR_OTHER_PLAYER_STATE
			MAINTAIN_LOVE_METER_WAIT_FOR_OTHER_PLAYER_STATE()
		BREAK
		CASE LOVE_METER_CALCULATE_TOTAL_STATS
			MAINTAIN_CALCULATE_STATS()
		BREAK
		CASE LOVE_METER_CALCULATE_ASCII
			MAINTAIN_CALCULATE_NAME_ASCII()
		BREAK
		CASE LOVE_METER_COMPARE_FIRST_VALUES
			MAINTAIN_COMPARE_FIRST_VALUES()
		BREAK
		CASE LOVE_METER_CALCULATE_FINAL_VALUES
			MAINTAIN_LOVE_METER_FINAL_VALUE()
		BREAK
		CASE LOVE_METER_PLAY_ANIM
			MAINTAIN_LOVE_METER_PLAY_ANIM()
		BREAK
		CASE LOVE_METER_CLEANUP_ANIM
			MAINTAIN_CLEANUP_ANIM()
		BREAK
		CASE LOVE_METER_GAME_CLEANUP
			MAINTAIN_LOVE_METER_GAME_CLEANUP()
		BREAK
	ENDSWITCH 
	
	MAINTAIN_LOVE_METER_HYPE_AI_CONTEXT()
ENDPROC

PROC MAINTAIN_LOVE_METER_CLEANUP_STATE()
	SET_ARCADE_CABINET_CHILD_SCRIPT_CLIENT_STATE(sData.eState, ARCADE_CABINET_STATE_LOADING)
ENDPROC

PROC GET_LOVE_METER_LIGHT_LOCATION(INT iLight, VECTOR &vLoc,VECTOR &vRot)
	IF iLight >= 10
		vLoc = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sData.cabinetObj,<<0.728,0,0>>)
	ELSE
		vLoc = GET_ENTITY_COORDS(sData.cabinetObj)
	ENDIF
	vRot = GET_ENTITY_ROTATION(sData.cabinetObj)
ENDPROC

FUNC MODEL_NAMES GET_LOVE_METER_LIGHT_MODEL(INT iLight)
	SWITCH iLight
		CASE 0
		CASE 10
			RETURN INT_TO_ENUM(MODEL_NAMES,HASH("ch_Prop_Arc_Love_Btn_Ice"))	
		BREAK
		CASE 1
		CASE 11
			RETURN INT_TO_ENUM(MODEL_NAMES,HASH("ch_Prop_Arc_Love_Btn_Cold"))
		BREAK
		CASE 2
		CASE 12
			RETURN INT_TO_ENUM(MODEL_NAMES,HASH("ch_Prop_Arc_Love_Btn_Thaw"))
		BREAK
		CASE 3
		CASE 13
			RETURN INT_TO_ENUM(MODEL_NAMES,HASH("ch_Prop_Arc_Love_Btn_Clam"))
		BREAK
		CASE 4
		CASE 14
			RETURN INT_TO_ENUM(MODEL_NAMES,HASH("ch_Prop_Arc_Love_Btn_Warm"))
		BREAK
		CASE 5
		CASE 15
			RETURN INT_TO_ENUM(MODEL_NAMES,HASH("ch_Prop_Arc_Love_Btn_Flush"))
		BREAK
		CASE 6
		CASE 16
			RETURN INT_TO_ENUM(MODEL_NAMES,HASH("ch_Prop_Arc_Love_Btn_Gett"))
		BREAK
		CASE 7
		CASE 17
			RETURN INT_TO_ENUM(MODEL_NAMES,HASH("ch_Prop_Arc_Love_Btn_Burn"))
		BREAK
		CASE 8
		CASE 18
			RETURN INT_TO_ENUM(MODEL_NAMES,HASH("ch_Prop_Arc_Love_Btn_Hot"))
		BREAK
		CASE 9
		CASE 19
			RETURN INT_TO_ENUM(MODEL_NAMES,HASH("ch_Prop_Arc_Love_Btn_Sizz"))
		BREAK
	ENDSWITCH
	RETURN INT_TO_ENUM(MODEL_NAMES,HASH("ch_Prop_Arc_Love_Btn_Ice"))
ENDFUNC

FUNC FLOAT GET_LOVE_METER_POSITION_VALUE(INT iPos)
	SWITCH iPos
		CASE 0 RETURN LOVE_METER_BAR_Y_0
		CASE 1 RETURN LOVE_METER_BAR_Y_1
		CASE 2 RETURN LOVE_METER_BAR_Y_2
		CASE 3 RETURN LOVE_METER_BAR_Y_3
		CASE 4 RETURN LOVE_METER_BAR_Y_4
		CASE 5 RETURN LOVE_METER_BAR_Y_5
		CASE 6 RETURN LOVE_METER_BAR_Y_6
		CASE 7 RETURN LOVE_METER_BAR_Y_7
		CASE 8 RETURN LOVE_METER_BAR_Y_8
		CASE 9 RETURN LOVE_METER_BAR_Y_9
		CASE 10 RETURN LOVE_METER_BAR_Y_10
		CASE 11 RETURN LOVE_METER_BAR_Y_11
	ENDSWITCH
	RETURN LOVE_METER_BAR_Y_0
ENDFUNC

FUNC FLOAT GET_LOVE_METER_POSITION_AVERAGE_POSITION(INT iScore1,INT iScore2)
	FLOAT fDif
	FLOAT fLoveMeterPos = GET_LOVE_METER_POSITION_VALUE(iScore1)
	FLOAT fLoveMeterPos2 = GET_LOVE_METER_POSITION_VALUE(iScore2)

	fDif = (fLoveMeterPos-fLoveMeterPos2)/2
	
	//IF fDif < 0
		RETURN fLoveMeterPos-fDif
	//ELSE
	//	RETURN fLoveMeterPos+fDif
	//ENDIF

	RETURN fLoveMeterPos
ENDFUNC

PROC SET_LOVE_METER_LIGHT_PATTERN(LOVE_METER_PATTERNS thePattern)
	IF ENUM_TO_INT(thePattern) != sData.lights.iPatternID
		IF HAS_NET_TIMER_STARTED(sData.lights.patternTimer)
			RESET_NET_TIMER(sData.lights.patternTimer)
		ENDIF
		sData.lights.iPatternID = ENUM_TO_INT(thePattern)
		sData.lights.iMeterColours[0] = 247
		sData.lights.iMeterColours[1] = 112
		sData.lights.iMeterColours[2] = 164
		sData.lights.iPatternLightID = 0
		sData.lights.fMeterRectLoc = LOVE_METER_BAR_Y_0
		sData.lights.iMeterAlpha = 255
		
		sData.lights.iLoopsComplete = 0
		sData.lights.bPatternFinished = FALSE
		SWITCH thePattern
			CASE PATTERN_CYCLE
				sData.lights.iPatternLightID = -1
			BREAK
			CASE PATTERN_END_MATCH
			CASE PATTERN_END_LOVE
				#IF IS_DEBUG_BUILD
				IF sDebugData.db_bVisTest
				OR sDebugData.idb_LoveMeterPattern != -1
					sData.lights.fMeterRectLoc = GET_LOVE_METER_POSITION_AVERAGE_POSITION(sDebugData.db_Score[0],sDebugData.db_Score[1])
				ELSE
				#ENDIF
					sData.lights.fMeterRectLoc = GET_LOVE_METER_POSITION_AVERAGE_POSITION(sData.lights.playerScore[ARCADE_CAB_PLAYER_1],sData.lights.playerScore[ARCADE_CAB_PLAYER_2])
				#IF IS_DEBUG_BUILD
				ENDIF
				#ENDIF
			BREAK
			CASE PATTERN_END_NEMESIS
				sData.lights.fMeterRectLoc = LOVE_METER_BAR_Y_11
			BREAK
		ENDSWITCH

		INT i
		REPEAT 20 i
			IF DOES_ENTITY_EXIST(sData.lights.Objs[i])
				SET_ENTITY_VISIBLE(sData.lights.Objs[i],TRUE)
			ENDIF
		ENDREPEAT
		PRINTLN("SET_LOVE_METER_LIGHT_PATTERN: setting pattern to be: ",thePattern)
		PRINTLN("SET_LOVE_METER_LIGHT_PATTERN: setting sData.lights.fMeterRectLoc to be: ",sData.lights.fMeterRectLoc)
	ENDIF
ENDPROC

PROC SET_LOVE_MACHINE_SCORES(INT iScoreLeft,INT iScoreRight)
	sData.lights.playerScore[ARCADE_CAB_PLAYER_1] = iScoreLeft
	sData.lights.playerScore[ARCADE_CAB_PLAYER_2] = iScoreRight
	
	PRINTLN("SET_LOVE_MACHINE_SCORES: sData.lights.playerScore[ARCADE_CAB_PLAYER_1] = ",sData.lights.playerScore[ARCADE_CAB_PLAYER_1])
	PRINTLN("SET_LOVE_MACHINE_SCORES: sData.lights.playerScore[ARCADE_CAB_PLAYER_2] = ",sData.lights.playerScore[ARCADE_CAB_PLAYER_2])
ENDPROC

PROC MAINTAIN_LOVE_METER_RENDER_TARGET()
	IF NOT sData.lights.bRegisteredRenderTarget
		IF NOT IS_NAMED_RENDERTARGET_REGISTERED("Arc_Love_01a")
			REGISTER_NAMED_RENDERTARGET("Arc_Love_01a")
			IF NOT IS_NAMED_RENDERTARGET_LINKED(GET_ENTITY_MODEL(sData.cabinetObj))
				LINK_NAMED_RENDERTARGET(GET_ENTITY_MODEL(sData.cabinetObj))
				IF sData.lights.iRenderTargetID = -1
					sData.lights.iRenderTargetID = GET_NAMED_RENDERTARGET_RENDER_ID("Arc_Love_01a")
					PRINTLN("MAINTAIN_LOVE_METER_RENDER_TARGET: 1 Setting RT:", sData.lights.iRenderTargetID)
				ENDIF
				sData.lights.bRegisteredRenderTarget = TRUE
				PRINTLN("MAINTAIN_LOVE_METER_RENDER_TARGET: 1 linked and registered Arc_Love_01a")
			ENDIF
		ELSE
			IF NOT IS_NAMED_RENDERTARGET_LINKED(GET_ENTITY_MODEL(sData.cabinetObj))
				LINK_NAMED_RENDERTARGET(GET_ENTITY_MODEL(sData.cabinetObj))
				IF sData.lights.iRenderTargetID= -1
					sData.lights.iRenderTargetID = GET_NAMED_RENDERTARGET_RENDER_ID("Arc_Love_01a")
					PRINTLN("MAINTAIN_LOVE_METER_RENDER_TARGET: 2 Setting RT:", sData.lights.iRenderTargetID)
				ENDIF
				sData.lights.bRegisteredRenderTarget = TRUE
				PRINTLN("MAINTAIN_LOVE_METER_RENDER_TARGET: 2 linked and registered Arc_Love_01a")
			ENDIF
		ENDIF
	ELSE
		IF sData.lights.iRenderTargetID != -1
			//PRINTLN("MAINTAIN_LOVE_METER_RENDER_TARGET: sData.lights.iRenderTargetID = ",sData.lights.iRenderTargetID)
			SET_TEXT_RENDER_ID(sData.lights.iRenderTargetID)
			SET_SCRIPT_GFX_ALIGN(UI_ALIGN_IGNORE,UI_ALIGN_IGNORE)
			SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
			SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
			DRAW_RECT(0.5,sData.lights.fMeterRectLoc,1,1,sData.lights.iMeterColours[0],sData.lights.iMeterColours[1],sData.lights.iMeterColours[2],sData.lights.iMeterAlpha)
			RESET_SCRIPT_GFX_ALIGN()
			SET_TEXT_RENDER_ID(GET_DEFAULT_SCRIPT_RENDERTARGET_RENDER_ID())
			SWITCH INT_TO_ENUM(LOVE_METER_PATTERNS,sData.lights.iPatternID)
				CASE PATTERN_MATCHING
					sData.lights.fMeterRectLoc =  sData.lights.fMeterRectLoc -@ sData.lights.fMeterSpeed
				
					#IF IS_DEBUG_BUILD
					IF sDebugData.db_bVisTest
						IF sData.lights.fMeterRectLoc < GET_LOVE_METER_POSITION_AVERAGE_POSITION(sDebugData.db_Score[0],sDebugData.db_Score[1])
							sData.lights.fMeterRectLoc = GET_LOVE_METER_POSITION_AVERAGE_POSITION(sDebugData.db_Score[0],sDebugData.db_Score[1])
						ENDIF
					ELSE
					#ENDIF
						IF sData.lights.fMeterRectLoc < GET_LOVE_METER_POSITION_AVERAGE_POSITION(sData.lights.playerScore[ARCADE_CAB_PLAYER_1],sData.lights.playerScore[ARCADE_CAB_PLAYER_2])
							sData.lights.fMeterRectLoc = GET_LOVE_METER_POSITION_AVERAGE_POSITION(sData.lights.playerScore[ARCADE_CAB_PLAYER_1],sData.lights.playerScore[ARCADE_CAB_PLAYER_2])
						ENDIF
					#IF IS_DEBUG_BUILD
					ENDIF
					#ENDIF
				BREAK
			ENDSWITCH
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_LOVE_METER_PATTERN_CYCLE()
	
	IF NOT HAS_NET_TIMER_STARTED(sData.lights.patternTimer)
		START_NET_TIMER(sData.lights.patternTimer,TRUE)
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF sDebugData.iOverrideFlashSpeed > 0
		sData.lights.iLightFlashTime = sDebugData.iOverrideFlashSpeed
	ELSE
	#ENDIF
		sData.lights.iLightFlashTime = 250
	#IF IS_DEBUG_BUILD
	ENDIF
	#ENDIF

	IF HAS_NET_TIMER_EXPIRED(sData.lights.patternTimer,sData.lights.iLightFlashTime,TRUE)
		IF sData.lights.iPatternLightID <= 9
			sData.lights.iPatternLightID++
			IF sData.lights.iPatternLightID > 9
				sData.lights.iPatternLightID = 19
			ENDIF
		ELSE
			sData.lights.iPatternLightID--
			IF sData.lights.iPatternLightID < 10
				sData.lights.iPatternLightID = 0
				sData.lights.iLoopsComplete++
			ENDIF
		ENDIF
		REINIT_NET_TIMER(sData.lights.patternTimer,TRUE)
	ENDIF

	IF sData.lights.iLoopsComplete >= 2
		sData.lights.bPatternFinished = TRUE
	ENDIF
ENDPROC

PROC MAINTAIN_LOVE_METER_PATTERN_FLASH_ALTERNATE()
	IF NOT HAS_NET_TIMER_STARTED(sData.lights.patternTimer)
		START_NET_TIMER(sData.lights.patternTimer,TRUE)
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF sDebugData.iOverrideFlashSpeed > 0
		sData.lights.iLightFlashTime = sDebugData.iOverrideFlashSpeed
	ELSE
	#ENDIF
		IF INT_TO_ENUM(LOVE_METER_PATTERNS,sData.lights.iPatternID) = PATTERN_FLASH
			sData.lights.iLightFlashTime = 500
		ELIF INT_TO_ENUM(LOVE_METER_PATTERNS,sData.lights.iPatternID) = PATTERN_CALCULATE
			sData.lights.iLightFlashTime = 500
		ENDIF
	#IF IS_DEBUG_BUILD
	ENDIF
	#ENDIF

	IF HAS_NET_TIMER_EXPIRED(sData.lights.patternTimer,sData.lights.iLightFlashTime,TRUE)
		sData.lights.iPatternLightID++
		IF sData.lights.iPatternLightID > 1
			sData.lights.iPatternLightID = 0
			sData.lights.iLoopsComplete++
		ENDIF
		REINIT_NET_TIMER(sData.lights.patternTimer,TRUE)
	ENDIF
	
	IF sData.lights.iLoopsComplete >= 5
		sData.lights.bPatternFinished = TRUE
	ENDIF
ENDPROC


PROC MAINTAIN_LOVE_METER_PATTERN_MATCHING()
	IF NOT HAS_NET_TIMER_STARTED(sData.lights.patternTimer)
		START_NET_TIMER(sData.lights.patternTimer,TRUE)
	ENDIF

	#IF IS_DEBUG_BUILD
	IF sDebugData.iOverrideFlashSpeed > 0
		sData.lights.iLightFlashTime = sDebugData.iOverrideFlashSpeed
	ELSE
	#ENDIF
		sData.lights.iLightFlashTime = 500
	#IF IS_DEBUG_BUILD
	ENDIF
	#ENDIF
	
	#IF IS_DEBUG_BUILD
	IF sDebugData.db_bVisTest
		IF sData.lights.iPatternLightID >= sDebugData.db_Score[0]
		AND sData.lights.iPatternLightID >= sDebugData.db_Score[1]
			IF sData.lights.fMeterRectLoc <= GET_LOVE_METER_POSITION_AVERAGE_POSITION(sDebugData.db_Score[0],sDebugData.db_Score[1])
				IF HAS_NET_TIMER_EXPIRED(sData.lights.patternTimer,sData.lights.iLightFlashTime,TRUE)
					sData.lights.bPatternFinished = TRUE
				ENDIF
			ENDIF
		ENDIF
	ELSE
	#ENDIF
		IF sData.lights.iPatternLightID >=  sData.lights.playerScore[ARCADE_CAB_PLAYER_1]
		AND sData.lights.iPatternLightID >= sData.lights.playerScore[ARCADE_CAB_PLAYER_2]
			IF sData.lights.fMeterRectLoc <= GET_LOVE_METER_POSITION_AVERAGE_POSITION(sData.lights.playerScore[ARCADE_CAB_PLAYER_1],sData.lights.playerScore[ARCADE_CAB_PLAYER_2])
				IF HAS_NET_TIMER_EXPIRED(sData.lights.patternTimer,sData.lights.iLightFlashTime,TRUE)
					sData.lights.bPatternFinished = TRUE
				ENDIF
			ENDIF
		ENDIF
	#IF IS_DEBUG_BUILD
	ENDIF
	#ENDIF
	
	IF sData.lights.iPatternLightID < 11
		IF HAS_NET_TIMER_EXPIRED(sData.lights.patternTimer,sData.lights.iLightFlashTime,TRUE)
			sData.lights.iPatternLightID++
			REINIT_NET_TIMER(sData.lights.patternTimer,TRUE)
		ENDIF
	ENDIF
	
ENDPROC

PROC MAINTAIN_LOVE_METER_PATTERN_END_MATCH()
	IF NOT HAS_NET_TIMER_STARTED(sData.lights.patternTimer)
		START_NET_TIMER(sData.lights.patternTimer,TRUE)
	ENDIF

	#IF IS_DEBUG_BUILD
	IF sDebugData.iOverrideFlashSpeed > 0
		sData.lights.iLightFlashTime = sDebugData.iOverrideFlashSpeed
	ELSE
	#ENDIF
		sData.lights.iLightFlashTime = 500
	#IF IS_DEBUG_BUILD
	ENDIF
	#ENDIF
	
	
	IF HAS_NET_TIMER_EXPIRED(sData.lights.patternTimer,sData.lights.iLightFlashTime,TRUE)
		sData.lights.iPatternLightID++
		REINIT_NET_TIMER(sData.lights.patternTimer,TRUE)
	ENDIF
	IF sData.lights.iPatternLightID > 1
		sData.lights.iPatternLightID = 0
		sData.lights.iLoopsComplete++
	ENDIF
	IF sData.lights.iLoopsComplete >= 3
		sData.lights.bPatternFinished = TRUE
	ENDIF
ENDPROC

PROC MAINTAIN_LOVE_METER_PATTERN_END_LOVE()
	IF NOT HAS_NET_TIMER_STARTED(sData.lights.patternTimer)
		START_NET_TIMER(sData.lights.patternTimer,TRUE)
	ENDIF

	#IF IS_DEBUG_BUILD
	IF sDebugData.iOverrideFlashSpeed > 0
		sData.lights.iLightFlashTime = sDebugData.iOverrideFlashSpeed
	ELSE
	#ENDIF
		sData.lights.iLightFlashTime = 250
	#IF IS_DEBUG_BUILD
	ENDIF
	#ENDIF
	
	IF HAS_NET_TIMER_EXPIRED(sData.lights.patternTimer,sData.lights.iLightFlashTime,TRUE)
		sData.lights.iPatternLightID++
		REINIT_NET_TIMER(sData.lights.patternTimer,TRUE)
	ENDIF
	IF sData.lights.iPatternLightID > 1
		sData.lights.iPatternLightID = 0
		sData.lights.iLoopsComplete++
	ENDIF
	IF sData.lights.iLoopsComplete >= 3
		sData.lights.bPatternFinished = TRUE
	ENDIF
ENDPROC

PROC MAINTAIN_LOVE_METER_PATTERN_END_NEMESIS()
	IF NOT HAS_NET_TIMER_STARTED(sData.lights.patternTimer)
		START_NET_TIMER(sData.lights.patternTimer,TRUE)
		sData.lights.iPatternLightID = 11
	ENDIF

	#IF IS_DEBUG_BUILD
	IF sDebugData.iOverrideFlashSpeed > 0
		sData.lights.iLightFlashTime = sDebugData.iOverrideFlashSpeed
	ELSE
	#ENDIF
		sData.lights.iLightFlashTime = 125
	#IF IS_DEBUG_BUILD
	ENDIF
	#ENDIF
	IF sData.lights.iPatternLightID > 0
		IF HAS_NET_TIMER_EXPIRED(sData.lights.patternTimer,sData.lights.iLightFlashTime,TRUE)
			sData.lights.iPatternLightID--
			REINIT_NET_TIMER(sData.lights.patternTimer,TRUE)
		ENDIF
	ELSE
		IF HAS_NET_TIMER_EXPIRED(sData.lights.patternTimer,1000,TRUE)
		AND sData.lights.iMeterAlpha <= 0
			sData.lights.bPatternFinished = TRUE
		ENDIF
	ENDIF
	sData.lights.iMeterAlpha = FLOOR(TO_FLOAT(sData.lights.iMeterAlpha) -@ 80.0)
ENDPROC

FUNC BOOL SHOULD_LOVE_METER_LIGHT_BE_VISIBLE_FOR_SCORE(INT iLight, BOOL bCheckIncrement)
	IF iLight < 10
		IF sData.lights.iPatternLightID > iLight
		OR NOT bCheckIncrement
			#IF IS_DEBUG_BUILD
			IF sDebugData.db_bVisTest
			OR NOT bCheckIncrement
				IF sDebugData.db_Score[0] > iLight
					RETURN TRUE
				ENDIF
			ENDIF
			#ENDIF
			IF sData.lights.playerScore[ARCADE_CAB_PLAYER_1] > iLight
				RETURN TRUE
			ENDIF
		ENDIF
	ELSE
		IF sData.lights.iPatternLightID > iLight-10
		OR NOT bCheckIncrement
			#IF IS_DEBUG_BUILD
			IF sDebugData.db_bVisTest
			OR NOT bCheckIncrement
				IF sDebugData.db_Score[1] > iLight-10
					RETURN TRUE
				ENDIF
			ENDIF
			#ENDIF
			IF sData.lights.playerScore[ARCADE_CAB_PLAYER_2] > iLight-10
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_LOVE_METER_LIGHT_BE_VISIBLE(INT iLight)
	//INT i
	SWITCH INT_TO_ENUM(LOVE_METER_PATTERNS,sData.lights.iPatternID)
		CASE PATTERN_OFF
			RETURN FALSE
		BREAK
		CASE PATTERN_CYCLE
			IF sData.lights.iPatternLightID = iLight
				RETURN TRUE
			ENDIF
		BREAK
		CASE PATTERN_FLASH
			IF sData.lights.iPatternLightID = 1
				RETURN TRUE
			ENDIF
		BREAK
		CASE PATTERN_CALCULATE
			IF sData.lights.iPatternLightID = 1
				IF IS_INT_EVEN(ilight+1)
					RETURN TRUE
				ELSE
					RETURN FALSE
				ENDIF
				
			ELSE
				IF IS_INT_EVEN(ilight+1)
					RETURN FALSE
				ELSE
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
		CASE PATTERN_MATCHING
			RETURN SHOULD_LOVE_METER_LIGHT_BE_VISIBLE_FOR_SCORE(iLight,TRUE)
		BREAK
		CASE PATTERN_END_MATCH
			IF IS_INT_EVEN(sData.lights.iPatternLightID+1)
				RETURN SHOULD_LOVE_METER_LIGHT_BE_VISIBLE_FOR_SCORE(iLight,FALSE)
			ELSE
				RETURN FALSE
			ENDIF
		BREAK
		CASE PATTERN_END_LOVE
			IF IS_INT_EVEN(sData.lights.iPatternLightID+1)
				sData.lights.iMeterAlpha = 0
				RETURN TRUE
			ELSE
				sData.lights.iMeterAlpha = 255
				RETURN FALSE
			ENDIF
		BREAK
		CASE PATTERN_END_NEMESIS
			IF iLight < 10
				IF sData.lights.iPatternLightID > iLight
					RETURN TRUE
				ENDIF
			ELSE
				IF sData.lights.iPatternLightID > iLight-10
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_LOVE_METER_LIGHTS()
	#IF IS_DEBUG_BUILD
	IF sDebugData.db_bVisTest
		IF sData.lights.iPatternID < ENUM_TO_INT(PATTERN_MATCHING)
			IF NOT HAS_NET_TIMER_STARTED(sDebugData.stTestTimer)
				START_NET_TIMER(sDebugData.stTestTimer,TRUE)
				SET_LOVE_METER_LIGHT_PATTERN(PATTERN_CALCULATE)
			ELSE
				IF HAS_NET_TIMER_EXPIRED(sDebugData.stTestTimer,2000,TRUE)
					IF sDebugData.db_bTrueLove
						sDebugData.db_Score[0] = 11
						sDebugData.db_Score[1] = 11
					ELIF sDebugData.db_bNemesis
						sDebugData.db_Score[0] = 0
						sDebugData.db_Score[1] = 0
					ELSE
						sDebugData.db_Score[0] = GET_RANDOM_INT_IN_RANGE(0,12)
						sDebugData.db_Score[1] = GET_RANDOM_INT_IN_RANGE(0,12)
					ENDIF
					SET_LOVE_METER_LIGHT_PATTERN(PATTERN_MATCHING)
				ENDIF
			ENDIF
		ELSE
			RESET_NET_TIMER(sDebugData.stTestTimer)
			IF sData.lights.bPatternFinished
				IF sData.lights.iPatternID = ENUM_TO_INT(PATTERN_MATCHING)
					IF sDebugData.db_Score[0] = 0
					AND sDebugData.db_Score[1] = 0
						SET_LOVE_METER_LIGHT_PATTERN(PATTERN_END_NEMESIS)
					ELIF sDebugData.db_Score[0] = 11
					AND sDebugData.db_Score[1] = 11
						SET_LOVE_METER_LIGHT_PATTERN(PATTERN_END_LOVE)
					ELSE
						SET_LOVE_METER_LIGHT_PATTERN(PATTERN_END_MATCH)
					ENDIF
				ELSE
					SET_LOVE_METER_LIGHT_PATTERN(PATTERN_CYCLE)
				ENDIF
			ENDIF
		ENDIF
		
	ELIF sDebugData.idb_LoveMeterPattern != -1
	AND sDebugData.idb_LoveMeterPattern  != sData.lights.iPatternID
		SET_LOVE_METER_LIGHT_PATTERN(INT_TO_ENUM(LOVE_METER_PATTERNS,sDebugData.idb_LoveMeterPattern))	
		sDebugData.idb_LoveMeterPattern  = sData.lights.iPatternID
	ENDIF
	#ENDIF
	
	IF sData.lights.bPatternFinished
		SWITCH INT_TO_ENUM(LOVE_METER_PATTERNS,sData.lights.iPatternID)
			CASE PATTERN_CYCLE
				SET_LOVE_METER_LIGHT_PATTERN(PATTERN_FLASH)
			BREAK
			CASE PATTERN_FLASH
				SET_LOVE_METER_LIGHT_PATTERN(PATTERN_CYCLE)
			BREAK
		ENDSWITCH
	ENDIF
	
	INT i
	VECTOR vLoc, vRot
	MODEL_NAMES theModel
	REPEAT 20 i
		IF NOT DOES_ENTITY_EXIST(sData.cabinetObj)
			IF DOES_ENTITY_EXIST(sData.lights.Objs[i])
				IF IS_ENTITY_VISIBLE(sData.lights.Objs[i])
					SET_ENTITY_VISIBLE(sData.lights.Objs[i],FALSE)
				ENDIF
			ENDIF
		ELSE
			IF NOT DOES_ENTITY_EXIST(sData.lights.Objs[i])
				theModel = GET_LOVE_METER_LIGHT_MODEL(i)
				IF REQUEST_LOAD_MODEL(theModel)
					GET_LOVE_METER_LIGHT_LOCATION(i,vLoc,vRot)
					sData.lights.Objs[i] = CREATE_OBJECT_NO_OFFSET(GET_LOVE_METER_LIGHT_MODEL(i),vLoc,FALSE,FALSE,TRUE)
					SET_ENTITY_ROTATION(sData.lights.Objs[i],vRot)
					FREEZE_ENTITY_POSITION(sData.lights.Objs[i],TRUE)
					SET_ENTITY_PROOFS(sData.lights.Objs[i],TRUE,TRUE,TRUE,TRUE,TRUE,TRUE)
					SET_ENTITY_INVINCIBLE(sData.lights.Objs[i],TRUE)
					SET_CAN_CLIMB_ON_ENTITY(sData.lights.Objs[i],FALSE)
					SET_ENTITY_VISIBLE(sData.lights.Objs[i],FALSE)
					PRINTLN("MAINTAIN_LOVE_METER_LIGHTS: creating light #",i," pos = ",vLoc," rot = ",vRot)
				ENDIF
				EXIT
			ELSE
				IF DOES_ENTITY_EXIST(sData.lights.Objs[19])
					IF SHOULD_LOVE_METER_LIGHT_BE_VISIBLE(i)
						IF IS_ENTITY_VISIBLE(sData.lights.Objs[i])
							SET_ENTITY_VISIBLE(sData.lights.Objs[i],FALSE)
						ENDIF
					ELSE
						IF NOT IS_ENTITY_VISIBLE(sData.lights.Objs[i])
							SET_ENTITY_VISIBLE(sData.lights.Objs[i],TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	SWITCH INT_TO_ENUM(LOVE_METER_PATTERNS, sData.lights.iPatternID)
		CASE PATTERN_CYCLE
			MAINTAIN_LOVE_METER_PATTERN_CYCLE()
		BREAK
		CASE PATTERN_FLASH
		CASE PATTERN_CALCULATE
			MAINTAIN_LOVE_METER_PATTERN_FLASH_ALTERNATE()
		BREAK
		CASE PATTERN_MATCHING
			MAINTAIN_LOVE_METER_PATTERN_MATCHING()
		BREAK
		CASE PATTERN_END_MATCH
			MAINTAIN_LOVE_METER_PATTERN_END_MATCH()
		BREAK
		CASE PATTERN_END_LOVE
			MAINTAIN_LOVE_METER_PATTERN_END_LOVE()
		BREAK
		CASE PATTERN_END_NEMESIS
			MAINTAIN_LOVE_METER_PATTERN_END_NEMESIS()
		BREAK
	ENDSWITCH
ENDPROC

PROC MAINTAIN_LOVE_METER_LIGHT_IDLE_STATE()
	IF IS_BIT_SET(serverBD.iServerBS, LOVE_METER_SERVER_BD_GET_USING_PLAYERS)	
		SET_LOVE_METER_LIGHT_PATTERN(PATTERN_CALCULATE)
		SET_LOVE_METER_LIGHT_STATE(LOVE_METER_LIGHT_CALCULATE_STATE)
	ENDIF
ENDPROC

PROC MAINTAIN_LOVE_METER_LIGHT_CALCULATE_STATE()
	IF IS_NET_PLAYER_OK(serverBD.playerOne)
	AND IS_NET_PLAYER_OK(serverBD.playerTwo)
		IF IS_BIT_SET(playerBD[NATIVE_TO_INT(serverBD.playerOne)].iLocalBS, LOVE_METER_PLAYER_BD_FINAL_SCORE_CALCULATED)
		AND IS_BIT_SET(playerBD[NATIVE_TO_INT(serverBD.playerTwo)].iLocalBS, LOVE_METER_PLAYER_BD_FINAL_SCORE_CALCULATED)
			INT iScoreLeft, iScoreRight
			INT iRandomLeft, iRandomRight
			INT iPlayerSlot = GET_ARCADE_CABINET_LOCATE_PLAYER_SLOT_PLAYER_IS_IN(serverBD.playerOne)
			
			IF iPlayerSlot = ENUM_TO_INT(ARCADE_CAB_PLAYER_1)
				iScoreLeft = playerBD[NATIVE_TO_INT(serverBD.playerOne)].iFinalScore
				iScoreRight = playerBD[NATIVE_TO_INT(serverBD.playerTwo)].iFinalScore
				iRandomLeft = playerBD[NATIVE_TO_INT(serverBD.playerOne)].iRandomScore
				iRandomRight = playerBD[NATIVE_TO_INT(serverBD.playerTwo)].iRandomScore
			ELSE
				iScoreRight = playerBD[NATIVE_TO_INT(serverBD.playerOne)].iFinalScore
				iScoreLeft = playerBD[NATIVE_TO_INT(serverBD.playerTwo)].iFinalScore
				iRandomRight = playerBD[NATIVE_TO_INT(serverBD.playerOne)].iRandomScore
				iRandomLeft = playerBD[NATIVE_TO_INT(serverBD.playerTwo)].iRandomScore
			ENDIF
			
			IF iScoreLeft = iScoreRight
				SWITCH iScoreLeft
					CASE 0
					CASE 1	
						IF iRandomLeft = iRandomRight
						#IF IS_DEBUG_BUILD
						OR sDebugData.bMatchRandomScore
						#ENDIF
							// LOVETEST_NEMESIS
							SET_LOVE_MACHINE_SCORES(0, 0)
						ELSE
							// LOVETEST_ICE_COLD
							SET_LOVE_MACHINE_SCORES(1, 1)
						ENDIF
					BREAK
					CASE 2
					CASE 4
					CASE 5
					CASE 6
					CASE 7
					CASE 8
					CASE 9
						SET_LOVE_MACHINE_SCORES(iScoreLeft, iScoreRight)	
					BREAK
					CASE 10
						IF iRandomLeft = iRandomRight
						OR SHOULD_FORCE_TRUE_LOVE_FOR_PLAYER()
						#IF IS_DEBUG_BUILD
						OR sDebugData.bMatchRandomScore
						#ENDIF
							//LOVETEST_TRUELOVE
							SET_LOVE_MACHINE_SCORES(11, 11)
						ELSE
							//LOVETEST_SIZZLIN
							SET_LOVE_MACHINE_SCORES(10, 10)	
						ENDIF	
					BREAK
				ENDSWITCH
			ELSE
				IF iScoreLeft = 0 
				OR iScoreLeft = 1
					iScoreLeft = 1
				ENDIF
				IF iScoreRight = 0 
				OR iScoreRight = 1
					iScoreRight = 1
				ENDIF
				IF SHOULD_FORCE_TRUE_LOVE_FOR_PLAYER()
					SET_LOVE_MACHINE_SCORES(11, 11)
				ELSE
					SET_LOVE_MACHINE_SCORES(iScoreLeft, iScoreRight)
				ENDIF	
			ENDIF

			SET_LOVE_METER_LIGHT_PATTERN(PATTERN_MATCHING)
			SET_LOVE_METER_LIGHT_STATE(LOVE_METER_LIGHT_MATCHING_STATE)
		ENDIF
	ENDIF	
ENDPROC

PROC MAINTAIN_LOVE_METER_LIGHT_MATCHING_STATE()
	IF sData.lights.bPatternFinished
		IF sData.lights.playerScore[ARCADE_CAB_PLAYER_1] = 11
		AND sData.lights.playerScore[ARCADE_CAB_PLAYER_2] = 11
			SET_LOVE_METER_LIGHT_PATTERN(PATTERN_END_LOVE)
		ELIF sData.lights.playerScore[ARCADE_CAB_PLAYER_1] = 0
		AND sData.lights.playerScore[ARCADE_CAB_PLAYER_2] = 0			
			SET_LOVE_METER_LIGHT_PATTERN(PATTERN_END_NEMESIS)
		ELSE	
			SET_LOVE_METER_LIGHT_PATTERN(PATTERN_END_MATCH)
		ENDIF	
		
		SET_LOVE_METER_LIGHT_STATE(LOVE_METER_LIGHT_END_MATCHING_STATE)
	ENDIF
ENDPROC

PROC MAINTAIN_LOVE_METER_LIGHT_END_MATCHING_STATE()
	IF sData.lights.bPatternFinished
		IF !IS_BIT_SET(serverBD.iServerBS, LOVE_METER_SERVER_BD_GET_USING_PLAYERS)	
			SET_LOVE_METER_LIGHT_PATTERN(PATTERN_CYCLE)
			SET_LOVE_METER_LIGHT_STATE(LOVE_METER_LIGHT_IDLE_STATE)
		ENDIF	
	ENDIF
ENDPROC

PROC MAINTAIN_LOVE_METER_LIGHT_STATE()
	SWITCH sData.eLightState
		CASE LOVE_METER_LIGHT_IDLE_STATE
			MAINTAIN_LOVE_METER_LIGHT_IDLE_STATE()
		BREAK
		CASE LOVE_METER_LIGHT_CALCULATE_STATE
			MAINTAIN_LOVE_METER_LIGHT_CALCULATE_STATE()
		BREAK
		CASE LOVE_METER_LIGHT_MATCHING_STATE
			MAINTAIN_LOVE_METER_LIGHT_MATCHING_STATE()
		BREAK
		CASE LOVE_METER_LIGHT_END_MATCHING_STATE
			MAINTAIN_LOVE_METER_LIGHT_END_MATCHING_STATE()
		BREAK
	ENDSWITCH
ENDPROC

PROC RUN_MAIN_CLIENT_LOGIC()
	MAINTAIN_LOVE_METER_RENDER_TARGET()
	MAINTAIN_LOVE_METER_LIGHTS()
	MAINTAIN_LOVE_METER_LIGHT_STATE()
	
	SWITCH sData.eState
		CASE ARCADE_CABINET_STATE_LOADING
			MAINTAIN_LOVE_METER_LOADING_STATE()
		BREAK
		CASE ARCADE_CABINET_STATE_IDLE	
			MAINTAIN_LOVE_METER_IDLE_STATE()
		BREAK
		CASE ARCADE_CABINET_STATE_CLEANUP
			MAINTAIN_LOVE_METER_CLEANUP_STATE()
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Check if both players are ready to use the love meter then let them start at the same to avoid syncing issue 
PROC MAINTAIN_GET_USING_PLAYERS_DETAILS()
	IF NOT IS_BIT_SET(serverBD.iServerBS, LOVE_METER_SERVER_BD_GET_USING_PLAYERS)
		INT i, iNumberOfUsers
		PLAYER_INDEX playerToCheck
		REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
				playerToCheck = INT_TO_NATIVE(PLAYER_INDEX, i)
				IF IS_BIT_SET(playerBD[NATIVE_TO_INT(playerToCheck)].iLocalBS, LOVE_METER_PLAYER_BD_USING_LOVE_METER)
					IF IS_PLAYER_USING_ARCADE_CABINET(playerToCheck)	
						iNumberOfUsers++
						IF iNumberOfUsers = 1
							serverBD.playerOne = playerToCheck
						ELSE	
							serverBD.playerTwo = playerToCheck
						ENDIF	
						PRINTLN("[AM_ARCADE_LOVE_METER] MAINTAIN_GET_USING_PLAYERS_DETAILS ", GET_PLAYER_NAME(playerToCheck), " iNumberOfUsers: ", iNumberOfUsers)
					ENDIF	
				ENDIF
			ENDIF
		ENDREPEAT	
		
		IF iNumberOfUsers = 2
		#IF IS_DEBUG_BUILD
		OR (sDebugData.bSoloGame AND iNumberOfUsers = 1)
		#ENDIF
			#IF IS_DEBUG_BUILD
			IF sDebugData.bSoloGame
				serverBD.playerTwo = serverBD.playerOne
			ENDIF
			#ENDIF
			
			SET_BIT(serverBD.iServerBS, LOVE_METER_SERVER_BD_GET_USING_PLAYERS)
			serverBD.iMatchID = GET_CLOUD_TIME_AS_INT()
			REINIT_NET_TIMER(serverBD.tCalculationTimer)
			PRINTLN("[AM_ARCADE_LOVE_METER] MAINTAIN_GET_USING_PLAYERS_DETAILS LOVE_METER_SERVER_BD_GET_USING_PLAYERS TRUE serverBD.iMatchID: ", serverBD.iMatchID)
		ENDIF
	ELSE
		IF NOT IS_NET_PLAYER_OK(serverBD.playerOne)
		OR (NOT IS_PLAYER_USING_ARCADE_CABINET(serverBD.playerOne) AND NOT IS_PLAYER_USING_ARCADE_CABINET(serverBD.playerTwo))
			serverBD.playerOne = INVALID_PLAYER_INDEX()
		ENDIF
		
		IF NOT IS_NET_PLAYER_OK(serverBD.playerTwo)
		OR (NOT IS_PLAYER_USING_ARCADE_CABINET(serverBD.playerOne) AND NOT IS_PLAYER_USING_ARCADE_CABINET(serverBD.playerTwo))
			serverBD.playerTwo = INVALID_PLAYER_INDEX()
		ENDIF
		
		IF NOT IS_NET_PLAYER_OK(serverBD.playerOne)
		AND NOT IS_NET_PLAYER_OK(serverBD.playerTwo)
			CLEAR_BIT(serverBD.iServerBS, LOVE_METER_SERVER_BD_GET_USING_PLAYERS)
			PRINTLN("[AM_ARCADE_LOVE_METER] MAINTAIN_GET_USING_PLAYERS_DETAILS LOVE_METER_SERVER_BD_GET_USING_PLAYERS FALSE")
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Check if both players played the final animation then lets them cleanup the player broadcastdata
PROC MAINTAIN_PLAYER_CLEANUP_REQUEST()
	IF IS_NET_PLAYER_OK(serverBD.playerOne)
	AND IS_NET_PLAYER_OK(serverBD.playerTwo)
	 	IF IS_BIT_SET(playerBD[NATIVE_TO_INT(serverBD.playerOne)].iLocalBS, LOVE_METER_PLAYER_BD_FINAL_ANIM_ENDED)
		AND IS_BIT_SET(playerBD[NATIVE_TO_INT(serverBD.playerTwo)].iLocalBS, LOVE_METER_PLAYER_BD_FINAL_ANIM_ENDED)
			IF NOT IS_BIT_SET(serverBD.iServerBS, LOVE_METER_SERVER_BD_CLEANUP_PLAYERS_GAME_STATE)
				SET_BIT(serverBD.iServerBS, LOVE_METER_SERVER_BD_CLEANUP_PLAYERS_GAME_STATE)
				PRINTLN("[AM_ARCADE_LOVE_METER] MAINTAIN_PLAYER_CLEANUP_REQUEST LOVE_METER_SERVER_BD_CLEANUP_PLAYERS_GAME_STATE TRUE")
			ENDIF	
		ENDIF
	ELSE
		IF IS_BIT_SET(serverBD.iServerBS, LOVE_METER_SERVER_BD_CLEANUP_PLAYERS_GAME_STATE)
			CLEAR_BIT(serverBD.iServerBS, LOVE_METER_SERVER_BD_CLEANUP_PLAYERS_GAME_STATE)
			PRINTLN("[AM_ARCADE_LOVE_METER] MAINTAIN_PLAYER_CLEANUP_REQUEST LOVE_METER_SERVER_BD_CLEANUP_PLAYERS_GAME_STATE FALSE")
		ENDIF	
	ENDIF
ENDPROC

PROC RUN_MAIN_SERVER_LOGIC()
	MAINTAIN_GET_USING_PLAYERS_DETAILS()
	MAINTAIN_PLAYER_CLEANUP_REQUEST()
ENDPROC	

#IF IS_DEBUG_BUILD
PROC DEBUG_DISPLAY_PLAYER_SCORE_ON_SCREE()
	IF sDebugData.bDisplayScores
		
		// Background
		DRAW_RECT(0.133, 0.30, 0.246, 0.4, 0, 0, 0, 200)
		
		// Title
		SET_TEXT_SCALE(0.4, 0.4)
		SET_TEXT_COLOUR(0, 188, 158, 255)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.013, 0.123, "STRING", "Love Meter Debug")

		// local player score
		SET_TEXT_SCALE(0.35, 0.35)
		SET_TEXT_COLOUR(178, 34, 34, 255)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.013, 0.16, "STRING", "local Player score: ")
		
		SET_TEXT_SCALE(0.35, 0.35)
		SET_TEXT_COLOUR(0, 255, 20, 255)
		DISPLAY_TEXT_WITH_NUMBER(0.15, 0.16, "NUMBER", playerBD[NATIVE_TO_INT(PLAYER_ID())].iFinalScore)
		
		IF sData.secondPlayer != INVALID_PLAYER_INDEX()
			// Remote player score
			SET_TEXT_SCALE(0.35, 0.35)
			SET_TEXT_COLOUR(178, 34, 34, 255)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.013, 0.25, "STRING", "remote Player score: ")
			
			SET_TEXT_SCALE(0.35, 0.35)
			SET_TEXT_COLOUR(0, 255, 20, 255)
			DISPLAY_TEXT_WITH_NUMBER(0.15, 0.25, "NUMBER", playerBD[NATIVE_TO_INT(sData.secondPlayer)].iFinalScore)
		ENDIF
		
		
		INT iFinalScore = playerBD[NATIVE_TO_INT(PLAYER_ID())].iFinalScore
		
		SET_TEXT_SCALE(0.5, 0.5)
		SET_TEXT_COLOUR(240, 230, 140, 255)
		
		INT iFinalSecondPlayerScore, iRanSecondPlayerScore  
		IF sData.secondPlayer != INVALID_PLAYER_INDEX()
			iFinalSecondPlayerScore = playerBD[NATIVE_TO_INT(sData.secondPlayer)].iFinalScore
			iRanSecondPlayerScore = playerBD[NATIVE_TO_INT(sData.secondPlayer)].iRandomScore
		ENDIF	
		
		STRING strScore 
		SWITCH iFinalScore
			CASE 0
			CASE 1
				IF (iRanSecondPlayerScore = playerBD[NATIVE_TO_INT(PLAYER_ID())].iRandomScore OR sDebugData.bMatchRandomScore)
					strScore = "Nemesis"			
				ELSE
					strScore = "Ice Cold"
				ENDIF
			BREAK
			CASE 2	strScore = "Cold Shoulder"			BREAK
			CASE 3	strScore = "Thaw out"				BREAK
			CASE 4	strScore = "Clammy"					BREAK
			CASE 5	strScore = "Warmer"					BREAK
			CASE 6	strScore = "Flushed"				BREAK
			CASE 7	strScore = "Getting Steamy"			BREAK
			CASE 8	strScore = "Burning loins"			BREAK
			CASE 9	strScore = "Hotn' Heavy"			BREAK
			CASE 10	
				IF iFinalSecondPlayerScore = 10
				AND (iRanSecondPlayerScore = playerBD[NATIVE_TO_INT(PLAYER_ID())].iRandomScore	OR sDebugData.bMatchRandomScore)
					strScore = "Perfect Match – True Love"	
				ELSE	
					strScore = "Sizzlin"	
				ENDIF
			BREAK
		ENDSWITCH 
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.075, 0.4, "STRING", strScore)

	ENDIF
ENDPROC

PROC UPDATE_DEBUG_WIDGETS()
	DEBUG_DISPLAY_PLAYER_SCORE_ON_SCREE()
ENDPROC
#ENDIF

SCRIPT (ACM_CHILD_SCRIPT_LAUNCHER_STRUCT missionScriptArgs)

	IF NETWORK_IS_GAME_IN_PROGRESS() 
		SCRIPT_INITIALISE(missionScriptArgs)	
	ELSE
		SCRIPT_CLEANUP()
	ENDIF

	WHILE TRUE
		MP_LOOP_WAIT_ZERO()
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		SCRIPT_PROFILER_START_OF_FRAME() 
		#ENDIF
		#ENDIF	
			
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			PRINTLN("[AM_ARCADE_LOVE_METER] - SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE = TRUE")
			SCRIPT_CLEANUP()
		ENDIF
		
		IF SHOULD_KILL_THIS_SCRIPT()
			PRINTLN("[AM_ARCADE_LOVE_METER] - SHOULD_KILL_THIS_SCRIPT = TRUE")
			SCRIPT_CLEANUP()
		ENDIF
		
		RUN_MAIN_CLIENT_LOGIC()

		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			RUN_MAIN_SERVER_LOGIC()
		ENDIF
		
		#IF IS_DEBUG_BUILD
		UPDATE_DEBUG_WIDGETS()
		
		#IF SCRIPT_PROFILER_ACTIVE
		SCRIPT_PROFILER_END_OF_FRAME() 
		#ENDIF
		#ENDIF

	ENDWHILE

#ENDIF	
	
#IF NOT FEATURE_CASINO_HEIST
SCRIPT
#ENDIF
		
ENDSCRIPT 

