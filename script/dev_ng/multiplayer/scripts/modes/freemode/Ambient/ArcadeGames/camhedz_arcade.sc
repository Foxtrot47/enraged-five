USING "globals.sch"
USING "net_events.sch"
USING "net_wait_zero.sch"
USING "net_mission.sch"
USING "camhedz_arcade_main.sch"

#IF IS_DEBUG_BUILD
//#IF CMHDZ_CMDLINE_EDITOR_NEW
	WIDGET_GROUP_ID widgetMinigame
//#ENDIF
#ENDIF

PROC CLEANUP_SCRIPT()

	CMHDZ_CLEANUP_AND_EXIT_CLIENT()
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
	
ENDPROC

/// PURPOSE:
///    Do necessary pre gamme start initialization.
PROC PROCESS_PRE_GAME(MP_MISSION_DATA missionScriptArgs)
	//CDEBUG2LN(DEBUG_MINIGAME, "[CMHDZ_ARCADE] [BAZ] [DSW] [PROCESS_PRE_GAME]")
	 
//	////CDEBUG1LN(DEBUG_MINIGAME, "[CMHDZ_ARCADE] {DSW} [PROCESS_PRE_GAME] Current stack: ",GET_CURRENT_STACK_SIZE(), " Allocated: ", GET_ALLOCATED_STACK_SIZE())
	missionScriptArgs = missionScriptArgs
	
	// This marks the script as a net script, and handles any instancing setup. 
	SETUP_MP_MISSION_FOR_NETWORK(GET_MAX_NUM_PARTICIPANTS_FOR_MP_MISSION(missionScriptArgs.mdID.idMission) , missionScriptArgs)
	
	// This makes sure the net script is active, waits until it is.
	HANDLE_NET_SCRIPT_INITIALISATION()

	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(cmhdzServerBd, SIZE_OF(cmhdzServerBd))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBd, SIZE_OF(playerBd))
	
	// This script will not be paused if another script calls PAUSE_GAME
	SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
	NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()
	
	CMHDZ_PREPARE_MUSIC_EVENT(CMHDZ_MUSIC_EVENT_INTRO)
		
#IF IS_DEBUG_BUILD
	CMHDZ_START_AUDIO_SCENE(CMHDZ_AUDIO_SCENE_FORCE_MACHINE)
#ENDIF
	
	sCMHDZData.bSkipLeaderboard = CMHDZ_SHOULD_SKIP_LEADERBOARD()

ENDPROC

FUNC INT CMHDZ_GET_SERVER_STATE()
	RETURN cmhdzServerBd.iServerGameState
ENDFUNC

PROC CMHDZ_SET_SERVER_STATE(INT iNewState)
//	#IF IS_DEBUG_BUILD
//	STRING sCurrent = CMHDZ_GET_SERVER_STATE_AS_STRING(cmhdzServerBd.iServerGameState)
//	STRING sNew 	= CMHDZ_GET_SERVER_STATE_AS_STRING(iNewState)
//	//CDEBUG2LN(DEBUG_MINIGAME, "[CMHDZ_ARCADE] {BAZ} [CMHDZ_SET_SERVER_STATE] ", sCurrent, " -> ", sNew)
//	#ENDIF
	
	cmhdzServerBd.iServerGameState = iNewState
ENDPROC

FUNC INT CMHDZ_GET_CLIENT_STATE(INT iPart)
	RETURN playerBd[iPart].iGameState
ENDFUNC

PROC CMHDZ_SET_CLIENT_STATE(INT iNewState)
	#IF IS_DEBUG_BUILD
	STRING sCurrent = CMHDZ_GET_SERVER_STATE_AS_STRING(playerBd[PARTICIPANT_ID_TO_INT()].iGameState)
	STRING sNew 	= CMHDZ_GET_SERVER_STATE_AS_STRING(iNewState)
	CDEBUG2LN(DEBUG_MINIGAME, "[CMHDZ_ARCADE] {BAZ} [CMHDZ_SET_CLIENT_STATE] ", sCurrent, " -> ", sNew)
	#ENDIF
	
	playerBd[PARTICIPANT_ID_TO_INT()].iGameState = iNewState
ENDPROC

FUNC BOOL HAVE_MISSION_END_CONDITIONS_BEEN_MET()
	RETURN FALSE
ENDFUNC

PROC CMHDZ_PROCESS_SERVER_LOOP()
	INT i, j
	INT iNumPlayers
	INT iHiScore
	INT iScore
	INT iPartSort = -1
	REPEAT GET_MAX_NUM_PARTICIPANTS_FOR_MP_MISSION(eAM_CAMHEDZ_ARCADE_CABINET) i
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_NATIVE(PARTICIPANT_INDEX, i))
			iNumPlayers++
			
			REPEAT CMHDZ_MAX_LEVELS j
				iHiScore = CMHDZ_GET_PARTICIPANT_HI_SCORE_FOR_LEVEL(i, j)
				IF cmhdzServerBd.sPlayerScores[i].iHiScoreForLevel[j] != iHiScore
					cmhdzServerBd.sPlayerScores[i].iHiScoreForLevel[j] = iHiScore
					////CDEBUG1LN(DEBUG_MINIGAME, "[CMHDZ_ARCADE] {DSW} [CMHDZ_PROCESS_SERVER_LOOP] part ",i, " Hi Score for level ", j, " updated to ", cmhdzServerBd.sPlayerScores[i].iHiScoreForLevel[j])
				ENDIF
				
				iScore = CMHDZ_GET_PARTICIPANT_SCORE_FOR_LEVEL(i, j)
				IF cmhdzServerBd.sPlayerScores[i].iCurrentScoreForLevel[j] != iScore
					cmhdzServerBd.sPlayerScores[i].iCurrentScoreForLevel[j] = iScore
					////CDEBUG1LN(DEBUG_MINIGAME, "[CMHDZ_ARCADE] {DSW} [CMHDZ_PROCESS_SERVER_LOOP] part ",i, " Score for level ", j, " updated to ", cmhdzServerBd.sPlayerScores[i].iCurrentScoreForLevel[j])
				ENDIF
			ENDREPEAT
			
			IF NOT IS_BIT_SET(cmhdzServerBd.iSortLbForPart, i) 
				IF IS_BIT_SET(playerBd[i].iPlayerBitset, ENUM_TO_INT(CMHDZ_PLAYER_BD_BIT_EDITING_LEADERBOARD))
					SET_BIT(cmhdzServerBd.iSortLbForPart, i)
					iPartSort = i
					////CDEBUG1LN(DEBUG_MINIGAME, "[CMHDZ_ARCADE] {DSW} [CMHDZ_PROCESS_SERVER_LOOP] [LEADERBOARD] Should sort because part ", i, " wants to edit the leaderboard")
				ENDIF
			ELSE
				IF NOT IS_BIT_SET(playerBd[i].iPlayerBitset, ENUM_TO_INT(CMHDZ_PLAYER_BD_BIT_EDITING_LEADERBOARD))
					CLEAR_BIT(cmhdzServerBd.iSortLbForPart, i)
					////CDEBUG1LN(DEBUG_MINIGAME, "[CMHDZ_ARCADE] {DSW} [CMHDZ_PROCESS_SERVER_LOOP] [LEADERBOARD] Part ", i, " no longer wants to edit the leaderboard")
				ENDIF
			ENDIF
			
		ENDIF
	ENDREPEAT
	
	
	
	IF iPartSort > -1
		FOR i = 0 TO ciCASINO_ARCADE_LEADERBOARD_POSITIONS - 1
			IF cmhdzServerBd.sLeaderboard[i].iPlayer = iPartSort 
				cmhdzServerBd.sLeaderboard[i].iPlayer = -1
			ENDIF
		ENDFOR
		
		CMHDZ_SORT_SERVER_LEADERBOARD(playerBd, cmhdzServerBd.sLeaderboard, iPartSort)
	ENDIF
	
	IF cmhdzServerBd.iNumPlayers != iNumPlayers
		cmhdzServerBd.iNumPlayers = iNumPlayers
		
		////CDEBUG1LN(DEBUG_MINIGAME, "[CMHDZ_ARCADE] {DSW} [CMHDZ_PROCESS_SERVER_LOOP] cmhdzServerBd.iNumPlayers: ", cmhdzServerBd.iNumPlayers)
		
		IF iNumPlayers = 2
			//-- For telemetry
			IF NOT IS_BIT_SET(cmhdzServerBd.iServerBitset, ENUM_TO_INT(CMHDZ_SERVER_BIT_TWO_PLAYERS_JOINED))
				SET_BIT(cmhdzServerBd.iServerBitset, ENUM_TO_INT(CMHDZ_SERVER_BIT_TWO_PLAYERS_JOINED))
				////CDEBUG1LN(DEBUG_MINIGAME, "[CMHDZ_ARCADE] {DSW} [CMHDZ_PROCESS_SERVER_LOOP] Set CMHDZ_SERVER_BIT_TWO_PLAYERS_JOINED")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL CMHDZ_HANDLE_PLAYER_QUITTING()
	INT iQuitTime = 1000

	IF IS_LOCAL_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR()
		////CDEBUG1LN(DEBUG_MINIGAME, "[CMHDZ_ARCADE] {DSW} [CMHDZ_HANDLE_PLAYER_QUITTING] True - IS_LOCAL_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR")
		RETURN TRUE
	ENDIF
	
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		IF IS_CONTROL_HELD(sCMHDZData.sPcQuitTimer, DEFAULT, iQuitTime)
			////CDEBUG1LN(DEBUG_MINIGAME, "[CMHDZ_ARCADE] {DSW} [CMHDZ_HANDLE_PLAYER_QUITTING] True - sPcQuitTimer")
			RETURN TRUE
		ENDIF
		
		IF (sCMHDZData.eClientState >= CMHDZ_ARCADE_CLIENT_STATE_TITLE_SCREEN)
			
			IF IS_CONTROL_JUST_PRESSED(sCMHDZData.sControlTimer.control, sCMHDZData.sControlTimer.action)
				RESET_NET_TIMER(sCMHDZData.sControlTimer.sTimer)
			ENDIF
		
			IF IS_CONTROL_PRESSED(sCMHDZData.sPcQuitTimer.control, sCMHDZData.sPcQuitTimer.action)
			OR (IS_DISABLED_CONTROL_PRESSED(sCMHDZData.sPcQuitTimer.control, sCMHDZData.sPcQuitTimer.action))
				DRAW_GENERIC_METER(ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), sCMHDZData.sPcQuitTimer.sTimer.Timer)), iQuitTime, "DEG_GAME_QUIT")
			ELSE
				DRAW_GENERIC_METER(0, iQuitTime, "DEG_GAME_QUIT")
			ENDIF
		ENDIF

	ELSE
		IF IS_CONTROL_HELD(sCMHDZData.sControlTimer, DEFAULT, iQuitTime)
			////CDEBUG1LN(DEBUG_MINIGAME, "[CMHDZ_ARCADE] {DSW} [CMHDZ_HANDLE_PLAYER_QUITTING] True - sControlTimer")
			RETURN TRUE
		ENDIF
				
		IF (sCMHDZData.eClientState >= CMHDZ_ARCADE_CLIENT_STATE_TITLE_SCREEN)
					
			IF IS_CONTROL_JUST_PRESSED(sCMHDZData.sControlTimer.control, sCMHDZData.sControlTimer.action)
				RESET_NET_TIMER(sCMHDZData.sControlTimer.sTimer)
			ENDIF
			
			IF IS_CONTROL_PRESSED(sCMHDZData.sControlTimer.control, sCMHDZData.sControlTimer.action)
			OR (IS_DISABLED_CONTROL_PRESSED(sCMHDZData.sControlTimer.control, sCMHDZData.sControlTimer.action))
				DRAW_GENERIC_METER(ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), sCMHDZData.sControlTimer.sTimer.Timer)), iQuitTime, "DEG_GAME_QUIT")
			ELSE
				DRAW_GENERIC_METER(0, iQuitTime, "DEG_GAME_QUIT")
			ENDIF
		ENDIF

	ENDIF
	
	RETURN FALSE
ENDFUNC


SCRIPT(MP_MISSION_DATA missionScriptArgs)
	
	missionScriptArgs.mdID.idMission = eAM_CAMHEDZ_ARCADE_CABINET
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		PROCESS_PRE_GAME(missionScriptArgs)
	ENDIF
	
	WHILE TRUE
		
		MP_LOOP_WAIT_ZERO()
		
		IF NETWORK_IS_GAME_IN_PROGRESS()
		
#IF IS_DEBUG_BUILD

			IF NOT DOES_WIDGET_GROUP_EXIST(widgetMinigame)
				widgetMinigame = START_WIDGET_GROUP("Camhedz: Found Footage")
				STOP_WIDGET_GROUP()
				CMHDZ_DEBUG_SET_PARENT_WIDGET_GROUP(widgetMinigame)
			ENDIF
			
			CMHDZ_UPDATE_WIDGETS()


	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_1, KEYBOARD_MODIFIER_CTRL, "Debug Rifle")
		CMHDZ_PLAY_SOUND_AND_RELEASE_NO_POSITION(CMHDZ_AUDIO_EFFECT_RIFLE_RELOAD_SINGLE)
		CMHDZ_SET_PLAYER_SPECIAL_WEAPON(CMHDZ_PLAYER_WEAPON_RIFLE)
	ELIF IS_DEBUG_KEY_JUST_PRESSED(KEY_2, KEYBOARD_MODIFIER_CTRL, "Debug Shotgun") 
		CMHDZ_PLAY_SOUND_AND_RELEASE_NO_POSITION(CMHDZ_AUDIO_EFFECT_SHOTGUN_RELOAD_SINGLE)
		CMHDZ_SET_PLAYER_SPECIAL_WEAPON(CMHDZ_PLAYER_WEAPON_SHOTGUN)
	ELIF IS_DEBUG_KEY_JUST_PRESSED(KEY_3, KEYBOARD_MODIFIER_CTRL, "Debug NAILGUN") 
		CMHDZ_PLAY_SOUND_AND_RELEASE_NO_POSITION(CMHDZ_AUDIO_EFFECT_NAILGUN_RELOAD_SINGLE)
		CMHDZ_SET_PLAYER_SPECIAL_WEAPON(CMHDZ_PLAYER_WEAPON_NAILGUN)
	ELIF IS_DEBUG_KEY_JUST_PRESSED(KEY_4, KEYBOARD_MODIFIER_CTRL, "Debug INVINCIBILITY") 	
		sCMHDZData.bDebugInvunerablePlayer = !sCMHDZData.bDebugInvunerablePlayer
	ELIF IS_DEBUG_KEY_JUST_PRESSED(KEY_5, KEYBOARD_MODIFIER_CTRL, "Debug Give score") 
				
		playerBd[PARTICIPANT_ID_TO_INT()].iPlayerScore[CMHDZ_LEVEL_CITY] = 25000
		playerBd[PARTICIPANT_ID_TO_INT()].iPlayerHiScore[CMHDZ_LEVEL_CITY] = playerBd[PARTICIPANT_ID_TO_INT()].iPlayerScore[CMHDZ_LEVEL_CITY]
		sCMHDZData.bLevelsCompleted[CMHDZ_LEVEL_CITY] = TRUE
		sCMHDZData.iTotalLevelShots[CMHDZ_LEVEL_CITY] = 100
		sCMHDZData.iTotalLevelEnemyHits[CMHDZ_LEVEL_CITY] = 100
		CMHDZ_ASIGN_STAR_RATING(CMHDZ_LEVEL_CITY)
		
		playerBd[PARTICIPANT_ID_TO_INT()].iPlayerScore[CMHDZ_LEVEL_SCRAPYARD] = 25000
		playerBd[PARTICIPANT_ID_TO_INT()].iPlayerHiScore[CMHDZ_LEVEL_SCRAPYARD] = playerBd[PARTICIPANT_ID_TO_INT()].iPlayerScore[CMHDZ_LEVEL_SCRAPYARD]
		sCMHDZData.bLevelsCompleted[CMHDZ_LEVEL_SCRAPYARD] = TRUE
		sCMHDZData.iTotalLevelShots[CMHDZ_LEVEL_SCRAPYARD] = 100
		sCMHDZData.iTotalLevelEnemyHits[CMHDZ_LEVEL_SCRAPYARD] = 100
		CMHDZ_ASIGN_STAR_RATING(CMHDZ_LEVEL_SCRAPYARD)
		
		playerBd[PARTICIPANT_ID_TO_INT()].iPlayerScore[CMHDZ_LEVEL_ASYLUM] = 25000
		playerBd[PARTICIPANT_ID_TO_INT()].iPlayerHiScore[CMHDZ_LEVEL_ASYLUM] = playerBd[PARTICIPANT_ID_TO_INT()].iPlayerScore[CMHDZ_LEVEL_ASYLUM]
		sCMHDZData.bLevelsCompleted[CMHDZ_LEVEL_ASYLUM] = TRUE
		sCMHDZData.iTotalLevelShots[CMHDZ_LEVEL_ASYLUM] = 100
		sCMHDZData.iTotalLevelEnemyHits[CMHDZ_LEVEL_ASYLUM] = 100
		CMHDZ_ASIGN_STAR_RATING(CMHDZ_LEVEL_ASYLUM)
		
		playerBd[PARTICIPANT_ID_TO_INT()].iPlayerScore[CMHDZ_LEVEL_PARK] = 25000
		playerBd[PARTICIPANT_ID_TO_INT()].iPlayerHiScore[CMHDZ_LEVEL_PARK] = playerBd[PARTICIPANT_ID_TO_INT()].iPlayerScore[CMHDZ_LEVEL_PARK]
		sCMHDZData.bLevelsCompleted[CMHDZ_LEVEL_PARK] = TRUE
		sCMHDZData.iTotalLevelShots[CMHDZ_LEVEL_PARK] = 100
		sCMHDZData.iTotalLevelEnemyHits[CMHDZ_LEVEL_PARK] = 100
		CMHDZ_ASIGN_STAR_RATING(CMHDZ_LEVEL_PARK)
		
		playerBd[PARTICIPANT_ID_TO_INT()].iPlayerScore[CMHDZ_LEVEL_SLAUGHTERHOUSE] = 50000
		playerBd[PARTICIPANT_ID_TO_INT()].iPlayerHiScore[CMHDZ_LEVEL_SLAUGHTERHOUSE] = playerBd[PARTICIPANT_ID_TO_INT()].iPlayerScore[CMHDZ_LEVEL_SLAUGHTERHOUSE]
		sCMHDZData.bLevelsCompleted[CMHDZ_LEVEL_SLAUGHTERHOUSE] = TRUE
		sCMHDZData.iTotalLevelShots[CMHDZ_LEVEL_SLAUGHTERHOUSE] = 100
		sCMHDZData.iTotalLevelEnemyHits[CMHDZ_LEVEL_SLAUGHTERHOUSE] = 100
		CMHDZ_ASIGN_STAR_RATING(CMHDZ_LEVEL_SLAUGHTERHOUSE)
		
		CMHDZ_SET_LEVEL_BIT_FOR_LEVEL(CMHDZ_LEVEL_CITY, CMHDZ_LEVELS_BIT_CHALLENGE_MONKEY_C_MONKEY_DIE_MONKEY_KILL)
		CMHDZ_SET_LEVEL_BIT_FOR_LEVEL(CMHDZ_LEVEL_SCRAPYARD, CMHDZ_LEVELS_BIT_CHALLENGE_MONKEY_C_MONKEY_DIE_BUNNY_KILL)
		CMHDZ_SET_LEVEL_BIT_FOR_LEVEL(CMHDZ_LEVEL_ASYLUM, CMHDZ_LEVELS_BIT_CHALLENGE_MONKEY_C_MONKEY_DIE_BUNNY_KILL)
		CMHDZ_SET_LEVEL_BIT_FOR_LEVEL(CMHDZ_LEVEL_PARK, CMHDZ_LEVELS_BIT_CHALLENGE_MONKEY_C_MONKEY_DIE_MONKEY_KILL)
		
		sCMHDZData.iTotalLevelEnemyHeadShots[CMHDZ_LEVEL_CITY] = 10
		sCMHDZData.iTotalLevelEnemyHeadShots[CMHDZ_LEVEL_SCRAPYARD] = 10
		sCMHDZData.iTotalLevelEnemyHeadShots[CMHDZ_LEVEL_ASYLUM] = 10
		sCMHDZData.iTotalLevelEnemyHeadShots[CMHDZ_LEVEL_PARK] = 10
		sCMHDZData.iTotalLevelEnemyHeadShots[CMHDZ_LEVEL_SLAUGHTERHOUSE] = 10
		
	ELIF IS_DEBUG_KEY_JUST_PRESSED(KEY_6, KEYBOARD_MODIFIER_CTRL, "Debug Give score") 
		playerBd[PARTICIPANT_ID_TO_INT()].iPlayerScore[CMHDZ_GET_CURRENT_LEVEL()] = 20000
		playerBd[PARTICIPANT_ID_TO_INT()].iPlayerHiScore[CMHDZ_GET_CURRENT_LEVEL()] = 20000
		sCMHDZData.iTotalLevelShots[CMHDZ_GET_CURRENT_LEVEL()] = 100
		sCMHDZData.iTotalLevelEnemyHits[CMHDZ_GET_CURRENT_LEVEL()] = 100
		sCMHDZData.bLevelsCompleted[CMHDZ_GET_CURRENT_LEVEL()] = TRUE
		CMHDZ_ASIGN_STAR_RATING(sCMHDZData.eCurrentLevel)
		CMHDZ_SET_LOCAL_STATE(CMHDZ_ARCADE_CLIENT_STATE_RESULT_SCREEN)
	ENDIF

#ENDIF
		
			
		ELSE
			CLEANUP_SCRIPT()
			EXIT
		ENDIF
		
//	////CDEBUG1LN(DEBUG_MINIGAME, "[CMHDZ_ARCADE] {DSW} [RUNNING] Current stack: ",GET_CURRENT_STACK_SIZE(), " Allocated: ", GET_ALLOCATED_STACK_SIZE())
	IF NETWORK_IS_GAME_IN_PROGRESS()

	
		SWITCH  CMHDZ_GET_CLIENT_STATE(PARTICIPANT_ID_TO_INT())
			// Wait untill the server gives the all go before moving on
			CASE CMHDZ_GAME_STATE_INIT
				IF CMHDZ_GET_SERVER_STATE() = CMHDZ_GAME_STATE_RUNNING
					//CMHDZ_INIT_TEXTURE_DICTIONARIES()
					CMHDZ_STORE_RIVAL_PLAYER_PART_ID()
					ARCADE_GAMES_SOUND_INIT(CASINO_ARCADE_GAME_TLG) // needed for alex's LB sounds
					CMHDZ_INIT_SOUND()
					
					ARCADE_GAMES_POSTFX_INIT(CASINO_ARCADE_GAME_CAMHEDZ)
					sCMHDZData.iTotalTimePlayed = GET_GAME_TIMER()
					CMHDZ_SET_CLIENT_STATE( CMHDZ_GAME_STATE_RUNNING )
					
				ELIF CMHDZ_GET_SERVER_STATE() = CMHDZ_GAME_STATE_END
					CMHDZ_SET_CLIENT_STATE( CMHDZ_GAME_STATE_END )
					
				ENDIF
			BREAK
			
			CASE CMHDZ_GAME_STATE_RUNNING
				IF CMHDZ_GET_SERVER_STATE() = CMHDZ_GAME_STATE_RUNNING
					PROCESS_CMHDZ_ARCADE()
					
					IF CMHDZ_HANDLE_PLAYER_QUITTING()
						CMHDZ_SET_CLIENT_STATE( CMHDZ_GAME_STATE_LEAVE )
					ENDIF
					
				ELIF CMHDZ_GET_SERVER_STATE() = CMHDZ_GAME_STATE_END
					CMHDZ_SET_CLIENT_STATE( CMHDZ_GAME_STATE_END )
			
				ENDIF
				
			BREAK
			
		
			// The game stage the local player is placed when we want him to leave a mission
			CASE CMHDZ_GAME_STATE_LEAVE
				CMHDZ_SET_CLIENT_STATE( CMHDZ_GAME_STATE_END )

			FALLTHRU
			
			//Cleans up then terminates the mission
			CASE CMHDZ_GAME_STATE_END
				CLEANUP_SCRIPT()
				EXIT
			BREAK
			
		ENDSWITCH
		
		// -----------------------------------
		// Process server game logic		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			
			SWITCH CMHDZ_GET_SERVER_STATE()
				
				
				CASE CMHDZ_GAME_STATE_INIT
					
					CMHDZ_SET_SERVER_STATE( CMHDZ_GAME_STATE_RUNNING ) 	
					cmhdzServerBd.iMatchId= GET_CLOUD_TIME_AS_INT()
				BREAK
				
				// Look for game end conditions
				CASE CMHDZ_GAME_STATE_RUNNING
					CMHDZ_PROCESS_SERVER_LOOP()
									
					IF HAVE_MISSION_END_CONDITIONS_BEEN_MET()
						CMHDZ_SET_SERVER_STATE( CMHDZ_GAME_STATE_END )
						
					ENDIF
					
				BREAK
				
				CASE CMHDZ_GAME_STATE_END
				BREAK
				
			ENDSWITCH
			
		ENDIF
		
	ENDIF	
		#IF IS_DEBUG_BUILD
			IF IS_KEYBOARD_KEY_PRESSED(KEY_F)
				CLEANUP_SCRIPT()
				EXIT
			ENDIF
		#ENDIF
	ENDWHILE

ENDSCRIPT

