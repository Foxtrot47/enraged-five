USING "globals.sch"
USING "net_events.sch"
USING "net_wait_zero.sch"
USING "net_mission.sch"
USING "ggsm_main.sch"

#IF IS_DEBUG_BUILD
	WIDGET_GROUP_ID widgetMinigame
#ENDIF

PROC CLEANUP_SCRIPT()
	GGSM_CLEANUP_AND_EXIT_CLIENT()
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC

/// PURPOSE:
///    Do necessary pre game start initialization.
PROC PROCESS_PRE_GAME(MP_MISSION_DATA missionScriptArgs)
	missionScriptArgs = missionScriptArgs
	NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()
ENDPROC

SCRIPT(MP_MISSION_DATA missionScriptArgs)
	
	missionScriptArgs.mdID.idMission = eAM_GGSM_ARCADE_CABINET
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		PROCESS_PRE_GAME(missionScriptArgs)
	ENDIF
		
	WHILE TRUE
		MP_LOOP_WAIT_ZERO()
		
		IF NETWORK_IS_GAME_IN_PROGRESS()
		
			#IF IS_DEBUG_BUILD
				IF NOT DOES_WIDGET_GROUP_EXIST(widgetMinigame)
					widgetMinigame = START_WIDGET_GROUP("Go Go Space Monkey Minigame")
					STOP_WIDGET_GROUP()
					GGSM_DEBUG_SET_PARENT_WIDGET_GROUP(widgetMinigame)
				ENDIF
			#ENDIF
		
			PROCESS_GGSM()
		ELSE
			CLEANUP_SCRIPT()
		ENDIF
		
		IF IS_CONTROL_HELD(sGGSMData.sQuitTimer, TRUE, 2000)
			#IF IS_DEBUG_BUILD
				GGSM_DEBUG_DUMP_DATA_TO_LOGS()
			#ENDIF
			
			sGGSMData.sArcadeCabinetAnimEvent.bHoldLastFrame = FALSE
			sGGSMData.sArcadeCabinetAnimEvent.bLoopAnim = FALSE
			PLAY_ARCADE_CABINET_ANIMATION(sGGSMData.sArcadeCabinetAnimEvent, ARCADE_CABINET_ANIM_CLIP_EXIT)
			CLEANUP_SCRIPT()
		ELIF IS_LOCAL_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR()
		OR sGGSMData.bGameForceQuit
			#IF IS_DEBUG_BUILD
				GGSM_DEBUG_DUMP_DATA_TO_LOGS()
			#ENDIF
			
			CLEANUP_SCRIPT()
		ENDIF
		
		#IF IS_DEBUG_BUILD
			IF IS_KEYBOARD_KEY_PRESSED(KEY_F)
				GGSM_DEBUG_DUMP_DATA_TO_LOGS()
				CLEANUP_SCRIPT()
			ENDIF
		#ENDIF
	ENDWHILE

ENDSCRIPT