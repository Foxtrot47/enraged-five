USING "globals.sch"
USING "net_events.sch"
USING "net_wait_zero.sch"
USING "net_mission.sch"
USING "gunslinger_arcade_main.sch"

#IF IS_DEBUG_BUILD
	WIDGET_GROUP_ID widgetMinigame
#ENDIF







PROC CLEANUP_SCRIPT()

	TLG_CLEANUP_AND_EXIT_CLIENT()
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
	
ENDPROC

/// PURPOSE:
///    Do necessary pre game start initialization.
PROC PROCESS_PRE_GAME(MP_MISSION_DATA missionScriptArgs)
	CDEBUG1LN(DEBUG_MINIGAME, "[TLG_ARCADE] [JS] [DSW] [PROCESS_PRE_GAME]")
	 
//	CDEBUG1LN(DEBUG_MINIGAME, "[TLG_ARCADE] {DSW} [PROCESS_PRE_GAME] Current stack: ",GET_CURRENT_STACK_SIZE(), " Allocated: ", GET_ALLOCATED_STACK_SIZE())
	missionScriptArgs = missionScriptArgs
	
	// This marks the script as a net script, and handles any instancing setup. 
	SETUP_MP_MISSION_FOR_NETWORK(GET_MAX_NUM_PARTICIPANTS_FOR_MP_MISSION(missionScriptArgs.mdID.idMission) , missionScriptArgs)
	
	// This makes sure the net script is active, waits until it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(tlgServerBd, SIZE_OF(tlgServerBd))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBd, SIZE_OF(playerBd))
	
	// This script will not be paused if another script calls PAUSE_GAME
	SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
	NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()
	
	TLG_PREPARE_MUSIC_EVENT(TLG_MUSIC_EVENT_INTRO)
	
	
ENDPROC

FUNC INT TLG_GET_SERVER_STATE()
	RETURN tlgServerBd.iServerGameState
ENDFUNC

PROC TLG_SET_SERVER_STATE(INT iNewState)
	#IF IS_DEBUG_BUILD
	STRING sCurrent = TLG_GET_SERVER_STATE_AS_STRING(tlgServerBd.iServerGameState)
	STRING sNew 	= TLG_GET_SERVER_STATE_AS_STRING(iNewState)
	CDEBUG1LN(DEBUG_MINIGAME, "[TLG_ARCADE] {DSW} [TLG_SET_SERVER_STATE] ", sCurrent, " -> ", sNew)
	#ENDIF
	
	tlgServerBd.iServerGameState = iNewState
ENDPROC

FUNC INT TLG_GET_CLIENT_STATE(INT iPart)
	RETURN playerBd[iPart].iGameState
ENDFUNC

PROC TLG_SET_CLIENT_STATE(INT iNewState)
	#IF IS_DEBUG_BUILD
	STRING sCurrent = TLG_GET_SERVER_STATE_AS_STRING(playerBd[PARTICIPANT_ID_TO_INT()].iGameState)
	STRING sNew 	= TLG_GET_SERVER_STATE_AS_STRING(iNewState)
	CDEBUG1LN(DEBUG_MINIGAME, "[TLG_ARCADE] {DSW} [TLG_SET_CLIENT_STATE] ", sCurrent, " -> ", sNew)
	#ENDIF
	
	playerBd[PARTICIPANT_ID_TO_INT()].iGameState = iNewState
ENDPROC

FUNC BOOL HAVE_MISSION_END_CONDITIONS_BEEN_MET()
	RETURN FALSE
ENDFUNC

PROC TLG_PROCESS_SERVER_LOOP()
	INT i, j
	INT iNumPlayers
	INT iHiScore
	INT iScore
	INT iPartSort = -1
	REPEAT GET_MAX_NUM_PARTICIPANTS_FOR_MP_MISSION(eAM_TLG_ARCADE_CABINET) i
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_NATIVE(PARTICIPANT_INDEX, i))
			iNumPlayers++
			
			REPEAT TLG_MAX_LEVELS j
				iHiScore = TLG_GET_PARTICIPANT_HI_SCORE_FOR_LEVEL(i, j)
				IF tlgServerBd.sPlayerScores[i].iHiScoreForLevel[j] != iHiScore
					tlgServerBd.sPlayerScores[i].iHiScoreForLevel[j] = iHiScore
					CDEBUG1LN(DEBUG_MINIGAME, "[TLG_ARCADE] {DSW} [TLG_PROCESS_SERVER_LOOP] part ",i, " Hi Score for level ", j, " updated to ", tlgServerBd.sPlayerScores[i].iHiScoreForLevel[j])
				ENDIF
				
				iScore = TLG_GET_PARTICIPANT_SCORE_FOR_LEVEL(i, j)
				IF tlgServerBd.sPlayerScores[i].iCurrentScoreForLevel[j] != iScore
					tlgServerBd.sPlayerScores[i].iCurrentScoreForLevel[j] = iScore
					CDEBUG1LN(DEBUG_MINIGAME, "[TLG_ARCADE] {DSW} [TLG_PROCESS_SERVER_LOOP] part ",i, " Score for level ", j, " updated to ", tlgServerBd.sPlayerScores[i].iCurrentScoreForLevel[j])
				ENDIF
			ENDREPEAT
			
			IF NOT IS_BIT_SET(tlgServerBd.iSortLbForPart, i) 
				IF IS_BIT_SET(playerBd[i].iPlayerBitset, ENUM_TO_INT(TLG_PLAYER_BD_BIT_EDITING_LEADERBOARD))
					SET_BIT(tlgServerBd.iSortLbForPart, i)
					iPartSort = i
					CDEBUG1LN(DEBUG_MINIGAME, "[TLG_ARCADE] {DSW} [TLG_PROCESS_SERVER_LOOP] [LEADERBOARD] Should sort because part ", i, " wants to edit the leaderboard")
				ENDIF
			ELSE
				IF NOT IS_BIT_SET(playerBd[i].iPlayerBitset, ENUM_TO_INT(TLG_PLAYER_BD_BIT_EDITING_LEADERBOARD))
					CLEAR_BIT(tlgServerBd.iSortLbForPart, i)
					CDEBUG1LN(DEBUG_MINIGAME, "[TLG_ARCADE] {DSW} [TLG_PROCESS_SERVER_LOOP] [LEADERBOARD] Part ", i, " no longer wants to edit the leaderboard")
				ENDIF
			ENDIF
			
		ENDIF
	ENDREPEAT
	
	
	
	IF iPartSort > -1
		FOR i = 0 TO ciCASINO_ARCADE_LEADERBOARD_POSITIONS - 1
			IF tlgServerBd.sLeaderboard[i].iPlayer = iPartSort 
				tlgServerBd.sLeaderboard[i].iPlayer = -1
			ENDIF
		ENDFOR
		
		TLG_SORT_SERVER_LEADERBOARD(playerBd, tlgServerBd.sLeaderboard, iPartSort)
	ENDIF
	
	IF tlgServerBd.iNumPlayers != iNumPlayers
		tlgServerBd.iNumPlayers = iNumPlayers
		
		CDEBUG1LN(DEBUG_MINIGAME, "[TLG_ARCADE] {DSW} [TLG_PROCESS_SERVER_LOOP] tlgServerBd.iNumPlayers: ", tlgServerBd.iNumPlayers)
		
		IF iNumPlayers = 2
			//-- For telemetry
			IF NOT IS_BIT_SET(tlgServerBd.iServerBitset, ENUM_TO_INT(TLG_SERVER_BIT_TWO_PLAYERS_JOINED))
				SET_BIT(tlgServerBd.iServerBitset, ENUM_TO_INT(TLG_SERVER_BIT_TWO_PLAYERS_JOINED))
				CDEBUG1LN(DEBUG_MINIGAME, "[TLG_ARCADE] {DSW} [TLG_PROCESS_SERVER_LOOP] Set TLG_SERVER_BIT_TWO_PLAYERS_JOINED")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL TLG_HANDLE_PLAYER_QUITTING()
	IF IS_LOCAL_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR()
		CDEBUG1LN(DEBUG_MINIGAME, "[TLG_ARCADE] {DSW} [TLG_HANDLE_PLAYER_QUITTING] True - IS_LOCAL_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR")
		RETURN TRUE
	ENDIF
	
	INT iQuitTime = 1000

	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		
		IF IS_CONTROL_PRESSED(sTLGData.sPcQuitTimer.control, sTLGData.sPcQuitTimer.action)
		OR (IS_DISABLED_CONTROL_PRESSED(sTLGData.sPcQuitTimer.control, sTLGData.sPcQuitTimer.action))
			DRAW_GENERIC_METER(ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), sTLGData.sPcQuitTimer.sTimer.Timer)), iQuitTime, "DEG_GAME_QUIT")
		ELSE
			DRAW_GENERIC_METER(0, iQuitTime, "DEG_GAME_QUIT")
		ENDIF

	ELSE
		
		IF IS_CONTROL_PRESSED(sTLGData.sControlTimer.control, sTLGData.sControlTimer.action)
		OR (IS_DISABLED_CONTROL_PRESSED(sTLGData.sControlTimer.control, sTLGData.sControlTimer.action))
			DRAW_GENERIC_METER(ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), sTLGData.sControlTimer.sTimer.Timer)), iQuitTime, "DEG_GAME_QUIT")
		ELSE
			DRAW_GENERIC_METER(0, iQuitTime, "DEG_GAME_QUIT")
		ENDIF
	
	ENDIF
	
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		IF IS_CONTROL_HELD(sTLGData.sPcQuitTimer, DEFAULT, iQuitTime)
			CDEBUG1LN(DEBUG_MINIGAME, "[TLG_ARCADE] {DSW} [TLG_HANDLE_PLAYER_QUITTING] True - sPcQuitTimer")
			RETURN TRUE
		ENDIF
	ELSE
		IF IS_CONTROL_HELD(sTLGData.sControlTimer, DEFAULT, iQuitTime)
			CDEBUG1LN(DEBUG_MINIGAME, "[TLG_ARCADE] {DSW} [TLG_HANDLE_PLAYER_QUITTING] True - sControlTimer")
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


SCRIPT(MP_MISSION_DATA missionScriptArgs)
	
	missionScriptArgs.mdID.idMission = eAM_TLG_ARCADE_CABINET
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		PROCESS_PRE_GAME(missionScriptArgs)
	ENDIF
	
	WHILE TRUE
		MP_LOOP_WAIT_ZERO()
		
		IF NETWORK_IS_GAME_IN_PROGRESS()
		
#IF IS_DEBUG_BUILD
			IF NOT DOES_WIDGET_GROUP_EXIST(widgetMinigame)
				widgetMinigame = START_WIDGET_GROUP("The Last Gunslingers Arcade Cabinet Minigame")
				STOP_WIDGET_GROUP()
				TLG_DEBUG_SET_PARENT_WIDGET_GROUP(widgetMinigame)
			ENDIF
			
			TLG_UPDATE_WIDGETS()
#ENDIF
		
			
		ELSE
			CLEANUP_SCRIPT()
		ENDIF
		
//	CDEBUG1LN(DEBUG_MINIGAME, "[TLG_ARCADE] {DSW} [RUNNING] Current stack: ",GET_CURRENT_STACK_SIZE(), " Allocated: ", GET_ALLOCATED_STACK_SIZE())
		SWITCH  TLG_GET_CLIENT_STATE(PARTICIPANT_ID_TO_INT())
			// Wait untill the server gives the all go before moving on
			CASE TLG_GAME_STATE_INIT
				IF TLG_GET_SERVER_STATE() = TLG_GAME_STATE_RUNNING
					TLG_INIT_TEXTURE_DICTIONARIES()
					TLG_STORE_RIVAL_PLAYER_PART_ID()
					ARCADE_GAMES_SOUND_INIT(CASINO_ARCADE_GAME_TLG) // needed for alex's LB sounds
					TLG_INIT_SOUND()
					
					ARCADE_GAMES_POSTFX_INIT(CASINO_ARCADE_GAME_TLG)
					sTLGData.iTotalTimePlayed = GET_GAME_TIMER()
					TLG_SET_CLIENT_STATE( TLG_GAME_STATE_RUNNING )
					
				ELIF TLG_GET_SERVER_STATE() = TLG_GAME_STATE_END
					TLG_SET_CLIENT_STATE( TLG_GAME_STATE_END )
					
				ENDIF
			BREAK
			
			CASE TLG_GAME_STATE_RUNNING
				IF TLG_GET_SERVER_STATE() = TLG_GAME_STATE_RUNNING
					PROCESS_TLG_ARCADE()
					
					IF TLG_HANDLE_PLAYER_QUITTING()
						TLG_SET_CLIENT_STATE( TLG_GAME_STATE_LEAVE )
					ENDIF
					
				ELIF TLG_GET_SERVER_STATE() = TLG_GAME_STATE_END
					TLG_SET_CLIENT_STATE( TLG_GAME_STATE_END )
			
				ENDIF
			BREAK
			
		
			// The game stage the local player is placed when we want him to leave a mission
			CASE TLG_GAME_STATE_LEAVE
				TLG_SET_CLIENT_STATE( TLG_GAME_STATE_END )

			FALLTHRU
			
			//Cleans up then terminates the mission
			CASE TLG_GAME_STATE_END
				CLEANUP_SCRIPT()
			BREAK
			
		ENDSWITCH
		
		// -----------------------------------
		// Process server game logic		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			
			SWITCH TLG_GET_SERVER_STATE()
				
				
				CASE TLG_GAME_STATE_INIT
					
					TLG_SET_SERVER_STATE( TLG_GAME_STATE_RUNNING ) 	
					tlgServerBd.iMatchId= GET_CLOUD_TIME_AS_INT()
				BREAK
				
				// Look for game end conditions
				CASE TLG_GAME_STATE_RUNNING
					TLG_PROCESS_SERVER_LOOP()
					
				
					
					IF HAVE_MISSION_END_CONDITIONS_BEEN_MET()
						TLG_SET_SERVER_STATE( TLG_GAME_STATE_END )
						
					ENDIF
					
				BREAK
				
				CASE TLG_GAME_STATE_END
				BREAK
				
			ENDSWITCH
			
		ENDIF
		
		
		#IF IS_DEBUG_BUILD
			IF IS_KEYBOARD_KEY_PRESSED(KEY_F)
				CLEANUP_SCRIPT()
			ENDIF
		#ENDIF
	ENDWHILE

ENDSCRIPT