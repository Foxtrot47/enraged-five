USING "globals.sch"
USING "net_events.sch"
USING "net_wait_zero.sch"
USING "net_mission.sch"
USING "degenatron_games_main.sch"

#IF IS_DEBUG_BUILD
	WIDGET_GROUP_ID widgetMinigame
#ENDIF

PROC CLEANUP_SCRIPT()

	DEGENATRON_GAMES_CLEANUP_AND_EXIT_CLIENT()
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
	
ENDPROC

/// PURPOSE:
///    Do necessary pre game start initialization.
PROC PROCESS_PRE_GAME(MP_MISSION_DATA missionScriptArgs)
	
	missionScriptArgs = missionScriptArgs
	
	NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()
	
ENDPROC

SCRIPT(MP_MISSION_DATA missionScriptArgs)
	
	missionScriptArgs.mdID.idMission = eAM_DEGENATRON_GAMES_CABINET
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		PROCESS_PRE_GAME(missionScriptArgs)
	ENDIF
		
	WHILE TRUE
		MP_LOOP_WAIT_ZERO()
		
		IF NETWORK_IS_GAME_IN_PROGRESS()
		
#IF IS_DEBUG_BUILD
			IF NOT DOES_WIDGET_GROUP_EXIST(widgetMinigame)
				widgetMinigame = START_WIDGET_GROUP("Degenatron Games Cabinet Minigame")
				STOP_WIDGET_GROUP()
				DEGENATRON_GAMES_DEBUG_SET_PARENT_WIDGET_GROUP(widgetMinigame)
			ENDIF
#ENDIF
		
			PROCESS_DEGENATRON_GAMES()
		ELSE
			CLEANUP_SCRIPT()
		ENDIF

		#IF IS_DEBUG_BUILD
			IF IS_KEYBOARD_KEY_PRESSED(KEY_F)
				CLEANUP_SCRIPT()
			ENDIF
			IF IS_DEBUG_KEY_JUST_PRESSED(KEY_M, KEYBOARD_MODIFIER_CTRL_SHIFT, "Ctrl+Shift+M Pressed")
				sDegenatronGamesData.bShowHelp = NOT sDegenatronGamesData.bShowHelp
			ENDIF
		#ENDIF
	ENDWHILE

ENDSCRIPT